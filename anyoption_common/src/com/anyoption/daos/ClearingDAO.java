package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.bl_vos.ErrorCode;
import com.anyoption.common.daos.ClearingDAOBase;


/**
 * Clearing DAO.
 *
 * @author Tony
 */
public class ClearingDAO extends ClearingDAOBase {

	 private static final Logger log = Logger.getLogger(ClearingDAO.class);

		public static ArrayList<ErrorCode> getAllErrorCodes(Connection conn) throws SQLException{
			ArrayList<ErrorCode> list = new ArrayList<ErrorCode>();
			PreparedStatement ps = null;
			ResultSet rs = null;
			String sql = " SELECT cec.* ,cpg.CLEARING_PROVIDER_ID  " +
						 " FROM clearing_routes_error_codes cec, clearing_providers_groups cpg " +
						 " WHERE cec.CLEARING_PROVIDER_ID_GROUP = cpg.GROUP_ID ";
			try{
				ps = conn.prepareStatement(sql);
				rs = ps.executeQuery();

				while(rs.next()){
					ErrorCode ec = new ErrorCode();
					ec.setId(rs.getLong("id"));
					ec.setResult(rs.getString("result"));
					ec.setSpecialCode(rs.getLong("is_special_code") == 0 ? false : true);
					ec.setClearingProviderId(rs.getLong("clearing_provider_id"));
					ec.setClearingProviderIdGroup(rs.getLong("clearing_provider_id_group"));
					list.add(ec);
				}

			}finally{
				closeStatement(ps);
				closeResultSet(rs);
			}
			return list;
		}
}