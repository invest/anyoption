package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Bonus;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.BonusUsersStep;
import com.anyoption.common.bl_vos.BonusCurrency;
import com.anyoption.common.bonus.BonusHandlerBase;
import com.anyoption.common.bonus.BonusHandlerFactory;
import com.anyoption.common.bonus.BonusHandlersException;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.util.BonusFormulaDetails;
import com.anyoption.util.CommonUtil;
import com.anyoption.util.ConstantsBase;
//import com.anyoption.bonus.BonusHandlersException;

public class BonusDAOBase extends com.anyoption.common.daos.BonusDAOBase {
	 public static final Logger log = Logger.getLogger(BonusDAOBase.class);


    public static int useNextInvestOnUs(Connection conn, long bonusUsersId, long amount) throws SQLException {
        int updatedCount = 0;
        PreparedStatement pstmt = null;
        try {
            String sql =
                    "UPDATE " +
                        "bonus_users " +
                    "SET " +
                        "bonus_state_id = 3, " +
                        "time_used = current_date, " +
                        "sum_invest_withdraw = wagering_parameter * ? " +
                    "WHERE " +
                        "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, amount);
            pstmt.setLong(2, bonusUsersId);
            updatedCount = pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
        return updatedCount;
    }

    /**
     * Get bonus by id
     * @param con db connection
     * @param id bonus id for search
     * @return
     * @throws SQLException
     */
    public static Bonus getBonusById(Connection conn, long id) throws SQLException {
        Bonus b = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "b.*, " +
                    "bt.class_type_id " +
                "FROM " +
                    "bonus b, " +
                    "bonus_types bt " +
                "WHERE " +
                    "b.id = ? AND " +
                    "b.type_id = bt.id";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                b = getVO(rs);
                b.setClassType(rs.getLong("class_type_id"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return b;
    }

    /**
     * Fill Bonus object
     *
     * @param rs ResultSet
     * @return Bonus object
     * @throws SQLException
     */
    protected static Bonus getVO(ResultSet rs) throws SQLException {
        Bonus vo = new Bonus();
        vo.setId(rs.getLong("ID"));
        vo.setStartDate(convertToDate(rs.getTimestamp("START_DATE")));
        vo.setEndDate(convertToDate(rs.getTimestamp("END_DATE")));
        vo.setWageringParameter(rs.getLong("WAGERING_PARAMETER"));
        vo.setDefaultPeriod(rs.getLong("DEFAULT_PERIOD"));
        vo.setAutoRenewInd(rs.getLong("AUTOMATIC_RENEW_IND"));
        vo.setRenewFrequency(rs.getLong("RENEW_FREQUENCY"));
        vo.setTypeId(rs.getLong("TYPE_ID"));
        vo.setWriterId(rs.getLong("WRITER_ID"));
        vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
        vo.setNumberOfActions(rs.getLong("NUMBER_OF_ACTIONS"));
        vo.setName(rs.getString("name"));
        vo.setHasSteps(rs.getBoolean("HAS_STEPS"));
        vo.setOddsWin(rs.getDouble("odds_win"));
        vo.setOddsLose(rs.getDouble("odds_lose"));
        vo.setRoundUpType(rs.getInt("round_up_type"));
        return vo;
    }

    /**
     * Fill BonusUsers object
     * @param rs ResultSet
     * @return Bonus object
     * @throws SQLException
     */
    public static BonusUsers getBonusUsersVO(ResultSet rs, Connection conn) throws SQLException {
        BonusUsers vo = new BonusUsers();
        vo.setId(rs.getLong("id"));
        vo.setUserId(rs.getLong("user_id"));
        vo.setStartDate(convertToDate(rs.getTimestamp("start_date")));
        vo.setEndDate(convertToDate(rs.getTimestamp("end_date")));
        vo.setBonusStateId(rs.getLong("bonus_state_id"));
        vo.setBonusAmount(rs.getLong("bonus_amount"));
        vo.setBonusPercent(rs.getDouble("bonus_percent"));
        vo.setMinDepositAmount(rs.getLong("min_deposit_amount"));
        vo.setMaxDepositAmount(rs.getLong("max_deposit_amount"));
        vo.setSumInvQualify(rs.getLong("sum_invest_qualify"));
        vo.setWriterId(rs.getLong("writer_id"));
        vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
        vo.setSumInvQualifyReached(rs.getLong("sum_invest_qualify_reached"));
        vo.setSumInvWithdrawal(rs.getLong("sum_invest_withdraw"));
        vo.setSumInvWithdrawalReached(rs.getLong("sum_invest_withdraw_reached"));
        vo.setTimeActivated(convertToDate(rs.getTimestamp("time_activated")));
        vo.setTimeUsed(convertToDate(rs.getTimestamp("time_used")));
        vo.setTimeDone(convertToDate(rs.getTimestamp("time_done")));
        vo.setTypeId(rs.getLong("type_id"));
        vo.setWageringParam(rs.getLong("wagering_parameter"));
        vo.setNumOfActions(rs.getLong("number_of_actions"));
        vo.setNumOfActionsReached(rs.getLong("number_of_actions_reached"));
        vo.setOddsWin(rs.getDouble("odds_win"));
        vo.setOddsLose(rs.getDouble("odds_lose"));
        vo.setMinInvestAmount(rs.getLong("min_invest_amount"));
        vo.setMaxInvestAmount(rs.getLong("max_invest_amount"));
        vo.setSumDeposits(rs.getLong("sum_deposits"));
		vo.setSumDepositsReached(rs.getLong("sum_deposits_reached"));
		vo.setActivatedTransactionId(rs.getLong("activated_transaction_id"));
		vo.setActivatedInvestmentId(rs.getLong("activated_investment_id"));
		vo.setWageringEendInvestmentId(rs.getLong("wagering_end_investment_id"));

        long bonusId = rs.getLong("bonus_id");
        vo.setBonusId(bonusId);
        vo.setHasSteps(getBonusById(conn, bonusId).isHasSteps());
        vo.setAdjustedAmount(rs.getLong("adjusted_amount"));
		vo.setTimeWageringWaived(convertToDate(rs.getTimestamp("time_wagering_waived")));
		vo.setTimeWithdrawn(convertToDate(rs.getTimestamp("time_withdrawn")));

        return vo;
    }

    public static void useBonusUsers(Connection conn, long bonusUsersId) throws SQLException {
        PreparedStatement pstmt = null;
        try{
            String sql =
                    "UPDATE " +
                        "bonus_users " +
                    "SET " +
                        "bonus_state_id = " + Bonus.STATE_USED + ", " +
                        "time_used = current_date " +
                    "WHERE " +
                        "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, bonusUsersId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

//    public static void addBonusUserWithdrawWagering(Connection conn, long bonusUsersId, long amount) throws SQLException {
//        PreparedStatement pstmt = null;
//        try{
//            String sql =
//                    "UPDATE " +
//                        "bonus_users " +
//                    "SET " +
//                        "sum_invest_withdraw_reached = sum_invest_withdraw_reached + ? " +
//                    "WHERE " +
//                        "id = ?";
//            pstmt = conn.prepareStatement(sql);
//            pstmt.setLong(1, amount);
//            pstmt.setLong(2, bonusUsersId);
//            pstmt.executeUpdate();
//        } finally {
//            closeStatement(pstmt);
//        }
//    }

//    public static void doneBonusUsers(Connection conn, long bonusUsersId, boolean updateSumInvestWithdraw, long investmentId) throws SQLException {
//        PreparedStatement pstmt = null;
//        try{
//            String sql =
//                    " UPDATE " +
//                        " bonus_users " +
//                    " SET ";
//
//            if (updateSumInvestWithdraw){
//            	sql += " sum_invest_withdraw_reached = sum_invest_withdraw, " +
//            		   " wagering_end_investment_id = " + investmentId + " , ";
//            }
//
//            sql +=      " bonus_state_id = " + Bonus.STATE_DONE + " , " +
//                        " time_done = current_date " +
//                    " WHERE " +
//                        " id = ?";
//            pstmt = conn.prepareStatement(sql);
//            pstmt.setLong(1, bonusUsersId);
//            pstmt.executeUpdate();
//        } finally {
//            closeStatement(pstmt);
//        }
//    }

    public static ArrayList<BonusUsers> getForActivationByInvestmentAmount(Connection conn, long userId) throws SQLException {
        ArrayList<BonusUsers> list = new ArrayList<BonusUsers>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try{
            String sql =
                    "SELECT " +
                        "* " +
                    "FROM " +
                        "bonus_users " +
                    "WHERE " +
                        "user_id = ? AND " +
                        "bonus_state_id = " + Bonus.STATE_GRANTED + " AND " +
                        "(start_date IS NULL OR start_date <= current_date) AND " +
                        "(end_date IS NULL OR end_date >= current_date) AND " +
                        "type_id IN (" +
                                Bonus.TYPE_AMOUNT_AFTER_WAGERING + ", " +
                                Bonus.TYPE_NEXT_INVEST_ON_US_AFTER_WAGERING +
                        ") " +
                    "ORDER BY " +
                        "time_created";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                list.add(getBonusUsersVO(rs, conn));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }

    public static void addBonusUserQualifyWagering(Connection conn, long bonusUsersId, long amount) throws SQLException {
        PreparedStatement pstmt = null;
        try{
            String sql =
                    "UPDATE " +
                        "bonus_users " +
                    "SET " +
                        "sum_invest_qualify_reached = sum_invest_qualify_reached + ? " +
                    "WHERE " +
                        "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, amount);
            pstmt.setLong(2, bonusUsersId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

//    public static void activateBonusUsers(Connection conn, BonusUsers bu, long transactionId, long investmentId) throws SQLException {
//        PreparedStatement pstmt = null;
//        int index = 1;
//        try{
//            String sql =
//                    "UPDATE " +
//                        "bonus_users " +
//                    "SET " +
//                        "bonus_state_id = " + Bonus.STATE_ACTIVE + ", " +
//                        "time_activated = current_date, " +
//                        "number_of_actions_reached = number_of_actions_reached + 1 , " +
//                        "sum_invest_qualify_reached = sum_invest_qualify , " +
//                        "bonus_amount = ? , " +
//                        "adjusted_amount = ? , " +
//                        "sum_invest_withdraw = ? ";
//
//            if (transactionId > 0){
//            	sql += " ,activated_transaction_id = "	+ transactionId + " ";
//            }
//
//            if (investmentId > 0){
//            	sql += " ,activated_investment_id = "	+ investmentId + " ";
//            }
//
//            if (bu.isHasSteps()){
//            	sql += 	"," +
//            			"bonus_percent = ?, " +
//			            "min_deposit_amount = ?, " +
//			            "max_deposit_amount  = ? ";
//            }
//
//            sql +=  "WHERE " +
//			            "id = ?";
//
//
//            pstmt = conn.prepareStatement(sql);
//
//            pstmt.setLong(index++, bu.getBonusAmount());
//            pstmt.setLong(index++, bu.getBonusAmount());
//            pstmt.setLong(index++, bu.getSumInvWithdrawal());
//
//            if (bu.isHasSteps()){
//				pstmt.setDouble(index++, bu.getBonusPercent());
//				pstmt.setLong(index++, bu.getMinDepositAmount());
//				pstmt.setLong(index++, bu.getMaxDepositAmount());
//            } 
//        	pstmt.setLong(index++, bu.getId());
//            
//            pstmt.executeUpdate();
//        } finally {
//            closeStatement(pstmt);
//        }
//    }

    public static ArrayList<BonusUsers> getForActivationByInvestmentCount(Connection conn, long userId) throws SQLException {
        ArrayList<BonusUsers> list = new ArrayList<BonusUsers>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try{
            String sql =
                    "SELECT " +
                        "* " +
                    "FROM " +
                        "bonus_users " +
                    "WHERE " +
                        "user_id = ? AND " +
                        "bonus_state_id = " + Bonus.STATE_GRANTED + " AND " +
                        "(start_date IS NULL OR start_date <= current_date) AND " +
                        "(end_date IS NULL OR end_date >= current_date) AND " +
                        "type_id = " + Bonus.TYPE_NEXT_INVEST_ON_US_AFTER_X_INVS + " " +
                    "ORDER BY " +
                        "time_created";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                list.add(getBonusUsersVO(rs, conn));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }

    public static void addBonusUsersAction(Connection conn, long bonusUsersId) throws SQLException {
        PreparedStatement pstmt = null;
        try{
            String sql =
                    "UPDATE " +
                        "bonus_users " +
                    "SET " +
                        "number_of_actions_reached = number_of_actions_reached + 1 " +
                    "WHERE " +
                        "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, bonusUsersId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    /**
     * Check if bonus on registration is available
     *
     * @param con db connection
     * @param bonusId the bonus id on registration
     * @return
     * @throws SQLException
     */
    public static boolean isBonusUponRegistrationAvailable(Connection conn, long bonusId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "bonus " +
                "WHERE " +
                    "id = ? AND " +
                    "sysdate >= start_date AND " +
                    "sysdate <= end_date";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, bonusId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                return true;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return false;
    }

//	/**
//	 * insert bonus to user
//	 * @param con db connection
//	 * @param popEntryId TODO
//	 * @throws SQLException
//	 */
//	public static boolean insertBonusUser(Connection con, BonusUsers bu, Bonus b,BonusCurrency bc, long popEntryId) throws SQLException {
//		PreparedStatement ps = null;
//		try {
//			String sql="INSERT " +
//						"INTO bonus_users(ID, " +
//										  "USER_ID," +
//										  "START_DATE," +
//										  "END_DATE," +
//										  "BONUS_STATE_ID," +
//										  "BONUS_AMOUNT," +
//										  "BONUS_PERCENT," +
//										  "MIN_DEPOSIT_AMOUNT," +
//										  "MAX_DEPOSIT_AMOUNT," +
//										  "SUM_INVEST_QUALIFY," +
//										  "WRITER_ID," +
//										  "TIME_CREATED," +
//										  "SUM_INVEST_QUALIFY_REACHED," +
//										  "SUM_INVEST_WITHDRAW," +
//										  "SUM_INVEST_WITHDRAW_REACHED," +
//										  "TIME_ACTIVATED," +
//										  "TIME_USED," +
//										  "TIME_DONE," +
//										  "TYPE_ID," +
//										  "WAGERING_PARAMETER," +
//										  "NUMBER_OF_ACTIONS," +
//										  "NUMBER_OF_ACTIONS_REACHED," +
//										  "BONUS_ID, " +
//										  "min_invest_amount, " +
//										  "max_invest_amount, " +
//										  "odds_win, " +
//										  "odds_lose," +
//										  "SUM_DEPOSITS," +
//										  "POPULATION_ENTRY_ID," +
//										  "ADJUSTED_AMOUNT) " +
//						"VALUES(SEQ_BONUS_USERS.nextval,?,sysdate,?,?,?,?,?,?,?," +
//								"?,sysdate,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
//
//			// calculate end date
//			Calendar cal = Calendar.getInstance();
//			cal.set(Calendar.HOUR_OF_DAY, 21);
//            cal.set(Calendar.MINUTE, 0);
//            cal.set(Calendar.SECOND, 0);
//
//			if (bu.getBonusId() != ConstantsBase.XMASS_BONUS_ID ){ // not xmass bonus
//				cal.add(Calendar.DAY_OF_MONTH, ((int)b.getDefaultPeriod()));
//			}else{ // xmass bonus
//	            cal.set(GregorianCalendar.DAY_OF_MONTH, 31);
//			}
//
//			ps = con.prepareStatement(sql);
//			ps.setLong(1, bu.getUserId());
//			ps.setTimestamp(2, convertToTimestamp(cal.getTime()));
//			ps.setLong(3, bu.getBonusStateId());
//			ps.setLong(4, bc.getBonusAmount());
//			ps.setDouble(5, bc.getBonusPercent());
//			ps.setLong(6, bc.getMinDepositAmount());
//			ps.setLong(7, bc.getMaxDepositAmount());
//			ps.setLong(8, bc.getSumInvestQualify());
//			ps.setLong(9, bu.getWriterId());
//			ps.setLong(10, bu.getSumInvQualifyReached());
//			ps.setLong(11, bu.getSumInvWithdrawal());
//			ps.setLong(12, bu.getSumInvWithdrawalReached());
//			ps.setTimestamp(13, convertToTimestamp(bu.getTimeActivated()));
//			ps.setTimestamp(14, null);
//			ps.setTimestamp(15, convertToTimestamp(bu.getTimeDone()));
//			ps.setLong(16, b.getTypeId());
//			ps.setLong(17, b.getWageringParameter());
//			ps.setLong(18, b.getNumberOfActions());
//			ps.setLong(19, bu.getNumOfActionsReached());
//			ps.setLong(20, bu.getBonusId());
//			ps.setLong(21, bc.getMinInvestAmount());
//			ps.setLong(22, bc.getMaxInvestAmount());
//			ps.setDouble(23, b.getOddsWin());
//			ps.setDouble(24, b.getOddsLose());
//			ps.setLong(25, bu.getSumDeposits());
//			if (popEntryId > 0){
//				ps.setLong(26, popEntryId);
//			}else{
//				ps.setString(26, null);
//			}
//			ps.setLong(27, bc.getBonusAmount());
//			ps.executeUpdate();
//
//			// save needed values in bonus user instance
//			bu.setTypeId(b.getTypeId());
//			bu.setBonusAmount(bc.getBonusAmount());
//			bu.setId(getSeqCurValue(con, "SEQ_BONUS_USERS"));
//
//			// Insert new email
//			try {
//				long skinId = UsersDAOBase.getSkinIdByUserId(con, bu.getUserId());
//				if (!CommonUtil.isHebrewSkin(skinId)) {
//					int langId = SkinsDAOBase.getById(con, skinId).getDefaultLanguageId();
//					MailBoxTemplate template = MailBoxTemplatesDAOBase.getTemplateByTypeSkinLanguage(con, ConstantsBase.MAILBOX_TYPE_BONUS, skinId, langId);
//					MailBoxUser email = new MailBoxUser();
//					email.setTemplateId(template.getId());
//					email.setUserId(bu.getUserId());
//					email.setWriterId(bu.getWriterId());
//					email.setSenderId(template.getSenderId());
//					email.setIsHighPriority(template.getIsHighPriority());
//					email.setPopupTypeId(template.getPopupTypeId());
//					email.setSubject(template.getSubject());
//					email.setBonusUserId(bu.getId());
//					MailBoxUsersDAOBase.insert(con, email);
//				}
//			} catch (Exception e) {
//				log.warn("Problem sending a new email to user inbox!", e);
//			}
//		} finally {
//			closeStatement(ps);
//		}
//		return true;
//	}

//	/**
//	 * Get bonusCurrencyId list by bonus id and currency id
//	 * @return
//	 * @throws SQLException
//	 */
//	public static ArrayList<BonusUsersStep> getBonusSteps(Connection con, long bonusId, long currencyId) throws SQLException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		ArrayList<BonusUsersStep> list =  new ArrayList<BonusUsersStep>();
//
//		try	{
//		    String sql = "SELECT * " +
//		    			 "FROM " +
//		    			 	"bonus_currency " +
//		    			 "WHERE " +
//		    			 	"bonus_id = ? AND " +
//		    			 	"currency_id = ? " +
//		    			 " ORDER BY step ";
//
//			ps = con.prepareStatement(sql);
//			ps.setLong(1, bonusId);
//			ps.setLong(2, currencyId);
//			rs = ps.executeQuery();
//
//			while ( rs.next() ) {
//				BonusUsersStep temp = new BonusUsersStep();
//		    	temp.setBonusPercent(rs.getDouble("bonus_percent"));
//		    	temp.setMinDepositAmount(rs.getLong("min_deposit_amount"));
//		    	temp.setMaxDepositAmount(rs.getLong("max_deposit_amount"));
//				list.add(temp);
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return list;
//	}

    /**
     * Fill BonusCurrency
     *
     * @return BonusCurrency object
     * @throws SQLException
     */
    protected static BonusCurrency getBCVO(ResultSet rs) throws SQLException {
        BonusCurrency vo = new BonusCurrency();
        vo.setId(rs.getLong("ID"));
        vo.setBonusId(rs.getLong("BONUS_ID"));
        vo.setCurrencyId(rs.getLong("CURRENCY_ID"));
        vo.setBonusAmount(rs.getLong("BONUS_AMOUNT"));
        vo.setBonusPercent(rs.getDouble("BONUS_PERCENT"));
        vo.setMinDepositAmount(rs.getLong("MIN_DEPOSIT_AMOUNT"));
        vo.setMaxDepositAmount(rs.getLong("MAX_DEPOSIT_AMOUNT"));
        vo.setSumInvestQualify(rs.getLong("SUM_INVEST_QUALIFY"));
        vo.setWriterId(rs.getLong("WRITER_ID"));
        vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
        vo.setSumInvestWithdraw(rs.getLong("SUM_INVEST_WITHDRAW"));
        vo.setStep(rs.getLong("STEP"));
        vo.setMinInvestAmount(rs.getLong("min_invest_amount"));
        vo.setMaxInvestAmount(rs.getLong("max_invest_amount"));
        return vo;
    }

	/**
	 * insert bonus to user
	 * @param con db connection
	 * @throws SQLException
	 */
	public static boolean insertBonusUserSteps(Connection con, ArrayList<BonusUsersStep> bonusSteps, long bonusUserId) throws SQLException {
		PreparedStatement ps = null;

		try {
			for (int index=0; index < bonusSteps.size(); index++){

				String sql="INSERT " +
							"INTO bonus_users_steps(ID, " +
													"BONUS_PERCENT," +
													"MIN_DEPOSIT_AMOUNT," +
													"MAX_DEPOSIT_AMOUNT," +
													"BONUS_USER_ID) " +
							"VALUES(SEQ_BONUS_USERS_STEPS.nextval,?,?,?,?)";

				ps = con.prepareStatement(sql);
				ps.setDouble(1, bonusSteps.get(index).getBonusPercent());
				ps.setLong(2, bonusSteps.get(index).getMinDepositAmount());
				ps.setLong(3, bonusSteps.get(index).getMaxDepositAmount());
				ps.setLong(4, bonusUserId);
				ps.executeUpdate();
			}
		}
		finally {
			closeStatement(ps);
		}
		return true;
	}


    /**
     * Get bonus currency by bonus id and currency id
     *
     * @param conn
     * @param bonusId
     * @param currencyId
     * @return <code>BonusCurrency</code>
     * @throws SQLException
     */
    public static BonusCurrency getBonusCurrency(Connection conn, long bonusId, long currencyId) throws SQLException {
        BonusCurrency bc = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "bonus_currency " +
                "WHERE " +
                    "bonus_id = ? AND " +
                    "currency_id = ? AND " +
                    "rownum = 1 " +
                "ORDER BY " +
                    "step";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, bonusId);
            pstmt.setLong(2, currencyId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                bc = getBCVO(rs);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return bc;
    }

    public static ArrayList<BonusUsers> getForActivationByTransaction(Connection conn, long userId) throws SQLException {
        ArrayList<BonusUsers> list = new ArrayList<BonusUsers>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try{
            String sql =
                    "SELECT " +
                        "* " +
                    "FROM " +
                        "bonus_users " +
                    "WHERE " +
                        "user_id = ? AND " +
                        "bonus_state_id = " + Bonus.STATE_GRANTED + " AND " +
                        "(start_date IS NULL OR start_date <= current_date) AND " +
                        "(end_date IS NULL OR end_date >= current_date) AND " +
                        "type_id IN (" + Bonus.TYPE_AMOUNT_AFTER_DEPOSIT + ", " + Bonus.TYPE_PERCENT_AFTER_DEPOSIT + ") " +
                    "ORDER BY " +
                        "time_created";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                list.add(getBonusUsersVO(rs, conn));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }

    /**
     * Get bonus users steps
     *
     * @param conn
     * @param bonusUserId
     * @return <code>ArrayList<BonusUsersStep></code>
     * @throws SQLException
     */
    public static ArrayList<BonusUsersStep> getBonusUsersSteps(Connection conn, long bonusUserId) throws SQLException {
        ArrayList<BonusUsersStep> list = new ArrayList<BonusUsersStep>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "bonus_users_steps bus " +
                "WHERE " +
                    "bus.bonus_user_id = ? " +
                "ORDER BY " +
                    "bonus_percent";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, bonusUserId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                BonusUsersStep temp = new BonusUsersStep();
                temp.setId(rs.getLong("id"));
                temp.setBonusPercent(rs.getDouble("bonus_percent"));
                temp.setMinDepositAmount(rs.getLong("min_deposit_amount"));
                temp.setMaxDepositAmount(rs.getLong("max_deposit_amount"));
                temp.setBonusUserId(rs.getLong("bonus_user_Id"));
                list.add(temp);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }

//    public static ArrayList<BonusUsers> getBonusesForActivation(Connection conn, long userId) throws SQLException {
//        ArrayList<BonusUsers> list = new ArrayList<BonusUsers>();
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        try{
//            String sql =
//                    " SELECT " +
//                        " * " +
//                    " FROM " +
//                        " bonus_users bu," +
//                        " bonus_types bt " +
//                    " WHERE " +
//                        " bu.user_id = ? " +
//                        " AND bu.bonus_state_id = " + Bonus.STATE_GRANTED + " " +
//                        " AND (bu.start_date IS NULL OR bu.start_date <= current_date) " +
//                        " AND(bu.end_date IS NULL OR bu.end_date >= current_date) " +
//                        " AND bu.type_id = bt.id " +
//                    " ORDER BY " +
//                        " time_created ";
//            pstmt = conn.prepareStatement(sql);
//            pstmt.setLong(1, userId);
//            rs = pstmt.executeQuery();
//            while (rs.next()) {
//            	BonusUsers bu = getBonusUsersVO(rs, conn);
//            	bu.setBonusClassType(rs.getLong("class_type_id"));
//                list.add(bu);
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(pstmt);
//        }
//        return list;
//    }

//    /**
//     * Load all bonus population limits
//     * @param con
//     * @return
//     * @throws SQLException
//     */
//    public static ArrayList<BonusPopulationLimit> getBonusPopulationLimits(Connection con, long popEntryId, long bonusId, long currencyId) throws SQLException {
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ArrayList<BonusPopulationLimit> list = new ArrayList<BonusPopulationLimit>();
//        try	{
//            String sql =
//            	" SELECT " +
//            		" bp.bonus_id, " +
//            		" bp.population_type_id, " +
//            		" bp.update_type, " +
//            		" bpl.* " +
//            	" FROM " +
//            		" bonus_populations bp, " +
//            		" bonus_population_limits bpl," +
//            		" populations p," +
//            		" population_entries pe " +
//            	" WHERE " +
//            		" bpl.bonus_population_id = bp.id " +
//            		" AND bp.population_type_id = p.population_type_id " +
//            		" AND p.id = pe.population_id " +
//            		" AND pe.id = " + popEntryId + " " +
//            		" AND bp.bonus_id = " + bonusId + " " +
//            		" AND bpl.currency_id = " + currencyId + " " +
//            	" ORDER BY " +
//            		" bpl.bonus_population_id, " +
//            		" bpl.currency_id, " +
//            		" bpl.level_no ";
//
//            ps = con.prepareStatement(sql);
//            rs = ps.executeQuery();
//            while (rs.next()) {
//            	BonusPopulationLimit bpl = new BonusPopulationLimit();
//            	bpl.setId(rs.getLong("id"));
//            	bpl.setBonusId(rs.getLong("bonus_id"));
//            	bpl.setPopTypeId(rs.getLong("population_type_id"));
//            	bpl.setCurrencyId(rs.getLong("currency_id"));
//            	bpl.setMinDepositParam(rs.getLong("min_deposit_param"));
//            	bpl.setMaxDepositParam(rs.getLong("max_deposit_param"));
//            	bpl.setMinResult(rs.getLong("min_result"));
//            	bpl.setMultiplication(rs.getLong("multiplication"));
//            	bpl.setLevel(rs.getLong("level_no"));
//            	bpl.setStepRangeMul(rs.getLong("STEP_RANGE_MUL"));
//            	bpl.setStepLevelMul(rs.getLong("STEP_LEVEL_MUL"));
//            	bpl.setStepBonusAddition(rs.getDouble("STEP_BONUS_ADDITION"));
//            	bpl.setMinBonusPercent(rs.getDouble("MIN_BONUS_PERCENT"));
//            	bpl.setLimitUpdateType(rs.getInt("UPDATE_TYPE"));
//            	bpl.setBonusAmount(rs.getLong("BONUS_AMOUNT"));
//            	bpl.setMinInvestmentAmount(rs.getLong("MIN_INVESTMENT_AMOUNT"));
//            	bpl.setMaxInvestmentAmount(rs.getLong("MAX_INVESTMENT_AMOUNT"));
//                list.add(bpl);
//            }
//        } finally {
//            closeStatement(ps);
//            closeResultSet(rs);
//        }
//        return list;
//    }

	public static String getDescriptionByTypeAndState (Connection con,long bonusId,long stateId) throws SQLException{
    	String description = "";
    	PreparedStatement stmt = null;
    	ResultSet rs = null;
    	try{
    		String sql =
    			" SELECT "+
    				" message "+
    			" FROM " +
    				" bonus_type_state_msg btsm, " +
    				" bonus b "+
    			" WHERE "+
    				" type = b.type_id " +
    				" and state = ? " +
    				" and b.id = ? ";
    		stmt = con.prepareStatement(sql);
    		stmt.setLong(1, stateId);
    		stmt.setLong(2, bonusId);
    		rs = stmt.executeQuery();
    		if(rs.next()){
    			description = rs.getString("message");
    		}
    	} finally{
    		closeResultSet(rs);
    		closeStatement(stmt);
    	}
    	return description;
    }

//	   /**
//     * Update state id of the bonus
//     * @param con db connection
//     * @param id bonus users id
//     * @param stateId bonus state id for update
//     * @throws SQLException
//     */
//    public static void updateState(Connection con, long id, long stateId, long writerIdCancel) throws SQLException {
//
//        PreparedStatement ps = null;
//        try {
//            String sql = " UPDATE " +
//            				" bonus_users " +
//            		     " SET " +
//            		     	" bonus_state_id = ? ";
//
//            if (Bonus.STATE_CANCELED == stateId){
//            	sql += 		" ,TIME_CANCELED = sysdate ";
//            }else if (Bonus.STATE_REFUSED == stateId){
//            	sql += 		" ,TIME_REFUSED = sysdate ";
//            } else if (ConstantsBase.BONUS_STATE_WITHDRAWN == stateId) { //TODO need to be refactor - contain time status change in other table. 
//            	sql += 		" ,TIME_WITHDRAWN = sysdate ";
//            } else if (ConstantsBase.BONUS_STATE_WAGERING_WAIVED == stateId) {
//            	sql += 		" ,TIME_WAGERING_WAIVED = sysdate ";
//            }
//            
//            if (stateId == ConstantsBase.BONUS_STATE_CANCELED || 
//            		stateId == ConstantsBase.BONUS_STATE_WITHDRAWN || 
//            		stateId == ConstantsBase.BONUS_STATE_WAGERING_WAIVED) {
//            	sql += 		" ,WRITER_ID_CANCEL = " + writerIdCancel + " ";
//            }
//            
//            sql +=
//                        " WHERE " +
//                            " id = ?";
//
//            ps = con.prepareStatement(sql);
//
//            ps.setLong(1, stateId);
//            ps.setLong(2, id);
//            ps.executeUpdate();
//
//        } finally {
//            closeStatement(ps);
//        }
//    }

    public static void addBonusUsersSumDeposits(Connection conn, long bonusUsersId, long amount) throws SQLException {
        PreparedStatement pstmt = null;
        try{
            String sql =
                    "UPDATE " +
                        "bonus_users " +
                    "SET " +
                    	"sum_deposits_reached = sum_deposits_reached + ? " +
                    "WHERE " +
                        "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, amount);
            pstmt.setLong(2, bonusUsersId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }


    public static long hasNextInvestOnUs(Connection conn, long userId) throws SQLException {
        long id = 0;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "bu.id " +
                    "FROM " +
                        "bonus_users bu, " +
                        "bonus_types bt " +
                    "WHERE " +
                        "bu.user_id = ? AND " +
                        "bt.class_type_id = " + ConstantsBase.BONUS_CLASS_TYPE_NEXT_INVEST_ON_US + " AND " +
                        "bu.bonus_state_id = " + ConstantsBase.BONUS_STATE_ACTIVE + " AND " +
                        "bt.id = bu.type_id AND "+
                        "(bu.start_date IS NULL OR bu.start_date <= current_date) AND " +
                        "(bu.end_date IS NULL OR bu.end_date >= current_date) " +
                    "ORDER BY " +
                        "bu.id";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                id = rs.getLong("id");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return id;
    }

    /**
     * get all user bonus by user id
     * @param userId
     * @return list of bonus
     * @throws SQLException
     */
    public static ArrayList<BonusUsers> getAllUserBonus(Connection con, String utcOffset, long userId,
    		Date from, Date to, long state, Integer startRow, Integer pageSize, int showOnlyNotSeen) throws SQLException {
    	ArrayList<BonusUsers> list = new ArrayList<BonusUsers>();
    	PreparedStatement pstmt = null;
		ResultSet rs = null;
		String stateCondition = "<= " + ConstantsBase.BONUS_STATE_PENDING  + " AND ";
		if (state > 0 && state != 3 && state != 4) {
			stateCondition = "= " + state + " AND ";
		} else if (state == 3) {
			stateCondition = "IN (" + ConstantsBase.BONUS_STATE_USED + ", " + ConstantsBase.BONUS_STATE_DONE + ", " + ConstantsBase.BONUS_STATE_WITHDRAWN + ", " + ConstantsBase.BONUS_STATE_WAGERING_WAIVED + ") AND ";
		} else if (state == 4) {
			stateCondition = "IN (" + ConstantsBase.BONUS_STATE_GRANTED + ", " + ConstantsBase.BONUS_STATE_ACTIVE + ") AND ";
		}
		try{
		    String sql = "SELECT " +
							"* " +
						 "FROM " +
				   			 "(SELECT  " +
				   			 	"X.*, " +
				   			 	"rownum AS rownumIn " +
			   				 "FROM " +
				    			"(SELECT " +
					            	"bu.*, " +
					            	"btsm.message, " +
					            	"u.currency_id, " +
					            	"bt.class_type_id, " +
					            	"i.id as invId " +
					            "FROM " +
					            	"bonus_users bu, " +
					            	"bonus_type_state_msg btsm, " +
					            	"users u, " +
					            	"bonus_types bt, " +
					            	"investments i " +
					            "WHERE " +
					            	"btsm.state = bu.bonus_state_id AND " +
					            	"btsm.type = bu.type_id AND " +
					            	"bu.bonus_state_id " + stateCondition +
					            	"u.id = bu.user_id AND " +
					            	"bt.id = bu.type_id AND " +
					            	"bu.id = i.bonus_user_id(+) AND " +
					        		"bu.user_id = ? AND " +
					        		"bt.id <> ? AND " + 
					        		"bu.bonus_id <> ? ";
					    if (null != from && null != to) {
					    	 sql += "AND bu.start_date >= ? "+
							 		"AND bu.start_date <= ? ";
					    }
					    //to show a list for moblie with all bonuses as they are showOnlyNotSeen == 0, or only the not seen showOnlyNotSeen == 1
					    if(showOnlyNotSeen == 1) {
						sql += "AND bu.IS_BONUS_SEEN = 0 ";
					    }
					    // order by unclaimed aka pending then date
					    sql += "ORDER BY " +
							    	"case when bu.bonus_state_id  = '10' then -1 end, " +
							    	"bu.start_date DESC)X ) ";

					    if (null != startRow && null != pageSize) {
					    	sql += "WHERE rownumIn > ? " +
			   						"AND rownumIn <= ? ";
					    }
		    pstmt = con.prepareStatement(sql);
		    int idx = 1;
		    pstmt.setLong(idx++, userId);
		    pstmt.setLong(idx++, Bonus.COPYOP_COINS_CONVERT);
		    pstmt.setLong(idx++, ConstantsBase.BONUS_TYPE_CONVERT_POINTS_TO_CASH);
		    
		    if (null != from && null != to) {
		    	pstmt.setTimestamp(idx++, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, utcOffset)));
		    	pstmt.setTimestamp(idx++, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to), utcOffset)));
		    }
		    
		    if (null != startRow && null != pageSize) {
		    	pstmt.setInt(idx++, startRow);
		    	pstmt.setInt(idx++, (startRow + pageSize));
		    }

		    rs = pstmt.executeQuery();
		    while (rs.next()) {
		    	BonusUsers temp = getBonusUsersVO(rs, con);
		    	temp.setCurrency(CurrenciesManagerBase.getCurrency(rs.getInt("currency_id")));
		    	temp.setBonusStateDescription(rs.getString("message"));
		    	temp.setBonusClassType(rs.getLong("class_type_id"));
		    	temp.setInvestmentId(rs.getLong("invId"));
		    	temp.setStateExpired();

		    	BonusHandlerBase bh = BonusHandlerFactory.getInstance(temp.getTypeId());

		    	if (null != bh){
		    		try {
						bh.setBonusStateDescription(temp);
					} catch (BonusHandlersException e) {
						log.error("Error in setBonusStateDescription for bonus user id " + temp.getId(),e);
					}
		    	}

		    	list.add(temp);
		    }
		} finally {
		   closeStatement(pstmt);
		   closeResultSet(rs);
		}
		return list;
	}

    /**
     * Check if user can withdraw.
     * User cannot withdraw in case:
     * 1. have bonuses with used state.
     * 2. have bonuses with active state and amountForWithdraw > (balance - sumActiveAmount)
     *
     * @param con
     * @param userId
     * @param balance
     * @param amount
     * @return true only if user don't have any bonus in state used else false
     * @throws SQLException
     */
    public static boolean userCanWithdraw(Connection con, long userId, long balance, long amount) throws SQLException {
        boolean canWithdraw = true;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        long sumWageringAmount = 0;
        try {
            String sql =
                    " SELECT " +
                        " bu.*," +
                        " bt.class_type_id, " +
                        " i.win, " +
                        " i.lose, " +
                        " i.amount inv_amount, " +
                        " i.is_settled " +
                    " FROM " +
                        " bonus_types bt, " +
                        " bonus_users bu " +
                            " left join investments i on i.bonus_user_id = bu.id " +
                    " WHERE " +
                        " bu.type_id = bt.id " +
                        " AND bu.user_id = ? ";
            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                BonusUsers bu = getBonusUsersVO(rs, con);
                if (ConstantsBase.BONUS_CLASS_TYPE_NEXT_INVEST_ON_US == rs.getLong("class_type_id")) {
                    if (bu.getBonusStateId() == ConstantsBase.BONUS_STATE_USED && rs.getInt("IS_SETTLED") == 1) {
                        BonusHandlerBase bh = BonusHandlerFactory.getInstance(bu.getTypeId());
                        long win = rs.getLong("win");
                        long lose = rs.getLong("lose");
                        long invAmount = rs.getLong("inv_amount");
                        sumWageringAmount += bh.getAmountThatUserCantWithdraw(win, lose, invAmount, bu);
                    }
                } else {
                    if (bu.getBonusStateId() == ConstantsBase.BONUS_STATE_USED) {
                        canWithdraw = false;
                        break;
                    } else if (bu.getBonusStateId() == ConstantsBase.BONUS_STATE_ACTIVE) {
                        sumWageringAmount += bu.getBonusAmount();
                    }
                }
            }

            if (canWithdraw && sumWageringAmount > 0) { // have active bonuses and don't have used bonuses
                if (amount > (balance - sumWageringAmount)) {
                    canWithdraw = false;
                }
            }
        } finally {
           closeStatement(pstmt);
           closeResultSet(rs);
        }
        return canWithdraw;
    }
    
//	/**
//	 * Insert Statement to bonus_investments table
//	 * @param conn
//	 * @param investmentId
//	 * @param bonusUsersId
//	 * @param bonusAmount
//	 * @param adjustedAmountHistory
//	 * @param typeId
//	 * @throws SQLException
//	 */
//	public static void insertBonusInvestments(Connection conn, long investmentId, long bonusUsersId, long bonusAmount, long adjustedAmountHistory, int typeId) throws SQLException {
//		PreparedStatement ps = null;
//		int ind = 1;
//		try {
//			String sql =
//						"INSERT INTO bonus_investments " +
//						"	( " +
//						"		id, " +
//						"		investments_id, " +
//						"		bonus_users_id, " +
//						"		bonus_amount, " +
//						"		time_created, " +
//						"		adjusted_amount, " +
//						"		type_id " +
//						" 	) " +
//						"VALUES	" +
//						"	( " +
//						"		SEQ_BONUS_INVESTMENTS.nextval, " +
//						"		?, " +
//						"		?, " +
//						"		?, " +
//						"		sysdate, " +
//						"		?, " +
//						"		? " +
//						"	) ";
//
//			ps = conn.prepareStatement(sql);
//			ps.setLong(ind++, investmentId);
//			ps.setLong(ind++, bonusUsersId);
//			ps.setLong(ind++, bonusAmount);
//			ps.setLong(ind++, adjustedAmountHistory);
//			ps.setLong(ind++, typeId);
//			ps.executeUpdate();
//			log.debug("\nBMS, insertBonusInvestments, investmentId:" + investmentId + ", bonusUsersId:" + bonusUsersId + ", bonusAmount:" + bonusAmount +
//					", adjustedAmount:" + adjustedAmountHistory + ", typeId:" + typeId);
//		} finally {
//			closeStatement(ps);
//		}
//	}
	
//    /**
//     * Update adjusted_amount attribute by the given bonus_users id.
//     * @param conn
//     * @param id - of bonus_users table
//     * @param amount - to be insert to adjusted amount attribute
//     * @throws SQLException
//     */
//    public static void updateAdjustedAmount(Connection conn, long id, long amount) throws SQLException {
//        PreparedStatement ps = null;
//        int ind = 1;
//        try {
//            String sql = 
//            			"UPDATE " +
//            			"	bonus_users " +
//            		    "SET " +
//            		    "	adjusted_amount = ? " +
//                        "WHERE " +
//                        "	id = ?";
//            ps = conn.prepareStatement(sql);
//            ps.setLong(ind++, amount);
//            ps.setLong(ind++, id);
//            ps.executeUpdate();
//            log.debug("\nBMS, updateAdjustedAmount in bonus_users table, adjusted_amount:" + amount + " where id: " + id);
//        } finally {
//            closeStatement(ps);
//        }
//    }	
	
    
    /**
     * Use Select statement to retrieve total bonus amount in a given investment id.
     * @param conn
     * @param investmentId
     * @return sum of Bonus Amount where investmentId in bonus_investment table.
     * @throws SQLException
     */
    public static long getBonusInvestmentsAmount(Connection conn, long investmentId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        long bonusAmount = -1;
        try {
            String sql =
		            	"SELECT " +
		            	"	SUM(bi.bonus_amount) as totalBonusAmount " +
		            	"FROM " +
		            	"	bonus_investment bi " +
		            	"WHERE " +
	            		"	investments_id = ? ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, investmentId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                bonusAmount = rs.getLong("totalBonusAmount");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return bonusAmount;
    }
    
//    /**
//     * Get bonus amount when there was insert investment by given investmentId and type.
//    /**
//     * @param conn
//     * @param investmentId
//     * @param isSettled
//     * @return
//     * @throws SQLException
//     */
//    public static ArrayList<BonusFormulaDetails> getBonusAmounts(Connection conn, long investmentId, boolean isSettled) throws SQLException {
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        ArrayList<BonusFormulaDetails> bonusFormulaD = new ArrayList<BonusFormulaDetails>();
//        try {
//            String sql =
//		            	"SELECT " +
//		            	"	bu.id as bonus_users_id, " +
//		            	"	bi.bonus_amount, " +
//		            	"	bu.adjusted_amount " +
//		            	"FROM " +
//		            	"	bonus_investments bi, " +
//		            	" 	bonus_users bu " +
//		            	"WHERE " +
//	            		"	bu.id = bi.bonus_users_id AND " +
//	            		"	bi.investments_id = ? AND " +
//	            		"	bi.type_id = ? ";
//            pstmt = conn.prepareStatement(sql);
//            pstmt.setLong(1, investmentId);
//            pstmt.setLong(2, isSettled == true ? ConstantsBase.BONUS_INVESTMENTS_TYPE_SETTLED_INV : ConstantsBase.BONUS_INVESTMENTS_TYPE_INSERT_INV);
//            rs = pstmt.executeQuery();
//            while (rs.next()) {
//            	BonusFormulaDetails bfd = new BonusFormulaDetails();
//            	bfd.setAdjustedAmount(rs.getLong("adjusted_amount"));
//            	bfd.setAmount(rs.getLong("bonus_amount"));
//            	bfd.setBonusUsersId(rs.getLong("bonus_users_id"));
//            	bonusFormulaD.add(bfd);
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(pstmt);
//        }
//        return bonusFormulaD;
//    }

    public static BonusUsers getBonusUserByBonusId(Connection con, long userId, long bonusId) throws SQLException {
    	BonusUsers bu = null;
    	PreparedStatement pstmt = null;
		ResultSet rs = null;
    	try{
		    String sql =
		            "SELECT " +
		            	"bu.* " +
		            "FROM " +
		            	"bonus_users bu " +
		            "WHERE " +
		        		"bu.user_id = ? AND " +
		            	"bu.id = ? "; // here take id in bonus_users table because we have it in request but don't have bonus_id value

		    pstmt = con.prepareStatement(sql);
		    pstmt.setLong(1, userId);
		    pstmt.setLong(2, bonusId);
		    rs = pstmt.executeQuery();

		    if (rs.next()) {
		    	bu = new BonusUsers();
		    	bu = getBonusUsersVO(rs, con);
		    }
    	} finally {
		   closeStatement(pstmt);
		   closeResultSet(rs);
		}
		return bu;
    }
    
    public static BonusUsers getLastGrantedActiveUserBonus(Connection con, long userId) throws SQLException {
    	PreparedStatement pstmt = null;
		ResultSet rs = null;
		BonusUsers bonusUser = null;		
		try{
		    String sql =
				    			"SELECT " +
					            	"bu.*, " +
					            	"btsm.message, " +
					            	"u.currency_id, " +
					            	"bt.class_type_id, " +
					            	"i.id as invId " +
					            "FROM " +
					            	"bonus_users bu, " +
					            	"bonus_type_state_msg btsm, " +
					            	"users u, " +
					            	"bonus_types bt, " +
					            	"investments i " +
					            "WHERE " +
					            	"btsm.state = bu.bonus_state_id AND " +
					            	"btsm.type = bu.type_id AND " +
					            	"bu.bonus_state_id IN (" + ConstantsBase.BONUS_STATE_GRANTED + ", " + ConstantsBase.BONUS_STATE_ACTIVE + ") AND " +
					            	"u.id = bu.user_id AND " +
					            	"bt.id = bu.type_id AND " +
					            	"bu.id = i.bonus_user_id(+) AND " +
					        		"bu.user_id = ? AND " +
					        		"bt.id <> ? " +
		    					"ORDER BY " +
							    	"bu.start_date desc";
		    pstmt = con.prepareStatement(sql);
		    pstmt.setLong(1, userId);
		    pstmt.setLong(2, ConstantsBase.BONUS_TYPE_CONVERT_POINTS_TO_CASH);
		    rs = pstmt.executeQuery();
		    if (rs.next()) {
		    	bonusUser = getBonusUsersVO(rs, con);
		    	bonusUser.setCurrency(CurrenciesManagerBase.getCurrency(rs.getInt("currency_id")));
		    	bonusUser.setBonusStateDescription(rs.getString("message"));
		    	bonusUser.setBonusClassType(rs.getLong("class_type_id"));
		    	bonusUser.setInvestmentId(rs.getLong("invId"));
		    	bonusUser.setStateExpired();

		    	BonusHandlerBase bh = BonusHandlerFactory.getInstance(bonusUser.getTypeId());

		    	if (null != bh){
		    		try {
						bh.setBonusStateDescription(bonusUser);
					} catch (BonusHandlersException e) {
						log.error("Error in setBonusStateDescription for bonus user id " + bonusUser.getId(),e);
					}
		    	}
		    }
		} finally {
		   closeStatement(pstmt);
		   closeResultSet(rs);
		}
		return bonusUser;
	}
}