package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import com.anyoption.common.daos.DAOBase;

public class ConfigDAO extends DAOBase{

	public static HashMap<Long, Long> getPriorityOfProvider(Connection conn) throws SQLException{
		String priorityKey = "transaction_reroute_provider_priority_";
		HashMap<Long, Long> hm =  new HashMap<Long, Long>(); 
				
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = " SELECT c.* " +
					 " FROM config c " +
					 " WHERE c.key like '" + priorityKey + "%' ";
		try{ 
			ps = conn.prepareStatement(sql);
			rs = ps .executeQuery();
			while (rs.next()){
				
				String priorityKeyFromDB = rs.getString("key");
				Long priority = Long.parseLong(priorityKeyFromDB.substring(priorityKeyFromDB.length()-1));
				Long clearingProviderIdGroup = rs.getLong("value");
				hm.put( priority, clearingProviderIdGroup);
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}		
		return hm;
	}

	public static long getDaysNumForFailures(Connection conn) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		long value = 0;
		String sql = " select c.value " +
					 " from config c " +
					 " where c.key like 'transaction_reroute_x_days' ";
		try{
			ps = conn.prepareStatement(sql);
		
			rs = ps.executeQuery();
			if(rs.next()){
				value = rs.getLong("value");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return value;
	}

	public static long getFailuresNumConsecutive(Connection conn) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		long value = 0;
		String sql = " select c.value " +
					 " from config c " +
					 " where c.key like 'transaction_reroute_y_failures' ";
		try{
			ps = conn.prepareStatement(sql);
		
			rs = ps.executeQuery();
			if(rs.next()){
				value = rs.getLong("value");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return value;
	}
}
