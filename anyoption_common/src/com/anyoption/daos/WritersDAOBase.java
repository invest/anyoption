package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.beans.Writer;

public class WritersDAOBase extends com.anyoption.common.daos.WritersDAOBase {
    public static Writer get(Connection conn, long id) throws SQLException {
        Writer vo = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "writers " +
                "WHERE " +
                    "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                vo = getVO(rs);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return vo;
    }

    private static Writer getVO(ResultSet rs) throws SQLException {
        Writer vo = new Writer();
        vo.setId(rs.getLong("id"));
        vo.setUserName(rs.getString("USER_NAME"));
        vo.setPassword(rs.getString("PASSWORD"));
        vo.setFirstName(rs.getString("FIRST_NAME"));
        vo.setLastName(rs.getString("LAST_NAME"));
        vo.setStreet(rs.getString("STREET"));
        vo.setStreetNo(rs.getString("STREET_no"));
        vo.setCityId(rs.getString("CITY_ID"));
        vo.setZipCode(rs.getString("ZIP_CODE"));
        vo.setEmail(rs.getString("EMAIL"));
        vo.setComments(rs.getString("COMMENTS"));
        vo.setTimeBirthDate(convertToDate(rs.getTimestamp("TIME_BIRTH_DATE")));
        vo.setMobilePhone(rs.getString("MOBILE_PHONE"));
        vo.setLandLinePhone(rs.getString("LAND_LINE_PHONE"));
        vo.setIsActive(rs.getLong("IS_ACTIVE"));
        vo.setSupportEnable(rs.getBoolean("IS_SUPPORT_ENABLE"));
        vo.setRoleId(rs.getLong("role_id"));
        return vo;
    }
}