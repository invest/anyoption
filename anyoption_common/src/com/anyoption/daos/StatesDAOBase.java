//package com.anyoption.daos;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.ArrayList;
//
//import com.anyoption.beans.State;
//import com.anyoption.common.daos.DAOBase;
//
//public class StatesDAOBase extends DAOBase {
//
//	public static ArrayList<State> getAll(Connection con) throws SQLException{
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		ArrayList<State> list = new ArrayList<State>();
//		try {
//			String sql =
//				"SELECT " +
//					"* " +
//				"FROM " +
//					"states " +
//				"ORDER BY " +
//					"state_name";
//			ps = con.prepareStatement(sql);
//			rs = ps.executeQuery();
//			while(rs.next()){
//				list.add(getVO(rs));
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return list;
//	}
//
//    /**
//     * Fill Country object
//     *
//     * @param rs ResultSet
//     * @return <code>Country</code> object
//     * @throws SQLException
//     */
//    protected static State getVO(ResultSet rs) throws SQLException {
//        State vo = new State();
//        vo.setId(rs.getLong("id"));
//        vo.setName(rs.getString("state_name"));
//       return vo;
//    }
//}