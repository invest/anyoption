//package com.anyoption.daos;
//
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//import oracle.jdbc.OraclePreparedStatement;
//
//import com.anyoption.common.beans.base.BaroPayRequest;
//import com.anyoption.common.beans.base.BaroPayResponse;
//import com.anyoption.common.beans.base.BaroPayWithdraw;
//import com.anyoption.common.daos.DAOBase;
//import com.anyoption.managers.TransactionsManagerBase;
//
///**
// * @author Eyal O 
// *
// */
//public class BaroPayDAOBase extends DAOBase {
//
//    
//     /**
//      * Insert into BaroPay initial response  
//       * @param con
//      * @param vo
//      * @throws SQLException
//      */
//      public static void insertBaroPayResponse(Connection con, BaroPayResponse vo) throws SQLException { 
//         PreparedStatement ps = null;
//         ResultSet rs = null;
//         try {
//            String sql =" INSERT INTO baropay_response (id, transaction_id, status, payment_id, description, xml_response) " +
//                                " VALUES " +
//                                " (seq_baropay_response.nextval,?,?,?,?,?) ";
//            
//            ps = (OraclePreparedStatement) con.prepareStatement(sql);
//            ps.setLong(1, vo.getTransactionId());
//            ps.setInt(2, vo.getStatus());
//            ps.setLong(3, vo.getPaymentId());
//            ps.setString(4, vo.getDescription());
//            ps.setString(5, vo.getXmlResponse());
//            ps.executeUpdate();
//        } finally {
//            closeStatement(ps);
//            closeResultSet(rs);
//        }
//      }
//      
//  	/**
//       * Insert into BaroPay request for deposit/withdraw
//       * @param con
//       * @param vo
//       * @throws SQLException
//       */
//       public static void insertBaroPayRequest(Connection con, BaroPayRequest vo) throws SQLException { 
//          PreparedStatement ps = null; 
//          ResultSet rs = null;
//          try {
//             String sql =" INSERT INTO baropay_request (id, transaction_id, phone, sender, bank_name, account_owner, account_number) " +
//                                 " VALUES " +
//                                 " (seq_baropay_request.nextval,?,?,?,?,?,?) ";                   
//             ps = (OraclePreparedStatement) con.prepareStatement(sql);
//             ps.setLong(1, vo.getTransactionId());
//             ps.setString(2, vo.getPhone());
//             ps.setString(3, vo.getSender());
//             ps.setString(4, vo.getBankName() );
//             ps.setString(5, vo.getAccountOwner());
//             ps.setString(6, vo.getAccountNumber());
//             ps.executeUpdate();                 
//         } finally {
//             closeStatement(ps);
//             closeResultSet(rs);
//         }
//       }      
//      
//       
// 	/**
// 	 * Get BaroPay Request details
// 	 * @param con
// 	 * @param id
// 	 * @return
// 	 * @throws SQLException
// 	 */
// 	public static BaroPayRequest getBaroPayRequest(Connection con, long transactionId) throws SQLException {
// 		PreparedStatement ps = null;
// 		ResultSet rs = null;
// 		BaroPayRequest baroPayRequest = null;
// 		try {
// 			String sql =" SELECT " +
// 							" * " +
// 						" FROM " +
//  			   				" baropay_request bpr  " +
//  			   			" WHERE " +
//  			   				" bpr.transaction_id = ? ";
//			ps = con.prepareStatement(sql);
//			ps.setLong(1, transactionId);
//			rs = ps.executeQuery();
//			if (rs.next()) {
//				baroPayRequest = new BaroPayRequest();
//				baroPayRequest.setId(rs.getLong("id"));
//				baroPayRequest.setPhone(rs.getString("phone"));
//				baroPayRequest.setSender(rs.getString("sender"));
//				baroPayRequest.setBankName(rs.getString("bank_name"));
//				baroPayRequest.setAccountOwner(rs.getString("account_owner"));
//				baroPayRequest.setAccountNumber(rs.getString("account_number"));
//			}
//		} finally {
//		 closeResultSet(rs);
//		 closeStatement(ps);
//		}
// 		return baroPayRequest;
// 	}       
//       
//       
//  	/**
//  	 * Get BaroPay Response deposit/withdraw  details
//  	 * @param con
//  	 * @param id
//  	 * @return
//  	 * @throws SQLException
//  	 */
//  	public static BaroPayResponse getBaroPayResponse(Connection con, long transactionId) throws SQLException {
//  		  PreparedStatement ps = null;
//  		  ResultSet rs = null;
//  		  BaroPayResponse baroPayResponse = null;
//  		  try {
//  			  String sql = " SELECT " +
//  			  			   		" * " +
//  			  			   " FROM " +
//  			  			   		" baropay_response bpr  " +
//  			  			   " WHERE " +
//  			  			   		" bpr.transaction_id = ? ";
//  			  ps = con.prepareStatement(sql);
//  			  ps.setLong(1, transactionId);
//  			  rs = ps.executeQuery();
//  			  if (rs.next()) {
//  				  baroPayResponse = new BaroPayResponse();
//  				  baroPayResponse.setId(rs.getLong("id"));
//  				  baroPayResponse.setPaymentId(rs.getLong("PAYMENT_ID"));
//  				  baroPayResponse.setStatus(rs.getInt("STATUS"));
//  				  baroPayResponse.setDescription(rs.getString("DESCRIPTION"));
//  			  }
//  		  } finally {
//  			  closeResultSet(rs);
//  			  closeStatement(ps);
//  		  }
//  		  return baroPayResponse;
//  	  }
//  	
//  	/**
//  	 * Get BaroPay withdraw  details
//  	 * @param con
//  	 * @param id
//  	 * @return
//  	 * @throws SQLException
//  	 */
//  	public static BaroPayWithdraw getBaroPayWithdraw(Connection con, long transactionId) throws SQLException {
//  		  PreparedStatement ps = null;
//  		  ResultSet rs = null;
//  		  BaroPayWithdraw baroPayWithdraw = null;
//  		  try {
//  			  String sql = " SELECT " +
//  			  			   		" * " +
//  			  			   " FROM " +
//  			  			   		" baropay_withdraw bpw  " +
//  			  			   " WHERE " +
//  			  			   		" bpw.transaction_id = ? ";
//  			  ps = con.prepareStatement(sql);
//  			  ps.setLong(1, transactionId);
//  			  rs = ps.executeQuery();
//  			  if (rs.next()) {
//  				  baroPayWithdraw = new BaroPayWithdraw();
//  				  baroPayWithdraw.setId(rs.getLong("id"));
//  				  baroPayWithdraw.setBankName(rs.getString("BANK_NAME"));
//  				  baroPayWithdraw.setAccountOwner(rs.getString("ACCOUNT_OWNER"));
//  				  baroPayWithdraw.setAccountNumber(rs.getString("ACCOUNT_NUMBER"));  				  
//  			  }
//  		  } finally {
//  			  closeResultSet(rs);
//  			  closeStatement(ps);
//  		  }
//  		  return baroPayWithdraw;
//  	}
//  	
//  	/**
//  	 * Check if user have success deposit from BaroPay
//  	 * @param con
//  	 * @param id
//  	 * @return
//  	 * @throws SQLException
//  	 */
//  	public static boolean isHaveBaroPaySuccessDeposit(Connection con, long userId) throws SQLException {
//  		  PreparedStatement ps = null;
//  		  ResultSet rs = null;
//  		  try {
//  			  String sql = " SELECT " +
//  			  			   		" * " +
//  			  			   " FROM " +
//  			  			   		" transactions t  " +
//  			  			   " WHERE " +
//  			  			   		" t.user_id = ? " +
//  			  			   		" AND t.type_id = ? " +
//  			  			   		" AND t.status_id = ? ";
//  			  ps = con.prepareStatement(sql);
//  			  ps.setLong(1, userId);
//  			  ps.setLong(2, TransactionsManagerBase.TRANS_TYPE_BAROPAY_DEPOSIT);
//  			  ps.setLong(3, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
//  			  rs = ps.executeQuery();
//  			  if (rs.next()) {
//  				  return true;
//  			  }
//  		  } finally {
//  			  closeResultSet(rs);
//  			  closeStatement(ps);
//  		  }
//  		  return false;
//  	}
//
//}