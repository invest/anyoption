package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

import com.anyoption.beans.ProviderDistribution;
import com.anyoption.common.daos.DAOBase;
import com.anyoption.distribution.DistributionHistory;

public class DistributionDAO extends DAOBase {
	
	public static Map<Long, Integer> getProvidersMapForDistribution(Connection con, int type) throws SQLException{
		PreparedStatement ps=null;
		ResultSet rs=null;
		LinkedHashMap<Long, Integer> map = new LinkedHashMap<Long, Integer>();
		String sql =
					" SELECT " +
							" id, " +
							" distribution_percentage " +
					" FROM " +
							" clearing_providers " +
					" WHERE " +	
							" distribution_type = ? " +
					" AND " +
							" is_active = 1 " +
					" ORDER BY " +
							" distribution_percentage DESC ";
		
		try {
			ps = con.prepareStatement(sql);
			ps.setLong(1, type);
			rs=ps.executeQuery();
			while (rs.next()) {
				map.put(rs.getLong("id"),rs.getInt("distribution_percentage"));
			}
		}
		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		
		return map;
	}
	
	public static ArrayList<Long> getProvidersForType( Connection con, int type ) throws SQLException{
		PreparedStatement ps=null;
		ResultSet rs=null;
		ArrayList<Long> list = new ArrayList<Long>();
		String sql =
					" SELECT " +
							" id " +
					" FROM " +
							" clearing_providers " +
					" WHERE " +	
							" distribution_type = ? " +
					" AND " +
							" is_active = 1 " +
					" ORDER BY " +
							" id ";
		
		try {
			ps = con.prepareStatement(sql);
			ps.setLong(1, type);
			rs=ps.executeQuery();
			while (rs.next()) {
				list.add(rs.getLong("id"));
			}
		}
		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		
		return list;
	}
	
	
	public static ArrayList<ProviderDistribution> getProvidersListForDistribution( Connection con, int type ) throws SQLException{
		PreparedStatement ps=null;
		ResultSet rs=null;
		ArrayList<ProviderDistribution> list = new ArrayList<ProviderDistribution>();
		String sql =
					" SELECT " +
							" id, " +
							" name, " +
							" distribution_percentage " +
					" FROM " +
							" clearing_providers " +
					" WHERE " +	
							" distribution_type = ? " +
					" ORDER BY " +
							" distribution_percentage DESC ";
		
		try {
			ps = con.prepareStatement(sql);
			ps.setLong(1, type);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new ProviderDistribution(rs.getLong("id"), rs.getString("name"), rs.getInt("distribution_percentage")));
			}
		}
		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		
		return list;
	}

	public static Long getAssignedProvider(Connection conn, long userId, int type) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		String sql =
					" SELECT " +
							" provider_id " +
					" FROM " +
							" user_to_provider " +
					" WHERE " +	
							" distribution_type = ? " +
					" AND " +
							" user_id = ? ";
					
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setLong(1, type);
			ps.setLong(2, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong("provider_id");
			}
		}
		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		
		return null;
	}
	
	public static ArrayList<Long> getFailedProviderIds( Connection conn, long userId, int type) throws SQLException {
		PreparedStatement ps=null;
		ResultSet rs = null;
		ArrayList<Long> al = new ArrayList<Long>();
		
		String sql =
					" SELECT " +
							" UNIQUE(clearing_provider_id) cp_id " +
					" FROM " +
							" transactions t, " +
							" clearing_providers cp " +
					" WHERE " +
							" t.status_id = 3 " +
					" AND " +
							" t.user_Id = ? " +
					" AND " +
							" cp.distribution_type = ? " +
					" AND " +
							" cp.id = t.clearing_provider_id ";
		try {
			ps = conn.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, type);
			rs = ps.executeQuery();
			while (rs.next()) {
				al.add(rs.getLong("cp_id"));
			}
		}
		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		
		return al;
		
	}
	
	public static boolean cleanUpDistribution(Connection conn, long providerId, int type) throws SQLException { 
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				String sql = "DELETE " +
							 "FROM " +
							 	" user_to_provider " +
							 "WHERE " +
							 		" provider_id = ? " +
							 " AND " +
							 		" distribution_type = ? ";
	
				ps = conn.prepareStatement(sql);
				ps.setLong(1, providerId);
				ps.setLong(2, type);
				int k = ps.executeUpdate();
				return k > 0;
		  } finally {
					closeStatement(ps);
					closeResultSet(rs);
		  }
	}
	
	public static boolean updateClearingProvider(Connection conn, long providerId, int newPercentage) throws SQLException { 
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "UPDATE " +
						 	"clearing_providers " +
						 "SET " +
						 	"distribution_percentage = ? " +
						 "WHERE " +
						 		"id = ? ";

			ps = conn.prepareStatement(sql);
			ps.setInt(1, newPercentage);
			ps.setLong(2, providerId);
			int k = ps.executeUpdate();
			return k > 0;
		} catch(SQLException e) {
			try {
	              conn.rollback();
            } catch (Throwable it) {
            }
            throw e;
	   } finally {
				closeStatement(ps);
				closeResultSet(rs);
	  }
	}
	
	public static void updateDistributionHistory(Connection conn, int type, long writerId, String providerCsv, String percentageCsv) throws SQLException { 
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;

		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		try {
			conn.setAutoCommit(false);
			String sql1  =  " UPDATE " +
									" distribution_history " +
							" SET " +
									" is_active = 0 " +
							" WHERE " +
									" distribution_type = ?";
			
			ps1 = conn.prepareStatement(sql1);
			ps1.setInt(1, type);
			ps1.executeUpdate();
			
			String sql2 = " INSERT INTO  " +
						 		" distribution_history " +
						  		"(id, writer_id, providers_csv, percentages_csv, date_created, is_active, distribution_type) " +
						  	"VALUES " + 
						  		" (SEQ_DISTRIBUTION_HISTORY.nextval, ?, ?, ?, ?, ?, ?)";

			ps2 = conn.prepareStatement(sql2);
			ps2.setLong(1, writerId);
			ps2.setString(2, providerCsv);
			ps2.setString(3, percentageCsv);
			ps2.setDate(4, new java.sql.Date(Calendar.getInstance().getTime().getTime()));
			ps2.setInt(5, 1);
			ps2.setInt(6, type);
			ps2.executeUpdate();
			
			conn.commit();
		} catch(SQLException e) {
			try {
	              conn.rollback();
            } catch (Throwable it) {
            }
            throw e;
	  } finally {
		  		conn.setAutoCommit(true);
		  		closeStatement(ps1);
				closeResultSet(rs1);
				closeStatement(ps2);
				closeResultSet(rs2);
	  }
	}
	
	public static void createUserToProvider(Connection conn, long transactionId, long userId, long providerId, int distribution_type) throws SQLException { 
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = 
					"INSERT INTO " +
					"user_to_provider " +
					"(id, provider_id, user_id, distribution_id, transaction_id, date_created, distribution_type)" +
					"VALUES " +
					 "(SEQ_USER_TO_PROVIDER.NEXTVAL, ?, ?, (SELECT id FROM distribution_history WHERE is_active = 1 AND distribution_type = ?) ,?, SYSDATE, ?)";
 
			ps = conn.prepareStatement(sql);
			ps.setLong(1, providerId);
			ps.setLong(2, userId);
			ps.setInt(3, distribution_type);
			ps.setLong(4, transactionId);
			ps.setInt(5, distribution_type);
			ps.executeUpdate();
	  } finally {
				closeStatement(ps);
				closeResultSet(rs);
	  }	
	}

	public static DistributionHistory getDistributionHistory(Connection conn, int distributionType) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = 
				" SELECT " +
						" TO_CHAR(dh.date_created, 'HH:MM DD/MM/YYYY') datec,  " +
						" w.user_name name" +
				" FROM " +
						" distribution_history dh, " +
						" writers w " +
				" WHERE " +
						" dh.distribution_type = ? " +
				" AND " +
						" dh.writer_id = w.id " +
				" AND " +
						" dh.is_active = 1";
			
			ps = conn.prepareStatement(sql);
			ps.setInt(1, distributionType);

			rs = ps.executeQuery();
			if (rs.next()) {
				return new DistributionHistory(rs.getString("name"), rs.getString("datec"));
			}
		  } finally {
				closeStatement(ps);
				closeResultSet(rs);
		  }
		return null;
	}
}
