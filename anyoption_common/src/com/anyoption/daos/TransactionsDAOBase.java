//package com.anyoption.daos;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Timestamp;
//import java.sql.Types;
//import java.util.ArrayList;
//import java.util.Date;
//
//import oracle.jdbc.OraclePreparedStatement;
//
//import org.apache.log4j.Level;
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.Transaction;
//import com.anyoption.common.bl_vos.ClearingRoute;
//import com.anyoption.common.bl_vos.TransactionBin;
//import com.anyoption.common.daos.CreditCardsDAOBase;
//import com.anyoption.common.managers.TransactionsManagerBase;
//import com.anyoption.common.util.CommonUtil;
//import com.anyoption.common.util.ConstantsBase;
//
//public class TransactionsDAOBase extends com.anyoption.common.daos.TransactionsDAOBase {
//    private static final Logger log = Logger.getLogger(TransactionsDAOBase.class);

//    public static void insert(Connection con, Transaction vo) throws SQLException {
//        OraclePreparedStatement ps = null;
//        ResultSet rs = null;
//        try {
//            String rate = "trunc(convert_amount_to_usd(1, (select currency_id from users where id = ?), sysdate), " + ConstantsBase.RATE_PRECISISON + ")";
//            String sql =
//                "INSERT INTO transactions " +
//                    "(id, user_id, credit_card_id, type_id, amount, status_id, description, writer_id, ip, time_settled, comments, " +
//                    "processed_writer_id, time_created, reference_id, cheque_id, wire_id, charge_back_id, receipt_num, fee_cancel, " +
//                    "utc_offset_created, utc_offset_settled, is_credit_withdrawal, splitted_reference_id, deposit_reference_id, " +
//                    "payment_type_id, bonus_user_id, is_accounting_approved, rate, auth_number, clearing_provider_id, acquirer_response_id" +
//                    ", is_rerouted, rerouting_transaction_id, selector_id) " +
//                "VALUES " +
//                    "(seq_transactions.nextval, ? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,sysdate, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + rate + ", ?, ?, ?, ?, ?, ?)";
//
//            ps = (OraclePreparedStatement) con.prepareStatement(sql);
//            ps.setFormOfUse(6, OraclePreparedStatement.FORM_NCHAR);
//            ps.setFormOfUse(10, OraclePreparedStatement.FORM_NCHAR);
//
//            ps.setLong(1, vo.getUserId());
//            if (null == vo.getCreditCardId() || 0 == vo.getCreditCardId().longValue()) {
//            	ps.setNull(2, Types.NUMERIC);
//            } else {
//            	ps.setBigDecimal(2,vo.getCreditCardId());
//            }
//            ps.setLong(3, vo.getTypeId());
//            ps.setLong(4, vo.getAmount());
//            ps.setLong(5, vo.getStatusId());
//            ps.setString(6, vo.getDescription());
//            ps.setLong(7, vo.getWriterId());
//            ps.setString(8, vo.getIp());
//            ps.setTimestamp(9, convertToTimestamp(vo.getTimeSettled()));
//            ps.setString(10, vo.getComments());
//            ps.setLong(11, vo.getProcessedWriterId());
//            if (null == vo.getReferenceId() || 0 == vo.getReferenceId().longValue()) {
//                ps.setNull(12, Types.NUMERIC);
//            } else {
//                ps.setBigDecimal(12, vo.getReferenceId());
//            }
//            if (null == vo.getChequeId() || 0 == vo.getChequeId().longValue()) {
//                ps.setNull(13, Types.NUMERIC);
//            } else {
//                ps.setBigDecimal(13, vo.getChequeId());
//            }
//            if (null == vo.getWireId() || 0 == vo.getWireId().longValue()) {
//                ps.setNull(14, Types.NUMERIC);
//            } else {
//                ps.setBigDecimal(14, vo.getWireId());
//            }
//            if (null == vo.getChargeBackId() || 0 == vo.getChargeBackId().longValue()) {
//                ps.setNull(15, Types.NUMERIC);
//            } else {
//                ps.setBigDecimal(15, vo.getChargeBackId());
//            }
//            if (null == vo.getReceiptNum() || 0 == vo.getReceiptNum().longValue()) {
//                ps.setNull(16, Types.NUMERIC);
//            } else {
//                ps.setBigDecimal(16, vo.getReceiptNum());
//            }
//            if (vo.isFeeCancel()) {
//                ps.setInt(17, ConstantsBase.CC_FEE_CANCEL_YES);
//            } else {
//                ps.setInt(17, ConstantsBase.CC_FEE_CANCEL_NO);
//            }
//            ps.setString(18, vo.getUtcOffsetCreated());
//            ps.setString(19, vo.getUtcOffsetSettled());
//            if (vo.isCreditWithdrawal()) {
//                ps.setInt(20, 1);
//            } else {
//                ps.setInt(20, 0);
//            }
//            ps.setLong(21, vo.getSplittedReferenceId());
//            ps.setLong(22, vo.getDepositReferenceId());
//            ps.setLong(23, vo.getInatecPaymentTypeId());
//            if (vo.getBonusUserId() > 0) {
//                ps.setLong(24, vo.getBonusUserId());
//            } else {
//                ps.setNull(24, Types.NUMERIC);
//            }
//            if (vo.isAccountingApproved()) {
//                ps.setInt(25, 1);
//            } else {
//                ps.setInt(25, 0);
//            }
//            ps.setLong(26, vo.getUserId());
//            ps.setString(27, vo.getAuthNumber());
//            if (0 == vo.getClearingProviderId()) {
//            	ps.setNull(28, Types.NUMERIC);
//            } else {
//            	ps.setLong(28, vo.getClearingProviderId());
//            }
//            ps.setString(29, vo.getAcquirerResponseId());
//            ps.setLong(30, vo.isRerouted()?1:0);
//            ps.setLong(31, vo.getReroutingTranId());
//            ps.setLong(32, vo.getSelectorId());
//            ps.executeUpdate();
//
//            vo.setId(getSeqCurValue(con, "seq_transactions"));
//
//            if (log.isDebugEnabled()) {
//                log.debug("Transaction started: " + vo.toString());
//            }
//        } finally {
//            closeStatement(ps);
//            closeResultSet(rs);
//        }
//    }
//
//    public static long update(Connection con, Transaction vo) throws SQLException {
//        OraclePreparedStatement ps = null;
//        try {
//            String sql =
//                "UPDATE " +
//                "transactions t " +
//                "SET " +
//                "user_id = ?, " +
//                "credit_card_id = ?, " +
//                "type_id = ?, " +
//                "amount = ?, " +
//                "status_id = ?, " +
//                "description = ?, " +
//                "writer_id = ?, " +
//                "ip = ?, " +
//                "time_settled = ?, " +
//                "comments = ?, " +
//                "processed_writer_id = ?, " +
//                "reference_id = ?, " +
//                "cheque_id = ?, " +
//                "wire_id = ?, " +
//                "charge_back_id = ?, " +
//                "receipt_num = ?, " +
//                "auth_number = ?, " +
//                "xor_id_authorize = ?, " +
//                "utc_offset_settled = ?, " +
//                "clearing_provider_id = ?, " +
//                "fee_cancel = ?, " +
//                "xor_id_capture = ? , " +
//                "acquirer_response_id =?, " +
//                "is_rerouted = ? , " +
//                "rerouting_transaction_id = ? " +
//                "WHERE " +
//                "id = ?";
//
//            ps = (OraclePreparedStatement)con.prepareStatement(sql);
//            ps.setFormOfUse(6, OraclePreparedStatement.FORM_NCHAR);
//            ps.setFormOfUse(11, OraclePreparedStatement.FORM_NCHAR);
//
//            ps.setLong(1, vo.getUserId());
//            if (null == vo.getCreditCardId() || 0 == vo.getCreditCardId().longValue()) {
//            	ps.setNull(2, Types.NUMERIC);
//            } else {
//            	ps.setBigDecimal(2,vo.getCreditCardId());
//            }
//
//            ps.setLong(3, vo.getTypeId());
//            ps.setLong(4, vo.getAmount());
//            ps.setLong(5, vo.getStatusId());
//            ps.setString(6, vo.getDescription());
//            ps.setLong(7, vo.getWriterId());
//            ps.setString(8, vo.getIp());
//            ps.setTimestamp(9, convertToTimestamp(vo.getTimeSettled()));
//            ps.setString(10, vo.getComments());
//            ps.setLong(11, vo.getProcessedWriterId());
//            if (null == vo.getReferenceId() || 0 == vo.getReferenceId().longValue()) {
//                ps.setNull(12, Types.NUMERIC);
//            } else {
//                ps.setBigDecimal(12, vo.getReferenceId());
//            }
//            if (null == vo.getChequeId() || 0 == vo.getChequeId().longValue()) {
//                ps.setNull(13, Types.NUMERIC);
//            } else {
//                ps.setBigDecimal(13, vo.getChequeId());
//            }
//            if (null == vo.getWireId() || 0 == vo.getWireId().longValue()) {
//                ps.setNull(14, Types.NUMERIC);
//            } else {
//                ps.setBigDecimal(14, vo.getWireId());
//            }
//            if (null == vo.getChargeBackId() || 0 == vo.getChargeBackId().longValue()) {
//                ps.setNull(15, Types.NUMERIC);
//            } else {
//                ps.setBigDecimal(15, vo.getChargeBackId());
//            }
//            if (null == vo.getReceiptNum() || 0 == vo.getReceiptNum().longValue()) {
//                ps.setNull(16, Types.NUMERIC);
//            } else {
//                ps.setBigDecimal(16, vo.getReceiptNum());
//            }
//            ps.setString(17, vo.getAuthNumber());
//            ps.setString(18, vo.getXorIdAuthorize());
//            ps.setString(19, vo.getUtcOffsetSettled());
//            if (0 == vo.getClearingProviderId()) {
//            	ps.setNull(20, Types.NUMERIC);
//            } else {
//            	ps.setLong(20, vo.getClearingProviderId());
//            }
//            if (vo.isFeeCancel()) {
//                ps.setLong(21, ConstantsBase.CC_FEE_CANCEL_YES);
//            } else {
//                ps.setLong(21, ConstantsBase.CC_FEE_CANCEL_NO);
//            }
//            ps.setString(22, vo.getXorIdCapture());
//            ps.setString(23, vo.getAcquirerResponseId());
//            ps.setLong(24, vo.isRerouted()?1:0);
//            ps.setLong(25, vo.getReroutingTranId());
//            ps.setLong(26, vo.getId());
//            ps.executeUpdate();
//            if (log.isDebugEnabled()) {
//                log.log(Level.DEBUG, "Transaction updated: " + vo);
//            }
//            ps.executeUpdate();
//        } finally {
//            closeStatement(ps);
//        }
//        return vo.getId();
//    }
//    
//    public static long updateReverseWithdraw(Connection con, Transaction vo) throws SQLException {
//        OraclePreparedStatement ps = null;
//        try {
//            String sql =
//                "UPDATE " +
//                "transactions t " +
//                "SET " +
//                "user_id = ?, " +
//                "credit_card_id = ?, " +
//                "type_id = ?, " +
//                "amount = ?, " +
//                "status_id = ?, " +
//                "description = ?, " +
//                "writer_id = ?, " +
//                "ip = ?, " +
//                "time_settled = ?, " +
//                "comments = ?, " +
//                "processed_writer_id = ?, " +
//                "reference_id = ?, " +
//                "cheque_id = ?, " +
//                "wire_id = ?, " +
//                "charge_back_id = ?, " +
//                "receipt_num = ?, " +
//                "auth_number = ?, " +
//                "xor_id_authorize = ?, " +
//                "utc_offset_settled = ?, " +
//                "clearing_provider_id = ?, " +
//                "fee_cancel = ?, " +
//                "xor_id_capture = ? , " +
//                "acquirer_response_id =?, " +
//                "is_rerouted = ? , " +
//                "rerouting_transaction_id = ? " +
//                "WHERE " +
//                "id = ? " + 
//                " and status_id in (" + TransactionsManagerBase.TRANS_STATUS_REQUESTED + ", " + TransactionsManagerBase.TRANS_STATUS_APPROVED + ", " + TransactionsManagerBase.TRANS_STATUS_SECOND_APPROVAL + ")";
//
//            ps = (OraclePreparedStatement)con.prepareStatement(sql);
//            ps.setFormOfUse(6, OraclePreparedStatement.FORM_NCHAR);
//            ps.setFormOfUse(11, OraclePreparedStatement.FORM_NCHAR);
//
//            ps.setLong(1, vo.getUserId());
//            if (null == vo.getCreditCardId() || 0 == vo.getCreditCardId().longValue()) {
//            	ps.setNull(2, Types.NUMERIC);
//            } else {
//            	ps.setBigDecimal(2,vo.getCreditCardId());
//            }
//
//            ps.setLong(3, vo.getTypeId());
//            ps.setLong(4, vo.getAmount());
//            ps.setLong(5, vo.getStatusId());
//            ps.setString(6, vo.getDescription());
//            ps.setLong(7, vo.getWriterId());
//            ps.setString(8, vo.getIp());
//            ps.setTimestamp(9, convertToTimestamp(vo.getTimeSettled()));
//            ps.setString(10, vo.getComments());
//            ps.setLong(11, vo.getProcessedWriterId());
//            if (null == vo.getReferenceId() || 0 == vo.getReferenceId().longValue()) {
//                ps.setNull(12, Types.NUMERIC);
//            } else {
//                ps.setBigDecimal(12, vo.getReferenceId());
//            }
//            if (null == vo.getChequeId() || 0 == vo.getChequeId().longValue()) {
//                ps.setNull(13, Types.NUMERIC);
//            } else {
//                ps.setBigDecimal(13, vo.getChequeId());
//            }
//            if (null == vo.getWireId() || 0 == vo.getWireId().longValue()) {
//                ps.setNull(14, Types.NUMERIC);
//            } else {
//                ps.setBigDecimal(14, vo.getWireId());
//            }
//            if (null == vo.getChargeBackId() || 0 == vo.getChargeBackId().longValue()) {
//                ps.setNull(15, Types.NUMERIC);
//            } else {
//                ps.setBigDecimal(15, vo.getChargeBackId());
//            }
//            if (null == vo.getReceiptNum() || 0 == vo.getReceiptNum().longValue()) {
//                ps.setNull(16, Types.NUMERIC);
//            } else {
//                ps.setBigDecimal(16, vo.getReceiptNum());
//            }
//            ps.setString(17, vo.getAuthNumber());
//            ps.setString(18, vo.getXorIdAuthorize());
//            ps.setString(19, vo.getUtcOffsetSettled());
//            if (0 == vo.getClearingProviderId()) {
//            	ps.setNull(20, Types.NUMERIC);
//            } else {
//            	ps.setLong(20, vo.getClearingProviderId());
//            }
//            if (vo.isFeeCancel()) {
//                ps.setLong(21, ConstantsBase.CC_FEE_CANCEL_YES);
//            } else {
//                ps.setLong(21, ConstantsBase.CC_FEE_CANCEL_NO);
//            }
//            ps.setString(22, vo.getXorIdCapture());
//            ps.setString(23, vo.getAcquirerResponseId());
//            ps.setLong(24, vo.isRerouted()?1:0);
//            ps.setLong(25, vo.getReroutingTranId());
//            ps.setLong(26, vo.getId());
//            int updateCount = ps.executeUpdate();
//            if (updateCount == 0) {
//				throw new SQLException("No rows updated! Probably the user tries to reverse withdraw twice");
//			}
//            if (log.isDebugEnabled()) {
//            	log.log(Level.DEBUG, "Transaction updated: " + vo);
//            }
//        } finally {
//            closeStatement(ps);
//        }
//        return vo.getId();
//    }
//
////	/**
////	 * Is first succeed deposit
////	 * @param con
////	 * @param userId
////	 * @return
////	 * @throws SQLException
////	 */
////	public static boolean isFirstDeposit(Connection con,long userId, long expectedValue) throws SQLException {
////
////		  PreparedStatement ps = null;
////		  ResultSet rs = null;
////
////		  try {
////			    String sql = " select count(*) num" +
////			    			 " from transactions t, transaction_types tt " +
////			    			 " where t.type_id = tt.id and t.user_id=? and "+
////			    			 " tt.class_type = ? AND "+
////			    			 " t.status_id in ("+ TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES +") ";
////
////				ps = con.prepareStatement(sql);
////				ps.setLong(1, userId);
////				ps.setLong(2, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
////
////				rs = ps.executeQuery();
////
////				if (rs.next()) {
////					long num = rs.getLong("num");
////					if (num > expectedValue) {
////						return false;
////					} else {
////						return true;
////					}
////
////				}
////				return false;
////			}
////
////			finally {
////				closeResultSet(rs);
////				closeStatement(ps);
////			}
////	}
//
//    public static long getSummaryByDates(Connection conn, long userId, long classType, Date from, Date to) throws SQLException {
//        long total = 0;
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        try {
//            String sql =
//                "SELECT " +
//                    "SUM(amount) total " +
//                "FROM " +
//                    "transactions, " +
//                    "transaction_types tt " +
//                "WHERE " +
//                    "type_id = tt.id AND " +
//                    "user_id = ? AND " +
//                    "tt.class_type = ? AND " +
//                    "status_id IN (" +
//                        TransactionsManagerBase.TRANS_STATUS_SUCCEED + "," +
//                        TransactionsManagerBase.TRANS_STATUS_PENDING +
//                    ") AND " +
//                    "trunc(time_created, 'DDD') >= trunc(?, 'DDD') AND " +
//                    "trunc(time_created, 'DDD') <= trunc(?, 'DDD')";
//            pstmt = conn.prepareStatement(sql);
//            pstmt.setLong(1, userId);
//            pstmt.setLong(2, classType);
//            pstmt.setTimestamp(3, new Timestamp(from.getTime()));
//            pstmt.setTimestamp(4, new Timestamp(to.getTime()));
//            rs = pstmt.executeQuery();
//            if (rs.next()) {
//                total = rs.getLong("total");
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(pstmt);
//        }
//        return total;
//
//    }
//
//    /**
//     * get user transactions by date range and statuses
//     * @param con
//     * @param id
//     * @param from
//     * @param to
//     * @param status
//     * @param internalType
//     * @return
//     * @throws SQLException
//     */
//	public static ArrayList<Transaction> getByUserAndDates(Connection con,long id,Date from,Date to,
//			String status, int internalType, Integer startRow, Integer pageSize) throws SQLException {
//
//		  PreparedStatement ps = null;
//		  ResultSet rs = null;
//		  ArrayList<Transaction> list = new ArrayList<Transaction>();
//		  try {
//			    String sql = "SELECT " +
//			    				"* " +
//			    			 "FROM " +
//				    			 "(SELECT  " +
//				    				"X.*, " +
//				    				"rownum AS rownumIn " +
//				    			 "FROM " +
//					    			"(SELECT " +
//									    "t.*, " +
//						                "ty.description typename, " +
//						                "ty.class_type classtype, " +
//						                "ts.description statusdesc, " +
//						                "u.currency_id currency_id, " +
//						                "pt.name paymentMethodName, " +
//						                "pt.description directProviderName " +
//					                "FROM " +
//					                	"transactions t " +
//					                	"left join  bonus_users bu on t.bonus_user_id = bu.id " +
//					                         "left join bonus_types bt on bt.id = bu.type_id " +
//					                    "left join payment_methods pt on t.payment_type_id = pt.id, " +
//					                    "transaction_types ty, " +
//					                    "transaction_statuses ts, " +
//					                    "users u " +
//					                 "WHERE " +
//					                 	"t.type_id = ty.id and " +
//					                 	"t.status_id = ts.id and " +
//					                 	"t.user_id = ? and " +
//					                 	"t.user_id = u.id and " +
//					                 	"t.type_id <> ? and " +
//			    						"t.type_id <> ? and ";
//					    	if (null != from && null != to) {
//							    sql +=  "t.time_created >= ? and " +
//							    		"t.time_created <= ? and ";
//					    	}
//							    sql +=	"t.status_id in (" + status + ") and " +
//							    		"(bt.class_type_id is null or bt.class_type_id != " + ConstantsBase.BONUS_CLASS_TYPE_NEXT_INVEST_ON_US + ") " +
//							    	"ORDER BY " +
//					                 	"t.time_created desc) X) ";
//
//					    if (null != startRow && null != pageSize) {
//					    	sql += "WHERE " +
//						    		   "rownumIn > " + startRow + " AND " +
//			   						   "rownumIn <= " + (startRow + pageSize);
//					    }
//
//				ps = con.prepareStatement(sql);
//				ps.setLong(1, id);
//				ps.setLong(2, internalType);
//				ps.setLong(3, TransactionsManagerBase.TRANS_TYPE_CUP_INTERNAL_CREDIT); 
//				if (null != from && null != to) {
//					ps.setTimestamp(4, convertToTimestamp(from));
//					ps.setTimestamp(5, convertToTimestamp(to));
//				}
//				rs = ps.executeQuery();
//
//				while (rs.next()) {
//					Transaction t = getVO(con,rs);
//					t.setPaymentTypeDesc(rs.getString("directProviderName"));
//					t.setPaymentTypeName(rs.getString("paymentMethodName"));
//				    list.add(t);
//				}
//			} finally {
//				closeResultSet(rs);
//				closeStatement(ps);
//			}
//			return list;
//	  }
//
//	/**
//	 * Get transaction by status and classType
//	 * @param con
//	 * @param id
//	 * @param status
//	 * @param classType
//	 * @param order
//	 * @return
//	 * @throws SQLException
//	 */
//	public static ArrayList<Transaction> getByUserAndStatus(Connection con, long id, String status, long classType, String order) throws SQLException {
//		  PreparedStatement ps = null;
//		  ResultSet rs = null;
//		  ArrayList<Transaction> list = new ArrayList<Transaction>();
//		  try {
//			    String sql =
//			    		"SELECT " +
//			    			"t.*," +
//			    			"p.description typename," +
//			    			"p.class_type classtype, " +
//			    			"u.currency_id currency_id " +
//			    		"FROM " +
//			    			"transactions t," +
//			    			"transaction_types p, " +
//			    			"users u " +
//			    		"WHERE " +
//			    			"t.user_id = u.id AND " +
//			    			"t.user_id=? AND " +
//			    		    "t.status_id IN ( " + status + " ) AND " +
//			    		    "t.type_id = p.id AND " +
//			    		    "p.class_type = ? " +
//		    		    "ORDER BY " +
//		    		    	"t.time_created  " + order;
//
//				ps = con.prepareStatement(sql);
//				ps.setLong(1, id);
//				ps.setLong(2, classType);
//				rs = ps.executeQuery();
//
//				while (rs.next()) {
//					list.add(getVO(con,rs));
//				}
//			} finally {
//				closeResultSet(rs);
//				closeStatement(ps);
//			}
//			return list;
//	}
//
//    public static Transaction getById(Connection con, long id) throws SQLException {
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        Transaction vo = null;
//        try {
//            String sql =
//                "SELECT " +
//                    "t.*, " +
//                    "ty.description typename, " +
//                    "ty.class_type classtype, " +
//                    "u.currency_id currency_id, " +
//                    "pt.description directProviderName " +
//                "FROM " +
//                    "transactions t, " +
//                    "transaction_types ty, " +
//                    "users u, " +
//                    "direct_payment_types pt " +
//                "WHERE " +
//                    "t.user_id = u.id AND " +
//                    "t.type_id = ty.id AND " +
//                    "t.payment_type_id = pt.id (+) AND " +
//                    "t.id = ?";
//            ps = con.prepareStatement(sql);
//            ps.setLong(1, id);
//            rs = ps.executeQuery();
//            if (rs.next()) {
//            	vo = getVO(con, rs);
//            	vo.setPaymentTypeName(rs.getString("directProviderName"));
//            	vo.setBonusUserId(rs.getLong("bonus_user_id"));
//            	if (vo.getTypeId() == TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT) {
//            		 vo.setCc4digit(CreditCardsDAOBase.getById(con, Long.parseLong(String.valueOf(vo.getCreditCardId()))).getCcNumberLast4());
//            	}
//
//                return vo;
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(ps);
//        }
//        return vo;
//    }
//
//    protected static Transaction getVO(Connection con, ResultSet rs) throws SQLException {
//        Transaction vo = new Transaction();
//        vo.setId(rs.getLong("id"));
//        vo.setUserId(rs.getLong("user_id"));
//        vo.setCreditCardId(rs.getBigDecimal("credit_card_id"));
//        vo.setReferenceId(rs.getBigDecimal("reference_id"));
//        vo.setTypeId(rs.getLong("type_id"));
//        vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
//        vo.setTimeSettled(convertToDate(rs.getTimestamp("time_settled")));
//        vo.setAmount(rs.getLong("amount"));
//        vo.setStatusId(rs.getLong("status_id"));
//        vo.setDescription(rs.getString("description"));
//        vo.setWriterId(rs.getLong("writer_id"));
//        vo.setIp(rs.getString("ip"));
//        vo.setComments(rs.getString("comments"));
//        vo.setProcessedWriterId(rs.getLong("processed_writer_id"));
//        vo.setChequeId(rs.getBigDecimal("cheque_id"));
//        vo.setWireId(rs.getBigDecimal("wire_id"));
//        vo.setChargeBackId(rs.getBigDecimal("charge_back_id"));
//        vo.setReceiptNum(rs.getBigDecimal("receipt_num"));
//        vo.setTypeName(rs.getString("typename"));
//        vo.setClassType(rs.getLong("classtype"));
//        vo.setAuthNumber(rs.getString("auth_number"));
//        vo.setXorIdAuthorize(rs.getString("xor_id_authorize"));
//        vo.setXorIdCapture(rs.getString("xor_id_capture"));
//        vo.setCurrencyId(rs.getLong("currency_id"));
//        //vo.setCurrency(CommonUtil.getAppData().getCurrencyById(rs.getInt("currency_id")));
//        vo.setUtcOffsetCreated(rs.getString("utc_offset_created"));
//        vo.setUtcOffsetSettled(rs.getString("utc_offset_settled"));
//        vo.setClearingProviderId(rs.getLong("clearing_provider_id"));
//        vo.setAcquirerResponseId(rs.getString("acquirer_response_id"));
//        vo.setIsRerouted(rs.getLong("is_rerouted") == 1 ? true : false);
//        vo.setReroutingTranId(rs.getLong("rerouting_transaction_id"));
//        vo.setSelectorId(rs.getInt("selector_id"));
//        return vo;
//    }
//
//    /**
//     * Get sum deposits of user this is for deposit limitation for anyoption
//     * we take only riski transaction type, (is_risky=1)
//     *
//     * @param con Db connection
//     * @param userId user id for taking his deposits
//     * @return sum deposits of the user
//     * @throws SQLException
//     */
//    public static long getSumDepositsForLimitation(Connection conn, long userId) throws SQLException {
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        long sumDeposits = 0;
//        try {
//            String sql =
//                "SELECT " +
//                    "sum(t.amount) sum_deposits " +
//                "FROM " +
//                    "transactions t, " +
//                    "transaction_types tt " +
//                "WHERE " +
//                    "t.user_id = ? AND " +
//                    "t.type_id = tt.id AND " +
//                    "tt.class_type = ? and " +
//                    "tt.is_risky = 1 and " +
//                    "t.status_id in (" +
//                        TransactionsManagerBase.TRANS_STATUS_SUCCEED + "," +
//                        TransactionsManagerBase.TRANS_STATUS_PENDING +
//                    ")";
//            pstmt = conn.prepareStatement(sql);
//            pstmt.setLong(1, userId);
//            pstmt.setLong(2, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
//            rs = pstmt.executeQuery();
//            if (rs.next()) {
//                sumDeposits = rs.getLong("sum_deposits");
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(pstmt);
//        }
//        return sumDeposits;
//    }
//
//    public static long getNextReceipt(Connection conn) throws SQLException {
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        try {
//            String sql =
//                "SELECT " +
//                    "seq_receipt.nextval val " +
//                "FROM " +
//                    "dual ";
//            pstmt = conn.prepareStatement(sql);
//            rs = pstmt.executeQuery();
//            if (rs.next()) {
//                return rs.getLong("val");
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(pstmt);
//        }
//        return 0;
//    }
//
//    public static void setTransactionBonuUsersId(Connection conn, long transactionId, long bonusUsersId) throws SQLException {
//        PreparedStatement pstmt = null;
//        try{
//            String sql =
//                    "UPDATE " +
//                        "transactions " +
//                    "SET " +
//                        "bonus_user_id = ? " +
//                    "WHERE " +
//                        "id = ?";
//            pstmt = conn.prepareStatement(sql);
//            pstmt.setLong(1, bonusUsersId);
//            pstmt.setLong(2, transactionId);
//            pstmt.executeUpdate();
//        } finally {
//            closeStatement(pstmt);
//        }
//    }
//
////    public static boolean isFailureRepeat(Connection conn, long userId, long cardId, long amount , long transactionId, boolean firstDeposit) throws SQLException {
////    	boolean clearingFlag = false;
////        PreparedStatement pstmt = null;
////        ResultSet rs = null;
////        long failsNum = Long.valueOf(CommonUtil.getEnum("failure_transaction_range", "transactions_number")).longValue();
////        try {
////            String sql =
////                "SELECT " +
////                	" * " +
////                "FROM " +
////                	"(SELECT " +
////	                	"t.time_created trx_time_created, " +
////	                	"t.status_id, " +
////	                	"t.clearing_provider_id, " +
////	                	"t.amount, " +
////	                	"cc.time_modified " +
////		              "FROM " +
////		               	"transactions t, " +
////		               	"credit_cards cc " +
////		              "WHERE " +
////		                "t.credit_card_id = cc.id AND " +
////		               	"t.user_id = ? AND " +
////		               	"t.credit_card_id = ? AND " +
////		               	"t.type_id = ? AND " +
////		               	"t.id <> ? AND " + //don't count current transaction with status started
////		               	"t.description not like 'error.min.deposit' AND " +
////		               	"t.time_created >= SYSDATE - " +
////		               		"(SELECT value " +
////		              		  "FROM enumerators " +
////		               		  "WHERE enumerator = 'failure_transaction_range' AND code = 'minutes') " +
////		               		" * 1/24/60 " +
////		                "ORDER BY t.time_created DESC) " +
////		        "WHERE rownum <= ? "; //we always look at the last X trx" +
////            pstmt = conn.prepareStatement(sql);
////            pstmt.setLong(1, userId);
////            pstmt.setLong(2, cardId);
////            pstmt.setLong(3, TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT);
////            pstmt.setLong(4, transactionId);
////            pstmt.setLong(5, failsNum);
////            rs = pstmt.executeQuery();
////            long minAmount = 0;
////            int counter = 0;
////            long clearing_provider_id = -1;
////            long status_id = 0;
////            long currAmount = 0;
////            while (rs.next()) {
////            	if (clearing_provider_id != 0 &&
////            			clearing_provider_id != -1 &&
////            			rs.getLong("clearing_provider_id") == 0){ // identifying request to provider to reset counting
////            		clearingFlag = true;
////            	}
////            	clearing_provider_id = (rs.getLong("clearing_provider_id"));
////            	status_id = rs.getLong("status_id");
////
////                if (status_id != TransactionsManagerBase.TRANS_STATUS_FAILED){
////                	break;
////                }
////
////                ++counter;
////                currAmount = rs.getLong("amount");
////                if (1 == counter) {
////                	Date ccTimeModified = convertToDate(rs.getTimestamp("time_modified"));
////                	Date trx_time_created = convertToDate(rs.getTimestamp("trx_time_created"));
////                	if (ccTimeModified.after(trx_time_created) && !firstDeposit){ //if cc modified after trx was created
////                		return false;
////                	}
////
////                	minAmount = currAmount;
////                } else if (currAmount < minAmount) {
////                		minAmount = currAmount;
////                }
////            }
////
////            if (counter >= failsNum && clearingFlag == true){ // if we identified request to provider we want to reset the counter
////            	return false;
////            }
////
////            if (counter >= failsNum && minAmount <= amount) {
////            	return true;
////            }
////        } finally {
////            closeResultSet(rs);
////            closeStatement(pstmt);
////        }
////        return false;
////    }
//
//    /**
//     * Get last failed deposit
//     * @param con
//     * @param userId
//     * @return
//     * @throws SQLException
//     */
//    public static Transaction getLastFailedDeposit(Connection con, long userId) throws SQLException {
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        Transaction vo = null;
//        try {
//            String sql =
//            	"SELECT " +
//		        	"t.id, " +
//		        	"t.amount " +
//		        "FROM " +
//		            "transactions t, " +
//		            "transaction_types tt " +
//		        "WHERE " +
//		            "t.user_id = ? AND " +
//		            "t.type_id = tt.id AND " +
//		            "tt.class_type = ? AND " +
//		            "t.status_id = ? " +
//		        "ORDER BY " +
//		                "t.time_created DESC " ;
//            ps = con.prepareStatement(sql);
//            ps.setLong(1, userId);
//            ps.setLong(2, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
//            ps.setLong(3, TransactionsManagerBase.TRANS_STATUS_FAILED);
//            rs = ps.executeQuery();
//            if (rs.next()) {
//            	vo = new Transaction();
//            	vo.setId(rs.getLong("id"));
//            	vo.setAmount(rs.getLong("amount"));
//                return vo;
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(ps);
//        }
//        return vo;
//    }
//
//    /**
//     * Get avg of deposits, used for population type noXdeposits.
//     * @param con
//     * @param userId
//     * @return
//     * @throws SQLException
//     */
//    public static long getAVGSucceedDeposit(Connection con, long userId) throws SQLException {
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        long avgDeposits = 0;
//        try {
//            String sql =
//            	" SELECT " +
//		        	" SUM(t.amount) AS deposits, " +
//		        	" COUNT(t.id) AS num_of_deposits " +
//		        " FROM " +
//		            " transactions t, " +
//		            " transaction_types tt " +
//		        " WHERE " +
//		            " t.user_id = ? AND " +
//		            " t.type_id = tt.id AND " +
//		            " tt.class_type = ? AND " +
//		            " t.status_id in ( " + TransactionsManagerBase.TRANS_STATUS_SUCCEED + ", " + TransactionsManagerBase.TRANS_STATUS_PENDING + " ) ";
//
//            ps = con.prepareStatement(sql);
//            ps.setLong(1, userId);
//            ps.setLong(2, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
//            rs = ps.executeQuery();
//            if (rs.next()) {
//            	avgDeposits = rs.getLong("deposits") / rs.getLong("num_of_deposits");
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(ps);
//        }
//        return avgDeposits;
//    }
//
//  //a new insert method is added to allow inserting transaction from the service (since the service is running under Jboss)
//	public static void insertFromService(Connection con, Transaction vo) throws SQLException {
//	  PreparedStatement ps = null;
//	  ResultSet rs = null;
//	  try {
//		  	String rate = "trunc(convert_amount_to_usd(1,(select currency_id from users where id=?),sysdate),"+ ConstantsBase.RATE_PRECISISON +")";
//			String sql="insert into transactions t(id,user_id,credit_card_id,type_id," +
//					"amount,status_id,description,writer_id,ip,time_settled,comments,processed_writer_id," +
//					"time_created,reference_id,cheque_id,wire_id,charge_back_id,receipt_num,fee_cancel," +
//					"utc_offset_created, utc_offset_settled, bonus_user_id, rate, acquirer_response_id) " +
//										"values(seq_transactions.nextval,?,?,?,?,?,?,?,?,?,?,?,sysdate,?,?,?,?,?,?,?,?,?,"+rate+", ?)";
//
//			ps = (PreparedStatement)con.prepareStatement(sql);
//			ps.setLong(1, vo.getUserId());
//			ps.setBigDecimal(2,vo.getCreditCardId());
//			ps.setLong(3, vo.getTypeId());
//			ps.setLong(4, vo.getAmount());
//			ps.setLong(5, vo.getStatusId());
//			ps.setString(6, vo.getDescription());  //In Orace this field is of type NCHAR hence will have problems inserting hebrew
//			ps.setLong(7, vo.getWriterId());
//			ps.setString(8, vo.getIp());
//			ps.setTimestamp(9, CommonUtil.convertToTimeStamp(vo.getTimeSettled()));
//			ps.setString(10, vo.getComments());	//In Orace this field is of type NCHAR hence will have problems inserting hebrew
//			ps.setLong(11, vo.getProcessedWriterId());
//			ps.setBigDecimal(12,vo.getReferenceId());
//			ps.setBigDecimal(13,vo.getChequeId());
//			ps.setBigDecimal(14,vo.getWireId());
//			ps.setBigDecimal(15,vo.getChargeBackId());
//			ps.setBigDecimal(16,vo.getReceiptNum());
//			if (vo.isFeeCancel()) {
//				ps.setInt(17, ConstantsBase.CC_FEE_CANCEL_YES);
//			}
//			else {
//				ps.setInt(17, ConstantsBase.CC_FEE_CANCEL_NO);
//			}
//			ps.setString(18, vo.getUtcOffsetCreated());
//			ps.setString(19, vo.getUtcOffsetSettled());
//            if (vo.getBonusUserId() > 0) {
//                ps.setLong(20, vo.getBonusUserId());
//            } else {
//                ps.setNull(20, Types.NUMERIC);
//            }
//			ps.setLong(21, vo.getUserId());
//			ps.setString(22, vo.getAcquirerResponseId());
//			ps.executeUpdate();
//
//			vo.setId(getSeqCurValue(con,"seq_transactions"));
//
//			if (log.isDebugEnabled()) {
//	            String ls = System.getProperty("line.separator");
//	            log.log(Level.DEBUG, ls + "Transaction started:" + vo.toString());
//			}
//	  }
//		finally
//		{
//			closeStatement(ps);
//			closeResultSet(rs);
//		}
//
//  }
//
//    /**
//     * Check paypal email
//     *
//     * @param con
//     * @param userId
//     * @param paypalEmail
//     * @return boolean - is valid paypal email
//     * @throws SQLException
//     */
//    public static boolean isPaypalEmailValid(Connection con, long userId, String paypalEmail) throws SQLException {
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        try {
//            String sql =
//                " SELECT " +
//                    " * " +
//                " FROM " +
//                    " transactions t " +
//                " WHERE " +
//                    " t.user_id = ? " +
//                    " AND t.paypal_email = ? " +
//                    " AND t.type_id = " + TransactionsManagerBase.TRANS_TYPE_PAYPAL_DEPOSIT + " " +
//                    " AND t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ")";
//            ps = con.prepareStatement(sql);
//            ps.setLong(1, userId);
//            ps.setString(2, paypalEmail);
//            rs = ps.executeQuery();
//            if (rs.next()) {
//                return true;
//            }
//        } finally {
//            closeStatement(ps);
//        }
//        return false;
//    }
//
//    /**
//     * Check Moneybookers email
//     *
//     * @param con
//     * @param userId
//     * @param moneybookersEmail
//     * @return boolean - is valid moneybookersEmail
//     * @throws SQLException
//     */
//    public static boolean isMoneybookersEmailValid(Connection con, long userId, String moneybookersEmail) throws SQLException {
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        try {
//            String sql =
//                " SELECT " +
//                    " * " +
//                " FROM " +
//                    " transactions t " +
//                " WHERE " +
//                    " t.user_id = ? " +
//                    " AND t.MONEYBOOKERS_EMAIL = ? " +
//                    " AND t.type_id = " + TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_DEPOSIT + " " +
//                    " AND t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ")";
//            ps = con.prepareStatement(sql);
//            ps.setLong(1, userId);
//            ps.setString(2, moneybookersEmail);
//            rs = ps.executeQuery();
//            if (rs.next()) {
//                return true;
//            }
//        } finally {
//            closeStatement(ps);
//        }
//        return false;
//    }
//
//    /**
//	 * Update deposit pixel run time
//	 * @param con
//	 * @param id
//	 * @throws SQLException
//	 */
//	public static void updatePixelRunTime(Connection con, long id) throws SQLException {
//		PreparedStatement ps = null;
//		try {
//			String sql =
//				"UPDATE " +
//					"transactions " +
//				"SET " +
//					"pixel_run_time = sysdate " +
//				"WHERE " +
//					"id = ? ";
//
//			ps = con.prepareStatement(sql);
//			ps.setLong(1, id);
//			ps.executeUpdate();
//		} finally {
//			closeStatement(ps);
//		}
//	}
//	
//	public static long getSumOfDepositsForRank(Connection con, long userId) throws SQLException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		long amount = 0;
//
//		try {
//			String sql = 
//					" SELECT " +
//					"		sum((t.amount/100) * t.rate) as userAmount " +
//					" FROM " +
//					"		transactions t, transaction_types tt " +
//					" WHERE " +
//					"		t.user_id = ? " +
//					"		AND t.type_id = tt.id " +
//					"		AND tt.class_type = ? " +
//					"		AND t.status_id in (?,?,?)";
//
//			ps = con.prepareStatement(sql);
//			ps.setLong(1, TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_DEPOSIT);
//			ps.setLong(2, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
//			ps.setLong(3, TransactionsManagerBase.TRANS_STATUS_PENDING);
//			ps.setLong(4, TransactionsManagerBase.TRANS_STATUS_CANCELED_ETRADER);
//			rs = ps.executeQuery();
//
//			if (rs.next()) {
//				amount = rs.getLong("userAmount");
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return amount;
//	}
//	
//
//	public static long getConsecutiveFailuresByBin(Connection conn , ClearingRoute cr, long bin) throws SQLException{
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		String sql = " SELECT count( distinct( cc.cc_number ) ) as num_of_failures " +
//					 " FROM transactions t , credit_cards cc " +
//					 " WHERE " +
//					 "		t.credit_card_id = cc.id " +
//					 " AND  t.type_id = ? " +
//					 " AND t.status_id = ? " +
//					 " AND t.time_created >=  ? " +
//					 " AND cc.bin = ? ";
//		try{
//			ps = conn.prepareStatement(sql);
//			ps.setLong(1, TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT);
//			ps.setLong(2, TransactionsManagerBase.TRANS_STATUS_FAILED);
//			ps.setTimestamp(3, CommonUtil.convertToTimeStamp(cr.getTimelastSuccess()));
//			ps.setLong(4, bin);
//			
//			rs = ps.executeQuery();
//			if(rs.next()){
//				return rs.getLong("num_of_failures");
//			}
//			
//		} finally {
//			closeStatement(ps);
//			closeResultSet(rs);
//		}
//		return 0;
//	}
//	
//
//	public static long insertTranBin(Connection con, TransactionBin tb) throws SQLException{
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		
//		String sql = " insert into TRANSACTIONS_BINS( id, from_bin, time_last_success, time_last_failed_reroute, updated_by_rerouting, business_case_id, platform_id) " +
//				" values (SEQ_TRANSACTIONS_BINS.nextval , ?, null, null, 1, ?, ?)";
//		try {
//			ps = con.prepareStatement(sql);
//			ps.setLong(1, tb.getBin());
//			ps.setLong(2, tb.getBusinessCaseId());
//			ps.setInt(3, tb.getPlatformId());
//			rs = ps.executeQuery();
//			tb.setId(getSeqCurValue(con, "SEQ_TRANSACTIONS_BINS"));
//			tb = getTransactionBinById(con, tb.getId());
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);			
//		}
//		return tb.getId();
//	}
//
//	public static TransactionBin getTransactionBinById(Connection conn, long id) throws SQLException{
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		TransactionBin tb = new TransactionBin();
//		String sql ="select * from transactions_bins where id = " + id ;
//		try {
//			ps = conn.prepareStatement(sql);
//			rs = ps.executeQuery();
//			if(rs.next()){
//				tb.setId(rs.getLong("id"));
//				tb.setBin(rs.getLong("from_bin"));
//				tb.setTimeLastSuccess(rs.getDate("time_last_success"));
//				tb.setTimeLastFailedReroute(rs.getDate("time_last_failed_reroute"));
//				tb.setBusinessCaseId(rs.getLong("business_case_id"));
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return tb;		
//	}
//	
//
//	
//	public static void updateTimeLastSuccess(Connection con, TransactionBin vo) throws SQLException{
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		String sql = " UPDATE " +
//				 "		transactions_bins " +
//				 " SET " +
//				 "		TIME_LAST_SUCCESS = sysdate " +
//				 " WHERE " +
//				 "		id = ? ";
//		try {
//			ps = con.prepareStatement(sql);
//			ps.setLong(1, vo.getId());
//			ps.execute();
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//	}
//	
//	public static void updateTimeLastRerouteFailed(Connection con, TransactionBin vo) throws SQLException{
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		String sql = " UPDATE " +
//				 "		transactions_bins " +
//				 " SET " +
//				 "		time_last_failed_reroute = sysdate " +
//				 " WHERE " +
//				 "		id = ? ";
//		try {
//			ps = con.prepareStatement(sql);
//			ps.setLong(1, vo.getId());
//			ps.execute();
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//	}
//	
//	public static String velocityCheck(Connection con, long userID) throws SQLException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		String resault = null;
//		long velocityResault = 0;
//		try {
//		    String sql = "SELECT " +
//							" TRANSACTIONS_MANAGER.VELOCITY_CHECK(?) as RES" +
//						" FROM " +
//							" DUAL ";
//	
//			ps = con.prepareStatement(sql);
//			ps.setLong(1, userID);
//			rs = ps.executeQuery();
//			if (rs.next()) {   // user have monthly withdrawals
//				velocityResault = rs.getLong("RES");
//			}
//			
//			if(velocityResault == 1) {
//				resault = ConstantsBase.ERROR_DEPOSIT_VELOCITY_AO_NO_DOC;
//			} else if(velocityResault == 2) {
//				resault = ConstantsBase.ERROR_DEPOSIT_VELOCITY_AO_HAVE_DOC;			
//			} else if(velocityResault == 3) {
//				resault = ConstantsBase.ERROR_DEPOSIT_VELOCITY_ET_NO_DOC;
//			} else if(velocityResault == 4) {
//				resault = ConstantsBase.ERROR_DEPOSIT_VELOCITY_ET_HAVE_DOC;
//			} else {
//				resault = null;
//			}				
//			
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return resault;
//	}
//
//	public static long getCUPWithdrawalAmount(Connection con, long userId, boolean countBeforeCup) throws SQLException {
//
//			PreparedStatement ps = null;
//			ResultSet rs = null;
//			
//			try {
//				String sql = 
//						
//					" SELECT " +
//							" nvl( deposit_amount - withdraw_amount,0) as available_to_withdraw " + 
//					" FROM " +
//					" (" +
//							" SELECT " +
//									" nvl(sum(t.amount - t.credit_amount),0) as deposit_amount " + 
//							" FROM " +
//									" transactions t, clearing_providers cp " + 
//							" WHERE " +
//									" t.user_id = ? " + 
//							" AND " +
//									" (cp.id = t.clearing_provider_id and cp.is_active = 1 ) " +
//							" AND " +
//									" t.type_id = 38 " + // CUP_DEPOSIT
//							" AND " +
//									" t.status_id in  ( 2,7,8 ) " + 
//							" AND " +
//									" t.selector_id > ?" +
//							" AND " + 
//						            " (t.amount - t.credit_amount) > 0 " + 
//					" ), " + 
//					" ( " +
//							" SELECT" + 
//									" NVL(SUM (t.amount - t.internals_amount),0)  AS WITHDRAW_AMOUNT" + 
//							" FROM " +
//									" transactions t " + 
//							" WHERE " +
//									" t.user_id = ? " +
//							" AND " + 
//									" t.type_id = 39 " + // CUP_WITHDRAW
//							" AND " +
//									" (t.status_id = 4 or t.status_id = 9) " + 
//							" AND " +
//									" t.selector_id > ? " +           
//					")"; 	
//	
//				ps = con.prepareStatement(sql);
//	
//				ps.setLong(1, userId);
//				if(countBeforeCup) { // selector_id = -1 are transactions before CUP
//					ps.setLong(2, -2); // include transactions before CUP
//				} else { 
//					ps.setLong(2, -1); // exclude transactions before CUP
//				}
//				ps.setLong(3, userId);
//				if(countBeforeCup) {
//					ps.setLong(4, -2);
//				} else { 
//					ps.setLong(4, -1);
//				};
//				
//	
//				rs = ps.executeQuery();
//	
//				if(rs.next()) {
//					return rs.getLong("available_to_withdraw");
//				}
//	
//			} finally {
//				closeResultSet(rs);
//				closeStatement(ps);
//			}
//			return 0;
//		}
//	
//	public static long getFirstTransactionId(Connection con, long userId) throws SQLException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		
//		try {
//			String sql = 
//						" SELECT " +
//							" u.first_deposit_id " +
//						" FROM " +
//							" users u " +
//						" WHERE " +
//							" u.id = ? ";
//				           
//			ps = con.prepareStatement(sql);
//			ps.setLong(1, userId);
//			rs = ps.executeQuery();
//			if (rs.next()) {
//				return rs.getLong("first_deposit_id");
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return 0;	
//	}
//
//	public static boolean isFirstDepositWithCC(Connection con,long userId, long creditCardId) throws SQLException {
//	
//		  PreparedStatement ps = null;
//		  ResultSet rs = null;
//	
//		  try {
//			    String sql =" SELECT " +
//			    					"COUNT(*) num" +
//			    			" FROM " +
//			    			 		" transactions t, transaction_types tt " +
//			    			" WHERE " +
//			    			 		" t.type_id = tt.id and t.user_id = ?" +
//	  			 		" AND " +
//			    			 		" tt.class_type = ?" +
//			    			" AND " +
//			    			 		" t.credit_card_id = ? " +
//			    			" AND " +
//			    			 		" t.status_id in ("+ TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES +") ";
//	
//				ps = con.prepareStatement(sql);
//				ps.setLong(1, userId);
//				ps.setLong(2, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
//				ps.setLong(3, creditCardId);
//	
//				rs = ps.executeQuery();
//	
//				if (rs.next()) {
//					long num = rs.getLong("num");
//					return num == 1;
//				}
//	
//				return true;
//			}
//	
//			finally {
//				closeResultSet(rs);
//				closeStatement(ps);
//			}
//	}
//
//	/**
//	 * Method that returns the last user deposit
//	 * 
//	 * @param con
//	 * @param userId
//	 * @return the last user deposit
//	 * @throws SQLException
//	 */
//	public static Transaction getLastUserDeposit(Connection con, long userId) throws SQLException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		try {
//			String sql = "select t.*" +
//		    			 "from transactions t, transaction_types tt " +
//		    			 "where t.type_id = tt.id and t.user_id = ? and "+
//		    			 "tt.class_type = ? AND "+
//		    			 "t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
//		    			 "order by id desc";
//			ps = con.prepareStatement(sql);
//			ps.setLong(1, userId);
//			ps.setLong(2, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
//			rs = ps.executeQuery();
//			if (rs.next()) {
//				return getVO(con, rs);
//			} else {
//				log.warn("No transaction found returning empty transaction");
//				return new Transaction();
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//	}
//
//	public static boolean isInatecNotNotifiedTrx(Connection con, long id) throws SQLException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//
//		try {
//			String sql =
//				"SELECT " +
//					"id " +
//				"FROM " +
//					"transactions " +
//				"WHERE " +
//					"type_id = ? AND " +
//					"status_id = ? AND " +
//					"id = ? ";
//
//			ps = con.prepareStatement(sql);
//			ps.setLong(1, TransactionsManagerBase.TRANS_TYPE_DIRECT_BANK_DEPOSIT);
//			ps.setLong(2, TransactionsManagerBase.TRANS_STATUS_AUTHENTICATE);
//			ps.setLong(3, id);
//			rs = ps.executeQuery();
//
//			if (rs.next()) {
//				return true;
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return false;
//	}
//
//	public static void updatePurchase(Connection conn, long transactionId, long statusId, String description, String comments,
//										String purchaseAuthNumber, String utcOffset, long processedWriterId) throws SQLException {
//		PreparedStatement pstmt = null;
//		try {
//			String sql =
//	                "UPDATE " +
//	                    "transactions " +
//	                "SET " +
//	                    "status_id = ?, " +
//	                    "description = ?, " +
//	                    "comments = comments || ?, " +
//	                    "xor_id_capture = ? ";
//
//			if (statusId == TransactionsManagerBase.TRANS_STATUS_SUCCEED || statusId == TransactionsManagerBase.TRANS_STATUS_FAILED) {
//				sql += ",time_settled = sysdate, " +
//                 	   "utc_offset_settled = ?, " +
//                 	   "processed_writer_id = ? ";
//			}
//			sql += "WHERE " +
//	                    "id = ?";
//			pstmt = conn.prepareStatement(sql);
//			pstmt.setLong(1, statusId);
//			pstmt.setString(2, description);
//			pstmt.setString(3, comments);
//			pstmt.setString(4, purchaseAuthNumber);
//			if (statusId == TransactionsManagerBase.TRANS_STATUS_SUCCEED || statusId == TransactionsManagerBase.TRANS_STATUS_FAILED) {
//				pstmt.setString(5, utcOffset);
//				pstmt.setLong(6, processedWriterId);
//				pstmt.setLong(7, transactionId);
//			} else {
//				pstmt.setLong(5, transactionId);
//			}
//			pstmt.executeUpdate();
//		} finally {
//			closeStatement(pstmt);
//		}
//	}
//}