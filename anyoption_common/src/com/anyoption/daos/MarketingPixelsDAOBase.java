package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.beans.MarketingAffiliatePixels;
import com.anyoption.beans.MarketingCombinationPixels;
import com.anyoption.beans.MarketingPixel;
import com.anyoption.common.daos.DAOBase;

public class MarketingPixelsDAOBase extends DAOBase {


		  /**
		   * Get VO
		   * @param rs
		   * 	Result set instance
		   * @return
		   * 	MarketingPixel object
		   * @throws SQLException
		   */
		  protected static MarketingPixel getVO(ResultSet rs) throws SQLException {
			  MarketingPixel vo = new MarketingPixel();
			  vo.setId(rs.getLong("id"));
			  vo.setHttpCode(rs.getString("http_code"));
			  vo.setHttpsCode(rs.getString("https_code"));
			  vo.setWriterId(rs.getLong("writer_id"));
			  vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
			  vo.setName(rs.getString("name"));
			  return vo;
		  }

		  /**
		   * Get combination pixels by combinationId
		   * @param con
		   * @param combId
		   * @return
		   * @throws SQLException
		   */
		  public static MarketingCombinationPixels getPixelsByCombId(Connection con, long combId) throws SQLException {
			  PreparedStatement ps = null;
		      ResultSet rs = null;
		      MarketingCombinationPixels mp = new MarketingCombinationPixels();
		      ArrayList<MarketingPixel> pixels = new ArrayList<MarketingPixel>();
		      try{
		            String sql =
		            	"SELECT " +
		            		"mcp.id mcp_id, " +
		            		"mcp.combination_id comb_id, " +
		            		"mp.id p_id, " +
		            		"mp.http_code, " +
		            		"mp.https_code, " +
		            		"mp.type_id p_type_id, " +
		            		"mp.name " +
		            	"FROM " +
		            		"marketing_combination_pixels mcp, " +
		            		"marketing_pixels mp " +
		            	"WHERE " +
		            		"mcp.combination_id = ? AND " +
		            		"mcp.pixel_id = mp.id ";

		            ps = con.prepareStatement(sql);
		            ps.setLong(1, combId);
		            rs = ps.executeQuery();
		            while (rs.next()) {
		            	mp.setId(rs.getLong("mcp_id"));
		            	mp.setCombId(rs.getLong("comb_id"));
		            	MarketingPixel p = new MarketingPixel();
						p.setId(rs.getLong("p_id"));
						p.setHttpCode(rs.getString("http_code"));
						p.setHttpsCode(rs.getString("https_code"));
						p.setTypeId(rs.getLong("p_type_id"));
						p.setName(rs.getString("name"));
						pixels.add(p);
		            }
		            mp.setPixels(pixels);
		      } finally {
		            closeStatement(ps);
		            closeResultSet(rs);
		      }
		      return mp;
		  }
		  
	  /**
	   * Get affiliate pixels by affiliateId
	   * @param con
	   * @param affiliateId
	   * @return
	   * @throws SQLException
	   */
	  public static MarketingAffiliatePixels getPixelsByAffiliateId(Connection con, long affiliateId) throws SQLException {
		  PreparedStatement ps = null;
	      ResultSet rs = null;
	      MarketingAffiliatePixels mp = new MarketingAffiliatePixels();
	      ArrayList<MarketingPixel> pixels = new ArrayList<MarketingPixel>();
	      try{
	            String sql =
	            	"SELECT " +
	            		"map.id map_id, " +
	            		"map.affiliate_id affiliate_id, " +
	            		"mp.id p_id, " +
	            		"mp.http_code, " +
	            		"mp.https_code, " +
	            		"mp.type_id p_type_id, " +
	            		"mp.name " +
	            	"FROM " +
	            		"marketing_affilate_pixels map, " +
	            		"marketing_pixels mp " +
	            	"WHERE " +
	            		"map.affiliate_id = ? AND " +
	            		"map.pixel_id = mp.id ";

	            ps = con.prepareStatement(sql);
	            ps.setLong(1, affiliateId);
	            rs = ps.executeQuery();
	            while (rs.next()) {
	            	mp.setId(rs.getLong("map_id"));
	            	mp.setAffiliateId(rs.getLong("affiliate_id"));
	            	MarketingPixel p = new MarketingPixel();
					p.setId(rs.getLong("p_id"));
					p.setHttpCode(rs.getString("http_code"));
					p.setHttpsCode(rs.getString("https_code"));
					p.setTypeId(rs.getLong("p_type_id"));
					p.setName(rs.getString("name"));
					pixels.add(p);
	            }
	            mp.setPixels(pixels);
	      } finally {
	            closeStatement(ps);
	            closeResultSet(rs);
	      }
	      return mp;
	  }
}