package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.anyoption.common.beans.Skin;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.util.ConstantsBase;

public class SkinsDAOBase extends com.anyoption.common.daos.SkinsDAOBase {


	/**
	 * Return Skins instance by id
	 *
	 * @param con connection to db
	 * @param id skin id
	 * @return Skin object
	 * @throws SQLException
	 */
	public static Skin getById(Connection con, long id) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Skin skin = null;
		try {
			String sql =
				"SELECT " +
				    "* " +
				"FROM " +
				    "skins " +
				"WHERE " +
				    "id = ?";
			ps = con.prepareStatement(sql);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			if (rs.next()) {
				skin = getVO(rs);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return skin;
	}

	/**
	 * Returns a Skin name by id
	 *
	 * @param con DB connection
	 * @param id Skin id
	 * @return Skin name
	 * @throws SQLException
	 */
	public static String getNameById(Connection con, int id) throws SQLException {
		String name = ConstantsBase.EMPTY_STRING;
		Statement ps = null;
		ResultSet rs = null;
		try {
			String sql =
			    "SELECT " +
			        "name " +
			     "FROM " +
			         "skins " +
			     "WHERE " +
			         "id = " + id;
			ps = con.createStatement();
			rs = ps.executeQuery(sql);
			if (rs.next()) {
				name = rs.getString("name");
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return name;
	}

    /**
     * Get skin_id and isRegulated by country_id and url_id
     *
     * @param con connection to Db
     * @param countryId country id
     * @param urlId url id
     * @return skin id
     * @throws SQLException
     */
    public static ArrayList<String> getSkinIdIsRegulatedByCountryAndUrl(Connection con, long countryId, int urlId) throws SQLException {
    	ArrayList<String> list = new ArrayList<String>();
    	PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "sucm.skin_id, " +
                    "s.is_regulated, " +
                    "s.default_currency_id, " +
                    "s.skin_group_id, " +
                    "c.is_left_symbol, " +
                    "c.symbol " +
                "FROM " +
                    "skin_url_country_map sucm, " +
                    "skins s, " +
                    "currencies c " +
                "WHERE " +
                    "sucm.country_id = ? AND " +
                    "sucm.url_id = ? AND " +
                    "sucm.skin_id = s.id AND " +
                    "s.default_currency_id = c.id";
            ps = con.prepareStatement(sql);
            ps.setLong(1, countryId);
            ps.setInt(2, urlId);
            rs = ps.executeQuery();
            if (rs.next()) {
            	list.add(Long.toString(rs.getLong("skin_id")));
            	list.add(Boolean.toString(rs.getBoolean("is_regulated")));
            	list.add(Long.toString(rs.getLong("default_currency_id")));
            	list.add(Long.toString(rs.getLong("skin_group_id")));
            	list.add(Boolean.toString(rs.getBoolean("is_left_symbol")));
            	list.add(rs.getString("symbol"));
            }
        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
        return list;
    }

    /**
	 * This method returns the combination id of netrrefer for this skin
	 *
	 * @return Netrefer Combination Id
	 * @throws SQLException
	 */
	public static ArrayList<Long> getNetreferCombinationsId(Connection con, long skinId)
			throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Long> list = new ArrayList<Long>();
		String skinIds = CommonUtil.getArrayToString(SkinsManagerBase.getRegulatedAndNonSkinIdsBySkinId(skinId));
		try {
			String sql = "SELECT " +
							"* " +
						"FROM " +
							"marketing_combinations " +
						"WHERE " +
							"skin_id in (" + skinIds + ") AND " +
							"campaign_id = ? " +
						"ORDER BY " +
							"id ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, ConstantsBase.NETREFER_CAMPAIGN_ID);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(rs.getLong("id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
	
	/**
	 * This method returns the combination id of Referpartner  for this skin
	 *
	 * @return Referpartner  Combination Id
	 * @throws SQLException
	 */
	public static ArrayList<Long> getReferpartnerCombinationsId(Connection con, long skinId)
			throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Long> list = new ArrayList<Long>();
		String skinIds = CommonUtil.getArrayToString(SkinsManagerBase.getRegulatedAndNonSkinIdsBySkinId(skinId));
		try {
			String sql = "SELECT " +
							"* " +
						"FROM " +
							"marketing_combinations " +
						"WHERE " +
							"skin_id in (" + skinIds + ") AND " +
							"campaign_id = ? " +
						"ORDER BY " +
							"id ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, ConstantsBase.REFERPARTNER_CAMPAIGN_ID);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(rs.getLong("id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
}