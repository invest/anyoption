package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import com.anyoption.beans.InvestmentLimit;
import com.anyoption.beans.Limit;
import com.anyoption.managers.TransactionsManagerBase;
import com.anyoption.util.ConstantsBase;

public class LimitsDAOBase extends com.anyoption.common.daos.LimitsDAOBase {
    /**
     * Get default limit id by skin id and currency id
     * 
     * @param con Db connection
     * @param skinId skin id for mapping
     * @param currencyId currency id for mapping
     * @return limit id
     * @throws SQLException
     */
    public static long getIdBySkinAndCurrency(Connection con, long skinId, long currencyId) throws SQLException {
        long limitId = 0;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "limit_id " +
                "FROM " +
                    "skin_currencies " +
                "WHERE " +
                    "skin_id = ? AND " +
                    "currency_id = ? AND " +
                    "is_default = 1";
            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, skinId);
            pstmt.setLong(2, currencyId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                limitId = rs.getLong("limit_id");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return limitId;
    }

    public static Limit getById(Connection conn, long id) throws SQLException {
        Limit l = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "l.*, " +
                    "w.user_name writer " +
                "FROM " +
                    "limits l, " +
                    "writers w " +
                "WHERE " +
                    "l.id = ? AND " +
                    "l.writer_id = w.id";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                l = getVO(rs);
                l.setWriterName(rs.getString("writer"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
    }
    
    public static Limit getBySkinAndCurrency(Connection conn,
    											long skinId,
    											long currencyId) throws SQLException {
    	Limit limit = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql = " SELECT l.*, "
							+ "  w.user_name writer "
							+ " FROM limits l, "
							+ "  writers w "
							+ " WHERE l.id= "
							+ "  (SELECT limit_id "
							+ "  FROM skin_currencies "
							+ "  WHERE skin_id   = ? "
							+ "  AND currency_id = ? "
							+ "  AND is_default  = 1) "
							+ " AND l.writer_id=w.id ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, skinId);
            pstmt.setLong(2, currencyId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                limit = getVO(rs);
                limit.setWriterName(rs.getString("writer"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return limit;
    }

    private static Limit getVO(ResultSet rs) throws SQLException {
          Limit vo = new Limit();
          vo.setId(rs.getLong("id"));
          vo.setName(rs.getString("name"));
          vo.setMinDeposit(rs.getLong("MIN_DEPOSIT"));
          vo.setMinWithdraw(rs.getLong("MIN_WITHDRAW"));
          vo.setMaxDeposit(rs.getLong("MAX_DEPOSIT"));
          vo.setMaxWithdraw(rs.getLong("MAX_WITHDRAW"));
          vo.setMaxDepositPerDay(rs.getLong("MAX_DEPOSIT_PER_DAY"));
          vo.setMaxDepositPerMonth(rs.getLong("MAX_DEPOSIT_PER_MONTH"));
          vo.setTimeLastUpdate(convertToDate(rs.getTimestamp("TIME_LAST_UPDATE")));
          vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
          vo.setWriterId(rs.getLong("WRITER_ID"));
          vo.setComments(rs.getString("COMMENTS"));
          vo.setMinBankWire(rs.getLong("min_bank_wire"));
          vo.setMaxBankWire(rs.getLong("max_bank_wire"));
          vo.setFreeInvestMin(rs.getLong("free_invest_min"));
          vo.setFreeInvestMax(rs.getLong("free_invest_max"));
          vo.setBankWireFee(rs.getLong("bank_wire_fee"));
          vo.setCcFee(rs.getLong("cc_fee"));
          vo.setChequeFee(rs.getLong("cheque_fee"));
          vo.setDepositDocsLimit1(rs.getLong("deposit_docs_limit1"));
          vo.setDepositDocsLimit2(rs.getLong("deposit_docs_limit2"));
          vo.setMinFirstDeposit(rs.getLong("min_first_deposit"));
          vo.setAmountForLowWithdraw(rs.getLong("amt_for_low_withdraw"));
          return vo;
    }

    /**
     * get deposit limits by limitId
     * 
     * @param limitId user limit id
     * @return
     *      HashMap with deposit limits values
     *          value1: deposit amount for warnning (limit1)
     *          value2: deposit amount for lock account (limit2)
     * @throws SQLException
     */
    public static HashMap<String, Long> getDepositLimits(Connection conn, long limitId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        HashMap<String, Long> limits = new HashMap<String, Long>();
        try {
            String sql =
                "SELECT " +
                    "deposit_docs_limit1," +
                    "deposit_docs_limit2 " +
                "FROM " +
                    "limits " +
                "WHERE " +
                    "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, limitId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                limits.put(ConstantsBase.DEPOSIT_DOCS_LIMIT1, rs.getLong("deposit_docs_limit1"));
                limits.put(ConstantsBase.DEPOSIT_DOCS_LIMIT2, rs.getLong("deposit_docs_limit2"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return limits;
    }
    
    /**
	   * Get fee By limit id and tran type is
	   * @param con
	   * 	Db connection
	   * @param limitId
	   * 	user limit id, for mapping
	   * @param tranTypeId
	   * 	withdraw tranTypeId , for mapping
	   * @return
	   * 	withdrawal fee
	   * @throws SQLException
	   */
     @Deprecated
	  public static long getFeeByTranTypeId(Connection con, long limitId, int tranTypeId) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  long fee = 0;
		  String feeTypeStr = "";

		  // find fee type
		  switch (tranTypeId) {
		  	case TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW:
		  		feeTypeStr = " cc_fee ";
				break;
		  	case TransactionsManagerBase.TRANS_TYPE_DELTAPAY_CHINA_WITHDRAW:
		  		feeTypeStr = " cc_fee ";
		  		break;
		  	case TransactionsManagerBase.TRANS_TYPE_BAROPAY_WITHDRAW:
		  		feeTypeStr = " bank_wire_fee ";
		  		break;
		  	case TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW:
		  		feeTypeStr = " bank_wire_fee ";
				break;
          case TransactionsManagerBase.TRANS_TYPE_EFT_WITHDRAW:
              feeTypeStr = " bank_wire_fee ";
              break;
		  	case TransactionsManagerBase.TRANS_TYPE_CHEQUE_WITHDRAW:
		  		feeTypeStr = " cheque_fee ";
				break;
		  	case TransactionsManagerBase.TRANS_TYPE_PAYPAL_WITHDRAW:
		  		feeTypeStr = " paypal_fee" ;
				break;
		  	case TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW:
		  		feeTypeStr = " envoy_fee ";
				break;
		  	case TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_WITHDRAW:
		  		feeTypeStr = " bank_wire_fee ";
				break;
		  	case TransactionsManagerBase.TRANS_TYPE_WEBMONEY_WITHDRAW:
		  		feeTypeStr = " bank_wire_fee ";
				break;
//		  	case TransactionsManagerBase.TRANS_TYPE_LOW_AMOUNT_WITHDRAW_FEE:
//		  		feeTypeStr = " amt_for_low_withdraw * " + TransactionsManagerBase.TRANS_TYPE_LOW_AMOUNT_WITHDRAW_FEE_COEFFICIENT + " ";
//		  		break;
			default:
				return 0;
		  }

		  try {
			    String sql =
			    	"SELECT " +
			    		feeTypeStr + " fee " +
			    	"FROM " +
			    		"limits " +
			    	"WHERE " +
			    		"id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, limitId);
				rs = ps.executeQuery();

				if (rs.next()) {
					fee = rs.getLong("fee");
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return fee;
	  }
	  
	  /**
	   * get all from limits table
	   * @param con
	   * 	Db connection
	   * @throws SQLException
	   */
	  public static HashMap<Long, Limit> getAll(Connection con) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  HashMap<Long, Limit> list = new HashMap<Long, Limit>();
		  try {
			    String sql = "select l.*, w.user_name writer " +
			    		     "from limits l, writers w " +
			    		     "where l.writer_id = w.id ";
				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();
				while (rs.next()) {
					Limit l = getVO(rs);
					l.setWriterName(rs.getString("writer"));
					list.put(rs.getLong("id"), l);
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
		  }

	public static Limit getBDADepositLimits(Connection con, long currencyId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Limit limit = null;
		try {
			String sql = "select min_deposit, max_deposit from bda_limits where currency_id = ?";
			ps = con.prepareStatement(sql);
			ps.setLong(1, currencyId);
			rs = ps.executeQuery();
			if (rs.next()) {
				limit = new Limit();
				limit.setMinDeposit(rs.getLong("min_deposit"));
				limit.setMaxDeposit(rs.getLong("max_deposit"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return limit;
	}

	public static InvestmentLimit getBDAInvestmentLimit(Connection con, long currencyId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		InvestmentLimit limit = null;
		try {
			String sql = "select min_investment, max_investment from bda_limits where currency_id = ?";
			ps = con.prepareStatement(sql);
			ps.setLong(1, currencyId);
			rs = ps.executeQuery();
			if (rs.next()) {
				limit = new InvestmentLimit();
				limit.setMinAmount(rs.getLong("min_investment"));
				limit.setMaxAmount(rs.getLong("max_investment"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return limit;
	}
}