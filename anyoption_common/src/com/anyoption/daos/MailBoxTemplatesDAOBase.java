package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.beans.base.MailBoxTemplate;
import com.anyoption.common.daos.DAOBase;

/**
 * MailBoxUsersDAOBase
 * @author Kobi
 *
 */
public class MailBoxTemplatesDAOBase extends DAOBase {

	public static MailBoxTemplate getTemplateByTypeSkinLanguage(Connection con, long typeId, long skinId, long languageId) throws SQLException {
		MailBoxTemplate t = new MailBoxTemplate();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try	{
		    String sql =
		    	"SELECT " +
			      "mt.* " +
				"FROM " +
				      "mailbox_templates mt " +
				"WHERE " +
				      "mt.type_id = ? AND " +
				      "mt.skin_id = ? AND " +
				      "mt.language_id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, typeId);
			ps.setLong(2, skinId);
			ps.setLong(3, languageId);
			rs = ps.executeQuery();

			if (rs.next()) {
				t = getVO(rs);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return t;
	}

	public static MailBoxTemplate getVO(ResultSet rs) throws SQLException {
		MailBoxTemplate vo = new MailBoxTemplate();
		vo.setId(rs.getLong("id"));
		vo.setName(rs.getString("name"));
		vo.setSubject(rs.getString("subject"));
		vo.setTypeId(rs.getLong("type_id"));
		vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
		vo.setWriterId(rs.getLong("writer_id"));
		vo.setSenderId(rs.getLong("sender_id"));
		vo.setTemplate(rs.getString("template"));
		vo.setIsHighPriority(rs.getInt("is_high_priority") == 1 ? true : false);
		vo.setSkinId(rs.getLong("skin_id"));
		vo.setLanguageId(rs.getLong("language_id"));
		vo.setPopupTypeId(rs.getLong("popup_type_id"));

		return vo;
	}

}
