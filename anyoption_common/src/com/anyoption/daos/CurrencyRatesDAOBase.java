///**
// *
// */
//package com.anyoption.daos;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.base.LastCurrencyRate;
//import com.anyoption.common.daos.DAOBase;
//import com.anyoption.util.CommonUtil;
//
///**
// * @author pavelhe
// *
// */
//public class CurrencyRatesDAOBase extends DAOBase {
//
//	private static final Logger logger = Logger.getLogger(CurrencyRatesDAOBase.class);
//
//	/**
//	 * this method convert an amount from one currency to an amount in base currency
//	 * @param amount - the amount to convert from
//	 * @param currencyId - the currency to convert from
//	 * @param date - the exchange rate date
//	 * @return the new amount
//	 */
//	public static Double convertToBaseAmount(Connection con,
//												long amount,
//												long currencyId,
//												Date date) throws SQLException {
//		PreparedStatement ps=null;
//		ResultSet rs=null;
//		Double res = new Double(0);
//
//		try {
//			String sql="select convert_amount_to_usd(?,?,?) base_amount from dual";
//			ps = con.prepareStatement(sql);
//
//			ps.setLong(1, amount);
//			ps.setLong(2, currencyId);
//			ps.setTimestamp(3, CommonUtil.convertToTimeStamp((date != null ? date : new Date())));
//
//			rs=ps.executeQuery();
//
//			if (rs.next()) {
//				res = rs.getDouble("base_amount");
//			} else {
//				/* This should never happen! */
//				logger.error("SQL query [" + sql + "] did not return converted amount for given amount ["
//								+ amount + "], currency [" + currencyId + "] and date [" + date + "]");
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//
//		return res;
//	}
//
//	public static Double convertToEuroAmount(Connection con, long amount, long currencyId, Date date) throws SQLException {
//		PreparedStatement ps=null;
//		ResultSet rs=null;
//		Double res = new Double(0);
//
//		try {
//			String sql="select convert_amount_to_eur(?,?,?) base_amount from dual";
//			ps = con.prepareStatement(sql);
//
//			ps.setLong(1, amount);
//			ps.setLong(2, currencyId);
//			ps.setTimestamp(3, CommonUtil.convertToTimeStamp(new Date()));
//
//			rs=ps.executeQuery();
//
//			if (rs.next()) {
//				res = rs.getDouble("base_amount");
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return res;
//	}
//
//	/**
//	 * Get last currencies rate
//	 * For USD/BGN - currently it's not on currencies table so we will take it "hard coded"
//	 * @param con
//	 * @return
//	 * @throws SQLException
//	 * @throws ParseException
//	 */
//	public static ArrayList<LastCurrencyRate> getLastCurrenciesRate(Connection con)  throws SQLException, ParseException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		ArrayList<LastCurrencyRate> list = new ArrayList<LastCurrencyRate>();
//		try {
//						// Get all currencies rate
//			String sql =" SELECT " +
//						"	max_last_level.code, " +
//						"   max_last_level.market_id, " +
//						"	to_char(SYS_EXTRACT_UTC( max_last_level.time_created), 'YYYY-MM-DD HH24:MI:SS.FF3')  time_created, " +
//						"   CASE WHEN is_multiply_to_convert(max_last_level.market_id) <> 0 THEN ll2.closing_level ELSE 1/ll2.closing_level END rate " +
//						" FROM " +
//						"	(SELECT " +
//						"	    market_id, " +
//						"       MAX(time_est_closing) time_created, " +
//						"       c.code " +
//						"	 FROM " +
//						"       last_levels ll, " +
//						"       markets m, " +
//						"       currencies c " +
//						"    WHERE " +
//						"       m.id = ll.market_id " +
//						"       AND c.code || '=' = m.feed_name " +
//						"    GROUP BY  " +
//						"       market_id, " +
//						"       c.code " +
//						"   ) max_last_level " +
//						"   LEFT JOIN last_levels ll2 ON max_last_level.market_id = ll2.market_id AND max_last_level.time_created = ll2.time_est_closing " +
//						"									" +
//						// Get USD/BGN - currently is not on currencies table
//						"	UNION " +
//						"									" +
//						"	SELECT " +
//						"	  'BGN', " +
//						"      max_last_level.market_id, " +
//						"	   to_char(SYS_EXTRACT_UTC( max_last_level.time_created), 'YYYY-MM-DD HH24:MI:SS.FF3')  time_created, " +
//						"      CASE WHEN is_multiply_to_convert(max_last_level.market_id) <> 0  THEN ll2.closing_level ELSE 1/ll2.closing_level END rate " +
//						"	FROM " +
//						"    (SELECT " +
//						"	     market_id, " +
//						"        MAX(time_est_closing) time_created " +
//						"     FROM " +
//						"        last_levels ll " +
//						"     WHERE " +
//						"        ll.market_id = 651 " + // USD/BGN market_id
//						"	  GROUP BY " +
//						"        market_id " +
//						"     ) max_last_level " +
//						"       LEFT JOIN last_levels ll2 ON " +
//						"       max_last_level.market_id = ll2.market_id " +
//						"       AND max_last_level.time_created = ll2.time_est_closing ";
//			ps = con.prepareStatement(sql);
//			rs = ps.executeQuery();
//			SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
//			while (rs.next()) {
//				LastCurrencyRate currencyRate = new LastCurrencyRate();
//				currencyRate.setRate(rs.getDouble("rate"));
//				currencyRate.setTimeCreated(localDateFormat.parse(rs.getString("time_created")));
//				currencyRate.setTimeCreatedStr(rs.getString("time_created"));
//				currencyRate.setCurrencyCode(rs.getString("code"));
//				list.add(currencyRate);
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return list;
//	}
//
//
//}
