///**
// * 
// */
//package com.anyoption.daos;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//import com.anyoption.common.beans.base.AssetIndexBase;
//import com.anyoption.common.daos.DAOBase;
///**
// * @author AviadH
// *
// */
//public class AssetIndexDaoBase extends DAOBase{
//	  public static AssetIndexBase getAssetyByMarket(Connection con, long skinId, long marketId) throws SQLException {
//
//		  PreparedStatement ps = null;
//		  ResultSet rs = null;
//		  AssetIndexBase vo = null;
//
//		  try {
//	        String sql = "SELECT " +
//	                        "ai.*, m.feed_name, sm.market_group_id, ot.time_zone, m.option_plus_market_id, m.exchange_id " +
//	                     "FROM " +
//	                        "asset_index ai, " +
//	                        "skin_market_groups sm, " +
//	                        "skin_market_group_markets smg, " +
//	                        "markets m, " +
//	                        "opportunity_templates ot " +
//	                     "WHERE " +
//	                        "sm.id = smg.skin_market_group_id AND " +
//	                        "sm.skin_id = ? AND " +
//	                        "ai.market_id = ? AND " +
//	                        "smg.market_id = ai.market_id AND " +
//	                        "m.id = ai.market_id AND " +
//	                        "m.id = ot.market_id ";
//
//	        ps = con.prepareStatement(sql);
//	        ps.setLong(1, skinId);
//	        ps.setLong(2, marketId);
//	        rs = ps.executeQuery();
//
//	        if (rs.next()) {
//	        	vo = new AssetIndexBase();
//	        	vo.setId(rs.getLong("market_id"));
//	        	vo.setTradingDays(rs.getString("trading_days"));
//	        	if(null != rs.getString("friday_time")) {
//	        		vo.setFridayTime(rs.getString("friday_time"));
//	            }
//	            vo.setStartTime(rs.getString("start_time"));
//	            vo.setEndTime(rs.getString("end_time"));
//	            vo.setFullDay(rs.getInt("IS_FULL_DAY") == 1 ? true : false);
//	            vo.setMarketGroupId(rs.getLong("market_group_id"));
//	            vo.setFeedName(rs.getString("feed_name"));
//	            vo.setTimeZoneString(rs.getString("time_zone"));
//	            vo.setOptionPlusMarketId(rs.getLong("option_plus_market_id"));
//	            vo.setExchangeId(rs.getLong("exchange_id"));
//	        }
//		  } finally {
//	          closeResultSet(rs);
//	          closeStatement(ps);
//		  }
//
//		return vo;
//	  }
//
//}
