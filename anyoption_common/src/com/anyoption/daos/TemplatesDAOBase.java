package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.bl_vos.Template;
import com.anyoption.common.daos.DAOBase;

public class TemplatesDAOBase extends DAOBase {
    public static Template getById(Connection con, long id) throws SQLException {
        Template vo = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "templates " +
                "WHERE " +
                    "id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                vo = getVO(rs);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return vo;
    }

    private static Template getVO(ResultSet rs) throws SQLException {
        Template vo = new Template();
        vo.setId(rs.getLong("id"));
        vo.setName(rs.getString("name"));
        vo.setSubject(rs.getString("subject"));
        vo.setFileName(rs.getString("file_name"));
        vo.setDisplayed(rs.getBoolean("is_displayed"));
        vo.setMailBoxEmailType(rs.getLong("mailbox_email_type"));
        return vo;
    }
}