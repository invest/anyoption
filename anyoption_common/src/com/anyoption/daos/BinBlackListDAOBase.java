package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.daos.DAOBase;

public class BinBlackListDAOBase extends DAOBase {
    public static boolean isBinBlacklisted(Connection conn, String bin, boolean deposit, long skinId) throws SQLException {
        boolean b = false;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "id " +
                "FROM " +
                    "bin_black_List " +
                "WHERE " +
                    "from_bin <= ? AND " +
                    "to_bin >= ? AND " +
                    "is_active = 1 AND " +
                    "is_deposit = ? AND " +
                    "skin_id = ? ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, bin);
            pstmt.setString(2, bin);
            pstmt.setBoolean(3, deposit);
            pstmt.setLong(4 , skinId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                b = true;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return b;
    }
}