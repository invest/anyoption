//package com.anyoption.daos;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.Hashtable;
//
//import com.anyoption.beans.State;
//import com.anyoption.common.beans.Country;
//import com.anyoption.common.daos.DAOBase;
//import com.anyoption.common.enums.CountryStatus;
//import com.anyoption.util.ConstantsBase;
//
//public class CountriesDAOBase extends DAOBase {
//    /**
//     * Get all countries
//     *
//     * @param con connection to Db
//     * @return <code>Hashtable<Long, Country></code>
//     * @throws SQLException
//     */
//    public static Hashtable<Long, Country> getAll(Connection conn) throws SQLException {
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        Hashtable<Long, Country> ht = new Hashtable<Long, Country>();
//        try {
//            String sql =
//                "SELECT " +
//                    "* " +
//                "FROM " +
//                    "countries " +
//                " WHERE " +
//                    " id != "+ ConstantsBase.COUNTRY_NORTH_KOREA_ID +
//                " ORDER BY " +
//                    "country_name";
//            pstmt = conn.prepareStatement(sql);
//            rs = pstmt.executeQuery();
//            while (rs.next()) {
//                ht.put(rs.getLong("id"), getVO(rs));
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(pstmt);
//        }
//        return ht;
//    }
//    
//    public static Hashtable<Long, Country> getAllAllowedCountries(Connection conn) throws SQLException {
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        Hashtable<Long, Country> ht = new Hashtable<Long, Country>();
//        try {
//            String sql =
//                "SELECT " +
//                    "* " +
//                "FROM " +
//                    "countries " +
//                " WHERE " +
//                    " id not in (" + ConstantsBase.COUNTRY_NORTH_KOREA_ID + "," + ConstantsBase.COUNTRY_ID_IL + ") " +
//                    " AND is_blocked = 0 " +
//                " ORDER BY " +
//                    "country_name";
//            pstmt = conn.prepareStatement(sql);
//            rs = pstmt.executeQuery();
//            while (rs.next()) {
//                ht.put(rs.getLong("id"), getVO(rs));
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(pstmt);
//        }
//        return ht;
//    }
//
//	public static ArrayList<State> getStates(Connection con) throws SQLException{
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		ArrayList<State> list = new ArrayList<State>();
//		try {
//			String sql =
//				"SELECT " +
//					"* " +
//				"FROM " +
//					"states " +
//				"ORDER BY " +
//					"state_name";
//			ps = con.prepareStatement(sql);
//			rs = ps.executeQuery();
//			while(rs.next()){
//				list.add(new State(rs.getLong("id"), rs.getString("state_name")));
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return list;
//	}
//
//    /**
//     * Fill Country object
//     *
//     * @param rs ResultSet
//     * @return <code>Country</code> object
//     * @throws SQLException
//     */
//    protected static Country getVO(ResultSet rs) throws SQLException {
//        Country vo = new Country();
//        vo.setId(rs.getLong("id"));
//        vo.setName(rs.getString("country_name"));
//        vo.setA2(rs.getString("a2"));
//        vo.setA3(rs.getString("a3"));
//        vo.setPhoneCode(rs.getString("phone_code"));
//        vo.setDisplayName(rs.getString("display_name"));
//        vo.setSupportPhone(rs.getString("support_phone"));
//        vo.setSupportFax(rs.getString("support_fax"));
//        vo.setGmtOffset(rs.getString("gmt_offset"));
//        vo.setCapitalCity(rs.getString("CAPITAL_CITY"));
//        vo.setCapitalCityLatitude(rs.getString("CAPITAL_CITY_LATITUDE"));
//        vo.setCapitalCityLongitude(rs.getString("CAPITAL_CITY_LONGITUDE"));        
//        vo.setRegulated(rs.getLong("is_regulated") == 1 ? true : false);
//        vo.setCountryStatus(CountryStatus.getById(rs.getLong("is_blocked")));
//        return vo;
//    }
//
//    /**
//	 * Get id by country_code
//	 * @param con
//	 * 		connrction to Db
//	 * @param code
//	 * 		country code
//	 * @return
//	 * 		id of the country
//	 * @throws SQLException
//	 */
//	public static long getCountryIdByCountryCode(Connection con, String code) throws SQLException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		long id = 0;
//
//		try
//		{
//		    String sql = "SELECT " +
//    					 	"id " +
//		    			 "FROM " +
//		    			 	"countries " +
//		    		     "WHERE " +
//		    		     	"a2 like ? ";
//
//		    ps = con.prepareStatement(sql);
//		    ps.setString(1, code);
//			rs = ps.executeQuery();
//			
//			if ( rs.next() ) {
//				id = rs.getLong("id");
//			}
//		} finally {
//			closeStatement(ps);
//			closeResultSet(rs);
//		}
//		return id;
//	}
//
//	/**
//	 * Get skin_id by country_id and url_id
//	 * @param con
//	 * 		connection to Db
//	 * @param countryId
//	 * 		country id
//	 * @param urlId
//	 * 		url id
//	 * @return
//	 * 		skin id
//	 * @throws SQLException
//	 */
//	public static int getSkinIdByCountryId(Connection con, long countryId) throws SQLException {
//
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		int skinId = 0;
//
//		try
//		{
//		    String sql = "SELECT " +
//		    			 	"skin_id " +
//		    			 "FROM " +
//		    			 	"skin_url_country_map "+
//		    		     "WHERE " +
//		    		     	"country_id = ? " +
//		    		     	"AND url_id = ? ";
//
//		    ps = con.prepareStatement(sql);
//			ps.setLong(1, countryId);
//			ps.setInt(2, ConstantsBase.ANYOPTION_URL);
//
//			rs = ps.executeQuery();
//
//			if ( rs.next() ) {
//				skinId = rs.getInt("skin_id");
//			}
//
//		} finally {
//			closeStatement(ps);
//			closeResultSet(rs);
//		}
//
//		return skinId;
//	}
//
//}