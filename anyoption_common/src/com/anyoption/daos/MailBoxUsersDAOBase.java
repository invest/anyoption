package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.anyoption.common.beans.base.MailBoxUser;
import com.anyoption.common.daos.DAOBase;

/**
 * MailBoxUsersDAOBase
 * @author Kobi
 *
 */
public class MailBoxUsersDAOBase extends DAOBase {
	/**
	 * insert email to user
	 * @param con db connection
	 * @throws SQLException
	 */
	public static void insert(Connection con, MailBoxUser email) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql =
				"INSERT INTO " +
					  "mailbox_users " +
					  		"(id, " +
					  		"template_id, " +
					  		"user_id, " +
					  		"status_id, " +
					  		"time_created, " +
					  		"writer_id, " +
					  		"free_text, " +
					  		"sender_id, " +
					  		"is_high_priority, " +
					  		"subject," +
					  		"bonus_user_id," +
					  		"popup_type_id," +
					  		"transaction_id, " +
					  		"template_parameters, " +
					  		"attachment_id) " +
					"VALUES" +
                            "(SEQ_MAILBOX_USERS.nextval,?,?,?,sysdate,?,?,?,?,?,?,?,?,?,?)";

			ps = con.prepareStatement(sql);
			ps.setLong(1, email.getTemplateId());
			ps.setLong(2, email.getUserId());
			ps.setLong(3, MailBoxUser.MAILBOX_STATUS_NEW);
			ps.setLong(4, email.getWriterId());
			ps.setString(5, email.getFreeText());
			ps.setLong(6, email.getSenderId());
			ps.setLong(7, email.getIsHighPriority() ? 1 : 0);
			ps.setString(8, email.getSubject());
			if (email.getBonusUserId() != 0) {
				ps.setLong(9, email.getBonusUserId());
			} else {
				ps.setString(9, null);
			}
			ps.setLong(10, email.getPopupTypeId());
			if (email.getTransactionId() != 0) {
				ps.setLong(11, email.getTransactionId());
			} else {
				ps.setString(11, null);
			}
			ps.setString(12, email.getParameters());
			if (null != email.getAttachmentId()) {
				ps.setLong(13, email.getAttachmentId());
			} else {
				ps.setString(13, null);
			}
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
}