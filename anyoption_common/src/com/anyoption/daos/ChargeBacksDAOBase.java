//package com.anyoption.daos;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//import com.anyoption.common.bl_vos.ChargeBack;
//import com.anyoption.common.daos.DAOBase;
//
//public class ChargeBacksDAOBase extends DAOBase {
//    public static ChargeBack getById(Connection conn, long id) throws SQLException {
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        try {
//            String sql = 
//                "SELECT " +
//                    "c.*, " +
//                    "w.user_name writer, " +
//                    "cs.name status_name " +
//                "FROM " +
//                    "charge_backs c, " +
//                    "writers w, "+
//                    "charge_back_statuses cs " +
//                "WHERE " +
//                    "c.id = ? AND " +
//                    "c.writer_id = w.id AND " +
//                    "c.status_id = cs.id";
//            pstmt = conn.prepareStatement(sql);
//            pstmt.setLong(1, id);
//            rs = pstmt.executeQuery();
//            if (rs.next()) {
//                ChargeBack cb = getVO(rs);
//                cb.setWriter(rs.getString("writer"));
//                cb.setStatusName(rs.getString("status_name"));
//                return cb;
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(pstmt);
//        }
//        return null;
//    }
//
//    private static ChargeBack getVO(ResultSet rs) throws SQLException {
//        ChargeBack vo = new ChargeBack();
//        vo.setId(rs.getLong("id"));
//        vo.setDescription(rs.getString("description"));
//        vo.setStatusId(rs.getLong("status_id"));
//        vo.setWriterId(rs.getLong("writer_id"));
//        vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
//        vo.setUtcOffsetCreated(rs.getString("utc_offset_created"));
//        return vo;
//    }
//}