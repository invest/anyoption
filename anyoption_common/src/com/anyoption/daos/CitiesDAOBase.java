/**
 * 
 */
package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.beans.base.City;
import com.anyoption.common.daos.DAOBase;

/**
 * @author AviadH
 *
 */
public class CitiesDAOBase extends DAOBase {
	
	  public static ArrayList<City> getAll(Connection con) throws SQLException {

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  ArrayList<City> list=new ArrayList<City>();

		  try {
			   //for anyoption: CITIES.ID=0 is a AO city
			    String sql="SELECT " +
			    				"* " +
			    			"FROM " +
			    				"Cities " +
			    			"WHERE " +
			    				"id<>0 " +
			    			"ORDER by name";

				ps = con.prepareStatement(sql);

				rs=ps.executeQuery();

				while (rs.next()) {
					City vo=new City();
					vo.setId(rs.getLong("id"));
					vo.setName(rs.getString("name"));
					list.add(vo);
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;

	  }

}
