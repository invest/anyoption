package com.anyoption.clearing;

import java.sql.SQLException;

import com.anyoption.beans.User;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.bl_vos.WireBase;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.util.ConstantsBase;

/**
 * Envoy clearing info class
 *
 * @author Eliran
 */
public class EnvoyInfo extends ClearingInfo {
    private final String MERCHANT_REFERENCE = "AYP";

    // for Envoy
	private String redirectUrl;
    private String merchantId;
    private String customerRef;
    private String language;
    private String amountStr;
    private String requestReference;
    private String xmlString;

    // withdraw info
	private WireBase account;

	// Notification info
	private String uniqueReference;
	private String epacsReference;
	private int notificationType;
	private String userOffset;

	// PayIn notification info
	private String postingDate;
	private String bankCurrency;
	private String bankAmount;
	private String appliedCurrency;
	private String appliedAmount;
	private String bankInformation;

	public EnvoyInfo() {
    }

	public EnvoyInfo(User user, Transaction t, int source) throws SQLException{
    	super(user, t.getAmount(), t.getTypeId(), t.getId(), t.getIp(), t.getAcquirerResponseId(), source);
        customerRef = MERCHANT_REFERENCE + currencySymbol.substring(0, 2) + getReferencedUserId();
        language = LanguagesManagerBase.getLanguage(user.getLanguageId()).getCode();
        userOffset = user.getUtcOffset();
    }

	/**
	 * @return the appliedAmount
	 */
	public String getAppliedAmount() {
		return appliedAmount;
	}

	/**
	 * @param appliedAmount the appliedAmount to set
	 */
	public void setAppliedAmount(String appliedAmount) {
		this.appliedAmount = appliedAmount;
	}

	/**
	 * @return the appliedCurrency
	 */
	public String getAppliedCurrency() {
		return appliedCurrency;
	}

	/**
	 * @param appliedCurrency the appliedCurrency to set
	 */
	public void setAppliedCurrency(String appliedCurrency) {
		this.appliedCurrency = appliedCurrency;
	}

	/**
	 * @return the bankAmount
	 */
	public String getBankAmount() {
		return bankAmount;
	}

	/**
	 * @param bankAmount the bankAmount to set
	 */
	public void setBankAmount(String bankAmount) {
		this.bankAmount = bankAmount;
	}

	/**
	 * @return the bankCurrency
	 */
	public String getBankCurrency() {
		return bankCurrency;
	}

	/**
	 * @param bankCurrency the bankCurrency to set
	 */
	public void setBankCurrency(String bankCurrency) {
		this.bankCurrency = bankCurrency;
	}

	/**
	 * @return the bankInformation
	 */
	public String getBankInformation() {
		return bankInformation;
	}

	/**
	 * @param bankInformation the bankInformation to set
	 */
	public void setBankInformation(String bankInformation) {
		this.bankInformation = bankInformation;
	}

	/**
	 * @return the epacsReference
	 */
	public String getEpacsReference() {
		return epacsReference;
	}

	/**
	 * @param epacsReference the epacsReference to set
	 */
	public void setEpacsReference(String epacsReference) {
		this.epacsReference = epacsReference;
	}

	/**
	 * @return the postingDate
	 */
	public String getPostingDate() {
		return postingDate;
	}

	/**
	 * @param postingDate the postingDate to set
	 */
	public void setPostingDate(String postingDate) {
		this.postingDate = postingDate;
	}

	/**
	 * @return the uniqueReference
	 */
	public String getUniqueReference() {
		return uniqueReference;
	}

	/**
	 * @param uniqueReference the uniqueReference to set
	 */
	public void setUniqueReference(String uniqueReference) {
		this.uniqueReference = uniqueReference;
	}

	/**
	 * @return the redirectUrl
	 */
	public String getRedirectUrl() {
		return redirectUrl;
	}
	/**
	 * @param redirectUrl the redirectUrl to set
	 */
	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	/**
	 * @return the customerRef
	 */
	public String getCustomerRef() {
		return customerRef;
	}

	/**
	 * @param customerRef the customerRef to set
	 */
	public void setCustomerRef(String customerRef) {
		this.customerRef = customerRef;
	}

	/**
	 * @return the merchantId
	 */
	public String getMerchantId() {
		return merchantId;
	}

	/**
	 * @param merchantId the merchantId to set
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getReferencedUserId(){
		String userIdString = String.valueOf(userId);
		while (userIdString.length() < ConstantsBase.USER_ID_REFERENCE_DIGITS_NUMBER){
			userIdString = "0" + userIdString;
		}
		return userIdString;
	}

	/**
	 * @return the account
	 */
	public WireBase getAccount() {
		return account;
	}

	/**
	 * @param account the account to set
	 */
	public void setAccount(WireBase account) {
		this.account = account;
	}

	/**
	 * @return the withdarwAmount
	 */
	public String getAmountStr() {
		return amountStr;
	}

	/**
	 * @param withdarwAmount the withdarwAmount to set
	 */
	public void setAmountStr(String withdarwAmount) {
		this.amountStr = withdarwAmount;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the requestReference
	 */
	public String getRequestReference() {
		return requestReference;
	}

	/**
	 * @param requestReference the requestReference to set
	 */
	public void setRequestReference(String requestReference) {
		this.requestReference = requestReference;
	}

	/**
	 * @return the xmlString
	 */
	public String getXmlString() {
		return xmlString;
	}

	/**
	 * @param xmlString the xmlString to set
	 */
	public void setXmlString(String xmlString) {
		this.xmlString = xmlString;
	}

	/**
	 * @return the notificationType
	 */
	public int getNotificationType() {
		return notificationType;
	}

	/**
	 * @param notificationType the notificationType to set
	 */
	public void setNotificationType(int notificationType) {
		this.notificationType = notificationType;
	}

	/**
	 * @return the userOffset
	 */
	public String getUserOffset() {
		return userOffset;
	}

	/**
	 * @param userOffset the userOffset to set
	 */
	public void setUserOffset(String userOffset) {
		this.userOffset = userOffset;
	}

	public String toString() {
	    String ls = System.getProperty("line.separator");
	    return ls + "EnvoyInfo:" + ls +
	        super.toString() +
	        "email: " + email + ls +
	        "redirectUrl: " + redirectUrl + ls;
	}
}