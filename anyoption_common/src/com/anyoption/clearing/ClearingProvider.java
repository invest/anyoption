//package com.anyoption.clearing;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.clearing.ClearingException;
//import com.anyoption.common.clearing.ClearingInfo;
//import com.anyoption.common.clearing.ClearingProviderConfig;
//
///**
// * Base abstract class to be extended for clearing providers so the rest of the logic
// * don't depend on specific clearing provider.
// *
// * @author Tony
// */
//public abstract class ClearingProvider {
//    private static final Logger log = Logger.getLogger(ClearingProvider.class);
//    
//    protected long id;
//    protected String depositTerminalId;
//    protected String withdrawTerminalId;
//    protected String url;
//    protected String username;
//    protected String password;
//    protected String name;
//    protected long nonCftAvailable;
//    protected String privateKey;
//    protected static final String STATUS_SUCCESSFUL = "A0";
//    
//    /**
//     * Process an "authorize" call through this provider.
//     * 
//     * @param info thransaction info
//     * @throws ClearingException
//     */
//    public abstract void authorize(ClearingInfo info) throws ClearingException;
//
//    /**
//     * Process an "capture" call through this provider.
//     * 
//     * @param info thransaction info
//     * @throws ClearingException
//     */
//    public abstract void capture(ClearingInfo info) throws ClearingException;
//
//    /**
//     * Process a "enroll" request through this provider. Enroll is the
//     * request that checks through hosted MPI if certain card has 3D
//     * security enrolled. A Merchant Plug In (MPI) is a software module
//     * which provides a communication interface between the merchant and
//     * the Visa or MasterCard directory servers. Hosted MPI is when the
//     * clearing provider also provide MPI to be used through the communication
//     * interface with it (and no need to be installed on merchant side).
//     * 
//     * @param info transaction info
//     * @throws ClearingException
//     */
//    public abstract void enroll(ClearingInfo info) throws ClearingException;
//    
//    /**
//     * Process a "purchase" call through this provider. Purchase is a direct
//     * one step debit of the card (compared to authorize and capture 2 steps). 
//     * 
//     * @param info transaction info
//     * @throws ClearingException
//     */
//    public abstract void purchase(ClearingInfo info) throws ClearingException;
//    
//    /**
//     * Process a "withdraw" call through this provider.
//     * 
//     * @param info
//     * @throws ClearingException
//     */
//    public abstract void withdraw(ClearingInfo info) throws ClearingException;
//    
//    /**
//     * Process a "bookback" call through this provider. This is refund money
//     * for previous successful transaction over the same provider.
//     * 
//     * @param info
//     * @throws ClearingException
//     */
//    public abstract void bookback(ClearingInfo info) throws ClearingException;
//    
//    /**
//     * @return <code>true</code> if this provider support 3D enrollment else
//     *      <code>false</code>.
//     */
//    public abstract boolean isSupport3DEnrollement();
//    
//    /**
//     * Set clearing provider configuration.
//     *
//     * @param key
//     * @param props
//     * @throws ClearingException
//     */
//    public void setConfig(ClearingProviderConfig config) throws ClearingException {
//        id = config.getId();
//        url = config.getUrl();
//        String[] terminals = config.getTerminalId().split("_");
//        depositTerminalId = terminals[0];
//        if (terminals.length > 1) {
//            withdrawTerminalId = terminals[1];
//        }
//        username = config.getUserName();
//        password = config.getPassword();
//        name = config.getName();
//        nonCftAvailable = config.getNonCftAvailable();
//
//        if (log.isInfoEnabled()) {
//            String ls = System.getProperty("line.separator");
//            log.info(getClass().getName() + " configured with:" + ls +
//                    "id: " + id + ls +
//                    "url: " + url + ls +
//                    "depositTerminalId: " + depositTerminalId + ls +
//                    "withdrawTerminalId: " + withdrawTerminalId + ls +
//                    "username: " + username + ls +
//                    "name: " + name + ls +
//                    "privateKey: " + privateKey + ls);
//        }
//    }
//
//    public long getId() {
//        return id;
//    }
//
//    public String getName() {
//        return name;
//    }
//    
//	public long getNonCftAvailable() {
//		return nonCftAvailable;
//	}
//
//	public abstract void cancel(ClearingInfo info) throws ClearingException  ;
//
//	public String getUrl() {
//		return url;
//	}
//
//	public void setUrl(String url) {
//		this.url = url;
//	}
//}