//package com.anyoption.clearing;
//
//import com.anyoption.common.beans.User;
//import com.anyoption.common.beans.Transaction;
//import com.anyoption.common.bl_vos.WireBase;
//import com.anyoption.common.clearing.ClearingInfo;
//
///**
// * Inatec clearing info class
// * 
// * @author Kobi
// */
//public class InatecInfo extends ClearingInfo {
//    // for Inatec
//    private String zipCode;
//    private String city;
//    private String bankCode;
//    private String accountNum;
//    private String streetNo;
//    private String redirectSecret;    // pass bu Inatec in the first response
//    private String redirectUrl;
//    private long inatecPaymentType;    // Inatec provider id
//    private boolean needNotification;
//    private String accountHolder;
//
//    public InatecInfo() {
//    }
//
//    public InatecInfo(User user, Transaction t, WireBase wire, int source) {
//    	super(user, t.getAmount(), t.getTypeId(), t.getId(), t.getIp(), t.getAcquirerResponseId(), source);
//        zipCode = user.getZipCode();
//        city = user.getCityName();
//        streetNo = user.getStreetNo();
//    	bankCode = wire.getBankCode();
//    	accountNum = wire.getAccountNum();
//    	accountHolder = wire.getBeneficiaryName();
//    	inatecPaymentType = t.getPaymentTypeId();
//    }
//    
//	/**
//	 * @return the accountNum
//	 */
//	public String getAccountNum() {
//		return accountNum;
//	}
//	
//	/**
//	 * @param accountNum the accountNum to set
//	 */
//	public void setAccountNum(String accountNum) {
//		this.accountNum = accountNum;
//	}
//	
//	/**
//	 * @return the bankCode
//	 */
//	public String getBankCode() {
//		return bankCode;
//	}
//	
//	/**
//	 * @param bankCode the bankCode to set
//	 */
//	public void setBankCode(String bankCode) {
//		this.bankCode = bankCode;
//	}
//	
//	/**
//	 * @return the city
//	 */
//	public String getCity() {
//		return city;
//	}
//	
//	/**
//	 * @param city the city to set
//	 */
//	public void setCity(String city) {
//		this.city = city;
//	}
//
//	/**
//	 * @return the redirectSecret
//	 */
//	public String getRedirectSecret() {
//		return redirectSecret;
//	}
//	
//	/**
//	 * @param redirectSecret the redirectSecret to set
//	 */
//	public void setRedirectSecret(String redirectSecret) {
//		this.redirectSecret = redirectSecret;
//	}
//	
//	/**
//	 * @return the redirectUrl
//	 */
//	public String getRedirectUrl() {
//		return redirectUrl;
//	}
//	
//	/**
//	 * @param redirectUrl the redirectUrl to set
//	 */
//	public void setRedirectUrl(String redirectUrl) {
//		this.redirectUrl = redirectUrl;
//	}
//	
//	/**
//	 * @return the streetNo
//	 */
//	public String getStreetNo() {
//		return streetNo;
//	}
//	
//	/**
//	 * @param streetNo the streetNo to set
//	 */
//	public void setStreetNo(String streetNo) {
//		this.streetNo = streetNo;
//	}
//	
//	/**
//	 * @return the zipCode
//	 */
//	public String getZipCode() {
//		return zipCode;
//	}
//	
//	/**
//	 * @param zipCode the zipCode to set
//	 */
//	public void setZipCode(String zipCode) {
//		this.zipCode = zipCode;
//	}
//
//	/**
//	 * @return the inatecPaymentType
//	 */
//	public long getInatecPaymentType() {
//		return inatecPaymentType;
//	}
//
//	/**
//	 * @param inatecPaymentType the inatecPaymentType to set
//	 */
//	public void setInatecPaymentType(long inatecPaymentType) {
//		this.inatecPaymentType = inatecPaymentType;
//	}
//
//	/**
//	 * @return the needNotification
//	 */
//	public boolean isNeedNotification() {
//		return needNotification;
//	}
//
//	/**
//	 * @param needNotification the needNotification to set
//	 */
//	public void setNeedNotification(boolean needNotification) {
//		this.needNotification = needNotification;
//	}
//
//	/**
//	 * @return the accountHolder
//	 */
//	public String getAccountHolder() {
//		return accountHolder;
//	}
//
//	/**
//	 * @param accountHolder the accountHolder to set
//	 */
//	public void setAccountHolder(String accountHolder) {
//		this.accountHolder = accountHolder;
//	}
//
//	public String toString() {
//	    String ls = System.getProperty("line.separator");
//	    return ls + "InatecInfo:" + ls +
//	        super.toString() +
//	        "inatecPaymentType: " + inatecPaymentType + ls +
//	        "zipCode: " + zipCode + ls +
//	        "bankCode: " + bankCode + ls +
//	        "accountNum: " + accountNum + ls +
//	        "accountHolder: " + accountHolder + ls +
//	        "email: " + email + ls +
//	        "streetNo: " + streetNo + ls +
//	        "redirectSecret: " + redirectSecret + ls +
//	        "redirectUrl: " + redirectUrl + ls;
//	}
//}