package com.anyoption.distribution;

import com.anyoption.managers.ClearingManager;

public class MobileSupportSelector implements Selector<Long> {

	private final int SELECTOR_ID = 5;
	
	@Override
	public int getId() {
		return SELECTOR_ID;
	}

	@Override
	public Long select() {
		// currently only Deltapay is supported on mobile
		return ClearingManager.DELTAPAY_CHINA_PROVIDER_ID ;
	}

}
