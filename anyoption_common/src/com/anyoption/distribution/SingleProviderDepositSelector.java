package com.anyoption.distribution;

import java.util.HashMap;
import java.util.Iterator;

public class SingleProviderDepositSelector implements Selector<Long> {
	HashMap<Long, Long> providerAmount;
	Long requestedAmount;
	private final int SELECTOR_ID = 3;
	
	public SingleProviderDepositSelector(HashMap<Long, Long> providerAmount, Long requestedAmount) {
		this.providerAmount = providerAmount;
		this.requestedAmount = requestedAmount;
	}
	
	@Override
	public int getId() {
		return SELECTOR_ID;
	}

	@Override
	public Long select() {
		Iterator<Long> it = providerAmount.keySet().iterator();
		Long providerID = null;
	    while (it.hasNext()) {
	        Long provider	= it.next();
	        Long amount 	= providerAmount.get(provider);
	        if(requestedAmount<amount) {
	        	if(providerID == null) {
	        		providerID = provider;
	        	} else {
	        		return null;
	        	}
	        }
	    }

		return providerID;
	}

}
