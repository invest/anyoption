package com.anyoption.distribution;

import java.util.Map;
import java.util.Map.Entry;

public class HundredPercentSelector implements Selector<Long> {
	Map<Long,Integer> providerPercentage;
	private final int SELECTOR_ID = 4;
	
	public HundredPercentSelector(Map<Long,Integer> providerPercentage) {
		this.providerPercentage = providerPercentage;
	}
	
	@Override
	public int getId() {
		return SELECTOR_ID;
	}

	@Override
	public Long select() {
		for (Entry<Long, Integer> entry : providerPercentage.entrySet()) {
			if(entry.getValue() == 100) {
				return entry.getKey();
			}
		}
		return null;
	}

}
