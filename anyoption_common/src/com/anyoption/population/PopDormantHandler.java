package com.anyoption.population;




import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.beans.User;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.anyoption.managers.PopulationsManagerBase;
import com.anyoption.managers.UsersManagerBase;

public class PopDormantHandler extends PopulationHandlerBase {
	private static final Logger log = Logger.getLogger(PopDormantHandler.class);


	/**
	 * deposit event handler implementation
	 */
	@Override
	public void deposit(Connection con, PopulationEntryBase popUserEntry, boolean successful, long writerId, Transaction tran, User user) throws PopulationHandlersException {
		if (!successful){
			PopulationsManagerBase.insertIntoDeclinePop(con, user, popUserEntry, false, writerId, tran.getDescription());
		} else {
			try {
				PopulationsManagerBase.insertIntoPopulation(user.getContactId(), 0, user.getId(), user.getUserName(), 0, PopulationsManagerBase.POP_TYPE_RETENTION, popUserEntry);
				UsersManagerBase.updateUserRankIfNeeded(user.getId(), tran.getId(), user.getRankId());
				UsersManagerBase.updateUserStatusIfNeeded(	user.getId(), user.getStatusId(), user.getBalance(), tran.getId(), null, null,
															null);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}	
	}

	/**
	 * invest event handler implementation
	 */
	@Override
	public void invest(PopulationEntryBase popUserEntry, boolean successful, long writerId) throws PopulationHandlersException {
		log.debug("event event Process, popEntryId: " + popUserEntry.getCurrEntryId());
		delete(null, popUserEntry, writerId, PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_QUALIFICATION, 0,false);
	}

	/**
	 * user creation event handler implementation
	 */
	@Override
	public void userCreation(Connection con, PopulationEntryBase popUserEntry, long writerId) throws PopulationHandlersException {
		//	do nothing
	}

	/**
	 * reached call event handler implementation
	 */
	@Override
	public void reachedCall(Connection con, PopulationEntryBase popUserEntry, long writerId, User user) throws PopulationHandlersException {
		//	do nothing
	}
}