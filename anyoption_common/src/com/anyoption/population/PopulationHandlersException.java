package com.anyoption.population;

/**
 * populationHandlerException class
 * @author Kobi
 *
 */
public class PopulationHandlersException extends Exception {
    private static final long serialVersionUID = 1L;

    public PopulationHandlersException() {
        super();
    }

    public PopulationHandlersException(String message) {
        super(message);
    }

    public PopulationHandlersException(String message, Throwable t) {
        super(message, t);
    }
}