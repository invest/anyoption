package com.anyoption.population;



import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.beans.User;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.managers.PopulationsManagerBase;
import com.anyoption.managers.UsersManagerBase;

public class PopRetentionHandler extends PopulationHandlerBase {

	@Override
	public void deposit(Connection con, PopulationEntryBase popUserEntry, boolean successful, long writerId, Transaction tran, User user) throws PopulationHandlersException {
		if (!successful){
			PopulationsManagerBase.insertIntoDeclinePop(con, user, popUserEntry, false, writerId, tran.getDescription());
		} else {
			try {
				long statusId = user.getStatusId();
				if (CommonUtil.isHasRetentionDepartmentId(popUserEntry.getAssignWriterId(), ConstantsBase.SALES_TYPE_DEPARTMENT_COMA_RETENTION) && statusId == UsersManagerBase.USER_STATUS_COMA) {
		        	PopulationsManagerBase.insertIntoPopulation(0, 0, user.getId(), user.getUserName(), 0, PopulationsManagerBase.POP_TYPE_RETENTION, popUserEntry);
				}
				UsersManagerBase.updateUserRankIfNeeded(user.getId(), tran.getId(), user.getRankId());
				UsersManagerBase.updateUserStatusIfNeeded(user.getId(), statusId, user.getBalance(), tran.getId(), null, null, null);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
	}

	@Override
	public void invest(PopulationEntryBase popUserEntry, boolean successful,
			long writerId) throws PopulationHandlersException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void userCreation(Connection con, PopulationEntryBase popUserEntry,
			long writerId) throws PopulationHandlersException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reachedCall(Connection con, PopulationEntryBase popUserEntry,
			long writerId, User user) throws PopulationHandlersException {
		// TODO Auto-generated method stub
		
	}

}
