package com.anyoption.population;


import java.sql.Connection;

import org.apache.log4j.Logger;

import com.anyoption.beans.User;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.anyoption.managers.PopulationsManagerBase;

public class PopNoXDepositHandler extends PopulationHandlerBase {
	private static final Logger log = Logger.getLogger(PopNoXDepositHandler.class);

	/**
	 * deposit event handler implementation
	 */
	@Override
	public void deposit(Connection con, PopulationEntryBase popUserEntry, boolean successful, long writerId, Transaction tran, User user) throws PopulationHandlersException {
		if(successful) {
			log.debug("deposit event Process, popEntryId: " + popUserEntry.getCurrEntryId());
			delete(con, popUserEntry, writerId, PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_QUALIFICATION, 0,false);
		}else{
			PopulationsManagerBase.insertIntoDeclinePop(con, user, popUserEntry, true, writerId, tran.getDescription());
		}
	}

	/**
	 * invest event handler implementation
	 */
	@Override
	public void invest(PopulationEntryBase popUserEntry, boolean successful, long writerId) throws PopulationHandlersException {
		if(successful) {
			log.debug("invest event Process, popEntryId: " + popUserEntry.getCurrEntryId());
			delete(null, popUserEntry, writerId, PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_QUALIFICATION, 0,false);
		}
	}

	/**
	 * user creation event handler implementation
	 */
	@Override
	public void userCreation(Connection con, PopulationEntryBase popUserEntry, long writerId) throws PopulationHandlersException {
		//	do nothing
	}

	/**
	 * reached call event handler implementation
	 */
	@Override
	public void reachedCall(Connection con, PopulationEntryBase popUserEntry, long writerId, User user) throws PopulationHandlersException {
		//	do nothing
	}
}