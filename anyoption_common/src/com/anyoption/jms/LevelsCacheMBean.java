//package com.anyoption.jms;
//
///**
// * MBean interface implemented by the LevelsCache to alow state monitoring over JMX.
// * 
// * @author Tony
// */
//public interface LevelsCacheMBean {
//    /**
//     * @return <code>true</code> if the communication with the service working else <code>false</code>.
//     *      This check depends on the heartbeat logic. The idea of the heartbeats is the clients (LevelsCache)
//     *      to know that service is available and the channel from it to the client is ok (in case no updates
//     *      are present at the moment).
//     */
//    public boolean isJMSConnectionOK();
//
//    /**
//     * @return The size of the sender thread internal queue.
//     */
//    public int getSenderQueueSize();
//}