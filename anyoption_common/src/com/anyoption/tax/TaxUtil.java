package com.anyoption.tax;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * TaxUtil class.
 * This class represent some data that needed for tax job
 *
 * @author Kobi
 */

public class TaxUtil {

	private static Logger log = Logger.getLogger(TaxUtil.class);

    // constants for tax_job
    public static final String TAX_JOB_PERIOD1 = "1";
    public static final String TAX_JOB_PERIOD2 = "2";

    // current tax year
    public static final GregorianCalendar CURRENT_YEAR = new GregorianCalendar();
    public static final long TAX_JOB_YEAR_LONG = CURRENT_YEAR.get(GregorianCalendar.YEAR);
    public static final long NEXT_YEAR = TAX_JOB_YEAR_LONG + 1;
    public static final String TAX_JOB_YEAR = String.valueOf(CURRENT_YEAR.get(GregorianCalendar.YEAR));

    public static final String TAX_JOB_PERIOD1_START_PART = "0101";
    //public static final String TAX_JOB_PERIOD1_START = TAX_JOB_PERIOD1_START_PART+TAX_JOB_YEAR;

    public static final String TAX_JOB_PERIOD1_END_PART = "3006";
    //public static final String TAX_JOB_PERIOD1_END_PART = "0107";
    //public static final String TAX_JOB_PERIOD1_END = TAX_JOB_PERIOD1_END_PART+TAX_JOB_YEAR;

    public static final String TAX_JOB_PERIOD2_START_PART = "0101";
    //public static final String TAX_JOB_PERIOD2_START = TAX_JOB_PERIOD2_START_PART+TAX_JOB_YEAR;
    public static final String TAX_JOB_PERIOD2_END_PART = "3112";
    //public static final String TAX_JOB_PERIOD2_END = TAX_JOB_PERIOD2_END_PART+TAX_JOB_YEAR;

    public static final String TAX_JOB_PERIOD2_FROM = "0107";

    public static final String TAX_JOB_PERIOD1_END_PART_MONTH = "07";
    public static final String TAX_JOB_PERIOD1_START_PART_MONTH = "01";

    public static final int ZERO_TAX_BALANCE = 0;
    public static final int NO_EXEMPTION = 0;

    public static final int TAX_JOB_WRITER_ID = 29;

    public static final int KEY_VALUE = 555;

    public static final String DB_URL = "tax.db.url";
    public static final String DB_USER = "tax.db.user";
    public static final String DB_PASS = "tax.db.pass";

    // Offset constants
    public static final String GMT_2_OFFSET_DELTA = "+2/24-2/1440";
    public static final String GMT_3_OFFSET_DELTA = "+3/24-2/1440";
    public static final int GMT_2_TO_3_FROM_MONTH = 4;
    public static final int GMT_2_TO_3_TO_MONTH = 9;


    /*  connection for testing
       public static Connection getConnection() throws SQLException {
     	 DriverManager.registerDriver(new OracleDriver());
     	 return DriverManager.getConnection("jdbc:oracle:thin:@dbtest.etrader.co.il:1521:etrader",
     			"etrader","etrader");
   }*/


    /**
     *
     * @param
     * 		con the connection to DB
     * @param seq
     * 		the sequence that need to increase
     * @return
     * 		the next value of this sequence
     * @throws SQLException
     */
    public static long getSeqNextValue(Connection con,String seq)throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try
			{
			  	ps = con.prepareStatement("select "+seq+".NEXTVAL from dual");

				rs = ps.executeQuery();

				if (rs.next()) {

					return rs.getLong(1);
				}

			}

			finally
			{

				try {
            		rs.close();
            	} catch (Exception e) {
            		log.log(Level.ERROR, "Can't close", e);
            	}
            	try {
            		ps.close();
            	} catch (Exception e) {
            		log.log(Level.ERROR, "Can't close", e);
            	}
			}

			return 0;
	  }

}
