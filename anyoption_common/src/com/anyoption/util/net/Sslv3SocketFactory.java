package com.anyoption.util.net;


import java.io.IOException;
import java.net.Socket;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;

import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.params.HttpParams;
 
/**
 * An {@link SSLSocketFactory} supporting only SSLv3 sockets.
 */
public class Sslv3SocketFactory extends SSLSocketFactory {
 
	/**
	 * Instantiates a new {@link Sslv3SocketFactory}.
	 * 
	 * @param sslContext The SSLContext.
	 * @param hostnameVerifier The X509HostnameVerifier.
	 */
	public Sslv3SocketFactory(SSLContext sslContext, X509HostnameVerifier hostnameVerifier) {
		super(sslContext, hostnameVerifier);
	}
 
	@Override
	public Socket createSocket(HttpParams params) throws IOException {
 		SSLSocket socket = (SSLSocket) super.createSocket(params);
 		socket.setEnabledProtocols(new String[] { "SSLv3"});
 		return socket;
	}
}