package com.anyoption.util;

import java.io.File;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.jfree.util.Log;

import com.anyoption.common.beans.base.Skin;
import com.anyoption.managers.SkinsManagerBase;

public class SendTemplateEmail extends com.anyoption.common.util.SendTemplateEmail  {
    public static final Logger log = Logger.getLogger(SendTemplateEmail.class);

    // The email parameters that we need
    public static final String PARAM_USER_ID = "userId";
    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_USER_FIRST_NAME = "userFirstName";
    public static final String PARAM_USER_LAST_NAME = "userLastName";
    public static final String PARAM_USER_NAME = "userName";
    public static final String PARAM_ACCOUNT_ID = "accountId";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_USER_BIRTH_DAY = "userBirthDay";
    public static final String PARAM_USER_ADDRESS = "userAddress";
    public static final String PARAM_USER_ADDRESS_STREET = "userAddressStreet";
    public static final String PARAM_USER_ADDRESS_NUMBER = "userAddressNumber";
    public static final String PARAM_USER_ADDRESS_CITY = "userAddressCity";
    public static final String PARAM_USER_ADDRESS_ZIP_CODE = "userAddressZipCode";
    public static final String PARAM_WRITER_FIRST_NAME = "writerFirstName";
    public static final String PARAM_WRITER_LAST_NAME = "writerLastName";
    public static final String PARAM_WRITER_FULL_NAME = "writerFullName";
    public static final String PARAM_USER_BALANCE = "userBalance";
    public static final String PARAM_USER_TAX_BALANCE = "userTaxBalance";
    public static final String PARAM_MOBILE_PHONE = "mobilePhone";
    public static final String PARAM_TAX = "tax";
    public static final String PARAM_WIN = "win";
    public static final String PARAM_LOSE = "lose";
    public static final String PARAM_SUM_INVESTMENTS = "sumInvest";
    public static final String PARAM_ID_NUMBER = "userIdNum";
    public static final String PARAM_SUPPORT_PHONE ="supportPhone";
    public static final String PARAM_SUPPORT_FAX ="supportFax";
    public static final String PARAM_BONUS_AMOUNT ="bonusAmount";
    public static final String PARAM_TRANSACTION_AMOUNT = "transactionAmount";
    public static final String PARAM_TRANSACTION_PAN = "transactionPan";
    public static final String PARAM_TRANSACTION_BANK_NAME = "transactionBankName";
    public static final String PARAM_DATE_DMMYYYY = "dateDDMMYYYY";
    public static final String PARAM_DATE_MMMMMDYYYY = "dateMMMMMDYYYY";
    public static final String PARAM_GENDER_DESC_EN_OR_HE = "genderTxt";
    public static final String PARAM_GENDER_DESC = "genderDes";
	public static final String PARAM_PAYPAL_EMAIL = "paypalEmail";
	public static final String PARAM_RESET_PASSWORD_LINK = "resetLink";
	public static final String PARAM_CANCEL_WITHDRAWL_DAYS = "cancelWithdrawalDays";
	public static final String PARAM_BONUS_EXP_DAYS = "bonusExpDays";
	public static final String PARAM_AUTHENTICATION_LINK = "authenticationLink";
    public static final String PARAM_DOWNLOAD_LINK = "downloadLink";
    public static final String PARAM_RECEIPT_NUM = "receiptNum";
	public static final String PARAM_DATE = "date";
	public static final String PARAM_AMOUNT = "amount";
	public static final String PARAM_FOOTER = "footer";
	public static final String PARAM_BANK_TRANSFER_DETAILS = "bankTransferDetails";
	public static final String PARAM_ACTIVATION_LINK = "activationLink";
	public static final String PARAM_LOSS_TRESHOLD = "lossThreshold";
    public static final String PARAM_TEMPLATE_ACTIVATION_LOW_GROUP_MAIL = "Activation_questionnaire.html";
    public static final String PARAM_TEMPLATE_ACTIVATION_TRESHOLD_MAIL = "Activation_loss_threshold.html";
	public static final String PARAM_SUPPORT_MAIL_ET = "Support.team@etrader.co.il";
	public static final String PARAM_MISSING_DOCUMENTS = "missingDocuments";
	   

    private static Template emailTemplate;
    private static Hashtable<String, String> serverProperties;
    private static Hashtable<String, String> emailProperties;
	private File [] attachments;
    

    /**
     * @param request
     * @throws Exception - if template not found
     */
    public SendTemplateEmail(HashMap<String, String> params, String subject, String fileName, String lang, long skinId,
    		Object att, long recipientUserWriterId, int platformId) throws Exception {
        if (Log.isDebugEnabled()) {
            log.debug("Trying to send email with the following params: " + params);
        }

		if (null != att) {
			attachments = (File[])att;
		} else {
			attachments = null;
		}

        init(subject, fileName, lang, skinId, recipientUserWriterId, platformId);

        // set the specific email properties
        emailProperties.put("to", params.get(PARAM_EMAIL));
        emailProperties.put("body", getEmailBody(params));

    }

    /**
     * SendTemplateEmail with input properties file.
     *
     * @param request
     * @throws Exception if template not found
     */
    public SendTemplateEmail(HashMap<String, String> params, String subject, String fileName, String lang,
    		long skinId, Properties properties, long recipientUserWriterId, int platformId) throws Exception {
        if (log.isDebugEnabled()) {
            log.debug("Trying to send email with the following params: " + params);
        }
        attachments = null;
        init(subject, fileName, lang, skinId, recipientUserWriterId, platformId);
        // set the specific email properties
        emailProperties.put("to", params.get(PARAM_EMAIL));
        emailProperties.put("body", getEmailBody(params));
    }

    private void init(String subject, String name, String lang, long skinId, long recipientUserWriterId, int platformId) throws Exception {
        // get the emailTemplate
        String templatePath = CommonUtil.getConfig("templates.path");
        emailTemplate = getEmailTemplate(name, lang, skinId, templatePath, recipientUserWriterId, platformId);

        // set the server properties
        serverProperties = new Hashtable<String, String>();
        serverProperties.put("url", CommonUtil.getConfig("email.server"));
        serverProperties.put("auth", "true");
        serverProperties.put("user", CommonUtil.getConfig("email.uname"));
        serverProperties.put("pass", CommonUtil.getConfig("email.pass"));

        // Set the email properties
        emailProperties = new Hashtable<String, String>();
        emailProperties.put("subject", subject);

        emailProperties.put("from", SkinsManagerBase.getSupportEmailBySkinId(skinId));
        if (skinId != Skin.SKIN_KOREAN && skinId != Skin.SKIN_CHINESE) {
        	if (lang.equals(ConstantsBase.ETRADER_LOCALE)) {
        		if(name.equalsIgnoreCase(PARAM_TEMPLATE_ACTIVATION_LOW_GROUP_MAIL) ||name.equalsIgnoreCase(PARAM_TEMPLATE_ACTIVATION_TRESHOLD_MAIL)) {
        			emailProperties.put("from", PARAM_SUPPORT_MAIL_ET);
        		}
        	}
        } 
        
        if(name != null && name.equalsIgnoreCase(ConstantsBase.PARAM_TEMPLATE_PEP_MAIL)){
        	emailProperties.put("from", ConstantsBase.PARAM_COMPLIANCE_MAIL);
        }
        
        updateCopyopSenderAndSubject(emailProperties, recipientUserWriterId, platformId);
    }

//    private void init(String subject, String name, String lang, long skinId, Properties properties) throws Exception {
//        // get the emailTemplate
//        String templatePath = properties.getProperty("templates.path");
//        emailTemplate = getEmailTemplate(name, lang, skinId,templatePath);
//
//        // set the server properties
//        serverProperties = new Hashtable<String, String>();
//        serverProperties.put("url", properties.getProperty("email.server"));
//        serverProperties.put("auth", "true");
//        serverProperties.put("user", properties.getProperty("email.uname"));
//        serverProperties.put("pass", properties.getProperty("email.pass"));
//
//        // Set the email properties
//        emailProperties = new Hashtable<String, String>();
//        emailProperties.put("subject", subject);
//        emailProperties.put("from", properties.getProperty("email.from"));
//    }

    public void run() {
        log.debug("Sending Email is running...");
        CommonUtil.sendEmail(serverProperties, emailProperties, attachments);
        log.debug("Sending Email completed");
    }

    public String getEmailBody(HashMap<String, String> params) throws Exception {
        // set the context and the parameters
        VelocityContext context = new VelocityContext();

        // get all the parameters and add them to the context
        String paramName = "";
        log.debug("Adding password to context");

        for (Iterator<String> keys = params.keySet().iterator(); keys.hasNext();) {
            paramName = keys.next();
            context.put(paramName, params.get(paramName));
        }
        StringWriter sw = new StringWriter();
        emailTemplate.merge(context, sw);
        return sw.toString();
    }
}