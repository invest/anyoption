//package com.anyoption.util;
//
//public class MessageToFormat {
//    protected String key;
//    protected Object[] params;
//    protected int errorMsgType; //when we have to decide whether to display the field or not
//    
//    public static final int ERROR_MSG_DISPLAY_FIELD = 1;
//    public static final int ERROR_MSG_NOT_DISPLAY_FIELD = 2;
//       
//    public MessageToFormat() {
//    	errorMsgType = ERROR_MSG_DISPLAY_FIELD;
//    }
//    
//    public MessageToFormat(String key, Object[] params) {
//        this.key = key;
//        this.params = params;
//    }
//    
//    public MessageToFormat(String key, Object[] params, int errorMsgType) {
//        this.key = key;
//        this.params = params;
//        this.errorMsgType = errorMsgType;
//    }
//    
//    public String getKey() {
//        return key;
//    }
//    
//    public void setKey(String key) {
//        this.key = key;
//    }
//    
//    public Object[] getParams() {
//        return params;
//    }
//    
//    public void setParams(Object[] params) {
//        this.params = params;
//    }
//
//	public int getErrorMsgType() {
//		return errorMsgType;
//	}
//
//	public void setErrorMsgType(int errorMsgType) {
//		this.errorMsgType = errorMsgType;
//	}
//}