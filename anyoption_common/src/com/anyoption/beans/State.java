//package com.anyoption.beans;
//
//import java.sql.SQLException;
//import java.util.ArrayList;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.managers.StatesManagerBase;
//
///**
// * State bean wrapper
// *
// * @author KobiM
// *
// */
//public class State extends com.anyoption.beans.base.State {
//	private static final Logger log = Logger.getLogger(State.class);
//
//	private static ArrayList<State> states;
//
//	public State() {
//		super();
//	}
//
//	public State(long id, String name) {
//		super(id, name);
//	}
//
//	/**
//	 * @return the states
//	 */
//	public static ArrayList<State> getStates() {
//		if (null == states) {
//	        try {
//	            states = StatesManagerBase.getAllStates();
//	        } catch (SQLException sqle) {
//	            log.error("Can't load states.", sqle);
//	        }
//		}
//		return states;
//	}
//
//	public String toString() {
//	    String ls = System.getProperty("line.separator");
//	    return ls + "State" + ls
//	        + super.toString() + ls
//	        + "id: " + id + ls
//	        + "name: " + name;
//	}
//}
