//package com.anyoption.beans;
//
//import java.math.BigDecimal;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.Locale;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.annotations.AnyoptionNoJSON;
//import com.anyoption.common.beans.Exchange;
//import com.anyoption.common.beans.Market;
//import com.anyoption.common.beans.OpportunityOddsType;
//import com.anyoption.common.beans.OpportunityType;
//import com.anyoption.managers.MarketsManagerBase;
//import com.anyoption.util.CommonUtil;
//import com.anyoption.util.ConstantsBase;
//
//public class Opportunity extends com.anyoption.beans.base.Opportunity {
//    private static final Logger log = Logger.getLogger(Opportunity.class);
//
//	public static final int PUBLISHED_YES = 1;
//	public static final int PUBLISHED_NO = 0;
//
//	public static final long TYPE_REGULAR = 1;
//	public static final long TYPE_ONE_TOUCH = 2;
//	public static final long TYPE_OPTION_PLUS = 3;
//	public static final long TYPE_BINARY_0_100_ABOVE = 4;
//	public static final long TYPE_BINARY_0_100_BELOW = 5;
//
//    // 12:00 in turkish time zone
//    public static final String TURKISH_BREAK_START = "12:00";
//    // 14:00 in turkish time zone
//    public static final String TURKISH_BREAK_END = "14:30";
//
//	@AnyoptionNoJSON
//    private Market market;
//	@AnyoptionNoJSON
//	private Date timeCreated;
//	@AnyoptionNoJSON
//	private Date timeActClosing;
//	@AnyoptionNoJSON
//	private Date timeSettled;
//	@AnyoptionNoJSON
//	private long oddsTypeId;
//	@AnyoptionNoJSON
//	private OpportunityOddsType oddsType;
//	@AnyoptionNoJSON
//	private long opportunityTypeId;
//	@AnyoptionNoJSON
//	private boolean disabledService;
//	@AnyoptionNoJSON
//	private boolean disabledTrader;
//	@AnyoptionNoJSON
//	private long writerId;
//	@AnyoptionNoJSON
//	private long exchangeId;
//	@AnyoptionNoJSON
//	private long investmentLimitGroupId;
//	@AnyoptionNoJSON
//	private double shiftParameter; // the value that can be set from the backend to shift the level
//	@AnyoptionNoJSON
//	private double autoShiftParameter;
//	@AnyoptionNoJSON
//	private long winLose;
//	@AnyoptionNoJSON
//	private boolean deductWinOdds;
//	@AnyoptionNoJSON
//	private String opportunityTypeDesc;
//	@AnyoptionNoJSON
//	private String opportunityOddsTypeName;
//
//	// for the LS data adaptor - the exposure calculation amounts
//	@AnyoptionNoJSON
//	private double puts;
//	@AnyoptionNoJSON
//	private double calls;
//	@AnyoptionNoJSON
//	private boolean shifting;
//	@AnyoptionNoJSON
//	private boolean shiftUp;
//	@AnyoptionNoJSON
//	private BigDecimal lastShift;
//	@AnyoptionNoJSON
//	private boolean timeToClose;
//	@AnyoptionNoJSON
//	private Date timeNextOpen;
//
//	/**
//	 * For hour opportunities the level is updated until update for next hour is
//	 * received. This flag should be set to true once an update for the next
//	 * hour is received.
//	 */
//	@AnyoptionNoJSON
//	private boolean closingLevelReceived;
//	@AnyoptionNoJSON
//	private double nextClosingLevel;
//	@AnyoptionNoJSON
//	private boolean notClosedAlertSent;
//
//	// one touch variables
//	@AnyoptionNoJSON
//	private String oneTouchMsg;
//	@AnyoptionNoJSON
//	private long oneTouchUpDown;
//	@AnyoptionNoJSON
//	private boolean isOneTouchUp;
//	@AnyoptionNoJSON
//	protected int maxExposure;
//	@AnyoptionNoJSON
//	protected double gmLevel;
//	@AnyoptionNoJSON
//	protected boolean isOpenGraph;
//	@AnyoptionNoJSON
//	protected long oneTouchOppExposure;
//	@AnyoptionNoJSON
//	protected boolean useManualClosingLevel;  /** if to use manual closing level for this opportunity */
//	@AnyoptionNoJSON
//	protected double manualClosingLevel;  /** manual closing level to use for this opportunity */
//	@AnyoptionNoJSON
//	private OpportunityType type;
//	@AnyoptionNoJSON
//	private Exchange exchange;
//
//
//	public Opportunity() {
//		super();
//		state = 0;
//		timeToClose = false;
//		notClosedAlertSent = false;
//		closingLevelReceived = false;
//	}
//
//	/**
//	 * @return the calls
//	 */
//	public double getCalls() {
//		return calls;
//	}
//
//	/**
//	 * @param calls
//	 *            the calls to set
//	 */
//	public void setCalls(double calls) {
//		this.calls = calls;
//	}
//
//	/**
//	 * @return the exchangeId
//	 */
//	public long getExchangeId() {
//		return exchangeId;
//	}
//
//	/**
//	 * @param exchangeId the exchangeId to set
//	 */
//	public void setExchangeId(long exchangeId) {
//		this.exchangeId = exchangeId;
//	}
//
//	public String getIdTxt() {
//		return Long.toString(this.id);
//	}
//
//	/**
//	 * @return the investmentLimitGroupId
//	 */
//	public long getInvestmentLimitGroupId() {
//		return investmentLimitGroupId;
//	}
//
//	/**
//	 * @param investmentLimitGroupId the investmentLimitGroupId to set
//	 */
//	public void setInvestmentLimitGroupId(long investmentLimitGroupId) {
//		this.investmentLimitGroupId = investmentLimitGroupId;
//	}
//
//	public Market getMarket() {
//        return market;
//    }
//
//    public void setMarket(Market market) {
//        this.market = market;
//    }
//
//    public long getOddsTypeId() {
//		return oddsTypeId;
//	}
//
//	public void setOddsTypeId(long oddsTypeId) {
//		this.oddsTypeId = oddsTypeId;
//	}
//
//	public OpportunityOddsType getOddsType() {
//        return oddsType;
//    }
//
//    public void setOddsType(OpportunityOddsType oddsType) {
//        this.oddsType = oddsType;
//    }
//
//    /**
//	 * @return the puts
//	 */
//	public double getPuts() {
//		return puts;
//	}
//
//	/**
//	 * @param puts the puts to set
//	 */
//	public void setPuts(double puts) {
//		this.puts = puts;
//	}
//
//	public BigDecimal getLastShift() {
//		return lastShift;
//	}
//
//	public void setLastShift(BigDecimal lastShift) {
//		this.lastShift = lastShift;
//	}
//
//    /**
//	 * @return the shifting
//	 */
//	public boolean isShifting() {
//		return shifting;
//	}
//
//	/**
//	 * @param shifting the shifting to set
//	 */
//	public void setShifting(boolean shifting) {
//		this.shifting = shifting;
//	}
//
//	public double getShiftParameter() {
//		return shiftParameter;
//	}
//
//	public void setShiftParameter(double shiftParameter) {
//		this.shiftParameter = shiftParameter;
//	}
//
//	public double getAutoShiftParameter() {
//		return autoShiftParameter;
//	}
//
//	public void setAutoShiftParameter(double autoShiftParameter) {
//		this.autoShiftParameter = autoShiftParameter;
//	}
//
//	/**
//	 * @return the shiftUp
//	 */
//	public boolean isShiftUp() {
//		return shiftUp;
//	}
//
//	/**
//	 * @param shiftUp the shiftUp to set
//	 */
//	public void setShiftUp(boolean shiftUp) {
//		this.shiftUp = shiftUp;
//	}
//
//	/**
//	 * @return <code>true</code> if the state is one that need to receive
//	 *         market updates (from Reuters) else <code>false</code>. State
//	 *         that does not need updates for example is "CREATED".
//	 */
//	public boolean isInOppenedState() {
//		return state >= Opportunity.STATE_OPENED && state <= Opportunity.STATE_CLOSED;
//	}
//
//	/**
//	 * @return the timeCreated
//	 */
//	public Date getTimeCreated() {
//		return timeCreated;
//	}
//
//	/**
//	 * @param timeCreated the timeCreated to set
//	 */
//	public void setTimeCreated(Date timeCreated) {
//		this.timeCreated = timeCreated;
//	}
//
//	/**
//	 * @return the timeActClosing
//	 */
//	public Date getTimeActClosing() {
//		return timeActClosing;
//	}
//
//	/**
//	 * @param timeActClosing the timeActClosing to set
//	 */
//	public void setTimeActClosing(Date timeActClosing) {
//		this.timeActClosing = timeActClosing;
//	}
//
//	/**
//	 * @return the timeNextOpen
//	 */
//	public Date getTimeNextOpen() {
//		return timeNextOpen;
//	}
//
//	/**
//	 * @param timeNextOpen the timeNextOpen to set
//	 */
//	public void setTimeNextOpen(Date timeNextOpen) {
//		this.timeNextOpen = timeNextOpen;
//	}
//
//	/**
//	 * @return the timeSettled
//	 */
//	public Date getTimeSettled() {
//		return timeSettled;
//	}
//
//	/**
//	 * @param timeSettled the timeSettled to set
//	 */
//	public void setTimeSettled(Date timeSettled) {
//		this.timeSettled = timeSettled;
//	}
//
//	public long getWriterId() {
//		return writerId;
//	}
//
//	public void setWriterId(long writerId) {
//		this.writerId = writerId;
//	}
//
//	public long getOpportunityTypeId() {
//		return opportunityTypeId;
//	}
//
//	public void setOpportunityTypeId(long opportunityTypeId) {
//		this.opportunityTypeId = opportunityTypeId;
//	}
//
//	public String getOpportunityOddsTypeName() {
//		return opportunityOddsTypeName;
//	}
//
//	public void setOpportunityOddsTypeName(String opportunityOddsTypeName) {
//		this.opportunityOddsTypeName = opportunityOddsTypeName;
//	}
//
//	public String getOpportunityTypeDesc() {
//		return opportunityTypeDesc;
//	}
//
//	public void setOpportunityTypeDesc(String opportunityTypeDesc) {
//		this.opportunityTypeDesc = opportunityTypeDesc;
//	}
//
//	public boolean isDisabledService() {
//		return disabledService;
//	}
//
//	public void setDisabledService(boolean disabledService) {
//		this.disabledService = disabledService;
//	}
//
//	public boolean isDisabledTrader() {
//		return disabledTrader;
//	}
//
//	public void setDisabledTrader(boolean disabledTrader) {
//		this.disabledTrader = disabledTrader;
//	}
//
//	public boolean isLongTerm() {
//		return scheduled == Opportunity.SCHEDULED_WEEKLY
//				|| scheduled == Opportunity.SCHEDULED_MONTHLY;
//	}
//
//	public boolean isTimeToClose() {
//		return timeToClose;
//	}
//
//	public void setTimeToClose(boolean timeToClose) {
//		this.timeToClose = timeToClose;
//	}
//
//	public boolean isClosingLevelReceived() {
//		return closingLevelReceived;
//	}
//
//	public void setClosingLevelReceived(boolean closingLevelReceived) {
//		this.closingLevelReceived = closingLevelReceived;
//	}
//
//	public double getNextClosingLevel() {
//		return nextClosingLevel;
//	}
//
//	public void setNextClosingLevel(double nextClosingLevel) {
//		this.nextClosingLevel = nextClosingLevel;
//	}
//
//	public boolean isNotClosedAlertSent() {
//		return notClosedAlertSent;
//	}
//
//	public void setNotClosedAlertSent(boolean notClosedAlertSent) {
//		this.notClosedAlertSent = notClosedAlertSent;
//	}
//
//	public boolean isDeductWinOdds() {
//		return deductWinOdds;
//	}
//
//	public void setDeductWinOdds(boolean deductWinOdds) {
//		this.deductWinOdds = deductWinOdds;
//	}
//
//    public float getOverOddsWin() {
//        if (!deductWinOdds) {
//            return oddsType.getOverOddsWin();
//        }
//        long timeToLastInvest = timeLastInvest.getTime() - System.currentTimeMillis();
//        int steps = 0;
//        if (timeToLastInvest < 8 * 60000 && timeToLastInvest >= 6 * 60000) {
//            steps = 1;
//        } else if (timeToLastInvest < 6 * 60000 && timeToLastInvest >= 4 * 60000) {
//            steps = 2;
//        } else if (timeToLastInvest < 4 * 60000 && timeToLastInvest >= 2 * 60000) {
//            steps = 3;
//        } else if (timeToLastInvest < 2 * 60000) {
//            steps = 4;
//        }
//        BigDecimal odds = new BigDecimal(Float.toString(oddsType.getOverOddsWin()));
//        BigDecimal stps = new BigDecimal(steps);
//        BigDecimal deductStep = new BigDecimal(Float.toString(oddsType.getOddsWinDeductStep()));
//        float oow = odds.subtract(stps.multiply(deductStep)).floatValue();
//        if (log.isTraceEnabled()) {
//            log.trace(
//                    "timeToLastInvest: " + timeToLastInvest
//                    + " steps: " + steps
//                    + " odds: " + odds
//                    + " stps: " + stps
//                    + " deductStep: " + deductStep
//                    + " oow: " + oow);
//        }
//        return oow;
//    }
//
//	public long getWinLose() {
//		return winLose;
//	}
//
//	public void setWinLose(long winLose) {
//		this.winLose = winLose;
//	}
//
//	public String getMsg() {
//		return oneTouchMsg;
//	}
//
//	public void setOneTouchMsg(String oneTouchMsg) {
//		this.oneTouchMsg = oneTouchMsg;
//	}
//
//	public long getOneTouchUpDown() {
//		return oneTouchUpDown;
//	}
//
//	public void setOneTouchUpDown(long upDown) {
//		this.oneTouchUpDown = upDown;
//
//		if (this.oneTouchUpDown == 1) {
//			this.isOneTouchUp = true;
//		} else {
//			this.isOneTouchUp = false;
//		}
//	}
//
//	public boolean getIsBooleanOneTouchUpDown() {
//		if (this.oneTouchUpDown == 1) {
//			return true;
//		} else {
//			return false;
//		}
//	}
//
//	public boolean getIsOneTouchUp() {
//		return isOneTouchUp;
//	}
//
//	public void setIsOneTouchUp(boolean isOneTouchUp) {
//		this.isOneTouchUp = isOneTouchUp;
//	}
//
//	public boolean getIsOneTouchOpp() {
//		if (opportunityTypeId == ConstantsBase.OPPORTUNITIES_TYPE_ONE_TOUCH) {
//			return true;
//		}
//		return false;
//	}
//
//	public int getMaxExposure() {
//		return maxExposure;
//	}
//
//	public void setMaxExposure(int maxExposure) {
//		this.maxExposure = maxExposure;
//	}
//
//	/**
//	 * Check if this opportunity is good for current Golden Minutes interval.
//	 *
//	 * @param endOfHour
//	 *            if the current Golden Minutes interval is a end of hour or mid
//	 *            hour one
//	 * @return <code>true</code> if this opp is good for the current GM
//	 *         interval else <code>false</code>
//	 */
//	public boolean isGoodForGoldenMinute(boolean endOfHour) {
//		if (scheduled != SCHEDULED_HOURLY || !isInOppenedState()) {
//			return false;
//		}
//		Calendar c = Calendar.getInstance();
//		c.setTime(timeEstClosing);
//		int min = c.get(Calendar.MINUTE);
//		if ((min == 0 && endOfHour) || (min == 30 && !endOfHour)) {
//			return true;
//		}
//		return false;
//	}
//
//	public double getGmLevel() {
//		return gmLevel;
//	}
//
//	public void setGmLevel(double gmLevel) {
//		this.gmLevel = gmLevel;
//	}
//
//	/**
//	 * @return the isOpenGraph
//	 */
//	public boolean isOpenGraph() {
//		return isOpenGraph;
//	}
//
//	/**
//	 * @param isOpenGraph the isOpenGraph to set
//	 */
//	public void setOpenGraph(boolean isOpenGraph) {
//		this.isOpenGraph = isOpenGraph;
//	}
//
//    /**
//     * Init formatted values before serializing to JSON
//     * @param l
//     */
//	public void initFormattedValues(Locale l, String utcOffset) {
//		Market m = MarketsManagerBase.getMarket(marketId);
//		closeLevelTxt = CommonUtil.formatLevel(closingLevel, m.getDecimalPoint());
//		marketName = CommonUtil.getMessage(l, m.getDisplayNameKey(), null);
//		timeEstClosingTxt = CommonUtil.getDateAndTimeFormat(timeEstClosing, utcOffset);
//	}
//
//    /**
//	 * @return the manualClosingLevel
//	 */
//	public double getManualClosingLevel() {
//		return manualClosingLevel;
//	}
//
//	/**
//	 * @param manualClosingLevel the manualClosingLevel to set
//	 */
//	public void setManualClosingLevel(double manualClosingLevel) {
//		this.manualClosingLevel = manualClosingLevel;
//	}
//
//	/**
//	 * @return the useManualClosingLevel
//	 */
//	public boolean isUseManualClosingLevel() {
//		return useManualClosingLevel;
//	}
//
//	/**
//	 * @param useManualClosingLevel the useManualClosingLevel to set
//	 */
//	public void setUseManualClosingLevel(boolean useManualClosingLevel) {
//		this.useManualClosingLevel = useManualClosingLevel;
//	}
//
//	/**
//	 * @return the oneTouchOppExposure
//	 */
//	public long getOneTouchOppExposure() {
//		return oneTouchOppExposure;
//	}
//
//	/**
//	 * @param oneTouchOppExposure the oneTouchOppExposure to set
//	 */
//	public void setOneTouchOppExposure(long oneTouchOppExposure) {
//		this.oneTouchOppExposure = oneTouchOppExposure;
//	}
//
//	public Exchange getExchange() {
//		return exchange;
//	}
//
//	public void setExchange(Exchange exchange) {
//		this.exchange = exchange;
//	}
//
//	public OpportunityType getType() {
//		return type;
//	}
//
//	public void setType(OpportunityType type) {
//		this.type = type;
//	}
//
//	public String toString() {
//        String ls = System.getProperty("line.separator");
//        return ls + "Opportunity" + ls
//            + super.toString() + ls
//            + "id: " + id + ls
//            + "marketId: " + marketId + ls
//            + "timeCreated: " + timeCreated + ls
//            + "timeFirstInvest: " + timeFirstInvest + ls
//            + "timeEstClosing: " + timeEstClosing + ls
//            + "timeActClosing: " + timeActClosing + ls
//            + "timeLastInvest: " + timeLastInvest + ls
//            + "currentLevel: " + currentLevel + ls
//            + "closingLevel: " + closingLevel + ls
//            + "timeSettled: " + timeSettled + ls
//            + "published: " + published + ls
//            + "settled: " + settled + ls
//            + "oddsTypeId: " + oddsTypeId + ls
//            + "opportunityTypeId: " + opportunityTypeId + ls
//            + "disabled: " + disabled + ls
//            + "disabledService: " + disabledService + ls
//            + "disabledTrader: " + disabledTrader + ls
//            + "scheduled: " + scheduled + ls
//            + "writerId: " + writerId + ls
//            + "typeId: " + typeId + ls
//            + "exchangeId: " + exchangeId + ls
//            + "investmentLimitGroupId: " + investmentLimitGroupId + ls
//            + "shiftParameter: " + shiftParameter + ls
//            + "autoShiftParameter: " + autoShiftParameter + ls
//            + "opportunityTypeDesc: " + opportunityTypeDesc + ls
//            + "opportunityOddsTypeName: " + opportunityOddsTypeName + ls
//            + "puts: " + puts + ls
//            + "calls: " + calls + ls
//            + "shifting: " + shifting + ls
//            + "shiftUp: " + shiftUp + ls
//            + "lastShift: " + lastShift + ls
//            + "state: " + state + ls
//            + "timeToClose: " + timeToClose + ls
//            + "timeNextOpen: " + timeNextOpen + ls
//            + "closingLevelReceived: " + closingLevelReceived + ls
//            + "nextClosingLevel: " + nextClosingLevel + ls
//            + "notClosedAlertSent: " + notClosedAlertSent + ls
//            + "deductWinOdds: " + deductWinOdds + ls
//            + "closeLevelTxt: " + closeLevelTxt + ls;
//    }
//}