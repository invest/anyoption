//package com.anyoption.beans;
//
//import java.util.Date;
//
//import com.anyoption.common.annotations.AnyoptionNoJSON;
//
///**
// * Market rate bean.
// * 
// * @author Tony
// */
//public class MarketRate extends com.anyoption.beans.base.MarketRate {
//    @AnyoptionNoJSON
//    protected long id;
//    @AnyoptionNoJSON
//    protected long marketId;
//    @AnyoptionNoJSON
//    protected double rate;
//    
//    public long getId() {
//        return id;
//    }
//    
//    public void setId(long id) {
//        this.id = id;
//    }
//    
//    public long getMarketId() {
//        return marketId;
//    }
//    
//    public void setMarketId(long marketId) {
//        this.marketId = marketId;
//    }
//    
//    public Date getRateTime() {
//        return rateTime;
//    }
//    
//    public void setRateTime(Date rateTime) {
//        this.rateTime = rateTime;
//    }
//    
//    public double getRate() {
//        return rate;
//    }
//
//    public void setRate(double rate) {
//        this.rate = rate;
//    }
//
//    public String toString() {
//        String ls = System.getProperty("line.separator");
//        return ls + "MarketRate:" + ls +
//            "id: " + id + ls +
//            "marketId: " + marketId + ls +
//            "rateTime: " + rateTime + ls +
//            "rate: " + rate + ls +
//            "graphRate: " + graphRate + ls +
//            "dayRate: " + dayRate + ls;
//    }
//}