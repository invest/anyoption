//package com.anyoption.beans;
//
//import java.io.Serializable;
//import java.util.Date;
//
///**
// * TierUser class
// *
// * @author Kobi.
// */
//public class TierUser implements Serializable {
//    private static final long serialVersionUID = 1L;
//
//    protected long id;
//    protected long userId;
//    protected long tierId;
//    protected long writerId;
//    protected Date timeCreated;
//    protected long points;
//    protected Date qualificationTime;
//
//    private String tierName;
//
//	/**
//	 * @return the id
//	 */
//	public long getId() {
//		return id;
//	}
//
//	/**
//	 * @param id the id to set
//	 */
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	/**
//	 * @return the points
//	 */
//	public long getPoints() {
//		return points;
//	}
//
//	/**
//	 * @param points the points to set
//	 */
//	public void setPoints(long points) {
//		this.points = points;
//	}
//
//	/**
//	 * @return the qualificationDate
//	 */
//	public Date getQualificationTime() {
//		return qualificationTime;
//	}
//
//	/**
//	 * @param qualificationDate the qualificationDate to set
//	 */
//	public void setQualificationTime(Date qualificationTime) {
//		this.qualificationTime = qualificationTime;
//	}
//
//	/**
//	 * @return the tierId
//	 */
//	public long getTierId() {
//		return tierId;
//	}
//
//	/**
//	 * @param tierId the tierId to set
//	 */
//	public void setTierId(long tierId) {
//		this.tierId = tierId;
//	}
//
//	/**
//	 * @return the timeCreated
//	 */
//	public Date getTimeCreated() {
//		return timeCreated;
//	}
//
//	/**
//	 * @param timeCreated the timeCreated to set
//	 */
//	public void setTimeCreated(Date timeCreated) {
//		this.timeCreated = timeCreated;
//	}
//
//	/**
//	 * @return the userId
//	 */
//	public long getUserId() {
//		return userId;
//	}
//
//	/**
//	 * @param userId the userId to set
//	 */
//	public void setUserId(long userId) {
//		this.userId = userId;
//	}
//
//	/**
//	 * @return the writerId
//	 */
//	public long getWriterId() {
//		return writerId;
//	}
//
//	/**
//	 * @param writerId the writerId to set
//	 */
//	public void setWriterId(long writerId) {
//		this.writerId = writerId;
//	}
//
//	/**
//	 * @return the tierName
//	 */
//	public String getTierName() {
//		return tierName;
//	}
//
//	/**
//	 * @param tierName the tierName to set
//	 */
//	public void setTierName(String tierName) {
//		this.tierName = tierName;
//	}
//
//	/**
//	 * can convert from points to cash allow
//	 * @return
//	 */
////	public boolean isPointsConvertAllow() {
////		Tier t = ApplicationDataBase.getTierById(tierId);
////		if (points >= t.getConvertMinPoints()) {
////			return true;
////		}
////		return false;
////	}
//
//	/**
//	 * toString implementation.
//	 */
//	public String toString() {
//        String ls = System.getProperty("line.separator");
//        return ls + "TierUserHis:" + ls +
//            "id: " + id + ls +
//            "userId: " + userId + ls +
//            "tierId: " + tierId + ls +
//            "writerId: " + writerId + ls +
//            "timeCreated: " + timeCreated + ls +
//            "points: " + points + ls +
//            "qualificationTime: " + qualificationTime + ls;
//    }
//
//}