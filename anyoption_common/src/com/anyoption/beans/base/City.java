/**
 * 
 */
package com.anyoption.beans.base;

import java.io.Serializable;

/**
 * @author AviadH
 *
 */
public class City implements Serializable {

	protected long id;
	protected String name;

	public long getId() {
		return id;
	}
    
	public void setId(long id) {
		this.id = id;
	}
    
	public String getName() {
		return name;
	}
    
	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
    	if (null !=  this.name) {
    		return this.name;
    	}
    	return "";
    }
}
