//package com.anyoption.beans.base;
//
//import java.io.Serializable;
//import java.util.ArrayList;
//
///**
// * Market group VO.
// */
//public class MarketGroup implements Serializable, Cloneable {
//
//	private long id;
//    private String name;
//    private ArrayList<Market> markets;
//
//	/**
//     * @return Returns the id.
//     */
//    public long getId() {
//        return id;
//    }
//
//    /**
//     * @param id The id to set.
//     */
//    public void setId(long id) {
//        this.id = id;
//    }
//
//    /**
//     * @return Returns the name.
//     */
//    public String getName() {
//        return name;
//    }
//
//    /**
//     * @param name The name to set.
//     */
//    public void setName(String name) {
//        this.name = name;
//    }
//
//	/**
//	 * @return the markets
//	 */
//	public ArrayList<Market> getMarkets() {
//		return markets;
//	}
//
//	/**
//	 * @param markets the markets to set
//	 */
//	public void setMarkets(ArrayList<Market> markets) {
//		this.markets = markets;
//	}
//
//	@Override
//	public Object clone() throws CloneNotSupportedException {
//		MarketGroup clon = (MarketGroup) super.clone();
//		ArrayList<Market> cloneList = new ArrayList<Market>(markets.size());
//		for (Market m : clon.markets) {
//			cloneList.add((Market) m.clone());
//		}
//		clon.markets = cloneList;
//		return clon;
//	}
//
//}