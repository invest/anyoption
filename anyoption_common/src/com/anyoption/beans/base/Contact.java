package com.anyoption.beans.base;

import java.io.Serializable;
import java.util.Date;

import com.google.gson.annotations.Expose;

public class Contact implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public static long CONTACT_ME_TYPE = 1;
	public static long CONTACT_US_TYPE = 2;
    public static long CONTACT_US_DIRECT_DOWNLOAD_WEB_EMAIL = 3;
    public static long CONTACT_US_DIRECT_DOWNLOAD_MOBILE = 4;
    public static long CONTACT_US_SHORT_REG_FORM = 5;
    public static long CONTACT_US_REGISTER_ATTEMPTS = 6;
    public static long CONTACT_MAIL_FORM = 9; 
    
    @Expose
    protected long id;
    protected String text;
    protected long userId;
	protected String phone;
	protected String name;
	protected long type;
	protected long countryId;
	protected String email;
	protected long skinId;
	protected long writerId;
	protected Long combId;
	protected String dynamicParameter;
    protected String aff_sub1;
    protected String aff_sub2;
    protected String aff_sub3;
	protected String utcOffset;
	protected Date timeCreated;
	protected String countryName;
	protected String ip;
    protected String phoneType;
    protected String userAgent;
    protected String dfaPlacementId;
    protected String dfaCreativeId;
    protected String dfaMacro;
    //Register attempts parameters
	protected String firstName;
    protected String lastName;
    protected String mobilePhone;
    protected String landLinePhone;
	protected boolean isContactByEmail;
	protected boolean isContactBySMS;
    protected String deviceUniqueId;
    
    protected String httpReferer;
    protected long affiliateKey;
    
    //Marketing Tracking
    protected String mId;
    protected String marketingStaticPart;
    protected String etsMId;
    protected String utmSource;
    
    
	/**
	 * @return the combId
	 */
	public Long getCombId() {
		return combId;
	}

	/**
	 * @param combId the combId to set
	 */
	public void setCombId(Long combId) {
		this.combId = combId;
	}

	/**
	 * @return the dynamicParameter
	 */
	public String getDynamicParameter() {
		return dynamicParameter;
	}

	/**
	 * @param dynamicParameter the dynamicParameter to set
	 */
	public void setDynamicParameter(String dynamicParameter) {
		this.dynamicParameter = dynamicParameter;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public long getType() {
		return type;
	}

	public void setType(long type) {
		this.type = type;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getSkinId() {
		return skinId;
	}

	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the utcOffset
	 */
	public String getUtcOffset() {
		return utcOffset;
	}

	/**
	 * @param utcOffset the utcOffset to set
	 */
	public void setUtcOffset(String utcOffset) {
		this.utcOffset = utcOffset;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the countryName
	 */
	public String getCountryName() {
		return countryName;
	}

	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * @return the deviceUniqueId
	 */
	public String getDeviceUniqueId() {
		return deviceUniqueId;
	}

	/**
	 * @param deviceUniqueId the deviceUniqueId to set
	 */
	public void setDeviceUniqueId(String deviceUniqueId) {
		this.deviceUniqueId = deviceUniqueId;
	}

	/**
	 * @return the dfaCreativeId
	 */
	public String getDfaCreativeId() {
		return dfaCreativeId;
	}

	/**
	 * @param dfaCreativeId the dfaCreativeId to set
	 */
	public void setDfaCreativeId(String dfaCreativeId) {
		this.dfaCreativeId = dfaCreativeId;
	}

	/**
	 * @return the dfaMacro
	 */
	public String getDfaMacro() {
		return dfaMacro;
	}

	/**
	 * @param dfaMacro the dfaMacro to set
	 */
	public void setDfaMacro(String dfaMacro) {
		this.dfaMacro = dfaMacro;
	}

	/**
	 * @return the dfaPlacementId
	 */
	public String getDfaPlacementId() {
		return dfaPlacementId;
	}

	/**
	 * @param dfaPlacementId the dfaPlacementId to set
	 */
	public void setDfaPlacementId(String dfaPlacementId) {
		this.dfaPlacementId = dfaPlacementId;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the isContactByEmail
	 */
	public boolean isContactByEmail() {
		return isContactByEmail;
	}

	/**
	 * @param isContactByEmail the isContactByEmail to set
	 */
	public void setContactByEmail(boolean isContactByEmail) {
		this.isContactByEmail = isContactByEmail;
	}

	/**
	 * @return the isContactBySMS
	 */
	public boolean isContactBySMS() {
		return isContactBySMS;
	}

	/**
	 * @param isContactBySMS the isContactBySMS to set
	 */
	public void setContactBySMS(boolean isContactBySMS) {
		this.isContactBySMS = isContactBySMS;
	}

	/**
	 * @return the landLinePhone
	 */
	public String getLandLinePhone() {
		return landLinePhone;
	}

	/**
	 * @param landLinePhone the landLinePhone to set
	 */
	public void setLandLinePhone(String landLinePhone) {
		this.landLinePhone = landLinePhone;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the mobilePhone
	 */
	public String getMobilePhone() {
		return mobilePhone;
	}

	/**
	 * @param mobilePhone the mobilePhone to set
	 */
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	/**
	 * @return the phoneType
	 */
	public String getPhoneType() {
		return phoneType;
	}

	/**
	 * @param phoneType the phoneType to set
	 */
	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}

	/**
	 * @return the userAgent
	 */
	public String getUserAgent() {
		return userAgent;
	}

	/**
	 * @param userAgent the userAgent to set
	 */
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	/**
	 * @return the httpReferer
	 */
	public String getHttpReferer() {
		return httpReferer;
	}

	/**
	 * @param httpReferer the httpReferer to set
	 */
	public void setHttpReferer(String httpReferer) {
		this.httpReferer = httpReferer;
	}

	public long getAffiliateKey() {
		return affiliateKey;
	}

	public void setAffiliateKey(long affiliateKey) {
		this.affiliateKey = affiliateKey;
	}
    /**
     * @return the marketingStaticPart
     */
    public String getMarketingStaticPart() {
        return marketingStaticPart;
    }

    /**
     * @param marketingStaticPart the marketingStaticPart to set
     */
    public void setMarketingStaticPart(String marketingStaticPart) {
        this.marketingStaticPart = marketingStaticPart;
    }

    /**
     * @return the mId
     */
    public String getmId() {
        return mId;
    }

    /**
     * @param mId the mId to set
     */
    public void setmId(String mId) {
        this.mId = mId;
    }
        
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "Contact" + ls
            + super.toString() + ls
            + "id: " + id + ls
            + "text: " + text + ls
            + "userId: " + userId + ls
            + "phone: " + phone + ls
            + "name: " + name + ls
            + "type: " + type + ls
            + "countryId: " + countryId + ls
            + "email: " + email + ls
            + "skinId: " + skinId + ls
            + "writerId: " + writerId + ls
            + "userId: " + userId + ls
            + "combId: " + combId + ls
            + "dynamicParameter: " + dynamicParameter + ls
            + "countryName: " + countryName + ls
            + "ip: " + ip + ls
            + "phoneType: " + phoneType + ls
            + "userAgent: " + userAgent + ls
            + "dfaPlacementId: " + dfaPlacementId + ls
            + "dfaCreativeId: " + dfaCreativeId + ls
            + "dfaMacro: " + dfaMacro + ls
            + "utcOffset: " + utcOffset + ls
            + "httpReferer: " + httpReferer + ls
            + "affiliateKey: " + affiliateKey + ls
            + "mId: " + mId + ls
            + "etsMid: " + etsMId + ls
            + "marketingStaticPart: " + marketingStaticPart + ls
            + "firstName: " + firstName + ls
            + "lastName: " + lastName + ls
            + "mobilePhone: " + mobilePhone + ls
            + "landLinePhone: " + landLinePhone + ls
            + "deviceUniqueId: " + deviceUniqueId + ls;
    }

    /**
     * @return the etsMId
     */
    public String getEtsMId() {
        return etsMId;
    }

    /**
     * @param etsMId the etsMId to set
     */
    public void setEtsMId(String etsMId) {
        this.etsMId = etsMId;
    }

	public String getUtmSource() {
		return utmSource;
	}

	public void setUtmSource(String utmSource) {
		this.utmSource = utmSource;
	}

	public String getAff_sub1() {
		return aff_sub1;
	}

	public void setAff_sub1(String aff_sub1) {
		this.aff_sub1 = aff_sub1;
	}

	public String getAff_sub2() {
		return aff_sub2;
	}

	public void setAff_sub2(String aff_sub2) {
		this.aff_sub2 = aff_sub2;
	}

	public String getAff_sub3() {
		return aff_sub3;
	}

	public void setAff_sub3(String aff_sub3) {
		this.aff_sub3 = aff_sub3;
	}
	
	
}