//package com.anyoption.beans.base;
//
//import java.io.Serializable;
//import java.util.Date;
//
//
//public class Bonus implements Serializable {
//    protected long id;
//    protected Date startDate;
//    protected Date endDate;
//    protected long wageringParameter;
//    protected long defaultPeriod;
//    protected long autoRenewInd;
//    protected long renewFrequency;
//    protected long typeId;
//    protected long writerId;
//    protected Date timeCreated;
//    protected long numberOfActions;
//    protected String typeName;
//    protected String name;
//    protected long classType;
//    protected String classTypeName;
//    protected boolean isHasSteps;
//    protected double oddsWin;
//    protected double oddsLose;
//	private String condition;
//
//	/**
//	 * @return the condition
//	 */
//	public String getCondition() {
//		return condition;
//	}
//
//	/**
//	 * @param condition the condition to set
//	 */
//	public void setCondition(String condition) {
//		this.condition = condition;
//	}
//
//	public boolean isHasSteps() {
//		return isHasSteps;
//	}
//
//	public void setHasSteps(boolean isHasSteps) {
//		this.isHasSteps = isHasSteps;
//	}
//
//	/**
//	 * @return the autoRenewInd
//	 */
//	public long getAutoRenewInd() {
//		return autoRenewInd;
//	}
//
//	/**
//	 * @param autoRenewInd the autoRenewInd to set
//	 */
//	public void setAutoRenewInd(long autoRenewInd) {
//		this.autoRenewInd = autoRenewInd;
//	}
//
//	/**
//	 * @return the defaultPeriod
//	 */
//	public long getDefaultPeriod() {
//		return defaultPeriod;
//	}
//
//	/**
//	 * @param defaultPeriod the defaultPeriod to set
//	 */
//	public void setDefaultPeriod(long defaultPeriod) {
//		this.defaultPeriod = defaultPeriod;
//	}
//
//	/**
//	 * @return the endDate
//	 */
//	public Date getEndDate() {
//		return endDate;
//	}
//
//	/**
//	 * @param endDate the endDate to set
//	 */
//	public void setEndDate(Date endDate) {
//		this.endDate = endDate;
//	}
//
//	/**
//	 * @return the id
//	 */
//	public long getId() {
//		return id;
//	}
//
//	/**
//	 * @param id the id to set
//	 */
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	/**
//	 * @return the numberOfActions
//	 */
//	public long getNumberOfActions() {
//		return numberOfActions;
//	}
//
//	/**
//	 * @param numberOfActions the numberOfActions to set
//	 */
//	public void setNumberOfActions(long numberOfActions) {
//		this.numberOfActions = numberOfActions;
//	}
//
//	/**
//	 * @return the renewFrequency
//	 */
//	public long getRenewFrequency() {
//		return renewFrequency;
//	}
//
//	/**
//	 * @param renewFrequency the renewFrequency to set
//	 */
//	public void setRenewFrequency(long renewFrequency) {
//		this.renewFrequency = renewFrequency;
//	}
//
//	/**
//	 * @return the startDate
//	 */
//	public Date getStartDate() {
//		return startDate;
//	}
//
//	/**
//	 * @param startDate the startDate to set
//	 */
//	public void setStartDate(Date startDate) {
//		this.startDate = startDate;
//	}
//
//	/**
//	 * @return the timeCreated
//	 */
//	public Date getTimeCreated() {
//		return timeCreated;
//	}
//
//	/**
//	 * @param timeCreated the timeCreated to set
//	 */
//	public void setTimeCreated(Date timeCreated) {
//		this.timeCreated = timeCreated;
//	}
//
//	/**
//	 * @return the typeId
//	 */
//	public long getTypeId() {
//		return typeId;
//	}
//
//	/**
//	 * @param typeId the typeId to set
//	 */
//	public void setTypeId(long typeId) {
//		this.typeId = typeId;
//	}
//
//	/**
//	 * @return the wageringParameter
//	 */
//	public long getWageringParameter() {
//		return wageringParameter;
//	}
//
//	/**
//	 * @param wageringParameter the wageringParameter to set
//	 */
//	public void setWageringParameter(long wageringParameter) {
//		this.wageringParameter = wageringParameter;
//	}
//
//	/**
//	 * @return the writerId
//	 */
//	public long getWriterId() {
//		return writerId;
//	}
//
//	/**
//	 * @param writerId the writerId to set
//	 */
//	public void setWriterId(long writerId) {
//		this.writerId = writerId;
//	}
//
//    /**
//     * @return the typeName
//     */
//    public String getTypeName() {
//        return typeName;
//    }
//
//    /**
//     * @param typeName the typeName to set
//     */
//    public void setTypeName(String typeName) {
//        this.typeName = typeName;
//    }
//
//    /**
//     * @return the name
//     */
//    public String getName() {
//        return name;
//    }
//
//    /**
//     * @param name the name to set
//     */
//    public void setName(String name) {
//        this.name = name;
//    }
//
//	/**
//	 * @return the classType
//	 */
//	public long getClassType() {
//		return classType;
//	}
//
//	/**
//	 * @param classType the classType to set
//	 */
//	public void setClassType(long classType) {
//		this.classType = classType;
//	}
//
//	/**
//	 * @return the classTypeName
//	 */
//	public String getClassTypeName() {
//		return classTypeName;
//	}
//
//	/**
//	 * @param classTypeName the classTypeName to set
//	 */
//	public void setClassTypeName(String classTypeName) {
//		this.classTypeName = classTypeName;
//	}
//
//    public double getOddsWin() {
//        return oddsWin;
//    }
//
//    public void setOddsWin(double oddsWin) {
//        this.oddsWin = oddsWin;
//    }
//
//    public double getOddsLose() {
//        return oddsLose;
//    }
//
//    public void setOddsLose(double oddsLose) {
//        this.oddsLose = oddsLose;
//    }
//
//}