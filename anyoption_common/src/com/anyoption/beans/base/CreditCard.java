//package com.anyoption.beans.base;
//
//public class CreditCard implements java.io.Serializable {
//
//	private static final long serialVersionUID = 1L;
//	
//	protected long id;
//	protected long userId;
//	protected long typeId;
//	protected long ccNumber;
//	protected String ccPass;
//	protected String expMonth;
//	protected String expYear;
//	protected String holderName;
//	protected String holderId;
//	protected int isVisible;
//	protected int isAllowed; 
//	protected long countryId;
//	protected long typeIdByBin;
//	protected CreditCardType type;
//		
//	protected String typeName;
//	protected String ccNumberXXXX;
//	protected long creditAmount;  // credit amount that allow in case cftEnabled = false
//	protected String creditAmountTxt;
//	protected boolean cftAvailable; // exists in cft_bins lsle
//    protected boolean nonCftAvailable;
//    protected long clearingProviderId;
//    protected long cftClearingProviderId;
//	
//	
//	/**
//	 * @return the clearingProviderId
//	 */
//	public long getClearingProviderId() {
//		return clearingProviderId;
//	}
//
//	/**
//	 * @param clearingProviderId the clearingProviderId to set
//	 */
//	public void setClearingProviderId(long clearingProviderId) {
//		this.clearingProviderId = clearingProviderId;
//	}
//	
//	/**
//	 * @return the cftClearingProviderId
//	 */
//	public long getCftClearingProviderId() {
//		return cftClearingProviderId;
//	}
//
//	/**
//	 * @param cftClearingProviderId the cftClearingProviderId to set
//	 */
//	public void setCftClearingProviderId(long cftClearingProviderId) {
//		this.cftClearingProviderId = cftClearingProviderId;
//	}
//
//	/**
//	 * @return the cftAvailable
//	 */
//	public boolean isCftAvailable() {
//		return cftAvailable;
//	}
//
//	/**
//	 * @param cftAvailable the cftAvailable to set
//	 */
//	public void setCftAvailable(boolean cftAvailable) {
//		this.cftAvailable = cftAvailable;
//	}
//
//	/**
//	 * @return the nonCftAvailable
//	 */
//	public boolean isNonCftAvailable() {
//		return nonCftAvailable;
//	}
//
//	/**
//	 * @param nonCftAvailable the nonCftAvailable to set
//	 */
//	public void setNonCftAvailable(boolean nonCftAvailable) {
//		this.nonCftAvailable = nonCftAvailable;
//	}
//	/**
//	 * @return the creditAmountTxt
//	 */
//	public String getCreditAmountTxt() {
//		return creditAmountTxt;
//	}
//
//	/**
//	 * @param creditAmountTxt the creditAmountTxt to set
//	 */
//	public void setCreditAmountTxt(String creditAmountTxt) {
//		this.creditAmountTxt = creditAmountTxt;
//	}
//
//	public long getCcNumber() {
//		return ccNumber;
//	}
//
//	public void setCcNumber(long ccNumber) {
//		this.ccNumber = ccNumber;
//	}
//
//	public String getCcPass() {
//		return ccPass;
//	}
//
//	public void setCcPass(String ccPass) {
//		this.ccPass = ccPass;
//	}
//
//	public String getExpMonth() {
//		return expMonth;
//	}
//
//	public void setExpMonth(String expMonth) {
//		this.expMonth = expMonth;
//	}
//
//	public String getExpYear() {
//		return expYear;
//	}
//	
//	public String getHolderId() {
//		return holderId;
//	}
//
//	public void setHolderId(String holderId) {
//		this.holderId = holderId;
//	}
//
//	public void setExpYear(String expYear) {
//		this.expYear = expYear;
//	}
//
//	public String getHolderName() {
//		return holderName;
//	}
//
//	public void setHolderName(String holderName) {
//		this.holderName = holderName;
//	}
//
//	public long getId() {
//		return id;
//	}
//
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	public long getTypeId() {
//		return typeId;
//	}
//
//	public void setTypeId(long typeId) {
//		this.typeId = typeId;
//	}
//
//	public long getUserId() {
//		return userId;
//	}
//
//	public void setUserId(long userId) {
//		this.userId = userId;
//	}
//
//	public String getTypeIdStr() {
//		return String.valueOf(typeId);
//	}
//
//	public void setTypeIdStr(String typeIdStr) {
//		this.typeId = Long.parseLong(typeIdStr);
//	}
//
//    /**
//	 * @return the typeName
//	 */
//	public String getTypeName() {		
//		return typeName;
//	}
//
//	/**
//	 * @param typeName the typeName to set
//	 */
//	public void setTypeName(String typeName) {
//		this.typeName = typeName;
//	}
//
//	/**
//	 * @return the ccNumberXXXX
//	 */
//	public String getCcNumberXXXX() {
//		String ccnum = String.valueOf(ccNumber);
//		String out = "";
//		for (int i = 0 ;i < ccnum.length() - 4; i++){
//			out += "X";
//		}
//		return out + ccnum.substring(ccnum.length() - 4);
//	}
//
//	/**
//	 * @param ccNumberXXXX the ccNumberXXXX to set
//	 */
//	public void setCcNumberXXXX(String ccNumberXXXX) {
//		this.ccNumberXXXX = ccNumberXXXX;
//	}
//
//	/**
//	 * @return the isVisible
//	 */
//	public int getIsVisible() {
//		return isVisible;
//	}
//
//	/**
//	 * @param isVisible the isVisible to set
//	 */
//	public void setIsVisible(int isVisible) {
//		this.isVisible = isVisible;
//	}
//
//	/**
//	 * @return the isAllowed
//	 */
//	public int getIsAllowed() {
//		return isAllowed;
//	}
//
//	/**
//	 * @param isAllowed the isAllowed to set
//	 */
//	public void setIsAllowed(int isAllowed) {
//		this.isAllowed = isAllowed;
//	}
//
//	/**
//	 * @return the countryId
//	 */
//	public long getCountryId() {
//		return countryId;
//	}
//
//	/**
//	 * @param countryId the countryId to set
//	 */
//	public void setCountryId(long countryId) {
//		this.countryId = countryId;
//	}
//
//	/**
//	 * @return the typeIdByBin
//	 */
//	public long getTypeIdByBin() {
//		return typeIdByBin;
//	}
//
//	/**
//	 * @param typeIdByBin the typeIdByBin to set
//	 */
//	public void setTypeIdByBin(long typeIdByBin) {
//		this.typeIdByBin = typeIdByBin;
//	}
//	
//	public String getExpDateStr(){
//		return expMonth + "/" + expYear;
//	}
//	
//    /**
//	 * @return the type
//	 */
//	public CreditCardType getType() {
//		return type;
//	}
//
//	/**
//	 * @param type the type to set
//	 */
//	public void setType(CreditCardType type) {
//		this.type = type;
//	}
//
//	public String toString() {
//    	if (0 !=  this.ccNumber) {
//    		int ccLength = String.valueOf(ccNumber).length();
//			String ccNum = String.valueOf(ccNumber).substring(ccLength - 4, ccLength);
//    		return ccNum + " " + typeName;
//    	}
//    	return typeName;  
//    }
//
//	/**
//	 * @return the creditAmount
//	 */
//	public long getCreditAmount() {
//		return creditAmount;
//	}
//
//	/**
//	 * @param creditAmount the creditAmount to set
//	 */
//	public void setCreditAmount(long creditAmount) {
//		this.creditAmount = creditAmount;
//	}
//	
//	public boolean getIsAllowedBoolean() {
//		return isAllowed == 1;
//	}
//    
//    
//	
//}