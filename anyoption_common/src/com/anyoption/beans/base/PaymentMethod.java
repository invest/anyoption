//package com.anyoption.beans.base;
//
//import java.io.Serializable;
//
//public class PaymentMethod implements Serializable, Cloneable {
//
//	private static final long serialVersionUID = 1L;
//	protected long id;
//	protected long skinId;
//	protected long transactionTypeId;
//	protected int typeId;
//	protected String description;
//	protected String name;
//	protected String envoyWithdrawCountryCode;
//	protected String displayName;
//	protected String bigLogo;
//	protected String smallLogo;
//	//Types
//	public static final long TYPE_ID_DEPOSIT = 1;
//	public static final long TYPE_ID_WITHDRAW = 2;	
//	
//	// Payment method types	
//	public static final long PAYMENT_TYPE_CC = 1;
//	public static final long PAYMENT_TYPE_DIRECT24 = 2;
//	public static final long PAYMENT_TYPE_GIROPAY= 3;
//	public static final long PAYMENT_TYPE_EPS = 4;
//	public static final long PAYMENT_TYPE_ENV_SPAIN_BANK = 5;
//	public static final long PAYMENT_TYPE_ENV_MEXICO_BANK = 6;
//	public static final long PAYMENT_TYPE_ENVOY = 7;
//	public static final long PAYMENT_TYPE_ACH = 8;
//	public static final long PAYMENT_TYPE_CASHU = 9;
//	public static final long PAYMENT_TYPE_WIRE = 10;
//	public static final long PAYMENT_TYPE_PAYPAL = 11;
//	public static final long PAYMENT_TYPE_TELEINGRESO = 12;
//	public static final long PAYMENT_TYPE_POLI2 = 13;
//	public static final long PAYMENT_TYPE_SANTANDER = 14;
//	public static final long PAYMENT_TYPE_IDEAL = 15;
//	public static final long PAYMENT_TYPE_MONETA = 16;
//	public static final long PAYMENT_TYPE_UKASH = 17;
//	public static final long PAYMENT_TYPE_WEB_MONEY = 26;
//	public static final long PAYMENT_TYPE_DINERO_MAIL = 37;
//	public static final long PAYMENT_TYPE_MONETA_WITHDRAWAL = 38;
//	public static final long PAYMENT_TYPE_WEB_MONEY_WITHDRAWAL = 39;
//	public static final long PAYMENT_TYPE_LOBANET = 47;
//	public static final long PAYMENT_TYPE_MONEYBOOKERS = 53;
//		
//	public long getId() {
//		return id;
//	}
//
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	public long getSkinId() {
//		return skinId;
//	}
//
//	public void setSkinId(long skinId) {
//		this.skinId = skinId;
//	}
//
//	public long getTransactionTypeId() {
//		return transactionTypeId;
//	}
//
//	public void setTransactionTypeId(long transactionTypeId) {
//		this.transactionTypeId = transactionTypeId;
//	}
//
//	/**
//	 * @return the typeId
//	 */
//	public int getTypeId() {
//		return typeId;
//	}
//
//	/**
//	 * @param typeId the typeId to set
//	 */
//	public void setTypeId(int typeId) {
//		this.typeId = typeId;
//	}
//
//	/**
//	 * @return the description
//	 */
//	public String getDescription() {
//		return description;
//	}
//
//	/**
//	 * @param description the description to set
//	 */
//	public void setDescription(String description) {
//		this.description = description;
//	}
//
//	/**
//	 * @return the name
//	 */
//	public String getName() {
//		return name;
//	}
//
//	/**
//	 * @param name the name to set
//	 */
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	/**
//	 * @return the envoyWithdrawCountryCode
//	 */
//	public String getEnvoyWithdrawCountryCode() {
//		return envoyWithdrawCountryCode;
//	}
//
//	/**
//	 * @param envoyWithdrawCountryCode the envoyWithdrawCountryCode to set
//	 */
//	public void setEnvoyWithdrawCountryCode(String envoyWithdrawCountryCode) {
//		this.envoyWithdrawCountryCode = envoyWithdrawCountryCode;
//	}
//
//	/**
//	 * @return the displayName
//	 */
//	public String getDisplayName() {
//		return displayName;
//	}
//
//	/**
//	 * @param displayName the displayName to set
//	 */
//	public void setDisplayName(String displayName) {
//		this.displayName = displayName;
//	}
//
//	/**
//	 * @return the bigLogo
//	 */
//	public String getBigLogo() {
//		return bigLogo;
//	}
//
//	/**
//	 * @param bigLogo the bigLogo to set
//	 */
//	public void setBigLogo(String bigLogo) {
//		this.bigLogo = bigLogo;
//	}
//
//	/**
//	 * @return the smallLogo
//	 */
//	public String getSmallLogo() {
//		return smallLogo;
//	}
//
//	/**
//	 * @param smallLogo the smallLogo to set
//	 */
//	public void setSmallLogo(String smallLogo) {
//		this.smallLogo = smallLogo;
//	}
//	
//	
//	
//}