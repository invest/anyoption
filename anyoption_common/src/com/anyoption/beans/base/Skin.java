//package com.anyoption.beans.base;
//
//import java.io.Serializable;
//
//
//public class Skin implements Serializable {
//    // TODO: Move here all skin constants from ConstantsBase
//	public static final long SKIN_ETRADER = 1;
//    public static final long SKIN_ENGLISH = 2;
//    public static final long SKIN_DE = 8;
//    public static final long SKIN_EN_US = 13;
//    public static final long SKIN_ES_US = 14;
//
//    protected int id;
//    protected String name;
//    protected String displayName;
//    protected int defaultCountryId;
//    protected int defaultLanguageId;
//    protected int defaultXorId;
//    protected int defaultCurrencyId;
//    protected long defaultCombinationId;
//    protected long tierQualificationPeriod;
//    protected long businessCaseId;
//    private String supportStartTime;
//    private String supportEndTime;
//
//	public Skin() {
//        name = "";
//        displayName = "";
//    }
//
//	 /**
//	 * @return the supportStartTime
//	 */
//	public String getSupportStartTime() {
//		return supportStartTime;
//	}
//
//	/**
//	 * @param supportStartTime the supportStartTime to set
//	 */
//	public void setSupportStartTime(String supportStartTime) {
//		this.supportStartTime = supportStartTime;
//	}
//
//	/**
//	 * @return the supportEndTime
//	 */
//	public String getSupportEndTime() {
//		return supportEndTime;
//	}
//
//	/**
//	 * @param supportEndTime the supportEndTime to set
//	 */
//	public void setSupportEndTime(String supportEndTime) {
//		this.supportEndTime = supportEndTime;
//	}
//
//    /**
//     * @return the defaultCountryId
//     */
//    public int getDefaultCountryId() {
//        return defaultCountryId;
//    }
//
//    /**
//     * @param defaultCountryId the defaultCountryId to set
//     */
//    public void setDefaultCountryId(int defaultCountryId) {
//        this.defaultCountryId = defaultCountryId;
//    }
//
//    /**
//     * @return the defaultLanguageId
//     */
//    public int getDefaultLanguageId() {
//        return defaultLanguageId;
//    }
//
//    /**
//     * @param defaultLanguageId the defaultLanguageId to set
//     */
//    public void setDefaultLanguageId(int defaultLanguageId) {
//        this.defaultLanguageId = defaultLanguageId;
//    }
//
//
//    public int getDefaultXorId() {
//        return defaultXorId;
//    }
//
//    public void setDefaultXorId(int defaultXorId) {
//        this.defaultXorId = defaultXorId;
//    }
//
//    /**
//     * @return the displayName
//     */
//    public String getDisplayName() {
//        return displayName;
//    }
//
//    /**
//     * @param displayName the displayName to set
//     */
//    public void setDisplayName(String displayName) {
//        this.displayName = displayName;
//    }
//
//    /**
//     * @return the id
//     */
//    public int getId() {
//        return id;
//    }
//
//    /**
//     * @param id the id to set
//     */
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    /**
//     * @return the name
//     */
//    public String getName() {
//        return name;
//    }
//
//    /**
//     * @param name the name to set
//     */
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    /**
//     * @return the defaultCurrencyId
//     */
//    public int getDefaultCurrencyId() {
//        return defaultCurrencyId;
//    }
//
//    /**
//     * @param defaultCurrencyId the defaultCurrencyId to set
//     */
//    public void setDefaultCurrencyId(int defaultCurrencyId) {
//        this.defaultCurrencyId = defaultCurrencyId;
//    }
//
//    /**
//     * @return the defaultCombinationId
//     */
//    public long getDefaultCombinationId() {
//        return defaultCombinationId;
//    }
//
//    /**
//     * @param defaultCombinationId the defaultCombinationId to set
//     */
//    public void setDefaultCombinationId(long defaultCombinationId) {
//        this.defaultCombinationId = defaultCombinationId;
//    }
//
//   /**
//     * @return the tierQualificationPeriod
//     */
//    public long getTierQualificationPeriod() {
//        return tierQualificationPeriod;
//    }
//
//    /**
//     * @param tierQualificationPeriod the tierQualificationPeriod to set
//     */
//    public void setTierQualificationPeriod(long tierQualificationPeriod) {
//        this.tierQualificationPeriod = tierQualificationPeriod;
//    }
//
//   /**
//     * @return the businessCaseId
//     */
//    public long getBusinessCaseId() {
//        return businessCaseId;
//    }
//
//    /**
//     * @param businessCaseId the businessCaseId to set
//     */
//    public void setBusinessCaseId(long businessCaseId) {
//        this.businessCaseId = businessCaseId;
//    }
//
//}