//package com.anyoption.beans.base;
//
///**
// * BonusUsersStep vo class.
// * @author Eliran
// */
//public class BonusUsersStep implements java.io.Serializable {
//	private static final long serialVersionUID = -277969522156581671L;
//
//	private long id;
//	private double bonusPercent;
//	private long minDepositAmount;
//	private long maxDepositAmount;
//	private long bonusUserId;
//
//	/**
//	 * @return the bonusPercent
//	 */
//	public double getBonusPercent() {
//		return bonusPercent;
//	}
//    
//	/**
//	 * @param bonusPercent the bonusPercent to set
//	 */
//	public void setBonusPercent(double bonusPercent) {
//		this.bonusPercent = bonusPercent;
//	}
//    
//	/**
//	 * @return the bonusUserId
//	 */
//	public long getBonusUserId() {
//		return bonusUserId;
//	}
//    
//	/**
//	 * @param bonusUserId the bonusUserId to set
//	 */
//	public void setBonusUserId(long bonusUserId) {
//		this.bonusUserId = bonusUserId;
//	}
//    
//	/**
//	 * @return the id
//	 */
//	public long getId() {
//		return id;
//	}
//    
//	/**
//	 * @param id the id to set
//	 */
//	public void setId(long id) {
//		this.id = id;
//	}
//    
//	/**
//	 * @return the maxDepositAmount
//	 */
//	public long getMaxDepositAmount() {
//		return maxDepositAmount;
//	}
//    
//	/**
//	 * @param maxDepositAmount the maxDepositAmount to set
//	 */
//	public void setMaxDepositAmount(long maxDepositAmount) {
//		this.maxDepositAmount = maxDepositAmount;
//	}
//    
//	/**
//	 * @return the minDepositAmount
//	 */
//	public long getMinDepositAmount() {
//		return minDepositAmount;
//	}
//    
//	/**
//	 * @param minDepositAmount the minDepositAmount to set
//	 */
//	public void setMinDepositAmount(long minDepositAmount) {
//		this.minDepositAmount = minDepositAmount;
//	}
//}