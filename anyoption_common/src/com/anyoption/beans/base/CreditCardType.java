//package com.anyoption.beans.base;
//
//import java.io.Serializable;
//
//public class CreditCardType implements Serializable {
//
//	protected long id;
//	protected String name;
//    protected boolean threeDSecure;
//
//	public long getId() {
//		return id;
//	}
//
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public boolean isThreeDSecure() {
//        return threeDSecure;
//    }
//
//    public void setThreeDSecure(boolean threeDSecure) {
//        this.threeDSecure = threeDSecure;
//    }
//
//}