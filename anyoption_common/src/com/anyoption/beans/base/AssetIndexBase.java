//package com.anyoption.beans.base;
//
//import java.io.Serializable;
//
///**
// * @author EranL
// *
// */
//public class AssetIndexBase implements Serializable{
//	
//	protected long id;
//	protected String tradingDays;
//	protected String fridayTime = null;
//	protected String startTime;
//	protected String endTime;
//	protected boolean isFullDay;
//	
//	//market
//	protected String feedName;
//	protected long marketGroupId;	
//	protected String timeZoneString;
//	protected String tradingDaysFormat;
//	protected long optionPlusMarketId;
//	protected String expiryFormula;
//	protected long exchangeId;
//	protected String reutersField;
//					
//	/**
//	 * @return the id
//	 */
//	public long getId() {
//		return id;
//	}
//	/**
//	 * @param id the id to set
//	 */
//	public void setId(long id) {
//		this.id = id;
//	}
//	/**
//	 * @return the tradingDays
//	 */
//	public String getTradingDays() {
//		return tradingDays;
//	}
//	/**
//	 * @param tradingDays the tradingDays to set
//	 */
//	public void setTradingDays(String tradingDays) {
//		this.tradingDays = tradingDays;
//	}
//	/**
//	 * @return the fridayTime
//	 */
//	public String getFridayTime() {
//		return fridayTime;
//	}
//	/**
//	 * @param fridayTime the fridayTime to set
//	 */
//	public void setFridayTime(String fridayTime) {
//		this.fridayTime = fridayTime;
//	}
//	/**
//	 * @return the startTime
//	 */
//	public String getStartTime() {
//		return startTime;
//	}
//	/**
//	 * @param startTime the startTime to set
//	 */
//	public void setStartTime(String startTime) {
//		this.startTime = startTime;
//	}
//	/**
//	 * @return the endTime
//	 */
//	public String getEndTime() {
//		return endTime;
//	}
//	/**
//	 * @param endTime the endTime to set
//	 */
//	public void setEndTime(String endTime) {
//		this.endTime = endTime;
//	}
//	/**
//	 * @return the isFullDay
//	 */
//	public boolean isFullDay() {
//		return isFullDay;
//	}
//	/**
//	 * @param isFullDay the isFullDay to set
//	 */
//	public void setFullDay(boolean isFullDay) {
//		this.isFullDay = isFullDay;
//	}
//	/**
//	 * @return the feedName
//	 */
//	public String getFeedName() {
//		return feedName;
//	}
//	/**
//	 * @param feedName the feedName to set
//	 */
//	public void setFeedName(String feedName) {
//		this.feedName = feedName;
//	}
//	/**
//	 * @return the marketGroupId
//	 */
//	public long getMarketGroupId() {
//		return marketGroupId;
//	}
//	/**
//	 * @param marketGroupId the marketGroupId to set
//	 */
//	public void setMarketGroupId(long marketGroupId) {
//		this.marketGroupId = marketGroupId;
//	} 
//			 
//	/**
//	 * @return the timeZoneString
//	 */
//	public String getTimeZoneString() {
//		return timeZoneString;
//	}
//	/**
//	 * @param timeZoneString the timeZoneString to set
//	 */
//	public void setTimeZoneString(String timeZoneString) {
//		this.timeZoneString = timeZoneString;
//	}
//	/**
//	 * @return the tradingDaysFormat
//	 */
//	public String getTradingDaysFormat() {
//		return tradingDaysFormat;
//	}
//	/**
//	 * @param tradingDaysFormat the tradingDaysFormat to set
//	 */
//	public void setTradingDaysFormat(String tradingDaysFormat) {
//		this.tradingDaysFormat = tradingDaysFormat;
//	}
//	/**
//	 * @return the optionPlusMarketId
//	 */
//	public long getOptionPlusMarketId() {
//		return optionPlusMarketId;
//	}
//	/**
//	 * @param optionPlusMarketId the optionPlusMarketId to set
//	 */
//	public void setOptionPlusMarketId(long optionPlusMarketId) {
//		this.optionPlusMarketId = optionPlusMarketId;
//	}
//	/**
//	 * @return the expiryFormula
//	 */
//	public String getExpiryFormula() {
//		return expiryFormula;
//	}
//	/**
//	 * @param expiryFormula the expiryFormula to set
//	 */
//	public void setExpiryFormula(String expiryFormula) {
//		this.expiryFormula = expiryFormula;
//	}
//	/**
//	 * @return the exchangeId
//	 */
//	public long getExchangeId() {
//		return exchangeId;
//	}
//	/**
//	 * @param exchangeId the exchangeId to set
//	 */
//	public void setExchangeId(long exchangeId) {
//		this.exchangeId = exchangeId;
//	}
//	/**
//	 * @return the reutersField
//	 */
//	public String getReutersField() {
//		return reutersField;
//	}
//	/**
//	 * @param reutersField the reutersField to set
//	 */
//	public void setReutersField(String reutersField) {
//		this.reutersField = reutersField;
//	}
//	
//}
