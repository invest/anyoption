/**
 *
 */
package com.anyoption.beans;

import java.io.Serializable;

/**
 * @author Eliran
 *
 */
public class PopulationDelay implements Serializable{

	private static final long serialVersionUID = 6611859155895330904L;

	private long id;
	private long popUserId;
	private int delayType;
	private int delayCount;
	private int maxDelayCount;
	private long delayActionId;


	/**
	 * @return the delayCount
	 */
	public int getDelayCount() {
		return delayCount;
	}
	/**
	 * @param delayCount the delayCount to set
	 */
	public void setDelayCount(int delayCount) {
		this.delayCount = delayCount;
	}
	/**
	 * @return the delayType
	 */
	public int getDelayType() {
		return delayType;
	}
	/**
	 * @param delayType the delayType to set
	 */
	public void setDelayType(int delayType) {
		this.delayType = delayType;
	}
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the maxDelayCount
	 */
	public int getMaxDelayCount() {
		return maxDelayCount;
	}
	/**
	 * @param maxDelayCount the maxDelayCount to set
	 */
	public void setMaxDelayCount(int maxDelayCount) {
		this.maxDelayCount = maxDelayCount;
	}
	/**
	 * @return the popUserId
	 */
	public long getPopUserId() {
		return popUserId;
	}
	/**
	 * @param popUserId the popUserId to set
	 */
	public void setPopUserId(long popUserId) {
		this.popUserId = popUserId;
	}
	/**
	 * @return the delayActionId
	 */
	public long getDelayActionId() {
		return delayActionId;
	}
	/**
	 * @param delayActionId the delayActionId to set
	 */
	public void setDelayActionId(long delayActionId) {
		this.delayActionId = delayActionId;
	}

	public boolean isPassedDelayMaxCount(){
		return (delayCount > maxDelayCount);
	}


}
