package com.anyoption.beans;

import java.util.Date;

public class InvestmentLimit extends com.anyoption.common.bl_vos.InvestmentLimit {

    
    /**
	 * 
	 */
	private static final long serialVersionUID = 409998041400657322L;
	
	public void setMinDouble(double m) {
    	minAmount = new Double(m * 100).longValue();
    }
    public double getMinDouble() {
    	return minAmount / 100;
    }
    public void setMaxDouble(double m) {
    	maxAmount = new Double(m * 100).longValue();
    }
    public double getMaxDouble() {
    	return maxAmount / 100;
    }

    /**
     * @return the currencyId
     */
    public Long getCurrencyId() {
        return currencyId;
    }

    /**
     * @param currencyId the currencyId to set
     */
    public void setCurrencyId(long currencyId) {
        this.currencyId = currencyId;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the max
     */
    public long getMaxAmount() {
        return maxAmount;
    }

    /**
     * @param max the max to set
     */
    public void setMaxAmount(long max) {
        this.maxAmount = max;
    }

    /**
     * @return the min
     */
    public long getMinAmount() {
        return minAmount;
    }

    /**
     * @param min the min to set
     */
    public void setMinAmount(long min) {
        this.minAmount = min;
    }

	/**
	 * @return the one_touch_min
	 */
	public long getOneTouchMin() {
		return oneTouchMin;
	}

	/**
	 * @param one_touch_min the one_touch_min to set
	 */
	public void setOneTouchMin(long oneTouchMin) {
		this.oneTouchMin = oneTouchMin;
	}

    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "InvestmentLimit" + ls
            + super.toString() + ls
            + "id = " + id + ls
            + "currencyId = " + currencyId + ls
            + "minAmount = " + minAmount + ls
            + "maxAmount = " + maxAmount + ls
            + "one touch min = " + oneTouchMin + ls;
    }

    /**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}



	/**
	 * @return the marketId
	 */
	public long getMarketId() {
		return marketId;
	}

	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	/**
	 * @return the scheduled
	 */
	public long getScheduled() {
		return scheduled;
	}

	/**
	 * @param scheduled the scheduled to set
	 */
	public void setScheduled(long scheduled) {
		this.scheduled = scheduled;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public int getInvLimitTypeId() {
		return invLimitTypeId;
	}

	public void setInvLimitTypeId(int invLimitTypeId) {
		this.invLimitTypeId = invLimitTypeId;
	}

	public String getMarketNameKey() {
		return marketNameKey;
	}

	public void setMarketNameKey(String marketNameKey) {
		this.marketNameKey = marketNameKey;
	}

	public long getOpportunityTypeId() {
		return opportunityTypeId;
	}

	public void setOpportunityTypeId(long opportunityTypeId) {
		this.opportunityTypeId = opportunityTypeId;
	}

	public String getOpportunityTypeName() {
		return opportunityTypeName;
	}

	public void setOpportunityTypeName(String opportunityTypeName) {
		this.opportunityTypeName = opportunityTypeName;
	}

	public long getPeriod() {
		return period;
	}

	public void setPeriod(long period) {
		this.period = period;
	}

	public String getStartDateHour() {
		return startDateHour;
	}

	public void setStartDateHour(String startDateHour) {
		this.startDateHour = startDateHour;
	}

	public String getStartDateMin() {
		return startDateMin;
	}

	public void setStartDateMin(String startDateMin) {
		this.startDateMin = startDateMin;
	}
	public long getMaxAmountGroupId() {
		return maxAmountGroupId;
	}
	public void setMaxAmountGroupId(long maxAmountGroupId) {
		this.maxAmountGroupId = maxAmountGroupId;
	}
	public long getMinAmountGroupId() {
		return minAmountGroupId;
	}
	public void setMinAmountGroupId(long minAmountGroupId) {
		this.minAmountGroupId = minAmountGroupId;
	}





}