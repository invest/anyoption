package com.anyoption.beans;

import java.sql.SQLException;
import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.Transaction;
import com.anyoption.managers.InvestmentsManagerBase;
import com.anyoption.managers.TransactionsManagerBase;
import com.anyoption.util.ConstantsBase;

public class RiskAlert implements java.io.Serializable {
	private static final Logger log = Logger.getLogger(RiskAlert.class);

	private static final long serialVersionUID = 1L;

	private long id;
	private long typeId;
	private long writerId;
	private Date timeCreated;
	private Date timeSettled;
	private Transaction transaction;
	private Investment investment;
	private long skinId;
	private long investmentId ;
	private long transactionId;
	private User user;
	private long riskAlertClassType;
	private long riskAlertType;
	private String riskAlertClassTypeDecription;
	private String riskAlertTypeDecription;
	private int succeededTrx;
	private int campaign_id;
    private String businessCaseName;

	public RiskAlert() {
		user = new User();
		succeededTrx = 0;
	}

	public RiskAlert(long typeId, long writerId, long transactionId, long investmentId) {
		this.typeId = typeId;
		this.writerId = writerId;
		this.transactionId = transactionId;
		this.investmentId = investmentId;
	}

	/**
	 * Get Transaction details
	 * @return
	 * @throws SQLException
	 */
	public Transaction getTransaction() throws SQLException {
		if (transaction == null) {
			log.debug("get transaction from db");
			transaction = TransactionsManagerBase.getTransaction(transactionId);
		}
		return transaction;
	}

	/**
	 * Get Investment details
	 * @return
	 * @throws SQLException
	 */
	public Investment getInvestment() throws SQLException {
		if (investment == null) {
			log.debug("get investment from db");
			investment = InvestmentsManagerBase.getInvestmentById(investmentId, ConstantsBase.PROFILE_LAST_INVESTMENTS_COUNT_LIMIT);
		}
		return investment;
	}


	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the typeId
	 */
	public long getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}


	/**
	 * @param transaction the transaction to set
	 */
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}


	/**
	 * @return the riskAlertClassType
	 */
	public long getRiskAlertClassType() {
		return riskAlertClassType;
	}

	/**
	 * @param riskAlertClassType the riskAlertClassType to set
	 */
	public void setRiskAlertClassType(long riskAlertClassType) {
		this.riskAlertClassType = riskAlertClassType;
	}

	/**
	 * @return the riskAlertType
	 */
	public long getRiskAlertType() {
		return riskAlertType;
	}

	/**
	 * @param riskAlertType the riskAlertType to set
	 */
	public void setRiskAlertType(long riskAlertType) {
		this.riskAlertType = riskAlertType;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}


	/**
	 * @return the riskAlertClassTypeDecription
	 */
	public String getRiskAlertClassTypeDecription() {
		return riskAlertClassTypeDecription;
	}

	/**
	 * @param riskAlertClassTypeDecription the riskAlertClassTypeDecription to set
	 */
	public void setRiskAlertClassTypeDecription(String riskAlertClassTypeDecription) {
		this.riskAlertClassTypeDecription = riskAlertClassTypeDecription;
	}

	/**
	 * @return the riskAlertTypeDecription
	 */
	public String getRiskAlertTypeDecription() {
		return riskAlertTypeDecription;
	}

	/**
	 * @param riskAlertTypeDecription the riskAlertTypeDecription to set
	 */
	public void setRiskAlertTypeDecription(String riskAlertTypeDecription) {
		this.riskAlertTypeDecription = riskAlertTypeDecription;
	}

	/**
	 * @return the investmentId
	 */
	public long getInvestmentId() {
		return investmentId;
	}

	/**
	 * @param investmentId the investmentId to set
	 */
	public void setInvestmentId(long investmentId) {
		this.investmentId = investmentId;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the succeededTrx
	 */
	public int getSucceededTrx() {
		return succeededTrx;
	}

	/**
	 * @param succeededTrx the succeededTrx to set
	 */
	public void setSucceededTrx(int succeededTrx) {
		this.succeededTrx = succeededTrx;
	}

	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @param investment the investment to set
	 */
	public void setInvestment(Investment investment) {
		this.investment = investment;
	}

	/**
	 * @return the campaign_id
	 */
	public int getCampaign_id() {
		return campaign_id;
	}

	/**
	 * @param campaign_id the campaign_id to set
	 */
	public void setCampaign_id(int campaign_id) {
		this.campaign_id = campaign_id;
	}


	/**
	 * @return the businessCaseName
	 */
	public String getBusinessCaseName() {
		return businessCaseName;
	}

	/**
	 * @param businessCaseName the businessCaseName to set
	 */
	public void setBusinessCaseName(String businessCaseName) {
		this.businessCaseName = businessCaseName;
	}

	/**
	 * @return the timeSettled
	 */
	public Date getTimeSettled() {
		return timeSettled;
	}

	/**
	 * @param timeSettled the timeSettled to set
	 */
	public void setTimeSettled(Date timeSettled) {
		this.timeSettled = timeSettled;
	}

	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "RiskAlert" + ls
            + super.toString() + ls
            + "id: " + id + ls
            + "typeId: " + typeId + ls
            + "transactionId: " + transactionId + ls
            + "investmentId: " + investmentId + ls
            + "userId: " + user.getId() + ls
            + "writerId: " + writerId + ls;
    }
}