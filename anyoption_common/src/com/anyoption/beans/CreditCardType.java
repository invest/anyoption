//package com.anyoption.beans;
//
//import java.sql.SQLException;
//import java.util.HashMap;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.managers.CreditCardTypesManagerBase;
//import com.anyoption.util.ConstantsBase;
//
//public class CreditCardType extends com.anyoption.beans.base.CreditCardType {
//
//	private static final Logger log = Logger.getLogger(CreditCardType.class);
//    protected static HashMap<Long, CreditCardType> creditCardTypesET;
//    protected static HashMap<Long, CreditCardType> creditCardTypesAO;
//
//    /**
//     * @param skinId
//     * @return
//     */
//    public static HashMap<Long, CreditCardType> getCreditCardTypes(long skinId) {
//    	if (skinId == ConstantsBase.SKIN_ETRADER) {
//    		creditCardTypesET = getCreditCardTypes(creditCardTypesET, skinId);
//        	return creditCardTypesET;
//    	} else {
//    		creditCardTypesAO = getCreditCardTypes(creditCardTypesAO, skinId);
//    		return creditCardTypesAO;
//    	}
//    }
//
//    /**
//     * @param creditCardTypes
//     * @param skinId
//     * @return
//     */
//    public static HashMap<Long, CreditCardType> getCreditCardTypes(HashMap<Long, CreditCardType> creditCardTypes, long skinId) {
//    	if (null == creditCardTypes) {
//    		  try {
//    			  creditCardTypes = CreditCardTypesManagerBase.getBySkin(skinId);
//  	        } catch (SQLException sqle) {
//  	            log.error("Can't load creditCard types.", sqle);
//  	        }
//      	}
//      	return creditCardTypes;
//    }
//
//	/* (non-Javadoc)
//	 * @see java.lang.Object#toString()
//	 */
//	public String toString() {
//	    String ls = System.getProperty("line.separator");
//	    return ls + "CreditCardType:" + ls
//	        + "id: " + id + ls
//	        + "name: " + name + ls
//            + "threeDSecure (3D): " + threeDSecure + ls;
//	}
//}