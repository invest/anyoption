package com.anyoption.beans;

public class MobileVersion {
    private Integer apkVersion;
    private Boolean forceUpdate;
    private String downloadLink;
	/**
	 * @return the apkVersion
	 */
	public Integer getApkVersion() {
		return apkVersion;
	}
	/**
	 * @param apkVersion the apkVersion to set
	 */
	public void setApkVersion(Integer apkVersion) {
		this.apkVersion = apkVersion;
	}
	/**
	 * @return the downloadLink
	 */
	public String getDownloadLink() {
		return downloadLink;
	}
	/**
	 * @param downloadLink the downloadLink to set
	 */
	public void setDownloadLink(String downloadLink) {
		this.downloadLink = downloadLink;
	}
	/**
	 * @return the forceUpdate
	 */
	public Boolean getForceUpdate() {
		return forceUpdate;
	}
	/**
	 * @param forceUpdate the forceUpdate to set
	 */
	public void setForceUpdate(Boolean forceUpdate) {
		this.forceUpdate = forceUpdate;
	}

	public String toString() {
		final String newLine = " \n ";
		return " apk Version = " + apkVersion + newLine +
			   " download link = " + downloadLink + newLine +
			   " force update = " + forceUpdate;
	}

}