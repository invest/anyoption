//package com.anyoption.beans;
//
//import java.math.BigDecimal;
//import java.sql.SQLException;
//import java.util.Date;
//import java.util.Hashtable;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.managers.MarketsManagerBase;
//
//public class Market extends com.anyoption.beans.base.Market implements Cloneable {
//    private static final Logger log = Logger.getLogger(Market.class);
//
//    /**
//     * Use the fixed value in the markets.fixed_amount_for_shifting in the
//     * exposure calculations.
//     */
//    public static final int TYPE_OF_SHIFTING_FIXED_AMOUNT = 0;
//
//    /**
//     * Use the auto calculated average investment in the last 30 days
//     * (markets.average_investments) in exposure calculations.
//     */
//    public static final int TYPE_OF_SHIFTING_AVERAGE_INVESTMENT = 1;
//
//    /**
//     * Use the higher of the fixed markets.average_investments and
//     * markets.fixed_amount_for_shifting in the
//     * exposure calculations.
//     */
//    public static final int TYPE_OF_SHIFTING_MAX_OF_BOTH = 2;
//
//    public static final int MARKET_TEL_AVIV_25_ID   = 3;
//    public static final int MARKET_FTSE_ID          = 4;
//    public static final int MARKET_CAC_ID           = 10;
//    public static final int MARKET_USD_ILS_ID       = 15;
//    public static final int MARKET_USD_TRY_ID       = 17;
//    public static final int MARKET_USD_JPY_ID       = 18;
//    public static final int MARKET_GOLD_ID          = 20;
//    public static final int MARKET_OIL_ID           = 21;
//    public static final int MARKET_USD_RAND_ID      = 26;
//    public static final int MARKET_NASDAQF_ID       = 136;
//    public static final int MARKET_SILVER_ID        = 137;
//    public static final int MARKET_COPPER_ID        = 138;
//    public static final int MARKET_SHANGHAI_ID      = 294;
//    public static final int MARKET_JAKARTA_ID		= 370;
//    public static final int MARKET_ENERGY_ISRAEL_ID	= 435;
//    public static final int MARKET_TEL_AVIV_25_MAOF_ID = 436;
//    public static final int MARKET_ISE30_INDEX_ID = 437;
//    public static final int MARKET_KLSE_FUTURE_ID = 477 ;
//    public static final int MARKET_VIX_ID = 538;
//
//  //  private static Hashtable<Long, Market> markets;
//
//	private String name;
//	private String displayNameKey;
//	private String feedName;
//	private Date timeCreated;
//	private long writerId;
//	private Long decimalPoint;
//    private Long groupId;
//    private Long investmentLimitsGroupId;
//    private String groupName;
//    private Long exchangeId;
//    private BigDecimal currentLevelAlertPerc;
//
//    private int updatesPause;
//    private BigDecimal significantPercentage;
//    private int realUpdatesPause;
//    private BigDecimal realSignificantPercentage;
//    private BigDecimal randomCeiling;
//    private BigDecimal randomFloor;
//    private BigDecimal firstShiftParameter;
//    private BigDecimal averageInvestments;
//    private BigDecimal percentageOfAverageInvestments;
//    private BigDecimal fixedAmountForShifting;
//    private BigDecimal acceptableDeviation;
//    private BigDecimal acceptableDeviation3;
//    private Long typeOfShifting;
//    private Long disableAfterPeriod;
//    private boolean suspended;
//    private String suspendedMessage;
//    private boolean hasFutureAgreement;
//    private boolean nextAgreementSameDay;
//
//    private String exchangeName;
//    private String investmentLimitsGroupName;
//
//    // banner_priority used to tell us the priority of this market
//    private int bannersPriority;
//
//    private String timeFirstInvest;
//    private String timeEstClosing;
//    private String timeZone;
//
//    private boolean haveHourly;
//    private boolean havedayly;
//    private boolean haveweekly;
//    private boolean havemonthly;
//    private boolean isHasLongTermOpp;
//
//    private int secBetweenInvestments;
//    private int secBetweenInvestmentsMobile;
//    private int secForDev2;
//    private int secForDev2Mobile;
//    private float amountForDev2;
//    private float amountForDev2Night;
//    private float amountForDev3;
//
//	private int maxExposure;
//    // frequencePriority used to tell the frequence of level change when user not login bugId: 2029
//    private Date lastHourClosingTime;
//    private boolean opened;
//
//    //for 5 golden minutse
//    private double insurancePremiaPercent;
//    private double insuranceQualifyPercent;
//    private boolean isGoldenMinutes;
//
////  for roll up
//    private double rollUpPremiaPercent;
//    private double rollUpQualifyPercent;
//    private boolean isRollUp;
//
//    private double noAutoSettleAfter;
//
//    private long optionPlusFee;
//    private String tradingDays;  // assetIndex
//    private String displayGroupNameKey; // assetIndex
//    private int groupMarketsCounter; // assetIndex
//    private long optionPlusMarketId;
//
//    private int secBetweenInvestmentsSameIp;
//    private int secBetweenInvestmentsSameIpMobile;
//    private int decimalPointSubtractDigits;
//
//    private boolean isFullDay;    // for mobile assetIndex use
//    private boolean isOptionPlus;
//
////  for 1T
//    private long exposureReached;
//    private long raiseExposureUpAmount;
//    private long raiseExposureDownAmount;
//    private double upStrikeRaiseLowerPercent;
//    private double downStrikeRaiseLowerPercent;
//
//	public Long getDecimalPoint() {
//		return decimalPoint;
//	}
//
//	public void setDecimalPoint(Long decimalPoint) {
//		this.decimalPoint = decimalPoint;
//	}
//
//	public String getFeedName() {
//		return feedName;
//	}
//
//	public void setFeedName(String feedName) {
//		this.feedName = feedName;
//	}
//
//	/**
//     * @return the investmentLimitsGroupId
//     */
//    public Long getInvestmentLimitsGroupId() {
//        return investmentLimitsGroupId;
//    }
//
//    /**
//     * @param investmentLimitsGroupId the investmentLimitsGroupId to set
//     */
//    public void setInvestmentLimitsGroupId(Long investmentLimitsGroupId) {
//        this.investmentLimitsGroupId = investmentLimitsGroupId;
//    }
//
//    public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public BigDecimal getRealSignificantPercentage() {
//        return realSignificantPercentage;
//    }
//
//    public void setRealSignificantPercentage(BigDecimal realSignificantPercentage) {
//        this.realSignificantPercentage = realSignificantPercentage;
//    }
//
//    public int getRealUpdatesPause() {
//        return realUpdatesPause;
//    }
//
//    public void setRealUpdatesPause(int realUpdatesPause) {
//        this.realUpdatesPause = realUpdatesPause;
//    }
//
//    public BigDecimal getSignificantPercentage() {
//        return significantPercentage;
//    }
//
//    public void setSignificantPercentage(BigDecimal significantPercentage) {
//        this.significantPercentage = significantPercentage;
//    }
//
//    public Date getTimeCreated() {
//		return timeCreated;
//	}
//
//	public void setTimeCreated(Date timeCreated) {
//		this.timeCreated = timeCreated;
//	}
//
//	public String getGroupName() {
//		return groupName;
//	}
//
//	public long getWriterId() {
//		return writerId;
//	}
//
//	public void setWriterId(long writerId) {
//		this.writerId = writerId;
//	}
//
//    /**
//     * @return Returns the groupId.
//     */
//    public Long getGroupId() {
//        return groupId;
//    }
//
//    /**
//     * @param groupId The groupId to set.
//     */
//    public void setGroupId(Long groupId) {
//        this.groupId = groupId;
//    }
//
//    public void setGroupName(String groupName) {
//		this.groupName = groupName;
//	}
//
//	public Long getExchangeId() {
//		return exchangeId;
//	}
//
//	public void setExchangeId(Long exchangeId) {
//		this.exchangeId = exchangeId;
//	}
//
//	public String getExchangeName() {
//		return exchangeName;
//	}
//
//	public void setExchangeName(String exchangeName) {
//		this.exchangeName = exchangeName;
//	}
//
//	public String getInvestmentLimitsGroupName() {
//		return investmentLimitsGroupName;
//	}
//
//	public void setInvestmentLimitsGroupName(String investmentLimitsGroupName) {
//		this.investmentLimitsGroupName = investmentLimitsGroupName;
//	}
//
//	public int getUpdatesPause() {
//        return updatesPause;
//    }
//
//    public void setUpdatesPause(int updatesPause) {
//        this.updatesPause = updatesPause;
//    }
//
//    public BigDecimal getAverageInvestments() {
//        return averageInvestments;
//    }
//
//    public void setAverageInvestments(BigDecimal averageInvestments) {
//        this.averageInvestments = averageInvestments;
//    }
//
//    public BigDecimal getFirstShiftParameter() {
//        return firstShiftParameter;
//    }
//
//    public void setFirstShiftParameter(BigDecimal firstShiftParameter) {
//        this.firstShiftParameter = firstShiftParameter;
//    }
//
//    public BigDecimal getFixedAmountForShifting() {
//        return fixedAmountForShifting;
//    }
//
//    public void setFixedAmountForShifting(BigDecimal fixedAmountForShifting) {
//        this.fixedAmountForShifting = fixedAmountForShifting;
//    }
//
//    public BigDecimal getPercentageOfAverageInvestments() {
//        return percentageOfAverageInvestments;
//    }
//
//    public void setPercentageOfAverageInvestments(
//            BigDecimal percentageOfAverageInvestments) {
//        this.percentageOfAverageInvestments = percentageOfAverageInvestments;
//    }
//
//    public BigDecimal getRandomCeiling() {
//        return randomCeiling;
//    }
//
//    public void setRandomCeiling(BigDecimal randomCeiling) {
//        this.randomCeiling = randomCeiling;
//    }
//
//    public BigDecimal getRandomFloor() {
//        return randomFloor;
//    }
//
//    public void setRandomFloor(BigDecimal randomFloor) {
//        this.randomFloor = randomFloor;
//    }
//
//    public Long getTypeOfShifting() {
//        return typeOfShifting;
//    }
//
//    public void setTypeOfShifting(Long typeOfShifting) {
//        this.typeOfShifting = typeOfShifting;
//    }
//
//    public Long getDisableAfterPeriod() {
//        return disableAfterPeriod;
//    }
//
//    public void setDisableAfterPeriod(Long disableAfterPeriod) {
//        this.disableAfterPeriod = disableAfterPeriod;
//    }
//
//	public boolean isSuspended() {
//        return suspended;
//    }
//
//    public void setSuspended(boolean suspended) {
//        this.suspended = suspended;
//    }
//
//    public String getSuspendedMessage() {
//        return suspendedMessage;
//    }
//
//    public void setSuspendedMessage(String suspendedMessage) {
//        this.suspendedMessage = suspendedMessage;
//    }
//
//    public String getDisplayNameKey() {
//		return displayNameKey;
//	}
//
//	public void setDisplayNameKey(String displayNameKey) {
//		this.displayNameKey = displayNameKey;
//	}
//
//	public float getAmountForDev2() {
//        return amountForDev2;
//    }
//
//    public void setAmountForDev2(float amountForDev2) {
//        this.amountForDev2 = amountForDev2;
//    }
//
//    public int getSecBetweenInvestments() {
//        return secBetweenInvestments;
//    }
//
//    public void setSecBetweenInvestments(int secBetweenInvestments) {
//        this.secBetweenInvestments = secBetweenInvestments;
//    }
//
//    public int getSecBetweenInvestmentsMobile() {
//        return secBetweenInvestmentsMobile;
//    }
//
//    public void setSecBetweenInvestmentsMobile(int secBetweenInvestmentsMobile) {
//        this.secBetweenInvestmentsMobile = secBetweenInvestmentsMobile;
//    }
//
//    public int getSecForDev2() {
//        return secForDev2;
//    }
//
//    public void setSecForDev2(int secForDev2) {
//        this.secForDev2 = secForDev2;
//    }
//
//	public int getSecForDev2Mobile() {
//        return secForDev2Mobile;
//    }
//
//    public void setSecForDev2Mobile(int secForDev2Mobile) {
//        this.secForDev2Mobile = secForDev2Mobile;
//    }
//
//    public BigDecimal getCurrentLevelAlertPerc() {
//		return currentLevelAlertPerc;
//	}
//
//	public void setCurrentLevelAlertPerc(BigDecimal currentLevelAlertPerc) {
//		this.currentLevelAlertPerc = currentLevelAlertPerc;
//	}
//
//	public BigDecimal getAcceptableDeviation() {
//        return acceptableDeviation;
//    }
//
//    public void setAcceptableDeviation(BigDecimal acceptableDeviation) {
//        this.acceptableDeviation = acceptableDeviation;
//    }
//
//    public boolean isHasFutureAgreement() {
//		return hasFutureAgreement;
//	}
//
//	public void setHasFutureAgreement(boolean hasFutureAgreement) {
//		this.hasFutureAgreement = hasFutureAgreement;
//	}
//
//	public boolean isNextAgreementSameDay() {
//		return nextAgreementSameDay;
//	}
//
//	public void setNextAgreementSameDay(boolean nextAgreementSameDay) {
//		this.nextAgreementSameDay = nextAgreementSameDay;
//	}
//
//	public int getBannersPriority() {
//		return bannersPriority;
//	}
//
//	public void setBannersPriority(int bannersPriority) {
//		this.bannersPriority = bannersPriority;
//	}
//
//	public boolean isHavedayly() {
//		return havedayly;
//	}
//
//	public void setHavedayly(boolean havedayly) {
//		this.havedayly = havedayly;
//	}
//
//	public boolean isHaveHourly() {
//		return haveHourly;
//	}
//
//	public void setHaveHourly(boolean haveHourly) {
//		this.haveHourly = haveHourly;
//	}
//
//	public boolean isHavemonthly() {
//		return havemonthly;
//	}
//
//	public void setHavemonthly(boolean havemonthly) {
//		this.havemonthly = havemonthly;
//	}
//
//	public boolean isHaveweekly() {
//		return haveweekly;
//	}
//
//	public void setHaveweekly(boolean haveweekly) {
//		this.haveweekly = haveweekly;
//	}
//
//	public String getTimeEstClosing() {
//		return timeEstClosing;
//	}
//
//	public void setTimeEstClosing(String timeEstClosing) {
//		this.timeEstClosing = timeEstClosing;
//	}
//
//	public String getTimeFirstInvest() {
//		return timeFirstInvest;
//	}
//
//	public void setTimeFirstInvest(String timeFirstInvest) {
//		this.timeFirstInvest = timeFirstInvest;
//	}
//
//	public String getTimeZone() {
//		return timeZone;
//	}
//
//	public void setTimeZone(String timeZone) {
//		this.timeZone = timeZone;
//	}
//
//	public Date getLastHourClosingTime() {
//        return lastHourClosingTime;
//    }
//
//    public void setLastHourClosingTime(Date lastHourClosingTime) {
//        this.lastHourClosingTime = lastHourClosingTime;
//    }
//
//    public boolean isOpened() {
//        return opened;
//    }
//
//    public void setOpened(boolean opened) {
//        this.opened = opened;
//    }
//
//    public int getMaxExposure() {
//        return maxExposure;
//    }
//
//    public void setMaxExposure(int maxExposure) {
//        this.maxExposure = maxExposure;
//    }
//
//    /**
//     * @return the insurancePremiaPercent
//     */
//    public double getInsurancePremiaPercent() {
//        return insurancePremiaPercent;
//    }
//
//    /**
//     * @param insurancePremiaPercent the insurancePremiaPercent to set
//     */
//    public void setInsurancePremiaPercent(double insurancePremiaPercent) {
//        this.insurancePremiaPercent = insurancePremiaPercent;
//    }
//
//    /**
//     * @return the insuranceQualifyPercent
//     */
//    public double getInsuranceQualifyPercent() {
//        return insuranceQualifyPercent;
//    }
//
//    /**
//     * @param insuranceQualifyPercent the insuranceQualifyPercent to set
//     */
//    public void setInsuranceQualifyPercent(double insuranceQualifyPercent) {
//        this.insuranceQualifyPercent = insuranceQualifyPercent;
//    }
//
//    /**
//     * @return the isGoldenMinutes
//     */
//    public boolean isGoldenMinutes() {
//        return isGoldenMinutes;
//    }
//
//    /**
//     * @param isGoldenMinutes the isGoldenMinutes to set
//     */
//    public void setGoldenMinutes(boolean isGoldenMinutes) {
//        this.isGoldenMinutes = isGoldenMinutes;
//    }
//
//    /**
//     * @return the isRollUp
//     */
//    public boolean isRollUp() {
//        return isRollUp;
//    }
//
//    /**
//     * @param isRollUp the isRollUp to set
//     */
//    public void setRollUp(boolean isRollUp) {
//        this.isRollUp = isRollUp;
//    }
//
//    /**
//     * @return the rollUpPremiaPercent
//     */
//    public double getRollUpPremiaPercent() {
//        return rollUpPremiaPercent;
//    }
//
//    /**
//     * @param rollUpPremiaPercent the rollUpPremiaPercent to set
//     */
//    public void setRollUpPremiaPercent(double rollUpPremiaPercent) {
//        this.rollUpPremiaPercent = rollUpPremiaPercent;
//    }
//
//    /**
//     * @return the rollUpQualifyPercent
//     */
//    public double getRollUpQualifyPercent() {
//        return rollUpQualifyPercent;
//    }
//
//    /**
//     * @param rollUpQualifyPercent the rollUpQualifyPercent to set
//     */
//    public void setRollUpQualifyPercent(double rollUpQualifyPercent) {
//        this.rollUpQualifyPercent = rollUpQualifyPercent;
//    }
//
//    /**
//     * @return the isHasLongTerm
//     */
//    public boolean isHasLongTermOpp() {
//        return isHasLongTermOpp;
//    }
//
//    /**
//     * @param isHasLongTerm the isHasLongTerm to set
//     */
//    public void setHasLongTerm(boolean isHasLongTermOpp) {
//        this.isHasLongTermOpp = isHasLongTermOpp;
//    }
//
//    /**
//     * @return the noAutoSettleAfter
//     */
//    public double getNoAutoSettleAfter() {
//        return noAutoSettleAfter;
//    }
//
//    /**
//     * @param noAutoSettleAfter the noAutoSettleAfter to set
//     */
//    public void setNoAutoSettleAfter(double noAutoSettleAfter) {
//        this.noAutoSettleAfter = noAutoSettleAfter;
//    }
//
////    public static Market getMarket(long id) {
////        if (null == markets) {
////            try {
////                markets = MarketsManagerBase.getAll();
////            } catch (SQLException sqle) {
////                log.error("Can't load markets.", sqle);
////            }
////        }
////        return markets.get(id);
////    }
//
//    /**
//     * @return the exposureReached
//     */
//    public long getExposureReached() {
//        return exposureReached;
//    }
//    /**
//     * @param exposureReached the exposureReached to set
//     */
//    public void setExposureReached(long exposureReached) {
//        this.exposureReached = exposureReached;
//    }
//    /**
//     * @return the raiseExposureUpAmount
//     */
//    public long getRaiseExposureUpAmount() {
//        return raiseExposureUpAmount;
//    }
//    /**
//     * @param raiseExposureUpAmount the raiseExposureUpAmount to set
//     */
//    public void setRaiseExposureUpAmount(long raiseExposureUpAmount) {
//        this.raiseExposureUpAmount = raiseExposureUpAmount;
//    }
//    /**
//     * @return the raiseExposureDownAmount
//     */
//    public long getRaiseExposureDownAmount() {
//        return raiseExposureDownAmount;
//    }
//    /**
//     * @param raiseExposureDownAmount the raiseExposureDownAmount to set
//     */
//    public void setRaiseExposureDownAmount(long raiseExposureDownAmount) {
//        this.raiseExposureDownAmount = raiseExposureDownAmount;
//    }
//    /**
//     * @return the upStrikeRaiseLowerPercent
//     */
//    public double getUpStrikeRaiseLowerPercent() {
//        return upStrikeRaiseLowerPercent;
//    }
//    /**
//     * @param upStrikeRaiseLowerPercent the upStrikeRaiseLowerPercent to set
//     */
//    public void setUpStrikeRaiseLowerPercent(double upStrikeRaiseLowerPercent) {
//        this.upStrikeRaiseLowerPercent = upStrikeRaiseLowerPercent;
//    }
//    /**
//     * @return the downStrikeRaiseLowerPercent
//     */
//    public double getDownStrikeRaiseLowerPercent() {
//        return downStrikeRaiseLowerPercent;
//    }
//    /**
//     * @param downStrikeRaiseLowerPercent the downStrikeRaiseLowerPercent to set
//     */
//    public void setDownStrikeRaiseLowerPercent(double downStrikeRaiseLowerPercent) {
//        this.downStrikeRaiseLowerPercent = downStrikeRaiseLowerPercent;
//    }
//
//    /**
//     * @return the isOptionPlus
//     */
//    public boolean isOptionPlus() {
//        return isOptionPlus;
//    }
//
//    /**
//     * @param isOptionPlus the isOptionPlus to set
//     */
//    public void setOptionPlus(boolean isOptionPlus) {
//        this.isOptionPlus = isOptionPlus;
//    }
//
//	/**
//	 * @return the isFullDay
//	 */
//	public boolean isFullDay() {
//		return isFullDay;
//	}
//	/**
//	 * @param isFullDay the isFullDay to set
//	 */
//	public void setFullDay(boolean isFullDay) {
//		this.isFullDay = isFullDay;
//	}
//
//    /**
//	 * @return the decimalPointSubtractDigits
//	 */
//	public int getDecimalPointSubtractDigits() {
//		return decimalPointSubtractDigits;
//	}
//
//	/**
//	 * @param decimalPointSubtractDigits the decimalPointSubtractDigits to set
//	 */
//	public void setDecimalPointSubtractDigits(int decimalPointSubtractDigits) {
//		this.decimalPointSubtractDigits = decimalPointSubtractDigits;
//	}
//
//	/**
//	 * @return the displayGroupNameKey
//	 */
//	public String getDisplayGroupNameKey() {
//		return displayGroupNameKey;
//	}
//
//	/**
//	 * @param displayGroupNameKey the displayGroupNameKey to set
//	 */
//	public void setDisplayGroupNameKey(String displayGroupNameKey) {
//		this.displayGroupNameKey = displayGroupNameKey;
//	}
//
//	/**
//	 * @return the groupMarketsCounter
//	 */
//	public int getGroupMarketsCounter() {
//		return groupMarketsCounter;
//	}
//
//	/**
//	 * @param groupMarketsCounter the groupMarketsCounter to set
//	 */
//	public void setGroupMarketsCounter(int groupMarketsCounter) {
//		this.groupMarketsCounter = groupMarketsCounter;
//	}
//
//	/**
//	 * @return the optionPlusFee
//	 */
//	public long getOptionPlusFee() {
//		return optionPlusFee;
//	}
//
//	/**
//	 * @param optionPlusFee the optionPlusFee to set
//	 */
//	public void setOptionPlusFee(long optionPlusFee) {
//		this.optionPlusFee = optionPlusFee;
//	}
//
//	/**
//	 * @return the optionPlusMarketId
//	 */
//	public long getOptionPlusMarketId() {
//		return optionPlusMarketId;
//	}
//
//	/**
//	 * @param optionPlusMarketId the optionPlusMarketId to set
//	 */
//	public void setOptionPlusMarketId(long optionPlusMarketId) {
//		this.optionPlusMarketId = optionPlusMarketId;
//	}
//
//	/**
//	 * @return the secBetweenInvestmentsSameIp
//	 */
//	public int getSecBetweenInvestmentsSameIp() {
//		return secBetweenInvestmentsSameIp;
//	}
//
//	/**
//	 * @param secBetweenInvestmentsSameIp the secBetweenInvestmentsSameIp to set
//	 */
//	public void setSecBetweenInvestmentsSameIp(int secBetweenInvestmentsSameIp) {
//		this.secBetweenInvestmentsSameIp = secBetweenInvestmentsSameIp;
//	}
//
//    public int getSecBetweenInvestmentsSameIpMobile() {
//        return secBetweenInvestmentsSameIpMobile;
//    }
//
//    public void setSecBetweenInvestmentsSameIpMobile(
//            int secBetweenInvestmentsSameIpMobile) {
//        this.secBetweenInvestmentsSameIpMobile = secBetweenInvestmentsSameIpMobile;
//    }
//
//    /**
//	 * @return the tradingDays
//	 */
//	public String getTradingDays() {
//		return tradingDays;
//	}
//
//	/**
//	 * @param tradingDays the tradingDays to set
//	 */
//	public void setTradingDays(String tradingDays) {
//		this.tradingDays = tradingDays;
//	}
//
//	/**
//	 * @param isHasLongTermOpp the isHasLongTermOpp to set
//	 */
//	public void setHasLongTermOpp(boolean isHasLongTermOpp) {
//		this.isHasLongTermOpp = isHasLongTermOpp;
//	}
//
//	public BigDecimal getAcceptableDeviation3() {
//		return acceptableDeviation3;
//	}
//
//	public void setAcceptableDeviation3(BigDecimal acceptableDeviation3) {
//		this.acceptableDeviation3 = acceptableDeviation3;
//	}
//
//	public float getAmountForDev2Night() {
//		return amountForDev2Night;
//	}
//
//	public void setAmountForDev2Night(float amountForDev2Night) {
//		this.amountForDev2Night = amountForDev2Night;
//	}
//
//	public float getAmountForDev3() {
//		return amountForDev3;
//	}
//
//	public void setAmountForDev3(float amountForDev3) {
//		this.amountForDev3 = amountForDev3;
//	}
//
//	public String toString() {
//        String ls = System.getProperty("line.separator");
//        return ls + "Market" + ls
//            + super.toString() + ls
//            + "id: " + id + ls
//            + "name: " + name + ls
//            + "displayName: " + displayName + ls
//            + "displayNameKey: " + displayNameKey + ls
//            + "feedName: " + feedName + ls
//            + "timeCreated: " + timeCreated + ls
//            + "writerId: " + writerId + ls
//            + "decimalPoint: " + decimalPoint + ls
//            + "groupId: " + groupId + ls
//            + "investmentLimitsGroupId: " + investmentLimitsGroupId + ls
//            + "groupName: " + groupName + ls
//            + "exchangeId: " + exchangeId + ls
//            + "updatesPause: " + updatesPause + ls
//            + "significantPercentage: " + significantPercentage + ls
//            + "realUpdatesPause: " + realUpdatesPause + ls
//            + "realSignificantPercentage: " + realSignificantPercentage + ls
//            + "randomCeiling: " + randomCeiling + ls
//            + "randomFloor: " + randomFloor + ls
//            + "firstShiftParameter: " + firstShiftParameter + ls
//            + "acceptableDeviation: " + acceptableDeviation + ls
//            + "acceptableDeviation3: " + acceptableDeviation3 + ls
//            + "averageInvestments: " + averageInvestments + ls
//            + "percentageOfAverageInvestments: " + percentageOfAverageInvestments + ls
//            + "fixedAmountForShifting: " + fixedAmountForShifting + ls
//            + "typeOfShifting: " + typeOfShifting + ls
//            + "disableAfterPeriod: " + disableAfterPeriod + ls
//            + "suspended: " + suspended + ls
//            + "suspendedMessage: " + suspendedMessage + ls
//            + "exchangeName: " + exchangeName + ls
//            + "investmentLimitsGroupName: " + investmentLimitsGroupName + ls
//            + "bannersPriority: " + bannersPriority + ls
//            + "secBetweenInvestments: " + secBetweenInvestments + ls
//            + "secForDev2: " + secForDev2 + ls
//            + "amountForDev2: " + amountForDev2 + ls
//            + "lastHourClosingTime: " + lastHourClosingTime + ls
//            + "opened: " + opened + ls
//            + "isHasLongTermOpp: " + isHasLongTermOpp + ls
//            + "noAutoSettleAfter: " + noAutoSettleAfter + ls;
//    }
//}