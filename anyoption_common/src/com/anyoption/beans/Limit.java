package com.anyoption.beans;

import java.io.Serializable;
import java.util.Date;

public class Limit implements Serializable {
	private long id;
	private String name;
	private long minDeposit;
	private long minWithdraw;
	private long minBankWire;
	private long maxDeposit;
	private long maxWithdraw;
	private long maxBankWire;
	private long maxDepositPerDay;
	private long maxDepositPerMonth;
	private Date timeLastUpdate;
	private Date timeCreated;
	private long writerId;
	private String comments;
	private long freeInvestMin;
	private long freeInvestMax;
	private long bankWireFee;
	private long ccFee;
	private long chequeFee;
	private long depositDocsLimit1;
	private long depositDocsLimit2;
	private long minFirstDeposit;

	private String writerName;
	private long currencyId;
	private int skinId;
	private boolean dflt;
	private String skinName;
	private String currencyName;
	private long amountForLowWithdraw;

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getMaxDeposit() {
		return maxDeposit;
	}

	public void setMaxDeposit(long maxDeposit) {
		this.maxDeposit = maxDeposit;
	}

	public long getMaxDepositPerDay() {
		return maxDepositPerDay;
	}

	public void setMaxDepositPerDay(long maxDepositPerDay) {
		this.maxDepositPerDay = maxDepositPerDay;
	}

	public long getMaxDepositPerMonth() {
		return maxDepositPerMonth;
	}

	public void setMaxDepositPerMonth(long maxDepositPerMonth) {
		this.maxDepositPerMonth = maxDepositPerMonth;
	}

	public long getMaxWithdraw() {
		return maxWithdraw;
	}

	public void setMaxWithdraw(long maxWithdraw) {
		this.maxWithdraw = maxWithdraw;
	}

	public long getMinDeposit() {
		return minDeposit;
	}

	public void setMinDeposit(long minDeposit) {
		this.minDeposit = minDeposit;
	}

	public long getMinWithdraw() {
		return minWithdraw;
	}

	public void setMinWithdraw(long minWithdraw) {
		this.minWithdraw = minWithdraw;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getTimeLastUpdate() {
		return timeLastUpdate;
	}
	
	public void setTimeLastUpdate(Date timeLastUpdate) {
		this.timeLastUpdate = timeLastUpdate;
	}
	
	public long getWriterId() {
		return writerId;
	}
	
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	
	public String getWriterName() {
		return writerName;
	}
	
	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}
	
	public boolean isDefault() {
		return dflt;
	}

	public void setDefault(boolean dflt) {
	    this.dflt = dflt;
	}

	/**
	 * @return the maxBankWire
	 */
	public long getMaxBankWire() {
		return maxBankWire;
	}

	/**
	 * @param maxBankWire the maxBankWire to set
	 */
	public void setMaxBankWire(long maxBankWire) {
		this.maxBankWire = maxBankWire;
	}

	/**
	 * @return the minBankWire
	 */
	@Deprecated
	public long getMinBankWire() {
		return minBankWire;
	}

	/**
	 * @param minBankWire the minBankWire to set
	 */
	public void setMinBankWire(long minBankWire) {
		this.minBankWire = minBankWire;
	}

	/**
	 * @return the freeInvestMax
	 */
	public long getFreeInvestMax() {
		return freeInvestMax;
	}

	/**
	 * @param freeInvestMax the freeInvestMax to set
	 */
	public void setFreeInvestMax(long freeInvestMax) {
		this.freeInvestMax = freeInvestMax;
	}

	/**
	 * @return the freeInvestMin
	 */
	public long getFreeInvestMin() {
		return freeInvestMin;
	}

	/**
	 * @param freeInvestMin the freeInvestMin to set
	 */
	public void setFreeInvestMin(long freeInvestMin) {
		this.freeInvestMin = freeInvestMin;
	}

	/**
	 * @return the bankWireFee
	 */
	public long getBankWireFee() {
		return bankWireFee;
	}

	/**
	 * @param bankWireFee the bankWireFee to set
	 */
	public void setBankWireFee(long bankWireFee) {
		this.bankWireFee = bankWireFee;
	}

	/**
	 * @return the ccFee
	 */
	public long getCcFee() {
		return ccFee;
	}

	/**
	 * @param ccFee the ccFee to set
	 */
	public void setCcFee(long ccFee) {
		this.ccFee = ccFee;
	}

	/**
	 * @return the chequeFee
	 */
	public long getChequeFee() {
		return chequeFee;
	}

	/**
	 * @param chequeFee the chequeFee to set
	 */
	public void setChequeFee(long chequeFee) {
		this.chequeFee = chequeFee;
	}

	/**
	 * @return the depositDocsLimit1
	 */
	public long getDepositDocsLimit1() {
		return depositDocsLimit1;
	}

	/**
	 * @param depositDocsLimit1 the depositDocsLimit1 to set
	 */
	public void setDepositDocsLimit1(long depositDocsLimit1) {
		this.depositDocsLimit1 = depositDocsLimit1;
	}

	/**
	 * @return the depositDocsLimit2
	 */
	public long getDepositDocsLimit2() {
		return depositDocsLimit2;
	}

	/**
	 * @param depositDocsLimit2 the depositDocsLimit2 to set
	 */
	public void setDepositDocsLimit2(long depositDocsLimit2) {
		this.depositDocsLimit2 = depositDocsLimit2;
	}

	/**
	 * @return the currencyId
	 */
	public long getCurrencyId() {
		return currencyId;
	}

	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the skinId
	 */
	public int getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the skinName
	 */
	public String getSkinName() {
		return skinName;
	}

	/**
	 * @param skinName the skinName to set
	 */
	public void setSkinName(String skinName) {
		this.skinName = skinName;
	}

	/**
	 * @return the currencyName
	 */
	public String getCurrencyName() {
		return currencyName;
	}

	/**
	 * @param currencyName the currencyName to set
	 */
	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	/**
	 * @return the minFirstDeposit
	 */
	public long getMinFirstDeposit() {
		return minFirstDeposit;
	}

	/**
	 * @param minFirstDeposit the minFirstDeposit to set
	 */
	public void setMinFirstDeposit(long minFirstDeposit) {
		this.minFirstDeposit = minFirstDeposit;
	}

	public long getAmountForLowWithdraw() {
		return amountForLowWithdraw;
	}

	public void setAmountForLowWithdraw(long amountForLowWithdraw) {
		this.amountForLowWithdraw = amountForLowWithdraw;
	}

	public String toString() {
	    String ls = System.getProperty("line.separator");
	    return ls + "Limit" + ls
	        + super.toString() + ls
	        + "id: " + id + ls
	        + "name: " + name + ls
	        + "minDeposit: " + minDeposit + ls
	        + "minWithdraw: " + minWithdraw + ls
	        + "maxDeposit: " + maxDeposit + ls
	        + "maxWithdraw: " + maxWithdraw + ls
	        + "maxDepositPerDay: " + maxDepositPerDay + ls
	        + "maxDepositPerMonth: " + maxDepositPerMonth + ls
	        + "timeLastUpdate: " + timeLastUpdate + ls
	        + "timeCreated: " + timeCreated + ls
	        + "writerId: " + writerId + ls
	        + "comments: " + comments + ls
	        + "default: " + dflt + ls
	        + "writerName: " + writerName + ls
	        + "freeInvestMin: " + freeInvestMin + ls
	        + "freeInvestMax: " + freeInvestMax + ls
	    	+ "amountForLowWithdraw: " + amountForLowWithdraw + ls;
	}
}