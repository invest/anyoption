package com.anyoption.beans;

import java.util.Locale;

import com.anyoption.util.CommonUtil;


 /* Currency bean wrapper
 *
 * @author KobiM
 *
 */
public class Currency extends com.anyoption.common.beans.base.Currency {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;


	public Currency(){
		super();

	}

	public Currency(com.anyoption.common.beans.base.Currency currency){
		super(currency);
		this.id = super.id;
		this.symbol=super.symbol;
		this.defaultSymbol = super.defaultSymbol;
		this.isLeftSymbol=super.isLeftSymbol;
		this.code=super.code;
		this.nameKey=super.nameKey;
		this.displayName=super.displayName;
		this.decimalPointDigits = super.decimalPointDigits;

	}

	public void initFormattedValues(Locale locale) {
//		setSymbol(locale);
		setDisplayName(locale);

	}



	public void setSymbol(Locale locale) {
		this.symbol = CommonUtil.getMessage(symbol, null, locale);
	}


	public void setDisplayName(Locale locale) {
		this.displayName = CommonUtil.getMessage(displayName, null, locale);
	}


	@Override
	public String toString() {
        String ls = System.getProperty("line.separator");
	    return ls + "Currency" + ls
	        + super.toString() + ls
	        + "id: " + id + ls
	        + "symbol: " + symbol + ls
	        + "defaultSymbol: " + defaultSymbol + ls
	        + "isLeftSymbol: " + isLeftSymbol + ls
	        + "code: " + code + ls
	        + "nameKey: " + nameKey + ls
            + "displayName: " + displayName + ls
            + "decimalPointDigits: " + decimalPointDigits + ls;
	}
}
