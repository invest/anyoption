//package com.anyoption.beans;
//
//import java.io.Serializable;
//
//public class Template implements Serializable {
//	private long id;
//	private String name;
//	private String subject;
//	private String fileName;
//	private boolean isDisplayed;
//	private long mailBoxEmailType;
//
//	public String getFileName() {
//		return fileName;
//	}
//
//	public void setFileName(String fileName) {
//		this.fileName = fileName;
//	}
//
//	public long getId() {
//		return id;
//	}
//
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public String getSubject() {
//		return subject;
//	}
//
//	public void setSubject(String subject) {
//		this.subject = subject;
//	}
//
//	/**
//	 * @return the isDisplayed
//	 */
//	public boolean isDisplayed() {
//		return isDisplayed;
//	}
//
//	/**
//	 * @param isDisplayed the isDisplayed to set
//	 */
//	public void setDisplayed(boolean isDisplayed) {
//		this.isDisplayed = isDisplayed;
//	}
//
//	public long getMailBoxEmailType() {
//		return mailBoxEmailType;
//	}
//
//	public void setMailBoxEmailType(long mailBoxEmailType) {
//		this.mailBoxEmailType = mailBoxEmailType;
//	}
//
//    public String toString() {
//        String ls = System.getProperty("line.separator");
//        return ls + "Template" + ls
//            + super.toString() + ls
//            + "id: " + id + ls
//            + "name: " + name + ls
//            + "subject: " + subject + ls
//            + "fileName: " + fileName + ls;
//    }
//}