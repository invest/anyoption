package com.anyoption.beans;

import java.sql.SQLException;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.managers.MobileVersionsManagerBase;

public class MobileApplication {
	public static final Logger log = Logger.getLogger(MobileApplication.class);

    private static HashMap<String, MobileVersion> apks = new HashMap<String, MobileVersion>();

    public static Integer getApkVersion(String apkName) throws SQLException {
    	synchronized (apks) {
	    	MobileVersion mv = apks.get(apkName);
	    	if (null == mv) {
	    		mv = MobileVersionsManagerBase.getAPK(apkName);
	    		apks.put(apkName, mv);
	        }
	        return mv.getApkVersion();
    	}
    }

    public static Boolean getForceUpdate(String apkName) throws SQLException {
    	synchronized (apks) {
	    	MobileVersion mv = apks.get(apkName);
	    	if (null == mv) {
	    		mv = MobileVersionsManagerBase.getAPK(apkName);
	    		apks.put(apkName, mv);
	        }
	        return mv.getForceUpdate();
    	}
    }

    public static boolean isNeedUpdate(String apkName, int versionCode) throws SQLException {
        return versionCode < getApkVersion(apkName);
    }

	/**
	 * @return the downloadLink
	 * @throws SQLException
	 */
	public static String getDownloadLink(String apkName) throws SQLException {
		synchronized (apks) {
			MobileVersion mv = apks.get(apkName);
	    	if (null == mv) {
	    		mv = MobileVersionsManagerBase.getAPK(apkName);
	    		apks.put(apkName, mv);
	        }
			return mv.getDownloadLink();
		}
	}

	/**
	 * this method clear the apk info so next time user ask we will load it from db
	 * @param apkName - the apk name we want to clear from cache
	 *
	 */
	public static void clearApkInfo(String apkName) {
		synchronized (apks) {
			apks.remove(apkName);
		}
	}

	public static String getApkInfo(String apkName) {
		synchronized (apks) {
			MobileVersion mv = apks.get(apkName);
	    	if (null == mv) {
	    		return apkName + " no info";
	    	}
	    	return apkName + "\n" + mv.toString();
		}
	}
}