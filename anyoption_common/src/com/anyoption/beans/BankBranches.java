//package com.anyoption.beans;
//
//import java.sql.SQLException;
//import java.util.ArrayList;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.managers.BanksManagerBase;
//
///**
// * Bank bean wrapper
// *
// * @author KobiM
// *
// */
//public class BankBranches extends com.anyoption.beans.base.BankBranches {
//	private static final Logger log = Logger.getLogger(BankBranches.class);
//
//	private static ArrayList<BankBranches> banksBranches;
//
//    public static ArrayList<BankBranches> getBanksBranches(long id) {
//        if (null == banksBranches) {
//            try {
//            	banksBranches = BanksManagerBase.getBanksBranches();
//            } catch (SQLException sqle) {
//                log.error("Can't load banks branches.", sqle);
//            }
//        }
//        return banksBranches;
//    }
//}
