//package com.anyoption.beans;
//
//import java.util.Date;
//
//public class InvestmentRejects {
//    private long userId;
//    private long opportunityId;
//    private int rejectTypeId;
//    private String sessionId;
//    private Double realLevel;
//    private Double pageLevel;
//    private Double wwwLevel;
//    private Long amount;
//    private Float returnInv;
//    private String rejectAdditionalInfo;
//    private Date timeCreated;
//    private long writerId;
//    private int secBetweenInvestments;
//    private Double rate;
//    private int typeId;
//    private int opportunityType;
//    private Float refundInv;
//
//    public InvestmentRejects() {
//    }
//
//    public InvestmentRejects(long oppId, double pageLevel, String sessionId, long invAmount, long userId, double userRate, float pageOddsWin, Float pageOddsLose, int choice, int oppType, long writerId) {
//        this.opportunityId = oppId;
//        this.pageLevel = pageLevel;
//        this.sessionId = sessionId;
//        this.amount = invAmount;
//        this.userId = userId;
//        this.returnInv = pageOddsWin;
//        this.refundInv = pageOddsLose;
//        this.rate = userRate;
//        this.typeId = choice;
//        this.opportunityType = oppType;
//        this.writerId = writerId;
//    }
//
//    /**
//     * @return the amount
//     */
//    public Long getAmount() {
//        return amount;
//    }
//    /**
//     * @param amount the amount to set
//     */
//    public void setAmount(Long amount) {
//        this.amount = amount;
//    }
//    /**
//     * @return the opportunityId
//     */
//    public long getOpportunityId() {
//        return opportunityId;
//    }
//    /**
//     * @param opportunityId the opportunityId to set
//     */
//    public void setOpportunityId(long opportunityId) {
//        this.opportunityId = opportunityId;
//    }
//    /**
//     * @return the pageLevel
//     */
//    public Double getPageLevel() {
//        return pageLevel;
//    }
//    /**
//     * @param pageLevel the pageLevel to set
//     */
//    public void setPageLevel(Double pageLevel) {
//        this.pageLevel = pageLevel;
//    }
//    /**
//     * @return the realLevel
//     */
//    public Double getRealLevel() {
//        return realLevel;
//    }
//    /**
//     * @param realLevel the realLevel to set
//     */
//    public void setRealLevel(Double realLevel) {
//        this.realLevel = realLevel;
//    }
//    /**
//     * @return the rejectAdditionalInfo
//     */
//    public String getRejectAdditionalInfo() {
//        return rejectAdditionalInfo;
//    }
//    /**
//     * @param rejectAdditionalInfo the rejectAdditionalInfo to set
//     */
//    public void setRejectAdditionalInfo(String rejectAdditionalInfo) {
//        this.rejectAdditionalInfo = rejectAdditionalInfo;
//    }
//    /**
//     * @return the rejectTypeId
//     */
//    public int getRejectTypeId() {
//        return rejectTypeId;
//    }
//    /**
//     * @param rejectTypeId the rejectTypeId to set
//     */
//    public void setRejectTypeId(int rejectTypeId) {
//        this.rejectTypeId = rejectTypeId;
//    }
//    /**
//     * @return the returnInv
//     */
//    public Float getReturnInv() {
//        return returnInv;
//    }
//    /**
//     * @param returnInv the returnInv to set
//     */
//    public void setReturnInv(Float returnInv) {
//        this.returnInv = returnInv;
//    }
//    /**
//     * @return the secBetweenInvestments
//     */
//    public int getSecBetweenInvestments() {
//        return secBetweenInvestments;
//    }
//    /**
//     * @param secBetweenInvestments the secBetweenInvestments to set
//     */
//    public void setSecBetweenInvestments(int secBetweenInvestments) {
//        this.secBetweenInvestments = secBetweenInvestments;
//    }
//    /**
//     * @return the sessionId
//     */
//    public String getSessionId() {
//        return sessionId;
//    }
//    /**
//     * @param sessionId the sessionId to set
//     */
//    public void setSessionId(String sessionId) {
//        this.sessionId = sessionId;
//    }
//    /**
//     * @return the timeCreated
//     */
//    public Date getTimeCreated() {
//        return timeCreated;
//    }
//    /**
//     * @param timeCreated the timeCreated to set
//     */
//    public void setTimeCreated(Date timeCreated) {
//        this.timeCreated = timeCreated;
//    }
//    /**
//     * @return the userId
//     */
//    public long getUserId() {
//        return userId;
//    }
//    /**
//     * @param userId the userId to set
//     */
//    public void setUserId(long userId) {
//        this.userId = userId;
//    }
//    /**
//     * @return the writerId
//     */
//    public long getWriterId() {
//        return writerId;
//    }
//    /**
//     * @param writerId the writerId to set
//     */
//    public void setWriterId(long writerId) {
//        this.writerId = writerId;
//    }
//    /**
//     * @return the wwwLevel
//     */
//    public Double getWwwLevel() {
//        return wwwLevel;
//    }
//    /**
//     * @param wwwLevel the wwwLevel to set
//     */
//    public void setWwwLevel(Double wwwLevel) {
//        this.wwwLevel = wwwLevel;
//    }
//
//    /**
//     * @return the typeId
//     */
//    public int getTypeId() {
//        return typeId;
//    }
//
//    /**
//     * @param typeId the typeId to set
//     */
//    public void setTypeId(int typeId) {
//        this.typeId = typeId;
//    }
//
//    public int getOpportunityType() {
//        return opportunityType;
//    }
//
//    public void setOpportunityType(int opportunityType) {
//        this.opportunityType = opportunityType;
//    }
//
//    /**
//     * @return the rate
//     */
//    public Double getRate() {
//        return rate;
//    }
//
//    /**
//     * @param rate the rate to set
//     */
//    public void setRate(Double rate) {
//        this.rate = rate;
//    }
//
//    /**
//     * @return the refundInv
//     */
//    public Float getRefundInv() {
//        return refundInv;
//    }
//
//    /**
//     * @param refundInv the refundInv to set
//     */
//    public void setRefundInv(Float refundInv) {
//        this.refundInv = refundInv;
//    }
//}