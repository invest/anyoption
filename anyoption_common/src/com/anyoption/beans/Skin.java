//package com.anyoption.beans;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Hashtable;
//import java.util.Iterator;
//
//import com.anyoption.common.annotations.AnyoptionNoJSON;
//import com.anyoption.common.beans.Market;
//import com.anyoption.common.beans.SkinCurrency;
//import com.anyoption.common.beans.SkinLanguage;
//import com.anyoption.common.beans.Tier;
//import com.anyoption.common.beans.TreeItem;
//import com.anyoption.common.beans.base.MarketGroup;
//import com.anyoption.common.bl_vos.BankBase;
//
//public class Skin extends com.anyoption.common.beans.base.Skin implements Cloneable {
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 260101303109129572L;
//	
//	@AnyoptionNoJSON
//	private ArrayList<SkinCurrency> skinCurrenciesList;
//	@AnyoptionNoJSON
//    private ArrayList<SkinLanguage> skinLanguagesList;
//	@AnyoptionNoJSON
//    private HashMap<Long, String> skinMarketsList;       // all markets(id,display_name) of the skin
//	@AnyoptionNoJSON
//    private ArrayList<TreeItem> treeItems;
//	@AnyoptionNoJSON
//    private ArrayList<Market> assetIndex;
//	@AnyoptionNoJSON
//    private ArrayList<com.anyoption.common.beans.base.Market> skinMarketsListSorted;
//	@AnyoptionNoJSON
//    private HashMap<Long, Long> tiersLevels;     // save tiersLevel for the skin (for knowing the next tier level)
//	@AnyoptionNoJSON
//    private Hashtable<Long, MarketGroup> skinGroupsMarkets;
//	@AnyoptionNoJSON
//	private HashMap<Long, SkinCurrency> skinCurrenciesHM;
//	@AnyoptionNoJSON
//	private ArrayList<Long> netreferCombinationId;
//	@AnyoptionNoJSON
//	private ArrayList<Long> referpartnerCombinationId;
//	@AnyoptionNoJSON
//    private Hashtable<Long, Market> hourlyBinaryMarketsHM; // Binary market include group_id and group_name
//	@AnyoptionNoJSON
//    private Hashtable<Long, Market> allBinaryMarketsHM; // Binary market include group_id and group_name
//
//	@AnyoptionNoJSON
//	private ArrayList<BankBase> bankWireWithdrawList;
//
//   /**
//     * @return the skinCurrenciesList
//     */
//    public ArrayList<SkinCurrency> getSkinCurrenciesList() {
//        return skinCurrenciesList;
//    }
//
//    /**
//     * @param skinCurrenciesList the skinCurrenciesList to set
//     */
//    public void setSkinCurrenciesList(ArrayList<SkinCurrency> skinCurrenciesList) {
//        this.skinCurrenciesList = skinCurrenciesList;
//    }
//
//    /**
//     * @return the skinLanguagesList
//     */
//    public ArrayList<SkinLanguage> getSkinLanguagesList() {
//        return skinLanguagesList;
//    }
//
//    /**
//     * @param skinLanguagesList the skinLanguagesList to set
//     */
//    public void setSkinLanguagesList(ArrayList<SkinLanguage> skinLanguagesList) {
//        this.skinLanguagesList = skinLanguagesList;
//    }
//
//    /**
//     * @return the skinMarketsList
//     */
//    public HashMap<Long, String> getSkinMarketsList() {
//        return skinMarketsList;
//    }
//
//    /**
//     * @param skinMarketsList the skinMarketsList to set
//     */
//    public void setSkinMarketsList(HashMap<Long, String> skinMarketsList) {
//        this.skinMarketsList = skinMarketsList;
//    }
//
//    public ArrayList<TreeItem> getTreeItems() {
//        return treeItems;
//    }
//
//    public void setTreeItems(ArrayList<TreeItem> treeItems) {
//        this.treeItems = treeItems;
//    }
//
//    /**
//     * This method returns a flag to indicate if this skin is allowed to use reverse withdraw (allways true)
//     * @return whether this skin is allowed to use reverse withdraw
//     */
//    public boolean isReverseWithdrawAllowed() {
//        return true;
//    }
//
//    /**
//     * This method returns a flag to indicate if this skin is allowed to use chargeback withdraw (allways true)
//     * @return whether this skin is allowed to use chargeback withdraw
//     */
//    public boolean isChargebackWithdrawAllowed() {
//        return true;
//    }
//
//    public ArrayList<Market> getAssetIndex() {
//        return assetIndex;
//    }
//
//    public void setAssetIndex(ArrayList<Market> assetIndex) {
//        this.assetIndex = assetIndex;
//    }
//
//    /**
//     * @return the tiersLevels
//     */
//    public HashMap<Long, Long> getTiersLevels() {
//        return tiersLevels;
//    }
//
//    /**
//     * @param tiersLevels the tiersLevels to set
//     */
//    public void setTiersLevels(HashMap<Long, Long> tiersLevels) {
//        this.tiersLevels = tiersLevels;
//    }
//
//    /**
//     * Get blue level tier
//     * @param skinId
//     * @return
//     */
//    public long getBlueTier() {
//        return getTierIdByLevel(Tier.TIER_LEVEL_BLUE);
//    }
//
//    /**
//     * Get next level tier by tierId
//     * @param tierId
//     * @return
//     */
//    public long getNextLvlTierId(long tierId) {
//        long nextLevl = tiersLevels.get(new Long(tierId)) + 1;
//        return getTierIdByLevel(nextLevl);
//    }
//
//    /**
//     * Get tierId by level
//     * @param level
//     * @return
//     */
//    public long getTierIdByLevel(long level) {
//        long tierId = 0;
//        for (Iterator<Long> iter = tiersLevels.keySet().iterator() ; iter.hasNext(); ) {
//            long id = iter.next();
//            if (tiersLevels.get(new Long(id)) == level ) {
//                tierId = id;
//                break;
//            }
//        }
//        return tierId;
//    }
//
//    /**
//     * Is tierLevel is the maxLevel in this skin
//     * @param tierId
//     * @return
//     */
//    public boolean isHigherLvlTier(long tierId) {
//        long level = tiersLevels.get(new Long(tierId));
//        if (level == tiersLevels.size()) {
//            return true;
//        }
//        return false;
//    }
//
//    /**
//	 * @return the skinGroupsMarkets
//	 */
//	public Hashtable<Long, MarketGroup> getSkinGroupsMarkets() {
//		return skinGroupsMarkets;
//	}
//
//	/**
//	 * @param skinGroupsMarkets the skinGroupsMarkets to set
//	 */
//	public void setSkinGroupsMarkets(Hashtable<Long, MarketGroup> skinGroupsMarkets) {
//		this.skinGroupsMarkets = skinGroupsMarkets;
//	}
//
//	/**
//	 * @return the skinMarketsListSorted
//	 */
//	public ArrayList<com.anyoption.common.beans.base.Market> getSkinMarketsListSorted() {
//		return skinMarketsListSorted;
//	}
//
//	/**
//	 * @param skinMarketsListSorted the skinMarketsListSorted to set
//	 */
//	public void setSkinMarketsListSorted(
//			ArrayList<com.anyoption.common.beans.base.Market> skinMarketsListSorted) {
//		this.skinMarketsListSorted = skinMarketsListSorted;
//	}
//
//	/**
//	 * @return the skinCurrenciesHM
//	 */
//	public HashMap<Long, SkinCurrency> getSkinCurrenciesHM() {
//		return skinCurrenciesHM;
//	}
//
//	/**
//	 * @param skinCurrenciesHM the skinCurrenciesHM to set
//	 */
//	public void setSkinCurrenciesHM(HashMap<Long, SkinCurrency> skinCurrenciesHM) {
//		this.skinCurrenciesHM = skinCurrenciesHM;
//	}
//
//	public ArrayList<Long> getNetreferCombinationId() {
//		return netreferCombinationId;
//	}
//
//	public void setNetreferCombinationId(ArrayList<Long> netreferCombinationId) {
//		this.netreferCombinationId = netreferCombinationId;
//	}
//	
//	public ArrayList<Long> getReferpartnerCombinationId() {
//		return referpartnerCombinationId;
//	}
//
//	public void setReferpartnerCombinationId(ArrayList<Long> netreferCombinationId) {
//		this.referpartnerCombinationId = netreferCombinationId;
//	}
//
//	/**
//	 * @return the hourlyBinaryMarketsHM
//	 */
//	public Hashtable<Long, Market> getHourlyBinaryMarketsHM() {
//		return hourlyBinaryMarketsHM;
//	}
//
//	/**
//	 * @param hourlyBinaryMarketsHM the hourlyBinaryMarketsHM to set
//	 */
//	public void setHourlyBinaryMarketsHM(
//			Hashtable<Long, Market> hourlyBinaryMarketsHM) {
//		this.hourlyBinaryMarketsHM = hourlyBinaryMarketsHM;
//	}
//
//	/**
//	 * @return the allBinaryMarketsHM
//	 */
//	public Hashtable<Long, Market> getAllBinaryMarketsHM() {
//		return allBinaryMarketsHM;
//	}
//
//	/**
//	 * @param allBinaryMarketsHM the allBinaryMarketsHM to set
//	 */
//	public void setAllBinaryMarketsHM(Hashtable<Long, Market> allBinaryMarketsHM) {
//		this.allBinaryMarketsHM = allBinaryMarketsHM;
//	}
//
//	/**
//	 * @return the bankWireWithdrawList
//	 */
//	public ArrayList<BankBase> getBankWireWithdrawList() {
//		return bankWireWithdrawList;
//	}
//
//	/**
//	 * @param bankWireWithdrawList the bankWireWithdrawList to set
//	 */
//	public void setBankWireWithdrawList(ArrayList<BankBase> bankWireWithdrawList) {
//		this.bankWireWithdrawList = bankWireWithdrawList;
//	}
//
//}