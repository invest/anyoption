package com.anyoption.beans;

import java.util.Date;

public class PopulationType {
    protected long id;
    protected String name;
    protected long refreshPeriod;
    protected long refreshDays;
    protected Date lastRefreshTime;
    protected int bonusLimitTypeId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getLastRefreshTime() {
        return lastRefreshTime;
    }

    public void setLastRefreshTime(Date lastRefreshTime) {
        this.lastRefreshTime = lastRefreshTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getRefreshDays() {
        return refreshDays;
    }

    public void setRefreshDays(long refreshDays) {
        this.refreshDays = refreshDays;
    }

    public long getRefreshPeriod() {
        return refreshPeriod;
    }

    public void setRefreshPeriod(long refreshPeriod) {
        this.refreshPeriod = refreshPeriod;
    }

	public int getBonusLimitTypeId() {
		return bonusLimitTypeId;
	}

	public void setBonusLimitTypeId(int bonusLimitTypeId) {
		this.bonusLimitTypeId = bonusLimitTypeId;
	}

}