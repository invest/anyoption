package com.anyoption.beans;


import java.io.Serializable;
import java.util.Date;

import org.apache.log4j.Logger;

/**
 * Population Entry History VO class
 * @author Kobi
 *
 */
public class PopulationEntryHis implements Serializable {

	private static final long serialVersionUID = 1063897471618041902L;

	private static final Logger logger = Logger.getLogger(PopulationEntryHis.class);

	private long id;
	private long populationEntryId;
	private long statusId;
	private long writerId;
	private Date timeCreated;
	private long assignWriterId;
	private long issueActionId;

	/**
	 * @return the issueActionId
	 */
	public long getIssueActionId() {
		return issueActionId;
	}

	/**
	 * @param issueActionId the issueActionId to set
	 */
	public void setIssueActionId(long issueActionId) {
		this.issueActionId = issueActionId;
	}

	public PopulationEntryHis() {
    }

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the populationEntryId
	 */
	public long getPopulationEntryId() {
		return populationEntryId;
	}

	/**
	 * @param populationEntryId the populationEntryId to set
	 */
	public void setPopulationEntryId(long populationEntryId) {
		this.populationEntryId = populationEntryId;
	}

	/**
	 * @return the statusId
	 */
	public long getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the assignWriterId
	 */
	public long getAssignWriterId() {
		return assignWriterId;
	}

	/**
	 * @param assignWriterId the assignWriterId to set
	 */
	public void setAssignWriterId(long assignWriterId) {
		this.assignWriterId = assignWriterId;
	}

	/**
	 * toString implementation
	 */
	public String toString() {
	   String ls = System.getProperty("line.separator");
	   return "population Entry History (" + ls +
		  "id: " + id + ls +
		  "population Entry Id: " + this.populationEntryId + ls +
		  "issue Action Id: " + this.issueActionId + ls +
	      "writerId: " + writerId + ls +
	      "time Created: " + this.timeCreated + ls +
	      "assign Writer Id: " + this.assignWriterId + ls +
	   	  "status Id: " + this.statusId + ls;
	}
}
