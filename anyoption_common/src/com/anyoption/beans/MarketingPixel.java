package com.anyoption.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * Marketing pixel class
 *
 * @author Kobi.
 */
public class MarketingPixel implements Serializable {
    protected long id;
    protected String name;
    protected String httpCode;
    protected String httpsCode;
    protected long writerId;
    protected Date timeCreated;
    protected long typeId;

    private String typeName;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the httpCode
	 */
	public String getHttpCode() {
		return httpCode;
	}

	/**
	 * @param httpCode the httpCode to set
	 */
	public void setHttpCode(String httpCode) {
		this.httpCode = httpCode;
	}

	/**
	 * @return the httpsCode
	 */
	public String getHttpsCode() {
		return httpsCode;
	}

	/**
	 * @param httpsCode the httpsCode to set
	 */
	public void setHttpsCode(String httpsCode) {
		this.httpsCode = httpsCode;
	}

	/**
	 * @return the typeId
	 */
	public long getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return the typeName
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * @param typeName the typeName to set
	 */
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	/**
	 * toString implementation.
	 */
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "MarketingContent:" + ls +
            "id: " + id + ls +
            "name: " + name + ls +
	        "writerId: " + writerId + ls +
	        "typeId: " + typeId + ls +
	        "timeCreated: " + timeCreated + ls;
    }
}