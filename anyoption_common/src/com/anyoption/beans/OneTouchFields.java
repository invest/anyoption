//package com.anyoption.beans;
//
///**
// * class used to contain all the fields special for One Touch
// */
//public class OneTouchFields implements java.io.Serializable {
//	private long id;
//	private int upDown;
//	private int decimalPoint;
//
//	public long getId() {
//		return id;
//	}
//    
//    public void setId(long id) {
//        this.id = id;
//    }
//    
//	public int getUpDown() {
//        return upDown;
//    }
//
//    public void setUpDown(int upDown) {
//        this.upDown = upDown;
//    }
//
//    public int getDecimalPoint() {
//		return decimalPoint;
//	}
//    
//	public void setDecimalPoint(int decimalPoint) {
//		this.decimalPoint = decimalPoint;
//	}
//    
//	public String toString() {
//        String ls = System.getProperty("line.separator");
//	    return ls + "OneTouchFields" + ls
//	        + super.toString() + ls
//	        + "id: " + id + ls
//	        + "upDown: " + upDown + ls
//	        + "decimalPoint: " + decimalPoint + ls;
//	}
//}