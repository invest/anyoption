//package com.anyoption.beans;
//
//import java.io.Serializable;
//import java.util.ArrayList;
//import java.util.Date;
//
//public class Writer implements Serializable {
//	//Writers
//    public static final int WRITER_ID_AUTO = 0;
//    public static final int WRITER_ID_WEB = 1;
//    public static final int WRITER_ID_MOBILE = 200;
//	
//    private long id;
//    private String userName;
//    private String password;
//    private String firstName;
//    private String lastName;
//    private String street;
//    private String streetNo;
//    private String cityId;
//    private String zipCode;
//    private String email;
//    private String comments;
//    private Date timeBirthDate;
//    private String mobilePhone;
//    private String landLinePhone;
//    private ArrayList<Integer> skins;
//    private int isActive;
//    private long groupId;
//    private boolean isSupportEnable;
//
//    // rep data
//    private long generalAssignedRecords;
//    private long trackingAssignedRecords;
//    private long callBacksAssignedRecords;
//    private boolean toCollect;
//    private long allAssignedRecords;
//    private long calledReachedRecords;
//    private long calledNotReachedRecords;
//    private long notCalleRecords;
//    private long callBacksRecords;
//
//    private long onlineDepositSkinId;  // recognize deposit user skin for Tv screens
//    private String onlineDepositAmount;
//
//	/**
//	 * @return the isSupportEnable
//	 */
//	public boolean isSupportEnable() {
//		return isSupportEnable;
//	}
//	
//	/**
//	 * @param isSupportEnable the isSupportEnable to set
//	 */
//	public void setSupportEnable(boolean isSupportEnable) {
//		this.isSupportEnable = isSupportEnable;
//	}
//	
//	public String getStreetNo() {
//		return streetNo;
//	}
//	
//	public void setStreetNo(String streetNo) {
//		this.streetNo = streetNo;
//	}
//	
//	public String getCityId() {
//		return cityId;
//	}
//	
//	public void setCityId(String cityId) {
//		this.cityId = cityId;
//	}
//	
//	public String getComments() {
//		return comments;
//	}
//	
//	public void setComments(String comments) {
//		this.comments = comments;
//	}
//	
//	public String getEmail() {
//		return email;
//	}
//	
//	public void setEmail(String email) {
//		this.email = email;
//	}
//	
//	public String getFirstName() {
//		return firstName;
//	}
//	
//	public void setFirstName(String firstName) {
//		this.firstName = firstName;
//	}
//	
//	public long getId() {
//		return id;
//	}
//	
//	public void setId(long id) {
//		this.id = id;
//	}
//	
//	public int getIsActive() {
//		return isActive;
//	}
//	
//	public void setIsActive(int isActive) {
//		this.isActive = isActive;
//	}
//	
//	public String getLandLinePhone() {
//		return landLinePhone;
//	}
//	
//	public void setLandLinePhone(String landLinePhone) {
//		this.landLinePhone = landLinePhone;
//	}
//	
//	public String getLastName() {
//		return lastName;
//	}
//	
//	public void setLastName(String lastName) {
//		this.lastName = lastName;
//	}
//	
//	public String getMobilePhone() {
//		return mobilePhone;
//	}
//	
//	public void setMobilePhone(String mobilePhone) {
//		this.mobilePhone = mobilePhone;
//	}
//	
//	public String getPassword() {
//		return password;
//	}
//	
//	public void setPassword(String password) {
//		this.password = password;
//	}
//	
//	public String getStreet() {
//		return street;
//	}
//	
//	public void setStreet(String street) {
//		this.street = street;
//	}
//	
//	public Date getTimeBirthDate() {
//		return timeBirthDate;
//	}
//	
//	public void setTimeBirthDate(Date timeBirthDate) {
//		this.timeBirthDate = timeBirthDate;
//	}
//	
//	public String getUserName() {
//		return userName;
//	}
//	
//	public void setUserName(String userName) {
//		this.userName = userName;
//	}
//	
//	public String getZipCode() {
//		return zipCode;
//	}
//	
//	public void setZipCode(String zipCode) {
//		this.zipCode = zipCode;
//	}
//
//    public String getLandLinePhonePrefix() {
//    	if (landLinePhone == null || landLinePhone.equals("")) {
//    		return "";
//    	}
//    	return landLinePhone.substring(0,3);
//    }
//    
//    public String getLandLinePhoneSuffix() {
//    	if (landLinePhone == null || landLinePhone.equals("")) {
//    		return "";
//    	}
//    	if (landLinePhone.length() > 7) {
//    		return landLinePhone.substring(3);
//    	}
//    	return landLinePhone;
//    }
//    
//    public String getMobilePhonePrefix() {
//    	if (mobilePhone == null || mobilePhone.equals("")) {
//    		return "";
//    	}
//    	return mobilePhone.substring(0,3);
//    }
//
//    public String getMobilePhoneSuffix() {
//    	if (mobilePhone == null || mobilePhone.equals("")) {
//    		return "";
//    	}
//    	if (mobilePhone.length() > 7) {
//    		return mobilePhone.substring(3);
//    	}
//    	return mobilePhone;
//    }
//
//    public void setLandLinePhonePrefix(String s) {
//    	landLinePhone = s + getLandLinePhoneSuffix();
//    }
//    
//    public void setLandLinePhoneSuffix(String s) {
//    	landLinePhone = getLandLinePhonePrefix() + s;
//    }
//    
//    public void setMobilePhonePrefix(String s) {
//    	mobilePhone = s + getMobilePhoneSuffix();
//    }
//    
//    public void setMobilePhoneSuffix(String s) {
//    	mobilePhone = getMobilePhonePrefix() + s;
//    }
//
//    public ArrayList<Integer> getSkins() {
//		return skins;
//	}
//
//	public void setSkins(ArrayList<Integer> skins) {
//		this.skins = skins;
//	}
//
//	/**
//	 * @return the groupId
//	 */
//	public long getGroupId() {
//		return groupId;
//	}
//
//	/**
//	 * @param groupId the groupId to set
//	 */
//	public void setGroupId(long groupId) {
//		this.groupId = groupId;
//	}
//
//	/**
//	 * @return the callBacksAssignedRecords
//	 */
//	public long getCallBacksAssignedRecords() {
//		return callBacksAssignedRecords;
//	}
//
//	/**
//	 * @param callBacksAssignedRecords the callBacksAssignedRecords to set
//	 */
//	public void setCallBacksAssignedRecords(long callBacksAssignedRecords) {
//		this.callBacksAssignedRecords = callBacksAssignedRecords;
//	}
//
//	/**
//	 * @return the generalAssignedRecords
//	 */
//	public long getGeneralAssignedRecords() {
//		return generalAssignedRecords;
//	}
//
//	/**
//	 * @param generalAssignedRecords the generalAssignedRecords to set
//	 */
//	public void setGeneralAssignedRecords(long generalAssignedRecords) {
//		this.generalAssignedRecords = generalAssignedRecords;
//	}
//
//	/**
//	 * @return the trackingAssignedRecords
//	 */
//	public long getTrackingAssignedRecords() {
//		return trackingAssignedRecords;
//	}
//
//	/**
//	 * @param trackingAssignedRecords the trackingAssignedRecords to set
//	 */
//	public void setTrackingAssignedRecords(long trackingAssignedRecords) {
//		this.trackingAssignedRecords = trackingAssignedRecords;
//	}
//
//	/**
//	 * @return the toCollect
//	 */
//	public boolean isToCollect() {
//		return toCollect;
//	}
//
//	/**
//	 * @param toCollect the toCollect to set
//	 */
//	public void setToCollect(boolean toCollect) {
//		this.toCollect = toCollect;
//	}
//
//	/**
//	 * @return the allAssignedRecords
//	 */
//	public long getAllAssignedRecords() {
//		return allAssignedRecords;
//	}
//
//	/**
//	 * @param allAssignedRecords the allAssignedRecords to set
//	 */
//	public void setAllAssignedRecords(long allAssignedRecords) {
//		this.allAssignedRecords = allAssignedRecords;
//	}
//
//	/**
//	 * @return the callBacksRecords
//	 */
//	public long getCallBacksRecords() {
//		return callBacksRecords;
//	}
//
//	/**
//	 * @param callBacksRecords the callBacksRecords to set
//	 */
//	public void setCallBacksRecords(long callBacksRecords) {
//		this.callBacksRecords = callBacksRecords;
//	}
//
//	/**
//	 * @return the calledNotReachedRecords
//	 */
//	public long getCalledNotReachedRecords() {
//		return calledNotReachedRecords;
//	}
//
//	/**
//	 * @param calledNotReachedRecords the calledNotReachedRecords to set
//	 */
//	public void setCalledNotReachedRecords(long calledNotReachedRecords) {
//		this.calledNotReachedRecords = calledNotReachedRecords;
//	}
//
//	/**
//	 * @return the calledReachedRecords
//	 */
//	public long getCalledReachedRecords() {
//		return calledReachedRecords;
//	}
//
//	/**
//	 * @param calledReachedRecords the calledReachedRecords to set
//	 */
//	public void setCalledReachedRecords(long calledReachedRecords) {
//		this.calledReachedRecords = calledReachedRecords;
//	}
//
//	/**
//	 * @return the notCalleRecords
//	 */
//	public long getNotCalleRecords() {
//		return notCalleRecords;
//	}
//
//	/**
//	 * @param notCalleRecords the notCalleRecords to set
//	 */
//	public void setNotCalleRecords(long notCalleRecords) {
//		this.notCalleRecords = notCalleRecords;
//	}
//
//	/**
//	 * @return the onlineDepositSkinId
//	 */
//	public long getOnlineDepositSkinId() {
//		return onlineDepositSkinId;
//	}
//
//	/**
//	 * @param onlineDepositSkinId the onlineDepositSkinId to set
//	 */
//	public void setOnlineDepositSkinId(long onlineDepositSkinId) {
//		this.onlineDepositSkinId = onlineDepositSkinId;
//	}
//
//	/**
//	 * @return the onlineDepositAmount
//	 */
//	public String getOnlineDepositAmount() {
//		return onlineDepositAmount;
//	}
//
//	/**
//	 * @param onlineDepositAmount the onlineDepositAmount to set
//	 */
//	public void setOnlineDepositAmount(String onlineDepositAmount) {
//		this.onlineDepositAmount = onlineDepositAmount;
//	}
//
//	public String toString() {
//	    String ls = System.getProperty("line.separator");
//        return ls + "Writer" + ls
//            + super.toString() + ls
//            + "id: " + id + ls
//            + "userName: " + userName + ls
//            + "password: " + password + ls
//            + "firstName: " + firstName + ls
//            + "lastName: " + lastName + ls
//            + "street: " + street + ls
//            + "streetNo: " + streetNo + ls
//            + "cityId: " + cityId + ls
//            + "zipCode: " + zipCode + ls
//            + "email: " + email + ls
//            + "comments: " + comments + ls
//            + "timeBirthDate: " + timeBirthDate + ls
//            + "mobilePhone: " + mobilePhone + ls
//            + "landLinePhone: " + landLinePhone + ls
//            + "isActive: " + isActive + ls;
//    }
//}