//package com.anyoption.beans;
//
//import com.anyoption.util.CommonUtil;
//
///**
// * MailBoxUser vo class.
// * @author Kobi
// *
// */
//public class MailBoxUser extends com.anyoption.beans.base.MailBoxUser {
//	
//	private static final long serialVersionUID = 1L;
//	
//	public static final int MAX_SUBJECT = 25; 
//	
//	public void initFormattedValues(String utcOffset) {
//		setTimeCreatedTxt(utcOffset);
//		setSubjectShortTxt();
//	}
//	
//	public void setSubjectShortTxt() {
//		if (subject != null && subject.length() > MAX_SUBJECT) {
//			this.subjectShortTxt = subject.substring(0, MAX_SUBJECT) + "...";
//		} else {
//			this.subjectShortTxt = subject;
//		}
//	}
//	
//	public void setTimeCreatedTxt(String utcOffset) {
//		this.timeCreatedTxt = CommonUtil.getDateAndTimeFormatByDots(timeCreated, utcOffset);
//	}
//}
