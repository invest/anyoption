//package com.anyoption.beans;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Locale;
//
//import javax.faces.model.SelectItem;
//
//import com.anyoption.common.beans.Market;
//import com.anyoption.common.beans.SkinCurrency;
//import com.anyoption.common.beans.SkinLanguage;
//import com.anyoption.common.beans.TreeItem;
//import com.anyoption.util.CommonUtil;
//import com.anyoption.util.ConstantsBase;
//
///**
// * Skin vo class
// * @author Kobi
// *
// */
//public class Skins implements java.io.Serializable, Cloneable, Comparable<Skins> {
//	private static final long serialVersionUID = -1807172844554557361L;
//	private int id;
//	private String name;
//	private String displayName;
//	private int defaultCountryId;
//	private int defaultLanguageId;
//	private int defaultXorId;
//	private int defaultCurrencyId;
//	private long defaultCombinationId;
//	private ArrayList<Long> netreferCombinationId;
//	private long tierQualificationPeriod;
//	private long businessCaseId;
//    private String supportStartTime;
//    private String supportEndTime;
//    private boolean isLoadTickerMarkets;
//    private boolean isCountryInLocaleUse;
//
//    private ArrayList<SkinCurrency> skinCurrenciesList;
//	private ArrayList<SkinLanguage> skinLanguagesList;
//	private HashMap<Long, String> skinMarketsList;       // all markets(id,display_name) of the skin
//	private ArrayList<SelectItem> skinMarketsHomePageList;     // all markets for homePage
//	private ArrayList<TreeItem> treeItems;
//	private ArrayList<SelectItem> optionTypes;
//	private ArrayList<Market> assetIndex;
//	private HashMap<Long, Long> tiersLevels;     // save tiersLevel for the skin (for knowing the next tier level)
//    private ArrayList<SelectItem> marketsListSI; // markets with active templates
//    private ArrayList<SelectItem> optionPlusMarketsListSI; // option plus markets with active templates
//
//	/**
//	 * In the service storage for the current market on home page field that is sent to
//	 * all LS items
//	 */
//	private String currentMarketsOnHomePage;
//
//	public Skins() {
//		name = "";
//		displayName = "";
//	}
//
//	/**
//	 * @return the defaultCountryId
//	 */
//	public long getDefaultCountryId() {
//		return defaultCountryId;
//	}
//
//	/**
//	 * @param defaultCountryId the defaultCountryId to set
//	 */
//	public void setDefaultCountryId(int defaultCountryId) {
//		this.defaultCountryId = defaultCountryId;
//	}
//
//	/**
//	 * @return the defaultLanguageId
//	 */
//	public int getDefaultLanguageId() {
//		return defaultLanguageId;
//	}
//
//	/**
//	 * @param defaultLanguageId the defaultLanguageId to set
//	 */
//	public void setDefaultLanguageId(int defaultLanguageId) {
//		this.defaultLanguageId = defaultLanguageId;
//	}
//
//
//	public int getDefaultXorId() {
//		return defaultXorId;
//	}
//
//	public void setDefaultXorId(int defaultXorId) {
//		this.defaultXorId = defaultXorId;
//	}
//
//	/**
//	 * @return the displayName
//	 */
//	public String getDisplayName() {
//		return displayName;
//	}
//
//	/**
//	 * @param displayName the displayName to set
//	 */
//	public void setDisplayName(String displayName) {
//		this.displayName = displayName;
//	}
//
//	/**
//	 * @return the id
//	 */
//	public int getId() {
//		return id;
//	}
//
//	/**
//	 * @param id the id to set
//	 */
//	public void setId(int id) {
//		this.id = id;
//	}
//
//	/**
//	 * @return the name
//	 */
//	public String getName() {
//		return name;
//	}
//
//	/**
//	 * @param name the name to set
//	 */
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	/**
//	 * @return the defaultCurrencyId
//	 */
//	public int getDefaultCurrencyId() {
//		return defaultCurrencyId;
//	}
//
//	/**
//	 * @param defaultCurrencyId the defaultCurrencyId to set
//	 */
//	public void setDefaultCurrencyId(int defaultCurrencyId) {
//		this.defaultCurrencyId = defaultCurrencyId;
//	}
//
//	/**
//	 * @return the skinCurrenciesList
//	 */
//	public ArrayList<SkinCurrency> getSkinCurrenciesList() {
//		return skinCurrenciesList;
//	}
//
//	/**
//	 * @param skinCurrenciesList the skinCurrenciesList to set
//	 */
//	public void setSkinCurrenciesList(ArrayList<SkinCurrency> skinCurrenciesList) {
//		this.skinCurrenciesList = skinCurrenciesList;
//	}
//
//	/**
//	 * @return the skinLanguagesList
//	 */
//	public ArrayList<SkinLanguage> getSkinLanguagesList() {
//		return skinLanguagesList;
//	}
//
//	/**
//	 * @param skinLanguagesList the skinLanguagesList to set
//	 */
//	public void setSkinLanguagesList(ArrayList<SkinLanguage> skinLanguagesList) {
//		this.skinLanguagesList = skinLanguagesList;
//	}
//
//	/**
//	 * @return the skinMarketsList
//	 */
//	public HashMap<Long, String> getSkinMarketsList() {
//		return skinMarketsList;
//	}
//
//	/**
//	 * @param skinMarketsList the skinMarketsList to set
//	 */
//	public void setSkinMarketsList(HashMap<Long, String> skinMarketsList) {
//		this.skinMarketsList = skinMarketsList;
//	}
//
//	/**
//	 * @return the skinMarketsHomePageList
//	 */
//	public ArrayList<SelectItem> getSkinMarketsHomePageList() {
//		return skinMarketsHomePageList;
//	}
//
//	/**
//	 * @param skinMarketsHomePageList the skinMarketsHomePageList to set
//	 */
//	public void setSkinMarketsHomePageList(
//			ArrayList<SelectItem> skinMarketsHomePageList) {
//		this.skinMarketsHomePageList = skinMarketsHomePageList;
//	}
//
//	public ArrayList<SelectItem> getOptionTypes() {
//		return optionTypes;
//	}
//
//	public void setOptionTypes(ArrayList<SelectItem> optionTypes) {
//		this.optionTypes = optionTypes;
//	}
//
//
//	/**
//	 * Constructs a <code>String</code> with all attributes
//	 * in name = value format.
//	 *
//	 * @return a <code>String</code> representation
//	 * of this object.
//	 */
//	public String toString()
//	{
//	    final String TAB = " \n ";
//
//	    String retValue = "";
//
//	    retValue = "skin ( "
//	        + super.toString() + TAB
//	        + "id = " + this.id + TAB
//	        + "name = " + this.name + TAB
//	        + "displayName = " + this.displayName + TAB
//	        + "defaultCountryId = " + this.defaultCountryId + TAB
//	        + "defaultLanguageId = " + this.defaultLanguageId + TAB
//	        + "defaultXorId = " + this.defaultXorId + TAB
//	        + "defaultCurrencyId = " + this.defaultCurrencyId + TAB
//	        + " )";
//
//	    return retValue;
//	}
//
//	public ArrayList<TreeItem> getTreeItems() {
//		return treeItems;
//	}
//
//		public void setTreeItems(ArrayList<TreeItem> treeItems) {
//		this.treeItems = treeItems;
//	}
//
//
//	/**
//	 * This method returns a flag to indicate if this skin is allowed to use reverse withdraw (allways true)
//	 * @return whether this skin is allowed to use reverse withdraw
//	 */
//	public boolean isReverseWithdrawAllowed() {
//		return true;
//	}
//
//	/**
//	 * This method returns a flag to indicate if this skin is allowed to use chargeback withdraw (allways true)
//	 * @return whether this skin is allowed to use chargeback withdraw
//	 */
//	public boolean isChargebackWithdrawAllowed() {
//		return true;
//	}
//
//
//	public ArrayList<Market> getAssetIndex() {
//		return assetIndex;
//	}
//
//	public void setAssetIndex(ArrayList<Market> assetIndex) {
//		this.assetIndex = assetIndex;
//	}
//
//	/**
//	 * @return the defaultCombinationId
//	 */
//	public long getDefaultCombinationId() {
//		return defaultCombinationId;
//	}
//
//	/**
//	 * @param defaultCombinationId the defaultCombinationId to set
//	 */
//	public void setDefaultCombinationId(long defaultCombinationId) {
//		this.defaultCombinationId = defaultCombinationId;
//	}
//
//	/**
//	 * @return the defaultCombinationId
//	 */
//	public ArrayList<Long> getNetreferCombinationId() {
//		return netreferCombinationId;
//	}
//
//	/**
//	 * @param defaultCombinationId the defaultCombinationId to set
//	 */
//	public void setNetreferCombinationId(ArrayList<Long> netreferCombinationId) {
//		this.netreferCombinationId = netreferCombinationId;
//	}
//
//	/**
//	 * @return the tiersLevels
//	 */
//	public HashMap<Long, Long> getTiersLevels() {
//		return tiersLevels;
//	}
//
//	/**
//	 * @param tiersLevels the tiersLevels to set
//	 */
//	public void setTiersLevels(HashMap<Long, Long> tiersLevels) {
//		this.tiersLevels = tiersLevels;
//	}
//
//	/**
//	 * @return the tierQualificationPeriod
//	 */
//	public long getTierQualificationPeriod() {
//		return tierQualificationPeriod;
//	}
//
//	/**
//	 * @param tierQualificationPeriod the tierQualificationPeriod to set
//	 */
//	public void setTierQualificationPeriod(long tierQualificationPeriod) {
//		this.tierQualificationPeriod = tierQualificationPeriod;
//	}
//	
//
//	/**
//	 * @return the businessCaseId
//	 */
//	public long getBusinessCaseId() {
//		return businessCaseId;
//	}
//
//	/**
//	 * @param businessCaseId the businessCaseId to set
//	 */
//	public void setBusinessCaseId(long businessCaseId) {
//		this.businessCaseId = businessCaseId;
//	}
//
//	/**
//	 * Get next level tier by tierId
//	 * @param tierId
//	 * @return
//	 */
//	public long getNextLvlTierId(long tierId) {
//		long nextLevl = tiersLevels.get(new Long(tierId)) + 1;
//		return getTierIdByLevel(nextLevl);
//	}
//
//	/**
//	 * Get tierId by level
//	 * @param level
//	 * @return
//	 */
//	public long getTierIdByLevel(long level) {
//		long tierId = 0;
//		for (Iterator<Long> iter = tiersLevels.keySet().iterator() ; iter.hasNext(); ) {
//			long id = iter.next();
//			if (tiersLevels.get(new Long(id)) == level ) {
//				tierId = id;
//				break;
//			}
//		}
//		return tierId;
//	}
//
//	/**
//	 * Is tierLevel is the maxLevel in this skin
//	 * @param tierId
//	 * @return
//	 */
//	public boolean isHigherLvlTier(long tierId) {
//		long level = tiersLevels.get(new Long(tierId));
//		if (level == tiersLevels.size()) {
//			return true;
//		}
//		return false;
//	}
//
//	/**
//	 * Override Clone function for Skins Object
//	 * Doing deep clone just to skinPaymentMethodsList.
//	 * All other list are null.
//	 * @return cloned Skins instance
//	 */
//	public Object clone() throws CloneNotSupportedException {
//		Skins clonedSkin = (Skins)super.clone();
//		
//		// set other attributes to null ( we use this just in userBase and we dont want to save all lists! )
//		clonedSkin.setSkinCurrenciesList(null);
//		clonedSkin.setSkinLanguagesList(null);
//		clonedSkin.setSkinMarketsList(null);
//		clonedSkin.setSkinMarketsHomePageList(null);
//		clonedSkin.setTreeItems(null);
//		clonedSkin.setOptionTypes(null);
//		clonedSkin.setAssetIndex(null);
//		clonedSkin.setTiersLevels(null);
//
//		return clonedSkin;
//	}
//
//	/**
//	 * CompareTo implementation by skin name
//	 */
//	public int compareTo(Skins o) {
//		return CommonUtil.getMessage(this.displayName, null, new Locale(ConstantsBase.LOCALE_DEFAULT)).compareTo(CommonUtil.getMessage(o.displayName, null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
//	}
//
//    public String getCurrentMarketsOnHomePage() {
//        return currentMarketsOnHomePage;
//    }
//
//    public void setCurrentMarketsOnHomePage(String currentMarketsOnHomePage) {
//        this.currentMarketsOnHomePage = currentMarketsOnHomePage;
//    }
//
//    /**
//     * @return the marketsListSi
//     */
//    public ArrayList<SelectItem> getMarketsListSI() {
//        return marketsListSI;
//    }
//
//    /**
//     * @param marketsListSi the marketsListSi to set
//     */
//    public void setMarketsListSI(ArrayList<SelectItem> marketsListSI) {
//        this.marketsListSI = marketsListSI;
//    }
//
//    /**
//     * @return the optionPlusMarketsListSI
//     */
//    public ArrayList<SelectItem> getOptionPlusMarketsListSI() {
//        return optionPlusMarketsListSI;
//    }
//
//    /**
//     * @param optionPlusMarketsListSI the optionPlusMarketsListSI to set
//     */
//    public void setOptionPlusMarketsListSI(
//            ArrayList<SelectItem> optionPlusMarketsListSI) {
//        this.optionPlusMarketsListSI = optionPlusMarketsListSI;
//    }
//
//    /**
//     * @return the supportEndTime
//     */
//    public String getSupportEndTime() {
//        return supportEndTime;
//    }
//
//    /**
//     * @param supportEndTime the supportEndTime to set
//     */
//    public void setSupportEndTime(String supportEndTime) {
//        this.supportEndTime = supportEndTime;
//    }
//
//    /**
//     * @return the supportStartTime
//     */
//    public String getSupportStartTime() {
//        return supportStartTime;
//    }
//
//    /**
//     * @param supportStartTime the supportStartTime to set
//     */
//    public void setSupportStartTime(String supportStartTime) {
//        this.supportStartTime = supportStartTime;
//    }
//
//    /**
//     * @return the isLoadTickerMarkets
//     */
//    public boolean isLoadTickerMarkets() {
//        return isLoadTickerMarkets;
//    }
//
//    /**
//     * @param isLoadTickerMarkets the isLoadTickerMarkets to set
//     */
//    public void setLoadTickerMarkets(boolean isLoadTickerMarkets) {
//        this.isLoadTickerMarkets = isLoadTickerMarkets;
//    }
//
//	public boolean isCountryInLocaleUse() {
//		return isCountryInLocaleUse;
//	}
//
//	public void setCountryInLocaleUse(boolean isCountryInLocaleUse) {
//		this.isCountryInLocaleUse = isCountryInLocaleUse;
//	}
//
//}