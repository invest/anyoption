//package com.anyoption.beans;
//
//import java.sql.SQLException;
//import java.util.Hashtable;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.managers.BanksManagerBase;
//
///**
// * Bank bean wrapper
// *
// * @author KobiM
// *
// */
//public class Bank extends com.anyoption.beans.base.Bank {
//	private static final Logger log = Logger.getLogger(Bank.class);
//
//	private static Hashtable<Long, Bank> banks;
//
//    public static Bank getBank(long id) {
//        if (null == banks) {
//            try {
//                banks = BanksManagerBase.getBanks();
//            } catch (SQLException sqle) {
//                log.error("Can't load banks.", sqle);
//            }
//        }
//        return banks.get(id);
//    }
//}
