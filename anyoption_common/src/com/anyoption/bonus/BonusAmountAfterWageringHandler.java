//package com.anyoption.bonus;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//
//import com.anyoption.common.beans.User;
//
//import com.anyoption.beans.Investment;
//import com.anyoption.common.beans.Bonus;
//import com.anyoption.common.beans.base.BonusUsers;
//import com.anyoption.common.bl_vos.BonusCurrency;
//import com.anyoption.bonus.BonusHandlerBase;
//import com.anyoption.common.bonus.BonusHandlersException;
//import com.anyoption.common.bonus.BonusUtil;
//import com.anyoption.daos.BonusDAOBase;
//import com.anyoption.daos.InvestmentsDAOBase;
//import com.anyoption.managers.BonusManagerBase;
//import com.anyoption.util.CommonUtil;
//
//public class BonusAmountAfterWageringHandler extends BonusHandlerBase {
//
//	/**
//	 *  bonusInsert event handler implementation
//	 */
//	@Override
//	public boolean bonusInsert(Connection conn,BonusUsers bu, Bonus b, BonusCurrency bc, User user, long writerId, long popEntryId, int bonusPopLimitTypeId, String ip) throws BonusHandlersException{
//		boolean res = false;
//		bu.setBonusStateId(Bonus.STATE_PENDING);
//
//		try{
//			BonusManagerBase.getLimitsForBonus(conn, bu, b, bc, popEntryId,bonusPopLimitTypeId,user.getId(), user.getCurrencyId(), false);
//
//			bu.setSumInvWithdrawal(bc.getBonusAmount() * b.getWageringParameter());
//
//			res = BonusDAOBase.insertBonusUser(conn, bu, b, bc, popEntryId, true);
//
//		} catch (SQLException e) {
//			throw new BonusHandlersException("Error in inserting bonus" , e);
//		}
//
//		return res;
//
//	}
//
//	/**
//	 *  getBonusMessage event handler implementation
//	 */
//	@Override
//	public String getBonusMessage(Bonus b, BonusCurrency bc, long popEnteyId, int bonusPopLimitTypeId, long userId) throws BonusHandlersException{
//		String out = "";
//
//		try{
//			long bonusId = b.getId();
//
//			String descMsg = BonusManagerBase.getBonusDescription(bonusId, 1); // state could only be 1 - granted
//
//
//			if (null != bc){
//				long currencyId = bc.getCurrencyId();
//				BonusUsers limitsBu = new BonusUsers();
//
//				limitsBu.setBonusId(bonusId);
//				BonusManagerBase.getLimitsForBonus(limitsBu, b, bc, popEnteyId, bonusPopLimitTypeId,userId, currencyId, false);
//				long minDepositAmount = limitsBu.getSumDeposits();
//
//				out += BonusHandlerBase.getBonusDescription(bc.getBonusAmount()/100,bc.getBonusPercent(),bc.getSumInvestQualify()/100,
//						b.getNumberOfActions(),descMsg,String.valueOf(bc.getMinDepositAmount()/100),String.valueOf(bc.getMaxDepositAmount()/100),
//						String.valueOf(bc.getMinInvestAmount()/100), bc.getMaxInvestAmount(), String.valueOf((int)(b.getOddsWin()*100)),
//						String.valueOf((int)(b.getOddsLose()*100)), currencyId) +  " <br/>";
//
//				out += "Wagering parameter " + b.getWageringParameter() + " on the " + b.getCondition() +  " <br/>";
//
//				if (minDepositAmount >0){
//					out += "<br/>" +
//					CommonUtil.getMessage(null,"bonus.population.grant.msg", new String[] {CommonUtil.formatCurrencyAmount(minDepositAmount, true, currencyId)});
//				}
//
//			}else{
//				throw new BonusHandlersException("No BonusCurrency for bonus id " + bonusId);
//			}
//		} catch (SQLException e) {
//			throw new BonusHandlersException("Error in creating bonus message" , e);
//		}
//		return out;
//	}
//
//	/**
//	 *  isActivateBonus event handler implementation
//	 */
//	@Override
//	public boolean isActivateBonusAfterTransactionSuccess(Connection conn, BonusUsers bu, long amount, long transactionId) throws BonusHandlersException{
//
//    	long depositsSum = bu.getSumDeposits();
//
//    	// Check if requiered deposits sum > 0
//    	if (depositsSum > 0){
//
//    		if (bu.getSumDepositsReached() == 0 && amount >= depositsSum){
//    			// Add the amount of current deposit to sum deposits reached
//        		try{
//        			bu.setSumDepositsReached(amount);
//        			BonusDAOBase.addBonusUsersSumDeposits(conn, bu.getId(), amount);
////        			TransactionsDAOBase.setTransactionBonuUsersId(conn, transactionId, bu.getId());
//        		}catch (SQLException e) {
//    				throw new BonusHandlersException("can't addBonusUsersSumDeposits ", e);
//    			}
//    		}
//
//        	if (bu.getSumDepositsReached() >= depositsSum &&
//        			bu.getSumInvQualifyReached() >= bu.getSumInvQualify()){
//        		return true;
//        	}else{
//        		return false;
//        	}
//    	}
//
//    	return false;
//	}
//
//	/**
//	 *  activateBonusAfterTransactionSuccess event handler implementation
//	 */
//	@Override
//	public void activateBonusAfterTransactionSuccess(Connection conn, BonusUsers bu, long transactionId, long userId, long amount, long writerId) throws BonusHandlersException{
//		activateBonusAndAddToBalance(conn, bu, userId, writerId, transactionId, 0);
//	}
//
//	/**
//	 *  touchBonusesAfterInvestmentSuccess event handler implementation
//	 */
//	@Override
//	public long touchBonusesAfterInvestmentSuccess(Connection conn, BonusUsers bu, long amountLeft) throws BonusHandlersException{
//		try {
//			BonusDAOBase.useBonusUsers(conn, bu.getId());
//		} catch (SQLException e) {
//			throw new BonusHandlersException("Error in touchBonusesAfterInvestmentSuccess ",e);
//		}
//        return (amountLeft - bu.getBonusAmount());
//	}
//
//	/**
//	 *  cancelBonusToUser event handler implementation
//	 */
//	@Override	
//	public void cancelBonusToUser(Connection conn, BonusUsers bonusUser, long stateToUpdate, String utcOffset, long writerId, long skinId) throws BonusHandlersException {
//		BonusUtil.cancelBonusToUserWithdraw(conn, bonusUser, stateToUpdate, utcOffset, writerId, skinId, "");
//    }
//
//	/**
//	 *  setBonusStateDescription event handler implementation
//	 */
//	@Override
//    public void setBonusStateDescription(BonusUsers bonusUser) throws BonusHandlersException{
//		// Do Nothing
//	}
//
//	/**
//	 *  activateBonusAfterInvestmentSuccessByInvAmount event handler implementation
//	 */
//	@Override
//	public boolean activateBonusAfterInvestmentSuccessByInvAmount(Connection conn, BonusUsers bu, long amountLeft, long userId, long investmentId, long writerId, boolean isFree) throws BonusHandlersException{
//		try{
//			boolean isAmountLeftUsed = true;
//
//			if (bu.getSumInvQualify() - bu.getSumInvQualifyReached() > amountLeft) {
//		        BonusDAOBase.addBonusUserQualifyWagering(conn, bu.getId(), amountLeft);
//		    } else {
//				if (bu.getSumInvQualify() - bu.getSumInvQualifyReached() > 0) {
//			    	BonusDAOBase.addBonusUserQualifyWagering(conn, bu.getId(), bu.getSumInvQualify());
//		            if (!isFree) { //check if the invest wasnt next invest on us
//		            	InvestmentsDAOBase.setInvestmentBonuUsersId(conn, investmentId, bu.getId());
//		            }
//				}else{
//					isAmountLeftUsed = false;
//				}
//
//				//	check if sum deposit was reached (in case there is one)
//	        	if (bu.getSumDeposits() == 0 || bu.getSumDepositsReached() >= bu.getSumDeposits()){
//	        		activateBonusAndAddToBalance(conn, bu, userId, writerId, 0, investmentId);
//	        	}
//		    }
//
//			return isAmountLeftUsed;
//		}catch (SQLException e) {
//			throw new BonusHandlersException("Error in activateBonusAfterInvestmentSuccessByInvAmount ", e);
//		}
//	}
//
//	/**
//	 *  activateBonusAfterInvestmentSuccessByInvCount event handler implementation
//	 */
//	@Override
//    public boolean activateBonusAfterInvestmentSuccessByInvCount(Connection conn, BonusUsers bu,  boolean isInvWasCountForActivation, long investmentId) throws BonusHandlersException{
//		return isInvWasCountForActivation;
//	}
//
//	/**
//	 *  handleBonusOnSettleInvestment event handler implementation
//	 */
//	@Override
//	public long handleBonusOnSettleInvestment(Connection conn, Investment investment, boolean isWin) throws BonusHandlersException{
//        return 0;
//	}
//
//	/**
//	 *  getAmountThatUserCantWithdraw event handler implementation
//	 */
//	@Override
//	public long getAmountThatUserCantWithdraw(long win, long lose, long invAmount, BonusUsers bu){
//		return 0;
//	}
//}