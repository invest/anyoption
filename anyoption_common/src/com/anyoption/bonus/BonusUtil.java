//package com.anyoption.bonus;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.Bonus;
//import com.anyoption.common.beans.Skin;
//import com.anyoption.common.beans.base.BonusUsers;
//import com.anyoption.common.bonus.BonusHandlersException;
//import com.anyoption.managers.BonusManagerBase;
//import com.anyoption.managers.TransactionsManagerBase;
//
//
///**
// * 
// * @author Lior
// *
// */
//public class BonusUtil {
//
//	private static final Logger log = Logger.getLogger(BonusUtil.class);
//
//    /**
//     * This method cancel bonus to user with the withdraw mechanism if the bonus is in active or in used state.
//     * 
//     * @param conn
//     * @param bonusUser
//     * @param stateToUpdate
//     * @param utcOffset
//     * @param writerId
//     * @param skinId
//     * @throws BonusHandlersException
//     */
//	public static void cancelBonusToUserWithdraw(Connection conn, BonusUsers bonusUser, long stateToUpdate, String utcOffset, long writerId, String ip, long skinId) throws BonusHandlersException {
//		try {
//	    	BonusManagerBase.updateBonusUser(conn, bonusUser.getId(), stateToUpdate, bonusUser.getWriterIdCancel());
//	    	if (bonusUser.getBonusStateId() == Bonus.STATE_ACTIVE || bonusUser.getBonusStateId() == Bonus.STATE_USED) {
//	    		long amountToDeduct = bonusUser.getBonusAmount();
//	    		if (skinId != Skin.SKIN_ETRADER) {
//	    			amountToDeduct = Math.max(bonusUser.getBonusAmount(), bonusUser.getAdjustedAmount());
//	    		}
//	    		TransactionsManagerBase.insertBonusWithdraw(conn, amountToDeduct, bonusUser.getUserId(), utcOffset, writerId, bonusUser.getId(), ip);
//	    	}
//		} catch (SQLException e) {
//			throw new BonusHandlersException("Error cancelBonusToUserWithdraw ",e);
//		}
//    }  
//}
