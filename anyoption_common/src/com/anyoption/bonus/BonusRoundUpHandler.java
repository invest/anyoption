//package com.anyoption.bonus;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//import java.util.HashMap;
//import java.util.Hashtable;
//
//import com.anyoption.beans.User;
//import com.anyoption.common.beans.Bonus;
//import com.anyoption.common.beans.base.BonusUsers;
//import com.anyoption.common.beans.base.Currency;
//import com.anyoption.common.bl_vos.BonusCurrency;
//import com.anyoption.common.bonus.BonusHandlersException;
//import com.anyoption.daos.BonusDAOBase;
//import com.anyoption.daos.InvestmentsDAOBase;
//import com.anyoption.managers.BonusManagerBase;
//import com.anyoption.managers.CurrenciesManagerBase;
//import com.anyoption.managers.TransactionsManagerBase;
//
//public class BonusRoundUpHandler extends BonusInstantAmountHandler {
//
//	/**
//	 *  bonusInsert event handler implementation
//	 */
//	@Override
//	public boolean bonusInsert(Connection conn,BonusUsers bu, Bonus b, BonusCurrency bc, User user, long writerId, long popEntryId, int bonusPopLimitTypeId, String ip) throws BonusHandlersException{
//		boolean res = false;
//		bu.setBonusStateId(Bonus.STATE_PENDING);
//		
//		try {
//			BonusManagerBase.getLimitsForBonus(conn, bu, b, bc, popEntryId,bonusPopLimitTypeId,user.getId(), user.getCurrencyId(), false);
//
//			long bonusAmount = 0;
//			if (b.getRoundUpType() == Bonus.ROUND_UP_TYPE_ROUND_UP) {
//	    		long minInv = InvestmentsDAOBase.getUserMinInvestmentLimit(conn, user.getCurrencyId());
//		    	long avgInv = InvestmentsDAOBase.getUserAvgInvestment(conn, user.getId());
//		    	avgInv = (long) (0.5 * avgInv);
//		    	bonusAmount = Math.max(minInv, avgInv);
//		    	bonusAmount = (bonusAmount/100);
//		    	bonusAmount = Math.round(bonusAmount/5.0) * 5;
//		    	bonusAmount = (bonusAmount*100);
//		    	bonusAmount = bonusAmount - user.getBalance();
//		    	if (bonusAmount <= 0){
//		    		return false;
//		    	}
//	    	} else if (b.getRoundUpType() == Bonus.ROUND_UP_TYPE_GET_STARTED) {
//	    		HashMap<Long, Long> minInvByCurrencyHM = new HashMap<Long, Long>();
//	    		Hashtable<Long, Currency> hm = CurrenciesManagerBase.getCurrencies();
//        		for (Long currencyId : hm.keySet()) {
//        			minInvByCurrencyHM.put(currencyId, InvestmentsDAOBase.getUserMinInvestmentLimit(conn, currencyId));
//        		}
//	    		long minInv = minInvByCurrencyHM.get(user.getCurrencyId());
//	    		bonusAmount = 4 * minInv;
//	    	}
//			bc.setBonusAmount(bonusAmount);
//
//			bu.setSumInvWithdrawal(bc.getBonusAmount() * b.getWageringParameter());
//
//			res = BonusDAOBase.insertBonusUser(conn, bu, b, bc, popEntryId);
//
//		} catch (SQLException e) {
//			throw new BonusHandlersException("Error in inserting bonus" , e);
//		}
//		return res;
//	}
//	
//	@Override
//	public boolean acceptBonus(Connection conn, BonusUsers bu, User user, String ip) throws SQLException {
//		boolean res = false;
//
//		long stateId = Bonus.STATE_DONE;;
//		//	for bonus deposit amount that have no wagering
//		if (bu.getWageringParam() != 0) {
//			stateId = Bonus.STATE_ACTIVE;
//		} 
//		
//		res = BonusDAOBase.acceptBonusUser(conn, bu.getId(), stateId);
//		
//		if (res) {
//			res = TransactionsManagerBase.insertBonusDeposit(conn, user, bu.getId(), bu.getWriterId(), bu.getBonusAmount(), bu.getComments(), ip);
//		}
//		
//		return res;
//	}
//}