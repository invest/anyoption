//package com.anyoption.bonus;
//
//
//import java.math.BigDecimal;
//import java.sql.Connection;
//import java.sql.SQLException;
//import java.util.ArrayList;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.beans.Investment;
//import com.anyoption.beans.User;
//import com.anyoption.common.beans.Bonus;
//import com.anyoption.common.beans.base.BonusUsers;
//import com.anyoption.common.beans.base.BonusUsersStep;
//import com.anyoption.common.bl_vos.BonusCurrency;
//import com.anyoption.common.bonus.BonusHandlerBase;
//import com.anyoption.common.bonus.BonusHandlersException;
//import com.anyoption.common.bonus.BonusUtil;
//import com.anyoption.common.daos.TransactionsDAOBase;
//import com.anyoption.daos.BonusDAOBase;
//import com.anyoption.daos.UsersDAOBase;
//import com.anyoption.managers.BonusManagerBase;
//import com.anyoption.managers.TransactionsManagerBase;
//import com.anyoption.managers.UsersManagerBase;
//import com.anyoption.util.CommonUtil;
//import com.anyoption.util.ConstantsBase;
//
//public class BonusPercentAfterDepositHandler extends BonusHandlerBase {
//
//	private static final Logger log = Logger.getLogger(BonusPercentAfterDepositHandler.class);
//
//	/**
//	 *  bonusInsert event handler implementation
//	 */
//	@Override
//	public boolean bonusInsert(Connection conn,BonusUsers bu, Bonus b, BonusCurrency bc, User user, long writerId, long popEntryId, int bonusPopLimitTypeId, String ip) throws BonusHandlersException{
//		boolean res = false;
//		bu.setBonusStateId(Bonus.STATE_PENDING);
//		
//		long currencyId = user.getCurrencyId();
//		boolean isHasSteps = b.isHasSteps();
//
//		try {
//			boolean isHasLimits = BonusManagerBase.getLimitsForBonus(conn, bu, b, bc, popEntryId,bonusPopLimitTypeId,user.getId(), currencyId, isHasSteps);
//
//			// set min deposit amount dynamically
//			if (!isHasSteps && bu.getSumDeposits() > 0) {
//				bc.setMinDepositAmount(bu.getSumDeposits());
//			}
//
//			res = BonusDAOBase.insertBonusUser(conn, bu, b, bc, popEntryId);
//
//    		if (res && isHasSteps) {
//
//    			ArrayList<BonusUsersStep> bonusSteps = null;
//
//    			if (isHasLimits) {
//    				bonusSteps = bu.getBonusSteps();
//    			} else {
//					bonusSteps = BonusDAOBase.getBonusSteps(conn, b.getId(), currencyId);
//				}
//
//    			res = BonusDAOBase.insertBonusUserSteps(conn, bonusSteps, bu.getId());
//    		}
//
//		} catch (SQLException e) {
//			throw new BonusHandlersException("Error in inserting bonus" , e);
//		}
//		return res;
//	}
//
//	/**
//	 *  getBonusMessage event handler implementation
//	 */
//	@Override
//	public String getBonusMessage(Bonus b, BonusCurrency bc, long popEnteyId, int bonusPopLimitTypeId, long userId) throws BonusHandlersException{
//		String out = "";
//		boolean isHaveSteps = b.isHasSteps();
//		long bonusId = b.getId();
//
//		try{
//			String descMsg = BonusManagerBase.getBonusDescription(bonusId, 1); // state could only be 1 - granted
//
//			if(b.getNumberOfActions()>1)	{
//				out+="Make " + b.getNumberOfActions() +" deposits and get " + bc.getBonusPercent()*100 + "% of the " +b.getNumberOfActions()+"th  deposit to your account<br/>";
//			}
//			if (null != bc){
//				long currencyId = bc.getCurrencyId();
//				BonusUsers limitsBu = new BonusUsers();
//
//				limitsBu.setBonusId(bonusId);
//				boolean isHasLimits = BonusManagerBase.getLimitsForBonus(limitsBu, b, bc, popEnteyId, bonusPopLimitTypeId,userId, currencyId, isHaveSteps);
//				long minDepositAmount = limitsBu.getSumDeposits();
//
//				if(!isHaveSteps){
//					if(!(b.getNumberOfActions()>1)){
//						out += BonusHandlerBase.getBonusDescription(bc.getBonusAmount()/100,bc.getBonusPercent(),bc.getSumInvestQualify()/100,
//								b.getNumberOfActions(),descMsg,String.valueOf(bc.getMinDepositAmount()/100),String.valueOf(bc.getMaxDepositAmount()/100),
//								String.valueOf(bc.getMinInvestAmount()/100), bc.getMaxInvestAmount(), String.valueOf((int)(b.getOddsWin()*100)),
//								String.valueOf((int)(b.getOddsLose()*100)), currencyId) +  " <br/>";
//					}
//
//					out += "Wagering parameter " + b.getWageringParameter() + " on the " + b.getCondition() +  " <br/>";
//
//					if (minDepositAmount >0){
//						out += "<br/>" +
//						CommonUtil.getMessage(null,"bonus.population.grant.msg", new String[] {CommonUtil.formatCurrencyAmount(minDepositAmount, true, currencyId)});
//					}
//
//
//				} else { // bonus has steps
//					ArrayList<BonusUsersStep> bonusSteps = null;
//
//					if (isHasLimits) {
//						bonusSteps = limitsBu.getBonusSteps();
//	            	}else{
//	            		bonusSteps = BonusManagerBase.getBonusSteps(bonusId, currencyId);
//	            	}
//
//					if (null != bonusSteps){
//						for(BonusUsersStep step:bonusSteps) {
//							out += "Make a deposit between "+step.getMinDepositAmount()/100+" and "+step.getMaxDepositAmount()/100;
//							out += " and get bonus of "+step.getBonusPercent()*100+"% of the deposit to your account<br/>";
//							out += "Wagering parameter "+b.getWageringParameter()*step.getBonusPercent()+" ";
//							out += "on the "+b.getCondition()+"<br/>";
//						}
//					}else{
//						throw new BonusHandlersException("No Bonus steps for bonus id " + bonusId +
//								" with currency id: " + currencyId + " for user id " + userId);
//					}
//				}
//			}else{
//				throw new BonusHandlersException("No BonusCurrency for bonus id " + bonusId + " for user id " + userId);
//			}
//		} catch (SQLException e) {
//			throw new BonusHandlersException("Error in creating bonus message for bonus id " + bonusId + " for user id " + userId , e);
//		}
//		return out;
//	}
//
//	/**
//	 *  isActivateBonus event handler implementation
//	 */
//	@Override
//	public boolean isActivateBonusAfterTransactionSuccess(Connection conn, BonusUsers bu, long amount, long transactionId) throws BonusHandlersException{
//    	long minDepositAmount = bu.getMinDepositAmount();
//        long maxDepositAmount = bu.getMaxDepositAmount();
//        boolean isActivate = false;
//
//    	try {
//
//	        if (bu.isHasSteps()){
//	        	ArrayList<BonusUsersStep> stepsList = BonusManagerBase.getBonusUsersSteps(conn, bu.getId());
//
//	        	for (int index=0; index<stepsList.size(); index++){
//	        		if (stepsList.get(index).getMinDepositAmount() <= amount &&
//	        				stepsList.get(index).getMaxDepositAmount() >= amount){
//    					bu.setBonusPercent(stepsList.get(index).getBonusPercent());
//    					bu.setMinDepositAmount(stepsList.get(index).getMinDepositAmount());
//    					bu.setMaxDepositAmount(stepsList.get(index).getMaxDepositAmount());
//    					isActivate = true;
//    					break;
//	   				}
//	        	}
//
//	        }else if((minDepositAmount == 0 && maxDepositAmount == 0) ||
//	        			(minDepositAmount <= amount &&	maxDepositAmount >= amount)){
//	        	isActivate = true;
//	        }
//
////	        if (isActivate){
////	        	TransactionsDAOBase.setTransactionBonuUsersId(conn, transactionId, bu.getId());
////	        }
//
//		} catch (SQLException e) {
//			throw new BonusHandlersException("Error in getBonusUsersSteps ",e);
//		}
//
//		return isActivate;
//	}
//
//	/**
//	 *  activateBonusAfterTransactionSuccess event handler implementation
//	 */
//	@Override
//	public void activateBonusAfterTransactionSuccess(Connection conn, BonusUsers bu, long transactionId, long userId, long amount, long writerId) throws BonusHandlersException{
//
//		try {
//			if (bu.getNumOfActionsReached() + 1 == bu.getNumOfActions() ) {
//	            long bonusAmount = bu.getBonusAmount();
//	            long sumInvestWithdraw = bu.getSumInvWithdrawal();
//
//	    		BigDecimal bd = new BigDecimal(amount);
//	            bd = bd.multiply(new BigDecimal(Double.toString(bu.getBonusPercent())));
//	            bd = CommonUtil.round(bd, new BigDecimal(1));
//	            bonusAmount = bd.longValue();
//	            if (bu.getBonusAmount() > 0 && bonusAmount > bu.getBonusAmount()) {  // percent up to X amount
//	            	bonusAmount = bu.getBonusAmount();
//	            }
//	            sumInvestWithdraw = bonusAmount * bu.getWageringParam();
//
//	            bu.setBonusAmount(bonusAmount);
//	            bu.setSumInvWithdrawal(sumInvestWithdraw);
//
//	            try {
//	                conn.setAutoCommit(false);
//
//		            BonusDAOBase.activateBonusUsers(conn, bu, transactionId, 0);
//		            UsersDAOBase.addToBalance(conn, userId, bonusAmount);
//		            User user = UsersManagerBase.getById(conn,userId);
//		            TransactionsDAOBase.insert(conn, TransactionsManagerBase.createBonusTransaction(userId, bonusAmount, bu.getId(), writerId,user));
//		            UsersDAOBase.insertBalanceLog(
//		                    conn,
//		                    writerId,
//		                    userId,
//		                    ConstantsBase.TABLE_BONUS_USERS,
//		                    bu.getId(),
//		                    ConstantsBase.LOG_BALANCE_BONUS_DEPOSIT,
//		                    user.getUtcOffset());
////		            TransactionsDAOBase.setTransactionBonuUsersId(conn, transactionId, bu.getId());
//
//		            conn.commit();
//
//	            } catch (SQLException sqle) {
//	                log.error("Can't set connection back to autocommit.", sqle);
//	                try {
//	                    conn.rollback();
//	                } catch (SQLException sqlie) {
//	                    log.error("Can't rollback.", sqlie);
//	                }
//	                throw sqle;
//	            } finally {
//	           		conn.setAutoCommit(true);
//	            }
//	        } else {
//	        	BonusDAOBase.addBonusUsersAction(conn, bu.getId());
//	        }
//	    }catch (SQLException sqle) {
//            throw new BonusHandlersException("can't addBonusUsersAction ",sqle);
//        }
//	}
//
//	/**
//	 *  touchBonusesAfterInvestmentSuccess event handler implementation
//	 */
//	@Override
//	public long touchBonusesAfterInvestmentSuccess(Connection conn, BonusUsers bu, long amountLeft) throws BonusHandlersException{
//		try {
//			BonusDAOBase.useBonusUsers(conn, bu.getId());
//		} catch (SQLException e) {
//			throw new BonusHandlersException("Error in touchBonusesAfterInvestmentSuccess ",e);
//		}
//        return (amountLeft - bu.getBonusAmount());
//	}
//
//	/**
//	 *  cancelBonusToUser event handler implementation
//	 */
//	@Override
//	public void cancelBonusToUser(Connection conn, BonusUsers bonusUser, long stateToUpdate, String utcOffset, long writerId, String ip, long skinId) throws BonusHandlersException {
//		BonusUtil.cancelBonusToUserWithdraw(conn, bonusUser, stateToUpdate, utcOffset, writerId, ip, skinId);
//    }
//
//	/**
//	 *  setBonusStateDescription event handler implementation
//	 */
//	@Override
//    public void setBonusStateDescription(BonusUsers bonusUser) throws BonusHandlersException{
//		long bonusStateId = bonusUser.getBonusStateId();
//
//    	if(bonusUser.isHasSteps()){
//    		try{
//    			bonusUser.setBonusSteps(BonusManagerBase.getBonusUsersSteps(bonusUser.getId()));
//    		}catch (SQLException e) {
//    			throw new BonusHandlersException("Error getBonusUsersSteps ",e);
//    		}
//
//    		if(bonusUser.getBonusStateId() == Bonus.STATE_ACTIVE){
//    			bonusUser.setBonusStateDescription("bonus.percentAfterDeposit.steps.active.description");
//    		}else{
//    			bonusUser.setBonusStateDescription("bonus.percentAfterDeposit.steps.granted.description");
//    		}
//
//    	} else if(bonusUser.getNumOfActions()>1){
//    		if(Bonus.STATE_ACTIVE == bonusStateId){
//    			bonusUser.setBonusStateDescription("bonus.percentAfterDeposit.numOfActions.active.description");
//    		} else if(Bonus.STATE_GRANTED == bonusStateId ||
//    				Bonus.STATE_MISSED == bonusStateId ||
//					Bonus.STATE_USED == bonusStateId ||
//					Bonus.STATE_DONE == bonusStateId){
//    			bonusUser.setBonusStateDescription("bonus.percentAfterDeposit.numOfActions.granted.description");
//    		}
//    	}
//	}
//
//	/**
//	 *  activateBonusAfterInvestmentSuccessByInvAmount event handler implementation
//	 */
//	@Override
//	public boolean activateBonusAfterInvestmentSuccessByInvAmount(Connection conn, BonusUsers bu, long amountLeft, long userId, long investmentId, long writerId, boolean isFree) throws BonusHandlersException{
//		return false;
//	}
//
//	/**
//	 *  activateBonusAfterInvestmentSuccessByInvCount event handler implementation
//	 */
//	@Override
//    public boolean activateBonusAfterInvestmentSuccessByInvCount(Connection conn, BonusUsers bu,  boolean isInvWasCountForActivation, long investmentId) throws BonusHandlersException{
//		return isInvWasCountForActivation;
//	}
//
//	/**
//	 *  handleBonusOnSettleInvestment event handler implementation
//	 */
//	@Override
//	public long handleBonusOnSettleInvestment(Connection conn, Investment investment, boolean isWin) throws BonusHandlersException{
//        return 0;
//	}
//
//	/**
//	 *  getAmountThatUserCantWithdraw event handler implementation
//	 */
//	@Override
//	public long getAmountThatUserCantWithdraw(long win, long lose, long invAmount, BonusUsers bu){
//		return 0;
//	}
//}