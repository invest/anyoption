package com.anyoption.bl_vos;

import java.io.Serializable;
import java.util.Date;

/**
 * Trading page message bean.
 *
 * @author Tony
 */
public class Message implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final long WEB_SCREEN_HOME = 1;
    public static final long WEB_SCREEN_INDICES = 2;
    public static final long WEB_SCREEN_STOCKS = 3;
    public static final long WEB_SCREEN_CURRENCIES = 4;
    public static final long WEB_SCREEN_COMMODITIES = 5;
    public static final long WEB_SCREEN_FOREIGN_STOCKS = 6;
    public static final long WEB_SCREEN_ONE_TOUCH = 7;
    public static final long WEB_SCREEN_ETRADER_INDICES = 8;

    protected long id;
    protected String text;
    protected long webScreen;
    protected Date startEffDate;
    protected Date endEffDate;
    protected long skinId;
    protected long languageId;

    public Date getEndEffDate() {
        return endEffDate;
    }

    public void setEndEffDate(Date endEffDate) {
        this.endEffDate = endEffDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getStartEffDate() {
        return startEffDate;
    }

    public void setStartEffDate(Date startEffDate) {
        this.startEffDate = startEffDate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getWebScreen() {
        return webScreen;
    }

    public void setWebScreen(long webScreen) {
        this.webScreen = webScreen;
    }


    /**
	 * @return the languageId
	 */
	public long getLanguageId() {
		return languageId;
	}

	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(long languageId) {
		this.languageId = languageId;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "Message:" + ls +
            "id: " + id + ls +
            "text: " + text + ls +
            "webScreen: " + webScreen + ls +
            "startEffDate: " + startEffDate + ls +
            "endEffDate: " + endEffDate + ls +
        	"skinId: " + skinId + ls +
        	"languageId: " + languageId + ls;
    }
}