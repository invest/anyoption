package com.anyoption.managers;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.beans.base.CreditCard;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.daos.CcBlackListDAOBase;
import com.anyoption.daos.ClearingDAO;
import com.anyoption.util.ConstantsBase;

/**
 * Admin manager.
 */
public abstract class AdminManagerBase extends BaseBLManager {

    public static boolean isBlacklisted(CreditCard card, boolean deposit , User user) throws SQLException {
        boolean b = false;
        Connection con = getConnection();
        try {
            b = CcBlackListDAOBase.isCardBlacklisted(con, card.getCcNumber(), deposit)
                    || isBINBlacklisted(con, deposit, user, card);
        } finally {
            closeConnection(con);
        }
        return b;
    }

    public static boolean isCCBlacklisted(long ccn, boolean deposit , long skinId) throws SQLException {
        boolean b = false;
        Connection con = getConnection();
        try {
            b = CcBlackListDAOBase.isCardBlacklisted(con, ccn, deposit);
        } finally {
            closeConnection(con);
        }
        return b;
    }

    public static boolean isCCBlackListedWithdraw(long ccn) throws SQLException{
    	boolean retVal;
    	Connection con = getConnection();
    	try{
    		retVal = CcBlackListDAOBase.isCardBlacklistedWithdraw(con, ccn);
    	}finally{
    		closeConnection(con);
    	}
    	return retVal;

    }

    public static boolean isBINBlacklisted(CreditCard card, boolean deposit , User user) throws SQLException {
        Connection con = getConnection();
        try {
        	return isBINBlacklisted(con, deposit, user, card);
        } finally {
            closeConnection(con);
        }
    }

    public static boolean isBINBlacklisted(Connection con,  boolean deposit , User user, CreditCard card) throws SQLException {
    	String bin = String.valueOf(card.getCcNumber()).substring(0, 6);
        
    	if(TransactionsManagerBase.isBlackListBin(bin)){
    		return true;
    	}
    	
    	long clearingProviderId = 0;
       	clearingProviderId = ClearingDAO.findRoute(con, user.getSkinId(), bin, user.getCurrencyId(), deposit, 
       			card.getCountryId(),  card.getTypeIdByBin(), user.getPlatformId()).getDepositClearingProviderId();

        return (clearingProviderId == ConstantsBase.BLACK_LIST_BIN);
    }

//    public static long getCityIdByName(String n) throws SQLException {
//        Connection con = getConnection();
//        try {
//            return CitiesDAO.getIdByName(con, n);
//        } finally {
//            closeConnection(con);
//        }
//    }

//    public static City getCityById(long n) throws SQLException {
//        Connection con=getConnection();
//        try {
//            return CitiesDAO.getById(con, n);
//        } finally {
//            closeConnection(con);
//        }
//    }

//    public static void addToLog(long writerId, String table, long key, int command, String desc) throws SQLException {
//        Connection con = getConnection();
//        try {
//            GeneralDAO.insertLog(con, writerId, table, key, command, desc);
//        } finally {
//            closeConnection(con);
//        }
//    }


}