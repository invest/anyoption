/**
 * 
 */
package com.anyoption.managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Bonus;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.daos.UsersDAOBase;
import com.copyop.common.dto.CoinsBalanceHistory;
import com.copyop.common.dto.CoinsConvert;
import com.copyop.common.enums.CoinsActionTypeEnum;
import com.copyop.common.managers.CoinsManager;
import com.copyop.common.managers.ProfileCountersManager;
import com.copyop.common.managers.ProfileManager;

public class CopyopCoinsConvertManager extends BaseBLManager {

	private static final Logger log = Logger.getLogger(CopyopCoinsConvertManager.class);

    public static BonusUsers convertCopyopCoins(User user, long writerId, String ip) throws Exception {
    	BonusUsers bonusUser = null;
    	Connection conn = null;
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            
            UsersDAOBase.lockUserTable(conn, user.getId());
            CoinsConvert coinsConvert = CoinsManager.getCopyopCoinsConvertValue(user.getId());
            log.debug(coinsConvert);
            
            if(coinsConvert.getAmount() > 0){               	
                bonusUser = new BonusUsers();
                com.anyoption.beans.User u = UsersDAOBase.getById(conn, user.getId());
                bonusUser.setBonusId(Bonus.COPYOP_COINS_CONVERT);
                bonusUser.setBonusAmount(coinsConvert.getAmount());
                bonusUser.setUserId(user.getId());
                bonusUser.setWriterId(writerId);
                
                if(!BonusManagerBase.insertBonusUser(conn, bonusUser, u, writerId, 0, 0, ip)){
                	log.error("Can't insert COPYOP_COINS_CONVERT Bonus!!!");
                	throw new Exception();
                }
				
				ProfileCountersManager.increaseProfileCoinsBalance(user.getId(), 0 - coinsConvert.getConvrtedCoins());
				ProfileManager.updateTimeResetCoinsNow(user.getId());
				CoinsManager.insertCoinsBalanceHistory(new CoinsBalanceHistory(user.getId(), CoinsActionTypeEnum.CONVERT.getId(), true, 0 - coinsConvert.getConvrtedCoins(), coinsConvert.getAmount(), coinsConvert.toString(), writerId));					
            }            
            conn.commit();  
        } catch (Exception e) {
            try {
                conn.rollback();
            } catch (SQLException sqle) {
                log.error("Can't rollback.", sqle);
            }
            throw e;
        } finally {
            conn.setAutoCommit(true);
            closeConnection(conn);
        }        
        return bonusUser;
    }
}
