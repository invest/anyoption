package com.anyoption.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;

import org.apache.log4j.Logger;

import com.anyoption.beans.RiskAlert;
import com.anyoption.common.beans.File;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.daos.InvestmentsDAOBase;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.managers.RiskAlertsManagerCommonBase;
import com.anyoption.daos.FilesDAOBase;
import com.anyoption.daos.RiskAlertsDAOBase;
import com.anyoption.util.CommonUtil;
import com.anyoption.util.ConstantsBase;


/**
 * RiskAlerts Manager
 *
 * @author Kobi
 *
 */
public class RiskAlertsManagerBase extends RiskAlertsManagerCommonBase {
    private static final Logger logger = Logger.getLogger(RiskAlertsManagerBase.class);

	// Risk alerts types
	public static final int RISK_TYPE_CC_USED_ON_WEBSITE = 1;
	public static final int RISK_TYPE_CC_STOLEN = 2;
	public static final int RISK_TYPE_CC_DEPOSIT_MORE_THAN_ALLOWED = 3;
	public static final int RISK_TYPE_DEPOSIT_BALANCE_GREATER_THAN_MIN_INVEST = 4;
	public static final int RISK_TYPE_WITHDRAW_NO_INVESTMENTS = 5;
	public static final int RISK_TYPE_WIRE_WITHDRAW_AMOUNT_GREATER_THAN_BALANCE = 6;
	public static final int RISK_TYPE_FIRST_INVESTMENT_IS_1T = 7;
	public static final int RISK_TYPE_DEPOSIT_MISMATCH_CARD_HOLDER = 8;
	public static final int RISK_TYPE_WAGERING_FAILED_WITHDRAW = 9;
	public static final int RISK_TYPE_X_DEPOSIT_NO_DOCUMENT = 10;
	public static final int RISK_TYPE_DEPOSIT_GREATER_THAN_X_NO_DOCUMENT = 11;
	public static final int RISK_TYPE_DEPOSIT_VELOCITY_AO_NO_DOC = 13;
	public static final int RISK_TYPE_DEPOSIT_VELOCITY_AO_HAVE_DOC = 14;
	public static final int RISK_TYPE_DEPOSIT_VELOCITY_ET_NO_DOC = 15;
	public static final int RISK_TYPE_DEPOSIT_VELOCITY_ET_HAVE_DOC = 16;
	public static final int RISK_TYPE_DEPOSIT_CC_FROM_DIFFERENT_COUNTRIES = 19;

	private static final long NO_INVESTMENT_ID = 0;
	private static final long NO_TRANSACTION_ID = 0;

	public static final int TYPE_INVESTMENT = 99;
	public static final int RISK_ALERT_INVESTMENT_CLASS_TYPE = 3;
	
	// Risk alert enumerators 
	public static final long INVESTMENT_LIMITS_GROUP_ID = 1;
	public static final String RISK_ALERT_ENUMERATOR  = "risk_alert";
	public static final String ET_DEPOSIT_LIMIT_NO_CODE_  = "et_deposit_limit_no_doc";
	public static final String AO_DEPOSIT_LIMIT_NO_DOC_CODE  = "ao_deposit_limit_no_doc";
	public static final String X_DEPOSIT_NO_DOCUMNET_CODE  = "x_deposit_no_doc_num";
	public static final int RISK_TYPE_XX_DEPOSIT_NO_DOCUMENT = 12;
	
	public static final int FIVE_DOC_SUBJECT = 36;
    public static final int FIVE_DOC_PRIORITY = 1;
    public static final int FIVE_DOC_STATUS = 4;
    public static final int FIVE_DOC_ISSUE_TYPE = 2;
    public static final String FIVE_DOC_ISSUE = "2";
    
    //Doc request type
    public static final String RISK_DOC_REQ_ALL = "0";
    public static final String RISK_DOC_REQ_ID = "1";
    public static final String RISK_DOC_REQ_CC = "2";
    public static final String RISK_DOC_REQ_CONTACT_USER = "3";
    
    public static final long RISK_ISSUE_STATUS_NEW = 4;
    public static final String RISK_ISSUE_STATUS_NEW_STRING = "4";


    /**
     * Insert into risk alert table
     * @param typeId
     * @param writerId
     * @param trxId
     * @param invId
     * @throws SQLException
     */
    public static void insert(int typeId, long writerId, long trxId, long invId) throws SQLException {
    	if (logger.isInfoEnabled()) {
	    	String ls = System.getProperty("line.separator");
	    	logger.info("Going to insert new risk alert: " + ls +
	    			"typeId: " + typeId + ls +
	    			"trxId: " + trxId + ls +
	    			"invId: " + invId);
    	}
        Connection con = getConnection();
        try {
        	RiskAlertsDAOBase.insert(con, new RiskAlert(typeId, writerId, trxId, invId));
        } finally {
            closeConnection(con);
        }
    }


    /**
     * Handle Deposit alerts
     * @param writerId
     * @param error
     * @param tran
     * @param user
     * @param riskAlert
     * @param errorCode
     * @throws Exception 
     */
    public static void riskAlertDepositHandler(long writerId, String error, Transaction tran,
    		User user, boolean riskAlert, String errorCode, String ccHolderName,
    		boolean firstDeposit, boolean isDocumentsSent, long ccNumber) throws Exception{

    	Connection con = null;
    	//Duplicate card
    	if (error != null && error.equals(ConstantsBase.ERROR_CARD_NOT_ALLOWED_DUP_CARD)){
    		insert(RISK_TYPE_CC_USED_ON_WEBSITE, writerId, tran.getId(), NO_INVESTMENT_ID);
    	}
    	//Stolen card
    	if (isStolenCard(errorCode, tran)){
			insert(RISK_TYPE_CC_STOLEN, writerId, tran.getId(), NO_INVESTMENT_ID);
		}
    	//CC deposit more than allowed
    	if(error != null && error.equals(ConstantsBase.ERROR_MAX_DEPOSIT)){
    		insert(RISK_TYPE_CC_DEPOSIT_MORE_THAN_ALLOWED, writerId, tran.getId(), NO_INVESTMENT_ID);
    	}

    	//Mismatch card holder
    	if (error == null && !CommonUtil.IsParameterEmptyOrNull(ccHolderName) && firstDeposit == true){
    		String userName = user.getFirstName() + " " + user.getLastName();
    		if(!userName.equalsIgnoreCase(ccHolderName)){
    			insert(RISK_TYPE_DEPOSIT_MISMATCH_CARD_HOLDER, writerId, tran.getId(), NO_INVESTMENT_ID);
    		}
    	}
    	
    	//Velocity Checks
    	if (error != null && error.equals(ConstantsBase.ERROR_DEPOSIT_VELOCITY_AO_NO_DOC)){
    		insert(RISK_TYPE_DEPOSIT_VELOCITY_AO_NO_DOC, writerId, tran.getId(), NO_INVESTMENT_ID);
    	} else if (error != null && error.equals(ConstantsBase.ERROR_DEPOSIT_VELOCITY_AO_HAVE_DOC)){
    		insert(RISK_TYPE_DEPOSIT_VELOCITY_AO_HAVE_DOC, writerId, tran.getId(), NO_INVESTMENT_ID);
    	} else if (error != null && error.equals(ConstantsBase.ERROR_DEPOSIT_VELOCITY_ET_NO_DOC)){
    		insert(RISK_TYPE_DEPOSIT_VELOCITY_ET_NO_DOC, writerId, tran.getId(), NO_INVESTMENT_ID);
    	} else if (error != null && error.equals(ConstantsBase.ERROR_DEPOSIT_VELOCITY_ET_HAVE_DOC)){
    		insert(RISK_TYPE_DEPOSIT_VELOCITY_ET_HAVE_DOC, writerId, tran.getId(), NO_INVESTMENT_ID);
    	}

    	con = getConnection();

    	//Docs required
    	if (!isDocumentsSent){
    		String enumerator = null;
    		if (!CommonUtil.isHebrewSkin(user.getSkinId())){//For AO users
    			enumerator = CommonUtil.getEnum(RISK_ALERT_ENUMERATOR,AO_DEPOSIT_LIMIT_NO_DOC_CODE);
    		}
    		else{//For Etrader users
    			enumerator = CommonUtil.getEnum(RISK_ALERT_ENUMERATOR,ET_DEPOSIT_LIMIT_NO_CODE_);
    		}
    		//Deposit greater than X and no document
    		if (tran.getAmount()> 0 && tran.getAmount() > Long.valueOf(enumerator)){
    			insert(RISK_TYPE_DEPOSIT_GREATER_THAN_X_NO_DOCUMENT, writerId, tran.getId(), NO_INVESTMENT_ID);
    		}
    		//X deposit and no document
    		enumerator = CommonUtil.getEnum(RISK_ALERT_ENUMERATOR,X_DEPOSIT_NO_DOCUMNET_CODE);
    		if (RiskAlertsDAOBase.getNumberOfDeposit(con, user.getId()) >= Long.valueOf(enumerator) ) {
                if (RiskAlertsDAOBase.getNumberOfDeposit(con, user.getId()) == Long.valueOf(enumerator)) {
                    insert(RISK_TYPE_X_DEPOSIT_NO_DOCUMENT, writerId, tran.getId(), NO_INVESTMENT_ID);
                }
                if (!IssuesManagerBase.isExistsIssueByUserAndCC(ccNumber, user.getId(), FIVE_DOC_SUBJECT, con)) {
                    int resID[] = IssuesManagerBase.getExistsIssueByUserAndCC(tran.getCreditCardId().longValue(), user.getId(), con);
                    
                    Issue issue = new Issue();
                    issue.setUserId(user.getId());
                    issue.setStatusId(Integer.toString(FIVE_DOC_STATUS));
                    issue.setPriorityId(Integer.toString(FIVE_DOC_PRIORITY));
                    issue.setSubjectId(Integer.toString(FIVE_DOC_SUBJECT));
                    issue.setCreditCardId(tran.getCreditCardId().longValue());
                    issue.setType(FIVE_DOC_ISSUE_TYPE);
                    
                    IssueAction issueAction = new IssueAction();
                    
                    issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_RISK_NEED_DOCUMENT));
                    issueAction.setSignificant(false);
                    issueAction.setComments("Please ask for docs");
                    issueAction.setRiskReasonId(FIVE_DOC_ISSUE);
       
                    if (resID[0] == 0) {
                        IssuesManagerBase.insertIssue(issue, issueAction, user.getUtcOffset(), writerId, user, 0, -1, con);
                    } else {
                        issue.setId(Long.valueOf(resID[0]));
                        if(resID[2] == IssuesManagerBase.FILES_RISK_STATUS_CLOSED) {
                            issue.setStatusId(RISK_ISSUE_STATUS_NEW_STRING);
                            IssuesManagerBase.updateIssueState(IssuesManagerBase.RISK_STATUS_NEW_ISSUE, issueAction.getActionId(), issue.getId(), con);
                        }
                        IssuesManagerBase.insertAction(issue, issueAction, user.getUtcOffset(), writerId, user, 0, -1, con);
                    }
                    if (resID[1] != 1 && resID[3] != 1) {                        
                        IssuesManagerBase.insertDocRequest(issueAction.getActionId(), 0, RISK_DOC_REQ_ID, user.getId(), con);
                    }
                    if (resID[1] != 2 && resID[3] != 1) {
                        IssuesManagerBase.insertDocRequest(issueAction.getActionId(), tran.getCreditCardId().longValue(), RISK_DOC_REQ_CC, user.getId(), con);                        
                    }
                    IssuesManagerBase.updateDocState(user.getId(), tran.getCreditCardId().longValue(), IssuesManagerBase.FILES_RISK_STATUS_NEW_ISSUE, con);
                }
                if (RiskAlertsDAOBase.getNumberOfDeposit(con, user.getId()) == Long.valueOf(RISK_TYPE_X_DEPOSIT_NO_DOCUMENT)) {
                    insert(RISK_TYPE_XX_DEPOSIT_NO_DOCUMENT, writerId, tran.getId(), NO_INVESTMENT_ID); 
                } 
            }
    	}

    	//User balance greater than minimum investment
    	if ((error == null) && (tran.getStatusId() == TransactionsManagerBase.TRANS_STATUS_PENDING
    			|| tran.getStatusId() == TransactionsManagerBase.TRANS_STATUS_SUCCEED)) {

    		long amount = InvestmentsDAOBase.getUserMinInvestmentLimit(con, user.getCurrencyId());
    		if ((user.getBalance()- tran.getAmount()) > amount){ //Remove current amount from balance
    			insert(RISK_TYPE_DEPOSIT_BALANCE_GREATER_THAN_MIN_INVEST, writerId, tran.getId(), NO_INVESTMENT_ID);
    		}
    	}

    }

    /**
     * Handle Withdrawal alerts
     * @param writerId
     * @param error
     * @param trxId
     * @param amount
     * @param userId
     * @param balance
     * @throws SQLException
     */
    public static void riskAlertWithdrawalHandler(long writerId, String error, long trxId, long amount, long userId,long balance) throws SQLException{
		Connection con = null;
		try {			
			con = getConnection();
    		//Withdraw amount greater than the balance
            if (null != error && (error.equals("error.transaction.bankWire.highamount")
            		|| error.equals("error.withdraw.highAmount"))) {
            	insert(RISK_TYPE_WIRE_WITHDRAW_AMOUNT_GREATER_THAN_BALANCE, writerId, trxId, NO_INVESTMENT_ID); 
           	//Wagering Failed            	
            } else if (null != error && error.equals("bonus.cannot.withdraw.error")) {  
            	insert(RISK_TYPE_WAGERING_FAILED_WITHDRAW, writerId, trxId, NO_INVESTMENT_ID);
       		//Withdraw with no investment            	
            } else if (!InvestmentsDAOBase.hasInvestments(con, userId, false)) {
    			insert(RISK_TYPE_WITHDRAW_NO_INVESTMENTS, writerId, trxId, NO_INVESTMENT_ID);
	    	}	    		
		} finally {
			closeConnection(con);
		}
    }

    /**
     * Handle Investments alerts
     * @param writerId
     * @param userId
     * @param invId
     * @throws SQLException
     */
    public static void riskAlertInvestmentHandler(long writerId, long userId, long invId) throws SQLException {
		Connection con = getConnection();
		try {
			//First investment of the user is 1T
	    	if (invId != 0 && !InvestmentsDAOBase.hasInvestments(con, userId, true)) {
	    		insert(RISK_TYPE_FIRST_INVESTMENT_IS_1T, writerId, NO_TRANSACTION_ID, invId);
	    	}
		} finally {
			closeConnection(con);
		}
    }


    /**
     * Check if card is stolen
     * @param errorCode
     * @param tran
     * @return
     */
    public static boolean isStolenCard(String errorCode,Transaction tran){
		if ( (tran.getClearingProviderId() == ClearingManager.XOR_ET_PROVIDER_ID ||
				tran.getClearingProviderId() == ClearingManager.XOR_AO_PROVIDER_ID) &&
				(errorCode.equals("001") || errorCode.equals("002") || errorCode.equals("005"))) {
			return true;
		}
		if (isCreditCardStolen(errorCode, tran)){
			return true;
		}

		return false;
    }
    
	public void insertAction(Connection conn,long userId,Issue i,IssueAction action, boolean mandQuestDone) throws Exception{
		
		//call file insert
		LinkedHashSet<Long> fileTypeHashMap = null;
    	fileTypeHashMap = new LinkedHashSet<Long>(); 
    	if(mandQuestDone) {
		    fileTypeHashMap.add( FileType.USER_ID_COPY); 
		    fileTypeHashMap.add( FileType.PASSPORT); 
		    fileTypeHashMap.add( FileType.DRIVER_LICENSE); 
		    fileTypeHashMap.add(FileType.UTILITY_BILL);
    	}
    	
		fileTypeHashMap.add( FileType.CC_COPY_FRONT);  
		fileTypeHashMap.add( FileType.CC_COPY_BACK);

	    long tempCCId = 0;   
	    try {
	    ArrayList<File> fileList = FilesDAOBase.getUserFileIdsByIdNoIssue(conn, userId);
		if(null != fileList) {
			for(File file : fileList) {
				if(file.getFileStatusId() == File.STATUS_IN_PROGRESS) {

						FilesDAOBase.updateFileRecords(conn, file.getId(), action.getActionId());
						IssuesManagerBase.InsertIssueRiskDocReq(action.getActionId(), ConstantsBase.RISK_DOC_REQ_REGULATION,0);
						fileTypeHashMap.remove(file.getFileTypeId());
			   }
			}
		 }
		
		 Transaction tran = TransactionsManagerBase.getTransaction(TransactionsManagerBase.getFirstTransactionId(userId));
	     if(null !=tran) {
		 if (tran.getTypeId() != 1) {
			 fileTypeHashMap.remove( FileType.CC_COPY_FRONT);
			 fileTypeHashMap.remove( FileType.CC_COPY_BACK);
		 }else {
			 tempCCId = tran.getCreditCardId().longValue();
		 } 
	  }
		 for (Long key :  fileTypeHashMap) {		 
			 
			 if(key == FileType.CC_COPY_FRONT || key == FileType.CC_COPY_BACK){
			 //FilesDAOBase.insertFile(conn,action.getActionId(), tempCCId, key, userId, File.STATUS_REQUESTED, null, action.getWriterId());
			 IssuesManagerBase.InsertIssueRiskDocReq(action.getActionId(), ConstantsBase.RISK_DOC_REQ_REGULATION,tempCCId);
			 }else {
			 //FilesDAOBase.insertFile(conn,action.getActionId(), 0, key, userId, File.STATUS_REQUESTED, null, action.getWriterId());
			 IssuesManagerBase.InsertIssueRiskDocReq(action.getActionId(), ConstantsBase.RISK_DOC_REQ_REGULATION,0);
			 }
		 }
		 
		 logger.info(" Successfully insert regulation document need issue for user id: " + userId);
	    }catch(Exception ex) {
	    	logger.error("Couldn't insert regulation document need issue for user id: " + userId, ex);
	    	throw ex;
	    }
	}
	
	public static void riskAlertDepositHandlerCCFromDiffCountry(Connection conn, Transaction tran){
		try {
			if(RiskAlertsDAOBase.isCCFromDiffCountries(conn, tran.getCreditCardId().longValue(), tran.getId())){
				insert(RISK_TYPE_DEPOSIT_CC_FROM_DIFFERENT_COUNTRIES, tran.getWriterId(), tran.getId(), NO_INVESTMENT_ID);
			}
		} catch (Exception e) {
			logger.error("When riskAlertDepositHandlerCCFromDiffCountry", e);
		}
	}
}