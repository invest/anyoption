package com.anyoption.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.beans.Limit;
import com.anyoption.daos.LimitsDAOBase;

/**
 * 
 * @author eyalo
 *
 */
public class LimitsManagerBase extends com.anyoption.common.managers.LimitsManagerBase {
	
	private static final Logger log = Logger.getLogger(LimitsManagerBase.class);
	private static HashMap<Long, Limit> limits;
	
	/**
	 * Takes limit directly from DB as limits are configurable from BE.
	 * 
	 * @param skinId
	 * @param currencyId
	 * @return
	 * @throws SQLException
	 */
	public static Limit getBySkinAndCurrency(long skinId,
												long currencyId) throws SQLException {
		Connection conn = null;
        try {
            conn = getConnection();
            return LimitsDAOBase.getBySkinAndCurrency(conn, skinId, currencyId);
        } finally {
            closeConnection(conn);
        }
	}
	
	/**
	 * @param id
	 * @return limit
	 */
	public static Limit getLimit(long id) {
		getLimits();
	    return limits.get(id);
	}
	
	/**
	 * @return limits as HashMap 
	 */
	public static HashMap<Long, Limit> getLimits() {
		if (null == limits) {
	        try {
	        	limits = LimitsManagerBase.getAll();
	        } catch (SQLException sqle) {
	            log.error("Can't load limits.", sqle);
	        }
	    }
		return limits;
	}
	
	/**
	 * @return limits as HashMap 
	 */
	public static HashMap<Long, Limit> getAll() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return LimitsDAOBase.getAll(conn);
        } finally {
            closeConnection(conn);
        }
    }
	
}