package com.anyoption.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import com.anyoption.common.beans.base.BankBranches;
import com.anyoption.common.bl_vos.BankBase;
import com.anyoption.common.daos.BanksDAOBase;
import com.anyoption.common.managers.BaseBLManager;

public class BanksManagerBase extends BaseBLManager {
    public static Hashtable<Long, BankBase> getBanks() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return BanksDAOBase.getBanks(conn);
        } finally {
            closeConnection(conn);
        }
    }
    
    public static HashMap<Long, BankBase> getBanksET(int bankType, long businessCase) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return BanksDAOBase.getBanksET(conn, bankType, businessCase);
        } finally {
            closeConnection(conn);
        }
    }
    
    public static ArrayList<BankBranches> getBanksBranches() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return BanksDAOBase.getBanksBranches(conn);
        } finally {
            closeConnection(conn);
        }
    }
}