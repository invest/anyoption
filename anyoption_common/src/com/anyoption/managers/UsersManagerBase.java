package com.anyoption.managers;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

import com.anyoption.beans.Contact;
import com.anyoption.beans.Limit;
import com.anyoption.beans.MarketingAffiliatePixels;
import com.anyoption.beans.MarketingCombination;
import com.anyoption.beans.MarketingCombinationPixels;
import com.anyoption.beans.User;
import com.anyoption.common.beans.Bonus;
import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.Login;
import com.anyoption.common.beans.Skin;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.CurrenciesRules;
import com.anyoption.common.beans.base.MailBoxTemplate;
import com.anyoption.common.beans.base.MailBoxUser;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.beans.base.Platform;
import com.anyoption.common.beans.base.Register;
import com.anyoption.common.beans.base.UserAnycapital;
import com.anyoption.common.beans.base.UserAnycapitalExtraFields;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.bl_vos.Template;
import com.anyoption.common.daos.CurrenciesRulesDAOBase;
import com.anyoption.common.daos.UserBalanceStepDAO;
import com.anyoption.common.daos.UsersRegulationDAOBase;
import com.anyoption.common.enums.DeviceFamily;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.managers.LoginManager;
import com.anyoption.common.managers.MailBoxUsersManagerBase;
import com.anyoption.common.managers.UserBalanceStepManager;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.sms.SMSException;
import com.anyoption.common.util.MessageToFormat;
import com.anyoption.daos.BonusDAOBase;
import com.anyoption.daos.ContactsDAOBase;
import com.anyoption.daos.LimitsDAOBase;
import com.anyoption.daos.MailBoxTemplatesDAOBase;
import com.anyoption.daos.MailBoxUsersDAOBase;
import com.anyoption.daos.MarketingCombinationsDAOBase;
import com.anyoption.daos.MarketingPixelsDAOBase;
import com.anyoption.daos.TemplatesDAOBase;
import com.anyoption.daos.UsersDAOBase;
import com.anyoption.daos.WritersDAOBase;
import com.anyoption.sms.SMS;
import com.anyoption.util.CommonUtil;
import com.anyoption.util.ConstantsBase;
import com.anyoption.util.SendTemplateEmail;
import com.copyop.common.dao.FrozenCopyopDao;
import com.copyop.common.dto.Frozen;


public class UsersManagerBase extends com.anyoption.common.managers.UsersManagerBase {
    private static final Logger log = Logger.getLogger(UsersManagerBase.class);

    protected static int TEMPLATE_CONFIRM_MAIL_ID = 2;
    protected static int TEMPLATE_PASSWORD_REMINDER_MAIL_ID = 3;
    protected static int TEMPLATE_PASSWORD_REMINDER_MAIL_CAL_ID = 21;
    protected static int TEMPLATE_WRONG_NUMBER_MAIL_USER = 22;
    protected static int TEMPLATE_WRONG_NUMBER_MAIL_CONTACT = 23;
    protected static int TEMPLATE_PASSWORD_REMINDER_MOBILE_MAIL_ID = 43;
    public static int TEMPLATE_DEPOSIT_DOCUMENT1 = 12;
    public static int TEMPLATE_DEPOSIT_DOCUMENT2 = 15;
    public static int TEMPLATE_DEPOSIT_DOCUMENT3 = 6;
    public static int TEMPLATE_DEPOSIT_DOCUMENT4 = 7;
    
	public static final int USER_RANK_NEWBIES = 1;
	public static final int USER_RANK_REGULARS = 2;
	public static final int USER_RANK_BEGGINER_ROLLERS = 3;
	public static final int USER_RANK_MEDUIM_ROLLERS = 4;
	public static final int USER_RANK_HIGH_ROLLERS = 5;
	public static final int USER_RANK_GOLD_ROLLERS = 6;
	public static final int USER_RANK_PLATINUM_ROLLERS = 7;
	public static final int STATUS_ID_SUCCESS = 4;
	public static final int STATUS_ID_ERROR_NOT_EXIST_EMAIL = 6;
	public static final int STATUS_ID_ERROR_MORE_ONE_EMAIL = 7;
	public static final int STATUS_ID_ERROR_NOT_MATCH_PHONE = 8;

    public static User getById(long userId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return getById(conn, userId);
        } finally {
            closeConnection(conn);
        }
    }

    public static User getById(Connection conn, long userId) throws SQLException {
        return UsersDAOBase.getById(conn, userId);

    }

//    public static User getUserByName(String name) throws SQLException {
//        return getUserByName(name, null);
//    }
    
    public static void getUserByName(String name, User user) throws SQLException {
    	if (user == null) {
    		throw new NullPointerException("The user parameter cannot be null");
    	}
        Connection conn = getConnection();
        try {
            UsersDAOBase.getByUserName(conn, name, user);
            if (user.getCurrencyId() != null &&  user.getCurrencyId() > 0) {
            	user.setCurrency(CurrenciesManagerBase.getCurrency(user.getCurrencyId()));
            }
        } finally {
            closeConnection(conn);
        }
    }

    public static Map<String, MessageToFormat> validateUpdateUserDetails(com.anyoption.common.beans.base.User user, Locale l) throws SQLException {
        Map<String, MessageToFormat> res = new HashMap<String, MessageToFormat>();
        if (user.getSkinId() == Skin.SKIN_ETRADER) {
            if (!CommonUtil.validateHebrewOnly(user.getCityName())) {
                log.debug("Failed to updateUser: Illegal city - not hebrew!");
                res.put("cityId", new MessageToFormat("error.city.nothebrew", null));
                return res;
            }
	} else {
	    if (user.getCityName() != null && user.getCityName().length() > 0) {
		if (!CommonUtil.validateLettersOnly(user.getCityName(), l)) {
		    log.debug("Failed to updateUser: Illegal city characters!");
		    res.put("cityId", new MessageToFormat("error.city.nothebrew", null));
		    return res;
		}
	    }

	    try {
		if (user.getTimeBirthDate() != null) {
		    Calendar cal = Calendar.getInstance();
		    cal.clear();
		    cal.setTime(user.getTimeBirthDate());
		    cal.add(Calendar.YEAR, 18);
		    Calendar curCal = Calendar.getInstance();
		    curCal.add(Calendar.MONTH, 1);
		    // if the current day is NOT after the 18th birth date, ergo can not make deposit since < 18
		    if (!(curCal).after(cal)) {
			log.debug("failed registerform: Illegal birth date!");
			res.put("", new MessageToFormat("error.register.birthdate", null,
				MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
		    }
		}
	    } catch (Exception e) {
		log.error("Error processing birth date.", e);
		res.put("birth", new MessageToFormat("error.register.birthdate", null,
			MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
	    }
	    if (user.getSkinId() == Skin.SKIN_GERMAN) {
		if ((null != user.getMobilePhone() && user.getMobilePhone().length() > 0)
			|| (null != user.getLandLinePhone() && user.getLandLinePhone().length() > 0)) {
		    if (user.isContactBySMS()) {
			if (null == user.getMobilePhone() || user.getMobilePhone().length() == 0) {
			    log.debug("failed registerform: no mobile phone and want contact by SMS");
			    res.put("mobilePhone", new MessageToFormat("error.mandatory", null));
			}
		    }
		} else {
		    if (user.isContactBySMS()) {
			log.debug("failed registerform: no mobile phone and want contact by SMS");
			res.put("mobilePhone", new MessageToFormat("error.mandatory", null));
		    } else {
			log.debug("failed registerform: no mobile phone or landing phone");
			res.put("landLinePhone", new MessageToFormat("error.mandatory", null));
		    }
		}
	    }
	    Connection con = getConnection();
	    try {
		// Just for anyoption - email is unique
		if (!CommonUtil.isHebrewSkin(user.getSkinId())) {
		    if (UsersDAOBase.isEmailInUse(con, user.getEmail(), user.getId())) {
			log.debug("failed registerform: email in use!");
			res.put("email", new MessageToFormat("error.register.email.inuse", null));
		    }
		}
	    } finally {
		closeConnection(con);
	    }
	}
        return res;
    }

    /**
     * Update user details
     *
     * @param user
     * @return
     * @throws SQLException
     */
    public static void updateUserDetails(com.anyoption.common.beans.base.User user) throws SQLException {
        if (log.isInfoEnabled()) {
            log.log(Level.DEBUG, "Update user details: " + user);
        }
        Connection con = getConnection();
        try {
            UsersDAOBase.updateDetails(con, user);
            log.debug("Updated successfully");
        } finally {
            closeConnection(con);
        }
    }

    public static Map<String, MessageToFormat> validateRegisterForm(Register form, boolean isAPIUser, long writerId) throws SQLException, CryptoException {
        Map<String, MessageToFormat> res = new HashMap<String, MessageToFormat>();
        // TODO when etrader mobile has reg funnel this check will be unnecessary
	if (form.getBirthYear() != null || form.getBirthMonth() != null || form.getBirthDay() != null) {
	    try {
		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(Integer.parseInt(form.getBirthYear()), Integer.parseInt(form.getBirthMonth()),
			Integer.parseInt(form.getBirthDay()));
		cal.add(Calendar.YEAR, 18);
		Calendar curCal = Calendar.getInstance();
		curCal.add(Calendar.MONTH, 1);
		// if the current day is NOT after the 18th birthdate, ergo can not make deposit since < 18
		if (!(curCal).after(cal)) {
		    log.debug("failed registerform: Illegal birth date!");
		    res.put("", new MessageToFormat("error.register.birthdate", null,
			    MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
		}
	    } catch (Exception e) {
		log.error("Error processing birth date.", e);
		res.put("birth", new MessageToFormat("error.register.birthdate", null,
			MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
	    }
	}

        if (!form.isTerms() && !isAPIUser) {
            log.debug("failed registerform: terms not activated!");
            res.put("terms", new MessageToFormat("error.register.terms", null, MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
        }

        if (form.getSkinId() == Skin.SKIN_GERMAN) {
            if ((null != form.getMobilePhone() && form.getMobilePhone().length() > 0)
            		|| (null != form.getLandLinePhone() && form.getLandLinePhone().length() > 0)) {
                if (form.isContactBySms()) {
                    if (null == form.getMobilePhone() || form.getMobilePhone().length() == 0) {
                        log.debug("failed registerform: no mobile phone and want contact by SMS");
                        res.put("mobilePhone", new MessageToFormat("error.mandatory", null));
                    }
                }
            } else {
                if (form.isContactBySms()) {
                    log.debug("failed registerform: no mobile phone and want contact by SMS");
                    res.put("mobilePhone", new MessageToFormat("error.mandatory", null));
                } else {
                    log.debug("failed registerform: no mobile phone or landing phone");
                    res.put("landLinePhone", new MessageToFormat("error.mandatory", null));
                }
            }
        }

        Connection con = getConnection();

        try {
	        // Just for anyoption - email is unique
	        if (!CommonUtil.isHebrewSkin(form.getSkinId())) {
				if (UsersDAOBase.isEmailInUse(con, form.getEmail(), 0)) {
					log.debug("failed registerform: email in use!");
					res.put("email", new MessageToFormat("error.register.email.inuse", null,
															MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
				}
	        }

	        Writer userWriter = new Writer();
            if (UsersDAOBase.isUserNameInUse(con, form.getUserName(), true, userWriter)) {
            	if ((writerId == Writer.WRITER_ID_COPYOP_WEB || writerId == Writer.WRITER_ID_COPYOP_MOBILE || writerId == Writer.WRITER_ID_CO_MINISITE)
						&& (userWriter.getId() != Writer.WRITER_ID_COPYOP_WEB && userWriter.getId() != Writer.WRITER_ID_COPYOP_MOBILE && userWriter.getId() != Writer.WRITER_ID_CO_MINISITE)) {
            		log.debug("failed registerform: user name in use probably by this user! Returning message accordingly");
            		res.clear();
					res.put("", new MessageToFormat("error.register.username.login.with.ao", null,
															MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
            	} else {
	                log.debug("failed registerform: user name in use!");
	                res.put("email", new MessageToFormat("error.register.email.inuse", null, MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
	                //overwrite email message, but actually isUserNameInUse is checking. Due to DEV-7005 [copyop web] Weird error message when entering wrong email
	                // hotfix
            	}
            }
        } finally {
            closeConnection(con);
        }

        // Just for eTrader skin numId validation
        if (form.getSkinId() == Skin.SKIN_ETRADER) {
        	if (form.getCityName() != null && !form.getCityName().isEmpty()) {
	            if (!CommonUtil.validateHebrewOnly(form.getCityName())) {
	                log.debug("failed registerform: Illegal city - not hebrew!");
	                res.put("cityId", new MessageToFormat("error.city.nothebrew", null));
	            } else if (form.getCityId() == 0) {
	                log.debug("failed registerform: Illegal city!");
	                res.put("cityId", new MessageToFormat("error.register.city", null));
	            }
        	}
        }

        return res;
    }

    public static User insertUser(Register f, long writerId ,boolean isBaidu, String osVersion, String deviceType, String appVersion, DeviceFamily deviceFamily, long fingerPrint) throws SQLException {
        User user = new User();
        Connection conn = null;
        try {
            conn = getConnection();
            user.setTimeFirstVisit(f.getTimeFirstVisit());
            Long combId = f.getCombinationId();
            if (combId == null || MarketingCombinationsDAOBase.getById(conn, combId) == null) {
                combId = SkinsManagerBase.getSkin(f.getSkinId()).getDefaultCombinationId();
            }
            user.setCombinationId(combId);
            user.setDynamicParam(f.getDynamicParam());
			if (null != f.getDynamicParam()) {
				long affId = CommonUtil.getAffIdFromDynamicParameter(user.getDynamicParam(), user.getCombinationId(), f.getSkinId());
				user.setAffiliateKey(affId);
			}
			user.setAffSub1(f.getAff_sub1());
			user.setAffSub2(f.getAff_sub2());
			user.setAffSub3(f.getAff_sub3());
            user.setIp(f.getIp());
            user.setBalance(0);
            user.setCityId(f.getCityId());
            user.setComments("");
            user.setEmail(f.getEmail());
            user.setFirstName(CommonUtil.capitalizeFirstLetters(f.getFirstName()));
            user.setGender(f.getGender());
            user.setIdNum(f.getIdNum());
            user.setLastName(CommonUtil.capitalizeFirstLetters(f.getLastName()));
            user.setIsActive("1");

            if (f.isContactByEmail()) {
                user.setIsContactByEmail(1);
            } else {
                user.setIsContactByEmail(0);
            }
            if (f.isContactBySms()) {
                user.setIsContactBySMS(1);
            } else {
                user.setIsContactBySMS(0);
            }

            if (f.getSkinId() == Skin.SKIN_ETRADER) {
                user.setLandLinePhoneET(null);
                user.setMobilePhone(f.getMobilePhonePrefix() + f.getMobilePhone());
                //TODO: bring city name from israel city list
                user.setCityName(f.getCityName());
            } else { // for anyoption
                user.setMobilePhone(f.handleLeadingZero(f.getMobilePhone()));
                user.setLandLinePhone(f.handleLeadingZero(f.getLandLinePhone()));
                user.setTaxExemption(true);
                user.setCityName(f.getCityNameAO());
            }

            user.setPassword(f.getPassword());
            user.setStreet(f.getStreet());
            user.setStreetNo(f.getStreetNo());
            user.setTaxBalance(0);
            user.setTimeLastLogin(new Date());
            user.setUserAgent(f.getUserAgent());
            user.setHttpReferer(f.getHttpReferer());

            // TODO when etrader mobile has reg funnel this check will be unnecessary
            if (f.getBirthYear() != null && f.getBirthMonth() != null && f.getBirthDay() != null) {
	            Calendar c = new GregorianCalendar(Integer.parseInt(f
	                    .getBirthYear()), Integer.parseInt(f.getBirthMonth()) - 1,
	                    Integer.parseInt(f.getBirthDay()), 0, 1);

	            user.setTimeBirthDate(new Date(c.getTimeInMillis()));
            }
            user.setUserName(f.getUserName());
            user.setZipCode(f.getZipCode());
            user.setClassId(UsersDAOBase.getDefaultClassId(conn));
            user.setCountryId(f.getCountryId());
            user.setLanguageId(f.getLanguageId());
            user.setIdDocVerify(false);

            user.setUtcOffsetCreated(f.getUtcOffsetCreated());
            user.setUtcOffsetModified(f.getUtcOffsetCreated());

            user.setState(f.getStateId());
            user.setAcceptedTerms(f.isTerms());
            user.setWriterId(writerId);

            //  for setting the skin by us country(if is choosen) in registration
            if (f.getCountryId() == ConstantsBase.COUNTRY_ID_US &&
                    f.getSkinId() != Skin.SKIN_EN_US &&
                    f.getSkinId() != Skin.SKIN_ES_US) {
                if (f.getSkinId() == Skin.SKIN_SPAIN) {
                    user.setSkinId(Skin.SKIN_ES_US);
                } else {
                    user.setSkinId(Skin.SKIN_EN_US);
                }
                user.setCurrencyId(ConstantsBase.CURRENCY_USD_ID);
            } else {
                user.setSkinId(f.getSkinId());
                // TODO when etrader mobile has reg funnel this check will be unnecessary
                if (f.getCurrencyId() > 0) {
                	user.setCurrencyId(f.getCurrencyId());
                } else {
                	if (f.getCountryId() == ConstantsBase.COUNTRY_ID_CHINA && f.getSkinId() == Skin.SKIN_CHINESE) {
                		user.setCurrencyId(ConstantsBase.CURRENCY_CNY_ID);
                	} else if( f.getCountryId() == ConstantsBase.COUNTRY_ID_SOUTH_KOREA && f.getSkinId() == Skin.SKIN_KOREAN) {
                		user.setCurrencyId(ConstantsBase.CURRENCY_KR_ID);
                	} else {                		
                		user.setCurrencyId(new Long(SkinsManagerBase.getSkin(f.getSkinId()).getDefaultCurrencyId()));
                	}
                }
            }

            if ((f.getSkinId() == Skin.SKIN_EN_US || f.getSkinId() == Skin.SKIN_ES_US) &&
            		f.getCountryId() != ConstantsBase.COUNTRY_ID_US) {
            	if (f.getSkinId() == Skin.SKIN_EN_US) {
            		user.setSkinId(Skin.SKIN_ENGLISH);
            	} else if (f.getSkinId() == Skin.SKIN_ES_US) {
            		user.setSkinId(Skin.SKIN_SPAIN);
            	}
            }

            user.setDeviceUniqueId(f.getDeviceUniqueId());
            user.setDeviceFamily(deviceFamily.getId());
            CurrenciesRules currenciesRules = new CurrenciesRules();
            currenciesRules.setCountryId(f.getCountryId());
            currenciesRules.setSkinId(user.getSkinId());
            long defaultCurrencyId = CurrenciesRulesDAOBase.getDefaultCurrencyId(conn, currenciesRules);
            user.setCurrencyId(defaultCurrencyId);
            log.info("CurrenciesRules [countryId=" + currenciesRules.getCountryId() + ", skinId=" + currenciesRules.getSkinId() + "]. Default currency id by the table CurrenciesRules=" + defaultCurrencyId);
            user.setLimitId(LimitsDAOBase.getIdBySkinAndCurrency(conn, user.getSkinId(), defaultCurrencyId));

            if (CommonUtil.isHebrewSkin(f.getSkinId())) {
		    	user.setAuthorizedMail(false);
		    } else {
		    	user.setAuthorizedMail(true);
		    }
                    
            user.setPlatformId(f.getPlatformId());
            
            UsersManagerBase.insert(conn, user, f.getContactId());
            //TODO: insert first combid logic - insertFirstVisitRecord

            if (user.getContactId() > 0) {
                try {
                    ContactsManagerBase.updateContactRequest(user.getContactId(), user.getId());
                    PopulationsManagerBase.updateContactPopulation(user.getContactId(), user.getId(), Writer.WRITER_ID_WEB);
                } catch (Exception e) {
                    log.warn("Problem with population removeUser event " + e);
                }
            }

            try {            	
            	if (isBaidu) {
            		String authLink =  CommonUtil.getConfig("login.auth.link") + "&userName=" + user.getUserName();
                 	UsersManagerBase.sendMailTemplateFile(ConstantsBase.TEMPLATE_WELCOME_BAIDU_MAIL_ID, writerId, user, false,
                 			null, null, null, null, null, authLink);
            	} else {
            		sendMailTemplateFile(ConstantsBase.TEMPLATE_WELCOME_MAIL_ID, writerId, user, null, null, null, null);
            	}
            } catch (Exception e) {
                log.error("can't send registration mail!! ", e);
            }

            // next invest bonus
            if (f.getSkinId() == Skin.SKIN_ETRADER) {
                try {
                    if (BonusDAOBase.isBonusUponRegistrationAvailable(conn, Bonus.NEXT_INVEST_REGISTRATION)) {
                        BonusUsers bonusUser = new BonusUsers();
                        bonusUser.setBonusId(Bonus.NEXT_INVEST_REGISTRATION);
                        bonusUser.setUserId(user.getId());
                        bonusUser.setWriterId(writerId);

                        //TODO: if BE need to consider sending popEntryId & bonusPopLimitTypeId
                        BonusManagerBase.insertBonusUser(conn, bonusUser, user, writerId, 0, 0, user.getIp());
                        user.setHaveRegistrationBonus(true);
                    }
                } catch (Exception e) {
                    log.error("can't insert bonus upon registration! ", e);
                }
            }

            if (user.getId() != 0 && f.getRegisterAttemptId() != 0) {
            	Contact regAttemp = ContactsManagerBase.getContactById(f.getRegisterAttemptId());
            	regAttemp.setUserId(user.getId());
            	regAttemp.setContactByEmail(f.isContactByEmail());
            	regAttemp.setContactBySMS(f.isContactBySms());
            	regAttemp.setFirstName(f.getFirstName());
            	regAttemp.setLastName(f.getLastName());
            	regAttemp.setEmail(f.getEmail());
            	regAttemp.setLandLinePhone(f.getLandLinePhone());
            	regAttemp.setCountryId(f.getCountryId());
            	regAttemp.setSkinId(f.getSkinId());
            	ContactsManagerBase.updateContact(regAttemp);
            }

            // Login user when registration is finished
            LoginManager.loginActionFromWeb(user, user.getPassword());
            UserBalanceStepManager.loadUserBalanceStep(user,false, writerId);
            
            if (log.isDebugEnabled()) {
                log.debug("Insert user successfully: " + user);
            }

            Login login = new Login();
			login.setLoginOffset(user.getUtcOffsetCreated());
			login.setSuccess(true);
			login.setUserAgent(null);
			login.setDeviceUniqueId(user.getDeviceUniqueId());
			login.setLoginFrom(ConstantsBase.LOGIN_FROM_REGULAR);
			login.setWriterId(user.getWriterId());
			login.setServerId(CommonUtil.getServerName().split(":")[1]);
			login.setFingerPrint(fingerPrint);
			login.setOsVersion(osVersion);
			login.setDeviceType(deviceType);
			login.setAppVersion(appVersion);
			login.setDeviceFamilyId(deviceFamily.getId());
			login.setCombinationId(combId);
			login.setDynamicParam(f.getDynamicParam());
			login.setAffSub1(f.getAff_sub1());
			login.setAffSub2(f.getAff_sub2());
			login.setAffSub3(f.getAff_sub3());
            
            long userLastLoginId = LoginsManager.insertLogin(user.getUserName(), login);
            user.setLastLoginId(userLastLoginId);
            if (deviceFamily!=DeviceFamily.NONMOBILE && user.getDeviceUniqueId() != null && !user.getDeviceUniqueId().isEmpty() && user.getDeviceUniqueId().equals(""+com.anyoption.common.beans.Writer.WRITER_ID_COPYOP_MOBILE)) {
            	UsersManagerBase.updateUserLastLoginId(user.getId(), userLastLoginId);
            }
            /* 
			 * The skin id in the session/cookie may not be the same as the
			 * user's, so we should check the user skin.
			 */
			Skin userSkin = SkinsManagerBase.getSkin(user.getSkinId());
			if (userSkin.isRegulated()) {
				// WC
				UserRegulationBase ur = new UserRegulationBase();
				ur.setUserId(user.getId());
				UserRegulationManager.getUserRegulation(ur);
				// HARD CODED
				ur.setWcApproval(true);
				ur.setApprovedRegulationStep(UserRegulationBase.REGULATION_WC_APPROVAL);
				ur.setWcComment("HARDCODED WC APPROVAL");
				ur.setWriterId(user.getWriterId());
				UserRegulationManager.updateWCApproval(ur);
				// suspend account due WC fail
				if (!ur.isWcApproval()) {
					ur.setSuspendedReasonId(UserRegulationBase.SUSPENDED_BY_WORLD_CHECK);
					ur.setComments("World check");
					UserRegulationManager.updateSuspended(ur);
				}
			}
        } finally {
            closeConnection(conn);
        }
        return user;
    }

    /**
     * Insert new user.
     * @param connection
     * @param user
     * @throws SQLException
     */
    public static void insert(Connection con, User user, long contactId) throws SQLException {
        if (0 == contactId) { // contact cookie not found
        	log.debug("Begin search Free Contact");
            Contact contact = ContactsDAOBase.searchFreeContactByUserPhone(con, user);
            if (null != contact) {
                contactId = contact.getId();
                user.setCombinationId(contact.getCombId());
                user.setDynamicParam(contact.getDynamicParameter());
                user.setAffSub1(contact.getAff_sub1());
                user.setAffSub2(contact.getAff_sub2());
                user.setAffSub3(contact.getAff_sub3());
                if (null != user.getDynamicParam()) {
    				long affId = CommonUtil.getAffIdFromDynamicParameter(user.getDynamicParam(), user.getCombinationId(), user.getSkinId());
    				user.setAffiliateKey(affId);
    			}
            }
            log.debug("End search Free Contact");
        }
        user.setContactId(contactId);
        
        try {
        	con.setAutoCommit(false);
	        UsersDAOBase.insert(con, user);
	        /*
			 * Make sure user is not inserted in DB if not marked as regulated.
			 * 
			 * The skin id in the session/cookie may not be the same as the
			 * user's, so we should check the user skin.
			 */
	    	// Regulation check
	    	Skin userSkin = SkinsManagerBase.getSkin(user.getSkinId());
			if (userSkin.isRegulated() || user.getSkinId() == Skin.SKIN_ETRADER) {
				// insert users
				UserRegulationBase ur = new UserRegulationBase();
				ur.setUserId(user.getId());
				ur.setWriterId(user.getWriterId());
				ur.setApprovedRegulationStep(UserRegulationBase.REGULATION_USER_REGISTERED);
				ur.setSuspendedReasonId(UserRegulationBase.NOT_SUSPENDED);
				UsersRegulationDAOBase.insertUserRegulation(con, ur);
			}
			
			//Add copyop frozen when user is created
			try { 
				Frozen fr = new Frozen(user.getId(), true, Frozen.FROZEN_REASON_FRESH, user.getWriterId());
				FrozenCopyopDao.insert(con, fr);
			} catch (Exception e) {
				log.error("Can't insert frozen user", e);
			}
			
			
			
	        con.commit();
        } catch (SQLException e) {
			if (!con.getAutoCommit()) {
				log.error("Rolling back user insert due to exception", e);
				con.rollback();
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
		}
        
        try {
        	log.info("Trying to insert auto bonus upon registration for user " + user.getId());
	    	// Insert Automatic Bonus.
	    	long autoBonusId = BonusDAOBase.getAutoBonusId(con, user.getId());

	    	if (autoBonusId > 0){
	    		BonusUsers bonusUser = new BonusUsers();
	    		bonusUser.setUserId(user.getId());
	    		bonusUser.setWriterId(user.getWriterId());
	    		bonusUser.setBonusId(autoBonusId);

	    		BonusManagerBase.insertBonusUser(con, bonusUser, user, user.getWriterId(), 0, 0, user.getIp());
	    	}
    	} catch (Exception e) {
			log.error("Failed to insert auto bonus upon registration for user " + user.getId(),e);
		}
    }

	public static void sendMailTemplateFile(long templateId, long writerId, User u) throws Exception{
    	sendMailTemplateFile(templateId, writerId, u, null, null, null, null);
	}

	public static void sendMailTemplateFile(long templateId, long writerId, User u, String amount, String footer, String receiptNum, HashMap<String,String> params) throws Exception{
    	sendMailTemplateFile(templateId, writerId, u, false, null, amount, null, footer, receiptNum, params);
	}

    public static void sendMailTemplateFile(long templateId, long writerId, User u,
            boolean calcalistGame, String transactionPan, String transactionAmonut, String transactionBankName, String footer, String receiptNum, HashMap<String,String> params) throws Exception {
        Template t = null;
        Connection conn = getConnection();
        try {
            t = TemplatesDAOBase.getById(conn, templateId);
        } finally {
            closeConnection(conn);
        }
        sendMailTemplateFile(t, writerId, u, calcalistGame, transactionPan, transactionAmonut, transactionBankName, footer, receiptNum, null, null, null, params);
    }

    public static void sendMailTemplateFile(
            Template t,
            long writerId,
            User u,
            boolean calcalistGame,
            String transactionPan,
            String transactionAmonut,
            String transactionBankName,
            String footer,
            String receiptNum,
            String authLink,
            String[] emailsTo,
            String emailAuthMail,
            HashMap<String,String> params) throws Exception {
    	
    	String bankTransferDetails = "";
		Writer w = null;
        long attachmentId = 0;
		Connection conn = getConnection();
		try {
		    w = WritersDAOBase.get(conn, writerId);
		    UsersDAOBase.getByUserName(conn, u.getUserName(), u);
		} finally {
		    closeConnection(conn);
		}

        long langId = SkinsManagerBase.getSkin(u.getSkinId()).getDefaultLanguageId();
        String langCode = LanguagesManagerBase.getLanguage(langId).getCode();
        Locale locale = new Locale(langCode);

        String subject = CommonUtil.getMessage(locale, t.getSubject(), null);

        // handle phone
        String phone = null;
        String supportPhone = "";
        String supportFax = "";
        if (u.getSkinId() == Skin.SKIN_ETRADER) {
            phone = u.getMobilePhonePrefix() + "-" + u.getMobilePhoneSuffix();
        } else { // anyoption
            Country c = CountryManagerBase.getCountry(u.getCountryId());
            phone = c.getPhoneCode() + "-" + u.getMobilePhone();
            supportPhone = c.getSupportPhone();
            supportFax = c.getSupportFax();
        }


        if (null == params) {
        	params = new HashMap<String,String>();
        }
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_USER_ID, String.valueOf(u.getId()));
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_USER_FIRST_NAME, CommonUtil.capitalizeFirstLetters(u.getFirstName()));
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_USER_LAST_NAME, u.getLastName());
        if (!calcalistGame) {
            CommonUtil.putParam(params, SendTemplateEmail.PARAM_USER_NAME, CommonUtil.capitalizeFirstLetters(u.getUserName()));
        } else {
            CommonUtil.putParam(params, SendTemplateEmail.PARAM_USER_NAME, u.getUserNameCalGame());
        }
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_PASSWORD, u.getPassword());
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_ACCOUNT_ID, String.valueOf(u.getId()));
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_USER_ADDRESS,  u.getStreet() + " " + u.getStreetNo() + " " + u.getCityName() + " " + u.getZipCode());
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_USER_ADDRESS_STREET,  u.getStreet());
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_USER_ADDRESS_NUMBER,  u.getStreetNo());
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_USER_ADDRESS_CITY,  u.getCityName());
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_USER_ADDRESS_ZIP_CODE,  u.getZipCode());
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_WRITER_FIRST_NAME,  w.getFirstName());
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_WRITER_LAST_NAME,  w.getLastName());
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_WRITER_FULL_NAME,  w.getFirstName() + " " + w.getLastName());
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_USER_BALANCE,  u.getBalanceTxt());
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_USER_TAX_BALANCE,  u.getTaxBalanceTxt());
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_MOBILE_PHONE, phone);
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_SUPPORT_PHONE, supportPhone);
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_SUPPORT_FAX, supportFax);
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_TRANSACTION_AMOUNT, String.valueOf(transactionAmonut));
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_TRANSACTION_PAN, transactionPan);
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_TRANSACTION_BANK_NAME, transactionBankName);

        Date date = new Date();
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_DATE_DMMYYYY, new SimpleDateFormat("dd.MM.yy").format(date));
        CommonUtil.putParam(params, SendTemplateEmail.PARAM_DATE_MMMMMDYYYY, new SimpleDateFormat("dd/MM/yyyy").format(date));


        // for reciept after deposit template

        CommonUtil.putParam(params, SendTemplateEmail.PARAM_AMOUNT, transactionAmonut);
		CommonUtil.putParam(params, SendTemplateEmail.PARAM_DATE, CommonUtil.getTimeAndDateFormat(date, u.getUtcOffset()));
		CommonUtil.putParam(params, SendTemplateEmail.PARAM_FOOTER, footer);
		CommonUtil.putParam(params, SendTemplateEmail.PARAM_RECEIPT_NUM, receiptNum);

		// for activation after suspend email
		CommonUtil.putParam(params, SendTemplateEmail.PARAM_AMOUNT, transactionAmonut);
		
        // load gender param for Backend use (english / hebrew)
        if (writerId != Writer.WRITER_ID_WEB && null != u.getGender()) {
            String genderTxt = u.getGenderForTemplateEnHeUse();
            CommonUtil.putParam(params, SendTemplateEmail.PARAM_GENDER_DESC_EN_OR_HE, CommonUtil.getMessage(locale, genderTxt, null));
        }

        // load genderDesc param for all skins
       if (u.getGender() != null) {                 
            String genderDes = u.getGenderForTemplateUse();
            String genderMsg = CommonUtil.getMessage(locale, genderDes, null);
            CommonUtil.putParam(params, SendTemplateEmail.PARAM_GENDER_DESC, genderMsg);
    		CommonUtil.putParam(params, SendTemplateEmail.PARAM_AUTHENTICATION_LINK, authLink);
       }

        // send email for tracking if needed
        if (null != emailsTo && emailsTo.length > 0) {
            for (int i = 0; i < emailsTo.length; i++) {
                CommonUtil.putParam(params, SendTemplateEmail.PARAM_EMAIL, emailsTo[i]);
                if (t.getId() == ConstantsBase.TEMPLATE_WELCOME_MAIL_ID && u.getPlatformId() == Platform.ANYOPTION) {
            		t.setFileName("Welcome_Mail_Attachment.html");
            	}
                new SendTemplateEmail(params, subject, t.getFileName(), langCode,
                		u.getSkinId(), null, u.getWriterId(), u.getPlatformId()).start();
            }
        }
         boolean isUser = false; // If false, is contact.
		if (u.getId() != 0) {
			isUser = true;
		}
		int currencyForTemplate;
		if (isUser == true) {	// if user, get user's currency and send related bank details 
			currencyForTemplate = u.getCurrencyId().intValue();
			if (SkinsManagerBase.getSkin(u.getSkinId()).isRegulated()){
				if (currencyForTemplate == ConstantsBase.CURRENCY_EUR_ID) {
					bankTransferDetails = CommonUtil.getMessage("bank.transfer.details.boxC", null, u.getLocale());
				} else if (currencyForTemplate == ConstantsBase.CURRENCY_USD_ID || currencyForTemplate == ConstantsBase.CURRENCY_GBP_ID) {
					bankTransferDetails = CommonUtil.getMessage("bank.transfer.details.boxE", null, u.getLocale());
				}
			} else {
				switch (currencyForTemplate) {
				case (int) ConstantsBase.CURRENCY_EUR_ID:
					bankTransferDetails = CommonUtil.getMessage("bank.transfer.details.boxB", null, u.getLocale());
					break;
				case (int) ConstantsBase.CURRENCY_USD_ID:
				case (int) ConstantsBase.CURRENCY_GBP_ID:
				case (int) ConstantsBase.CURRENCY_TRY_ID:
				case (int) ConstantsBase.CURRENCY_CNY_ID:
				case (int) ConstantsBase.CURRENCY_RUB_ID:
				case (int) ConstantsBase.CURRENCY_KR_ID:
						bankTransferDetails = CommonUtil.getMessage("bank.transfer.details.boxA", null, u.getLocale());
						break;
				}
			}
		} else {
			long skin = u.getSkinId();
			if (skin == Skin.SKIN_CHINESE || skin == Skin.SKIN_KOREAN || skin == Skin.SKIN_RUSSIAN || skin == Skin.SKIN_TURKISH){		//sending all available bank options for ZH, KR, RU and TR skins
    			bankTransferDetails = CommonUtil.getMessage("bank.transfer.details.boxD", null, u.getLocale());
			}
		}
		params.put(SendTemplateEmail.PARAM_BANK_TRANSFER_DETAILS, bankTransferDetails);
        
        if(CommonUtil.IsParameterEmptyOrNull(emailAuthMail)){
        	CommonUtil.putParam(params, SendTemplateEmail.PARAM_EMAIL, u.getEmail());
        } else {
        	CommonUtil.putParam(params, SendTemplateEmail.PARAM_EMAIL, emailAuthMail);
        }        
        
        if(t.getId() == ConstantsBase.TEMPLATE_REGULATION_PEP){
			String fileName = "PEP_Application.pdf";
			String templatePath = CommonUtil.getProperty("templates.path") + LanguagesManagerBase.getLanguage(u.getLanguageId()).getCode() + "_" + u.getSkinId() + "/" + fileName;
        	File attachedFile = new File(templatePath);
        	File []attachments={attachedFile};
        	attachmentId = MailBoxUsersManagerBase.insertAttachmentAsBLOBWithUserIdinName(attachments[0], u.getId());
            new SendTemplateEmail(params, subject, t.getFileName(), langCode, 
            		u.getSkinId(), attachments, writerId, u.getPlatformId()).start();
        } else{
        	if (t.getId() == ConstantsBase.TEMPLATE_WELCOME_MAIL_ID  && u.getPlatformId() == Platform.ANYOPTION) {
        		t.setFileName("Welcome_Mail_Attachment.html");
        	}
            new SendTemplateEmail(params, subject, t.getFileName(), langCode, 
            		u.getSkinId(), null, u.getWriterId(), u.getPlatformId()).start();
        }                
        String activationLink = params.get(SendTemplateEmail.PARAM_ACTIVATION_LINK);
        if (activationLink!=null && !activationLink.isEmpty()) {
        	HashMap<String,String> paramsToSend = new HashMap<String,String>();
        	CommonUtil.putParam(paramsToSend, SendTemplateEmail.PARAM_ACTIVATION_LINK, activationLink);
        	SendToMailBox(u, t, writerId, langId, null, attachmentId, paramsToSend);
        } else {
        	SendToMailBox(u, t, writerId, langId, null, attachmentId, null);
        }
    }



	public static void SendToMailBox(User u, Template t, long writerId, long langId, Long trxId, long attachmentId, HashMap<String, String> params) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			SendToMailBox(u, t, writerId, langId, trxId, conn, attachmentId, params);
		} finally {
            closeConnection(conn);
        }
	}

	public static void sendMailTemplateFile(long templateId,long writerId,User u,
			boolean calcalistGame, String transactionPan, String transactionAmonut, String transactionBankName, String resetLink, Long trxId, String authLink) throws Exception {
		Template t = null;
    	Connection con = getConnection();
		try {
            t = TemplatesDAOBase.getById(con,templateId);
            sendMailTemplateFile(t, writerId, u, calcalistGame, transactionPan, transactionAmonut, transactionBankName, null, null, authLink, null, u.getEmail(), null);
		} finally {
            closeConnection(con);
        }
	}

	public static void SendToMailBox(User u, Template t, long writerId, long langId, Long trxId, Connection conn, long attachmentId, HashMap<String, String> params) throws SQLException {
		SendToMailBox(u, t, writerId, langId, trxId, params, conn, attachmentId);
	}

	/**
	 * add email to MailBox user
	 * @param u user instance
	 * @param t template instance
	 * @param writerId
	 * @param langId
	 * @param trxId transaction id, if needed
	 * @throws SQLException
	 */
	public static void SendToMailBox(User u, Template t, long writerId, long langId, Long trxId,
			HashMap<String, String> params, Connection conn, long attachmentId) throws SQLException {
 		// 	Send email to the user mailBox
 		if (!CommonUtil.isHebrewSkin(u.getSkinId())) {
 			if (t.getMailBoxEmailType() != 0) {
				MailBoxTemplate template = MailBoxTemplatesDAOBase.getTemplateByTypeSkinLanguage(conn, t.getMailBoxEmailType() , u.getSkinId(), langId);
				if (null != template) {
					log.debug("going to send email to user mailBox. templateId: " + template.getId() + " userId: " + u.getId());
					MailBoxUser email = new MailBoxUser();
					email.setTemplateId(template.getId());
					email.setUserId(u.getId());
					email.setWriterId(writerId);
					email.setSenderId(template.getSenderId());
					email.setIsHighPriority(template.getIsHighPriority());
					email.setPopupTypeId(template.getPopupTypeId());
					email.setSubject(template.getSubject());
					if (attachmentId != 0) {
						email.setAttachmentId(attachmentId);
					}
					if (null != trxId) {
						email.setTransactionId(trxId);
					}
					if (params != null) {
						email.setParameters(params);
					}
					MailBoxUsersDAOBase.insert(conn, email);
				}
 			}
		}
	}

    /**
     * Send user password
     * @param userName
     * @param email
     * @param skinId
     * @return
     * @throws Exception
     */
	public static Map<String, MessageToFormat> sendPasswordValidation(String userName, String email, long skinId) throws Exception {
		Map<String, MessageToFormat> res = new HashMap<String, MessageToFormat>();
		Connection con = null;
		try {
			userName = userName.toUpperCase();
			email = email.toUpperCase();

			HashMap map = null;
			String realEmail = "";
			con = getConnection();
			map = UsersDAOBase.getPasswordDetails(con, userName, skinId);

			if (map != null) {
				realEmail = ((String) map.get("email")).toUpperCase();
			}
			if (map == null || !realEmail.equals(email)) {
				res.put("email", new MessageToFormat("error.password.illegal", null));
			}
		} finally {
			closeConnection(con);
		}
		return res;
	}

	public static void sendPassword(String userName, String email, long skinId, int writerId) throws Exception {
		try {
			User user = new User();
			getUserByName(userName, user);
			sendMailTemplateFile(ConstantsBase.TEMPLATE_PASSWORD_REMINDER_MAIL_ID, writerId, user);
		} catch (Exception e) {
			log.error("can't send password reminder mail!! ", e);
		}
	}


	/**
	 * Change user password
	 * @param user
	 * @param oldPass
	 * @param newPass
	 * @return
	 * @throws SQLException
	 */
	public static Map<String, MessageToFormat> changePassValidation(User user, String oldPass, String newPass)	throws SQLException {
		Map<String, MessageToFormat> res = new HashMap<String, MessageToFormat>();
		if (log.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.DEBUG, "Changing user password.  " + ls
					+ "User:" + user.getUserName() + ls
					+ "new password:" + newPass + ls);
		}
		if (user.getSkinId() == Skin.SKIN_ETRADER) {
			if (!(user.getPassword().toUpperCase()).equals(oldPass.toUpperCase())) {
				res.put("password", new MessageToFormat("error.changePass.wrongPass", null));
				return res;
			}
		} else {
			if (!(user.getPassword().equals(oldPass))) { // in AO password is case-sensative
				res.put("password", new MessageToFormat("error.changePass.wrongPass", null));
				return res;
			}
		}
		return res;
	}

	public static void changePass(User user, String oldPass, String newPass, long writerId) throws SQLException {
		Connection con = getConnection();
		try {
			user.setPassword(newPass);
			UsersDAOBase.updateUser(con, user);
			log.debug(" password updated successfully! ");
			try {
				sendMailTemplateFile(ConstantsBase.TEMPLATE_CONFIRM_MAIL_ID, writerId, user);
			} catch (Exception e2) {
				log.error("can't send password confirm mail!! ", e2);
			}
		} finally {
			closeConnection(con);
		}
	}
	
	public static boolean sendActivationMailForSuspend(long userId, boolean isUserRequest, long skinId) throws SQLException {
		Connection con = getConnection();
		HashMap<String,String> params = new HashMap<String,String>();
		try {
			User user = UsersDAOBase.getById(con, userId);
			UserRegulationBase userRegulation = new UserRegulationBase();
            userRegulation.setUserId(user.getId());
            UserRegulationManager.getUserRegulation(userRegulation);
            int suspendReason = userRegulation.getSuspendedReasonId();
            if (suspendReason == 0) {
            	log.debug(" sendActivationMailForSuspend : user is not suspended! ");
            	return false;
            } else {
            	///String activationLink = CommonUtil.getConfig("homepage.url.etrader");
            	String activationLink = null;
            	long writerId = user.getWriterId();
            	long treshold = userRegulation.getTresholdLimit();
            	String tresholdTxt = "";
            	tresholdTxt = CommonUtil.formatCurrencyAmountAO(treshold, user.getCurrencyId());
            	if (suspendReason == UserRegulationBase.ET_SUSPEND_LOW_SCORE_GROUP) {
            		try {
            			activationLink = CommonUtil.generateEtActivationLink(userId, suspendReason, treshold, isUserRequest, skinId);
            			CommonUtil.putParam(params, SendTemplateEmail.PARAM_ACTIVATION_LINK, activationLink);
            			sendMailTemplateFile(ConstantsBase.TEMPLATE_ACTIVATION_MAIL_LOW_SCORE, writerId, user, null, null, null, params);
            			log.debug("Sending low score group activation email with activation link : " + activationLink);
            			return true;
            		} catch (Exception e) {
            			log.error("Cannot send low score group activation email!! ", e);
            			return false;
            		}
            	} else if (suspendReason == UserRegulationBase.ET_SUSPEND_TRESHEHOLD_LOW_SCORE_GROUP) {
            		try {
            			activationLink = CommonUtil.generateEtActivationLink(userId, suspendReason, treshold, isUserRequest, skinId);
            			CommonUtil.putParam(params, SendTemplateEmail.PARAM_ACTIVATION_LINK, activationLink);
            			CommonUtil.putParam(params, SendTemplateEmail.PARAM_LOSS_TRESHOLD, tresholdTxt);
            			sendMailTemplateFile(ConstantsBase.TEMPLATE_ACTIVATION_MAIL_TRESHOLD, writerId, user, null, null, null, params);
            			log.debug("Sending treshold activation email with activation link : " + activationLink + "and treshold: " + tresholdTxt);
            			return true;
            		} catch (Exception e) {
            			log.error("Cannot send low score low treshold group activation email!! ", e);
            			return false;
            		}
            	} else if (suspendReason == UserRegulationBase.ET_SUSPEND_TRESHEHOLD_MEDIUM_SCORE_GROUP) {
            		try {
            			activationLink = CommonUtil.generateEtActivationLink(userId, suspendReason, treshold, isUserRequest, skinId);
            			CommonUtil.putParam(params, SendTemplateEmail.PARAM_ACTIVATION_LINK, activationLink);
            			CommonUtil.putParam(params, SendTemplateEmail.PARAM_LOSS_TRESHOLD, tresholdTxt);
            			sendMailTemplateFile(ConstantsBase.TEMPLATE_ACTIVATION_MAIL_TRESHOLD, writerId, user, null, null, null, params);
            			log.debug("Sending treshold activation email with activation link : " + activationLink + "and treshold: " + tresholdTxt);
            			return true;
            		} catch (Exception e) {
            			log.error("Cannot send high treshold group activation email!! ", e);
            			return false;
            		}
            	}
            }
            } catch (SQLException sqle) {
            	log.error("Cannot get user from user ID!!! ", sqle);
            	return false;
            } finally {
            	closeConnection(con);
            }
            return false;
	}

	public static boolean sendMailAfterQuestAORegulation(UserRegulationBase ur) throws SQLException {
		Connection con = getConnection();
		HashMap<String,String> params = new HashMap<String,String>();
		try {
			User user = UsersDAOBase.getById(con, ur.getUserId());
			UserRegulationManager.getUserRegulation(ur);
			long writerId = user.getWriterId();
            //String activationLink = CommonUtil.getConfig("homepage.url");
			String activationLink = null;
			log.info("Trying to send email for blocked account/required documents: " + ur);
            if (ur.isRestrictedGroup()) {
            	// send email for restricted users
            	try{
            		if(ur.getScoreGroup() == UserRegulationBase.AO_REGULATION_RESTRICTED_FAILED_Q_SCORE_GROUP_ID){
            			activationLink = CommonUtil.generateAOActivationLinkAfterFaildQuest(user.getId(), user.getSkinId());
            		} else {
            			activationLink = CommonUtil.generateAOActivationLinkAndInsertLinkIssue(user.getId(), user.getSkinId());
            		} 
        			CommonUtil.putParam(params, SendTemplateEmail.PARAM_ACTIVATION_LINK, activationLink);
        			sendMailTemplateFile(ConstantsBase.TEMPLATE_ACTIVATION_MAIL_RESTRICTED, writerId, user, null, null, null, params);
        			log.debug("Sending activation email with activation link for restricted user: " + activationLink);
        			return true;
            	} catch(Exception ex){
            		log.error("Cannot send activation email with activation link for restricted user!! ", ex);
            		return false;
            	}
            }
            } catch (SQLException sqle) {
            	log.error("Cannot get user from user ID!!! ", sqle);
            	return false;
            } finally {
            	closeConnection(con);
            }
            return false;
	}

    public static long getUserIdByPhoneEmail(String phone, String email, long countryId) throws SQLException {
    	Connection con = getConnection();
    	try {
        	return UsersDAOBase.getUserByPhoneEmail(con, phone, email, countryId);
        } finally {
            closeConnection(con);
        }
    }

    /**
     * Get limit by id
     * @param id
     * 		limit id
     * @return
     * 		Limit instance
     * @throws SQLException
     */
    public static Limit getLimitById(long id) throws SQLException {
    	Limit l = new Limit();
    	Connection con = getConnection();
        try {
        	l = LimitsDAOBase.getById(con, id);
        } finally {
            closeConnection(con);
        }
        return l;
    }

    public static Map<String, MessageToFormat> sendResetPassword(String email, String mobilePhone, String mobilePhonePrefix, boolean sendMail, boolean sendSms, long skinId) throws Exception {
    	Map<String, MessageToFormat> res = new HashMap<String, MessageToFormat>();

		Connection con = getConnection();
		try {
			email = email.toUpperCase();

			ArrayList<User> usrArr = UsersDAOBase.getPasswordDetails(con, email);
			
//			String realMobilePhoneWithPrefix = "";
//
//			if (usrArr.size() == 0) {
//				res.put("", new MessageToFormat("error.reset.password.not.exist.email", null));
//				insertPasswordRecoveryHistory(ConstantsBase.WRITER_MOBILE_ID, email, mobilePhonePrefix + mobilePhone, STATUS_ID_ERROR_NOT_EXIST_EMAIL);
//				return res;
//			}
//			
//	         if (usrArr.size() > 1) {
//	                res.put("", new MessageToFormat("error.reset.password.more.one.email", null));
//	                insertPasswordRecoveryHistory(ConstantsBase.WRITER_MOBILE_ID, email, mobilePhonePrefix + mobilePhone, STATUS_ID_ERROR_MORE_ONE_EMAIL);
//	                return res;
//	            }
//			
//			realMobilePhoneWithPrefix = usrArr.get(0).getMobilePhonePrefix() + usrArr.get(0).getMobilePhone();
//            if (!realMobilePhoneWithPrefix.equals(mobilePhonePrefix + mobilePhone)) {
//	            	res.put("", new MessageToFormat("error.reset.password.mobile.not.match.phone", null));
//	            	insertPasswordRecoveryHistory(ConstantsBase.WRITER_MOBILE_ID, email, mobilePhonePrefix + mobilePhone, STATUS_ID_ERROR_NOT_MATCH_PHONE);
//					return res;
//	           }
            
            if (sendMail){
                try {
                    sendMailTemplateFile(TEMPLATE_PASSWORD_REMINDER_MOBILE_MAIL_ID, Writer.WRITER_ID_MOBILE, usrArr.get(0));
                    insertPasswordRecoveryHistory(ConstantsBase.WRITER_MOBILE_ID, email, mobilePhonePrefix + mobilePhone, STATUS_ID_SUCCESS);
                } catch (Exception e) {
                    log.error("can't send reset password email!!", e);
                }                
            }
            
            if (sendSms) {
                try {

                    String SenderName = "anyoption";
                    String senderNumber = CountryManagerBase.getById(usrArr.get(0).getCountryId()).getSupportPhone();
                    String msg = usrArr.get(0).getPassword();

                    try {
                        SMSManagerBase.sendTextMessage(SenderName, senderNumber, mobilePhonePrefix + mobilePhone, msg, usrArr.get(0).getId(), SMSManagerBase.SMS_KEY_TYPE_USERID, ConstantsBase.MOBIVATE_PROVIDER, SMS.DESCRIPTION_RESET_PASSWORD, usrArr.get(0).getMobileNumberValidation());
                        insertPasswordRecoveryHistory(ConstantsBase.WRITER_MOBILE_ID, email, mobilePhonePrefix + mobilePhone, STATUS_ID_SUCCESS);
                    } catch (SMSException smse) {
                        log.warn("Failed to send SMS.", smse);
                    }

                } catch (Exception e) {
                    log.error("Error processing send SMS when reset password.", e);
                }
            }

		} finally {
			closeConnection(con);
		}

		return res;
	}


    /**
	 * Get user MailBox
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<MailBoxUser> getUserMailBox(long userId) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return UsersDAOBase.getUserMailBox(conn, userId);
		} finally {
			closeConnection(conn);
		}
	}

	public static void updateEmailsByUserId(long userId, long statusId, String emails, String utcOffset) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			UsersDAOBase.updateEmailsByUserId(con, userId, emails, statusId, utcOffset);
		} finally {
			closeConnection(con);
		}
	}

	public static String getCurrencyLettersById(long currencyId) throws SQLException {
		Connection con = null;
		String currencyLetters;
		try {
			con = getConnection();
			currencyLetters = UsersDAOBase.getCurrencyLettersById(con, currencyId);
		} finally {
			closeConnection(con);
		}
		return currencyLetters;
	}

	/**
	 * Get combination instace by id
	 * @param combId
	 * @throws SQLException
	 */
	public static MarketingCombination getCombinationById(long combId) throws SQLException {
		Connection con = getConnection();
		try {
			return MarketingCombinationsDAOBase.getCombinationById(con, combId);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * @param combinationId
	 * @return
	 * @throws SQLException
	 */
	public static MarketingCombinationPixels getPixelsByCombId(long combinationId) throws SQLException {
        Connection con = getConnection();
        try {
        	 return MarketingPixelsDAOBase.getPixelsByCombId(con, combinationId);
        } finally {
            closeConnection(con);
        }
    }
	
	/**
	 * @param affiliateId
	 * @return
	 * @throws SQLException
	 */
	public static MarketingAffiliatePixels getPixelsByAffiliateId(long affiliateId) throws SQLException {
		Connection connection = getConnection();
		try {
			return MarketingPixelsDAOBase.getPixelsByAffiliateId(connection, affiliateId);
		} finally {
			closeConnection(connection);
		}
	}

	public static long getCurrencyIdByUserName(String userName) throws SQLException {
		Connection con = null;
		long currencyId;
		try {
			con = getConnection();
			currencyId = UsersDAOBase.getCurrencyIdByUserName(con, userName);
		} finally {
			closeConnection(con);
		}
		return currencyId;
	}
	
	public static void updateUserDepNoInv24H(long userId) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		UsersDAOBase.updateUserDepNoInv24H(con, userId);
    	} finally {
    		closeConnection(con);
    	}
		
	}

	public static long calculateUserRankById(long userId) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		return UsersDAOBase.calculateUserRankById(con, userId);
    	} finally {
    		closeConnection(con);
    	}
		
	}

	public static void updateUserRankIfNeeded(long userId, long transactionId, long currUserRank) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		int countRealDeposits = TransactionsManagerBase.countRealDeposit(userId);
			if(countRealDeposits == 1) {
				UsersDAOBase.updateUserRank(con, userId, UsersManagerBase.USER_RANK_NEWBIES);
				UsersDAOBase.insertIntoUserRankHist(con, userId, UsersManagerBase.USER_RANK_NEWBIES, transactionId);
			} else {
				boolean isNebies = UsersManagerBase.isNewbies(userId);
				if (!isNebies) {
					long userRank = UsersManagerBase.calculateUserRankById(userId);
					if (userRank != currUserRank) {
						UsersDAOBase.updateUserRank(con, userId, userRank);
						UsersDAOBase.insertIntoUserRankHist(con, userId, userRank, transactionId);
					}
				}
			}
    	} finally {
    		closeConnection(con);
    	}		
	}

	public static void updateUserStatusIfNeeded(long userId, long userCurrStatus, Long balanceAfterDeposit, Long depositId,
												Long investmentAmount, Long investmentId, Long opportunityTypeId) throws SQLException {
		Connection con = null;
		if (userCurrStatus != UsersManagerBase.USER_STATUS_ACTIVE) {
			try {
				con = getConnection();
				con.setAutoCommit(false);
				UsersDAOBase.updateUserStatus(con, userId, UsersManagerBase.USER_STATUS_ACTIVE);
				UsersDAOBase.insertIntoUserStatusHist(con, userId, UsersManagerBase.USER_STATUS_ACTIVE);
				StringBuilder msg = new StringBuilder("Updated user status, inserted into user status hist table");
				if (userCurrStatus == UsersManagerBase.USER_STATUS_COMA
						&& (opportunityTypeId == null || opportunityTypeId == Opportunity.TYPE_REGULAR || opportunityTypeId == Opportunity.TYPE_OPTION_PLUS)) {
					UserBalanceStepDAO.clearUserActiveData(con, userId, balanceAfterDeposit, depositId, investmentAmount, investmentId);
					msg.append(" and cleared user active data");
				}
				msg.append(". Commiting in db");
				log.info(msg.toString());
				con.commit();
			} catch (SQLException e) {
				try {
					con.rollback();
				} catch (SQLException sqle) {
					log.error("Failed to rollback.", sqle);
				}
				throw e;
			} finally {
				try {
					con.setAutoCommit(true);
				} catch (SQLException e) {
					log.error("Can't return connection back to autocommit.", e);
				}
				closeConnection(con);
			}
		}
	}

	public static boolean isNewbies(long userId) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		return UsersDAOBase.isNewbies(con, userId);
    	} finally {
    		closeConnection(con);
    	}	
	}
	
	
	/**
	 * Check for city coordinates
	 * @param u
	 * @return
	 * @throws SQLException
	 */
	public static boolean getUserCityCoordinates(User u, String countryName) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return UsersDAOBase.getUserCityCoordinates(con, u, countryName);
		} finally {
			closeConnection(con);
		}
	}
	
	public static boolean isTestUser(long userId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return UsersDAOBase.isTestUser(con, userId);
		} finally {
			closeConnection(con);
		}		
	}	
	
	 public static void insertPasswordRecoveryHistory(int writer, String email, String mobilePhone, int status) throws SQLException {
		  Connection con = null;
	  	try {
	          con = getConnection();
	          UsersDAOBase.insertPasswordRecoveryHistory(con, writer, email, mobilePhone, status);
	  	} finally {
	          closeConnection(con);
	      }
		  
	  }
	 
	public static boolean isUserIdNumberInUse(String idnum) throws Exception {
		Connection con = null;
		try {
			con = getConnection();
			return UsersDAOBase.isUserIdNumberInUse(con, idnum);
		} finally {
			closeConnection(con);
		}		
	}
	
	public static boolean isUserIdNumberInUse(String idnum, long skinId, long userId) throws Exception {
		Connection con = null;
		try {
			con = getConnection();
			return UsersDAOBase.isUserIdNumberInUse(con, idnum, skinId, userId);
		} finally {
			closeConnection(con);
		}		
	}

	/**
	 * @return user
	 * @throws Exception 
	 */
	public static UserAnycapital getCapitalUser(UserMethodRequest request) throws Exception {
		Connection connection = null;
		try {
			connection = getConnection();
			return UsersDAOBase.getCapitalUser(connection, request.getUserName());
		} finally {
			closeConnection(connection);
		}
	}
	
	public static UserAnycapitalExtraFields getCapitalUserExtraFields(UserMethodRequest request) throws Exception {
		Connection connection = null;
		try {
			connection = getConnection();
			return UsersDAOBase.getCapitalUserExtraFields(connection, request.getUserName());
		} finally {
			closeConnection(connection);
		}
	}
}