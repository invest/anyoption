package com.anyoption.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.bl_vos.ErrorCode;
import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.bl_vos.ClearingRoute;
import com.anyoption.common.clearing.CDPayClearingProvider;
import com.anyoption.common.clearing.CDPayInfo;
import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.clearing.ClearingManagerBase;
import com.anyoption.common.clearing.ClearingProvider;
import com.anyoption.common.clearing.InatecClearingProvider;
import com.anyoption.common.clearing.InatecInfo;
import com.anyoption.common.clearing.NetellerClearingProvider;
import com.anyoption.daos.ClearingDAO;

/**
 * Clearing manager.
 *
 * @author Tony
 */
public class ClearingManager extends ClearingManagerBase {
    private static final Logger log = Logger.getLogger(ClearingManager.class);
    

    /**
     * Load clearing provider by id
     * @param conn
     * @param providerId  provider to load
     * @throws ClearingException
     
    public static void loadClearingProviderById(Connection conn, long providerId) throws ClearingException {
        if (null == clearingProviders) {
            clearingProviders = new Hashtable<Long, ClearingProvider>();
            try {
                ClearingProviderConfig c = ClearingDAO.loadClearingProviderById(conn, providerId);
                    try {
                        Class cl = Class.forName(c.getProviderClass());
                        ClearingProvider p = (ClearingProvider) cl.newInstance();
                        p.setConfig(c);
                        clearingProviders.put(c.getId(), p);
                    } catch (Throwable t) {
                        log.error("Failed to load provider for config: " + c, t);
                    }
            } catch (Throwable t) {
                throw new ClearingException("Error loading clearing providers.", t);
            }
        }
    }*/

    /**
     * Direct banking deposit
     * @param info
     * @throws ClearingException
     */
    public static void directDeposit(InatecInfo info, long clearingProviderId) throws ClearingException {
    	ClearingProvider p = getClearingProviders().get(clearingProviderId);
    	if (null != p) {
    		info.setProviderId(clearingProviderId);
    		info.setInatecPaymentType(info.getInatecPaymentType());
    		try {
    			((InatecClearingProvider)p).onlineInitializeRequest(info); 
    		} catch (Throwable t) {
                throw new ClearingException("Problem processing directDeposit.", t);
            }
    	} else {
            throw new ClearingException("No route");
        }
    }
    
    /**
     * Direct banking deposit with Envoy
     * @param info
     * @throws ClearingException
     */
//    public static void directDeposit(EnvoyInfo info) throws ClearingException {
//    	ClearingProvider p = clearingProviders.get(ENVOY_PROVIDER_ID);
//    	if (null != p) {
//    		info.setProviderId(ENVOY_PROVIDER_ID);
//     		try {
//    			p.purchase(info);
//    		} catch (Throwable t) {
//                throw new ClearingException("Problem processing directDeposit.", t);
//            }
//
//    	} else {
//            throw new ClearingException("No route");
//        }
//    }

    /**
     * Process envoy withdraw request.
     *
     * @param info the request info
     * @throws ClearingException
     */
//    public static void withdraw(EnvoyInfo info) throws ClearingException {
//        ClearingProvider p = clearingProviders.get(ENVOY_PROVIDER_ID);
//        p.withdraw(info);
//    }





    /**
     * Find clearing route to process the request through.
     *
     * @param info the request info
     * @param deposit <code>true</code> to look for deposit route, <code>false</code> for withdraw
     * @return The id of the provider to process the request through or 0 if no route.
     * @throws ClearingException
     */
    public static ClearingRoute findRoute(ClearingInfo info, boolean deposit, Connection conn) throws ClearingException {
        ClearingRoute cr = null;
        try {
            conn = getConnection();
            String bin = info.getCcn().substring(0, 6);
            cr = ClearingDAO.findRoute(conn, info.getSkinId(), bin, info.getCurrencyId(), deposit, info.getCcCountryId(), info.getCcTypeId(), info.getPlatformId());
        } catch (Throwable t) {
            throw new ClearingException("Error finding route.", t);
        } finally {
            closeConnection(conn);
        }
        return cr;
    }

    /**
     * Process envoy Notification handler.
     *
     * @param info the request info
     * @param ip
     * @throws ClearingException
     */
//    public static EnvoyInfo handleNotifications(String requestXmlString, String ip, int source) throws ClearingException {
//        EnvoyClearingProvider p = (EnvoyClearingProvider) clearingProviders.get(ENVOY_PROVIDER_ID);
//        return p.handleNotifications(requestXmlString, ip, source);
//    }

	public static ArrayList<ErrorCode> getAllErrorCodes() throws SQLException{
		Connection conn = getConnection();
		try{
			return ClearingDAO.getAllErrorCodes(conn);
		} finally {
			closeConnection(conn);
		}
	}

	public static ArrayList<Long> getRelevantlimits(ClearingInfo ci, ClearingRoute cr) throws SQLException{
		Connection conn = getConnection();
		try{
			return ClearingDAO.getRelevantlimits(conn, ci, cr);
		} finally {
			closeConnection(conn);
		}
	}

	public static void updateTimeLastSuccess(long id) throws SQLException{
		Connection conn = getConnection();
		try{
			ClearingDAO.updateTimeLastSuccess(conn, id);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static void updateTimeLastReroute(long id) throws SQLException{
		Connection conn = getConnection();
		try{
			ClearingDAO.updateTimeLastReroute(conn, id);
		} finally {
			closeConnection(conn);
		}
	}

	public static void updateClearingProviderAndLog(ClearingRoute cr, CreditCard cc, long newClearingProviderId, long erroCodeId, boolean isInatec) throws SQLException{
		Connection con = getConnection();
		try{
			ClearingDAO.updateClearingProviderAndLog(con, cr, cc, newClearingProviderId, erroCodeId);
		} finally {
			closeConnection(con);
		}
	}

	public static long getproviderGroupById(long clearingProviderId){
		if(clearingProviderId == TBI_AO_PROVIDER_ID){
			return CLEARING_GROUP_TBI;
		} else if(clearingProviderId == WC_PROVIDER_ID){
			return CLEARING_GROUP_WIRE_CARD;
		} else if(clearingProviderId == WC_3D_PROVIDER_ID){
			return CLEARING_GROUP_WIRE_CARD_3D;
		} else if(clearingProviderId == INATEC_PROVIDER_ID_EUR || clearingProviderId == INATEC_PROVIDER_ID_USD || clearingProviderId == INATEC_PROVIDER_ID_GBP || clearingProviderId == INATEC_PROVIDER_ID_TRY){
			return CLEARING_GROUP_INATEC;
		}
		return 0;
	}

	public static long insertNewClearingRouteAndLog(ClearingRoute cr, CreditCard cc, long newClearingProviderId, long erroCodeId, long currnecyId, boolean isInatec, int platformId) throws SQLException{
		Connection con = getConnection();
		try{
			return ClearingDAO.insertNewClearingRouteAndLog(con, cr, cc, newClearingProviderId, erroCodeId, currnecyId, isInatec, platformId);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ClearingRoute getClearingRouteById(long id) throws SQLException{
		Connection con = getConnection();
		try{
			return ClearingDAO.getClearingRouteById(con, id);
		}finally{
			closeConnection(con);
		}
	}
	
	public static ArrayList<ClearingRoute> getClearingRouteByBin(String bin) throws SQLException{
		Connection con = getConnection();
		try{
			return ClearingDAO.getClearingRouteByBin(con, bin);
		} finally {
			closeConnection(con);
		}
	}
	
	public static void updateClearingRouteIsActive(boolean isActive, long crId, CreditCard cc, long errorCodeId) throws Exception{
		Connection con = getConnection();
		try {
			ClearingDAO.updateClearingRouteIsActive(con, isActive, crId, cc, errorCodeId);
		} finally {
			closeConnection(con);
		}
	}
	

	public static long logicForUpdateClearingRoute(String bin, long providerId) throws SQLException{
		long crId = 0;
		ArrayList<ClearingRoute> list = new ArrayList<ClearingRoute>();
		ClearingRoute cr = new ClearingRoute();
		list = getClearingRouteByBin(bin);
		if(list.size() > 0){
			return list.get(0).getId();
		}
		return crId;
	}

	public static ArrayList<ClearingRoute> existclearingRouteWithCurrnecy(String bin) throws SQLException {
		Connection conn = getConnection();
		try{
			return ClearingDAO.existclearingRouteWithCurrnecy(conn, bin);
		}finally{
			closeConnection(conn);
		}
	}
	
	/**
     * Deposit with BaroPay
     * @param info
     * @param clearingProviderId
     * @throws ClearingException
     */
//    public static void setBaroPayDepositDetails(BaroPayInfo info, long clearingProviderId) throws ClearingException {
//    	BaroPayClearingProvider p = (BaroPayClearingProvider)getClearingProviders().get(clearingProviderId);
//    	if (null != p) {
//    		info.setProviderId(clearingProviderId);
//    		try {
//    			p.setProviderDetails(info);
//    		} catch (Throwable t) {
//                throw new ClearingException("Problem processing BaroPay deposit.", t);
//            }
//    	} else {
//            throw new ClearingException("No route");
//        }
//    }
    
    /**
     * Authorize with BaroPay
     * @param info
     * @param clearingProviderId
     * @throws ClearingException
     */
//    public static void authorizeBaroPayDeposit(BaroPayInfo info, long clearingProviderId) throws ClearingException {
//    	BaroPayClearingProvider p = (BaroPayClearingProvider)getClearingProviders().get(clearingProviderId);
//    	if (null != p) {
//    		try {
//    			p.authorize(info);
//    		} catch (Throwable t) {
//                throw new ClearingException("Problem processing BaroPay authorize.", t);
//            }
//    	} else {
//            throw new ClearingException("No route");
//        }
//    }
    


	 /**
     * Deposit with CDPay - set cdpay clearing provider details.
     * @param info
     * @throws ClearingException
     */
    public static void setCDPayDepositDetails(CDPayInfo info, long clearingProviderId) throws ClearingException {
    	CDPayClearingProvider p = (CDPayClearingProvider)getClearingProviders().get(clearingProviderId);
    	if (null != p) {
    		info.setProviderId(clearingProviderId);
    		try {
    			p.setProviderDetails(info);
    		} catch (Throwable t) {
                throw new ClearingException("Problem processing CDPay deposit.", t);
            }
    	} else {
            throw new ClearingException("No route");
        }
    }



    /**
     * Process Inatec online status result request.
     *
     * @param info the request info
     * @throws ClearingException
     */
    public static void onlineStatusResult(ClearingInfo info) throws ClearingException {
        InatecClearingProvider inp = (InatecClearingProvider) getClearingProviders().get(info.getProviderId());
        if (null != inp) {
        	inp.onlineStatusResult(info);
        } else {
            throw new ClearingException("No route");
        }
    }
   
}