package com.anyoption.managers;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.beans.MarketingCombination;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.daos.MarketingCombinationsDAOBase;

/**
 * @author EranL
 *
 */
public abstract class MarketingCombinationManagerBase extends BaseBLManager {

    public static MarketingCombination getById(long marketingCombinationId) throws SQLException {
        Connection con = getConnection();
        try {
        	return MarketingCombinationsDAOBase.getById(con, marketingCombinationId); 
        } finally {
            closeConnection(con);
        }
    }

}