package com.anyoption.managers;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;


import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.anyoption.daos.IssuesDAOBase;
import com.anyoption.daos.PopulationsDAOBase;

/**
 *
 * @author KobiM
 *
 */
public class IssuesManagerBase extends com.anyoption.common.managers.IssuesManagerBase {

	private static final Logger logger = Logger.getLogger(IssuesManagerBase.class);

	public static final long ISSUE_NO_DIRECTION = 0;
	public static final long ISSUE_DIRECTION_OUT_BOUND_PHONE_CALL = 1;
	public static final long ISSUE_DIRECTION_IN_BOUND_PHONE_CALL = 2;
	public static final long ISSUE_DIRECTION_MEMBER_REQUESTED_PHONE_CALL = 3;

	public static final long ISSUE_REACHED_STATUS_NOT_REACHED = 1;
	public static final long ISSUE_REACHED_STATUS_REACHED = 2;
	public static final long ISSUE_REACHED_STATUS_FALSE_ACCOUNT = 3;

	public static final long ISSUE_REACTION_NO_ANSWER = 1;
	public static final long ISSUE_REACTION_ALREADY_DEPOSIT = 2;
	public static final long ISSUE_REACTION_NOT_COOPERATIVE = 3;
	public static final long ISSUE_REACTION_CALL_BACK = 4;
	public static final long ISSUE_REACTION_CUT_OFF_CALLS = 5;
	public static final long ISSUE_REACTION_DIF_LANGUAGE = 6;
	public static final long ISSUE_REACTION_LINE_BUSY = 7;
	public static final long ISSUE_REACTION_NOT_INTERESTED = 8;
	public static final long ISSUE_REACTION_THIRD_PARTY = 9;
	public static final long ISSUE_REACTION_WRONG_NUMBER = 10;
	public static final long ISSUE_REACTION_DEPOSIT_DURING_CALL = 11;
	public static final long ISSUE_REACTION_COOPERATION = 12;
	public static final long ISSUE_REACTION_DENIED_ACCOUNT_OPENING = 13;
	public static final long ISSUE_REACTION_CANT_TALK_NOW = 14;
	public static final long ISSUE_REACTION_WILL_DEPOSIT_LATER = 15;
	public static final long ISSUE_REACTION_NOT_INTERESTED_NOW = 16;
	public static final long ISSUE_REACTION_NOT_SURE = 17;
	public static final long ISSUE_REACTION_BURN = 18;
	public static final long ISSUE_REACTION_QUERY_ANSWERED = 19;
	public static final long ISSUE_REACTION_OPENED_ACCOUNT = 20;

	// reaction's deposit search type
	public static final int DEPOSIT_SEARCH_TYPE_NONE = 0;
	public static final int DEPOSIT_SEARCH_TYPE_DEPOSIT_DURING_CALL = 1;
	public static final int DEPOSIT_SEARCH_TYPE_FUTURE_DEPOSIT = 2;

	public static final int ISSUE_STATUS_ACTION_REQUIRED = 1;
	public static final int ISSUE_STATUS_ON_PROGRESS = 2;
	public static final int ISSUE_STATUS_FINISHED = 3;
	public static final int ISSUE_STATUS_CLOSED = 10;
	
	//files risk status
	public static final int FILES_RISK_STATUS_NEW_ISSUE = 4;
	public static final int FILES_RISK_STATUS_IN_PROGRESS = 5;
	public static final int FILES_RISK_STATUS_ADDITIONAL_REQUEST = 6;
	public static final int FILES_RISK_STATUS_DONE_SUPPORT_UNREACHED = 7;
	public static final int FILES_RISK_STATUS_DONE_SUPPORT = 8;
	public static final int FILES_RISK_STATUS_NEED_APPROVE = 9;
	public static final int FILES_RISK_STATUS_CLOSED = 10;
	public static final int FILES_RISK_STATUS_DONE_UNCOOPERATIVE = 11;

	public static final int ISSUE_PRIORITY_LOW = 1;

	public static final long ISSUE_SUBJ_DOCUMENTS_REQUIRED = 17;
	public static final long ISSUE_SUBJ_GENERAL = 5;
	public static final long ISSUE_SUBJ_CC_CHANGED = 51;
	public static final long ISSUE_SUBJ_POPULATION = 31;
    public static final long ISSUE_SUBJ_SMS = 34;
	public static final long ISSUE_SUBJ_CHARGEBACK = 19;

	public static final int ISSUE_CHANNEL_CALL = 1;
	public static final int ISSUE_CHANNEL_EMAIL = 3;
	public static final int ISSUE_CHANNEL_COMMENT = 4;
	public static final int ISSUE_CHANNEL_FALSE_ACCOUNT = 5;
	public static final int ISSUE_CHANNEL_DISABLE_PHONE_CONTACT = 6;
	public static final int ISSUE_CHANNEL_ACTIVITY = 7;
	public static final int ISSUE_CHANNEL_CHAT = 8;

	public static final int ISSUE_ACTIVITY_FALSE_ACCOUNT = 1;
	public static final int ISSUE_ACTIVITY_CANCEL_PHONE_CONTACT = 2;
	public static final int ISSUE_ACTIVITY_CANCEL_PHONE_CONTACT_DAA = 3;
	public static final int ISSUE_ACTIVITY_CANCEL_CALL_BACK = 4;
	public static final int ISSUE_ACTIVITY_RETURN_TO_SALES = 5;
	public static final int ISSUE_ACTIVITY_REMOVE_FROM_POPULATION = 6;
	public static final int ISSUE_ACTIVITY_REMOVE_FROM_SALES = 7;
	public static final int ISSUE_ACTIVITY_DISPLAY_MARKETING_USERS = 8;
	public static final int ISSUE_ACTIVITY_FRAUD = 9;
	public static final int ISSUE_ACTIVITY_CHB_CLOSE= 10;
	public static final int ISSUE_ACTIVITY_DUPLICATE_ACCOUNT= 11;
	public static final int ISSUE_ACTIVITY_ACTIVATE_ACCOUNT= 12;
	public static final int ISSUE_ACTIVITY_DISACTIVATE_ACCOUNT= 13;


	/* ACTION TYPES */

	// Activities
	public static final int ISSUE_ACTION_FALSE_ACCOUNT = 1;
	public static final int ISSUE_ACTION_CANCEL_PHONE_CONTACT = 2;
	public static final int ISSUE_ACTION_CANCEL_PHONE_CONTACT_DAA = 3;
	public static final int ISSUE_ACTION_CANCEL_CALL_BACK = 4;
	public static final int ISSUE_ACTION_RETURN_TO_SALES = 5;
	public static final int ISSUE_ACTION_REMOVE_FROM_POPULATION = 6;
	public static final int ISSUE_ACTION_REMOVE_FROM_SALES = 7;
	public static final int ISSUE_ACTION_DISPLAY_MARKETING_USERS = 8;
	public static final int ISSUE_ACTION_FRAUD = 9;
	public static final int ISSUE_ACTION_CHB_CLOSE = 10;
	public static final int ISSUE_ACTION_DUPLICATE_ACCOUNT = 11;
	public static final int ISSUE_ACTION_ACTIVATE_ACCOUNT = 12;
	public static final int ISSUE_ACTION_DEACTIVATE_ACCOUNT = 13;
	public static final int ISSUE_ACTION_ALLOWED_CREDIT_CARD = 42;
	// Call Reactions
	public static final int ISSUE_ACTION_CANT_TALK_NOW = 15;
	public static final int ISSUE_ACTION_WILL_DEPOSIT_LATER = 16;
	public static final int ISSUE_ACTION_NOT_INTERESTED_NOW = 17;
	public static final int ISSUE_ACTION_NOT_SURE = 18;
	public static final int ISSUE_ACTION_BURN = 19;
	public static final int ISSUE_ACTION_QUERY_ANSWERED = 20;
	public static final int ISSUE_ACTION_NO_ANSWER = 21;
	public static final int ISSUE_ACTION_ALREADY_DEPOSIT = 22;
	public static final int ISSUE_ACTION_NOT_COOPERATIVE = 23;
	public static final int ISSUE_ACTION_CALL_BACK = 24;
	public static final int ISSUE_ACTION_CUT_OFF_CALLS = 25;
	public static final int ISSUE_ACTION_DIFFERENT_LANGUAGE = 26;
	public static final int ISSUE_ACTION_LINE_BUSY = 27;
	public static final int ISSUE_ACTION_NOT_INTERESTED = 28;
	public static final int ISSUE_ACTION_THIRD_PARTY = 29;
	public static final int ISSUE_ACTION_WRONG_NUMBER = 30;
	public static final int ISSUE_ACTION_DEPOSIT_DURING_CALL = 31;
	public static final int ISSUE_ACTION_COOPERATION = 32;
	public static final int ISSUE_ACTION_DENIED_ACCOUNT_OPENING = 33;
	public static final int ISSUE_ACTION_OPENED_ACCOUNT = 34;
	public static final int ISSUE_ACTION_LOGIN_FAIL_CLOSE = 39;
	public static final int ISSUE_ACTION_RISK_CONTACT_USER = 40;
	public static final int ISSUE_ACTION_RISK_NEED_DOCUMENT = 41;
	public static final int ISSUE_ACTION_PENDING_DOC=44;
	public static final int ISSUE_SUBJECT_REGISTRATION_SMS = 71;

	// Channels Action Types
	public static final int ISSUE_ACTION_SMS = 35;
	public static final int ISSUE_ACTION_EMAIL = 36;
	public static final int ISSUE_ACTION_COMMENT = 37;
	public static final int ISSUE_ACTION_CHAT = 38;

	//Risk issue subject
	public static final String ISSUE_SUBJECT_RISK_ISSUE = "47";
	public static final int ISSUE_SUBJECT_REG_DOC = 56;
	
	//Issue reason
	public static final int ISSUE_REASON_TYPE_REG_DOC = 14;

	/**
	 * Update issues by population entry id
	 * this menthod called after in
	 * @param popEntryId
	 * @param userId
	 * @throws SQLException
	 */
	public static void updateIssuesAndUserForContact(Connection con, PopulationEntryBase popUser) throws SQLException {
		try {
			con.setAutoCommit(false);

			IssuesDAOBase.updateIssuesByPopUserId(con, popUser);
			PopulationsDAOBase.updateUserIdForContact(con, popUser);
		} catch (Exception exp) {
			logger.error("Exception in updateIssuesAndUserForContact event ", exp);
		    try {
				con.rollback();
		    } catch (Throwable it) {
		    	logger.error("Can't rollback.", it);
			}
		} finally {
			con.setAutoCommit(true);
		}
	}
	
	public static boolean isExistsIssueByUserAndCC(	long ccNum, long userId, int subjectId,
													Connection con)	throws SQLException, CryptoException, InvalidKeyException,
																	NoSuchAlgorithmException, NoSuchPaddingException,
																	IllegalBlockSizeException, BadPaddingException,
																	UnsupportedEncodingException, InvalidAlgorithmParameterException {
        try {
            con.setAutoCommit(false);
            if (IssuesDAOBase.isExistsIssueByUserAndCC(con, ccNum, userId, subjectId)==0) {
                return false;
            } else {
                return true;
            }
            
        } finally {
            con.setAutoCommit(true);
        }
    }
	
	public static int[] getExistsIssueByUserAndCC(long ccNum, long userId, Connection con) throws SQLException, CryptoException{
	   try {
	       con.setAutoCommit(false);

	       return IssuesDAOBase.getExistsIssueByUserAndCC(con, ccNum, userId);
	       
	     } finally {
	       con.setAutoCommit(true);
	     }
	   }
	
	
	public static void insertAction(Issue issue, IssueAction issueAction,String utcOffset,long writerId, User user, int screenId, long creditCardId, Connection conn) throws Exception {

            issueAction.setIssueId(issue.getId());
            issueAction.setWriterId(writerId);
            issueAction.setActionTime(new Date());
            issueAction.setActionTimeOffset(utcOffset);
            validateAndInsertAction(conn, issueAction, user, issue, screenId, creditCardId);

    }
	

	public static boolean insertDocRequest(long issueActionId, long ccId, String type, long userId, Connection con) throws Exception{
	        boolean res = true;

	        IssuesDAOBase.insertDocRequest(con, issueActionId, ccId, type, userId);
	        
	        return res;
	  }
	
	   public static boolean updateDocState(long userId, long ccId, long docState, Connection con) throws Exception{
	        boolean res = true;

	        IssuesDAOBase.updateDocState(con, userId, ccId, docState);
	        
	        return res;
	  }
	   
	   public static boolean updateIssueState(int statusId, long actionId, long issueId, Connection con) throws Exception {
	       boolean res = false;

	       res=IssuesDAOBase.updateIssueStatusByIaId(con, statusId, actionId, issueId);            

	       return res;
	   }  
}
