package com.anyoption.managers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.beans.TierUserHistory;
import com.anyoption.beans.User;
import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.InvestmentRejects;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunityMiniBean;
import com.anyoption.common.beans.OpportunityType;
import com.anyoption.common.beans.Skin;
import com.anyoption.common.beans.TierUser;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.bl_vos.InvestmentLimit;
import com.anyoption.common.bl_vos.OptionPlusQuote;
import com.anyoption.common.daos.InvestmentsDAOBase;
import com.anyoption.common.jms.WebLevelLookupBean;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.managers.InvestmentRejectsManagerBase;
import com.anyoption.common.managers.RewardUserTasksManager;
import com.anyoption.common.managers.RewardUserTasksManager.TaskGroupType;
import com.anyoption.common.util.BonusFormulaDetails;
import com.anyoption.common.util.GoldenMinute.GoldenMinuteType;
import com.anyoption.common.util.InvestmentValidatorAbstract;
import com.anyoption.common.util.MessageToFormat;
import com.anyoption.common.util.OpportunityCache;
import com.anyoption.common.util.OpportunityCacheBean;
import com.anyoption.daos.BonusDAOBase;
import com.anyoption.daos.GeneralDAO;
import com.anyoption.daos.LimitsDAOBase;
import com.anyoption.daos.OpportunitiesDAOBase;
import com.anyoption.daos.TaxHistoryDAOBase;
import com.anyoption.daos.TiersDAOBase;
import com.anyoption.daos.UsersDAOBase;
import com.anyoption.util.CommonUtil;
import com.anyoption.util.ConstantsBase;
import com.copyop.common.enums.base.ProfileLinkCommandEnum;
import com.copyop.common.jms.CopyOpEventSender;
import com.copyop.common.jms.events.ProfileManageEvent;
import com.copyop.common.jms.events.TournamentInvestmentEvent;

public class InvestmentsManagerBase extends com.anyoption.common.managers.InvestmentsManagerBase {
    private static final Logger log = Logger.getLogger(InvestmentsManagerBase.class);

    public static MessageToFormat submitInvestment(
            User user,
            Investment inv,
            OpportunityCacheBean opportunity,
            boolean fromGraph,
            boolean calcalistGame,
            long writerId,
            String sessionId,
            WebLevelsCache levelsCache,
            InvestmentValidatorAbstract investmentValidator,
            long destUserId,
            long loginId) {
        if (log.isDebugEnabled()) {
            log.debug("try to submit -" +
                    " id: " + inv.getOpportunityId() +
                    " amount: " + inv.getAmount() +
                    " Time to be Created " + inv.getTimeCreated() +
                    " pageLevel: " + inv.getCurrentLevel() +
                    " pageOddsWin: " + inv.getOddsWin() +
                    " pageOddsLose: " + inv.getOddsLose() +
                    " choice: " + inv.getTypeId() +
                    " UTC offset: " + inv.getUtcOffsetCreated() +
                    " user: " + user.getUserName() +
                    " sessionId: " + sessionId +
                    " fromGraph: " + fromGraph +
                    " calcalistGame: " + calcalistGame +
                    " writerId: " + writerId +
                    " ip: " + inv.getIp());
        }
        MessageToFormat msg = null;
        Connection conn = null;
        try {
        	double rate = CurrencyRatesManagerBase.getInvestmentsRate(user.getCurrencyId());
        	if (rate == 0D) {
        		log.error("Rejecting investment, because the rate is 0");
        		msg = new MessageToFormat("error.investment", null);
        	} else {
	            double convertAmount = inv.getAmount() * rate;
	            long optionPlusFee = inv.getOptionPlusFee();
	            Float  pageOddsLose = (float)inv.getOddsLose();
	            if (opportunity.getOpportunityTypeId() != Opportunity.TYPE_REGULAR){
	                pageOddsLose = null;
	            }
	            InvestmentRejects invRej = new InvestmentRejects(inv.getOpportunityId(), inv.getCurrentLevel(), sessionId, inv.getAmount(), user.getId(), rate, (float) inv.getOddsWin(), pageOddsLose, (int) inv.getTypeId(), ConstantsBase.OPPORTUNITIES_TYPE_DEFAULT, writerId, inv.getApiExternalUserId());
	            inv.setMarketId(opportunity.getMarketId());

	            conn = getConnection();
	            WebLevelLookupBean wllb = new WebLevelLookupBean();
	            wllb.setSkinGroup(SkinsManagerBase.getSkin(user.getSkinId()).getSkinGroup());	            
	            msg = investmentValidator.validate(conn, user, inv, opportunity, sessionId, levelsCache, wllb, convertAmount, invRej, rate, inv.getAmount());
	            if( msg!= null && msg.getKey().equals("error.investment.nomoney")){
                	if((CommonUtil.getProperty("copyop.isEnabled").equalsIgnoreCase("true"))) {
                		if(opportunity.getOpportunityTypeId() == Opportunity.TYPE_REGULAR) {
	            			if(user.getSkinId() != Skin.SKIN_ETRADER) {
	            				CopyOpEventSender.sendEvent(new ProfileManageEvent(ProfileLinkCommandEnum.LOWBALANCE_COPIERS, user.getId(), 1), CopyOpEventSender.PROFILE_MANAGE_DESTINATION_NAME);
	            				log.debug("Send to copyop");
	            			}
                		}
                	}
	            }
	            if (null == msg) {
	                inv.setId(
	                		InvestmentsManagerBase.insertInvestment(user,
	                												opportunity,
	                												inv.getAmount() + optionPlusFee,
	                												(int) inv.getTypeId(),
	                												inv.getCurrentLevel(),
	                												inv.getIp(),
	                												wllb.getRealLevel(),
	                												wllb.getDevCheckLevel(),
	                												inv.getUtcOffsetCreated(),
	                												rate,
	                												fromGraph,
	                												writerId,
	                												optionPlusFee,
	                												user.getCountryId(),
	                												inv.getApiExternalUserId(),
	                												inv.getOddsWin() - 1,
	                												1 - inv.getOddsLose(),
	                												inv.getCopyopInvId(),
	                												inv.getCopyopType(),
	                												wllb.isClosingFlagClosest(),
	                												destUserId,
	                												inv.getBubbleStartTime(),
	                												inv.getBubbleEndTime(),
	                												inv.getBubbleLowLevel(),
	                												inv.getBubbleHighLevel(),
	                												loginId,
	                												inv.getPrice()
	                												)
	                		);
	                inv.setTimeCreated(new Date());
	                if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
	            		CommonUtil.sendNotifyForInvestment(
	            						opportunity.getId(),
										inv.getId(),
										convertAmount,
										inv.getAmount(),
										inv.getTypeId(),
										inv.getMarketId(),
										new Long(opportunity.getOpportunityTypeId()).intValue(),
										inv.getCurrentLevel(),
										levelsCache,
										user,
										inv.getCopyopType());
	            		
	            		CopyOpEventSender.sendEvent(new TournamentInvestmentEvent(CommonUtil.getProductTypeId((int)opportunity.getOpportunityTypeId()), 
	            				convertAmount, user.getId(), user.getSkinId()),
								CopyOpEventSender.TOURNAMENT_INVESTMENT);
	                }
	                RewardUserTasksManager.rewardTasksHandler(TaskGroupType.INVESTMENT, user.getId(), inv.getAmount() + optionPlusFee, inv.getId(), BonusManagerBase.class, (int) writerId,
	                		opportunity.getOpportunityTypeId() == Opportunity.TYPE_OPTION_PLUS ? OpportunityType.PRODUCT_TYPE_OPTION_PLUS : OpportunityType.PRODUCT_TYPE_BINARY, opportunity.getOpportunityTypeId());
	            }
        	}
        } catch (Exception e) {
            log.error("Failed to insert investment - user: " + user.getUserName() + " sessionId: " + sessionId, e);
            msg = new MessageToFormat("error.investment", null);
        } finally {
            closeConnection(conn);
        }

        try {
			PopulationsManagerBase.invest(	user.getId(), null == msg ? true : false, Writer.WRITER_ID_WEB, null, null,
											inv.getAmount() - inv.getOptionPlusFee(), inv.getId(), opportunity.getOpportunityTypeId());
        } catch (Exception e) {
            log.warn("Problem with population invest event", e);
        }
        return msg;
    }

    /**
     * Cehck if this user can make this investment. Check if he has enough money and that
     * the amount he want to invest is in the allowed interval (min, max).
     *
     * @param user the user who wants to make investment (the balance should be up to date)
     * @param slipEntry the slip entry of the investment
     * @param pageOddsWin the current odds win displayed on the client page
     * @param pageOddsLose the current odds lose displayed on the client page
     * @param invRej the reject investment detaiis
     * @return <code>null</code> if check is ok else error message.
     * @throws SQLException
     */
    public static MessageToFormat validateInvestment(User user, Opportunity o, long amount, double pageOddsWin, double pageOddsLose, InvestmentRejects invRej) throws SQLException {
        Connection conn = getConnection();
        try {

//            InvestmentLimit il = InvestmentsDAOBase.getInvestmentLimit(conn, o.getMarket().getInvestmentLimitsGroupId(), user.getCurrencyId());
            InvestmentLimit il = InvestmentsDAOBase.getInvestmentLimit(conn, o.getOpportunityTypeId(), o.getScheduled(), o.getMarketId(), user.getCurrencyId(), user.getId());
            long minAmount = il.getMinAmount();
            long maxAmount = il.getMaxAmount();

            if (amount < minAmount || amount > maxAmount) {
                if (log.isDebugEnabled()) {
                    log.debug("Limit reached. min: " + minAmount + " max: " + maxAmount);
                }

                int rejetcTypeId = ConstantsBase.INVESTMENT_REJECT_MIN_INV_LIMIT;
                String errorMsg = "";
                long limitAmount;
                if ( amount < minAmount ) {
                    invRej.setRejectAdditionalInfo("Min Limit:, amount: " + amount + " , minimum: " + minAmount);
                    errorMsg = "error.investment.limit.min";
                    limitAmount = minAmount;
                } else {
                    invRej.setRejectAdditionalInfo("Max Limit:, amount: " + amount + " , maximum: " + maxAmount);
                    rejetcTypeId = ConstantsBase.INVESTMENT_REJECT_MAX_INV_LIMIT;
                    errorMsg = "error.investment.limit.max";
                    limitAmount = maxAmount;
                }

                invRej.setRejectTypeId(rejetcTypeId);
                InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
                return new MessageToFormat(errorMsg, new Object[] {CommonUtil.formatCurrencyAmountAO(limitAmount, true, user.getCurrencyId())});
            }
/*
            if (System.currentTimeMillis() < o.getTimeFirstInvest().getTime()) {
                log.debug("Opportunity not opened.");
                return new MessageToFormat("error.investment.notopened", null);
            }
            if (System.currentTimeMillis() > o.getTimeLastInvest().getTime()) {
                log.debug("Opportunity expired.");
                return new MessageToFormat("error.investment.expired", null);
            }
            long dbWin = Math.round(o.getOverOddsWin() * 100);
            long dbLose = Math.round(o.getOddsType().getOverOddsLose() * 100);
            long pageWin = Math.round((pageOddsWin - 1) * 100);
            long pageLose = Math.round((1 - pageOddsLose) * 100);
            if (dbWin != pageWin || dbLose != pageLose) {
                if (log.isDebugEnabled()) {
                    log.debug("db win: " + dbWin +
                            " db lose: " + dbLose +
                            " page win: " + pageWin +
                            " page lose: " + pageLose);
                }
                invRej.setRejectAdditionalInfo("Odds Change:, dbwin: " + dbWin + " , pagewin: " + pageWin + " , dblose: " + dbLose + " , pagelose: " + pageLose);
                invRej.setRejectTypeId(ConstantsBase.INVESTMENT_REJECT_ODDS_CHANGE);
                InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
                return new MessageToFormat("error.investment.odds.change", null);
            }*/
        } finally {
            closeConnection(conn);
        }
        return null;
    }
    
    public static InvestmentLimit getInvestmentLimit(long oppTypeId, long oppScheduled, long oppMarketId, long userCurrencyId, long userId) throws SQLException {
        Connection conn = getConnection();
        InvestmentLimit il = null;
        try {
            il = InvestmentsDAOBase.getInvestmentLimit(conn, oppTypeId, oppScheduled, oppMarketId, userCurrencyId, userId);
        } finally {
            closeConnection(conn);
        }
        return il;
    }



 

//    public static void afterInvestmentSuccess(User user, long balance, long investmentId, long investmentAmount, long writerId, boolean isFree, long opportunityTypeId, long optionPlusFee, int insuranceType) throws SQLException {
//    	// update one click user field
//		if (insuranceType == 0 && (opportunityTypeId == Opportunity.TYPE_REGULAR || opportunityTypeId == Opportunity.TYPE_OPTION_PLUS)) {
//			user.setDefaultAmountValue(investmentAmount - optionPlusFee);
//		}
//    	Connection conn = null;
//        try {
//            conn = getConnection();
//            conn.setAutoCommit(false);
//            afterInvestmentSuccess(conn, user.getId(), balance, investmentId, investmentAmount, writerId, isFree, opportunityTypeId);
//            conn.commit();
//        } catch (SQLException sqle) {
//            try {
//                conn.rollback();
//            } catch (SQLException sqlie) {
//                log.error("Failed to rollback.", sqlie);
//            }
//            throw sqle;
//        } finally {
//            try {
//                conn.setAutoCommit(true);
//            } catch (SQLException sqle) {
//                log.error("Can't return connection back to autocommit.", sqle);
//            }
//            closeConnection(conn);
//        }
//    }

//    public static ArrayList<Investment> getInvestmentsByUser(long userId, Date from, Date to, boolean isSettled, long groupId, long marketId, long skinId, Integer startRow, Integer pageSize, long writerId, int period) throws SQLException {
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            return InvestmentsDAOBase.getByUser(conn, userId, from, to, isSettled, groupId, marketId, skinId, startRow, pageSize, writerId, period);
//        } finally {
//            closeConnection(conn);
//        }
//    }

    /**
     * Load todays opened investments for specified user.
     *
     * @throws SQLException
     */
    public static ArrayList<Investment> getTodaysOpenedInvestments(long userId, String userOffset) throws SQLException {
        ArrayList<Investment> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = InvestmentsDAOBase.getTodaysOpenedInvestments(conn, userId, userOffset);
        } finally {
            closeConnection(conn);
        }
        return l;
    }



    /**
     * Settle investment.
     *
     * @param investmentId the id of the investment to settle.
     * @param writerId
     * @throws SQLException
     */
    public static void settleInvestment(long investmentId, long writerId, long insuranceAmount, Investment inv, long optionPlusePrice, long loginId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            settleInvestment(conn, investmentId, writerId, insuranceAmount, inv, optionPlusePrice, ConstantsBase.LOG_BALANCE_SETTLE_INVESTMENT, loginId);
        } finally {
            closeConnection(conn);
        }
    }

//    /**
//     * settle investment
//     *
//     * @param conn
//     * @param investmentId
//     * @param writerId
//     * @param insuranceAmount the amount of the gm insurance, 0 if it wasnt bought
//     * @param inv the investment to settle or null
//     * @throws SQLException
//     */
//    public static void settleInvestment(Connection conn, long investmentId, long writerId, long GMInsuranceAmount, Investment inv,
//    									OracleConnection oracleCon, long optionPlusePrice, long loginId) throws SQLException {
//        Investment investment = inv;
//        long result;
//        long netResult;
//        boolean win = false;
//        boolean voidBet = false;
//        if (GMInsuranceAmount == 0 && optionPlusePrice == 0 && inv.getTypeId() != Investment.TYPE_BUBBLE) { // if its not golden minutes or bubbles settlement
//            investment = InvestmentsDAOBase.getInvestmentToSettle(conn, investmentId, true, false);
//            boolean callWin = investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL && investment.getClosingLevel() > investment.getCurrentLevel();
//            boolean putWin = investment.getTypeId() == Investment.INVESTMENT_TYPE_PUT && investment.getClosingLevel() < investment.getCurrentLevel();
//            boolean oneWin = false;
//            boolean isOneTouch = investment.getTypeId() == Investment.INVESTMENT_TYPE_ONE;
//            if (isOneTouch) {
//            	long upDown = InvestmentsDAOBase.getOneTouchInvestmentUpDown(conn, investmentId);
//            	if (upDown == Investment.INVESTMENT_ONE_TOUCH_UP) {
//            		oneWin = investment.getClosingLevel() >= investment.getCurrentLevel();
//            	} else if (upDown == Investment.INVESTMENT_ONE_TOUCH_DOWN) {
//            		oneWin = investment.getClosingLevel() <= investment.getCurrentLevel();
//            	} else if (upDown == -1) {
//            		if (log.isEnabledFor(Level.INFO)) {
//                    	log.log(Level.ERROR, "The one touch opportunity " + investment.getOpportunityId() + " related to this investment " + investment.getId() + " is invalid");
//                    }
//            	}
//            }
//
//            win = callWin || putWin || oneWin;
//
//            if (log.isEnabledFor(Level.INFO)) {
//            	log.log(Level.INFO, "investment.getTypeId() = " + investment.getTypeId() + "; investment.getClosingLevel() = " + investment.getClosingLevel() + "; investment.getCurrentLevel() = " + investment.getCurrentLevel() + "; investment.isUpInvestment() = " + InvestmentsManagerBase.isUpInvestment(investment));
//            	log.log(Level.INFO, "putWin = " + putWin + "; callWin = " + callWin + "; isOneTouch = " + isOneTouch + "; win= " + win);
//            }
//
//            if (investment.getClosingLevel().doubleValue() == investment.getCurrentLevel().doubleValue() && investment.getTypeId() != Investment.INVESTMENT_TYPE_ONE) {
//                voidBet = true;
//            }
//        } else { //golden minutes settlement
//            if (GMInsuranceAmount > 0) {
//                win = true;
//            } else { //its option pluse settlement (only win or lose)
//                if (optionPlusePrice > investment.getAmount() - investment.getOptionPlusFee()) {
//                    win = true;
//                }
//            }
//        }
//        
//        //hack for bubble settlement we dont want to take GMInsuranceAmount as premia
//        if (inv.getTypeId() == Investment.TYPE_BUBBLE) {
//        	GMInsuranceAmount = 0;
//        }
//
//        // NOTE: Here is the key moment - we take the win/lose and we expect no one else
//        // will change it before we finish the settlement of this investment.
//        // This shouldn't be a problem as far as someone don't manage to squeeze a cancel
//        // in the middle from the backend.
//
//
//        // subtract the premia amount
//        long invesAmount = investment.getAmount() - investment.getInsuranceAmountRU() - investment.getOptionPlusFee();
//        long bonusAmount = 0;
//        log.debug("settlemnt inv id: " + investment.getId() + " getBonusOddsChangeTypeId " + investment.getBonusOddsChangeTypeId() + " win " + win);
//        if (optionPlusePrice == 0) {
//            if (win) {
//                result = Math.round(invesAmount * (1 + investment.getOddsWin()));
//                netResult = Math.round(invesAmount * investment.getOddsWin());
//            } else {
//                result = Math.round(invesAmount * (1 - investment.getOddsLose()));
//                netResult = Math.round(-invesAmount * investment.getOddsLose());
//            }
//        } else {
//            if (win) {
//                result = optionPlusePrice;
//                netResult = Math.round(optionPlusePrice - invesAmount);
//            } else {
//                result = optionPlusePrice;
//                netResult = Math.round(-1 * (invesAmount - optionPlusePrice));
//            }
//        }
//
//        long bonusTypeId = (investment.getTypeId() != Investment.TYPE_BUBBLE) ? investment.getBonusOddsChangeTypeId() : 0l;
//
//        if (bonusTypeId > 0) {
//            BonusHandlerBase bh = BonusHandlerFactory.getInstance(bonusTypeId);
//            if (null != bh){
//            	try {
//            		InvestmentBonusData ibd = new InvestmentBonusData(investment.getOddsWin(), investment.getOddsLose(), 
//										            				investment.getBonusWinOdds(), investment.getBonusLoseOdds(),
//										            				investment.getId(), investment.getBonusUserId(), 
//										            				investment.getAmount(), investment.getInsuranceAmountRU());
//    				bonusAmount = bh.handleBonusOnSettleInvestment(conn, ibd, win);
//    			} catch (BonusHandlersException e) {
//    				log.error("Error in getBonusAmountOnSettleInvestment for investment " + investmentId, e);
//    			}
//            }
//        }
//
//        log.debug("settlemnt inv id: " + investment.getId() + " bonusAmount " + bonusAmount);
//
//        long winLose = 0;
//
//        User u = new User();
//        UsersDAOBase.getByUserId(conn, investment.getUserId(), u, true);
//        log.info(u);
//
//        //tax should be calculated only for ETRADER users
//        if (CommonUtil.isHebrewSkin(u.getSkinId())) {
//	        winLose = u.getTotalWinLose();
//	        log.info("User :"+u.getId()+" win lose for the year :" +winLose);
//        }
//
//        //in case of a losing "next invest on us" investment the netResult is not added to the W\L
//        boolean isNextInvestOnUs = investment.getBonusOddsChangeTypeId() == 1 && !win;
//        if (!isNextInvestOnUs) {
//        	winLose += netResult - investment.getInsuranceAmountRU() - GMInsuranceAmount + bonusAmount - investment.getOptionPlusFee();
//        } else if (investment.getTypeId() == Investment.TYPE_BUBBLE) {
//        	winLose += netResult - investment.getInsuranceAmountRU() - GMInsuranceAmount + bonusAmount - investment.getOptionPlusFee();
//        }
//
//        long tax = 0;
//        if (winLose > 0) {
////            tax = Math.round(winLose * ApplicationDataBase.getTaxPercentage());
//            // TODO: Tony: find a nice way to avoid hardcoding the tax %
//            tax = Math.round(winLose * ConstantsBase.TAX_PERCENTAGE);
//        }
//
//        if (log.isDebugEnabled()) {
//            String ls = System.getProperty("line.separator");
//            log.debug(ls + "Settle investment: " + investment.getId() + ls +
//                    " win: " + win + ls +
//                    " nextInvestOnUs: " + isNextInvestOnUs + ls +
//                    " voidBet: " + voidBet + ls +
//                    " result: " + result + ls +
//                    " netResult: " + netResult + ls +
//                    " winLose: " + winLose + ls +
//                    " tax: " + tax + ls);
//        }
//
//        investment.setUtcOffsetSettled(u.getUtcOffset());
//        long winC = 0;
//        long loseC = 0;
//        long premiaC = investment.getInsuranceAmountRU() + GMInsuranceAmount + investment.getOptionPlusFee();
//        if (netResult > 0) {  // win
//            winC = netResult;
//            loseC = premiaC;
//        } else if (netResult < 0) { // lose
//            winC = 0;
//            loseC = (-netResult) + premiaC;
//        } else {
//            winC = 0;
//            loseC = premiaC;
//        }
//        investment.setWin(winC);
//        investment.setLose(loseC);
//
//        try {
//            conn.setAutoCommit(false);
//            InvestmentsDAOBase.settleInvestmentUpdateUserBalances(conn, investment.getUserId(), result, tax, winLose);
//            InvestmentsDAOBase.settleInvestment(conn, investment.getUserId(), investmentId, result, voidBet, GMInsuranceAmount, winC, loseC, investment.getClosingLevel(), investment.getTimeQuoted());
//            GeneralDAO.insertBalanceLog(conn, writerId, investment.getUserId(), ConstantsBase.TABLE_INVESTMENTS, investment.getId(), ConstantsBase.LOG_BALANCE_SETTLE_INVESTMENT, u.getUtcOffset());
//            if (investment.getTypeId() != Investment.TYPE_BUBBLE) {
//	            //in case of a "next invest on us" losing investment or (odds change with bonus amount > 0) the user is granted with a bonus.
//	            if (isNextInvestOnUs || (investment.getBonusOddsChangeTypeId() > 1 && bonusAmount > 0)) {
//	            	long amount = isNextInvestOnUs ? -netResult : bonusAmount;
//	                if (log.isInfoEnabled()) {
//	                    log.info("granting bonus. Investment id: " + investment.getId() + " Bonus amount: " + amount);
//	                 }
//	            	grantBonus(conn, investment, writerId, amount, loginId);
//	            }
//            }
//
//            if (investment.getTypeId() != Investment.TYPE_BUBBLE) {
//	            //Bonus Management System (BMS).
//	            ArrayList<BonusFormulaDetails> bonusFormulaD = BonusDAOBase.getBonusAmounts(conn, investmentId, false);
//	            if (!bonusFormulaD.isEmpty()) {
//			        //coefficient -  multiplicative factor for the formula.
//			        //double coefficient = investment.getOddsWin();//user win.
//	            	long coefficient = investment.getAmount() + investment.getWin() - investment.getLose();//settle end time: binary (win & lose), option+ (win & lose), onetouch (win & lose). take profit (before time).
//			        log.debug("BMS, coefficient: " + coefficient + "=" + investment.getAmount() + "+" + investment.getWin() + "-" + investment.getLose());
//
//			        if (GMInsuranceAmount == 0) {//settle before time. not golden
//			        	coefficient = optionPlusePrice; //option+, 0100
//			        	log.debug("BMS, settle before time. not golden, coefficient:" + coefficient);
//			        }
//			        if (investment.getAmount() != 0) {
//				        for (BonusFormulaDetails item : bonusFormulaD) {
//				        	log.debug("\n\nBMS, settle investment formula \n*****************************\n" +
//				        			"" + item.getAdjustedAmount() + "+(" + (double)item.getAmount() + "/" + (double)investment.getAmount() + ")*" + coefficient +
//				        			"=\n (double)item.getAmount() / (double)investment.getAmount() = " + (double)item.getAmount() / (double)investment.getAmount() +
//				        			" ((double)item.getAmount() / (double)investment.getAmount()) * coefficient " + ((double)item.getAmount() / (double)investment.getAmount()) * coefficient +
//				        			" BigDecimal.valueOf(((double)item.getAmount() / (double)investment.getAmount()) * coefficient).setScale(0, RoundingMode.HALF_UP).longValue() " +
//				        			BigDecimal.valueOf(((double)item.getAmount() / (double)investment.getAmount()) * coefficient).setScale(0, RoundingMode.HALF_UP).longValue());
//				        	long adjustedAmount = item.getAdjustedAmount() + BigDecimal.valueOf(((double)item.getAmount() / (double)investment.getAmount()) * coefficient).setScale(0, RoundingMode.HALF_UP).longValue();
//				        	log.debug("\nBMS, adjusted_amount to be update at bonus_users: (id,adjustedAmount)" + "(" + item.getBonusUsersId() + "," + adjustedAmount + ")");
//				        	BonusDAOBase.updateAdjustedAmount(conn, item.getBonusUsersId(), adjustedAmount);
//				        	BonusDAOBase.insertBonusInvestments(conn, investmentId, item.getBonusUsersId(),
//				        			BigDecimal.valueOf(((double)item.getAmount() / (double)investment.getAmount()) * coefficient).setScale(0, RoundingMode.HALF_UP).longValue(),
//				        			adjustedAmount, ConstantsBase.BONUS_INVESTMENTS_TYPE_SETTLED_INV);
//	
//						}
//			        }
//	            }
//            }
//
//            conn.commit();
//        } catch (Exception e) {
//            try {
//                conn.rollback();
//            } catch (Exception ie) {
//                log.error("Can't rollback.", ie);
//            }
//            SQLException sqle = new SQLException();
//            sqle.initCause(e);
//            throw sqle;
//        } finally {
//            try {
//                conn.setAutoCommit(true);
//            } catch (Exception e) {
//                log.error("Can't set back to autocommit.", e);
//            }
//        }
//
//        if((CommonUtil.getProperty("copyop.isEnabled").equalsIgnoreCase("true"))) {
//            log.debug("About to send to copyop:"+investmentId);
//        	if(investment.getOpportunityTypeId() == Opportunity.TYPE_REGULAR) {
//        		if(investment.isLikeHourly()) {
//        			if(u.getSkinId() != Skin.SKIN_ETRADER) {
//        				CopyOpAsyncEventSender.sendEvent(new InvSettleEvent(investment.getUserId(), investmentId, result, investment.getCopyopType(), investment.getMarketId(),
//            					winC, loseC, investment.getClosingLevel(), new Date(), investment.getCopyopInvId(), u.getCurrencyId(), investment.getRate()));
//            			log.debug("Send to copyop:"+investmentId);
//        			}
//        		}
//        	}
//        }
//        
//        // add sms message to db queue if needed
//        // boolean isFreeSms = UsersDAOBase.isFreeSms(conn, investment.getUserId());
//        if (investment.isAcceptedSms()) {
//            log.info("Going to queued sms for invId: " + investment.getId() + ", userId: " + investment.getUserId());
//            String phoneCode = CountryDAOBase.getPhoneCodeById(conn, u.getCountryId());
//            if (null != u.getMobilePhone() && u.getMobilePhone().indexOf("0") == 0) { // leading zero (index 0)
//                u.setMobilePhone(u.getMobilePhone().substring(1));
//            }
//            //from now we will use english for AO and Hebrew for ET
//           /* Skins s = SkinsDAOBase.getById(conn, (int)u.getSkinId());
//            String langCode = LanguagesDAOBase.getCodeById(conn, s.getDefaultLanguageId());
//            if(null == langCode) { // take default
//                langCode = ConstantsBase.LOCALE_DEFAULT;
//            }*/
//            String[] params = new String[3];
//            investment.setCurrencyId(u.getCurrencyId());
//            if (GMInsuranceAmount != 0) { //GM : add premia to the amount
//                investment.setAmount(investment.getAmount() + GMInsuranceAmount);
//            }
////            long providerId = ConstantsBase.UNICELL_PROVIDER_AO;
//            long providerId = ConstantsBase.MOBIVATE_PROVIDER;//ConstantsBase.MBLOX_PROVIDER;
//            String SenderName = "anyoption";
//            String separator = " ";
//            String langCode = ConstantsBase.LOCALE_DEFAULT;
//            if (CommonUtil.isHebrewSkin(u.getSkinId())) {
//            	providerId = ConstantsBase.MOBIVATE_PROVIDER;//UNICELL_PROVIDER_ET;
//            	SenderName = "etrader";
//            	separator = "|";
//            	langCode = ConstantsBase.ETRADER_LOCALE;
//            }
//            String senderNumber = CountryManagerBase.getById(u.getCountryId()).getSupportPhone();
//            ResourceBundle bundle = ResourceBundle.getBundle("MessageResources", new Locale(langCode));
//            String refundTxt = CommonUtil.formatCurrencyAmount(InvestmentsManagerBase.getRefund(investment) + (isNextInvestOnUs ? -netResult : bonusAmount), false, u.getCurrencyId());
//            if (u.getCurrencyId() == ConstantsBase.CURRENCY_ILS_ID|| u.getCurrencyId() == ConstantsBase.CURRENCY_TRY_ID) {
//            	refundTxt = refundTxt + CommonUtil.getMessageShared(CommonUtil.getCurrencySymbol(u.getCurrencyId()), params, bundle);
//            } else {
//            	refundTxt = CommonUtil.getMessageShared(CommonUtil.getCurrencySymbol(u.getCurrencyId()), params, bundle) + refundTxt;
//            }
//            params[0] =  CommonUtil.getMessageShared(investment.getMarketName() + ".short", null, bundle) + separator +
//                            CommonUtil.formatLevelByMarket(investment.getCurrentLevel(), investment.getMarketId(), investment.getDecimalPoint()) + separator +
//                            CommonUtil.getMessageShared(investment.getTypeName(), null, bundle) + separator +
//                            CommonUtil.getTimeFormat(investment.getTimeEstClosing(), investment.getUtcOffsetCreated()) + separator;
//            params[1] = CommonUtil.formatLevelByMarket(investment.getClosingLevel(), investment.getMarketId(), investment.getDecimalPoint());
//            params[2] = refundTxt;
//            String msg = CommonUtil.getMessageShared("sms.settelment.message", params, bundle);
//
//            try {
//                SMSManagerBase.sendTextMessage(SenderName, senderNumber, phoneCode + u.getMobilePhone(), msg, oracleCon, investmentId, SMSManagerBase.SMS_KEY_TYPE_SMS, providerId, SMS.DESCRIPTION_EXPIRY);
//            } catch (SMSException smse) {
//                log.warn("Failed to send SMS.", smse);
//            }
//        }
//    }

    public static boolean cancelInvestment(Connection con, long id, long writer) throws SQLException {
        if (log.isEnabledFor(Level.DEBUG)) {
            String ls = System.getProperty("line.separator");
            log.log(Level.DEBUG, "Cancel Investment: InvestmentId:" + id + ls);
        }

        Investment i = InvestmentsDAOBase.getById(con, id);
        boolean isPayTax = false;

        if (i.getIsCanceled() == 1) {
            return false;
        }
        double taxPercentage = ConstantsBase.TAX_PERCENTAGE;
        if (UsersDAOBase.isTaxExemption(con, i.getUserId())) {
            taxPercentage = 0;
        }

		ArrayList<BonusFormulaDetails> bonusFormulaD = BonusDAOBase.getBonusAmounts(con, id, false);
		long adjustedAmount;

		//reverse bonus wagering
		BonusDAOBase.reverseBonusWagering(con, id, writer, i.getUserId());
				
        if (i.getIsSettled() == 0) { // not settled
            UsersDAOBase.addToBalance(con, i.getUserId(), i.getAmount());
            InvestmentsDAOBase.cancelInvestment(con, id, writer, false);
			if (!bonusFormulaD.isEmpty()) {
				for (BonusFormulaDetails item : bonusFormulaD) {
					log.debug("bonus amount = " + item.getAmount());
					adjustedAmount = item.getAdjustedAmount() + item.getAmount();
					log.debug("new adjustedAmount = " + adjustedAmount);
					BonusDAOBase.updateAdjustedAmount(con, item.getBonusUsersId(), adjustedAmount);
				}
			}
        } else {   // investment already settled

            /*
             *  check if we can cancel the investment.
             *  if settle-date done in period1(mid-year) and we take tax then we cannot cancel
            */

            // find the year(text format) of the investment
            if (CommonUtil.isHebrewSkin(Long.parseLong(i.getSkin()))) {
                Calendar cal = GregorianCalendar.getInstance();
                cal.setTime(i.getTimeSettled());
                long investmentYear = new Long(cal.get(Calendar.YEAR));
                boolean isSettleInOnePeriod = InvestmentsDAOBase.isSettleInMidYear(con, id, String.valueOf(investmentYear));
                long settleYear = Long.valueOf(Long.valueOf(investmentYear));

                isPayTax = TaxHistoryDAOBase.isPayMidYearTax(con, i.getUserId(), settleYear);
                if (isSettleInOnePeriod) {
                    if (isPayTax) { // user pay tax and investment settle befor period1 job
                        if (log.isEnabledFor(Level.WARN)) {
                            String ls = System.getProperty("line.separator");
                            log.warn("Error With Cancel Investment, Tax already taken For Mid-Year" + ls);
                        }
                        return false;
                    }
                }
            }

            //cancel investment
            InvestmentsDAOBase.cancelInvestment(con, id, writer, true);

            //amount to refund (to us or to user)
            long amount=0;
            if (i.getWin()>0) {
                amount = -(i.getWin() - i.getLose()) ;
            } else {
                amount = i.getLose();
            }

            //update balance with win\lose
            if (i.getIsVoid()==0) { //for voided investments only the status will be updated
                UsersDAOBase.addToBalance(con, i.getUserId(), amount);
            }

			//update bonus user Adjusted Amount
			if (!bonusFormulaD.isEmpty()) {
				for (BonusFormulaDetails item : bonusFormulaD) {
					log.debug("bonus amount = " + item.getAmount() +
							" (double)(i.getWin() - i.getLose()) " + (double)(i.getWin() - i.getLose()) +
							" ((double)item.getAmount() * (double)(i.getWin() - i.getLose())) = " + ((double)item.getAmount() * (double)(i.getWin() - i.getLose())) +
							" ((double)item.getAmount() * (double)(i.getWin() - i.getLose()))/(double)i.getAmount() = " + ((double)item.getAmount() * (double)(i.getWin() - i.getLose()))/i.getAmount() +
							" BigDecimal.valueOf(((double)item.getAmount() * (double)(i.getWin() - i.getLose()))/(double)i.getAmount()).setScale(0, RoundingMode.HALF_UP).longValue() " + BigDecimal.valueOf(((double)item.getAmount() * (double)(i.getWin() - i.getLose()))/i.getAmount()).setScale(0, RoundingMode.HALF_UP).longValue());
					adjustedAmount = item.getAdjustedAmount() - BigDecimal.valueOf(((double)item.getAmount() * (double)(i.getWin() - i.getLose()))/(double)i.getAmount()).setScale(0, RoundingMode.HALF_UP).longValue();
					BonusDAOBase.updateAdjustedAmount(con, item.getBonusUsersId(), adjustedAmount);
				}
			}

            if (CommonUtil.isHebrewSkin(Long.parseLong(i.getSkin()))) {
                //update balance with tax
                long currentTax = UsersDAOBase.getTaxBalance(con, i.getUserId());
                UsersDAOBase.addToBalance(con, i.getUserId(), currentTax);

                String utcOffsetET = ConstantsBase.UTCOFFSET_DEFAULT_PERIOD2;
                if (CommonUtil.isHebrewSkin(Long.parseLong(i.getSkin()))) {
                    TimeZone tzEt = TimeZone.getTimeZone("Israel");
                    utcOffsetET = ConstantsBase.UTCOFFSET_DEFAULT_PERIOD2;
                    if (tzEt.inDaylightTime(new Date())) {
                        utcOffsetET= ConstantsBase.UTCOFFSET_DEFAULT_PERIOD1;
                    }
                }
                //recalculate tax (excluding this cancelled investment)
                long winLose = InvestmentsDAOBase.getTotalWinLoseForTheYear(con, i.getUserId(), isPayTax, utcOffsetET);
                long newTax = 0;
                if (winLose > 0) {
                    newTax = Math.round(winLose * taxPercentage);
                }

                //update new tax
                UsersDAOBase.updateTax(con, i.getUserId(), newTax);

                //upldate balance with new tax (deduct previous added tax)
                UsersDAOBase.addToBalance(con, i.getUserId(), -newTax);
            }
        }

        //update balance history
        String table = ConstantsBase.TABLE_INVESTMENTS;
        int command = ConstantsBase.LOG_BALANCE_CANCEL_INVESTMENT;
        String utcoffset = UsersDAOBase.getUtcOffset(con, i.getUserId());
        GeneralDAO.insertBalanceLog(con,writer,i.getUserId(), table, i.getId(), command, utcoffset);

        // Loyalty reverse points
        long investPoints = TiersDAOBase.getInvetPointsByInvestmentId(con, i.getId());
        if (investPoints > 0) {
            log.debug("Reverse LoyaltyPoints, investPoints: " +investPoints);
            TierUser tierU = TiersDAOBase.getTierUserByUserId(con, i.getUserId());
            long updatedPoints = tierU.getPoints() - investPoints;
            log.debug("Going to update tierUser points, currentPoints: " + tierU.getPoints() + ", " +
                        "UpdatedPoints: " + updatedPoints);
            TiersManagerBase.updateTierUser(con, tierU.getId(), writer, updatedPoints);

            TierUserHistory h = TiersManagerBase.getTierUserHisIns(tierU.getTierId(), i.getUserId(), TiersManagerBase.TIER_ACTION_REVERSE_POINTS,
                                    writer, updatedPoints, -investPoints, i.getId(), ConstantsBase.TABLE_INVESTMENTS, utcoffset);
            log.debug("Going to insert reverse action, " + h.toString());
            TiersDAOBase.insertIntoTierHistory(con, h);
        }

        //add log
        if (log.isEnabledFor(Level.DEBUG)) {
            log.log(Level.DEBUG, "Cancel Investment finished successfully.");
        }
        return true;
    }

    /**
     * get investment to settle.
     *
     * @param investmentId the id of the investment to settle.
     * @param oppSettled true if to bring the investment only if the opp setteld, false to bring it also if its not settled (use in 5 gold minutes)
     * @param isGM if its take profit (GM) true else false
     * @return the investment
     * @throws SQLException
     */
    public static Investment getInvestmentToSettle(long investmentId, boolean oppSettled, boolean isGM) throws SQLException {
        Connection conn = null;
        Investment investment = null;
        try {
            conn = getConnection();
            investment = InvestmentsDAOBase.getInvestmentToSettle(conn, investmentId, oppSettled, isGM);
        } finally {
            closeConnection(conn);
        }
        return investment;
    }

    public static double getOptionsPlusePrice(double promil, double minPass, long marketId) throws SQLException {
        Connection conn = null;
        double price = 0;
        try {
            conn = getConnection();
            price = InvestmentsDAOBase.getOptionsPlusePrice(conn, promil, minPass, marketId);
        } finally {
            closeConnection(conn);
        }

        return price;
    }

    public static double calcOptionsPlusePrice(double promil, double minPass, long marketId, long userId, long investmentId, Date timeQuoted, BigDecimal invAmount, double level) throws SQLException {
        Connection conn = null;
        double price = 0;
        try {
            conn = getConnection();
            price = InvestmentsDAOBase.calcOptionsPlusePrice(conn, promil, minPass, marketId, userId, investmentId, timeQuoted, invAmount,  level);
        } finally {
            closeConnection(conn);
        }

        return price;
    }

    public static OptionPlusQuote getLastQuote(long userId, long investmentId, double price) throws SQLException {
        Connection conn = getConnection();
        try {
            return InvestmentsDAOBase.getLastQuote(conn, userId, investmentId, price);
        } finally {
            closeConnection(conn);
        }
    }

	public static void updatePurchasedQuote(long quoteId) throws SQLException {
        Connection conn = getConnection();
        try {
            InvestmentsDAOBase.updatePurchasedQuote(conn, quoteId);
        } finally {
            closeConnection(conn);
        }
	}
//
//    public static ArrayList<Investment> getOpenedInvestmentsByUser(long userId, long opportunityId, long typeId) throws SQLException {
//        ArrayList<Investment> l = null;
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            l = InvestmentsDAOBase.getOpenedByUser(conn, userId, opportunityId, typeId);
//        } finally {
//            closeConnection(conn);
//        }
//        return l;
//    }
//
//    public static void grantBonus(Connection con, Investment investment, long writerId, long amount, long loginId) throws SQLException {
//    	//get transaction
//    	Transaction bonus = getBonusTransaction(investment, amount, loginId);
//
//    	//add bonus transaction to user
//		TransactionsDAOBase.insertFromService(con, bonus);
//		if (log.isEnabledFor(Level.INFO)) {
//			String ls = System.getProperty("line.separator");
//			log.log(Level.INFO, "Bonus transaction inserted. TransactionId: " + bonus.getId() + " Amount: " + bonus.getAmount() + ls);
//		}
//
//		//update balance
//		UsersDAOBase.addToBalance(con, bonus.getUserId(), bonus.getAmount());
//
//		//update balance history
//		String table = ConstantsBase.TABLE_INVESTMENTS;
//		int command = ConstantsBase.LOG_BALANCE_BONUS_DEPOSIT;
//		GeneralDAO.insertBalanceLog(con, writerId, investment.getUserId(), table, investment.getId(), command, bonus.getUtcOffsetCreated());
//
//		//update log
//		String tlog = "Bonus was granted to User.";
//		if (log.isEnabledFor(Level.DEBUG)) {
//			String ls = System.getProperty("line.separator");
//			log.log(Level.DEBUG, tlog + ls);
//		}
//    }

//    /**
//	 * This method receives an investment and create a bonus transaction according to that investment.
//	 * @param i - the investment to create a transaction (towards the customer) upon
//	 * @return
//	 */
//	private static Transaction getBonusTransaction(Investment i, long amount, long loginId) {
//		Transaction bonus = new Transaction();
//		bonus.setUserId(i.getUserId());
//		bonus.setAmount(amount);
//		bonus.setTypeId(TransactionsManagerBase.TRANS_TYPE_BONUS_DEPOSIT);
//		bonus.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
//		bonus.setCreditCardId(null);
//		bonus.setComments("investment id: " + i.getId());
//		bonus.setDescription("log.balance.bonus.deposit");
//		bonus.setIp("IP NOT FOUND!");
//		bonus.setChequeId(null);
//		bonus.setTimeSettled(new Date());
//		bonus.setTimeCreated(new Date());
//		bonus.setUtcOffsetCreated(i.getUtcOffsetSettled());
//		bonus.setUtcOffsetSettled(i.getUtcOffsetSettled());
//		bonus.setWriterId(i.getWriterId());
//		bonus.setWireId(null);
//		bonus.setChargeBackId(null);
//		bonus.setBonusUserId(i.getBonusUserId());
//		bonus.setLoginId(loginId);
//		return bonus;
//	}

	/**
     * Get details for an opportunity that is currently running.
     *
     * @param id the id of the opportunity
     * @return <code>Opportunity</code> with the requested details
     * @throws SQLException
     */
    public static Opportunity getRunningOpportunityById(long id) throws SQLException {
        Opportunity o = null;
        Connection conn = null;
        try {
            conn = getConnection();
            o = OpportunitiesDAOBase.getRunningById(conn, id);
        } finally {
            closeConnection(conn);
        }
        return o;
    }

    public static Investment getInvestmentById(long id, int investmentLimit) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            List<Long> list = new ArrayList<Long>();
            list.add(id);
            List<Investment> res = InvestmentsDAOBase.getDetails(conn, list, investmentLimit);
            if (res.isEmpty()) {
            	return null;
            }
            return res.get(0);
        } finally {
            closeConnection(conn);
        }
    }

	public static List<Investment> getInvestmentsById(List<Long> ids, int investmentLimit) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			List<Investment> res = InvestmentsDAOBase.getDetails(conn, ids, investmentLimit);
			if (res.isEmpty()) {
				return new ArrayList<Investment>();
			}
			return res;
		} finally {
			closeConnection(conn);
		}
	}

    public static MessageToFormat validateBuyInsurance(User user, long invesmtnetId, int insuranceType, long insuranceAmount, OpportunityCache oc, GoldenMinuteType gmType) throws SQLException {
		long insuranceStartTime = Long.valueOf(CommonUtil.getEnum("insurance_period_start_time", "insurance_period_start_time")) * 60000;
		long insuranceEndTime = Long.valueOf(CommonUtil.getEnum("insurance_period_end_time", "insurance_period_end_time")) * 60000;
		if(gmType == GoldenMinuteType.ADDITIONAL) {
			insuranceStartTime = Long.valueOf(CommonUtil.getEnum("insurance_period_2_start_time", "insurance_period_2_start_time")) * 60000;
			insuranceEndTime = Long.valueOf(CommonUtil.getEnum("insurance_period_2_end_time", "insurance_period_2_end_time")) * 60000;
		}
    	
        boolean isGM = insuranceType == Investment.INSURANCE_TAKE_PROFIT;
        Investment investment = InvestmentsManagerBase.getInvestmentToSettle(invesmtnetId, false, isGM);
        if (null == investment) {
            log.info("trying to buy insurance for settled investment! investment id: " + invesmtnetId);
            return new MessageToFormat("error.investment", null);
        }
        Connection conn = getConnection();
        try {
        	UsersDAOBase.getByUserName(conn, user.getUserName(), user); // actually take the current balance
            if (user.getBalance() < insuranceAmount) {
                log.debug("Not enough money. Balance is: " + user.getBalance());
                return new MessageToFormat("error.investment.nomoney", null);
            }
            OpportunityMiniBean o = OpportunitiesDAOBase.getOpportunityMiniBean(conn, investment.getOpportunityId());
            OpportunityCacheBean oppCacheBean = oc.getOpportunity(investment.getOpportunityId());
            if (o.isDisabled() || o.isMarketSuspended()) {
                log.debug("Market disabled or suspended.");
                return new MessageToFormat("error.investment.disabled", null);
            }
            if (System.currentTimeMillis() < oppCacheBean.getTimeFirstInvest().getTime()) {
                log.debug("Opportunity not opened.");
                return new MessageToFormat("error.investment.notopened", null);
            }

            if (System.currentTimeMillis() < oppCacheBean.getTimeEstClosing().getTime() - insuranceStartTime) {
                log.debug("insurance buy time not started.");
                return new MessageToFormat("error.investment.notopened", null);
            }

            long insuranceToleranceDelaySeconds = Long.valueOf(CommonUtil.getEnum("insurance_tolerance_delay_seconds", "insurance_tolerance_delay_seconds")) * 1000;
            if (System.currentTimeMillis() > oppCacheBean.getTimeEstClosing().getTime() - insuranceEndTime + insuranceToleranceDelaySeconds) {
                log.debug("insurance time Expired.");
                return new MessageToFormat("error.investment.expired", null);
            }
        } finally {
            closeConnection(conn);
        }

        double insurancePremiaPercen = (insuranceType == Investment.INSURANCE_TAKE_PROFIT ? investment.getInsurancePremiaPercent() : investment.getRollUpPremiaPercent());
        long dbInsuranceAmount = Math.round(getAmountWithoutFees(investment) * insurancePremiaPercen);
        log.debug("insurancePremiaPercen=" + insurancePremiaPercen + "  investment.getAmount()=" + investment.getAmount() + " dbInsuranceAmount=" + dbInsuranceAmount);
        // check that page insurance amount is right
        if (dbInsuranceAmount != insuranceAmount) {
            log.info("page insurance amount: " + insuranceAmount + " not equle to db insurance amount: " + dbInsuranceAmount);
            return new MessageToFormat("golden.error.wrong.amount", null);
        }
        if (insuranceType == Investment.INSURANCE_ROLL_FORWARD && null == OpportunitiesManagerBase.getNextHourlyOppToOpenByOppId(investment.getOpportunityId())) {
            log.info("Next opportunity is null");
            return new MessageToFormat("error.investment", null);
        }
        return null;
    }

    /**
     * Buy Take Profit/Roll Forward insurance.
     *
     * @param user
     * @param invesmtnetId
     * @param insuranceType
     * @param insuranceAmount
     * @param writerId
     * @return The amount in cents credited to the user balance (only for Take Profit).
     * @throws SQLException
     */
    public static long buyInsurance(User user, long invesmtnetId, int insuranceType, long insuranceAmount, long writerId, WebLevelsCache levelsCache, long loginId) throws SQLException {
        boolean isGM = insuranceType == Investment.INSURANCE_TAKE_PROFIT;
        Investment investment = InvestmentsManagerBase.getInvestmentToSettle(invesmtnetId, false, isGM);
        double convertAmount =  investment.getAmount() * investment.getRate();
        if (insuranceType == Investment.INSURANCE_TAKE_PROFIT) {
            log.info("buy take profit insurance for user: " + user.getUserName() + " invId: " + invesmtnetId + " insuranceAmount: " + insuranceAmount + " insurance type: " + insuranceType);
            buyTakeProfitInsurance(user, investment, insuranceAmount, writerId, loginId);
        	if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
        		CommonUtil.sendNotifyForInvestment(investment.getOpportunityId(),
        											investment.getId(),
        											(-1d) * convertAmount,
        											(-1L) * investment.getAmount(),
        											investment.getTypeId(),
        											investment.getMarketId(),
        											OpportunityType.PRODUCT_TYPE_BINARY,
        											investment.getCurrentLevel(),
        											levelsCache,
        											user,
        											investment.getCopyopType());
            }
            return Math.round(investment.getAmount() * (1 + investment.getOddsWin()));
        } else {
            Opportunity nextOpp = OpportunitiesManagerBase.getNextHourlyOppToOpenByOppId(investment.getOpportunityId());
            long nextOppId = nextOpp.getId();
            log.info("buy roll forward insurance for user: " + user.getUserName() + " invId: " + invesmtnetId + " insuranceAmount: " + insuranceAmount + " insurance type: " + insuranceType);
            buyRollForwardInsurance(user, investment, insuranceAmount, nextOppId, writerId, loginId);
            if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
            	//add the amount to the next opp
        		CommonUtil.sendNotifyForInvestment(nextOppId,
        											investment.getId(),
        											convertAmount,
        											investment.getAmount(),
        											investment.getTypeId(),
        											investment.getMarketId(),
        											OpportunityType.PRODUCT_TYPE_BINARY,
        											investment.getCurrentLevel(),
        											levelsCache,
        											user,
        											investment.getCopyopType());
            	//TODO: move this line to the cancel investment like we have in the web
            	//remove the amount from the current opp
        		CommonUtil.sendNotifyForInvestment(investment.getOpportunityId(),
        											investment.getId(),
        											(-1d) * convertAmount,
        											(-1L) * investment.getAmount(),
        											investment.getTypeId(),
        											investment.getMarketId(),
        											OpportunityType.PRODUCT_TYPE_BINARY,
        											investment.getCurrentLevel(),
        											levelsCache,
        											user,
        											investment.getCopyopType());
            }
            return 0;
        }
    }

    /**
     * Buy Take Profit insurance.
     *
     * @param user
     * @param inv
     * @param insuranceAmount
     * @param writerId
     * @throws SQLException
     */
    public static void buyTakeProfitInsurance(User user, Investment inv, long insuranceAmount, long writerId, long loginId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            user.setBalance(user.getBalance() - insuranceAmount);
            UsersDAOBase.addToBalance(conn, user.getId(), -insuranceAmount);
            InvestmentsManagerBase.settleInvestment(conn, inv.getId(), writerId, insuranceAmount, inv, 0, ConstantsBase.LOG_BALANCE_SETTLE_INVESTMENT, loginId);
            conn.commit();
        } catch (SQLException e) {
            log.error("Exception in bought GoldenMinutes Investment! ", e);
            try {
                conn.rollback();
            } catch (Throwable it) {
                log.error("Can't rollback.", it);
            }
            throw e;
        } finally {
            try {
                conn.setAutoCommit(true);
            } catch (Exception e) {
                log.error("Can't set back to autocommit.", e);
            }
            closeConnection(conn);
        }
        try {
        	afterInvestmentSuccess(user, user.getBalance(), inv.getId(), insuranceAmount, writerId, false, inv.getOpportunityTypeId(), inv.getOptionPlusFee(), ConstantsBase.INSURANCE_GOLDEN_MINUTES, loginId);
        } catch (SQLException e){
        	log.error("BMS, Error! cannot run afterInvestmentSuccess after bought Golden Minutes Investment ", e);
        }
    }

    /**
     * Buy Roll Forward insurance.
     *
     * @param user
     * @param inv
     * @param amount
     * @param nextOppId
     * @return The new investment id.
     * @throws Exception
     */
    public static long buyRollForwardInsurance(User user, Investment inv, long amount, long nextOppId, long writerId, long loginId) throws SQLException {
        Connection conn = null;
        long id = 0;
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            InvestmentsManagerBase.cancelInvestment(conn, inv.getId(), writerId);
            long insuranceAmountTotal = inv.getInsuranceAmountRU() + amount;
            long invAmount = inv.getAmount() + amount;
            UsersDAOBase.getByUserName(conn, user.getUserName(), user); // actually take the current balance
            user.setBalance(user.getBalance() - invAmount);
            UsersDAOBase.addToBalance(conn, user.getId(), -invAmount);
            id = InvestmentsDAOBase.insertInvestment(conn, user.getId(), nextOppId, inv.getTypeId(), invAmount, inv.getCurrentLevelValue(), inv.getIp(), inv.getRealLevel(),
            		inv.getWwwLevel().doubleValue(), inv.getBonusOddsChangeTypeId(), user.getUtcOffset(), inv.getRate(), inv.getBonusUserId(), inv.getOddsWin(),
            		inv.getOddsLose(), inv.getId(), insuranceAmountTotal, false, 0, writerId, user.getCountryId(), inv.getApiExternalUserId(), inv.getDefaultAmountValue(),
            		0l, 0l, inv.isLikeHourly(), inv.getBubbleStartTime(), inv.getBubbleEndTime(), inv.getBubbleLowLevel(), inv.getBubbleHighLevel(), loginId, inv.getPrice());
            if (inv.isAcceptedSms()) { //need to update the sms column
                InvestmentsDAOBase.updateAcceptedSmsById(conn, id);
            }
            GeneralDAO.insertBalanceLog(conn, writerId, user.getId(), ConstantsBase.TABLE_INVESTMENTS, id, ConstantsBase.LOG_BALANCE_INSERT_INVESTMENT, user.getUtcOffset());
            afterInvestmentSuccess(conn, user.getId(), user.getBalance(), id, amount, writerId, false, inv.getOpportunityTypeId(), loginId);
            conn.commit();
        } catch (SQLException e) {
            log.error("Exception in roll up investment! ", e);
            try {
                conn.rollback();
            } catch (Throwable it) {
                log.error("Can't rollback.", it);
            }
            throw e;
        } finally {
            try {
                conn.setAutoCommit(true);
            } catch (Exception e) {
                log.error("Can't set back to autocommit.", e);
            }
            closeConnection(conn);
        }
        try {
        	log.debug("\nBMS,in buyRollForwardInsurance, before insertBonusInvestment, userBalance: " + user.getBalance());
            insertBonusInvestment(id, user.getId(), user.getBalance(), amount);
        } catch (Exception eb) {
            log.error("BMS, Problem processing bonus investments.", eb);
        }
        return id;
    }

    /**
     * check if the user already saw the current banner (if he saw it in less then 5 min ago)
     * @param id - user id
     * @return
     */
    public static boolean isGMBannerViewed(long id) throws SQLException {
        Connection conn = null;
        boolean isViewed = false;
        try {
            conn = getConnection();
            isViewed = InvestmentsDAOBase.isGMBannerViewed(conn, id);
        } finally {
            closeConnection(conn);
        }
        return isViewed;
    }

    public static boolean insertGMBannerViewed(long id) throws SQLException {
        Connection conn = null;
        boolean isUpdate = false;
        try {
            conn = getConnection();
            isUpdate = InvestmentsDAOBase.insertGMBannerViewed(conn, id);
        } finally {
            closeConnection(conn);
        }
        return isUpdate;

    }
	
	public static double getTurnoverFactor(int opportunityTypeId) throws SQLException {
	    Connection conn = null;
	    double factor = 0;
	    try {
		conn = getConnection();
		factor = com.anyoption.common.daos.InvestmentsDAOBase.getTurnoverFactor(conn, opportunityTypeId) * 100;
	    } finally {
		closeConnection(conn);
	    }
	    return factor;
	}
	public static long getUserMinInvestmentLimit(long currencyId) throws SQLException {
	    Connection con = getConnection();
	    try {
		return InvestmentsDAOBase.getUserMinInvestmentLimit(con, currencyId);
	    } finally {
		closeConnection(con);
	    }
	}

	public static InvestmentLimit getBDAInvestmentLimit(long curencyId) {
		Connection con = null;
        try {
        	Connection conn = getConnection();
            return LimitsDAOBase.getBDAInvestmentLimit(conn, curencyId);
        } catch(SQLException e) {
        	log.debug("Cannot load bda limits", e);
        	return null;
        } finally {
            closeConnection(con);
        }
	}
}