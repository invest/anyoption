package com.anyoption.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import org.jfree.util.Log;

import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.Skin;
import com.anyoption.common.beans.SkinCurrency;
import com.anyoption.common.beans.SkinLanguage;
import com.anyoption.common.beans.TreeItem;
import com.anyoption.common.bl_vos.BankBase;
import com.anyoption.common.daos.BanksDAOBase;
import com.anyoption.daos.SkinCurrenciesDAOBase;
import com.anyoption.daos.SkinLanguagesDAOBase;
import com.anyoption.daos.SkinsDAOBase;

public class SkinsManagerBase extends com.anyoption.common.managers.SkinsManagerBase {

    /**
     * Get skinCurrencies list by skinId
     * @param skinId skinId for getting currencies list
     * @return ArrayList<SkinCurrency>
     * @throws SQLException
     */
    public static ArrayList<SkinCurrency> getCurrenciesBySkinId(long skinId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return SkinCurrenciesDAOBase.getAllBySkin(conn, skinId);
        } finally {
            closeConnection(conn);
        }
    }

    /**
     * Get skinLanguages list by skinId
     * @param skinId skinId for getting languages list
     * @return ArrayList<SkinLanguage>
     * @throws SQLException
     */
    public static ArrayList<SkinLanguage> getLanguagesBySkinId(long skinId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return SkinLanguagesDAOBase.getAllBySkin(conn, skinId);
        } finally {
            closeConnection(conn);
        }
    }

    /**
     * Get skin_id and is_regulated by country_id and url_id
     * 
     * @param con connection to Db
     * @param countryId country id
     * @param urlId url id
     * @return skin id
     * @throws SQLException
     */
	 public static ArrayList<String> getSkinIdIsRegulatedByCountryAndUrl(long countryId, int urlId) throws SQLException {
	     Connection conn = getConnection();
	     try {
	         return SkinsDAOBase.getSkinIdIsRegulatedByCountryAndUrl(conn, countryId, urlId);
	     } finally {
	         closeConnection(conn);
	     }
	 } 

    public static ArrayList<SkinCurrency> getSkinCurrencies(long skinId) {
    	getSkins();
    	if (null == skins.get(skinId).getSkinCurrenciesList()) {
    		try {
    			((Skin)skins.get(skinId)).setSkinCurrenciesList(SkinsManagerBase.getCurrenciesBySkinId(skinId));
    		} catch (SQLException sqle) {
    			Log.error("Can't load skinCurrencies.", sqle);
    		}
    	}
    	return skins.get(skinId).getSkinCurrenciesList();
    }

    public static ArrayList<SkinLanguage> getSkinLanguages(long skinId) {
    	getSkins();
    	if (null == skins.get(skinId).getSkinLanguagesList()) {
    		try {
    			((Skin)skins.get(skinId)).setSkinLanguagesList(SkinsManagerBase.getLanguagesBySkinId(skinId));
    		} catch (SQLException sqle) {
    			Log.error("Can't load skinCurrencies.", sqle);
    		}
    	}
    	return skins.get(skinId).getSkinLanguagesList();
    }

//    /**
//     * Get tree items list (markets groups list) by skinId
//     */
//    public static ArrayList<TreeItem> getTreeItems(long skinId) {
//    	getSkins();
//    	if (null == skins.get(skinId).getTreeItems()) {
//    		try {
//    			((Skin)skins.get(skinId)).setTreeItems(MarketsManagerBase.getTreeItemsAnyoption(skinId));
//    		} catch (SQLException sqle) {
//    			Log.error("Can't load skinTreeItems.", sqle);
//    		}
//    	}
//    	return skins.get(skinId).getTreeItems();
//    }

//    /**
//     * Get market groups with markets by skinId
//     */
//    public static Hashtable<Long, MarketGroup> getSkinGroupsMarkets(long skinId) {
//    	getSkins();
//    	if (null == skins.get(skinId).getSkinGroupsMarkets()) {
//    		try {
//    			((Skin)skins.get(skinId)).setSkinGroupsMarkets(MarketsManagerBase.getSkinGroupsMarkets(skinId));
//    		} catch (SQLException sqle) {
//    			Log.error("Can't load getSkinGroupsMarkets.", sqle);
//    		}
//    	}
//    	return skins.get(skinId).getSkinGroupsMarkets();
//    }
//    

	/**
     * Get market groups with markets by skinId
     */
    public static ArrayList<com.anyoption.common.beans.base.Market> getSkinMarketsListSorted(long skinId) {
    	getSkins();
    	if (null == skins.get(skinId).getSkinMarketsListSorted()) {
    		try {
    			((Skin)skins.get(skinId)).setSkinMarketsListSorted(MarketsManagerBase.getSkinMarketsListSorted(skinId, 0));
    		} catch (SQLException sqle) {
    			Log.error("Can't load getSkinGroupsMarkets.", sqle);
    		}
    	}
    	return skins.get(skinId).getSkinMarketsListSorted();
    }
    
    /**
     * Get market groups with markets by skinId
     */
    public static ArrayList<com.anyoption.common.beans.base.Market> getSkinMarketsListSorted(long skinId, long marketTypeId) {
    	getSkins();
    	if (marketTypeId == 0) {
    		return getSkinMarketsListSorted(skinId);
    	}
    	if (null == skins.get(skinId).getSkinMarketsByTypeListSorted().get(marketTypeId)) {
    		try {
    			((Skin)skins.get(skinId)).getSkinMarketsByTypeListSorted().put(marketTypeId, (MarketsManagerBase.getSkinMarketsListSorted(skinId, marketTypeId)));
    		} catch (SQLException sqle) {
    			Log.error("Can't load getSkinGroupsMarkets.", sqle);
    		}
    	}
    	return skins.get(skinId).getSkinMarketsByTypeListSorted().get(marketTypeId);
    }
    
    /**
     * @param skinId
     * @return support email by requested skin.
     */
    public static String getSupportEmailBySkinId(long skinId) {	
    	String supportEmailString = "";	
		supportEmailString = getSkins().get((long)Skin.SKINS_DEFAULT).getSupportEmail();
		if (skinId > 0) {
			try {
				supportEmailString = getSkins().get(skinId).getSupportEmail();
			} catch (Exception e) {
				Log.error("ERROR! try to get support email by skin", e);
			}
		} 
		return supportEmailString;		
	}   
    
	public static long getRegulatedSkinIdByNonRegulatedSkinId(long skinId) {
		if (skinId == Skin.SKIN_ENGLISH) {
			return Skin.SKIN_REG_EN;
		} else if (skinId == Skin.SKIN_SPAIN) {
			return Skin.SKIN_REG_ES;
		} else if (skinId == Skin.SKIN_GERMAN){
			return Skin.SKIN_REG_DE;
		} else if (skinId == Skin.SKIN_ITALIAN){
			return Skin.SKIN_REG_IT;
		} else if (skinId == Skin.SKIN_FRANCE){
			return Skin.SKIN_REG_FR;
		} else {
			return skinId;
		}
	}
	
	public static long getNonRegulatedSkinIdByRegulatedSkinId(long skinId) {
		if (skinId == Skin.SKIN_REG_EN) {
			return Skin.SKIN_ENGLISH;
		} else if (skinId == Skin.SKIN_REG_ES) {
			return Skin.SKIN_SPAIN;
		} else if (skinId == Skin.SKIN_REG_DE){
			return Skin.SKIN_GERMAN;
		} else if (skinId == Skin.SKIN_REG_IT){
			return Skin.SKIN_ITALIAN;
		} else if (skinId == Skin.SKIN_REG_FR){
			return Skin.SKIN_FRANCE;
		} else {
			return skinId;
		}
	}
	
	/**
	 * get skin currencies by skinId
	 * @param skinId
	 * @return skinCurrenciesHM as HashMap
	 */
	public static HashMap<Long, SkinCurrency> getSkinCurrenciesHM(long skinId) {
    	getSkins();
    	if (null == skins.get(skinId).getSkinCurrenciesHM()) {
    		try {
    			ArrayList<SkinCurrency> scList = SkinsManagerBase.getCurrenciesBySkinId(skinId);
    			HashMap<Long, SkinCurrency> skinCurrencies = new HashMap<Long, SkinCurrency>();
            	for (SkinCurrency sc : scList) {
            		skinCurrencies.put(sc.getCurrencyId(), sc);
            	}
            	((Skin)skins.get(skinId)).setSkinCurrenciesHM(skinCurrencies);
    		} catch (SQLException sqle) {
    			Log.error("Can't load skinCurrencies.", sqle);
    		}
    	}
    	return skins.get(skinId).getSkinCurrenciesHM();
    }
	

	/**
	 * Get HashTable of hourly binary markets for skin
	 * @param skinId
	 * @return
	 */
	public static Hashtable<Long, Market> getHourlyBinaryMarketsForSkinHM(long skinId) {
    	getSkins();
    	if (null == skins.get(skinId).getHourlyBinaryMarketsHM()) {
    		try {
    			((Skin)skins.get(skinId)).setHourlyBinaryMarketsHM(SkinsManagerBase.getMarketsForSkinByOppType(skinId, Opportunity.TYPE_REGULAR, true)) ;
    		} catch (SQLException sqle) {
    			Log.error("Can't load getHourlyBinaryMarketsForSkinHM.", sqle);
    		}
    	}
    	return skins.get(skinId).getHourlyBinaryMarketsHM();  
    }
	
	/**
	 * Get HashTable of all binary markets for skin
	 * @param skinId
	 * @return
	 */
//	public static Hashtable<Long, Market> getAllBinaryMarketsForSkinHM(long skinId) {
//    	getSkins();
//    	if (null == skins.get(skinId).getAllBinaryMarketsHM()) {
//    		try {
//    			((Skin)skins.get(skinId)).setAllBinaryMarketsHM(SkinsManagerBase.getMarketsForSkinByOppType(skinId, Opportunity.TYPE_REGULAR, false)) ;
//    		} catch (SQLException sqle) {
//    			Log.error("Can't load getAllBinaryMarketsForSkinHM.", sqle);
//    		}
//    	}
//    	return skins.get(skinId).getAllBinaryMarketsHM();  
//    }

	/**
     * Get getNetreferCombinationsId by skinId
     * @param skinId skinId for getting getNetreferCombinationsId list
     * @return ArrayList<Long>
     * @throws SQLException
     */
    private static ArrayList<Long> getNetreferCombinationsId(long skinId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return SkinsDAOBase.getNetreferCombinationsId(conn, skinId);
        } finally {
            closeConnection(conn);
        }
    }

    /**
     * Get getReferpartnerCombinationsId by skinId
     * @param skinId skinId for getting getReferpartnerCombinationsId list
     * @return ArrayList<Long>
     * @throws SQLException
     */
    private static ArrayList<Long> getReferpartnerCombinationsId(long skinId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return SkinsDAOBase.getReferpartnerCombinationsId(conn, skinId);
        } finally {
            closeConnection(conn);
        }
    }
    
	/**
	 * get skin Netrefer Combinations by skinId
	 * @param skinId
	 * @return NetreferCombinations as ArrayList<Long>
	 */
	public static ArrayList<Long> getNetreferCombinationsIdList(long skinId) {
    	getSkins();
    	if (null == skins.get(skinId).getNetreferCombinationId()) {
    		try {
    			ArrayList<Long> NetreferCombinations = SkinsManagerBase.getNetreferCombinationsId(skinId);
            	((Skin)skins.get(skinId)).setNetreferCombinationId(NetreferCombinations);
    		} catch (SQLException sqle) {
    			Log.error("Can't load NetreferCombinations.", sqle);
    		}
    	}
    	return skins.get(skinId).getNetreferCombinationId();
    }
	
	/**
	 * get skin Referpartner Combinations by skinId
	 * @param skinId
	 * @return NetreferCombinations as ArrayList<Long>
	 */
	public static ArrayList<Long> getReferpartnerCombinationsIdList(long skinId) {
    	getSkins();
    	if (null == skins.get(skinId).getReferpartnerCombinationId()) {
    		try {
    			ArrayList<Long> ReferpartnerCombinations = SkinsManagerBase.getReferpartnerCombinationsId(skinId);
            	((Skin)skins.get(skinId)).setReferpartnerCombinationId(ReferpartnerCombinations);
    		} catch (SQLException sqle) {
    			Log.error("Can't load ReferpartnerCombinations.", sqle);
    		}
    	}
    	return skins.get(skinId).getReferpartnerCombinationId();
    }

	/**
	 * @param skinIdByLocale the skinIdByLocale to set
	 */
	public static void setSkinIdByLocale(Hashtable<String, Skin> skinIdByLocale) {
		SkinsManagerBase.skinIdByLocale = skinIdByLocale;
	}
	
    
    /**
     * 
     * Get markets for skin by opportunity type
     * @param skinId
     * @param opportunityTypeId
     * @return
     * @throws SQLException
     */
//    public static Hashtable<Long, Market> getMarketsForSkinByOppType(long skinId, long opportunityTypeId, boolean onlyHorly) throws SQLException {
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            return MarketsDAOBase.getMarketsForSkinByOppType(conn, skinId, opportunityTypeId, onlyHorly);  
//        } finally {
//            closeConnection(conn);
//        }
//    }
    
    /**
     * Get bank wire withdraw by skinId
     * @param skinId
     * @return ArrayList<Bank>
     * @throws SQLException
     */
    public static ArrayList<BankBase> getBankWireWithdrawBySkin(long skinId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return BanksDAOBase.getBankWireWithdrawBySkin(conn, skinId);
        } finally {
            closeConnection(conn);
        }
    }
    
    /**
     * Get bank wire withdraw
     * @param skinId
     * @return ArrayList<Bank>
     */
    public static ArrayList<BankBase> getBankWireWithdraw(long skinId) {
    	getSkins();
    	if (null == skins.get(skinId).getBankWireWithdrawList()) {
    		try {
    			((Skin)skins.get(skinId)).setBankWireWithdrawList(SkinsManagerBase.getBankWireWithdrawBySkin(skinId));
    		} catch (SQLException sqle) {
    			Log.error("Can't load bank wire withdraw.", sqle);
    		}
    	}
    	return skins.get(skinId).getBankWireWithdrawList();
    }
	
}