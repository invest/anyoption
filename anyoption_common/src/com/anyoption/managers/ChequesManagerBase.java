//package com.anyoption.managers;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//
//import com.anyoption.common.bl_vos.Cheque;
//import com.anyoption.common.daos.ChequesDAOBase;
//import com.anyoption.common.managers.BaseBLManager;
//
//public class ChequesManagerBase extends BaseBLManager {
//    public static Cheque getById(long id) throws SQLException {
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            return ChequesDAOBase.getById(conn, id);
//        } finally {
//            closeConnection(conn);
//        }
//    }
//}