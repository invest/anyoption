package com.anyoption.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.beans.Contact;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.util.MarketingTrackerBase;
import com.anyoption.common.util.MessageToFormat;
import com.anyoption.daos.ContactsDAOBase;
import com.anyoption.util.CommonUtil;
import com.anyoption.util.ConstantsBase;
import com.anyoption.util.SendContactEmail;

public class ContactsManagerBase extends BaseBLManager {
	private static final Logger log = Logger.getLogger(ContactsManagerBase.class);

    public static void updateContactRequest(long contactId, long userId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            ContactsDAOBase.updateContactRequest(conn, contactId, userId);
        } finally {
            closeConnection(conn);
        }
    }
    
	/**
	 * Update contact
	 * @param c
	 * @throws SQLException
	 */
	public static void updateContact(Contact c)throws SQLException{
		Connection con = null;
		try {
			con = getConnection();
			ContactsDAOBase.updateContact(con, c);
		} finally {
			closeConnection(con);
		}
	}

	public static Map<String, MessageToFormat>
			validateContact(com.anyoption.beans.base.Contact contact, Locale l, boolean isApiUser) throws SQLException {
		Map<String, MessageToFormat> res = new HashMap<String, MessageToFormat>();
		String field = ConstantsBase.EMPTY_STRING;
		if (CommonUtil.IsParameterEmptyOrNull(contact.getName())
				|| contact.getName().equals(CommonUtil.getMessage(l, "banner.contactme.name", null))) {
			field = "name";
			log.debug("Failed to insertContact: Illegal name");
			res.put(field,
					new MessageToFormat("error.banner.contactme.required", new Object[] {CommonUtil.getMessage(	l, "banner.contactme.name",
																												null)}));
			return res;
		}
		if (CommonUtil.IsParameterEmptyOrNull(contact.getPhone())
				|| contact.getPhone().equals(CommonUtil.getMessage(l, "banner.contactme.phone", null))) {
			field = "phone";
			log.debug("Failed to insertContact: Illegal phone");
			res.put(field,
					new MessageToFormat("error.banner.contactme.required", new Object[] {CommonUtil.getMessage(	l,
																												"banner.contactme.phone",
																												null)}));
			return res;
		}
		if (CommonUtil.IsParameterEmptyOrNull(contact.getPhone()) || !contact.getPhone().matches("^[0-9]{7,}$")) {
			log.debug("Failed to insertContact: Illegal phone");
			field = "phone";
			res.put(field, new MessageToFormat("error.banner.contactme.phone", null));
			return res;
		}
		if (CommonUtil.IsParameterEmptyOrNull(contact.getEmail())) {
			log.debug("Failed to insertContact: Illegal email");
			field = "email";
			res.put(field,
					new MessageToFormat("error.banner.contactme.required", new Object[] {CommonUtil.getMessage(	l,
																												"banner.contactme.email",
																												null)}));
			return res;
		}
		if (!CommonUtil.isEmailValidUnix(contact.getEmail())) {
			log.debug("Failed to insertContact: Illegal email");
			field = "email";
			res.put(field, new MessageToFormat("error.banner.contactme.email", null));
			return res;
		}
		return res;
	}

	/**
	 * Check is contact already exists, if yes return contactId
	 * @param phone contact me phone number
	 * @param email contact email
	 * @return
	 * @throws SQLException
	 */
	public static long isExist(String phone, String email) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return ContactsDAOBase.isExists(con, phone, email);
		} finally {
			closeConnection(con);
		}
	}

	public static void insertContactRequest(com.anyoption.beans.base.Contact newContactReq)throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			long id = DAOBase.getSequenceNextVal(con,"SEQ_CONTACTS_ID");
			newContactReq.setId(id);
			ContactsDAOBase.insertContactRequest(con, newContactReq);
		} finally {
			closeConnection(con);
		}
	}

    public static void insertContact(com.anyoption.beans.base.Contact contact, String ip, Locale l, String issue, String comments, long skinId, int writerId) throws SQLException {
		PopulationEntryBase popUser = null;
		if (contact.getCombId() == null || MarketingCombinationManagerBase.getById(contact.getCombId()) == null) {
            contact.setCombId(SkinsManagerBase.getSkin(contact.getSkinId()).getDefaultCombinationId());
       }
		
        // Marketing Tracking Check if exist staticPart/Mid by email or phone
        try {
            
            log.debug("Begin Marketing Tracking Check if exist staticPart/mId by email or phone when insert Contact");
            //If Marketing Tracking param. is null set default
            if (CommonUtil.IsParameterEmptyOrNull(contact.getMarketingStaticPart()) && CommonUtil.IsParameterEmptyOrNull(contact.getmId())){
                log.debug("Marketing Tracking staticPart/mId is null set default ");
                String marketingStaticPart = MarketingTrackerBase.createDefaultStatciCookieData(contact.getCombId(), contact.getHttpReferer(), contact.getDynamicParameter(),
                		contact.getUtmSource(), contact.getSkinId(), contact.getAff_sub1(), contact.getAff_sub2(), contact.getAff_sub3());
                String mId = MarketingTrackerBase.getMidFromStaticPart(marketingStaticPart);
                
                contact.setMarketingStaticPart(marketingStaticPart);
                contact.setmId(mId);
            }
            
//            if (!CommonUtil.IsParameterEmptyOrNull(contact.getMarketingStaticPart())) {
//                log.debug("Marketing Tracking Check mId for emai: " + contact.getEmail() + " and pnoe: " + contact.getPhone() + " and current static part : " + contact.getMarketingStaticPart());
//                String existMarketingStaticPart = MarketingTrackerBase.checkStaticPartByContactDetailsInsertingContact(contact.getEmail(), contact.getPhone(),
//                                contact.getMarketingStaticPart());
//                if (existMarketingStaticPart != null) {
//                    contact.setMarketingStaticPart(existMarketingStaticPart);
//                    log.debug("Marketing Tracking found exist mId for emai: " + contact.getEmail() + " and pnoe: " + contact.getPhone() + " replace wih new value : " + existMarketingStaticPart);
//                }
//            }
//            if (!CommonUtil.IsParameterEmptyOrNull(contact.getmId())) {
//                log.debug("Marketing Tracking Check mId for emai: " + contact.getEmail() + " and pnoe: " + contact.getPhone() + " and current mId : " + contact.getmId());
//                String existMId = MarketingTrackerBase.checkMidByContactDetailsInsertingContacts(contact.getEmail(), contact.getPhone(), contact.getmId(), contact.getSkinId());
//                if (existMId != null) {
//                    contact.setmId(existMId);
//                    log.debug("Marketing Tracking found exist mId for emai: " + contact.getEmail() + " and pnoe: " + contact.getPhone() + " replace wih new value : " + existMId);
//                }
//            }
            //ETS Marketing
//            String etsMidExist = MarketingETSMobile.checkMarketingEtsMidInsertingContact(contact.getEtsMId(), contact.getEmail(), contact.getPhone());
//            if (etsMidExist != null){
//                contact.setEtsMId(etsMidExist);
//            }
        } catch (Exception e) {
            log.error("Marketing Tracking when check exist mId.", e);
        }
		
		long contactId = isExist(contact.getPhone(), contact.getEmail());
		long userId = UsersManagerBase.getUserIdByPhoneEmail(contact.getPhone(), contact.getEmail(), contact.getCountryId());
		if (CommonUtil.IsParameterEmptyOrNull(contact.getIp())) {
			contact.setIp(ip);	
		}		
		if (userId != 0) {
			popUser =  PopulationsManagerBase.getPopulationUserByUserId(userId);
		}
		if (contactId == 0) {   // new contact
			contact.setUserId(userId);
			insertContactRequest(contact);
			contactId = contact.getId();
			
			//Insert Marketing Tracking Activity
			try {
			    log.debug("Marketing Tracking insert activity for contactId:" + contact.getId());
			    MarketingTrackerBase.insertMarketingTrackerJson(contact.getMarketingStaticPart(), contact.getmId(), contact.getId(), contact.getUserId(), MarketingTrackerBase.getMarketingTrackingActivity(contact.getType()));			    
	         } catch (Exception e) {
	             log.error("When insert contact Marketing Tracking", e);
	         }
			
			try {
				sendEmail("contact.first.email", contact, l, issue, comments);
			} catch (Exception e) {
				log.error("Could not send email.", e);
			}
			//TODO: cookie saved in web, should be saved in shared preferences (device)
		} else {   // existing contact, add update event
			if (userId != 0) {
				log.info("going to update contactId: " + contactId + " with userId: " + userId);
				updateContactRequest(contactId, userId);
			}
			if (null == popUser){
				popUser = PopulationsManagerBase.getPopulationUserByContactId(contactId);
			}
		}
		
		if (null != popUser){
			PopulationsManagerBase.updateContactMeEvent(popUser, contactId, skinId, userId, writerId);
		}else{
			PopulationsManagerBase.insertIntoPopulation(contactId, skinId,userId, null, writerId, PopulationsManagerBase.POP_TYPE_CALLME, null);
		}
    }

	/**
	 * Send email function
	 * @param emailParam
	 * 		email property
	 * @throws Exception
	 */
	public static void sendEmail(String emailParam, com.anyoption.beans.base.Contact contact, Locale l, String issue, String comments) throws Exception {

		HashMap<String, String> params = null;
		// collect the parameters we neeed for the email
		params = new HashMap<String, String>();
		params.put(SendContactEmail.PARAM_EMAIL, contact.getEmail());
		params.put(SendContactEmail.PARAM_FIRST_NAME, CommonUtil.capitalizeFirstLetters(contact.getName()));
		params.put(SendContactEmail.PARAM_MOBILE, contact.getMobilePhone());
		if(contact.getId() != 0){
			params.put(SendContactEmail.PARAM_CONTACT_ID, contact.getId() + "");
		}
		String subject = ConstantsBase.CONTACT_ME_SUBJECT;
		subject += ", " + 	ConstantsBase.CONTACT_ME_SUBJECT_LANG;
		if (!CommonUtil.IsParameterEmptyOrNull(l.getLanguage())) {
			subject += l.getLanguage();
		}
		params.put(SendContactEmail.MAIL_FROM, CommonUtil.getConfig(emailParam, null));
		params.put(SendContactEmail.MAIL_TO, CommonUtil.getConfig(emailParam, null));
		params.put(SendContactEmail.MAIL_SUBJECT ,subject);
		
		if (issue != null && comments != null) {
			params.put(SendContactEmail.PARAM_ISSUE, issue);
			params.put(SendContactEmail.PARAM_COMMENTS, comments);
		}
		
		params.put(SendContactEmail.SUCCESS_MESSAGE, CommonUtil.getMessage(new Locale(ConstantsBase.LOCALE_DEFAULT), "contact.mail.success", null));
		new SendContactEmail(params).start();
	}
	
    /**
     * Insert register attempt entry (contact) 
     * @param email
     * @param firstName
     * @param lastName
     * @param countryId
     * @param mobilePhone
     * @param landLinePhone
     * @param contactEmailRegisterAttempts
     * @param contactSMSRegisterAttempts
     * @param isShortForm
     * @param session
     * @param isAjaxCall
     * @throws SQLException 
     */	
	public static long insertRegAttempt(String ip, String email, String firstName, String lastName, 
			String mobilePhone,  String landLinePhone, boolean contactEmailRegisterAttempts, boolean contactSMSRegisterAttempts, 
			String duId, long skinId, String userAgent, int writerId, boolean isShortForm, String utcOffset, 
			Long combinationId, long countryId, String dynamicParameter, long id, String httpReferer, String mId, String aff_sub1, String aff_sub2, String aff_sub3) throws SQLException {
				
		Contact regAttempt = null;
		//from mobile 		
		if (!CommonUtil.IsParameterEmptyOrNull(duId)){
			// find contact from mobile device
			regAttempt = findContactByDeviceUniqueId(duId, Contact.CONTACT_US_REGISTER_ATTEMPTS);
		} else if (id != 0) { //when coming from browser (landing page baidu for example) 
			regAttempt = getContactById(id);
		}
		if (regAttempt == null) {
			regAttempt = new Contact();
		}
        boolean isEmailValid = false;
        boolean isMobileValid = false;
        boolean isLandLineValid = false; 
        boolean updateContactBy = false;

        //Validations
    	if (!CommonUtil.IsParameterEmptyOrNull(email) && !email.equalsIgnoreCase(regAttempt.getEmail())){ //mail validation and value changed
    		if (CommonUtil.isEmailValidUnix(email)){
    			isEmailValid = true;
    		}
    	}
    	int mobileLengVal = 6;
    	int langLengVal = 6;
    	if (skinId == Skin.SKIN_ETRADER) {
        	mobileLengVal = 9;
        	langLengVal = 8;
    	}
    	if (!CommonUtil.IsParameterEmptyOrNull(mobilePhone) && !mobilePhone.equalsIgnoreCase(regAttempt.getMobilePhone())){//mobile validation and value changed
    		String numToken = "\\b\\d+\\b";
    		if (null != mobilePhone && mobilePhone.length() > mobileLengVal && mobilePhone.matches(numToken)){
    			isMobileValid = true;
    		}
    	}
    	if (!CommonUtil.IsParameterEmptyOrNull(landLinePhone) && !landLinePhone.equalsIgnoreCase(regAttempt.getLandLinePhone())){//land line phone validation and value changed
    		String numToken = "\\b\\d+\\b";
    		if (null != landLinePhone && landLinePhone.length() > langLengVal && landLinePhone.matches(numToken)){
    			isLandLineValid = true;
    		}
    	}
    	
    	if (regAttempt.getId() != 0 && (regAttempt.isContactByEmail() != contactEmailRegisterAttempts
    			|| regAttempt.isContactBySMS() != contactSMSRegisterAttempts)) {
    		updateContactBy = true;
    	}
    	
    	
    	if (!isEmailValid && !isLandLineValid && !isMobileValid && !updateContactBy){
    		return 0;
    	}
    	
		if (isEmailValid){
			regAttempt.setEmail(email);
		}
		if (isMobileValid){
			regAttempt.setMobilePhone(mobilePhone);
		}
		if (isLandLineValid){
			regAttempt.setLandLinePhone(landLinePhone);
		}
		if (null != firstName){
			regAttempt.setFirstName(firstName);
		}
		if (null != lastName){
			regAttempt.setLastName(lastName);
		}

		regAttempt.setCountryId(countryId);
		regAttempt.setWriterId(writerId);
		regAttempt.setDeviceUniqueId(duId);
		regAttempt.setIp(ip);
		regAttempt.setUtcOffset(utcOffset);
		regAttempt.setUserAgent(userAgent);
        regAttempt.setType(Contact.CONTACT_US_REGISTER_ATTEMPTS);
        regAttempt.setContactByEmail(contactEmailRegisterAttempts);
        regAttempt.setContactBySMS(contactSMSRegisterAttempts);        		
        regAttempt.setText((firstName + " " + lastName).trim());		
		regAttempt.setSkinId(Long.valueOf(skinId));
		regAttempt.setCombId(combinationId);
		regAttempt.setDynamicParameter(dynamicParameter);
		regAttempt.setAff_sub1(aff_sub1);
		regAttempt.setAff_sub2(aff_sub2);
		regAttempt.setAff_sub3(aff_sub3);
		regAttempt.setHttpReferer(httpReferer);
		regAttempt.setUserAgent(userAgent);
		                
        if (isShortForm){
        	regAttempt.setType(Contact.CONTACT_US_SHORT_REG_FORM);
            if (regAttempt.getUserId() == 0){ //connect userId in short form
            	long userId = 0;
            		try {
    					userId = ContactsManagerBase.searchUserIdFromContactDetails(regAttempt);
    					regAttempt.setUserId(userId);
    				} catch (SQLException e) {
    					log.error("Can't get userId on register attempts or SQL EXCEPTION!!.", e);
    					e.printStackTrace();
    				}
            }        	        	        	
        }

		//TODO: Register attempt params from WEB          
		//        regAttempt.setDfaCreativeId(UsersManager.getDFACreativeId(req));
		//        regAttempt.setDfaPlacementId(UsersManager.getDFAPlacementId(req));
		//        regAttempt.setDfaMacro(UsersManager.getDFAMacro(req));


        try {
        	if (regAttempt.getId() == 0){
        		insertContactRequest(regAttempt);
        		
        		try{
        		    log.debug("Marketing Tracking insert Activity RegAttempt for Id: " + regAttempt.getId());
        		    if (!CommonUtil.IsParameterEmptyOrNull(mId)){
        		        MarketingTrackerBase.insertMarketingTrackerJson(null, mId, regAttempt.getId(), regAttempt.getUserId(), MarketingTrackerBase.getMarketingTrackingActivity(regAttempt.getType()));
        		    }
        		} catch (Exception e){
        		    log.error("When insert RegAtemp Marketing Tracking", e);   
        		}
        		
        	} else {        		
        		updateContact(regAttempt);
        	}
        	log.debug("Insert/Update Register Attempts table, id: " + regAttempt.getId());        	
		} catch (Exception e) {
			log.error("Can't write reply or SQL EXCEPTION!!.", e);
			return 0;
		}
        return regAttempt.getId();
    }
	
    /**
     * Checks if there's a user with the same email/mobile in order to connect contact with user
     * @param contact
     * @return
     * @throws SQLException
     */
    public static long searchUserIdFromContactDetails(Contact contact) throws SQLException { 
		Connection con = null;
		try {
			con = getConnection();
			return ContactsDAOBase.searchUserIdFromContactDetails(con, contact);
		} finally {
			closeConnection(con);
		}
    }
    
    public static Contact findContactByDeviceUniqueId(String duId, long contactType) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return ContactsDAOBase.findContactByDeviceUniqueId(con, duId, contactType);
		} finally {
			closeConnection(con);
		}    	
    }
    
    public static Contact getContactById(long id) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return ContactsDAOBase.getContactById(con, id);
		} finally {
			closeConnection(con);
		}    	
    }
    
	public static void insertContactMailForm(com.anyoption.beans.base.Contact contact)throws SQLException {
		log.info("Going to insert Mail Contact:" + contact);
		Connection con = null;
		try {
			con = getConnection();
			ContactsDAOBase.insertContact(con, contact);
		} finally {
			closeConnection(con);
		}
	}
    	
}