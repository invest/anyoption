package com.anyoption.sms;

public class SMSProviderConfig {
    protected long id;
    protected String name;
    protected String providerClass;
    protected String url;
    protected String userName;
    protected String password;
    protected String props;
    protected String dlrUrl;
    protected int maxRetries;
    
    public long getId() {
        return id;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    
    public String getDlrUrl() {
        return dlrUrl;
    }

    public void setDlrUrl(String dlrUrl) {
        this.dlrUrl = dlrUrl;
    }

    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getUrl() {
        return url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }
    
    public String getUserName() {
        return userName;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public String getProviderClass() {
        return providerClass;
    }

    public void setProviderClass(String providerClass) {
        this.providerClass = providerClass;
    }

    public String getProps() {
        return props;
    }

    public void setProps(String props) {
        this.props = props;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxRetries() {
        return maxRetries;
    }

    public void setMaxRetries(int maxRetries) {
        this.maxRetries = maxRetries;
    }

    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "SMSProviderConfig:" + ls +
            "id: " + id + ls +
            "name: " + name + ls +
            "providerClass: " + providerClass + ls +
            "url: " + url + ls +
            "userName: " + userName + ls +
            "password: " + password + ls +
            "props: " + props + ls +
            "dlrUrl: " + dlrUrl + ls +
            "maxRetries: " + maxRetries + ls;
    }
}