//package il.co.etrader.bl_vos;
//
//import java.io.Serializable;
//import java.math.BigDecimal;
//import java.util.Date;
//
///**
// * Market rate bean.
// *
// * @author Tony
// */
//public class MarketRate implements Serializable {
//    private static final long serialVersionUID = 1L;
//
//    protected long id;
//    protected long marketId;
//    protected Date rateTime;
//    protected BigDecimal rate;
//    protected BigDecimal graphRate;
//    protected BigDecimal dayRate;
//    protected BigDecimal graphRateAO;
//    protected BigDecimal dayRateAO;
//
//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }
//
//    public long getMarketId() {
//        return marketId;
//    }
//
//    public void setMarketId(long marketId) {
//        this.marketId = marketId;
//    }
//
//    public BigDecimal getRate() {
//        return rate;
//    }
//
//    public void setRate(BigDecimal rate) {
//        this.rate = rate;
//    }
//
//    public Date getRateTime() {
//        return rateTime;
//    }
//
//    public void setRateTime(Date rateTime) {
//        this.rateTime = rateTime;
//    }
//
//    public BigDecimal getGraphRate() {
//        return graphRate;
//    }
//
//    public void setGraphRate(BigDecimal graphRate) {
//        this.graphRate = graphRate;
//    }
//
//    public BigDecimal getDayRate() {
//        return dayRate;
//    }
//
//    public void setDayRate(BigDecimal dayRate) {
//        this.dayRate = dayRate;
//    }
//
//    public BigDecimal getDayRateAO() {
//		return dayRateAO;
//	}
//
//	public void setDayRateAO(BigDecimal dayRateAO) {
//		this.dayRateAO = dayRateAO;
//	}
//
//	public BigDecimal getGraphRateAO() {
//		return graphRateAO;
//	}
//
//	public void setGraphRateAO(BigDecimal graphRateAO) {
//		this.graphRateAO = graphRateAO;
//	}
//
//    public String toString() {
//        String ls = System.getProperty("line.separator");
//        return ls + "MarketRate:" + ls +
//            "id: " + id + ls +
//            "marketId: " + marketId + ls +
//            "rateTime: " + rateTime + ls +
//            "rate: " + rate + ls +
//            "graphRate: " + graphRate + ls +
//            "dayRate: " + dayRate + ls +
//            "graphRateAO: " + graphRateAO + ls +
//            "dayRateAO: " + dayRateAO + ls;
//    }
//}