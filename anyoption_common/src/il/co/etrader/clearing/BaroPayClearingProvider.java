//package il.co.etrader.clearing;
//
//import java.io.IOException;
//import java.net.URLEncoder;
//
//import javax.xml.parsers.ParserConfigurationException;
//
//import org.apache.log4j.Logger;
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
//import org.xml.sax.SAXException;
//
//import com.anyoption.clearing.BaroPayInfo;
//import com.anyoption.common.clearing.ClearingException;
//import com.anyoption.common.clearing.ClearingInfo;
//import com.anyoption.common.clearing.ClearingProvider;
//import com.anyoption.common.util.ClearingUtil;
//
///**
// * Implement the communication with BaroPay
// *
// * @author Eyal O
// */
//public class BaroPayClearingProvider extends ClearingProvider {
//	private static Logger log = Logger.getLogger(BaroPayClearingProvider.class);
//
//    public static final String STATUS_PROCESSED_CODE = "2";
//    public static final String STATUS_PENDING_CODE = "0";
//    public static final String STATUS_CANCELLED_CODE = "-1";
//    public static final String STATUS_FAILED_CODE = "-2";
//    public static final String STATUS_CHARGEBACK_CODE = "-3";
//    public static final String STATUS_PROCESSED_STRING = "Processed";
//    public static final String STATUS_PENDING_STRING = "Pending";
//    public static final String STATUS_CANCELLED_STRING = "Cancelled";
//    public static final String STATUS_FAILED_STRING = "Failed";
//    public static final String STATUS_CHARGEBACK_STRING = "Chargeback";
//
//    public void authorize(ClearingInfo info) throws ClearingException {
//    	if (log.isTraceEnabled()) {
//            log.trace(info.toString());
//        }
//        try {
//        	BaroPayInfo baroPayInfo = (BaroPayInfo) info;
//        	log.info("Enter to baroPay authorize for transaction:" + baroPayInfo);
//            String body =
//            		"DepositID=" + URLEncoder.encode(Long.toString(baroPayInfo.getTransactionId()),"UTF-8") +
//            		"&MemberCode=" + URLEncoder.encode(Long.toString(baroPayInfo.getUserId()),"UTF-8") +
//            		"&MobileNumber=" + URLEncoder.encode(baroPayInfo.getLandLinePhone(),"UTF-8") +
//            		"&Currency=" + URLEncoder.encode(baroPayInfo.getCurrencySymbol(),"UTF-8") +
//            		"&Amount=" + URLEncoder.encode(baroPayInfo.getDepositAmount(),"UTF-8") +
//            		"&SenderName=" + URLEncoder.encode(baroPayInfo.getSenderName(),"UTF-8") +
//            		"&ReturnURL=" + URLEncoder.encode(baroPayInfo.getStatusURL(),"UTF-8") +
//            		"&AgentID=" + URLEncoder.encode(username,"UTF-8");
//
//            String response = ClearingUtil.executePOSTRequest(url + "/deposit.baro", body);
//            parseResponse(response, baroPayInfo);
//        } catch (Throwable t) {
//            throw new ClearingException("Transaction failed.", t);
//        }
//    }
//
//    public void capture(ClearingInfo info) throws ClearingException {
//    	throw new ClearingException("Unsupported operation.");
//    }
//
//    public void enroll(ClearingInfo info) throws ClearingException {
//    	throw new ClearingException("Unsupported operation.");
//    }
//
//    public void purchase(ClearingInfo info) throws ClearingException {
//    	throw new ClearingException("Unsupported operation.");
//    }
//
//    public void withdraw(ClearingInfo info) throws ClearingException {
//    	throw new ClearingException("Unsupported operation.");
//    }
//
//    public void bookback(ClearingInfo info) throws ClearingException {
//    	if (log.isTraceEnabled()) {
//            log.trace(info.toString());
//        }
//        try {
//        	BaroPayInfo baroPayInfo = (BaroPayInfo) info;
//        	log.info("Enter to baroPay bookback for transaction:" + baroPayInfo);
//        	String body =
//            		"WithdrawalID=" + URLEncoder.encode(Long.toString(baroPayInfo.getTransactionId()),"UTF-8") +
//            		"&MemberCode=" + URLEncoder.encode(Long.toString(baroPayInfo.getUserId()),"UTF-8") +
//            		"&BankName=" + URLEncoder.encode(baroPayInfo.getBaroPayRequest().getBankName(),"UTF-8") +
//            		"&AccountNo=" + URLEncoder.encode(baroPayInfo.getBaroPayRequest().getAccountNumber(),"UTF-8") +
//            		"&Payee=" + URLEncoder.encode(baroPayInfo.getBaroPayRequest().getAccountOwner(),"UTF-8") +
//            		"&Currency=" + URLEncoder.encode(baroPayInfo.getCurrencySymbol(),"UTF-8") +
//            		"&Amount=" + URLEncoder.encode(baroPayInfo.getDepositAmount(),"UTF-8") +
//            		"&ReturnURL=" + URLEncoder.encode(baroPayInfo.getStatusWithdrawURL(),"UTF-8") +
//            		"&AgentID=" + URLEncoder.encode(username,"UTF-8");
//
//            String response = ClearingUtil.executePOSTRequest(url + "/withdrawal.baro", body);
//            parseResponse(response, baroPayInfo);
//        } catch (Throwable t) {
//            throw new ClearingException("Transaction failed.", t);
//        }
//    }
//
//    public boolean isSupport3DEnrollement() {
//        return false;
//    }
//
//
//    public void setProviderDetails(BaroPayInfo info) {
//    	info.setRedirectURL(url);
//    	info.setMerchantAccount(username);
//    }
//
//	@Override
//	public void cancel(ClearingInfo info) throws ClearingException {
//		// TODO Auto-generated method stub
//	}
//	
//	/**
//     * Parse BaroPayInfo
//     *
//     * @param result the result XML
//     * @return <code>Hashtable</code> with result values.
//     * @throws ParserConfigurationException
//     * @throws SAXException
//     * @throws IOException
//     */
//    private void parseResponse(String response, BaroPayInfo info) throws ParserConfigurationException, SAXException, IOException {
//        Document respDoc = ClearingUtil.parseXMLToDocument(response);
//        Element root = (Element) ClearingUtil.getNode(respDoc.getDocumentElement(), "");
//        String status = ClearingUtil.getElementValue(root, "Status/*");
//        String paymentID = ClearingUtil.getElementValue(root, "PaymentID/*");
//        String errDesc = ClearingUtil.getElementValue(root, "ErrDesc/*");
//        
//        info.setResult(status);
//        info.setProviderTransactionId(paymentID);
//        info.setMessage(errDesc);
//        info.setXmlResponse(response);
//
//        if (log.isDebugEnabled()) {
//            log.debug(info.toString());
//        }
//    }
//
//}