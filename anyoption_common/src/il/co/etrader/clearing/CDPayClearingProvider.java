//package il.co.etrader.clearing;
//
//import java.io.IOException;
//import java.net.URLEncoder;
//
//import javax.xml.parsers.ParserConfigurationException;
//
//import org.apache.log4j.Logger;
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
//import org.xml.sax.SAXException;
//
//import com.anyoption.clearing.CDPayInfo;
//import com.anyoption.common.clearing.ClearingException;
//import com.anyoption.common.clearing.ClearingInfo;
//import com.anyoption.common.clearing.ClearingProvider;
//import com.anyoption.common.util.ClearingUtil;
//
///**
// * @author liors
// *
// */
//public class CDPayClearingProvider extends ClearingProvider {
//	private static Logger log = Logger.getLogger(CDPayClearingProvider.class);
//	
//	@Override
//	public void authorize(ClearingInfo info) throws ClearingException {
//		throw new ClearingException("Unsupported operation.");
//		
//	}
//
//	@Override
//	public void capture(ClearingInfo info) throws ClearingException {
//		throw new ClearingException("Unsupported operation.");
//		
//	}
//
//	@Override
//	public void cancel(ClearingInfo info) throws ClearingException {
//		throw new ClearingException("Unsupported operation.");
//		
//	}
//
//	@Override
//	public void enroll(ClearingInfo info) throws ClearingException {
//		throw new ClearingException("Unsupported operation.");
//		
//	}
//
//	@Override
//	public void purchase(ClearingInfo info) throws ClearingException {
//		throw new ClearingException("Unsupported operation.");
//		
//	}
//
//	@Override
//	public void withdraw(ClearingInfo info) throws ClearingException {
//		throw new ClearingException("Unsupported operation.");
//		
//	}
//
//	@Override
//	public void bookback(ClearingInfo info) throws ClearingException {
//		if (log.isTraceEnabled()) {
//            log.trace(info.toString());
//        }
//		try {
//			CDPayInfo cdpayInfo = (CDPayInfo) info; 
//			log.info("Enter to cdpay BookBack for transaction:" + cdpayInfo);
//			String body = 
//					"version=" + URLEncoder.encode(String.valueOf(cdpayInfo.getVersion()), "UTF-8") +
//					"&merchant_account=" + URLEncoder.encode(cdpayInfo.getMerchantAccount(), "UTF-8") +
//					"&order_id=" + URLEncoder.encode(String.valueOf(cdpayInfo.getOrderId()), "UTF-8") +
//					"&amount=" + URLEncoder.encode(String.valueOf(cdpayInfo.getAmount()),"UTF-8") +
//					"&currency=" + URLEncoder.encode(cdpayInfo.getCurrencySymbol(),"UTF-8") +
//					"&refund_reason=" + URLEncoder.encode("anyreason","UTF-8") +
//					"&control=" + URLEncoder.encode(cdpayInfo.createRefundAuthenticationChecksum(),"UTF-8");
//			log.debug("post url: " + url + "\n" + " post body: " + body);
//			String response = ClearingUtil.executePOSTRequest(url + CDPayInfo.URL_POST_REFUND, body);
//			log.debug("cdpay refund response: " + response);
//			parseResponse(response, cdpayInfo);
//		} catch (Throwable t) {
//			throw new ClearingException("cdpay, Transaction failed.", t);
//		}
//	}
//
//	@Override
//	public boolean isSupport3DEnrollement() {
//		return false;
//	}
//	
//	public void setProviderDetails(CDPayInfo info) {
//    	info.setRedirectURL(url + CDPayInfo.URL_POST_DEPOSIT);
//    	info.setMerchantAccount(username);
//    	info.setMerchant_id(password); // password in our case equal to merchantId
//    	info.setPrivateKey(privateKey);
//    }
//	
//    /**
//     * Parse CDPay refund result.
//     *
//     * @param result the result XML
//     * @return <code>Hashtable</code> with result values.
//     * @throws ParserConfigurationException
//     * @throws SAXException
//     * @throws IOException
//     * @throws ClearingException 
//     */
//	//TODO: cdpay. unit testing.
//    private  void parseResponse(String response, CDPayInfo cdpayInfo) throws ParserConfigurationException, SAXException, IOException, ClearingException {
//    	Document responseDocument = ClearingUtil.parseXMLToDocument(response);
//          
//        try {
//        	Element rootOrder = (Element) ClearingUtil.getNode(responseDocument.getDocumentElement(), "");
//        	Element rootTransaction = (Element) ClearingUtil.getNode(responseDocument.getDocumentElement(), "transaction/");     
//        	String statusMsg = ClearingUtil.getElementValue(rootTransaction, "message/*");
//        	cdpayInfo.setStatusMessage(statusMsg);
//        	String cdpayTransactionId = ClearingUtil.getElementValue(rootOrder, "orderid/*");
//        	cdpayInfo.setCdpayTransactionId(Integer.valueOf(cdpayTransactionId));
//        	cdpayInfo.setOrderId(Integer.valueOf(cdpayTransactionId));
//        	cdpayInfo.setMerchantOrder(ClearingUtil.getElementValue(rootOrder, "merchantOrder/*"));
//        	cdpayInfo.setTransactionId(Integer.valueOf(ClearingUtil.getElementValue(rootOrder, "merchantOrder/*")));
//        	cdpayInfo.setAmount(Integer.valueOf(ClearingUtil.getElementValue(rootOrder, "amount/*")));
//        	cdpayInfo.setCurrencySymbol(ClearingUtil.getElementValue(rootOrder, "currency/*"));
//        	//transaction
//        	cdpayInfo.setBankId(Integer.valueOf(ClearingUtil.getElementValue(rootTransaction, "bankid/*")));
//        	cdpayInfo.setBankTransactionId((ClearingUtil.getElementValue(rootTransaction, "banktranid/*")));
//        	String status = ClearingUtil.getElementValue(rootTransaction, "status/*");
//        	cdpayInfo.setStatus(status);
//        	
//        	//success.
//        	cdpayInfo.setResult(status);
//        	cdpayInfo.setAuthNumber(cdpayTransactionId);
//        	cdpayInfo.setMessage(statusMsg);
//        	cdpayInfo.setSuccessful(status.equals(STATUS_SUCCESSFUL));
//        	
//        	//refund - if succeed.      	
//        	Element rootRefund = (Element) ClearingUtil.getNode(responseDocument.getDocumentElement(), "refund");
//        	String refundId = ClearingUtil.getElementValue(rootRefund, "refundid/*");
//        	if (refundId != null) {
//        		cdpayInfo.setRefundId(Integer.valueOf(refundId));
//        		cdpayInfo.setRefundAmount(Double.valueOf(ClearingUtil.getElementValue(rootOrder, "refundamount/*")));
//        		cdpayInfo.setRefundReqDate(ClearingUtil.getElementValue(rootOrder, "refundreqdate/*"));    		
//        		cdpayInfo.setRefundCompleteDate(ClearingUtil.getElementValue(rootOrder, "refundcompletedate/*"));
//        		cdpayInfo.setRefundStatus(ClearingUtil.getElementValue(rootOrder, "refundstatus/*"));
//        	} else {
//        		log.info("cdpay parse response, refund id was not found, seek the reason in `message` tag.");
//        	}  	
//        } catch (Throwable t) {
//        	log.error("cdpay parseResponse," + cdpayInfo.getStatusMessage());
//			throw new ClearingException("cdpay, parseResponse failed." + cdpayInfo.getStatusMessage(), t);
//		}
//
//        if (log.isDebugEnabled()) {
//            log.debug(cdpayInfo.toString());
//        }
//    }
//    
//    /*public static Document stringToDom(String xmlSource) 
//            throws SAXException, ParserConfigurationException, IOException {
//        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
//        DocumentBuilder builder = factory.newDocumentBuilder();
//        return builder.parse(new InputSource(new StringReader(xmlSource)));
//    }*/
//}
