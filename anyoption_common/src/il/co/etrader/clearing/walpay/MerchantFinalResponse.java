//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.01.16 at 04:34:29 PM EET 
//


package il.co.etrader.clearing.walpay;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ResponseInfo"/>
 *         &lt;element ref="{}ProcessorInfo"/>
 *         &lt;element ref="{}ErrorInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "responseInfo",
    "processorInfo",
    "errorInfo"
})
@XmlRootElement(name = "MerchantFinalResponse")
public class MerchantFinalResponse {

    @XmlElement(name = "ResponseInfo", required = true)
    protected ResponseInfo responseInfo;
    @XmlElement(name = "ProcessorInfo", required = true)
    protected ProcessorInfo processorInfo;
    @XmlElement(name = "ErrorInfo", required = true)
    protected ErrorInfo errorInfo;

    /**
     * Gets the value of the responseInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseInfo }
     *     
     */
    public ResponseInfo getResponseInfo() {
        return responseInfo;
    }

    /**
     * Sets the value of the responseInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseInfo }
     *     
     */
    public void setResponseInfo(ResponseInfo value) {
        this.responseInfo = value;
    }

    /**
     * Gets the value of the processorInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessorInfo }
     *     
     */
    public ProcessorInfo getProcessorInfo() {
        return processorInfo;
    }

    /**
     * Sets the value of the processorInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessorInfo }
     *     
     */
    public void setProcessorInfo(ProcessorInfo value) {
        this.processorInfo = value;
    }

    /**
     * Gets the value of the errorInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ErrorInfo }
     *     
     */
    public ErrorInfo getErrorInfo() {
        return errorInfo;
    }

    /**
     * Sets the value of the errorInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorInfo }
     *     
     */
    public void setErrorInfo(ErrorInfo value) {
        this.errorInfo = value;
    }

}
