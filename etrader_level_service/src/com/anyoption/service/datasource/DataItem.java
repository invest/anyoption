package com.anyoption.service.datasource;

import java.util.HashMap;
import java.util.Map;

import com.anyoption.service.level.formula.FormulaConfig;

public class DataItem {
    private Map<String, String> subscriptionConfig;
    private boolean stateOK;
    private int subscribeCount;
    private Map<String, Object> fieldsCache;

	public DataItem(String dataSubscription) {
		this.subscriptionConfig = new HashMap<String, String>();
		subscriptionConfig.put(FormulaConfig.SUBSCRIPTION_CONFIG_IDENTIFIER, dataSubscription);
		stateOK = true;
		fieldsCache = new HashMap<String, Object>();
	}

	public DataItem(Map<String, String> subscriptionConfig) {
		this.subscriptionConfig = subscriptionConfig;
		stateOK = true;
		fieldsCache = new HashMap<String, Object>();
	}

	public String getDataSubscription() {
		return subscriptionConfig.get(FormulaConfig.SUBSCRIPTION_CONFIG_IDENTIFIER);
	}
    
    public int getSubscribeCount() {
        return subscribeCount;
    }
    
    public void setSubscribeCount(int subscribeCount) {
        this.subscribeCount = subscribeCount;
    }
    
    public Map<String, Object> getFieldsCache() {
        return fieldsCache;
    }
    
    public void setFieldsCache(Map<String, Object> fieldsCache) {
        this.fieldsCache = fieldsCache;
    }

    public boolean isStateOK() {
        return stateOK;
    }

    public void setStateOK(boolean stateOK) {
        this.stateOK = stateOK;
    }

	public Map<String, String> getSubscriptionConfig() {
		return subscriptionConfig;
	}
}