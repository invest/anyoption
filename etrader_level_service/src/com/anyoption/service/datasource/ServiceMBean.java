package com.anyoption.service.datasource;

/**
 * @author kirilim
 */
public interface ServiceMBean {

	public void startService();

	public void stopService();

	public boolean isInternal();

	public void setInternal(boolean internal);

	public int getDataUpdatesQueueSize();
}