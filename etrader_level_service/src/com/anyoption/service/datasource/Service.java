package com.anyoption.service.datasource;

import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import com.anyoption.common.jms.ConnectionLoop;
import com.anyoption.common.jms.HeartbeatMessage;
import com.anyoption.common.jms.JMSHandler;
import com.anyoption.common.jms.ifc.ExtendedMessageListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.reuters.BitcoinUpdatesThread;

/**
 * @author kirilim
 */
public abstract class Service implements ServiceIfc, ServiceMBean, ExtendedMessageListener {

	private static final Logger log = Logger.getLogger(Service.class);
	protected long id;

    // JMS properties
    protected boolean internal;
    protected String initialContextFactory;
    protected String providerURL;
    protected String connectionFactoryName;
    protected String queueName;
    protected String topicName;
    protected String webTopicName;
    protected int msgPoolSize;
    protected int recoveryPause;

    protected long lifeId;
    protected DataUpdatesListener dataUpdatesListener;
    protected Map<String, DataItem> items;
    //this object handles comunications with JMS. Hides the use of Session, Connections, Publishers etc..
    protected JMSHandler jmsHandler;
    protected HeartbeatThread heartbeat;
    protected ConnectionLoopTPQR connector;
    protected SenderThread sender;

	protected abstract void startSubscription();

	protected abstract void stopSubscription();

	@Override
    public void sendUpdate(DataUpdate update) {
    	log.debug(update);
    	if (update.getDataSubscription().equals(BitcoinUpdatesThread.BITCOIN_FEED_NAME) || update.getDataSubscription().equals(BitcoinUpdatesThread.LITECOIN_FEED_NAME)) {
    	    // cache the update so we can send snapshots later
            synchronized (items) {
                DataItem item = items.get(update.getDataSubscription());
                if (null != item) {
                    item.getFieldsCache().putAll(update.getFields());
                }
            }
    	}
        if (internal) {
            if (null != dataUpdatesListener) {
                dataUpdatesListener.update(update);
            } else {
                log.warn("No listener.");
            }
        } else {
            sender.send(update);
        }
    }

    protected void startupJMS() {
        log.info("Data Service ========== startupJMS");

        lifeId = System.currentTimeMillis();

        //instantiate a JMSHandler that will handle LightStreamer updates sending and commands Queue receiving
        jmsHandler = new JMSHandler("RS", initialContextFactory, providerURL, connectionFactoryName, queueName, connectionFactoryName, topicName);
        jmsHandler.setListener(this);

        //start the loop that tries to connect to JMS
        connector = new ConnectionLoopTPQR(jmsHandler, recoveryPause);
        connector.start();

        sender = new SenderThread(jmsHandler, false);
        sender.start();

        heartbeat = new HeartbeatThread();
        heartbeat.start();
    }
    
    protected void stopJMS() {
        log.info("Data Service ========== stopJMS");

        heartbeat.stopHeartbeat();
        heartbeat = null;

        if (null != sender) {
            sender.stopSenderThread();
        }
        sender = null;

        if (null != connector) {
            connector.abort();
        }
        connector = null;

        jmsHandler.close();
        jmsHandler = null;
    }

    /**
     * receive messages from JMSHandler
     */
    public void onMessage(Message message) {
        String feedMsg = null;
        log.debug("Data Service Message received: processing...");
        try {
            //pull out text from the Message object
            TextMessage textMessage = (TextMessage) message;
            feedMsg = textMessage.getText();
            if (log.isInfoEnabled()) {
                log.info("TextMessage received: " + feedMsg);
            }
        } catch (ClassCastException cce) {
            // if message isn't a TextMessage then this update is not "correct"
            log.warn("Unknown message (ClassCastException)");
            return;
        } catch (JMSException jmse) {
            log.error("", jmse);
            return;
        }

        String[] cmd = feedMsg.split("_");
        if (cmd[0].equals("subscribe")) {
        	Gson gson = new GsonBuilder().serializeNulls().create();
        	Map<String, String> map = null;
        	try {
        		map = gson.fromJson(cmd[1], new TypeToken<Map<String, String>>(){}.getType());
        	} catch (JsonSyntaxException e) {
        		log.error("Subscribe failed", e);
        	}
            subscribeMarket(map); // deserialize as map
            return;
        } else if (cmd[0].equals("unsubscribe")) {
        	Gson gson = new GsonBuilder().serializeNulls().create();
        	Map<String, String> map = null;
        	try {
        	 map = gson.fromJson(cmd[1], new TypeToken<Map<String, String>>(){}.getType());
        	} catch (JsonSyntaxException e) {
        		log.error("Unsubscribe failed", e);
        	}
        	unsubscribeMarket(map);
            return;
        }

        log.warn("Unknown message. Message: " + feedMsg);
    }

    public void onException(JMSException jmse) {
        jmsHandler.reset();
        connector = new ConnectionLoopTPQR(jmsHandler, recoveryPause);
        connector.start();
    }

	public long getId() {
		return id;
	}

	public boolean isHeartbeatAlive() {
        return heartbeat.isWorking();
    }

    @Override
	public boolean isInternal() {
		return internal;
	}

    @Override
	public void setInternal(boolean internal) {
		this.internal = internal;
	}

	public String getInitialContextFactory() {
		return initialContextFactory;
	}

	public void setInitialContextFactory(String initialContextFactory) {
		this.initialContextFactory = initialContextFactory;
	}

	public String getProviderURL() {
		return providerURL;
	}

	public void setProviderURL(String providerURL) {
		this.providerURL = providerURL;
	}

	public String getConnectionFactoryName() {
		return connectionFactoryName;
	}

	public void setConnectionFactoryName(String connectionFactoryName) {
		this.connectionFactoryName = connectionFactoryName;
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public String getWebTopicName() {
		return webTopicName;
	}

	public void setWebTopicName(String webTopicName) {
		this.webTopicName = webTopicName;
	}

	public int getMsgPoolSize() {
		return msgPoolSize;
	}

	public void setMsgPoolSize(int msgPoolSize) {
		this.msgPoolSize = msgPoolSize;
	}

	public int getRecoveryPause() {
		return recoveryPause;
	}

	public void setRecoveryPause(int recoveryPause) {
		this.recoveryPause = recoveryPause;
	}

    public DataUpdatesListener getDataUpdatesListener() {
        return dataUpdatesListener;
    }

    public void setDataUpdatesListener(DataUpdatesListener dataUpdatesListener) {
        this.dataUpdatesListener = dataUpdatesListener;
    }

	//////////////////////RecoveryThread
    private class ConnectionLoopTPQR extends ConnectionLoop {

        public ConnectionLoopTPQR(JMSHandler jmsHandler, int recoveryPause) {
            super(jmsHandler, recoveryPause);
            threadName = "ConnectionTPQR";
        }

        protected void onConnectionCall() {
            if (log.isInfoEnabled()) {
                log.info("onConnectionCall");
            }
            return;
        }

        protected void connectionCall() throws JMSException, NamingException {
            //initialize TopicPublisher and QueueReceiver
            if (log.isInfoEnabled()) {
                log.info("connectionCall");
            }
            if (null != jmsHandler.getTopicName()) {
                jmsHandler.initTopicPublisher(msgPoolSize);
            }
            if (null != jmsHandler.getQueueName()) {
                jmsHandler.initQueueReceiver();
            }
        }
    }
    
    //////////////////////HeartbeatThread
    private class HeartbeatThread extends Thread {
        private HeartbeatMessage fixedMessage = new HeartbeatMessage(lifeId);
        private boolean running;
        private long lastJingleTime;

        public void run() {
            Thread.currentThread().setName("Heartbeat");
            running = true;
            while (running) {
                lastJingleTime = System.currentTimeMillis();
                try {
                    //publish the update to JMS
                    jmsHandler.publishMessage(fixedMessage);
                } catch (JMSException je) {
                    log.error("Unable to send message - JMSException:" + je.getMessage());
                }
                if (log.isTraceEnabled()) {
                    log.trace("Heartbeat sent: " + fixedMessage.lifeId);
                }

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
            }
            log.warn("HeartbeatThread ends. running = " + running);
        }

        public void stopHeartbeat() {
            running = false;
        }

        public boolean isWorking() {
            return System.currentTimeMillis() - lastJingleTime < 2000;
        }
    }
}