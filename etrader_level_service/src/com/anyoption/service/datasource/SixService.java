package com.anyoption.service.datasource;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Logger;

import com.anyoption.service.level.formula.FormulaConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sixtelekurs.rtds.api.client.BindType;
import com.sixtelekurs.rtds.api.client.DeathHandler;
import com.sixtelekurs.rtds.api.client.IBindChangeEvent;
import com.sixtelekurs.rtds.api.client.IBindStatusHandler;
import com.sixtelekurs.rtds.api.client.RtdsApi;
import com.sixtelekurs.rtds.api.misc.Dictionary;
import com.sixtelekurs.rtds.api.tin.Tin;
import com.sixtelekurs.rtds.api.tin.TinField;

/**
 * @author kirilim
 */
public class SixService extends Service implements SixServiceMBean {
	private static Logger log = Logger.getLogger(SixService.class);
	private static final String SUBSCRIBTION_CONFIG_SIX_BC = "bc";
	private static final String SUBSCRIBTION_CONFIG_SIX_VALOR = "valor";
	private static final String SUBSCRIBTION_CONFIG_SIX_FIELDS = "fields";

	private RtdsApi api;
	private Dictionary tdfDic;
	private MyDeathHandler deathHandler;
	private MyBindStatusHandler bindHandler;
	private Map<Integer, String> subscriptionTags;

	/** connection timeout */
//	private int timeout = 300; // seconds

	private Config config;
	// private String fims = "153.46.111.212";
	// private short defaultPort = 5013;
	// private String user = "anymdfint01@GB28491";
	// private String password = "829885";
	// private String application = "FIMS API";
//	private boolean clientSessionRequestFinished;
	private MessageDataExtractorThread messageExtractor;
	private boolean serviceUp;
	private MyReceiveThread receiver;

	private static Map<String, Boolean> doubleFields;
	static {
		doubleFields = new HashMap<String, Boolean>();
		doubleFields.put("TDF_LPRICE", true);
		doubleFields.put("TDF_ASKPRI", false);
		doubleFields.put("TDF_BIDPRI", false);
	}

	private static Map<String, Boolean> stringFields;
	static {
		stringFields = new HashMap<String, Boolean>();
		stringFields.put("TDF_TRTIME", false);
	}

	private static Map<String, Boolean> usedFields;
	static {
		usedFields = new HashMap<String, Boolean>();
		usedFields.putAll(doubleFields);
		usedFields.putAll(stringFields);
	}
	
	public SixService() {
		api = new RtdsApi(true); // TODO: move the verbose flag to be configuration
	}

	public SixService(String dataServiceAttributes, boolean internal, DataUpdatesListener dataUpdatesListener, long id) {
		this();
		this.id = id;
		setDataServiceAttributes(dataServiceAttributes);
		this.internal = internal;
		this.dataUpdatesListener = dataUpdatesListener;
	}

	@Override
	public void setDataServiceAttributes(String dataServiceAttributes) {
		Gson gson = new GsonBuilder().serializeNulls().create();
		this.config = gson.fromJson(dataServiceAttributes, Config.class);
	}

	/** Starts the ClientSession */
	@Override
	public void startService() {
		log.info("SixService.startService() begin...");
		items = new HashMap<String, DataItem>();

		if (!internal) {
			startupJMS();
		}

		messageExtractor = new MessageDataExtractorThread();
		messageExtractor.start();

		startSubscription();
		
		receiver = new MyReceiveThread();
		new Thread(receiver).start();
		
		log.info("SixService.startService() end");
	}

	/** Stops the ClientSession */
	@Override
	public void stopService() {
		log.info("SixService.stopService() begin...");
		stopSubscription();

		if (!internal) {
			stopJMS();
		}

		if (null != messageExtractor) {
			messageExtractor.stopExtractorThread();
			messageExtractor = null;
		}
		
		if (null != receiver) {
			receiver.stop();
			receiver = null;
		}

		log.info("SixService.stopService() end");
	}

	@Override
	protected void startSubscription() {
		subscriptionTags = new HashMap<Integer, String>();
		
        //  Open the RtdsApi session
        log.info("Opening RtdsApi session...");
        log.info(config);
        if (!api.openApi(config.user, config.password, "FIMS API", config.fims)) {
//            log.error("Could not open session : " + api.getLastErrorStr() + " (error #" + api.getLastError() + ")");
            log.info("Could not open session : " + api.getLastErrorStr() + " (error #" + api.getLastError() + ")");
            return;
        }

        //  Register callback handlers
        log.info("Registering callback handlers...");
        deathHandler = new MyDeathHandler();
        api.setDeathCallBack(deathHandler);
        bindHandler = new MyBindStatusHandler();
        api.setBindStatusCallBack(bindHandler);

        // load data dictionary
        tdfDic = api.requestDictionary("TDF");

        //  Bind service 'TDFPRICES' to agent 'TDFPRICES' on any server (best server)
        log.info("Binding service TDFPRICES...");
        BindType bindType = BindType.BEST_ROUTE;
        api.bind(bindType, "tdfprices", "tdfprices");
	}

	@Override
	protected void stopSubscription() {
		api.closeApi();
		subscriptionTags = null;
	}

	@Override
	public void subscribeMarket(Map<String, String> subscriptionConfig) {
		synchronized (items) {
			String itemName = subscriptionConfig.get(FormulaConfig.SUBSCRIPTION_CONFIG_IDENTIFIER);
			DataItem item = items.get(itemName);
			if (item == null) {
				String trgName = subscriptionConfig.get(SUBSCRIBTION_CONFIG_SIX_VALOR) + "_" + itemName;
				String sql = "create trigger " + trgName + " on select "
								+ subscriptionConfig.get(SUBSCRIBTION_CONFIG_SIX_FIELDS)
								+ " from tdfprices where "
								+ subscriptionConfig.get(SUBSCRIBTION_CONFIG_SIX_VALOR) + "=" + itemName + ", tdf_bourse_code="
								+ subscriptionConfig.get(SUBSCRIBTION_CONFIG_SIX_BC) + " for update of selected as hit";

				//  Synchronize access to the RtdsApi object (do not allow calling receiveMsg() when sending a request)
				synchronized(api) {
					int reqTag = api.getNextTag();
					if (!api.send(sql, reqTag)) {
						short ern = api.getLastError();
						String ert = api.getLastErrorStr();
						log.warn("Sending request failed, status=" + ern + " " + ert);
					} else {
						subscriptionTags.put(reqTag, itemName);
						log.info("Sent request : " + sql + " with tag " + reqTag);
					}
				}

				item = new DataItem(subscriptionConfig);
				items.put(subscriptionConfig.get(FormulaConfig.SUBSCRIPTION_CONFIG_IDENTIFIER), item);
				item.setSubscribeCount(1);
			} else {
				item.setSubscribeCount(item.getSubscribeCount() + 1);
			}
		}
	}

	@Override
	public void unsubscribeMarket(Map<String, String> subscriptionConfig) {
		synchronized (items) {
			String itemName = subscriptionConfig.get(FormulaConfig.SUBSCRIPTION_CONFIG_IDENTIFIER);
			DataItem item = items.get(itemName);
			if (item != null) {
				if (item.getSubscribeCount() == 1) {
// TODO: stop the trigger
//					int rtc = clientSession.stopRequest(subscriptionConfig.get(FormulaConfig.SUBSCRIPTION_CONFIG_IDENTIFIER));
//					log.info("unsubscribeMarket: rtc: " + rtc);
					String trgName = subscriptionConfig.get(SUBSCRIBTION_CONFIG_SIX_VALOR) + "_" + itemName;
					String sql = "drop trigger " + trgName + " from tdfprices";
					synchronized (api) {
						int reqTag = api.getNextTag();
						if (!api.send(sql, reqTag)) {
							short ern = api.getLastError();
							String ert = api.getLastErrorStr();
							log.warn("Drop trigger request failed, status=" + ern + " " + ert);
						} else {
							items.remove(itemName);							
						}
					}
				} else {
					item.setSubscribeCount(item.getSubscribeCount() - 1);
				}
			} else {
				log.warn("Unsubscribe for missing market: " + itemName);
			}
		}
	}

	/** "Adds" a data message to the queue */
	private synchronized void addMessageData(String name, Tin tin) {
		messageExtractor.send(new ExtendedMessageData(tin, name));

	}

	/** Logs an error */
//	private synchronized void addLastError(String text) {
//		log.error("ERROR: " + text);
//	}
//
//	@Override
//	public void connectionProblem(int problemType, int problemId, int time, String errorMessage) {
//		// connection problem, just log it
//		addLastError("Connection Problem: errorMessage: " + errorMessage + ", problemType: " + problemType + ", problemId: " + problemId + ", time: " + time);
//	}
//
//	@Override
//	public void openResult(int result, String errorMessage) {
//		if (result != OPEN_RESULTS.OR_SUCCESS) {
//			synchronized (this) {
//				log.fatal("Open failed: errorMessage: " + errorMessage + ", result: " + result);
//				addLastError("Open failed: " + errorMessage);
//				clientSessionRequestFinished = true;
//				return;
//			}
//		}
//		clientSessionRequestFinished = true;
//	}
//
//	@Override
//	public void requestReply(String name, short messageType, MessageData data) {
//		// has been stopped
//		if (clientSession == null) {
//			data.dispose();
//			return;
//		}
//
//		// we got data from the trigger request we sent
//		// we ignore some of the message types
//		switch (messageType) {
//		// error with the request
//		// some problem with the request (e.g. "not found")
//		case MESSAGE_TYPES.MT_REQUEST_ERROR:
//			String error = data.getFieldAsStringByID(FIDS.TN_STATUS);
//			error = error + " - " + data.getFieldAsStringByID(FIDS.TN_ERROR_TEXT);
//			addLastError("Error for request " + name + ": " + error);
//			break;
//
//		// real data
//		case MESSAGE_TYPES.MT_DATA_RECORD:
//		case MESSAGE_TYPES.MT_UPDATE_DATA_RECORD:
//			addMessageData(name, data); // send data to "queue" for GUI to display
//			return;
//		case MESSAGE_TYPES.MT_MODIFY_TRIGGER_RESPONSE:
//			log.info("Received MT_MODIFY_TRIGGER_RESPONSE");
//			// send data to "queue" for GUI to display
//			addMessageData(name, data);
//			return;
//
//		case MESSAGE_TYPES.MT_START_DATA:
//			// ignored
//		case MESSAGE_TYPES.MT_END_DATA:
//			// ignored
//			// will not normally contain any valuable data,
//			// except for some time series services
//			break;
//		case MESSAGE_TYPES.MT_REQUEST_RESTARTED:
//			// ignored
//			// request was interrupted and has been restarted
//			break;
//		default: // unexpected
//			log.error("Received unknown message type: " + messageType);
//			break;
//		}
//		data.dispose();
//	}
//
//	@Override
//	public void serviceStatusChanged(String service, boolean up) {
//		log.info("serviceStatusChanged: service: " + service + ", up: " + up);
//		// service availability changed
//		// we only watch TDFPRICES
//		if (serviceUp != up) {
//			serviceUp = up;
//			changeDataItemStatus(up);
//		}
//		if (!up && service.equals("TDFPRICES")) {
//			// log the problem
//			addLastError("TDFPRICES down ");
//		}
//	}

	@Override
	public int getDataUpdatesQueueSize() {
		return messageExtractor.getQueueSize();
	}

//	private void changeDataItemStatus(boolean up) {
//		synchronized (items) {
//			for (DataItem item : items.values()) {
//				item.setStateOK(up);
//			}
//		}
//	}

	private class Config {
		private String fims;
		private short defaultPort;
		private String user;
		private String password;
		private String application;
		
		public String toString() {
			String ls = System.getProperty("line.separator");
			return ls + "Config:" + ls
					+ "fims: " + fims + ls
					+ "defaultPort: " + defaultPort + ls
					+ "user: " + user + ls
					+ "application: " + application + ls;
		}
	}

	private class MessageDataExtractorThread extends Thread {
		private final Logger log = Logger.getLogger(MessageDataExtractorThread.class);

		private boolean running;
		private AtomicLong queueSize;

		// this is the queue of pending requests for the LevelsService
		private ConcurrentLinkedQueue<ExtendedMessageData> toExtractMessages = new ConcurrentLinkedQueue<ExtendedMessageData>();

		public MessageDataExtractorThread() {
			queueSize = new AtomicLong();
		}

		public void run() {
			Thread.currentThread().setName("DataExtractor");
			running = true;
			log.warn("DataExtractor started");

			while (running) {
				if (log.isTraceEnabled()) {
					log.trace("DataExtractor starting work");
				}

				// go on until there are requests in the queue
				ExtendedMessageData nextMessage = null;
				// just get the message from the head but don't remove it
				while ((nextMessage = toExtractMessages.peek()) != null) {

					DataUpdate update = null;
					synchronized (items) {
						DataItem item = items.get(nextMessage.itemName);
						if (null == item) {
							log.warn("Update for item that is not subscribed - " + nextMessage.itemName);
							toExtractMessages.poll();
							continue;
						}
//						String[] subscrFields = item.getSubscriptionConfig().get(SUBSCRIBTION_CONFIG_SIX_FIELDS).split(",");
						update = new DataUpdate(nextMessage.itemName);
						Map<String, Object> fields = new HashMap<String, Object>();
						Map<String, Object> fieldsCache = item.getFieldsCache();
						StringBuffer sb = null;
						if (log.isTraceEnabled()) {
							sb = new StringBuffer();
							sb.append('\n').append(nextMessage.itemName).append('\n');
						}

						TinField tinField = new TinField();
						Tin tin = nextMessage.tin;
						int status = tin.getFirst(tinField);
						while (status == 0) {
							short fieldId = tinField.id();
							String fieldName = tdfDic.name(fieldId);
							Object fieldValue = null;
							log.debug("TIN field id: " + fieldId + " TIN filed name: " + fieldName + " TIN field type: " + tinField.type());
							switch (tinField.type()) {
								case TIN_SCALAR:
									fieldValue = new Double(tinField.getValueLong());
									break;
								case TIN_FLOAT:
									fieldValue = tinField.getValueFloat();
									break;
								case TIN_TEXT:
									fieldValue = tinField.getValueString();
									break;
									// TODO
//								default:
//									break;
							}
							
							if (doubleFields.get(fieldName) != null) {
								if (isFieldChanged(fieldName, (Double) fieldValue, fieldsCache)) {
									fieldsCache.put(fieldName, (Double) fieldValue);
									fields.put(fieldName, (Double) fieldValue);
									if (log.isTraceEnabled()) {
										sb.append(String.format("%1$-16s", fieldName)).append(": ")
											.append(fieldValue).append('\n');
									}
								}
							} else if (stringFields.get(fieldName) != null) {
								if (isFieldChanged(fieldName, (String) fieldValue, fieldsCache)) {
									fieldsCache.put(fieldName, (String) fieldValue);
									fields.put(fieldName, (String) fieldValue);
									if (log.isTraceEnabled()) {
										sb.append(String.format("%1$-16s", fieldName)).append(": ")
											.append(fieldValue).append('\n');
									}
								}
							}
							status = tin.getNext(tinField);
						}
						if (fields.size() > 0) {
							if (log.isTraceEnabled()) {
								log.trace(sb.toString());
							}
							update.setFields(fields);
						} else {
							log.trace("No interesting fields");
						}

					}
					if (update.getFields() != null) {
						update.setStateOK(serviceUp);
						sendUpdate(update);
					}
// TODO: remove
//					nextMessage.data.dispose();
					toExtractMessages.poll(); // now that the message was sent we can remove it
					long qs = queueSize.decrementAndGet();
					if (qs > 100) {
						log.warn("Dispatched. queueSize: " + qs);
					} else if (log.isTraceEnabled()) {
						log.trace("Dispatched. queueSize: " + qs);
					}
				}

				if (log.isTraceEnabled()) {
					log.trace("DataExtractor falling asleep");
				}

				try {
					synchronized (this) {
						this.wait();
					}
				} catch (InterruptedException ie) {
					// do nothing
				}
			}
// TODO: remove
			// dispose data from the queue before ending
//			ExtendedMessageData message;
//			while ((message = toExtractMessages.poll()) != null) {
//				message.data.dispose();
//			}
			log.warn("DataExtractor ends");
		}

		private boolean isFieldChanged(String field, double doubleFieldValue, Map<String, Object> fieldsCache) {
			if (!fieldsCache.containsKey(field)) {
				return true;
			}
			Object fieldValue = fieldsCache.get(field);
			if ((Double) fieldValue == doubleFieldValue && !usedFields.get(field)) {
				return false;
			}
			return true;
		}

		private boolean isFieldChanged(String field, String stringFieldValue, Map<String, Object> fieldsCache) {
			if (!fieldsCache.containsKey(field)) {
				return true;
			}
			Object fieldValue = fieldsCache.get(field);
			if (String.valueOf(fieldValue).equals(stringFieldValue) && !usedFields.get(field)) {
				return false;
			}
			return true;
		}

		/**
		 * Queue a message to be parsed.
		 * 
		 * @param message the message to parse
		 */
		public void send(ExtendedMessageData message) {
			toExtractMessages.add(message);
			long qs = queueSize.incrementAndGet();
			log.trace("Queued. Queue size: " + qs);
			wakeUp();
		}

		/**
		 * Wake up the extractor to parse messages if available.
		 */
		public void wakeUp() {
			synchronized (this) {
				try {
					this.notify();
				} catch (Exception e) {
					log.error("Can't wake up the MessageDataExtractor.", e);
				}
			}
		}

		public void stopExtractorThread() {
			running = false;
			wakeUp();
		}

		public int getQueueSize() {
			return (int) queueSize.get();
		}
	}

	private class ExtendedMessageData {

		private Tin tin;
		private String itemName;

		public ExtendedMessageData(Tin tin, String itemName) {
			this.tin = tin;
			this.itemName = itemName;
		}
	}
	
	class MyDeathHandler implements DeathHandler {
	   public void deathCallback() {
	      log.warn("Death Handler called, stopping now !!!");
	      // According to the documentation in this case there is a need of manual handling. So not much we can do here.
	   }
	}

	class MyBindStatusHandler implements IBindStatusHandler {
	    public void bindStatusChanged(IBindChangeEvent evt) {
//	        BindStatus bindStatus = evt.bindStatus(); 
	        log.info("[MyBindStatusHandler] Service " + evt.serviceName() +
	                       " on server " + evt.serverName() +
	                       " [agent " + evt.agentName() + "] " +
	                       evt.bindStatus().toString());
	    }
	}
	
	/**
	 * Directly copied from SIX HighPerformanceExample.
	 */
	class MyReceiveThread implements Runnable {
	    //  Some system TIN field id's that we need when processing a received TIN.
	    private final short TN_RESPONSE = 20009;
	    private final short TN_ERROR_TEXT = 20021;
	    private final short TN_SQL = 20003;

	    private final short TIN_CACHE_IMSG = 20119;
	    private final short TIN_CACHE_DMSG = 20120;
	    private final short TIN_CACHE_UMSG = 20116;

	    //  The field id of the valoren number (service TDFPRICES)
	    private final short TDF_VALOREN = 1;

	    //  Values as returned by the Tin.messageType() method
	    private final char S_AS_LAST                      = 107;
	    private final char S_AS_MULTIPLE                  = 106;
	    private final char S_AS_NOT_ENTITLED              = 103;
	    private final char S_AS_INVALID_QUERY             = 104;
	    private final char S_AS_NOT_FOUND                 = 105;
	    private final char S_AS_AGENT_UNAVAIL             = 111;
	    private final char S_AS_TRIGGER_DROPPED           = 120;
	    private final char S_AS_UPDATE = 0;

	    //  Object variables
	    private volatile boolean stop;
	    private TinField messageField;

	    public MyReceiveThread() {
	        stop = false;
	        messageField = new TinField();	//  Pre-construct TinField to process Tin messages
	    } 

	    public int processTin(Tin tin) {
	        //  Get Tin message type and tag
	        int msgSts = tin.messageType();
	        int msgTag = tin.tag();

	        log.debug("TIN message received for tag " + msgTag);
	        switch(msgSts) {
	            case S_AS_LAST:
	            	log.debug("S_AS_LAST received for tag " + msgTag);
	                break;
	            case S_AS_MULTIPLE:
	            case S_AS_UPDATE:
	                break;
	            case S_AS_TRIGGER_DROPPED:
	            	log.debug("Received trigger dropped");
	                return 0;
	            default:
	                String errorText = "<no error text>";
	                if (tin.getField(TN_ERROR_TEXT, messageField) == 0) {
	                    errorText = messageField.getValueString();
	                }
	                log.warn("Received error for request with tag " + msgTag + " : Status=" + msgSts + " Error=" + errorText);
	                return 0;
	        }

	        String itemName = subscriptionTags.get(msgTag);
	        
	        //  Count of how many Tin records were dispatched
	        int dispatchCount = 0;

	        //  Iterator over all (top level) TinFields in this TIN message
	        int sts = tin.getFirst(messageField);
	        while (sts == 0) {
	            //  Determine top-level field-id
	            switch(messageField.id()) {
	                case TN_RESPONSE:       // Initial snapshot response
	                    addMessageData(itemName, new Tin(messageField));
	                    dispatchCount++;
	                    break;

	                case TIN_CACHE_IMSG:    // Insert trigger message (new listing)
	                    addMessageData(itemName, new Tin(messageField));
	                    dispatchCount++;
	                    break;

	                case TIN_CACHE_DMSG:    // Delete trigger message (listing deleted)
	                    addMessageData(itemName, new Tin(messageField));
	                    dispatchCount++;
	                    break;

	                case TIN_CACHE_UMSG:    // Update trigger message (i.e. price update)
	                    addMessageData(itemName, new Tin(messageField));
	                    dispatchCount++;
	                    break;

	                default:
	                    break;  // Other top-level fields, not relevant in here
	            }
	            sts = tin.getNext(messageField);
	        }
	        log.debug("TIN message processed");
	        return dispatchCount;
	    }

	    //  Run the receive thread
	    public void run() {
	    	Thread.currentThread().setName("MyReceiveThread");
	        log.info("Starting...");

	        //  The TIN (message) that will be filled by the RtdsApi.receiveMsg() method
	        Tin tin = new Tin();

	        //  The current sleep-time between calls to the receiveMsg() method
	        long sleepTimeMicroSeconds = 0;

	        //  The max. sleep-time between calls to the receiveMsg() method
	        long maxSleepTimeMicroSeconds = 100000;  // Max. 100 milli-seconds

	        //  The no. of TIN records that were dispatched
	        int dispatchCount = 0;

	        //  Mainloop reading TIN messages from the RtdsApi object
	        while (stop == false) {
	            //  Attempt to read the next Tin object
	            boolean rcvMsg; 
	            synchronized(api) {
	                rcvMsg = api.receiveMsg(tin);
	            }

	            //  If no message received...
	            if (!rcvMsg) {
	                //  No Tin received, wait a bit (do not waste CPU)
	                if (sleepTimeMicroSeconds < maxSleepTimeMicroSeconds) {
	                    sleepTimeMicroSeconds++;
	                }
	                try {
	                	Thread.sleep(sleepTimeMicroSeconds / 1000);
	                } catch (InterruptedException ie) {
	                	// do nothing
	                }
	                dispatchCount = 0;
	            } else {
	                //  Tin message received, reset sleep-time
	                sleepTimeMicroSeconds = 0;

	                //  Process the TIN message
	                dispatchCount += processTin(tin);

	                //  If more TIN records were dispatched than a threshhold...
	                if (dispatchCount > 1000) {
	                    //  If this system (or VM) has only a single CPU...
	                    if (Runtime.getRuntime().availableProcessors() < 2) {
	                        //  Yield CPU to other threads (give them a chance to run)
	                        Thread.yield();
	                        dispatchCount -= 1000;
	                    }
	                }
	            }
	        }
	        log.info("Finished.");
	    }

	    public void stop() {
	        stop = true;
	    }
	}
}