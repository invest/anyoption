package com.anyoption.service.datasource;

public interface DataUpdatesListener {
    public void update(DataUpdate update);
}