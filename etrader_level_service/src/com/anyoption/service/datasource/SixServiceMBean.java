package com.anyoption.service.datasource;

/**
 * @author kirilim
 */
public interface SixServiceMBean extends ServiceMBean {

	public void setDataServiceAttributes(String dataServiceAttributes);
}