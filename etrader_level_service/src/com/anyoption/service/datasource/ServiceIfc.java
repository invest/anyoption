package com.anyoption.service.datasource;

import java.util.Map;

/**
 * @author kirilim
 */
public interface ServiceIfc {

	public void subscribeMarket(Map<String, String> subscriptionConfig);

	public void unsubscribeMarket(Map<String, String> subscriptionConfig);

	public void sendUpdate(DataUpdate update);
}