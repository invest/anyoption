package com.anyoption.service.datasource;

import java.io.Serializable;
import java.util.Map;

@SuppressWarnings("serial")
public class DataUpdate implements Serializable {
    private String dataSubscription;
    private boolean stateOK;
    private Map<String, Object> fields;
    
    public DataUpdate(String dataSubscription) {
        this.dataSubscription = dataSubscription;
        stateOK = false;
    }
    
    public Map<String, Object> getFields() {
        return fields;
    }
    
    public void setFields(Map<String, Object> fields) {
        this.fields = fields;
    }

	public String getDataSubscription() {
		return dataSubscription;
	}

	public void setDataSubscription(String dataSubscription) {
		this.dataSubscription = dataSubscription;
	}

	public boolean isStateOK() {
        return stateOK;
    }

    public void setStateOK(boolean stateOK) {
        this.stateOK = stateOK;
    }
    
    public String toString() {
    	String ls = System.getProperty("line.separator");
    	return ls + "DataUpdate: " + ls +
    			"dataSubscription: " + dataSubscription + ls +
    			"stateOK: " + stateOK + ls +
    			"fields: " + fields + ls;
    }
    
    public String toRecordingString(String time) {
        StringBuffer sb = new StringBuffer();
        sb.append(time);
        sb.append('|');
        sb.append(dataSubscription);
        sb.append('|');
        sb.append(stateOK);
        if (null != fields) {
            for (String key : fields.keySet()) {
                sb.append('|');
                sb.append(key);
                sb.append('=');
                sb.append(fields.get(key));
            }
        }
        return sb.toString();
    }
}