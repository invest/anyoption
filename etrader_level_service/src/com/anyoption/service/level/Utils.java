package com.anyoption.service.level;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

import com.anyoption.common.util.CommonUtil;
import com.anyoption.service.level.market.MarketState;

/**
 * @author kirilim
 */
public class Utils extends CommonUtil {

	private static Random random = new Random();

	/**
	 * Format the result of the exposure calc (the level) with the number of decimal digits for this item.
	 *
	 * @param exposureResult
	 * @return
	 */
	public static String formatLevel(double exposureResult, int decimalPoint) {
		String res = null;
		switch (decimalPoint) {
		case 1:
			DecimalFormat numberFormat1 = new DecimalFormat("#,##0.0");
			res = numberFormat1.format(exposureResult);
			break;
		case 2:
			DecimalFormat numberFormat2 = new DecimalFormat("#,##0.00");
			res = numberFormat2.format(exposureResult);
			break;
		case 3:
			DecimalFormat numberFormat3 = new DecimalFormat("#,##0.000");
			res = numberFormat3.format(exposureResult);
			break;
		case 4:
			DecimalFormat numberFormat4 = new DecimalFormat("#,##0.0000");
			res = numberFormat4.format(exposureResult);
			break;
		case 5:
			DecimalFormat numberFormat5 = new DecimalFormat("#,##0.00000");
			res = numberFormat5.format(exposureResult);
			break;
		default:
			res = String.valueOf(exposureResult);
			break;
		}
		return res;
	}

	/**
	 * Randomly modify the level with % in specified interval. For example if you want to increase the level with 0.01% to 0.07% call with
	 * level, 0.0001 and 0.0007.
	 *
	 * @param level the level to change
	 * @return
	 */
	public static double formulaRandomChange(MarketState state, double level, double randomCeiling, double randomFloor) {
		// randomly choose a value in the spec interval to change the level with
		double rnd = random.nextDouble() * (randomCeiling - randomFloor) + randomFloor;
		state.setLastRandomChange(rnd);
		return new Double(level * (1 + rnd));
	}

	/**
	 * Modify the level with specified change.
	 *
	 * @param level the level to change
	 * @param rnd the change
	 * @return
	 */
	public static double formulaRandomChange(double level, double rnd) {
		return new Double(level * (1 + rnd));
	}

	/**
	 * Used to format float values with 2 decimal digits.
	 *
	 * @param val the val to format
	 * @return
	 */
	public static String formatFloat(float val) {
		DecimalFormat numberFormat2 = new DecimalFormat("#,##0.00");
		return numberFormat2.format(val);
	}

	/**
	 * Used to format the est close time (format MM/dd/yy HH:mm) gmt 0.
	 *
	 * @param val the time to format
	 * @return
	 */
	public static String formatDate(Date val) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+0"));
		return dateFormat.format(val);
	}

	/**
	 * Used to format the odds as % (format #0%).
	 *
	 * @param val the val to format
	 * @return
	 */
	public static String formatOdds(float val) {
		DecimalFormat percentFormat = new DecimalFormat("#0%");
		return percentFormat.format(val);
	}

	/**
	 * Round a double value to have "decimals" number of digits after the decimal point. For example roundDouble(1.12345, 3) = 1.123
	 *
	 * @param val
	 * @param decimals
	 * @return
	 */
	public static double roundDouble(double val, long decimals) {
		BigDecimal bd = new BigDecimal(Double.toString(val));
		MathContext mc = new MathContext(bd.precision() - bd.scale() + (int) decimals, RoundingMode.HALF_UP);
		return bd.round(mc).doubleValue();
	}

	/**
	 * Check if specified time by <code>Date</code> object is today.
	 *
	 * @param date the to check for
	 * @return <code>true</code> if the passed time is today else <code>false</code> (even if can't parse).
	 */
	public static boolean isToday(Date date, String timeZone) {
		Calendar today = Calendar.getInstance(TimeZone.getTimeZone(timeZone));
		Calendar dc = Calendar.getInstance(TimeZone.getTimeZone(timeZone));
		dc.setTime(date);
		return dc.get(Calendar.YEAR) == today.get(Calendar.YEAR) && dc.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR);
	}

    /**
     * Calculate how many days left to expire.
     *
     * @param expire the expire date
     * @return
     */
    public static long getDaysToExpire(Date expire) {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);

        Calendar e = Calendar.getInstance();
        e.setTime(expire);
        e.set(Calendar.HOUR_OF_DAY, 0);
        e.set(Calendar.MINUTE, 0);
        e.set(Calendar.SECOND, 0);
        e.set(Calendar.MILLISECOND, 0);

        // NOTE: take the difference as double and round it so this way we will
        // drop the 1 hour difference in the day length when change to/from
        // day light saving time
        double dif = e.getTimeInMillis() - today.getTimeInMillis();
        return Math.round(dif / (1000 * 60 * 60 * 24)); // millis in day
    }
}