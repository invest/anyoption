package com.anyoption.service.level.market;

/**
 * @author pavelhe
 *
 */
public class MarketState {

	private double lastCalcResult; // formula without random
	private double lastFormulaResult; // formula + random
	private double lastRandomChange;
	private double lastRealLevel; // real level
	private double lastWeekResult;
	private double lastMonthResult;
	private double lastQuarterResult;
	private double oldCalcResult;
	private double oldRealLevel;

	/**
	 * @return the lastCalcResult
	 */
	public double getLastCalcResult() {
		return lastCalcResult;
	}

	/**
	 * @param lastCalcResult the lastCalcResult to set
	 */
	public void setLastCalcResult(double lastCalcResult) {
		this.lastCalcResult = lastCalcResult;
	}

	/**
	 * @return the lastFormulaResult
	 */
	public double getLastFormulaResult() {
		return lastFormulaResult;
	}

	/**
	 * @param lastFormulaResult the lastFormulaResult to set
	 */
	public void setLastFormulaResult(double lastFormulaResult) {
		this.lastFormulaResult = lastFormulaResult;
	}

	/**
	 * @return the lastRandomChange
	 */
	public double getLastRandomChange() {
		return lastRandomChange;
	}

	/**
	 * @param lastRandomChange the lastRandomChange to set
	 */
	public void setLastRandomChange(double lastRandomChange) {
		this.lastRandomChange = lastRandomChange;
	}

	/**
	 * @return the lastRealLevel
	 */
	public double getLastRealLevel() {
		return lastRealLevel;
	}

	/**
	 * @param lastRealLevel the lastRealLevel to set
	 */
	public void setLastRealLevel(double lastRealLevel) {
		this.lastRealLevel = lastRealLevel;
	}

	/**
	 * @return the lastWeekResult
	 */
	public double getLastWeekResult() {
		return lastWeekResult;
	}

	/**
	 * @param lastWeekResult the lastWeekResult to set
	 */
	public void setLastWeekResult(double lastWeekResult) {
		this.lastWeekResult = lastWeekResult;
	}

	/**
	 * @return the lastMonthResult
	 */
	public double getLastMonthResult() {
		return lastMonthResult;
	}

	/**
	 * @param lastMonthResult the lastMonthResult to set
	 */
	public void setLastMonthResult(double lastMonthResult) {
		this.lastMonthResult = lastMonthResult;
	}

	public double getOldCalcResult() {
		return oldCalcResult;
	}

	public void setOldCalcResult(double oldCalcResult) {
		this.oldCalcResult = oldCalcResult;
	}

	public double getOldRealLevel() {
		return oldRealLevel;
	}

	public void setOldRealLevel(double oldRealLevel) {
		this.oldRealLevel = oldRealLevel;
	}

	public double getLastQuarterResult() {
		return lastQuarterResult;
	}

	public void setLastQuarterResult(double lastQuarterResult) {
		this.lastQuarterResult = lastQuarterResult;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder	.append("MarketState ").append(super.toString()).append(" [lastCalcResult=").append(lastCalcResult)
				.append(", lastFormulaResult=").append(lastFormulaResult).append(", lastRandomChange=").append(lastRandomChange)
				.append(", lastRealLevel=").append(lastRealLevel).append(", lastWeekResult=").append(lastWeekResult)
				.append(", lastMonthResult=").append(lastMonthResult).append(", oldCalcResult=").append(oldCalcResult)
				.append(", oldRealLevel=").append(oldRealLevel).append(", lastQuarterResult=").append(lastQuarterResult).append("]");
		return builder.toString();
	}
}