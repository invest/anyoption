package com.anyoption.service.level.market;

import com.anyoption.common.beans.MarketConfig;

public class MarketFactory {
	public static Market createMarket(long typeId, MarketConfig config, String name, long id) throws Exception {
		if (typeId == com.anyoption.common.beans.Market.PRODUCT_TYPE_DYNAMICS) {
			return new MarketDynamics(config, name, id, typeId);
		} else {
			return new Market(config, name, id, typeId);
		}
	}
}