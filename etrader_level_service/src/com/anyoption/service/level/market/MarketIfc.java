/**
 *
 */
package com.anyoption.service.level.market;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.jms.ETraderFeedMessage;
import com.anyoption.common.util.GoldenMinute.Expiry;
import com.anyoption.service.level.market.Market.BackendCommand;

import il.co.etrader.service.level.LevelService;
import il.co.etrader.service.level.SenderThread;

/**
 * @author pavelhe
 *
 */
public interface MarketIfc {

	public static final int UPDATE_ALL_OPPS = 1;
	public static final int UPDATE_TODAY_OPPS = 2;
	public static final int UPDATE_LONG_TERM_OPPS = 3;

	public void subscribeData(LevelService levelService);

	public void subscribeData(LevelService levelService, Map<String, Map<String, String>> subscriptionConfigs);

	public UpdateResult dataUpdate(String dataSubscription, Map<String, Object> fields, List<Opportunity> opportunities);

	public void unsubscribeData(LevelService levelService);

	public void unsubscribeData(LevelService levelService, Map<String, Map<String, String>> subscriptionConfigs);

	public double getClosingLevel(Opportunity o);

	public boolean investmentNotification(	long oppId, double invId, Double amount, long type, double aboveTotal, double belowTotal,
											long skinId);

	public boolean isOnHomePage(int skinId);

	public void setOnHomePage(boolean onHomePage, int skinId, SenderThread sender, long lifeId);

	public boolean isSpecialEntry(Opportunity o);

	public Opportunity getOppById(long id);

	public int getOppsCount();

	public Opportunity getOpp(int i);

	public void updateLastClosedLevel(Opportunity closed);

	public boolean removeOpportunity(long id, SenderThread sender, long lifeId);

	public boolean hasOpenedOpportunities();

	public Opportunity getFirstOpenOpportunity(int... scheduleTypeOrder);

	public Opportunity getOpenedOppByScheduled(int requestedScheduled);

	public boolean hasGoodForHomepage(int skinId);

	public boolean hasOpenedNotExpiringOpportunities();

	public boolean hasOpportunities();

	public String shouldDisableOpportunities();

	public boolean disableOpportunities(String desc);

	public boolean shouldEnableOpportunities();

	public boolean enableOpportunities(String desc, boolean forseEnable);

	public boolean isStateOK();

	public void setStateOK(boolean state);

	public Integer getHomePagePriorityById(int skinId);

	public ArrayList<Opportunity> getOppsToClose();

	public int getOpportunityGroupClose(Opportunity o);

	public String getTimeEstCloseForLevelUpdate(Opportunity o);

	public String getOppStateForLevelUpdate(Opportunity o);

	public void resetOpportunitiesShiftings();

	public Date getLastOppOfTheDay();

	public void resetInTheEndOfTheDay();

	public boolean hasJustOpened();

	public double getGraphLevel(SkinGroup skinGroup);

	public double getDayLevel(SkinGroup skinGroup);

	public boolean hasOnHomePage();

	public void pauseWaitingToPauseLongTermOpps();

	public boolean hasScheduledWaitingToPause(int scheduled);

	public Opportunity getGoldenMinutesOpportunity(Expiry expiry);

	public String getNewFilterScheduled();

	public void sendLevelChangeAlert(double currentCalc);

	public boolean hasGoingToCloseOpportunities();

	public boolean hasOpenNonOneTouchOpportunities();

	public Opportunity getOppInState(int state);

	public boolean isSnapshotReceived();

	public void setSnapshotReceived(boolean snapshotReceived);

	public boolean isSnapshotReceived(Opportunity o);

	public boolean isClosingLevelReceived(Opportunity o);

	public void addOpportunity(Opportunity o, SenderThread sender, long lifeId);

	public void refreshSpecialEntry(SenderThread sender, long lifeId);

	public Opportunity getTimeMarketOpensNext();

	public void sendUpdate(SenderThread sender, long lifeId, int toUpdate, boolean sendCurrentLevel, boolean suspend);

	public void sendUpdate(SenderThread sender, long lifeId, Opportunity o, boolean forseDelete, boolean sendCurrentLevel);

	public void getOpenedOpportunityLevels(Opportunity o, ETraderFeedMessage toSend, SkinGroup skinGroup);

	public boolean isShouldBeClosed(long oppId);

	public boolean isSubscribedData();

	public void openOpportunity(Opportunity o);

	public Map<String, BigDecimal> getQuoteFields();

	public void handleBackendCommands(Map<BackendCommand, String> commands);

}