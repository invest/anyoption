/**
 *
 */
package com.anyoption.service.level.market;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.MarketConfig;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunitySkinGroupMap;
import com.anyoption.common.beans.ReutersQuotes;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.InvestmentType;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.jms.ETraderFeedMessage;
import com.anyoption.common.jms.MonitoringAlertMessage;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.common.util.GoldenMinute.Expiry;
import com.anyoption.service.level.Utils;
import com.anyoption.service.level.condition.MarketDisableCondition;
import com.anyoption.service.level.formula.Formula;
import com.anyoption.service.level.formula.FormulaRole;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import il.co.etrader.bl_vos.Skins;
import il.co.etrader.service.level.LevelService;
import il.co.etrader.service.level.OpportunitiesManager;
import il.co.etrader.service.level.SenderThread;


/**
 * @author pavelhe
 *
 */
public class Market implements MarketIfc {
	
    private static final BigDecimal O7 = new BigDecimal("0.7");
    private static final BigDecimal O8 = new BigDecimal("0.8");
    private static final int PERMITTED_NEXT_SCHEDULED_OPPORTUNITIES = 3;
	
    private static Logger log = Logger.getLogger(Market.class);
    // TODO formulas should be certain class, check with instanceof
	private Formula hourLevelFromula;
	private Formula hourClosingFormula;
	private Formula dayClosingFormula;
	private Formula longTermFormula;
	private Formula realLevelFormula;
	private final List<Formula> formulas;
	private MarketState state;
	protected boolean snapshotReceived;
	private boolean longTermSnapshotReceived;
	protected final String marketName;
	private long marketId;
	protected ArrayList<Opportunity> opportunities;
	private HashMap<Integer, Boolean> onHomePage;
	private long lastUpdateReceiveTime;
	private long todayOpeningTime;
	private boolean stateOK;
//	private long dontCloseAfterXSecNoUpdate; -> in marketParams
    protected Opportunity specialEntry;
	private long disableAfterPeriod;
	private long disabledAfterUpdateAt;
	protected boolean disabled;
	private boolean disabledByMarketConditions;
    /**
     * skins Priorities
     * home page and group Priorities for all skins
     */
    HashMap<Integer, HashMap<String, Integer>> skinsPriorities = new HashMap<Integer, HashMap<String, Integer>>();
	protected boolean suspended;
	protected long decimalPoint;
	private BigDecimal significantPercentage;
	private long lastUpdateTime;
	private int updatesPause;
	private BigDecimal realSignificantPercentage;
	private long lastRealUpdateTime;
	private int realUpdatesPause;
	private double randomCeiling;
	private double randomFloor;
	protected String suspendedMessage;
	private BigDecimal firstShiftParameter;
	private BigDecimal averageInvestments;
	private BigDecimal percentageOfAverageInvestments;
	private BigDecimal fixedAmountForShifting;
	private long typeOfShifting;
	private Long marketGroupId;
	private long optionPlusMarketId;
//	private boolean optionPlusMarket;
//	private boolean binary0100Market;
//	private boolean dynamicsMarket;
	private long typeId;
	private MarketParams marketParams;
	private HashMap<String, Map<String, String>> subscriptionConfigs;
	private int homePagePriority;
	private boolean hasGoodForHomepageCompare; //if this item have good for homepage for skin, only for homepage priorty compare function
	private boolean onHomePageCompare; //if this item have opp on homepage for skin, only for homepage priorty compare function
	private long compareSkinId;
	private List<MarketDisableCondition> marketDisableConditions;
	public enum BackendCommand {
		FXAL_CHANGE;
	}

	public Market(MarketConfig config, String name, long id, long typeId) throws Exception {
		log.trace("Creating market instance for: " + name);
		List<Formula> formulas = new ArrayList<Formula>();
		try {
			if (config.getHourLevelFormulaClass() != null) {
				hourLevelFromula = (Formula) Class.forName(config.getHourLevelFormulaClass()).getConstructor(String.class, FormulaRole.class).newInstance(config.getHourLevelFormulaParams(), FormulaRole.CURRENT_LEVEL);
				formulas.add(hourLevelFromula);
			}
			if (config.getHourClosingFormulaClass() != null) {
				hourClosingFormula = (Formula) Class.forName(config.getHourClosingFormulaClass()).getConstructor(String.class, FormulaRole.class).newInstance(config.getHourClosingFormulaParams(), FormulaRole.HOUR_CLOSING);
				formulas.add(hourClosingFormula);
			}
			if (config.getDayClosingFormulaClass() != null) {
				dayClosingFormula = (Formula) Class.forName(config.getDayClosingFormulaClass()).getConstructor(String.class, FormulaRole.class).newInstance(config.getDayClosingFormulaParams(), FormulaRole.DAY_CLOSING);
				formulas.add(dayClosingFormula);
			}
			if (config.getLongTermFormulaClass() != null) {
				longTermFormula = (Formula) Class.forName(config.getLongTermFormulaClass()).getConstructor(String.class, FormulaRole.class).newInstance(config.getLongTermFormulaParams(), FormulaRole.LONG_TERM);
				formulas.add(longTermFormula);
			}
			if (config.getRealLevelFormulaClass() != null) {
				realLevelFormula = (Formula) Class.forName(config.getRealLevelFormulaClass()).getConstructor(String.class, FormulaRole.class).newInstance(config.getRealLevelFormulaParams(), FormulaRole.REAL_LEVEL);
				formulas.add(realLevelFormula);
			}
		} catch (IllegalArgumentException e) {
			log.error("Cannot load formula, market config: " + config, e);
			throw e;
		} catch (SecurityException e) {
			log.error("Cannot load formula, market config: " + config, e);
			throw e;
		} catch (InstantiationException e) {
			log.error("Cannot load formula, market config: " + config, e);
			throw e;
		} catch (IllegalAccessException e) {
			log.error("Cannot load formula, market config: " + config, e);
			throw e;
		} catch (InvocationTargetException e) {
			log.error("Cannot load formula, market config: " + config, e);
			throw e;
		} catch (NoSuchMethodException e) {
			log.error("Cannot load formula, market config: " + config, e);
			throw e;
		} catch (ClassNotFoundException e) {
			log.error("Cannot load formula, market config: " + config, e);
			throw e;
		} catch (Exception e) {
			log.error("Cannot load formula, market config: " + config, e);
			throw e;
		}
		this.formulas = Collections.unmodifiableList(formulas);
		Gson gson = new GsonBuilder().serializeNulls().create();
		marketParams = gson.fromJson(config.getMarketParams(), MarketParams.class);
		this.state = new MarketState();
		this.subscriptionConfigs = new HashMap<String, Map<String,String>>();
		this.snapshotReceived = false;
        this.longTermSnapshotReceived = false;
        this.marketName = name;
        this.marketId = id;
//        this.handle = handle;
        opportunities = new ArrayList<Opportunity>();
        onHomePage = new HashMap<Integer, Boolean>();//false;
//        options = new ArrayList<Option>();
//        useMainRic = true;
        lastUpdateReceiveTime = System.currentTimeMillis(); // left 0 cause problems if the first update is STALE (market will not be enabled when thing get OK)
        todayOpeningTime = 0;
        stateOK = true;
//        dontCloseAfterXSecNoUpdate = 60;
//        optionPlusMarket = marketName.endsWith("Option+");
//        binary0100Market = marketName.endsWith("Binary0-100");
//        dynamicsMarket = marketName.endsWith("Dynamicssss");
        this.typeId = typeId;
        if (null != config.getMarketDisableConditions() && !config.getMarketDisableConditions().trim().equals("")) {
        	try {
        		Map<String, Map<String, String>> conditionsConfig = gson.fromJson(config.getMarketDisableConditions(), new TypeToken<Map<String, Map<String, String>>>(){}.getType());
        		marketDisableConditions = new ArrayList<MarketDisableCondition>(conditionsConfig.size());
				for (String key : conditionsConfig.keySet()) {
					Map<String, String> conditionConfig = conditionsConfig.get(key);
					// This is done because we don't need it as map but as config object, which here we cannot access since it's private for every condition
					String conditionConfigStr = gson.toJson(conditionConfig);
					marketDisableConditions.add((MarketDisableCondition) Class.forName(key).getConstructor(String.class).newInstance(conditionConfigStr));
				}
        	} catch (Exception e) {
        		log.error("Failed to init MarketDisableConditions." + config.getMarketDisableConditions(), e);
        		throw e;
        	}
        }
	}

	public Market(String name, MarketState state, MarketParams params, double randomCeiling, double randomFloor) {
		this.marketName = name;
		this.state = state;
		this.marketParams = params;
		this.randomCeiling = randomCeiling;
		this.randomFloor = randomFloor;
		this.formulas = Collections.unmodifiableList(new ArrayList<>());
	}

	@Override
	public void subscribeData(LevelService levelService) {		
		for (Formula formula : formulas) {
			formula.collectDataSubscription(subscriptionConfigs);
		}
		levelService.subscribeMarket(this, subscriptionConfigs);
	}

	@Override
	public void subscribeData(LevelService levelService, Map<String, Map<String, String>> subscriptionConfigs) {
		levelService.subscribeMarket(this, subscriptionConfigs);
		this.subscriptionConfigs.putAll(subscriptionConfigs);
	}

	/**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
	@Override
    public UpdateResult dataUpdate(String dataSubscription, Map<String, Object> fields, List<Opportunity> opportunities) {
		UpdateResult result = new UpdateResult();
		lastUpdateReceiveTime = System.currentTimeMillis();
		for (Formula formula : formulas) {
			if (formula.needUpdate(dataSubscription)) {
				formula.dataUpdate(dataSubscription, fields, this, opportunities, result);
			}
		}
    	if (null != marketDisableConditions && hasOpenedNotExpiringOpportunities()) {
    		String desc = null;
    		for (MarketDisableCondition c : marketDisableConditions) {
    			String d = c.shouldDisableMarket(dataSubscription, fields);
    			if (null != d) {
    				if (null != desc) {
    					desc += d;
    				} else {
    					desc = d;
    				}
    			}
    		}
    		if (null != desc) {
    			disableOpportunities(desc);
    			disabledByMarketConditions = true;
    		} else {
    			enableOpportunities("MarketDisableConditions OK", false);
    			disabledByMarketConditions = false;
    		}
    	}
    	haveUpdate(LevelService.getInstance().getSender(), LevelService.getInstance().getLifeId(), true, state.getLastCalcResult(), state.getLastFormulaResult(), true, state.getLastRealLevel());
    	setSnapshotReceived(hourLevelFromula.isSnapshotReceived());
    	if (longTermFormula != null) {
    		setLongTermSnapshotReceived(longTermFormula.isSnapshotReceived());
    	}
    	return result;
    }
	
	@Override
	public void unsubscribeData(LevelService levelService) {
		levelService.unsubscribeMarket(this, subscriptionConfigs);
		subscriptionConfigs.clear();
		for (Formula formula : formulas) {
			formula.resetSubscriptionConfigElements();
		}
	}

	@Override
	public void unsubscribeData(LevelService levelService, Map<String, Map<String, String>> subscriptionConfigs) {
		levelService.unsubscribeMarket(this, subscriptionConfigs);
		this.subscriptionConfigs.keySet().removeAll(subscriptionConfigs.keySet());
	}

//    /**
//     * Notify the item that one of its options have received update.
//     *
//     * @param option the option that received update
//     * @param sender
//     * @param lifeId
//     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
//     */
//    // ??
//    public abstract boolean optionUpdated(Option option, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunities);
	
//    /**
//     * @param o the opportunity for which to calc the level
//     * @return The long term formula result for this item.
//     */
//    // ??
//    protected abstract double longTermFormula(Opportunity o);

	/**
	 * Check if the closing level for specified opportunity has been received.
	 *
	 * @param w the opportunity to check for if the closing level is ready
	 * @return <code>true</code> if the closing level is ready to be taken else <code>false</code>
	 */
    @Override
	public boolean isClosingLevelReceived(Opportunity o) {
		return o.isClosingLevelReceived();
	}

    /**
     * Get the closing level for the specified opportunity.
     *
     * @param o the opportunity for which to get closing level
     * @return The level as received. No formula and exposure control calculations.
     */
    @Override
    public double getClosingLevel(Opportunity o) {
    	long decimal = o.getMarket().getDecimalPoint() - o.getMarket().getDecimalPointSubtractDigits();
    	if (o.isUseManualClosingLevel()) {
            return Utils.roundDouble(o.getManualClosingLevel(), decimal);
        }
    	double cl = Utils.roundDouble(o.getNextClosingLevel(), decimal);
        if (log.isInfoEnabled()) {
            log.info("TRADINGDATA_" + marketName + " expiration - opportunityId: " + o.getId() + " expiration level: " + cl);
        }
        return cl;
    }

	/**
	 * Handle investment notification. Here is where the exposure flow calculations are done as the exposure can change after an investment.
	 *
	 * @param oppId the id of opp on which investment was made
	 * @param invId the investment id
	 * @param amount the investment amount
	 * @param type the type of investment (1 - call/2 - put)
	 * @return <code>true</code> if this investment caused shifting else <code>false</code>.
	 */
	@Override
	public boolean investmentNotification(long oppId, double invId, Double amount, long type, double aboveTotal, double belowTotal,
											long skinId) {
		boolean shifted = false;
		Opportunity opp = getOppById(oppId);
		Calendar nightStartTime = Calendar.getInstance();
		Calendar currentTime = Calendar.getInstance();
		Calendar nightEndTime = Calendar.getInstance();
		nightStartTime.set(Calendar.HOUR_OF_DAY, 22);
		nightStartTime.set(Calendar.MINUTE, 00);
		nightStartTime.set(Calendar.SECOND, 00);
		nightEndTime.set(Calendar.HOUR_OF_DAY, 06);
		nightEndTime.set(Calendar.MINUTE, 00);
		nightEndTime.set(Calendar.SECOND, 00);

		// first update the amounts
		if (type == InvestmentType.CALL) {
			opp.setCalls(opp.getCalls() + amount);
			if (amount > 0.0d /* Make sure this is not investment cancellation */) {
				opp.setCallsCount(opp.getCallsCount() + 1);
				if (skinId == Skin.SKIN_ETRADER) {
					opp.setCallsCountET(opp.getCallsCountET() + 1);
				}
			}
			opp.setCallsShift(opp.getCallsShift() + amount);
		} else {
			opp.setPuts(opp.getPuts() + amount);
			if (amount > 0.0d /* Make sure this is not investment cancellation */) {
				opp.setPutsCount(opp.getPutsCount() + 1);
				if (skinId == Skin.SKIN_ETRADER) {
					opp.setPutsCountET(opp.getPutsCountET() + 1);
				}
			}
			opp.setPutsShift(opp.getPutsShift() + amount);
		}

		if (opp.getOpportunityTypeId() == Opportunity.TYPE_OPTION_PLUS || nightStartTime.before(currentTime)
				|| nightEndTime.after(currentTime)) {
			return false;
		}

		double exposure = opp.getCallsShift() - opp.getPutsShift();
		boolean shouldShift = false;
		double fafs = 0;
		if (null != fixedAmountForShifting) {
			fafs = fixedAmountForShifting.doubleValue();
		}
		double poai = 0;
		if (null != percentageOfAverageInvestments) {
			poai = percentageOfAverageInvestments.doubleValue();
		}
		double ai = 0;
		if (null != averageInvestments) {
			ai = averageInvestments.doubleValue();
		}
		if (typeOfShifting == com.anyoption.common.beans.Market.TYPE_OF_SHIFTING_FIXED_AMOUNT) {
			if (Math.abs(exposure) > fafs * 100) {
				shouldShift = true;
			}
		} else if (typeOfShifting == com.anyoption.common.beans.Market.TYPE_OF_SHIFTING_AVERAGE_INVESTMENT) {
			if (Math.abs(exposure) > poai * 100 * ai) {
				shouldShift = true;
			}
		} else {
			if (Math.abs(exposure) > Math.max(fafs * 100, poai * 100 * ai)) {
				shouldShift = true;
			}
		}
		if (log.isInfoEnabled()) {
			log.info("Investment notification -" + " item: " + marketName + " opportunity: " + oppId + " exposure: " + exposure
						+ " fixedAmountForShifting: " + fixedAmountForShifting + " percentageOfAverageInvestments: "
						+ percentageOfAverageInvestments + " averageInvestments: " + averageInvestments + " typeOfShifting: "
						+ typeOfShifting + " shouldShift: " + shouldShift);
		}
		if (shouldShift) {
			if (!opp.isShifting()) { // first shifting
				opp.setLastShift(firstShiftParameter);
				if (exposure > 0) {
					opp.setAutoShiftParameter(firstShiftParameter.doubleValue());
					// opp.setShiftParameter((new
					// BigDecimal(Double.toString(opp.getShiftParameter()))).add(firstShiftParameter).doubleValue());
					opp.setShiftUp(true);
				} else {
					opp.setAutoShiftParameter(-firstShiftParameter.doubleValue());
					// opp.setShiftParameter((new
					// BigDecimal(Double.toString(opp.getShiftParameter()))).subtract(firstShiftParameter).doubleValue());
					opp.setShiftUp(false);
				}
				opp.setShifting(true);
			} else {
				if ((exposure > 0 && opp.isShiftUp()) || (exposure < 0 && !opp.isShiftUp())) { // shifting on the same direction
					opp.setLastShift(opp.getLastShift().multiply(O8));
				} else {
					opp.setLastShift(opp.getLastShift().multiply(O7));
				}
				if (exposure > 0) {
					opp.setAutoShiftParameter((new BigDecimal(Double.toString(opp.getAutoShiftParameter()))).add(opp.getLastShift())
																											.doubleValue());
					opp.setShiftUp(true);
				} else {
					opp.setAutoShiftParameter((new BigDecimal(Double.toString(opp.getAutoShiftParameter()))).subtract(opp.getLastShift())
																											.doubleValue());
					opp.setShiftUp(false);
				}
			}
			try {
				OpportunitiesManager.insertShifting(oppId, opp.getAutoShiftParameter(), opp.getLastShift().doubleValue(), exposure > 0,
													SkinGroup.AUTO);
			} catch (SQLException sqle) {
				log.error("Can't log shifting in the db.", sqle);
			}
			opp.setCallsShift(0);
			opp.setPutsShift(0);
			shifted = true;
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyyy HH:mm:ss");

			StringBuilder sb = new StringBuilder();
			sb.append("Market: ").append(marketName).append(" opportunity: ").append(oppId).append(" time: ")
				.append(df.format(new Date(System.currentTimeMillis()))).append(" current shift: ").append(opp.getLastShift())
				.append(" total auto shift: ").append(new BigDecimal(Double.toString(opp.getAutoShiftParameter())));
			for (Map.Entry<SkinGroup, OpportunitySkinGroupMap> entry : opp.getSkinGroupMappings().entrySet()) {
				sb.append(" manual shift ").append(entry.getKey()).append(": ")
					.append(new BigDecimal(Double.toString(entry.getValue().getShiftParameter())));
			}
			LevelService.getInstance().sendAlertEmail("Shifting " + marketName + " " + oppId, sb.toString());
		}
		// TODO: Tony: log processed
		if (log.isInfoEnabled()) {
			log.info("calls " + opp.getCalls() + " puts " + opp.getPuts() + " callsShift: " + opp.getCallsShift() + " putsShift "
						+ opp.getPutsShift() + " shifted " + shifted);
		}
		return shifted;
	}

	/**
	 * @param skinId id of the skin to check if the market is on homepage
	 * @return the onHomePage
	 */
    @Override
	public boolean isOnHomePage(int skinId) {
		if (onHomePage.containsKey(skinId)) {
			return onHomePage.get(skinId);
		}
		return false;
	}

	/**
	 * Change the state (on home page/not on home page) of this market. If the market was not on the home page then pick an opportunity to
	 * show for it on the home page and send update for it. If it was and is no longer send the corresponding update - "DELETE" (from the
	 * cache) if it is a created one else just "UPDATE" (the levels cache will handle the removing from the LS).
	 *
	 * @param onHomePage the onHomePage to set
	 * @param skinId the skin id to change homepage flag in the array
	 * @param sender handler to send update about the opp on the home page from this market
	 * @param lifeId the service life id
	 */
    @Override
	public void setOnHomePage(boolean onHomePage, int skinId, SenderThread sender, long lifeId) {
		if (log.isDebugEnabled()) {
			log.debug("setOnHomePage for " + marketName + " skinId: " + skinId + ". was: " + this.onHomePage + " setting to: " + onHomePage);
		}
		this.onHomePage.put(skinId, onHomePage);
		refreshSpecialEntry(sender, lifeId);
		// the refreshSpecialEntry can't catch the case when closed market
		// move in or out of the home page
		sendUpdate(sender, lifeId, UPDATE_TODAY_OPPS, true, false);
	}

	/**
	 * Add opportunity to the RICs list.
	 *
	 * @param o
	 * @param sender handler to send update about the opp on the home page from this market
	 * @param lifeId the service life id
	 */
    @Override
	public void addOpportunity(Opportunity o, SenderThread sender, long lifeId) {
		if (log.isDebugEnabled()) {
			log.debug("Adding opp: " + o.getMarket().getFeedName() + " state: " + o.getState() + " Time: " + o.getTimeEstClosing());
		}
		opportunities.add(o);
		// homePagePriority = o.getMarket().getHomePagePriority(); no need any more
		// homePageHourFirst = o.getMarket().isHomePageHourFirst(); i thing we dont need it anymore
		decimalPoint = o.getMarket().getDecimalPoint();
		updatesPause = o.getMarket().getUpdatesPause();
		significantPercentage = o.getMarket().getSignificantPercentage();
		realUpdatesPause = o.getMarket().getRealUpdatesPause();
		realSignificantPercentage = o.getMarket().getRealSignificantPercentage();
		marketId = o.getMarket().getId();
		optionPlusMarketId = o.getMarket().getOptionPlusMarketId();
		setRandomCeiling(o.getMarket().getRandomCeiling().doubleValue());
		setRandomFloor(o.getMarket().getRandomFloor().doubleValue());
		firstShiftParameter = o.getMarket().getFirstShiftParameter();
		averageInvestments = o.getMarket().getAverageInvestments();
		percentageOfAverageInvestments = o.getMarket().getPercentageOfAverageInvestments();
		fixedAmountForShifting = o.getMarket().getFixedAmountForShifting();
		typeOfShifting = o.getMarket().getTypeOfShifting();
		disableAfterPeriod = o.getMarket().getDisableAfterPeriod();

		// take the disabled flag only from the first opp on startup.
		// avoid taking the flag of newly created opps (which is always enabled = lose disabled during create time)
		if (opportunities.size() == 1) {
			disabled = o.isDisabledService();
			if (disabled) {
				disabledAfterUpdateAt = 1; // set something so the enable will work
			}
		}
		refreshSpecialEntry(sender, lifeId);
		suspended = o.getMarket().isSuspended();
		suspendedMessage = o.getMarket().getSuspendedMessage();
		// bugid: 2029
		// frequencePriority = o.getMarket().getFrequencePriority();
		marketGroupId = o.getMarket().getGroupId();
	}

	/**
	 * While market is opened every opportunity that is in opened state is displayed separately and get updates separately. While market is
	 * closed or suspended we want single "row" in the trading pages main table to represent that market.
	 *
	 * There is one more special case - home page. On the home page we want to display single opportunity for market. Depending on the
	 * config we may want the hour opp that will close next or the day opp that will close today.
	 *
	 * "Special entry" is the entry that is selected to be displayed on home page (when sending updates only to this one a flag is set that
	 * says to the Lightstreamer adapter this opportunity should be forwarded to the "home" subscribtion) or to represent the market in
	 * closed/suspended states. In the closed state the opportunity that will open next is selected. In suspended state the one that will
	 * close next is used.
	 *
	 * @param sender
	 * @param lifeId
	 */
    @Override
	public void refreshSpecialEntry(SenderThread sender, long lifeId) {
		Opportunity oldSpecialEntry = specialEntry;
		specialEntry = null;
		boolean hasOpened = hasOpenedOpportunities();
		if (hasOnHomePage() || !hasOpened || !isSnapshotReceived() || suspended) {
			if (log.isTraceEnabled()) {
				log.trace("Pick new special entry. onHomePage: " + onHomePage + " hasOpened: " + hasOpened + " suspended: " + suspended);
			}
			if (hasOpened && isSnapshotReceived()) {
				// we are looking for home page entry (the next that will close)
				Opportunity o = null;
				for (int i = 0; i < opportunities.size(); i++) {
					o = opportunities.get(i);
//	                  if (o.getOpportunityTypeId() != Opportunity.TYPE_REGULAR) {
//	                	  continue;
//	                  }
//		              if (null == specialEntry ||
//		                      (o.isInOppenedState() && !specialEntry.isInOppenedState()) ||
//		                      (o.isInOppenedState() && /*homePageHourFirst &&*/ o.getTimeEstClosing().compareTo(specialEntry.getTimeEstClosing()) < 0) //||
//		                   /* (o.isInOppenedState() && !homePageHourFirst && o.getScheduled() == Opportunity.SCHEDULED_DAYLY) ||
//		                      (o.isInOppenedState() && !homePageHourFirst && specialEntry.getScheduled() == Opportunity.SCHEDULED_HOURLY) ||
//		                      (o.isInOppenedState() && o.isLongTerm() && specialEntry.isLongTerm() && o.getTimeEstClosing().compareTo(specialEntry.getTimeEstClosing()) < 0)
//		                   */
//		                      ) {
//		                  specialEntry = o;
//		              }
					if (specialEntry == null || (o.isInOppenedState() && !specialEntry.isInOppenedState())
							|| (o.isInOppenedState() && o.getTimeEstClosing().compareTo(specialEntry.getTimeEstClosing()) < 0)) {
						specialEntry = o;
					}
				}
			} else {
				specialEntry = getTimeMarketOpensNext();
			}
		}
		if (null != specialEntry) {
			if (log.isTraceEnabled()) {
				log.trace("Picked up: " + specialEntry.getId()
							+ (null != oldSpecialEntry ? " Old was: " + oldSpecialEntry.getId() : " No old."));
			}
		} else {
			if (log.isTraceEnabled()) {
				log.trace("No new special entry." + (null != oldSpecialEntry ? " Old was: " + oldSpecialEntry.getId() : " No old."));
			}
		}
		if (null != oldSpecialEntry && specialEntry != oldSpecialEntry) {
			if (log.isTraceEnabled()) {
				log.trace("Removing: " + oldSpecialEntry.getId());
			}
			// When removing (sending the "DELETE" command) the old special
			// entry it still need to be special entry.
			// Else it will not be removed from the home page!
			Opportunity newSpecialEntry = specialEntry;
			specialEntry = oldSpecialEntry;
			sendUpdate(sender, lifeId, oldSpecialEntry, true, true);
			specialEntry = newSpecialEntry;
		}
		if (null != specialEntry && specialEntry != oldSpecialEntry) {
			if (log.isTraceEnabled()) {
				log.trace("Adding: " + specialEntry.getId());
			}
			sendUpdate(sender, lifeId, specialEntry, false, true);
		}
	}

	/**
	 * @return <code>Opportunity</code> that can be used as special entry in the case when the market is closed or to take the markets time
	 *         next open from the timeNextOpen property.
	 */
    @Override
	public Opportunity getTimeMarketOpensNext() {
		boolean hasOpened = hasOpenedOpportunities();
		Opportunity se = null;
		Opportunity o = null;
		boolean hasLongTerm = false;

		// we are looking for the time the market will open next
		// will be done in two steps:
		// step 1 - find the short term opp (hour/day) that will open next
		for (int i = 0; i < opportunities.size(); i++) {
			o = opportunities.get(i);
			// if (o.getOpportunityTypeId() != Opportunity.TYPE_REGULAR) {
			// continue;
			// }
			if (o.getScheduled() == Opportunity.SCHEDULED_WEEKLY	|| o.getScheduled() == Opportunity.SCHEDULED_MONTHLY
					|| o.getScheduled() == Opportunity.SCHEDULED_QUARTERLY) {
				hasLongTerm = true;
				continue;
			}
			if (o.getState() != Opportunity.STATE_CREATED || o.getScheduled() == Opportunity.SCHEDULED_DAYLY) {
				continue;
			}
			if (hasOpened && Utils.isToday(o.getTimeFirstInvest(), "GMT+00")) {
				continue;
			}
			if (null == se || (o.getTimeFirstInvest().compareTo(se.getTimeFirstInvest()) < 0)) {
				se = o;
			}
		}

		// step 2 - by the short term opp we know the next working day
		// now check if we have long term and if one of them will open earlyer
		// on that day
		if (null != se) {
			se.setTimeNextOpen(se.getTimeFirstInvest());
			for (int i = 0; i < opportunities.size(); i++) {
				o = opportunities.get(i);
				// if (o.getOpportunityTypeId() != Opportunity.TYPE_REGULAR) {
				// continue;
				// }
				if (o.isLongTerm() && o.getState() == Opportunity.STATE_PAUSED) { // doesn't really matter which one we will take
																					// (week/month)
					Calendar sec = Calendar.getInstance();
					sec.setTime(se.getTimeFirstInvest());

					Calendar longFirstInv = Calendar.getInstance();
					longFirstInv.setTime(o.getTimeFirstInvest());

					Calendar longLastInv = Calendar.getInstance();
					longLastInv.setTime(o.getTimeLastInvest());

					// compose the time this long term opp will open on that day
					Calendar longStartTime = Calendar.getInstance();
					longStartTime.set(Calendar.YEAR, sec.get(Calendar.YEAR));
					longStartTime.set(Calendar.MONTH, sec.get(Calendar.MONTH));
					longStartTime.set(Calendar.DAY_OF_MONTH, sec.get(Calendar.DAY_OF_MONTH));
					longStartTime.set(Calendar.HOUR_OF_DAY, longFirstInv.get(Calendar.HOUR_OF_DAY));
					longStartTime.set(Calendar.MINUTE, longFirstInv.get(Calendar.MINUTE));
					longStartTime.set(Calendar.SECOND, 0);
					longStartTime.set(Calendar.MILLISECOND, 0);

					// compose the time this long term opp will end on that day
					Calendar longEndTime = Calendar.getInstance();
					longEndTime.set(Calendar.YEAR, sec.get(Calendar.YEAR));
					longEndTime.set(Calendar.MONTH, sec.get(Calendar.MONTH));
					longEndTime.set(Calendar.DAY_OF_MONTH, sec.get(Calendar.DAY_OF_MONTH));
					longEndTime.set(Calendar.HOUR_OF_DAY, longLastInv.get(Calendar.HOUR_OF_DAY));
					longEndTime.set(Calendar.MINUTE, longLastInv.get(Calendar.MINUTE));
					longEndTime.set(Calendar.SECOND, 0);
					longEndTime.set(Calendar.MILLISECOND, 0);

					Calendar cnow = Calendar.getInstance(); // the time now

					if (longStartTime.compareTo(sec) < 0 && cnow.before(longStartTime) && sec.compareTo(longEndTime) < 0) { // check if the
																															// long is b4
																															// the daily and
																															// we didn't pass
																															// the long
																															// start time
						o.setTimeNextOpen(longStartTime.getTime());
						se = o;
						break;
					}
				}
			}
		} else { // we dont have hourly
			log.debug("found market without hourly market_id" + marketId);
			Date nextOpenDateFromDb = null;
			try {
				nextOpenDateFromDb = OpportunitiesManager.getNextOpenDate(this.marketId);
			} catch (Exception e) {
				log.debug("cant get next open date from db", e);
			}
			if (null != nextOpenDateFromDb) {
				for (int i = 0; i < opportunities.size(); i++) {
					o = opportunities.get(i);
					if (o.getScheduled() == Opportunity.STATE_PAUSED || o.getScheduled() == Opportunity.STATE_WAITING_TO_PAUSE) {
						se = o;
					}
				}
				if (null == se) {
					se = opportunities.get(0);
				}
				se.setTimeNextOpen(nextOpenDateFromDb);
				log.debug("set time next open from db to" + nextOpenDateFromDb.toString());
				return se;
			}
		}

		// if we have only long term then choose the closest one
		if (hasLongTerm && se == null) {
			for (int i = 0; i < opportunities.size(); i++) {
				o = opportunities.get(i);
				// if (o.getOpportunityTypeId() != Opportunity.TYPE_REGULAR) {
				// continue;
				// }
				if (o.getScheduled() == Opportunity.SCHEDULED_WEEKLY	|| o.getScheduled() == Opportunity.SCHEDULED_MONTHLY
						|| o.getScheduled() == Opportunity.SCHEDULED_QUARTERLY) {
					if (null == se || (o.getTimeFirstInvest().compareTo(se.getTimeFirstInvest()) < 0)) {
						se = o;
						se.setTimeNextOpen(se.getTimeFirstInvest());
					}
				}
			}
		}
		return se;
	}

    /**
     * Check if specified opportunity is the special entry for this market.
     *
     * @param o the opportunity to check for
     * @return <code>true</code> if the specified opportunity is the special entry else <code>false</code>.
     */
    @Override
    public boolean isSpecialEntry(Opportunity o) {
    	return o == specialEntry;
    }

    /**
     * Get opportunity with specified id from the opps list.
     *
     * @param id the id to look for
     * @return <code>Opportunity</code> of the opp with specified id or null if not in the list.
     */
    @Override
    public Opportunity getOppById(long id) {
    	Opportunity o = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            if (o.getId() == id) {
                return o;
            }
        }
        return null;
    }

	/**
	 * @return The count of opps connected to this item.
	 */
    @Override
	public int getOppsCount() {
		return opportunities.size();
	}

	/**
	 * The i-th opp connected to this item.
	 *
	 * @param i the index of the opp to return
	 * @return <code>Opportunity</code> of the i-th opp of this item.
	 */
    @Override
	public Opportunity getOpp(int i) {
		return opportunities.get(i);
	}

	/**
	 * Update the level of the opps that are not opened with the level of the opportunity that have just closed.
	 *
	 * @param closed the opportunity that just closed
	 */
    @Override
	public void updateLastClosedLevel(Opportunity closed) {
		double closingLevel = getClosingLevel(closed);
		Opportunity w = null;
		for (Iterator<Opportunity> i = opportunities.iterator(); i.hasNext();) {
			w = i.next();
			if (w != closed && w.getState() != Opportunity.STATE_CLOSED && w.getState() != Opportunity.STATE_DONE) {
				w.setClosingLevel(closingLevel);
			}
		}
	}

	/**
	 * Update the market opportunities closing level. The opportunity closing level is updated according to the opportunity type
	 * (HOUR/DAY...). The closing level value is updated only if the closing time is after the current time.
	 *
	 * @param crrTime the time of the update
	 * @param newHourClosingLevel
	 * @param newDayClosingLevel
	 * @param closingLevelTxtDay
	 * @param closingLevelTxtHour
	 * @param opportunities2
	 * @return <code>true</code> if there is an opened opportunity which closing time is before this update (and was not updated - which
	 *         mean it is ready to be closed) else <code>false</code>.
	 */
	public void updateOpportunitiesClosingLevel(Date crrTime, double newHourClosingLevel, double newDayClosingLevel,
															String closingLevelTxtHour, String closingLevelTxtDay,
															ReutersQuotes currentHourlyQuote, ReutersQuotes currentDailyQuote,
															List<Opportunity> opportunitiesTemp, UpdateResult result) {
		Opportunity o = null;
		if (null == opportunitiesTemp) {
			opportunitiesTemp = this.opportunities;
		}
		for (Iterator<Opportunity> i = opportunitiesTemp.iterator(); i.hasNext();) {
			o = i.next();
			if (o.isInOppenedState()) {
				if (crrTime.compareTo(o.getTimeEstClosing()) < 0) {
					if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
						if  (closingLevelTxtHour != null) {
							o.setNextClosingLevel(newHourClosingLevel);
							o.setCloseLevelTxt(closingLevelTxtHour);
							if (null != currentHourlyQuote) {
								o.setLastAndBeforeUpdateQuote(currentHourlyQuote);
							}
						}
					} else {
						if (closingLevelTxtDay != null) {
							o.setNextClosingLevel(newDayClosingLevel);
							o.setCloseLevelTxt(closingLevelTxtDay);
							if (null != currentDailyQuote) {
								o.setLastAndBeforeUpdateQuote(currentDailyQuote);
							}
						}
					}
				} else {
					if (!o.isClosingLevelReceived()) {
						o.setClosingLevelReceived(true);
						result.getOpportunitiesToClose().add(o);
						if (null != currentHourlyQuote) {
							o.setFirstUpdateQuote(currentHourlyQuote);
						}
						if (log.isInfoEnabled()) {
							log.info("Got closing level for " + marketName + " oppId: " + o.getId() + " closing level: "
										+ o.getNextClosingLevel() + " update time: " + crrTime + " closing level txt: "
										+ o.getCloseLevelTxt());
						}
					}
				}
			}
		}
	}

	/**
	 * Remove the opportunity with specified id.
	 *
	 * @param id the id of the opp to remove
	 * @return <code>true</code> if the opp was removed else <code>false</code> (it will not be removed if it is not found).
	 */
    @Override
	public boolean removeOpportunity(long id, SenderThread sender, long lifeId) {
		boolean removed = false;
		boolean removedSpecial = false;
		Opportunity o = null;
		for (Iterator<Opportunity> i = opportunities.iterator(); i.hasNext();) {
			o = i.next();
			if (o.getId() == id) {
				if (o == specialEntry) {
					removedSpecial = true;
				}
				i.remove();
				removed = true;
			}
		}
		// if home or the last opened is removed - need new special
		if (removedSpecial || !hasOpenedOpportunities()) {
			refreshSpecialEntry(sender, lifeId);
		}
		return removed;
	}

	/**
	 * @return <code>true</code> if this item has at least one opp in opened state else <code>false</code>.
	 */
    @Override
	public boolean hasOpenedOpportunities() {
		if (opportunities.size() == 0) {
			return false;
		}
		for (int i = 0; i < opportunities.size(); i++) {
			if (opportunities.get(i).isInOppenedState()) {
				return true;
			}

		}
		return false;
	}

	/**
	 * @param scheduleTypeOrder
	 * @return Returns the first open opportunity of the item with the given schedule type priority.
	 */
    @Override
	public Opportunity getFirstOpenOpportunity(int... scheduleTypeOrder) {
		log.trace("Looking for open market [" + getMarketId() + "] opportunity with schedule type priority order [" + scheduleTypeOrder
					+ "]");
		for (int scheduleType : scheduleTypeOrder) {
			for (Opportunity opportunity : opportunities) {
				if (opportunity.isAvailableForTrading() && opportunity.getScheduled() == scheduleType) { // FIXME State may be open but
																											// snapshot may not be received
																											// - please, check for snapshot
																											// too or the shown level will
																											// be 0.0 which is not good
					log.trace("Found [" + opportunity + "]");
					return opportunity;
				}
			}
		}

		log.trace("Could not find open opportunity");
		return null;
	}

	/**
	 * @return <code>true</code> if there is opened opportunity of the specified type on this market at the moment else <code>false</code>.
	 */
	private boolean hasOpened(int requestedScheduled) {
		if (opportunities.size() == 0) {
			return false;
		}
		Opportunity o = null;
		for (int i = 0; i < opportunities.size(); i++) {
			o = opportunities.get(i);
			if (o.isInOppenedState() && o.getScheduled() == requestedScheduled) {
				return true;
			}
		}
		return false;
	}
    
	/**
	 * @return <code>Opportunity</code> if there is opened opportunity of the specified type on this market at the moment else
	 *         <code>null</code>.
	 */
	@Override
	public Opportunity getOpenedOppByScheduled(int requestedScheduled) {
		if (opportunities.size() == 0) {
			return null;
		}
		Opportunity o = null;
		/*
		 * For the hour opps there is a chance there are two opportunities that are opened. This can happen when the previous hour is either
		 * closed and waiting to be removed or if the closing of the previous hour get delayed due to waiting for closing level. In such
		 * cases we will return the new (the current hour) opportunity. To do so if we run at hour opportunity that is closing or is closed
		 * we will keep reference to it giving the opportunity to the for loop to find and return the other hour opp (the new one). If no
		 * other hour opp then we will return the one we found.
		 */
		Opportunity hourWaitingForExpiry = null;
		for (int i = 0; i < opportunities.size(); i++) {
			o = opportunities.get(i);
			if (o.isInOppenedState() && o.getScheduled() == requestedScheduled) {
				if (requestedScheduled == Opportunity.SCHEDULED_HOURLY
						&& (o.getState() == Opportunity.STATE_CLOSING || o.getState() == Opportunity.STATE_CLOSED)) {
					hourWaitingForExpiry = o;
				} else {
					return o;
				}
			}
		}
		return hourWaitingForExpiry;
	}
    
    /**
     * @return <code>true</code> if there is opened hourly opportunity on this market at the moment else <code>false</code>.
     */
    public boolean hasOpenedHourly() {
        return hasOpened(Opportunity.SCHEDULED_HOURLY);
    }

    /**
     * @return <code>true</code> if there is opened weekly opportunity on this market at the moment else <code>false</code>.
     */
    private boolean hasOpenedWeekly() {
        return hasOpened(Opportunity.SCHEDULED_WEEKLY);
    }

    private boolean hasOpenedMonthly() {
    	return hasOpened(Opportunity.SCHEDULED_MONTHLY);
    }

	/**
	 * @param skinId index of the skin in the array.
	 * @return <code>true</code> if the markets has hourly opportunity you can invest on at the moment else <code>false</code>.
	 */
    @Override
	public boolean hasGoodForHomepage(int skinId) {
		if (opportunities.size() == 0) {
			return false;
		}
		boolean hasOpenedHourly = hasOpenedHourly();
		Opportunity o = null;
		for (int i = 0; i < opportunities.size(); i++) {
			o = opportunities.get(i);
			if (((hasOpenedHourly && o.getScheduled() == Opportunity.SCHEDULED_HOURLY) || (!hasOpenedHourly && o.getScheduled() == Opportunity.SCHEDULED_DAYLY))
					&& (onHomePage.get(skinId) == null ? false : onHomePage.get(skinId))
					&& (o.getState() >= Opportunity.STATE_OPENED && o.getState() < Opportunity.STATE_CLOSING_1_MIN)) {
				return true;
			}
			if (((hasOpenedHourly && o.getScheduled() == Opportunity.SCHEDULED_HOURLY) || (!hasOpenedHourly && o.getScheduled() == Opportunity.SCHEDULED_DAYLY))
					&& (onHomePage.get(skinId) == null ? true : !onHomePage.get(skinId)) && (o.getState() == Opportunity.STATE_OPENED)) {
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.MINUTE, 20);
				Calendar cls = Calendar.getInstance();
				cls.setTime(o.getTimeEstClosing());
				if (cal.compareTo(cls) < 0) {
					return true;
				}

			}

		}
		return false;
	}

	private boolean hasHour() {
		if (opportunities.size() == 0) {
			return false;
		}
		Opportunity o = null;
		for (int i = 0; i < opportunities.size(); i++) {
			o = opportunities.get(i);
			if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @return <code>true</code> if there are long term opportunities for this market else <code>false</code>.
	 */
	private boolean hasLongTerm() {
		if (opportunities.size() == 0) {
			return false;
		}
		Opportunity o = null;
		for (int i = 0; i < opportunities.size(); i++) {
			o = opportunities.get(i);
			if (o.getScheduled() == Opportunity.SCHEDULED_WEEKLY	|| o.getScheduled() == Opportunity.SCHEDULED_MONTHLY
					|| o.getScheduled() == Opportunity.SCHEDULED_QUARTERLY) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @return <code>true</code> if this item has at least one opp in opened or last 10 min state else <code>false</code>.
	 */
    @Override
	public boolean hasOpenedNotExpiringOpportunities() {
		if (opportunities.size() == 0) {
			return false;
		}
		Opportunity o = null;
		int state = 0;
		for (int i = 0; i < opportunities.size(); i++) {
			o = opportunities.get(i);
			state = o.getState();
			if (state == Opportunity.STATE_OPENED || state == Opportunity.STATE_LAST_10_MIN) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @return If the item has any opportunities left.
	 */
    @Override
	public boolean hasOpportunities() {
		return opportunities.size() > 0;
	}

	/**
	 * @return The opportunity on this market with closest closing time.
	 */
	private Opportunity getClosestToClose() {
		Opportunity ctc = null;
		Opportunity o = null;
		for (int i = 0; i < opportunities.size(); i++) {
			o = opportunities.get(i);
			if (null == ctc || o.getTimeEstClosing().compareTo(ctc.getTimeEstClosing()) < 0) {
				ctc = o;
			}
		}
		return ctc;
	}

	/**
	 * @return Description why the opps should be disabled or empty string if the opps should not be disabled.
	 */
    @Override
	public String shouldDisableOpportunities() {
    	String desc = "";
		// make sure we got one so we don't disable all on startup
		if (disableAfterPeriod > 0 && isSnapshotReceived() && lastUpdateReceiveTime != disabledAfterUpdateAt
				&& System.currentTimeMillis() - lastUpdateReceiveTime >= disableAfterPeriod && hasOpenedNotExpiringOpportunities()) {
			desc = "No updates received for market " + marketName + " for " + (System.currentTimeMillis() - lastUpdateReceiveTime) + " milliseconds";
		}
    	if (null != marketDisableConditions && hasOpenedNotExpiringOpportunities()) {
    		for (MarketDisableCondition c : marketDisableConditions) {
    			String d = c.shouldDisableMarket();
    			if (null != d) {
    				desc += d;
    			}
    		}
    	}
		return desc;
	}

	/**
	 * Disable not settled opportunities on this market.
	 *
	 * @param desc description to put in the log records
	 * @return <code>true</code> if any opps were disabled else <code>false</code>.
	 */
    @Override
	public boolean disableOpportunities(String desc) {
		if (!disabled) {
			if (log.isTraceEnabled()) {
				log.trace("Disable " + marketName + " desc: " + desc);
			}
			try {
				if (OpportunitiesManager.disableMarketOpportunities(marketId, desc) > 0) {
					disabledAfterUpdateAt = lastUpdateReceiveTime;
					disabled = true;
					MonitoringAlertMessage a = new MonitoringAlertMessage(marketId, true, desc);
					LevelService.getInstance().sendMessage(a);
					return true;
				}
			} catch (Throwable t) {
				log.error("Can't disable for " + marketName, t);
			}
		} else {
			if (log.isTraceEnabled()) {
				log.trace("Market " + marketName + " already disabled.");
			}
		}
		return false;
	}

	/**
	 * @return <code>true</code> if the opened opportunities of this item should be enabled else <code>false</code>.
	 */
    @Override
	public boolean shouldEnableOpportunities() {
		if (log.isTraceEnabled()) {
			log.trace("Market.shouldenableOpportunities for " + marketName + " - " + " disableAfterPeriod: " + disableAfterPeriod
						+ " disabledAfterUpdateAt: " + disabledAfterUpdateAt + " lastUpdateReceiveTime: " + lastUpdateReceiveTime
						+ " hasOpenedNotExpiringOpportunities: " + hasOpenedNotExpiringOpportunities() + " isStateOK: " + isStateOK());
		}
		if (!disabledByMarketConditions	&& disableAfterPeriod > 0 && disabledAfterUpdateAt > 0
				&& System.currentTimeMillis() - lastUpdateReceiveTime < disableAfterPeriod && hasOpenedNotExpiringOpportunities()
				&& isStateOK()) {
			log.warn("Updates resumed for market " + marketName);
			return true;
		}
		return false;
	}

	/**
	 * Enable opened opportunities on this market that were disabled by the service.
	 *
	 * @param desc description to put in the log records
	 * @param forseEnable <code>true</code> if to execute db update no matter what the value of the internal flag is else <code>false</code>
	 * @return <code>true</code> if any opps were enabled else <code>false</code>.
	 */
    @Override
	public boolean enableOpportunities(String desc, boolean forseEnable) {
		if (disabled || forseEnable) {
			if (log.isTraceEnabled()) {
				log.trace("Enable " + marketName + " desc: " + desc);
			}
			try {
				if (OpportunitiesManager.enableMarketOpportunities(marketId, desc) > 0) {
					disabledAfterUpdateAt = 0;
					disabled = false;
					MonitoringAlertMessage a = new MonitoringAlertMessage(marketId, false, desc);
					LevelService.getInstance().sendMessage(a);
					return true;
				}
			} catch (Throwable t) {
				log.error("Can't enable for " + marketName, t);
			}
		} else {
			if (log.isTraceEnabled()) {
				log.trace("Market " + marketName + " already enabled.");
			}
		}
		return false;
	}
    
    @Override
    public boolean isStateOK() { //isReutersStateOK
    	return stateOK;
    }
    
    // TODO more than one RIC subscribed
    @Override
    public void setStateOK(boolean state) { //setReutersStateOK
    	if (stateOK != state) {
    		StringBuilder sb = new StringBuilder("Changing stateOK on market ");
    		sb.append(marketName).append("(marketId = ").append(marketId).append(") from ").append(stateOK).append(" to ").append(state);
    		log.trace(sb.toString());
    		stateOK = state;
    		String desc = stateOK + " state from Provider";
    		if (!stateOK) {
    			disableOpportunities(desc);
    		} else {
    			if (shouldEnableOpportunities()) {
    				enableOpportunities(desc, false);
    			}
    		}
    		LevelService.getInstance().sendAlertEmail((stateOK ? "Disabled " : "Enabled ") + marketName, desc);
    	}
    }

	/**
	 *
	 * @param skin id
	 * @return The home page priority of this item (market) by skin id.
	 *
	 */
    @Override
	public Integer getHomePagePriorityById(int skinId) {
		if (skinsPriorities.containsKey(skinId)) {
			return skinsPriorities.get(skinId).get("home_page_priority");
		}
		return null;
	}

	/**
	 * @return <code>ArrayList<Opportunity></code> with the opps that should be closed (have the timeToClose flag on and have received
	 *         closing level).
	 */
    @Override
	public ArrayList<Opportunity> getOppsToClose() {
		ArrayList<Opportunity> l = new ArrayList<Opportunity>();
		for (int i = 0; i < opportunities.size(); i++) {
			Opportunity o = opportunities.get(i);
			if (o.getState() != Opportunity.STATE_CLOSED && o.isTimeToClose() && isClosingLevelReceived(o)) {
				l.add(o);
			}
		}
		return l;
	}

	/**
	 * Perform exposure control.
	 *
	 * @param o the opportunity to check
	 * @param formulaResult the result of the formula calculation to pass through the exposure flow
	 * @param skinGroup the skin group wich holds the corresponding shift parameter
	 * @return The opportunity level.
	 */
	private double exposure(Opportunity o, double formulaResult, SkinGroup skinGroup) {
		return formulaResult * (1 + o.getSkinGroupMappings().get(skinGroup).getShiftParameter() + o.getAutoShiftParameter());
	}

	/**
	 * Prepare the <code>HashMap</code> with the values for the LS update.
	 *
	 * @param o
	 * @param levelET the current opportunity level for ET
	 * @param levelAO the current opportunity level for AO
	 * @param forseDelete if to forse "DELETE" command or to determine the command by the state
	 */
	private HashMap<String, String> getOpportunityLevelUpdate(Opportunity o, Map<SkinGroup, LevelsBean> levelsMap, boolean forseDelete) {
		HashMap<String, String> flds = new HashMap<String, String>();
		// flds.put("ET_NAME", o.getMarket().getDisplayName());
		// send it by id and translate it to name with js
		flds.put("ET_NAME", String.valueOf(o.getMarketId()));

		flds.put("ET_ODDS", Utils.formatOdds(o.getOverOddsWin()));

		// use the time field for the suspended message or date
		// SimpleDateFormat df = new SimpleDateFormat("yyy-MM-dd HH:mm");
		// df.
		flds.put("ET_EST_CLOSE", getTimeEstCloseForLevelUpdate(o));
		flds.put("ET_LAST_INV", Utils.formatDate(o.getTimeLastInvest()));
		// flds.put("ET_EST_CLOSE", formatDate(o.getState() == Opportunity.STATE_CREATED ? o.getTimeFirstInvest() : o.getTimeEstClosing()));
		flds.put("ET_OPP_ID", String.valueOf(o.getId()));
		flds.put("ET_ODDS_WIN", Utils.formatFloat(1 + o.getOverOddsWin()));
		flds.put("ET_ODDS_LOSE", Utils.formatFloat(1 - o.getOddsType().getOverOddsLose()));
		flds.put("ET_ODDS_GROUP", o.getOddsGroup());
		flds.put("command", (forseDelete || o.getState() == Opportunity.STATE_DONE ? "DELETE" : "UPDATE"));
		flds.put("key", "opp" + o.getId());

		SkinGroup skinGroup = null;
		LevelsBean levelsBean = null;
		for (Map.Entry<SkinGroup, LevelsBean> entry : levelsMap.entrySet()) {
			skinGroup = entry.getKey();
			levelsBean = entry.getValue();
			if (o.getTimeEstClosing().compareTo(new Date()) > 0 || o.getState() == Opportunity.STATE_CLOSED) {
				flds.put(skinGroup.getLevelUpdateKey(), Utils.formatLevel(levelsBean.level, o.getMarket().getDecimalPoint().intValue()));
			} else {
				flds.put(skinGroup.getLevelUpdateKey(), "0");
			}
			flds.put(	skinGroup.getColorUpdateKey(),
						levelsBean.level < o.getClosingLevel() ? "0" : levelsBean.level == o.getClosingLevel() ? "1" : "2");
		}

		// flds.put("ET_STATE", suspended ? String.valueOf(Opportunity.STATE_SUSPENDED) : String.valueOf(o.getState()));
		flds.put("ET_STATE", getOppStateForLevelUpdate(o));
		int priority = 0; // o.getMarket().getHomePagePriority();
		if (!o.isInOppenedState()) {
			priority += 2000; // the closed ones should show after the opened and suspended
		} else if (suspended) {
			priority += 1000; // the suspended ones should show after the opened
		}
		flds.put("ET_PRIORITY", String.valueOf(priority));
		flds.put("AO_OPP_STATE", o.isSuspended() ? "1" : "0");

		// The markets in group should be less than 100.
		// Add 100 for the opened and 200 for the suspended and
		// 300 for the closed.
		// This way priority should have 2 digits always and
		// opened will be before suspended and closed will come
		// last. So when concatenated with the close time and
		// sorted by this as string we will get the right order.
		int groupPriority = 0;// o.getMarket().getGroupPriority();
		if (o.isInOppenedState()) {
			if (!suspended) {
				groupPriority += 100;
			} else {
				groupPriority += 200;
			}
		} else {
			groupPriority += 300;
		}
		SimpleDateFormat sortDateFormat = new SimpleDateFormat("yyMMddHHmm");
		flds.put("ET_GROUP_PRIORITY", groupPriority + "_" + sortDateFormat.format(o.getTimeEstClosing()));

		flds.put("ET_GROUP_CLOSE", String.valueOf(getOpportunityGroupClose(o)));
		flds.put("ET_SCHEDULED", String.valueOf(o.getScheduled()));
		// fields for bugId: 2029
		flds.put("ET_RND_FLOOR", String.valueOf(new BigDecimal(Double.toString(getRandomFloor()))));
		flds.put("ET_RND_CEILING", String.valueOf(new BigDecimal(Double.toString(getRandomCeiling()))));
		// flds.put("ET_FREQUENCE_PRIORITY", String.valueOf(frequencePriority));
		// end bugId: 2029
		flds.put("ET_GROUP", String.valueOf(o.getMarket().getGroupId()));
		flds.put("ET_MARKET", String.valueOf(marketId));
		flds.put("ET_SUSPENDED_MESSAGE", suspendedMessage);
		// mark which scheduled should be available in filter: 6 all, 4 monthly/weekly/daily/hourly, 3 weekly/daily/hourly, 2 daily/hourly,
		// 1 hourly, 0 close
		// flds.put("FILTER_SCHEDULED", String.valueOf(getFilterScheduled()));
		flds.put("FILTER_NEW_SCHEDULED", getNewFilterScheduled());

		// AO priorities
		// for (Iterator<Integer> i = skinsPriorities.keySet().iterator(); i.hasNext();) {
		// Integer sId = i.next();
		// flds.put("AO_PRIORITY_SKIN_" + sId, String.valueOf(getHomePagePriorityById(sId)));
		// }

		// AO markets on homepage
		ArrayList<Skins> l = LevelService.getInstance().getSkins();
		for (int i = 0; i < l.size(); i++) {
			flds.put("AO_HP_MARKETS_" + l.get(i).getId(), l.get(i).getCurrentMarketsOnHomePage());
		}

		// AO markets states
		flds.put("AO_STATES", LevelService.getInstance().getOpenedMarkets());

		flds.put("AO_TS", String.valueOf(System.currentTimeMillis()));
		int aoFlags = 0;
		if (hasHour()) {
			aoFlags |= 1;
		}
		if (o.isLessThanHourToClosing()) {
			aoFlags |= 2;

			if (o.getOpportunityTypeId() == Opportunity.TYPE_REGULAR) {
				if ((o.getCallsCount() - o.getCallsCountET()) + (o.getPutsCount() - o.getPutsCountET()) > 0 /* avoid division by zero */) {
					/* Show trends during the profit line. */
					flds.put("AO_CALLS_TREND", o.getCallsCountRate(Skin.SKIN_BUSINESS_ANYOPTION));
				} else {
					// if there were no investments made on the asset the distribution will be 50%/50%
					flds.put("AO_CALLS_TREND", "0.5");
				}
				if (o.getCallsCountET() + o.getPutsCountET() > 0 /* avoid division by zero */) {
					/* Show trends during the profit line. */
					flds.put("ET_CALLS_TREND", o.getCallsCountRate(Skin.SKIN_BUSINESS_ETRADER));
				} else {
					// if there were no investments made on the asset the distribution will be 50%/50%
					flds.put("ET_CALLS_TREND", "0.5");
				}
			}
		}
		flds.put("AO_FLAGS", String.valueOf(aoFlags));

		if (!flds.containsKey("AO_CALLS_TREND")) {
			flds.put("AO_CALLS_TREND", "0.0");
		}
		if (!flds.containsKey("ET_CALLS_TREND")) {
			flds.put("ET_CALLS_TREND", "0.0");
		}

		flds.put("AO_TYPE", String.valueOf(o.getOpportunityTypeId()));
        flds.put("ET_NH", getNextOpenHourly());
        flds.put("AO_NH", getNextOpenedScheduledOpportunities(Opportunity.SCHEDULED_HOURLY));
        flds.put("AO_NQ", getNextOpenedScheduledOpportunities(Opportunity.SCHEDULED_QUARTERLY));

		Map<String, BigDecimal> quoFlds = hourLevelFromula.getQuoteFields();
        if (null != quoFlds) {
        	BigDecimal val = null;
        	for (String key : quoFlds.keySet()) {
        		val = quoFlds.get(key);
        		flds.put(key, val != null ? String.valueOf(val.doubleValue()) : "");
        	}
        }

		return flds;
	}

	/**
	 * Returns an integer that represents a bitmask which indicates the closing filter flags of an opportunity.
	 *
	 * @param o
	 * @return a bitmask comprised of {@link Opportunity#CLOSING_FILTER_FLAG_CLOSEST}, {@link Opportunity#CLOSING_FILTER_FLAG_QUARTER},
	 *         {@link Opportunity#CLOSING_FILTER_FLAG_MONTH}, {@link Opportunity#CLOSING_FILTER_FLAG_TODAY} and
	 *         {@link Opportunity#CLOSING_FILTER_FLAG_WEEK}
	 */
	@Override
	public int getOpportunityGroupClose(Opportunity o) {
		int groupClose = 0;
		if ((o.isInOppenedState() || o.getState() == Opportunity.STATE_DONE) && !suspended) {
			Calendar cls = Calendar.getInstance();
			cls.setTime(o.getTimeEstClosing());
			Calendar now = Calendar.getInstance();
			if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY || o == getClosestToClose()) {
				groupClose |= Opportunity.CLOSING_FILTER_FLAG_CLOSEST;
			}
			if (log.isTraceEnabled()) {
				log.trace("cls.get(Calendar.DAY_OF_YEAR): " + cls.get(Calendar.DAY_OF_YEAR) + " now.get(Calendar.DAY_OF_YEAR): "
							+ now.get(Calendar.DAY_OF_YEAR));
			}
			if (o.getScheduled() != Opportunity.SCHEDULED_HOURLY
					&& (o.getScheduled() == Opportunity.SCHEDULED_DAYLY || cls.get(Calendar.DAY_OF_YEAR) == now.get(Calendar.DAY_OF_YEAR))) {
				groupClose |= Opportunity.CLOSING_FILTER_FLAG_TODAY;
			}
			if (o.getScheduled() == Opportunity.SCHEDULED_WEEKLY
					|| (o.getScheduled() == Opportunity.SCHEDULED_MONTHLY && !hasOpenedWeekly())
					|| (o.getScheduled() == Opportunity.SCHEDULED_QUARTERLY && !hasOpenedWeekly() && !hasOpenedMonthly())) {
				groupClose |= Opportunity.CLOSING_FILTER_FLAG_WEEK;
			}
			if (o.getScheduled() == Opportunity.SCHEDULED_MONTHLY
					|| (o.getScheduled() == Opportunity.SCHEDULED_QUARTERLY && !hasOpenedMonthly())) {
				groupClose |= Opportunity.CLOSING_FILTER_FLAG_MONTH;
			}
			if (o.getScheduled() == Opportunity.SCHEDULED_QUARTERLY) {
				groupClose |= Opportunity.CLOSING_FILTER_FLAG_QUARTER;
			}
		} else if (o.getState() == Opportunity.STATE_WAITING_TO_PAUSE) {
			if (!hasOpened(Opportunity.SCHEDULED_DAYLY)) {
				groupClose |= Opportunity.CLOSING_FILTER_FLAG_TODAY;
			}
			if (o.getScheduled() == Opportunity.SCHEDULED_WEEKLY
					|| (o.getScheduled() == Opportunity.SCHEDULED_MONTHLY && !hasScheduledWaitingToPause(Opportunity.SCHEDULED_WEEKLY))
					|| (o.getScheduled() == Opportunity.SCHEDULED_QUARTERLY	&& !hasScheduledWaitingToPause(Opportunity.SCHEDULED_WEEKLY)
						&& !hasScheduledWaitingToPause(Opportunity.SCHEDULED_MONTHLY))) {
				groupClose |= Opportunity.CLOSING_FILTER_FLAG_WEEK;
			}
			if (o.getScheduled() == Opportunity.SCHEDULED_MONTHLY
					|| (o.getScheduled() == Opportunity.SCHEDULED_QUARTERLY && !hasScheduledWaitingToPause(Opportunity.SCHEDULED_MONTHLY))) {
				groupClose |= Opportunity.CLOSING_FILTER_FLAG_MONTH;
			}
			if (o.getScheduled() == Opportunity.SCHEDULED_QUARTERLY) {
				groupClose |= Opportunity.CLOSING_FILTER_FLAG_QUARTER;
			}
		} else {
			groupClose |= Opportunity.CLOSING_FILTER_FLAG_TODAY;
			if (hasLongTerm()) {
				groupClose |= Opportunity.CLOSING_FILTER_FLAG_WEEK;
				groupClose |= Opportunity.CLOSING_FILTER_FLAG_MONTH;
				groupClose |= Opportunity.CLOSING_FILTER_FLAG_QUARTER;
			}
		}

		return groupClose;
	}

	@Override
	public String getTimeEstCloseForLevelUpdate(Opportunity o) {
		Date dts = null;
		if (o.isInOppenedState() && isSnapshotReceived()) {
			dts = o.getTimeEstClosing();
		} else if (o.getState() == Opportunity.STATE_CREATED || (o.isInOppenedState() && !isSnapshotReceived())) {
			dts = o.getTimeFirstInvest();
		} else { // only PAUSED and WAITING_TO_PAUSE left
			dts = o.getTimeNextOpen();
			// TimeNextOpen is not null only on the specific case before week/month last working day
			// when the long term opp will open first on the last working day.
			// This is because we don't have day opp on the last working day of the week/month.
			if (null == dts) {
				if (o.getState() == Opportunity.STATE_WAITING_TO_PAUSE) {
					// give it one more try. in case the service has just started
					Opportunity tmon = getTimeMarketOpensNext();
					if (log.isDebugEnabled()) {
						log.debug("TimeMarketOpensNext: " + (null != tmon ? tmon.getTimeNextOpen() : "none"));
					}
					if (null != tmon) {
						o.setTimeNextOpen(tmon.getTimeNextOpen());
						dts = tmon.getTimeNextOpen();
					} else {
						dts = o.getTimeEstClosing();
					}
				} else {
					dts = o.getTimeEstClosing();
				}
			}
		}
		return Utils.formatDate(dts);
	}

	@Override
	public String getOppStateForLevelUpdate(Opportunity o) {
		int stt = o.getState();
		if (suspended) {
			stt = Opportunity.STATE_SUSPENDED;
		} else if (!isSnapshotReceived()) {
			stt = Opportunity.STATE_CREATED;
		} else if (stt == Opportunity.STATE_CLOSING && ((o.getTimeLastInvest().getTime() + 15 * 60 * 1000) <= System.currentTimeMillis())
					&& o.getMarketId() == com.anyoption.common.beans.Market.MARKET_TEL_AVIV_25_MAOF_ID) {
			stt = Opportunity.STATE_HIDDEN_WAITING_TO_EXPIRY;
		}
		return String.valueOf(stt);
	}

	/**
	 * Handle sending of update. Send the update only if needed. If the update pause passed or the update is significant.
	 *
	 * @param sender
	 * @param lifeId
	 * @param haveCurrentLevelUpdate if this call is passing current level to be sent
	 * @param currentCalc the current level as calculated before pass thorugh random
	 * @param currentLevel the current level after passing through the random
	 * @param haveRealLevelUpdate if this call is passing real level to be sent
	 * @param realLevelUpdate the real level to send
	 */
	private void haveUpdate(SenderThread sender, long lifeId, boolean haveCurrentLevelUpdate, double currentCalc, double currentLevel,
								boolean haveRealLevelUpdate, double realLevelUpdate) {
		boolean sendCurrent = false;
		if (state.getOldCalcResult() > 0 && currentCalc > 0) {
			sendLevelChangeAlert(currentCalc);
		}
		if (haveCurrentLevelUpdate) {
			boolean cs = (state.getOldCalcResult() == 0)	? true
															: Math.abs(1 - currentCalc
																			/ state.getOldCalcResult()) > significantPercentage.doubleValue();
			boolean tp = System.currentTimeMillis() - lastUpdateTime > updatesPause;
			if (cs || tp) {
				if (log.isTraceEnabled()) {
					log.trace(marketName + " Current page siginificant: " + cs + " time elapsed: " + tp);
				}
				sendCurrent = true;
				lastUpdateTime = System.currentTimeMillis();
				state.setOldCalcResult(currentCalc);
			}
		}
		boolean sendReal = false;
		if (haveRealLevelUpdate) {
			boolean rs = (state.getOldRealLevel() == 0)	? true
														: Math.abs(1 - realLevelUpdate
																		/ state.getOldRealLevel()) > realSignificantPercentage.doubleValue();
			boolean tp = System.currentTimeMillis() - lastRealUpdateTime > realUpdatesPause;
			if (rs || tp) {
				if (log.isTraceEnabled()) {
					log.trace(marketName + " Current real siginificant: " + rs + " time elapsed: " + tp);
				}
				sendReal = true;
				lastRealUpdateTime = System.currentTimeMillis();
				state.setOldRealLevel(realLevelUpdate);
			}
		}
		if (sendCurrent || sendReal) {
			if (log.isTraceEnabled()) {
				log.trace("sendCurrent: " + sendCurrent + " sendReal: " + sendReal);
			}
			sendUpdate(sender, lifeId, UPDATE_ALL_OPPS, sendCurrent, false);
		}
		if (log.isTraceEnabled()) {
			log.trace((sendCurrent || sendReal ? "SENDING " : "DISCARDING ") + " TRADINGDATA_" + marketName + " currentLevel: "
						+ currentLevel + " lastFormulaResult: " + state.getLastFormulaResult() + " realLevelUpdate: " + realLevelUpdate
						+ " lastRealLevel: " + state.getLastRealLevel() + " currentCalc: " + currentCalc + " lastCalcResult: "
						+ state.getLastCalcResult() + " oldCalcResult: " + state.getOldCalcResult() + " oldRealLevel: " + state.getOldRealLevel());
		}
	}

	/**
	 * Send update message for all item opps.
	 *
	 * @param sender
	 * @param lifeId
	 * @param toUpdate to which opps to send update all, todays or long term
	 * @param sendCurrentLevel
	 * @param suspend <code>true</code> to make suspend update (delete all but the special entry) else <code>false</code>
	 */
	@Override
	public void sendUpdate(SenderThread sender, long lifeId, int toUpdate, boolean sendCurrentLevel, boolean suspend) {
		if (hasOpenedOpportunities() && isSnapshotReceived()) {
			Opportunity o = null;
			for (int i = 0; i < opportunities.size(); i++) {
				o = opportunities.get(i);
				if (toUpdate == UPDATE_ALL_OPPS
						|| (toUpdate == UPDATE_TODAY_OPPS && (o.getScheduled() == Opportunity.SCHEDULED_HOURLY || o.getScheduled() == Opportunity.SCHEDULED_DAYLY))
						|| (toUpdate == UPDATE_LONG_TERM_OPPS
							&& (o.getScheduled() == Opportunity.SCHEDULED_WEEKLY	|| o.getScheduled() == Opportunity.SCHEDULED_MONTHLY
								|| o.getScheduled() == Opportunity.SCHEDULED_QUARTERLY))) {
					if ((o.isInOppenedState() || o.getState() == Opportunity.STATE_WAITING_TO_PAUSE)
							&& !(o.getState() == Opportunity.STATE_CLOSED) && isSnapshotReceived(o)) {
						sendUpdate(sender, lifeId, o, suspend && o != specialEntry && !isOptionPlusMarket(), sendCurrentLevel);
						// TODO: Tony: send only 1 message for real level change (one per market not one per opp)
					}
				}
			}
		} else {
			if (null != specialEntry) {
				sendUpdate(sender, lifeId, specialEntry, false, sendCurrentLevel);
			} else {
				log.warn("Closed market without special entry - " + marketName);
			}
		}
	}

	@Override
	public void getOpenedOpportunityLevels(Opportunity o, ETraderFeedMessage toSend, SkinGroup skinGroup) {
		toSend.realLevel = state.getLastRealLevel();
		LevelsBean b = getOpportunityLevels(o, skinGroup);
		toSend.level = b.level;
		toSend.devCheckLevel = b.devCheckLevel;
	}

	/**
	 * Send update for item opp.
	 *
	 * @param sender
	 * @param lifeId
	 * @param o
	 * @param forseDelete if to forse "DELETE" command or to determine the command by the state
	 * @param sendCurrentLevel if to send current level update in this message (LS update) or just the real level
	 */
	@Override
	public void sendUpdate(SenderThread sender, long lifeId, Opportunity o, boolean forseDelete, boolean sendCurrentLevel) {
		Map<SkinGroup, LevelsBean> levelsMap = new EnumMap<SkinGroup, Market.LevelsBean>(SkinGroup.class);
		LevelsBean levelsBean = null;
		for (Map.Entry<SkinGroup, OpportunitySkinGroupMap> entry : o.getSkinGroupMappings().entrySet()) {
			// DELETE commands don't need level
			// also don't let missing level to stop sending DELETE command (eg when market is opened and don't have snapshot yet)
			if (sendCurrentLevel && !forseDelete) {
				levelsBean = getOpportunityLevels(o, entry.getKey());
			} else {
				levelsBean = new LevelsBean();
				levelsBean.level = 0;
				levelsBean.devCheckLevel = state.getLastCalcResult();
			}
			levelsMap.put(entry.getKey(), levelsBean);
		}

		// prepare the object to send through JMS
		@SuppressWarnings("unchecked")
		HashMap<Integer, Boolean> tmpHomepage = (HashMap<Integer, Boolean>) onHomePage.clone();
		if (o != specialEntry) { // maket all the keys false so its will not go to home page
			int key;
			for (Iterator<Integer> i = tmpHomepage.keySet().iterator(); i.hasNext();) {
				key = i.next();
				tmpHomepage.put(key, false);
			}
		}
		ETraderFeedMessage toSend = new ETraderFeedMessage(
															// o == specialEntry?onHomePage:new HashMap<Integer, Boolean>(), //only if its
															// specialEntry send the real hashmap else send new 1 its will act like all have
															// false on home page
															tmpHomepage,
															sendCurrentLevel
																	&& (forseDelete || o == specialEntry || !suspended || isOptionPlusMarket())	? getOpportunityLevelUpdate(o,
																																										levelsMap,
																																										forseDelete)
																																			: null,
															lifeId,
															levelsMap.get(SkinGroup.ETRADER).level, // use by insurance adapter (GM/RF)
															o.getMarket().getGroupId(),
															o.getId(),
															state.getLastRealLevel(),
															levelsMap.get(SkinGroup.ETRADER).devCheckLevel, // not in use
															skinsPriorities, o.getAutoShiftParameter(), o.getCalls(), o.getPuts(),
															marketName, decimalPoint, o.isDisabledTrader(), disabled,
															Utils.roundDouble(o.getContractsBought(), 2),
															Utils.roundDouble(o.getContractsSold(), 2), o.getQuoteParamsObject(),
															0,// bid not in use for updates only for devation check
															0,// offer not in use for updates only for devation check
															null, !o.isClosingLevelReceived(), o.getSkinGroupMappings());
		sender.send(toSend);
	}
   
   private LevelsBean getOpportunityLevels(Opportunity o, SkinGroup skinGroup) {
	   LevelsBean b = new LevelsBean();
       b.level = 0;
       b.devCheckLevel = exposure(o, state.getLastCalcResult(), skinGroup);
       if (o.getState() == Opportunity.STATE_CLOSED) {
           b.level = getClosingLevel(o);
       } else if (o.isInOppenedState() && !suspended) {
           if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY || o.getScheduled() == Opportunity.SCHEDULED_DAYLY) {
               if (isSnapshotReceived()) {
                   if (o.getState() == Opportunity.STATE_CLOSING_1_MIN || o.getState() == Opportunity.STATE_CLOSING) {
                       b.level = Utils.formulaRandomChange(state, state.getLastRealLevel(), getRandomCeiling(), getRandomFloor());
                   } else {
                       b.level = exposure(o, state.getLastFormulaResult(), skinGroup);
                   }
               }
           } else {
               if (isLongTermSnapshotReceived()) {
                   if (o.getState() == Opportunity.STATE_CLOSING_1_MIN || o.getState() == Opportunity.STATE_CLOSING) {
                       b.level = Utils.formulaRandomChange(state, state.getLastRealLevel(), getRandomCeiling(), getRandomFloor());
                   } else {
                	   if (o.getScheduled() == Opportunity.SCHEDULED_WEEKLY) {
                		   b.devCheckLevel = state.getLastWeekResult();
                	   } else if (o.getScheduled() == Opportunity.SCHEDULED_MONTHLY) {
                		   b.devCheckLevel = state.getLastMonthResult();
                	   } else if (o.getScheduled() == Opportunity.SCHEDULED_QUARTERLY) {
                		   b.devCheckLevel = state.getLastQuarterResult();
                	   }
//                       b.devCheckLevel = longTermFormula(o);
                       b.level = Utils.formulaRandomChange(b.devCheckLevel, state.getLastRandomChange());
                       b.devCheckLevel = exposure(o, b.devCheckLevel, skinGroup);
                       b.level = exposure(o, b.level, skinGroup);
//                       if (log.isTraceEnabled()) {
//                           log.trace("Long term formula for: " + o.getId() + " ltf: " + b.devCheckLevel + " level: " + b.level);
//                       }
                   }
               }
           }
       } else { // Opportunity.STATE_CREATED
           b.level = o.getClosingLevel(); // last closing level
       }
       return b;
   }
   
//   /**
//    * Format the result of the exposure calc (the level) with the number of
//    * decimal digits for this item.
//    *
//    * @param exposureResult
//    * @return
//    */
//   public String formatLevel(double exposureResult, int decimalPoint) {
//	   log.info("Not implemented");
//		return null;
//   }
//   
//   /**
//    * Randomly modify the level with % in specified interval. For example
//    * if you want to increase the level with 0.01% to 0.07% call with
//    * level, 0.0001 and 0.0007.
//    *
//    * @param level the level to change
//    * @return
//    */
//   protected double formulaRandomChange(double level) {
//	   log.info("Not implemented");
//		return 0d;
//   }
//   
//   /**
//    * Randomly modify the level with % in specified interval. For example
//    * if you want to increase the level with 0.01% to 0.07% call with
//    * level, 0.0001 and 0.0007.
//    *
//    * @param level the level to change
//    * @param floor
//    * @param ceiling
//    * @return
//    */
//   protected double formulaRandomChange(double level, double floor, double ceiling) {
//	   log.info("Not implemented");
//		return 0d;
//   }
//   
//   /**
//    * Modify the level with specified change.
//    *
//    * @param level the level to change
//    * @param rnd the change
//    * @return
//    */
//   protected double formulaRandomChange(double level, double rnd) {
//	   log.info("Not implemented");
//		return 0d;
//   }
//   
//   /**
//    * Used to format float values with 2 decimal digits.
//    *
//    * @param val the val to format
//    * @return
//    */
//   protected static String formatFloat(float val) {
//	   log.info("Not implemented");
//		return null;
//   }
//   
//   /**
//    * Used to format the est close time (format MM/dd/yy HH:mm) gmt 0.
//    *
//    * @param val the time to format
//    * @return
//    */
//   protected static String formatDate(Date val) {
//	   log.info("Not implemented");
//		return null;
//   }
//   
//   /**
//    * Used to format the odds as % (format #0%).
//    *
//    * @param val the val to format
//    * @return
//    */
//   protected static String formatOdds(float val) {
//	   log.info("Not implemented");
//		return null;
//   }
//   
//   /**
//    * Round a double value to have "decimals" number of digits after
//    * the decimal point.
//    * For example roundDouble(1.12345, 3) = 1.123
//    *
//    * @param val
//    * @param decimals
//    * @return
//    */
//   protected static double roundDouble(double val, long decimals) {
//	   log.info("Not implemented");
//		return 0d;
//   }
//
//	/**
//	 * Calculate how many days left to expire.
//	 *
//	 * @param expireDate the expire date in format "dd MMM yyyy" in enUS locale
//	 * @return
//	 */
//   // TODO this has no place here
//	public static long getDaysToExpire(String expireDate) throws ParseException {
//		SimpleDateFormat updateDateFormat = new SimpleDateFormat("dd MMM yyyy", new Locale("en", "US"));
//		Date expire = updateDateFormat.parse(expireDate);
//		return getDaysToExpire(expire);
//	}
//
//	/**
//	 * Calculate how many days left to expire.
//	 *
//	 * @param expire the expire date
//	 * @return
//	 */
//	   // TODO this has no place here
//	public static long getDaysToExpire(Date expire) {
//		Calendar today = Calendar.getInstance();
//		today.set(Calendar.HOUR_OF_DAY, 0);
//		today.set(Calendar.MINUTE, 0);
//		today.set(Calendar.SECOND, 0);
//		today.set(Calendar.MILLISECOND, 0);
//
//		Calendar e = Calendar.getInstance();
//		e.setTime(expire);
//		e.set(Calendar.HOUR_OF_DAY, 0);
//		e.set(Calendar.MINUTE, 0);
//		e.set(Calendar.SECOND, 0);
//		e.set(Calendar.MILLISECOND, 0);
//
//		// NOTE: take the difference as double and round it so this way we will
//		// drop the 1 hour difference in the day length when change to/from
//		// day light saving time
//		double dif = e.getTimeInMillis() - today.getTimeInMillis();
//		return Math.round(dif / (1000 * 60 * 60 * 24)); // millis in day
//	}

//   ??
//   /**
//    * Tell the item to open (subscribe) all its options (when you want to start using it).
//    *
//    * @param itm the Item instance to receive the updates for the options.
//    */
//   public void subscribeOptions(Item itm) {
//       Option o = null;
//       for (Iterator<Option> i = options.iterator(); i.hasNext();) {
//           o = i.next();
//           if (null == o.getHandle()) {
//               optionSubscribtionManager.subscribeOption(itm, o);
//           }
//       }
//   }
//
//   /**
//    * Ask this item to close (unsubscribe) all its options (when you want to close/unsubscribe it).
//    *
//    * @param itm the Item whose options should be unsubscribed
//    */
//   public void unsubscribeOptions(Item itm) {
//       clearOptions(itm);
//   }
//
//   /**
//    * Find option with specified name from the options.
//    *
//    * @param optionName the name of the option to look for
//    * @return The <code>Option</code> with the specified name or <code>null</code> if not found.
//    */
//   protected Option getOption(String optionName) {
//       Option o = null;
//       for (Iterator<Option> i = options.iterator(); i.hasNext();) {
//           o = i.next();
//           if (o.getOptionName().equals(optionName)) {
//               return o;
//           }
//       }
//       return null;
//   }
//
//   /**
//    * Clear the options list (unsibscribe each option it remove).
//    * Make sure you have lock over the options when you call this metod.
//    *
//    * @param itm the Item instance to be removed from the options.
//    */
//   protected void clearOptions(Item itm) {
//       Option o = null;
//       for (Iterator<Option> i = options.iterator(); i.hasNext();) {
//           o = i.next();
//           optionSubscribtionManager.unsubscribeOption(itm, o);
//           i.remove();
//       }
//   }
   
   /**
    * Check if all options have received snapshots and level can be calculated.
    * Call this metod only when you have lock on the options.
    *
    * @return <code>true</code> if level can be claculated else <code>false</code>.
    */
   protected boolean canCalcLevel() {
	   log.info("Not implemented");
		return false;
   }
   
//   /**
//    * Handle update for the interest option. If the update is for this interest
//    * option update the "EXPIR_DATE", "ASK" and "BID" fields.
//    *
//    * @param itemName the name of the item to update
//    * @param interest the interest to try to update
//    * @param fields the update fields
//    * @return <code>ture</code> if the update is for the interest option else <code>false</code>.
//    */
//   protected boolean handleInterest(String itemName, Option interest, HashMap<String, Object> fields) {
//	   log.info("Not implemented");
//		return false;
//   }

//	/**
//	 * Check if specified date is today.
//	 *
//	 * @param date the date to check in format "dd MMM yyyy"
//	 * @return <code>true</code> if the passed date is today else <code>false</code> (even if can't parse).
//	 */
//	private static boolean isToday(String date, String timeZone) {
//		try {
//			SimpleDateFormat updateDateFormat = new SimpleDateFormat("dd MMM yyyy", new Locale("en", "US"));
//			updateDateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
//			Date d = updateDateFormat.parse(date);
//			return Utils.isToday(d, timeZone);
//		} catch (ParseException pe) {
//			log.error("Can't parse the date.", pe);
//		}
//		return false;
//	}

//	/**
//	 * Check if specified time is today.
//	 *
//	 * @param time the time to check in format "dd MMM yyyy HH:mm:ss"
//	 * @return <code>true</code> if the passed date is today else <code>false</code> (even if can't parse).
//	 */
//	private static boolean isTodayTime(String time, String timeZone) {
//		try {
//			SimpleDateFormat updateDateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm:ss", new Locale("en", "US"));
//			updateDateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
//			Date d = updateDateFormat.parse(time);
//			if (log.isTraceEnabled()) {
//				log.trace("isTodayTime - time: " + time + " timeZone: " + timeZone + " d: " + d);
//			}
//			return Utils.isToday(d, timeZone);
//		} catch (ParseException pe) {
//			log.error("Can't parse the date.", pe);
//		}
//		return false;
//	}

	/**
	 * Reset (zero) the shiftings of the opportunities.
	 */
	@Override
	public void resetOpportunitiesShiftings() {
		Opportunity o = null;
		for (int i = 0; i < opportunities.size(); i++) {
			o = opportunities.get(i);
			o.setCallsShift(0);
			o.setPutsShift(0);
			o.setShifting(false);

			for (Map.Entry<SkinGroup, OpportunitySkinGroupMap> entry : o.getSkinGroupMappings().entrySet()) {
				OpportunitySkinGroupMap mapping = entry.getValue();
				if (mapping.getShiftParameter() != 0d) {
					try {
						// TODO call DB procedure
						OpportunitiesManager.insertShifting(o.getId(), 0, -mapping.getShiftParameter(), mapping.getShiftParameter() < 0,
															entry.getKey());
					} catch (Throwable t) {
						log.error("Can't reset " + entry.getKey() + " shifting of opp: " + o.getId(), t);
					}
				}
				mapping.setShiftParameter(0d);
			}

			if (o.getAutoShiftParameter() != 0) {
				try {
					OpportunitiesManager.insertShifting(o.getId(), 0, -o.getAutoShiftParameter(), o.getAutoShiftParameter() < 0,
														SkinGroup.AUTO);
				} catch (Throwable t) {
					log.error("Can't reset shifting of opp: " + o.getId(), t);
				}
			}
			o.setAutoShiftParameter(0);
			o.setLastShift(null);
		}
	}

//	/**
//	 * Create <code>Date</code> object by passed date, time and timezone. The date should be in format "dd MMM yyyy". The time should be in
//	 * format HH:mm or HH:mm:ss.
//	 *
//	 * @param date
//	 * @param time
//	 * @param timeZone
//	 * @return
//	 */
//	private Date getUpdateDateTime(String date, String time, String timeZone) throws ParseException {
//		SimpleDateFormat sdf = null;
//		if (time.length() > 5) {
//			sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.US);
//		} else {
//			sdf = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.US);
//		}
//		sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
//		return sdf.parse(date + " " + time);
//	}

	/**
	 * Create <code>Date</code> object by passed time and timezone. If no pattern given, the time should be informat HH:mm or HH:mm:ss.
	 *
	 * @param time
	 * @param timeZone
	 * @param timePattern 
	 * @return
	 * @throws ParseException 
	 */
	public Date getUpdateTime(String time, String timeZone, String timePattern) throws ParseException {
		Date updateTime = null;
		if (timePattern != null) {
			SimpleDateFormat sdf = new SimpleDateFormat(timePattern);
			sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
			updateTime = sdf.parse(time);
		} else {
//			log.trace("No time pattern. Trying to parse time with \":\" token");
			StringTokenizer st = new StringTokenizer(time, ":");
			String hour = st.nextToken();
			String min = st.nextToken();
			String sec = st.hasMoreTokens() ? st.nextToken() : null;
	
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(timeZone));
			cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hour));
			cal.set(Calendar.MINUTE, Integer.parseInt(min));
			if (null != sec) {
				cal.set(Calendar.SECOND, Integer.parseInt(sec));
			} else {
				cal.set(Calendar.SECOND, 0);
			}
			
			updateTime = cal.getTime();
		}
		
		// Since we parse only time field and assume the date is the current one we get a problem with updates that arrive late. For example
		// update for 11.08.2016 23:59:59 arrive on 12.08.2016 00:00:00. So when we combine the time of the update with the arrive date we get
		// time that is way past the expiry time of the opportunity and we close prematurely the opportunity.
		if (updateTime.getTime() > System.currentTimeMillis() + 60000) { // We shouldn't have updates in the future. This is buffer for not synchronized server clocks.
			Calendar c = Calendar.getInstance();
			c.setTime(updateTime);
			// if the update is from the future obviously we got the date wrong
			c.add(Calendar.DAY_OF_MONTH, -1);
			log.info("Update time in the future. Moving it 1 day back. before: " + updateTime + " after: " + c.getTime());
			updateTime = c.getTime();
		}
		
		return updateTime;
	}
   
//   in formula
//   /**
//    * Get the closing level formula for the specified opportunity.
//    *
//    * @param w the opportunity for which to get closing level formula
//    * @return The formula and the formula patamters with there values as one string.
//    */
//	public String getClosingLevelTxt(Opportunity o) {
//		log.info("Not implemented");
//		return null;
//	}

	/**
	 * get the date of the last opp of the day.
	 *
	 * @return the date of the last opp of the day.
	 */
	@Override
	public Date getLastOppOfTheDay() {
		Date lastOppOfTheDay = null;
		Calendar cls = Calendar.getInstance();
		Calendar now = Calendar.getInstance();

		for (Opportunity o : opportunities) {
			Date tempOppEstClosing = o.getTimeEstClosing();
			cls.setTime(o.getTimeEstClosing());
			if (cls.get(Calendar.YEAR) == now.get(Calendar.YEAR) && cls.get(Calendar.DAY_OF_YEAR) == now.get(Calendar.DAY_OF_YEAR)) {

				if (lastOppOfTheDay == null || lastOppOfTheDay.before(tempOppEstClosing)) {
					lastOppOfTheDay = tempOppEstClosing;
				}
			}
		}
		return lastOppOfTheDay;
	}

	/**
	 * Reset the market internal state in the end of the day to be ready for the next day opening. Long term shiftings, updates flags etc.
	 */
	@Override
	public void resetInTheEndOfTheDay() {
		// reset (zero) the shiftings of the long term opps
		resetOpportunitiesShiftings();

		// reset updates flags
		setSnapshotReceived(false);
		setLongTermSnapshotReceived(false);
		for (Formula formula : formulas) {
			formula.resetInTheEndOfTheDay();
		}

		// setLastReutersUpdateReceiveTime(0);
		todayOpeningTime = 0;

		// if the opps on the market were disabled in the end of the day - reset
		enableOpportunities("Reset in the end of the day", true);
		disabledByMarketConditions = false;
		state = new MarketState();
	}

	/**
	 * @return <code>true</code> if the market opened in the last 1 min else <code>false</code>.
	 */
	@Override
	public boolean hasJustOpened() {
		return System.currentTimeMillis() - todayOpeningTime < 60000; // opened in the last 1 min
	}

	@Override
	public double getGraphLevel(SkinGroup skinGroup) {
		Opportunity o = getOpenedOppByScheduled(Opportunity.SCHEDULED_HOURLY);
		if (null == o) {
			o = getOpenedOppByScheduled(Opportunity.SCHEDULED_DAYLY);
		}
		if (null == o) {
			o = getOpenedOppByScheduled(Opportunity.SCHEDULED_WEEKLY);
		}
		if (null == o) {
			o = getOpenedOppByScheduled(Opportunity.SCHEDULED_MONTHLY);
		}
		if (o == null) {
			o = getOpenedOppByScheduled(Opportunity.SCHEDULED_QUARTERLY);
		}
		if (null != o) {
			LevelsBean b = getOpportunityLevels(o, skinGroup);
			return b.level;
		}
		return 0;
	}

	@Override
	public double getDayLevel(SkinGroup skinGroup) {
		Opportunity o = getOpenedOppByScheduled(Opportunity.SCHEDULED_DAYLY);
		if (null == o) {
			o = getOpenedOppByScheduled(Opportunity.SCHEDULED_WEEKLY);
		}
		if (null == o) {
			o = getOpenedOppByScheduled(Opportunity.SCHEDULED_MONTHLY);
		}
		if (o == null) {
			o = getOpenedOppByScheduled(Opportunity.SCHEDULED_QUARTERLY);
		}
		if (null != o) {
			LevelsBean b = getOpportunityLevels(o, skinGroup);
			return b.level;
		}
		return 0;
	}

	/**
	 * @return true if any of the skin have this market on home page
	 */
	@Override
	public boolean hasOnHomePage() {
		boolean key = false;
		for (Iterator<Boolean> i = onHomePage.values().iterator(); i.hasNext();) {
			key = i.next();
			if (key) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Pause the long term opps that are waiting to pause.
	 */
	@Override
	public void pauseWaitingToPauseLongTermOpps() {
		Opportunity o = null;
		for (int i = 0; i < opportunities.size(); i++) {
			o = opportunities.get(i);
			if (o.getState() == Opportunity.STATE_WAITING_TO_PAUSE) {
				LevelService.getInstance().opportunityStateChanged(o.getId(), Opportunity.STATE_PAUSED);
			}
		}
	}

	/**
	 * Returns whether there is opportunity with the given {@code scheduled} that is in waiting to pause state
	 * 
	 * @param scheduled the scheduled type to check
	 * @return {@code true} if there is opportunity in waiting to pause state with the given {@code scheduled}
	 */
	@Override
	public boolean hasScheduledWaitingToPause(int scheduled) {
		Opportunity o = null;
		for (int i = 0; i < opportunities.size(); i++) {
			o = opportunities.get(i);
			if (o.getScheduled() == scheduled && o.getState() == Opportunity.STATE_WAITING_TO_PAUSE) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Get this Item current Golden Minutes opportunity if there is one.
	 *
	 * @param endOfHour if the current Golden Minutes interval is a end of hour or mid hour one
	 * @return <code>Opportunity</code> that is the Golden Minutes one for the current GM interval or <code>null</code> if one is not
	 *         available.
	 */
	@Override
	public Opportunity getGoldenMinutesOpportunity(Expiry expiry) {
		Opportunity o = null;
		for (int i = 0; i < opportunities.size(); i++) {
			o = opportunities.get(i);
			if (o.isGoodForGoldenMinute(expiry)) {
				return o;
			}
		}
		return null;
	}

    /* 
     * @see com.anyoption.service.level.market.MarketIfc#getNewFilterScheduled()
     * 
     * returns a bitmask of opportunities that are scheduled and not suspended or market itself
     * 
     * 00000 -> all opportunities for the market are suspended
     * 10000 -> quarterly
     * 01000 -> monthly
     * 00100 -> weekly
     * 00010 -> daily
     * 00001 -> hourly
     * 
     */
    @Override
    public String getNewFilterScheduled() {
	int scheduled = 0;
	if (opportunities.size() == 0 || suspended) {
	    return String.valueOf(scheduled);
	}
	for (int i = 0; i < opportunities.size(); i++) {
	    if (opportunities.get(i).isInOppenedState() && !opportunities.get(i).isSuspended()) {
			switch (opportunities.get(i).getScheduled()) {
			case Opportunity.SCHEDULED_QUARTERLY: // 10000 -> 16
			    scheduled |= 16;
			    break;
			case Opportunity.SCHEDULED_MONTHLY: // 01000 -> 8
			    scheduled |= 8;
			    break;
			case Opportunity.SCHEDULED_WEEKLY: // 00100 -> 4
			    scheduled |= 4;
			    break;
			case Opportunity.SCHEDULED_DAYLY: // 00010 -> 2
			    scheduled |= 2;
			    break;
			case Opportunity.SCHEDULED_HOURLY: // 00001 -> 1
			    scheduled |= 1;
			    break;
			}
	    }
	}
	String bits = Integer.toBinaryString(scheduled);
	while (bits.length() < 5) {
	    bits = "0" + bits;
	}
	return bits;
    }

	/**
	 * if level was change in big percentage send email to traders only if Current Level Alert Perc is not 0
	 * 
	 * @param currentCalc the current level
	 *
	 */
	@Override
	public void sendLevelChangeAlert(double currentCalc) {
		if (opportunities.size() > 0
				&& opportunities.get(0).getMarket().getCurrentLevelAlertPerc().doubleValue() > 0
				&& new BigDecimal(state.getLastCalcResult()).subtract(new BigDecimal(currentCalc))
														.divide(new BigDecimal(state.getLastCalcResult()), RoundingMode.HALF_UP)
														.abs(new MathContext(4, RoundingMode.HALF_UP))
														.compareTo(opportunities.get(0).getMarket().getCurrentLevelAlertPerc()) > 0) {
			// Mail alert is no longer required. No one is interested in it and floods mailserver
			// LevelService.getInstance().sendBigLevelChangeEmail("big level change alert " + itemName,
			// "there was big level change in market: " + opportunities.get(0).getMarket().getName() + " more then " +
			// opportunities.get(0).getMarket().getCurrentLevelAlertPerc().doubleValue() * 100 + "% last level was: " + lastCalcResult +
			// " current level is: " + currentCalc);
			log.debug("big level change for market " + opportunities.get(0).getMarket().getName() + " more than "
						+ opportunities.get(0).getMarket().getCurrentLevelAlertPerc().doubleValue() * 100 + "% last level was: "
						+ state.getLastCalcResult() + " current level is: " + currentCalc);
		}
	}

	/**
	 * @return <code>true</code> if this item has at least one opp in waiting for expiry or closed or last 1 min at home page state else
	 *         <code>false</code>.
	 */
	@Override
	public boolean hasGoingToCloseOpportunities() {
		if (opportunities.size() == 0) {
			return false;
		}
		Opportunity o = null;
		int state = 0;
		for (int i = 0; i < opportunities.size(); i++) {
			o = opportunities.get(i);
			state = o.getState();
			if (state == Opportunity.STATE_CLOSING_1_MIN || state == Opportunity.STATE_CLOSING || state == Opportunity.STATE_CLOSED) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @return <code>true</code> if the item has at least one open non one-touch opportunity, otherwise <code>false</code>
	 */
	@Override
	public boolean hasOpenNonOneTouchOpportunities() {
		for (Opportunity opportunity : opportunities) {
			if (opportunity.isAvailableForTrading() && !opportunity.getIsOneTouchOpp()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Opportunity getOppInState(int state) {
		for (Opportunity opportunity : opportunities) {
			if (opportunity.getState() == state) {
				return opportunity;
			}
		}
		return null;
	}

	/**
	 * check if should close opp from this item
	 * if we got updates in last X sec
	 * @param oppId
	 *
	 * @return true if should close, false if not.
	 */
	@Override
	public boolean isShouldBeClosed(long oppId) {
		if (marketParams.getDontCloseAfterXSecNoUpdate() > 0) {
			Opportunity o = null;
			for (int i = 0; i < opportunities.size(); i++) {
	            o = opportunities.get(i);
	            if (o.getId() == oppId) {
	                if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
	                    return System.currentTimeMillis() - lastUpdateReceiveTime <= marketParams.getDontCloseAfterXSecNoUpdate() * 1000;
	                }
	                break;
	            }
			}
		}
		return true;
	}

	@Override
	public boolean isSubscribedData() {
		return !subscriptionConfigs.isEmpty();
	}

	/**
	 * @return the snapshotReceived
	 */
	@Override
	public boolean isSnapshotReceived(Opportunity o) {
		if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY || o.getScheduled() == Opportunity.SCHEDULED_DAYLY) {
			return snapshotReceived;
		} else {
			return longTermSnapshotReceived;
		}
	}

	@Override
	public boolean isSnapshotReceived() {
		return snapshotReceived;
	}

	@Override
	public void setSnapshotReceived(boolean snapshotReceived) {
		if (!this.snapshotReceived && snapshotReceived) {
            this.snapshotReceived = snapshotReceived;
            todayOpeningTime = System.currentTimeMillis();
            LevelService lvlSrv = LevelService.getInstance();
            refreshSpecialEntry(lvlSrv.getSender(), lvlSrv.getLifeId());
            lvlSrv.updateHomePageOpportunities();
        } else {
            this.snapshotReceived = snapshotReceived;
        }
	}

	public boolean isOnHomePageCompare() {
		return onHomePageCompare;
	}

	public void setOnHomePageCompare(int skinId) {
        compareSkinId = skinId;
		if (onHomePage.containsKey(skinId)) {
			onHomePageCompare = onHomePage.get(skinId);
			return;
		}
		onHomePageCompare = false;
		if (log.isDebugEnabled()) {
			log.debug("setOnHomePage for " + marketName + " skinId: " + skinId + ". was: " + null + " setting to: " + false);
	    }
		onHomePage.put(skinId, false);
	}

	public boolean isShouldBeClosedByMonitoring() {
		return marketParams.isShouldBeClosedByMonitoring();
	}

	@Override
	public void openOpportunity(Opportunity o) {

	}

	public boolean isLongTermSnapshotReceived() {
		return longTermSnapshotReceived;
	}

	public void setLongTermSnapshotReceived(boolean longTermSnapshotReceived) {
		this.longTermSnapshotReceived = longTermSnapshotReceived;
	}

	public String getMarketName() {
		return marketName;
	}

	public long getLastUpdateReceiveTime() {
		return lastUpdateReceiveTime;
	}

	public void setLastUpdateReceiveTime(long lastUpdateReceiveTime) {
		this.lastUpdateReceiveTime = lastUpdateReceiveTime;
	}

	public long getTodayOpeningTime() {
		return todayOpeningTime;
	}

	public void setTodayOpeningTime(long todayOpeningTime) {
		this.todayOpeningTime = todayOpeningTime;
	}

	public List<Formula> getFormulas() {
		return formulas;
	}
	
	public Opportunity getSpecialEntry() {
		return specialEntry;
	}

	public long getMarketId() {
		return marketId;
	}

	public void setPriorities(HashMap<Integer, HashMap<String, Integer>> skinsPriorities) {
		this.skinsPriorities = skinsPriorities;
	}

	public boolean isSuspended() {
		return suspended;
	}

	public void setSuspended(boolean suspended) {
		this.suspended = suspended;
	}

	public MarketState getState() {
		return state;
	}

	public void setState(MarketState state) {
		this.state = state;
	}

	public MarketParams getMarketParams() {
		return marketParams;
	}

	public void setMarketParams(MarketParams marketParams) {
		this.marketParams = marketParams;
	}

	public double getRandomCeiling() {
		return randomCeiling;
	}

	public void setRandomCeiling(double randomCeiling) {
		this.randomCeiling = randomCeiling;
	}

	public double getRandomFloor() {
		return randomFloor;
	}

	public void setRandomFloor(double randomFloor) {
		this.randomFloor = randomFloor;
	}

	public String getClosingLevelTxt(Opportunity o) {
		return o.getCloseLevelTxt();
	}

	public Formula getHourLevelFromula() {
		return hourLevelFromula;
	}

	public Formula getHourClosingFormula() {
		return hourClosingFormula;
	}

	public Formula getLongTermFormula() {
		return longTermFormula;
	}

	public Formula getRealLevelFormula() {
		return realLevelFormula;
	}

	public boolean isBinaryMarket() {
		return typeId == com.anyoption.common.beans.Market.PRODUCT_TYPE_BINARY;
	}

	public boolean isOptionPlusMarket() {
		return typeId == com.anyoption.common.beans.Market.PRODUCT_TYPE_OPTION_PLUS;
	}

	public long getDisableAfterPeriod() {
		return disableAfterPeriod;
	}

	public void setDisableAfterPeriod(long disableAfterPeriod) {
		this.disableAfterPeriod = disableAfterPeriod;
	}

	public BigDecimal getSignificantPercentage() {
		return significantPercentage;
	}

	public void setSignificantPercentage(BigDecimal significantPercentage) {
		this.significantPercentage = significantPercentage;
	}

	public int getUpdatesPause() {
		return updatesPause;
	}

	public void setUpdatesPause(int updatesPause) {
		this.updatesPause = updatesPause;
	}

	public BigDecimal getRealSignificantPercentage() {
		return realSignificantPercentage;
	}

	public void setRealSignificantPercentage(BigDecimal realSignificantPercentage) {
		this.realSignificantPercentage = realSignificantPercentage;
	}

	public int getRealUpdatesPause() {
		return realUpdatesPause;
	}

	public void setRealUpdatesPause(int realUpdatesPause) {
		this.realUpdatesPause = realUpdatesPause;
	}

	public BigDecimal getFirstShiftParameter() {
		return firstShiftParameter;
	}

	public void setFirstShiftParameter(BigDecimal firstShiftParameter) {
		this.firstShiftParameter = firstShiftParameter;
	}

	public BigDecimal getPercentageOfAverageInvestments() {
		return percentageOfAverageInvestments;
	}

	public void setPercentageOfAverageInvestments(BigDecimal percentageOfAverageInvestments) {
		this.percentageOfAverageInvestments = percentageOfAverageInvestments;
	}

	public BigDecimal getFixedAmountForShifting() {
		return fixedAmountForShifting;
	}

	public void setFixedAmountForShifting(BigDecimal fixedAmountForShifting) {
		this.fixedAmountForShifting = fixedAmountForShifting;
	}

	public long getTypeOfShifting() {
		return typeOfShifting;
	}

	public void setTypeOfShifting(long typeOfShifting) {
		this.typeOfShifting = typeOfShifting;
	}

	public long getOptionPlusMarketId() {
		return optionPlusMarketId;
	}

	public String getSuspendedMessage() {
		return suspendedMessage;
	}

	public void setSuspendedMessage(String suspendedMessage) {
		this.suspendedMessage = suspendedMessage;
	}

	public Long getMarketGroupId() {
		return marketGroupId;
	}

	public BigDecimal getAverageInvestments() {
		return averageInvestments;
	}

	public void setAverageInvestments(BigDecimal averageInvestments) {
		this.averageInvestments = averageInvestments;
	}

	public int getHomePagePriority() {
		return homePagePriority;
	}

	public void setHomePagePriority(int homePagePriority) {
		this.homePagePriority = homePagePriority;
	}

	public boolean isHasGoodForHomepageCompare() {
		return hasGoodForHomepageCompare;
	}

	public void setHasGoodForHomepageCompare(int skinId) {
		this.hasGoodForHomepageCompare = hasGoodForHomepage(skinId);
	}

	public long getCompareSkinId() {
        return compareSkinId;
    }

	public boolean isBinary0100Market() {
		return typeId == com.anyoption.common.beans.Market.PRODUCT_TYPE_BINARY0100;
	}
	
	/**
     * Send update message for all opps in the item.
	 * @param isDelete should send delete msg
     */
    public void sendOppsUpdate(boolean isDelete) {
        Opportunity o = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            if ((o.isInOppenedState() || o.getState() == Opportunity.STATE_WAITING_TO_PAUSE) &&
            		!(o.getState() == Opportunity.STATE_CLOSED)) {
            	LevelService.getInstance().sendOppUpdate(o, isDelete ? ConstantsBase.JMS_OPPS_UPDATE_COMMAND_DELETE : 
            															ConstantsBase.JMS_OPPS_UPDATE_COMMAND_ADD);
            }
        }
    }
    
    private String getNextOpenHourly() {
    	TreeSet<Opportunity> oppTreeSet = new TreeSet<Opportunity>(new Comparator<Opportunity>() {
			@Override
			public int compare(Opportunity opp1, Opportunity opp2) {
	            return opp1.getTimeEstClosing().after(opp2.getTimeEstClosing()) ? 1 : -1;
	        }
		});
        Opportunity o = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY && o.isInOppenedState()) {
            	oppTreeSet.add(o);
            }
        }
        return oppTreeSet.size() > 1 ? getSecondElement(oppTreeSet) : null;
    }

	private String getSecondElement(TreeSet<Opportunity> oppTreeSet) {
		oppTreeSet.pollFirst();
		Opportunity o = oppTreeSet.pollFirst();
		return o.getId() + "|" + Utils.formatDate(o.getTimeEstClosing());
	}

	/**
	 * Retrieves the next opened opportunities for the given scheduled type(for hourly does not include the earliest open - the nearest option). The number of opportunities returned is
	 * determined by the {@code PERMITTED_NEXT_SCHEDULED_OPPORTUNITIES} constant. The opportunities are returned as string. The elements have
	 * format of {@code oppId|timeEstClosng} and are separated by semicolons.
	 * 
	 * @param scheduled the scheduled type of the opportunities to return
	 * @return a string consisting of the next opened opportunities
	 */
	private String getNextOpenedScheduledOpportunities(int scheduled) {
		TreeSet<Opportunity> oppTreeSet = new TreeSet<Opportunity>(new Comparator<Opportunity>() {

			@Override
			public int compare(Opportunity opp1, Opportunity opp2) {
				return opp1.getTimeEstClosing().after(opp2.getTimeEstClosing()) ? 1 : -1;
			}
		});
		Opportunity o = null;
		for (int i = 0; i < opportunities.size(); i++) {
			o = opportunities.get(i);
			if (o.getScheduled() == scheduled && o.isInOppenedState()) {
				oppTreeSet.add(o);
			}
		}
		if (scheduled == Opportunity.SCHEDULED_HOURLY) { // remove the nearest option
			oppTreeSet.pollFirst();
		}
		return oppTreeSet.size() > 0 ? getElements(oppTreeSet) : null;
	}

	/**
	 * Retrieves the elements from the given tree set with opportunities. The mechanism of retrieval is specified by
	 * {@link #getNextOpenedScheduledOpportunities()} method.
	 * 
	 * @param oppTreeSet the tree set from which the opportunities should be retrieved
	 * 
	 * @return a string consisting of the constructed elements
	 * 
	 * @see #getNextOpenedScheduledOpportunities()
	 */
	private String getElements(TreeSet<Opportunity> oppTreeSet) {
		int elementsCount = 0;
		StringBuilder elements = new StringBuilder();
		while (oppTreeSet.size() > 0 && elementsCount < PERMITTED_NEXT_SCHEDULED_OPPORTUNITIES) {
			Opportunity o = oppTreeSet.pollFirst();
			elements.append(o.getId()).append("|").append(Utils.formatDate(o.getTimeEstClosing())).append(";");
			elementsCount++;
		}
		if (elements.length() > 0) { // remove the last semicolon
			return elements.deleteCharAt(elements.length() - 1).toString();
		} else {
			return null;
		}
	}

	public boolean isDynamicsMarket() {
		return typeId == com.anyoption.common.beans.Market.PRODUCT_TYPE_DYNAMICS;
	}

	public boolean isBubblesMarket() {
		return typeId == com.anyoption.common.beans.Market.PRODUCT_TYPE_BUBBLES;
	}

	@Override
	public Map<String, BigDecimal> getQuoteFields() {
		return hourLevelFromula.getQuoteFields();
	}

	@Override
	public void handleBackendCommands(Map<BackendCommand, String> commands) {
		for (BackendCommand command : commands.keySet()) {
			if (command == BackendCommand.FXAL_CHANGE) {
				hourLevelFromula.handleBackendCommand(BackendCommand.FXAL_CHANGE, commands.get(BackendCommand.FXAL_CHANGE));
			}
		}
	}

	public long getTypeId() {
		return typeId;
	}

	class LevelsBean {
		public double level;
		public double devCheckLevel;
	}
}