package com.anyoption.service.level.market;

/**
 * @author kirilim
 */
public class MarketParams {

	private long dontCloseAfterXSecNoUpdate;
	private String timeZone;
	private String timePattern;
	private boolean shouldBeClosedByMonitoring;

	public long getDontCloseAfterXSecNoUpdate() {
		return dontCloseAfterXSecNoUpdate;
	}

	public void setDontCloseAfterXSecNoUpdate(long dontCloseAfterXSecNoUpdate) {
		this.dontCloseAfterXSecNoUpdate = dontCloseAfterXSecNoUpdate;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public boolean isShouldBeClosedByMonitoring() {
		return shouldBeClosedByMonitoring;
	}

	public void setShouldBeClosedByMonitoring(boolean shouldBeClosedByMonitoring) {
		this.shouldBeClosedByMonitoring = shouldBeClosedByMonitoring;
	}

	public String getTimePattern() {
		return timePattern;
	}

	public void setTimePattern(String timePattern) {
		this.timePattern = timePattern;
	}
}