//package com.anyoption.service.level.market;
//
//import java.math.BigDecimal;
//import java.sql.SQLException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Map;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.BinaryZeroOneHundred;
//import com.anyoption.common.beans.MarketConfig;
//import com.anyoption.common.beans.Opportunity;
//import com.anyoption.common.bl_vos.InvestmentType;
//import com.anyoption.common.enums.SkinGroup;
//import com.anyoption.common.jms.ETraderFeedMessage;
//import com.anyoption.service.level.Utils;
//import com.anyoption.service.level.formula.Binary0100Formula;
//import com.anyoption.service.level.formula.Binary0100Quote;
//
//import il.co.etrader.service.level.LevelService;
//import il.co.etrader.service.level.OpportunitiesManager;
//import il.co.etrader.service.level.SenderThread;
//import il.co.etrader.util.CommonUtil;
//
//public class Market0100 extends Market {
//    private static Logger log = Logger.getLogger(Market0100.class);
//
//    private static final int BINARY0100_DECIMAL_POINT = 1;
//
//    public Market0100(MarketConfig config, String name, long id, long typeId) throws Exception {
//		super(config, name, id, typeId);
//	}
//
//    @Override
//	public void addOpportunity(Opportunity o, SenderThread sender, long lifeId) {
//    	if (o.getQuoteParams() != null) {
//    		o.setBinaryZeroOneHundred(new BinaryZeroOneHundred(o.getQuoteParams()));
//    	}
//		super.addOpportunity(o, sender, lifeId);
//	}
//
//    /**
//     * Send update for item opp.
//     *
//     * @param sender
//     * @param lifeId
//     * @param o
//     * @param forseDelete if to forse "DELETE" command or to determine the command by the state
//     * @param sendCurrentLevel if to send current level update in this message (LS update) or just the real level
//     */
//	@Override
//    public void sendUpdate(SenderThread sender, long lifeId, Opportunity o, boolean forseDelete, boolean sendCurrentLevel) {
//        ETraderFeedMessage toSend = new ETraderFeedMessage(
//                //o == specialEntry?onHomePage:new HashMap<Integer, Boolean>(), //only if its specialEntry send the real hashmap else send new 1 its will act like all have false on home page
//        		null,
//        		sendCurrentLevel && (forseDelete || o == specialEntry || !suspended) ? getOpportunityLevelUpdate(o, forseDelete) : null,
//                lifeId,
//                getState().getLastCalcResult(), //use by insurance adapter (GM/RF)
//                o.getMarket().getGroupId(),
//                o.getId(),
//                0,
//                0, //not in use
//                null,
//        		o.getAutoShiftParameter(),
//         		o.getCalls(),
//         		o.getPuts(),
//         		marketName,
//         		decimalPoint,
//         		o.isDisabledTrader(),
//         		disabled,
//         		CommonUtil.roundDouble(o.getContractsBought(), 2),
//         		CommonUtil.roundDouble(o.getContractsSold(), 2),
//         		o.getBinaryZeroOneHundred(),
//         		0,//bid not in use for updates only for deviation check
//         		0,//offer not in use for updates only for deviation check
//         		null,
//         		!o.isClosingLevelReceived(),
//         		o.getSkinGroupMappings()
//        );
//        sender.send(toSend);
//    }
//
//	/**
//     * Prepare the <code>HashMap</code> with the values for the LS update.
//     *
//     * @param o
//     * @param levelET the current opportunity level for ET
//     * @param levelAO the current opportunity level for AO
//     * @param forseDelete if to forse "DELETE" command or to determine the command by the state
//     */
//    private HashMap<String, String> getOpportunityLevelUpdate(Opportunity o, boolean forseDelete) {
//        HashMap<String, String> flds = new HashMap<String, String>();
//        flds.put("key", String.valueOf(o.getId()));
//        flds.put("command", (forseDelete || o.getState() == Opportunity.STATE_DONE ? "DELETE" : "UPDATE"));
//        flds.put("BZ_MARKET_ID", String.valueOf(o.getMarketId()));
//        flds.put("BZ_EVENT", String.valueOf(o.getCurrentLevel())); //the binary 0-100 event level
//        Binary0100Quote q = ((Binary0100Formula) getHourLevelFromula()).getOpportunityQuote(this, o);
//        flds.put("BZ_OFFER", Utils.formatLevel(q.getOffer(), BINARY0100_DECIMAL_POINT)); //the OFFER price
//        flds.put("BZ_BID", Utils.formatLevel(q.getBid(), BINARY0100_DECIMAL_POINT)); //the BID price
//        double levelToSend = getState().getLastCalcResult();
//        if (o.getState() == Opportunity.STATE_CLOSED) {
//            levelToSend = getClosingLevel(o);
//        }
//        flds.put("BZ_CURRENT", Utils.formatLevel(levelToSend, (int) decimalPoint)); // current level
//
//        flds.put("BZ_LAST_INV", Utils.formatDate(o.getTimeLastInvest())); //time last invest
//        flds.put("BZ_EST_CLOSE", getTimeEstCloseForLevelUpdate(o)); //estimate close time
//        flds.put("BZ_SUSPENDED_MESSAGE", suspendedMessage); //suspend msg
//
//        flds.put("BZ_STATE", getOppStateForLevelUpdate(o)); //the state of the opportunity (OPENED, SUSPENDED etc)
//
//        flds.put("AO_TYPE", String.valueOf(o.getOpportunityTypeId())); //the opportunity type id
//        flds.put("AO_TS", String.valueOf(System.currentTimeMillis()));
//        // TODO why we accept investments when opp is suspended?
//        // TODO why the option is automatically updated upon suspend but is not updated through ls upon unsuspend?
//        flds.put("AO_OPP_STATE", o.isSuspended() ? "1" : "0");
//
//        Map<String, BigDecimal> quoFlds = getHourLevelFromula().getQuoteFields();
//        if (null != quoFlds) {
//        	BigDecimal val = null;
//        	for (String key : quoFlds.keySet()) {
//        		val = quoFlds.get(key);
//        		flds.put(key, val != null ? String.valueOf(val.doubleValue()) : "");
//        	}
//        }
//        return flds;
//    }
//
//    /**
//     * Handle investment notification for binary0-100. Here is where the exposure flow calculations are done
//     * as the exposure can change after an investment.
//    /**
//     * Handle investment notification for binary0-100.
//     *
//     * @param oppId the id of opp on which investment was made
//     * @param contracts the number of contracts that bought or sold
//     * @param amount the investments count
//     * @param type the type of investment (3 - buy/4 - sell)
//     * @return <code>true</code> if this investment caused stop/start trading else <code>false</code>.
//     */
//    @Override
//    public boolean investmentNotification(long oppId, double contracts, Double amount, long type, double aboveTotal, double belowTotal, long skinId) {
//        boolean isChangeState = false;
//        Opportunity opp = getOppById(oppId);
//        // first update the amounts and the contracts
//        double contracts2DP = CommonUtil.roundDouble(contracts, 2);
//        log.debug("opp calls " + opp.getCalls() + " above total " + aboveTotal);
//        log.debug("opp Puts " + opp.getPuts() + " below total " + belowTotal);
//        opp.setCalls(opp.getCalls() + aboveTotal); //if above
//        opp.setPuts(opp.getPuts() + belowTotal); //if below
//        log.debug("after opp calls " + opp.getCalls());
//        log.debug("after opp Puts " + opp.getPuts());
//        if (type == InvestmentType.BUY) {
//            opp.setContractsBought(CommonUtil.roundDouble(opp.getContractsBought(), 2) + contracts2DP);
//        } else {
//            opp.setContractsSold( CommonUtil.roundDouble(opp.getContractsSold(), 2) + contracts2DP);
//        }
//
//        double exposure = (CommonUtil.roundDouble(opp.getContractsBought(), 2) - CommonUtil.roundDouble(opp.getContractsSold(), 2)) / opp.getBinaryZeroOneHundred().getZ();
//
//        if (log.isInfoEnabled()) {
//            log.info("Investment notification binary Zero One Hundred -" +
//                    " item: " + marketName +
//                    " opportunity: " + opp.getId() +
//                    " exposure: " + exposure);
//        }
//        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyyy HH:mm:ss");
//        String desc = "Market: " + marketName +
//        " opportunity: " + opp.getId() +
//        " time: " + df.format(new Date(System.currentTimeMillis())) +
//        " contracts bought: " + CommonUtil.roundDouble(opp.getContractsBought(), 2) +
//        " contracts sold: " + CommonUtil.roundDouble(opp.getContractsSold(), 2) +
//        " Q: " +  opp.getBinaryZeroOneHundred().getQ() +
//		" T: " +  opp.getBinaryZeroOneHundred().getT() +
//		" S: " +  opp.getBinaryZeroOneHundred().getCThreashold();
//        if (Math.abs(exposure) >= opp.getBinaryZeroOneHundred().getCThreashold()) {
//        	if (!opp.isDisabledService()) {
//	        	desc = "stop trading " + desc;
//	        	log.warn(desc);
//	            LevelService.getInstance().sendBinray0100TradingHaltEmail("stop trading " + marketName, desc);
//	            opp.setDisabledService(true);
//        	}
//        } else {
//        	if (opp.isDisabledService()) {
//	        	desc = "start trading " + desc;
//	            log.warn(desc);
//	            LevelService.getInstance().sendBinray0100TradingHaltEmail("start trading " + marketName, desc);
//	            opp.setDisabledService(false);
//        	}
//        }
//        return isChangeState;
//    }
//
//    @Override
//    public void getOpenedOpportunityLevels(Opportunity o, ETraderFeedMessage toSend, SkinGroup skinGroup) {
//        Binary0100Quote q = ((Binary0100Formula) getHourLevelFromula()).getOpportunityQuote(this, o);
//        toSend.realLevel = getState().getLastCalcResult();
//        toSend.devCheckLevel = getState().getLastCalcResult();
//        toSend.isAutoDisable = Math.abs((CommonUtil.roundDouble(o.getContractsBought(), 2) -  CommonUtil.roundDouble(o.getContractsSold(), 2)) / o.getBinaryZeroOneHundred().getZ()) >= o.getBinaryZeroOneHundred().getCthreashold();
//        toSend.offer = q.getOffer();
//        toSend.bid = q.getBid();
//        toSend.investmentsRejectInfo = "Contracts Bought: " + CommonUtil.roundDouble(o.getContractsBought(), 2) + " , Contracts Sold: " +  CommonUtil.roundDouble(o.getContractsSold(), 2) +
//        									" , Z: " + o.getBinaryZeroOneHundred().getZ() + " , C: " + o.getBinaryZeroOneHundred().getCthreashold();
//    }
//
//    @Override
//	public void setSnapshotReceived(boolean snapshotReceived) {
//    	log.debug("setSnapshotReceived 0100 this.snapshotReceived = " + this.snapshotReceived + " , snapshotReceived = " + snapshotReceived);
//        if (!this.snapshotReceived && snapshotReceived) {
//        	this.snapshotReceived = snapshotReceived;
//            setTodayOpeningTime(System.currentTimeMillis());
//            LevelService lvlSrv = LevelService.getInstance();
//            refreshSpecialEntry(lvlSrv.getSender(), lvlSrv.getLifeId());
//            lvlSrv.updateHomePageOpportunities();
//            Opportunity o = null;
//            for (int i = 0; i < opportunities.size(); i++) {
//                o = opportunities.get(i);
//                log.debug("o.isInOppenedState() " + o.isInOppenedState() + " , o.getCurrentLevel() " + o.getCurrentLevel());
//                if (o.isInOppenedState() && o.getCurrentLevel() == 0) {
//                	openOpportunity(o);
//                }
//            }
//        } else {
//            this.snapshotReceived = snapshotReceived;
//        }
//	}
//    
//    @Override
//    public void openOpportunity(Opportunity o) {
//    	log.debug("openOpportunity " + o.getId());
//    	try {
//    		double eventLevel = Utils.roundDouble(getState().getLastCalcResult(), decimalPoint);
//			OpportunitiesManager.updateOppCurrentLevel(o.getId(), eventLevel);
//			o.setCurrentLevel(eventLevel);
//		} catch (SQLException e) {
//			log.error("cant write event level for oppId " + o.getId());
//		}
//    }
//}