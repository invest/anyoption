/**
 *
 */
package com.anyoption.service.level.market;

import java.util.ArrayList;
import java.util.List;

import com.anyoption.common.beans.Opportunity;

/**
 * @author pavelhe
 *
 */
public class UpdateResult {

	private List<Opportunity> opportunitiesToClose;

	public UpdateResult() {
		opportunitiesToClose = new ArrayList<Opportunity>();
	}

	/**
	 * @return the opportunitiesToClose
	 */
	public List<Opportunity> getOpportunitiesToClose() {
		return opportunitiesToClose;
	}

	/**
	 * @param opportunitiesToClose the opportunitiesToClose to set
	 */
	public void setOpportunitiesToClose(List<Opportunity> opportunitiesToClose) {
		this.opportunitiesToClose = opportunitiesToClose;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UpdateResult [opportunitiesToClose=").append(opportunitiesToClose).append("]");
		return builder.toString();
	}

}
