package com.anyoption.service.level.market;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.MarketConfig;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.bl_vos.InvestmentType;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.jms.ETraderFeedMessage;
import com.anyoption.common.util.DynamicsUtil;
import com.anyoption.service.level.Utils;
import com.anyoption.service.level.formula.DynamicsFormula;
import com.anyoption.service.level.formula.DynamicsQuote;

import il.co.etrader.service.level.LevelService;
import il.co.etrader.service.level.OpportunitiesManager;
import il.co.etrader.service.level.SenderThread;
import il.co.etrader.util.CommonUtil;

public class MarketDynamics extends Market {
	private static Logger log = Logger.getLogger(MarketDynamics.class); 

	private static final int DYNAMICS_DECIMAL_POINT = 1;

    public MarketDynamics(MarketConfig config, String name, long id, long typeId) throws Exception {
		super(config, name, id, typeId);
	}

    @Override
	public void addOpportunity(Opportunity o, SenderThread sender, long lifeId) {
    	if (o.getQuoteParams() != null) {
    		DynamicsUtil.parseOpportunityQuoteParams(o);
    	}
		super.addOpportunity(o, sender, lifeId);
	}

    /**
     * Send update for item opp.
     *
     * @param sender
     * @param lifeId
     * @param o
     * @param forseDelete if to forse "DELETE" command or to determine the command by the state
     * @param sendCurrentLevel if to send current level update in this message (LS update) or just the real level
     */
	@Override
    public void sendUpdate(SenderThread sender, long lifeId, Opportunity o, boolean forseDelete, boolean sendCurrentLevel) {
        ETraderFeedMessage toSend = new ETraderFeedMessage(
        		null,
        		sendCurrentLevel && (forseDelete || o == specialEntry || !suspended) ? getOpportunityLevelUpdate(o, forseDelete) : null,
                lifeId,
                getState().getLastCalcResult(), //use by insurance adapter (GM/RF)
                o.getMarket().getGroupId(),
                o.getId(),
                0,
                0, //not in use
                null,
        		o.getAutoShiftParameter(),
         		o.getCalls(),
         		o.getPuts(),
         		marketName,
         		decimalPoint,
         		o.isDisabledTrader(),
         		disabled,
         		CommonUtil.roundDouble(o.getContractsBought(), 2),
         		CommonUtil.roundDouble(o.getContractsSold(), 2),
         		o.getQuoteParamsObject(),
         		0, // bid not in use for updates only for deviation check
         		0, // offer not in use for updates only for deviation check
         		null,
         		!o.isClosingLevelReceived(),
         		o.getSkinGroupMappings()
        );
        sender.send(toSend);
    }

	/**
     * Prepare the <code>HashMap</code> with the values for the LS update.
     *
     * @param o
     * @param levelET the current opportunity level for ET
     * @param levelAO the current opportunity level for AO
     * @param forseDelete if to forse "DELETE" command or to determine the command by the state
     */
    private HashMap<String, String> getOpportunityLevelUpdate(Opportunity o, boolean forseDelete) {
        HashMap<String, String> flds = new HashMap<String, String>();
        flds.put("key", String.valueOf(o.getId()));
        flds.put("command", (forseDelete || o.getState() == Opportunity.STATE_DONE ? "DELETE" : "UPDATE"));
        flds.put("BZ_MARKET_ID", String.valueOf(o.getMarketId()));
        flds.put("BZ_EVENT", String.valueOf(o.getCurrentLevel()));
        DynamicsQuote q = ((DynamicsFormula) getHourLevelFromula()).getOpportunityQuote(this, o);
        flds.put("BZ_OFFER", Utils.formatLevel(q.getAsk(), DYNAMICS_DECIMAL_POINT));
        flds.put("BZ_BID", Utils.formatLevel(q.getBid(), DYNAMICS_DECIMAL_POINT));
        double levelToSend = getState().getLastCalcResult();
        if (o.getState() == Opportunity.STATE_CLOSED) {
            levelToSend = getClosingLevel(o);
        }
        flds.put("BZ_CURRENT", Utils.formatLevel(levelToSend, (int) decimalPoint)); // current level
        flds.put("BZ_LAST_INV", Utils.formatDate(o.getTimeLastInvest()));
        flds.put("BZ_EST_CLOSE", getTimeEstCloseForLevelUpdate(o));
        flds.put("BZ_SUSPENDED_MESSAGE", suspendedMessage);
        flds.put("BZ_STATE", getOppStateForLevelUpdate(o));
        flds.put("AO_TYPE", String.valueOf(o.getOpportunityTypeId()));
        flds.put("AO_TS", String.valueOf(System.currentTimeMillis()));
        // TODO why we accept investments when opp is suspended?
        // TODO why the option is automatically updated upon suspend but is not updated through ls upon unsuspend?
        flds.put("AO_OPP_STATE", o.isSuspended() ? "1" : "0");

        Map<String, BigDecimal> quoFlds = getHourLevelFromula().getQuoteFields();
        if (null != quoFlds) {
        	BigDecimal val = null;
        	for (String key : quoFlds.keySet()) {
        		val = quoFlds.get(key);
        		flds.put(key, val != null ? String.valueOf(val.doubleValue()) : "");
        	}
        }
        return flds;
    }

    /**
     * Handle investment notification for dynamics.
     *
     * @param oppId the id of opp on which investment was made
     * @param invId
     * @param amount the investment amount in USD
     * @param type the type of investment (3 - buy/4 - sell)
     * @return <code>true</code> if this investment caused stop/start trading else <code>false</code>.
     */
    @Override
    public boolean investmentNotification(long oppId, double invId, Double amount, long type, double aboveTotal, double belowTotal, long skinId) {
        Opportunity opp = getOppById(oppId);
        opp.setCalls(opp.getCalls() + aboveTotal);
        opp.setPuts(opp.getPuts() + belowTotal);
        if (type == InvestmentType.DYNAMICS_BUY) {
            opp.setContractsBought(opp.getContractsBought() + amount);
        } else {
            opp.setContractsSold(opp.getContractsSold() + amount);
        }
        log.debug("above amount: " + opp.getContractsBought() + " below amount: " + opp.getContractsSold() + " if above result: " + opp.getCalls() + " if below result: " + opp.getPuts());
        return false;
    }

    @Override
    public void getOpenedOpportunityLevels(Opportunity o, ETraderFeedMessage toSend, SkinGroup skinGroup) {
    	DynamicsQuote q = ((DynamicsFormula) getHourLevelFromula()).getOpportunityQuote(this, o);
        toSend.realLevel = getState().getLastCalcResult();
        toSend.devCheckLevel = getState().getLastCalcResult();
        toSend.isAutoDisable = Math.abs(CommonUtil.roundDouble(o.getContractsBought(), 2) -  CommonUtil.roundDouble(o.getContractsSold(), 2)) >= o.getSkinGroupMappings().get(skinGroup).getMaxExposure();
        toSend.offer = q.getAsk();
        toSend.bid = q.getBid();
        if (toSend.isAutoDisable) {
	        toSend.investmentsRejectInfo = "Contracts Bought: " + CommonUtil.roundDouble(o.getContractsBought(), 2) + " , Contracts Sold: " +  CommonUtil.roundDouble(o.getContractsSold(), 2)
	        		+ " , max exposure: " + o.getSkinGroupMappings().get(skinGroup).getMaxExposure();
        }
    }

    @Override
	public void setSnapshotReceived(boolean snapshotReceived) {
    	log.debug("setSnapshotReceived dynamics this.snapshotReceived = " + this.snapshotReceived + " , snapshotReceived = " + snapshotReceived);
        if (!this.snapshotReceived && snapshotReceived) {
        	this.snapshotReceived = snapshotReceived;
            setTodayOpeningTime(System.currentTimeMillis());
            LevelService lvlSrv = LevelService.getInstance();
            refreshSpecialEntry(lvlSrv.getSender(), lvlSrv.getLifeId());
            lvlSrv.updateHomePageOpportunities();
            Opportunity o = null;
            for (int i = 0; i < opportunities.size(); i++) {
                o = opportunities.get(i);
                log.debug("o.isInOppenedState() " + o.isInOppenedState() + " , o.getCurrentLevel() " + o.getCurrentLevel());
                if (o.isInOppenedState() && o.getCurrentLevel() == 0) {
                	openOpportunity(o);
                }
            }
        } else {
            this.snapshotReceived = snapshotReceived;
        }
	}
    
    @Override
    public void openOpportunity(Opportunity o) {
    	log.debug("openOpportunity " + o.getId());
    	try {
    		double eventLevel = Utils.roundDouble(getState().getLastCalcResult(), decimalPoint);
			OpportunitiesManager.updateOppCurrentLevel(o.getId(), eventLevel);
			o.setCurrentLevel(eventLevel);
		} catch (SQLException e) {
			log.error("cant write event level for oppId " + o.getId());
		}
    }
}