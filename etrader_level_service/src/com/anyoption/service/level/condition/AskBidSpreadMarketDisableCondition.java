package com.anyoption.service.level.condition;

import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AskBidSpreadMarketDisableCondition implements MarketDisableCondition {
	private Config config;
	private Double lastAsk;
	private Double lastBid;
	
	public AskBidSpreadMarketDisableCondition(String config) {
		Gson gson = new GsonBuilder().serializeNulls().create();
		this.config = gson.fromJson(config, Config.class);
	}
	
	/**
	 * This method is called every time data update arrive to check on the field values.
	 * 
	 * @param dataSubscription
	 * @param fields
	 * @return String description of the reason if the market should be disabled after this update else <code>null</code> if the market should be enabled
	 */
	@Override
	public String shouldDisableMarket(String dataSubscription, Map<String, Object> fields) {
		if (dataSubscription.startsWith(config.subscriptionName)) {
			if (null != fields.get(config.askField)) {
				lastAsk = (Double) fields.get(config.askField);
			}
			if (null != fields.get(config.bidField)) {
				lastBid = (Double) fields.get(config.bidField);
			}
		}

		if (null == lastAsk || null == lastBid) {
			return null;
		}
		
		if (lastAsk - lastBid > config.spread) {
			return "(" + config.askField + " - " + config.bidField + ") > " + config.spread + "; "
					+ config.askField + " = " + lastAsk + " "
					+ config.bidField + " = " + lastBid;
		}
		return null;
	}
	
	/**
	 * This method is called by the <code>MonitoringThread</code> to check specific time sensitive disable conditions.
	 * 
	 * @return String description of the reason if the market should be disabled after this update else <code>null</code> if the market should be enabled
	 */
	@Override
	public String shouldDisableMarket() {
		return null;
	}
	
	@Override
	public void resetInTheEndOfTheDay() {
		lastAsk = null;
		lastBid = null;
	}
	
	class Config {
		private String subscriptionName;
		private String askField;
		private String bidField;
		private double spread;
	}
}