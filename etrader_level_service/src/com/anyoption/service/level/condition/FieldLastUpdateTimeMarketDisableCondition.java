package com.anyoption.service.level.condition;

import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class FieldLastUpdateTimeMarketDisableCondition implements MarketDisableCondition {
	private Config config;
	private long fieldLastUpdateTime;
	
	public FieldLastUpdateTimeMarketDisableCondition(String config) {
		Gson gson = new GsonBuilder().serializeNulls().create();
		this.config = gson.fromJson(config, Config.class);
	}
	/**
	 * This method is called every time data update arrive to check on the field values.
	 * 
	 * @param dataSubscription
	 * @param fields
	 * @return String description of the reason if the market should be disabled after this update else <code>null</code> if the market should be enabled
	 */
	@Override
	public String shouldDisableMarket(String dataSubscription, Map<String, Object> fields) {
		if (!dataSubscription.startsWith(config.subscriptionName)) {
			return null;
		}
		
		if (null != fields.get(config.fieldName)) {
			fieldLastUpdateTime = System.currentTimeMillis();
		}
		return null;
	}
	
	/**
	 * This method is called by the <code>MonitoringThread</code> to check specific time sensitive disable conditions.
	 * 
	 * @return String description of the reason if the market should be disabled after this update else <code>null</code> if the market should be enabled
	 */
	@Override
	public String shouldDisableMarket() {
		if (fieldLastUpdateTime > 0 && // make sure we got one update so we don't disable all on startup
                System.currentTimeMillis() - fieldLastUpdateTime >= config.disablePeriod * 1000) {
            String str = config.subscriptionName + " " + config.fieldName + " not updated for " + config.disablePeriod + " sec";
            return str;
		}
		return null;
	}
	
	public void resetInTheEndOfTheDay() {
		fieldLastUpdateTime = 0;
	}
	
	class Config {
		private String subscriptionName;
		private String fieldName;
		private long disablePeriod;
	}
}