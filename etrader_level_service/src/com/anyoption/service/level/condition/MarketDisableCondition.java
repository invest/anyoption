package com.anyoption.service.level.condition;

import java.util.Map;

public interface MarketDisableCondition {
	/**
	 * This method is called every time data update arrive to check on the field values.
	 * 
	 * @param dataSubscription
	 * @param fields
	 * @return String description of the reason if the market should be disabled after this update else <code>null</code> if the market should be enabled
	 */
	public String shouldDisableMarket(String dataSubscription, Map<String, Object> fields);
	
	/**
	 * This method is called by the <code>MonitoringThread</code> to check specific time sensitive disable conditions.
	 * 
	 * @return String description of the reason if the market should be disabled after this update else <code>null</code> if the market should be enabled
	 */
	public String shouldDisableMarket();
	
	public void resetInTheEndOfTheDay();
}