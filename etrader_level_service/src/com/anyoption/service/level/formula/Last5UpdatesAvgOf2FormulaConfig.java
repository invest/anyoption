package com.anyoption.service.level.formula;

public class Last5UpdatesAvgOf2FormulaConfig extends FormulaConfig {
	private String field1Name;
	private String field2Name;

	public String getField1Name() {
		return field1Name;
	}

	public void setField1Name(String field1Name) {
		this.field1Name = field1Name;
	}

	public String getField2Name() {
		return field2Name;
	}

	public void setField2Name(String field2Name) {
		this.field2Name = field2Name;
	}
}