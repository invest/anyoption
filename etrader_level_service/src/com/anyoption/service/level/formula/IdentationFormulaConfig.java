package com.anyoption.service.level.formula;

/**
 * @author EyalG
 */
public class IdentationFormulaConfig extends FormulaConfig {

	private String field1Name;
	private String lastTradeTimeField;

	public String getField1Name() {
		return field1Name;
	}

	public void setField1Name(String field1Name) {
		this.field1Name = field1Name;
	}

	public String getLastTradeTimeField() {
		return lastTradeTimeField;
	}

	public void setLastTradeTimeField(String lastTradeTimeField) {
		this.lastTradeTimeField = lastTradeTimeField;
	}
}