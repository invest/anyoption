package com.anyoption.service.level.formula;

public class IndexStockLongTermFormulaConfig extends FormulaConfig {
	private String interestSubscription;
	private String interestAskField;
	private String interestBidField;
	private int daysConstant;
	
	public String getInterestSubscription() {
		return interestSubscription;
	}
	
	public void setInterestSubscription(String interestSubscription) {
		this.interestSubscription = interestSubscription;
	}
	
	public String getInterestAskField() {
		return interestAskField;
	}
	
	public void setInterestAskField(String interestAskField) {
		this.interestAskField = interestAskField;
	}
	
	public String getInterestBidField() {
		return interestBidField;
	}
	
	public void setInterestBidField(String interestBidField) {
		this.interestBidField = interestBidField;
	}
	
	public int getDaysConstant() {
		return daysConstant;
	}
	
	public void setDaysConstant(int daysConstant) {
		this.daysConstant = daysConstant;
	}
}