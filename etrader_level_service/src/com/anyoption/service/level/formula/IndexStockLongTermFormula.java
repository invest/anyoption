package com.anyoption.service.level.formula;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.service.level.Utils;
import com.anyoption.service.level.market.Market;
import com.anyoption.service.level.market.MarketState;
import com.anyoption.service.level.market.UpdateResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class IndexStockLongTermFormula extends Formula {
	private static final Logger log = Logger.getLogger(IndexStockLongTermFormula.class);
	
	private IndexStockLongTermFormulaConfig formulaConfig;
	private boolean snapshotReceived;
	private boolean interestSnapshotReceived;
	private Double interestLastASK;
	private Double interestLastBID;
	private double c6;

	public IndexStockLongTermFormula(String formulaParams, FormulaRole role) {
		super();
		Gson gson = new GsonBuilder().serializeNulls().create();
		formulaConfig = gson.fromJson(formulaParams, IndexStockLongTermFormulaConfig.class);
		formulaConfig.setDefaultSubscriptionConfig(formulaConfig.cloneSubscriptionConfig(formulaConfig.getSubscriptionConfig()));
		this.role = role;
	}

	@Override
	public void collectDataSubscription(Map<String, Map<String, String>> subscriptionConfigs) {
		subscriptionConfigs.putAll(formulaConfig.getSubscriptionConfig());
	}

	@Override
	public void dataUpdate(String dataSubscription, Map<String, Object> fields, Market market, List<Opportunity> opportunities, UpdateResult result) {
		if (dataSubscription.equals(formulaConfig.getInterestSubscription())) {
			Double ask = (Double) fields.get(formulaConfig.getInterestAskField());
			if (ask != null) {
				interestLastASK = ask;
			}
			Double bid = (Double) fields.get(formulaConfig.getInterestBidField());
			if (bid != null) {
				interestLastBID = bid;
			}
			if (interestLastASK != null && interestLastBID != null) {
				c6 = (interestLastASK + interestLastBID) / 2;
				interestSnapshotReceived = true;
			}
		} else {
			snapshotReceived = true;
		}
		
		if (isSnapshotReceived()) {
            MarketState state = market.getState();
            
            Opportunity o = market.getOpenedOppByScheduled(Opportunity.SCHEDULED_WEEKLY);
            if (null != o) {
	            state.setLastWeekResult(calc(o, state.getLastCalcResult()));
            }

            o = market.getOpenedOppByScheduled(Opportunity.SCHEDULED_MONTHLY);
            if (null != o) {
	            state.setLastMonthResult(calc(o, state.getLastCalcResult()));
            }

            o = market.getOpenedOppByScheduled(Opportunity.SCHEDULED_QUARTERLY);
            if (null != o) {
	            state.setLastQuarterResult(calc(o, state.getLastCalcResult()));
            }
		}
	}
	
	private double calc(Opportunity o, double lastCacl) {
		long c5 = Utils.getDaysToExpire(o.getTimeEstClosing());
        log.trace("oppId: " + o.getId() + " days to expire: " + c5);
		if (log.isTraceEnabled()) {
			DecimalFormat df = new DecimalFormat("###.#####");
			log.trace("c5: " + df.format(c5) + " c6: " + df.format(c6) + " daysConstant: " + formulaConfig.getDaysConstant() + " lastCalcResult: " + lastCacl
						+ "; role: " + role);
		}
		return lastCacl * (1 + c6 / 100 / formulaConfig.getDaysConstant() * c5);
	}

	@Override
	public FormulaConfig getFormulaConfig() {
		return formulaConfig;
	}

	@Override
	public boolean isSnapshotReceived() {
		return snapshotReceived && interestSnapshotReceived;
	}

	@Override
	public void resetInTheEndOfTheDay() {
		snapshotReceived = false;
		interestSnapshotReceived = false;
		formulaConfig.setSubscriptionConfig(formulaConfig.cloneSubscriptionConfig(formulaConfig.getDefaultSubscriptionConfig()));
	}
	
	@Override
	public Map<String, BigDecimal> getQuoteFields() {
		// This one should never be used. Maybe throw an exception...
		return null;
	}
}