package com.anyoption.service.level.formula;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.service.level.Utils;
import com.anyoption.service.level.market.Market;
import com.anyoption.service.level.market.UpdateResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Last5UpdatesAvgOf2Formula extends Formula {
	private static final Logger log = Logger.getLogger(Last5UpdatesAvgOf2Formula.class);
	private BigDecimal field1;
	private BigDecimal field2;
	private Last5UpdatesAvgOf2FormulaConfig formulaConfig;
	private boolean snapshotReceived;

	private static final double[] QUOTE_WEIGHTS = {0.5d, 0.3d, 0.2d};
    protected ArrayList<Double> lastFiveQuotes;

	public Last5UpdatesAvgOf2Formula(String formulaParams, FormulaRole role) {
		super();
		Gson gson = new GsonBuilder().serializeNulls().create();
		formulaConfig = gson.fromJson(formulaParams, Last5UpdatesAvgOf2FormulaConfig.class);
		formulaConfig.setDefaultSubscriptionConfig(formulaConfig.cloneSubscriptionConfig(formulaConfig.getSubscriptionConfig()));
		this.role = role;

		lastFiveQuotes = new ArrayList<Double>();
	}

	public Last5UpdatesAvgOf2Formula(Last5UpdatesAvgOf2FormulaConfig formulaConfig, FormulaRole role) {
		super();
		this.formulaConfig = formulaConfig;
		formulaConfig.setDefaultSubscriptionConfig(formulaConfig.cloneSubscriptionConfig(formulaConfig.getSubscriptionConfig()));
		this.role = role;
		lastFiveQuotes = new ArrayList<Double>();
	}

	@Override
	public void collectDataSubscription(Map<String, Map<String, String>> subscriptionConfigs) {
		subscriptionConfigs.putAll(formulaConfig.getSubscriptionConfig());
	}

	@Override
	public void dataUpdate(String dataSubscription, Map<String, Object> fields, Market market, List<Opportunity> opportunities, UpdateResult result) {
		Double fieldOne = (Double) fields.get(formulaConfig.getField1Name());
		Double fieldTwo = (Double) fields.get(formulaConfig.getField2Name());
		if ((null == fieldOne && null == fieldTwo) || (null != fieldOne && fieldOne == 0) || (null != fieldTwo && fieldTwo == 0)) {
			return;
		}

		market.setLastUpdateReceiveTime(System.currentTimeMillis());

		if (fieldOne != null) {
			field1 = new BigDecimal(fieldOne);
		}
		if (fieldTwo != null) {
			field2 = new BigDecimal(fieldTwo);
		}

		if (field1 == null || field2 == null) {
			return;
		}
        double lastUpdateAvg = field1.add(field2).divide(new BigDecimal(2)).doubleValue();
        lastFiveQuotes.add(lastUpdateAvg);
        if (lastFiveQuotes.size() > 5) {
            lastFiveQuotes.remove(0);
        }

        Double newLevel = calcLevel();
        // for test
        log.trace("lastFiveQuotes: " + lastFiveQuotes + "; newLevel: " + newLevel + "; role: " + role);
		// This formula cannot be used as FormulaRole.LONG_TERM
		if (role == FormulaRole.CURRENT_LEVEL) {
			double crrLevel = Utils.formulaRandomChange(market.getState(), newLevel, market.getRandomCeiling(), market.getRandomFloor());
			market.getState().setLastCalcResult(newLevel);
			market.getState().setLastFormulaResult(crrLevel);
		} else if (role == FormulaRole.REAL_LEVEL) {
			market.getState().setLastRealLevel(newLevel);
		} else { // (role == FormulaRole.DAY_CLOSING || role == FormulaRole.HOUR_CLOSING)
			log.warn("Last5Updates2Fields formula configured as closing level. NOT SUPPORTED!!!");
		}
		snapshotReceived = true;
	}

	@Override
	public FormulaConfig getFormulaConfig() {
		return formulaConfig;
	}

	@Override
	public boolean isSnapshotReceived() {
		return snapshotReceived;
	}

	@Override
	public void resetInTheEndOfTheDay() {
		field1 = null;
		field2 = null;
		snapshotReceived = false;
		lastFiveQuotes.clear();
		formulaConfig.setSubscriptionConfig(formulaConfig.cloneSubscriptionConfig(formulaConfig.getDefaultSubscriptionConfig()));
//		lastResult = null;
//		lastTradeTime = null;
	}
    
    /**
     * @return The new level if the update was successful, otherwise <code>null</code>.
     */
    private Double calcLevel() {
    	switch (lastFiveQuotes.size()) {
		case 1:
			return lastFiveQuotes.get(0);
		case 2:
			return (lastFiveQuotes.get(0) + lastFiveQuotes.get(1)) / 2d;
		case 3:
			return lastFiveQuotes.get(2) * QUOTE_WEIGHTS[0]
							+ lastFiveQuotes.get(1) * QUOTE_WEIGHTS[1]
							+ lastFiveQuotes.get(0) * QUOTE_WEIGHTS[2];
		case 4:
		case 5:
			double min = Double.MAX_VALUE;
            double max = Double.MIN_VALUE;
            double crr;
            for (int i = 0; i < lastFiveQuotes.size(); i++) {
                crr = lastFiveQuotes.get(i);
                if (crr < min) {
                    min = crr;
                }
                if (crr > max) {
                    max = crr;
                }
            }
            
            double sum = 0;
            int weightIdx = 0;
            boolean minSkipped = false;
            boolean maxSkipped = false;
            for (int i = lastFiveQuotes.size() - 1; i >= 0; i--) {
            	crr = lastFiveQuotes.get(i);
            	if (crr == min && !minSkipped) {
            		minSkipped = true;
            		continue;
            	} else if (lastFiveQuotes.size() == 5
            					&& crr == max
            					&& !maxSkipped) {
            		maxSkipped = true;
            		continue;
            	} else {
            		sum += crr * QUOTE_WEIGHTS[weightIdx++];
            	}
            }
            
            return sum;
		}
		log.error("Illegal size of last quotes list [" + lastFiveQuotes + "]");
        return null;
    }
    
    public BigDecimal getField1() {
    	return field1;
    }
    
    public BigDecimal getField2() {
    	return field2;
    }
    
    @Override
    public Map<String, BigDecimal> getQuoteFields() {
		// Here we assume that if we are doing avg of 2 fields they will be ASK and BID
		// Also we assume that we are with Reuters as feed and there the ASK and BID fields are named ASK and BID
		// If one of the above is not ture we will need a more sophisticated implementation
		Map<String, BigDecimal> q = new HashMap<String, BigDecimal>();
		q.put(formulaConfig.getField1Name(), field1);
		q.put(formulaConfig.getField2Name(), field2);
		return q;
    }
}