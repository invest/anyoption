package com.anyoption.service.level.formula;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.service.level.Utils;
import com.anyoption.service.level.market.Market;
import com.anyoption.service.level.market.MarketState;
import com.anyoption.service.level.market.UpdateResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class CurrencyLongTermFormula extends Formula {
	private static final Logger log = Logger.getLogger(CurrencyLongTermFormula.class);
	
	private CurrencyLongTermFormulaConfig formulaConfig;
	private boolean snapshotReceived;
	private boolean interest1SnapshotReceived;
	private boolean interest2SnapshotReceived;
	private Double interest1LastASK;
	private Double interest1LastBID;
	private double c9;
	private Double interest2LastASK;
	private Double interest2LastBID;
	private double c10;

	public CurrencyLongTermFormula(String formulaParams, FormulaRole role) {
		super();
		Gson gson = new GsonBuilder().serializeNulls().create();
		formulaConfig = gson.fromJson(formulaParams, CurrencyLongTermFormulaConfig.class);
		formulaConfig.setDefaultSubscriptionConfig(formulaConfig.cloneSubscriptionConfig(formulaConfig.getSubscriptionConfig()));
		this.role = role;
	}

	@Override
	public void collectDataSubscription(Map<String, Map<String, String>> subscriptionConfigs) {
		subscriptionConfigs.putAll(formulaConfig.getSubscriptionConfig());
	}

	@Override
	public void dataUpdate(String dataSubscription, Map<String, Object> fields, Market market, List<Opportunity> opportunities, UpdateResult result) {
		if (dataSubscription.equals(formulaConfig.getInterestSubscription1())) {
			Double ask = (Double) fields.get(formulaConfig.getInterestAskField());
			if (null != ask) {
				interest1LastASK = ask;
			}
			Double bid = (Double) fields.get(formulaConfig.getInterestBidField());
			if (null != bid) {
				interest1LastBID = bid;
			}
			if (interest1LastASK != null && interest1LastBID != null) {
				c9 = (interest1LastASK + interest1LastASK) / 2; // roundDouble(interestUSD1M.getAsk() + interestUSD1M.getBid(), decimalPoint) / 2;
				interest1SnapshotReceived = true;
			}
		} else if (dataSubscription.equals(formulaConfig.getInterestSubscription2())) {
			Double ask = (Double) fields.get(formulaConfig.getInterestAskField());
			if (null != ask) {
				interest2LastASK = ask;
			}
			Double bid = (Double) fields.get(formulaConfig.getInterestBidField());
			if (null != bid) {
				interest2LastBID = bid;
			}
			if (interest2LastASK != null && interest2LastBID != null) {
				c10 = (interest2LastASK + interest2LastBID) / 2; // roundDouble(interestCRR1M.getAsk() + interestCRR1M.getBid(), decimalPoint) / 2;
				interest2SnapshotReceived = true;
			}
		} else {
			snapshotReceived = true;
		}
		
		if (isSnapshotReceived()) {
            MarketState state = market.getState();
            
            Opportunity o = market.getOpenedOppByScheduled(Opportunity.SCHEDULED_WEEKLY);
            if (null != o) {
	            state.setLastWeekResult(calc(o, state.getLastCalcResult()));
            }

            o = market.getOpenedOppByScheduled(Opportunity.SCHEDULED_MONTHLY);
            if (null != o) {
	            state.setLastMonthResult(calc(o, state.getLastCalcResult()));
            }

            o = market.getOpenedOppByScheduled(Opportunity.SCHEDULED_QUARTERLY);
            if (null != o) {
	            state.setLastQuarterResult(calc(o, state.getLastCalcResult()));
            }
		}
	}

	@Override
	public FormulaConfig getFormulaConfig() {
		return formulaConfig;
	}

	private double calc(Opportunity o, double lastCacl) {
        double c11 = 1;
        long daysToExpire = Utils.getDaysToExpire(o.getTimeEstClosing());
        if (daysToExpire > 2) {
            c11 = daysToExpire - formulaConfig.getSpotConstant();
        } else {
            c11 = daysToExpire;
        }
        double c13 = c9 / formulaConfig.getDaysConstant1() * c11;
        double c14 = c10 / formulaConfig.getDaysConstant2() * c11;
        double c15 = c14 + 1;
        double c16 = c13 + 1;
        double c17 = 1;
        if (formulaConfig.isDivideC16ByC15()) {
            c17 = c16 / c15;
        } else {
            c17 = c15 / c16;
        }
        if (log.isTraceEnabled()) {
        	DecimalFormat df = new DecimalFormat("###.#####");
            log.trace(
                    " c9: " + df.format(c9) +
                    " c10: " + df.format(c10) +
                    " c11: " + df.format(c11) +
                    " c13: " + df.format(c13) +
                    " c14: " + df.format(c14) +
                    " c15: " + df.format(c15) +
                    " c16: " + df.format(c16) +
                    " c17: " + df.format(c17) +
                    " lastCalcResult: " + lastCacl +
                    "; role: " + role);
        }
        return c17 * lastCacl;
	}

	@Override
	public boolean isSnapshotReceived() {
		return snapshotReceived && interest1SnapshotReceived && interest2SnapshotReceived;
	}

	@Override
	public void resetInTheEndOfTheDay() {
		snapshotReceived = false;
		interest1SnapshotReceived = false;
		interest1LastASK = null;
		interest1LastBID = null;
		interest2SnapshotReceived = false;
		interest2LastASK = null;
		interest2LastBID = null;
		formulaConfig.setSubscriptionConfig(formulaConfig.cloneSubscriptionConfig(formulaConfig.getDefaultSubscriptionConfig()));
	}
	
	@Override
	public Map<String, BigDecimal> getQuoteFields() {
		// This one should never be used. Maybe throw an exception...
		return null;
	}
}