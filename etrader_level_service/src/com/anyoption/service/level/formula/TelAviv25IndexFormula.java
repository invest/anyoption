package com.anyoption.service.level.formula;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.service.level.market.Market;
import com.anyoption.service.level.market.UpdateResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import il.co.etrader.service.level.BigDecimalHelper;
import il.co.etrader.service.level.LevelService;
import il.co.etrader.service.level.OpportunitiesManager;

/**
 * @author kiril.mutafchiev
 */
public class TelAviv25IndexFormula extends Formula implements FormulaIfc {

	private static final Logger log = Logger.getLogger(TelAviv25IndexFormula.class);
	private BigDecimal field1;
	private Boolean nextMonth;
	private TelAviv25IndexFormulaConfig formulaConfig;
	private boolean snapshotReceived;
	private List<TelAvivOption> options;

	public TelAviv25IndexFormula(String formulaParams, FormulaRole role) {
		super();
		Gson gson = new GsonBuilder().serializeNulls().create();
		formulaConfig = gson.fromJson(formulaParams, TelAviv25IndexFormulaConfig.class);
		formulaConfig.setDefaultSubscriptionConfig(formulaConfig.cloneSubscriptionConfig(formulaConfig.getSubscriptionConfig()));
		this.role = role;
		options = new ArrayList<>();
	}

	@Override
	public void collectDataSubscription(Map<String, Map<String, String>> subscriptionConfigs) {
		subscriptionConfigs.putAll(formulaConfig.getSubscriptionConfig());
	}

	@Override
	public void dataUpdate(	String dataSubscription, Map<String, Object> fields, Market market, List<Opportunity> opportunities,
							UpdateResult result) {
		if (dataSubscription.equals(formulaConfig.getMainSubscriptionName())) {
			handleMainSubscriptionUpdate(dataSubscription, fields, market, opportunities, result);			
		} else {
			handleSecondarySubscriptionUpdate(dataSubscription, fields, market, opportunities, result);
		}
	}

	private void handleMainSubscriptionUpdate(	String dataSubscription, Map<String, Object> fields, Market market,
												List<Opportunity> opportunities, UpdateResult result) {
		Double trdprc1 = (Double) fields.get(formulaConfig.getLastTradePriceField());
		String tradeTime = (String) fields.get(formulaConfig.getLastTradeTimeField());
		if (nextMonth == null) {
			nextMonth = OpportunitiesManager.getOpportunityOptionsChangeMonth(market.getMarketId());
		}
		if (null != trdprc1 && trdprc1 > 0) {
			this.field1 = new BigDecimal(trdprc1);
        }
		if (tradeTime != null) {
			this.lastTradeTime = tradeTime;
		}

		if (trdprc1 != null && trdprc1 > 0) {
			market.setLastUpdateReceiveTime(System.currentTimeMillis());
			log.trace("TRADINGDATA_.TA25 - .TA25 TRDPRC_1: " + trdprc1);
			long y = Math.round(trdprc1 / 10) * 10; // round to be multiple of 10
			 // TODO: Tony: handle the option name creation when the level goes under 1000
            char call = getCallLetter(nextMonth);
            char put = getPutLetter(nextMonth);
            String year = new SimpleDateFormat("YY").format(new Date());
            String yM10CName = "TLVJ" + getStrike(y - 10) + call + year + ".TA";
            String yM10PName = "TLVJ" + getStrike(y - 10) + put + year + ".TA";
            String yCName = "TLVJ" + getStrike(y) + call + year + ".TA";
            String yPName = "TLVJ" + getStrike(y) + put + year + ".TA";
            String yP10CName = "TLVJ" + getStrike(y + 10) + call + year + ".TA";
            String yP10PName = "TLVJ" + getStrike(y + 10) + put + year + ".TA";
            if (log.isTraceEnabled()) {
                log.trace("Y = " + y + " " + yM10CName + " " + yM10PName + " " + yCName + " " + yPName + " " + yP10CName + " " + yP10PName);
            }
			if (options.size() < 6) { // first .TA25 update
				resubscribe(new TelAvivOption[] {	new TelAvivOption(yM10CName, y - 10),
				                                 	new TelAvivOption(yM10PName, y - 10),
													new TelAvivOption(yCName, y),
													new TelAvivOption(yPName, y),
													new TelAvivOption(yP10CName, y + 10),
													new TelAvivOption(yP10PName, y + 10)},
							market);
			} else { // see what we can reuse from the currently subscribed options the 6 options should go in package so if the strike of
					 // the 3rd and 4th is the same as y we can reuse them
				double oldy = options.get(2).getStrike();
				log.trace("old Y = " + oldy);
				if (oldy < y) { // the quote went up. we need to drop the y - 10 (at least) and subscr for new y + 10 (at least)
                    double move = (y - oldy) / 10;
                    if (move == 1) { // we can reuse 4 options subscr
						resubscribe(new TelAvivOption[] {	new TelAvivOption(yP10CName, y + 10),
															new TelAvivOption(yP10PName, y + 10)},
									market);
                    } else if (move == 2) { // we can reuse 2 options subscr
                    	resubscribe(new TelAvivOption[] {
                                                  new TelAvivOption(yCName, y),
                                                  new TelAvivOption(yPName, y),
                                                  new TelAvivOption(yP10CName, y + 10),
                                                  new TelAvivOption(yP10PName, y + 10)},
                    	            market);
                    } else { // we can't reuse a thing
                    	resubscribe(new TelAvivOption[] {
                                                  new TelAvivOption(yM10CName, y - 10),
                                                  new TelAvivOption(yM10PName, y - 10),
                                                  new TelAvivOption(yCName, y),
                                                  new TelAvivOption(yPName, y),
                                                  new TelAvivOption(yP10CName, y + 10),
                                                  new TelAvivOption(yP10PName, y + 10)},
                    	            market);
                    } 
				} else if (oldy > y) {
					double move = (oldy - y) / 10;
                    if (move == 1) { // we can reuse 4 options subscr
						resubscribe(new TelAvivOption[] {	new TelAvivOption(yM10CName, y - 10),
															new TelAvivOption(yM10PName, y - 10)},
									market);
                    } else if (move == 2) { // we can reuse 2 options subscr
						resubscribe(new TelAvivOption[] {	new TelAvivOption(yM10CName, y - 10),
															new TelAvivOption(yM10PName, y - 10),
															new TelAvivOption(yCName, y),
															new TelAvivOption(yPName, y)},
									market);
                    } else { // we can't reuse a thing
						resubscribe(new TelAvivOption[] {	new TelAvivOption(yM10CName, y - 10),
															new TelAvivOption(yM10PName, y - 10),
															new TelAvivOption(yCName, y),
															new TelAvivOption(yPName, y),
															new TelAvivOption(yP10CName, y + 10),
															new TelAvivOption(yP10PName, y + 10)},
									market);
                    }
				} /*else if (options.get(2).getStrike() == y) {
                    // we can reuse all 6 options subscr
                }*/
			}
			calcLevel(market);
		}
	}

	private void handleSecondarySubscriptionUpdate(	String dataSubscription, Map<String, Object> fields, Market market,
													List<Opportunity> opportunities, UpdateResult result) {
		TelAvivOption option = null;
		for (TelAvivOption o : options) {
			if (o.getName().equals(dataSubscription)) {
				option = o;
				break;
			}
		}
		Double fieldOne = (Double) fields.get(formulaConfig.getSecondarySubscriptionField1());
		Double fieldTwo = (Double) fields.get(formulaConfig.getSecondarySubscriptionField2());
		Double fieldThree = (Double) fields.get(formulaConfig.getSecondarySubscriptionField3());
		if (fieldOne != null && fieldOne > 0) {
			option.setField1(fieldOne);
		}
		if (fieldTwo != null && fieldTwo > 0) {
			option.setField2(fieldTwo);
		}
		if (fieldThree != null && fieldThree > 0) {
			option.setField3(fieldThree);
		}
		option.setSnapshotReceived(true); // the first update is snapshot

		if (log.isTraceEnabled()) {
			log.trace("TRADINGDATA_"+ formulaConfig.getMainSubscriptionName() + " - " + option.getName() + " "
						+ formulaConfig.getSecondarySubscriptionField3() + ": " + option.getField3() + " "
						+ formulaConfig.getSecondarySubscriptionField1() + ": " + option.getField1() + " "
						+ formulaConfig.getSecondarySubscriptionField2() + ": " + option.getField2());
		}
		calcLevel(market);		
	}

	private void calcLevel(Market market) {
		if (canCalcLevel()) {
			log.trace("Can calc level");
			snapshotReceived = true;
			double level = calcLevel();
			market.getState().setLastCalcResult(level);
			market.getState().setLastFormulaResult(level);
		} else {
			log.trace("Don't calc level.");
		}
	}

	@Override
	public boolean isSnapshotReceived() {
		return snapshotReceived;
	}

	@Override
	public void resetInTheEndOfTheDay() {
		field1 = null;
		nextMonth = null;
		snapshotReceived = false;
		options.clear();
		formulaConfig.setSubscriptionConfig(formulaConfig.cloneSubscriptionConfig(formulaConfig.getDefaultSubscriptionConfig()));
	}

	@Override
	public Map<String, BigDecimal> getQuoteFields() {
		Map<String, BigDecimal> q = new HashMap<>();
    	if (null != field1) {
    		q.put(LevelService.QUOTE_FIELDS_LAST, field1);
    	}
    	return q;
	}

	@Override
	public FormulaConfig getFormulaConfig() {
		return formulaConfig;
	}

	private void resubscribe(TelAvivOption[] newOptions, Market market) {
		if (log.isTraceEnabled()) {
            log.trace("before options size = " + options.size() + " newOptions length = " + newOptions.length);
        }
		boolean up = false;
		if (newOptions.length > 0 && options.size() > 0) {
            up = newOptions[0].getStrike() > options.get(0).getStrike();
        }

		TelAvivOption o = null;
		Map<String, Map<String, String>> subscriptionConfigToUnsubscribe = new HashMap<>();
		Map<String, Map<String, String>> subscriptionConfigToSubscribe = new HashMap<>();
		for (int i = 0; i < newOptions.length && options.size() > 0; i++) {
			o = options.get(up ? 0 : options.size() - 1);
			addToSubscriptionConfig(subscriptionConfigToUnsubscribe, o);
			options.remove(o);
		}

		for (int i = 0; i < newOptions.length; i++) {
            o = newOptions[i];
            addToSubscriptionConfig(subscriptionConfigToSubscribe, o);
            if (up) {
                options.add(o);
            } else {
                options.add(i, o);
            }
        }
        if (log.isTraceEnabled()) {
            log.trace("after options size = " + options.size() + " newOptions length = " + newOptions.length);
        }
        market.unsubscribeData(LevelService.getInstance(), subscriptionConfigToUnsubscribe);
        market.subscribeData(LevelService.getInstance(), subscriptionConfigToSubscribe);
        subscriptionConfigElements.removeAll(subscriptionConfigToUnsubscribe.keySet());
        subscriptionConfigElements.addAll(subscriptionConfigToSubscribe.keySet());
	}

	private void addToSubscriptionConfig(Map<String, Map<String, String>> subscriptionConfigToUnsubscribe, TelAvivOption o) {
		Map<String, String> map = new HashMap<>();
		map.put(FormulaConfig.SUBSCRIPTION_CONFIG_IDENTIFIER, o.getName());
		subscriptionConfigToUnsubscribe.put(o.getName(), map);
	}

	/**
     * Check if all options have received snapshots and level can be calculated.
     * Call this metod only when you have lock on the options.
     *
     * @return <code>true</code> if level can be claculated else <code>false</code>.
     */
    private boolean canCalcLevel() {
        if (options.size() == 0) { // if no options you can't calc
            log.trace("options.size() = 0");
            return false;
        }
        for (TelAvivOption o : options) {
            if (!o.isSnapshotReceived()) {
                log.trace("option " + o.getName() + " no snapshot");
                return false; // if options have no value you can't calc
            }
        }
        return true;
    }

    /**
     * Calculate the level in to the <code>lastFormulaResult</code>.
     * Call this metod only when you have lock on the options.
     */
    private double calcLevel() {
        BigDecimal z = BigDecimalHelper.ZERO;
        TelAvivOption co = null;
        TelAvivOption po = null;
        BigDecimal c = null;
        BigDecimal p = null;
        int oppsUsed = 0;
        for (int i = 0; i < 3; i++) {
            co = options.get(i * 2);
            po = options.get(i * 2 + 1);
            if (null == co || null == co.getField3() || co.getField3() == 0 ||
                    null == co.getField1() || co.getField1() == 0 ||
                    null == co.getField2() || co.getField2() == 0 ||
                    co.getField1() - co.getField2() > formulaConfig.getOptionIgnoreAskBidSpread() ||
                    null == po || null == po.getField3() || po.getField3() == 0 ||
                    null == po.getField1() || po.getField1() == 0 ||
                    null == po.getField2() || po.getField2() == 0 ||
                    po.getField1() - po.getField2() > formulaConfig.getOptionIgnoreAskBidSpread()) {
                continue;
            }
            oppsUsed++;
            c =
                    new BigDecimal(co.getField3().toString())
                    .add(new BigDecimal(co.getField1().toString()))
                    .add(new BigDecimal(co.getField2().toString()))
                    .divide(BigDecimalHelper.THREE, BigDecimalHelper.BIG_DECIMAL_CALC_SCALE, RoundingMode.HALF_UP);
            p =
                    new BigDecimal(po.getField3().toString())
                    .add(new BigDecimal(po.getField1().toString()))
                    .add(new BigDecimal(po.getField2().toString()))
                    .divide(BigDecimalHelper.THREE, BigDecimalHelper.BIG_DECIMAL_CALC_SCALE, RoundingMode.HALF_UP);
            z =
                    z
                    .add(new BigDecimal(co.getStrike()))
                    .add(c.subtract(p).divide(new BigDecimal(100)));
        }
        if (oppsUsed == 0) {
            return field1.doubleValue();
        }
        z = z.divide(new BigDecimal(oppsUsed), BigDecimalHelper.BIG_DECIMAL_CALC_SCALE, RoundingMode.HALF_UP);
        //level without parameter
        BigDecimal k = z.multiply(BigDecimalHelper.TA_WEIGHT_OPTIONS)
		   			    .add(field1.multiply(BigDecimalHelper.TA_WEIGHT_MARKET));
		//level with parameter
        BigDecimal traderParameter = new BigDecimal(LevelService.getTa25Parameter());
        log.trace("TA25 trader parameter: " + traderParameter);
        double crrLevel = k.add(traderParameter).doubleValue();
        return crrLevel;
    }

	/**
	 * Prepare the 5 chars for the option name that represents the strike. If needed pad with 0s on the left.
	 *
	 * @param y
	 * @return
	 */
	private String getStrike(long y) {
		return String.format("%1$05d", y);
	}

	/**
	 * Get the CALL options letter for this/next month. The G is for July, Aug will be H, Sep will be I... January Call start with "A".
	 *
	 * @param nextMonth <code>true</code> if to return the next month letter or <code>false</code> for this month letter
	 * @return The current letter for the call option name.
	 */
	private char getCallLetter(boolean nextMonth) {
		return getMonthLetter('A', nextMonth);
	}

	/**
	 * Get the PUT options letter for this/next month. The S is for Jul Aug will be T... January Putt start with "M".
	 *
	 * @param nextMonth <code>true</code> if to return the next month letter or <code>false</code> for this month letter
	 * @return The current letter for the put option name.
	 */
	private char getPutLetter(boolean nextMonth) {
		return getMonthLetter('M', nextMonth);
	}

	/**
	 * Helper method for the call/put month letter methods.
	 *
	 * @param startLetter the start letter ('A' or 'M')
	 * @param nextMonth <code>true</code> if to return the next month letter or <code>false</code> for this month letter
	 * @return The requested month letter.
	 */
	private char getMonthLetter(char startLetter, boolean nextMonth) {
		Calendar calendar = Calendar.getInstance();
		if (nextMonth) {
			calendar.add(Calendar.MONTH, 1);
		}
		return (char) (startLetter + calendar.get(Calendar.MONTH)); // NOTE: m is between 0 and 11 as Calendar months are 0 to 11
	}

	private class TelAvivOption {

		private String name;
		private long strike;
		private Double field1;
		private Double field2;
		private Double field3;
		private boolean snapshotReceived;

		public TelAvivOption(String name, long strike) {
			this.name = name;
			this.strike = strike;
		}

		public String getName() {
			return name;
		}

		public long getStrike() {
			return strike;
		}

		public Double getField3() {
			return field3;
		}

		public void setField3(Double field3) {
			this.field3 = field3;
		}

		public Double getField1() {
			return field1;
		}

		public void setField1(Double field1) {
			this.field1 = field1;
		}

		public Double getField2() {
			return field2;
		}

		public void setField2(Double field2) {
			this.field2 = field2;
		}

		public boolean isSnapshotReceived() {
			return snapshotReceived;
		}

		public void setSnapshotReceived(boolean snapshotReceived) {
			this.snapshotReceived = snapshotReceived;
		}
	}
}