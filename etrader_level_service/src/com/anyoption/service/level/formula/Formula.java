/**
 *
 */
package com.anyoption.service.level.formula;

import java.util.List;

import com.anyoption.service.level.market.Market.BackendCommand;

/**
 * @author pavelhe
 *
 */
public abstract class Formula implements FormulaIfc {

	protected String lastTradeTime;
	protected FormulaRole role;
	protected List<String> subscriptionConfigElements;

	public boolean needUpdate(String dataSubscription) {
		if (subscriptionConfigElements == null) {
			subscriptionConfigElements = getFormulaConfig().collectSubscriptionConfigElements();
		}
		for (String key : subscriptionConfigElements) {
			if (dataSubscription.equals(key)) {
				return true;
			}
		}
		return false;
	}

	public void resetSubscriptionConfigElements() {
		subscriptionConfigElements = null;
	}

	public abstract FormulaConfig getFormulaConfig();

	public FormulaRole getRole() {
		return role;
	}

	public void setRole(FormulaRole role) {
		this.role = role;
	}

	public String getLastTradeTime() {
		return lastTradeTime;
	}

	public void setLastTradeTime(String lastTradeTime) {
		this.lastTradeTime = lastTradeTime;
	}

	@Override
	public void handleBackendCommand(BackendCommand command, String commandParams) {
		// This should not be used
	}
}