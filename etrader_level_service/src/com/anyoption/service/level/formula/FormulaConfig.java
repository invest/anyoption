/**
 * 
 */
package com.anyoption.service.level.formula;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * @author pavelhe
 */
public class FormulaConfig {

	public static final String SUBSCRIPTION_CONFIG_IDENTIFIER = "identifier";
	public static final String SUBSCRIPTION_CONFIG_SWITCHTOFUTURE = "switchToFuture";
	public static final String SUBSCRIPTION_CONFIG_SWITCHTOFUTURE_PREFIX_LENGTH = "switchToFuturePrefixLength";
	private Map<String, Map<String, String>> subscriptionConfig;
	private Map<String, Map<String, String>> defaultSubscriptionConfig;
	private static final Logger log = Logger.getLogger(FormulaConfig.class);

	public List<String> collectSubscriptionConfigElements() {
//		List<String> elements = new ArrayList<String>();
//		for (String key : subscriptionConfig.keySet()) {
//			String element;
//			if (subscriptionConfig.get(key).get(SUBSCRIPTION_CONFIG_SWITCHTOFUTURE) != null
//					&& Boolean.valueOf(subscriptionConfig.get(key).get(SUBSCRIPTION_CONFIG_SWITCHTOFUTURE))) {
//				element = key.substring(0,
//										Integer.valueOf(subscriptionConfig.get(key).get(SUBSCRIPTION_CONFIG_SWITCHTOFUTURE_PREFIX_LENGTH)));
//			} else {
//				element = key;
//			}
//			elements.add(element);
//		}
//		return elements;
		log.info("collectSubscriptionConfigElements(): " + subscriptionConfig.keySet());
		return new ArrayList<>(subscriptionConfig.keySet());
	}

	public Map<String, Map<String, String>> cloneSubscriptionConfig(Map<String, Map<String, String>> subscriptionConfig) {
		Map<String, Map<String, String>> newConfig = new HashMap<>();
		for (String key : subscriptionConfig.keySet()) {
			newConfig.put(key, new HashMap<>(subscriptionConfig.get(key)));
		}
		return newConfig;
	}

	public Map<String, Map<String, String>> getSubscriptionConfig() {
		return subscriptionConfig;
	}

	public void setSubscriptionConfig(Map<String, Map<String, String>> subscriptionConfig) {
		this.subscriptionConfig = subscriptionConfig;
	}

	public Map<String, Map<String, String>> getDefaultSubscriptionConfig() {
		return defaultSubscriptionConfig;
	}

	public void setDefaultSubscriptionConfig(Map<String, Map<String, String>> defaultSubscriptionConfig) {
		this.defaultSubscriptionConfig = defaultSubscriptionConfig;
	}
}