package com.anyoption.service.level.formula;

import il.co.etrader.service.level.BigDecimalHelper;
import il.co.etrader.service.level.LevelService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.service.level.Utils;
import com.anyoption.service.level.market.Market;
import com.anyoption.service.level.market.UpdateResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author EyalG
 * this class is used for average of 5 last update minus the minimum and the maximum update, we save each update in list as (filed1 + filed2 + filed3) / 3  
 */
public class Last5UpdatesAvgOf3Formula extends Formula {

	private static final Logger log = Logger.getLogger(Last5UpdatesAvgOf3Formula.class);
	private BigDecimal field1;
	private BigDecimal field2;
	private BigDecimal field3;
	private Last5UpdatesAvgOf3FormulaConfig formulaConfig;
	private boolean snapshotReceived;
	
	protected ArrayList<Double> lastFiveQuotes;

	public Last5UpdatesAvgOf3Formula(String formulaParams, FormulaRole role) {
		super();
		Gson gson = new GsonBuilder().serializeNulls().create();
		formulaConfig = gson.fromJson(formulaParams, Last5UpdatesAvgOf3FormulaConfig.class);
		formulaConfig.setDefaultSubscriptionConfig(formulaConfig.cloneSubscriptionConfig(formulaConfig.getSubscriptionConfig()));
		this.role = role;
		lastFiveQuotes = new ArrayList<Double>();
	}

	@Override
	public void collectDataSubscription(Map<String, Map<String, String>> subscriptionConfigs) {
		subscriptionConfigs.putAll(formulaConfig.getSubscriptionConfig());
	}

	@Override
	public void dataUpdate(String dataSubscription, Map<String, Object> fields, Market market, List<Opportunity> opportunities,
							UpdateResult result) {
		Double fieldOne = (Double) fields.get(formulaConfig.getField1Name());
		Double fieldTwo = (Double) fields.get(formulaConfig.getField2Name());
		Double trdprc1 = (Double) fields.get(formulaConfig.getLastTradePriceField());
		String tradeTime = (String) fields.get(formulaConfig.getLastTradeTimeField());
		if ((null == trdprc1 && null == fieldOne && null == fieldTwo && null == tradeTime) ||
                (null != trdprc1 && trdprc1 == 0) ||
                (null != fieldOne && fieldOne == 0) ||
                (null != fieldTwo && fieldTwo == 0)) {
			return;
		}
		market.setLastUpdateReceiveTime(System.currentTimeMillis());
		
		if (null != fieldOne) {
			this.field1 = new BigDecimal(fieldOne.toString());
        }
        if (null != fieldTwo) {
        	this.field2 = new BigDecimal(fieldTwo.toString());
        }
        if (null != trdprc1) {
        	this.field3 = new BigDecimal(trdprc1.toString());
        }

        if (null != tradeTime && !tradeTime.trim().equals("")) {
        	this.lastTradeTime = tradeTime;
        }
        if (null != field1 && null != field3 && null != field2) {
        	snapshotReceived = true;

            double crrRealLevel = field3.add(field1).add(field2).divide(BigDecimalHelper.THREE, BigDecimalHelper.BIG_DECIMAL_CALC_SCALE, RoundingMode.HALF_UP).doubleValue();

            lastFiveQuotes.add(crrRealLevel);
            if (lastFiveQuotes.size() > 5) {
                lastFiveQuotes.remove(0);
            }
            double min = Double.MAX_VALUE;
            double max = Double.MIN_VALUE;
            double sum = 0;
            double crr;
            for (int i = 0; i < lastFiveQuotes.size(); i++) {
                crr = lastFiveQuotes.get(i);
                if (crr < min) {
                    min = crr;
                }
                if (crr > max) {
                    max = crr;
                }
                sum += crr;
            }

            double avg = (sum - (lastFiveQuotes.size() > 3 ? min : 0) - (lastFiveQuotes.size() > 4 ? max : 0)) / (lastFiveQuotes.size() == 5 || lastFiveQuotes.size() == 4 ? 3 : lastFiveQuotes.size());

            log.trace("avg: " + avg + "; crrRealLevel: " + crrRealLevel + "; lastFiveQuotes: " + lastFiveQuotes + "; role: " + role);

            if (role == FormulaRole.CURRENT_LEVEL) {
				double crrLevel = Utils.formulaRandomChange(market.getState(), avg, market.getRandomCeiling(), market.getRandomFloor());
				market.getState().setLastCalcResult(avg);
				market.getState().setLastFormulaResult(crrLevel);
			} else if (role == FormulaRole.REAL_LEVEL) {
				market.getState().setLastRealLevel(avg);
				if (market.hasOpenedOpportunities() && !market.hasOpenedHourly()) {
	            	Opportunity o = market.getOpenedOppByScheduled(Opportunity.SCHEDULED_DAYLY);
	    			if (null != o && (o.getState() == Opportunity.STATE_CLOSING_1_MIN || o.getState() == Opportunity.STATE_CLOSING)) {
	                    if (log.isTraceEnabled()) {
	                        log.trace("Replace real level with " + formulaConfig.getLastTradePriceField());
	                    }
	                    market.getState().setLastRealLevel(field3.doubleValue());
	                }
	            }					
			} else { // (role == FormulaRole.DAY_CLOSING || role == FormulaRole.HOUR_CLOSING)
				log.warn("Last5Updates2Fields formula configured as closing level. NOT SUPPORTED!!!");
			}
        }	
	}

	@Override
	public FormulaConfig getFormulaConfig() {
		return formulaConfig;
	}

	@Override
	public boolean isSnapshotReceived() {
		return snapshotReceived;
	}

	@Override
	public void resetInTheEndOfTheDay() {
		field1 = null;
		field2 = null;
		field3 = null;
		lastFiveQuotes.clear();
		snapshotReceived = false;
		lastTradeTime = null;
		formulaConfig.setSubscriptionConfig(formulaConfig.cloneSubscriptionConfig(formulaConfig.getDefaultSubscriptionConfig()));
	}
	
	@Override
	public Map<String, BigDecimal> getQuoteFields() {
		// Here we assume that we are with Reuters as feed and there the ASK and BID fields are named ASK and BID
		// If the above is not ture we will need a more sophisticated implementation
		Map<String, BigDecimal> q = new HashMap<String, BigDecimal>();
		q.put(formulaConfig.getField1Name(), field1);
		q.put(formulaConfig.getField2Name(), field2);
		q.put(LevelService.QUOTE_FIELDS_LAST, field3);
		return q;
	}
}