package com.anyoption.service.level.formula;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.MarketDynamicsQuoteParams;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.service.level.market.Market;
import com.anyoption.service.level.market.UpdateResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Implements the new Dynamics formula. For reference see "CORE-4533 Dynamics New Formula".
 * There are attached documents with detailed description of the model and reference implementation in Excel.
 * 
 * @author Anton Antonov
 */
public abstract class DynamicsFormula extends Formula {
	private static final Logger log = Logger.getLogger(DynamicsFormula.class);
	
	enum SpreadSide {
		ASK,
		BID
	}
	
	protected DynamicsFormulaConfig formulaConfig;
	Formula internalFormula;
	
	protected long opportunityLengthInMin;
	LinkedList<Observation> observations;

	public DynamicsFormula(String formulaParams, FormulaRole role) {
		super();
		Gson gson = new GsonBuilder().serializeNulls().create();
		formulaConfig = gson.fromJson(formulaParams, DynamicsFormulaConfig.class);
		String internalConfig = formulaParams.substring(formulaParams.indexOf("{", 1), formulaParams.length() - 1);
		this.role = role;
		try {
			internalFormula = (Formula) Class	.forName(formulaConfig.internalFormulaClassName)
												.getConstructor(String.class, FormulaRole.class)
												.newInstance(internalConfig, role);
		} catch (Exception e) {
			log.error("Can't create internal formula: " + formulaConfig, e);
		}
		
		opportunityLengthInMin = 0;
		observations = new LinkedList<>();
	}

	@Override
	public void collectDataSubscription(Map<String, Map<String, String>> subscriptionConfigs) {
		internalFormula.collectDataSubscription(subscriptionConfigs);
	}

	@Override
	public void dataUpdate(String dataSubscription, Map<String, Object> fields, Market market, List<Opportunity> opportunities, UpdateResult result) {
		internalFormula.dataUpdate(dataSubscription, fields, market, opportunities, result);
		observe(market.getOppsCount() > 0 ? market.getOpp(0) : null, System.currentTimeMillis(), getInternalFormulaCurrentLevel(market));
	}
	
	
	
	public DynamicsQuote getOpportunityQuote(Market market, Opportunity o) {
		return getOpportunityQuote(market, o, System.currentTimeMillis());
	}
	
	public DynamicsQuote getOpportunityQuote(Market market, Opportunity o, long time) {
		DynamicsQuote dq = new DynamicsQuote();
		if (isSnapshotReceived() && o.isInOppenedState()) {
			double spot = getInternalFormulaCurrentLevel(market);
			double strike = o.getCurrentLevel();
		
			// volatility
			long timeToExpiry = o.getTimeEstClosing().getTime() - time;
			long n = 1 + timeToExpiry / 60000;
			
			if (n > opportunityLengthInMin) {
				log.warn("Getting quote for " + o + " n: " + n + " opportunityLengthInMin: " + opportunityLengthInMin);
			}
			
			double sigma = vol(n);
			double sigmaStar = volSkew(n, strike);
		
			// row price
			double t = timeToExpiry / 60000 + ((double) (timeToExpiry % 60000 / 1000)) / 60D;
			double priceAtM = edC(spot, strike, t, sigma);
			double priceMrkt = edC(spot, strike, t, sigmaStar);
		
			// price with spread
			MarketDynamicsQuoteParams quoteParams = (MarketDynamicsQuoteParams) o.getQuoteParamsObject();
			double bid = spreadPrice(priceAtM, priceMrkt, SpreadSide.BID, quoteParams.getSp(), quoteParams.getSpMax());
			double ask = spreadPrice(priceAtM, priceMrkt, SpreadSide.ASK, quoteParams.getSp(), quoteParams.getSpMax());
		
			// final price with skew and spread
			dq.setBid(skewPrice(bid, ask, o.getCalls(), o.getPuts(), quoteParams.gettU(), quoteParams.gettD(), quoteParams.getAmountU(), quoteParams.getAmountD(), SpreadSide.BID));
			dq.setAsk(skewPrice(bid, ask, o.getCalls(), o.getPuts(), quoteParams.gettU(), quoteParams.gettD(), quoteParams.getAmountU(), quoteParams.getAmountD(), SpreadSide.ASK));
			
			// We want to send the number in %
			dq.setBid(dq.getBid() * 100);
			dq.setAsk(dq.getAsk() * 100);
			
			// Add trader parameter
			dq.setBid(dq.getBid() + quoteParams.getT());
			dq.setAsk(dq.getAsk() + quoteParams.getT());
			
			String ls = System.lineSeparator();
			log.trace(ls + "S: " + spot + ls
					+ "K: " + strike + ls
					+ "n: " + n + ls
					+ "sigma: " + sigma + ls
					+ "sigmaStar: " + sigmaStar + ls
					+ "T: " + t + ls
					+ "priceAtM: " + priceAtM + ls
					+ "priceMrkt: " + priceMrkt + ls
					+ "spread bid: " + bid + ls
					+ "spread ask: " + ask + ls
					+ "trader T: " + quoteParams.getT() + ls
					+ "skew bid + TT: " + dq.getBid() + ls
					+ "skew ask + TT: " + dq.getAsk() + ls);
		}		
        return dq;
	}

    protected abstract double getInternalFormulaCurrentLevel(Market market);

	@Override
	public FormulaConfig getFormulaConfig() {
		return internalFormula.getFormulaConfig();
	}

	@Override
	public boolean isSnapshotReceived() {
		return internalFormula.isSnapshotReceived()
				&& opportunityLengthInMin > 0
				&& observations.size() == opportunityLengthInMin;
	}

	@Override
	public void resetInTheEndOfTheDay() {
		internalFormula.resetInTheEndOfTheDay();
		opportunityLengthInMin = 0;
		observations.clear();
	}
	
	@Override
	public Map<String, BigDecimal> getQuoteFields() {
		return internalFormula.getQuoteFields();
	}
	
	public double vol(long n) {
		double sum1 = 0D;
		double sum2 = 0D;
		for (int i = 0; i < n; i++) {
			double a = observations.get(i).ln;
			sum1 = sum1 + a * a;
			sum2 = sum2 + a;
		}

		return Math.sqrt((1D / (n - 1D)) * sum1 - (1D / (n * (n - 1D))) * Math.pow(sum2, 2));
	}
	
	public double volSkew(long n, double strike) {
		double sum1 = 0D;
		double sum2 = 0D;
		for (int i = 0; i < n; i++) {
			double a = observations.get(i).ln;
			if (i == 0) {
				a = Math.log(strike / observations.get(1).level);
			}
			sum1 = sum1 + a * a;
			sum2 = sum2 + a;
		}

		return Math.sqrt((1D / (n - 1D)) * sum1 - (1D / (n * (n - 1D))) * Math.pow(sum2, 2));
	}
	
	public double edC(double spot, double strike, double t, double vol) {
		double d1 = (Math.log(spot / strike) + 0.5 * Math.pow(vol, 2) * t) / (vol * Math.sqrt(t));
		double d2 = d1 - vol * Math.sqrt(t);
		return new NormalDistribution().cumulativeProbability(d2);
	}
	
	public double spreadPrice(double priceAtM, double priceMrkt, SpreadSide side, double sp, double spMax) {
		double s = Math.abs(priceAtM - priceMrkt);
		double p = 0D;
		if (s >= spMax && priceAtM >= 0.5) {
		    if (side == SpreadSide.ASK) {
		        p = 0.001 * Math.ceil(1000 * priceAtM);
		    } else {
		        p = 0.001 * Math.ceil(1000 * priceAtM) - spMax;
			}
		} else if (priceAtM >= 0.5 && (s <= spMax) && (s >= sp)) {
		    if (side == SpreadSide.ASK) {
		        p = 0.001 * Math.ceil(1000 * priceAtM);
		    } else {
		        p = 0.001 * Math.floor(1000 * priceMrkt);
		    }
		} else if (priceAtM >= 0.5 && (s <= sp)) {
		    double m = (priceAtM + priceMrkt) / 2;
		    if (side == SpreadSide.ASK) {
		        p = m + sp / 2;
		    } else {
		        p = m - sp / 2;
		    }
		} else if ((s >= spMax) && priceAtM < 0.5) {
		    if (side == SpreadSide.BID) {
		        p = 0.001 * Math.floor(1000 * priceAtM);
		    } else {
		        p = 0.001 * Math.floor(1000 * priceAtM) + spMax;
		    }
		} else if (priceAtM < 0.5 && (s <= spMax) && (s >= sp)) {
			if (side == SpreadSide.BID) {
		        p = 0.001 * Math.floor(1000 * priceAtM);
			} else {
		        p = 0.001 * Math.ceil(1000 * priceMrkt);
			}
		} else if (priceAtM < 0.5 && (s <= sp)) {
		    double m = (priceAtM + priceMrkt) / 2;
		    if (side == SpreadSide.BID) {
		        p = m - sp / 2;
		    } else {
		        p = m + sp / 2;
		    }
		}
		return p;
	}
	
	public double skewPrice(double bid, double ask, double up, double down, double tUp, double tDown, double amountU, double amountD, SpreadSide side) {
		double m = 0.5 * (bid + ask);
		double sk = 0D;

		if (up == 0 && down == 0) {
			if (side == SpreadSide.ASK) {
				sk = ask;
			} else {
				sk = bid;
			}
		}

		if (up == 0 && down != 0) {
			if (side == SpreadSide.ASK) {
				if (down >= amountD) {
					sk = m;
				} else {
					sk = ((m - ask) / amountD) * down + ask;
				}
			} else {
				if (down >= amountD) {
					sk = m - (ask - bid);
				} else {
					sk = ((m - ask) / amountD) * down + ask - (ask - bid);
				}
				if (sk < 0) {
					sk = 0;
				}
			}
		} else if (up != 0 && down == 0) {
			if (side == SpreadSide.BID) {
				if (up >= amountU) {
					sk = m;
				} else {
					sk = ((m - bid) / amountU) * up + bid;
				}
			} else {
				if (up >= amountU) {
					sk = m + (ask - bid);
				} else {
					sk = ((m - bid) / amountU) * up + bid + (ask - bid);
				}
				if (sk > 1) {
					sk = 1;
				}
			}
		} else if (up != 0 && down != 0) {
			double c = up / down;
			if (c >= tUp) {
				if (side == SpreadSide.BID) {
					sk = m;
				} else {
					sk = m + ask - bid;
					if (sk > 1) {
						sk = 1;
					}
				}
			} else if (c <= tDown) {
				if (side == SpreadSide.ASK) {
					sk = m;
				} else {
					sk = m - (ask - bid);
					if (sk < 0) {
						sk = 0;
					}
				}
			} else if (c >= 1 && c < tUp) {
				if (side == SpreadSide.BID) {
					sk = ((m - bid) / (tUp - 1)) * c + bid - (m - bid) / (tUp - 1);
				} else {
					sk = ((m - bid) / (tUp - 1)) * c + bid - (m - bid) / (tUp - 1) + (ask - bid);
					if (sk > 1) {
						sk = 1;
					}
				}
			} else if (c < 1 && c > tDown) {
				if (side == SpreadSide.ASK) {
					sk = ((m - ask) / (tDown - 1)) * c + ask - (m - ask) / (tDown - 1);
				} else {
					sk = ((m - ask) / (tDown - 1)) * c + ask - (m - ask) / (tDown - 1) - (ask - bid);
					if (sk < 0) {
						sk = 0;
					}
				}
			}
		}
		return sk;
	}
	
	private long getOpportunityLengthInMin(Opportunity opportunity) {
		if (opportunityLengthInMin == 0) {
			if (opportunity != null) {
				if (opportunity.getTimeFirstInvest() != null && opportunity.getTimeEstClosing() != null) {
					opportunityLengthInMin = (opportunity.getTimeEstClosing().getTime() - opportunity.getTimeFirstInvest().getTime()) / 60000;
				}
			}
			if (opportunityLengthInMin == 0) {
				log.warn("Can't calc opp length in min. Use default 30.");
				opportunityLengthInMin = 30;
			}
		}
		return opportunityLengthInMin;
	}
	
	public void observe(Opportunity opportunity, long time, double level) {
		if (observations.size() == 0) {
			if (level > 0) {
				observations.add(new Observation(time / 60000 * 60000, level)); // round minute
				log.debug(observations);
			}
			return;
		}
		
		Observation o = observations.getFirst();
		if (time - o.time < 60000) {
			return;
		}
		
		do {
			o = new Observation(o.time + 60000, level, Math.log(level/o.level));
			observations.add(0, o);
		} while (time - o.time > 60000);
		
		
		while (observations.size() > getOpportunityLengthInMin(opportunity)) {
			observations.removeLast();
		}
		log.debug(observations);
	}
	
	class DynamicsFormulaConfig {
		private String internalFormulaClassName;
	}
	
	class Observation {
		long time;
		double level;
		double ln;
		
		public Observation(long time, double level) {
			this.time = time;
			this.level = level;
		}
		
		public Observation(long time, double level, double ln) {
			this(time, level);
			this.ln = ln;
		}
		
		public String toString() {
			return "(" + time + "," + level + ")";
		}
	}
}