package com.anyoption.service.level.formula;

/**
 * @author kiril.mutafchiev
 */
public class PresentDayIdentationFormulaConfig extends FormulaConfig {

	private String field1Name;
	private String lastTradeTimeField;
	private String specificValueName;
	private String presentDayFieldName;
	private String secondTimeFieldName;

	public String getField1Name() {
		return field1Name;
	}

	public void setField1Name(String field1Name) {
		this.field1Name = field1Name;
	}

	public String getLastTradeTimeField() {
		return lastTradeTimeField;
	}

	public void setLastTradeTimeField(String lastTradeTimeField) {
		this.lastTradeTimeField = lastTradeTimeField;
	}

	public String getSpecificValueName() {
		return specificValueName;
	}

	public void setSpecificValueName(String specificValueName) {
		this.specificValueName = specificValueName;
	}

	public String getPresentDayFieldName() {
		return presentDayFieldName;
	}

	public void setPresentDayFieldName(String presentDayFieldName) {
		this.presentDayFieldName = presentDayFieldName;
	}

	public String getSecondTimeFieldName() {
		return secondTimeFieldName;
	}

	public void setSecondTimeFieldName(String secondTimeFieldName) {
		this.secondTimeFieldName = secondTimeFieldName;
	}
}