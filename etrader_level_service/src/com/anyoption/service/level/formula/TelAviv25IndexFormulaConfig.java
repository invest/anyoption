package com.anyoption.service.level.formula;

/**
 * @author kiril.mutafchiev
 */
public class TelAviv25IndexFormulaConfig extends FormulaConfig {

	private String lastTradePriceField;
	private String lastTradeTimeField;
	private String mainSubscriptionName;
	private String secondarySubscriptionField1;
	private String secondarySubscriptionField2;
	private String secondarySubscriptionField3;
	private int optionIgnoreAskBidSpread;

	public String getMainSubscriptionName() {
		return mainSubscriptionName;
	}

	public void setMainSubscriptionName(String mainSubscriptionName) {
		this.mainSubscriptionName = mainSubscriptionName;
	}

	public String getSecondarySubscriptionField1() {
		return secondarySubscriptionField1;
	}

	public void setSecondarySubscriptionField1(String secondarySubscriptionField1) {
		this.secondarySubscriptionField1 = secondarySubscriptionField1;
	}

	public String getSecondarySubscriptionField2() {
		return secondarySubscriptionField2;
	}

	public void setSecondarySubscriptionField2(String secondarySubscriptionField2) {
		this.secondarySubscriptionField2 = secondarySubscriptionField2;
	}

	public String getSecondarySubscriptionField3() {
		return secondarySubscriptionField3;
	}

	public void setSecondarySubscriptionField3(String secondarySubscriptionField3) {
		this.secondarySubscriptionField3 = secondarySubscriptionField3;
	}

	public String getLastTradePriceField() {
		return lastTradePriceField;
	}

	public void setLastTradePriceField(String lastTradePriceField) {
		this.lastTradePriceField = lastTradePriceField;
	}

	public String getLastTradeTimeField() {
		return lastTradeTimeField;
	}

	public void setLastTradeTimeField(String lastTradeTimeField) {
		this.lastTradeTimeField = lastTradeTimeField;
	}

	public int getOptionIgnoreAskBidSpread() {
		return optionIgnoreAskBidSpread;
	}

	public void setOptionIgnoreAskBidSpread(int optionIgnoreAskBidSpread) {
		this.optionIgnoreAskBidSpread = optionIgnoreAskBidSpread;
	}
}