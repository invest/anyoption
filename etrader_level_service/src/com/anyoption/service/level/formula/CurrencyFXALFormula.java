package com.anyoption.service.level.formula;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.service.level.Utils;
import com.anyoption.service.level.market.Market;
import com.anyoption.service.level.market.MarketParams;
import com.anyoption.service.level.market.MarketState;
import com.anyoption.service.level.market.UpdateResult;
import com.anyoption.service.level.market.Market.BackendCommand;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author kirilim
 */
public class CurrencyFXALFormula extends Formula {

	private static final Logger log = Logger.getLogger(CurrencyFXALFormula.class);
	private Last5UpdatesAvgOf2Formula last5Updates;
	private Market last5UpdatesFakeMarket;
	private AvgOf2Formula avgOf2;
	private Market avgOf2FakeMarket;
	private CurrencyFXALFormulaConfig formulaConfig;

	public CurrencyFXALFormula(String formulaParams, FormulaRole role) {
		super();
		if (role != FormulaRole.CURRENT_LEVEL) {
			throw new IllegalArgumentException("This formula is only applicable for "+ FormulaRole.CURRENT_LEVEL + ". Given role is "
												+ role);
		}
		Gson gson = new GsonBuilder().serializeNulls().create();
		formulaConfig = gson.fromJson(formulaParams, CurrencyFXALFormulaConfig.class);
		last5Updates = new Last5UpdatesAvgOf2Formula(formulaConfig.getLast5UpdatesFormulaConfig(), role);
		int indx = 0;
		last5UpdatesFakeMarket = createFakeMarket("FakeMarket" + indx++);
		avgOf2 = new AvgOf2Formula(formulaConfig.getAvgOf2FormulaConfig(), role);
		avgOf2FakeMarket = createFakeMarket("FakeMarket" + indx++);
		this.role = role;
		Map<String, Map<String, String>> subscriptionConfig = new HashMap<>(last5Updates.getFormulaConfig().getSubscriptionConfig());
		subscriptionConfig.putAll(avgOf2.getFormulaConfig().getSubscriptionConfig());
		formulaConfig.setSubscriptionConfig(subscriptionConfig);
	}

	@Override
	public void collectDataSubscription(Map<String, Map<String, String>> subscriptionConfigs) {
		subscriptionConfigs.putAll(last5Updates.getFormulaConfig().getSubscriptionConfig());
		subscriptionConfigs.putAll(avgOf2.getFormulaConfig().getSubscriptionConfig());
	}

	@Override
	public void dataUpdate(	String dataSubscription, Map<String, Object> fields, Market market, List<Opportunity> opportunities,
							UpdateResult result) {
		// for now this formula is used only as CURRENT_LEVEL formula. See constructor call
		if (last5Updates.getFormulaConfig().getSubscriptionConfig().keySet().contains(dataSubscription)) {
			last5Updates.dataUpdate(dataSubscription, fields, last5UpdatesFakeMarket, opportunities, result);
		}
		if (avgOf2.getFormulaConfig().getSubscriptionConfig().keySet().contains(dataSubscription)) {
			avgOf2.dataUpdate(dataSubscription, fields, avgOf2FakeMarket, opportunities, result);
		}
		double crrRealLevel;
		if (formulaConfig.getFXALParameter() > 0 && isSnapshotReceived()) {
			crrRealLevel = ((1 - formulaConfig.getFXALParameter()) * last5UpdatesFakeMarket.getState().getLastCalcResult())
							+ (formulaConfig.getFXALParameter() * avgOf2FakeMarket.getState().getLastCalcResult());
		} else if (formulaConfig.getFXALParameter() == 0 && last5Updates.isSnapshotReceived()) {
			crrRealLevel = last5UpdatesFakeMarket.getState().getLastCalcResult();
		} else {
			return;
		}
		// for test
		log.trace("crrRealLevel: " + crrRealLevel + " role: " + role);
		double crrLevel = Utils.formulaRandomChange(market.getState(), crrRealLevel, market.getRandomCeiling(), market.getRandomFloor());
		market.getState().setLastCalcResult(crrRealLevel);
		market.getState().setLastFormulaResult(crrLevel);
	}

	private Market createFakeMarket(String name) {
		return new Market(name, new MarketState(), new MarketParams(), 0d, 0d);
	}

	@Override
	public boolean isSnapshotReceived() {
		if (formulaConfig.getFXALParameter() > 0) {
			return last5Updates.isSnapshotReceived() && avgOf2.isSnapshotReceived();
		} else if (formulaConfig.getFXALParameter() == 0) {
			return last5Updates.isSnapshotReceived();
		}
		return false;
	}

	@Override
	public void resetInTheEndOfTheDay() {
		last5Updates.resetInTheEndOfTheDay();
		avgOf2.resetInTheEndOfTheDay();
	}

	@Override
	public FormulaConfig getFormulaConfig() {
		return formulaConfig;
	}
	
	@Override
	public Map<String, BigDecimal> getQuoteFields() {
		return last5Updates.getQuoteFields();
	}

	@Override
	public void handleBackendCommand(BackendCommand command, String commandParams) {
		if (command == BackendCommand.FXAL_CHANGE) {
			String fXALParameter = commandParams.substring(commandParams.indexOf("FXAL=")+ 5,
															commandParams.indexOf(";", commandParams.indexOf("FXAL=") + 5));
			formulaConfig.setFXALParameter(Float.valueOf(fXALParameter));
		}
	}
}