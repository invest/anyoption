package com.anyoption.service.level.formula;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ReutersQuotes;
import com.anyoption.service.level.Utils;
import com.anyoption.service.level.market.Market;
import com.anyoption.service.level.market.UpdateResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import il.co.etrader.service.level.LevelService;

/**
 * @author kiril.mutafchiev
 */
public class PresentDayIdentationFormula extends Formula implements FormulaIfc {

	private static final Logger log = Logger.getLogger(PresentDayIdentationFormula.class);
//	private BigDecimal field1;
	private BigDecimal specificValue;
	private String presentDay;
	private String secondTimeField;
	private PresentDayIdentationFormulaConfig formulaConfig;
	private boolean snapshotReceived;

	public PresentDayIdentationFormula(String formulaParams, FormulaRole role) {
		super();
		Gson gson = new GsonBuilder().serializeNulls().create();
		formulaConfig = gson.fromJson(formulaParams, PresentDayIdentationFormulaConfig.class);
		formulaConfig.setDefaultSubscriptionConfig(formulaConfig.cloneSubscriptionConfig(formulaConfig.getSubscriptionConfig()));
		this.role = role;
	}

	@Override
	public void collectDataSubscription(Map<String, Map<String, String>> subscriptionConfigs) {
		subscriptionConfigs.putAll(formulaConfig.getSubscriptionConfig());
	}

	@Override
	public void dataUpdate(	String dataSubscription, Map<String, Object> fields, Market market, List<Opportunity> opportunities,
							UpdateResult result) {
//		Double fieldOne = (Double) fields.get(formulaConfig.getField1Name());
		Double specificValue = (Double) fields.get(formulaConfig.getSpecificValueName());
		String presentDay = (String) fields.get(formulaConfig.getPresentDayFieldName());
		if (presentDay == null || presentDay.trim().equals("")) {
			presentDay = this.presentDay;
		} else {
			this.presentDay = presentDay;
		}
		if (specificValue != null && specificValue > 0) {
//			this.field1 = new BigDecimal(fieldOne);			
			String timeAct = (String) fields.get(formulaConfig.getLastTradeTimeField());
			String secondTimeField = (String) fields.get(formulaConfig.getSecondTimeFieldName());
			if (timeAct == null || timeAct.trim().equals("")) {
				timeAct = lastTradeTime;
			} else {
				lastTradeTime = timeAct;
			}
			if (secondTimeField == null || secondTimeField.trim().equals("")) {
				secondTimeField = this.secondTimeField;
			} else {
				this.secondTimeField = secondTimeField;
			}
			Calendar timeActCal = Calendar.getInstance();
            Calendar secondTimeFieldCal = Calendar.getInstance();
            try {
	            timeActCal.setTime(market.getUpdateTime(timeAct, "GMT", null));
	            secondTimeFieldCal.setTime(market.getUpdateTime(secondTimeField, "GMT", null));
            } catch (ParseException e) {
            	log.warn("Cannot parse update time", e);
            }
            secondTimeFieldCal.add(Calendar.MINUTE, 10);
            if ((timeActCal.after(secondTimeFieldCal) && (presentDay != null && isTodayTime(presentDay + " 05:00:00", "GMT"))) || (presentDay != null && !isTodayTime(presentDay + " 05:00:00", "GMT"))) { //we set the time to 05:00:00 to be sure day time saveing will not have any effect
                log.debug("got spike in " + market.getMarketName());
                return;
            }

        	this.specificValue = new BigDecimal(specificValue);
        	snapshotReceived = true;
        	log.trace("specificValue: " + this.specificValue + "; role: " + role);
        	if (role == FormulaRole.DAY_CLOSING) {
        		Date updateTime = null;
				if (lastTradeTime != null) {
					try {
						updateTime = market.getUpdateTime(lastTradeTime, market.getMarketParams().getTimeZone(), market.getMarketParams().getTimePattern());
					} catch (Throwable t) {
						log.error("Can't process lastTradeTime.", t);
						updateTime = new Date(System.currentTimeMillis());
					}
				} else {
					log.error("Update without lastTradeTime!!!");
					updateTime = new Date(System.currentTimeMillis());
				}
				String closingLevelTxt = "formula:" + formulaConfig.getSpecificValueName() + ";" + formulaConfig.getSpecificValueName() + ":" + this.specificValue + ";closeLevelBeforeRound:" + this.specificValue;
				ReutersQuotes currentQuote = new ReutersQuotes(updateTime, this.specificValue.doubleValue());
				market.updateOpportunitiesClosingLevel(	updateTime, 0d, this.specificValue.doubleValue(), null, closingLevelTxt, null, currentQuote,
														opportunities, result);
        	}
		}
	}

	@Override
	public boolean isSnapshotReceived() {
		return snapshotReceived;
	}

	@Override
	public void resetInTheEndOfTheDay() {
//		field1 = null;
		snapshotReceived = false;
		lastTradeTime = null;
		specificValue = null;
		presentDay = null;
		secondTimeField = null;
		formulaConfig.setSubscriptionConfig(formulaConfig.cloneSubscriptionConfig(formulaConfig.getDefaultSubscriptionConfig()));
	}

	@Override
	public Map<String, BigDecimal> getQuoteFields() {
		// Here we assume that this formula is used only on markets that you quote the LAST field. There is no point to quote only ASK or
		// only BID
		Map<String, BigDecimal> q = new HashMap<String, BigDecimal>();
		q.put(LevelService.QUOTE_FIELDS_LAST, specificValue);
		return q;
	}

	@Override
	public FormulaConfig getFormulaConfig() {
		return formulaConfig;
	}

	private static boolean isTodayTime(String time, String timeZone) {
		try {
			SimpleDateFormat updateDateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm:ss", new Locale("en", "US"));
			updateDateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
			Date d = updateDateFormat.parse(time);
			if (log.isTraceEnabled()) {
				log.trace("isTodayTime - time: " + time + " timeZone: " + timeZone + " d: " + d);
			}
			return Utils.isToday(d, timeZone);
		} catch (ParseException pe) {
			log.error("Can't parse the date.", pe);
		}
		return false;
	}
}