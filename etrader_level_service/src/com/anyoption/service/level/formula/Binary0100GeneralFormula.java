//package com.anyoption.service.level.formula;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.service.level.market.Market;
//
//public class Binary0100GeneralFormula extends Binary0100Formula {
//	private static final Logger log = Logger.getLogger(Binary0100GeneralFormula.class);
//
//	public Binary0100GeneralFormula(String formulaParams, FormulaRole role) {
//		super(formulaParams, role);
//	}
//	
//	@Override
//    protected double getInternalFormulaCurrentLevel(Market market) {
//    	log.debug("base level = " + market.getState().getLastCalcResult()) ;
//    	return market.getState().getLastCalcResult();
//    }
//}