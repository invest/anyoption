//package com.anyoption.service.level.formula;
//
//import java.math.BigDecimal;
//import java.util.List;
//import java.util.Map;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.Opportunity;
//import com.anyoption.service.level.market.Market;
//import com.anyoption.service.level.market.UpdateResult;
//import com.google.gson.Gson;
//import com.google.gson.GsonBuilder;
//
//import il.co.etrader.util.CommonUtil;
//
//public abstract class Binary0100Formula extends Formula {
//	private static final Logger log = Logger.getLogger(Binary0100Formula.class);
//	
//	protected Binary0100FormulaConfig formulaConfig;
//	protected Formula internalFormula;
//
//	public Binary0100Formula(String formulaParams, FormulaRole role) {
//		super();
//		Gson gson = new GsonBuilder().serializeNulls().create();
//		formulaConfig = gson.fromJson(formulaParams, Binary0100FormulaConfig.class);
//		String internalConfig = formulaParams.substring(formulaParams.indexOf("{", 1), formulaParams.length() - 1);
//		this.role = role;
//		try {
//			internalFormula = (Formula) Class	.forName(formulaConfig.internalFormulaClassName)
//												.getConstructor(String.class, FormulaRole.class)
//												.newInstance(internalConfig, role);
//		} catch (Exception e) {
//			log.error("Can't create internal formula: " + formulaConfig, e);
//		}
//	}
//
//	@Override
//	public void collectDataSubscription(Map<String, Map<String, String>> subscriptionConfigs) {
//		internalFormula.collectDataSubscription(subscriptionConfigs);
//	}
//
//	@Override
//	public void dataUpdate(String dataSubscription, Map<String, Object> fields, Market market, List<Opportunity> opportunities, UpdateResult result) {
//		internalFormula.dataUpdate(dataSubscription, fields, market, opportunities, result);
//	}
//	
//	
//	public Binary0100Quote getOpportunityQuote(Market market, Opportunity o) {		
//    	log.debug("calculate opp qoute for opp id = " + o.getId() + " params = " + o.getBinaryZeroOneHundred().toString());
//        Binary0100Quote q = new Binary0100Quote();
//        double currentLevel = getInternalFormulaCurrentLevel(market);
//        if (currentLevel == 0) { //first update when loading opps and nothing in the option
//        	log.debug("got current Level " + currentLevel + " sending offer and bid = 0 ");
//        	q.setOffer(0d);
//            q.setBid(0d);
//            return q;
//        }
//        long timeToExpiry = (o.getTimeEstClosing().getTime() - System.currentTimeMillis()) / 1000;
//        double invDelta = CommonUtil.roundDouble(o.getContractsBought(), 2) -  CommonUtil.roundDouble(o.getContractsSold(), 2);
//        double promileDistance = Math.abs((currentLevel - o.getCurrentLevel()) / currentLevel) * 1000;
//        double x = 0;
//        double levelDiff = o.getCurrentLevel() / currentLevel;
//        if (0.99996 < levelDiff && levelDiff < 1.00004) {
//        	x = 50 + (invDelta / o.getBinaryZeroOneHundred().getZ())  + o.getBinaryZeroOneHundred().getT();
//        } else {
//	        if (o.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_ABOVE || o.getOpportunityTypeId() == Opportunity.TYPE_DYNAMICS) {
//	            log.debug("above type = "  + o.getOpportunityTypeId() + " diff = " + (currentLevel - o.getCurrentLevel()) / currentLevel);
//	            if (o.getCurrentLevel() - currentLevel < 0) {
//	                x = o.getBinaryZeroOneHundred().getFormulaResultPositive(promileDistance, invDelta, timeToExpiry);
//	            } else {
//	                x = o.getBinaryZeroOneHundred().getFormulaResultNegative(promileDistance, invDelta, timeToExpiry);
//	            }
//	        } else if (o.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_BELOW) {
//	            log.debug("below type = "  + o.getOpportunityTypeId() + " diff = " + (currentLevel - o.getCurrentLevel()) / currentLevel);
//	            if (o.getCurrentLevel() - currentLevel < 0) {
//	                x = o.getBinaryZeroOneHundred().getFormulaResultNegative(promileDistance, invDelta, timeToExpiry);
//	            } else {
//	                x = o.getBinaryZeroOneHundred().getFormulaResultPositive(promileDistance, invDelta, timeToExpiry);
//	            }
//	        }
//        }
//        q.setOffer(x + o.getBinaryZeroOneHundred().getQ() / 2);
//        q.setBid(x - o.getBinaryZeroOneHundred().getQ() / 2);
//        if (q.getOffer() > 100) {
//            q.setOffer(100);
//        } else if (q.getOffer() < 11) {
//            q.setOffer(11);
//        }
//
//        if (q.getBid() > 89) {
//            q.setBid(89);
//        } else if (q.getBid() < 0) {
//            q.setBid(0);
//        }
//        log.debug("currentLevel = " + currentLevel + " promileDistance = " + promileDistance + " x = " + x + " lastoffer = " + q.getOffer() + " last bid = " + q.getBid());
//        return q;
//	}
//
//    protected abstract double getInternalFormulaCurrentLevel(Market market);
//
//	@Override
//	public FormulaConfig getFormulaConfig() {
//		return internalFormula.getFormulaConfig();
//	}
//
//	@Override
//	public boolean isSnapshotReceived() {
//		return internalFormula.isSnapshotReceived();
//	}
//
//	@Override
//	public void resetInTheEndOfTheDay() {
//		internalFormula.resetInTheEndOfTheDay();
//	}
//	
//	@Override
//	public Map<String, BigDecimal> getQuoteFields() {
//		return internalFormula.getQuoteFields();
//	}
//	
//	class Binary0100FormulaConfig {
//		private String internalFormulaClassName;
////		private Map<String, String> formulaConfig;
//	}
//}