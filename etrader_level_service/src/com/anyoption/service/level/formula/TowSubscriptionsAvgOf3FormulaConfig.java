package com.anyoption.service.level.formula;

/**
 * @author EyalG
 */
public class TowSubscriptionsAvgOf3FormulaConfig extends FormulaConfig {

	private String firstSubscriptionName;
	private String firstSubscriptionField1;
	private String secondSubscriptionField1;
	private String secondSubscriptionField2;
	private String secondSubscriptionField3;
	private String lastTradeTimeField;
	private String traderParamName;

	public String getFirstSubscriptionName() {
		return firstSubscriptionName;
	}

	public void setFirstSubscriptionName(String firstSubscriptionName) {
		this.firstSubscriptionName = firstSubscriptionName;
	}

	public String getFirstSubscriptionField1() {
		return firstSubscriptionField1;
	}

	public void setFirstSubscriptionField1(String firstSubscriptionField1) {
		this.firstSubscriptionField1 = firstSubscriptionField1;
	}

	public String getSecondSubscriptionField1() {
		return secondSubscriptionField1;
	}

	public void setSecondSubscriptionField1(String secondSubscriptionField1) {
		this.secondSubscriptionField1 = secondSubscriptionField1;
	}

	public String getSecondSubscriptionField2() {
		return secondSubscriptionField2;
	}

	public void setSecondSubscriptionField2(String secondSubscriptionField2) {
		this.secondSubscriptionField2 = secondSubscriptionField2;
	}

	public String getSecondSubscriptionField3() {
		return secondSubscriptionField3;
	}

	public void setSecondSubscriptionField3(String secondSubscriptionField3) {
		this.secondSubscriptionField3 = secondSubscriptionField3;
	}

	public String getLastTradeTimeField() {
		return lastTradeTimeField;
	}

	public void setLastTradeTimeField(String lastTradeTimeField) {
		this.lastTradeTimeField = lastTradeTimeField;
	}

	public String getTraderParamName() {
		return traderParamName;
	}

	public void setTraderParamName(String traderParamName) {
		this.traderParamName = traderParamName;
	}
}