package com.anyoption.service.level.formula;

import com.anyoption.service.level.market.Market;

public class DynamicsGeneralFormula extends DynamicsFormula {
	public DynamicsGeneralFormula(String formulaParams, FormulaRole role) {
		super(formulaParams, role);
	}
	
	@Override
    protected double getInternalFormulaCurrentLevel(Market market) {
    	return market.getState().getLastCalcResult();
    }
}