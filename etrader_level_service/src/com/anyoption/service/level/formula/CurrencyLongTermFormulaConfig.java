package com.anyoption.service.level.formula;

public class CurrencyLongTermFormulaConfig extends FormulaConfig {
	private String interestSubscription1;
	private String interestSubscription2;
	private String interestAskField;
	private String interestBidField;
	private long daysConstant1;
	private long daysConstant2;
	private int spotConstant;
	private boolean divideC16ByC15;

	public long getDaysConstant1() {
		return daysConstant1;
	}

	public void setDaysConstant1(long daysConstant1) {
		this.daysConstant1 = daysConstant1;
	}

	public long getDaysConstant2() {
		return daysConstant2;
	}

	public void setDaysConstant2(long daysConstant2) {
		this.daysConstant2 = daysConstant2;
	}

	public String getInterestSubscription1() {
		return interestSubscription1;
	}

	public void setInterestSubscription1(String interestSubscription1) {
		this.interestSubscription1 = interestSubscription1;
	}

	public String getInterestSubscription2() {
		return interestSubscription2;
	}

	public void setInterestSubscription2(String interestSubscription2) {
		this.interestSubscription2 = interestSubscription2;
	}

	public String getInterestAskField() {
		return interestAskField;
	}

	public void setInterestAskField(String interestAskField) {
		this.interestAskField = interestAskField;
	}

	public String getInterestBidField() {
		return interestBidField;
	}

	public void setInterestBidField(String interestBidField) {
		this.interestBidField = interestBidField;
	}

	public int getSpotConstant() {
		return spotConstant;
	}

	public void setSpotConstant(int spotConstant) {
		this.spotConstant = spotConstant;
	}

	public boolean isDivideC16ByC15() {
		return divideC16ByC15;
	}

	public void setDivideC16ByC15(boolean divideC16ByC15) {
		this.divideC16ByC15 = divideC16ByC15;
	}
}