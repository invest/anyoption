package com.anyoption.service.level.formula;

import il.co.etrader.service.level.BigDecimalHelper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ReutersQuotes;
import com.anyoption.service.level.Utils;
import com.anyoption.service.level.market.Market;
import com.anyoption.service.level.market.UpdateResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author kirilim
 */
public class AvgOf2Formula extends Formula {

	private static final Logger log = Logger.getLogger(AvgOf2Formula.class);
	private BigDecimal field1;
	private BigDecimal field2;
	private AvgOf2FormulaConfig formulaConfig;
	private boolean snapshotReceived;

	public AvgOf2Formula(String formulaParams, FormulaRole role) {
		super();
		Gson gson = new GsonBuilder().serializeNulls().create();
		formulaConfig = gson.fromJson(formulaParams, AvgOf2FormulaConfig.class);
		formulaConfig.setDefaultSubscriptionConfig(formulaConfig.cloneSubscriptionConfig(formulaConfig.getSubscriptionConfig()));
		this.role = role;
	}

	public AvgOf2Formula(AvgOf2FormulaConfig formulaConfig, FormulaRole role) {
		super();
		this.formulaConfig = formulaConfig;
		formulaConfig.setDefaultSubscriptionConfig(formulaConfig.cloneSubscriptionConfig(formulaConfig.getSubscriptionConfig()));
		this.role = role;
	}

	@Override
	public void collectDataSubscription(Map<String, Map<String, String>> subscriptionConfigs) {
		subscriptionConfigs.putAll(formulaConfig.getSubscriptionConfig());
	}

	@Override
	public void dataUpdate(String dataSubscription, Map<String, Object> fields, Market market, List<Opportunity> opportunities,
							UpdateResult result) {
		Double fieldOne = (Double) fields.get(formulaConfig.getField1Name());
		Double fieldTwo = (Double) fields.get(formulaConfig.getField2Name());
		Double trdprc1 = (Double) fields.get(formulaConfig.getLastTradePriceField());
		String tradeTime = (String) fields.get(formulaConfig.getLastTradeTimeField());

		if ((null == fieldOne && null == fieldTwo && null == tradeTime) || (null != trdprc1 && trdprc1 == 0)
				|| (null != fieldOne && fieldOne == 0) || (null != fieldTwo && fieldTwo == 0)) {
			return;
		}
		market.setLastUpdateReceiveTime(System.currentTimeMillis());

		if (fieldOne != null) {
			this.field1 = new BigDecimal(fieldOne);
		}
		if (fieldTwo != null) {
			this.field2 = new BigDecimal(fieldTwo);
		}
		if (tradeTime != null) {
			this.lastTradeTime = tradeTime;
		}

		if (field1 == null || field2 == null || lastTradeTime == null) {
			return;
		}
		
		double crrRealLevel = field1.add(field2)
									.divide(BigDecimalHelper.TWO, BigDecimalHelper.BIG_DECIMAL_CALC_SCALE, RoundingMode.HALF_UP)
									.doubleValue();
		// for test
		log.trace("crrRealLevel: " + crrRealLevel + " role: " + role);
		// This formula cannot be used as FormulaRole.LONG_TERM
		if (role == FormulaRole.CURRENT_LEVEL) {
			double crrLevel = Utils.formulaRandomChange(market.getState(), crrRealLevel, market.getRandomCeiling(), market.getRandomFloor());
			market.getState().setLastCalcResult(crrRealLevel);
			market.getState().setLastFormulaResult(crrLevel);
		} else if (role == FormulaRole.REAL_LEVEL) {
			market.getState().setLastRealLevel(crrRealLevel);
		} else if (trdprc1 != null || formulaConfig.isIgnoreTradePrice()) { // (role == FormulaRole.DAY_CLOSING || role == FormulaRole.HOUR_CLOSING)
			Date updateTime = null;
			if (lastTradeTime != null) {
				try {
					updateTime = market.getUpdateTime(lastTradeTime, market.getMarketParams().getTimeZone(), market.getMarketParams().getTimePattern());
				} catch (Throwable t) {
					log.error("Can't process lastTradeTime.", t);
					updateTime = new Date(System.currentTimeMillis());
				}
			} else {
				log.error("Update without lastTradeTime!!!");
				updateTime = new Date(System.currentTimeMillis());
			}
			String closingLevelTxt = "formula:round((" + formulaConfig.getField1Name() + " + " + formulaConfig.getField2Name() + ") / 2); "
										+ formulaConfig.getField1Name() + ": " + field1 + "; " + formulaConfig.getField2Name() + ": "
										+ field2 + ";closeLevelBeforeRound: " + crrRealLevel;
			ReutersQuotes currentQuote = new ReutersQuotes(updateTime, null, field1, field2);
			if (role == FormulaRole.HOUR_CLOSING) {
				market.updateOpportunitiesClosingLevel(	updateTime, crrRealLevel, 0d, closingLevelTxt, null, currentQuote, null,
														opportunities, result);
			} else {
				market.updateOpportunitiesClosingLevel(	updateTime, 0d, crrRealLevel, null, closingLevelTxt, null, currentQuote,
														opportunities, result);
			}
		}
		snapshotReceived = true;
	}

	@Override
	public FormulaConfig getFormulaConfig() {
		return formulaConfig;
	}

	@Override
	public boolean isSnapshotReceived() {
		return snapshotReceived;
	}

	@Override
	public void resetInTheEndOfTheDay() {
		field1 = null;
		field2 = null;
		snapshotReceived = false;
		lastTradeTime = null;
		formulaConfig.setSubscriptionConfig(formulaConfig.cloneSubscriptionConfig(formulaConfig.getDefaultSubscriptionConfig()));
	}
	
	@Override
	public Map<String, BigDecimal> getQuoteFields() {
		// Here we assume that if we are doing avg of 2 fields they will be ASK and BID
		// Also we assume that we are with Reuters as feed and there the ASK and BID fields are named ASK and BID
		// If one of the above is not ture we will need a more sophisticated implementation
		Map<String, BigDecimal> q = new HashMap<String, BigDecimal>();
		q.put(formulaConfig.getField1Name(), field1);
		q.put(formulaConfig.getField2Name(), field2);
		return q;
	}
}