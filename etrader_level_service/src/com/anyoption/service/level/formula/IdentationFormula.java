package com.anyoption.service.level.formula;

import il.co.etrader.service.level.LevelService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ReutersQuotes;
import com.anyoption.service.level.Utils;
import com.anyoption.service.level.market.Market;
import com.anyoption.service.level.market.UpdateResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author EyalG
 */
public class IdentationFormula extends Formula {

	private static final Logger log = Logger.getLogger(IdentationFormula.class);
	private BigDecimal field1;
	private IdentationFormulaConfig formulaConfig;
	private boolean snapshotReceived;

	public IdentationFormula(String formulaParams, FormulaRole role) {
		super();
		Gson gson = new GsonBuilder().serializeNulls().create();
		formulaConfig = gson.fromJson(formulaParams, IdentationFormulaConfig.class);
		formulaConfig.setDefaultSubscriptionConfig(formulaConfig.cloneSubscriptionConfig(formulaConfig.getSubscriptionConfig()));
		this.role = role;
	}

	@Override
	public void collectDataSubscription(Map<String, Map<String, String>> subscriptionConfigs) {
		subscriptionConfigs.putAll(formulaConfig.getSubscriptionConfig());
	}

	@Override
	public void dataUpdate(String dataSubscription, Map<String, Object> fields, Market market, List<Opportunity> opportunities,
							UpdateResult result) {
		//TODO: long term
		Double fieldOne = (Double) fields.get(formulaConfig.getField1Name());
		String tradeTime = (String) fields.get(formulaConfig.getLastTradeTimeField());
		if (null != fieldOne && fieldOne > 0) {
			this.field1 = new BigDecimal(fieldOne);
        }

        if (null != tradeTime) {
        	this.lastTradeTime = tradeTime;
        }
        
        if (null != fieldOne && fieldOne > 0) {
        	market.setLastUpdateReceiveTime(System.currentTimeMillis());
        	snapshotReceived = true;
//        	if (interest.isSnapshotReceived()) {
//                setLongTermSnapshotReceived(true);
//            }
			// for test
			log.trace("crrRealLevel: " + field1 + "; role: " + role);
			// This formula cannot be used as FormulaRole.LONG_TERM
			if (role == FormulaRole.CURRENT_LEVEL) {
				double crrLevel = Utils.formulaRandomChange(market.getState(), field1.doubleValue(), market.getRandomCeiling(), market.getRandomFloor());
				market.getState().setLastCalcResult(field1.doubleValue());
				market.getState().setLastFormulaResult(crrLevel);
			} else if (role == FormulaRole.REAL_LEVEL) {
				market.getState().setLastRealLevel(field1.doubleValue());
			} else { //if (role == FormulaRole.DAY_CLOSING || role == FormulaRole.HOUR_CLOSING) {
				Date updateTime = null;
				if (lastTradeTime != null) {
					try {
						updateTime = market.getUpdateTime(lastTradeTime, market.getMarketParams().getTimeZone(), market.getMarketParams().getTimePattern());
					} catch (Throwable t) {
						log.error("Can't process lastTradeTime.", t);
						updateTime = new Date(System.currentTimeMillis());
					}
				} else {
					log.error("Update without lastTradeTime!!!");
					updateTime = new Date(System.currentTimeMillis());
				}
				String closingLevelTxt = "formula:" + formulaConfig.getField1Name() + ";" + formulaConfig.getField1Name() + ":" + field1 + ";closeLevelBeforeRound:" + field1;
				
				ReutersQuotes currentQuote = new ReutersQuotes(updateTime, field1.doubleValue());
				if (role == FormulaRole.HOUR_CLOSING) {
					market.updateOpportunitiesClosingLevel(	updateTime, field1.doubleValue(), 0d, closingLevelTxt, null, currentQuote, null,
															opportunities, result);
				} else {
					market.updateOpportunitiesClosingLevel(	updateTime, 0d, field1.doubleValue(), null, closingLevelTxt, null, currentQuote,
															opportunities, result);
				}
			}
        }
	}

	@Override
	public FormulaConfig getFormulaConfig() {
		return formulaConfig;
	}

	@Override
	public boolean isSnapshotReceived() {
		return snapshotReceived;
	}

	@Override
	public void resetInTheEndOfTheDay() {
		field1 = null;
		snapshotReceived = false;
		lastTradeTime = null;
		formulaConfig.setSubscriptionConfig(formulaConfig.cloneSubscriptionConfig(formulaConfig.getDefaultSubscriptionConfig()));
	}
	
	@Override
	public Map<String, BigDecimal> getQuoteFields() {
		// Here we assume that this formula is used only on markets that you quote the LAST field. There is no point to quote only ASK or only BID
		Map<String, BigDecimal> q = new HashMap<String, BigDecimal>();
		q.put(LevelService.QUOTE_FIELDS_LAST, field1);
		return q;
	}
}