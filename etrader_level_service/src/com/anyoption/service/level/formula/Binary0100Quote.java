package com.anyoption.service.level.formula;

public class Binary0100Quote {
    private double offer;
    private double bid;
    
	public double getOffer() {
		return offer;
	}
	
	public void setOffer(double offer) {
		this.offer = offer;
	}
	
	public double getBid() {
		return bid;
	}
	
	public void setBid(double bid) {
		this.bid = bid;
	}
}