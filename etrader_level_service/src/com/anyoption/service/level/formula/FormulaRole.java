package com.anyoption.service.level.formula;

/**
 * @author kirilim
 *
 */
public enum FormulaRole {
	CURRENT_LEVEL, HOUR_CLOSING, DAY_CLOSING, LONG_TERM, REAL_LEVEL;
}
