package com.anyoption.service.level.formula;

/**
 * @author kirilim
 */
public class CurrencyFXALFormulaConfig extends FormulaConfig {

	private Last5UpdatesAvgOf2FormulaConfig last5UpdatesFormulaConfig;
	private AvgOf2FormulaConfig avgOf2FormulaConfig;
	private float fXALParameter;

	public Last5UpdatesAvgOf2FormulaConfig getLast5UpdatesFormulaConfig() {
		return last5UpdatesFormulaConfig;
	}

	public void setLast5UpdatesFormulaConfig(Last5UpdatesAvgOf2FormulaConfig last5UpdatesFormulaConfig) {
		this.last5UpdatesFormulaConfig = last5UpdatesFormulaConfig;
	}

	public AvgOf2FormulaConfig getAvgOf2FormulaConfig() {
		return avgOf2FormulaConfig;
	}

	public void setAvgOf2FormulaConfig(AvgOf2FormulaConfig avgOf2FormulaConfig) {
		this.avgOf2FormulaConfig = avgOf2FormulaConfig;
	}

	public float getFXALParameter() {
		return fXALParameter;
	}

	public void setFXALParameter(float fXALParameter) {
		this.fXALParameter = fXALParameter;
	}
}