//package com.anyoption.service.level.formula;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.service.level.market.Market;
//
//public class Binary0100CurrencyFormula extends Binary0100Formula {
//	private static final Logger log = Logger.getLogger(Binary0100CurrencyFormula.class);
//	
//	private Last5UpdatesAvgOf2Formula currencyInternalFormula;
//
//	public Binary0100CurrencyFormula(String formulaParams, FormulaRole role) {
//		super(formulaParams, role);
//		currencyInternalFormula = (Last5UpdatesAvgOf2Formula) internalFormula;
//	}
//	
//	@Override
//    protected double getInternalFormulaCurrentLevel(Market market) {
//		if (currencyInternalFormula.isSnapshotReceived()) {
//	    	double ask = currencyInternalFormula.getField1().doubleValue();
//	    	double bid = currencyInternalFormula.getField2().doubleValue();
//	    	double result = ((ask + bid) / 2 + market.getState().getLastCalcResult()) / 2;
//	    	log.debug("current level =  ((ask + bid) / 2 " +  (ask + bid) / 2 + " + current level " + market.getState().getLastCalcResult() + ") / 2 = " + result);
//	    	return result;
//		}
//	    return 0;
//    }
//}