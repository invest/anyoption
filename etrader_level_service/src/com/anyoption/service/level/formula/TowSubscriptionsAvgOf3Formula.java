package com.anyoption.service.level.formula;

import il.co.etrader.service.level.BigDecimalHelper;
import il.co.etrader.service.level.LevelService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.service.level.Utils;
import com.anyoption.service.level.market.Market;
import com.anyoption.service.level.market.UpdateResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author EyalG
 * this class is used for markets with future and trader paramter
 * the formula is first Subscription ((filed1 + traderparameter) / 2) + second Subscription (filed1 + filed2 + filed3) / 3 
 */
public class TowSubscriptionsAvgOf3Formula extends Formula {

	private static final Logger log = Logger.getLogger(TowSubscriptionsAvgOf3Formula.class);
	
	protected static final BigDecimal OOO5 = new BigDecimal("0.005");
	
	private BigDecimal firstSubscriptionField1;
	private BigDecimal secondSubscriptionField1;
	private BigDecimal secondSubscriptionField2;
	private BigDecimal secondSubscriptionField3;
	private long firstSubscriptionField1UpdateTime;
	private long secondSubscriptionField1UpdateTime;
	private long secondSubscriptionField2UpdateTime;
	private long secondSubscriptionField3UpdateTime;

	private TowSubscriptionsAvgOf3FormulaConfig formulaConfig;
	private boolean firstSubscriptionSnapshotReceived;
	private boolean secondSubscriptionSnapshotReceived;

	public TowSubscriptionsAvgOf3Formula(String formulaParams, FormulaRole role) {
		super();
		Gson gson = new GsonBuilder().serializeNulls().create();
		formulaConfig = gson.fromJson(formulaParams, TowSubscriptionsAvgOf3FormulaConfig.class);
		formulaConfig.setDefaultSubscriptionConfig(formulaConfig.cloneSubscriptionConfig(formulaConfig.getSubscriptionConfig()));
		this.role = role;
	}

	@Override
	public void collectDataSubscription(Map<String, Map<String, String>> subscriptionConfigs) {
		subscriptionConfigs.putAll(formulaConfig.getSubscriptionConfig());
	}

	@Override
	public void dataUpdate(String dataSubscription, Map<String, Object> fields, Market market, List<Opportunity> opportunities,
							UpdateResult result) {
		if (dataSubscription.equals(formulaConfig.getFirstSubscriptionName())) {	        
	        Double trdprc1 = (Double) fields.get(formulaConfig.getFirstSubscriptionField1());
			String tradeTime = (String) fields.get(formulaConfig.getLastTradeTimeField());
	        if (tradeTime != null) {
				this.lastTradeTime = tradeTime;
			}
	        
	        if (null != trdprc1 && trdprc1 > 0) {
	        	market.setLastUpdateReceiveTime(System.currentTimeMillis());
	        	firstSubscriptionField1UpdateTime = market.getLastUpdateReceiveTime();
	        	
	        	firstSubscriptionField1 = new BigDecimal(trdprc1.toString());
	        	firstSubscriptionSnapshotReceived = true;
				if (secondSubscriptionSnapshotReceived) {
					calcLevel(market, true, trdprc1);
				}
	        }
		} else { //Subscription
			try {
                boolean shouldRecalc = false;
                Double fieldOne = (Double) fields.get(formulaConfig.getSecondSubscriptionField1());
        		Double fieldTwo = (Double) fields.get(formulaConfig.getSecondSubscriptionField2());
        		Double fieldThree = (Double) fields.get(formulaConfig.getSecondSubscriptionField3());
        		
        		long lastUpdateTime = System.currentTimeMillis();
                if (null != fieldOne && fieldOne > 0) {
                	secondSubscriptionField1UpdateTime = lastUpdateTime;
                    secondSubscriptionField1 = new BigDecimal(fieldOne.toString());
                    shouldRecalc = true;
                }
                if (null != fieldTwo && fieldTwo > 0) {
                	secondSubscriptionField2UpdateTime = lastUpdateTime;
                    secondSubscriptionField2 = new BigDecimal(fieldTwo.toString());
                    shouldRecalc = true;
                }
                if (null != fieldThree && fieldThree > 0) {
                	secondSubscriptionField3UpdateTime = lastUpdateTime;
                    secondSubscriptionField3 = new BigDecimal(fieldThree.toString());
                    shouldRecalc = true;
                }
				if (null != secondSubscriptionField1 && null != secondSubscriptionField2 && null != secondSubscriptionField3) {
					secondSubscriptionSnapshotReceived = true;
				}
				if (shouldRecalc) {
					market.setLastUpdateReceiveTime(System.currentTimeMillis());					
				}
				if (shouldRecalc && firstSubscriptionSnapshotReceived && secondSubscriptionSnapshotReceived) {
					calcLevel(market, false, 0);
				}
            } catch (Exception e) {
                log.error("Failed to process data update.", e);
            }
		}
	}
	
	/**
     * Calculate the level in to the <code>lastFormulaResult</code>.
     * Call this metod only when you have lock on the options.
	 * @param market 
     * 
     * @param market the market we calc the level for
     * @param haveRealLevelUpdate
     * @param realLevelUpdate
     */
    protected void calcLevel(Market market, boolean haveRealLevelUpdate, double realLevelUpdate) {
        BigDecimal tmpcl =
        		secondSubscriptionField3
                .add(secondSubscriptionField1)
                .add(secondSubscriptionField2)
                .divide(BigDecimalHelper.THREE, BigDecimalHelper.BIG_DECIMAL_CALC_SCALE, RoundingMode.HALF_UP);
        double cl = addFuture(tmpcl);

        // for test
        log.trace("cl: " + cl + "; role: " + role);
        if (role == FormulaRole.CURRENT_LEVEL) {
			double crrLevel = Utils.formulaRandomChange(market.getState(), cl, market.getRandomCeiling(), market.getRandomFloor());
			market.getState().setLastCalcResult(cl);
			market.getState().setLastFormulaResult(crrLevel);
		} else if (role == FormulaRole.REAL_LEVEL) {
			market.getState().setLastRealLevel(realLevelUpdate);
		} else { // (role == FormulaRole.DAY_CLOSING || role == FormulaRole.HOUR_CLOSING)
			log.warn("Last5Updates2Fields formula configured as closing level. NOT SUPPORTED!!!");
		}
    }
    
    /**
     * add the future to the level
     * @param tmpcl the level without the future
     * @return the level with future
     */
    protected double addFuture(BigDecimal tmpcl) {
    	return tmpcl.add(LevelService.getTraderParameter(formulaConfig.getTraderParamName()))
				.add(firstSubscriptionField1)
    			.divide(BigDecimalHelper.TWO)
    			.doubleValue();
	}

	@Override
	public FormulaConfig getFormulaConfig() {
		return formulaConfig;
	}

	@Override
	public boolean isSnapshotReceived() {
		return firstSubscriptionSnapshotReceived && secondSubscriptionSnapshotReceived;
	}

	@Override
	public void resetInTheEndOfTheDay() {
		firstSubscriptionField1 = null;
		secondSubscriptionField1 = null;
		secondSubscriptionField2 = null;
		secondSubscriptionField3 = null;
		firstSubscriptionField1UpdateTime = 0;
		secondSubscriptionField1UpdateTime = 0;
		secondSubscriptionField2UpdateTime = 0;
		secondSubscriptionField3UpdateTime = 0;
		firstSubscriptionSnapshotReceived = false;
		secondSubscriptionSnapshotReceived = false;
		lastTradeTime = null;
		formulaConfig.setSubscriptionConfig(formulaConfig.cloneSubscriptionConfig(formulaConfig.getDefaultSubscriptionConfig()));
	}

	public long getFirstSubscriptionField1UpdateTime() {
		return firstSubscriptionField1UpdateTime;
	}

	public void setFirstSubscriptionField1UpdateTime(
			long firstSubscriptionField1UpdateTime) {
		this.firstSubscriptionField1UpdateTime = firstSubscriptionField1UpdateTime;
	}

	public long getSecondSubscriptionField1UpdateTime() {
		return secondSubscriptionField1UpdateTime;
	}

	public void setSecondSubscriptionField1UpdateTime(
			long secondSubscriptionField1UpdateTime) {
		this.secondSubscriptionField1UpdateTime = secondSubscriptionField1UpdateTime;
	}

	public long getSecondSubscriptionField2UpdateTime() {
		return secondSubscriptionField2UpdateTime;
	}

	public void setSecondSubscriptionField2UpdateTime(
			long secondSubscriptionField2UpdateTime) {
		this.secondSubscriptionField2UpdateTime = secondSubscriptionField2UpdateTime;
	}

	public long getSecondSubscriptionField3UpdateTime() {
		return secondSubscriptionField3UpdateTime;
	}

	public void setSecondSubscriptionField3UpdateTime(
			long secondSubscriptionField3UpdateTime) {
		this.secondSubscriptionField3UpdateTime = secondSubscriptionField3UpdateTime;
	}
	
	@Override
	public Map<String, BigDecimal> getQuoteFields() {
		// Here we assume that this formula is used only on markets that you quote the LAST field. There is no point to quote only ASK or only BID
		Map<String, BigDecimal> q = new HashMap<String, BigDecimal>();
		q.put(LevelService.QUOTE_FIELDS_LAST, firstSubscriptionField1);
		return q;
	}
}