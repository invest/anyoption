package com.anyoption.service.level.formula;

/**
 * @author EyalG
 */
public class AvgOf3FormulaConfig extends FormulaConfig {

	private String field1Name;
	private String field2Name;
	private String lastTradePriceField;
	private String lastTradeTimeField;

	public String getField1Name() {
		return field1Name;
	}

	public void setField1Name(String field1Name) {
		this.field1Name = field1Name;
	}

	public String getField2Name() {
		return field2Name;
	}

	public void setField2Name(String field2Name) {
		this.field2Name = field2Name;
	}

	public String getLastTradePriceField() {
		return lastTradePriceField;
	}

	public void setLastTradePriceField(String lastTradePriceField) {
		this.lastTradePriceField = lastTradePriceField;
	}

	public String getLastTradeTimeField() {
		return lastTradeTimeField;
	}

	public void setLastTradeTimeField(String lastTradeTimeField) {
		this.lastTradeTimeField = lastTradeTimeField;
	}
}