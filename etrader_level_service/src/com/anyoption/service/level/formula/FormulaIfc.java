package com.anyoption.service.level.formula;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.service.level.market.Market;
import com.anyoption.service.level.market.UpdateResult;
import com.anyoption.service.level.market.Market.BackendCommand;

/**
 * @author pavelhe
 */
public interface FormulaIfc {
	public void collectDataSubscription(Map<String, Map<String, String>> subscriptionConfigs);

	/**
	 * 
	 * @param dataSubscription
	 * @param fields
	 * @param marketState
	 * @param opportunities
	 * @return true if there is LAST field update
	 */
	public void dataUpdate(String dataSubscription, Map<String, Object> fields, Market market, List<Opportunity> opportunities, UpdateResult result);

	public boolean isSnapshotReceived();

//	public String getClosingLevelTxt(Opportunity o);

	public void resetInTheEndOfTheDay();

	public boolean needUpdate(String dataSubscription);

	public FormulaConfig getFormulaConfig();
	
	public Map<String, BigDecimal> getQuoteFields();

	public void handleBackendCommand(BackendCommand command, String commandParams);

	public void resetSubscriptionConfigElements();
}