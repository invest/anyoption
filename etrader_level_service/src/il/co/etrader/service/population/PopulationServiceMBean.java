package il.co.etrader.service.population;

public interface PopulationServiceMBean {
    public void startService();
    public void stopService();
}