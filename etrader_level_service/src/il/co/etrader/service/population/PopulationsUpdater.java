package il.co.etrader.service.population;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import il.co.etrader.bl_vos.PopulationType;
import il.co.etrader.service.population.managers.PopulationsManager;

public class PopulationsUpdater extends Thread {
    private static Logger log = Logger.getLogger(PopulationService.class);

    private boolean running;
    private ArrayList<PopulationType> populationTypes;

    public void run() {
        try {
            populationTypes = PopulationsManager.getPopulationTypes();
        } catch (Exception e) {
            log.fatal("Can't load population types.", e);
        }
        if (null == populationTypes) {
            return;
        }
        Thread.currentThread().setName("PopulationsUpdater");
        if (log.isInfoEnabled()) {
            log.info("PopulationsUpdater started.");
        }
        running = true;
        while (running) {
            log.debug("Updating populations.");
//            long now = System.currentTimeMillis();
//            for (PopulationType t : populationTypes) {
//                boolean refreshToday = ((t.getRefreshDays() & (1 << (Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1))) != 0);
//                boolean refreshPeriodExpired = null == t.getLastRefreshTime() || now - t.getLastRefreshTime().getTime() > t.getRefreshPeriod() * 60 * 1000;
//                if (refreshToday && refreshPeriodExpired) {

	            try {
	            	PopulationsManager.populationHandlerJob();
	            } catch (Exception e) {
	                log.error("Failed to update population.", e);
	            }
//                    t.setLastRefreshTime(new Date(System.currentTimeMillis()));
//                }
//            }
	            log.debug("Finished updating populations.");
            synchronized (this) {
                try {
                    wait(50 * 60 * 1000);
                } catch (InterruptedException ie) {
                    // do nothing
                }
            }
        }
        log.warn("PopulationsUpdater done.");
    }

    public void stopPopulationsUpdater() {
        log.info("Stopping PopulationsUpdater...");
        running = false;
        synchronized (this) {
            notify();
        }
    }
}