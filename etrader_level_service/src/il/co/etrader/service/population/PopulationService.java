package il.co.etrader.service.population;

import org.apache.log4j.Logger;

public class PopulationService implements PopulationServiceMBean {
    private static Logger log = Logger.getLogger(PopulationService.class);

    private PopulationsUpdater updater;

    public void startService() {
        log.debug("PopulationService.startService begin");
        updater = new PopulationsUpdater();
        updater.start();
        log.info("PopulationService started...");
    }

    public void stopService() {
        log.debug("PopulationService.stopService begin");
        if (null != updater) {
            updater.stopPopulationsUpdater();
            updater = null;
        }
        log.info("PopulationService stopped");
    }
}