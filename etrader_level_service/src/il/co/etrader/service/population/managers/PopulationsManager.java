package il.co.etrader.service.population.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.bl_vos.PopulationType;
import il.co.etrader.service.population.daos.PopulationsDAO;

public class PopulationsManager extends BaseBLManager {
    public static ArrayList<PopulationType> getPopulationTypes() throws SQLException {
        ArrayList<PopulationType> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = PopulationsDAO.getPopulationTypes(conn);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    public static void fillNoDeposit() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            PopulationsDAO.fillNoDeposit(conn);
        } finally {
            closeConnection(conn);
        }
    }

	public static void declineFirst() throws SQLException {
		Connection conn = null;
        try {
            conn = getConnection();
            PopulationsDAO.declineFirst(conn);
        } finally {
            closeConnection(conn);
        }
	}

	public static void declineLast() throws SQLException {
		Connection conn = null;
        try {
            conn = getConnection();
            PopulationsDAO.declineLast(conn);
        } finally {
            closeConnection(conn);
        }
	}

	public static void oneDeposite() throws SQLException {
		Connection conn = null;
        try {
            conn = getConnection();
            PopulationsDAO.oneDeposite(conn);
        } finally {
            closeConnection(conn);
        }
	}

	public static void dormant() throws SQLException {
		Connection conn = null;
        try {
            conn = getConnection();
            PopulationsDAO.dormant(conn);
        } finally {
            closeConnection(conn);
        }
	}

	public static void balanceLessThenMinInvLimit() throws SQLException {
		Connection conn = null;
        try {
            conn = getConnection();
            PopulationsDAO.balanceLessThenMinInvLimit(conn);
        } finally {
            closeConnection(conn);
        }
	}

	public static void newDormant() throws SQLException {
		Connection conn = null;
        try {
            conn = getConnection();
            PopulationsDAO.newDormant(conn);
        } finally {
            closeConnection(conn);
        }
	}

	public static void newNoInvestments() throws SQLException {
		Connection conn = null;
        try {
            conn = getConnection();
            PopulationsDAO.newNoInvestments(conn);
        } finally {
            closeConnection(conn);
        }
	}

	public static void populationHandlerJob() throws SQLException {
		Connection conn = null;
        try {
            conn = getConnection();
            PopulationsDAO.populationHandlerJob(conn);
        } finally {
            closeConnection(conn);
        }
	}
}