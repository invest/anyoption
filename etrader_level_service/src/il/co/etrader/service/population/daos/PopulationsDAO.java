package il.co.etrader.service.population.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.bl_vos.PopulationType;

public class PopulationsDAO extends DAOBase {
    public static ArrayList<PopulationType> getPopulationTypes(Connection conn) throws SQLException {
        Statement stmt = null;
        ResultSet rs = null;
        ArrayList<PopulationType> list = new ArrayList<PopulationType>();
        try {
            String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "population_types";
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                PopulationType t = new PopulationType();
                t.setId(rs.getInt("id"));
                t.setName(rs.getString("name"));
                t.setRefreshPeriod(rs.getLong("refresh_period"));
                t.setRefreshDays(rs.getLong("refresh_days"));
                list.add(t);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(stmt);
        }
        return list;
    }

    public static void fillNoDeposit(Connection conn) throws SQLException {
        CallableStatement cstmt = null;
        try {
            cstmt = conn.prepareCall("BEGIN population_no_deposit_fill(); END;");
            cstmt.execute();
        } finally {
            closeStatement(cstmt);
        }
    }

	public static void declineFirst(Connection conn) throws SQLException {
		CallableStatement cstmt = null;
        try {
            cstmt = conn.prepareCall("BEGIN population_first_depo_failed(); END;");
            cstmt.execute();
        } finally {
            closeStatement(cstmt);
        }
	}

	public static void declineLast(Connection conn) throws SQLException {
		CallableStatement cstmt = null;
        try {
            cstmt = conn.prepareCall("BEGIN population_last_depo_failed(); END;");
            cstmt.execute();
        } finally {
            closeStatement(cstmt);
        }
	}

	public static void oneDeposite(Connection conn) throws SQLException {
		CallableStatement cstmt = null;
        try {
            cstmt = conn.prepareCall("BEGIN population_one_time_deposite(); END;");
            cstmt.execute();
        } finally {
            closeStatement(cstmt);
        }
	}

	public static void dormant(Connection conn) throws SQLException {
		CallableStatement cstmt = null;
        try {
            cstmt = conn.prepareCall("BEGIN population_dormant_account(); END;");
            cstmt.execute();
        } finally {
            closeStatement(cstmt);
        }
	}

	public static void balanceLessThenMinInvLimit(Connection conn) throws SQLException {
		CallableStatement cstmt = null;
        try {
            cstmt = conn.prepareCall("BEGIN population_minimum_balance(); END;");
            cstmt.execute();
        } finally {
            closeStatement(cstmt);
        }
	}

	public static void newDormant(Connection conn) throws SQLException {
		CallableStatement cstmt = null;
        try {
            cstmt = conn.prepareCall("BEGIN POPULATION_DORMANT_ACCOUNT_NEW(); END;");
            cstmt.execute();
        } finally {
            closeStatement(cstmt);
        }
	}

	public static void newNoInvestments(Connection conn) throws SQLException {
		CallableStatement cstmt = null;
        try {
            cstmt = conn.prepareCall("BEGIN POPULATION_NO_INVESTMENTS(); END;");
            cstmt.execute();
        } finally {
            closeStatement(cstmt);
        }
	}

	public static void populationHandlerJob(Connection conn) throws SQLException {
		CallableStatement cstmt = null;
        try {
            cstmt = conn.prepareCall("BEGIN POPULATIONS_HANDLER(0,0); END;");
            cstmt.execute();
        } finally {
            closeStatement(cstmt);
        }
	}
}