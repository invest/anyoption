package il.co.etrader.service.level;

import il.co.etrader.service.level.items.Item;
import il.co.etrader.service.level.options.Option;

/**
 * Interface to be implemented by the <code>LevelService</code> so
 * the <code>Item</code>s can request subscribtion for options.
 * 
 * @author Tony
 */
public interface OptionSubscribtionManager {
    /**
     * Request subscribtion for option.
     * 
     * @param item the item that should receive the updates for the newly subscribed item (option for item)
     * @param option the option details (where to save the new item (option) handle)
     */
    public void subscribeOption(Item item, Option option);

    /**
     * Request unsubscribtion for option.
     * 
     * @param item the item that want to stop receiving updates for this item (option for item)
     * @param option the option details (where to take the item (option) handle from)
     */
    public void unsubscribeOption(Item item, Option option);
}