/**
 * 
 */
package il.co.etrader.service.level;

import java.util.Date;

import com.anyoption.common.beans.LiveJMSMessage;

/**
 * @author pavelhe
 *
 */
public class LiveMessageWrapper {
	
	private LiveJMSMessage message;
	private long amount;
	private Date time;
	private long opportunityId;
	private long marketId;

	/**
	 * @return the message
	 */
	public LiveJMSMessage getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(LiveJMSMessage message) {
		this.message = message;
	}

	/**
	 * @return the amount
	 */
	public long getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(long amount) {
		this.amount = amount;
	}

	/**
	 * @return the time
	 */
	public Date getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(Date time) {
		this.time = time;
	}

	/**
	 * @return the opportunityId
	 */
	public long getOpportunityId() {
		return opportunityId;
	}

	/**
	 * @param opportunityId the opportunityId to set
	 */
	public void setOpportunityId(long opportunityId) {
		this.opportunityId = opportunityId;
	}

	/**
	 * @return the marketId
	 */
	public long getMarketId() {
		return marketId;
	}

	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	@Override
	public String toString() {
		return "LiveMessageWrapper [message=" + message + ", amount=" + amount
				+ ", time=" + time + ", opportunityId=" + opportunityId
				+ ", marketId=" + marketId + "]";
	}

}
