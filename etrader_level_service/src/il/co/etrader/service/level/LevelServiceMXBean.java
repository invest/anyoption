package il.co.etrader.service.level;

public interface LevelServiceMXBean {
    public String getInitialContextFactory();
    public void setInitialContextFactory(String initialContextFactory);

    public String getProviderURL();
    public void setProviderURL(String providerURL);

    public String getConnectionFactoryName();
    public void setConnectionFactoryName(String connectionFactoryName);

    public String getQueueName();
    public void setQueueName(String queueName);

    public String getTopicName();
    public void setTopicName(String topicName);

    public String getWebTopicName();
    public void setWebTopicName(String webTopicName);

    public Integer getMsgPoolSize();
    public void setMsgPoolSize(Integer msgPoolSize);

    public Integer getRecoveryPause();
    public void setRecoveryPause(Integer recoveryPause);

    public Boolean isInternalDataService();
    public void setInternalDataService(Boolean intnalDataService);
    
    public String getReutersProviderURL();
    public void setReutersProviderURL(String reutersProviderURL);

    public String getReutersQueueName();
    public void setReutersQueueName(String reutersQueueName);

    public String getReutersTopicName();
    public void setReutersTopicName(String reutersTopicName);
    
    public String getCreateTime();
    public void setCreateTime(String createTime);

    public String getDaysAhead();
    public void setDaysAhead(String daysAhead);

    public String getEmailServer();
    public void setEmailServer(String emailServer);

    public Boolean isEmailServerAuth();
    public void setEmailServerAuth(Boolean auth);

    public String getEmailUser();
    public void setEmailUser(String emailUser);

    public String getEmailPass();
    public void setEmailPass(String emailPass);

    public String getEmailFrom();
    public void setEmailFrom(String emailFrom);

    public String getEmailRcpt();
    public void setEmailRcpt(String emailRcpt);

    public String getEmailSubject();
    public void setEmailSubject(String emailSubject);

    public String getReportEmailRcpt();
    public void setReportEmailRcpt(String reportEmailRcpt);

    public String getReportEmailSubject();
    public void setReportEmailSubject(String reportEmailSubject);

    public Integer getAlertAfterXMinNotClosed();
    public void setAlertAfterXMinNotClosed(Integer alertAfterXMinNotClosed);

    public Integer getCloseAfterXSecNotClosed();
    public void setCloseAfterXSecNotClosed(Integer closeAfterXSecNotClosed);

    public Integer getTa25AskBidIgnoreSpread();
    public void setTa25AskBidIgnoreSpread(Integer ta25AskBidIgnoreSpread);

    public Integer getIlsAskBidIgnoreSpread();
    public void setIlsAskBidIgnoreSpread(Integer ilsAskBidIgnoreSpread);

    public Integer getReutersSubscribeRetryPeriod();
    public void setReutersSubscribeRetryPeriod(Integer reutersSubscribeRetryPeriod);

    public String getReportEmailSubjectOneTouch();
    public void setReportEmailSubjectOneTouch(String reportEmailSubjectOneTouch);

    public String getNonSettlementEmailSubject();
    public void setNonSettlementEmailSubject(String nonSettlementEmailSubject);

    public Boolean isSettlementAlive();
    public Boolean isSchedulerAlive();
    public Boolean isHeartbeatAlive();
    public Boolean isMonitoringAlive();
    public Boolean isRatesJobAlive();
    public String getConnectionState();
    public String getConnectionText();
    
    public String getDataServiceClassName();
    public void setDataServiceClassName(String dataServiceClassName);
    
    public String getDataServiceAttributes();
    public void setDataServiceAttributes(String dataServiceAttributes);
}