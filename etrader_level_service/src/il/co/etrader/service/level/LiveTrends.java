package il.co.etrader.service.level;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.LiveJMSMessage;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.base.Skin;

import il.co.etrader.bl_vos.OpportunityProductType;

public class LiveTrends implements LiveInvestmentListener {
	private static final Logger log = Logger.getLogger(LiveTrends.class);
	
	private SenderThread sender;
	private Map<Long, Set<Opportunity>> opps;
	private LiveTrendsDataComparator comparator;
	private Map<Long, Map<String, String>> lastUpdateHM = new HashMap<Long, Map<String,String>>();
	
	public LiveTrends(SenderThread sender) {
		log.trace("Initializing LiveTrends...");
		this.sender = sender;
		opps = new HashMap<Long, Set<Opportunity>>();
		opps.put(Skin.SKIN_BUSINESS_ETRADER, Collections.newSetFromMap(new ConcurrentHashMap<Opportunity, Boolean>()));
		opps.put(Skin.SKIN_BUSINESS_ANYOPTION, Collections.newSetFromMap(new ConcurrentHashMap<Opportunity, Boolean>()));
		comparator = new LiveTrendsDataComparator();
		log.trace("Initializing LiveTrends done.");
	}
	
	public void investment(long oppId,
							double invId,
							double amount,
							long type,
							String amountForDisplay,
							String city,
							String countryId,
							String marketId,
							String productTypeId,
							long skinId,
							String level,
							Date time,
							String globeLat,
							String globeLong,
							Opportunity opp,
							long flooredAmount) {
		if (Long.parseLong(productTypeId) != OpportunityProductType.BINARY_TYPE_ID) {
			// skipping non-binary investments
			/* WARNING !!! METHOD EXIT POINT!!! */
			return;
		}
		
		long skinBusinessCase = Skin.SKIN_BUSINESS_ANYOPTION;
		if (skinId == Skin.SKIN_ETRADER) {
			skinBusinessCase = Skin.SKIN_BUSINESS_ETRADER;
		}
		opps.get(skinBusinessCase).add(opp);
		
		if (skinBusinessCase == Skin.SKIN_BUSINESS_ETRADER) {
			log.trace("LiveTrends (SKIN_BUSINESS_ETRADER) - oppId: " + oppId + " calls: " + opp.getCallsCountET() + " puts: " + opp.getPutsCountET());
		} else {
			log.trace("LiveTrends (SKIN_BUSINESS_ANYOPTION) - oppId: " + oppId + " calls: " + (opp.getCallsCount() - opp.getCallsCountET()) + " puts: " + (opp.getPutsCount() - opp.getPutsCountET()));
		}
		sortAndUpdate(opp, skinBusinessCase);
	}
	
	/**
	 * This method triggers re-sort of trends data and sends update to Lightstreamer
	 * if needed. If a specific trend is changed or added, it should be passed
	 * as parameter. If a trend is removed, the parameter should be <code>null</code>.
	 * 
	 * @param changedOppData the changed trends data or <code>null</code> if the data was removed.
	 */
	private void sortAndUpdate(Opportunity changedOppData, long skinBusinessCase) {
		List<Opportunity> list = new LinkedList<Opportunity>(opps.get(skinBusinessCase));
		Collections.sort(list, comparator);
		log.trace("LiveTrends - initial list length: " + list.size());
		
		Set<Long> passedMarkets = new HashSet<Long>();
		Iterator<Opportunity> iter = list.iterator();
		Opportunity currentOpp = null;
		while (iter.hasNext()) {
			currentOpp = iter.next();
			if (passedMarkets.contains(currentOpp.getMarket().getId())) {
				iter.remove();
				log.trace("Removed [" + currentOpp + "] because it has duplicated market");
			} else if (!currentOpp.isLessThanHourToClosing()) {
				iter.remove();
				log.trace("Removed [" + currentOpp + "] because it is not yet in profit line (less than one hour before closing)");
			} else {
				passedMarkets.add(currentOpp.getMarket().getId());
			}
		}
		
		log.trace("LiveTrends - list length after duplicate removal: " + list.size());
		
		if (null != lastUpdateHM) {
			Map<String, String> lastUpdate = lastUpdateHM.get(skinBusinessCase);
			if (null == lastUpdate) {
				log.trace("No previous Live Trends update");
				sendUpdate(list, skinBusinessCase);
				return;
			}
			
			for (int i = 0; i < 3; i++) {
				if (list.size() < i + 1) {
					if (!lastUpdate.get("LTT_OPP_ID" + (i + 1)).equals("0")) {
						log.trace("Opp on position " + (i + 1) + " changed.");
						sendUpdate(list, skinBusinessCase);
						return;
					}
				} else if (list.get(i).getId() != Long.parseLong(lastUpdate.get("LTT_OPP_ID" + (i + 1)))) {
					log.trace("Opp on position " + (i + 1) + " changed.");
					sendUpdate(list, skinBusinessCase);
					return;
				} else if (changedOppData != null && list.get(i).getId() == changedOppData.getId()) {
					if (!lastUpdate.get("LTT_TREND" + (i + 1)).equals(changedOppData.getCallsCountRate(skinBusinessCase))) {
						log.trace("Trend on position " + (i + 1) + " changed.");
						sendUpdate(list, skinBusinessCase);
						return;
					}
				}
			}
		}
		log.trace("Nothing changed.");
	}

	private void sendUpdate(List<Opportunity> list, long skinBusinessCase) {
		LiveJMSMessage msg = new LiveJMSMessage();
		msg.fields = new HashMap<String, String>();
		msg.fields.put("key", LiveJMSMessage.KEY_TRENDS + "_" + skinBusinessCase);
		for (int i = 0; i < 3; i++) {
			msg.fields.put("LTT_MARKET" + (i + 1), i < list.size() ? String.valueOf(list.get(i).getMarket().getId()) : "0");
			msg.fields.put("LTT_OPP_ID" + (i + 1), i < list.size() ? String.valueOf(list.get(i).getId()) : "0");
			msg.fields.put("LTT_TREND" + (i + 1), i < list.size() ? list.get(i).getCallsCountRate(skinBusinessCase) : "0");
		}
		sender.send(msg);
		lastUpdateHM.put(skinBusinessCase, msg.fields);
	}
	
	public void removeOpportunity(Opportunity opp) {
		log.trace("LiveTrends - remove opportunity: " + opp);
		Iterator<Long> iter = opps.keySet().iterator();
		long skinBusinessCase;
		while (iter.hasNext()) {
			skinBusinessCase = iter.next();
			if (opps.get(skinBusinessCase).remove(opp)) {
				sortAndUpdate(null, skinBusinessCase);
			}
		}
	}
	
	public void removeMarket(long marketId) {
		log.trace("Removing live trends for market [" + marketId + "] ...");
		Iterator<Long> iter = opps.keySet().iterator();
		long skinBusinessCase;
		while (iter.hasNext()) {
			skinBusinessCase = iter.next();
			boolean removed = false;
			Iterator<Opportunity> iterator = opps.get(skinBusinessCase).iterator();
			Opportunity opp;
			while (iterator.hasNext()) {
				opp = iterator.next();
				if (opp.getMarket().getId() == marketId) {
					iterator.remove();
					log.trace("Removed live trend [" + opp + "]");
					if (!removed) {
						removed = true;
					}
				}
			}
			
			if (removed) {
				sortAndUpdate(null, skinBusinessCase);
			}
		}
		
		log.trace("Removing live trends for market [" + marketId + "] done");
	}
	
	class LiveTrendsDataComparator implements Comparator<Opportunity> {
		public int compare(Opportunity o1, Opportunity o2) {
			// Descending order
			if (o1.getCallsCount() + o1.getPutsCount() > o2.getCallsCount() + o2.getPutsCount()) {
				return -1;
			} else if (o1.getCallsCount() + o1.getPutsCount() < o2.getCallsCount() + o2.getPutsCount()) {
				return 1;
			}
			return 0;
		}
	}
	
	public void sendSnapshot() {
		log.debug("Sending live trends snapshot...");
		Iterator<Long> iter = lastUpdateHM.keySet().iterator();
		long skinBusinessCase;
		while (iter.hasNext()) {
			skinBusinessCase = iter.next();
			Map<String, String> lastUpdate = lastUpdateHM.get(skinBusinessCase);
			if (lastUpdate != null) {
				LiveJMSMessage lastSentMsg = new LiveJMSMessage();
				lastSentMsg.fields = lastUpdate;
				sender.send(lastSentMsg);
				log.debug("Sending live trends snapshot done");
			} else {
				log.warn("Live trends snapshot is [null]. Nothing to send...");
			}
		}
	}
	
}