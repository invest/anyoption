package il.co.etrader.service.level;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.LiveGlobeCity;
import com.anyoption.common.beans.LiveJMSMessage;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.bl_vos.InvestmentType;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.service.level.Utils;
import com.anyoption.service.level.market.Market;

import il.co.etrader.bl_managers.ConfigManager;
import il.co.etrader.bl_vos.OpportunityProductType;
import il.co.etrader.helper.LiveHelper;
import il.co.etrader.service.level.managers.MarketsManager;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class LiveGlobe implements LiveInvestmentListener {
	private static final Logger log = Logger.getLogger(LiveGlobe.class);
	
	public static final long [] BINARY_ZERO_HUNDRED_INVESTMENT_TYPES = new long [] {InvestmentType.BUY, InvestmentType.SELL};
	public static final long [] NON_BINARY_ZERO_HUNDRED_INVESTMENT_TYPES = new long [] {InvestmentType.CALL, InvestmentType.PUT};
	
	private SenderThread sender;
	
	private long globeUpdateIntervalSeconds;
	private long globeMinAmountCents;
	private long globeQueueTTLMillis;
	
	private long fakeInvMinAmountCents;
	private long fakeInvMaxAmountCents;
	
	private PriorityBlockingQueue<LiveMessageWrapper> messageQueue;
	private ScheduledExecutorService executor;
	private volatile boolean running;
	private Random random;
	private Map<Long, String> fakeInvMarketMap;
	private HashMap<String, Market> markets;
	private Hashtable<Long, Boolean> openedMarkets;
	private ScheduledFuture<?> queueTask;
	private LiveMessageWrapper lastSentMsg;
	
	public LiveGlobe(SenderThread sender,
						HashMap<String, Market> markets,
						Hashtable<Long, Boolean> openedMarkets) throws SQLException {
		log.debug("Initializing live globe stream...");
		this.sender = sender;
		this.markets = markets;
		this.openedMarkets = openedMarkets;
		globeUpdateIntervalSeconds = ConfigManager.getLongParameter(
												ConfigManager.LIVEPAGE_GLOBE_UPDATE_INTERVAL_PARAM_NAME,
												ConfigManager.LIVEPAGE_GLOBE_UPDATE_INTERVAL_DEFAULT_VALUE);
		globeMinAmountCents = ConfigManager.getLongParameter(
												ConfigManager.LIVEPAGE_GLOBE_MIN_AMOUNT_PARAM_NAME,
												ConfigManager.LIVEPAGE_GLOBE_MIN_AMOUNT_DEFAULT_VALUE);
		globeQueueTTLMillis = ConfigManager.getLongParameter(
												ConfigManager.LIVEPAGE_GLOBE_QUEUE_TTL_PARAM_NAME,
												ConfigManager.LIVEPAGE_GLOBE_QUEUE_TTL_DEFAULT_VALUE) * 1000L;
		fakeInvMinAmountCents = ConfigManager.getLongParameter(
												ConfigManager.LIVEPAGE_GLOBE_FAKE_MIN_AMOUNT_PARAM_NAME,
												ConfigManager.LIVEPAGE_GLOBE_FAKE_MIN_AMOUNT_DEFAULT_VALUE);
		fakeInvMaxAmountCents = ConfigManager.getLongParameter(
												ConfigManager.LIVEPAGE_GLOBE_FAKE_MAX_AMOUNT_PARAM_NAME,
												ConfigManager.LIVEPAGE_GLOBE_FAKE_MAX_AMOUNT_DEFAULT_VALUE);
		
		messageQueue = new PriorityBlockingQueue<LiveMessageWrapper>(10, new LiveGlobeQueueComparator());
		
		random = new Random();
		
		fakeInvMarketMap = MarketsManager.getLiveGlobeFakeInvestmentMarkets();
		
		executor = Executors.newSingleThreadScheduledExecutor(new ThreadFactory() {
			@Override
			public Thread newThread(Runnable r) {
				Thread thread = new Thread(r);
				thread.setName("LIVE-GLOBE-MSG-HANDLER");
				return thread;
			}
		});
		queueTask = executor.scheduleWithFixedDelay(new QueueTask(),
													30 /* initial delay */,
													globeUpdateIntervalSeconds,
													TimeUnit.SECONDS);
		
		running = true;
		log.debug("Initializing live globe stream done");
	}
	
	public void stop() {
		running = false;
		queueTask.cancel(false); 
		executor.shutdown();
		messageQueue.clear();
	}
	
	public void investment(long oppId,
							double invId,
							double amount,
							long type,
							String amountForDisplay,
							String city,
							String countryId,
							String marketId,
							String productTypeId,
							long skinId,
							String level,
							Date time,
							String globeLat,
							String globeLong,
							Opportunity opp,
							long flooredAmount) {
		
		if (!running) {
			log.trace("Live Globe has shut down. Skipping investment [" + invId + "]");
			/* WARNING !!! METHOD EXIT POINT!!! */
			return;
		}
		
		if (amount < globeMinAmountCents) {
			log.trace("Investment [" + invId + "] has amount [" + amount + "] below minimum [" + globeMinAmountCents + "]. Ignoring it");
			/* WARNING !!! METHOD EXIT POINT!!! */
			return;
		}
		
		log.trace("Adding investment [" + invId + "] to live globe queue...");
		
		LiveJMSMessage msg = new LiveJMSMessage();
		msg.fields = new HashMap<String, String>();
		msg.fields.put("key", LiveJMSMessage.KEY_GLOBE);
		msg.fields.put("LI_CITY", city);
		msg.fields.put("LI_COUNTRY", countryId);
		msg.fields.put("LI_ASSET", marketId);
		msg.fields.put("LI_OPP_ID", Long.toString(oppId));
		msg.fields.put("LI_PROD_TYPE", productTypeId);
		msg.fields.put("LI_INV_TYPE", String.valueOf(type));
		msg.fields.put("LI_LEVEL", level);
		msg.fields.put("LI_AMOUNT", amountForDisplay);
		msg.fields.put("LI_TIME", String.valueOf(time.getTime()));
		msg.fields.put("LI_LAT", globeLat);
		msg.fields.put("LI_LONG", globeLong);
		
		LiveMessageWrapper msgWrapper = new LiveMessageWrapper();
		msgWrapper.setAmount(flooredAmount);
		msgWrapper.setTime(time);
		msgWrapper.setMessage(msg);
		msgWrapper.setOpportunityId(oppId);
		msgWrapper.setMarketId(Long.parseLong(marketId));
		
		messageQueue.add(msgWrapper);
		
		log.trace("Adding investment [" + invId + "] to live globe queue done");
	}
	
	private class QueueTask implements Runnable {
		@Override
		public void run() {
			log.trace("Running live globe queue..");
			runQueue();
			log.trace("Running live globe queue done");
		}
	}
	
	private void runQueue() {
		try {
			LiveMessageWrapper msgWrapper = null;
			
			while(true) {
				msgWrapper = messageQueue.poll();
				if (msgWrapper != null) {
					if (msgWrapper.getTime().getTime() + globeQueueTTLMillis >= System.currentTimeMillis()) {
						log.trace("Live globe investment [" + msgWrapper + "] valid, sending it to LS...");
						break;
					} else {
						log.trace("Live globe investment [" + msgWrapper + "] has expired, skipping it...");
						continue;
					}
				} else {
					log.trace("Live globe queue is empty, generating a fake investment...");
					msgWrapper = generateFakeMessage();
					log.trace("Sending fake live globe investment [" + msgWrapper + "] to LS");
					break;
				}
			}
			
			if (msgWrapper != null) {
				lastSentMsg = msgWrapper;
				sender.send(msgWrapper.getMessage());
			} else {
				log.error("No investment to send to live globe");
			}
		} catch (Exception e) {
			log.error("Unable to handle globe message queue", e);
		}
	}
	
	private LiveMessageWrapper generateFakeMessage() {
		long amount = getRandomAmont();
		LiveGlobeCity city = LiveHelper.getRandomCityForEtrader();
		MarketOpportunityCouple itemOppCouple = getRandomMarket();
		
		if (itemOppCouple == null) {
			/* WARNING !!! Method Exit point! */
			return null;
		}
		
		if (amount < fakeInvMinAmountCents
				|| amount > fakeInvMaxAmountCents
				|| city == null) {
			log.error("Unable to generate fake investment. Amount [" + amount + "], city [" + city + "], [" + itemOppCouple + "]");
			/* WARNING !!! Method Exit point! */
			return null;
		}
		
		Date time = new Date();
		
		LiveJMSMessage msg = new LiveJMSMessage();
		msg.fields = new HashMap<String, String>();
		msg.fields.put("key", LiveJMSMessage.KEY_GLOBE);
		msg.fields.put("LI_CITY", city.getCityName());
		msg.fields.put("LI_COUNTRY", Long.toString(city.getCountryId()));
		msg.fields.put("LI_LAT", city.getGlobeLatitude());
		msg.fields.put("LI_LONG", city.getGlobeLongtitude());
		msg.fields.put("LI_ASSET", Long.toString(itemOppCouple.getMarket().getMarketId()));
		msg.fields.put("LI_OPP_ID", Long.toString(itemOppCouple.getOpportunity().getId()));
		
		String productType;
		long [] invTypesArray;
		if (itemOppCouple.getMarket().isBinary0100Market()) {
			productType = Long.toString(OpportunityProductType.BINARY_ZERO_HUNDRED_TYPE_ID);
			invTypesArray = BINARY_ZERO_HUNDRED_INVESTMENT_TYPES;
		} else if (itemOppCouple.getMarket().isOptionPlusMarket()) {
			productType = Long.toString(OpportunityProductType.OPTION_PLUS_TYPE_ID);
			invTypesArray = NON_BINARY_ZERO_HUNDRED_INVESTMENT_TYPES;
		} else {
			productType = Long.toString(OpportunityProductType.BINARY_TYPE_ID);
			invTypesArray = NON_BINARY_ZERO_HUNDRED_INVESTMENT_TYPES;
		}
		
		msg.fields.put("LI_PROD_TYPE", productType);
		msg.fields.put("LI_INV_TYPE", Long.toString(invTypesArray[random.nextInt(invTypesArray.length)]));
		
		Market market = itemOppCouple.getMarket();
		msg.fields.put("LI_LEVEL", Utils.formatLevel(
											// TODO We may need to change the graph level in case globe is used in Etrader, too. 
											market.getGraphLevel(SkinGroup.ANYOPTION),
											itemOppCouple.getOpportunity().getMarket().getDecimalPoint().intValue()));
		
		String displayAmount = getDisplayAmount(amount, city.getCurrencyId());
		if (displayAmount == null) {
			log.error("Unable to generate display amount for amount [" + amount + "], currency [" + city.getCurrencyId() + "]");
			/* WARNING !!! Method Exit point! */
			return null;
		}
		msg.fields.put("LI_AMOUNT", displayAmount);
		
		msg.fields.put("LI_TIME", String.valueOf(time.getTime()));
		
		LiveMessageWrapper msgWrapper = new LiveMessageWrapper();
		msgWrapper.setAmount(amount);
		msgWrapper.setTime(time);
		msgWrapper.setMessage(msg);
		msgWrapper.setMarketId(itemOppCouple.getMarket().getMarketId());
		msgWrapper.setOpportunityId(itemOppCouple.getOpportunity().getId());
		
		return msgWrapper;
	}
	
	private String getDisplayAmount(long amount, long currencyId) {
		double rateBaseToGiven = CurrencyRatesManagerBase.getInvestmentsRate(currencyId);
		if (rateBaseToGiven <= 0.0D) {
			log.error("Could not get currency [" + currencyId + "] to USD");
			/*
			 * WARNING !!! METHOD EXIT POINT !!!
			 */
			return null;
		}
		rateBaseToGiven = 1.0D / rateBaseToGiven;
		return CommonUtil.displayAmount(Math.round(amount * rateBaseToGiven),
										true,
										currencyId,
										false,
										ConstantsBase.DISPLAY_FORMAT_LIVE_PAGE);
	}
	
	private long getRandomAmont() {
		int maxMinusMin = (int) (fakeInvMaxAmountCents - fakeInvMinAmountCents);
		if (maxMinusMin > 0) {
			return random.nextInt(maxMinusMin) + fakeInvMinAmountCents;
		} else {
			log.error("Wrong configuration for max [" + fakeInvMaxAmountCents
						+ "] and min [" + fakeInvMinAmountCents + "] fake investment amount. Returning min amount - 1");
			return fakeInvMinAmountCents - 1;
		}
	}
	
	
	private MarketOpportunityCouple getRandomMarket() {
		MarketOpportunityCouple couple = null;
		
		synchronized (markets) {
			List<Market> tmpMarkets = new ArrayList<Market>();
			for (Long marketId : fakeInvMarketMap.keySet()) {
				if (openedMarkets.containsKey(marketId)) {
					Market item = markets.get(fakeInvMarketMap.get(marketId));
					if (item != null && item.hasOpenNonOneTouchOpportunities()) {
						tmpMarkets.add(item);
					}
				}
			}
			
			if (tmpMarkets.size() > 0) {
				Market item = tmpMarkets.get(random.nextInt(tmpMarkets.size()));
				couple = new MarketOpportunityCouple();
				couple.setMarket(item);
				couple.setOpportunity(item.getFirstOpenOpportunity(
												Opportunity.SCHEDULED_HOURLY,
												Opportunity.SCHEDULED_DAYLY,
												Opportunity.SCHEDULED_WEEKLY,
												Opportunity.SCHEDULED_MONTHLY,
												Opportunity.SCHEDULED_QUARTERLY));
			} else {
				log.info("No open markets for fake investment available");
			}
		}
		
		return couple;
	}
	
	private class MarketOpportunityCouple {
		private Market market;
		private Opportunity opportunity;
		/**
		 * @return the market
		 */
		public Market getMarket() {
			return market;
		}
		/**
		 * @param market the market to set
		 */
		public void setMarket(Market market) {
			this.market = market;
		}
		/**
		 * @return the opportunity
		 */
		public Opportunity getOpportunity() {
			return opportunity;
		}
		/**
		 * @param opportunity the opportunity to set
		 */
		public void setOpportunity(Opportunity opportunity) {
			this.opportunity = opportunity;
		}
		
		@Override
		public String toString() {
			return "MarketOpportunityCouple [market=" + market
					+ ", opportunity=" + opportunity + "]";
		}
		
	}
	
	public void sendSnapshot() {
		log.debug("Sending live globe snapshot...");
		if (lastSentMsg != null) {
			sender.send(lastSentMsg.getMessage());
			log.debug("Sending live globe snapshot done");
		} else {
			log.warn("Live globe snapshot is [null]. Nothing to send...");
		}
	}
	
	public void removeOpportunity(long oppId) {
		log.trace("Removing live globe investments for oppotunity [" + oppId + "] ...");
		
		Iterator<LiveMessageWrapper> iterator = messageQueue.iterator();
		
		while (iterator.hasNext()) {
			LiveMessageWrapper msgWrapper = iterator.next();
			if (msgWrapper.getOpportunityId() == oppId) {
				iterator.remove();
				log.trace("Removed investment [" + msgWrapper + "] from live globe queue");
			}
		}
		
		if (lastSentMsg != null && lastSentMsg.getOpportunityId() == oppId) {
			log.trace("Last sent live globe investment was removed - running queue");
			runQueue();
		}
		log.trace("Removing live globe investments for oppotunity [" + oppId + "] done");
	}
	
	public void removeMarket(long marketId) {
		log.trace("Removing live globe investments for market [" + marketId + "] ...");
		Iterator<LiveMessageWrapper> iterator = messageQueue.iterator();
		
		while (iterator.hasNext()) {
			LiveMessageWrapper msgWrapper = iterator.next();
			if (msgWrapper.getMarketId() == marketId) {
				iterator.remove();
				log.trace("Removed investment [" + msgWrapper + "] from live globe queue");
			}
		}
		
		if (lastSentMsg != null && lastSentMsg.getMarketId() == marketId) {
			log.trace("Last sent live globe investment was removed - running queue");
			runQueue();
		}
		log.trace("Removing live globe investments for market [" + marketId + "] done");
	}

}