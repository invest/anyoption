package il.co.etrader.service.level;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.util.GoldenMinute.GoldenMinutePeriod;
import com.anyoption.common.util.GoldenMinute.GoldenMinuteType;

public class GoldenMinutesJob implements Runnable {
    private static Logger log = Logger.getLogger(GoldenMinutesJob.class);

    GoldenMinutePeriod gmPeriod;
    
    private int goldenMinStart;
    private int goldenMinEnd;
    
    public GoldenMinutesJob(GoldenMinutePeriod gmPeriod, int goldenMinStart, int goldenMinEnd) {
        this.goldenMinStart = goldenMinStart;
        this.goldenMinEnd = goldenMinEnd;
        this.gmPeriod = gmPeriod;
    }
    
    public void run() {
        Thread.currentThread().setName("GoldenMinutesJob");
        log.info("GoldenMinutesJob start: " + gmPeriod.type.name() + " for expiry at: " + gmPeriod.expiry.name());
        try {
            ArrayList<Opportunity> l = LevelService.getInstance().getGoldenMinutesOpportunities(gmPeriod.expiry);
            log.debug("Levels collected count: " + l.size());
            StringBuilder sb = new StringBuilder();
            for (Opportunity o : l) {
            	sb.append("id=").append(o.getId()).append(" level=").append(o.getGmLevel()).append(", ");
            }
            log.debug("Levels collected: " + sb.toString());
            
            if (gmPeriod.type == GoldenMinuteType.STANDARD) {
            	OpportunitiesManager.writeOpportunitiesGMLevels(l);
            	log.debug("Levels for STANDARD written");
             	InvestmentsManager.qualifyInvestmentsForInsurances(goldenMinStart, goldenMinEnd);
             	log.debug("qualified for RF end");
            } else if (gmPeriod.type == GoldenMinuteType.ADDITIONAL) {
            	OpportunitiesManager.writeOpportunitiesGMLevelsAdditional(l);
            	log.debug("Levels for ADDITIONAL written");
            	InvestmentsManager.qualifyInvestmentsForAdditionalInsurance(goldenMinStart, goldenMinEnd);
            	log.debug("qualified for additional RF end");
            }
            
            LevelService.getInstance().getSender().send("startGM_" + gmPeriod.id);
        } catch (Exception e) {
            log.error("Problem starting Golden Minutes.", e);
        }
        log.info("GoldenMinutesJob end");
    }
}