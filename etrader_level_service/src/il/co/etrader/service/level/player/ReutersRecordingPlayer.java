package il.co.etrader.service.level.player;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import com.anyoption.common.jms.ConnectionLoop;
import com.anyoption.common.jms.HeartbeatMessage;
import com.anyoption.common.jms.JMSHandler;
import com.anyoption.common.jms.ifc.ExtendedMessageListener;
import com.anyoption.service.datasource.DataUpdate;
import com.anyoption.service.level.formula.FormulaConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.recorder.ReutersFeedRecorder;
import il.co.etrader.service.level.reuters.ReutersService;

public class ReutersRecordingPlayer implements ReutersRecordingPlayerMBean, ExtendedMessageListener {
    private static final Logger log = Logger.getLogger(ReutersRecordingPlayer.class);
    
    private static final long PROCESSING_DELAY_MARGIN = 1 * 60 * 60 * 1000; // 1 hour
    
    // JMS properties
    private String initialContextFactory;
    private String providerURL;
    private String connectionFactoryName;
    private String queueName;
    private String topicName;
    private int msgPoolSize;
    private int recoveryPause;

    // settings
    private String recordingFileName;

    //this object handles comunications with JMS. Hides the use of Session, Connections, Publishers etc..
    private JMSHandler jmsHandler;
    private HeartbeatThread heartbeat;
    private ConnectionLoopTPQR connector;
    private SenderThread sender;
    private RecordingPlayer player;

    // state
    private long lifeId;
    private InputStream is;
    private InputStreamReader isr;
    private BufferedReader br;
    private SimpleDateFormat recordingTimeFormat;
    private Date recordingDate;
    private SimpleDateFormat reutersDateFieldFormat;

    private Map<String, DataUpdate> cache;

    public void startService() {
        log.info("ReutersRecordingPlayer ========== startService begin");
        startupJMS();
        cache = new HashMap<String, DataUpdate>();
        openRecording(true);
        player = new RecordingPlayer();
        player.start();
        log.info("ReutersRecordingPlayer ========== startService end");
    }
    
    public void stopService() {
        log.info("ReutersRecordingPlayer ========== stopService begin");
        player.stopRecordingPlayer();
        closeRecording();
        stopJMS();
        cache = null;
        log.info("ReutersRecordingPlayer ========== stopService end");
    }

    private void startupJMS() {
        log.info("ReutersRecordingPlayer ========== startupJMS");

        lifeId = System.currentTimeMillis();

        //instantiate a JMSHandler that will handle LightStreamer updates sending and commands Queue receiving
        jmsHandler = new JMSHandler("RS", initialContextFactory, providerURL, connectionFactoryName, queueName, connectionFactoryName, topicName);
        jmsHandler.setListener(this);

        //start the loop that tries to connect to JMS
        connector = new ConnectionLoopTPQR(jmsHandler, recoveryPause);
        connector.start();

        sender = new SenderThread(jmsHandler, false);
        sender.start();

        heartbeat = new HeartbeatThread();
        heartbeat.start();
    }
    
    private void stopJMS() {
        log.info("ReutersRecordingPlayer ========== stopJMS");

        heartbeat.stopHeartbeat();
        heartbeat = null;

        if (null != sender) {
            sender.stopSenderThread();
        }
        sender = null;

        if (null != connector) {
            connector.abort();
        }
        connector = null;

        jmsHandler.close();
        jmsHandler = null;
    }

    private void openRecording(boolean rewind) {
        try {
            is = new FileInputStream(recordingFileName);
            isr = new InputStreamReader(is);
            br = new BufferedReader(isr);
        } catch (IOException ioe) {
            log.error("Can't open recording.", ioe);
        }
        recordingTimeFormat = new SimpleDateFormat(ReutersFeedRecorder.RECORDING_TIME_PATTERN);
        reutersDateFieldFormat = new SimpleDateFormat(ReutersService.REUTERS_DATE_FIELD_PATTERN, Locale.US);
        try {
            if (rewind) {
                log.info("Rewind...");
                // Assumption is made that the recording file contains data about 1 day
                String line = null;
                Date updateTime = null;
                while ((line = br.readLine()) != null) {
                    if (null == recordingDate) {
                        recordingDate = extractRecordingDate(line, recordingTimeFormat);
                    }
                    StringTokenizer st = new StringTokenizer(line, "|");
                    updateTime = getUpdateTime(st, recordingTimeFormat, rewind);
                    cacheUpdate(ReutersRecordingPlayer.getUpdate(st));
                    if (updateTime.getTime() < System.currentTimeMillis()) {
                        continue;
                    } else {
                        // we will lose this update but that shouldn't be a big deal
                        break;
                    }
                }
            }
        } catch (Exception e) {
            log.error("Can't move current position.", e);
        }
    }
    
    private Date extractRecordingDate(String line, SimpleDateFormat sdf) throws ParseException {
        StringTokenizer st = new StringTokenizer(line, "|");
        Date lineDate = sdf.parse(st.nextToken());
        Calendar cal = Calendar.getInstance();
        cal.setTime(lineDate);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
    
    private void cacheUpdate(DataUpdate update) {
        if (null != update.getFields()) {
            updateDateFields(update);
        }
        
        DataUpdate cacheUpdate = cache.get(update.getDataSubscription());
        if (null == cacheUpdate || null == cacheUpdate.getFields()) {
            cache.put(update.getDataSubscription(), update);
        } else {
            if (null != update.getFields()) {
                cacheUpdate.getFields().putAll(update.getFields());
            }
            cacheUpdate.setStateOK(update.isStateOK());
        }
    }
    
    private void updateDateFields(DataUpdate update) {
        String sd = null;
        Date dd = null;
        String ud = null;
        for (String key : update.getFields().keySet()) {
            if (ReutersService.dateFields.keySet().contains(key)) {
                try {
                    sd = (String) update.getFields().get(key);
                    dd = reutersDateFieldFormat.parse(sd);
                    if (dd.getTime() < recordingDate.getTime()) {
                        // yesterday
                        ud = reutersDateFieldFormat.format(new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000));
                    } else {
                        ud = reutersDateFieldFormat.format(new Date());
                    }
                    update.getFields().put(key, ud);
                } catch (Exception e) {
                    log.trace("Can't update date field " + key + " value <" + sd + ">");
                }
            }
        }
    }
    
    public static Date getUpdateTime(StringTokenizer st, SimpleDateFormat sdf, boolean rewind) throws Exception {
        Date updateTime = sdf.parse(st.nextToken());
        Calendar c = Calendar.getInstance();
        Date d = c.getTime();
        int year = c.get(Calendar.YEAR);
        int day = c.get(Calendar.DAY_OF_YEAR);
        c.setTime(updateTime);
        c.set(Calendar.YEAR, year);
        c.set(Calendar.DAY_OF_YEAR, day);
        if (!rewind && d.getTime() - c.getTime().getTime() > PROCESSING_DELAY_MARGIN) {
            // after a reopening of the file in the end of the day the time of the update will be "in the beginning of the day"
            // while we may still be in the "end of the day" (the period after the last update in the recording before mid night
            // so we should move the update for the next day
            c.add(Calendar.DAY_OF_YEAR, 1);
        }
        return c.getTime();
    }
    
    public static DataUpdate getUpdate(StringTokenizer st) {
        DataUpdate update = new DataUpdate(st.nextToken());
        update.setStateOK(st.nextToken().equals("true"));
        String field = null;
        String kv[] = null;
        while (st.hasMoreTokens()) {
            if (null == update.getFields()) {
                update.setFields(new HashMap<String, Object>());
            }
            field = st.nextToken();
            kv = field.split("=");
            if (ReutersService.isDoubleField(kv[0])) {
                update.getFields().put(kv[0], Double.valueOf(kv[1]));
            } else {
                update.getFields().put(kv[0], kv[1]);
            }
        }
        return update;
    }
    
    private void closeRecording() {
        recordingTimeFormat = null;
//        recordingDate = null;
        reutersDateFieldFormat = null;
        if (null != br) {
            try {
                br.close();
            } catch (IOException ioe) {
                log.error("Can't close.", ioe);
            }
        }
        if (null != isr) {
            try {
                isr.close();
            } catch (IOException ioe) {
                log.error("Can't close.", ioe);
            }
        }
        if (null != is) {
            try {
                is.close();
            } catch (IOException ioe) {
                log.error("Can't close.", ioe);
            }
        }
    }

    /**
     * receive messages from JMSHandler
     */
    public void onMessage(Message message) {
        log.debug("ReutersRecordingPlayer Message received: processing...");
        String feedMsg = null;
        try {
            TextMessage textMessage = (TextMessage) message;
            feedMsg = textMessage.getText();
        } catch (Exception e) {
            log.error("", e);
            return;
        }
        log.info("TextMessage received: " + feedMsg);
        String[] cmd = feedMsg.split("_");
        if (cmd[0].equals("subscribe")) {
        	if (cmd[1].startsWith("{")) {
        		try {
	        		Gson gson = new GsonBuilder().serializeNulls().create();
	        		Map<String, String> config = gson.fromJson(cmd[1], new TypeToken<Map<String, String>>(){}.getType());
	        		cmd[1] = config.get(FormulaConfig.SUBSCRIPTION_CONFIG_IDENTIFIER);
        		} catch (JsonSyntaxException e) {
        			log.error("Unable to parse command: " + cmd[1], e);
        		}
        	}
            DataUpdate snapshot = cache.get(cmd[1]);
            if (null != snapshot) {
                log.trace("Sending snapshot for " + cmd[1]);
                sender.send(snapshot);
            } else {
                log.warn("No snapshot found for " + cmd[1]);
            }
            return;
        } else if (cmd[0].equals("unsubscribe")) {
            // do nothing
            return;
        }
        log.warn("Unknown message. Message: " + feedMsg);
    }

    public void onException(JMSException jmse) {
        jmsHandler.reset();
        connector = new ConnectionLoopTPQR(jmsHandler, recoveryPause);
        connector.start();
    }

    public String getConnectionFactoryName() {
        return connectionFactoryName;
    }
    
    public void setConnectionFactoryName(String connectionFactoryName) {
        this.connectionFactoryName = connectionFactoryName;
    }

    public String getInitialContextFactory() {
        return initialContextFactory;
    }

    public void setInitialContextFactory(String initialContextFactory) {
        this.initialContextFactory = initialContextFactory;
    }

    public String getProviderURL() {
        return providerURL;
    }

    public void setProviderURL(String providerURL) {
        this.providerURL = providerURL;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public int getMsgPoolSize() {
        return msgPoolSize;
    }

    public void setMsgPoolSize(int msgPoolSize) {
        this.msgPoolSize = msgPoolSize;
    }

    public int getRecoveryPause() {
        return recoveryPause;
    }

    public void setRecoveryPause(int recoveryPause) {
        this.recoveryPause = recoveryPause;
    }

    public String getRecordingFileName() {
        return recordingFileName;
    }

    public void setRecordingFileName(String recordingFileName) {
        this.recordingFileName = recordingFileName;
    }
    
    private class RecordingPlayer extends Thread {
        private boolean running;
        
        public void run() {
            Thread.currentThread().setName("RecordingPlayer");
            log.info("RecordingPlayer start");
            running = true;
            String line = null;
            StringTokenizer st = null;
            Date updateTime = null;
            long timeToUpdate = 0;
            DataUpdate update = null;
            do {
                try {
                    line = br.readLine();
                    log.trace("Readed line: " + line);
                    if (null == line) {
                        // We've reached the end of the recording - most likely this is the end of the day.
                        // Reopen the recording file to start again...
                        log.info("End of the day. Reopening the recording...");
                        closeRecording();
                        openRecording(false);
                        continue;
                    }
                    
                    st = new StringTokenizer(line, "|");
                    updateTime = getUpdateTime(st, recordingTimeFormat, false);
                    
                    timeToUpdate = updateTime.getTime() - System.currentTimeMillis();
                    log.trace("updateTime: " + updateTime + " timeToUpdate: " + timeToUpdate);
                    if (timeToUpdate > 0) {
                        log.trace("Waiting for " + updateTime);
                        synchronized (this) {
                            this.wait(timeToUpdate);
                        }
                    }
                    
                    update = getUpdate(st);
                    cacheUpdate(update);
                    sender.send(update);
                } catch (Exception e) {
                    log.error("Error in RecordingPlayer", e);
                }
            } while (running);
            log.info("RecordingPlayer end");
        }
        
        public void wakeUp() {
            synchronized (this) {
                try {
                    this.notify();
                } catch (Exception e) {
                    log.error("Can't wake up", e);
                }
            }
        }
        
        public void stopRecordingPlayer() {
            running = false;
            wakeUp();
        }
    }

    //////////////////////RecoveryThread
    private class ConnectionLoopTPQR extends ConnectionLoop {

        public ConnectionLoopTPQR(JMSHandler jmsHandler, int recoveryPause) {
            super(jmsHandler, recoveryPause);
            threadName = "ConnectionTPQR";
        }

        protected void onConnectionCall() {
            if (log.isInfoEnabled()) {
                log.info("onConnectionCall");
            }
            return;
        }

        protected void connectionCall() throws JMSException, NamingException {
            //initialize TopicPublisher and QueueReceiver
            if (log.isInfoEnabled()) {
                log.info("connectionCall");
            }
            if (null != jmsHandler.getTopicName()) {
                jmsHandler.initTopicPublisher(msgPoolSize);
            }
            if (null != jmsHandler.getQueueName()) {
                jmsHandler.initQueueReceiver();
            }
        }
    }

    //////////////////////HeartbeatThread
    private class HeartbeatThread extends Thread {
        private HeartbeatMessage fixedMessage = new HeartbeatMessage(lifeId);
        private boolean running;
//        private long lastJingleTime;

        public void run() {
            Thread.currentThread().setName("Heartbeat");
            running = true;
            while (running) {
//                lastJingleTime = System.currentTimeMillis();
                try {
                    //publish the update to JMS
                    jmsHandler.publishMessage(fixedMessage);
                } catch (JMSException je) {
                    log.error("Unable to send message - JMSException:" + je.getMessage());
                }
                if (log.isTraceEnabled()) {
                    log.trace("Heartbeat sent: " + fixedMessage.lifeId);
                }

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
            }
            log.warn("HeartbeatThread ends. running = " + running);
        }

        public void stopHeartbeat() {
            running = false;
        }
//
//        public boolean isWorking() {
//            return System.currentTimeMillis() - lastJingleTime < 2000;
//        }
    }
}