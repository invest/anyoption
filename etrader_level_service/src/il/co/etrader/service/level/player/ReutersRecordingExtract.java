package il.co.etrader.service.level.player;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.anyoption.service.datasource.DataUpdate;

import il.co.etrader.service.level.recorder.ReutersFeedRecorder;

public class ReutersRecordingExtract {
    private static final Logger log = Logger.getLogger(ReutersRecordingExtract.class);
    
    private InputStream is;
    private InputStreamReader isr;
    private BufferedReader br;

    private PrintWriter pw;
    
    private Map<String, DataUpdate> cache;
    
    public ReutersRecordingExtract() {
        cache = new HashMap<String, DataUpdate>();
    }
    
    private void openRecording(String recordingFileName) {
        try {
            is = new FileInputStream(recordingFileName);
            isr = new InputStreamReader(is);
            br = new BufferedReader(isr);
        } catch (IOException ioe) {
            log.error("Can't open recording.", ioe);
        }
    }
    
    private void openExtract(String extractFileName) {
        try {
            pw = new PrintWriter(new File(extractFileName));
        } catch (IOException ioe) {
            log.error("Can't open recording.", ioe);
        }
    }
    
    private void extract(String extractDate) {
        try {
            String line = null;
            StringTokenizer st = null;
            Date updateTime = null;
            SimpleDateFormat sdf = new SimpleDateFormat(ReutersFeedRecorder.RECORDING_TIME_PATTERN);
            long extractStart = sdf.parse(extractDate + " 00:00:00,000").getTime();
            long extractEnd = extractStart + 24 * 60 * 60 * 1000; // 1 day
            DataUpdate update = null;
            DataUpdate cacheUpdate = null;
            boolean caching = true;
            while ((line = br.readLine()) != null) {
                st = new StringTokenizer(line, "|");
                updateTime = sdf.parse(st.nextToken());
                
                if (updateTime.getTime() < extractStart) {
                    // we are still caching prev day updates
                    update = ReutersRecordingPlayer.getUpdate(st);
                    cacheUpdate = cache.get(update.getDataSubscription());
                    if (null == cacheUpdate || null == cacheUpdate.getFields()) {
                        cache.put(update.getDataSubscription(), update);
                    } else {
                        if (null != update.getFields()) {
                            cacheUpdate.getFields().putAll(update.getFields());
                        }
                        cacheUpdate.setStateOK(update.isStateOK());
                    }
                } else if (caching) {
                    // this is the first update to extract. flush the cache first
                    String time = sdf.format(updateTime);
                    for (DataUpdate u : cache.values()) {
                        pw.println(u.toRecordingString(time));
                    }
                    pw.println(line);
                    caching = false;
                } else if (updateTime.getTime() > extractEnd) {
                    break;
                } else {
                    pw.println(line);
                }
            }
        } catch (Exception e) {
            log.error("Error extracting recording", e);
        }
    }
    
    private void closeRecording() {
        if (null != br) {
            try {
                br.close();
            } catch (IOException ioe) {
                log.error("Can't close.", ioe);
            }
        }
        if (null != isr) {
            try {
                isr.close();
            } catch (IOException ioe) {
                log.error("Can't close.", ioe);
            }
        }
        if (null != is) {
            try {
                is.close();
            } catch (IOException ioe) {
                log.error("Can't close.", ioe);
            }
        }
    }
    
    private void closeExtract() {
        if (null != pw) {
            pw.close();
        }
    }
    
    public static void main(String[] args) {
        ReutersRecordingExtract rre = new ReutersRecordingExtract();
        rre.openRecording(args[0]);
        rre.openExtract(args[1]);
        rre.extract(args[2]);
        rre.closeRecording();
        rre.closeExtract();
    }
}