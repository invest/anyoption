package il.co.etrader.service.level.player;

public interface ReutersRecordingPlayerMXBean {
    public String getInitialContextFactory();
    public void setInitialContextFactory(String initialContextFactory);

    public String getProviderURL();
    public void setProviderURL(String providerURL);

    public String getConnectionFactoryName();
    public void setConnectionFactoryName(String connectionFactoryName);

    public String getQueueName();
    public void setQueueName(String queueName);

    public String getTopicName();
    public void setTopicName(String topicName);

    public int getMsgPoolSize();
    public void setMsgPoolSize(int msgPoolSize);

    public int getRecoveryPause();
    public void setRecoveryPause(int recoveryPause);

    public String getRecordingFileName();
    public void setRecordingFileName(String fileName);
}