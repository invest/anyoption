package il.co.etrader.service.level;

import java.util.Date;

import com.anyoption.common.beans.Opportunity;

public interface LiveInvestmentListener {
	public void investment(
			long oppId,
			double invId,
			double amount,
			long type,
			String amountForDisplay,
			String city,
			String countryId,
			String marketId,
			String productTypeId,
			long skinId,
			String level,
			Date time,
			String globeLat,
			String globeLong,
			Opportunity opp,
			long flooredAmount);
}