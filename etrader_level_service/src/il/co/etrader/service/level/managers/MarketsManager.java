package il.co.etrader.service.level.managers;



import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.MarketConfig;
import com.anyoption.common.beans.MarketRate;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.service.level.market.Market;

import il.co.etrader.service.level.daos.MarketsDAO;

/**
 * Service markets manager.
 *
 * @author Tony
 */
public class MarketsManager extends BaseBLManager {
	
	protected static Logger log = Logger.getLogger(MarketsManager.class);
    /**
     * Write a list of markets rates to the db.
     *
     * @param marketRates
     * @throws SQLException
     */
    public static void writeMarketsRates(ArrayList<MarketRate> marketRates) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            Set<Long> marketIds = MarketsDAO.insertMarketRates(conn, marketRates);
            conn.commit();
            conn.setAutoCommit(true);
            MarketsDAO.deleteMarketRates(conn, marketIds);
        } catch (SQLException e) {
        	if (!conn.getAutoCommit()) {
	        	log.error("Unable to write market rates. Rolling-back transaction", e);
				conn.rollback();
        	}
			throw e;
		} finally {
			conn.setAutoCommit(true);
			closeConnection(conn);
		}
    }
    
    /**
     * Delete all records from MARKET_RATES table.
     * 
     * @throws SQLException
     */
    public static void deleteMarketsRates() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            MarketsDAO.deleteMarketsRates(conn);
        } finally {
            closeConnection(conn);
        }
    }

	public static void loadSkinsPriorities(Market market) throws SQLException {
		Connection conn = null;
        try {
            conn = getConnection();
            market.setPriorities(MarketsDAO.getSkinsPrioritiesById(conn, market.getMarketId()));
        } finally {
            closeConnection(conn);
        }
	}

	public static Map<Long, String> getLiveGlobeFakeInvestmentMarkets() throws SQLException {
		Connection conn = null;
        try {
            conn = getConnection();
            return MarketsDAO.getLiveGlobeFakeInvestmentMarkets(conn);
        } finally {
            closeConnection(conn);
        }
	}

	public static MarketConfig getMarketConfig(long marketId, long dataServiceId) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return MarketsDAO.getMarketConfig(conn, marketId, dataServiceId);
		} finally {
			closeConnection(conn);
		}
	}

	public static long getDataServiceId(String dataServiceClassName) {
		Connection conn = null;
		try {
			conn = getConnection();
			return MarketsDAO.getDataServiceId(conn, dataServiceClassName);
		} catch(SQLException e) {
			log.error("Unable to load data service id", e);
			return 0l;
		} finally {
			closeConnection(conn);
		}
	}
}