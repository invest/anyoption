package il.co.etrader.service.level.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.bl_vos.Skins;
import il.co.etrader.service.level.daos.SkinsDAO;

/**
 * Service skins manager.
 *
 * @author Tony
 */
public class SkinsManager extends BaseBLManager {
    private static ArrayList<Skins> skins = null;

    public static ArrayList<Skins> getAllSkins() throws SQLException {
        if (null == skins) {
            Connection conn = null;
            try {
                conn = getConnection();
                skins = SkinsDAO.getAll(conn);
            } finally {
                closeConnection(conn);
            }
        }
        return skins;
    }
}