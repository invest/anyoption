package il.co.etrader.service.level.recorder;

public interface ReutersFeedRecorderMBean {
    public String getInitialContextFactory();
    public void setInitialContextFactory(String initialContextFactory);

    public String getProviderURL();
    public void setProviderURL(String providerURL);

    public String getConnectionFactoryName();
    public void setConnectionFactoryName(String connectionFactoryName);

    public String getTopicName();
    public void setTopicName(String topicName);

    public int getRecoveryPause();
    public void setRecoveryPause(int recoveryPause);
    
    public String getRecordingFileName();
    public void setRecordingFileName(String fileName);

    public void startService();
    public void stopService();
}