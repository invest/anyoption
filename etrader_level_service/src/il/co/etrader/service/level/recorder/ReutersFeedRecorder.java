package il.co.etrader.service.level.recorder;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import com.anyoption.common.jms.ConnectionLoop;
import com.anyoption.common.jms.HeartbeatMessage;
import com.anyoption.common.jms.JMSHandler;
import com.anyoption.common.jms.ifc.ExtendedMessageListener;
import com.anyoption.service.datasource.DataUpdate;

public class ReutersFeedRecorder implements ReutersFeedRecorderMBean, ExtendedMessageListener {
    private static final Logger log = Logger.getLogger(ReutersFeedRecorder.class);
    
    public static final String RECORDING_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss,SSS";
    
    // JMS properties
    private String initialContextFactory;
    private String providerURL;
    private String connectionFactoryName;
    private String topicName;
    private int recoveryPause;

    // settings
    private String recordingFileName;
    
    //this object handles comunications with JMS. Hides the use of Session, Connections, Publishers etc..
    private JMSHandler jmsHandler;
    private ConnectionLoopTS connector;

    private FileOutputStream fos;
    private OutputStreamWriter osw;
    private PrintWriter pw;
    private SimpleDateFormat updateTimeFormat;
    
    public void startService() {
        log.info("ReutersFeedRecorder ========== startService begin");

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd");
            String fileName = recordingFileName + "_" + sdf.format(new Date()) + ".txt";
            fos = new FileOutputStream(fileName);
            osw = new OutputStreamWriter(fos);
            pw = new PrintWriter(osw);
            
            updateTimeFormat = new SimpleDateFormat(RECORDING_TIME_PATTERN);
        
            //instantiate a JMSHandler that will handle LightStreamer updates sending and commands Queue receiving
            jmsHandler = new JMSHandler("RR", initialContextFactory, providerURL, connectionFactoryName, null, connectionFactoryName, topicName);
            jmsHandler.setListener(this);
    
            //start the loop that tries to connect to JMS
            connector = new ConnectionLoopTS(jmsHandler, recoveryPause);
            connector.start();
        } catch (Exception e) {
            log.error("Can't start ReutersFeedRecorder.", e);
        }

        log.info("ReutersFeedRecorder ========== startService end");
    }
    
    public void stopService() {
        log.info("ReutersFeedRecorder ========== stopService begin");

        if (null != connector) {
            connector.abort();
            connector = null;
        }

        if (null != jmsHandler) {
            jmsHandler.close();
            jmsHandler = null;
        }
        
        if (null != pw) {
            pw.close();
            pw = null;
        }
        
        updateTimeFormat = null;
        
        if (null != osw) {
            try {
                osw.close();
            } catch (Exception e) {
                log.error("Can't close recording file.", e);
            }
            osw = null;
        }
        
        if (null != fos) {
            try {
                fos.close();
            } catch (Exception e) {
                log.error("Can't close recording file.", e);
            }
            fos = null;
        }

        log.info("ReutersFeedRecorder ========== stopService end");
    }

    /**
     * receive messages from JMSHandler
     */
    public void onMessage(Message message) {
        log.debug("ReutersFeedRecorder Message received: processing...");
        try {
            if (message instanceof ObjectMessage) {
                Object obj = ((ObjectMessage) message).getObject();
                if (obj instanceof DataUpdate) {
                    log.info("ReutersUpdate");
                    update((DataUpdate) obj);
                } else if (obj instanceof HeartbeatMessage) {
                    log.info("HeartbeatMessage");
                    // TODO: handle Heartbeats
                }
            } else {
                log.warn("Unknown message: " + message + " of type: " + message.getClass().getCanonicalName());
            }
        } catch (Exception e) {
            log.error("Error processing JMS message.", e);
        }
    }

    public void onException(JMSException jmse) {
        jmsHandler.reset();
        connector = new ConnectionLoopTS(jmsHandler, recoveryPause);
        connector.start();
    }

    private void update(DataUpdate update) {
        log.debug("recording update - itemName: " + update.getDataSubscription());
        pw.println(update.toRecordingString(updateTimeFormat.format(new Date())));
    }

    public String getInitialContextFactory() {
        return initialContextFactory;
    }

    public void setInitialContextFactory(String initialContextFactory) {
        this.initialContextFactory = initialContextFactory;
    }

    public String getProviderURL() {
        return providerURL;
    }

    public void setProviderURL(String providerURL) {
        this.providerURL = providerURL;
    }

    public String getConnectionFactoryName() {
        return connectionFactoryName;
    }

    public void setConnectionFactoryName(String connectionFactoryName) {
        this.connectionFactoryName = connectionFactoryName;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public int getRecoveryPause() {
        return recoveryPause;
    }

    public void setRecoveryPause(int recoveryPause) {
        this.recoveryPause = recoveryPause;
    }

    public String getRecordingFileName() {
        return recordingFileName;
    }

    public void setRecordingFileName(String recordingFileName) {
        this.recordingFileName = recordingFileName;
    }

    private class ConnectionLoopTS extends ConnectionLoop {
        public ConnectionLoopTS(JMSHandler jmsHandler, int recoveryPause) {
            super(jmsHandler, recoveryPause);
            threadName = "ConnectionTS";
        }

        protected void onConnectionCall() {
            if (log.isInfoEnabled()) {
                log.info("onConnectionCall");
            }
            return;
        }

        protected void connectionCall() throws JMSException, NamingException {
            //initialize TopicPublisher and QueueReceiver
            if (log.isInfoEnabled()) {
                log.info("connectionCall");
            }
            if (null != jmsHandler.getTopicName()) {
                jmsHandler.initTopicSubscriber();
            }
        }
    }
}