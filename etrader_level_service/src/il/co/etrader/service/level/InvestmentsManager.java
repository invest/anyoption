package il.co.etrader.service.level;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.dao_managers.InvestmentsDAOBase;
import il.co.etrader.service.level.daos.InvestmentsDAO;
import il.co.etrader.util.ConstantsBase;

/**
 * Level service investments manager.
 *
 * @author Tony
 */
public class InvestmentsManager extends BaseBLManager {
    /**
     * List all the investments done on opportunity that is closed and are not settled, canceled or void.
     *
     * @return
     * @throws SQLException
     */
    public static ArrayList<Integer> getInvestmentsToSettle() throws SQLException {
        ArrayList<Integer> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = InvestmentsDAOBase.getInvestmentsToSettle(conn);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    /**
     * Settle investment.
     *
     * @param userId the id of the user who did the investment
     * @param investmentId the id of the investment to settle
     * @param result the result of the investment - the amount in cents won/lost
     *      from this investment(to be added to/taken from the balance). Positive amount means winning, negative losing.
     *      If a positive amount is passed it is expected to include the invested amount also (not net win).
     * @param netResult the clean win/lose from this investment in cents
     *      (if win it should not include the amount invested only the profit)
     * @param tax the tax balance amount after the settlement
     * @throws SQLException
     */
//    public static void settleInvestment(long userId, long investmentId, long result, long netResult, long tax) throws SQLException {
//        Connection conn = null;
//        try {
//            conn = DBUtil.getConnection();
//            if (log.isDebugEnabled()) {
//                log.debug("Balance before: " + InvestmentsDAOBase.getUserBalance(conn, userId));
//                log.debug("TaxBalance before: " + InvestmentsDAOBase.getUserTaxBalance(conn, userId));
//            }
//            conn.setAutoCommit(false);
//            InvestmentsDAOBase.settleInvestmentUpdateUserBalances(conn, userId, result, tax);
//            InvestmentsDAOBase.settleInvestment(conn, userId, investmentId, result, netResult);
//            conn.commit();
//            if (log.isDebugEnabled()) {
//                log.debug("Balance after: " + InvestmentsDAOBase.getUserBalance(conn, userId));
//                log.debug("TaxBalance after: " + InvestmentsDAOBase.getUserTaxBalance(conn, userId));
//            }
//        } catch (Throwable t) {
//            try {
//                conn.rollback();
//            } catch (Throwable it) {
//                log.error("Can't rollback.", it);
//            }
//            SQLException sqle = new SQLException();
//            sqle.initCause(t);
//            throw sqle;
//        } finally {
//            try {
//                conn.setAutoCommit(true);
//            } catch (Throwable t) {
//                log.error("Can't set back to autocommit.", t);
//            }
//            DBUtil.closeConnection(conn);
//        }
//    }

    /**
     * Settle investment.
     *
     * @param investmentId the id of the investment to settle.
     * @throws SQLException
     */
    public static void settleInvestment(long investmentId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            InvestmentsManagerBase.settleInvestment(conn, investmentId, 0, 0, null, ConstantsBase.REGULAR_SETTLEMNT, ConstantsBase.LOG_BALANCE_SETTLE_INVESTMENT, 0L);
        } finally {
            closeConnection(conn);
        }
    }

    public static void qualifyInvestmentsForInsurances(int goldenMinStart, int goldenMinEnd) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            InvestmentsDAO.qualifyInvestmentsForInsurances(conn, goldenMinStart, goldenMinEnd);
        } finally {
            closeConnection(conn);
        }
    }
    
    public static void qualifyInvestmentsForAdditionalInsurance(int goldenMinStart, int goldenMinEnd) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            InvestmentsDAO.qualifyInvestmentsForAdditionalInsurance(conn, goldenMinStart, goldenMinEnd);
        } finally {
            closeConnection(conn);
        }
    }
}