package il.co.etrader.service.level;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunitySettle;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.service.level.Utils;

import il.co.etrader.dao_managers.OpportunitiesDAOBase;
import il.co.etrader.service.level.daos.OpportunitiesDAO;
import il.co.etrader.util.ConstantsBase;

/**
 * Opportunities BL manager.
 *
 * @author Tony
 */
public class OpportunitiesManager extends BaseBLManager {
    private static Logger log = Logger.getLogger(OpportunitiesManager.class);

    /**
     * Set the drivers connection session time zone so timestamps with local time zone can
     * be used. The time zone is set to the current session time zone.
     * NOTE: Dont ask me... Oracle...
     * SEE: http://orafaq.com/usenet/comp.databases.oracle.misc/2005/12/06/0081.htm
     *
     * @param conn
     * @throws SQLException
     */
//    public static void setSessionTimeZone(Connection conn) throws SQLException {
//        Statement stmt = null;
//        ResultSet rs = null;
//        try {
//            String sql =
//                "SELECT " +
//                    "sessiontimezone " +
//                "FROM " +
//                    "dual";
//            stmt = conn.createStatement();
//            rs = stmt.executeQuery(sql);
//            if (rs.next()) {
//                String tz = rs.getString("sessiontimezone");
//                if (log.isDebugEnabled()) {
//                    log.debug("The session timezone is: " + tz);
//                }
//                OracleConnection oconn = (OracleConnection) ((WrappedConnection) conn).getUnderlyingConnection();
//                oconn.setSessionTimeZone("Israel");
//            }
//        } finally {
//            DAOBase.closeResultSet(rs);
//            DAOBase.closeStatement(stmt);
//        }
//    }

    /**
     * Load all the opportunities that the Level Service need on startup.
     * Only the data needed by the service is loaded.
     *
     * @param conn the db connection to use
     * @return <code>ArrayList</code> of <code>Opportunity</code>s.
     * @throws SQLException
     */
    public static ArrayList<Opportunity> getLevelServiceOpportunities() throws SQLException {
        ArrayList<Opportunity> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
//            setSessionTimeZone(conn);
//            OracleConnection oconn = (OracleConnection) ((WrappedConnection) conn).getUnderlyingConnection();
            l = OpportunitiesDAOBase.getLevelServiceOpportunities(conn);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    /**
     * Get details for an opportunity that is currently running.
     *
     * @param id the id of the opportunity
     * @return <code>Opportunity</code> with the requested details
     * @throws SQLException
     */
    public static Opportunity getRunningOpportunityById(long id) throws SQLException {
        Opportunity o = null;
        Connection conn = null;
        try {
            conn = getConnection();
            o = OpportunitiesDAOBase.getRunningById(conn, id);
        } finally {
            closeConnection(conn);
        }
        return o;
    }

    /**
     * Create the daily opportunities for specified day by copying the daily
     * template opportunities for which that day is a working day (it not a holiday).
     *
     * @param daysAhead for how much days ahead to create the opportunities.
     *      example:
     *      1 is to create the opps for tomorrow
     *      2 is for the day after tomorrow
     *      3 is for the day in 3 days time
     * @throws SQLException
     */
    public static void createDailyOpportunities(int daysAhead) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            OpportunitiesDAOBase.createDailyOpportunities(conn, daysAhead);
        } finally {
            closeConnection(conn);
        }
    }

    /**
     * Create weekly opportunities for the next week.
     *
     * @throws SQLException
     */
    public static void createWeeklyOpportunities() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            OpportunitiesDAOBase.createWeeklyOpportunities(conn);
        } finally {
            closeConnection(conn);
        }
    }

    /**
     * Create monthly opportunities for the next month.
     *
     * @throws SQLException
     */
    public static void createMonthlyOpportunities() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            OpportunitiesDAOBase.createMonthlyOpportunities(conn);
        } finally {
            closeConnection(conn);
        }
    }

    public static void createQuarterlyOpportunities() throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		OpportunitiesDAOBase.createQuarterlyOpportunities(con);
    	} finally {
    		
    	}
    }

    /**
     * List as HTML table the opportunities created in the last hour.
     *
     * @return
     * @throws SQLException
     */
    public static String reportCreatedOpportunities() throws SQLException {
        String report = null;
        Connection conn = null;
        try {
            conn = getConnection();
            report = OpportunitiesDAOBase.reportCreatedOpportunities(conn);
        } finally {
            closeConnection(conn);
        }
        return report;
    }

    /**
     * List as HTML table the opportunities created in the last hour.
     *
     * @return
     * @throws SQLException
     */
    public static String reportCreatedWeeklyOpportunities() throws SQLException {
        String report = null;
        Connection conn = null;
        try {
            conn = getConnection();
            report = OpportunitiesDAOBase.reportCreatedWeeklyOpportunities(conn);
        } finally {
            closeConnection(conn);
        }
        return report;
    }

    public static String reportCreatedScheduledOpportunities(int scheduled) throws SQLException {
        String report = null;
        Connection conn = null;
        try {
            conn = getConnection();
            report = OpportunitiesDAOBase.reportCreatedScheduledOpportunities(conn, scheduled);
        } finally {
            closeConnection(conn);
        }
        return report;
    }

    /**
     * Gets list of ids of the opportunities that have moved to specified state in the specified time.
     *
     * @param state the state of interest
     * @param time the time of interest
     * @return
     * @throws SQLException
     */
    public static ArrayList<Long> getOpportunitiesMovedToState(int state, Date time) throws SQLException {
        ArrayList<Long> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = OpportunitiesDAOBase.getOpportunitiesMovedToState(conn, state, time);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    /**
     * Load the week/month opportunities that need to be paused for the night/weekend/holiday(s) at that minute
     * (the market for them closed at that minute).
     *
     * @param time the time (the minute to check for)
     * @return <code>ArrayList<Long></code> with the ids of the opportunities to pause.
     * @throws SQLException
     */
    public static ArrayList<Long> getOpportunitiesToPause(Date time) throws SQLException {
        ArrayList<Long> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = OpportunitiesDAOBase.getOpportunitiesToPause(conn, time);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    /**
     * Load the week/month opportunities that need to be unpaused at that minute
     * (the market for them opened at that minute after night/weekend/holiday(s)).
     *
     * @param time the time (the minute to check for)
     * @return <code>ArrayList<Long></code> with the ids of the opportunities to unpause.
     * @throws SQLException
     */
    public static ArrayList<Long> getOpportunitiesToUnpause(Date time) throws SQLException {
        ArrayList<Long> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = OpportunitiesDAOBase.getOpportunitiesToUnpause(conn, time);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    /**
     * Close opportunity and set the closing level.
     *
     * @param oppId the id of the opp to close
     * @param closingLevel the closing level
     * @param closeLevelTxt the closing level formula as txt
     * @throws SQLException
     */
    public static void closeOpportunity(long oppId, double closingLevel , String closeLevelTxt) throws SQLException {
		log.info("closeOpportunity - oppId: " + oppId + " expiration level: " + closingLevel + " closeLevelTxt: " + closeLevelTxt);
        Connection conn = null;
        try {
            conn = getConnection();
            OpportunitiesDAOBase.closeOpportunity(conn, oppId, closingLevel, closeLevelTxt);
        } catch (SQLException sqle) {
            throw sqle;
        } catch (Throwable t) {
            log.fatal("Can't clos opp.", t);
        } finally {
            closeConnection(conn);
        }
    }

    /**
     * Log a opportunity auto level shifting.
     *
     * @param oppId the opp id
     * @param shifting the current shifting
     * @param lastShifting the last (current) step of the shifting
     * @param up is the shifting up or down
     * @throws SQLException
     */
    public static void insertShifting(long oppId,
										double shifting,
										double lastShifting,
										boolean up,
										SkinGroup skinGroup) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            OpportunitiesDAOBase.insertShifting(conn, oppId, new BigDecimal(Double.toString(lastShifting)), up, 0, skinGroup);
            switch(skinGroup) {
	            case AUTO:
	                OpportunitiesDAOBase.updateOpportunityAutoShiftParameter(conn, oppId, shifting);
	            	break;
	            // all the skin groups 	
	            default: 	
	                OpportunitiesDAOBase.updateOpportunityShiftParameter(conn, oppId, skinGroup, shifting, Writer.WRITER_ID_AUTO);
	            	break;
            }
        } finally {
            closeConnection(conn);
        }
    }

    /**
     * Update opportunity shift_parameter.
     *
     * @param oppId the opp id
     * @param shifting the new shift_parameter value
     * @throws SQLException
     */
//    public static void updateOpportunityShiftParameter(long oppId, double shifting) throws SQLException {
//        Connection conn = null;
//        try {
//            conn = DBUtil.getConnection();
//            OpportunitiesDAOBase.updateOpportunityShiftParameter(conn, oppId, shifting);
//        } finally {
//            DBUtil.closeConnection(conn);
//        }
//    }

    /**
     * Update opportunities that are closed and don't have unsettled investments
     * to settled state.
     * 
     * @return list of opportunities that have been settled or
     * 			<code>null</code> if there were no opportunities to settle.
     * @throws SQLException
     */
    public static List<OpportunitySettle> settleOpportunities() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            List<OpportunitySettle> oppsToSettle = OpportunitiesDAOBase.getOpportunitiesToSettle(conn);
            if (oppsToSettle != null && oppsToSettle.size() > 0) {
            	OpportunitiesDAOBase.settleOpportunities(conn, oppsToSettle);
            	return oppsToSettle;
            } else {
            	return null;
            }
        } finally {
            closeConnection(conn);
        }
    }
    
    /**
     * Reload from the db the odds for opportunity.
     *
     * @param o the opportunity to update the odds of (will take the id of the opp)
     * @throws SQLException
     */
    public static void updateOpportunityOdds(Opportunity o) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            OpportunitiesDAOBase.updateOpportunityOdds(conn, o);
        } finally {
            closeConnection(conn);
        }
    }

    /**
     * Change the published state of an opp.
     *
     * @param oppId the id of the opp which published state should be changed
     * @param published the new published state of the opp
     * @throws SQLException
     */
    public static void publishOpportunity(long oppId, boolean published) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            OpportunitiesDAOBase.publishOpportunity(conn, oppId, published);
        } finally {
            closeConnection(conn);
        }
    }
    /**
     * Reset the opportunities.is_published flag to 0 for all opps that have 1.
     *
     * @param conn
     * @throws SQLException
     */
    public static void resetIsPublished() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            OpportunitiesDAOBase.resetIsPublished(conn);
        } finally {
            closeConnection(conn);
        }
    }

    /**
     * get option expertion letter
     *
     * @param market id
     * @return the letter to use with the market ric or null
     * @throws SQLException
     */
    public static String getOpportunityOptionsExpirationLetter(long marketId) {
        String useSecondLetter = null;
        Connection conn = null;
        try {
            conn = getConnection();
            useSecondLetter = OpportunitiesDAOBase.getOpportunityOptionsExpirationLetter(conn, marketId);
        } catch (SQLException e) {
            log.error("cant load option expiration letter for market" + marketId);
        } finally {
            closeConnection(conn);
        }
        return useSecondLetter;
    }

    /**
     * Check if we should switch to next month options for certain market (.TA25 or ILS=).
     *
     * @param marketId the id of the market to check for
     * @return <code>true</code> if to switch to next month options else <code>false</code>.
     * @throws SQLException
     */
    public static boolean getOpportunityOptionsChangeMonth(long marketId) {
        Date expDate = null;
        Connection conn = null;
        try {
            conn = getConnection();
            expDate = OpportunitiesDAOBase.getOpportunityOptionsExpirationDate(conn, marketId);
        } catch (SQLException sqle) {
            log.error("Can't load expiration date for market " + marketId);
        } finally {
            closeConnection(conn);
        }

        if (null != expDate) {
            Calendar exp = Calendar.getInstance();
            exp.setTime(expDate);
            Calendar today = Calendar.getInstance();

            if (exp.get(Calendar.YEAR) > today.get(Calendar.YEAR) || exp.get(Calendar.MONTH) > today.get(Calendar.MONTH)) {
                // if the month of the exp date is next month
                // this month exp date passed
                return true;
            }

            if (exp.get(Calendar.DAY_OF_MONTH) > today.get(Calendar.DAY_OF_MONTH)) {
                // expire some of the next days in the month
                return false;
            }

            // only possible case left is today which will mean change the month
        }

        // assume the exp date came and passed and we forget
        // to fill in the db the next month exp date
        return true;
    }

    /**
     * Calculate how many days left until options expiration for specified market.
     *
     * @param marketId
     * @return The number of days left until the date specified for this market in
     *      markets.expiry_date deducted by 1.
     */
    public static long getOpportunityOptionsExpireIn(long marketId) {
        Date expDate = null;
        Connection conn = null;
        try {
            conn = getConnection();
            expDate = OpportunitiesDAOBase.getOpportunityOptionsExpirationDate(conn, marketId);
        } catch (SQLException sqle) {
            log.error("Can't load expiration date for market " + marketId);
        } finally {
            closeConnection(conn);
        }

        long expIn = 0;
        if (null != expDate) {
            expIn = Utils.getDaysToExpire(expDate);
        } else {
            log.warn("Can't take the days to expire for market: " + marketId);
        }
        return expIn;
    }

    /**
     * Disable market opportunities.
     *
     * @param marketId
     * @param desc description for the log table
     * @return The number of rows updated.
     * @throws SQLException
     */
    public static int disableMarketOpportunities(long marketId, String desc) throws SQLException {
        int count = 0;
        Connection conn = null;
        try {
            conn = getConnection();
            count = OpportunitiesDAOBase.disableMarketOpportunities(conn, marketId);
            if (count > 0) {
                AdminManager.addToLog(0, "markets", marketId, ConstantsBase.LOG_COMMANDS_MONITORING_DISABLE_OPP, desc);
            }
        } finally {
            closeConnection(conn);
        }
        return count;
    }

    /**
     * Enable market opportunities.
     *
     * @param marketId
     * @param desc description for the log table
     * @return The number of rows updated.
     * @throws SQLException
     */
    public static int enableMarketOpportunities(long marketId, String desc) throws SQLException {
        int count = 0;
        Connection conn = null;
        try {
            conn = getConnection();
            count = OpportunitiesDAOBase.enableMarketOpportunities(conn, marketId);
            if (count > 0) {
                AdminManager.addToLog(0, "markets", marketId, ConstantsBase.LOG_COMMANDS_MONITORING_ENABLE_OPP, desc);
            }
        } finally {
            closeConnection(conn);
        }
        return count;
    }

	public static void createOneTouchOpportunities() throws SQLException {
		Connection conn = null;
        try {
            conn = getConnection();
            OpportunitiesDAOBase.createOneTouchOpportunities(conn);
        } finally {
            closeConnection(conn);
        }

	}

	public static String oneTouchReportCreatedOpportunities() throws SQLException {
		String report = null;
        Connection conn = null;
        try {
            conn = getConnection();
            report = OpportunitiesDAOBase.oneTouchReportCreatedOpportunities(conn);
        } finally {
            closeConnection(conn);
        }
        return report;
	}

    public static void writeOpportunitiesGMLevels(ArrayList<Opportunity> l) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            for (int i = 0; i < l.size(); i++) {
                OpportunitiesDAO.setGMLevel(conn, l.get(i));
            }
        } finally {
            closeConnection(conn);
        }
    }

    public static void writeOpportunitiesGMLevelsAdditional(ArrayList<Opportunity> l) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            for (int i = 0; i < l.size(); i++) {
                OpportunitiesDAO.setGMLevelAdditional(conn, l.get(i));
            }
        } finally {
            closeConnection(conn);
        }
    }
    
    public static Date getNextOpenDate(long marketId) throws SQLException {
        Date nextOpenDateFromDb = null;
        Connection conn = null;
        try {
            conn = getConnection();
            nextOpenDateFromDb = OpportunitiesDAOBase.getNextOpenDate(conn, marketId);
        } finally {
            closeConnection(conn);
        }
        return nextOpenDateFromDb;
    }

	public static void updateOppCurrentLevel(long oppId, double lastCalcResult) throws SQLException {
		 Connection conn = null;
		 try {
			 conn = getConnection();
			 OpportunitiesDAO.updateOppCurrentLevel(conn, oppId, lastCalcResult);
		 } finally {
			 closeConnection(conn);
		 }
	}

	public static void getOpportunityCallPutAmounts(Opportunity opp) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            OpportunitiesDAOBase.getOpportunityCallPutAmounts(conn, opp);
        } finally {
            closeConnection(conn);
        }
	}

    public static void getOpportunityCallPutBoughtSoldAmounts(Opportunity opp) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            OpportunitiesDAOBase.getOpportunityCallPutBoughtSoldAmounts(conn, opp);
        } finally {
            closeConnection(conn);
        }
    }
}