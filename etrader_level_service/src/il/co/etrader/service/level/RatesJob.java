package il.co.etrader.service.level;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.MarketRate;

import il.co.etrader.service.level.managers.MarketsManager;

/**
 * Write the current opened markets rates to the db.
 * 
 * @author Tony
 */
public class RatesJob extends Thread {
    private static Logger log = Logger.getLogger(RatesJob.class);
    
    private boolean running;
    private long lastTickTime;
    
    public void run() {
        Thread.currentThread().setName("RatesJob");
        if (log.isTraceEnabled()) {
            log.trace("RatesJob start");
        }
        running = true;
        lastTickTime = System.currentTimeMillis();
        long crrTickTime = 0;
        while (running) {
            crrTickTime = System.currentTimeMillis();
            if (crrTickTime - lastTickTime > 12000) {
                log.warn("There were no RatesJob thick for " + (crrTickTime - lastTickTime));
            }
            lastTickTime = crrTickTime;
            try {
                long ttw = System.currentTimeMillis();
                ArrayList<MarketRate> l = LevelService.getInstance().getMarketsCurrentRates();
                MarketsManager.writeMarketsRates(l);
                if (log.isTraceEnabled()) {
                    log.trace("RatesJob time to write rates to db - " + (System.currentTimeMillis() - ttw));
                }
            } catch (Exception e) {
                log.error("Can't write markets rates to db.", e);
            }
            
            synchronized (this) {
                try {
                    this.wait(6000);
                } catch (InterruptedException ie) {
                    // do nothing
                }
            }
        }
        if (log.isTraceEnabled()) {
            log.trace("RatesJob end");
        }
    }
    
    public void stopRatesJob() {
        if (log.isInfoEnabled()) {
            log.info("Stopping RatesJob.");
        }
        running = false;
        synchronized (this) {
            this.notify();
        }
    }
    
    public boolean isWorking() {
        return System.currentTimeMillis() - lastTickTime < 12000;
    }
}