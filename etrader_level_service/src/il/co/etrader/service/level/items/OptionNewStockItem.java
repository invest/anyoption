package il.co.etrader.service.level.items;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.options.NewStockOption;
import il.co.etrader.service.level.options.Option;

/**
 * Implement the formula for new stock item option (except the ILS).
 * for stocks that we need as an option and not main ric bcoz same feed name cant be option and main ric
 *
 * @author Eyal
 */
public class OptionNewStockItem extends NewStockItem {
    private static final Logger log = Logger.getLogger(OptionNewStockItem.class);
    protected NewStockOption option;

    public OptionNewStockItem(String itemName, Handle handle) {
        super(itemName, handle);

        useMainRic = false;

        option = (NewStockOption) Option.getInstance(itemName.endsWith(".Option+") ? itemName.substring(0, itemName.length() - 8) : itemName);

    }

    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
    	log.trace(getClass().getSimpleName() + " update");
        return processStockUpdate(fields, sender, lifeId, false, opportunitiesTemp);
    }

    /**
     * Tell the item to open (subscribe) all its options (when you want to start using it).
     *
     * @param itm the Item instance to receive the updates for the options.
     */
    public void subscribeOptions(Item itm) {
        super.subscribeOptions(itm);
        optionSubscribtionManager.subscribeOption(itm, option);
    }

    public boolean isReutersStateOK() {
        return option.isReutersStateOK();
    }

    /**
     * Clear the options list (unsibscribe each option it remove).
     * Make sure you have lock over the options when you call this metod.
     *
     * @param itm the Item instance to be removed from the options.
     */
    protected void clearOptions(Item itm) {
        super.clearOptions(itm);
        optionSubscribtionManager.unsubscribeOption(itm, option);
    }
}