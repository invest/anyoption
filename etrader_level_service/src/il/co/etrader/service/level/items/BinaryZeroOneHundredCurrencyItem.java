//package il.co.etrader.service.level.items;
//
//import org.apache.log4j.Logger;
//
//import com.reuters.rfa.common.Handle;
//
///**
// * Implement the formula for 0100 currencies
// *
// * @author Eyal
// */
//public class BinaryZeroOneHundredCurrencyItem extends BinaryZeroOneHundredItem {
//    private static final Logger log = Logger.getLogger(BinaryZeroOneHundredCurrencyItem.class);
//
//
//    public BinaryZeroOneHundredCurrencyItem(String itemName, Handle handle) {
//        super(itemName, handle);
//    }
//
//    /**
//     * get the current level from the option+ market
//     * @return current level
//     */
//    @Override
//    protected double getBinaryMarketCurrentLevel() {
//	    if (((OptionPlusCurrencyItem)binaryMarket).isSnapshotReceived()) {
//	    	double ask = ((OptionPlusCurrencyItem)binaryMarket).getOptionAsk();
//	    	double bid = ((OptionPlusCurrencyItem)binaryMarket).getOptionBid();
//	    	log.debug("current level =  ((ask + bid) / 2 " +  (ask + bid) / 2 + " + current level " + binaryMarket.lastCalcResult + ") / 2 = " + ((ask + bid) / 2 + binaryMarket.lastCalcResult) / 2);
//	    	return ((ask + bid) / 2 + binaryMarket.lastCalcResult) / 2;
//	    }
//	    return 0;
//    }
//}