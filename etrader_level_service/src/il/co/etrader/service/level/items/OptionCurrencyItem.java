package il.co.etrader.service.level.items;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.anyoption.common.beans.Opportunity;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.options.AskBidOption;
import il.co.etrader.service.level.options.CurrencyOption;
import il.co.etrader.service.level.options.Option;

/**
 * Implement the formula for currency items (except the ILS).
 * for currency that we need as an option and not main ric bcoz same feed name cant be option and main ric
 *
 * @author Eyal
 */
public class OptionCurrencyItem extends CurrencyItem {
    protected CurrencyOption option;
    protected AskBidOption fxal;
    protected float fXALParameter;

    public OptionCurrencyItem(String itemName, Handle handle) {
        super(itemName, handle);

        useMainRic = false;
        shouldBeClosedByMonitoring = true;

        option = (CurrencyOption) Option.getInstance(itemName);
        fxal = (AskBidOption) Option.getInstance(itemName + "FXAL");
    }

    @Override
    public void addOpportunity(Opportunity o, SenderThread sender, long lifeId) {
    	super.addOpportunity(o, sender, lifeId);
    	if (o.getMarket().getQuoteParams() != null) {
    		int start = o.getMarket().getQuoteParams().lastIndexOf("=") + 1;
    		int end = o.getMarket().getQuoteParams().substring(start).indexOf(";");
    		if (end < 0) {
    			end = o.getMarket().getQuoteParams().length();
    		}
    		fXALParameter = new Float(o.getMarket().getQuoteParams().substring(start, end));
    	}
    }

    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
    @Override
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        return false;
    }

    @Override
    public boolean optionUpdated(Option option, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
    	lastReutersUpdateReceiveTime = System.currentTimeMillis();
        if (option.getLastLevel() != null && (fXALParameter <= 0 || fxal.isSnapshotReceived())) {
            double avg = option.getLastLevel();
            if (fXALParameter > 0) {
            	avg = ((1 - fXALParameter) * avg) + (fXALParameter * (fxal.getAsk() + fxal.getBid()) / 2); 
            }
            double crrLevel = formulaRandomChange(avg);
            setSnapshotReceived(true);
            if (interestUSD1M.isSnapshotReceived() && interestCRR1M.isSnapshotReceived()) {
                setLongTermSnapshotReceived(true);
            }
            haveUpdate(sender, lifeId, true, avg, crrLevel, true, ((CurrencyOption)option).getLastRealLevel());
        }
        return super.optionUpdated(option, sender, lifeId, opportunitiesTemp);
    }

    /**
     * Tell the item to open (subscribe) all its options (when you want to start using it).
     *
     * @param itm the Item instance to receive the updates for the options.
     */
    @Override
    public void subscribeOptions(Item itm) {
        super.subscribeOptions(itm);
        optionSubscribtionManager.subscribeOption(itm, option);
        optionSubscribtionManager.subscribeOption(itm, fxal);
    }

    @Override
    public boolean isReutersStateOK() {
        return option.isReutersStateOK();
    }

    /**
     * Clear the options list (unsibscribe each option it remove).
     * Make sure you have lock over the options when you call this metod.
     *
     * @param itm the Item instance to be removed from the options.
     */
    @Override
    protected void clearOptions(Item itm) {
        super.clearOptions(itm);
        optionSubscribtionManager.unsubscribeOption(itm, option);
        optionSubscribtionManager.unsubscribeOption(itm, fxal);
    }
    
    @Override
    public void resetInTheEndOfTheDay() {
    	super.resetInTheEndOfTheDay();

    	option.resetInTheEndOfTheDay();
    	fxal.resetInTheEndOfTheDay();
    }
    
    @Override
    protected Map<String, String> getQuoteFields() {
    	Map<String, String> quoFlds = new HashMap<String, String>();
    	if (null != option && null != option.getAsk()) {
    		quoFlds.put(QUOTE_FIELDS_ASK, option.getAsk().toString());
    	}
    	if (null != option && null != option.getBid()) {
    		quoFlds.put(QUOTE_FIELDS_BID, option.getBid().toString());
    	}
    	if (null != option && null != option.getLast()) {
    		quoFlds.put(QUOTE_FIELDS_LAST, option.getLast().toString());
    	}
    	return quoFlds;
    }

	public void setQuoteParams(String quoteParams) {
		super.setQuoteParams(quoteParams);
		String fXALParamStr = quoteParams.substring(quoteParams.lastIndexOf("=") + 1).trim();
		if (fXALParamStr.endsWith(";")) {
			fXALParamStr = fXALParamStr.substring(0, fXALParamStr.length() - 1);
		}
		fXALParameter = new Float(fXALParamStr);
	}
}