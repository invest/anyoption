package il.co.etrader.service.level.items;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ReutersQuotes;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.options.CurrencyOption;
import il.co.etrader.service.level.options.Option;

/**
 * Implement the formula for currency items (except the ILS).
 *
 * @author Tony
 */
public class CurrencyItem extends Item {
    private static final Logger log = Logger.getLogger(CurrencyItem.class);

    protected ArrayList<Double> lastFiveQuotes;
    protected Option interestUSD1M;
    protected Option interestCRR1M;

    public CurrencyItem(String itemName, Handle handle) {
        super(itemName, handle);

        lastFiveQuotes = new ArrayList<Double>();

        String currItemName = itemName.substring(0, itemName.length()-1);
        String leadingCurrency = "USD";
        if(currItemName.equals("EURJPY")) {
        	leadingCurrency = "EUR";
        	currItemName = 	"JPY";
        } else if (currItemName.equals("EURGBP")) {
        	leadingCurrency = "EUR";
        	currItemName = 	"GBP";
        } else if (currItemName.equals("GBPJPY")) {
        	leadingCurrency = "GBP";
        	currItemName = 	"JPY";
        } else if (currItemName.equals("AUDJPY")) {
        	leadingCurrency = "AUD";
        	currItemName = 	"JPY";
        }
        interestUSD1M = Option.getInstance(leadingCurrency + "1MD=");
        if (itemName.equals("KRW=")) {
        	interestCRR1M = Option.getInstance("KRW1M=");
        } else {
        	interestCRR1M = Option.getInstance(currItemName + "1MD=");
        }
    }

    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
    @Override
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        Double bid = (Double) fields.get("BID");
        Double ask = (Double) fields.get("ASK");
        if (null != bid && bid > 0 && null != ask && ask > 0) {
            lastReutersUpdateReceiveTime = System.currentTimeMillis();

            BigDecimal bda = new BigDecimal(Double.toString(ask));
            BigDecimal bdb = new BigDecimal(Double.toString(bid));
            double crrRealLevel = bda.add(bdb).divide(new BigDecimal(2)).doubleValue();

            lastFiveQuotes.add(crrRealLevel);
            if (lastFiveQuotes.size() > 5) {
                lastFiveQuotes.remove(0);
            }
            double min = Double.MAX_VALUE;
            double max = Double.MIN_VALUE;
            double sum = 0;
            double crr;
            for (int i = 0; i < lastFiveQuotes.size(); i++) {
                crr = lastFiveQuotes.get(i);
                if (crr < min) {
                    min = crr;
                }
                if (crr > max) {
                    max = crr;
                }
                sum += crr;
            }
            setSnapshotReceived(true);
            if (interestUSD1M.isSnapshotReceived() && interestCRR1M.isSnapshotReceived()) {
                setLongTermSnapshotReceived(true);
            }

            double avg = (sum - (lastFiveQuotes.size() > 3 ? min : 0) - (lastFiveQuotes.size() > 4 ? max : 0)) / (lastFiveQuotes.size() == 5 || lastFiveQuotes.size() == 4 ? 3 : lastFiveQuotes.size());
            double crrLevel = formulaRandomChange(avg);
            haveUpdate(sender, lifeId, true, avg, crrLevel, true, crrRealLevel);
        } else {
            if (log.isTraceEnabled()) {
                log.trace("No value");
            }
        }
        return false;
    }

    /**
     * Notify the item that one of its options have received update.
     *
     * @param option the option that received update
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    @Override
    public boolean optionUpdated(Option option, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
    	boolean gotClosingLevel = false;
    	if (option instanceof CurrencyOption && option.getLastLevel() != null) {
	    	Date updateTime = null;
	        try {
	            updateTime = getUpdateTime(option.getActiveTime(), "GMT");
	        } catch (Exception e) {
	            log.error("Can't process TIMACT", e);
	            updateTime = new Date(System.currentTimeMillis());
	        }
	        gotClosingLevel = gotClosingLevel || updateOpportunitiesClosingLevel(updateTime, ((CurrencyOption)option).getLastRealLevel(), 
	        		((CurrencyOption)option).getLastRealLevel(), "(ASK+BID)/2", "(ASK+BID)/2", new ReutersQuotes(updateTime, null, new BigDecimal(option.getAsk()), 
	        				new BigDecimal(option.getBid())), new ReutersQuotes(updateTime, null, new BigDecimal(option.getAsk()), new BigDecimal(option.getBid())), opportunitiesTemp);
    	}
        if (option.getOptionName().equals(interestUSD1M.getOptionName()) ||
                option.getOptionName().equals(interestCRR1M.getOptionName())) {
            if (isSnapshotReceived() && interestUSD1M.isSnapshotReceived() && interestCRR1M.isSnapshotReceived()) {
                setLongTermSnapshotReceived(true);
                sendUpdate(sender, lifeId, UPDATE_LONG_TERM_OPPS, true, false);
            }
        }
        return gotClosingLevel;
    }

    /**
     * @param o the opportunity for which to calc the level
     * @return The long term formula result for this item.
     */
    @Override
    protected double longTermFormula(Opportunity o) {
        return calcLongTermLevel(o);
    }

    /**
     * Check if the closing level for specified opportunity has been received.
     *
     * @param w the opportunity to check for if the closing level is ready
     * @return <code>true</code> if the closing level is ready to be taken else <code>false</code>
     */
    @Override
    public boolean isClosingLevelReceived(Opportunity w) {
    	return w.isClosingLevelReceived();
    }

    /**
     * Get the closing level for the specified opportunity.
     *
     * @param o the opportunity for which to get closing level
     * @return The level as received from Reuters. No formula and exposure control calculations.
     */
    @Override
    public double getClosingLevel(Opportunity o) {
        long decimal = o.getMarket().getDecimalPoint() - o.getMarket().getDecimalPointSubtractDigits();
        if (o.isUseManualClosingLevel()) {
            return roundDouble(o.getManualClosingLevel(), decimal);
        }
        return roundDouble(o.getNextClosingLevel(), decimal);
    }

    /**
     * Tell the item to open (subscribe) all its options (when you want to start using it).
     *
     * @param itm the Item instance to receive the updates for the options.
     */
    @Override
    public void subscribeOptions(Item itm) {
        super.subscribeOptions(itm);
        
        optionSubscribtionManager.subscribeOption(itm, interestUSD1M);
        optionSubscribtionManager.subscribeOption(itm, interestCRR1M);
    }

    /**
     * Find option with specified name from the options.
     *
     * @param optionName the name of the option to look for
     * @return The <code>Option</code> with the specified name or <code>null</code> if not found.
     */
    @Override
    protected Option getOption(String optionName) {
        Option o = super.getOption(optionName);
        if (null != o) {
            return o;
        }
        log.warn("Opption " + optionName + " not found!");
        return null;
    }

    @Override
    public boolean isReutersStateOK() {
        if (!interestUSD1M.isReutersStateOK() || !interestCRR1M.isReutersStateOK()) {
            return false;
        }
        return super.isReutersStateOK();
    }

    /**
     * Clear the options list (unsibscribe each option it remove).
     * Make sure you have lock over the options when you call this metod.
     *
     * @param itm the Item instance to be removed from the options.
     */
    @Override
    protected void clearOptions(Item itm) {
        super.clearOptions(itm);
        
        optionSubscribtionManager.unsubscribeOption(itm, interestUSD1M);
        optionSubscribtionManager.unsubscribeOption(itm, interestCRR1M);
    }

    /**
     * @param o the opportunity for which to calc the level
     * @return The wee/month opportunities level.
     */
    protected double calcLongTermLevel(Opportunity o) {
        double c9 = roundDouble(interestUSD1M.getAsk() + interestUSD1M.getBid(), decimalPoint) / 2;
        double c10 = roundDouble(interestCRR1M.getAsk() + interestCRR1M.getBid(), decimalPoint) / 2;
        double c11 = 1;
        long daysToExpire = getDaysToExpire(o.getTimeEstClosing());
        if (daysToExpire > 2) {
            c11 = daysToExpire - getSpotConstant();
        } else {
            c11 = daysToExpire;
        }
        double c13 = c9 / 36000 * c11;
        double c14 = c10 / getLongTermLevelDivideDays() * c11;
        double c15 = c14 + 1;
        double c16 = c13 + 1;
        double c17 = 1;
        if (itemName.equals("GBP=")
        		|| itemName.equals("EUR=")
        		|| itemName.equals("AUD=")
        		|| itemName.equals("AUDJPY=")
        		|| itemName.equals("NZD=")) {
            c17 = c16 / c15;
        } else {
            c17 = c15 / c16;
        }
        if (log.isTraceEnabled()) {
            log.trace(
                    interestUSD1M.getOptionName() + ": " + interestUSD1M.getAsk() + " " +
                    interestUSD1M.getOptionName() + ": " + interestUSD1M.getBid() + " " +
                    interestCRR1M.getOptionName() + ": " + interestCRR1M.getAsk() + " " +
                    interestCRR1M.getOptionName() + ": " + interestCRR1M.getBid() + " " +
                    " c9: " + c9 +
                    " c10: " + c10 +
                    " c11: " + c11 +
                    " c13: " + c13 +
                    " c14: " + c14 +
                    " c15: " + c15 +
                    " c16: " + c16 +
                    " c17: " + c17);
        }
        return c17 * lastCalcResult;
    }

    protected long getLongTermLevelDivideDays() {
        if (itemName.equals("EUR=")) {
            return 36000;
        }
        return 36500; // ILS=, GBP=
    }

    /**
     * @return The spot days constant for this item.
     */
    protected int getSpotConstant() {
        if (itemName.equals("TRY=")) {
            return 1;
        }
        return 2;
    }

    /**
     * Reset the market internal state in the end of the day to be ready for the next
     * day opening. Long term shiftings, updates flags etc.
     */
    @Override
    public void resetInTheEndOfTheDay() {
        super.resetInTheEndOfTheDay();

        lastFiveQuotes.clear();
    }

    @Override
    public boolean isShouldBeClosed(long oppId) {
        return true;
    }

    public void setLastUpdateQuote(Opportunity o, Option opt) {
    	o.setLastUpdateQuote(new ReutersQuotes(new Date(), null, new BigDecimal(opt.getAsk()), new BigDecimal(opt.getBid())));
    }
}