package il.co.etrader.service.level.items;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ReutersQuotes;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.BigDecimalHelper;
import il.co.etrader.service.level.LevelService;
import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.options.Option;
import il.co.etrader.service.level.options.SecondRICOption;

/**
 * Implement current level calculation for Nasdaq100  Future.
 *
 * @author Tony
 */
public class NasdaqFItem extends Item {
    private static final Logger log = Logger.getLogger(NasdaqFItem.class);

    private BigDecimal SPREAD_CHECK_VALUE ;

    protected SecondRICOption secondRIC;
    protected BigDecimal lastPrimact1;
    protected BigDecimal lastAsk;
    protected BigDecimal lastBid;
    protected boolean valuesCheckDisable;
    protected String lastTrdTim;
    protected String timeZone;

    public NasdaqFItem(String itemName, Handle handle) {
        super(itemName, handle);

        useMainRic = false;
        shouldBeClosedByMonitoring = true;
        valuesCheckDisable = false;
        SPREAD_CHECK_VALUE = new BigDecimal("1.7");

        if (itemName.equals("FKLI")) {
            dontCloseAfterXSecNoUpdate = 240;
        }
        timeZone = "GMT";
        if (itemName.startsWith("FDX")) {
//            timeZone = "Europe/Berlin";
            SPREAD_CHECK_VALUE = new BigDecimal("3.4");
        }


        if (itemName.startsWith("RIRTS")) {
        	SPREAD_CHECK_VALUE = new BigDecimal("75");
        }
    }

    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
    @Override
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        return false;
    }

    /**
     * Notify the item that one of its options have received update.
     *
     * @param option the option that received update
     * @param jmsHandler
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    @Override
    public boolean optionUpdated(Option option, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        boolean gotClosingLevel = false;
        if (option.getOptionName().equals(secondRIC.getOptionName())) {
            try {
                Double trdprc1 = option.getLast();
                Double ask = option.getAsk();
                Double bid = option.getBid();
                String trdTim = option.getActiveDate();
                if (null != trdTim) {
                    lastTrdTim = trdTim;
                }
                if ((null != trdprc1 && trdprc1 > 0) ||
                        (null != ask && ask > 0) ||
                        (null != bid && bid > 0)) {
                    lastReutersUpdateReceiveTime = System.currentTimeMillis();

                    Date updateTime = null;
                    if (null != lastTrdTim) {
                        try {
                            updateTime = getUpdateTime(lastTrdTim, timeZone);
                        } catch (Throwable t) {
                            log.error("Can't process " + (itemName.equals("TRF30")?" TIMACT.":" SALTIM."), t);
                            updateTime = new Date(System.currentTimeMillis());
                        }
                    } else {
                        log.error("Update without " + (itemName.equals("TRF30")?" TIMACT.":" SALTIM."));
                        updateTime = new Date(System.currentTimeMillis());
                    }

                    if (null != trdprc1 && trdprc1 > 0) {
                        lastPrimact1 = new BigDecimal(trdprc1.toString());
                    }

                    if (null != ask && ask > 0) {
                        lastAsk = new BigDecimal(ask.toString());
                    }
                    if (null != bid && bid > 0) {
                        lastBid = new BigDecimal(bid.toString());
                    }

                    if (null != lastPrimact1 && null != lastAsk && null != lastBid) {
                    	double calc = lastPrimact1
		                                .add(lastAsk)
		                                .add(lastBid)
		                                .divide(BigDecimalHelper.THREE,
		                            			BigDecimalHelper.BIG_DECIMAL_CALC_SCALE,
		                            			RoundingMode.HALF_UP)
		                                .doubleValue();

                    	if (null != trdprc1 && trdprc1 > 0) {
	                    	String closeLevelTxt = "formula:round((TRDPRC_1+ASK+BID)/3)"
				                						+ ";TRDPRC_1:" + lastPrimact1.toString()
						                				+ ";ask:" + lastAsk.toString()
						                				+ ";bid:" + lastBid.toString()
						                				+ ";closeLevelBeforeRound:" + calc;
	                        ReutersQuotes currentQuote = new ReutersQuotes(updateTime, lastPrimact1, lastAsk, lastBid);
	                        gotClosingLevel = updateOpportunitiesClosingLevel(
	                											updateTime,
	                											calc,
	                											calc,
	                											closeLevelTxt,
	                											closeLevelTxt,
	                											currentQuote,
	                											currentQuote,
	                											opportunitiesTemp);
                    	}
                    	
                    	/*
                    	 * In case (ASK-BID)>1 disable the market when (ASK-BID)<=1 Enable it again
                    	 * start bugID: 2032
                    	 */
                        if (lastAsk.subtract(lastBid).compareTo(SPREAD_CHECK_VALUE) > 0) {
                        	if (disableOpportunities("( ASK - BID ) > " + SPREAD_CHECK_VALUE)){
                        		valuesCheckDisable=true;
	                        	String str =
	                                "Stock " + itemName + " ( ASK - BID ) > " + SPREAD_CHECK_VALUE +
	                                " ASK = " + lastAsk +
	                                " BID = " + lastBid +
	                                " PRIMACT_1 = " + lastPrimact1;
	                        	LevelService.getInstance().sendAlertEmail("Values check disable " + itemName, str);
                        	}
                        } else {
                        	if (enableOpportunities("( ASK - BID ) <= " + SPREAD_CHECK_VALUE, false)){
                        		valuesCheckDisable=false;
                        		String str =
	                                "Stock " + itemName + "( ASK - BID ) <= " + SPREAD_CHECK_VALUE +
	                                " ASK = " + lastAsk +
	                                " BID = " + lastBid +
	                                " PRIMACT_1 = " + lastPrimact1;
	                        	LevelService.getInstance().sendAlertEmail("Values check enable " + itemName, str);
                        	}
                        }
//                      end bugID: 2032

                        double crrLevel = formulaRandomChange(calc);
                        haveUpdate(sender, lifeId, true, calc, crrLevel, true, lastPrimact1.doubleValue());

                        setSnapshotReceived(true);
                        setLongTermSnapshotReceived(true);
                    }
                } else {
                    if (log.isDebugEnabled()) {
                        log.debug("No value");
                    }
                }
            } catch (Throwable t) {
                log.error("Failed to process option update.", t);
            }
        }
        return gotClosingLevel;
    }

    /**
     * Check if the closing level for specified opportunity has been received.
     *
     * @param o the opportunity to check for if the closing level is ready
     * @return <code>true</code> if the closing level is ready to be taken else <code>false</code>
     */
    @Override
    public boolean isClosingLevelReceived(Opportunity o) {
//        return isSnapshotReceived(o);
        return o.isClosingLevelReceived();
    }

    /**
     * Get the closing level for the specified opportunity.
     *
     * @param o the opportunity for which to get closing level
     * @return The level as received from Reuters. No formula and exposure control calculations.
     */
    @Override
    public double getClosingLevel(Opportunity o) {
        long decimal = o.getMarket().getDecimalPoint() - o.getMarket().getDecimalPointSubtractDigits();
        if (o.isUseManualClosingLevel()) {
            return roundDouble(o.getManualClosingLevel(), decimal);
        }
        double cl = roundDouble(o.getNextClosingLevel(), decimal);
        if (log.isTraceEnabled()) {
            log.trace("TRADINGDATA_" + itemName + " expiration - opportunityId: " + o.getId() + " expiration level: " + cl);
        }
        return cl;
    }


    protected String getSecondRicName() {
        return (itemName.endsWith(".Option+") ? itemName.substring(0, itemName.length() - 8) : itemName) + getSecondRicLetter(INDICES_DEFAULT_LETTER); //getFutureQuarterLetter(nextMonth) + getFutureYearDigit(nextMonth ? 1 : 0);
    }

    /**
     * Tell the item to open (subscribe) all its options (when you want to start using it).
     *
     * @param itm the Item instance to receive the updates for the options.
     */
    @Override
    public void subscribeOptions(Item itm) {
        super.subscribeOptions(itm);
       // boolean nextMonth = OpportunitiesManager.getOpportunityOptionsChangeMonth(marketId);
        secondRIC = (SecondRICOption) Option.getInstance(getSecondRicName());
   //   secondRIC = (NasdaqFOption) Option.getInstance(getSecondRicName()); //getSecondRicName(nextMonth));


        optionSubscribtionManager.subscribeOption(itm, secondRIC);
    }

    @Override
    public boolean isReutersStateOK() {
        return secondRIC.isReutersStateOK() && super.isReutersStateOK();
    }

    /**
     * Clear the options list (unsibscribe each option it remove).
     * Make sure you have lock over the options when you call this metod.
     *
     * @param itm the Item instance to be removed from the options.
     */
    @Override
    protected void clearOptions(Item itm) {
        super.clearOptions(itm);
        optionSubscribtionManager.unsubscribeOption(itm, secondRIC);
        secondRIC = null;
    }

    /**
     * @param o the opportunity for which to calc the level
     * @return The long term formula result for this item.
     */
    @Override
    protected double longTermFormula(Opportunity o) {
        return 0;
    }

    // start bugId: 2032
    /**
     * @return <code>ture</code> if the opened opportunities of this item
     *      should be enabled else <code>false</code>.
     */
    @Override
    public boolean shouldEnableOpportunities() {
        if (!valuesCheckDisable && super.shouldEnableOpportunities()) {
            log.warn("Updates resumed and values ok for market " + itemName);

            return true;
        }
        return false;
    }
    //end  bugId: 2032

    /**
     * Reset the market internal state in the end of the day to be ready for the next
     * day opening. Long term shiftings, updates flags etc.
     */
    @Override
    public void resetInTheEndOfTheDay() {
        super.resetInTheEndOfTheDay();

        lastPrimact1 = null;
        lastAsk = null;
        lastBid = null;
        valuesCheckDisable = false;
        lastTrdTim = null;
    }

    @Override
    public boolean isShouldBeClosed(long oppId) {
        Opportunity o = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            if (o.getId() == oppId) {
                if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
                    return System.currentTimeMillis() - secondRIC.getLastReutersUpdateReceiveTime() <= dontCloseAfterXSecNoUpdate * 1000 &&
                            System.currentTimeMillis() - lastReutersUpdateReceiveTime <= dontCloseAfterXSecNoUpdate * 1000;
                }
                break;
            }
        }
        return true;
    }
}