package il.co.etrader.service.level.items;

import java.math.BigDecimal;

import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.LevelService;

/**
 * CAC index current level calculation implementation.
 *
 * @author Tony
 */
public class CACIndexItem extends FTSEIndexItem {
    public CACIndexItem(String itemName, Handle handle) {
        super(itemName, handle);
    }
    
    @Override
    protected String getInterestName() {
    	return "EUR1MD=";
    }

    /**
     * @return Hook to get the param value.
     */
    @Override
    protected BigDecimal getParameter() {
        return LevelService.getCacParameter();
    }

    @Override
    protected String getSecondRicName() {
        //int mta = nextMonth ? 1 : 0; // months to add
        return "FCE" + getSecondRicLetter(INDICES_DEFAULT_LETTER) ; //+ getFutureMonthLetter(mta) + getFutureYearDigit(mta);
    }
}