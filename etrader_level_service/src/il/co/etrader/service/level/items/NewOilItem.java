package il.co.etrader.service.level.items;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ReutersQuotes;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.BigDecimalHelper;
import il.co.etrader.service.level.SenderThread;

/**
 * Handle Oil logic.
 *
 * @author Tony
 */
public class NewOilItem extends CommoditiesItem {
    private static final Logger log = Logger.getLogger(CommoditiesItem.class);

    public NewOilItem(String itemName, Handle handle) {
        super(itemName, handle);
    }

    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
    @Override
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        boolean gotClosingLevel = false;
        try {
            boolean haveRealLevel = false;
            boolean haveCurrentLevel = false;
            double crrLevelNoRandom = 0;
            double crrLevel = 0;

            Double trdprc1 = (Double) fields.get("TRDPRC_1");
            String salTim = (String) fields.get("SALTIM");
            if (null != salTim) {
                lastSALTIM = salTim;
            }
            if (null != trdprc1 && trdprc1 > 0) {
                lastReutersUpdateReceiveTime = System.currentTimeMillis();

                Date updateTime = null;
                if (null != lastSALTIM) {
                    try {
                        updateTime = getUpdateTime(lastSALTIM, "GMT");
                    } catch (Throwable t) {
                        log.error("Can't process SALTIM.", t);
                        updateTime = new Date(System.currentTimeMillis());
                    }
                } else {
                    log.error("Update without SALTIM!!!");
                    updateTime = new Date(System.currentTimeMillis());
                }

                String closingLevelTxt = "formula:round(TRDPRC_1);TRDPRC_1:" + trdprc1 + ";closeLevelBeforeRound:" + trdprc1;
                ReutersQuotes currentQuote = new ReutersQuotes(updateTime, trdprc1);
                gotClosingLevel = updateOpportunitiesClosingLevel(updateTime, trdprc1, trdprc1, closingLevelTxt, closingLevelTxt, currentQuote, currentQuote, opportunitiesTemp);
                haveRealLevel = true;
            }

            Double bid = (Double) fields.get("BID");
            Double ask = (Double) fields.get("ASK");
            if (null != bid && bid > 0 && null != ask && ask > 0) {
                lastReutersUpdateReceiveTime = System.currentTimeMillis();

                BigDecimal a = new BigDecimal(ask.toString());
                BigDecimal b = new BigDecimal(bid.toString());
                crrLevelNoRandom = a.add(b).divide(BigDecimalHelper.TWO).doubleValue();
                crrLevel = formulaRandomChange(crrLevelNoRandom);

                setSnapshotReceived(true);
                setLongTermSnapshotReceived(true);
                haveCurrentLevel = true;
            }
            if (haveRealLevel || haveCurrentLevel) {
               haveUpdate(sender, lifeId, haveCurrentLevel, crrLevelNoRandom, crrLevel, haveRealLevel, null != trdprc1 ? trdprc1 : 0);
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("No value");
                }
            }
        } catch (Throwable t) {
            log.error("Failed to process option update.", t);
        }
        return gotClosingLevel;
    }
}