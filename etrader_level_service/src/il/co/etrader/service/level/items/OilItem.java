package il.co.etrader.service.level.items;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ReutersQuotes;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.BigDecimalHelper;
import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.options.Option;

/**
 * Implements the current level/closing level calculation for Oil.
 *
 * @author Tony
 */
public class OilItem extends GoldItem {
    private static final Logger log = Logger.getLogger(OilItem.class);

    public OilItem(String itemName, Handle handle) {
        super(itemName, handle);

        dontCloseAfterXSecNoUpdate = 120;
    }

    /**
     * Notify the item that one of its options have received update.
     *
     * @param option the option that received update
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    @Override
    public boolean optionUpdated(Option option, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        boolean gotClosingLevel = false;
        if (option.getOptionName().equals(secondRIC.getOptionName())) {
            try {
                boolean haveRealLevel = false;
                boolean haveCurrentLevel = false;
                double crrLevelNoRandom = 0;
                double crrLevel = 0;

                Double trdprc1 = option.getLast();
                String trdTim = option.getActiveDate();
                if (null != trdTim) {
                    lastSALTIM = trdTim;
                }
                if (null != trdprc1 && trdprc1 > 0) {
                    lastReutersUpdateReceiveTime = System.currentTimeMillis();

                    Date updateTime = null;
                    if (null != lastSALTIM) {
                        try {
                            updateTime = getUpdateTime(lastSALTIM, "GMT");
                        } catch (Throwable t) {
                            log.error("Can't process TRDTIM_1.", t);
                            updateTime = new Date(System.currentTimeMillis());
                        }
                    } else {
                        log.error("TRDPRC_1 update without TRDTIM_1!!!");
                        updateTime = new Date(System.currentTimeMillis());
                    }

                    String closingLevelTxt = "formula:round(TRDPRC_1);TRDPRC_1:" + trdprc1 + ";closeLevelBeforeRound:" + trdprc1;
                    ReutersQuotes currentQuote = new ReutersQuotes(updateTime, trdprc1);
                    gotClosingLevel = updateOpportunitiesClosingLevel(updateTime, trdprc1, trdprc1, closingLevelTxt, closingLevelTxt, currentQuote, currentQuote, opportunitiesTemp);
                    haveRealLevel = true;
                }

                Double bid = option.getBid();
                Double ask = option.getAsk();

                if ((null != bid && bid > 0) || (null != ask && ask > 0)) {
                    lastReutersUpdateReceiveTime = System.currentTimeMillis();
                    if (null != ask) {
                        lastASK = new BigDecimal(ask.toString());
                    }
                    if (null != bid) {
                        lastBID = new BigDecimal(bid.toString());
                    }
                }
                log.trace("OilItem - trdprc1: " + trdprc1 + " lastASK: " + lastASK + " lastBID: " + lastBID);
                if (null != lastASK && null != lastBID) {
                    crrLevelNoRandom = lastASK.add(lastBID).divide(BigDecimalHelper.TWO).doubleValue();
                    crrLevel = formulaRandomChange(crrLevelNoRandom);

                    setSnapshotReceived(true);
                    setLongTermSnapshotReceived(true);
                    haveCurrentLevel = true;
                }
                if (haveRealLevel || haveCurrentLevel) {
                   haveUpdate(sender, lifeId, haveCurrentLevel, crrLevelNoRandom, crrLevel, haveRealLevel, null != trdprc1 ? trdprc1 : 0);
                } else {
                    if (log.isDebugEnabled()) {
                        log.debug("No value");
                    }
                }
            } catch (Throwable t) {
                log.error("Failed to process option update.", t);
            }
        }
        return gotClosingLevel;
    }

    @Override
    protected String getSecondRicName() {
    	String ricPrefix = null;

    	if (itemName.startsWith("CL")) {
    		ricPrefix = "CL";
    	} else if (itemName.startsWith("SI")) {
    		ricPrefix = "SI";
    	} else {
    		log.error("Unknown item name [" + itemName + "] for " + this.getClass().getSimpleName());
    		/* !!! WARNING !!! Method exit point! */
    		return null;
    	}

        //int mta = nextMonth ? 2 : 1; // months to add
        return ricPrefix + getSecondRicLetter(COMMODITIES_DEFAULT_LETTER);//+ getFutureMonthLetter(mta) + getFutureYearDigit(mta);
    }

}