package il.co.etrader.service.level.items;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ReutersQuotes;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.options.InterestOption;
import il.co.etrader.service.level.options.Option;

/**
 * Handle levels calculations for all China Stocks
 *
 * @author Eyal
 */
public class ChinaNewStockItem extends NewStockItem {
    private static final Logger log = Logger.getLogger(ChinaNewStockItem.class);

    protected double irgprc;

    public ChinaNewStockItem(String itemName, Handle handle) {
        super(itemName, handle);

        interest = (InterestOption) Option.getInstance("CNY1MD=");
    }

    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
    	Double currentIrgprc = (Double) fields.get("IRGPRC");
        if (null != fields.get("IRGPRC")) {
            lastReutersUpdateReceiveTime = System.currentTimeMillis();
            irgprc = (Double) currentIrgprc;
        }
        return processStockUpdate(fields, sender, lifeId, false, opportunitiesTemp);
    }

    /**
     * Get the closing level for the specified opportunity.
     *
     * @param w the opportunity for which to get closing level
     * @return The level as received from Reuters. No formula and exposure control calculations.
     */
    public double getClosingLevel(Opportunity o) {
        long decimal = o.getMarket().getDecimalPoint() - o.getMarket().getDecimalPointSubtractDigits();
        if (o.isUseManualClosingLevel()) {
            return roundDouble(o.getManualClosingLevel(), decimal);
        }
        if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
            double cl = roundDouble(o.getNextClosingLevel(), decimal);
            if (log.isInfoEnabled()) {
                log.info("TRADINGDATA_" + itemName +
                        " expiration - opportunityId: " + o.getId() +
                        " expiration level: " + cl);
            }
            return cl;
        } else {
            double cl = roundDouble(irgprc, decimal);
            if (log.isInfoEnabled()) {
                log.info("TRADINGDATA_" + itemName +
                        " expiration - opportunityId: " + o.getId() +
                        " IRGPRC: " + irgprc +
                        " expiration level: " + cl);
            }
            return cl;
        }
    }

    /**
     * Check if the closing level for specified opportunity has been received.
     *
     * @param o the opportunity to check for if the closing level is ready
     * @return <code>true</code> if the closing level is ready to be taken else <code>false</code>
     */
    public boolean isClosingLevelReceived(Opportunity o) {
        if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
//            return isSnapshotReceived(o);
            return o.isClosingLevelReceived();
        } else {
            return irgprc > 0;
        }
    }

    /**
     * Reset the market internal state in the end of the day to be ready for the next
     * day opening. Long term shiftings, updates flags etc.
     */
    public void resetInTheEndOfTheDay() {
        super.resetInTheEndOfTheDay();
        // clear internally stored last values
        irgprc = 0;
    }

    public String getClosrLevelTxtDay() {
		return "formula:round(IRGPRC);IRGPRC:" + irgprc + ";closeLevelBeforeRound:" + irgprc;
	}

    public boolean setUpdateOpportunitiesClosingLevel(Date updateTime, double crrRealLevel, double dailyClosingLevel, String closeLevelTxtHour, String closeLevelTxtDay, ArrayList<Opportunity> opportunitiesTemp) {
		return updateOpportunitiesClosingLevel(updateTime, crrRealLevel, irgprc, closeLevelTxtHour, closeLevelTxtDay, new ReutersQuotes(updateTime, lastTrdPrc, lastASK, lastBID), new ReutersQuotes(updateTime, irgprc), opportunitiesTemp);
	}

}