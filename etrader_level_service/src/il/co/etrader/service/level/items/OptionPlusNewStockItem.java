package il.co.etrader.service.level.items;

import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ReutersQuotes;
import com.reuters.rfa.common.Handle;

/**
 * Implement the formula for new stock item option + (except the ILS).
 * for stocks that we need as an option and not main ric bcoz same feed name cant be option and main ric
 *
 * @author Eyal
 */
public class OptionPlusNewStockItem extends OptionNewStockItem {
    private static final Logger log = Logger.getLogger(OptionPlusNewStockItem.class);

    public OptionPlusNewStockItem(String itemName, Handle handle) {
        super(itemName, handle);
    }

    /*
     * in option plus markets the hourly expiry level is trdprc
     */
    public boolean setUpdateOpportunitiesClosingLevel(Date updateTime, double crrRealLevel, double dailyClosingLevel, String closeLevelTxtHour, String closeLevelTxtDay, ArrayList<Opportunity> opportunitiesTemp) {
		return updateOpportunitiesClosingLevel(updateTime, lastTrdPrc.doubleValue(), dailyClosingLevel, closeLevelTxtHour, closeLevelTxtDay, new ReutersQuotes(updateTime, lastTrdPrc.doubleValue()), null, opportunitiesTemp);
	}


    public String getClosrLevelTxtHourly(double crrRealLevel) {
		return "formula:TRDPRC_1;TRDPRC_1:" + lastTrdPrc;
	}




}