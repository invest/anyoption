package il.co.etrader.service.level.items;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.options.EnergyOption;
import il.co.etrader.service.level.options.Option;

/**
 * Implements the current level/closing level calculation for Energy.
 *
 * @author Eyal
 */
public class EnergyItem extends Item {
    private static final Logger log = Logger.getLogger(EnergyItem.class);

    private static final BigDecimal O2 = new BigDecimal("0.20");
    private static final BigDecimal N91 = new BigDecimal("91");
    private static final BigDecimal N6 = new BigDecimal("6");
    private static final BigDecimal N470 = new BigDecimal("470");
    private static final BigDecimal N18 = new BigDecimal("18");
    private static final BigDecimal OO1 = new BigDecimal("0.1");
    //private static final BigDecimal OO2 = new BigDecimal("0.02");



    protected double lastHstClose2;
    protected EnergyOption deol;
    protected EnergyOption avnrp;
    protected EnergyOption nfta;
    protected EnergyOption israp;
    protected EnergyOption dedrp;
    protected EnergyOption dlkis;
    protected EnergyOption dral;

    public EnergyItem(String itemName, Handle handle) {
        super(itemName, handle);

        useMainRic = false;
        shouldBeClosedByMonitoring = true;

        deol = (EnergyOption) Option.getInstance("DLEN.TA"); //was deol
        avnrp = (EnergyOption) Option.getInstance("AVNRp.TA");
        nfta = (EnergyOption) Option.getInstance("NFTA.TA");
        israp = (EnergyOption) Option.getInstance("ISRAp.TA");
        dedrp = (EnergyOption) Option.getInstance("DEDRp.TA");
        dlkis = (EnergyOption) Option.getInstance("DLKIS.TA");
        dral = (EnergyOption) Option.getInstance("DRAL.TA");

    }

    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
    @Override
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        return false;
    }


    /**
     * Notify the item that one of its options have received update.
     *
     * @param option the option that received update
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    @Override
    public boolean optionUpdated(Option option, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        boolean gotClosingLevel = false;
        lastReutersUpdateReceiveTime = System.currentTimeMillis();
        try {
	        if (option.getHstClose2() != null) {
	        	if (deol.getHstClose2() != null && //check if they all have hstClose2
			        avnrp.getHstClose2() != null &&
			        nfta.getHstClose2() != null &&
			        israp.getHstClose2() != null &&
			        dedrp.getHstClose2() != null &&
			        dlkis.getHstClose2() != null &&
			        dral.getHstClose2() != null) {

	        		if (log.isTraceEnabled()) {
	        			log.trace("calc close level for item: " + itemName + " lastHstClose2: deol " + deol.getHstClose2() + " avnrp " + avnrp.getHstClose2() + " nfta " + nfta.getHstClose2() + " israp " + israp.getHstClose2() + " dedrp " + dedrp.getHstClose2() + " dlkis " + dlkis.getHstClose2() + " dral " + dral.getHstClose2());
	        		}

	        		lastHstClose2 = new BigDecimal(deol.getHstClose2()).multiply(O2).add(new BigDecimal(avnrp.getHstClose2()).multiply(N91)).add(new BigDecimal(nfta.getHstClose2()).multiply(N6)).add(new BigDecimal(israp.getHstClose2()).multiply(N470)).add(new BigDecimal(dedrp.getHstClose2()).multiply(N18)).add(new BigDecimal(dlkis.getHstClose2()).multiply(OO1)).add(new BigDecimal(dral.getHstClose2()).multiply(O2)).doubleValue();

	        		if (log.isTraceEnabled()) {
	        			log.trace("closing level for item: " + itemName + " lastHstClose2: " + lastHstClose2);
	        		}

	        		gotClosingLevel = true;
	        	} else {
	        		if (log.isTraceEnabled()) {
	                    log.trace("No value for 1 or more OFF_CLOSE fields deol " + deol.getHstClose2() + " avnrp " + avnrp.getHstClose2() + " nfta " + nfta.getHstClose2() + " israp " + israp.getHstClose2() + " dedrp " + dedrp.getHstClose2() + " dlkis " + dlkis.getHstClose2() + " dral " + dral.getHstClose2());
	                }
	        	}
	        }
	        if (option.getLast() != null) {
	        	if (deol.getLast() != null && //check if they all have TRDPRC_1
			        avnrp.getLast() != null &&
			        nfta.getLast() != null &&
			        israp.getLast() != null &&
			        dedrp.getLast() != null &&
			        dlkis.getLast() != null &&
			        dral.getLast() != null) {

	        		if (log.isTraceEnabled()) {
	                    log.trace("calc level for item: " + itemName + " lasts: deol " + deol.getLast() + " avnrp " + avnrp.getLast() + " nfta " + nfta.getLast() + " israp " + israp.getLast() + " dedrp " + dedrp.getLast() + " dlkis " + dlkis.getLast() + " dral " + dral.getLast());
	                }

	            	double crrRealLevel = new BigDecimal(deol.getLast()).multiply(O2).add(new BigDecimal(avnrp.getLast()).multiply(N91)).add(new BigDecimal(nfta.getLast()).multiply(N6)).add(new BigDecimal(israp.getLast()).multiply(N470)).add(new BigDecimal(dedrp.getLast()).multiply(N18)).add(new BigDecimal(dlkis.getLast()).multiply(OO1)).add(new BigDecimal(dral.getLast()).multiply(O2)).doubleValue();
	            	double crrLevel = formulaRandomChange(crrRealLevel);
	            	if (log.isTraceEnabled()) {
	        			log.trace("sending new level for item: " + itemName + " crrRealLevel: " + crrRealLevel + " with formula crrLevel " + crrLevel);
	        		}
	                haveUpdate(sender, lifeId, true, crrRealLevel, crrLevel, true, crrRealLevel);
	                setSnapshotReceived(true);
	                //needed?
	                setLongTermSnapshotReceived(true);
	            } else {
	            	if (log.isTraceEnabled()) {
	                    log.trace("No value for 1 or more last fields deol: " + deol.getLast() + " avnrp " + avnrp.getLast() + " nfta " + nfta.getLast() + " israp " + israp.getLast() + " dedrp " + dedrp.getLast() + " dlkis " + dlkis.getLast() + " dral " + dral.getLast());
	                }
	            }
	        }
        } catch (Exception e) {
        	log.warn("cant proccess update for item: " + itemName , e);
        }

        return gotClosingLevel;
    }

    /**
     * Tell the item to open (subscribe) all its options (when you want to start using it).
     *
     * @param itm the Item instance to receive the updates for the options.
     */
    @Override
    public void subscribeOptions(Item itm) {
        super.subscribeOptions(itm);
        optionSubscribtionManager.subscribeOption(itm, deol);
        optionSubscribtionManager.subscribeOption(itm, avnrp);
        optionSubscribtionManager.subscribeOption(itm, nfta);
        optionSubscribtionManager.subscribeOption(itm, israp);
        optionSubscribtionManager.subscribeOption(itm, dedrp);
        optionSubscribtionManager.subscribeOption(itm, dlkis);
        optionSubscribtionManager.subscribeOption(itm, dral);
    }

    @Override
    public boolean isReutersStateOK() {
        return deol.isReutersStateOK() && avnrp.isReutersStateOK() &&nfta.isReutersStateOK() && israp.isReutersStateOK() &&
        	   dedrp.isReutersStateOK() && dlkis.isReutersStateOK() && dral.isReutersStateOK() && super.isReutersStateOK();
    }

    /**
     * Clear the options list (unsibscribe each option it remove).
     * Make sure you have lock over the options when you call this metod.
     *
     * @param itm the Item instance to be removed from the options.
     */
    @Override
    protected void clearOptions(Item itm) {
        super.clearOptions(itm);
        optionSubscribtionManager.unsubscribeOption(itm, deol);
        optionSubscribtionManager.unsubscribeOption(itm, avnrp);
        optionSubscribtionManager.unsubscribeOption(itm, nfta);
        optionSubscribtionManager.unsubscribeOption(itm, israp);
        optionSubscribtionManager.unsubscribeOption(itm, dedrp);
        optionSubscribtionManager.unsubscribeOption(itm, dlkis);
        optionSubscribtionManager.unsubscribeOption(itm, dral);
    }

    /**
     * @param o the opportunity for which to calc the level
     * @return The long term formula result for this item.
     */
    @Override
    protected double longTermFormula(Opportunity o) {
        return 0;
    }

    /**
     * Check if the closing level for specified opportunity has been received.
     *
     * @param o the opportunity to check for if the closing level is ready
     * @return <code>true</code> if the closing level is ready to be taken else <code>false</code>
     */
    @Override
    public boolean isClosingLevelReceived(Opportunity o) {
    	return lastHstClose2 > 0;
    }

    /**
     * Get the closing level for the specified opportunity.
     *
     * @param o the opportunity for which to get closing level
     * @return The level as received from Reuters. No formula and exposure control calculations.
     */
    @Override
    public double getClosingLevel(Opportunity o) {
    	 long decimal = o.getMarket().getDecimalPoint() - o.getMarket().getDecimalPointSubtractDigits();
         if (o.isUseManualClosingLevel()) {
             return roundDouble(o.getManualClosingLevel(), decimal);
         }
         double cl = roundDouble(lastHstClose2, decimal);
         if (log.isInfoEnabled()) {
             log.info("TRADINGDATA_" + itemName +
                     " expiration - opportunityId: " + o.getId() +
                     " OFF_CLOSE: " + lastHstClose2 +
                     " expiration level: " + cl);
         }
         return cl;
    }

    /**
     * Reset the market internal state in the end of the day to be ready for the next
     * day opening. Long term shiftings, updates flags etc.
     */
    @Override
    public void resetInTheEndOfTheDay() {
        super.resetInTheEndOfTheDay();

        lastHstClose2 = 0;
    }

    @Override
    public boolean isShouldBeClosed(long oppId) {
        return true;
    }
}