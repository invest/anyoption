package il.co.etrader.service.level.items;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ReutersQuotes;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.BigDecimalHelper;
import il.co.etrader.service.level.LevelService;
import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.options.InterestOption;
import il.co.etrader.service.level.options.Option;

/**
 * Implement the stocks markets opportunities current/closing levels calculation.
 *
 * @author Tony
 */
public class StockItem extends Item {
    private static final Logger log = Logger.getLogger(StockItem.class);

    protected BigDecimal lastASK;
    protected BigDecimal lastBID;
    protected BigDecimal lastTrdPrc;
    protected double lastHstClose2;
    protected InterestOption interest;
    protected boolean valuesCheckDisable;
    protected String timeField;
    protected ArrayList<Double> lastFiveQuotes;
    protected String lastTrdTim;
    protected String timezone;

    public StockItem(String itemName, Handle handle) {
        super(itemName, handle);
        lastFiveQuotes = new ArrayList<Double>();

        interest = (InterestOption) Option.getInstance("ILS1MD=");
        valuesCheckDisable = false;
        timeField = "TIMACT";
        shouldBeClosedByMonitoring = true;
        timezone = "GMT";
    }

    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
    @Override
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        boolean gotClosingLevel = false;
        if (null != fields.get("OFF_CLOSE")) {
            lastReutersUpdateReceiveTime = System.currentTimeMillis();

            lastHstClose2 = (Double) fields.get("OFF_CLOSE");
            if (lastHstClose2 > 0) {
                if (log.isTraceEnabled()) {
                    log.trace("Got closing level. OFF_CLOSE: " + lastHstClose2);
                }
                gotClosingLevel = true;
            }
        }
        return processStockUpdate(fields, sender, lifeId, gotClosingLevel, opportunitiesTemp);
    }

    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @param gotClosingLevel what to return
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
    protected boolean processStockUpdate(HashMap<String, Object> fields, SenderThread sender, long lifeId, boolean gotClosingLevel, ArrayList<Opportunity> opportunitiesTemp) {
        Double crrTrdPrc1 = (Double) fields.get("TRDPRC_1");
        Double crrAsk = (Double) fields.get("ASK");
        Double crrBid = (Double) fields.get("BID");
        String timeAct = (String) fields.get(timeField);
        // validate the update (make sure we got at least 1 interesting field and we didn't get 0s)
        if ((null == crrTrdPrc1 && null == crrAsk && null == crrBid && null == timeAct) ||
                (null != crrTrdPrc1 && crrTrdPrc1 == 0) ||
                (null != crrAsk && crrAsk == 0) ||
                (null != crrBid && crrBid == 0)) {
            return gotClosingLevel;
        }

        lastReutersUpdateReceiveTime = System.currentTimeMillis();

        if (null != crrAsk) {
            lastASK = new BigDecimal(crrAsk.toString());
        }
        if (null != crrBid) {
            lastBID = new BigDecimal(crrBid.toString());
        }
        if (null != crrTrdPrc1) {
            lastTrdPrc = new BigDecimal(crrTrdPrc1.toString());
        }

        if (null != timeAct && !timeAct.trim().equals("")) {
            lastTrdTim = timeAct;
        }

        if (null != lastTrdPrc && null != lastASK && null != lastBID) {
            setSnapshotReceived(true);
            if (interest.isSnapshotReceived()) {
                setLongTermSnapshotReceived(true);
            }

            double crrRealLevel = lastTrdPrc.add(lastASK).add(lastBID).divide(BigDecimalHelper.THREE, BigDecimalHelper.BIG_DECIMAL_CALC_SCALE, RoundingMode.HALF_UP).doubleValue();

            lastFiveQuotes.add(crrRealLevel);
            if (lastFiveQuotes.size() > 5) {
                lastFiveQuotes.remove(0);
            }
            double min = Double.MAX_VALUE;
            double max = Double.MIN_VALUE;
            double sum = 0;
            double crr;
            for (int i = 0; i < lastFiveQuotes.size(); i++) {
                crr = lastFiveQuotes.get(i);
                if (crr < min) {
                    min = crr;
                }
                if (crr > max) {
                    max = crr;
                }
                sum += crr;
            }

            double avg = (sum - (lastFiveQuotes.size() > 3 ? min : 0) - (lastFiveQuotes.size() > 4 ? max : 0)) / (lastFiveQuotes.size() == 5 || lastFiveQuotes.size() == 4 ? 3 : lastFiveQuotes.size());

            if (null != crrTrdPrc1) {
                Date updateTime = null;
                if (null != lastTrdTim) {
                    try {
                        updateTime = getUpdateTime(lastTrdTim, timezone);
                    } catch (Throwable t) {
                        log.error("Can't process " + timeField, t);
                        updateTime = new Date(System.currentTimeMillis());
                    }
                } else {
                    log.error("TRDPRC_1 update without " + timeField + "!!!");
                    updateTime = new Date(System.currentTimeMillis());
                }

                String closeLevelTxtHour = getClosrLevelTxtHourly(crrRealLevel);
                String closeLevelTxtDay = getClosrLevelTxtDay();
                gotClosingLevel = gotClosingLevel || setUpdateOpportunitiesClosingLevel(updateTime, crrRealLevel, lastTrdPrc.doubleValue(), closeLevelTxtHour, closeLevelTxtDay, opportunitiesTemp);
            }

            double crrFormulaResult = formulaRandomChange(avg);
            if (hasOpenedOpportunities() && !hasOpenedHourly()) {
            	Opportunity o = getOpenedOppByScheduled(Opportunity.SCHEDULED_DAYLY);
    			if (null != o && (o.getState() == Opportunity.STATE_CLOSING_1_MIN || o.getState() == Opportunity.STATE_CLOSING)) {
                    if (log.isTraceEnabled()) {
                        log.trace("Replace real level with TRDPRC_1");
                    }
	                crrRealLevel = lastTrdPrc.doubleValue();
                }
            }
            haveUpdate(sender, lifeId, true, avg, crrFormulaResult, true, crrRealLevel);

            if (log.isDebugEnabled()) {
                log.debug(itemName +
                        " ASK: " + lastASK +
                        " BID: " + lastBID +
                        " TRDPRC_1: " + lastTrdPrc +
                        " realLevel: " + crrRealLevel +
                        " crrLevel: " + crrFormulaResult);
            }

            boolean c1 = lastASK.subtract(lastBID).compareTo(lastTrdPrc.multiply(new BigDecimal("0.02"))) > 0;
            boolean c2 = lastBID.compareTo(lastASK) > 0;
            if ((c1 || c2) && hasOpenedNotExpiringOpportunities()) {
                String desc = "";
                desc += c1 ? "(ASK - BID) > LAST * 0.02; " : "";
                desc += c2 ? "(ASK - BID) < 0; " : "";
                if (disableOpportunities(desc)) {
                    valuesCheckDisable = true;
                    String str =
                            "Stock " + itemName + " " + desc +
                            " ASK = " + lastASK +
                            " BID = " + lastBID +
                            " TRDPRC_1 = " + lastTrdPrc;
                    log.warn(str);
                    LevelService.getInstance().sendAlertEmail("Values check disable " + itemName, str);
                }
            } else {
                if (enableOpportunities("Values check enable.", false)) {
                    valuesCheckDisable = false;
                    String str =
                            "Stock " + itemName + " Values back to normal." +
                            " ASK = " + lastASK +
                            " BID = " + lastBID +
                            " TRDPRC_1 = " + lastTrdPrc;
                    log.warn(str);
                    LevelService.getInstance().sendAlertEmail("Values check enable " + itemName, str);
                }
            }
        }
        return gotClosingLevel;
    }

    public boolean setUpdateOpportunitiesClosingLevel(Date updateTime, double crrRealLevel, double dailyClosingLevel, String closeLevelTxtHour, String closeLevelTxtDay, ArrayList<Opportunity> opportunitiesTemp) {
		return updateOpportunitiesClosingLevel(updateTime, crrRealLevel, dailyClosingLevel, closeLevelTxtHour, closeLevelTxtDay, new ReutersQuotes(updateTime, lastTrdPrc, lastASK, lastBID), new ReutersQuotes(updateTime, dailyClosingLevel), opportunitiesTemp);
	}

	public String getClosrLevelTxtDay() {
		return "formula:round(OFF_CLOSE);OFF_CLOSE:" + lastHstClose2 + ";closeLevelBeforeRound:" + lastHstClose2;
	}

	public String getClosrLevelTxtHourly(double crrRealLevel) {
		return "formula:round((TRDPRC_1+ASK+BID)/3);TRDPRC_1:" + lastTrdPrc
				+ ";ask:" + lastASK.toString()
				+ ";bid:" + lastBID.toString()
				+ ";closeLevelBeforeRound:" + crrRealLevel ;
	}

	/**
     * Notify the item that one of its options have received update.
     *
     * @param option the option that received update
     * @param jmsHandler
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
	@Override
    public boolean optionUpdated(Option option, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        if (option.getOptionName().equals(interest.getOptionName())) {
            if (isSnapshotReceived() && interest.isSnapshotReceived()) {
                setLongTermSnapshotReceived(true);
                sendUpdate(sender, lifeId, UPDATE_LONG_TERM_OPPS, true, false);
            }
        }
        return false;
    }

    /**
     * @param o the opportunity for which to calc the level
     * @return The long term formula result for this item.
     */
	@Override
    protected double longTermFormula(Opportunity o) {
        return calcLongTermLevel(o);
    }

    /**
     * Tell the item to open (subscribe) all its options (when you want to start using it).
     *
     * @param itm the Item instance to receive the updates for the options.
     */
	@Override
    public void subscribeOptions(Item itm) {
        super.subscribeOptions(itm);
        optionSubscribtionManager.subscribeOption(itm, interest);
    }

	@Override
    public boolean isReutersStateOK() {
        return interest.isReutersStateOK() && super.isReutersStateOK();
    }

    /**
     * Clear the options list (unsibscribe each option it remove).
     * Make sure you have lock over the options when you call this metod.
     *
     * @param itm the Item instance to be removed from the options.
     */
	@Override
    protected void clearOptions(Item itm) {
        super.clearOptions(itm);
        optionSubscribtionManager.unsubscribeOption(itm, interest);
    }

    /**
     * Check if the closing level for specified opportunity has been received.
     *
     * @param o the opportunity to check for if the closing level is ready
     * @return <code>true</code> if the closing level is ready to be taken else <code>false</code>
     */
	@Override
    public boolean isClosingLevelReceived(Opportunity o) {
        if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
//            return isSnapshotReceived(o);
            return o.isClosingLevelReceived();
        } else {
            return lastHstClose2 > 0;
        }
    }

    /**
     * Get the closing level for the specified opportunity.
     *
     * @param w the opportunity for which to get closing level
     * @return The level as received from Reuters. No formula and exposure control calculations.
     */
	@Override
    public double getClosingLevel(Opportunity o) {
        long decimal = o.getMarket().getDecimalPoint() - o.getMarket().getDecimalPointSubtractDigits();
/*        if (!(itemName.endsWith("ISRAp.TA") || itemName.equals("RATIp.TA"))) {
            decimal -= 1;
        }*/
        if (o.isUseManualClosingLevel()) {
            return roundDouble(o.getManualClosingLevel(), decimal);
        }
        if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
            double cl = roundDouble(o.getNextClosingLevel(), decimal);
            if (log.isInfoEnabled()) {
                log.info("TRADINGDATA_" + itemName +
                        " expiration - opportunityId: " + o.getId() +
                        " expiration level: " + cl);
            }
            return cl;
        } else {
            double cl = roundDouble(lastHstClose2, decimal);
            if (log.isInfoEnabled()) {
                log.info("TRADINGDATA_" + itemName +
                        " expiration - opportunityId: " + o.getId() +
                        " OFF_CLOSE: " + lastHstClose2 +
                        " expiration level: " + cl);
            }
            return cl;
        }
    }

    /**
     * @param o the opportunity for which to calc the level
     * @return The week/month level for this item.
     */
    protected double calcLongTermLevel(Opportunity o) {
    	return ItemsUtil.calcIndexAndStockLongTermLevel(itemName, o, interest, lastCalcResult);
    }

    /**
     * @return <code>ture</code> if the opened opportunities of this item
     *      should be enabled else <code>false</code>.
     */
    @Override
    public boolean shouldEnableOpportunities() {
    	log.trace("StockItem.shouldEnableOpportunities - valuesCheckDisable: " + valuesCheckDisable);
        if (!valuesCheckDisable && super.shouldEnableOpportunities()) {
            log.warn("Updates resumed and values ok for market " + itemName);

            return true;
        }
        return false;
    }

    /**
     * Reset the market internal state in the end of the day to be ready for the next
     * day opening. Long term shiftings, updates flags etc.
     */
    @Override
    public void resetInTheEndOfTheDay() {
        super.resetInTheEndOfTheDay();

        // clear internally stored last values
        lastASK = null;
        lastBID = null;
        lastTrdPrc = null;
        lastHstClose2 = 0;
        valuesCheckDisable = false;
        lastFiveQuotes.clear();
        lastTrdTim = null;
    }

    @Override
    public boolean isShouldBeClosed(long oppId) {
        return true;
    }
}