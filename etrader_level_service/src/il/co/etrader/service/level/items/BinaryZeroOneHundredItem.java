//package il.co.etrader.service.level.items;
//
//import java.sql.SQLException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.BinaryZeroOneHundred;
//import com.anyoption.common.beans.Opportunity;
//import com.anyoption.common.bl_vos.InvestmentType;
//import com.anyoption.common.enums.SkinGroup;
//import com.anyoption.common.jms.ETraderFeedMessage;
//import com.reuters.rfa.common.Handle;
//
//import il.co.etrader.service.level.LevelService;
//import il.co.etrader.service.level.OpportunitiesManager;
//import il.co.etrader.service.level.OptionSubscribtionManager;
//import il.co.etrader.service.level.SenderThread;
//import il.co.etrader.service.level.options.Option;
//import il.co.etrader.util.CommonUtil;
//
///**
// * Implement the formula for 0100 except currency items.
// *
// * @author Eyal
// */
//public class BinaryZeroOneHundredItem extends Item {
//    private static final Logger log = Logger.getLogger(BinaryZeroOneHundredItem.class);
//    protected Item binaryMarket;
//    private boolean isTradingHalt;
////    protected double lastOffer;
////    protected double lastBid;
//    private static final int BINARY0100_DECIMAL_POINT = 1;
//
//    public BinaryZeroOneHundredItem(String itemName, Handle handle) {
//        super(itemName, handle);
//
//        useMainRic = false;
//        //TODO: shoud we change it?
//        shouldBeClosedByMonitoring = false;
//        log.debug("item name: " + itemName + " after substring: " + itemName.substring(0, itemName.length() - 12) + " binary market: " + itemName.substring(0, itemName.length() - 12).concat(".Option+"));
//        binaryMarket = Item.getInstance(itemName.substring(0, itemName.length() - 12).concat(".Option+"), handle);
//    }
//
//    /**
//     * Notify the item about update for it received from Reuters.
//     *
//     * @param fields the fields and values received from reuters
//     * @param sender
//     * @param lifeId
//     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
//     */
//    @Override
//    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
//        return false;
//    }
//
//    /**
//     * Tell the item to open (subscribe) all its options (when you want to start using it).
//     *
//     * @param itm the Item instance to receive the updates for the options.
//     */
//    @Override
//    public void subscribeOptions(Item itm) {
//        super.subscribeOptions(itm);
//        binaryMarket.subscribeOptions(itm);
//    }
//
//    @Override
//    public boolean isReutersStateOK() {
//        return binaryMarket.isReutersStateOK();
//    }
//
//    /**
//     * Clear the options list (unsibscribe each option it remove).
//     * Make sure you have lock over the options when you call this metod.
//     *
//     * @param itm the Item instance to be removed from the options.
//     */
//    @Override
//    protected void clearOptions(Item itm) {
//        super.clearOptions(itm);
//        binaryMarket.clearOptions(itm);
//    }
//
//	@Override
//	public double getClosingLevel(Opportunity w) {
//		return binaryMarket.getClosingLevel(w);
//	}
//
//	@Override
//	public boolean isClosingLevelReceived(Opportunity w) {
//		return binaryMarket.isClosingLevelReceived(w);
//	}
//
//	@Override
//	public boolean isShouldBeClosed(long l) {
//		return true;
//	}
//
//	@Override
//	protected double longTermFormula(Opportunity o) {
//		return 0;
//	}
//
//	@Override
//	public boolean optionUpdated(Option option, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
//		log.debug("got optionUpdated  BinaryZeroOneHundredItem");
//		boolean isClose = binaryMarket.optionUpdated(option, sender, lifeId, opportunities);
//		log.debug("got optionUpdated BinaryZeroOneHundredItem binaryMarket");
//		setSnapshotReceived(binaryMarket.isSnapshotReceived());
//		lastReutersUpdateReceiveTime = System.currentTimeMillis();
////		double currentLevel = binaryMarket.lastCalcResult;
////		Opportunity currentOpenOpp = getOpenedOppByScheduled(Opportunity.SCHEDULED_HOURLY);
////		long timeToExpiry = (currentOpenOpp.getTimeEstClosing().getTime() - System.currentTimeMillis()) / 1000;
////		double invDelta = currentOpenOpp.getContractsBought() - currentOpenOpp.getContractsSold();
////		double promileDistance = Math.abs((currentLevel - currentOpenOpp.getCurrentLevel())/currentLevel) * 1000;
////		double x = 0;
//////		if (currentOpenOpp.getCurrentLevel() - currentLevel < 0) {
////			if (currentOpenOpp.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_ABOVE) {
////				log.debug("above type = "  + currentOpenOpp.getOpportunityTypeId() + " diff = " + (currentLevel - currentOpenOpp.getCurrentLevel())/currentLevel);
////				if (currentOpenOpp.getCurrentLevel() - currentLevel < 0) {
////					x = currentOpenOpp.getBinaryZeroOneHundred().getFormulaResultPositive(promileDistance, invDelta, timeToExpiry);
////				} else {
////					x = currentOpenOpp.getBinaryZeroOneHundred().getFormulaResultNegative(promileDistance, invDelta, timeToExpiry);
////				}
////			} else {
////				log.debug("below type = "  + currentOpenOpp.getOpportunityTypeId() + " diff = " + (currentLevel - currentOpenOpp.getCurrentLevel())/currentLevel);
////				if (currentOpenOpp.getCurrentLevel() - currentLevel < 0) {
////					x = currentOpenOpp.getBinaryZeroOneHundred().getFormulaResultNegative(promileDistance, invDelta, timeToExpiry);
////				} else {
////					x = currentOpenOpp.getBinaryZeroOneHundred().getFormulaResultPositive(promileDistance, invDelta, timeToExpiry);
////				}
////			}
////			lastOffer = x + currentOpenOpp.getBinaryZeroOneHundred().getQ() / 2;
////			lastBid = x - currentOpenOpp.getBinaryZeroOneHundred().getQ() / 2;
////			if (lastOffer > 100) {
////				lastOffer = 100;
////			} else if (lastOffer < 7.5) {
////				lastOffer = 7.5;
////			}
////
////			if (lastBid > 95) {
////				lastBid = 95;
////			} else if (lastBid < 0) {
////				lastBid = 0;
////			}
//////		}
////		log.debug("currentLevel = " + currentLevel + " promileDistance = " + promileDistance + " x = " + x + " lastoffer = " + lastOffer + " last bid = " + lastBid);
//		sendUpdate(sender, lifeId, UPDATE_ALL_OPPS, true, false);
//		return isClose;
//	}
//
//	/**
//     * Send update for item opp.
//     *
//     * @param sender
//     * @param lifeId
//     * @param o
//     * @param forseDelete if to forse "DELETE" command or to determine the command by the state
//     * @param sendCurrentLevel if to send current level update in this message (LS update) or just the real level
//     */
//	@Override
//    public void sendUpdate(SenderThread sender, long lifeId, Opportunity o, boolean forseDelete, boolean sendCurrentLevel) {
//        ETraderFeedMessage toSend = new ETraderFeedMessage(
//                //o == specialEntry?onHomePage:new HashMap<Integer, Boolean>(), //only if its specialEntry send the real hashmap else send new 1 its will act like all have false on home page
//        		null,
//        		sendCurrentLevel && (forseDelete || o == specialEntry || !suspended) ? getOpportunityLevelUpdate(o, forseDelete) : null,
//                lifeId,
//                binaryMarket.lastCalcResult, //use by insurance adapter (GM/RF)
//                o.getMarket().getGroupId(),
//                o.getId(),
//                0,
//                0, //not in use
//                null,
//        		o.getAutoShiftParameter(),
//         		o.getCalls(),
//         		o.getPuts(),
//         		itemName,
//         		decimalPoint,
//         		o.isDisabledTrader(),
//         		disabled,
//         		CommonUtil.roundDouble(o.getContractsBought(), 2),
//         		CommonUtil.roundDouble(o.getContractsSold(), 2),
//         		o.getBinaryZeroOneHundred(),
//         		0,//bid not in use for updates only for deviation check
//         		0,//offer not in use for updates only for deviation check
//         		null,
//         		!o.isClosingLevelReceived(),
//         		o.getSkinGroupMappings()
//        );
//        sender.send(toSend);
//    }
//
//	/**
//     * Prepare the <code>HashMap</code> with the values for the LS update.
//     *
//     * @param o
//     * @param levelET the current opportunity level for ET
//     * @param levelAO the current opportunity level for AO
//     * @param forseDelete if to forse "DELETE" command or to determine the command by the state
//     */
//    private HashMap<String, String> getOpportunityLevelUpdate(Opportunity o, boolean forseDelete) {
//        HashMap<String, String> flds = new HashMap<String, String>();
//        flds.put("key", String.valueOf(o.getId()));
//        flds.put("command", (forseDelete || o.getState() == Opportunity.STATE_DONE ? "DELETE" : "UPDATE"));
//        flds.put("BZ_MARKET_ID", String.valueOf(o.getMarketId()));
//        flds.put("BZ_EVENT", String.valueOf(o.getCurrentLevel())); //the binary 0-100 event level
//        Binary0100Quote q = getOpportunityQuote(o);
//        flds.put("BZ_OFFER", formatLevel(q.offer, BINARY0100_DECIMAL_POINT)); //the OFFER price
//        flds.put("BZ_BID", formatLevel(q.bid, BINARY0100_DECIMAL_POINT)); //the BID price
//        double levelToSend = binaryMarket.lastCalcResult;
//        if (o.getState() == Opportunity.STATE_CLOSED) {
//            levelToSend = getClosingLevel(o);
//        }
//        flds.put("BZ_CURRENT", formatLevel(levelToSend, (int) decimalPoint)); // current level
//
//        flds.put("BZ_LAST_INV", formatDate(o.getTimeLastInvest())); //time last invest
//        flds.put("BZ_EST_CLOSE", getTimeEstCloseForLevelUpdate(o)); //estimate close time
//        flds.put("BZ_SUSPENDED_MESSAGE", suspendedMessage); //suspend msg
//
//        flds.put("BZ_STATE", getOppStateForLevelUpdate(o)); //the state of the opportunity (OPENED, SUSPENDED etc)
//
//        flds.put("AO_TYPE", String.valueOf(o.getOpportunityTypeId())); //the opportunity type id
//        flds.put("AO_TS", String.valueOf(System.currentTimeMillis()));
//
//        flds.put("AO_OPP_STATE", o.isSuspended() ? "1" : "0");
//        
//        return flds;
//    }
//
//    /**
//     * get the current level
//     * @return current level
//     */
//    protected double getBinaryMarketCurrentLevel() {
//    	log.debug("current level = " + binaryMarket.lastCalcResult) ;
//    	return binaryMarket.lastCalcResult;
//    }
//
//    private Binary0100Quote getOpportunityQuote(Opportunity o) {
//    	log.debug("calculate opp qoute for opp id = " + o.getId() + " params = " + o.getBinaryZeroOneHundred().toString());
//        Binary0100Quote q = new Binary0100Quote();
//        double currentLevel = getBinaryMarketCurrentLevel();
//        if (currentLevel == 0) { //first update when loading opps and nothing in the option
//        	log.debug("got current Level " + currentLevel + " sending offer and bid = 0 ");
//        	q.offer = 0;
//            q.bid = 0;
//            return q;
//        }
//        long timeToExpiry = (o.getTimeEstClosing().getTime() - System.currentTimeMillis()) / 1000;
//        double invDelta = CommonUtil.roundDouble(o.getContractsBought(), 2) -  CommonUtil.roundDouble(o.getContractsSold(), 2);
//        double promileDistance = Math.abs((currentLevel - o.getCurrentLevel()) / currentLevel) * 1000;
//        double x = 0;
//        double levelDiff = o.getCurrentLevel() / currentLevel;
//        if (0.99996 < levelDiff && levelDiff < 1.00004) {
//        	x = 50 + (invDelta / o.getBinaryZeroOneHundred().getZ())  + o.getBinaryZeroOneHundred().getT();
//        } else {
//	        if (o.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_ABOVE || o.getOpportunityTypeId() == Opportunity.TYPE_DYNAMICS) {
//	            log.debug("above type = "  + o.getOpportunityTypeId() + " diff = " + (currentLevel - o.getCurrentLevel()) / currentLevel);
//	            if (o.getCurrentLevel() - currentLevel < 0) {
//	                x = o.getBinaryZeroOneHundred().getFormulaResultPositive(promileDistance, invDelta, timeToExpiry);
//	            } else {
//	                x = o.getBinaryZeroOneHundred().getFormulaResultNegative(promileDistance, invDelta, timeToExpiry);
//	            }
//	        } else {
//	            log.debug("below type = "  + o.getOpportunityTypeId() + " diff = " + (currentLevel - o.getCurrentLevel()) / currentLevel);
//	            if (o.getCurrentLevel() - currentLevel < 0) {
//	                x = o.getBinaryZeroOneHundred().getFormulaResultNegative(promileDistance, invDelta, timeToExpiry);
//	            } else {
//	                x = o.getBinaryZeroOneHundred().getFormulaResultPositive(promileDistance, invDelta, timeToExpiry);
//	            }
//	        }
//        }
//        q.offer = x + o.getBinaryZeroOneHundred().getQ() / 2;
//        q.bid = x - o.getBinaryZeroOneHundred().getQ() / 2;
//        if (q.offer > 100) {
//            q.offer = 100;
//        } else if (q.offer < 11) {
//            q.offer = 11;
//        }
//
//        if (q.bid > 89) {
//            q.bid = 89;
//        } else if (q.bid < 0) {
//            q.bid = 0;
//        }
//        log.debug("currentLevel = " + currentLevel + " promileDistance = " + promileDistance + " x = " + x + " lastoffer = " + q.offer + " last bid = " + q.bid);
//        return q;
//    }
//
//    /**
//     * Handle investment notification for binary0-100. Here is where the exposure flow calculations are done
//     * as the exposure can change after an investment.
//    /**
//     * Handle investment notification for binary0-100.
//     *
//     * @param oppId the id of opp on which investment was made
//     * @param contracts the number of contracts that bought or sold
//     * @param amount the investments count
//     * @param type the type of investment (3 - buy/4 - sell)
//     * @return <code>true</code> if this investment caused stop/start trading else <code>false</code>.
//     */
//    @Override
//    public boolean investmentNotification(long oppId, double contracts, Double amount, long type, double aboveTotal, double belowTotal, long skinId) {
//        boolean isChangeState = false;
//        Opportunity opp = getOppById(oppId);
//        // first update the amounts and the contracts
//        double contracts2DP = CommonUtil.roundDouble(contracts, 2);
//        log.debug("opp calls " + opp.getCalls() + " above total " + aboveTotal);
//        log.debug("opp Puts " + opp.getPuts() + " below total " + belowTotal);
//        opp.setCalls(opp.getCalls() + aboveTotal); //if above
//        opp.setPuts(opp.getPuts() + belowTotal); //if below
//        log.debug("after opp calls " + opp.getCalls());
//        log.debug("after opp Puts " + opp.getPuts());
//        if (type == InvestmentType.BUY || type == InvestmentType.DYNAMICS_BUY) {
//            opp.setContractsBought(CommonUtil.roundDouble(opp.getContractsBought(), 2) + contracts2DP);
//        } else {
//            opp.setContractsSold( CommonUtil.roundDouble(opp.getContractsSold(), 2) + contracts2DP);
//        }
//
//        double exposure = (CommonUtil.roundDouble(opp.getContractsBought(), 2) - CommonUtil.roundDouble(opp.getContractsSold(), 2)) / opp.getBinaryZeroOneHundred().getZ();
//
//        if (log.isInfoEnabled()) {
//            log.info("Investment notification binary Zero One Hundred -" +
//                    " item: " + itemName +
//                    " opportunity: " + opp.getId() +
//                    " exposure: " + exposure);
//        }
//        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyyy HH:mm:ss");
//        String desc = "Market: " + itemName +
//        " opportunity: " + opp.getId() +
//        " time: " + df.format(new Date(System.currentTimeMillis())) +
//        " contracts bought: " + CommonUtil.roundDouble(opp.getContractsBought(), 2) +
//        " contracts sold: " + CommonUtil.roundDouble(opp.getContractsSold(), 2) +
//        " Q: " +  opp.getBinaryZeroOneHundred().getQ() +
//		" T: " +  opp.getBinaryZeroOneHundred().getT() +
//		" S: " +  opp.getBinaryZeroOneHundred().getCThreashold();
//        if (Math.abs(exposure) >= opp.getBinaryZeroOneHundred().getCThreashold()) {
//        	if (!opp.isDisabledService()) {
//	        	desc = "stop trading " + desc;
//	        	log.warn(desc);
//	            LevelService.getInstance().sendBinray0100TradingHaltEmail("stop trading " + itemName, desc);
//	            opp.setDisabledService(true);
//        	}
//        } else {
//        	if (opp.isDisabledService()) {
//	        	desc = "start trading " + desc;
//	            log.warn(desc);
//	            LevelService.getInstance().sendBinray0100TradingHaltEmail("start trading " + itemName, desc);
//	            opp.setDisabledService(false);
//        	}
//        }
//        return isChangeState;
//    }
//
//    @Override
//    public void getOpenedOpportunityLevels(Opportunity o, ETraderFeedMessage toSend, SkinGroup skinGroup) {
//        Binary0100Quote q = getOpportunityQuote(o);
//        toSend.realLevel = binaryMarket.lastRealLevel;
//        toSend.devCheckLevel = binaryMarket.lastCalcResult;
//        toSend.isAutoDisable = Math.abs((CommonUtil.roundDouble(o.getContractsBought(), 2) -  CommonUtil.roundDouble(o.getContractsSold(), 2)) / o.getBinaryZeroOneHundred().getZ()) >= o.getBinaryZeroOneHundred().getCthreashold();
//        toSend.offer = q.offer;
//        toSend.bid = q.bid;
//        toSend.investmentsRejectInfo = "Contracts Bought: " + CommonUtil.roundDouble(o.getContractsBought(), 2) + " , Contracts Sold: " +  CommonUtil.roundDouble(o.getContractsSold(), 2) +
//        									" , Z: " + o.getBinaryZeroOneHundred().getZ() + " , C: " + o.getBinaryZeroOneHundred().getCthreashold();
//    }
//
//    @Override
//    public void openOpportunity(Opportunity o) {
//    	log.debug("openOpportunity " + o.getId());
//    	try {
//    		double eventLevel = roundDouble(binaryMarket.lastCalcResult, decimalPoint);
//			OpportunitiesManager.updateOppCurrentLevel(o.getId(), eventLevel);
//			o.setCurrentLevel(eventLevel);
//		} catch (SQLException e) {
//			log.error("cant write event level for oppId " + o.getId());
//		}
//
//    }
//
//    /**
//     * Set the option subscribtion manager to use for subscribe/unsubscribe options.
//     *
//     * @param optionSubscribtionManager
//     */
//    @Override
//    public void setOptionSubscribtionManager(OptionSubscribtionManager optionSubscribtionManager) {
//        super.setOptionSubscribtionManager(optionSubscribtionManager);
//        binaryMarket.setOptionSubscribtionManager(optionSubscribtionManager);
//    }
//
//    /**
//     * Add opportunity to the RICs list.
//     *
//     * @param o
//     * @param sender handler to send update about the opp on the home page from this market
//     * @param lifeId the service life id
//     */
//    @Override
//    public void addOpportunity(Opportunity o, SenderThread sender, long lifeId) {
//    	if (o.getQuoteParams() != null) {
//    		o.setBinaryZeroOneHundred(new BinaryZeroOneHundred(o.getQuoteParams()));
//    	}
//        super.addOpportunity(o, sender, lifeId);
//        binaryMarket.marketId = o.getMarket().getId();
//        binaryMarket.optionPlusMarketId = o.getMarket().getOptionPlusMarketId();
//        binaryMarket.significantPercentage = o.getMarket().getSignificantPercentage();
//        binaryMarket.realSignificantPercentage = o.getMarket().getRealSignificantPercentage();
//    }
//
//    /**
//     * @return the longTermSnapshotReceived
//     */
//    @Override
//    public boolean isLongTermSnapshotReceived() {
//        return true;
//    }
//
//    @Override
//    public double getLastRealLevel() {
//        return binaryMarket.lastRealLevel;
//    }
//
//    @Override
//    public double getGraphLevel(SkinGroup skinGroup) {
//    	return binaryMarket.lastCalcResult;
//    }
//
//    @Override
//    public double getDayLevel(SkinGroup skinGroup) {
//    	return binaryMarket.lastCalcResult;
//    }
//
//    public class Binary0100Quote {
//        double offer;
//        double bid;
//    }
//
//    /**
//     * set snap shot received and also set the event level if its 0
//     */
//    @Override
//    public void setSnapshotReceived(boolean snapshotReceived) {
//    	log.debug("setSnapshotReceived 0100 this.snapshotReceived = " + this.snapshotReceived + " , snapshotReceived = " + snapshotReceived);
//        if (!this.snapshotReceived && snapshotReceived) {
//        	this.snapshotReceived = snapshotReceived;
//            setTodayOpeningTime(System.currentTimeMillis());
//            LevelService lvlSrv = LevelService.getInstance();
//            refreshSpecialEntry(lvlSrv.getSender(), lvlSrv.getLifeId());
//            lvlSrv.updateHomePageOpportunities();
//            Opportunity o = null;
//            for (int i = 0; i < opportunities.size(); i++) {
//                o = opportunities.get(i);
//                log.debug("o.isInOppenedState() " + o.isInOppenedState() + " , o.getCurrentLevel() " + o.getCurrentLevel());
//                if (o.isInOppenedState() && o.getCurrentLevel() == 0) {
//                	openOpportunity(o);
//                }
//            }
//        } else {
//            this.snapshotReceived = snapshotReceived;
//        }
//    }
//
//    @Override
//    public String getOppStateForLevelUpdate(Opportunity o) {
//        if (o.isAvailableForTrading() && o.isDisabledService()) {
//            return String.valueOf(Opportunity.STATE_BINARY0100_UPDATING_PRICES);
//        }
//        return super.getOppStateForLevelUpdate(o);
//    }
//
//    /**
//     * Reset the market internal state in the end of the day to be ready for the next
//     * day opening. Long term shiftings, updates flags etc.
//     */
//    @Override
//    public void resetInTheEndOfTheDay() {
//        super.resetInTheEndOfTheDay();
//        binaryMarket.resetInTheEndOfTheDay();
//    }
//}