//package il.co.etrader.service.level.items;
//
//import il.co.etrader.service.level.BigDecimalHelper;
//import il.co.etrader.service.level.LevelService;
//import il.co.etrader.service.level.OpportunitiesManager;
//import il.co.etrader.service.level.SenderThread;
//import il.co.etrader.service.level.options.AskBidOption;
//import il.co.etrader.service.level.options.HourClosingLevelOption;
//import il.co.etrader.service.level.options.InterestOption;
//import il.co.etrader.service.level.options.Option;
//import il.co.etrader.service.level.options.StrikeOption;
//import il.co.etrader.service.level.options.YearInterestOption;
//
//import java.math.BigDecimal;
//import java.math.RoundingMode;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.TimeZone;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.Opportunity;
//import com.anyoption.common.beans.ReutersQuotes;
//import com.reuters.rfa.common.Handle;
//
///**
// * Implements the ILS formula calculation.
// *
// * NOTE: Generally the XL description says that we shoulw use ILS2= on friday but as we don't
// * actually take anything from this overall RIC. It makes no difference if we subscribe to
// * ILS= all days or to ILS= Mon to Thu and ILS2= on Fri.
// *
// * NOTE: As we don't use actually ILS= we can not subscribe for it at all.
// *
// * @author Tony
// */
//public class ILSItem extends CurrencyItem {
//    private static final Logger log = Logger.getLogger(ILSItem.class);
//
//    private static final BigDecimal O25 = new BigDecimal("0.25");
//    private static final BigDecimal O3 = new BigDecimal("0.3");
//    private static final BigDecimal O4 = new BigDecimal("0.4");
//    private static final BigDecimal O45 = new BigDecimal("0.45");
//    private static final BigDecimal O6 = new BigDecimal("0.6");
//    private static final BigDecimal O75 = new BigDecimal("0.75");
//    private static final BigDecimal O2 = new BigDecimal("0.2");
//
//    private static final BigDecimal OOO13 = new BigDecimal("0.0013");
//    private static final BigDecimal OOO15 = new BigDecimal("0.0015");
//    private static final BigDecimal OOO16 = new BigDecimal("0.0016");
//    private static final BigDecimal OOO35 = new BigDecimal("0.0035");
//    private static final BigDecimal OOO4 = new BigDecimal("0.004");
//    private static final BigDecimal O99 = new BigDecimal("0.99");
//    private static final BigDecimal OOO1 = new BigDecimal("0.001");
//    private static final BigDecimal OOO3 = new BigDecimal("0.003");
//
//    private static final long STRIKE_STEP = 50;
//
//
//    protected ArrayList<InterestOption> banks;
//    protected YearInterestOption interestILS;
//    protected YearInterestOption interestUSD;
//    protected AskBidOption spot3Option;
//
//    private boolean nextMonth = false;
//    private BigDecimal expireIn;
//    private String year = "";
//    private int optionIgnoreAskBidSpread;
//
//    private long lastOptionUpdateReceiveTime;
//    private boolean valuesCheckDisable;
//
//    public ILSItem(String itemName, Handle handle) {
//        super(itemName, handle);
//
//        useMainRic = false;
//        banks = new ArrayList<InterestOption>();
//        banks.add((InterestOption) Option.getInstance("ILS=BLTA"));
//        banks.add((InterestOption) Option.getInstance("ILS=POTA"));
//        banks.add((InterestOption) Option.getInstance("ILS=IDBT"));
//        banks.add((InterestOption) Option.getInstance("ILS=MIZI"));
//        banks.add((InterestOption) Option.getInstance("ILS=FIBI"));
//        banks.add((InterestOption) Option.getInstance("ILS=UBII"));
//
//        hourClosingLevelOptionsTimeZone = "Israel";
//        closingLevelOptions = new ArrayList<Option>();
//        for (int i = 9; i <= 17; i++) {
//            closingLevelOptions.add(Option.getInstance(getHourlyOptionName(i)));
//        }
//
//        interestILS = (YearInterestOption) Option.getInstance("TELILSOND=");
//        interestUSD = (YearInterestOption) Option.getInstance("USD1MD="); // used to be USD1MFSR=
//
//        spot3Option = (AskBidOption) Option.getInstance("ILS=D4");
//
//        optionIgnoreAskBidSpread = LevelService.getInstance().getIlsAskBidIgnoreSpread();
//        valuesCheckDisable = false;
//    }
//
//    /**
//     * Notify the item about update for it received from Reuters.
//     *
//     * @param fields the fields and values received from reuters
//     * @param sender
//     * @param lifeId
//     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
//     */
//    @Override
//    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
////        try {
////            if (log.isInfoEnabled()) {
////                log.info("Unsubscribe from Reuters " + itemName);
////            }
////            LevelService.getInstance().unsubscribeRicFromReuters(getHandle());
////        } catch (Throwable t) {
////            log.error("Can't unsubscribe from " + itemName, t);
////        }
//        return false;
//    }
//
//    /**
//     * Notify the item that one of its options have received update.
//     *
//     * @param option the option that received update
//     * @param sender
//     * @param lifeId
//     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
//     */
//    @Override
//    public boolean optionUpdated(Option option, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
//        if (log.isDebugEnabled()) {
//            if (option instanceof StrikeOption) { // TISXXXXY.TA
//                StrikeOption so = (StrikeOption) option;
//                log.debug("TRADINGDATA_ILS= - " + so.getOptionName() + " TRDPRC_1: " + so.getLast() + " ASK: " + so.getAsk() + " BID: " + so.getBid());
//            }
//            if (option instanceof YearInterestOption) { // TELILSOND=, USD1MFSR=
//                YearInterestOption yio = (YearInterestOption) option;
//                log.debug("TRADINGDATA_ILS= - " + yio.getOptionName() + " BID: " + yio.getBid() + " BID1: " + yio.getBid1());
//            } else if (option instanceof InterestOption) { // USD1MD=, ILS1MD=
//                InterestOption io = (InterestOption) option;
//                log.debug("TRADINGDATA_ILS= - " + io.getOptionName() + " ASK: " + io.getAsk() + " BID: " + io.getBid());
//            } else if (option instanceof HourClosingLevelOption) {
//                HourClosingLevelOption ho = (HourClosingLevelOption) option;
//                log.debug("TRADINGDATA_ILS= - " + ho.getOptionName() + " BID: " + ho.getBid() + " VALUE_DT1: " + ho.getActiveDate());
//            } else if (option instanceof AskBidOption) { // ILS=D4
//                AskBidOption abo = (AskBidOption) option;
//                log.debug("TRADINGDATA_ILS= - " + abo.getOptionName() + " ASK: " + abo.getAsk() + " BID: " + abo.getBid());
//            }
//
//        }
//        lastReutersUpdateReceiveTime = System.currentTimeMillis();
//
//        boolean bankUpdate = false;
//        boolean haveBanksLevels = false;
//        double crrRealLevel = 0;
//        long strike = 0;
//        long strikeForName = 0;
//        if (bankUpdate = isBank(option)) {
//            if (haveBanksLevels = haveBanksLevels()) {
//                crrRealLevel = calcX();
//                strike = calcStrike(crrRealLevel);
//                strikeForName = strike / 10;
//            }
//        }
//
//        if (log.isDebugEnabled()) {
//            log.debug("bankUpdate: " + bankUpdate + " haveBankLevels: " + haveBanksLevels + " strike: " + strike);
//        }
//
//        if (haveBanksLevels) {
//            Calendar cal = Calendar.getInstance();
//            if (cal.get(Calendar.DAY_OF_WEEK) != Calendar.FRIDAY) { // Monday to Thursday we calc by the strike options
//                // only after a bank update an change in the options can happen
//
//                char call = getCallLetter(nextMonth);
//                char put = getPutLetter(nextMonth);
//                String o1c = "TIS00" + (strikeForName - STRIKE_STEP/10) + call + year + ".TA";
//                String o1p = "TIS00" + (strikeForName - STRIKE_STEP/10) + put + year + ".TA";
//                String o2c = "TIS00" + strikeForName + call + year + ".TA";
//                String o2p = "TIS00" + strikeForName + put + year + ".TA";
//                String o3c = "TIS00" + (strikeForName + STRIKE_STEP/10) + call + year + ".TA";
//                String o3p = "TIS00" + (strikeForName + STRIKE_STEP/10) + put + year + ".TA";
//
//                if (options.size() < 6) {
//                    resubscribe(new Option[] {
//                            Option.getInstance(o1c, true, strike - STRIKE_STEP),
//                            Option.getInstance(o1p, false, strike - STRIKE_STEP),
//                            Option.getInstance(o2c, true, strike),
//                            Option.getInstance(o2p, false, strike),
//                            Option.getInstance(o3c, true, strike + STRIKE_STEP),
//                            Option.getInstance(o3p, false, strike + STRIKE_STEP)});
//                } else {
//                    long oldStrike = options.get(2).getStrike();
//                    if (log.isTraceEnabled()) {
//                        log.trace("oldStrike: " + oldStrike + " strike: " + strike);
//                    }
//                    if (oldStrike < strike) {
//                        // the quote went up. we need to drop the strike - 50 (at least) and subscr for new strike + 50 (at least)
//                        long move = (strike - oldStrike) / STRIKE_STEP;
//                        if (log.isTraceEnabled()) {
//                            log.trace("move: " + move);
//                        }
//                        if (move == 1) {
//                            // we can reuse 4 options subscr
//                            resubscribe(new Option[] {
//                                    Option.getInstance(o3c, true, strike + STRIKE_STEP),
//                                    Option.getInstance(o3p, false, strike + STRIKE_STEP)});
//                        } else if (move == 2) {
//                            // we can reuse 2 options subscr
//                            resubscribe(new Option[] {
//                                    Option.getInstance(o2c, true, strike),
//                                    Option.getInstance(o2p, false, strike),
//                                    Option.getInstance(o3c, true, strike + STRIKE_STEP),
//                                    Option.getInstance(o3p, false, strike + STRIKE_STEP)});
//                        } else {
//                            // we can't reuse a thing
//                            resubscribe(new Option[] {
//                                    Option.getInstance(o1c, true, strike - STRIKE_STEP),
//                                    Option.getInstance(o1p, false, strike - STRIKE_STEP),
//                                    Option.getInstance(o2c, true, strike),
//                                    Option.getInstance(o2p, false, strike),
//                                    Option.getInstance(o3c, true, strike + STRIKE_STEP),
//                                    Option.getInstance(o3p, false, strike + STRIKE_STEP)});
//                        }
//                    } else if (options.get(2).getStrike() == strike) {
//                        // we can reuse all 6 options subscr
//                        if (log.isTraceEnabled()) {
//                            log.trace("No move");
//                        }
//                    } else {
//                        // the quote went up. we need to drop the strike + 50 (at least) and subscr for new strike - 50 (at least)
//                        double move = (oldStrike - strike) / STRIKE_STEP;
//                        if (log.isTraceEnabled()) {
//                            log.trace("move: " + move);
//                        }
//                        if (move == 1) {
//                            // we can reuse 4 options subscr
//                            resubscribe(new Option[] {
//                                    Option.getInstance(o1c, true, strike - STRIKE_STEP),
//                                    Option.getInstance(o1p, false, strike - STRIKE_STEP)});
//                        } else if (move == 2) {
//                            // we can reuse 2 options subscr
//                            resubscribe(new Option[] {
//                                    Option.getInstance(o1c, true, strike - STRIKE_STEP),
//                                    Option.getInstance(o1p, false, strike - STRIKE_STEP),
//                                    Option.getInstance(o2c, true, strike),
//                                    Option.getInstance(o2p, false, strike)});
//                        } else {
//                            // we can't reuse a thing
//                            resubscribe(new Option[] {
//                                    Option.getInstance(o1c, true, strike - STRIKE_STEP),
//                                    Option.getInstance(o1p, false, strike - STRIKE_STEP),
//                                    Option.getInstance(o2c, true, strike),
//                                    Option.getInstance(o2p, false, strike),
//                                    Option.getInstance(o3c, true, strike + STRIKE_STEP),
//                                    Option.getInstance(o3p, false, strike + STRIKE_STEP)});
//                        }
//                    }
//                }
//                if (crrRealLevel != lastRealLevel) {
//                    haveUpdate(sender, lifeId, false, 0, 0, true, crrRealLevel);
//                }
//            } else { // on Friday we calc the level from the banks levels
//                log.trace("Can calc Friday level");
//                setSnapshotReceived(true);
//                if (interestUSD1M.isSnapshotReceived() && interestCRR1M.isSnapshotReceived()) {
//                    setLongTermSnapshotReceived(true);
//                }
//                calcLevel(sender, lifeId, crrRealLevel, true);
//            }
//        }
//
//        if (option instanceof StrikeOption) {
//            lastOptionUpdateReceiveTime = System.currentTimeMillis();
//        }
//
//        if ((option instanceof StrikeOption ||
//                option.getOptionName().equals(spot3Option.getOptionName())) &&
//                canCalcLevel() &&
//                spot3Option.isSnapshotReceived()) { // if we have values for all options and we can continue with level calc
//            log.trace("Can calc level");
//            setSnapshotReceived(true);
//            if (interestUSD1M.isSnapshotReceived() && interestCRR1M.isSnapshotReceived()) {
//                setLongTermSnapshotReceived(true);
//            }
//            calcLevel(sender, lifeId, lastRealLevel, false);
//        } else {
//            log.trace("Don't calc level - strike option: " + (option instanceof StrikeOption) + " spot3Option snapshot: " + spot3Option.isSnapshotReceived());
//        }
//
//        if (option instanceof HourClosingLevelOption) {
//            log.info("Hour closing level update.");
//            HourClosingLevelOption ho = (HourClosingLevelOption) option;
//            if (isTodayTime(ho.getActiveDate() + " " + ho.getActiveTime(), hourClosingLevelOptionsTimeZone)) {
//            	Opportunity o = getOppInState(Opportunity.STATE_CLOSING);
//            	if (null != o) {
//            		o.setClosingLevelReceived(true);
//            	}
//                return true;
//            }
//        }
//        if (option.getOptionName().equals(interestUSD1M.getOptionName()) ||
//                option.getOptionName().equals(interestCRR1M.getOptionName())) {
//            if (isSnapshotReceived() && interestUSD1M.isSnapshotReceived() && interestCRR1M.isSnapshotReceived()) {
//                setLongTermSnapshotReceived(true);
//                sendUpdate(sender, lifeId, UPDATE_LONG_TERM_OPPS, true, false);
//            }
//        }
//        return false;
//    }
//
//    /**
//     * Check if all options have received snapshots and level can be calculated.
//     * Call this metod only when you have lock on the options.
//     *
//     * @return <code>true</code> if level can be claculated else <code>false</code>.
//     */
//    @Override
//    protected boolean canCalcLevel() {
//        if (!super.canCalcLevel()) {
//            return false;
//        }
//        if (!interestILS.isSnapshotReceived() || !interestUSD.isSnapshotReceived()) {
//            log.trace("interestILS snapshot: " + interestILS.isSnapshotReceived() + " interestUSD: " + interestUSD.isSnapshotReceived());
//            return false;
//        }
//        return true;
//    }
//
//    /**
//     * Compose hourly closing level option name by hour.
//     *
//     * @param hour the hour for which to compose option name
//     * @return The name of the hourly closing level option for the requested hour.
//     */
//    @Override
//    protected String getHourlyOptionName(int hour) {
//        DecimalFormat twoDigitNumberFormat = new DecimalFormat("#00");
//        return itemName.substring(0, 3) + twoDigitNumberFormat.format(hour) + "H=RR";
//    }
//
//    /**
//     * Tell the item to open (subscribe) all its options (when you want to start using it).
//     *
//     * @param itm the Item instance to receive the updates for the options.
//     */
//    @Override
//    public void subscribeOptions(Item itm) {
//        super.subscribeOptions(itm);
//
//        Option b = null;
//        for (Iterator<InterestOption> i = banks.iterator(); i.hasNext();) {
//            b = i.next();
//            if (null == b.getHandle()) {
//                optionSubscribtionManager.subscribeOption(itm, b);
//            }
//        }
//
//        if (null == interestILS.getHandle()) {
//            optionSubscribtionManager.subscribeOption(itm, interestILS);
//        }
//        if (null == interestUSD.getHandle()) {
//            optionSubscribtionManager.subscribeOption(itm, interestUSD);
//        }
//
//        optionSubscribtionManager.subscribeOption(itm, spot3Option);
//
//        nextMonth = OpportunitiesManager.getOpportunityOptionsChangeMonth(marketId);
//        expireIn = new BigDecimal(OpportunitiesManager.getOpportunityOptionsExpireIn(marketId) - 2);
//        year = (new SimpleDateFormat("yy")).format(new Date());
//        log.debug("ILS options - nextMonth: " + nextMonth + " expire in: " + expireIn + " year: " + year);
//    }
//
//    @Override
//    public boolean isReutersStateOK() {
//        if (!interestILS.isReutersStateOK() || !interestUSD.isReutersStateOK() || !spot3Option.isReutersStateOK()) {
//            return false;
//        }
//        for (Option o : banks) {
//            if (!o.isReutersStateOK()) {
//                return false;
//            }
//        }
//        return super.isReutersStateOK();
//    }
//
//    /**
//     * Ask this item to close (unsubscribe) all its options (when you want to close/unsubscribe it).
//     *
//     * @param itm the Item instance to be removed from the options.
//     */
//    @Override
//    public void unsubscribeOptions(Item itm) {
//        super.unsubscribeOptions(itm);
//
//        Option b = null;
//        for (Iterator<InterestOption> i = banks.iterator(); i.hasNext();) {
//            b = i.next();
//            optionSubscribtionManager.unsubscribeOption(itm, b);
//        }
//
//        optionSubscribtionManager.unsubscribeOption(itm, interestILS);
//        optionSubscribtionManager.unsubscribeOption(itm, interestUSD);
//        optionSubscribtionManager.unsubscribeOption(itm, spot3Option);
//    }
//
//    /**
//     * Check if this is one of the banks.
//     *
//     * @param option the option to check if it is one of the banks options
//     */
//    protected boolean isBank(Option option) {
//        Option b = null;
//        for (Iterator<InterestOption> i = banks.iterator(); i.hasNext();) {
//            b = i.next();
//            if (option == b) { // there should be only 1 instance of the option
//                return true;
//            }
//        }
//        return false;
//    }
//
//    /**
//     * Check if all banks have received snapshots.
//     * Call this metod when you have lock on banks.
//     *
//     * @return <code>true</code> if all banks have levels else <code>false</code>.
//     */
//    protected boolean haveBanksLevels() {
//        for (Iterator<InterestOption> i = banks.iterator(); i.hasNext();) {
//            if (!i.next().isSnapshotReceived()) {
//                return false;
//            }
//        }
//        return true;
//    }
//
//    /**
//     * @return the X value from the ILS formula. Get the avg value of ask + bid for each
//     *      bank. Remove the highest and lowest and make the avg of the left.
//     */
//    protected double calcX() {
//        BigDecimal min = new BigDecimal(Double.MAX_VALUE);
//        BigDecimal max = new BigDecimal(Double.MIN_VALUE);
//        BigDecimal sum = new BigDecimal(0);
//        Option bank = null;
//        BigDecimal avg = null;
//        BigDecimal a = null;
//        BigDecimal b = null;
//        for (Iterator<InterestOption> i = banks.iterator(); i.hasNext();) {
//            bank = i.next();
//            a = new BigDecimal(Double.toString(bank.getAsk()));
//            b = new BigDecimal(Double.toString(bank.getBid()));
//            avg = a.add(b).divide(BigDecimalHelper.TWO);
//            if (avg.compareTo(min) < 0) {
//                min = avg;
//            }
//            if (avg.compareTo(max) > 0) {
//                max = avg;
//            }
//            sum = sum.add(avg);
//        }
//        double x = sum.subtract(min).subtract(max).divide(new BigDecimal(banks.size() - 2), BigDecimalHelper.BIG_DECIMAL_CALC_SCALE, RoundingMode.HALF_UP).doubleValue();
//        if (log.isTraceEnabled()) {
//            log.trace("calcX - sum: " + sum + " max: " + max + " min: " + min + " banks.size(): " + banks.size() + " x: " + x);
//        }
//        return x;
//    }
//
//    /**
//     * NOTE: Because of the double math problems all calculations that matter will be done
//     * with long. So all is multiplied by 1000. This will give us the ready strike for the
//     * opportunities.
//     *
//     * @param crrRealLevel the current real level ( the X from the formula )
//     * @return the strike (field F22) from the ILS formula(3.9 for example is returned as 3900).
//     */
//    protected long calcStrike(double crrRealLevel) {
//        // NOTE 2008-02-19 - In the current version the calc is to rows up
//        // so e18 is e16 in the new XL
//        long e18 = Math.round(crrRealLevel * 1000);
//        long e19 = Math.round(((double) (e18 - 50)) / 100.0) * 100 + 50;
//        long e20 = Math.round(((double) e18) / 100.0) * 100;
//        long e21 = Math.abs(e18 - e19);
//        long e22 = Math.abs(e18 - e20);
//        long strike;
//        if (e21 < e22) {
//            strike = e19;
//        } else {
//            strike = e20;
//        }
//        if (log.isTraceEnabled()) {
//            log.trace("e18: " + e18 +
//                    " e19: " + e19 +
//                    " e20: " + e20 +
//                    " e21: " + e21 +
//                    " e22: " + e22 +
//                    " strike: " + strike);
//        }
//        return strike;
//    }
//
//    /**
//     * Calculate the level in to the <code>lastFormulaResult</code>.
//     * Call this metod only when you have lock on the options.
//     *
//     * @param sender
//     * @param lifeId
//     * @param realLevel the current real level (the X)
//     * @param friday if to use friday formula or mon-thu one
//     */
//    protected void calcLevel(SenderThread sender, long lifeId, double realLevel, boolean friday) {
//        BigDecimal d43 = BigDecimalHelper.ZERO;
//        if (null != spot3Option && null != spot3Option.getAsk()) {
//            d43 = new BigDecimal(Double.toString(spot3Option.getAsk()));
//        }
//        BigDecimal d44 = BigDecimalHelper.ZERO;
//        if (null != spot3Option && null != spot3Option.getBid()) {
//            d44 = new BigDecimal(Double.toString(spot3Option.getBid()));
//        }
//        BigDecimal spot2 = new BigDecimal(Double.toString(realLevel));
//        BigDecimal spot3 = d43.add(d44).divide(BigDecimalHelper.TWO);
//
//        int oppsUsed = 0;
//        BigDecimal fwd = BigDecimalHelper.ZERO;
//        if (System.currentTimeMillis() - lastOptionUpdateReceiveTime >= 300000 && log.isDebugEnabled()) {
//            if (log.isInfoEnabled()) {
//                log.info("No options updates for 5 min. Using friday formula.");
//            }
//        }
//        if (!friday && System.currentTimeMillis() - lastOptionUpdateReceiveTime < 300000) {
//            Option co = null;
//            Option po = null;
//            BigDecimal cov = null;
//            BigDecimal pov = null;
//            for (int i = 0; i < 3; i++) {
//                co = options.get(i * 2);
//                po = options.get(i * 2 + 1);
//                if (null == co || null == co.getLast() || co.getLast() == 0 ||
//                        null == co.getAsk() || co.getAsk() == 0 || null == co.getBid() ||
//                        co.getBid() == 0 || co.getAsk() - co.getBid() > optionIgnoreAskBidSpread ||
//                        null == po || null == po.getLast() || po.getLast() == 0 ||
//                        null == po.getAsk() || po.getAsk() == 0 || null == po.getBid() ||
//                        po.getBid() == 0 || po.getAsk() - po.getBid() > optionIgnoreAskBidSpread) {
//                    continue;
//                }
//                oppsUsed++;
//                BigDecimal coLast = new BigDecimal(Double.toString(co.getLast()));
//                BigDecimal coAsk = new BigDecimal(Double.toString(co.getAsk()));
//                BigDecimal coBid = new BigDecimal(Double.toString(co.getBid()));
//                cov = coLast.add(coAsk).add(coBid).divide(BigDecimalHelper.THREE, BigDecimalHelper.BIG_DECIMAL_CALC_SCALE, RoundingMode.HALF_UP);
//                BigDecimal poLast = new BigDecimal(Double.toString(po.getLast()));
//                BigDecimal poAsk = new BigDecimal(Double.toString(po.getAsk()));
//                BigDecimal poBid = new BigDecimal(Double.toString(po.getBid()));
//                pov = poLast.add(poAsk).add(poBid).divide(BigDecimalHelper.THREE, BigDecimalHelper.BIG_DECIMAL_CALC_SCALE, RoundingMode.HALF_UP);
//                BigDecimal strike = new BigDecimal(Double.toString(co.getStrike()));
//                BigDecimal bd = strike.divide(new BigDecimal("1000")).add(cov.subtract(pov).divide(new BigDecimal("10000")));
//                fwd = fwd.add(bd);
//            }
//        }
//        boolean c0 = spot3.abs().compareTo(spot2.multiply(O99)) <= 0;
//        if (c0 && hasOpenedNotExpiringOpportunities()) {
//            double z = spot2.doubleValue();
//            haveUpdate(sender, lifeId, true, z, z, true, realLevel);
//            if (disableOpportunities("|Spot3| <= 0.99*Spot2")) {
//                valuesCheckDisable = true;
//                String str =
//                        itemName + "|Spot3| <= 0.99 * Spot2;" +
//                        " Spot2 = " + spot2 +
//                        " Spot3 = " + spot3;
//                log.warn(str);
//                LevelService.getInstance().sendAlertEmail("Values check disable " + itemName, str);
//            }
//            return;
//        }
//        String sdo = shouldDisableOpportunities();
//        if (oppsUsed == 0) {
//            double z = spot3.multiply(O25).add(spot2.multiply(O75)).doubleValue();
//            haveUpdate(sender, lifeId, true, z, z, true, realLevel);
//
//            boolean c1 = d43.subtract(d44).abs().compareTo(spot2.multiply(getD4SpreadParameter())) > 0;
//            boolean c2 = spot3.subtract(spot2).abs().compareTo(spot2.multiply(getD4BanksParameter())) > 0;
//            if ((c1 || c2 || !sdo.equals("")) && hasOpenedNotExpiringOpportunities()) {
//                String desc = "";
//                desc += c1 ? "|D43 - D44| > " + getD4SpreadParameter() + " * Spot2; " : "";
//                desc += c2 ? "|Spot3 - Spot2| > " + getD4BanksParameter() + " * Spot2; " : "";
//                desc += !sdo.equals("") ? sdo + "; " : "";
//                if (disableOpportunities(desc)) {
//                    valuesCheckDisable = true;
//                    String str =
//                            itemName + " " + desc +
//                            (c1 ? " D43 = " + d43 + " D44 = " + d44 : "") +
//                            " Spot2 = " + spot2 +
//                            (c2 ? " Spot3 = " + spot3 : "");
//                    log.warn(str);
//                    LevelService.getInstance().sendAlertEmail("Values check disable " + itemName, str);
//                }
//            } else {
//                if (enableOpportunities("Values back to normal or no opps to put invest on", false)) {
//                    valuesCheckDisable = false;
//                    String str = itemName + " enabled opportunities";
//                    log.warn(str);
//                    LevelService.getInstance().sendAlertEmail("Values check enable " + itemName, str);
//                }
//            }
//            return;
//        }
//        fwd = fwd.divide(new BigDecimal(oppsUsed), BigDecimalHelper.BIG_DECIMAL_CALC_SCALE, RoundingMode.HALF_UP);
//        BigDecimal ei365 = expireIn.divide(new BigDecimal(36500), BigDecimalHelper.BIG_DECIMAL_CALC_SCALE, RoundingMode.HALF_UP); // expireIn / 365 * interest / 100 = expireIn / (365 * 100) * interest
//        BigDecimal ei360 = expireIn.divide(new BigDecimal(36000), BigDecimalHelper.BIG_DECIMAL_CALC_SCALE, RoundingMode.HALF_UP); // expireIn / 360 * interest / 100 = expireIn / (360 * 100) * interest
//        BigDecimal spot1 =
//                fwd
//                .divide(BigDecimalHelper.ONE.add(ei365.multiply(interestILS.getAvgBidBid1())), BigDecimalHelper.BIG_DECIMAL_CALC_SCALE, RoundingMode.HALF_UP)
//                .multiply(BigDecimalHelper.ONE.add(ei360.multiply(interestUSD.getAvgBidBid1())));
//        double crrLevel =
//                spot1.multiply(O2)
//                .add(spot2.multiply(O6))
//                .add(spot3.multiply(O2))
//                .doubleValue();
//        haveUpdate(sender, lifeId, true, crrLevel, crrLevel, true, realLevel);
//
//        boolean c1 = spot2.subtract(spot1).abs().compareTo(spot2.multiply(getBanksBursaParameter())) > 0;
//        boolean c2 = spot3.subtract(spot2).abs().compareTo(spot2.multiply(getD4BanksParameter())) > 0;
//        boolean c3 = d43.subtract(d44).abs().compareTo(spot2.multiply(getD4SpreadParameter())) > 0;
//        if ((c1 || c2 || c3 || !sdo.equals("")) && hasOpenedNotExpiringOpportunities()) {
//            String desc = "";
//            desc += c1 ? "|Spot2 - Spot1| > " + getBanksBursaParameter() + " * Spot2; " : "";
//            desc += c2 ? "|Spot3 - Spot2| > " + getD4BanksParameter() + " * Spot2; " : "";
//            desc += c3 ? "|D43 - D44| > " + getD4SpreadParameter() + " * Spot2; " : "";
//            desc += !sdo.equals("") ? sdo + "; " : "";
//            if (disableOpportunities(desc)) {
//                valuesCheckDisable = true;
//                String str =
//                        itemName + " " + desc +
//                        " Spot1 = " + spot1 +
//                        " Spot2 = " + spot2 +
//                        " Spot3 = " + spot3 +
//                        " D43 = " + d43 +
//                        " D44 = " + d44;
//                log.warn(str);
//                LevelService.getInstance().sendAlertEmail("Values check disable " + itemName, str);
//            }
//        } else {
//            if (enableOpportunities("Values back to normal or no opps to put invest on", false)) {
//                valuesCheckDisable = false;
//                String str = itemName + " enabled opportunities";
//                log.warn(str);
//                LevelService.getInstance().sendAlertEmail("Values check enable " + itemName, str);
//            }
//        }
//    }
//
//    /**
//     * Calculate the end of hour closing level.
//     *
//     * @param o the option with the closing level for this hour
//     * @return The closing level for this hour.
//     */
//    @Override
//    protected double calcEndOfHourClosingLevel(Option o) {
//        return o.getBid();
//    }
//
//    /**
//     * @return <code>ture</code> if the opened opportunities of this item
//     *      should be enabled else <code>false</code>.
//     */
//    @Override
//    public boolean shouldEnableOpportunities() {
//        if (!valuesCheckDisable && super.shouldEnableOpportunities()) {
//            log.warn("Updates resumed and values ok for market " + itemName);
//            return true;
//        }
//        return false;
//    }
//
//    /**
//     * Check if the closing level for specified opportunity has been received.
//     *
//     * @param w the opportunity to check for if the closing level is ready
//     * @return <code>true</code> if the closing level is ready to be taken else <code>false</code>
//     */
//    @Override
//    public boolean isClosingLevelReceived(Opportunity w) {
//        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(hourClosingLevelOptionsTimeZone));
//        int hour = cal.get(Calendar.HOUR_OF_DAY);
//        String oName = getHourlyOptionName(hour);
//        Option o = getOption(oName);
//        if (null != o) {
//            if (log.isTraceEnabled()) {
//                log.trace("option found active date is " + o.getActiveDate());
//            }
//            return isTodayTime(o.getActiveDate() + " " + o.getActiveTime(), hourClosingLevelOptionsTimeZone);
//        } else {
//            log.warn("Can't find hour closing option " + oName);
//            return false;
//        }
//    }
//
//    /**
//     * Get the closing level formula for the specified opportunity.
//     *
//     * @param w the opportunity for which to get closing level formula
//     * @return The formula and the formula patamters with there values as one string.
//     */
//    @Override
//	public String getClosingLevelTxt(Opportunity o) {
//		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(hourClosingLevelOptionsTimeZone));
//		int hour = cal.get(Calendar.HOUR_OF_DAY);
//        Option opt = getOption(getHourlyOptionName(hour));
//        if (o.isUseManualClosingLevel()) {
//            return "manualClosingLevel:" + o.getManualClosingLevel() + ";closingLevel:" + getClosingLevel(o);
//        }
//		return "formula:round(bid);bid:" + opt.getBid() + ";closeLevelBeforeRound:" + calcEndOfHourClosingLevel(opt) + ";closeLevel:" + getClosingLevel(o) ;
//	}
//
//    /**
//     * Reset the market internal state in the end of the day to be ready for the next
//     * day opening. Long term shiftings, updates flags etc.
//     */
//    @Override
//    public void resetInTheEndOfTheDay() {
//        super.resetInTheEndOfTheDay();
//
//        lastOptionUpdateReceiveTime = 0;
//        valuesCheckDisable = false;
//    }
//
//
//    protected BigDecimal getD4BanksParameter() {
//    	return LevelService.getD4BanksParameter();
//    }
//
//    protected BigDecimal getBanksBursaParameter() {
//    	return LevelService.getBanksBursaParameter();
//    }
//
//    protected BigDecimal getD4SpreadParameter() {
//    	return LevelService.getD4SpreadParameter();
//    }
//
//    @Override
//    public void setLastUpdateQuote(Opportunity o, Option opt) {
//    	o.setLastUpdateQuote(new ReutersQuotes(new Date(), null, null, new BigDecimal(opt.getBid())));
//    }
//}