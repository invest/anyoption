package il.co.etrader.service.level.items;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.options.InterestOption;
import il.co.etrader.service.level.options.Option;

/**
 * Handle levels calculations for Coca Cola (KO), General Electric (GE), Citi Group (C),
 * Apple (AAPL.OQ), Microsoft (MSFT.OQ), eBay (EBAY.OQ), Google (GOOG.OQ), Vodafone (VOD.L),
 * HSBC (HSBA.L), British Airways (BAY.L), JPM, maxico, turkish, spain.
 *
 * @author Tony
 */
public class NewStockItem extends StockItem {
    private static final Logger log = Logger.getLogger(NewStockItem.class);

    public NewStockItem(String itemName, Handle handle) {
        super(itemName, handle);

        if (itemName.endsWith(".L")) {
            interest = (InterestOption) Option.getInstance("GBP1MD=");
        } else if (itemName.endsWith(".MC")
        			|| itemName.endsWith(".DE")
        			|| itemName.endsWith(".MI")
        			|| itemName.endsWith(".PA")
        			|| itemName.endsWith(".AS")) {
        	interest = (InterestOption) Option.getInstance("EUR1MD=");
    	} else if (itemName.endsWith(".MX")) {
        	interest = (InterestOption) Option.getInstance("MXN1M=");
    	} else if (itemName.endsWith(".IS")) {
    		interest = (InterestOption) Option.getInstance("TRY1MD=");
    	} else if (itemName.endsWith(".MM")) {
            interest = (InterestOption) Option.getInstance("RUB1MD=");
        } else if (itemName.endsWith(".AX")) {
            interest = (InterestOption) Option.getInstance("AUD1MD=");
        } else if (itemName.endsWith(".T")) {
            interest = (InterestOption) Option.getInstance("JPY1MD=");
        } else if (itemName.equals("TAMO.NS")) {
            interest = (InterestOption) Option.getInstance("INR1MD=");
        } else if (itemName.endsWith(".SS")) {
            interest = (InterestOption) Option.getInstance("CNY1MD=");
        } else if (itemName.endsWith(".KS")) {
        	interest = (InterestOption) Option.getInstance("KRW1MID=KR"); // KRW1M=
        } else {
            interest = (InterestOption) Option.getInstance("USD1MD=");
        }

        //turkish markets have diff timefield
        if (itemName.endsWith(".IS")) {
            timeField = "VALUE_TS1";
        } else if (itemName.endsWith(".DE")) {
        	timeField = "EXCHTIM";
        } else if (itemName.endsWith(".MM")) {
        	timeField = "TRDTIM_1";
        } else {
            timeField = "SALTIM";
        }

//        if (itemName.endsWith(".DE")) {
//        	timezone = "Europe/Berlin";
//        }

        if (itemName.equals("TKC") || itemName.equals("BBVA.MC") || itemName.equals("ITX.MC") || itemName.endsWith(".MX") ||
                itemName.equals("VOWG.DE")) {
            dontCloseAfterXSecNoUpdate = 120;
        }
    }

    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
    @Override
	public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        return processStockUpdate(fields, sender, lifeId, false, opportunitiesTemp);
    }

    /**
     * Check if the closing level for specified opportunity has been received.
     *
     * @param o the opportunity to check for if the closing level is ready
     * @return <code>true</code> if the closing level is ready to be taken else <code>false</code>
     */
    @Override
	public boolean isClosingLevelReceived(Opportunity o) {
//        return isSnapshotReceived(o);
        return o.isClosingLevelReceived();
    }

    /**
     * Get the closing level for the specified opportunity.
     *
     * @param w the opportunity for which to get closing level
     * @return The level as received from Reuters. No formula and exposure control calculations.
     */
    @Override
	public double getClosingLevel(Opportunity o) {
        long decimal = o.getMarket().getDecimalPoint() - o.getMarket().getDecimalPointSubtractDigits();
/*        if (!(itemName.endsWith(".IS") || itemName.equals("EONGn.DE") || itemName.equals("TEF.MC") || itemName.equals("BBVA.MC") ||
        		itemName.equals("SAN.MC") || itemName.equals("IBE.MC") || itemName.equals("GASI.MI") || itemName.equals("FIA.MI") ||
        		itemName.equals("ENI.MI") || itemName.equals("STD"))) {
            decimal -= 1;
        } */
        if (o.isUseManualClosingLevel()) {
            return roundDouble(o.getManualClosingLevel(), decimal);
        }
        double cl = roundDouble(o.getNextClosingLevel(), decimal);
        if (log.isTraceEnabled()) {
            log.trace("TRADINGDATA_" + itemName +
                    " expiration - opportunityId: " + o.getId() +
                    " expiration level: " + cl);
        }
        return cl;
    }

    protected long getLongTermLevelDivideDays() {
        if (itemName.endsWith(".L") || itemName.endsWith(".MC") || itemName.endsWith(".IS") || itemName.endsWith(".MI") || itemName.endsWith(".AX")) { // VOD.L, HSBA.L, BAY.L, europe
            return 365;
        } else { // KO, GE, C, AAPL.OQ, MSFT.OQ, EBAY.OQ, GOOG.OQ, .MX, JPM, .DE
            return 360;
        }
    }

    @Override
	public String getClosrLevelTxtDay() {
		return "formula:round(TRDPRC_1);TRDPRC_1:" + lastTrdPrc.doubleValue() + ";closeLevelBeforeRound:" + lastTrdPrc.doubleValue();
	}

    @Override
    public boolean isShouldBeClosed(long oppId) {
        if (itemName.endsWith(".IS")) {
            return true;
        }
        Opportunity o = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            if (o.getId() == oppId) {
                if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
                    return System.currentTimeMillis() - lastReutersUpdateReceiveTime <= dontCloseAfterXSecNoUpdate * 1000;
                }
                break;
            }
        }
        return true;
    }
}