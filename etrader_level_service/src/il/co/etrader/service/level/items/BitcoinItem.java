package il.co.etrader.service.level.items;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ReutersQuotes;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.options.BitcoinOption;
import il.co.etrader.service.level.options.Option;

public class BitcoinItem extends Item {
    private static final Logger log = Logger.getLogger(BitcoinItem.class);
    
    private BitcoinOption option;
    protected Double lastASK;
    protected Double lastBID;
    protected Double lastTrdPrc;

    protected BitcoinItem(String itemName, Handle handle) {
        super(itemName, handle);
        
        useMainRic = false;
        option = (BitcoinOption) Option.getInstance(itemName);
    }

    /**
     * Tell the item to open (subscribe) all its options (when you want to start using it).
     *
     * @param itm the Item instance to receive the updates for the options.
     */
    @Override
    public void subscribeOptions(Item itm) {
        super.subscribeOptions(itm);
        optionSubscribtionManager.subscribeOption(itm, option);
    }

    /**
     * Clear the options list (unsibscribe each option it remove).
     * Make sure you have lock over the options when you call this metod.
     *
     * @param itm the Item instance to be removed from the options.
     */
    @Override
    protected void clearOptions(Item itm) {
        super.clearOptions(itm);
        optionSubscribtionManager.unsubscribeOption(itm, option);
    }
    
    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
    @Override
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunities) {
        return false;
    }

    /**
     * Notify the item that one of its options have received update.
     *
     * @param option the option that received update
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    @Override
    public boolean optionUpdated(Option option, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunities) {
        // validate the update (make sure we got at least 1 interesting field and we didn't get 0s)
        if ((null == option.getAsk() && null == option.getBid() && null == option.getLast() && null == option.getActiveTime()) ||
                (null != option.getLast() && option.getLast() == 0) ||
                (null != option.getAsk() && option.getAsk() == 0) ||
                (null != option.getBid() && option.getBid() == 0)) {
            return false;
        }

        lastReutersUpdateReceiveTime = System.currentTimeMillis();
        if (null != option.getAsk()) {
            lastASK = option.getAsk();
        }
        if (null != option.getBid()) {
            lastBID = option.getBid();
        }
        if (null != option.getLast()) {
            lastTrdPrc = option.getLast();
        }

        boolean gotClosingLevel = false;
        if (null != lastTrdPrc && null != lastASK && null != lastBID) {
            setSnapshotReceived(true);

            double crrRealLevel = 0;
            String closeLevelTxtHour = null;
            if (lastTrdPrc < lastBID) {
                crrRealLevel = lastBID * 0.7 + lastASK * 0.2 + lastTrdPrc * 0.1;
                closeLevelTxtHour = "formula:(BID * 0.7 + ASK * 0.2 + LAST * 0.1);BID:" + lastBID + ";ASK:" + lastASK + ";LAST:" + lastTrdPrc + ";closeLevelBeforeRound:" + crrRealLevel;
            } else if (lastTrdPrc > lastASK) {
                crrRealLevel = lastBID * 0.2 + lastASK * 0.7 + lastTrdPrc * 0.1;
                closeLevelTxtHour = "formula:(BID * 0.2 + ASK * 0.7 + LAST * 0.1);BID:" + lastBID + ";ASK:" + lastASK + ";LAST:" + lastTrdPrc + ";closeLevelBeforeRound:" + crrRealLevel;
            } else {
                crrRealLevel = lastBID * 0.25 + lastASK * 0.25 + lastTrdPrc * 0.5;
                closeLevelTxtHour = "formula:(BID * 0.25 + ASK * 0.25 + LAST * 0.5);BID:" + lastBID + ";ASK:" + lastASK + ";LAST:" + lastTrdPrc + ";closeLevelBeforeRound:" + crrRealLevel;
            }

            Date updateTime = new Date();
            ReutersQuotes rq = new ReutersQuotes(updateTime, new BigDecimal(lastTrdPrc.toString()), new BigDecimal(lastASK.toString()), new BigDecimal(lastBID.toString()));
            gotClosingLevel = updateOpportunitiesClosingLevel(updateTime, crrRealLevel, crrRealLevel, closeLevelTxtHour, closeLevelTxtHour, rq, rq, null);

            double crrFormulaResult = formulaRandomChange(crrRealLevel);
            haveUpdate(sender, lifeId, true, crrRealLevel, crrFormulaResult, true, crrRealLevel);
        }
        return gotClosingLevel;
    }

    /**
     * @param o the opportunity for which to calc the level
     * @return The long term formula result for this item.
     */
    @Override
    protected double longTermFormula(Opportunity o) {
        return 0;
    }

    /**
     * Check if the closing level for specified opportunity has been received.
     *
     * @param o the opportunity to check for if the closing level is ready
     * @return <code>true</code> if the closing level is ready to be taken else <code>false</code>
     */
    @Override
    public boolean isClosingLevelReceived(Opportunity o) {
        return o.isClosingLevelReceived();
    }

    /**
     * Get the closing level for the specified opportunity.
     *
     * @param o the opportunity for which to get closing level
     * @return The level as received from Reuters. No formula and exposure control calculations.
     */
    @Override
    public double getClosingLevel(Opportunity o) {
        long decimal = o.getMarket().getDecimalPoint() - o.getMarket().getDecimalPointSubtractDigits();
        if (o.isUseManualClosingLevel()) {
            return roundDouble(o.getManualClosingLevel(), decimal);
        }
        double cl = roundDouble(o.getNextClosingLevel(), decimal);
        log.info("TRADINGDATA_" + itemName + " expiration - opportunityId: " + o.getId() + " expiration level: " + cl);
        // Do not write before and after quotes for currency
        o.setBeforeLastUpdateQuote(null);
        o.setFirstUpdateQuote(null);
        return cl;
    }

    /**
     * check if should close opp from this item
     * if we got updates in last X sec
     * @param l
     *
     * @return true if should close, false if not.
     */
    @Override
    public boolean isShouldBeClosed(long l) {
        return true;
    }
}