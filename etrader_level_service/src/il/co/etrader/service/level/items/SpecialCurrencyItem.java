package il.co.etrader.service.level.items;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.SenderThread;

/**
 * Implement the formula for currency items Usd-Cad and USD-CHF(except the ILS).
 * this is the formula:
 * 5 last Bid + Ask:
 * Average of each (ASK +BID).
 * The newest (bid +ask) - take it in to calculation 2 times
 * Remove the highest and lowest values
 * Now calculate Average of the 4 remain values
 *
 * @author Eyal
 */
public class SpecialCurrencyItem extends CurrencyItem {
	private static final Logger log = Logger.getLogger(SpecialCurrencyItem.class);

	public SpecialCurrencyItem(String itemName, Handle handle) {
		super(itemName, handle);
	}

    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
	@Override
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        Double bid = (Double) fields.get("BID");
        Double ask = (Double) fields.get("ASK");
        if (null != bid && bid > 0 && null != ask && ask > 0) {
            lastReutersUpdateReceiveTime = System.currentTimeMillis();

            BigDecimal bda = new BigDecimal(Double.toString(ask));
            BigDecimal bdb = new BigDecimal(Double.toString(bid));
            double crrRealLevel = bda.add(bdb).divide(new BigDecimal(2)).doubleValue();

            lastFiveQuotes.add(crrRealLevel);
            lastFiveQuotes.add(crrRealLevel);
            if (lastFiveQuotes.size() > 5) {
                lastFiveQuotes.remove(0);
            }
            double min = Double.MAX_VALUE;
            double max = Double.MIN_VALUE;
            double sum = 0;
            double crr;
            for (int i = 0; i < lastFiveQuotes.size(); i++) {
                crr = lastFiveQuotes.get(i);
                if (crr < min) {
                    min = crr;
                }
                if (crr > max) {
                    max = crr;
                }
                sum += crr;
            }
            setSnapshotReceived(true);
            if (interestUSD1M.isSnapshotReceived() && interestCRR1M.isSnapshotReceived()) {
                setLongTermSnapshotReceived(true);
            }

            double avg = (sum - (lastFiveQuotes.size() > 4 ? min : 0) - (lastFiveQuotes.size() > 5 ? max : 0)) / (lastFiveQuotes.size() == 6 || lastFiveQuotes.size() == 5 ? 4 : lastFiveQuotes.size());
            double crrLevel = formulaRandomChange(avg);
            haveUpdate(sender, lifeId, true, avg, crrLevel, true, crrRealLevel);
            //delete 1 instance of the last update
            lastFiveQuotes.remove(lastFiveQuotes.size() - 1);
        } else {
            if (log.isTraceEnabled()) {
                log.trace("No value");
            }
        }
        return false;
    }
}