package il.co.etrader.service.level.items;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ReutersQuotes;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.BigDecimalHelper;
import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.options.Option;

/**
 * Handle Gold, Copper and Silver logic.
 *
 * @author Tony
 */
public class CommoditiesItem extends Item {
    private static final Logger log = Logger.getLogger(CommoditiesItem.class);

    protected BigDecimal lastTrdprc;
    protected BigDecimal lastASK;
    protected BigDecimal lastBID;
    protected String lastSALTIM;

    public CommoditiesItem(String itemName, Handle handle) {
        super(itemName, handle);

        useMainRic = true;
        shouldBeClosedByMonitoring = true;
    }

    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
    @Override
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        boolean gotClosingLevel = false;
        try {
            Double bid = (Double) fields.get("BID");
            Double ask = (Double) fields.get("ASK");
            Double trdprc1 = (Double) fields.get("TRDPRC_1");
            String salTim = (String) fields.get("SALTIM");

            if ((null == trdprc1 && null == ask && null == bid) ||
                    (null != trdprc1 && trdprc1 == 0) ||
                    (null != ask && ask == 0) ||
                    (null != bid && bid == 0)) {
                return gotClosingLevel;
            }
            lastReutersUpdateReceiveTime = System.currentTimeMillis();

            if (null != ask) {
                lastASK = new BigDecimal(ask.toString());
            }
            if (null != bid) {
                lastBID = new BigDecimal(bid.toString());
            }

            if (null != trdprc1 && trdprc1 > 0) {
            	lastTrdprc = new BigDecimal(trdprc1.toString());
            }

            if (null != salTim) {
                lastSALTIM = salTim;
            }

            double crrRealLevel = lastASK.add(lastBID).add(lastTrdprc).divide(BigDecimalHelper.THREE, BigDecimalHelper.BIG_DECIMAL_CALC_SCALE, RoundingMode.HALF_UP).doubleValue();

            Date updateTime = null;
            if (null != lastSALTIM) {
                try {
                    updateTime = getUpdateTime(lastSALTIM, "GMT");
                } catch (Throwable t) {
                    log.error("Can't process SALTIM.", t);
                    updateTime = new Date(System.currentTimeMillis());
                }
            } else {
                log.error("Update without SALTIM!!!");
                updateTime = new Date(System.currentTimeMillis());
            }

            String closingLevelTxt = "formula:round((ask+bid+trdprc_1)/3);ask:" + lastASK + ";bid:" + lastBID + "trdprc_1:" + lastTrdprc + ";closeLevelBeforeRound:" + crrRealLevel ;
            ReutersQuotes currentQuote = new ReutersQuotes(updateTime, lastTrdprc, lastASK, lastBID);
            gotClosingLevel = updateOpportunitiesClosingLevel(updateTime, crrRealLevel, crrRealLevel, closingLevelTxt, closingLevelTxt, currentQuote, currentQuote, opportunitiesTemp);

            double crrLevel = formulaRandomChange(crrRealLevel);
            haveUpdate(sender, lifeId, true, crrRealLevel, crrLevel, true, crrRealLevel);

            setSnapshotReceived(true);
            setLongTermSnapshotReceived(true);

        } catch (Throwable t) {
            log.error("Failed to process option update.", t);
        }
        return gotClosingLevel;
    }

    /**
     * Notify the item that one of its options have received update.
     *
     * @param option the option that received update
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    @Override
    public boolean optionUpdated(Option option, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        return false;
    }

    /**
     * @param o the opportunity for which to calc the level
     * @return The long term formula result for this item.
     */
    @Override
    protected double longTermFormula(Opportunity o) {
        return 0;
    }

    /**
     * Check if the closing level for specified opportunity has been received.
     *
     * @param o the opportunity to check for if the closing level is ready
     * @return <code>true</code> if the closing level is ready to be taken else <code>false</code>
     */
    @Override
    public boolean isClosingLevelReceived(Opportunity o) {
        return o.isClosingLevelReceived();
    }

    /**
     * Get the closing level for the specified opportunity.
     *
     * @param o the opportunity for which to get closing level
     * @return The level as received from Reuters. No formula and exposure control calculations.
     */
    @Override
    public double getClosingLevel(Opportunity o) {
        long decimal = o.getMarket().getDecimalPoint() - o.getMarket().getDecimalPointSubtractDigits();
        if (o.isUseManualClosingLevel()) {
            return roundDouble(o.getManualClosingLevel(), decimal);
        }
        double cl = roundDouble(o.getNextClosingLevel(), decimal);
        if (log.isInfoEnabled()) {
            log.info("TRADINGDATA_" + itemName + " expiration - opportunityId: " + o.getId() + " expiration level: " + cl);
        }
        return cl;
    }

    @Override
    public boolean isShouldBeClosed(long oppId) {
        Opportunity o = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            if (o.getId() == oppId) {
                if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
                    return System.currentTimeMillis() - lastReutersUpdateReceiveTime <= dontCloseAfterXSecNoUpdate * 1000;
                }
                break;
            }
        }
        return true;
    }

    /**
     * Reset the market internal state in the end of the day to be ready for the next
     * day opening. Long term shiftings, updates flags etc.
     */
    @Override
    public void resetInTheEndOfTheDay() {
        super.resetInTheEndOfTheDay();
        lastTrdprc = null;
        lastASK = null;
        lastBID = null;
        lastSALTIM = null;
    }
}