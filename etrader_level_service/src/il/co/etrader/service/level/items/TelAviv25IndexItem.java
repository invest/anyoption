package il.co.etrader.service.level.items;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ReutersQuotes;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.BigDecimalHelper;
import il.co.etrader.service.level.LevelService;
import il.co.etrader.service.level.OpportunitiesManager;
import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.options.InterestOption;
import il.co.etrader.service.level.options.Option;
import il.co.etrader.service.level.options.StrikeOption;
import il.co.etrader.service.level.options.TelAviv25Option;

public class TelAviv25IndexItem extends NadlanIndexItem {
    private static final Logger log = Logger.getLogger(TelAviv25IndexItem.class);

    private boolean nextMonth = false;
    private int optionIgnoreAskBidSpread;
    protected TelAviv25Option telAviv25;

    public TelAviv25IndexItem(String itemName, Handle handle) {
        super(itemName, handle);
        optionIgnoreAskBidSpread = LevelService.getInstance().getTa25AskBidIgnoreSpread();
        dontCloseAfterXSecNoUpdate = 120;
        useMainRic = false;
    }

    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        boolean gotClosingLevel = false;
        Double crrTrdPrc1 = (Double) fields.get("TRDPRC_1");
        String trdTim = (String) fields.get("SALTIM");//fields.get("TRDTIM_1");

        if (null != trdTim) {
            lastTrdTim = trdTim;
        }
        if (null != crrTrdPrc1 && crrTrdPrc1 > 0) {
            lastReutersUpdateReceiveTime = System.currentTimeMillis();

            if (log.isTraceEnabled()) {
                log.trace("TRADINGDATA_.TA25 - .TA25 TRDPRC_1: " + crrTrdPrc1);
            }
            lastTrdPrc = new BigDecimal(crrTrdPrc1.toString());

            Date updateTime = null;
            if (null != lastTrdTim) {
                try {
                    updateTime = getUpdateTime(lastTrdTim, "GMT");
                } catch (Throwable t) {
                    log.error("Can't process TRDTIM_1.", t);
                    updateTime = new Date(System.currentTimeMillis());
                }
            } else {
                log.error("TRDPRC_1 update without TRDTIM_1!!!");
                updateTime = new Date(System.currentTimeMillis());
            }

            String closingLevelTxt = "formula:TRDPRC_1;TRDPRC_1:" + crrTrdPrc1 + ";closeLevelBeforeRound:" + crrTrdPrc1;
            gotClosingLevel = updateOpportunitiesClosingLevel(updateTime, crrTrdPrc1, crrTrdPrc1, closingLevelTxt, closingLevelTxt, new ReutersQuotes(updateTime, crrTrdPrc1), null, opportunitiesTemp);

            long y = Math.round(crrTrdPrc1 / 10) * 10; // round to be multiple of 10

            // TODO: Tony: handle the option name creation when the level goes under 1000
            char call = getCallLetter(nextMonth);
            char put = getPutLetter(nextMonth);
            //SimpleDateFormat sdf = new SimpleDateFormat("yy");
            //String year = sdf.format(new Date());
            String year = "16";
            String yM10CName = "TLVJ" + getStrike(y - 10) + call + year + ".TA";
            String yM10PName = "TLVJ" + getStrike(y - 10) + put + year + ".TA";
            String yCName = "TLVJ" + getStrike(y) + call + year + ".TA";
            String yPName = "TLVJ" + getStrike(y) + put + year + ".TA";
            String yP10CName = "TLVJ" + getStrike(y + 10) + call + year + ".TA";
            String yP10PName = "TLVJ" + getStrike(y + 10) + put + year + ".TA";
            if (log.isTraceEnabled()) {
                log.trace("Y = " + y + " " + yM10CName + " " + yM10PName + " " + yCName + " " + yPName + " " + yP10CName + " " + yP10PName);
            }

            if (options.size() < 6) { // first .TA25 update
                resubscribe(new Option[] {
                        Option.getInstance(yM10CName, true, y - 10),
                        Option.getInstance(yM10PName, false, y - 10),
                        Option.getInstance(yCName, true, y),
                        Option.getInstance(yPName, false, y),
                        Option.getInstance(yP10CName, true, y + 10),
                        Option.getInstance(yP10PName, false, y + 10)});
            } else { // see what we can reuse from the currently subscribed options
                // the 6 options should go in package so if the strike of the 3rd and 4th is the same
                // as y we can reuse them
                double oldy = options.get(2).getStrike();
                if (log.isTraceEnabled()) {
                    log.trace("old Y = " + oldy);
                }
                if (oldy < y) {
                    // the quote went up. we need to drop the y - 10 (at least) and subscr for new y + 10 (at least)
                    double move = (y - oldy) / 10;
                    if (move == 1) {
                        // we can reuse 4 options subscr
                        resubscribe(new Option[] {
                                Option.getInstance(yP10CName, true, y + 10),
                                Option.getInstance(yP10PName, false, y + 10)});
                    } else if (move == 2) {
                        // we can reuse 2 options subscr
                        resubscribe(new Option[] {
                                Option.getInstance(yCName, true, y),
                                Option.getInstance(yPName, false, y),
                                Option.getInstance(yP10CName, true, y + 10),
                                Option.getInstance(yP10PName, false, y + 10)});
                    } else {
                        // we can't reuse a thing
                        resubscribe(new Option[] {
                                Option.getInstance(yM10CName, true, y - 10),
                                Option.getInstance(yM10PName, false, y - 10),
                                Option.getInstance(yCName, true, y),
                                Option.getInstance(yPName, false, y),
                                Option.getInstance(yP10CName, true, y + 10),
                                Option.getInstance(yP10PName, false, y + 10)});
                    }
                } else if (options.get(2).getStrike() == y) {
                    // we can reuse all 6 options subscr
                } else {
                    // the quote went down. we need to drop the y + 10 (at least) and subscr for new y - 10 (at least)
                    double move = (oldy - y) / 10;
                    if (move == 1) {
                        // we can reuse 4 options subscr
                        resubscribe(new Option[] {
                                Option.getInstance(yM10CName, true, y - 10),
                                Option.getInstance(yM10PName, false, y - 10)});
                    } else if (move == 2) {
                        // we can reuse 2 options subscr
                        resubscribe(new Option[] {
                                Option.getInstance(yM10CName, true, y - 10),
                                Option.getInstance(yM10PName, false, y - 10),
                                Option.getInstance(yCName, true, y),
                                Option.getInstance(yPName, false, y)});
                    } else {
                        // we can't reuse a thing
                        resubscribe(new Option[] {
                                Option.getInstance(yM10CName, true, y - 10),
                                Option.getInstance(yM10PName, false, y - 10),
                                Option.getInstance(yCName, true, y),
                                Option.getInstance(yPName, false, y),
                                Option.getInstance(yP10CName, true, y + 10),
                                Option.getInstance(yP10PName, false, y + 10)});
                    }
                }
            }
/*
            if (hasOpenedOpportunities() && !hasOpenedHourly()) {
                if (log.isTraceEnabled()) {
                    log.trace(itemName + " - add random to real level");
                }
                crrTrdPrc1 = formulaRandomChange(crrTrdPrc1, -0.0001, 0.0001);
            }
*/
            // send the real level
            haveUpdate(sender, lifeId, false, 0, 0, true, crrTrdPrc1);
        }
        if (null != fields.get("HST_CLOSE") && null != fields.get("HSTCLSDATE")) {
            lastHstClose = (Double) fields.get("HST_CLOSE");
            lastHstCloseDate = (String) fields.get("HSTCLSDATE");
            log.debug("checking is today for " + itemName + " lastHstcloseDate " + lastHstCloseDate);
            if (isToday(lastHstCloseDate, "GMT")) {
                log.debug("got true its close for today");
                Opportunity o = getOppInState(Opportunity.STATE_CLOSING);
            	if (null != o) {
            		o.setClosingLevelReceived(true);
            	}
                return true;
            }
            log.debug("got false its close for today");
        }
        return gotClosingLevel;
    }

    /**
     * Prepare the 5 chars for the option name that represents the strike.
     * If needed pad with 0s on the left.
     *
     * @param y
     * @return
     */
    private String getStrike(long y) {
        String tmp = String.valueOf(y);
        while (tmp.length() < 5) {
            tmp = "0" + tmp;
        }
        return tmp;
    }

    /**
     * Notify the item that one of its options have received update.
     *
     * @param option the option that received update
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    public boolean optionUpdated(Option option, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        if (log.isTraceEnabled()) {
            if (option instanceof StrikeOption) {
                StrikeOption so = (StrikeOption) option;
                log.trace("TRADINGDATA_.TA25 - " + so.getOptionName() + " TRDPRC_1: " + so.getLast() + " ASK: " + so.getAsk() + " BID: " + so.getBid());
            }
            if (option instanceof InterestOption) {
                InterestOption io = (InterestOption) option;
                log.trace("TRADINGDATA_.TA25 - " + io.getOptionName() + " ASK: " + io.getAsk() + " BID: " + io.getBid());
            }
        }
//        lastReutersUpdateReceiveTime = System.currentTimeMillis();
        if (option.getOptionName().equals(interest.getOptionName())) {
            if (isSnapshotReceived() && interest.isSnapshotReceived()) {
                setLongTermSnapshotReceived(true);
                sendUpdate(sender, lifeId, UPDATE_LONG_TERM_OPPS, true, false);
            }
        }
        if (canCalcLevel()) {
            if (log.isTraceEnabled()) {
                log.trace("Can calc level");
            }
            setSnapshotReceived(true);
            if (interest.isSnapshotReceived()) {
                setLongTermSnapshotReceived(true);
            }
            calcLevel(sender, lifeId);
//            sendUpdate(sender, lifeId, UPDATE_ALL_OPPS);
        } else {
            if (log.isTraceEnabled()) {
                log.trace("Don't calc level.");
            }
        }
        return false;
    }

    /**
     * Instance secondRIC in method so subclasses that doesn't need it can overwrite this method with empty one or something else.
     */
	protected void getSecondRic() {
		// do nothing. we don't need secondRIC in this class
	}

	/**
     * Tell the item to open (subscribe) all its options (when you want to start using it).
     *
     * @param itm the Item instance to receive the updates for the options.
     */
    public void subscribeOptions(Item itm) {
        super.subscribeOptions(itm);
        telAviv25 = (TelAviv25Option) Option.getInstance(".TA25");
        optionSubscribtionManager.subscribeOption(itm, telAviv25);
        nextMonth = OpportunitiesManager.getOpportunityOptionsChangeMonth(marketId);
    }

    /**
     * Clear the options list (unsibscribe each option it remove).
     * Make sure you have lock over the options when you call this metod.
     *
     * @param itm the Item instance to be removed from the options.
     */
    protected void clearOptions(Item itm) {
        super.clearOptions(itm);
        optionSubscribtionManager.unsubscribeOption(itm, telAviv25);

    }

    /**
     * When the parameter have been updated notify the item so it recalculate
     * the last level (if possible).
     */
    public void parametherUpdated(SenderThread sender, long lifeId) {
        if (canCalcLevel()) {
            calcLevel(sender, lifeId);
        }
    }

    /**
     * Check if the closing level for specified opportunity has been received.
     *
     * @param o the opportunity to check for if the closing level is ready
     * @return <code>true</code> if the closing level is ready to be taken else <code>false</code>
     */
    public boolean isClosingLevelReceived(Opportunity o) {
        if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
//            return null != lastTrdPrc;
            return o.isClosingLevelReceived();
        } else {
            // NOTE: this check rely that it will be called only for today opps. If we want
            // a more general implementation we should check for the opp closing day.
            log.debug("isClosingLevelReceived checking is today for " + itemName + " lastHstcloseDate " + lastHstCloseDate);
            if (null != lastHstCloseDate) {
                log.debug("got " + isToday(lastHstCloseDate, "GMT") + " its close for today " + itemName );
                return isToday(lastHstCloseDate, "GMT");
            }
            log.debug("got false its close for today no lastHstCloseDate" + itemName );
            return false;
        }
    }

    /**
     * Calculate the level in to the <code>lastFormulaResult</code>.
     * Call this metod only when you have lock on the options.
     */
    protected void calcLevel(SenderThread sender, long lifeId) {
        BigDecimal z = BigDecimalHelper.ZERO;
        StrikeOption co = null;
        StrikeOption po = null;
        BigDecimal c = null;
        BigDecimal p = null;
        int oppsUsed = 0;
        for (int i = 0; i < 3; i++) {
            co = (StrikeOption) options.get(i * 2);
            po = (StrikeOption) options.get(i * 2 + 1);
            if (null == co || null == co.getLast() || co.getLast() == 0 ||
                    null == co.getAsk() || co.getAsk() == 0 ||
                    null == co.getBid() || co.getBid() == 0 ||
                    co.getAsk() - co.getBid() > optionIgnoreAskBidSpread ||
                    null == po || null == po.getLast() || po.getLast() == 0 ||
                    null == po.getAsk() || po.getAsk() == 0 ||
                    null == po.getBid() || po.getBid() == 0 ||
                    po.getAsk() - po.getBid() > optionIgnoreAskBidSpread) {
                continue;
            }
            oppsUsed++;
            c =
                    new BigDecimal(co.getLast().toString())
                    .add(new BigDecimal(co.getAsk().toString()))
                    .add(new BigDecimal(co.getBid().toString()))
                    .divide(BigDecimalHelper.THREE, BigDecimalHelper.BIG_DECIMAL_CALC_SCALE, RoundingMode.HALF_UP);
            p =
                    new BigDecimal(po.getLast().toString())
                    .add(new BigDecimal(po.getAsk().toString()))
                    .add(new BigDecimal(po.getBid().toString()))
                    .divide(BigDecimalHelper.THREE, BigDecimalHelper.BIG_DECIMAL_CALC_SCALE, RoundingMode.HALF_UP);
            z =
                    z
                    .add(new BigDecimal(co.getStrike()))
                    .add(c.subtract(p).divide(new BigDecimal(100)));
        }
        if (oppsUsed == 0) {
            haveUpdate(sender, lifeId, true, lastRealLevel, lastRealLevel, false, 0);
            return;
        }
        z = z.divide(new BigDecimal(oppsUsed), BigDecimalHelper.BIG_DECIMAL_CALC_SCALE, RoundingMode.HALF_UP);
        //level without parameter
        BigDecimal k = z.multiply(BigDecimalHelper.TA_WEIGHT_OPTIONS)
		   			    .add(lastTrdPrc.multiply(BigDecimalHelper.TA_WEIGHT_MARKET));
		//level with parameter
        double crrLevel = k.add(new BigDecimal(LevelService.getTa25Parameter()))
        				   .doubleValue(); //.divide(BigDecimalHelper.TWO).doubleValue();;

        haveUpdate(sender, lifeId, true, crrLevel, crrLevel, false, 0);
    }

    @Override
    public boolean isShouldBeClosed(long oppId) {
        Opportunity o = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            if (o.getId() == oppId) {
                if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
                    return System.currentTimeMillis() - lastReutersUpdateReceiveTime <= dontCloseAfterXSecNoUpdate * 1000;
                }
                break;
            }
        }
        return true;
    }

    public void resetInTheEndOfTheDay() {
        super.resetInTheEndOfTheDay();

        lastHstClose = 0;
        lastHstCloseDate = null;
        telAviv25 = null;
    }

    public boolean isReutersStateOK() {
        return telAviv25.isReutersStateOK() && super.isReutersStateOK();
    }
}