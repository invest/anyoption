package il.co.etrader.service.level.items;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ReutersQuotes;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.SenderThread;

/**
 * Implement the logic for Tel Aviv Nadlan index item.
 *
 * @author Tony
 */
public class NadlanIndexItem extends IndexItem {
    private static final Logger log = Logger.getLogger(NadlanIndexItem.class);

    protected double lastHstClose;
    protected String lastHstCloseDate;

    public NadlanIndexItem(String itemName, Handle handle) {
        super(itemName, handle);
        useMainRic = true;
    }

    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        super.update(fields, sender, lifeId, opportunitiesTemp);
        if (null != fields.get("HST_CLOSE") && null != fields.get("HSTCLSDATE")) {
            lastHstClose = (Double) fields.get("HST_CLOSE");
            lastHstCloseDate = (String) fields.get("HSTCLSDATE");
            if (isToday(lastHstCloseDate, "Israel")) {
            	Opportunity o = getOppInState(Opportunity.STATE_CLOSING);
            	if (null != o) {
            		o.setClosingLevelReceived(true);
            	}
            	return true;
            }
        }
        return false;
    }

    /**
     * Check if the closing level for specified opportunity has been received.
     *
     * @param o the opportunity to check for if the closing level is ready
     * @return <code>true</code> if the closing level is ready to be taken else <code>false</code>
     */
    public boolean isClosingLevelReceived(Opportunity o) {
        if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
//            return isSnapshotReceived(o);
            return o.isClosingLevelReceived();
        } else {
            // NOTE: this check rely that it will be called only for today opps. If we want
            // a more general implementation we should check for the opp closing day.
            if (null != lastHstCloseDate) {
                return isToday(lastHstCloseDate, "Israel");
            }
            return false;
        }
    }

    /**
     * Get the closing level for the specified opportunity.
     *
     * @param o the opportunity for which to get closing level
     * @return The level as received from Reuters. No formula and exposure control calculations.
     */
    public double getClosingLevel(Opportunity o) {
        long decimal = o.getMarket().getDecimalPoint() - o.getMarket().getDecimalPointSubtractDigits();
        if (o.isUseManualClosingLevel()) {
            return roundDouble(o.getManualClosingLevel(), decimal);
        }
        if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
            double cl = roundDouble(o.getNextClosingLevel(), decimal);
            if (log.isInfoEnabled()) {
                log.info("TRADINGDATA_" + itemName +
                        " expiration - opportunityId: " + o.getId() +
                        " TRDPRC_1: " + o.getNextClosingLevel() +
                        " expiration level: " + cl);
            }
            return cl;
        } else {
            double cl = roundDouble(lastHstClose, decimal);
            if (log.isInfoEnabled()) {
                log.info("TRADINGDATA_" + itemName +
                        " expiration - opportunityId: " + o.getId() +
                        " HST_CLOSE: " + lastHstClose +
                        " expiration level: " + cl);
            }
            o.setLastUpdateQuote(new ReutersQuotes(new Date(), lastHstClose));
            return cl;
        }
    }

    /**
     * Reset the market internal state in the end of the day to be ready for the next
     * day opening. Long term shiftings, updates flags etc.
     */
    public void resetInTheEndOfTheDay() {
        super.resetInTheEndOfTheDay();

        lastHstClose = 0;
        lastHstCloseDate = null;
    }

    @Override
    public boolean isShouldBeClosed(long oppId) {
        return true;
    }
}