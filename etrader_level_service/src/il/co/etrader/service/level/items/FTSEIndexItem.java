package il.co.etrader.service.level.items;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ReutersQuotes;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.BigDecimalHelper;
import il.co.etrader.service.level.LevelService;
import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.options.InterestOption;
import il.co.etrader.service.level.options.Option;
import il.co.etrader.service.level.options.SecondRICOption;

/**
 * FTSE index current level calculation implementation.
 *
 * @author Tony
 */
public class FTSEIndexItem extends Item {
    private static final Logger log = Logger.getLogger(FTSEIndexItem.class);

    protected static final BigDecimal OOO5 = new BigDecimal("0.005");

    protected SecondRICOption secondRIC;
    protected InterestOption interest;

    protected BigDecimal lastFirstRICTrdprc1;
    protected BigDecimal lastASK;
    protected BigDecimal lastBID;
    protected BigDecimal lastTrdPrc;
    protected String lastTrdTim;
    protected long lastASKUpdateTime;
    protected long lastBIDUpdateTime;
    protected long lastTrdprcUpdateTime;
    protected long lastFirstRICTrdprcUpdateTime;
    protected boolean valuesCheckDisable;

    public FTSEIndexItem(String itemName, Handle handle) {
        super(itemName, handle);

        shouldBeClosedByMonitoring = true;
        interest = (InterestOption) Option.getInstance(getInterestName());
        valuesCheckDisable = false;
    }

    protected String getInterestName() {
    	return "GBP1MD=";
    }

    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
    @Override
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        boolean gotClosingLevel = false;
        Double crrTrdPrc1 = (Double) fields.get("TRDPRC_1");
        String trdTim = (String) fields.get("SALTIM");//fields.get("TRDTIM_1");

        if (null != trdTim) {
            lastTrdTim = trdTim;
        }
        if (null != crrTrdPrc1 && crrTrdPrc1 > 0) {
            lastReutersUpdateReceiveTime = System.currentTimeMillis();
            lastFirstRICTrdprcUpdateTime = lastReutersUpdateReceiveTime;

            Date updateTime = null;
            if (null != lastTrdTim) {
                try {
                    updateTime = getUpdateTime(lastTrdTim, "GMT");
                } catch (Throwable t) {
                    log.error("Can't process TRDTIM_1.", t);
                    updateTime = new Date(System.currentTimeMillis());
                }
            } else {
                log.error("TRDPRC_1 update without TRDTIM_1!!!");
                updateTime = new Date(System.currentTimeMillis());
            }

            String closingLevelTxt = "formula:TRDPRC_1;TRDPRC_1:" + crrTrdPrc1 + ";closeLevelBeforeRound:" + crrTrdPrc1;
            ReutersQuotes currentQuote = new ReutersQuotes(updateTime, crrTrdPrc1);
            gotClosingLevel = updateOpportunitiesClosingLevel(updateTime, crrTrdPrc1, crrTrdPrc1,closingLevelTxt, closingLevelTxt, currentQuote, currentQuote, opportunitiesTemp);

            lastFirstRICTrdprc1 = new BigDecimal(crrTrdPrc1.toString());
            if (null != secondRIC && secondRIC.isSnapshotReceived()) {
                setSnapshotReceived(true);

                if (interest.isSnapshotReceived()) {
                    setLongTermSnapshotReceived(true);
                }
/*
                if (hasOpenedOpportunities() && !hasOpenedHourly()) {
                    if (log.isTraceEnabled()) {
                        log.trace(itemName + " - add random to real level");
                    }
                    crrTrdPrc1 = formulaRandomChange(crrTrdPrc1, -0.0001, 0.0001);
                }
*/
                calcLevel(sender, lifeId, true, crrTrdPrc1);
            }
        }
        return gotClosingLevel;
    }

    /**
     * Notify the item that one of its options have received update.
     *
     * @param option the option that received update
     * @param jmsHandler
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    @Override
    public boolean optionUpdated(Option option, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        if (option.getOptionName().equals(secondRIC.getOptionName())) {
            try {
                boolean shouldRecalc = false;
                if (null != secondRIC.getAsk() && secondRIC.getAsk() > 0) {
//                    lastReutersUpdateReceiveTime = System.currentTimeMillis();
                    lastASKUpdateTime = secondRIC.getLastReutersUpdateReceiveTime();
                    lastASK = new BigDecimal(secondRIC.getAsk().toString());
                    shouldRecalc = true;
                }
                if (null != secondRIC.getBid() && secondRIC.getBid() > 0) {
//                    lastReutersUpdateReceiveTime = System.currentTimeMillis();
                    lastBIDUpdateTime = secondRIC.getLastReutersUpdateReceiveTime();
                    lastBID = new BigDecimal(secondRIC.getBid().toString());
                    shouldRecalc = true;
                }
                if (null != secondRIC.getLast() && secondRIC.getLast() > 0) {
//                    lastReutersUpdateReceiveTime = System.currentTimeMillis();
                    lastTrdprcUpdateTime = secondRIC.getLastReutersUpdateReceiveTime();
                    lastTrdPrc = new BigDecimal(secondRIC.getLast().toString());
                    shouldRecalc = true;
                }
                if (shouldRecalc &&
                        null != lastFirstRICTrdprc1 &&
                        null != lastASK &&
                        null != lastBID &&
                        null != lastTrdPrc) {
                    setSnapshotReceived(true);

                    if (!isLongTermSnapshotReceived() && interest.isSnapshotReceived()) {
                        setLongTermSnapshotReceived(true);
                    }

                    calcLevel(sender, lifeId, false, 0);
                }
            } catch (Exception e) {
                log.error("Failed to process option update.", e);
            }
        }
        return false;
    }

    protected String getSecondRicName() {
        return "FFI" + getSecondRicLetter(INDICES_DEFAULT_LETTER); // getFutureQuarterLetter(nextMonth) + getFutureYearDigit(nextMonth ? 1 : 0);
    }

    /**
     * Tell the item to open (subscribe) all its options (when you want to start using it).
     *
     * @param itm the Item instance to receive the updates for the options.
     */
    @Override
    public void subscribeOptions(Item itm) {
        super.subscribeOptions(itm);
        //not in use any more
        secondRIC = (SecondRICOption) Option.getInstance(getSecondRicName());
        optionSubscribtionManager.subscribeOption(itm, secondRIC);
        optionSubscribtionManager.subscribeOption(itm, interest);
    }

    @Override
    public boolean isReutersStateOK() {
        return null != secondRIC && secondRIC.isReutersStateOK() && null != interest && interest.isReutersStateOK() && super.isReutersStateOK();
    }

    /**
     * Clear the options list (unsibscribe each option it remove).
     * Make sure you have lock over the options when you call this metod.
     *
     * @param itm the Item instance to be removed from the options.
     */
    @Override
    protected void clearOptions(Item itm) {
        super.clearOptions(itm);
        optionSubscribtionManager.unsubscribeOption(itm, secondRIC);
        secondRIC = null;
        optionSubscribtionManager.unsubscribeOption(itm, interest);
    }

    /**
     * When the parameter have been updated notify the item so it recalculate
     * the last level (if possible).
     */
    public void parametherUpdated(SenderThread sender, long lifeId) {
        if (canCalcLevel()) {
            calcLevel(sender, lifeId, false, 0);
        }
    }

	/**
     * Check if the closing level for specified opportunity has been received.
     *
     * @param o the opportunity to check for if the closing level is ready
     * @return <code>true</code> if the closing level is ready to be taken else <code>false</code>
     */
    @Override
    public boolean isClosingLevelReceived(Opportunity o) {
        return o.isClosingLevelReceived();
    }

    /**
     * Get the closing level for the specified opportunity.
     *
     * @param o the opportunity for which to get closing level
     * @return The level as received from Reuters. No formula and exposure control calculations.
     */
    @Override
    public double getClosingLevel(Opportunity o) {
        long decimal = o.getMarket().getDecimalPoint() - o.getMarket().getDecimalPointSubtractDigits();
        if (o.isUseManualClosingLevel()) {
            return roundDouble(o.getManualClosingLevel(), decimal);
        }
        double cl = roundDouble(o.getNextClosingLevel(), decimal);
        if (log.isInfoEnabled()) {
            log.info("TRADINGDATA_" + itemName +
                    " expiration - opportunityId: " + o.getId() +
                    " TRDPRC_1: " + o.getNextClosingLevel() +
                    " expiration level: " + cl);
        }
        return cl;
    }

    /**
     * Calculate the level in to the <code>lastFormulaResult</code>.
     * Call this metod only when you have lock on the options.
     *
     * @param sender
     * @param lifeId
     * @param haveRealLevelUpdate
     * @param realLevelUpdate
     */
    protected void calcLevel(SenderThread sender, long lifeId, boolean haveRealLevelUpdate, double realLevelUpdate) {
        boolean c1 = lastASK.compareTo(lastBID) < 0;
        boolean c2 = lastASK.subtract(lastBID).compareTo(lastTrdPrc.multiply(OOO5)) > 0;
        double cl = 0;
        String sdo = shouldDisableOpportunities();
        if ((c1 || c2 || !sdo.equals("")) && hasOpenedNotExpiringOpportunities()) {
            cl = lastFirstRICTrdprc1.doubleValue();
            if (log.isInfoEnabled()) {
                log.info(itemName + " safety case - current level: " + cl);
            }

            String desc = "";
            desc += c1 ? "(ASK - BID) < 0; " : "";
            desc += c2 ? "(ASK - BID) > LAST * 0.005; " : "";
            desc += !sdo.equals("") ? sdo + "; " : "";
            if (disableOpportunities(desc)) {
                valuesCheckDisable = true;
                String str =
                        itemName + " " + desc +
                        " ASK = " + lastASK +
                        " BID = " + lastBID +
                        " LAST = " + lastTrdPrc;
                log.warn(str);
                LevelService.getInstance().sendAlertEmail("Values check disable " + itemName, str);
            }
        } else {
            BigDecimal tmpcl =
                    lastTrdPrc
                    .add(lastASK)
                    .add(lastBID)
                    .divide(BigDecimalHelper.THREE, BigDecimalHelper.BIG_DECIMAL_CALC_SCALE, RoundingMode.HALF_UP);
            cl = addFuture(tmpcl);

            if (log.isDebugEnabled()) {
                valuesCheckDisable = false;
                if (log.isTraceEnabled()) {
                    log.debug(itemName +
                            " normal case - ASK: " + lastASK +
                            " BID: " + lastBID +
                            " TRDPRC_1: " + lastTrdPrc +
                            " param: " + getParameter() +
                            " cl: " + cl);
                }
            }

            if (enableOpportunities("Values back to normal", false)) {
                String str = itemName + " enabled opportunities";
                log.warn(str);
                LevelService.getInstance().sendAlertEmail("Values check enable " + itemName, str);
            }
        }
        haveUpdate(sender, lifeId, true, cl, cl, haveRealLevelUpdate, realLevelUpdate);
    }

    /**
     * @param o the opportunity for which to calc the level
     * @return The long term formula result for this item.
     */
    @Override
    protected double longTermFormula(Opportunity o) {
    	return ItemsUtil.calcIndexAndStockLongTermLevel(itemName, o, interest, lastCalcResult);
    }

    /**
     * add the future to the level
     * @param tmpcl the level without the future
     * @return the level with future
     */
    protected double addFuture(BigDecimal tmpcl) {

    	return tmpcl.add(getParameter())
    				.add(lastFirstRICTrdprc1)
        			.divide(BigDecimalHelper.TWO)
        			.doubleValue();
	}

	/**
     * @return Hook to get the param value.
     */
    protected BigDecimal getParameter() {
        return LevelService.getFtseParameter();
    }

    /**
     * @return Description why the opps should be disabled or empty string if
     *      the opss should not be disabled.
     */
    @Override
    public String shouldDisableOpportunities() {
        String res = super.shouldDisableOpportunities();
        if (hasOpenedNotExpiringOpportunities()) {
            if (lastASKUpdateTime > 0 && // make sure we got one so we don't disable all on startup
                    System.currentTimeMillis() - lastASKUpdateTime >= 120000) {
                String str = secondRIC.getOptionName() + " ASK not updated for 2 min.";
                log.warn(str);
                if (!res.equals("")) {
                    res += "; ";
                }
                res += str;
            }
            if (lastBIDUpdateTime > 0 && // make sure we got one so we don't disable all on startup
                    System.currentTimeMillis() - lastBIDUpdateTime >= 120000) {
                String str = secondRIC.getOptionName() + " BID not updated for 2 min.";
                log.warn(str);
                if (!res.equals("")) {
                    res += "; ";
                }
                res += str;
            }
            if (lastTrdprcUpdateTime > 0 && // make sure we got one so we don't disable all on startup
                    System.currentTimeMillis() - lastTrdprcUpdateTime >= 120000) {
                String str = secondRIC.getOptionName() + " TRDPRC_1 not updated for 2 min.";
                log.warn(str);
                if (!res.equals("")) {
                    res += "; ";
                }
                res += str;
            }
            if (lastFirstRICTrdprcUpdateTime > 0 && // make sure we got one so we don't disable all on startup
                    System.currentTimeMillis() - lastFirstRICTrdprcUpdateTime >= 120000) {
                String str = itemName + " TRDPRC_1 not updated for 2 min.";
                log.warn(str);
                if (!res.equals("")) {
                    res += "; ";
                }
                res += str;
            }
        }
        return res;
    }

    /**
     * @return <code>ture</code> if the opened opportunities of this item
     *      should be enabled else <code>false</code>.
     */
    @Override
    public boolean shouldEnableOpportunities() {
        if (!valuesCheckDisable &&
                super.shouldEnableOpportunities() &&
                System.currentTimeMillis() - lastASKUpdateTime < 120000 &&
                System.currentTimeMillis() - lastBIDUpdateTime < 120000 &&
                System.currentTimeMillis() - lastTrdprcUpdateTime < 120000 &&
                System.currentTimeMillis() - lastFirstRICTrdprcUpdateTime < 120000) {
            log.warn("Updates and values back to normal " + itemName);
            return true;
        }
        return false;
    }


    /**
     * Reset the market internal state in the end of the day to be ready for the next
     * day opening. Long term shiftings, updates flags etc.
     */
    @Override
    public void resetInTheEndOfTheDay() {
        super.resetInTheEndOfTheDay();

        lastASK = null;
        lastBID = null;
        lastTrdPrc = null;
        valuesCheckDisable = false;
        lastFirstRICTrdprc1 = null;
        lastASKUpdateTime = 0;
        lastBIDUpdateTime = 0;
        lastTrdprcUpdateTime = 0;
        lastFirstRICTrdprcUpdateTime = 0;
    }

    @Override
    public boolean isShouldBeClosed(long oppId) {
        Opportunity o = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            if (o.getId() == oppId) {
                if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
                    return (System.currentTimeMillis() - lastReutersUpdateReceiveTime <= dontCloseAfterXSecNoUpdate * 1000) &&
                    		(System.currentTimeMillis() - secondRIC.getLastReutersUpdateReceiveTime() <= dontCloseAfterXSecNoUpdate * 1000);
                }
                break;
            }
        }
        return true;
    }
}