package il.co.etrader.service.level.items;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.options.CurrencyOption;
import il.co.etrader.service.level.options.Option;

/**
 * Implement the formula for currency items (except the ILS).
 *
 * @author Eyal
 */
public class JpyCurrencyItem extends CurrencyItem {
    private static final Logger log = Logger.getLogger(JpyCurrencyItem.class);
    protected CurrencyOption firstOption;
    protected CurrencyOption jpyOption;

    public JpyCurrencyItem(String itemName, Handle handle) {
        super(itemName, handle);

        useMainRic = false;
        shouldBeClosedByMonitoring = false;

        firstOption = (CurrencyOption) Option.getInstance("EUR=");
        if (itemName.equals("GBPJPY=")) {
            firstOption = (CurrencyOption) Option.getInstance("GBP=");
        }
        jpyOption = (CurrencyOption) Option.getInstance("JPY=");

    }

    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
    @Override
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        return false;
    }

    @Override
    public boolean optionUpdated(Option option, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
    	lastReutersUpdateReceiveTime = System.currentTimeMillis();
        if (jpyOption.getLastLevel() != null && firstOption.getLastLevel() != null) {
            double lvl = jpyOption.getLastLevel() * firstOption.getLastLevel();
            double crrLevel = formulaRandomChange(lvl);
            setSnapshotReceived(true);
            if (interestUSD1M.isSnapshotReceived() && interestCRR1M.isSnapshotReceived()) {
                setLongTermSnapshotReceived(true);
            }
            haveUpdate(sender, lifeId, true, lvl, crrLevel, true, lvl);
        }
        return super.optionUpdated(option, sender, lifeId, opportunitiesTemp);
    }

    /**
     * Tell the item to open (subscribe) all its options (when you want to start using it).
     *
     * @param itm the Item instance to receive the updates for the options.
     */
    @Override
    public void subscribeOptions(Item itm) {
        super.subscribeOptions(itm);
        optionSubscribtionManager.subscribeOption(itm, firstOption);
        optionSubscribtionManager.subscribeOption(itm, jpyOption);
    }

    @Override
    public boolean isReutersStateOK() {
        return firstOption.isReutersStateOK() && jpyOption.isReutersStateOK() && super.isReutersStateOK();
    }

    /**
     * Clear the options list (unsibscribe each option it remove).
     * Make sure you have lock over the options when you call this metod.
     *
     * @param itm the Item instance to be removed from the options.
     */
    @Override
    protected void clearOptions(Item itm) {
        super.clearOptions(itm);
        optionSubscribtionManager.unsubscribeOption(itm, firstOption);
        optionSubscribtionManager.unsubscribeOption(itm, jpyOption);
    }

    /**
     * @return Description why the opps should be disabled or empty string if
     *      the opss should not be disabled.
     */
    @Override
    public String shouldDisableOpportunities() {
        String str = "";
        if (disableAfterPeriod > 0 &&
                isSnapshotReceived() && // make sure we got one so we don't disable all on startup
                lastReutersUpdateReceiveTime != disabledAfterUpdateAt &&
                hasOpenedNotExpiringOpportunities()) {
            if (System.currentTimeMillis() - lastReutersUpdateReceiveTime >= disableAfterPeriod) {
                str = "No updates received for market " + itemName +
                " for " + (System.currentTimeMillis() - lastReutersUpdateReceiveTime) + " milliseconds";
                log.warn(str);

                return str;
            } else if (System.currentTimeMillis() - firstOption.getLastReutersUpdateReceiveTime() >= disableAfterPeriod){
                str = "No updates received for option " + firstOption.getOptionName() +
                " for " + (System.currentTimeMillis() - firstOption.getLastReutersUpdateReceiveTime()) + " milliseconds. disable market " + itemName;
                log.warn(str);

                return str;
            } else if (System.currentTimeMillis() - jpyOption.getLastReutersUpdateReceiveTime() >= disableAfterPeriod){
                str = "No updates received for option " + jpyOption.getOptionName() +
                " for " + (System.currentTimeMillis() - jpyOption.getLastReutersUpdateReceiveTime()) + " milliseconds. disable market " + itemName;
                log.warn(str);

                return str;
            }
        }
        return str;
    }

    /**
     * @return <code>ture</code> if the opened opportunities of this item
     *      should be enabled else <code>false</code>.
     */
    @Override
    public boolean shouldEnableOpportunities() {
        if (disableAfterPeriod > 0 &&
                disabledAfterUpdateAt > 0 &&
                System.currentTimeMillis() - lastReutersUpdateReceiveTime < disableAfterPeriod &&
                System.currentTimeMillis() - firstOption.getLastReutersUpdateReceiveTime() < disableAfterPeriod &&
                System.currentTimeMillis() - jpyOption.getLastReutersUpdateReceiveTime() < disableAfterPeriod &&
                hasOpenedNotExpiringOpportunities() &&
                isReutersStateOK()) {
            log.warn("Updates resumed for market " + itemName);

            return true;
        }
        return false;
    }

}