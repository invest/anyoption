package il.co.etrader.service.level.items;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunitySkinGroupMap;
import com.anyoption.common.beans.ReutersQuotes;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.InvestmentType;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.jms.ETraderFeedMessage;
import com.anyoption.common.jms.MonitoringAlertMessage;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.common.util.GoldenMinute.Expiry;
import com.anyoption.service.level.Utils;
import com.reuters.rfa.common.Handle;

import il.co.etrader.bl_vos.Skins;
import il.co.etrader.service.level.LevelService;
import il.co.etrader.service.level.OpportunitiesManager;
import il.co.etrader.service.level.OptionSubscribtionManager;
import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.options.Option;
import il.co.etrader.service.level.reuters.BitcoinUpdatesThread;


/**
 * Helper class to keep the item snapshot state and handle and opportunities.
 *
 * @author Tony
 */
public abstract class Item {
    private static Logger log = Logger.getLogger(Item.class);

    public static final int UPDATE_ALL_OPPS             = 1;
    public static final int UPDATE_TODAY_OPPS           = 2;
    public static final int UPDATE_LONG_TERM_OPPS       = 3;

    private static final BigDecimal O7 = new BigDecimal("0.7");
    private static final BigDecimal O8 = new BigDecimal("0.8");

    //number of skines
    public static final int NUMBER_OF_SKINES = 10;

    public static final String INDICES_DEFAULT_LETTER = "c1";
    public static final String INDICES_SECOND_LETTER = "c2";
    public static final String COMMODITIES_DEFAULT_LETTER = "v1";

    protected static final String QUOTE_FIELDS_ASK	= "QUO_ASK";
    protected static final String QUOTE_FIELDS_BID	= "QUO_BID";
    protected static final String QUOTE_FIELDS_LAST	= "QUO_LAST";
    
    private static Random random = new Random();

    protected String itemName;
    protected boolean useMainRic;
    protected boolean snapshotReceived;
    private boolean longTermSnapshotReceived;
    private Handle handle;
    protected boolean subscribedToReuters;
    protected ArrayList<Opportunity> opportunities;
    protected boolean HasGoodForHomepageCompare; //if this item have good for homepage for skin, only for homepage priorty compare function
    protected boolean onHomePageCompare; //if this item have opp on homepage for skin, only for homepage priorty compare function
    protected HashMap<Integer, Boolean> onHomePage;
    protected int homePagePriority;
    protected long decimalPoint;
    protected long marketId;
    protected long optionPlusMarketId;
    protected double lastCalcResult;
    protected double lastFormulaResult;
    protected double lastRandomChange;
    protected double lastRealLevel;
    protected Opportunity specialEntry;
    protected int updatesPause;
    protected BigDecimal significantPercentage;
    protected long lastUpdateTime;
    protected int realUpdatesPause;
    protected BigDecimal realSignificantPercentage;
    protected long lastRealUpdateTime;
    protected double randomCeiling;
    protected double randomFloor;
    protected BigDecimal firstShiftParameter;
    protected BigDecimal averageInvestments;
    protected BigDecimal percentageOfAverageInvestments;
    protected BigDecimal fixedAmountForShifting;
    protected long typeOfShifting;
    protected boolean suspended;
    protected String suspendedMessage;
    protected String quoteParams;

    protected long dontCloseAfterXSecNoUpdate;

    protected long lastReutersUpdateReceiveTime;
    private long todayOpeningTime;
    protected boolean reutersStateOK;

    /**
     * Time since last reuters update after which to disable the market
     * (opportunities on this market). Set to 0 to deactivate this logic.
     */
    protected long disableAfterPeriod;
    protected long disabledAfterUpdateAt;
    protected boolean disabled;

    /**
     * This is a flag that says if when the monitoring thread find that
     * opportunity on this market is not closed after configured period
     * and has current level should be closed or not. Monitoring will set
     * the "closingLevelReceived" flag of the opportunity and change the
     * state to closed on it.
     */
    protected boolean shouldBeClosedByMonitoring;

    protected OptionSubscribtionManager optionSubscribtionManager;
    protected ArrayList<Option> options;

    /**
     * skins Priorities
     * home page and group Priorities for all skins
     */
    HashMap<Integer, HashMap<String, Integer>> skinsPriorities = new HashMap<Integer, HashMap<String, Integer>>();

    /** skin id of the current skin we sort for home page priorities. */
    protected long compareSkinId;
    protected boolean optionPlusMarket;
    protected boolean binary0100Market;
    private boolean dynamicsMarket;

	/**
	 * The market group ID of the Item.
	 */
	private Long marketGroupId;

    /**
     * Create instance of the right subclass of <code>Item</code> for the specified
     * item name (RIC).
     *
     * @param itemName the item name to create <code>Item</code> instance for
     * @param handle the Reuters handle of the item
     * @return
     */
    public static Item getInstance(String itemName, Handle handle) {
        if (log.isTraceEnabled()) {
            log.trace("Getting Item instance for: " + itemName);
        }
        if (itemName.equals(BitcoinUpdatesThread.BITCOIN_FEED_NAME) || itemName.equals(BitcoinUpdatesThread.LITECOIN_FEED_NAME)) {
            return new BitcoinItem(itemName, handle);
//        } else if (itemName.equals("EURJPY=.Binary0-100") ||
//        		itemName.equals("GBP=.Binary0-100") ||
//        		itemName.equals("EUR=.Binary0-100") ||
//        		itemName.equals("SEK=.Binary0-100") ||
//        		itemName.equals("AUD=.Binary0-100") ||
//        		itemName.equals("GBP=.Dynamicssss") ||
//        		itemName.equals("AUD=.Dynamicssss") ||
//        		itemName.equals("SEK=.Dynamicssss") ||
//        		itemName.equals("JPY=.Dynamicssss") ||
//        		itemName.equals("EUR=.Dynamicssss")) {
//        	return new BinaryZeroOneHundredCurrencyItem(itemName, handle);
//        } else if (itemName.endsWith(".Binary0-100") || itemName.endsWith(".Dynamicssss")) {
//        	return new BinaryZeroOneHundredItem(itemName, handle);
        } else if (itemName.equals("GOOG.O.Option+")
        		|| itemName.equals("AAPL.O.Option+")
        		|| itemName.equals("FB.O.Option+")
        		|| itemName.equals("ORAN.PA.Option+")
        		|| itemName.equals("DAIGn.DE.Option+")
        		|| itemName.equals("BP.L.Option+")) {
        	return new OptionPlusNewStockItem(itemName, handle);
        } else if (itemName.equals("FB.O") ||
        		itemName.equals("GOOG.O") ||
        		itemName.equals("AAPL.O")) {
        	return new OptionNewStockItem(itemName, handle);
        } else if (itemName.equals("EUR=.Option+")
    			|| itemName.equals("JPY=.Option+")
    			|| itemName.equals("AUD=.Option+")
    			|| itemName.equals("GBP=.Option+")
    			|| itemName.equals("EURJPY=.Option+")
    			|| itemName.equals("BGN=")
    			|| itemName.equals("SEK=.Option+")
    			|| itemName.equals("NOK=.Option+")
    			|| itemName.endsWith(".Bubbles")) {
            return new OptionPlusCurrencyItem(itemName, handle);
        } else if (itemName.equals("CL.Option+")
        		|| itemName.equals("SI.Option+")) {
            return new OilItem(itemName, handle);
        } else if (itemName.equals(".TA25")) {
            return new TelAviv25IndexItem(itemName, handle);
        } else if (itemName.equals(".TA25INDEX")) {
            return new TelAviv25MaofIndexItem(itemName, handle);
        } else if (itemName.equals("energy israel")) {
            return new EnergyItem(itemName, handle);
        } else if (itemName.equals(".TELREAL")) {
            return new NadlanIndexItem(itemName, handle);
        } /*else if (itemName.equals(".FTSE")) {
            return new FTSEIndexItem(itemName, handle);
        } */else if (itemName.equals(".SPX") ||
                itemName.equals(".KLSE")) {
            return new SP500AndKLSEIndexItem(itemName, handle);
        } else if (itemName.equals(".FCHI")) {
            return new CACIndexItem(itemName, handle);
        } else if (itemName.equals("NQ") ||
                itemName.equals("FKLI") ||
                itemName.equals("TRF30") ||
                itemName.equals("ES") ||
                itemName.equals("ES.Option+") ||
                itemName.equals("FDX") ||
                itemName.equals("FDX.Option+") ||
                itemName.equals("RIRTS") ||
                itemName.equals("RIRTS.Option+") ||
                itemName.equals("FCE") ||
                itemName.equals("FCE.Option+")) {
            return new NasdaqFItem(itemName, handle);
        } else if (itemName.startsWith(".")
        		|| itemName.equals("VXX")) {
            return new IndexItem(itemName, handle);
        } else if (itemName.endsWith(".TA")) {
            return new StockItem(itemName, handle);
        } else if (itemName.equals("KO") ||
        		itemName.equals("PG") ||
                itemName.equals("MCD") ||
                itemName.equals("JNJ") ||
                itemName.equals("GE") ||
                itemName.equals("TKC") ||
                itemName.equals("NKE") ||
                itemName.equals("C") ||
                itemName.endsWith(".OQ") ||
                itemName.equals("PTR.N") ||
                itemName.equals("FRE") ||
                itemName.equals("MS") ||
                itemName.endsWith(".MC") ||
                itemName.endsWith(".IS") ||
                itemName.endsWith(".MX") ||
                itemName.equals("JPM") ||
                itemName.endsWith(".DE") ||
                itemName.endsWith(".CO") ||
                itemName.endsWith(".ST") ||
                itemName.endsWith(".MI") ||
                itemName.equals("NBL") ||
                itemName.endsWith(".MM") ||
                itemName.equals("WMT") ||
                itemName.equals("XOM") ||
                itemName.equals("STD") ||
                itemName.equals("AMX") ||
                itemName.equals("PBR") ||
                itemName.endsWith(".AX") ||
                itemName.endsWith(".T") ||
                itemName.equals("TAMO.NS") ||
                itemName.endsWith(".PA") ||
                itemName.endsWith(".O") ||
                itemName.equals("DIS") ||
                itemName.equals("LVS") ||
                itemName.equals("BAC") ||
                itemName.equals("PFE") ||
                itemName.equals("AIG") ||
                itemName.equals("GG") ||
                itemName.equals("SLW") ||
                itemName.equals("GS") ||
                itemName.endsWith(".KS") ||
                itemName.endsWith(".AS") ||
                itemName.endsWith(".K") ||
                itemName.equals("PEP")) {
            return new NewStockItem(itemName, handle);
        }  else if (itemName.endsWith(".SS")) {
            return new ChinaNewStockItem(itemName, handle);
        } else if (itemName.endsWith(".L")) {
            return new LondonNewStockItem(itemName, handle);
//        } else if (itemName.equals("ILS=")) {
//            return new ILSItem(itemName, handle);
//        } else if (itemName.equals("GBPJPY=") || itemName.equals("EURJPY=")) {
//            return new JpyCurrencyItem(itemName, handle);
        } else if (itemName.endsWith("=")) {
            return new OptionCurrencyItem(itemName, handle);
//        } else if (itemName.equals("GCc1") || itemName.equals("SI") || itemName.equals("HG")) {
//            return new GoldItem(itemName, handle);
        } else if (itemName.startsWith("GC") || itemName.equals("GC.Option+") || itemName.equals("SI") || itemName.equals("HG") || itemName.equals("NG") || itemName.equals("S")) {
            return new GoldItem(itemName, handle);
        } else if (itemName.startsWith("CL")) {
            return new OilItem(itemName, handle);
        } else {
        	log.fatal("Trying to create item with unknown name [" + itemName + "]");
        	return null;
        }
    }

    protected Item(String itemName, Handle handle) {
        this.snapshotReceived = false;
        this.longTermSnapshotReceived = false;
        this.itemName = itemName;
        this.handle = handle;
        opportunities = new ArrayList<Opportunity>();
        onHomePage = new HashMap<Integer, Boolean>();//false;
        options = new ArrayList<Option>();
        useMainRic = true;
        shouldBeClosedByMonitoring = false;
        lastReutersUpdateReceiveTime = System.currentTimeMillis(); // left 0 cause problems if the first update is STALE (market will not be enabled when thing get OK)
        todayOpeningTime = 0;
        reutersStateOK = true;
        dontCloseAfterXSecNoUpdate = 60;
        optionPlusMarket = itemName.endsWith("Option+");
        binary0100Market = itemName.endsWith("Binary0-100");
        dynamicsMarket = itemName.endsWith("Dynamicssss");
    }

    /**
     * @return the snapshotReceived
     */
    public boolean isSnapshotReceived(Opportunity o) {
        if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY || o.getScheduled() == Opportunity.SCHEDULED_DAYLY) {
            return snapshotReceived;
        } else {
            return longTermSnapshotReceived;
        }
    }

    /**
     * @return the snapshotReceived
     */
    public boolean isSnapshotReceived() {
        return snapshotReceived;
    }

    /**
     * @param snapshotReceived the snapshotReceived to set
     */
    public void setSnapshotReceived(boolean snapshotReceived) {
        if (!this.snapshotReceived && snapshotReceived) {
            this.snapshotReceived = snapshotReceived;
            todayOpeningTime = System.currentTimeMillis();
            LevelService lvlSrv = LevelService.getInstance();
            refreshSpecialEntry(lvlSrv.getSender(), lvlSrv.getLifeId());
            lvlSrv.updateHomePageOpportunities();
        } else {
            this.snapshotReceived = snapshotReceived;
        }
    }

    /**
     * @return the longTermSnapshotReceived
     */
    public boolean isLongTermSnapshotReceived() {
        return longTermSnapshotReceived;
    }

    /**
     * @param longTermSnapshotReceived the longTermSnapshotReceived to set
     */
    public void setLongTermSnapshotReceived(boolean longTermSnapshotReceived) {
        this.longTermSnapshotReceived = longTermSnapshotReceived;
    }

    /**
     * @return the handle
     */
    public Handle getHandle() {
        return handle;
    }

    /**
     * @param handle the handle to set
     */
    public void setHandle(Handle handle) {
        this.handle = handle;
    }

    public boolean isSubscribedToReuters() {
        return subscribedToReuters;
    }

    public void setSubscribedToReuters(boolean subscribedToReuters) {
        this.subscribedToReuters = subscribedToReuters;
    }

    /**
     * @return the itemName
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * @param skinId the id of the skin to check if market on homepage
     * @return the onHomePage
     */
    public boolean isOnHomePage(int skinId) {
        if (onHomePage.containsKey(skinId)) {
        	return onHomePage.get(skinId);
        }
        return false;
    }

    /**
     * Change the state (on home page/not on home page) of this market. If the market was
     * not on the home page then pick an opportunity to show for it on the home page and
     * send update for it. If it was and is no longer send the corresponding update -
     * "DELETE" (from the cache) if it is a created one else just "UPDATE" (the
     * levels cache will handle the removing from the LS).
     *
     * @param onHomePage the onHomePage to set
     * @param skingId the skin id to change homepage flag in the array
     * @param sender handler to send update about the opp on the home page from this market
     * @param lifeId the service life id
     */
    public void setOnHomePage(boolean onHomePage, int skinId , SenderThread sender, long lifeId) {
        if (log.isDebugEnabled()) {
            log.debug("setOnHomePage for " + itemName + " skinId: " + skinId + ". was: " + this.onHomePage + " setting to: " + onHomePage);
        }
        this.onHomePage.put(skinId, onHomePage);
        refreshSpecialEntry(sender, lifeId);
        // the refreshSpecialEntry can't catch the case when closed market
        // move in or out of the home page
        sendUpdate(sender, lifeId, UPDATE_TODAY_OPPS, true, false);
    }

    /**
     * Add opportunity to the RICs list.
     *
     * @param o
     * @param sender handler to send update about the opp on the home page from this market
     * @param lifeId the service life id
     */
    public void addOpportunity(Opportunity o, SenderThread sender, long lifeId) {
        if (log.isDebugEnabled()) {
            log.debug("Adding opp: " + o.getMarket().getFeedName() + " state: " + o.getState() + " Time: " + o.getTimeEstClosing());
        }
        opportunities.add(o);
        //homePagePriority = o.getMarket().getHomePagePriority(); no need any more
  //      homePageHourFirst = o.getMarket().isHomePageHourFirst(); i thing we dont need it anymore
        decimalPoint = o.getMarket().getDecimalPoint();
        updatesPause = o.getMarket().getUpdatesPause();
        significantPercentage = o.getMarket().getSignificantPercentage();
        realUpdatesPause = o.getMarket().getRealUpdatesPause();
        realSignificantPercentage = o.getMarket().getRealSignificantPercentage();
        marketId = o.getMarket().getId();
        optionPlusMarketId = o.getMarket().getOptionPlusMarketId();
        randomCeiling = o.getMarket().getRandomCeiling().doubleValue();
        randomFloor = o.getMarket().getRandomFloor().doubleValue();
        firstShiftParameter = o.getMarket().getFirstShiftParameter();
        averageInvestments = o.getMarket().getAverageInvestments();
        percentageOfAverageInvestments = o.getMarket().getPercentageOfAverageInvestments();
        fixedAmountForShifting = o.getMarket().getFixedAmountForShifting();
        typeOfShifting = o.getMarket().getTypeOfShifting();
        disableAfterPeriod = o.getMarket().getDisableAfterPeriod();

        // take the disabled flag only from the first opp on startup.
        // avoid taking the flag of newly created opps (which is always enabled = lose disabled during create time)
        if (opportunities.size() == 1) {
	        disabled = o.isDisabledService();
	        if (disabled) {
	            disabledAfterUpdateAt = 1; // set something so the enable will work
	        }
        }
        refreshSpecialEntry(sender, lifeId);
        suspended = o.getMarket().isSuspended();
        suspendedMessage = o.getMarket().getSuspendedMessage();
        //bugid: 2029
//        frequencePriority = o.getMarket().getFrequencePriority();

        marketGroupId = o.getMarket().getGroupId();
    }

    /**
     * While market is opened every opportunity that is in opened state is
     * displayed separately and get updates separately. While market is closed
     * or suspended we want single "row" in the trading pages main table to
     * represent that market.
     *
     * There is one more special case - home page. On the home page we want to
     * display single opportunity for market. Depending on the config we may
     * want the hour opp that will close next or the day opp that will close
     * today.
     *
     * "Special entry" is the entry that is selected to be displayed on home
     * page (when sending updates only to this one a flag is set that says to
     * the Lightstreamer adapter this opportunity should be forwarded to the
     * "home" subscribtion) or to represent the market in closed/suspended
     * states. In the closed state the opportunity that will open next is
     * selected. In suspended state the one that will close next is used.
     *
     * @param sender
     * @param lifeId
     */
    public void refreshSpecialEntry(SenderThread sender, long lifeId) {
        Opportunity oldSpecialEntry = specialEntry;
        specialEntry = null;
        boolean hasOpened = hasOpenedOpportunities();
        if (hasOnHomePage() || !hasOpened || !isSnapshotReceived() || suspended) {
            if (log.isTraceEnabled()) {
                log.trace("Pick new special entry. onHomePage: " + onHomePage + " hasOpened: " + hasOpened + " suspended: " + suspended);
            }
            if (hasOpened && isSnapshotReceived()) {
                // we are looking for home page entry (the next that will close)
                Opportunity o = null;
                for (int i = 0; i < opportunities.size(); i++) {
                    o = opportunities.get(i);
//                    if (o.getOpportunityTypeId() != Opportunity.TYPE_REGULAR) {
//                        continue;
//                    }
                    if (null == specialEntry ||
                            (o.isInOppenedState() && !specialEntry.isInOppenedState()) ||
                            (o.isInOppenedState() && /*homePageHourFirst &&*/ o.getTimeEstClosing().compareTo(specialEntry.getTimeEstClosing()) < 0) //||
                         /* (o.isInOppenedState() && !homePageHourFirst && o.getScheduled() == Opportunity.SCHEDULED_DAYLY) ||
                            (o.isInOppenedState() && !homePageHourFirst && specialEntry.getScheduled() == Opportunity.SCHEDULED_HOURLY) ||
                            (o.isInOppenedState() && o.isLongTerm() && specialEntry.isLongTerm() && o.getTimeEstClosing().compareTo(specialEntry.getTimeEstClosing()) < 0)
                         */
                            ) {
                        specialEntry = o;
                    }
                }
            } else {
                specialEntry = getTimeMarketOpensNext();
            }
        }
        if (null != specialEntry) {
            if (log.isTraceEnabled()) {
                log.trace("Picked up: " + specialEntry.getId() + (null != oldSpecialEntry ? " Old was: " + oldSpecialEntry.getId() : " No old."));
            }
        } else {
            if (log.isTraceEnabled()) {
                log.trace("No new special entry." + (null != oldSpecialEntry ? " Old was: " + oldSpecialEntry.getId() : " No old."));
            }
        }
        if (null != oldSpecialEntry && specialEntry != oldSpecialEntry) {
            if (log.isTraceEnabled()) {
                log.trace("Removing: " + oldSpecialEntry.getId());
            }
            // When removing (sending the "DELETE" command) the old special
            // entry it still need to be special entry.
            // Else it will not be removed from the home page!
            Opportunity newSpecialEntry = specialEntry;
            specialEntry = oldSpecialEntry;
            sendUpdate(sender, lifeId, oldSpecialEntry, true, true);
            specialEntry = newSpecialEntry;
        }
        if (null != specialEntry && specialEntry != oldSpecialEntry) {
            if (log.isTraceEnabled()) {
                log.trace("Adding: " + specialEntry.getId());
            }
            sendUpdate(sender, lifeId, specialEntry, false, true);
        }
    }

    /**
     * @return <code>Opportunity</code> that can be used as special entry in the case
     *      when the market is closed or to take the markets time next open from the
     *      timeNextOpen property.
     */
    public Opportunity getTimeMarketOpensNext() {
        boolean hasOpened = hasOpenedOpportunities();
        Opportunity se = null;
        Opportunity o = null;
        boolean hasLongTerm = false;

        // we are looking for the time the market will open next
        // will be done in two steps:
        // step 1 - find the short term opp (hour/day) that will open next
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
//            if (o.getOpportunityTypeId() != Opportunity.TYPE_REGULAR) {
//                continue;
//            }
            if (o.getScheduled() == Opportunity.SCHEDULED_WEEKLY || o.getScheduled() == Opportunity.SCHEDULED_MONTHLY) {
                hasLongTerm = true;
                continue;
            }
            if (o.getState() != Opportunity.STATE_CREATED || o.getScheduled() == Opportunity.SCHEDULED_DAYLY) {
                continue;
            }
            if (hasOpened && Utils.isToday(o.getTimeFirstInvest(), "GMT+00")) {
                continue;
            }
            if (null == se ||
                    (o.getTimeFirstInvest().compareTo(se.getTimeFirstInvest()) < 0)) {
                se = o;
            }
        }

        // step 2 - by the short term opp we know the next working day
        // now check if we have long term and if one of them will open earlyer
        // on that day
        if (null != se) {
            se.setTimeNextOpen(se.getTimeFirstInvest());
            for (int i = 0; i < opportunities.size(); i++) {
                o = opportunities.get(i);
//                if (o.getOpportunityTypeId() != Opportunity.TYPE_REGULAR) {
//                    continue;
//                }
                if (o.isLongTerm() && o.getState() == Opportunity.STATE_PAUSED) { // doesn't really matter which one we will take (week/month)
                    Calendar sec = Calendar.getInstance();
                    sec.setTime(se.getTimeFirstInvest());

                    Calendar longFirstInv = Calendar.getInstance();
                    longFirstInv.setTime(o.getTimeFirstInvest());

                    Calendar longLastInv = Calendar.getInstance();
                    longLastInv.setTime(o.getTimeLastInvest());

                    // compose the time this long term opp will open on that day
                    Calendar longStartTime = Calendar.getInstance();
                    longStartTime.set(Calendar.YEAR, sec.get(Calendar.YEAR));
                    longStartTime.set(Calendar.MONTH, sec.get(Calendar.MONTH));
                    longStartTime.set(Calendar.DAY_OF_MONTH, sec.get(Calendar.DAY_OF_MONTH));
                    longStartTime.set(Calendar.HOUR_OF_DAY, longFirstInv.get(Calendar.HOUR_OF_DAY));
                    longStartTime.set(Calendar.MINUTE, longFirstInv.get(Calendar.MINUTE));
                    longStartTime.set(Calendar.SECOND, 0);
                    longStartTime.set(Calendar.MILLISECOND, 0);

//                  compose the time this long term opp will end on that day
                    Calendar longEndTime = Calendar.getInstance();
                    longEndTime.set(Calendar.YEAR, sec.get(Calendar.YEAR));
                    longEndTime.set(Calendar.MONTH, sec.get(Calendar.MONTH));
                    longEndTime.set(Calendar.DAY_OF_MONTH, sec.get(Calendar.DAY_OF_MONTH));
                    longEndTime.set(Calendar.HOUR_OF_DAY, longLastInv.get(Calendar.HOUR_OF_DAY));
                    longEndTime.set(Calendar.MINUTE, longLastInv.get(Calendar.MINUTE));
                    longEndTime.set(Calendar.SECOND, 0);
                    longEndTime.set(Calendar.MILLISECOND, 0);

                    Calendar cnow = Calendar.getInstance(); //the time now

                    if (longStartTime.compareTo(sec) < 0 && cnow.before(longStartTime) && sec.compareTo(longEndTime) < 0) { //check if the long is b4 the daily and we didnt pass the long start time
                        o.setTimeNextOpen(longStartTime.getTime());
                        se = o;
                        break;
                    }
                }
            }
        } else { //we dont have hourly
            log.debug("found market without hourly market_id" + marketId);
            Date nextOpenDateFromDb = null;
            try {
                nextOpenDateFromDb = OpportunitiesManager.getNextOpenDate(this.marketId);
            } catch (Exception e) {
                log.debug("cant get next open date from db",e);
            }
            if (null != nextOpenDateFromDb) {
            	for (int i = 0; i < opportunities.size(); i++) {
                    o = opportunities.get(i);
                    if (o.getScheduled() == Opportunity.STATE_PAUSED || o.getScheduled() == Opportunity.STATE_WAITING_TO_PAUSE) {
                    	se = o;
                    }
            	}
            	if (null == se) {
            		se = opportunities.get(0);
            	}
                se.setTimeNextOpen(nextOpenDateFromDb);
                log.debug("set time next open from db to" + nextOpenDateFromDb.toString());
                return se;
            }
        }

        //if we have only long term then choose the closest one
        if (hasLongTerm && se == null) {
            for (int i = 0; i < opportunities.size(); i++) {
                o = opportunities.get(i);
//                if (o.getOpportunityTypeId() != Opportunity.TYPE_REGULAR) {
//                    continue;
//                }
                if (o.getScheduled() == Opportunity.SCHEDULED_WEEKLY || o.getScheduled() == Opportunity.SCHEDULED_MONTHLY) {
                    if (null == se ||
                            (o.getTimeFirstInvest().compareTo(se.getTimeFirstInvest()) < 0)) {
                        se = o;
                        se.setTimeNextOpen(se.getTimeFirstInvest());
                    }
                }
            }
        }

        return se;
    }

    /**
     * Check if specified opportunity is the special entry for this market.
     *
     * @param o the opportunity to check for
     * @return <code>true</code> if the specified opportunity is the special entry else <code>false</code>.
     */
    public boolean isSpecialEntry(Opportunity o) {
        return o == specialEntry;
    }

    /**
     * Get opportunity with specified id from the opps list.
     *
     * @param id the id to look for
     * @return <code>Opportunity</code> of the opp with specified id or null if not in the list.
     */
    public Opportunity getOppById(long id) {
        Opportunity o = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            if (o.getId() == id) {
                return o;
            }
        }
        return null;
    }

    /**
     * @return The count of opps connected to this item.
     */
    public int getOppsCount() {
        return opportunities.size();
    }

    /**
     * The i-th opp connected to this item.
     *
     * @param i the index of the opp to return
     * @return <code>Opportunity</code> of the i-th opp of this item.
     */
    public Opportunity getOpp(int i) {
        return opportunities.get(i);
    }

    /**
     * Update the level of the opps that are not opened with the level
     * of the opportunity that have just closed.
     *
     * @param closed the opportunity that just closed
     */
    public void updateLastClosedLevel(Opportunity closed) {
        double closingLevel = getClosingLevel(closed);
        Opportunity w = null;
        for (Iterator<Opportunity> i = opportunities.iterator(); i.hasNext();) {
            w = i.next();
            if (w != closed && w.getState() != Opportunity.STATE_CLOSED && w.getState() != Opportunity.STATE_DONE) {
                w.setClosingLevel(closingLevel);
            }
        }
    }

    /**
     * Update the market opportunities closing level. The opportunity closing level is updated according to
     * the opportunity type (HOUR/DAY...). The closing level value is updated only if the closing time is
     * after the current time.
     *
     * @param crrTime the time of the update
     * @param newHourClosingLevel
     * @param newDayClosingLevel
     * @param closingLevelTxtDay
     * @param closingLevelTxtHour
     * @param opportunities2
     * @return <code>true</code> if there is an opened opportunity which closing time is before this update
     *      (and was not updated - which mean it is ready to be closed) else <code>false</code>.
     */
    protected boolean updateOpportunitiesClosingLevel(Date crrTime, double newHourClosingLevel, double newDayClosingLevel, String closingLevelTxtHour, String closingLevelTxtDay, ReutersQuotes currentHourlyQuote, ReutersQuotes currentDailyQuote, ArrayList<Opportunity> opportunitiesTemp) {
        boolean haveReadyToClose = false;
        Opportunity o = null;
        if (null == opportunitiesTemp) {
        	opportunitiesTemp = this.opportunities;
        }
        for (Iterator<Opportunity> i = opportunitiesTemp.iterator(); i.hasNext();) {
            o = i.next();
            if (o.isInOppenedState()) {
                if (crrTime.compareTo(o.getTimeEstClosing()) < 0) {
                    if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
                        o.setNextClosingLevel(newHourClosingLevel);
                        o.setCloseLevelTxt(closingLevelTxtHour);
                        if (null != currentHourlyQuote) {
                        	o.setLastAndBeforeUpdateQuote(currentHourlyQuote);
                        }
                    } else {
                        o.setNextClosingLevel(newDayClosingLevel);
                        o.setCloseLevelTxt(closingLevelTxtDay);
                        if (null != currentDailyQuote) {
                        	o.setLastAndBeforeUpdateQuote(currentDailyQuote);
                        }
                    }
                } else {
                    if (!o.isClosingLevelReceived()) {
                        o.setClosingLevelReceived(true);
                        haveReadyToClose = true;
                        if (null != currentHourlyQuote) {
                        	o.setFirstUpdateQuote(currentHourlyQuote);
                        }
                        if (log.isInfoEnabled()) {
                            log.info("Got closing level for " + itemName +
                                    " oppId: " + o.getId() +
                                    " closing level: " + o.getNextClosingLevel() +
                                    " update time: " + crrTime +
                                    " closing level txt: " + o.getCloseLevelTxt());
                        }
                    }
                }
            }
        }
        return haveReadyToClose;
    }

    /**
     * Remove the opportunity with specified id.
     *
     * @param id the id of the opp to remove
     * @return <code>true</code> if the opp was removed else <code>false</code>
     *      (it will not be removed if it is not found).
     */
    public boolean removeOpportunity(long id, SenderThread sender, long lifeId) {
        boolean removed = false;
        boolean removedSpecial = false;
        Opportunity o = null;
        for (Iterator<Opportunity> i = opportunities.iterator(); i.hasNext();) {
            o = i.next();
            if (o.getId() == id) {
                if (o == specialEntry) {
                    removedSpecial = true;
                }
                i.remove();
                removed = true;
            }
        }
        // if you removed the home or the last opened - need new special
        if (removedSpecial || !hasOpenedOpportunities()) {
            refreshSpecialEntry(sender, lifeId);
        }
        return removed;
    }

    /**
     * @return <code>true</code> if this item has at least one opp in opened state else <code>false</code>.
     */
    public boolean hasOpenedOpportunities() {
        if (opportunities.size() == 0) {
            return false;
        }
        for (int i = 0; i < opportunities.size(); i++) {
            if (opportunities.get(i).isInOppenedState()) {
                return true;
            }


        }
        return false;
    }

    /**
     * @param scheduleTypeOrder
     * @return Returns the first open opportunity of the item with the given schedule type priority.
     */
    public Opportunity getFirstOpenOpportunity(int... scheduleTypeOrder) {
    	log.trace("Looking for open market [" + getMarketId()
    					+ "] opportunity with schedule type priority order [" + scheduleTypeOrder + "]");
    	for (int scheduleType : scheduleTypeOrder) {
			for (Opportunity opportunity : opportunities) {
				if (opportunity.isAvailableForTrading() // FIXME State may be open but snapshot may not be received - please, check for snapshot too or the shown level will be 0.0 which is not good
							&& opportunity.getScheduled() == scheduleType) {
					log.trace("Found [" + opportunity + "]");
					return opportunity;
				}
			}
		}

    	log.trace("Could not find open opportunity");
    	return null;
    }

    /**
     * @return <code>true</code> if there is opened opportunity of the specified
     *      type on this market at the moment else <code>false</code>.
     */
    protected boolean hasOpened(int requestedScheduled) {
        if (opportunities.size() == 0) {
            return false;
        }
        Opportunity o = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            if (o.isInOppenedState() && o.getScheduled() == requestedScheduled) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return <code>Opportunity</code> if there is opened opportunity of the specified
     *      type on this market at the moment else <code>null</code>.
     */
    protected Opportunity getOpenedOppByScheduled(int requestedScheduled) {
        if (opportunities.size() == 0) {
            return null;
        }
        Opportunity o = null;
        /*
         * For the hour opps there is a chance there are two opportunities that are opened.
         * This can happen when the previous hour is either closed and waiting to be removed
         * or if the closing of the previous hour get delayed due to waiting for closing level.
         * In such cases we will return the new (the current hour) opportunity. To do so
         * if we run at hour opportunity that is closing or is closed we will keep reference
         * to it giving the opportunity to the for loop to find and return the other hour opp
         * (the new one). If no other hour opp then we will return the one we found.
         */
        Opportunity hourWaitingForExpiry = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            if (o.isInOppenedState() && o.getScheduled() == requestedScheduled) {
                if (requestedScheduled == Opportunity.SCHEDULED_HOURLY &&
                        (o.getState() == Opportunity.STATE_CLOSING || o.getState() == Opportunity.STATE_CLOSED)) {
                    hourWaitingForExpiry = o;
                } else {
                    return o;
                }
            }
        }
        return hourWaitingForExpiry;
    }

    /**
     * @return <code>true</code> if there is opened hourly opportunity on this market at the moment else <code>false</code>.
     */
    protected boolean hasOpenedHourly() {
        return hasOpened(Opportunity.SCHEDULED_HOURLY);
    }

    /**
     * @return <code>true</code> if there is opened weekly opportunity on this market at the moment else <code>false</code>.
     */
    private boolean hasOpenedWeekly() {
        return hasOpened(Opportunity.SCHEDULED_WEEKLY);
    }

    /**
     * @param skinId index of the skin in the array.
     * @return <code>true</code> if the markets has hourly opportunity you can invest on at the moment else <code>false</code>.
     */
    public boolean hasGoodForHomepage(int skinId) {
        if (opportunities.size() == 0) {
            return false;
        }
        boolean hasOpenedHourly = hasOpenedHourly();
        Opportunity o = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            if (((hasOpenedHourly && o.getScheduled() == Opportunity.SCHEDULED_HOURLY) ||
                    (!hasOpenedHourly && o.getScheduled() == Opportunity.SCHEDULED_DAYLY)) &&
                    (onHomePage.get(skinId) == null ? false : onHomePage.get(skinId)) &&
                    (o.getState() >= Opportunity.STATE_OPENED && o.getState() < Opportunity.STATE_CLOSING_1_MIN)) {
                return true;
            }
            if (((hasOpenedHourly && o.getScheduled() == Opportunity.SCHEDULED_HOURLY) ||
                    (!hasOpenedHourly && o.getScheduled() == Opportunity.SCHEDULED_DAYLY)) &&
                    (onHomePage.get(skinId) == null ? true : !onHomePage.get(skinId)) &&
                    (o.getState() == Opportunity.STATE_OPENED)) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.MINUTE, 20);
                Calendar cls = Calendar.getInstance();
                cls.setTime(o.getTimeEstClosing());
                if (cal.compareTo(cls) < 0) {
                    return true;
                }

            }

        }
        return false;
    }

    private boolean hasHour() {
        if (opportunities.size() == 0) {
            return false;
        }
        Opportunity o = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return <code>true</code> if there are long term opportunities for this market else <code>false</code>.
     */
    private boolean hasLongTerm() {
        if (opportunities.size() == 0) {
            return false;
        }
        Opportunity o = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            if (o.getScheduled() == Opportunity.SCHEDULED_WEEKLY ||
                    o.getScheduled() == Opportunity.SCHEDULED_MONTHLY) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return <code>true</code> if this item has at least one opp in opened or last 10 min state
     *      else <code>false</code>.
     */
    public boolean hasOpenedNotExpiringOpportunities() {
        if (opportunities.size() == 0) {
            return false;
        }
        Opportunity o = null;
        int state = 0;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            state = o.getState();
            if (state == Opportunity.STATE_OPENED || state == Opportunity.STATE_LAST_10_MIN) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return If the item has any opportunities left.
     */
    public boolean hasOpportunities() {
        return opportunities.size() > 0;
    }

    /**
     * @return The opportunity on this market with closest closing time.
     */
    protected Opportunity getClosestToClose() {
        Opportunity ctc = null;
        Opportunity o = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            if (null == ctc || o.getTimeEstClosing().compareTo(ctc.getTimeEstClosing()) < 0) {
                ctc = o;
            }
        }
        return ctc;
    }

    /**
     * @return Description why the opps should be disabled or empty string if
     *      the opss should not be disabled.
     */
    public String shouldDisableOpportunities() {
        if (disableAfterPeriod > 0 &&
                isSnapshotReceived() && // make sure we got one so we don't disable all on startup
                lastReutersUpdateReceiveTime != disabledAfterUpdateAt &&
                System.currentTimeMillis() - lastReutersUpdateReceiveTime >= disableAfterPeriod &&
                hasOpenedNotExpiringOpportunities()) {
            String str = "No updates received for market " + itemName +
            " for " + (System.currentTimeMillis() - lastReutersUpdateReceiveTime) + " milliseconds";
            log.warn(str);

            return str;
        }
        return "";
    }

    /**
     * Disable not settled opportunities on this market.
     *
     * @param desc description to put in the log records
     * @return <code>true</code> if any opps were disabled else <code>false</code>.
     */
    public boolean disableOpportunities(String desc) {
        if (!disabled) {
            if (log.isTraceEnabled()) {
                log.trace("Disable " + itemName + " desc: " + desc);
            }
            try {
                if (OpportunitiesManager.disableMarketOpportunities(marketId, desc) > 0) {
                    disabledAfterUpdateAt = lastReutersUpdateReceiveTime;
                    disabled = true;
                    MonitoringAlertMessage a = new MonitoringAlertMessage(marketId, true, desc);
                    LevelService.getInstance().sendMessage(a);
                    return true;
                }
            } catch (Throwable t) {
                log.error("Can't disable for " + itemName, t);
            }
        } else {
            if (log.isTraceEnabled()) {
                log.trace("Item " + itemName + " already disabled.");
            }
        }
        return false;
    }

    /**
     * @return <code>ture</code> if the opened opportunities of this item
     *      should be enabled else <code>false</code>.
     */
    public boolean shouldEnableOpportunities() {
    	if (log.isTraceEnabled()) {
    		log.trace("Item.shouldenableOpportunities for " + itemName + " - " +
    				" disableAfterPeriod: " + disableAfterPeriod +
    				" disabledAfterUpdateAt: " + disabledAfterUpdateAt +
    				" lastReutersUpdateReceiveTime: " + lastReutersUpdateReceiveTime +
    				" hasOpenedNotExpiringOpportunities: " + hasOpenedNotExpiringOpportunities() +
    				" isReutersStateOK: " + isReutersStateOK());
    	}
        if (disableAfterPeriod > 0 &&
                disabledAfterUpdateAt > 0 &&
                System.currentTimeMillis() - lastReutersUpdateReceiveTime < disableAfterPeriod &&
                hasOpenedNotExpiringOpportunities() &&
                isReutersStateOK()) {
            log.warn("Updates resumed for market " + itemName);

            return true;
        }
        return false;
    }

    /**
     * Enable opened opportunities on this market that were disabled by the service.
     *
     * @param desc description to put in the log records
     * @param forseEnable <code>true</code> if to execute db update no matter
     *      what the value of the internal flag is else <code>false</code>
     * @return <code>true</code> if any opps were enabled else <code>false</code>.
     */
    public boolean enableOpportunities(String desc, boolean forseEnable) {
        if (disabled || forseEnable) {
            if (log.isTraceEnabled()) {
                log.trace("Enable " + itemName + " desc: " + desc);
            }
            try {
                if (OpportunitiesManager.enableMarketOpportunities(marketId, desc) > 0) {
                    disabledAfterUpdateAt = 0;
                    disabled = false;
                    MonitoringAlertMessage a = new MonitoringAlertMessage(marketId, false, desc);
                    LevelService.getInstance().sendMessage(a);
                    return true;
                }
            } catch (Throwable t) {
                log.error("Can't enable for " + itemName, t);
            }
        } else {
            if (log.isTraceEnabled()) {
                log.trace("Item " + itemName + " already enabled.");
            }
        }
        return false;
    }

    public boolean isReutersStateOK() {
        if (!reutersStateOK) { // don't loop over the options if the state is not OK anyway
            return false;
        }
        for (Option o : options) {
            if (!o.isReutersStateOK()) {
                return false;
            }
        }
        return true;
    }

    public void setReutersStateOK(boolean stateOK) {
        if (reutersStateOK != stateOK) {
        	log.trace("Changing reutersStateOK on Item " + itemName + " from " + reutersStateOK + " to " + stateOK);
            reutersStateOK = stateOK;
            String desc = stateOK + " state from Reuters";
            if (!stateOK) {
                disableOpportunities(desc);
            } else {
                if (shouldEnableOpportunities()) {
                    enableOpportunities(desc, false);
                }
            }
            LevelService.getInstance().sendAlertEmail((stateOK ? "Disabled " : "Enabled ") + itemName, desc);
        }
    }

    /**
     * @return The home page priority of this item (market).
     */
    public int getHomePagePriority() {
        return homePagePriority;
    }

    /**
     * set The home page priority of this item (market).
     * @param Priority of this item
     */
    public void setHomePagePriority(int p) {
    	homePagePriority = p;
    }

    /**
     *
     * @param skin id
     * @return The home page priority of this item (market) by skin id.
     *
     */
    public Integer getHomePagePriorityById(int skinId) {
    	if (skinsPriorities.containsKey(skinId)){
    		return skinsPriorities.get(skinId).get("home_page_priority");
    	}
        return null;
    }

    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
    public abstract boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunities);

    /**
     * Notify the item that one of its options have received update.
     *
     * @param option the option that received update
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    public abstract boolean optionUpdated(Option option, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunities);

    /**
     * Handle investment notification. Here is where the exposure flow calculations are done
     * as the exposure can change after an investment.
     *
     * @param oppId the id of opp on which investment was made
     * @param invId the investment id
     * @param amount the investment amount
     * @param type the type of investment (1 - call/2 - put)
     * @return <code>true</code> if this investment caused shifting else <code>false</code>.
     */
    public boolean investmentNotification(long oppId, double invId, Double amount, long type, double aboveTotal, double belowTotal, long skinId) {
        boolean shifted = false;
        Opportunity opp = getOppById(oppId);
        Calendar nightStartTime = Calendar.getInstance();
        Calendar currentTime = Calendar.getInstance();
        Calendar nightEndTime = Calendar.getInstance();
        nightStartTime.set(Calendar.HOUR_OF_DAY, 22);
        nightStartTime.set(Calendar.MINUTE, 00);
        nightStartTime.set(Calendar.SECOND, 00);
        nightEndTime.set(Calendar.HOUR_OF_DAY, 06);
        nightEndTime.set(Calendar.MINUTE, 00);
        nightEndTime.set(Calendar.SECOND, 00);

        // first update the amounts
        if (type == InvestmentType.CALL) {
            opp.setCalls(opp.getCalls() + amount);
            if (amount > 0.0d /* Make sure this is not investment cancellation */) {
            	opp.setCallsCount(opp.getCallsCount() + 1);
            	if (skinId == Skin.SKIN_ETRADER) {
            		opp.setCallsCountET(opp.getCallsCountET() + 1);
            	}
            }
            opp.setCallsShift(opp.getCallsShift() + amount);
        } else {
            opp.setPuts(opp.getPuts() + amount);
            if (amount > 0.0d /* Make sure this is not investment cancellation */) {
            	opp.setPutsCount(opp.getPutsCount() + 1);
            	if (skinId == Skin.SKIN_ETRADER) {
            		opp.setPutsCountET(opp.getPutsCountET() + 1);
            	}
            }
            opp.setPutsShift(opp.getPutsShift() + amount);
        }

        if (opp.getOpportunityTypeId() == Opportunity.TYPE_OPTION_PLUS || nightStartTime.before(currentTime) || nightEndTime.after(currentTime)) {
            return false;
        }

        double exposure = opp.getCallsShift() - opp.getPutsShift();
        boolean shouldShift = false;
        double fafs = 0;
        if (null != fixedAmountForShifting) {
            fafs = fixedAmountForShifting.doubleValue();
        }
        double poai = 0;
        if (null != percentageOfAverageInvestments) {
            poai = percentageOfAverageInvestments.doubleValue();
        }
        double ai = 0;
        if (null != averageInvestments) {
            ai = averageInvestments.doubleValue();
        }
        if (typeOfShifting == Market.TYPE_OF_SHIFTING_FIXED_AMOUNT) {
            if (Math.abs(exposure) > fafs * 100) {
                shouldShift = true;
            }
        } else if (typeOfShifting == Market.TYPE_OF_SHIFTING_AVERAGE_INVESTMENT) {
            if (Math.abs(exposure) > poai * 100 * ai) {
                shouldShift = true;
            }
        } else {
            if (Math.abs(exposure) > Math.max(fafs * 100, poai * 100 * ai)) {
                shouldShift = true;
            }
        }
        if (log.isInfoEnabled()) {
            log.info("Investment notification -" +
                    " item: " + itemName +
                    " opportunity: " + oppId +
                    " exposure: " + exposure +
                    " fixedAmountForShifting: " + fixedAmountForShifting +
                    " percentageOfAverageInvestments: " + percentageOfAverageInvestments +
                    " averageInvestments: " + averageInvestments +
                    " typeOfShifting: " + typeOfShifting +
                    " shouldShift: " + shouldShift);
        }
        if (shouldShift) {
            if (!opp.isShifting()) { // first shifting
                opp.setLastShift(firstShiftParameter);
                if (exposure > 0) {
                    opp.setAutoShiftParameter(firstShiftParameter.doubleValue());
//                    opp.setShiftParameter((new BigDecimal(Double.toString(opp.getShiftParameter()))).add(firstShiftParameter).doubleValue());
                    opp.setShiftUp(true);
                } else {
                    opp.setAutoShiftParameter(-firstShiftParameter.doubleValue());
//                    opp.setShiftParameter((new BigDecimal(Double.toString(opp.getShiftParameter()))).subtract(firstShiftParameter).doubleValue());
                    opp.setShiftUp(false);
                }
                opp.setShifting(true);
            } else {
                if ((exposure > 0 && opp.isShiftUp()) ||
                        (exposure < 0 && !opp.isShiftUp())) { // shifting on the same direction
                    opp.setLastShift(opp.getLastShift().multiply(O8));
                } else {
                    opp.setLastShift(opp.getLastShift().multiply(O7));
                }
                if (exposure > 0) {
                    opp.setAutoShiftParameter((new BigDecimal(Double.toString(opp.getAutoShiftParameter()))).add(opp.getLastShift()).doubleValue());
                    opp.setShiftUp(true);
                } else {
                    opp.setAutoShiftParameter((new BigDecimal(Double.toString(opp.getAutoShiftParameter()))).subtract(opp.getLastShift()).doubleValue());
                    opp.setShiftUp(false);
                }
            }
            try {
                OpportunitiesManager.insertShifting(oppId, opp.getAutoShiftParameter(), opp.getLastShift().doubleValue(), exposure > 0, SkinGroup.AUTO);
            } catch (SQLException sqle) {
                log.error("Can't log shifting in the db.", sqle);
            }
            opp.setCallsShift(0);
            opp.setPutsShift(0);
            shifted = true;
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyyy HH:mm:ss");

            StringBuilder sb = new StringBuilder();
            sb.append("Market: ").append(itemName)
        		.append(" opportunity: ").append(oppId)
        		.append(" time: ").append(df.format(new Date(System.currentTimeMillis())))
        		.append(" current shift: ").append(opp.getLastShift())
        		.append(" total auto shift: ").append(new BigDecimal(Double.toString(opp.getAutoShiftParameter())));
            for (Map.Entry<SkinGroup, OpportunitySkinGroupMap> entry : opp.getSkinGroupMappings().entrySet()) {
				sb.append(" manual shift ")
					.append(entry.getKey())
					.append(": ")
					.append(new BigDecimal(Double.toString(entry.getValue().getShiftParameter())));
			}
            LevelService.getInstance().sendAlertEmail("Shifting " + itemName + " " + oppId, sb.toString());
        }
        // TODO: Tony: log processed
        if (log.isInfoEnabled()) {
            log.info("calls " + opp.getCalls() + " puts " + opp.getPuts() + " callsShift: " + opp.getCallsShift() + " putsShift " + opp.getPutsShift() + " shifted " + shifted);
        }
        return shifted;
    }


    /**
     * @return The formula result for this item.
     */
    protected double formula() {
        return lastFormulaResult;
    }

    /**
     * @param o the opportunity for which to calc the level
     * @return The long term formula result for this item.
     */
    protected abstract double longTermFormula(Opportunity o);

    /**
     * Check if the closing level for specified opportunity has been received.
     *
     * @param w the opportunity to check for if the closing level is ready
     * @return <code>true</code> if the closing level is ready to be taken else <code>false</code>
     */
    public abstract boolean isClosingLevelReceived(Opportunity w);

    /**
     * Get the closing level for the specified opportunity.
     *
     * @param w the opportunity for which to get closing level
     * @return The level as received from Reuters. No formula and exposure control calculations.
     */
    public abstract double getClosingLevel(Opportunity w);

    /**
     * @return <code>ArrayList<Opportunity></code> with the opps that should be closed
     *      (have the timeToClose flag on and have received closing level).
     */
    public ArrayList<Opportunity> getOppsToClose() {
        ArrayList<Opportunity> l = new ArrayList<Opportunity>();
        for (int i = 0; i < opportunities.size(); i++) {
            Opportunity o = opportunities.get(i);
            if (o.getState() != Opportunity.STATE_CLOSED && o.isTimeToClose() && isClosingLevelReceived(o)) {
                l.add(o);
            }
        }
        return l;
    }

    /**
     * Perform exposure control.
     *
     * @param o the opportunity to check
     * @param formulaResult the result of the formula calculation to pass through the exposure flow
     * @param skinGroup the skin group wich holds the corresponding shift parameter
     * @return The opportunity level.
     */
    protected double exposure(Opportunity o, double formulaResult, SkinGroup skinGroup) {
//        return (formulaResult + formulaResult * w.getShift()) * (1 + w.getShiftParameter());
        return formulaResult * (1 + o.getSkinGroupMappings().get(skinGroup).getShiftParameter() + o.getAutoShiftParameter());
    }

    /**
     * Prepare the <code>HashMap</code> with the values for the LS update.
     *
     * @param o
     * @param levelET the current opportunity level for ET
     * @param levelAO the current opportunity level for AO
     * @param forseDelete if to forse "DELETE" command or to determine the command by the state
     */
    private HashMap<String, String> getOpportunityLevelUpdate(Opportunity o, Map<SkinGroup, LevelsBean> levelsMap, boolean forseDelete) {
        HashMap<String, String> flds = new HashMap<String, String>();
      //  flds.put("ET_NAME", o.getMarket().getDisplayName());
        // send it by id and translate it to name with js
        flds.put("ET_NAME", String.valueOf(o.getMarketId()));

        flds.put("ET_ODDS", formatOdds(o.getOverOddsWin()));

        // use the time field for the suspended message or date
//        SimpleDateFormat df = new SimpleDateFormat("yyy-MM-dd HH:mm");
//        df.
        flds.put("ET_EST_CLOSE", getTimeEstCloseForLevelUpdate(o));
        flds.put("ET_LAST_INV", formatDate(o.getTimeLastInvest()));
//        flds.put("ET_EST_CLOSE", formatDate(o.getState() == Opportunity.STATE_CREATED ? o.getTimeFirstInvest() : o.getTimeEstClosing()));
        flds.put("ET_OPP_ID", String.valueOf(o.getId()));
        flds.put("ET_ODDS_WIN", formatFloat(1 + o.getOverOddsWin()));
        flds.put("ET_ODDS_LOSE", formatFloat(1 - o.getOddsType().getOverOddsLose()));
        flds.put("ET_ODDS_GROUP", o.getOddsGroup());
        flds.put("command", (forseDelete || o.getState() == Opportunity.STATE_DONE ? "DELETE" : "UPDATE"));
        flds.put("key", "opp" + o.getId());

        SkinGroup skinGroup = null;
        LevelsBean levelsBean = null;
        for (Map.Entry<SkinGroup, LevelsBean> entry : levelsMap.entrySet()) {
        	skinGroup = entry.getKey();
        	levelsBean = entry.getValue();
        	if (o.getTimeEstClosing().compareTo(new Date()) > 0 || o.getState() == Opportunity.STATE_CLOSED) {
        		flds.put(skinGroup.getLevelUpdateKey(), formatLevel(levelsBean.level, o.getMarket().getDecimalPoint().intValue()));
        	} else {
        		flds.put(skinGroup.getLevelUpdateKey(), "0");
        	}
        	flds.put(skinGroup.getColorUpdateKey(), levelsBean.level < o.getClosingLevel() ? "0" : levelsBean.level == o.getClosingLevel() ? "1" : "2");
        }

//        flds.put("ET_STATE", suspended ? String.valueOf(Opportunity.STATE_SUSPENDED) : String.valueOf(o.getState()));
        flds.put("ET_STATE", getOppStateForLevelUpdate(o));
        int priority = 0; //o.getMarket().getHomePagePriority();
        if (!o.isInOppenedState()) {
            priority += 2000; // the closed ones should show after the opened and suspended
        } else if (suspended) {
            priority += 1000; // the suspended ones should show after the opened
        }
        flds.put("ET_PRIORITY", String.valueOf(priority));
        flds.put("AO_OPP_STATE", o.isSuspended() ? "1" : "0");
        // The markets in group should be less than 100.
        // Add 100 for the opened and 200 for the suspended and
        // 300 for the closed.
        // This way priority should have 2 digits always and
        // opened will be before suspended and closed will come
        // last. So when conatenated with the close time and
        // sorted by this as string we will get the right order.
        int groupPriority = 0;//o.getMarket().getGroupPriority();
        if (o.isInOppenedState()) {
            if (!suspended) {
                groupPriority += 100;
            } else {
                groupPriority += 200;
            }
        } else {
            groupPriority += 300;
        }
        SimpleDateFormat sortDateFormat = new SimpleDateFormat("yyMMddHHmm");
        flds.put("ET_GROUP_PRIORITY", groupPriority + "_" + sortDateFormat.format(o.getTimeEstClosing()));

        flds.put("ET_GROUP_CLOSE", String.valueOf(getOpportunityGroupClose(o)));
        flds.put("ET_SCHEDULED", String.valueOf(o.getScheduled()));
        //fields for bugId: 2029
        flds.put("ET_RND_FLOOR", String.valueOf(new BigDecimal(Double.toString(randomFloor))));
        flds.put("ET_RND_CEILING", String.valueOf(new BigDecimal(Double.toString(randomCeiling))));
//        flds.put("ET_FREQUENCE_PRIORITY", String.valueOf(frequencePriority));
        //end bugId: 2029
        flds.put("ET_GROUP", String.valueOf(o.getMarket().getGroupId()));
        flds.put("ET_MARKET", String.valueOf(marketId));
        flds.put("ET_SUSPENDED_MESSAGE",suspendedMessage);
        //mark which scheduled should be available in filter: 4 all, 3 weekly/daily/hourly, 2 daily/hourly, 1 hourly, 0 close
        flds.put("FILTER_SCHEDULED", String.valueOf(getFilterScheduled()));
        flds.put("FILTER_NEW_SCHEDULED", getNewFilterScheduled());
        
        // AO priorities
//        for (Iterator<Integer> i = skinsPriorities.keySet().iterator(); i.hasNext();) {
//            Integer sId = i.next();
//            flds.put("AO_PRIORITY_SKIN_" + sId, String.valueOf(getHomePagePriorityById(sId)));
//        }

        // AO markets on homepage
        ArrayList<Skins> l = LevelService.getInstance().getSkins();
        for (int i = 0; i < l.size(); i++) {
            flds.put("AO_HP_MARKETS_" + l.get(i).getId(), l.get(i).getCurrentMarketsOnHomePage());
        }

        // AO markets states
        flds.put("AO_STATES", LevelService.getInstance().getOpenedMarkets());

        flds.put("AO_TS", String.valueOf(System.currentTimeMillis()));
        int aoFlags = 0;
        if (hasHour()) {
            aoFlags |= 1;
        }
        if (o.isLessThanHourToClosing()) {
            aoFlags |= 2;

            if (o.getOpportunityTypeId() == Opportunity.TYPE_REGULAR) {
        		if ((o.getCallsCount() - o.getCallsCountET()) + (o.getPutsCount() - o.getPutsCountET()) > 0 /* avoid division by zero */) {
	            	/* Show trends during the profit line. */
	            	flds.put("AO_CALLS_TREND", o.getCallsCountRate(Skin.SKIN_BUSINESS_ANYOPTION));
        		} else {
        			// if there were no investments made on the asset the distribution will be 50%/50%
        			flds.put("AO_CALLS_TREND", "0.5");
        		}
        		if (o.getCallsCountET() + o.getPutsCountET() > 0 /* avoid division by zero */) {
	            	/* Show trends during the profit line. */
	            	flds.put("ET_CALLS_TREND", o.getCallsCountRate(Skin.SKIN_BUSINESS_ETRADER));
        		} else {
        			// if there were no investments made on the asset the distribution will be 50%/50%
        			flds.put("ET_CALLS_TREND", "0.5");
        		}
            }
        }
        flds.put("AO_FLAGS", String.valueOf(aoFlags));

        if (!flds.containsKey("AO_CALLS_TREND")) {
        	flds.put("AO_CALLS_TREND", "0.0");
        }
        if (!flds.containsKey("ET_CALLS_TREND")) {
        	flds.put("ET_CALLS_TREND", "0.0");
        }

        flds.put("AO_TYPE", String.valueOf(o.getOpportunityTypeId()));
        flds.put("ET_NH", getNextOpenHourly());
        
        Map<String, String> quoFlds = getQuoteFields();
        if (null != quoFlds) {
        	flds.putAll(quoFlds);
        }

        return flds;
    }

    /**
     * For the Bubbles project we are requested to expose some of the fields we receive from Reuters as is only for our partner
     * internal use. For this purpose this method should collect the needed fields for this specific market and return them ready
     * for the getOpportunityLevelUpdate to include them directly in the Lightstreamer update.
     * 
     * @return Map of fields ready to be added to Lightstreamer update. <code>null</code> if this market should not quote any fields.
     */
    protected Map<String, String> getQuoteFields() {
    	return null;
    }
    
    /**
     * Returns an integer that represents a bitmask which indicates the closing fileter flags of an opportunity.
     *
     * @param o
     * @return a bitmask comprised of {@link Opportunity#CLOSING_FILTER_FLAG_CLOSEST}
     * 									, {@link Opportunity#CLOSING_FILTER_FLAG_MONTH}
     * 									, {@link Opportunity#CLOSING_FILTER_FLAG_TODAY}
     * 									and {@link Opportunity#CLOSING_FILTER_FLAG_WEEK}
     */
    public int getOpportunityGroupClose(Opportunity o) {
    	int groupClose = 0;
//      if (!o.isInOppenedState() || suspended) {
//          groupClose = 0;
//      } else if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
//          groupClose = 1;
//      } else if (o.getScheduled() == Opportunity.SCHEDULED_DAYLY) {
//          groupClose = 2;
//      } else {
//          Calendar cls = Calendar.getInstance();
//          cls.setTime(o.getTimeEstClosing());
//          Calendar now = Calendar.getInstance();
//          // if closing today then it is 2
//          if (cls.get(Calendar.DAY_OF_YEAR) == now.get(Calendar.DAY_OF_YEAR)) {
//              groupClose = 2;
//          } else {
//              groupClose = 3;
//          }
//      }
      if ((o.isInOppenedState() || o.getState() == Opportunity.STATE_DONE) && !suspended) {
          Calendar cls = Calendar.getInstance();
          cls.setTime(o.getTimeEstClosing());
          Calendar now = Calendar.getInstance();
          if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY ||
                  o == getClosestToClose()) {
              groupClose |= Opportunity.CLOSING_FILTER_FLAG_CLOSEST;
          }
          if (log.isTraceEnabled()) {
              log.trace("cls.get(Calendar.DAY_OF_YEAR): " + cls.get(Calendar.DAY_OF_YEAR) + " now.get(Calendar.DAY_OF_YEAR): " + now.get(Calendar.DAY_OF_YEAR));
          }
          if (o.getScheduled() != Opportunity.SCHEDULED_HOURLY &&
                  (o.getScheduled() == Opportunity.SCHEDULED_DAYLY ||
                  cls.get(Calendar.DAY_OF_YEAR) == now.get(Calendar.DAY_OF_YEAR))) {
              groupClose |= Opportunity.CLOSING_FILTER_FLAG_TODAY;
          }
          if (o.getScheduled() == Opportunity.SCHEDULED_WEEKLY ||
                  (o.getScheduled() == Opportunity.SCHEDULED_MONTHLY && !hasOpenedWeekly())) {
              groupClose |= Opportunity.CLOSING_FILTER_FLAG_WEEK;
          }
          if (o.getScheduled() == Opportunity.SCHEDULED_MONTHLY) {
              groupClose |= Opportunity.CLOSING_FILTER_FLAG_MONTH;
          }
      } else if (o.getState() == Opportunity.STATE_WAITING_TO_PAUSE) {
          if (!hasOpened(Opportunity.SCHEDULED_DAYLY)) {
              groupClose |= Opportunity.CLOSING_FILTER_FLAG_TODAY;
          }
          if (o.getScheduled() == Opportunity.SCHEDULED_WEEKLY || !hasWeeklyWaitingToPause()) {
              groupClose |= Opportunity.CLOSING_FILTER_FLAG_WEEK;
          }
          if (o.getScheduled() == Opportunity.SCHEDULED_MONTHLY) {
              groupClose |= Opportunity.CLOSING_FILTER_FLAG_MONTH;
          }
      } else {
          groupClose |= Opportunity.CLOSING_FILTER_FLAG_TODAY;
          if (hasLongTerm()) {
              groupClose |= Opportunity.CLOSING_FILTER_FLAG_WEEK;
              groupClose |= Opportunity.CLOSING_FILTER_FLAG_MONTH;
          }
      }

      return groupClose;
    }

    public String getTimeEstCloseForLevelUpdate(Opportunity o) {
    	Date dts = null;
        if (o.isInOppenedState() && isSnapshotReceived()) {
            dts = o.getTimeEstClosing();
        } else if (o.getState() == Opportunity.STATE_CREATED || (o.isInOppenedState() && !isSnapshotReceived())) {
            dts = o.getTimeFirstInvest();
        } else { // only PAUSED and WAITING_TO_PAUSE left
            dts = o.getTimeNextOpen();
            // TimeNextOpen is not null only on the specific case before week/month last working day
            // when the long term opp will open first on the last working day.
            // This is because we don't have day opp on the last working day of the week/month.
            if (null == dts) {
                if (o.getState() == Opportunity.STATE_WAITING_TO_PAUSE) {
                    // give it one more try. in case the service has just started
                    Opportunity tmon = getTimeMarketOpensNext();
                    if (log.isDebugEnabled()) {
                        log.debug("TimeMarketOpensNext: " + (null != tmon ? tmon.getTimeNextOpen() : "none"));
                    }
                    if (null != tmon) {
                        o.setTimeNextOpen(tmon.getTimeNextOpen());
                        dts = tmon.getTimeNextOpen();
                    } else {
                        dts = o.getTimeEstClosing();
                    }
                } else {
                    dts = o.getTimeEstClosing();
                }
            }
        }
        return formatDate(dts);
    }

    public String getOppStateForLevelUpdate(Opportunity o) {
    	int stt = o.getState();
        if (suspended) {
            stt = Opportunity.STATE_SUSPENDED;
        } else if (!isSnapshotReceived()) {
            stt = Opportunity.STATE_CREATED;
        } else if (stt == Opportunity.STATE_CLOSING &&
                    ((o.getTimeLastInvest().getTime() +  15 * 60 * 1000) <= System.currentTimeMillis()) &&
                    o.getMarketId() == Market.MARKET_TEL_AVIV_25_MAOF_ID) {
            stt = Opportunity.STATE_HIDDEN_WAITING_TO_EXPIRY;
        }
        return String.valueOf(stt);
    }

    /**
     * Handle sending of update. Send the update only if needed. If the update pause passed
     * or the update is significant.
     *
     * @param sender
     * @param lifeId
     * @param haveCurrentLevelUpdate if this call is passing current level to be sent
     * @param currentCalc the current level as calculated before pass thorugh random
     * @param currentLevel the current level after passing through the random
     * @param haveRealLevelUpdate if this call is passing real level to be sent
     * @param realLevelUpdate the real level to send
     */
    protected void haveUpdate(
            SenderThread sender,
            long lifeId,
            boolean haveCurrentLevelUpdate,
            double currentCalc,
            double currentLevel,
            boolean haveRealLevelUpdate,
            double realLevelUpdate) {
        boolean sendCurrent = false;
        if (lastCalcResult > 0 && currentCalc > 0) {
        	sendLevelChangeAlert(currentCalc);
        }
        if (haveCurrentLevelUpdate) {
//            boolean cs = currentCalc * 100 / lastCalcResult > significantPercentage.doubleValue();
            boolean cs = Math.abs(1 - currentCalc / lastCalcResult) > significantPercentage.doubleValue();
            boolean tp = System.currentTimeMillis() - lastUpdateTime > updatesPause;
            if (cs || tp) {
                if (log.isTraceEnabled()) {
                    log.trace(itemName + " Current page siginificant: " + cs + " time elapsed: " + tp);
                }
                sendCurrent = true;
                lastUpdateTime = System.currentTimeMillis();
                lastCalcResult = currentCalc;
                lastFormulaResult = currentLevel;
            }
        }
        boolean sendReal = false;
        if (haveRealLevelUpdate) {
//            boolean rs = realLevelUpdate * 100 / lastRealLevel > realSignificantPercentage.doubleValue();
            boolean rs = Math.abs(1 - realLevelUpdate / lastRealLevel) > realSignificantPercentage.doubleValue();
            boolean tp = System.currentTimeMillis() - lastRealUpdateTime > realUpdatesPause;
            if (rs || tp) {
                if (log.isTraceEnabled()) {
                    log.trace(itemName + " Current real siginificant: " + rs + " time elapsed: " + tp);
                }
                sendReal = true;
                lastRealUpdateTime = System.currentTimeMillis();
                lastRealLevel = realLevelUpdate;
            }
        }
        if (sendCurrent || sendReal) {
            if (log.isTraceEnabled()) {
                log.trace("sendCurrent: " + sendCurrent + " sendReal: " + sendReal);
            }
            sendUpdate(sender, lifeId, UPDATE_ALL_OPPS, sendCurrent, false);
        }
        log.debug(
                (sendCurrent || sendReal ? "SENDING " : "DISCARDING ") +
                " TRADINGDATA_" + itemName +
                " currentLevel: " + currentLevel +
                " lastFormulaResult: " + lastFormulaResult +
                " realLevelUpdate: " + realLevelUpdate +
                " lastRealLevel: " + lastRealLevel +
                " currentCalc: " + currentCalc +
                " lastCalcResult: " + lastCalcResult);
    }

    /**
     * Send update message for all item opps.
     *
     * @param sender
     * @param lifeId
     * @param toUpdate to which opps to send update all, todays or long term
     * @param sendCurrentLevel
     * @param suspend <code>true</code> to make suspend update (delete all but the special entry) else <code>false</code>
     */
    public void sendUpdate(SenderThread sender, long lifeId, int toUpdate, boolean sendCurrentLevel, boolean suspend) {
        if (hasOpenedOpportunities() && isSnapshotReceived()) {
            Opportunity o = null;
            for (int i = 0; i < opportunities.size(); i++) {
                o = opportunities.get(i);
                if (toUpdate == UPDATE_ALL_OPPS ||
                        (toUpdate == UPDATE_TODAY_OPPS && (o.getScheduled() == Opportunity.SCHEDULED_HOURLY || o.getScheduled() == Opportunity.SCHEDULED_DAYLY)) ||
                        (toUpdate == UPDATE_LONG_TERM_OPPS && (o.getScheduled() == Opportunity.SCHEDULED_WEEKLY || o.getScheduled() == Opportunity.SCHEDULED_MONTHLY))) {
                    if ((o.isInOppenedState() || o.getState() == Opportunity.STATE_WAITING_TO_PAUSE) && !(o.getState() == Opportunity.STATE_CLOSED) && isSnapshotReceived(o)) {
                        sendUpdate(sender, lifeId, o, suspend && o != specialEntry && !optionPlusMarket, sendCurrentLevel);
                        // TODO: Tony: send only 1 message for real level change (one per market not one per opp)
                    }
                }
            }
        } else {
        	if (null != specialEntry) {
        		sendUpdate(sender, lifeId, specialEntry, false, sendCurrentLevel);
        	} else {
        		log.warn("Closed market without special entry - " + itemName);
        	}
        }
    }

    public void getOpenedOpportunityLevels(Opportunity o, ETraderFeedMessage toSend, SkinGroup skinGroup) {
//        toSend.devCheckLevel = exposure(o, lastCalcResult);
        toSend.realLevel = lastRealLevel;
        LevelsBean b = getOpportunityLevels(o, skinGroup);
        toSend.level = b.level;
        toSend.devCheckLevel = b.devCheckLevel;
    }

    /**
     * Send update for item opp.
     *
     * @param sender
     * @param lifeId
     * @param o
     * @param forseDelete if to forse "DELETE" command or to determine the command by the state
     * @param sendCurrentLevel if to send current level update in this message (LS update) or just the real level
     */
    public void sendUpdate(SenderThread sender, long lifeId, Opportunity o, boolean forseDelete, boolean sendCurrentLevel) {
        Map<SkinGroup, LevelsBean> levelsMap = new EnumMap<SkinGroup, Item.LevelsBean>(SkinGroup.class);
        LevelsBean levelsBean = null;
        for (Map.Entry<SkinGroup, OpportunitySkinGroupMap> entry : o.getSkinGroupMappings().entrySet()) {
			// DELETE commands don't need level
	        // also don't let missing level to stop sending DELETE command (eg when market is opened and don't have snapshot yet)
	        if (sendCurrentLevel && !forseDelete) {
	        	levelsBean = getOpportunityLevels(o, entry.getKey());
	        } else {
	        	levelsBean = new LevelsBean();
				levelsBean.level = 0;
				levelsBean.devCheckLevel = lastCalcResult;
	        }
	        levelsMap.put(entry.getKey(), levelsBean);
		}

        //prepare the object to send through JMS
        HashMap<Integer, Boolean> tmpHomepage = new HashMap<Integer, Boolean>(); 
        tmpHomepage.putAll(onHomePage);
        if (o != specialEntry) { //maket all the keys false so its will not go to home page
        	int key;
        	for (Iterator<Integer> i = tmpHomepage.keySet().iterator(); i.hasNext();) {
                key = i.next();
                tmpHomepage.put(key, false);
        	}
        }
        ETraderFeedMessage toSend = new ETraderFeedMessage(
                //o == specialEntry?onHomePage:new HashMap<Integer, Boolean>(), //only if its specialEntry send the real hashmap else send new 1 its will act like all have false on home page
        		tmpHomepage,
        		sendCurrentLevel && (forseDelete || o == specialEntry || !suspended || optionPlusMarket) ? getOpportunityLevelUpdate(o, levelsMap, forseDelete) : null,
                lifeId,
                levelsMap.get(SkinGroup.ETRADER).level, //use by insurance adapter (GM/RF)
                o.getMarket().getGroupId(),
                o.getId(),
                lastRealLevel,
                levelsMap.get(SkinGroup.ETRADER).devCheckLevel, //not in use
                skinsPriorities,
                o.getAutoShiftParameter(),
        		o.getCalls(),
        		o.getPuts(),
        		itemName,
        		decimalPoint,
        		o.isDisabledTrader(),
        		disabled,
        		Utils.roundDouble(o.getContractsBought(), 2),
        		Utils.roundDouble(o.getContractsSold(), 2),
        		o.getQuoteParamsObject(),
         		0,//bid not in use for updates only for devation check
         		0,//offer not in use for updates only for devation check
         		null,
         		!o.isClosingLevelReceived(),
         		o.getSkinGroupMappings()
        );
        sender.send(toSend);
    }

    private LevelsBean getOpportunityLevels(Opportunity o, SkinGroup skinGroup) {
        LevelsBean b = new LevelsBean();
        b.level = 0;
        b.devCheckLevel = exposure(o, lastCalcResult, skinGroup);
        if (o.getState() == Opportunity.STATE_CLOSED) {
            b.level = getClosingLevel(o);
        } else if (o.isInOppenedState() && !suspended) {
            if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY || o.getScheduled() == Opportunity.SCHEDULED_DAYLY) {
                if (isSnapshotReceived()) {
                    if (o.getState() == Opportunity.STATE_CLOSING_1_MIN || o.getState() == Opportunity.STATE_CLOSING) {
                        b.level = formulaRandomChange(lastRealLevel);
                    } else {
                        b.level = exposure(o, formula(), skinGroup);
                    }
                }
            } else {
                if (isLongTermSnapshotReceived()) {
                    if (o.getState() == Opportunity.STATE_CLOSING_1_MIN || o.getState() == Opportunity.STATE_CLOSING) {
                        b.level = formulaRandomChange(lastRealLevel);
                    } else {
                        b.devCheckLevel = longTermFormula(o);
                        b.level = formulaRandomChange(b.devCheckLevel, lastRandomChange);
                        b.devCheckLevel = exposure(o, b.devCheckLevel, skinGroup);
                        b.level = exposure(o, b.level, skinGroup);
                        if (log.isTraceEnabled()) {
                            log.trace("Long term formula for: " + o.getId() + " ltf: " + b.devCheckLevel + " level: " + b.level);
                        }
                    }
                }
            }
        } else { // Opportunity.STATE_CREATED
            b.level = o.getClosingLevel(); // last closing level
        }
        return b;
    }

    /**
     * Format the result of the exposure calc (the level) with the number of
     * decimal digits for this item.
     *
     * @param exposureResult
     * @return
     */
    public String formatLevel(double exposureResult, int decimalPoint) {
        String rez = null;
        switch (decimalPoint) {
        case 1:
            DecimalFormat numberFormat1 = new DecimalFormat("#,##0.0");
            rez = numberFormat1.format(exposureResult);
            break;
        case 2:
            DecimalFormat numberFormat2 = new DecimalFormat("#,##0.00");
            rez = numberFormat2.format(exposureResult);
            break;
        case 3:
            DecimalFormat numberFormat3 = new DecimalFormat("#,##0.000");
            rez = numberFormat3.format(exposureResult);
            break;
        case 4:
            DecimalFormat numberFormat4 = new DecimalFormat("#,##0.0000");
            rez = numberFormat4.format(exposureResult);
            break;
        case 5:
            DecimalFormat numberFormat5 = new DecimalFormat("#,##0.00000");
            rez = numberFormat5.format(exposureResult);
            break;
        default:
            rez = String.valueOf(exposureResult);
            break;
        }
        return rez;
    }

    /**
     * Randomly modify the level with % in specified interval. For example
     * if you want to increase the level with 0.01% to 0.07% call with
     * level, 0.0001 and 0.0007.
     *
     * @param level the level to change
     * @return
     */
    protected double formulaRandomChange(double level) {
        // randomly choose a value in the spec interval to change the level with
        double rnd = random.nextDouble() * (randomCeiling - randomFloor) + randomFloor;
        lastRandomChange = rnd;
        return new Double(level * (1 + rnd));
    }

    /**
     * Randomly modify the level with % in specified interval. For example
     * if you want to increase the level with 0.01% to 0.07% call with
     * level, 0.0001 and 0.0007.
     *
     * @param level the level to change
     * @param floor
     * @param ceiling
     * @return
     */
    protected double formulaRandomChange(double level, double floor, double ceiling) {
        // randomly choose a value in the spec interval to change the level with
        double rnd = random.nextDouble() * (ceiling - floor) + floor;
        return new Double(level * (1 + rnd));
    }

    /**
     * Modify the level with specified change.
     *
     * @param level the level to change
     * @param rnd the change
     * @return
     */
    protected double formulaRandomChange(double level, double rnd) {
        return new Double(level * (1 + rnd));
    }

    /**
     * Used to format float values with 2 decimal digits.
     *
     * @param val the val to format
     * @return
     */
    protected static String formatFloat(float val) {
        DecimalFormat numberFormat2 = new DecimalFormat("#,##0.00");
        return numberFormat2.format(val);
    }

    /**
     * Used to format the est close time (format MM/dd/yy HH:mm) gmt 0.
     *
     * @param val the time to format
     * @return
     */
    protected static String formatDate(Date val) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        return dateFormat.format(val);
    }

    /**
     * Used to format the odds as % (format #0%).
     *
     * @param val the val to format
     * @return
     */
    protected static String formatOdds(float val) {
        DecimalFormat percentFormat = new DecimalFormat("#0%");
        return percentFormat.format(val);
    }

    /**
     * Round a double value to have "decimals" number of digits after
     * the decimal point.
     * For example roundDouble(1.12345, 3) = 1.123
     *
     * @param val
     * @param decimals
     * @return
     */
    protected static double roundDouble(double val, long decimals) {
        BigDecimal bd = new BigDecimal(Double.toString(val));
        MathContext mc = new MathContext(bd.precision() - bd.scale() + (int) decimals, RoundingMode.HALF_UP);
        return bd.round(mc).doubleValue();
    }

    /**
     * Calculate how many days left to expire.
     *
     * @param expireDate the expire date in format "dd MMM yyyy" in enUS locale
     * @return
     */
    public static long getDaysToExpire(String expireDate) throws ParseException {
        SimpleDateFormat updateDateFormat = new SimpleDateFormat("dd MMM yyyy", new Locale("en", "US"));
        Date expire = updateDateFormat.parse(expireDate);
        return getDaysToExpire(expire);
    }

    /**
     * Calculate how many days left to expire.
     *
     * @param expire the expire date
     * @return
     */
    public static long getDaysToExpire(Date expire) {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);

        Calendar e = Calendar.getInstance();
        e.setTime(expire);
        e.set(Calendar.HOUR_OF_DAY, 0);
        e.set(Calendar.MINUTE, 0);
        e.set(Calendar.SECOND, 0);
        e.set(Calendar.MILLISECOND, 0);

        // NOTE: take the difference as double and round it so this way we will
        // drop the 1 hour difference in the day length when change to/from
        // day light saving time
        double dif = e.getTimeInMillis() - today.getTimeInMillis();
        return Math.round(dif / (1000 * 60 * 60 * 24)); // millis in day
    }

    /**
     * Set the option subscribtion manager to use for subscribe/unsubscribe options.
     *
     * @param optionSubscribtionManager
     */
    public void setOptionSubscribtionManager(OptionSubscribtionManager optionSubscribtionManager) {
        this.optionSubscribtionManager = optionSubscribtionManager;
    }

    /**
     * Tell the item to open (subscribe) all its options (when you want to start using it).
     *
     * @param itm the Item instance to receive the updates for the options.
     */
    public void subscribeOptions(Item itm) {
        Option o = null;
        for (Iterator<Option> i = options.iterator(); i.hasNext();) {
            o = i.next();
            if (null == o.getHandle()) {
                optionSubscribtionManager.subscribeOption(itm, o);
            }
        }
    }

    /**
     * Ask this item to close (unsubscribe) all its options (when you want to close/unsubscribe it).
     *
     * @param itm the Item whose options should be unsubscribed
     */
    public void unsubscribeOptions(Item itm) {
        clearOptions(itm);
    }

    /**
     * Find option with specified name from the options.
     *
     * @param optionName the name of the option to look for
     * @return The <code>Option</code> with the specified name or <code>null</code> if not found.
     */
    protected Option getOption(String optionName) {
        Option o = null;
        for (Iterator<Option> i = options.iterator(); i.hasNext();) {
            o = i.next();
            if (o.getOptionName().equals(optionName)) {
                return o;
            }
        }
        return null;
    }

    /**
     * Clear the options list (unsibscribe each option it remove).
     * Make sure you have lock over the options when you call this metod.
     *
     * @param itm the Item instance to be removed from the options.
     */
    protected void clearOptions(Item itm) {
        Option o = null;
        for (Iterator<Option> i = options.iterator(); i.hasNext();) {
            o = i.next();
            optionSubscribtionManager.unsubscribeOption(itm, o);
            i.remove();
        }
    }

    /**
     * Unsubscribe from old options and subscribe for new. Unsubscribe from
     * the same number of options as the new passed. If the new options strike
     * is higher than the old remove the lower options else the higher.
     * Make sure you have lock over the options when you call this metod.
     *
     * @param newOptions
     */
    protected void resubscribe(Option[] newOptions) {
        if (log.isTraceEnabled()) {
            log.trace("before options size = " + options.size() + " newOptions length = " + newOptions.length);
        }
        boolean up = false;
        if (newOptions.length > 0 && options.size() > 0) {
            up = newOptions[0].getStrike() > options.get(0).getStrike();
        }

        Option o = null;
        for (int i = 0; i < newOptions.length && options.size() > 0; i++) {
            o = options.get(up ? 0 : options.size() - 1);
            optionSubscribtionManager.unsubscribeOption(this, o);
            options.remove(o);
        }

        for (int i = 0; i < newOptions.length; i++) {
            o = newOptions[i];
            optionSubscribtionManager.subscribeOption(this, o);
            if (up) {
                options.add(o);
            } else {
                options.add(i, o);
            }
        }
        if (log.isTraceEnabled()) {
            log.trace("after options size = " + options.size() + " newOptions length = " + newOptions.length);
        }
    }

    /**
     * Check if all options have received snapshots and level can be calculated.
     * Call this metod only when you have lock on the options.
     *
     * @return <code>true</code> if level can be claculated else <code>false</code>.
     */
    protected boolean canCalcLevel() {
        if (options.size() == 0) { // if no options you can't calc
            log.trace("options.size() = 0");
            return false;
        }
        for (int i = 0; i < options.size(); i++) {
            if (!options.get(i).isSnapshotReceived()) {
                log.trace("option " + options.get(i).getOptionName() + " no snapshot");
                return false; // if options have no value you can't calc
            }
        }
        return true;
    }

    /**
     * Get the CALL options letter for this/next month.
     * The G is for July, Aug will be H, Sep will be I... January Call start with "A".
     *
     * @param nextMonth <code>true</code> if to return the next month letter or <code>false</code> for this month letter
     * @return The current letter for the call option name.
     */
    protected static char getCallLetter(boolean nextMonth) {
        return getMonthLetter('A', nextMonth);
    }

    /**
     * Get the PUT options letter for this/next month.
     * The S is for Jul Aug will be T... January Putt start with "M".
     *
     * @param nextMonth <code>true</code> if to return the next month letter or <code>false</code> for this month letter
     * @return The current letter for the put option name.
     */
    protected static char getPutLetter(boolean nextMonth) {
        return getMonthLetter('M', nextMonth);
    }

    /**
     * Helper method for the call/put month letter methods.
     *
     * @param startLetter the start letter ('A' or 'M')
     * @param nextMonth <code>true</code> if to return the next month letter or <code>false</code> for this month letter
     * @return The requested month letter.
     */
    private static char getMonthLetter(char startLetter, boolean nextMonth) {
        Calendar calendar = Calendar.getInstance();
        int m = calendar.get(Calendar.MONTH);
        if (nextMonth) {
            m++;
            if (m > Calendar.DECEMBER) {
                m = Calendar.JANUARY;
            }
        }
        return (char) (startLetter + m); // NOTE: m is between 0 and 11 as Calendar months are 0 to 11
    }

    /**
     * Get the future quater letter.
     *
     * @param nextMonth <code>true</code> if to return the next month letter or <code>false</code> for this month letter
     * @return The requested mont future quater letter.
     */
    protected static char getFutureQuarterLetter(boolean nextMonth) {
        Calendar calendar = Calendar.getInstance();
        int m = calendar.get(Calendar.MONTH);
        if (nextMonth) {
            m++;
            if (m > Calendar.DECEMBER) {
                m = Calendar.JANUARY;
            }
        }
        int q = m / 3; // get the quater
        switch (q) {
        case 0:
            return 'H';
        case 1:
            return 'M';
        case 2:
            return 'U';
        default:
            return 'Z';
        }
    }

    /**
     * Get the future year digit.
     *
     * @param monthsToAdd number of months to add to the current one
     * @return The requested month future year digit.
     */
    protected static char getFutureYearDigit(int monthsToAdd) {
        Calendar calendar = Calendar.getInstance();
        if (monthsToAdd > 0) {
            calendar.add(Calendar.MONTH, monthsToAdd);
        }
        String y = String.valueOf(calendar.get(Calendar.YEAR));
        return y.charAt(y.length() - 1);
    }

    /**
     * Get future month letter.
     *
     * @param monthsToAdd number of months to add to the current one
     * @return The requested month future letter.
     */
    protected static char getFutureMonthLetter(int monthsToAdd) {
        Calendar calendar = Calendar.getInstance();
        if (monthsToAdd > 0) {
            calendar.add(Calendar.MONTH, monthsToAdd);
        }
        int m = calendar.get(Calendar.MONTH);
        switch (m) {
        case Calendar.JANUARY:
            return 'F';
        case Calendar.FEBRUARY:
            return 'G';
        case Calendar.MARCH:
            return 'H';
        case Calendar.APRIL:
            return 'J';
        case Calendar.MAY:
            return 'K';
        case Calendar.JUNE:
            return 'M';
        case Calendar.JULY:
            return 'N';
        case Calendar.AUGUST:
            return 'Q';
        case Calendar.SEPTEMBER:
            return 'U';
        case Calendar.OCTOBER:
            return 'V';
        case Calendar.NOVEMBER:
            return 'X';
        default:
            return 'Z';
        }
    }

    /**
     * Check if the last MON/TUE/WED/THU/FRI/SAT/SUN of the month came and it is not the last day of the month.
     *
     * @param day the day we want to check for. pass <code>Calendar</code> constant. eg <code>Calendar.MONDAY</code>
     * @return <code>true</code> if the day of interest already came (it is today or passed) and it is not
     *      the last day of the month else <code>false</code>.
     */
    protected static boolean lastDayOfMonthCame(int day) {
        Calendar today = Calendar.getInstance();

        Calendar cal = Calendar.getInstance();
        // go to the 1st day of next month
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.add(Calendar.MONTH, 1);

        boolean lastDayOfMonth = true;
        do {
            // go back day by day. the first step should get us to the last day of our month
            cal.add(Calendar.DAY_OF_MONTH, -1);
            if (cal.get(Calendar.DAY_OF_WEEK) == day) { // we got the day we look for
                if (cal.get(Calendar.DAY_OF_MONTH) == today.get(Calendar.DAY_OF_MONTH)) { // it is today
                    return !lastDayOfMonth;
                } else {
                    return false;
                }
            }
            lastDayOfMonth = false;
        } while (cal.get(Calendar.DAY_OF_MONTH) != today.get(Calendar.DAY_OF_MONTH));
        // we reached today before we found the day we look for so it already passed
        return true;
    }

    /**
     * Handle update for the interest option. If the update is for this interest
     * option update the "EXPIR_DATE", "ASK" and "BID" fields.
     *
     * @param itemName the name of the item to update
     * @param interest the interest to try to update
     * @param fields the update fields
     * @return <code>ture</code> if the update is for the interest option else <code>false</code>.
     */
    protected boolean handleInterest(String itemName, Option interest, HashMap<String, Object> fields) {
        if (itemName.equals(interest.getOptionName())) {
            // the first update is normally the snapshot
            interest.setSnapshotReceived(true);
            if (null != fields.get("EXPIR_DATE")) {
                interest.setExpireDate((String) fields.get("EXPIR_DATE"));
            }
            if (null != fields.get("ASK")) {
                interest.setAsk((Double) fields.get("ASK"));
            }
            if (null != fields.get("BID")) {
                interest.setBid((Double) fields.get("BID"));
            }
            return true;
        }
        return false;
    }

    /**
     * Check if specified date is today.
     *
     * @param date the date to check in format "dd MMM yyyy"
     * @return <code>true</code> if the passed date is today else <code>false</code> (even if can't parse).
     */
    protected static boolean isToday(String date, String timeZone) {
        try {
            SimpleDateFormat updateDateFormat = new SimpleDateFormat("dd MMM yyyy", new Locale("en", "US"));
            updateDateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
        	Date d = updateDateFormat.parse(date);
        	return Utils.isToday(d, timeZone);
        } catch (ParseException pe) {
            log.error("Can't parse the date.", pe);

        }
        return false;
    }

    /**
     * Check if specified time is today.
     *
     * @param time the time to check in format "dd MMM yyyy HH:mm:ss"
     * @return <code>true</code> if the passed date is today else <code>false</code> (even if can't parse).
     */
    protected static boolean isTodayTime(String time, String timeZone) {
        try {
            SimpleDateFormat updateDateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm:ss", new Locale("en", "US"));
            updateDateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
            Date d = updateDateFormat.parse(time);
            if (log.isTraceEnabled()) {
                log.trace("isTodayTime - time: " + time + " timeZone: " + timeZone + " d: " + d);
            }
            return Utils.isToday(d, timeZone);
        } catch (ParseException pe) {
            log.error("Can't parse the date.", pe);
        }
        return false;
    }

    /**
     * Reset (zero) the shiftings of the opportunities.
     */
    public void resetOpportunitiesShiftings() {
        Opportunity o = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            o.setCallsShift(0);
            o.setPutsShift(0);
            o.setShifting(false);

            for (Map.Entry<SkinGroup, OpportunitySkinGroupMap> entry : o.getSkinGroupMappings().entrySet()) {
            	OpportunitySkinGroupMap mapping = entry.getValue();
				if (mapping.getShiftParameter() != 0d) {
					try {
						// TODO call DB procedure
	                    OpportunitiesManager.insertShifting(
	                							o.getId(),
	                							0,
	                							-mapping.getShiftParameter(),
	                							mapping.getShiftParameter() < 0,
	                							entry.getKey());
	                } catch (Throwable t) {
	                    log.error("Can't reset " + entry.getKey() + " shifting of opp: " + o.getId(), t);
	                }
				}
				mapping.setShiftParameter(0d);
			}

            if (o.getAutoShiftParameter() != 0) {
                try {
                    OpportunitiesManager.insertShifting(o.getId(), 0, -o.getAutoShiftParameter(), o.getAutoShiftParameter() < 0, SkinGroup.AUTO);
                } catch (Throwable t) {
                    log.error("Can't reset shifting of opp: " + o.getId(), t);
                }
            }
            o.setAutoShiftParameter(0);
            o.setLastShift(null);
//            try {
//                OpportunitiesManager.updateOpportunityShiftParameter(o.getId(), 0);
//            } catch (Throwable t) {
//                log.error("Can't zero the shift_parameter of opp: " + o.getId());
//            }
        }
    }

    public BigDecimal getAverageInvestments() {
        return averageInvestments;
    }

    public void setAverageInvestments(BigDecimal averageInvestments) {
        this.averageInvestments = averageInvestments;
    }

    public BigDecimal getFirstShiftParameter() {
        return firstShiftParameter;
    }

    public void setFirstShiftParameter(BigDecimal firstShiftParameter) {
        this.firstShiftParameter = firstShiftParameter;
    }

    public BigDecimal getFixedAmountForShifting() {
        return fixedAmountForShifting;
    }

    public void setFixedAmountForShifting(BigDecimal fixedAmountForShifting) {
        this.fixedAmountForShifting = fixedAmountForShifting;
    }

    public BigDecimal getPercentageOfAverageInvestments() {
        return percentageOfAverageInvestments;
    }

    public void setPercentageOfAverageInvestments(
            BigDecimal percentageOfAverageInvestments) {
        this.percentageOfAverageInvestments = percentageOfAverageInvestments;
    }

    public double getRandomCeiling() {
        return randomCeiling;
    }

    public void setRandomCeiling(double randomCeiling) {
        this.randomCeiling = randomCeiling;
    }

    public double getRandomFloor() {
        return randomFloor;
    }

    public void setRandomFloor(double randomFloor) {
        this.randomFloor = randomFloor;
    }

    public long getTypeOfShifting() {
        return typeOfShifting;
    }

    public void setTypeOfShifting(long typeOfShifting) {
        this.typeOfShifting = typeOfShifting;
    }

    public BigDecimal getRealSignificantPercentage() {
        return realSignificantPercentage;
    }

    public void setRealSignificantPercentage(BigDecimal realSignificantPercentage) {
        this.realSignificantPercentage = realSignificantPercentage;
    }

    public int getRealUpdatesPause() {
        return realUpdatesPause;
    }

    public void setRealUpdatesPause(int realUpdatesPause) {
        this.realUpdatesPause = realUpdatesPause;
    }

    public BigDecimal getSignificantPercentage() {
        return significantPercentage;
    }

    public void setSignificantPercentage(BigDecimal significantPercentage) {
        this.significantPercentage = significantPercentage;
    }

    public int getUpdatesPause() {
        return updatesPause;
    }

    public void setUpdatesPause(int updatesPause) {
        this.updatesPause = updatesPause;
    }

    public long getDisableAfterPeriod() {
        return disableAfterPeriod;
    }

    public void setDisableAfterPeriod(long disableAfterPeriod) {
        this.disableAfterPeriod = disableAfterPeriod;
    }

    public long getLastReutersUpdateReceiveTime() {
        return lastReutersUpdateReceiveTime;
    }

    public void setLastReutersUpdateReceiveTime(long lastReutersUpdateReceiveTime) {
        this.lastReutersUpdateReceiveTime = lastReutersUpdateReceiveTime;
    }

    public long getDisabledAfterUpdateAt() {
        return disabledAfterUpdateAt;
    }

    public void setDisabledAfterUpdateAt(long disabledAfterUpdateAt) {
        this.disabledAfterUpdateAt = disabledAfterUpdateAt;
    }

    public boolean isSuspended() {
        return suspended;
    }

    public void setSuspended(boolean suspended) {
        this.suspended = suspended;
    }

    public String getSuspendedMessage() {
        return suspendedMessage;
    }

    public void setSuspendedMessage(String suspendedMessage) {
        this.suspendedMessage = suspendedMessage;
    }

    public boolean isUseMainRic() {
        return useMainRic;
    }

    public void setUseMainRic(boolean useMainRic) {
        this.useMainRic = useMainRic;
    }

    /**
     * Create <code>Date</code> object by passed date, time and timezone.
     * The date should be in format "dd MMM yyyy".
     * The time should be in format HH:mm or HH:mm:ss.
     *
     * @param date
     * @param time
     * @param timeZone
     * @return
     */
    protected Date getUpdateDateTime(String date, String time, String timeZone) throws ParseException {
        SimpleDateFormat sdf = null;
        if (time.length() > 5) {
            sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.US);
        } else {
            sdf = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.US);
        }
        sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
        return sdf.parse(date + " " + time);
    }

    /**
     * Create <code>Date</code> object by passed time and timezone.
     * The time should be informat HH:mm or HH:mm:ss.
     *
     * @param time
     * @param timeZone
     * @return
     */
    protected Date getUpdateTime(String time, String timeZone) {
        StringTokenizer st = new StringTokenizer(time, ":");
        String hour = st.nextToken();
        String min = st.nextToken();
        String sec = st.hasMoreTokens() ? st.nextToken() : null;

        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(timeZone));
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hour));
        cal.set(Calendar.MINUTE, Integer.parseInt(min));
        if (null != sec) {
            cal.set(Calendar.SECOND, Integer.parseInt(sec));
        } else {
            cal.set(Calendar.SECOND, 0);
        }
        return cal.getTime();
    }

    /**
     * Get the closing level formula for the specified opportunity.
     *
     * @param w the opportunity for which to get closing level formula
     * @return The formula and the formula patamters with there values as one string.
     */
	public String getClosingLevelTxt(Opportunity o) {
		return o.getCloseLevelTxt() + ";colseLevel:" + getClosingLevel(o);
	}

    public boolean isShouldBeClosedByMonitoring() {
        return shouldBeClosedByMonitoring;
    }

    public void setShouldBeClosedByMonitoring(boolean shouldBeClosedByMonitoring) {
        this.shouldBeClosedByMonitoring = shouldBeClosedByMonitoring;
    }


    /**
     * get the date of the last opp of the day.
     *
     * @return the date of the last opp of the day.
     */
    public Date getLastOppOfTheDay(){
    	Date lastOppOfTheDay = null;
        Calendar cls = Calendar.getInstance();
        Calendar now = Calendar.getInstance();

    	for (Opportunity o : opportunities) {
			Date tempOppEstClosing = o.getTimeEstClosing();
			cls.setTime(o.getTimeEstClosing());
	        if (cls.get(Calendar.YEAR) == now.get(Calendar.YEAR) &&
	        		cls.get(Calendar.DAY_OF_YEAR) == now.get(Calendar.DAY_OF_YEAR)) {

	        	if (lastOppOfTheDay == null || lastOppOfTheDay.before(tempOppEstClosing)) {
					lastOppOfTheDay = tempOppEstClosing;
				}
	        }
		}
    	return lastOppOfTheDay;
    }

    /**
     * Reset the market internal state in the end of the day to be ready for the next
     * day opening. Long term shiftings, updates flags etc.
     */
    public void resetInTheEndOfTheDay() {
        // reset (zero) the shiftings of the long term opps
        resetOpportunitiesShiftings();

        // reset updates flags
        setSnapshotReceived(false);
        setLongTermSnapshotReceived(false);

    //    setLastReutersUpdateReceiveTime(0);
        todayOpeningTime = 0;

        // if the opps on the market were disabled in the end of the day - reset
        enableOpportunities("Reset in the end of the day", true);
    }

    /**
     * @return <code>true</code> if the market opened in the last 1 min else <code>false</code>.
     */
    public boolean hasJustOpened() {
        return System.currentTimeMillis() - todayOpeningTime < 60000; // opened in the last 1 min
    }

    public double getLastRealLevel() {
        return lastRealLevel;
    }

    public double getGraphLevel(SkinGroup skinGroup) {
        Opportunity o = getOpenedOppByScheduled(Opportunity.SCHEDULED_HOURLY);
        if (null == o) {
            o = getOpenedOppByScheduled(Opportunity.SCHEDULED_DAYLY);
        }
        if (null == o) {
            o = getOpenedOppByScheduled(Opportunity.SCHEDULED_WEEKLY);
        }
        if (null == o) {
            o = getOpenedOppByScheduled(Opportunity.SCHEDULED_MONTHLY);
        }
        if (null != o) {
            LevelsBean b = getOpportunityLevels(o, skinGroup);
            return b.level;
        }
        return 0;
    }

    public double getDayLevel(SkinGroup skinGroup) {
        Opportunity o = getOpenedOppByScheduled(Opportunity.SCHEDULED_DAYLY);
        if (null == o) {
            o = getOpenedOppByScheduled(Opportunity.SCHEDULED_WEEKLY);
        }
        if (null == o) {
            o = getOpenedOppByScheduled(Opportunity.SCHEDULED_MONTHLY);
        }
        if (null != o) {
            LevelsBean b = getOpportunityLevels(o, skinGroup);
            return b.level;
        }
        return 0;
    }

    public long getMarketId() {
        return marketId;
    }

	public void setPriorities(HashMap<Integer, HashMap<String, Integer>> skinsPriorities) {
		this.skinsPriorities = skinsPriorities;
	}

	/**
	 *
	 * only use in update hompage priroty compare
	 * @return onHomePageCompare if this item is on home page for specific skin
	 */
	public boolean isOnHomePageCompare() {
		return onHomePageCompare;
	}

	public void setOnHomePageCompare(int skinId) {
        compareSkinId = skinId;
		if (onHomePage.containsKey(skinId)) {
			onHomePageCompare = onHomePage.get(skinId);
			return;
		}
		onHomePageCompare = false;
		if (log.isDebugEnabled()) {
			log.debug("setOnHomePage for " + itemName + " skinId: " + skinId + ". was: " + null + " setting to: " + false);
	    }
		onHomePage.put(skinId, false);
	}

	public boolean isHasGoodForHomepageCompare() {
		return HasGoodForHomepageCompare;
	}

	public void setHasGoodForHomepageCompare(int skinId) {
		HasGoodForHomepageCompare = hasGoodForHomepage(skinId);
	}

	/**
	 * @return true if any of the skin have this market on home page
	 */
	public boolean hasOnHomePage() {
		boolean key = false;
        for (Iterator<Boolean> i = onHomePage.values().iterator(); i.hasNext();) {
            key = i.next();
            if (key){
            	return true;
            }
        }
        return false;
	}

    /**
     * Pause the long term opps that are waiting to pause.
     */
    public void pauseWaitingToPauseLongTermOpps() {
        Opportunity o = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            if (o.getState() == Opportunity.STATE_WAITING_TO_PAUSE) {
                LevelService.getInstance().opportunityStateChanged(o.getId(), Opportunity.STATE_PAUSED);
            }
        }
    }

    public boolean hasWeeklyWaitingToPause() {
        Opportunity o = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            if (o.getScheduled() == Opportunity.SCHEDULED_WEEKLY && o.getState() == Opportunity.STATE_WAITING_TO_PAUSE) {
                return true;
            }
        }
        return false;
    }

	/**
	 * @return the specialEntry
	 */
	public Opportunity getSpecialEntry() {
		return specialEntry;
	}

	/**
     * Get this Item current Golden Minutes opportunity if there is one.
     *
     * @param endOfHour if the current Golden Minutes interval is a end of hour or mid hour one
     * @return <code>Opportunity</code> that is the Golden Minutes one for the current GM interval
     *      or <code>null</code> if one is not available.
	 */
    public Opportunity getGoldenMinutesOpportunity(Expiry expiry) {
        Opportunity o = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            if (o.isGoodForGoldenMinute(expiry)) {
                return o;
            }
        }
        return null;
    }

    class LevelsBean {
        public double level;
        public double devCheckLevel;
    }

    protected String getSecondRicLetter(String letter) {
        String UseSecondLetter = OpportunitiesManager.getOpportunityOptionsExpirationLetter(optionPlusMarket ? optionPlusMarketId : marketId);
        if (null != UseSecondLetter) {
            letter = UseSecondLetter; //INDICES_SECOND_LETTER;
        }
        return letter;
    }

    /**
     * check if should close opp from this item
     * if we got updates in last X sec
     * @param l
     *
     * @return true if should close, false if not.
     */
    public abstract boolean isShouldBeClosed(long l);

    /**
     * @return the dontCloseAfterXSecNoUpdate
     */
    public long getDontCloseAfterXSecNoUpdate() {
        return dontCloseAfterXSecNoUpdate;
    }

    /**
     * @param dontCloseAfterXSecNoUpdate the dontCloseAfterXSecNoUpdate to set
     */
    public void setDontCloseAfterXSecNoUpdate(long dontCloseAfterXSecNoUpdate) {
        this.dontCloseAfterXSecNoUpdate = dontCloseAfterXSecNoUpdate;
    }

    /**
     * check which opps are open and the scheduled they have for filter in the trade pages
     * return  4 all, 3 weekly/daily/hourly, 2 daily/hourly, 1 hourly, 0 close/suspend
     */
    public int getFilterScheduled() {
        int scheduled = 0;
        if (opportunities.size() == 0 || suspended) {
            return scheduled;
        }
        for (int i = 0; i < opportunities.size(); i++) {
            if (opportunities.get(i).isInOppenedState()) {
                switch (opportunities.get(i).getScheduled()) {
                case Opportunity.SCHEDULED_MONTHLY:
                    return Opportunity.SCHEDULED_MONTHLY;
                case Opportunity.SCHEDULED_WEEKLY:
                    scheduled = Opportunity.SCHEDULED_WEEKLY;
                    break;
                case Opportunity.SCHEDULED_DAYLY:
                    if (scheduled < Opportunity.SCHEDULED_DAYLY) {
                        scheduled = Opportunity.SCHEDULED_DAYLY;
                    }
                    break;
                case Opportunity.SCHEDULED_HOURLY:
                    if (scheduled < Opportunity.SCHEDULED_HOURLY) {
                        scheduled = Opportunity.SCHEDULED_HOURLY;
                    }
                    break;
                default:
                    break;
                }
            }
        }
        return scheduled;
    }
    
    /* 
     * returns a bitmask of opportunities that are scheduled and not suspended or market itself
     * 
     * 0000 -> all opportunities for the market are suspended
     * 1000 -> monthly
     * 0100 -> weekly
     * 0010 -> daily
     * 0001 -> hourly
     * 
     */
    public String getNewFilterScheduled() {
	int scheduled = 0;
	if (opportunities.size() == 0 || suspended) {
	    return String.valueOf(scheduled);
	}
	for (int i = 0; i < opportunities.size(); i++) {
	    if (opportunities.get(i).isInOppenedState() && !opportunities.get(i).isSuspended()) {
		switch (opportunities.get(i).getScheduled()) {
		case Opportunity.SCHEDULED_MONTHLY: // 1000 -> 8
		    scheduled |= 8;
		    break;
		case Opportunity.SCHEDULED_WEEKLY: // 0100 -> 4
		    scheduled |= 4;
		    break;
		case Opportunity.SCHEDULED_DAYLY: // 0010 -> 2
		    scheduled |= 2;
		    break;
		case Opportunity.SCHEDULED_HOURLY: // 0001 -> 1
		    scheduled |= 1;
		    break;
		}
	    }
	}
	String bits = Integer.toBinaryString(scheduled);
	while (bits.length() < 4) {
	    bits = "0" + bits;
	}
	return bits;
    }

    /**
     * if level was change in big percentage send email to traders
     * only if Current Level Alert Perc is not 0
     * @param currentCalc the current level
     *
     */
    public void sendLevelChangeAlert(double currentCalc) {
    	if (opportunities.size() > 0
    			&& opportunities.get(0).getMarket().getCurrentLevelAlertPerc().doubleValue() > 0
    			&& new BigDecimal(lastCalcResult)
    					.subtract(new BigDecimal(currentCalc))
    						.divide(new BigDecimal(lastCalcResult), RoundingMode.HALF_UP)
    							.abs(new MathContext(4, RoundingMode.HALF_UP))
    								.compareTo(opportunities.get(0).getMarket().getCurrentLevelAlertPerc()) > 0) {
    		// Mail alert is no longer required. No one is interested in it and floods mailserver
//	    	LevelService.getInstance().sendBigLevelChangeEmail("big level change alert " + itemName, "there was big level change in market: " + opportunities.get(0).getMarket().getName() + " more then " + opportunities.get(0).getMarket().getCurrentLevelAlertPerc().doubleValue() * 100 + "% last level was: " + lastCalcResult + " current level is: " + currentCalc);
	    	log.debug("big level change for market " + opportunities.get(0).getMarket().getName() + " more than " + opportunities.get(0).getMarket().getCurrentLevelAlertPerc().doubleValue() * 100 + "% last level was: " + lastCalcResult + " current level is: " + currentCalc);
	    }
    }

    /**
     * @return the compareSkinId
     */
    public long getCompareSkinId() {
        return compareSkinId;
    }

    /**
     * @return <code>true</code> if this item has at least one opp in waiting for expiry or closed or last 1 min at home page state
     *      else <code>false</code>.
     */
    public boolean hasGoingToCloseOpportunities() {
        if (opportunities.size() == 0) {
            return false;
        }
        Opportunity o = null;
        int state = 0;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            state = o.getState();
            if (state == Opportunity.STATE_CLOSING_1_MIN || state == Opportunity.STATE_CLOSING || state == Opportunity.STATE_CLOSED ) {
                return true;
            }
        }
        return false;
    }

    public boolean isOptionPlusMarket() {
        return optionPlusMarket;
    }

    public void setOptionPlusMarket(boolean optionPlusMarket) {
        this.optionPlusMarket = optionPlusMarket;
    }

    /**
     * @return the optionPlusMarketId
     */
    public long getOptionPlusMarketId() {
        return optionPlusMarketId;
    }

    public boolean isBinary0100Market() {
		return binary0100Market;
	}

	public void setBinary0100Market(boolean binary0100Market) {
		this.binary0100Market = binary0100Market;
	}

	public void openOpportunity(Opportunity oppId) {}

	public void setTodayOpeningTime(long todayOpeningTime) {
		this.todayOpeningTime = todayOpeningTime;
	};

	/**
	 * @return <code>true</code> if the item has at least one open non one-touch opportunity, otherwise <code>false</code>
	 */
	public boolean hasOpenNonOneTouchOpportunities() {
		for (Opportunity opportunity : opportunities) {
			if (opportunity.isAvailableForTrading()
					&& !opportunity.getIsOneTouchOpp()) {
				return true;
			}
		}
		return false;
	}

	public Opportunity getOppInState(int state) {
		for (Opportunity opportunity : opportunities) {
			if (opportunity.getState() == state) {
				return opportunity;
			}
		}
		return null;
	}

	/**
	 * @return the marketGroupId
	 */
	public Long getMarketGroupId() {
		return marketGroupId;
	}

   /*/**
     * when closing opp need to save the closed opp id and set the lastClosedOppNextUpdateWritten to false
     * then we know we need to save the next update into the db
     * @param oppId the close opp id
     *//*
    public void setNeedToSaveNextUpdate(long oppId) {
    	lastClosedOppId = oppId;
    	lastClosedOppNextUpdateShouldBeWritten = true;
    }

    public void updateFirstQuote(ReutersQuotes firstQuote) {
    	if (lastClosedOppNextUpdateShouldBeWritten && lastClosedOppId != 0) {
        	try {
				ReutersQuotesManager.insert(lastClosedOppId, firstQuote);
			} catch (SQLException e) {
				log.debug("Can't insert reuters qoute update into db", e);
			}
			lastClosedOppNextUpdateShouldBeWritten = false;
        	lastClosedOppId = 0;
        }
    }*/
	
	/**
     * Send update message for all opps in the item.
	 * @param isDelete should send delete msg
     */
    public void sendOppsUpdate(boolean isDelete) {
        Opportunity o = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            if ((o.isInOppenedState() || o.getState() == Opportunity.STATE_WAITING_TO_PAUSE) &&
            		!(o.getState() == Opportunity.STATE_CLOSED)) {
            	LevelService.getInstance().sendOppUpdate(o, isDelete ? ConstantsBase.JMS_OPPS_UPDATE_COMMAND_DELETE : 
					ConstantsBase.JMS_OPPS_UPDATE_COMMAND_ADD);
            }
        }
    }
    
    public String getNextOpenHourly() {
    	TreeSet<Opportunity> oppTreeSet = new TreeSet<Opportunity>(new Comparator<Opportunity>() {
			@Override
			public int compare(Opportunity opp1, Opportunity opp2) {
	            return opp1.getTimeEstClosing().after(opp2.getTimeEstClosing()) ? 1 : -1;
	        }
		});
        Opportunity o = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY && o.isInOppenedState()) {
            	oppTreeSet.add(o);
            }
        }
        return oppTreeSet.size() > 1 ? getSecondElement(oppTreeSet) : null;
    }

	private String getSecondElement(TreeSet<Opportunity> oppTreeSet) {
		oppTreeSet.pollFirst();
		Opportunity o = oppTreeSet.pollFirst();
		return o.getId() + "|" + formatDate(o.getTimeEstClosing());
	}

	public boolean isDynamicsMarket() {
		return dynamicsMarket;
	}

	public void setDynamicsMarket(boolean dynamicsMarket) {
		this.dynamicsMarket = dynamicsMarket;
	}

	public String getQuoteParams() {
		return quoteParams;
	}

	public void setQuoteParams(String quoteParams) {
		this.quoteParams = quoteParams;
	}
}