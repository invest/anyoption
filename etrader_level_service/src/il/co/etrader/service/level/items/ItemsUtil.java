package il.co.etrader.service.level.items;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;

import il.co.etrader.service.level.options.InterestOption;

public class ItemsUtil {
	private static final Logger log = Logger.getLogger(ItemsUtil.class);

    /**
     * @param o the opportunity for which to calc the level
     * @return The week/month level for this item.
     */
    public static double calcIndexAndStockLongTermLevel(String itemName, Opportunity o, InterestOption interest, double lastCalcResult) {
        long c5 = Item.getDaysToExpire(o.getTimeEstClosing());
        if (log.isTraceEnabled()) {
            log.trace("oppId: " + o.getId() + " days to expire: " + c5);
        }
        double c6 = (interest.getAsk() + interest.getBid()) / 2;
        return lastCalcResult * (1 + c6 / 100 / getLongTermLevelDivideDays(itemName) * c5);
    }

    protected static long getLongTermLevelDivideDays(String itemName) {
//        if (itemName.equals(".TA25")) {
//            return 365;
//        } else if (itemName.equals(".TELREAL")) {
//            return 365;
//        } else if (itemName.equals(".FTSE")) {
//            return 365;
//        } else
        if (itemName.equals(".DJI")) {
            return 360;
        } else if (itemName.equals(".SPX")) {
            return 360;
        } else if (itemName.equals(".IXIC")) {
            return 360;
//      } else if (itemName.equals(".GDAXI")) {
/*          return 360;
        } else if (itemName.equals(".FCHI")) {
            return 360;*/
//        } else if (itemName.equals(".HSI")) {
//            return 365;
//        } else if (itemName.endsWith(".TA")) { // stocks
//            return 365;
        }
        return 365; // .TA25, .TELREAL, .FTSE, .HSI, stocks
    }
}