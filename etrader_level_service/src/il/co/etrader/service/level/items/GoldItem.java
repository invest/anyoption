package il.co.etrader.service.level.items;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ReutersQuotes;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.BigDecimalHelper;
import il.co.etrader.service.level.OpportunitiesManager;
import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.options.Option;
import il.co.etrader.service.level.options.SecondRICOption;

/**
 * Implements the current level/closing level calculation for Gold.
 * It is probably better to rename it to Commodities already as it already
 * handle Copper and Silver as well.
 *
 * @author Tony
 */
public class GoldItem extends Item {
    private static final Logger log = Logger.getLogger(GoldItem.class);

    protected SecondRICOption secondRIC;
    protected BigDecimal lastTrdprc;
    protected BigDecimal lastASK;
    protected BigDecimal lastBID;
    protected String lastSALTIM;

    public GoldItem(String itemName, Handle handle) {
        super(itemName, handle);

        useMainRic = false;
        shouldBeClosedByMonitoring = true;

        dontCloseAfterXSecNoUpdate = 240;
    }

    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
    @Override
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        return false;
    }

    /**
     * Notify the item that one of its options have received update.
     *
     * @param option the option that received update
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    @Override
    public boolean optionUpdated(Option option, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        boolean gotClosingLevel = false;
        if (option.getOptionName().equals(secondRIC.getOptionName())) {
            try {
            	Double bid = (Double) option.getBid();
                Double ask = (Double) option.getAsk();
                Double trdprc1 = (Double) option.getLast();
                String salTim = (String) option.getActiveDate();

                if ((null == ask && null == bid && null == salTim && null == trdprc1) ||
                        (null != trdprc1 && trdprc1 == 0) ||
                        (null != ask && ask == 0) ||
                        (null != bid && bid == 0)) {
                    return gotClosingLevel;
                }
                lastReutersUpdateReceiveTime = System.currentTimeMillis();

                if (null != ask) {
                    lastASK = new BigDecimal(ask.toString());
                }
                if (null != bid) {
                    lastBID = new BigDecimal(bid.toString());
                }

                if (null != trdprc1 && trdprc1 > 0) {
                	lastTrdprc = new BigDecimal(trdprc1.toString());
                }

                if (null != salTim) {
                    lastSALTIM = salTim;
                }

                if (null == lastASK || null == lastBID || null == lastSALTIM) {
                	return gotClosingLevel;
                }
                
                //double crrRealLevel = lastASK.add(lastBID).add(lastTrdprc).divide(BigDecimalHelper.THREE, BigDecimalHelper.BIG_DECIMAL_CALC_SCALE, RoundingMode.HALF_UP).doubleValue();
                double crrRealLevel = lastASK.add(lastBID).divide(BigDecimalHelper.TWO, BigDecimalHelper.BIG_DECIMAL_CALC_SCALE, RoundingMode.HALF_UP).doubleValue();

                Date updateTime = null;
                if (null != lastSALTIM) {
                    try {
                        updateTime = getUpdateTime(lastSALTIM, "GMT");
                    } catch (Throwable t) {
                        log.error("Can't process SALTIM.", t);
                        updateTime = new Date(System.currentTimeMillis());
                    }
                } else {
                    log.error("Update without SALTIM!!!");
                    updateTime = new Date(System.currentTimeMillis());
                }

                if (null != trdprc1) {
	                String closingLevelTxt = "formula:round((ask+bid)/2);ask:" + lastASK + ";bid:" + lastBID + ";closeLevelBeforeRound:" + crrRealLevel ;
	                ReutersQuotes currentQuote = new ReutersQuotes(updateTime, null, lastASK, lastBID);
	                gotClosingLevel = updateOpportunitiesClosingLevel(updateTime, crrRealLevel, crrRealLevel, closingLevelTxt, closingLevelTxt, currentQuote, currentQuote, opportunitiesTemp);
                }

                double crrLevel = formulaRandomChange(crrRealLevel);
                haveUpdate(sender, lifeId, true, crrRealLevel, crrLevel, true, crrRealLevel);

                setSnapshotReceived(true);
                setLongTermSnapshotReceived(true);
            } catch (Throwable t) {
                log.error("Failed to process option update.", t);
            }
        }
        return gotClosingLevel;
    }

    protected String getSecondRicName() {
      //  int mta = nextMonth ? 1 : 0; // months to add
//        String start = "GC";
//        if (itemName.equals("SI")) {
//            start = "SI";
//        } else if (itemName.equals("HG")) {
//            start = "HG";
//        }
    	String start = itemName;
    	if (start.endsWith(".Option+")) {
    		start = start.substring(0, start.indexOf(".Option+"));
    	}
    	if (start.endsWith(".Bubbles")) {
    		start = start.substring(0, start.indexOf(".Bubbles"));
    	}
        return start + getSecondRicLetter(COMMODITIES_DEFAULT_LETTER); // getFutureMonthLetter(mta) + getFutureYearDigit(mta);
    }

    /**
     * Tell the item to open (subscribe) all its options (when you want to start using it).
     *
     * @param itm the Item instance to receive the updates for the options.
     */
    @Override
    public void subscribeOptions(Item itm) {
        super.subscribeOptions(itm);
        //boolean nextMonth = OpportunitiesManager.getOpportunityOptionsChangeMonth(marketId);
        secondRIC = (SecondRICOption) Option.getInstance(getSecondRicName());
        optionSubscribtionManager.subscribeOption(itm, secondRIC);
    }

    @Override
    public boolean isReutersStateOK() {
        return secondRIC.isReutersStateOK() && super.isReutersStateOK();
    }

    /**
     * Clear the options list (unsibscribe each option it remove).
     * Make sure you have lock over the options when you call this metod.
     *
     * @param itm the Item instance to be removed from the options.
     */
    @Override
    protected void clearOptions(Item itm) {
        super.clearOptions(itm);
        optionSubscribtionManager.unsubscribeOption(itm, secondRIC);
        secondRIC = null;
    }

    /**
     * @param o the opportunity for which to calc the level
     * @return The long term formula result for this item.
     */
    @Override
    protected double longTermFormula(Opportunity o) {
        return 0;
    }

    /**
     * Check if the closing level for specified opportunity has been received.
     *
     * @param o the opportunity to check for if the closing level is ready
     * @return <code>true</code> if the closing level is ready to be taken else <code>false</code>
     */
    @Override
    public boolean isClosingLevelReceived(Opportunity o) {
//        return isSnapshotReceived(o);
        return o.isClosingLevelReceived();
    }

    /**
     * Get the closing level for the specified opportunity.
     *
     * @param o the opportunity for which to get closing level
     * @return The level as received from Reuters. No formula and exposure control calculations.
     */
    @Override
    public double getClosingLevel(Opportunity o) {
        long decimal = o.getMarket().getDecimalPoint() - o.getMarket().getDecimalPointSubtractDigits();
        if (itemName.equals("CL.Option+")) {
            decimal += 1;
        }
        if (o.isUseManualClosingLevel()) {
            return roundDouble(o.getManualClosingLevel(), decimal);
        }
        double cl = roundDouble(o.getNextClosingLevel(), decimal);
        if (log.isInfoEnabled()) {
            log.info("TRADINGDATA_" + itemName + " expiration - opportunityId: " + o.getId() + " expiration level: " + cl);
        }
        return cl;
    }

    @Override
    protected String getSecondRicLetter(String letter) {
        String useSecondLetter = OpportunitiesManager.getOpportunityOptionsExpirationLetter(optionPlusMarket ? optionPlusMarketId : marketId);
        if (null != useSecondLetter) {
            /*int mta = OpportunitiesManager.getOpportunityOptionsChangeMonth(optionPlusMarket ? optionPlusMarketId : marketId) ? 1 : 0;
            letter = String.valueOf(getFutureMonthLetter(mta)) + String.valueOf(getFutureYearDigit(mta));
            */
            letter = useSecondLetter;
        }
        return letter;
    }

    @Override
    public boolean isShouldBeClosed(long oppId) {
        Opportunity o = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            if (o.getId() == oppId) {
                if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
                    return System.currentTimeMillis() - lastReutersUpdateReceiveTime <= dontCloseAfterXSecNoUpdate * 1000;
                }
                break;
            }
        }
        return true;
    }

    /**
     * Reset the market internal state in the end of the day to be ready for the next
     * day opening. Long term shiftings, updates flags etc.
     */
    @Override
    public void resetInTheEndOfTheDay() {
        super.resetInTheEndOfTheDay();
        lastASK = null;
        lastBID = null;
        lastSALTIM = null;
        lastTrdprc = null;
    }
    
    @Override
    protected Map<String, String> getQuoteFields() {
    	Map<String, String> quoFlds = new HashMap<String, String>();
    	if (null != lastASK) {
    		quoFlds.put(QUOTE_FIELDS_ASK, lastASK.toString());
    	}
    	if (null != lastBID) {
    		quoFlds.put(QUOTE_FIELDS_BID, lastBID.toString());
    	}
    	if (null != lastTrdprc) {
    		quoFlds.put(QUOTE_FIELDS_LAST, lastTrdprc.toString());
    	}
    	return quoFlds;
    }
}