package il.co.etrader.service.level.items;

import java.math.BigDecimal;

import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.BigDecimalHelper;
import il.co.etrader.service.level.LevelService;

/**
 * SP500 index current level calculation implementation.
 *
 * @author Eyal
 */
public class SP500AndKLSEIndexItem extends FTSEIndexItem {
    public SP500AndKLSEIndexItem(String itemName, Handle handle) {
        super(itemName, handle);
    }
    
    @Override
    protected String getInterestName() {
    	return itemName.equals(".KLSE") ? "MYR1MD=" : "USD1MD=";
    }

    /**
     * @return Hook to get the param value.
     */
    @Override
    protected BigDecimal getParameter() {
    	if (itemName.equals(".SPX")) {
    		return LevelService.getSp500Parameter();
    	} //only KLSE left
    	return LevelService.getKlseParameter();
    }

    @Override
    protected String getSecondRicName() {
    	if (itemName.equals(".SPX")) {
    		return "ES" + getSecondRicLetter(INDICES_DEFAULT_LETTER);
    	} //only KLSE left
    	return "FKLI" + getSecondRicLetter(INDICES_DEFAULT_LETTER);
    }

    /**
     * add the future to the level
     * @param tmpcl the level without the future
     * @return the level with future
     */
    @Override
    protected double addFuture(BigDecimal tmpcl) {
    	return tmpcl.multiply(BigDecimalHelper.WEIGHT_OPTIONS)
    				.add(lastFirstRICTrdprc1.multiply(BigDecimalHelper.WEIGHT_MARKET))
    				.add(getParameter())
        			.doubleValue();
	}
}