package il.co.etrader.service.level.items;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.jboss.logging.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ReutersQuotes;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.options.CurrencyOption;
import il.co.etrader.service.level.options.Option;


public class OptionPlusCurrencyItem extends Item {
    private Logger log = Logger.getLogger(OptionPlusCurrencyItem.class);

    protected CurrencyOption option;

    public OptionPlusCurrencyItem(String itemName, Handle handle) {
        super(itemName, handle);

        useMainRic = false;
        if (itemName.endsWith(".Option+") || itemName.endsWith(".Bubbles")) {
            option = (CurrencyOption) Option.getInstance(itemName.substring(0, itemName.length() - 8)); // Remove the ".Option+"/".Bubbles" from the end
        } else {
            option = (CurrencyOption) Option.getInstance(itemName);
        }
    }

    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
    @Override
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        return false;
    }

    /**
     * Notify the item that one of its options have received update.
     *
     * @param option the option that received update
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    @Override
    public boolean optionUpdated(Option option, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
    	log.debug("got optionUpdated BinaryZeroOneHundredItem binaryMarket inner instance");
        boolean gotClosingLevel = false;
        if (option.getOptionName().equals(this.option.getOptionName())) {
            setSnapshotReceived(true);
            lastReutersUpdateReceiveTime = System.currentTimeMillis();
            Date updateTime = null;
            try {
                updateTime = getUpdateTime(option.getActiveTime(), "GMT");
            } catch (Exception e) {
                log.error("Can't process TIMACT", e);
                updateTime = new Date(System.currentTimeMillis());
            }
            gotClosingLevel = gotClosingLevel || updateOpportunitiesClosingLevel(updateTime, ((CurrencyOption)option).getLastRealLevel(), ((CurrencyOption)option).getLastRealLevel(), "(ASK+BID)/2", "(ASK+BID)/2", new ReutersQuotes(updateTime, null, new BigDecimal(option.getAsk()), new BigDecimal(option.getBid())), null, opportunitiesTemp);

            haveUpdate(sender, lifeId, true, option.getLastLevel(), option.getLastLevel(), true, option.getLastLevel());
        }
        return gotClosingLevel;
    }

    /**
     * Tell the item to open (subscribe) all its options (when you want to start using it).
     *
     * @param itm the Item instance to receive the updates for the options.
     */
    @Override
    public void subscribeOptions(Item itm) {
        super.subscribeOptions(itm);
        optionSubscribtionManager.subscribeOption(itm, option);
    }

    @Override
    public boolean isReutersStateOK() {
        return option.isReutersStateOK();
    }

    /**
     * Clear the options list (unsibscribe each option it remove).
     * Make sure you have lock over the options when you call this metod.
     *
     * @param itm the Item instance to be removed from the options.
     */
    @Override
    protected void clearOptions(Item itm) {
        super.clearOptions(itm);
        optionSubscribtionManager.unsubscribeOption(itm, option);
    }

    /**
     * @param o the opportunity for which to calc the level
     * @return The long term formula result for this item.
     */
    @Override
    protected double longTermFormula(Opportunity o) {
        return 0;
    }

    /**
     * Check if the closing level for specified opportunity has been received.
     *
     * @param w the opportunity to check for if the closing level is ready
     * @return <code>true</code> if the closing level is ready to be taken else <code>false</code>
     */
    @Override
    public boolean isClosingLevelReceived(Opportunity w) {
        return w.isClosingLevelReceived();
    }

    /**
     * Get the closing level for the specified opportunity.
     *
     * @param o the opportunity for which to get closing level
     * @return The level as received from Reuters. No formula and exposure control calculations.
     */
    @Override
    public double getClosingLevel(Opportunity o) {
        long decimal = o.getMarket().getDecimalPoint() - o.getMarket().getDecimalPointSubtractDigits();
        return roundDouble(o.getNextClosingLevel(), decimal);
    }

    /**
     * check if should close opp from this item
     * if we got updates in last X sec
     * @param l
     *
     * @return true if should close, false if not.
     */
    @Override
    public boolean isShouldBeClosed(long l) {
        return true;
    }

    /**
     * return the option ask
     * @return option ask
     */
    public double getOptionAsk() {
    	return option.getAsk();
    }

    /**
     * return the option bid
     * @return option bid
     */
    public double getOptionBid() {
    	return option.getBid();
    }
    
    @Override
    public void resetInTheEndOfTheDay() {
    	super.resetInTheEndOfTheDay();
    	
    	option.resetInTheEndOfTheDay();
    }
    
    @Override
    protected Map<String, String> getQuoteFields() {
    	Map<String, String> quoFlds = new HashMap<String, String>();
    	if (null != option.getAsk()) {
    		quoFlds.put(QUOTE_FIELDS_ASK, option.getAsk().toString());
    	}
    	if (null != option.getBid()) {
    		quoFlds.put(QUOTE_FIELDS_BID, option.getBid().toString());
    	}
    	if (null != option.getLast()) {
    		quoFlds.put(QUOTE_FIELDS_LAST, option.getLast().toString());
    	}
    	return quoFlds;
    }
}