package il.co.etrader.service.level.items;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.options.Option;
import il.co.etrader.service.level.options.TelAviv25Option;

public class TelAviv25MaofIndexItem extends TelAviv25IndexItem {
    private static final Logger log = Logger.getLogger(TelAviv25MaofIndexItem.class);

  //  protected TelAviv25Option telAviv25;
//  use to get opp special entry start time
//    protected Item telAviv25Item;

    public TelAviv25MaofIndexItem(String itemName, Handle handle) {
        super(itemName, handle);
        shouldBeClosedByMonitoring = false;
        useMainRic = false;

        telAviv25 = (TelAviv25Option) Option.getInstance(".TA25");
    }

    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        super.update(fields, sender, lifeId, opportunitiesTemp);
        return false;
    }

    @Override
    public boolean isShouldBeClosed(long oppId) {
        return true;
    }

}