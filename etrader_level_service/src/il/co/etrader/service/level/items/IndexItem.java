package il.co.etrader.service.level.items;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ReutersQuotes;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.options.InterestOption;
import il.co.etrader.service.level.options.Option;
import il.co.etrader.service.level.options.SecondRICOption;

/**
 * Implement the formula for index items (except the Tel Aviv 25 and Tel Aviv Nadlan).
 *
 * @author Tony
 */
public class IndexItem extends StockItem {
    private static final Logger log = Logger.getLogger(IndexItem.class);

    protected SecondRICOption secondRIC;
    protected String timezone;
    
    protected String lastQuoteLast;

    public IndexItem(String itemName, Handle handle) {
        super(itemName, handle);

        useMainRic = false;

        getSecondRic();

        String interestName = "ILS1MD=";
        if (itemName.equals(".TA25") || itemName.equals(".TELREAL") || itemName.equals(".TA25INDEX")) { // ILS
            // do nothing it is already ILS
            // interestName = "ILS1MD=";
        } else if (itemName.equals(".FTSE")) { // GBP
            interestName = "GBP1MD=";
        } else if (itemName.equals(".DJI") || itemName.equals(".SPX") || itemName.startsWith(".IXIC") || itemName.equals("VXX")) { // USD
            interestName = "USD1MD=";
        } else if (itemName.startsWith(".IBEX")
        		|| itemName.startsWith(".FTMIB")
        		|| itemName.equals(".GDAXI")
        		|| itemName.equals(".FCHI")
        		|| itemName.equals(".MDAX")
        		|| itemName.equals(".TECDAX")
        		|| itemName.equals(".PSI20")
        		|| itemName.equals(".FTITLMS")
        		|| itemName.equals(".AEX")) { // EUR
            interestName = "EUR1MD=";
        } else if (itemName.equals(".N225") || itemName.equals(".TOPX500")) { // JPY
            interestName = "JPY1MD=";
        } else if (itemName.equals(".HSI") || itemName.startsWith(".KS11")) { // HKD
            interestName = "HKD1MD=";
        } else if (itemName.equals(".BSESN")) {
            interestName = "INR1MD=";
        } else if (itemName.equals(".TASI")) {
            interestName = "SAR1MD=";
        } else if (itemName.equals(".KWSE")) {
            interestName = "KWD1MD=";
        } else if (itemName.equals(".ADI") || itemName.equals(".DFMGI")) {
            interestName = "AED1MD=";
	    } else if (itemName.equals(".MXX")) {
	        interestName = "MXN1M=";
	    } else if (itemName.equals(".XU100")) {
	    	interestName = "TRY1MD=";
	    } else if (itemName.equals(".JKSE")) {
	    	interestName = "IDR1MD=";
	    } else if (itemName.equals(".KLSE")) {
	    	interestName = "MYR1MD=";
	    } else if (itemName.equals(".IRTS")) {
            interestName = "RUB1MD=";
        } else if (itemName.equals(".AXJO")) {
            interestName = "AUD1MD=";
        } else if (itemName.equals(".FTSTI")) {
            interestName = "SGD1MD=";
        } else if (itemName.equals(".TWII")) {
            interestName = "TWD1MD=";
        } else if (itemName.equals(".SSE180")) {
        	interestName = "CNY1MD=";
        } else if (itemName.equals(".OMXS30")) {
        	interestName = "SEK1MD=";
        }

        interest = (InterestOption) Option.getInstance(interestName);

        if (itemName.equals(".TECDAX") || itemName.equals(".VIX")) {
            dontCloseAfterXSecNoUpdate = 120;
        } else if (itemName.equals(".DFMGI")) {
        	dontCloseAfterXSecNoUpdate = 240;
        }

        timezone = "GMT";
        if (itemName.startsWith(".GDAXI") || itemName.equals(".MDAX") || itemName.equals(".TECDAX")) {
        	timezone = "Europe/Berlin";
        }

    }

    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
    @Override
	public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        return false;
    }

    /**
     * Notify the item that one of its options have received update.
     *
     * @param option the option that received update
     * @param jmsHandler
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    @Override
	public boolean optionUpdated(Option option, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        boolean gotClosingLevel = super.optionUpdated(option, sender, lifeId, opportunitiesTemp);
        if (option.getOptionName().equals(secondRIC.getOptionName())) {
	        Double crrTrdPrc1 = option.getLast();
	        String trdTim = option.getActiveDate();

            if (null != crrTrdPrc1 && crrTrdPrc1 > 0) {
            	lastQuoteLast = crrTrdPrc1.toString();
            }

	        if (null != trdTim) {
	            lastTrdTim = trdTim;
	        }

	        if (null != crrTrdPrc1 && crrTrdPrc1 > 0) {
	        	// keep it updated so the Item.shouldEnableOpportunities will work properly
	            lastReutersUpdateReceiveTime = System.currentTimeMillis();

	            setSnapshotReceived(true);
	            if (interest.isSnapshotReceived()) {
	                setLongTermSnapshotReceived(true);
	            }

	            Date updateTime = null;
	            if (null != lastTrdTim) {
	                try {
	                    updateTime = getUpdateTime(lastTrdTim, timezone);
	                } catch (Exception e) {
	                    log.error("Can't process TRDTIM_1.", e);
	                    updateTime = new Date(System.currentTimeMillis());
	                }
	            } else {
	                log.error("TRDPRC_1 update without TRDTIM_1!!!");
	                updateTime = new Date(System.currentTimeMillis());
	            }

	            String closingLevelTxt = "formula:TRDPRC_1;TRDPRC_1:" + crrTrdPrc1 + ";closeLevelBeforeRound:" + crrTrdPrc1;
	            ReutersQuotes currentQuote = new ReutersQuotes(updateTime, crrTrdPrc1);
	            gotClosingLevel |= updateOpportunitiesClosingLevel(updateTime, crrTrdPrc1, crrTrdPrc1, closingLevelTxt, closingLevelTxt, currentQuote, currentQuote, opportunitiesTemp);

	            double crrFormulaResult = formulaRandomChange(crrTrdPrc1);
	            haveUpdate(sender, lifeId, true, crrTrdPrc1, crrFormulaResult, true, crrTrdPrc1);
	        }
        }
        return gotClosingLevel;
    }

    /**
     * Instance secondRIC in method so subclasses that doesn't need it can overwrite this method with empty one or something else.
     */
    protected void getSecondRic() {
    	secondRIC = (SecondRICOption) Option.getInstance(itemName.endsWith(".Option+") || itemName.endsWith(".Bubbles") ? itemName.substring(0, itemName.length() - 8) : itemName);
    }

    /**
     * Tell the item to open (subscribe) all its options (when you want to start using it).
     *
     * @param itm the Item instance to receive the updates for the options.
     */
	@Override
	public void subscribeOptions(Item itm) {
		super.subscribeOptions(itm);
		if (null != secondRIC) {
			optionSubscribtionManager.subscribeOption(itm, secondRIC);
		}
	}

	@Override
	public boolean isReutersStateOK() {
		if (null == secondRIC) {
			return super.isReutersStateOK();
		}
		return secondRIC.isReutersStateOK() && super.isReutersStateOK();
	}

	/**
     * Clear the options list (unsibscribe each option it remove).
     * Make sure you have lock over the options when you call this metod.
     *
     * @param itm the Item instance to be removed from the options.
     */
	@Override
	protected void clearOptions(Item itm) {
		super.clearOptions(itm);
		if (null != secondRIC) {
			optionSubscribtionManager.unsubscribeOption(itm, secondRIC);
		}
	}

	/**
     * Check if the closing level for specified opportunity has been received.
     *
     * @param o the opportunity to check for if the closing level is ready
     * @return <code>true</code> if the closing level is ready to be taken else <code>false</code>
     */
    @Override
	public boolean isClosingLevelReceived(Opportunity o) {
        return o.isClosingLevelReceived();
    }

    /**
     * Get the closing level for the specified opportunity.
     *
     * @param o the opportunity for which to get closing level
     * @return The level as received from Reuters. No formula and exposure control calculations.
     */
    @Override
	public double getClosingLevel(Opportunity o) {
        long decimal = o.getMarket().getDecimalPoint() - o.getMarket().getDecimalPointSubtractDigits();
        if (o.isUseManualClosingLevel()) {
            return roundDouble(o.getManualClosingLevel(), decimal);
        }
        double cl = roundDouble(o.getNextClosingLevel(), decimal);
        if (log.isInfoEnabled()) {
            log.info("TRADINGDATA_" + itemName +
                    " expiration - opportunityId: " + o.getId() +
                    " TRDPRC_1: " + o.getNextClosingLevel() +
                    " expiration level: " + cl);
        }
        return cl;
    }

    @Override
    public boolean isShouldBeClosed(long oppId) {
        Opportunity o = null;
        for (int i = 0; i < opportunities.size(); i++) {
            o = opportunities.get(i);
            if (o.getId() == oppId) {
                if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
                    return System.currentTimeMillis() - secondRIC.getLastReutersUpdateReceiveTime() <= dontCloseAfterXSecNoUpdate * 1000;
                }
                break;
            }
        }
        return true;
    }
    
    @Override
    public void resetInTheEndOfTheDay() {
    	super.resetInTheEndOfTheDay();
    	lastQuoteLast = null;
    }
    
    @Override
    protected Map<String, String> getQuoteFields() {
    	Map<String, String> quoFlds = new HashMap<String, String>();
    	if (null != lastQuoteLast) {
    		quoFlds.put(QUOTE_FIELDS_LAST, lastQuoteLast);
    	}
    	return quoFlds;
    }
}