package il.co.etrader.service.level.items;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ReutersQuotes;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.options.InterestOption;
import il.co.etrader.service.level.options.Option;

/**
 * Handle levels calculations for all London Stocks
 *
 * @author Eyal
 */
public class LondonNewStockItem extends NewStockItem {
    private static final Logger log = Logger.getLogger(LondonNewStockItem.class);

    protected double GenVal1;
    protected String lastExchtim;
    protected String lastGV1Date;

    public LondonNewStockItem(String itemName, Handle handle) {
        super(itemName, handle);

        interest = (InterestOption) Option.getInstance("GBP1MD=");

        if (itemName.equals("BP.L")) {
            dontCloseAfterXSecNoUpdate = 120;
        }
    }

    /**
     * Notify the item about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
     */
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
        try {
            Double crrTrdPrc1 = (Double) fields.get("TRDPRC_1");
            String GV1Date = (String) fields.get("GV1_DATE");
            if (null == GV1Date || GV1Date.trim().equals("")) {
                GV1Date = lastGV1Date;
            } else {
                lastGV1Date = GV1Date;
            }
            if (null != crrTrdPrc1 && crrTrdPrc1 > 0) {
                String timeAct = (String) fields.get(timeField);
                String exchtim = (String) fields.get("EXCHTIM");

                if (null == timeAct || timeAct.trim().equals("")) {
                    timeAct = lastTrdTim;
                }
                if (null == exchtim || exchtim.trim().equals("")) {
                    exchtim = lastExchtim;
                } else {
                    lastExchtim = exchtim;
                }

                Calendar timeActCal = Calendar.getInstance();
                Calendar exchtimCal = Calendar.getInstance();
                timeActCal.setTime(getUpdateTime(timeAct, "GMT"));
                exchtimCal.setTime(getUpdateTime(exchtim, "GMT"));
                exchtimCal.add(Calendar.MINUTE, 10);
                if ((timeActCal.after(exchtimCal) && isTodayTime(GV1Date + " 05:00:00", "GMT")) || !isTodayTime(GV1Date + " 05:00:00", "GMT")) { //we set the time to 05:00:00 to be sure day time saveing will not have any effect
                    log.debug("got spike in " + itemName);
                    return false;
                }
            }
        } catch (Exception e) {
            log.debug("cant chekc for spike in " + itemName, e);
        }
        if (null != fields.get("GEN_VAL1")) {
            lastReutersUpdateReceiveTime = System.currentTimeMillis();
            GenVal1 = (Double) fields.get("GEN_VAL1");
        }
        return processStockUpdate(fields, sender, lifeId, false, opportunitiesTemp);
    }

    /**
     * Get the closing level for the specified opportunity.
     *
     * @param w the opportunity for which to get closing level
     * @return The level as received from Reuters. No formula and exposure control calculations.
     */
    public double getClosingLevel(Opportunity o) {
        long decimal = o.getMarket().getDecimalPoint() - o.getMarket().getDecimalPointSubtractDigits();
        if (o.isUseManualClosingLevel()) {
            return roundDouble(o.getManualClosingLevel(), decimal);
        }
        if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
            double cl = roundDouble(o.getNextClosingLevel(), decimal);
            if (log.isInfoEnabled()) {
                log.info("TRADINGDATA_" + itemName +
                        " expiration - opportunityId: " + o.getId() +
                        " expiration level: " + cl);
            }
            return cl;
        } else {
            double cl = roundDouble(GenVal1, decimal);
            if (log.isInfoEnabled()) {
                log.info("TRADINGDATA_" + itemName +
                        " expiration - opportunityId: " + o.getId() +
                        " GEN_VAL1: " + GenVal1 +
                        " expiration level: " + cl);
            }
            return cl;
        }
    }

    /**
     * Check if the closing level for specified opportunity has been received.
     *
     * @param o the opportunity to check for if the closing level is ready
     * @return <code>true</code> if the closing level is ready to be taken else <code>false</code>
     */
    public boolean isClosingLevelReceived(Opportunity o) {
        if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
//            return isSnapshotReceived(o);
            return o.isClosingLevelReceived();
        } else {
            return GenVal1 > 0;
        }
    }

    /**
     * Reset the market internal state in the end of the day to be ready for the next
     * day opening. Long term shiftings, updates flags etc.
     */
    public void resetInTheEndOfTheDay() {
        super.resetInTheEndOfTheDay();
        // clear internally stored last values
        GenVal1 = 0;
        lastExchtim = null;
        lastGV1Date = null;
    }

    public String getClosrLevelTxtDay() {
		return "formula:round(GEN_VAL1);GEN_VAL1:" + GenVal1 + ";closeLevelBeforeRound:" + GenVal1;
	}

    public boolean setUpdateOpportunitiesClosingLevel(Date updateTime, double crrRealLevel, double dailyClosingLevel, String closeLevelTxtHour, String closeLevelTxtDay, ArrayList<Opportunity> opportunitiesTemp) {
		return updateOpportunitiesClosingLevel(updateTime, crrRealLevel, GenVal1, closeLevelTxtHour, closeLevelTxtDay, new ReutersQuotes(updateTime, lastTrdPrc, lastASK, lastBID), new ReutersQuotes(updateTime, GenVal1), opportunitiesTemp);
	}

}