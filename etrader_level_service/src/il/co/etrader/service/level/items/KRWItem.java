//not in use anymore
//package il.co.etrader.service.level.items;
//
//import il.co.etrader.bl_vos.Opportunity;
//import il.co.etrader.service.level.SenderThread;
//import il.co.etrader.service.level.options.CurrencyOption;
//import il.co.etrader.service.level.options.Option;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//
//import org.apache.log4j.Logger;
//
//import com.reuters.rfa.common.Handle;
//
//public class KRWItem extends CurrencyItem {
//    private static final Logger log = Logger.getLogger(KRWItem.class);
//    protected CurrencyOption option;
//
//    public KRWItem(String itemName, Handle handle) {
//        super(itemName, handle);
//
//        useMainRic = false;
//        shouldBeClosedByMonitoring = true;
//
//        option = (CurrencyOption) Option.getInstance(itemName);
//        interestCRR1M = Option.getInstance("KRW1M=");
//    }
//
//    /**
//     * Notify the item about update for it received from Reuters.
//     *
//     * @param fields the fields and values received from reuters
//     * @param sender
//     * @param lifeId
//     * @return <code>true</code> if this update brought closing level (HST_CLOSE for example) else <code>false</code>.
//     */
//    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
//        return false;
//    }
//
//    /**
//     * Notify the item that one of its options have received update.
//     *
//     * @param option the option that received update
//     * @param sender
//     * @param lifeId
//     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
//     */
//    public boolean optionUpdated(Option option, SenderThread sender, long lifeId, ArrayList<Opportunity> opportunitiesTemp) {
//        boolean gotClosingLevel = false;
//        if (option.getOptionName().equals(this.option.getOptionName())) {
//            setSnapshotReceived(true);
//            lastReutersUpdateReceiveTime = System.currentTimeMillis();
//            Date updateTime = null;
//            try {
//                updateTime = getUpdateDateTime(option.getActiveDate(), option.getActiveTime(), "GMT");
//            } catch (Exception e) {
//                log.error("Can't process ACTIVE_DATE or TIMACT", e);
//                updateTime = new Date(System.currentTimeMillis());
//            }
//            gotClosingLevel = gotClosingLevel || updateOpportunitiesClosingLevel(updateTime, ((CurrencyOption) option).getLastRealLevel(), ((CurrencyOption) option).getLastRealLevel(), "(ASK+BID)/2", "(ASK+BID)/2", opportunitiesTemp);
//
//            haveUpdate(sender, lifeId, true, option.getLastLevel(), option.getLastLevel(), true, option.getLastLevel());
//        }
//        gotClosingLevel = gotClosingLevel || super.optionUpdated(option, sender, lifeId, opportunitiesTemp);
//        return gotClosingLevel;
//    }
//
//    /**
//     * Check if the closing level for specified opportunity has been received.
//     *
//     * @param o the opportunity to check for if the closing level is ready
//     * @return <code>true</code> if the closing level is ready to be taken else <code>false</code>
//     */
//    public boolean isClosingLevelReceived(Opportunity o) {
//        return o.isClosingLevelReceived();
//    }
//
//    /**
//     * Get the closing level for the specified opportunity.
//     *
//     * @param o the opportunity for which to get closing level
//     * @return The level as received from Reuters. No formula and exposure control calculations.
//     */
//    public double getClosingLevel(Opportunity o) {
//        return o.getNextClosingLevel();
//    }
//
//    /**
//     * Get the closing level formula for the specified opportunity.
//     *
//     * @param w the opportunity for which to get closing level formula
//     * @return The formula and the formula patamters with there values as one string.
//     */
//    public String getClosingLevelTxt(Opportunity o) {
//        return o.getCloseLevelTxt() + ";colseLevel:" + getClosingLevel(o);
//    }
//
//    @Override
//    public boolean isShouldBeClosed(long oppId) {
//        Opportunity o = null;
//        for (int i = 0; i < opportunities.size(); i++) {
//            o = opportunities.get(i);
//            if (o.getId() == oppId) {
//                if (o.getScheduled() == Opportunity.SCHEDULED_HOURLY) {
//                    return System.currentTimeMillis() - lastReutersUpdateReceiveTime <= dontCloseAfterXSecNoUpdate * 1000;
//                }
//                break;
//            }
//        }
//        return true;
//    }
//
//    /**
//     * Tell the item to open (subscribe) all its options (when you want to start using it).
//     *
//     * @param itm the Item instance to receive the updates for the options.
//     */
//    public void subscribeOptions(Item itm) {
//        optionSubscribtionManager.subscribeOption(itm, option);
//        optionSubscribtionManager.subscribeOption(itm, interestUSD1M);
//        optionSubscribtionManager.subscribeOption(itm, interestCRR1M);
//    }
//
//    public boolean isReutersStateOK() {
//        if (!interestUSD1M.isReutersStateOK() || !interestCRR1M.isReutersStateOK()) {
//            return false;
//        }
//        return option.isReutersStateOK();
//    }
//
//    /**
//     * Clear the options list (unsibscribe each option it remove).
//     * Make sure you have lock over the options when you call this metod.
//     *
//     * @param itm the Item instance to be removed from the options.
//     */
//    protected void clearOptions(Item itm) {
//        optionSubscribtionManager.unsubscribeOption(itm, option);
//        optionSubscribtionManager.unsubscribeOption(itm, interestUSD1M);
//        optionSubscribtionManager.unsubscribeOption(itm, interestCRR1M);
//    }
//}