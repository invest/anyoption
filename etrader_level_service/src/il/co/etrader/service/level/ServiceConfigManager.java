package il.co.etrader.service.level;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.beans.Market;
import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.bl_vos.ServiceConfig;
import il.co.etrader.dao_managers.ServiceConfigDAO;

/**
 * Service db config manager.
 * 
 * @author Tony
 */
public class ServiceConfigManager extends BaseBLManager {
    /**
     * Load the service config.
     * 
     * @return <code>ServiceConfig</code> with the service configuration or <code>null</code> if
     *      no config in the db.
     * @throws SQLException
     */
    public static ServiceConfig getServiceConfig() throws SQLException {
        ServiceConfig sc = null;
        Connection conn = null;
        try {
            conn = getConnection();
            sc = ServiceConfigDAO.getServiceConfig(conn);
        } finally {
            closeConnection(conn);
        }
        return sc;
    }

    /**
     * Update the service_config table average_investment field with the average
     * value up to yesterday including.
     * 
     * @throws SQLException
     */
    public static void updateAverageInvestments() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            ServiceConfigDAO.updateAverageInvestments(conn);
        } finally {
            closeConnection(conn);
        }
    }
    
    /**
     * Get all markets average investments for the last 30 days. (id, feed_name, average_investments).
     * 
     * @return <code>ArrayList<Market></code> each element of the list has only
     *      id, feed_name and average_investments properties filled.
     * @throws SQLException
     */
    public static ArrayList<Market> getAverageInvestments() throws SQLException {
        ArrayList<Market> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = ServiceConfigDAO.getAverageInvestments(conn);
        } finally {
            closeConnection(conn);
        }
        return l;
    }
}