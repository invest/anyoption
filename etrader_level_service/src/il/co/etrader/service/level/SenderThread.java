package il.co.etrader.service.level;

import java.io.Serializable;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicLong;

import javax.jms.JMSException;

import org.apache.log4j.Logger;

import com.anyoption.common.jms.ETraderFeedMessage;
import com.anyoption.common.jms.JMSHandler;

/**
 * A thread that queue and send messages over JMS.
 * 
 * @author Tony
 */
public class SenderThread extends Thread {
    private static final Logger log = Logger.getLogger(SenderThread.class);
    
    private boolean running;
    private JMSHandler jmsHandler;
    private AtomicLong queueSize;
    private boolean sendText;
    
    //this is the queue of pending requests for the LevelsService 
    private ConcurrentLinkedQueue<Serializable> toSendRequests = new ConcurrentLinkedQueue<Serializable>();
    
    public SenderThread(JMSHandler jmsHandler, boolean sendText) {
        this.jmsHandler = jmsHandler;
        queueSize = new AtomicLong();
        this.sendText = sendText;
    }
    
    public void run() {
        running = true;
        log.info("SenderThread started");
        
        while (running) {
            Thread.currentThread().setName(jmsHandler.getLogName() + "JMSSender");
            log.debug("Sender starting work.");
            
            //go on until there are requests in the queue
            Serializable nextRequest = null;
            try {
                // just get the message from the head but don't remove it
                while ((nextRequest = toSendRequests.peek()) != null) {
                    if (nextRequest instanceof ETraderFeedMessage) {
                        ((ETraderFeedMessage) nextRequest).timeDispatched = System.currentTimeMillis();
                    }
                    //send message to through JMS
                    if (sendText) {
                    	jmsHandler.sendMessage((String) nextRequest);
                    } else {
                    	jmsHandler.publishMessage(nextRequest);
                    }
                    toSendRequests.poll(); // now that the message was sent we can remove it
                    long qs = queueSize.decrementAndGet();
                    if (qs > 100) {
                        log.info("Dispatched. queueSize: " + qs);
                    } else if (log.isTraceEnabled()) {
                        log.trace("Dispatched. queueSize: " + qs);
                    }
                }
            } catch (JMSException je) {
                log.error("Can't actually dispatch request " + nextRequest + ": JMSException -> " + je.getMessage());
                // if we can't send we have nothing to do and we will fall asleep until better times
            }
            
            log.debug("Sender falling asleep.");

            try {
                synchronized (this) {
                    this.wait();
                }
            } catch (InterruptedException ie) {
                // do nothing
            }
        }
        log.info("SenderThread ends");
    }
    
    /**
     * Queue a message to be sent to the LevelsService.
     * 
     * @param message the message to send
     */
    public void send(Serializable message) {
        if (message instanceof ETraderFeedMessage) {
            ((ETraderFeedMessage) message).timeQueued = System.currentTimeMillis();
        }
        toSendRequests.add(message);
        long qs = queueSize.incrementAndGet();
        log.trace(jmsHandler.getLogName() + "JMSSender Queued. queueSize: " + qs);
        wakeUp();
    }
    
    /**
     * Wake up the sender to try to send messages if available.
     */
    public void wakeUp() {
        synchronized (this) {
            try {
                this.notify();
            } catch (Exception e) {
                log.error("Can't wake up the sender.", e);
            }
        }
    }
    
    public void stopSenderThread() {
        running = false;
        wakeUp();
    }
    
    public int getQueueSize() {
        return (int) queueSize.get();
    }
}