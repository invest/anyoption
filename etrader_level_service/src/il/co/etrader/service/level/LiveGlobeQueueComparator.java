/**
 * 
 */
package il.co.etrader.service.level;

import java.util.Comparator;

/**
 * @author pavelhe
 *
 */
public class LiveGlobeQueueComparator implements Comparator<LiveMessageWrapper> {

	@Override
	public int compare(LiveMessageWrapper o1, LiveMessageWrapper o2) {
		// DESCENDING ORDER
		if (o1.getAmount() > o2.getAmount()) {
			return -1;
		} else if (o1.getAmount() < o2.getAmount()) {
			return 1;
		} else {
			return 0;
		}
	}

}
