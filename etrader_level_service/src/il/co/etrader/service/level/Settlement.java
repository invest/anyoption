package il.co.etrader.service.level;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.OpportunitySettle;
import com.anyoption.common.jms.msgs.SettledOpportunitiesMessage;

/**
 * The settlement logic assume there is only 1 process (thread) doing settlement at one moment.
 * Having two may lead to unpredictable results.
 *
 * The settlement logic depends on the total amounts for the current year so keep this in mind
 * when the current year changes!!!
 *
 * @author Tony
 */
public class Settlement extends Thread {
    private static final Logger log = Logger.getLogger(Settlement.class);

    private boolean running;
    private long lastJingleTime;
    private SenderThread sender;

    public Settlement(SenderThread sender) {
    	this.sender = sender;
    }
    
    public void run() {
        Thread.currentThread().setName("Settlement");
        running = true;
        log.warn("Settlement thread started.");
        while (running) {
            if (log.isTraceEnabled()) {
                log.trace("Settlement start");
            }
            lastJingleTime = System.currentTimeMillis();
            try {
                // TODO: Tony: don't need to load all the investments info just the id
                ArrayList<Integer> l = InvestmentsManager.getInvestmentsToSettle();
                for (int i = 0; i < l.size(); i++) {
                    InvestmentsManager.settleInvestment(l.get(i));
                }
                List<OpportunitySettle> oppsToSettle = OpportunitiesManager.settleOpportunities();
                if (oppsToSettle != null && oppsToSettle.size() > 0) {
                	if (log.isTraceEnabled()) {
                		StringBuilder sb = new StringBuilder();
                		sb.append("Settled opportunities [");
                		for(int idx = 0; idx < oppsToSettle.size(); idx++) {
                			sb.append(oppsToSettle.get(idx).getId());
                			if (idx + 1 < oppsToSettle.size()) {
                				sb.append(", ");
                			}
                		}
                		sb.append("]. Sending to lightstreamer...");
                		log.trace(sb.toString());
                	}
                	sender.send(new SettledOpportunitiesMessage(oppsToSettle));
                } else {
                	log.trace("No opportunities to settle");
                }
            } catch (Throwable t) {
                log.error("Failed to settle investments.", t);
            }
            if (log.isTraceEnabled()) {
                log.trace("Settlement done");
            }
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException ie) {
                log.warn("Settlement thread sleep interrupted", ie);
            }
        }
        log.warn("Settlement thread ends.");
    }

    public void stopSettlementThread() {
        running = false;
    }

    public boolean isWorking() {
        return System.currentTimeMillis() - lastJingleTime < 2000;
    }
}