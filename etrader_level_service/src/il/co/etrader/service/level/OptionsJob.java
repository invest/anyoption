package il.co.etrader.service.level;

import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.service.level.market.Market;

import il.co.etrader.bl_managers.ReutersQuotesManagerBase;

public class OptionsJob implements Runnable {
    private static Logger log = Logger.getLogger(OptionsJob.class);

    private Date runTime;
    private LevelService levelService;

    public OptionsJob(Date runTime, LevelService levelService) {
        this.runTime = runTime;
        this.levelService = levelService;
    }

    public void run() {
        Thread.currentThread().setName("OptionsJob");
        if (log.isTraceEnabled()) {
            log.trace("Start OptionsJob. runTime: " + runTime);
        }

        for (int i = Opportunity.STATE_CREATED; i <= Opportunity.STATE_DONE; i++) {
            if (log.isTraceEnabled()) {
                log.trace("Checking for state: " + i);
            }
            try {
                ArrayList<Long> list = OpportunitiesManager.getOpportunitiesMovedToState(i, runTime);
                if (log.isTraceEnabled()) {
                	log.trace(list);
                }
                for (int l = 0; l < list.size(); l++) {
                	// TODO Potential race condition with closing after update
                    try {
                        if (i == Opportunity.STATE_CLOSED) {
                            if (levelService.isOpportunityClosingLevelReceived(list.get(l))) {
                            	Market market = levelService.getMarketByOppId(list.get(l));
                            	Opportunity opp = market.getOppById(list.get(l));
                        		boolean shouldBeClosed = market.isShouldBeClosed(list.get(l));
                            	if (null != opp && opp.getState() != Opportunity.STATE_CLOSED) {
	                            	double closingLevel = market.getClosingLevel(opp); //levelService.getOpportunityClosingLevel(list.get(l));
                            		String closingLevelTxt = market.getClosingLevelTxt(opp);
	                                if (closingLevel == 0) {
	                                    log.warn("Opportunity closing level is 0. Not closing opp: " + list.get(l));
	                                    levelService.sendNonSettlementEmail("", "Opportunity id " + list.get(l) + " not settled, closing level is 0. resettle it manually!");
	                                    continue;
	                                }
	                                if (!shouldBeClosed) { //should not be closed
	                                    log.warn("no update Not closing opp: " + list.get(l));
	                                    levelService.sendNonSettlementEmail("", "Opportunity id " + list.get(l) + " not settled, no update for more then X sec . settle it manually!");
	                                    continue;
	                                }
	                                OpportunitiesManager.closeOpportunity(list.get(l), closingLevel, closingLevelTxt);
	                                levelService.opportunityStateChanged(list.get(l), i);
	                                //should call getClosingLevel first
	                                log.debug("insert reuters qoutes for opp " + list.get(l));
	                                ReutersQuotesManagerBase.insert(opp);
                            	}
                            } else {
                                if (log.isInfoEnabled()) {
                                    log.info("No closing level yet for " + list.get(l));
                                }
                            }
                        } else {
                            if (i == Opportunity.STATE_OPENED) {
                                OpportunitiesManager.publishOpportunity(list.get(l), true);
                            }
                            levelService.opportunityStateChanged(list.get(l), i);
                        }
                    } catch (Throwable t) {
                        log.error("Can't change state of opp: " + list.get(l), t);
                    }
                }
            } catch (Throwable t) {
                log.error("Problem checking opportunities.", t);
            }
        }
//        new Settlement().start();

        if (log.isTraceEnabled()) {
            log.trace("Pause week/month opportunities.");
        }
        try {
            ArrayList<Long> list = OpportunitiesManager.getOpportunitiesToPause(runTime);
            for (int l = 0; l < list.size(); l++) {
                try {
                    OpportunitiesManager.publishOpportunity(list.get(l), false);
//                    levelService.opportunityStateChanged(list.get(l), Opportunity.STATE_PAUSED);
                    levelService.opportunityStateChanged(list.get(l), Opportunity.STATE_WAITING_TO_PAUSE);
                } catch (Throwable t) {
                    log.error("Can't pause opp: " + list.get(l), t);
                }
            }
        } catch (Throwable t) {
            log.error("Problem pausing week/month opportunities.", t);
        }

        if (log.isTraceEnabled()) {
            log.trace("Unpause week/month opportunities.");
        }
        try {
            ArrayList<Long> list = OpportunitiesManager.getOpportunitiesToUnpause(runTime);
            for (int l = 0; l < list.size(); l++) {
                try {
                    OpportunitiesManager.publishOpportunity(list.get(l), true);
                    levelService.opportunityStateChanged(list.get(l), Opportunity.STATE_OPENED);
                } catch (Throwable t) {
                    log.error("Can't unpause opp: " + list.get(l), t);
                }
            }
        } catch (Throwable t) {
            log.error("Problem unpausing week/month opportunities.", t);
        }

        if (log.isTraceEnabled()) {
            log.trace("End OptionsJob.");
        }
    }
}