package il.co.etrader.service.level;

import java.util.Hashtable;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;

import il.co.etrader.util.CommonUtil;

/**
 * A thread that queue and send emails.
 *
 * @author Tony
 */
public class EmailThread extends Thread {
    private static final Logger log = Logger.getLogger(EmailThread.class);

    private String emailServer;
    private boolean emailServerAuth;
    private String emailUser;
    private String emailPass;
    private String emailFrom;

    private boolean running;

    //this is the queue of pending requests for the LevelsService
    private ConcurrentLinkedQueue<EmailBean> toSendRequests = new ConcurrentLinkedQueue<EmailBean>();

    public EmailThread(String emailServer, boolean emailServerAuth, String emailUser, String emailPass, String emailFrom) {
        this.emailServer = emailServer;
        this.emailServerAuth = emailServerAuth;
        this.emailUser = emailUser;
        this.emailPass = emailPass;
        this.emailFrom = emailFrom;
    }

    public void run() {
        Thread.currentThread().setName("EmailSender");
        running = true;
        log.warn("EmailThread started");

        while (running) {
            if (log.isTraceEnabled()) {
                log.trace("EmailThread starting work.");
            }

            // go on until there are requests in the queue
            EmailBean nextEmail = null;
            try {
                // just get the message from the head but don't remove it
                while ((nextEmail = toSendRequests.peek()) != null) {
                    //send message to the feed through JMS
                    sendEmail(nextEmail.getTo(), nextEmail.getSubject(), nextEmail.getBody());
                    toSendRequests.poll(); // now that the message was sent we can remove it
                    if (log.isTraceEnabled()) {
                        log.trace("Email sent: " + nextEmail);
                    }
                }
            } catch (Throwable t) {
                log.error("Can't actually send email " + nextEmail + ": " + t.getMessage());
                // if we can't send we have nothing to do and we will fall asleep until better times
            }

            if (log.isTraceEnabled()) {
                log.trace("EmailThread falling asleep.");
            }

            try {
                synchronized (this) {
                    this.wait();
                }
            } catch (InterruptedException ie) {
                // do nothing
            }
        }

        log.warn("EmailThread ends");
    }

    /**
     * Queue a email to be sent.
     *
     * @param message the message to send
     */
    public void send(String to, String subject, String body) {
        if (null != to && !to.equals("")) {
            toSendRequests.add(new EmailBean(to, subject, body));
            synchronized (this) {
                try {
                    this.notify();
                } catch (Throwable t) {
                    log.error("Can't wake up the email sender.", t);
                }
            }
        } else {
            if (log.isInfoEnabled()) {
                String ls = System.getProperty("line.separator");
                log.info("No RCPTs. Will not send: " + ls +
                        "subject: " + subject + ls +
                        "body: " + body);
            }
        }
    }

    /**
     * Wake up the email sender to try to send emails if available.
     */
    public void wakeUp() {
        synchronized (this) {
            try {
                this.notify();
            } catch (Throwable t) {
                log.error("Can't wake up the email sender.", t);
            }
        }
    }

    public void stopEmailThread() {
        running = false;
        wakeUp();
    }

    public int getQueueSize() {
        return toSendRequests.size();
    }

    private void sendEmail(String to, String subject, String body) {
        // set the server properties
        Hashtable<String, String> serverProperties = new Hashtable<String, String>();
        serverProperties.put("url", emailServer);
        if (emailServerAuth) {
            serverProperties.put("auth", "true");
            serverProperties.put("user", emailUser);
            serverProperties.put("pass", emailPass);
        }

        // Set the email properties
        Hashtable<String, String> emailProperties = new Hashtable<String, String>();
        emailProperties.put("subject", subject);
        emailProperties.put("from", emailFrom);
        emailProperties.put("to", to);
        emailProperties.put("body", body);

        CommonUtil.sendEmail(serverProperties, emailProperties, null);
    }

    /**
     * Bean to hold email data in the queue.
     *
     * @author Tony
     */
    class EmailBean {
        private String to;
        private String subject;
        private String body;

        public EmailBean(String to, String subject, String body) {
            this.to = to;
            if (null != subject && !subject.equals("")) {
                this.subject = subject;
            } else {
                this.subject = "";
            }
            if (null != body && !body.equals("")) {
                this.body = body;
            } else {
                this.body = "";
            }
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }

        public String toString() {
            String ls = System.getProperty("line.separator");
            return ls + "EmailBean:" + ls +
                    "to: " + to + ls +
                    "subject: " + subject + ls +
                    "body: " + body + ls;
        }
    }
}