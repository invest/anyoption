package il.co.etrader.service.level;

import java.util.Calendar;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;

import il.co.etrader.service.level.managers.MarketsManager;

/**
 * Job to create todays opportunities.
 *
 * @author Tony
 */
public class OptionsCreate implements Runnable {
    private static Logger log = Logger.getLogger(OptionsCreate.class);

    private int daysAhead;
    private LevelService levelService;

    public OptionsCreate(LevelService levelService, int daysAhead) {
        this.levelService = levelService;
        this.daysAhead = daysAhead;
    }

    public void run() {
        Thread.currentThread().setName("OptionsCreate");
        Calendar cal = Calendar.getInstance();
        if (log.isInfoEnabled()) {
            log.info("Creating todays opportunities.");
        }
        try {
            OpportunitiesManager.createDailyOpportunities(daysAhead);
        } catch (Throwable t) {
            log.error("Failed to create todays opportunities.", t);
            levelService.sendReportEmail("BARUCH HAMOR GADOl daily", "Failed to create daily opps please press the RED button ASAP /n Never tell your problems to anyone. 20% don't care and the other 80% are glad you have them");
        }
        try {
            // 3 days ahead - the week starts on sunday
            if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) {
                OpportunitiesManager.createWeeklyOpportunities();
            }
        } catch (Throwable t) {
            log.error("Failed to create next week opportunities.", t);
            levelService.sendReportEmail("BARUCH HAMOR GADOl Weekly", "Failed to create weekly opps please press the RED button ASAP /n life is too short to remove USB safely");
        }
        try {
            // 3 days ahead of the shortest month (feb - 28 days)
            if (cal.get(Calendar.DAY_OF_MONTH) == 25) {
                OpportunitiesManager.createMonthlyOpportunities();
            }
        } catch (Throwable t) {
            log.error("Failed to create next month opportunities.", t);
            levelService.sendReportEmail("BARUCH HAMOR GADOl Monthly", "Failed to create Monthly opps please press the RED button ASAP /n There are no limits to what you can accomplish when you are supposed to be doing something else");
        }
        try {
            // 3 days ahead of the shortest month (feb - 28 days)
            if (cal.get(Calendar.DAY_OF_MONTH) == 25 && cal.get(Calendar.MONTH) % 3 == 1) {
                OpportunitiesManager.createQuarterlyOpportunities();
            }
        } catch (Throwable t) {
            log.error("Failed to create next quarter opportunities.", t);
            levelService.sendReportEmail("BARUCH HAMOR GADOl Quarterly", "Failed to create Quarterly opps please press the RED button ASAP /n There are no limits to what you can accomplish when you are supposed to be doing something else");
        }
        try {
            // 4 days ahead - the week starts on sunday
            if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) {
                OpportunitiesManager.createOneTouchOpportunities();
            }
        } catch (Throwable t) {
            log.error("Failed to create one touch opportunities.", t);
            levelService.sendReportEmail("BARUCH HAMOR GADOl one touch", "Failed to create one touch opps please press the RED button ASAP /n IF YOU BUILT IT, IT WILL COME!!!");
        }
        try {
            String report = OpportunitiesManager.reportCreatedOpportunities();
            levelService.sendReportEmail("daily", report);
        } catch (Throwable t) {
            log.error("Failed to report created opportunities.", t);
        }

        try {
            // 3 days ahead - the week starts on sunday
            if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) {
                String report = OpportunitiesManager.reportCreatedWeeklyOpportunities();
                levelService.sendReportEmail("weekly", report);
            }
        } catch (Throwable t) {
            log.error("Failed to report created weekly opportunities.", t);
        }

        try {
            // 3 days ahead of the shortest month (feb - 28 days)
            if (cal.get(Calendar.DAY_OF_MONTH) == 25) {
                String report = OpportunitiesManager.reportCreatedScheduledOpportunities(Opportunity.SCHEDULED_MONTHLY);
                levelService.sendReportEmail("monthly", report);
            }
        } catch (Throwable t) {
            log.error("Failed to report created monthly opportunities.", t);
        }

        try {
            if (cal.get(Calendar.DAY_OF_MONTH) == 25 && cal.get(Calendar.MONTH) % 3 == 1) {
                String report = OpportunitiesManager.reportCreatedScheduledOpportunities(Opportunity.SCHEDULED_QUARTERLY);
                levelService.sendReportEmail("quarterly", report);
            }
        } catch (Throwable t) {
            log.error("Failed to report created quarterly opportunities.", t);
        }

        try {
        	if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) {
	            String report = OpportunitiesManager.oneTouchReportCreatedOpportunities();
	            levelService.sendOneTouchReportEmail("", report);
        	}
        } catch (Throwable t) {
            log.error("Failed to report created one touch opportunities.", t);
        }
        if (log.isInfoEnabled()) {
            log.info("Done creating todays opportunities.");
        }

        try {
            ServiceConfigManager.updateAverageInvestments();
            levelService.updateMarketsAverageInvestments();
            if (log.isDebugEnabled()) {
                log.debug("Updated average investment last 30 days.");
            }
        } catch (Throwable t) {
            log.error("Problem updating average investment last 30 days.", t);
        }
        try {
            MarketsManager.deleteMarketsRates();
            if (log.isInfoEnabled()) {
                log.info("Cleaned markets rates.");
            }
        } catch (Throwable t) {
            log.error("Problem cleaning yesterdays markets rates.", t);
        }
        if (log.isInfoEnabled()) {
            log.info("Creating todays opportunities done.");
        }
    }
}