package il.co.etrader.service.level;

//import org.jboss.system.ServiceMBean;

public interface LevelServiceMBean extends LevelServiceMXBean {
    public void startService();
    public void stopService();
    public Integer getJMSSenderQueueSize();
    public Integer getWebJMSSenderQueueSize();
    public Integer getDataUpdatesQueueSize();
    public Integer getEmailSenderQueueSize();
    public void reset();
}