package il.co.etrader.service.level;

import java.math.BigDecimal;

public class BigDecimalHelper {
    public static final BigDecimal ZERO = new BigDecimal(0);
    public static final BigDecimal ONE = new BigDecimal(1);
    public static final BigDecimal TWO = new BigDecimal(2);
    public static final BigDecimal THREE = new BigDecimal(3);
    public static final BigDecimal WEIGHT_OPTIONS = new BigDecimal(0.7);
    public static final BigDecimal WEIGHT_MARKET = new BigDecimal(0.3);
    public static final BigDecimal TA_WEIGHT_OPTIONS = new BigDecimal(0.5);
    public static final BigDecimal TA_WEIGHT_MARKET = new BigDecimal(0.5);


    public static final int BIG_DECIMAL_CALC_SCALE = 7;
}