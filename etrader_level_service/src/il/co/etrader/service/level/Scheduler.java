package il.co.etrader.service.level;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;

import org.apache.log4j.Logger;
import com.anyoption.common.util.GoldenMinute.Expiry;
import com.anyoption.common.util.GoldenMinute.GoldenMinutePeriod;

import il.co.etrader.bl_managers.InvestmentsManagerBase;

public class Scheduler implements Runnable {
    private static Logger log = Logger.getLogger(Scheduler.class);
    
    private boolean running;
    private String createTime;
    private String daysAhead;
    private LevelService levelService;
    private long lastTickTime;
    
    public Scheduler(String createTime, String daysAhead, LevelService levelService) {
        this.createTime = createTime;
        this.daysAhead = daysAhead;
        this.levelService = levelService;
    }
    
    public void run() {
        Thread.currentThread().setName("Scheduler");
        
        // init create
        Calendar calendar = Calendar.getInstance();
        int createTimeLastRun = calendar.get(Calendar.DAY_OF_YEAR) - 1;
        int createTimeHour;
        int createTimeMin;
        // parse the create time
        try {
            createTimeHour = Integer.parseInt(createTime.substring(0, createTime.indexOf(':')));
            createTimeMin = Integer.parseInt(createTime.substring(createTime.indexOf(':') + 1));
            if (log.isInfoEnabled()) {
                log.info("Create time set to " + createTimeHour + ":" + createTimeMin);
            }
        } catch (Throwable t) {
            log.error("Error parsing create time. Using 02:00.", t);
            createTimeHour = 2;
            createTimeMin = 0;
        }
        
        int daysAheadInt;
        // parse the days ahead
        try {
            daysAheadInt = Integer.parseInt(daysAhead);
        } catch (Throwable t) {
            log.error("Error parsing the days ahead. Using 0.", t);
            daysAheadInt = 0;
        }
        
        // init Golden Minutes
        // minutes BEFORE expiry to start qualification of gm investments
        int goldenMinStart;
        int goldenMinEnd;

        int goldenMin2Start;
        int goldenMin2End;
        
        try {
            HashMap<String, Integer> a = InvestmentsManagerBase.getInsuranceTime();
            goldenMinStart	= a.get("insurance_period_start_time");
            goldenMinEnd	= a.get("insurance_period_end_time");
            goldenMin2Start	= a.get("insurance_period_2_start_time");
            goldenMin2End	= a.get("insurance_period_2_end_time");
            
            log.info("loaded start and end time for insurance. start " + goldenMinStart + " , end " + goldenMinEnd);
            
            if(goldenMinEnd > goldenMin2Start ) {
            	log.error("golden minute periods are overlapping !");
            }
        } catch (SQLException e) {
            log.info("cant load start and end time for insurance. setting defaults to start 30, end 25", e);
            goldenMinStart	= 30;
            goldenMinEnd	= 25;
            goldenMin2Start	= 5;
            goldenMin2End	= 4;
        }
        
        running = true;

        int lastRun = calendar.get(Calendar.MINUTE - 1);
        // try to make the ticks to be at XX:XX:00:000
        try {
            calendar.setTimeInMillis(System.currentTimeMillis());
            Thread.sleep(1000 - calendar.get(Calendar.MILLISECOND));
        } catch (Throwable t) {
        }
        
        lastTickTime = System.currentTimeMillis();
        long crrTickTime = 0;

        if (log.isInfoEnabled()) {
            log.info("Scheduler started.");
        }
        while (running) {
            crrTickTime = System.currentTimeMillis();
            if (crrTickTime - lastTickTime > 60000) {
                log.warn("There were no scheduler thick for " + (crrTickTime - lastTickTime));
            }
            lastTickTime = crrTickTime;
            calendar = Calendar.getInstance();
            long sleepInterval = 500;
            if (calendar.get(Calendar.MINUTE) != lastRun) { // round minute
                lastRun = calendar.get(Calendar.MINUTE);
                log.debug("Tick for " + lastRun);

                new Thread(new OptionsJob(calendar.getTime(), levelService)).start();
                
                if (calendar.get(Calendar.HOUR_OF_DAY) == createTimeHour &&
                		calendar.get(Calendar.MINUTE) == createTimeMin &&
                		calendar.get(Calendar.DAY_OF_YEAR) != createTimeLastRun) {
                	createTimeLastRun = calendar.get(Calendar.DAY_OF_YEAR);
                	new Thread(new OptionsCreate(levelService, daysAheadInt)).start();
                }

                if (lastRun == extractMinutes(Expiry.AT_FULL.minutes, goldenMinStart )) {
                    new Thread(new GoldenMinutesJob(GoldenMinutePeriod.STANDARD_AT_FULL, goldenMinStart, goldenMinEnd)).start();
                } else if (lastRun == extractMinutes(Expiry.AT_FULL.minutes, goldenMin2Start)) {
                	new Thread(new GoldenMinutesJob(GoldenMinutePeriod.ADDITIONAL_AT_FULL, goldenMin2Start, goldenMin2End)).start();
                }
                
                else if (lastRun == extractMinutes(Expiry.AT_QUARTER.minutes, goldenMinStart)) {
                	new Thread(new GoldenMinutesJob(GoldenMinutePeriod.STANDARD_AT_QUARTER, goldenMinStart, goldenMinEnd)).start();
                } else if(lastRun == extractMinutes(Expiry.AT_QUARTER.minutes, goldenMin2Start)) {
                	new Thread(new GoldenMinutesJob(GoldenMinutePeriod.ADDITIONAL_AT_QUARTER, goldenMin2Start, goldenMin2End)).start();
                }

                else if (lastRun == extractMinutes(Expiry.AT_HALF.minutes, goldenMinStart)) {
                	new Thread(new GoldenMinutesJob(GoldenMinutePeriod.STANDARD_AT_HALF, goldenMinStart, goldenMinEnd)).start();
                } else if(lastRun == extractMinutes(Expiry.AT_HALF.minutes, goldenMin2Start)) {
                	new Thread(new GoldenMinutesJob(GoldenMinutePeriod.ADDITIONAL_AT_HALF, goldenMin2Start, goldenMin2End)).start();
                }

                else if (lastRun == extractMinutes(Expiry.AT_THREE_QUARTERS.minutes, goldenMinStart)) {
                	new Thread(new GoldenMinutesJob(GoldenMinutePeriod.STANDARD_AT_THREEQUARTERS, goldenMinStart, goldenMinEnd)).start();
                } else if (lastRun == extractMinutes(Expiry.AT_THREE_QUARTERS.minutes, goldenMin2Start)) {
                	new Thread(new GoldenMinutesJob(GoldenMinutePeriod.ADDITIONAL_AT_THREEQUARTERS, goldenMin2Start, goldenMin2End)).start();
                }
            } else {
            	calendar.set(Calendar.SECOND, 0);
            	calendar.set(Calendar.MILLISECOND, 0);
            	calendar.add(Calendar.MINUTE, 1);
            	sleepInterval = calendar.getTimeInMillis() - System.currentTimeMillis();
            }
            
            try {
                Thread.sleep(sleepInterval);
            } catch (Throwable t) {
                log.error("Opa.", t);
            }
        }
        log.warn("Scheduler done.");
    }
    
    public static int extractMinutes(int currentMinutes, int minutesToExtract) {
    	int result = currentMinutes - minutesToExtract;
    	while(result < 0 ){
    		result = 60 + result;
    	}
    	return result;
    }
    
    public void stopSheduler() {
        if (log.isInfoEnabled()) {
            log.info("Stopping scheduler.");
        }
        running = false;
    }
    
    public boolean isWorking() {
        return System.currentTimeMillis() - lastTickTime < 2000;
    }
}