package il.co.etrader.service.level;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.LiveJMSMessage;
import com.anyoption.common.beans.Opportunity;

import il.co.etrader.bl_managers.ConstantInvestmentsStreamManagerBase;
import il.co.etrader.bl_vos.ConstantInvestmentsStream;

public class LiveInvestments implements LiveInvestmentListener {
	private static final Logger log = Logger.getLogger(LiveInvestments.class);
	
	private SenderThread sender;
	private Map<ConstantInvestmentsStream, List<LiveMessageWrapper>> streamsCache;
	private AtomicLong investmentKeySequence;
	
	public LiveInvestments(SenderThread sender) throws SQLException {
		log.info("Initializing live investments stream...");
		this.sender = sender;
		
		List<ConstantInvestmentsStream> streamList = ConstantInvestmentsStreamManagerBase.getActive();
		log.debug("Loaded investment streams [" + streamList + "]");
		streamsCache = new LinkedHashMap<ConstantInvestmentsStream, List<LiveMessageWrapper>>();
		for (ConstantInvestmentsStream streamDefinition : streamList) {
			streamsCache.put(streamDefinition, new LinkedList<LiveMessageWrapper>());
		}
		
		investmentKeySequence = new AtomicLong(0L);
		log.info("Initializing live investments stream done");
	}
	
	public void investment(long oppId,
							double invId,
							double amount,
							long type,
							String amountForDisplay,
							String city,
							String countryId,
							String marketId,
							String productTypeId,
							long skinId,
							String level,
							Date time,
							String globeLat,
							String globeLong,
							Opportunity opp,
							long flooredAmount) {
		
		String strType = String.valueOf(type);
		String strTime = String.valueOf(time.getTime());
		String strInvKey = String.valueOf(investmentKeySequence.incrementAndGet());
		String strOpportunityId = Long.toString(oppId);
		ConstantInvestmentsStream streamDef;
		List<LiveMessageWrapper> streamCache;
		LiveJMSMessage msgToAdd;
		LiveMessageWrapper msgWrapperToRemove;
		for (Map.Entry<ConstantInvestmentsStream, List<LiveMessageWrapper>> streamEntry : streamsCache.entrySet()) {
			streamDef = streamEntry.getKey();
			if (amount >= streamDef.getInvestmentMinAmount()
					&& (streamDef.getSkinId() == null || streamDef.getSkinId() == skinId)) {
				log.trace("Creating investment for live stream [" + streamDef.getStreamName()
															+ "] with amount in cents [" + amount
															+ "], seqience id [" + strInvKey + "]");
				msgToAdd = new LiveJMSMessage();
				msgToAdd.fields = new HashMap<String, String>();
				msgToAdd.fields.put("key", streamDef.getStreamName());
				msgToAdd.fields.put("command", "UPDATE");
				msgToAdd.fields.put("LI_CITY", city);
				msgToAdd.fields.put("LI_COUNTRY", countryId);
				msgToAdd.fields.put("LI_ASSET", marketId);
				msgToAdd.fields.put("LI_OPP_ID", strOpportunityId);
				msgToAdd.fields.put("LI_PROD_TYPE", productTypeId);
				msgToAdd.fields.put("LI_INV_TYPE", strType);
				msgToAdd.fields.put("LI_LEVEL", level);
				msgToAdd.fields.put("LI_AMOUNT", amountForDisplay);
				msgToAdd.fields.put("LI_TIME", strTime);
				msgToAdd.fields.put("LI_INV_KEY", strInvKey);
				msgToAdd.fields.put("LI_CONVERTED_AMOUNT", Long.toString(flooredAmount));
				sender.send(msgToAdd);
				
				LiveMessageWrapper msgWrapperToAdd = new LiveMessageWrapper();
				msgWrapperToAdd.setAmount(flooredAmount);
				msgWrapperToAdd.setMarketId(Long.parseLong(marketId));
				msgWrapperToAdd.setOpportunityId(oppId);
				msgWrapperToAdd.setTime(time);
				msgWrapperToAdd.setMessage(msgToAdd);
				
				streamCache = streamEntry.getValue();
				synchronized (streamCache) {
					streamCache.add(0, msgWrapperToAdd);
					
					if (streamCache.size() > streamDef.getMaxRows()) {
						// remove last value
						msgWrapperToRemove = streamCache.remove(streamCache.size() - 1);
						msgWrapperToRemove.getMessage().fields.put("command", "DELETE");
						log.trace("Deleting investment from live stream [" + streamDef.getStreamName()
									+ "] with amount in cents [" + msgWrapperToRemove.getAmount()
									+ "], seqience id [" +  msgWrapperToRemove.getMessage().fields.get("LI_INV_KEY") + "]");
						sender.send(msgWrapperToRemove.getMessage());
					}
				}
			}
		}
	}
	
	public void sendSnapshot() {
		log.debug("Sending live investments streams snapshot...");
		for (List<LiveMessageWrapper> msgList : streamsCache.values()) {
			synchronized (msgList) {
				for (LiveMessageWrapper msg : msgList) {
					sender.send(msg.getMessage());
				}
			}
		}
		log.debug("Sending live investments streams snapshot done");
	}
	
	public void removeOpportunity(long oppId) {
		log.trace("Removing live investments for oppotunity [" + oppId + "] ...");
		List<LiveMessageWrapper> streamCache;
		LiveMessageWrapper msgWrapper;
		for (Map.Entry<ConstantInvestmentsStream, List<LiveMessageWrapper>> streamEntry : streamsCache.entrySet()) {
			streamCache = streamEntry.getValue();
			synchronized (streamCache) {
				Iterator<LiveMessageWrapper> iterator = streamCache.iterator();
				while (iterator.hasNext()) {
					msgWrapper = iterator.next();
					if (msgWrapper.getOpportunityId() == oppId) {
						iterator.remove();
						msgWrapper.getMessage().fields.put("command", "DELETE");
						sender.send(msgWrapper.getMessage());
						log.trace("Removed investment [" + msgWrapper + "] from live investments");
					}
				}
			}
		}
		log.trace("Removing live investments for oppotunity [" + oppId + "] done");
	}
	
	public void removeMarket(long marketId) {
		log.trace("Removing live investments for market [" + marketId + "] ...");
		List<LiveMessageWrapper> streamCache;
		LiveMessageWrapper msgWrapper;
		for (Map.Entry<ConstantInvestmentsStream, List<LiveMessageWrapper>> streamEntry : streamsCache.entrySet()) {
			streamCache = streamEntry.getValue();
			synchronized (streamCache) {
				Iterator<LiveMessageWrapper> iterator = streamCache.iterator();
				while (iterator.hasNext()) {
					msgWrapper = iterator.next();
					if (msgWrapper.getMarketId() == marketId) {
						iterator.remove();
						msgWrapper.getMessage().fields.put("command", "DELETE");
						sender.send(msgWrapper.getMessage());
						log.trace("Removed investment [" + msgWrapper + "] from live investments");
					}
				}
			}
		}
		log.trace("Removing live investments for market [" + marketId + "] done");
	}
	
}