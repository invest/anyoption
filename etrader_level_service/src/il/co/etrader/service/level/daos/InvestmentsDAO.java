package il.co.etrader.service.level.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.daos.DAOBase;

public class InvestmentsDAO extends DAOBase {
    public static void qualifyInvestmentsForInsurances(Connection conn, int goldenMinStart, int goldenMinEnd) throws SQLException {
        CallableStatement cstmt = null;
        try {
            cstmt = conn.prepareCall("BEGIN QUALIFY_INV_FOR_INSURANCES(?, ?); END;");
            cstmt.setString(1, goldenMinStart < 10 ? "0" + goldenMinStart : String.valueOf(goldenMinStart));
            cstmt.setString(2, goldenMinEnd < 10 ? "0" + goldenMinEnd : String.valueOf(goldenMinEnd));
            cstmt.execute();
        } finally {
            closeStatement(cstmt);
        }
    }

    public static void qualifyInvestmentsForAdditionalInsurance(Connection conn, int goldenMinStart, int goldenMinEnd) throws SQLException {
        CallableStatement cstmt = null;
        try {
            cstmt = conn.prepareCall("BEGIN QUALIFY_INV_FOR_ADD_INSURANCES(?, ?); END;");
            cstmt.setString(1, goldenMinStart < 10 ? "0" + goldenMinStart : String.valueOf(goldenMinStart));
            cstmt.setString(2, goldenMinEnd < 10 ? "0" + goldenMinEnd : String.valueOf(goldenMinEnd));
            cstmt.execute();
        } finally {
            closeStatement(cstmt);
        }
    }
    
}
