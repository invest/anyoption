package il.co.etrader.service.level.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.MarketConfig;
import com.anyoption.common.beans.MarketRate;
import com.anyoption.common.daos.DAOBase;

/**
 * Service markets DAO.
 *
 * @author Tony
 */
public class MarketsDAO extends DAOBase {
	
	protected static Logger log = Logger.getLogger(MarketsDAO.class);
	
    /**
     * Insert market rate record in the db.
     *
     * @param conn
     * @param marketRates
     * @return set of market IDs 
     * @throws SQLException
     */
    public static Set<Long> insertMarketRates(Connection conn, List<MarketRate> marketRates) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                "INSERT INTO market_rates " +
                    "(id, market_id, rate, rate_time, graph_rate, day_rate, SKIN_GROUP_ID, last, ask, bid) " +
                "VALUES " +
                    "(SEQ_MARKET_RATES.NEXTVAL, ?, ?, to_date(?, 'yyyy-mm-dd hh24:mi:ss'), ?, ?, ?, ?, ?, ?) ";
            pstmt = conn.prepareStatement(sql);
            Set<Long> marketIdSet = new LinkedHashSet<Long>();
            for (MarketRate marketRate : marketRates) {
            	pstmt.setLong(1, marketRate.getMarketId());
            	pstmt.setBigDecimal(2, marketRate.getRate());
            	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            	pstmt.setString(3, sdf.format(new Date()));
            	pstmt.setBigDecimal(4, marketRate.getGraphRate());
            	pstmt.setBigDecimal(5, marketRate.getDayRate());
            	pstmt.setLong(6, marketRate.getSkinGroup().getId());
            	if (marketRate.getLast() == null) {
            		pstmt.setNull(7, Types.DECIMAL);
            	} else {
            		pstmt.setBigDecimal(7, marketRate.getLast());
            	}
            	if (marketRate.getAsk() == null) {
            		pstmt.setNull(8, Types.DECIMAL);
            	} else {
            		pstmt.setBigDecimal(8, marketRate.getAsk());
            	}
            	if (marketRate.getBid() == null) {
            		pstmt.setNull(9, Types.DECIMAL);
            	} else {
            		pstmt.setBigDecimal(9, marketRate.getBid());
            	}
				pstmt.addBatch();
				
				marketIdSet.add(marketRate.getMarketId());
			}
            
            pstmt.executeBatch();
            return marketIdSet;
        } finally {
            closeStatement(pstmt);
        }
    }

    /**
     * Clean certain market rates that are older than hour and a half.
     *
     * @param conn
     * @param marketIds
     * @throws SQLException
     */
    public static void deleteMarketRates(Connection conn, Set<Long> marketIds) throws SQLException {
    	if (marketIds == null || marketIds.size() == 0) {
    		log.debug("No market rates to delete");
    		return;
    	}
    	
        PreparedStatement pstmt = null;
        try {
        	String sql = " DELETE FROM market_rates WHERE market_id IN (%s) AND rate_time < current_date - 3/48 "; // hour and a half
        	StringBuilder sb = new StringBuilder();
        	Iterator<Long> iter = marketIds.iterator();
        	if (iter.hasNext()) {
        		iter.next();
        		sb.append(" ? ");
        		while (iter.hasNext()) {
        			iter.next();
        			sb.append(" , ? ");
				}
        	}
        	
            pstmt = conn.prepareStatement(String.format(sql, sb.toString()));
            
            int idx = 1;
            for (Long marketId : marketIds) {
            	pstmt.setLong(idx++, marketId);
            }
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    /**
     * Delete all records from MARKET_RATES table.
     *
     * @param conn
     * @throws SQLException
     */
    public static void deleteMarketsRates(Connection conn) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            /*
             * The records will be cleaned everyday.
             * So in the morning all the records in the table should be from the previous day.
             * Thus deleting all should be like cleaning yesterdays rates.
             */
            String sql =
                "DELETE FROM " +
                    "market_rates";
            pstmt = conn.prepareStatement(sql);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

	public static HashMap<Integer, HashMap<String, Integer>> getSkinsPrioritiesById(Connection conn, long marketId) throws SQLException {
		HashMap<Integer, HashMap<String, Integer>> itemSkins = new HashMap<Integer, HashMap<String, Integer>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql =
                "SELECT " +
					"A.skin_id, " +
					"B.group_priority, " +
					"B.home_page_priority " +
				 "FROM " +
                    "skin_market_groups A, " +
				 	"skin_market_group_markets B " +
				 "WHERE " +
				 	"A.id = B.skin_market_group_id AND " +
				 	"B.market_id = ?";
			ps = conn.prepareStatement(sql);
            ps.setLong(1, marketId);
			rs = ps.executeQuery();
			while (rs.next()) {
				HashMap<String, Integer> skin = new HashMap<String, Integer>();
				skin.put("group_priority", rs.getInt("group_priority"));
				skin.put("home_page_priority", rs.getInt("home_page_priority"));
				itemSkins.put(rs.getInt("skin_id"), skin);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return itemSkins;
	}
	
	public static Map<Long, String> getLiveGlobeFakeInvestmentMarkets(Connection conn) throws SQLException {
		  Map<Long, String> marketMap = new LinkedHashMap<Long, String>();
		  PreparedStatement pstmt = null;
	      ResultSet rs = null;
	      String sql = " SELECT id, feed_name " +
			    		  " FROM markets " +
			    		  " WHERE (use_for_fake_investments IS NULL " +
			    		  			" OR use_for_fake_investments = 1) " +
			    		  " AND (id = 3 " + // TA25 market
			    		  /* ^ TODO Temporary fix for market 3 (TV 25) Filtering logic should be refactored in next releases */
			    		  			" OR exchange_id NOT IN (1, 9)) " +
			    		  " ORDER BY id ";
		  try {
			  pstmt = conn.prepareStatement(sql);
			  rs = pstmt.executeQuery();
			  while (rs.next()) {
				  marketMap.put(rs.getLong("id"), rs.getString("feed_name"));
			  }
			  
			  return marketMap;
		  } finally {
			  closeResultSet(rs);
	          closeStatement(pstmt);
		  }
	  }

	public static MarketConfig getMarketConfig(Connection conn, long marketId, long dataServiceId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "select mf1.formula_class as hour_level_formula_class, "
								+ "mf2.formula_class as hour_closing_formula_class, "
								+ "mf3.formula_class as day_closing_formula_class, "
								+ "mf4.formula_class as long_term_formula_class, "
								+ "mf5.formula_class as real_level_formula_class, "
								+ "a.* "
							+ "from market_data_source_maps a, "
								+ "market_formulas mf1, "
								+ "market_formulas mf2, "
								+ "market_formulas mf3, "
								+ "market_formulas mf4, "
								+ "market_formulas mf5 "
							+ "where mf1.id(+) = a.hour_level_formula_id "
								+ "and mf2.id(+) = a.hour_closing_formula_id "
								+ "and mf3.id(+) = a.day_closing_formula_id "
								+ "and mf4.id(+) = a.long_term_formula_id "
								+ "and mf5.id(+) = a.real_level_formula_id "
								+ "and market_id = ? and market_data_source_id = ?";
			ps = conn.prepareStatement(sql);
			ps.setLong(1, marketId);
			ps.setLong(2, dataServiceId);
			rs = ps.executeQuery();
			if (rs.next()) {
				// set the config
				MarketConfig config = new MarketConfig();
				config.setHourLevelFormulaClass(rs.getString("hour_level_formula_class"));
				config.setHourLevelFormulaParams(rs.getString("hour_level_formula_params"));
				config.setHourClosingFormulaClass(rs.getString("hour_closing_formula_class"));
				config.setHourClosingFormulaParams(rs.getString("hour_closing_formula_params"));
				config.setDayClosingFormulaClass(rs.getString("day_closing_formula_class"));
				config.setDayClosingFormulaParams(rs.getString("day_closing_formula_params"));
				config.setLongTermFormulaClass(rs.getString("long_term_formula_class"));
				config.setLongTermFormulaParams(rs.getString("long_term_formula_params"));
				config.setRealLevelFormulaClass(rs.getString("real_level_formula_class"));
				config.setRealLevelFormulaParams(rs.getString("real_level_formula_params"));
				config.setMarketParams(rs.getString("market_params"));
				config.setMarketDisableConditions(rs.getString("market_disable_conditions"));
				return config;
			}
			
			return null;
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}

	public static long getDataServiceId(Connection conn, String dataServiceClassName) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT id "
							+ "FROM market_data_sources "
							+ "WHERE data_receive_layer_class = ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, dataServiceClassName);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong("id");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return 0l;
	}
}