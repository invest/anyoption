package il.co.etrader.service.level.daos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import il.co.etrader.bl_vos.Skins;
import il.co.etrader.dao_managers.SkinsDAOBase;

/**
 * Service markets DAO.
 *
 * @author Tony
 */
public class SkinsDAO extends SkinsDAOBase {

	/**
	 * Return all skins id
	 * @param con
	 * 		DB connection
	 * @return
	 * 		list of all skins
	 * @throws SQLException
	 */
	public static ArrayList<Skins> getAll(Connection conn) throws SQLException{
		Statement st = null;
		ResultSet rs = null;
		ArrayList<Skins> list = new ArrayList<Skins>();

		try {

			String sql = "select id from skins ";

			st = conn.createStatement();
			rs = st.executeQuery(sql);

			while ( rs.next() ) {
				Skins skin = new Skins();
				skin.setId(rs.getInt("id"));
				list.add(skin);
			}

		} finally {
			closeResultSet(rs);
			closeStatement(st);
		}

		return list;
	}

}