package il.co.etrader.service.level.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.daos.DAOBase;

public class OpportunitiesDAO extends DAOBase {
    public static void setGMLevel(Connection conn, Opportunity o) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                "UPDATE " +
                    "opportunities " +
                "SET " +
                    "gm_level = ? " +
                "WHERE " +
                    "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setDouble(1, o.getGmLevel());
            pstmt.setLong(2, o.getId());
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    public static void setGMLevelAdditional(Connection conn, Opportunity o) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                "UPDATE " +
                    "opportunities " +
                "SET " +
                    "gm_level_add = ? " +
                "WHERE " +
                    "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setDouble(1, o.getGmLevel());
            pstmt.setLong(2, o.getId());
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }
    
	public static void updateOppCurrentLevel(Connection conn, long oppId, double lastCalcResult) throws SQLException {
		PreparedStatement pstmt = null;
        try {
            String sql =
                "UPDATE " +
                    "opportunities " +
                "SET " +
                    "CURRENT_LEVEL = ? " +
                "WHERE " +
                    "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setDouble(1, lastCalcResult);
            pstmt.setLong(2, oppId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
	}
}