package il.co.etrader.service.level;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.managers.BaseBLManager;

/**
 * Common logic.
 * 
 * @author Tony
 */
public class AdminManager extends BaseBLManager {
    /**
     * Add record to the log table.
     * 
     * @param writerId
     * @param table
     * @param key
     * @param command
     * @param desc
     * @throws SQLException
     */
    public static void addToLog(long writerId, String table, long key, int command, String desc) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            GeneralDAO.insertLog(conn, writerId, table, key, command, desc);
        } finally {
            closeConnection(conn);
        }
    }
}