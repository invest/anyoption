package il.co.etrader.service.level;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Exchange;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.MarketConfig;
import com.anyoption.common.beans.MarketRate;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunitySkinGroupMap;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.jms.ConnectionLoop;
import com.anyoption.common.jms.ETraderFeedMessage;
import com.anyoption.common.jms.HeartbeatMessage;
import com.anyoption.common.jms.JMSHandler;
import com.anyoption.common.jms.ifc.ExtendedMessageListener;
import com.anyoption.common.jmx.AmqJMXMonitor;
import com.anyoption.common.jmx.DbJMXMonitor;
import com.anyoption.common.jmx.base.JMXBase;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.managers.DBUtil;
import com.anyoption.common.util.DynamicsUtil;
import com.anyoption.common.util.GoldenMinute.Expiry;
import com.anyoption.service.datasource.DataUpdate;
import com.anyoption.service.datasource.DataUpdatesListener;
import com.anyoption.service.datasource.Service;
import com.anyoption.service.level.formula.Formula;
import com.anyoption.service.level.formula.FormulaConfig;
import com.anyoption.service.level.market.Market.BackendCommand;
import com.anyoption.service.level.market.MarketFactory;
import com.anyoption.service.level.market.UpdateResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import il.co.etrader.bl_managers.ReutersQuotesManagerBase;
import il.co.etrader.bl_vos.ServiceConfig;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.service.level.items.Item;
import il.co.etrader.service.level.managers.MarketsManager;
import il.co.etrader.service.level.managers.SkinsManager;
import il.co.etrader.service.level.options.Option;
import il.co.etrader.util.ConstantsBase;

/**
 * The ETrader levels service.
 *
 * @author Tony
 */
public class LevelService extends JMXBase implements LevelServiceMBean, ExtendedMessageListener, OptionSubscribtionManager, DataUpdatesListener {
    private static final Logger log = Logger.getLogger(LevelService.class);

    /**
     * The maximum number of markets displayed on homepage.
     */
    private static final int MAX_MARKETS_ON_HOMEPAGE = 4;
    /**
     * The maximum number of top markets in a market group.
     */
    private static final int MAX_MARKETS_BY_GROUP = 2;

    public static final String QUOTE_FIELDS_ASK = "ASK";
    public static final String QUOTE_FIELDS_BID = "BID";
    public static final String QUOTE_FIELDS_LAST = "LAST";

    private static LevelService levelService; // so we have reference to the service from the inner classes

    // JMS properties
    private String initialContextFactory;
    private String providerURL;
    private String reutersProviderURL;
    private String connectionFactoryName;
    private String queueName;
    private String topicName;
    private String webTopicName;
    private String reutersQueueName;
    private String reutersTopicName;
    private int msgPoolSize;
    private int recoveryPause;

    private String createTime;
    private String daysAhead;
    private Scheduler scheduler;
    private HeartbeatThread heartbeat;
    private ConnectionLoopTPQR connector;
    private ConnectionLoopQSTS reutersConnector;
    private SenderThread sender;
    private SenderThread webSender;
    private SenderThread reutersSender;
    private Settlement settlement;
    private EmailThread emailSender;

    private boolean internalDataService;
    private String dataServiceClassName;
    private String dataServiceAttributes;
    private Service dataService;
    // Reuters configuration
    private int reutersSubscribeRetryPeriod;
    private long dataServiceId;

    private MonitoringThread monitor;
    private RatesJob ratesJob;

    // A id that represents the life of this generator. Is sent within the
    // heartbeat to let Lightstreamer distinguish between 2 different lives of the Generator.
    // We should have two types of changes of the lifeId - initial value and increase after reconnection.
    // It should be safe enough to assume that putting init value the current system time and increase with
    // one on reconnect should give us unique value of lifeId (we assume we will have less than one reconnect
    // per millis)
    private long lifeId;

    //this object handles comunications with JMS. Hides the use of Session, Connections, Publishers etc..
    private JMSHandler jmsHandler;
    private JMSHandler webJmsHandler;
    private JMSHandler reutersJmsHandler;

    // levels calculation parameters (config)
    private static double ta25Parameter = 0;
    private static BigDecimal ftseParameter = new BigDecimal(0);
    private static BigDecimal cacParameter = new BigDecimal(0);
    private static BigDecimal sp500Parameter = new BigDecimal(0);
    private static BigDecimal klseParameter = new BigDecimal(0);

	private static BigDecimal d4BanksParameter = new BigDecimal(0);
	private static BigDecimal banksBursaParameter = new BigDecimal(0);
	private static BigDecimal d4SpreadParameter = new BigDecimal(0);

    // Subscription states
    private HashMap<String, Item> items = new HashMap<String, Item>();
    private HashMap<String, Option> options = new HashMap<String, Option>();
    private HashMap<String, List<com.anyoption.service.level.market.Market>> fieldMarketsMap = new HashMap<String, List<com.anyoption.service.level.market.Market>>();
    private HashMap<String, com.anyoption.service.level.market.Market> markets = new HashMap<String, com.anyoption.service.level.market.Market>();


    private boolean oppsLoaded;

    private String emailServer;
    private boolean emailServerAuth;
    private String emailUser;
    private String emailPass;
    private String emailFrom;
    private String emailRcpt;
    private String emailSubject;
    private String reportEmailRcpt;
    private String reportEmailSubject;
    private String reportEmailSubjectOneTouch;
    private String nonSettlementEmailSubject;

    private int alertAfterXMinNotClosed;
    private int closeAfterXSecNotClosed;
    private int ta25AskBidIgnoreSpread;
    private int ilsAskBidIgnoreSpread;

    private ArrayList<Skins> skins = null;
    private Hashtable<Long, Boolean> openedMarkets = null;
    private String openedMarketsStr;

    public static final long SNAPSHOT_THREAD_SLEEPING_TIME_DEFUALT = 1000;
    public static final long SNAPSHOT_THREAD_SLEEPING_TIME_STARTUP = 10000;
    private SendSnapshot sendSnapshotThread;
    private Object sendSnapshotThreadLock;

    public static final int SKIN_ETRADER_INT = 1;

    // Live
    private LiveGlobe liveGlobe;
    private LiveInvestments liveInvestments;
    private LiveTrends liveTrends;

    private AmqJMXMonitor amqJmx;
    private DbJMXMonitor dbMon;

    public LevelService() {
    	super("il.co.etrader.service.level:type=LevelService,name=LevelService", "base");
        levelService = this;
    }


    public String getName() {
        if (log.isDebugEnabled()) {
            log.debug("LevelService ========== getName");
        }
        return "il.co.etrader.service.level:service=LevelService";
    }

    protected void createService() throws Exception {
        if (log.isInfoEnabled()) {
            log.info("LevelService ========== createService msgPoolSize: " + msgPoolSize);
        }

        lifeId = System.currentTimeMillis();
        oppsLoaded = false;
    }
    
    public void start() throws Exception {
    	startService();
    }
    
//    protected void startService() throws Exception {
//    @Override
	public void startService() {
        try {
            if (log.isInfoEnabled()) {
                log.info("LevelService ========== startService begin");
            }
            createService();

            CurrencyRatesManagerBase.loadInvestmentRartesCache();

            try {
                skins = SkinsManager.getAllSkins();
                if (log.isTraceEnabled()) {
                    log.trace("Skins: " + skins);
                }
            } catch (Exception e) {
            	log.error("Cant load skins.", e);
            }

            // JMS startup
            //instantiate a JMSHandler that will handle LightStreamer updates sending and commands Queue receiving
            jmsHandler = new JMSHandler("LSQ", initialContextFactory, providerURL, connectionFactoryName, queueName, connectionFactoryName, topicName);
            //The service will be the JMS listener
            jmsHandler.setListener(this);

            webJmsHandler = new JMSHandler("WEB", initialContextFactory, providerURL, null, null, connectionFactoryName, webTopicName);
            webJmsHandler.setListener(this);

            //start the loop that tries to connect to JMS
            connector = new ConnectionLoopTPQR(jmsHandler, webJmsHandler, recoveryPause);
            connector.start();

            sender = new SenderThread(jmsHandler, false);
            sender.start();

            webSender = new SenderThread(webJmsHandler, false);
            webSender.start();

            emailSender = new EmailThread(emailServer, emailServerAuth, emailUser, emailPass, emailFrom);
            emailSender.start();

            heartbeat = new HeartbeatThread();
            heartbeat.start();

            dataServiceId = MarketsManager.getDataServiceId(dataServiceClassName);
            if (internalDataService) {
            	dataService = (Service) Class.forName(dataServiceClassName).getConstructor(String.class, boolean.class, DataUpdatesListener.class, long.class).newInstance(dataServiceAttributes, true, this, dataServiceId);
            	dataService.startService();
            } else {
            	reutersJmsHandler = new JMSHandler("RTS", initialContextFactory, reutersProviderURL, connectionFactoryName, reutersQueueName, connectionFactoryName, reutersTopicName);
            	reutersJmsHandler.setListener(this);

            	reutersConnector = new ConnectionLoopQSTS(reutersJmsHandler, recoveryPause);
            	reutersConnector.start();

            	reutersSender = new SenderThread(reutersJmsHandler, true);
            	reutersSender.start();
            }

            loadDBConfig();

            sendSnapshotThreadLock = new Object();
            synchronized (sendSnapshotThreadLock) {
                sendSnapshotThread = null;
            }

            openedMarkets = new Hashtable<Long, Boolean>();
            loadOpportunities();
            scheduler = new Scheduler(createTime, daysAhead, this);
            new Thread(scheduler).start();

            if (log.isInfoEnabled()) {
                log.info("Level service started.");
            }

            settlement = new Settlement(sender);
            settlement.start();

            monitor = new MonitoringThread();
            monitor.start();

            ratesJob = new RatesJob();
            ratesJob.start();


            // TODO market??
            // Live
            liveGlobe = new LiveGlobe(sender, markets, openedMarkets);
            liveInvestments = new LiveInvestments(sender);
            liveTrends = new LiveTrends(sender);

            // in case the service crashed when we had running closing threads close opps that need to be closed
            // TODO: close opps that need to be closed (if service crashed with running closing threads)
            if (log.isInfoEnabled()) {
                log.info("LevelService ========== startService end");
            }
        } catch (Exception e) {
            log.fatal("Can't start service.", e);
        }
        
		try {
			dbMon = new DbJMXMonitor(DBUtil.getDataSource(), "LevelService");
			dbMon.registerJMX();
		} catch (SQLException e) {
			log.error("Cant start DB monitor", e);
		}
		
        amqJmx = new AmqJMXMonitor("LevelService");
        AmqJMXMonitor.initConnectionFactory();
        amqJmx.registerJMX();
        
        registerJMX();
    }

    public void stop() throws Exception {
    	stopService();
    }

//    protected void stopService() throws Exception {
//    @Override
	public void stopService() {
        if (log.isInfoEnabled()) {
            log.info("LevelService ========== stopService begin");
        }

        if (internalDataService) {
	        if (null != dataService) {
	        	dataService.stopService();
	        	dataService = null;
	        }
        } else {
        	unsubscribeItems();
        }

        // Live
        liveTrends = null;
        liveInvestments = null;

        liveGlobe.stop();
        liveGlobe = null;

        // Stop helper threads
        ratesJob.stopRatesJob();
        ratesJob = null;
        scheduler.stopSheduler();
        scheduler = null;
        heartbeat.stopHeartbeat();
        heartbeat = null;
        if (null != settlement) {
            settlement.stopSettlementThread();
            settlement = null;
        }
        if (null != monitor) {
            monitor.stopMonitoringThread();
            monitor = null;
        }
        if (null != sender) {
            sender.stopSenderThread();
            sender = null;
        }
        if (null != webSender) {
            webSender.stopSenderThread();
            webSender = null;
        }
        if (null != reutersSender) {
        	reutersSender.stopSenderThread();
        	reutersSender = null;
        }
        if (null != emailSender) {
            emailSender.stopEmailThread();
            emailSender = null;
        }
        if (null != connector) {
            connector.abort();
            connector = null;
        }

        if (null != reutersConnector) {
        	reutersConnector.abort();
        	reutersConnector = null;
        }

        // Disconnect JMS
        jmsHandler.close();
        jmsHandler = null;
        webJmsHandler.close();
        webJmsHandler = null;
        if (null != reutersJmsHandler) {
        	reutersJmsHandler.close();
        	reutersJmsHandler = null;
        }

//        options.clear();
//        items.clear();
        synchronized (items) {
            items = new HashMap<String, Item>();
            options = new HashMap<String, Option>();
            markets = new HashMap<String, com.anyoption.service.level.market.Market>();
        }

        oppsLoaded = false;
        synchronized (sendSnapshotThreadLock) {
            sendSnapshotThread = null;
        }

        amqJmx.unregisterJMX();
        dbMon.unregisterJMX();
        unregisterJMX();
        
        if (log.isInfoEnabled()) {
            log.info("LevelService ========== stopService end");
        }
    }

    protected void destroyService() throws Exception {
        if (log.isInfoEnabled()) {
            log.info("LevelService ========== destroyService");
        }
    }

    public static LevelService getInstance() {
        return levelService;
    }

    /**
     * @return the initialContextFactory
     */
    @Override
	public String getInitialContextFactory() {
        return initialContextFactory;
    }

    /**
     * @param initialContextFactory the initialContextFactory to set
     */
    @Override
	public void setInitialContextFactory(String initialContextFactory) {
        this.initialContextFactory = initialContextFactory;
    }

    /**
     * @return the providerURL
     */
    @Override
	public String getProviderURL() {
        return providerURL;
    }

    /**
     * @param providerURL the providerURL to set
     */
    @Override
	public void setProviderURL(String providerURL) {
        this.providerURL = providerURL;
    }

    /**
     * @return the connectionFactoryName
     */
    @Override
	public String getConnectionFactoryName() {
        return connectionFactoryName;
    }

    /**
     * @param connectionFactoryName the connectionFactoryName to set
     */
    @Override
	public void setConnectionFactoryName(String connectionFactoryName) {
        this.connectionFactoryName = connectionFactoryName;
    }

    /**
     * @return the queueName
     */
    @Override
	public String getQueueName() {
        return queueName;
    }

    /**
     * @param queueName the queueName to set
     */
    @Override
	public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    /**
     * @return the topicName
     */
    @Override
	public String getTopicName() {
        return topicName;
    }

    /**
     * @param topicName the topicName to set
     */
    @Override
	public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    /**
     * @return the webTopicName
     */
    @Override
	public String getWebTopicName() {
        return webTopicName;
    }

    /**
     * @param webTopicName the webTopicName to set
     */
    @Override
	public void setWebTopicName(String webTopicName) {
        this.webTopicName = webTopicName;
    }

    /**
     * @return the msgPoolSize
     */
    @Override
	public Integer getMsgPoolSize() {
        return msgPoolSize;
    }

    /**
     * @param msgPoolSize the msgPoolSize to set
     */
    @Override
	public void setMsgPoolSize(Integer msgPoolSize) {
    	if (msgPoolSize != null) {
    		this.msgPoolSize = msgPoolSize;
    	}
    }

    /**
     * @return the recoveryPause
     */
    @Override
	public Integer getRecoveryPause() {
        return recoveryPause;
    }

    /**
     * @param recoveryPause the recoveryPause to set
     */
    @Override
	public void setRecoveryPause(Integer recoveryPause) {
    	if (recoveryPause != null) {
    		this.recoveryPause = recoveryPause;
    	}
    }

    @Override
	public Boolean isInternalDataService() {
		return internalDataService;
	}

	@Override
	public void setInternalDataService(Boolean internalDataService) {
		if (internalDataService != null) {
			this.internalDataService = internalDataService;
		}
	}

	@Override
	public String getReutersProviderURL() {
		return reutersProviderURL;
	}

	@Override
	public void setReutersProviderURL(String reutersProviderURL) {
		this.reutersProviderURL = reutersProviderURL;
	}

	@Override
	public String getReutersQueueName() {
		return reutersQueueName;
	}

	@Override
	public void setReutersQueueName(String reutersQueueName) {
		this.reutersQueueName = reutersQueueName;
	}

	@Override
	public String getReutersTopicName() {
		return reutersTopicName;
	}

	@Override
	public void setReutersTopicName(String reutersTopicName) {
		this.reutersTopicName = reutersTopicName;
	}

    @Override
	public String getCreateTime() {
        return createTime;
    }

    @Override
	public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    @Override
	public String getDaysAhead() {
        return daysAhead;
    }

    @Override
	public void setDaysAhead(String daysAhead) {
        this.daysAhead = daysAhead;
    }

    public static double getTa25Parameter() {
        return ta25Parameter;
    }

    public static void setTa25Parameter(double ta25Parameter) {
        LevelService.ta25Parameter = ta25Parameter;
    }

    public static BigDecimal getCacParameter() {
        return cacParameter;
    }

    public static void setCacParameter(BigDecimal cacParameter) {
        LevelService.cacParameter = cacParameter;
    }

	public static BigDecimal getSp500Parameter() {
		return sp500Parameter;
	}

	public static void setSp500Parameter(BigDecimal sp500Parameter) {
		LevelService.sp500Parameter = sp500Parameter;
	}

    /**
	 * @return the klseParameter
	 */
	public static BigDecimal getKlseParameter() {
		return klseParameter;
	}

	/**
	 * @param klseParameter the klseParameter to set
	 */
	public static void setKlseParameter(BigDecimal klseParameter) {
		LevelService.klseParameter = klseParameter;
	}

	public static BigDecimal getFtseParameter() {
        return ftseParameter;
    }

    public static void setFtseParameter(BigDecimal ftseParameter) {
        LevelService.ftseParameter = ftseParameter;
    }

    /**
	 * @return the banksBursaParameter
	 */
	public static BigDecimal getBanksBursaParameter() {
		return banksBursaParameter;
	}

	/**
	 * @param banksBursaParameter the banksBursaParameter to set
	 */
	public static void setBanksBursaParameter(BigDecimal banksBursaParameter) {
		LevelService.banksBursaParameter = banksBursaParameter;
	}

	/**
	 * @return the d4BanksParameter
	 */
	public static BigDecimal getD4BanksParameter() {
		return d4BanksParameter;
	}

	/**
	 * @param banksParameter the d4BanksParameter to set
	 */
	public static void setD4BanksParameter(BigDecimal banksParameter) {
		d4BanksParameter = banksParameter;
	}

	/**
	 * @return the d4SpreadParameter
	 */
	public static BigDecimal getD4SpreadParameter() {
		return d4SpreadParameter;
	}

	/**
	 * @param spreadParameter the d4SpreadParameter to set
	 */
	public static void setD4SpreadParameter(BigDecimal spreadParameter) {
		d4SpreadParameter = spreadParameter;
	}

	@Override
	public String getEmailSubject() {
        return emailSubject;
    }

    @Override
	public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    @Override
	public String getEmailFrom() {
        return emailFrom;
    }

    @Override
	public void setEmailFrom(String emailFrom) {
        this.emailFrom = emailFrom;
    }

    @Override
	public String getEmailPass() {
        return emailPass;
    }

    @Override
	public void setEmailPass(String emailPass) {
        this.emailPass = emailPass;
    }

    @Override
	public String getEmailRcpt() {
        return emailRcpt;
    }

    @Override
	public void setEmailRcpt(String emailRcpt) {
        this.emailRcpt = emailRcpt;
    }

    @Override
	public String getEmailServer() {
        return emailServer;
    }

    @Override
	public void setEmailServer(String emailServer) {
        this.emailServer = emailServer;
    }

    @Override
	public Boolean isEmailServerAuth() {
        return emailServerAuth;
    }

    @Override
	public void setEmailServerAuth(Boolean emailServerAuth) {
    	if (emailServerAuth != null) {
    		this.emailServerAuth = emailServerAuth;
    	}
    }

    @Override
	public String getEmailUser() {
        return emailUser;
    }

    @Override
	public String getReportEmailRcpt() {
        return reportEmailRcpt;
    }

    @Override
	public void setReportEmailRcpt(String reportEmailRcpt) {
        this.reportEmailRcpt = reportEmailRcpt;
    }

    @Override
	public String getReportEmailSubject() {
        return reportEmailSubject;
    }

    @Override
	public void setReportEmailSubject(String reportEmailSubject) {
        this.reportEmailSubject = reportEmailSubject;
    }

    @Override
	public String getReportEmailSubjectOneTouch() {
        return reportEmailSubjectOneTouch;
    }

    @Override
	public void setReportEmailSubjectOneTouch(String reportEmailSubjectOneTouch) {
        this.reportEmailSubjectOneTouch = reportEmailSubjectOneTouch;
    }

    @Override
	public String getNonSettlementEmailSubject() {
    	return nonSettlementEmailSubject;
    }

    @Override
	public void setNonSettlementEmailSubject(String nonSettlementEmailSubject) {
    	this.nonSettlementEmailSubject = nonSettlementEmailSubject;
    }

    @Override
	public void setEmailUser(String emailUser) {
        this.emailUser = emailUser;
    }

    @Override
	public Integer getAlertAfterXMinNotClosed() {
        return alertAfterXMinNotClosed;
    }

    @Override
	public void setAlertAfterXMinNotClosed(Integer alertAfterXMinNotClosed) {
    	if (alertAfterXMinNotClosed != null) {
    		this.alertAfterXMinNotClosed = alertAfterXMinNotClosed;
    	}
    }

    @Override
	public Integer getCloseAfterXSecNotClosed() {
        return closeAfterXSecNotClosed;
    }

    @Override
	public void setCloseAfterXSecNotClosed(Integer closeAfterXSecNotClosed) {
    	if (closeAfterXSecNotClosed != null) {
    		this.closeAfterXSecNotClosed = closeAfterXSecNotClosed;
    	}
    }

    @Override
	public Integer getIlsAskBidIgnoreSpread() {
        return ilsAskBidIgnoreSpread;
    }

    @Override
	public void setIlsAskBidIgnoreSpread(Integer ilsAskBidIgnoreSpread) {
    	if (ilsAskBidIgnoreSpread != null) {
    		this.ilsAskBidIgnoreSpread = ilsAskBidIgnoreSpread;
    	}
    }

    @Override
	public Integer getTa25AskBidIgnoreSpread() {
        return ta25AskBidIgnoreSpread;
    }

    @Override
	public void setTa25AskBidIgnoreSpread(Integer ta25AskBidIgnoreSpread) {
    	if (ta25AskBidIgnoreSpread != null) {
    		this.ta25AskBidIgnoreSpread = ta25AskBidIgnoreSpread;
    	}
    }

    @Override
	public Integer getReutersSubscribeRetryPeriod() {
        return reutersSubscribeRetryPeriod;
    }

    @Override
	public void setReutersSubscribeRetryPeriod(Integer reutersSubscribeRetryPeriod) {
    	if (reutersSubscribeRetryPeriod != null) {
    		this.reutersSubscribeRetryPeriod = reutersSubscribeRetryPeriod;
    	}
    }

    @Override
	public Integer getJMSSenderQueueSize() {
        if (null == sender) {
            return -1;
        }
        return sender.getQueueSize();
    }

    @Override
	public Integer getWebJMSSenderQueueSize() {
        if (null == webSender) {
            return -1;
        }
        return webSender.getQueueSize();
    }

    @Override
	public Integer getDataUpdatesQueueSize() {
        return dataService.getDataUpdatesQueueSize();
    }

    @Override
	public Integer getEmailSenderQueueSize() {
        return emailSender.getQueueSize();
    }

    @Override
	public Boolean isSettlementAlive() {
        return settlement.isWorking();
    }

    @Override
	public Boolean isSchedulerAlive() {
        return scheduler.isWorking();
    }

    @Override
	public Boolean isHeartbeatAlive() {
        return heartbeat.isWorking();
    }

    @Override
	public Boolean isMonitoringAlive() {
        return monitor.isWorking();
    }

    @Override
	public Boolean isRatesJobAlive() {
        return ratesJob.isWorking();
    }

    @Override
	public String getConnectionState() {
//        return dataService.getReutersConnectionState();
    	return null;
    }

    @Override
	public String getConnectionText() {
//        return dataService.getReutersConnectionText();
    	return null;
    }

    public long getLifeId() {
        return lifeId;
    }

    public void setLifeId(long lifeId) {
        this.lifeId = lifeId;
    }

    public SenderThread getSender() {
        return sender;
    }

    public void setSender(SenderThread sender) {
        this.sender = sender;
    }

    /**
     * receive messages from JMSHandler
     */
    @Override
	public void onMessage(Message message) {
        log.debug("LevelService Message received. Processing...");
        try {
        	if (message instanceof TextMessage) {
	            String feedMsg = ((TextMessage) message).getText();
	            if (log.isInfoEnabled()) {
	                log.info("TextMessage received: " + feedMsg);
	            }
	            onTextMessage(feedMsg);
        	} else if (message instanceof ObjectMessage) {
        		Object obj = ((ObjectMessage) message).getObject();
	        	if (obj instanceof DataUpdate) {
//	        		log.info("ReutersUpdate");
	        		update((DataUpdate) obj);
	        	} else if (obj instanceof HeartbeatMessage) {
	        		log.info("HeartbeatMessage");
	        		// TODO: handle Heartbeats
	        	}
        	} else {
        		log.warn("Unknown message: " + message + " of type: " + message.getClass().getCanonicalName());
        	}
        } catch (Exception e) {
            log.error("Error processing JMS message.", e);
        }
    }

    private void onTextMessage(String feedMsg) {
        if (feedMsg.equals("snapshot")) {
            sendSnapshot(SNAPSHOT_THREAD_SLEEPING_TIME_DEFUALT);
            return;
        } else if (feedMsg.startsWith("investment")) {
            handleInvestment(feedMsg);
            return;
        } else if (feedMsg.startsWith("marketcfg")) {
            handleMarketCfgUpdate(feedMsg);
            return;
        } else if (feedMsg.startsWith("ta25")) {
            handleTA25Update(feedMsg);
            return;
        } else if (feedMsg.startsWith("ftse")) {
            handleFTSEUpdate(feedMsg);
            return;
        } else if (feedMsg.startsWith("cac")) {
            handleCACUpdate(feedMsg);
            return;
        } else if (feedMsg.startsWith("sp500")) {
        	handleSP500Update(feedMsg);
            return;
        } else if (feedMsg.startsWith("klse")) {
        	handleKLSEUpdate(feedMsg);
            return;
        }else if (feedMsg.startsWith("odds")) {
            handleOddsUpdate(feedMsg);
            return;
        } else if (feedMsg.startsWith("shift")) {
            handleOpportunityShiftParameterUpdate(feedMsg);
            return;
        } else if (feedMsg.startsWith("close")) {
            handleOpportunityManualClose(feedMsg);
            return;
        } else if (feedMsg.startsWith("suspend")) {
            handleMarketSuspend(feedMsg);
            return;
        } else if (feedMsg.startsWith("level")) {
            handleCurrentLevelRequest(feedMsg);
            return;
        } else if (feedMsg.startsWith("d4Banks")) {
        	handleD4BanksUpdate(feedMsg);
            return;
        } else if (feedMsg.startsWith("banksBursa")) {
        	handleBanksBursaUpdate(feedMsg);
            return;
        } else if (feedMsg.startsWith("d4Spread")) {
        	handleD4SpreadUpdate(feedMsg);
            return;
        } else if (feedMsg.startsWith("homepages")) {
            for (Skins skin : skins) {
                webSender.send("aohp_" + skin.getId() + "_" + skin.getCurrentMarketsOnHomePage());
                webSender.send(ConstantsBase.JMS_MARKETS_BY_GROUP_PREF + "_"+ skin.getId() + "_" + skin.getCurrentMarketsByGroup());
            }
            webSender.send("aost_" + openedMarketsStr);
            sendAllOppsUpdate();
            return;
        } else if (feedMsg.startsWith("optionExpiration")) {
            handleOptionsExpirationChange(feedMsg);
            return;
//        } else if (feedMsg.startsWith("binary0100opp")) {
//            handleBinary0100OppUpdate(feedMsg);
//            return;
        } else if (feedMsg.startsWith("dynamicsOpportunityQuoteParams")) {
        	handleDynamicsQuoteParamsUpdate(feedMsg);
        	return;
//        } else if (feedMsg.startsWith("binary0100market")) {
//        	handleBinary0100MarketUpdate(feedMsg);
//            return;
        } else if (feedMsg.startsWith("dynamicsMarketQuoteParams")) {
        	handleDynamicsMarketQuoteParams(feedMsg);
            return;
        } else if (feedMsg.startsWith("updateMaxExposure")) {
        	handleOpportunityUpdateMaxExposure(feedMsg);
            return;
        } else if (feedMsg.startsWith("updateTraderDisable")) {
        	handleOpportunityUpdateTraderDisable(feedMsg);
            return;
        } else if (feedMsg.startsWith("opportunitySuspend")) {
        	handleSuspendOpportunity(feedMsg);
        	return;
        } else if (feedMsg.startsWith("updateMarketPriority")) {
        	handleUpdateMarketPriority(feedMsg);
        	return;
        }

        log.warn("Unknown message. Message: " + feedMsg);
    }

    private void sendBackendCommandResponse(String requestId, boolean ok) {
        webSender.send("cmdresp_" + requestId + "_" + (ok ? "ok" : "nok"));
    }

    private void sendSnapshot(long sleepTime) {
        if (!oppsLoaded) {
            return;
        }

        synchronized (sendSnapshotThreadLock) {
            if ((null == sendSnapshotThread) || (null != sendSnapshotThread && sendSnapshotThread.isSending)) {
                sendSnapshotThread = new SendSnapshot(sleepTime);
                sendSnapshotThread.start();
            }
        }
    }

    private void handleInvestment(String feedMsg) {
        if (log.isTraceEnabled()) {
            log.trace("Processing investment");
        }
        try {
            StringTokenizer st = new StringTokenizer(feedMsg, "_");
            st.nextToken(); // drop the "investment" token
            long oppId = Long.parseLong(st.nextToken());
            double invId = Double.parseDouble(st.nextToken());
            double amount = Double.parseDouble(st.nextToken());
            long type = Long.parseLong(st.nextToken());
            String amountForDisplay = st.nextToken();
            String city = st.nextToken();
            String countryId = st.nextToken();
            String marketId = st.nextToken();
            String productTypeId = st.nextToken();
            long skinId = Long.parseLong(st.nextToken());
            String level = st.nextToken();
            Date time = new Date(Long.parseLong(st.nextToken()));
            String globeLat = st.nextToken();
            String globeLong = st.nextToken();
            double aboveTotal = Double.parseDouble(st.nextToken());
            double belowTotal = Double.parseDouble(st.nextToken());
            long flooredAmount = Long.parseLong(st.nextToken());
            CopyOpInvTypeEnum cpOpInvType =	CopyOpInvTypeEnum.of(Integer.parseInt(st.nextToken()));
//            InvestmentMessage toSend = new InvestmentMessage(oppId, invId, amount, type);
//            sender.send(toSend);

            Opportunity o = null;
            synchronized (items) {
                com.anyoption.service.level.market.Market market = null;
                for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                    market = markets.get(i.next());
                    o = market.getOppById(oppId);
                    if (null != o) {
                    	market.investmentNotification(oppId, invId, amount, type, aboveTotal, belowTotal, skinId);
                    	if (o.isInOppenedState() || market.getSpecialEntry() == o) {
                    		market.sendUpdate(sender, lifeId, o, false, true);
                    	}

                    	handleLiveAOInvestment(oppId,
				                				invId,
				                				amount,
				                				type,
				                				amountForDisplay,
				                				city,
				                				countryId,
				                				marketId,
				                				productTypeId,
				                				skinId,
				                				level,
				                				time,
				                				globeLat,
				                				globeLong,
				                				market.isSuspended(),
				                				o,
				                				flooredAmount,
				                				cpOpInvType);
                        break;
                    }
                }
                if (o == null) {
                	log.warn("Investment is for opportunity [" + oppId + "] that does not exist in level service, skipping it");
                }
            }
        } catch (Exception e) {
            log.error("Can't process invetment notification.", e);
        }
    }

    /**
     * This method updates the Live AO specific investment info. It should be
     * called in synchronized block with monitor <code>private HashMap<String, Item> items = new HashMap<String, Item>();</code>
     * because of the market suspended check.
     *
     * @param oppId
     * @param invId
     * @param amount
     * @param type
     * @param amountForDisplay
     * @param city
     * @param countryId
     * @param marketId
     * @param productTypeId
     * @param skinId
     * @param level
     * @param time
     * @param globeLat
     * @param globeLong
     * @param item
     * @param opportunity
     * @param flooredAmount
     * @param cpOpInvType
     */
    private void handleLiveAOInvestment(long oppId,
										double invId,
										double amount,
										long type,
										String amountForDisplay,
										String city,
										String countryId,
										String marketId,
										String productTypeId,
										long skinId,
										String level,
										Date time,
										String globeLat,
										String globeLong,
										boolean suspended,
										Opportunity opportunity,
										long flooredAmount,
										CopyOpInvTypeEnum cpOpInvType) {
    	if (CopyOpInvTypeEnum.COPY == cpOpInvType) {
    		log.debug("Investment [" + new Double(invId).longValue() + "] is of copyop type [" + cpOpInvType + "]. Skipping it for live page");
			/* !!! WARNING !!! METHOD EXIT POINT !!! */
			return;
    	}
    	if (suspended) {
    		log.debug("Market [" + opportunity.getMarket().getId()
	    				+ "] is suspended, skipping investment from opportunity ["
	    				+ oppId + "] with amount [" + amount + "] USD");
    		/* !!! WARNING !!! METHOD EXIT POINT !!! */
    		return;
    	}

    	if (amount <= 0.0D) {
    		log.debug("Skipped investment with non-positive amount [" + amount
    								+ "] USD from opportunity [" + oppId + "]");
    		/* !!! WARNING !!! METHOD EXIT POINT !!! */
    		return;
    	}

    	boolean isEtraderMarket = false;
    	if ((Exchange.ISRAEL_ID == opportunity.getMarket().getExchangeId()
    				|| Exchange.USD_ILS_ID == opportunity.getMarket().getExchangeId())
				&& Integer.parseInt(marketId) != Market.MARKET_TEL_AVIV_25_ID) {
    		/* TODO Temporary fix for market 3 ^ (TV 25) Filtering logic should be refactored in next releases */
    		log.trace("Skipped investment (for liveGlobe and liveInvestments) with etrader specific market exchange ID ["
	    				+ opportunity.getMarket().getExchangeId() + "], opportunity ["
	    				+ oppId + "], amount [" + amount + "] USD");
    		/* !!! WARNING !!! METHOD EXIT POINT !!! */
    		isEtraderMarket = true;
    	}

    	if (!isEtraderMarket) {
			liveGlobe.investment(oppId,
	                				invId,
	                				amount,
	                				type,
	                				amountForDisplay,
	                				city,
	                				countryId,
	                				marketId,
	                				productTypeId,
	                				skinId,
	                				level,
	                				time,
	                				globeLat,
	                				globeLong,
	                				opportunity,
	                				flooredAmount);
			liveInvestments.investment(oppId,
		                				invId,
		                				amount,
		                				type,
		                				amountForDisplay,
		                				city,
		                				countryId,
		                				marketId,
		                				productTypeId,
		                				skinId,
		                				level,
		                				time,
		                				globeLat,
		                				globeLong,
		                				opportunity,
		                				flooredAmount);
    	}
		liveTrends.investment(oppId,
                				invId,
                				amount,
                				type,
                				amountForDisplay,
                				city,
                				countryId,
                				marketId,
                				productTypeId,
                				skinId,
                				level,
                				time,
                				globeLat,
                				globeLong,
                				opportunity,
                				flooredAmount);
    }

    private void handleMarketCfgUpdate(String feedMsg) {
        try {
            StringTokenizer st = new StringTokenizer(feedMsg, "_");
            st.nextToken(); // drop the "marketcfg" token
            String feedName = st.nextToken();
            int updatesPause = Integer.parseInt(st.nextToken());
            BigDecimal significantPercentage = new BigDecimal(st.nextToken());
            int realUpdatesPause = Integer.parseInt(st.nextToken());
            BigDecimal realSignificantPercentage = new BigDecimal(st.nextToken());
            double randomCeiling = Double.parseDouble(st.nextToken());
            double randomFloor = Double.parseDouble(st.nextToken());
            BigDecimal firstShiftParameter = new BigDecimal(st.nextToken());
            BigDecimal percentageOfAvgInvestment = new BigDecimal(st.nextToken());
            BigDecimal fixedAmountForShifting = new BigDecimal(st.nextToken());
            int typeOfShifting = Integer.parseInt(st.nextToken());
            long disableAfterPeriod = Long.parseLong(st.nextToken());
            String quoteParams = st.nextToken();
            String requestId = st.nextToken();
            boolean ok = true;
            synchronized(items) {
                com.anyoption.service.level.market.Market market = markets.get(feedName);
                if (market != null) {
                	market.setUpdatesPause(updatesPause);
                	market.setSignificantPercentage(significantPercentage);
                	market.setRealUpdatesPause(realUpdatesPause);
					market.setRealSignificantPercentage(realSignificantPercentage);
					market.setRandomCeiling(randomCeiling);
					market.setRandomFloor(randomFloor);
					market.setFirstShiftParameter(firstShiftParameter);
					market.setPercentageOfAverageInvestments(percentageOfAvgInvestment);
					market.setFixedAmountForShifting(fixedAmountForShifting);
					market.setTypeOfShifting(typeOfShifting);
					market.setDisableAfterPeriod(disableAfterPeriod);
					Map<BackendCommand, String> commands = new HashMap<>();
					commands.put(BackendCommand.FXAL_CHANGE, quoteParams);
					market.handleBackendCommands(commands);
                } else {
                    log.warn("Market cfg update for market that does not exist: " + feedName);
                    ok = false;
                }
            }
            sendBackendCommandResponse(requestId, ok);
        } catch (Throwable t) {
            log.error("Can't process Avg Last 30 Day Investment update.", t);
        }
    }

    private void handleTA25Update(String feedMsg) {
        if (log.isTraceEnabled()) {
            log.trace("Processing TA25 parameter update");
        }
        try {
            StringTokenizer st = new StringTokenizer(feedMsg, "_");
            st.nextToken(); // drop the first tooken
            ta25Parameter = Double.parseDouble(st.nextToken());
            String requestId = st.nextToken();
            synchronized (items) {
                // TODO TA25 formula
                com.anyoption.service.level.market.Market market = null;
                for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                	market = markets.get(i.next());
                    if (market.getMarketName().startsWith(".TA25")) {
//                        TelAviv25IndexItem ta25ii = (TelAviv25IndexItem) market;
//                        if (ta25ii.hasOpportunities()) {
//                        	ta25ii.parametherUpdated(sender, lifeId);
//                        }
////                        item.sendUpdate(sender, lifeId, Item.UPDATE_ALL_OPPS, true);
////                        break;
                    }
                }
            }
            sendBackendCommandResponse(requestId, true);
        } catch (Throwable t) {
            log.error("Can't process TA25 parameter update.", t);
        }
    }

    private void handleFTSEUpdate(String feedMsg) {
        if (log.isTraceEnabled()) {
            log.trace("Processing FTSE parameter update");
        }
        try {
            StringTokenizer st = new StringTokenizer(feedMsg, "_");
            st.nextToken(); // drop the first tooken
            ftseParameter = new BigDecimal(st.nextToken());
            String requestId = st.nextToken();
            if (log.isDebugEnabled()) {
                log.debug("New param: " + ftseParameter);
            }
            synchronized (items) {
                // TODO FTSE formula
                com.anyoption.service.level.market.Market market = null;
                for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                    market = markets.get(i.next());
                    if (market.getMarketName().equals(".FTSE")) {
//                        FTSEIndexItem ftseii = (FTSEIndexItem) market;
//                        ftseii.parametherUpdated(sender, lifeId);
//                        if (log.isDebugEnabled()) {
//                            log.debug("Level updated");
//                        }
                        break;
                    }
                }
            }
            sendBackendCommandResponse(requestId, true);
        } catch (Throwable t) {
            log.error("Can't process FTSE parameter update.", t);
        }
    }

    private void handleCACUpdate(String feedMsg) {
        if (log.isTraceEnabled()) {
            log.trace("Processing CAC parameter update");
        }
        try {
            StringTokenizer st = new StringTokenizer(feedMsg, "_");
            st.nextToken(); // drop the first tooken
            cacParameter = new BigDecimal(st.nextToken());
            String requestId = st.nextToken();
            if (log.isDebugEnabled()) {
                log.debug("New param: " + cacParameter);
            }
            synchronized (items) {
                // TODO CAC formula
                com.anyoption.service.level.market.Market market = null;
                for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                	market = markets.get(i.next());
                    if (market.getMarketName().equals(".FCHI")) {
//                        CACIndexItem cacii = (CACIndexItem) market;
//                        cacii.parametherUpdated(sender, lifeId);
//                        if (log.isDebugEnabled()) {
//                            log.debug("Level updated");
//                        }
                        break;
                    }
                }
            }
            sendBackendCommandResponse(requestId, true);
        } catch (Throwable t) {
            log.error("Can't process CAC parameter update.", t);
        }
    }

    private void handleSP500Update(String feedMsg) {
        if (log.isTraceEnabled()) {
            log.trace("Processing SP500 parameter update");
        }
        try {
            StringTokenizer st = new StringTokenizer(feedMsg, "_");
            st.nextToken(); // drop the first tooken
        	sp500Parameter = new BigDecimal(st.nextToken());
            String requestId = st.nextToken();
            if (log.isDebugEnabled()) {
                log.debug("New param: " + sp500Parameter);
            }
            synchronized (items) {
                // TODO sp500 formula
                com.anyoption.service.level.market.Market market = null;
                for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                	market = markets.get(i.next());
                    if (market.getMarketName().equals(".SPX")) {
//                    	SP500AndKLSEIndexItem sp500i = (SP500AndKLSEIndexItem) market;
//                    	sp500i.parametherUpdated(sender, lifeId);
//                        if (log.isDebugEnabled()) {
//                            log.debug("Level updated");
//                        }
                        break;
                    }
                }
            }
            sendBackendCommandResponse(requestId, true);
        } catch (Throwable t) {
            log.error("Can't process SP500 parameter update.", t);
        }
    }

    private void handleKLSEUpdate(String feedMsg) {
        if (log.isTraceEnabled()) {
            log.trace("Processing KLSE parameter update");
        }
        try {
            StringTokenizer st = new StringTokenizer(feedMsg, "_");
            st.nextToken(); // drop the first tooken
            klseParameter = new BigDecimal(st.nextToken());
            String requestId = st.nextToken();
            if (log.isDebugEnabled()) {
                log.debug("New param: " + klseParameter);
            }
            synchronized (items) {
                // TODO sp500 and klse formula
                com.anyoption.service.level.market.Market market = null;
                for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                	market = markets.get(i.next());
                    if (market.getMarketName().equals(".KLSE")) {
//                    	SP500AndKLSEIndexItem klsei = (SP500AndKLSEIndexItem) market;
//                    	klsei.parametherUpdated(sender, lifeId);
//                        if (log.isDebugEnabled()) {
//                            log.debug("Level updated");
//                        }
                        break;
                    }
                }
            }
            sendBackendCommandResponse(requestId, true);
        } catch (Throwable t) {
            log.error("Can't process KLSE parameter update.", t);
        }
    }

    private void handleD4SpreadUpdate(String feedMsg) {
        if (log.isTraceEnabled()) {
            log.trace("Processing D4Spread parameter update");
        }
        try {
            StringTokenizer st = new StringTokenizer(feedMsg, "_");
            st.nextToken(); // drop the first tooken
            d4SpreadParameter = new BigDecimal(st.nextToken());
            String requestId = st.nextToken();
            if (log.isDebugEnabled()) {
                log.debug("New param: " + d4SpreadParameter);
            }
            sendBackendCommandResponse(requestId, true);
        } catch (Throwable t) {
            log.error("Can't process D4Spread parameter update.", t);
        }
    }

    private void handleD4BanksUpdate(String feedMsg) {
        if (log.isTraceEnabled()) {
            log.trace("Processing D4Banks parameter update");
        }
        try {
            StringTokenizer st = new StringTokenizer(feedMsg, "_");
            st.nextToken(); // drop the first tooken
            d4BanksParameter = new BigDecimal(st.nextToken());
            String requestId = st.nextToken();
            if (log.isDebugEnabled()) {
                log.debug("New param: " + d4BanksParameter);
            }
            sendBackendCommandResponse(requestId, true);
        } catch (Throwable t) {
            log.error("Can't process D4Banks parameter update.", t);
        }
    }

    private void handleBanksBursaUpdate(String feedMsg) {
        if (log.isTraceEnabled()) {
            log.trace("Processing BanksBursa parameter update");
        }
        try {
            StringTokenizer st = new StringTokenizer(feedMsg, "_");
            st.nextToken(); // drop the first tooken
            banksBursaParameter = new BigDecimal(st.nextToken());
            String requestId = st.nextToken();
            if (log.isDebugEnabled()) {
                log.debug("New param: " + banksBursaParameter);
            }
            sendBackendCommandResponse(requestId, true);
        } catch (Throwable t) {
            log.error("Can't process BanksBursa parameter update.", t);
        }
    }

    private void handleOddsUpdate(String feedMsg) {
        if (log.isTraceEnabled()) {
            log.trace("Processing odds update");
        }
        try {
            StringTokenizer st = new StringTokenizer(feedMsg, "_");
            st.nextToken(); // drop the "odds" token
            long oppId = Long.parseLong(st.nextToken());
            String requestId = st.nextToken();
            synchronized (items) {
                Opportunity o = null;
                com.anyoption.service.level.market.Market market = null;
                for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                	market = markets.get(i.next());
                    o = market.getOppById(oppId);
                    if (null != o) {
                        OpportunitiesManager.updateOpportunityOdds(o);
                        if (market.isSnapshotReceived(o) && o.isInOppenedState()) {
                        	market.sendUpdate(sender, lifeId, o, false, true);
                        }
                    }
                }
            }
            sendBackendCommandResponse(requestId, true);
        } catch (Throwable t) {
            log.error("Can't process odds update notification.", t);
        }
    }

    private void handleSuspendOpportunity(String feedMsg) {
	if (log.isTraceEnabled()) {
	    log.trace("Processing Suspend Opportunity");
	}
	try {
	    // ("opportunitySuspend_" + feedName + oppId + paramValue + req.requestId
	    StringTokenizer st = new StringTokenizer(feedMsg, "_");
	    st.nextToken(); // drop the "opportunitySuspend_" token
	    String feedName = st.nextToken();
	    long oppId = Long.parseLong(st.nextToken());
	    int paramValue = Integer.parseInt(st.nextToken());
	    String requestId = st.nextToken();
	    boolean ok = true;
	    synchronized (items) {
			Opportunity o = null;
			com.anyoption.service.level.market.Market market = markets.get(feedName);
			if (market != null) {
			    o = market.getOppById(oppId);
			    if (null != o) {
			    	o.setSuspended(paramValue == 1 ? Boolean.TRUE : Boolean.FALSE);
					if (market.isSnapshotReceived(o) && o.isInOppenedState()) {
					    market.sendUpdate(sender, lifeId, o, true, true);
					}
			    }
			} else {
			    log.warn("Can't Suspend Opportunity for market: " + feedName);
			    ok = false;
			}
	    }
	    sendBackendCommandResponse(requestId, ok);
	} catch (Throwable t) {
	    log.error("Can't process Suspend Opportunity.", t);
	}
    }

//    private void handleBinary0100OppUpdate(String feedMsg) {
//        if (log.isTraceEnabled()) {
//            log.trace("Processing binary 0-100 opp update");
//        }
//        try {
//            StringTokenizer st = new StringTokenizer(feedMsg, "_");
//            st.nextToken(); // drop the "binary 0-100" token
//            long oppId = Long.parseLong(st.nextToken());
//            String params = st.nextToken();
//            String requestId = st.nextToken();
//            synchronized (items) {
//                Opportunity o = null;
//                com.anyoption.service.level.market.Market market = null;
//                for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
//                	market = markets.get(i.next());
//                    o = market.getOppById(oppId);
//                    if (null != o) {
//                        //OpportunitiesManager.updateOpportunityOdds(o);
//                    	o.getBinaryZeroOneHundred().changeParams(params);
//                        if (market.isSnapshotReceived(o) && o.isInOppenedState()) {
//                        	market.sendUpdate(sender, lifeId, o, false, true);
//                        }
//                        break;
//                    }
//                }
//            }
//            sendBackendCommandResponse(requestId, true);
//        } catch (Throwable t) {
//            log.error("Can't process binary 0-100 opp update notification.", t);
//        }
//    }

    private void handleDynamicsQuoteParamsUpdate(String feedMsg) {
        log.trace("Processing opp dynamics quote params update");
        try {
            StringTokenizer st = new StringTokenizer(feedMsg, "_");
            st.nextToken(); // drop the command token
            long oppId = Long.parseLong(st.nextToken());
            String params = st.nextToken();
            String requestId = st.nextToken();
            synchronized (items) {
                Opportunity o = null;
                com.anyoption.service.level.market.Market market = null;
                for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                	market = markets.get(i.next());
                    o = market.getOppById(oppId);
                    if (null != o) {
                    	o.setQuoteParams(params);
                    	DynamicsUtil.parseOpportunityQuoteParams(o);
                        if (market.isSnapshotReceived(o) && o.isInOppenedState()) {
                        	market.sendUpdate(sender, lifeId, o, false, true);
                        }
                        break;
                    }
                }
            }
            sendBackendCommandResponse(requestId, true);
        } catch (Throwable t) {
            log.error("Can't process opp dynamics quote params update notification.", t);
        }
    }

//    private void handleBinary0100MarketUpdate(String feedMsg) {
//        try {
//            StringTokenizer st = new StringTokenizer(feedMsg, "_");
//            st.nextToken(); // drop the "Binary0100Market" token
//            String feedName = st.nextToken();
//            String params = st.nextToken();
//            String requestId = st.nextToken();
//            boolean ok = true;
//            synchronized(items) {
//                com.anyoption.service.level.market.Market market = markets.get(feedName);
//                if (market != null) {
//                	for (int i = 0; i < market.getOppsCount(); i++) {
//                    	Opportunity o = market.getOpp(i);
//                    	if (!o.isInOppenedState()) {
//                    		o.getBinaryZeroOneHundred().changeParams(params);
//                    	}
//                    }
//                } else {
//                    log.warn("Binary 0100 Market update for market that does not exist: " + feedName);
//                    ok = false;
//                }
//            }
//            sendBackendCommandResponse(requestId, ok);
//        } catch (Throwable t) {
//            log.error("Can't process Binary 0100 Market update.", t);
//        }
//    }

    private void handleDynamicsMarketQuoteParams(String feedMsg) {
        try {
            StringTokenizer st = new StringTokenizer(feedMsg, "_");
            st.nextToken(); // drop the "dynamicsMarketQuoteParams" token
            String feedName = st.nextToken();
            String params = st.nextToken();
            String requestId = st.nextToken();
            boolean ok = true;
            synchronized(items) {
                com.anyoption.service.level.market.Market market = markets.get(feedName);
                if (market != null) {
                	for (int i = 0; i < market.getOppsCount(); i++) {
                    	Opportunity o = market.getOpp(i);
                    	if (!o.isInOppenedState()) {
                        	o.setQuoteParams(params);
                        	DynamicsUtil.parseOpportunityQuoteParams(o);
                    	}
                    }
                } else {
                    log.warn("Dynamics market update for market that does not exist: " + feedName);
                    ok = false;
                }
            }
            sendBackendCommandResponse(requestId, ok);
        } catch (Throwable t) {
            log.error("Can't process Binary 0100 Market update.", t);
        }
    }

    private void handleOpportunityShiftParameterUpdate(String feedMsg) {
        if (log.isTraceEnabled()) {
            log.trace("Processing opp shift update");
        }
        try {
            StringTokenizer st = new StringTokenizer(feedMsg, "_");
            st.nextToken(); // drop the "shift" token
            long oppId = Long.parseLong(st.nextToken());
            double[] shiftParams = new double[SkinGroup.NON_AUTO_SET.size()];
            int idx = 0;
            // Following the natural order of the SkinGroup enum
            for (Iterator<SkinGroup> iterator = SkinGroup.NON_AUTO_SET.iterator(); iterator.hasNext();) {
				iterator.next();
				shiftParams[idx++] = Double.parseDouble(st.nextToken());
			}
			String requestId = st.nextToken();
			boolean oppFound = false;
            synchronized (items) {
            	Opportunity o = null;
            	Map<SkinGroup, OpportunitySkinGroupMap> mapping = null;
            	com.anyoption.service.level.market.Market market = null;
            	for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
            		market = markets.get(i.next());
            		o = market.getOppById(oppId);
            		if (null != o) {
            			mapping = o.getSkinGroupMappings();
            			idx = 0;
            			// Following the natural order of the SkinGroup enum
            			for (SkinGroup skinGroup : SkinGroup.NON_AUTO_SET) {
            				mapping.get(skinGroup).setShiftParameter(shiftParams[idx++]);
						}

            			if (market.isSnapshotReceived(o) && o.isInOppenedState()) {
            				market.sendUpdate(sender, lifeId, o, false, true);
            			}
            			oppFound = true;
            			break;
            		}
            	}
            }
            if (!oppFound) {
            	log.warn("Could not update shiftings of opportunity [" + oppId + "] - opp not loaded in level service");
            }
            sendBackendCommandResponse(requestId, true);
       } catch (Throwable t) {
            log.error("Can't process shift update notification.", t);
       }
    }

    /**
     * when we got notify from BACKEND that traders change option expiration
     * @param feedMsg optionExpiration_marketid
     */
    private void handleOptionsExpirationChange(String feedMsg) {
        if (log.isTraceEnabled()) {
            log.trace("Processing Options Expiration Change");
        }
        try {
            StringTokenizer st = new StringTokenizer(feedMsg, "_");
            st.nextToken(); // drop the "optionExpiration" token
            long marketId = Long.parseLong(st.nextToken());
            String requestId = st.nextToken();
            synchronized (items) {
                com.anyoption.service.level.market.Market market = null;
                for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                	market = markets.get(i.next());
                    if (market.getMarketId() == marketId || market.getOptionPlusMarketId() == marketId) {
                    	market.unsubscribeData(levelService);
                    	market.subscribeData(levelService);
                    }
                }
            }
            sendBackendCommandResponse(requestId, true);
       } catch (Throwable t) {
            log.error("Can't process Options Expiration notification.", t);
        }

    }

    private void handleOpportunityManualClose(String feedMsg) {
        if (log.isTraceEnabled()) {
            log.trace("Processing opp manual close");
        }
        try {
            StringTokenizer st = new StringTokenizer(feedMsg, "_");
            st.nextToken(); // drop the "close" token
            long oppId = Long.parseLong(st.nextToken());
            double manualClosingLevel = Double.parseDouble(st.nextToken());
            String requestId = st.nextToken();
            synchronized (items) {
                Opportunity o = null;
                com.anyoption.service.level.market.Market market = null;
                for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                	market = markets.get(i.next());
                    o = market.getOppById(oppId);
                    if (null != o) {
                        break;
                    }
                }
                if (o == null) {
                    o = OpportunitiesManager.getRunningOpportunityById(oppId);
                    addOpportunity(o);
                }
                o.setManualClosingLevel(manualClosingLevel);
                OpportunitiesManager.closeOpportunity(oppId, manualClosingLevel, "manualClosingLevel: " + manualClosingLevel);
                opportunityStateChanged(oppId, Opportunity.STATE_CLOSED);
            }
            sendBackendCommandResponse(requestId, true);
        } catch (Throwable t) {
            log.error("Can't process opp manual close.", t);
        }
    }

    private void handleMarketSuspend(String feedMsg) {
        if (log.isTraceEnabled()) {
            log.trace("Processing market suspend");
        }
        try {
            StringTokenizer st = new StringTokenizer(feedMsg, "_");
            st.nextToken(); // drop the "suspend" token
            String feedName = st.nextToken();
            boolean suspend = st.nextToken().equalsIgnoreCase("true");
            String message = "";
            String requestId = null;
            try {
            	message = st.nextToken();
            	while (st.hasMoreTokens()) {
                    requestId = st.nextToken();
                    if (st.hasMoreTokens()) {
                        message += "_" + requestId;
                    }
            	}
            } catch (Throwable t) {
                // do nothing - there might be no message especially in unsuspend
            }
            boolean ok = true;
            synchronized(items) {
                com.anyoption.service.level.market.Market market = markets.get(feedName);
                if (market != null) {
                	market.setSuspended(suspend);
                    market.setSuspendedMessage(message);
                    market.sendOppsUpdate(suspend);
                    updateHomePageOpportunities();
                    market.refreshSpecialEntry(sender, lifeId);
                    market.sendUpdate(sender, lifeId, com.anyoption.service.level.market.Market.UPDATE_ALL_OPPS, true, suspend);
                    if (suspend) {
                        openedMarkets.remove(market.getMarketId());

                        /*
                         * Clear live investments related to the suspended market.
                         */
                        log.debug("Clearing live investments for market [" + market.getMarketId() + "] ...");
                        liveGlobe.removeMarket(market.getMarketId());
                        liveInvestments.removeMarket(market.getMarketId());
                        liveTrends.removeMarket(market.getMarketId());
                        log.debug("Clearing live investments for market [" + market.getMarketId() + "] done");
                    } else if (market.hasOpenedOpportunities()) {
                        openedMarkets.put(market.getMarketId(), true);
                    }
                    openedMarketsStr = getOpenedMarketsStr();
                    sendSnapshot(SNAPSHOT_THREAD_SLEEPING_TIME_DEFUALT);
                    webSender.send("aost_" + openedMarketsStr);
                } else {
                    log.warn("Suspend for market that does not exist: " + feedName);
                    ok = false;
                }
            }
            sendBackendCommandResponse(requestId, ok);
        } catch (Throwable t) {
            log.error("Can't process market suspend.", t);
        }
    }

    private void handleCurrentLevelRequest(String feedMsg) {
        if (log.isTraceEnabled()) {
            log.trace("Processing current level request");
        }
        try {
            StringTokenizer st = new StringTokenizer(feedMsg, "_");
            st.nextToken(); // drop the "level" token
            String feedName = st.nextToken();
            if (feedName.equals("WLCCHECK")) {
            	HashMap<String, String> currentValues = new HashMap<String, String>(1);
            	currentValues.put("ET_GROUP_CLOSE", "1");
                ETraderFeedMessage toSend = new ETraderFeedMessage(
                        null,
                        currentValues,
                        0,
                        -1,
                        0,
                        -1,
                        -1,
                        -1,
                        null,
                        0,
                		0,
                		0,
                		null,
                		0,
                		false,
                		false,
                		0,
                		0,
                		null,
                 		0,//bid not in use for updates only for devation check
                 		0,//offer not in use for updates only for devation check
                 		null,
                 		false,
                		null);
                webSender.send(toSend);
            } else {
                long oppId = Long.parseLong(st.nextToken());
                SkinGroup skinGroup = SkinGroup.getById(Long.parseLong(st.nextToken()));
                synchronized(items) {
                    com.anyoption.service.level.market.Market market = markets.get(feedName);
                    if (market != null) {
                    	Opportunity o = market.getOppById(oppId);
                        if (o != null) {
                        	HashMap<String, String> currentValues = new HashMap<String, String>(1);
	                        ETraderFeedMessage toSend = new ETraderFeedMessage(
	                                null,
	                                currentValues,
	                                0,
	                                0,
	                                0,
	                                oppId,
	                                0,
	                                0,
	                                null,
	                                0,
	                        		0,
	                        		0,
	                        		null,
	                        		0,
	                        		false,
	                        		false,
	                        		0,
	                        		0,
	                        		null,
	                         		0,//bid not in use for updates only for devation check
	                         		0,//offer not in use for updates only for devation check
	                         		null,
	                         		false,
	                         		null);
	                        market.getOpenedOpportunityLevels(o, toSend, skinGroup);
	                        currentValues.put("ET_GROUP_CLOSE", String.valueOf(market.getOpportunityGroupClose(o)));
	                        webSender.send(toSend);
                        } else {
                        	log.warn("Opportunity [" + oppId + "] for market [" + feedName
                 					+ "] level request does not exits, skipping it");
                        	}
                    } else {
                        log.warn("Level request for market that does not exist: " + feedName);
                    }
                }
            }
        } catch (Throwable t) {
            log.error("Can't process current level request.", t);
        }
    }

    private void handleOpportunityUpdateMaxExposure(String feedMsg) {
        if (log.isTraceEnabled()) {
            log.trace("Processing opp Update Max Exposure");
        }
        try {
            StringTokenizer st = new StringTokenizer(feedMsg, "_");
            st.nextToken(); // drop the "Update Max Exposure" token
            long oppId = Long.parseLong(st.nextToken());
            int[] maxExposureParams = new int[SkinGroup.NON_AUTO_SET.size()];
            int idx = 0;
            // Following the natural order of the SkinGroup enum
            for (Iterator<SkinGroup> iterator = SkinGroup.NON_AUTO_SET.iterator(); iterator.hasNext();) {
				iterator.next();
				maxExposureParams[idx++] = Integer.parseInt(st.nextToken());
			}
			String requestId = st.nextToken();
			boolean oppFound = false;
            synchronized (items) {
            	Opportunity o = null;
            	Map<SkinGroup, OpportunitySkinGroupMap> mapping = null;
            	com.anyoption.service.level.market.Market market = null;
            	for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
            		market = markets.get(i.next());
            		o = market.getOppById(oppId);
            		if (null != o) {
            			mapping = o.getSkinGroupMappings();
            			idx = 0;
            			// Following the natural order of the SkinGroup enum
            			for (SkinGroup skinGroup : SkinGroup.NON_AUTO_SET) {
            				mapping.get(skinGroup).setMaxExposure(maxExposureParams[idx++]);
						}

            			if (market.isSnapshotReceived(o) && o.isInOppenedState()) {
            				market.sendUpdate(sender, lifeId, o, false, true);
            			}
            			oppFound = true;
            			break;
            		}
            	}
            }

            if (!oppFound) {
            	log.warn("Could not update max exposure of opportunity [" + oppId + "] - opp not loaded in level service");
            }
            sendBackendCommandResponse(requestId, true);
       } catch (Throwable t) {
            log.error("Can't process Max Exposure update notification.", t);
       }
    }

    private void handleOpportunityUpdateTraderDisable(String feedMsg) {
        if (log.isTraceEnabled()) {
            log.trace("Processing opp Update Trader Disable");
        }
        try {
            StringTokenizer st = new StringTokenizer(feedMsg, "_");
            st.nextToken(); // drop the "Update Max Exposure" token
            long oppId = Long.parseLong(st.nextToken());
            boolean isDisabled = Boolean.parseBoolean(st.nextToken());
            String requestId = st.nextToken();
            synchronized (items) {
                Opportunity o = null;
                com.anyoption.service.level.market.Market market = null;
                for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                	market = markets.get(i.next());
                    o = market.getOppById(oppId);
                    if (null != o) {
                    	o.setDisabledTrader(isDisabled);
                        if (market.isSnapshotReceived(o) && o.isInOppenedState()) {
                        	market.sendUpdate(sender, lifeId, o, false, true);
                        }
                        break;
                    }
                }
            }
            sendBackendCommandResponse(requestId, true);
       } catch (Throwable t) {
            log.error("Can't process Trader Disable update notification.", t);
       }
    }
    
    private void handleUpdateMarketPriority(String feedMsg) {
        if (log.isTraceEnabled()) {
            log.trace("Processing updateMarketPriority");
        }
        try {
            StringTokenizer st = new StringTokenizer(feedMsg, "_");
            st.nextToken(); // drop the "updateMarketPriority" token
            String requestId = st.nextToken();
            synchronized (items) {
                com.anyoption.service.level.market.Market market = null;
                for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                	market = markets.get(i.next());
					MarketsManager.loadSkinsPriorities(market);
                }
                updateHomePageOpportunities();
            }
            sendBackendCommandResponse(requestId, true);
       } catch (Throwable t) {
            log.error("Can't process updateMarketPriority.", t);
       }
    }
    
    private void sendAllOppsUpdate() {
        if (log.isTraceEnabled()) {
            log.trace("Processing send All Opps update");
        }
        try {
            synchronized (items) {
                com.anyoption.service.level.market.Market market = null;
                for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                	market = markets.get(i.next());
                	market.sendOppsUpdate(false);
                }
            }
        } catch (Throwable t) {
            log.error("Can't process send All Opps update.", t);
        }
    }

    @Override
	public void onException(JMSException jmse) {
        // we have lost the connection to JMS
//        synchronized (items) {
//            //empty the items map; this way, once reconnected
//            //we are able to re-send snapshots
//            items = new HashMap<String, SubscribedItemAttributes>();
//        }
        // change the lifeId so the adaptors know to resend subscribe events after reconnect
//        lifeId++;
        // and loop to try to reconnect
    	reset();
    }

    public void reset() {
        jmsHandler.reset();
        webJmsHandler.reset();
        connector = new ConnectionLoopTPQR(jmsHandler, webJmsHandler, recoveryPause);
        connector.start();
    }

    /**
     * Subscribe item to ReutersService. Internal or remove one.
     *
     * @param itemName
     */
    protected void subscribeItem(String itemName) {
    	log.info("subscribeItem: " + itemName);
    	if (internalDataService) {
//    		dataService.subscribeItem(itemName);
    		Map<String, String> subscriptionConfig = new HashMap<String, String>();
    		subscriptionConfig.put(FormulaConfig.SUBSCRIPTION_CONFIG_IDENTIFIER, itemName);
    		dataService.subscribeMarket(subscriptionConfig);
    	} else {
    		reutersSender.send("subscribe_" + itemName);
    	}
    }

    /**
     * Request subscribtion for option.
     *
     * @param item the item that should receive the updates for the newly subscribed item (option for item)
     * @param option the option details (where to save the new item (option) handle)
     */
    @Override
	public void subscribeOption(Item item, Option option) {
        if (log.isInfoEnabled()) {
            log.info("subscribe item: " + item.getItemName() + " for option: " + option.getOptionName());
        }
        if (null == options.get(option.getOptionName())) {
            if (log.isInfoEnabled()) {
                log.info("subscribe to Reuters option: " + option.getOptionName());
            }
            subscribeItem(option.getOptionName());
            options.put(option.getOptionName(), option);
        }
        option.addItem(item);
        if (log.isDebugEnabled()) {
            log.debug("done subscribe item: " + item.getItemName() + " for option: " + option.getOptionName());
        }
    }

    /**
     * Unsubscribe item from ReutersService. Internal or remote one.
     *
     * @param itemName
     */
    protected void unsubscribeItem(String itemName) {
    	if (internalDataService) {
//    		dataService.unsubscribeItem(itemName);
    		Map<String, String> subscriptionConfig = new HashMap<String, String>();
    		subscriptionConfig.put(FormulaConfig.SUBSCRIPTION_CONFIG_IDENTIFIER, itemName);
    		dataService.unsubscribeMarket(subscriptionConfig);
    	} else {
    		reutersSender.send("unsubscribe_" + itemName);
    	}
    }

    /**
     * Request unsubscribtion for option.
     *
     * @param item the item that want to stop receiving updates for this item (option for item)
     * @param option the option details (where to take the item (option) handle from)
     */
    @Override
	public void unsubscribeOption(Item item, Option option) {
        if (log.isInfoEnabled()) {
            log.info("unsubscribe item: " + item.getItemName() + " from option: " + option.getOptionName());
        }
        option.removeItem(item);
        if (!option.hasItems()) {
            if (log.isInfoEnabled()) {
                log.info("unsubscribe from Reuters option: " + option.getOptionName());
            }
            unsubscribeItem(option.getOptionName());
            option.setHandle(null);
            option.setSnapshotReceived(false);
            options.remove(option.getOptionName());
        }
        if (log.isDebugEnabled()) {
            log.debug("done unsubscribeOption: " + option.getOptionName() + " items left: " + option.getItemsToString());
        }
    }

    private void unsubscribeItems() {
    	synchronized (items) {
    		log.info("unsubscribeItems...");
    		com.anyoption.service.level.market.Market market = null;
            for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
            	market = markets.get(i.next());
            	if (market.isSubscribedData()) {
            		unsubscribeItem(market.getMarketName());
            	}
            }
    		// Busy waiting....
    		int sleepCount = 0;
    		while (reutersSender.getQueueSize() > 0 && sleepCount < 10) {
    			log.info("Waiting for reutersSender...");
    			try {
    				Thread.sleep(1000);
    			} catch (Exception e) {
    				log.error("Problem waiting for sender", e);
    			}
    			sleepCount++;
    		}
    		if (reutersSender.getQueueSize() > 0) {
    			log.warn("Not waiting for reutersSender to finish queue size: " + reutersSender.getQueueSize());
    		}
    	}
    }

    /**
     * Add opportunity to the set of opportunities handled by the service.
     *
     * @param opp the opportunity to add
     */
    private void addOpportunity(Opportunity opp) {
        if (log.isInfoEnabled()) {
            log.info("Add opp: " + opp.getId() + " feed: " + opp.getMarket().getFeedName());
        }
    	com.anyoption.service.level.market.Market market = markets.get(opp.getMarket().getFeedName());
    	if (market == null) {
    		try {
				MarketConfig config = MarketsManager.getMarketConfig(opp.getMarketId(), dataServiceId);
        		market = MarketFactory.createMarket(opp.getMarket().getTypeId(), config, opp.getMarket().getFeedName(), opp.getMarketId());
        		markets.put(opp.getMarket().getFeedName(), market);
        		market.addOpportunity(opp, sender, lifeId);					
			} catch (SQLException e) {
				log.error("Cannot load the market configuration for marketId = " + opp.getMarketId(), e);
				return;
			} catch (NullPointerException e) {
				log.error("Cannot load market(probalby the market configuration is null), marketId = " + opp.getMarketId(), e);
				return;
			} catch (Exception e) {
				log.error("Cannot load market with marketId = " + opp.getMarketId(), e);
				return;
			}
			// TODO make the tree set comparator of markets on home page to accept object and cast as appropriate
			try {
				MarketsManager.loadSkinsPriorities(market);
			} catch (Throwable t) {
				log.error("Cant load Skins Priorities for market: " + market.getMarketName(), t);
			}
    		
    	} else {
    		market.addOpportunity(opp, sender, lifeId);
    	}
    	if (opp.isInOppenedState() && !market.isSubscribedData()) {
//        		subscribeMarket(market);
    		market.subscribeData(levelService);
    	}
    	if (opp.isInOppenedState() && market.isSnapshotReceived(opp)) {
    		market.sendUpdate(sender, lifeId, opp, false, true);
    	}
//        }
        return;
    }

    private void loadDBConfig() {
        try {
            ServiceConfig sc = ServiceConfigManager.getServiceConfig();
            ta25Parameter = sc.getTa25Parameter();
            ftseParameter = sc.getFtseParameter();
            cacParameter = sc.getCacParameter();
            sp500Parameter = sc.getSp500Parameter();
            klseParameter = sc.getKlseParameter();
            d4BanksParameter = sc.getD4BanksParameter();
            d4SpreadParameter = sc.getD4SpreadParameter();
            banksBursaParameter = sc.getBanksBursaParameter();
            String ls = System.getProperty("line.separator");
            log.info(ls
            		+ "ta25Parameter: " + ta25Parameter + ls
            		+ "ftseParameter: " + ftseParameter + ls
            		+ "cacParameter: " + cacParameter + ls
            		+ "sp500Parameter: " + sp500Parameter + ls
            		+ "klseParameter: " + klseParameter + ls
            		+ "d4BanksParameter: " + d4BanksParameter + ls
            		+ "d4SpreadParameter: " + d4SpreadParameter + ls
            		+ "banksBursaParameter: " + banksBursaParameter + ls);
        } catch (Throwable t) {
            log.error("Can't load db config.", t);
        }
    }

    /**
     * On the home page we want to show only 6 opportunities. We pick the markets
     * with highest "home page priority" that are opened. If not enough opened
     * we pad to 6 by taking from the one that are not oppened again ordered
     * by priority.
     *
     * As after each opportunity creation/opening/closing the ordering might
     * have changed this method check and send the corresponding updates to
     * the levels caches.
     *
     * NOTE: make sure you have lock over the items when you call
     * this method
     */
    public void updateHomePageOpportunities() {
        if (log.isTraceEnabled()) {
            log.trace("updateHomePageOpportunities...");
		}
		TreeSet<com.anyoption.service.level.market.Market> ts = new TreeSet<com.anyoption.service.level.market.Market>(new Comparator<com.anyoption.service.level.market.Market>() {
            /**
             * We want to sort the items (markets) in this way - first items with opened
             * opportunity ordered by "home page priority" (smaller number is higher priority
             * so we want acsending order) then suspended then items that don't have opened
             * opportunities ordered by "home page priority".
             *
             * @param i1
             * @param i2
             * @return
             */
            @Override
			public int compare(com.anyoption.service.level.market.Market i1, com.anyoption.service.level.market.Market i2) {
            	if (i1.getTypeId() < i2.getTypeId()) {
            		return -1;
            	} 
            	if (i1.getTypeId() > i2.getTypeId()) {
            		return 1;
            	}

                if (i1.isBinary0100Market() && i2.isBinary0100Market()) {
                	return 0;
                }
                if (i1.isBubblesMarket() && i2.isBubblesMarket()) {
                	return 0;
                }

                boolean o1 = (!i1.isSuspended()) && i1.isSnapshotReceived() && i1.isHasGoodForHomepageCompare();
                boolean o2 = (!i2.isSuspended()) && i2.isSnapshotReceived() && i2.isHasGoodForHomepageCompare();

                // if a market just opened that should go to the home page according to the home page priorities
                if (o1 && o2 && i1.getHomePagePriority() < i2.getHomePagePriority() && i1.hasJustOpened()) {
                    return -1;
                }
                if (o1 && o2 && i1.getHomePagePriority() > i2.getHomePagePriority() && i2.hasJustOpened()) {
                    return 1;
                }

                // keep a market that is substitute for a closing market on the home page there until it expire
                if (o1 && o2 && (i1.isOnHomePageCompare() || isMarketShouldGoToHomePage(i1)) && !(i2.isOnHomePageCompare() || isMarketShouldGoToHomePage(i2))) {
                    return -1;
                }
                if (o1 && o2 && !(i1.isOnHomePageCompare() || isMarketShouldGoToHomePage(i1)) && (i2.isOnHomePageCompare()|| isMarketShouldGoToHomePage(i2))) {
                    return 1;
                }

                // replace expired market on the home page after its closing 1 min state pass
                if (o1 && !o2) {
                    return -1;
                }
                if (o2 && !o1) {
                    return 1;
                }

                // opened markets go first then suspended then closed
                boolean b1 = i1.hasOpenedOpportunities() && i1.isSnapshotReceived();
                boolean s1 = i1.isSuspended();
                boolean b2 = i2.hasOpenedOpportunities() && i2.isSnapshotReceived();
                boolean s2 = i2.isSuspended();
                if ((b1 && !b2) || (b1 && b2 && !s1 && s2)) {
                    return -1;
                }
                if ((!b1 && b2) || (b1 && b2 && s1 && !s2)) {
                    return 1;
                }

                // sort according to priority
                return i1.getHomePagePriority() - i2.getHomePagePriority();
            }

			/**
			 * @return <code>true</code> if this item has no opp that wait for expiry or closed and its ta25 market or eur/usd and its skin
			 *         etrader else <code>false</code>.
			 */
			public boolean isMarketShouldGoToHomePage(com.anyoption.service.level.market.Market m) {
				return ((m.getMarketName().equals(".TA25") || m.getMarketName().equals("EUR=") || m.getMarketName().equals(".IXIC"))
						&& m.getCompareSkinId() == SKIN_ETRADER_INT && !m.hasGoingToCloseOpportunities());
			}
        });

        for (Skins skin : skins) {
            int skinId = skin.getId();
            if (log.isTraceEnabled()) {
                log.trace("Sorting skin: " + skinId);
            }
            for (com.anyoption.service.level.market.Market market : markets.values()) {
                Integer priority = market.getHomePagePriorityById(skinId);
                if (null != priority) {
                    // set all this for every skin then we will compare them easy
                	market.setHomePagePriority(priority);
                	market.setOnHomePageCompare(skinId);
                	market.setHasGoodForHomepageCompare(skinId);
                    ts.add(market);
                }
            }
//        	ts.comparator();
        //ts.addAll(items.values());
	        int idx = 0;
	        com.anyoption.service.level.market.Market market = null;
	        StringBuilder sbCurrentMarketsOnHomePage = new StringBuilder();
	        Map<Long, List<Long>> marketsByGroup = new TreeMap<Long, List<Long>>();
	        
	        for (Iterator<com.anyoption.service.level.market.Market> i = ts.iterator(); i.hasNext(); idx++) {
	            market = i.next();
//	            if (log.isTraceEnabled()) {
//	                log.trace("Home page sorting " + item.getItemName() +
//	                        " priority: " + item.getHomePagePriority() +
//	                        " hasGoodForHomepage: " + item.hasGoodForHomepage(skinId) +
//	                        " hasOpenedOpportunities: " + item.hasOpenedOpportunities() +
//	                        " isSnapshotReceived: " + item.isSnapshotReceived() +
//	                        " isSuspended: " + item.isSuspended() +
//	                        " isOnHomePage:" + item.isOnHomePageCompare());
//	            }

	            if (idx < MAX_MARKETS_ON_HOMEPAGE) {

	            	if (sbCurrentMarketsOnHomePage.length() > 0) {
                        sbCurrentMarketsOnHomePage.append("|");
                    }
                    sbCurrentMarketsOnHomePage.append(market.getMarketId());

	            	if (!market.isOnHomePageCompare()) {
	            			market.setOnHomePage(true, skinId, sender, lifeId);
	            	}
	            } else { // idx >= MAX_MARKETS_ON_HOMEPAGE
	            	if (market.isOnHomePageCompare()) {
	            			market.setOnHomePage(false, skinId, sender, lifeId);	            			
	            	}
	            }

	            Long marketGroupId = market.getMarketGroupId();
	            if (market.isOptionPlusMarket()) {
	            	marketGroupId = ConstantsBase.MARKET_GROUP_OPTION_PLUS_ID;
	            }
	            if (market.isDynamicsMarket()) {
	            	marketGroupId = ConstantsBase.MARKET_GROUP_DYNAMICS_ID;
	            }
	            
	            List<Long> markets = marketsByGroup.get(marketGroupId);
	            if (markets == null) {
	            	markets = new ArrayList<Long>(MAX_MARKETS_BY_GROUP);
	            	markets.add(market.getMarketId());
	            	marketsByGroup.put(marketGroupId, markets);
	            } else if (markets.size() < MAX_MARKETS_BY_GROUP) {
	            	markets.add(market.getMarketId());
	            }
	        }

	        String currentMarketsOnHomePage = sbCurrentMarketsOnHomePage.toString();
	        if (null == skin.getCurrentMarketsOnHomePage() || !skin.getCurrentMarketsOnHomePage().equals(currentMarketsOnHomePage)) {
	            webSender.send("aohp_" + skin.getId() + "_" + currentMarketsOnHomePage);
	            skin.setCurrentMarketsOnHomePage(currentMarketsOnHomePage);
	        }

	        String strMarketsByGroup = buildMarketsByGroupString(marketsByGroup);
	        if (null == skin.getCurrentMarketsByGroup() || !skin.getCurrentMarketsByGroup().equals(strMarketsByGroup)) {
	        	webSender.send(ConstantsBase.JMS_MARKETS_BY_GROUP_PREF + "_"+ skin.getId() + "_" + strMarketsByGroup);
	        	skin.setCurrentMarketsByGroup(strMarketsByGroup);
	        }

	        ts.clear();
        }
    }

    /**
     * @param marketsByGroup
     * @return a String message that contains top markets by market group
     */
    private String buildMarketsByGroupString(Map<Long, List<Long>> marketsByGroup) {
    	StringBuilder sb = new StringBuilder();
    	if (marketsByGroup != null && marketsByGroup.size() > 0) {
    		for (Map.Entry<Long, List<Long>> marketEntry : marketsByGroup.entrySet()) {
    			sb.append(marketEntry.getKey()).append("$");
    			for (Long marketId : marketEntry.getValue()) {
					sb.append(marketId).append("|");
				}
    			sb.replace(sb.length() - 1, sb.length(), "");
    			sb.append("_");
    		}
    		sb.replace(sb.length() - 1, sb.length(), "");
    	}
    	return sb.toString();
    }

    /**
     * Load the opportunities that the service need from the db.
     */
    private void loadOpportunities() {
        synchronized (items) {
            try {
            	sendOppUpdate(null, ConstantsBase.JMS_OPPS_UPDATE_COMMAND_CLEAR);
                OpportunitiesManager.resetIsPublished();
                ArrayList<Opportunity> opps = OpportunitiesManager.getLevelServiceOpportunities();
                if (log.isInfoEnabled()) {
                    log.info("Loading " + opps.size() + " opportunities.");
                }
                for (int i = 0; i < opps.size(); i++) {
                    Opportunity opp = null;
                    try {
                        opp = opps.get(i);
                        OpportunitiesManager.publishOpportunity(opp.getId(), opp.isInOppenedState());
                        if (opp.getState() > Opportunity.STATE_CREATED) {
                            if (opp.getTypeId() == Opportunity.TYPE_BINARY_0_100_ABOVE || opp.getTypeId() == Opportunity.TYPE_BINARY_0_100_BELOW) {
                                OpportunitiesManager.getOpportunityCallPutBoughtSoldAmounts(opp);
                            } else {
                                OpportunitiesManager.getOpportunityCallPutAmounts(opp);
                            }
                            if (log.isTraceEnabled()) {
                                log.trace("calls: " + opp.getCalls() + " puts: " + opp.getPuts() + " bought: " + opp.getContractsBought() + " sold: " + opp.getContractsSold());
                            }
                        }
                        if (opp.isInOppenedState()) {
                        	sendOppUpdate(opp, ConstantsBase.JMS_OPPS_UPDATE_COMMAND_ADD);
                        }
                        addOpportunity(opp);
                    } catch (Throwable t) {
                        log.error("Can't load opportunity " + (null != opp ? String.valueOf(opp.getId()) : "null"), t);
                    }
                }
                updateHomePageOpportunities();
            } catch (Exception e) {
                log.fatal("Can't load the opportunities.", e);
            }
        }
        oppsLoaded = true;
        sendSnapshot(SNAPSHOT_THREAD_SLEEPING_TIME_STARTUP);
        if (log.isInfoEnabled()) {
            log.info("Opportunities loading done.");
        }
    }

    /**
     * This is a callback method so the service is notified by the opportunity management
     * (the scheduler) for change in opportunity state.
     *
     * @param oppId the id of the opportunity which state have changed
     * @param state the new state of the opportunity
     */
    public void opportunityStateChanged(long oppId, int state) {
        if (log.isInfoEnabled()) {
            log.info("Opp state change for oppId: " + oppId + " state: " + state);
        }
        synchronized (items) {
            switch (state) {
            case Opportunity.STATE_CREATED:
                try {
                    Opportunity o = OpportunitiesManager.getRunningOpportunityById(oppId);
                    addOpportunity(o);
                    updateHomePageOpportunities();
                } catch (Throwable t) {
                    log.error("Can't load opp.", t);
                }
                break;
            case Opportunity.STATE_OPENED:
            case Opportunity.STATE_LAST_10_MIN:
            case Opportunity.STATE_CLOSING_1_MIN:
            case Opportunity.STATE_CLOSING:
            case Opportunity.STATE_CLOSED:
            case Opportunity.STATE_DONE:
            case Opportunity.STATE_PAUSED:
            case Opportunity.STATE_WAITING_TO_PAUSE:
                Opportunity o = null;
                com.anyoption.service.level.market.Market marketToUnsubscribe = null;
                for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                    String key = i.next();
                    com.anyoption.service.level.market.Market market = markets.get(key);
                    // some markets like the currencies are attached to the currency RIC
                    // and to the options RICs so make sure you don't send one update
                    // for each key in the map that points to this market
                    if (market.getMarketName().equals(key)) {
                        for (int l = 0; l < market.getOppsCount(); l++) {
                            o = market.getOpp(l);
                            if (o.getId() == oppId) {
                        		// The point here is to allow the state to change only forward in the opportunity state life cycle.
                        		// 1 (Created) -> 2 (Opened) -> 3 (Last 10 min) -> 4 (Closing 1 min) -> 5 (Closing) -> 6 (Closed) -> 7 (Done)
                        		// The exceptions should be 8 (Paused), 9 (Suspended) and "up" since you can get there and back from any normal state.
                        		if (state > o.getState() || o.getState() > Opportunity.STATE_DONE) {
                        			o.setState(state);
                        			sendOppUpdate(o, o.isInOppenedState() ? ConstantsBase.JMS_OPPS_UPDATE_COMMAND_ADD : ConstantsBase.JMS_OPPS_UPDATE_COMMAND_DELETE);
                        		}
                                if (state == Opportunity.STATE_OPENED) {
                                    if (!market.isSubscribedData()) {
                                        // this should be the day opening so reset the disabled markets (just in case)
                                    	market.enableOpportunities("Reset in the beginning of the day", true);
//                                    	market.setOptionSubscribtionManager(levelService);
//                                        subscribeMarket(market);
                                    	market.subscribeData(levelService);
                                    }
									log.debug("change state oppId: "+ oppId + " state " + state + " market.isSnapshotReceived() "
												+ market.isSnapshotReceived() + " market.isLongTermSnapshotReceived() "
												+ market.isLongTermSnapshotReceived() + " market.getLongTermFormula() "
												+ market.getLongTermFormula());
                                    if (market.isSnapshotReceived() && (market.getLongTermFormula() == null || market.isLongTermSnapshotReceived())) {
                                        // check if the opened should replace the
                                        // one already showing. this should be done only
                                        // if the market is already opened on the page
                                    	market.refreshSpecialEntry(sender, lifeId);
                                    	market.openOpportunity(o);
                                    }
                                    // this will hanle if the market was not on the home page - will
                                    // put it and select home page entry
                                    // NOTE: the order of the last two is important as if we first
                                    // pick home page entries for the new we will get two messages
//                                    updateHomePageOpportunities();
                                }
                                if (state == Opportunity.STATE_WAITING_TO_PAUSE) {
                                    Opportunity tmon = market.getTimeMarketOpensNext();
                                    if (log.isDebugEnabled()) {
                                        log.debug("TimeMarketOpensNext: " + (null != tmon ? tmon.getTimeNextOpen() : "none"));
                                    }
                                    if (null != tmon) {
                                        o.setTimeNextOpen(tmon.getTimeNextOpen());
                                    } else {
                                        o.setTimeNextOpen(null);
                                    }
                                }
                                if (state == Opportunity.STATE_CLOSING_1_MIN
                                		/* List of backup states (in case of investments with latency) [ */
                                		|| state == Opportunity.STATE_CLOSING
                                		|| state == Opportunity.STATE_CLOSED
                                		|| state == Opportunity.STATE_DONE
                                		/* ] */
                                		|| state == Opportunity.STATE_PAUSED
                                		|| state == Opportunity.STATE_WAITING_TO_PAUSE) {
                                	liveTrends.removeOpportunity(o);
                                	liveGlobe.removeOpportunity(oppId);
                                	liveInvestments.removeOpportunity(oppId);
                                }
                                if (state == Opportunity.STATE_WAITING_TO_PAUSE
                                		|| state == Opportunity.STATE_PAUSED
                                		|| state == Opportunity.STATE_CLOSING_1_MIN
                                		|| state == Opportunity.STATE_CLOSING) {
                                    updateHomePageOpportunities();
	                                //spichely for turkish markets :)
                                    market.refreshSpecialEntry(sender, lifeId);
                                }
                                if (market.isSnapshotReceived(o)) {
                                	market.sendUpdate(
                                            sender,
                                            lifeId,
                                            o,
                                            state == Opportunity.STATE_PAUSED && !market.isSpecialEntry(o),
                                            true);
                                }
                                if (state == Opportunity.STATE_CLOSED) {
                                	// if a daily, weekly or monthly opp is closing this is the end of the day
                                	if (o.getScheduled() != Opportunity.SCHEDULED_HOURLY) {
                                		market.updateLastClosedLevel(o);
                                	}
                                }
                                if (state == Opportunity.STATE_DONE || state == Opportunity.STATE_WAITING_TO_PAUSE) {
                                    if (!market.hasOpenedOpportunities()) {
                                    	market.pauseWaitingToPauseLongTermOpps();
                                    	marketToUnsubscribe = market;
                                    } else if (o.getScheduled() != Opportunity.SCHEDULED_HOURLY) {
                                    	market.pauseWaitingToPauseLongTermOpps();
                                    }
                                }
                                if (state == Opportunity.STATE_DONE) {
                                	market.removeOpportunity(oppId, sender, lifeId);
                                }
                            }
                        }
                    }
                }
                if (marketToUnsubscribe != null) {
                    if (log.isInfoEnabled()) {
                        log.info("Unsubscribe market: " + marketToUnsubscribe.getMarketName());
                    }
                    marketToUnsubscribe.unsubscribeData(levelService);
//                    if (marketToUnsubscribe.isUseMainRic()) {
//                    	unsubscribeItem(marketToUnsubscribe.getMarketName());
//                    }
//                    toUnsubscribe.setHandle(null);
//                    marketToUnsubscribe.setSubscribedToReuters(false);
                    openedMarkets.remove(marketToUnsubscribe.getMarketId());
                    openedMarketsStr = getOpenedMarketsStr();
                    webSender.send("aost_" + openedMarketsStr);
                    if (!marketToUnsubscribe.hasOpportunities()) {
                        markets.remove(marketToUnsubscribe.getMarketName());
                    } else {
                        // normally this should happen in the end of the day
                    	marketToUnsubscribe.resetInTheEndOfTheDay();
                    }
                    sendSnapshot(SNAPSHOT_THREAD_SLEEPING_TIME_DEFUALT);
                }
                if (state == Opportunity.STATE_DONE) {
                    updateHomePageOpportunities();
                }
                break;
            }
        }
    }

    /**
     * Check if opportunity got its closing level. If the opp didn't got its closing level
     * it will rize a flag so the opportunity will be closed right after it receive its
     * closing level.
     *
     * @param oppId the id of the opp to check
     * @return <code>true</code> if the opp got its closing level else <code>false</code> (even if not found).
     */
    public boolean isOpportunityClosingLevelReceived(long oppId) {
        synchronized (items) {
            Opportunity o = null;
            for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                com.anyoption.service.level.market.Market market = markets.get(i.next());
                o = market.getOppById(oppId);
                if (null != o) {
                	boolean hasCL = market.isClosingLevelReceived(o);
                    if (!hasCL) {
                        o.setTimeToClose(true);
                    }
                    return hasCL;
                }
            }
        }
        return false;
    }

    /**
     * check if opportunity last update was in less then X sec (x is parameter of each market)
     *
     * @param oppId
     * @return true if there was update, false if there wasn't
     */
    public boolean isOpportunityShouldBeClosed(long oppId) {
        synchronized (items) {
            Opportunity o = null;
            for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                com.anyoption.service.level.market.Market market = markets.get(i.next());
                o = market.getOppById(oppId);
                if (null != o) {
                    return market.isShouldBeClosed(oppId);
                }
            }
        }
        return false;
    }

    /**
     * Gets the closing level of opp with specified id.
     *
     * @param oppId the id of the opp
     * @return The closing level of the opp or 0 if the opp not found.
     */
    public double getOpportunityClosingLevel(long oppId) {
        synchronized (items) {
            Opportunity o = null;
            for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                com.anyoption.service.level.market.Market market = markets.get(i.next());
                o = market.getOppById(oppId);
                if (null != o) {
                    return market.getClosingLevel(o);
                }
            }
        }
        return 0;
    }

    /**
     * Gets the closing level formula of opp with specified id.
     *
     * @param oppId the id of the opp
     * @return The closing level of the opp or null if the opp not found.
     */
    public String getOpportunityClosingLevelTxt(long oppId) {
        synchronized (items) {
            Opportunity o = null;
            for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                com.anyoption.service.level.market.Market market = markets.get(i.next());
                o = market.getOppById(oppId);
                if (null != o) {
                    return market.getClosingLevelTxt(o);
                }
            }
        }
        return null;
    }

    /**
     * Gets the opp with specified id.
     *
     * @param oppId the id of the opp
     * @return The opp or null if the opp not found.
     */
    public Opportunity getOpportunityById(long oppId) {
        synchronized (items) {
            Opportunity o = null;
            for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                com.anyoption.service.level.market.Market market = markets.get(i.next());
                o = market.getOppById(oppId);
                if (null != o) {
                    return o;
                }
            }
        }
        return null;
    }

    /**
     * @return <code>ArrayList<MarketRate></code> with the current rates of the
     * markets opened at the moment.
     */
    public ArrayList<MarketRate> getMarketsCurrentRates() {
        ArrayList<MarketRate> l = new ArrayList<MarketRate>();
        synchronized (items) {
            MarketRate mr = null;
            for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                com.anyoption.service.level.market.Market market = markets.get(i.next());
                if (market.hasOpenedOpportunities() && market.isSnapshotReceived()) {
                	BigDecimal lastRealLevel = new BigDecimal(Double.toString(market.getState().getLastRealLevel()));
                	Map<String, BigDecimal> q = market.getQuoteFields();
                	for (SkinGroup skinGroup : SkinGroup.NON_AUTO_SET) {
                		mr = new MarketRate();
						mr.setMarketId(market.getMarketId());
						mr.setSkinGroup(skinGroup);
						mr.setRate(lastRealLevel);
						mr.setGraphRate(new BigDecimal(Double.toString(market.getGraphLevel(skinGroup))));
						mr.setDayRate(new BigDecimal(Double.toString(market.getDayLevel(skinGroup))));
						mr.setLast(q.get(QUOTE_FIELDS_LAST));
						mr.setAsk(q.get(QUOTE_FIELDS_ASK));
						mr.setBid(q.get(QUOTE_FIELDS_BID));
						l.add(mr);
					}
                }
            }
        }
        return l;
    }

    public ArrayList<Opportunity> getGoldenMinutesOpportunities(Expiry expiry) {
        ArrayList<Opportunity> l = new ArrayList<Opportunity>();
        Opportunity o = null;
        synchronized (items) {
            for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                com.anyoption.service.level.market.Market market = markets.get(i.next());
                o = market.getGoldenMinutesOpportunity(expiry);
                if (null != o) {
                    o.setGmLevel(market.getState().getLastRealLevel());
                    l.add(o);
                }
            }
        }
        return l;
    }

    /**
     * Update markets average investment in the last 30 days exposure calculation
     * parameter.
     */
    public void updateMarketsAverageInvestments() {
        try {
            ArrayList<Market> l = ServiceConfigManager.getAverageInvestments();
            synchronized (items) {
                Market market = null;
                com.anyoption.service.level.market.Market marketItem = null;
                for (int i = 0; i < l.size(); i++) {
                    market = l.get(i);
                    marketItem = markets.get(market.getFeedName());
                    if (null != marketItem) {
                    	marketItem.setAverageInvestments(market.getAverageInvestments());
                    }
                }
            }
        } catch (Throwable t) {
            log.error("Can't update average investments.", t);
        }
    }

    public void sendAlertEmail(String subjectTail, String body) {
        //we dont want to sent any more emails
    	//emailSender.send(emailRcpt, emailSubject + " - " + subjectTail, body);
    }

    public void sendBinray0100TradingHaltEmail(String emailSubject, String body) {
        emailSender.send(reportEmailRcpt, emailSubject, body);
    }

    /**
     * @param subjectTail
     * @param body
     *
     * @deprecated Currently does nothing in order to avoid mailserver flood
     */
    @Deprecated
	public void sendBigLevelChangeEmail(String subjectTail, String body) {
    	// This method is flooding mailserver with messages that no longer are read.
//        emailSender.send(reportEmailRcpt, emailSubject, body);
    }

    public void sendNonSettlementEmail(String subjectTail, String body) {
        emailSender.send(reportEmailRcpt, nonSettlementEmailSubject, body);
    }

    public void sendReportEmail(String subjectTail, String body) {
        emailSender.send(reportEmailRcpt, reportEmailSubject + " - " + subjectTail, body);
    }

    public void sendMessage(Serializable s) {
        sender.send(s);
    }

    public ArrayList<Skins> getSkins() {
        return skins;
    }

    private String getOpenedMarketsStr() {
        boolean first = true;
        StringBuffer sb = new StringBuffer();
        for (Iterator<Long> i = openedMarkets.keySet().iterator(); i.hasNext();) {
            if (!first) {
                sb.append("|");
            } else {
                first = false;
            }
            sb.append(i.next());
        }
        return sb.toString();
    }

    public String getOpenedMarkets() {
        return openedMarketsStr;
    }


	public void subscribeMarket(com.anyoption.service.level.market.Market market, Map<String, Map<String, String>> subscriptionConfigs) {
		log.info("subscribeMarket: " + market.getMarketName());
		Iterator<String> configIterator = subscriptionConfigs.keySet().iterator();
		Map<String, Map<String, String>> newSubscritpionConfigs = new HashMap<String, Map<String,String>>();
		while (configIterator.hasNext()) {
			String subscriptionKey = configIterator.next();
			if (subscriptionConfigs.get(subscriptionKey).get(FormulaConfig.SUBSCRIPTION_CONFIG_SWITCHTOFUTURE) != null
					&& Boolean.valueOf(subscriptionConfigs.get(subscriptionKey).get(FormulaConfig.SUBSCRIPTION_CONFIG_SWITCHTOFUTURE))) {
				String secondLetter = OpportunitiesManager.getOpportunityOptionsExpirationLetter(market.isOptionPlusMarket() ? market.getOptionPlusMarketId() : market.getMarketId());
				if (secondLetter != null) {
					log.info("Found market with second letter");
					Map<String, String> config = subscriptionConfigs.get(subscriptionKey);
					String start = subscriptionKey.substring(0, Integer.valueOf(config.get(FormulaConfig.SUBSCRIPTION_CONFIG_SWITCHTOFUTURE_PREFIX_LENGTH)));
					String newSubscriptionKey = start + secondLetter;
					config.put(FormulaConfig.SUBSCRIPTION_CONFIG_IDENTIFIER, newSubscriptionKey);
					newSubscritpionConfigs.put(newSubscriptionKey, config);
					configIterator.remove();
					for (Formula formula : market.getFormulas()) {
						if (null != formula.getFormulaConfig().getSubscriptionConfig().get(subscriptionKey)) {
							formula.getFormulaConfig().getSubscriptionConfig().remove(subscriptionKey);
							formula.getFormulaConfig().getSubscriptionConfig().put(newSubscriptionKey, config);
						}
					}
					log.info("Changed market subscription from " + subscriptionKey + " to " + newSubscriptionKey);
				}
			}
		}
		subscriptionConfigs.putAll(newSubscritpionConfigs);
		for (String subscriptionKey : subscriptionConfigs.keySet()) {
			log.info("subscribeMarket: subscriptionKey = " + subscriptionKey);
			List<com.anyoption.service.level.market.Market> markets = fieldMarketsMap.get(subscriptionKey);
			if (markets == null) {
				markets = new ArrayList<com.anyoption.service.level.market.Market>();
				fieldMarketsMap.put(subscriptionKey, markets);
			}
			markets.add(market);
			if (internalDataService) {
				dataService.subscribeMarket(subscriptionConfigs.get(subscriptionKey));
			} else {
				Gson gson = new GsonBuilder().serializeNulls().create();
				reutersSender.send("subscribe_" + gson.toJson(subscriptionConfigs.get(subscriptionKey)));
			}
		}
		if (!market.isSuspended()) {
			openedMarkets.put(market.getMarketId(), true);
			openedMarketsStr = getOpenedMarketsStr();
		}
		webSender.send("aost_" + openedMarketsStr);
		sendSnapshot(SNAPSHOT_THREAD_SLEEPING_TIME_DEFUALT);
	}

	public void
			unsubscribeMarket(com.anyoption.service.level.market.Market market, Map<String, Map<String, String>> subscriptionConfigs) {
		log.info("unsubscribeMarket: " + market.getMarketName());
		for (String subscriptionKey : subscriptionConfigs.keySet()) {
			if (internalDataService) {
				dataService.unsubscribeMarket(subscriptionConfigs.get(subscriptionKey));
			} else {
				Gson gson = new GsonBuilder().serializeNulls().create();
				reutersSender.send("unsubscribe_" + gson.toJson(subscriptionConfigs.get(subscriptionKey)));
			}
			List<com.anyoption.service.level.market.Market> markets = fieldMarketsMap.get(subscriptionKey);
			if (markets != null) {
				markets.remove(market);
			}
		}
	}

	public com.anyoption.service.level.market.Market getMarketByOppId(long oppId) {
		synchronized (markets) {
			Opportunity o = null;
			for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
				com.anyoption.service.level.market.Market market = markets.get(i.next());
				o = market.getOppById(oppId);
				if (null != o) {
					return market;
				}
			}
		}
		return null;
	}


	public static BigDecimal getTraderParameter(String traderParamName) {
		BigDecimal value = new BigDecimal(0);
		if (traderParamName.equalsIgnoreCase("ta25")) {
			value = new BigDecimal(ta25Parameter); 
		} else if (traderParamName.equalsIgnoreCase("ftse")) {
			value = ftseParameter;
		} else if (traderParamName.equalsIgnoreCase("cac")) {
			value = cacParameter;
		} else if (traderParamName.equalsIgnoreCase("sp500")) {
			value = sp500Parameter;
		} else if (traderParamName.equalsIgnoreCase("klse")) {
			value = klseParameter;
		} else if (traderParamName.equalsIgnoreCase("d4Banks")) {
			value = d4BanksParameter;
		} else if (traderParamName.equalsIgnoreCase("banksBursa")) {
			value = banksBursaParameter;
		} else if (traderParamName.equalsIgnoreCase("d4Spread")) {
			value = d4SpreadParameter;
		}
		return value;		
	}

	@Override
	public String getDataServiceClassName() {
		return dataServiceClassName;
	}

	@Override
	public void setDataServiceClassName(String dataServiceClassName) {
		this.dataServiceClassName = dataServiceClassName;
	}

	@Override
	public String getDataServiceAttributes() {
		return dataServiceAttributes;
	}

	@Override
	public void setDataServiceAttributes(String dataServiceAttributes) {
		this.dataServiceAttributes = dataServiceAttributes;
	}
	
	/**
	 * send update opp update when opp is open/close/suspend/unsuspend
	 * @param o opportunity to send the update for
	 */
	public void sendOppUpdate(Opportunity o, String command) {
		String delimiter = "_";
		if (command.equals(ConstantsBase.JMS_OPPS_UPDATE_COMMAND_CLEAR)) {
			webSender.send(ConstantsBase.JMS_OPPS_UPDATE + delimiter + command);
		} else { // add or delete
			webSender.send(ConstantsBase.JMS_OPPS_UPDATE + delimiter + command + delimiter + o.getMarketId() + delimiter + o.getId() + delimiter + o.getScheduled() +
					delimiter + o.getTimeEstClosing().getTime() + delimiter + o.getState());
		}
	}

    //////////////////////RecoveryThread
    private class ConnectionLoopTPQR extends ConnectionLoop {
        protected JMSHandler webJmsHandler;

        public ConnectionLoopTPQR(JMSHandler jmsHandler, JMSHandler webJmsHandler, int recoveryPause) {
            super(jmsHandler, recoveryPause);
            threadName = "ConnectionTPQR";
            this.webJmsHandler = webJmsHandler;
        }

        @Override
		protected void reset() {
            super.reset();
            if (null != webJmsHandler) {
            	webJmsHandler.reset();
            }
        }

        @Override
		protected void onConnectionCall() {
            log.info("onConnectionCall");
            return;
        }

        @Override
		protected void connectionCall() throws JMSException, NamingException {
            //initialize TopicPublisher and QueueReceiver
            if (log.isInfoEnabled()) {
                log.info("connectionCall");
            }
            if (null != jmsHandler.getTopicName()) {
                jmsHandler.initTopicPublisher(msgPoolSize);
            }
            if (null != jmsHandler.getQueueName()) {
                jmsHandler.initQueueReceiver();
            }
            if (null != webJmsHandler && null != webJmsHandler.getTopicName()) {
                webJmsHandler.initTopicPublisher(msgPoolSize);
            }
            if (null != webJmsHandler && null != webJmsHandler.getQueueName()) {
                webJmsHandler.initQueueReceiver();
            }
        }
    }

    private class ConnectionLoopQSTS extends ConnectionLoop {
        public ConnectionLoopQSTS(JMSHandler jmsHandler, int recoveryPause) {
            super(jmsHandler, recoveryPause);
            threadName = "ConnectionQSTS";
        }

        @Override
		protected void onConnectionCall() {
            log.info("onConnectionCall");
        }

        @Override
		protected void connectionCall() throws JMSException, NamingException {
            //initialize TopicSubscriber and QueueSender
            log.info("connectionCall");
            if (null != jmsHandler.getTopicName()) {
                jmsHandler.initTopicSubscriber();
            }
            if (null != jmsHandler.getQueueName()) {
                jmsHandler.initQueueSender(msgPoolSize);
            }
        }
    }

    //////////////////////HeartbeatThread
    private class HeartbeatThread extends Thread {
        private HeartbeatMessage fixedMessage = new HeartbeatMessage(lifeId);
        private boolean running;
        private long lastJingleTime;

        @Override
		public void run() {
            Thread.currentThread().setName("Heartbeat");
            running = true;
            while (running) {
                lastJingleTime = System.currentTimeMillis();
                try {
                    //publish the update to JMS
                    jmsHandler.publishMessage(fixedMessage);
                } catch (JMSException je) {
                    log.error("Unable to send message - JMSException:" + je.getMessage());
                }
                if (log.isTraceEnabled()) {
                    log.trace("Heartbeat sent: " + fixedMessage.lifeId);
                }

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
            }
            log.warn("HeartbeatThread ends. running = " + running);
        }

        public void stopHeartbeat() {
            running = false;
        }

        public boolean isWorking() {
            return System.currentTimeMillis() - lastJingleTime < 2000;
        }
    }

    @Override
	public void update(DataUpdate update) {
    	synchronized (items) {
    		boolean error = !update.isStateOK() && null == update.getFields();
    		if (null == update.getFields()) {
    			update.setFields(new HashMap<String, Object>());
    		}
   			processUpdate(update.getDataSubscription(), (HashMap<String, Object>) update.getFields(), error);
    	}
    	processItemStatusUpdate(update.getDataSubscription(), update.isStateOK());
    }

    /**
     * Process update received from Reuters.
     *
     * @param itemName
     * @param fields
     * @param error <code>true</code> if an error update else <code>false</code>
     */
    private void processUpdate(String itemName, HashMap<String, Object> fields, boolean error) {
//    	log.trace("processUpdate - itemName: " + itemName);
        try {
            ArrayList<Opportunity> oppsToClose = new ArrayList<Opportunity>();
            if (fieldMarketsMap.containsKey(itemName)) { // handle error param
            	for (com.anyoption.service.level.market.Market market : fieldMarketsMap.get(itemName)) {
	            	log.trace("Updating market " + market.getMarketName());
	            	UpdateResult result = market.dataUpdate(itemName, fields, null);
	            	oppsToClose.addAll(result.getOpportunitiesToClose());
            	}
            } else {
                log.warn("Update about missing market: " + itemName);
            }
            if (oppsToClose.size() > 0) {
                log.debug("Have opps to close after this update. size: " + oppsToClose.size());
                Opportunity o = null;
                for (int i = 0; i < oppsToClose.size(); i++) {
                    try {
                        o = oppsToClose.get(i);
                        log.info("Closing opp: " + o.getId());
                        if (System.currentTimeMillis() - o.getTimeEstClosing().getTime() > o.getMarket().getNoAutoSettleAfter() * 60 * 1000) {
                            log.warn("More than " + o.getMarket().getNoAutoSettleAfter() + " min after est closing time passed. Not closing opp: " + o.getId());
                            sendNonSettlementEmail("", "More than " + o.getMarket().getNoAutoSettleAfter() + " min after est closing time passed. Opportunity id " + o.getId() + " not settled resettle it manually!");
                            continue;
                        }
                        com.anyoption.service.level.market.Market oppMarket = levelService.getMarketByOppId(o.getId());
                        boolean shouldBeClosed = false;
                        double closingLevel = 0;
                        String closingLevelTxt = null;
                        if (oppMarket != null) {
                        	shouldBeClosed = oppMarket.isShouldBeClosed(o.getId());
                        	closingLevel = oppMarket.getClosingLevel(o);
                        	closingLevelTxt = oppMarket.getClosingLevelTxt(o);
                        }

                        if (!shouldBeClosed) { //should not be closed
                            log.warn("no update for market " + o.getMarket().getId() + " for more then X sec Not closing opp: " + o.getId());
                            if (!o.isNotClosedAlertSent()) {
                                sendNonSettlementEmail("", "no update for market " + o.getMarket().getId() + " Opportunity id " + o.getId() + " not settled, no update for more then X sec. settle it manually!");
                                o.setNotClosedAlertSent(true);
                            }
                            continue;
                        }
//                        closingLevel = oppItem.getClosingLevel(o);
                        if (closingLevel == 0) {
                            log.warn("Opportunity closing level is 0. Not closing opp: " + o.getId() + " market: " + o.getMarket().getId());
                            sendNonSettlementEmail("", "market: " + o.getMarket().getId() + " Opportunity id " + o.getId() + " not settled, closing level is 0. resettle it manually!");
                            continue;
                        }
                        OpportunitiesManager.closeOpportunity(o.getId(), closingLevel, closingLevelTxt);
                        opportunityStateChanged(o.getId(), Opportunity.STATE_CLOSED);
//                      should call getClosingLevel first
                        log.debug("insert reuters qoutes for opp " + o.getId());
                        ReutersQuotesManagerBase.insert(o);
                    } catch (Throwable t) {
                        log.error("Can't close opp: " + (null != o ? o.getId() : "null"));
                    }
                }
            }
        } catch (Throwable t) {
            log.error("Failed to process update.", t);
        }
    }

	private void processItemStatusUpdate(String itemName, boolean isStateOK) {
//		log.trace("processItemStatusUpdate - itemName: " + itemName + " isStateOK: " + isStateOK);
		if (fieldMarketsMap.containsKey(itemName)) {
			for (com.anyoption.service.level.market.Market market : fieldMarketsMap.get(itemName)) {
				market.setStateOK(isStateOK);
			}
		} else {
			log.warn("processItemStatusUpdate for unknown " + itemName);
		}
	}

    /**
     * Checks on the markets if they all get updates.
     * If a market is found that didn't get updates for
     * certain period the trading on it is blocked and
     * alert is sent to the trader.
     *
     * @author Tony
     */
    class MonitoringThread extends Thread {
        private boolean running;
        private long lastJingleTime;
        private HashMap<Integer, String> invScheduled;
        @Override
		public void run() {
            Thread.currentThread().setName("Monitoring");
            running = true;
            invScheduled = new HashMap<Integer, String>();
            invScheduled.put(1, "Hourly");
            invScheduled.put(2, "Daily");
            invScheduled.put(3, "Weekly");
            invScheduled.put(4, "Monthly");
            invScheduled.put(5, "One touch");
            while (running) {
                lastJingleTime = System.currentTimeMillis();
                try {
                    String disabledMarkets = "";
                    String disabledMarketsDesc = "";
                    String enabledMarkets = "";
                    String closedMarkets = "";
                    String notClosedMarkets = "";
                    synchronized (items) {
                        com.anyoption.service.level.market.Market market = null;
                        for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                        	market = markets.get(i.next());
                            if (market.hasOpenedOpportunities()) {
                                String sd = market.shouldDisableOpportunities();
                                if (!sd.equals("")) {
                                    if (market.disableOpportunities("No updates from Reuters.")) {
                                        if (!disabledMarkets.equals("")) {
                                            disabledMarkets += ", ";
                                        }
                                        disabledMarkets += market.getMarketName();
                                        disabledMarketsDesc += market.getMarketName() + " " + sd + "<br />";
                                    }
                                }

                                if (market.shouldEnableOpportunities()) {
                                    if (market.enableOpportunities("Updates from Reuters resumed.", false)) {
                                        if (!enabledMarkets.equals("")) {
                                            enabledMarkets += ", ";
                                        }
                                        enabledMarkets += market.getMarketName();
                                    }
                                }

                                Opportunity o = null;
                                for (int l = 0; l < market.getOppsCount(); l++) {
                                    o = market.getOpp(l);

                                    // Check if opp didn't closed and is on market that is configured
                                    // to have them closed by monitoring if current level is calculated
                                    if (o.getState() == Opportunity.STATE_CLOSING &&
                                    		market.isShouldBeClosedByMonitoring() &&
                                    		market.isSnapshotReceived(o) &&
                                            System.currentTimeMillis() - o.getTimeEstClosing().getTime() >= closeAfterXSecNotClosed * 1000) {
                                        String marketName = o.getMarket().getName();
                                        if (getOpportunityClosingLevel(o.getId()) == 0) {
                                            log.warn("Opportunity closing level is 0. Not closing opp: " + o.getId());
                                            if (!o.isNotClosedAlertSent()) {
                                                sendNonSettlementEmail("", "Opportunity id " + o.getId() + " scheduled " + invScheduled.get(o.getScheduled()) + " market " + marketName + " not settled, closing level is 0. settle it manually!");
                                                o.setNotClosedAlertSent(true);
                                            }
                                        } else if (System.currentTimeMillis() - o.getTimeEstClosing().getTime() > o.getMarket().getNoAutoSettleAfter() * 60 * 1000) {
                                            log.warn("More than " + o.getMarket().getNoAutoSettleAfter() + " min after est closing time passed. Not closing opp: " + o.getId() + " , market: " + marketName);
                                            if (!o.isNotClosedAlertSent()) {
                                                sendNonSettlementEmail("", "More than " + o.getMarket().getNoAutoSettleAfter() + " min after est closing time passed. Market: " + marketName + " opportunity id " + o.getId() + " scheduled " + invScheduled.get(o.getScheduled()) + " not settled resettle it manually!");
                                                o.setNotClosedAlertSent(true);
                                            }
                                        } else if (!market.isShouldBeClosed(o.getId())) { //should not be closed
                                            log.warn("no update for market " + marketName + " for more then " + market.getMarketParams().getDontCloseAfterXSecNoUpdate() + " sec Not closing opp: " + o.getId());
                                            if (!o.isNotClosedAlertSent()) {
                                                sendNonSettlementEmail("", "Opportunity id " + o.getId() + " scheduled " + invScheduled.get(o.getScheduled()) + " not settled market: " + marketName + " , no update for more then " + market.getMarketParams().getDontCloseAfterXSecNoUpdate() + " sec. settle it manually!");
                                                o.setNotClosedAlertSent(true);
                                            }
                                        } else {
                                            log.warn("Opportunity " + o.getId() +
                                                    " scheduled " + invScheduled.get(o.getScheduled()) +
                                                    " on market " + marketName +
                                                    " not closed after " + closeAfterXSecNotClosed + " sec. Monitoring closing it.");
                                            o.setClosingLevelReceived(true);
                                            OpportunitiesManager.closeOpportunity(o.getId(), getOpportunityClosingLevel(o.getId()), getOpportunityClosingLevelTxt(o.getId()));
                                            opportunityStateChanged(o.getId(), Opportunity.STATE_CLOSED);
                                            log.debug("insert reuters qoutes for opp " + o.getId());
                                            ReutersQuotesManagerBase.insert(o);
                                            if (!closedMarkets.equals("")) {
                                                closedMarkets += ", ";
                                            }
                                            closedMarkets += market.getMarketName() + " " + o.getId();
                                        }
                                    }

                                    // Check if opp was not closed 5 min after it should have been
                                    if (o.getState() == Opportunity.STATE_CLOSING &&
                                            System.currentTimeMillis() - o.getTimeEstClosing().getTime() >= alertAfterXMinNotClosed * 60000) {
                                        String marketName = o.getMarket().getName();
                                        log.warn("Opportunity " + o.getId() +
                                                " scheduled " + invScheduled.get(o.getScheduled()) +
                                                " on market " + marketName +
                                                " not closed after " + alertAfterXMinNotClosed + " min.");
                                        OpportunitiesManager.publishOpportunity(o.getId(), false); // Set it to not published so it will not interfere with the trading area current opp selection
                                        opportunityStateChanged(o.getId(), Opportunity.STATE_DONE);
                                        if (!o.isNotClosedAlertSent()) {
                                            sendNonSettlementEmail("", "Opportunity " + o.getId() +
                                                                       " scheduled " + invScheduled.get(o.getScheduled()) +
                                                                       " on market " + marketName +
                                                                       " not closed after " + alertAfterXMinNotClosed + " min. settle it manually!");
                                            o.setNotClosedAlertSent(true);
                                        }
                                        if (!notClosedMarkets.equals("")) {
                                            notClosedMarkets += ", ";
                                        }
                                        notClosedMarkets += market.getMarketName() + " " + o.getId();
                                    }
                                }
                            }
                        }
                    }

                    if (!disabledMarkets.equals("")) {
                        sendAlertEmail("Disabled " + disabledMarkets, disabledMarketsDesc);
                    }
                    if (!enabledMarkets.equals("")) {
                        sendAlertEmail("Enabled " + enabledMarkets, "Opportunities enabled for markets: " + enabledMarkets + ".");
                    }
                    if (!closedMarkets.equals("")) {
                        sendAlertEmail("Closed by Monitoring " + closedMarkets,
                                "Opportunities on markets closed by Monitoring thread after they were not closed for " +
                                closeAfterXSecNotClosed + " sec: " + closedMarkets);
                    }
                    if (!notClosedMarkets.equals("")) {
                        sendAlertEmail("Not closed " + notClosedMarkets,
                                "Opportunities on markets not closed after " + alertAfterXMinNotClosed + " min: " + notClosedMarkets);
                    }
                } catch (Throwable t) {
                    log.error("Error monitoring markets.", t);
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ie) {
                    log.error("MonitoringThread sleep interrupted.", ie);
                }
            }
            log.warn("Monitoring thread finished.");
        }

        public void stopMonitoringThread() {
            running = false;
        }

        public boolean isWorking() {
            return System.currentTimeMillis() - lastJingleTime < 2000;
        }
    }

	public void sendOneTouchReportEmail(String subjectTail, String body) {
		emailSender.send(reportEmailRcpt, reportEmailSubjectOneTouch + " - " + subjectTail, body);
	}

	public void sendExceptionReportEmail(String subject, String body) {
		emailSender.send(reportEmailRcpt, subject, body);
	}

    /**
     * send snapshot for all items to LS, group all requests to snapshot in 1 sec to 1 snapshot
     *
     * @author Eyal
     */
    class SendSnapshot extends Thread {
        private boolean isSending;
        private long sleepTime;

        public SendSnapshot(long sleepTime) {
            isSending = false;
            this.sleepTime = sleepTime;
        }

        /**
         * @return the isSending
         */
        public boolean isSending() {
            return isSending;
        }

        @Override
		public void run() {
            isSending = false;
            Thread.currentThread().setName("sendSnapshot");
            try {
                Thread.sleep(sleepTime);
                isSending = true;
                log.info("Sending snapshot...");
                synchronized (items) {
                    for (Iterator<String> i = markets.keySet().iterator(); i.hasNext();) {
                        String key = i.next();
                        com.anyoption.service.level.market.Market market = markets.get(key);
                        // some markets like the currencies are attached to the currency RIC
                        // and to the options RICs so make sure you don't send one update
                        // for each key in the map that points to this market
                        if (market.getMarketName().equals(key)) {
                            try {
                            	market.sendUpdate(sender, lifeId, com.anyoption.service.level.market.Market.UPDATE_ALL_OPPS, true, false);
                            } catch (Throwable t) {
                                log.error("Can't send update for market: " + market.getMarketName(), t);
                            }
                        }
                    }
                }

                liveGlobe.sendSnapshot();
                liveInvestments.sendSnapshot();
                liveTrends.sendSnapshot();

                log.info("Sending snapshot done.");
            } catch (Throwable t) {
                log.error("thread failed to send snapshot");
            }
            synchronized (sendSnapshotThreadLock) {
                if (this == sendSnapshotThread) {
                    sendSnapshotThread = null;
                }
            }
        }
    }
}