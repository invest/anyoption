package il.co.etrader.service.level.reuters;

import com.anyoption.service.datasource.ServiceMBean;

public interface ReutersServiceMBean extends ServiceMBean {
    public String getInitialContextFactory();
    public void setInitialContextFactory(String initialContextFactory);

    public String getProviderURL();
    public void setProviderURL(String providerURL);

    public String getConnectionFactoryName();
    public void setConnectionFactoryName(String connectionFactoryName);

    public String getQueueName();
    public void setQueueName(String queueName);

    public String getTopicName();
    public void setTopicName(String topicName);

    public int getMsgPoolSize();
    public void setMsgPoolSize(int msgPoolSize);

    public int getRecoveryPause();
    public void setRecoveryPause(int recoveryPause);

//    public String getReutersSessionName();
//    public void setReutersSessionName(String reutersSessionName);
//
//    public String getReutersServiceName();
//    public void setReutersServiceName(String reutersServiceName);
//
//    public String getReutersUserName();
//    public void setReutersUserName(String reutersUserName);
//
//    public int getReutersSubscribeRetryPeriod();
//    public void setReutersSubscribeRetryPeriod(int reutersSubscribeRetryPeriod);
    
    public ReutersAttributes getAttributes();
    public void setAttributes(ReutersAttributes attributes);

//    public boolean isDelayedFeed();
//    public void setDelayedFeed(boolean delayedFeed);

    public boolean isHeartbeatAlive();
    public String getReutersConnectionState();
    public String getReutersConnectionText();
}