package il.co.etrader.service.level.reuters;

import com.anyoption.service.datasource.DataItem;
import com.reuters.rfa.common.Handle;

/**
 * @author kirilim
 */
public class ReutersItem extends DataItem {

	private Handle handle;

	public ReutersItem(String dataSubscription) {
		super(dataSubscription);
	}

	public Handle getHandle() {
		return handle;
	}

	public void setHandle(Handle handle) {
		this.handle = handle;
	}
}