package il.co.etrader.service.level.reuters;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.reuters.rfa.dictionary.DictionaryException;
import com.reuters.rfa.dictionary.FieldDictionary;

/**
 * The GenericOMMParser is used to read and initialize dictionaries.
 */
public final class OMMParser {
	private static final Logger log = Logger.getLogger(OMMParser.class);
	
	private static HashMap<Integer, FieldDictionary> DICTIONARIES = new HashMap<Integer, FieldDictionary>();

	/**
	 * This method should be called one before parsing and data.
	 * 
	 * @param fieldDictionaryFilename
	 * @param enumDictionaryFilename
	 * @throws DictionaryException
	 *             if an error has occurred
	 */
	public static void initializeDictionary(String fieldDictionaryFilename, String enumDictionaryFilename) throws DictionaryException {
		FieldDictionary dictionary = FieldDictionary.create();
		FieldDictionary.readRDMFieldDictionary(dictionary, fieldDictionaryFilename);
		log.info("field dictionary read from RDMFieldDictionary file");

		FieldDictionary.readEnumTypeDef(dictionary, enumDictionaryFilename);
		log.info("enum dictionary read from enumtype.def file");

		initializeDictionary(dictionary);
	}

	// This method can be used to initialize a downloaded dictionary
	public synchronized static void initializeDictionary(FieldDictionary dict) {
		int dictId = dict.getDictId();
		if (dictId == 0)
			dictId = 1; // RDMFieldDictionary has a DictionaryId of 1;
						// Since our examples use the RDMFieldDictionary,
						// use 1 for DictionaryId, if it is 0
		DICTIONARIES.put(new Integer(dictId), dict);
	}

	public static FieldDictionary getDictionary(int dictId) {
		if (dictId == 0)
			dictId = 1;
		return (FieldDictionary) DICTIONARIES.get(new Integer(dictId));
	}
}