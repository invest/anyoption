package il.co.etrader.service.level.reuters;

/**
 * @author kirilim
 */
public class ReutersAttributes {

	private boolean delayedFeed;
	private String reutersSessionName;
	private String reutersServiceName;
	private String reutersUserName;
	private int reutersSubscribeRetryPeriod;
	private String fieldDictionaryFilename;
	private String enumDictionaryFilename;

	public boolean isDelayedFeed() {
		return delayedFeed;
	}

	public void setDelayedFeed(boolean delayedFeed) {
		this.delayedFeed = delayedFeed;
	}

	public String getReutersSessionName() {
		return reutersSessionName;
	}

	public void setReutersSessionName(String reutersSessionName) {
		this.reutersSessionName = reutersSessionName;
	}

	public String getReutersServiceName() {
		return reutersServiceName;
	}

	public void setReutersServiceName(String reutersServiceName) {
		this.reutersServiceName = reutersServiceName;
	}

	public String getReutersUserName() {
		return reutersUserName;
	}

	public void setReutersUserName(String reutersUserName) {
		this.reutersUserName = reutersUserName;
	}

	public int getReutersSubscribeRetryPeriod() {
		return reutersSubscribeRetryPeriod;
	}

	public void setReutersSubscribeRetryPeriod(int reutersSubscribeRetryPeriod) {
		this.reutersSubscribeRetryPeriod = reutersSubscribeRetryPeriod;
	}
	
	public String getFieldDictionaryFilename() {
		return fieldDictionaryFilename;
	}

	public void setFieldDictionaryFilename(String fieldDictionaryFilename) {
		this.fieldDictionaryFilename = fieldDictionaryFilename;
	}

	public String getEnumDictionaryFilename() {
		return enumDictionaryFilename;
	}

	public void setEnumDictionaryFilename(String enumDictionaryFilename) {
		this.enumDictionaryFilename = enumDictionaryFilename;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "ReutersAttributes: " + ls
				+ super.toString() + ls
				+ "delayedFeed: " + delayedFeed + ls
				+ "reutersSessionName: " + reutersSessionName + ls
				+ "reutersServiceName: " + reutersServiceName + ls
				+ "reutersUserName: " + reutersUserName + ls
				+ "reutersSubscribeRetryPeriod: " + reutersSubscribeRetryPeriod + ls
				+ "fieldDictionaryFilename: " + fieldDictionaryFilename + ls
				+ "enumDictionaryFilename: " + enumDictionaryFilename + ls;
	}
}