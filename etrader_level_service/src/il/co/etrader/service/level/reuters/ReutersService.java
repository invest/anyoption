package il.co.etrader.service.level.reuters;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.service.datasource.DataItem;
import com.anyoption.service.datasource.DataUpdate;
import com.anyoption.service.datasource.DataUpdatesListener;
import com.anyoption.service.datasource.Service;
import com.anyoption.service.level.formula.FormulaConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.reuters.rfa.common.Client;
import com.reuters.rfa.common.Context;
import com.reuters.rfa.common.DispatchException;
import com.reuters.rfa.common.Event;
import com.reuters.rfa.common.EventQueue;
import com.reuters.rfa.common.EventSource;
import com.reuters.rfa.common.Handle;
import com.reuters.rfa.common.StandardPrincipalIdentity;
import com.reuters.rfa.common.TokenizedPrincipalIdentity;
import com.reuters.rfa.dictionary.DictionaryException;
import com.reuters.rfa.dictionary.FidDef;
import com.reuters.rfa.dictionary.FieldDictionary;
import com.reuters.rfa.omm.OMMData;
import com.reuters.rfa.omm.OMMElementList;
import com.reuters.rfa.omm.OMMEncoder;
import com.reuters.rfa.omm.OMMEntry;
import com.reuters.rfa.omm.OMMFieldEntry;
import com.reuters.rfa.omm.OMMFieldList;
import com.reuters.rfa.omm.OMMIterable;
import com.reuters.rfa.omm.OMMMsg;
import com.reuters.rfa.omm.OMMNumeric;
import com.reuters.rfa.omm.OMMPool;
import com.reuters.rfa.omm.OMMState;
import com.reuters.rfa.omm.OMMTypes;
import com.reuters.rfa.rdm.RDMInstrument;
import com.reuters.rfa.rdm.RDMMsgTypes;
import com.reuters.rfa.rdm.RDMUser;
import com.reuters.rfa.session.Session;
import com.reuters.rfa.session.event.ConnectionStatus;
import com.reuters.rfa.session.omm.OMMConnectionEvent;
import com.reuters.rfa.session.omm.OMMConsumer;
import com.reuters.rfa.session.omm.OMMItemEvent;
import com.reuters.rfa.session.omm.OMMItemIntSpec;

public class ReutersService extends Service implements ReutersServiceMBean {
    private static final Logger log = Logger.getLogger(ReutersService.class);
    
    public static final String REUTERS_DATE_FIELD_PATTERN = "dd MMM yyyy";

    // The value in the maps is if to send the field in the update to the LevelService even if the value didn't change
    // true - send the field even if it didn't change
    // false - don't send the field if it didn't change
    // This is done because some markets need to know when we receive deal level even if the level didn't change.
    private static Map<String, Boolean> doubleFields;
    static {
        doubleFields = new HashMap<String, Boolean>();
        doubleFields.put("ASK", false);
        doubleFields.put("BID", false);
        doubleFields.put("TRDPRC_1", true);
        doubleFields.put("HST_CLOSE", false);
//      doubleFields.put("HST_CLOSE2", false);
        doubleFields.put("OFF_CLOSE", false);
        doubleFields.put("PRIMACT_1", false);
        doubleFields.put("BID_1", false);
        doubleFields.put("GEN_VAL1", false);
        doubleFields.put("IRGPRC", false);
    }
    
    public static Map<String, Boolean> dateFields;
    static {
        dateFields = new HashMap<String, Boolean>();
        dateFields.put("HSTCLSDATE", false);
        dateFields.put("ACTIV_DATE", false);
        dateFields.put("VALUE_DT1", false);
        dateFields.put("EXPIR_DATE", false);
        dateFields.put("GV1_DATE", false);
    }
    
    private static Map<String, Boolean> timeFields;
    static {
        timeFields = new HashMap<String, Boolean>();
        timeFields.put("TRDTIM_1", false);
        timeFields.put("SALTIM", false);
        timeFields.put("VALUE_TS1", false);
        timeFields.put("TIMACT", false);
        timeFields.put("EXCHTIM", false);
    }
    
    private static Map<String, Boolean> stringFields;
    static {
        stringFields = new HashMap<String, Boolean>();
        stringFields.putAll(dateFields);
        stringFields.putAll(timeFields);
    }
    
    private static Map<String, Boolean> usedFields;
    static {
        usedFields = new HashMap<String, Boolean>();
        usedFields.putAll(doubleFields);
        usedFields.putAll(stringFields);
    }
    
    // JMS properties
//    private boolean internal;
//    private boolean delayedFeed;
//    private String initialContextFactory;
//    private String providerURL;
//    private String connectionFactoryName;
//    private String queueName;
//    private String topicName;
//    private String webTopicName;
//    private int msgPoolSize;
//    private int recoveryPause;

    // Reuters configuration
//    private String reutersSessionName;
//    private String reutersServiceName;
//    private String reutersUserName;
//    private int reutersSubscribeRetryPeriod;

    private ReutersAttributes attributes;

    // RFA objects
    protected StandardPrincipalIdentity _standardPI;
    protected TokenizedPrincipalIdentity _tokenizedPI;
    protected EventQueue eventQueue;
    protected Session session;
    protected OMMConsumer consumer;
    protected OMMEncoder encoder;
    protected OMMPool pool;
    protected LoginClient loginClient = new LoginClient();
    protected Handle loginHandle;
    private DispatcherThread dispatcher;

    //this object handles comunications with JMS. Hides the use of Session, Connections, Publishers etc..
//    private JMSHandler jmsHandler;
//    private HeartbeatThread heartbeat;
//    private ConnectionLoopTPQR connector;
//    private SenderThread sender;

    // state
    private String reutersConnectionState;
    private String reutersConnectionText;
//    private long lifeId;
//    private Map<String, DataItem> items;
//    private DataUpdatesListener reutersUpdatesListener;
    
    private BitcoinUpdatesThread bitcoinUpdatesThread;
    private BitcoinUpdatesThread litecoinUpdatesThread;

    public ReutersService(String dataServiceAttributes, boolean internal, DataUpdatesListener dataUpdatesListener, long id) {
    	// TODO handle dataServiceAttributes
    	this.id = id;
    	Gson gson = new GsonBuilder().serializeNulls().create();
    	attributes = gson.fromJson(dataServiceAttributes, ReutersAttributes.class);
    	log.info(attributes);
    	this.internal = internal;
    	this.dataUpdatesListener = dataUpdatesListener;
    }

    public void startService() {
        log.info("ReutersService ========== startService begin");
        items = new HashMap<String, DataItem>();
        
        if (!internal) {
            startupJMS();
        }
        
        startSubscription();
        log.info("ReutersService ========== startService end");
    }
    
    public void stopService() {
        log.info("ReutersService ========== stopService begin");
        stopSubscription();
        
    	if (!internal) {
    	    stopJMS();
    	}
        log.info("ReutersService ========== stopService end");
    }

//    private void startupJMS() {
//        log.info("ReutersService ========== startupJMS");
//
//        lifeId = System.currentTimeMillis();
//
//        //instantiate a JMSHandler that will handle LightStreamer updates sending and commands Queue receiving
//        jmsHandler = new JMSHandler("RS", initialContextFactory, providerURL, connectionFactoryName, queueName, connectionFactoryName, topicName);
//        jmsHandler.setListener(this);
//
//        //start the loop that tries to connect to JMS
//        connector = new ConnectionLoopTPQR(jmsHandler, recoveryPause);
//        connector.start();
//
//        sender = new SenderThread(jmsHandler, false);
//        sender.start();
//
//        heartbeat = new HeartbeatThread();
//        heartbeat.start();
//    }
//    
//    private void stopJMS() {
//        log.info("ReutersService ========== stopJMS");
//
//        heartbeat.stopHeartbeat();
//        heartbeat = null;
//
//        if (null != sender) {
//            sender.stopSenderThread();
//        }
//        sender = null;
//
//        if (null != connector) {
//            connector.abort();
//        }
//        connector = null;
//
//        jmsHandler.close();
//        jmsHandler = null;
//    }
    
    protected void startSubscription() {
        log.info("ReutersService ========== startupReuters");

        Context.initialize();

        //Create an Event Queue
        eventQueue = EventQueue.create("etraderEventQueue");

        session = Session.acquire(attributes.getReutersSessionName());
        if (null == session) {
            log.fatal("Could not acquire session.");
            Context.uninitialize();
            return;
        }
        
        consumer = (OMMConsumer) session.createEventSource(EventSource.OMM_CONSUMER, "myOMMConsumer", true);

        try  {
            OMMParser.initializeDictionary(attributes.getFieldDictionaryFilename(), attributes.getEnumDictionaryFilename());
        } catch (DictionaryException ex) {
        	log.fatal("Unable to initialize dictionaries", ex);
//    		cleanup();
    		return;
    	}

        pool = OMMPool.create();
        encoder = pool.acquireEncoder();
                
        sendLoginRequest();
    }
    
    protected void sendLoginRequest() {
        String application = "256";
        String position = "1.1.1.1/net";
        try {
        	position = InetAddress.getLocalHost().getHostAddress() + "/" + InetAddress.getLocalHost().getHostName();
        }  catch (Exception e) {
          log.fatal("Can't prepare position.", e);
          return;
        }

        encoder.initialize(OMMTypes.MSG, 500);
        OMMMsg msg = pool.acquireMsg();
        msg.setMsgType(OMMMsg.MsgType.REQUEST);
        msg.setMsgModelType(RDMMsgTypes.LOGIN);
        msg.setIndicationFlags(OMMMsg.Indication.REFRESH);
        msg.setAttribInfo(null, attributes.getReutersUserName(), RDMUser.NameType.USER_NAME);

        encoder.encodeMsgInit(msg, OMMTypes.ELEMENT_LIST, OMMTypes.NO_DATA);
        encoder.encodeElementListInit(OMMElementList.HAS_STANDARD_DATA, (short)0, (short) 0);
        encoder.encodeElementEntryInit(RDMUser.Attrib.ApplicationId, OMMTypes.ASCII_STRING);
        encoder.encodeString(application, OMMTypes.ASCII_STRING);
        encoder.encodeElementEntryInit(RDMUser.Attrib.Position, OMMTypes.ASCII_STRING);
        encoder.encodeString(position, OMMTypes.ASCII_STRING);
        encoder.encodeElementEntryInit(RDMUser.Attrib.Role, OMMTypes.UINT);
	    encoder.encodeUInt((long)RDMUser.Role.CONSUMER);
        encoder.encodeAggregateComplete();

        //Get the encoded message from the encoder
        OMMMsg ommmsg = (OMMMsg) encoder.getEncodedObject();

        //Release the message that own by the application
        pool.releaseMsg(msg);

        OMMItemIntSpec ommItemIntSpec = new OMMItemIntSpec();
        ommItemIntSpec.setMsg(ommmsg);
        log.info("Sending login request");
        loginHandle = consumer.registerClient(eventQueue, ommItemIntSpec, loginClient, null);

        dispatcher = new DispatcherThread();
        dispatcher.setDaemon(true);
        dispatcher.start();
    }
    
    protected void stopSubscription() {
        log.info("ReutersService ========== stopReuters");

        if (null != bitcoinUpdatesThread) {
            bitcoinUpdatesThread.stopBitcoinUpdatesThread();
            bitcoinUpdatesThread = null;
        }
        if (null != litecoinUpdatesThread) {
            litecoinUpdatesThread.stopBitcoinUpdatesThread();
            litecoinUpdatesThread = null;
        }

        eventQueue.deactivate();

        // TODO: unsubscribe all items

    	if (loginHandle != null) {
	        consumer.unregisterClient(loginHandle);
	        loginHandle = null;
    	}
    	
    	eventQueue.destroy();
    	
    	if (consumer != null) {
			consumer.destroy();
    	}

        session.release();
        session = null;

        Context.uninitialize();
        
        dispatcher.stopDispatcher();
        dispatcher = null;
    }

//	@Override
//	protected void startSubscription() {
////		startupReuters();
//	}

//	@Override
//	protected void stopSubscription() {
////		stopReuters();
//	}

    /**
     * receive messages from JMSHandler
     */
//    public void onMessage(Message message) {
//        String feedMsg = null;
//        log.debug("ReutersService Message received: processing...");
//        try {
//            //pull out text from the Message object
//            TextMessage textMessage = (TextMessage) message;
//            feedMsg = textMessage.getText();
//            if (log.isInfoEnabled()) {
//                log.info("TextMessage received: " + feedMsg);
//            }
//        } catch (ClassCastException cce) {
//            // if message isn't a TextMessage then this update is not "correct"
//            log.warn("Unknown message (ClassCastException)");
//            return;
//        } catch (JMSException jmse) {
//            log.error("", jmse);
//            return;
//        }
//
//        String[] cmd = feedMsg.split("_");
//        if (cmd[0].equals("subscribe")) {
//            subscribeItem(cmd[1]);
//            return;
//        } else if (cmd[0].equals("unsubscribe")) {
//            unsubscribeItem(cmd[1]);
//            return;
//        }
//
//        log.warn("Unknown message. Message: " + feedMsg);
//    }
//
//    public void onException(JMSException jmse) {
//        jmsHandler.reset();
//        connector = new ConnectionLoopTPQR(jmsHandler, recoveryPause);
//        connector.start();
//    }
    
    public void subscribeItem(String itemName) {
    	DataUpdate update = null;
        synchronized (items) {
			DataItem item = items.get(itemName);
			if (null == item) {
				log.info("Subscribing to Reuters: " + itemName);
				item = new ReutersItem(itemName);
				if (itemName.equals(BitcoinUpdatesThread.BITCOIN_FEED_NAME)) {
					bitcoinUpdatesThread = new BitcoinUpdatesThread(this, itemName);
					bitcoinUpdatesThread.start();
				} else if (itemName.equals(BitcoinUpdatesThread.LITECOIN_FEED_NAME)) {
					litecoinUpdatesThread = new BitcoinUpdatesThread(this, itemName);
					litecoinUpdatesThread.start();
				} else {
			        OMMItemIntSpec ommItemIntSpec = new OMMItemIntSpec();

			        OMMMsg ommmsg = pool.acquireMsg();
			        ommmsg.setMsgType(OMMMsg.MsgType.REQUEST);
			        ommmsg.setMsgModelType(RDMMsgTypes.MARKET_PRICE);
			        ommmsg.setIndicationFlags(OMMMsg.Indication.REFRESH | OMMMsg.Indication.ATTRIB_INFO_IN_UPDATES);
			        ommmsg.setPriority((byte) 1, 1);
			        if (loginHandle != null) {
			        	ommmsg.setAssociatedMetaInfo(loginHandle);
			        }
		        	ommmsg.setAttribInfo(attributes.getReutersServiceName(), itemName, RDMInstrument.NameType.RIC);
		            ommItemIntSpec.setMsg(ommmsg);

		            ((ReutersItem) item).setHandle(consumer.registerClient(eventQueue, ommItemIntSpec, new ReutersClient(itemName), null));
			        pool.releaseMsg(ommmsg);
				}
				item.setSubscribeCount(1);
				items.put(itemName, item);
			} else {
				item.setSubscribeCount(item.getSubscribeCount() + 1);
				log.info(itemName + " already subscribed to Retuers. Current count: " + item.getSubscribeCount());
				update = new DataUpdate(itemName);
				update.setStateOK(item.isStateOK());
				update.setFields(item.getFieldsCache()); // TODO: clone?
			}
        }
        if (null != update) {
            sendUpdate(update); // Send snapshot because of the new client
        }
    }
    
    public void unsubscribeItem(String itemName) {
        synchronized (items) {
            DataItem item = items.get(itemName);
            if (null != item) {
                item.setSubscribeCount(item.getSubscribeCount() - 1);
                log.info("Unsubscribe item. Current count: " + item.getSubscribeCount());
                if (item.getSubscribeCount() == 0) {
                    if (itemName.equals(BitcoinUpdatesThread.BITCOIN_FEED_NAME)) {
                        bitcoinUpdatesThread.stopBitcoinUpdatesThread();
                        bitcoinUpdatesThread = null;
                    } else if (itemName.equals(BitcoinUpdatesThread.LITECOIN_FEED_NAME)) {
                        litecoinUpdatesThread.stopBitcoinUpdatesThread();
                        litecoinUpdatesThread = null;
                    } else {
                        log.info("Unsubscribe from Reuters: " + itemName);
                        consumer.unregisterClient(((ReutersItem)item).getHandle());
                    }
                    items.remove(itemName);
                }
            } else {
                log.warn("Unsubscribe for missing item: " + itemName);
            }
        }
    }
    
//    void sendUpdate(DataUpdate update) {
//    	log.trace(update);
//    	if (update.getRic().equals(BitcoinUpdatesThread.BITCOIN_FEED_NAME) || update.getRic().equals(BitcoinUpdatesThread.LITECOIN_FEED_NAME)) {
//    	    // cache the update since it doesn't go the Reuters way so we can send snapshots later
//            synchronized (items) {
//                DataItem item = items.get(update.getRic());
//                if (null != item) {
//                    item.getFieldsCache().putAll(update.getFields());
//                }
//            }
//    	}
//        if (internal) {
//            if (null != reutersUpdatesListener) {
//                reutersUpdatesListener.update(update);
//            } else {
//                log.warn("No listener.");
//            }
//        } else {
//            sender.send(update);
//        }
//    }
    
//	public String getConnectionFactoryName() {
//		return connectionFactoryName;
//	}
//	
//	public void setConnectionFactoryName(String connectionFactoryName) {
//		this.connectionFactoryName = connectionFactoryName;
//	}
//
//	public String getInitialContextFactory() {
//		return initialContextFactory;
//	}
//
//	public void setInitialContextFactory(String initialContextFactory) {
//		this.initialContextFactory = initialContextFactory;
//	}
//
//	public String getProviderURL() {
//		return providerURL;
//	}
//
//	public void setProviderURL(String providerURL) {
//		this.providerURL = providerURL;
//	}
//
//	public String getQueueName() {
//		return queueName;
//	}
//
//	public void setQueueName(String queueName) {
//		this.queueName = queueName;
//	}
//
//	public String getTopicName() {
//		return topicName;
//	}
//
//	public void setTopicName(String topicName) {
//		this.topicName = topicName;
//	}
//
//	public String getWebTopicName() {
//		return webTopicName;
//	}
//
//	public void setWebTopicName(String webTopicName) {
//		this.webTopicName = webTopicName;
//	}
//
//	public int getMsgPoolSize() {
//		return msgPoolSize;
//	}
//
//	public void setMsgPoolSize(int msgPoolSize) {
//		this.msgPoolSize = msgPoolSize;
//	}
//
//	public int getRecoveryPause() {
//		return recoveryPause;
//	}
//
//	public void setRecoveryPause(int recoveryPause) {
//		this.recoveryPause = recoveryPause;
//	}

//	public String getReutersSessionName() {
//		return reutersSessionName;
//	}
//
//	public void setReutersSessionName(String reutersSessionName) {
//		this.reutersSessionName = reutersSessionName;
//	}
//
//	public String getReutersServiceName() {
//		return reutersServiceName;
//	}
//
//	public void setReutersServiceName(String reutersServiceName) {
//		this.reutersServiceName = reutersServiceName;
//	}
//
//	public String getReutersUserName() {
//		return reutersUserName;
//	}
//
//	public void setReutersUserName(String reutersUserName) {
//		this.reutersUserName = reutersUserName;
//	}
//
//    public int getReutersSubscribeRetryPeriod() {
//		return reutersSubscribeRetryPeriod;
//	}
//
//	public void setReutersSubscribeRetryPeriod(int reutersSubscribeRetryPeriod) {
//		this.reutersSubscribeRetryPeriod = reutersSubscribeRetryPeriod;
//	}

//	public boolean isInternal() {
//        return internal;
//    }
//
//    public void setInternal(boolean internal) {
//        this.internal = internal;
//    }
//    
//    public boolean isDelayedFeed() {
//        return delayedFeed;
//    }
//    public void setDelayedFeed(boolean delayedFeed) {
//        this.delayedFeed = delayedFeed;
//    }

    public int getDataUpdatesQueueSize() {
        return dispatcher.getQueueSize();
    }

//    public boolean isHeartbeatAlive() {
//        return heartbeat.isWorking();
//    }

    public String getReutersConnectionState() {
        return reutersConnectionState;
    }

    public String getReutersConnectionText() {
        return reutersConnectionText;
    }

//    public DataUpdatesListener getReutersUpdatesListener() {
//        return reutersUpdatesListener;
//    }
//
//    public void setReutersUpdatesListener(DataUpdatesListener reutersUpdatesListener) {
//        this.reutersUpdatesListener = reutersUpdatesListener;
//    }

    public static boolean isDoubleField(String fieldName) {
        return doubleFields.keySet().contains(fieldName);
    }
    
    /**
     * Reuters queue dispatcher.
     *
     * @author Tony
     */
    private class DispatcherThread extends Thread {
        private boolean running;
        private int lastEventsInTheQueue;

        public void run() {
            Thread.currentThread().setName("Dispatcher");
            running = true;
            lastEventsInTheQueue = -1;
            int eventsInTheQueue = -1;
            while (running) {
                try {
                    eventsInTheQueue = eventQueue.dispatch(100);
                } catch (DispatchException de) {
                    log.error("Queue deactivated", de);
                    break;
                }
                if (eventsInTheQueue > 100) {
                    log.warn("queue: " + eventsInTheQueue);
                } else if ((eventsInTheQueue != -1 || eventsInTheQueue != lastEventsInTheQueue) && log.isTraceEnabled()) {
                    log.trace("queue: " + eventsInTheQueue);
                }
                lastEventsInTheQueue = eventsInTheQueue;
                Thread.yield();
            }
            log.warn(Context.string());
            log.warn("Daemon " + getClass().toString() + " exiting");
        }

        public void stopDispatcher() {
            running = false;
        }

        public int getQueueSize() {
            return lastEventsInTheQueue;
        }
    }
	
    class LoginClient implements Client {
	    public void processEvent(Event event) {
			log.info("Received Login event...");
			if (event.getType() == Event.COMPLETION_EVENT) {
				log.info("Receive a COMPLETION_EVENT, " + event.getHandle());
				return;
			}

			log.info("Received Login Response...");

			OMMItemEvent ie = (OMMItemEvent) event;
			OMMMsg respMsg = ie.getMsg();

			// The login is unsuccessful, RFA forwards the message from the network
			if (respMsg.isFinal()) {
				log.fatal("Login Response message is final.");
				reutersConnectionState = "DOWN";
				return;
			}

			// The login is successful, RFA forwards the message from the network
			if ((respMsg.getMsgType() == OMMMsg.MsgType.STATUS_RESP) && (respMsg.has(OMMMsg.HAS_STATE))
					&& (respMsg.getState().getStreamState() == OMMState.Stream.OPEN)
					&& (respMsg.getState().getDataState() == OMMState.Data.OK)) {
				log.info("Received Login STATUS OK Response");
	            reutersConnectionState = "UP";
			} else { // This message is sent by RFA indicating that RFA is processing the login
				log.info("Received Login Response - " + OMMMsg.MsgType.toString(respMsg.getMsgType()));
			}
	    }    	
    }
    
	class ReutersClient implements Client {
		private String itemName;
		private boolean stateOK;
		
		ReutersClient(String itemName) {
			this.itemName = itemName;
			stateOK = false;
		}
		
	    public void processEvent(Event event) {
	        try {
	            switch (event.getType()) {
// TODO: Maybe remove the code for handling connection events
	            case Event.OMM_CONNECTION_EVENT:
	            	processConnectionEvent((OMMConnectionEvent) event);
	            	break;
	            case Event.OMM_ITEM_EVENT:
	                stateOK = processItemEvent(itemName, (OMMItemEvent) event, stateOK);
	                break;
	            case Event.COMPLETION_EVENT:
	                if (log.isInfoEnabled()) {
	                    log.info("Received COMPLETION_EVENT for handle " + event.getHandle());
	                }
	                break;
	            default:
	                log.warn("Unhandled event type: " + event.getType());
	                break;
	            }
	        } catch (Exception e) {
	            log.error("Failed to process Reuters event.", e);
	        }
	    }

	    protected void processConnectionEvent(OMMConnectionEvent connectionEvent) {
	        if (log.isInfoEnabled()) {
	            log.info("Received CONNECTION_EVENT: " + connectionEvent.getConnectionName() + " " + connectionEvent.getConnectionStatus().toString());
	        }
	        if (connectionEvent.getConnectionStatus().getState() == ConnectionStatus.UP) {
	            reutersConnectionState = "UP";
	        } else {
	            reutersConnectionState = "DOWN";
	        }
	        reutersConnectionText = connectionEvent.getConnectionStatus().getStatusText();
	    }
	}

    protected boolean processItemEvent(String itemName, OMMItemEvent itemEvent, boolean stateOK) {
    	boolean shouldSendUpdate = false;
    	OMMMsg msg = itemEvent.getMsg();
    	if (msg.has(OMMMsg.HAS_STATE)) {
    		boolean tmpStateOK = msg.getState().getDataState() == OMMState.Data.OK
    				|| (msg.getState().getDataState() == OMMState.Data.NO_CHANGE && stateOK);
    		if (stateOK != tmpStateOK) {
    			shouldSendUpdate = true;
    		}
    		stateOK = tmpStateOK;
    	}

        if (log.isDebugEnabled()) {
        	StringBuilder sb = new StringBuilder();
        	sb.append("item = ").append(itemName);
        	if (msg.has(OMMMsg.HAS_STATE)) {
        		sb.append(" state = ").append(msg.getState());
        	}
        	if (msg.getDataType() != OMMTypes.NO_DATA) {
        		sb.append(" dataSize = ").append(msg.getPayload().getEncodedLength());
        	}
        	log.debug(sb.toString());
        }

        if ((msg.getMsgType() == OMMMsg.MsgType.REFRESH_RESP || msg.getMsgType() == OMMMsg.MsgType.UPDATE_RESP) // snapshot or update
        		&& msg.getMsgModelType() == RDMMsgTypes.MARKET_PRICE && null != itemName) {
        	OMMData data = msg.getPayload();
        	if (data.isBlank() || data.getType() != OMMTypes.FIELD_LIST) {
        		return stateOK;
        	}

//        	long startTimeStamp = System.currentTimeMillis();
            
            if (attributes.isDelayedFeed() && itemName.startsWith("/")) {
            	itemName = itemName.substring(1);
            }
            DataUpdate update = null;
            synchronized (items) {
	            DataItem item = items.get(itemName);
	            if (null == item) {
	                log.warn("Update for item that is not subscribed - " + itemName);
	                return stateOK;
	            }
	            update = new DataUpdate(itemName);
                try {
                    Map<String, Object> fields = new HashMap<String, Object>();
                    StringBuffer sb = null;
                    if (log.isTraceEnabled()) {
                        sb = new StringBuffer();
                        sb.append('\n').append(itemName).append('\n');
                    }
                	FieldDictionary dict = OMMParser.getDictionary(((OMMFieldList) data).getDictId());
                    Iterator<OMMEntry> iter = ((OMMIterable) data).iterator();
                    Map<String, Object> ricFields = (HashMap<String, Object>) item.getFieldsCache();
                    while (iter.hasNext()) {
                    	OMMFieldEntry entry = (OMMFieldEntry) iter.next();
                    	FidDef fiddef = dict.getFidDef(entry.getFieldId());
                    	OMMData ed = entry.getData(fiddef.getOMMType());
                    	if (log.isTraceEnabled()) {
                    		sb.append(String.format("%1$-16s", fiddef.getName())).append(": ").append(ed).append('\n');
                    	}
                        if (usedFields.keySet().contains(fiddef.getName())) {
                        	Object o = null;
                            if (ed instanceof OMMNumeric) {
                                Double fData = ((OMMNumeric) ed).toDouble();
                                if (fData == 0) {
                                    continue;
                                }
                                o = fData;
                            } else { // convert the rest to String
                                String fDataTxt = ed.toString();
                                if (null == fDataTxt || (null != fDataTxt && fDataTxt.length() == 0)) {
                                    continue;
                                }
                                o = fDataTxt;
                            }
                            if (null != ricFields) {
                                if (ricFields.containsKey(fiddef.getName())) {
                                    Object ricFieldValue = ricFields.get(fiddef.getName());
                                    if (ricFieldValue.equals(o) && !usedFields.get(fiddef.getName())) { // !usedFields.get(field.Name()) in other words "don't send the field if it didn't change"
                                        continue;
                                    }
                                }
                                ricFields.put(fiddef.getName(), o);
                            }
                            fields.put(fiddef.getName(), o);
                        }
                    }
                    item.setFieldsCache(ricFields);
                    if (fields.size() > 0) {
                        if (log.isTraceEnabled()) {
                            log.trace(sb.toString());
                        }
                        update.setFields(fields);
                        shouldSendUpdate = true;
//                    } else {
//                        if (log.isTraceEnabled()) {
//                            log.trace("No interesting fields.");
//                        }
                    }
                } catch (Exception e) {
                    log.error("Unable to unpack data.");
                }
                update.setStateOK(stateOK);
            }
            if (shouldSendUpdate) {
                sendUpdate(update);
            }
//            if (log.isTraceEnabled()) {
//                log.trace("Event processed in: " + (System.currentTimeMillis() - startTimeStamp));
//            }
        }
        return stateOK;
    }

    /**
     * Daemon thread that will retry to subscribe specified item to reuters in 1 min.
     *
     * @author Tony
     */
    class ReutersSubscribeRetryThread extends Thread {
        private String itemName;

        public ReutersSubscribeRetryThread(String itemName) {
            this.itemName = itemName;
            setDaemon(true);
        }

        public void run() {
            Thread.currentThread().setName("Resubscribe " + itemName);
            try {
                synchronized (items) {
                    DataItem item = items.get(itemName);
                    if (null != item) {
                        consumer.unregisterClient(((ReutersItem) item).getHandle());
                        ((ReutersItem)item).setHandle(null);
                    } else {
                        log.error("Abort retry to subscribe from reuters " + itemName + " no item!");
                        return;
                    }
                }
                
                Thread.sleep(attributes.getReutersSubscribeRetryPeriod() * 60000);

                synchronized (items) {
	                DataItem item = items.get(itemName);
	                if (null != item) {
	                    if (log.isInfoEnabled()) {
	                        log.info("Retry to subscribe from reuters " + itemName);
	                    }
				        OMMItemIntSpec ommItemIntSpec = new OMMItemIntSpec();

				        OMMMsg ommmsg = pool.acquireMsg();
				        ommmsg.setMsgType(OMMMsg.MsgType.REQUEST);
				        ommmsg.setMsgModelType(RDMMsgTypes.MARKET_PRICE);
				        ommmsg.setIndicationFlags(OMMMsg.Indication.REFRESH);
				        ommmsg.setPriority((byte) 1, 1);
				        if (loginHandle != null) {
				        	ommmsg.setAssociatedMetaInfo(loginHandle);
				        }
			        	ommmsg.setAttribInfo(attributes.getReutersServiceName(), itemName, RDMInstrument.NameType.RIC);
			            ommItemIntSpec.setMsg(ommmsg);

			            ((ReutersItem) item).setHandle(consumer.registerClient(eventQueue, ommItemIntSpec, new ReutersClient(itemName), null));
				        pool.releaseMsg(ommmsg);

//				        MarketDataItemSub marketDataItemSub = new MarketDataItemSub();
//	                    marketDataItemSub.setItemName(itemName);
//	                    marketDataItemSub.setServiceName(attributes.getReutersServiceName());
//	                    marketDataItemSub.setRequestedQualityOfService(qosr);
//	                    ((ReutersItem)item).setHandle(marketDataSubscriber.subscribe(eventQueue, marketDataItemSub, client, null));
	                } else {
	                    log.warn("Abort retry to subscribe from reuters " + itemName);
	                }
                }
            } catch (Exception e) {
                log.error("Retry thread failed for RIC: " + itemName);
            }
        }
    }

//    //////////////////////RecoveryThread
//    private class ConnectionLoopTPQR extends ConnectionLoop {
//
//        public ConnectionLoopTPQR(JMSHandler jmsHandler, int recoveryPause) {
//            super(jmsHandler, recoveryPause);
//            threadName = "ConnectionTPQR";
//        }
//
//        protected void onConnectionCall() {
//            if (log.isInfoEnabled()) {
//                log.info("onConnectionCall");
//            }
//            return;
//        }
//
//        protected void connectionCall() throws JMSException, NamingException {
//            //initialize TopicPublisher and QueueReceiver
//            if (log.isInfoEnabled()) {
//                log.info("connectionCall");
//            }
//            if (null != jmsHandler.getTopicName()) {
//                jmsHandler.initTopicPublisher(msgPoolSize);
//            }
//            if (null != jmsHandler.getQueueName()) {
//                jmsHandler.initQueueReceiver();
//            }
//        }
//    }

//    //////////////////////HeartbeatThread
//    private class HeartbeatThread extends Thread {
//        private HeartbeatMessage fixedMessage = new HeartbeatMessage(lifeId);
//        private boolean running;
//        private long lastJingleTime;
//
//        public void run() {
//            Thread.currentThread().setName("Heartbeat");
//            running = true;
//            while (running) {
//                lastJingleTime = System.currentTimeMillis();
//                try {
//                    //publish the update to JMS
//                    jmsHandler.publishMessage(fixedMessage);
//                } catch (JMSException je) {
//                    log.error("Unable to send message - JMSException:" + je.getMessage());
//                }
//                if (log.isTraceEnabled()) {
//                    log.trace("Heartbeat sent: " + fixedMessage.lifeId);
//                }
//
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                }
//            }
//            log.warn("HeartbeatThread ends. running = " + running);
//        }
//
//        public void stopHeartbeat() {
//            running = false;
//        }
//
//        public boolean isWorking() {
//            return System.currentTimeMillis() - lastJingleTime < 2000;
//        }
//    }

	public void subscribeMarket(Map<String, String> subscriptionConfig) {
		subscribeItem(subscriptionConfig.get(FormulaConfig.SUBSCRIPTION_CONFIG_IDENTIFIER));
	}

	public void unsubscribeMarket(Map<String, String> subscriptionConfig) {
		unsubscribeItem(subscriptionConfig.get(FormulaConfig.SUBSCRIPTION_CONFIG_IDENTIFIER));
	}

	@Override
	public ReutersAttributes getAttributes() {
		return attributes;
	}

	@Override
	public void setAttributes(ReutersAttributes attributes) {
		this.attributes = attributes;
	}
}