package il.co.etrader.service.level.reuters;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.service.datasource.DataUpdate;

public class BitcoinUpdatesThread extends Thread {
    private static final Logger log = Logger.getLogger(BitcoinUpdatesThread.class);
    
    private static final String URL_BITCOIN = "https://www.bitstamp.net/api/ticker/";
    private static final String URL_LITECOIN = "https://btc-e.com/api/2/ltc_usd/ticker";
    private static final int UPDATES_INTERVAL = 2000;
    public static final String BITCOIN_FEED_NAME = "Bitcoin";
    public static final String LITECOIN_FEED_NAME = "Litecoin";
    
    private boolean running;
    private ReutersService reutersService;
    private String feedName;
    
    public BitcoinUpdatesThread(ReutersService reutersService, String feedName) {
        this.reutersService = reutersService;
        this.feedName = feedName;
    }
    
    public void run() {
        Thread.currentThread().setName(feedName + "UpdatesThread");
        running = true;
        log.info(feedName + "UpdatesThread Start");
        

        InputStream is = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        Map<String, String> cache = new HashMap<String, String>();
        URL u = null;
        try {
            u = new URL(feedName.equals(BITCOIN_FEED_NAME) ? URL_BITCOIN : URL_LITECOIN);
        } catch (Exception e) {
            log.fatal("Wrong URL", e);
            running = false;
        }
        while (running) {
            try {
                HttpURLConnection httpCon = (HttpURLConnection) u.openConnection();
                is = httpCon.getInputStream();
                isr = new InputStreamReader(is);
                br = new BufferedReader(isr);
                String line = br.readLine();
                log.debug("reponse: " + line);

                Map<String, Object> fields = new HashMap<String, Object>();
                
                line = line.replaceAll("[{\" }]", "");
                String pairs[] = line.split(",");
                for (int i = 0; i < pairs.length; i++) {
                    String kv[] = pairs[i].split(":");
                    if (cache.containsKey(kv[0])) {
                        if (cache.get(kv[0]).equals(kv[1])) {
                            continue;
                        }
                    }
                    cache.put(kv[0], kv[1]);

                    if (kv[0].equals("ask") || kv[0].equals("bid") || kv[0].equals("last") || kv[0].equals("buy") || kv[0].equals("sell")) {
                        if (kv[0].equals("last")) {
                            kv[0] = "TRDPRC_1";
                        }
                        if (kv[0].equals("buy")) {
                            kv[0] = "ASK";
                        }
                        if (kv[0].equals("sell")) {
                            kv[0] = "BID";
                        }
                        kv[0] = kv[0].toUpperCase();
                        fields.put(kv[0], Double.valueOf(kv[1]));
                    }
                    if (kv[0].equals("timestamp") || kv[0].equals("server_time")) {
                        fields.put("TRDTIM_1", kv[1]);
                    }
                }
                if (fields.size() > 0) {
                    DataUpdate update = new DataUpdate(feedName);
                    update.setFields(fields);
                    update.setStateOK(true);
                    reutersService.sendUpdate(update);
                } else {
                    log.debug("No changes.");
                }
            } catch (Exception e) {
                log.error(feedName + " Error getting update.", e);
            } finally {
                try {
                    br.close();
                    isr.close();
                    is.close();
                } catch (Exception e) {
                    // do nothing
                }
                br = null;
                isr = null;
                is = null;
            }

            try {
                synchronized (this) {
                    this.wait(UPDATES_INTERVAL);
                }
            } catch (InterruptedException ie) {
                // do nothing
            }
        }
        log.info(feedName + "UpdatesThread done");
    }
    
    /**
     * Wake up the sender to try to send messages if available.
     */
    public void wakeUp() {
        synchronized (this) {
            try {
                this.notify();
            } catch (Exception e) {
                log.error(feedName + "UpdatesThread can't wake up", e);
            }
        }
    }
    
    public void stopBitcoinUpdatesThread() {
        log.info(feedName + "UpdatesThread Stopping...");
        running = false;
        wakeUp();
    }
    
    public static void main(String[] args) throws Exception {
        BitcoinUpdatesThread t = new BitcoinUpdatesThread(null, BitcoinUpdatesThread.BITCOIN_FEED_NAME);
        t.run();
    }
}