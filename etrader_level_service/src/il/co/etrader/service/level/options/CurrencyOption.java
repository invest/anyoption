package il.co.etrader.service.level.options;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.items.Item;

/**
 * Option that has ASK, BID, PRIMACT_1 abd TIMACT fields.
 *
 * @author Tony
 */
public class CurrencyOption extends Option {
	private static final Logger log = Logger.getLogger(CurrencyOption.class);
	
	private static final double[] QUOTE_WEIGHTS = {0.5d, 0.3d, 0.2d};
	
    protected ArrayList<Double> lastFiveQuotes;
    protected double lastRealLevel;

    public CurrencyOption(String optionName) {
        super(optionName);
        lastFiveQuotes = new ArrayList<Double>();
    }

    /**
     * Notify the option about update for it received from Reuters.
     *
     * @param fields the fields and values received from Reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId) {
        Double currentBid = (Double) fields.get("BID");
        Double currentAsk = (Double) fields.get("ASK");
        Double primact1 = (Double) fields.get("PRIMACT_1");
        String timeAct = (String) fields.get("TIMACT");
        String dateAct = (String) fields.get("ACTIV_DATE");

        if ((null == currentBid || currentBid == 0) && (null == currentAsk || currentAsk == 0) && (null == primact1 || primact1 == 0) && (null == timeAct) && (null == dateAct)) {

            if (log.isTraceEnabled()) {
                log.trace("No value");
            }
            return false;
        }

        if (null != currentBid && currentBid >= 0) {
        	this.bid = currentBid;
        }

        if (null != currentAsk && currentAsk >= 0) {
        	this.ask = currentAsk;
        }

        if (null != primact1 && primact1 > 0) {
            last = primact1;
        }

        if (null != timeAct) {
            activeTime = timeAct;
        }

        if (null != dateAct) {
            activeDate = dateAct;
        }

        setSnapshotReceived(true);
        lastReutersUpdateReceiveTime = System.currentTimeMillis();

        if (null != bid && bid > 0 && null != ask && ask > 0) {
            BigDecimal bda = new BigDecimal(Double.toString(ask));
            BigDecimal bdb = new BigDecimal(Double.toString(bid));
            lastRealLevel = bda.add(bdb).divide(new BigDecimal(2)).doubleValue();

            lastFiveQuotes.add(lastRealLevel);
            if (lastFiveQuotes.size() > 5) {
                lastFiveQuotes.remove(0);
            }
        }

        boolean gotClosingLevel = false;
        Item temp;
        if (updateLastLevel()) {
	        for (int i = 0; i < items.size(); i++) {
	            temp = items.get(i);
	            if (temp.optionUpdated(this, sender, lifeId, null)) {
	                gotClosingLevel = true;
	            }
	        }
        }
        return gotClosingLevel;
    }
    
    /**
     * @return <code>true</code> if the update was successful, otherwise <code>false</code>.
     */
    private boolean updateLastLevel() {
    	switch (lastFiveQuotes.size()) {
		case 1:
			lastLevel = lastFiveQuotes.get(0);
			break;
		case 2:
			lastLevel = (lastFiveQuotes.get(0) + lastFiveQuotes.get(1)) / 2d;
			break;
		case 3:
			lastLevel = lastFiveQuotes.get(2) * QUOTE_WEIGHTS[0]
							+ lastFiveQuotes.get(1) * QUOTE_WEIGHTS[1]
							+ lastFiveQuotes.get(0) * QUOTE_WEIGHTS[2];
			break;
		case 4:
		case 5:
			double min = Double.MAX_VALUE;
            double max = Double.MIN_VALUE;
            double crr;
            for (int i = 0; i < lastFiveQuotes.size(); i++) {
                crr = lastFiveQuotes.get(i);
                if (crr < min) {
                    min = crr;
                }
                if (crr > max) {
                    max = crr;
                }
            }
            
            double sum = 0;
            int weightIdx = 0;
            boolean minSkipped = false;
            boolean maxSkipped = false;
            for (int i = lastFiveQuotes.size() - 1; i >= 0; i--) {
            	crr = lastFiveQuotes.get(i);
            	if (crr == min && !minSkipped) {
            		minSkipped = true;
            		continue;
            	} else if (lastFiveQuotes.size() == 5
            					&& crr == max
            					&& !maxSkipped) {
            		maxSkipped = true;
            		continue;
            	} else {
            		sum += crr * QUOTE_WEIGHTS[weightIdx++];
            	}
            }
            
            lastLevel = sum;
			break;
		default:
			log.error("Illegal size of last quotes list [" + lastFiveQuotes
										+ "] for option [" + optionName + "]");
			/*
			 * !!! WARNING !!! Method exit point!
			 */
			return false;
		}
        if (log.isDebugEnabled()) {
            log.debug("got currency option update level " + lastLevel
            				+ " item name " + optionName
            				+ " last quotes " + lastFiveQuotes);
        }
        
        return true;
    }
    

    /**
     * @return the lastRealLevel
     */
    public double getLastRealLevel() {
        return lastRealLevel;
    }

    /**
     * @param lastRealLevel the lastRealLevel to set
     */
    public void setLastRealLevel(double lastRealLevel) {
        this.lastRealLevel = lastRealLevel;
    }
    
    @Override
    public void resetInTheEndOfTheDay() {
    	super.resetInTheEndOfTheDay();
    	
    	lastFiveQuotes.clear();
    }
}