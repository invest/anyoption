package il.co.etrader.service.level.options;

import java.util.HashMap;

import il.co.etrader.service.level.SenderThread;

public class BitcoinOption extends Option {
    protected BitcoinOption(String optionName) {
        super(optionName);
    }

    /**
     * Notify the option about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId) {
        ask = (Double) fields.get("ASK");
        bid = (Double) fields.get("BID");
        last = (Double) fields.get("TRDPRC_1");
        activeTime = (String) fields.get("TRDTIM_1");
        return super.update(fields, sender, lifeId);
    }
}