package il.co.etrader.service.level.options;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.reuters.rfa.common.Handle;

import il.co.etrader.service.level.LevelService;
import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.items.Item;
import il.co.etrader.service.level.reuters.BitcoinUpdatesThread;

/**
 * Item option details.
 *
 * @author Tony
 */
public abstract class Option {
    private static Logger log = Logger.getLogger(Option.class);

    protected String optionName;
    protected Handle handle;
    protected boolean snapshotReceived;
    protected boolean call;
    protected long strike;
    protected Double last;
    protected String expireDate;
    protected Double ask;
    protected Double bid;
    protected String activeDate;
    protected String activeTime;
    protected Double hstClose2;
    protected Double lastLevel;
    protected ArrayList<Item> items;
    protected boolean reutersStateOK;
    protected long lastReutersUpdateReceiveTime;

    private static HashMap<String, Option> options = new HashMap<String, Option>();

    public static Option getInstance(String optionName) {
        if (log.isTraceEnabled()) {
            log.trace("Getting instance for: " + optionName);
        }
        Option instance = options.get(optionName);
        if (null != instance) {
            return instance;
        } else {
            if (log.isTraceEnabled()) {
                log.trace("Create new Option instance.");
            }
            if (optionName.equals(BitcoinUpdatesThread.BITCOIN_FEED_NAME) || optionName.equals(BitcoinUpdatesThread.LITECOIN_FEED_NAME)) {
                instance = new BitcoinOption(optionName);
            } else if ((optionName.endsWith("1MD=")
            				&& !optionName.equals("USD1MD=")) // USD1MD= shall be treated as YearInterestOption
	            		|| optionName.equals("MXN1M=")
	            		|| optionName.equals("KRW1M=")
	            		|| optionName.equals("KRW1MID=KR")) {
                instance = new InterestOption(optionName);
            } else if (optionName.equals(".TA25")) {
                instance = new TelAviv25Option(optionName);
            } else if (optionName.endsWith(".TA")) {
                instance = new EnergyOption(optionName);
            } else if (optionName.endsWith("=FXAL")) {
            	instance = new AskBidOption(optionName);
            } else if (optionName.equals("EUR=")
            			|| optionName.equals("GBP=")
            			|| optionName.equals("JPY=")
            			|| optionName.equals("AUD=")
            			|| optionName.equals("KRW=")
            			|| optionName.equals("EURJPY=")
            			|| optionName.equals("AUDJPY=")
            			|| optionName.equals("GBPJPY=")
            			|| optionName.equals("CAD=")
            			|| optionName.equals("CHF=")
            			|| optionName.equals("EUR=")
            			|| optionName.equals("TRY=")
            			|| optionName.equals("ZAR=")
            			|| optionName.equals("NZD=")
            			|| optionName.equals("EURGBP=")
            			|| optionName.equals("RUB=")
            			|| optionName.equals("CNY=")
            			|| optionName.equals("BGN=")
            			|| optionName.equals("SEK=")
            			|| optionName.equals("NOK=")
            			|| optionName.equals("ILS=")
            			|| optionName.equals("EUR=FXAL")
            			|| optionName.equals("CZK=")
            			|| optionName.equals("DKK=")) {
                instance = new CurrencyOption(optionName);
            } else if (optionName.endsWith("H=")) {
                instance = new HourClosingLevelOption(optionName);
            } else if (optionName.endsWith("H=RR")) {
                instance = new ILSHourClosingLevelOption(optionName);
            } else if (optionName.equals("XAUFIX=")) {
                instance = new GoldClosingLevelOption(optionName);
            } else if (optionName.equals("ILS=D4")) {
                instance = new AskBidOption(optionName);
            } else if (optionName.length() == 8 && optionName.startsWith("ILS=")) { // the ILS bank options
                instance = new InterestOption(optionName);
            } else if (optionName.equals("TELILSOND=")
            		|| optionName.equals("USD1MFSR=")
            		|| optionName.equals("USD1MD=")) { // ILS year (not sure if it is year but...) options
                instance = new YearInterestOption(optionName);
            } else if (optionName.startsWith("FFI") ||
                    optionName.startsWith("FCE") ||
                    optionName.startsWith("ES") ||
                    optionName.startsWith("GC") ||
                    optionName.startsWith("CL") ||
                    optionName.startsWith("SI") ||
                    optionName.startsWith("HG") ||
                    optionName.startsWith("NQ") ||
                    optionName.startsWith("FKLI") ||
                    optionName.startsWith("TRF30") ||
                    optionName.startsWith("FDX") ||
                    optionName.startsWith("RIRTS") ||
                    optionName.startsWith("NG") ||
                    optionName.startsWith("S") || // WARNING WARNING !!!!!!!!!!!!!!!!!!!! This is done for the Soy Beans (market id 686). It should normally use Sv1 but if I put just Sv1 traders will not be able to switch options.
                    optionName.equals("VXX") ||
                    optionName.startsWith(".")) {
                instance = new SecondRICOption(optionName);
            } else if (optionName.startsWith("GOOG.O")
            		|| optionName.startsWith("FB.O")
            		|| optionName.startsWith("AAPL.O")
            		|| optionName.startsWith("ORAN.PA")
            		|| optionName.startsWith("DAIGn.DE")
            		|| optionName.startsWith("BP.L")) {
            	instance = new NewStockOption(optionName);
            } else {
            	log.error("Could not create option: unknown name [" + optionName + "]");
            }
            options.put(optionName, instance);
            return instance;
        }
    }

    public static Option getInstance(String optionName, boolean call, long strike) {
        if (log.isTraceEnabled()) {
            log.trace("Getting instance for: " + optionName);
        }
        Option instance = options.get(optionName);
        if (null != instance) {
            return instance;
        } else {
            if (log.isTraceEnabled()) {
                log.trace("Create new instance.");
            }
            if (optionName.startsWith("TIS")) { // ILS strike options
                instance = new StrikeOption(optionName, call, strike);
            } else if (optionName.startsWith("TLVJ")) { // TA25 strike options
                instance = new StrikeOption(optionName, call, strike);
            }
            options.put(optionName, instance);
            return instance;
        }
    }

    protected Option(String optionName) {
        this(optionName, false, 0);
    }

    protected Option(String optionName, boolean call, long strike) {
        snapshotReceived = false;
        this.optionName = optionName;
        this.call = call;
        this.strike = strike;
        items = new ArrayList<Item>();
        reutersStateOK = true;
    }

    /**
     * @return the activeDate
     */
    public String getActiveDate() {
        return activeDate;
    }

    /**
     * @param activeDate the activeDate to set
     */
    public void setActiveDate(String activeDate) {
        this.activeDate = activeDate;
    }

    public String getActiveTime() {
        return activeTime;
    }

    public void setActiveTime(String activeTime) {
        this.activeTime = activeTime;
    }

    public Double getAsk() {
        return ask;
    }

    public void setAsk(Double ask) {
        this.ask = ask;
    }

    public Double getBid() {
        return bid;
    }

    public void setBid(Double bid) {
        this.bid = bid;
    }

    /**
     * @return the handle
     */
    public Handle getHandle() {
        return handle;
    }

    /**
     * @param handle the handle to set
     */
    public void setHandle(Handle handle) {
        this.handle = handle;
    }

    /**
     * @return the optionName
     */
    public String getOptionName() {
        return optionName;
    }

    /**
     * @param optionName the optionName to set
     */
    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    /**
     * @return the snapshotReceived
     */
    public boolean isSnapshotReceived() {
        return snapshotReceived;
    }

    /**
     * @param snapshotReceived the snapshotReceived to set
     */
    public void setSnapshotReceived(boolean snapshotReceived) {
        this.snapshotReceived = snapshotReceived;
    }

    /**
     * @return the call
     */
    public boolean isCall() {
        return call;
    }

    /**
     * @param call the call to set
     */
    public void setCall(boolean call) {
        this.call = call;
    }

    public Double getLast() {
        return last;
    }

    public void setLast(Double last) {
        this.last = last;
    }

    /**
     * @return the strike
     */
    public long getStrike() {
        return strike;
    }

    /**
     * @param strike the strike to set
     */
    public void setStrike(long strike) {
        this.strike = strike;
    }

    /**
     * @return the expireDate
     */
    public String getExpireDate() {
        return expireDate;
    }

    /**
     * @param expireDate the expireDate to set
     */
    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public boolean isReutersStateOK() {
        return reutersStateOK;
    }

    public void setReutersStateOK(boolean stateOK) {
        if (reutersStateOK != stateOK) {
        	log.trace("Changing reutersStateOK on Option " + optionName + " from " + reutersStateOK + " to " + stateOK);
            reutersStateOK = stateOK;
            String desc = optionName + " got " + stateOK + " state from Reuters";
            String itemsList = "";
            for (Item i : items) {
                boolean changed = false;
                if (!stateOK) {
                    changed = i.disableOpportunities(desc);
                } else {
                    if (i.shouldEnableOpportunities()) {
                        changed = i.enableOpportunities(desc, false);
                    }
                }
                if (changed) {
                    if (!itemsList.equals("")) {
                        itemsList += ", ";
                    }
                    itemsList += i.getItemName();
                }
            }
            LevelService.getInstance().sendAlertEmail((stateOK ? "Disabled " : "Enabled ") + itemsList, desc);
        }
    }

    /**
     * Notify the option about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId) {
        boolean gotClosingLevel = false;
        snapshotReceived = true; // the first update is snapshot
        lastReutersUpdateReceiveTime = System.currentTimeMillis();
        Item itm = null;
        for (int i =  0; i < items.size(); i++) {
        	itm = items.get(i);
        	log.trace("Option.update item: " + itm.getItemName());
            if (itm.optionUpdated(this, sender, lifeId, null)) {
                gotClosingLevel = true;
            }
        }
        return gotClosingLevel;
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public void removeItem(Item item) {
        items.remove(item);
    }

    public boolean hasItems() {
        return items.size() > 0;
    }

    public Iterator<Item> getItemsIterator() {
        return items.iterator();
    }

    public String getItemsToString() {
    	StringBuffer sb = new StringBuffer();
    	for (int i = 0; i < items.size(); i++) {
    		sb.append(items.get(i).getItemName());
    		if (i > 0) {
    			sb.append(" ");
    		}
    	}
    	return sb.toString();
    }

    /**
     * @return <code>ArrayList<Opportunity></code> with the opps that should be closed
     *      (have the timeToClose flag on and have received closing level).
     */
    public ArrayList<Opportunity> getOppsToClose() {
        ArrayList<Opportunity> l = new ArrayList<Opportunity>();
        for (int i =  0; i < items.size(); i++) {
            l.addAll(items.get(i).getOppsToClose());
        }
        return l;
    }

	/**
	 * @return the hstClose2
	 */
	public Double getHstClose2() {
		return hstClose2;
	}

	/**
	 * @param hstClose2 the hstClose2 to set
	 */
	public void setHstClose2(Double hstClose2) {
		this.hstClose2 = hstClose2;
	}

    /**
     * @return the lastLevel
     */
    public Double getLastLevel() {
        return lastLevel;
    }

    /**
     * @param lastLevel the lastLevel to set
     */
    public void setLastLevel(Double lastLevel) {
        this.lastLevel = lastLevel;
    }

    /**
     * @return the lastReutersUpdateReceiveTime
     */
    public long getLastReutersUpdateReceiveTime() {
        return lastReutersUpdateReceiveTime;
    }

    /**
     * @param lastReutersUpdateReceiveTime the lastReutersUpdateReceiveTime to set
     */
    public void setLastReutersUpdateReceiveTime(long lastReutersUpdateReceiveTime) {
        this.lastReutersUpdateReceiveTime = lastReutersUpdateReceiveTime;
    }

    /**
     * Reset the market internal state in the end of the day to be ready for the next
     * day opening. Long term shiftings, updates flags etc.
     */
    public void resetInTheEndOfTheDay() {
    	// Do nothing - default implementation
    }
}