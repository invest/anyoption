package il.co.etrader.service.level.options;

import java.util.HashMap;

import il.co.etrader.service.level.SenderThread;

/**
 * Nasdaq100 Future option.
 *
 * @author Tony
 */
public class NasdaqFOption extends Option {
    public NasdaqFOption(String optionName) {
        super(optionName);
    }

    /**
     * Notify the option about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId) {
//        last = (Double) fields.get("PRIMACT_1");
        last = (Double) fields.get("TRDPRC_1");
        ask = (Double) fields.get("ASK");
        bid = (Double) fields.get("BID");
//        activeDate = (String) fields.get("TRDTIM_1");
        activeDate = (String) fields.get("SALTIM");
        if (optionName.startsWith("TRF30")) {
        	activeDate = (String) fields.get("TIMACT");
        }
        if (null != last || null != ask || null != bid) {
            snapshotReceived = true;
            return super.update(fields, sender, lifeId);
        }
        return false;
    }
}