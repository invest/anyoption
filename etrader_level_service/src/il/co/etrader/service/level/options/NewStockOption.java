package il.co.etrader.service.level.options;

import java.util.HashMap;

import org.apache.log4j.Logger;

import il.co.etrader.service.level.SenderThread;

/**
 * Option that has ASK, BID, PRIMACT_1 abd TIMACT fields.
 *
 * @author Eyal
 */
public class NewStockOption extends Option {
	private static final Logger log = Logger.getLogger(NewStockOption.class);


    public NewStockOption(String optionName) {
        super(optionName);
    }

    /**
     * Notify the option about update for it received from Reuters.
     *
     * @param fields the fields and values received from Reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId) {
    	log.debug("OptionNewStockItem option update");
    	boolean gotClosingLevel = false;
        snapshotReceived = true; // the first update is snapshot
        lastReutersUpdateReceiveTime = System.currentTimeMillis();
        for (int i =  0; i < items.size(); i++) {
            if (items.get(i).update(fields, sender, lifeId, null)) {
                gotClosingLevel = true;
            }
        }
        return gotClosingLevel;
    }

}