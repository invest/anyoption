package il.co.etrader.service.level.options;

import java.util.HashMap;

import org.apache.log4j.Logger;

import il.co.etrader.service.level.SenderThread;
import il.co.etrader.service.level.items.EnergyItem;

/**
 * Option that has TRDPRC_1, OFF_CLOSE and HSTCLSDATE fields.
 *
 * @author Tony
 */
public class EnergyOption extends Option {

	private static final Logger log = Logger.getLogger(EnergyItem.class);

    public EnergyOption(String optionName) {
        super(optionName);
    }

    /**
     * Notify the option about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId) {
        if (null != fields.get("TRDPRC_1")) {
            setLast((Double) fields.get("TRDPRC_1"));
        }
        if (null != fields.get("OFF_CLOSE") && snapshotReceived) {
        	if (((Double) fields.get("OFF_CLOSE")).doubleValue() > 0) {
        		hstClose2 = (Double) fields.get("OFF_CLOSE");
        	}
        }
        return super.update(fields, sender, lifeId);
    }
}