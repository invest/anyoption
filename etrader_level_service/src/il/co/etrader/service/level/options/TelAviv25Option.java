package il.co.etrader.service.level.options;

import java.util.HashMap;

import il.co.etrader.service.level.SenderThread;

/**
 * Option that has TRDPRC_1, OFF_CLOSE and HSTCLSDATE fields.
 *
 * @author Tony
 */
public class TelAviv25Option extends Option {
    public TelAviv25Option(String optionName) {
        super(optionName);
    }

    /**
     * Notify the option about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId) {
        boolean gotClosingLevel = false;
        snapshotReceived = true; // the first update is snapshot
        for (int i =  0; i < items.size(); i++) {
            if (items.get(i).update(fields, sender, lifeId, null)) {
                gotClosingLevel = true;
            }
        }
        return gotClosingLevel;
    }
}