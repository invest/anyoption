package il.co.etrader.service.level.options;

import java.util.HashMap;

import il.co.etrader.service.level.SenderThread;

public class StrikeOption extends Option {
    public StrikeOption(String optionName, boolean call, long strike) {
        super(optionName, call, strike);
    }
    
    /**
     * Notify the option about update for it received from Reuters.
     * 
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId) {
        if (null != fields.get("EXPIR_DATE")) {
            expireDate = (String) fields.get("EXPIR_DATE");
        }
        if (null != fields.get("TRDPRC_1")) {
            last = (Double) fields.get("TRDPRC_1");
        }
        if (null != fields.get("ASK")) {
            ask = (Double) fields.get("ASK");
        }
        if (null != fields.get("BID")) {
            bid = (Double) fields.get("BID");
        }
        return super.update(fields, sender, lifeId);
    }
}