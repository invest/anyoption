package il.co.etrader.service.level.options;

import java.util.HashMap;

import org.apache.log4j.Logger;

import il.co.etrader.service.level.SenderThread;

public class HourClosingLevelOption extends AskBidOption {
	private static final Logger logger = Logger.getLogger(HourClosingLevelOption.class);

    public HourClosingLevelOption(String optionName) {
        super(optionName);
    }

    /**
     * Notify the option about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId) {
        if (null != fields.get("ACTIV_DATE") && !((String)fields.get("ACTIV_DATE")).trim().equals("")) {
        	logger.debug("after trim we got:" + ((String)fields.get("ACTIV_DATE")).trim().equals(""));
            activeDate = (String) fields.get("ACTIV_DATE");
        }
        return super.update(fields, sender, lifeId);
    }
}