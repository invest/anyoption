package il.co.etrader.service.level.options;

import java.util.HashMap;

import il.co.etrader.service.level.SenderThread;

public class ILSHourClosingLevelOption extends HourClosingLevelOption {
    public ILSHourClosingLevelOption(String optionName) {
        super(optionName);
    }
    
    /**
     * Notify the option about update for it received from Reuters.
     * 
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId) {
        if (null != fields.get("VALUE_DT1")) {
            activeDate = (String) fields.get("VALUE_DT1");
        }
        if (null != fields.get("VALUE_TS1")) {
            activeTime = (String) fields.get("VALUE_TS1");
        }
        return super.update(fields, sender, lifeId);
    }
}