package il.co.etrader.service.level.options;

import java.math.BigDecimal;
import java.util.HashMap;

import il.co.etrader.service.level.BigDecimalHelper;
import il.co.etrader.service.level.SenderThread;

public class YearInterestOption extends InterestOption {
    protected Double bid1;
    private BigDecimal avgBidBid1;
    
    public YearInterestOption(String optionName) {
        super(optionName);
    }

    public void setBid(Double bid) {
        super.setBid(bid);
        setAvgBidBid1(bid, bid1);
    }
    
    public Double getBid1() {
        return bid1;
    }

    public void setBid1(Double bid1) {
        this.bid1 = bid1;
        setAvgBidBid1(bid, bid1);
    }

    public void setAvgBidBid1(Double bid, Double bid1) {
        if (null != bid && null != bid1) {
            BigDecimal b = new BigDecimal(bid.toString());
            BigDecimal b1 = new BigDecimal(bid1.toString());
            avgBidBid1 = b.add(b1).divide(BigDecimalHelper.TWO);
        }
    }
    
    public BigDecimal getAvgBidBid1() {
        return avgBidBid1;
    }
    
    /**
     * Notify the option about update for it received from Reuters.
     * 
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId) {
        if (null != fields.get("BID_1")) {
            setBid1((Double) fields.get("BID_1"));
        }
        return super.update(fields, sender, lifeId);
    }

    /**
     * @return the snapshotReceived
     */
    public boolean isSnapshotReceived() {
        return null != bid1 && null != bid;
    }
}