package il.co.etrader.service.level.options;

import java.util.HashMap;

import org.apache.log4j.Logger;

import il.co.etrader.service.level.SenderThread;

public class GoldClosingLevelOption extends Option {
    private static Logger log = Logger.getLogger(GoldClosingLevelOption.class);
    
    public GoldClosingLevelOption(String optionName) {
        super(optionName);
    }
    
    /**
     * Notify the option about update for it received from Reuters.
     * 
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId) {
        if (null != fields.get("PRIMACT_1")) {
            last = (Double) fields.get("PRIMACT_1");
            snapshotReceived = true;
            if (log.isTraceEnabled()) {
                log.trace("New Gold closing level: " + last);
            }
            return super.update(fields, sender, lifeId);
        }
        return false;
    }
}