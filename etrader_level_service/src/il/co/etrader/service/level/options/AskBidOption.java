package il.co.etrader.service.level.options;

import java.util.HashMap;

import il.co.etrader.service.level.SenderThread;

/**
 * Option that has ASK and BID fields.
 * 
 * @author Tony
 */
public class AskBidOption extends Option {
    public AskBidOption(String optionName) {
        super(optionName);
    }
    
    /**
     * Notify the option about update for it received from Reuters.
     * 
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId) {
        if (null != fields.get("ASK")) {
            setAsk((Double) fields.get("ASK"));
        }
        if (null != fields.get("BID")) {
            setBid((Double) fields.get("BID"));
        }
        return super.update(fields, sender, lifeId);
    }

    /**
     * @return the snapshotReceived
     */
    public boolean isSnapshotReceived() {
        return null != ask && null != bid;
    }

    public void resetInTheEndOfTheDay() {
    	ask = null;
    	bid = null;
    }
}