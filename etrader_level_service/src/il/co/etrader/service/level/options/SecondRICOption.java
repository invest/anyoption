package il.co.etrader.service.level.options;

import java.util.HashMap;

import org.apache.log4j.Logger;

import il.co.etrader.service.level.SenderThread;

public class SecondRICOption extends Option {
    private static final Logger log = Logger.getLogger(SecondRICOption.class);

    protected String lastFieldName;
    protected String timeField;

    public SecondRICOption(String optionName) {
        super(optionName);
        lastFieldName = "TRDPRC_1";
        timeField = "SALTIM";
        if (optionName.startsWith("TRF30")) { //|| optionName.startsWith("RIRTS")) {
            timeField = "TIMACT";
        } else if (optionName.equals(".AXJO") || optionName.equals(".TASI") || optionName.equals(".TOPX500")) {//(optionName.startsWith(".")) {
        	timeField = "TRDTIM_1";
        } else if (optionName.equals(".N225")) {
        	timeField = "EXCHTIM";
        }
    }

    /**
     * Notify the option about update for it received from Reuters.
     *
     * @param fields the fields and values received from reuters
     * @param sender
     * @param lifeId
     * @return <code>true</code> if this update brought closing level (hour closing level for example) else <code>false</code>.
     */
    public boolean update(HashMap<String, Object> fields, SenderThread sender, long lifeId) {
        activeDate = (String) fields.get(timeField);
        ask = (Double) fields.get("ASK");
        bid = (Double) fields.get("BID");
        last = (Double) fields.get(lastFieldName);
        expireDate = (String) fields.get("EXPIR_DATE");
        log.trace(optionName + " - activeDate: " + activeDate + " ask: " + ask + " bid: " + bid + " last: " + last + " expireDate: " + expireDate);
        return super.update(fields, sender, lifeId);
    }
}