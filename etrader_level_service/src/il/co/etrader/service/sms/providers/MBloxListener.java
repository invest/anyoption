package il.co.etrader.service.sms.providers;

import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.mblox.gateway.clientapi.Listener;
import com.mblox.gateway.clientapi.MbloxClient;

import il.co.etrader.service.sms.managers.SMSManager;
import il.co.etrader.sms.SMS;

public class MBloxListener implements Listener {
    private static final Logger log = Logger.getLogger(MBloxListener.class);
    private static String ls = System.getProperty("line.separator");
    
    private int maxRetries;

    public MBloxListener(int maxRetries) {
        this.maxRetries = maxRetries;
    }
    
    /**
     * Receipt from the server that this message has been accepted. The client application can not consider the SMS sent
     * until this receipt is received. If a parameter is not supplied by the mBlox servers, it will be supplied as null
     * or -1.
     *
     * @param sequence The sequence matches the one used when sending the SMS for which this is a receipt
     * @param msgReference When the servers send status indications about this SMS, they will from now on use this
     *            reference instead.
     * @param at The time when the receipt was created. Millis since 0:0:0 1/1/1970 UTC.
     * @param operator The ID of the operator that message was sent over. If not included the value will be -1. This is
     *            configured on the mBlox side if this value should be sent to the client.
     */
    public void submitConf(
            int sequence,
            String msgReference,
            long at,
            int operator) {
        if (log.isDebugEnabled()) {
            log.debug(ls +
                    "submitConf: " + ls +
                    "sequence: " + sequence + ls +
                    "msgReference: " + msgReference + ls +
                    "at: " + at + ls +
                    "operator: " + operator + ls);
        }
        try {
            SMSManager.updateMessageStatus(sequence, SMS.STATUS_SUBMITTED, msgReference, "");
        } catch (Exception e) {
            log.error("Can't process submitConf.", e);
        }
    }

    /**
     * Instead of the receipt in submitConf above there might come a rejection from the servers. That mean that the
     * corresponding SMS will never be sent. If a parameter is not supplied by the mBlox servers, it will be supplied as
     * null or -1.
     *
     * @param sequence The sequence matches the one used when sending the SMS for which this is a rejection
     * @param reason String describing why the message was rejected
     * @param rejectCode A code describing the reason. Use this if you want to make your application act different
     *            depending on reject reason.
     * @param at Not always supplied. The time when the rejection was created. Millis since 0:0:0 1/1/1970 UTC.
     * @param mqFrom The username with which the socket used for sending the SMS was logged on.
     * @param destination The destination from the original SMS.
     * @param retry Retry code as supplied by the mBlox server. Tells whether the message should be retried or not.
     */
    public void submitRej(
            int sequence,
            String reason,
            int rejectCode,
            long at,
            String mqFrom,
            String destination,
            int retry) {
        if (log.isDebugEnabled()) {
            log.debug(ls +
                    "submitRej: " + ls +
                    "sequence: " + sequence + ls +
                    "reason: " + reason + ls +
                    "rejectCode: " + rejectCode + ls +
                    "at: " + at + ls +
                    "mqFrom: " + mqFrom + ls +
                    "destination: " + destination + ls +
                    "retry: " + retry + ls);
        }
        try {
            if (retry == 0) { // 0 is permanent reject. do not retry
                SMSManager.updateMessageStatus(sequence, SMS.STATUS_FAILED_TO_SUBMIT, null, rejectCode + " " + reason);
            } else { // 1 is retry later
                SMS sms = SMSManager.getMessage(sequence);
                if (sms.getRetries() < maxRetries) {
                    SMSManager.updateMessageAfterSubmit(sequence, sms.getRetries() + 1, sms.getTimeSent(), SMS.STATUS_QUEUED, rejectCode + " " + reason, null, new Date(System.currentTimeMillis() + 120000)); // after 2 min
                } else {
                    SMSManager.updateMessageAfterSubmit(sequence, sms.getRetries(), sms.getTimeSent(), SMS.STATUS_FAILED_TO_SUBMIT, rejectCode + " " + reason, null, sms.getScheduledTime());
                }
            }
        } catch (Exception e) {
            log.error("Can't process submitRej.", e);
        }
    }

    /**
     * There might arrive status-indications about the SMS, depending on configurations. If a parameter is not supplied
     * by the mBlox servers, it will be supplied as null or -1.
     *
     * @param msgReference String used to match which SMS this indication referres to
     * @param at When the indication was created. In milliseconds since 0:0:0 1/1/1970 UTC.
     * @param originator Destination from original SMS
     * @param status String describing the status, e.g. "delivered", "failed" or "buffered". Read the API manual for
     *            details.
     * @param reason Number coding of the status field above. Use this when matching in your client application.
     * @param occurrenceTime Not always supplied. A time value supplied by an SMSC that states when the event actually
     *            occurred. In seconds since 0:0:0 1/1/1970 UTC
     * @param clientRef Currently not in use.
     * @param sequence Sequence number for this statusInd. Uniqually created by the mBlox servers to identify this
     *            particular statusIns message.
     * @param tags Optional and dynamic tag parameters. Properties object with name-value pairs. Will be empty if no 
     *            tags present in message.
     * @return True if the message could be handled correctly. If false, the message will be resubmitted by the mBlox
     *         servers.
     */
    public boolean statusInd(
            String msgReference,
            long at,
            String originator,
            String status,
            int reason,
            int occurrenceTime,
            String clientRef,
            long sequence,
            Properties tags) {
        long t = System.currentTimeMillis();
        try {
            SMSManager.insertDLR(0, msgReference, status, new Date(at));
            SMS sms = SMSManager.getMessage(msgReference);
            if (null == sms) {
                log.warn("statusInd for non existing message with reference: " + msgReference);
                return true;
            }
            long sId = sms.getStatus();
            if (sId != SMS.STATUS_DELIVERED && sId != SMS.STATUS_FAILED) { // do not overwrite final statuses
                switch (reason) {
                case 1: // Buffered, phone related (e.g. handset switched off)
                    sId = SMS.STATUS_QUEUED_ON_SMSC;
                    break;
                case 2: // Buffered, operator unknown (e.g. congestion, HLR fault)
                    sId = SMS.STATUS_QUEUED_ON_SMSC;
                    break;
                case 3: // Acked, message has been accepted by an SMSC for deliverance.
                    sId = SMS.STATUS_DELIVERED_TO_SMSC;
                    break;
                case 4: // Delivered, message has successfully been delivered to the handset.
                    sId = SMS.STATUS_DELIVERED;
                    break;
                case 5: // Failed, the message could not be delivered to the handset.
                    sId = SMS.STATUS_FAILED;
                    break;
                case 6: // 6 � Unknown, the sending SMSC did not supply any information about the success of the deliverance.
                    break;
                default:
                    // We are receiving undocumented values > 6. Like 8, 20, 23 etc. They all arrive with description "failed".
                    // Will assume that all > 6 reasons are "failed".
                    sId = SMS.STATUS_FAILED;
                    break;
                }
            }
            SMSManager.updateMessageStatus(sms.getId(), sId, msgReference, reason + " " + status);
        } catch (Exception e) {
            log.error("Can't process statusInd.", e);
            return false;
        }
        log.trace("statusInd time to process: " + (System.currentTimeMillis() - t));
        return true;
    }

    /**
     * Called when a mobile originated SMS arrives. If a parameter is not supplied by the mBlox servers, it will be
     * supplied as null or -1.
     *
     * @param destination The MSISDN to where the SMS was sent.
     * @param originator The MSISDN from where the SMS was sent.
     * @param body Body of the SMS
     * @param at Time when the SMS was received. In milliseconds since 0:0:0 1/1/1970 UTC
     * @param sequence Sequence number for this SMS. Uniqually created by the mBlox servers.
     * @param msgReference Reference to this SMS creaded by the original SMSC. Not always supplied from the SMSC.
     * @param deliverer From which SMS-deliverer this SMS arrived to mBlox servers.
     * @param operator Specifies the originator operator/network. Supplied accordingly with a numberplan supplied with
     *            the account details.
     * @param tariff Used with premium messaging services to specify the tariff for a message. Possible values are
     *            supplied with the account details.
     * @param bodyEncoding The encoding of the body value, states eg if the message is binary. Look in the manual for
     *            further details about possible values.
     * @param UDH UserDataHeader for this message.
     * @param sessionId The SessionId for this message, will be null if not present in message.
     * @param tags Optional and dynamic tag paramters. Properties object with name-value pairs. Will be empty if no tags
     *            present in message.
     * @return True if the message could be handled correctly. If false, the message will be resubmitted by the mBlox
     *         servers.
     */
    public boolean deliverReq(
            String destination,
            String originator,
            String body,
            long at,
            long sequence,
            String msgReference,
            int deliverer,
            int operator,
            String tariff,
            String bodyEncoding,
            String UDH,
            String sessionId,
            Properties tags) {
        if (log.isDebugEnabled()) {
            log.debug(ls +
                    "deliverReq: " + ls +
                    "destination: " + destination + ls +
                    "originator: " + originator + ls +
                    "body: " + body + ls +
                    "at: " + at + ls +
                    "sequence: " + sequence + ls +
                    "msgReference: " + msgReference + ls +
                    "deliverer: " + deliverer + ls +
                    "operator: " + operator + ls +
                    "tariff: " + tariff + ls +
                    "bodyEncoding: " + bodyEncoding + ls +
                    "UDH: " + UDH + ls +
                    "sessionId: " + sessionId + ls +
                    "tags: " + tags + ls);
        }
        return true;
    }

    /**
     * Called when the API wants to communicate some system status information. If a parameter is not supplied by the
     * mBlox servers, it will be supplied as null or -1.
     *
     * @param message String describing the message.
     * @param messageCode Code that describes the message. For more details see the API manual. All values are found as
     *            static parameters, MbloxClient.SYSTEMINFO_XXX
     * @param value The value has a different meaning depending on the message code.
     * @param at Time when this message was generated. In milliseconds since 0:0:0 1/1/1970 UTC.
     */
    public void systemStatusInfo(String message, int messageCode, int value, long at) {
        if (log.isDebugEnabled()) {
            log.debug(ls +
                    "systemStatusInfo: " + ls +
                    "message: " + message + ls +
                    "messageCode: " + messageCode + ls +
                    "value: " + value + ls +
                    "at: " + at + ls);
        }
    }

    /**
     * Called each time there is a message to log. Use this method if you want to have different logging then the file
     * logging supplied in the API. If not, just leave the method blank. If a parameter is not supplied by the mBlox
     * servers, it will be supplied as null or -1.
     *
     * @param logLevel Specifies which log this message is for. Used values are found in MbloxClient as MbloxClient.LOG_XX.
     * @param message The message to log.
     */
    public void log(int logLevel, String message) {
        switch (logLevel) {
        case MbloxClient.LOG_TRACE:
            log.trace(message);
            break;
        case MbloxClient.LOG_FULLIO:
            log.info(message);
            break;
        case MbloxClient.LOG_ERROR:
            log.error(message);
            break;
        default:
            log.warn(message);
            break;
        }
    }
}