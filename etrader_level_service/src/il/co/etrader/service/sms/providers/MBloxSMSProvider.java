package il.co.etrader.service.sms.providers;

import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.sms.SMSException;
import com.mblox.gateway.clientapi.MbloxClient;

import il.co.etrader.sms.PushRegistrySMS;
import il.co.etrader.sms.SMS;
import il.co.etrader.sms.SMSProvider;
import il.co.etrader.sms.SMSProviderConfig;
import il.co.etrader.sms.TextSMS;
import il.co.etrader.sms.WapPushSMS;

public class MBloxSMSProvider extends SMSProvider {
    private static final Logger log = Logger.getLogger(MBloxSMSProvider.class);
    private static String ls = System.getProperty("line.separator");

    public static final int ORIGINATOR_TYPE_NUMERIC = 0;
    public static final int ORIGINATOR_TYPE_SHORT_CODE = 3;
    public static final int ORIGINATOR_TYPE_ALPHANUMERIC = 5;

    private static final String SMS_ENCODING_TEXT = "Text";
    private static final String SMS_ENCODING_UNICODE = "Unicode";

    private String confPath;
    private String logPath;
    private boolean logTrace;
    private int profile;
    private boolean statusReport;
    private String billinRef;
    private int waitOnShutdown;

    private MbloxClient mBloxClient;
    private MBloxListener mBloxListener;
    
    public void setConfig(SMSProviderConfig config) {
        super.setConfig(config);
        
        if (null != config.getProps()) {
            try {
                String[] pa = config.getProps().split(";");
                for (int i = 0; i < pa.length; i++) {
                    String[] p = pa[i].split("=");
                    if (p[0].equalsIgnoreCase("confPath")) {
                        confPath = p[1];
                    } else if (p[0].equalsIgnoreCase("logPath")) {
                        logPath = p[1];
                    } else if (p[0].equalsIgnoreCase("logTrace")) {
                        logTrace = p[1].equalsIgnoreCase("true");
                    } else if (p[0].equalsIgnoreCase("profile")) {
                        profile = Integer.parseInt(p[1]);
                    } else if (p[0].equalsIgnoreCase("statusReport")) {
                        statusReport = p[1].equalsIgnoreCase("true");
                    } else if (p[0].equalsIgnoreCase("billinRef")) {
                        billinRef = p[1];
                    } else if (p[0].equalsIgnoreCase("waitOnShutdown")) {
                        waitOnShutdown = Integer.parseInt(p[1]);
                    }
                }
            } catch (Exception e) {
                log.error("Can't config.", e);
            }
        }
        log.info("confPath: " + confPath + ls +
                "logPath: " + logPath + ls +
                "logTrace: " + logTrace + ls +
                "profile: " + profile + ls +
                "statusReport: " + statusReport + ls +
                "billinRef: " + billinRef + ls +
                "waitOnShutdown: " + waitOnShutdown + ls);
        try {
            mBloxListener = new MBloxListener(maxRetries);
            mBloxClient = new MbloxClient(confPath, logPath, logTrace, mBloxListener);
            mBloxClient.connect(-1); // -1 is return immediately. later should check the connection status.
        } catch (Exception e) {
            log.fatal("Problem connecting to mBlox.", e);
        }
    }
    
    
    public void send(TextSMS msg) throws SMSException {
        if (!mBloxClient.isUp(0)) {
            throw new SMSException("mBlox is not connected");
        }

        String bodyEncoding = SMS_ENCODING_TEXT;
        if (SMS.isUnicode(msg.getMessageBody())) {
            log.info("Sending unicode message...");
            bodyEncoding = SMS_ENCODING_UNICODE;
        }

        int rez = -1;
        try {
            rez = mBloxClient.sendSMSDetailed(
                "00" + msg.getPhoneNumber(), // destination - must always start with 00 before the country code
                msg.getMessageBody(), // body
                profile, // profile - Different products are configured on a single mBlox account by use of multiple profiles
                msg.getSenderName(), // msg.getSenderPhoneNumber(), // originator
                msg.getSenderName().matches("\\d+") ? ORIGINATOR_TYPE_NUMERIC : ORIGINATOR_TYPE_ALPHANUMERIC, // originatorType
                //ORIGINATOR_TYPE_ALPHANUMERIC, // originatorType
                (int) msg.getId(), // sequence
                -1, // smsClass
                statusReport, // statusReport
                -1, // pid
                -1, // expiration time
                -1, // reply
                bodyEncoding, // bodyEncoding
                null, // UDH
                -1, // blocking
                billinRef, // billingRef - The client invoice can be potentially split in different sections.
                -1, // multypart
                -1, // operator
                null, // tariff
                null, // sessionId
                null, // tags
                null, // serviceDesc
                -1, // contentType
                -1); // serviceId
        } catch (Exception e) {
            throw new SMSException("Error sending message to mBlox.", e);
        }
        if (rez == -1) {
            // TODO: What will be our retry strategy in this case? For now leave it in the queue for immediate retry.
            throw new SMSException("Failed to send SMS text message.");
        }
        msg.setTimeSent(new Date());
        msg.setStatus(SMS.STATUS_SUBMITTED);
        msg.setError("");
    }

    public void send(WapPushSMS msg) throws SMSException {
        throw new SMSException("Not implemented.");
    }

    public void send(PushRegistrySMS msg) throws SMSException {
        throw new SMSException("Not implemented.");
    }
    
    public void close() {
        if (null != mBloxClient) {
            if (mBloxClient.isActive()) {
                mBloxClient.close();
                try {
                    mBloxClient.joinAll(waitOnShutdown);
                } catch (InterruptedException ie) {
                    log.info("Interrupted the join all.");
                }
            }
        }
    }
}