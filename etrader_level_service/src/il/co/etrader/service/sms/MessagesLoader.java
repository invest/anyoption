package il.co.etrader.service.sms;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import il.co.etrader.service.sms.managers.SMSManager;
import il.co.etrader.sms.SMS;

public class MessagesLoader extends Thread {
    private static final Logger log = Logger.getLogger(MessagesLoader.class);

    private List<SMS> queue;
    private int messagesBatch;
    private boolean running;

    public MessagesLoader(List<SMS> queue, int messagesBatch) {
        this.queue = queue;
        this.messagesBatch = messagesBatch;
    }

    public void run() {
        log.info("MessagesLoader started. messagesBatch: " + messagesBatch);
        Thread.currentThread().setName("MessagesLoader");
        running = true;
        while (running) {

        	List<SMS> smsList = null;
        	try {
        		int maxToLoad = this.messagesBatch - queue.size();
        		if (maxToLoad > 0) {
        			log.info("going to pull queued messages from db, maxToLoad: " + maxToLoad);
        			smsList = SMSManager.loadMessages(maxToLoad, getLoadedList());
        		}
			} catch (SQLException e) {
				log.error("Problem to pull messages from DB. " + e);
			}

			if (null != smsList && smsList.size() > 0) {  // have new messages
				for (SMS s : smsList) {
					synchronized (queue) {
						queue.add(s);
					}
				}
			}
			log.debug("queue.size():" + queue.size());

            try {
                Thread.sleep(500);
            } catch (InterruptedException ie) {
                // do nothing
            }
        }
        log.info("MessagesLoader done.");
    }

    public void stopMessagesLoader() {
        log.info("Stopping MessagesLoader...");
        running = false;
    }

    /**
     * Return the loaded smsId's list
     * @return
     */
    private String getLoadedList() {
    	String list = null;
    	if (null != queue && queue.size() > 0) {
    		list = "";
    	}
    	for (SMS s : queue) {
    		list += s.getId() + ",";
    	}
    	if (null != list) {
    		list = list.substring(0, list.length()-1);
    	}
    	return list;
    }

}