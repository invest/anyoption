package il.co.etrader.service.sms;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import il.co.etrader.service.sms.managers.SMSManager;
import il.co.etrader.sms.SMS;


public class SMSServiceEngine extends Thread {
    private static final Logger log = Logger.getLogger(SMSServiceEngine.class);

    private int messagesBatch;
    private int workersCount;
    private int maxRetries;
    private List<SMS> queue;
    private List<SMSWorker> workers;
    private MessagesLoader messageLoader;
    private boolean running;

    public SMSServiceEngine(int messagesBatch, int workersCount, int maxRetries) {
        this.messagesBatch = messagesBatch;
        this.workersCount = workersCount;
        this.maxRetries = maxRetries;
        this.queue = new LinkedList<SMS>();
    }

    @Override
    public void run() {
        log.info("SMSServiceEngine started.");
        Thread.currentThread().setName("SMSServiceEngine");

        try {
        	SMSManager.loadSMSProviders(maxRetries);
        } catch (Exception e) {
        	log.fatal("Error to load smsProviders", e);
        }

        // starting messageLoader Thread
        messageLoader = new MessagesLoader(queue, messagesBatch);
        messageLoader.start();
        // starting workers Threads
        workers = new ArrayList<SMSWorker>();
        for (int i = 1 ; i <= workersCount ; i ++) {
        	SMSWorker w = new SMSWorker(this);
        	w.setName("sms-worker-" + i);
        	w.start();
        	workers.add(w);
        }

        running = true;
        while (running) {
            log.debug("queue.size(): " + queue.size());
            try {
                synchronized (queue) {
            		for (SMS msg : queue) {
                		if (!msg.isWorkerHandle()) {
        	        		log.debug("going to give to worker msg " + msg.getId());
        	        		SMSWorker w = getAvailableWorker();
        	        		if (null != w) {
        	        		    log.debug("got worker " + w.getName());
//        	        			msg.setMaxRetries(maxRetries);
        	        		    //ugly way to temp resolve leading zero for IL numbers...
        	        		    if(msg.getSkinId() == 1) {
        	        			msg.setPhoneNumber(msg.getPhoneNumber().replaceFirst("9720", "972"));
        	        		    }
        		        		synchronized (w) {
                                    w.setMessage(msg);
                                    w.setBusy(true);
        		        		    log.debug("notify worker " + w.getName());
        		        			w.notify();
        		                }
        		        		msg.setWorkerHandle(true);
        	        		} else {
        	        			log.debug("all workers are busy!");
        	        		}
                		} else {
                		    log.debug("msg in progress " + msg.getId());
                		}
                	}
                }
            } catch (Exception e) {
                log.error("", e);
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException ie) {
                // do nothing
            }
        }
        SMSManager.closeSMSProviders();
        log.info("SMSServiceEngine done.");
    }

    public void stopSMSServiceEngine() {
        log.info("Stopping SMSServiceEngine...");
        running = false;
        // stop workers threads
       	for (SMSWorker w : workers) {
        	w.stopWorker();
       	}
       	messageLoader.stopMessagesLoader();
    }

    /**
     * Get available worker from workers threads list
     * @return
     */
    private SMSWorker getAvailableWorker() {
    	for (SMSWorker w : workers) {
    		if (!w.isBusy()) {
    			return w;
    		}
    	}
    	return null;
    }

    /**
     * Remove message from the queue
     * @param msg
     */
    public void removeMessage(SMS msg) {
    	synchronized (queue) {
    		queue.remove(msg);
    	}
    	log.debug("removeMessage: " + msg.getId());
    }

	/**
	 * @return the messagesBatch
	 */
	public int getMessagesBatch() {
		return messagesBatch;
	}

	/**
	 * @param messagesBatch the messagesBatch to set
	 */
	public void setMessagesBatch(int messagesBatch) {
		this.messagesBatch = messagesBatch;
	}

	/**
	 * @return the workersCount
	 */
	public int getWorkersCount() {
		return workersCount;
	}

	/**
	 * @param workersCount the workersCount to set
	 */
	public void setWorkersCount(int workersCount) {
		this.workersCount = workersCount;
	}

	/**
	 * @return the maxRetries
	 */
	public int getMaxRetries() {
		return maxRetries;
	}

	/**
	 * @param maxRetries the maxRetries to set
	 */
	public void setMaxRetries(int maxRetries) {
		this.maxRetries = maxRetries;
	}

}