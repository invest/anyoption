package il.co.etrader.service.sms;

public interface SMSServiceMBean {
    public void startService();
    public void stopService();

    public void setMessagesBatch(int messagesBatch);
    public int getMessagesBatch();

    public void setWorkersCount(int workersCount);
    public int getWorkersCount();

    public void setMaxRetries(int maxRetries);
    public int getMaxRetries();
}