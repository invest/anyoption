package il.co.etrader.service.sms.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.sms.SMS;
import il.co.etrader.util.CommonUtil;

public class SMSDAO extends DAOBase {
    public static SMS getMessage(Connection conn, long msgId) throws SQLException {
        SMS sms = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "* " +
                    "FROM " +
                        "sms " +
                    "WHERE " +
                        "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, msgId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                sms = SMS.getInstance(rs);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return sms;
    }

    public static SMS getMessage(Connection conn, String reference) throws SQLException {
        SMS sms = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "* " +
                    "FROM " +
                        "sms " +
                    "WHERE " +
                        "reference = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, reference);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                sms = SMS.getInstance(rs);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return sms;
    }
    
    public static List<SMS> loadMessages(Connection conn, int maxToLoad, String loadedList) throws SQLException {
        List<SMS> l = new LinkedList<SMS>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "* " +
                    "FROM " +
                        "(SELECT " +
                            "* " +
                        "FROM " +
                            "sms " +
                        "WHERE " +
                            " ( sms_status_id = " + SMS.STATUS_QUEUED + " OR sms_status_id = " + SMS.STATUS_NOT_VERIFIED + " ) " + 
                            " AND scheduled_time <= sysdate ";
            if (null != loadedList) {
            	sql += "AND id not in (" + loadedList + ") ";
            }

            sql += "ORDER BY " +
                       "scheduled_time, " +
                       "id) " +
                   "WHERE " +
                       "rownum < ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, maxToLoad);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                l.add(SMS.getInstance(rs));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
    }

    /**
     * Update sms record by id
     * @param con db connection
     * @param retries number of sending retries
     * @param timeSent time of the submit request
     * @param statusId message status
     * @param error code & message description from the response
     * @throws SQLException
     */
    public static void updateMessageAfterSubmit(Connection con, long msgId, long retries, Date timeSent, long statusId, String error,
    		String reference, Date timeScheduled) throws SQLException {

    	PreparedStatement pstmt = null;
    	try {
    		String sql =
    			"UPDATE " +
    				"sms " +
    			"SET " +
    				"retries = ?, " +
    				"time_sent = ?, " +
    				"sms_status_id = ?, " +
    				"reference = ?, " +
    				"error = concat(error, ? || '/'), " +
    				"scheduled_time = ? " +
    			"WHERE " +
    				"id = ?";

    		pstmt = con.prepareStatement(sql);
    		pstmt.setLong(1, retries);
    		pstmt.setTimestamp(2, CommonUtil.convertToTimeStamp(timeSent));
    		pstmt.setLong(3, statusId);
    		pstmt.setString(4, reference);
    		pstmt.setString(5, error);
    		pstmt.setTimestamp(6, CommonUtil.convertToTimeStamp(timeScheduled));
    		pstmt.setLong(7, msgId);
    		pstmt.executeUpdate();
    	} finally {
    		closeStatement(pstmt);
    	}
    }

    /**
     * Update message status
     * @param con
     * @param statusId
     * @param smsId
     * @throws SQLException
     */
    public static void updateMessageStatus(Connection con, long statusId, long smsId, String reference, String error) throws SQLException {

    	PreparedStatement pstmt = null;
    	try {
    		String sql =
    			"UPDATE " +
    				"sms " +
    			"SET " +
    				"sms_status_id = ?, " +
    				"reference = ?, " +
    				"error = concat(error, ? || '/')" +
    			"WHERE " +
    				"id = ?";
    		pstmt = con.prepareStatement(sql);
    		pstmt.setLong(1, statusId);
    		pstmt.setString(2, reference);
    		if (null != error) {
    			pstmt.setString(3, error);
    		} else {
    			pstmt.setString(3, null);
    		}
    		pstmt.setLong(4, smsId);
    		pstmt.executeUpdate();
    	} finally {
    		closeStatement(pstmt);
    	}
    }

    /**
     * Insert new DRL
     * @param conn db connection
     * @param messageId the message that related to the given dlr
     * @param reference
     * @param status dlr status of the message
     * @param timeRecived
     * @throws SQLException
     */
    public static void insertDLR(Connection conn, long messageId, String reference,
            	String status, Date timeRecived) throws SQLException {

    	PreparedStatement opstmt = null;
        try {
            String sql =
                "INSERT INTO sms_dlrs " +
                    "(id, sms_message_id, reference, status, time_received) " +
                 "VALUES " +
                     "(SEQ_SMS_DLRS.NEXTVAL, ?, ?, ?, ?)";

            opstmt =  conn.prepareStatement(sql);
            opstmt.setLong(1, messageId);
            opstmt.setString(2, reference);
            opstmt.setString(3, status);
            opstmt.setTimestamp(4, CommonUtil.convertToTimeStamp(timeRecived));
            opstmt.executeUpdate();
        } finally {
            closeStatement(opstmt);
        }
    }
}