package il.co.etrader.service.sms;

import java.util.Date;

import org.apache.log4j.Logger;

import il.co.etrader.service.sms.managers.SMSManager;
import il.co.etrader.sms.SMS;

public class SMSWorker extends Thread {
    private static final Logger log = Logger.getLogger(SMSWorker.class);

    private SMS message;
    private boolean running;
    private boolean isBusy;
    private SMSServiceEngine smsEngine;

    public SMSWorker(SMSServiceEngine smsEngine) {
    	this.smsEngine = smsEngine;
    }

    public void run() {
        log.info("SMSWorker started.");
        running = true;
        while (running) {
        	if (null != message) {  // have message to work on
        		try {
	        		log.debug("going to send message. " + message.toString());
	        		SMSManager.send(message);
	        		log.trace("message sent.");
	        		
	        		SMSManager.updateMessageAfterSubmit(message.getId(), message.getRetries(), message.getTimeSent(),
	        				message.getStatus(), message.getError(), message.getReference(), message.getScheduledTime());
	        		log.trace("message updated in db.");
        		} catch (Exception e) {
        			log.error("Error while sending sms!", e);
        			try {
            			if (message.getRetries() < smsEngine.getMaxRetries()) {
            			    SMSManager.updateMessageAfterSubmit(message.getId(), message.getRetries() + 1, message.getTimeSent(), SMS.STATUS_QUEUED, "Exception: " + e.getMessage(), null, new Date(System.currentTimeMillis() + 120000)); // after 2 min
            			} else {
                            SMSManager.updateMessageAfterSubmit(message.getId(), message.getRetries(), message.getTimeSent(), SMS.STATUS_FAILED_TO_SUBMIT, "Exception: " + e.getMessage(), null, message.getScheduledTime());
            			}
        			} catch (Exception ie) {
        			    log.error("Can't update failed message in db.", e);
        			}
				} finally {
                    smsEngine.removeMessage(message);
				}
        	}
            try {
            	 synchronized (this) {
                     log.debug("worker free.");
                     message = null;
                     isBusy = false;
            		 wait();  // waits for notify() call from Engine
            	 }
            } catch (InterruptedException ie) {
                // do nothing
            }
        }
        log.info("SMSWorker done.");
    }

    public void stopWorker() {
        log.info("Stopping SMSWorker...");
        running = false;
        synchronized (this) {
            notify();
        }
    }

	/**
	 * @return the message
	 */
	public SMS getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(SMS message) {
		this.message = message;
	}

	/**
	 * @return the isBusy
	 */
	public boolean isBusy() {
        return isBusy;
	}

	/**
	 * @param isBusy the isBusy to set
	 */
	public void setBusy(boolean isBusy) {
        this.isBusy = isBusy;
	}
}