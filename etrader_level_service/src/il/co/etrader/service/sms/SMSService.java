package il.co.etrader.service.sms;

import org.apache.log4j.Logger;

public class SMSService implements SMSServiceMBean {
    private static Logger log = Logger.getLogger(SMSService.class);

    private int messagesBatch;
    private int workersCount;
    private int maxRetries;
    private SMSServiceEngine engine;

    public int getMessagesBatch() {
        return messagesBatch;
    }

    public void setMessagesBatch(int messagesBatch) {
        this.messagesBatch = messagesBatch;
    }

    public int getWorkersCount() {
        return workersCount;
    }

    public void setWorkersCount(int workersCount) {
        this.workersCount = workersCount;
    }

    /**
	 * @return the maxRetries
	 */
	public int getMaxRetries() {
		return maxRetries;
	}

	/**
	 * @param maxRetries the maxRetries to set
	 */
	public void setMaxRetries(int maxRetries) {
		this.maxRetries = maxRetries;
	}

	public void startService() {
        log.debug("SMSService.startService begin");
        if (null == engine) {
            engine = new SMSServiceEngine(messagesBatch, workersCount, maxRetries);
            engine.start();
            log.info("SMSService started");
        } else {
            log.warn("SMSService already started. Doing nothing");
        }
    }

    public void stopService() {
        log.debug("SMSService.stopService begin");
        if (null != engine) {
            engine.stopSMSServiceEngine();
            engine = null;
            log.info("SMSService stopped");
        } else {
            log.warn("No SMSService started.");
        }
    }
}