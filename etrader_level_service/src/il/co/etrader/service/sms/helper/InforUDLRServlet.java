package il.co.etrader.service.sms.helper;

import java.util.Date;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import il.co.etrader.service.sms.managers.SMSManager;
import il.co.etrader.sms.SMS;

public class InforUDLRServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(InforUDLRServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        doDLR(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        doDLR(request, response);
    }

    protected void doDLR(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        if (log.isTraceEnabled()) {
            String ls = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer();
            Enumeration paramNames = request.getParameterNames();
            while (paramNames.hasMoreElements()) {
                String paramName = (String) paramNames.nextElement();
                sb.append(paramName).append(": ").append(request.getParameter((paramName))).append(ls);
            }
            log.trace("Got SMS DLR, parametersMap: " + ls + sb.toString());
        }
        try {
            String status = request.getParameter("Status");
            String statusDescription = request.getParameter("StatusDescription");
            String msgId = request.getParameter("CustomerMessageId");
            long mId = Long.parseLong(msgId);
            long sId = SMS.STATUS_FAILED;
            if (null != status && status.equals("2")) {
                sId = SMS.STATUS_DELIVERED;
            }
            SMSManager.insertDLR(mId, "", statusDescription, new Date());
            SMSManager.updateMessageStatus(mId, sId, "", statusDescription);
        } catch (Exception e) {
            log.error("Failed to process DLR.", e);
        }
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().write("OK");
        response.setStatus(HttpServletResponse.SC_OK);
    }
}