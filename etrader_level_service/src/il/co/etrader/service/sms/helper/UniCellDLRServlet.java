package il.co.etrader.service.sms.helper;

import java.util.Date;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import il.co.etrader.service.sms.managers.SMSManager;
import il.co.etrader.sms.SMS;

public class UniCellDLRServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(UniCellDLRServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        doDLR(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
    	doDLR(request, response);
    }

    protected void doDLR(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
 	    StringBuffer sb = new StringBuffer();
 	    String ls = System.getProperty("line.separator");
        Enumeration paramNames = request.getParameterNames();
        while(paramNames.hasMoreElements()) {
          String paramName = (String)paramNames.nextElement();
          sb.append(paramName).append(": ").append(request.getParameter((paramName))).append(ls);
        }
        if (log.isTraceEnabled()) {
            log.trace("Got SMS DLR, parametersMap: " + ls + sb.toString());
        }
        if (request.getParameterMap().size() > 1) {  // need at least smsId and stateCode
	        String reference = (String)request.getParameter("reference");
	        String smsId = (String) request.getParameter("external_reference");
	        String finalStateCode = (String) request.getParameter("final_state_code");  // status code
	        String finalStateTime = (String) request.getParameter("final_state_time");
	        String finalStateReason = (String) request.getParameter("final_state_reason");
	        String finalStateNetCode = (String) request.getParameter("final_state_network_code");
	        String operation = (String) request.getParameter("operation");
	        String finalStateNetDes = (String) request.getParameter("final_state_network_description");
	        String homeNetId = (String) request.getParameter("home_network_id");
	        try {
		        long code = Long.parseLong(finalStateCode);
		        long smsIdL = Long.parseLong(smsId);
		         // insert DLR and upadte the message
		         log.info("Going to insert new DLR, smsId: " + smsIdL + " stateCode: " + code);
		         SMSManager.insertDLR(smsIdL, reference, finalStateCode, new Date());
		         String error = null;
		         long smsStatus = SMS.STATUS_DELIVERED;
		         if (code > 0) {  // got error
		        	 error = "stateCode: " + code + ",reason: " + finalStateReason + ",stateNetCode: " + finalStateNetCode +
		        	 			",homeNetId: " + homeNetId + ",netDesc: " + finalStateNetDes + ",time: " + finalStateTime +
		        	 				",operation: " +operation;
		        	 smsStatus = SMS.STATUS_FAILED;
		         }
		         log.info("Going to update smsId: " + smsIdL + " with status: " + smsStatus);
		         SMSManager.updateMessageStatus(smsIdL,smsStatus, reference, error);
	        } catch (Exception e) {
	        	log.error("Problem with the DLR! " + e);
	        }
        } else {
        	log.warn("got DLR but without required parmeters!");
        }
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().write("OK");
        response.setStatus(HttpServletResponse.SC_OK);
    }
}