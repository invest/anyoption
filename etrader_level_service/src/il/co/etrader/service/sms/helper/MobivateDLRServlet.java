package il.co.etrader.service.sms.helper;

import java.util.Date;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.anyoption.common.beans.base.User.MobileNumberValidation;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.util.ClearingUtil;

import il.co.etrader.service.sms.managers.SMSManager;
import il.co.etrader.sms.SMS;
import il.co.etrader.sms.TextSMS;

public class MobivateDLRServlet extends HttpServlet{

    private static final long serialVersionUID = -3930948761469578688L;
    private static final Logger log = Logger.getLogger(MobivateDLRServlet.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        doDLR(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        doDLR(request, response);
    }

    /*
	* MOBIVATE Status reference
	* 	1 -> DELIVERED		Message delivered to its destination
	* 	2 -> ACCEPTED		Message accepted by gateway
	* 	3 -> EXPIRED		Message validity period expired
	* 	4 -> DELETED		Message deleted
	* 	5 -> UNDELIVERABLE	Message is undeliverable
	* 	6 -> UNKNOWN		Message is in unknown state
	* 	7 -> REJECTED		Message rejected
	* 	8 -> interim queued
	* 	? -> SENT		Message delivered to gateway
	*/
    protected void doDLR(HttpServletRequest request, HttpServletResponse response)
		    throws ServletException, java.io.IOException {
	    String ls = System.getProperty("line.separator");
	    StringBuffer sb = new StringBuffer();
	    Enumeration<String> paramNames = request.getParameterNames();
	    while (paramNames.hasMoreElements()) {
	    	String paramName = (String) paramNames.nextElement();
	    	sb.append(paramName).append(": ").append(request.getParameter((paramName))).append(ls);
	    }
	    String messagе = sb.toString();
	    log.trace("Got SMS DLR, parametersMap: " + ls + messagе);

		try {
		    messagе = ( (messagе.replaceFirst("xml:", "")).replaceFirst("Send: Send", "") ).trim();
		    log.debug(messagе);
		    Document doc = ClearingUtil.parseXMLToDocument(messagе);
		    Element root = doc.getDocumentElement();
		    NodeList hlrNode = root.getElementsByTagName("hlr");
		    boolean isHlr = (hlrNode != null && hlrNode.getLength() >0); 
		    String statusCode = ClearingUtil.getElementValue(root, "statusCode/*");
		    log.trace("Status code: " + statusCode);
		    String statusDescription = ClearingUtil.getElementValue(root, "status/*");
		    log.trace("Status Description: " + statusDescription);
		    String msgId = ClearingUtil.getElementValue(root, "clientReference/*");
		    long mId = Long.parseLong(msgId);
		    SMS message = SMSManager.getMessage(mId);
		    log.trace( (isHlr ? "HLR" : "") + " Message :"+msgId + " in status :" + message.getStatus());
		    long sId = message.getStatus();
		    
		    sId = SMS.STATUS_FAILED;
		    //just in case new or undocumented in their API status comes up so we can have records in db
		    if (null != statusCode && statusCode.equals("1")) {
		    	sId = SMS.STATUS_DELIVERED;
		    } else if (null != statusCode && statusCode.equals("2")) {
		    	sId = SMS.STATUS_DELIVERED_TO_SMSC;
		    } else if (null != statusCode && statusCode.equals("3")) {
		    	sId = SMS.STATUS_FAILED;
		    } else if (null != statusCode && statusCode.equals("4")) {
		    	sId = SMS.STATUS_FAILED;
		    } else if (null != statusCode && statusCode.equals("5")) {
		    	sId = SMS.STATUS_UNROUTABLE;
		    } else if (null != statusCode && statusCode.equals("6")) {
		    	sId = SMS.STATUS_FAILED;
		    } else if (null != statusCode && statusCode.equals("7")) {
		    	sId = SMS.STATUS_FAILED_TO_SUBMIT;
		    } else if (null != statusCode && statusCode.equals("8")) {
		    	sId = SMS.STATUS_QUEUED_ON_SMSC;
		    }
 
		    
		    String reference = "";
	    	// check current state of the message ( skip if already verified)		    	
	    	if(message.getStatus() == SMS.STATUS_WAITING_HLR) {
	    		if(isHlr) {
	 		    	MobileNumberValidation mnv;
			    	if(sId == SMS.STATUS_DELIVERED) {
			    		// if HLR is successfully delivered the mobile number is valid 
			    		sId = SMS.STATUS_QUEUED;
			    		mnv = MobileNumberValidation.VALID;
			    		log.debug("message is HLR VALID");
			    	} else {
			    		sId = SMS.STATUS_INVALID;
			    		mnv = MobileNumberValidation.INVALID;
			    		log.debug("message is HLR INVALID");
			    	}
			    	long userId = ((TextSMS)message).getKeyValue();
			    	if(userId != 0) {
			    		UsersManagerBase.updateUserPhoneValidated(userId, mnv);
			    		log.debug("updated userID :" +userId);
			    	} else {
			    		log.debug("sms_key_types is not \"Users\" can't update user table: "+mId);
			    	}
			    	reference = "HLR-"+ mnv.name();
		    	} else {
		    		reference = " HLR " + statusDescription;
		    		log.trace("received status " + sId+ " for message :" +messagе + " in status:" + message.getStatus());
		    		sId = message.getStatus();
		    	}
		    } else {
		    	if(isHlr) {
		    		log.warn("HLR for message in wrong status :"+message.getId());
		    		statusDescription = (isHlr ? "HLR" : "") ;
		    	}
		    }
		    
	    	SMSManager.insertDLR(mId, reference, statusDescription, new Date());
	    	log.debug("updating mID:"+mId + " to status :"+ sId);
		    SMSManager.updateMessageStatus(mId, sId, "", statusDescription);
		} catch (Exception e) {
		    log.error("Failed to process DLR.", e);
		}
		response.setHeader("Cache-Control", "no-cache");
		response.getWriter().write("OK");
		response.setStatus(HttpServletResponse.SC_OK);
    }
}