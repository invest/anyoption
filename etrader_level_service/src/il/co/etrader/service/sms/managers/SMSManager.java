package il.co.etrader.service.sms.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.sms.SMSException;

import il.co.etrader.bl_managers.SMSManagerBase;
import il.co.etrader.service.sms.daos.SMSDAO;
import il.co.etrader.sms.SMS;
import il.co.etrader.sms.SMSProvider;

public class SMSManager extends BaseBLManager {
    public static SMS getMessage(long msgId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return SMSDAO.getMessage(conn, msgId);
        } finally {
            closeConnection(conn);
        }
    }

    public static SMS getMessage(String reference) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return SMSDAO.getMessage(conn, reference);
        } finally {
            closeConnection(conn);
        }
    }

	/**
	 * Load messages
	 * @param maxToLoad max number of messages to load
	 * @return
	 * @throws SQLException
	 */
    public static List<SMS> loadMessages(int maxToLoad, String loadedList) throws SQLException {
    	List<SMS> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l= SMSDAO.loadMessages(conn, maxToLoad, loadedList);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    /**
     * Load SMS Provider
     * @throws SMSException
     */
    public static void loadSMSProviders(int maxRetries) throws SMSException {
        Connection conn = null;
        try {
        	 conn = getConnection();
             SMSManagerBase.loadSMSProviders(conn, maxRetries);
        } catch (SMSException se) {
            throw se;
        } catch (Throwable t) {
            throw new SMSException("Error loading sms provider.", t);
        } finally {
        	closeConnection(conn);
        }
    }

    /**
     * Update sms record
     * @param retries
     * @param timeSent
     * @param statusId
     * @param error
     * @throws SQLException
     */
    public static void updateMessageAfterSubmit(long msgId, long retries, Date timeSent, long statusId, String error, String reference, Date timeScheduled) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		SMSDAO.updateMessageAfterSubmit(con, msgId, retries, timeSent, statusId, error, reference, timeScheduled);
    	} finally {
        	closeConnection(con);
        }
    }

    /**
     * Update message status
     * @param statusId
     * @param smsId
     * @throws SQLException
     */
    public static void updateMessageStatus(long smsId, long statusId, String reference, String error) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		SMSDAO.updateMessageStatus(con, statusId, smsId, reference, error);
    	} finally {
        	closeConnection(con);
        }
    }

    /**
     * send message to provider
     *
     * @param msg
     * @throws SMSException
     */
    public static void send(SMS msg) throws SMSException {
    	SMSProvider provider = SMSManagerBase.getProviderById(msg.getProviderId());
    	provider.send(msg);
    }

    /**
     * Insert DLR
     * @throws SQLException
     */
    public static void insertDLR(long messageId, String reference,
        	String status, Date timeRecived) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		SMSDAO.insertDLR(con, messageId, reference, status, timeRecived);
    	} finally {
        	closeConnection(con);
        }
    }
    
    public static void closeSMSProviders() {
        SMSManagerBase.closeSMSProviders();
    }
}