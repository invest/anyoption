package il.co.etrader.service.inatec.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.daos.DAOBase;

import il.co.etrader.bl_managers.TransactionsManagerBase;

public class InatecDAO extends DAOBase {

	/**
	 * Get inatec transaction in status 'in progress'
	 * @param conn db connection
	 * @return
	 * @throws SQLException
	 */
    public static ArrayList<Transaction> getInProgressTrx(Connection conn) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Transaction> list = new ArrayList<Transaction>();
        try {
            String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "transactions t, " +
                    "transaction_types tt, " +
                    "transaction_statuses ts " +
                "WHERE " +
                	"t.type_id = tt.id AND " +
                	"t.status_id = ts.id AND " +
                	"t.type_id = ? AND " +
                	"(t.status_id = ? OR (t.status_id = ? AND (SYSDATE - INTERVAL '30' MINUTE) >= t.time_created) )";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, TransactionsManagerBase.TRANS_TYPE_DIRECT_BANK_DEPOSIT);
            ps.setLong(2, TransactionsManagerBase.TRANS_STATUS_IN_PROGRESS);
            ps.setLong(3, TransactionsManagerBase.TRANS_STATUS_AUTHENTICATE);

            rs = ps.executeQuery();
            while (rs.next()) {
            	Transaction vo = new Transaction();
                vo.setId(rs.getLong("id"));
                vo.setUserId(rs.getLong("user_id"));
                vo.setAmount(rs.getLong("amount"));
                vo.setClearingProviderId(rs.getLong("clearing_provider_id"));
                vo.setAuthNumber(rs.getString("auth_number"));
                vo.setXorIdAuthorize(rs.getString("xor_id_authorize"));
                vo.setPaymentTypeId(rs.getLong("payment_type_id"));
                vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
                vo.setUtcOffsetCreated(rs.getString("utc_offset_created"));
                list.add(vo);
            }
        } finally {
            closeResultSet(rs);
        }
        return list;
    }
}
