package il.co.etrader.service.inatec.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.ClearingProvider;
import com.anyoption.common.clearing.InatecInfo;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.TransactionsDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.population.PopulationHandlersException;
import il.co.etrader.service.inatec.InatecService;
import il.co.etrader.service.inatec.daos.InatecDAO;
import il.co.etrader.util.ConstantsBase;

public class InatecManager extends BaseBLManager {
	private static Logger log = Logger.getLogger(InatecService.class);

	/**
	 * Get inatec in progress transactions
	 * @return ArrayList of <Transaction>
	 * @throws SQLException
	 */
    public static ArrayList<Transaction> getInProgressTrx() throws SQLException {
        ArrayList<Transaction> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = InatecDAO.getInProgressTrx(conn);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    /**
     * Load Inatec clearingProvider
     * @throws ClearingException
     */
    public static void loadClearingProvider() throws ClearingException {
        Connection conn = null;
        try {
        	 conn = getConnection();
             ClearingManager.loadClearingProviders(conn);
        } catch (ClearingException ce) {
            throw ce;
        } catch (Throwable t) {
            throw new ClearingException("Error loading clearing provider.", t);
        } finally {
        	closeConnection(conn);
        }
    }

	/**
	 * Load population handlers
	 * @throws PopulationHandlersException
	 */
    public static void loadPopulatioHandlers() throws PopulationHandlersException {
        Connection conn = null;
        try {
            conn = getConnection();
            PopulationsManagerBase.loadPopulatioHandlers(conn);
        } catch (PopulationHandlersException phx) {
        	throw(phx);
		} catch (Throwable t) {
            throw new PopulationHandlersException("Error loading population handlers.", t);
        } finally {
        	closeConnection(conn);
        }
    }

    /**
     *
     * @param t
     * @return
     * @throws ClearingException
     * @throws SQLException
     */
    public static Transaction inatecStatusReqCall(Transaction t) throws ClearingException, SQLException  {
    	Connection con = null;
    	InatecInfo info = null;
    	boolean updateDb = true;
    	try {
    		info = new InatecInfo();
            info.setTransactionId(t.getId());
            info.setProviderId(t.getClearingProviderId());
            info.setAuthNumber(t.getXorIdAuthorize());
            info.setInatecPaymentType(t.getPaymentTypeId());
            //ClearingManager.onlineStatusResult(info);
            ClearingManager.powerPayStatusResult(info);
            con = getConnection();

            if (info.isSuccessful()) {
                t.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
                t.setDescription(null);
                t.setXorIdCapture(info.getAuthNumber());
                try {
                	con.setAutoCommit(false);
	                UsersDAOBase.addToBalance(con, t.getUserId(), t.getAmount());
	                GeneralDAO.insertBalanceLog(con, 0, t.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, t.getId(), ConstantsBase.LOG_BALANCE_DIRECT_DEPOSIT, t.getUtcOffsetCreated());
                	t.setComments(info.getResult() + "|" + info.getMessage() + "|" + info.getUserMessage() + "|" + info.getProviderTransactionId());
                	TransactionsDAOBase.updatePurchase(con, t.getId(), t.getStatusId(), t.getDescription(), t.getComments(), t.getXorIdCapture(), t.getUtcOffsetCreated(), Writer.WRITER_ID_AUTO);
	                con.commit();
                } catch (SQLException e) {
            		log.error("Exception in finishInatecTransaction! success case. ", e);
            		try {
            			con.rollback();
            		} catch (Throwable it) {
            			log.error("Can't rollback.", it);
            		}
            		throw e;
            	} finally {
            		try {
            			con.setAutoCommit(true);
            		} catch (Exception e) {
            			log.error("Can't set back to autocommit.", e);
            		}
            	}
    			try {
    				PopulationsManagerBase.deposit(con, t.getUserId(), true, 0, t);
    			} catch (Exception e) {
    				log.warn("Problem with population succeed deposit event ", e);
    			}
                try {
                    TransactionsManagerBase.afterTransactionSuccess(t.getId(), t.getUserId(), t.getAmount(), t.getWriterId(), 0L);
                } catch (Exception e) {
                    log.error("Problem processing bonuses after success transaction", e);
                }
            } else {
        		if (info.isNeedNotification()) {  // still pending
        			updateDb = false;
        		} else {
              		t.setStatusId(TransactionsManagerBase.TRANS_STATUS_FAILED);
            		t.setDescription(null);
       				try {
    					PopulationsManagerBase.deposit(con, t.getUserId(), false, 0, t);
    				} catch (Exception e) {
    					log.warn("Problem with population failed deposit event ", e);
    				}
                	t.setComments(info.getResult() + "|" + info.getMessage() + "|" + info.getUserMessage() + "|" + info.getProviderTransactionId());
                	TransactionsDAOBase.updatePurchase(con, t.getId(), t.getStatusId(), t.getDescription(), t.getComments(), t.getXorIdCapture(), t.getUtcOffsetCreated(), Writer.WRITER_ID_AUTO);
        		}
       		}
    	} finally {
    		closeConnection(con);
    	}

    	if (updateDb) {   // got final status from Inatec
    		return t;
    	} else {
    		return null;
    	}
    }
}