/**
 *
 */
package il.co.etrader.service.inatec;

import org.apache.log4j.Logger;

/**
 * Inatec pulling service
 *
 *
 * @author Kobi
 *
 */
public class InatecService implements InatecServiceMBean {
    private static Logger log = Logger.getLogger(InatecService.class);

    private InatecStatusReqUpdater updater;

    public void startService() {
        log.debug("InatecService.startService begin");
        updater = new InatecStatusReqUpdater();
        updater.start();
        log.info("InatecService started");
    }

    public void stopService() {
        log.debug("InatecService.stopService begin");
        if (null != updater) {
            updater.stopInatecStatusReqUpdater();
            updater = null;
        }
        log.info("InatecService stopped");
    }

}
