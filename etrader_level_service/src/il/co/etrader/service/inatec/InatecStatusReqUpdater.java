package il.co.etrader.service.inatec;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.util.CommonUtil;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.service.inatec.managers.InatecManager;

/**
 * This updater pull inatec 'in progress' transactions (transactions that don't have final status)
 * and make statusRequest call to Inatec for getting final status.
 *
 * @author Kobi
 *
 */
public class InatecStatusReqUpdater extends Thread {
    private static Logger log = Logger.getLogger(InatecService.class);

    private boolean running;
    private static HashMap<Long, Transaction> pulledTransactions = new HashMap<Long, Transaction>();

    public void run() {
        try {
			InatecManager.loadClearingProvider();
			InatecManager.loadPopulatioHandlers(); 
			ApplicationDataBase.init();
		} catch (Throwable e) {
			log.fatal("Can't load clearing providers / population handlers.", e);
			return;
		}
        Thread.currentThread().setName("InatecStatusReqUpdater");
        if (log.isInfoEnabled()) {
            log.info("InatecStatusReqUpdater started.");
        }
        running = true;

        while (running) {
            log.debug("Getting Inatec in progress transactions.");

            ArrayList<Transaction> transactions = null;
            try {
				transactions = InatecManager.getInProgressTrx();
			} catch (SQLException e) {
				log.error("Error! problem to pull in progress trx from DB," + e);
			}

			try {
				// fill relevant transactions
				for (Transaction t : transactions) {
					insertToPulledList(t);
				}
				
				ArrayList<Long> needToRemoveList = new ArrayList<Long>();
	            for (Iterator<Long> iter = pulledTransactions.keySet().iterator() ; iter.hasNext();) {
	            	Long id = iter.next();
	            	Transaction t = pulledTransactions.get(id);
	            	//long now = System.currentTimeMillis();
	            	//if ((now - t.getTimeCreated().getTime()) <=  1 * 60 * 60 * 1000) {  // 1 hour
	            		log.debug("going to call inatec statusRequest for trx: " + t.getId());
	            		try{
	            			Transaction res = InatecManager.inatecStatusReqCall(t);
		            		if (null != res) {  // got final result
		            			String statusTxt = "";
		            			if (t.getStatusId() == TransactionsManagerBase.TRANS_STATUS_SUCCEED) {
		            				statusTxt = "SUCCEED";
		            			} else if (t.getStatusId() == TransactionsManagerBase.TRANS_STATUS_FAILED) {
		            				statusTxt = "FAILED";
		            				UsersManagerBase.setUserLastDecline(t.getUserId(), true, t);
		            			}
		            			log.debug("statusResponse with status: " + statusTxt);
		            			needToRemoveList.add(new Long(t.getId()));
		            		} else {
		            			log.debug("statusResponse still pending.");
		            		}
	            		}catch(Exception e){
	            			sendEmail(t, e);
	            		}
	            	//} else {
	            	//	log.warn("An hour that Transaction id: " + t.getId() + " didn't get final status!");
	            		// change status to failed
	            		// and remove from the list
	            	//}
	            }

	            // remove trx with final status
	            for (Long id : needToRemoveList) {
	            	pulledTransactions.remove(id);
	            }

			} catch (Exception e) {
				log.error("Failed to proceed Inatec statusRequest call! ", e);
			}

            synchronized (this) {
                try {
                    wait(30 * 1000);
                } catch (InterruptedException ie) {
                    // do nothing
                }
            }
        }
        log.warn("InatecStatusReqUpdater done.");
    }

    public void stopInatecStatusReqUpdater() {
        log.info("Stopping InatecStatusReqUpdater...");
        running = false;
        synchronized (this) {
            notify();
        }
    }

    /**
     * Insert into pulled transactions Hash
     * @param trxId transaction id for insert
     * @param timeCreated time created of the transation
     */
    public void insertToPulledList(Transaction t) {
    	if (null == pulledTransactions.get(new Long(t.getId()))) {
    		pulledTransactions.put(new Long(t.getId()), t);
    	}
    }
    
    private void sendEmail(Transaction t, Exception ex) {
    	final String TAB = " \n ";
        // set the server properties
        Hashtable<String, String> serverProperties = new Hashtable<String, String>();
        serverProperties.put("url", "smtp.anyoption.com");
        serverProperties.put("auth", "true");
        serverProperties.put("user", "support@etrader.co.il");
        serverProperties.put("pass", "Setrader12!");
        String amountTxt = CommonUtil.displayAmount(t.getAmount(), true, t.getCurrency().getId(), false);
        String body = "Transaction info( "
        		+ "trx id : " + t.getId() + TAB
        		+ "User id : " + t.getUserId() + TAB
        		+ "Time trx created : " + t.getTimeCreatedTxt() + TAB
        		+ "Transaction type id : " + t.getTypeId() + TAB
        		+ "Transaction amount : " + amountTxt + TAB
        		+ "Exception : " + ex + TAB
        		+ " )";
       

        // Set the email properties
        Hashtable<String, String> emailProperties = new Hashtable<String, String>();
        emailProperties.put("subject", "anyoption_inatec_service exception with trx" + t.getId());
        emailProperties.put("from", "support@etrader.co.il");
        emailProperties.put("to", "QA@anyoption.com");
        emailProperties.put("body", body);

        CommonUtil.sendEmail(serverProperties, emailProperties, null);
    }

}