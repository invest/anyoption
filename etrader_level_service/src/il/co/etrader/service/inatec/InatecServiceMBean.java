package il.co.etrader.service.inatec;

public interface InatecServiceMBean {
    public void startService();
    public void stopService();
}