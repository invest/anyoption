//package il.co.etrader.service.transactionsIssues;
//
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.Hashtable;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.base.Skin;
//
//import il.co.etrader.service.transactionIssues.helper.UserForFirstDepositEmail;
//import il.co.etrader.service.transactionsIssues.managers.TransactionsIssuesManager;
//import il.co.etrader.util.CommonUtil;
//
//public class TransactionsIssuesUpdater extends Thread {
//    private static Logger log = Logger.getLogger(TransactionsIssuesService.class);
//
//    private boolean running;
//    private String emailUser;
//    private String emailPass;
//    private String emailFrom;
//    private String emailServer;
//    private String emailSubject;
//    private String emailToEtrader;
//    private String emailToAnyoption;
//    private String emailToAnyoptionZh;
//
//    public TransactionsIssuesUpdater(String emailUser, String emailPass, String emailFrom, String emailServer,
//    		String emailSubject, String emailToEtrader, String emailToAnyoption, String emailToAnyoptionZh) {
//		super();
//		this.emailUser = emailUser;
//		this.emailPass = emailPass;
//		this.emailFrom = emailFrom;
//		this.emailServer = emailServer;
//		this.emailSubject = emailSubject;
//		this.emailToEtrader = emailToEtrader;
//		this.emailToAnyoption = emailToAnyoption;
//		this.emailToAnyoptionZh = emailToAnyoptionZh;
//	}
//
//	public void run() {
//        Thread.currentThread().setName("TransactionsIssuesUpdater");
//        if (log.isInfoEnabled()) {
//            log.info("TransactionsIssuesUpdater started.");
//        }
//        running = true;
//        while (running) {
//            log.debug("Updating Transactions Issues.");
//            try {
//            	TransactionsIssuesManager.transactionsIssuesFill();
//            	TransactionsIssuesManager.transactionsIssuesCycleFill();
//            	sendEmail();
//            } catch (Exception e) {
//                log.error("Failed to update Transactions Issues.", e);
//            }
//            log.debug("Finished updating Transactions Issues.");
//            synchronized (this) {
//                try {
//                    wait(5 * 60 * 1000);
//                } catch (InterruptedException ie) {
//                    // do nothing
//                }
//            }
//        }
//        log.warn("TransactionsIssuesUpdater done.");
//    }
//
//    public void stopTransactionIssuesUpdater() {
//        log.info("Stopping TransactionsIssuesUpdater...");
//        running = false;
//        synchronized (this) {
//            notify();
//        }
//    }
//
//    private void sendEmail() throws SQLException {
//    	ArrayList<UserForFirstDepositEmail> users = TransactionsIssuesManager.getPastFiveMinutesTransactionsUsers();
//    	log.info("Got all the users from DB And found " + users.size() + " users");
//    	for(UserForFirstDepositEmail u : users) {
//	        log.info("Start inserting server properties data into hashtable");
//	    	boolean emailServerAuth = true;// need to find out who change this parameter from true to false
//	        Hashtable<String, String> serverProperties = new Hashtable<String, String>();
//	        serverProperties.put("url", emailServer);// need to chack what happand as test & live
//	        log.info("email server: " + emailServer);
//	        if (emailServerAuth) {
//	            serverProperties.put("auth", "true");
//	            serverProperties.put("user", emailUser);
//	            serverProperties.put("pass", emailPass);
//	        }
//	        log.info("Finished to insert server properties data to the hashtable");
//	        log.info("Start inserting email properties data into hashtable");
//	        // Set the email properties
//	        Hashtable<String, String> emailProperties = new Hashtable<String, String>();
//	        emailProperties.put("subject", emailSubject + " - " + u.diaplaySkin(u.getSkinId()));
//	        emailProperties.put("from", emailFrom);
//	        if (!CommonUtil.isHebrewSkin(u.getSkinId())) {
//	        	if (u.getSkinId() == Skin.SKIN_CHINESE) {
//	        		log.info("Send to Anyoption ZH reps");
//	        		emailProperties.put("to", emailToAnyoptionZh);
//	        	} else {
//	        		log.info("Send to Anyoption reps");
//	        		emailProperties.put("to", emailToAnyoption);
//	        	}
//	        } else {
//	        	log.info("Send to Etrader reps");
//	        	emailProperties.put("to", emailToEtrader);
//	        }
//	        emailProperties.put("body", u.toString());
//	        log.info("Finished to insert email properties data to the hashtable");
//	        CommonUtil.sendEmail(serverProperties, emailProperties, null);
//    	}
//    }
//}