//package il.co.etrader.service.transactionsIssues.managers;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//import java.util.ArrayList;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.managers.BaseBLManager;
//
//import il.co.etrader.service.transactionIssues.helper.UserForFirstDepositEmail;
//import il.co.etrader.service.transactionsIssues.daos.TransactionsIssuesDAO;
//
//
//
//public class TransactionsIssuesManager extends BaseBLManager {
//
//	private static final Logger logger = Logger.getLogger(TransactionsIssuesManager.class);
//	public static void transactionsIssuesFill() throws SQLException {
//		Connection conn = null;
//        try {
//            conn = getConnection();
//            TransactionsIssuesDAO.transactionsIssuesFill(conn);
//        } finally {
//            closeConnection(conn);
//        }
//	}
//
//	public static void transactionsIssuesCycleFill() throws SQLException {
//		Connection conn = null;
//        try {
//            conn = getConnection();
//            TransactionsIssuesDAO.transactionsIssuesCycleFill(conn);
//        } finally {
//            closeConnection(conn);
//        }
//	}
//
//    public static ArrayList<UserForFirstDepositEmail> getPastFiveMinutesTransactionsUsers() throws SQLException {
//    	Connection con = null;
//    	ArrayList<UserForFirstDepositEmail> users = null;
//    	try {
//    		con = getConnection();
//    		users = TransactionsIssuesDAO.getPastFiveMinutesTransactionsUsers(con);
//    		TransactionsIssuesDAO.updateLastRun(con);
//    	} catch (SQLException e) {
//			logger.error("Exception on Insert/update date value " + e);
//    	} finally {
//    		closeConnection(con);
//    	}
//    	return users;
//    }
//}