package il.co.etrader.service.transactionsIssues;

public interface TransactionsIssuesServiceMBean {
    public void startService();
    public void stopService();

	/**
	 * @return the emailPass
	 */
	public String getEmailPass();

	/**
	 * @param emailPass the emailPass to set
	 */
	public void setEmailPass(String emailPass);

	/**
	 * @return the emailFrom
	 */
	public String getEmailFrom();

	/**
	 * @param emailFrom the emailFrom to set
	 */
	public void setEmailFrom(String emailFrom);

	/**
	 * @return the emailUser
	 */
	public String getEmailUser();

	/**
	 * @param emailUser the emailUser to set
	 */
	public void setEmailUser(String emailUser);

	/**
	 * @return the emailServer
	 */
	public String getEmailServer();

	/**
	 * @param emailServer the emailServer to set
	 */
	public void setEmailServer(String emailServer);

	/**
	 * @return the emailSubject
	 */
	public String getEmailSubject();

	/**
	 * @param emailSubject the emailSubject to set
	 */
	public void setEmailSubject(String emailSubject);

	/**
	 * @return the emailToEtrader
	 */
	public String getEmailToEtrader();

	/**
	 * @param emailToEtrader the emailToEtrader to set
	 */
	public void setEmailToEtrader(String emailToEtrader);
	/**
	 * @return the emailToAnyoption
	 */
	public String getEmailToAnyoption();

	/**
	 * @param emailToAnyoption the emailToAnyoption to set
	 */
	public void setEmailToAnyoption(String emailToAnyoption);

	public String getEmailToAnyoptionZh();

	/**
	 * @param emailToAnyoptionZh the emailToAnyoptionZh to set
	 */
	public void setEmailToAnyoptionZh(String emailToAnyoption);
}