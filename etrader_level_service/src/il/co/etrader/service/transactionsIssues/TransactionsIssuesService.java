//package il.co.etrader.service.transactionsIssues;
//
//import org.apache.log4j.Logger;
//
//public class TransactionsIssuesService implements TransactionsIssuesServiceMBean {
//    private static Logger log = Logger.getLogger(TransactionsIssuesService.class);
//
//    private String emailUser;
//    private String emailPass;
//    private String emailFrom;
//    private String emailServer;
//    private String emailSubject;
//    private String emailToEtrader;
//    private String emailToAnyoption;
//    private String emailToAnyoptionZh;
//    private TransactionsIssuesUpdater updater;
//
//    public void startService() {
//        log.debug("TransactionsIssues.startService begin");
//        updater = new TransactionsIssuesUpdater(emailUser, emailPass, emailFrom, emailServer, emailSubject,
//        		emailToEtrader, emailToAnyoption, emailToAnyoptionZh);
//        updater.start();
//        log.info("TransactionsIssuesService started");
//    }
//
//    public void stopService() {
//        log.debug("TransactionsIssues.stopService begin");
//        if (null != updater) {
//            updater.stopTransactionIssuesUpdater();
//            updater = null;
//        }
//        log.info("TransactionsIssuesService stopped");
//    }
//
//	/**
//	 * @return the emailPass
//	 */
//	public String getEmailPass() {
//		return emailPass;
//	}
//
//	/**
//	 * @param emailPass the emailPass to set
//	 */
//	public void setEmailPass(String emailPass) {
//		this.emailPass = emailPass;
//	}
//
//	/**
//	 * @return the emailRcpt
//	 */
//	public String getEmailFrom() {
//		return emailFrom;
//	}
//
//	/**
//	 * @param emailRcpt the emailRcpt to set
//	 */
//	public void setEmailFrom(String emailFrom) {
//		this.emailFrom = emailFrom;
//	}
//
//	/**
//	 * @return the emailServer
//	 */
//	public String getEmailServer() {
//		return emailServer;
//	}
//
//	/**
//	 * @param emailServer the emailServer to set
//	 */
//	public void setEmailServer(String emailServer) {
//		this.emailServer = emailServer;
//	}
//
//	/**
//	 * @return the emailSubject
//	 */
//	public String getEmailSubject() {
//		return emailSubject;
//	}
//
//	/**
//	 * @param emailSubject the emailSubject to set
//	 */
//	public void setEmailSubject(String emailSubject) {
//		this.emailSubject = emailSubject;
//	}
//
//	/**
//	 * @return the emailUser
//	 */
//	public String getEmailUser() {
//		return emailUser;
//	}
//
//	/**
//	 * @param emailUser the emailUser to set
//	 */
//	public void setEmailUser(String emailUser) {
//		this.emailUser = emailUser;
//	}
//
//	/**
//	 * @return the emailToEtrader
//	 */
//	public String getEmailToEtrader() {
//		return emailToEtrader;
//	}
//
//	/**
//	 * @param emailToEtrader the emailToEtrader to set
//	 */
//	public void setEmailToEtrader(String emailToEtrader) {
//		this.emailToEtrader = emailToEtrader;
//	}
//
//	/**
//	 * @return the emailToAnyoption
//	 */
//	public String getEmailToAnyoption() {
//		return emailToAnyoption;
//	}
//
//	/**
//	 * @param emailToAnyoption the emailToAnyoption to set
//	 */
//	public void setEmailToAnyoption(String emailToAnyoption) {
//		this.emailToAnyoption = emailToAnyoption;
//	}
//
//	/**
//	 * @return the emailToAnyoptionZh
//	 */
//	public String getEmailToAnyoptionZh() {
//		return emailToAnyoptionZh;
//	}
//
//	/**
//	 * @param emailToAnyoptionZh the emailToAnyoptionZh to set
//	 */
//	public void setEmailToAnyoptionZh(String emailToAnyoptionZh) {
//		this.emailToAnyoptionZh = emailToAnyoptionZh;
//	}
//}