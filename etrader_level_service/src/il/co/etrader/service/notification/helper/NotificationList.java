package il.co.etrader.service.notification.helper;

import java.util.ArrayList;

import com.anyoption.common.beans.Notification;

import il.co.etrader.service.notification.NotificationService;

/**
 * @author EyalG
 *
 */
public class NotificationList {
	private String ids;
	private ArrayList<Notification> notifications;
	private int maxRetries;           // max retires for sumbiting the message to the provider
	private boolean workerHandle;     // true if there is some worker that handle the message
    private String url; // url to send the json
    private String dontTryIds;
    private String sendAlertIds;
    private String apiUser;

	public NotificationList(String url, String apiUser) {
		notifications = new ArrayList<Notification>();
		this.url = url;
		this.apiUser = apiUser;
	}

	/**
	 * @return the ids
	 */
	public String getIds() {
		return ids;
	}

	/**
	 * @param ids the ids to set
	 */
	public void setIds(String ids) {
		this.ids = ids;
	}

	/**
	 * @return the maxRetries
	 */
	public int getMaxRetries() {
		return maxRetries;
	}

	/**
	 * @param maxRetries the maxRetries to set
	 */
	public void setMaxRetries(int maxRetries) {
		this.maxRetries = maxRetries;
	}

	/**
	 * @return the notifications
	 */
	public ArrayList<Notification> getNotifications() {
		return notifications;
	}

	/**
	 * @param notifications the notifications to set
	 */
	public void setNotifications(ArrayList<Notification> notifications) {
		this.notifications = notifications;
	}

	/**
	 * @return the workerHandle
	 */
	public boolean isWorkerHandle() {
		return workerHandle;
	}

	/**
	 * @param workerHandle the workerHandle to set
	 */
	public void setWorkerHandle(boolean workerHandle) {
		this.workerHandle = workerHandle;
	}

	public String getJsonNotification() {
		String token = "";
		StringBuilder json = new StringBuilder();
		json.append("{ notifications: [ ");
		for (Notification notification : notifications) {
			json.append(token + notification.getJsonNotfication());
			token = ",";
		}
		json.append(" ] }");
		return json.toString();
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	public void addNotification(Notification n) {
		if (n.getRetries() > NotificationService.maxRetries) {
			if (null == this.dontTryIds) {
				this.dontTryIds = String.valueOf(n.getId());
			} else {
				this.dontTryIds += "," + n.getId();
			}
		} else {
			this.notifications.add(n);
			if (null == this.ids) {
				this.ids = String.valueOf(n.getId());
			} else {
				this.ids += "," + n.getId();
			}
			if (n.getRetries() == NotificationService.firstRetriesRound) {
				if (null == this.sendAlertIds) {
					this.sendAlertIds = String.valueOf(n.getId());
				} else {
					this.sendAlertIds += "," + n.getId();
				}
			}
		}
	}

	public void addNotificationList(NotificationList n) {
		this.notifications.addAll(n.getNotifications());
		if (null == this.ids) {
			this.ids = n.getIds();
		} else {
			this.ids += "," + n.getIds();
		}
		if (null == this.dontTryIds) {
			this.dontTryIds = n.getDontTryIds();
		} else {
			this.dontTryIds += "," + n.getDontTryIds();
		}
		if (null == this.sendAlertIds) {
			this.sendAlertIds = n.getSendAlertIds();
		} else {
			this.sendAlertIds += "," + n.getSendAlertIds();
		}
	}

	/**
	 * @return the apiUser
	 */
	public String getApiUser() {
		return apiUser;
	}

	/**
	 * @param apiUser the apiUser to set
	 */
	public void setApiUser(String apiUser) {
		this.apiUser = apiUser;
	}

	/**
	 * @return the dontTryIds
	 */
	public String getDontTryIds() {
		return dontTryIds;
	}

	/**
	 * @param dontTryIds the dontTryIds to set
	 */
	public void setDontTryIds(String dontTryIds) {
		this.dontTryIds = dontTryIds;
	}

	/**
	 * @return the sendAlertIds
	 */
	public String getSendAlertIds() {
		return sendAlertIds;
	}

	/**
	 * @param sendAlertIds the sendAlertIds to set
	 */
	public void setSendAlertIds(String sendAlertIds) {
		this.sendAlertIds = sendAlertIds;
	}
}
