package il.co.etrader.service.notification.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import com.anyoption.common.beans.Notification;
import com.anyoption.common.daos.DAOBase;

import il.co.etrader.service.notification.NotificationService;
import il.co.etrader.service.notification.helper.NotificationList;

public class NotificationDAO extends DAOBase {
	private final static String SQL_2_MINUTES = "2/1440";
	private final static String SQL_7_MINUTES = "7/1440";

    public static HashMap<String, NotificationList> loadMessages(Connection conn, int maxToLoad, String loadedList) throws SQLException {
    	HashMap<String, NotificationList> notificationListByApiUser = new HashMap<String, NotificationList>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "* " +
                    "FROM " +
                        "(" +
                        "SELECT " +
                        	"n.id, " +
                            "n.reference, " +
                            "n.notification || ', externalUserRef:' || eu.reference as notification, " +
                            "n.type_id, " +
                            "n.retries, " +
                            "u.json_url as url, " +
                            "u.user_name as api_user " +
                        "FROM " +
                            "api_notification n, " +
                            "api_external_users eu, " +
                            "api_users u " +
                        "WHERE " +
                            "n.status_id = " + Notification.STATUS_QUEUED + " AND " +
                            "( " +
                            	"n.time_sent IS NULL OR " +
                            	"( " +
                            		"n.retries < " + NotificationService.firstRetriesRound + " AND " +
                            		"n.time_sent + " + SQL_2_MINUTES + " <= sysdate " +
                            	") OR " +
                            	"( " +
	                        		"n.retries >= " + NotificationService.firstRetriesRound + " AND " +
	                        		"n.time_sent + " + SQL_7_MINUTES + " <= sysdate " +
                        		") " +
                            ") AND " +
                            "n.api_external_user_id = eu.id AND " +
                            "u.id = eu.api_user_id ";
	            if (null != loadedList) {
	            	sql += "AND n.id NOT IN (" + loadedList + ") ";
	            }

	            sql += "ORDER BY " +
	                       "u.user_name, " +
	                       "n.time_created " +
	                    ") " +
                   "WHERE " +
                       "rownum < ? ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, maxToLoad);
            rs = pstmt.executeQuery();
            String lastApiUser = null;
            String apiUser = null;
            NotificationList notificationList = null;
            while (rs.next()) {
            	apiUser = rs.getString("api_user");
            	if (null == lastApiUser || !lastApiUser.equals(apiUser)) {
            		if (null != notificationList) {
            			notificationListByApiUser.put(lastApiUser, notificationList);
            		}
            		notificationList = new NotificationList(rs.getString("url"), apiUser);
            		lastApiUser = apiUser;
            	}
            	notificationList.addNotification(getJsonVO(rs));
            }
            if (null != notificationList) {
    			notificationListByApiUser.put(apiUser, notificationList);
    		}
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return notificationListByApiUser;
    }


    /**
     * only columns we need to send in the json
     * @param rs the Result Set
     * @return notification
     * @throws SQLException
     */
    private static Notification getJsonVO(ResultSet rs) throws SQLException {
    	Notification n = new Notification();
    	n.setId(rs.getLong("id"));
    	n.setReference(rs.getString("reference"));
    	n.setNotification(rs.getString("notification"));
    	n.setType(rs.getLong("type_id"));
    	n.setRetries(rs.getLong("retries"));
    	return n;
    }

    /**
     * Update notifications records by id
     * @param con db connection
     * @param notificationsIds the notifications ids that need to be updated
     * @param con db statusId the new status
     * @param con db comment the new comment to add
     * @throws SQLException
     */
    public static void updateNotificationsAfterSubmit(Connection con, String notificationsIds, long statusId, String comment) throws SQLException {

    	PreparedStatement pstmt = null;
    	try {
    		String sql =
    			"UPDATE " +
    				"api_notification " +
    			"SET " +
    				"retries = retries + 1, " +
    				"time_sent = sysdate, " +
    				"status_id = ?, " +
    				"comments = comments || ' | ' || ? " +
    			"WHERE " +
    				"id IN (" + notificationsIds + ")";

    		pstmt = con.prepareStatement(sql);
    		pstmt.setLong(1, statusId);
    		pstmt.setString(2, comment);
    		pstmt.executeUpdate();
    	} finally {
    		closeStatement(pstmt);
    	}
    }

    /**
     * Update message status
     * @param con
     * @param statusId
     * @param comment
     * @param smsId
     * @throws SQLException
     */
    public static void updateMessageStatus(Connection con, long statusId, String notificationsId, String comment) throws SQLException {

    	PreparedStatement pstmt = null;
    	try {
    		String sql =
    			"UPDATE " +
    				"api_notification " +
    			"SET " +
    				"status_id = ? ";
            if (null != comment) {
             sql += ", comments = comments || ' | ' || ? ";
            }
         sql += "WHERE " +
    				"id IN (" + notificationsId + ")";
    		pstmt = con.prepareStatement(sql);
    		pstmt.setLong(1, statusId);
            if (null != comment) {
                pstmt.setString(2, comment);
            }

    		pstmt.executeUpdate();
    	} finally {
    		closeStatement(pstmt);
    	}
    }
}