package il.co.etrader.service.notification;

import org.apache.log4j.Logger;

/**
 * @author EyalG
 *
 */
public class NotificationService implements NotificationServiceMBean {
    private static Logger log = Logger.getLogger(NotificationService.class);

    private int messagesBatch;
    private int workersCount;
    public static int maxRetries;
    public static int firstRetriesRound; //b4 this send every 2 min after this send alert and try every 7 min
    private NotificationServiceEngine engine;
    public static String emailServer;
    public static String emailFrom;
    public static String emailTo;
    public static String emailSubject;

    public int getMessagesBatch() {
        return messagesBatch;
    }

    public void setMessagesBatch(int messagesBatch) {
        this.messagesBatch = messagesBatch;
    }

    public int getWorkersCount() {
        return workersCount;
    }

    public void setWorkersCount(int workersCount) {
        this.workersCount = workersCount;
    }

    /**
	 * @return the maxRetries
	 */
	public int getMaxRetries() {
		return maxRetries;
	}

	/**
	 * @return the firstRetriesRound
	 */
	public int getFirstRetriesRound() {
		return firstRetriesRound;
	}

	/**
	 * @param firstRetriesRound the firstRetriesRound to set
	 */
	public void setFirstRetriesRound(int firstRetriesRound) {
		NotificationService.firstRetriesRound = firstRetriesRound;
	}

	/**
	 * @param maxRetries the maxRetries to set
	 */
	public void setMaxRetries(int maxRetries) {
		NotificationService.maxRetries = maxRetries;
	}

	/**
	 * @return the emailFrom
	 */
	public String getEmailFrom() {
		return emailFrom;
	}

	/**
	 * @param emailFrom the emailFrom to set
	 */
	public void setEmailFrom(String emailFrom) {
		NotificationService.emailFrom = emailFrom;
	}

	/**
	 * @return the emailServer
	 */
	public String getEmailServer() {
		return emailServer;
	}

	/**
	 * @param emailServer the emailServer to set
	 */
	public void setEmailServer(String emailServer) {
		NotificationService.emailServer = emailServer;
	}

	/**
	 * @return the emailSubject
	 */
	public String getEmailSubject() {
		return emailSubject;
	}

	/**
	 * @param emailSubject the emailSubject to set
	 */
	public void setEmailSubject(String emailSubject) {
		NotificationService.emailSubject = emailSubject;
	}

	/**
	 * @return the emailTo
	 */
	public String getEmailTo() {
		return emailTo;
	}

	/**
	 * @param emailTo the emailTo to set
	 */
	public void setEmailTo(String emailTo) {
		NotificationService.emailTo = emailTo;
	}

	public void startService() {
        log.debug("NotificationService.startService begin");
        if (null == engine) {
            engine = new NotificationServiceEngine(messagesBatch, workersCount, maxRetries);
            engine.start();
            log.info("NotificationService started");
        } else {
            log.warn("NotificationService already started. Doing nothing");
        }
    }

    public void stopService() {
        log.debug("NotificationService.stopService stop");
        if (null != engine) {
            engine.stopNotificationServiceEngine();
            engine = null;
            log.info("NotificationService stopped");
        } else {
            log.warn("No NotificationService started.");
        }
    }
}