package il.co.etrader.service.notification;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.util.Hashtable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Notification;
import com.anyoption.common.util.ConstantsBase;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import il.co.etrader.service.notification.helper.NotificationList;
import il.co.etrader.service.notification.managers.NotificationManager;
import il.co.etrader.util.CommonUtil;
/**
 * @author EyalG
 *
 */
public class NotificationWorker extends Thread {
    private static final Logger log = Logger.getLogger(NotificationWorker.class);

    private NotificationList notification;
    private boolean running;
    private boolean isBusy;
    private NotificationServiceEngine notificationServiceEngine;

    public NotificationWorker(NotificationServiceEngine notificationServiceEngine) {
    	log.info("NotificationWorker constractor.");
    	this.notificationServiceEngine = notificationServiceEngine;
    }

    public void run() {
        log.info("NotificationWorker started.");
        running = true;
        while (running) {
        	if (null != notification) {  // have notification to work on
        		log.debug("going to send notification. " + notification.getJsonNotification());
        		sendAndUpdate();
        		log.trace("notification sent.");

                try {
	        		if (null != notification.getDontTryIds()) {
	        			NotificationManager.updateNotificationsStatus(Notification.STATUS_FAILED, notification.getDontTryIds(), null);
	        		}
                } catch (Exception e) {
                    log.error("Error update dont retry " + notification.getDontTryIds(), e);
                }
                log.trace("updated dont retry ids");
        		try {
        			if (null != notification.getSendAlertIds()) {
        				log.debug("notification send alert to api user: " + notification.getApiUser() + " for ids: " + notification.getSendAlertIds());
        				sendEmail("api user: " + notification.getApiUser() + " notification url: " + notification.getUrl() + " notification ids: " + notification.getSendAlertIds());
        			}
        		} catch (Exception ie) {
    			    log.error("Can't send alert notification.", ie);
    			}

        		log.trace("notification updated in db.");

                notificationServiceEngine.removeMessage(notification.getApiUser());
        	}
            try {
            	 synchronized (this) {
                     log.debug("worker free.");
                     notification = null;
                     isBusy = false;
            		 wait();  // waits for notify() call from Engine
            	 }
            } catch (InterruptedException ie) {
                // do nothing
            }
        }
        log.info("SMSWorker done.");
    }

    public void stopWorker() {
        log.info("Stopping notificationWorker...");
        running = false;
        synchronized (this) {
            notify();
        }
    }

	/**
	 * @return the isBusy
	 */
	public boolean isBusy() {
        return isBusy;
	}

	/**
	 * @param isBusy the isBusy to set
	 */
	public void setBusy(boolean isBusy) {
        this.isBusy = isBusy;
	}

	/**
	 * send the json request to the external system
	 */
	public String sendAndUpdate() {
		String error = null;
		if (log.isDebugEnabled()) {
			log.log(Level.DEBUG, "JsonNotification: " + notification.getJsonNotification() + ",url: " + notification.getUrl());
		}

        String statusApprove = ConstantsBase.EMPTY_STRING;
        String statusFail    = ConstantsBase.EMPTY_STRING;
        String statusOther   = ConstantsBase.EMPTY_STRING;

		HttpURLConnection connection = null;
	    try {
		    URL u = new URL(notification.getUrl());
		    connection = (HttpURLConnection)u.openConnection();
    	    //Change request method to POST
		    connection.setRequestMethod("POST");
		    connection.setUseCaches(false);
		    connection.setDoInput(true);
		    connection.setDoOutput(true);
		    connection.setRequestProperty("Content-Length", "" + notification.getJsonNotification().length());
		    // To post JSON content, set the content type to application/json OR application/jsonrequest [Reference]
		    connection.setRequestProperty("Content-Type", "application/json");
	        connection.setRequestProperty("Cache-Control", "no-cache");
	        // Post your content
	        java.io.OutputStream stream = connection.getOutputStream();
	        stream.write(notification.getJsonNotification().getBytes());
            stream.flush();
            stream.close();
            log.log(Level.DEBUG, "resonse string = " + connection.getResponseCode());


////		      // Read the response
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuffer sb = new StringBuffer();
            String str = br.readLine();
            while (str != null) {
                sb.append(str);
	            str = br.readLine();
            }
            br.close();
            String responseString = sb.toString();
            log.log(Level.DEBUG, "resonse string = " + responseString);

//            Gson gson = new GsonBuilder().setExclusionStrategies(new JsonExclusionStrategy()).serializeNulls().create();
//            T result = gson.fromJson(br, clazz);
//            in.close();
            JsonElement jelement = new JsonParser().parse(responseString);
            log.log(Level.DEBUG, "resonse jsonObject " + jelement);
            JsonObject jobject = jelement.getAsJsonObject();
            log.log(Level.DEBUG, "resonse jobject " + jobject);
            JsonArray jarray = jobject.getAsJsonArray("notificationsResponse");
//            JsonObject jsonObject = new JsonObject();

//            JSONArray tsmresponse = (JSONArray) jsonObject.get("notificationsResponse");
            log.log(Level.DEBUG, "resonse jarray " + jarray);
//

            log.log(Level.DEBUG, "resonse jarray.size() " + jarray.size());
            for (int i = 0; i < jarray.size(); i++) {
                jobject = jarray.get(i).getAsJsonObject();
                int notficationId = jobject.get("id").getAsInt();
                int notificationStatus = jobject.get("status").getAsInt();

                log.info("Notification id:" +notficationId + "\n" +
                     "Notification status:" + notificationStatus + "\n");

                if (notficationId != 0) {
                    switch (notificationStatus) {
                        case (int)Notification.STATUS_SUCCEED:
                            statusApprove += notficationId + ",";
                            break;
                        case (int)Notification.STATUS_FAILED:
                            statusFail += notficationId + ",";
                            break;
                        default:
                            statusOther += notficationId + ",";
                            break;
                    }
                }
            }

            log.info("statusApprove:" + statusApprove + "\n" +
                        "statusFail:" + statusFail + "\n" +
                        "statusOther:" + statusOther + "\n");

            NotificationManager.updateNotificationsAfterSubmit(notification.getIds(), Notification.STATUS_QUEUED,
                    Notification.COMMENT_SUCCESS_SENT);

	    } catch (Exception ex) {
	    	if (log.isDebugEnabled()) {
				log.log(Level.DEBUG, "JsonNotification cant send notification ", ex);
			}
	    	error = ex.getMessage();
            try {
                NotificationManager.updateNotificationsAfterSubmit(notification.getIds(), Notification.STATUS_QUEUED,
                        Notification.COMMENT_FAILED_TO_SUBMIT + ex.getMessage());
            } catch (SQLException e) {
                log.debug("cant update retries after submit");
            }
	    } finally {
		    if (connection != null) {
		      connection.disconnect();
		    }
	    }

        if (!CommonUtil.isParameterEmptyOrNull(statusApprove)) {
            try {
                NotificationManager.updateNotificationsStatus(Notification.STATUS_SUCCEED, statusApprove.substring(0, statusApprove.length() - 1), Notification.COMMENT_SUCCESS_RECEIVED);
            } catch (SQLException e) {
                log.error("cant update status to success ids " + statusApprove);
            }
        }

        if (!CommonUtil.isParameterEmptyOrNull(statusFail)) {
            try {
                NotificationManager.updateNotificationsStatus(Notification.STATUS_QUEUED, statusFail.substring(0, statusFail.length() - 1), Notification.COMMENT_FAILED_RECEIVED);
            } catch (SQLException e) {
                log.error("cant update status to success ids " + statusFail);
            }
        }
	    if (log.isDebugEnabled()) {
			log.log(Level.DEBUG, "JsonNotification: " + notification.getJsonNotification() + ",url: " + notification.getUrl() + " doneee");
		}
	    return error;
	}

	/**
	 * @return the notification
	 */
	public NotificationList getNotification() {
		return notification;
	}

	/**
	 * @param notification the notification to set
	 */
	public void setNotification(NotificationList notification) {
		this.notification = notification;
	}

	private void sendEmail(String body) {
        // set the server properties
        Hashtable<String, String> serverProperties = new Hashtable<String, String>();
        serverProperties.put("url", NotificationService.emailServer);
//        if (emailServerAuth) {
//            serverProperties.put("auth", "true");
//            serverProperties.put("user", emailUser);
//            serverProperties.put("pass", emailPass);
//        }

        // Set the email properties
        Hashtable<String, String> emailProperties = new Hashtable<String, String>();
        emailProperties.put("subject", NotificationService.emailSubject);
        emailProperties.put("from", NotificationService.emailFrom);
        emailProperties.put("to", NotificationService.emailTo);
        emailProperties.put("body", body);

        CommonUtil.sendEmail(serverProperties, emailProperties, null);
    }
}