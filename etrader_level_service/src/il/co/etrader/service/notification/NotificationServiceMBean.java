package il.co.etrader.service.notification;

/**
 * @author EyalG
 *
 */
public interface NotificationServiceMBean {
    public void startService();
    public void stopService();

    public void setMessagesBatch(int messagesBatch);
    public int getMessagesBatch();

    public void setWorkersCount(int workersCount);
    public int getWorkersCount();

    public void setMaxRetries(int maxRetries);
    public int getMaxRetries();

    public void setFirstRetriesRound(int firstRetriesRound);
    public int getFirstRetriesRound();


	/**
	 * @return the emailFrom
	 */
	public String getEmailFrom();

	/**
	 * @param emailFrom the emailFrom to set
	 */
	public void setEmailFrom(String emailFrom);

	/**
	 * @return the emailServer
	 */
	public String getEmailServer();

	/**
	 * @param emailServer the emailServer to set
	 */
	public void setEmailServer(String emailServer);

	/**
	 * @return the emailSubject
	 */
	public String getEmailSubject();

	/**
	 * @param emailSubject the emailSubject to set
	 */
	public void setEmailSubject(String emailSubject);

	/**
	 * @return the emailTo
	 */
	public String getEmailTo();

	/**
	 * @param emailTo the emailTo to set
	 */
	public void setEmailTo(String emailTo);
}