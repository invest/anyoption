package il.co.etrader.service.notification;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import il.co.etrader.service.notification.helper.NotificationList;
import il.co.etrader.service.notification.managers.NotificationManager;

/**
 * @author EyalG
 *
 */
public class MessagesLoader extends Thread {
    private static final Logger log = Logger.getLogger(MessagesLoader.class);

    private HashMap<String, NotificationList> queue;
    private int messagesBatch;
    private boolean running;

    public MessagesLoader(HashMap<String, NotificationList> queue, int messagesBatch) {
        this.queue = queue;
        this.messagesBatch = messagesBatch;
    }

    public void run() {
        log.info("MessagesLoader started. messagesBatch: " + messagesBatch);
        Thread.currentThread().setName("MessagesLoader");
        running = true;
        while (running) {

        	HashMap<String, NotificationList> notificationList = null;
        	try {
        		int maxToLoad = this.messagesBatch - queue.size();
        		if (maxToLoad > 0) {
        			log.info("going to pull queued messages from db, maxToLoad: " + maxToLoad);
        			notificationList = NotificationManager.loadMessages(maxToLoad, getLoadedList());
        		}
			} catch (SQLException e) {
				log.error("Problem to pull messages from DB. ", e);
			}

			log.info("notificationList: " + (null != notificationList ? notificationList.size() : "null"));
			if (null != notificationList && notificationList.size() > 0) {  // have new messages
				synchronized (queue) {
					for (String apiUser : notificationList.keySet()) {
						if (queue.containsKey(apiUser)) {
							if (queue.get(apiUser).isWorkerHandle()) { //if we already working on this api user notifications (sending them)
								queue.put(apiUser + (new Date().getTime()), notificationList.get(apiUser)); //add the new notifications for this api user on different api user + time bcoz we cant have same key
							} else { //we can add the new notifications to the old notifications on the queue
								queue.get(apiUser).addNotificationList(notificationList.get(apiUser));
							}
						} else { //no notifications on the list for this api user
							queue.put(apiUser, notificationList.get(apiUser));
						}
					}
				}
			}
			log.debug("queue.size():" + queue.size());

            try {
                Thread.sleep(500);
            } catch (InterruptedException ie) {
                // do nothing
            }
        }
        log.info("MessagesLoader done.");
    }

    public void stopMessagesLoader() {
        log.info("Stopping MessagesLoader...");
        running = false;
    }

    /**
     * Return the loaded notifications Id's list
     * @return
     */
    private String getLoadedList() {
    	String list = null;
    	if (null != queue && queue.size() > 0) {
    		list = "";
    	}
    	for (NotificationList notificationList : queue.values()) {
    		list += notificationList.getIds() + ",";
    	}
    	if (null != list) {
    		list = list.substring(0, list.length()-1);
    	}
    	return list;
    }

}