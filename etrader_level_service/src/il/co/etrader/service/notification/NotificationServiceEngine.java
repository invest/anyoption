package il.co.etrader.service.notification;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import il.co.etrader.service.notification.helper.NotificationList;
import il.co.etrader.service.sms.managers.SMSManager;

/**
 * @author EyalG
 *
 */
public class NotificationServiceEngine extends Thread {
    private static final Logger log = Logger.getLogger(NotificationServiceEngine.class);

    private int messagesBatch;
    private int workersCount;
    private int maxRetries;
    private HashMap<String, NotificationList> queue;
    private List<NotificationWorker> workers;
    private MessagesLoader messageLoader;
    private boolean running;

    public NotificationServiceEngine(int messagesBatch, int workersCount, int maxRetries) {
        this.messagesBatch = messagesBatch;
        this.workersCount = workersCount;
        this.maxRetries = maxRetries;
        this.queue = new HashMap<String, NotificationList>();
    }

    public void run() {
        log.info("NotificationServiceEngine started.");
        Thread.currentThread().setName("NotificationServiceEngine");

        // starting messageLoader Thread
        messageLoader = new MessagesLoader(queue, messagesBatch);
        messageLoader.start();
        // starting workers Threads
        log.info("starting workers Threads " + workersCount);
        workers = new ArrayList<NotificationWorker>();
        try {
			for (int i = 1; i <= workersCount; i++) {
				log.info("starting worker Thread " + i);
				NotificationWorker w = new NotificationWorker(this);
				log.info("starting worker Thread " + i);
				w.setName("notification-worker-" + i);
				log.info("starting worker Thread " + i);
				w.start();
				log.info("starting worker Thread " + i);
				workers.add(w);
				log.info("starting worker Thread " + i + " end");
			}
			running = true;
		} catch (Exception e) {
			log.info("cant start workers Threads", e);
		}
        log.info("starting workers Threads end");

        while (running) {
            log.debug("NotificationServiceEngine queue.size(): " + queue.size());
            try {
                synchronized (queue) {
            		for (String apiUser : queue.keySet()) {
            			NotificationList apiUserNotifications = queue.get(apiUser);
                		if (!apiUserNotifications.isWorkerHandle()) {
        	        		log.debug("going to give to worker api user " + apiUser);
        	        		NotificationWorker w = getAvailableWorker();
        	        		if (null != w) {
        	        		    log.debug("got worker " + w.getName());
//        	        			msg.setMaxRetries(maxRetries);
        		        		synchronized (w) {
                                    w.setNotification(apiUserNotifications);
                                    w.setBusy(true);
        		        		    log.debug("notify worker " + w.getName());
        		        			w.notify();
        		                }
        		        		apiUserNotifications.setWorkerHandle(true);
        	        		} else {
        	        			log.debug("all workers are busy!");
        	        		}
                		} else {
                		    log.debug("notification in progress " + apiUserNotifications.getIds());
                		}
                	}
                }
            } catch (Exception e) {
                log.error("ERROR! problem with worker", e);
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException ie) {
                // do nothing
            }
        }
        SMSManager.closeSMSProviders();
        log.info("NotificationServiceEngine done.");
    }

    public void stopNotificationServiceEngine() {
        log.info("Stopping NotificationServiceEngine...");
        running = false;
        // stop workers threads
       	for (NotificationWorker w : workers) {
        	w.stopWorker();
       	}
       	messageLoader.stopMessagesLoader();
    }

    /**
     * Get available worker from workers threads list
     * @return
     */
    private NotificationWorker getAvailableWorker() {
    	for (NotificationWorker w : workers) {
    		if (!w.isBusy()) {
    			return w;
    		}
    	}
    	return null;
    }

    /**
     * Remove message from the queue
     * @param msg
     */
    public void removeMessage(String apiUser) {
    	synchronized (queue) {
    		queue.remove(apiUser);
    	}
    	log.debug("remove api user: " + apiUser);
    }

	/**
	 * @return the messagesBatch
	 */
	public int getMessagesBatch() {
		return messagesBatch;
	}

	/**
	 * @param messagesBatch the messagesBatch to set
	 */
	public void setMessagesBatch(int messagesBatch) {
		this.messagesBatch = messagesBatch;
	}

	/**
	 * @return the workersCount
	 */
	public int getWorkersCount() {
		return workersCount;
	}

	/**
	 * @param workersCount the workersCount to set
	 */
	public void setWorkersCount(int workersCount) {
		this.workersCount = workersCount;
	}

	/**
	 * @return the maxRetries
	 */
	public int getMaxRetries() {
		return maxRetries;
	}

	/**
	 * @param maxRetries the maxRetries to set
	 */
	public void setMaxRetries(int maxRetries) {
		this.maxRetries = maxRetries;
	}

}