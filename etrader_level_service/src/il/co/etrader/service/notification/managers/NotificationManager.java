package il.co.etrader.service.notification.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.service.notification.daos.NotificationDAO;
import il.co.etrader.service.notification.helper.NotificationList;

/**
 * @author EyalG
 *
 */
public class NotificationManager extends BaseBLManager {

	/**
	 * Load messages
	 * @param maxToLoad max number of messages to load
	 * @return
	 * @throws SQLException
	 */
    public static HashMap<String, NotificationList> loadMessages(int maxToLoad, String loadedList) throws SQLException {
    	HashMap<String, NotificationList> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = NotificationDAO.loadMessages(conn, maxToLoad, loadedList);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    /**
     * Update notifications records by id
     * @param notificationsIds the notifications ids that need to be updated
     * @throws SQLException
     */
    public static void updateNotificationsAfterSubmit(String notificationsIds, long statusId, String comment) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		NotificationDAO.updateNotificationsAfterSubmit(con, notificationsIds, statusId, comment);
    	} finally {
        	closeConnection(con);
        }
    }

    /**
     * Update Notifications status
     * @param statusId the new status
     * @param comment the comment to add
     * @param notifications Ids to update
     * @throws SQLException
     */
    public static void updateNotificationsStatus(long statusId, String notificationIds, String comment) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		NotificationDAO.updateMessageStatus(con, statusId, notificationIds, comment);
    	} finally {
        	closeConnection(con);
        }
    }
}