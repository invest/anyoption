package il.co.etrader.service.onlineDeposits;

public interface OnlineDepositsServiceMBean {
    public void startService();
    public void stopService();
}