package il.co.etrader.service.onlineDeposits;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;

import il.co.etrader.service.onlineDeposits.managers.OnlineDepositsManager;

/**
 * This updater pull online deposits 'authenticate' transactions - user redirected to the bank site e.g. and don't back to our site,
 * for example when he close the window.
 * and marked them as failed with population handle.
 *
 * @author Kobi
 *
 */
public class OnlineDepositsUpdater extends Thread {
    private static Logger log = Logger.getLogger(OnlineDepositsService.class);

    private boolean running;
    private static HashMap<Long, Transaction> pulledTransactions = new HashMap<Long, Transaction>();

    public void run() {
        try {
			OnlineDepositsManager.loadPopulatioHandlers();
		} catch (Exception e1) {
			log.fatal("Can't load population handlers." + e1);
			return;
		}
        Thread.currentThread().setName("OnlineDepositsUpdater");
        if (log.isInfoEnabled()) {
            log.info("OnlineDepositsUpdater started.");
        }
        running = true;

        while (running) {
            log.debug("Getting authenticate transactions.");
            try {
            	ArrayList<Transaction> transactions = null;
            	try {
            		transactions = OnlineDepositsManager.getAuthenticateTrx();
            	} catch (SQLException e) {
            		log.error("Error! problem to pull authenticate trx from DB," + e);
            	}

            	// fill relevant transactions
            	for (Transaction t : transactions) {
            		insertToPulledList(t);
            	}

            	try {
            		ArrayList<Long> needToRemoveList = new ArrayList<Long>();
            		for (Iterator<Long> iter = pulledTransactions.keySet().iterator() ; iter.hasNext();) {
            			Long id = iter.next();
            			Transaction t = pulledTransactions.get(id);
            			log.debug("going to update trx: " + t.getId());
            			OnlineDepositsManager.finishTransaction(t);
            			needToRemoveList.add(new Long(t.getId()));
            		}

            		// remove trx
            		for (Long id : needToRemoveList) {
            			pulledTransactions.remove(id);
            		}
            	} catch (Exception e) {
            		log.error("Failed to proceed update!", e);
            	}

            	synchronized (this) {
            		try {
            			wait(60 * 1000);
            		} catch (InterruptedException ie) {
            			// do nothing
            		}
            	}
            } catch (Throwable t) {
            	log.error("", t);
            }
        }
        log.warn("OnlineDepositsUpdater done.");
    }

    public void stopUpdater() {
        log.info("Stopping OnlineDepositsUpdater...");
        running = false;
        synchronized (this) {
            notify();
        }
    }

    /**
     * Insert into pulled transactions Hash
     * @param trxId transaction id for insert
     * @param timeCreated time created of the transation
     */
    public void insertToPulledList(Transaction t) {
    	if (null == pulledTransactions.get(new Long(t.getId()))) {
    		pulledTransactions.put(new Long(t.getId()), t);
    	}
    }

}