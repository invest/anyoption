/**
 *
 */
package il.co.etrader.service.onlineDeposits;

import org.apache.log4j.Logger;

/**
 * OnlineDeposits service
 *
 * @author Kobi
 *
 */
public class OnlineDepositsService implements OnlineDepositsServiceMBean {
    private static Logger log = Logger.getLogger(OnlineDepositsService.class);

    private OnlineDepositsUpdater updater;

    public void startService() {
        log.debug("OnlineDepositsService.startService begin");
        updater = new OnlineDepositsUpdater();
        updater.start();
        log.info("InatecService started");
    }

    public void stopService() {
        log.debug("OnlineDepositsService.stopService begin");
        if (null != updater) {
            updater.stopUpdater();
            updater = null;
        }
        log.info("OnlineDepositsService stopped");
    }

}
