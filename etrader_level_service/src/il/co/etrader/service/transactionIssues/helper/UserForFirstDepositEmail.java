//package il.co.etrader.service.transactionIssues.helper;
//
//
//import java.io.Serializable;
//import java.text.DecimalFormat;
//import java.util.Date;
//import java.util.HashMap;
//
//import il.co.etrader.util.CommonUtil;
//
//public class UserForFirstDepositEmail implements Serializable {
//	/**
//	 *
//	 */
//	private static final long serialVersionUID = 1L;
//	private long userId;
//	private String userName = "";
//	private long skinId;
//	private String mobilePhone = "";
//	private String landLinePhone= "";
//	private long firstDepAmount;
//	private Date firstDepositTimeCreated;
//	private long currencyId;
//	private HashMap<Long, String> skinName;
//	private HashMap<Long, String> currencySymbol;
//	private long countryCode;
//	private String countryName;
//
//	public UserForFirstDepositEmail() {
//		skinName = new HashMap<Long, String>();
//		skinName.put(new Long(1), "Etrader");
//		skinName.put(new Long(2), "AO-EN");
//		skinName.put(new Long(3), "AO-TR");
//		skinName.put(new Long(4), "AO-AR");
//		skinName.put(new Long(5), "AO-ES");
//		skinName.put(new Long(6), "ES-MI");
//		skinName.put(new Long(7), "EN-GR");
//		skinName.put(new Long(8), "AO-DE");
//		skinName.put(new Long(9), "AO-IT");
//		skinName.put(new Long(10), "AO-RU");
//		skinName.put(new Long(12), "AO-FR");
//		skinName.put(new Long(13), "EN-US");
//		skinName.put(new Long(14), "ES-US");
//		skinName.put(new Long(15), "AO-ZH");
//
//		currencySymbol = new HashMap<Long, String>();
//		currencySymbol.put(new Long(1), "ILS");
//		currencySymbol.put(new Long(2), "USD");
//		currencySymbol.put(new Long(3), "EUR");
//		currencySymbol.put(new Long(4), "GBP");
//		currencySymbol.put(new Long(5), "TRY");
//		currencySymbol.put(new Long(6), "RUB");
//		currencySymbol.put(new Long(7), "CNY");
//	}
//
//	public String toString() {
//		final String TAB = " <br/> ";
//		if(CommonUtil.isParameterEmptyOrNull(mobilePhone)) {
//			mobilePhone = "none";
//		}
//
//		if(CommonUtil.isParameterEmptyOrNull(landLinePhone)) {
//			landLinePhone = "none";
//		}
//
//		if(CommonUtil.isParameterEmptyOrNull(userName)) {
//			userName = "none";
//		}
//		String retValue = "";
//		retValue = "id = " + this.userId + TAB
//	        + "userName = " + this.userName + TAB
//	        + "skin id = " + diaplaySkin(this.skinId) + TAB
//	        + "country = " + this.countryName + TAB
//	        + "mobile phone = " + this.countryCode + "-" + this.mobilePhone + TAB
//	        + "landLine phone = " + this.countryCode + "-" + this.landLinePhone + TAB
//	        + "first deposit amount = " + displayAmountAndCurrency(this.currencyId, this.firstDepAmount) + TAB
//	        + "first deposit time created = " + CommonUtil.getDateAndTimeFormat(this.firstDepositTimeCreated, "Israel")
//	        + " (Israel time)";
//		return retValue;
//	}
//
//	public String diaplaySkin(long id) {
//		return skinName.get(id);
//	}
//
//	public String displayAmountAndCurrency(long id, double amount) {
//		DecimalFormat sd = new DecimalFormat("###,###,##0.00");
//		double amountDecimal = amount;
//		amountDecimal /= 100;
//		String out = "";
//		out = currencySymbol.get(id) + " " + sd.format(amountDecimal);
//		return out;
//	}
//	/**
//	 * @return the firstDepAmount
//	 */
//	public long getFirstDepAmount() {
//		return firstDepAmount;
//	}
//	/**
//	 * @param firstDepAmount the firstDepAmount to set
//	 */
//	public void setFirstDepAmount(long firstDepAmount) {
//		this.firstDepAmount = firstDepAmount;
//	}
//	/**
//	 * @return the firstDepositTimeCreated
//	 */
//	public Date getFirstDepositTimeCreated() {
//		return firstDepositTimeCreated;
//	}
//	/**
//	 * @param firstDepositTimeCreated the firstDepositTimeCreated to set
//	 */
//	public void setFirstDepositTimeCreated(Date firstDepositTimeCreated) {
//		this.firstDepositTimeCreated = firstDepositTimeCreated;
//	}
//	/**
//	 * @return the landLinePhone
//	 */
//	public String getLandLinePhone() {
//		return landLinePhone;
//	}
//	/**
//	 * @param landLinePhone the landLinePhone to set
//	 */
//	public void setLandLinePhone(String landLinePhone) {
//		this.landLinePhone = landLinePhone;
//	}
//	/**
//	 * @return the mobilePhone
//	 */
//	public String getMobilePhone() {
//		return mobilePhone;
//	}
//	/**
//	 * @param mobilePhone the mobilePhone to set
//	 */
//	public void setMobilePhone(String mobilePhone) {
//		this.mobilePhone = mobilePhone;
//	}
//	/**
//	 * @return the skinId
//	 */
//	public long getSkinId() {
//		return skinId;
//	}
//	/**
//	 * @param skinId the skinId to set
//	 */
//	public void setSkinId(long skinId) {
//		this.skinId = skinId;
//	}
//	/**
//	 * @return the userId
//	 */
//	public long getUserId() {
//		return userId;
//	}
//	/**
//	 * @param userId the userId to set
//	 */
//	public void setUserId(long userId) {
//		this.userId = userId;
//	}
//	/**
//	 * @return the userName
//	 */
//	public String getUserName() {
//		return userName;
//	}
//	/**
//	 * @param userName the userName to set
//	 */
//	public void setUserName(String userName) {
//		this.userName = userName;
//	}
//	/**
//	 * @return the currencyId
//	 */
//	public long getCurrencyId() {
//		return currencyId;
//	}
//	/**
//	 * @param currencyId the currencyId to set
//	 */
//	public void setCurrencyId(long currencyId) {
//		this.currencyId = currencyId;
//	}
//	/**
//	 * @return the countryCode
//	 */
//	public long getCountryCode() {
//		return countryCode;
//	}
//
//	/**
//	 * @param countryCode the countryCode to set
//	 */
//	public void setCountryCode(long countryCode) {
//		this.countryCode = countryCode;
//	}
//	/**
//	 * @return the countryName
//	 */
//	public String getCountryName() {
//		return countryName;
//	}
//
//	/**
//	 * @param countryName the countryName to set
//	 */
//	public void setCountryName(String countryName) {
//		this.countryName = countryName;
//	}
//}
