package il.co.etrader.schedule.email;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jboss.varia.scheduler.Schedulable;

import il.co.etrader.schedule.email.managers.EmailAlertManager;

public class EmailScheduler implements Schedulable{
	
	Logger logger = Logger.getLogger(EmailScheduler.class);
	Random generator = new Random( System.currentTimeMillis() );
		
	private String propertiesFile;
	Properties scheduleProps;
	Properties mailServerProps;
	
	public EmailScheduler(String propertiesFile) {
		this.propertiesFile = propertiesFile;
		logger.debug("Loading properties.");
		scheduleProps = loadProperties();
		logger.debug("Create session.");
		mailServerProps = new Properties();
		mailServerProps.put("mail.smtp.host", scheduleProps.get("mail.smtp.host"));
	}
	
	@Override
	public void perform(Date now, long remainingRepetitions) {
		logger.info("Starting EmailScheduler: "+now);
		long start = System.currentTimeMillis();
		logger.debug("Gather alerts from DB.");
		/* 
		 * load the records with status PROCESS and change the status
		 * to random variable so another process can't process them
		 */
		Integer random = generator.nextInt();
		ArrayList<EmailAlert> alerts = null;
		try {
			alerts = EmailAlertManager.getEmailAlertsByStatus(EmailAlert.PROCESS, random);
			if (alerts.size() > 0) {
				logger.debug("Start sending "+ alerts.size()+" emails.");
				/*
				 * create session to smtp server 
				 */
				Session session = null;
		        if ("true".equals(scheduleProps.get("auth"))) {
		            session = Session.getInstance(mailServerProps, new Authenticator() {
		                    public PasswordAuthentication getPasswordAuthentication() {
		                        return new PasswordAuthentication((String)scheduleProps.get("user"), (String)scheduleProps.get("pass"));
		                    }
		                });
		        } else {
					/*
					 * create session to smtp server without login (relay is forbidden) 
					 */
		            session = Session.getInstance(mailServerProps);
		        }
				for(EmailAlert alert: alerts) {
					boolean sucess = sendEmailAlert(session, alert, scheduleProps);
					if(sucess) {
						logger.debug("Successfully sent email");
					} else {
						/*
						 * update status to ERROR with error message
						 */
						EmailAlertManager.updateEmailAlertErrorByID(alert.getAlertID(), alert.getAlertError(), EmailAlert.ERROR);	
					}
				}
				/*
				 * update status to PROCESSED.
				 */
				EmailAlertManager.updateEmailAlertsWithStatus(random, EmailAlert.PROCESSED);
			} else {
				logger.info("No pending alerts");
			}
		} catch (SQLException e) {
			logger.log(Level.FATAL, e.getMessage());
			return;
		}
		
		long end = System.currentTimeMillis();
		long time = (end - start) / 1000;
		logger.info("Done. Took :"+time + " seconds");
	}
	
	private boolean sendEmailAlert(Session session, EmailAlert alert, Properties props) {
		String[] to = alert.getAlertEmailRecipients().split(",");
        for(int i =0;i<to.length;i++) {
			Message mess = new MimeMessage(session);
	
			try {
				// Set Headers
				mess.setHeader("From", props.getProperty("DEFAULT_SENDER"));
				mess.setHeader("To", to[i]);
				mess.setHeader("Content-Type", props.getProperty("Content-Type"));
				mess.setHeader("Content-Transfer-Encoding", props.getProperty("Content-Transfer-Encoding"));
				mess.setFrom(new InternetAddress(props.getProperty("DEFAULT_SENDER")));
				mess.setSubject(alert.getAlertEmailTitle());
				mess.setContent(alert.getAlertEmailBody(), props.getProperty("Content-Type"));
	
				Transport.send(mess);
			} catch (AddressException ae) {
				logger.log(Level.FATAL, "Error Sending Mail to: " + to, ae);
				alert.setAlertError(ae.getMessage());
				alert.setAlertStatus(EmailAlert.ERROR);
				return false;
			} catch (MessagingException me) {
				logger.log(Level.FATAL, "Error Sending Mail to: " + to, me);
				alert.setAlertError(me.getMessage());
				alert.setAlertStatus(EmailAlert.ERROR);
				return false;
			}
        }
        return true;
	}
	
	private Properties loadProperties() {
		InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(propertiesFile);
		Properties props = new Properties();
		try {
			props.load(stream);
		} catch (Exception e) {
			logger.debug("PROPERTIES NOT FOUND");
			/* for test purposes
				props.put("Content-Type", "text/html; charset=UTF-8");
				props.put("Content-Transfer-Encoding", "base64");
				props.put("DEFAULT_SENDER", "mailer_daemon@anyoption.com");
	
				props.put("mail.smtp.host", "zimbra.etrader.co.il");
				props.put("url", "zimbra.etrader.co.il");
				props.put("auth", "true");
				props.put("user", "support@etrader.co.il");
				props.put("pass", "0okmnB");
			*/
		}
		return props;
	}
	
//	for test purposes
//	public static void main(String[] args) {
//		EmailScheduler.setupDS();
//		EmailScheduler es = new EmailScheduler("");
//		es.perform(null, 0);
//	}

//	public static void main(String[] args) throws Exception {
//		EmailScheduler.setupDS();
//		long start = System.currentTimeMillis();
//		
//		EmailAlertManager.insertTestEmailAlert(1000);
//		
//		long end = System.currentTimeMillis();
//		long time = (end - start)/1000;
//		System.out.println("Done. took: "+time+" seconds");
//	}

	//java:comp/env/jdbc/anyoptiondb
	/*
	public static void setupDS(){
		try {
            // Create initial context
            System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                "org.apache.naming.java.javaURLContextFactory");
            System.setProperty(Context.URL_PKG_PREFIXES, 
                "org.apache.naming");            
            InitialContext ic = new InitialContext();

            ic.createSubcontext("java:");
            ic.createSubcontext("java:comp");
            ic.createSubcontext("java:comp/env");
            ic.createSubcontext("java:comp/env/jdbc");
            
            // Construct DataSource
            OracleConnectionPoolDataSource ds = new OracleConnectionPoolDataSource();
            ds.setURL("jdbc:oracle:thin:@dbtestbg.anyoption.com:1521:etrader");
            ds.setUser("ETRADER");
            ds.setPassword("superman#1");
            
            ic.bind("java:comp/env/jdbc/anyoptiondb", ds);
        } catch (NamingException ex) {
        	ex.printStackTrace();
        } catch (SQLException e) {
			e.printStackTrace();
		}
        
	}
	*/
}
