package il.co.etrader.schedule.email;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;


/*
 * create table users_alerts_log (
 * "ID"                     NUMBER,
 * 
 * "USER_ID"				NUMBER(*,0),
 * "ALERT_TYPE"				VARCHAR2(100),
 * 
 * "ALERT_EMAIL_TITLE"		VARCHAR2(4000),
 * "ALERT_EMAIL_BODY"		VARCHAR2(4000),
 * "ALERT_EMAIL_RECEPIENTS"	VARCHAR2(4000),
 * 
 * "ALERT_STATUS"			NUMBER, -- -1 error, 0 for process, 1 processed
 * "ALERT_ERROR"			VARCHAR2(4000),
 * "ALERT_TIME"				timestamp default systimestamp -- last time accessed
 * ); 
 */
public class EmailAlert {
	
    public static final Integer ERROR		= -1;
    public static final Integer PROCESS		= 0;
    public static final Integer PROCESSED	= 1;
	
	private Integer			alertID;
	
	private String			alertEmailTitle;
	private	String			alertEmailBody;
	private	String			alertEmailRecipients;
	
	private	Integer			alertStatus;
	private	String			alertError;
	private	Timestamp		alertTime;
	
	 public static EmailAlert getInstance(ResultSet rs) throws SQLException {
		 
	        	EmailAlert email = new EmailAlert();
	            email.setAlertID(rs.getInt("ID"));
	            
	            email.setAlertEmailTitle(rs.getString("ALERT_EMAIL_TITLE"));
	            email.setAlertEmailBody(rs.getString("ALERT_EMAIL_BODY"));
	            email.setAlertEmailRecipients(rs.getString("ALERT_EMAIL_RECEPIENTS"));
	            
	            email.setAlertStatus(rs.getInt("ALERT_STATUS"));
	            return email;
	    }
	

	public Integer getAlertID() {
		return alertID;
	}


	public void setAlertID(Integer alertID) {
		this.alertID = alertID;
	}


	public String getAlertEmailTitle() {
		return alertEmailTitle;
	}

	public void setAlertEmailTitle(String alertEmailTitle) {
		this.alertEmailTitle = alertEmailTitle;
	}

	public String getAlertEmailBody() {
		return alertEmailBody;
	}

	public void setAlertEmailBody(String alertEmailBody) {
		this.alertEmailBody = alertEmailBody;
	}

	public String getAlertEmailRecipients() {
		return alertEmailRecipients;
	}

	public void setAlertEmailRecipients(String alertEmailRecipients) {
		this.alertEmailRecipients = alertEmailRecipients;
	}

	public Integer getAlertStatus() {
		return alertStatus;
	}

	public void setAlertStatus(Integer alertStatus) {
		this.alertStatus = alertStatus;
	}

	public String getAlertError() {
		return alertError;
	}

	public void setAlertError(String alertError) {
		this.alertError = alertError;
	}

	public Timestamp getAlertTime() {
		return alertTime;
	}

	public void setAlertTime(Timestamp alertTime) {
		this.alertTime = alertTime;
	}
	 
	 
}
