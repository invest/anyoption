package il.co.etrader.schedule.email.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.schedule.email.EmailAlert;

public class EmailAlertDAO extends DAOBase {

    public static ArrayList<EmailAlert> getEmailAlertsByStatus(Connection conn, Integer oldStatus, Integer newStatus) throws SQLException {
    	conn.setAutoCommit(false);
    	ArrayList<EmailAlert> alerts = new ArrayList<EmailAlert>();
    	PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
            		"SELECT " +
            			"id, " +
            			"alert_email_title, " +
            			"alert_email_body, " +
            			"alert_email_recepients, " +
            			"alert_status " +
            		"FROM " +
            			"users_alerts_log " +
            		"WHERE " +
            			"alert_status = ? " +
            		"FOR UPDATE";
            pstmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            pstmt.setInt(1, oldStatus);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                rs.updateInt(5, newStatus);
                rs.updateRow(); 
                
            	EmailAlert eal = EmailAlert.getInstance(rs);
            	eal.setAlertStatus(newStatus);
            	alerts.add(eal);
            }
            conn.commit();
        } catch(SQLException ex) {
        	conn.rollback();
        	throw(ex);
        } finally {
        	conn.setAutoCommit(true);
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return alerts;
    }
    
    public static boolean updateEmailAlertsWithStatus(Connection conn, Integer oldStatus, Integer newStatus) throws SQLException {
    	PreparedStatement pstmt = null;
    	try {
    		String sql =
    				"UPDATE " +
    					"users_alerts_log set " +
    					"alert_status = ?, " +
    					"alert_time = sysdate " +
    				"WHERE " +
    					"alert_status = ? ";

    		pstmt = conn.prepareStatement(sql);
    		pstmt.setInt(1, newStatus);
    		pstmt.setInt(2, oldStatus);
    		int rez = pstmt.executeUpdate();
    		return rez > 0;
    	} finally {
    		closeStatement(pstmt);
    	}
    }
    
    public static boolean updateEmailAlertErrorByID(Connection conn, Integer emailAlertID, String msg, Integer newStatus) throws SQLException {
    	PreparedStatement pstmt = null;
    	try {
    		String sql =
    				"UPDATE " +
    					"users_alerts_log set " +
    					"alert_status = ?, " +
    					"alert_error = ?, " +
    					"alert_time = sysdate " +
    				"WHERE" +
    					"ID = ? ";
    				

    		pstmt = conn.prepareStatement(sql);
    		pstmt.setInt(1, newStatus);
    		pstmt.setString(2, msg);
    		pstmt.setInt(3, emailAlertID);
    		int rez = pstmt.executeUpdate();
    		return rez > 0;
    	} finally {
    		closeStatement(pstmt);
    	}
    }
}

