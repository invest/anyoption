package il.co.etrader.schedule.email.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.schedule.email.EmailAlert;
import il.co.etrader.schedule.email.daos.EmailAlertDAO;

public class EmailAlertManager extends BaseBLManager {
	
    public static ArrayList<EmailAlert> getEmailAlertsByStatus(Integer oldStatus, Integer newStatus) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return EmailAlertDAO.getEmailAlertsByStatus(conn, oldStatus, newStatus);
        } finally {
            closeConnection(conn);
        }
    }
    
    public static boolean updateEmailAlertsWithStatus(Integer oldStatus, Integer newStatus) throws SQLException {
    	Connection conn = null;
    	try{
    		conn = getConnection();
    		return EmailAlertDAO.updateEmailAlertsWithStatus(conn, oldStatus, newStatus);
    	} finally {
    		closeConnection(conn);
    	}
    }
    
    public static boolean updateEmailAlertErrorByID( Integer emailAlertID, String msg, Integer newStatus) throws SQLException {
    	Connection conn = null;
    	try{
    		conn = getConnection();
    		return EmailAlertDAO.updateEmailAlertErrorByID(conn, emailAlertID, msg, newStatus);
    	} finally {
    		closeConnection(conn);
    	}
    }
    /*
    public static void insertTestEmailAlert(int count) throws SQLException{
    	Connection conn = null;
    	PreparedStatement opstmt = null;
    	try{
    		conn = getConnection();
            String sql =
           		 "insert into users_alerts_log1 ( ID, user_id, alert_type, alert_email_title, alert_email_body, alert_email_recepients, alert_status, alert_error, alert_time) values"+ 
           		 "((select max(ID)+1 from users_alerts_log1), 0,'test', 'test title', 'test body', 'pavlinsm@anyoption.com', 0, '', sysdate)";
        	opstmt =  conn.prepareStatement(sql);
            for(int i=0;i<count;i++){
            	opstmt.executeUpdate();
            }
        } finally {
        	 if (opstmt != null) {
                 try {
                	 opstmt.close();
                 } catch (SQLException ex) {}
             }
    		closeConnection(conn);
        }
    }
    */
}
