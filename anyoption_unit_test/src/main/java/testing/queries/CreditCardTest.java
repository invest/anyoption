package testing.queries;

import java.sql.Connection;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import utils.Config;
import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.daos.CreditCardsDAOBase;
import com.anyoption.common.util.OracleUtil;

/**
 * @author LioR SoLoMoN
 *
 */
public class CreditCardTest {
	private static final Logger logger = Logger.getLogger(CreditCardTest.class);
	
	public static int updateWebDetails(CreditCard card) {			
		Connection connection = null;
		try {
			connection = OracleUtil.getConnection(Config.DB_URL, Config.DB_USER, Config.DB_PASS);
			CreditCardsDAOBase.updateWebDetails(connection, card);
			return 1;
		} catch (SQLException e) {
			logger.error("ERROR! ", e);
		} finally {
			OracleUtil.closeConnection(connection);
		}
		return 0;
	}
	
	public static int update(CreditCard card) {	
		Connection connection = null;		
		try {
			connection = OracleUtil.getConnection(Config.DB_URL, Config.DB_USER, Config.DB_PASS);
//			CreditCardsDAOBase.update(connection, card);
			return 1;
		} catch (SQLException e) {
			logger.error("ERROR! ", e);
		} finally {
			OracleUtil.closeConnection(connection);
		}
		return 0;
	}
}