package testing.queries;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.daos.TransactionsDAOBase;
import com.anyoption.common.daos.UsersDAOBase;
import com.anyoption.common.payments.CancelDeposit;
import com.anyoption.common.payments.CaptureWithdrawal;
import com.anyoption.common.util.OracleUtil;
import utils.Config;

/**
 * @author LioR SoLoMoN
 *
 */
public class TransactionsTest {
	private static final Logger logger = Logger.getLogger(TransactionsTest.class);

	/**
	 * 
	 * @param cancelDeposit - TransactionId(), Status(), WriterId());
	 * 
	 * @return
	 */
	public static int updateStatusId(CancelDeposit cancelDeposit) {			
		Connection connection = null;
		try {
			connection = OracleUtil.getConnection(Config.DB_URL, Config.DB_USER, Config.DB_PASS);
			TransactionsDAOBase.updateStatusId(connection, cancelDeposit.getClearingInfo().getTransactionId(), cancelDeposit.getStatus(), true, cancelDeposit.getWriterId());	
			return 1;
		} catch (SQLException e) {
			logger.error("ERROR! ", e);
		} finally {
			OracleUtil.closeConnection(connection);
		}
		return 0;
	}
	
	/**
	 * @param ClearingInfo - isSuccessful, getProviderTransactionId, getTransactionId
	 * @param userUtcOffset
	 * @return
	 */
	public static int updateTransactionCapture(ClearingInfo info, String userUtcOffset) {			
		Connection connection = null;
		try {
			connection = OracleUtil.getConnection(Config.DB_URL, Config.DB_USER, Config.DB_PASS);
			TransactionsDAOBase.updateTransactionCapture(connection, info, userUtcOffset);	
			return 1;
		} catch (SQLException e) {
			logger.error("ERROR! ", e);
		} finally {
			OracleUtil.closeConnection(connection);
		}
		return 0;
	}
	
	/**
	 * @param userId
	 * @param ccId
	 * @param clearingProviderId
	 * @param isNot
	 * @return
	 */
	public static ArrayList<Transaction> getDepositTrxForCredit(long userId, long ccId, String clearingProviderId, boolean isNot) {			
		Connection connection = null;
		try {
			connection = OracleUtil.getConnection(Config.DB_URL, Config.DB_USER, Config.DB_PASS);
			return TransactionsDAOBase.getDepositTrxForCredit(connection, userId, ccId, clearingProviderId, isNot);			
		} catch (SQLException e) {
			logger.error("ERROR! ", e);
		} finally {
			OracleUtil.closeConnection(connection);
		}
		return null;
	}
	
	/**
	 * @param captureWithdrawal
	 * @param userId 
	 * @return
	 */
	public static int updateTransactionAfterCaptureWithdrawal(CaptureWithdrawal captureWithdrawal, long userId) {			
		Connection connection = null;
		try {
			connection = OracleUtil.getConnection(Config.DB_URL, Config.DB_USER, Config.DB_PASS);
			captureWithdrawal.setUser(UsersDAOBase.getUserById(connection, userId));
			TransactionsDAOBase.updateTransactionAfterCaptureWithdrawal(connection, captureWithdrawal);	
			return 1;
		} catch (SQLException e) {
			logger.error("ERROR! ", e);
		} finally {
			OracleUtil.closeConnection(connection);
		}
		return 0;
	}
}
