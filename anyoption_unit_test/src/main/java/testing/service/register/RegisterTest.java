package testing.service.register;

import org.apache.log4j.Logger;
import utils.HttpClientPost;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.requests.InsertUserMethodRequest;

/**
 * @author liors
 *
 */
public class RegisterTest {
	private static final Logger logger = Logger.getLogger(RegisterTest.class);
	
	public static MethodResult insertUser(String serviceUrl, InsertUserMethodRequest umr) {			
		logger.debug("about to call serviceURL = " + serviceUrl);
		MethodResult result = (MethodResult)HttpClientPost.doPost(serviceUrl, umr, new MethodResult());
        logger.info(result);
		return result;
	}
}
