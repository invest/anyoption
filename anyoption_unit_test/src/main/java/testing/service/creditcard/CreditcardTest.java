package testing.service.creditcard;

import org.apache.log4j.Logger;
import utils.HttpClientPost;
import com.anyoption.json.requests.CardMethodRequest;
import com.anyoption.json.results.CardMethodResult;

/**
 * @author EyalG
 *
 */
public class CreditcardTest {
	private static final Logger logger = Logger.getLogger(CreditcardTest.class);
	
	public static CardMethodResult insertCreditcard(String serviceUrl, CardMethodRequest umr) {			
		logger.debug("about to call serviceURL = " + serviceUrl);
		CardMethodResult result = (CardMethodResult)HttpClientPost.doPost(serviceUrl, umr, new CardMethodResult());
        logger.info(result);
		return result;
	}
	
	public static CardMethodResult updateCreditcard(String serviceUrl, CardMethodRequest umr) {		
		logger.debug("about to call serviceURL = " + serviceUrl);
		CardMethodResult result = (CardMethodResult)HttpClientPost.doPost(serviceUrl, umr, new CardMethodResult());
        logger.info(result);
		return result;
	}
}
