package testing.service.payment.deposit;

import org.apache.log4j.Logger;
import utils.HttpClientPost;
import com.anyoption.json.requests.CcDepositMethodRequest;
import com.anyoption.json.results.TransactionMethodResult;

/**
 * @author EyalG
 *
 */
public class DepositTest {
	private static final Logger logger = Logger.getLogger(DepositTest.class);
	
	public static TransactionMethodResult CreditcardDeposit(String serviceUrl, CcDepositMethodRequest umr) {			
		logger.debug("about to call serviceURL = " + serviceUrl);
		TransactionMethodResult result = (TransactionMethodResult)HttpClientPost.doPost(serviceUrl, umr, new TransactionMethodResult());
        logger.info(result);
		return result;
	}
}
