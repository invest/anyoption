package testing.service.checkForUpdate;

import org.apache.log4j.Logger;
import utils.HttpClientPost;
import com.anyoption.json.requests.CheckUpdateMethodRequest;
import com.anyoption.json.results.UpdateMethodResult;

/**
 * @author EyalG
 *
 */
public class CheckForUpdateTest {
	private static final Logger logger = Logger.getLogger(CheckForUpdateTest.class);
	
	public static UpdateMethodResult checkForUpdate(String serviceUrl, CheckUpdateMethodRequest umr) {			
		logger.debug("about to call serviceURL = " + serviceUrl);
		UpdateMethodResult result = (UpdateMethodResult)HttpClientPost.doPost(serviceUrl, umr, new UpdateMethodResult());
        logger.info(result);
		return result;
	}
}
