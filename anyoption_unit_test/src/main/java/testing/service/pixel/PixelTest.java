package testing.service.pixel;

import org.apache.log4j.Logger;

import utils.HttpClientPost;

import com.anyoption.json.requests.PixelMethodRequest;
import com.anyoption.json.results.PixelMethodResult;

public class PixelTest {
	private static final Logger logger = Logger.getLogger(PixelTest.class);
	
	public static PixelMethodResult getTrackingPixel(String serviceUrl, PixelMethodRequest pmr) {
		logger.info("about to call serviceURL = " + serviceUrl);
		PixelMethodResult result = (PixelMethodResult) HttpClientPost.doPost(serviceUrl, pmr, new PixelMethodResult());
		logger.info(result);
		return result;
	}
}
