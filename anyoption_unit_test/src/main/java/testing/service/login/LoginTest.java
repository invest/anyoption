package testing.service.login;

import org.apache.log4j.Logger;

import utils.HttpClientPost;

import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.UserMethodResult;

/**
 * @author eyalg
 *
 */
public class LoginTest {
	private static final Logger logger = Logger.getLogger(LoginTest.class);
	
	public static UserMethodResult getUser(String serviceUrl, UserMethodRequest umr) {			
		logger.debug("about to call serviceURL = " + serviceUrl);
		UserMethodResult result = (UserMethodResult)HttpClientPost.doPost(serviceUrl, umr, new UserMethodResult());
        logger.info(result);
		return result;
	}
}
