package testing.service.fire.server.pixel;

import org.apache.log4j.Logger;
import utils.HttpClientPost;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.requests.FireServerPixelMethodRequest;

/**
 * @author Eyal G
 *
 */
public class FireServerPixelTest {
	private static final Logger logger = Logger.getLogger(FireServerPixelTest.class);
	
	public static MethodResult firePixel(String serviceUrl, FireServerPixelMethodRequest umr) {			
		logger.debug("about to call serviceURL = " + serviceUrl);
		MethodResult result = (MethodResult)HttpClientPost.doPost(serviceUrl, umr, new MethodResult());
        logger.info(result);
		return result;
	}
}
