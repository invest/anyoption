package testing.service.payments.withdraw;

import org.apache.log4j.Logger;
import utils.HttpClientPost;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;

/**
 * @author liors
 *
 */
public class Withdraw {
	private static final Logger logger = Logger.getLogger(Withdraw.class);
	
	public static MethodResult isWithdrawalWeekendsAvailable(String serviceUrl, UserMethodRequest methodRequest) {			
		logger.info("about to call serviceURL = " + serviceUrl);
		MethodResult result = (MethodResult)HttpClientPost.doPost(serviceUrl, methodRequest, new MethodResult());
        logger.info(result);
		return result;
	}
}
