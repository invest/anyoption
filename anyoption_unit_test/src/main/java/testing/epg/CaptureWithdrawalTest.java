package testing.epg;

import il.co.etrader.bl_managers.ClearingManager;
import java.sql.Connection;
import java.util.TreeSet;
import org.apache.log4j.Logger;
import com.anyoption.common.util.OracleUtil;
import testing.queries.TransactionsTest;
import utils.Config;
import epg.jobs.EPGCaptureWithdrawalJobManager;
import epg.jobs.SummaryEPGCaptureWithdrawal;

/**
 * @author liors
 *
 */
public class CaptureWithdrawalTest {
	private static final Logger logger = Logger.getLogger(TransactionsTest.class);
	public static int CaptureWithdrawalDoJob(String transactionIds) {			
		Connection connection = null;
		try {
			connection = OracleUtil.getConnection(Config.DB_URL, Config.DB_USER, Config.DB_PASS);
			ClearingManager.loadClearingProviders(connection);
			TreeSet<SummaryEPGCaptureWithdrawal> summaryRecords = EPGCaptureWithdrawalJobManager.doJob(connection, transactionIds);
			for (SummaryEPGCaptureWithdrawal r : summaryRecords) {
				logger.info("transactionId: " + r.getTransactionId() + " "
						+ " epgTransactionId " + r.getEpgTransactionId());
			}
			return 1;
		} catch (Exception e) {
			logger.error("ERROR! CaptureWithdrawalTest ", e);
		} finally {
			OracleUtil.closeConnection(connection);
		}
		return 0;
	}
}
