package com.anyoption.test.manager;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.test.dao.OpportunitiesDAO;

public class OpportunitiesManager extends BaseManager {

	public static ArrayList<Opportunity> getOpenOpportunities() throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return OpportunitiesDAO.getOpenOpportunities(conn);
		} finally {
			closeConnection(conn);
		}
	}
	
}
