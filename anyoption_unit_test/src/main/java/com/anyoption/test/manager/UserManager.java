package com.anyoption.test.manager;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.test.dao.UserDAO;

public class UserManager extends BaseManager {
	
	public static boolean deleteUser(long userId) throws SQLException {
		Connection connection = null;
		try{
			connection = getConnection();
			return UserDAO.deleteUser(connection, userId);
		} finally {
			closeConnection(connection);
		}
	}
	public static long selectLastUserInvestment(long userId, long investmentTypeId) throws SQLException {
		Connection connection = null;
		try{
			connection = getConnection();
			return UserDAO.selectLastUserInvestment(connection, userId, investmentTypeId);
		} finally {
			closeConnection(connection);
		}
	}
	
	public static long selectLastUserTransaction(long userId, long transactionTypeId) throws SQLException {
		Connection connection = null;
		try{
			connection = getConnection();
			return UserDAO.selectLastUserTransaction(connection, userId, transactionTypeId);
		} finally {
			closeConnection(connection);
		}
	}
}
