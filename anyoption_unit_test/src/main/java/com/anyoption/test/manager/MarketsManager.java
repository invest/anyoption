package com.anyoption.test.manager;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.test.dao.MarketsDAO;

public class MarketsManager extends BaseManager {
	
	public static long getMarketRate(long marketId) throws SQLException {
		Connection connection = null;
		try {
			connection = getConnection();
			return MarketsDAO.getMarketLeveLByOppId(connection, marketId);
		} finally {
			closeConnection(connection);
		}
	}
}
