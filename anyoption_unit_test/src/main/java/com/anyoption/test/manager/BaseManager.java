package com.anyoption.test.manager;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import oracle.jdbc.pool.OracleConnectionPoolDataSource;

public class BaseManager {
	
	static private OracleConnectionPoolDataSource dataSource;
	static private String server;
	static private String serviceName;
	static private String userName;
	static private String password;
	
	public static void init(String server, String serviceName, String userName, String password) {
		BaseManager.server = server;
		BaseManager.serviceName = serviceName;
		BaseManager.userName = userName;
		BaseManager.password = password;
	}
	
	public static DataSource getDataSource() throws SQLException {
        // Construct DataSource
		if(dataSource == null) {
			dataSource = new OracleConnectionPoolDataSource();
			dataSource.setURL("jdbc:oracle:thin:@"+server+":1521/"+serviceName);
			dataSource.setUser(userName);
			dataSource.setPassword(password);
		}

		return dataSource;
	}
	
	public static Connection getConnection() throws SQLException {
		return getDataSource().getConnection();
	}

	public static void closeConnection(Connection connection) {
		try {
			if(connection != null){
				connection.close();
			}
		} catch ( Exception ex) {
			
		}
	}
}
