package com.anyoption.test.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DAOBase {
	
    public static void closeStatement(Statement stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {

            }
        }
    }

    public static void closeResultSet(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {

            }
        }
    }
}
