package com.anyoption.test.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.beans.Opportunity;

public class OpportunitiesDAO extends DAOBase {

	
    public static ArrayList<Opportunity> getOpenOpportunities(Connection conn) throws SQLException {
	    ArrayList<Opportunity> l = new ArrayList<Opportunity>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql = 
            		" SELECT " +
            					" o.id," +
            					" o.market_id, " +
            					" m.feed_name " +
       				" FROM " +
       							" opportunities o, " +
       							" markets m" +
            		" WHERE " +
			            		" CURRENT_TIMESTAMP > o.time_first_invest " + 
			            		" AND m.id = o.market_id " +
			            		" AND CURRENT_TIMESTAMP < o.time_last_invest " +
			            		" AND o.is_published = 1 " +
			            		" AND o.opportunity_type_id = 1 " + 
			            		" AND o.scheduled = 1 " +
			            		" AND o.is_disabled_service = 0 " + 
            		" ORDER BY " +
            					 " o.time_last_invest DESC ";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	Opportunity o = new Opportunity();
            	o.setMarketId(rs.getLong("market_id"));
            	o.setId(rs.getLong("id"));
            	o.setMarketName(rs.getString("feed_name"));
            	l.add(o);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
	}
}
