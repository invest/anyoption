package com.anyoption.test.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class MarketsDAO extends DAOBase {

	public static long getMarketLeveLByOppId(Connection connection, long marketId) throws SQLException {

	        PreparedStatement pstmt = null;
	        ResultSet rs = null;
	        try {
					String sql = 
							"SELECT " +
									"rate, graph_rate " + 
							"FROM " +
									"market_rates " + 
							"WHERE " +
									"market_id = ? " +
							"AND " +
									"rownum = 1 " + 
							"ORDER BY rate_time desc " ;
            pstmt = connection.prepareStatement(sql);
            pstmt.setLong(1, marketId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
            	return rs.getLong("graph_rate");
            }
            return -1;
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }

	}
}
