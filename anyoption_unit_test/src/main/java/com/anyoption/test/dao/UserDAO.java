package com.anyoption.test.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAO extends DAOBase {

	public static boolean deleteUser(Connection connection, long userId) throws SQLException {

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
				String sql = "DELETE FROM users where id = ? ";
	            pstmt = connection.prepareStatement(sql);
	            pstmt.setLong(1, userId);
	            int k = pstmt.executeUpdate();
				return k > 0;
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
	}
	
	public static long selectLastUserInvestment(Connection connection, long userId, long investmentTypeId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
				String sql = "SELECT id FROM investments WHERE user_id = ? AND type_id = ? AND rownum < 2 order by time_created desc";
	            pstmt = connection.prepareStatement(sql);
	            pstmt.setLong(1, userId);
	            pstmt.setLong(2, investmentTypeId);
	            rs = pstmt.executeQuery();
	            if (rs.next()) {
	            	return rs.getLong("graph_rate");
	            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return -1;
	}
	
	public static long selectLastUserTransaction(Connection connection, long userId, long transactionTypeId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
				String sql = "SELECT id FROM transactions WHERE user_id = ? AND type_id = ? AND rownum < 2 order by time_created desc";
	            pstmt = connection.prepareStatement(sql);
	            pstmt.setLong(1, userId);
	            pstmt.setLong(2, transactionTypeId);
	            rs = pstmt.executeQuery();
	            if (rs.next()) {
	            	return rs.getLong("graph_rate");
	            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
		return -1;
	}
}
