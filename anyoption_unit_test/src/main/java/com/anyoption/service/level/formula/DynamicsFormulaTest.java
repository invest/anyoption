package com.anyoption.service.level.formula;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.anyoption.common.beans.MarketConfig;
import com.anyoption.common.beans.MarketDynamicsQuoteParams;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.service.level.formula.DynamicsFormula.SpreadSide;
import com.anyoption.service.level.market.Market;
import com.anyoption.service.level.market.MarketDynamics;
import com.anyoption.service.level.market.UpdateResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class DynamicsFormulaTest {
	private static Market market;
	private static DynamicsFormula formula;
	private static Opportunity opportunity;

	@BeforeClass
	public static void init() {
		MarketConfig marketConfig = new MarketConfig();
		marketConfig.setHourLevelFormulaClass("com.anyoption.service.level.formula.DynamicsCurrencyFormula");
		marketConfig.setHourLevelFormulaParams("{\"internalFormulaClassName\": \"com.anyoption.service.level.formula.Last5UpdatesAvgOf2Formula\", \"formulaConfig\": {\"field1Name\": \"ASK\", \"field2Name\": \"BID\", \"subscriptionConfig\": {\"EUR=\": {\"identifier\": \"EUR=\"}}}}");
		marketConfig.setMarketParams("{\"dontCloseAfterXSecNoUpdate\": \"60\", \"timeZone\": \"GMT\", \"shouldBeClosedByMonitoring\": \"false\"}");
		try {
			market = new MarketDynamics(marketConfig, "EUR=.Dynamicssss", 719, 6);
		} catch (Exception e) {
			// do nothing for now
		}
//		formula = new DynamicsCurrencyFormula(
//				"{\"internalFormulaClassName\": \"com.anyoption.service.level.formula.Last5UpdatesAvgOf2Formula\", \"formulaConfig\": {\"field1Name\": \"ASK\", \"field2Name\": \"BID\", \"subscriptionConfig\": {\"EUR=\": {\"identifier\": \"EUR=\"}}}}",
//				FormulaRole.CURRENT_LEVEL);
		formula = (DynamicsFormula) market.getHourLevelFromula();
		
		opportunity = new Opportunity();
		opportunity.setQuoteParams("{\"sp\": \"0.08\", \"spMax\": \"0.15\", \"tU\": \"5\", \"tD\": \"0.2\", \"amountU\": \"5000\", \"amountD\": \"5000\", \"t\": \"0\"}");
		Gson gson = new GsonBuilder().serializeNulls().create();
		Object o = gson.fromJson(opportunity.getQuoteParams(), MarketDynamicsQuoteParams.class);
		opportunity.setQuoteParamsObject(o);
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			opportunity.setTimeFirstInvest(sdf.parse("2017-05-03 13:50"));
			opportunity.setTimeEstClosing(sdf.parse("2017-05-03 14:15"));
		} catch (Exception e) {
			// WTF?
		}
		opportunity.setCurrentLevel(1.6632);

		// Internal formula snapshot
		Map<String, Object> fields = new HashMap<String, Object>();
		fields.put("ASK", new Double(1.663));
		fields.put("BID", new Double(1.662));
		formula.dataUpdate("EUR=", fields, market, null, new UpdateResult());
		
		// Fill the observations
		formula.observations.clear(); // clear the snapshot update to avoid mess
		formula.observe(opportunity, 1493819340000L, 1.6645875);		
		formula.observe(opportunity, 1493819400000L, 1.664675);
		formula.observe(opportunity, 1493819460000L, 1.6646225000000001);
		formula.observe(opportunity, 1493819520000L, 1.6644999999999999);
		formula.observe(opportunity, 1493819580000L, 1.6642325);
		formula.observe(opportunity, 1493819640000L, 1.6643625);
		formula.observe(opportunity, 1493819700000L, 1.66433);
		formula.observe(opportunity, 1493819760000L, 1.6640875);
		formula.observe(opportunity, 1493819820000L, 1.6639749999999998);
		formula.observe(opportunity, 1493819880000l, 1.6641374999999998);
		formula.observe(opportunity, 1493819940000L, 1.664395);
		formula.observe(opportunity, 1493820000000L, 1.6644299999999999);
		formula.observe(opportunity, 1493820060000L, 1.66492);
		formula.observe(opportunity, 1493820120000L, 1.664305);
		formula.observe(opportunity, 1493820180000L, 1.6643750000000002);
		formula.observe(opportunity, 1493820240000L, 1.6643175000000001);
		formula.observe(opportunity, 1493820300000L, 1.6642625);
		formula.observe(opportunity, 1493820360000L, 1.6642000000000001);
		formula.observe(opportunity, 1493820420000L, 1.6635624999999998);
		formula.observe(opportunity, 1493820480000L, 1.6636925);
		formula.observe(opportunity, 1493820540000L, 1.6636325);
		formula.observe(opportunity, 1493820600000L, 1.6635425000000001);
		formula.observe(opportunity, 1493820660000L, 1.6636375); 
		formula.observe(opportunity, 1493820720000L, 1.6632);
		formula.observe(opportunity, 1493820780000L, 1.6634325);
	}
	
	@Test
	public void vol() {
		Double vol = Double.valueOf(formula.vol(2));
		assertEquals("0.0002848", String.format("%1$.7f", vol));
	}
	
	@Test
	public void volSkew() {
		Double volSkew = Double.valueOf(formula.volSkew(2, 1.6632));
		assertEquals("0.0001860", String.format("%1$.7f", volSkew));
	}
	
	@Test
	public void edC() {
		Double edC = Double.valueOf(formula.edC(1.6633125, 1.6632, 1.9666666, 0.0002996));
		assertEquals("0.5639", String.format("%1$.4f", edC));
	}
	
	@Test
	public void spreadBigPriceSmallSpreadAsk() {
		Double ask = Double.valueOf(formula.spreadPrice(0.5639, 0.5843, SpreadSide.ASK, 0.08, 0.15));
		assertEquals("0.6141", String.format("%1$.4f", ask));
	}
	
	@Test
	public void spreadBigPriceSmallSpreadBid() {
		Double bid = Double.valueOf(formula.spreadPrice(0.5639, 0.5843, SpreadSide.BID, 0.08, 0.15));
		assertEquals("0.5341", String.format("%1$.4f", bid));
	}
	
	@Test
	public void spreadBigPriceMidSpreadAsk() {
		Double ask = Double.valueOf(formula.spreadPrice(0.5639, 0.5843, SpreadSide.ASK, 0.02, 0.15));
		assertEquals("0.564", String.format("%1$.3f", ask));
	}
	
	@Test
	public void spreadBigPriceMidSpreadBid() {
		Double bid = Double.valueOf(formula.spreadPrice(0.5639, 0.5843, SpreadSide.BID, 0.02, 0.15));
		assertEquals("0.584", String.format("%1$.3f", bid));
	}
	
	@Test
	public void spreadBigPriceBigSpreadAsk() {
		Double ask = Double.valueOf(formula.spreadPrice(0.5639, 0.5843, SpreadSide.ASK, 0.01, 0.02));
		assertEquals("0.564", String.format("%1$.3f", ask));
	}
	
	@Test
	public void spreadBigPriceBigSpreadBid() {
		Double bid = Double.valueOf(formula.spreadPrice(0.5639, 0.5843, SpreadSide.BID, 0.01, 0.02));
		assertEquals("0.544", String.format("%1$.3f", bid));
	}
	
	@Test
	public void spreadSmallPriceSmallSpreadAsk() {
		Double ask = Double.valueOf(formula.spreadPrice(0.4639, 0.4843, SpreadSide.ASK, 0.08, 0.15));
		assertEquals("0.5141", String.format("%1$.4f", ask));
	}
	
	@Test
	public void spreadSmallPriceSmallSpreadBid() {
		Double bid = Double.valueOf(formula.spreadPrice(0.4639, 0.4843, SpreadSide.BID, 0.08, 0.15));
		assertEquals("0.4341", String.format("%1$.4f", bid));
	}
	
	@Test
	public void spreadSmallPriceMidSpreadAsk() {
		Double ask = Double.valueOf(formula.spreadPrice(0.4639, 0.4843, SpreadSide.ASK, 0.02, 0.15));
		assertEquals("0.485", String.format("%1$.3f", ask));
	}
	
	@Test
	public void spreadSmallPriceMidSpreadBid() {
		Double bid = Double.valueOf(formula.spreadPrice(0.4639, 0.4843, SpreadSide.BID, 0.02, 0.15));
		assertEquals("0.463", String.format("%1$.3f", bid));
	}
	
	@Test
	public void spreadSmallPriceBigSpreadAsk() {
		Double ask = Double.valueOf(formula.spreadPrice(0.4639, 0.4843, SpreadSide.ASK, 0.01, 0.02));
		assertEquals("0.483", String.format("%1$.3f", ask));
	}
	
	@Test
	public void spreadSmallPriceBigSpreadBid() {
		Double bid = Double.valueOf(formula.spreadPrice(0.4639, 0.4843, SpreadSide.BID, 0.01, 0.02));
		assertEquals("0.463", String.format("%1$.3f", bid));
	}
	
	@Test
	public void skewNoInvestmentsAsk() {
		Double ask = Double.valueOf(formula.skewPrice(0.5341, 0.6141, 0, 0, 5.0, 0.2, 5000, 5000, SpreadSide.ASK));
		assertEquals("0.6141", String.format("%1$.4f", ask));
	}
	
	@Test
	public void skewNoInvestmentsBid() {
		Double bid = Double.valueOf(formula.skewPrice(0.5341, 0.6141, 0, 0, 5.0, 0.2, 5000, 5000, SpreadSide.BID));
		assertEquals("0.5341", String.format("%1$.4f", bid));
	}

	@Test
	public void skewOnlyDownLargeAmountAsk() {
		Double ask = Double.valueOf(formula.skewPrice(0.5341, 0.6141, 0, 6000, 5.0, 0.2, 5000, 5000, SpreadSide.ASK));
		assertEquals("0.5741", String.format("%1$.4f", ask));
	}

	@Test
	public void skewOnlyDownLargeAmountBid() {
		Double bid = Double.valueOf(formula.skewPrice(0.5341, 0.6141, 0, 6000, 5.0, 0.2, 5000, 5000, SpreadSide.BID));
		assertEquals("0.4941", String.format("%1$.4f", bid));
	}

	@Test
	public void skewOnlyDownSmallAmountAsk() {
		Double ask = Double.valueOf(formula.skewPrice(0.5341, 0.6141, 0, 1000, 5.0, 0.2, 5000, 5000, SpreadSide.ASK));
		assertEquals("0.6061", String.format("%1$.4f", ask));
	}

	@Test
	public void skewOnlyDownSmallAmountBid() {
		Double bid = Double.valueOf(formula.skewPrice(0.5341, 0.6141, 0, 1000, 5.0, 0.2, 5000, 5000, SpreadSide.BID));
		assertEquals("0.5261", String.format("%1$.4f", bid));
	}

	@Test
	public void skewOnlyUpLargeAmountAsk() {
		Double ask = Double.valueOf(formula.skewPrice(0.5341, 0.6141, 6000, 0, 5.0, 0.2, 5000, 5000, SpreadSide.ASK));
		assertEquals("0.6541", String.format("%1$.4f", ask));
	}

	@Test
	public void skewOnlyUpLargeAmountBid() {
		Double bid = Double.valueOf(formula.skewPrice(0.5341, 0.6141, 6000, 0, 5.0, 0.2, 5000, 5000, SpreadSide.BID));
		assertEquals("0.5741", String.format("%1$.4f", bid));
	}

	@Test
	public void skewOnlyUpSmallAmountAsk() {
		Double ask = Double.valueOf(formula.skewPrice(0.5341, 0.6141, 1000, 0, 5.0, 0.2, 5000, 5000, SpreadSide.ASK));
		assertEquals("0.6221", String.format("%1$.4f", ask));
	}

	@Test
	public void skewOnlyUpSmallAmountBid() {
		Double bid = Double.valueOf(formula.skewPrice(0.5341, 0.6141, 1000, 0, 5.0, 0.2, 5000, 5000, SpreadSide.BID));
		assertEquals("0.5421", String.format("%1$.4f", bid));
	}
	
	@Test
	public void skewWayUpAsk() {
		Double ask = Double.valueOf(formula.skewPrice(0.5341, 0.6141, 5000, 1000, 5.0, 0.2, 5000, 5000, SpreadSide.ASK));
		assertEquals("0.6541", String.format("%1$.4f", ask));
	}
	
	@Test
	public void skewWayUpBid() {
		Double bid = Double.valueOf(formula.skewPrice(0.5341, 0.6141, 5000, 1000, 5.0, 0.2, 5000, 5000, SpreadSide.BID));
		assertEquals("0.5741", String.format("%1$.4f", bid));
	}
	
	@Test
	public void skewUpAsk() {
		Double ask = Double.valueOf(formula.skewPrice(0.5341, 0.6141, 1000, 1000, 5.0, 0.2, 5000, 5000, SpreadSide.ASK));
		assertEquals("0.6141", String.format("%1$.4f", ask));
	}
	
	@Test
	public void skewUpBid() {
		Double ask = Double.valueOf(formula.skewPrice(0.5341, 0.6141, 1000, 1000, 5.0, 0.2, 5000, 5000, SpreadSide.BID));
		assertEquals("0.5341", String.format("%1$.4f", ask));
	}
	
	@Test
	public void skewDownAsk() {
		Double ask = Double.valueOf(formula.skewPrice(0.5341, 0.6141, 500, 1000, 5.0, 0.2, 5000, 5000, SpreadSide.ASK));
		assertEquals("0.5891", String.format("%1$.4f", ask));
	}
	
	@Test
	public void skewDownBid() {
		Double ask = Double.valueOf(formula.skewPrice(0.5341, 0.6141, 500, 1000, 5.0, 0.2, 5000, 5000, SpreadSide.BID));
		assertEquals("0.5091", String.format("%1$.4f", ask));
	}

	@Test
	public void skewWayDownAsk() {
		Double ask = Double.valueOf(formula.skewPrice(0.5341, 0.6141, 1000, 5000, 5.0, 0.2, 5000, 5000, SpreadSide.ASK));
		assertEquals("0.5741", String.format("%1$.4f", ask));
	}
	
	@Test
	public void skewWayDownBid() {
		Double bid = Double.valueOf(formula.skewPrice(0.5341, 0.6141, 1000, 5000, 5.0, 0.2, 5000, 5000, SpreadSide.BID));
		assertEquals("0.4941", String.format("%1$.4f", bid));
	}

	@Test
	public void quoteAsk() {
		DynamicsQuote dq = formula.getOpportunityQuote(market, opportunity, 1493819434000L);
		assertEquals("32.5925", String.format("%1$.4f", dq.getAsk()));
	}

	@Test
	public void quoteBid() {
		DynamicsQuote dq = formula.getOpportunityQuote(market, opportunity, 1493819434000L);
		assertEquals("24.5925", String.format("%1$.4f", dq.getBid()));
	}
	
	@AfterClass
	public static void clean() {
		// do nothing for now
	}
}