package utils;

/**
 * @author liors
 *
 */
public class Config {
	
	//IL db credentials
	public static final String DB_URL = "jdbc:oracle:thin:@(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = dbtest.etrader.co.il)(PORT = 1521))(CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME = etrader)))";
	public static final String DB_USER = "etrader_web";
	public static final String DB_PASS = "w3tr4d3r";

	//BG db credentials
	//public static final String DB_URL = "jdbc:oracle:thin:@(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = dbtestbg.anyoption.com)(PORT = 1521))(CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME = etrader)))";
	//public static final String DB_USER = "etrader_web";
	//public static final String DB_PASS = "w3tr4d3r";
	
	//anyoption API
	public static final String ANYOPTION_API_URL = "https://api.anyoption.com/api_gateway/services"; 
	
	//anyoption service 
	//local
	public static final String AO_SERVICE_URL = "http://www.liors.testenv.anyoption.com/jsonService/AnyoptionService/";
	
	//anyoption service
	//Live
	//public static final String AO_SERVICE_URL = "https://json.anyoptionservice.com/jsonService/AnyoptionService/";
}
