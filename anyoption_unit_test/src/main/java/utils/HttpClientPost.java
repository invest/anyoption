package utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import com.anyoption.json.util.JsonExclusionStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author liors
 * 
 * RESTful Java HttpClient with Apache 
 * Create a RESTful Java client with Apache HttpClient, to perform a “POST” request
 * 
 * It's Just an Example!
 *
 */
public class HttpClientPost {	
	private static final Logger logger = Logger.getLogger(HttpClientPost.class);
	
	static final String COOKIES_HEADER = "Set-Cookie";
	static java.net.CookieManager cookieManager = new java.net.CookieManager();
	
	/**
	 * Post http json request to the given URL 
	 * 
	 * @param URL
	 * @param jsonRequest
	 * @return output string
	 */
	public static String doPost(String URL, String jsonRequest) {
		String output = "";
		DefaultHttpClient httpClient = new DefaultHttpClient();
		try {			
			HttpPost postRequest = new HttpPost(URL);
			StringEntity request = new StringEntity(jsonRequest);
			request.setContentType("application/json");
			postRequest.setEntity(request);
			HttpResponse response = httpClient.execute(postRequest);
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}
			BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
			output = br.readLine();
		} catch (IOException e) {
			logger.error("doPost IOException", e);
		} catch (Exception e) {
			logger.error("doPost Exception", e);
		} finally {
			httpClient.getConnectionManager().shutdown();
		}
		return output;
	}
	
	
	/**
	 * Post http Object request to the given URL 
	 * 
	 * @param serviceUrl
	 * @param request
	 * @param response
	 * @return Object
	 */
	public static Object doPost(String serviceUrl, Object request, Object response) {
    	try { 		
	        byte[] buffer = new byte[2048]; // 2K read/write buffer. This might need change for better performance
	        URL u = new URL(serviceUrl);
	        HttpURLConnection httpCon = (HttpURLConnection)u.openConnection();
	        httpCon.setDoOutput(true);	 
	        Gson prettyGson = new GsonBuilder().setPrettyPrinting().create();
	        String pretJson = prettyGson.toJson(request);
	        logger.info("Pretty printing: " + pretJson);
	        Gson gson = new GsonBuilder().setPrettyPrinting().create();     
	        httpCon.getOutputStream().write(gson.toJson(request).getBytes("UTF-8"));
	        int readCount = -1;
	        InputStream is = httpCon.getInputStream(); 
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        OutputStream os = baos;
	        while ((readCount = is.read(buffer)) != -1) {
	        	logger.info("readCount: " + readCount);
	            os.write(buffer, 0, readCount);
	        }	       
	        os.flush();
	        os.close();
            
            byte[] resp = baos.toByteArray();
            String json = new String(resp, "UTF-8");
            logger.info("json: " + json);           
            return gson.fromJson(json, response.getClass());
	    } catch (Exception e) {
	    	logger.info("ERROR! problem forword service call", e); 
		}   
    	return null;
    }
	
	/**
	 * @param serviceUrl
	 * @param clazz
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public static <T> T doPost(String serviceUrl, Class<T> clazz, Object request) throws Exception {
		HttpURLConnection httpConn = null;
		try {
	 		URL url = new URL(serviceUrl);
			httpConn = (HttpURLConnection) url.openConnection();
			httpConn.setRequestMethod("POST");
			
			if(cookieManager.getCookieStore().getCookies().size() > 0)	{
				httpConn.setRequestProperty("Cookie", cookieManager.getCookieStore().getCookies().stream().map(Object::toString).collect(Collectors.joining(";")));    
			}
			
			Gson gson = new GsonBuilder().setExclusionStrategies(new JsonExclusionStrategy()).serializeNulls().create();
			String json =  gson.toJson(request);
			httpConn.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(httpConn.getOutputStream());
			wr.writeBytes(json);
			wr.flush();
			wr.close();	 
			
			BufferedReader in = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
			T result = gson.fromJson(in, clazz);
			
			Map<String, List<String>> headerFields = httpConn.getHeaderFields();
			List<String> cookiesHeader = headerFields.get(COOKIES_HEADER);

			cookieManager.getCookieStore().removeAll();
			if(cookiesHeader != null) {
			    for (String cookie : cookiesHeader) {
			      cookieManager.getCookieStore().add(null,HttpCookie.parse(cookie).get(0));
			    }               
			}
			
			in.close();	 
			return result;
		} finally {
			httpConn.disconnect();
		}
	}
}

