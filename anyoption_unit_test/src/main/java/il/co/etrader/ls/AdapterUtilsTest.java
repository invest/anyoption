package il.co.etrader.ls;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class AdapterUtilsTest {
	@Test
	public void patchForUnderscoreInUserNameInsurances() {
    	String [] itemNameArray = patchForUnderscore("insurances_autotestwlbxsd@anyoption.com");
    	assertEquals("autotestwlbxsd@anyoption.com", itemNameArray[1]);
	}

	@Test
	public void patchForUnderscoreInUserNameInsurancesWithUnderscore() {
    	String [] itemNameArray = patchForUnderscore("insurances_autotest_wlbxsd@anyoption.com");
    	assertEquals("autotest_wlbxsd@anyoption.com", itemNameArray[1]);
	}

	@Test
	public void patchForUnderscoreInUserNameShoppingBag() {
    	String [] itemNameArray = patchForUnderscore("shoppingbag_autotestwlbxsd@anyoption.com_185793");
    	assertEquals("autotestwlbxsd@anyoption.com", itemNameArray[1]);
	}

	@Test
	public void patchForUnderscoreInUserNameShoppingBagWithUnderscore() {
    	String [] itemNameArray = patchForUnderscore("shoppingbag_autotest_wlbxsd@anyoption.com_185793");
    	assertEquals("autotest_wlbxsd@anyoption.com", itemNameArray[1]);
	}

	@Test
	public void patchForUnderscoreInUserNameCopyopNews() {
    	String [] itemNameArray = patchForUnderscore("1_autotestwlbxsd@anyoption.com_185793");
    	assertEquals("autotestwlbxsd@anyoption.com", itemNameArray[1]);
	}

	@Test
	public void patchForUnderscoreInUserNameCopyopNewsWithUnderscore() {
    	String [] itemNameArray = patchForUnderscore("1_autotest_wlbxsd@anyoption.com_185793");
    	assertEquals("autotest_wlbxsd@anyoption.com", itemNameArray[1]);
	}
	
	private String[] patchForUnderscore(String itemName) {
    	String [] itemNameArray = itemName.split("_");
    	return AdapterUtils.patchForUnderscoreInUserName(itemNameArray);
	}
}