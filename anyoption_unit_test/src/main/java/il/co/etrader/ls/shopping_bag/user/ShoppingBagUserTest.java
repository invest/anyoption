package il.co.etrader.ls.shopping_bag.user;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ShoppingBagUserTest {
	@Test
	public void createShoppingBagUser() {
		String itemName = "shoppingbag_autotestwlbxsd@anyoption.com_185793";
    	String [] itemNameArray = itemName.split("_");
    	ShoppingBagUser user = new ShoppingBagUser(itemNameArray, itemName);
    	assertEquals("autotestwlbxsd@anyoption.com", user.getUserName());
	}

	@Test
	public void createShoppingBagUserWithUnderscore() {
		String itemName = "shoppingbag_autotest_wlbxsd@anyoption.com_185793";
    	String [] itemNameArray = itemName.split("_");
    	ShoppingBagUser user = new ShoppingBagUser(itemNameArray, itemName);
    	assertEquals("autotest_wlbxsd@anyoption.com", user.getUserName());
	}
}