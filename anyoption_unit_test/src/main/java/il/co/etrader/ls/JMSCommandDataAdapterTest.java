package il.co.etrader.ls;

import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.lightstreamer.interfaces.data.FailureException;
import com.lightstreamer.interfaces.data.SubscriptionException;

public class JMSCommandDataAdapterTest {
	@Test
	public void subscribeInsurances() throws SubscriptionException, FailureException {
		subscribe("insurances_autotestwlbxsd@anyoption.com", "autotestwlbxsd@anyoption.com");
	}

	@Test
	public void unsubscribeInsurances() throws SubscriptionException, FailureException {
		unsubscribe("insurances_autotestwlbxsd@anyoption.com", "autotestwlbxsd@anyoption.com");
	}

	@Test
	public void subscribeInsurancesWithUnderscore() throws SubscriptionException, FailureException {
		subscribe("insurances_autotest_wlbxsd@anyoption.com", "autotest_wlbxsd@anyoption.com");
	}

	@Test
	public void unsubscribeInsurancesWithUnderscore() throws SubscriptionException, FailureException {
		unsubscribe("insurances_autotest_wlbxsd@anyoption.com", "autotest_wlbxsd@anyoption.com");
	}
	
	private void subscribe(String itemName, String userName) throws SubscriptionException, FailureException {
		JMSCommandDataAdapter adapter = new JMSCommandDataAdapter();
		adapter.log = Logger.getLogger(JMSCommandDataAdapter.class);
		adapter.subscribe(itemName, false);
		assertEquals(userName, adapter.usersQueue.get(0));
	}
	
	private void unsubscribe(String itemName, String userName) throws SubscriptionException, FailureException {
		JMSCommandDataAdapter adapter = new JMSCommandDataAdapter();
		adapter.log = Logger.getLogger(JMSCommandDataAdapter.class);
		adapter.subscribe(itemName, false);
		assertEquals(userName, adapter.usersQueue.get(0));
		adapter.unsubscribe(itemName);
		assertEquals(0, adapter.usersQueue.size());
	}
}