package junits.pixel;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import testing.service.pixel.PixelTest;
import utils.Config;

import com.anyoption.common.beans.Skin;
import com.anyoption.json.requests.PixelMethodRequest;
import com.anyoption.json.results.PixelMethodResult;

public class GetTrackingPixelJUnit {
	private PixelMethodRequest pixelMethodRequest = new PixelMethodRequest();
	private String serviceUrl;
	
	@Before
	public void init() {
		pixelMethodRequest.setDynamicParam("123456");
		pixelMethodRequest.setCombinationId(21966);
		pixelMethodRequest.setSkinId(Skin.SKIN_ENGLISH);
		pixelMethodRequest.setPixelType(2); //firstNewCard - 1, register - 2
		pixelMethodRequest.setUserId(1014251);
		pixelMethodRequest.setTransactionId(0);
		
		String serviceName = "getTrackingPixel";
		serviceUrl = Config.AO_SERVICE_URL + serviceName;
	}
	
	@Test
	public void test() {
		PixelMethodResult result = PixelTest.getTrackingPixel(serviceUrl, pixelMethodRequest);
		assertEquals(0, result.getErrorCode());
	}
}
