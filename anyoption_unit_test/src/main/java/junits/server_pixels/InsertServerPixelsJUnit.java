package junits.server_pixels;

import java.sql.Connection;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import org.junit.Test;
import utils.Config;
import com.anyoption.common.beans.ServerPixel;
import com.anyoption.common.daos.ServerPixelsDAOBase;
import com.anyoption.common.util.OracleUtil;


/**
 * @author LioR SoLoMoN
 *
 */
public class InsertServerPixelsJUnit {
	private static final Logger logger = Logger.getLogger(InsertServerPixelsJUnit.class);
	
	@Test
	public void afterInvestmentSuccess() {	
		Connection connection = null;
		try {
			connection = OracleUtil.getConnection(Config.DB_URL, Config.DB_USER, Config.DB_PASS);		
			ServerPixelsDAOBase.insert(connection, new ServerPixel(1, 123123123, ServerPixel.SERVER_PIXELS_PUBLISHER_APPSFLYER));
		} catch (SQLException e) {
			logger.error("ERROR! cant after Investment Success", e);
		} finally {
			OracleUtil.closeConnection(connection);
		}
	}
}
