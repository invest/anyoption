package junits.register;

import static org.junit.Assert.*;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import com.anyoption.beans.base.Register;

/**
 * @author liors
 *
 */
public class InsertUserJUnit {
	private Register user = new Register();
	@Before
	public void init() {
		user.setFirstName("ufnit");
    	user.setLastName("tesft");
    	user.setEmail("unittest@anyoption.com");
		user.setPassword("123456");
		user.setPassword2("123456");
		user.setMobilePhone("0132456789");
		user.setUserName("unittest");
		user.setCountryId(22);
		user.setContactByEmail(false);
		user.setContactBySms(false);
		user.setTerms(true);
    	user.setSkinId(1);
    	user.setCombinationId(22L);
    	user.setDynamicParam("");
    	user.setAff_sub1("1");
    	user.setAff_sub2("2");
    	user.setAff_sub3("3");
    	user.setHttpReferer("");
    	user.setTimeFirstVisit(new Date());
    	user.setRegisterAttemptId(0);
    	user.setPlatformId(2);
	}
	
	@Test
	public void test() {
		fail("Not yet implemented");
	}

}
