package junits.email;

import java.util.Properties;
import javax.mail.Address;
import javax.mail.internet.InternetAddress;
import org.junit.Before;
import org.junit.Test;
import com.anyoption.common.beans.EmailConfig;
import com.anyoption.common.util.CommonUtil;

/**
 * @author LioR SoLoMoN
 *
 */
public class SendEmailJUnit {

	private EmailConfig emailConfig;

	@Before
	public void init() {
		try {
			Properties serverProp = new Properties();
			serverProp.put("mail.smtp.host", (String) "zimbra.etrader.co.il");
			emailConfig = new EmailConfig();
			emailConfig.setServerProp(serverProp);
			emailConfig.setAuthenticationPassword("Setrader12!");
			emailConfig.setAuthenticationUsername("support@etrader.co.il");
			emailConfig.setFrom("support@etrader.co.il");
			InternetAddress a = new InternetAddress("eranl@etrader.co.il");			
			//InternetAddress b = new InternetAddress("liors@etrader.co.il");
			//InternetAddress c = new InternetAddress("");
			Address[] to = {a}; 
			//Address[] cc = {b};
			//Address[] bcc = {b};
			emailConfig.setTo(to);
			//emailConfig.setCc(cc);
			//emailConfig.setBcc(bcc);
			emailConfig.setSubject("Subject");	
			emailConfig.setBody("for u :)");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Test
	public void test() {
		CommonUtil.sendSingleEmail(emailConfig);
	}
}
