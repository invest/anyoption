package junits.epg;

import org.junit.Before;
import org.junit.Test;
import testing.epg.CaptureWithdrawalTest;

/**
 * @author liors
 *
 */
public class CaptureWithdrawalJUnit {
	private String transactionIds;
	@Before
	public void init() {
		transactionIds = "";
	}
	
	@Test
	public void test() {
		CaptureWithdrawalTest.CaptureWithdrawalDoJob(transactionIds);
	}
}
