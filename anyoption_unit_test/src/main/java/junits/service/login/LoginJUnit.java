package junits.service.login;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import testing.service.login.LoginTest;
import utils.Config;

import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.Login;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.UserMethodResult;

/**
 * @author liors
 *
 */
public class LoginJUnit {
	private UserMethodRequest request = new UserMethodRequest();
	private String serviceUrl;
	
	@Before
	public void init() {
		request.setUserName("LIOREN");
		request.setPassword("123456");
		request.setSkinId(2);
		request.setEncrypt(false);
		request.setWriterId(Writer.WRITER_ID_MOBILE);
		request.setAppVersion("300");
		request.setUtcOffset(180);
		
		Login login = new Login();
		login.setAffSub1("aflL");
		login.setAffSub2("af2L");
		login.setAffSub3("af3L");
		login.setCombinationId(123L);
		login.setDynamicParam("DPL");
		request.setLogin(login);
    	
		
		String serviceName = "getUser";
		serviceUrl = Config.AO_SERVICE_URL + serviceName;
	}
	
	@Test
	public void test() {
		UserMethodResult result = LoginTest.getUser(serviceUrl, request);
		assertEquals(0, result.getErrorCode());
	}

}
