package junits.service.deposit;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.anyoption.json.requests.CcDepositMethodRequest;
import com.anyoption.json.results.TransactionMethodResult;

import testing.service.payment.deposit.DepositTest;
import utils.Config;

/**
 * @author EyalG
 *
 */
public class InsertCCDepositServiceJUnit {
	private CcDepositMethodRequest request = new CcDepositMethodRequest();
	private String serviceUrl;
	
	@Before
	public void init() {
		// change according to unit test case.		
		
		request.setAmount("10000");
		request.setCardId(270641);
		request.setCcPass(null);
		request.setUserName("ASAFIS1");
		request.setPassword("108_-95_51_-34_84_111_26_23");
		request.setSkinId(1);

		
		String serviceName = "insertDepositCard";
		serviceUrl = Config.AO_SERVICE_URL + serviceName;
	}

	@Test
	public void test() {
		TransactionMethodResult result = DepositTest.CreditcardDeposit(serviceUrl, request);
		assertEquals(0, result.getErrorCode());
	}
}
