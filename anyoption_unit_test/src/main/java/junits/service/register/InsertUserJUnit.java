package junits.service.register;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import testing.service.register.RegisterTest;
import utils.Config;

import com.anyoption.beans.base.Register;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.requests.InsertUserMethodRequest;

/**
 * @author liors
 *
 */
public class InsertUserJUnit {
	private InsertUserMethodRequest iumr = new InsertUserMethodRequest();
	private String serviceUrl;
	
	@Before
	public void init() {
		Register register = new Register();
		register.setFirstName("ujjjdsa");
		register.setLastName("tnnnnrjs");
    	register.setEmail("unittest_" + new Date().getTime() + "@anyoption.com");
		register.setPassword("123456");
		register.setPassword2("123456");
		register.setMobilePhone("6665554433");
		register.setUserName(register.getEmail());
		register.setCountryId(219);
		register.setContactByEmail(false);
		register.setContactBySms(false);
		register.setTerms(true);
    	register.setCombinationId(null);
    	register.setDynamicParam(null);
    	register.setAff_sub1(null);
    	register.setAff_sub2(null);
    	register.setAff_sub3(null);
    	register.setHttpReferer("");
    	register.setTimeFirstVisit(new Date());
    	register.setRegisterAttemptId(0);
    	register.setPlatformId(2);
    	register.setIp("212.179.246.19");
    	register.setSkinId(16);
    	iumr.setIp(register.getIp());
    	iumr.setSkinId(register.getSkinId());
    	iumr.setRegister(register);
    	iumr.setDeviceId("testjunit_010816");
		
		String serviceName = "insertUser";
		serviceUrl = Config.AO_SERVICE_URL + serviceName;
	}
	
	@Test
	public void test() {
		MethodResult result = RegisterTest.insertUser(serviceUrl, iumr);
		assertEquals(0, result.getErrorCode());
	}

}
