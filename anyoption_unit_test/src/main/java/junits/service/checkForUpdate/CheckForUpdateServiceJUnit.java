package junits.service.checkForUpdate;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import com.anyoption.json.requests.CheckUpdateMethodRequest;
import com.anyoption.json.results.UpdateMethodResult;
import testing.service.checkForUpdate.CheckForUpdateTest;
import utils.Config;

/**
 * @author EyalG
 *
 */
public class CheckForUpdateServiceJUnit {
	private CheckUpdateMethodRequest request = new CheckUpdateMethodRequest();
	private String serviceUrl;
	
	@Before
	public void init() {
		// change according to unit test case.		
		
		request.setDeviceId("testjunit2");
		request.setIp("123.12.43.65"); //not in use localy
		request.setFirstOpening(false);
//		request.setApkVersion(9);
		request.setApkName("android apk");
		request.setIdfa("test_idfa");
		request.setAdvertisingId("test_advertisingId");

		String serviceName = "checkForUpdate";
		serviceUrl = Config.AO_SERVICE_URL + serviceName;
	}

	@Test
	public void test() {
		UpdateMethodResult result = CheckForUpdateTest.checkForUpdate(serviceUrl, request);
		assertEquals(0, result.getErrorCode());
	}
}
