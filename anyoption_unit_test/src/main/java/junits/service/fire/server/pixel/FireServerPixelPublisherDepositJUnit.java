package junits.service.fire.server.pixel;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import testing.service.fire.server.pixel.FireServerPixelTest;
import utils.Config;

import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.requests.FireServerPixelMethodRequest;

/**
 * @author EyalG
 *
 */
public class FireServerPixelPublisherDepositJUnit {
	private FireServerPixelMethodRequest requset = new FireServerPixelMethodRequest();
	private String serviceUrl;
	
	@Before
	public void init() {
		requset.setDeviceId("C1A73AEB-D6B7-428A-9779-71274C6EAA39");
		requset.setPixelTypeId(5);
		requset.setPublisherId(2);
		requset.setTransactionId(22402771);
		
		String serviceName = "fireServerPixel";
		serviceUrl = Config.AO_SERVICE_URL + serviceName;
	}
	
	@Test
	public void test() {
		MethodResult result = FireServerPixelTest.firePixel(serviceUrl, requset);
		assertEquals(0, result.getErrorCode());
	}

}
