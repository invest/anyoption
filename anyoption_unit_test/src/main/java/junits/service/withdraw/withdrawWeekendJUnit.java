package junits.service.withdraw;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import testing.service.payments.withdraw.Withdraw;
import utils.Config;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;

/**
 * @author liors
 *
 */
public class withdrawWeekendJUnit {
	private static final String SERVICE_NAME = "isWithdrawalWeekendsAvailable";
	UserMethodRequest user = new UserMethodRequest();
	
	@Test
	public void test() {
		user.setUserName("LIOREN");
		user.setPassword("108_-95_51_-34_84_111_26_23");
		user.setSkinId(2);
		
		MethodResult result = Withdraw.isWithdrawalWeekendsAvailable(Config.AO_SERVICE_URL + SERVICE_NAME, user);
		assertEquals(0, result.getErrorCode());
	}
}
