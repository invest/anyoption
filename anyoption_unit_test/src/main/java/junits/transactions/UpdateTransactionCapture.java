package junits.transactions;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import testing.queries.TransactionsTest;
import com.anyoption.common.clearing.ClearingInfo;

/**
 * @author liors
 *
 */
public class UpdateTransactionCapture {

	ClearingInfo clearingInfo = new ClearingInfo();
	
	@Before
	public void init() {
		clearingInfo.setSuccessful(false);
		clearingInfo.setProviderTransactionId("1234");
		clearingInfo.setTransactionId(29439682);
	}

	@Test
	public void testUpdateTransactionCapture() {
		assertEquals(1, TransactionsTest.updateTransactionCapture(clearingInfo, ""));
	}
}
