package junits.transactions;

import static org.junit.Assert.*;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import com.anyoption.common.beans.Transaction;
import testing.queries.TransactionsTest;


/**
 * @author liors
 *
 */
public class DepositTrxForCredit {
	private static final Logger logger = Logger.getLogger(DepositTrxForCredit.class);
	private long userId;
	private long ccId;
	private String providerId;
	
	@Before
	public void init() {
		userId = 130782;
		ccId = 43659;
		providerId = "54";
	}

	@Test
	public void getDepositTrxForCreditTrue() {
		ArrayList<Transaction> transactions = TransactionsTest.getDepositTrxForCredit(userId, ccId, providerId, true);
		for(Transaction transaction : transactions) {
			logger.info("transaction id = " + transaction.getId());
		}
		assertNotNull(transactions);
	}
	
	@Test
	public void getDepositTrxForCreditFalse() {
		ArrayList<Transaction> transactions = TransactionsTest.getDepositTrxForCredit(userId, ccId, providerId, false);
		for(Transaction transaction : transactions) {
			logger.info("transaction id = " + transaction.getId());
		}
		assertNotNull(transactions);
	}
}
