package junits.transactions;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import testing.queries.TransactionsTest;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.payments.CancelDeposit;

/**
 * @author liors
 *
 */
public class CancelDepositUdpate {

	private CancelDeposit cancelDeposit;
	
	@Before
	public void init() {
		cancelDeposit = new CancelDeposit();
		ClearingInfo c = new ClearingInfo();
		c.setTransactionId(29439682);
		
		cancelDeposit.setClearingInfo(c);
		cancelDeposit.setStatus(3);
		cancelDeposit.setUserUtcOffset("");
		cancelDeposit.setWriterId(Writer.WRITER_ID_AUTO);
	}

	@Test
	public void testCancelDepositUdpate() {
		assertEquals(1, TransactionsTest.updateStatusId(cancelDeposit));
	}
}
