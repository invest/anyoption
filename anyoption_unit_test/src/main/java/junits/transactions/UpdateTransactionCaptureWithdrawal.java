package junits.transactions;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import testing.queries.TransactionsTest;
import com.anyoption.common.payments.CaptureWithdrawal;

/**
 * @author liors
 *
 */
public class UpdateTransactionCaptureWithdrawal {
	private CaptureWithdrawal captureWithdrawal;
	private long userId;
	
	@Before
	public void init() {
		userId = 130782;
		captureWithdrawal = new CaptureWithdrawal(3, "test comment", "test description", 2542980, "222", "", 54);
	}

	@Test
	public void testUpdateTransactionCapture() {
		assertEquals(1, TransactionsTest.updateTransactionAfterCaptureWithdrawal(captureWithdrawal, userId));
	}
}
