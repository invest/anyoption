package junits.credit.cards;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.base.CreditCardType;
import com.anyoption.json.requests.CardMethodRequest;
import com.anyoption.json.results.CardMethodResult;

import testing.service.creditcard.CreditcardTest;
import utils.Config;

/**
 * @author EyalG
 *
 */
public class InsertCreditCardServiceJUnit {
	private CardMethodRequest cardMethodRequest = new CardMethodRequest();
	private String serviceUrl;
	private CreditCard card = new CreditCard();
	
	@Before
	public void init() {
		// change according to unit test case.		

		card.setUserId(5849);
//		card.setId(270641);
		CreditCardType c = new CreditCardType();
		c.setId(2);
		card.setType(c);
		card.setTypeId(2);
		card.setVisible(true);
		card.setCcNumber(4200000);
		card.setCcPass("123");
		card.setExpMonth("10");
		card.setExpYear("16");
		card.setHolderName("test");
		card.setHolderIdNum("1111111");	
		card.setHolderId("1111111");//100000000
		card.setPermission(1);
		card.setUtcOffsetModified("");
		card.setDocumentsSent(true);
		card.setRecurringTransaction("");
		
		cardMethodRequest.setCard(card);
		cardMethodRequest.setUserName("ASAFIS1");
		cardMethodRequest.setPassword("108_-95_51_-34_84_111_26_23");
		cardMethodRequest.setSkinId(1);

		
		String serviceName = "insertCard";
		serviceUrl = Config.AO_SERVICE_URL + serviceName;
	}

	@Test
	public void test() {
		CardMethodResult result = CreditcardTest.insertCreditcard(serviceUrl, cardMethodRequest);
		assertEquals(200, result.getErrorCode()); //error not good cc number
		
		cardMethodRequest.getCard().setCcNumber(4200000000000000L);
		result = CreditcardTest.insertCreditcard(serviceUrl, cardMethodRequest);
		assertEquals(0, result.getErrorCode());
	}
}
