package junits.credit.cards;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.base.CreditCardType;
import testing.queries.CreditCardTest;

/**
 * @author liors
 *
 */
public class CreditCardUpdateJUnit {
	private CreditCard card = new CreditCard();
	
	@Before
	public void init() {
		// change according to unit test case.		
		card.setUserId(5849);
		CreditCardType c = new CreditCardType();
		c.setId(2);
		card.setType(c);
		card.setTypeId(2);
		card.setVisible(true);
		card.setCcNumber(111111);
		card.setCcPass(null);
		card.setExpMonth("10");
		card.setExpYear("15");
		card.setHolderName("test");
		card.setHolderIdNum("1111111");	
		card.setPermission(1);
		card.setUtcOffsetModified("");
		card.setDocumentsSent(true);
		card.setRecurringTransaction("");
		card.setId(270641);
	}

	@Test
	public void test() {
		assertEquals(1, CreditCardTest.update(card));
	}
}
