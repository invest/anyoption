package junits.credit.cards;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import testing.queries.CreditCardTest;
import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.base.CreditCardType;

/**
 * @author liors
 *
 */
public class CreditCardUpdateWebDetailsJUnit {
	private CreditCard card = new CreditCard();
	
	@Before
	public void init() {
		//change according to unit test case.
		card.setUserId(5849);
		CreditCardType c = new CreditCardType();
		c.setId(2);
		card.setType(c);
		card.setTypeId(2);
		card.setVisible(true);
		card.setCcNumber(111111);
		card.setCcPass("123");
		card.setExpMonth("12");
		card.setExpYear("16");
		card.setHolderName("test");
		card.setHolderIdNum("000000000");
		card.setPermission(1);
		card.setUtcOffsetModified("");
		card.setDocumentsSent(true);
		card.setRecurringTransaction("");
		card.setId(270639);
	}

	@Test
	public void test() {
		assertEquals(1, CreditCardTest.updateWebDetails(card));
	}
}
