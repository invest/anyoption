package junits.credit.cards;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author liors
 *
 */
@RunWith(Suite.class)
@SuiteClasses({CreditCardUpdateJUnit.class,
		CreditCardUpdateWebDetailsJUnit.class})
public class AllTests {

}
