package junits.bonuses;


import java.sql.Connection;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import org.junit.Test;
import utils.Config;
import com.anyoption.common.managers.InvestmentsManagerBase;
import com.anyoption.common.util.OracleUtil;


/**
 * @author LioR SoLoMoN
 *
 */
public class WageringJUnit {
	private static final Logger logger = Logger.getLogger(WageringJUnit.class);
	
	@Test
	public void afterInvestmentSuccess() {	
		Connection connection = null;
		try {
			connection = OracleUtil.getConnection(Config.DB_URL, Config.DB_USER, Config.DB_PASS);		
			//InvestmentsManagerBase.afterInvestmentSuccess(connection, 1013231, 20000, 177100352, 2000, 1,false, 6, 0, 0);
		} catch (SQLException e) {
			logger.error("ERROR! ", e);
		} finally {
			OracleUtil.closeConnection(connection);
		}
	}
}
