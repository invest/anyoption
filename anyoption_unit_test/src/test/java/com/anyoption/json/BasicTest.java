package com.anyoption.json;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;
import org.junit.Before;

import com.anyoption.beans.base.Register;
import com.anyoption.common.beans.Login;
import com.anyoption.common.beans.Skin;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.util.Encryptor;
import com.anyoption.json.requests.InsertUserMethodRequest;
import com.anyoption.test.manager.BaseManager;


public class BasicTest {

	protected static final Logger logger = Logger.getLogger(BasicTest.class);

	private static final String TIMEZONE_OFFSET				= "timezone.offset";
	private static final String SKIN_ID						= "skin.id";
	private static final String WRITER_ID					= "writer.id";
	private static final String UDID						= "UDID";
	private static final String DEVICE_OS_VERSION			= "device.os.version";
	private static final String DEVICE_MODEL				= "device.model";
	private static final String DEVICE_MANUFACTURER			= "device.manufacturer";
	private static final String APPLICATION_VERSION_NAME	= "application.version.name";
	private static final String IS_TABLET					= "is.tablet";

	protected static final String SERVICE_URL				= "service.url";// the url where is deployed the json service
	protected static final String USER_NAME					= "user.name";	// anyoption user
	protected static final String PASSWORD					= "password";	// anyoption pass
	
	protected static final String DB_URL					= "db.url";
	protected static final String DB_SERVICE				= "db.service";
	protected static final String DB_USER					= "db.user";
	protected static final String DB_PASS					= "db.pass";

	protected static final String REG_USER_NAME				= "register.user.name";
	protected static final String REG_PASSWORD				= "register.password";
	protected static final String REG_CURR_ID				= "register.currency.id";
	protected static final String REG_FIRST_NAME			= "register.first.name";
	protected static final String REG_LAST_NAME				= "register.last.name";
	protected static final String REG_STREET				= "register.street";
	protected static final String REG_STREE_NUMBER			= "register.street.number";
	protected static final String REG_CITY_NAME				= "register.city.name";
	protected static final String REG_CITY_ID				= "register.city.id";
	protected static final String REG_ZIPCODE				= "register.zipcode";
	protected static final String REG_LANG_ID				= "register.language.id";
	protected static final String REG_BIRTH_YEAR			= "register.birth.year";
	protected static final String REG_BIRTH_MONTH			= "register.birth.month";
	protected static final String REG_BIRTH_DATE			= "register.birth.date";
	protected static final String REG_PHONE_PREFIX			= "register.phone.prefix";
	protected static final String REG_PHONE_NUMBER			= "register.phone.number";
	protected static final String REG_COUNTRY_ID			= "register.country.id";
	
	protected static final String SERVICE_LEVEL_INITIAL_CONTEXT_FACTORY	= "service.level.INITIAL_CONTEXT_FACTORY";
	protected static final String SERVICE_LEVEL_PROVIDER_URL			= "service.level.PROVIDER_URL";
	protected static final String SERVICE_LEVEL_CONNECTION_FACTORY_NAME	= "service.level.CONNECTION_FACTORY_NAME";
	protected static final String SERVICE_LEVEL_TOPIC_NAME				= "service.level.TOPIC_NAME";
	protected static final String SERVICE_LEVEL_QUEUE_NAME				= "service.level.QUEUE_NAME";
	protected static final String SERVICE_LEVEL_MSG_POOL_SIZE			= "service.level.MSG_POOL_SIZE";
	protected static final String SERVICE_LEVEL_RECOVERY_PAUSE			= "service.level.RECOVERY_PAUSE";
	
	protected static String serviceUrl;
	
	private static final String[] keys = {
						TIMEZONE_OFFSET, SKIN_ID, WRITER_ID, UDID, DEVICE_OS_VERSION, DEVICE_MODEL, DEVICE_MANUFACTURER, APPLICATION_VERSION_NAME, IS_TABLET, 
						SERVICE_URL, USER_NAME, PASSWORD,
						DB_URL, DB_SERVICE, DB_USER, DB_PASS,
						REG_USER_NAME, REG_PASSWORD, REG_CURR_ID, REG_FIRST_NAME, REG_LAST_NAME, REG_STREET, REG_STREE_NUMBER, REG_CITY_NAME,
						REG_CITY_ID, REG_ZIPCODE, REG_LANG_ID, REG_BIRTH_YEAR, REG_BIRTH_MONTH, REG_BIRTH_DATE, REG_PHONE_PREFIX, REG_PHONE_NUMBER, REG_COUNTRY_ID,
						SERVICE_LEVEL_INITIAL_CONTEXT_FACTORY, SERVICE_LEVEL_PROVIDER_URL, SERVICE_LEVEL_CONNECTION_FACTORY_NAME, SERVICE_LEVEL_TOPIC_NAME, SERVICE_LEVEL_QUEUE_NAME, SERVICE_LEVEL_MSG_POOL_SIZE, SERVICE_LEVEL_RECOVERY_PAUSE
	};
	
	protected Long currencyIds[] =  {	
							Currency.CURRENCY_ILS_ID, Currency.CURRENCY_USD_ID, Currency.CURRENCY_EUR_ID, Currency.CURRENCY_GBP_ID,  
							Currency.CURRENCY_TRY_ID, Currency.CURRENCY_RUB_ID, Currency.CURRENCY_CNY_ID, Currency.CURRENCY_KRW_ID,
							Currency.CURRENCY_SEK_ID, Currency.CURRENCY_AUD_ID, Currency.CURRENCY_ZAR_ID, Currency.CURRENCY_CZK_ID
	};
	
	protected Long skinIds[] =  {
		     			Skin.SKIN_ETRADER,	Skin.SKIN_ENGLISH,	Skin.SKIN_TURKISH,	Skin.SKIN_SPAIN,	Skin.SKIN_ARABIC,	Skin.SKIN_GERMAN,
		     			Skin.SKIN_ITALIAN,	Skin.SKIN_API,		Skin.SKIN_SPAINMI,	Skin.SKIN_RUSSIAN,	/*Skin.SKIN_TLV,*/	Skin.SKIN_FRANCE, 
		     			Skin.SKIN_EN_US,	Skin.SKIN_ES_US,	Skin.SKIN_CHINESE,	Skin.SKIN_REG_EN,	Skin.SKIN_KOREAN,	Skin.SKIN_REG_ES, 
		     			Skin.SKIN_REG_DE,	Skin.SKIN_REG_IT,	Skin.SKIN_REG_FR,	Skin.SKIN_CHINESE_VIP,	Skin.SKIN_DUTCH_REG, Skin.SKIN_SWEDISH_REG 	
	};
	
	protected int withdrawTransactionIds[] = {
												TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW,	TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW,	TransactionsManagerBase.TRANS_TYPE_CHEQUE_WITHDRAW,
												TransactionsManagerBase.TRANS_TYPE_PAYPAL_WITHDRAW,	TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW,	TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_WITHDRAW,
//												TransactionsManagerBase.TRANS_TYPE_WEBMONEY_WITHDRAW,	TransactionsManagerBase.TRANS_TYPE_LOW_AMOUNT_WITHDRAW_FEE 
	};
	
	static Properties properties = null;
	static WebLevelsCache levelsCache = null;
	
	@Before
	public void init() {
		try {
			properties = new Properties();
			properties.load(new FileInputStream("./src/test/resources/configuration.properties"));
			if(!isComplete(properties)) {
				throw new RuntimeException("Non complete properties :"+ properties.keys());
			}
			serviceUrl = properties.getProperty(SERVICE_URL);
			
			BaseManager.init(properties.getProperty(DB_URL), properties.getProperty(DB_SERVICE), properties.getProperty(DB_USER), properties.getProperty(DB_PASS));
	        
			String initialContextFactory	= properties.getProperty(SERVICE_LEVEL_INITIAL_CONTEXT_FACTORY);
	        String providerURL 				= properties.getProperty(SERVICE_LEVEL_PROVIDER_URL);
	        String connectionFactory		= properties.getProperty(SERVICE_LEVEL_CONNECTION_FACTORY_NAME);
	        String topic					= properties.getProperty(SERVICE_LEVEL_TOPIC_NAME);
	        String queue					= properties.getProperty(SERVICE_LEVEL_QUEUE_NAME);
	        String msgPoolSizeStr			= properties.getProperty(SERVICE_LEVEL_MSG_POOL_SIZE);
	        String recoveryPauseStr			= properties.getProperty(SERVICE_LEVEL_RECOVERY_PAUSE);
	        
	        levelsCache = new WebLevelsCache(initialContextFactory, providerURL, connectionFactory, queue, topic, Integer.parseInt(msgPoolSizeStr), Integer.parseInt(recoveryPauseStr), null);
		} catch (IOException e) {
			logger.error("while loading properties :", e);
		}
	}
	
	public static WebLevelsCache getWebLevelsCache() {
		return levelsCache;
	}
	
	public static void addRegisterRequestParam(InsertUserMethodRequest request) {
		Register reg = new Register();
		reg.setUserName(properties.getProperty(REG_USER_NAME));
		reg.setPassword(properties.getProperty(REG_PASSWORD));
		reg.setPassword2(properties.getProperty(REG_PASSWORD));
		reg.setCurrencyId(Long.valueOf(properties.getProperty(REG_CURR_ID)));
		reg.setFirstName(properties.getProperty(REG_FIRST_NAME));
		reg.setLastName(properties.getProperty(REG_LAST_NAME));
		reg.setStreet(properties.getProperty(REG_STREET));
		reg.setStreetNo(properties.getProperty(REG_STREE_NUMBER));
		reg.setCityName(properties.getProperty(REG_CITY_NAME));
		reg.setCityId(Long.valueOf(properties.getProperty(REG_CITY_ID)));
		reg.setZipCode(properties.getProperty(REG_ZIPCODE));
		reg.setLanguageId(Long.valueOf(properties.getProperty(REG_LANG_ID)));
		reg.setBirthYear(properties.getProperty(REG_BIRTH_YEAR));
		reg.setBirthMonth(properties.getProperty(REG_BIRTH_MONTH));
		reg.setBirthDay(properties.getProperty(REG_BIRTH_DATE));
		reg.setMobilePhonePrefix(properties.getProperty(REG_PHONE_PREFIX));
		reg.setMobilePhone(properties.getProperty(REG_PHONE_NUMBER));
		reg.setCountryId(Long.valueOf(properties.getProperty(REG_COUNTRY_ID)));
		reg.setTerms(true);
		request.setRegister(reg);
	}
	
	public static void addCommonRequestParams(MethodRequest request) {
		request.setTablet(new Boolean(properties.getProperty(IS_TABLET)));
		request.setAppVersion(properties.getProperty(APPLICATION_VERSION_NAME));
		request.setDeviceType(properties.getProperty(DEVICE_MANUFACTURER) + " " + properties.getProperty(DEVICE_MODEL));
		request.setOsVersion(properties.getProperty(DEVICE_OS_VERSION));
		request.setDeviceId(properties.getProperty(UDID));
		request.setWriterId(new Long(properties.getProperty(WRITER_ID)));
		request.setSkinId(new Long(properties.getProperty(SKIN_ID)));
		request.setUtcOffset(new Integer(properties.getProperty(TIMEZONE_OFFSET)));
		
		if (request instanceof UserMethodRequest) {
			UserMethodRequest userMethodRequest = (UserMethodRequest) request;
			if (!userMethodRequest.isLogin() && userMethodRequest.getPassword() == null) {
				userMethodRequest.setEncrypt(true);
			}
		}
	}

	private static boolean isComplete(Properties properties) {
		for(String key: keys) {
			if(!properties.containsKey(key)) {
				logger.error("Missing property:"+key);
				return false;
			}
		}
		return true;
	}
	
	public static UserMethodRequest createUserMethodRequest(UserMethodRequest request) {
		request.setUserName(properties.getProperty(USER_NAME));
		try{
			if(request.isLogin()) {
				request.setPassword(properties.getProperty(PASSWORD));
			} else {
				request.setPassword(Encryptor.encryptStringToString(properties.getProperty(PASSWORD)));
			}
		} catch (CryptoException ex) {
			return null;
		}
		Login login = new Login();
		request.setLogin(login);
		addCommonRequestParams(request);
		return request;
	}
	
	public static InsertUserMethodRequest createInsertUserMethodRequest() {
		InsertUserMethodRequest request = new InsertUserMethodRequest();
		addCommonRequestParams(request);
		addRegisterRequestParam(request);
		return request;
	}
	
}
