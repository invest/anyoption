package com.anyoption.json.service;


import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Calendar;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunityType;
import com.anyoption.common.beans.Skin;
import com.anyoption.common.beans.base.CreditCardType;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.jms.WebLevelLookupBean;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.service.requests.DepositBonusBalanceMethodRequest;
import com.anyoption.common.service.requests.InsertInvestmentMethodRequest;
import com.anyoption.common.service.requests.InvestmentsMethodRequest;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.DepositBonusBalanceMethodResult;
import com.anyoption.common.service.results.ErrorMessage;
import com.anyoption.common.service.results.InvestmentMethodResult;
import com.anyoption.common.service.results.InvestmentsMethodResult;
import com.anyoption.common.service.results.MarketsMethodResult;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.UserFPResult;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.json.BasicTest;
import com.anyoption.json.requests.AssetIndexMethodRequest;
import com.anyoption.json.requests.BonusMethodRequest;
import com.anyoption.json.requests.CardMethodRequest;
import com.anyoption.json.requests.ChangePasswordMethodRequest;
import com.anyoption.json.requests.ChartDataMethodRequest;
import com.anyoption.json.requests.CheckUpdateMethodRequest;
import com.anyoption.json.requests.CreditCardRequest;
import com.anyoption.json.requests.CurrencyMethodRequest;
import com.anyoption.json.requests.FeeMethodRequest;
import com.anyoption.json.requests.FirstEverDepositCheckRequest;
import com.anyoption.json.requests.FormsMethodRequest;
import com.anyoption.json.requests.InboxMethodResult;
import com.anyoption.json.requests.InsertUserMethodRequest;
import com.anyoption.json.requests.MarketingUsersMethodRequest;
import com.anyoption.json.requests.PastExpiresMethodRequest;
import com.anyoption.json.results.AssetIndexMethodResult;
import com.anyoption.json.results.BonusMethodResult;
import com.anyoption.json.results.CardMethodResult;
import com.anyoption.json.results.ChartDataMethodResult;
import com.anyoption.json.results.CountriesOptionsMethodResult;
import com.anyoption.json.results.CountryIdMethodResult;
import com.anyoption.json.results.CreditCardsMethodResult;
import com.anyoption.json.results.CurrenciesResult;
import com.anyoption.json.results.CurrencyMethodResult;
import com.anyoption.json.results.FirstEverDepositCheckResult;
import com.anyoption.json.results.LastCurrenciesRateMethodResult;
import com.anyoption.json.results.LogoutBalanceStepMethodResult;
import com.anyoption.json.results.MarketingUsersMethodResult;
import com.anyoption.json.results.OpportunitiesMethodResult;
import com.anyoption.json.results.OpportunityMethodResult;
import com.anyoption.json.results.OpportunityOddsPairResult;
import com.anyoption.json.results.OptionsMethodResult;
import com.anyoption.json.results.SkinMethodResult;
import com.anyoption.json.results.SkinsOptionsMethodResult;
import com.anyoption.json.results.TransactionMethodResult;
import com.anyoption.json.results.TransactionsMethodResult;
import com.anyoption.json.results.UpdateMethodResult;
//import com.anyoption.json.results.UserQuestionnaireResult;
import com.anyoption.test.manager.OpportunitiesManager;

import utils.HttpClientPost;

public class AnyoptionServiceTest extends BasicTest {
	private static final Logger logger = Logger.getLogger(AnyoptionServiceTest.class);
	
	@Test
	public void testGetUser() throws Exception {
		UserMethodRequest userMethodRequest = new UserMethodRequest();
		userMethodRequest.setLogin(true);
		MethodResult result = (MethodResult)HttpClientPost.doPost(serviceUrl+"getUser", UserMethodResult.class, createUserMethodRequest(userMethodRequest));
		assertEquals(0, result.getErrorCode());
	}
	
	@Test
	public void testInsertUser() throws Exception{
		InsertUserMethodRequest iumr = createInsertUserMethodRequest();
		// make user name and email unique between tests
		iumr.getRegister().setUserName(iumr.getRegister().getUserName()+Calendar.getInstance().getTimeInMillis()+"@anyoption.com");
		iumr.getRegister().setEmail(iumr.getRegister().getUserName());
		UserMethodResult result = HttpClientPost.doPost(serviceUrl + "insertUser", UserMethodResult.class, iumr);
		assertEquals(0, result.getErrorCode());
	}
	
	@Test
	public void testGetCurrencyOption() throws Exception {
		OptionsMethodResult result = HttpClientPost.doPost(serviceUrl+"getCurrenciesOptions", OptionsMethodResult.class, createUserMethodRequest(new UserMethodRequest()));
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getOptions());
	}

	@Test
	public void testGetSkinCurrenciesOption() throws Exception {
		OptionsMethodResult result = HttpClientPost.doPost(serviceUrl+"getSkinCurrenciesOptions", OptionsMethodResult.class, createUserMethodRequest(new UserMethodRequest()));
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getOptions());
	}
	
	@Test
	public void testGetBonusStatesOptions() throws Exception {
		OptionsMethodResult result = HttpClientPost.doPost(serviceUrl+"getBonusStatesOptions", OptionsMethodResult.class, createUserMethodRequest(new UserMethodRequest()));
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getOptions());
	}

	@Test
	public void testGetCountriesOptions() throws Exception {
		CountriesOptionsMethodResult result = HttpClientPost.doPost(serviceUrl+"getCountriesOptions", CountriesOptionsMethodResult.class, createUserMethodRequest(new UserMethodRequest()));
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getOptions());
	}

	@Test
	public void testGetLanguagesOptions() throws Exception {
		OptionsMethodResult result = HttpClientPost.doPost(serviceUrl+"getLanguagesOptions", OptionsMethodResult.class, createUserMethodRequest(new UserMethodRequest()));
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getOptions());
	}
	
	@Test
	public void testGetCreditCardTypesOptions() throws Exception {
		OptionsMethodResult result = HttpClientPost.doPost(serviceUrl+"getCreditCardTypesOptions", OptionsMethodResult.class, createUserMethodRequest(new UserMethodRequest()));
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getOptions());
	}
	
	@Test
	public void testGetSkinsOptions() throws Exception {
		SkinsOptionsMethodResult result = HttpClientPost.doPost(serviceUrl+"getSkinsOptions", SkinsOptionsMethodResult.class, createUserMethodRequest(new UserMethodRequest()));
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getOptions());
	}

	@Test
	public void testGetUserTransactions() throws Exception {
		FormsMethodRequest formMethodRequest = new FormsMethodRequest();
		formMethodRequest.setPageSize(10);
		formMethodRequest.setStartRow(0);

		TransactionsMethodResult result = HttpClientPost.doPost(serviceUrl+"getUserTransactions", TransactionsMethodResult.class, createUserMethodRequest(formMethodRequest));
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getTransactions());
	}

	@Test
	public void testGetChartData() throws Exception {
		ChartDataMethodRequest chartDataMethodRequest = new ChartDataMethodRequest();
		chartDataMethodRequest.setBox(1);
		ChartDataMethodResult result = HttpClientPost.doPost(serviceUrl+"getChartData", ChartDataMethodResult.class, createUserMethodRequest(chartDataMethodRequest));
		assertEquals(0, result.getErrorCode());
	}
	
	@Test
	public void testGetChartDataPreviousHour() throws Exception {
		ChartDataMethodRequest chartDataMethodRequest = new ChartDataMethodRequest();
        ArrayList<Opportunity> opps = OpportunitiesManager.getOpenOpportunities();
        int randomNum = (int)(Math.random() * opps.size()-1);
        long opportunityId = opps.get(randomNum).getId();
		chartDataMethodRequest.setOpportunityId(opportunityId);
		ChartDataMethodResult result = HttpClientPost.doPost(serviceUrl+"getChartDataPreviousHour", ChartDataMethodResult.class, createUserMethodRequest(chartDataMethodRequest));
		assertEquals(0, result.getErrorCode());
	}
	
	@Test
	public void testGetStatesOptions() throws Exception {
		OptionsMethodResult result = HttpClientPost.doPost(serviceUrl+"getStatesOptions", OptionsMethodResult.class, createUserMethodRequest(new UserMethodRequest()));
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getOptions());
	}
	@Test
	public void testGetPastExpiries() throws Exception {
		PastExpiresMethodRequest pastExpiresMethodRequest = new PastExpiresMethodRequest();
		pastExpiresMethodRequest.setUtcOffset(1);
		pastExpiresMethodRequest.setMarketId(16);
		pastExpiresMethodRequest.setStartRow(0);
		pastExpiresMethodRequest.setPageSize(10);
		OpportunitiesMethodResult result = HttpClientPost.doPost(serviceUrl+"getPastExpiries", OpportunitiesMethodResult.class, createUserMethodRequest(pastExpiresMethodRequest));
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getOpportunities());
	}

	@Test
	public void testGetSkinIdByIp() throws Exception {
		SkinMethodResult result = HttpClientPost.doPost(serviceUrl+"getSkinIdByIp", SkinMethodResult.class, createUserMethodRequest(new UserMethodRequest()));
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getSkinId());
	}

	@Test
	public void testGetUserInvestments() throws Exception {
		InvestmentsMethodRequest investmentsMethodRequest = new InvestmentsMethodRequest();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -30);
		investmentsMethodRequest.setFrom(cal);
		investmentsMethodRequest.setTo(Calendar.getInstance());
		investmentsMethodRequest.setSettled(true);
		investmentsMethodRequest.setMarketId(0);
		investmentsMethodRequest.setUtcOffset(2);
		investmentsMethodRequest.setPeriod(0);
		investmentsMethodRequest.setStartRow(0);
		investmentsMethodRequest.setPageSize(10);
		InvestmentsMethodResult result = HttpClientPost.doPost(serviceUrl+"getUserInvestments", InvestmentsMethodResult.class, createUserMethodRequest(investmentsMethodRequest));
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getInvestments());
	}
	
	@Test
	public void testGetAssetIndexByMarket() throws Exception {
		AssetIndexMethodRequest assetIndexMethodRequest = new AssetIndexMethodRequest();
		assetIndexMethodRequest.setMarketId(Market.MARKET_GOLD_ID);
		assetIndexMethodRequest.setUtcOffset(2);
		assetIndexMethodRequest.setSkinId(Skin.SKIN_REG_EN);
		AssetIndexMethodResult result = HttpClientPost.doPost(serviceUrl+"getAssetIndexByMarket", AssetIndexMethodResult.class, assetIndexMethodRequest);
		assertEquals(0, result.getErrorCode());
	}
	
	@Test
	public void testGetUserEncryptedPass() throws Exception {
		UserMethodRequest request = createUserMethodRequest(new UserMethodRequest());
		UserMethodResult result = HttpClientPost.doPost(serviceUrl+"getUserEncryptedPass", UserMethodResult.class, request);
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getUser());
	}
	
	@Test
	public void testGetChangePassword() throws Exception {
		ChangePasswordMethodRequest changePasswordMethodRequest =new ChangePasswordMethodRequest();
		// same password - should fail ? 
		changePasswordMethodRequest.setPasswordNew("123123");
		changePasswordMethodRequest.setPasswordReType("123123");
		changePasswordMethodRequest.setLogin(true);
		MethodResult result = (MethodResult)HttpClientPost.doPost(serviceUrl+"getChangePassword", UserMethodResult.class, createUserMethodRequest(changePasswordMethodRequest));
		assertEquals(0, result.getErrorCode());
	}

	@Test 
	public void testInsertInvestment() throws Exception {
        ArrayList<Opportunity> opps = OpportunitiesManager.getOpenOpportunities();
        if(opps == null || opps.size() <1 ) {
        	fail();
        }
        long oppId = opps.get(1).getId();			
        WebLevelsCache levelsCache = getWebLevelsCache();
		WebLevelLookupBean wllb = new WebLevelLookupBean();
		wllb.setOppId(oppId);
		wllb.setFeedName(opps.get(1).getMarketName());
		wllb.setSkinGroup(SkinGroup.ANYOPTION);
		
		levelsCache.getLevels(wllb);
		logger.info(wllb);
        double currentLevel = wllb.getRealLevel();
        if(currentLevel == 0 ) {
        	fail();
        }
        InsertInvestmentMethodRequest request = new InsertInvestmentMethodRequest();        
        request.setPageLevel(currentLevel);
        request.setPageOddsLose(0.05f);
        request.setPageOddsWin(1.80f);
        request.setChoice(Investment.INVESTMENT_TYPE_CALL);
        request.setOpportunityId(oppId);
        request.setRequestAmount(100);
        
        InvestmentMethodResult invResult = (InvestmentMethodResult)HttpClientPost.doPost(serviceUrl+"insertInvestment", InvestmentMethodResult.class, createUserMethodRequest(request));
        
        logger.info("insertInvestment Error code: " +invResult.getErrorCode());
        ErrorMessage[] err = invResult.getErrorMessages() ;
        if(err != null) {
           for(int q=0;q<err.length;q++) {
        	   logger.info("\t"+err[q]);
           }
        }
        assertEquals(0, invResult.getErrorCode());
	}
	@Test
	public void testGetMarketList() throws Exception {
		MethodRequest request = new MethodRequest();
		request.setSkinId(16);
		MarketsMethodResult result = HttpClientPost.doPost(serviceUrl+"getMarketList", MarketsMethodResult.class, request);
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getMarkets());
	}

	@Test
	public void testGetUserBonuses() throws Exception {
		BonusMethodResult result = HttpClientPost.doPost(serviceUrl+"getUserBonuses", BonusMethodResult.class, createUserMethodRequest(new BonusMethodRequest()));
		assertEquals(0, result.getErrorCode());
	}
	
	
	@Test
	public void testGetAvailableCreditCards() throws Exception {
		CreditCardsMethodResult result = HttpClientPost.doPost(serviceUrl+"getAvailableCreditCards", CreditCardsMethodResult.class, createUserMethodRequest(new UserMethodRequest()));
		assertEquals(0, result.getErrorCode());
	}
	
	@Test
	public void testGetAvailableCreditCardsWithEmptyLine() throws Exception {
		CreditCardsMethodResult result = HttpClientPost.doPost(serviceUrl+"getAvailableCreditCardsWithEmptyLine", CreditCardsMethodResult.class, createUserMethodRequest(new UserMethodRequest()));
		assertEquals(0, result.getErrorCode());
	}
	@Test
	public void testGetWithdrawCCList() throws Exception {
		CreditCardsMethodResult result = HttpClientPost.doPost(serviceUrl+"getWithdrawCCList", CreditCardsMethodResult.class, createUserMethodRequest(new UserMethodRequest()));
		assertEquals(0, result.getErrorCode());
	}

	@Test
	public void testCheckForUpdate() throws Exception {
		CheckUpdateMethodRequest checkUpdateMethodRequest = new CheckUpdateMethodRequest();
		checkUpdateMethodRequest.setDeviceId("0");
		checkUpdateMethodRequest.setSkinId(Skin.SKIN_REG_EN);
		checkUpdateMethodRequest.setAppVersion("0");
		checkUpdateMethodRequest.setApkVersion(0);
		checkUpdateMethodRequest.setApkName("android apk");
		UpdateMethodResult result = HttpClientPost.doPost(serviceUrl+"checkForUpdate", UpdateMethodResult.class, checkUpdateMethodRequest);
		assertEquals(0, result.getErrorCode());
		assertNotEquals(0, result.getServerTime());
		assertTrue(result.isNeedAPKUpdate());
	}
	
	@Test
	public void testC2dmUpdate() throws Exception {
		CheckUpdateMethodRequest checkUpdateMethodRequest = new CheckUpdateMethodRequest();
		checkUpdateMethodRequest.setDeviceId("0");
		checkUpdateMethodRequest.setSkinId(Skin.SKIN_REG_EN);
		checkUpdateMethodRequest.setAppVersion("0");
		checkUpdateMethodRequest.setApkVersion(0);
		checkUpdateMethodRequest.setApkName("android apk");
		MethodResult result = HttpClientPost.doPost(serviceUrl+"c2dmUpdate", MethodResult.class, checkUpdateMethodRequest);
		assertEquals(0, result.getErrorCode());
	}
	
	@Test
	public void testSkinUpdate() throws Exception {
		CheckUpdateMethodRequest checkUpdateMethodRequest = new CheckUpdateMethodRequest();
		checkUpdateMethodRequest.setDeviceId("0");
		checkUpdateMethodRequest.setSkinId(Skin.SKIN_REG_EN);
		checkUpdateMethodRequest.setAppVersion("0");
		checkUpdateMethodRequest.setApkVersion(0);
		checkUpdateMethodRequest.setApkName("android apk");
		MethodResult result = HttpClientPost.doPost(serviceUrl+"skinUpdate", MethodResult.class, checkUpdateMethodRequest);
		assertEquals(0, result.getErrorCode());
	}
	
	@Test
	public void testGetUserInboxMails() throws Exception {
		InboxMethodResult result = HttpClientPost.doPost(serviceUrl+"getUserInboxMails", InboxMethodResult.class, createUserMethodRequest(new UserMethodRequest()));
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getEmails());
	}
	
	@Test
	public void testSendPassword() throws Exception {
		UserMethodRequest userMethodRequest = new UserMethodRequest();
		userMethodRequest.setUserName("pavlintest");
		UserFPResult userFPResult = HttpClientPost.doPost(serviceUrl+"getForgetPasswordContacts", UserFPResult.class, userMethodRequest);
		assertEquals(0, userFPResult.getErrorCode());
		assertNotNull(userFPResult.getObfuscatedEmail());
		 
		InsertUserMethodRequest insertUserMethodRequest = new InsertUserMethodRequest();
		insertUserMethodRequest.setSkinId(Skin.SKIN_FRANCE);
		MethodResult result = HttpClientPost.doPost(serviceUrl+"sendPassword", MethodResult.class, insertUserMethodRequest);
		assertEquals(0, result.getErrorCode());
	}
	@Test
	public void testGetCountryId() throws Exception {
		MethodRequest request = new MethodRequest();
		request.setSkinId(Skin.SKIN_ITALIAN);
		request.setIp("92.247.82.234");
		CountryIdMethodResult result = HttpClientPost.doPost(serviceUrl+"getCountryId", CountryIdMethodResult.class, request);
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getCountryId());
		assertNotEquals(0, result.getCountryId());
	}
	@Test
	public void testGetCurrencyLetters() throws Exception {
		CurrencyMethodRequest request = new CurrencyMethodRequest();
		for( Long currencyId: currencyIds) {
			request.setCurrencyId(currencyId);
			CurrencyMethodResult result = HttpClientPost.doPost(serviceUrl+"getCurrencyLetters", CurrencyMethodResult.class, request);
			assertEquals(0, result.getErrorCode());
			assertNotNull(result.getCurrencyLetters());
		}
	}
	@Test
	public void testGetCitiesOptions() throws Exception {
		MethodRequest methodRequest = new MethodRequest();
		for(Long skinId: skinIds) {
			methodRequest.setSkinId(skinId);
			OptionsMethodResult result = HttpClientPost.doPost(serviceUrl+"getCitiesOptions", OptionsMethodResult.class, methodRequest);
			assertEquals(0, result.getErrorCode());
			assertNotNull(result.getOptions());
		}
	}
	@Test
	public void testGetFeeAmountByType() throws Exception {
		FeeMethodRequest feeMethodRequest = new FeeMethodRequest();
		for(int transTypeId : withdrawTransactionIds) {
			feeMethodRequest.setTranTypeId(transTypeId);
			TransactionMethodResult result = HttpClientPost.doPost(serviceUrl+"getFeeAmountByType", TransactionMethodResult.class, createUserMethodRequest(feeMethodRequest));
			assertEquals(0, result.getErrorCode());
			assertNotNull(result.getFee());
			assertNotEquals(0, result.getFee());
		}
	}

//	@Test
//	public void testGetUserQuestionnaire() throws Exception {
//		String questionnaireServices[] = {"getUserSingleQuestionnaire", "getUserMandatoryQuestionnaire", "getUserOptionalQuestionnaire", "getUserCapitalQuestionnaire"};
//		
//		for(String service: questionnaireServices) {
//			UserQuestionnaireResult result = HttpClientPost.doPost(serviceUrl+service, UserQuestionnaireResult.class, createUserMethodRequest(new UserMethodRequest()));
//			assertEquals(0, result.getErrorCode());
//			assertNotNull(result.getQuestions());
//		}
//	}

	@Test
	public void testGetMinFirstDeposit() throws Exception {
		TransactionMethodResult result = HttpClientPost.doPost(serviceUrl+"getMinFirstDeposit", TransactionMethodResult.class, createUserMethodRequest(new UserMethodRequest()));
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getFormattedAmount());
	}
	
	@Test
	public void testGetOpenHourlyBinaryMarkets() throws Exception {
		MarketsMethodResult result = HttpClientPost.doPost(serviceUrl+"getOpenHourlyBinaryMarkets", MarketsMethodResult.class, createUserMethodRequest(new UserMethodRequest()));
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getMarkets());
	}
	@Test
	public void testGetCurrentHourlyOppByMarket() throws Exception {
		ChartDataMethodRequest chartDataMethodRequest = new ChartDataMethodRequest();
		chartDataMethodRequest.setMarketId(Market.MARKET_USD_EUR_ID);
		chartDataMethodRequest.setOpportunityTypeId(OpportunityType.PRODUCT_TYPE_BINARY);
		OpportunityMethodResult result = HttpClientPost.doPost(serviceUrl+"getCurrentHourlyOppByMarket", OpportunityMethodResult.class, createUserMethodRequest(chartDataMethodRequest));
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getOpportunity());
	}
	
	@Test
	public void testGetCurrentLevelByOpportunityId() throws Exception {
		ArrayList<Opportunity> opps = OpportunitiesManager.getOpenOpportunities();
		int randomNum = (int)(Math.random() * opps.size()-1);
		long opportunityId = opps.get(randomNum).getId();
		ChartDataMethodRequest  chartDataMethodRequest = new ChartDataMethodRequest();
		chartDataMethodRequest.setOpportunityId(opportunityId);
		OpportunityMethodResult result = HttpClientPost.doPost(serviceUrl+"getCurrentLevelByOpportunityId", OpportunityMethodResult.class, createUserMethodRequest(chartDataMethodRequest));
		
		assertEquals(0, result.getErrorCode());
		assertNotEquals(0, result.getOpportunity().getCurrentLevel());
	}
	@Test
	public void testGetFirstEverDepositCheck() throws Exception {
		FirstEverDepositCheckRequest firstEverDepositCheckRequest = new FirstEverDepositCheckRequest();
		firstEverDepositCheckRequest.setUserId(134320);
		FirstEverDepositCheckResult result = HttpClientPost.doPost(serviceUrl+"getFirstEverDepositCheck", FirstEverDepositCheckResult.class, firstEverDepositCheckRequest);
		assertEquals(0, result.getErrorCode());
		assert(!result.isFirstEverDeposit());
	}
	@Test
	public void testGetBankWireWithdrawOptions() throws Exception {
		UserMethodRequest userMethodRequest = new UserMethodRequest();
		for(Long skinId: skinIds) {
			userMethodRequest.setSkinId(skinId);
			OptionsMethodResult result = HttpClientPost.doPost(serviceUrl+"getBankWireWithdrawOptions", OptionsMethodResult.class, userMethodRequest);
			assertEquals(0, result.getErrorCode());
			assertNotNull(result.getOptions());
		}
	}
	@Test
	public void testGetAllReturnOdds() throws Exception {
		OpportunityOddsPairResult result = HttpClientPost.doPost(serviceUrl+"getAllReturnOdds", OpportunityOddsPairResult.class, new MethodRequest());
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getOpportunityOddsPair());
	}

	@Test
	public void testGetBinaryHomePageMarkets() throws Exception {
		MethodRequest methodRequest = new MethodRequest();
		for(Long skinId: skinIds) {
			methodRequest.setSkinId(skinId);
			MarketsMethodResult result = HttpClientPost.doPost(serviceUrl+"getBinaryHomePageMarkets", MarketsMethodResult.class, methodRequest);
			assertEquals(0, result.getErrorCode());
			assertNotNull(result.getMarkets());
		}
	}
	@Test
	public void testGetSkinCurrencies() throws Exception {
		MethodRequest methodRequest = new MethodRequest();
		for(Long skinId: skinIds) {
			methodRequest.setSkinId(skinId);		
			CurrenciesResult result = HttpClientPost.doPost(serviceUrl+"getSkinCurrencies", CurrenciesResult.class, methodRequest);
			assertEquals(0, result.getErrorCode());
			assertNotNull(result.getCurrency());
			assertNotNull(result.getPredefinedDepositAmountMap());
		}
	}
	
	@Test
	public void testGetLastCurrenciesRate() throws Exception {
		LastCurrenciesRateMethodResult result = HttpClientPost.doPost(serviceUrl+"getLastCurrenciesRate", LastCurrenciesRateMethodResult.class, createUserMethodRequest(new UserMethodRequest()));
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getLastCurrenciesRate());
	}
	
	@Test
	public void testGetLogoutBalanceStep() throws Exception {
		MethodRequest methodRequest = new MethodRequest();
		for(Long skinId: skinIds) {
			methodRequest.setSkinId(skinId);	
			LogoutBalanceStepMethodResult result = HttpClientPost.doPost(serviceUrl+"getLogoutBalanceStep", LogoutBalanceStepMethodResult.class, createUserMethodRequest(new UserMethodRequest()));
			assertEquals(0, result.getErrorCode());
			assertNotNull(result.getDefaultAmountValue());
			assertNotNull(result.getPredefValues());
			assertNotNull(result.getCurrencyId());
		}
	}
	@Test
	public void testGetMarketingUsers() throws Exception {
		MarketingUsersMethodRequest marketingUsersMethodRequest = new MarketingUsersMethodRequest();
		marketingUsersMethodRequest.setAffiliateKey(111111);
		marketingUsersMethodRequest.setDateRequest("2000-10-10");
		MarketingUsersMethodResult result = HttpClientPost.doPost(serviceUrl+"getMarketingUsers", MarketingUsersMethodResult.class, marketingUsersMethodRequest);
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getMarketingInfo());
	}
	
	@Test
	public void testGetUserDepositBonusBalance() throws Exception {
		DepositBonusBalanceMethodRequest depositBonusBalanceMethodRequest = new DepositBonusBalanceMethodRequest();
		depositBonusBalanceMethodRequest.setInvestmentId(28435857);
		DepositBonusBalanceMethodResult result = HttpClientPost.doPost(serviceUrl+"getUserDepositBonusBalance", DepositBonusBalanceMethodResult.class, createUserMethodRequest(depositBonusBalanceMethodRequest));
		assertEquals(0, result.getErrorCode());
		assertNotNull(result.getDepositBonusBalanceBase());
	}

	
	@Test
	public void testInsertDeleteCard() throws Exception {
		CardMethodRequest insertCardMethodRequest = new CardMethodRequest();
        CreditCard card = new CreditCard();
        card.setHolderId("No-Id");
        card.setCcNumber(new Long("4200000000000000"));
        card.setCcPass("123");
        card.setHolderName("Testi Testov");
		card.setExpMonth("06");
		card.setExpYear("17");
		card.setTypeId(CreditCardType.CC_TYPE_VISA);
		insertCardMethodRequest.setCard(card);
		
		CardMethodResult insertCardResult = HttpClientPost.doPost(serviceUrl+"insertCard", CardMethodResult.class, createUserMethodRequest(insertCardMethodRequest));
		assertEquals(0, insertCardResult.getErrorCode());
		assertNotNull(insertCardResult.getCard());
		
		CreditCardRequest deleteCreditCardMethodRequest = new CreditCardRequest();
		deleteCreditCardMethodRequest.setCardId((int) insertCardResult.getCard().getId());
		CardMethodResult deleteCreditCardResult = HttpClientPost.doPost(serviceUrl+"deleteCreditCard", CardMethodResult.class, createUserMethodRequest(deleteCreditCardMethodRequest));
		assertEquals(0, deleteCreditCardResult.getErrorCode());
		
	}

//// 	TODO
////	@Test
////	public void testInsertDepositCard() throws Exception {
////		CcDepositMethodRequest ccDepositMethodRequest = new CcDepositMethodRequest();
////		ccDepositMethodRequest.setCardId(cardId);
////		ccDepositMethodRequest.setAmount("10000");
////		TransactionMethodResult result = HttpClientPost.doPost(serviceUrl+"insertDepositCard", TransactionMethodResult.class, createUserMethodRequest(ccDepositMethodRequest));
////		assertEquals(0, result.getErrorCode());
////	}
////	@Test
////	public void testOptionPlusFlashSell() throws Exception {
////		MethodResult result = (MethodResult)HttpClientPost.doPost(serviceUrl+"optionPlusFlashSell", UserMethodResult.class, createUserMethodRequest());
////		assertEquals(0, result.getErrorCode());
////	}
////	@Test
////	public void testGetPrice() throws Exception {
////		OptionPlusMethodRequest optionPlusMethodRequest = new OptionPlusMethodRequest();
////		TODO - insert valid option plus investment
////		optionPlusMethodRequest.setInvestmentId(investmentId);
////		OptionPlusMethodResult result = HttpClientPost.doPost(serviceUrl+"getPrice", OptionPlusMethodResult.class, createUserMethodRequest(optionPlusMethodRequest));
////		assertEquals(0, result.getErrorCode());
////		assertNotNull(result.getPrice());
////		assertNotNull(result.getPriceTxt());
////	}
////	SendDepositReceipt
////	buyInsurances
////	insuranceBannerSeen
////	validateWithdrawBank
////	validateWithdrawCard
////	updateBonusStatus
////	checkBaroPayWithdrawAvailable
////	validateBaroPayWithdraw
////	updateMandatoryQuestionnaireDone
////	updateOptionalQuestionnaireDone
////	updateCapitalQuestionnaireDone
////	updateSingleQuestionnaireDone"
////	updateUserQuestionnaireAnswers
////	insertMarketingTrackerClick
////	validateCC
////	updateUserAdditionalField
////	insertLoginToken
////	deleteCreditCard
////	sendBankTransferDetailsUser
////	logoutUser
////	insertDirectDeposit
////	finishDirectDeposit
////	insertKnowledgeConfirm
////	unsuspendUserRequest
////	getData
////	insertBubbleInvestment
////	settleBubbleInvestment
////	isWithdrawalWeekendsAvailable
////	updateIsKnowledgeQuestion
////	retrieveUserDocuments
////	fireServerPixel
////	unblockUserForThreshold
////	insertDynamicsInvestment
////	sellDynamicsInvestment
////	sellAllDynamicsInvestments
////	insertWithdrawCancel
////	cancelInvestment
////	changeCancelInvestmentCheckboxState
////	cancelBubbleInvestment
}
