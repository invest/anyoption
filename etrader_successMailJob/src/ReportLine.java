
public class ReportLine {
	private String userId;
	private String mail;
	private String mailSent;
	private String skinId;
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getMailSent() {
		return mailSent;
	}
	public void setMailSent(String mailSent) {
		this.mailSent = mailSent;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getSkinId() {
		return skinId;
	}
	public void setSkinId(String skinId) {
		this.skinId = skinId;
	}

}
