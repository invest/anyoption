public class UserData{ //
	String marketDisplayName;
	long investmentsCount;

	public UserData(String market,long invCount){
		marketDisplayName = market;
		investmentsCount = invCount;
	}

	public long getInvestmentsCount(){
		return investmentsCount;
	}
	public String getMarketDisplayName(){
		return marketDisplayName;
	}
	public void setInvestmentsCount(long invCount){
		investmentsCount = invCount;
	}
	public void setMarketDisplayName(String market){
		marketDisplayName = market;
	}

	@Override
	public String toString() {
		return "UserData [marketDisplayName=" + marketDisplayName
				+ ", investmentsCount=" + investmentsCount + "]";
	}
	
	
}
