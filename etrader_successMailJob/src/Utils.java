
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Hashtable;
import java.util.MissingResourceException;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleDriver;
/**
 * console utils
 */
public class Utils {
	private static final Logger log = Logger.getLogger(Utils.class);

	private static boolean registered=false;
    private static Properties properties=null;
    public static String propFile;
    public static String EMAIL_USER = "";
	public static String EMAIL_PASS = "";

	public static final String CURRENCY_ILS = "ILS";
	public static final String CURRENCY_USD = "USD";
	public static final String CURRENCY_TRY = "TRY";
	public static final String CURRENCY_RUB = "RUB";
	public static final String CURRENCY_EUR = "EUR";


    public static Connection getConnection() throws SQLException{
   	 	if (!registered) {
   	 		DriverManager.registerDriver(new OracleDriver());
   	 		registered=true;
   	 	}
   	 	log.debug("getting db connection");

   	 	return DriverManager.getConnection(getProperty("db.url"),getProperty("db.user"),getProperty("db.pass"));

   }

    public static String getProperty(String key) {

		if (properties == null) {
			properties = new Properties();
			try {
				properties.load(new FileInputStream(propFile));
			} catch (Exception ex) {
				log.fatal("failed to load properties "+ex.toString());
				System.exit(1);
			}
		}

		if (key == null || key.trim().equals(""))
			return "";

		String text = null;
		try {
			text = properties.getProperty(key);
		} catch (MissingResourceException e) {
			return "";
		}

		return text;

	}

    public static void sendEmail(Hashtable server, Hashtable msg) {
		Properties props = new Properties();
		EMAIL_USER = (String) server.get("user");
		EMAIL_PASS = (String) server.get("pass");
		props.put("mail.smtp.host", (String) server.get("url"));
		props.put("mail.smtp.auth", "true");
		String subject = (String) msg.get("subject");
		String to = (String) msg.get("to");
		String from = (String) msg.get("from");
		String body = (String) msg.get("body");
		//  log.debug(body);     //For Test

		String[] splitTo = to.split(";");
		for (int i = 0; i < splitTo.length; i++) {
			sendSingleEmail(props, subject, splitTo[i], from, body, server);
		}
	}

    private static void sendSingleEmail(Properties props, String subject, String to, String from, String body, Hashtable server) {
		Session session = Session.getInstance(props, new ForcedAuthenticator());
		Message mess = new MimeMessage(session);

		try {
			// Set Headers
			mess.setHeader("From", from);
			mess.setHeader("To", to);
			mess.setHeader("Content-Type", "text/html; charset=utf-8");
			mess.setHeader("Content-Transfer-Encoding", "base64");

			mess.setFrom(new InternetAddress(from));
			mess.setSubject(subject);
			mess.setContent(body, "text/html; charset=utf-8");

			// Address[] address = { new InternetAddress(to) };
			// Transport trans = session.getTransport("smtp");
			// if (null != server.get("auth") && ((String)
			// server.get("auth")).equals("true")) {
			// trans.connect((String) server.get("url"), EMAIL_USER,
			// EMAIL_PASS);
			// } else {
			// trans.connect();
			// }
			Transport.send(mess);

		} catch (AddressException ae) {
			log.log(Level.FATAL, "Error Sending Mail to: " + to + "body: " + body, ae);
		} catch (MessagingException me) {
			log.log(Level.FATAL, "Error Sending Mail to: " + to + "body: " + body, me);
		}
	}

	static class ForcedAuthenticator extends Authenticator {
		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(EMAIL_USER,EMAIL_PASS);

		}
	}

	public static String displayAmount(double amount) {
		DecimalFormat sd = new DecimalFormat("###,###,##0.00");
		String out = "";
		out = sd.format(amount);

		return out;
	}

}