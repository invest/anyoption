import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.TemplatesDAO;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.JobUtil;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.swing.SwingUtilities;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import com.anyoption.common.beans.base.Skin;


public class SuccessRateMailJob extends JobUtil {
	private static Logger log = Logger.getLogger(SuccessRateMailJob.class);
	private static String fromDate;
	private static String tillDate;
	private static double d2r   = Math.PI / 180.0;
	private static int PAD = 12; // padding of percents from the circle's border
	private static ArrayList<Color> colorsArr = new ArrayList<Color>();
	private static Hashtable<String, String> serverProperties;
	private static Hashtable<String, String> emailProperties;
	private static Template mailTemplate;
	private static org.apache.velocity.Template reportTemplate;
	private static String EMAIL_USER = "";
	private static String EMAIL_PASS = "";
	private static String dateStr;
	private static SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	private static int MINIMUM_PERCENTS = 5;
	private static String unSuccessfullLog = new String("");
	private static String SENT = "OK";
	private static String NOT_SENT= "ERROR";
	private static String SKIN_ID = "";
	private static String uploadPath;
	private static boolean createIssue = true;
	private static boolean sendToMailBox = true;

	private static String IMAGE_PATH_ET = "picbase.etrader.co.il";
//	private static String IMAGE_PATH_AO = "picbase.anyoption.com";
	
//	private static Font fontArielUnicode;

	public static void main(String[] args) {
		Connection conn = null;
		// arguments are : config.file skin fromDate tillDate
		if (null == args || args.length < 1) {
			log.log(Level.FATAL, "Parameters not specified, insert properties file name.");
			System.exit(1);
		}
		try {
			propFile = args[0];
		} catch (Exception ex) {
			log.fatal("Could not load server properties " + ex.getMessage());
			ex.printStackTrace();
			System.exit(1);
		}
		
//		try{
//			//InputStream is = new FileInputStream(new File("Arial Unicode MS.ttf"));
//			InputStream is = SuccessRateMailJob.class.getResourceAsStream("Arial Unicode MS.ttf");
//			Font f = Font.createFont(Font.TRUETYPE_FONT, is);
//			if(f == null || f.getFamily().equals("Dialog")) {
//				throw new Exception("font not found");
//			}
//			fontArielUnicode = f.deriveFont((float)10);
//		} catch (Exception ex) {
//			log.fatal("Could not load system font" + ex.getMessage());
//			ex.printStackTrace();
//			System.exit(1);
//		}
		
		try {
			SKIN_ID = args[1];
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			SKIN_ID = getPropertyByFile("skinId");
		}
		// If the time range properties are not in the PropertyFile, the default is last month
		fromDate = getPropertyByFile("from", null);
		tillDate = getPropertyByFile("till", null);
		if (fromDate == null || tillDate == null) {
			fromDate = getFromLastMonth();
			tillDate = getTillLastMonth();
		}

		dateStr = fromDate.replace("/", "");
		uploadPath = "test";
		if (getPropertyByFile("sendTo").equalsIgnoreCase("LIVE")) {
			uploadPath = "live";
		}
		createIssue = Boolean.valueOf(getPropertyByFile("createIssue"));
		sendToMailBox = Boolean.valueOf(getPropertyByFile("sendToMailBox"));

		serverProperties = new Hashtable<String, String>();
		serverProperties.put("url", getPropertyByFile("email.url"));
		serverProperties.put("user", getPropertyByFile("email.user"));
		serverProperties.put("pass", getPropertyByFile("email.pass"));

		if (log.isEnabledFor(Level.INFO)) {
			log.log(Level.INFO, "Starting the job.");
		}
		//prepare colors table
		colorsArr.add(new Color(254,200,47));
		colorsArr.add(new Color(255,11,48));
		colorsArr.add(new Color(152,53,170));
		colorsArr.add(new Color(81,243,246));
		colorsArr.add(new Color(28,166,229));
		colorsArr.add(new Color(120,130,200));
		colorsArr.add(new Color(0,97,176));
		colorsArr.add(new Color(21,125,128));
		colorsArr.add(new Color(15,180,2));
		colorsArr.add(new Color(108,106,109));

		HashMap<String,ArrayList<UserData>> usersData = prepareData(); // hash with all the users (key- userId_skin_email)

		if(usersData != null && usersData.size() > 0) {
			if (log.isEnabledFor(Level.INFO)) {
				log.log(Level.INFO, "Starting image generation ." + new Date());
			}
			Set<Entry<String,ArrayList<UserData>>> entries = usersData.entrySet();
			Iterator<Map.Entry<String, ArrayList<UserData>>> itr = entries.iterator();
			while(itr.hasNext()){
				Map.Entry<String, ArrayList<UserData>> currEntry = (Map.Entry<String, ArrayList<UserData>>)itr.next();
				String currUserIdSkinMail = currEntry.getKey();
				ArrayList<UserData> currUserDetails = currEntry.getValue();
				if(Integer.parseInt(currUserIdSkinMail.split("#")[1]) == 1){ // ET user
					generateImage(currUserIdSkinMail,currUserDetails);
				} else {// got user from AO
					generateImageAO(currUserIdSkinMail,currUserDetails);
				}
			}
			// upload generated files to picbase FTP
			try {
				log.info("Sending over FTP...");
				uploadFiles(usersData);
			} catch(Exception e) {
				log.error("Error storing files on FTP..." + e.getMessage());
				System.exit(1); // do not send any mails
			}
			//afer done with uploading - initiate template with correct data and link to img on picbase
			itr = entries.iterator();
			while(itr.hasNext()){
				Map.Entry<String, ArrayList<UserData>> currEntry = (Map.Entry<String, ArrayList<UserData>>)itr.next();
				String currUserIdSkinMail = currEntry.getKey();
				UserBase user = new UserBase();
				user.setId(Long.valueOf(currUserIdSkinMail.split("#")[0]));
				user.setSkinId(Long.valueOf(currUserIdSkinMail.split("#")[1]));
				user.setUtcOffset(currUserIdSkinMail.split("#")[7]);
				user.setFirstName(currUserIdSkinMail.split("#")[3].substring(0,1).toUpperCase()+currUserIdSkinMail.split("#")[3].substring(1));
				user.setEmail(currUserIdSkinMail.split("#")[2]);
				user.setIsContactByEmail(Integer.valueOf(currUserIdSkinMail.split("#")[6]));
				user.setLanguageId(Long.valueOf(currUserIdSkinMail.split("#")[8]));

				HashMap<String,String> params = new HashMap<String, String>();
				String subj = getProperty("mail.subject", String.valueOf(user.getSkinId()));
				params.put("subject", user.getFirstName() +", " + subj);
				params.put("userFirstName", user.getFirstName());
				params.put("imgName","http://" + IMAGE_PATH_ET + "/pieGraph/" + uploadPath + "/" + dateStr + "/chart" + user.getId() + ".png"); //image name : chart+userId+fromDate+extension
				params.put("startDate", fromDate);
				params.put("endDate", tillDate);
				params.put("supportPhone", currUserIdSkinMail.split("#")[5]);
				String maxAssetName = new String (); // for holding asset with max
				maxAssetName = currEntry.getValue().get(0).getMarketDisplayName();
				params.put("assetname", maxAssetName);
				String sendTo = getPropertyByFile("sendTo");
				String to;
				if (!sendTo.equalsIgnoreCase("LIVE")) {
					to = getPropertyByFile("sendTo"); // send to tester's mail
				} else {
					to = user.getEmail(); // send to user itself
				}
				params.put("to", to);
				if (user.getSkinId() == Skins.SKIN_ETRADER) {
					params.put("from",getPropertyByFile("email.from.etrader"));
				} else if (user.getSkinId() == Skins.SKIN_CHINESE) {
				    params.put("from",getPropertyByFile("email.from.anyoption.zh"));
				} else if (user.getSkinId() == Skins.SKIN_KOREAN) {
                    params.put("from",getPropertyByFile("email.from.anyoption.kr")); 
			    } else {
					params.put("from",getPropertyByFile("email.from.anyoption"));
				}

				if(user.getIsContactByEmail() == 1){
					// send email only to users with IS_CONTACT_BY_EMAIL=1
					try {
						conn = getConnection();
						try{
							mailTemplate = TemplatesDAO.get(conn, Template.SUCCESS_MAIL_ID);
						}catch(Exception e){
							log.fatal("Error getting template !" + e.getMessage());
							System.exit(1); //do not send any mails
						}

						sendSingleEmail(conn, serverProperties, mailTemplate.getFileName(), params,
								mailTemplate.getSubject(), "success.mail.subject", createIssue, false, 0, user, null, mailTemplate.getId());

						if (user.getId() != 0 && user.getSkinId() != Skin.SKIN_ETRADER && sendToMailBox) {  // user contact
							try {
								UsersManagerBase.SendToMailBox(user, mailTemplate, Writer.WRITER_ID_AUTO,
										user.getLanguageId(), null, params, conn, 0);
							} catch (Exception e) {
								log.warn("Problem adding email to user inbox! userId: " + user.getId());
							}
						}
					} catch (Exception e) {
						log.log(Level.FATAL,"Can't get users " + e);
					} finally {
						try {
							conn.close();
						} catch (Exception e) {
							log.log(Level.ERROR,"Can't close",e);
						}
					}

					log.info("Mail sent to userID:" + user.getId());
				} else {
					unSuccessfullLog += user.getId() +";";
				}
			}
		}
		if (log.isEnabledFor(Level.INFO)) {
			log.log(Level.INFO, "Job completed." + new Date());
			log.log(Level.INFO,"There were errors sending mail to following users :" +unSuccessfullLog);
		}
		sendReport(usersData);
	}
	/**
	 * upload files to FTP picbase
	 * @param HashMap<String,ArrayList<UserData>> - all users data
	 * @throws Exception
	 */
	private static void uploadFiles(HashMap<String,ArrayList<UserData>> usersData)throws Exception {
		Set<String> keys = usersData.keySet();
		Iterator itr = keys.iterator();
		FTPClient ftp = new FTPClient();
		ftp.connect(IMAGE_PATH_ET);
		ftp.login("piemail", "Woj7q2H0l9");
		log.info("Connected to server .");
		log.info(ftp.getReplyString());
		ftp.changeWorkingDirectory(uploadPath);
		ftp.makeDirectory(dateStr);
		ftp.changeWorkingDirectory(dateStr);
		ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
		int i=0;
		while(itr.hasNext()){
			String currUserId = ((String)itr.next()).split("#")[0];
			String fileName = new String(getPropertyByFile("image.generate.path") + "chart" + currUserId + ".png");
			File file = new File(fileName);
			FileInputStream fis = new FileInputStream(file);
			ftp.storeFile ("chart" + currUserId + ".png",fis);
			log.info("chart" + currUserId + ".png("+i+")" + " was stored on FTP");
			i++;
		}
		ftp.disconnect();
		log.info("Files uploaded...FTP disconnected");
	}

	/**
	 * send report about the results of the job
	 * @param HashMap<String,ArrayList<UserData>> - all users data
	 * @throws Exception
	 */
	private static void sendReport(HashMap<String,ArrayList<UserData>> usersData){
		HashMap<String, Object> params = new HashMap<String, Object>();
		Set<Entry<String,ArrayList<UserData>>> entries = usersData.entrySet();
		ArrayList<ReportLine> reportData = new ArrayList<ReportLine>();
		if(unSuccessfullLog.length()>1){
			String[] unsuccessfullUsers = unSuccessfullLog.substring(0, unSuccessfullLog.length()-1).split(";");
			Map.Entry<String, ArrayList<UserData>> currEntry = null;
			if(unSuccessfullLog.length() > 0){
				for(String userId :unsuccessfullUsers){ // first we add all the users which didn't get mail
					ReportLine reportLine = new ReportLine();
					Iterator<Map.Entry<String, ArrayList<UserData>>> itr = entries.iterator();
					while(itr.hasNext()){
						Map.Entry<String, ArrayList<UserData>> currHashEntry = (Map.Entry<String, ArrayList<UserData>>)itr.next();
						String currUserIdSkinMail = currHashEntry.getKey();
						if(currUserIdSkinMail.split("#")[0].equals(userId)){
							currEntry = currHashEntry;
							break;
						}
					}
					String currUserIdSkinMail = currEntry.getKey();
					reportLine.setUserId(userId);
					reportLine.setMail(currUserIdSkinMail.split("#")[2]);
					reportLine.setSkinId(currUserIdSkinMail.split("#")[1]);
					reportLine.setMailSent(NOT_SENT);
					usersData.remove(currUserIdSkinMail); // remove user with error to get only users without errors at the end
					reportData.add(reportLine);
				}
			}
		}
		entries = usersData.entrySet();
		Iterator<Map.Entry<String, ArrayList<UserData>>> itr = entries.iterator();
		while(itr.hasNext()) { // add all the users that mail was sent to them
			ReportLine reportLine = new ReportLine();
			Map.Entry<String, ArrayList<UserData>> currEntry = (Map.Entry<String, ArrayList<UserData>>)itr.next();
			String currUserIdSkinMail = currEntry.getKey();
			reportLine.setUserId(currUserIdSkinMail.split("#")[0]);
			reportLine.setMail(currUserIdSkinMail.split("#")[2]);
			reportLine.setMailSent(SENT);
			reportLine.setSkinId(currUserIdSkinMail.split("#")[1]);
			reportData.add(reportLine);
		}
		params.put("reportData",reportData);
		params.put("totalUsers",reportData.size());
		try {
			reportTemplate = getEmailTemplate("successMailReport.html", null);
		} catch (Exception e) {
			log.fatal("!!! ERROR >> INIT report email: " + e.getMessage());
		}
		if (null == emailProperties) {
			emailProperties = new Hashtable<String, String>();
		    if (getPropertyByFile("skinId").equals("1")){
			    emailProperties.put("from",getPropertyByFile("email.from.etrader"));
		    } else {
			   emailProperties.put("from",getPropertyByFile("email.from.anyoption"));
		    }
		}
		emailProperties.put("subject",getPropertyByFile("report.subject"));
		try {
			emailProperties.put("body",getEmailBody(params));
		} catch (Exception e) {
			log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
		}

		emailProperties.put("to",getPropertyByFile("report.sendTo"));
		Utils.sendEmail(serverProperties,emailProperties);
		emailProperties.remove("to");
	}

	public static String getEmailBody(HashMap params) throws Exception {
		// set the context and the parameters
		VelocityContext context = new VelocityContext();
		// get all the params and add them to the context
		String paramName = "";
		log.debug("Adding parrams");
		for (Iterator keys = params.keySet().iterator(); keys.hasNext();) {
			paramName = (String) keys.next();
			context.put(paramName,params.get(paramName));
		}
		StringWriter sw = new StringWriter();
		reportTemplate.merge(context,sw);
		return sw.toString();
	}

	/**
	 * get the data from DB
	 * @throws Exception
	 */
	public static HashMap<String,ArrayList<UserData>> prepareData() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = new String();
		HashMap<String,ArrayList<UserData>> userData = new HashMap<String,ArrayList<UserData>>(); //
		ArrayList<UserData> marketsAndInvs = new ArrayList<UserData>();
		try {
			conn = getConnection();
			String userId = getPropertyByFile("user.id", "");
						
			//first, get all the records
			sql ="SELECT " +
					"userid,mns.name market_name, " +
				 	"investmentscount, " +
                    "users.skin_id, " +
				 	"users.email, " +
                    "users.first_name, " +
                    "users.time_created, " +
				 	"countries.support_phone, " +
                    "users.is_contact_by_email, " +
				 	"users.utc_offset, " +
                    "users.language_id, " +
                    "markets.option_plus_market_id " +
				 "FROM " +
				 	"TABLE(PipelinePieData(to_date('" + fromDate + "','DD/MM/YYYY'),to_date('" + tillDate + "','DD/MM/YYYY')))," +
					"users, " +
					"markets, " +
					"countries, " +
					"market_name_skin mns, " +
					"skin_market_groups smg, " +
			        "skin_market_group_markets smgm " +
				 "WHERE " +
				 	"userid = users.id AND " +
				 	"marketid = markets.id AND " +
				 	"users.country_id = countries.id AND " +
				 	"users.skin_id = " + SKIN_ID + " AND " +
				 	"markets.id = mns.market_id AND " +
				 	"users.skin_id = mns.skin_id AND " +
				 	"users.is_contact_by_email = 1 AND " +
				 	"smg.skin_id = users.skin_id AND ";
			//run on specific user			
			if (!CommonUtil.isParameterEmptyOrNull(userId) ) {
					sql+=	" userid = ? AND ";
			}	 	
			sql+= 	"smgm.skin_market_group_id = smg.id AND " +
			        "smgm.market_id = markets.id " +
				 "ORDER BY " +
				 	"users.skin_id, " +
				 	"userid, " +
				 	"investmentsCount desc";
			pstmt = conn.prepareStatement(sql);
			if (!CommonUtil.isParameterEmptyOrNull(userId) ) {
				pstmt.setString(1, userId);
			}
			rs = pstmt.executeQuery();
			String prevUserId = new String("");
			String currUserId = new String("");
			long skinId;
			while (rs.next()){ // get the data for each user
				skinId = rs.getLong("skin_id");
				String userCreated = fmt.format(rs.getTimestamp("time_created"));
				currUserId = rs.getString("userid") + "#" +skinId + "#" + rs.getString("email") +
					"#" + rs.getString("first_name") + "#"+userCreated+"#" + rs.getString("support_phone") +
					"#" + rs.getString("is_contact_by_email") + "#" + rs.getString("utc_offset") +
					"#" + rs.getString("language_id");
				if(currUserId.equals(prevUserId) || prevUserId.equals("")){ //we are still having the rows for current user
					marketsAndInvs.add(new UserData(rs.getString("market_name") + (rs.getLong("option_plus_market_id") > 0 ? " +" : ""), rs.getLong("investmentscount")));
				} else {
					// uncomment break - send just to the first user
					//break;
					userData.put(prevUserId,marketsAndInvs);
					marketsAndInvs = new ArrayList<UserData>();
					marketsAndInvs.add(new UserData(rs.getString("market_name") + (rs.getLong("option_plus_market_id") > 0 ? " +" : ""), rs.getLong("investmentscount")));
				}
				prevUserId = currUserId;
			}
			if (currUserId.length() > 0) {
				userData.put(currUserId, marketsAndInvs);
			}
			log.log(Level.INFO, "Size is :" + userData.size() + " users");
			return userData;
		} catch (Exception e) {
			log.error("Error in SuccessRateMailJob" + e.getMessage());
			System.exit(1);
		} finally {
			try {
				conn.close();
			} catch(Exception e){
				log.error("Can't close connection..." + e.getMessage());
				System.exit(1);
			}
		}
		return null;
	}

	/**
	 * get name of the market from the properties file(localized)
	 * @param property - market display name
	 * @param skinId - skin
	 * @throws Exception
	 */
	public static String getProperty(String property, String skinId){
		try {
			Locale locale = new Locale("en");
			if (skinId.equals("3")) {
				locale = new Locale("tr");
			} else if (skinId.equals("5")) {
				locale = new Locale("es");
			} else 	if (skinId.equals("2") || skinId.equals("7")) {
				locale = new Locale("en");
			} else if (skinId.equals("1")) {
				locale = new Locale("iw");
			} else if (skinId.equals("4")) {
				locale = new Locale("ar");
			} else if (skinId.equals("8")) {
				locale = new Locale("de");
			} else if (skinId.equals("9")) {
				locale = new Locale("it");
			} else if (skinId.equals("10")) {
				locale = new Locale("ru");
			} else if (skinId.equals("12")) {
				locale = new Locale("fr");
			} else if (skinId.equals("13")) {
				locale = new Locale("en");
			} else if (skinId.equals("14")) {
				locale = new Locale("es");
			} else if (skinId.equals("15")) {
				locale = new Locale("zh");
			}
			ResourceBundle bundle = ResourceBundle.getBundle("successMailUTF", locale);
			return bundle.getString(property);
		} catch(Exception e){
			log.error("Error getting property for: " + property + " " + e.getMessage());
			return property;
		}
	}

	/**
	 * generate chart image (used for ET)
	 * @param userSkinMail - user's id,skinId,and mail adress separated by "#"
	 * @param ArrayList<UserData> userData - user's investments
	 * @throws Exception
	 */
	public static void generateImage(String userSkinMail,ArrayList<UserData> userData) {
		int skinId = Integer.parseInt(userSkinMail.split("#")[1]);
		int displayWidth = 460;
		int displayHeight = 281;
		BufferedImage bufferedImage = new BufferedImage(displayWidth, displayHeight, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = bufferedImage.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		g.setColor(new Color(255, 255, 255));
		g.fillRect(0, 0, displayWidth, displayHeight);
		g.setPaint(Color.black);
		g.drawRect(1, 1, displayWidth-2, displayHeight-2);
		Font font = new Font("Arial",0,10);
		font = font.deriveFont(new Float(10));
		g.setFont(font);
//		g.setFont(fontArielUnicode);

		int pie = displayHeight - 4*PAD;
		double total = 0;
		int loopLimit = userData.size() >= 10 ? 10 : userData.size();
		for(int j = 0; j < loopLimit; j++){
			total += userData.get(j).getInvestmentsCount();
		}

		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setMaximumFractionDigits(1);

		double sum = 0;
		// first we need to refine data for the low percentages

		ArrayList<UserData> reducedUserData = new ArrayList<UserData>();
		int tempSum = 0;
		float tempPercentSum = 0;
		int stopIndex =0;

		for (int i = loopLimit-1; i > -1; i--) {
			if (tempPercentSum + userData.get(i).getInvestmentsCount()/total*100 < MINIMUM_PERCENTS && (tempPercentSum + userData.get(i).getInvestmentsCount())/total*100>0){
					tempPercentSum += userData.get(i).getInvestmentsCount()/total*100;
					tempSum += userData.get(i).getInvestmentsCount();
					// we have finished with small percentages and have to finish the pass on array from where we stopped
			} else {
				stopIndex = i;
				break;
			}
		}

		UserData tmpUserData = new UserData(getProperty("markets.other", SKIN_ID),tempSum);
		if (tempSum >0 ){
			reducedUserData.add(tmpUserData);
		}
		reducedUserData.add(userData.get(stopIndex));
		for(int i=stopIndex-1;i>-1;i--)	{
			reducedUserData.add(userData.get(i));
		}

		int[] angles = new int[reducedUserData.size()+1];
		angles[0] = 0;
		angles[reducedUserData.size()] = 360;
		sum=0;

		loopLimit = reducedUserData.size() >= 10 ? 10 : reducedUserData.size();
		//reverse the array to get more logical graph
		ArrayList<UserData> reverseReducedUserData = new ArrayList<UserData>();
		for(int i=reducedUserData.size()-1;i>-1;i--){
			reverseReducedUserData.add(reducedUserData.get(i));
		}
		reducedUserData = reverseReducedUserData;

		for (int i = 0; i < loopLimit; i++) {
			sum += reducedUserData.get(i).getInvestmentsCount();  // Add in next data value.
			angles[i+1] = (int)(360*sum/total);
		}

		float totalPercents=0;
		for(int j = 0; j < loopLimit; j++) {
			g.setPaint(colorsArr.get(j));
			totalPercents+=Float.parseFloat(numberFormat.format(reducedUserData.get(j).getInvestmentsCount()/total*100));
			g.fillArc( 15+PAD*2,PAD+10, pie, pie,angles[j], angles[j+1] - angles[j] );//draw the pie
		}

		String percentStr = new String();
		double tempPercent = 0;
		for(int j = 0; j <loopLimit; j++){
			if(j==0){ // first member of the array holds maximum investments - we can subtract or add 1 to get 100% exactly
				tempPercent = reducedUserData.get(j).getInvestmentsCount()/total*100 + (100 - totalPercents);
			}else{
				tempPercent = ((reducedUserData.get(j).getInvestmentsCount()/total)*100);
			}
			percentStr = numberFormat.format(tempPercent) + "%";
			drawLabel(g, percentStr, angles[j] + ((angles[j+1] - angles[j]) / 2),pie/2); // draw percent labels
		}
		drawLegend(g,reducedUserData,String.valueOf(skinId));
		byte[] ba = null;
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(bufferedImage, "png", baos);
			baos.flush();
			ba = baos.toByteArray();
			baos.close();
			FileOutputStream fos = new FileOutputStream(getPropertyByFile("image.generate.path") + "chart" + userSkinMail.split("#")[0] + ".png");
			fos.write(ba);
			fos.flush();
			fos.close();
		} catch (Throwable t) {
			log.error("Problem creating the image.", t);
			System.exit(1);
		}
	}

	/**
	 * draws labels with percents
	 * @param g  - graphics2D object
	 * @param text - percent
	 * @param angle - for placement
	 * @param radius - for placement
	 * @throws Exception
	 */
	public static void drawLabel(Graphics2D g, String text, int angle,int radius) {
		g.setColor(Color.BLACK);
		//need to check the text if its value is lower than 1.5 percent
		//Float ft = Float.parseFloat(text.substring(0,text.indexOf("%")));
		double radians = angle * d2r;
		int offset;
		int x = (int) ((radius + PAD/2) * Math.cos(radians));
		if(angle > 270 &&  angle < 360){ // need to take care of labels on the right side of the graph
			offset = 4;
			if(angle > 355 &&  angle < 360)
				offset = -5;
		} else {
			offset = 0;
		}
		int y = (int) ((radius + PAD/2) * Math.sin(radians))+offset;
		if (x < 0) {
			x -= SwingUtilities.computeStringWidth(g.getFontMetrics(), text);
		}
		if (y < 0 && !(angle > 355 && angle < 360)) {
			y -= (g.getFontMetrics().getHeight());
		} else { //small angle,small percents

		}
		g.drawString(text, x + 155, 135 - y);
	}

	/**
	 * draws legend (used for ET)
	 * @param g  - graphics2D object
	 * @param userMarkets - all user's market
	 * @param skinId - for localization
	 */
	public static void drawLegend(Graphics2D g,ArrayList<UserData> userMarkets,String skinId){
		String marketName = new String("");
		int loopLimit = userMarkets.size() >= 10 ? 10 : userMarkets.size();
		for(int i=0;i<loopLimit;i++) {
			marketName = new String(userMarkets.get(i).getMarketDisplayName());
			g.setColor(colorsArr.get(i));
			g.fill3DRect(325, 25 + (15*i), 5, 5,true);
			g.setColor(Color.BLACK);
			g.drawString(marketName, 333, 30 + (15*i));
		}
	}

	private static org.apache.velocity.Template getEmailTemplate(String fileName, String skinId) throws Exception {
		try {
			Properties props = new Properties();
			props.setProperty("file.resource.loader.path",getPropertyByFile("templates.path"));
			props.setProperty("resource.loader","file");
			props.setProperty("file.resource.loader.class","org.apache.velocity.runtime.resource.loader.FileResourceLoader");
			props.setProperty("file.resource.loader.cache","true");
			VelocityEngine v = new VelocityEngine();
			v.init(props);
			return v.getTemplate(fileName,"UTF-8");
		} catch (Exception ex) {
			log.fatal("ERROR! Cannot find Report template : " + ex.getMessage());
			throw ex;
		}
	}

	static class ForcedAuthenticator extends Authenticator {
		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(EMAIL_USER,EMAIL_PASS);
		}
	}

	/**
	 * generate chart image (used for AO)
	 * @param userSkinMail - user's id,skinId,and mail adress separated by "#"
	 * @param ArrayList<UserData> userData - user's investments
	 * @throws Exception
	 */
	public static void generateImageAO(String userSkinMail,ArrayList<UserData> userData){
		int skinId = Integer.parseInt(userSkinMail.split("#")[1]);
		int displayWidth = 460;
		int displayHeight = 281;
		BufferedImage bufferedImage = new BufferedImage(displayWidth, displayHeight, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = bufferedImage.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		g.setColor(new Color(255, 255, 255));
		g.fillRect(0, 0, displayWidth, displayHeight);
		g.setPaint(Color.black);
		g.drawRect(1, 1, displayWidth-2, displayHeight-2);
		Font font = null;
		if (skinId == Skin.SKIN_ARABIC) {
			font = new Font("Arial",0,12);
		} else {
			font = new Font("Verdana",0,10);
		}
		font = font.deriveFont(new Float(10));
		g.setFont(font);
		int pie = displayHeight - 4*PAD;
		double total = 0;
		int loopLimit = userData.size() >= 10 ? 10 : userData.size();
		for(int j = 0; j < loopLimit; j++){
			total += userData.get(j).getInvestmentsCount();
		}

		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setMaximumFractionDigits(1);

		double sum = 0;
		// first we need to refine data for the low percentages

		ArrayList<UserData> reducedUserData = new ArrayList<UserData>();
		int tempSum = 0;
		float tempPercentSum = 0;
		int stopIndex =0;

		for (int i = loopLimit-1; i > -1; i--) {
			if (tempPercentSum + userData.get(i).getInvestmentsCount()/total*100 < MINIMUM_PERCENTS && (tempPercentSum + userData.get(i).getInvestmentsCount())/total*100>0){
					tempPercentSum += userData.get(i).getInvestmentsCount()/total*100;
					tempSum += userData.get(i).getInvestmentsCount();
					// we have finished with small percentages and have to finish the pass on array from where we stopped
			} else {
				stopIndex=i;
				break;
			}
		}

		UserData tmpUserData = new UserData(getProperty("markets.other", SKIN_ID),tempSum);
		if(tempSum >0 ){
			reducedUserData.add(tmpUserData);
		}
		reducedUserData.add(userData.get(stopIndex));
		for(int i=stopIndex-1;i>-1;i--) {
			reducedUserData.add(userData.get(i));
		}

		int[] angles = new int[reducedUserData.size()+1];
		angles[0] = 0;
		angles[reducedUserData.size()] = 360;
		sum=0;

		loopLimit = reducedUserData.size() >= 10 ? 10 : reducedUserData.size();
		//reverse the array to get more logical graph
		ArrayList<UserData> reverseReducedUserData = new ArrayList<UserData>();
		for(int i=reducedUserData.size()-1;i>-1;i--){
			reverseReducedUserData.add(reducedUserData.get(i));
		}
		reducedUserData = reverseReducedUserData;

		for (int i = 0; i < loopLimit; i++) {
			sum += reducedUserData.get(i).getInvestmentsCount();  // Add in next data value.
			angles[i+1] = (int)(360*sum/total);
		}

		float totalPercents = 0;
		for(int j = 0; j < loopLimit; j++) {
			g.setPaint(colorsArr.get(j));
			totalPercents+=Float.parseFloat(numberFormat.format(reducedUserData.get(j).getInvestmentsCount()/total*100));
			g.fillArc( 145+PAD*2,PAD+10, pie, pie,angles[j], angles[j+1] - angles[j] );//draw the pie
		}

		String percentStr = new String();
		double tempPercent = 0;
		for(int j = 0; j <loopLimit; j++) {
			if(j==0){ // first member of the array holds maximum investments - we can subtract or add 1 to get 100% exactly
				tempPercent = reducedUserData.get(j).getInvestmentsCount()/total*100 + (100 - totalPercents);
			} else {
				tempPercent = ((reducedUserData.get(j).getInvestmentsCount()/total)*100);
			}
			percentStr = numberFormat.format(tempPercent) + "%";
			drawLabelAO(g, percentStr, angles[j] + ((angles[j+1] - angles[j]) / 2),pie/2); // draw percent labels
		}
		drawLegendAO(g,reducedUserData,String.valueOf(skinId));
		byte[] ba = null;
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(bufferedImage, "png", baos);
			baos.flush();
			ba = baos.toByteArray();
			baos.close();
			FileOutputStream fos = new FileOutputStream(getPropertyByFile("image.generate.path") + "chart"+userSkinMail.split("#")[0] +  ".png");
			fos.write(ba);
			fos.flush();
			fos.close();
		} catch (Throwable t) {
			log.error("Problem creating the image.", t);
			System.exit(1);
		}
	}

	/**
	 * draws labels with percents
	 * @param g  - graphics2D object
	 * @param text - percent
	 * @param angle - for placement
	 * @param radius - for placement
	 * @throws Exception
	 */
	public static void drawLabelAO(Graphics2D g, String text, int angle,int radius) {
		g.setColor(Color.BLACK);
		//need to check the text if its value is lower than 1.5 percent
		double radians = angle * d2r;
		int offset;
		int x = (int) ((radius + PAD/2) * Math.cos(radians));
		if(angle > 270 &&  angle < 360){ // need to take care of labels on the right side of the graph
			offset = 4;
			if(angle > 355 &&  angle < 360)
				offset = -5;
		}else{
			offset = 0;
		}
		int y = (int) ((radius + PAD/2) * Math.sin(radians))+offset;

		if (x < 0) {
			x -= SwingUtilities.computeStringWidth(g.getFontMetrics(), text);
		}
		if (y < 0 && !(angle > 355 && angle < 360)) {
			y -= (g.getFontMetrics().getHeight());
		}else{ //small angle,small percents

		}
		g.drawString(text, x + 285, 135 - y);

	}

	/**
	 * draws legend (used for AO)
	 * @param g  - graphics2D object
	 * @param userMarkets - all user's market
	 * @param skinId - for localization
	 */
	public static void drawLegendAO(Graphics2D g,ArrayList<UserData> userMarkets,String skinId){
		String marketName = new String("");
		int loopLimit = userMarkets.size() >= 10 ? 10 : userMarkets.size();
		for(int i=0;i<loopLimit;i++) {
			marketName = new String(userMarkets.get(i).getMarketDisplayName());
			g.setColor(colorsArr.get(i));
			g.fill3DRect(15, 25 + (15*i), 5, 5,true);
			g.setColor(Color.BLACK);
			g.drawString(marketName, 25, 30 + (15*i));
		}
	}

	private static String getFromLastMonth() {

		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		gc.add(GregorianCalendar.MONTH, -1);
		gc.set(GregorianCalendar.DAY_OF_MONTH, 1);
		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");

		return sd.format(gc.getTime());
	}

	private static String getTillLastMonth() {

		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		gc.set(GregorianCalendar.DAY_OF_MONTH, 1);
		gc.add(GregorianCalendar.DAY_OF_MONTH, -1);
		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");

		return sd.format(gc.getTime());
	}
}