package com.anyoption.autotest.openaccount;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.common.User.Skin;
import com.anyoption.autotest.pages.BinaryOptionsPage;
import com.anyoption.autotest.pages.DynamicsPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.OpenAccountPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ChangeUrl;
import com.anyoption.autotest.utils.CreateUser;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class OpenAccountFromDynamics extends BaseTest {

	public OpenAccountFromDynamics() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/CreateAccountTestData.xlsx", numberRows, 5);
	    return(retObjArr);
	}
	/**
	 * Opens new account from dynamics
	 * @param skin
	 * @param openAccount
	 * @param myAccountTitle
	 * @param firstMessage
	 * @param secondMessage
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void openAccountFromDynamics(String skin, String openAccount, String myAccountTitle, String firstMessage, 
			String secondMessage) throws Exception {
		parameterSkin = skin;
		if (! skin.equals("EN")) {
			ChangeUrl changeUrl = new ChangeUrl();
			String testUrlNow = changeUrl.changeUrl(testUrl, skin);
			driver.get(testUrlNow);
		}	
		OpenAccountPage openAccountPage = new OpenAccountPage(driver, browserType);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		BinaryOptionsPage binaryOptionsPage = new BinaryOptionsPage(driver);
		DynamicsPage dynamicsPage = new DynamicsPage(driver);
		binaryOptionsPage.openOptionTrading();
		dynamicsPage.openDynamics();
		if (dynamicsPage.isDynamicsTutorialDisplayed() == true) {
			dynamicsPage.closeTutorial();
		}
		dynamicsPage.chooseMarket();
		dynamicsPage.clickUpButton();
		dynamicsPage.openAccount();
		Assert.assertEquals(openAccount, openAccountPage.getOppenAccountWindowTitle().toUpperCase());
		CreateUser user = new CreateUser();
		String firstName = user.getFirstName();
		String lastName = user.getLastName();
		String email = user.getEMail();
		String phone = user.getPhone();
		String pass = "123456";
		openAccountPage.selectCountry();
		openAccountPage.typeFirstName(firstName);
		openAccountPage.typeSecondName(lastName);
		openAccountPage.typeEmail(email);
		openAccountPage.typePhone(phone);
		openAccountPage.typePassword(pass);
		openAccountPage.typeConfirmPassword(pass);
		openAccountPage.clickTermAndConditionsCheckBox();
		if (! browserType.equalsIgnoreCase("firefox") || ! operationSys.equals("Linux")) {
			Assert.assertTrue(openAccountPage.isNamesOk());
			Assert.assertTrue(openAccountPage.isEmailOk());
			Assert.assertTrue(openAccountPage.isPhoneOk());
			Assert.assertTrue(openAccountPage.isPasswordOk());
			Assert.assertTrue(openAccountPage.isConfirmPassOk());
		}	
		Assert.assertEquals(true, openAccountPage.isCheckedTermAndCond());
		openAccountPage.clickOpenAccountButton();
		if (! testUrl.contains("bgtestenv")) {
			driver.navigate().refresh();
		}
		Assert.assertEquals(myAccountTitle, openAccountPage.getMyAccountFormTitle());
		Skin sk = UtilsMethods.getSkin(skin);
		User.compareUserDataAfterCreationOfAccount(email, firstName, lastName, sk);
		Assert.assertTrue(headerMenuPage.getBalance().contains("0.00"));
		Assert.assertEquals(firstMessage, openAccountPage.getNewDepositFirstMessage());
		Assert.assertEquals(secondMessage, openAccountPage.getNewDepositSecondMessage());
		User.compareContactsDataAfterCreationOfAccount(email, firstName, lastName, sk, phone);
	}
}
