package com.anyoption.autotest.openaccount;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.OpenAccountPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ChangeUrl;
import com.anyoption.autotest.utils.CreateUser;
import com.anyoption.autotest.utils.ReadTestData;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class OpenAccWithIncorrectDataTest extends BaseTest {

	public OpenAccWithIncorrectDataTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/OpenAccWithIncorrectDataTestData.xlsx", numberRows, 13);
	    return(retObjArr);
	}
	/**
	 * Tries to open new account with incorrect data
	 * @param skin
	 * @param mandatoryMess
	 * @param termsMess
	 * @param lettersToolTip
	 * @param numberTooltip
	 * @param invNumberTooltip
	 * @param phoneMess
	 * @param passTooltip
	 * @param passMess
	 * @param retypeMess
	 * @param emailErr
	 * @param invEmail
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void openAccWithIncorrectDataTest(String skin, String mandatoryMess, String termsMess, String lettersToolTip, 
			String numberTooltip, String invNumberTooltip, String phoneMess, String passTooltip, String passMess, String retypeMess, 
			String emailErr, String emailTooltip, String invEmail) throws Exception {
		parameterSkin = skin;
		if (! skin.equals("EN")) {
			ChangeUrl changeUrl = new ChangeUrl();
			String testUrlNow = changeUrl.changeUrl(testUrl, skin);
			driver.get(testUrlNow);
		}
		CreateUser user = new CreateUser();
		OpenAccountPage openAccountPage = new OpenAccountPage(driver, browserType);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		openAccountPage.clickOpenAccMenu();
		openAccountPage.selectCountry();
		openAccountPage.clickOpenAccountButton();
		Assert.assertTrue(openAccountPage.isNamesErrorDisplayed());
		Assert.assertEquals(mandatoryMess, openAccountPage.getFirstNameError());
		Assert.assertEquals(mandatoryMess, openAccountPage.getSecondNameError());
		Assert.assertTrue(openAccountPage.isPhoneErrorDisplayed());
		Assert.assertEquals(mandatoryMess, openAccountPage.getPhoneError());
		Assert.assertTrue(openAccountPage.isPasswordErrorDisplayed());
		Assert.assertEquals(mandatoryMess, openAccountPage.getPasswordErrorText());
		Assert.assertTrue(openAccountPage.isConfirmPassErrorDisplayed());
		Assert.assertEquals(mandatoryMess, openAccountPage.getConfirmPassError());
		Assert.assertTrue(openAccountPage.isTermsErrorDisplayed());
		Assert.assertTrue(openAccountPage.isEmailErrorDisplayed());
		Assert.assertEquals(termsMess, openAccountPage.getTermsError());
		openAccountPage.setFirstName("10");
		Assert.assertEquals(lettersToolTip, openAccountPage.getNamesTooltip());
		openAccountPage.setFirstName(user.getFirstName());
		Assert.assertEquals("", openAccountPage.getNamesTooltip());
		openAccountPage.setSecondName("12");
		Assert.assertEquals(lettersToolTip, openAccountPage.getNamesTooltip());
		openAccountPage.setSecondName(user.getLastName());
		Assert.assertEquals("", openAccountPage.getNamesTooltip());
		openAccountPage.clickOpenAccountButton();
		Assert.assertFalse(openAccountPage.isNamesErrorDisplayed());
		Assert.assertEquals("", openAccountPage.getFirstNameError());
		Assert.assertEquals("", openAccountPage.getSecondNameError());
		if (! browserType.equalsIgnoreCase("firefox")) {
			Assert.assertEquals(numberTooltip, openAccountPage.getPhoneTooltip(0));
			openAccountPage.setPhone("t");
			Assert.assertEquals(invNumberTooltip, openAccountPage.getPhoneTooltip(1));
		}
		openAccountPage.setPhone("123456");
		openAccountPage.clickOpenAccountButton();
		Assert.assertTrue(openAccountPage.isPhoneErrorDisplayed());
		Assert.assertEquals(phoneMess, openAccountPage.getPhoneError());
		openAccountPage.setPhone(user.getPhone());
		openAccountPage.clickOpenAccountButton();
		Assert.assertFalse(openAccountPage.isPhoneErrorDisplayed());
		Assert.assertEquals("", openAccountPage.getPhoneError());
		if (! browserType.equalsIgnoreCase("firefox")) {
			Assert.assertEquals(passTooltip, openAccountPage.getPasswordTooltip());
		}
		openAccountPage.setPassword("12345");
		openAccountPage.clickOpenAccountButton();
		Assert.assertTrue(openAccountPage.isPasswordErrorDisplayed());
		Assert.assertEquals(passMess, openAccountPage.getPasswordErrorText());
		openAccountPage.setPassword("123456");
		openAccountPage.clickOpenAccountButton();
		Assert.assertFalse(openAccountPage.isPasswordErrorDisplayed());
		Assert.assertEquals("", openAccountPage.getPasswordErrorText());
		openAccountPage.setConfirmPassword("12345");
		Assert.assertTrue(openAccountPage.isConfirmPassErrorDisplayed());
		if (! browserType.equalsIgnoreCase("firefox")) {
			Assert.assertEquals(retypeMess, openAccountPage.getConfirmPassError());
		}	
		openAccountPage.setConfirmPassword("123456");
		Assert.assertFalse(openAccountPage.isConfirmPassErrorDisplayed());
		Assert.assertEquals("", openAccountPage.getConfirmPassError());
		openAccountPage.clickTermAndConditionsCheckBox();
		openAccountPage.clickOpenAccountButton();
		Assert.assertFalse(openAccountPage.isTermsErrorDisplayed());
		
		Assert.assertEquals(mandatoryMess, openAccountPage.getEmailError());
		if (! browserType.equalsIgnoreCase("firefox")) {
			Assert.assertEquals(emailTooltip, openAccountPage.getEmailTooltip());
		}	
		openAccountPage.setEmailWithClick(user.getFirstName() + "@");
		openAccountPage.clickOpenAccountButton();
		Assert.assertTrue(openAccountPage.isEmailErrorDisplayed());
		if (! browserType.equalsIgnoreCase("firefox")) {
			Assert.assertEquals(invEmail, openAccountPage.getEmailError());
		}	
		openAccountPage.setEmailWithClick(user.getEMail());
		Assert.assertTrue(openAccountPage.isNamesOk());
		if (! browserType.equalsIgnoreCase("firefox")) {
			Assert.assertTrue(openAccountPage.isEmailOk());
		}	
		Assert.assertTrue(openAccountPage.isPhoneOk());
		Assert.assertTrue(openAccountPage.isPasswordOk());
		Assert.assertTrue(openAccountPage.isConfirmPassOk());
		Assert.assertTrue(openAccountPage.isTermsOK());
		openAccountPage.clickOpenAccountButton();
		Assert.assertEquals(user.getFirstName(), headerMenuPage.getUserNameFromHeader());
	}
}
