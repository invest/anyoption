package com.anyoption.autotest.tests;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.AssertJUnit;

import com.anyoption.autotest.common.Browser;
import com.anyoption.autotest.common.DBLayer;
import com.anyoption.autotest.common.User;
import com.anyoption.autotest.utils.SpreadsheetData;

@RunWith(value = Parameterized.class)
public class AO_DepositWithNewCard {

	private static WebDriver driver;

	private static String baseUrl;
	private static StringBuffer verificationErrors = new StringBuffer();

	static DBLayer dB = new DBLayer();

	private String skin;
	private String bgTestEnvURL;
	private String testEnvURL;
	private String liveEnvURL;
	private String skinId;
	private String languageId;
	private String termsLinkQuickSignUpForm;
	private String termsLinkRegistratrationForm;
	private String termsAndConditions;
	private String depositPageTitleWait;
	private String depositPageTitleAfterCreationOfAccount;
	private String depositPageTitle;
	private String newDeposith1;
	private String newDeposith2;
	private String userGreeting;
	private String userBalance;
	private String titleAfterLogoutWait;
	private String titleAfterLogout;
	private String registerPageTitle;
	private String contactsAllOthers;
	private String phoneNumber;
	private String depositAmount;
	private String loginButton;
	private String logoutButton;
	private String regulationQuestPageTitle;
	private String submitButtonText;
	private String thankYouText;
	private String documentsPageTitle;
	private String idText;
	private String fileUploadedText;

	@Parameters
	public static Collection<?> testData() throws Exception {
		InputStream spreadsheet = new FileInputStream(
				"C:\\Users\\tsvetelinad\\Desktop\\NewSkinInfo.xlsx");
		return new SpreadsheetData(spreadsheet).getData();
	}

	public AO_DepositWithNewCard(String skin, String bgTestEnvURL,
			String testEnvURL, String liveEnvURL, String skinId,
			String languageId, String termsLinkQuickSignUpForm,
			String termsLinkRegistratrationForm, String termsAndConditions,
			String depositPageTitleWait,
			String depositPageTitleAfterCreationOfAccount,
			String depositPageTitle, String newDeposith1, String newDeposith2,
			String userGreeting, String userBalance,
			String titleAfterLogoutWait, String titleAfterLogout,
			String registerPageTitle, String contactsAllOthers,
			String phoneNumber, String depositAmount, String loginButton,
			String logoutButton, String regulationQuestPageTitle,
			String submitButtonText, String thankYouText,
			String documentsPageTitle, String idText, String fileUploadedText) {
		this.skin = skin;
		this.bgTestEnvURL = bgTestEnvURL;
		this.testEnvURL = testEnvURL;
		this.liveEnvURL = liveEnvURL;
		this.skinId = skinId;
		this.languageId = languageId;
		this.termsLinkQuickSignUpForm = termsLinkQuickSignUpForm;
		this.termsLinkRegistratrationForm = termsLinkRegistratrationForm;
		this.termsAndConditions = termsAndConditions;
		this.depositPageTitleWait = depositPageTitleWait;
		this.depositPageTitleAfterCreationOfAccount = depositPageTitleAfterCreationOfAccount;
		this.depositPageTitle = depositPageTitle;
		this.newDeposith1 = newDeposith1;
		this.newDeposith2 = newDeposith2;
		this.userGreeting = userGreeting;
		this.userBalance = userBalance;
		this.titleAfterLogoutWait = titleAfterLogoutWait;
		this.titleAfterLogout = titleAfterLogout;
		this.registerPageTitle = registerPageTitle;
		this.contactsAllOthers = contactsAllOthers;
		this.phoneNumber = phoneNumber;
		this.depositAmount = depositAmount;
		this.loginButton = loginButton;
		this.logoutButton = logoutButton;
		this.regulationQuestPageTitle = regulationQuestPageTitle;
		this.submitButtonText = submitButtonText;
		this.thankYouText = thankYouText;
		this.documentsPageTitle = documentsPageTitle;
		this.idText = idText;
		this.fileUploadedText = fileUploadedText;
	}

	@Before
	public void setUp() throws Exception {
		// use the Firefox browser and go to the base site
		driver = new Browser().init("Firefox");
		driver.manage().window().maximize();
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// driver.get(testEnvURL + "/");
	}

	@After
	public void tearDown() throws Exception {
		// quit from WebDriver
		driver.quit();
		if (dB != null) {
			dB.closeConnection();
		}

		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			AssertJUnit.fail(verificationErrorString);
		}
	}

	@Test
	public void testDepositWithNewCard() throws Exception {

		driver.get(bgTestEnvURL + "/");
		User.createUser(driver, this.registerPageTitle, this.skin,
				this.depositPageTitleAfterCreationOfAccount, this.skinId,
				this.languageId, true);

		// wait up to 30 seconds to create the account and load deposit page
		new WebDriverWait(driver, 30)
				.until(new ExpectedCondition<WebElement>() {
					@Override
					public WebElement apply(WebDriver d) {
						return d.findElement(By.id("newCardForm"));
					}
				});
		try {
			AssertJUnit.assertEquals(depositPageTitleAfterCreationOfAccount,
					driver.getTitle());
		} catch (Exception e) {
			verificationErrors.append(e.toString());
		}

		String userInitialBalance = User.getUserBalanceFromDB(User.eMail);

		User.makeDepositAfterCreationOfAccount(driver, skin, User.firstName,
				depositAmount, testEnvURL, depositPageTitle, User.eMail);
		String userBalanceInDB = User.getUserBalanceFromDB(User.eMail);
		System.out.println("The user balance in DB after the deposit is: "
				+ userBalanceInDB);
		User.compareUserBalanceAfterDeposit(driver, skin, depositAmount,
				User.eMail, userInitialBalance);
		// User.compareUserBalanceAfterDeposit(driver, skin, depositAmount,
		// eMail);
	}
}
