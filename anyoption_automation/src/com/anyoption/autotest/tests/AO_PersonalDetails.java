package com.anyoption.autotest.tests;

import static org.junit.Assert.fail;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.AssertJUnit;

import com.anyoption.autotest.common.Browser;
import com.anyoption.autotest.common.DBLayer;
import com.anyoption.autotest.common.User;
import com.anyoption.autotest.utils.RandomStringGenerator;
import com.anyoption.autotest.utils.RandomStringGenerator.Mode;
import com.anyoption.autotest.utils.SpreadsheetData;

@RunWith(value = Parameterized.class)
public class AO_PersonalDetails {

	private static WebDriver driver;

	private static String baseUrl;
	private static StringBuffer verificationErrors = new StringBuffer();

	static DBLayer dB = new DBLayer();

	private String skin;
	private String bgTestEnvURL;
	private String testEnvURL;
	private String skinId;
	private String languageId;
	private String username;
	private String password;
	private String currency;
	private String firstName;
	private String lastName;
	private String eMail;
	private String phone;
	private String loginPageTitle;
	private String afterLoginPageTitle;
	private String depositAmount;
	private String depositPageTitle;
	private String registerPageTitle;
	private String editDeleteCreditCardPageTitle;

	@Parameters
	public static Collection<?> testData() throws Exception {
		InputStream spreadsheet = new FileInputStream(
				"C:\\Users\\tsvetelinad\\Desktop\\EditAndDeleteCreditCard.xlsx");
		return new SpreadsheetData(spreadsheet).getData();
	}

	public AO_PersonalDetails(String skin, String bgTestEnvURL,
			String testEnvURL, String skinId, String languageId,
			String username, String password, String currency,
			String firstName, String lastName, String eMail, String phone,
			String loginPageTitle, String afterLoginPageTitle,
			String depositAmount, String depositPageTitle,
			String registerPageTitle, String editDeleteCreditCardPageTitle) {
		this.skin = skin;
		this.bgTestEnvURL = bgTestEnvURL;
		this.testEnvURL = testEnvURL;
		this.skinId = skinId;
		this.languageId = languageId;
		this.username = username;
		this.password = password;
		this.currency = currency;
		this.firstName = firstName;
		this.lastName = lastName;
		this.eMail = eMail;
		this.phone = phone;
		this.loginPageTitle = loginPageTitle;
		this.afterLoginPageTitle = afterLoginPageTitle;
		this.depositAmount = depositAmount;
		this.depositPageTitle = depositPageTitle;
		this.registerPageTitle = registerPageTitle;
		this.editDeleteCreditCardPageTitle = editDeleteCreditCardPageTitle;
	}

	@Before
	public void setUp() throws Exception {
		// use the Firefox browser and go to the base site
		driver = new Browser().init("Firefox");
		driver.manage().window().maximize();
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// driver.get(testEnvURL + "/");
	}

	@After
	public void tearDown() throws Exception {
		// quit from WebDriver
		driver.quit();
		if (dB != null) {
			dB.closeConnection();
		}
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			System.out.println(verificationErrorString);
			fail(verificationErrorString);
		}
	}

	@Test
	public void testPersonalDetails() throws Exception {
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get(bgTestEnvURL + "/");
		// Browser.openLoginPage(driver, loginPageTitle);
		Browser.login(username, password, loginPageTitle, afterLoginPageTitle,
				eMail, depositPageTitle);
		Browser.openPersonalDetailsPage(bgTestEnvURL, password);
		ArrayList<String[]> initialPersonalDetailsInDB = User
				.getUserPersonalDetailsFromDB(eMail);
		ArrayList<String> initialPersonalDetailsInWeb = User
				.getPersonalDetailsFromWeb(driver, eMail, password);

		for (int i = 0; i < initialPersonalDetailsInDB.get(0).length; i++) {
			if (initialPersonalDetailsInDB.get(0)[i].equalsIgnoreCase("NA")) {
				AssertJUnit
						.assertEquals(initialPersonalDetailsInWeb.get(i), "");
			} else {
				AssertJUnit.assertEquals(initialPersonalDetailsInDB.get(0)[i],
						initialPersonalDetailsInWeb.get(i));
			}
		}

		String randomString = RandomStringGenerator.generateRandomString(4,
				Mode.SMALL_LETTERS);

		String newStreet = "Mladost" + randomString;
		String newStreetNo = RandomStringGenerator.generateRandomString(3,
				Mode.NUMERIC);
		String newCityName = "Sofia" + randomString;
		String newMobilePhone = RandomStringGenerator.generateRandomString(8,
				Mode.NUMERIC);
		driver.findElement(By.id("personalForm:street")).click();
		driver.findElement(By.id("personalForm:street")).clear();
		driver.findElement(By.id("personalForm:street")).sendKeys(newStreet);
		driver.findElement(By.id("personalForm:streetno")).click();
		driver.findElement(By.id("personalForm:streetno")).clear();
		driver.findElement(By.id("personalForm:streetno"))
				.sendKeys(newStreetNo);
		driver.findElement(By.id("personalForm:cityName")).click();
		driver.findElement(By.id("personalForm:cityName")).clear();
		driver.findElement(By.id("personalForm:cityName"))
				.sendKeys(newCityName);
		driver.findElement(By.id("personalForm:mobilePhone")).click();
		driver.findElement(By.id("personalForm:mobilePhone")).clear();
		driver.findElement(By.id("personalForm:mobilePhone")).sendKeys(
				newMobilePhone);

		driver.findElement(By.id("personalForm:j_id1078748838_4eb51a31"))
				.click();
		String actualTitleAfterSuccessfulUpdate = driver.getTitle().trim();
		AssertJUnit
				.assertEquals("My Account", actualTitleAfterSuccessfulUpdate);

		Browser.openPersonalDetailsPage(bgTestEnvURL, password);

		ArrayList<String[]> personalDetailsInDBAfterUpdate = User
				.getUserPersonalDetailsFromDB(eMail);
		ArrayList<String> personalDetailsInWebAfterUpdate = User
				.getPersonalDetailsFromWeb(driver, eMail, password);

		// personalDetailsInWeb.removeAll(Collections.singleton(null));
		// initialPersonalDetailsInDB.removeAll(Collections.singleton(null));

		for (int j = 0; j < personalDetailsInDBAfterUpdate.get(0).length; j++) {
			if (personalDetailsInDBAfterUpdate.get(0)[j].equalsIgnoreCase("NA")) {
				AssertJUnit.assertEquals(
						personalDetailsInWebAfterUpdate.get(j), "");
			} else {
				AssertJUnit.assertEquals(
						personalDetailsInDBAfterUpdate.get(0)[j],
						personalDetailsInWebAfterUpdate.get(j));
			}
		}
		AssertJUnit.assertEquals(newStreet,
				personalDetailsInDBAfterUpdate.get(0)[0]);
		AssertJUnit.assertEquals(newStreetNo,
				personalDetailsInDBAfterUpdate.get(0)[1]);
		AssertJUnit.assertEquals(initialPersonalDetailsInDB.get(0)[2],
				personalDetailsInDBAfterUpdate.get(0)[2]);
		AssertJUnit.assertEquals(newCityName,
				personalDetailsInDBAfterUpdate.get(0)[3]);
		AssertJUnit.assertEquals(initialPersonalDetailsInDB.get(0)[4],
				personalDetailsInDBAfterUpdate.get(0)[4]);
		AssertJUnit.assertEquals(newMobilePhone,
				personalDetailsInDBAfterUpdate.get(0)[5]);
	}
}
