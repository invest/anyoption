package com.anyoption.autotest.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AO_OpenAccount.class, AO_BO_Investment.class,
		AO_ChangePassword.class, AO_DepositWithNewCard.class,
		AO_DepositWithExistingCard.class, AO_Documents.class,
		AO_EditAndDeleteCreditCard_EN.class, /* AO_Forgot_Password.class, */
		AO_PersonalDetails.class, /*
								 * AO_QuickSignUp_EN_skin.class,
								 * AO_QuickSignUp_ES_skin.class,
								 */AO_ReverseWithdraw.class, AO_Withdraw.class })
public class AllJUTests {

}
