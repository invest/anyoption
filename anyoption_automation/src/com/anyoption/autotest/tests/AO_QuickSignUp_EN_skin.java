package com.anyoption.autotest.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.Browser;
import com.anyoption.autotest.common.DBLayer;
import com.anyoption.autotest.common.User;
import com.anyoption.autotest.common.User.Skin;
import com.anyoption.autotest.utils.RandomStringGenerator;

// POSITIVE TEST SCENARIO
// Create an account through quick sign up form with EN skin as follow:
// 1. Fill all mandatory fields with correct data - First name, Last name and so on; (Note: All data should be unique)
// 2. Click on "Open Account" button;
// 3. Check if the account is successfully created.
// 4. Verify data enter correctly in DB users and contacts tables 
// 5. Verify welcome mail arrived - NOT IMPLEMENTED in that test script
// 6. Verify user can see terms and conditions
// Expected result:
// The account is successfully created and correctly stored in DB.

public class AO_QuickSignUp_EN_skin extends RandomStringGenerator {
	// create a WebDriver
	private WebDriver driver;

	private String baseUrl;
	private StringBuffer verificationErrors = new StringBuffer();

	DBLayer dB = new DBLayer();

	@BeforeTest
	public void setUp() throws Exception {
		// use the Firefox browser and go to the base site
		driver = new Browser().init("Firefox");
		driver.manage().window().maximize();
		baseUrl = "http://www.testenv.anyoption.com";
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get(baseUrl + "/");
	}

	@AfterTest
	public void tearDown() throws Exception {
		// quit from WebDriver
		driver.quit();
		if (dB != null) {
			dB.closeConnection();
		}

		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			AssertJUnit.fail(verificationErrorString);
			// fail(verificationErrorString);
		}
	}

	@Test
	public void test() throws Exception {
		User.generateValidUserDataForOpenAccount();
		Browser.typeUserGeneratedData("EN");
		String mainHandle = driver.getWindowHandle();
		Browser.clickOnTermsLinkAtQuickSignUpForm();
		Thread.sleep(5 /* sec */* 1000);
		Browser.verifyTermsAndConditions(Skin.EN);
		// switch to main window
		driver.switchTo().window(mainHandle);
		Browser.confirmTerms();

		Browser.clickOpenAccountButton();

		// wait up to 30 seconds to create the account and load deposit page
		new WebDriverWait(driver, 30)
				.until(new ExpectedCondition<WebElement>() {
					@Override
					public WebElement apply(WebDriver d) {
						return d.findElement(By.id("newCardForm"));
					}
				});

		User.compareUserDataAfterCreationOfAccount(User.eMail, User.firstName,
				User.lastName, Skin.EN);
		User.compareContactsDataAfterCreationOfAccount(User.eMail,
				User.firstName, User.lastName, Skin.EN);
		User.checkAndUpdateUserIdToTesterInDB(User.eMail);

		try {
			AssertJUnit.assertEquals("Binary Options Trading - anyoption",
					driver.getTitle());
			// the old value: Binary Options Trading | Forex Options -
			// anyoption™
			// java.lang.AssertionError: expected:<Binary Options Trading |
			// Forex Options - anyoption™> but was:<Binary Options Trading -
			// anyoption>
			// java.lang.AssertionError: expected:<Binary Options Trading -
			// anyoption> but was:<Binary Options Trading | 80% Profit on
			// Investment | anyoption>
			// java.lang.AssertionError: expected:<Binary Options Trading -
			// anyoption> but was:<Account Login | anyoption™>
		} catch (Exception e) {
			verificationErrors.append(e.toString());
			// System.out.println(e.getMessage());
			// e.toString();
		}

		User.checkDepositPageHeaders(driver, Skin.EN);
		User.checkUserGreeting(driver, Skin.EN);
		User.checkUserBalanceAfterCreationOfAccount(driver, Skin.EN);
		Browser.clickLogoutButton();

		// wait up to 30 seconds to log out
		(new WebDriverWait(driver, 30)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				return d.getTitle().toLowerCase()
						.startsWith("mobile binary option trading");
			}
		});

		try {
			AssertJUnit.assertEquals(
					"Mobile Binary Option Trading | anyoption™",
					driver.getTitle());
		} catch (Exception e) {
			verificationErrors.append(e.toString());
		}
	}

}
