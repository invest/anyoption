package com.anyoption.autotest.tests;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.WebDriver;
import org.testng.AssertJUnit;

import com.anyoption.autotest.common.Browser;
import com.anyoption.autotest.common.DBLayer;
import com.anyoption.autotest.common.User;
import com.anyoption.autotest.utils.SpreadsheetData;

@RunWith(value = Parameterized.class)
public class AO_DepositWithExistingCard {

	private static WebDriver driver;

	private static String baseUrl;
	private static StringBuffer verificationErrors = new StringBuffer();

	static DBLayer dB = new DBLayer();

	private String skin;
	private String bgTestEnvURL;
	private String testEnvURL;
	private String skinId;
	private String languageId;
	private String username;
	private String password;
	private String currency;
	private String firstName;
	private String lastName;
	private String eMail;
	private String phone;
	private String loginPageTitle;
	private String afterLoginPageTitle;
	private String depositAmount;
	private String depositPageTitle;
	private String withdrawPageTitle;
	private String reverseWithdrawPageTitle;
	private String successfulReverseText;
	private String registerPageTitle;
	private String investmentAmount;

	@Parameters
	public static Collection<?> testData() throws Exception {
		InputStream spreadsheet = new FileInputStream(
				"C:\\Users\\tsvetelinad\\Desktop\\UserInfo.xlsx");
		return new SpreadsheetData(spreadsheet).getData();
	}

	public AO_DepositWithExistingCard(String skin, String bgTestEnvURL,
			String testEnvURL, String skinId, String languageId,
			String username, String password, String currency,
			String firstName, String lastName, String eMail, String phone,
			String loginPageTitle, String afterLoginPageTitle,
			String depositAmount, String depositPageTitle,
			String withdrawPageTitle, String reverseWithdrawPageTitle,
			String successfulReverseText, String registerPageTitle,
			String investmentAmount) {
		this.skin = skin;
		this.bgTestEnvURL = bgTestEnvURL;
		this.testEnvURL = testEnvURL;
		this.skinId = skinId;
		this.languageId = languageId;
		this.username = username;
		this.password = password;
		this.currency = currency;
		this.firstName = firstName;
		this.lastName = lastName;
		this.eMail = eMail;
		this.phone = phone;
		this.loginPageTitle = loginPageTitle;
		this.afterLoginPageTitle = afterLoginPageTitle;
		this.depositAmount = depositAmount;
		this.depositPageTitle = depositPageTitle;
		this.withdrawPageTitle = withdrawPageTitle;
		this.reverseWithdrawPageTitle = reverseWithdrawPageTitle;
		this.successfulReverseText = successfulReverseText;
		this.registerPageTitle = registerPageTitle;
		this.investmentAmount = investmentAmount;
	}

	@Before
	public void setUp() throws Exception {
		// use the Firefox browser and go to the base site
		driver = new Browser().init("Firefox");
		driver.manage().window().maximize();
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// driver.get(testEnvURL + "/");
	}

	@After
	public void tearDown() throws Exception {
		// quit from WebDriver
		driver.quit();
		if (dB != null) {
			dB.closeConnection();
		}

		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			AssertJUnit.fail(verificationErrorString);
		}
	}

	@Test
	public void testDepositWithExistingCard() throws Exception {
		driver.get(bgTestEnvURL + "/");
		Browser.login(username, password, loginPageTitle, afterLoginPageTitle,
				eMail, depositPageTitle);
		String userInitialBalance = User.getUserBalanceFromDB(eMail);
		System.out.println("The initial user balance in web page is: "
				+ userInitialBalance);
		System.out.println("The deposit amount is: " + depositAmount);
		User.depositWithExistingCard(driver, skin, depositAmount, bgTestEnvURL,
				depositPageTitle, eMail);
		User.compareUserBalanceAfterDeposit(driver, skin, depositAmount, eMail,
				userInitialBalance);

	}

}
