package com.anyoption.autotest.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.Browser;

public class Login {
	// create a WebDriver
	private WebDriver driver;

	private String baseUrl;
	// private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	@BeforeTest
	public void setUp() throws Exception {
		// use the Firefox browser and go to the base site
		driver = new Browser().init("Firefox");
		driver.manage().window().maximize();
		baseUrl = "http://www.testenv.anyoption.com";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get(baseUrl + "/");
	}

	@AfterTest
	public void tearDown() throws Exception {
		// quit from WebDriver
		driver.quit();

		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			AssertJUnit.fail(verificationErrorString);
		}
	}

	@Test
	public void test() throws Exception {

		Browser.openLoginPage(driver, "Account Login | anyoption™");
		Browser.login("144030", "123456", "Account Login | anyoption™",
				"Binary Options – Live Trading Area | anyoption ™",
				"tsvetelinad@anyoption.com", "Deposit - anyoption™");

	}
}
