package com.anyoption.autotest.tests;

import static org.junit.Assert.fail;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.AssertJUnit;

import com.anyoption.autotest.common.Browser;
import com.anyoption.autotest.common.DBLayer;
import com.anyoption.autotest.common.User;
import com.anyoption.autotest.utils.RandomStringGenerator;
import com.anyoption.autotest.utils.RandomStringGenerator.Mode;
import com.anyoption.autotest.utils.SpreadsheetData;

@RunWith(value = Parameterized.class)
public class AO_ChangePassword {

	private static WebDriver driver;

	private static String baseUrl;
	private static StringBuffer verificationErrors = new StringBuffer();

	static DBLayer dB = new DBLayer();

	private String skin;
	private String bgTestEnvURL;
	private String testEnvURL;
	private String skinId;
	private String languageId;
	private String username;
	private String password;
	private String currency;
	private String firstName;
	private String lastName;
	private String eMail;
	private String phone;
	private String loginPageTitle;
	private String afterLoginPageTitle;
	private String depositAmount;
	private String depositPageTitle;
	private String registerPageTitle;
	private String editDeleteCreditCardPageTitle;

	@Parameters
	public static Collection<?> testData() throws Exception {
		InputStream spreadsheet = new FileInputStream(
				"C:\\Users\\tsvetelinad\\Desktop\\ChangePassword.xlsx");
		return new SpreadsheetData(spreadsheet).getData();
	}

	public AO_ChangePassword(String skin, String bgTestEnvURL,
			String testEnvURL, String skinId, String languageId,
			String username, String password, String currency,
			String firstName, String lastName, String eMail, String phone,
			String loginPageTitle, String afterLoginPageTitle,
			String depositAmount, String depositPageTitle,
			String registerPageTitle, String editDeleteCreditCardPageTitle) {
		this.skin = skin;
		this.bgTestEnvURL = bgTestEnvURL;
		this.testEnvURL = testEnvURL;
		this.skinId = skinId;
		this.languageId = languageId;
		this.username = username;
		this.password = password;
		this.currency = currency;
		this.firstName = firstName;
		this.lastName = lastName;
		this.eMail = eMail;
		this.phone = phone;
		this.loginPageTitle = loginPageTitle;
		this.afterLoginPageTitle = afterLoginPageTitle;
		this.depositAmount = depositAmount;
		this.depositPageTitle = depositPageTitle;
		this.registerPageTitle = registerPageTitle;
		this.editDeleteCreditCardPageTitle = editDeleteCreditCardPageTitle;
	}

	@Before
	public void setUp() throws Exception {
		// use the Firefox browser and go to the base site
		driver = new Browser().init("Firefox");
		driver.manage().window().maximize();
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// driver.get(testEnvURL + "/");
	}

	@After
	public void tearDown() throws Exception {
		// quit from WebDriver
		driver.quit();
		if (dB != null) {
			dB.closeConnection();
		}
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			System.out.println(verificationErrorString);
			fail(verificationErrorString);
		}
	}

	@Test
	public void testChangePassword() throws Exception {
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get(bgTestEnvURL + "/");
		ArrayList<String[]> initialPsswrdInDB = User
				.getUserPasswordFromDB(eMail);
		String initialPasswordInDB = initialPsswrdInDB.get(0)[0];
		// Browser.openLoginPage(driver, loginPageTitle);
		Browser.login(username, initialPasswordInDB, loginPageTitle,
				afterLoginPageTitle, eMail, depositPageTitle);
		Browser.openPasswordPage(bgTestEnvURL);

		String randomString = RandomStringGenerator.generateRandomString(3,
				Mode.SMALL_LETTERS)
				+ RandomStringGenerator.generateRandomString(2, Mode.NUMERIC);
		String newPassword = "Pass" + randomString;
		System.out.println("Generated password is: " + newPassword);
		driver.findElement(By.id("registerForm:oldPassword")).click();
		driver.findElement(By.id("registerForm:oldPassword")).clear();
		driver.findElement(By.id("registerForm:oldPassword")).sendKeys(
				initialPasswordInDB);
		driver.findElement(By.id("registerForm:password")).click();
		driver.findElement(By.id("registerForm:password")).clear();
		driver.findElement(By.id("registerForm:password"))
				.sendKeys(newPassword);
		driver.findElement(By.id("registerForm:password2")).click();
		driver.findElement(By.id("registerForm:password2")).clear();
		driver.findElement(By.id("registerForm:password2")).sendKeys(
				newPassword);

		driver.findElement(By.id("registerForm:j_id1741218297_328b0104"))
				.click();
		String actualTitleAfterSuccessfulUpdate = driver.getTitle().trim();
		AssertJUnit
				.assertEquals("My Account", actualTitleAfterSuccessfulUpdate);
		// Password was changed successfully

		ArrayList<String[]> psswrdInDB = User.getUserPasswordFromDB(eMail);
		String passwordInDB = psswrdInDB.get(0)[0];

		AssertJUnit.assertEquals(newPassword, passwordInDB);
		Browser.logout("mobile binary option trading",
				"Mobile Binary Option Trading | anyoption™");

		Browser.login(username, newPassword, loginPageTitle,
				afterLoginPageTitle, eMail, depositPageTitle);

	}
}
