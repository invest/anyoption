package com.anyoption.autotest.tests;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.AssertJUnit;

import com.anyoption.autotest.common.Browser;
import com.anyoption.autotest.common.DBLayer;
import com.anyoption.autotest.common.User;
import com.anyoption.autotest.utils.SpreadsheetData;

@RunWith(value = Parameterized.class)
public class AO_EditAndDeleteCreditCard_EN {

	private static WebDriver driver;

	private static String baseUrl;
	private static StringBuffer verificationErrors = new StringBuffer();

	static DBLayer dB = new DBLayer();

	private String skin;
	private String bgTestEnvURL;
	private String testEnvURL;
	private String skinId;
	private String languageId;
	private String username;
	private String password;
	private String currency;
	private String firstName;
	private String lastName;
	private String eMail;
	private String phone;
	private String loginPageTitle;
	private String afterLoginPageTitle;
	private String depositAmount;
	private String depositPageTitle;
	private String registerPageTitle;
	private String editDeleteCreditCardPageTitle;

	@Parameters
	public static Collection<?> testData() throws Exception {
		InputStream spreadsheet = new FileInputStream(
				"C:\\Users\\tsvetelinad\\Desktop\\EditAndDeleteCreditCard.xlsx");
		return new SpreadsheetData(spreadsheet).getData();
	}

	public AO_EditAndDeleteCreditCard_EN(String skin, String bgTestEnvURL,
			String testEnvURL, String skinId, String languageId,
			String username, String password, String currency,
			String firstName, String lastName, String eMail, String phone,
			String loginPageTitle, String afterLoginPageTitle,
			String depositAmount, String depositPageTitle,
			String registerPageTitle, String editDeleteCreditCardPageTitle) {
		this.skin = skin;
		this.bgTestEnvURL = bgTestEnvURL;
		this.testEnvURL = testEnvURL;
		this.skinId = skinId;
		this.languageId = languageId;
		this.username = username;
		this.password = password;
		this.currency = currency;
		this.firstName = firstName;
		this.lastName = lastName;
		this.eMail = eMail;
		this.phone = phone;
		this.loginPageTitle = loginPageTitle;
		this.afterLoginPageTitle = afterLoginPageTitle;
		this.depositAmount = depositAmount;
		this.depositPageTitle = depositPageTitle;
		this.registerPageTitle = registerPageTitle;
		this.editDeleteCreditCardPageTitle = editDeleteCreditCardPageTitle;
	}

	@Before
	public void setUp() throws Exception {
		// use the Firefox browser and go to the base site
		driver = new Browser().init("Firefox");
		driver.manage().window().maximize();
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// driver.get(testEnvURL + "/");
	}

	@After
	public void tearDown() throws Exception {
		// quit from WebDriver
		driver.quit();
		if (dB != null) {
			dB.closeConnection();
		}

		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			AssertJUnit.fail(verificationErrorString);
		}
	}

	@Test
	public void testEditAndDeleteCreditCard() throws Exception {
		driver.get(bgTestEnvURL + "/");
		Browser.openLoginPage(driver, loginPageTitle);

		Browser.login(username, password, loginPageTitle, afterLoginPageTitle,
				eMail, depositPageTitle);
		ArrayList<String[]> visibleCards = User
				.getUserVisibleCreditCards(eMail);
		if (visibleCards.size() > 0) {
			Browser.openDepositPage(bgTestEnvURL, depositPageTitle);
			Browser.openEditDeleteCardPage(bgTestEnvURL,
					editDeleteCreditCardPageTitle);

			Browser.openEditCardPage(bgTestEnvURL,
					editDeleteCreditCardPageTitle);

			User.updateCreditCardDetails(driver, editDeleteCreditCardPageTitle,
					depositPageTitle, visibleCards, eMail);
		}
		// login, open deposit page, check in db for cards - if true then make
		// change of details
		// if false then add new card, edit it, delete card
		else {
			Browser.openDepositPage(bgTestEnvURL, depositPageTitle);
			Browser.fillCreditCardDetails(firstName, depositAmount);
			driver.findElement(By.id("btn_newCard")).click();
			if (driver.findElement(By.id("btn_newCard")).isDisplayed()) {
				User.allowCreditCard(eMail);
			} else {
				System.out.println("The credit card could not be added.");
			}

			Browser.openEditDeleteCardPage(bgTestEnvURL,
					editDeleteCreditCardPageTitle);

			Browser.openEditCardPage(bgTestEnvURL,
					editDeleteCreditCardPageTitle);
			visibleCards = User.getUserVisibleCreditCards(eMail);
			User.updateCreditCardDetails(driver, editDeleteCreditCardPageTitle,
					depositPageTitle, visibleCards, eMail);
		}
	}
}
