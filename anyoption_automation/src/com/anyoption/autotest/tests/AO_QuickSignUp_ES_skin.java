package com.anyoption.autotest.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.Browser;
import com.anyoption.autotest.common.DBLayer;
import com.anyoption.autotest.common.User;
import com.anyoption.autotest.common.User.Skin;
import com.anyoption.autotest.utils.RandomStringGenerator;

// POSITIVE TEST SCENARIO
// Create an account through quick sign up form with ES skin as follow:
// 1. Fill all mandatory fields with correct data - First name, Last name and so on; (Note: All data should be unique)
// 2. Click on "Open Account" button;
// 3. Check if the account is successfully created.
// 4. Verify data enter correctly in DB users and contacts tables 
// 5. Verify welcome mail arrived - NOT IMPLEMENTED in that test script
// 6. Verify user can see terms and conditions
// Expected result:
// The account is successfully created and correctly stored in DB.

public class AO_QuickSignUp_ES_skin extends RandomStringGenerator {
	// create a WebDriver
	private WebDriver driver;

	private String baseUrl;
	// private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	DBLayer dB = new DBLayer();

	@BeforeTest
	public void setUp() throws Exception {
		// use the Firefox browser and go to the base site
		// System.setProperty("webdriver.chrome.driver",
		// "C:\\SeleniumDrivers\\chromedriver.exe");
		// driver = new ChromeDriver();
		// driver = new FirefoxDriver();
		driver = new Browser().init("Firefox");
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		baseUrl = "http://es.testenv.anyoption.com/";
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get(baseUrl + "/");
	}

	@AfterTest
	public void tearDown() throws Exception {
		// quit from WebDriver
		driver.quit();
		if (dB != null) {
			dB.closeConnection();
		}

		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			AssertJUnit.fail(verificationErrorString);
		}
	}

	@Test
	public void test() throws Exception {
		User.generateValidUserDataForOpenAccount();
		Browser.typeUserGeneratedData("ES");
		String mainHandle = driver.getWindowHandle();
		Browser.clickOnTermsLinkAtQuickSignUpForm();
		Thread.sleep(5 /* sec */* 1000);
		Browser.verifyTermsAndConditions(Skin.ES);
		// switch to main window
		driver.switchTo().window(mainHandle);
		Browser.confirmTerms();
		// driver.findElement(By.id("funnel_terms_field")).click();
		// driver.findElement(
		// By.cssSelector("span#funnel_terms_h.funnel_terms_h2 span.funnel_terms_h_in"))
		// .click();
		// driver.findElement(By.cssSelector("#funnel_terms_h > #funnel_terms_h"))
		// .click();

		Browser.clickOpenAccountButton();

		// wait up to 30 seconds to create the account and load deposit page
		new WebDriverWait(driver, 30)
				.until(new ExpectedCondition<WebElement>() {
					@Override
					public WebElement apply(WebDriver d) {
						return d.findElement(By.id("newCardForm"));
					}
				});

		User.compareUserDataAfterCreationOfAccount(User.eMail, User.firstName,
				User.lastName, Skin.ES);
		User.compareContactsDataAfterCreationOfAccount(User.eMail,
				User.firstName, User.lastName, Skin.ES);
		User.checkAndUpdateUserIdToTesterInDB(User.eMail);

		try {
			AssertJUnit
					.assertEquals(
							"Opciones binarias | Opciones Forex | opciones de valores | anyoption�",
							driver.getTitle());
		} catch (Exception e) {
			verificationErrors.append(e.toString());
		}

		User.checkDepositPageHeaders(driver, Skin.ES);
		User.checkUserGreeting(driver, Skin.ES);
		User.checkUserBalanceAfterCreationOfAccount(driver, Skin.ES);
		Browser.clickLogoutButton();

		// wait up to 30 seconds to log out
		(new WebDriverWait(driver, 30)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				return d.getTitle().toLowerCase()
						.startsWith("inversi�n en opciones binarias por m�vil");
			}
		});

		try {
			AssertJUnit.assertEquals(
					"Inversi�n en opciones binarias por m�vil | anyoption�",
					driver.getTitle());
		} catch (Exception e) {
			verificationErrors.append(e.toString());
		}

	}
}
