package com.anyoption.autotest.tests;

import static org.junit.Assert.fail;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.AssertJUnit;

import com.anyoption.autotest.common.Browser;
import com.anyoption.autotest.common.DBLayer;
import com.anyoption.autotest.common.User;
import com.anyoption.autotest.utils.SpreadsheetData;

//POSITIVE TEST SCENARIO
//Create an account through quick sign up form with EN skin as follow:
//1. Fill all mandatory fields with correct data - First name, Last name and so on; (Note: All data should be unique)
//2. Click on "Open Account" button;
//3. Check if the account is successfully created.
//4. Verify data enter correctly in DB users and contacts tables 
//5. Verify welcome mail arrived - NOT IMPLEMENTED in that test script
//6. Verify user can see terms and conditions
//Expected result:
//The account is successfully created and correctly stored in DB.

@RunWith(value = Parameterized.class)
public class AO_OpenAccount {

	private static WebDriver driver;

	private static String baseUrl;
	private static StringBuffer verificationErrors = new StringBuffer();

	static DBLayer dB = new DBLayer();

	private String skin;
	private String bgTestEnvURL;
	private String testEnvURL;
	private String liveEnvURL;
	private String skinId;
	private String languageId;
	private String termsLinkQuickSignUpForm;
	private String termsLinkRegistratrationForm;
	private String termsAndConditions;
	private String depositPageTitleWait;
	private String depositPageTitleAfterCreationOfAccount;
	private String depositPageTitle;
	private String newDeposith1;
	private String newDeposith2;
	private String userGreeting;
	private String userBalance;
	private String titleAfterLogoutWait;
	private String titleAfterLogout;
	private String registerPageTitle;
	private String contactsAllOthers;
	private String phoneNumber;
	private String depositAmount;
	private String loginButton;
	private String logoutButton;
	private String regulationQuestPageTitle;
	private String submitButtonText;
	private String thankYouText;
	private String documentsPageTitle;
	private String idText;
	private String fileUploadedText;

	@Parameters
	public static Collection<?> testData() throws Exception {
		InputStream spreadsheet = new FileInputStream(
				"C:\\Users\\tsvetelinad\\Desktop\\NewSkinInfo.xlsx");
		return new SpreadsheetData(spreadsheet).getData();
	}

	public AO_OpenAccount(String skin, String bgTestEnvURL, String testEnvURL,
			String liveEnvURL, String skinId, String languageId,
			String termsLinkQuickSignUpForm,
			String termsLinkRegistratrationForm, String termsAndConditions,
			String depositPageTitleWait,
			String depositPageTitleAfterCreationOfAccount,
			String depositPageTitle, String newDeposith1, String newDeposith2,
			String userGreeting, String userBalance,
			String titleAfterLogoutWait, String titleAfterLogout,
			String registerPageTitle, String contactsAllOthers,
			String phoneNumber, String depositAmount, String loginButton,
			String logoutButton, String regulationQuestPageTitle,
			String submitButtonText, String thankYouText,
			String documentsPageTitle, String idText, String fileUploadedText) {
		this.skin = skin;
		this.bgTestEnvURL = bgTestEnvURL;
		this.testEnvURL = testEnvURL;
		this.liveEnvURL = liveEnvURL;
		this.skinId = skinId;
		this.languageId = languageId;
		this.termsLinkQuickSignUpForm = termsLinkQuickSignUpForm;
		this.termsLinkRegistratrationForm = termsLinkRegistratrationForm;
		this.termsAndConditions = termsAndConditions;
		this.depositPageTitleWait = depositPageTitleWait;
		this.depositPageTitleAfterCreationOfAccount = depositPageTitleAfterCreationOfAccount;
		this.depositPageTitle = depositPageTitle;
		this.newDeposith1 = newDeposith1;
		this.newDeposith2 = newDeposith2;
		this.userGreeting = userGreeting;
		this.userBalance = userBalance;
		this.titleAfterLogoutWait = titleAfterLogoutWait;
		this.titleAfterLogout = titleAfterLogout;
		this.registerPageTitle = registerPageTitle;
		this.contactsAllOthers = contactsAllOthers;
		this.phoneNumber = phoneNumber;
		this.depositAmount = depositAmount;
		this.loginButton = loginButton;
		this.logoutButton = logoutButton;
		this.regulationQuestPageTitle = regulationQuestPageTitle;
		this.submitButtonText = submitButtonText;
		this.thankYouText = thankYouText;
		this.documentsPageTitle = documentsPageTitle;
		this.idText = idText;
		this.fileUploadedText = fileUploadedText;
	}

	@Before
	public void setUp() throws Exception {
		// use the Firefox browser and go to the base site
		driver = new Browser().init("Firefox");
		driver.manage().window().maximize();
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// driver.get(testEnvURL + "/");
	}

	@After
	public void tearDown() throws Exception {
		// quit from WebDriver
		driver.quit();
		if (dB != null) {
			dB.closeConnection();
		}
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			System.out.println(verificationErrorString);
			fail(verificationErrorString);
		}
	}

	@Test
	public void openAccount() throws Exception {
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// driver.get(testEnvURL + "/");
		driver.get(bgTestEnvURL + "/");
		User.generateValidUserDataForOpenAccount();

		/*
		 * if (skin.equalsIgnoreCase("SG")) {
		 * driver.findElement(By.linkText("开通账户")).click(); //
		 * href="/register-binary-options-platform" } else {
		 */
		driver.findElement(By.id("menu_register")).click();

		AssertJUnit.assertEquals(registerPageTitle, driver.getTitle());

		Browser.typeUserGeneratedData(skin);

		driver.findElement(By.id("cont_us_right_banner_arrow2")).click();
		String otherPhonesText = driver.findElement(
				By.id("bannerSupport_allOther")).getText();

		System.out.println("Other phone text: " + otherPhonesText);
		AssertJUnit.assertEquals(contactsAllOthers, otherPhonesText);
		String otherPhoneNumber = driver.findElement(
				By.id("bannerSupport_allOther_phone")).getText();
		System.out.println("Other phone number: " + otherPhoneNumber);
		AssertJUnit.assertEquals(phoneNumber, otherPhoneNumber);
		driver.findElement(By.id("close_link_cont_us")).click();

		String mainHandle = driver.getWindowHandle();
		Browser.clickOnTermsLinkAtRegistrationForm();
		Thread.sleep(5 /* sec */* 1000);
		Browser.verifyTermsAndConditions(driver, termsAndConditions, skin);
		driver.switchTo().window(mainHandle); // switch to main window
		Browser.confirmTerms();

		Browser.clickOpenAccountButton();

		// wait up to 30 seconds to create the account and load deposit page
		new WebDriverWait(driver, 30)
				.until(new ExpectedCondition<WebElement>() {
					@Override
					public WebElement apply(WebDriver d) {
						return d.findElement(By.id("newCardForm"));
					}
				});

		User.compareUserDataAfterCreationOfAccount(User.eMail, User.firstName,
				User.lastName, skinId, languageId);
		User.compareContactsDataAfterCreationOfAccount(User.eMail,
				User.firstName, User.lastName, skinId);
		User.checkAndUpdateUserIdToTesterInDB(User.eMail);

		try {
			AssertJUnit.assertEquals(depositPageTitleAfterCreationOfAccount,
					driver.getTitle());
		} catch (Exception e) {
			verificationErrors.append(e.toString());
			// System.out.println(e.getMessage());
			// e.toString();
		}

		User.checkDepositPageHeaders(driver, newDeposith1, newDeposith2);
		User.checkUserGreeting(driver, userGreeting);
		User.checkUserBalanceAfterCreationOfAccount(driver, userBalance);
		Browser.logout(titleAfterLogoutWait, titleAfterLogout);

		// Browser.clickLogoutButton();

		// // wait up to 30 seconds to log out
		// (new WebDriverWait(driver, 30)).until(new
		// ExpectedCondition<Boolean>() {
		// public Boolean apply(WebDriver d) {
		// return d.getTitle().toLowerCase()
		// .startsWith(titleAfterLogoutWait.toLowerCase());
		// }
		// });
		//
		// try {
		// AssertJUnit.assertEquals(titleAfterLogout, driver.getTitle());
		// } catch (Exception e) {
		// verificationErrors.append(e.toString());
		// }
	}

}
