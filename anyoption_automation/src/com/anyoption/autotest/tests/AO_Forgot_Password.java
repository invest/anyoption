package com.anyoption.autotest.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.Browser;

public class AO_Forgot_Password {
	// create a WebDriver
	private WebDriver driver;

	private String baseUrl;
	// private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	@BeforeTest
	public void setUp() throws Exception {
		// use the Firefox browser and go to the base site
		driver = new Browser().init("Firefox");
		driver.manage().window().maximize();
		baseUrl = "http://www.bgtestenv.anyoption.com";// "http://www.testenv.anyoption.com";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get(baseUrl + "/");
	}

	@AfterTest
	public void tearDown() throws Exception {
		// quit from WebDriver
		driver.quit();

		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			AssertJUnit.fail(verificationErrorString);
		}
	}

	@Test
	public void testForgotPassword() throws Exception {
		Browser.openLoginPage(driver, "Account Login | anyoption™");
		Browser.openForgotPasswordPage();
		Browser.populateForgotPasswordFields("12345656",
				"tsvetelinad9@anyoption.com", "+359");

		// get the text after pressing of "Submit" button and print it
		String text = driver.findElement(By.id("passwordForm:errorloginMsg"))
				.getText();
		System.out.println(text);

		// check the text
		try {
			AssertJUnit.assertEquals(
					"A link for resetting your password was sent successfully",
					text);
		} catch (Exception e) {
			verificationErrors.append(e.toString());
		}
	}
}
