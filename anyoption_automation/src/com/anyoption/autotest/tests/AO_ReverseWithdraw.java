package com.anyoption.autotest.tests;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.AssertJUnit;

import com.anyoption.autotest.common.Browser;
import com.anyoption.autotest.common.DBLayer;
import com.anyoption.autotest.common.User;
import com.anyoption.autotest.utils.SpreadsheetData;

@RunWith(value = Parameterized.class)
public class AO_ReverseWithdraw {

	private static WebDriver driver;

	private static String baseUrl;
	private static StringBuffer verificationErrors = new StringBuffer();

	static DBLayer dB = new DBLayer();

	private String skin;
	private String bgTestEnvURL;
	private String testEnvURL;
	private String skinId;
	private String languageId;
	private String username;
	private String password;
	private String currency;
	private String firstName;
	private String lastName;
	private String eMail;
	private String phone;
	private String loginPageTitle;
	private String afterLoginPageTitle;
	private String depositAmount;
	private String depositPageTitle;
	private String withdrawPageTitle;
	private String reverseWithdrawPageTitle;
	private String successfulReverseText;
	private String registerPageTitle;
	private String investmentAmount;

	@Parameters
	public static Collection<?> testData() throws Exception {
		InputStream spreadsheet = new FileInputStream(
				"C:\\Users\\tsvetelinad\\Desktop\\UserInfo.xlsx");
		return new SpreadsheetData(spreadsheet).getData();
	}

	public AO_ReverseWithdraw(String skin, String bgTestEnvURL,
			String testEnvURL, String skinId, String languageId,
			String username, String password, String currency,
			String firstName, String lastName, String eMail, String phone,
			String loginPageTitle, String afterLoginPageTitle,
			String depositAmount, String depositPageTitle,
			String withdrawPageTitle, String reverseWithdrawPageTitle,
			String successfulReverseText, String registerPageTitle,
			String investmentAmount) {
		this.skin = skin;
		this.bgTestEnvURL = bgTestEnvURL;
		this.testEnvURL = testEnvURL;
		this.skinId = skinId;
		this.languageId = languageId;
		this.username = username;
		this.password = password;
		this.currency = currency;
		this.firstName = firstName;
		this.lastName = lastName;
		this.eMail = eMail;
		this.phone = phone;
		this.loginPageTitle = loginPageTitle;
		this.afterLoginPageTitle = afterLoginPageTitle;
		this.depositAmount = depositAmount;
		this.depositPageTitle = depositPageTitle;
		this.withdrawPageTitle = withdrawPageTitle;
		this.reverseWithdrawPageTitle = reverseWithdrawPageTitle;
		this.successfulReverseText = successfulReverseText;
		this.registerPageTitle = registerPageTitle;
		this.investmentAmount = investmentAmount;
	}

	@Before
	public void setUp() throws Exception {
		// use the Firefox browser and go to the base site
		driver = new Browser().init("Firefox");
		driver.manage().window().maximize();
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// driver.get(testEnvURL + "/");
	}

	@After
	public void tearDown() throws Exception {
		// quit from WebDriver
		driver.quit();
		if (dB != null) {
			dB.closeConnection();
		}

		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			AssertJUnit.fail(verificationErrorString);
		}
	}

	@Test
	public void testReverseWithdraw() throws Exception {
		driver.get(bgTestEnvURL + "/");
		Browser.openLoginPage(driver, loginPageTitle);
		Browser.login(username, password, loginPageTitle, afterLoginPageTitle,
				eMail, depositPageTitle);
		String userInitialBalance = User.getUserBalanceFromDB(eMail);

		ArrayList<String[]> userWithdraws = User
				.getUserActiveWithdrawsTransactionFromDB(eMail);
		int initialNumberOfWithdraws = userWithdraws.size();
		if (initialNumberOfWithdraws == 0) {
			String withdrawURL = bgTestEnvURL
					+ "/jsp/pages/creditCard_withdraw.jsf";
			driver.get(withdrawURL);
			AssertJUnit.assertEquals(withdrawPageTitle, driver.getTitle());
			if (skin.equalsIgnoreCase("KR")) {
				ArrayList<String[]> resultsForCreditCards = User
						.getUserVisibleCreditCards(eMail);
				if (resultsForCreditCards.size() != 0) {
					String userCreditCardIDInDB = resultsForCreditCards.get(0)[0];
					System.out.println("The user's credit card ID is: "
							+ userCreditCardIDInDB);
					new Select(driver.findElement(By.id("withdrawForm:ccNum")))
							.selectByValue(userCreditCardIDInDB);
				} else {
					System.out
							.println("No existing credit cards for the user!");
					AssertJUnit.fail("No existing credit cards for the user!");
				}
			} else {
				new Select(driver.findElement(By.id("withdrawForm:ccNum")))
						.selectByVisibleText("0000 " + "Visa"); // "1111 "
			}

			driver.findElement(By.id("withdrawForm:amount")).click();
			driver.findElement(By.id("withdrawForm:amount")).clear();
			driver.findElement(By.id("withdrawForm:amount")).sendKeys(
					depositAmount);
			driver.findElement(By.id("withdrawForm:j_id39148377_2191c06c"))
					.click();
			driver.findElement(By.id("withdrawForm:j_id553304962_2047abea"))
					.click();
			User.compareUserBalanceAfterWithdraw(driver, depositAmount, eMail,
					userInitialBalance);
			userWithdraws = User.getUserActiveWithdrawsTransactionFromDB(eMail);
			initialNumberOfWithdraws = userWithdraws.size();
			userInitialBalance = User.getUserBalanceFromDB(eMail);
		}

		if (initialNumberOfWithdraws > 0) {

			ArrayList<String[]> reverseWithdraws = User
					.getUserReverseWithdrawsTransactionFromDB(eMail);
			int initialNumberOfReverseWithdraws = reverseWithdraws.size();

			String reverseWithdrawURL = bgTestEnvURL
					+ "/jsp/pages/reverseWithdraw.jsf";
			driver.get(reverseWithdrawURL);
			AssertJUnit.assertEquals(reverseWithdrawPageTitle,
					driver.getTitle());
			driver.findElement(By.id("reverseWithdrawForm:data:0:select"))
					.click();
			driver.findElement(
					By.xpath("//span[@onclick=\"myfaces.oam.submitForm('reverseWithdrawForm','reverseWithdrawForm:hiddenbtn');\"]"))
					.click();
			AssertJUnit.assertEquals(
					successfulReverseText,
					driver.findElement(
							By.cssSelector("td.reverse_withdraw_text > span"))
							.getText());

			ArrayList<String[]> userWithdrawsAfterReverse = User
					.getUserActiveWithdrawsTransactionFromDB(eMail);
			int numberOfWithdrawsAfterReverse = userWithdrawsAfterReverse
					.size();

			ArrayList<String[]> reverseWithdrawsAfterReverse = User
					.getUserReverseWithdrawsTransactionFromDB(eMail);
			int numberOfReverseWithdrawsAfterReverse = reverseWithdrawsAfterReverse
					.size();

			AssertJUnit
					.assertEquals(
							"Check the number of withdraws after the reverse",
							initialNumberOfWithdraws - 1,
							numberOfWithdrawsAfterReverse);
			AssertJUnit.assertEquals(
					"Check the number of reverse withdraws after the reverse",
					initialNumberOfReverseWithdraws + 1,
					numberOfReverseWithdrawsAfterReverse);

			// "Your withdrawal request for $300.00 was accepted. We will credit your card with the requested amount within 3 business days (note: if this is not your first withdrawal in this calendar month we will deduct $30.00 withdrawal fee from this amount). The transaction will appear on your card statement according to your bank and/or card issuer policy."

			User.compareUserBalanceAfterReverseWithdraw(driver, depositAmount,
					eMail, userInitialBalance);
			System.out.println("Test");
		} else {
			AssertJUnit.fail("The user has NO withdraw to be reversed!!!");
		}
	}
}
