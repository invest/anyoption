package com.anyoption.autotest.devtests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite.SuiteClasses;

import com.anyoption.autotest.utils.Concurrent;
import com.anyoption.autotest.utils.ConcurrentSuite;

@RunWith(ConcurrentSuite.class)
@SuiteClasses({ TestLoop1.class, TestLoop2.class })
@Concurrent
public class AllTests {

}
