package com.anyoption.autotest.devtests;

import junit.framework.JUnit4TestAdapter;
import junit.framework.TestSuite;

import org.junit.runner.RunWith;
import org.junit.runners.AllTests;

@RunWith(AllTests.class)
public final class TestLoop1 {

	public static TestSuite suite() {
		TestSuite suite = new TestSuite();
		for (int i = 0; i < 1000; i++) {
			suite.addTest(new JUnit4TestAdapter(InsertInvestment.class));
			// suite.addTest(new JUnit4TestAdapter(InsertInvestment2.class));
			// suite.addTest(new JUnit4TestAdapter(InsertInvestment3.class));
			// suite.addTest(new JUnit4TestAdapter(InsertInvestment4.class));
			// suite.addTest(new JUnit4TestAdapter(InsertInvestment5.class));
			// suite.addTest(new JUnit4TestAdapter(InsertInvestment6.class));
			// suite.addTest(new JUnit4TestAdapter(InsertInvestment7.class));
		}
		return suite;
	}
}
