package com.anyoption.autotest.devtests;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.parser.ParseException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.testng.AssertJUnit;

import com.anyoption.autotest.common.DBLayer;
import com.anyoption.autotest.common.User;
import com.anyoption.autotest.utils.SpreadsheetData;

@RunWith(value = Parameterized.class)
public class InsertInvestment10 {

	private String apiURL;

	private static StringBuffer verificationErrors = new StringBuffer();
	static DBLayer dB = new DBLayer();

	// private String testEnvURL;

	private String username;
	private String password;
	private String skinId;
	private String currencyId;

	@Parameters
	public static Collection<?> testData() throws Exception {
		InputStream spreadsheet = new FileInputStream(
				"C:\\Users\\tsvetelinad\\Desktop\\PavlinLoadTest150715.xlsx"); // CopyopInvestments.xlsx
		return new SpreadsheetData(spreadsheet).getData();
	}

	public InsertInvestment10(String username, String password, String skinId,
			String currencyId) {
		this.username = username;
		this.password = password;
		this.skinId = skinId;
		this.currencyId = currencyId;
	}

	@Before
	public void setUp() throws Exception {
		// this.apiURL =
		// "http://ww1.copyop.com/jsonService/AnyoptionService/insertInvestment";
		this.apiURL = "https://www.bgtestenv.anyoption.com/jsonService/AnyoptionService/insertInvestment";
	}

	@After
	public void tearDown() throws Exception {
		// quit from WebDriver

		if (dB != null) {
			dB.closeConnection();
		}

		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			AssertJUnit.fail(verificationErrorString);
		}
	}

	@Test
	public void insertInvestment() throws Exception {
		// Thread.sleep(5 * 1000);
		try {
			@SuppressWarnings("deprecation")
			HttpClient c = new DefaultHttpClient();
			HttpPost p = new HttpPost(this.apiURL);

			// p.setEntity(new StringEntity("{\"username\":\"" +
			// this.apiusername
			// + "\",\"password\":\"" + this.apipassword + "\"}",
			// ContentType.create("application/json")));

			String investmentAmount = "";
			if (currencyId.equals("5")) {
				investmentAmount = "500";
			} else if (currencyId.equals("6")) {
				investmentAmount = "5000";
			} else {
				investmentAmount = "150";
			}
			// ArrayList<String[]> resultOpportunitiesId = User
			// .getOpenOpportunitiesFromDB("16,20,132,136,137, 251"); //
			// String opportunityId = resultOpportunitiesId.get(0)[0];
			// String opportunityMarketId = resultOpportunitiesId.get(0)[1];
			ArrayList<String[]> resultOpportunitiesId = User
					.getOpenOpportunitiesFromDB();
			int i = (int) Math.floor(resultOpportunitiesId.size()
					* Math.random());
			String opportunityId = resultOpportunitiesId.get(i)[0];
			String opportunityMarketId = resultOpportunitiesId.get(i)[1];
			System.out.println("Opertunity ID is: " + opportunityId
					+ " Market id is: " + opportunityMarketId);

			ArrayList<String[]> resultClosingLevel = User
					.getClosingLevelFromDB(opportunityId);
			String closingLevel = resultClosingLevel.get(0)[0];

			ArrayList<String[]> resultMarketRates = User
					.getMarketRatesFromDB(opportunityMarketId);
			String levelGraphRate = resultMarketRates.get(0)[1];
			// String requestAmount = "150";
			String choice = ""; // 1-CALL, 2-PUT
			double dblClosingLevel = Double.parseDouble(closingLevel);
			double dblLevelGraphRate = Double.parseDouble(levelGraphRate);
			System.out.println("Closing level is: " + dblClosingLevel);
			System.out.println("LevelGraphRate is: " + dblLevelGraphRate);
			if (dblClosingLevel > dblLevelGraphRate) {
				choice = "1"; // 1-CALL
				System.out.println("Investment is: CALL");
				// //AABF5621-C806-4B0F-877A-A2C2CBB2E0F4

				String jsonStringRequest = " {\"osVersion\":\"8.1\",\"ip\":\"null\",\"writerId\":\"20000\","
						+ "\"serviceMethod\":\"insertInvestment\",\"isTablet\":\"false\","
						+ "\"deviceId\":\"6D19CA00-1837-461D-9556-26B81D2C890C\","
						+ "\"writeId\":\"0\",\"deviceType\":\"x86_64\",\"appVersion\":\"22.0.14\","
						+ "\"utcOffset\":\"-180\",\"choice\":\""
						+ choice
						+ "\",\"encrypt\":\"null\","
						+ "\"isFromGraph\":\"false\",\"pageOddsWin\":\"1.7\","
						+ "\"opportunityId\":\""
						+ opportunityId
						+ "\",\"pageLevel\":\""
						+ levelGraphRate
						+ "\","
						+ "\"requestAmount\":\""
						+ investmentAmount
						+ "\",\"userName\":\""
						+ username
						+ "\","
						+ "\"password\":\""
						+ password
						+ "\","
						+ "\"skinId\":\""
						+ skinId
						+ "\","
						+ "\"pageOddsLose\":\"0.15\"}";

				/*
				 * String jsonStringRequest = "{appVersion = \"2.0.20\"; " +
				 * " choice = " + choice +
				 * "; deviceId = \"35368273-BB4D-400A-B2B1-555D315F6487\"; " +
				 * "deviceType = \"x86_64\"; encrypt = \"<null>\"; ip = \"<null>\"; "
				 * + "isFromGraph = 0; isTablet = 0; " + "opportunityId = " +
				 * opportunityId + ";" + "osVersion = \"8.4\";" +
				 * "pageLevel = \"" + levelGraphRate + "\";" +
				 * "pageOddsLose = \"0.15\"; pageOddsWin = \"1.7\";" +
				 * "password = \"" + password + "\";" + "requestAmount = " +
				 * investmentAmount + ";" + "serviceMethod = insertInvestment;"
				 * + "skinId = " + skinId + ";" + "userName = " + username + ";"
				 * + "utcOffset = \"-180\"; writeId = 0; writerId = 200;}";
				 */
				System.out.println("JSON is: " + jsonStringRequest);
				p.setEntity(new StringEntity(jsonStringRequest, ContentType
						.create("application/json")));

				HttpResponse r = c.execute(p);
				System.out.println("Response is: " + r);

			} else if (dblClosingLevel < dblLevelGraphRate) {
				choice = "2"; // 2-PUT
				System.out.println("Investment is: PUT");

				String jsonStringRequest = " {\"osVersion\":\"8.1\",\"ip\":\"null\",\"writerId\":\"20000\","
						+ "\"serviceMethod\":\"insertInvestment\",\"isTablet\":\"false\","
						+ "\"deviceId\":\"AABF5621-C806-4B0F-877A-A2C2CBB2E0F4\","
						+ "\"writeId\":\"0\",\"deviceType\":\"x86_64\",\"appVersion\":\"0.1.4\","
						+ "\"utcOffset\":\"-180\",\"choice\":\""
						+ choice
						+ "\",\"encrypt\":\"null\","
						+ "\"isFromGraph\":\"false\",\"pageOddsWin\":\"1.7\","
						+ "\"opportunityId\":\""
						+ opportunityId
						+ "\",\"pageLevel\":\""
						+ levelGraphRate
						+ "\","
						+ "\"requestAmount\":\""
						+ investmentAmount
						+ "\",\"userName\":\""
						+ username
						+ "\","
						+ "\"password\":\""
						+ password
						+ "\","
						+ "\"skinId\":\""
						+ skinId
						+ "\","
						+ "\"pageOddsLose\":\"0.15\"}";

				/*
				 * String jsonStringRequest = "{appVersion = \"2.0.20\"; " +
				 * " choice = " + choice +
				 * "; deviceId = \"35368273-BB4D-400A-B2B1-555D315F6487\"; " +
				 * "deviceType = \"x86_64\"; encrypt = \"<null>\"; ip = \"<null>\"; "
				 * + "isFromGraph = 0; isTablet = 0; " + "opportunityId = " +
				 * opportunityId + ";" + "osVersion = \"8.4\";" +
				 * "pageLevel = \"" + levelGraphRate + "\";" +
				 * "pageOddsLose = \"0.15\"; pageOddsWin = \"1.7\";" +
				 * "password = \"" + password + "\";" + "requestAmount = " +
				 * investmentAmount + ";" + "serviceMethod = insertInvestment;"
				 * + "skinId = " + skinId + ";" + "userName = " + username + ";"
				 * + "utcOffset = \"-180\"; writeId = 0; writerId = 200;}";
				 */
				System.out.println("JSON is: " + jsonStringRequest);
				p.setEntity(new StringEntity(jsonStringRequest, ContentType
						.create("application/json")));

				HttpResponse r = c.execute(p);
				System.out.println("Response is: " + r);
			} else {
				System.out
						.println("!!!!!!!!!!DO NOT INVEST!!!!!!!!! BECAUSE CLOSING LEVEL = LEVEL GRAPH RATE");
			}

			// BufferedReader rd = new BufferedReader(new InputStreamReader(r
			// .getEntity().getContent()));
			// String line = "";
			// while ((line = rd.readLine()) != null) {
			// // Parse our JSON response
			// JSONParser j = new JSONParser();
			// JSONObject o = (JSONObject) j.parse(line);
			// Map response = (Map) o.get("response");
			//
			// System.out.println(response.get("somevalue"));
			// }
		} catch (ParseException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		}

	}
}
