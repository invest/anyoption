package com.anyoption.autotest.devtests;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.testng.AssertJUnit;

import com.anyoption.autotest.common.DBLayer;
import com.anyoption.autotest.utils.SpreadsheetData;

@RunWith(value = Parameterized.class)
public class LoginThruJSON14 {

	private String apiURL;

	private static StringBuffer verificationErrors = new StringBuffer();
	static DBLayer dB = new DBLayer();

	// private String testEnvURL;

	private String username;
	private String password;
	private String skinId;

	@Parameters
	public static Collection<?> testData() throws Exception {
		InputStream spreadsheet = new FileInputStream(
				"C:\\Users\\tsvetelinad\\Desktop\\AsparuhLogins.xlsx");// BGTestCopyOp.xlsx,
																		// 123456.xlxs,CopyopInvestments.xlsx
		// TestInvFromEclipse
		return new SpreadsheetData(spreadsheet).getData();
	}

	public LoginThruJSON14(String username, String password, String skinId) {
		this.username = username;
		this.password = password;
		this.skinId = skinId;
	}

	@Before
	public void setUp() throws Exception {
		// this.apiURL =
		// "http://ww1.copyop.com/jsonService/AnyoptionService/insertInvestment";
		this.apiURL = "http://www.bgtestenv.anyoption.com/jsonService/AnyoptionService/getCopyopUser";
	}

	@After
	public void tearDown() throws Exception {
		// quit from WebDriver

		if (dB != null) {
			dB.closeConnection();
		}

		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			AssertJUnit.fail(verificationErrorString);
		}
	}

	@Test
	public void insertInvestment() throws Exception {
		try {
			@SuppressWarnings("deprecation")
			HttpClient c = new DefaultHttpClient();
			HttpPost p = new HttpPost(this.apiURL);
			String jsonStringRequest = "{afterDeposit: false, encrypt: false, fingerPrint: 2981612959, isLogin: false, locale: \"\", password: \""
					+ password
					+ "\", skinId: "
					+ skinId
					+ ", userName: \""
					+ username + "\", utcOffset: -120, writerId: 10000}";
			// //AABF5621-C806-4B0F-877A-A2C2CBB2E0F4
			/*
			 * String jsonStringRequest = "{appVersion=\"22.0.14\";" +
			 * "deviceId=\"6D19CA00-1837-461D-9556-26B81D2C890C\";" +
			 * "deviceType=\"x86_64\";" + "encrypt=\"0\";isTablet=\"0\";" +
			 * "osVersion=\"8.1\";" + "password=\"" + password + "\";" +
			 * "skinId=\"" + skinId + "\";" + "userName=\"" + username + "\";" +
			 * "utcOffset=\"-120\";" + "writerId=\"20000\";}";
			 */
			System.out.println("JSON is: " + jsonStringRequest);
			p.setEntity(new StringEntity(jsonStringRequest, ContentType
					.create("application/json")));

			HttpResponse r = c.execute(p);

		} catch (IOException e) {
			System.out.println(e);
		}

	}
}
