package com.anyoption.autotest.devtests;

//
//import org.junit.runner.RunWith;
//import org.junit.runners.Suite.SuiteClasses;
//
//import utilities.ConcurrentSuite;
//
////@RunWith(ConcurrentJunitRunner.class)
////@SuiteClasses({ InsertInvestment.class, InsertInvestment2.class })
////public class AllTestsInvest {
////	Class[] cls = { InsertInvestment.class, InsertInvestment2.class };
////
////	// simultaneously all methods in all classes
////	org.junit.runner.Result result = JUnitCore.runClasses(new ParallelComputer(
////			true, true), cls);
////
////}
//
//@RunWith(ConcurrentSuite.class)
//@SuiteClasses({ InsertInvestment.class, InsertInvestment2.class })
//public class AllTestsInvest {
//}

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.anyoption.autotest.utils.Concurrent;
import com.anyoption.autotest.utils.ConcurrentSuite;

/**
 * @author Mathieu Carbou (mathieu.carbou@gmail.com)
 */

@RunWith(ConcurrentSuite.class)
@Suite.SuiteClasses({ InsertInvestment6.class, InsertInvestment7.class /*
																		 * ,
																		 * InsertInvestment8
																		 * .
																		 * class
																		 * ,
																		 * InsertInvestment9
																		 * .
																		 * class
																		 * /* ,
																		 * InsertInvestment10
																		 * .
																		 * class
																		 */})
/*
 * InsertInvestment6.class, InsertInvestment7.class, InsertInvestment8.class,
 * InsertInvestment9.class, InsertInvestment10.class InsertInvestment11.class,
 * InsertInvestment12.class, InsertInvestment13.class, InsertInvestment14.class,
 * InsertInvestment15.class, InsertInvestment16.class, InsertInvestment17.class,
 * InsertInvestment18.class, InsertInvestment19.class, InsertInvestment20.class
 */
@Concurrent
public class NewsFeedTest {
}
