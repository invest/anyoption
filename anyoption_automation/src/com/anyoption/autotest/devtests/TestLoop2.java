package com.anyoption.autotest.devtests;

import junit.framework.JUnit4TestAdapter;
import junit.framework.TestSuite;

import org.junit.runner.RunWith;
import org.junit.runners.AllTests;

@RunWith(AllTests.class)
public final class TestLoop2 {

	public static TestSuite suite() {
		TestSuite suite = new TestSuite();
		for (int i = 0; i < 3; i++) {
			suite.addTest(new JUnit4TestAdapter(InsertInvestment6.class));
			suite.addTest(new JUnit4TestAdapter(InsertInvestment7.class));
			// suite.addTest(new JUnit4TestAdapter(InsertInvestment8.class));
			// suite.addTest(new JUnit4TestAdapter(InsertInvestment9.class));
			// suite.addTest(new JUnit4TestAdapter(InsertInvestment10.class));
			// suite.addTest(new JUnit4TestAdapter(InsertInvestment6.class));
			// suite.addTest(new JUnit4TestAdapter(InsertInvestment7.class));
		}
		return suite;
	}
}
