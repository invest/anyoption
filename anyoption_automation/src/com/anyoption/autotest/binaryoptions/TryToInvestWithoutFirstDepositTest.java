package com.anyoption.autotest.binaryoptions;

import java.io.IOException;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.pages.BubblesPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.InvestmentPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.LongTermPage;
import com.anyoption.autotest.pages.OptionPlusPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;

public class TryToInvestWithoutFirstDepositTest extends BaseTest {

	public TryToInvestWithoutFirstDepositTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/TryToInvestWithoutDeposit.xlsx", numberRows, 3);
	    return(retObjArr);
	}
	/**
	 * Tries to invest with user who is never make deposit
	 * @param skin
	 * @param popUpMessage
	 * @param notification
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void tryToInvestWithoutFirstDepositTest(String skin, String popUpMessage, String notification) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoIsCreated(skinId);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, skin);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		InvestmentPage investmentPage = new InvestmentPage(driver);
		OptionPlusPage optionPlusPage = new OptionPlusPage(driver);
		BubblesPage bubblesPage = new BubblesPage(driver);
		LongTermPage longTermPage = new LongTermPage(driver);
		loginPage.loginWithUserFromDB(sqlQuery, false);
		headerMenuPage.openHomePage();
		if (! skin.equals("EN") && driver.getCurrentUrl().contains("bgtestenv")) {
			System.out.println("Bg test problem with redirect");
			if (skin.equalsIgnoreCase("IT")) {
				driver.get("http://www1.bgtestenv.anyoption.it");
			} else {
				driver.get("http://" + skin.toLowerCase() + ".bgtestenv.anyoption.com");
			}	
		}
		int number = 0;
		investmentPage.clickUpButton(number);
		Assert.assertTrue(investmentPage.isPopUpForMakeDepositDisplayed(number, "UP"));
		Assert.assertTrue(investmentPage.getMessageFromPopUpForDeposit(number).replace("\n", " ").contains(popUpMessage));
  		investmentPage.closeNoDepositPopUp(number);
  		investmentPage.clickDownButton(number);
  		Assert.assertTrue(investmentPage.isPopUpForMakeDepositDisplayed(number, "DOWN"));
  		Assert.assertTrue(investmentPage.getMessageFromPopUpForDeposit(number).replace("\n", " ").contains(popUpMessage));
		headerMenuPage.openOptionTradingMenu();
		number = investmentPage.getAcctiveInvestment();
		if (number == -1) {
  			investmentPage.changeMarket(0);
  			number = 0;
  		}
		investmentPage.clickUpButton(number);
		Assert.assertTrue(investmentPage.isPopUpForMakeDepositDisplayed(number, "UP"));
		Assert.assertTrue(investmentPage.getMessageFromPopUpForDeposit(number).replace("\n", " ").contains(popUpMessage));
  		investmentPage.closeNoDepositPopUp(number);
  		investmentPage.clickDownButton(number);
  		Assert.assertTrue(investmentPage.isPopUpForMakeDepositDisplayed(number, "DOWN"));
  		Assert.assertTrue(investmentPage.getMessageFromPopUpForDeposit(number).replace("\n", " ").contains(popUpMessage));
		optionPlusPage.openOptionPlusMenu(browserType);
  		optionPlusPage.setActiveMarket();
  		investmentPage.clickUpButton(0);
  		Assert.assertTrue(investmentPage.isPopUpForMakeDepositDisplayed(0, "UP"));
  		Assert.assertTrue(investmentPage.getMessageFromPopUpForDeposit(0).replace("\n", " ").contains(popUpMessage));
  		investmentPage.closeNoDepositPopUp(0);
  		UtilsMethods.sleep(300);
  		investmentPage.clickDownButton(0);
  		Assert.assertTrue(investmentPage.isPopUpForMakeDepositDisplayed(0, "DOWN"));
  		Assert.assertTrue(investmentPage.getMessageFromPopUpForDeposit(0).replace("\n", " ").contains(popUpMessage));
  		bubblesPage.openBubbles();
		bubblesPage.switchDriverToBubbles();
		bubblesPage.passTutorial();
		bubblesPage.expandBubble();
		int profit = bubblesPage.getProfite();
		if (profit == 0) {
			profit = bubblesPage.moveBubble();
		}
		bubblesPage.clickGoButton();
		Assert.assertEquals(notification, bubblesPage.getNegativeNotification());
		driver.switchTo().defaultContent();
		Assert.assertEquals(popUpMessage, bubblesPage.getNoDepositMessage().replace("\n", " "));
		bubblesPage.closeRegulationPopUp();
		longTermPage.openLongTerm();
		longTermPage.setMarket(1);
  		String id = longTermPage.getOptionId(0);
  		longTermPage.selectUnitsCount(id, "1");
  		longTermPage.buyLongOption(id);
  		Assert.assertTrue(longTermPage.isErrorPopUpDisplayed());
  		Assert.assertEquals(notification, longTermPage.getNoDepositMessage());
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
