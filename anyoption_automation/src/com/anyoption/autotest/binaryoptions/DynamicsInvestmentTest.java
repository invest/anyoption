package com.anyoption.autotest.binaryoptions;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.DynamicsPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.MyAccountPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class DynamicsInvestmentTest extends BaseTest {

	public DynamicsInvestmentTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/DynamicsInvestmentTestData.xlsx", numberRows, 7);
	    return(retObjArr);
	}
	/**
	 * Makes dynamics investment 
	 * @param skin
	 * @param countMessage
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void dynamicsInvestmentTest(String skin, String text1, String text2, String text3, String text4, String text5,
			String countMessage) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		DynamicsPage dynamicsPage = new DynamicsPage(driver);
		MyAccountPage myAccountPage = new MyAccountPage(driver);
		String sqlQuery = UserSQL.getUserWhoCanMakeDynamicsInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		String balance = headerMenuPage.getBalance();
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		String moneyType = balance.replaceAll("\\d+.*", "");
		String email = testData.get("email");
		currentUserId = testData.get("id");
		String userBalanceInDB = testData.get("balance");
		int userBalanceInDBInt = Integer.parseInt(userBalanceInDB);
		dynamicsPage.openDynamics();
		if (! dynamicsPage.isDynamicsTutorialDisplayed() == true) {
			dynamicsPage.openTutorial();
		}
		String tutorialMessages[] = {text1, text2, text3, text4, text5};
		for (int i = 0; i < tutorialMessages.length; i++) {
			Assert.assertEquals(tutorialMessages[i], dynamicsPage.getTutorialMessage(i + 1));
			dynamicsPage.clickTutorialOkButton(i + 1);
			UtilsMethods.sleep(500);
		}
		dynamicsPage.chooseMarket();
		String asset = dynamicsPage.getCurrentMarket();
		String incorrect = dynamicsPage.getAboveIncorrect();
		Assert.assertEquals(incorrect, dynamicsPage.getAboveIncorrect());
		String amount = "100";
		double amountDouble = Double.parseDouble(amount);
		dynamicsPage.setAmount(amount);
		int openedOptionsInChartBefore = dynamicsPage.getOpenedOptionsInChart();
		Assert.assertEquals(0, openedOptionsInChartBefore);
		dynamicsPage.makeAboveInvestment();
		Assert.assertTrue(dynamicsPage.isOptionPurchased());
		Assert.assertEquals(asset, dynamicsPage.getMarketNameFromPurchOption());
		Assert.assertEquals(amount, dynamicsPage.getAmountFromPurchOption(moneyType));
		String level = dynamicsPage.getPurchasedOptionLevel(skin);
		String profit = dynamicsPage.getProfitFromPurchOption();
		double profitDouble = Double.parseDouble(profit);
		String correct = dynamicsPage.getCorrectAmountFromPurchOption(moneyType);
		double correctDouble = Double.parseDouble(correct);
		Assert.assertEquals(Math.round(amountDouble * (1.0 + (profitDouble / 100))), Math.round(correctDouble));
		Assert.assertEquals(incorrect, dynamicsPage.getIncorrectAmountFromPurchOption(moneyType));
		String balanceAfter = headerMenuPage.getBalance();
		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
		String userBalanceInDBAfter = User.getUserBalanceFromDB(email, testData.get("id"));
		int userBalanceInDBAfterInt = Integer.parseInt(userBalanceInDBAfter);
		Assert.assertEquals(balanceInt - Integer.parseInt(amount + "00"), balanceAfterInt);
		Assert.assertEquals(userBalanceInDBInt - Integer.parseInt(amount + "00"), userBalanceInDBAfterInt);
		int openedOptionsInChartAfter = dynamicsPage.getOpenedOptionsInChart();
		Assert.assertEquals(openedOptionsInChartBefore + 1, openedOptionsInChartAfter);
		Assert.assertEquals(countMessage, dynamicsPage.getAboveOpenOptions());
		Assert.assertEquals(moneyType + amount, dynamicsPage.getAboveOpenOptionAmount());
		Assert.assertEquals(moneyType + correct, dynamicsPage.getAboveOpenOptionCorrect());
		User.verifyDynamicsInvestment(testData.get("id"), amount + "00");
		headerMenuPage.openMyAccountMenu();
		Assert.assertEquals(asset, myAccountPage.getAsset());
		Assert.assertEquals(moneyType + amount + ".00", myAccountPage.getInvestmentAmount());
		Assert.assertEquals(moneyType + correct, myAccountPage.getAmountIfCorrect());
		Assert.assertEquals(moneyType + "0.00", myAccountPage.getAmountIfIncorrect());
		Assert.assertEquals(level.replace("0", "").replace(",", ""), myAccountPage.getLevel().replace("0", "").replace(",", ""));
		Assert.assertTrue(myAccountPage.isDynamicsInvestment());
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
