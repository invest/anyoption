package com.anyoption.autotest.binaryoptions;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.DynamicsPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class DynamicsInvestWithIncorrectAmount extends BaseTest {

	public DynamicsInvestWithIncorrectAmount() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/DynamicsInvetsWithIncorrectData.xlsx", numberRows, 3);
	    return(retObjArr);
	}
	/**
	 * Tries to make dynamics investment with incorrect amounts
	 * @param skin
	 * @param minMess
	 * @param notEnoughMess
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void dynamicsInvestWithIncorrectAmount(String skin, String minMess, String notEnoughMess) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		DynamicsPage dynamicsPage = new DynamicsPage(driver);
		String sqlQuery = UserSQL.getUserWhoCanMakeDynamicsInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = testData.get("id");
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		String balance = headerMenuPage.getBalance();
		String moneyType = balance.replaceAll("\\d+.*", "");
		String balanceInDb = testData.get("balance");
		String email = testData.get("email");
		dynamicsPage.openDynamics();
		if (dynamicsPage.isDynamicsTutorialDisplayed() == true) {
			dynamicsPage.closeTutorial();
		}
		dynamicsPage.chooseMarket();
		dynamicsPage.setAmount("24");
		dynamicsPage.clickUpButton();
		String minMassage = dynamicsPage.getNegativeNotification("");
		Assert.assertEquals(minMess, minMassage);
		int balanceInt = Integer.parseInt(balance.substring(0, balance.length() - 3).replace(moneyType, "").replace(",", ""));
		String amount = String.valueOf(balanceInt + 1);
		dynamicsPage.setAmount(amount);
		dynamicsPage.clickUpButton();
		Assert.assertEquals(notEnoughMess, dynamicsPage.getNegativeNotification(minMassage));
		String balanceAfter = headerMenuPage.getBalance();
		Assert.assertEquals(balance, balanceAfter);
		String balanceInDbAfter = User.getUserBalanceFromDB(email, currentUserId);
		Assert.assertEquals(balanceInDb, balanceInDbAfter);
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
