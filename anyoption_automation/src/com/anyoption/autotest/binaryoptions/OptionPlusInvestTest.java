package com.anyoption.autotest.binaryoptions;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.InvestmentPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.MyAccountPage;
import com.anyoption.autotest.pages.OptionConfirmationPage;
import com.anyoption.autotest.pages.OptionPlusPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class OptionPlusInvestTest extends BaseTest {
	public OptionPlusInvestTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/OptionPlusInvestTestData.xlsx", numberRows, 3);
	    return(retObjArr);
	}
	/**
	 * Makes investment from option plus
	 * 	- verifies balance before and after make investment (all positions)
	 * 	- verifies balance and return amounts from my account menu
	 * @param skin
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void optionPlusInvestTest(String skin, String message, String confirmTitle) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		OptionConfirmationPage optionConfirmationPage = new OptionConfirmationPage(driver);
		String sqlQuery = UserSQL.getUserWhoCanMakeInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		InvestmentPage investmentPage = new InvestmentPage(driver);
		OptionPlusPage optionPlusPage = new OptionPlusPage(driver);
		optionPlusPage.openOptionPlusMenu(browserType);
		String balance = headerMenuPage.getBalance();
		String moneyType = balance.replaceAll("\\d+.*", "");
		String email = testData.get("email");
  		currentUserId = testData.get("id");
  		String userBalanceInDB = testData.get("balance");
  		optionPlusPage.setActiveMarket();
  		String profitBalanceBefore = optionPlusPage.getProfitBalance();
  		Assert.assertEquals(balance, profitBalanceBefore);
  		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
  		int totalinvestmentBefore = optionPlusPage.getTotalInvestment();
  		optionPlusPage.chooseAmountToInvest(0);
  		String amount = optionPlusPage.getAmountForInvest();
  		Assert.assertTrue(amount.contains(moneyType));
  		int amountInt = Integer.parseInt(amount.replaceAll("[^\\d]", "")) * 100;
  		if (amountInt > balanceInt) {
  			amount = "30";
  			optionPlusPage.setCustomAmountToInvest(amount);
  			amountInt = Integer.parseInt(amount.replaceAll("[^\\d]", "")) * 100;
  		}
  		String percents = optionPlusPage.getProfitPercents().replace("%", "");
  		double percenCoefProfit = (Double.parseDouble(percents) / 100) + 1;
  		double percentCoefRefund = 0.15;
  		String correctValue = optionPlusPage.getReturnValueIfCorrect();
  		Assert.assertTrue(correctValue.contains(moneyType));
  		int correctValueInt = Integer.parseInt(correctValue.replaceAll("[^\\d]", ""));
  		Assert.assertEquals((int)(amountInt * percenCoefProfit), correctValueInt);
  		String incorrectValue = optionPlusPage.getReturnValueIfIncorrect();
  		Assert.assertTrue(incorrectValue.contains(moneyType));
  		int incorrectValueInt = Integer.parseInt(incorrectValue.replaceAll("[^\\d]", ""));
  		Assert.assertEquals((int)(amountInt * percentCoefRefund), incorrectValueInt);
  		String asset = optionPlusPage.clickUpButton();
  		Assert.assertTrue(investmentPage.afterInvest(0, false));
  		String investmentId = investmentPage.getInvestmentId(0);
  		String expiresTime = investmentPage.getExpiresTime(0);
  		String amountInAfterInvestPopUp = investmentPage.getAmountFormAfterInvestPopUp(0);
  		String level = investmentPage.getLevelFromAfterInvest(0);
  		investmentPage.openPrintInvestmentWindow(0);
  		String profit[] = new String[2];
  		profit[0] = percents;
  		profit[1] = "15";
  		double amountDouble = Double.parseDouble(amount.replace(moneyType, ""));
  		amountDouble = amountDouble + 1.00;
  		Assert.assertEquals(moneyType + String.valueOf(amountDouble) + "0", amountInAfterInvestPopUp);
  		Assert.assertEquals(correctValueInt, investmentPage.getCorrectAmountFromAfterInvestPopUp(0));
  		Assert.assertEquals(incorrectValueInt, investmentPage.getIncorrectAmountFromAfterInvestPopUp(0));
  		Assert.assertEquals(message, investmentPage.getCommissionMessage());
  		String winHandleBefore = driver.getWindowHandle();
  		for (String winHandle : driver.getWindowHandles()) {
  		    driver.switchTo().window(winHandle);
  		}
  		Assert.assertEquals(confirmTitle, optionConfirmationPage.getOptionConfirmationTitle());
  		Assert.assertEquals(asset, optionConfirmationPage.getOptionConfirmationMarket());
  		if (level.replace(",", "") != optionConfirmationPage.getOptionConfirmationLevel()) {
  			Assert.assertEquals(level.replace(",", "").replace("0", "").replace(".", ""), 
  					optionConfirmationPage.getOptionConfirmationLevel().replace("0", "").replace(".", ""));
  		}	
  		Assert.assertEquals(moneyType + amount.replace(moneyType, "") + ".00", optionConfirmationPage.getOptionConfirmationInvestmentAmount());
  		Assert.assertEquals(correctValue, optionConfirmationPage.getOptionConfirmationCorrectValue().replace(".", ""));
  		Assert.assertEquals(incorrectValue, optionConfirmationPage.getOptionConfirmationIncorrectValue().replace(".", ""));
  		Assert.assertEquals("CALL", optionConfirmationPage.getOptionConfirmationType());
  		String confirmationExpTime = optionConfirmationPage.getOptionConfirmationExpireTime();
  		driver.close();
  		driver.switchTo().window(winHandleBefore);
  		String balanceAfter = headerMenuPage.getBalance();
  		String userBalanceInDBAfter = User.getUserBalanceFromDB(email, currentUserId);
  		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
  		int optionPlusAmount = amountInt + 100;
  		Assert.assertEquals(balanceInt - optionPlusAmount, balanceAfterInt);
  		int totalInvestmentAfter = optionPlusPage.getTotalInvestment();
  		String profitBalanceAfter = optionPlusPage.getProfitBalance();
  		Assert.assertEquals(balanceAfter, profitBalanceAfter);
  		Assert.assertEquals(totalinvestmentBefore + optionPlusAmount, totalInvestmentAfter);
  		Assert.assertEquals(Integer.parseInt(userBalanceInDB) - optionPlusAmount, Integer.parseInt(userBalanceInDBAfter));
  		amountInAfterInvestPopUp = amountInAfterInvestPopUp.replaceAll("[^\\d]", "");
  		User.verifyInvestmentData(investmentId, testData.get("id"), "1", amountInAfterInvestPopUp, level, profit);
  		headerMenuPage.openMyAccountMenu();
  		MyAccountPage myAccountPage = new MyAccountPage(driver);
  		String amountFromMyAccount = myAccountPage.getInvestmentAmount();
  		int amountFromMyAccountInt = Integer.parseInt(amountFromMyAccount.replaceAll("[^\\d]", ""));
  		Assert.assertEquals(investmentId, myAccountPage.getOptionId());
  		Assert.assertEquals(asset, myAccountPage.getAsset());
  		String levelFromMyAcc = myAccountPage.getLevel();
  		if (! level.equals(levelFromMyAcc)) {
  			Assert.assertEquals(level.replace("0", "").replace(".", ""), levelFromMyAcc.replace("0", "").replace(".", ""));
  		}	
  		String myAccountEpiryTyme = myAccountPage.getExpiryTime();
  		if (! expiresTime.equals(myAccountEpiryTyme)) {
  			Assert.assertTrue(myAccountEpiryTyme.contains(expiresTime));
  		}	
  		Assert.assertEquals(optionPlusAmount, amountFromMyAccountInt);
  		Assert.assertTrue(amountFromMyAccount.contains(moneyType));
  		String correctFromMyAcc = myAccountPage.getAmountIfCorrect();
  		Assert.assertEquals(correctValue, correctFromMyAcc.replace(".", ""));
  		String incorrectFromMyAcc = myAccountPage.getAmountIfIncorrect();
  		Assert.assertEquals(incorrectValue, incorrectFromMyAcc.replace(".", ""));
  		Assert.assertEquals(confirmationExpTime.replace(" ", ""), myAccountPage.getExpiryTimeText().replace(" ", ""));
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
