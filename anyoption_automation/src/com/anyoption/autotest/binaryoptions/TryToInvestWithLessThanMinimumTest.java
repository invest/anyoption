package com.anyoption.autotest.binaryoptions;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.InvestmentPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.OptionPlusPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class TryToInvestWithLessThanMinimumTest extends BaseTest {
	public TryToInvestWithLessThanMinimumTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/TryToInvestWithLessThanMinimumTestData.xlsx", numberRows, 2);
	    return(retObjArr);
	}
	/**
	 * Tries to make investment with less than minimum amount(25)
	 *  - checks binary trades
	 *  - checks option plus menu
	 * @throws Exception 
	 * 
	 */
	@Test(dataProvider = "dp")
	public void tryToInvestWithLessThanMinimumTest(String skin, String message) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		String sqlQuery = UserSQL.getUserWhoCanNotMakeInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = testData.get("id");
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		InvestmentPage investmentPage = new InvestmentPage(driver);
		String balance = headerMenuPage.getBalance();
		String moneyType = balance.replaceAll("\\d+.*", "");
		String email = testData.get("email");
		String userBalanceInDB = testData.get("balance");
  		int number = investmentPage.getAcctiveInvestment();
  		if (number == -1) {
  			investmentPage.changeMarket(0);
  			number = 0;
  		}
  		String amount = "24";
  		investmentPage.setAmountToInvest(number, amount);
  		String amountToInvest = investmentPage.getTheAmountForInvest(number);
  		Assert.assertEquals(moneyType + amount, amountToInvest);
  		investmentPage.clickUpButton(number);
  		Assert.assertTrue(investmentPage.isErrorMessageDisplayed(number, "UP"));
  		Assert.assertEquals(message, investmentPage.getErrorMessage(number));
  		investmentPage.closeFooterMessage(number);
  		investmentPage.clickDownButton(number);
  		Assert.assertTrue(investmentPage.isErrorMessageDisplayed(number, "DOWN"));
  		Assert.assertEquals(message, investmentPage.getErrorMessage(number));
  		investmentPage.closeFooterMessage(number);
  		OptionPlusPage optionPlusPage = new OptionPlusPage(driver);
  		optionPlusPage.openOptionPlusMenu(browserType);
  		optionPlusPage.setActiveMarket();
  		optionPlusPage.setCustomAmountToInvest(amount);
  		String amountToInvestOptionPlus = optionPlusPage.getAmountForInvest();
  		Assert.assertEquals(moneyType + amount, amountToInvestOptionPlus);
  		Assert.assertEquals(moneyType + amount, amountToInvest);
  		investmentPage.clickUpButton(0);
  		Assert.assertTrue(investmentPage.isErrorMessageDisplayed(0, "UP"));
  		Assert.assertEquals(message, investmentPage.getErrorMessage(0));
  		investmentPage.closeFooterMessage(0);
  		investmentPage.clickDownButton(0);
  		Assert.assertTrue(investmentPage.isErrorMessageDisplayed(0, "DOWN"));
  		Assert.assertEquals(message, investmentPage.getErrorMessage(0));
  		investmentPage.closeFooterMessage(0);
  		String balanceAfter = headerMenuPage.getBalance();
  		Assert.assertEquals(balance, balanceAfter);
  		String userBalanceInDBAfter = User.getUserBalanceFromDB(email, currentUserId);
  		Assert.assertEquals(userBalanceInDB, userBalanceInDBAfter);
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
