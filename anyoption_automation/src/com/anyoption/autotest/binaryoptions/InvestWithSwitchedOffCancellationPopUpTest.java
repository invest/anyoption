package com.anyoption.autotest.binaryoptions;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.InvestmentPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.LongTermPage;
import com.anyoption.autotest.pages.OptionPlusPage;
import com.anyoption.autotest.pages.SettingsPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class InvestWithSwitchedOffCancellationPopUpTest extends BaseTest {

	public InvestWithSwitchedOffCancellationPopUpTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/InvestWithSwitchedOffCancelTestData.xlsx", numberRows, 4);
	    return(retObjArr);
	}
	/**
	 * Makes investments with switched off slider for cancel investment pop up
	 * @param skin
	 * @param title
	 * @param description
	 * @param popUpText
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void investWithSwitchedOffCancellationPopUpTest(String skin, String title, String description, String popUpText) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		int timeOut = 5;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		String sqlQuery = UserSQL.getUserWhoCanMakeInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = testData.get("id");
		SettingsPage settingsPage = new SettingsPage(driver);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		InvestmentPage investmentPage = new InvestmentPage(driver);
		OptionPlusPage optionPlusPage = new OptionPlusPage(driver);
		LongTermPage longTermPage = new LongTermPage(driver);
		headerMenuPage.openMyAccountMenu();
		settingsPage.openSettings();
		settingsPage.switchOffSlider();
		Assert.assertEquals(title, settingsPage.getScreenTitle());
		Assert.assertEquals(description, settingsPage.getDescription());
		Assert.assertEquals(popUpText, settingsPage.getPopUpText());
		User.verifyShowCancelInv(currentUserId, "0");
		headerMenuPage.openOptionTradingMenu();
		if (! skin.equalsIgnoreCase("EN") && driver.getCurrentUrl().contains("bgtestenv")) {
			System.out.println("Bg test problem with redirect");
			if (skin.equalsIgnoreCase("IT")) {
				driver.get("http://www1.bgtestenv.anyoption.it/binary-options-trading");
			} else {
				driver.get("http://" + skin.toLowerCase() + ".bgtestenv.anyoption.com/binary-options-trading");
			}	
  		}
  		int number = investmentPage.getAcctiveInvestment();
  		if (number == -1) {
  			investmentPage.changeMarket(0);
  			number = 0;
  		}
  		String amount = "25";
  		investmentPage.setAmountToInvest(number, amount);
  		investmentPage.makeBinaryOptionInvestWithRecursion(number);
  		Assert.assertFalse(investmentPage.isCancelInvestmentDisplayed(number, timeOut));
  		optionPlusPage.openOptionPlusMenu(browserType);
  		optionPlusPage.setActiveMarket();
  		optionPlusPage.setCustomAmountToInvest(amount);
  		investmentPage.makeBinaryOptionInvestWithRecursion(0);
  		Assert.assertFalse(investmentPage.isCancelInvestmentDisplayed(0, timeOut));
  		longTermPage.openLongTerm();
  		longTermPage.setMarket(1);
  		String id = longTermPage.getOptionId(0);
  		longTermPage.selectUnitsCount(id, "1");
  		longTermPage.buyLongOption(id);
  		number = -2;// Long term investment param
  		Assert.assertFalse(investmentPage.isCancelInvestmentDisplayed(number, timeOut));
	}
	/**
	 * Switches slider on
	 * @throws Exception 
	 */
	@Parameters("browser")
	@AfterMethod
	public void switchSliderOn(ITestResult result, @Optional String browser) throws Exception {
		SettingsPage settingsPage = new SettingsPage(driver);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		headerMenuPage.openMyAccountMenu();
		settingsPage.openSettings();
		if (settingsPage.isSliderOn() == false) {
			settingsPage.switchSliderOn();
		}
		User.verifyShowCancelInv(currentUserId, "1");
		driver.switchTo().defaultContent();
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
