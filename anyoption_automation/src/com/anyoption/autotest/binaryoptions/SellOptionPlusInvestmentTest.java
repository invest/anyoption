package com.anyoption.autotest.binaryoptions;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.InvestmentPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.MyAccountPage;
import com.anyoption.autotest.pages.OptionPlusPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class SellOptionPlusInvestmentTest extends BaseTest {

	public SellOptionPlusInvestmentTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/SellOptionPlusInvestmentTestData.xlsx", numberRows, 7);
	    return(retObjArr);
	}
	/**
	 * Makes option plus investment and sell option
	 * @param skin
	 * @param offerMess
	 * @param noLongerMess
	 * @param newQuoteMess
	 * @param soldMess
	 * @param soldAmountMess
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void sellOptionPlusInvestmentTest(String skin, String offerMess, String noLongerMess, String newQuoteMess, String soldMess, 
			String soldAmountMess, String noOptionsMess) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		String sqlQuery = UserSQL.getUserWhoCanMakeInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = testData.get("id");
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		InvestmentPage investmentPage = new InvestmentPage(driver);
		OptionPlusPage optionPlusPage = new OptionPlusPage(driver);
		MyAccountPage myAccountPage = new MyAccountPage(driver);
		optionPlusPage.openOptionPlusMenu(browserType);
  		optionPlusPage.setActiveMarket();
  		optionPlusPage.setCustomAmountToInvest("100");
  		String amount = optionPlusPage.getAmountForInvest();
  		int amountInt = Integer.parseInt(amount.replaceAll("[^\\d]", "")) * 100;
  		int optionPlusAmount = amountInt + 100;
  		optionPlusPage.clickUpButton();
  		Assert.assertTrue(investmentPage.afterInvest(0));
  		String optionId = investmentPage.getInvestmentId(0);
  		String balance = headerMenuPage.getBalance();
		String moneyType = balance.replaceAll("\\d+.*", "");
		String userBalanceInDB = User.getUserBalanceFromDB(testData.get("email"), currentUserId);
		int userBalanceInDBInt = Integer.parseInt(userBalanceInDB);
		for (int i = 0; i < 2; i++) {
			optionPlusPage.clickSellButton(skin);
			if (optionPlusPage.isSellPopUpDisplayed() == true) {
				break;
			} else {
				System.out.println("Sell option plus error");
				Assert.assertTrue(optionPlusPage.isErrorPopUpDisplayed());
			}
		}
		Assert.assertTrue(optionPlusPage.isSellPopUpDisplayed());
		Assert.assertEquals(offerMess, optionPlusPage.getOfferMessage(skin));
		optionPlusPage.waitOfferToExpire();
		Assert.assertEquals(noLongerMess, optionPlusPage.getExpiredOfferText());
		Assert.assertEquals(newQuoteMess, optionPlusPage.getExpiredOfferText(skin));
		for (int i = 0; i < 2; i++) {
			optionPlusPage.getNewQuote();
			if (optionPlusPage.isSellPopUpDisplayed() == true) {
				break;
			} else {
				System.out.println("Sell option plus error");
				Assert.assertTrue(optionPlusPage.isErrorPopUpDisplayed());
			}
		}	
		Assert.assertTrue(optionPlusPage.isSellPopUpDisplayed());
		String sellAmount = optionPlusPage.getSellAmount();
		Assert.assertTrue(sellAmount.contains(moneyType));
		optionPlusPage.sellOption();
		Assert.assertTrue(optionPlusPage.isOfferSold());
		Assert.assertEquals(soldMess, optionPlusPage.getSoldOfferMessage());
		Assert.assertEquals(soldAmountMess, optionPlusPage.getSoldOfferAmountMessage());
		String soldAmount = optionPlusPage.getSoldOfferAmount();
		Assert.assertEquals(sellAmount, soldAmount);
		soldAmount = soldAmount.replace(moneyType, "");
		String balanceAfter = headerMenuPage.getBalance();
		balanceAfter = balanceAfter.replaceAll("[^\\d]", "");
		String userBalanceInDbAfter = User.getUserBalanceFromDB(testData.get("email"), currentUserId);
		int userBalanceInDbAfterInt = Integer.parseInt(userBalanceInDbAfter);
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		int balanceAfterInt = Integer.parseInt(balanceAfter);
		int soldAmountInt = Integer.parseInt(soldAmount);
		if (balanceInt + soldAmountInt != balanceAfterInt) {
			if (balanceInt + soldAmountInt + 1 != balanceAfterInt) {
				Assert.assertEquals(balanceInt + soldAmountInt - 1, balanceAfterInt);
			}	
		} 
		if (userBalanceInDBInt + soldAmountInt != userBalanceInDbAfterInt) {
			if (userBalanceInDBInt + soldAmountInt + 1 != userBalanceInDbAfterInt) {
				Assert.assertEquals(userBalanceInDBInt + soldAmountInt - 1, userBalanceInDbAfterInt);
			}	
		}	
		User.verifyOptionPlusSoldInvestment(optionId, currentUserId, optionPlusAmount, soldAmountInt);
		headerMenuPage.openMyAccountMenu();
		Assert.assertEquals(noOptionsMess, myAccountPage.getNoOptionsMessage());
		myAccountPage.clickClosedInvestmentRadioButton();
		myAccountPage.clickSubmitButton();
		String level = myAccountPage.getSoldOptionLevel();
		String returnValue = myAccountPage.getAmountIfIncorrect();
		Assert.assertEquals(level, "Option+");
		if (! returnValue.replace(".", "").equals(moneyType + soldAmount)) {
			soldAmountInt = soldAmountInt - 1;
			Assert.assertEquals(returnValue.replace(".", ""), moneyType + String.valueOf(soldAmountInt));
		}	
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		driver.switchTo().defaultContent();
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
