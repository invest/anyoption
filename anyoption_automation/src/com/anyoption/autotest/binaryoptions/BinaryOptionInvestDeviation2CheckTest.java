package com.anyoption.autotest.binaryoptions;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.InvestmentPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.MyAccountPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class BinaryOptionInvestDeviation2CheckTest extends BaseTest {
	public BinaryOptionInvestDeviation2CheckTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/BinaryOptionInvestTestData.xlsx", numberRows, 1);
	    return(retObjArr);
	}
	/**
	 * Makes binary option invest and check deviation 2
	 * @param skin
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void binaryOptionInvestDeviation2CheckTest(String skin) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		String sqlQuery = UserSQL.getUserWhoCanMakeDeviationCheckInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		InvestmentPage investmentPage = new InvestmentPage(driver);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		String balance = headerMenuPage.getBalance();
		String moneyType = balance.replaceAll("\\d+.*", "");
		String email = testData.get("email");
		currentUserId = testData.get("id");
		String userBalanceInDB = testData.get("balance");
		int userBalanceInDBInt = Integer.parseInt(userBalanceInDB) / 100;
  		int number = investmentPage.getAcctiveInvestment();
  		if (number == -1) {
  			investmentPage.chooseActiveMarket(0, "");
  			number = 0;
  		}
  		String currentMarket = investmentPage.getMarketName(number);
  		HashMap<String, String> dataToInvest = new HashMap<>();
  		for (int i = 0; i < 10; i++) {
  			dataToInvest = investmentPage.getDataForDev2Check(number);
  			boolean isMarketActive = investmentPage.isMarketActive(number);
  			if (dataToInvest.isEmpty() || (userBalanceInDBInt < Integer.parseInt(dataToInvest.get("amount"))) || isMarketActive == false) {
  				investmentPage.chooseActiveMarket(number, currentMarket);
  			} else {
  				System.out.println(dataToInvest);
  				break;
  			}
  		}
  		String amountToInvest = dataToInvest.get("amount");
  		int amountToInvestInt = Integer.parseInt(amountToInvest);
  		investmentPage.setAmountToInvest(number, amountToInvest);
  		investmentPage.clickProfitDropDown(number);
  		String chooseProfit[] = investmentPage.chooseProfit(number, 0);
  		String profitToInvest = investmentPage.getTheProfit(number);
  		Assert.assertEquals(chooseProfit[0] + "%", profitToInvest);
  		int correct = investmentPage.getReturnValueIfCorrect(number);
  		int incorrect = investmentPage.getReturnValueIfIncorrect(number);
  		String incorectForVerificationInMyAcc = investmentPage.getIncorectAmount(number);
  		double profitPercent = Double.parseDouble(chooseProfit[0]);
  		double refundPercent = Double.parseDouble(chooseProfit[1]);
  		double returnValue = amountToInvestInt * (1 + (profitPercent / 100)) * 100;
  		returnValue = Math.round(returnValue);
  		Assert.assertEquals(correct, (int)returnValue);
  		returnValue = (amountToInvestInt * (refundPercent / 100)) * 100;
  		Assert.assertEquals(incorrect, (int)returnValue);
  		String asset = investmentPage.getMarketName(number);
  		for (int i = 0; i < 6; i++) {
  			investmentPage.clickUpButton(number);
  			if (investmentPage.isCancelInvestmentDisplayed(number) == true) {
  				break;
  			}
  		}
  		Assert.assertTrue(investmentPage.isCancelInvestmentDisplayed(number));
  		String secs = investmentPage.getTimeToCancel(number);
  		if (Integer.parseInt(dataToInvest.get("secs")) - 1 != Integer.parseInt(secs)) {
  			Assert.assertEquals(Integer.parseInt(dataToInvest.get("secs")) - 2, Integer.parseInt(secs));
  		}	
  		Assert.assertTrue(investmentPage.afterInvest(number, true));
  		String amountAfterInvestPopUp = investmentPage.getAmountFormAfterInvestPopUp(number);
  		Assert.assertEquals(moneyType + amountToInvest + ".00", amountAfterInvestPopUp.replace(",", ""));
  		Assert.assertEquals(correct, investmentPage.getCorrectAmountFromAfterInvestPopUp(number));
  		Assert.assertEquals(incorrect, investmentPage.getIncorrectAmountFromAfterInvestPopUp(number));
  		String investmentId = investmentPage.getInvestmentId(number);
  		String expiresTime = investmentPage.getExpiresTime(number);
  		amountAfterInvestPopUp = amountAfterInvestPopUp.replaceAll("[^\\d]", "");
  		String level = investmentPage.getLevelFromAfterInvest(number);
  		investmentPage.openProfitLine(number);
  		User.verifyInvestmentData(investmentId, testData.get("id"), "1", amountAfterInvestPopUp, level, chooseProfit);
  		String balanceFromProfitLine = investmentPage.getBalanceFromProfitLineWindow(4);
  		String balanceAfter = headerMenuPage.getBalance();
  		Assert.assertEquals(balanceFromProfitLine, balanceAfter);
  		String userBalanceInDBAfter = User.getUserBalanceFromDB(email, currentUserId);
  		int ballanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
  		int amountInt = Integer.parseInt(amountToInvest.replace(moneyType, "") + "00");
  		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
  		Assert.assertEquals(ballanceInt - amountInt, balanceAfterInt);
  		int balanceInDbInt = Integer.parseInt(userBalanceInDB);
  		int balanceInDbAfterInt = Integer.parseInt(userBalanceInDBAfter);
  		Assert.assertEquals(balanceInDbInt - amountInt, balanceInDbAfterInt);
  		String totalInvest = investmentPage.getTotalInvestmentFromProfitLine(4);
  		Assert.assertEquals(moneyType + amountToInvest + ".00", totalInvest.replace(",", ""));
  		investmentPage.closeProfitLineWindow(operationSys);
  		headerMenuPage.openMyAccountMenu();
  		MyAccountPage myAccountPage = new MyAccountPage(driver);
  		String amountFromMyAccount = myAccountPage.getInvestmentAmount();
  		int amountFromMyAccountInt = Integer.parseInt(amountFromMyAccount.replaceAll("[^\\d]", ""));
  		Assert.assertEquals(investmentId, myAccountPage.getOptionId());
  		Assert.assertEquals(asset, myAccountPage.getAsset());
  		String levelFromMyAcc = myAccountPage.getLevel();
  		if (! level.equals(levelFromMyAcc)) {
  			Assert.assertEquals(level.replace("0", "").replace(".", ""), levelFromMyAcc.replace("0", "").replace(".", ""));
  		}	
  		String myAccountEpiryTyme = myAccountPage.getExpiryTime();
  		if (! expiresTime.equals(myAccountEpiryTyme)) {
  			Assert.assertTrue(myAccountEpiryTyme.contains(expiresTime));
  		}
  		Assert.assertEquals(amountInt, amountFromMyAccountInt);
  		Assert.assertTrue(amountFromMyAccount.contains(moneyType));
  		String correctFromMyAcc = myAccountPage.getAmountIfCorrect();
  		Assert.assertEquals(moneyType + correct, correctFromMyAcc.replace(".", "").replace(",", ""));
  		String incorrectFromMyAcc = myAccountPage.getAmountIfIncorrect();
  		Assert.assertEquals(incorectForVerificationInMyAcc, incorrectFromMyAcc);
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
