package com.anyoption.autotest.binaryoptions;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.pages.BubblesPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.InvestmentPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.LongTermPage;
import com.anyoption.autotest.pages.OptionPlusPage;
import com.anyoption.autotest.pages.QuestionnairePage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class InvestWithNotRegulatedUserTest extends BaseTest {
	public InvestWithNotRegulatedUserTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/InvestWithNotRegulatedUserTestData.xlsx", numberRows, 2);
	    return(retObjArr);
	}
	/**
	 * Tries to invest with not regulated user
	 * @param skin
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void investWithNotRegulatedUserTest(String skin, String bubblesNotification) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, skin);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		QuestionnairePage questionnairePage = new QuestionnairePage(driver);
		InvestmentPage investmentPage = new InvestmentPage(driver);
		OptionPlusPage optionPlusPage = new OptionPlusPage(driver);
		LongTermPage longTermPage = new LongTermPage(driver);
		BubblesPage bubblesPage = new BubblesPage(driver);
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanMakeQuestionnaire(skinId);
		HashMap<String, String> userData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = userData.get("id");
		headerMenuPage.openOptionTradingMenu();
		int number = investmentPage.getAcctiveInvestment();
		if (number == -1) {
			investmentPage.changeMarket(0);
			number = 0;
		}
		investmentPage.clickUpButton(number);
		Assert.assertTrue(questionnairePage.isQuestionnairePopUpDisplayed());
		questionnairePage.closeQuestionnairePopUp();
		optionPlusPage.openOptionPlusMenu(browserType);
		optionPlusPage.setActiveMarket();
		optionPlusPage.clickUpButton(0, browserType);
		Assert.assertTrue(questionnairePage.isQuestionnairePopUpDisplayed());
		questionnairePage.closeQuestionnairePopUp();
		longTermPage.openLongTerm();
  		longTermPage.setMarket(1);
  		String id = longTermPage.getOptionId(0);
  		longTermPage.buyLongOption(id);
  		Assert.assertTrue(questionnairePage.isQuestionnairePopUpDisplayed());
  		questionnairePage.closeQuestionnairePopUp();
  		longTermPage.openBubblesMenu();
  		bubblesPage.switchDriverToBubbles();
  		bubblesPage.passTutorial();
  		bubblesPage.expandBubble();
		int profit = bubblesPage.getProfite();
		if (profit == 0) {
			profit = bubblesPage.moveBubble();
		}
		bubblesPage.clickGoButton();
		String notification = bubblesPage.getNegativeNotification();
		driver.switchTo().defaultContent();
		Assert.assertEquals(bubblesNotification, notification);
		Assert.assertTrue(questionnairePage.isQuestionnairePopUpDisplayed());
  		questionnairePage.closeQuestionnairePopUp();
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		driver.switchTo().defaultContent();
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
