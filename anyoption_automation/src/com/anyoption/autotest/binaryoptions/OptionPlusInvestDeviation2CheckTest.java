package com.anyoption.autotest.binaryoptions;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.InvestmentPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.OptionPlusPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class OptionPlusInvestDeviation2CheckTest extends BaseTest {
	public OptionPlusInvestDeviation2CheckTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/OptionPlusInvestTestData.xlsx", numberRows, 2);
	    return(retObjArr);
	}
	/**
	 * Makes option plus investment with deviation 2 check
	 * @param skin
	 * @param message
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void optionPlusInvestDeviation2CheckTest(String skin, String message) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		String sqlQuery = UserSQL.getUserWhoCanMakeDeviationCheckInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		InvestmentPage investmentPage = new InvestmentPage(driver);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		OptionPlusPage optionPlusPage = new OptionPlusPage(driver);
		String balance = headerMenuPage.getBalance();
		String moneyType = balance.replaceAll("\\d+.*", "");
		String email = testData.get("email");
  		currentUserId = testData.get("id");
  		String userBalanceInDB = testData.get("balance");
  		int userBalanceInDBInt = Integer.parseInt(userBalanceInDB) / 100;
  		optionPlusPage.openOptionPlusMenu(browserType);
  		optionPlusPage.setActiveMarket();
  		String profitBalanceBefore = optionPlusPage.getProfitBalance();
  		Assert.assertEquals(balance, profitBalanceBefore);
  		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
  		int totalinvestmentBefore = optionPlusPage.getTotalInvestment();
  		HashMap<String, String> dev2Data = new HashMap<>();
  		for (int i = 0; i < 10; i++) {
  			dev2Data = optionPlusPage.getOptionPlusDeviation2Data();
  			boolean isMarketActive = optionPlusPage.isMarketActive();
  			if (dev2Data.isEmpty() || (userBalanceInDBInt <= Integer.parseInt(dev2Data.get("amount"))) || isMarketActive == false) {
  				optionPlusPage.changeMarket(i);
  			} else {
  				System.out.println(dev2Data);
  				break;
  			}
  		}
  		String customAmount = dev2Data.get("amount");
  		optionPlusPage.setCustomAmountToInvest(customAmount);
  		String percents = optionPlusPage.getProfitPercents().replace("%", "");
  		double percenCoefProfit = (Double.parseDouble(percents) / 100) + 1;
  		double percentCoefRefund = 0.15;
  		String amount = optionPlusPage.getAmountForInvest();
  		Assert.assertEquals(moneyType + customAmount, amount.replace(",", ""));
  		int amountInt = Integer.parseInt(amount.replaceAll("[^\\d]", "")) * 100;
  		String correctValue = optionPlusPage.getReturnValueIfCorrect();
  		Assert.assertTrue(correctValue.contains(moneyType));
  		int correctValueInt = Integer.parseInt(correctValue.replaceAll("[^\\d]", ""));
  		Assert.assertEquals((int)(amountInt * percenCoefProfit), correctValueInt);
  		String incorrectValue = optionPlusPage.getReturnValueIfIncorrect();
  		Assert.assertTrue(incorrectValue.contains(moneyType));
  		int incorrectValueInt = Integer.parseInt(incorrectValue.replaceAll("[^\\d]", ""));
  		Assert.assertEquals((int)(amountInt * percentCoefRefund), incorrectValueInt);
  		for (int i = 0; i < 4; i++) {
  			investmentPage.clickUpButton(0);
  			if (investmentPage.isCancelInvestmentDisplayed(0) == true) {
  				break;
  			}
  		}
  		Assert.assertTrue(investmentPage.isCancelInvestmentDisplayed(0));
  		String secs = investmentPage.getTimeToCancel(0);
  		if (Integer.parseInt(dev2Data.get("secs")) - 1 != Integer.parseInt(secs)) {
  			if (Integer.parseInt(dev2Data.get("secs")) - 2 != Integer.parseInt(secs))
  			Assert.assertTrue(Integer.parseInt(dev2Data.get("secs")) - 2 > Integer.parseInt(secs));
  		}	
  		Assert.assertTrue(investmentPage.afterInvest(0));
  		String investmentId = investmentPage.getInvestmentId(0);
  		System.out.println("investment id " + investmentId);
  		String amountInAfterInvestPopUp = investmentPage.getAmountFormAfterInvestPopUp(0);
  		System.out.println("amount in after investment pop up " + amountInAfterInvestPopUp);
  		String level = investmentPage.getLevelFromAfterInvest(0);
  		String profit[] = new String[2];
  		profit[0] = percents;
  		profit[1] = "15";
  		double amountDouble = Double.parseDouble(amount.replace(moneyType, ""));
  		amountDouble = amountDouble + 1.00;
  		Assert.assertEquals(moneyType + String.valueOf(amountDouble) + "0", amountInAfterInvestPopUp);
  		Assert.assertEquals(correctValueInt, investmentPage.getCorrectAmountFromAfterInvestPopUp(0));
  		Assert.assertEquals(incorrectValueInt, investmentPage.getIncorrectAmountFromAfterInvestPopUp(0));
  		Assert.assertEquals(message, investmentPage.getCommissionMessage());
  		String balanceAfter = headerMenuPage.getBalance();
  		String userBalanceInDBAfter = User.getUserBalanceFromDB(email, currentUserId);
  		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
  		int optionPlusAmount = amountInt + 100;
  		Assert.assertEquals(balanceInt - optionPlusAmount, balanceAfterInt);
  		String profitBalanceAfter = optionPlusPage.getProfitBalance();
  		Assert.assertEquals(balanceAfter, profitBalanceAfter);
  		if (! browserType.equalsIgnoreCase("firefox") || ! operationSys.equals("Linux")) {
  			optionPlusPage.waitTotalInvestments(moneyType);
  			int totalInvestmentAfter = optionPlusPage.getTotalInvestment();
  			Assert.assertEquals(totalinvestmentBefore + optionPlusAmount, totalInvestmentAfter);
  		}	
  		Assert.assertEquals(Integer.parseInt(userBalanceInDB) - optionPlusAmount, Integer.parseInt(userBalanceInDBAfter));
  		amountInAfterInvestPopUp = amountInAfterInvestPopUp.replaceAll("[^\\d]", "");
  		User.verifyInvestmentData(investmentId, testData.get("id"), "1", amountInAfterInvestPopUp, level, profit);
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
