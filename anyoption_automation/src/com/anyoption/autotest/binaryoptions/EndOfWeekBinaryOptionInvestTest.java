package com.anyoption.autotest.binaryoptions;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.InvestmentPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.MyAccountPage;
import com.anyoption.autotest.pages.OptionConfirmationPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class EndOfWeekBinaryOptionInvestTest extends BaseTest {

	public EndOfWeekBinaryOptionInvestTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/BinaryOptionInvestEndOfWeekTestData.xlsx", numberRows, 5);
	    return(retObjArr);
	}
	/**
	 * Makes end of the week binary option investment
	 * @param skin
	 * @param confirmTitle
	 * @param endOfWeek
	 * @param chartMess
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void endOfWeekBinaryOptionInvestTest(String skin, String confirmTitle, String endOfWeek, String chartMess, String today) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		String sqlQuery = UserSQL.getUserWhoCanMakeInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		InvestmentPage investmentPage = new InvestmentPage(driver);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		OptionConfirmationPage optionConfirmationPage = new OptionConfirmationPage(driver);
		String balance = headerMenuPage.getBalance();
		String moneyType = balance.replaceAll("\\d+.*", "");
		String email = testData.get("email");
		currentUserId = testData.get("id");
		String userBalanceInDB = testData.get("balance");
  		int number = investmentPage.getAcctiveInvestment();
  		if (number == -1) {
  			number = 0;
  		}
  		investmentPage.setCurrencyMarket(number);
  		investmentPage.chooseBinaryOptionExpTime(number, endOfWeek);
  		UtilsMethods.sleep(2000);
  		String investmentExpTime = investmentPage.getInvestmentExpTime(number);
  		Calendar calendar = Calendar.getInstance();
  		int day = calendar.get(Calendar.DAY_OF_WEEK); 
  		if (day != 6) {
	  		String[] date = investmentExpTime.split(" ");
	  		String time = date[1];
	  		String[] times = time.split("\\.");
	  		String month = times[1];
	  		if (Integer.parseInt(month) < 10) {
	  			month = "0" + month;
	  		}
	  		String investDay = times[0];
	  		if (investDay.length() == 1) {
	  			investDay = "0" + investDay;
	  		}
	  		investmentExpTime = date[0] + " " + investDay + "." + month + "." + times[2];
	  		investmentExpTime = investmentExpTime.replace(",", "");
  		} else {	
  			String todayAsString = new SimpleDateFormat("dd.MM.yy").format(new Date());
  			investmentExpTime = investmentExpTime.replace(today, todayAsString);
  		}
  		Assert.assertTrue(investmentPage.isChartMessageDisplayed(number));
  		Assert.assertEquals(chartMess, investmentPage.getChartMessage(number));
  		investmentPage.clickInvestmentDropDown(number);
  		investmentPage.chooseAmountToInvest(number, 0);
  		String amountToInvest = investmentPage.getTheAmountForInvest(number);
  		int amountToInvestInt = Integer.parseInt(amountToInvest.replace(moneyType, ""));
  		investmentPage.clickProfitDropDown(number);
//  		String chooseProfit[] = investmentPage.chooseProfit(number, 0);
  		String chooseProfit[] = investmentPage.chooseDefaultProfit(number);
  		String profitToInvest = investmentPage.getTheProfit(number);
  		Assert.assertEquals(chooseProfit[0] + "%", profitToInvest);
  		int correct = investmentPage.getReturnValueIfCorrect(number);
  		int incorrect = investmentPage.getReturnValueIfIncorrect(number);
  		String incorectForVerificationInMyAcc = investmentPage.getIncorectAmount(number);
  		double profitPercent = Double.parseDouble(chooseProfit[0]);
  		double refundPercent = Double.parseDouble(chooseProfit[1]);
  		double returnValue = amountToInvestInt * (1 + (profitPercent / 100)) * 100;
  		returnValue = Math.round(returnValue);
  		Assert.assertEquals(correct, (int)returnValue);
  		returnValue = (amountToInvestInt * (refundPercent / 100)) * 100;
  		Assert.assertEquals(incorrect, (int)returnValue);
  		String asset = investmentPage.getMarketName(number);
//  		investmentPage.clickUpButton(number);
  		investmentPage.makeBinaryOptionInvestWithRecursion(number);
  		Assert.assertTrue(investmentPage.afterInvest(number, true));
  		String amountAfterInvestPopUp = investmentPage.getAmountFormAfterInvestPopUp(number);
  		Assert.assertEquals(amountToInvest + ".00", amountAfterInvestPopUp);
  		Assert.assertEquals(correct, investmentPage.getCorrectAmountFromAfterInvestPopUp(number));
  		Assert.assertEquals(incorrect, investmentPage.getIncorrectAmountFromAfterInvestPopUp(number));
  		String investmentId = investmentPage.getInvestmentId(number);
  		amountAfterInvestPopUp = amountAfterInvestPopUp.replaceAll("[^\\d]", "");
  		String level = investmentPage.getLevelFromAfterInvest(number);
  		investmentPage.openPrintInvestmentWindow(number);
  		investmentPage.openProfitLine(number);
  		String winHandleBefore = driver.getWindowHandle();
  		for (String winHandle : driver.getWindowHandles()) {
  		    driver.switchTo().window(winHandle);
  		}
  		Assert.assertEquals(confirmTitle, optionConfirmationPage.getOptionConfirmationTitle());
  		Assert.assertEquals(asset, optionConfirmationPage.getOptionConfirmationMarket());
  		if (level.replace(",", "") != optionConfirmationPage.getOptionConfirmationLevel()) {
  			Assert.assertEquals(level.replace(",", "").replace("0", ""), optionConfirmationPage.getOptionConfirmationLevel().replace("0", ""));
  		}	
  		Assert.assertEquals(amountToInvest + ".00", optionConfirmationPage.getOptionConfirmationInvestmentAmount());
  		Assert.assertEquals(moneyType + correct, optionConfirmationPage.getOptionConfirmationCorrectValue().replace(".", ""));
  		Assert.assertEquals(moneyType + incorrect, optionConfirmationPage.getOptionConfirmationIncorrectValue().replace(".", ""));
  		Assert.assertEquals("CALL", optionConfirmationPage.getOptionConfirmationType());
  		String confirmationExpTime = optionConfirmationPage.getOptionConfirmationExpireTime();
  		Assert.assertEquals(confirmationExpTime, investmentExpTime);
  		driver.close();
  		driver.switchTo().window(winHandleBefore);
  		User.verifyInvestmentData(investmentId, testData.get("id"), "1", amountAfterInvestPopUp, level, chooseProfit);
  		String balanceFromProfitLine = investmentPage.getBalanceFromProfitLineWindow(4);
  		String balanceAfter = headerMenuPage.getBalance();
  		Assert.assertEquals(balanceFromProfitLine, balanceAfter);
  		String userBalanceInDBAfter = User.getUserBalanceFromDB(email, currentUserId);
  		int ballanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
  		int amountInt = Integer.parseInt(amountToInvest.replace(moneyType, "") + "00");
  		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
  		Assert.assertEquals(ballanceInt - amountInt, balanceAfterInt);
  		int balanceInDbInt = Integer.parseInt(userBalanceInDB);
  		int balanceInDbAfterInt = Integer.parseInt(userBalanceInDBAfter);
  		Assert.assertEquals(balanceInDbInt - amountInt, balanceInDbAfterInt);
  		investmentPage.closeProfitLineWindow(operationSys);
  		headerMenuPage.openMyAccountMenu();
  		MyAccountPage myAccountPage = new MyAccountPage(driver);
  		String amountFromMyAccount = myAccountPage.getInvestmentAmount();
  		int amountFromMyAccountInt = Integer.parseInt(amountFromMyAccount.replaceAll("[^\\d]", ""));
  		Assert.assertEquals(investmentId, myAccountPage.getOptionId());
  		Assert.assertEquals(asset, myAccountPage.getAsset());
  		String levelFromMyAcc = myAccountPage.getLevel();
  		if (! level.equals(levelFromMyAcc)) {
  			Assert.assertEquals(level.replace("0", "").replace(".", ""), levelFromMyAcc.replace("0", "").replace(".", ""));
  		}	
  		String myAccountEpiryTyme = myAccountPage.getExpiryTimeText();
  		Assert.assertEquals(myAccountEpiryTyme.replace(" ", ""), investmentExpTime.replace(" ", ""));
  		Assert.assertEquals(confirmationExpTime.replace(" ", ""), myAccountPage.getExpiryTimeText().replace(" ", ""));
  		Assert.assertEquals(amountInt, amountFromMyAccountInt);
  		Assert.assertTrue(amountFromMyAccount.contains(moneyType));
  		String correctFromMyAcc = myAccountPage.getAmountIfCorrect();
  		Assert.assertEquals(moneyType + correct, correctFromMyAcc.replace(".", ""));
  		String incorrectFromMyAcc = myAccountPage.getAmountIfIncorrect();
  		Assert.assertEquals(incorectForVerificationInMyAcc, incorrectFromMyAcc);
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		driver.switchTo().defaultContent();
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
