package com.anyoption.autotest.binaryoptions;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.LongTermPage;
import com.anyoption.autotest.pages.MyAccountPage;
import com.anyoption.autotest.pages.OptionConfirmationPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class LongTermInvestTest extends BaseTest {
	public LongTermInvestTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/LongTermInvestTestData.xlsx", numberRows, 2);
	    return(retObjArr);
	}
	/**
	 * Makes long term investment
	 * @param skin
	 * @param confirmTitle
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void longTermInvestTest(String skin, String confirmTitle) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		OptionConfirmationPage optionConfirmationPage = new OptionConfirmationPage(driver);
		String sqlQuery = UserSQL.getUserWhoCanMakeLongTermInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		LongTermPage longTermPage = new LongTermPage(driver);
		String balance = headerMenuPage.getBalance();
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		String moneyType = balance.replaceAll("\\d+.*", "");
		String email = testData.get("email");
		currentUserId = testData.get("id");
		String userBalanceInDB = testData.get("balance");
  		int userBalanceInDBInt = Integer.parseInt(userBalanceInDB);
  		longTermPage.openLongTerm();
  		longTermPage.setMarket(1);
  		String id = longTermPage.getOptionId(0);
  		String asset = longTermPage.getMarketName(id);
  		longTermPage.selectUnitsCount(id, "1");
  		int profit = longTermPage.getProfit(id);
  		String price = longTermPage.getPrice(id);
  		int priceInt = Integer.parseInt(price.replace(moneyType, "").replace(" ", ""));
  		Assert.assertTrue(price.contains(moneyType));
  		String returnValue = longTermPage.getReturnValue(id);
  		double returnValueDouble = Double.parseDouble(returnValue.replace(moneyType, "").replace(" ", ""));
  		double value = (priceInt * (1 + (double)profit / 100));
  		Assert.assertEquals(returnValueDouble, value);
  		Assert.assertTrue(returnValue.contains(moneyType));
  		longTermPage.buyLongOption(id);
  		Assert.assertTrue(longTermPage.isPrintButtonDiplayed(id));
  		longTermPage.openPrintInvestmentWindow(id);
  		String winHandleBefore = driver.getWindowHandle();
  		for (String winHandle : driver.getWindowHandles()) {
  		    driver.switchTo().window(winHandle);
  		}
  		Assert.assertEquals(confirmTitle, optionConfirmationPage.getOptionConfirmationTitle());
  		Assert.assertEquals(asset, optionConfirmationPage.getOptionConfirmationMarket());
  		Assert.assertEquals(price, optionConfirmationPage.getOptionConfirmationInvestmentAmount());
  		Assert.assertEquals(returnValue, optionConfirmationPage.getLongTermReturnAmount());
  		Assert.assertEquals("One Touch", optionConfirmationPage.getOptionConfirmationType());
  		String confirmationExpTime = optionConfirmationPage.getOptionConfirmationExpireTime();
  		driver.close();
  		driver.switchTo().window(winHandleBefore);
  		String balanceAfter = headerMenuPage.getBalance();
  		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
  		Assert.assertEquals(balanceInt - (priceInt * 100), balanceAfterInt);
  		String userBalanceInDBAfter = User.getUserBalanceFromDB(email, currentUserId);
  		int userBalanceInDBAfterInt = Integer.parseInt(userBalanceInDBAfter);
  		Assert.assertEquals(userBalanceInDBInt - (priceInt * 100), userBalanceInDBAfterInt);
  		headerMenuPage.openMyAccountMenu();
  		MyAccountPage myAccountPage = new MyAccountPage(driver);
  		String amountFromMyAccount = myAccountPage.getInvestmentAmount();
  		int amountFromMyAccountInt = Integer.parseInt(amountFromMyAccount.replaceAll("[^\\d]", ""));
  		Assert.assertEquals(priceInt * 100, amountFromMyAccountInt);
  		Assert.assertTrue(amountFromMyAccount.contains(moneyType));
  		String correctFromMyAcc = myAccountPage.getAmountIfCorrect();
  		Assert.assertEquals(moneyType + (int)(returnValueDouble * 100), correctFromMyAcc.replace(".", ""));
  		Assert.assertEquals(confirmationExpTime.replace(" ", ""), myAccountPage.getExpiryTimeText().replace(" ", ""));
  		String incorrectFromMyAcc = myAccountPage.getAmountIfIncorrect();
  		if (driver.getCurrentUrl().contains("bgtest")) {
  			System.out.println("bg test bug in long term remove after fix");
  		} else {
  			Assert.assertEquals(moneyType + "0.00", incorrectFromMyAcc);
  		}
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
