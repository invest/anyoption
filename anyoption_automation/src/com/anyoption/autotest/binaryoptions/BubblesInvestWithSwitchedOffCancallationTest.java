package com.anyoption.autotest.binaryoptions;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.pages.BubblesPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class BubblesInvestWithSwitchedOffCancallationTest extends BaseTest {

	public BubblesInvestWithSwitchedOffCancallationTest() throws IOException {
		super();
		numberRows = 1;
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/BubblesInvestWithSwitchedOffCancellationTestData.xlsx", numberRows, 2);
	    return(retObjArr);
	}
	/**
	 * Makes investment with switched off cancel investment pop up.
	 * @param skin
	 * @param linkText
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void bubblesInvestWithSwitchedOffCancallationTest(String skin, String linkText) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		String sqlQuery = UserSQL.getUserWhoCanMakeInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		BubblesPage bubblesPage = new BubblesPage(driver);
		currentUserId = testData.get("id");
		bubblesPage.openBubbles();
		bubblesPage.switchDriverToBubbles();
		bubblesPage.passTutorial();
		bubblesPage.expandBubble();
		int profit = bubblesPage.getProfite();
		if (profit == 0) {
			profit = bubblesPage.moveBubble();
		}
		bubblesPage.setCustomAmount("25");
		bubblesPage.clickGoButton();
		Assert.assertTrue(bubblesPage.isCancelNotificationDisplayed());
		Assert.assertEquals(linkText, bubblesPage.getCancelCheckBox());
		bubblesPage.clickDoNotShowCancelInvLink();
		driver.navigate().refresh();
		bubblesPage.switchDriverToBubbles();
		bubblesPage.expandBubble();
		profit = bubblesPage.getProfite();
		if (profit == 0) {
			profit = bubblesPage.moveBubble();
		}
		bubblesPage.setCustomAmount("25");
		bubblesPage.clickGoButton();
		Assert.assertFalse(bubblesPage.isCancelNotificationDisplayed());
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
