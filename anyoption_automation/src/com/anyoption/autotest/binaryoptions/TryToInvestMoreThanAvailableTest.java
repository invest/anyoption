package com.anyoption.autotest.binaryoptions;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.DepositPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.InvestmentPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.OptionPlusPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class TryToInvestMoreThanAvailableTest extends BaseTest {
	public TryToInvestMoreThanAvailableTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/TryToInvestMoreThanAvailableTestData.xlsx", numberRows, 3);
	    return(retObjArr);
	}
	/**
	 * @throws Exception 
	 * 
	 */
	@Test(dataProvider = "dp")
	public void tryToInvestMoreThanAvailableTest(String skin, String message, String depositTitle) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		String sqlQuery = UserSQL.getUserWhoCanMakeInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		InvestmentPage investmentPage = new InvestmentPage(driver);
		String balance = headerMenuPage.getBalance();
		String moneyType = balance.replaceAll("\\d+.*", "");
		String email = testData.get("email");
		currentUserId = testData.get("id");
		String userBalanceInDB = testData.get("balance");
  		int number = investmentPage.getAcctiveInvestment();
  		if (number == -1) {
  			investmentPage.changeMarket(0);
  			number = 0;
  		}
  		int amountInt = (int)Double.parseDouble(balance.replace(moneyType, "").replace(",", ""));
  		amountInt = amountInt + 1;
  		String amount = String.valueOf(amountInt);
  		investmentPage.setAmountToInvest(number, amount);
  		String amountToInvest = investmentPage.getTheAmountForInvest(number);
  		Assert.assertEquals(moneyType + amount, amountToInvest.replace(",", ""));
  		investmentPage.clickUpButton(number);
  		Assert.assertTrue(investmentPage.isPopUpForMakeDepositDisplayed(number, "UP"));
  		Assert.assertTrue(investmentPage.getMessageFromPopUpForDeposit(number).startsWith(message));
  		investmentPage.closeNoDepositPopUp(number);
  		investmentPage.clickDownButton(number);
  		investmentPage.clickDepositButtonFromPopUp(number);
  		DepositPage depositPage = new DepositPage(driver, browserType);
  		Assert.assertEquals(depositTitle, depositPage.getDepositTitle());
  		String currentUrl = driver.getCurrentUrl();
  		Assert.assertTrue(currentUrl.contains("jsp/pages/deposit.jsf"));
  		currentUrl = "";
  		headerMenuPage.openOptionTradingMenu();
  		OptionPlusPage optionPlusPage = new OptionPlusPage(driver);
  		if (! skin.equalsIgnoreCase("EN") && driver.getCurrentUrl().contains("bgtestenv")) {
			System.out.println("Bg test problem with redirect");
			if (skin.equalsIgnoreCase("IT")) {
				driver.get("http://www1.bgtestenv.anyoption.it/binary-options-trading");
			} else {
				driver.get("http://" + skin.toLowerCase() + ".bgtestenv.anyoption.com/binary-options-trading");
			}	
  		}
  		optionPlusPage.openOptionPlusMenu(browserType);
  		optionPlusPage.setActiveMarket();
  		UtilsMethods.sleep(2000);
  		optionPlusPage.setCustomAmountToInvest(amount);
  		investmentPage.clickUpButton(0);
  		Assert.assertTrue(investmentPage.isPopUpForMakeDepositDisplayed(0, "UP"));
  		Assert.assertTrue(investmentPage.getMessageFromPopUpForDeposit(0).startsWith(message));
  		investmentPage.closeNoDepositPopUp(0);
  		UtilsMethods.sleep(300);
  		investmentPage.clickDownButton(0);
  		investmentPage.clickDepositButtonFromPopUp(0, "DOWN");
  		Assert.assertEquals(depositTitle, depositPage.getDepositTitle());
  		currentUrl = driver.getCurrentUrl();
  		Assert.assertTrue(currentUrl.contains("jsp/pages/deposit.jsf"));
  		String balanceAfter = headerMenuPage.getBalance();
  		Assert.assertEquals(balance, balanceAfter);
  		String userBalanceInDBAfter = User.getUserBalanceFromDB(email, currentUserId);
  		Assert.assertEquals(userBalanceInDB, userBalanceInDBAfter);
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
