package com.anyoption.autotest.binaryoptions;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.pages.BubblesPage;
import com.anyoption.autotest.pages.DepositPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class BubblesInvestWithIncorrectAmountTest extends BaseTest {

	public BubblesInvestWithIncorrectAmountTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/BubblesInvestWithIncorrectAmountTestData.xlsx", numberRows, 5);
	    return(retObjArr);
	}
	/**
	 * Tries bubbles investment with incorrect amounts
	 * @param skin
	 * @param minMess
	 * @param balanceMess
	 * @param depositMess
	 * @param title
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void bubblesInvestWithIncorrectAmountTest(String skin, String minMess, String balanceMess, String depositMess, String title) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		String sqlQuery = UserSQL.getUserWhoCanNotMakeBubblesInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		BubblesPage bubblesPage = new BubblesPage(driver);
		DepositPage depositPage = new DepositPage(driver, browserType);
		String balance = headerMenuPage.getBalance();
		String moneyType = balance.replaceAll("\\d+.*", "");
		currentUserId = testData.get("id");
		bubblesPage.openBubbles();
		bubblesPage.switchDriverToBubbles();
		bubblesPage.passTutorial();
		bubblesPage.expandBubble();
		int profit = bubblesPage.getProfite();
		if (profit == 0) {
			profit = bubblesPage.moveBubble();
		}
		String amount = "24";
		bubblesPage.setCustomAmount(amount);
		bubblesPage.clickGoButton();
		Assert.assertEquals(minMess, bubblesPage.getNegativeNotification());
		int balanceInt = Integer.parseInt(balance.substring(0, balance.length() - 3).replace(moneyType, "").replace(",", ""));
		amount = String.valueOf(balanceInt + 1);
		bubblesPage.setCustomAmount(amount);
		bubblesPage.clickGoButton();
		Assert.assertEquals(balanceMess, bubblesPage.getNegativeNotification());
		driver.switchTo().defaultContent();
		Assert.assertEquals(depositMess, bubblesPage.getNoDepositMessage().replace("\n", " "));
		bubblesPage.clickDepositButton();
  		Assert.assertEquals(title, depositPage.getDepositTitle());
  		String currentUrl = driver.getCurrentUrl();
  		Assert.assertTrue(currentUrl.contains("jsp/pages/deposit.jsf"));
  		String balanceAfter = headerMenuPage.getBalance();
  		Assert.assertEquals(balance, balanceAfter);
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
