package com.anyoption.autotest.binaryoptions;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.BubblesPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.MyAccountPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class BubblesInvestTest extends BaseTest {

	public BubblesInvestTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/BubblesInvestTestData.xlsx", numberRows, 7);
	    return(retObjArr);
	}
	/**
	 * Makes Bubbles investment
	 * @param skin
	 * @param lostMessage
	 * @param wonMessage
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void bubblesInvestTest(String skin, String lostMessage, String wonMessage, String purchasedMess, String tutMess1, String tutMess2, String noOptionsMess) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		String sqlQuery = UserSQL.getUserWhoCanMakeInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		BubblesPage bubblesPage = new BubblesPage(driver);
		MyAccountPage myAccountPage = new MyAccountPage(driver);
		String balance = headerMenuPage.getBalance();
		String moneyType = balance.replaceAll("\\d+.*", "");
		String email = testData.get("email");
		currentUserId = testData.get("id");
		bubblesPage.openBubbles();
		bubblesPage.switchDriverToBubbles();
		bubblesPage.passTutorial(tutMess1, tutMess2);
		bubblesPage.expandBubble();
		int profit = bubblesPage.getProfite();
		if (profit == 0) {
			profit = bubblesPage.moveBubble();
		}
		int amount = bubblesPage.chooseAmountForInvest(1, moneyType);
		bubblesPage.makeBubblesInvestment();
		Assert.assertEquals(purchasedMess, bubblesPage.getPositiveNotification());
		String market = bubblesPage.getAssetName();
		UtilsMethods.sleep(2000);
		profit = bubblesPage.getProfite();
		double returnIfCorrect = bubblesPage.getReturnIfCorrect(moneyType);
		double value = (double)amount * (1 + ((double)profit / 100));
		Assert.assertEquals(Math.round(value), Math.round(returnIfCorrect));
		Assert.assertEquals(0, bubblesPage.getReturnIfIncorrect(moneyType));
		driver.switchTo().defaultContent();
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		String balanceAfter = headerMenuPage.getBalance();
		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
		Assert.assertEquals(balanceInt - amount * 100, balanceAfterInt);
		String userBalanceInDBAfter = User.getUserBalanceFromDB(email, currentUserId);
		int balanceInDbInt = Integer.parseInt(testData.get("balance"));
		int balanceInDbAfterInt = Integer.parseInt(userBalanceInDBAfter);
		Assert.assertEquals(balanceInDbInt - (amount * 100), balanceInDbAfterInt);
		balanceInDbInt = balanceInDbAfterInt;
		bubblesPage.switchDriverToBubbles();
		boolean isWon = false;
		String balanceAfterInvest;
		int retAmount = bubblesPage.getReturnValueFromEndWindow(moneyType);
		if (retAmount != 0) {
			isWon = true;
		} 
		String expLevel = bubblesPage.getExpLevel(isWon);
		String expTime = bubblesPage.getExpTime(isWon);
		if (isWon == false) {
			Assert.assertEquals(lostMessage.toUpperCase(), bubblesPage.getMessageFromEndWindow(isWon));
			driver.switchTo().defaultContent();
			balanceAfterInvest = headerMenuPage.getBalance();
			Assert.assertEquals(balanceAfter, balanceAfterInvest);
		} else {
			Assert.assertEquals(wonMessage.toUpperCase(), bubblesPage.getMessageFromEndWindow(isWon));
			driver.switchTo().defaultContent();
			balanceAfterInvest = headerMenuPage.getBalance();
			int balanceAfterInvestInt = Integer.parseInt(balanceAfterInvest.replaceAll("[^\\d]", ""));
			Assert.assertEquals(balanceAfterInt + retAmount, balanceAfterInvestInt);
		}
		userBalanceInDBAfter = User.getUserBalanceFromDB(email, currentUserId);
		balanceInDbAfterInt = Integer.parseInt(userBalanceInDBAfter);
		Assert.assertEquals(balanceInDbInt + retAmount, balanceInDbAfterInt);
		driver.switchTo().defaultContent();
		headerMenuPage.openMyAccountMenu();
		myAccountPage.selectOptionsType("1");
		Assert.assertEquals(noOptionsMess, myAccountPage.getNoOptionsMessage());
		myAccountPage.clickClosedInvestmentRadioButton();
		myAccountPage.clickSubmitButton();
		String optionId = myAccountPage.getBubblesOptionId();
		String asset = myAccountPage.getBubblesMarketName();
		String expiryTime = myAccountPage.getBubblesExpiryTime();
		System.out.println(currentUserId + " my option expirity time " + expiryTime);
		String amountFromMyOptions = myAccountPage.getBubblesInvestedAmount();
		String retAmountFromMyOptions = myAccountPage.getBubblesReturnAmount(moneyType);
		Assert.assertEquals(market, asset);
		System.out.println(currentUserId + " bubbles exp time " + expTime);
		if (! expiryTime.contains(expTime)) {
			System.out.println("Rounding problem with seconds");
		}	
		Assert.assertEquals(moneyType + amount + ".00", amountFromMyOptions);
		Assert.assertEquals(moneyType + retAmount, retAmountFromMyOptions.replace(".", ""));
		User.verifyBubblesInvestmentData(optionId, testData.get("id"), String.valueOf(amount), expLevel, profit, isWon);
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		driver.switchTo().defaultContent();
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
