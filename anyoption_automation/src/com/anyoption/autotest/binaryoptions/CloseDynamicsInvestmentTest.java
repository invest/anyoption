package com.anyoption.autotest.binaryoptions;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.DynamicsPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.MyAccountPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class CloseDynamicsInvestmentTest extends BaseTest {

	public CloseDynamicsInvestmentTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/CloseDynamicsInvestmentTestData.xlsx", numberRows, 3);
	    return(retObjArr);
	}
	/**
	 * Closes dynamics investment
	 * @param skin
	 * @param noOptionMess
	 * @param optionStatus
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void closeDynamicsInvestmentTest(String skin, String noOptionMess, String optionStatus) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		DynamicsPage dynamicsPage = new DynamicsPage(driver);
		MyAccountPage myAccountPage = new MyAccountPage(driver);
		String sqlQuery = UserSQL.getUserWhoCanMakeDynamicsInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = testData.get("id");
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		String email = testData.get("email");
		dynamicsPage.openDynamics();
		if (dynamicsPage.isDynamicsTutorialDisplayed() == true) {
			dynamicsPage.closeTutorial();
		}
		dynamicsPage.chooseMarketToInvest();
		String asset = dynamicsPage.getCurrentMarket();
		String level = dynamicsPage.getDynamicsLevel();
		String amount = "100";
		dynamicsPage.setAmount(amount);
		dynamicsPage.makeAboveInvestment();
		String balance = headerMenuPage.getBalance();
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		String moneyType = balance.replaceAll("\\d+.*", "");
		String balanceInDb = User.getUserBalanceFromDB(email, currentUserId);
		int balanceInDbInt = Integer.parseInt(balanceInDb);
		String returnAmount = dynamicsPage.getCloseAmount(moneyType);
		int returnAmountInt = Integer.parseInt(returnAmount);
		String returnAmAfter = dynamicsPage.closeAllInvestment(moneyType);
		int returnAmAfterInt = Integer.parseInt(returnAmAfter);
		if (returnAmAfterInt != 0 && returnAmAfterInt != returnAmountInt) {
			System.out.println("Value is changed");
			returnAmount = returnAmAfter;
			returnAmountInt = returnAmAfterInt;
		}
		String balanceAfter = headerMenuPage.getBalance();
		String balanceIndbAfter = User.getUserBalanceFromDB(email, currentUserId);
		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
		int balanceInDbAfterInt = Integer.parseInt(balanceIndbAfter);
		Assert.assertEquals(balanceInt + returnAmountInt , balanceAfterInt);
		Assert.assertEquals(balanceInDbInt + returnAmountInt, balanceInDbAfterInt);
		
		headerMenuPage.openMyAccountMenu();
		Assert.assertEquals(noOptionMess, myAccountPage.getNoOptionsMessage());
  		myAccountPage.clickClosedInvestmentRadioButton();
  		myAccountPage.clickSubmitButton();
		String settledOptionId = myAccountPage.getSettledOptionInvestmentId();
  		String marketNameFromMyOption = myAccountPage.getSettledOptionAsset();
  		String levelFromMyOptions = myAccountPage.getSettledOptionLevel();
  		String amountFromMyOptions = myAccountPage.getSettledOptionAmount();
  		String returnValueFromMyOptions = myAccountPage.getSettledOptionReturnValue();
  		Assert.assertEquals(asset, marketNameFromMyOption);
  		if (! level.replace("0", "").equals(levelFromMyOptions.replace("0", ""))) {
  			Assert.assertEquals(level.replace("0", "").replace(",", ""), levelFromMyOptions.replace("0", "").replace(",", ""));
  		}	
  		Assert.assertEquals(moneyType + amount + ".00", amountFromMyOptions);
  		Assert.assertEquals(moneyType + returnAmount, returnValueFromMyOptions.replace(".", ""));
  		Assert.assertEquals(optionStatus, myAccountPage.getSettledOptionExpiryLevel());
  		amount = amount + "00";
  		User.verifyDynamicsClosedOption(settledOptionId, amount, returnAmount); 
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
