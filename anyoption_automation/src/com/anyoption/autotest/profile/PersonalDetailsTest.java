package com.anyoption.autotest.profile;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.PersonalDetailsPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class PersonalDetailsTest extends BaseTest {

	public PersonalDetailsTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/UpdatePersonalDetailsTestData.xlsx", numberRows, 7);
	    return(retObjArr);
	}
	/**
	 * Updates personal details
	 * @param skin
	 * @param mandatoryMess
	 * @param incorrectPassMess
	 * @param title
	 * @param emailLetters
	 * @param smsLetters
	 * @param successMess
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void personalDetailsTest(String skin, String mandatoryMess, String incorrectPassMess, String title, String emailLetters, 
			String smsLetters, String successMess) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		PersonalDetailsPage personalDetailsPage = new PersonalDetailsPage(driver);
		String sqlQuery = UserSQL.getUserWhoCanChangePersonalDetails(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = testData.get("id");
		headerMenuPage.openMyAccountMenu();
		personalDetailsPage.openPersonalDetails();
		personalDetailsPage.submitPassword();
		Assert.assertEquals(mandatoryMess, personalDetailsPage.getPasswordError());
		String pass = "errorPass";
		personalDetailsPage.setPassword(pass);
		personalDetailsPage.submitPassword();
		Assert.assertEquals(incorrectPassMess, personalDetailsPage.getPasswordError());
		pass = "123456";
		personalDetailsPage.setPassword(pass);
		personalDetailsPage.submitPassword();
		Assert.assertEquals(title, personalDetailsPage.getScreenTitle());
		Assert.assertEquals(testData.get("first_name"), personalDetailsPage.getFirstName());
		Assert.assertEquals(testData.get("last_name"), personalDetailsPage.getFamily());
		Assert.assertEquals(testData.get("email"), personalDetailsPage.getEmail());
		String street = "New street";
		personalDetailsPage.setStreet(street);
		String number = "7";
		personalDetailsPage.setStreetNumber(number);
		String postCode = "1000";
		personalDetailsPage.setPostCode(postCode);
		String city = "Sofia";
		personalDetailsPage.setCity(city);
		personalDetailsPage.selectCountry("33");
		personalDetailsPage.clickMaleRadioButton();
		String mobilePhone = "0887712345";
		personalDetailsPage.setMobilePhoneInput(mobilePhone);
		String landLinePhone = "029929922";
		personalDetailsPage.setLandLinePhone(landLinePhone);
		Assert.assertEquals(emailLetters, personalDetailsPage.getContactByEmailCheckBoxText());
		Assert.assertEquals(smsLetters, personalDetailsPage.getContactBySmsCheckBoxText());
		personalDetailsPage.clickContactByMailCheckBox();
		personalDetailsPage.clickContactBySmsCheckBox();
		personalDetailsPage.submitPersonalDetails();
		Assert.assertEquals(successMess, personalDetailsPage.getSuccessMessage());
		User.verifyUpdatedPersonalDetails(currentUserId, street, number, postCode, city, mobilePhone, landLinePhone);
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		driver.switchTo().defaultContent();
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
