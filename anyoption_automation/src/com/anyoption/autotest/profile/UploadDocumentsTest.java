package com.anyoption.autotest.profile;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.DocumentsPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.Configuration;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class UploadDocumentsTest extends BaseTest {
	private String docsPath;
	public UploadDocumentsTest() throws IOException {
		super();
		docsPath = Configuration.getProperty("pathToDocsDir");
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/UploadDocumentsTestData.xlsx", numberRows, 7);
	    return(retObjArr);
	}
	/**
	 * Uploads documents
	 * @param skin
	 * @param missMessage
	 * @param approvalMess
	 * @param additionalMess
	 * @param summaryMess
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void uploadDocumentsTest(String skin, String missMessage, String approvalMess, String additionalMess, 
			String summaryMess, String approvedStatus, String approvedMess) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, skin);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		DocumentsPage documentsPage = new DocumentsPage(driver, browserType);
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanUploadDocuments(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = testData.get("id");
		headerMenuPage.openMyAccountMenu();
		documentsPage.openDocuments();
		Assert.assertEquals(missMessage, documentsPage.getUtilityStatusMessage());
		documentsPage.attachFile(docsPath + "Utillity_bill_scan.png", 0);
		Assert.assertEquals(approvalMess, documentsPage.getUtilityStatusMessage());
		Assert.assertEquals(missMessage, documentsPage.getPassOrIdStatusMessage());
		documentsPage.browsePass();
		Assert.assertTrue(documentsPage.isPassAdditionalStausDisplayed());
		Assert.assertEquals(additionalMess, documentsPage.getPassAdditionalStatusMessage());
		documentsPage.selectPass();
		Assert.assertFalse(documentsPage.isPassAdditionalStausDisplayed());
		documentsPage.attachFile(docsPath + "passport_scan.png", 1);
		Assert.assertEquals(approvalMess, documentsPage.getPassOrIdStatusMessage());
		Assert.assertEquals(missMessage, documentsPage.getCcFrontFileStatus());
		documentsPage.attachFile(docsPath + "CC_FRONT_scan.png", 2);
		Assert.assertEquals(approvalMess, documentsPage.getCcFrontFileStatus());
		Assert.assertEquals(missMessage, documentsPage.getCcBackFileStatus());
		documentsPage.attachFile(docsPath + "back.png", 3);
		Assert.assertEquals(approvalMess, documentsPage.getCcBackFileStatus());
		Assert.assertEquals(summaryMess, documentsPage.getDocumentsSummaryText());
		User.approveDocument(currentUserId, "Utillity_bill_scan.png".replace("_", ""));
		User.approveDocument(currentUserId, "passport_scan.png".replace("_", ""));
		User.approveDocument(currentUserId, "CC_FRONT_scan.png".replace("_", ""));
		User.approveDocument(currentUserId, "back.png".replace("_", ""));
		driver.navigate().refresh();
		Assert.assertTrue(documentsPage.isUtilityBrowseButtonDisable());
		Assert.assertTrue(documentsPage.isUtilityIconApproved());
		Assert.assertEquals(approvedStatus, documentsPage.getUtilityStatusMessage());
		Assert.assertTrue(documentsPage.isPassportBrowseButtonDisabled());
		Assert.assertTrue(documentsPage.isPassportIconApproved());
		Assert.assertEquals(approvedStatus, documentsPage.getPassOrIdStatusMessage());
		Assert.assertTrue(documentsPage.isFrontCCBrowseButtonDisabled());
		Assert.assertTrue(documentsPage.isFrontCCIconApproved());
		Assert.assertEquals(approvedStatus, documentsPage.getCcFrontFileStatus());
		Assert.assertTrue(documentsPage.isBackCCBrowseButtonDisabled());
		Assert.assertTrue(documentsPage.isBackCCBrowseButtonDisabled());
		Assert.assertEquals(approvedStatus, documentsPage.getCcBackFileStatus());
		Assert.assertEquals(approvedMess, documentsPage.getApprovedMessage());
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
