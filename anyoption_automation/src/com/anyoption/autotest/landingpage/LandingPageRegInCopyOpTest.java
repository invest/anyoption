package com.anyoption.autotest.landingpage;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LandingPage;
import com.anyoption.autotest.pages.OpenAccountPage;
import com.anyoption.autotest.utils.CreateUser;
import com.anyoption.autotest.utils.ReadTestData;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class LandingPageRegInCopyOpTest extends LandingPageBaseTest {

	public LandingPageRegInCopyOpTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
//		LandingPageTestData read = new LandingPageTestData();
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/top LPs.xlsx", "Copy_Op_Most_Traffic", 1, 1);
	    return(retObjArr);
	}
	
	@Test(dataProvider = "dp")
	public void landingPageRegInCopyOpTest(String params) throws Exception {
		isRegister = false;
		paramsFromDataProvider = params;
		testUrl = "http://cdn.tradingbinaryoption.com";
		String currentUrl = testUrl + params;
		driver.get(currentUrl);
		LandingPage landingPage = new LandingPage(driver);
		OpenAccountPage openAccountPage = new OpenAccountPage(driver, browserType);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		landingPage.tradingbinaryoptionStartNow();
		Assert.assertTrue(landingPage.isCopyOpRegisterFormDisplayed());
		user = new CreateUser();
		String firstName = user.getFirstName();
		String lastName = user.getLastName();
		String email = user.getEMail();
		currentUser = email;
		System.out.println(email);
		String phone = user.getPhone();
		String pass = "123456";
		openAccountPage.cpSelectCountry();
		openAccountPage.cpTypeFirstName(firstName);
		openAccountPage.cpTypeSecondName(lastName);
		openAccountPage.cpTypeEmail(email);
		openAccountPage.cpTypePhone(phone);
		openAccountPage.cpTypePassword(pass);
		openAccountPage.cpTypeConfirmPassword(pass);
		openAccountPage.cpTypeNickName(firstName.replace("Autotest", ""));
		openAccountPage.cpClickTermAndConditionsCheckBox();
		Assert.assertEquals(true, openAccountPage.cpIsCheckedTermAndCond());
		openAccountPage.cpClickOpenAccountButton();
		isRegister = true;
		Assert.assertTrue(headerMenuPage.cpGetBalance().contains("0.00"));
		Assert.assertEquals(firstName + " " + lastName, headerMenuPage.cpGetUserNameFromHeader());
		Assert.assertTrue(driver.getCurrentUrl().contains("/home/main"));
		Assert.assertTrue(landingPage.isPopUpForDepositDisplayed());
	}
}
