package com.anyoption.autotest.landingpage;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.anyoption.autotest.pages.AnyOptionBackEndPage;
import com.anyoption.autotest.utils.Configuration;
import com.anyoption.autotest.utils.CreateUser;
import com.anyoption.autotest.utils.ScreenShot;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class LandingPageBaseTest {
	protected WebDriver driver;
	protected String operationSys;
	protected String browserType;
	protected String clName;
	protected String testUrl;
	protected String paramsFromDataProvider;
	protected String currentUser;
	protected boolean isRegister;
	protected CreateUser user;
	
	public LandingPageBaseTest() throws IOException {
		operationSys = Configuration.getProperty("operationSystem");
		Class<? extends LandingPageBaseTest> className = this.getClass();
		clName = className.toString().replace("class com.anyoption.autotest.", "");
		testUrl = "http://cdn.anyoption.com";
	}
	/**
	 * Starts selenium grid 
	 * @param browser
	 * @throws IOException
	 */
	@Parameters("browser")
	@BeforeClass
	public void launchapp(String browser) throws IOException {
		String URL = testUrl;
		String ip = Configuration.getProperty("ip");
		browserType = browser.replace(" ", "");
		if (browser.equalsIgnoreCase("firefox")) {
			System.out.println(" Executing on FireFox");
			String Node = "http://" + ip + ":5555/wd/hub";
			DesiredCapabilities cap = DesiredCapabilities.firefox();
			cap.setBrowserName("firefox");
			driver = new RemoteWebDriver(new URL(Node), cap);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			driver.navigate().to(URL);
			driver.manage().window().maximize();
		} else if (browser.equalsIgnoreCase("chrome")) {
			if (operationSys.equals("Windows")) {
				System.out.println("Operational system: " + operationSys);
				System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");
			} else if (operationSys.equals("Linux")){
				System.setProperty("webdriver.chrome.driver", "lib/chromedriver");
			} else {
				System.out.println("No correct operational system");
			}
			
			System.out.println(" Executing on CHROME");
	        DesiredCapabilities cap = DesiredCapabilities.chrome();
	        
	        if (operationSys.equals("Linux")) {
	        	ChromeOptions options = new ChromeOptions();
	        	options.addArguments("--test-type");
	        	options.addArguments("--allow-running-insecure-content");
	        	options.setBinary("/usr/bin/google-chrome");
	        	options.addArguments(Arrays.asList("--window-size=1920,1080"));
	        	cap.setCapability(ChromeOptions.CAPABILITY, options);
	        }
	        
	        cap.setBrowserName("chrome");
	        String Node = "http://" + ip + ":5556/wd/hub";
	        driver = new RemoteWebDriver(new URL(Node), cap);
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	         
	        driver.navigate().to(URL);
	        driver.manage().window().maximize();
		} else if (browser.equalsIgnoreCase("internet explorer")) {
			System.out.println(" Executing on IE");
	        DesiredCapabilities cap = DesiredCapabilities.internetExplorer();
	        cap.setBrowserName("internet explorer");
	        String Node = "http://" + ip + ":5558/wd/hub";
	        driver = new RemoteWebDriver(new URL(Node), cap);
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	         
	        driver.navigate().to(URL);
	        driver.manage().window().maximize();
		} else if (browser.equalsIgnoreCase("phantomJs")) {
			System.out.println(" Executing on PhantomJs");
			if (operationSys.equals("Windows")) {
				System.setProperty("phantomjs.binary.path", "lib/phantomjs.exe");
				driver = new PhantomJSDriver();
			} else {
				DesiredCapabilities cap = DesiredCapabilities.phantomjs();
				cap.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "lib/phantomjs");
				cap.setCapability("phantomjs.binary.path", "lib/phantomjs");
				System.setProperty("phantomjs.binary.path", "lib/phantomjs");
				driver = new PhantomJSDriver(cap);
			}
			driver.navigate().to(URL);
			driver.manage().window().maximize();
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
	}
	/**
	 * Generates screen shot if test fail
	 * @param result
	 * @throws IOException
	 */
	@Parameters("browser")
	@AfterMethod
	public void afterMethodTearDown(ITestResult result, @Optional String browser) throws IOException {
		String params[] = paramsFromDataProvider.split("\\?");
		if (ITestResult.FAILURE == result.getStatus()) {
			String brName = browser.replace(" ", "");
			String name = result.getMethod().getMethodName();
			String screenShotName = name + params[0] + brName;
			ScreenShot scShot = new ScreenShot();
			scShot.takeScreenShot(driver, screenShotName);
			System.out.println("Screenshot generated");
		} 
		if (ITestResult.FAILURE == result.getStatus()) {
			System.out.println("-");
			System.out.println(clName + params[0] + " FAIL with " + currentUser);
			System.out.println("-");
		} else {
			System.out.println("-");
			System.out.println(clName + " " + params[0] + " PASS");
			System.out.println("-");
		}
		UtilsMethods.sleep(1500);
		
		if (isRegister == true) {
			driver.get(Configuration.getProperty("backEndUrl"));
			AnyOptionBackEndPage anyOptionBackEndPage = new AnyOptionBackEndPage(driver, browserType);
			anyOptionBackEndPage.loginInBackEnd();
			try {
				anyOptionBackEndPage.searchUser(user);
				anyOptionBackEndPage.makeTester(user);
				anyOptionBackEndPage.logoutFromBackEnd();
			} catch (TimeoutException e) {
				System.out.println("Back end exception");
			}
		}	
		driver.manage().deleteAllCookies();
	}
	/**
	 * Closes browser
	 * @throws IOException 
	 */
	@AfterClass
	public void afterClassTearDown() throws IOException {
//		driver.manage().deleteAllCookies();
        driver.quit();
	}
}
