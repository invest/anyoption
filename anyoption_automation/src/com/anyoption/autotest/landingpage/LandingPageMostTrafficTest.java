package com.anyoption.autotest.landingpage;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LandingPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.OpenAccountPage;
import com.anyoption.autotest.utils.Configuration;
import com.anyoption.autotest.utils.CreateUser;
import com.anyoption.autotest.utils.ReadTestData;

public class LandingPageMostTrafficTest extends LandingPageBaseTest {

	public LandingPageMostTrafficTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/top LPs.xlsx", "Most_Traffic", 8, 1);
	    return(retObjArr);
	}
	
	@Test(dataProvider = "dp")
	public void landingPageMostTrafficTest(String params) throws Exception {
		isRegister = false;
		paramsFromDataProvider = params;
		testUrl = "http://cdn.tradingbinaryoption.com";
		String currentUrl = testUrl + params;
		driver.get(currentUrl);
		LandingPage landingPage = new LandingPage(driver);
		OpenAccountPage openAccountPage = new OpenAccountPage(driver, browserType);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		LoginPage loginPage = new LoginPage(driver, browserType, currentUrl);
		landingPage.startNow(0);
		landingPage.tradingbinaryoptionStartNow();
		Assert.assertTrue(landingPage.isRegisterFormDisplayed());
		user = new CreateUser();
		String firstName = user.getFirstName();
		String lastName = user.getLastName();
		String email = user.getEMail();
		currentUser = email;
		System.out.println(email);
		String phone = user.getPhone();
		String pass = "123456";
		openAccountPage.selectCountry();
		openAccountPage.typeFirstName(firstName);
		openAccountPage.typeSecondName(lastName);
		openAccountPage.typeEmail(email);
		openAccountPage.typePhone(phone);
		openAccountPage.typePassword(pass);
		openAccountPage.typeConfirmPassword(pass);
		openAccountPage.clickTermAndConditionsCheckBox();
		if (! browserType.equalsIgnoreCase("firefox") || ! Configuration.getProperty("operationSystem").equals("Linux")) {
			Assert.assertTrue(openAccountPage.isNamesOk());
			Assert.assertTrue(openAccountPage.isEmailOk());
			Assert.assertTrue(openAccountPage.isPhoneOk());
			Assert.assertTrue(openAccountPage.isPasswordOk());
			Assert.assertTrue(openAccountPage.isConfirmPassOk());
		}	
		Assert.assertEquals(true, openAccountPage.isCheckedTermAndCond());
		openAccountPage.clickOpenAccountButton();
		isRegister = true;
		
		if (landingPage.isUrlFirstNewCard() == false) {
			System.out.println("Redirect problem");
			loginPage.loginWithDesiredUser("https://www.anyoption.com/jsp/login.jsf", user);
		} else {
			Assert.assertTrue(driver.getCurrentUrl().contains("jsp/pages/firstNewCard.jsf"));
		}
		Assert.assertTrue(headerMenuPage.getBalance().contains("0.00"));
		Assert.assertEquals(firstName, headerMenuPage.getUserNameFromHeader());
		loginPage.logout();
	}
}
