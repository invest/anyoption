package com.anyoption.autotest.landingpage;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LandingPage;
import com.anyoption.autotest.pages.OpenAccountPage;
import com.anyoption.autotest.utils.Configuration;
import com.anyoption.autotest.utils.CreateUser;
import com.anyoption.autotest.utils.LandingPageTestData;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class LandingPageAOFinanceWeeklyTest extends LandingPageBaseTest {
	public LandingPageAOFinanceWeeklyTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		LandingPageTestData read = new LandingPageTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/top LPs.xlsx", "ao_finance_weekly", 9, 1);
	    return(retObjArr);
	}
	/**
	 * Creates accounts from landing URL
	 * @param params
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void landingPageAOFinanceWeeklyTest(String params) throws Exception {
		isRegister = false;
		paramsFromDataProvider = params;
		String currentUrl = testUrl + params;
		driver.get(currentUrl);
		LandingPage landingPage = new LandingPage(driver);
		OpenAccountPage openAccountPage = new OpenAccountPage(driver, browserType);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		landingPage.startNow(0);
		Assert.assertTrue(landingPage.isRegisterFormDisplayed());
		user = new CreateUser();
		String firstName = user.getFirstName();
		String lastName = user.getLastName();
		String email = user.getEMail();
		currentUser = email;
		System.out.println(email);
		String phone = user.getPhone();
		String pass = "123456";
		openAccountPage.selectCountry();
		openAccountPage.typeFirstName(firstName);
		openAccountPage.typeSecondName(lastName);
		openAccountPage.typeEmail(email);
		openAccountPage.typePhone(phone);
		openAccountPage.typePassword(pass);
		openAccountPage.typeConfirmPassword(pass);
		openAccountPage.clickTermAndConditionsCheckBox();
		if (! browserType.equalsIgnoreCase("firefox") || ! Configuration.getProperty("operationSystem").equals("Linux")) {
			Assert.assertTrue(openAccountPage.isNamesOk());
			Assert.assertTrue(openAccountPage.isEmailOk());
			Assert.assertTrue(openAccountPage.isPhoneOk());
			Assert.assertTrue(openAccountPage.isPasswordOk());
			Assert.assertTrue(openAccountPage.isConfirmPassOk());
		}	
		Assert.assertEquals(true, openAccountPage.isCheckedTermAndCond());
		openAccountPage.clickOpenAccountButton();
		isRegister = true;
		Assert.assertTrue(headerMenuPage.getBalance().contains("0.00"));
		Assert.assertEquals(firstName, headerMenuPage.getUserNameFromHeader());
		Assert.assertTrue(driver.getCurrentUrl().contains("jsp/pages/firstNewCard.jsf"));
	}
}
