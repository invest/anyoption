package com.anyoption.autotest.questionnaire;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.MessagesPage;
import com.anyoption.autotest.pages.QuestionnairePage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimirm
 *
 */
public class MakeQuestionnaireTest extends BaseTest {
	public MakeQuestionnaireTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/MakeQuestionnaireTestData.xlsx", numberRows, 3);
	    return(retObjArr);
	}
	/**
	 * Fill questionnaire form
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void makeQuestionnaireTest(String skin, String title, String subjectText) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		String sqlQuery = UserSQL.getUserWhoCanMakeQuestionnaire(UtilsMethods.getSkinId(skin));
		HashMap<String, String> data = loginPage.loginWithUserFromDB(sqlQuery, false);
		System.out.println(data);
		currentUserId = data.get("id");
		QuestionnairePage questionnairePage = new QuestionnairePage(driver);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		MessagesPage messagesPage = new MessagesPage(driver, browserType);
		Assert.assertEquals(title, questionnairePage.getCurrentScreenTitle());
		questionnairePage.selectAnnualIncome(1);
		questionnairePage.selectSourceIncome(1);
		questionnairePage.selectEducation(1);
		questionnairePage.selectEmoploymentStatus(1);
		questionnairePage.selectProfession(1);
		questionnairePage.moveNext();
		questionnairePage.selectTradingExperience(1);
		questionnairePage.selectFinancialDerivative(1);
		questionnairePage.selectTradeCount(1);
		questionnairePage.selectNatureOfTrading(1);
		questionnairePage.checkRiskCheckBox(operationSys);
		questionnairePage.tradeNow(operationSys);
		questionnairePage.checkInPopUp();
		questionnairePage.confirmPopUp();
		Assert.assertTrue(questionnairePage.isBinaryOptionsActive());
		headerMenuPage.switchDriver();
		headerMenuPage.openMessages();
		driver.switchTo().defaultContent();
		Assert.assertEquals(subjectText, messagesPage.getMailSubject(1));
		messagesPage.openEmail(1);
		Assert.assertEquals(subjectText, messagesPage.getSubjectFromTradeMail());
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
