package com.anyoption.autotest.utils;


import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

/**
 * 
 * @author vladimirm
 *
 */
public class BaseTest {
	protected WebDriver driver;
	protected String browserType;
	protected String testUrl;
	protected String clName;
	protected String parameterSkin = "";
	protected boolean afterMethod = false;
	protected int numberRows;
	protected String operationSys;
	protected String currentUserId = null;
	public static ArrayList<String> users = new ArrayList<>();
	private String ip;
	private Process grid;
	private Process browserNode;
	private String mode;
	
	public BaseTest() throws IOException {
		testUrl = Configuration.getProperty("testUrl");
		numberRows = Integer.parseInt(Configuration.getProperty("numberRows"));
		operationSys = Configuration.getProperty("operationSystem");
		Class<? extends BaseTest> className = this.getClass();
		clName = className.toString().replace("class com.anyoption.autotest.", "");
		ip = Configuration.getProperty("ip");
		mode = Configuration.getProperty("mode");
	}
	/**
	 * Set up 
	 * @throws IOException 
	 */
	private void setUp() throws IOException {
		browserType = Configuration.getProperty("browserType");
		System.out.println("Browser type is " + browserType);
		System.out.println();
		System.out.println("Running " + clName);
		System.out.println();
		if (browserType.equals("FireFox")) {
			driver = new FirefoxDriver();
		} else if (browserType.equals("Chrome")) {
			String operationSys = Configuration.getProperty("operationSystem");
			if (operationSys.equals("Windows")) {
				System.out.println("Operational system: " + operationSys);
				System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (operationSys.equals("Linux")){
				System.setProperty("webdriver.chrome.driver", "lib/chromedriver");
				driver = new ChromeDriver();
			} else {
				System.out.println("No correct operational system");
			}
		} else if (browserType.equals("internetexplorer")) {
			System.setProperty("webdriver.ie.driver", "lib/IEDriverServer.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			driver = new InternetExplorerDriver(capabilities);
		} else if (browserType.equals("phantomJs")) {
			System.setProperty("phantomjs.binary.path", "lib/phantomjs.exe");
			driver = new PhantomJSDriver();
		}
		driver.get(testUrl);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	/**
	 * Starts selenium grid 
	 * @param browser
	 * @throws IOException
	 */
	@Parameters("browser")
	private void launchapp(String browser) throws IOException {
		String URL = testUrl;
		browserType = browser.replace(" ", "");
		System.out.println("Browser type: " + browserType);
		if (browser.equalsIgnoreCase("firefox")) {
			System.out.println(" Executing on FireFox");
			String Node = "http://" + ip + ":5555/wd/hub";
			DesiredCapabilities cap = DesiredCapabilities.firefox();
			cap.setBrowserName("firefox");
			driver = new RemoteWebDriver(new URL(Node), cap);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			driver.navigate().to(URL);
			
//			driver.manage().window().maximize();
			
			Dimension targetSize = new Dimension(1920, 1080);
			driver.manage().window().setSize(targetSize);
		} else if (browser.equalsIgnoreCase("chrome")) {
			if (operationSys.equals("Windows")) {
				System.out.println("Operational system: " + operationSys);
				System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");
			} else if (operationSys.equals("Linux")){
				System.setProperty("webdriver.chrome.driver", "lib/chromedriver");
			} else {
				System.out.println("No correct operational system");
			}
			
			System.out.println(" Executing on CHROME");
	        DesiredCapabilities cap = DesiredCapabilities.chrome();
	        
	        if (operationSys.equals("Linux")) {
	        	ChromeOptions options = new ChromeOptions();
	        	options.addArguments("--test-type");
	        	options.addArguments("--allow-running-insecure-content");
	        	options.setBinary("/usr/bin/google-chrome");
	        	options.addArguments(Arrays.asList("--window-size=1920,1080"));
	        	cap.setCapability(ChromeOptions.CAPABILITY, options);
	        }
	        
	        cap.setBrowserName("chrome");
	        String Node = "http://" + ip + ":5556/wd/hub";
	        driver = new RemoteWebDriver(new URL(Node), cap);
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	         
	        driver.navigate().to(URL);
	        driver.manage().window().maximize();
		} else if (browser.equalsIgnoreCase("internet explorer")) {
			System.out.println(" Executing on IE");
	        DesiredCapabilities cap = DesiredCapabilities.internetExplorer();
	        cap.setBrowserName("internet explorer");
	        String Node = "http://" + ip + ":5558/wd/hub";
	        driver = new RemoteWebDriver(new URL(Node), cap);
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	         
	        driver.navigate().to(URL);
	        driver.manage().window().maximize();
		} else if (browser.equalsIgnoreCase("phantomJs")) {
			System.out.println(" Executing on PhantomJs");
			if (operationSys.equals("Windows")) {
				System.setProperty("phantomjs.binary.path", "lib/phantomjs.exe");
				driver = new PhantomJSDriver();
			} else {
				DesiredCapabilities cap = DesiredCapabilities.phantomjs();
				cap.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "lib/phantomjs");
				cap.setCapability("phantomjs.binary.path", "lib/phantomjs");
				System.setProperty("phantomjs.binary.path", "lib/phantomjs");
				driver = new PhantomJSDriver(cap);
			}
			driver.navigate().to(URL);
			driver.manage().window().maximize();
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		System.out.println("------------------------------------------------------------------------------");
		System.out.println(clName + " " + "browser " + browser);
		System.out.println("------------------------------------------------------------------------------");
	}
	/**
	 * Starts selenium grid or set up browser according to "mode" parameter in config.properties file
	 * @param browser
	 * @throws IOException
	 */
	@Parameters("browser")
	@BeforeClass
	public void startSuite(@Optional String browser) throws IOException {
		if (mode.equals("testMode")) {
			this.launchapp(browser);
		} else if (mode.equals("devMode")) {
			this.setUp();
		}
	}
	/**
	 * Starts selenium grid
	 * @param context
	 * @throws IOException
	 * @throws InterruptedException
	 */
	@BeforeSuite
	public void startSeleniumGrid(ITestContext context) throws IOException, InterruptedException {
		if (mode.equals("testMode") && operationSys.equals("Windows")) {
			String browser = context.getCurrentXmlTest().getParameter("browser");
			File serverFile = new File("lib/selenium-server-standalone-2.53.1.jar");
			String serverPath = serverFile.getAbsolutePath();
			
			String gridCommand = "java -jar " + serverPath + " -port 4444 -role hub -nodeTimeout 1000";  
			grid = Runtime.getRuntime().exec(gridCommand);
			
			File driverFile;
			String driverPath;
			String command;
			
			if (browser.equals("chrome")) {
				driverFile = new File("lib/chromedriver.exe");
				driverPath = driverFile.getAbsolutePath();
				command = "java -Dwebdriver.chrome.driver=" + driverPath + " -jar " + serverPath + " -role webdriver -hub  http://" + ip + ":4444/grid/register -browser browserName=chrome,platform=WINDOWS -port 5556";
				browserNode = Runtime.getRuntime().exec(command);
				System.out.println("Chrome node is run");
			} else if (browser.equalsIgnoreCase("firefox")) {
				command = "java -jar " + serverPath + " -role node -hub http://" + ip + ":4444/grid/register -browser browserName=firefox -port 5555";
				browserNode = Runtime.getRuntime().exec(command);
				System.out.println("FireFox node is run");
			}
		}
	}
	/**
	 * Closes browser
	 * @throws IOException 
	 */
	@AfterClass
	public void afterClassTearDown() throws IOException {
        driver.quit();
	}
	/**
	 * Generates screen shot if test fail
	 * @param result
	 * @throws IOException
	 */
	@Parameters("browser")
	@AfterMethod
	public void afterMethodTearDown(ITestResult result, @Optional String browser) throws IOException {
		if (currentUserId != null) {
        	System.out.println("Delete current user " + currentUserId);
        	users.remove(currentUserId);
        	System.out.println(users);
        }	
		if (afterMethod == false) {
			if (ITestResult.FAILURE == result.getStatus()) {
				String brName = browser.replace(" ", "");
				String name = result.getMethod().getMethodName();
				String screenShotName = name + parameterSkin + brName;
				ScreenShot scShot = new ScreenShot();
				scShot.takeScreenShot(driver, screenShotName);
				System.out.println("Screenshot generated");
			} 
		}
		if (ITestResult.FAILURE == result.getStatus()) {
			System.out.println("-");
			System.out.println(clName + " " + parameterSkin + " FAIL with user " + currentUserId);
			System.out.println("-");
		} else {
			System.out.println("-");
			System.out.println(clName + " " + parameterSkin + " PASS");
			System.out.println("-");
		}
		driver.manage().deleteAllCookies();
	}
	/**
	 * Deletes all temporary fails after executing test suite (it is for windows only)
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	@AfterSuite
	public void afterSuite() throws IOException, InterruptedException {
		if (operationSys.equals("Windows")) {
			System.out.println("Delete all temp fails");
			String filePath = "";
			filePath = Configuration.getProperty("tempDirPath");
			try {
				FileUtils.deleteDirectory(new File(filePath));
			} catch (IOException e) {
			}
			if (browserType.equalsIgnoreCase("internetexplorer")) {
				UtilsMethods.killIEBrowser();
			}
			if (browserNode != null) {
				browserNode.waitFor();
				browserNode.destroy();
				System.out.println("Node is stoped");
			}
			if (grid != null) {
				grid.destroy();
				grid.waitFor();
				System.out.println("Grid is stoped");
			}
		}
	}
}	
