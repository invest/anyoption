package com.anyoption.autotest.utils;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 
 * @author vladimirm
 *
 */
public class Configuration {
	/**
	 * Gets information from configuration file
	 * @param key - String 
	 * @return String
	 * @throws IOException
	 */
	public static String getProperty(String key) throws IOException {
		Properties prop = new Properties();
		InputStream input = null;
		String filename = "config.properties";
		input = new FileInputStream(filename);
		prop.load(input);
		String property = prop.getProperty(key);
		return property;
	}
}
