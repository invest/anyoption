package com.anyoption.autotest.utils;

public class UserSQL {
	private static String returnedUsers = "check";
	/**
	 * Gets user who is created
	 * @param skinId
	 * @return String 
	 */
	public static String getUserWhoIsCreated(String skinId) {
		String sqlQuery = "SELECT * FROM users "
				+ "LEFT JOIN CREDIT_CARDS ON CREDIT_CARDS.USER_ID = users.ID "
				+ "WHERE "
				+ "EMAIL LIKE '%@anyoption.com%' "
				+ "AND PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
				+ "AND FIRST_NAME LIKE '%Autotest%' "
				+ "AND users.BALANCE = 0 "
				+ "AND users.skin_id = " + skinId + " "
				+ "AND CREDIT_CARDS.USER_ID IS NULL " 
				+ "AND users.id NOT IN (" + returnedUsers + ") "
//				+ "AND users.TIME_CREATED < SYSDATE - 1"
				+ "AND users.TIME_LAST_LOGIN < SYSDATE - INTERVAL '5' MINUTE "
				+ "ORDER BY users.TIME_CREATED DESC ";
		return sqlQuery;
	}
	/**
	 * Gets user who is created
	 * @param skinId
	 * @return String 
	 */
	public static String getUserWhoIsCreatedWithoutCheck(String skinId) {
		String sqlQuery = "SELECT * FROM users "
				+ "LEFT JOIN CREDIT_CARDS ON CREDIT_CARDS.USER_ID = users.ID "
				+ "WHERE "
				+ "EMAIL LIKE '%@anyoption.com%' "
				+ "AND PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
				+ "AND FIRST_NAME LIKE '%Autotest%' "
				+ "AND users.BALANCE = 0 "
				+ "AND users.skin_id = " + skinId + " "
				+ "AND CREDIT_CARDS.USER_ID IS NULL " 
//				+ "AND users.id NOT IN (" + returnedUsers + ") "
//				+ "AND users.TIME_CREATED < SYSDATE - 1"
				+ "AND users.TIME_LAST_LOGIN < SYSDATE - INTERVAL '5' MINUTE "
				+ "ORDER BY users.TIME_CREATED DESC ";
		return sqlQuery;
	}
	/**
	 * Gets user who can make questionnaire
	 * @param skinId
	 * @return String
	 */
	public static String getUserWhoCanMakeQuestionnaire(String skinId) {
		String sqlQuery = "SELECT * FROM users "
				+ "WHERE PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
				+ "AND FIRST_NAME LIKE '%Autotest%' "
				+ "AND EMAIL LIKE '%@anyoption.com%' "
				+ "AND BALANCE > 2500 "
				+ "AND SKIN_ID = " + skinId + " "
				+ "AND CLASS_ID = 0 "
				+ "AND id NOT IN (" + returnedUsers + ") "
				+ "AND ( TIME_CREATED < TO_DATE('2016-06-06','yyyy-mm-dd') or TIME_CREATED >= TO_DATE('2016-06-08','yyyy-mm-dd') ) "
				+ "AND users.TIME_LAST_LOGIN < SYSDATE - INTERVAL '5' MINUTE "
				+ "AND 4 > (SELECT COUNT (*) FROM users_regulation_history WHERE users.id = user_id) "
				+ "ORDER BY TIME_CREATED DESC";
		return sqlQuery;
	}
	/**
	 * Gets user who can make deposit
	 * @param skinId
	 * @return String
	 */
	public static String getUserWhoCanMakeDeposit(String skinId) {
		String sqlQuery = "SELECT * FROM users "
				+ "WHERE PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
				+ "AND FIRST_NAME LIKE '%Autotest%' "
				+ "AND EMAIL LIKE '%@anyoption.com%' "
				+ "AND BALANCE != 0 "
				+ "AND SKIN_ID = " + skinId + " "
				+ "AND CLASS_ID = 0 "
				+ "AND COUNTRY_ID = 33 "
				+ "AND id NOT IN (" + returnedUsers + ") "
				+ "AND 5 < (SELECT COUNT (*) FROM users_regulation_history WHERE users.id = user_id) "
				+ "AND users.TIME_LAST_LOGIN < SYSDATE - INTERVAL '5' MINUTE "
				+ "ORDER BY TIME_CREATED DESC";
		return sqlQuery;
	}
	/**
	 * Gets user who can withdraw deposit
	 * @param skinId
	 * @return String
	 */
	public static String getUserWhoCanWithdrawDeposit(String skinId) {
		String sqlQuery = "SELECT * FROM users "
				+ "WHERE PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
				+ "AND FIRST_NAME LIKE '%Autotest%' "
				+ "AND EMAIL LIKE '%@anyoption.com%' "
				+ "AND BALANCE >= 20000 "
				+ "AND SKIN_ID = " + skinId + " "
				+ "AND CLASS_ID = 0 "
				+ "AND COUNTRY_ID = 33 "
				+ "AND IS_ACTIVE = 1 "
				+ "AND id NOT IN (" + returnedUsers + ") "
				+ "AND 5 < (SELECT COUNT (*) FROM users_regulation_history WHERE users.id = user_id) "
				+ "AND id NOT IN (SELECT user_id FROM TRANSACTIONS WHERE status_id = 4) "
//				+ "AND users.TIME_LAST_LOGIN < SYSDATE - INTERVAL '1' HOUR "
				+ "ORDER BY TIME_CREATED DESC";
		return sqlQuery;
	}
	/**
	 * Gets user who can reverse deposit
	 * @param skinId
	 * @return String
	 */
	public static String getUserWhoCanReverseWithdraw(String skinId) {
		String sqlQuery = "SELECT * FROM users "
				+ "WHERE PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
				+ "AND FIRST_NAME LIKE '%Autotest%' "
				+ "AND EMAIL LIKE '%@anyoption.com%' "
				+ "AND BALANCE >= 10000 AND BALANCE < 200000 "
				+ "AND SKIN_ID = " + skinId + " "
				+ "AND CLASS_ID = 0 "
				+ "AND id NOT IN (" + returnedUsers + ") "
				+ "AND 5 < (SELECT COUNT (*) FROM users_regulation_history WHERE users.id = user_id) "
				+ "AND id IN (SELECT user_id FROM TRANSACTIONS WHERE status_id = 4) "
//				+ "AND TIME_LAST_LOGIN < SYSDATE - INTERVAL '1' HOUR "
				+ "ORDER BY users.TIME_LAST_LOGIN";
		return sqlQuery;
	}
	/**
	 * Gets user who can make investment and has no current investments
	 * @param skinId
	 * @return
	 */
	public static String getUserWhoCanMakeInvestment(String skinId) {
		String sqlQuery = "SELECT * FROM users "
				+ "INNER JOIN USERS_ACTIVE_DATA ON USERS_ACTIVE_DATA.USER_ID = users.ID AND USERS_ACTIVE_DATA.SHOW_CANCEL_INVESTMENT = 1 "
				+ "WHERE PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
				+ "AND FIRST_NAME LIKE '%Autotest%' "
				+ "AND EMAIL LIKE '%@anyoption.com%' "
				+ "AND BALANCE >= 20000 "
				+ "AND SKIN_ID = " + skinId + " "
				+ "AND CLASS_ID = 0 "
				+ "AND COUNTRY_ID = 33 "
				+ "AND IS_ACTIVE = 1 "
				+ "AND users.id NOT IN (" + returnedUsers + ") "
//				+ "AND TIME_LAST_LOGIN < SYSDATE - INTERVAL '1' HOUR "
				+ "AND 5 < (SELECT COUNT (*) FROM users_regulation_history WHERE users.id = user_id) "
				+ "AND users.id NOT IN (SELECT user_id FROM TRANSACTIONS WHERE status_id = 4) "
				+ "AND users.id NOT IN (SELECT user_id FROM INVESTMENTS WHERE INVESTMENTS.IS_SETTLED = 0) "
				+ "ORDER BY TIME_CREATED DESC";
		return sqlQuery;
	}
	/**
	 * Gets user who can not make bubbles investment and has no current investments
	 * @param skinId
	 * @return
	 */
	public static String getUserWhoCanNotMakeBubblesInvestment(String skinId) {
		String sqlQuery = "SELECT * FROM users "
				+ "WHERE PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
				+ "AND FIRST_NAME LIKE '%Autotest%' "
				+ "AND EMAIL LIKE '%@anyoption.com%' "
				+ "AND BALANCE >= 20000 AND BALANCE <= 80000 "
				+ "AND SKIN_ID = " + skinId + " "
				+ "AND CLASS_ID = 0 "
				+ "AND COUNTRY_ID = 33 "
				+ "AND IS_ACTIVE = 1 "
				+ "AND id NOT IN (" + returnedUsers + ") "
//				+ "AND TIME_LAST_LOGIN < SYSDATE - INTERVAL '1' HOUR "
				+ "AND 5 < (SELECT COUNT (*) FROM users_regulation_history WHERE users.id = user_id) "
				+ "AND id NOT IN (SELECT user_id FROM TRANSACTIONS WHERE status_id = 4) "
				+ "AND id NOT IN (SELECT user_id FROM INVESTMENTS WHERE INVESTMENTS.IS_SETTLED = 0) "
				+ "ORDER BY TIME_CREATED DESC";
		return sqlQuery;
	}
	/**
	 * Gets user who can make investment and has no current investments
	 * @param skinId
	 * @return
	 */
	public static String getUserWhoCanMakeLongTermInvestment(String skinId) {
		String sqlQuery = "SELECT * FROM users "
				+ "WHERE PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
				+ "AND FIRST_NAME LIKE '%Autotest%' "
				+ "AND EMAIL LIKE '%@anyoption.com%' "
				+ "AND BALANCE >= 10000 "
				+ "AND SKIN_ID = " + skinId + " "
				+ "AND CLASS_ID = 0 "
				+ "AND COUNTRY_ID = 33 "
				+ "AND id NOT IN (" + returnedUsers + ") "
//				+ "AND TIME_LAST_LOGIN < SYSDATE - INTERVAL '1' HOUR "
				+ "AND 5 < (SELECT COUNT (*) FROM users_regulation_history WHERE users.id = user_id) "
				+ "AND id NOT IN (SELECT user_id FROM TRANSACTIONS WHERE status_id = 4) "
				+ "AND id NOT IN (SELECT user_id FROM INVESTMENTS WHERE IS_SETTLED = 0 AND type_id != 3) "
				+ "ORDER BY TIME_CREATED";
		return sqlQuery;
	}
	/**
	 * Gets user who can not make investment and has no current investments
	 * @param skinId
	 * @return
	 */
	public static String getUserWhoCanNotMakeInvestment(String skinId) {
		String sqlQuery = "SELECT * FROM users "
				+ "WHERE PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
				+ "AND FIRST_NAME LIKE '%Autotest%' "
				+ "AND EMAIL LIKE '%@anyoption.com%' "
//				+ "AND BALANCE < 50000 "
				+ "AND BALANCE > 20000 "
				+ "AND SKIN_ID = " + skinId + " "
				+ "AND CLASS_ID = 0 "
				+ "AND COUNTRY_ID = 33 "
				+ "AND IS_ACTIVE = 1 "
				+ "AND id NOT IN (" + returnedUsers + ") "
//				+ "AND TIME_LAST_LOGIN < SYSDATE - INTERVAL '1' HOUR "
				+ "AND 5 < (SELECT COUNT (*) FROM users_regulation_history WHERE users.id = user_id) "
				+ "AND id NOT IN (SELECT user_id FROM TRANSACTIONS WHERE status_id = 4) "
				+ "AND id NOT IN (SELECT user_id FROM INVESTMENTS WHERE IS_SETTLED = 0 AND type_id != 3) "
				+ "ORDER BY TIME_CREATED DESC";
		return sqlQuery;
	}
	/**
	 * Gets user who is not uploaded documents
	 * @param skinId
	 * @return
	 */
	public static String getUserWhoCanUploadDocuments(String skinId) {
		String sqlQuery = "SELECT * FROM users "
				+ "WHERE PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
				+ "AND FIRST_NAME LIKE '%Autotest%' "
				+ "AND EMAIL LIKE '%@anyoption.com%' "
				+ "AND BALANCE != 0 AND BALANCE < 200000 "
				+ "AND SKIN_ID = " + skinId + " "
				+ "AND CLASS_ID = 0 "
				+ "AND IS_ACTIVE = 1 "
				+ "AND id NOT IN (" + returnedUsers + ") "
				+ "AND 5 < (SELECT COUNT (*) FROM users_regulation_history WHERE users.id = user_id) "
				+ "AND users.TIME_LAST_LOGIN < SYSDATE - INTERVAL '5' MINUTE "
				+ "AND 0 = (SELECT COUNT (*) FROM files WHERE users.id = user_id AND files.FILE_STATUS_ID != 1) "
				+ "ORDER BY TIME_CREATED DESC";
		return sqlQuery;
	}
	/**
	 * Gets user who can make investment and has no current investments
	 * @param skinId
	 * @return
	 */
	public static String getUserWhoCanMakeDeviationCheckInvestment(String skinId) {
		String sqlQuery = "SELECT * FROM users "
				+ "INNER JOIN USERS_ACTIVE_DATA ON USERS_ACTIVE_DATA.USER_ID = users.ID AND USERS_ACTIVE_DATA.SHOW_CANCEL_INVESTMENT = 1 "
				+ "WHERE PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
				+ "AND FIRST_NAME LIKE '%Autotest%' "
				+ "AND EMAIL LIKE '%@anyoption.com%' "
				+ "AND BALANCE >= 40000 "
				+ "AND SKIN_ID = " + skinId + " "
				+ "AND CLASS_ID = 0 "
				+ "AND COUNTRY_ID = 33 "
				+ "AND users.id NOT IN (" + returnedUsers + ") "
//				+ "AND TIME_LAST_LOGIN < SYSDATE - INTERVAL '1' HOUR "
				+ "AND 5 < (SELECT COUNT (*) FROM users_regulation_history WHERE users.id = user_id) "
				+ "AND users.id NOT IN (SELECT user_id FROM TRANSACTIONS WHERE status_id = 4) "
				+ "AND users.id NOT IN (SELECT user_id FROM INVESTMENTS WHERE INVESTMENTS.IS_SETTLED = 0) "
				+ "ORDER BY TIME_CREATED";
		return sqlQuery;
	}
	/**
	 * Gets user who can delete credit card
	 * @param skinId
	 * @return
	 */
	public static String getUserWhoCanDeleteCreditCard(String skinId) {
		String sqlQuery = "SELECT * FROM users "
					+ "INNER JOIN CREDIT_CARDS ON CREDIT_CARDS.USER_ID = users.ID AND CREDIT_CARDS.IS_ALLOWED = 1 "
					+ "AND CREDIT_CARDS.CC_NUMBER LIKE '114_-40_-80_61_-52_-1_32_116_-115_2_-83_-101_54_50_1_13_-59_5_54_-47_59_-116_30_34' " 
					+ "AND CREDIT_CARDS.IS_VISIBLE = 1 "
					+ "WHERE PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
					+ "AND FIRST_NAME LIKE '%Autotest%' "
					+ "AND EMAIL LIKE '%@anyoption.com%' "
					+ "AND BALANCE != 0 AND BALANCE < 200000 "
					+ "AND SKIN_ID = " + skinId + " "
					+ "AND CLASS_ID = 0 "
					+ "AND users.COUNTRY_ID = 33 "
					+ "AND users.id NOT IN (" + returnedUsers + ") "
					+ "AND 5 < (SELECT COUNT (*) FROM users_regulation_history WHERE users.id = user_id) "
					+ "ORDER BY users.TIME_CREATED";
		return sqlQuery;
	}
	/**
	 * Gets user who can edit credit card
	 * @param skinId
	 * @return
	 */
	public static String getUserWhoCanEditCreditCard(String skinId) {
		String sqlQuery = "SELECT * FROM users "
					+ "INNER JOIN CREDIT_CARDS ON CREDIT_CARDS.USER_ID = users.ID AND CREDIT_CARDS.IS_ALLOWED = 1 "
					+ "AND CREDIT_CARDS.IS_VISIBLE = 1 AND CREDIT_CARDS.EXP_YEAR = 17 "
					+ "WHERE PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
					+ "AND FIRST_NAME LIKE '%Autotest%' "
					+ "AND EMAIL LIKE '%@anyoption.com%' "
					+ "AND SKIN_ID = " + skinId + " "
					+ "AND CLASS_ID = 0 "
					+ "AND BALANCE != 0 "
					+ "AND users.COUNTRY_ID = 33 "
					+ "AND users.id NOT IN (" + returnedUsers + ") "
					+ "AND 5 < (SELECT COUNT (*) FROM users_regulation_history WHERE users.id = user_id) "
					+ "ORDER BY users.TIME_CREATED";
		return sqlQuery;
	}
	/**
	 * Gets user who can make dynamics investment
	 * @param skinId
	 * @return
	 */
	public static String getUserWhoCanMakeDynamicsInvestment(String skinId) {
		String sqlQuery = "SELECT * FROM users "
						+ "INNER JOIN USERS_ACTIVE_DATA ON USERS_ACTIVE_DATA.USER_ID = users.ID AND USERS_ACTIVE_DATA.HAS_DYNAMICS = 1 "
						+ "AND USERS_ACTIVE_DATA.SHOW_CANCEL_INVESTMENT = 1"
						+ "WHERE users.PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
						+ "AND users.FIRST_NAME LIKE '%Autotest%' "
						+ "AND users.EMAIL LIKE '%@anyoption.com%' "
						+ "AND users.BALANCE >= 10000 "
						+ "AND SKIN_ID = " + skinId + " " 
						+ "AND users.CLASS_ID = 0 "
						+ "AND users.COUNTRY_ID = 33 "
						+ "AND users.IS_ACTIVE = 1 "
						+ "AND users.id NOT IN (" + returnedUsers + ") "
						+ "AND 5 < (SELECT COUNT (*) FROM users_regulation_history WHERE users.id = user_id) "
						+ "AND users.id NOT IN (SELECT user_id FROM TRANSACTIONS WHERE status_id = 4) "
						+ "AND users.id NOT IN (SELECT user_id FROM INVESTMENTS WHERE INVESTMENTS.IS_SETTLED = 0) "
						+ "ORDER BY TIME_CREATED DESC";
		
		return sqlQuery;
	}
	/**
	 * Gets user who can change details
	 * @param skinId 
	 * @return String
	 */
	public static String getUserWhoCanChangePersonalDetails(String skinId) {
		String sqlQuery = "SELECT * FROM users "
						+ "WHERE PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
						+ "AND FIRST_NAME LIKE '%Autotest%' " 
						+ "AND EMAIL LIKE '%@anyoption.com%' "
						+ "AND BALANCE != 0 "
						+ "AND SKIN_ID = " + skinId + " " 
						+ "AND CLASS_ID = 0 "
						+ "AND COUNTRY_ID = 33 "
						+ "AND GENDER is NULL "
						+ "AND users.IS_ACTIVE = 1 "
						+ "AND users.id NOT IN (" + returnedUsers + ") "
						+ "AND 5 < (SELECT COUNT (*) FROM users_regulation_history WHERE users.id = user_id) "
						+ "AND users.TIME_LAST_LOGIN < SYSDATE - INTERVAL '5' MINUTE "
						+ "ORDER BY TIME_CREATED DESC";
		return sqlQuery;
	}
}
