package com.anyoption.autotest.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class LandingPageTestData {
	/**
	 * Reads test data
	 * @param filePath
	 * @param sheetName
	 * @param rowSize
	 * @param cellSize
	 * @return
	 * @throws IOException
	 */
	public String[][] getDataFromExcelFile(String filePath, String sheetName, int rowSize, int cellSize) throws IOException {
		String[][] returnString = null;
		String returnStringNow[][] = null;
		try {
			FileInputStream fileInputStream = new FileInputStream(filePath);
			XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
			XSSFSheet worksheet = workbook.getSheet(sheetName);
			returnString = new String[rowSize][cellSize];
			ArrayList<String> writed = new ArrayList<>();
			int index = 0;
			for (int i = 1; i < rowSize; i++) {
				for (int j = 0; j < cellSize; j++) {
					XSSFRow row = worksheet.getRow(i);
					XSSFCell cell = row.getCell((short) j);
					String cellVal = cell.getStringCellValue();
					String[] arr = cellVal.split("\\?");
					if (i >= 2) {
						if (writed.contains(arr[0])) {
							index = index + 1;
							continue;
						} else {
							returnString[i - 1 - index][j] = cellVal;
							writed.add(arr[0]);
						}
					} else {
						returnString[i - 1][j] = cellVal;
						writed.add(arr[0]);
					}	
				}
			}	
		
			int size = returnString.length;
			int newSize = size - index - 1;
			returnStringNow = new String[newSize][cellSize];
			for (int i = 0; i < newSize; i++) {
				returnStringNow[i][cellSize - 1] = returnString[i][cellSize - 1];
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return returnStringNow;
	}
}
