package com.anyoption.autotest.utils;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class ChangeUrl {
	/**
	 * Determines test URL according to skin
	 * @param url - URL to be changed
	 * @param skin - skin for test
	 * @return
	 */
	public String changeUrl(String url, String skin) {
		String testUrl = "";
		switch (skin) {
		case "EN":
			testUrl = url;
			break;
		case "DE":
			testUrl = url.replace("www", skin.toLowerCase());
			break;
		case "ES":
			testUrl = url.replace("www", skin.toLowerCase());
			break;
		case "IT":
			testUrl = url.replace("www", "www1");
			testUrl = testUrl.replace("com", "it");
			break;
		case "FR":
			testUrl = url.replace("www", skin.toLowerCase());
			break;
		case "SE":
			testUrl = url.replace("www", skin.toLowerCase());
			break;
		case "NL":
			testUrl = url.replace("www", skin.toLowerCase());
			break;	
		}	
		return testUrl;
	}
}
