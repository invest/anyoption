package com.anyoption.autotest.utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
/**
 * 
 * @author vladimirm
 *
 */
public class ScreenShot {
	/**
	 * Generates screen shot
	 * @param driver
	 * @param name
	 * @throws IOException
	 */
	public void takeScreenShot(WebDriver driver, String name) throws IOException {
		File theDir = new File("target/screenShots");

		// if the directory does not exist, create it
		if (! theDir.exists()) {
		    System.out.println("creating directory: " + "screenShots");
		    boolean result = false;
		    try {
		    	theDir.mkdir();
		        result = true;
		    } 
		    catch(SecurityException se){
		        
		    }        
		    if (result) {    
		        System.out.println("DIR created");  
		    }
		}
        String path = "target/screenShots/" + name + ".png"; 
        
    	File fileName = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    	FileUtils.copyFile(fileName, new File(path));   
	} 
}
