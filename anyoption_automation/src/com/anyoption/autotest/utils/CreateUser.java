package com.anyoption.autotest.utils;

import com.anyoption.autotest.utils.RandomStringGenerator.Mode;
/**
 * 
 * @author vladimirm
 *
 */
public class CreateUser {
	private String firstName;
	private String lastName;
	private String eMail;
	private String phone;
	
	public CreateUser() throws Exception {
		String randomString = RandomStringGenerator.generateRandomString(6, Mode.CAPITAL_LETTERS);
		String randomNumeric = RandomStringGenerator.generateRandomString(8, Mode.NUMERIC);
		if (randomNumeric.startsWith("0")) {
			randomNumeric.replace("0", "9");
		}
		firstName = "Autotest" + randomString;
		lastName = "Family" + randomString;
		eMail = firstName + "@anyoption.com";
		phone = randomNumeric;
	}
	
	public CreateUser(String skin) throws Exception {
		String randomString = RandomStringGenerator.generateRandomString(6, Mode.CAPITAL_LETTERS);
		String randomNumeric = RandomStringGenerator.generateRandomString(8, Mode.NUMERIC);
		if (randomNumeric.startsWith("0")) {
			randomNumeric.replace("0", "9");
		}
		firstName = "Autotest" + skin + randomString;
		lastName = "Family" + randomString;
		eMail = firstName + "@anyoption.com";
		phone = randomNumeric;
	}
	/**
	 * Gets first name
	 * @return String
	 */
	public String getFirstName() {
		return this.firstName;
	}
	/**
	 * Gets last name
	 * @return String
	 */
	public String getLastName() {
		return this.lastName;
	}
	/**
	 * Gets email
	 * @return String
	 */
	public String getEMail() {
		return this.eMail;
	}
	/**
	 * Gets phone number
	 * @return String
	 */
	public String getPhone() {
		return this.phone;
	}
}
