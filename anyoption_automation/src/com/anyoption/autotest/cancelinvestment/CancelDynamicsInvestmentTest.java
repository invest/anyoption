package com.anyoption.autotest.cancelinvestment;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.DynamicsPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.MyAccountPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class CancelDynamicsInvestmentTest extends BaseTest {

	public CancelDynamicsInvestmentTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/CancelInvestmentTestData.xlsx", numberRows, 6);
	    return(retObjArr);
	}
	/**
	 * Cancels dynamics investment
	 * @param skin
	 * @param linkMess
	 * @param canceledMess
	 * @param amountMess
	 * @param noOptionMess
	 * @param myOptionStatus
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void cancelDynamicsInvestmentTest(String skin, String linkMess, String canceledMess, String amountMess, String noOptionMess, 
			String myOptionStatus) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		DynamicsPage dynamicsPage = new DynamicsPage(driver);
		MyAccountPage myAccountPage = new MyAccountPage(driver);
		String sqlQuery = UserSQL.getUserWhoCanMakeDynamicsInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		String balance = headerMenuPage.getBalance();
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		String moneyType = balance.replaceAll("\\d+.*", "");
		String email = testData.get("email");
		currentUserId = testData.get("id");
		String userBalanceInDB = testData.get("balance");
		int userBalanceInDBInt = Integer.parseInt(userBalanceInDB);
		dynamicsPage.openDynamics();
		if (dynamicsPage.isDynamicsTutorialDisplayed() == true) {
			dynamicsPage.closeTutorial();
		}
		dynamicsPage.chooseMarket();
		String asset = dynamicsPage.getCurrentMarket();
		String amount = "50";
		int amountInt = Integer.parseInt(amount);
		dynamicsPage.setAmount(amount);
		dynamicsPage.makeAboveInvestmentToCancel();
		dynamicsPage.cancelInvestment();
		Assert.assertTrue(dynamicsPage.isCancelMessageDisplayed());
		Assert.assertEquals(canceledMess, dynamicsPage.getCancelInvMessage());
		dynamicsPage.showCancelInvPopUp();
		String cancelAmount = dynamicsPage.getInvestmentAmount(moneyType);
		String message = dynamicsPage.getCancelInvAmountMessage();
		String linkMessage = dynamicsPage.getCancelLinkText();
		String level = dynamicsPage.getLevelFromCancelWindow();
		String cancelTax = message.replaceAll("[^\\d]", "");
		Assert.assertEquals(amount, cancelAmount);
		Assert.assertEquals(linkMess, linkMessage);
		Assert.assertEquals(amountMess, message);
		String balanceAfter = headerMenuPage.getBalance();
		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
		Assert.assertEquals(balanceInt - Integer.parseInt(cancelTax + "00"), balanceAfterInt);
		String balanceInDBAfter = User.getUserBalanceFromDB(email, currentUserId);
		int balanceInDBAfterInt = Integer.parseInt(balanceInDBAfter);
		Assert.assertEquals(userBalanceInDBInt - Integer.parseInt(cancelTax + "00"), balanceInDBAfterInt);
		headerMenuPage.openMyAccountMenu();
  		Assert.assertEquals(noOptionMess, myAccountPage.getNoOptionsMessage());
  		myAccountPage.clickClosedInvestmentRadioButton();
  		myAccountPage.clickSubmitButton();
  		String settledOptionId = myAccountPage.getSettledOptionInvestmentId();
  		String marketNameFromMyOption = myAccountPage.getSettledOptionAsset();
  		String levelFromMyOptions = myAccountPage.getSettledOptionLevel();
  		String amountFromMyOptions = myAccountPage.getSettledOptionAmount();
  		String returnValueFromMyOptions = myAccountPage.getSettledOptionReturnValue();
  		int returnAmValue = amountInt - Integer.parseInt(cancelTax);
  		Assert.assertEquals(asset, marketNameFromMyOption);
  		if (! level.replace("0", "").equals(levelFromMyOptions.replace("0", ""))) {
  			Assert.assertEquals(level.replace("0", "").replace(".", ""), levelFromMyOptions.replace("0", "").replace(".", ""));
  		}	
  		Assert.assertEquals(moneyType + amount + ".00", amountFromMyOptions);
  		Assert.assertEquals(moneyType + returnAmValue + ".00", returnValueFromMyOptions);
  		Assert.assertEquals(myOptionStatus, myAccountPage.getSettledOptionExpiryLevel());
  		User.verifyDynamicsCanceledInvestment(settledOptionId, currentUserId, amount + "00", cancelTax + "00");
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
