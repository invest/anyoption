package com.anyoption.autotest.cancelinvestment;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.InvestmentPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.MyAccountPage;
import com.anyoption.autotest.pages.OptionPlusPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class CancelOptionPlusInvestmentTest extends BaseTest {

	public CancelOptionPlusInvestmentTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/CancelInvestmentTestData.xlsx", numberRows, 6);
	    return(retObjArr);
	}
	/**
	 * Cancel option plus investment
	 * @param skin
	 * @param cancelLink
	 * @param cancelMessage
	 * @param amountMessage
	 * @param noOptionMess
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void cancelOptionPlusInvestmentTest(String skin, String cancelLink, String cancelMessage, String amountMessage,
			String noOptionMess, String myOptionStatus) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		String sqlQuery = UserSQL.getUserWhoCanMakeInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		InvestmentPage investmentPage = new InvestmentPage(driver);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		MyAccountPage myAccountPage = new MyAccountPage(driver);
		OptionPlusPage optionPlusPage = new OptionPlusPage(driver);
		String balance = headerMenuPage.getBalance();
		String moneyType = balance.replaceAll("\\d+.*", "");
		String email = testData.get("email");
  		currentUserId = testData.get("id");
  		String userBalanceInDB = testData.get("balance");
  		int userBalanceInDBInt = Integer.parseInt(userBalanceInDB);
  		optionPlusPage.openOptionPlusMenu(browserType);
  		optionPlusPage.setActiveMarket();
  		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
  		String amount = "50";
  		String optionPlusAmount = "51";
  		String marketName = optionPlusPage.getCurrentAsset();
  		optionPlusPage.setCustomAmountToInvest(amount);
//  		for (int i = 0; i < 6; i++) {
//  			investmentPage.clickUpButton(0);
//  			if (investmentPage.isCancelInvestmentDisplayed(0) == true) {
//  				break;
//  			}
//  		}
  		investmentPage.makeInvestmentToCancelWithRecursion(0);
  		Assert.assertTrue(investmentPage.isCancelInvestmentDisplayed(0));
  		investmentPage.cancelInvestment(0);
  		Assert.assertTrue(investmentPage.isCanceledMessageDisplayed(0));
  		Assert.assertEquals(cancelMessage, investmentPage.getCanceledMessage(0));
  		
  		investmentPage.showCancelInvestmentPopUp(0);
  		String amountCancel = investmentPage.getAmountFromCancelInv(0);
  		String secs = investmentPage.getTimeToCancel(0);
  		String level = investmentPage.getLevelFromCancelInv(0);
  		String message = investmentPage.getMessageFromCancelInv(0);
  		String returnAmount = message.replaceAll("[^\\d]", "");
  		String cancelLinkText = investmentPage.getCancelLinkText(0);
  		
  		if (2 != Integer.parseInt(secs)) {
  			Assert.assertTrue(2 > Integer.parseInt(secs));
  		}
  		Assert.assertEquals(moneyType + optionPlusAmount + ".00", amountCancel);
  		Assert.assertEquals(amountMessage, message);
  		Assert.assertEquals(cancelLink, cancelLinkText);
  		int amountInt = Integer.parseInt(amount);
  		int optionPlusAmountInt = Integer.parseInt(optionPlusAmount);
  		int returnAmountInt = Integer.parseInt(returnAmount);
  		Assert.assertEquals((int)(amountInt * 0.1), returnAmountInt);
  		String balanceAfter = headerMenuPage.getBalance();
  		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
  		Assert.assertEquals(balanceAfterInt, balanceInt - Integer.parseInt(returnAmount + "00"));
  		String userBalanceInDBAfter = User.getUserBalanceFromDB(email, currentUserId);
  		int userBalanceInDBAfterInt = Integer.parseInt(userBalanceInDBAfter);
  		Assert.assertEquals(userBalanceInDBAfterInt, userBalanceInDBInt - Integer.parseInt(returnAmount + "00"));
  		headerMenuPage.openMyAccountMenu();
  		Assert.assertEquals(noOptionMess, myAccountPage.getNoOptionsMessage());
  		myAccountPage.clickClosedInvestmentRadioButton();
  		myAccountPage.clickSubmitButton();
  		String settledOptionId = myAccountPage.getSettledOptionInvestmentId();
  		String marketNameFromMyOption = myAccountPage.getSettledOptionAsset();
  		String levelFromMyOptions = myAccountPage.getSettledOptionLevel();
  		String amountFromMyOptions = myAccountPage.getSettledOptionAmount();
  		String returnValueFromMyOptions = myAccountPage.getSettledOptionReturnValue();
  		int returnAmValue = optionPlusAmountInt - returnAmountInt;
  		Assert.assertEquals(marketName, marketNameFromMyOption);
  		if (! level.replace("0", "").equals(levelFromMyOptions.replace("0", ""))) {
  			Assert.assertEquals(level.replace("0", "").replace(".", ""), levelFromMyOptions.replace("0", "").replace(".", ""));
  		}	
  		Assert.assertEquals(moneyType + optionPlusAmount + ".00", amountFromMyOptions);
  		Assert.assertEquals(moneyType + returnAmValue + ".00", returnValueFromMyOptions);
  		Assert.assertEquals(myOptionStatus, myAccountPage.getSettledOptionExpiryLevel());
  		User.verifyCanceledInvestment(settledOptionId, currentUserId, "1", optionPlusAmount + "00", returnAmount + "00", levelFromMyOptions);
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
