package com.anyoption.autotest.cancelinvestment;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.InvestmentPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.MyAccountPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class CancelBinaryDev2OptionTest extends BaseTest {

	public CancelBinaryDev2OptionTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/CancelInvestmentTestData.xlsx", numberRows, 6);
	    return(retObjArr);
	}
	/**
	 * Cancels dev2 binary option investment
	 * @param skin
	 * @param cancelLink
	 * @param cancelMessage
	 * @param amountMessage
	 * @param noOptionMess
	 * @param myOptionStatus
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void cancelBinaryDev2OptionTest(String skin, String cancelLink, String cancelMessage, String amountMessage, String noOptionMess, 
			String myOptionStatus) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		String sqlQuery = UserSQL.getUserWhoCanMakeDeviationCheckInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		InvestmentPage investmentPage = new InvestmentPage(driver);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		MyAccountPage myAccountPage = new MyAccountPage(driver);
		String balance = headerMenuPage.getBalance();
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		String moneyType = balance.replaceAll("\\d+.*", "");
		currentUserId = testData.get("id");
		String eMail = testData.get("email");
		String userBalanceInDB = testData.get("balance");
		int userBalanceInDBInt = Integer.parseInt(userBalanceInDB);
  		int number = investmentPage.getAcctiveInvestment();
  		if (number == -1) {
  			investmentPage.changeMarket(0);
  			number = 0;
  		}
  		String currentMarket = investmentPage.getMarketName(number);
  		int balanceToInv = userBalanceInDBInt / 100;
  		HashMap<String, String> dataToInvest = new HashMap<>();
  		for (int i = 0; i < 10; i++) {
  			dataToInvest = investmentPage.getDataForDev2Check(number);
  			boolean isMarketActive = investmentPage.isMarketActive(number);
  			if (dataToInvest.isEmpty() || (balanceToInv < Integer.parseInt(dataToInvest.get("amount"))) || isMarketActive == false) {
  				investmentPage.chooseActiveMarket(number, currentMarket);
  			} else {
  				System.out.println(dataToInvest);
  				break;
  			}
  		}
  		String marketName = investmentPage.getMarketName(number);
  		String amount = dataToInvest.get("amount");
  		investmentPage.setAmountToInvest(number, amount);
  		for (int i = 0; i < 6; i++) {
  			investmentPage.clickUpButton(number);
  			if (investmentPage.isCancelInvestmentDisplayed(number) == true) {
  				break;
  			}
  		}
  		Assert.assertTrue(investmentPage.isCancelInvestmentDisplayed(number));
  		investmentPage.cancelInvestment(number);
  		Assert.assertTrue(investmentPage.isCanceledMessageDisplayed(number));
  		Assert.assertEquals(cancelMessage, investmentPage.getCanceledMessage(number));
  		
  		investmentPage.showCancelInvestmentPopUp(number);
  		String secs = investmentPage.getTimeToCancel(number);
  		String level = investmentPage.getLevelFromCancelInv(number);
  		String amountCancel = investmentPage.getAmountFromCancelInv(number);
  		String message = investmentPage.getMessageFromCancelInv(number);
  		String cancelLinkText = investmentPage.getCancelLinkText(number);
  		
  		if (Integer.parseInt(dataToInvest.get("secs")) - 1 != Integer.parseInt(secs)) {
  			if (Integer.parseInt(dataToInvest.get("secs")) - 2 != Integer.parseInt(secs)) { 
  				Assert.assertTrue(Integer.parseInt(dataToInvest.get("secs")) - 2 > Integer.parseInt(secs));;
  			}	
  		}
  		Assert.assertEquals(cancelLink, cancelLinkText);
  		Assert.assertEquals(moneyType + amount + ".00".replace(",", ""), amountCancel.replace(",", ""));
  		int amountInt = Integer.parseInt(amount);
  		String loseAm;
  		String am = String.valueOf(amountInt * 0.1);
  		if (am.contains(".0")) {
  			am = am.replace(".0", "");
  			loseAm = am + "00";
  		} else {
  			am = am + "0";
  			loseAm = am.replace(".", "");
  		}
  		amountMessage = amountMessage.replace("5", am);
  		Assert.assertEquals(amountMessage, message);
  		Assert.assertEquals((amountInt * 0.1), Double.parseDouble(am));
  		String balanceAfter = headerMenuPage.getBalance();
  		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
  		Assert.assertEquals(balanceAfterInt, balanceInt - Integer.parseInt(loseAm));
  		String userBalanceInDBAfter = User.getUserBalanceFromDB(eMail, currentUserId);
  		int userBalanceInDBAfterInt = Integer.parseInt(userBalanceInDBAfter);
  		Assert.assertEquals(userBalanceInDBAfterInt, userBalanceInDBInt - Integer.parseInt(loseAm));
  		headerMenuPage.openMyAccountMenu();
  		Assert.assertEquals(noOptionMess, myAccountPage.getNoOptionsMessage());
  		myAccountPage.clickClosedInvestmentRadioButton();
  		myAccountPage.clickSubmitButton();
  		String settledOptionId = myAccountPage.getSettledOptionInvestmentId();
  		String marketNameFromMyOption = myAccountPage.getSettledOptionAsset();
  		String levelFromMyOptions = myAccountPage.getSettledOptionLevel();
  		String amountFromMyOptions = myAccountPage.getSettledOptionAmount();
  		String returnValueFromMyOptions = myAccountPage.getSettledOptionReturnValue();
  		double returnAmValue = amountInt - Double.parseDouble(am);
  		Assert.assertEquals(marketName, marketNameFromMyOption);
  		if (! level.replace("0", "").equals(levelFromMyOptions.replace("0", ""))) {
//  			Assert.assertEquals(level.replace("0", "").replace(".", ""), levelFromMyOptions.replace("0", "").replace(".", ""));
  			System.out.println("Level change");
  		}	
  		Assert.assertEquals(moneyType + amount + ".00", amountFromMyOptions.replace(",", ""));
  		Assert.assertEquals(moneyType + returnAmValue + "0", returnValueFromMyOptions);
  		Assert.assertEquals(myOptionStatus, myAccountPage.getSettledOptionExpiryLevel());
  		User.verifyCanceledInvestment(settledOptionId, currentUserId, "1", amount + "00", loseAm, levelFromMyOptions);
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
