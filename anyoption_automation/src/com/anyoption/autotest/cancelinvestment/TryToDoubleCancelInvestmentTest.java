package com.anyoption.autotest.cancelinvestment;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.BubblesPage;
import com.anyoption.autotest.pages.DynamicsPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.InvestmentPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.LongTermPage;
import com.anyoption.autotest.pages.OptionPlusPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class TryToDoubleCancelInvestmentTest extends BaseTest {

	public TryToDoubleCancelInvestmentTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/DoubleClickCancelInvTestData.xlsx", numberRows, 1);
	    return(retObjArr);
	}
	/**
	 * Double clicks on cancel investment button
	 * @param skin
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void tryToDoubleCancelInvestmentTest(String skin) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		InvestmentPage investmentPage = new InvestmentPage(driver);
		String sqlQuery = UserSQL.getUserWhoCanMakeDynamicsInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		OptionPlusPage optionPlusPage = new OptionPlusPage(driver);
		LongTermPage longTermPage = new LongTermPage(driver);
		BubblesPage bubblesPage = new BubblesPage(driver);
		DynamicsPage dynamicsPage = new DynamicsPage(driver);
		int balance = Integer.parseInt(headerMenuPage.getBalance().replaceAll("[^\\d]", ""));
		String email = testData.get("email");
		currentUserId = testData.get("id");
		int balanceInDB = Integer.parseInt(testData.get("balance"));
		int number = investmentPage.getAcctiveInvestment();
  		if (number == -1) {
  			investmentPage.changeMarket(0);
  			number = 0;
  		}
  		String amount = "50";
  		int amountInt = Integer.parseInt(amount + "00"); 
  		investmentPage.setAmountToInvest(number, amount);
  		investmentPage.makeInvestmentToCancelWithRecursion(number);
  		Assert.assertTrue(investmentPage.isCancelInvestmentDisplayed(number));
  		investmentPage.doubleClickCancelInvestment(number);
  		Assert.assertTrue(investmentPage.isCanceledMessageDisplayed(number));
  		double loseValue = amountInt * 0.1;
  		int balanceAfter = Integer.parseInt(headerMenuPage.getBalance().replaceAll("[^\\d]", ""));
  		int balanceInDBAfter = Integer.parseInt(User.getUserBalanceFromDB(email, currentUserId));
  		Assert.assertEquals(balance - (int)loseValue, balanceAfter);
  		Assert.assertEquals(balanceInDB - (int)loseValue, balanceInDBAfter);
  		
  		balance = balanceAfter;
  		balanceInDB = balanceInDBAfter;
  		optionPlusPage.openOptionPlusMenu(browserType);
  		optionPlusPage.setActiveMarket();
  		optionPlusPage.setCustomAmountToInvest(amount);
  		investmentPage.makeInvestmentToCancelWithRecursion(0);
  		Assert.assertTrue(investmentPage.isCancelInvestmentDisplayed(0));
  		investmentPage.doubleClickCancelInvestment(0);
  		Assert.assertTrue(investmentPage.isCanceledMessageDisplayed(0));
  		balanceAfter = Integer.parseInt(headerMenuPage.getBalance().replaceAll("[^\\d]", ""));
  		balanceInDBAfter = Integer.parseInt(User.getUserBalanceFromDB(email, currentUserId));
  		Assert.assertEquals(balance - (int)loseValue, balanceAfter);
  		Assert.assertEquals(balanceInDB - (int)loseValue, balanceInDBAfter);
  		
  		balance = balanceAfter;
  		balanceInDB = balanceInDBAfter;
  		longTermPage.openLongTerm();
  		longTermPage.setMarket(1);
  		String id = longTermPage.getOptionId(0);
  		longTermPage.selectUnitsCount(id, "1");
  		String price = longTermPage.getPrice(id).replace(" ", "");
  		amountInt = Integer.parseInt(price.replaceAll("[^\\d]", "") + "00");
  		loseValue = amountInt * 0.1;
  		longTermPage.buyLongOption(id);
  		number = -2;// Long term investment param
  		Assert.assertTrue(investmentPage.isCancelInvestmentDisplayed(number));
  		investmentPage.doubleClickCancelInvestment(number);
  		balanceAfter = Integer.parseInt(headerMenuPage.getBalance().replaceAll("[^\\d]", ""));
  		balanceInDBAfter = Integer.parseInt(User.getUserBalanceFromDB(email, currentUserId));
  		Assert.assertEquals(balance - (int)loseValue, balanceAfter);
  		Assert.assertEquals(balanceInDB - (int)loseValue, balanceInDBAfter);
  		
  		balance = balanceAfter;
  		balanceInDB = balanceInDBAfter;
  		longTermPage.openBubblesMenu();
  		bubblesPage.switchDriverToBubbles();
		bubblesPage.passTutorial();
		bubblesPage.expandBubble();
		int profit = bubblesPage.getProfite();
		if (profit == 0) {
			profit = bubblesPage.moveBubble();
		}
		bubblesPage.setCustomAmount(amount);
		amountInt = Integer.parseInt(amount + "00"); 
		loseValue = amountInt * 0.1;
		bubblesPage.clickGoButton();
		UtilsMethods.sleep(1200);
		Assert.assertTrue(bubblesPage.isCancelNotificationDisplayed());
		bubblesPage.doubleClickCancelLink();
		driver.switchTo().defaultContent();
		balanceAfter = Integer.parseInt(headerMenuPage.getBalance().replaceAll("[^\\d]", ""));
  		balanceInDBAfter = Integer.parseInt(User.getUserBalanceFromDB(email, currentUserId));
  		Assert.assertEquals(balance - (int)loseValue, balanceAfter);
  		Assert.assertEquals(balanceInDB - (int)loseValue, balanceInDBAfter);
  		
  		balance = balanceAfter;
  		balanceInDB = balanceInDBAfter;
  		dynamicsPage.openDynamics();
  		if (dynamicsPage.isDynamicsTutorialDisplayed() == true) {
			dynamicsPage.closeTutorial();
		}
		dynamicsPage.chooseMarket();
		dynamicsPage.setAmount(amount);
		dynamicsPage.makeAboveInvestmentToCancel();
		dynamicsPage.doubleClickCancelInvLink();
		balanceAfter = Integer.parseInt(headerMenuPage.getBalance().replaceAll("[^\\d]", ""));
  		balanceInDBAfter = Integer.parseInt(User.getUserBalanceFromDB(email, currentUserId));
  		Assert.assertEquals(balance - (int)loseValue, balanceAfter);
  		Assert.assertEquals(balanceInDB - (int)loseValue, balanceInDBAfter);
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
