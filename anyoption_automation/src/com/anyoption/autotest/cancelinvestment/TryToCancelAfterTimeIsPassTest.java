package com.anyoption.autotest.cancelinvestment;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.InvestmentPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.MyAccountPage;
import com.anyoption.autotest.pages.OptionPlusPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class TryToCancelAfterTimeIsPassTest extends BaseTest {

	public TryToCancelAfterTimeIsPassTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/BinaryOptionInvestTestData.xlsx", numberRows, 1);
	    return(retObjArr);
	}
	/**
	 * Tries to cancel investment when time is pass
	 * @param skin
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void tryToCancelAfterTimeIsPassTest(String skin) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		String sqlQuery = UserSQL.getUserWhoCanMakeInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = testData.get("id");
		InvestmentPage investmentPage = new InvestmentPage(driver);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		MyAccountPage myAccountPage = new MyAccountPage(driver);
		OptionPlusPage optionPlusPage = new OptionPlusPage(driver);
		String balance = headerMenuPage.getBalance();
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		String eMail = testData.get("email");
		String userBalanceInDB = testData.get("balance");
		int userBalanceInDBInt = Integer.parseInt(userBalanceInDB);
  		int number = investmentPage.getAcctiveInvestment();
  		if (number == -1) {
  			investmentPage.changeMarket(0);
  			number = 0;
  		}
  		String amount = "50";
  		int amountInt = Integer.parseInt(amount + "00"); 
  		investmentPage.setAmountToInvest(number, amount);
//  		for (int i = 0; i < 8; i++) {
//  			investmentPage.clickUpButton(number);
//  			if (investmentPage.isCancelInvestmentDisplayed(number) == true) {
//  				break;
//  			}
//  		}
  		investmentPage.makeInvestmentToCancelWithRecursion(number);
  		Assert.assertTrue(investmentPage.isCancelInvestmentDisplayed(number));
  		UtilsMethods.sleep(6000);
  		investmentPage.showCancelInvestmentPopUp(number);
  		investmentPage.cancelInvestment(number);
  		investmentPage.hideCancelInvestmentPopUp(number);
  		if (! browserType.equalsIgnoreCase("firefox")) {
  			Assert.assertTrue(investmentPage.isCanceledMessageDisplayed(number));
  		}	
  		String balanceAfter = headerMenuPage.getBalance();
  		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
  		String userBalanceInDBAfter = User.getUserBalanceFromDB(eMail, currentUserId);
  		int userBalanceInDBAfterInt = Integer.parseInt(userBalanceInDBAfter);
  		Assert.assertEquals(balanceInt - amountInt, balanceAfterInt);
  		Assert.assertEquals(userBalanceInDBInt - amountInt, userBalanceInDBAfterInt);
  		balanceInt = balanceAfterInt;
  		userBalanceInDBInt = userBalanceInDBAfterInt;
  		amountInt = amountInt + 100;
  		number = 0;
  		optionPlusPage.openOptionPlusMenu(browserType);
  		optionPlusPage.setActiveMarket();
  		optionPlusPage.setCustomAmountToInvest(amount);
//  		for (int i = 0; i < 10; i++) {
//  			investmentPage.clickUpButton(number);
//  			if (investmentPage.isCancelInvestmentDisplayed(number) == true) {
//  				break;
//  			}
//  		}
  		investmentPage.makeInvestmentToCancelWithRecursion(number);
  		Assert.assertTrue(investmentPage.isCancelInvestmentDisplayed(number));
  		UtilsMethods.sleep(8000);
  		investmentPage.showCancelInvestmentPopUp(number);
  		investmentPage.cancelInvestment(number);
  		investmentPage.hideCancelInvestmentPopUp(number);
  		if (! browserType.equalsIgnoreCase("firefox")) {
  			Assert.assertTrue(investmentPage.isCanceledMessageDisplayed(number));
  		}	
  		balanceAfter = headerMenuPage.getBalance();
  		balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
  		userBalanceInDBAfter = User.getUserBalanceFromDB(eMail, currentUserId);
  		userBalanceInDBAfterInt = Integer.parseInt(userBalanceInDBAfter);
  		Assert.assertEquals(balanceInt - amountInt, balanceAfterInt);
  		Assert.assertEquals(userBalanceInDBInt - amountInt, userBalanceInDBAfterInt);
  		headerMenuPage.openMyAccountMenu();
  		Assert.assertEquals(2, myAccountPage.getCountActiveInvestments());
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
