package com.anyoption.autotest.cancelinvestment;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.BubblesPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.MyAccountPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class CancelBubblesInvestmentTest extends BaseTest {

	public CancelBubblesInvestmentTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/CancelInvestmentTestData.xlsx", numberRows, 6);
	    return(retObjArr);
	}
	/**
	 * Cancels bubbles investment
	 * @param skin
	 * @param cancelLink
	 * @param cancelMessage
	 * @param amountMessage
	 * @param noOptionMess
	 * @param myOptionStatus
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void cancelBubblesInvestmentTest(String skin, String cancelLink, String cancelMessage, String amountMessage, String noOptionMess, 
			String myOptionStatus) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		String sqlQuery = UserSQL.getUserWhoCanMakeInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		BubblesPage bubblesPage = new BubblesPage(driver);
		MyAccountPage myAccountPage = new MyAccountPage(driver);
		String balance = headerMenuPage.getBalance();
		String moneyType = balance.replaceAll("\\d+.*", "");
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		String email = testData.get("email");
		currentUserId = testData.get("id");
		int userBalanceInDbInt = Integer.parseInt(testData.get("balance"));
		bubblesPage.openBubbles();
		bubblesPage.switchDriverToBubbles();
		bubblesPage.passTutorial();
		bubblesPage.expandBubble();
		int profit = bubblesPage.getProfite();
		if (profit == 0) {
			profit = bubblesPage.moveBubble();
		}
		int amount = bubblesPage.setCustomAmount("50");
		String market = bubblesPage.getAssetName();
		bubblesPage.clickGoButton();
		UtilsMethods.sleep(1200);
		Assert.assertTrue(bubblesPage.isCancelNotificationDisplayed());
		String message = bubblesPage.getCancelMessage();
		System.out.println(message);
		String secs = bubblesPage.getTimeToCancel();
		System.out.println(secs);
		String cancelLinkText = bubblesPage.getCancelLinkText();
		System.out.println(cancelLinkText);
		bubblesPage.clickCancelLink();
		Assert.assertEquals(cancelMessage, bubblesPage.getPositiveNotification().replace(".", ""));
  		if (6 != Integer.parseInt(secs)) {
  			Assert.assertTrue(6 > Integer.parseInt(secs));
  		}	
		Assert.assertEquals(cancelLink.toLowerCase(), cancelLinkText.toLowerCase());
		Assert.assertEquals(amountMessage, message.replace(".0", "").replace(".", ""));
		String loseAmount = message.replaceAll("[^\\d]", "").replace("0", "");
		int loseAmountInt = Integer.parseInt(loseAmount);
		Assert.assertEquals((int)(amount * 0.1), loseAmountInt);
		driver.switchTo().defaultContent();
		String balanceAfter = headerMenuPage.getBalance();
  		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
  		Assert.assertEquals(balanceAfterInt, balanceInt - Integer.parseInt(loseAmount + "00"));
  		String userBalanceInDBAfter = User.getUserBalanceFromDB(email, currentUserId);
  		int userBalanceInDBAfterInt = Integer.parseInt(userBalanceInDBAfter);
  		Assert.assertEquals(userBalanceInDBAfterInt, userBalanceInDbInt - Integer.parseInt(loseAmount + "00"));
  		headerMenuPage.openMyAccountMenu();
		myAccountPage.selectOptionsType("1");
		Assert.assertEquals(noOptionMess, myAccountPage.getNoOptionsMessage());
		myAccountPage.clickClosedInvestmentRadioButton();
		myAccountPage.clickSubmitButton();
		String optionId = myAccountPage.getBubblesOptionId();
		String asset = myAccountPage.getBubblesMarketName();
		String expiryTime = myAccountPage.getBubblesExpiryTime();
		System.out.println(currentUserId + " my option expirity time " + expiryTime);
		String amountFromMyOptions = myAccountPage.getBubblesInvestedAmount();
		String retAmountFromMyOptions = myAccountPage.getBubblesReturnAmount(moneyType);
		Assert.assertEquals(market, asset);
		Assert.assertEquals(moneyType + amount + ".00", amountFromMyOptions);
		Assert.assertEquals(moneyType + (amount - loseAmountInt) + "00", retAmountFromMyOptions.replace(".", ""));
		User.verifyBubblesCanceledInvestment(optionId, currentUserId, amount + "00", loseAmount + "00");
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		driver.switchTo().defaultContent();
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
