package com.anyoption.autotest.cancelinvestment;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.InvestmentPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.MyAccountPage;
import com.anyoption.autotest.pages.OptionPlusPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class CancelOptionPlusDev2InvestmentTest extends BaseTest {

	public CancelOptionPlusDev2InvestmentTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/CancelInvestmentTestData.xlsx", numberRows, 6);
	    return(retObjArr);
	}
	/**
	 * Cancels option plus dev2 investment
	 * @param skin
	 * @param cancelLink
	 * @param cancelMessage
	 * @param amountMessage
	 * @param noOptionMess
	 * @param myOptionStatus
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void cancelOptionPlusDev2InvestmentTest(String skin, String cancelLink, String cancelMessage, String amountMessage, String noOptionMess, 
			String myOptionStatus) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		String sqlQuery = UserSQL.getUserWhoCanMakeDeviationCheckInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		InvestmentPage investmentPage = new InvestmentPage(driver);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		OptionPlusPage optionPlusPage = new OptionPlusPage(driver);
		MyAccountPage myAccountPage = new MyAccountPage(driver);
		String balance = headerMenuPage.getBalance();
		String moneyType = balance.replaceAll("\\d+.*", "");
		String email = testData.get("email");
  		currentUserId = testData.get("id");
  		String userBalanceInDB = testData.get("balance");
  		int userBalanceInDBInt = Integer.parseInt(userBalanceInDB);
  		optionPlusPage.openOptionPlusMenu(browserType);
  		optionPlusPage.setActiveMarket();
  		String profitBalanceBefore = optionPlusPage.getProfitBalance();
  		Assert.assertEquals(balance, profitBalanceBefore);
  		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
  		HashMap<String, String> dev2Data = new HashMap<>();
  		for (int i = 0; i < 10; i++) {
  			dev2Data = optionPlusPage.getOptionPlusDeviation2Data();
  			boolean isMarketActive = optionPlusPage.isMarketActive();
  			if (dev2Data.isEmpty() || (userBalanceInDBInt / 100 <= Integer.parseInt(dev2Data.get("amount"))) || isMarketActive == false) {
  				optionPlusPage.changeMarket(i);
  			} else {
  				System.out.println(dev2Data);
  				break;
  			}
  		}
  		String amount = dev2Data.get("amount");
  		optionPlusPage.setCustomAmountToInvest(amount);
  		int optionPlusAmount = Integer.parseInt(amount) + 1;
  		String marketName = optionPlusPage.getCurrentAsset();
//  		for (int i = 0; i < 8; i++) {
//  			investmentPage.clickUpButton(0);
//  			if (investmentPage.isCancelInvestmentDisplayed(0) == true) {
//  				break;
//  			}
//  		}
  		investmentPage.makeInvestmentToCancelWithRecursion(0);
  		Assert.assertTrue(investmentPage.isCancelInvestmentDisplayed(0));
  		investmentPage.cancelInvestment(0);
  		Assert.assertTrue(investmentPage.isCanceledMessageDisplayed(0));
  		Assert.assertEquals(cancelMessage, investmentPage.getCanceledMessage(0));
  		
  		investmentPage.showCancelInvestmentPopUp(0);
  		String amountCancel = investmentPage.getAmountFromCancelInv(0);
  		String secs = investmentPage.getTimeToCancel(0);
  		String level = investmentPage.getLevelFromCancelInv(0);
  		String message = investmentPage.getMessageFromCancelInv(0);
  		String cancelLinkText = investmentPage.getCancelLinkText(0);
  		
  		if (Integer.parseInt(dev2Data.get("secs")) != Integer.parseInt(secs)) {
  			Assert.assertTrue(Integer.parseInt(dev2Data.get("secs")) > Integer.parseInt(secs));
  		}
  		Assert.assertEquals(moneyType + optionPlusAmount + ".00", amountCancel.replace(",", ""));
  		
  		String loseAm;
  		int amountInt = Integer.parseInt(amount);
  		String am = String.valueOf(amountInt * 0.1);
  		if (am.contains(".0")) {
  			am = am.replace(".0", "");
  			loseAm = am + "00";
  		} else {
  			am = am + "0";
  			loseAm = am.replace(".", "");
  		}
  		amountMessage = amountMessage.replace("5", am);
  		
  		Assert.assertEquals(amountMessage, message);
  		Assert.assertEquals(cancelLink, cancelLinkText);
  		Assert.assertEquals(amountInt * 0.1, Double.parseDouble(am));
  		String balanceAfter = headerMenuPage.getBalance();
  		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
  		Assert.assertEquals(balanceAfterInt, balanceInt - Integer.parseInt(loseAm));
  		String userBalanceInDBAfter = User.getUserBalanceFromDB(email, currentUserId);
  		int userBalanceInDBAfterInt = Integer.parseInt(userBalanceInDBAfter);
  		Assert.assertEquals(userBalanceInDBAfterInt, userBalanceInDBInt - Integer.parseInt(loseAm));
  		headerMenuPage.openMyAccountMenu();
  		Assert.assertEquals(noOptionMess, myAccountPage.getNoOptionsMessage());
  		myAccountPage.clickClosedInvestmentRadioButton();
  		myAccountPage.clickSubmitButton();
  		String settledOptionId = myAccountPage.getSettledOptionInvestmentId();
  		String marketNameFromMyOption = myAccountPage.getSettledOptionAsset();
  		String levelFromMyOptions = myAccountPage.getSettledOptionLevel();
  		String amountFromMyOptions = myAccountPage.getSettledOptionAmount();
  		String returnValueFromMyOptions = myAccountPage.getSettledOptionReturnValue();
  		double returnAmValue = Integer.parseInt(amount) - Double.parseDouble(am);
  		Assert.assertEquals(marketName, marketNameFromMyOption);
  		if (! level.replace("0", "").equals(levelFromMyOptions.replace("0", ""))) {
//  			Assert.assertEquals(level.replace("0", "").replace(".", ""), levelFromMyOptions.replace("0", "").replace(".", ""));
  			System.out.println("Level change");
  		}	
  		Assert.assertEquals(moneyType + amount + ".00", amountFromMyOptions);
  		Assert.assertEquals(moneyType + returnAmValue + "0", returnValueFromMyOptions);
  		Assert.assertEquals(myOptionStatus, myAccountPage.getSettledOptionExpiryLevel());
  		User.verifyCanceledInvestment(settledOptionId, currentUserId, "1", amount + "00", loseAm, levelFromMyOptions);
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
