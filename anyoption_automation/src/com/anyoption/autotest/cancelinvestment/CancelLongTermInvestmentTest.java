package com.anyoption.autotest.cancelinvestment;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.InvestmentPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.LongTermPage;
import com.anyoption.autotest.pages.MyAccountPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class CancelLongTermInvestmentTest extends BaseTest {

	public CancelLongTermInvestmentTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/CancelInvestmentTestData.xlsx", numberRows, 6);
	    return(retObjArr);
	}
	/**
	 * Cancels long term investment
	 * @param skin
	 * @param cancelLink
	 * @param cancelMessage
	 * @param amountMessage
	 * @param noOptionMess
	 * @param myOptionStatus
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void cancelLongTermInvestmentTest(String skin, String cancelLink, String cancelMessage, String amountMessage,
			String noOptionMess, String myOptionStatus) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		String sqlQuery = UserSQL.getUserWhoCanMakeInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		LongTermPage longTermPage = new LongTermPage(driver);
		InvestmentPage investmentPage = new InvestmentPage(driver);
		MyAccountPage myAccountPage = new MyAccountPage(driver);
		String balance = headerMenuPage.getBalance();
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		String moneyType = balance.replaceAll("\\d+.*", "");
		String email = testData.get("email");
		currentUserId = testData.get("id");
		String userBalanceInDB = testData.get("balance");
  		int userBalanceInDBInt = Integer.parseInt(userBalanceInDB);
  		longTermPage.openLongTerm();
  		longTermPage.setMarket(1);
  		String id = longTermPage.getOptionId(0);
  		longTermPage.selectUnitsCount(id, "1");
  		String price = longTermPage.getPrice(id).replace(" ", "");
  		String marketName = longTermPage.getMarketName(id);
  		longTermPage.buyLongOption(id);
  		int number = -2;// Long term investment param
  		Assert.assertTrue(investmentPage.isCancelInvestmentDisplayed(number));
  		String amountCancel = investmentPage.getAmountFromCancelInv(number).replace(" ", "");
  		String secs = investmentPage.getTimeToCancel(number);
  		if (2 != Integer.parseInt(secs)) {
  			Assert.assertTrue(2 > Integer.parseInt(secs));
  		}	
  		String message = investmentPage.getMessageFromCancelInv(number);
  		String returnAmount = message.replaceAll("[^\\d]", "");
  		String cancelLinkText = investmentPage.getCancelLinkText(number);
  		investmentPage.cancelInvestment(number);
  		Assert.assertEquals(cancelLink, cancelLinkText);
  		Assert.assertEquals(price, amountCancel);
  		Assert.assertEquals(amountMessage, message);
  		int amountInt = Integer.parseInt(price.replace(moneyType, ""));
  		int returnAmountInt = Integer.parseInt(returnAmount);
  		Assert.assertEquals((int)(amountInt * 0.1), returnAmountInt);
  		String balanceAfter = headerMenuPage.getBalance();
  		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
  		Assert.assertEquals(balanceAfterInt, balanceInt - Integer.parseInt(returnAmount + "00"));
  		String userBalanceInDBAfter = User.getUserBalanceFromDB(email, currentUserId);
  		int userBalanceInDBAfterInt = Integer.parseInt(userBalanceInDBAfter);
  		Assert.assertEquals(userBalanceInDBAfterInt, userBalanceInDBInt - Integer.parseInt(returnAmount + "00"));
  		headerMenuPage.openMyAccountMenu();
  		Assert.assertEquals(noOptionMess, myAccountPage.getNoOptionsMessage());
  		myAccountPage.clickClosedInvestmentRadioButton();
  		myAccountPage.clickSubmitButton();
  		String settledOptionId = myAccountPage.getSettledOptionInvestmentId();
  		String marketNameFromMyOption = myAccountPage.getSettledOptionAsset();
  		String amountFromMyOptions = myAccountPage.getSettledOptionAmount();
  		String returnValueFromMyOptions = myAccountPage.getSettledOptionReturnValue();
  		int returnAmValue = amountInt - returnAmountInt;
  		Assert.assertEquals(marketName, marketNameFromMyOption);
  		Assert.assertEquals(price + ".00", amountFromMyOptions);
  		Assert.assertEquals(moneyType + returnAmValue + ".00", returnValueFromMyOptions);
  		Assert.assertEquals(myOptionStatus, myAccountPage.getSettledOptionExpiryLevel());
  		User.verifyLongTermCanceledInvestment(settledOptionId, currentUserId, price.replace(moneyType, "") + "00", returnAmount + "00");
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
