package com.anyoption.autotest.cancelinvestment;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.InvestmentPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.MyAccountPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class CancelBinaryOptionInvestmentTest extends BaseTest {

	public CancelBinaryOptionInvestmentTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/CancelInvestmentTestData.xlsx", numberRows, 6);
	    return(retObjArr);
	}
	/**
	 * Cancel binary option investment
	 * @param skin
	 * @param cancelMessage
	 * @param amountMessage
	 * @param noOptionMess
	 * @param myOptionStatus
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void cancelBinaryOptionInvestmentTest(String skin, String cancelLink, String cancelMessage, String amountMessage, String noOptionMess, 
			String myOptionStatus) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		String skinId = UtilsMethods.getSkinId(skin);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		String sqlQuery = UserSQL.getUserWhoCanMakeInvestment(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, false);
		InvestmentPage investmentPage = new InvestmentPage(driver);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		MyAccountPage myAccountPage = new MyAccountPage(driver);
		String balance = headerMenuPage.getBalance();
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		String moneyType = balance.replaceAll("\\d+.*", "");
		currentUserId = testData.get("id");
		String eMail = testData.get("email");
		String userBalanceInDB = testData.get("balance");
		int userBalanceInDBInt = Integer.parseInt(userBalanceInDB);
  		int number = investmentPage.getAcctiveInvestment();
  		if (number == -1) {
  			investmentPage.changeMarket(0);
  			number = 0;
  		}
  		String marketName = investmentPage.getMarketName(number);
  		String amount = "50";
  		investmentPage.setAmountToInvest(number, amount);
//  		for (int i = 0; i < 10; i++) {
//  			investmentPage.clickUpButton(number);
//  			if (investmentPage.isCancelInvestmentDisplayed(number) == true) {
//  				break;
//  			}
//  		}
  		investmentPage.makeInvestmentToCancelWithRecursion(number);
  		Assert.assertTrue(investmentPage.isCancelInvestmentDisplayed(number));
  		investmentPage.cancelInvestment(number);
  		Assert.assertTrue(investmentPage.isCanceledMessageDisplayed(number));
  		Assert.assertEquals(cancelMessage, investmentPage.getCanceledMessage(number));
  		
  		investmentPage.showCancelInvestmentPopUp(number);
  		String secs = investmentPage.getTimeToCancel(number);
  		if (2 != Integer.parseInt(secs)) {
  			Assert.assertTrue(2 > Integer.parseInt(secs));
  		}	
  		String level = investmentPage.getLevelFromCancelInv(number);
  		String amountCancel = investmentPage.getAmountFromCancelInv(number);
  		String message = investmentPage.getMessageFromCancelInv(number);
  		String returnAmount = message.replaceAll("[^\\d]", "");
  		String cancelLinkText = investmentPage.getCancelLinkText(number);
  		
  		Assert.assertEquals(cancelLink, cancelLinkText);
  		Assert.assertEquals(moneyType + amount + ".00", amountCancel);
  		Assert.assertEquals(amountMessage, message);
  		int amountInt = Integer.parseInt(amount);
  		int returnAmountInt = Integer.parseInt(returnAmount);
  		Assert.assertEquals((int)(amountInt * 0.1), returnAmountInt);
  		String balanceAfter = headerMenuPage.getBalance();
  		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
  		Assert.assertEquals(balanceAfterInt, balanceInt - Integer.parseInt(returnAmount + "00"));
  		String userBalanceInDBAfter = User.getUserBalanceFromDB(eMail, currentUserId);
  		int userBalanceInDBAfterInt = Integer.parseInt(userBalanceInDBAfter);
  		Assert.assertEquals(userBalanceInDBAfterInt, userBalanceInDBInt - Integer.parseInt(returnAmount + "00"));
  		headerMenuPage.openMyAccountMenu();
  		Assert.assertEquals(noOptionMess, myAccountPage.getNoOptionsMessage());
  		myAccountPage.clickClosedInvestmentRadioButton();
  		myAccountPage.clickSubmitButton();
  		String settledOptionId = myAccountPage.getSettledOptionInvestmentId();
  		String marketNameFromMyOption = myAccountPage.getSettledOptionAsset();
  		String levelFromMyOptions = myAccountPage.getSettledOptionLevel();
  		String amountFromMyOptions = myAccountPage.getSettledOptionAmount();
  		String returnValueFromMyOptions = myAccountPage.getSettledOptionReturnValue();
  		int returnAmValue = amountInt - returnAmountInt;
  		Assert.assertEquals(marketName, marketNameFromMyOption);
  		if (! level.replace("0", "").equals(levelFromMyOptions.replace("0", ""))) {
  			Assert.assertEquals(level.replace("0", "").replace(".", ""), levelFromMyOptions.replace("0", "").replace(".", ""));
  		}	
  		Assert.assertEquals(moneyType + amount + ".00", amountFromMyOptions);
  		Assert.assertEquals(moneyType + returnAmValue + ".00", returnValueFromMyOptions);
  		Assert.assertEquals(myOptionStatus, myAccountPage.getSettledOptionExpiryLevel());
  		User.verifyCanceledInvestment(settledOptionId, currentUserId, "1", amount + "00", returnAmount + "00", levelFromMyOptions);
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
