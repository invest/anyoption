package com.anyoption.autotest.deposit;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.BankingHistoryPage;
import com.anyoption.autotest.pages.DepositPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimirm
 *
 */
public class DepositWithExistingCardTest extends BaseTest {
	public DepositWithExistingCardTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/DepositWithExistingCardTestData.xlsx", numberRows, 5);
	    return(retObjArr);
	}
	/**
	 * Makes deposit with exist card. 
	 * - Logins with user with exist card
	 * - Verifies balance in data base and in user interface.
	 * - Verifies messages
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void depositWithExistingCardTest(String skin, String succMessage, String amMessage, 
			String screenTitle, String bankHisdtoryDesc) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		BankingHistoryPage bankingHistoryPage = new BankingHistoryPage(driver);
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanMakeDeposit(skinId);
		HashMap<String, String> data = loginPage.loginWithUserFromDB(sqlQuery, false);
		String email = data.get("email");
		currentUserId = data.get("id");
		DepositPage depositPage = new DepositPage(driver, browserType);
		depositPage.openDepositMenu();
		String userBalanceInDB = data.get("balance");
		Assert.assertEquals(screenTitle, depositPage.getDepositTitle());
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		String balance = headerMenuPage.getBalance();
		String currency = balance.replaceAll("\\d+.*", "");
		String moneyType = depositPage.getMoneyType();
		Assert.assertEquals(moneyType, currency);
		balance = balance.replaceAll("\\D+", "");
		int balanceInt = Integer.parseInt(balance);
		int userBalanceInDBInt = Integer.parseInt(userBalanceInDB);
		depositPage.selectCard(1);
		depositPage.setCvvPass("123");
		String amount = "200";
		if (! skin.equals("EN")) {
			amount = "250";
		}
		depositPage.setDepositAmount(amount);
		depositPage.submitOldCard();
		Assert.assertEquals(succMessage, depositPage.getSuccessMessage());
		String balanceNow = headerMenuPage.getBalance().replaceAll("\\D+", "");
		int balanceNowInt = Integer.parseInt(balanceNow);
		String userBalanceInDBAfter = User.getUserBalanceFromDB(email, currentUserId);
		int userBalanceInDBAfterInt = Integer.parseInt(userBalanceInDBAfter);
		Assert.assertEquals(balanceInt + Integer.parseInt(amount + "00"), balanceNowInt);
		Assert.assertEquals(userBalanceInDBInt + Integer.parseInt(amount + "00"), userBalanceInDBAfterInt);
		String amountMessage = depositPage.getAmountMessage();
		Assert.assertTrue(amountMessage.contains(amount));
		Assert.assertTrue(amountMessage.contains(moneyType + amount + ".00 " + amMessage));
		String transactionId = depositPage.getTransactionId();
		User.verifyDepositTransaction(transactionId, amount);
		depositPage.continiueAfterNewDeposit();
		depositPage.openDepositMenu();
		bankingHistoryPage.openBankingHistory();
		Assert.assertEquals(transactionId, bankingHistoryPage.getTransactionId(0));
		Assert.assertEquals(bankHisdtoryDesc, bankingHistoryPage.getDescription(0));
		Assert.assertEquals(moneyType + amount + ".00", bankingHistoryPage.getCredit(0));
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
