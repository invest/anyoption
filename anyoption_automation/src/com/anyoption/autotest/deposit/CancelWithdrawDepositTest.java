package com.anyoption.autotest.deposit;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.DepositPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class CancelWithdrawDepositTest extends BaseTest {

	public CancelWithdrawDepositTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/CancelWithdrawTestData.xlsx", numberRows, 2);
	    return(retObjArr);
	}
	/**
	 * Cancels withdraw deposit
	 * @param skin
	 * @param cancelMessage
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void cancelWithdrawDepositTest(String skin, String cancelMessage) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanWithdrawDeposit(skinId);
		HashMap<String, String> userData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = userData.get("id");
		DepositPage depositPage = new DepositPage(driver, browserType);
		depositPage.openDepositMenu();
		depositPage.openWithdrawMenu();
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		String balance = headerMenuPage.getBalance();
		String balanceInDb = User.getUserBalanceFromDB(userData.get("email"), currentUserId);
		depositPage.selectCardToWithdraw(1);
		String amount = "200";
		depositPage.setAmountToWithdraw(amount);
		depositPage.clickWithdrawButton();
		if (depositPage.isResetBonusPopUpDisplayed() == true) {
			depositPage.confirmResetBonus();
		}
		depositPage.skipWithdrawPopup();
		depositPage.closeWithdrawPopUpToCancel();
		Assert.assertEquals(cancelMessage, depositPage.getWithdrawPopUpText());
		depositPage.closeWithdrawAcceptedText();
		String balanceAfter = headerMenuPage.getBalance();
		String balanceInDbAfter = User.getUserBalanceFromDB(userData.get("email"), currentUserId);
		Assert.assertEquals(balance, balanceAfter);
		Assert.assertEquals(balanceInDb, balanceInDbAfter);
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
