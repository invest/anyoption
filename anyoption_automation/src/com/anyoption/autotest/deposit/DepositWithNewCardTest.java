package com.anyoption.autotest.deposit;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.BankingHistoryPage;
import com.anyoption.autotest.pages.DepositPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimirm
 *
 */
public class DepositWithNewCardTest extends BaseTest {
	public DepositWithNewCardTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/DepositWithNewCardTestData.xlsx", numberRows, 4);
	    return(retObjArr);
	}
	/**
	 * Deposits with new card
	 * 	- logins with user fetched from data base
	 * 	- makes the user tester
	 *  - makes the credit card allowed
	 *  - verifies deposit amount in data base and in user interface 
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
  	public void depositWithNewCardTest(String skin, String succMessage, String amMessage, String bankHistoryDescription) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoIsCreated(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, true);
  		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
  		BankingHistoryPage bankingHistoryPage = new BankingHistoryPage(driver);
  		String balance = headerMenuPage.getBalance();
  		Assert.assertTrue(balance.contains("0.00"));
  		String moneyType = balance.replace("0.00", "");
  		String email = testData.get("email");
  		currentUserId = testData.get("id");
  		String userBalanceInDB = testData.get("balance");
  		Assert.assertEquals("0", userBalanceInDB);
		DepositPage depositPage = new DepositPage(driver, browserType);
		String amount = depositPage.getDefaultAmount();
		String selectedCurrency = depositPage.getSelectedCurrencySign();
		depositPage.setCardNumber("4200000000000000");
		depositPage.selectMonthOfExpiry(11);
		depositPage.selectYearOfExpiry(1);
		depositPage.setCardHolderName("test");
		depositPage.setCVV("123");
		depositPage.setStreet("StreetTest");
		depositPage.setCity("testCity");
		depositPage.setPostCode("1234");
		depositPage.selectDayOfBirth("19");
		depositPage.selectMonthOfBirth("06");
		depositPage.selectYearOfBirth("1986");
		if (! browserType.equalsIgnoreCase("firefox") || ! operationSys.equals("Linux")) {
			Assert.assertTrue(depositPage.isNewCardOK());
			Assert.assertTrue(depositPage.isExpDateOK());
			Assert.assertTrue(depositPage.isHolderNameOK());
			Assert.assertTrue(depositPage.isCcPassOK());
			Assert.assertTrue(depositPage.isAddressOK());
			Assert.assertTrue(depositPage.isCityOK());
			Assert.assertTrue(depositPage.isZipCodeOK());
			Assert.assertTrue(depositPage.isBirthDateOK());
		}	
		depositPage.registerNewCard();
		depositPage.waitErrorMessage();
		User.allowCreditCard(email);
		UtilsMethods.sleep(2000);
		String currentUrl = driver.getCurrentUrl();
		String depositUrl = currentUrl.replace("newCard", "deposit");
		driver.get(depositUrl);
		depositPage.selectCard(1);
		depositPage.setCvvPass("123");
		depositPage.submitOldCard();
		Assert.assertTrue(depositPage.isPopUpQuestionsEnable());
		depositPage.closePopUpForQuestions();
		String balanceNow = headerMenuPage.getBalance();
		Assert.assertEquals(selectedCurrency + amount + ".00", balanceNow);
		String ballanceAfterDeposit = User.getUserBalanceFromDB(email, currentUserId);
		Assert.assertEquals(Integer.parseInt(amount + "00"), Integer.parseInt(ballanceAfterDeposit));
		Assert.assertEquals(succMessage, depositPage.getSuccessMessage());
		String amountMessage = depositPage.getAmountMessage();
		Assert.assertTrue(amountMessage.contains(amount + ".00"));
		Assert.assertTrue(amountMessage.contains(moneyType + amount + ".00 " + amMessage));
		String transactionId = depositPage.getTransactionId();
		depositPage.closeAfterDepositPopUp();
		UtilsMethods.waitUrlToContains(driver, "questionnaire.jsf");
		User.verifyDepositTransaction(transactionId, amount);
		bankingHistoryPage.openBankingHistory();
		Assert.assertEquals(transactionId, bankingHistoryPage.getTransactionId(0));
		Assert.assertEquals(bankHistoryDescription, bankingHistoryPage.getDescription(0));
		Assert.assertEquals(moneyType + amount + ".00", bankingHistoryPage.getCredit(0));
  	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
