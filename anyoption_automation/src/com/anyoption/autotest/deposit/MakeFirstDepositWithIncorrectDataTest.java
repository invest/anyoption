package com.anyoption.autotest.deposit;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.DepositPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class MakeFirstDepositWithIncorrectDataTest extends BaseTest {

	public MakeFirstDepositWithIncorrectDataTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/MakeFirstDepositWithIncorrectData.xlsx", numberRows, 10);
	    return(retObjArr);
	}
	/**
	 * Makes first deposit with incorrect data
	 * @param skin
	 * @param mandatoryMess
	 * @param tooltipMess
	 * @param cvvMess
	 * @param holderNameMess
	 * @param cityMessage
	 * @param dayMess
	 * @param monthMess
	 * @param yearMess
	 * @param expDateMess
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void makeFirstDepositWithIncorrectDataTest(String skin, String mandatoryMess, String tooltipMess, String cvvMess, 
			String holderNameMess, String cityMessage, String dayMess, String monthMess, String yearMess, 
			String expDateMess) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		DepositPage depositPage = new DepositPage(driver, browserType);
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoIsCreated(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, true);
		currentUserId = testData.get("id");
		String amount = depositPage.getDefaultAmount();
		String selectedCurrency = depositPage.getSelectedCurrencySign();
  		depositPage.registerNewCard();
  		Assert.assertTrue(depositPage.isNewCardNumberErrorDisplayed());
  		Assert.assertEquals(mandatoryMess, depositPage.getNewCardErrorMessage());
  		depositPage.setCardNumber("test");
  		Assert.assertEquals(tooltipMess, depositPage.getNewCardTooltip());
  		depositPage.setCardNumber("4200000000000000");
  		depositPage.registerNewCard();
  		Assert.assertTrue(depositPage.isNewCardOK());
  		Assert.assertEquals("", depositPage.getNewCardErrorMessage());
  		Assert.assertTrue(depositPage.isCvvErrorDisplayed());
  		Assert.assertEquals(mandatoryMess, depositPage.getNewCardCvvErrorMessage());
  		depositPage.setCVV("t");
  		Assert.assertEquals(tooltipMess, depositPage.getCvvTooltip());
  		depositPage.setCVV("1234");
  		depositPage.registerNewCard();
  		Assert.assertEquals(cvvMess, depositPage.getNewCardCvvErrorMessage());
  		depositPage.setCVV("123");
  		depositPage.registerNewCard();
  		Assert.assertTrue(depositPage.isCcPassOK());
  		Assert.assertEquals("", depositPage.getNewCardCvvErrorMessage());
  		Assert.assertTrue(depositPage.isNewCardHolderNameErrorDisplayed());
  		Assert.assertEquals(mandatoryMess, depositPage.getHolderNameErrorMessage());
  		depositPage.setCardHolderName("43");
  		depositPage.registerNewCard();
  		Assert.assertEquals(holderNameMess, depositPage.getHolderNameErrorMessage());
  		depositPage.setCardHolderName("test");
  		depositPage.registerNewCard();
  		Assert.assertTrue(depositPage.isHolderNameOK());
  		Assert.assertEquals("", depositPage.getHolderNameErrorMessage());
  		Assert.assertTrue(depositPage.isStreetErrorDisplayed());
  		Assert.assertEquals(mandatoryMess, depositPage.getStreetErrorMessage());
  		depositPage.setStreet("test");
  		depositPage.registerNewCard();
  		Assert.assertTrue(depositPage.isAddressOK());
  		Assert.assertEquals("", depositPage.getStreetErrorMessage());
  		Assert.assertTrue(depositPage.isCityErrorDisplayed());
  		Assert.assertEquals(cityMessage, depositPage.getCityErrorMessage());
  		depositPage.setCity("test");
  		depositPage.registerNewCard();
  		Assert.assertTrue(depositPage.isCityOK());
  		Assert.assertEquals("", depositPage.getCityErrorMessage());
  		Assert.assertTrue(depositPage.isZipCodeErrorDisplayed());
  		Assert.assertEquals(mandatoryMess, depositPage.getZipCodeErrorMessage());
  		depositPage.setPostCode("1234");
  		depositPage.registerNewCard();
  		Assert.assertTrue(depositPage.isZipCodeOK());
  		Assert.assertEquals("", depositPage.getZipCodeErrorMessage());
  		Assert.assertTrue(depositPage.isBirthDateErrorDisplayed());
  		Assert.assertEquals(dayMess, depositPage.getBirthDayErrorMessage());
  		depositPage.selectDayOfBirth("19");
  		Assert.assertEquals("", depositPage.getBirthDayErrorMessage());
  		Assert.assertEquals(monthMess, depositPage.getBirthMonthErrorMessage());
  		depositPage.selectMonthOfBirth("06");
  		Assert.assertEquals("", depositPage.getBirthMonthErrorMessage());
  		Assert.assertEquals(yearMess, depositPage.getBirthYearErrorMessage());
  		depositPage.selectYearOfBirth("1986");
  		Assert.assertEquals("", depositPage.getBirthYearErrorMessage());
  		int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
  		String monthValue = "0" + currentMonth;
  		depositPage.selectMonthOfExpiry(monthValue);
  		Assert.assertTrue(depositPage.isExpDateErrorDisplayed());
  		Assert.assertEquals(expDateMess, depositPage.getExpDateErrorMessage());
  		int currentYear = Calendar.getInstance().get(Calendar.YEAR) + 1;
		String yearValue = String.valueOf(currentYear).replace("20", "");
		depositPage.selectYearOfExpiry(yearValue);
		Assert.assertTrue(depositPage.isExpDateOK());
		Assert.assertEquals("", depositPage.getExpDateErrorMessage());
		depositPage.registerNewCard();
		depositPage.waitErrorMessage();
		User.allowCreditCard(testData.get("email"));
		UtilsMethods.sleep(2000);
		String currentUrl = driver.getCurrentUrl();
		String depositUrl = currentUrl.replace("newCard", "deposit");
		driver.get(depositUrl);
		depositPage.selectCard(1);
		depositPage.setCvvPass("123");
		depositPage.submitOldCard();
		String balanceNow = headerMenuPage.getBalance();
		Assert.assertEquals(selectedCurrency + amount + ".00", balanceNow);
		String ballanceAfterDeposit = User.getUserBalanceFromDB(testData.get("email"), currentUserId);
		Assert.assertEquals(Integer.parseInt(amount + "00"), Integer.parseInt(ballanceAfterDeposit));
		String amountMessage = depositPage.getAmountMessage();
		Assert.assertTrue(amountMessage.contains(amount + ".00"));
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
