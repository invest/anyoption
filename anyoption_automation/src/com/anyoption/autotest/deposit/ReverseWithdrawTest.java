package com.anyoption.autotest.deposit;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.BankingHistoryPage;
import com.anyoption.autotest.pages.DepositPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimirm
 *
 */
public class ReverseWithdrawTest extends BaseTest {
	public ReverseWithdrawTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/ReverseWithdrawTestData.xlsx", numberRows, 5);
	    return(retObjArr);
	}
	/**
	 * Reverses withdraw 
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void reverseWithdrawTest(String skin, String description, String successMessage, String errorMessage, 
			String bankHistoryDescription) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		BankingHistoryPage bankingHistoryPage = new BankingHistoryPage(driver);
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanReverseWithdraw(skinId);
		HashMap<String, String> userData = loginPage.loginWithUserFromDB(sqlQuery, false);
		DepositPage depositPage = new DepositPage(driver, browserType);
		depositPage.openDepositMenu();
		depositPage.openReverseWithdraw();
		String transactionId = depositPage.getTransactionIdFromReverse();
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		String balance = headerMenuPage.getBalance();
		balance = balance.replaceAll("\\D+", "");
		String email = userData.get("email");
		currentUserId = userData.get("id");
		String balanceFromDb = userData.get("balance");
		depositPage.submitReverseWithdraw();
		Assert.assertEquals(errorMessage, depositPage.getReverseWithdrawErrorMessage());
		Assert.assertEquals(description, depositPage.getReverseWithdrawDescription());
		String amount = depositPage.getReverseWithdrawAmount();
		depositPage.clickReverseWithdrawCheckBox();
		depositPage.submitReverseWithdraw();
		String message = depositPage.getReverseWithdrawSuccessMessage();
		Assert.assertTrue(message.contains(successMessage));
		Assert.assertTrue(message.contains(amount));
		String balanceAfter = headerMenuPage.getBalance().replaceAll("\\D+", "");
		String balanceFromDbAfter = User.getUserBalanceFromDB(email, currentUserId);
		String amountBankingHistory = amount;
		amount = amount.replaceAll("\\D+", "");
		Assert.assertEquals(Integer.parseInt(balanceAfter), Integer.parseInt(balance) + Integer.parseInt(amount));
		Assert.assertEquals(Integer.parseInt(balanceFromDbAfter), Integer.parseInt(balanceFromDb) + Integer.parseInt(amount));
		User.verifyReverseWithdrawTransaction(currentUserId, amount, transactionId);
		bankingHistoryPage.openBankingHistory();
		Assert.assertEquals(bankHistoryDescription, bankingHistoryPage.getDescription(0));
		Assert.assertEquals(amountBankingHistory, bankingHistoryPage.getCredit(0));
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
