package com.anyoption.autotest.deposit;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.DepositPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class DepositWithIncorrectDataTest extends BaseTest {

	public DepositWithIncorrectDataTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/DepositWithIncorrectData.xlsx", numberRows, 6);
	    return(retObjArr);
	}
	/**
	 * Makes deposit with incorrect data
	 * @param skin
	 * @param mandatoryMess
	 * @param toolTipMess
	 * @param cvvError
	 * @param minErrorMess
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void depositWithIncorrectDataTest(String skin, String mandatoryMess, String toolTipMess, String cvvError, 
			String minErrorMess, String successMess) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		DepositPage depositPage = new DepositPage(driver, browserType);
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanMakeDeposit(skinId);
		HashMap<String, String> data = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = data.get("id");
		String balance = headerMenuPage.getBalance().replaceAll("\\D+", "");
		int balanceInt = Integer.parseInt(balance);
		depositPage.openDepositMenu();
		depositPage.submitOldCard();
		Assert.assertTrue(depositPage.isCreditCardErrorDisplayed());
		Assert.assertEquals(mandatoryMess, depositPage.getCVVpassErrorMessage());
		depositPage.setCvvPass("t");
		Assert.assertEquals(toolTipMess, depositPage.getCvvTooltip());
		depositPage.setCvvPass("1234");
		depositPage.submitOldCard();
		Assert.assertEquals(cvvError, depositPage.getCVVpassErrorMessage());
		depositPage.setCvvPass("123");
		depositPage.submitOldCard();
		Assert.assertTrue(depositPage.isCreditCardErrorDisplayed());
		Assert.assertEquals(mandatoryMess, depositPage.getCcNumberErrorMessage());
		depositPage.selectCard(1);
		Assert.assertTrue(depositPage.isCreditCardOk());
		depositPage.setDepositAmount("t");
		Assert.assertEquals(toolTipMess, depositPage.getDepositAmountTooltip());
		depositPage.setDepositAmount("99");
		depositPage.submitOldCard();
		Assert.assertEquals(minErrorMess, depositPage.getDepositAmountErrorMessage());
		User.verifyMinDepositError(currentUserId);
		depositPage.setDepositAmount("50001");
		depositPage.submitOldCard();
		UtilsMethods.sleep(3000);
//		Assert.assertEquals("Maximum deposit amount is €50,000.00", depositPage.getDepositAmountErrorMessage());
		User.verifyMaxDepositError(currentUserId);
		depositPage.closeHelpWindow();
		String amount = "250";
		depositPage.setDepositAmount(amount);
		depositPage.setCvvPass("123");
		depositPage.submitOldCard();
		Assert.assertEquals(successMess, depositPage.getSuccessMessage());
		String balanceNow = headerMenuPage.getBalance().replaceAll("\\D+", "");
		int balanceNowInt = Integer.parseInt(balanceNow);
		Assert.assertEquals(balanceInt + Integer.parseInt(amount + "00"), balanceNowInt);
		String transactionId = depositPage.getTransactionId();
		User.verifyDepositTransaction(transactionId, amount);
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
