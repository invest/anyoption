package com.anyoption.autotest.deposit;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.DepositPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class EditCreditCardTest extends BaseTest {

	public EditCreditCardTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/EditCreditCardTestData.xlsx", numberRows, 2);
	    return(retObjArr);
	}
	/**
	 * Edits credit card
	 * @param skin
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void editCreditCardTest(String skin, String error) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		DepositPage depositPage = new DepositPage(driver, browserType);
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanEditCreditCard(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, true);
		currentUserId = testData.get("id");
		depositPage.openDepositMenu();
		depositPage.openEditCreditCardMenu();
		String cardType = depositPage.getCardType();
		String oldExpiryDate = depositPage.getCardExpiryDate();
		String cardNumber = depositPage.getCardNumber();
		depositPage.openCardToEdit();
		Assert.assertEquals(cardType, depositPage.getEditCardType());
		Assert.assertEquals(cardNumber, depositPage.getEditCardNumber());
		depositPage.cancelEditCard();
		Assert.assertTrue(driver.getCurrentUrl().contains("cards.jsf"));
		depositPage.openCardToEdit();
		int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
		String monthValue = "0" + currentMonth;
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		String yearValue = String.valueOf(currentYear).replace("20", "");
		depositPage.selectEditMonth(monthValue);
		depositPage.selectEditYear(yearValue);
		depositPage.submitEditCard();
		Assert.assertTrue(depositPage.isEditCardErrorDisplayed());
		Assert.assertEquals(error, depositPage.getEditCardErrorMessage());
		currentYear = currentYear + 2;
		yearValue = String.valueOf(currentYear).replace("20", "");
		depositPage.selectEditYear(yearValue);
		depositPage.submitEditCard();
		Assert.assertTrue(driver.getCurrentUrl().contains("cards.jsf"));
		String newExpiryDate = depositPage.getCardExpiryDate();
		Assert.assertTrue(! oldExpiryDate.equals(newExpiryDate));
		Assert.assertEquals(monthValue + "/" + yearValue, newExpiryDate);
		User.verifyEditedCreditCard(currentUserId, monthValue, yearValue);
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
