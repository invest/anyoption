package com.anyoption.autotest.deposit;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.DepositPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.InvestmentPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class DeleteCreditCardAndAddNewTest extends BaseTest {

	public DeleteCreditCardAndAddNewTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/DeleteCreditCardTestData.xlsx", numberRows, 4);
	    return(retObjArr);
	}
	/**
	 * Deletes credit card, add new and make deposit with it.
	 * @param skin
	 * @param message
	 * @param question
	 * @param title
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void deleteCreditCardAndAddNewTest(String skin, String message, String question, String title) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		DepositPage depositPage = new DepositPage(driver, browserType);
		InvestmentPage investmentPage = new InvestmentPage(driver);
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanDeleteCreditCard(skinId);
		HashMap<String, String> testData = loginPage.loginWithUserFromDB(sqlQuery, true);
		currentUserId = testData.get("id");
  		String balance = headerMenuPage.getBalance();
  		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
  		String balanceInDb = testData.get("balance");
  		int balanceInDbInt = Integer.parseInt(balanceInDb);
  		depositPage.openDepositMenu();
  		depositPage.openEditCreditCardMenu();
  		depositPage.clickDeleteCardButton();
  		Assert.assertEquals(message, depositPage.getNotSelectedCardError());
  		depositPage.checkCardToDelete();
  		depositPage.clickDeleteCardButton();
  		Assert.assertEquals(question, depositPage.getDeleteCardInfoMessage());
  		depositPage.continueDelete();
  		depositPage.waitPaimentMethodSelector();
  		depositPage.openDepositMenu();
  		Assert.assertTrue(depositPage.isDepositWithNewCardSelected());
  		Assert.assertEquals(title, depositPage.getScreenTitle());
  		User.verifyDeletedCreditCard(testData.get("id"));
  		
  		Assert.assertTrue(depositPage.isCurrencySelectDisabled());
  		String amount = "250";
  		depositPage.setNewDepositAmount(amount);
  		depositPage.setCardNumber("4200000000000000");
  		depositPage.selectMonthOfExpiry(11);
		depositPage.selectYearOfExpiry(1);
		depositPage.setCardHolderName("test onather card");
		depositPage.setCVV("123");
		depositPage.clickTitle();
		if (! browserType.equalsIgnoreCase("firefox") || ! operationSys.equals("Linux")) {
			Assert.assertTrue(depositPage.isNewCardOK());
			Assert.assertTrue(depositPage.isExpDateOK());
			Assert.assertTrue(depositPage.isHolderNameOK());
		}	
		depositPage.registerNewCard();
		depositPage.waitErrorMessage();
		User.allowCreditCard(testData.get("email"));
		UtilsMethods.sleep(2000);
		String currentUrl = driver.getCurrentUrl();
		String depositUrl = currentUrl.replace("newCard", "deposit");
		driver.get(depositUrl);
		depositPage.selectCard(1);
		depositPage.setDepositAmount(amount);
		depositPage.setCvvPass("123");
		depositPage.submitOldCard();
		String balanceNow = headerMenuPage.getBalance();
		int balanceNowInt = Integer.parseInt(balanceNow.replaceAll("[^\\d]", ""));
		String balanceNowInDb = User.getUserBalanceFromDB(testData.get("email"), currentUserId);
		int balanceNowInDbInt = Integer.parseInt(balanceNowInDb);
		Assert.assertEquals(balanceInt + Integer.parseInt(amount + "00"), balanceNowInt);
		Assert.assertEquals(balanceInDbInt + Integer.parseInt(amount + "00") , balanceNowInDbInt);
		String amountMessage = depositPage.getAmountMessage();
		Assert.assertTrue(amountMessage.contains(amount + ".00"));
		String transactionId = depositPage.getTransactionId();
		depositPage.continiueAfterNewDeposit();
		Assert.assertTrue(investmentPage.IsBinaryOptionTabActive());
		User.verifyDepositTransaction(transactionId, amount);
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
