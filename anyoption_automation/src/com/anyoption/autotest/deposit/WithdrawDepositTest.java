package com.anyoption.autotest.deposit;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.BankingHistoryPage;
import com.anyoption.autotest.pages.DepositPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimirm
 *
 */
public class WithdrawDepositTest extends BaseTest {
	public WithdrawDepositTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/WithdrawDepositTestData.xlsx", numberRows, 4);
	    return(retObjArr);
	}
	/**
	 * Withdraws deposit
	 * - verify balance
	 * - verify balance in data base
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void withdrawDeposit(String skin, String screenTitle, String thanksMessage, String bankHistoryDesc) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		BankingHistoryPage bankingHistoryPage = new BankingHistoryPage(driver);
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanWithdrawDeposit(skinId);
		HashMap<String, String> userData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = userData.get("id");
		DepositPage depositPage = new DepositPage(driver, browserType);
		depositPage.openDepositMenu();
		depositPage.openWithdrawMenu();
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		String balance = headerMenuPage.getBalance();
		String moneyType = balance.replaceAll("\\d+.*", "");
		balance = balance.replaceAll("\\D+", "");
		String userFirstName = userData.get("first_name");
		String email = userData.get("email");
		String balanceFromDb = userData.get("balance");
		Assert.assertEquals(screenTitle, depositPage.getWithdrawScreenTitle());
		depositPage.selectCardToWithdraw(1);
		String amount = "200";
		depositPage.setAmountToWithdraw(amount);
		depositPage.clickWithdrawButton();
		if (depositPage.isResetBonusPopUpDisplayed() == true) {
			depositPage.confirmResetBonus();
		}
		depositPage.choosekWithdrawProfitRadioButton();
		depositPage.submitPopUpWithdraw();
		Assert.assertEquals(thanksMessage, depositPage.getFeedbackMessage());
		depositPage.confirmFeedback();
		Assert.assertTrue(depositPage.isWithdrawPopUpDisplayed());
		Assert.assertTrue(depositPage.getWithdrawPopUpHeading().contains(userFirstName));
		switch (moneyType) {
		case "$":
			Assert.assertEquals("3000", depositPage.getFeeFromImortantNote(moneyType));
			break;
		case "€":
			Assert.assertEquals("2500", depositPage.getFeeFromImortantNote(moneyType));
		}
		depositPage.submitWithdrawFinally();
		Assert.assertTrue(depositPage.getWithdrawPopUpText().contains(moneyType + amount));
		depositPage.closeWithdrawAcceptedText();
		String balanceAfter = headerMenuPage.getBalance().replaceAll("\\D+", "");
		String balanceFromDbAfter = User.getUserBalanceFromDB(email, currentUserId);
		Assert.assertEquals(Integer.parseInt(balanceAfter), Integer.parseInt(balance) - Integer.parseInt(amount + "00"));
		String amountInDb = amount + "00";
		Assert.assertEquals(Integer.parseInt(balanceFromDb) - Integer.parseInt(amountInDb), Integer.parseInt(balanceFromDbAfter));
		User.verifyWithdrawTransaction(userData.get("id"), amount);
		bankingHistoryPage.openBankingHistory();
		Assert.assertEquals(bankHistoryDesc, bankingHistoryPage.getDescription(0));
		Assert.assertEquals(moneyType + amount + ".00", bankingHistoryPage.getDebit(0));
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
