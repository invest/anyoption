package com.anyoption.autotest.deposit;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.pages.DepositPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class WithdrawDepositWithIncorrectDataTest extends BaseTest {

	public WithdrawDepositWithIncorrectDataTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/WithdrawWithIncorrectData.xlsx", numberRows, 5);
	    return(retObjArr);
	}
	/**
	 * Withdraws deposit with incorrect data
	 * @param skin
	 * @param mandatoryMess
	 * @param invNumberMess
	 * @param minMessage
	 * @param maxMessage
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void withdrawDepositWithIncorrectDataTest(String skin, String mandatoryMess, String invNumberMess, String minMessage, 
			String maxMessage) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl, parameterSkin);
		DepositPage depositPage = new DepositPage(driver, browserType);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanWithdrawDeposit(skinId);
		HashMap<String, String> userData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = userData.get("id");
		String balance = headerMenuPage.getBalance().replaceAll("\\D+", "");
		depositPage.openDepositMenu();
		depositPage.openWithdrawMenu();
		depositPage.selectCardToWithdraw(1);
		depositPage.clickWithdrawButton();
		if (depositPage.isResetBonusPopUpDisplayed() == true) {
			depositPage.confirmResetBonus();
		}
		Assert.assertEquals(mandatoryMess, depositPage.getWithdrawDepositErrorMessage());
		depositPage.setAmountToWithdraw("test");
		depositPage.clickWithdrawButton();
		if (depositPage.isResetBonusPopUpDisplayed() == true) {
			depositPage.confirmResetBonus();
		}
		depositPage.skipWithdrawPopup();
		Assert.assertEquals(invNumberMess, depositPage.getWithdrawDepositErrorMessage());
		depositPage.setAmountToWithdraw("24");
		depositPage.clickWithdrawButton();
		if (depositPage.isResetBonusPopUpDisplayed() == true) {
			depositPage.confirmResetBonus();
		}
		depositPage.skipWithdrawPopup();
		Assert.assertEquals(minMessage, depositPage.getWithdrawDepositErrorMessage());
		User.verifyMinWithdrawError(currentUserId);
		String amount = balance.substring(0, balance.length() - 2);
		amount = String.valueOf(Integer.parseInt(amount) + 1);
		depositPage.setAmountToWithdraw(amount);
		depositPage.clickWithdrawButton();
		if (depositPage.isResetBonusPopUpDisplayed() == true) {
			depositPage.confirmResetBonus();
		}
		depositPage.skipWithdrawPopup();
		Assert.assertEquals(maxMessage, depositPage.getWithdrawDepositErrorMessage());
		User.verifyMaxWithdrawError(currentUserId);
		amount = "100";
		depositPage.setAmountToWithdraw(amount);
		depositPage.clickWithdrawButton();
		if (depositPage.isResetBonusPopUpDisplayed() == true) {
			depositPage.confirmResetBonus();
		}
		depositPage.skipWithdrawPopup();
		depositPage.submitWithdrawFinally();
		depositPage.closeWithdrawAcceptedText();
		String balanceAfter = headerMenuPage.getBalance().replaceAll("\\D+", "");
		Assert.assertEquals(Integer.parseInt(balanceAfter), Integer.parseInt(balance) - Integer.parseInt(amount + "00"));
		User.verifyWithdrawTransaction(userData.get("id"), amount);
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
