package com.anyoption.autotest.login;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ChangeUrl;
import com.anyoption.autotest.utils.ReadTestData;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class ForgotPasswordTest extends BaseTest {

	public ForgotPasswordTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/ForgotPasswordTestData.xlsx", numberRows, 6);
	    return(retObjArr);
	}
	/**
	 * Sends request for forgotten password
	 * @param skin
	 * @param title
	 * @param forgotPassMess1
	 * @param incorrectMess
	 * @param forgotPassMess2
	 * @param infoMess
	 */
	@Test(dataProvider = "dp")
	public void forgotPasswordTest(String skin, String title, String forgotPassMess1, String incorrectMess, String forgotPassMess2, 
			String infoMess) {
		parameterSkin = skin;
		if (! skin.equals("EN")) {
			ChangeUrl changeUrl = new ChangeUrl();
			String testUrlNow = changeUrl.changeUrl(testUrl, skin);
			driver.get(testUrlNow);
		}
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		loginPage.clickLoginLink();
		loginPage.clickForgotPassword();
		Assert.assertEquals(title, loginPage.getLoginScreenTitle());
		Assert.assertEquals(forgotPassMess1, loginPage.getForgotPassMessage(0));
		loginPage.submitForgotPass(0);
		Assert.assertTrue(loginPage.isForgotPassErrorDisplayed());
		Assert.assertEquals(incorrectMess, loginPage.getForgotPassErrorMessage());
		driver.navigate().refresh();
		String username = "test@";
		loginPage.setForgotPassUsername(username);
		loginPage.submitForgotPass(0);
		Assert.assertTrue(loginPage.isForgotPassErrorDisplayed());
		Assert.assertEquals(incorrectMess, loginPage.getForgotPassErrorMessage());
		username = "vladimir.mladenov@anyoption.com";
		loginPage.setForgotPassUsername(username);
		loginPage.submitForgotPass(0);
		Assert.assertEquals(forgotPassMess2, loginPage.getForgotPassMessage(1));
		Assert.assertTrue(username.contains(loginPage.getForgotPassEmail().replace("*", "")));
		loginPage.submitForgotPass(1);
		Assert.assertTrue(loginPage.isForgotPassInfoMessageDisplayed());
		Assert.assertEquals(infoMess, loginPage.getForgotPassInfoMessage());
	}
}
