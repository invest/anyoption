package com.anyoption.autotest.login;

import java.io.IOException;
import java.util.ArrayList;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.anyoption.autotest.common.DBLayer;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ChangeUrl;
import com.anyoption.autotest.utils.ReadTestData;
import com.anyoption.autotest.utils.UserSQL;
import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class LoginWithIncorrectDataTest extends BaseTest {

	public LoginWithIncorrectDataTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/LoginWithIncorrectDataTestData.xlsx", numberRows, 4);
	    return(retObjArr);
	}
	/**
	 * Tries to login with incorrect data.
	 * @param skin
	 * @param incorrectMess
	 * @param shortPassMess
	 * @param shortUsernameMess
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void loginWithIncorrectDataTest(String skin, String incorrectMess, String shortPassMess, String shortUsernameMess) throws Exception {
		parameterSkin = skin;
		if (! skin.equals("EN")) {
			ChangeUrl changeUrl = new ChangeUrl();
			String testUrlNow = changeUrl.changeUrl(testUrl, skin);
			driver.get(testUrlNow);
		}
		DBLayer db = new DBLayer();
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoIsCreatedWithoutCheck(skinId);
  		ArrayList<String[]> results = db.getResultsFromDBImmediately(sqlQuery, false, 2);
  		if (results.size() == 0) {
  			throw new SkipException("Skipping the test case no users");
  		}
  		String userName = results.get(0)[3];
  		String firstName = results.get(0)[6];
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		loginPage.clickLoginLink();
		loginPage.clickLoginButton();
		Assert.assertEquals(incorrectMess, loginPage.getLoginError());
		loginPage.setUsername(userName);
		loginPage.setPassword("12345");
		loginPage.clickLoginButton();
		Assert.assertEquals(shortPassMess, loginPage.getLoginError());
		loginPage.setUsername(userName + "E");
		loginPage.setPassword("123456");
		loginPage.clickLoginButton();
		Assert.assertEquals(incorrectMess, loginPage.getLoginErrorMessage());
		loginPage.setUsername("Autot");
		loginPage.setPassword("123456");
		loginPage.clickLoginButton();
		Assert.assertEquals(shortUsernameMess, loginPage.getLoginError());
		loginPage.setUsername(userName);
		loginPage.setPassword("123456");
		loginPage.clickLoginButton();
		Assert.assertEquals(firstName, headerMenuPage.getUserNameFromHeader());
	}
	/**
	 * Logout
	 * @throws IOException 
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		afterMethod = loginPage.logout(result, parameterSkin, browser);
	}
}
