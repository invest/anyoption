package com.anyoption.autotest.login;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.anyoption.autotest.pages.BubblesPage;
import com.anyoption.autotest.pages.DynamicsPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.InvestmentPage;
import com.anyoption.autotest.pages.LoginPage;
import com.anyoption.autotest.pages.LongTermPage;
import com.anyoption.autotest.pages.OptionPlusPage;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ChangeUrl;
import com.anyoption.autotest.utils.ReadTestData;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class CheckLoginFromInvestmentScreensTest extends BaseTest {

	public CheckLoginFromInvestmentScreensTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/CheckLoginScreenTestData.xlsx", numberRows, 4);
	    return(retObjArr);
	}
	/**
	 * Checks login screen from all investment screens
	 * @param skin
	 * @param title
	 * @param message
	 * @param bubblesNotification
	 */
	@Test(dataProvider = "dp")
	public void checkLoginFromInvestmentScreensTest(String skin, String title, String message, String bubblesNotification) {
		if (! skin.equals("EN")) {
			ChangeUrl changeUrl = new ChangeUrl();
			String testUrlNow = changeUrl.changeUrl(testUrl, skin);
			driver.get(testUrlNow);
		}
		InvestmentPage investmentPage = new InvestmentPage(driver);
		LoginPage loginPage = new LoginPage(driver, browserType, testUrl);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		OptionPlusPage optionPlusPage = new OptionPlusPage(driver);
		DynamicsPage dynamicsPage = new DynamicsPage(driver);
		BubblesPage bubblesPage = new BubblesPage(driver);
		LongTermPage longTermPage = new LongTermPage(driver);
		investmentPage.clickUpButton(0);
		Assert.assertTrue(investmentPage.isPopUpForLoginDisplayed(0));
		investmentPage.clickLoginButton(0);
		Assert.assertTrue(loginPage.isLoginScreenOpen());
		Assert.assertEquals(title, loginPage.getLoginScreenTitle());
		Assert.assertEquals(message, loginPage.getLoginScreenMessage());
		driver.navigate().back();
		headerMenuPage.openInvstmentScreenFromHomePage(0);
		int number = investmentPage.getAcctiveInvestment();
  		if (number == -1) {
  			investmentPage.changeMarket(0);
  			number = 0;
  		}
  		investmentPage.clickUpButton(number);
  		Assert.assertTrue(investmentPage.isPopUpForLoginDisplayed(number));
  		investmentPage.clickLoginButton(number);
		Assert.assertTrue(loginPage.isLoginScreenOpen());
		Assert.assertEquals(title, loginPage.getLoginScreenTitle());
		Assert.assertEquals(message, loginPage.getLoginScreenMessage());
		driver.navigate().back();
		optionPlusPage.openOptionPlusMenu(browserType);
		optionPlusPage.setActiveMarket();
		optionPlusPage.clickUpButton(0, browserType);
		Assert.assertTrue(investmentPage.isPopUpForLoginDisplayed(0));
		investmentPage.clickLoginButton(0);
		Assert.assertTrue(loginPage.isLoginScreenOpen());
		Assert.assertEquals(title, loginPage.getLoginScreenTitle());
		Assert.assertEquals(message, loginPage.getLoginScreenMessage());
		driver.navigate().back();
		dynamicsPage.openDynamics();
		if (dynamicsPage.isDynamicsTutorialDisplayed() == true) {
			dynamicsPage.closeTutorial();
		}
		dynamicsPage.chooseMarketToInvest();
		dynamicsPage.clickUpButton();
		dynamicsPage.loginFromDynamics();
		Assert.assertTrue(loginPage.isLoginScreenOpen());
		Assert.assertEquals(title, loginPage.getLoginScreenTitle());
		Assert.assertEquals(message, loginPage.getLoginScreenMessage());
		driver.navigate().back();
		bubblesPage.openBubbles();
		bubblesPage.switchDriverToBubbles();
		bubblesPage.passTutorial();
		bubblesPage.clickGoButton();
		Assert.assertEquals(bubblesNotification, bubblesPage.getNegativeNotification());
		driver.switchTo().defaultContent();
		Assert.assertTrue(bubblesPage.isLoginPopUpDisplayed());
		bubblesPage.clickLoginButton();
		Assert.assertTrue(loginPage.isLoginScreenOpen());
		Assert.assertEquals(title, loginPage.getLoginScreenTitle());
		Assert.assertEquals(message, loginPage.getLoginScreenMessage());
		driver.navigate().back();
		longTermPage.openLongTerm();
  		longTermPage.setMarket(1);
  		String id = longTermPage.getOptionId(0);
  		longTermPage.buyLongOption(id);
  		longTermPage.clickLoginButton();
  		Assert.assertTrue(loginPage.isLoginScreenOpen());
		Assert.assertEquals(title, loginPage.getLoginScreenTitle());
		Assert.assertEquals(message, loginPage.getLoginScreenMessage());
	}
}
