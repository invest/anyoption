package com.anyoption.autotest.common;

import java.util.ArrayList;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.AssertJUnit;

import com.anyoption.autotest.common.User.Skin;

public class Browser {

	public static WebDriver browser;

	public WebDriver init(String browserType) {
		if (browserType.equalsIgnoreCase("firefox")) {
			browser = new FirefoxDriver();
			return browser;
		} else if (browserType.equalsIgnoreCase("ie")) {
			System.setProperty("webdriver.ie.driver",
					"C:\\SeleniumDrivers\\IEDriverServer.exe");
			browser = new InternetExplorerDriver();
			return browser;
		} else if (browserType.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver",
					"C:\\SeleniumDrivers\\chromedriver.exe");
			// ChromeOptions options = new ChromeOptions();
			// options.setBinary("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
			// options.addArguments("--start-maximized",
			// "--disable-extensions");
			browser = new ChromeDriver(/* options */);
			return browser;
		} else {
			Assert.fail("Please start the correct browser: IE, Frefox or Crome!");
			return browser;
		}
	}

	public static void typeUserGeneratedData(String skin) {
		browser.findElement(By.id("funnel_firstName")).click();
		browser.findElement(By.id("funnel_firstName")).clear();
		browser.findElement(By.id("funnel_firstName")).sendKeys(User.firstName);
		browser.findElement(By.id("funnel_lastName")).click();
		browser.findElement(By.id("funnel_lastName")).clear();
		browser.findElement(By.id("funnel_lastName")).sendKeys(User.lastName);
		browser.findElement(By.id("funnel_email")).click();
		browser.findElement(By.id("funnel_email")).clear();
		browser.findElement(By.id("funnel_email")).sendKeys(User.eMail);
		browser.findElement(By.id("funnel_drop_Country_h")).click(); // className("funnel_drop_selected")
		if (skin.equalsIgnoreCase("ET")) {
			browser.findElement(By.id("funnel_drop_li_55")).click();
		} else if (skin.equalsIgnoreCase("RU")) {
			browser.findElement(By.id("funnel_drop_li_172")).click();
		} else if (skin.equalsIgnoreCase("TR")) {
			browser.findElement(By.id("funnel_drop_li_212")).click();
		} else if (skin.equalsIgnoreCase("ES")) {
			browser.findElement(By.id("funnel_drop_li_192")).click();
		} else if (skin.equalsIgnoreCase("DE")) {
			browser.findElement(By.id("funnel_drop_li_80")).click();
		} else if (skin.equalsIgnoreCase("IT")) {
			browser.findElement(By.id("funnel_drop_li_102")).click();
		} else if (skin.equalsIgnoreCase("FR")) {
			browser.findElement(By.id("funnel_drop_li_73")).click();
		} else if (skin.equalsIgnoreCase("SG")) {
			browser.findElement(By.id("funnel_drop_li_44")).click();
		} else if (skin.equalsIgnoreCase("KR")) {
			browser.findElement(By.id("funnel_drop_li_191")).click();
		} else {
			browser.findElement(By.id("funnel_drop_li_219")).click();
		}
		browser.findElement(By.id("funnel_mobilePhone")).click();
		browser.findElement(By.id("funnel_mobilePhone")).clear();
		browser.findElement(By.id("funnel_mobilePhone")).sendKeys(User.phone);
		browser.findElement(By.id("funnel_password")).clear();
		browser.findElement(By.id("funnel_password")).sendKeys(User.password);
		browser.findElement(By.id("funnel_passwordConfirm")).clear();
		browser.findElement(By.id("funnel_passwordConfirm")).sendKeys(
				User.password);
	}

	public static void clickOnTermsLinkAtQuickSignUpForm() {
		// open terms and conditions
		browser.findElement(By.id("funnel_terms_link")).click();
	}

	public static void clickOnTermsLinkAtRegistrationForm() {
		// open terms and conditions
		browser.findElement(By.id("funnel_terms_h_link")).click();
	}

	public static void verifyTermsAndConditions(Skin skin)
			throws InterruptedException {
		String[] handles = browser.getWindowHandles().toArray(new String[0]);

		// switch to the pop-up window with terms and conditions
		browser.switchTo().window(handles[handles.length - 1]);
		String actualTermsText = browser
				.findElement(By.id("agreement_title_1")).getText();

		try {
			if (skin == Skin.EN) {
				AssertJUnit
						.assertEquals(
								"AGREEMENT\n\nTERMS AND CONDITIONS FOR THE SERVICES OFFERED BY THE COMPANY",
								actualTermsText);
			} else if (skin == Skin.ES) {
				AssertJUnit
						.assertEquals(
								"ACUERDO\n\nTÉRMINOS Y CONDICIONES DE LOS SERVICIOS OFRECIDOS POR LA COMPAÑÍA",
								actualTermsText);
			} else if (skin == Skin.DE) {
				AssertJUnit
						.assertEquals(
								"VERTRAG\n\nALLGEMEINE GESCHÄFTSBEDINGUNGEN FÜR DIE VOM UNTERNEHMEN ANGEBOTENEN DIENSTLEISTUNGEN",
								actualTermsText);
			} else if (skin == Skin.TR) {
				AssertJUnit.assertEquals("SÖZLEŞME", actualTermsText);
			} else if (skin == Skin.IT) {
				AssertJUnit
						.assertEquals(
								"CONTRATTO\n\nTERMINI GENERALI DEI SERVIZI OFFERTI DALLA SOCIETÀ",
								actualTermsText);
			} else if (skin == Skin.FR) {
				AssertJUnit
						.assertEquals(
								"ACCORD\n\nCONDITIONS GÉNÉRALES POUR LES SERVICES FOURNIS PAR LA SOCIÉTÉ",
								actualTermsText);
			} else if (skin == Skin.RU) {
				AssertJUnit.assertEquals("СОГЛАШЕНИЕ", actualTermsText);
			} else if (skin == Skin.SG) {
				AssertJUnit.assertEquals("协议", actualTermsText);
			} else if (skin == Skin.KR) {
				AssertJUnit.assertEquals("AGREEMENT", actualTermsText);
			} else if (skin == Skin.ET) {
				AssertJUnit.assertEquals("הסכם התקשרות", actualTermsText);
			} else {
				System.out.println("Please enter a valid skin!");
			}
		} catch (Exception e) {
			// verificationErrors.append(e.toString());
			System.out.println(e.getMessage());
		}

		// close the pop-up window
		browser.close();
	}

	public static void verifyTermsAndConditions(WebDriver browser,
			String termsAndConditions, String skin) throws InterruptedException {
		String[] handles = browser.getWindowHandles().toArray(new String[0]);
		// switch to the pop-up window with terms and conditions
		browser.switchTo().window(handles[handles.length - 1]);
		String actualTermsText = "";
		String expectedTerms = termsAndConditions.replace("\\n", "\n");
		// remote below print!!!
		System.out.println("New terms text is:" + expectedTerms);

		try {
			if (skin.equalsIgnoreCase("TR") || skin.equalsIgnoreCase("RU")
					|| skin.equalsIgnoreCase("KR")
					|| skin.equalsIgnoreCase("SG")) {
				actualTermsText = browser.findElement(By.cssSelector("u"))
						.getText();
				System.out.println("A terms text is:" + actualTermsText);
				AssertJUnit.assertEquals(termsAndConditions, browser
						.findElement(By.cssSelector("u")).getText());
			} else {
				actualTermsText = browser.findElement(
						By.id("agreement_title_1")).getText();
				System.out.println("A terms text is:" + actualTermsText);
				AssertJUnit.assertEquals(expectedTerms, actualTermsText);
			}
		} catch (Exception e) {
			// verificationErrors.append(e.toString());
			System.out.println(e.getMessage());
		}

		// close the pop-up window
		browser.close();
	}

	public static void clickOpenAccountButton() {
		browser.findElement(By.id("btn_open_account_in")).click();
	}

	public static void clickLoginButtonAtTheTopOfThePage(WebDriver browser) {
		switchToiframe(browser);
		browser.findElement(By.id("loginStrip_link")).click();
		browser.switchTo().defaultContent();
	}

	public static void clickLogoutButton() {
		switchToiframe(browser);
		browser.findElement(By.id("logoutForm:logoutLink")).click();
		browser.switchTo().defaultContent();
	}

	public static void confirmTerms() {
		browser.findElement(By.id("funnel_terms_h_in")).click();
	}

	public static void switchToiframe(WebDriver browser) {
		browser.switchTo().frame("header");
		// browser.switchTo().frame(browser.findElement(By.tagName("iframe")));
	}

	public static void openLoginPage(WebDriver browser, String loginPageTitle) {
		clickLoginButtonAtTheTopOfThePage(browser);
		// String expectedText = loginPageTitle.replaceAll("\\s", "");
		String expectedText = loginPageTitle.trim();
		String actual = browser.getTitle().trim();
		AssertJUnit.assertEquals(expectedText, actual);
	}

	public static void openForgotPasswordPage() {
		browser.findElement(By.id("loginPage_forgotPassword")).click();
		AssertJUnit.assertEquals(
				"Forgot password – Change password in case needed",
				browser.getTitle());
	}

	public static void populateForgotPasswordFields(String phone, String eMail,
			String countryPhoneCode) {
		browser.findElement(By.id("passwordForm:mobilePhoneCode")).click();
		browser.findElement(By.id("passwordForm:mobilePhoneCode")).clear();
		browser.findElement(By.id("passwordForm:mobilePhoneCode")).sendKeys(
				countryPhoneCode);
		browser.findElement(By.id("passwordForm:mobilePhone")).click();
		browser.findElement(By.id("passwordForm:mobilePhone")).clear();
		browser.findElement(By.id("passwordForm:mobilePhone")).sendKeys(phone);
		browser.findElement(By.id("passwordForm:email")).click();
		browser.findElement(By.id("passwordForm:email")).clear();
		browser.findElement(By.id("passwordForm:email")).sendKeys(eMail);
		browser.findElement(By.id("passwordForm:mailSms:0")).click();

		browser.findElement(By.id("forgot_password_btn")).click();
	}

	public static void clickOnLoginButton() {
		// browser.findElement(By.id("login_btn")).click(); // there is no id
		WebElement t = browser
				.findElement(By
						.cssSelector("html body#theBody div.head_bgr div.main_bgr div.pageWidth.cont_pos div.page_bgr form#login div div.login_info_1 a.buton_blue.but_sprite.clear.mt10 span.rs.but_sprite span.login_but_w.cs.but_sprite.ttUC"));
		t.click();
	}

	public static void login(String username, String password,
			String loginPageTitle, String afterLoginPageTitle, String eMail,
			String depositPageTitle) throws Exception {
		openLoginPage(browser, loginPageTitle);
		// if (browser.findElement(
		// By.partialLinkText("Login with a different user"))
		// .isDisplayed()) {
		// browser.findElement(By.linkText("Login with a different user »"))
		// .click();
		// }
		String str = browser.findElement(By.id("login:j_username"))
				.getAttribute("style");
		if (str.equalsIgnoreCase("display: none;")) {
			browser.findElement(By.id("login:j_password")).click();
			browser.findElement(By.id("login:j_password")).clear();
			browser.findElement(By.id("login:j_password")).sendKeys(password); // User.password
		} else {
			browser.findElement(By.id("login:j_username")).click();
			browser.findElement(By.id("login:j_username")).clear();
			browser.findElement(By.id("login:j_username")).sendKeys(username); // User.eMail
			browser.findElement(By.id("login:j_password")).click();
			browser.findElement(By.id("login:j_password")).clear();
			browser.findElement(By.id("login:j_password")).sendKeys(password); // User.password
		}
		clickOnLoginButton();
		// ArrayList<String[]> userCreditCards = User
		// .getUserVisibleCreditCards(eMail);
		// (userCreditCards.size() > 0) &&

		ArrayList<String[]> resForFirstDeposit = User
				.checkForSuccessfulFirstDeposit(eMail);
		if (resForFirstDeposit.size() > 0) {
			String expectedText = "";
			ArrayList<String[]> userSkinId = User.getUserSkinID(eMail);
			String skinID = userSkinId.get(0)[0];
			ArrayList<String[]> questionary = User
					.checkIfUserCompleteQuestionary(eMail);

			if ((skinID.equals("16") || skinID.equals("18")
					|| skinID.equals("19") || skinID.equals("20") || skinID
						.equals("21")) && questionary.size() > 0) {
				expectedText = "EU regulation questionnaire - anyoption™";
			} else {
				expectedText = afterLoginPageTitle.trim();
			}
			AssertJUnit.assertEquals(expectedText, browser.getTitle().trim());
		} else {
			String expectedText = depositPageTitle.trim();
			AssertJUnit.assertEquals(expectedText, browser.getTitle().trim());
		}
	}

	public static void openMyAccount() {
		switchToiframe(browser);
		browser.findElement(By.id("myaccount_btn")).click(); // there is no id
		AssertJUnit.assertEquals("All the investments - anyoption™",
				browser.getTitle());
		browser.switchTo().defaultContent();
	}

	public static void openDepositPage(String mainURL, String depositPageTitle) {
		String depositURL = mainURL + "/jsp/pages/deposit.jsf";
		browser.get(depositURL);
		String expectedText = depositPageTitle.trim();
		String actualTitle = browser.getTitle().trim();
		AssertJUnit.assertEquals(expectedText, actualTitle);
	}

	public static void openEditCreditCardPage(WebDriver driver,
			String testEnvURL, String depositPageTitle,
			String editDeleteCreditCardsPageTitle) throws Exception {
		Browser.openDepositPage(testEnvURL, depositPageTitle);
		driver.findElement(By.partialLinkText("Edit card ")).click();
		String expectedText = editDeleteCreditCardsPageTitle.trim();
		String actualTitle = browser.getTitle().trim();
		AssertJUnit.assertEquals(expectedText, actualTitle);
	}

	public static void fillFirstCreditCardDetails(String firstName,
			String depositAmount) {
		browser.findElement(By.id("newCardForm:deposit")).click();
		browser.findElement(By.id("newCardForm:deposit")).clear();
		browser.findElement(By.id("newCardForm:deposit")).sendKeys(
				depositAmount);
		browser.findElement(By.id("newCardForm:ccNum")).click();
		browser.findElement(By.id("newCardForm:ccNum")).clear();
		browser.findElement(By.id("newCardForm:ccNum")).sendKeys(
				"4200000000000000");// 4111111111111111
		browser.findElement(By.id("newCardForm:ccPass")).click();
		browser.findElement(By.id("newCardForm:ccPass")).clear();
		browser.findElement(By.id("newCardForm:ccPass")).sendKeys("123");
		new Select(browser.findElement(By.id("newCardForm:expMonth")))
				.selectByVisibleText("05");
		new Select(browser.findElement(By.id("newCardForm:expYear")))
				.selectByVisibleText("18");
		browser.findElement(By.id("newCardForm:holderName")).click();
		browser.findElement(By.id("newCardForm:holderName")).clear();
		browser.findElement(By.id("newCardForm:holderName"))
				.sendKeys(firstName);
		browser.findElement(By.id("newCardForm:street")).click();
		browser.findElement(By.id("newCardForm:street")).clear();
		browser.findElement(By.id("newCardForm:street")).sendKeys("Mladost");
		browser.findElement(By.id("newCardForm:cityNameAO")).click();
		browser.findElement(By.id("newCardForm:cityNameAO")).clear();
		browser.findElement(By.id("newCardForm:cityNameAO")).sendKeys("Sofia");
		browser.findElement(By.id("newCardForm:zipCode")).click();
		browser.findElement(By.id("newCardForm:zipCode")).clear();
		browser.findElement(By.id("newCardForm:zipCode")).sendKeys("1000");
		new Select(browser.findElement(By.id("newCardForm:birthDay")))
				.selectByVisibleText("07");
		new Select(browser.findElement(By.id("newCardForm:birthMonth")))
				.selectByVisibleText("03");
		new Select(browser.findElement(By.id("newCardForm:birthYear")))
				.selectByVisibleText("1980");
	}

	public static void fillCreditCardDetails(String firstName,
			String depositAmount) {
		browser.findElement(By.id("newCardForm:deposit")).click();
		browser.findElement(By.id("newCardForm:deposit")).clear();
		browser.findElement(By.id("newCardForm:deposit")).sendKeys(
				depositAmount);
		browser.findElement(By.id("newCardForm:ccNum")).click();
		browser.findElement(By.id("newCardForm:ccNum")).clear();
		browser.findElement(By.id("newCardForm:ccNum")).sendKeys(
				"4200000000000000");// 4111111111111111
		browser.findElement(By.id("newCardForm:ccPass")).click();
		browser.findElement(By.id("newCardForm:ccPass")).clear();
		browser.findElement(By.id("newCardForm:ccPass")).sendKeys("123");
		new Select(browser.findElement(By.id("newCardForm:expMonth")))
				.selectByVisibleText("02");
		new Select(browser.findElement(By.id("newCardForm:expYear")))
				.selectByVisibleText("19");
		browser.findElement(By.id("newCardForm:holderName")).click();
		browser.findElement(By.id("newCardForm:holderName")).clear();
		browser.findElement(By.id("newCardForm:holderName"))
				.sendKeys(firstName);
	}

	public static void openEditDeleteCardPage(String mainURL,
			String editDeleteCreditCardPageTitle) {
		String editDeleteCardURL = mainURL + "/jsp/pages/cards.jsf";
		browser.get(editDeleteCardURL);
		String expectedText = editDeleteCreditCardPageTitle.trim();
		String actualTitle = browser.getTitle().trim();
		AssertJUnit.assertEquals(expectedText, actualTitle);
	}

	public static void openEditCardPage(String mainURL,
			String editCreditCardPageTitle) {
		browser.findElement(By.linkText("************0000")).click();
		// String editCardURL = mainURL + "/jsp/pages/editCard.jsf";
		// browser.get(editCardURL);
		String expectedText = editCreditCardPageTitle.trim();
		String actualTitle = browser.getTitle().trim();
		AssertJUnit.assertEquals(expectedText, actualTitle);
	}

	public static void clickDepositButtonAtTheTopOfThePage() {
		switchToiframe(browser);
		browser.findElement(By.id("loginStrip_link")).click();
		browser.switchTo().defaultContent();
	}

	public static void openPersonalDetailsPage(String mainURL, String passwrd) {
		// browser.findElement(By.partialLinkText("Personal details")).click();
		String editPersonalDetailsURL = mainURL + "/jsp/pages/personal.jsf";
		browser.get(editPersonalDetailsURL);
		String title = "Personal details - anyoption™";
		String expectedText = title.trim();
		String actualTitle = browser.getTitle().trim();
		AssertJUnit.assertEquals(expectedText, actualTitle);
		if (browser.findElement(By.id("personalForm:password")).isDisplayed()) {
			browser.findElement(By.id("personalForm:password")).click();
			browser.findElement(By.id("personalForm:password")).clear();
			browser.findElement(By.id("personalForm:password")).sendKeys(
					passwrd);
			browser.findElement(By.id("personalForm:j_id1078748838_4eb51d2d"))
					.click();
			String actlTitle = browser.getTitle().trim();
			AssertJUnit
					.assertEquals("Personal details - anyoption™", actlTitle);
		}
	}

	public static void openPasswordPage(String mainURL) {
		// browser.findElement(By.partialLinkText("Personal details")).click();
		String editPersonalDetailsURL = mainURL + "/jsp/pages/changePass.jsf";
		browser.get(editPersonalDetailsURL);
//		String title = "Change password - anyoption™";
//		String expectedText = title.trim();
//		String actualTitle = browser.getTitle().trim();
	}

	public static void logout(final String titleAfterLogoutWait,
			String titleAfterLogout) {
		Browser.clickLogoutButton();
		// wait up to 30 seconds to log out
		(new WebDriverWait(browser, 30))
				.until(new ExpectedCondition<Boolean>() {
					public Boolean apply(WebDriver d) {
						return d.getTitle().toLowerCase()
								.startsWith(titleAfterLogoutWait.toLowerCase());
					}
				});

		AssertJUnit.assertEquals(titleAfterLogout, browser.getTitle());

	}

}
