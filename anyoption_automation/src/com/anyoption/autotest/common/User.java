package com.anyoption.autotest.common;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.AssertJUnit;

import com.anyoption.autotest.utils.RandomStringGenerator;
import com.anyoption.autotest.utils.RandomStringGenerator.Mode;

public class User {

	private static StringBuffer verificationErrors = new StringBuffer();
	public static String eMail;
	public static String firstName;
	public static String lastName;
	public static String phone;
	public static String skinId;
	public static String languageID;
	public static final String password = "123456";

	public enum Skin {
		EN, ES, TR, DE, RU, IT, FR, SG, KR, ET, SE, NL
	}

	public enum Language {
		EN, ES, TR, DE, RU, IT, FR, SG, KR, ET
	}

	public enum InvestmentTypeEnums {
		CALL("1"), PUT("2"), ONETOUCH("3"), BUY("4"), SELL("5");

		private String investTypeId;

		private InvestmentTypeEnums(String investTypeId) {
			this.investTypeId = investTypeId;
		}

		public String getInvestmentTypeId() {
			return this.investTypeId;
		}
	}
	/**
	 * Updates user to tester
	 * @param email
	 * @param userId
	 * @throws Exception
	 */
	public static void updateUserToTester(String email, String userId) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlUpdate = "UPDATE users SET class_id = 0 where email LIKE '"+ email + "' AND id = " + userId;
		String sqlCommit = "commit";
		dB.executeDBQuery(sqlUpdate, false);
		dB.executeDBQuery(sqlCommit, false);
	}
	
	public static void checkAndUpdateUserIdToTesterInDB(String eMail)
			throws Exception {
		DBLayer dB = new DBLayer();
		String sqlQuery = "SELECT * FROM users WHERE email = '" + eMail + "'";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlQuery, false, 2);

		if (results.size() == 1) {
			System.out.println("The user is successfuly created! ");
			String sqlUpdate = "UPDATE users SET class_id = 0 where email = '"
					+ eMail + "'";
			String sqlCommit = "commit";
			dB.executeDBQuery(sqlUpdate, false);
			dB.executeDBQuery(sqlCommit, false);
			System.out
					.println("The user's CLASS_ID is successfuly updated to 0! ");
		} else {
			System.out.println("No results in DB for the user with e-mail: "
					+ eMail);
		}
	}

	public static ArrayList<String[]> getUserDataFromDBAfterCreationfAccount(String eMail) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT balance, user_name, "
				+ "first_name, last_name, is_active, "
				+ "email, is_contact_by_email, class_id, "
				+ "language_id, skin_id, is_accepted_terms"
				+ " FROM users WHERE email = '" + eMail + "'";

		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		return results;
	}

	public static ArrayList<String[]> getContactsDataFromDBAfterCreationfAccount(
			String eMail) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT name, type, skin_id, "
				+ "country_id, first_name, last_name, "
				+ "class_id, mobile_phone" + " FROM contacts WHERE email = '"
				+ eMail + "'";

		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		return results;
	}
	
	public static void compareUserDataAfterCreationOfAccount(String eMail, String firstName, String lastName, Skin skin) throws Exception {
		ArrayList<String[]> resultsForUser = getUserDataFromDBAfterCreationfAccount(eMail);
		System.out.println("Verifying user info for the created player...");
		switch (skin) {
		// EN, ES, TR, DE, RU, IT, FR, SG, KR, ET,  SE
		case EN:
			skinId = "16";
			languageID = "2";
			break;
		case ES:
			skinId = "18";
			languageID = "5";
			break;
		case TR:
			skinId = "3";
			languageID = "3";
			break;
		case DE:
			skinId = "19";
			languageID = "8";
			break;
		case RU:
			skinId = "10";
			languageID = "4";
			break;
		case IT:
			skinId = "20";
			languageID = "6";
			break;
		case FR:
			skinId = "21";
			languageID = "9";
			break;
		case SG:
			skinId = "15";
			languageID = "10";
			break;
		case KR:
			skinId = "17";
			languageID = "11";
			break;
		case ET:
			skinId = "1";
			languageID = "1";
			break;
		case SE:
			skinId = "24";
			languageID = "13";
			break;	
		case NL:
			skinId = "23";
			languageID = "12";
			break;
		default:
			System.out.println("Incorrect skin is set for verification!!");
			break;
		}

		AssertJUnit.assertEquals("Check user's balance", "0",
				resultsForUser.get(0)[0]);
		AssertJUnit.assertEquals("Check the username", eMail.toUpperCase(),
				resultsForUser.get(0)[1]);
		AssertJUnit.assertEquals("Check the first name", firstName,
				resultsForUser.get(0)[2]);
		AssertJUnit.assertEquals("Check the last name", lastName,
				resultsForUser.get(0)[3]);
		AssertJUnit.assertEquals("Check the active", "1",
				resultsForUser.get(0)[4]);
		AssertJUnit.assertEquals("Check user's email", eMail,
				resultsForUser.get(0)[5]);
		AssertJUnit.assertEquals("Check the contact by email value", "1",
				resultsForUser.get(0)[6]);
		AssertJUnit.assertEquals("Check the class_id", "1",
				resultsForUser.get(0)[7]);
		AssertJUnit.assertEquals("Check the language_id", languageID,
				resultsForUser.get(0)[8]);
		AssertJUnit.assertEquals("Check the skin_id", skinId,
				resultsForUser.get(0)[9]);
		AssertJUnit.assertEquals("Check the is_accepted_terms", "1",
				resultsForUser.get(0)[10]);
	}

	public static void compareUserDataAfterCreationOfAccount(String eMail, String firstName, String lastName, String idSkin, 
			String idLanguage) throws Exception {
		ArrayList<String[]> resultsForUser = getUserDataFromDBAfterCreationfAccount(eMail);
		System.out.println("Verifying user info for the created player...");

		AssertJUnit.assertEquals("Check user's balance", "0",
				resultsForUser.get(0)[0]);
		AssertJUnit.assertEquals("Check the username", eMail.toUpperCase(),
				resultsForUser.get(0)[1]);
		AssertJUnit.assertEquals("Check the first name", firstName,
				resultsForUser.get(0)[2]);
		AssertJUnit.assertEquals("Check the last name", lastName,
				resultsForUser.get(0)[3]);
		AssertJUnit.assertEquals("Check the active", "1",
				resultsForUser.get(0)[4]);
		AssertJUnit.assertEquals("Check user's email", eMail,
				resultsForUser.get(0)[5]);
		AssertJUnit.assertEquals("Check the contact by email value", "1",
				resultsForUser.get(0)[6]);
		AssertJUnit.assertEquals("Check the class_id", "1",
				resultsForUser.get(0)[7]);
		AssertJUnit.assertEquals("Check the language_id", idLanguage,
				resultsForUser.get(0)[8]);
		AssertJUnit.assertEquals("Check the skin_id", idSkin,
				resultsForUser.get(0)[9]);
		AssertJUnit.assertEquals("Check the is_accepted_terms", "1",
				resultsForUser.get(0)[10]);
	}

	public static void compareContactsDataAfterCreationOfAccount(String eMail,
			String firstName, String lastName, Skin skin) throws Exception {
		ArrayList<String[]> resultsForUser = getContactsDataFromDBAfterCreationfAccount(eMail);
		System.out.println("Verifying contacts info for the created player...");
		switch (skin) {
		// EN, ES, TR, DE, RU, IT, FR, SG, KR, ET
		case EN:
			skinId = "16";
			languageID = "2";
			break;
		case ES:
			skinId = "18";
			languageID = "5";
			break;
		case TR:
			skinId = "3";
			languageID = "3";
			break;
		case DE:
			skinId = "19";
			languageID = "8";
			break;
		case RU:
			skinId = "10";
			languageID = "4";
			break;
		case IT:
			skinId = "20";
			languageID = "6";
			break;
		case FR:
			skinId = "21";
			languageID = "9";
			break;
		case SG:
			skinId = "15";
			languageID = "10";
			break;
		case KR:
			skinId = "17";
			languageID = "11";
			break;
		case ET:
			skinId = "1";
			languageID = "1";
			break;
		default:
			System.out.println("Incorrect skin is set for verification!!");
			break;
		}

		AssertJUnit.assertEquals("Check user's name", firstName + " "
				+ lastName, resultsForUser.get(0)[0]);
		AssertJUnit.assertEquals("Check type value", "7",
				resultsForUser.get(0)[1]);
		AssertJUnit.assertEquals("Check the skin_id", skinId,
				resultsForUser.get(0)[2]);

		AssertJUnit.assertEquals("Check the first name", firstName,
				resultsForUser.get(0)[4]);
		AssertJUnit.assertEquals("Check the last name", lastName,
				resultsForUser.get(0)[5]);
		AssertJUnit.assertEquals("Check the class_id", "1",
				resultsForUser.get(0)[6]);
		AssertJUnit.assertEquals("Check the mobile phone", phone,
				resultsForUser.get(0)[7]);
	}
	
	/**
	 * Verifies users data in data base
	 * @param eMail
	 * @param firstName
	 * @param lastName
	 * @param skin
	 * @param phoneNumber
	 * @throws Exception
	 */
	public static void compareContactsDataAfterCreationOfAccount(String eMail, String firstName, String lastName, 
			Skin skin, String phoneNumber) throws Exception {
		ArrayList<String[]> resultsForUser = getContactsDataFromDBAfterCreationfAccount(eMail);
		System.out.println("Verifying contacts info for the created player...");
		String skinId = null;
		switch (skin) {
		// EN, ES, TR, DE, RU, IT, FR, SG, KR, ET
		case EN:
			skinId = "16";
			break;
		case ES:
			skinId = "18";
			break;
		case TR:
			skinId = "3";
			break;
		case DE:
			skinId = "19";
			break;
		case RU:
			skinId = "10";
			break;
		case IT:
			skinId = "20";
			break;
		case FR:
			skinId = "21";
			break;
		case SG:
			skinId = "15";
			break;
		case KR:
			skinId = "17";
			break;
		case ET:
			skinId = "1";
			break;
		case SE:
			skinId = "24";
			break;
		case NL:
			skinId = "23";
			break;	
		default:
			System.out.println("Incorrect skin is set for verification!!");
			break;
		}
		Assert.assertEquals(firstName + " " + lastName, resultsForUser.get(0)[0]);
		Assert.assertEquals("7", resultsForUser.get(0)[1]);
		Assert.assertEquals(skinId, resultsForUser.get(0)[2]);
		Assert.assertEquals(firstName, resultsForUser.get(0)[4]);
		Assert.assertEquals(lastName, resultsForUser.get(0)[5]);
		Assert.assertEquals("1", resultsForUser.get(0)[6]);
		Assert.assertEquals(phoneNumber, resultsForUser.get(0)[7]);
	}

	public static void compareContactsDataAfterCreationOfAccount(String eMail, String firstName, String lastName, String skinId) throws Exception {
		ArrayList<String[]> resultsForUser = getContactsDataFromDBAfterCreationfAccount(eMail);
		System.out.println("Verifying contacts info for the created player...");

		AssertJUnit.assertEquals("Check user's name", firstName + " "
				+ lastName, resultsForUser.get(0)[0]);
		AssertJUnit.assertEquals("Check type value", "7",
				resultsForUser.get(0)[1]);
		AssertJUnit.assertEquals("Check the skin_id", skinId,
				resultsForUser.get(0)[2]);
		AssertJUnit.assertEquals("Check the first name", firstName,
				resultsForUser.get(0)[4]);
		AssertJUnit.assertEquals("Check the last name", lastName,
				resultsForUser.get(0)[5]);
		AssertJUnit.assertEquals("Check the class_id", "1",
				resultsForUser.get(0)[6]);
		AssertJUnit.assertEquals("Check the mobile phone", phone,
				resultsForUser.get(0)[7]);
	}

	public static void generateValidUserDataForOpenAccount() throws Exception {
		String randomString = RandomStringGenerator.generateRandomString(6,
				Mode.CAPITAL_LETTERS);
		String randomNumeric = RandomStringGenerator.generateRandomString(8,
				Mode.NUMERIC);
		firstName = "Autotest" + randomString;
		lastName = "Family" + randomString;
		eMail = firstName + "@anyoption.com";
		phone = randomNumeric;

		System.out.println("The generated first name is: " + firstName);
		System.out.println("The generated last name is: " + lastName);
		System.out.println("The generated e-mail is: " + eMail);
		System.out.println("The generated phone number is: " + phone);
		System.out.println("The password is: " + password);
	}

	public static void checkUserGreeting(WebDriver browser, Skin skin) {
		Browser.switchToiframe(browser);
		String helloUserName = browser.findElement(By.id("loginStrip_userName")).getText(); 
		System.out.println("The text in the header frame is: " + helloUserName);

		try {
			if (skin == Skin.EN) {
				AssertJUnit.assertEquals("Hello, " + User.firstName,
						helloUserName);
			} else if (skin == Skin.ES) {
				AssertJUnit.assertEquals("Hola: " + User.firstName,
						helloUserName);
			} else if (skin == Skin.DE) {
				AssertJUnit.assertEquals("Hallo " + User.firstName,
						helloUserName);
			} else if (skin == Skin.TR) {
				AssertJUnit.assertEquals("Merhaba, " + User.firstName,
						helloUserName);
			} else if (skin == Skin.IT) {
				AssertJUnit.assertEquals("Salve, " + User.firstName,
						helloUserName);
			} else if (skin == Skin.FR) {
				AssertJUnit.assertEquals("Bonjour, " + User.firstName,
						helloUserName);
			} else if (skin == Skin.RU) {
				AssertJUnit.assertEquals("Р—РґСЂР°РІСЃС‚РІСѓР№С‚Рµ, " + User.firstName,
						helloUserName);
			} else if (skin == Skin.SG) {
				AssertJUnit
						.assertEquals("ж‚ЁеҐЅ, " + User.firstName, helloUserName);
			} else if (skin == Skin.KR) {
				AssertJUnit.assertEquals("м•€л…•н•�м„ёмљ” " + User.firstName,
						helloUserName);
			} else if (skin == Skin.ET) {
				AssertJUnit.assertEquals("Ч©ЧњЧ•Чќ " + User.firstName,
						helloUserName);
			} else {
				System.out.println("Please enter a valid skin!");
			}
		} catch (Exception e) {
			verificationErrors.append(e.toString());
		}
		browser.switchTo().defaultContent();
	}

	public static void checkUserGreeting(WebDriver browser, String userGreeting) {
		Browser.switchToiframe(browser);
		String helloUserName = browser
				.findElement(By.id("loginStrip_userName")).getText(); // cssSelector("div.fL span.header_user_name")
		System.out.println("The greeting in the header frame is: "
				+ helloUserName);
		String expectedGreeting = userGreeting + " " + User.firstName;
		System.out
				.println("The expected greeting in the header frame should be: "
						+ helloUserName);
		try {
			AssertJUnit.assertEquals(expectedGreeting, helloUserName);
		} catch (Exception e) {
			verificationErrors.append(e.toString());
		}
		browser.switchTo().defaultContent();
	}

	public static void checkUserBalanceAfterCreationOfAccount(WebDriver browser, Skin skin) {
		Browser.switchToiframe(browser);
		String balance = browser.findElement(By.id("loginStrip_balance")).getText();
		System.out.println("The balance is: " + balance);
		try {
			if (skin == Skin.EN) {
				AssertJUnit.assertEquals("$0.00", balance);
			} else if (skin == Skin.ES || skin == Skin.DE || skin == Skin.IT
					|| skin == Skin.FR) {
				AssertJUnit.assertEquals("в‚¬0.00", balance);
			} else if (skin == Skin.TR) {
				AssertJUnit.assertEquals("0.00TL", balance);
			} else if (skin == Skin.RU) {
				AssertJUnit.assertEquals("0.00СЂСѓР±.", balance);
			} else if (skin == Skin.SG) {
				AssertJUnit.assertEquals("ВҐ0.00", balance);
			} else if (skin == Skin.KR) {
				AssertJUnit.assertEquals("в‚©0", balance);
			} else if (skin == Skin.ET) {
				AssertJUnit.assertEquals("в‚Є0.00", balance);
			} else {
				System.out.println("Please enter a valid skin!");
			}
		} catch (Exception e) {
			verificationErrors.append(e.toString());
		}
		browser.switchTo().defaultContent();
	}

	public static void checkUserBalanceAfterCreationOfAccount(WebDriver browser, String userBalance) {
		Browser.switchToiframe(browser);
		String balance = browser.findElement(By.id("loginStrip_balance")).getText();
		System.out.println("The balance in web page is: " + balance);

		AssertJUnit.assertEquals("Verify the user balance: ", userBalance, balance);

		browser.switchTo().defaultContent();
	}

	public static String getCurrencySign(String skin) {
		String currencySign = "";
		if (skin.equalsIgnoreCase("EN")) {
			currencySign = "$";
		} else if (skin.equalsIgnoreCase("TR")) {
			currencySign = "TL";
		} else if (skin.equalsIgnoreCase("RU")) {
			currencySign = "СЂСѓР±.";
		} else if (skin.equalsIgnoreCase("KR")) {
			currencySign = "в‚©";
		} else if (skin.equalsIgnoreCase("GBP")) {
			currencySign = "ВЈ";
		}// else if (skin == "Yen") {
			// currencySign = "ВҐ";
		else {
			currencySign = "в‚¬";
		}
		return currencySign;
	}

	public static String getUserCurrencySign(String eMail) throws Exception {
		String currencyId = getUserCurrencyIdFromDB(eMail);
		String currencySign = "";
		if (currencyId.equals("2")) {
			currencySign = "$";
		} else if (currencyId.equals("5")) {
			currencySign = "TL";
		} else if (currencyId.equals("6")) {
			currencySign = "СЂСѓР±.";
		} else if (currencyId.equals("8")) {
			currencySign = "в‚©";
		} else if (currencyId.equals("4")) {
			currencySign = "ВЈ";
		}// else if (skin == "Yen") {
			// currencySign = "ВҐ";
		else {
			currencySign = "в‚¬";
		}
		return currencySign;
	}

	public static void compareUserBalanceAfterDeposit(WebDriver browser,String skin, String depositAmount, String eMail,
			String initialBalance) throws Exception {
		String actualBalanceInWeb = getUserBalanceFromWeb(browser);
		System.out.println("The balance in WEB is: " + actualBalanceInWeb);

		String userBalanceAfterDepositInDB = getUserBalanceFromDB(eMail);
		System.out.println("The balance in DB is: " + userBalanceAfterDepositInDB);

		// TODO: The bonus should be added to the expected balance
		String actBalanceInWebWithoutCurrencySign = removeCurrencySign(actualBalanceInWeb);
		double actUserBalanceInWeb = Double.parseDouble(actBalanceInWebWithoutCurrencySign);

		String actBalanceInDBWithoutCurrencySign = removeCurrencySign(actualBalanceInWeb);
		double actBalanceInDB = Double.parseDouble(actBalanceInDBWithoutCurrencySign);

		String userInitBalance = removeCurrencySign(initialBalance);
		System.out.println("The initial balance without currency: " + userInitBalance);
		double userInitialBalance = Double.parseDouble(userInitBalance);

		double deposit = Double.parseDouble(depositAmount);

		double expectedUserBalance = userInitialBalance + deposit;
		System.out.println("The expected balance without currency sign is: " + expectedUserBalance);
		try {
			AssertJUnit.assertEquals(
					"Compare expected user balance with the balance in WEB",
					expectedUserBalance, actUserBalanceInWeb);
		} catch (Exception e) {
			verificationErrors.append(e.toString());
		}
		try {
			AssertJUnit.assertEquals(
					"Compare expected user balance with the balance in DB",
					expectedUserBalance, actBalanceInDB);
		} catch (Exception e) {
			verificationErrors.append(e.toString());
		}
	}

	public static void compareUserBalanceAfterWithdraw(WebDriver browser, String withdrawAmount, String eMail, 
			String initialBalance) throws Exception {
		String actualBalanceInWeb = getUserBalanceFromWeb(browser);
		System.out.println("The balance in WEB is: " + actualBalanceInWeb);

		String userBalanceAfterWithdrawInDB = getUserBalanceFromDB(eMail);
		System.out.println("The balance in DB is: " + userBalanceAfterWithdrawInDB);

		// TODO: The bonus should be added to the expected balance
		String actBalanceInWebWithoutCurrencySign = removeCurrencySign(actualBalanceInWeb);
		double actUserBalanceInWeb = Double.parseDouble(actBalanceInWebWithoutCurrencySign);

		String actBalanceInDBWithoutCurrencySign = removeCurrencySign(actualBalanceInWeb);
		double actBalanceInDB = Double.parseDouble(actBalanceInDBWithoutCurrencySign);

		String userInitBalance = removeCurrencySign(initialBalance);
		System.out.println("The initial balance without currency: " + userInitBalance);
		double userInitialBalance = Double.parseDouble(userInitBalance);

		double deposit = Double.parseDouble(withdrawAmount);

		double expectedUserBalance = userInitialBalance - deposit;
		System.out.println("The expected balance without currency sign is: " + expectedUserBalance);
		try {
			AssertJUnit.assertEquals(
					"Compare expected user balance with the balance in WEB",
					expectedUserBalance, actUserBalanceInWeb);
		} catch (Exception e) {
			verificationErrors.append(e.toString());
		}
		try {
			AssertJUnit.assertEquals(
					"Compare expected user balance with the balance in DB",
					expectedUserBalance, actBalanceInDB);
		} catch (Exception e) {
			verificationErrors.append(e.toString());
		}
	}

	public static void compareUserBalanceAfterReverseWithdraw(WebDriver browser, String reverseWithdrawAmount, String eMail,
			String initialBalance) throws Exception {
		String actualBalanceInWeb = getUserBalanceFromWeb(browser);
		System.out.println("The balance in WEB is: " + actualBalanceInWeb);

		String userBalanceAfterWithdrawInDB = getUserBalanceFromDB(eMail);
		System.out.println("The balance in DB is: " + userBalanceAfterWithdrawInDB);

		// TODO: The bonus should be added to the expected balance
		String actBalanceInWebWithoutCurrencySign = removeCurrencySign(actualBalanceInWeb);
		double actUserBalanceInWeb = Double.parseDouble(actBalanceInWebWithoutCurrencySign);

		String actBalanceInDBWithoutCurrencySign = removeCurrencySign(actualBalanceInWeb);
		double actBalanceInDB = Double.parseDouble(actBalanceInDBWithoutCurrencySign);

		String userInitBalance = removeCurrencySign(initialBalance);
		System.out.println("The initial balance without currency: "	+ userInitBalance);
		double userInitialBalance = Double.parseDouble(userInitBalance);

		double reverseWithdraw = Double.parseDouble(reverseWithdrawAmount);

		double expectedUserBalance = userInitialBalance + reverseWithdraw;
		System.out.println("The expected balance without currency sign is: " + expectedUserBalance);
		try {
			AssertJUnit.assertEquals(
					"Compare expected user balance with the balance in WEB",
					expectedUserBalance, actUserBalanceInWeb);
		} catch (Exception e) {
			verificationErrors.append(e.toString());
		}
		try {
			AssertJUnit.assertEquals(
					"Compare expected user balance with the balance in DB",
					expectedUserBalance, actBalanceInDB);
		} catch (Exception e) {
			verificationErrors.append(e.toString());
		}
	}

	public static void compareUserBalanceAfterInvestment(WebDriver browser, String skin, String investAmount, String eMail,
			String initialBalance) throws Exception {
		String actualBalanceInWeb = getUserBalanceFromWeb(browser);
		System.out.println("The balance in WEB is: " + actualBalanceInWeb);

		String userBalanceAfterDepositInDB = getUserBalanceFromDB(eMail);
		System.out.println("The balance in DB is: " + userBalanceAfterDepositInDB);
		String actBalanceInWebWithoutCurrencySign = removeCurrencySign(actualBalanceInWeb);
		double actUserBalanceInWeb = Double.parseDouble(actBalanceInWebWithoutCurrencySign);

		String actBalanceInDBWithoutCurrencySign = removeCurrencySign(actualBalanceInWeb);
		double actBalanceInDB = Double.parseDouble(actBalanceInDBWithoutCurrencySign);

		String userInitBalance = removeCurrencySign(initialBalance);
		System.out.println("The initial balance without currency: " + userInitBalance);
		double userInitialBalance = Double.parseDouble(userInitBalance);

		double invest = Double.parseDouble(investAmount);

		double expectedUserBalance = userInitialBalance - invest;
		System.out.println("The expected balance after the investment without currency sign is: "
						+ expectedUserBalance);
		try {
			AssertJUnit.assertEquals(
					"Compare expected user balance with the balance in WEB",
					expectedUserBalance, actUserBalanceInWeb);
		} catch (Exception e) {
			verificationErrors.append(e.toString());
		}
		try {
			AssertJUnit.assertEquals(
					"Compare expected user balance with the balance in DB",
					expectedUserBalance, actBalanceInDB);
		} catch (Exception e) {
			verificationErrors.append(e.toString());
		}
	}

	public static String removeCurrencySign(String balanceWithCurrencySign) {
		String balanceWithoutCurrencySign = "";
		balanceWithoutCurrencySign = balanceWithCurrencySign
				.replaceAll("\\$", "").replaceAll("ВЈ", "").replaceAll("в‚¬", "")
				.replaceAll("в‚©", "").replaceAll("TL", "")
				.replaceAll("СЂСѓР±.", "").replaceAll(",", "");
		System.out.println(balanceWithoutCurrencySign);
		return balanceWithoutCurrencySign;
	}

	public static void checkDepositPageHeaders(WebDriver browser, Skin skin) {
		String newDeposith1 = browser.findElement(By.id("NewDeposit_h1")).getText();
		try {
			if (skin == Skin.EN) {
				AssertJUnit
						.assertEquals(
								"You are about to enter the exciting world of trading!",
								newDeposith1);
			} else if (skin == Skin.ES) {
				AssertJUnit
						.assertEquals(
								"ВЎUsted estГЎ a punto de entrar en el apasionante mundo de la inversiГіn!",
								newDeposith1);
			} else if (skin == Skin.DE) {
				AssertJUnit
						.assertEquals(
								"Sie sind im Begriff, die spannende Welt des Tradings zu betreten!",
								newDeposith1);
			} else if (skin == Skin.TR) {
				AssertJUnit
						.assertEquals(
								"Heyecan dolu bir yatД±rД±m dГјnyasД±na girmek Гјzeresiniz.",
								newDeposith1);
			} else if (skin == Skin.IT) {
				AssertJUnit.assertEquals(
						"Stai per entrare nell'emozionante mondo del trading!",
						newDeposith1);
			} else if (skin == Skin.FR) {
				AssertJUnit
						.assertEquals(
								"Vous ГЄtes sur le point d'entrer dans le monde fabuleux du trading !",
								newDeposith1);
			} else if (skin == Skin.RU) {
				AssertJUnit.assertEquals(
						"Р”РѕР±СЂРѕ РїРѕР¶Р°Р»РѕРІР°С‚СЊ РІ СѓРІР»РµРєР°С‚РµР»СЊРЅС‹Р№ РјРёСЂ С‚РѕСЂРіРѕРІР»Рё! ",
						newDeposith1);
			} else if (skin == Skin.SG) {
				AssertJUnit.assertEquals("ж‚ЁеЌіе°†иї›е…ҐзІѕеЅ©зљ„дє¤ж�“дё–з•ЊпјЃ", newDeposith1);
			} else if (skin == Skin.KR) {
				AssertJUnit.assertEquals("кі к°ќл‹�мќЂ м§Ђкё€ нќҐлЇёлЎњмљґ нЉёлћ�мќґл”©мќ� м„ёкі„лЎњ мћ…мћҐмќ„ н•�кІЊлђ©л‹€л‹¤!",
						newDeposith1);
			} else if (skin == Skin.ET) {
				AssertJUnit.assertEquals(
						"ЧђЧЄЧ” ЧўЧ•ЧћЧ“ ЧњЧ”Ч›Ч ЧЎ ЧњЧўЧ•ЧњЧќ Ч—Ч“Ч© Ч•ЧћЧЎЧўЧ™ЧЁ Ч©Чњ ЧћЧЎЧ—ЧЁ Ч‘ЧђЧ•Ч¤Ч¦Ч™Ч•ЧЄ",
						newDeposith1);
			} else {
				System.out.println("Please enter a valid skin!");
			}
		} catch (Exception e) {
			verificationErrors.append(e.toString());
		}
	}

	public static void checkDepositPageHeaders(WebDriver browser,
			String newDeposith1, String newDeposith2) {
		String newDepositTextOne = browser.findElement(By.id("NewDeposit_h1")).getText();
		String newDepositTextTwo = browser.findElement(By.id("NewDeposit_h2")).getText();
		try {
			AssertJUnit.assertEquals(newDeposith1, newDepositTextOne);
			AssertJUnit.assertEquals(newDeposith2, newDepositTextTwo);
		} catch (Exception e) {
			verificationErrors.append(e.toString());
		}
	}

	public static void createUser(WebDriver driver, String registerPageTitle,
			String skin, String depositPageTitle, String idSkin,
			String idLanguage, Boolean isTesterAccount) throws Exception {
		User.generateValidUserDataForOpenAccount();

		driver.findElement(By.id("menu_register")).click();

		AssertJUnit.assertEquals(registerPageTitle, driver.getTitle());

		Browser.typeUserGeneratedData(skin);

		Browser.confirmTerms();

		Browser.clickOpenAccountButton();

		// wait up to 30 seconds to create the account and load deposit page
		new WebDriverWait(driver, 30)
				.until(new ExpectedCondition<WebElement>() {
					public WebElement apply(WebDriver d) {
						return d.findElement(By.id("newCardForm"));
					}
				});

		compareUserDataAfterCreationOfAccount(eMail, firstName, lastName,
				idSkin, idLanguage);

		if (isTesterAccount)
			checkAndUpdateUserIdToTesterInDB(eMail);
	}
	/**
	 * Get balance from data base
	 * @param eMail - String
	 * @param userId - String
	 */
	public static String getUserBalanceFromDB(String eMail, String userId) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT balance FROM users WHERE email = '"+ eMail + "' AND id = " + userId;
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		String actBal = results.get(0)[0];
		return actBal;
	}	
	

	public static String getUserBalanceFromDB(String eMail) throws Exception {
		String userBalanceInDB = "";
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT balance" + " FROM users WHERE email = '" + eMail + "'";

		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		String actBal = results.get(0)[0];
		String displayFormat = "###,###,##0.00";
		DecimalFormat sd = new DecimalFormat(displayFormat,
				new DecimalFormatSymbols(Locale.US));
		double userBalance = Double.parseDouble(actBal);
		userBalance /= 100;
		userBalanceInDB = String.valueOf(sd.format(userBalance));
		String currencySign = getUserCurrencySign(eMail);
		String userCurrencyId = getUserCurrencyIdFromDB(eMail);
		if (userCurrencyId.equals("5") || userCurrencyId.equals("6")) /*
																	 * where 5
																	 * is TL
																	 * (Turkish
																	 * lira) and
																	 * 6 is СЂСѓР±.
																	 */{
			userBalanceInDB = userBalanceInDB + currencySign;
		} else {
			userBalanceInDB = currencySign + userBalanceInDB;
		}
		return userBalanceInDB;
	}
	
	public static String getUserCurrencyIdFromDB(String eMail) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT currency_id" + " FROM users WHERE email = '" + eMail + "'";

		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		String currencyId = results.get(0)[0];
		return currencyId;
	}

	public static String getUserBalanceFromWeb(WebDriver browser) {
		String userBalanceInWeb = "";
		Browser.switchToiframe(browser);
		userBalanceInWeb = browser.findElement(By.id("loginStrip_balance")).getText();
		System.out.println("The user balance in web page is: " + userBalanceInWeb);
		browser.switchTo().defaultContent();
		return userBalanceInWeb;
	}

	public static ArrayList<String[]> getUserId(String email) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT id" + " FROM users WHERE email = '" + email + "'";

		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		return results;
	}
	
	public static ArrayList<String[]> getUserVisibleCreditCards(String email)
			throws Exception {
		DBLayer dB = new DBLayer();

		ArrayList<String[]> resultsForUserID = getUserId(email);
		String userID = resultsForUserID.get(0)[0];
		String sqlRequest = "SELECT id, type_id, is_visible, exp_month, exp_year, cc_pass, bin FROM credit_cards WHERE user_id = '"
				+ userID + "' and is_visible = '1'";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 1);
		return results;
	}

	public static void allowCreditCard(String eMail) throws Exception {
		DBLayer dB = new DBLayer();
		ArrayList<String[]> resultsForCreditCards = getUserVisibleCreditCards(eMail);
		if (resultsForCreditCards.size() != 0) {
			ArrayList<String[]> resultsForUserID = getUserId(eMail);
			String userID = resultsForUserID.get(0)[0];
			String sqlUpdate = "UPDATE credit_cards SET is_allowed = 1 WHERE user_id = '" + userID + "'";
			String sqlCommit = "commit";
			dB.executeDBQuery(sqlUpdate, false);
			dB.executeDBQuery(sqlCommit, false);
			System.out.println("The user's credit card is successfuly updated (is_allowed = 1)! ");
		}
	}

	public static void makeDepositAfterCreationOfAccount(WebDriver driver,
			String skin, String firstName, String depositAmount,
			String testEnvURL, String depositPageTitle, String eMail)
			throws Exception {
		Browser.fillFirstCreditCardDetails(firstName, depositAmount);
		driver.findElement(By.id("btn_newCard")).click();
		if (driver.findElement(By.id("btn_newCard")).isDisplayed()) {
			User.allowCreditCard(eMail);
		}
		depositWithExistingCard(driver, skin, depositAmount, testEnvURL,
				depositPageTitle, eMail);
	}

	public static void depositWithExistingCard(WebDriver driver, String skin,
			String depositAmount, String testEnvURL, String depositPageTitle,
			String eMail) throws Exception {
		Browser.openDepositPage(testEnvURL, depositPageTitle);
		if (skin.equalsIgnoreCase("KR")) {
			ArrayList<String[]> resultsForCreditCards = getUserVisibleCreditCards(eMail);
			if (resultsForCreditCards.size() != 0) {
				String userCreditCardIDInDB = resultsForCreditCards.get(0)[0];
				System.out.println("The user's credit card ID is: "
						+ userCreditCardIDInDB);
				new Select(driver.findElement(By.id("depositForm:ccNum")))
						.selectByValue(userCreditCardIDInDB); // 51576 works
																// only for the
																// user
				// with id
				// = 146436 who has added Visa
				// credit card
			} else {
				System.out.println("No existing credit cards for the user!");
				AssertJUnit.fail("No existing credit cards for the user!");
			}
		} else {
			new Select(driver.findElement(By.id("depositForm:ccNum")))
					.selectByVisibleText("0000 " + "Visa"); // "1111 "
		}

		driver.findElement(By.id("depositForm:deposit")).click();
		driver.findElement(By.id("depositForm:deposit")).clear();
		driver.findElement(By.id("depositForm:deposit"))
				.sendKeys(depositAmount);
		driver.findElement(By.id("btn_oldCard")).click();
	}

	public static void editCreditCard(WebDriver driver, String depositAmount,
			String testEnvURL, String depositPageTitle,
			String editDeleteCreditCardsPageTitle) throws Exception {
		Browser.openDepositPage(testEnvURL, depositPageTitle);
		Browser.openEditCreditCardPage(driver, testEnvURL, depositPageTitle,
				editDeleteCreditCardsPageTitle);
		driver.findElement(By.partialLinkText("Edit card ")).click();
		// driver.findElement(By.partialLinkText(linkText))

		ArrayList<String[]> resultsForCreditCards = getUserVisibleCreditCards(eMail);
		if (resultsForCreditCards.size() != 0) {
			String userCreditCardIDInDB = resultsForCreditCards.get(0)[0];
			System.out.println("The user's credit card ID is: "
					+ userCreditCardIDInDB);
			new Select(driver.findElement(By.id("depositForm:ccNum")))
					.selectByValue(userCreditCardIDInDB);
		}

		driver.findElement(By.id("depositForm:deposit")).click();
		driver.findElement(By.id("depositForm:deposit")).clear();
		driver.findElement(By.id("depositForm:deposit"))
				.sendKeys(depositAmount);
		driver.findElement(By.id("btn_oldCard")).click();
	}

	public static void deleteCreditCard(WebDriver driver, String skin,
			String depositAmount, String testEnvURL, String depositPageTitle,
			String eMail) throws Exception {
		driver.findElement(By.partialLinkText("Edit card ")).click();
		driver.findElement(By.id("cardsForm:data:0:select")).click();

		driver.findElement(By.id("depositForm:deposit")).click();
		driver.findElement(By.id("depositForm:deposit")).clear();
		driver.findElement(By.id("depositForm:deposit"))
				.sendKeys(depositAmount);
		driver.findElement(By.id("btn_oldCard")).click();
	}

	public static void makeBOInvestmentAfterLogin(WebDriver driver,
			String investmentAmount, String skin, String userInitialBalance,
			String eMail, InvestmentTypeEnums typeId) throws Exception {
		driver.findElement(By.id("tradeBox_inv_amount_0")).click();
		driver.findElement(By.id("tradeBox_invest_drop_custom_0")).click();
		driver.findElement(By.id("tradeBox_invest_drop_custom_0")).clear();
		driver.findElement(By.id("tradeBox_invest_drop_custom_0")).sendKeys(
				investmentAmount);
		driver.findElement(By.id("tradeBox_call_btn_text_0")).click();
		Thread.sleep(2 /* sec */* 1000);
		String investLevelInTheSlip = driver.findElement(
				By.id("tradeBox_AI_level_0")).getText();
		String amount = driver.findElement(By.id("tradeBox_AI_amount_0"))
				.getText();
		if (investLevelInTheSlip.isEmpty()) {
			AssertJUnit
					.fail("Investment slip does NOT show! Maybe the investment is not successfully made!");
		} else {
			String userBalanceAfterInvestment = getUserBalanceFromDB(eMail);
			System.out.println("User balance after the investment of " + amount
					+ " on level: " + investLevelInTheSlip + " is: "
					+ userBalanceAfterInvestment);
			User.compareUserBalanceAfterInvestment(driver, skin,
					investmentAmount, eMail, userInitialBalance);
			compareInvestmentData(eMail, investLevelInTheSlip,
					investmentAmount, typeId);
		}
	}

	public static void compareInvestmentData(String eMail,
			String investLevelInTheSlip, String investmentAmountInTheSlip,
			InvestmentTypeEnums typeId) throws Exception {
		ArrayList<String[]> resultsForUser = getInvestmentDataFromDB(eMail);
		System.out.println("Verifying investment data in DB...");

		AssertJUnit.assertEquals("Check the investment type id",
				typeId.investTypeId, resultsForUser.get(0)[0]);
		String displayInvestFormat = "##0";
		DecimalFormat dfa = new DecimalFormat(displayInvestFormat,
				new DecimalFormatSymbols(Locale.US));
		double expInvestAmount = Double.parseDouble(investmentAmountInTheSlip);
		expInvestAmount *= 100;
		String expectedInvestAmount = String.valueOf(dfa
				.format(expInvestAmount));
		String actualInvestAmount = resultsForUser.get(0)[1];
		AssertJUnit.assertEquals("Check the investment amount",
				expectedInvestAmount, actualInvestAmount);
		investLevelInTheSlip = investLevelInTheSlip.replaceAll(",", "");
		double expInvestLevel = Double.parseDouble(investLevelInTheSlip);
		String displayLevelFormat = "#########.#####";
		DecimalFormat dfl = new DecimalFormat(displayLevelFormat,
				new DecimalFormatSymbols(Locale.US));
		String expectedInvestLevel = String.valueOf(dfl.format(expInvestLevel));
		String actualInvestLevel = resultsForUser.get(0)[2];
		AssertJUnit.assertEquals("Check the investment level",
				expectedInvestLevel, actualInvestLevel);
		AssertJUnit.assertEquals("Check the odds win", ".7",
				resultsForUser.get(0)[3]);
		AssertJUnit.assertEquals("Check the odds lose", ".85",
				resultsForUser.get(0)[4]);
	}

	public static ArrayList<String[]> getInvestmentDataFromDB(String eMail) throws Exception {
		DBLayer dB = new DBLayer();
		ArrayList<String[]> resultsForUserID = getUserId(eMail);
		String userID = resultsForUserID.get(0)[0];
		String sqlRequest = "SELECT type_id, amount, current_level, "
				+ "odds_win, odds_lose, rate "
				+ "FROM investments WHERE user_id = '" + userID + "' "
				+ "order by time_created desc";

		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		return results;
	}

	public static ArrayList<String[]> getOpenOpportunitiesFromDB() throws Exception {
		DBLayer dB = new DBLayer();

		String sqlRequest = "SELECT id, market_id "
				+ "FROM opportunities WHERE current_timestamp > time_first_invest "
				+ "AND current_timestamp < time_last_invest AND is_published = 1 "
				+ "AND opportunity_type_id = 1 AND scheduled = 1 "
				+ "AND is_disabled_service = 0 AND market_id in "
				+ "(select id from markets) order by market_id, time_est_closing";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		return results;
	}

	public static ArrayList<String[]> getClosingLevelFromDB(
			String opportunitiesId) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT a.closing_level FROM opportunities a, opportunities b "
				+ "WHERE b.id = "
				+ opportunitiesId
				+ " AND a.market_id = b.market_id "
				+ "AND (a.time_first_invest = b.time_first_invest - to_dsinterval('1 00:00:00') "
				+ "OR a.time_first_invest = b.time_first_invest - to_dsinterval('2 00:00:00') "
				+ "OR a.time_first_invest = b.time_first_invest - to_dsinterval('3 00:00:00') "
				+ "OR a.time_first_invest = b.time_first_invest - to_dsinterval('4 00:00:00')) "
				+ "AND (a.time_est_closing = b.time_est_closing - to_dsinterval('1 00:00:00') "
				+ "OR a.time_est_closing = b.time_est_closing - to_dsinterval('2 00:00:00') "
				+ "OR a.time_est_closing = b.time_est_closing - to_dsinterval('3 00:00:00') "
				+ "OR a.time_est_closing = b.time_est_closing - to_dsinterval('4 00:00:00'))"
				+ "AND a.opportunity_type_id = 1 AND a.scheduled = 1 AND a.closing_level != 0";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		return results;
	}

	public static ArrayList<String[]> getMarketRatesFromDB(String marketId) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT * FROM (SELECT rate, graph_rate FROM market_rates WHERE market_id = "
				+ marketId
				+ " AND skin_group_id = 2 order by id desc) WHERE rownum <= 1";
		// 16, 136, 137, 251
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		return results;
	}

	public static ArrayList<String[]> checkForSuccessfulFirstDeposit(
			String email) throws Exception {
		DBLayer dB = new DBLayer();
		ArrayList<String[]> resultsForUserID = getUserId(email);
		String userID = resultsForUserID.get(0)[0];
		String sqlRequest = "SELECT * FROM transactions WHERE type_id = '1'"
				+ " AND status_id = '1' AND user_id = '" + userID + "'";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		return results;
	}

	public static ArrayList<String[]> getUserSkinID(String email)
			throws Exception {
		DBLayer dB = new DBLayer();
		ArrayList<String[]> resultsForUserID = getUserId(email);
		String userID = resultsForUserID.get(0)[0];
		String sqlRequest = "SELECT skin_id FROM users WHERE id = '" + userID
				+ "'";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		return results;
	}

	public static ArrayList<String[]> checkIfUserCompleteQuestionary(
			String email) throws Exception {
		DBLayer dB = new DBLayer();
		ArrayList<String[]> resultsForUserID = getUserId(email);
		String userID = resultsForUserID.get(0)[0];
		String sqlRequest = "SELECT * FROM users_regulation WHERE user_id = '"
				+ userID + "' and approved_regulation_step < 3";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		return results;
	}

	public static void updateCreditCardDetails(WebDriver driver,
			String expPageTitleAfterEdit, String expPageTitleAfterDelete,
			ArrayList<String[]> visibleCards, String email) throws Exception {
		// id, type_id, is_visible, exp_month, exp_year, cc_pass, bin

		String cvv = driver.findElement(By.id("cardsForm:ccPass"))
				.getAttribute("value");
		// option selected="selected" value="05"
		String expMonth = driver.findElement(By.id("cardsForm:expMonth"))
				.getAttribute("value");
		String expYear = driver.findElement(By.id("cardsForm:expYear"))
				.getAttribute("value");

		ArrayList<String[]> initialCardDetails = visibleCards;

		int actualCvv = Integer.parseInt(cvv);
		int actualExpMonth = Integer.parseInt(expMonth);
		int actualExpYear = Integer.parseInt(expYear);

		AssertJUnit.assertEquals("Check the exp month",
				initialCardDetails.get(0)[3], expMonth);
		AssertJUnit.assertEquals("Check the exp year",
				initialCardDetails.get(0)[4], expYear);
		AssertJUnit.assertEquals("Check the CVV", initialCardDetails.get(0)[5],
				cvv);

		if (initialCardDetails.get(0)[3].equals("12")) {
			actualExpMonth = actualExpMonth - 1;
		} else {
			actualExpMonth = actualExpMonth + 1;
		}

		if (initialCardDetails.get(0)[4].equals("35")) {
			actualExpYear = actualExpYear - 1;
		} else {
			actualExpYear = actualExpYear + 1;
		}

		if (initialCardDetails.get(0)[5].equals("999")) {
			actualCvv = actualCvv - 1;
		} else {
			actualCvv = actualCvv + 1;
		}

		String newCvv = StringUtils.leftPad(String.valueOf(actualCvv), 3, "0");
		String newExpMonth = StringUtils.leftPad(
				String.valueOf(actualExpMonth), 2, "0");
		String newExpYear = StringUtils.leftPad(String.valueOf(actualExpYear),
				2, "0");

		driver.findElement(By.id("cardsForm:ccPass")).click();
		driver.findElement(By.id("cardsForm:ccPass")).clear();
		driver.findElement(By.id("cardsForm:ccPass")).sendKeys(newCvv);
		new Select(driver.findElement(By.id("cardsForm:expMonth")))
				.selectByVisibleText(newExpMonth);
		new Select(driver.findElement(By.id("cardsForm:expYear")))
				.selectByVisibleText(newExpYear);

		driver.findElement(
				By.xpath("//span[@onclick=\"return myfaces.oam.submitForm('cardsForm','cardsForm:hiddensave');\"]"))
				.click();
		String expectedText = expPageTitleAfterEdit.trim();
		String actualTitle = driver.getTitle().trim();
		AssertJUnit.assertEquals(expectedText, actualTitle);
		AssertJUnit.assertEquals("Card details were updated successfully.",
				driver.findElement(By.cssSelector("td.pages_text > span"))
						.getText());

		ArrayList<String[]> cardDetailsAfterChanges = getUserVisibleCreditCards(email);
		AssertJUnit
				.assertEquals(
						"Check the visibility of credit card after the update in DB",
						initialCardDetails.get(0)[2],
						cardDetailsAfterChanges.get(0)[2]);
		AssertJUnit.assertEquals("Check the exp. month after the update in DB",
				cardDetailsAfterChanges.get(0)[3], newExpMonth);
		AssertJUnit.assertEquals("Check the exp. year after the update in DB",
				cardDetailsAfterChanges.get(0)[4], newExpYear);
		AssertJUnit.assertEquals("Check the CVV after the update in DB",
				cardDetailsAfterChanges.get(0)[5], newCvv);
		driver.findElement(By.id("cardsForm:data:0:select")).click();

		driver.findElement(By.cssSelector("span.send_button.send_but")).click();
		driver.findElement(
				By.xpath("//span[@onclick=\"return myfaces.oam.submitForm('cardsForm','cardsForm:hiddensave');\"]"))
				.click();
		expectedText = expPageTitleAfterDelete.trim();
		actualTitle = driver.getTitle().trim();
		AssertJUnit.assertEquals(expectedText, actualTitle);
	}

	public static ArrayList<String[]> getUserPersonalDetailsFromDB(String eMail)
			throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT  nvl(street,'NA') street, nvl(street_no,'NA') street_no, "
				+ "nvl(zip_code,'NA') zip_code, nvl(city_name,'NA') city_name, "
				+ "country_id, nvl(mobile_phone,'NA') mobile_phone FROM users WHERE email = '"
				+ eMail + "'";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		return results;
	}

	public static ArrayList<String> getPersonalDetailsFromWeb(WebDriver driver,
			String email, String passwrd) throws Exception {
		String street = driver.findElement(By.id("personalForm:street"))
				.getAttribute("value"); // value="Mladost"
		String streetNo = driver.findElement(By.id("personalForm:streetno"))
				.getAttribute("value"); // value=""
		String postCode = driver.findElement(By.id("personalForm:zipCode"))
				.getAttribute("value"); // value="1000"
		String city = driver.findElement(By.id("personalForm:cityName"))
				.getAttribute("value"); // value="Sofia"
		String country = driver.findElement(
				By.id("personalForm:countriesRegulatedList")).getAttribute(
				"value"); // selected="selected",
		// value="33"
		String mobilePhone = driver.findElement(By.id("personalForm:mobilePhone")).getAttribute("value");
		ArrayList<String> personalDetailsInWeb = new ArrayList<String>();
		personalDetailsInWeb.add(0, street);
		personalDetailsInWeb.add(1, streetNo);
		personalDetailsInWeb.add(2, postCode);
		personalDetailsInWeb.add(3, city);
		personalDetailsInWeb.add(4, country);
		personalDetailsInWeb.add(5, mobilePhone);
		return personalDetailsInWeb;
	}

	public static ArrayList<String[]> getUserPasswordFromDB(String eMail) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT  password_back FROM users WHERE email = '" + eMail + "'";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		return results;
	}

	public static ArrayList<String[]> getUserActiveWithdrawsTransactionFromDB(
			String eMail) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT id, amount FROM transactions WHERE user_id = (SELECT id FROM users WHERE email = '"
				+ eMail
				+ "') AND type_id = 10 AND time_settled is null order by time_created asc";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 1);
		return results;
	}

	public static ArrayList<String[]> getUserReverseWithdrawsTransactionFromDB(
			String eMail) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT * FROM transactions WHERE user_id = (SELECT id FROM users WHERE email = '"
				+ eMail + "') AND type_id = 6 order by time_created desc";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 1);
		return results;
	}

	public static ArrayList<String[]> getUserFilesFromDB(String eMail)
			throws Exception {
		DBLayer dB = new DBLayer();
		ArrayList<String[]> resultsForUserID = getUserId(eMail);
		String userID = resultsForUserID.get(0)[0];
		String sqlRequest = "SELECT file_type_id, is_approved, file_status_id,  nvl(file_name, 'NA') FROM files WHERE user_id = '"
				+ userID + "'  order by file_type_id asc";

		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		return results;
	}
	/**
	 * Verifies investment data in the data base
	 * @param investMentId
	 * @param userId
	 * @param typeId
	 * @param amount
	 * @param level
	 * @param profit
	 * @throws Exception
	 */
	public static void verifyInvestmentData(String investmentId, String userId, String typeId, String amount, String level, 
			String[] profit) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT * FROM investments WHERE id = " + investmentId + " AND user_id = " + userId;
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals(typeId, results.get(0)[3], "investment type");
		Assert.assertEquals(amount, results.get(0)[4], "amount");
		if (results.get(0)[5].startsWith(".")) {
			results.get(0)[5] = "0" + results.get(0)[5];
		}
		Assert.assertEquals(level.replace(",", ""), results.get(0)[5], "current level");
		Assert.assertFalse(results.get(0)[5].equals("0"));
		Assert.assertEquals(Double.parseDouble(profit[0]) / 100, Double.parseDouble(results.get(0)[6]), "win");
		Assert.assertEquals((1 - Double.parseDouble(profit[1]) / 100), Double.parseDouble(results.get(0)[7]), "lose");
		Assert.assertEquals("1", results.get(0)[8], "writer_id");
		Assert.assertEquals(null, results.get(0)[14], "time setteled");
		Assert.assertEquals("0", results.get(0)[15], "is_settled");
	}
	/**
	 * Compares bubbles investment data in data base
	 * @param investmentId
	 * @param userId
	 * @param amount
	 * @param level
	 * @param profit
	 * @param isWon
	 * @throws Exception
	 */
	public static void verifyBubblesInvestmentData(String investmentId, String userId, String amount, String level, 
			int profit, boolean isWon) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT * FROM investments WHERE id = " + investmentId + " AND user_id = " + userId;
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals(results.get(0)[3], "6", "type_id");
		Assert.assertEquals(results.get(0)[4], amount + "00", "amount");
		if (isWon == true) {
			Assert.assertEquals(results.get(0)[10], String.valueOf((Integer.parseInt(amount + "00") * profit) / 100), "win");
			Assert.assertEquals(results.get(0)[11], "0", "lose");
		} else {
			Assert.assertEquals(results.get(0)[10], "0", "win");
			Assert.assertEquals(results.get(0)[11], amount + "00", "lose");
		}
		Assert.assertEquals(results.get(0)[15], "1", "is_settled");
	}
	/**
	 * Verify investment data after sold option plus investment
	 * @param optionId
	 * @param userId
	 * @param investAmount
	 * @param soldAmount
	 * @throws Exception
	 */
	public static void verifyOptionPlusSoldInvestment(String optionId, String userId, int investAmount,
			int soldAmount) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT * FROM investments WHERE id = " + optionId + " AND user_id = " + userId;
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		if (investAmount < soldAmount) {
			Assert.assertEquals("100", results.get(0)[11], "lose");
			int win = soldAmount - (investAmount - 100);
			Assert.assertEquals(String.valueOf(win), results.get(0)[10], "win");
			int houseResult = - (win - 100);
			Assert.assertEquals(String.valueOf(houseResult), results.get(0)[12], "house_result");
			
		} else {
			int lose = investAmount - soldAmount;
			if (! String.valueOf(lose).equals(results.get(0)[11])) {
				lose = lose - 1;
				if (! String.valueOf(lose).equals(results.get(0)[11])) {
					lose = lose + 2;
					Assert.assertEquals(String.valueOf(lose), results.get(0)[11], "lose");
				}	
			}	
			Assert.assertEquals("0", results.get(0)[10], "win");
			Assert.assertEquals(String.valueOf(lose), results.get(0)[12], "house_result");
		}
		Assert.assertEquals("1", results.get(0)[15], "is_settled");
	}
	/**
	 * Gets data for deviation 2 check
	 * @param marketId
	 * @return HasMap
	 * @throws Exception
	 */
	public static HashMap<String, String> getDev2Data(String marketId) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "select AMOUNT_FOR_DEV2, SEC_FOR_DEV2 from markets where id = '" + marketId + "'";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		HashMap<String, String> data = new HashMap<>();
		if (results.size() != 0) {
			data.put("amount", results.get(0)[0]);
			data.put("secs", results.get(0)[1]);
		}	
		return data;
	}
	/**
	 * Verifies data from canceled investment
	 * @param optionId
	 * @param userId
	 * @param typeId
	 * @param amount
	 * @param returnValue
	 * @param level
	 * @throws Exception
	 */
	public static void verifyCanceledInvestment(String optionId, String userId, String typeId, String amount, String returnValue, String level) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT * FROM investments WHERE id = " + optionId + " AND user_id = " + userId;
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals(typeId, results.get(0)[3], "type_id");
		Assert.assertEquals(amount, results.get(0)[4], "amount");
		if (! level.equals(results.get(0)[5])) {
			Assert.assertEquals(level.replace("0", "").replace(",", "").replace(".", ""), results.get(0)[5].replace("0", "").replace(",", "").replace(".", ""), "current_level");
		}	
		Assert.assertFalse(results.get(0)[5].equals("0"));
		Assert.assertEquals("0", results.get(0)[10], "win");
		Assert.assertEquals(returnValue, results.get(0)[11], "lose");
		Assert.assertEquals(returnValue, results.get(0)[12], "house_result");
		Assert.assertEquals("1", results.get(0)[15], "is_settled");
		Assert.assertEquals("1", results.get(0)[16], "is_canceled");
	}
	/**
	 * Verifies dynamics canceled investment 
	 * @param optionId
	 * @param userId
	 * @param amount
	 * @param returnValue
	 * @throws Exception
	 */
	public static void verifyDynamicsCanceledInvestment(String optionId, String userId, String amount, String returnValue) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT * FROM investments WHERE id = " + optionId + " AND user_id = " + userId;
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals("7", results.get(0)[3], "type_id");
		Assert.assertEquals(amount, results.get(0)[4], "amount");
		Assert.assertFalse(results.get(0)[5].equals("0"));
		Assert.assertEquals("0", results.get(0)[10], "win");
		Assert.assertEquals(returnValue, results.get(0)[11], "lose");
		Assert.assertEquals(returnValue, results.get(0)[12], "house_result");
		Assert.assertEquals("1", results.get(0)[15], "is_settled");
		Assert.assertEquals("1", results.get(0)[16], "is_canceled");
	}
	/**
	 * Verifies data from canceled Long term investment
	 * @param optionId
	 * @param userId
	 * @param amount
	 * @param returnValue
	 * @throws Exception
	 */
	public static void verifyLongTermCanceledInvestment(String optionId, String userId, String amount, String returnValue) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT * FROM investments WHERE id = " + optionId + " AND user_id = " + userId;
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals("3", results.get(0)[3], "type_id");
		Assert.assertEquals(amount, results.get(0)[4], "amount");
		Assert.assertEquals("0", results.get(0)[10], "win");
		Assert.assertEquals(returnValue, results.get(0)[11], "lose");
		Assert.assertEquals(returnValue, results.get(0)[12], "house_result");
		Assert.assertEquals("1", results.get(0)[15], "is_settled");
		Assert.assertEquals("1", results.get(0)[16], "is_canceled");
	}
	/**
	 * Verifies canceled bubbles investment
	 * @param investmentId
	 * @param userId
	 * @param amount
	 * @param loseValue
	 * @throws Exception
	 */
	public static void verifyBubblesCanceledInvestment(String investmentId, String userId, String amount, String loseValue) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT * FROM investments WHERE id = " + investmentId + " AND user_id = " + userId;
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals(results.get(0)[3], "6", "type_id");
		Assert.assertEquals(results.get(0)[4], amount, "amount");
		Assert.assertEquals("0", results.get(0)[10], "win");
		Assert.assertEquals(loseValue, results.get(0)[11], "lose");
		Assert.assertEquals(loseValue, results.get(0)[12], "house_result");
		Assert.assertEquals(results.get(0)[15], "1", "is_settled");
		Assert.assertEquals(results.get(0)[15], "1", "is_cancled");
	}
	/**
	 * Verifies deleted credit card
	 * @param userId
	 * @throws Exception
	 */
	public static void verifyDeletedCreditCard(String userId) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "select * from CREDIT_CARDS where USER_ID = " + userId;
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals("0", results.get(0)[3], "is_visible");
	}
	/**
	 * Verifies deposit transaction
	 * @param transactionId
	 * @param amount
	 * @throws Exception
	 */
	public static void verifyDepositTransaction(String transactionId, String amount) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "select * from TRANSACTIONS where id = " + transactionId;
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals("1", results.get(0)[3], "type_id");
		Assert.assertEquals(amount + "00", results.get(0)[5], "amount");
		Assert.assertEquals("7", results.get(0)[6], "status_id");
		Assert.assertTrue(! results.get(0)[11].equals(null));
	}
	/**
	 * Verifies withdraw transaction
	 * @param userId
	 * @param amount
	 * @throws Exception
	 */
	public static void verifyWithdrawTransaction(String userId, String amount) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "select * from TRANSACTIONS where USER_ID = " + userId + " ORDER BY TIME_CREATED DESC";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals("10", results.get(0)[3], "type_id");
		Assert.assertEquals(amount + "00", results.get(0)[5], "amount");
		Assert.assertEquals("4", results.get(0)[6], "status_id");
		Assert.assertTrue(results.get(0)[11] == null);
	}
	/**
	 * Verifies reverse withdraw transaction
	 * @param userId
	 * @param amount
	 * @param transactionId
	 * @throws Exception
	 */
	public static void verifyReverseWithdrawTransaction(String userId, String amount, String transactionId) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "select * from TRANSACTIONS where USER_ID = " + userId + " ORDER BY TIME_CREATED DESC";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals("6", results.get(0)[3], "type_id");
		Assert.assertEquals(amount, results.get(0)[5], "amount");
		Assert.assertEquals("2", results.get(0)[6], "status_id");
		Assert.assertTrue(results.get(0)[11] == null);
		
		sqlRequest = "select * from TRANSACTIONS where id = " + transactionId;
		ArrayList<String[]> withdrawTransaction = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals("6", withdrawTransaction.get(0)[6], "status_id");
	}
	/**
	 * Verifies month and year to edited credit card
	 * @param userId
	 * @param month
	 * @param year
	 * @throws Exception
	 */
	public static void verifyEditedCreditCard(String userId, String month, String year) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "select EXP_MONTH, EXP_YEAR from CREDIT_CARDS where USER_ID = " + userId + " and IS_VISIBLE = 1";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals(month, results.get(0)[0]);
		Assert.assertEquals(year, results.get(0)[1]);
	}
	/**
	 * Verifies failed deposit with minimum amount
	 * @param userId
	 * @throws Exception
	 */
	public static void verifyMinDepositError(String userId) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "select * from TRANSACTIONS where USER_ID = " + userId + " order by TIME_CREATED DESC";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals("3", results.get(0)[6]);
		Assert.assertEquals("error.min.deposit", results.get(0)[7]);
	}
	/**
	 * Verifies failed deposit with maximum amount
	 * @param userId
	 * @throws Exception
	 */
	public static void verifyMaxDepositError(String userId) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "select * from TRANSACTIONS where USER_ID = " + userId + " order by TIME_CREATED DESC";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals("3", results.get(0)[6]);
		Assert.assertEquals("error.max.deposit", results.get(0)[7]);
	}
	/**
	 * Verifies withdraw failed transaction with minimum amount
	 * @param userId
	 * @throws Exception
	 */
	public static void verifyMinWithdrawError(String userId) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "select * from TRANSACTIONS where USER_ID = " + userId + " order by TIME_CREATED DESC";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals("3", results.get(0)[6], "status_id");
		Assert.assertEquals("error.transaction.withdraw.minamounnt", results.get(0)[7]);
	}
	/**
	 * Verifies withdraw failed transaction with maximum amount
	 * @param userId
	 * @throws Exception
	 */
	public static void verifyMaxWithdrawError(String userId) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "select * from TRANSACTIONS where USER_ID = " + userId + " order by TIME_CREATED DESC";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals("3", results.get(0)[6], "status_id");
		Assert.assertEquals("error.transaction.withdraw.highamount", results.get(0)[7]);
	}
	/**
	 * Verifies dynamics investment 
	 * @param userId
	 * @param amount
	 * @throws Exception
	 */
	public static void verifyDynamicsInvestment(String userId, String amount) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "select * from INVESTMENTS where USER_ID = " + userId + " ORDER BY TIME_CREATED DESC";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals("7", results.get(0)[3], "type_id");
		Assert.assertEquals(amount, results.get(0)[4], "amount");
		Assert.assertEquals("0", results.get(0)[15], "is_settled");
	}
	/**
	 * Verifies closed dynamics investment
	 * @param optionId
	 * @param amount
	 * @param returnValue
	 * @throws Exception
	 */
	public static void verifyDynamicsClosedOption(String optionId, String amount, String returnValue) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "select * from INVESTMENTS where id = " + optionId;
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals("7", results.get(0)[3], "type_id");
		Assert.assertEquals(amount, results.get(0)[4], "amount");
		Assert.assertEquals("1", results.get(0)[15], "is_settled");
		int amountInt = Integer.parseInt(amount);
		int returnValueInt = Integer.parseInt(returnValue);
		if (amountInt < returnValueInt) {
			Assert.assertEquals(String.valueOf(returnValueInt - amountInt), results.get(0)[10], "win");
			Assert.assertEquals("0", results.get(0)[11], "lose");
			Assert.assertEquals("-" + String.valueOf(returnValueInt - amountInt), results.get(0)[12], "house_result");
		} else {
			Assert.assertEquals("0", results.get(0)[10], "win");
			Assert.assertEquals(String.valueOf(amountInt - returnValueInt), results.get(0)[11], "lose");
			Assert.assertEquals(String.valueOf(amountInt - returnValueInt), results.get(0)[12], "house_result");
		}
	}
	/**
	 * Verify updated personal details
	 * @param userId
	 * @param street
	 * @param streetNumber
	 * @param postCode
	 * @param city
	 * @param mobilePhone
	 * @param landLinePhone
	 * @throws Exception
	 */
	public static void verifyUpdatedPersonalDetails(String userId, String street, String streetNumber, String postCode, String city, 
			String mobilePhone, String landLinePhone) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "select * from users where id = " + userId;
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals(street, results.get(0)[8], "street");
		Assert.assertEquals(streetNumber, results.get(0)[26], "street_no");
		Assert.assertEquals(postCode, results.get(0)[10], "post_code");
		Assert.assertEquals(city, results.get(0)[44], "city");
		if (mobilePhone.startsWith("0")) {
			mobilePhone = mobilePhone.substring(1, mobilePhone.length());
		}
		Assert.assertEquals(mobilePhone, results.get(0)[19], "mobile_phone");
		if (landLinePhone.startsWith("0")) {
			landLinePhone = landLinePhone.substring(1, landLinePhone.length());
		}
		Assert.assertEquals(landLinePhone, results.get(0)[20], "land_line_phone");
		Assert.assertEquals("M", results.get(0)[21], "gender");
		Assert.assertEquals("0", results.get(0)[18], "is_contact_by_email");
		Assert.assertEquals("0", results.get(0)[35], "is_contact_by_sms");
	}
	/**
	 * Verifies show_cancel_investment column in data base
	 * @param userId
	 * @param expResult
	 * @throws Exception
	 */
	public static void verifyShowCancelInv(String userId, String expResult) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "select SHOW_CANCEL_INVESTMENT from USERS_ACTIVE_DATA where USER_ID = " + userId;
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals(expResult, results.get(0)[0]);
	}
	/**
	 * Approves attached document in data base
	 * @param userId
	 * @param fileName
	 * @throws Exception
	 */
	public static void approveDocument(String userId, String fileName) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlUpdate = "UPDATE files SET is_approved = 1, file_status_id = 3, is_approved_control = 1 "
				+ "where user_id = " + userId + " and file_name like '%" + fileName + "%'"; 
		String sqlCommit = "commit";
		dB.executeDBQuery(sqlUpdate, false);
		dB.executeDBQuery(sqlCommit, false);
	}
}
