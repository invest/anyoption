package com.anyoption.autotest.common;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.anyoption.autotest.utils.Configuration;

import oracle.jdbc.driver.OracleDriver;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class DBLayer {
	static Connection connection = null;
	/**
	 * Gets connection
	 * @return Connection
	 * @throws SQLException
	 * @throws IOException
	 */
	public Connection getConnection() throws SQLException, IOException {
		if (connection != null) {
			System.out.println("Can't create a connection!!!");
		}
		String dbConnectionUrl = Configuration.getProperty("dbHost");
		String userName = Configuration.getProperty("dbUser");
		String userPass = Configuration.getProperty("dbPass");

		DriverManager.registerDriver(new OracleDriver());
		return DriverManager.getConnection(dbConnectionUrl, userName, userPass);
	}
	/**
	 * Closes connection
	 * @throws SQLException
	 */
	public void closeConnection() throws SQLException {
		if (connection == null)
			return;

		System.out.println("Closing a conection...");
		connection.clearWarnings();
		connection.close();
		System.out.println("Connection closed.");
	}
	/**
	 * Gets result after execution SQL query
	 * @param con
	 * @param sqlQuery
	 * @return Array of String
	 * @throws SQLException
	 */
	public static String[] getResultFromDB(Connection con, String sqlQuery) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<String> a = new ArrayList<String>();

		pstmt = con.prepareStatement(sqlQuery);
		rs = pstmt.executeQuery();
		while (rs.next()) {
			a.add(rs.getString(1));
		}
		return (String[]) a.toArray(new String[a.size()]);
	}
	/**
	 * Executes SQL query
	 * @param sqlQuery
	 * @param throwOnFail
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	public void executeDBQuery(String sqlQuery, boolean throwOnFail) throws Exception {
		DBLayer dB = null;

		try {
			dB = new DBLayer();
			Connection con = dB.getConnection();

			PreparedStatement pstmt = null;
			ResultSet rs = null;

			pstmt = con.prepareStatement(sqlQuery);
			rs = pstmt.executeQuery();

		} catch (Exception exc) {
			if (throwOnFail)
				throw new Exception("Unable to execute data base query.", exc);
		} finally {
			if (dB != null) {
				dB.closeConnection();
			}
		}
	}
	/**
	 * Gets results from data base after executing SQL query
	 * @param query
	 * @param throwOnFail
	 * @param timesToTryToGetResult
	 * @return
	 * @throws Exception
	 */
	public ArrayList<String[]> getResultsFromDBImmediately(String query, boolean throwOnFail, int timesToTryToGetResult) throws Exception {
		ArrayList<String[]> tmpResults = new ArrayList<String[]>();
		DBLayer dB = null;

		try {
			dB = new DBLayer();
			Connection con = dB.getConnection();

			tmpResults = getResultsFromQueryImmediately(con, dB, query, timesToTryToGetResult);
		} catch (Exception exc) {
			if (throwOnFail)
				throw new Exception("Unable to execute data base query.", exc);
		} finally {
			if (dB != null) {
				dB.closeConnection();
			}
		}

		return tmpResults;
	}
	/**
	 * Gets result from data base
	 * @param con
	 * @param dB
	 * @param sql
	 * @param timesToTryToGetResult
	 * @return
	 * @throws Exception
	 */
	private ArrayList<String[]> getResultsFromQueryImmediately(Connection con, DBLayer dB, String sql, 
			int timesToTryToGetResult) throws Exception {
		ArrayList<String[]> resultArray = new ArrayList<String[]>();
		int tryes = 0;
		boolean ready = false;

		do {
			resultArray = dB.getResults(con, sql);
			if (resultArray.size() != 0) {
				ready = true; // there is a result from the query
			} else {
				if (++tryes < timesToTryToGetResult) {
					Thread.sleep(2/* sec */* 1000); // try again
				} else {
					ready = true; // there is no results from the query, max
									// tries are reached
				}
			}
		} while (!ready);

		return resultArray;
	}
	/**
	 * Gets results from query
	 * @param con
	 * @param sqlQuery
	 * @return
	 * @throws SQLException
	 */
	public List<String> getResultsFromQuery(Connection con, String sqlQuery) throws SQLException {
		PreparedStatement pstmt = null;

		List<String> results = new ArrayList<String>();
		try {
			pstmt = con.prepareStatement(sqlQuery);
			ResultSet rs = pstmt.executeQuery();
			int i = 1;
			while (rs.next()) {
				results.add(rs.getString(i));
				i++;
			}
			return results;
		} catch (SQLException e) {
			System.out.println(e);
			return results;
		} finally {
			if (pstmt != null) {
				pstmt.close();
			}
		}
	}
	/**
	 * Gets results
	 * @param con
	 * @param sqlQuery
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<String[]> getResults(Connection con, String sqlQuery) throws SQLException {
		ArrayList<String[]> result = new ArrayList<String[]>();
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement(sqlQuery);
			ResultSet rs = pstmt.executeQuery();
			int columnCount = rs.getMetaData().getColumnCount();
			while (rs.next()) {
				String[] row = new String[columnCount];
				for (int i = 0; i < columnCount; i++) {
					row[i] = rs.getString(i + 1);
				}
				result.add(row);
			}
			return result;
		} catch (SQLException e) {
			System.out.println(e);
			return result;
		} finally {
			if (pstmt != null) {
				pstmt.close();
			}
		}
	}
}
