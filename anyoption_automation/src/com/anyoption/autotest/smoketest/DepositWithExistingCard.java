package com.anyoption.autotest.smoketest;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.anyoption.autotest.pages.DepositPage;
import com.anyoption.autotest.pages.HeaderMenuPage;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class DepositWithExistingCard {
	private WebDriver driver;
	private String browserType;
	private String balance;
	/**
	 * 
	 * @param driver
	 * @param browserType
	 */
	public DepositWithExistingCard(WebDriver driver, String browserType, String balance) {
		this.driver = driver;
		this.browserType = browserType;
		this.balance = balance;
	}
	/**
	 * Makes deposit with existing card
	 * @param skin
	 * @param succMessage
	 * @param amMessage
	 * @param screenTitle
	 * @return String
	 */
	public String depositWithExistingCard(String skin, String succMessage, String amMessage, String screenTitle) {
		DepositPage depositPage = new DepositPage(driver, browserType);
		depositPage.openDepositSubTab();
		Assert.assertEquals(screenTitle, depositPage.getDepositTitle());
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		String currency = balance.replaceAll("\\d+.*", "");
		String moneyType = depositPage.getMoneyType();
		Assert.assertEquals(moneyType, currency);
		balance = balance.replaceAll("\\D+", "");
		balance = balance.substring(0, balance.length() - 2);
		depositPage.closeHelpWindow();
		depositPage.selectCard(1);
		depositPage.setCvvPass("123");
		String amount = "200";
		if (! skin.equals("EN")) {
			amount = "250";
		}
		depositPage.setDepositAmount(amount);
		depositPage.submitOldCard();
		Assert.assertEquals(succMessage, depositPage.getSuccessMessage());
		String balanceNow = headerMenuPage.getBalance().replace(",", "");
		int bal = Integer.parseInt(balance) + Integer.parseInt(amount);
		Assert.assertTrue(balanceNow.contains(String.valueOf(bal)));
		String amountMessage = depositPage.getAmountMessage();
		Assert.assertTrue(amountMessage.contains(amount));
		Assert.assertTrue(amountMessage.contains(moneyType + amount + ".00 " + amMessage));
		depositPage.continiueAfterNewDeposit();
		return balanceNow;
	}
}
