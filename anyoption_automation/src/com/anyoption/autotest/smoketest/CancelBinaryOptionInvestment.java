package com.anyoption.autotest.smoketest;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.InvestmentPage;
import com.anyoption.autotest.pages.MyAccountPage;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class CancelBinaryOptionInvestment {
	private WebDriver driver;
	private String balance;
	/**
	 * 
	 * @param driver
	 * @param balance
	 */
	public CancelBinaryOptionInvestment(WebDriver driver, String balance) {
		this.driver = driver;
		this.balance = balance;
	}
	/**
	 * Cancels binary option investment
	 * @param skin
	 * @param cancelMessage
	 * @param cancelLink
	 * @param amountMessage
	 * @param noOptionMess
	 * @param myOptionStatus
	 * @return String
	 */
	public String cancelBinaryOption(String skin, String cancelLink, String cancelMessage, String amountMessage, String noOptionMess, 
			String myOptionStatus) {
		InvestmentPage investmentPage = new InvestmentPage(driver);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		MyAccountPage myAccountPage = new MyAccountPage(driver);
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		String moneyType = balance.replaceAll("\\d+.*", "");
  		int number = investmentPage.getAcctiveInvestment();
  		if (number == -1) {
  			investmentPage.changeMarket(0);
  			number = 0;
  		}
  		String marketName = investmentPage.getMarketName(number);
  		String amount = "50";
  		investmentPage.setAmountToInvest(number, amount);
  		investmentPage.makeInvestmentToCancelWithRecursion(number);
  		Assert.assertTrue(investmentPage.isCancelInvestmentDisplayed(number));
  		investmentPage.doubleClickCancelInvestment(number);
  		Assert.assertTrue(investmentPage.isCanceledMessageDisplayed(number));
  		Assert.assertEquals(cancelMessage, investmentPage.getCanceledMessage(number));
  		
  		investmentPage.showCancelInvestmentPopUp(number);
  		String secs = investmentPage.getTimeToCancel(number);
  		if (2 != Integer.parseInt(secs)) {
  			Assert.assertTrue(2 > Integer.parseInt(secs));
  		}	
  		String level = investmentPage.getLevelFromCancelInv(number);
  		String amountCancel = investmentPage.getAmountFromCancelInv(number);
  		String message = investmentPage.getMessageFromCancelInv(number);
  		String returnAmount = message.replaceAll("[^\\d]", "");
  		String cancelLinkText = investmentPage.getCancelLinkText(number);
  		
  		Assert.assertEquals(cancelLink, cancelLinkText);
  		Assert.assertEquals(moneyType + amount + ".00", amountCancel);
  		Assert.assertEquals(amountMessage, message);
  		int amountInt = Integer.parseInt(amount);
  		int returnAmountInt = Integer.parseInt(returnAmount);
  		Assert.assertEquals((int)(amountInt * 0.1), returnAmountInt);
  		String balanceAfter = headerMenuPage.getBalance();
  		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
  		Assert.assertEquals(balanceAfterInt, balanceInt - Integer.parseInt(returnAmount + "00"));
  		headerMenuPage.openMyAccountMenu();
  		Assert.assertEquals(noOptionMess, myAccountPage.getNoOptionsMessage());
  		myAccountPage.clickClosedInvestmentRadioButton();
  		myAccountPage.clickSubmitButton();
  		String marketNameFromMyOption = myAccountPage.getSettledOptionAsset();
  		String levelFromMyOptions = myAccountPage.getSettledOptionLevel();
  		String amountFromMyOptions = myAccountPage.getSettledOptionAmount();
  		String returnValueFromMyOptions = myAccountPage.getSettledOptionReturnValue();
  		int returnAmValue = amountInt - returnAmountInt;
  		Assert.assertEquals(marketName, marketNameFromMyOption);
  		if (! level.replace("0", "").equals(levelFromMyOptions.replace("0", ""))) {
  			Assert.assertEquals(level.replace("0", "").replace(".", ""), levelFromMyOptions.replace("0", "").replace(".", ""));
  		}	
  		Assert.assertEquals(moneyType + amount + ".00", amountFromMyOptions);
  		Assert.assertEquals(moneyType + returnAmValue + ".00", returnValueFromMyOptions);
  		Assert.assertEquals(myOptionStatus, myAccountPage.getSettledOptionExpiryLevel());
  		return balanceAfter;
	}
}
