package com.anyoption.autotest.smoketest;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.InvestmentPage;
import com.anyoption.autotest.pages.MyAccountPage;
import com.anyoption.autotest.pages.OptionPlusPage;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class OptionPlusInvest {
	private WebDriver driver;
	private String balance;
	private String browserType;
	/**
	 * 
	 * @param driver
	 * @param balance
	 */
	public OptionPlusInvest(WebDriver driver, String balance, String browserType) {
		this.driver = driver;
		this.balance = balance;
		this.browserType = browserType;
	}
	/**
	 * Makes option plus investment
	 * @param skin
	 * @param message
	 * @return String
	 */
	public String makeOptionPlusBalance(String skin, String message) {
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		InvestmentPage investmentPage = new InvestmentPage(driver);
		OptionPlusPage optionPlusPage = new OptionPlusPage(driver);
		headerMenuPage.openOptionTradingMenu();
		optionPlusPage.openOptionPlusMenu(browserType);
		String moneyType = balance.replaceAll("\\d+.*", "");
  		optionPlusPage.setActiveMarket();
  		String profitBalanceBefore = optionPlusPage.getProfitBalance();
  		Assert.assertEquals(balance, profitBalanceBefore);
  		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
  		int totalinvestmentBefore = optionPlusPage.getTotalInvestment();
  		optionPlusPage.chooseAmountToInvest(0);
  		String percents = optionPlusPage.getProfitPercents().replace("%", "");
  		double percenCoefProfit = (Double.parseDouble(percents) / 100) + 1;
  		double percentCoefRefund = 0.15;
  		String amount = optionPlusPage.getAmountForInvest();
  		Assert.assertTrue(amount.contains(moneyType));
  		int amountInt = Integer.parseInt(amount.replaceAll("[^\\d]", "")) * 100;
  		String correctValue = optionPlusPage.getReturnValueIfCorrect();
  		Assert.assertTrue(correctValue.contains(moneyType));
  		int correctValueInt = Integer.parseInt(correctValue.replaceAll("[^\\d]", ""));
  		Assert.assertEquals((int)(amountInt * percenCoefProfit), correctValueInt);
  		String incorrectValue = optionPlusPage.getReturnValueIfIncorrect();
  		Assert.assertTrue(incorrectValue.contains(moneyType));
  		int incorrectValueInt = Integer.parseInt(incorrectValue.replaceAll("[^\\d]", ""));
  		Assert.assertEquals((int)(amountInt * percentCoefRefund), incorrectValueInt);
  		String asset = optionPlusPage.clickUpButton();
  		Assert.assertTrue(investmentPage.afterInvest(0, false));
  		String investmentId = investmentPage.getInvestmentId(0);
  		String expiresTime = investmentPage.getExpiresTime(0);
  		String amountInAfterInvestPopUp = investmentPage.getAmountFormAfterInvestPopUp(0);
  		String level = investmentPage.getLevelFromAfterInvest(0);
  		String profit[] = new String[2];
  		profit[0] = percents;
  		profit[1] = "15";
  		double amountDouble = Double.parseDouble(amount.replace(moneyType, ""));
  		amountDouble = amountDouble + 1.00;
  		Assert.assertEquals(moneyType + String.valueOf(amountDouble) + "0", amountInAfterInvestPopUp);
  		Assert.assertEquals(correctValueInt, investmentPage.getCorrectAmountFromAfterInvestPopUp(0));
  		Assert.assertEquals(incorrectValueInt, investmentPage.getIncorrectAmountFromAfterInvestPopUp(0));
  		Assert.assertEquals(message, investmentPage.getCommissionMessage());
  		String balanceAfter = headerMenuPage.getBalance();
  		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
  		int optionPlusAmount = amountInt + 100;
  		Assert.assertEquals(balanceInt - optionPlusAmount, balanceAfterInt);
  		optionPlusPage.waitTotalInvestments(moneyType);
  		int totalInvestmentAfter = optionPlusPage.getTotalInvestment();
  		String profitBalanceAfter = optionPlusPage.getProfitBalance();
  		Assert.assertEquals(balanceAfter, profitBalanceAfter);
  		Assert.assertEquals(totalinvestmentBefore + optionPlusAmount, totalInvestmentAfter);
  		amountInAfterInvestPopUp = amountInAfterInvestPopUp.replaceAll("[^\\d]", "");
  		headerMenuPage.openMyAccountMenu();
  		MyAccountPage myAccountPage = new MyAccountPage(driver);
  		String amountFromMyAccount = myAccountPage.getInvestmentAmount();
  		int amountFromMyAccountInt = Integer.parseInt(amountFromMyAccount.replaceAll("[^\\d]", ""));
  		Assert.assertEquals(investmentId, myAccountPage.getOptionId());
  		Assert.assertEquals(asset, myAccountPage.getAsset());
  		String levelFromMyAcc = myAccountPage.getLevel();
  		if (! level.equals(levelFromMyAcc)) {
  			Assert.assertEquals(level.replace("0", "").replace(".", ""), levelFromMyAcc.replace("0", "").replace(".", ""));
  		}	
  		String myAccountEpiryTyme = myAccountPage.getExpiryTime();
  		if (! expiresTime.equals(myAccountEpiryTyme)) {
  			Assert.assertTrue(myAccountEpiryTyme.contains(expiresTime));
  		}
  		Assert.assertEquals(optionPlusAmount, amountFromMyAccountInt);
  		Assert.assertTrue(amountFromMyAccount.contains(moneyType));
  		String correctFromMyAcc = myAccountPage.getAmountIfCorrect();
  		Assert.assertEquals(correctValue, correctFromMyAcc.replace(".", ""));
  		String incorrectFromMyAcc = myAccountPage.getAmountIfIncorrect();
  		Assert.assertEquals(incorrectValue, incorrectFromMyAcc.replace(".", ""));
  		return balanceAfter;
	}
}
