package com.anyoption.autotest.smoketest;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.anyoption.autotest.pages.BubblesPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.MyAccountPage;
import com.anyoption.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class BubblesInvest {
	private WebDriver driver;
	private String balance;
	
	/**
	 * 
	 * @param driver
	 */
	public BubblesInvest(WebDriver driver, String balance) {
		this.driver = driver;
		this.balance = balance;
	}
	/**
	 * Makes buubles investment
	 * @param lostMessage
	 * @param wonMessage
	 */
	public String makeBubblesInvestment(String lostMessage, String wonMessage, String purchasedMess, String tutMess1, String tutMess2, 
			String noOptionsMess) {
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		BubblesPage bubblesPage = new BubblesPage(driver);
		MyAccountPage myAccountPage = new MyAccountPage(driver);
		String moneyType = balance.replaceAll("\\d+.*", "");
		headerMenuPage.openOptionTradingMenu();
		bubblesPage.openBubbles();
		bubblesPage.switchDriverToBubbles();
		bubblesPage.passTutorial(tutMess1, tutMess2);
		bubblesPage.expandBubble();
		int profit = bubblesPage.getProfite();
		if (profit == 0) {
			profit = bubblesPage.moveBubble();
		}
		int amount = bubblesPage.chooseAmountForInvest(1, moneyType);
		bubblesPage.makeBubblesInvestment();
		Assert.assertEquals(purchasedMess, bubblesPage.getPositiveNotification());
		String market = bubblesPage.getAssetName();
		UtilsMethods.sleep(2000);
		profit = bubblesPage.getProfite();
		double returnIfCorrect = bubblesPage.getReturnIfCorrect(moneyType);
		double value = (double)(amount * (1 + ((double)profit / 100)));
		Assert.assertEquals(Math.round(value), Math.round(returnIfCorrect));
		Assert.assertEquals(0, bubblesPage.getReturnIfIncorrect(moneyType));
		driver.switchTo().defaultContent();
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		String balanceAfter = headerMenuPage.getBalance();
		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
		Assert.assertEquals(balanceInt - amount * 100, balanceAfterInt);
		bubblesPage.switchDriverToBubbles();
		boolean isWon = false;
		String balanceAfterInvest;
		int retAmount = bubblesPage.getReturnValueFromEndWindow(moneyType);
		if (retAmount != 0) {
			isWon = true;
		} 
		String expTime = bubblesPage.getExpTime(isWon);
		if (isWon == false) {
			Assert.assertEquals(lostMessage.toUpperCase(), bubblesPage.getMessageFromEndWindow(isWon));
			driver.switchTo().defaultContent();
			balanceAfterInvest = headerMenuPage.getBalance();
			Assert.assertEquals(balanceAfter, balanceAfterInvest);
		} else {
			Assert.assertEquals(wonMessage.toUpperCase(), bubblesPage.getMessageFromEndWindow(isWon));
			driver.switchTo().defaultContent();
			balanceAfterInvest = headerMenuPage.getBalance();
			int balanceAfterInvestInt = Integer.parseInt(balanceAfterInvest.replaceAll("[^\\d]", ""));
			Assert.assertEquals(balanceAfterInt + retAmount, balanceAfterInvestInt);
		}
		driver.switchTo().defaultContent();
		headerMenuPage.openMyAccountMenu();
		myAccountPage.selectOptionsType("1");
		Assert.assertEquals(noOptionsMess, myAccountPage.getNoOptionsMessage());
		myAccountPage.clickClosedInvestmentRadioButton();
		myAccountPage.clickSubmitButton();
		String asset = myAccountPage.getBubblesMarketName();
		String expiryTime = myAccountPage.getBubblesExpiryTime();
		String amountFromMyOptions = myAccountPage.getBubblesInvestedAmount();
		String retAmountFromMyOptions = myAccountPage.getBubblesReturnAmount(moneyType);
		Assert.assertEquals(market, asset);
		if (! expiryTime.contains(expTime)) {
			System.out.println("Rounding problem with seconds");
		}	
		Assert.assertEquals(moneyType + amount + ".00", amountFromMyOptions);
		Assert.assertEquals(moneyType + retAmount, retAmountFromMyOptions.replace(".", ""));
		return balanceAfterInvest;
	}
}
