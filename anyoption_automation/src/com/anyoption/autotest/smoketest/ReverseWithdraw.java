package com.anyoption.autotest.smoketest;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.anyoption.autotest.pages.DepositPage;
import com.anyoption.autotest.pages.HeaderMenuPage;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class ReverseWithdraw {
	private WebDriver driver;
	private String browserType;
	private String balance;
	/**
	 * 
	 * @param driver
	 * @param browserType
	 */
	public ReverseWithdraw(WebDriver driver, String browserType, String balance) {
		this.driver = driver;
		this.browserType = browserType;
		this.balance = balance;
	}
	/**
	 * Reverses withdraw
	 * @param skin
	 * @param description
	 * @param successMessage
	 * @return String
	 */
	public String reverseWithdraw(String skin, String description, String successMessage) {
		DepositPage depositPage = new DepositPage(driver, browserType);
		depositPage.openReverseWithdraw();
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		balance = balance.replaceAll("\\D+", "");
		Assert.assertEquals(description, depositPage.getReverseWithdrawDescription());
		String amount = depositPage.getReverseWithdrawAmount();
		depositPage.clickReverseWithdrawCheckBox();
		depositPage.submitReverseWithdraw();
		String message = depositPage.getReverseWithdrawSuccessMessage();
		Assert.assertTrue(message.contains(successMessage));
		Assert.assertTrue(message.contains(amount));
		String returnBalance = headerMenuPage.getBalance();
		String balanceAfter = returnBalance.replaceAll("\\D+", "");
		amount = amount.replaceAll("\\D+", "");
		Assert.assertEquals(Integer.parseInt(balanceAfter), Integer.parseInt(balance) + Integer.parseInt(amount));
		return returnBalance;
	}
}
