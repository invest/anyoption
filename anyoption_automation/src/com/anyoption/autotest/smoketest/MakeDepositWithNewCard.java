package com.anyoption.autotest.smoketest;

import java.awt.AWTException;
import java.io.IOException;
import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.anyoption.autotest.pages.AnyOptionBackEndPage;
import com.anyoption.autotest.pages.DepositPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.utils.Configuration;
import com.anyoption.autotest.utils.CreateUser;
import com.anyoption.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class MakeDepositWithNewCard {
	private WebDriver driver;
	private String browserType;
	private CreateUser user;
	private String backEndUrl;
	private String balance;
	private HashMap<String, String> newDepositData;
	private String cardHolderName = "AO automation tester";
	/**
	 * 
	 * @param driver
	 * @param browserType
	 * @param user
	 * @throws IOException
	 */
	public MakeDepositWithNewCard(WebDriver driver, String browserType, CreateUser user, String balance, HashMap<String, String> newDepositData) throws IOException {
		this.driver = driver;
		this.browserType = browserType;
		this.user = user;
		this.balance = balance;
		this.newDepositData = newDepositData;
		backEndUrl = Configuration.getProperty("backEndUrl");
	}
	/**
	 * Makes deposit with new card
	 * @param skin
	 * @param succMessage
	 * @param amMessage
	 * @throws IOException
	 * @return HasMap
	 */
	public HashMap<String, String> makeDepositWithNewCard(String skin, String succMessage, String amMessage) throws IOException {
		boolean tester = false;
		DepositPage depositPage = new DepositPage(driver, browserType);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		depositPage.closeHelpWindow();
		String currentUrl = driver.getCurrentUrl(); 
  		Assert.assertTrue(balance.contains("0.00"));
  		String moneyType = balance.replace("0.00", "");
		String amount = depositPage.getDefaultAmount();
		depositPage.setCardNumber("4200000000000000");
		depositPage.selectMonthOfExpiry(11);
		depositPage.selectYearOfExpiry(1);
		depositPage.setCardHolderName(cardHolderName);
		depositPage.setCVV("123");
		depositPage.setStreet("StreetTest");
		depositPage.setCity("testCity");
		depositPage.setPostCode("1234");
		depositPage.selectDayOfBirth("19");
		depositPage.selectMonthOfBirth("06");
		depositPage.selectYearOfBirth("1986");
		if (! browserType.equalsIgnoreCase("firefox") || ! Configuration.getProperty("operationSystem").equals("Linux")) {
			Assert.assertTrue(depositPage.isNewCardOK());
			Assert.assertTrue(depositPage.isExpDateOK());
			Assert.assertTrue(depositPage.isHolderNameOK());
			Assert.assertTrue(depositPage.isCcPassOK());
			Assert.assertTrue(depositPage.isAddressOK());
			Assert.assertTrue(depositPage.isCityOK());
			Assert.assertTrue(depositPage.isZipCodeOK());
		}	
		Assert.assertTrue(depositPage.isBirthDateOK());
		depositPage.registerNewCard();
		if (! driver.getCurrentUrl().contains("integration")) {
			depositPage.waitErrorMessage();		
		} 
		tester = this.makeUserTesterAndAllowCard(user);
		if (tester == true) {
			newDepositData.put("isTester", "yes");
		}
		UtilsMethods.sleep(2000);
		String depositUrl = currentUrl.replace("firstNewCard", "deposit");
		driver.get(depositUrl);
//		depositPage.closeHelpWindow();
		Assert.assertTrue(tester);
		depositPage.selectCard(1);
		depositPage.setCvvPass("123");
		depositPage.submitOldCard();
		Assert.assertTrue(depositPage.isPopUpQuestionsEnable()); 
		String balanceNow = headerMenuPage.getBalance();
		newDepositData.put("balance", balanceNow);
		Assert.assertTrue(balanceNow.contains(amount + ".00"));
		Assert.assertEquals(succMessage, depositPage.getSuccessMessage());
		String amountMessage = depositPage.getAmountMessage();
		Assert.assertTrue(amountMessage.contains(amount + ".00"));
		Assert.assertTrue(amountMessage.contains(moneyType + amount + ".00 " + amMessage));
		return newDepositData;
	}
	/**
	 * Makes user tester and allows the credit card in back end
	 * @param user
	 * @return
	 * @throws IOException
	 * @throws AWTException 
	 */
	private boolean makeUserTesterAndAllowCard(CreateUser user) throws IOException {
		boolean tester = false;
	    driver.get(backEndUrl);
		AnyOptionBackEndPage anyOptionBackEndPage = new AnyOptionBackEndPage(driver, browserType);
		anyOptionBackEndPage.loginInBackEnd();
		anyOptionBackEndPage.searchUser(user);
		tester = anyOptionBackEndPage.makeTester(user);
		if (tester == true) {
			anyOptionBackEndPage.allowCard(user, cardHolderName);
			anyOptionBackEndPage.logoutFromBackEnd();
		} else {
			anyOptionBackEndPage.logoutFromBackEnd();
		}
		return tester;
	}
}
