package com.anyoption.autotest.smoketest;

import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.OpenAccountPage;
import com.anyoption.autotest.utils.Configuration;
import com.anyoption.autotest.utils.CreateUser;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class RegisterUser {
	private WebDriver driver;
	private String browserType;
	private CreateUser user;
	private String url;
	private HashMap<String, String> registerData;
	/**
	 * 
	 * @param driver
	 * @param browserType
	 * @param user
	 */
	public RegisterUser(WebDriver driver, String browserType, CreateUser user, String url, HashMap<String, String> registerData) {
		this.driver = driver;
		this.browserType = browserType;
		this.user = user;
		this.url = url;
		this.registerData = registerData;
	}
	/**
	 * Register new user from quick sign up form
	 * @param skin
	 * @param openAccount
	 * @param myAccountTitle
	 * @param firstMessage
	 * @param secondMessage
	 * @return HasMap
	 * @throws Exception
	 */
	public HashMap<String, String> registerUser(String skin, String openAccount, String myAccountTitle, String firstMessage, String secondMessage) throws Exception {
		OpenAccountPage openAccountPage = new OpenAccountPage(driver, browserType);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			headerMenuPage.changeLang(skin);
			String currentUrl = driver.getCurrentUrl();
			if (currentUrl.contains("bgtestenv")) {
				driver.get(url);
			}
		}
		String firstName = user.getFirstName();
		String lastName = user.getLastName();
		String email = user.getEMail();
		String phone = user.getPhone();
		String pass = "123456";
		openAccountPage.selectCountry();
		openAccountPage.typeFirstName(firstName);
		openAccountPage.typeSecondName(lastName);
		openAccountPage.typeEmail(email);
		openAccountPage.typePhone(phone);
		openAccountPage.typePassword(pass);
		openAccountPage.typeConfirmPasswordQuickForm(pass);
		openAccountPage.clickTermAndConditionsCheckBox();
		if (! browserType.equalsIgnoreCase("firefox") || ! Configuration.getProperty("operationSystem").equals("Linux")) {
			Assert.assertTrue(openAccountPage.isNamesOk());
			Assert.assertTrue(openAccountPage.isEmailOk());
			Assert.assertTrue(openAccountPage.isPhoneOk());
			Assert.assertTrue(openAccountPage.isPasswordOk());
			Assert.assertTrue(openAccountPage.isConfirmPassOk());
		}	
		Assert.assertEquals(true, openAccountPage.isCheckedTermAndCond());
		openAccountPage.clickOpenAccountButton();
		registerData.put("IsRegistered", "yes");
		if (driver.getCurrentUrl().contains("integration")) {
			driver.navigate().refresh();
		}
		Assert.assertEquals(myAccountTitle, openAccountPage.getMyAccountFormTitle());
		String balance = headerMenuPage.getBalance();
		Assert.assertTrue(balance.contains("0.00"));
		registerData.put("balance", balance);
		Assert.assertEquals(firstMessage, openAccountPage.getNewDepositFirstMessage());
		Assert.assertEquals(secondMessage, openAccountPage.getNewDepositSecondMessage());
		Assert.assertEquals(firstName, headerMenuPage.getUserNameFromHeader());
		return registerData;
	}
}
