package com.anyoption.autotest.smoketest;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.anyoption.autotest.pages.DepositPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.utils.CreateUser;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class WithdrawDeposit {
	private WebDriver driver;
	private String browserType;
	private CreateUser user;
	private String balance;
	/**
	 * 
	 * @param driver
	 * @param browserType
	 * @param user
	 */
	public WithdrawDeposit(WebDriver driver, String browserType, CreateUser user, String balance) {
		this.driver = driver;
		this.browserType = browserType;
		this.user = user;
		this.balance = balance;
	}
	/**
	 * Withdraws deposit 
	 * @param skin
	 * @param screenTitle
	 * @param thanksMessage
	 */
	public String withdrawDeposit(String skin, String screenTitle, String thanksMessage) {
		DepositPage depositPage = new DepositPage(driver, browserType);
		depositPage.openDepositMenu();
		depositPage.openWithdrawMenu();
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		String moneyType = balance.replaceAll("\\d+.*", "");
		balance = balance.replaceAll("\\D+", "");
		String userFirstName = user.getFirstName();
		Assert.assertEquals(screenTitle, depositPage.getWithdrawScreenTitle());
		depositPage.selectCardToWithdraw(1);
		String amount = "200";
		depositPage.setAmountToWithdraw(amount);
		depositPage.clickWithdrawButton();
		if (depositPage.isResetBonusPopUpDisplayed() == true) {
			depositPage.confirmResetBonus();
		}
		depositPage.choosekWithdrawProfitRadioButton();
		depositPage.submitPopUpWithdraw();
		Assert.assertEquals(thanksMessage, depositPage.getFeedbackMessage());
		depositPage.confirmFeedback();
		Assert.assertTrue(depositPage.isWithdrawPopUpDisplayed());
		Assert.assertTrue(depositPage.getWithdrawPopUpHeading().contains(userFirstName));
		switch (moneyType) {
		case "$":
			Assert.assertEquals("3000", depositPage.getFeeFromImortantNote(moneyType));
			break;
		case "€":
			Assert.assertEquals("2500", depositPage.getFeeFromImortantNote(moneyType));
		}
		depositPage.submitWithdrawFinally();
		Assert.assertTrue(depositPage.getWithdrawPopUpText().contains(moneyType + amount));
		depositPage.closeWithdrawAcceptedText();
		String balanceAfter = headerMenuPage.getBalance().replaceAll("\\D+", "");
		Assert.assertEquals(Integer.parseInt(balanceAfter), Integer.parseInt(balance) - Integer.parseInt(amount + "00"));
		return balanceAfter;
	}
}
