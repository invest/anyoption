package com.anyoption.autotest.smoketest;

import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.anyoption.autotest.utils.ReadTestData;

public class SmokeTestDE extends SmokeBaseTest {
	private String testSkin;
	private String currentBalance;
	
	public SmokeTestDE(String parameterSkin) throws Exception {
		super("DE");
		testSkin = "DE";
	}
	/**
	 * Data provider method for regiterUser() method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "register")
	private Object[][] registerUserDataProvider() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/CreateAccountTestData.xlsx", testSkin, 5);
	    return(retObjArr);
	}
	/**
	 * Register user
	 * @param skin
	 * @param openAccount
	 * @param myAccountTitle
	 * @param firstMessage
	 * @param secondMessage
	 * @throws Exception
	 */
	@Test(dataProvider = "register", priority = 1)
	public void registerUserTest(String skin, String openAccount, String myAccountTitle, String firstMessage, String secondMessage) throws Exception {
		RegisterUser registerUser = new RegisterUser(driver, browserType, user, url, registerData);
		registerData = registerUser.registerUser(skin, openAccount, myAccountTitle, firstMessage, secondMessage);
		currentBalance = registerData.get("balance");
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "newDeposit")
	private Object[][] makeNewDepositDataProvider() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/DepositWithNewCardTestData.xlsx", testSkin, 3);
	    return(retObjArr);
	}
	/**
	 * Makes first deposit
	 * @param skin
	 * @param succMessage
	 * @param amMessage
	 * @throws IOException
	 */
	@Test(dataProvider = "newDeposit", priority = 2, dependsOnMethods = {"registerUserTest"})
	public void makeNewDepositTest(String skin, String succMessage, String amMessage) throws IOException {
		MakeDepositWithNewCard makeDepositWithNewCard = new MakeDepositWithNewCard(driver, browserType, user, currentBalance, newDepositData);
		newDepositData = makeDepositWithNewCard.makeDepositWithNewCard(skin, succMessage, amMessage);
		currentBalance = newDepositData.get("balance");
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "makeQuestionnaire")
	private Object[][] makeQuestionnaireDataProvider() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/MakeQuestionnaireTestData.xlsx", testSkin, 3);
	    return(retObjArr);
	}
	/**
	 * Makes the questionnaire
	 * @param skin
	 * @param title
	 * @throws IOException
	 */
	@Test(dataProvider = "makeQuestionnaire", priority = 3, dependsOnMethods = {"makeNewDepositTest"})
	public void makeQuestionnaireTest(String skin, String title, String subjectText) throws IOException {
		MakeQuestionnaire makeQuestionnaire = new MakeQuestionnaire(driver, operationSys, browserType);
		makeQuestionnaire.makeQuestionnaire(skin, title, subjectText);
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "witdrawDeposit")
	private Object[][] withdrawDepositDataProvider() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/WithdrawDepositTestData.xlsx", testSkin, 3);
	    return(retObjArr);
	}
	/**
	 * Withdraws deposit
	 * @param skin
	 * @param screenTitle
	 * @param thanksMessage
	 */
	@Test(dataProvider = "witdrawDeposit", priority = 4, dependsOnMethods = {"makeQuestionnaireTest"})
	public void withdrawDepositTest(String skin, String screenTitle, String thanksMessage) {
		WithdrawDeposit withdrawDeposit = new WithdrawDeposit(driver, browserType, user, currentBalance);
		currentBalance = withdrawDeposit.withdrawDeposit(skin, screenTitle, thanksMessage);
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "reverseWithdraw")
	private Object[][] reverseWithdrawDataProvider() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/ReverseWithdrawTestData.xlsx", testSkin, 3);
	    return(retObjArr);
	}
	/**
	 * Reverses withdraw
	 * @param skin
	 * @param description
	 * @param successMessage
	 */
	@Test(dataProvider = "reverseWithdraw", priority = 5, dependsOnMethods = {"withdrawDepositTest"})
	public void reverseWithdrawTest(String skin, String description, String successMessage) {
		ReverseWithdraw reverseWithdraw = new ReverseWithdraw(driver, browserType, currentBalance);
		currentBalance = reverseWithdraw.reverseWithdraw(skin, description, successMessage);
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "depositWithExistingCard")
	private Object[][] depositWithExistingCardDataProvider() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/DepositWithExistingCardTestData.xlsx", testSkin, 4);
	    return(retObjArr);
	}
	/**
	 * Makes deposit with existing card
	 * @param skin
	 * @param screenTitle
	 * @param succMessage
	 * @param amMessage
	 */
	@Test(dataProvider = "depositWithExistingCard", priority = 6, dependsOnMethods = {"reverseWithdrawTest"})
	public void depositWithExistingCardTest(String skin, String screenTitle, String succMessage, String amMessage) {
		DepositWithExistingCard depositWithExistingCard = new DepositWithExistingCard(driver, browserType, currentBalance);
		currentBalance = depositWithExistingCard.depositWithExistingCard(skin, screenTitle, succMessage, amMessage);
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "cancelBinaryOptionInvestment")
	private Object[][] cancelBinaryOptionInvestmentDataProvider() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/CancelInvestmentTestData.xlsx", testSkin, 6);
	    return(retObjArr);
	}
	/**
	 * Cancels binary option investment
	 * @param skin
	 * @param cancelMessage
	 * @param cancelLink
	 * @param amountMessage
	 * @param noOptionMess
	 * @param myOptionStatus
	 */
	@Test(dataProvider = "cancelBinaryOptionInvestment", priority = 7, dependsOnMethods = {"depositWithExistingCardTest"})
	public void cancelBinaryOptionInvestmentTest(String skin, String cancelMessage, String cancelLink, String amountMessage, String noOptionMess, 
			String myOptionStatus) {
		CancelBinaryOptionInvestment cancelBinaryOptionInvestment = new CancelBinaryOptionInvestment(driver, currentBalance);
		currentBalance = cancelBinaryOptionInvestment.cancelBinaryOption(skin, cancelMessage, cancelLink, amountMessage, noOptionMess, myOptionStatus);
	}
	/**
	 * Makes binary option investment
	 */
	@Test(priority = 8, dependsOnMethods = {"cancelBinaryOptionInvestmentTest"})
	public void binaryOptionInvestTest() {
		BinaryOptionInvest binaryOptionInvest = new BinaryOptionInvest(driver, currentBalance, operationSys, testSkin);
		currentBalance = binaryOptionInvest.makeBinatyOptionInvest();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "optionPlusInvest")
	private Object[][] optionPlusInvestDataProvider() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/OptionPlusInvestTestData.xlsx", testSkin, 2);
	    return(retObjArr);
	}
	/**
	 * Makes option plus investment
	 * @param skin
	 * @param message
	 */
	@Test(dataProvider = "optionPlusInvest", priority = 9, dependsOnMethods = {"binaryOptionInvestTest"})
	public void optionPlusInvestTest(String skin, String message) {
		OptionPlusInvest optionPlusInvest = new OptionPlusInvest(driver, currentBalance, browserType);
		currentBalance = optionPlusInvest.makeOptionPlusBalance(skin, message);
	}
	/**
	 * Makes long term investment
	 */
	@Test(priority = 10, dependsOnMethods = {"optionPlusInvestTest"})
	public void longTermInvestTest() {
		LongTermInvest longTermInvest = new LongTermInvest(driver, currentBalance);
		currentBalance = longTermInvest.makeLongTermInvest();
	}
	/**
	 * Data provider method for regiterUser() method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "message")
	private Object[][] readWelcomeMessageDataProvider() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/ReadWelcomeMessageTestData.xlsx", testSkin, 4);
	    return(retObjArr);
	}
	/**
	 * Reads welcome message
	 * @param skin
	 * @param welcome
	 * @param dear
	 * @param deatailsText
	 */
	@Test(dataProvider = "message", priority = 11, dependsOnMethods = {"longTermInvestTest"})
	public void readWelcomeMessageTest(String skin, String welcome, String dear, String deatailsText) {
		ReadWelcomeMessage readWelcomeMessage = new ReadWelcomeMessage(driver, user, browserType);
		readWelcomeMessage.readWelcomeMessage(skin, welcome, dear, deatailsText);
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "bubblesInvestment")
	private Object[][] makeBubblesInvestmentDataProvider() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/BubblesInvestTestData.xlsx", testSkin, 7);
	    return(retObjArr);
	}
	/**
	 * Makes bubbles investment
	 * @param skin
	 * @param lostMessage
	 * @param wonMessage
	 */
	@Test(dataProvider = "bubblesInvestment", priority = 12, dependsOnMethods = {"readWelcomeMessageTest"})
	public void makeBubblesInvestment(String skin, String lostMessage, String wonMessage, String purchasedMess, String tutMess1, 
			String tutMess2, String noOptionsMess) {
		BubblesInvest bubblesInvest = new BubblesInvest(driver, currentBalance);
		currentBalance = bubblesInvest.makeBubblesInvestment(lostMessage, wonMessage, purchasedMess, tutMess1, tutMess2, noOptionsMess);
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dynamicsInvestment")
	private Object[][] dynamicsInvestmentDataProvider() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("rests/DynamicsInvestmentTestData.xlsx", testSkin, 7);
	    return(retObjArr);
	}
	/**
	 * Makes dynamics investment
	 * @param skin
	 * @param text1
	 * @param text2
	 * @param text3
	 * @param text4
	 * @param text5
	 * @param countMessage
	 */
	@Test(dataProvider = "dynamicsInvestment", priority = 13, dependsOnMethods = {"makeBubblesInvestment"})
	public void makeDynamicsInvestment(String skin, String text1, String text2, String text3, String text4, String text5, 
			String countMessage) {
		DynamicsInvestment dynamicsInvestment = new DynamicsInvestment(driver, testSkin, currentBalance);
		currentBalance = dynamicsInvestment.makeDynamicsInvestment(text1, text2, text3, text4, text5, countMessage);
	}
}
