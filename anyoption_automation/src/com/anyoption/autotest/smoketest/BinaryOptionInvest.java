package com.anyoption.autotest.smoketest;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.InvestmentPage;
import com.anyoption.autotest.pages.MyAccountPage;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class BinaryOptionInvest {
	private WebDriver driver;
	private String balance;
	private String operationSys;
	private String skin;
	/**
	 * 
	 * @param driver
	 * @param balance
	 * @param operationSys
	 */
	public BinaryOptionInvest(WebDriver driver, String balance, String operationSys, String skin) {
		this.driver = driver;
		this.balance = balance;
		this.operationSys = operationSys;
		this.skin = skin;
	}
	/**
	 * Makes binary option investment
	 * @return
	 */
	public String makeBinatyOptionInvest() {
		InvestmentPage investmentPage = new InvestmentPage(driver);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		headerMenuPage.openOptionTradingMenu();
		String moneyType = balance.replaceAll("\\d+.*", "");
		Assert.assertFalse(investmentPage.isBinaryLegendDisplayed());
		int number;
		if (! skin.equalsIgnoreCase("EN") && driver.getCurrentUrl().contains("bgtestenv")) {
			System.out.println("Bg test problem with redirect");
			if (skin.equalsIgnoreCase("IT")) {
				driver.get("http://www1.bgtestenv.anyoption.it/binary-options-trading");
			} else {
				driver.get("http://" + skin.toLowerCase() + ".bgtestenv.anyoption.com/binary-options-trading");
			}	
			number = investmentPage.getAcctiveInvestment();
  		} else {
  			number = investmentPage.getAcctiveInvestment();
  		}
		
		if (number == -1) {
			investmentPage.changeMarket(0);
			number = 0;
		}
		
		investmentPage.waitHolder(number);
  		investmentPage.clickInvestmentDropDown(number);
  		investmentPage.chooseAmountToInvest(number, 0);
  		String amountToInvest = investmentPage.getTheAmountForInvest(number);
  		int amountToInvestInt = Integer.parseInt(amountToInvest.replace(moneyType, ""));
  		investmentPage.clickProfitDropDown(number);
  		String chooseProfit[] = investmentPage.chooseProfit(number, 0);
  		String profitToInvest = investmentPage.getTheProfit(number);
  		Assert.assertEquals(chooseProfit[0] + "%", profitToInvest);
  		int correct = investmentPage.getReturnValueIfCorrect(number);
  		int incorrect = investmentPage.getReturnValueIfIncorrect(number);
  		String incorectForVerificationInMyAcc = investmentPage.getIncorectAmount(number);
  		double profitPercent = Double.parseDouble(chooseProfit[0]);
  		double refundPercent = Double.parseDouble(chooseProfit[1]);
  		double returnValue = amountToInvestInt * (1 + (profitPercent / 100)) * 100;
  		returnValue = Math.round(returnValue);
  		Assert.assertEquals(correct, (int)returnValue);
  		returnValue = (amountToInvestInt * (refundPercent / 100)) * 100;
  		Assert.assertEquals(incorrect, (int)returnValue);
  		String asset = investmentPage.getMarketName(number);
//  		investmentPage.clickUpButton(number);
  		investmentPage.makeBinaryOptionInvestWithRecursion(number);
  		Assert.assertTrue(investmentPage.afterInvest(number, true));
  		String amountAfterInvestPopUp = investmentPage.getAmountFormAfterInvestPopUp(number);
  		Assert.assertEquals(amountToInvest + ".00", amountAfterInvestPopUp);
  		Assert.assertEquals(correct, investmentPage.getCorrectAmountFromAfterInvestPopUp(number));
  		Assert.assertEquals(incorrect, investmentPage.getIncorrectAmountFromAfterInvestPopUp(number));
  		String investmentId = investmentPage.getInvestmentId(number);
  		String expiresTime = investmentPage.getExpiresTime(number);
  		amountAfterInvestPopUp = amountAfterInvestPopUp.replaceAll("[^\\d]", "");
  		String level = investmentPage.getLevelFromAfterInvest(number);
  		investmentPage.openProfitLine(number);
  		String balanceFromProfitLine = investmentPage.getBalanceFromProfitLineWindow(4);
  		String balanceAfter = headerMenuPage.getBalance();
  		Assert.assertEquals(balanceFromProfitLine, balanceAfter);
  		int ballanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
  		int amountInt = Integer.parseInt(amountToInvest.replace(moneyType, "") + "00");
  		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
  		Assert.assertEquals(ballanceInt - amountInt, balanceAfterInt);
  		String totalInvest = investmentPage.getTotalInvestmentFromProfitLine(4);
  		if (! (amountToInvest + ".00").equals(totalInvest)) {
  			System.out.println("Network error with profit line response");
  		} else {
  			Assert.assertEquals(amountToInvest + ".00", totalInvest);
  		}
  		investmentPage.closeProfitLineWindow(operationSys);
  		Assert.assertTrue(investmentPage.isBinaryLegendDisplayed());
  		headerMenuPage.openMyAccountMenu();
  		MyAccountPage myAccountPage = new MyAccountPage(driver);
  		String amountFromMyAccount = myAccountPage.getInvestmentAmount();
  		int amountFromMyAccountInt = Integer.parseInt(amountFromMyAccount.replaceAll("[^\\d]", ""));
  		Assert.assertEquals(investmentId, myAccountPage.getOptionId());
  		Assert.assertEquals(asset, myAccountPage.getAsset());
  		String levelFromMyAcc = myAccountPage.getLevel();
  		if (! level.equals(levelFromMyAcc)) {
  			Assert.assertEquals(level.replace("0", "").replace(".", ""), levelFromMyAcc.replace("0", "").replace(".", ""));
  		}
  		String myAccountEpiryTyme = myAccountPage.getExpiryTime();
  		if (! expiresTime.equals(myAccountEpiryTyme)) {
  			Assert.assertTrue(myAccountEpiryTyme.contains(expiresTime));
  		}
  		Assert.assertEquals(amountInt, amountFromMyAccountInt);
  		Assert.assertTrue(amountFromMyAccount.contains(moneyType));
  		String correctFromMyAcc = myAccountPage.getAmountIfCorrect();
  		Assert.assertEquals(moneyType + correct, correctFromMyAcc.replace(".", ""));
  		String incorrectFromMyAcc = myAccountPage.getAmountIfIncorrect();
  		Assert.assertEquals(incorectForVerificationInMyAcc, incorrectFromMyAcc);
  		return balanceAfter;
	}
}
