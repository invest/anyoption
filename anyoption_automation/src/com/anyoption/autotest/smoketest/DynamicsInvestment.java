package com.anyoption.autotest.smoketest;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.anyoption.autotest.pages.DynamicsPage;
import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.MyAccountPage;
import com.anyoption.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class DynamicsInvestment {
	private WebDriver driver;
	private String skin;
	private String balance;
	/**
	 * 
	 * @param driver
	 * @param skin
	 * @param balance
	 */
	public DynamicsInvestment(WebDriver driver, String skin, String balance) {
		this.driver = driver;
		this.skin = skin;
		this.balance = balance;
	}
	/**
	 * Makes dynamics investment
	 * @param text1
	 * @param text2
	 * @param text3
	 * @param text4
	 * @param text5
	 * @param countMessage
	 * @return String
	 */
	public String makeDynamicsInvestment(String text1, String text2, String text3, String text4, String text5, String countMessage) {
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		DynamicsPage dynamicsPage = new DynamicsPage(driver);
		MyAccountPage myAccountPage = new MyAccountPage(driver);
		headerMenuPage.openOptionTradingMenu();
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		String moneyType = balance.replaceAll("\\d+.*", "");
		dynamicsPage.openDynamics();
		if (! dynamicsPage.isDynamicsTutorialDisplayed() == true) {
			dynamicsPage.openTutorial();
		}
		String tutorialMessages[] = {text1, text2, text3, text4, text5};
		for (int i = 0; i < tutorialMessages.length; i++) {
			Assert.assertEquals(tutorialMessages[i], dynamicsPage.getTutorialMessage(i + 1));
			dynamicsPage.clickTutorialOkButton(i + 1);
			UtilsMethods.sleep(500);
		}
		dynamicsPage.chooseMarket();
		String asset = dynamicsPage.getCurrentMarket();
		String incorrect = dynamicsPage.getAboveIncorrect();
		Assert.assertEquals(incorrect, dynamicsPage.getAboveIncorrect());
		String amount = "100";
		double amountDouble = Double.parseDouble(amount);
		dynamicsPage.setAmount(amount);
		int openedOptionsInChartBefore = dynamicsPage.getOpenedOptionsInChart();
		Assert.assertEquals(0, openedOptionsInChartBefore);
		dynamicsPage.makeAboveInvestment();
		Assert.assertTrue(dynamicsPage.isOptionPurchased());
		Assert.assertEquals(asset, dynamicsPage.getMarketNameFromPurchOption());
		Assert.assertEquals(amount, dynamicsPage.getAmountFromPurchOption(moneyType));
		String level = dynamicsPage.getPurchasedOptionLevel(skin);
		String profit = dynamicsPage.getProfitFromPurchOption();
		double profitDouble = Double.parseDouble(profit);
		String correct = dynamicsPage.getCorrectAmountFromPurchOption(moneyType);
		double correctDouble = Double.parseDouble(correct);
		Assert.assertEquals(Math.round(amountDouble * (1.0 + (profitDouble / 100))), Math.round(correctDouble));
		Assert.assertEquals(incorrect, dynamicsPage.getIncorrectAmountFromPurchOption(moneyType));
		String balanceAfter = headerMenuPage.getBalance();
		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
		Assert.assertEquals(balanceInt - Integer.parseInt(amount + "00"), balanceAfterInt);
		int openedOptionsInChartAfter = dynamicsPage.getOpenedOptionsInChart();
		Assert.assertEquals(openedOptionsInChartBefore + 1, openedOptionsInChartAfter);
		Assert.assertEquals(countMessage, dynamicsPage.getAboveOpenOptions());
		Assert.assertEquals(moneyType + amount, dynamicsPage.getAboveOpenOptionAmount());
		Assert.assertEquals(moneyType + correct, dynamicsPage.getAboveOpenOptionCorrect());
		headerMenuPage.openMyAccountMenu();
		Assert.assertEquals(asset, myAccountPage.getAsset());
		Assert.assertEquals(moneyType + amount + ".00", myAccountPage.getInvestmentAmount());
		Assert.assertEquals(moneyType + correct, myAccountPage.getAmountIfCorrect());
		Assert.assertEquals(moneyType + "0.00", myAccountPage.getAmountIfIncorrect());
		Assert.assertEquals(level.replace("0", "").replace(",", ""), myAccountPage.getLevel().replace("0", "").replace(",", ""));
		Assert.assertTrue(myAccountPage.isDynamicsInvestment());
		return balanceAfter;
	}
}
