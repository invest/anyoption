package com.anyoption.autotest.smoketest;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.MessagesPage;
import com.anyoption.autotest.utils.CreateUser;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class ReadWelcomeMessage {
	private WebDriver driver;
	private CreateUser user;
	private String browserType;
	/**
	 * 
	 * @param driver
	 * @param user
	 */
	public ReadWelcomeMessage(WebDriver driver, CreateUser user, String browserType) {
		this.driver = driver;
		this.user = user;
		this.browserType = browserType;
	}
	/**
	 * Reads welcome message
	 * @param skin
	 * @param welcome
	 * @param dearText
	 * @param deatailsText
	 */
	public void readWelcomeMessage(String skin, String welcome, String dearText, String deatailsText) {
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		MessagesPage messagesPage = new MessagesPage(driver, browserType);
		headerMenuPage.switchDriver();
		Assert.assertEquals(1, headerMenuPage.getCountNotReadedMessages());
		headerMenuPage.openMessages();
		driver.switchTo().defaultContent();
		Assert.assertEquals(welcome, messagesPage.getMailSubject(2));
		messagesPage.openEmail(2);
		messagesPage.switchDriver();
		Assert.assertEquals(dearText + " " + user.getFirstName() + ",", messagesPage.getWelcomeMessage());
		Assert.assertEquals(deatailsText, messagesPage.getRecordedDetailsText());
		Assert.assertEquals(user.getEMail().toLowerCase(), messagesPage.getUsernameFromMail());
		Assert.assertEquals("123456", messagesPage.getPassFromMail());
		Assert.assertEquals(user.getEMail(), messagesPage.getEmailFromWelcomeMail());
		Assert.assertEquals("359-" + user.getPhone(), messagesPage.getPhoneFromMail());
		driver.switchTo().defaultContent();
	}
}
