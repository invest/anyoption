package com.anyoption.autotest.smoketest;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.LongTermPage;
import com.anyoption.autotest.pages.MyAccountPage;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class LongTermInvest {
	private WebDriver driver;
	private String balance;
	/**
	 * 
	 * @param driver
	 * @param balance
	 */
	public LongTermInvest(WebDriver driver, String balance) {
		this.driver = driver;
		this.balance = balance;
	}
	/**
	 * Makes long terms investment
	 * @return String
	 */
	public String makeLongTermInvest() {
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		LongTermPage longTermPage = new LongTermPage(driver);
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		String moneyType = balance.replaceAll("\\d+.*", "");
		headerMenuPage.openOptionTradingMenu();
  		longTermPage.openLongTerm();
  		longTermPage.setMarket(1);
  		String id = longTermPage.getOptionId(0);
  		longTermPage.selectUnitsCount(id, "1");
  		int profit = longTermPage.getProfit(id);
  		String price = longTermPage.getPrice(id);
  		int priceInt = Integer.parseInt(price.replace(moneyType, "").replace(" ", ""));
  		Assert.assertTrue(price.contains(moneyType));
  		String returnValue = longTermPage.getReturnValue(id);
  		double returnValueDouble = Double.parseDouble(returnValue.replace(moneyType, "").replace(" ", ""));
  		double value = (priceInt * (1 + (double)profit / 100));
  		Assert.assertEquals(returnValueDouble, value);
  		Assert.assertTrue(returnValue.contains(moneyType));
  		longTermPage.buyLongOption(id);
  		Assert.assertTrue(longTermPage.isPrintButtonDiplayed(id));
  		String balanceAfter = headerMenuPage.getBalance();
  		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
  		Assert.assertEquals(balanceInt - (priceInt * 100), balanceAfterInt);
  		headerMenuPage.openMyAccountMenu();
  		MyAccountPage myAccountPage = new MyAccountPage(driver);
  		String amountFromMyAccount = myAccountPage.getInvestmentAmount();
  		int amountFromMyAccountInt = Integer.parseInt(amountFromMyAccount.replaceAll("[^\\d]", ""));
  		Assert.assertEquals(priceInt * 100, amountFromMyAccountInt);
  		Assert.assertTrue(amountFromMyAccount.contains(moneyType));
  		String correctFromMyAcc = myAccountPage.getAmountIfCorrect();
  		Assert.assertEquals(moneyType + (int)(returnValueDouble * 100), correctFromMyAcc.replace(".", ""));
  		String incorrectFromMyAcc = myAccountPage.getAmountIfIncorrect();
  		if (driver.getCurrentUrl().contains("bgtest")) {
  			System.out.println("bg test bug in long term remove after fix");
  		} else {
  			Assert.assertEquals(moneyType + "0.00", incorrectFromMyAcc);
  		}	
  		return balanceAfter;
	}
}
