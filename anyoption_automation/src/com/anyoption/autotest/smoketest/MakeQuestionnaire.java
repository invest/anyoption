package com.anyoption.autotest.smoketest;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.anyoption.autotest.pages.HeaderMenuPage;
import com.anyoption.autotest.pages.MessagesPage;
import com.anyoption.autotest.pages.QuestionnairePage;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class MakeQuestionnaire {
	private WebDriver driver;
	private String operationSys;
	private String browserType;
	/**
	 * 
	 * @param driver
	 * @param operationSys
	 */
	public MakeQuestionnaire(WebDriver driver, String operationSys, String browserType) {
		this.driver = driver;
		this.operationSys = operationSys;
		this.browserType = browserType;
	}
	/**
	 * Makes questionnaire
	 * @param skin
	 * @param title
	 * @throws IOException
	 */
	public void makeQuestionnaire(String skin, String title, String subjectText) throws IOException {
		QuestionnairePage questionnairePage = new QuestionnairePage(driver);
		HeaderMenuPage headerMenuPage = new HeaderMenuPage(driver);
		MessagesPage messagesPage = new MessagesPage(driver, browserType);
		questionnairePage.switchDriver();
		Assert.assertEquals(title, questionnairePage.getPopUpQuestionnaireTitle());
		questionnairePage.selectAnnualIncome(1);
		questionnairePage.selectSourceIncome(1);
		questionnairePage.selectEducation(1);
		questionnairePage.selectEmoploymentStatus(1);
		questionnairePage.selectProfession(1);
		questionnairePage.moveNext();
		questionnairePage.selectTradingExperience(1);
		questionnairePage.selectFinancialDerivative(1);
		questionnairePage.selectTradeCount(1);
		questionnairePage.selectNatureOfTrading(1);
		questionnairePage.checkRiskCheckBox(operationSys);
		questionnairePage.tradeNow(operationSys);
		driver.switchTo().defaultContent();
		questionnairePage.checkInPopUp();
		questionnairePage.confirmPopUp();
		Assert.assertTrue(questionnairePage.isBinaryOptionsActive());
		headerMenuPage.switchDriver();
		headerMenuPage.openMessages();
		driver.switchTo().defaultContent();
		Assert.assertEquals(subjectText, messagesPage.getMailSubject(1));
		messagesPage.openEmail(1);
		Assert.assertEquals(subjectText, messagesPage.getSubjectFromTradeMail());
	}
}
