package com.anyoption.autotest.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.anyoption.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class BubblesPage {
	private WebDriver driver;
	@FindBy(css = "body > ui-view > div.dashboard-container.no-select.ng-scope > div.dashboard > img.go-button.glow")
	private WebElement goButton;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div.page_bgr_trade > ul "
			+ "> li.trade_page_tabs_h.trade_page_tabs_bubbles_h > a > span > span")
	private WebElement bubblesMenu;
	@FindBys({@FindBy(css = "body > ui-view > div.tutorial.ng-scope.visible > div > div.tutorial-ok")})
	private List<WebElement> bublesTutorialOkButtons;
	@FindBy(css = "#popUpAORegulationStatusCodeBlock_not-login > div > div.clear.taC > div > a.trade_buton_login.trade_buton_login_h")
	private WebElement openAccButton;
	@FindBy(css = "#content > div.bubble-payout-wrap.ng-scope > div > div > p.bubble-payout-text")
	private WebElement profiteLabel;
	@FindBy(css = "#menu-container > nav > ul > li.menu-invest-li > div > a")
	private WebElement investAmountMenu;
	@FindBys({@FindBy(css = "#menu-container > nav > ul > li.menu-invest-li > ul > li > a")})
	private List<WebElement> amountList;
	@FindBy(css = "#menu-container > nav > ul > li.menu-return-li > div > a")
	private WebElement returnLabelIfCorrect;
	@FindBy(css = "#menu-container > nav > ul > li.menu-incorrect-li > div > a")
	private WebElement returnIfIncorectLabel;
	@FindBy(css = "body > ui-view > div.dashboard-container.no-select.ng-scope > div.dashboard > div.timer-wrap")
	private WebElement timer;
	@FindBy(css = "#content > div.notification.ng-scope")
	private WebElement notificationElement;
	@FindBys({@FindBy(css = "body > ui-view > div.endsplash-container.no-select.ng-scope > div > div > div.bottom > div.row.amount > div")})
	private List<WebElement> returnAmountFromEndWindows;
	@FindBy(css = "#content > div.notification.ng-scope > div")
	private WebElement notificationMessage;
	@FindBy(css = "body > ui-view > div.tutorial.ng-scope.visible > div.step.step1.visible > div.step1-text > table > tbody > tr > td")
	private WebElement tutorialStep1Text;
	@FindBy(css = "body > ui-view > div.tutorial.ng-scope.visible > div.step.step1.visible > div.tutorial-ok > table > tbody > tr > td")
	private WebElement tutorisalStep1Button;
	@FindBy(css = "body > ui-view > div.tutorial.ng-scope.visible > div.step.step2.visible > div.step2-text > table > tbody > tr > td")
	private WebElement tutorialStep2Text;
	@FindBy(css = "body > ui-view > div.tutorial.ng-scope.visible > div.step.step2.visible > div.tutorial-ok > table > tbody > tr > td")
	private WebElement tutorialStep2Button;
	@FindBy(css = "body > ui-view > div.dashboard-container.no-select.ng-scope > div.help-icon")
	private WebElement helpIcon;
	@FindBy(css = "#menu-container > nav > ul > li.menu-assets-li > div > a")
	private WebElement assetDropDown;
	@FindBys({@FindBy(css = "#menu-container > nav > ul > li.menu-assets-li > ul > li")})
	private List<WebElement> marketsList;	
//	@FindBy(css = "#content > div.cancel-notification.ng-scope")
	@FindBy(xpath = "//*[@id='content']/div[4]")
	private WebElement cancelNotification;
//	@FindBy(css = "#content > div.cancel-notification.ng-scope > div > span.message-text")
	@FindBy(xpath = "//*[@id='content']/div[4]/div/span[1]")
	private WebElement cancelInvMessage;
	@FindBy(id = "cancel-timer")
	private WebElement cancelTimer;
//	@FindBy(css = "#content > div.cancel-notification.ng-scope > div > div.cancel-button.ng-binding")
	@FindBy(xpath = "//*[@id='content']/div[4]/div/div[2]")
	private WebElement cancelInvButton;
	@FindBy(id = "other-amount-input")
	private WebElement otherAmountInput;
	@FindBy(css = "#popUpAORegulationStatusCodeBlock_low-balance > div > div.taC.mt20")
	private WebElement noDepositPopUpText;
	@FindBy(css = "#popUpAORegulationStatusCodeBlock_low-balance > div > div.clear.taC > div > a")
	private WebElement depositButton;
	@FindBy(id = "popUpAORegulationStatus")
	private WebElement loginPopUp;
	@FindBy(css = "#popUpAORegulationStatusCodeBlock_not-login > div > div.clear.taC > div > a.trade_buton_login.trade_buton_register_h > span > span")
	private WebElement loginButton;
	@FindBy(css = "#popUpAORegulationStatus > i")
	private WebElement regulationPopUpCloseButton;
	@FindBy(xpath = "//*[@id='content']/div[4]/div/div[1]")
	private WebElement cancelCheckBoxText;
	/**
	 * 
	 * @param driver
	 */
	public BubblesPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Chooses market by index
	 * @param index
	 */
	public void chooseMarketByIndex(int index) {
		assetDropDown.click();
		marketsList.get(index).click();
	}
	/**
	 * Clicks do not show cancel investment pop up link
	 */
	public void clickDoNotShowCancelInvLink() {
		cancelCheckBoxText.click();
	}
	/**
	 * Gets cancel check box text
	 * @return
	 */
	public String getCancelCheckBox() {
		return cancelCheckBoxText.getText();
	}
	/**
	 * Closes regulation pop up
	 */
	public void closeRegulationPopUp() {
		regulationPopUpCloseButton.click();
	}
	/**
	 * Clicks login button
	 */
	public void clickLoginButton() {
		loginButton.click();
	}
	/**
	 * Is login pop up displayed
	 * @return
	 */
	public boolean isLoginPopUpDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 5);
		try {
			wait.until(ExpectedConditions.attributeToBe(loginPopUp, "style", "display: block;"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Clicks deposit button from no deposit pop up
	 */
	public void clickDepositButton() {
		depositButton.click();
	}
	/**
	 * Gets no deposit message
	 * @return
	 */
	public String getNoDepositMessage() {
		String text = noDepositPopUpText.getText();
		return text;
	}
	/**
	 * Double clicks cancel investment button
	 */
	public void doubleClickCancelLink() {
		Actions action = new Actions(driver);
  		action.doubleClick(cancelInvButton).perform();
	}
	/**
	 * Clicks cancel investment link
	 */
	public void clickCancelLink() {
		cancelInvButton.click();
	}
	/**
	 * Gets cancel investment link text
	 * @return String
	 */
	public String getCancelLinkText() {
		String text = cancelInvButton.getText();
		return text;
	}
	/**
	 * Gets time to cancel investment
	 * @return String
	 */
	public String getTimeToCancel() {
		String time[] = cancelTimer.getText().split(":");
		String secs = time[1].replace("0", "");
		return secs;
	}
	/**
	 * Gets cancel investment message
	 * @return
	 */
	public String getCancelMessage() {
		String text = cancelInvMessage.getText();
		return text;
	}
	/**
	 * Is cancel notification displayed
	 * @return
	 */
	public boolean isCancelNotificationDisplayed() {
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			wait.until(ExpectedConditions.attributeToBe(cancelNotification, "class", "cancel-notification ng-scope active"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Gets expiry level
	 * @param isWin
	 * @return String
	 */
	public String getExpLevel(boolean isWin) {
		String css;
		if (isWin == true) {
			css = "body > ui-view > div.endsplash-container.no-select.ng-scope > div.endsplash-wrap.won > div > div.bottom > div.row.expiry-level > div > span.inner-value.ng-binding";
		} else {
			css = "body > ui-view > div.endsplash-container.no-select.ng-scope > div.endsplash-wrap.lost > div > div.bottom > div.row.expiry-level > div > span.inner-value.ng-binding";
		}
		String level = driver.findElement(By.cssSelector(css)).getText();
		return level;
	}
	/**
	 * Gets time for expiry
	 * @param isWin
	 * @return String
	 */
	public String getExpTime(boolean isWin) {
		String css;
		if (isWin == true) {
			css = "body > ui-view > div.endsplash-container.no-select.ng-scope > div.endsplash-wrap.won > div > div.bottom > div.row.expiry-time > div > span.inner-value.ng-binding";
		} else {
			css = "body > ui-view > div.endsplash-container.no-select.ng-scope > div.endsplash-wrap.lost > div > div.bottom > div.row.expiry-time > div > span.inner-value.ng-binding";
		}
		String time = driver.findElement(By.cssSelector(css)).getText();
		return time;
	}
	/**
	 * Gets amount form expiry pop up
	 * @param isWin
	 * @return String
	 */
	public String getExpAmount(boolean isWin) {
		String css;
		if (isWin == true) {
			css = "body > ui-view > div.endsplash-container.no-select.ng-scope > div.endsplash-wrap.won > div > div.bottom > div.row.amount > div";
		} else {
			css = "body > ui-view > div.endsplash-container.no-select.ng-scope > div.endsplash-wrap.lost > div > div.bottom > div.row.amount > div";
		}
		String amount = driver.findElement(By.cssSelector(css)).getText();
		return amount;
	}
	/**
	 * Gets current asset name
	 * @return
	 */
	public String getAssetName() {
		return assetDropDown.getText();
	}
	/**
	 * Gets message from window after invest is settled
	 */
	public String getMessageFromEndWindow(boolean isWon) {
		String className;
		if (isWon == true) {
			className = "endsplash-wrap.won";
		} else {
			className = "endsplash-wrap.lost";
		}
		final String css = "body > ui-view > div.endsplash-container.no-select.ng-scope > div." + className + " > div > div.top > div.row.message > div";
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(new ExpectedCondition<Boolean>() {

			@Override
			public Boolean apply(WebDriver driver) {
				String text = driver.findElement(By.cssSelector(css)).getText();
				if (text.equals("")) {
					System.out.println("wait message");
					return false;
				} else {
					return true;
				}
			}
			
		});
		String message = driver.findElement(By.cssSelector(css)).getText();
		return message;
	}
	/**
	 * Gets profit amount after investment 
	 * @param currency
	 * @return Integer
	 */
	public int getReturnValueFromEndWindow(String currency) {
		WebDriverWait wait = new WebDriverWait(driver, 15000);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("body > ui-view > div.endsplash-container.no-select.ng-scope > div.endsplash-wrap")));
		String amount = null;
		for (WebElement win : returnAmountFromEndWindows) {
			UtilsMethods.sleep(1500);
			if (! win.getText().equals("")) {
				amount = win.getText().replace(currency, "").replace(" ", "");
				break;
			}
		}
		return Integer.parseInt(amount);
	}
	/**
	 * Waits for positive notification
	 */
	public String getPositiveNotification() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.attributeToBe(notificationElement, "class", "notification ng-scope active success"));
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				String text = notificationMessage.getText();
				if (text.equals("")) {
					return false;
				} else {
					return true;
				}
			}
		});
		return notificationMessage.getText();
	}
	/**
	 * Waits for negative notification
	 */
	public String getNegativeNotification() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.attributeToBe(notificationElement, "class", "notification ng-scope active error"));
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				String text = notificationMessage.getText();
				if (text.equals("")) {
					System.out.println("wait");
					return false;
				} else {
					return true;
				}
			}
		});
		return notificationMessage.getText();
	}
	/**
	 * Gets amount if incorrect
	 * @param currency
	 * @return
	 */
	public int getReturnIfIncorrect(String currency) {
		String amount = returnIfIncorectLabel.getText();
		return Integer.parseInt(amount.replace(currency, ""));
	}
	/**
	 * Gets amount if correct
	 * @param currency
	 * @return
	 */
	public double getReturnIfCorrect(String currency) {
		String amount = returnLabelIfCorrect.getText().replace(currency, "");
		String centsCss = "#menu-container > nav > ul > li.menu-return-li > div > a > span.small-change.ng-binding";
		String cents = driver.findElement(By.cssSelector(centsCss)).getText();
		String returnValue = amount.replace(cents, "").replace(" ", "") + "." + cents;
		return Double.parseDouble(returnValue);
	}
	/**
	 * Choose amount
	 * @param index
	 * @param currency
	 * @return Integer
	 */
	public int chooseAmountForInvest(int index, String currency) {
		investAmountMenu.click();
		amountList.get(index).click();
		String amount = investAmountMenu.getText();
		return Integer.parseInt(amount.replace(currency, ""));
	}
	/**
	 * Set custom amount and return it 
	 * @param amount
	 * @return
	 */
	public int setCustomAmount(String amount) {
		investAmountMenu.click();
		otherAmountInput.clear();
		otherAmountInput.sendKeys(amount);
		investAmountMenu.click();
		return Integer.parseInt(amount);
	}
	/**
	 * Expands the bubble
	 */
	public void expandBubble() {
		Actions actions = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("bubble-resizer")));
		WebElement resizer = driver.findElement(By.id("bubble-resizer"));
		actions.clickAndHold(resizer);
		actions.moveByOffset(0, 30);
		actions.build().perform();
		actions.release();
		actions.click(resizer).perform();
	}
	/**
	 * Moves bubble
	 * @return
	 */
	public int moveBubble() {
		Actions actions = new Actions(driver);
		WebElement bubble = driver.findElement(By.id("elip-frame"));
		int profit = 0;
		do {
			actions.clickAndHold(bubble);
			actions.moveByOffset(0, 20);
			actions.build().perform();
			actions.click();
			actions.build().perform();
			UtilsMethods.sleep(2500);
			profit = this.getProfite();
		} while (profit == 0);
		return profit;
	}
	/**
	 * Gets profit
	 * @return
	 */
	public int getProfite() {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				String text = profiteLabel.getText();
				if (text.equals("")) {
					return false;
				} else {
					return true;
				}
			}
		});
		String label = profiteLabel.getText();
		return Integer.parseInt(label.replace("%", ""));
	}
	/**
	 * Clicks open account button
	 */
	public void clickOpenAccButton() {
		openAccButton.click();
	}
	/**
	 * Switches driver to bubbles
	 */
	public void switchDriverToBubbles() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("bubbles"));
	}
	/**
	 * Pass tutorial
	 */
	public void passTutorial() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		int size = bublesTutorialOkButtons.size();
		if (size != 0) {
			for (int i = 0; i < size; i++) {
				wait.until(ExpectedConditions.visibilityOf(bublesTutorialOkButtons.get(i)));
				bublesTutorialOkButtons.get(i).click();
			}
		}
	}
	/**
	 * Pass tutorial and verifies messages
	 * @param step1Message
	 * @param Step2Message
	 */
	public void passTutorial(String step1Message, String Step2Message) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		int size = bublesTutorialOkButtons.size();
		if (size == 0) {
			helpIcon.click();
		}	
		wait.until(ExpectedConditions.visibilityOf(tutorialStep1Text));
		Assert.assertEquals(step1Message, tutorialStep1Text.getText());
		tutorisalStep1Button.click();
		wait.until(ExpectedConditions.visibilityOf(tutorialStep2Text));
		Assert.assertEquals(Step2Message, tutorialStep2Text.getText());
		tutorialStep2Button.click();
	}
	/**
	 * Opens bubbles tab
	 */
	public void openBubbles() {
		try {
			bubblesMenu.click();
		} catch (WebDriverException e) {
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", bubblesMenu);
		}
	}
	/**
	 * Makes investment
	 */
	public void makeBubblesInvestment() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("body > ui-view > div.dashboard-container.no-select.ng-scope > div.dashboard > img.go-button.glow")));
		WebDriverWait waitNotification = new WebDriverWait(driver, 8);
		int tryCount = 0;
		
		for (int i = 0; i < 8; i++) {
			goButton.click();
			try {
				waitNotification.until(ExpectedConditions.attributeToBe(notificationElement, "class", "notification ng-scope active success"));
				break;
			} catch (TimeoutException e) {
				System.out.println("Try again");
				tryCount = tryCount + 1;
				if (tryCount >= 2) {
					assetDropDown.click();
					marketsList.get(tryCount - 1).click();
					UtilsMethods.sleep(3000);
				}
			}
		}
	}
	/**
	 * Clicks go button
	 */
	public void clickGoButton() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("body > ui-view > div.dashboard-container.no-select.ng-scope > div.dashboard > img.go-button.glow")));
		goButton.click();
	}
}
