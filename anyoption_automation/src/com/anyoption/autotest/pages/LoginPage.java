package com.anyoption.autotest.pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.Optional;

import com.anyoption.autotest.common.DBLayer;
import com.anyoption.autotest.common.User;
import com.anyoption.autotest.utils.BaseTest;
import com.anyoption.autotest.utils.ChangeUrl;
import com.anyoption.autotest.utils.CreateUser;
import com.anyoption.autotest.utils.ScreenShot;

/**
 * 
 * @author vladimirm
 *
 */
public class LoginPage {
	private WebDriver driver;
	private String browserType;
	private String testUrl;
	private String skin;
	@FindBy(id = "loginStrip_link")
	private WebElement loginLink;
	@FindBy(id = "login:j_username")
	private WebElement userNameInput;
	@FindBy(id = "login:j_password")
	private WebElement passwordInput;
	@FindBy(css = "#login > div:nth-child(1) > div:nth-child(8) > a.buton_blue.but_sprite.clear.mt10 > span > span")
	private WebElement loginButton;
	@FindBy(id = "logoutForm:logoutLink")
	private WebElement logoutLink;
	@FindBy(id = "emailAlertDiv")
	private WebElement emailAlert;
	@FindBy(css = "#emailAlertDiv > table > tbody > tr > td:nth-child(3) > img")
	private WebElement emailAlertCloseButton;
	@FindBy(id = "login:errorlogin")
	private WebElement errorLogin;
	@FindBy(id = "login:errorloginMsg")
	private WebElement errorLoginMessage;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div > h1")
	private WebElement loginScreenTitle;
	@FindBy(css = "#login > div:nth-child(1) > span")
	private WebElement loginScreenMessage;
	@FindBy(id = "loginPage_forgotPassword")
	private WebElement forgotPassLink;
	@FindBys({@FindBy(id = "forgot_password_btn_in")})
	private List<WebElement> forgotPassSubmitButton;
	@FindBy(id = "user-name-holder")
	private WebElement forgotPassUserName;
	@FindBy(css = "#user-name-holder > span")
	private WebElement forgotPassError;
	@FindBy(id = "username")
	private WebElement forgotPassUsernameInput;
	@FindBy(id = "email")
	private WebElement forgotPassEmail;
	@FindBy(id = "passwordForm:errorloginMsg")
	private WebElement forgotPassInfoMessage;
	@FindBy(id = "popUpAORegulationStatusCodeBlock_restricted")
	private WebElement regulationPopUp;
	@FindBy(css = "#popUpAORegulationStatusCodeBlock_restricted > div > div.checkbox_content.txt_size_s > label > i")
	private WebElement riskCheckBox;
	@FindBy(css = "#popUpAORegulationStatusCodeBlock_restricted > div > div.regulation_status_popUp_button > a > span > span")
	private WebElement regulationPopUpOkButton;
	/**
	 * 
	 * @param driver
	 * @param browserType
	 * @param testUrl
	 */
	public LoginPage(WebDriver driver, String browserType, String testUrl) {
		this.driver = driver;
		this.browserType = browserType;
		this.testUrl = testUrl;
		PageFactory.initElements(driver, this);
	}
	/**
	 * 
	 * @param driver
	 * @param browserType
	 * @param testUrl
	 * @param skin
	 */
	public LoginPage(WebDriver driver, String browserType, String testUrl, String skin) {
		this.driver = driver;
		this.browserType = browserType;
		this.testUrl = testUrl;
		this.skin = skin;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Gets forgot password info message
	 * @return String
	 */
	public String getForgotPassInfoMessage() {
		return forgotPassInfoMessage.getText();
	}
	/**
	 * Is forgot pass info message displayed
	 * @return boolean
	 */
	public boolean isForgotPassInfoMessageDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 6);
		try {
			wait.until(ExpectedConditions.attributeToBe(forgotPassInfoMessage, "class", "info_messages"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Gets forgot pass email
	 * @return String
	 */
	public String getForgotPassEmail() {
		return forgotPassEmail.getText();
	}
	/**
	 * Types username in forgot password input
	 * @param username
	 */
	public void setForgotPassUsername(String username) {
		forgotPassUsernameInput.clear();
		forgotPassUsernameInput.sendKeys(username);
	}
	/**
	 * Gets forgot password error message
	 * @return String
	 */
	public String getForgotPassErrorMessage() {
		return forgotPassError.getText();
	}
	/**
	 * Is forgot pass error displayed
	 * @return boolean
	 */
	public boolean isForgotPassErrorDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			wait.until(ExpectedConditions.attributeToBe(forgotPassUserName, "class", "input-row error"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Submits forgot password
	 */
	public void submitForgotPass(int index) {
		forgotPassSubmitButton.get(index).click();
	}
	/**
	 * Gets forgot pass message
	 * @param index
	 * @return String
	 */
	public String getForgotPassMessage(int index) {
		index = index + 1;
		final String css = "#step" + index + " > div.password_text";
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver input) {
				String text = driver.findElement(By.cssSelector(css)).getText();
				if (text.equals("")) {
					return false;
				} else {
					return true;
				}	
			}
		});
		return driver.findElement(By.cssSelector(css)).getText();
	}
	/**
	 * Clicks forgot password link
	 */
	public void clickForgotPassword() {
		forgotPassLink.click();
	}
	/**
	 * Is login screen opened
	 * @return boolean
	 */
	public boolean isLoginScreenOpen() {
		WebDriverWait wait = new WebDriverWait(driver, 8);
		boolean result = true;
		try {
			wait.until(ExpectedConditions.urlContains("/jsp/login.jsf"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Gets login screen message
	 * @return
	 */
	public String getLoginScreenMessage() {
		return loginScreenMessage.getText();
	}
	/**
	 * Gets login screen title
	 * @return String
	 */
	public String getLoginScreenTitle() {
		return loginScreenTitle.getText();
	}
	/**
	 * Gets login error message
	 * @return
	 */
	public String getLoginErrorMessage() {
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver driver) {
					String text = errorLoginMessage.getText();
					if (text.equals("")) {
						return true;
					} else {
						return false;
					}
				}
			});
		} catch (TimeoutException e) {
			System.out.println("No text");
		}
		return errorLoginMessage.getText();
	}
	/**
	 * Gets login error 
	 * @return
	 */
	public String getLoginError() {
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver driver) {
					String text = errorLogin.getText();
					if (text.equals("")) {
						return true;
					} else {
						return false;
					}
				}
			});
		} catch (TimeoutException e) {
			System.out.println("No text");
		}
		return errorLogin.getText();
	}
	/**
	 * Gets currently used users
	 * @return
	 */
	private String getCurrentUsers() {
		String returnedUsers = null;
		if (! BaseTest.users.isEmpty()) {
			returnedUsers = BaseTest.users.toString();
			System.out.println(returnedUsers);
			returnedUsers = returnedUsers.replaceAll("\\[", "");
			returnedUsers = returnedUsers.replaceAll("\\]", "");
		} else {
			returnedUsers = "1111";
		}
		return returnedUsers;
	}
	/**
	 * Clicks login link
	 */
	public void clickLoginLink() {
//		if (browserType.equalsIgnoreCase("chrome")) {
//			driver.switchTo().frame("header");
//			loginLink.click();
//			driver.switchTo().defaultContent();
//		} else {	
		String currentUrl = driver.getCurrentUrl();
		currentUrl = currentUrl.replace("/mobile-trading", "");
		String loginUrl = currentUrl + "/jsp/login.jsf";
		driver.get(loginUrl);
//		}	
	}
	/**
	 * Logout 
	 */
	public void logout() {
		driver.switchTo().frame("header");
		try {
			WebDriverWait wait = new WebDriverWait(driver, 15);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("logoutForm:logoutLink")));
			logoutLink.click();
		} catch (Exception e) {
			System.out.println("No user");
		} 
		driver.switchTo().defaultContent();
	}
	/**
	 * Logout
	 * @param result - if test is failed make screenshot
	 * @param parameterSkin - skin 
	 * @param browser - browser type optional
	 * @return boolean
	 * @throws IOException
	 */
	public boolean logout(ITestResult result, String parameterSkin, @Optional String browser) throws IOException {
		if (ITestResult.FAILURE == result.getStatus()) {
			String brName = "";
			if (browser != null) {
				brName = browser.replace(" ", "");
			}
			String name = result.getMethod().getMethodName();
			String screenShotName = name + parameterSkin + brName;
			ScreenShot scShot = new ScreenShot();
			scShot.takeScreenShot(driver, screenShotName);
			System.out.println("Screenshot generated");
		} else {
			System.out.println("The test pass");
		}
//		this.logout();
		return true;
	}
	/**
	 * Logins with user fetched from data base
	 * @param skinId
	 * @param makeTester - boolean - if is true make the user tester
	 * @return HasMap<String, String> - keys are names of columns in table users with lower case
	 * @throws Exception
	 */
	public HashMap<String, String> loginWithUserFromDB(String sqlQuery, boolean makeTester) throws Exception {
		//remove after fix problem on new environment
		String currentUrl = driver.getCurrentUrl();
		if (! skin.equals("EN")) {
			currentUrl = currentUrl.replace("www", skin.toLowerCase());
			ChangeUrl changeUrl = new ChangeUrl();
			String testUrlNow = changeUrl.changeUrl(testUrl, skin);
			driver.get(testUrlNow);
		}
		
		DBLayer db = new DBLayer();
  		String replace = getCurrentUsers();
  		String sqlQueryNow = sqlQuery.replace("check", replace);
  		ArrayList<String[]> results = db.getResultsFromDBImmediately(sqlQueryNow, false, 2);
  		if (results.size() == 0) {
  			throw new SkipException("Skipping the test case no users");
  		}
  		
  		if (BaseTest.users.contains(results.get(0)[0])) {
  			System.out.println("The user is already used");
  			for (int i = 0; i < 2; i++) {
	  			replace = getCurrentUsers();
	  			sqlQueryNow = sqlQuery.replace("check", replace);
				results = db.getResultsFromDBImmediately(sqlQueryNow, false, 2);
				if (results.size() == 0) {
		  			throw new SkipException("Skipping the test case no users");
		  		}
				if (! BaseTest.users.contains(results.get(0)[0])) {
					break;
				}
  			}	
  		}	
  		
//		System.out.println(results.get(0)[3]);
		String userName = results.get(0)[3];
		String userId = results.get(0)[0];
		BaseTest.users.add(userId);
		System.out.println(userName);
		String email = results.get(0)[14];
		String firstName = results.get(0)[6];
		String lastName = results.get(0)[7];
		String balance = results.get(0)[1];
		System.out.println("User_id: " + userId);
		String pass = "123456";
		HashMap<String, String> map = new HashMap<>();
		map.put("user_name", userName);
		map.put("id", userId);
		map.put("email", email);
		map.put("first_name", firstName);
		map.put("last_name", lastName);
		map.put("balance", balance);
//		BaseTest.users.add(userId);
		if (makeTester == true) {
			User.updateUserToTester(email, userId);
		}
		this.clickLoginLink(); //change with driver.get() for time safe
		
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("login:j_username")));
		this.setUsername(userName);
		this.setPassword(pass);
		loginButton.click();
		//Remove after fix problem on new environment
		if (! testUrl.contains("bgtestenv")) {
			driver.navigate().refresh();
		}
		WebDriverWait messageWait = new WebDriverWait(driver, 1);
		try {
			messageWait.until(ExpectedConditions.attributeToBe(emailAlert, "style", "display: block;"));
			emailAlertCloseButton.click();
		} catch (TimeoutException e) {
			
		}
		
		try {
			messageWait.until(ExpectedConditions.attributeToBe(regulationPopUp, "style", "display: block;"));
			riskCheckBox.click();
			regulationPopUpOkButton.click();
			
		} catch (TimeoutException e) {
			
		}
		
		return map;
	}
	/**
	 * Logins with desired user
	 */
	public void loginWithDesiredUser(String url, CreateUser user) {
		driver.get(url);
		userNameInput.clear();
		userNameInput.sendKeys(user.getEMail());
		passwordInput.sendKeys("123456");
		loginButton.click();
	}
	/**
	 * Clicks login button
	 */
	public void clickLoginButton() {
		loginButton.click();
	}
	/**
	 * Sets username
	 */
	public void setUsername(String userName) {
		userNameInput.clear();
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			String script = "document.getElementById('login:j_username').value='" + userName +"'";
			((JavascriptExecutor) driver).executeScript(script);
		} else {
			userNameInput.sendKeys(userName);
		}	
	}
	/**
	 * Sets password
	 */
	public void setPassword(String pass) {
		passwordInput.clear();
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			String script = "document.getElementById('login:j_password').value='" + pass +"'";
			((JavascriptExecutor) driver).executeScript(script);
		} else {
			passwordInput.sendKeys(pass);
		}	
	}
}
