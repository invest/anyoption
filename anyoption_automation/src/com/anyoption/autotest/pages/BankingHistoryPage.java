package com.anyoption.autotest.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class BankingHistoryPage {
	WebDriver driver;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div.my_account_menu > div:nth-child(3) > ul > li:nth-child(5) > a")
	private WebElement bankingHistoryLink;
	@FindBys({@FindBy(xpath = "//*[@id='transactionsForm:data:tbody_element']/tr/td[1]/span")})
	private List<WebElement> transactionsId;
	@FindBys({@FindBy(xpath = "//*[@id='transactionsForm:data:tbody_element']/tr/td[3]/span")})
	private List<WebElement> descriptions;
	@FindBys({@FindBy(xpath = "//*[@id='transactionsForm:data:tbody_element']/tr/td[4]/span")})
	private List<WebElement> credits;
	@FindBys({@FindBy(xpath = "//*[@id='transactionsForm:data:tbody_element']/tr/td[5]/span")})
	private List<WebElement> debits;
	/**
	 * 
	 * @param driver
	 */
	public BankingHistoryPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Gets debit
	 * @param index
	 * @return String
	 */
	public String getDebit(int index) {
		String debit = debits.get(index).getText();
		return debit;
	}
	/**
	 * Gets credit
	 * @param index
	 * @return String
	 */
	public String getCredit(int index) {
		String credit = credits.get(index).getText();
		return credit;
	}
	/**
	 * Gets description
	 * @param index
	 * @return String
	 */
	public String getDescription(int index) {
		String desc = descriptions.get(index).getText();
		return desc;
	}
	/**
	 * Gets transaction id
	 * @param index
	 * @return
	 */
	public String getTransactionId(int index) {
		String id = transactionsId.get(index).getText();
		return id;
	}
	/**
	 * Opens banking history menu
	 */
	public void openBankingHistory() {
		bankingHistoryLink.click();
	}
}
