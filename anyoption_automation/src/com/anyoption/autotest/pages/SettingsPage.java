package com.anyoption.autotest.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class SettingsPage {
	WebDriver driver;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div.my_account_menu > div:nth-child(4) > ul > li:nth-child(4) > a")
	private WebElement settingsLink;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div.my_account_main > div > div > div > div > div > span.slider-holder")
	private WebElement settingsSlider;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div.my_account_main > div > div > div > h2")
	private WebElement screenTitle;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div.my_account_main > div > div > div > div > span")
	private WebElement invCancelPopUpText;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div.my_account_main > div > div > div > p")
	private WebElement settingsDescription;
	/**
	 * 
	 * @param driver
	 */
	public SettingsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Gets settings description
	 * @return String
	 */
	public String getDescription() {
		return settingsDescription.getText();
	}
	/**
	 * Switch slider on
	 */
	public void switchSliderOn() {
		settingsSlider.click();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		String waitCss = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div.my_account_main > div > div > div > div > div";
		wait.until(ExpectedConditions.attributeToBe(By.cssSelector(waitCss), "class", "my-setting-slider"));
	}
	/**
	 * Is slider on 
	 * @return boolean
	 */
	public boolean isSliderOn() {
		String css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div.my_account_main > div > div > div > div > div";
		String attribute = driver.findElement(By.cssSelector(css)).getAttribute("class");
		if (attribute.contains("off")) {
			return false;
		} else {
			return true;
		}
	}
	/**
	 * Gets investment cancel pop up text
	 * @return String
	 */
	public String getPopUpText() {
		return invCancelPopUpText.getText();
	}
	/**
	 * Gets screen title
	 * @return String
	 */
	public String getScreenTitle() {
		return screenTitle.getText();
	}
	/**
	 * Switch off slider
	 */
	public void switchOffSlider() {
		settingsSlider.click();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		String waitCss = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div.my_account_main > div > div > div > div > div";
		wait.until(ExpectedConditions.attributeToBe(By.cssSelector(waitCss), "class", "my-setting-slider off"));
	}
	/**
	 * Opens settings
	 */
	public void openSettings() {
		settingsLink.click();
	}
}
