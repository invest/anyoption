package com.anyoption.autotest.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * 
 * @author vladimirm
 *
 */
public class LongTermPage {
	private WebDriver driver;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div.page_bgr_trade > ul > "
			+ "li.trade_page_tabs_h.trade_page_tabs_special_h > a > span > span")
	private WebElement longTermTabElement;
	@FindBys({@FindBy(className = "buyButtonSmall")})
	private List<WebElement> buyButtons;
	@FindBy(css = "#error_holder > div.clear > div > a.trade_buton_login.trade_buton_login_h.fL.bitcoin_login > span > span")
	private WebElement openAccountButton;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div:nth-child(2) > ul > "
			+ "li.special_elements_oil_h > a > span.special_p.special_elements_oil.vaM")
	private WebElement oilSpecialTab;
	@FindBys({@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div:nth-child(2) > ul > li > a")})
	private List<WebElement> longTermMarkets;
	@FindBys({@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div.pageMain > div.clear > table > tbody > tr")})
	private List<WebElement> marketOptions;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div:nth-child(1) > ul "
			+ "> li.trade_page_tabs_h.trade_page_tabs_bubbles_h > a > span > span")
	private WebElement bubblesMenu;
	@FindBy(css = "#error_holder > div.clear > div > a.trade_buton_login.trade_buton_register_h.fR.bitcoin_login > span > span")
	private WebElement loginButton;
	@FindBy(id = "j_id1699782240_1a288771:0:messages")
	private WebElement noDepositMessage;
	/**
	 * 
	 * @param driver
	 */
	public LongTermPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Gets no deposit message
	 * @return
	 */
	public String getNoDepositMessage() {
		return noDepositMessage.getText();
	}
	/**
	 * Is error pop up displayed
	 * @return
	 */
	public boolean isErrorPopUpDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 5);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("error_holder")));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Clicks login button
	 */
	public void clickLoginButton() {
		loginButton.click();
	}
	/**
	 * Gets market name
	 * @param id
	 * @return String
	 */
	public String getMarketName(String id) {
		String css = "#tr_" + id + " > td:nth-child(1) > table > tbody > tr > td > span:nth-child(1)";
		String name = driver.findElement(By.cssSelector(css)).getText();
		return name;
	}
	/**
	 * Opens bubbles menu from long term
	 */
	public void openBubblesMenu() {
		try {
			bubblesMenu.click();
		} catch (WebDriverException e) {
			System.out.println("Exception open bubbles");
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", bubblesMenu);
		}
	}
	/**
	 * Gets profit in percent
	 * @param id
	 * @return
	 */
	public int getProfit(String id) {
		String profitCss = "#tr_" + id + " > td:nth-child(3)";
		String profit = driver.findElement(By.cssSelector(profitCss)).getText();
		profit = profit.replace(" %", "");
		return Integer.parseInt(profit);
	}
	/**
	 * Is button for printing displayed
	 * @param id
	 * @return
	 */
	public boolean isPrintButtonDiplayed(String id) {
		String css = "#tr_" + id + " > td:nth-child(7) > span.icon_set_1.icon_set_1_print.vaM";
		WebDriverWait wait = new WebDriverWait(driver, 15);
		boolean result;
		try {
			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(css)));
			result = true;
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Opens print investment window
	 * @param id
	 */
	public void openPrintInvestmentWindow(String id) {
		String css = "#tr_" + id + " > td:nth-child(7) > span.icon_set_1.icon_set_1_print.vaM";
		driver.findElement(By.cssSelector(css)).click();
	}
	/**
	 * Buys long term option
	 * @param id
	 */
	public void buyLongOption(String id) {
		String buttonId = "submit_" + id;
		driver.findElement(By.id(buttonId)).click();
	}
	/**
	 * Gets return value
	 * @param id
	 * @return
	 */
	public String getReturnValue(String id) {
		String css = "#tr_" + id + " > td:nth-child(6)";
		String value = driver.findElement(By.cssSelector(css)).getText();
		return value;
	}
	/**
	 * Gets price
	 * @param id
	 * @return
	 */
	public String getPrice(String id) {
		String priceCss = "#tr_" + id + " > td:nth-child(5)";
		String price = driver.findElement(By.cssSelector(priceCss)).getText();
		return price;
	}
	/**
	 * Select units count
	 * @param id - option id
	 * @param value - the count
	 */
	public void selectUnitsCount(String id, String value) {
		String selectId = "select_" + id;
		WebElement selectElement = driver.findElement(By.id(selectId));
		Select select = new Select(selectElement);
		select.selectByValue(value);
	}
	/**
	 * Gets option id
	 * @param index
	 * @return
	 */
	public String getOptionId(int index) {
		index = index + 1;
		String id = marketOptions.get(index).getAttribute("id");
		id = id.replace("tr_", "");
		return id;
	}
	/**
	 * Opens long term tab
	 */
	public void openLongTerm() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 15);
			wait.until(ExpectedConditions.visibilityOf(longTermTabElement));
			longTermTabElement.click();
		} catch (WebDriverException e) {
			System.out.println("Exception is cathed try with javascriptExecutor");
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click();", longTermTabElement);
		}
	}
	/**
	 * Buys long term offer
	 * @param index
	 */
	public void buyLongTerm(int index) {
		int size = buyButtons.size();
		System.out.println(size);
		if (size != 0) {
			buyButtons.get(index).click();
		} else {
			System.out.println("No options for EUR/USD");
			oilSpecialTab.click();
			WebDriverWait wait = new WebDriverWait(driver, 15);
			String waitCss = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div:nth-child(2) > ul > "
					+ "li.special_elements_oil_h.active > a";
			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(waitCss)));
			System.out.println("Buttonns now: " + buyButtons.size());
			buyButtons.get(index).click();
		}
	}
	/**
	 * Clicks oppen acc button from pop up
	 */
	public void clickOpenAccButton() {
		openAccountButton.click();
	}
	/**
	 * Sets market
	 * @param index
	 */
	public void setMarket(int index) {
		longTermMarkets.get(index).click();
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("buyButtonSmall")));
	}
}
