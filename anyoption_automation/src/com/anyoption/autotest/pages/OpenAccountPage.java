package com.anyoption.autotest.pages;


import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
/**
 * 
 * @author vladimirm
 *
 */
public class OpenAccountPage {
	private static final String termsAndConditionsId = "funnel_terms_h_in";
	private static final String openAccountTitleCSS = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div "
			+ "> div.title_h > h1";
	private static final String openAccountButtonId = "btn_open_account_in";
	private WebDriver driver;
	private String brType;
	@FindBy(id = openAccountButtonId)
	private WebElement openAccountButton;
	@FindBy(css = openAccountTitleCSS)
	private WebElement openAccountTitle;
	@FindBy(id = "funnel_firstName")
	private WebElement firstNameInputElement;
	@FindBy(id = "funnel_lastName")
	private WebElement secondNameInputElement;
	@FindBy(id = "funnel_email")
	private WebElement emailInputElement;
	@FindBy(id = "funnel_mobilePhone")
	private WebElement phoneInputElement;
	@FindBy(id = "funnel_password")
	private WebElement passInputElement;
	@FindBy(id = "funnel_passwordConfirm")
	private WebElement passConfirmInputElement;
	@FindBy(id = termsAndConditionsId)
	private WebElement termAndConditionsCheckBox;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div.pageMain > div > h1")
	private WebElement myAccountTitle;
	@FindBy(id = "NewDeposit_h1")
	private WebElement newDepositFirstLabel;
	@FindBy(id = "NewDeposit_h2")
	private WebElement newDepositSecondLabel;
	@FindBy(id = "menu_register")
	private WebElement openAccountMenu;
	@FindBy(css = "#slider1 > li:nth-child(9) > div > a > span > span")
	private WebElement tradeNowButton;
	@FindBy(id = "funnel_drop_countryId")
	private WebElement countryIdInDropDown;
	@FindBy(id = "funnel_drop_li_33")
	private WebElement bulgariaCountryListInDropDown;
	@FindBy(id = "funnel_email_error")
	private WebElement emailError;
	@FindBy(id = "funnel_names_error")
	private WebElement namesError;
	@FindBy(id = "funnel_mobile_error")
	private WebElement phoneError;
	@FindBy(id = "funnel_password_error")
	private WebElement passError;
	@FindBy(id = "funnel_passwordConfirm_error")
	private WebElement confirmPassError;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div.title_h > h1")
	private WebElement registerFormTitle;
	@FindBy(id = "firstName")
	private WebElement cpFirstNameInput;
	@FindBy(id = "lastName")
	private WebElement cpLastNameInput;
	@FindBy(id = "email")
	private WebElement cpEmailInput;
	@FindBy(id = "mobilePhone")
	private WebElement cpPhoneInput;
	@FindBy(id = "password")
	private WebElement cpPasswordInput;
	@FindBy(id = "password2")
	private WebElement cpConfirmPassInput;
	@FindBy(id = "nickname")
	private WebElement cpNickName;
	@FindBy(id = "acceptedTermsAndConditions")
	private WebElement cpTermAndCondsCheckBox;
	@FindBy(css = "#main-container > section > div > div > div:nth-child(2) > form > div.table > div:nth-child(2) > div:nth-child(5) > button")
	private WebElement cpOpenAccButton;
	@FindBy(id = "countryId")
	private WebElement cpCopuntryDropDown;
	@FindBy(id = "funnel_firstName_error_txt_in")
	private WebElement firstNameErrorText;
	@FindBy(id = "funnel_lastName_error_txt_in")
	private WebElement lastNameErrorText;
	@FindBy(id = "funnel_mobilePhone_error_txt_in")
	private WebElement phoneErrorText;
	@FindBy(id = "funnel_password_error_txt_in")
	private WebElement passwordErrorText;
	@FindBy(id = "funnel_passwordConfirm_error_txt_in")
	private WebElement confirmPasswordErrorText;
	@FindBy(id = "funnel_terms_error")
	private WebElement termsError;
	@FindBy(id = "funnel_terms_error_txt_in")
	private WebElement termsErrorText;
	@FindBy(id = "funnel_tooltip_phone")
	private WebElement phoneTooltip;
	@FindBy(id = "funnel_tooltip_password")
	private WebElement passwordTooltip;
	@FindBy(id = "funnel_email_error_txt_in")
	private WebElement emailErrorText;
	@FindBy(id = "funnel_tooltip_name")
	private WebElement namesTooltip;
	@FindBy(id = "funnel_tooltip_email")
	private WebElement emailTooltip;
	/**
	 * 
	 * @param driver
	 * @param brType
	 */
	public OpenAccountPage(WebDriver driver, String brType) {
		this.brType = brType;
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Gets email toolTip
	 * @return String
	 */
	public String getEmailTooltip() {
		String tooltip = "";
		WebDriverWait wait = new WebDriverWait(driver, 3);
		emailInputElement.click();
		try {
			wait.until(ExpectedConditions.attributeContains(emailTooltip, "style", "display: block;"));
			tooltip = emailTooltip.getText();
		} catch (TimeoutException e) {
			
		}
		return tooltip;
	}
	/**
	 * Gets names tooltip
	 * @return String
	 */
	public String getNamesTooltip() {
		String tooltip = "";
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			wait.until(ExpectedConditions.attributeContains(namesTooltip, "style", "display: block;"));
			tooltip = namesTooltip.getText();
		} catch (TimeoutException e) {
			
		}
		return tooltip;
	}
	/**
	 * Gets email error text
	 * @return String
	 */
	public String getEmailError() {
		return emailErrorText.getText();
	}
	/**
	 * Is email error displayed
	 * @return boolean
	 */
	public boolean isEmailErrorDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 8);
		try {
			wait.until(ExpectedConditions.attributeToBe(emailError, "class", "funnel_error_bad"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Gets password tooltip
	 * @return String
	 */
	public String getPasswordTooltip() {
		String tooltip = "";
		WebDriverWait wait = new WebDriverWait(driver, 3);
		passInputElement.click();
		try {
			wait.until(ExpectedConditions.attributeContains(passwordTooltip, "style", "display: block;"));
			tooltip = passwordTooltip.getText();
		} catch (TimeoutException e) {
			
		}
		return tooltip;
	}
	/**
	 * Gets phone tooltip
	 * @return String
	 */
	public String getPhoneTooltip(int index) {
		index = index + 1;
		String tooltip = "";
		phoneInputElement.click();
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			String tooltipId = "funnel_tooltip_phone_" + index;
			wait.until(ExpectedConditions.attributeContains(phoneTooltip, "style", "display: block;"));
			wait.until(ExpectedConditions.attributeToBe(By.id(tooltipId), "style", "display: block;"));
			tooltip = driver.findElement(By.id(tooltipId)).getText();
		} catch (TimeoutException e) {
			
		}
		return tooltip;
	}
	/**
	 * Gets terms error text
	 * @return
	 */
	public String getTermsError() {
		return termsErrorText.getText();
	}
	/**
	 * Is terms error displayed
	 * @return boolean
	 */
	public boolean isTermsErrorDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			wait.until(ExpectedConditions.attributeToBe(termsError, "class", "funnel_error_bad"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Is terms OK
	 * @return boolean
	 */
	public boolean isTermsOK() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			wait.until(ExpectedConditions.attributeToBe(termsError, "class", "funnel_error_good"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Gets confirm password error text
	 * @return String
	 */
	public String getConfirmPassError() {
		return confirmPasswordErrorText.getText();
	}
	/**
	 * Is confirm password error displayed
	 * @return boolean
	 */
	public boolean isConfirmPassErrorDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			wait.until(ExpectedConditions.attributeToBe(confirmPassError, "class", "funnel_error_bad"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Gets password error text
	 * @return String
	 */
	public String getPasswordErrorText() {
		return passwordErrorText.getText();
	}
	/**
	 * Is password error displayed
	 * @return boolean
	 */
	public boolean isPasswordErrorDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			wait.until(ExpectedConditions.attributeToBe(passError, "class", "funnel_error_bad"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Gets phone error text
	 * @return String
	 */
	public String getPhoneError() {
		return phoneErrorText.getText();
	}
	/**
	 * Is phone error displayed
	 * @return boolean
	 */
	public boolean isPhoneErrorDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			wait.until(ExpectedConditions.attributeToBe(phoneError, "class", "funnel_error_bad"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Gets last name error text
	 * @return String
	 */
	public String getSecondNameError() {
		return lastNameErrorText.getText();
	}
	/**
	 * Gets first name error text
	 * @return String
	 */
	public String getFirstNameError() {
		return firstNameErrorText.getText();
	}
	/**
	 * Is names error displayed
	 * @return boolean
	 */
	public boolean isNamesErrorDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			wait.until(ExpectedConditions.attributeToBe(namesError, "class", "funnel_error_bad"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Types nick name in www.copyop.com
	 * @param name
	 */
	public void cpTypeNickName(String name) {
		cpNickName.clear();
		cpNickName.sendKeys(name);
	}
	/**
	 * Is confirm pass OK
	 * @return
	 */
	public boolean isConfirmPassOk() {
		boolean result = false;
		WebDriverWait wait = new WebDriverWait(driver, 10);
		try {
			wait.until(ExpectedConditions.attributeToBe(confirmPassError, "class", "funnel_error_good"));
			result = true;
		} catch (TimeoutException e) {
			result = false;
		}
		if (result == false) {
			passInputElement.click();
			try {
				wait.until(ExpectedConditions.attributeToBe(confirmPassError, "class", "funnel_error_good"));
				result = true;
			} catch (TimeoutException e) {
				result = false;
			}
		}
		return result;
	}
	/**
	 * Is password OK
	 * @return
	 */
	public boolean isPasswordOk() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.attributeToBe(passError, "class", "funnel_error_good"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Is phone OK
	 * @return
	 */
	public boolean isPhoneOk() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.attributeToBe(phoneError, "class", "funnel_error_good"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Is names OK
	 * @return
	 */
	public boolean isNamesOk() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.attributeToBe(namesError, "class", "funnel_error_good"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Is email valid
	 * @return
	 */
	public boolean isEmailOk() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 15);
			wait.until(ExpectedConditions.attributeToBe(emailError, "class", "funnel_error_good"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Select Bg country
	 */
	public void selectCountry() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("funnel_drop_countryId")));
		if (! countryIdInDropDown.getText().equalsIgnoreCase("+359")) {
			countryIdInDropDown.click();
			if (brType.equalsIgnoreCase("firefox")) {
				JavascriptExecutor jse = (JavascriptExecutor)driver;
				jse.executeScript("arguments[0].click();", bulgariaCountryListInDropDown);
			} else {
				bulgariaCountryListInDropDown.click();
			}	
		}
	}
	/**
	 * Selects Bulgaria
	 */
	public void cpSelectCountry() {
		Select select = new Select(cpCopuntryDropDown);
		select.selectByValue("28");//Bulgaria
	}
	/**
	 * Clicks trade now button from front page
	 */
	public void clickTradeNowButton() {
//		WebDriverWait wait = new WebDriverWait(driver, 45);
//		try {
//			wait.until(ExpectedConditions.visibilityOf(tradeNowButton));
//			tradeNowButton.click();
//		} catch (WebDriverException e) {
//			driver.findElement(By.cssSelector("#slider1_nav > li:nth-child(9)")).click();
//			wait.until(ExpectedConditions.visibilityOf(tradeNowButton));
//			tradeNowButton.click();
//		}
		String button = "but_trade_now_but";
		WebDriverWait wait = new WebDriverWait(driver, 90);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(button)));
		driver.findElement(By.className(button)).click();
	}
	/** 
	 * Clicks open account menu
	 */
	public void clickOpenAccMenu() {
		openAccountMenu.click();
	}
	/**
	 * Gets new deposit second message
	 * @return String
	 */
	public String getNewDepositSecondMessage() {
		String message = newDepositSecondLabel.getText();
		return message;
	}
	/**
	 * Gets new deposit first message
	 * @return String
	 */
	public String getNewDepositFirstMessage() {
		String message = newDepositFirstLabel.getText();
		return message;
	}
	/**
	 * Click open account button
	 */
	public void clickOpenAccountButton() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(By.id(openAccountButtonId)));
		if (brType.equalsIgnoreCase("firefox")) {
			try {
				openAccountButton.click();
			} catch (WebDriverException e) {
				JavascriptExecutor jse = (JavascriptExecutor)driver;
				jse.executeScript("arguments[0].click();", openAccountButton);
			}
		} else {
			openAccountButton.click();
		}	
	}
	/**
	 * Opens acc in www.copyop.com
	 */
	public void cpClickOpenAccountButton() {
		cpOpenAccButton.click();
	}
	/**
	 * Gets the open account window title
	 * @return String
	 */
	public String getOppenAccountWindowTitle() {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(openAccountTitleCSS)));
		String text = openAccountTitle.getText();
		System.out.println(text);
		return text;
	}
	/**
	 * Types first name
	 * @param firstName
	 * @param brType
	 * @throws IOException 
	 */
	public void typeFirstName(String firstName) throws IOException {
		try {
			firstNameInputElement.click();
		} catch (WebDriverException e) {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click();", firstNameInputElement);
		}
		if (brType.equalsIgnoreCase("internetexplorer") || brType.equalsIgnoreCase("firefox")) {
			String script = "document.getElementById('funnel_firstName').value='" + firstName +"'";
			((JavascriptExecutor) driver).executeScript(script);
		} else {
			firstNameInputElement.clear();
			firstNameInputElement.sendKeys(firstName);
		}
	}
	/**
	 * Sets first name 
	 * @param firstName
	 * @throws IOException
	 */
	public void setFirstName(String firstName) throws IOException {
		try {
			firstNameInputElement.click();
		} catch (WebDriverException e) {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click();", firstNameInputElement);
		}
		firstNameInputElement.clear();
		firstNameInputElement.sendKeys(firstName);
	}
	/**
	 * Types first name in www.copyop.com
	 * @param firstName
	 */
	public void cpTypeFirstName(String firstName) {
		cpFirstNameInput.clear();
		cpFirstNameInput.sendKeys(firstName);
	}
	/**
	 * Types second name
	 * @param secondName
	 */
	public void typeSecondName(String secondName) {
		try {
			secondNameInputElement.click();
		} catch (WebDriverException e) {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click();", secondNameInputElement);
		}
		if (brType.equalsIgnoreCase("internetexplorer") || brType.equalsIgnoreCase("firefox")) {
			String script = "document.getElementById('funnel_lastName').value='" + secondName +"'";
			((JavascriptExecutor) driver).executeScript(script);
		} else {
			secondNameInputElement.clear();
			secondNameInputElement.sendKeys(secondName);
		}	
	}
	/**
	 * Types second name
	 * @param secondName
	 */
	public void setSecondName(String secondName) {
		secondNameInputElement.clear();
		secondNameInputElement.sendKeys(secondName);
	}
	/**
	 * Types second name in www.copyop.com
	 * @param secondName
	 */
	public void cpTypeSecondName(String secondName) {
		cpLastNameInput.clear();
		cpLastNameInput.sendKeys(secondName);
	}
	/**
	 * Types email 
	 * @param email
	 */
	public void typeEmail(String email) {
		try {
			emailInputElement.click();
		} catch (WebDriverException e) {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click();", emailInputElement);
		}
		if (brType.equalsIgnoreCase("internetexplorer") || brType.equalsIgnoreCase("firefox")) {
			String script = "document.getElementById('funnel_email').value='" + email +"'";
			((JavascriptExecutor) driver).executeScript(script);
		} else {
			emailInputElement.clear();
			emailInputElement.sendKeys(email);
		}	
	}
	/**
	 * Types email 
	 * @param email
	 */
	public void typeEmailWithClick(String email) {
		try {
			emailInputElement.click();
		} catch (WebDriverException e) {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click();", emailInputElement);
		}
		if (brType.equalsIgnoreCase("internetexplorer") || brType.equalsIgnoreCase("firefox")) {
			String script = "document.getElementById('funnel_email').value='" + email +"'";
			((JavascriptExecutor) driver).executeScript(script);
		} else {
			emailInputElement.clear();
			emailInputElement.sendKeys(email);
		}	
		registerFormTitle.click();
	}
	/**
	 * Types email 
	 * @param email
	 */
	public void setEmailWithClick(String email) {
		emailInputElement.clear();
		emailInputElement.sendKeys(email);
		registerFormTitle.click();
	}
	/**
	 * Types email in www.copyop.com
	 * @param email
	 */
	public void cpTypeEmail(String email) {
		cpEmailInput.clear();
		cpEmailInput.sendKeys(email);
	}
	/**
	 * Types phone number
	 * @param phone
	 */
	public void typePhone(String phone) {
		phoneInputElement.click();
		if (brType.equalsIgnoreCase("internetexplorer") || brType.equalsIgnoreCase("firefox")) {
			String script = "document.getElementById('funnel_mobilePhone').value='" + phone +"'";
			((JavascriptExecutor) driver).executeScript(script);
		} else {
			phoneInputElement.clear();
			phoneInputElement.sendKeys(phone);
		}	
	}
	/**
	 * Types phone number
	 * @param phone
	 */
	public void setPhone(String phone) {
		phoneInputElement.click();
		phoneInputElement.clear();
		phoneInputElement.sendKeys(phone);
	}
	/**
	 * Types phone in www.copyop.com
	 * @param phone
	 */
	public void cpTypePhone(String phone) {
		cpPhoneInput.clear();
		cpPhoneInput.sendKeys(phone);
	}
	/**
	 * Types password
	 * @param pass
	 */
	public void typePassword(String pass) {
		try {
			passInputElement.click();
		} catch (WebDriverException e) {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click();", passInputElement);
		}
		if (brType.equalsIgnoreCase("firefox") || brType.equalsIgnoreCase("internetexplorer")) {
			String script = "document.getElementById('funnel_password').value='" + pass +"'";
			((JavascriptExecutor) driver).executeScript(script);
		} else {
			passInputElement.clear();
			passInputElement.sendKeys(pass);
		}	
	}
	/**
	 * Sets password
	 * @param pass
	 */
	public void setPassword(String pass) {
		passInputElement.click();
		passInputElement.clear();
		passInputElement.sendKeys(pass);
	}
	/**
	 * Types password in www.copyop.com
	 * @param pass
	 */
	public void cpTypePassword(String pass) {
		cpPasswordInput.clear();
		cpPasswordInput.sendKeys(pass);
	}
	/**
	 * Types confirmation of password
	 * @param confirmPass
	 */
	public void typeConfirmPassword(String confirmPass) {
		try {
			passConfirmInputElement.click();
		} catch (WebDriverException e) {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click();", passConfirmInputElement);
		}
		if (brType.equalsIgnoreCase("firefox") || brType.equalsIgnoreCase("internetexplorer")) {
			String script = "document.getElementById('funnel_passwordConfirm').value='" + confirmPass +"'";
			((JavascriptExecutor) driver).executeScript(script);
		} else {
			passConfirmInputElement.clear();
			passConfirmInputElement.sendKeys(confirmPass);
		}
		registerFormTitle.click();
	}
	/**
	 * Sets confirmation of password
	 * @param confirmPass
	 */
	public void setConfirmPassword(String confirmPass) {
		passConfirmInputElement.click();
		passConfirmInputElement.clear();
		passConfirmInputElement.sendKeys(confirmPass);
		registerFormTitle.click();
	}
	/**
	 * Types confirm password in www.copyop.com
	 * @param pass
	 */
	public void cpTypeConfirmPassword(String pass) {
		cpConfirmPassInput.clear();
		cpConfirmPassInput.sendKeys(pass);
	}
	/**
	 * Types confirmation of password
	 * @param confirmPass
	 */
	public void typeConfirmPasswordQuickForm(String confirmPass) {
		try {
			passConfirmInputElement.click();
		} catch (WebDriverException e) {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click();", passConfirmInputElement);
		}
		if (brType.equalsIgnoreCase("firefox") || brType.equalsIgnoreCase("internetexplorer")) {
			String script = "document.getElementById('funnel_passwordConfirm').value='" + confirmPass +"'";
			((JavascriptExecutor) driver).executeScript(script);
		} else {
			passConfirmInputElement.clear();
			passConfirmInputElement.sendKeys(confirmPass);
		}
	}
	/**
	 * Clicks term and conditions check box
	 */
	public void clickTermAndConditionsCheckBox() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(By.id(termsAndConditionsId)));
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("arguments[0].click();", termAndConditionsCheckBox);
	}
	/**
	 * Checks term and conds check box in www.copyop.com
	 */
	public void cpClickTermAndConditionsCheckBox() {
		driver.findElement(By.cssSelector("#main-container > section > div > div > div:nth-child(2) > form > div.table > div:nth-child(2) "
				+ "> div.row.ng-binding > label")).click();
	}
	/**
	 * Is check box for term and conditions checked 
	 * @return boolean
	 */
	public boolean isCheckedTermAndCond() {
		String className = termAndConditionsCheckBox.getAttribute("class");
		if (className.contains("checked")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Is check box for term and conditions checked in www.copyop.com
	 * @return
	 */
	public boolean cpIsCheckedTermAndCond() {
		String className = cpTermAndCondsCheckBox.getAttribute("class");
		if (className.equals("ng-untouched ng-dirty ng-valid-parse ng-valid ng-valid-required")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Gets my account title;
	 * @return
	 */
	public String getMyAccountFormTitle() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("newCardForm")));
		String title = myAccountTitle.getText();
		System.out.println(title);
		return title;
	}
}
