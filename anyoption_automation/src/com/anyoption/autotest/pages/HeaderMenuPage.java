package com.anyoption.autotest.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class HeaderMenuPage {
	private static final String balanceId = "loginStrip_balance";
	private WebDriver driver;
	@FindBy(css = "body > div.fL.header_login_holder > a:nth-child(3)")
	private WebElement myAccountLink;
	@FindBy(css = "#headerMenu > li:nth-child(2) > a")
	private WebElement optionTradingMenu;
	@FindBy(id = balanceId)
	private WebElement balanceLabel;
	@FindBy(id = "loginStrip_userName")
	private WebElement userName;
	@FindBy(css = "body > div.fL.header_login_holder > a.header_mails.vaM > span")
	private WebElement messagesBadje;
	@FindBy(css = "body > div.fL.header_login_holder > a.header_mails.vaM")
	private WebElement openMessagesLink;
	@FindBy(css = "#theBody > div.head_bgr > div > div.header_fixed > div > div > ul > li > div")
	private WebElement currentLangFlag;
	@FindBys({@FindBy(css = "#skin_selector_ul_in_h > ul > li > a")})
	private List<WebElement> langList;
	@FindBy(css = "#header > div > div.clear.header-status.ng-scope > div > div.header-balance.taC > b")
	private WebElement cpBalanceLabel;
	@FindBy(css = "#main-container > div > div.home-container > aside > div > h1")
	private WebElement cpNamesLabel;
	@FindBy(css = "#noOneTouchLinkDiv > ul")
	private WebElement homePageOptionTradingMenu;
	@FindBy(css = "#noOneTouchLinkDiv > ul > li > a")
	private List<WebElement> optionTradeingMenuElements;
	@FindBy(css = "#headerMenu > li:nth-child(1) > a")
	private WebElement homeButton;
	@FindBy(id = "profitLineDiv")
	private WebElement profiteLineIcon;
	/**
	 * 
	 * @param driver
	 */
	public HeaderMenuPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Opens profite line
	 */
	public void openProfiteLine() {
		profiteLineIcon.click();
	}
	/**
	 * Opens home page
	 */
	public void openHomePage() {
		homeButton.click();
	}
	/**
	 * Opens investment screen
	 * @param index
	 */
	public void openInvstmentScreenFromHomePage(int index) {
		String script = "document.querySelector('#noOneTouchLinkDiv > ul').setAttribute('style', 'display: block;');";
		((JavascriptExecutor) driver).executeScript(script);
		optionTradeingMenuElements.get(index).click();
	}
	/**
	 * Changes language
	 * @param skin
	 */
	public void changeLang(String skin) {
		if (! currentLangFlag.getAttribute("class").contains(skin.toLowerCase())) {
			currentLangFlag.click();
			for (WebElement lg : langList) {
				if (lg.getAttribute("class").contains(skin.toLowerCase())) {
					lg.click();
					break;
				}
			}
		}
	}
	/**
	 * Opens messages
	 */
	public void openMessages() {
		openMessagesLink.click();
	}
	/**
	 * Gets messages count
	 * @return
	 */
	public int getCountNotReadedMessages() {
		String count = messagesBadje.getText();
		return Integer.parseInt(count);
	}
	/**
	 * Switches driver to iframe
	 */
	public void switchDriver() {
		driver.switchTo().frame("header");
	}
	/**
	 * Gets user name from header menu
	 * @return
	 */
	public String getUserNameFromHeader() {
		driver.switchTo().frame("header");
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("loginStrip_userName")));
		String name = userName.getText();
		String text[] = name.split(" ");
		driver.switchTo().defaultContent();
		return text[text.length - 1].replace(" ", "");
	}
	/**
	 * Gets names from www.copyop.com
	 * @return
	 */
	public String cpGetUserNameFromHeader() {
		return cpNamesLabel.getText();
	}
	/**
	 * Opens my account menu
	 */
	public void openMyAccountMenu() {
		driver.switchTo().frame("header");
		myAccountLink.click();
		driver.switchTo().defaultContent();
	}
	/**
	 * Opens option trading menu
	 */
	public void openOptionTradingMenu() {
		optionTradingMenu.click();
	}
	/**
	 * Gets balance amount
	 * @return String
	 */
	public String getBalance() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("header"));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(balanceId)));
		String balance = balanceLabel.getText();
		System.out.println("balance " + balance);
		driver.switchTo().defaultContent();
		return balance;
	}
	/**
	 * Gets balance in www.copyop.com
	 * @return
	 */
	public String cpGetBalance() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#header > div > div.clear.header-status.ng-scope > div > div.header-balance.taC > b")));
		return cpBalanceLabel.getText();
	}
}
