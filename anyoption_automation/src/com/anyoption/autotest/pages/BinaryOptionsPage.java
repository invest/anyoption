package com.anyoption.autotest.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.anyoption.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimirm
 *
 */
public class BinaryOptionsPage {
	private WebDriver driver;
	@FindBy(css = "#noOneTouchLinkDiv > a")
	private WebElement optionTradingMenu;
	/**
	 * 
	 * @param driver
	 */
	public BinaryOptionsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Opens option trading
	 */
	public void openOptionTrading() {
		optionTradingMenu.click();
		WebDriverWait wait = new WebDriverWait(driver, 15);
		String css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div.page_bgr_trade > ul > "
				+ "li.trade_page_tabs_h.trade_page_tabs_binary_opt_h.active > a > span > span";
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(css)));
	}
	/**
	 * Clicks Up button 
	 * @param index
	 * @return Integer - index
	 */
	public int clickUpButton(int index, String brType) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		String waitCss = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div.page_bgr_trade";
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(waitCss)));
		String id = "tradeBox_call_btn_text_" + index;
		if (brType.equalsIgnoreCase("internetexplorer") || brType.equals("IE") || brType.equalsIgnoreCase("PhantomJS")) {
			UtilsMethods.sleep(8000);
			System.out.println("wait");
		}
		try {
			List<WebElement> buttons = driver.findElements(By.id(id));
			int size = buttons.size();
			System.out.println(size);
			buttons.get(size - 1).click();
		} catch (WebDriverException e) {
			System.out.println("exception is catched");
			UtilsMethods.sleep(10000);
			for (int i = 0; i < 4; i++) {
				try {
					String waitCssEx = "i#tradeBox_call_btn_" + i + ".tradeBox_img.tradeBox_call_btn.active";
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(waitCssEx)));
					wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(waitCssEx)));
					index = i;
					id = "tradeBox_call_btn_text_" + i;
					List<WebElement> buttons = driver.findElements(By.id(id));
					int size = buttons.size();
					System.out.println(size);
					buttons.get(size - 1).click();
					break;
				} catch (WebDriverException ex) {
					System.out.println("next");
				}
			}
		}
		return index;
	}
	/**
	 * Clicks open account button from pop up
	 * @param index
	 */
	public void openAccFromPopUp(int index) {
		String css = "#tradeBox_pop_login_" + index +" > a > span > span";
		WebElement button = driver.findElement(By.cssSelector(css));
		button.click();
	}
}
