package com.anyoption.autotest.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.anyoption.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class DocumentsPage {
	private WebDriver driver;
	private String browserType;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div.my_account_menu > div:nth-child(4) > ul > li:nth-child(3) > a")
	private WebElement documentsLink;
	@FindBy(css = "#utilityFileStatus > span")
	private WebElement utilityStatus;
	@FindBy(css = "#passOrIdFileStatus > span")
	private WebElement passOrIdStatus;
	@FindBy(css = "#documents_from_utility > div")
	private WebElement browseUtilityButton;
	@FindBy(css = "#documents_from_pass > div")
	private WebElement browsePassButton;
	@FindBy(id = "frontCcDiv")
	private WebElement browseCcFrontFileButton;
	@FindBy(id = "backCcDiv")
	private WebElement browseCcBackFileButton;
	@FindBy(id = "passOrIdFileStatusAdditional")
	private WebElement passOrIdAdditionalStatus;
	@FindBy(css = "#passOrIdFileStatusAdditional > span")
	private WebElement passAdditionalStatusMessage;
	@FindBy(id = "passOrIdCombo:passOrId")
	private WebElement passportSelect;
	@FindBy(css = "#ccFrontFileStatus > span")
	private WebElement ccFrontFileStatus;
	@FindBy(css = "#ccBackFileStatus > span")
	private WebElement ccBackFileStatus;
	@FindBy(css = "#summary_text > span")
	private WebElement documentsSummaryText;
	@FindBy(css = "#utilityFileStatus > img")
	private WebElement utilityIcon;
	@FindBy(css = "#passOrIdFileStatus > img")
	private WebElement passportIcon;
	@FindBy(css = "#ccFrontFileStatus > img")
	private WebElement frontCCIcon;
	@FindBy(css = "#ccBackFileStatus > img")
	private WebElement backCCIcon;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div.my_account_main > div > span")
	private WebElement approvedMessage;
	/**
	 * 
	 * @param driver
	 */
	public DocumentsPage(WebDriver driver, String browserType) {
		this.driver = driver;
		this.browserType = browserType;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Gets documents approved message
	 * @return String
	 */
	public String getApprovedMessage() {
		return approvedMessage.getText();
	}
	/**
	 * Is back credit card icon approved
	 * @return boolean
	 */
	public boolean isBackCCIconApproved() {
		if (backCCIcon.getAttribute("src").contains("images/icon_settled.gif")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Is back credit card browse button disabled
	 * @return boolean
	 */
	public boolean isBackCCBrowseButtonDisabled() {
		if (browseCcBackFileButton.getAttribute("class").contains("disabled")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Is front credit card icon approved
	 * @return boolean
	 */
	public boolean isFrontCCIconApproved() {
		if (frontCCIcon.getAttribute("src").contains("images/icon_settled.gif")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Is front credit card browse button disabled
	 * @return boolean
	 */
	public boolean isFrontCCBrowseButtonDisabled() {
		if (browseCcFrontFileButton.getAttribute("class").contains("disabled")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Is passport icon approved
	 * @return boolean
	 */
	public boolean isPassportIconApproved() {
		if (passportIcon.getAttribute("src").contains("images/icon_settled.gif")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Is passport browse button disabled
	 * @return boolean
	 */
	public boolean isPassportBrowseButtonDisabled() {
		if (browsePassButton.getAttribute("class").contains("disabled")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Is utility icon approved
	 * @return boolean
	 */
	public boolean isUtilityIconApproved() {
		if (utilityIcon.getAttribute("src").contains("images/icon_settled.gif")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Is utility browse button disabled
	 * @return boolean
	 */
	public boolean isUtilityBrowseButtonDisable() {
		if (browseUtilityButton.getAttribute("class").contains("disabled")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Gets documents summary message
	 */
	public String getDocumentsSummaryText() {
		return documentsSummaryText.getText();
	}
	/**
	 * Gets credit card back file status message
	 * @return
	 */
	public String getCcBackFileStatus() {
		return ccBackFileStatus.getText();
	}
	/**
	 * Gets credit card front file status
	 * @return
	 */
	public String getCcFrontFileStatus() {
		return ccFrontFileStatus.getText();
	}
	/**
	 * Selects pass
	 */
	public void selectPass() {
		Select select = new Select(passportSelect);
		select.selectByValue("0");
	}
	/**
	 * Gets pass additional status message
	 * @return
	 */
	public String getPassAdditionalStatusMessage() {
		return passAdditionalStatusMessage.getText();
	}
	/**
	 * Is additional status for pass displayed 
	 * @return
	 */
	public boolean isPassAdditionalStausDisplayed() {
		boolean result = false;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 3);
			wait.until(ExpectedConditions.attributeToBe(passOrIdAdditionalStatus, "style", "display: block;"));
			result = true;
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Sets data in clipboard
	 */
	public static void setClipboardData(String string) {
		StringSelection stringSelection = new StringSelection(string);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
	}
	/**
	 * Browses credit card back file
	 */
	public void browseCcBackFile() {
		browseCcBackFileButton.click();
	}
	/**
	 * Browses credit card front file
	 */
	public void browseCcFrontFile() {
		browseCcFrontFileButton.click();
	}
	/**
	 * Browses pass or id 
	 */
	public void browsePass() {
		browsePassButton.click();
	}
	/**
	 * Browses utility
	 */
	public void browseUttility() {
		browseUtilityButton.click();
	}
	/**
	 * Types attach file path
	 * @param path
	 * @throws AWTException 
	 */
	public void attachFile(String path, int index) throws AWTException {
		if (browserType.equalsIgnoreCase("firefox")) {
			switch (index) {
			case 0:
				this.browseUttility();
				break;
			case 1:
				this.browsePass();
				break;
			case 2:
				this.browseCcFrontFile();
				break;
			case 3:
				this.browseCcBackFile();
			}
			UtilsMethods.sleep(2000);
			String newPath = path.replace("/", "\\");
			setClipboardData(newPath);
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} else {
			List<WebElement> fileInput = driver.findElements(By.xpath("//input[@type='file']"));
			fileInput.get(index).sendKeys(path);
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_ESCAPE);
			r.keyRelease(KeyEvent.VK_ESCAPE);
		}	
	}
	/**
	 * gets passOrId status message
	 * @return
	 */
	public String getPassOrIdStatusMessage() {
		return passOrIdStatus.getText();
	}
	/**
	 * Gets utility status text
	 * @return
	 */
	public String getUtilityStatusMessage() {
		return utilityStatus.getText();
	}
	/**
	 * Opens documents
	 */
	public void openDocuments() {
		documentsLink.click();
	}
}
