package com.anyoption.autotest.pages;

import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * 
 * @author vladimirm
 *
 */
public class DepositPage {
	private static final String withdrawPopUpTextId = "withdraw_popUp_text";
	private static final String withdrawPopUpClassName = "withdraw_popUp";
	private static final String questionnairePopUpId = "popUpSingleQuestionnaire";
	private WebDriver driver;
	private String browserType;
	@FindBy(id = "newCardForm:deposit")
	private WebElement amountOfDepositInput;
	@FindBy(id = "newCardForm:ccNum")
	private WebElement cardNumberInput;
	@FindBy(id = "newCardForm:holderName")
	private WebElement cardHolderNameInput;
	@FindBy(id = "newCardForm:ccPass")
	private WebElement cvvInput;
	@FindBy(id = "newCardForm:street")
	private WebElement streetAddressInput;
	@FindBy(id = "newCardForm:cityNameAO")
	private WebElement cityInput;
	@FindBy(id = "newCardForm:zipCode")
	private WebElement postalCodeInput;
	@FindBy(id = "newCardForm:expMonth")
	private WebElement monthExpirySelect;
	@FindBy(id = "newCardForm:expYear")
	private WebElement yearExpirySelect;
	@FindBy(id = "newCardForm:birthDay")
	private WebElement dayDateOfBirthSelect;
	@FindBy(id = "newCardForm:birthMonth")
	private WebElement monthDateOfBirthSelect;
	@FindBy(id = "newCardForm:birthYear")
	private WebElement yearDateOfBirthSelect;
	@FindBy(id = "btn_newCard")
	private WebElement newCardButton;
	@FindBy(id = "depositForm:ccNum")
	private WebElement cardSelect;
	@FindBy(id = "depositForm:ccPass")
	private WebElement cvvDepositPassInput;
	@FindBy(id = questionnairePopUpId)
	private WebElement popUpQuestionForm;
	@FindBy(css = "#popup_single_questionnaire_main_holder > div.questionnaire_popUp > div > i.reg_log_close_img")
	private WebElement popUpCloseButton;
	@FindBy(id = "btn_oldCard")
	private WebElement submitCardButton;
	@FindBy(css = "#popup_v1 > h3")
	private WebElement successMessageElement;
	@FindBy(css = "#popup_v1 > div.text-info")
	private WebElement amountMessageElement;
	@FindBy(css = "body > div.fL.header_login_holder > a:nth-child(5)")
	private WebElement depositLink;
	@FindBy(css = "#depositForm > table > tbody > tr > td:nth-child(1) > span")
	private WebElement depositTitle;
	@FindBy(id = "depositForm:deposit")
	private WebElement depositInput;
	@FindBy(css = "#depositForm > div.deposit_holder.taL > div:nth-child(2) > span.vaM.deposit_currency")
	private WebElement moneyTypeElement;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div.my_account_menu > div:nth-child(3) > "
			+ "ul > li:nth-child(3) > a")
	private WebElement withdrawMenuElement;
	@FindBy(css = "#withdrawForm > div > h2")
	private WebElement withdrawTitle;
	@FindBy(id = "withdrawForm:ccNum")
	private WebElement withdrawCardSelect;
	@FindBy(id = "withdrawForm:amount")
	private WebElement amountToWithdraw;
	@FindBy(name = "withdraw_survey_question")
	private WebElement withdrawProfitRadioButton;
	@FindBy(css = "#popUpWithdrawSurvey > div > div.withdraw_survey_popUp_button > a > span > span")
	private WebElement popUpWithdrawSubmitButton;
	@FindBy(css = "#popUpWithdrawSurveyConfirmation > div > div")
	private WebElement feedBackMessageElement;
	@FindBy(css = "#popUpWithdrawSurveyConfirmation > div > a")
	private WebElement feedBackConfirmButton;
	@FindBy(className = withdrawPopUpClassName)
	private WebElement withdrawPopUpElement;
	@FindBy(css = "#withdrawForm > div > div > div > p.withdraw_popUp_heading")
	private WebElement withdrawPopUpHeading;
	@FindBy(css = "#withdrawForm > div > div > div > p:nth-child(2)")
	private WebElement importantNoteFromWithdrawPopUp;
	@FindBy(css = "a.trade_buton_login.trade_buton_login_h.clear.mt15")
	private WebElement withdrawPopUpSubmitButton;
	@FindBy(id = withdrawPopUpTextId)
	private WebElement withdrawAcceptedText;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div.my_account_main > div > div > a")
	private WebElement withdrawAcceptedTextCloseButton;
	@FindBy(id = "withdrawForm:withdrawBtn")
	private WebElement withdrawButton;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div.my_account_menu > div:nth-child(3) > ul > "
			+ "li:nth-child(4) > a")
	private WebElement openReverseWithdrawElement;
	@FindBy(xpath = "//*[@id='reverseWithdrawForm:data:tbody_element']/tr/td[3]/span")
	private WebElement descriptionElement;
	@FindBy(xpath = "//*[@id='reverseWithdrawForm:data:tbody_element']/tr/td[4]")
	private WebElement reverseWithdrawAmountElement;
	@FindBy(id = "reverseWithdrawForm:data:0:select")
	private WebElement reverseWithdrawCheckBox;
	@FindBy(css = "#reverseWithdrawForm > table > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > table > "
			+ "tbody > tr:nth-child(1) > td > span")
	private WebElement reverseWithdrawSubmitButton;
	@FindBy(xpath = "//*[@id='reverseWithdrawForm:messages']/tbody/tr/td/span")
	private WebElement reverseWithdrawSuccessMessage;
	@FindBy(id = "newCardForm:ccNum_all_error")
	private WebElement newCardError;
	@FindBy(id = "newCardForm:expDate_error")
	private WebElement expDateError;
	@FindBy(id = "newCardForm:holderName_error")
	private WebElement holderNameError;
	@FindBy(id = "newCardForm:ccPass_error")
	private WebElement ccPassError;
	@FindBy(id = "newCardForm:street_error")
	private WebElement streetError;
	@FindBy(id = "newCardForm:cityNameAO_error")
	private WebElement cityError;
	@FindBy(id = "newCardForm:zipCode_error")
	private WebElement zipCodeError;
	@FindBy(id = "newCardForm:birthDate_error")
	private WebElement birthDateError;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div.my_account_menu > div:nth-child(3) > ul "
			+ "> li:nth-child(2) > a")
	private WebElement depositSubTab;
	@FindBy(id = "popUpWithdrawResetBonus")
	private WebElement popUpResetBonus;
	@FindBy(css = "#popUpWithdrawResetBonusYes > span > span")
	private WebElement confirmResetBonusButton;
	@FindBy(css = "#depositForm > table > tbody > tr > td:nth-child(3) > a")
	private WebElement editCardLink;
	@FindBy(xpath = "//*[@id='cardsForm:data:tbody_element']/tr/td[2]/a")
	private WebElement cardNumber;
	@FindBy(id = "cardsForm:data:0:select")
	private WebElement cardToDeleteCheckBox;
	@FindBy(css = "#cardsForm > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td > table > tbody "
			+ "> tr:nth-child(1) > td:nth-child(2) > span")
	private WebElement deleteCardButton;
	@FindBy(xpath = "//*[@id='cardsForm:messages']/tbody/tr/td/span")
	private WebElement noSelectedCardError;
	@FindBy(css = "#cardsForm > table > tbody > tr:nth-child(1) > td > span")
	private WebElement deleteCardInfoMessage;
	@FindBy(css = "#cardsForm > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td > span:nth-child(1)")
	private WebElement cancelDeleteButton;
	@FindBy(css = "#cardsForm > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td > span:nth-child(6)")
	private WebElement continueDeleteCardButton;
	@FindBy(css = "#newCardForm > div > table > tbody > tr > td:nth-child(1) > span")
	private WebElement title;
	@FindBy(id = "newCardForm:currenciesList")
	private WebElement newCardCurrencySelect;
	@FindBy(css = "#popup_v1 > div.buttons-holder.table > div:nth-child(2) > button")
	private WebElement continiueButton;
	@FindBy(xpath = "//*[@id='reverseWithdrawForm:data:tbody_element']/tr/td[1]")
	private WebElement transactionIdFromReverse;
	@FindBy(xpath = "//*[@id='cardsForm:data:tbody_element']/tr/td[3]")
	private WebElement cardExpiryDate;
	@FindBy(xpath = "//*[@id='cardsForm:data:tbody_element']/tr/td[1]")
	private WebElement cardType;
	@FindBy(css = "#cardsForm > table > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr > td > span")
	private WebElement editCardType;
	@FindBy(css = "#cardsForm > table > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr > td > span")
	private WebElement editCardNumber;
	@FindBy(css = "#cardsForm > table > tbody > tr:nth-child(6) > td:nth-child(2) > table > tbody > tr > td > span:nth-child(1)")
	private WebElement cancelEditCardButton;
	@FindBy(css = "#cardsForm > table > tbody > tr:nth-child(6) > td:nth-child(2) > table > tbody > tr > td > span:nth-child(6)")
	private WebElement submitEditCardButton;
	@FindBy(id = "cardsForm:expError")
	private WebElement cardFormError;
	@FindBy(id = "cardsForm:expMonth")
	private WebElement editCardMonthSelect;
	@FindBy(id = "cardsForm:expYear")
	private WebElement editCardYearSelect;
	@FindBy(id = "newCardForm:typeId_error_txt")
	private WebElement newCardErrorMessage;
	@FindBy(id = "funnel_tooltip_ccNum")
	private WebElement newCardToolTip;
	@FindBy(id = "newCardForm:ccPass_error_txt")
	private WebElement newCardCvvErrorMessage;
	@FindBy(id = "funnel_tooltip_ccPass")
	private WebElement cvvTooltip;
	@FindBy(id = "newCardForm:holderName_error_txt")
	private WebElement newCardHolderErrorText;
	@FindBy(id = "newCardForm:street_error_txt")
	private WebElement streetErrorText;
	@FindBy(id = "newCardForm:cityNameAO_error_txt")
	private WebElement cityErrorText;
	@FindBy(id = "newCardForm:zipCode_error_txt")
	private WebElement zipCodeErrorText;
	@FindBy(id = "newCardForm:birthDay_error_txt")
	private WebElement birthDayErrorText;
	@FindBy(id = "newCardForm:birthMonth_error_txt")
	private WebElement birthMonthErrorText;
	@FindBy(id = "newCardForm:birthYear_error_txt")
	private WebElement birthYearErrorText;
	@FindBy(id = "newCardForm:expYear_error_txt")
	private WebElement expDateErrorText;
	@FindBy(id = "newCardForm:currenciesList_span")
	private WebElement selectedCurrencySign;
	@FindBy(id = "creditCard_error")
	private WebElement creditCardError;
	@FindBy(id = "depositForm:ccPass_error_txt_in")
	private WebElement ccPassErrorText;
	@FindBy(id = "depositForm:ccNum_error_txt_in")
	private WebElement ccNumErrorText;
	@FindBy(id = "depositForm:depositError")
	private WebElement depositAmounErrorText;
	@FindBy(id = "funnel_tooltip_amount")
	private WebElement depositAmountTooltip;
	@FindBy(id = "withdrawForm:amountError")
	private WebElement withdrawDepositErrorText;
	@FindBy(css = "#popUpWithdrawSurvey > div > div.withdraw_survey_popUp_button > div")
	private WebElement skipPopUpButton;
	@FindBy(css = "#withdrawForm > div > div > i.reg_log_close_img")
	private WebElement withdrawDepositPopUpCloseButton;
	@FindBy(id = "reverseWithdrawForm:messages")
	private WebElement reverseWithdrawErrorText;
	@FindBy(css = "#popup_v1 > span")
	private WebElement depositPopUpCloseButton;
	@FindBys({@FindBy(className = "LPMcloseButton")})
	private List<WebElement> helpPopUpCloseButton;
	/**
	 * 
	 * @param driver
	 */
	public DepositPage(WebDriver driver, String browserType) {
		this.driver = driver;
		this.browserType = browserType;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Closes help pop up if exist
	 */
	public void closeHelpWindow() {
		if (helpPopUpCloseButton.size() != 0) {
			helpPopUpCloseButton.get(0).click();
		}
	}
	/**
	 * Closes after deposit pop up
	 */
	public void closeAfterDepositPopUp() {
		depositPopUpCloseButton.click();
	}
	/**
	 * Gets reverse withdraw error message
	 * @return
	 */
	public String getReverseWithdrawErrorMessage() {
		return reverseWithdrawErrorText.getText();
	}
	/**
	 * Closes pop up to cancel withdraw
	 */
	public void closeWithdrawPopUpToCancel() {
		withdrawDepositPopUpCloseButton.click();
	}
	/**
	 * Skips withdraw pop up
	 */
	public void skipWithdrawPopup() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.attributeToBe(By.id("popUpWithdrawSurvey"), "style", "display: block;"));
		skipPopUpButton.click();
	}
	/**
	 * Gets withdraw error message
	 * @return String
	 */
	public String getWithdrawDepositErrorMessage() {
		return withdrawDepositErrorText.getText();
	}
	/**
	 * Gets deposit amount tooltip
	 * @return String
	 */
	public String getDepositAmountTooltip() {
		String tooltip = "";
		WebDriverWait wait = new WebDriverWait(driver, 5);
		try {
			wait.until(ExpectedConditions.attributeContains(depositAmountTooltip, "style", "display: block;"));
			tooltip = depositAmountTooltip.getText();
		} catch (TimeoutException e) {
			
		}
		return tooltip;
	}
	/**
	 * Gets deposit amount error message
	 * @return String
	 */
	public String getDepositAmountErrorMessage() {
		return depositAmounErrorText.getText();
	}
	/**
	 * Is credit card OK
	 * @return boolean
	 */
	public boolean isCreditCardOk() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 5);
		try {
			wait.until(ExpectedConditions.attributeToBe(creditCardError, "class", "funnel_error_good"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Gets credit card number error message
	 * @return String
	 */
	public String getCcNumberErrorMessage() {
		return ccNumErrorText.getText();
	}
	/**
	 * Gets cvv pass error message
	 * @return String
	 */
	public String getCVVpassErrorMessage() {
		return ccPassErrorText.getText();
	}
	/**
	 * Is credit card error displayed
	 * @return boolean
	 */
	public boolean isCreditCardErrorDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 5);
		try {
			wait.until(ExpectedConditions.attributeToBe(creditCardError, "class", "funnel_error_bad"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Gets selected currency sign
	 * @return String
	 */
	public String getSelectedCurrencySign() {
		return selectedCurrencySign.getText();
	}
	/**
	 * Gets exp date error message
	 * @return String
	 */
	public String getExpDateErrorMessage() {
		return expDateErrorText.getText();
	}
	/**
	 * Is exp date error displayed
	 * @return String
	 */
	public boolean isExpDateErrorDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 5);
		try {
			wait.until(ExpectedConditions.attributeToBe(expDateError, "class", "funnel_error_bad"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Gets birth year error message
	 * @return String
	 */
	public String getBirthYearErrorMessage() {
		return birthYearErrorText.getText();
	}
	/**
	 * Gets birth month error message
	 * @return String
	 */
	public String getBirthMonthErrorMessage() {
		return birthMonthErrorText.getText();
	}
	/**
	 * Gets birth day error message
	 * @return String
	 */
	public String getBirthDayErrorMessage() {
		return birthDayErrorText.getText();
	}
	/**
	 * Is birth date error displayed
	 * @return boolean
	 */
	public boolean isBirthDateErrorDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 5);
		try {
			wait.until(ExpectedConditions.attributeToBe(birthDateError, "class", "funnel_error_bad"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Gets zip code error message
	 * @return String
	 */
	public String getZipCodeErrorMessage() {
		return zipCodeErrorText.getText();
	}
	/**
	 * Is zip code error displayed
	 * @return boolean
	 */
	public boolean isZipCodeErrorDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 5);
		try {
			wait.until(ExpectedConditions.attributeToBe(zipCodeError, "class", "funnel_error_bad"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Gets city error message
	 * @return String
	 */
	public String getCityErrorMessage() {
		return cityErrorText.getText();
	}
	/**
	 * Is city error displayed
	 * @return boolean
	 */
	public boolean isCityErrorDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			wait.until(ExpectedConditions.attributeToBe(cityError, "class", "funnel_error_bad"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Gets street error text
	 * @return String
	 */
	public String getStreetErrorMessage() {
		return streetErrorText.getText();
	}
	/**
	 * Is street error displayed
	 * @return boolean
	 */
	public boolean isStreetErrorDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 5);
		try {
			wait.until(ExpectedConditions.attributeToBe(streetError, "class", "funnel_error_bad"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Gets new card holder name error message
	 * @return String
	 */
	public String getHolderNameErrorMessage() {
		return newCardHolderErrorText.getText();
	}
	/**
	 * Is new cars holder error displayed
	 * @return boolean
	 */
	public boolean isNewCardHolderNameErrorDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			wait.until(ExpectedConditions.attributeToBe(holderNameError, "class", "funnel_error_bad"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Gets cvv tooltip
	 * @return String
	 */
	public String getCvvTooltip() {
		String tooltip = "";
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			wait.until(ExpectedConditions.attributeContains(cvvTooltip, "style", "display: block;"));
			tooltip = cvvTooltip.getText();
		} catch (TimeoutException e) {
			
		}
		return tooltip;
	}
	/**
	 * Gets new card cvv error message
	 * @return String
	 */
	public String getNewCardCvvErrorMessage() {
		return newCardCvvErrorMessage.getText();
	}
	/**
	 * Is cvv error displayed
	 * @return boolean
	 */
	public boolean isCvvErrorDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			wait.until(ExpectedConditions.attributeToBe(ccPassError, "class", "funnel_error_bad"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Gets new card tooltip
	 * @return String
	 */
	public String getNewCardTooltip() {
		String tooltip = "";
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			wait.until(ExpectedConditions.attributeContains(newCardToolTip, "style", "display: block;"));
			tooltip = newCardToolTip.getText();
		} catch (TimeoutException e) {
			
		}
		return tooltip;
	}
	/**
	 * Gets new card message
	 * @return String
	 */
	public String getNewCardErrorMessage() {
		return newCardErrorMessage.getText();
	}
	/**
	 * Is error for new card displayed
	 * @return boolean
	 */
	public boolean isNewCardNumberErrorDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			wait.until(ExpectedConditions.attributeToBe(newCardError, "class", "funnel_error_bad"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Submits edit card
	 */
	public void submitEditCard() {
		submitEditCardButton.click();
	}
	/**
	 * Selects edit card year
	 * @param value
	 */
	public void selectEditYear(String value) {
		Select select = new Select(editCardYearSelect);
		select.selectByValue(value);
	}
	/**
	 * Selects edit card month
	 * @param value
	 */
	public void selectEditMonth(String value) {
		Select select = new Select(editCardMonthSelect);
		select.selectByValue(value);
	}
	/**
	 * Gets edit card error message
	 * @return String
	 */
	public String getEditCardErrorMessage() {
		return cardFormError.getText();
	}
	/**
	 * Is error for edit credit card displayed
	 * @return boolean
	 */
	public boolean isEditCardErrorDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 8);
		try {
			wait.until(ExpectedConditions.attributeToBe(cardFormError, "class", "error_messages"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Cancels edit card
	 */
	public void cancelEditCard() {
		cancelEditCardButton.click();
	}
	/**
	 * Gets card number from edit form
	 * @return String
	 */
	public String getEditCardNumber() {
		return editCardNumber.getText();
	}
	/**
	 * Gets card type from edit form
	 * @return String
	 */
	public String getEditCardType() {
		return editCardType.getText();
	}
	/**
	 * Gets card type
	 * @return String
	 */
	public String getCardType() {
		return cardType.getText();
	}
	/**
	 * Opens card to edit
	 */
	public void openCardToEdit() {
		cardNumber.click();
	}
	/**
	 * Gets card expiry date
	 * @return String
	 */
	public String getCardExpiryDate() {
		return cardExpiryDate.getText();
	}
	/**
	 * Gets transaction id from reverse withdraw menu
	 * @return String
	 */
	public String getTransactionIdFromReverse() {
		return transactionIdFromReverse.getText();
	}
	/**
	 * Clicks title
	 */
	public void clickTitle() {
		try {
			title.click();
		} catch (WebDriverException e) {
			cardNumberInput.click();
		}
	}
	/**
	 * Gets transaction id
	 * @return String
	 */
	public String getTransactionId() {
		String text[] = amountMessageElement.getText().split("\n");
		String id = text[text.length - 1].replaceAll("[^\\d]", "");
		return id;
	}
	/**
	 * Continue after deposit
	 */
	public void continiueAfterNewDeposit() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		continiueButton.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div.page_bgr_trade > ul > li.trade_page_tabs_h.trade_page_tabs_binary_opt_h")));
	}
	/**
	 * Is new card currency selector disabled
	 * @return boolean
	 */
	public boolean isCurrencySelectDisabled() {
		String attribute = newCardCurrencySelect.getAttribute("disabled");
		if (attribute.equals("disabled") || attribute.equals("true")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Is deposit with new card active tab
	 * @return boolean
	 */
	public boolean isDepositWithNewCardSelected() {
		String className = title.getAttribute("class");
		if (className.equals("deposit_selected_menu")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Gets screen title
	 * @return String
	 */
	public String getScreenTitle() {
		return title.getText();
	}
	/**
	 * Waits until url to be changed
	 */
	public void waitPaimentMethodSelector() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.urlContains("paymentMethodSelector.jsf"));
	}
	/**
	 * Continue delete credit card
	 */
	public void continueDelete() {
		continueDeleteCardButton.click();
	}
	/**
	 * Clicks cancel delete button
	 */
	public void cancelDeleteButton() {
		cancelDeleteButton.click();
	}
	/**
	 * Gets delete card info message
	 * @return String
	 */
	public String getDeleteCardInfoMessage() {
		return deleteCardInfoMessage.getText();
	}
	/**
	 * Gets no selected card error
	 * @return String
	 */
	public String getNotSelectedCardError() {
		return noSelectedCardError.getText();
	}
	/**
	 * Deletes credit card
	 */
	public void clickDeleteCardButton() {
		deleteCardButton.click();
	}
	/**
	 * Check check box to delete card
	 */
	public void checkCardToDelete() {
		cardToDeleteCheckBox.click();
	}
	/**
	 * Gets card number
	 * @return String
	 */
	public String getCardNumber() {
		return cardNumber.getText();
	}
	/**
	 * Opens edit credit card menu
	 */
	public void openEditCreditCardMenu() {
		editCardLink.click();
	}
	/**
	 * Confirms reset bonus
	 */
	public void confirmResetBonus() {
		try {
			confirmResetBonusButton.click();
		} catch (ElementNotVisibleException e) {
			
		}
	}
	/**
	 * Is pop up for reset bonus confirmation displayed
	 * @return
	 */
	public boolean isResetBonusPopUpDisplayed() {
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			try {
				wait.until(ExpectedConditions.attributeToBe(popUpResetBonus, "style", "display: block;"));
			} catch (StaleElementReferenceException e) {
				
			}
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Waits for error message
	 */
	public void waitErrorMessage() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("newCardForm:messages")));
	}
	/**
	 * Opens deposit sub tab
	 */
	public void openDepositSubTab() {
		depositSubTab.click();
	}
	/**
	 * Is birth date ok
	 * @return boolean
	 */
	public boolean isBirthDateOK() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.attributeToBe(birthDateError, "class", "funnel_error_good"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Is zip code ok
	 * @return boolean
	 */
	public boolean isZipCodeOK() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.attributeToBe(zipCodeError, "class", "funnel_error_good"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Is city ok
	 * @return boolean
	 */
	public boolean isCityOK() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.attributeToBe(cityError, "class", "funnel_error_good"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Is street address ok
	 * @return boolean
	 */
	public boolean isAddressOK() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.attributeToBe(streetError, "class", "funnel_error_good"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Is ccPass ok
	 * @return boolean
	 */
	public boolean isCcPassOK() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.attributeToBe(ccPassError, "class", "funnel_error_good"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Is holder name ok
	 * @return boolean
	 */
	public boolean isHolderNameOK() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.attributeToBe(holderNameError, "class", "funnel_error_good"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Is exp data ok
	 * @return boolean
	 */
	public boolean isExpDateOK() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.attributeToBe(expDateError, "class", "funnel_error_good"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Is new card ok
	 * @return boolean
	 */
	public boolean isNewCardOK() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.attributeToBe(newCardError, "class", "funnel_error_good"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Gets reverse withdraw positive message
	 * @return
	 */
	public String getReverseWithdrawSuccessMessage() {
		return reverseWithdrawSuccessMessage.getText();
	}
	/**
	 * Submits reverse withdraw
	 */
	public void submitReverseWithdraw() {
		if (browserType.equalsIgnoreCase("firefox")) {
			try {
				reverseWithdrawSubmitButton.click();
			} catch (WebDriverException e) {
				JavascriptExecutor jse = (JavascriptExecutor)driver;
				jse.executeScript("arguments[0].click();", reverseWithdrawSubmitButton);
			}
		} else {
			reverseWithdrawSubmitButton.click();
		}	
	}
	/**
	 * Clicks reverse withdraw check box
	 */
	public void clickReverseWithdrawCheckBox() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 15);
			wait.until(ExpectedConditions.visibilityOf(reverseWithdrawCheckBox));
			reverseWithdrawCheckBox.click();
		} catch (WebDriverException e){
			System.out.println("Try with java script");
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click();", reverseWithdrawCheckBox);
		}
	}
	/**
	 * Gets reverse withdraw amount
	 * @return
	 */
	public String getReverseWithdrawAmount() {
		return reverseWithdrawAmountElement.getText();
	}
	/**
	 * Gets reverse withdraw description
	 * @return
	 */
	public String getReverseWithdrawDescription() {
		return descriptionElement.getText();
	}
	/**
	 * Opens reverse withdraw
	 */
	public void openReverseWithdraw() {
		openReverseWithdrawElement.click();
	}
	/**
	 * Clicks withdraw button
	 */
	public void clickWithdrawButton() {
		withdrawButton.click();
	}
	/**
	 * Closes withdraw accepted text pop up
	 */
	public void closeWithdrawAcceptedText() {
		withdrawAcceptedTextCloseButton.click();
	}
	/**
	 * Gets withdraw accepted text
	 * @return
	 */
	public String getWithdrawPopUpText() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(withdrawPopUpTextId)));
		return withdrawAcceptedText.getText();
	}
	/**
	 * Submits from withdraw pop up
	 */
	public void submitWithdrawFinally() {
		withdrawPopUpSubmitButton.click();
	}
	/**
	 * Gets fee from important note in withdraw pop up
	 */
	public String getFeeFromImortantNote(String moneyType) {
		String note = importantNoteFromWithdrawPopUp.getText();
		String[] arr = note.split(Pattern.quote(moneyType));
		String fee = arr[1].replaceAll("\\D+", "");
		return fee;
	}
	/**
	 * Gets withdraw pop up heading text
	 * @return
	 */
	public String getWithdrawPopUpHeading() {
		return withdrawPopUpHeading.getText();
	}
	/**
	 * Returns if pop up withdraw is displayed
	 * @return boolean
	 */
	public boolean isWithdrawPopUpDisplayed() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(withdrawPopUpClassName)));
		if (withdrawPopUpElement.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Confirms feedback pop up
	 */
	public void confirmFeedback() {
		feedBackConfirmButton.click();
	}
	/**
	 * Gets feedback pop up massage
 	 * @return String
	 */
	public String getFeedbackMessage() {
		return feedBackMessageElement.getText();
	}
	/**
	 * Submits pop up withdraw
	 */
	public void submitPopUpWithdraw() {
		popUpWithdrawSubmitButton.click();
	}
	/**
	 * Chooses withdraw profit radio button
	 */
	public void choosekWithdrawProfitRadioButton() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOf(withdrawProfitRadioButton));
		withdrawProfitRadioButton.click();
	}
	/**
	 * Sets amount to withdraw
	 * @param amount
	 */
	public void setAmountToWithdraw(String amount) {
//		if (browserType.equalsIgnoreCase("internetexplorer")) {
//			String script = "document.getElementById('withdrawForm:amount').value='" + amount +"'";
//			((JavascriptExecutor) driver).executeScript(script);
//		} else {
//			amountToWithdraw.sendKeys(amount);
//		}	
		amountToWithdraw.clear();
		amountToWithdraw.sendKeys(amount);
	}
	/**
	 * Selects card to withdraw
	 * @param index
	 */
	public void selectCardToWithdraw(int index) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("withdrawForm:ccNum")));
		Select select = new Select(withdrawCardSelect);
		select.selectByIndex(index);
	}
	/**
	 * Gets withdraw screen title
	 * @return
	 */
	public String getWithdrawScreenTitle() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#withdrawForm > div > h2")));
		return withdrawTitle.getText();
	}
	/**
	 * Opens withdraw
	 */
	public void openWithdrawMenu() {
		withdrawMenuElement.click();
	}
	/**
	 * Gets money type
	 * @return String
	 */
	public String getMoneyType() {
		return moneyTypeElement.getText();
	}
	/**
	 * Sets deposit amount
	 * @param amount
	 */
	public void setDepositAmount(String amount) {
		depositInput.click();
		depositInput.clear();
//		if (browserType.equalsIgnoreCase("firefox") || browserType.equalsIgnoreCase("internetexplorer")) {
//			String script = "document.getElementById('depositForm:deposit').value='" + amount +"'";
//			((JavascriptExecutor) driver).executeScript(script);
//		} else {
//			depositInput.sendKeys(amount);
//		}
		depositInput.sendKeys(amount);
	}
	/**
	 * Gets deposit title
	 * @return String
	 */
	public String getDepositTitle() {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(depositTitle));
		return depositTitle.getText();
	}
	/**
	 * Opens deposit menu
	 */
	public void openDepositMenu() {
		driver.switchTo().frame("header");
		depositLink.click();
		driver.switchTo().defaultContent();
	}
	/**
	 * Gets message with amount of deposit
	 * @return
	 */
	public String getAmountMessage() {
		String message = amountMessageElement.getText();
		return message;
	}
	/**
	 * Gets message when deposit is successful
	 * @return
	 */
	public String getSuccessMessage() {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(successMessageElement));
		String message = successMessageElement.getText();
		return message;
	}
	/**
	 * Submits old card
	 */
	public void submitOldCard() {
		submitCardButton.click();
	}
	/**
	 * Closes pop up for questions
	 */
	public void closePopUpForQuestions() {
		driver.switchTo().frame("popUpSingleQuestionnaireContent");
		popUpCloseButton.click();
		driver.switchTo().defaultContent();
	}
	/**
	 * Returns is pop up for questions displayed
	 * @return
	 */
	public boolean isPopUpQuestionsEnable() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(questionnairePopUpId)));
		if (popUpQuestionForm.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Sets cvv deposit pass
	 * @param cvv
	 */
	public void setCvvPass(String cvv) {
		cvvDepositPassInput.clear();
//		if (browserType.equalsIgnoreCase("internetexplorer")) {
//			String script = "document.getElementById('depositForm:ccPass').value='" + cvv +"'";
//			((JavascriptExecutor) driver).executeScript(script);
//		} else {
//			cvvDepositPassInput.sendKeys(cvv);
//		}
		cvvDepositPassInput.sendKeys(cvv);
	}
	/**
	 * Selects already registered card
	 * @param index
	 */
	public void selectCard(int index) {
		Select select = new Select(cardSelect);
		select.selectByIndex(index);
	}
	/**
	 * Sets amount of deposit
	 * @param amount - String
	 */
	public void setNewDepositAmount(String amount) {
		amountOfDepositInput.clear();
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			String script = "document.getElementById('newCardForm:deposit').value='" + amount +"'";
			((JavascriptExecutor) driver).executeScript(script);
		} else {
			amountOfDepositInput.sendKeys(amount);
		}	
	}
	/**
	 * Gets default amount
	 * @return
	 */
	public String getDefaultAmount() {
		String amount = amountOfDepositInput.getAttribute("value");
		return amount;
	}
	/**
	 * Sets card number
	 * @param number - String
	 */
	public void setCardNumber(String number) {
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			cardNumberInput.click();
			String firstChar = "4";
			number = number.replace(firstChar, "");
			String script = "document.getElementById('newCardForm:ccNum').value='" + number +"'";
			((JavascriptExecutor) driver).executeScript(script);
			cardNumberInput.sendKeys(firstChar);
		} else {
			cardNumberInput.sendKeys(number);
		}	
	}
	/**
	 * Sets card holder name
	 * @param name - String
	 */
	public void setCardHolderName(String name) {
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			cardHolderNameInput.click();
			String script = "document.getElementById('newCardForm:holderName').value='" + name +"'";
			((JavascriptExecutor) driver).executeScript(script);
		} else {
			cardHolderNameInput.clear();
			cardHolderNameInput.sendKeys(name);
		}	
	}
	/**
	 * Sets cvv
	 * @param cvv - String
	 * @throws IOException 
	 */
	public void setCVV(String cvv) {
//		if (browserType.equalsIgnoreCase("internetexplorer")) {
//			cvvInput.click();
//			String script = "document.getElementById('newCardForm:ccPass').value='" + cvv +"'";
//			((JavascriptExecutor) driver).executeScript(script);
//		} else {
//			cvvInput.clear();
//			cvvInput.sendKeys(cvv);
//		}	
		cvvInput.clear();
		cvvInput.sendKeys(cvv);
	}
	/**
	 * Sets street
	 * @param street - String
	 */
	public void setStreet(String street) {
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			streetAddressInput.click();
			String script = "document.getElementById('newCardForm:street').value='" + street +"'";
			((JavascriptExecutor) driver).executeScript(script);
		} else {
			streetAddressInput.clear();
			streetAddressInput.sendKeys(street);
		}	
	}
	/**
	 * Sets city
	 * @param city - String
	 * @throws IOException 
	 */
	public void setCity(String city) throws IOException {
		if (browserType.equalsIgnoreCase("internetexplorer") && browserType.equalsIgnoreCase("firefox")) {
			cityInput.click();
			String script = "document.getElementById('newCardForm:cityNameAO').value='" + city +"'";
			((JavascriptExecutor) driver).executeScript(script);
		} else {
			cityInput.clear();
			cityInput.sendKeys(city);
		}	
	}
	/**
	 * Sets postal code
	 * @param code - String
	 * @throws IOException 
	 */
	public void setPostCode(String code) throws IOException {
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			postalCodeInput.click();
			String script = "document.getElementById('newCardForm:zipCode').value='" + code +"'";
			((JavascriptExecutor) driver).executeScript(script);
		} else {
			try {
				postalCodeInput.click();
			} catch (WebDriverException e) {
				jse.executeScript("arguments[0].click();", postalCodeInput);
			}
			postalCodeInput.clear();
			postalCodeInput.sendKeys(code);
		}	
	}
	/**
	 * Selects month expiry 
	 * @param index - Integer
	 */
	public void selectMonthOfExpiry(int index) {
		Select select = new Select(monthExpirySelect);
		select.selectByIndex(index);
	}
	/**
	 * Selects month expiry 
	 * @param value
	 */
	public void selectMonthOfExpiry(String value) {
		Select select = new Select(monthExpirySelect);
		select.selectByValue(value);
	}
	/**
	 * Selects year expiry
	 * @param index
	 */
	public void selectYearOfExpiry(int index) {
		Select select = new Select(yearExpirySelect);
		select.selectByIndex(index);
	}
	/**
	 * Selects year expiry
	 * @param value
	 */
	public void selectYearOfExpiry(String value) {
		Select select = new Select(yearExpirySelect);
		select.selectByValue(value);
	}
	/**
	 * Selects day of birth
	 * @param value
	 */
	public void selectDayOfBirth(String value) {
		Select select = new Select(dayDateOfBirthSelect);
		select.selectByValue(value);
	}
	/**
	 * Selects month of birth
	 * @param value - String
	 */
	public void selectMonthOfBirth(String value) {
		Select select = new Select(monthDateOfBirthSelect);
		select.selectByValue(value);
	}
	/**
	 * Selects year of birth
	 * @param value
	 */
	public void selectYearOfBirth(String value) {
		Select select = new Select(yearDateOfBirthSelect);
		select.selectByValue(value);;
	}
	/**
	 * Register new card
	 */
	public void registerNewCard() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 15);
			wait.until(ExpectedConditions.visibilityOf(newCardButton));
			newCardButton.click();
		} catch (WebDriverException e) {
			System.out.println("Exception cath try with javascriptExecutor");
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click();", newCardButton);
		}
	}
}
