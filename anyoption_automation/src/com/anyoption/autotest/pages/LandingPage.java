package com.anyoption.autotest.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class LandingPage {
	private WebDriver driver;
	@FindBys({@FindBy(css = "#device_button > span.visible-lg")})
	private List<WebElement> startNowButtons;
	/**
	 * 
	 * @param driver
	 */
	public LandingPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Is pop up for deposit in www.copyop.com
	 * @return
	 */
	public boolean isPopUpForDepositDisplayed() {
		boolean result = true;
		String css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-header.ng-binding";
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(css)));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Clicks start now button
	 * @param index
	 */
	public void startNow(int index) {
		for (WebElement but : startNowButtons) {
			if (but.isDisplayed()) {
				but.click();
				UtilsMethods.sleep(1000);
				break;
			}
		}
		String url = driver.getCurrentUrl();
		if (url.contains("#")) {
			url = url.replace("#", "");
			driver.get(url);
		}
		for (WebElement but : startNowButtons) {
			if (but.isDisplayed()) {
				but.click();
				UtilsMethods.sleep(1000);
				break;
			}
		}
	}
	/**
	 * Clicks start now button
	 */
	public void tradingbinaryoptionStartNow() {
		String url = driver.getCurrentUrl();
		String css = null;
		if (url.contains("ao_finance_weekly")) {
			css = "#TR > div.main > aside > a > img";
		} else if (url.contains("ao_investors_journal_versionC")) {
			css = "#TR > div.wrapper > div > div.row.mainRow > aside > a > img";
		} else if (url.contains("luxury_vacation")) {
			css = "#TR > div.container > div.row > div.content.col-sm-8 > p:nth-child(47) > a";
		} else if (url.contains("soccer_sa")) {
			css = "body > div.container > div.row > div.content.col-sm-8 > p:nth-child(45) > a";
		} else if (url.contains("co_world_business_celebs_ca")) {
			css = "body > div.wrapper > div > div.row.mainRow > section.col-lg-8.col-md-12.col-sm-12 > div > div > div > a";
		} else if (url.contains("ao_investors_journal_versionB")) {
			css = "#ES > div.wrapper > div > div.row.mainRow > aside > a > img";
		} else if (url.contains("ao_investors_journal_diary_install")) {
//			css = "#DE > div.wrapper > div > div.row.mainRow > div.col-md-4.col-sm-4.aside_wrap > aside > a:nth-child(2) > img";
			css = "#DE > div.wrapper > div > div.row.mainRow > div.col-md-4.col-sm-4.aside_wrap > aside > a > img";
		} 
		driver.findElement(By.cssSelector(css)).click();;
	}
	/**
	 * Is register form displayed
	 * @return
	 */
	public boolean isRegisterFormDisplayed() {
		boolean result = true;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			String waitCss = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div.pageMain.border_register";
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(waitCss)));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Is register form in www.copyop.com displayed
	 * @return
	 */
	public boolean isCopyOpRegisterFormDisplayed() {
		boolean result = true;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			String waitCss = "#main-container > section > div > div";
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(waitCss)));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Is current URL contains firstNewCard
	 * @return
	 */
	public boolean isUrlFirstNewCard() {
		boolean result = false;
		WebDriverWait wait = new WebDriverWait(driver, 30);
		try {
			wait.until(ExpectedConditions.urlContains("jsp/pages/firstNewCard.jsf"));
			result = true;
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
}
