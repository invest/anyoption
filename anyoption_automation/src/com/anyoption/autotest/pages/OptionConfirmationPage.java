package com.anyoption.autotest.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class OptionConfirmationPage {
	WebDriver driver;
	@FindBy(css = "body > div:nth-child(1) > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr > td > table > tbody "
			+ "> tr:nth-child(4) > td > strong")
	private WebElement optionConfirmationTitle;
	@FindBy(css = "body > div:nth-child(1) > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr > td > table > tbody "
			+ "> tr:nth-child(6) > td > table > tbody > tr:nth-child(3) > td.slip_conf_print > strong:nth-child(2)")
	private WebElement optionConfirmationMarket;
	@FindBy(css = "body > div:nth-child(1) > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr > td > table > tbody "
			+ "> tr:nth-child(6) > td > table > tbody > tr:nth-child(3) > td.slip_level")
	private WebElement optionConfirmationLevel;
	@FindBy(css = "body > div:nth-child(1) > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr > td > table > tbody "
			+ "> tr:nth-child(6) > td > table > tbody > tr:nth-child(6) > td.slipright")
	private WebElement optionConfirmationExpTime;
	@FindBy(css = "body > div:nth-child(1) > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr > td > table > tbody "
			+ "> tr:nth-child(6) > td > table > tbody > tr:nth-child(7) > td.slipright")
	private WebElement optionConfirmationInvestmentAmount;
	@FindBy(css = "body > div:nth-child(1) > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr > td > table > tbody "
			+ "> tr:nth-child(6) > td > table > tbody > tr:nth-child(10) > td.slipright")
	private WebElement optionConfirmationCorrectValue;
	@FindBy(css = "body > div:nth-child(1) > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr > td > table > tbody "
			+ "> tr:nth-child(6) > td > table > tbody > tr:nth-child(11) > td.slipright")
	private WebElement optionConfirmationIncorrectValue;
	@FindBy(css = "body > div:nth-child(1) > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr > td > table > tbody "
			+ "> tr:nth-child(6) > td > table > tbody > tr:nth-child(12) > td.slipright")
	private WebElement optionConfirmationOptionId;
	@FindBy(css = "body > div:nth-child(1) > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(5) > td.slipright")
	private WebElement optionConfirmationType;
	@FindBy(css = "body > div:nth-child(1) > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(9) > td.slipright")
	private WebElement longTermReturnAmount;
	/**
	 * 
	 * @param driver
	 */
	public OptionConfirmationPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Gets long term return amount
	 * @return
	 */
	public String getLongTermReturnAmount() {
		return longTermReturnAmount.getText();
	}
	/**
	 * Gets option confirmation type
	 * @return String
	 */
	public String getOptionConfirmationType() {
		return optionConfirmationType.getText();
	}
	/**
	 * Gets option id
	 * @return String
	 */
	public String getOptionConfirmationOptionId() {
		return optionConfirmationOptionId.getText().replace("#", "");
	}
	/**
	 * Gets value if incorrect
	 * @return
	 */
	public String getOptionConfirmationIncorrectValue() {
		return optionConfirmationIncorrectValue.getText();
	}
	/**
	 * Gets value if correct
	 * @return
	 */
	public String getOptionConfirmationCorrectValue() {
		return optionConfirmationCorrectValue.getText();
	}
	/**
	 * Gets option id
	 * @return String
	 */
	public String getOptionConfirmationInvestmentAmount() {
		return optionConfirmationInvestmentAmount.getText();
	}
	/**
	 * Gets expire time
	 * @return String
	 */
	public String getOptionConfirmationExpireTime() {
		return optionConfirmationExpTime.getText();
	}
	/**
	 * Gets level
	 * @return String
	 */
	public String getOptionConfirmationLevel() {
		return optionConfirmationLevel.getText();
	}
	/**
	 * Gets market
	 * @return String
	 */
	public String getOptionConfirmationMarket() {
		return optionConfirmationMarket.getText();
	}
	/**
	 * Gets option confirmation title
	 * @return String
	 */
	public String getOptionConfirmationTitle() {
		return optionConfirmationTitle.getText();
	}
}
