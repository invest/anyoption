package com.anyoption.autotest.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * 
 * @author vladimirm
 *
 */
public class QuestionnairePage {
	private static final String popUpCheckBoxCSS = "#popUpAORegulationStatusCodeBlock_restricted > div "
			+ "> div.checkbox_content.txt_size_s > label > i";
	private WebDriver driver;
	@FindBy(id = "regSingleQuestionnaireForm:question_81")
	private WebElement annualIncomeSelect;
	@FindBy(id = "regSingleQuestionnaireForm:question_82")
	private WebElement sourceIncomeSelect;
	@FindBy(id = "regSingleQuestionnaireForm:question_83")
	private WebElement educationSelect;
	@FindBy(id = "regSingleQuestionnaireForm:question_84")
	private WebElement employmentStatusSelect;
	@FindBy(id = "regSingleQuestionnaireForm:question_85")
	private WebElement professionSelect;
	@FindBy(css = "#single_questionnaire_step_1 > div.taC.mt60 > a > span > span")	
	private WebElement nextButton;
	@FindBy(id = "regSingleQuestionnaireForm:question_86")
	private WebElement tradingExperienceSelect;
	@FindBy(id = "regSingleQuestionnaireForm:question_87")
	private WebElement financialDerivativesSelect;
	@FindBy(id = "regSingleQuestionnaireForm:question_88")
	private WebElement tradesCountSelect;
	@FindBy(id = "regSingleQuestionnaireForm:question_89")
	private WebElement natureOfTradingSelect;
	@FindBy(css = "#single_questionnaire_step_2 > div.checkbox_content > p:nth-child(4) > label > i")
	private WebElement riskCheckBox;
	@FindBy(css = "#single_questionnaire_step_2 > div.taC > a.buton_blue.but_sprite.clear > span > span")
	private WebElement tradeNowButton;
	@FindBy(css = popUpCheckBoxCSS)
	private WebElement popUpCheckBox;
	@FindBy(css = "#popUpAORegulationStatusCodeBlock_restricted > div > div.regulation_status_popUp_button > a > span > span")
	private WebElement popUpConfirmButton;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div.page_bgr_trade > ul > "
			+ "li.trade_page_tabs_h.trade_page_tabs_binary_opt_h.active")
	private WebElement binaryOptionsActiveTab;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div.my_account_main > div > h1")
	private WebElement questTitle;
	@FindBy(id = "popUpSingleQuestionnaire")
	private WebElement popUpQuestionnaire;
	@FindBy(css = "#popup_single_questionnaire_main_holder > div.questionnaire_popUp > div > i.reg_log_close_img")
	private WebElement popUpQuestionnaireCloseButton;
	@FindBy(css = "#popup_single_questionnaire_main_holder > div.questionnaire_popUp > div > h1")
	private WebElement popUpQuestionnaireTitle;
	/**
	 * 
	 * @param driver
	 */
	public QuestionnairePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Gets pop up title
	 * @return
	 */
	public String getPopUpQuestionnaireTitle() {
		return popUpQuestionnaireTitle.getText();
	}
	/**
	 * Switch driver to iframe
	 */
	public void switchDriver() {
		driver.switchTo().frame("popUpSingleQuestionnaireContent");
	}
	/**
	 * Closes questionnaire pop up
	 */
	public void closeQuestionnairePopUp() {
		driver.switchTo().frame("popUpSingleQuestionnaireContent");
		popUpQuestionnaireCloseButton.click();
		driver.switchTo().defaultContent();
	}
	/**
	 * Is pop up for questionnaire displayed
	 * @return
	 */
	public boolean isQuestionnairePopUpDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 15);
		try {
			wait.until(ExpectedConditions.attributeToBe(popUpQuestionnaire, "style", "display: block;"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Gets questionnaire screen title
	 * @return String
	 */
	public String getCurrentScreenTitle() {
		return questTitle.getText();
	}
	/**
	 * Returns is binary trading available
	 * @return
	 */
	public boolean isBinaryOptionsActive() {
		if (binaryOptionsActiveTab.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Confirms in pop up
	 */
	public void confirmPopUp() {
		popUpConfirmButton.click();
	}
	/**
	 * Check the check box in pop up
	 */
	public void checkInPopUp() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(popUpCheckBoxCSS)));
		popUpCheckBox.click();
	}
	/**
	 * Selects annual income
	 * @param index
	 */
	public void selectAnnualIncome(int index) {
		Select select = new Select(annualIncomeSelect);
		select.selectByIndex(index);
	}
	/**
	 * Selects source income
	 * @param index
	 */
	public void selectSourceIncome(int index) {
		Select select = new Select(sourceIncomeSelect);
		select.selectByIndex(index);
	}
	/**
	 * Selects education
	 * @param index
	 */
	public void selectEducation(int index) {
		Select select = new Select(educationSelect);
		select.selectByIndex(index);
	}
	/**
	 * Selects employment status
	 * @param index
	 */
	public void selectEmoploymentStatus(int index) {
		Select select = new Select(employmentStatusSelect);
		select.selectByIndex(index);
	}
	/**
	 * Selects profession
	 * @param index
	 */
	public void selectProfession(int index) {
		Select select = new Select(professionSelect);
		select.selectByIndex(index);
	}
	/**
	 * Moves next
	 */
	public void moveNext() {
		nextButton.click();
	}
	/**
	 * Selects trading experience
	 * @param index
	 */
	public void selectTradingExperience(int index) {
		Select select = new Select(tradingExperienceSelect);
		select.selectByIndex(index);
	}
	/**
	 * Selects financial derivatives
	 * @param index
	 */
	public void selectFinancialDerivative(int index) {
		Select select = new Select(financialDerivativesSelect);
		select.selectByIndex(index);
	}
	/**
	 * Selects trade count
	 * @param index
	 */
	public void selectTradeCount(int index) {
		Select select = new Select(tradesCountSelect);
		select.selectByIndex(index);
	}
	/**
	 * Selects nature of trading
	 * @param index
	 */
	public void selectNatureOfTrading(int index) {
		Select select = new Select(natureOfTradingSelect);
		select.selectByIndex(index);
	}
	/**
	 * Checks risk check box
	 */
	public void checkRiskCheckBox(String operSys) {
		if (operSys.equals("Linux")) {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click();", riskCheckBox);
		} else {
			riskCheckBox.click();
		}
	}
	/**
	 * Clicks trade now button
	 * @throws IOException 
	 */
	public void tradeNow(String operSys) throws IOException {
		if (operSys.equals("Linux")) {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click();", tradeNowButton);
		} else {
			tradeNowButton.click();
		}	
	}
}
