package com.anyoption.autotest.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class MyAccountPage {
	private WebDriver driver;
	@FindBy(xpath = "//*[@id='investmentsContainer:data:tbody_element']/tr/td[6]/span")
	private WebElement investmentAmountLabel;
	@FindBy(xpath = "//*[@id='investmentsContainer:data:tbody_element']/tr/td[7]/span/span/text()")
	private WebElement commissionTooltip;
	@FindBy(xpath = "//*[@id='investmentsContainer:data:tbody_element']/tr/td[8]")
	private WebElement returnAmountIfCorrectLabel;
	@FindBy(xpath = "//*[@id='investmentsContainer:data:tbody_element']/tr/td[9]")
	private WebElement returnAmountIfIncorrectLabel;
	@FindBy(xpath = "//*[@id='investmentsContainer:data:tbody_element']/tr/td[1]")
	private WebElement optionId;
	@FindBy(xpath = "//*[@id='investmentsContainer:data:tbody_element']/tr/td[2]")
	private WebElement assetName;
	@FindBy(xpath = "//*[@id='investmentsContainer:data:tbody_element']/tr/td[3]")
	private WebElement levelLabel;
	@FindBy(xpath = "//*[@id='investmentsContainer:data:tbody_element']/tr/td[5]/table/tbody/tr/td[1]")
	private WebElement expiryTime;
	@FindBy(id = "investmentsContainer:marketChoice")
	private WebElement optionsTypeSelect;
	@FindBy(css = "#investmentsContainer > table:nth-child(12) > tbody > tr > td > table > tbody > tr > td > span")
	private WebElement noOptionsMessage;
	@FindBy(id = "investmentClose")
	private WebElement investmentCloseRadioButton;
	@FindBy(css = "#investmentsContainer > table.flex.date_range_table > tbody > tr:nth-child(1) > td:nth-child(7) > span")
	private WebElement submitButton;
	@FindBy(xpath = "//*[@id='investmentsContainer:bubble_data:tbody_element']/tr[1]/td[1]")
	private WebElement bubblesInvestmentOptionId;
	@FindBy(xpath = "//*[@id='investmentsContainer:bubble_data:tbody_element']/tr[1]/td[2]")
	private WebElement bubblesInvestmentMarketName;
	@FindBy(xpath = "//*[@id='investmentsContainer:bubble_data:tbody_element']/tr[1]/td[3]/div")
	private WebElement bubblesInvestmentLevel;
	@FindBy(xpath = "//*[@id='investmentsContainer:bubble_data:tbody_element']/tr[1]/td[5]/table/tbody/tr/td")
	private WebElement bubblesExpiryTime;
	@FindBy(xpath = "//*[@id='investmentsContainer:bubble_data:tbody_element']/tr[1]/td[6]/span")
	private WebElement bubblesInvestedAmount;
	@FindBy(xpath = "//*[@id='investmentsContainer:bubble_data:tbody_element']/tr[1]/td[7]/span")
	private WebElement bubblesReturnValue;
	@FindBy(xpath = "//*[@id='investmentsContainer:data:tbody_element']/tr[1]/td[4]/span")
	private WebElement optionPlusSoldOptionLevel;
	@FindBy(xpath = "//*[@id='investmentsContainer:data:tbody_element']/tr[1]/td[1]")
	private WebElement settledOptionInvestmentId;
	@FindBy(xpath = "//*[@id='investmentsContainer:data:tbody_element']/tr[1]/td[2]")
	private WebElement settledOptionMarked;
	@FindBy(xpath = "//*[@id='investmentsContainer:data:tbody_element']/tr[1]/td[3]")
	private WebElement settledOptionLevel;
	@FindBy(xpath = "//*[@id='investmentsContainer:data:tbody_element']/tr[1]/td[7]/span")
	private WebElement settledOptionAmount;
	@FindBy(xpath = "//*[@id='investmentsContainer:data:tbody_element']/tr[1]/td[9]/span")
	private WebElement settledOptionReturnedValue;
	@FindBy(xpath = "//*[@id='investmentsContainer:data:tbody_element']/tr[1]/td[4]")
	private WebElement settledOptionExpiryLevel;
	@FindBys({@FindBy(xpath = "//*[@id='investmentsContainer:data:tbody_element']/tr/td[1]")})
	private List<WebElement> binaryOptionsInvestmentId;
	@FindBy(xpath = "//*[@id='investmentsContainer:data:tbody_element']/tr/td[7]/span")
	private WebElement dynamicsPicture;
	/**
	 * 
	 * @param driver
	 */
	public MyAccountPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Is investment dynamics
	 * @return boolean
	 */
	public boolean isDynamicsInvestment() {
		String className = dynamicsPicture.getAttribute("class");
		if (className.equals("inv_dynamics inv_p")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Gets expiry time 
	 * @return
	 */
	public String getExpiryTimeText() {
		return expiryTime.getText();
	}
	/**
	 * Gets count binary options active investments.
	 * @return Integer
	 */
	public int getCountActiveInvestments() {
		return binaryOptionsInvestmentId.size();
	}
	/**
	 * Gets settled option expiry level
	 * @return String
	 */
	public String getSettledOptionExpiryLevel() {
		return settledOptionExpiryLevel.getText();
	}
	/**
	 * Gets settled option returned value
	 * @return String
	 */
	public String getSettledOptionReturnValue() {
		return settledOptionReturnedValue.getText();
	}
	/**
	 * Gets settled option amount
	 * @return String
	 */
	public String getSettledOptionAmount() {
		return settledOptionAmount.getText();
	}
	/**
	 * Gets settled option level
	 * @return String
	 */
	public String getSettledOptionLevel() {
		return settledOptionLevel.getText();
	}
	/**
	 * Gets settled option asset name
	 * @return String
	 */
	public String getSettledOptionAsset() {
		return settledOptionMarked.getText();
	}
	/**
	 * Gets settled option investment id
	 * @return String
	 */
	public String getSettledOptionInvestmentId() {
		return settledOptionInvestmentId.getText();
	}
	/**
	 * Gets sold option level
	 * @return
	 */
	public String getSoldOptionLevel() {
		return optionPlusSoldOptionLevel.getText();
	}
	/**
	 * Gets bubbles return value
	 */
	public String getBubblesReturnAmount(String currency) {
		String amount = bubblesReturnValue.getText();
		if (amount.equals(currency + "0.00")) {
			amount = amount.substring(0, amount.length() - 3);
		}
		return amount;
	}
	/**
	 * Gets bubbles invested amount
	 * @return
	 */
	public String getBubblesInvestedAmount() {
		return bubblesInvestedAmount.getText();
	}
	/**
	 * Gets bubbles expiry time
	 * @return
	 */
	public String getBubblesExpiryTime() {
		return bubblesExpiryTime.getText();
	}
	/**
	 * Gets bubbles investment level
	 * @return
	 */
	public String getBubblesLevel() {
		return bubblesInvestmentLevel.getText();
	}
	/**
	 * Gets bubbles investment market name
	 * @return
	 */
	public String getBubblesMarketName() {
		return bubblesInvestmentMarketName.getText();
	}
	/**
	 * Gets bubbles investment option id
	 * @return
	 */
	public String getBubblesOptionId() {
		return bubblesInvestmentOptionId.getText();
	}
	/**
	 * Clicks submit button
	 */
	public void clickSubmitButton() {
		submitButton.click();
	}
	/**
	 * Chooses closed investments
	 */
	public void clickClosedInvestmentRadioButton() {
		investmentCloseRadioButton.click();
	}
	/**
	 * Gets no options message
	 * @return
	 */
	public String getNoOptionsMessage() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#investmentsContainer > table:nth-child(12) > tbody > tr > td > table > tbody > tr > td > span")));
		return noOptionsMessage.getText();
	}
	/**
	 * Selects options type
	 * @param value
	 */
	public void selectOptionsType(String value) {
		Select select = new Select(optionsTypeSelect);
		select.selectByValue(value);
	}
	/**
	 * Gets expires time
	 * @return
	 */
	public String getExpiryTime() {
		String times[] = expiryTime.getText().split(" ");
		return times[0];
	}
	/**
	 * Gets level
	 * @return
	 */
	public String getLevel() {
		return levelLabel.getText();
	}
	/**
	 * Gets asset name
	 * @return
	 */
	public String getAsset() {
		return assetName.getText();
	}
	/**
	 * Gets option id
	 * @return
	 */
	public String getOptionId() {
		return optionId.getText();
	}
	/**
	 * Gets amount value to return if is incorrect or sell option plus option
	 * @return
	 */
	public String getAmountIfIncorrect() {
		return returnAmountIfIncorrectLabel.getText();
	}
	/**
	 * Gets amount value to return if is correct
	 * @return
	 */
	public String getAmountIfCorrect() {
		return returnAmountIfCorrectLabel.getText();
	}
	/**
	 * Gets commission tooltip text
	 * @return
	 */
	public String getCommisionTooltipText() {
		return commissionTooltip.getText();
	}
	/**
	 * Gets investment amount from my options menu
	 * @return
	 */
	public String getInvestmentAmount() {
		return investmentAmountLabel.getText();
	}
}
