package com.anyoption.autotest.pages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class InvestmentPage {
	private WebDriver driver;
	@FindBy(css = "#profitLine_h > i.tradeBox_img_big.sprite_profitLine_close")
	private WebElement profitLineWindowCloseButton;
	@FindBy(id = "profitLine_curr_rtn_val_4")
	private WebElement returnValueLabel;
	@FindBy(id = "binary-legend")
	private WebElement binaryLegendIcon;
	/**
	 * 
	 * @param driver
	 */
	public InvestmentPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Make double click on cancel investment link
	 * @param number
	 */
	public void doubleClickCancelInvestment(int number) {
		String css;
		if (number == - 2) {
			css = "#cancel-inv-slip > div.taC > span";
		} else {
			css = "#submit-link_" + number;
		}	
		WebElement cancelLink = driver.findElement(By.cssSelector(css));
		Actions action = new Actions(driver);
  		action.doubleClick(cancelLink).perform();
	}
	/**
	 * Is button for binary legend displayed
	 * @return boolean
	 */
	public boolean isBinaryLegendDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 1);
		try {
			wait.until(ExpectedConditions.attributeToBe(binaryLegendIcon, "style", "display: block;"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Gets message in chart
	 * @param number
	 * @return String
	 */
	public String getChartMessage(int number) {
		String id = "tradeBox_chart_longTerm_" + number;
		String message = driver.findElement(By.id(id)).getText().replace("\n", " ");
		return message;
	}
	/**
	 * Is message chart displayed
	 * @param number
	 * @return boolean
	 */
	public boolean isChartMessageDisplayed(int number) {
		String id = "tradeBox_chart_longTerm_" + number;
		String attribute = driver.findElement(By.id(id)).getAttribute("style");
		if (attribute.equals("display: block;")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Gets investment expiry time
	 * @param number
	 * @return
	 */
	public String getInvestmentExpTime(int number) {
		String id = "trade_box_exp_time_" + number;
		String time = driver.findElement(By.id(id)).getText();
		return time;
	}
	/**
	 * Chooses investment expiry time
	 * @param number
	 * @param text
	 */
	public void chooseBinaryOptionExpTime(int number, String text) {
		String id = "trade_box_exp_time_" + number;
		WebElement dropDown = driver.findElement(By.id(id));
		try {
			dropDown.click();
		} catch (WebDriverException e) {
			System.out.println("Linux exception");
			UtilsMethods.clickWithJavaScript(driver, dropDown);
		}
		String css = "#tradeBox_menu_exp_" + number + " > li";
		List<WebElement> options = driver.findElements(By.cssSelector(css));
		for (WebElement option : options) {
			if (option.getText().equals(text)) {
				try {
					option.click();
				} catch (WebDriverException e) {
					System.out.println("Linux exception");
					UtilsMethods.clickWithJavaScript(driver, option);
				}
				break;
			}
		}
 	}
	/**
	 * Sets currency market in binary option trasding
	 * @param number
	 */
	public void setCurrencyMarket(int number) {
		String id = "tradeBox_marketName_" + number;
		WebElement marketsDropDown = driver.findElement(By.id(id));
		String[] markets = {"289", "291", "287", "16", "351", "14", "290", "352", "692", "698", "18", "682", "684", "681"};
		for (int i = 0; i < markets.length; i++) {
			try {
				marketsDropDown.click();
			} catch (WebDriverException e) {
				System.out.println("Linux exception with FireFox");
				UtilsMethods.clickWithJavaScript(driver, marketsDropDown);
			}
			UtilsMethods.sleep(500);
			String marketId = "m" + markets[i];
			WebElement marketOption = driver.findElement(By.id(marketId));
			try {
				try {
					marketOption.click();
				} catch (WebDriverException e) {
					System.out.println("Linux exception with FireFox");
					UtilsMethods.clickWithJavaScript(driver, marketOption);
				}
				if (this.isMarketActive(number) == true) {
					break;
				} 
			} catch (UnhandledAlertException e) {
				Alert alert = driver.switchTo().alert();
				alert.accept();
				marketsDropDown.click();
			}
		}
	}
	/**
	 * Is binary option tab active
	 * @return
	 */
	public boolean IsBinaryOptionTabActive() {
		boolean result = true;
		String selector = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div.page_bgr_trade > ul "
				+ "> li.trade_page_tabs_h.trade_page_tabs_binary_opt_h.active";
		WebDriverWait wait = new WebDriverWait(driver, 10);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(selector)));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Opens print investment window 
	 * @param number
	 */
	public void openPrintInvestmentWindow(int number) {
		String id = "tradeBox_print_" + number;
		driver.findElement(By.id(id)).click();
	}
	/**
	 * Waits until profit line is opened
	 */
	public void waitProfitLineToBeOpened() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.attributeToBe(By.id("profitLine_h_bgr"), "style", "display: block;"));
	}
	/**
	 * Clicks login button
	 * @param number
	 */
	public void clickLoginButton(int number) {
		String css = "#tradeBox_pop_login_" + number + " > a.trade_buton_login.trade_buton_register_h.clear.mt2 > span > span";
		WebElement button = driver.findElement(By.cssSelector(css));
		try {
			button.click();
		} catch (WebDriverException e) {
			UtilsMethods.clickWithJavaScript(driver, button);
		}
	}
	/**
	 * Is pop up for login displayed
	 * @param number
	 * @return
	 */
	public boolean isPopUpForLoginDisplayed(int number) {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		String id = "tradeBox_pop_holder_" + number;
		boolean result = true;
		try {
			wait.until(ExpectedConditions.attributeToBe(By.id(id), "class", "tradeBox_pop_holder login"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Executes java script to hide cancel investment screen 
	 * @param number
	 */
	public void hideCancelInvestmentPopUp(int number) {
		String script = "document.getElementById('chart-overlays_" + number + "').setAttribute('class', 'chart-overlays')";
		((JavascriptExecutor) driver).executeScript(script);
	}
	/**
	 * Executes java script to show cancel investment screen
	 * @param number
	 */
	public void showCancelInvestmentPopUp(int number) {
		String script = "document.getElementById('chart-overlays_" + number + "').setAttribute('class', 'chart-overlays cancel-inv')";
		((JavascriptExecutor) driver).executeScript(script);
	}
	/**
	 * Is canceled message displayed
	 * @return boolean
	 */
	public boolean isCanceledMessageDisplayed(int number) {
		String id;
		if (number == - 2) {
			id = "chart-overlays";
		} else {
			id = "chart-overlays_" + number;
		}	
		WebDriverWait wait = new WebDriverWait(driver, 10);
		try {
			wait.until(ExpectedConditions.attributeContains(By.id(id), "class", "cancel-inv-msg"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Gets canceled message
	 * @return String
	 */
	public String getCanceledMessage(int number) {
		String css;
		if (number == - 2) {
			css = "#cancel-inv-msg-slip > div.text";
		} else {
			css = "#cancel-inv-msg-slip_" + number +" > div.text";
		}
		String text = driver.findElement(By.cssSelector(css)).getText();
		return text;
	}
	/**
	 * Cancels investment
	 * @param number
	 */
	public void cancelInvestment(int number) {
		String css;
		if (number == - 2) {
			css = "#cancel-inv-slip > div.taC > span";
		} else {
			css = "#submit-link_" + number;
		}	
		WebElement cancelLink = driver.findElement(By.cssSelector(css));
		try {
			cancelLink.click();
		} catch (WebDriverException e) {
			System.out.println("Exception with cancel link.");
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", cancelLink);
		}
	}
	/**
	 * Gets cancel link text
	 * @param number
	 * @return String
	 */
	public String getCancelLinkText(int number) {
		String css;
		if (number == - 2) {
			css = "#cancel-inv-slip > div.taC > span";
		} else {
			css = "#submit-link_" + number;
		}	
		String text = driver.findElement(By.cssSelector(css)).getText();
		return text;
	}
	/**
	 * Gets cancel investment text
	 * @param number
	 * @return String
	 */
	public String getMessageFromCancelInv(int number) {
		String id;
		if (number == - 2) {
			id = "cancelInvText";
		} else {
			id = "cancelInvText_" + number;
		}	
		String text = driver.findElement(By.id(id)).getText();
		return text;
	}
	/**
	 * Gets amount from cancel investment
	 * @param number
	 * @return String
	 */
	public String getAmountFromCancelInv(int number) {
		final String id;
		if (number == - 2) {
			id = "cancel-inv-amount";
		} else {
			id = "cancel-inv-amount_" + number;
		}	
		String amount = driver.findElement(By.id(id)).getText();
		return amount;
	}
	/**
	 * Gets level from cancel investment
	 * @param number
	 * @return String
	 */
	public String getLevelFromCancelInv(int number) {
		String id;
		if (number == - 2) {
			id = "cancel-inv-level";
		} else {
			id = "cancel-inv-level_" + number;
		}	
		String level = driver.findElement(By.id(id)).getText();
		return level;
	}
	/**
	 * Checks is market active
	 * @param number
	 * @return boolean
	 */
	public boolean isMarketActive(int number) {
		boolean result = false;
		String idButton = "tradeBox_call_btn_" + number;
		String idHolder = "tradeBox_pop_holder_" + number;
		String idTime = "tradeBox_time_invest_d_" + number;
		String idTradeBox = "tradeBox_chartHeader_" + number;
		WebElement elButton = driver.findElement(By.id(idButton));
		WebElement elHolder = driver.findElement(By.id(idHolder));
		WebElement elTime = driver.findElement(By.id(idTime));
		WebElement elTradeBox = driver.findElement(By.id(idTradeBox));
		String classTypeButton = elButton.getAttribute("class");
		String classTypeHolder = elHolder.getAttribute("class");
		String classTradeBoxName = elTradeBox.getAttribute("class");
		int seconds = 0;
		int minutes = 0;
		int remainingTime = 240; //OR 0
		String time = elTime.getText();
		if (! time.equals("") && classTradeBoxName.contains("time_invest")) {
			String[] timeArr = time.split(":");
			seconds = Integer.parseInt(timeArr[1]);
			minutes = Integer.parseInt(timeArr[0]);
			remainingTime = (minutes * 60) + seconds;
		} else if (classTradeBoxName.contains("waiting_for_exp")) {
			remainingTime = 0;
		}
		if (classTypeButton.contains("active") && ! classTypeHolder.contains("loading") && ! classTypeHolder.contains("notAv") 
				&& remainingTime >= 240 && ! classTradeBoxName.contains("waiting_for_exp")) {
			
			UtilsMethods.sleep(3000);
			
			String bottomId = "tradeBox_bottom_" + number;
			String bottomClass = driver.findElement(By.id(bottomId)).getAttribute("class");
			if (! bottomClass.contains("disabled")) {
				result = true;
			}
		}
		return result;
	}
	/**
	 * Gets time to cancel
	 * @param number
	 * @return String
	 */
	public String getTimeToCancel(int number) {
		String id;
		if (number == - 2) {
			id = "cancel-inv-time-left-sec";
		} else {
			id = "cancel-inv-time-left-sec_" + number;
		}	
		String[] time = driver.findElement(By.id(id)).getText().split(":");
		return time[1];
	}
	/**
	 * Gets current market id
	 * @param number
	 * @return String
	 */
	public String getCurrentMarketId(int number) {
		String id = "";
		String marketName = "tradeBox_marketName_" + number;
		WebElement name = driver.findElement(By.id(marketName));
		try {
			name.click();
		} catch (WebDriverException e) {
			System.out.println("Exception click");
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", name);
		}
		List<WebElement> markets = driver.findElements(By.cssSelector("#treeM > li"));
		int size = markets.size();
		for (int i = 0; i < size; i++) {
			if (markets.get(i).getAttribute("class").contains("active")) {
				id = markets.get(i).getAttribute("id").replace("m", "");
				break;
			}
		}
		try {
			name.click();
		} catch (WebDriverException e) {
			System.out.println("Exception click");
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", name);
		}
		return id;
	}
	/**
	 * Gets data for deviation 2 investment
	 * @param number
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, String> getDataForDev2Check(int number) throws Exception {
		String marketId = this.getCurrentMarketId(number);
		HashMap<String, String> data = new HashMap<>();
		data = User.getDev2Data(marketId);
		return data;
	}
	/**
	 * Is cancel investment displayed
	 * @param number
	 * @return boolean
	 */
	public boolean isCancelInvestmentDisplayed(int number) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		String id;
		if (number == - 2) { 
			id = "chart-overlays";
		} else {	
			id = "chart-overlays_" + number;
		}	
		try {
			wait.until(ExpectedConditions.attributeToBe(By.id(id), "class", "chart-overlays cancel-inv"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Is cancel investment displayed
	 * @param number
	 * @param timeOut
	 * @return boolean
	 */
	public boolean isCancelInvestmentDisplayed(int number, int timeOut) {
		WebDriverWait wait = new WebDriverWait(driver, timeOut);
		String id;
		if (number == - 2) { 
			id = "chart-overlays";
		} else {	
			id = "chart-overlays_" + number;
		}	
		try {
			wait.until(ExpectedConditions.attributeToBe(By.id(id), "class", "chart-overlays cancel-inv"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Gets all amount in string with currency sign and double digit
	 * @param number
	 * @return
	 */
	public String getIncorectAmount(int number) {
		String amount;
		String id = "tradeBox_incorrect_amount_" + number;
		String css = "#" + id + " > span";
		String allText = driver.findElement(By.id(id)).getText();
		String cents = driver.findElement(By.cssSelector(css)).getText();
		allText = allText.substring(0, allText.length() - 2);
		amount = allText + "." + cents;
		return amount;
	}
	/**
	 * Waits holder do not load
	 * @param number
	 */
	public void waitHolder(int number) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		String id = "tradeBox_pop_holder_" + number; 
		wait.until(ExpectedConditions.not(ExpectedConditions.attributeContains(By.id(id), "class", "loading")));
	}
	/**
	 * Gets market name
	 * @param number
	 * @return
	 */
	public String getMarketName(int number) {
		String id = "tradeBox_marketName_" + number;
		return driver.findElement(By.id(id)).getText();
	}
	/**
	 * Gets the level from after invest pop up
	 * @param number
	 * @return
	 */
	public String getLevelFromAfterInvest(int number) {
		String id = "tradeBox_AI_level_" + number;
		return driver.findElement(By.id(id)).getText();
	}
	/**
	 * Gets investmentId from after invest pop up
	 * @param number
	 * @return
	 */
	public String getInvestmentId(int number) {
		String id = "tradeBox_AI_option_id_" + number;
		return driver.findElement(By.id(id)).getText();
	}
	/**
	 * Gets expires time
	 * @param number
	 * @return
	 */
	public String getExpiresTime(int number) {
		String id = "tradeBox_AI_expires_" + number;
		String times[] = driver.findElement(By.id(id)).getText().split(" ");
		return times[0];
	}
	
	/**
	 * Clicks deposit button from pop up
	 * @param number
	 */
	public void clickDepositButtonFromPopUp(int number) {
		String css = "#tradeBox_pop_noDeposit_" + number + " > a > span";
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(css)));
		driver.findElement(By.cssSelector(css)).click();
	}
	/**
	 * Clicks deposit button from pop up
	 * @param number
	 */
	public void clickDepositButtonFromPopUp(int number, String type) {
		try {
			String css = "#tradeBox_pop_noDeposit_" + number + " > a > span";
			WebDriverWait wait = new WebDriverWait(driver, 15);
			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(css)));
			driver.findElement(By.cssSelector(css)).click();
		} catch (TimeoutException e) {
			if (type.equals("UP")) {
				this.clickUpButton(number);
			} else if (type.equals("DOWN")) {
				this.clickDownButton(number);
			}
		}
	}
	/**
	 * Closes no deposit pop up
	 * @param number
	 */
	public void closeNoDepositPopUp(int number) {
		String id = "tradeBox_pop_noDeposit_close_" + number;
		driver.findElement(By.id(id)).click();
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id(id)));
	}
	/**
	 * Gets text message form pop up no deposit
	 * @param number
	 * @return
	 */
	public String getMessageFromPopUpForDeposit(int number) {
		String id = "tradeBox_pop_noDeposit_" + number;
		String message = driver.findElement(By.id(id)).getText();
		return message;
	}
	/**
	 * Returns is pop up for make deposit displayed
	 * @param number
	 * @return
	 */
	public boolean isPopUpForMakeDepositDisplayed(int number) {
		String id = "tradeBox_pop_noDeposit_" + number;
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
		WebElement popUp = driver.findElement(By.id(id));
		if (popUp.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Returns is pop up for make deposit displayed
	 * @param number
	 * @param type - UP or DOWN
	 * @return
	 */
	public boolean isPopUpForMakeDepositDisplayed(int number, String type) {
		String id = "tradeBox_pop_noDeposit_" + number;
		WebDriverWait wait = new WebDriverWait(driver, 15);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
		} catch (TimeoutException e) {
			if (type.equals("UP")) {
				this.clickUpButton(number);
			} else if (type.equals("DOWN")) {
				this.clickDownButton(number);
			}
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
		}
		WebElement popUp = driver.findElement(By.id(id));
		if (popUp.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Gets first number of active trades
	 * @return
	 */
	public int getAcctiveInvestment() {
		int number = -1;
		for (int i = 0; i < 4; i++) {
			String idButton = "tradeBox_call_btn_" + i;
			String idHolder = "tradeBox_pop_holder_" + i;
			String idTime = "tradeBox_time_invest_d_" + i;
			String idTradeBox = "tradeBox_chartHeader_" + i;
			WebElement elButton = driver.findElement(By.id(idButton));
			WebElement elHolder = driver.findElement(By.id(idHolder));
			WebElement elTime = driver.findElement(By.id(idTime));
			WebElement elTradeBox = driver.findElement(By.id(idTradeBox));
			String classTypeButton = elButton.getAttribute("class");
			String classTypeHolder = elHolder.getAttribute("class");
			String classTradeBoxName = elTradeBox.getAttribute("class");
			int seconds = 0;
			int minutes = 0;
			int remainingTime = 240; //OR 0
			String time = elTime.getText();
			if (! time.equals("") && classTradeBoxName.contains("time_invest")) {
				String[] timeArr = time.split(":");
				seconds = Integer.parseInt(timeArr[1]);
				minutes = Integer.parseInt(timeArr[0]);
				remainingTime = (minutes * 60) + seconds;
			} else if (classTradeBoxName.contains("waiting_for_exp")) {
				remainingTime = 0;
			}
			if (classTypeButton.contains("active") && ! classTypeHolder.contains("loading") && ! classTypeHolder.contains("notAv") 
					&& remainingTime >= 240 && ! classTradeBoxName.contains("waiting_for_exp")) {
				
				UtilsMethods.sleep(3000);
				
				String bottomId = "tradeBox_bottom_" + i;
				String bottomClass = driver.findElement(By.id(bottomId)).getAttribute("class");
				if (! bottomClass.contains("disabled")) {
					number = i;
					break;
				}
			}
		}
		System.out.println("Active market number is " + number);
		return number;
	}
	/**
	 * Clicks on investment drop down
	 * @param number
	 */
	public void clickInvestmentDropDown(int number) {
		String css = "#tradeBox_bottom_" + number + " > div.tradeBox_bottom_rs > div.fL.tradeBox_invest_holder.tradeBox_invest_holder_1 > ul > li > span";
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(css)));
		try {
			driver.findElement(By.cssSelector(css)).click();
		} catch (WebDriverException e) {
			driver.navigate().refresh();
			UtilsMethods.sleep(3000);
//			driver.findElement(By.cssSelector(css)).click();
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click();", driver.findElement(By.cssSelector(css)));
		}
	}
	/**
	 * Gets current asset
	 * @return String
	 */
	public String getCurrentAssetFormProfiteLine() {
		WebElement marketDropDownTitle = driver.findElement(By.id("profitLine_marketDrop_title_txt_4"));
		String title[] = marketDropDownTitle.getText().split("-");
		String result = title[0].substring(0, title[0].length() - 1);
		return result;
	}
	/**
	 * Clicks on investment drop down
	 * @param number
	 */
	public void clickInvestmentDropDownById(int number) {
		String id = "tradeBox_inv_amount_" + number;
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(By.id(id)));
		try {
			driver.findElement(By.id(id)).click();
		} catch (WebDriverException e) {
			driver.navigate().refresh();
			UtilsMethods.sleep(3000);
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click();", driver.findElement(By.id(id)));
		}
	}
	/**
	 * Choose amount for invest
	 * @param number
	 * @param index
	 */
	public void chooseAmountToInvest(int number, int index) {
		String css = "#tradeBox_invest_drop_ul_" + number + " > li";
		List<WebElement> options = driver.findElements(By.cssSelector(css));
		try {
			options.get(index).click();
		} catch (WebDriverException e) {
			UtilsMethods.clickWithJavaScript(driver, options.get(index));
		}
	}
	/**
	 * Gets amount to invest from drop down
	 * @param number
	 * @return
	 */
	public String getTheAmountForInvest(int number) {
		String id = "tradeBox_inv_amount_" + number;
		WebElement elem = driver.findElement(By.id(id));
		return elem.getText();
	}
	/**
	 * Clicks on profit drop down
	 * @param number
	 */
	public void clickProfitDropDown(int number) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		String id = "tradeBox_profit_procent_" + number;
		wait.until(ExpectedConditions.elementToBeClickable(By.id(id)));
		WebElement element = driver.findElement(By.id(id));
		try {
			element.click();
		} catch (WebDriverException e) {
			System.out.println("Light streemer refresh");
			String idHolder = "tradeBox_pop_holder_" + number;
			wait.until(ExpectedConditions.attributeToBe(By.id(idHolder), "class", "tradeBox_pop_holder hidden"));
			UtilsMethods.clickWithJavaScript(driver, element);
		}
	}
	/**
	 * Sets custom amount to invest
	 * @param number
	 * @param amount
	 */
	public void setAmountToInvest(int number, String amount) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		String id = "tradeBox_inv_amount_" + number;
		WebElement dropDown = driver.findElement(By.id(id));
		try {
			dropDown.click();
		} catch (WebDriverException e) {
			System.out.println("Light streemer refresh");
			String idHolder = "tradeBox_pop_holder_" + number;
			wait.until(ExpectedConditions.attributeToBe(By.id(idHolder), "class", "tradeBox_pop_holder hidden"));
			try {
				dropDown.click();
			} catch (WebDriverException ex) {
				UtilsMethods.clickWithJavaScript(driver, dropDown);
			}
		}
		String idInput = "tradeBox_invest_drop_custom_" + number;
		driver.findElement(By.id(idInput)).sendKeys(amount);
	}
	/**
	 * Choose profit from drop down
	 * @param number
	 * @param index
	 */
	public String[] chooseProfit(final int number, int index) {
		String css = "#tradeBox_profit_drop_ul_" + number + " > li";
		List<WebElement> profits = driver.findElements(By.cssSelector(css));
		index = index + 1;
		String pr[] = new String[2];
		if (profits.size() > 1) {
			pr[0] = profits.get(index).getAttribute("data-profit");
			pr[1] = profits.get(index).getAttribute("data-refund");
			try {
				profits.get(index).click(); 
			} catch (WebDriverException e) {
				System.out.println("Light streem refresh");
				String idHolder = "tradeBox_pop_holder_" + number;
				WebDriverWait wait = new WebDriverWait(driver, 15);
				wait.until(ExpectedConditions.attributeToBe(By.id(idHolder), "class", "tradeBox_pop_holder hidden"));
				profits.get(index).click(); 
			}
		} else {
			ArrayList<String> markets = this.getCurrentMarkets();
			String id = "tradeBox_marketName_" + number;
			driver.findElement(By.id(id)).click();
			String className = "menu_market_element";
			List<WebElement> options = driver.findElements(By.className(className));
			for (WebElement opt : options) {
				String optionName = opt.getText();
				String attrName = opt.getAttribute("onclick");
				String attrStyle = opt.getAttribute("style");
				if (! markets.contains(optionName) && attrName != null && attrStyle.contains("block")) {
					opt.click();
					WebDriverWait wait = new WebDriverWait(driver, 20);
					try {
						wait.until(new ExpectedCondition<Boolean>() {
							@Override
							public Boolean apply(WebDriver driver) {
								String id = "tradeBox_level_" + number;
								String level = driver.findElement(By.id(id)).getText();
								if (level.equals("Loading...")) {
									System.out.println("wait level");
									return false;
								} else {
									return true;
								}
							}
						});
						this.clickProfitDropDown(number);
						profits = driver.findElements(By.cssSelector(css));
						pr[0] = profits.get(index).getAttribute("data-profit");
						pr[1] = profits.get(index).getAttribute("data-refund");
						profits.get(index).click(); 
						break;
					} catch (UnhandledAlertException e) {
						Alert alert = driver.switchTo().alert();
						alert.accept();
					}
				}
			}
		}
		return pr;
	}
	
	/**
	 * Choose profit from drop down from profit line
	 * @param number
	 * @param index
	 */
	public String[] chooseProfitFromProfitLine(final int number, int index) {
		String css = "#tradeBox_profit_drop_ul_" + number + " > li";
		List<WebElement> profits = driver.findElements(By.cssSelector(css));
		index = index + 1;
		String pr[] = new String[2];
		if (profits.size() > 1) {
			pr[0] = profits.get(index).getAttribute("data-profit");
			pr[1] = profits.get(index).getAttribute("data-refund");
			try {
				profits.get(index).click(); 
			} catch (WebDriverException e) {
				System.out.println("Light streem refresh");
				String idHolder = "tradeBox_pop_holder_" + number;
				WebDriverWait wait = new WebDriverWait(driver, 15);
				wait.until(ExpectedConditions.attributeToBe(By.id(idHolder), "class", "tradeBox_pop_holder hidden"));
				profits.get(index).click(); 
			}
		} else {
			String id = "profitLine_marketDrop_title_" + number;
			driver.findElement(By.id(id)).click();
			String className = "profitLine_marketDrop_element";
			List<WebElement> options = driver.findElements(By.className(className));
			for (WebElement opt : options) {
				opt.click();
				WebDriverWait wait = new WebDriverWait(driver, 20);
				wait.until(new ExpectedCondition<Boolean>() {
					@Override
					public Boolean apply(WebDriver driver) {
						String id = "tradeBox_level_" + number;
						String level = driver.findElement(By.id(id)).getText();
						if (level.equals("Loading...")) {
							System.out.println("wait level");
							return false;
						} else {
							return true;
						}
					}
				});
				this.clickProfitDropDown(number);
				profits = driver.findElements(By.cssSelector(css));
				if (profits.size() > 1) {
					pr[0] = profits.get(index).getAttribute("data-profit");
					pr[1] = profits.get(index).getAttribute("data-refund");
					profits.get(index).click(); 
					break;
				}	
			}
		}
		return pr;
	}
	/**
	 * Chooses default profits
	 * @param number
	 * @return Array of Strings
	 */
	public String[] chooseDefaultProfit(final int number) {
		String css = "#tradeBox_profit_drop_ul_" + number + " > li";
		List<WebElement> profits = driver.findElements(By.cssSelector(css));
		String pr[] = new String[2];
		if (profits.size() > 1) {
			for (WebElement prof : profits) {
				if (prof.getAttribute("class").contains("active")) {
					pr[0] = prof.getAttribute("data-profit");
					pr[1] = prof.getAttribute("data-refund");
					try {
						prof.click(); 
					} catch (WebDriverException e) {
						System.out.println("Light streem refresh");
						String idHolder = "tradeBox_pop_holder_" + number;
						WebDriverWait wait = new WebDriverWait(driver, 15);
						wait.until(ExpectedConditions.attributeToBe(By.id(idHolder), "class", "tradeBox_pop_holder hidden"));
						prof.click(); 
					}
					break;
				}
			}	
		} else {
			ArrayList<String> markets = this.getCurrentMarkets();
			String id = "tradeBox_marketName_" + number;
			driver.findElement(By.id(id)).click();
			String className = "menu_market_element";
			List<WebElement> options = driver.findElements(By.className(className));
			for (WebElement opt : options) {
				String optionName = opt.getText();
				String attrName = opt.getAttribute("onclick");
				String attrStyle = opt.getAttribute("style");
				if (! markets.contains(optionName) && attrName != null && attrStyle.contains("block")) {
					opt.click();
					WebDriverWait wait = new WebDriverWait(driver, 20);
					try {
						wait.until(new ExpectedCondition<Boolean>() {
							@Override
							public Boolean apply(WebDriver driver) {
								String id = "tradeBox_level_" + number;
								String level = driver.findElement(By.id(id)).getText();
								if (level.equals("Loading...")) {
									System.out.println("wait level");
									return false;
								} else {
									return true;
								}
							}
						});
						this.clickProfitDropDown(number);
						profits = driver.findElements(By.cssSelector(css));
						for (WebElement prof : profits) {
							if (prof.getAttribute("class").contains("active")) {
								pr[0] = prof.getAttribute("data-profit");
								pr[1] = prof.getAttribute("data-refund");
								prof.click();
								break;
							}
						}	
						break;
					} catch (UnhandledAlertException e) {
						Alert alert = driver.switchTo().alert();
						alert.accept();
					}
				}
			}
		}
		return pr;
	}
	/**
	 * Changes market
	 * @param index
	 */
	public void changeMarket(final int index) {
		ArrayList<String> markets = this.getCurrentMarkets();
		String id = "tradeBox_marketName_" + index;
		driver.findElement(By.id(id)).click();
		String className = "menu_market_element";
		List<WebElement> options = driver.findElements(By.className(className));
		for (WebElement opt : options) {
			String optionName = opt.getText();
			String attrName = opt.getAttribute("onclick");
			String attrStyle = opt.getAttribute("style");
			if (! markets.contains(optionName) && attrName != null && attrStyle.contains("block")) {
				opt.click();
				WebDriverWait wait = new WebDriverWait(driver, 20);
				try {
					wait.until(new ExpectedCondition<Boolean>() {
						@Override
						public Boolean apply(WebDriver driver) {
							String id = "tradeBox_level_" + index;
							String level = driver.findElement(By.id(id)).getText();
							if (level.equals("Loading...")) {
								System.out.println("wait level");
								return false;
							} else {
								return true;
							}
						}
					});
					break;
				} catch (UnhandledAlertException e) {
					Alert alert = driver.switchTo().alert();
					alert.accept();
				}
			}
		}
	}
	
	/**
	 * Changes market until set active market
	 * @param number
	 */
	public void chooseActiveMarket(final int number, String currentName) {
		ArrayList<String> markets = this.getCurrentMarkets();
		String id = "tradeBox_marketName_" + number;
		WebElement marketDropDown = driver.findElement(By.id(id));
		marketDropDown.click();
		String className = "menu_market_element";
		List<WebElement> options = driver.findElements(By.className(className));
		for (WebElement opt : options) {
			String optionName = opt.getText();
			if (optionName.equals(currentName)) {
				continue;
			}
			String attrName = opt.getAttribute("onclick");
			String attrStyle = opt.getAttribute("style");
			if (! markets.contains(optionName) && attrName != null && attrStyle.contains("block")) {
				opt.click();
				WebDriverWait wait = new WebDriverWait(driver, 20);
				try {
					try {
						wait.until(new ExpectedCondition<Boolean>() {
							@Override
							public Boolean apply(WebDriver driver) {
								String id = "tradeBox_level_" + number;
								String level = driver.findElement(By.id(id)).getText();
								if (level.equals("Loading...")) {
									System.out.println("wait level");
									return false;
								} else {
									return true;
								}
							}
						});
					} catch (TimeoutException e) {
						System.out.println("No level time out");
						continue;
					}
					if (this.isMarketActive(number) == true) {
						break;
					} else {
						marketDropDown.click();
					}
				} catch (UnhandledAlertException e) {
					Alert alert = driver.switchTo().alert();
					alert.accept();
				}
			}
		}
	}
	/**
	 * Changes market until set active market
	 */
	public void chooseActiveMarket() {
		String id = "tradeBox_marketName_0";
		WebElement marketDropDown = driver.findElement(By.id(id));
		marketDropDown.click();
		String className = "menu_market_element";
		List<WebElement> options = driver.findElements(By.className(className));
		for (WebElement opt : options) {
			String attrName = opt.getAttribute("onclick");
			String attrStyle = opt.getAttribute("style");
			if (attrName != null && attrStyle.contains("block")) {
				opt.click();
				WebDriverWait wait = new WebDriverWait(driver, 20);
				wait.until(new ExpectedCondition<Boolean>() {
					@Override
					public Boolean apply(WebDriver driver) {
						String id = "tradeBox_level_0";
						String level = driver.findElement(By.id(id)).getText();
						if (level.equals("Loading...")) {
							System.out.println("wait level");
							return false;
						} else {
							return true;
						}
					}
				});
				if (this.isMarketActive(0) == true) {
					break;
				} else {
					marketDropDown.click();
				}
			}
		}
	}
	/**
	 * Gets current markets in binary options 
	 * @return ArrayList<String>
	 */
	public ArrayList<String> getCurrentMarkets() {
		ArrayList<String> markets = new ArrayList<>();
		for (int i = 0; i < 4; i++) {
			String id = "tradeBox_marketName_" + i;
			String market = driver.findElement(By.id(id)).getText();
			markets.add(market);
		}
		return markets;
	}
	/**
	 * Gets the profit from drop down
	 * @param number
	 * @return
	 */
	public String getTheProfit(int number) {
		String id = "tradeBox_profit_procent_" + number;
		WebElement element = driver.findElement(By.id(id));
		return element.getText();
	}
	/**
	 * Clicks Up button
	 * @param number
	 */
	public void clickUpButton(int number) {
		String id = "tradeBox_call_btn_" + number;
		final WebElement button = driver.findElement(By.id(id));
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				String className = button.getAttribute("class");
				if (className.contains("active")) {
					return true;
				} else {
					return false;
				}	
			}
		});
		try {
			button.click();
		} catch (WebDriverException e) {
			System.out.println("Light streemer refresh");
			String idHolder = "tradeBox_pop_holder_" + number;
			wait.until(ExpectedConditions.attributeToBe(By.id(idHolder), "class", "tradeBox_pop_holder hidden"));
			button.click();
		}
	}
	/**
	 * Clicks UP button until investment is successfully
	 * @param number
	 */
	public void makeBinaryOptionInvestWithRecursion(int number) {
		String id = "tradeBox_call_btn_" + number;
		final WebElement button = driver.findElement(By.id(id));
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				String className = button.getAttribute("class");
				if (className.contains("active")) {
					return true;
				} else {
					return false;
				}	
			}
		});
		String holderId = "tradeBox_pop_holder_" + number;
		wait.until(ExpectedConditions.attributeToBe(By.id(holderId), "class", "tradeBox_pop_holder hidden"));
		button.click();
		String errorId = "tradeBox_error_footer_" + number;
		String errorAttribute = driver.findElement(By.id(errorId)).getAttribute("style");
		if (errorAttribute.equals("display: block;")) {
			wait.until(ExpectedConditions.attributeToBe(By.id(errorId), "style", "display: none;"));
			this.makeBinaryOptionInvestWithRecursion(number);
		} else {
			try {
				String afterInvestId = "tradeBox_after_invest_" + number;
				wait.until(ExpectedConditions.attributeToBe(By.id(afterInvestId), "style", "display: block;"));
			} catch (TimeoutException e) {
				this.makeBinaryOptionInvestWithRecursion(number);
			}
		}	
	}
	/**
	 * Makes investment until pop up for cancel investment is displayed
	 * @param number
	 */
	public void makeInvestmentToCancelWithRecursion(int number) {
		String id = "tradeBox_call_btn_" + number;
		final WebElement button = driver.findElement(By.id(id));
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				String className = button.getAttribute("class");
				if (className.contains("active")) {
					return true;
				} else {
					return false;
				}	
			}
		});
		String holderId = "tradeBox_pop_holder_" + number;
		wait.until(ExpectedConditions.attributeToBe(By.id(holderId), "class", "tradeBox_pop_holder hidden"));
		button.click();
		String errorId = "tradeBox_error_footer_" + number;
		String errorAttribute = driver.findElement(By.id(errorId)).getAttribute("style");
		if (errorAttribute.equals("display: block;")) {
			wait.until(ExpectedConditions.attributeToBe(By.id(errorId), "style", "display: none;"));
			this.makeInvestmentToCancelWithRecursion(number);
		} else {
			try {
				String cancelInvestId = "chart-overlays_" + number;
				wait.until(ExpectedConditions.attributeToBe(By.id(cancelInvestId), "class", "chart-overlays cancel-inv"));
			} catch (TimeoutException e) {
				this.makeInvestmentToCancelWithRecursion(number);
			}
		}	
	}
	/**
	 * Clicks Down button
	 * @param number
	 */
	public void clickDownButton(int number) {
		String id = "tradeBox_put_btn_" + number;
		driver.findElement(By.id(id)).click();
	}
	/**
	 * Is after invest pop up enable
	 * @param number
	 * @return boolean
	 */
	public boolean afterInvest(int number, boolean set) {
		String id = "tradeBox_after_invest_" + number;
		WebDriverWait wait = new WebDriverWait(driver, 90);
		for (int i = 0; i < 5; i++) {
			try {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
				break;
			} catch (TimeoutException e) {
				UtilsMethods.sleep(2500);
				if (set == true) {
					this.clickProfitDropDown(number);
					this.chooseProfit(number, 0);
				}	
				this.clickUpButton(number);
			}
		}
		WebElement after = driver.findElement(By.id(id));
		if (after.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Is after invest pop up enable
	 * @return boolean
	 */
	public boolean afterInvest(int number) {
		boolean result = false;
		String id = "tradeBox_after_invest_" + number;
		WebDriverWait wait = new WebDriverWait(driver, 40);
		for (int i = 0; i < 5; i++) {
			try {
				wait.until(ExpectedConditions.attributeToBe(By.id(id), "style", "display: block;"));
				result = true;
				break;
			} catch (TimeoutException e) {
				this.clickUpButton(number);
			}
		}
		return result;
	}
	/**
	 * Gets return value if is correct
	 * @param number
	 * @return Integer
	 */
	public int getReturnValueIfCorrect(int number) {
		String id = "tradeBox_profit_amount_" + number;
		String amount = driver.findElement(By.id(id)).getText();
		int value;
		amount = amount.replaceAll("[^\\d]", "");
		value = Integer.parseInt(amount);
		return value;
	}
	/**
	 * Gets return value if is incorrect
	 * @param number
	 * @return Integer
	 */
	public int getReturnValueIfIncorrect(int number) {
		String id = "tradeBox_incorrect_amount_" + number;
		String amount = driver.findElement(By.id(id)).getText();
		int value;
		amount = amount.replaceAll("[^\\d]", "");
		value = Integer.parseInt(amount);
		return value;
	}
	/**
	 * Gets commission message from option plus investment
	 * @return
	 */
	public String getCommissionMessage() {
		return driver.findElement(By.id("tradeBox_slipCommision_0")).getText();
	}
	/**
	 * Opens profit line window
	 * @param number
	 */
	public void openProfitLine(int number) {
		String id = "tradeBox_profitLine_" + number;
		driver.findElement(By.id(id)).click();
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#profitLine_h > div.clear.profitLine_header")));
	}
	/**
	 * Gets amount from after invest pop up
	 * @param number
	 * @return String
	 */
	public String getAmountFormAfterInvestPopUp(int number) {
		String id = "tradeBox_AI_amount_" + number;
		String amount = driver.findElement(By.id(id)).getText();
		return amount;
	}
	/**
	 * Gets return value from after invest pop up if investment is correct
	 * @param number
	 * @return Integer
	 */
	public int getCorrectAmountFromAfterInvestPopUp(int number) {
		final String id = "tradeBox_AI_correct_" + number;
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				WebElement textElement = driver.findElement(By.id(id));
				String text = textElement.getText().replaceAll("[^\\d]", "");
				if (text.equals("")) {
					return false;
				} else {
					return true;
				}
			}
		});
		String amount = driver.findElement(By.id(id)).getText().replaceAll("[^\\d]", "");
		return Integer.parseInt(amount);
	}
	/**
	 * Gets return value from after invest pop up if investment is incorrect
	 * @param number
	 * @return Integer
	 */
	public int getIncorrectAmountFromAfterInvestPopUp(int number) {
		String id = "tradeBox_AI_incorrect_" + number;
		String amount = driver.findElement(By.id(id)).getText().replaceAll("[^\\d]", "");
		return Integer.parseInt(amount);
	}
	/**
	 * Gets balance from profit line window
	 * @param profiteLineIndex - 1 profit line, 4 - after binary option investment
	 * @return String
	 */
	public String getBalanceFromProfitLineWindow(int profiteLineIndex) {
		final String id = "profitLine_balance_val_" + profiteLineIndex;
		WebDriverWait wait = new WebDriverWait(driver, 8);
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver input) {
				String text = driver.findElement(By.id(id)).getText();
				if (text.equals("")) {
					return false;
				} else {
					return true;
				}
			}
		});
		String balance = driver.findElement(By.id(id)).getText();
		return balance;
	}
	/**
	 * Gets balance from profit line window
	 * @param profiteLineIndex - 1 profit line, 4 - after binary option investment
	 * @return String
	 */
	public String getBalanceFromProfitLineWindow(int profiteLineIndex, final String balanceBefore) {
		final String id = "profitLine_balance_val_" + profiteLineIndex;
		WebDriverWait wait = new WebDriverWait(driver, 8);
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver input) {
				String text = driver.findElement(By.id(id)).getText();
				if (text.equals(balanceBefore)) {
					return false;
				} else {
					return true;
				}
			}
		});
		String balance = driver.findElement(By.id(id)).getText();
		return balance;
	}
	/**
	 * Closes profit line window
	 */
	public void closeProfitLineWindow(String operSys) {
		if (operSys.equals("Linux")) {
			try {
				profitLineWindowCloseButton.click();
			} catch (WebDriverException e) {
				JavascriptExecutor jse = (JavascriptExecutor)driver;
				jse.executeScript("arguments[0].click();", profitLineWindowCloseButton);
			}
		} else {
			profitLineWindowCloseButton.click();
		}
	}
	/**
	 * Gets total investment amount
	 * @param profiteLineIndex - 1 - open from home page, 4 - binary options 
	 * @return String
	 */
	public String getTotalInvestmentFromProfitLine(int profiteLineIndex) {
		String id = "profitLine_total_inv_val_" + profiteLineIndex;
		String total = null;
		for (int i = 0; i < 5; i++) {
			total = driver.findElement(By.id(id)).getText();
			int intTotal = Integer.parseInt(total.replaceAll("[^\\d]", ""));
			if (intTotal != 0) {
				break;
			} else {
				UtilsMethods.sleep(2000);
				System.out.println("no ready");
			}
		}	
		return total;
	}
	/**
	 * Gets current return value
	 * @return
	 */
	public int getCurrentReturnValue() {
		String label = returnValueLabel.getText().replaceAll("[^\\d]", "");
		return Integer.parseInt(label);
	}
	/**
	 * Returns is footer error message displayed
	 * @param number
	 * @return boolean
	 */
	public boolean isErrorMessageDisplayed(int number, String type) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		boolean next = false;
		final String id = "tradeBox_error_footer_" + number;
		final WebElement element = driver.findElement(By.id(id));
		try {
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver driver) {
					String attr = element.getAttribute("style");
					if (attr.contains("block")) {
						return true;
					} else {
						System.out.println("wait");
						return false;
					}
				}
			});
		} catch (TimeoutException e) {
			System.out.println("Next try");
			next = true;
			if (type.equals("UP")) {
				this.clickUpButton(number);
			}
			if (type.equals("DOWN")) {
				this.clickDownButton(number);
			}
		}
		if (next == true) {
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver driver) {
					String attr = element.getAttribute("style");
					if (attr.contains("block")) {
						return true;
					} else {
						System.out.println("wait");
						return false;
					}
				}
			});
		}
		WebElement message = driver.findElement(By.id(id));
		String style = message.getAttribute("style");
		if (style.equals("display: block;")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Gets footer error message text
	 * @param number
	 * @return
	 */
	public String getErrorMessage(int number) {
		String id = "tradeBox_error_footer_txt_" + number;
		String message = driver.findElement(By.id(id)).getText();
		return message;
	}
	/**
	 * Closes footer message
	 * @param number
	 */
	public void closeFooterMessage(final int number) {
		String id = "tradeBox_error_footer_close_" + number;
		driver.findElement(By.id(id)).click();
		WebDriverWait wait = new WebDriverWait(driver, 15);
		String idMessage = "tradeBox_error_footer_" + number;
		final WebElement element = driver.findElement(By.id(idMessage));
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				String attr = element.getAttribute("style");
				if (attr.contains("block")) {
					System.out.println("wait");
					return false;
				} else {
					return true;
				}	
			}
		});
	}
}
