package com.anyoption.autotest.pages;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.anyoption.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class MessagesPage {
	private WebDriver driver;
	private String browserType;
	@FindBys({@FindBy(css = "#emailsTable > tbody > tr > td:nth-child(4)")})
	private List<WebElement> mailSubject;
	@FindBy(css = "body > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td > span > strong")
	private WebElement welcomeMessage;
	@FindBy(css = "body > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td > strong:nth-child(7)")
	private WebElement recordedDetailsText;
	@FindBy(css = "body > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td > strong:nth-child(10)")
	private WebElement userNameFromMail;
	@FindBy(css = "body > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td > strong:nth-child(12)")
	private WebElement passFromMail;
	@FindBy(css = "body > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td > strong:nth-child(14)")
	private WebElement emailFromMail;
	@FindBy(css = "body > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td > strong:nth-child(16)")
	private WebElement phoneFromMail;
	@FindBy(css = "#readEmailForm > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(2) > table > tbody "
			+ "> tr:nth-child(6) > td:nth-child(2) > span")
	private WebElement subjectInTradeMail;
	/**
	 * 
	 * @param driver
	 * @param browserType
	 */
	public MessagesPage(WebDriver driver, String browserType) {
		this.driver = driver;
		this.browserType = browserType;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Gets subject from trade mail
	 * @return
	 */
	public String getSubjectFromTradeMail() {
		return subjectInTradeMail.getText();
	}
	/**
	 * Gets phone from welcome email
	 * @return
	 */
	public String getPhoneFromMail() {
		return phoneFromMail.getText();
	}
	/**
	 * Gets email from welcome email
	 * @return
	 */
	public String getEmailFromWelcomeMail() {
		return emailFromMail.getText();
	}
	/**
	 * Gets password from welcome mail
	 * @return
	 */
	public String getPassFromMail() {
		return passFromMail.getText();
	}
	/**
	 * Gets username from welcome email
	 * @return
	 */
	public String getUsernameFromMail() {
		return userNameFromMail.getText();
	}
	/**
	 * Gets recorded details text
	 * @return
	 */
	public String getRecordedDetailsText() {
		return recordedDetailsText.getText();
	}
	/**
	 * Gets welcome message
	 * @return
	 */
	public String getWelcomeMessage() {
		return welcomeMessage.getText();
	}
	/**
	 * Switch webdriver
	 */
	public void switchDriver() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("mailBox"));
	}
	/**
	 * Opens email
	 */
	public void openEmail(int index) {
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			UtilsMethods.sleep(1000);
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click();", mailSubject.get(index));
		} else {
			mailSubject.get(index).click();
		}	
	}
	/**
	 * Gets email subject
	 * @return
	 */
	public String getMailSubject(int index) {
		return mailSubject.get(index).getText();
	}
}
