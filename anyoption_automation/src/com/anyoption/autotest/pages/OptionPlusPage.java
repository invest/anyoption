package com.anyoption.autotest.pages;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.anyoption.autotest.common.User;
import com.anyoption.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimirm
 *
 */
public class OptionPlusPage {
	private WebDriver driver;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div.page_bgr_trade > ul > "
			+ "li.trade_page_tabs_h.trade_page_tabs_option_plus_h > a > span > span")
	private WebElement optionPlusMenu;
	@FindBy(id = "profitLine_total_inv_val_0")
	private WebElement totalInvestmentLabel;
	@FindBy(id = "profitLine_balance_val_0")
	private WebElement profitBalanceLabel;
	@FindBy(id = "tradeBox_call_btn_0")
	private WebElement callUpButton;
	@FindBy(css = "#profitLine_marketDrop_title_0 > i")
	private WebElement marketsDropDown;
	@FindBys({@FindBy(css = "#profitLine_marketDrop_ul_0 > li")})
	private List<WebElement> tradesList;
	@FindBy(id = "tradeBox_inv_amount_0")
	private WebElement investAmountDropDown;
	@FindBys({@FindBy(css = "#tradeBox_invest_drop_ul_0 > li")})
	private List<WebElement> investAmountDropDownList;
	@FindBy(id = "tradeBox_invest_drop_custom_0")
	private WebElement customAmountInput;
	@FindBy(id = "tradeBox_profit_dsp_0")
	private WebElement profitLabel;
	@FindBy(id = "tradeBox_profit_amount_0")
	private WebElement returnIfCorrectLabel;
	@FindBy(id = "tradeBox_incorrect_amount_0")
	private WebElement returnIfIncorrectLabel;
	@FindBy(id = "tradeBox_time_invest_d_0")
	private WebElement timeToInvestElement;
	@FindBy(id = "profitLine_marketDrop_title_txt_0")
	private WebElement marketDropDownTitle;
	@FindBy(id = "tradeBox_chartHeader_0")
	private WebElement tradeBoxTimeElement;
	@FindBy(id = "tradeBox_offer_sell_amount_0")
	private WebElement offerSellAmount;
	@FindBy(id = "tradeBox_offer_0")
	private WebElement sellOfferPopUp;
	@FindBy(css = "#tradeBox_offer_0 > div.tradeBox_offer_expired > div.tradeBox_offer_sell_bottom > span.tradeBox_offer_sell_txt1")
	private WebElement expiredOfferText1;
	@FindBy(id = "tradeBox_offer_sell_btn_qetNewQuot_0")
	private WebElement getNewQuoteButton;
	@FindBy(id = "tradeBox_offer_sell_btn_sell_0")
	private WebElement sellButton;
	@FindBy(css = "#tradeBox_offer_0 > div.tradeBox_offer_sold > div.tradeBox_offer_sold_timer")
	private WebElement soldOfferMessage;
	@FindBy(css = "#tradeBox_offer_0 > div.tradeBox_offer_sold > div.tradeBox_offer_sold_bottom > span.tradeBox_offer_sell_txt1")
	private WebElement soldOfferAmountMessage;
	@FindBy(id = "tradeBox_offer_sold_amount_0")
	private WebElement soldOfferAmount;
	/**
	 * 
	 * @param driver
	 */
	public OptionPlusPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Is sell pop up displayed
	 * @return boolean
	 */
	public boolean isSellPopUpDisplayed() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		try {
			wait.until(ExpectedConditions.attributeToBe(sellOfferPopUp, "class", "tradeBox_offer sell"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Is error pop up displayed
	 * @return
	 */
	public boolean isErrorPopUpDisplayed() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		try {
			wait.until(ExpectedConditions.attributeToBe(sellOfferPopUp, "class", "tradeBox_offer error"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Gets sold offer amount
	 * @return String
	 */
	public String getSoldOfferAmount() {
		return soldOfferAmount.getText();
	}
	/**
	 * Gets sold offer amount message
	 * @return String
	 */
	public String getSoldOfferAmountMessage() {
		return soldOfferAmountMessage.getText();
	}
	/**
	 * Is offer sold
	 * @return boolean
	 */
	public boolean isOfferSold() {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		try {
			wait.until(ExpectedConditions.attributeToBe(sellOfferPopUp, "class", "tradeBox_offer sold"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Gets sold offer message
	 * @return String
	 */
	public String getSoldOfferMessage() {
		return soldOfferMessage.getText();
	}
	/**
	 * Sell option from sell pop up
	 */
	public void sellOption() {
		sellButton.click();
	}
	/**
	 * Gets new quote
	 */
	public void getNewQuote() {
		getNewQuoteButton.click();
	}
	/**
	 * Gets expired offer second message
	 * @param skin
	 * @return String
	 */
	public String getExpiredOfferText(String skin) {
		String css = "#tradeBox_offer_0 > div.tradeBox_offer_expired > div.tradeBox_offer_sell_bottom "
				+ "> span.tradeBox_offer_expired_amount.tradeBox_offer_expired_amount_" + skin.toLowerCase();
		if (skin.equals("SE")) {
			css = "#tradeBox_offer_0 > div.tradeBox_offer_expired > div.tradeBox_offer_sell_bottom "
					+ "> span.tradeBox_offer_expired_amount.tradeBox_offer_expired_amount_sv";
		}
		return driver.findElement(By.cssSelector(css)).getText();
	}
	/**
	 * Gets expired offer first text
	 * @return String
	 */
	public String getExpiredOfferText() {
		return expiredOfferText1.getText();
	}
	/**
	 * Waits until offer expire
	 */
	public void waitOfferToExpire() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.attributeToBe(sellOfferPopUp, "class", "tradeBox_offer expired"));
	}
	/**
	 * Gets sell amount
	 * @return String
	 */
	public String getSellAmount() {
		return offerSellAmount.getText();
	}
	/**
	 * Gets offer message
	 * @param skin
	 * @return String
	 */
	public String getOfferMessage(String skin) {
		String css = "#tradeBox_offer_0 > div.tradeBox_offer_sell > div.tradeBox_offer_sell_bottom "
				+ "> span.tradeBox_offer_sell_txt1.tradeBox_offer_sell_txt1_" + skin.toLowerCase();
		if (skin.equals("SE")) {
			css = "#tradeBox_offer_0 > div.tradeBox_offer_sell > div.tradeBox_offer_sell_bottom "
					+ "> span.tradeBox_offer_sell_txt1.tradeBox_offer_sell_txt1_sv";
		}
		String text = driver.findElement(By.cssSelector(css)).getText();
		return text;
	}
	/**
	 * Clicks sell button
	 * @param skin
	 */
	public void clickSellButton(String skin) {
		String css = "#tradeBox_profit_invs_0 > li > span.tradeBox_profit_invs_c6.vaM.cP." + skin.toLowerCase();
		if (skin.equals("SE")) {
			css = "#tradeBox_profit_invs_0 > li > span.tradeBox_profit_invs_c6.vaM.cP.sv";
		}
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(css)));
		WebElement button = driver.findElement(By.cssSelector(css));
		try {
			button.click();
		} catch (WebDriverException e) {
			System.out.println("Light streemer refrsh");
			wait.until(ExpectedConditions.attributeToBe(By.id("tradeBox_pop_holder_0"), "class", "tradeBox_pop_holder hidden"));
			button.click();
		}
	}
	/**
	 * Gets value to return if incorrect
	 * @return
	 */
	public String getReturnValueIfIncorrect() {
		return returnIfIncorrectLabel.getText();
	}
	/**
	 * Gets amount for invest
	 * @return
	 */
	public String getAmountForInvest() {
		return investAmountDropDown.getText();
	}
	/**
	 * Gets value to return if correct
	 * @return
	 */
	public String getReturnValueIfCorrect() {
		String value = returnIfCorrectLabel.getText();
		return value;
	}
	/**
	 * Gets profit percents
	 * @return
	 */
	public String getProfitPercents() {
		return profitLabel.getText();
	}
	/**
	 * Chooses amount to invest
	 * @param index
	 */
	public void chooseAmountToInvest(int index) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		String css = "i#tradeBox_call_btn_" + index + ".tradeBox_img.tradeBox_call_btn.active";
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(css)));
		try {
			investAmountDropDown.click();
		} catch (WebDriverException e) {
			System.out.println("Light streemer refresh");
			String idHolder = "tradeBox_pop_holder_0";
			wait.until(ExpectedConditions.attributeToBe(By.id(idHolder), "class", "tradeBox_pop_holder hidden"));
			investAmountDropDown.click();	
		}
		investAmountDropDownList.get(index).click();
	}
	/**
	 * Sets custom amount for investment
	 * @param amount
	 */
	public void setCustomAmountToInvest(String amount) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		String css = "i#tradeBox_call_btn_0.tradeBox_img.tradeBox_call_btn.active";
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(css)));
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(css)));
		wait.until(ExpectedConditions.attributeContains(By.id("tradeBox_pop_holder_0"), "class", "tradeBox_pop_holder hidden"));
		investAmountDropDown.click();
		customAmountInput.sendKeys(amount);
	}
	/**
	 * Gets balance from option plus menu
	 * @return
	 */
	public String getProfitBalance() {
		String balance = profitBalanceLabel.getText();
		return balance;
	}
	/**
	 * Gets total investment
	 * @return
	 */
	public int getTotalInvestment() {
		String total = totalInvestmentLabel.getText().replaceAll("[^\\d]", "");
		return Integer.parseInt(total);
	}
	/**
	 * Wait total investments to be not 0
	 * @param currency
	 */
	public void waitTotalInvestments(final String currency) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		try {
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver input) {
					if (totalInvestmentLabel.getText().equals(currency + "0.00")) {
						System.out.println("wait total investments");
						return false;
					} else {
						return true;
					}
				}
			});
		} catch (TimeoutException e) {
			driver.navigate().refresh();
			System.out.println("Refresh screen");
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver input) {
					if (totalInvestmentLabel.getText().equals(currency + "0.00")) {
						System.out.println("wait total investments");
						return false;
					} else {
						return true;
					}
				}
			});
		}
	}
	/**
	 * Opens option plus menu
	 */
	public void openOptionPlusMenu(String brType) {
		if (brType.equalsIgnoreCase("internetexplorer") || brType.equalsIgnoreCase("firefox")) {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click();", optionPlusMenu);
		} else {
			try {
				optionPlusMenu.click();
			} catch (WebDriverException e) {
				JavascriptExecutor jse = (JavascriptExecutor)driver;
				jse.executeScript("arguments[0].click();", optionPlusMenu);
			}
		}	
		WebDriverWait wait = new WebDriverWait(driver, 15);
		String waitCss = "#markets_table > div > div.clear.optionPlus_header > i.optionPlus_title.fL";
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(waitCss)));
		} catch (TimeoutException e) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("off_hours_banner")));
		}
	}
	/**
	 * Clicks Up button 
	 * @param index
	 */
	public void clickUpButton(int index, String brType) {
		if (brType.equals("phantomJs")) {
			System.out.println("PhantomJs wait");
			UtilsMethods.sleep(5000);
		}
		WebDriverWait wait = new WebDriverWait(driver, 15);
		String waitCss = "#markets_table > div > div.clear.optionPlus_header > i.optionPlus_title.fL";
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(waitCss)));
		String id = "tradeBox_call_btn_text_" + index;
		try {
			String css = "i#tradeBox_call_btn_" + index + ".tradeBox_img.tradeBox_call_btn.active";
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(css)));
			List<WebElement> buttons = driver.findElements(By.id(id));
			int size = buttons.size();
			System.out.println(size);
			buttons.get(size - 1).click();
		} catch (WebDriverException e) {
			System.out.println("exception is catched");
			UtilsMethods.sleep(15000);
			String css = "i#tradeBox_call_btn_" + index + ".tradeBox_img.tradeBox_call_btn.active";
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(css)));
			driver.findElement(By.cssSelector(css)).click();
		}
	}
	/**
	 * Clicks Up button
	 * @return String - current market name
	 */
	public String clickUpButton() {
		String asset = this.getCurrentAsset();
		for (int i = 0; i < 10; i++) {
			String id = "tradeBox_after_invest_0";
			WebDriverWait wait = new WebDriverWait(driver, 30); 
			wait.until(ExpectedConditions.attributeContains(By.id("tradeBox_pop_holder_0"), "class", "hidden"));
			callUpButton.click();
			try {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
				break;
			} catch (TimeoutException e) {
				if (i == 5) {
					setActiveMarket();
					asset = this.getCurrentAsset();
				}
				System.out.println("Error investment");
			}
		}	
		return asset;
	}
	/**
	 * Chooses active market
	 */
	public void setActiveMarket() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		String waitCss = "#markets_table > div > div.clear.optionPlus_header > i.optionPlus_title.fL";
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(waitCss)));
		
		WebElement popHolder = driver.findElement(By.id("tradeBox_pop_holder_0"));
		
		int marketsSize = 0;
		
		try {
			marketsDropDown.click();
			List<WebElement> markets = driver.findElements(By.cssSelector("#profitLine_marketDrop_ul_0 > li"));
			marketsDropDown.click();
			marketsSize = markets.size() - 1;
			final String tradeBoxLevelId = "tradeBox_level_0";
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver driver) {
					String levelText = driver.findElement(By.id(tradeBoxLevelId)).getTagName();
					if (levelText.contains("Loading")) {
						System.out.println("wait level");
						return false;
					} else {
						return true;
					}
				}
			});
		} catch (ElementNotVisibleException e) {
			marketsSize = 5;
		}
		for (int i = 0; i < marketsSize; i++) {
			UtilsMethods.sleep(2000);
			int remainingTime = this.getRemainingTimeToInvest();
			String notAv = popHolder.getAttribute("class");
			if (! callUpButton.getAttribute("class").contains("off") && remainingTime >= 120 && ! notAv.contains("notAv") && ! notAv.contains("loading")) {
				break;
			} else {
				marketsDropDown.click();
				int size = tradesList.size();
				try {
					tradesList.get(size - 1 - i).click();
				} catch (WebDriverException e) {
					tradesList.get(i).click();
				}
			}
		}
	}
	/**
	 * Changes market
	 * @param index
	 */
	public void changeMarket(int index) {
		marketsDropDown.click();
		List<WebElement> markets = driver.findElements(By.cssSelector("#profitLine_marketDrop_ul_0 > li"));
		markets.get(index + 1).click();
		marketsDropDown.click();
	}
	/**
	 * Checks is market active
	 * @return boolean
	 */
	public boolean isMarketActive() {
		boolean result = false;
		WebElement popHolder = driver.findElement(By.id("tradeBox_pop_holder_0"));
		int remainingTime = this.getRemainingTimeToInvest();
		String notAv = popHolder.getAttribute("class");
		if (! callUpButton.getAttribute("class").contains("off") && remainingTime >= 120 && ! notAv.contains("notAv") && ! notAv.contains("loading")) {
			result = true;
		}
		return result;
	}
	/**
	 * Gets current market id
	 * @return String
	 */
	public String getMarketId() {
		String id = "";
		marketsDropDown.click();
		int size = tradesList.size();
		for (int i = 1; i < size; i++) {
			String attribute;
			try {
				attribute = tradesList.get(i).getAttribute("class");
			} catch (StaleElementReferenceException e) {
				UtilsMethods.sleep(200);
				attribute = tradesList.get(i).getAttribute("class");
			}
			
			if (attribute.contains("active")) {
				try {
					id = tradesList.get(i).getAttribute("data-marketid");
				} catch (StaleElementReferenceException e) {
					UtilsMethods.sleep(200);
					id = tradesList.get(i).getAttribute("data-marketid");
				}
				break;
			}
		}
		marketsDropDown.click();
		return id;
	}
	/**
	 * Gets data for deviation 2 check
	 * @return HasMap
	 * @throws Exception
	 */
	public HashMap<String, String> getOptionPlusDeviation2Data() throws Exception {
		String marketId = this.getMarketId();
		HashMap<String, String> data = new HashMap<>();
		data = User.getDev2Data(marketId);
		if (! data.isEmpty()) {
			if (data.get("amount").contains(".")) {
				String am = data.get("amount").replace(".", "");
				int amInt = Integer.parseInt(am);
				amInt = amInt / 100 + 1;
				String amNow = String.valueOf(amInt);
				data.put("amount", amNow);
			}
		}
		return data;
	}
	/**
	 * Gets remaining time for investment
	 * @return
	 */
	public int getRemainingTimeToInvest() {
		int seconds = 120;
		String time = timeToInvestElement.getText();
		String tradeBoxClassName = tradeBoxTimeElement.getAttribute("class");
		if (! time.equals("") && tradeBoxClassName.contains("time_invest")) {
			String times[] = time.split(":");
			int minutes = Integer.parseInt(times[0]);
			seconds = 60 * minutes + Integer.parseInt(times[1]);
		} else if (tradeBoxClassName.contains("waiting_for_exp")) {
			seconds = 0;
		}
		return seconds;
	}
	/**
	 * Gets current asset
	 * @return
	 */
	public String getCurrentAsset() {
		String title[] = marketDropDownTitle.getText().split("-");
		String result = title[0].substring(0, title[0].length() - 1);
		return result;
	}
}
