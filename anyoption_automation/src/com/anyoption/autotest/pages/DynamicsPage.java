package com.anyoption.autotest.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.anyoption.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class DynamicsPage {
	WebDriver driver;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div.page_bgr_trade > ul > li.trade_page_tabs_h.trade_page_tabs_binary0100_h > a")
	private WebElement dynamicsTab;
	@FindBy(css = "#dynamics-chart-0 > div.chart-container > div.chart-elements > button.invest-btn.call")
	private WebElement upButton;
	@FindBy(css = "#dynamics-chart-0 > div.chart-container > div.chart-elements > button.invest-btn.put")
	private WebElement downButton;
	@FindBy(css = "#dynamics-chart-0 > div.chart-container > div.popups-holder.table.showTbl > div > div.popup-container.showIb > button.btn-style-2.v1.mt20.ng-binding")
	private WebElement openAccButton;
	@FindBy(css = "#dynamics-chart-0 > div.dyn-walkthrough")
	private WebElement dynamicsTutorial;
	@FindBy(css = "#dynamics-chart-0 > div.dyn-walkthrough > div.main_el > i.cool-close")
	private WebElement tutorialCloseButton;
	@FindBy(css = "#dynamics-chart-0 > div.chart-container > div.popups-holder.table.showTbl > div > div.popup-container.showIb > button.btn-style-2.v2.mt10.ng-binding")
	private WebElement dynamicsLoginButton;
	@FindBy(css = "#dynamics-chart-0 > div.header.table > div.cell.market-info > div > div.market.ng-binding")
	private WebElement currentMarketLabel;
	@FindBy(css = "#dynamics-chart-0 > div:nth-child(2) > div.cell.taR.separator.space-right.ng-scope > div > span.big-font.ng-binding")
	private WebElement aboveReturnIfCorrect;
	@FindBy(css = "#dynamics-chart-0 > div:nth-child(2) > div.cell.taR.cell-side.ng-binding.ng-scope")
	private WebElement aboveReturnIfIncorrect;
	@FindBy(css = "#dynamics-chart-0 > div.header.table > div:nth-child(1) > div > span")
	private WebElement investmentAmountDropDown;
	@FindBy(css = "#dynamics-chart-0 > div.header.table > div:nth-child(1) > div > ul > li:nth-child(5) > input")
	private WebElement investmentOtherAmount;
	@FindBy(css = "#dynamics-chart-0 > div.chart-container > div.chart-elements > div.open-options.ng-binding")
	private WebElement openedOptionslabelInChart;
	@FindBy(css = "#dynamics-chart-0 > div.chart-container > div")
	private WebElement afterInvestmentChart;
	@FindBy(css = "#dynamics-chart-0 > div:nth-child(2) > div.cell.space-left.ng-scope > div > span")
	private WebElement aboveProfit;
	@FindBy(css = "#dynamics-chart-0 > div.chart-container.with-investment > div.chart-overlays.table.option-purchased > div.option-slip > div.row.top > div > div.market.ng-binding")
	private WebElement purchasedOptionMarket;
	@FindBy(css = "#dynamics-chart-0 > div.chart-container.with-investment > div.chart-overlays.table.option-purchased > div.option-slip > div.row.bottom > div > div:nth-child(1) > div:nth-child(2)")
	private WebElement purchasedOptionAmount;
	@FindBy(css = "#dynamics-chart-0 > div.chart-container.with-investment > div.chart-overlays.table.option-purchased > div.option-slip > div.row.bottom > div > div:nth-child(2) > div:nth-child(2)")
	private WebElement purchasedOptionProfit;
	@FindBy(css = "#dynamics-chart-0 > div.chart-container.with-investment > div.chart-overlays.table.option-purchased > div.option-slip > div.row.bottom > div > div:nth-child(3) > div:nth-child(2)")
	private WebElement purchasedOptionCorrectAmount;
	@FindBy(css = "#dynamics-chart-0 > div.chart-container.with-investment > div.chart-overlays.table.option-purchased > div.option-slip > div.row.bottom > div > div:nth-child(4) > div:nth-child(2)")
	private WebElement purchasedOptionIncorrectAmount;
	@FindBy(css = "#DynamicsOpenMarkets > table > tbody > tr:nth-child(3) > td:nth-child(1) > div")
	private WebElement aboveOpenOptionCount;
	@FindBy(css = "#DynamicsOpenMarkets > table > tbody > tr:nth-child(3) > td:nth-child(2)")
	private WebElement aboveOpenOptionAmount;
	@FindBy(css = "#DynamicsOpenMarkets > table > tbody > tr:nth-child(3) > td:nth-child(3)")
	private WebElement aboveOpenOptionReturnAmount;
	@FindBy(css = "#dynamics-chart-0 > div.chart-container.with-investment > div.chart-overlays.table.option-purchased > div.option-slip > div.row.top > div > div.level > span")
	private WebElement purchasedOptionLevel;
	@FindBy(xpath = "//*[@id='dynamics-chart-0']/div[3]/div[4]/div[2]/div[5]/span")
	private WebElement cancelInvLink;
	@FindBy(xpath = "//*[@id='dynamics-chart-0']/div[3]/div[4]/div[3]/div[2]")
	private WebElement cancelInvMessage;
	@FindBy(css = "#dynamics-chart-0 > div.chart-container > div.chart-overlays.table.cancel-inv > div.cancel-inv-slip > div.amount.ng-binding")
	private WebElement investmentAmountFromCancelWindow;
	@FindBy(css = "#dynamics-chart-0 > div.chart-container > div.chart-overlays.table.cancel-inv > div.cancel-inv-slip > div.text1.ng-binding")
	private WebElement cancelAmountMessage;
	@FindBy(css = "#dynamics-chart-0 > div.chart-container > div.chart-overlays.table.cancel-inv > div.cancel-inv-slip > div.level > span")
	private WebElement levelFromCancelWindow;
	@FindBy(css = "#dynamics-chart-0 > div.chart-container > div.chart-elements > div.default-error.ng-scope > div > span")
	private WebElement negativeNotification;
	@FindBy(css = "#dynamics-chart-0 > div.chart-footer.table > div:nth-child(4) > button")
	private WebElement closeInvestmentButton;
	@FindBy(css = "#dynamics-chart-0 > div.chart-footer.table > div:nth-child(4) > button > span > span")
	private WebElement closeAmountInButton;
	@FindBy(css = "#dynamics-chart-0 > div.header.table > div.cell.taR > span")
	private WebElement openDynamicsTutorial;
	@FindBy(css = "#dynamics-chart-0 > div.chart-container > div.chart-elements > div.level-container.table > div.level-box.cell > div")
	private WebElement dynamicsLevel;
	@FindBys({@FindBy(xpath = "//*[@id='DynamicsController']/div[1]/div[1]/div/ul/li/div[2]/div[2]")})
	private List<WebElement> marketsLevelTime;
	@FindBys({@FindBy(xpath = "//*[@id='DynamicsController']/div[1]/div[1]/div/ul/li/div[2]/div[3]/span")})
	private List<WebElement> marketsProfits;
	/**
	 * 
	 * @param driver
	 */
	public DynamicsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Gets dynamics level
	 * @return String
	 */
	public String getDynamicsLevel() {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		String id = "dynamics-chart-0";
		wait.until(ExpectedConditions.attributeToBe(By.id(id), "class", "main-chart open"));
		return dynamicsLevel.getText();
	}
	/**
	 * Clicks dynamics OK button
	 * @param index
	 */
	public void clickTutorialOkButton(int index) {
		String css = "#dynamics-chart-0 > div.dyn-walkthrough.step-" + index + " > div.main_el > button";
		driver.findElement(By.cssSelector(css)).click();
	}
	/**
	 * Gets dynamics tutorial message
	 * @param index
	 * @return String
	 */
	public String getTutorialMessage(int index) {
		String css = "#dynamics-chart-0 > div.dyn-walkthrough.step-" + index + " > div.main_el > div";
		String text = driver.findElement(By.cssSelector(css)).getText();
		return text;
	}
	/**
	 * Opens dynamics tutorial
	 */
	public void openTutorial() {
		openDynamicsTutorial.click();
	}
	/**
	 * Gets close investment amount
	 * @param currency
	 * @return String
	 */
	public String getCloseAmount(String currency) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver input) {
				String atr = closeAmountInButton.getAttribute("disabled");
				if (atr == null) {
					return true;
				} else {
					return false;
				}
			}
		});
		String amount = null;
		try {
			amount = closeAmountInButton.getText();
		} catch (StaleElementReferenceException e) {
			amount = closeAmountInButton.getText();
		}
		return amount.replace(currency, "");
	}
	/**
	 * Closes all investments
	 */
	public String closeAllInvestment(String currency) {
		closeInvestmentButton.click();
		UtilsMethods.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		try {
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver input) {
					String atr = "test";
					try {
						atr = closeInvestmentButton.getAttribute("disabled");
					} catch (StaleElementReferenceException e) {
						atr = null;
					}
					if (atr != null) {
						return true;
					} else {
						return false;
					}
				}
			});
		} catch (TimeoutException e) {
			closeInvestmentButton.click();
		}
		String amount = closeAmountInButton.getText();
		return amount.replace(currency, "");
	}
	/**
	 * Gets negative notification
	 * @param param - needed for wait method
	 * @return String
	 */
	public String getNegativeNotification(final String param) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver input) {
				String text;
				try {
					text = negativeNotification.getText();
				} catch (StaleElementReferenceException e) {
					return false;
				}
				if (! text.equals(param)) {
					return true;
				} else {
					return false;
				}
			}
		});
		return negativeNotification.getText();
	}
	/**
	 * Gets level from cancel window
	 * @return String
	 */
	public String getLevelFromCancelWindow() {
		String text = levelFromCancelWindow.getText();
		return text.replaceAll("[^\\d]", "");
	}
	/**
	 * Gets cancel investment link text
	 * @return String
	 */
	public String getCancelLinkText() {
		return cancelInvLink.getText();
	}
	/**
	 * Gets cancel amount message
	 * @return String
	 */
	public String getCancelInvAmountMessage() {
		return cancelAmountMessage.getText();
	}
	/**
	 * Gets investment amount from cancel window
	 * @param currency
	 * @return String
	 */
	public String getInvestmentAmount(String currency) {
		String amount = investmentAmountFromCancelWindow.getText();
		return amount.replace(currency, "");
	}
	/**
	 * Shows cancel investment pop up 
	 */
	public void showCancelInvPopUp() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		String xPath = "//*[@id='dynamics-chart-0']/div[3]/div[4]";
		wait.until(ExpectedConditions.attributeToBe(By.xpath(xPath), "class", "chart-overlays table"));
		String script = "document.querySelector('#dynamics-chart-0 > div.chart-container > div.chart-overlays.table').className = 'chart-overlays table cancel-inv'";
		((JavascriptExecutor) driver).executeScript(script);
	}
	/**
	 * Gets cancel investment message
	 * @return String
	 */
	public String getCancelInvMessage() {
		return cancelInvMessage.getText();
	}
	/**
	 * Is cancel inv message displayed
	 * @return boolean
	 */
	public boolean isCancelMessageDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 15);
		String waitXpath = "//*[@id='dynamics-chart-0']/div[3]/div[4]";
		try {
			wait.until(ExpectedConditions.attributeContains(By.xpath(waitXpath), "class", "cancel-inv-msg"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Is cancel investment pop up displayed
	 * @return boolean
	 */
	public boolean isCancelInvestmentDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 15);
		try {
			String waitCss = "#dynamics-chart-0 > div.chart-container.with-investment > div.chart-overlays.table.cancel-inv > div.cancel-inv-slip > div.taC > span";
			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(waitCss)));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Double clicks cancel investment link
	 */
	public void doubleClickCancelInvLink() {
		Actions action = new Actions(driver);
  		action.doubleClick(cancelInvLink).perform();
	}
	/**
	 * Cancels investment
	 */
	public void cancelInvestment() {
		try {
			cancelInvLink.click();
		} catch (ElementNotVisibleException e) {
			System.out.println("Cancel exception");
			UtilsMethods.clickWithJavaScript(driver, cancelInvLink);
		}
	}
	/**
	 * Gets purchased level
	 * @param skin
	 * @return String
	 */
	public String getPurchasedOptionLevel(String skin) {
		String text[] = purchasedOptionLevel.getText().split(" ");
		String level;
		if (skin.equals("ES")) {
			level = text[2];
		} else {
			level = text[1];
		}
		return level;
	}
	/**
	 * Gets above open option correct amount 
	 * @return String
	 */
	public String getAboveOpenOptionCorrect() {
		return aboveOpenOptionReturnAmount.getText();
	}
	/**
	 * Gets above open option amount
	 * @return String
	 */
	public String getAboveOpenOptionAmount() {
		return aboveOpenOptionAmount.getText();
	}
	/**
	 * Gets above open options
	 * @return String
	 */
	public String getAboveOpenOptions() {
		return aboveOpenOptionCount.getText();
	}
	/**
	 * Choose market if current is not active
	 */
	public void chooseMarket() {
		String att = upButton.getAttribute("disabled");
		if (att != null) {
			for (WebElement market : marketsLevelTime) {
				market.click();
				if (upButton.getAttribute("disabled") == null) {
					break;
				}
			}
		} else {
			System.out.println("don`t change market");
		}
	}
	/**
	 * Chooses market to invest
	 */
	public void chooseMarketToInvest() {
		int timeSize = marketsLevelTime.size();
		int profitSize = marketsProfits.size();
		int diff = 0;
		if (profitSize != timeSize) {
			diff = timeSize - profitSize;
		}
		String profit = null;
		for (int i = 0; i < timeSize; i++) {
			String info = marketsLevelTime.get(i + diff).getText();
			profit = marketsProfits.get(i).getText();
			if (info.contains("|")) {
				String text[] = info.split("\\|");
				String time[] = text[1].split(":");
				int seconds = Integer.parseInt(time[1]);
				int minutest = Integer.parseInt(time[0].replace(" ", ""));
				seconds = seconds + (minutest * 60);
				String pr[] = profit.split("\\.");
				profit = pr[0].replaceAll("[^\\d]", "");
				
				if (seconds > 60 && Integer.parseInt(profit) < 400) {
					marketsLevelTime.get(i + diff).click();
					break;
				}
			}
		}
	}
	/**
	 * Gets incorrect amount from purchased option
	 * @param currency
	 * @return String
	 */
	public String getIncorrectAmountFromPurchOption(String currency) {
		String amount = purchasedOptionIncorrectAmount.getText();
		return amount.replace(currency, "");
	}
	/**
	 * Gets return amount if correct from purchased option
	 * @param currecny
	 * @return String
	 */
	public String getCorrectAmountFromPurchOption(String currecny) {
		String amount = purchasedOptionCorrectAmount.getText();
		return amount.replace(currecny, "");
	}
	/**
	 * Gets profit from purchased option
	 * @return String
	 */
	public String getProfitFromPurchOption() {
		String profit = purchasedOptionProfit.getText();
		return profit.replace("%", "");
	}
	/**
	 * Gets amount from purchased option
	 * @param currency
	 * @return String
	 */
	public String getAmountFromPurchOption(String currency) {
		String amount = purchasedOptionAmount.getText();
		return amount.replace(currency, "");
	}
	/**
	 * Gets market name from purchased option
	 * @return String
	 */
	public String getMarketNameFromPurchOption() {
		return purchasedOptionMarket.getText();
	}
	/**
	 * Is option purchased
	 * @return boolean
	 */
	public boolean isOptionPurchased() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 15);
		try {
			String waitCss = "#dynamics-chart-0 > div.chart-container.with-investment > div.chart-overlays.option-purchased";
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(waitCss)));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Gets above profit percent
	 * @return String
	 */
	public String getAboveProfit() {
		String label = aboveProfit.getText();
		return label.replace("%", "");
	}
	/**
	 * Makes above investment
	 */
	public void makeAboveInvestment() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		String id = "dynamics-chart-0";
		wait.until(ExpectedConditions.attributeToBe(By.id(id), "class", "main-chart open"));
		upButton.click();
		try {
			String waitCss = "#dynamics-chart-0 > div.chart-container.with-investment > div.chart-overlays.option-purchased";
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(waitCss)));
		} catch (TimeoutException e) {
			this.makeAboveInvestment();
		}
	}
	/**
	 * Makes bellow investment
	 */
	public void makeBellowInvestment() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		String id = "dynamics-chart-0";
		wait.until(ExpectedConditions.attributeToBe(By.id(id), "class", "main-chart open"));
		downButton.click();
		try {
			String waitCss = "#dynamics-chart-0 > div.chart-container.with-investment > div.chart-overlays.option-purchased";
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(waitCss)));
		} catch (TimeoutException e) {
			this.makeBellowInvestment();
		}
	}
	/**
	 * Makes investment until pop up for cancel is displayed
	 */
	public void makeAboveInvestmentToCancel() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		String id = "dynamics-chart-0";
		wait.until(ExpectedConditions.attributeToBe(By.id(id), "class", "main-chart open"));
		upButton.click();
		try {
			String waitCss = "#dynamics-chart-0 > div.chart-container > div.chart-overlays.table.cancel-inv > div.cancel-inv-slip > div.taC > span";
			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(waitCss)));
		} catch (TimeoutException e) {
			this.makeAboveInvestmentToCancel();
		}
	}
	/**
	 * Gets opened options in chart
	 * @return Integer
	 */
	public int getOpenedOptionsInChart() {
		String label = openedOptionslabelInChart.getText();
		return Integer.parseInt(label.replaceAll("[^\\d]", ""));
	}
	/**
	 * Gets investment amount from drop down
	 * @return String
	 */
	public String getInvestmentAmount() {
		return investmentAmountDropDown.getText();
	}
	/**
	 * Sets investment other amount
	 * @param amount
	 */
	public void setAmount(String amount) {
		investmentAmountDropDown.click();
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.elementToBeClickable(investmentOtherAmount));		
		investmentOtherAmount.clear();
		investmentOtherAmount.sendKeys(amount);
		investmentAmountDropDown.click();
	}
	/**
	 * Gets above return amount if incorrect
	 * @return String
	 */
	public String getAboveIncorrect() {
		String amount = aboveReturnIfIncorrect.getText();
		return amount.replaceAll("[^\\d]", "");
	}
	/**
	 * Gets above return amount if correct
	 * @param currency
	 * @return String
	 */
	public String getAboveCorrect(String currency) {
		String amount = aboveReturnIfCorrect.getText();
		return amount.replace(currency, "");
	}
	/**
	 * Gets current market name
	 * @return String
	 */
	public String getCurrentMarket() {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver input) {
				String text = currentMarketLabel.getText();
				if (text.equals("")) {
					return false;
				} else {
					return true;
				}
			}
		});
		return currentMarketLabel.getText();
	}
	/**
	 * Closes tutorial
	 */
	public void closeTutorial() {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#dynamics-chart-0 > div.dyn-walkthrough > div.main_el > i.cool-close")));
		tutorialCloseButton.click();
	}
	/**
	 * Is tutorial displayed
	 * @return
	 */
	public boolean isDynamicsTutorialDisplayed() {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		try {
			wait.until(ExpectedConditions.attributeContains(dynamicsTutorial, "class", "step"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Clicks open account button
	 */
	public void openAccount() {
		openAccButton.click();
	}
	/**
	 * Logins from dynamics
	 */
	public void loginFromDynamics() {
		dynamicsLoginButton.click();
	}
	/**
	 * Opens dynamics
	 */
	public void openDynamics() {
		dynamicsTab.click();
	}
	/**
	 * Clicks Up button
	 */
	public void clickUpButton() {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		String id = "dynamics-chart-0";
		wait.until(ExpectedConditions.attributeToBe(By.id(id), "class", "main-chart open"));
		upButton.click();
	}
}
