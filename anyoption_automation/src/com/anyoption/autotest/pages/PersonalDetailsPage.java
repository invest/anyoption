package com.anyoption.autotest.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class PersonalDetailsPage {
	WebDriver driver;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div.my_account_menu > div:nth-child(4) > ul > li:nth-child(1) > a")
	private WebElement openPersonalDetailsLink;
	@FindBy(id = "personalForm:j_id1078748838_4eb51d2d")
	private WebElement submitPasswordButton;
	@FindBy(id = "personalForm:passwordError")
	private WebElement passwordError;
	@FindBy(id = "personalForm:password")
	private WebElement passwordInput;
	@FindBy(css = "#personalForm > div > div:nth-child(2) > div")
	private WebElement firstNameLabel;
	@FindBy(css = "#personalForm > div > div:nth-child(3) > div")
	private WebElement familyLabel;
	@FindBy(css = "#personalForm > div > div:nth-child(12) > div")
	private WebElement emailLabel;
	@FindBy(id = "personalForm:street")
	private WebElement streetInput;
	@FindBy(id = "personalForm:streetno")
	private WebElement streetNoInput;
	@FindBy(id = "personalForm:zipCode")
	private WebElement postCodeInput;
	@FindBy(id = "personalForm:cityName")
	private WebElement cityName;
	@FindBy(id = "personalForm:countriesRegulatedList")
	private WebElement countryDropDown;
	@FindBy(id = "personalForm:gender:0")
	private WebElement maleRadioButton;
	@FindBy(id = "personalForm:mobilePhone")
	private WebElement mobilePhoneInput;
	@FindBy(id = "personalForm:landLinePhone")
	private WebElement landLinePhoneInput;
	@FindBy(id = "personalForm:contactByEmail")
	private WebElement contactByEmailCheckBox;
	@FindBy(id = "personalForm:contactBySms")
	private WebElement contactBySmsCheckBox;
	@FindBy(xpath = "//*[@id='personalForm']/div/div[15]/label")
	private WebElement contactByEmailCheckBoxText;
	@FindBy(xpath = "//*[@id='personalForm']/div/div[16]/label")
	private WebElement contactBySmsCheckBoxText;
	@FindBy(id = "personalForm:j_id1078748838_4eb51a31")
	private WebElement submitPersonalDetailsButton;
	@FindBy(css = "#theBody > div.head_bgr > div > div.pageWidth.cont_pos > div > div.my_account_main > div > h2")
	private WebElement screenTitle;
	@FindBy(css = "#messages > tbody > tr > td > span")
	private WebElement successUpdateMessage;
	/**
	 * 
	 * @param driver
	 */
	public PersonalDetailsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Gets success message
	 * @return String
	 */
	public String getSuccessMessage() {
		return successUpdateMessage.getText();
	}
	/**
	 * Gets screen title
	 * @return String
	 */
	public String getScreenTitle() {
		return screenTitle.getText();
	}
	/**
	 * Submits personal details
	 */
	public void submitPersonalDetails() {
		submitPersonalDetailsButton.click();
	}
	/**
	 * Gets contact by sms check box text
	 * @return String
	 */
	public String getContactBySmsCheckBoxText() {
		return contactBySmsCheckBoxText.getText();
	}
	/**
	 * Gets contact by email check box text
	 * @return String
	 */
	public String getContactByEmailCheckBoxText() {
		return contactByEmailCheckBoxText.getText();
	}
	/**
	 * Clicks contact by sms check box
	 */
	public void clickContactBySmsCheckBox() {
		contactBySmsCheckBox.click();
	}
	/**
	 * Clicks contact by email check box
	 */
	public void clickContactByMailCheckBox() {
		contactByEmailCheckBox.click();
	}
	/**
	 * Sets land line phone
	 * @param landLinePhone
	 */
	public void setLandLinePhone(String landLinePhone) {
		landLinePhoneInput.clear();
		landLinePhoneInput.sendKeys(landLinePhone);
	}
	/**
	 * Sets mobile phone 
	 * @param mobilePhone
	 */
	public void setMobilePhoneInput(String mobilePhone) {
		mobilePhoneInput.clear();
		mobilePhoneInput.sendKeys(mobilePhone);
	}
	/**
	 * Chooses male radio button;
	 */
	public void clickMaleRadioButton() {
		maleRadioButton.click();
	}
	/**
	 * Selects country
	 * @param value
	 */
	public void selectCountry(String value) {
		Select select = new Select(countryDropDown);
		select.selectByValue(value);
	}
	/**
	 * Sets city
	 * @param city
	 */
	public void setCity(String city) {
		cityName.clear();
		cityName.sendKeys(city);
	}
	/**
	 * Sets post code
	 * @param code
	 */
	public void setPostCode(String code) {
		postCodeInput.clear();
		postCodeInput.sendKeys(code);
	}
	/**
	 * Sets street number
	 * @param number
	 */
	public void setStreetNumber(String number) {
		streetNoInput.clear();
		streetNoInput.sendKeys(number);
	}
	/**
	 * Sets street
	 * @param street
	 */
	public void setStreet(String street) {
		streetInput.clear();
		streetInput.sendKeys(street);
	}
	/**
	 * Gets email
	 * @return String
	 */
	public String getEmail() {
		return emailLabel.getText();
	}
	/**
	 * Gets family 
	 * @return
	 */
	public String getFamily() {
		return familyLabel.getText();
	}
	/**
	 * Gets first name
	 * @return String
	 */
	public String getFirstName() {
		return firstNameLabel.getText();
	}
	/**
	 * Sets password
	 * @param pass
	 */
	public void setPassword(String pass) {
		passwordInput.clear();
		passwordInput.sendKeys(pass);
	}
	/**
	 * Gets password error text
	 * @return String
	 */
	public String getPasswordError() {
		return passwordError.getText();
	}
	/**
	 * Submits password
	 */
	public void submitPassword() {
		submitPasswordButton.click();
	}
	/**
	 * Opens personal details link
	 */
	public void openPersonalDetails() {
		openPersonalDetailsLink.click();
	}
}
