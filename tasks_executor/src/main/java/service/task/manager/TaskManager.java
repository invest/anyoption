package service.task.manager;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import service.tasks.dao.TaskDAO;
import service.tasks.util.Constants;
import com.anyoption.common.beans.Task;
import com.anyoption.common.beans.TaskGroup;
import com.anyoption.common.util.OracleUtil;

/**
 * @author LioR SoLoMoN
 *
 */
public abstract class TaskManager extends com.anyoption.common.managers.TaskManager {
    private static final Logger logger = Logger.getLogger(TaskManager.class);
    
	public static List<Task> getPendingTasks() throws SQLException {
		logger.info("In getPendingTasks");
		Connection connection = null;
		try {
		    //TODO change get connection!
			connection = OracleUtil.getConnection(Manager.DB_URL, Manager.DB_USER, Manager.DB_PASSWORD);
			return TaskDAO.getPendingTasks(connection);
		} finally {
			OracleUtil.closeConnection(connection);
		}
	}
	
	/**
	 * @param numberOfRecords
	 * @return
	 * @throws SQLException
	 */
	public static List<Task> getPendingTasks(int numberOfRecords) throws SQLException {
		logger.info("In getPendingTasks");
		Connection connection = null;
		try {
		    //TODO change get connection!
			connection = OracleUtil.getConnection(Manager.DB_URL, Manager.DB_USER, Manager.DB_PASSWORD);
			return TaskDAO.getPendingTasks(connection, numberOfRecords);
		} finally {
			OracleUtil.closeConnection(connection);
		}
	}
	
	/**
	 * @param numberOfRecords
	 * @return
	 * @throws SQLException
	 */
	public static List<Task> getTasks(int numberOfRecords) {
		logger.info("In getTasks");
		Connection connection = null;
		List<Task> tasks = new ArrayList<Task>();
		String tableName = getTableName();	
		try {
		    //TODO change get connection!
			connection = OracleUtil.getConnection(Manager.DB_URL, Manager.DB_USER, Manager.DB_PASSWORD);
			connection.setAutoCommit(false);		
			
			TaskDAO.createTempTable(connection, tableName, numberOfRecords);
			
			TaskDAO.updateToInProgress(connection, tableName);
			
		    tasks = TaskDAO.getTasksFromTempTable(connection, tableName);	
	    
			logger.info("tasks size: " + tasks.size());
			connection.commit();
    	} catch (Exception e) {
    		logger.error("ERROR!", e);
			try {
				connection.rollback();
			} catch (Throwable it) {
				logger.error("Can't rollback.", it);
			}
    	} finally {
    		if (connection != null) {
	            try {
	            	connection.setAutoCommit(true);
	            } catch (Throwable t) {
	            	logger.error("Can't set autocommit true.", t);
	            }
    		}
            OracleUtil.closeConnection(connection);	
            dropTempTable(tableName); //make sure.
		}
		return tasks;
	}
	
	private static void dropTempTable(String tableName) {
    	Connection connection = null;
	
		try {
			connection = OracleUtil.getConnection(Manager.DB_URL, Manager.DB_USER, Manager.DB_PASSWORD);
			TaskDAO.dropTempTable(connection, tableName);
        	logger.info("DROP TABLE " + tableName);
		} catch (SQLException e) {
			 logger.error("ERROR! Failed to DROP temporary table " + tableName, e);
		} finally {
			OracleUtil.closeConnection(connection);
		}
    }


	private static String getTableName() {
		return Constants.TASK_SUBJECTS_TEMP_PENDING + System.currentTimeMillis();
	}
	
	/**
	 * @param task.status.id
	 * @return
	 * @throws SQLException
	 */
	public static int updateStatus(Task task) throws SQLException {
		Connection connection = null;
		int numOfRecords = 0;
		try {
			connection = OracleUtil.getConnection(Manager.DB_URL, Manager.DB_USER, Manager.DB_PASSWORD);
			numOfRecords = TaskDAO.updateStatus(connection, task);
			logger.info("number of records which updated: " + numOfRecords);
		} finally {
			OracleUtil.closeConnection(connection);
		}
		return numOfRecords;
	}
	
	public static int updateStatusAfterFinalize(Task task) throws SQLException {
		Connection connection = null;
		int numOfRecords = 0;
		try {
			connection = OracleUtil.getConnection(Manager.DB_URL, Manager.DB_USER, Manager.DB_PASSWORD);
			numOfRecords = TaskDAO.updateStatusAfterFinalize(connection, task);
			logger.info("number of records which updated: " + numOfRecords);
		} finally {
			OracleUtil.closeConnection(connection);
		}
		return numOfRecords;
	}
	
	/**
	 * @param task
	 * @throws SQLException
	 */
	public static void insertTask(Task task) throws SQLException {
		Connection connection = null;
		try {
			connection = OracleUtil.getConnection(Manager.DB_URL, Manager.DB_USER, Manager.DB_PASSWORD);
			TaskDAO.insertTask(connection, task);
		} finally {
			OracleUtil.closeConnection(connection);
		}
	}
	
	public static void insertTaskGroup(TaskGroup taskGroup) throws SQLException {
		Connection connection = null;
		try {
			connection = OracleUtil.getConnection(Manager.DB_URL, Manager.DB_USER, Manager.DB_PASSWORD);
			TaskDAO.insertTaskGroup(connection, taskGroup);
		} finally {
			OracleUtil.closeConnection(connection);
		}
	}
}