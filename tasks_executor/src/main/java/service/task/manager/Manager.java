package service.task.manager;

import service.tasks.util.TaskUtil;

/**
 * @author LioR SoLoMoN
 *
 */
public class Manager {

	public static final String DB_URL = TaskUtil.getValueFromFile("db.url");
	public static final String DB_USER = TaskUtil.getValueFromFile("db.user");
	public static final String DB_PASSWORD = TaskUtil.getValueFromFile("db.password");	
}
