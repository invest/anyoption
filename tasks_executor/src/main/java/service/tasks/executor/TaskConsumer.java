package service.tasks.executor;

import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Task;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import service.tasks.Taskable;

/**
 * @author LioR SoLoMoN
 *
 */
public class TaskConsumer implements Runnable {
	private static final Logger logger = Logger.getLogger(TaskConsumer.class);	
	protected BlockingQueue<Task> tasksQueue = null;
	
	public TaskConsumer(BlockingQueue<Task> queue) {
        this.tasksQueue = queue;
    }
	
	@Override
	public void run() {
		logger.info("run consumer");
		Gson gson = new GsonBuilder().create();   
		while(true) {
			try {
				Task task = tasksQueue.take(); //Queue empty => wait until there is
				logger.info("consumer, Queue size: " + tasksQueue.size());
				Class<?> clazz = Class.forName(task.getClazz());				
				Taskable runningTask = (Taskable) gson.fromJson(task.getParameters(), clazz);
				runningTask.initTask(task);
				boolean isSuccessful = runningTask.doTask(task);				
				runningTask.finalizeTask(task, isSuccessful);				
			} catch(Exception e) {				
				logger.error("ERROR! consumer", e);
			}
		}		
	}
}
