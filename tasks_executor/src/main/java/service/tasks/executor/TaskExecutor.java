package service.tasks.executor;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import org.apache.log4j.Logger;
import com.anyoption.common.beans.Task;
import service.task.manager.TaskManager;
import service.tasks.util.Constants;
import service.tasks.util.TaskException;

/**
 * @author LioR SoLoMoN
 *
 */
public class TaskExecutor {
	private static final Logger logger = Logger.getLogger(TaskExecutor.class);
	
	public static void main(String[] args) throws Exception {
        logger.info("***** START Main " + Constants.TASK_SERVICE_NAME + " SERVICE *****");
		
        BlockingQueue<Task> tasksQueue = new ArrayBlockingQueue<Task>(Constants.QUEUE_SIZE);
        try {
        	logger.debug("***** Queue size: " + Constants.QUEUE_SIZE + " " + "*****");
        	List<Task> tasks = TaskManager.getTasks(Constants.QUEUE_SIZE);
        	tasksQueue.addAll(tasks);
        } catch (IllegalStateException e) {
        	logger.error("ERROR, Queue is FULL! Queue Size: " + tasksQueue.size() + ". Continue! ", e);
        } catch (Exception e) {
        	throw new TaskException("ERROR! ", e);
        }
        
        Thread taskProducers = new Thread(new TaskProducer(tasksQueue));
        Thread[] taskConsumers = new Thread[Constants.CONSUMER_SIZE];
        
        for(int i = 0; i < Constants.CONSUMER_SIZE; i++) {
        	taskConsumers[i] = new Thread(new TaskConsumer(tasksQueue));
        }
        
        for(int i = 0; i < Constants.CONSUMER_SIZE; i++) {
        	taskConsumers[i].start();
        }
        
        taskProducers.start();
       
        logger.info("***** END Main " + Constants.TASK_SERVICE_NAME + " SERVICE *****");
	}
}
