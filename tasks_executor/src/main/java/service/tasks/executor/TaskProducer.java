package service.tasks.executor;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import org.apache.log4j.Logger;
import com.anyoption.common.beans.Task;
import service.task.manager.TaskManager;
import service.tasks.util.Constants;

/**
 * @author LioR SoLoMoN
 *
 */
public class TaskProducer implements Runnable {
	private static final Logger logger = Logger.getLogger(TaskProducer.class);
	protected BlockingQueue<Task> tasksQueue = null;
	
	public TaskProducer(BlockingQueue<Task> tasksQueue) {
        this.tasksQueue = tasksQueue;
    }
	
	@Override
	public void run() {
		logger.info("run producer");
		while(true) {
			try {
				// if the queue is full then the put will wait until there is space for new item in the queue
				logger.info("producer sleep");
				Thread.sleep(Constants.PRODUCER_SLEEP_TIME);
				List<Task> tasks = TaskManager.getTasks(Constants.QUEUE_SIZE);
				for (Task task : tasks) {	
					logger.info("producer, before put");
					tasksQueue.put(task);
					logger.info("producer, Queue size: " + tasksQueue.size());
				}	
			} catch(Exception e) {
				logger.error("ERROR! producer", e);
			}
		}
	}
}
