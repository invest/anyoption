package service.tasks;

import il.co.etrader.bl_managers.IssuesManagerBase;
import java.sql.Connection;
import org.apache.log4j.Logger;
import com.anyoption.common.beans.IssueActionTaskBase;
import com.anyoption.common.beans.Task;
import com.anyoption.common.util.OracleUtil;
import service.task.manager.Manager;

/**
 * @author LioR SoLoMoN
 *
 */
public class IssueActionTask extends IssueActionTaskBase implements Taskable {
	private static final Logger logger = Logger.getLogger(IssueActionTask.class);
	
	@Override
	public void initTask(Task task) {
		logger.info("IssueActionTask initTask");
	}

	@Override
	public boolean doTask(Task task) {
		logger.info("IssueActionTask doTask; " + task.toString());
		Connection connection = null;
		boolean res = true;
		try {
			connection = OracleUtil.getConnection(Manager.DB_URL, Manager.DB_USER, Manager.DB_PASSWORD);
			issue.setUserId(task.getReferenceId());
			IssuesManagerBase.insertIssueAndIssueAction(connection, issue, issueAction);
			
		} catch (Exception e) {
			logger.info("ERROR! IssueActionTask doTask", e);
			res = false;
		} finally {
			OracleUtil.closeConnection(connection);
		}
		
		return res;
	}
}
