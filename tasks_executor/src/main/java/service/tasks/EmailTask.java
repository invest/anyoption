package service.tasks;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Task;

/**
 * @author LioR SoLoMoN
 *
 */
public class EmailTask implements Taskable {
	private static final Logger logger = Logger.getLogger(EmailTask.class);

	@Override
	public void initTask(Task task) {
		// TODO Auto-generated method stub
		logger.info("initTask email");
		
	}

	@Override
	public boolean doTask(Task task) {
		// TODO Auto-generated method stub
		logger.info("doTask email");
		return true;
	}

	@Override
	public void finalizeTask(Task task, boolean isSuccessful) {
		// TODO Auto-generated method stub
		logger.info("finalizeTask email");
	}
}
