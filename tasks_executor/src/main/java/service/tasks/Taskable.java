package service.tasks;

import org.apache.log4j.Logger;
import com.anyoption.common.beans.Task;
import com.anyoption.common.beans.Task.Status;
import service.task.manager.TaskManager;

/**
 * @author LioR SoLoMoN
 *
 */
public interface Taskable {
	/**
	 * initial task
	 */
	public void initTask(Task task);
	
	/**
	 * do the task
	 * @return true is the task is done successfully
	 */
	public boolean doTask(Task task);

	/**
	 * finalize task.
	 */
	default public void finalizeTask(Task task, boolean isSuccessful) {
		Logger.getLogger("Taskable").info("finalizeTask TestTask");
		try {
			task.setStatus(Status.PENDING);
			task.setNumOfRetries(task.getNumOfRetries() + 1);
			if (isSuccessful) {
				task.setStatus(Status.DONE);
			} else if (task.getMaxRetries() == task.getNumOfRetries()) {
				task.setStatus(Status.FAIL);
			}
			TaskManager.updateStatusAfterFinalize(task);
		} catch (Exception e) {
			Logger.getLogger("Taskable").error("ERROR! finalizeTask ", e);
		}	
	}
}
