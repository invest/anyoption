package service.tasks.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 * @author LioR SoLoMoN
 *
 */
public class TaskUtil {
	private static final Logger logger = Logger.getLogger(TaskUtil.class);
	
	/**
	 * @param key
	 * @return
	 */
	public static String getValueFromFile(String key) {
		String res = "";
		try {
			File file = new File("TaskExecutor.properties");
			FileInputStream fileInput = new FileInputStream(file);
			Properties properties = new Properties();
			properties.load(fileInput);
			fileInput.close();
			res = properties.getProperty(key);			
		} catch (FileNotFoundException e) {
			logger.error("ERROR! file not found", e);
		} catch (IOException e) {
			logger.error("ERROR! file problem", e);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return res;
	}
}
