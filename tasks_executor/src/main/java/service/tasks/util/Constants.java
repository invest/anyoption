package service.tasks.util;

import service.tasks.executor.TaskExecutor;

/**
 * @author LioR SoLoMoN
 *
 */
public abstract class Constants {
	
	public static final int QUEUE_SIZE = 5000;
	public static final String TASK_SERVICE_NAME = TaskExecutor.class.getSimpleName();
	public static final int CONSUMER_SIZE = 10;
	public static String TASK_SUBJECTS_TEMP_PENDING = "TASK_SUBJECTS_";
	public static int PRODUCER_SLEEP_TIME = 600000;
}
