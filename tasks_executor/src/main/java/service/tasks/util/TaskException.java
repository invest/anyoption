package service.tasks.util;

/**
 * @author LioR SoLoMoN
 *
 */
public class TaskException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6595572446239650947L;

	public TaskException() {
        super();
    }

    public TaskException(String message) {
        super(message);
    }

    public TaskException(String message, Throwable t) {
        super(message, t);
        sendEmail(message, t);
    }

	private void sendEmail(String message, Throwable t) {
		// TODO Auto-generated method stub		
	}
}