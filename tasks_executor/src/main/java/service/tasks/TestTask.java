package service.tasks;

import org.apache.log4j.Logger;
import com.anyoption.common.beans.Task;

/**
 * @author LioR SoLoMoN
 *
 */
public class TestTask implements Taskable {
	private static final Logger logger = Logger.getLogger(TestTask.class);
	
	public TestTask() {
		logger.info("TestTask constractor");
	}
	
	@Override
	public void initTask(Task task) {
		logger.info("initTask TestTask");
		// TODO Auto-generated method stub		
	}

	@Override
	public boolean doTask(Task task) {
		logger.info("doTask TestTask, id: " + task.getTaskSubjectsId());
		// TODO Auto-generated method stub
		return true;
	}
	
	@Override
	public void finalizeTask(Task task, boolean isSuccessful) {
		logger.info("finalizeTask Taskable");
		Taskable.super.finalizeTask(task, isSuccessful);
	}
}
