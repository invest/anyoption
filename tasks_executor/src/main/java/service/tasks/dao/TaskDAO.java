package service.tasks.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import com.anyoption.common.beans.Task;
import com.anyoption.common.beans.Task.Status;
import com.anyoption.common.beans.TaskGroup;
import com.anyoption.common.daos.DAOBase;

/**
 * @author LioR SoLoMoN
 *
 */
public class TaskDAO extends DAOBase {
	private static final Logger logger = Logger.getLogger(TaskDAO.class);
    
	/**
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */
	private static Task getTaskVO(ResultSet resultSet) throws SQLException {
		Task task = new Task();
        task.setTaskSubjectsId(resultSet.getLong("task_subject_id"));
        task.setClazz(resultSet.getString("class"));
        task.setParameters(resultSet.getString("parameters"));
        task.setReferenceId(resultSet.getLong("reference_id"));
        task.setStatus(resultSet.getInt("status") == 1 ? Status.PENDING : Status.DONE);
        task.setWriterId(resultSet.getInt("writer_id"));
        task.setNumOfRetries(resultSet.getInt("num_of_retries"));
        task.setMaxRetries(resultSet.getInt("max_retries"));
        return task;
	}
	
	/**
	 * Prioritized
	 * 
	 * order by priority, ascending  
	 * @param connection
	 * @return
	 * @throws SQLException
	 */
	public static List<Task> getPendingTasks(Connection connection) throws SQLException {
        String sql =
                	"SELECT " +
        			"	ts.id as task_subject_id " +
                	"	,ts.* " +
        			"	,tsg.* " +
        			"	,tst.* " +
                    "FROM " + 
                    "	task_subjects ts " +
                    "	,task_subjects_group tsg " +
                    "	,task_subjects_types tst " +
                    "WHERE " +
                    "	ts.task_subjects_group_id = tsg.id " +
                    "	and tsg.task_subjects_types_id = tst.id " +
                    "	and ts.status = " + Status.PENDING.getId() + " " +
                    "ORDER BY " + 
                    "	tsg.priority asc ";
        
        List<Task> tasks = new ArrayList<Task>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = connection.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {               
                tasks.add(getTaskVO(rs));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return tasks;
    }
	
	/**
	 * @param connection
	 * @param numberOfRecords
	 * @return
	 * @throws SQLException
	 */
	public static List<Task> getPendingTasks(Connection connection, int numberOfRecords) throws SQLException {
        String sql =
        			"SELECT " +
        			"	* " +
        			"FROM " +
        			" ( " +
	                	"SELECT " +
	        			"	ts.id as task_subject_id " +
	                	"	,tst.class " +
	        			"	,tsg.parameters " +
	        			"	,ts.reference_id " +
	        			"	,ts.status " +
	        			"	,ts.writer_id " +
	        			"	,ts.num_of_retries " +
	        			"	,ts.max_retries " +
	                    "FROM " + 
	                    "	task_subjects ts " +
	                    "	,task_subjects_group tsg " +
	                    "	,task_subjects_types tst " +
	                    "WHERE " +
	                    "	ts.task_subjects_group_id = tsg.id " +
	                    "	and tsg.task_subjects_types_id = tst.id " +
	                    "	and ts.status = " + Status.PENDING.getId() + " " +
	                    "ORDER BY tsg.priority asc " +
                    " ) T " +
	                "WHERE " +
	                "	ROWNUM <= ? ";	                   
        List<Task> tasks = new ArrayList<Task>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, numberOfRecords);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                tasks.add(getTaskVO(rs));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return tasks;
    }

	/**
	 * 
	 * @param connection
	 * @param task.status.id
	 * @return 
	 * @throws SQLException
	 */
	public static int updateStatus(Connection connection, Task task) throws SQLException {
		int numOfRecords = 0;
		String sql =
				"UPDATE " +
			 	"	task_subjects ts " +
				"SET " +
			 	"	ts.status = ? " +
				"	,ts.time_updated = sysdate " +
			 	"WHERE " +
				"	ts.id = ? ";
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, task.getStatus().getId());
			ps.setLong(2, task.getTaskSubjectsId());
			numOfRecords = ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
		return numOfRecords;
	}
	
	/**
	 * @param connection
	 * @param task
	 * @return
	 * @throws SQLException
	 */
	public static int updateStatusAfterFinalize(Connection connection, Task task) throws SQLException {
		int numOfRecords = 0;
		String sql =
				"UPDATE " +
			 	"	task_subjects ts " +
				"SET " +
			 	"	ts.status = ? " +
				"	,ts.num_of_retries = ? " +
				"	,ts.time_updated = sysdate " +
			 	"WHERE " +
				"	ts.id = ? ";
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, task.getStatus().getId());
			ps.setInt(2, task.getNumOfRetries());
			ps.setLong(3, task.getTaskSubjectsId());
			numOfRecords = ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
		return numOfRecords;
	}
	
	/**
	 * @param connection
	 * @param task
	 * @throws SQLException
	 */
	public static void insertTask(Connection connection, Task task) throws SQLException {
		int index = 0;
		String sql =
				"INSERT INTO " +
			 	"	task_subjects " +
				"	(" +
				"	id " +
				"	,reference_id " +
				"	,status " +
				"	,time_created " +
				"	,task_subjects_group_id " +
				"	,writer_id " +
				"	,time_updated " +
				"	)" +
				"VALUES" +
				"	(" +
				"	seq_task_subjects.nextval " +
				"	,? " +
				"	,? " +
				"	,sysdate " +
				"	,? " +
				"	,? " +
				"	,sysdate " +
				"	)";
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(++index, task.getReferenceId());
			ps.setLong(++index, task.getStatus().getId());
			ps.setLong(++index, task.getTaskSubjectsGroupId());
			ps.setLong(++index, task.getWriterId());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @param connection
	 * @param tableName
	 * @return
	 * @throws SQLException
	 */
	public static List<Task> getTasksFromTempTable(Connection connection, String tableName) throws SQLException {
		String sql =
				"SELECT " +
				"	* " +
				"FROM " +
					tableName;
		                   
		 List<Task> tasks = new ArrayList<Task>();
		 PreparedStatement pstmt = null;
		 ResultSet rs = null;
		 try {
		     pstmt = connection.prepareStatement(sql);
		     rs = pstmt.executeQuery();
		     while (rs.next()) {
		         tasks.add(getTaskVO(rs));
		     }
		 } finally {
		     closeResultSet(rs);
		     closeStatement(pstmt);
		 }
		 return tasks;
		
	}

	/**
	 * @param connection
	 * @param tableName
	 * @param numberOfRecords
	 * @throws SQLException
	 */
	public static void createTempTable(Connection connection, String tableName, int numberOfRecords) throws SQLException {
		String sql =
				"CREATE GLOBAL TEMPORARY TABLE  " + tableName + " " +
				"	ON COMMIT PRESERVE ROWS AS " +
    			"SELECT " +
    			"	* " +
    			"FROM " +
    			" ( " +
                	"SELECT " +
        			"	ts.id as task_subject_id " +
                	"	,tst.class " +
        			"	,tsg.parameters " +
        			"	,ts.reference_id " +
        			"	,ts.status " +
        			"	,ts.writer_id " +
        			"	,ts.num_of_retries " +
        			"	,tst.max_retries " +
                    "FROM " + 
                    "	task_subjects ts " +
                    "	,task_subjects_group tsg " +
                    "	,task_subjects_types tst " +
                    "WHERE " +
                    "	ts.task_subjects_group_id = tsg.id " +
                    "	and tsg.task_subjects_types_id = tst.id " +
                    "	and ts.status = "  + Status.PENDING.getId() + " " +
                    "ORDER BY tsg.priority asc " +
                " ) T " +
                "WHERE " +
                "	ROWNUM <= " + numberOfRecords + " ";
                //"FOR UPDATE "; no need if 1 producer.
                   
	    
	    PreparedStatement pstmt = null;
	    try {
	        pstmt = connection.prepareStatement(sql);    		
	        pstmt.executeUpdate();
	    } finally {
	        closeStatement(pstmt);
	    }		
	}

	public static int updateToInProgress(Connection connection, String tableName) throws SQLException {
		int numOfRecords = 0;
		String sql =
				"UPDATE " +
			 	"	task_subjects ts " +
				"SET " +
			 	"	ts.status = " + Status.IN_PROGRESS.getId() + " " +
				"	,ts.time_updated = sysdate " +
			 	"WHERE " +
				"	ts.id in 	( " +
				"				SELECT " +
				"					task_subject_id " +
				"				FROM " +
				"						" + tableName + " " + 
				"				)";
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(sql);	
			numOfRecords = ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
		return numOfRecords;
	}

	/**
	 * @param connection
	 * @param tableName
	 * @throws SQLException
	 */
	public static void dropTempTable(Connection connection, String tableName) throws SQLException {
    	PreparedStatement ps = null;
    	try {
    		ps = connection.prepareStatement("DROP TABLE " + tableName);
    		ps.executeUpdate();
    	} finally {
    		closeStatement(ps);
    	}
	}
	
	/**
	 * @param connection
	 * @param taskGroup
	 * @throws SQLException
	 */
	public static void insertTaskGroup(Connection connection, TaskGroup taskGroup) throws SQLException {
		String sql = 
				"INSERT INTO " +
				"	task_subjects_group " +
				"	( " +
				"		id " +
				"		,task_subjects_types_id " +
				"		,parameters " +
				//"		,priority " +
				"		,time_created " +
				"	) " +
				"VALUES " +
				"	( " +
				"		seq_task_subjects_group.nextval " +
				"		,? " +
				"		,? " +
				//"		,? " +
				"		,sysdate " +
				"	)";
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, taskGroup.getTaskSubjectsTypesId());
			ps.setString(2, taskGroup.getParameters());
			//ps.setInt(3, taskGroup.getPriority());
			ps.executeUpdate();
			taskGroup.setId((int)getSeqCurValue(connection, "seq_task_subjects_group"));
		} finally {
			closeStatement(ps);
		}
	}
}