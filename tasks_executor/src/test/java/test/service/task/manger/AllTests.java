package test.service.task.manger;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author LioR SoLoMoN
 *
 */
@RunWith(Suite.class)
@SuiteClasses({
	GetPendingTasksJUnit.class
	,InsertTaskJUnit.class
	,UpdateTaskJUnit.class
	,InsertTaskGroup.class

})
public class AllTests {

}
