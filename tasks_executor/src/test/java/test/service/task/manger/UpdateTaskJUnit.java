package test.service.task.manger;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.anyoption.common.beans.Task;
import com.anyoption.common.beans.Task.Status;

import service.task.manager.TaskManager;

/**
 * @author LioR SoLoMoN
 *
 */
public class UpdateTaskJUnit {
	private static final Logger logger = Logger.getLogger(UpdateTaskJUnit.class);
	
	@Test
	public void updateTask() {	
		int updateCount = 0;
		try {
			updateCount = TaskManager.updateStatus(new Task(365, Status.PENDING));
		} catch (Exception e) {
			logger.info(e);
			fail("ERROR!");
		}
		logger.info("updateCount: " + updateCount + " row");
		return;
	}
}
