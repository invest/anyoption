package test.service.task.manger;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.Test;

import service.task.manager.TaskManager;

import com.anyoption.common.beans.Task;
import com.anyoption.common.beans.Task.Status;
import com.anyoption.common.beans.Writer;

/**
 * @author LioR SoLoMoN
 *
 */
public class InsertTaskJUnit {
	private static final Logger logger = Logger.getLogger(InsertTaskJUnit.class);
	
	@Test
	public void insertTask() {	
		try {
			Task task = new Task();
			task.setClazz("service.tasks.TestClass");
			task.setParameters("");
			task.setReferenceId(0);
			task.setStatus(Status.PENDING);
			task.setTaskSubjectsGroupId(2);
			task.setWriterId(Writer.WRITER_ID_AUTO);
			TaskManager.insertTask(task);
		} catch (Exception e) {
			logger.info(e);
			fail("ERROR!");
		}
		return;
	}
}
