package test.service.task.manger;

import static org.junit.Assert.fail;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.anyoption.common.beans.Task;
import com.anyoption.common.beans.Task.Status;
import com.anyoption.common.beans.Writer;

import service.task.manager.TaskManager;

/**
 * @author LioR SoLoMoN
 *
 */
public class TaskManagerJUnit {
	private static final Logger logger = Logger.getLogger(TaskManagerJUnit.class);
	
	@Test
	public void getPendingTasks() {	
		List<Task> tasks = null;
		try {
			tasks = TaskManager.getPendingTasks();
		} catch (Exception e) {
			logger.info(e);
			fail("ERROR!");
		}
		int count = 0;
		for (Task task : tasks) {
			logger.info(++count + ". " + task.toString());
		}
		return;
	}
	
	@Test
	public void getPendingTasksRowNum() {	
		List<Task> tasks = null;
		final int numberOfRecords = 10;
		try {
			tasks = TaskManager.getPendingTasks(numberOfRecords);
		} catch (Exception e) {
			logger.info(e);
			fail("ERROR!");
		}
		int count = 0;
		for (Task task : tasks) {
			logger.info(++count + ". " + task.toString());
		}
		return;
	}
	
	@Test
	public void updateTask() {	
		int updateCount = 0;
		try {
			updateCount = TaskManager.updateStatus(new Task(365, Status.PENDING));
		} catch (Exception e) {
			logger.info(e);
			fail("ERROR!");
		}
		logger.info("updateCount: " + updateCount + " row");
		return;
	}
	
	@Test
	public void insertTask() {	
		try {
			Task task = new Task();
			task.setClazz("service.tasks.TestClass");
			task.setParameters("");
			task.setReferenceId(0);
			task.setStatus(Status.PENDING);
			task.setTaskSubjectsGroupId(2);
			task.setWriterId(Writer.WRITER_ID_AUTO);
			TaskManager.insertTask(task);
		} catch (Exception e) {
			logger.info(e);
			fail("ERROR!");
		}
		return;
	}
}
