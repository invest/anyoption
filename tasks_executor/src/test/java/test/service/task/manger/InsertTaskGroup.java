package test.service.task.manger;

import static org.junit.Assert.fail;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import com.anyoption.common.beans.TaskGroup;

import service.task.manager.TaskManager;

/**
 * @author LioR SoLoMoN
 *
 */
public class InsertTaskGroup {
	private static final Logger logger = Logger.getLogger(InsertTaskGroup.class);
	private TaskGroup taskGroup;
	
	@Before
	public void init() {
		taskGroup = new TaskGroup();
		taskGroup.setParameters("");
		//taskGroup.setPriority(priority);
		taskGroup.setTaskSubjectsTypesId(2);
	}
	
	@Test
	public void insertTaskGroup() {	
		try {
			TaskManager.insertTaskGroup(taskGroup);
		} catch (Exception e) {
			logger.info(e);
			fail("ERROR!");
		}
		return;
	}
}
