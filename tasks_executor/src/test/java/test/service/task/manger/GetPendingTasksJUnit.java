package test.service.task.manger;

import static org.junit.Assert.*;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.anyoption.common.beans.Task;

import service.task.manager.TaskManager;

/**
 * @author LioR SoLoMoN
 *
 */
public class GetPendingTasksJUnit {
	private static final Logger logger = Logger.getLogger(GetPendingTasksJUnit.class);

	@Test
	public void getPendingTasks() {	
		List<Task> tasks = null;
		try {
			tasks = TaskManager.getPendingTasks();
		} catch (Exception e) {
			logger.info(e);
			fail("ERROR!");
		}
		int count = 0;
		for (Task task : tasks) {
			logger.info(++count + ". " + task.toString());
		}
		return;
	}
	
	@Test
	public void getPendingTasksRowNum() {	
		List<Task> tasks = null;
		final int numberOfRecords = 10;
		try {
			tasks = TaskManager.getPendingTasks(numberOfRecords);
		} catch (Exception e) {
			logger.info(e);
			fail("ERROR!");
		}
		int count = 0;
		for (Task task : tasks) {
			logger.info(++count + ". " + task.toString());
		}
		return;
	}
}
