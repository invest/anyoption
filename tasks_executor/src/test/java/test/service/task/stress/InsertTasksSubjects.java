package test.service.task.stress;

import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import service.task.manager.TaskManager;

import com.anyoption.common.beans.Task;
import com.anyoption.common.beans.Task.Status;
import com.anyoption.common.beans.Writer;

/**
 * @author LioR SoLoMoN
 * 
 * 1. @Parameterized -> 
 *	repeat the following flow 'Parameters array size times'	
 *	2. constructor
 *	3. @Before
 *	4. @Test
 */
@RunWith(Parameterized.class)
public class InsertTasksSubjects {		
	private static final Logger logger = Logger.getLogger(InsertTasksSubjects.class);
	private static final int NUMBER_OF_INSERTS = 100;
	private static final int REFERENCE_START_NUMBER = 130782;
	private static final int TASK_SUBJECTS_GROUP_ID = 3;
	private Task task;
	
	@Parameterized.Parameters
    public static Collection tasksParameters() {
		int ref = REFERENCE_START_NUMBER;
		Task tasks[] = new Task[NUMBER_OF_INSERTS];
		for(int i = 0; i < NUMBER_OF_INSERTS; i++) {
			tasks[i] = new Task(ref++, TASK_SUBJECTS_GROUP_ID);
		}
        return Arrays.asList(tasks);
    }

    public InsertTasksSubjects(Task task) {
    	this.task = task;
    }
    
    @Before
    public void initialize() {
    	task.setStatus(Status.PENDING);
    	task.setWriterId(Writer.WRITER_ID_AUTO);
    }

    @Test
    public void runsTenTimes() {
    	try {
			TaskManager.insertTask(task);
	    } catch (Exception e) {
			logger.info(e);
			fail("ERROR!");
		}
		return;
    }
}
