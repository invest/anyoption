package Reading;

import il.co.etrader.util.CommonUtil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

import com.sun.xml.internal.fastinfoset.util.StringArray;

public class convertToCountriesPlist {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		String fileToRead = "countries.txt";
		String fileToWrite = "countries.plist";
		String[] arr = null;
		String readString1 = "";
		int i = 0;
		
		BufferedReader fRead1 = null;
		BufferedWriter fWritefound = null;
		
		
		try {
			fRead1 = new BufferedReader(new InputStreamReader(new FileInputStream(fileToRead),"UTF8"));
			fWritefound = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileToWrite),"UTF8"));
			fWritefound.write("<dict>");
			fWritefound.newLine();
			
			while ((readString1 =  fRead1.readLine()) != null){
				i = 0;
				readString1 = readString1.trim();
				arr = readString1.split(",");
				fWritefound.write("	<key>" + arr[i] + "</key>");
				i++;
				fWritefound.newLine();
				fWritefound.write("	<dict>");
				fWritefound.newLine();
				fWritefound.write("		<key>display_name</key>");
				fWritefound.newLine();
				fWritefound.write("		<string>" + arr[i] + "</string>");
				fWritefound.newLine();
				i++;
				fWritefound.write("		<key>phone_code</key>");
				fWritefound.newLine();
				fWritefound.write("		<string>" + arr[i] + "</string>");
				fWritefound.newLine();
				i++;
				fWritefound.write("		<key>support_fax</key>");
				fWritefound.newLine();
				fWritefound.write("		<string>" + arr[i] + "</string>");
				fWritefound.newLine();
				i++;
				fWritefound.write("		<key>payment_method_ids</key>");
				fWritefound.newLine();
				if(arr.length > i){
					fWritefound.write("		<array>");
					fWritefound.newLine();
					while(arr.length > i){
						fWritefound.write("			<integer>" + arr[i] + "</integer>");
						fWritefound.newLine();
						i++;
					}
					fWritefound.write("		</array>");
					fWritefound.newLine();
				} else {
					fWritefound.write("		<array/>");
					fWritefound.newLine();
				}
				fWritefound.write("	</dict>");
				fWritefound.newLine();				
			}
			fRead1.close();
			fWritefound.close();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
