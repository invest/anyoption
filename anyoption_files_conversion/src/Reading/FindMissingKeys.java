package Reading;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

public class FindMissingKeys {

	/**
	 * @param args
	 * @throws Exception
	 * @author oshikl
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		String fileNameRead1 = "strings-aviadHe.xml";
		String fileNameRead2 = "strings-aviadEn.xml";
		String fileToWrite = "check.txt";
		String writeReady = "";
		String readString1 = "";
		String readString2 = "";
		Boolean found = false;
		String Prefix = "<string name=" + '"';

		BufferedReader fRead1 = null;
		BufferedReader fRead2 = null;
		BufferedWriter fWritefound = null;

		try {
			fRead1 = new BufferedReader(new InputStreamReader(new FileInputStream(fileNameRead1),"UTF8"));
			fWritefound = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileToWrite),"UTF8"));
			while ((readString1 =  fRead1.readLine()) != null){
				found = false;
				readString1 = readString1.trim();
				if (readString1.startsWith(Prefix)){
					writeReady = readString1.substring(readString1.indexOf('"' + ">")+2, readString1.indexOf("</string>"));
					readString1 =  readString1.substring(Prefix.length(), readString1.indexOf(">")-1);

					fRead2 = new BufferedReader(new InputStreamReader(new FileInputStream(fileNameRead2),"UTF8"));
					while((readString2 = fRead2.readLine()) != null && !found){
						readString2 = readString2.trim();
						if(readString2.startsWith(Prefix)){
							readString2 =  readString2.substring(Prefix.length(), readString2.indexOf(">")-1);
							if(readString1.equals(readString2)){
								found = true;
							}
						}
					}
					if(!found){
						fWritefound.write(readString1 + "=" + writeReady);
						fWritefound.newLine();
					}
					fRead2.close();
				}
			}
			fRead1.close();
			fWritefound.close();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
