package Reading;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class File_Reader {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int i=0;
		int j=0;
		int k=0;
		String fileNameEn = "strings-en.xml";
		String fileNameMRCheck = "messages.properties";
		String fileWriteFound = "strings-he.xml";
		String fileWriteNotFound = "strings-he-not-found.xml";
		String readStringEn = "";
		String readStringMR = "";
		String writeReady = "";
		Boolean found = false;
		String Prefix = "<string name=" + '"';

		/* readers & writers*/
		BufferedReader fRead1 = null;
		BufferedReader fRead2 = null;
		BufferedWriter fWriteNotfound = null;
		BufferedWriter fWritefound = null;
		String[] keys = new String[1024];
		String[] keysFound = new String[1024];
		String[] keysNotFound = new String[1024];

		try {
			 fRead1 = new BufferedReader(new InputStreamReader(new FileInputStream(fileNameEn),"UTF8"));
			///reading from fread1
			while ((readStringEn =  fRead1.readLine()) != null){
				readStringEn = readStringEn.trim();
				found = false;
				if (readStringEn.startsWith(Prefix)){
					keys[i]=readStringEn.substring(Prefix.length(), readStringEn.indexOf(">")-1);
					fRead2 = new BufferedReader(new InputStreamReader(new FileInputStream(fileNameMRCheck),"UTF8"));
					while(!found && (readStringMR = fRead2.readLine())!= null){
						readStringMR = readStringMR.trim();
						writeReady = keys[i] +  "=";
						if(readStringMR.startsWith(writeReady)){
							keysFound[j] = Prefix + keys[i] + '"'+">" + readStringMR.substring(readStringMR.indexOf("=")+1) + "</string>";
							found = true;
							j++;
						}else{
							writeReady = keys[i];
							if (readStringMR.startsWith(writeReady+ " ")){
								keysFound[j] = Prefix + keys[i] + '"'+">" + readStringMR.substring(readStringMR.indexOf("=")+1) + "</string>";
								found = true;
								j++;
							}
						}
					}
					if (!found){
						keysNotFound[k] = readStringEn;
						k++;
					}
					i++;
				}
			}
			fRead1.close();
			fRead2.close();

			///writing to files
			fWritefound = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileWriteFound),"UTF8"));
			fWriteNotfound = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileWriteNotFound),"UTF8"));
			j=0;
			k=0;
			writeReady = "<?xml version=" + '"' + "1.0" + '"' + " encoding=" + '"' + "utf-8" + '"' + "?>";

			fWritefound.write(writeReady);
			fWritefound.newLine();
			fWritefound.write("<resources>");
			fWritefound.newLine();
			while (keysFound[j] != null){
				fWritefound.write(keysFound[j]);
				fWritefound.newLine();
				j++;
			}
			fWritefound.write("</resources>");
			fWritefound.close();
			fWriteNotfound.write(writeReady);
			fWriteNotfound.newLine();
			fWriteNotfound.write("<resources>");
			fWriteNotfound.newLine();
			while(keysNotFound[k] != null){
				fWriteNotfound.write(keysNotFound[k]);
				fWriteNotfound.newLine();
				k++;
			}
			fWriteNotfound.write("</resources>");
			fWriteNotfound.close();

			System.out.println("Completed Checking");

		}
			 catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
