package Reading;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

public class messageResourcer {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args){
		// TODO Auto-generated method stub
		
		String readFile1 = "MessageResources_en.properties";
		String readFile2 = "messages.properties";
		String readLine1 = "";
		String readLine2 = "";
		String writeFile = "messageResources_he.txt";
		String buffer = "";
		boolean found= false;
		String equal = "=";
		
		BufferedReader fRead1 = null;
		BufferedReader fRead2 = null;
		BufferedWriter fWrite = null;
		
		try{
			fRead1 = new BufferedReader(new InputStreamReader(new FileInputStream(readFile1),"UTF8"));
			fWrite = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(writeFile),"UTF8"));
			while((readLine1 = fRead1.readLine())!=null){
				readLine1 = readLine1.trim();
				if(readLine1.indexOf(equal)>-1){
					fRead2 = new BufferedReader(new InputStreamReader(new FileInputStream(readFile2),"UTF8"));
					found = false;
					while((readLine2 = fRead2.readLine())!=null && !found){
						if(readLine2.startsWith(readLine1.substring(0, readLine1.indexOf(equal)).trim())){
							buffer = readLine1.substring(0, readLine1.indexOf(equal)).trim() + equal + readLine2.substring(readLine2.indexOf(equal)).trim();
							found = true;
						}						
					}
					if(!found){
						buffer = readLine1.trim();
					}
					fRead2.close();
					fWrite.write(buffer);
					fWrite.newLine();
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally{
			try {
				fRead1.close();
				fWrite.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			System.out.println("finish resource");
		}
	}
}
