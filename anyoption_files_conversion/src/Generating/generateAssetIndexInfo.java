package Generating;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

public class generateAssetIndexInfo {

	/*  language id
	1    iw    languages.heb
	2    en    languages.eng
	3    tr    languages.tur
	4    ru    languages.rus
	5    es    languages.spa
	6    it    languages.ita
	7    ar    languages.ar
	8    de    languages.de
	9    fr    languages.fr
	*/

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		boolean found = false;
		String buffer = "";
		String readyToWrite = "";
		String keyWordLookUp = "";
		String fileRead = "assetIndex.xhtml";
		String language_id = "10";
		String fileKeyWords = "KeyWords2.txt";
		String filewrite = "zh.txt";
		String prefix = "<h:panelGroup rendered=" + '"' + "#{market.id==";				
		//String prefix = "<h:panelGroup rendered=" + '"' + "#{marketDetails.market.id==";

		BufferedReader fReadBuffer = null;
		BufferedReader fReadKeyWord = null;
		BufferedWriter fWriteBuffer = null;

		try {
			fReadBuffer = new BufferedReader(new InputStreamReader(new FileInputStream(fileRead),"UTF8"));
			fWriteBuffer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filewrite),"UTF8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

		while((buffer = fReadBuffer.readLine()) != null){
			buffer = buffer.trim();
			if(buffer.startsWith(prefix)){
				int counter = 5 ;
				while(counter > 0){
					String temp = fReadBuffer.readLine().trim();
					if(temp.startsWith("<table")){
						counter = 0;
					}
					System.out.println(temp);
					
				}
				found = false;
				readyToWrite = buffer.substring(prefix.length(), buffer.indexOf("}"));
				fReadKeyWord = new BufferedReader(new InputStreamReader(new FileInputStream(fileKeyWords),"UTF8"));
				while (!found && (keyWordLookUp = fReadKeyWord.readLine())!= null ){
					if(keyWordLookUp.startsWith(readyToWrite)){
						keyWordLookUp = keyWordLookUp.substring(keyWordLookUp.indexOf("m"));
						found = true ;
						readyToWrite ="insert into translations (language_id,key,value) values (" +
						language_id + " , '"+ keyWordLookUp + "' , '" + fReadBuffer.readLine().trim()+ "');";


							/*"UPDATE " + '"' + "main" + '"' + "." + '"' +"translations" +
									'"' + " SET " + '"' + "value" + '"' + "= '" + fReadBuffer.readLine().trim() + "' WHERE " + '"' +
									"language_id" +'"' + " ='" + language_id +"' And " + '"' + "key" + '"' +
									"='" +  keyWordLookUp + "';";*/


						fWriteBuffer.write(readyToWrite);
						fWriteBuffer.newLine();
					}
				}
			}
		}
		
		
//		while((buffer = fReadBuffer.readLine()) != null){
//			buffer = buffer.trim();
//			if(buffer.startsWith(prefix)){
//				found = false;
//				readyToWrite = buffer.substring(prefix.length(), buffer.indexOf("}"));
//				fReadKeyWord = new BufferedReader(new InputStreamReader(new FileInputStream(fileKeyWords),"UTF8"));
//				while (!found && (keyWordLookUp = fReadKeyWord.readLine())!= null ){
//					if(keyWordLookUp.startsWith(readyToWrite)){
//						keyWordLookUp = keyWordLookUp.substring(keyWordLookUp.indexOf("m"));
//						found = true ;
//						readyToWrite ="insert into translations (language_id,key,value) values (" +
//						language_id + " , '"+ keyWordLookUp + "' , '" + fReadBuffer.readLine().trim()+ "');";
//
//
//							/*"UPDATE " + '"' + "main" + '"' + "." + '"' +"translations" +
//									'"' + " SET " + '"' + "value" + '"' + "= '" + fReadBuffer.readLine().trim() + "' WHERE " + '"' +
//									"language_id" +'"' + " ='" + language_id +"' And " + '"' + "key" + '"' +
//									"='" +  keyWordLookUp + "';";*/
//
//
//						fWriteBuffer.write(readyToWrite);
//						fWriteBuffer.newLine();
//					}
//				}
//			}
//		}
		fReadBuffer.close();
		fWriteBuffer.close();
		fReadKeyWord.close();
		System.out.println("completed Generating");

	}

}


