package NewMobileApp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class TextGeneration {
	private static final Logger log = Logger.getLogger(TextGeneration.class);

	public static void main(String[] args) {

		mapTransfer("C:/Users/ivannp/workspaceSVN/etrader_web/res_AnyOption/MessageResources_de.properties", "C:/templates/new_de.txt", "C:/templates/web.txt", "C:/templates/supernew_IOS_de.txt", "de");
		mapTransfer("C:/Users/ivannp/workspaceSVN/etrader_web/res_AnyOption/MessageResources_en.properties", "C:/templates/new_en.txt", "C:/templates/web.txt", "C:/templates/supernew_IOS_en.txt", "en");
		mapTransfer("C:/Users/ivannp/workspaceSVN/etrader_web/res_AnyOption/MessageResources_es.properties", "C:/templates/new_es.txt", "C:/templates/web.txt", "C:/templates/supernew_IOS_es.txt", "es");
		mapTransfer("C:/Users/ivannp/workspaceSVN/etrader_web/res_AnyOption/MessageResources_fr.properties", "C:/templates/new_fr.txt", "C:/templates/web.txt", "C:/templates/supernew_IOS_fr.txt", "fr");
		mapTransfer("C:/Users/ivannp/workspaceSVN/etrader_web/res_AnyOption/MessageResources_it.properties", "C:/templates/new_it.txt", "C:/templates/web.txt", "C:/templates/supernew_IOS_it.txt", "it");
		//mapTransfer("C:/Users/ivannp/workspaceSVN/etrader_web/res_AnyOption/MessageResources_ru.properties", "C:/Users/ivannp/workspaceSVN/etrader_android/res/values-ru/strings.xml", "C:/templates/new_ru.txt", "C:/templates/web.txt", "C:/templates/android.txt", "ru");
//		mapTransfer("C:/Users/ivannp/workspaceSVN/etrader_web/res/MessageResources_iw.properties", "C:/Users/ivannp/workspaceSVN/etrader_android_app/res/values/strings.xml", "C:/templates/new_iw.txt", "C:/templates/web.txt", "C:/templates/android.txt", "iw");
	}
	
	public static void mapTransfer(String bundleFile, String newFile, String webFile, String iosSpecific, String lang){
		getKeyMap(bundleFile, newFile, webFile, iosSpecific, true, lang);
		getKeyMap(bundleFile, newFile, webFile, iosSpecific, false, lang);
	}
	
	public static void getKeyMap(String bundleFile, String newFile, String webFile, String iosSpecific, boolean forAndroid, String lang) {

		String replace1 = "\\%";
		String replace2 = "\\$s";
		String replace3 = "[%]{1}(\\d+)[$]{1}[@]{1}|[%]{1}[@]{1}";
		if (!forAndroid) {
			replace2 = "\\$@";
			replace3 = "[%]{1}(\\d+)[$]{1}[s]{1}|[%]{1}[s]{1}";
		}
		HashMap<String, String> androidMap = new HashMap<String, String>();
		HashMap<String, String> iosMap = new HashMap<String, String>();
		Properties props = new Properties();
		Properties props2 = new Properties();
		Properties props3 = new Properties();
		InputStream stream = null;
		InputStream stream2 = null;
		InputStream stream3 = null;
		BufferedReader inWeb = null;

		try {
			
			List<String> myListWeb = new ArrayList<String>();
			inWeb = new BufferedReader(new FileReader(webFile));
		    String str;
		    while ((str = inWeb.readLine()) != null) {
		    	myListWeb.add(str);
		    }
		    
		    String valueBundle = "";
		    String keyNewWebBundleCapital = "";
			stream = new FileInputStream(bundleFile);
			props.load(new InputStreamReader(stream, "UTF-8"));
			for (String keyNewWebBundle : myListWeb) {
				if (keyNewWebBundle.endsWith(".CAPITAL")) {
					keyNewWebBundleCapital = keyNewWebBundle.substring(0, (keyNewWebBundle.length()-8));
					valueBundle = props.getProperty(keyNewWebBundleCapital);
				} else if (keyNewWebBundle.endsWith(".FCAPITAL")) {
					keyNewWebBundleCapital = keyNewWebBundle.substring(0, (keyNewWebBundle.length()-9));
					valueBundle = props.getProperty(keyNewWebBundleCapital);
				} else if (keyNewWebBundle.endsWith(".COLON")) {
					keyNewWebBundleCapital = keyNewWebBundle.substring(0, (keyNewWebBundle.length()-6));
					valueBundle = props.getProperty(keyNewWebBundleCapital);
				} else if (keyNewWebBundle.endsWith(".ARROW")) {
					keyNewWebBundleCapital = keyNewWebBundle.substring(0, (keyNewWebBundle.length()-6));
					valueBundle = props.getProperty(keyNewWebBundleCapital);
				} else if (keyNewWebBundle.endsWith(".NOCOLON")) {
					keyNewWebBundleCapital = keyNewWebBundle.substring(0, (keyNewWebBundle.length()-8));
					valueBundle = props.getProperty(keyNewWebBundleCapital);
				} else if (keyNewWebBundle.endsWith(".DOT")) {
					keyNewWebBundleCapital = keyNewWebBundle.substring(0, (keyNewWebBundle.length()-4));
					valueBundle = props.getProperty(keyNewWebBundleCapital);
				} else {
					valueBundle = props.getProperty(keyNewWebBundle);
				}
				if(valueBundle != null) {
					StringBuffer sb = new StringBuffer();
					Pattern regex = Pattern.compile("[{]{1}(\\d+)[}]{1}");
					Matcher matcher = regex.matcher(valueBundle);
					while(matcher.find()){
						int param = Integer.parseInt(matcher.group(1));
						++param;
						matcher.appendReplacement(sb, replace1 + param + replace2);
					}
					matcher.appendTail(sb);
					valueBundle = sb.toString();
					if (keyNewWebBundle.endsWith(".CAPITAL")) {
						valueBundle = valueBundle.toUpperCase();
						keyNewWebBundle = keyNewWebBundle.substring(0, (keyNewWebBundle.length()-8));
					} else if (keyNewWebBundle.endsWith(".FCAPITAL")) {
						String capital = Character.toString(valueBundle.charAt(0)).toUpperCase();
						valueBundle = capital + valueBundle.substring(1, valueBundle.length());
						keyNewWebBundle = keyNewWebBundle.substring(0, (keyNewWebBundle.length()-9));
					} else if (keyNewWebBundle.endsWith(".COLON")) {
						valueBundle = valueBundle.substring(0, (valueBundle.length()-1));
						keyNewWebBundle = keyNewWebBundle.substring(0, (keyNewWebBundle.length()-6));
					} else if (keyNewWebBundle.endsWith(".ARROW")) {
						valueBundle = valueBundle.substring(0, (valueBundle.length()-1));
						keyNewWebBundle = keyNewWebBundle.substring(0, (keyNewWebBundle.length()-6));
					} else if (keyNewWebBundle.endsWith(".NOCOLON")) {
						valueBundle = valueBundle + ":";
						keyNewWebBundle = keyNewWebBundle.substring(0, (keyNewWebBundle.length()-8));
					} else if (keyNewWebBundle.endsWith(".DOT")) {
						if (valueBundle.endsWith(".")) {
							valueBundle = valueBundle.substring(0, (valueBundle.length()-1));
						}
					}
					valueBundle = valueBundle.replace("\"", "\\\"");
					androidMap.put(keyNewWebBundle, valueBundle);
					iosMap.put(keyNewWebBundle, valueBundle);
				}
			}

	        
	        stream2 = new FileInputStream(newFile);
	        stream3 = new FileInputStream(iosSpecific);
			props2.load(new InputStreamReader(stream2, "UTF-8"));
			props3.load(new InputStreamReader(stream3, "UTF-8"));
			Set<Object> specKeys = props3.keySet();
			for(String keyNewFileBundle : props2.stringPropertyNames()) {
				String valueNewFileBundle="";
				if (specKeys.contains(keyNewFileBundle) && !forAndroid){
					valueNewFileBundle = props3.getProperty(keyNewFileBundle);
				} else {
					if (keyNewFileBundle.endsWith(".COPYOPREPLACE")){
						keyNewFileBundle = keyNewFileBundle.substring(0, keyNewFileBundle.length()-14);
						valueNewFileBundle = props2.getProperty(keyNewFileBundle);
						valueNewFileBundle = valueNewFileBundle.replace("anyoption", "copyop");
					} else {
						valueNewFileBundle = props2.getProperty(keyNewFileBundle);
					}
				}
				if (valueNewFileBundle != null) {
					StringBuffer sb2 = new StringBuffer();
					Pattern regex = Pattern.compile(replace3);
					Matcher matcher = regex.matcher(valueNewFileBundle);
					while(matcher.find()){
						if(matcher.group(1) != null){
							int param = Integer.parseInt(matcher.group(1));
							matcher.appendReplacement(sb2, replace1 + param + replace2);
						} else if(matcher.group(2) != null){
							int param = Integer.parseInt(matcher.group(2));
							matcher.appendReplacement(sb2, replace1 + param + replace2);
						}else {
							matcher.appendReplacement(sb2, replace1 + 1 + replace2);
						}
					}
					matcher.appendTail(sb2);
					valueNewFileBundle = sb2.toString();
					valueNewFileBundle = valueNewFileBundle.replace("\"", "\\\"");
					valueNewFileBundle = valueNewFileBundle.replace("\n", "\\n");
					if(forAndroid){
						androidMap.put(keyNewFileBundle, valueNewFileBundle);
					} else {
						iosMap.put(keyNewFileBundle, valueNewFileBundle);
					}
				}
			}
			
		} catch (FileNotFoundException fnfe) {
			log.error("FNF", fnfe);	
		} catch (IOException ioe) {
        	log.error("IO", ioe);	
		} catch (Exception e) {
        	log.error("Exception", e);
		} finally {
        	try {
				stream.close();
			} catch (IOException ioe) {
				log.error("Can't close.", ioe);	
			}
        	try {
				stream2.close();
			} catch (IOException ioe) {
				log.error("Can't close.", ioe);	
			}
        	try {
				inWeb.close();
			} catch (IOException ioe) {
				log.error("Can't close.", ioe);	
			}
        }
		
		mapToXmlOrBundle(androidMap, iosMap, forAndroid, lang);
	}
	
	
	public static void mapToXmlOrBundle(HashMap<String, String> mapAndroid, HashMap<String, String> mapIOS, boolean xml, String lang) {
		if(!xml) {
			try {
				File file = new File("C:/templates/test_" + lang + ".txt");
				Writer out = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(file), "UTF8"));
				for (Map.Entry<String, String> entry : mapIOS.entrySet()) {
				    String key = "\"" + entry.getKey() + "\"";
				    String value = "\"" + entry.getValue() + "\";";
				    value = value.replace("% ", "%% ");
				    value = value.replace("%.", "%%.");
				    out.append(key).append('=').append(value).append("\r\n");
				}
				out.flush();
				out.close();
			} catch (FileNotFoundException fnfe) {
				log.error("Cannot export to properties file", fnfe);
			} catch (IOException ioe) {
				log.error("Error with IO", ioe);
			}
		} else {
			try {
				DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				Document doc = docBuilder.newDocument();
				Element rootElement = doc.createElement("resources");
				doc.appendChild(rootElement);
				
				for (Map.Entry<String, String> entry : mapAndroid.entrySet()) {
					String key =entry.getKey();
				    String value =entry.getValue();
				    value = value.replace("\'", "\\\'");
				    value = value.replace("% ", "%% ");
				    value = value.replaceAll("%$", "%%");
				    value = value.replace("%.", "%%.");
				    value = value.replace("\\\\'", "\\'");
					Element stringNode = doc.createElement("string");
					rootElement.appendChild(stringNode);
					Attr attr = doc.createAttribute("name");
					attr.setValue(key);
					stringNode.setAttributeNode(attr);
					stringNode.appendChild(doc.createTextNode(value));
				}
				
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(new File("C:/templates/testXML_" + lang + ".xml"));
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
				transformer.transform(source, result);
		 
			  } catch (ParserConfigurationException pce) {
				  log.error("", pce);
			  } catch (TransformerException tfe) {
				  log.error("", tfe);
			  }
		}
	}

}
