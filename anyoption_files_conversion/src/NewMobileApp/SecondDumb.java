package NewMobileApp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class SecondDumb {

	private static final Logger log = Logger.getLogger(SecondDumb.class);
	
	public static void main(String[] args) {

		//mapTransfer("C:/Users/ivannp/workspaceSVN/etrader_web/res_AnyOption/MessageResources_de.properties", "C:/Users/ivannp/workspaceSVN/etrader_android/res/values-de/strings.xml", "C:/templates/new_de.txt", "C:/templates/web.txt", "C:/templates/android.txt", "de");
		//mapTransfer("C:/Users/ivannp/workspaceSVN/etrader_web/res_AnyOption/MessageResources_en.properties", "C:/Users/ivannp/workspaceSVN/etrader_android/res/values/strings.xml", "C:/templates/new_en.txt", "C:/templates/web.txt", "C:/templates/android.txt", "en");
		//mapTransfer("C:/Users/ivannp/workspaceSVN/etrader_web/res_AnyOption/MessageResources_es.properties", "C:/Users/ivannp/workspaceSVN/etrader_android/res/values-es/strings.xml", "C:/templates/new_es.txt", "C:/templates/web.txt", "C:/templates/android.txt", "es");
		//mapTransfer("C:/Users/ivannp/workspaceSVN/etrader_web/res_AnyOption/MessageResources_fr.properties", "C:/Users/ivannp/workspaceSVN/etrader_android/res/values-fr/strings.xml", "C:/templates/new_fr.txt", "C:/templates/web.txt", "C:/templates/android.txt", "fr");
		//mapTransfer("C:/Users/ivannp/workspaceSVN/etrader_web/res_AnyOption/MessageResources_it.properties", "C:/Users/ivannp/workspaceSVN/etrader_android/res/values-it/strings.xml", "C:/templates/new_it.txt", "C:/templates/web.txt", "C:/templates/android.txt", "it");
		//mapTransfer("C:/Users/ivannp/workspaceSVN/etrader_web/res_AnyOption/MessageResources_ru.properties", "C:/Users/ivannp/workspaceSVN/etrader_android/res/values-ru/strings.xml", "C:/templates/new_ru.txt", "C:/templates/web.txt", "C:/templates/android.txt", "ru");
		mapTransfer("C:/Users/ivannp/workspaceSVN/etrader_web/res/MessageResources_iw.properties", "C:/Users/ivannp/workspaceSVN/etrader_android_app/res/values/strings.xml", "C:/templates/new_iw.txt", "C:/templates/web.txt", "C:/templates/android.txt", "iw");
	}
	
	public static void mapTransfer(String bundleFile, String androidFile, String newFile, String webFile, String androidCompareFile, String lang){
		getKeyMap(bundleFile, androidFile, newFile, webFile, androidCompareFile, true, lang);
		getKeyMap(bundleFile, androidFile, newFile, webFile, androidCompareFile, false, lang);
	}
	
	public static HashMap<String, String> countriesToMap(String lang){
		HashMap<String, String> countryMap = new HashMap<String, String>();
		InputStream stream2 = null;
		Properties props2 = new Properties();
		boolean matches = false;
		try {
			 stream2 = new FileInputStream("C:/Users/ivannp/workspaceSVN/etrader_web/res/MessageResources_" + lang + ".properties");
				props2.load(new InputStreamReader(stream2, "UTF-8"));
				for(String key : props2.stringPropertyNames()) {
					String value = props2.getProperty(key);
					Pattern regex = Pattern.compile("(countries.)[a-zA-z]{3}");
					Matcher matcher = regex.matcher(key);
					while(matcher.find()){
						matches = true;
					}
					if(matches){
						countryMap.put(key, value);
					}
					matches = false;
				}
		} catch (FileNotFoundException fnfe) {
			log.error("FNF", fnfe);	
		} catch (IOException ioe) {
        	log.error("IO", ioe);	
		} catch (Exception e) {
        	log.error("Exception", e);
		} finally {
			
		}
		
		return countryMap;
	}
	
	public static void getKeyMap(String bundleFile, String androidFile, String newFile, String webFile, String androidCompareFile, boolean forAndroid, String lang) {

		String replace1 = "\\%";
		String replace2 = "\\$s";
		if (!forAndroid) {
			replace2 = "\\$@";
		}	
		HashMap<String, String> bundleMap = new HashMap<String, String>();
		HashMap<String, String> xmlMap = new HashMap<String, String>();
		Properties props = new Properties();
		Properties props2 = new Properties();
		InputStream stream = null;
		InputStream stream2 = null;
		BufferedReader in = null;
		BufferedReader inWeb = null;

		try {
			
			List<String> myListWeb = new ArrayList<String>();
			inWeb = new BufferedReader(new FileReader(webFile));
		    String str;
		    while ((str = inWeb.readLine()) != null) {
		    	myListWeb.add(str);
		    }
		    
		    String valueBundle = "";
		    String keyNewWebBundleCapital = "";
			stream = new FileInputStream(bundleFile);
			props.load(new InputStreamReader(stream, "UTF-8"));
			for (String keyNewWebBundle : myListWeb) {
				if (keyNewWebBundle.endsWith(".CAPITAL")) {
					keyNewWebBundleCapital = keyNewWebBundle.substring(0, (keyNewWebBundle.length()-8));
					valueBundle = props.getProperty(keyNewWebBundleCapital);
				} else if (keyNewWebBundle.endsWith(".FCAPITAL")) {
					keyNewWebBundleCapital = keyNewWebBundle.substring(0, (keyNewWebBundle.length()-9));
					valueBundle = props.getProperty(keyNewWebBundleCapital);
				} else if (keyNewWebBundle.endsWith(".COLON")) {
					keyNewWebBundleCapital = keyNewWebBundle.substring(0, (keyNewWebBundle.length()-6));
					valueBundle = props.getProperty(keyNewWebBundleCapital);
				} else if (keyNewWebBundle.endsWith(".ARROW")) {
					keyNewWebBundleCapital = keyNewWebBundle.substring(0, (keyNewWebBundle.length()-6));
					valueBundle = props.getProperty(keyNewWebBundleCapital);
				} else if (keyNewWebBundle.endsWith(".NOCOLON")) {
					keyNewWebBundleCapital = keyNewWebBundle.substring(0, (keyNewWebBundle.length()-8));
					valueBundle = props.getProperty(keyNewWebBundleCapital);
				} else if (keyNewWebBundle.endsWith(".DOT")) {
					keyNewWebBundleCapital = keyNewWebBundle.substring(0, (keyNewWebBundle.length()-4));
					valueBundle = props.getProperty(keyNewWebBundleCapital);
				} else {
					valueBundle = props.getProperty(keyNewWebBundle);
				}
				if(valueBundle != null) {
					StringBuffer sb = new StringBuffer();
					Pattern regex = Pattern.compile("[{]{1}(\\d+)[}]{1}");
					Matcher matcher = regex.matcher(valueBundle);
					while(matcher.find()){
						int param = Integer.parseInt(matcher.group(1));
						++param;
						matcher.appendReplacement(sb, replace1 + param + replace2);
					}
					matcher.appendTail(sb);
					valueBundle = sb.toString();
					if (keyNewWebBundle.endsWith(".CAPITAL")) {
						valueBundle = valueBundle.toUpperCase();
						keyNewWebBundle = keyNewWebBundle.substring(0, (keyNewWebBundle.length()-8));
					} else if (keyNewWebBundle.endsWith(".FCAPITAL")) {
						String capital = Character.toString(valueBundle.charAt(0)).toUpperCase();
						valueBundle = capital + valueBundle.substring(1, valueBundle.length());
						keyNewWebBundle = keyNewWebBundle.substring(0, (keyNewWebBundle.length()-9));
					} else if (keyNewWebBundle.endsWith(".COLON")) {
						valueBundle = valueBundle.substring(0, (valueBundle.length()-1));
						keyNewWebBundle = keyNewWebBundle.substring(0, (keyNewWebBundle.length()-6));
					} else if (keyNewWebBundle.endsWith(".ARROW")) {
						valueBundle = valueBundle.substring(0, (valueBundle.length()-1));
						keyNewWebBundle = keyNewWebBundle.substring(0, (keyNewWebBundle.length()-6));
					} else if (keyNewWebBundle.endsWith(".NOCOLON")) {
						valueBundle = valueBundle + ":";
						keyNewWebBundle = keyNewWebBundle.substring(0, (keyNewWebBundle.length()-8));
					} else if (keyNewWebBundle.endsWith(".DOT")) {
						if (valueBundle.endsWith(".")) {
							valueBundle = valueBundle.substring(0, (valueBundle.length()-1));
						}
					}
					valueBundle = valueBundle.replace("\"", "\\\"");
					bundleMap.put(keyNewWebBundle, valueBundle);
				}
			}
			
			List<String> myList = new ArrayList<String>();
		    in = new BufferedReader(new FileReader(androidCompareFile));
		    String str2;
		    while ((str2 = in.readLine()) != null) {
		        myList.add(str2);
		    }
		    
	        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	        Document doc = docBuilder.parse(androidFile);
	        NodeList mapping = doc.getElementsByTagName("string");
	        for (int j = 0; j < mapping.getLength(); j++) {
	        	Element eOld = (Element) mapping.item(j);
	        	String nameAttr = eOld.getAttribute("name");
	        	if (nameAttr != null) {
	        		String androidValue = eOld.getTextContent();
	        		xmlMap.put(nameAttr, androidValue);
	        	}
	        }
	        String androidNewKey = "";
	        String androidNewValue = "";
	        for (String e : myList) {
	        	if (e.endsWith(".NOCOLON")){
	        		androidNewKey = e.substring(0, (e.length()-8));
	        		androidNewValue = xmlMap.get(androidNewKey);
	        	} else if (e.endsWith(".FCAPITAL")) {
	        		androidNewKey = e.substring(0, (e.length()-9));
	        		androidNewValue = xmlMap.get(androidNewKey);
	        	}else {
	        		androidNewValue = xmlMap.get(e);
	        	}
	        		if (androidNewValue != null) {
	        			StringBuffer sb3 = new StringBuffer();
	        			Pattern regex = Pattern.compile("[%]{1}(\\d+)[$]{1}[s]{1}");
	        			Matcher matcher = regex.matcher(androidNewValue);
	        			while(matcher.find()){
	        				if(matcher.group(1) != null){
	        					int param = Integer.parseInt(matcher.group(1));
	        					matcher.appendReplacement(sb3, replace1 + param + replace2);
	        				} else {
	        					matcher.appendReplacement(sb3, replace1 + 1 + replace2);
	        				}
	        			}
	        			matcher.appendTail(sb3);
	        			androidNewValue = sb3.toString();
	        			androidNewValue = androidNewValue.replace("\"", "\\\"");
	        			if (e.endsWith(".NOCOLON")){
	        				e = e.substring(0, (e.length()-8));
	        				androidNewValue = androidNewValue + ":";
	        			} else if (e.endsWith(".FCAPITAL")) {
	        				String capital = Character.toString(androidNewValue.charAt(0)).toUpperCase();
	        				androidNewValue = capital + androidNewValue.substring(1, androidNewValue.length());
	        				e = e.substring(0, (e.length()-9));
	        			}
	        			bundleMap.put(e, androidNewValue);
	        		}
	        }
	        
	        stream2 = new FileInputStream(newFile);
			props2.load(new InputStreamReader(stream2, "UTF-8"));
			for(String keyNewFileBundle : props2.stringPropertyNames()) {
				String valueNewFileBundle = props2.getProperty(keyNewFileBundle);
				if (valueNewFileBundle != null) {
					StringBuffer sb2 = new StringBuffer();
					Pattern regex = Pattern.compile("[%]{1}(\\d+)[$]{1}[@]{1}|[%]{1}[@]{1}");
					Matcher matcher = regex.matcher(valueNewFileBundle);
					while(matcher.find()){
						if(matcher.group(1) != null){
							int param = Integer.parseInt(matcher.group(1));
							matcher.appendReplacement(sb2, replace1 + param + replace2);
						} else {
							matcher.appendReplacement(sb2, replace1 + 1 + replace2);
						}
					}
					matcher.appendTail(sb2);
					valueNewFileBundle = sb2.toString();
					valueNewFileBundle = valueNewFileBundle.replace("\"", "\\\"");
					bundleMap.put(keyNewFileBundle, valueNewFileBundle);
				}
			}
			
		} catch (FileNotFoundException fnfe) {
			log.error("FNF", fnfe);	
		} catch (IOException ioe) {
        	log.error("IO", ioe);	
		} catch (Exception e) {
        	log.error("Exception", e);
		} finally {
        	try {
				stream.close();
			} catch (IOException ioe) {
				log.error("Can't close.", ioe);	
			}
        	try {
				stream2.close();
			} catch (IOException ioe) {
				log.error("Can't close.", ioe);	
			}
        	try {
				in.close();
			} catch (IOException ioe) {
				log.error("Can't close.", ioe);	
			}
        	try {
				inWeb.close();
			} catch (IOException ioe) {
				log.error("Can't close.", ioe);	
			}
        }
		
		mapToXmlOrBundle(bundleMap, forAndroid, countriesToMap(lang), lang);
	}
	
	
	public static void mapToXmlOrBundle(HashMap<String, String> map, boolean xml, HashMap<String, String> mapCountries, String lang) {
		if(!xml) {
			try {
				File file = new File("C:/templates/test_" + lang + ".txt");
				Writer out = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(file), "UTF8"));
				for (Map.Entry<String, String> entry : map.entrySet()) {
				    String key = "\"" + entry.getKey() + "\"";
				    String value = "\"" + entry.getValue() + "\";";
				    out.append(key).append('=').append(value).append("\r\n");
				}
				for (Map.Entry<String, String> entry : mapCountries.entrySet()) {
				    String key = "\"" + entry.getKey() + "\"";
				    String value = "\"" + entry.getValue() + "\";";
				    out.append(key).append('=').append(value).append("\r\n");
				}
				out.flush();
				out.close();
			} catch (FileNotFoundException fnfe) {
				log.error("Cannot export to properties file", fnfe);
			} catch (IOException ioe) {
				log.error("Error with IO", ioe);
			}
		} else {
			try {
				DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				Document doc = docBuilder.newDocument();
				Element rootElement = doc.createElement("resources");
				doc.appendChild(rootElement);
				
				for (Map.Entry<String, String> entry : map.entrySet()) {
					String key =entry.getKey();
				    String value =entry.getValue();
				    value = value.replace("\'", "\\\'");
				    value = value.replace("% ", "%% ");
				    value = value.replaceAll("%$", "%%");
				    value = value.replace("%.", "%%.");
				    value = value.replace("\\\\'", "\\'");
					Element stringNode = doc.createElement("string");
					rootElement.appendChild(stringNode);
					Attr attr = doc.createAttribute("name");
					attr.setValue(key);
					stringNode.setAttributeNode(attr);
					stringNode.appendChild(doc.createTextNode(value));
				}
				
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(new File("C:/templates/testXML_" + lang + ".xml"));
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
				transformer.transform(source, result);
		 
			  } catch (ParserConfigurationException pce) {
				  log.error("", pce);
			  } catch (TransformerException tfe) {
				  log.error("", tfe);
			  }
		}
	}
	
}
