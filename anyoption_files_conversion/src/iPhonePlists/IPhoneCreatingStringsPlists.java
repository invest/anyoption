package iPhonePlists;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;


public class IPhoneCreatingStringsPlists {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String fileName = "strings-it.xml";
		String fileWrite = "strings_it.plist";
		String readString = "";
		String writeReady = "";
		String PrefixKey = "<string name=" + '"';
		String PrefixKeyArray = "<string-array name=" + '"';
		String prefixArrayItem = "<item>";
		String prefixKeyEnd = ">";
		int index1 = 0;
		int index2 = 0;
		
		/* readers & writers*/
		BufferedReader fRead = null;
		BufferedWriter fWrite = null;

		try {
			fRead = new BufferedReader(new InputStreamReader(new FileInputStream(fileName),"UTF8"));
			fWrite = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileWrite),"UTF8"));
			writeReady = "<?xml version=" + '"' + "1.0" + '"' + " encoding=" + '"' + "utf-8" + '"' + "?>";
			fWrite.write(writeReady);
			fWrite.newLine();
			writeReady = "<!DOCTYPE plist PUBLIC " + '"' + "-//Apple//DTD PLIST 1.0//EN" + '"' + '"' + "http://www.apple.com/DTDs/PropertyList-1.0.dtd>" ;
			fWrite.write(writeReady);
			fWrite.newLine();
			writeReady = "<plist version=" + '"' + "1.0" + '"' + ">";
			fWrite.write(writeReady);
			fWrite.newLine();
			fWrite.write("<dict>");
			fWrite.newLine();
			
			///reading from fread
			while ((readString =  fRead.readLine()) != null){
				readString = readString.trim();
				if (readString.startsWith(PrefixKey)){
					System.out.println(readString);
					index1 = readString.indexOf(prefixKeyEnd)-1;
					writeReady = "	<key>" + readString.substring(PrefixKey.length(),index1)+ "</key>";
					fWrite.write(writeReady);
					fWrite.newLine();
					index1 = readString.indexOf("</string");
					index2 = readString.indexOf('"' + ">")+2;
					if(index2 < 0){
						index2 = readString.indexOf('"' + " >")+2;
					}
					writeReady = "	<string>" + readString.substring(index2, index1) + "</string>";
					fWrite.write(writeReady);
					fWrite.newLine();
				} else if (readString.startsWith(PrefixKeyArray)){
					
					writeReady = "	<key>" + readString.substring(PrefixKeyArray.length(), readString.indexOf(">")-1)+ "</key>";
					fWrite.write(writeReady);
					fWrite.newLine();
					writeReady = "		<array>";
					fWrite.write(writeReady);
					fWrite.newLine();
					readString = fRead.readLine();
					readString = readString.trim();
					while(readString.startsWith(prefixArrayItem)){
						writeReady = "			<string>" + readString.substring(prefixArrayItem.length(), readString.indexOf("/")-1)+ "</string>";
						fWrite.write(writeReady);
						fWrite.newLine();
						readString = fRead.readLine();
						readString = readString.trim();
					}
					writeReady = "		</array>";
					fWrite.write(writeReady);
					fWrite.newLine();
				}
			}
			fWrite.write("</dict>");
			fWrite.newLine();
			fWrite.write("</plist>");
			fRead.close();
			fWrite.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
		System.out.println("Completed Checking");	
	}
}
