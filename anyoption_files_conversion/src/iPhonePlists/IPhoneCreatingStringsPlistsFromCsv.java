package iPhonePlists;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;


public class IPhoneCreatingStringsPlistsFromCsv {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String fileName = "MessageResources_tr.properties";
		String fileWrite = "strings_tr.plist";
		String readString = "";
		String writeReady = "";
		String equals = "=";
		String country = "countries.";
		String payment = "payment.type.";
		int index1 = 0;
		int index2 = 0;
		
		/* readers & writers*/
		BufferedReader fRead = null;
		BufferedWriter fWrite = null;

		try {
			fRead = new BufferedReader(new InputStreamReader(new FileInputStream(fileName),"UTF8"));
			fWrite = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileWrite),"UTF8"));
			writeReady = "<?xml version=" + '"' + "1.0" + '"' + " encoding=" + '"' + "utf-8" + '"' + "?>";
			fWrite.write(writeReady);
			fWrite.newLine();
			writeReady = "<!DOCTYPE plist PUBLIC " + '"' + "-//Apple//DTD PLIST 1.0//EN" + '"' + '"' + "http://www.apple.com/DTDs/PropertyList-1.0.dtd>" ;
			fWrite.write(writeReady);
			fWrite.newLine();
			writeReady = "<plist version=" + '"' + "1.0" + '"' + ">";
			fWrite.write(writeReady);
			fWrite.newLine();
			fWrite.write("<dict>");
			fWrite.newLine();
			
			///reading from fread
			while ((readString =  fRead.readLine()) != null){
				readString = readString.trim();
				if (readString.startsWith(country ) || readString.startsWith(payment)){
					System.out.println(readString);
					index1 = readString.indexOf(equals);
					writeReady = "	<key>" + readString.substring(0, index1)+ "</key>";
					fWrite.write(writeReady);
					fWrite.newLine();
					writeReady = "	<string>" + readString.substring(index1 + 1) + "</string>";
					fWrite.write(writeReady);
					fWrite.newLine();
				}
			}
			fWrite.write("</dict>");
			fWrite.newLine();
			fWrite.write("</plist>");
			fRead.close();
			fWrite.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
		System.out.println("Completed Checking");	
	}
}
