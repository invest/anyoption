package com.trading.bubbles.fragment.login_register;

import com.anyoption.android.app.Application.Screen;
import com.trading.bubbles.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.login_register.LoginFragment;
import com.trading.bubbles.manager.BULoginManager;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class BULoginFragment extends LoginFragment {

	public static BULoginFragment newInstance(Bundle bundle){
		BULoginFragment loginFragment = new BULoginFragment();
		loginFragment.setArguments(bundle);
		return loginFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = super.onCreateView(inflater, container, savedInstanceState);
		TextView registerLinkTextView = (TextView) rootView.findViewById(R.id.login_register_link_textview);
		registerLinkTextView.setText(getString(R.string.register) + " »");
		registerLinkTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				application.postEvent(new NavigationEvent(Screen.REGISTER, NavigationType.INIT));
			}
		});
		
		return rootView;
	}
	
	@Override
	protected void underlineForgotPass(TextView textView){
		//Ignore
	}

	@Override
	protected BULoginManager setupLoginManager(){
		return new BULoginManager();
	}

}
