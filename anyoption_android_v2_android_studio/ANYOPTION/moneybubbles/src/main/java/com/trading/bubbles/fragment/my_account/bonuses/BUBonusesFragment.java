package com.trading.bubbles.fragment.my_account.bonuses;

import java.util.List;

import com.trading.bubbles.R;
import com.anyoption.android.app.fragment.my_account.bonuses.BonusesFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.BonusUsers;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class BUBonusesFragment extends BonusesFragment {

	public static BUBonusesFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new BonusesFragment.");
		BUBonusesFragment bonusesFragment = new BUBonusesFragment();
		bonusesFragment.setArguments(bundle);
		return bonusesFragment;
	}

	@Override
	protected ListViewAdapter createAdapter() {
		return new BUListViewAdapter(application, bonusesList);
	}

	public class BUBonusItemHolder extends BonusItemHolder {
		public View claimButton;
	}

	public class BUListViewAdapter extends ListViewAdapter implements OnClickListener {

		public BUListViewAdapter(Context context, List<BonusUsers> items) {
			super(context, items);
		}

		@Override
		protected BonusItemHolder createItemHolder() {
			return new BUBonusItemHolder();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = super.getView(position, convertView, parent);
			BonusUsers bonus = getItem(position);
			BUBonusItemHolder bonusItemHolder = (BUBonusItemHolder) view.getTag();

			if (convertView == null) {
				bonusItemHolder.claimButton = view.findViewById(R.id.bonus_list_item_claim_button);
			}

			if (bonus.getBonusStateId() == Constants.BONUS_STATE_PANDING) {
				bonusItemHolder.claimButton.setVisibility(View.VISIBLE);
				bonusItemHolder.stateTextView.setVisibility(View.GONE);
				bonusItemHolder.claimButton.setTag(bonusItemHolder);
				bonusItemHolder.claimButton.setOnClickListener(this);
			} else {
				bonusItemHolder.claimButton.setVisibility(View.GONE);
				bonusItemHolder.stateTextView.setVisibility(View.VISIBLE);
			}

			return view;
		}

		@Override
		public void onClick(View v) {
			if (v.getTag() instanceof BUBonusItemHolder) {
				BUBonusItemHolder bonusItemHolder = (BUBonusItemHolder) v.getTag();
				int position = bonusItemHolder.position;
				listView.performItemClick(listView.getChildAt(position), position, listAdapter.getItemId(position));
			}
		}

	}

}
