package com.trading.bubbles.popup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.trading.bubbles.R;
import com.anyoption.android.app.popup.LowBalanceOkDialogFragment;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.results.UserMethodResult;

public class BULowBalanceOkDialogFragment extends LowBalanceOkDialogFragment {

	public static final String TAG = BULowBalanceOkDialogFragment.class.getSimpleName();
	
	public static BULowBalanceOkDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new BULowBalanceOkDialogFragment.");
		BULowBalanceOkDialogFragment fragment = new BULowBalanceOkDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if(args != null){
			setCancelable(args.getBoolean(Constants.EXTRA_CANCELABLE, false));
		}
	}

	@Override
	protected int getContentLayout() {
		return R.layout.popup_low_balance;
	}

	@Override
	protected void onCreateContentView(View contentView) {
		setupCloseImage(contentView);

		firstMessageTextView = (TextView) contentView.findViewById(R.id.low_balance_popup_first_message);
		secondMessageTextView = (TextView) contentView.findViewById(R.id.low_balance_popup_second_message);
		loading = (ProgressBar) contentView.findViewById(R.id.low_balance_popup_loading);
		image = (ImageView) contentView.findViewById(R.id.low_balance_popup_image);

		if(minInvestmentAmount == 0l) {
			showHideLoading(true);
			getUser();
		} else {
			setupUI();
		}

		TextUtils.decorateInnerText(firstMessageTextView, null, firstMessageTextView.getText().toString(), firstMessageTextView.getText().toString(), FontsUtils.Font.ROBOTO_BOLD, 0, 0);

		super.onCreateContentView(contentView);
	}

	protected void setupCloseImage(View contentView) {
		ImageView closeImage = (ImageView) contentView.findViewById(R.id.low_balance_popup_close_image);
		closeImage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});

	}

	public void getUserCallback(Object resultObj) {
		UserMethodResult result = (UserMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			application.saveUser(result);
			minInvestmentAmount = result.getMinIvestmentAmount();
		} else {
			minInvestmentAmount = 0l;
		}
		setupUI();
		showHideLoading(false);
	}

	private void setupUI() {
		String minInvAmount;
		minInvAmount = AmountUtil.getFormattedAmountWithoutDecimalPointDigits(minInvestmentAmount, application.getUser().getCurrency());
		secondMessageTextView.setText(secondMessageTextView.getText() + " " + minInvAmount);
		TextUtils.decorateInnerText(secondMessageTextView, null, secondMessageTextView.getText().toString(), minInvAmount, FontsUtils.Font.ROBOTO_BOLD, 0, 0);
	}

	private void showHideLoading(boolean isVisible) {
		if (loading != null) {
			if (isVisible) {
				loading.setVisibility(View.VISIBLE);
				firstMessageTextView.setVisibility(View.INVISIBLE);
				secondMessageTextView.setVisibility(View.INVISIBLE);
				image.setVisibility(View.INVISIBLE);

			} else {
				loading.setVisibility(View.INVISIBLE);
				firstMessageTextView.setVisibility(View.VISIBLE);
				secondMessageTextView.setVisibility(View.VISIBLE);
				image.setVisibility(View.VISIBLE);
			}
		}
	}

}
