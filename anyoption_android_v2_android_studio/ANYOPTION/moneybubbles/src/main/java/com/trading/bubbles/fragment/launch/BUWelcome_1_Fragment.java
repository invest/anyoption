package com.trading.bubbles.fragment.launch;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.trading.bubbles.R;
import com.anyoption.android.app.fragment.BaseFragment;

/**
 * @author Anastas Arnaudov
 */
public class BUWelcome_1_Fragment extends BaseFragment{

    public static final String TAG = BUWelcome_1_Fragment.class.getSimpleName();

    public static BUWelcome_1_Fragment newInstance(){
        BUWelcome_1_Fragment fragment = new BUWelcome_1_Fragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.welcome_1_fragment, container, false);
        return rootView;
    }

}
