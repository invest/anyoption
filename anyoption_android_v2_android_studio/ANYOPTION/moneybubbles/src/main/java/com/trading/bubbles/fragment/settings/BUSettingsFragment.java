package com.trading.bubbles.fragment.settings;

import android.os.Bundle;
import android.view.View;

import com.anyoption.android.app.fragment.settings.SettingsFragment;

/**
 * Created by veselin.hristozov on 18.8.2016 г..
 */
public class BUSettingsFragment extends SettingsFragment {


    public static BUSettingsFragment newInstance(Bundle bundle) {
        BUSettingsFragment buSettingsFragment = new BUSettingsFragment();
        buSettingsFragment.setArguments(bundle);
        return buSettingsFragment;
    }

    protected void setupCancelInvestmentButton(){
        if(cancelInvestmentLayout == null){
            cancelInvestmentLayout = rootView.findViewById(com.anyoption.android.R.id.settings_cancel_investment_layout);
        }

        cancelInvestmentLayout.setVisibility(View.GONE);
    }

}