package com.trading.bubbles.fragment;

import com.trading.bubbles.R;
import com.anyoption.android.app.fragment.StatusFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class BUStatusFragment extends StatusFragment {

	public static BUStatusFragment newInstance(){
		return BUStatusFragment.newInstance(new Bundle());
	}
	
	public static BUStatusFragment newInstance(Bundle bundle){
		BUStatusFragment statusfragment = new BUStatusFragment();
		statusfragment.setArguments(bundle);
		return statusfragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = super.onCreateView(inflater, container, savedInstanceState);
	
		balanceView = rootView.findViewById(R.id.status_left_layout);
		balanceInfoImageView = rootView.findViewById(R.id.status_balance_info_view);
		
		return rootView;
	}
	
}
