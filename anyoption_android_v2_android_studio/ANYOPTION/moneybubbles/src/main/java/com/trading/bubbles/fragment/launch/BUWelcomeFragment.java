package com.trading.bubbles.fragment.launch;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.app.Application;
import com.trading.bubbles.BUApplication;
import com.trading.bubbles.R;
import com.anyoption.android.app.fragment.launch.WelcomeFragment;
import com.anyoption.android.app.util.DrawableUtils;

import java.util.ArrayList;
import java.util.List;

public class BUWelcomeFragment extends WelcomeFragment {

	public static BUWelcomeFragment newInstance(Bundle bundle){
		Log.d(TAG, "Created new BUWelcomeFragment.");
		BUWelcomeFragment fragment = new BUWelcomeFragment();
		fragment.setArguments(bundle);	
		return fragment;
	}

	@Override
	protected List<Fragment> getFragments() {
		List<Fragment> fragmentList = new ArrayList<Fragment>();

		fragmentList.add(BUWelcome_1_Fragment.newInstance());
		fragmentList.add(BUWelcome_2_Fragment.newInstance());
		fragmentList.add(BUWelcome_3_Fragment.newInstance());

		return fragmentList;
	}

	protected void setupTryWithoutAccount() {
		tryWithoutAccountTextView = (TextView) rootView.findViewById(com.anyoption.android.R.id.welcome_screen_try_without_account_text);
		tryWithoutAccountTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tryApp();
			}
		});
	}

	@Override
	protected void tryApp() {
		((BUApplication)application).goToBubbles(null, null);
	}

	@Override
	protected void setupLoginRegisterButtons() {
		super.setupLoginRegisterButtons();
		((TextView) loginButton).setText(getString(R.string.welcome_screen_login_link_1_text));
	}

	@Override
	protected void setupDots(){
		//Create dot for each page:
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen.welcome_screen_dot_size), getResources().getDimensionPixelSize(R.dimen.welcome_screen_dot_size));
		params.setMargins(getResources().getDimensionPixelSize(R.dimen.welcome_screen_dots_margin), 0, getResources().getDimensionPixelSize(R.dimen.welcome_screen_dots_margin), 0);
		for(int i = 0; i < pagerAdapter.getCount(); i++){
			ImageView dotImage = new ImageView(application.getCurrentActivity());
			dotImage.setImageDrawable(getCurrentDot());
			dotImage.setLayoutParams(params);
			dotsLayout.addView(dotImage);
		}
		dotsLayout.invalidate();
	}

	@Override
	protected void changeDots(int page){
		for(int i = 0; i < pagerAdapter.getCount(); i++){
			ImageView dotImage = (ImageView) dotsLayout.getChildAt(i);
			if(i <= page){
				dotImage.setImageDrawable(getCurrentDot());
			}else{
				dotImage.setImageDrawable(getOtherDot());
			}
		}
		dotsLayout.invalidate();
	}

	protected Drawable getCurrentDot() {
		return DrawableUtils.getDrawable(R.drawable.slider_point_full);
	}

	protected Drawable getOtherDot() {
		return DrawableUtils.getDrawable(R.drawable.slider_point_empty);
	}

	@Override
	protected void goToNextLoggedInScreen() {
		((BUApplication)application).goToBubbles(Application.Screen.TRADE, null);
	}
}