package com.trading.bubbles.fragment.my_account.questionnaire_new;

import android.os.Bundle;

import com.anyoption.android.app.fragment.my_account.questionnaire_new.QuestionnaireFragment;
import com.trading.bubbles.BUApplication;

/**
 * Created by veselin.hristozov on 22.8.2016 г..
 */
public class BUQuestionnaireFragment extends QuestionnaireFragment {

    public final String TAG = BUQuestionnaireFragment.class.getSimpleName();

    public static BUQuestionnaireFragment newInstance(Bundle bundle){
        BUQuestionnaireFragment fragment = new BUQuestionnaireFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void goToMain() {
        ((BUApplication)application).goToBubbles(null, null);
    }

}
