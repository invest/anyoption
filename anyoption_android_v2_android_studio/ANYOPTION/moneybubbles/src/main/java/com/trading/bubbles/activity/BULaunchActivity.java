package com.trading.bubbles.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.activity.LaunchActivity;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.util.constants.Constants;

public class BULaunchActivity extends LaunchActivity {

    @Override
    protected void setupSplashScreen() {
        ImageView splashForeground = (ImageView) findViewById(com.anyoption.android.R.id.launch_splash_foreground);
        splashForeground.setVisibility(View.GONE);
    }

    public void onEventMainThread(NavigationEvent event){
        super.onEventMainThread(event);
        if(!isFinishing() && this != application.getCurrentActivity() && application.getCurrentActivity().getClass().getSimpleName().contains("BubblesActivity")) {
            Bundle bundle = new Bundle();
            Application.Screenable screen = event.getToScreen();
            Class<? extends BaseFragment> fragmentClass = getFragmentClass(bundle, screen, event);
            if (bundle.containsKey(Constants.EXTRA_SCREEN)) {
                openMainActivity(bundle);
            }
        }

    }

    @Override
    protected void handleStatusAndActionBarOnWelcomeScreens() {
        if (statusBarLayout != null && actionBarLayout != null) {
            statusBarLayout.setVisibility(View.GONE);
            actionBarLayout.setVisibility(View.GONE);
            statusBarSeparator.setVisibility(View.GONE);
            actionBarSeparator.setVisibility(View.GONE);
        } else {
            statusBarLayout.setVisibility(View.VISIBLE);
            actionBarLayout.setVisibility(View.VISIBLE);
            statusBarSeparator.setVisibility(View.VISIBLE);
            actionBarSeparator.setVisibility(View.VISIBLE);
        }
    }

}
