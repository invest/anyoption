package com.trading.bubbles.model;

import com.anyoption.common.beans.base.Investment;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Anastas Arnaudov
 */
public class MyOptionsGroup {

    public enum Type{
        BINARY,
        DYNAMICS,
        OPTION_PLUS,
        BUBBLES
    }

    private Type type;
    private String title;
    private List<Investment> investments;
    private boolean isExpanded;

    public MyOptionsGroup(Type type, String title){
        this.type = type;
        this.title = title;
        this.investments = new ArrayList<Investment>();
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return title;
    }

    public void addInvestments(Investment investment){
        investments.add(investment);
    }

    public List<Investment> getInvestments(){
        return investments;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public Type getType(){
        return type;
    }
}
