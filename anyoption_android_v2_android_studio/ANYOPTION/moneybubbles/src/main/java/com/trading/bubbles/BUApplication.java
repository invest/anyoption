package com.trading.bubbles;
import android.content.Intent;
import android.os.Bundle;

import java.util.HashMap;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.NavigationEvent;
import com.trading.bubbles.activity.BUBubblesActivity;
import com.trading.bubbles.manager.BUAppoxeeManager;
import com.trading.bubbles.manager.BUAppseeManager;
import com.trading.bubbles.manager.BUGoogleAdWordsManager;
import com.anyoption.android.app.manager.AppseeManager;
import com.anyoption.android.app.manager.GoogleAdWordsManager;
import com.anyoption.android.app.manager.GoogleAnalyticsManager;
import com.trading.bubbles.util.BUConstants;

public class BUApplication extends Application {

	public static final String SUBCLASS_PREFIX = "BU";
	
	protected BUAppoxeeManager 	appoxeeManager;
	protected BUAppseeManager 	appseeManager;

	//#################################################################################################################
	//######################					BUDA	SCREENs						###################################
	//#################################################################################################################
	
	public interface BUScreenable extends Screenable{
		
	}
	
	public enum BUScreen implements BUScreenable {
		FIRST_NEW_CARD("new card", get().getResources().getString(R.string.newCardScreenTitle), 0, Screenable.SCREEN_FLAG_ADD_HELP, Screenable.SCREEN_FLAG_ADD_BACK);

		/**
		 * Tag used to search.
		 */
		private String tag;

		/**
		 * A default title. "" - if no default title is needed.
		 */
		private String title;
		/**
		 * Id of an image used instead of title. 0 - if no image is needed.
		 */
		private int imageID;

		/**
		 * Additional details for the Screen
		 */
		private int[] flags;
		
		/**
		 * Map of all screens
		 */
		private static HashMap<String, Screenable> screensMap;
		
		BUScreen(String tag, String title, int imageID, int... flags){
			this.tag = tag;
			this.title = title;
			this.imageID = imageID;
			this.flags = flags;
		}

		public String getTag() {
			return tag;
		}
		public String getTitle() {
			return title;
		}
		public int getImageID() {
			return imageID;
		}
		public boolean isHasHelpInfo() {
			return false;
		}
		public int[] getFlags(){
			return this.flags;
		}

		public static Screenable getScreenByTag(String tag) {
			if (screensMap == null) {
				Screenable[] baseScreens = Screen.values();
				// Add base screens
				screensMap = new HashMap<String, Screenable>(baseScreens.length);
				for (Screenable screen : baseScreens) {
					screensMap.put(screen.getTag(), screen);
				}

				// Add CO screens
				Screenable[] coScreens = BUScreen.values();
				for (Screenable screen : coScreens) {
					screensMap.put(screen.getTag(), screen);
				}
			}

			return screensMap.get(tag);
		}

	}
	//#################################################################################################################
	//#################################################################################################################
	//#################################################################################################################
	
	@Override
	public String getServiceURL() {
		return BUConstants.SERVICE_URL;
	}

	@Override
	public String getLSURL() {
		return BUConstants.LS_URL;
	}

	@Override
	public String getMerchantID() {
		return BUConstants.MERCHANT_ID;
	}

	@Override
	public String getOneClickURL() {
		return BUConstants.ONE_CLICK_URL;
	}

	@Override
	public String getWebSiteLink() {
		return BUConstants.WEB_SITE_LINK;
	}

	@Override
	protected String getSubclassPrefix() {
		return SUBCLASS_PREFIX;
	}

	@Override
	public long getWriterId(){
		return BUConstants.MOBILE_WRITER_ID;
	}

	@Override
	public String getDeepLink(){
		return BUConstants.DEEP_LINK;
	}

	//#################################################################################################################
	//###########################					MANAGERS						###################################
	//#################################################################################################################
	
	@Override
	public synchronized BUAppoxeeManager getAppoxeeManager(){
		if(appoxeeManager == null){
			appoxeeManager = new BUAppoxeeManager(this);
		}
		return appoxeeManager;
	}

	@Override
	public synchronized AppseeManager getAppseeManager(){
		if(appseeManager == null){
			appseeManager = new BUAppseeManager();
		}
		return appseeManager;
	}

	@Override
	public GoogleAdWordsManager getGoogleAdWordsManager() {
		if (googleAdWordsManager == null) {
			googleAdWordsManager = new BUGoogleAdWordsManager(this);
		}
		return googleAdWordsManager;
	}

	@Override
	public synchronized GoogleAnalyticsManager getGoogleAnalyticsManager() {
		return new GoogleAnalyticsManager(R.xml.anyoption_app_tracker, R.xml.anyoption_old_app_tracker);
	}
	//#################################################################################################################


	public void goToBubbles(Screenable screen, Bundle bundle) {
		if(screen != null) {
			NavigationEvent navigationEvent = new NavigationEvent(screen, NavigationEvent.NavigationType.INIT);

			if (bundle != null) {
				navigationEvent.setBundle(bundle);
			}

			if (screen == Screen.TRADE || screen == Screen.TABLET_TRADE) {

				if (Application.get().isTablet()) {
					screen = Screen.TABLET_TRADE;
				} else {
					screen = Screen.TRADE;
				}

				navigationEvent.setFlags(Screenable.SCREEN_FLAG_REMOVE_HELP | Screenable.SCREEN_FLAG_REMOVE_MENU);
			}

			navigationEvent.setToScreen(screen);

			Application.get().postEvent(navigationEvent);
		}

		Application.get().setCurrentScreen(Application.Screen.BUBBLES);
		Intent intent = new Intent(Application.get().getApplicationContext(), BUBubblesActivity.class);
		Application.get().getCurrentActivity().startActivity(intent);
	}
}
