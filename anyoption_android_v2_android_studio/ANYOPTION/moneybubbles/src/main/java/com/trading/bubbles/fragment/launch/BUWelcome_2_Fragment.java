package com.trading.bubbles.fragment.launch;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.TextUtils;
import com.trading.bubbles.R;
import com.anyoption.android.app.fragment.BaseFragment;

/**
 * @author Anastas Arnaudov
 */
public class BUWelcome_2_Fragment extends BaseFragment{

    public static final String TAG = BUWelcome_2_Fragment.class.getSimpleName();

    public static BUWelcome_2_Fragment newInstance(){
        BUWelcome_2_Fragment fragment = new BUWelcome_2_Fragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.welcome_2_fragment, container, false);

        TextView text = (TextView) rootView.findViewById(R.id.welcome_screen_2_text_1);

        TextUtils.decorateInnerText(text, null, text.getText().toString(), getString(R.string.introScreen2Message1_bold), FontsUtils.Font.ROBOTO_REGULAR, 0, 0);

        return rootView;
    }

}
