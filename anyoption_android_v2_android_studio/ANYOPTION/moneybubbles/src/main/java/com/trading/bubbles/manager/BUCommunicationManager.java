package com.trading.bubbles.manager;

import com.anyoption.android.app.manager.CommunicationManager;
import com.trading.bubbles.BUApplication;

public class BUCommunicationManager extends CommunicationManager {

	public static final String TAG = BUCommunicationManager.class.getSimpleName();

	public BUCommunicationManager(BUApplication application) {
		super(application);
	}
	
}