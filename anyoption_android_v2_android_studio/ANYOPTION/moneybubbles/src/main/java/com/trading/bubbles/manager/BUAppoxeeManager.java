package com.trading.bubbles.manager;

import com.trading.bubbles.BUApplication;
import com.anyoption.android.app.manager.AppoxeeManager;

public class BUAppoxeeManager extends AppoxeeManager {

	public static final String APP_KEY 					= "54aa6a6f087a48.12356917";
	public static final String SECRET_KEY 				= "54aa6a6f087b74.34470779";
	
	public BUAppoxeeManager(BUApplication application) {
		super(application);
	}
	
	protected String getApiKey(){
		return BUAppoxeeManager.APP_KEY;
	}
	
	protected String getSecretKey(){
		return BUAppoxeeManager.SECRET_KEY;
	}
	
}
