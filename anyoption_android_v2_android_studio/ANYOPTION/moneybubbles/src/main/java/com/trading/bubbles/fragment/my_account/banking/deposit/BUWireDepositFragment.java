package com.trading.bubbles.fragment.my_account.banking.deposit;

import com.trading.bubbles.BUApplication;
import com.trading.bubbles.R;
import com.anyoption.android.app.fragment.my_account.banking.deposit.WireDepositFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class BUWireDepositFragment extends WireDepositFragment {

    public static BUWireDepositFragment newInstance(Bundle bundle) {
		BUWireDepositFragment wireDepositFragment = new BUWireDepositFragment();
        wireDepositFragment.setArguments(bundle);
        return wireDepositFragment;
    }
	
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = super.onCreateView(inflater, container, savedInstanceState);
		
		TextView skipLinkTextView = (TextView) rootView.findViewById(R.id.skip_link);
		if(application.getUser().getFirstDepositId() == 0){
			skipLinkTextView.setVisibility(View.VISIBLE);
			skipLinkTextView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					((BUApplication)application).goToBubbles(null, null);
				}
			});
		}else{
			skipLinkTextView.setVisibility(View.GONE);
		}
		
		return rootView;
    }

}
