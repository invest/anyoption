package com.trading.bubbles.activity;

import android.os.Bundle;
import android.util.Log;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.activity.MainActivity;
import com.anyoption.android.app.event.MainMenuEvent;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.util.RegulationUtils;
import com.trading.bubbles.BUApplication;

/**
 * Created by veselin.hristozov on 22.8.2016 г..
 */
public class BUMainActivity extends MainActivity {

    private static final String TAG = BUMainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void handleMenuOnChangeScreen(NavigationEvent event) {
        if (event.getToScreen() == Application.Screen.TRADE || event.getToScreen() == Application.Screen.TABLET_TRADE) {

            openMenuInTradeScreen();

        } else {
            // If the menu is opened, we only close it.
            if (isMenuOpened()) {
                closeMenu();
            }
        }
    }

    @Override
    protected boolean handleMenuOnBackPressed() {
        if(!(application.isLoggedIn() && application.getUser().getIsNeedChangePassword()) || !application.isLoggedIn()) {
            if (application.getCurrentScreen() != Application.Screen.BUBBLES && application.getCurrentScreen() != Application.Screen.TRADE && application.getCurrentScreen() != Application.Screen.TABLET_TRADE) {
                if (isMenuOpened()) {
                    return false;
                } else {
                    if (!application.getFragmentManager().hasPreviousScreen()) {
                        openMenu();
                        return true;
                    } else {
                        return false;
                    }
                }
            } else if (application.getCurrentScreen() == Application.Screen.BUBBLES || application.getCurrentScreen() == Application.Screen.TRADE || application.getCurrentScreen() == Application.Screen.TABLET_TRADE) {
                if (!isMenuOpened()) {
                    openMenuInTradeScreen();
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            if (isMenuOpened()) {
                closeMenu();
                return true;
            }
            return false;
        }
        return false;
    }

    @Override
    protected boolean handleBackPressedFromLastScreen() {
            if (application.isLoggedIn() && isMenuOpened()) {
                showLogoutPopUp();
                return true;
            }
        return false;
    }

    @Override
    protected void handleOnBackPressedBackToTrade() {
        application.postEvent(new NavigationEvent(Application.Screen.TRADE, NavigationEvent.NavigationType.INIT));
        openMenuInTradeScreen();
    }

    @Override
    protected boolean setUpMenuSlide(Application.Screenable screen) {
        boolean enableMenuSlide = true;
        enableMenuSlide = !checkInitialLoginRegister();
        if (screen == Application.Screen.ASSET_VIEW_PAGER || screen == Application.Screen.DEPOSIT_WELCOME
                || screen == Application.Screen.TERMS_AND_CONDITIONS || screen == Application.Screen.AGREEMENT || screen == Application.Screen.TRADE || screen == Application.Screen.TABLET_TRADE || (screen == Application.Screen.UPDATE_PASSWORD && application.isLoggedIn() && application.getUser().getIsNeedChangePassword())) {
            enableMenuSlide = false;
        }

        enableMenuSwipe(enableMenuSlide);
        return enableMenuSlide;
    }

    @Override
    protected void openMenu() {
        if ((!(application.isLoggedIn() && application.getUser().getIsNeedChangePassword())) && (application.getCurrentScreen() != Application.Screen.BUBBLES && application.getCurrentScreen() != Application.Screen.TRADE && application.getCurrentScreen() != Application.Screen.TABLET_TRADE)) {
            hideKeyboard();
            if (isSlidingMenuUsed) {
                slidingMenu.showMenu();
            } else if (!menuFragment.isVisible()) {
                showMenuFragment();
            }
        } else {
            closeMenu();
        }
    }

    protected void openMenuInTradeScreen() {
        if ((!(application.isLoggedIn() && application.getUser().getIsNeedChangePassword()))) {
            hideKeyboard();
            if (isSlidingMenuUsed) {
                slidingMenu.showMenu();
            } else if (!menuFragment.isVisible()) {
                showMenuFragment();
            }
        } else {
            closeMenu();
        }
    }


    /**
     * Called when a {@link MainMenuEvent} occurs.
     *
     * @param event
     */
    @Override
    public void onEventMainThread(MainMenuEvent event) {
        Log.d(TAG, "on MainMenuEvent");

        if(application.getCurrentScreen() != Application.Screen.BUBBLES && application.getCurrentScreen() != Application.Screen.TRADE && application.getCurrentScreen() != Application.Screen.TABLET_TRADE) {
            if (isMenuOpened()) {
                closeMenu();
            } else {
                openMenu();
            }
        }
    }

    @Override
    protected void goToScreen(Application.Screenable screen, Bundle bundle) {
        if ((screen == Application.Screen.TRADE || screen == Application.Screen.TABLET_TRADE) && RegulationUtils.isAfterFirstDeposit() && !RegulationUtils.shouldShowRegulationDialogs() && application.getMinInvestmentAmount() <= 0L) {
            ((BUApplication)application).goToBubbles(Application.Screen.TRADE, null);
        } else {
            application.postEvent(new NavigationEvent(screen, isTablet ? NavigationEvent.NavigationType.DEEP : NavigationEvent.NavigationType.INIT, bundle));
        }
    }

}
