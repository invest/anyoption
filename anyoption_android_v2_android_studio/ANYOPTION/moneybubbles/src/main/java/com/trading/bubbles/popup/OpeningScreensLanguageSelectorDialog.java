package com.trading.bubbles.popup;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.util.Log;
import android.util.StateSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Language;
import com.trading.bubbles.R;

import java.util.List;

/**
 * Created by veselin.hristozov on 30.8.2016 г..
 */
public class OpeningScreensLanguageSelectorDialog extends BaseDialogFragment implements AdapterView.OnItemClickListener {

    public static final String TAG = OpeningScreensLanguageSelectorDialog.class.getSimpleName();

    private ListViewAdapter adapter;
    private ListView listView;
    private List<Language> items;
    private int defaultLanguageIndex = 0;

    protected OnOpeningScreensLanguageSelectorChosenLanguage listener;

    public interface OnOpeningScreensLanguageSelectorChosenLanguage {
        public void onSelectedLanguage(Language language);
    }

    public void setListener(OnOpeningScreensLanguageSelectorChosenLanguage listener) {
        this.listener = listener;
    }

    public static OpeningScreensLanguageSelectorDialog newInstance(Bundle bundle) {
        Log.d(TAG, "Created new OpeningScreensLanguageSelectorDialog.");
        OpeningScreensLanguageSelectorDialog fragment = new OpeningScreensLanguageSelectorDialog();
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, true);
        bundle.putInt(ARG_BACKGROUND_COLOR_KEY, com.anyoption.android.R.color.black_alpha_40);
        bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, com.anyoption.android.R.color.transparent);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if(args != null){
            setCancelable(args.getBoolean(Constants.EXTRA_CANCELABLE, true));
        }
    }


    @Override
    protected int getContentLayout() {
        return R.layout.opening_screens_language_selector_popup;
    }

    @Override
    protected void onCreateContentView(View contentView) {
        listView = (ListView) contentView.findViewById(R.id.opening_screens_language_selector_popup_list_view);
        adapter = new ListViewAdapter();
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
        showCloseButton(true);
    }

    public void setItems(List<Language> items){
        this.items = items;
        if(adapter != null){
            adapter.notifyDataSetChanged();
        }
    }

    public void setDefaultLanguageIndex(int index){
        this.defaultLanguageIndex = index;
    }

    private class ListViewItemHolder {
        public TextView textView;
    }

    private class ListViewAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Language item = items.get(position);
            ListViewItemHolder holder;
            if (convertView == null) {
                LayoutInflater inf = (LayoutInflater) application.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inf.inflate(R.layout.opening_screens_language_selector_popup_item, parent, false);
                holder = new ListViewItemHolder();
                holder.textView = (TextView) convertView.findViewById(R.id.opening_screens_language_selector_popup_item_text_view);

                convertView.setTag(holder);
            }else{
                holder = (ListViewItemHolder) convertView.getTag();
            }

            holder.textView.setText(item.getDisplayName());

            if(position == defaultLanguageIndex) {
                if(position == getCount()-1) {
                    DrawableUtils.setBackground(holder.textView, DrawableUtils.getDrawable(R.drawable.opening_screens_popup_bottom_selected_background));
                } else if(position == 0) {
                    DrawableUtils.setBackground(holder.textView, DrawableUtils.getDrawable(R.drawable.opening_screens_popup_top_selected_background));
                } else {
                    DrawableUtils.setBackground(holder.textView, DrawableUtils.getDrawable(R.drawable.opening_screens_popup_middle_selected_background));
                }
            } else {
                StateListDrawable stateListDrawable = new StateListDrawable();

                if(position == getCount()-1) {
                    stateListDrawable.addState(new int[] { android.R.attr.state_pressed}, DrawableUtils.getDrawable(R.drawable.opening_screens_popup_bottom_selected_background));
                } else if(position == 0) {
                    stateListDrawable.addState(new int[] { android.R.attr.state_pressed}, DrawableUtils.getDrawable(R.drawable.opening_screens_popup_top_selected_background));
                } else {
                    stateListDrawable.addState(new int[] { android.R.attr.state_pressed}, DrawableUtils.getDrawable(R.drawable.opening_screens_popup_middle_selected_background));
                }

                stateListDrawable.addState(StateSet.WILD_CARD, null);

                DrawableUtils.setBackground(holder.textView, stateListDrawable);
            }

            return convertView;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        for(int listViewItems = 0; listViewItems < listView.getChildCount(); listViewItems++) {
            if(position != listViewItems) {
                DrawableUtils.setBackground(listView.getChildAt(listViewItems), null);
            }
        }

        if(position == adapter.getCount()-1) {
            DrawableUtils.setBackground(view, DrawableUtils.getDrawable(R.drawable.opening_screens_popup_bottom_selected_background));
        } else if(position == 0) {
            DrawableUtils.setBackground(view, DrawableUtils.getDrawable(R.drawable.opening_screens_popup_top_selected_background));
        } else {
            DrawableUtils.setBackground(view, DrawableUtils.getDrawable(R.drawable.opening_screens_popup_middle_selected_background));
        }


        if(listener != null) {
            listener.onSelectedLanguage(items.get(position));
        }
    }
}
