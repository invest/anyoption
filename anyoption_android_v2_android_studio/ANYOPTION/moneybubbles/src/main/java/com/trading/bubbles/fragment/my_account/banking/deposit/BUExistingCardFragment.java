package com.trading.bubbles.fragment.my_account.banking.deposit;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anyoption.android.app.fragment.my_account.banking.deposit.ExistingCardFragment;
import com.anyoption.android.app.util.RegulationUtils;
import com.trading.bubbles.BUApplication;

/**
 * @author Anastas Arnaudov
 */
public class BUExistingCardFragment extends ExistingCardFragment {

    public static BUExistingCardFragment newInstance(Bundle bundle){
        BUExistingCardFragment existingCardFragment = new BUExistingCardFragment();
        existingCardFragment.setArguments(bundle);
        return existingCardFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater,container,savedInstanceState);
        application.getAppseeManager().hideView(rootView.findViewById(com.anyoption.android.R.id.existing_card_num_textview));
        return rootView;
    }

    protected void handleSuccessfulDepositRedirect() {
        if (RegulationUtils.checkRegulationStatusAndDoNecessary(false) && RegulationUtils.showUploadDocuments()) {
            ((BUApplication)application).goToBubbles(null, null);
        }
    }

}
