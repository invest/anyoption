package com.trading.bubbles.fragment.my_account;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.anyoption.android.app.fragment.my_account.SettledOptionsFragment;
import com.trading.bubbles.popup.MyOptionsReceiptDialogFragment;
import com.anyoption.android.app.util.InvestmentUtil;
import com.anyoption.common.service.results.InvestmentsMethodResult;
import com.trading.bubbles.util.adapter.BUTradingOptionsListAdapter;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Investment;

import java.util.ArrayList;

public class BUSettledOptionsFragment extends SettledOptionsFragment {

	public static BUSettledOptionsFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created fragment BUSettledOptionsFragment");
		BUSettledOptionsFragment fragment = new BUSettledOptionsFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	protected void setUpListAdapter() {
		optionsListAdapter = new BUTradingOptionsListAdapter(investments, false);
	}

	protected void setupItemClickListener(){
		optionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Investment investment = investments.get(position);
				Bundle args = new Bundle();
				args.putSerializable(Constants.EXTRA_INVESTMENT, investment);
				args.putBoolean(Constants.EXTRA_IS_INVESTMENT_OPEN, false);
				application.getFragmentManager().showDialogFragment(MyOptionsReceiptDialogFragment.class, args);
			}
		});
	}

	@Override
	public void investmentsCallBack(Object result) {
		isLoading = false;
		setupLoading();

		InvestmentsMethodResult investmentsResult = (InvestmentsMethodResult) result;
		if (investmentsResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {

			ArrayList<Investment> filteredInvestments = new ArrayList<Investment>();

			for (Investment investment : investmentsResult.getInvestments()) {
				if(investment.getTypeId() == Investment.INVESTMENT_TYPE_BUBBLES) {
					filteredInvestments.add(investment);
				}
			}

			investmentsResult.setInvestments(new Investment[0]);

			Investment[] newInvestments = new Investment[filteredInvestments.size()];
			newInvestments = filteredInvestments.toArray(newInvestments);

			investmentsResult.setInvestments(newInvestments);

			InvestmentUtil.fixDate(newInvestments);
			getInvestmentsFromResult(investmentsResult);
			gotInvestmentsForAdapter();
			startRow += PAGE_SIZE;
			if(newInvestments.length < PAGE_SIZE){
				isLastSettledOptionsReached = true;
			}
			Log.d(TAG, "Just got settled options: " + newInvestments.length);

			int period = getArguments().getInt(Constants.EXTRA_PERIOD, PERIOD_DAY);
			if(period == PERIOD_MONTH) {
				getListView().setFooterDividersEnabled(true);
				getListView().removeFooterView(footerView);
				if(investments.size() > 0) {
					getListView().setFooterDividersEnabled(false);
					getListView().addFooterView(footerView);
				}
			} else {
				getListView().setFooterDividersEnabled(true);
				getListView().removeFooterView(footerView);
			}

		} else {
			Log.e(TAG, "Could not get settled options! :)");
		}
	}

}
