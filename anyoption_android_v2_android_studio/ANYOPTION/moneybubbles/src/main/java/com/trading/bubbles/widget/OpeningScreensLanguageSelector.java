package com.trading.bubbles.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.LanguageChangeEvent;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.widget.LanguageSelector;
import com.anyoption.common.beans.base.Language;
import com.trading.bubbles.R;
import com.trading.bubbles.popup.OpeningScreensLanguageSelectorDialog;

import java.util.List;

/**
 * Created by veselin.hristozov on 30.8.2016 г..
 */
public class OpeningScreensLanguageSelector extends LanguageSelector implements OpeningScreensLanguageSelectorDialog.OnOpeningScreensLanguageSelectorChosenLanguage {

    private int defaultLanguageIndex;
    private List<Language> items;

    public OpeningScreensLanguageSelector(Context context) {
        super(context);
    }

    public OpeningScreensLanguageSelector(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OpeningScreensLanguageSelector(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected int getSelectorLayout() {
        return R.layout.opening_screens_language_selector;
    }

    @Override
    protected int getSelectorItemLayout() {
        return R.layout.opening_screens_form_selector_list_item;
    }

    @Override
    protected int getSelectorTextColor() {
        return R.color.opening_screens_language_selector_text_color;
    }

    @Override
    protected int getSelectorTextSize() {
        return R.dimen.opening_screens_language_selector_text_size;
    }

    @Override
    protected FontsUtils.Font getSelectorTextFont() {
        return FontsUtils.Font.ROBOTO_REGULAR;
    }

    @Override
    public void setItems(List<Language> items, int defaultSelectedItem, final ItemListener<Language> onItemSelectedListener) {
        this.defaultLanguageIndex = defaultSelectedItem;
        this.items = items;
    }

    @Override
    public void onClick(View v) {
        OpeningScreensLanguageSelectorDialog openingScreensLanguageSelectorDialog = new OpeningScreensLanguageSelectorDialog();
        openingScreensLanguageSelectorDialog.setItems(items);
        openingScreensLanguageSelectorDialog.setDefaultLanguageIndex(defaultLanguageIndex);
        openingScreensLanguageSelectorDialog.setListener(this);
        Application.get().getFragmentManager().showDialogFragment(openingScreensLanguageSelectorDialog);
    }

    public void onSelectedLanguage(Language selectedLanguage) {
        text.setText(selectedLanguage.getDisplayName());
        Log.d(TAG, "Language Changed");
        boolean shouldGoToMain = (getMode() == MODE_SETTINGS) ? true : false;
        Application.get().postEvent(new LanguageChangeEvent(selectedLanguage.getCode(), shouldGoToMain));
        setDirection(Application.get().getLayoutDirection());
    }
}
