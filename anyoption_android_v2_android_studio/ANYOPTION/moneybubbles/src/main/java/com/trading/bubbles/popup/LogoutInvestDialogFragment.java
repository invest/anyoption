package com.trading.bubbles.popup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.anyoption.android.app.popup.LoginRegisterDialogfragment;
import com.trading.bubbles.R;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.TextUtils;

/**
 * @author Anastas Arnaudov
 */
public class LogoutInvestDialogFragment extends LoginRegisterDialogfragment {

    public static final String TAG = LogoutInvestDialogFragment.class.getSimpleName();

    public static LogoutInvestDialogFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new LogoutInvestDialogFragment.");
        LogoutInvestDialogFragment fragment = new LogoutInvestDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getContentLayout() {
        return R.layout.popup_logout_invest;
    }

    @Override
    protected void onCreateContentView(View contentView) {
        super.onCreateContentView(contentView);

        String innerText = getString(R.string.logoutInvestPopupLoginHere);
        int innerColor = R.color.logout_invest_popup_register_button_color;
        int innerSize  = (int) getResources().getDimension(R.dimen.logout_invest_popup_title_text_size);
        TextView loginButtonTextViewTextView = (TextView) contentView.findViewById(com.anyoption.android.R.id.login_register_popup_login_button);
        TextUtils.decorateInnerText(loginButtonTextViewTextView, null, loginButtonTextViewTextView.getText().toString(), innerText, FontsUtils.Font.ROBOTO_REGULAR, innerColor, innerSize);
    }
}
