package com.trading.bubbles.manager;

import android.util.Log;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.DepositFailEvent;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.manager.AppseeManager;
import com.appsee.Appsee;

/**
 * @author Anastas Arnaudov
 */
public class BUAppseeManager extends AppseeManager {

    public static final String TAG = BUAppseeManager.class.getSimpleName();

    private static final String SCREEN_DEPOSIT 				= "Deposit Screen";
    private static final String SCREEN_TRADE 				= "Trade Screen";

    private static final String EVENT_DEPOSIT 				= "Deposit";
    private static final String EVENT_MY_ACCOUNT	 		= "My Account";
    private static final String EVENT_MY_OPTIONS	 		= "My options";
    private static final String EVENT_SUPPORT		 		= "Support";
    private static final String EVENT_REGISTER 				= "Register";
    private static final String EVENT_LOGIN 				= "Login";

    public BUAppseeManager(){
        super();
        Log.d(TAG, "On Create");
        Application.get().registerForEvents(this, NavigationEvent.class, DepositFailEvent.class);
    }

    public void onEvent(NavigationEvent event) {
        Application.Screenable screen = event.getToScreen();
        if(screen == Application.Screen.DEPOSIT_MENU){
            Log.d(TAG, "Sending Deposit Screen event.");
            Appsee.addEvent(EVENT_DEPOSIT);

            Log.d(TAG, "Start recording Deposit Screen.");
            Appsee.startScreen(SCREEN_DEPOSIT);
        }else if(screen == Application.Screen.ASSET_PAGE){
            Log.d(TAG, "Start recording Trade Screen.");
            Appsee.startScreen(SCREEN_TRADE);
        }else if(screen == Application.Screen.MY_ACCOUNT){
            Log.d(TAG, "Sending My Account Screen event.");
            Appsee.addEvent(EVENT_MY_ACCOUNT);
        }else if(screen == Application.Screen.MY_OPTIONS){
            Log.d(TAG, "Sending My Options Screen event.");
            Appsee.addEvent(EVENT_MY_OPTIONS);
        }else if(screen == Application.Screen.SUPPORT){
            Log.d(TAG, "Sending Support Screen event.");
            Appsee.addEvent(EVENT_SUPPORT);
        }else if(screen == Application.Screen.REGISTER) {
            Log.d(TAG, "Sending Register Screen event.");
            Appsee.addEvent(EVENT_REGISTER);
        }else if(screen == Application.Screen.LOGIN) {
            Log.d(TAG, "Sending Login Screen event.");
            Appsee.addEvent(EVENT_LOGIN);
        }
    }

}
