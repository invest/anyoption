package com.trading.bubbles.fragment;

import com.trading.bubbles.BUApplication.BUScreen;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.fragment.ActionFragment;

import android.os.Bundle;

public class BUActionFragment extends ActionFragment {

	public static BUActionFragment newInstance(Bundle bundle){
		BUActionFragment fragment = new BUActionFragment();
		fragment.setArguments(bundle);
		return fragment;
	}
	
	@Override
	protected void setupSpecialCases(Screenable screen) {
		if(screen == BUScreen.FIRST_NEW_CARD){
			if(application.getUser() != null && application.getUser().getFirstDepositId() > 0){
				isBackbutton = true;
				setupBackButton();
			}else{
				isMenuButton = false;
				hideMenuButton();
			}
		} else if(screen == Screen.TRADE || screen == Screen.TABLET_TRADE){
			hideHelpButton();
			isMenuButton = false;
			hideMenuButton();
		}
	}
	
	@Override
	protected boolean shouldSetupTitle(Screenable screenable){
		return true;
	}
	
	@Override
	protected Screenable getScreenFromEvent(Screenable screenable){
		if(screenable == Screen.FIRST_NEW_CARD){
			screenable = BUScreen.FIRST_NEW_CARD;
		}
		return screenable;
	}
	
}
