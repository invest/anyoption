package com.trading.bubbles.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.Spannable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anyoption.android.app.Application;
import com.anyoption.common.beans.base.Skin;
import com.trading.bubbles.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.fragment.MenuFragment;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.widget.TextView;

import java.util.ArrayList;

/**
 * Created by veselinh on 23.11.2015 г..
 */
public class BUMenuFragment extends MenuFragment {

    private final String TAG = BUMenuFragment.class.getSimpleName();

    public static BUMenuFragment newInstance(Bundle bundle){
        BUMenuFragment fragment = new BUMenuFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        View footerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.menu_listview_footer_layout, null, false);

        TextView termsText = (TextView) footerView.findViewById(R.id.menu_bottom_text);

        TextView poweredByText = (TextView) rootView.findViewById(R.id.menu_powered_by_text_view);

        Spannable poweredByTextSpannable = TextUtils.decorateInnerText(poweredByText, null, poweredByText.getText().toString(), getString(R.string.poweredByAnyoptionBoldText), FontsUtils.Font.ROBOTO_MEDIUM, 0, 0);
        TextUtils.decorateInnerText(poweredByText, poweredByTextSpannable, poweredByText.getText().toString(), getString(R.string.poweredByAnyoptionLightBoldText), FontsUtils.Font.ROBOTO_REGULAR, 0, 0);

        TextUtils.OnInnerTextClickListener termsLinkListener = new TextUtils.OnInnerTextClickListener() {
            @Override
            public void onInnerTextClick(android.widget.TextView textView, String text, String innerText) {
                application.postEvent(new NavigationEvent(Application.Screen.TERMS_AND_CONDITIONS, NavigationEvent.NavigationType.DEEP));
            }
        };


        Spannable spannable = TextUtils.decorateInnerText(termsText, null, termsText.getText().toString(), getString(R.string.menu_bottom_text_risk_warning), FontsUtils.Font.ROBOTO_MEDIUM, 0, 0);
        TextUtils.makeClickableInnerText(termsText, spannable, termsText.getText().toString(), getString(R.string.menu_bottom_text_terms_link), null, 0, 0, true, termsLinkListener);

        menuList.addFooterView(footerView);
        return rootView;
    }

    protected void setUpItems() {
        items = new ArrayList<MenuItem>();

        if (!application.isLoggedIn()) {
            items.add(MenuItem.SIGN_UP);
        }

        items.add(MenuItem.BUBBLES);

        if (application.isLoggedIn()) {
            items.add(MenuItem.DEPOSIT);
            items.add(MenuItem.MY_ACCOUNT);
            items.add(MenuItem.MY_OPTIONS);
        }

        items.add(MenuItem.SUPPORT);
        if (application.isLoggedIn()) {
            //TODO Temporary solution:
            if(application.getUser().getSkinId() == Skin.SKIN_ETRADER_INT){
                items.add(MenuItem.AGREEMENT_TERMS);
            }
        }
        items.add(MenuItem.SETTINGS);

        if (application.isLoggedIn()) {
            items.add(MenuItem.LOGOUT);
        }
    }

}
