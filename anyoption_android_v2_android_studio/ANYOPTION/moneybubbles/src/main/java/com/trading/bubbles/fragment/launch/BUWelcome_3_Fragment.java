package com.trading.bubbles.fragment.launch;

import android.os.Bundle;
import android.text.Spannable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.TextUtils;
import com.trading.bubbles.R;
import com.anyoption.android.app.fragment.BaseFragment;

/**
 * @author Anastas Arnaudov
 */
public class BUWelcome_3_Fragment extends BaseFragment {

    public static final String TAG = BUWelcome_3_Fragment.class.getSimpleName();

    public static BUWelcome_3_Fragment newInstance() {
        BUWelcome_3_Fragment fragment = new BUWelcome_3_Fragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.welcome_3_fragment, container, false);

        TextView message1 = (TextView) rootView.findViewById(R.id.welcome_screen_3_text_1);
        TextView message2 = (TextView) rootView.findViewById(R.id.welcome_screen_3_text_2);

        TextUtils.decorateInnerText(message1, null, message1.getText().toString(), getString(R.string.introScreen3Message1_bold), FontsUtils.Font.ROBOTO_REGULAR, 0, 0);
        TextUtils.decorateInnerText(message2, null, message2.getText().toString(), getString(R.string.introScreen3Message2_bold), FontsUtils.Font.ROBOTO_REGULAR, 0, 0);

        return rootView;
    }

}
