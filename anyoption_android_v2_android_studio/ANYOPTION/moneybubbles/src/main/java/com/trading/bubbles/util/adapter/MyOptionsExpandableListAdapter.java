package com.trading.bubbles.util.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.anyoption.android.app.Application;
import com.trading.bubbles.BUApplication;
import com.trading.bubbles.R;
import com.trading.bubbles.model.MyOptionsGroup;
import com.anyoption.android.app.popup.SplitBalanceDialogFragment;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.InvestmentUtil;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.service.requests.DepositBonusBalanceMethodRequest;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Anastas Arnaudov
 */
public class MyOptionsExpandableListAdapter extends BaseExpandableListAdapter {

    public static final String TAG = MyOptionsExpandableListAdapter.class.getSimpleName();

    private class GroupHolder{
        public TextView titleTextView;
        public View expandButton;
        public View columnLabelsLayout;
        public View bubblesLinkView;
        public TextView bubblesLinkTextView;
    }

    private class ChildHolder{
        public ImageView assetIcon;
        public ImageView plusIcon;
        public TextView assetNameView;
        public TextView timeInvestedView;
        public TextView levelInvestedView;
        public TextView amountInvestedView;
        public TextView currentReturnView;
        public View profitableImageView;
        public View currentInfoLayout;
        public ImageView putCallIcon;
        public TextView levelView;
        public View balanceInfoButton;
        public View balanceInfoImage;
        public View returnBalanceInfoImage;
    }

    private static final String TIME_FORMAT_PATTERN         = "dd/MM/yyyy | HH:mm";
    private static final String TIME_FORMAT_PATTERN_BUBBLES = "dd/MM/yyyy | HH:mm:ss";

    private ExpandableListView expandableListView;
    private LayoutInflater inflater;
    private DecimalFormat percentDecimalFormat;
    private Resources resources;
    protected Application application;
    protected Currency currency;
    protected List<Investment> investments;
    private SimpleDateFormat timeFormat;
    private SimpleDateFormat timeFormatBubbles;
    private List<MyOptionsGroup> groups;
    private View.OnClickListener groupExpandButtonClickListener;
    private View.OnClickListener groupBubblesLinkClickListener;
    private View.OnClickListener balanceInfoClickListener;
    private View.OnClickListener returnBalanceInfoClickListener;

    public MyOptionsExpandableListAdapter(ExpandableListView expandableListView){
        application = Application.get();

        inflater = (LayoutInflater) application.getCurrentActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        resources = application.getResources();
        percentDecimalFormat = new DecimalFormat("##.##");
        if (application.isLoggedIn()) {
            currency = application.getCurrency();
        }
        timeFormat          = new SimpleDateFormat(TIME_FORMAT_PATTERN);
        timeFormatBubbles   = new SimpleDateFormat(TIME_FORMAT_PATTERN_BUBBLES);

        this.expandableListView = expandableListView;
        this.groups = new ArrayList<>();
        groupExpandButtonClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyOptionsGroup group = (MyOptionsGroup) v.getTag();
                group.setExpanded(!group.isExpanded());
                notifyDataSetChanged();
            }
        };
        groupBubblesLinkClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BUApplication)application).goToBubbles(null, null);
            }
        };
        balanceInfoClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Investment investment = (Investment) v.getTag();
                Bundle bundle = new Bundle();
                bundle.putLong(Constants.EXTRA_INVESTMENT_ID, investment.getId());
                application.getFragmentManager().showDialogFragment(SplitBalanceDialogFragment.class, bundle);
            }
        };
        returnBalanceInfoClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Investment investment = (Investment) v.getTag();
                Bundle bundle = new Bundle();
                bundle.putLong(Constants.EXTRA_INVESTMENT_ID, investment.getId());
                bundle.putInt(Constants.EXTRA_TYPE, DepositBonusBalanceMethodRequest.CALL_OPEN_INVESTMENTS_AMOUNT_BALANCE);
                application.getFragmentManager().showDialogFragment(SplitBalanceDialogFragment.class, bundle);
            }
        };
    }

    public MyOptionsGroup getGroupForType(MyOptionsGroup.Type type){
        MyOptionsGroup result = null;
        for(MyOptionsGroup group : getGroups()){
            if(group.getType() == type){
                result = group;
                break;
            }
        }
        return result;
    }

    public List<MyOptionsGroup> getGroups(){
        return groups;
    }

    public void setupGroups(List<Investment> investments){
        Log.d(TAG, "setupGroups");
        this.groups.clear();
        notifyDataSetChanged();

        if(investments != null && !investments.isEmpty()){
            List<MyOptionsGroup> groups = new ArrayList<MyOptionsGroup>();

            MyOptionsGroup binaryGroup		= null;
            MyOptionsGroup dynamicsGroup	= null;
            MyOptionsGroup optionPlusGroup	= null;
            MyOptionsGroup bubblesGroup		= null;

            for(Investment investment : investments){
                //Binary:
                if(investment.getOpportunityTypeId() != Opportunity.TYPE_OPTION_PLUS && (investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL || investment.getTypeId() == Investment.INVESTMENT_TYPE_PUT)){
                    if(binaryGroup == null){
                        binaryGroup = new MyOptionsGroup(MyOptionsGroup.Type.BINARY, application.getString(R.string.myOptionsBinaryGroupTitle).toUpperCase());
                    }
                    binaryGroup.addInvestments(investment);
                }
                //Dynamics:
                else if(investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY || investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_SELL){
                    if(dynamicsGroup == null){
                        dynamicsGroup = new MyOptionsGroup(MyOptionsGroup.Type.DYNAMICS, application.getString(R.string.myOptionsDynamicsGroupTitle).toUpperCase());
                    }
                    dynamicsGroup.addInvestments(investment);
                }
                //Option+:
                else if(investment.getOpportunityTypeId() == Opportunity.TYPE_OPTION_PLUS){
                    if(optionPlusGroup == null){
                        optionPlusGroup = new MyOptionsGroup(MyOptionsGroup.Type.OPTION_PLUS, application.getString(R.string.myOptionsOptionPlusGroupTitle).toUpperCase());
                    }
                    optionPlusGroup.addInvestments(investment);
                }
                //Bubbles:
                else if(investment.getTypeId() == Investment.INVESTMENT_TYPE_BUBBLES){
                    if(bubblesGroup == null){
                        bubblesGroup = new MyOptionsGroup(MyOptionsGroup.Type.BUBBLES, application.getString(R.string.myOptionsBubblesGroupTitle).toUpperCase());
                    }
                    bubblesGroup.addInvestments(investment);
                }
            }

            if(binaryGroup != null){
                groups.add(binaryGroup);
            }
            if(dynamicsGroup != null){
                groups.add(dynamicsGroup);
            }
            if(optionPlusGroup != null){
                groups.add(optionPlusGroup);
            }
            if(bubblesGroup != null){
                groups.add(bubblesGroup);
            }

            //Set the Groups to the Adapter:
            setGroups(groups);

            for(MyOptionsGroup g : groups){
                expandableListView.expandGroup(groups.indexOf(g));
            }

            //Expand the last Group:
            groups.get(groups.size()-1).setExpanded(true);

            notifyDataSetChanged();
        }else{
            this.groups.clear();
            notifyDataSetChanged();
        }
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        MyOptionsGroup group = groups.get(groupPosition);
        GroupHolder groupHolder;
        boolean isBubbles = group.getType() == MyOptionsGroup.Type.BUBBLES;

        if(convertView == null){
            convertView = inflater.inflate(R.layout.my_open_options_group_layout, parent, false);
            groupHolder = new GroupHolder();
            groupHolder.titleTextView 		= (TextView) convertView.findViewById(R.id.myOptionsGroupTitleTextView);
            groupHolder.expandButton 		=            convertView.findViewById(R.id.myOptionsGroupExpandView);
            groupHolder.columnLabelsLayout 	=            convertView.findViewById(R.id.myOptionsGroupColumnLabelsLayout);
            groupHolder.bubblesLinkView     =            convertView.findViewById(R.id.myOptionsGroupBubblesLinkLayout);
            groupHolder.bubblesLinkTextView = (TextView) convertView.findViewById(R.id.myOptionsGroupBubblesLinkTextView);

            convertView.setTag(groupHolder);
        }else{
            groupHolder = (GroupHolder) convertView.getTag();
        }

        groupHolder.titleTextView.setText(group.getTitle().toUpperCase() + " (" + group.getInvestments().size() + ")");
        groupHolder.expandButton.setTag(group);
        groupHolder.expandButton.setOnClickListener(groupExpandButtonClickListener);
        groupHolder.bubblesLinkTextView.setOnClickListener(groupBubblesLinkClickListener);
        TextUtils.underlineText(groupHolder.bubblesLinkTextView);

        if(!isBubbles && group.isExpanded()){
            groupHolder.columnLabelsLayout.setVisibility(View.VISIBLE);
            groupHolder.expandButton.setRotation(45f);
            groupHolder.bubblesLinkView.setVisibility(View.GONE);
        }else{
            groupHolder.columnLabelsLayout.setVisibility(View.GONE);
            groupHolder.expandButton.setRotation(0f);
            if(isBubbles){
                groupHolder.bubblesLinkView.setVisibility(View.VISIBLE);
            }else{
                groupHolder.bubblesLinkView.setVisibility(View.GONE);
            }
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        MyOptionsGroup group            = groups.get(groupPosition);
        Investment currentInvestment    = group.getInvestments().get(childPosition);
        Market market                   = application.getMarketForID(currentInvestment.getMarketId());
        ChildHolder childHolder;

        boolean isDynamics = currentInvestment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY || currentInvestment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_SELL;
        boolean isBubbles  = currentInvestment.getTypeId() == Investment.INVESTMENT_TYPE_BUBBLES;

        View rootView = convertView;
        if (convertView == null) {
            rootView = inflater.inflate(R.layout.trading_options_item_row, parent, false);
            childHolder = new ChildHolder();

            childHolder.assetIcon               = (ImageView) rootView.findViewById(R.id.trading_options_asset_image);
            childHolder.plusIcon                = (ImageView) rootView.findViewById(R.id.trading_options_option_plus_icon);
            childHolder.assetNameView           = (TextView)  rootView.findViewById(R.id.trading_options_asset_name);
            childHolder.timeInvestedView        = (TextView)  rootView.findViewById(R.id.trading_options_time_invested);
            childHolder.levelInvestedView       = (TextView)  rootView.findViewById(R.id.trading_options_level_invested);
            childHolder.amountInvestedView      = (TextView)  rootView.findViewById(R.id.trading_options_amount_invested);
            childHolder.currentReturnView       = (TextView)  rootView.findViewById(R.id.trading_options_current_return);
            childHolder.profitableImageView     =             rootView.findViewById(R.id.trading_options_profitable_image);
            childHolder.currentInfoLayout       =             rootView.findViewById(R.id.trading_options_current_info_layout);
            childHolder.putCallIcon             = (ImageView) rootView.findViewById(R.id.trading_options_put_call_icon);
            childHolder.levelView               = (TextView)  rootView.findViewById(R.id.trading_options_level);
            childHolder.balanceInfoButton       =             rootView.findViewById(R.id.trading_options_investment_info_layout);
            childHolder.balanceInfoImage        =             rootView.findViewById(R.id.trading_options_profitable_balance_info);
            childHolder.returnBalanceInfoImage  =             rootView.findViewById(R.id.trading_options_profitable_return_balance_info);

            rootView.setTag(childHolder);
        }else{
            childHolder = (ChildHolder) rootView.getTag();
        }

        if (market == null) {
            childHolder.assetIcon.setImageDrawable(DrawableUtils.getMarketIconForId(currentInvestment.getMarketId()));
        } else {
            childHolder.assetIcon.setImageDrawable(DrawableUtils.getMarketIcon(market));
        }


        if (currentInvestment.getOpportunityTypeId() == Opportunity.TYPE_OPTION_PLUS) {
            childHolder.plusIcon.setImageResource(R.drawable.option_plus_icon_1);
            childHolder.plusIcon.setVisibility(View.VISIBLE);
        }else if(isDynamics){
            childHolder.plusIcon.setImageResource(R.drawable.dynamics_icon_1);
            childHolder.plusIcon.setVisibility(View.VISIBLE);
        }else if(isBubbles){
            childHolder.plusIcon.setImageResource(R.drawable.bubblesMarketIcon);
            childHolder.plusIcon.setVisibility(View.VISIBLE);
        }else {
            childHolder.plusIcon.setVisibility(View.GONE);
        }

        childHolder.assetNameView.setText(currentInvestment.getAsset());
        if(isBubbles){
            childHolder.timeInvestedView.setText(timeFormatBubbles.format(currentInvestment.getTimeCreated()));
        }else{
            childHolder.timeInvestedView.setText(timeFormat.format(currentInvestment.getTimeCreated()));
        }
        childHolder.levelInvestedView.setText(currentInvestment.getLevel());
        if(isDynamics){
            //Format Percent like : (+26.23%)
            formatDynamicsProfit(childHolder.amountInvestedView, currentInvestment);
        }else{
            String newAmount = AmountUtil.insertIntervalBetweenAmountAndSymbol(currentInvestment.getAmountTxt(), application.getUser().getCurrency());
            childHolder.amountInvestedView.setText(newAmount);
        }

        if(isBubbles){
            childHolder.currentReturnView.setText("");
        }else if(isDynamics){
            if(InvestmentUtil.isOpenInvestmentProfitable(currentInvestment, currentInvestment.getCurrentLevel())){
                childHolder.currentReturnView.setText(AmountUtil.getFormattedAmount(InvestmentUtil.getDynamicsOpenInvestmentReturnAmount(currentInvestment), currency));
            }else{
                childHolder.currentReturnView.setText(AmountUtil.getFormattedAmount(0, currency));
            }
        }else{
            childHolder.currentReturnView.setText(currentInvestment.getAmountReturnWF());
        }

        if (!isBubbles && InvestmentUtil.isOpenInvestmentProfitable(currentInvestment, currentInvestment.getCurrentLevel())) {
            childHolder.profitableImageView.setVisibility(View.VISIBLE);
            childHolder.currentInfoLayout.setBackgroundColor(ColorUtils.getColor(R.color.trading_options_in_the_money));
            setPutCallIcon(currentInvestment, childHolder.putCallIcon, true);
        } else {
            childHolder.profitableImageView.setVisibility(View.GONE);
            childHolder.currentInfoLayout.setBackgroundColor(ColorUtils.getColor(R.color.trading_options_out_of_the_money));
            setPutCallIcon(currentInvestment, childHolder.putCallIcon, false);
        }

        if(isBubbles){
            childHolder.levelView.setText("");
        }else{
            childHolder.levelView.setText(currentInvestment.getCurrentLevelTxt());
        }
        childHolder.balanceInfoImage.setVisibility(View.GONE);
        childHolder.returnBalanceInfoImage.setVisibility(View.VISIBLE);
        childHolder.currentInfoLayout.setTag(currentInvestment);
        childHolder.currentInfoLayout.setOnClickListener(returnBalanceInfoClickListener);

        return rootView;
    }

    public void setGroups(List<MyOptionsGroup> groups){
        this.groups = groups;
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        MyOptionsGroup group = groups.get(groupPosition);
        boolean isBubbles = group.getType() == MyOptionsGroup.Type.BUBBLES;
        if(!isBubbles && group.isExpanded()){
            return group.getInvestments().size();
        }else{
            return 0;
        }
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return groups.get(groupPosition).getInvestments().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    protected void formatDynamicsProfit(TextView textView, Investment currentInvestment){
        //Format Profit Percent. Example : (+26.23%)
        if(currentInvestment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY || currentInvestment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_SELL){
            double profitPercent = InvestmentUtil.getDynamicsOpenInvestmentProfitPercent(currentInvestment);
            String sign = "";
            if(profitPercent>0){
                sign = "+";
            }else if(profitPercent<0){
                sign = "-";
            }
            String percentFormatted = " (" + sign + percentDecimalFormat.format(profitPercent) + "%)";
            String amountAndPercent = AmountUtil.insertIntervalBetweenAmountAndSymbol(currentInvestment.getAmountTxt(), application.getUser().getCurrency()) + percentFormatted;
            textView.setText(amountAndPercent);
            int percentTextSize 	= (int) (textView.getTextSize() * 6 / 7);
            TextUtils.decorateInnerText(textView, null, amountAndPercent, percentFormatted, null, 0, percentTextSize);
        }
    }

    @SuppressWarnings("static-method")
    protected void setPutCallIcon(Investment investment, ImageView view, boolean isProfitable) {
        long type = investment.getTypeId();

        boolean isBubbles  = type == Investment.INVESTMENT_TYPE_BUBBLES;

        if(isBubbles){
            view.setVisibility(View.GONE);
        }else{
            view.setVisibility(View.VISIBLE);
            if ((type == Investment.INVESTMENT_TYPE_CALL || type == Investment.INVESTMENT_TYPE_DYNAMICS_BUY) && isProfitable) {
                view.setImageResource(com.anyoption.android.R.drawable.trading_options_call_win);
            } else if ((type == Investment.INVESTMENT_TYPE_CALL  || type == Investment.INVESTMENT_TYPE_DYNAMICS_BUY) && !isProfitable) {
                view.setImageResource(com.anyoption.android.R.drawable.trading_options_call_lose);
            } else if ((type == Investment.INVESTMENT_TYPE_PUT || type == Investment.INVESTMENT_TYPE_DYNAMICS_SELL) && isProfitable) {
                view.setImageResource(com.anyoption.android.R.drawable.trading_options_put_win);
            } else {
                view.setImageResource(com.anyoption.android.R.drawable.trading_options_put_lose);
            }
        }

    }

}
