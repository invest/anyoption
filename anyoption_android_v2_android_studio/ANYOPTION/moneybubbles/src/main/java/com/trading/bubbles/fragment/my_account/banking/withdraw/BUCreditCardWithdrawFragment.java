package com.trading.bubbles.fragment.my_account.banking.withdraw;

import com.anyoption.android.app.Application.Screen;
import com.trading.bubbles.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.my_account.banking.withdraw.CreditCardWithdrawFragment;
import com.anyoption.android.app.manager.PopUpManager;
import com.anyoption.android.app.popup.WithdrawBonusResetDialogFragment;
import com.anyoption.android.app.popup.WithdrawBonusResetDialogFragment.WithdrawBonusResetPopupListener;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.requests.InsertWithdrawCancelMethodRequest;
import com.anyoption.common.service.requests.WithdrawBonusRegulationStateMethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.WithdrawBonusRegulationStateMethodResult;

import android.os.Bundle;
import android.util.Log;

public class BUCreditCardWithdrawFragment extends CreditCardWithdrawFragment {

    public static final int TRANS_TYPE_CC_WITHDRAW = 10;

    public static BUCreditCardWithdrawFragment newInstance(Bundle bundle) {
        BUCreditCardWithdrawFragment creditCardWithdrawFragment = new BUCreditCardWithdrawFragment();
        creditCardWithdrawFragment.setArguments(bundle);
        return creditCardWithdrawFragment;
    }

    @Override
    protected void tryToWithdraw() {
        getWithdrawBonusRegulationState();
    }

    private void getWithdrawBonusRegulationState() {
        WithdrawBonusRegulationStateMethodRequest withdrawBonusRegulationStateMethodRequest = new WithdrawBonusRegulationStateMethodRequest();
        withdrawBonusRegulationStateMethodRequest.setTransactionTypeId(TRANS_TYPE_CC_WITHDRAW);
        withdrawBonusRegulationStateMethodRequest.setCcId(creditCard.getId());
        application.getCommunicationManager().requestService(this, "getWithdrawBonusRegulationStateCallback", Constants.SERVICE_GET_WITHDRAW_BONUS_REGULATION_STATE, withdrawBonusRegulationStateMethodRequest, WithdrawBonusRegulationStateMethodResult.class, false, false);
    }

    public void getWithdrawBonusRegulationStateCallback(Object resObj) {
        WithdrawBonusRegulationStateMethodResult result = (WithdrawBonusRegulationStateMethodResult) resObj;
        if (result != null && result instanceof WithdrawBonusRegulationStateMethodResult && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            if (result.getState() == WithdrawBonusRegulationStateMethodResult.WithdrawBonusStateEnum.NO_OPEN_BONUSES) {
                super.tryToWithdraw();
            } else if (result.getState() == WithdrawBonusRegulationStateMethodResult.WithdrawBonusStateEnum.NOT_REGULATED_OPEN_BONUSES) {
                application.getPopUpManager().showAlertPopUp(null, getString(R.string.disabledWithdrawWhenActiveBonuses), getString(R.string.supportScreenTitle), new PopUpManager.OnAlertPopUpButtonPressedListener() {
                    @Override
                    public void onButtonPressed() {
                        application.postEvent(new NavigationEvent(Screen.SUPPORT, NavigationType.INIT));
                    }
                });
            } else if (result.getState() == WithdrawBonusRegulationStateMethodResult.WithdrawBonusStateEnum.REGULATED_OPEN_BONUSES) {
                final WithdrawBonusResetDialogFragment bonusResetDialogFragment = (WithdrawBonusResetDialogFragment) application.getFragmentManager().showDialogFragment(WithdrawBonusResetDialogFragment.class, null);
                bonusResetDialogFragment.setListener(new WithdrawBonusResetPopupListener() {
                    @Override
                    public void onYes() {
                        BUCreditCardWithdrawFragment.super.tryToWithdraw();
                        bonusResetDialogFragment.dismiss();
                    }

                    @Override
                    public void onNo() {
                        InsertWithdrawCancelMethodRequest insertWithdrawCancelMethodRequest = new InsertWithdrawCancelMethodRequest();
                        insertWithdrawCancelMethodRequest.setTransactionTypeId(TRANS_TYPE_CC_WITHDRAW);
                        insertWithdrawCancelMethodRequest.setCcId(creditCard.getId());
                        application.getCommunicationManager().requestService(this, "insertWithdrawCancelCallback", Constants.SERVICE_INSERT_WITHDRAW_CANCEL, insertWithdrawCancelMethodRequest, MethodResult.class, false, false);

                        bonusResetDialogFragment.dismiss();
                        application.postEvent(new NavigationEvent(Screen.TRADE, NavigationType.INIT));
                    }
                });
            }
        } else {
            Log.e(TAG, "Error in getWithdrawBonusRegulationStatus");
            ErrorUtils.handleResultError(result);
        }
    }

    public void insertWithdrawCancelCallback(Object resObj) {

    }
}
