package com.trading.bubbles.fragment.trade;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.fragment.NavigationalFragment;

/**
 * Created by veselin.hristozov on 17.8.2016 г..
 */
public class BUTradeFragment extends NavigationalFragment {

    public static BUTradeFragment newInstance(Bundle bundle){
        BUTradeFragment tradeFragment = new BUTradeFragment();
        tradeFragment.setArguments(bundle);
        return tradeFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(com.anyoption.android.R.layout.trade_fragment_layout, container, false);

        return rootView;
    }

    @Override
    protected Application.Screenable setupScreen() {
        return Application.Screen.TRADE;
    }
}
