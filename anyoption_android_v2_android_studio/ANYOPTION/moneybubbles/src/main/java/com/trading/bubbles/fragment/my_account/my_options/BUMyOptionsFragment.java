package com.trading.bubbles.fragment.my_account.my_options;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.fragment.my_account.SettledOptionsFragment;
import com.anyoption.android.app.fragment.my_account.my_options.MyOptionsFragment;
import com.anyoption.android.app.model.DropDownPopupItem;
import com.anyoption.android.app.popup.DropdownPopup;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.AutoResizeTextView;

import java.util.ArrayList;

/**
 * Created by veselin.hristozov on 17.8.2016 г..
 */
public class BUMyOptionsFragment extends MyOptionsFragment {

    public static BUMyOptionsFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created fragment BUMyOptionsFragment");
        BUMyOptionsFragment fragment = new BUMyOptionsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    //////////////////////////////////////////////////////////////////////
    //                            EVENTs                                //
    //////////////////////////////////////////////////////////////////////

    @Override
    public void onEventMainThread(NavigationEvent event) {
        if (event.getToScreen() != Application.Screen.MY_OPTIONS) {
            return;
        }
    }

    //////////////////////////////////////////////////////////////////////

    @Override
    protected void setupTabs() {
        openOptionsTab = rootView.findViewById(com.anyoption.android.R.id.my_options_open_options_tab);
        settledOptionsTab = rootView.findViewById(com.anyoption.android.R.id.my_options_settled_options_tab);
        dropDownImageView = (ImageView) rootView.findViewById(com.anyoption.android.R.id.my_options_settled_dropdown_image_view);
        settledOptionsTextView = (AutoResizeTextView) rootView.findViewById(com.anyoption.android.R.id.my_options_settled_text);
        currentTabLayout = rootView.findViewById(com.anyoption.android.R.id.my_options_current_tab_layout);

        if (dropDownPopupItems == null || dropDownPopupItems.size() < 1) {
            currentDropDownPosition = PERIOD_DAY;
            changeSettledOptionsSelectedFilter(currentDropDownPosition);
            dropDownPopupItems = new ArrayList<DropDownPopupItem>();
            dropDownPopupItems.add(new DropDownPopupItem(PERIOD_DAY, getString(com.anyoption.android.R.string.settled_options_dropdown_day_text), true));
            dropDownPopupItems.add(new DropDownPopupItem(PERIOD_WEEK, getString(com.anyoption.android.R.string.settled_options_dropdown_week_text), false));
            dropDownPopupItems.add(new DropDownPopupItem(PERIOD_MONTH, getString(com.anyoption.android.R.string.settled_options_dropdown_month_text), false));
        }

        View.OnClickListener dropDownClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dropDownImageView.setImageResource(com.anyoption.android.R.drawable.dd_arrow_up);
                    DropdownPopup.showPopup(dropDownPopupItems, dropDownImageView, BUMyOptionsFragment.this);
            }
        };

        dropDownImageView.setOnClickListener(dropDownClickListener);

        openOptionsTab.setVisibility(View.GONE);
    }

    @Override
    protected void selectTab() {
            settledOptionsTab.setBackgroundColor(ColorUtils.getColor(com.anyoption.android.R.color.my_options_tab_pressed_bg));
    }

    @Override
    protected void openTab() {
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.EXTRA_PERIOD, currentDropDownPosition);
        application.getFragmentManager().replaceInnerFragment(currentTabLayout.getId(), SettledOptionsFragment.class, this, bundle);
    }
}
