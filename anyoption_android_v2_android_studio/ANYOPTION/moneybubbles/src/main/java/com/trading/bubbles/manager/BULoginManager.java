package com.trading.bubbles.manager;

import android.os.Bundle;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.manager.LoginManager;
import com.anyoption.android.app.util.RegulationUtils;
import com.trading.bubbles.BUApplication;

/**
 * Created by veselin.hristozov on 17.8.2016 г..
 */
public class BULoginManager extends LoginManager {

    @Override
    protected void doAfterLoginNavigation(Application.Screenable toScreen, Bundle bundle) {
        if (toScreen != null) {
            if(toScreen == Application.Screen.TRADE || toScreen == Application.Screen.TABLET_TRADE) {
                if(RegulationUtils.isAfterFirstDeposit() && !RegulationUtils.shouldShowRegulationDialogs() && application.getMinInvestmentAmount() <= 0L) {
                    ((BUApplication)application).goToBubbles(toScreen, bundle);
                } else {
                    application.postEvent(new NavigationEvent(toScreen, NavigationEvent.NavigationType.INIT, bundle));
                }
            } else {
                application.postEvent(new NavigationEvent(toScreen, NavigationEvent.NavigationType.INIT, bundle));
            }
        }
    }
}
