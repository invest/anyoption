package com.trading.bubbles.fragment.login_register;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.trading.bubbles.BUApplication;
import com.trading.bubbles.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.login_register.RegisterFragment;
import com.anyoption.common.service.results.UserMethodResult;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class BURegisterFragment extends RegisterFragment {

	public static BURegisterFragment newInstance(Bundle bundle){
		BURegisterFragment registerFragment = new BURegisterFragment();
		registerFragment.setArguments(bundle);
		return registerFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		View rootView = super.onCreateView(inflater, container, savedInstanceState); 
		TextView loginLinkTextView = (TextView) rootView.findViewById(R.id.register_screen_login_link_textview);
		loginLinkTextView.setText(getString(R.string.login) + " »");
		loginLinkTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				application.postEvent(new NavigationEvent(Screen.LOGIN, NavigationType.INIT));
			}
		});
		
		return rootView;
	}
	
	@Override
	protected void underlineTryWithoutAccountText(){
		//Ignore
	}

	@Override
	protected Application.Screenable configureAfterRegisterScreen(UserMethodResult result) {
		Application.Screenable afterRegisterScreen = super.configureAfterRegisterScreen(result);

		Log.d(TAG, "Check if the user has registered through a Bonus campaign.");
		if(result.getUser().isHasBonus()){
			Log.d(TAG, "The user has registered through a Bonus campaign.");
			afterRegisterScreen = Screen.BONUSES;
		}

		return afterRegisterScreen;
	}

	protected void goToTradeScreen() {
		((BUApplication)application).goToBubbles(null, null);
	}

}
