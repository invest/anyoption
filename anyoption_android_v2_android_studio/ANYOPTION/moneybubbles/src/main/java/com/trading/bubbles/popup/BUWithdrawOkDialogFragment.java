package com.trading.bubbles.popup;

import com.trading.bubbles.R;
import com.anyoption.android.app.popup.WithdrawOkDialogFragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class BUWithdrawOkDialogFragment extends WithdrawOkDialogFragment {

	public static final String TAG = BUWithdrawOkDialogFragment.class.getSimpleName();

	/**
	 * Use this method to create new instance of the fragment.
	 * @return
	 */
	public static BUWithdrawOkDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new BUWithdrawOkDialogFragment.");
		BUWithdrawOkDialogFragment fragment = new BUWithdrawOkDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		fragment.setArguments(bundle);
		return fragment;
	}
	
	@Override
	protected void onCreateContentView(View contentView) {
		super.onCreateContentView(contentView);
		View closeButton = contentView.findViewById(R.id.dialog_close);
		closeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}

}