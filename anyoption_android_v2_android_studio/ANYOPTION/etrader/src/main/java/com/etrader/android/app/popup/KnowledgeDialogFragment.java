package com.etrader.android.app.popup;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.RequestButton.InitAnimationListener;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.requests.UserRegulationRequest;
import com.etrader.android.app.ETApplication.ETScreen;
import com.etrader.android.app.R;
import com.etrader.android.app.util.ETConstants;
import com.etrader.android.app.util.ETErrorUtils;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class KnowledgeDialogFragment extends BaseDialogFragment {
	
	public static final String TAG = KnowledgeDialogFragment.class.getSimpleName();

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static KnowledgeDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new KnowledgeDialogFragment.");
		KnowledgeDialogFragment fragment = new KnowledgeDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		fragment.setArguments(bundle);
		return fragment;
	}

	private RequestButton noButton;
	private RequestButton yesButton;

	@Override
	protected int getContentLayout() {
		return R.layout.knowledge_popup_layout;
	}
	
	@Override
	protected void onCreateContentView(View contentView) {
		setCancelable(false);
		noButton = (RequestButton) contentView.findViewById(R.id.popup_negative_button);
		yesButton = (RequestButton) contentView.findViewById(R.id.popup_positive_button);
		
		noButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				yesButton.disable();
				noButton.startLoading(new InitAnimationListener() {
					@Override
					public void onAnimationFinished() {
						submit(false);
					}
				});
			}
		});
		
		yesButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				noButton.disable();
				yesButton.startLoading(new InitAnimationListener() {
					@Override
					public void onAnimationFinished() {
						submit(true);
					}
				});
			}
		});
		
	}
	
	private void goToQuestionnaire(){
		application.postEvent(new NavigationEvent(ETScreen.ETQUESTIONNAIRE, NavigationType.INIT));
	}
	
	//////////				REQUESTs and RESPONSEs			///////////////////////
	
	private void submit(boolean isOK){
		UserRegulationRequest request = new UserRegulationRequest();
		UserRegulationBase regulation = application.getUserRegulation();
		regulation.setKnowledgeQuestion(isOK);
		request.setUserRegulation(regulation);
		
		if(!Application.get().getCommunicationManager().requestService(KnowledgeDialogFragment.this, "callback", ETConstants.SERVICE_KNOWLEDGE_QUESTION, request, MethodResult.class, false, false)){
			yesButton.stopLoading();
			noButton.stopLoading();
			yesButton.enable();
			noButton.enable();
		}
	}
	
	public void callback(Object resultObject){
		Log.d(TAG, "Callback.");
		
		yesButton.stopLoading();
		noButton.stopLoading();
		yesButton.enable();
		noButton.enable();
		
		MethodResult result = (MethodResult) resultObject;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Callback. OK.");
			if(application.getUserRegulation().isKnowledgeQuestion()){
				dismiss();
				goToQuestionnaire();
			}else{
				dismiss();
				application.getFragmentManager().showDialogFragment(BlockedDialogFragment.class, null);
			}
		} else {
			Log.d(TAG, "Callback. ERROR.");
			ETErrorUtils.handleResultError(result);
		}
	}
	
	//////////////////////////////////////////////////////////////////////////

}
