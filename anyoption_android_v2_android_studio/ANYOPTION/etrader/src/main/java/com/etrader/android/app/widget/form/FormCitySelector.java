package com.etrader.android.app.widget.form;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.LayoutDirection;
import com.anyoption.android.app.widget.form.FormSelector;
import com.anyoption.beans.base.City;
import com.etrader.android.app.R;
import com.etrader.android.app.popup.CitySearchDialogFragment;

public class FormCitySelector extends FormSelector<City>  implements CitySearchDialogFragment.ItemSelectListener{

	private static final float SCALE_HINT_RATIO = 0.65f;
	private View view;
	private TextView text;
	private TextView hintTextView;
	private boolean isHintUp = false;
	private LinearLayout layout;
	private CitySearchDialogFragment cityDialog;

	public FormCitySelector(Context context) {
		super(context);
	}

	public FormCitySelector(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public FormCitySelector(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void setupUI() {
		if(view == null){
			view = View.inflate(getContext(), R.layout.city_selector_layout, this);
			layout = (LinearLayout) view.findViewById(R.id.city_selector_layout);
			text = (TextView) findViewById(R.id.city_selector_text);
			hintTextView = (TextView) findViewById(R.id.city_selector_hint);
			text.setTextColor(getResources().getColorStateList(R.color.black_white));
		}
		isSetDefaultItem = false;
		if(!isInEditMode()){
			setupUIForDirection();
			if(getValue() != null){
				text.setText(getValue().getName());
			}
		}else{
			text.setText("City Selector");
		}
	}

	@Override
	public void onClick(View v) {
		cityDialog = (CitySearchDialogFragment) Application.get().getFragmentManager().showDialogFragment(CitySearchDialogFragment.class, null);
		cityDialog.setItemSelectListener(this);
	}

	@Override
	public void setValue(Object value) {
		super.setValue(value);
		if(value != null){
			if(!isHintUp ){
				isHintUp = true;
				moveHint();
			}
			text.setVisibility(View.VISIBLE);
		}
		setupUI();
	}

	@Override
	public boolean checkValidity() {
		return getValue() != null;
	}

	private void moveHint() {
		layout.setGravity(Gravity.TOP);
		float hinttextSize = hintTextView.getTextSize();
		hintTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, hinttextSize*SCALE_HINT_RATIO);
	}

	@SuppressLint("RtlHardcoded")
	@Override
	public void setupUIForDirection() {
		if(getDirection() == LayoutDirection.LEFT_TO_RIGHT){
			text.setGravity(Gravity.LEFT);
		} else if (getDirection() == LayoutDirection.RIGHT_TO_LEFT) {
			text.setGravity(Gravity.RIGHT);
		}
	}

	@Override
	public void setFieldName(String fieldName) {}

	@Override
	public String getFieldName() {
		return null;
	}

	@Override
	public void displayError(String error) {}

	@Override
	public void onItemSelected(City city) {
		cityDialog.dismiss();
		setValue(city);
	}
}
