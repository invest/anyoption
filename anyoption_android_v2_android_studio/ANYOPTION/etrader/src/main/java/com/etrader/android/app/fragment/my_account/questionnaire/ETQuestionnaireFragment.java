package com.etrader.android.app.fragment.my_account.questionnaire;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.RequestButton.InitAnimationListener;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.json.requests.UpdateUserQuestionnaireAnswersRequest;
import com.anyoption.json.results.UserQuestionnaireResult;
import com.etrader.android.app.ETApplication.ETScreen;
import com.etrader.android.app.R;
import com.etrader.android.app.model.Answer;
import com.etrader.android.app.model.Question;
import com.etrader.android.app.util.ETConstants;
import com.etrader.android.app.util.ETErrorUtils;
import com.etrader.android.app.util.ETRegulationUtils;
import com.etrader.android.app.widget.form.FormQuestionSelector;
import com.etrader.android.app.widget.form.FormQuestionSelector.OnItemSelectedListener;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class ETQuestionnaireFragment extends NavigationalFragment implements OnItemSelectedListener{

	private static final String TAG = ETQuestionnaireFragment.class.getSimpleName();

	public static ETQuestionnaireFragment newInstance(Bundle bundle) {
		ETQuestionnaireFragment fragment = new ETQuestionnaireFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	private RequestButton submitButton;
	private FormQuestionSelector formQuestion_1_Selector;
	private FormQuestionSelector formQuestion_2_Selector;
	private FormQuestionSelector formQuestion_3_Selector;
	private FormQuestionSelector formQuestion_4_Selector;
	private FormQuestionSelector formQuestion_5_Selector;
	private FormQuestionSelector formQuestion_6_Selector;
	
	private boolean isLoading = true;
	
	@SuppressLint("UseSparseArrays")
	private Map<Long, FormQuestionSelector> questionSelectorsMap = new HashMap<Long, FormQuestionSelector>();
	private List<QuestionnaireUserAnswer> userAnswers;
	private View loadingView;
	
	@Override
	protected Screenable setupScreen() {
		return ETScreen.ETQUESTIONNAIRE;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getQuestionnaire();
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.questionnaire_layout, container, false);	
		
		loadingView = rootView.findViewById(R.id.questionnaireLoadingView);
		
		formQuestion_1_Selector = (FormQuestionSelector) rootView.findViewById(R.id.questionnaire_question_1_selector);
		formQuestion_2_Selector = (FormQuestionSelector) rootView.findViewById(R.id.questionnaire_question_2_selector);
		formQuestion_3_Selector = (FormQuestionSelector) rootView.findViewById(R.id.questionnaire_question_3_selector);
		formQuestion_4_Selector = (FormQuestionSelector) rootView.findViewById(R.id.questioannaire_question_4_selector);
		formQuestion_5_Selector = (FormQuestionSelector) rootView.findViewById(R.id.questionnaire_question_5_selector);
		formQuestion_6_Selector = (FormQuestionSelector) rootView.findViewById(R.id.questionnaire_question_6_selector);
		
		formQuestion_1_Selector.setOnItemSelectedListener(this);
		formQuestion_2_Selector.setOnItemSelectedListener(this);
		formQuestion_3_Selector.setOnItemSelectedListener(this);
		formQuestion_4_Selector.setOnItemSelectedListener(this);
		formQuestion_5_Selector.setOnItemSelectedListener(this);
		formQuestion_6_Selector.setOnItemSelectedListener(this);
		
		submitButton = (RequestButton) rootView.findViewById(R.id.questionnaire_submit_button);
		submitButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				submitButton.startLoading(new InitAnimationListener() {
					@Override
					public void onAnimationFinished() {
						submitQuestionnaire();
					}
				});
			}
		});
		
		setupLoading();
		setupSubmitButton();
		
		loadQuestions();
		
		return rootView;
	}

	private void setupLoading(){
		if(loadingView != null){
			if(isLoading){
				loadingView.setVisibility(View.VISIBLE);
			}else{
				loadingView.setVisibility(View.GONE);
			}
		}
	}
	
	private void setupSubmitButton(){
		if(submitButton != null){
			boolean isAllQuestionsAnswered = isQuestionsAnswered();
			if(!isLoading && isAllQuestionsAnswered){
				submitButton.enable();
			}else{
				submitButton.disable();
			}
		}
	}
	
	private void loadQuestions(){
		
		Question question1 = new Question(61l, getString(R.string.questionnaireScreenQuestion_1), new Answer[]{
			new Answer(282l, getString(R.string.questionnaireScreenQuestion_1_Answer_1)),
			new Answer(283l, getString(R.string.questionnaireScreenQuestion_1_Answer_2)),
			new Answer(284l, getString(R.string.questionnaireScreenQuestion_1_Answer_3)),
			new Answer(285l, getString(R.string.questionnaireScreenQuestion_1_Answer_4)),
			new Answer(286l, getString(R.string.questionnaireScreenQuestion_1_Answer_5))
		});
		
		Question question2 = new Question(62l, getString(R.string.questionnaireScreenQuestion_2), new Answer[]{
			new Answer(287l, getString(R.string.questionnaireScreenQuestion_2_Answer_1)),
			new Answer(288l, getString(R.string.questionnaireScreenQuestion_2_Answer_2)),
			new Answer(289l, getString(R.string.questionnaireScreenQuestion_2_Answer_3)),
			new Answer(290l, getString(R.string.questionnaireScreenQuestion_2_Answer_4))
		});
		
		Question question3 = new Question(63l, getString(R.string.questionnaireScreenQuestion_3), new Answer[]{
			new Answer(291l, getString(R.string.questionnaireScreenQuestion_3_Answer_1)),
			new Answer(292l, getString(R.string.questionnaireScreenQuestion_3_Answer_2)),
			new Answer(293l, getString(R.string.questionnaireScreenQuestion_3_Answer_3))
		});
		
		Question question4 = new Question(64l, getString(R.string.questionnaireScreenQuestion_4), new Answer[]{
			new Answer(294l, getString(R.string.questionnaireScreenQuestion_4_Answer_1)),
			new Answer(295l, getString(R.string.questionnaireScreenQuestion_4_Answer_2)),
			new Answer(296l, getString(R.string.questionnaireScreenQuestion_4_Answer_3))
		});
		
		Question question5 = new Question(65l, getString(R.string.questionnaireScreenQuestion_5), new Answer[]{
			new Answer(297l, getString(R.string.questionnaireScreenQuestion_5_Answer_1)),
			new Answer(298l, getString(R.string.questionnaireScreenQuestion_5_Answer_2))
		});
		
		Question question6 = new Question(66l, getString(R.string.questionnaireScreenQuestion_6), new Answer[]{
			new Answer(299l, getString(R.string.questionnaireScreenQuestion_6_Answer_1)),
			new Answer(300l, getString(R.string.questionnaireScreenQuestion_6_Answer_2)),
			new Answer(301l, getString(R.string.questionnaireScreenQuestion_6_Answer_3)),
			new Answer(302l, getString(R.string.questionnaireScreenQuestion_6_Answer_4))
		});
		
		formQuestion_1_Selector.setQuestion(question1);
		formQuestion_2_Selector.setQuestion(question2);
		formQuestion_3_Selector.setQuestion(question3);
		formQuestion_4_Selector.setQuestion(question4);
		formQuestion_5_Selector.setQuestion(question5);
		formQuestion_6_Selector.setQuestion(question6);
		
		questionSelectorsMap.put(question1.getId(), formQuestion_1_Selector);
		questionSelectorsMap.put(question2.getId(), formQuestion_2_Selector);
		questionSelectorsMap.put(question3.getId(), formQuestion_3_Selector);
		questionSelectorsMap.put(question4.getId(), formQuestion_4_Selector);
		questionSelectorsMap.put(question5.getId(), formQuestion_5_Selector);
		questionSelectorsMap.put(question6.getId(), formQuestion_6_Selector);
		
		updateQuestionsSelectors();
	}
	
	private void updateQuestionsSelectors() {
		if(userAnswers != null && formQuestion_6_Selector != null){
			for(QuestionnaireUserAnswer answer : userAnswers){
				FormQuestionSelector formQuestionSelector = questionSelectorsMap.get(answer.getQuestionId());
				if(formQuestionSelector != null){
					Question question = formQuestionSelector.getQuestion();
					Answer value = question.getAnswerForId(answer.getAnswerId());
					if(value != null){
						formQuestionSelector.setValue(value);
					}
				}
			}
		}
	}
	
	private void updateUserAnswers(){
		for(QuestionnaireUserAnswer answer : userAnswers){
			FormQuestionSelector formQuestionSelector = questionSelectorsMap.get(answer.getQuestionId());
			if(formQuestionSelector != null){
				Answer value = formQuestionSelector.getValue();
				if(value != null){
					answer.setAnswerId(value.getId());
				}
			}
		}
	}
	
	private boolean isQuestionsAnswered(){
		boolean result = true;
		if(userAnswers != null){
			for(QuestionnaireUserAnswer answer : userAnswers){
				if(answer.getAnswerId() == null){
					result = false;
					break;
				}
			}
		}
		return result;
	}
	
	@Override
	public void onItemSelected(Answer answer) {
		updateUserAnswers();
		updateQuestionnaire();
		setupSubmitButton();
	}
	
	///////////				REQUESTs and RESPONSEs			////////////////////
	
	private void getQuestionnaire(){
		Log.d(TAG, "getQuestionnaire");
		if(application.getCommunicationManager().requestService(this, "getQuestionnaireCallback", ETConstants.SERVICE_GET_QUESTIONNAIRE, new UserMethodRequest(), UserQuestionnaireResult.class, false, false)){
			isLoading = false;
			setupLoading();
		}
	}
	
	public void getQuestionnaireCallback(Object resultObj){
		Log.d(TAG, "getQuestionnaireCallback");
		isLoading = false;
		setupLoading();
		UserQuestionnaireResult result = (UserQuestionnaireResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Got user saved answers.");
			userAnswers = result.getUserAnswers();
			updateQuestionsSelectors();
			setupSubmitButton();
		}else {
			Log.e(TAG, "Getting user answers failed");
		}
	}
	
	private void updateQuestionnaire() {
		Log.d(TAG, "updateQuestionnaire");
		UpdateUserQuestionnaireAnswersRequest request = new UpdateUserQuestionnaireAnswersRequest();
		updateUserAnswers();
		request.setUserAnswers(userAnswers);
		
		if(application.getCommunicationManager().requestService(ETQuestionnaireFragment.this, "updateQuestionnaireCallback", ETConstants.SERVICE_UPDATE_QUESTIONNAIRIE_ANSWERS, request , UserMethodResult.class, false, false)){
			submitButton.stopLoading();
		}
	}
	
	public void updateQuestionnaireCallback(Object resultObj){
		Log.d(TAG, "updateQuestionnaireCallback");
		UserMethodResult result = (UserMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "updateQuestionnaireCallback OK.");
			application.setUserRegulation(result.getUserRegulation());
		}else {
			Log.e(TAG, "updateQuestionnaireCallback failed");
			ETErrorUtils.handleResultError(result);
		}
	}
	
	private void submitQuestionnaire() {
		UpdateUserQuestionnaireAnswersRequest request = new UpdateUserQuestionnaireAnswersRequest();
		updateUserAnswers();
		request.setUserAnswers(userAnswers);
		
		if(application.getCommunicationManager().requestService(ETQuestionnaireFragment.this, "submitQuestionnaireCallback", ETConstants.SERVICE_SUBMIT_QUESTIONNAIRE, request , UserQuestionnaireResult.class, false, false)){
			submitButton.stopLoading();
		}
	}
	
	public void submitQuestionnaireCallback(Object resultObj){
		Log.d(TAG, "submitQuestionnaireCallback");
		submitButton.stopLoading();
		UserQuestionnaireResult result = (UserQuestionnaireResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "submitQuestionnaireCallback OK.");
			submitButton.disable();
			application.setUserRegulation(result.getUserRegulation());
			if(!ETRegulationUtils.checkLowGroupAndOpenPopup()){
				application.postEvent(new NavigationEvent(application.getHomeScreen(), NavigationType.INIT));
			}
		}else {
			Log.e(TAG, "submitQuestionnaireCallback failed");
			ETErrorUtils.handleResultError(result);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	
}
