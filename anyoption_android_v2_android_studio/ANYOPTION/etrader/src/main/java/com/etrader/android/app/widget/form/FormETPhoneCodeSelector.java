package com.etrader.android.app.widget.form;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.form.FormSimpleSelector;
import com.etrader.android.app.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

public class FormETPhoneCodeSelector extends FormSimpleSelector{

	protected TextView errorTextView;
	protected String defaultValue;
	
	public FormETPhoneCodeSelector(Context context) {
		super(context);
	}

	public FormETPhoneCodeSelector(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public FormETPhoneCodeSelector(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	@Override
	protected void setupUI() {
		if (view == null) {
			view = View.inflate(getContext(), R.layout.form_et_phonecode_selector, this);
			text = (TextView) view.findViewById(R.id.form_simple_selector_text);
			errorTextView = (TextView) view.findViewById(R.id.form_edittext_error_textview);
			
			if(!isInEditMode()){
				defaultValue = Application.get().getString(R.string.phoneCodeDefaultPrefix);
			}
		}

		if(!isInEditMode()){
			setValue(defaultValue);
		}else{
			text.setText("Selector");
		}
	}
	
	@Override
	public void refreshUI() {
		super.refreshUI();
		if(getValue() == null || getValue().equals(defaultValue)){
			text.setTextColor(Application.get().getResources().getColor(R.color.form_item_hint));
		}else{
			errorTextView.setVisibility(View.GONE);
			text.setTextColor(Application.get().getResources().getColor(R.color.form_item_selector_text));
		}
		
	}
	
	@Override
	public boolean checkValidity() {
		if(getValue() == null || getValue().equals(defaultValue)){
			displayError(ValidatorType.EMPTY.getErrorMessage());
			return false;
		}else{
			errorTextView.setVisibility(View.GONE);
			return true;
		}
	}
	
	@Override
	public void displayError(String error) {
		super.displayError(error);
		errorTextView.setText(error);
		errorTextView.setVisibility(View.VISIBLE);
	}
	
}
