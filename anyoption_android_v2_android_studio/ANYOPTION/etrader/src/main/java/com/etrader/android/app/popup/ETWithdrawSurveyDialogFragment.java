package com.etrader.android.app.popup;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.widget.CompoundButton;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.popup.WithdrawSurveyDialogFragment;
import com.anyoption.android.app.util.AnimationUtils;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.EditText;
import com.anyoption.android.app.widget.RadioButton;
import com.anyoption.android.app.widget.TextView;
import com.etrader.android.app.R;

public class ETWithdrawSurveyDialogFragment extends WithdrawSurveyDialogFragment {

    public static final String TAG = ETWithdrawSurveyDialogFragment.class.getSimpleName();

    private TextView usernameTextView;

    public static ETWithdrawSurveyDialogFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new ETWithdrawSurveyDialogFragment.");
        ETWithdrawSurveyDialogFragment fragment = new ETWithdrawSurveyDialogFragment();
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
        bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
        bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void onCreateContentView(View contentView) {
        usernameTextView = (TextView) contentView.findViewById(R.id.withdraw_survey_popup_first_message_username);
        super.onCreateContentView(contentView);
    }

    @Override
    protected void boldTexts(){
        TextUtils.decorateInnerText(usernameTextView, null, usernameTextView.getText().toString(), usernameTextView.getText().toString(), FontsUtils.Font.NOTO_SANS_BOLD, 0, 0);
        TextUtils.decorateInnerText(firstMessageTextView, null, firstMessageTextView.getText().toString(), firstMessageTextView.getText().toString(), FontsUtils.Font.NOTO_SANS_BOLD, 0, 0);
        TextUtils.decorateInnerText(questionTextView, null, questionTextView.getText().toString(), questionTextView.getText().toString(), FontsUtils.Font.NOTO_SANS_BOLD, 0, 0);
    }

    protected void fillTheFirstName(View contentView) {
        usernameTextView.setText(" " + Application.get().getUser().getFirstName());
    }

}
