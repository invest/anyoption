package com.etrader.android.app.util;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.results.MethodResult;
import com.etrader.android.app.ETApplication.ETScreen;
import com.etrader.android.app.popup.AdditionalFieldsDialogFragment;
import com.etrader.android.app.popup.AgreementDialogFragment;
import com.etrader.android.app.popup.KnowledgeDialogFragment;
import com.etrader.android.app.popup.ThresholdDialogFragment;

import android.os.Bundle;

public class ETErrorUtils extends ErrorUtils{

	
	public static void handleResultError(MethodResult result) {
		if (result == null) {
			ETErrorUtils.handleError(Constants.ERROR_CODE_UNKNOWN);
			return;
		}

		if (result.getUserMessages() != null && result.getUserMessages().length > 0) {
			ETErrorUtils.handleError(result.getErrorCode(), result.getUserMessages()[0].getMessage());
		} else {
			ETErrorUtils.handleError(result.getErrorCode());
		}
	}
	
	public static void handleError(int errorCode) {
		ETErrorUtils.handleError(errorCode, null);
	}
	
	public static void handleError(int errorCode, String message) {
		final Application application = Application.get();
		
		Bundle bundle = new Bundle();
		switch (errorCode) {
			case ETConstants.ERROR_CODE_ET_REGULATION_ADDITIONAL_INFO:
				application.getFragmentManager().showDialogFragment(AdditionalFieldsDialogFragment.class, null);
				break;
			case ETConstants.ERROR_CODE_NOT_ACCEPTED_TERMS:
				application.getFragmentManager().showDialogFragment(AgreementDialogFragment.class, null);
				break;
			case ETConstants.ERROR_CODE_ET_REGULATION_KNOWLEDGE_CONFIRM:
				application.getFragmentManager().showDialogFragment(KnowledgeDialogFragment.class, null);
				break;
			case ETConstants.ERROR_CODE_ET_REGULATION_CAPITAL_QUESTIONNAIRE_NOT_DONE:
				application.postEvent(new NavigationEvent(ETScreen.ETQUESTIONNAIRE, NavigationType.INIT));
				break;
			case ETConstants.ERROR_CODE_ET_REGULATION_SUSPENDED_LOW_SCORE:
				bundle.putBoolean(ETConstants.EXTRA_THRESHOLD_SHOW_THRESHOLD, false);
				application.getFragmentManager().showDialogFragment(ThresholdDialogFragment.class, bundle);
				break;
			case ETConstants.ERROR_CODE_ET_REGULATION_SUSPENDED_LOW_TRESHOLD:
				bundle.putBoolean(ETConstants.EXTRA_THRESHOLD_SHOW_THRESHOLD, true);
				application.getFragmentManager().showDialogFragment(ThresholdDialogFragment.class, bundle);
				break;
			case ETConstants.ERROR_CODE_ET_REGULATION_SUSPENDED_MEDIUM_TRESHOLD:
				bundle.putBoolean(ETConstants.EXTRA_THRESHOLD_SHOW_THRESHOLD, true);
				application.getFragmentManager().showDialogFragment(ThresholdDialogFragment.class, bundle);
				break;
			default:
				ErrorUtils.handleError(errorCode, message);
				break;
		}
	}
	
}
