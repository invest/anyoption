package com.etrader.android.app.popup;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.widget.EditText;
import com.anyoption.android.app.widget.TextView;
import com.anyoption.beans.base.City;
import com.etrader.android.app.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CitySearchDialogFragment extends BaseDialogFragment implements TextWatcher, AdapterView.OnItemClickListener, View.OnClickListener {

	public static final String TAG = CitySearchDialogFragment.class.getSimpleName();

	public interface ItemSelectListener{
		void onItemSelected(City city);
	}

	private ItemSelectListener itemSelectListener;
	private List<City> cityList;
	private List<City> filteredCityList;
	private ListView listView;
	private CityListAdapter adapter;
	private View backView;
	private View deleteView;
	private View searchView;
	private EditText editText;

	public static CitySearchDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new AgreementDialogFragment.");
		CitySearchDialogFragment fragment = new CitySearchDialogFragment();
		if(bundle == null){
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, 0);
		fragment.setArguments(bundle);

		return fragment;
	}
	
	protected View rootView;
	
	@Override
	protected int getContentLayout() {
		return R.layout.city_search_popup_layout;
	}

	@Override
	public void onCreate(Bundle b) {
		super.onCreate(b);
		cityList = Arrays.asList(Application.get().getCitiesArray());
	}
	
	@SuppressLint("SetJavaScriptEnabled")
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreateContentView(View contentView) {
		rootView = contentView;
		backView = rootView.findViewById(R.id.city_search_back_icon);
		deleteView = rootView.findViewById(R.id.city_search_delete_icon);
		searchView = rootView.findViewById(R.id.city_search_search_icon);

		backView.setOnClickListener(this);
		deleteView.setOnClickListener(this);
		searchView.setOnClickListener(this);

		editText = (EditText) rootView.findViewById(R.id.city_search_item_edit_text);
		editText.addTextChangedListener(this);
		listView = (ListView) rootView.findViewById(R.id.cityListView);
		adapter = new CityListAdapter();
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if(v == backView){
			dismiss();
		}else if(v == deleteView){
			editText.setText("");
			adapter.filter("");
			adapter.notifyDataSetChanged();
		}else if(v == searchView){
			editText.requestFocus();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		getItemSelectListener().onItemSelected(filteredCityList.get(position));
	}

	public ItemSelectListener getItemSelectListener() {
		return itemSelectListener;
	}

	public void setItemSelectListener(ItemSelectListener itemSelectListener) {
		this.itemSelectListener = itemSelectListener;
	}

	private static class ItemViewHolder{
		public TextView cityNameTextView;
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {}

	@Override
	public void afterTextChanged(Editable s) {
		adapter.filter(s.toString());
	}

	private class CityListAdapter extends BaseAdapter{

		public CityListAdapter(){
			filteredCityList = new ArrayList<City>();
			filteredCityList.addAll(cityList);
		}

		@Override
		public int getCount() {
			return filteredCityList.size();
		}

		@Override
		public Object getItem(int position) {
			return filteredCityList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			City city = filteredCityList.get(position);
			ItemViewHolder childViewHolder;
			if(convertView == null) {
				LayoutInflater inf = (LayoutInflater) application.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inf.inflate(R.layout.city_search_list_item_layout, parent, false);
				childViewHolder = new ItemViewHolder();
				childViewHolder.cityNameTextView = (TextView) convertView.findViewById(R.id.city_search_item_text_view);

				convertView.setTag(childViewHolder);
			}
			else{
				childViewHolder = (ItemViewHolder) convertView.getTag();
			}

			childViewHolder.cityNameTextView.setText(city.getName());

			return convertView;
		}

		public void filter(String constrain){
			filteredCityList.clear();
			for (City city : cityList){
				if(city.getName().startsWith(constrain)){
					filteredCityList.add(city);
				}
			}
			notifyDataSetChanged();
		}
	}

}
