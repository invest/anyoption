package com.etrader.android.app.fragment.my_account.banking.withdraw;

import com.anyoption.android.app.fragment.my_account.banking.withdraw.WithdrawClosedFragment;
import com.anyoption.android.app.util.DrawableUtils;
import com.etrader.android.app.R;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;

public class ETWithdrawClosedFragment extends WithdrawClosedFragment {
	
	private final static String TAG = ETWithdrawClosedFragment.class.getSimpleName();
	
	/**
	 * Use this method to obtain new instance of the fragment.
	 * @return
	 */
	public static ETWithdrawClosedFragment newInstance(Bundle bundle){
		Log.d(TAG, "Create new ETWithdrawClosedFragment");
		ETWithdrawClosedFragment withdrawFragment = new ETWithdrawClosedFragment();
		withdrawFragment.setArguments(bundle);
		return withdrawFragment;
	}

	@Override
	protected Drawable getDrawableForSkin(){
		Drawable drawable = DrawableUtils.getImage(R.drawable.withdraw_closed_table_image_iw);
		return drawable;
	}
}

