package com.etrader.android.app.fragment.my_account.banking.deposit;

import com.anyoption.android.app.fragment.my_account.banking.deposit.WireDepositFragment;
import com.anyoption.common.beans.base.User;
import com.etrader.android.app.R;
import com.etrader.android.app.util.ETRegulationUtils;

import android.os.Bundle;
import android.view.View;

public class ETWireDepositFragment extends WireDepositFragment {

	public static ETWireDepositFragment newInstance(Bundle bundle) {
		ETWireDepositFragment wireDepositFragment = new ETWireDepositFragment();
		wireDepositFragment.setArguments(bundle);

		return wireDepositFragment;
	}

	@Override
	public void setParamsGeneral(User u, View rootView) {
		View funnelStepsLayout = rootView.findViewById(R.id.wire_deposit_funnel_steps_layout);
		boolean isRegulationFunnel = ETRegulationUtils.isRegulated() && !ETRegulationUtils.checkFirstDeposit();
		funnelStepsLayout.setVisibility((isRegulationFunnel) ? View.VISIBLE : View.GONE);
	}
	
}
