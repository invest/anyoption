package com.etrader.android.app.manager;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.manager.LoginManager;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.service.results.UserMethodResult;
import com.etrader.android.app.popup.AdditionalFieldsDialogFragment;
import com.etrader.android.app.popup.AgreementDialogFragment;
import com.etrader.android.app.util.ETRegulationUtils;

import android.os.Bundle;
import android.util.Log;

public class ETLoginManager extends LoginManager{
	
	private static final String TAG = ETLoginManager.class.getSimpleName();

	protected void doAfterLoginNavigation(Screenable toScreen, Bundle bundle){
		Log.d(TAG, "doAfterLoginNavigation");
		
		if (!(application.isLoggedIn() && application.getUser().getIsNeedChangePassword())) {
		//OLD USERS:
		if(!ETRegulationUtils.isRegulated() && !ETRegulationUtils.checkFirstDeposit() && !ETRegulationUtils.checkAdditionalFieldsAndopenScreen()){
			//Do nothing
		}
		///////////////
		
		else if(ETRegulationUtils.isRegulated() && !checkAdditionalFields()){
			application.getFragmentManager().showDialogFragment(AdditionalFieldsDialogFragment.class, null);
		}
		else if(toScreen == Screen.AGREEMENT){
			Bundle b = new Bundle();
			b.putSerializable(Constants.EXTRA_SCREEN, Screen.FIRST_NEW_CARD);
			application.getFragmentManager().showDialogFragment(AgreementDialogFragment.class, b);
		}
		else if(toScreen == Screen.FIRST_NEW_CARD){
			application.postEvent(new NavigationEvent(toScreen, NavigationType.INIT, bundle));
		}
		else if(ETRegulationUtils.checkKnowledgeQuestionAndOpenPopup() || ETRegulationUtils.checkQuestionnaireAndOpenScreen() || ETRegulationUtils.checkThresholdAndOpenPopup()){
			//Do nothing
		}
		else if(toScreen != null){
			application.postEvent(new NavigationEvent(toScreen, NavigationType.INIT, bundle));
		}
		} else {
			if (application.getCurrentScreen() != Screen.UPDATE_PASSWORD) {
				application.postEvent(new NavigationEvent(Screen.UPDATE_PASSWORD, NavigationType.DEEP, Screenable.SCREEN_FLAG_REMOVE_MENU, Screenable.SCREEN_FLAG_REMOVE_BACK));
			}
		}
	}
	
	@Override
	protected boolean showBonuses(UserMethodResult userMethodResult) {
		return false;
	}
	
	protected boolean checkAdditionalFields(){
		User user = application.getUser();
		boolean isHasIdNum = user.getIdNum() != null && user.getIdNum().length() > 0;
		boolean isHasStreetName = user.getStreet() != null && user.getStreet().length() > 0;
		boolean isHasStreetNumber = user.getStreetNo() != null && user.getStreetNo().length() > 0;
		boolean isHasCityName = user.getCityName() != null && user.getCityName().length() > 0;
//		boolean isHasCityId = user.getCityId() > 0;//Without this because in the WEB it is not set
		boolean isHasZipCode = user.getZipCode() != null && user.getZipCode().length() > 0;
		boolean isHasBirthDate = user.getTimeBirthDate() != null;
		boolean isHasGender = user.getGender() != null && user.getGender().length() > 0;
		
		return isHasIdNum && isHasStreetName && isHasStreetNumber && isHasCityName && isHasZipCode && isHasBirthDate && isHasGender;
	}
	
}