package com.etrader.android.app.fragment.my_account.banking.deposit;

import com.anyoption.android.app.fragment.my_account.banking.deposit.ExistingCardFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.json.results.TransactionMethodResult;
import com.etrader.android.app.util.ETErrorUtils;
import com.etrader.android.app.util.ETRegulationUtils;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

/**
 * A simple {@link Fragment} subclass. 
 * <p>It manages the "Existing Card" screen and functionality.
 * @author Eyal O
 *
 */
public class ETExistingCardFragment extends ExistingCardFragment {
	
	private static final String TAG = ETExistingCardFragment.class.getSimpleName();
	
	/**
	 * Use this method to obtain new instance of the fragment.
	 * @return
	 */
	public static ETExistingCardFragment newInstance(){	
		return ETExistingCardFragment.newInstance(new Bundle());
	}

	/**
	 * Use this method to obtain new instance of the fragment.
	 * @return
	 */
	public static ETExistingCardFragment newInstance(Bundle bundle){
		ETExistingCardFragment eTExistingCardFragment = new ETExistingCardFragment();
		eTExistingCardFragment.setArguments(bundle);
		return eTExistingCardFragment;
	}
	
	@Override
	protected void tryToDeposit(){
		if(!ETRegulationUtils.isRegulated() && !ETRegulationUtils.checkFirstDeposit() && !ETRegulationUtils.checkAdditionalFieldsAndopenScreen()){
			//Do nothing
		}
		else if(!ETRegulationUtils.checkFirstDeposit()){
			super.tryToDeposit();
		}
		else if(!ETRegulationUtils.checkKnowledgeQuestionAndOpenPopup() && !ETRegulationUtils.checkQuestionnaireAndOpenScreen() && !ETRegulationUtils.checkThresholdAndOpenPopup()){
			super.tryToDeposit();
		}
	}
	
	@Override
	public void insertDepositCallBack(Object resultObj){
		Log.d(TAG, "insertDepositCallBack");
		
		TransactionMethodResult result = (TransactionMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Insert deposit succefully");
			// Navigate to next screen:
			if(!ETRegulationUtils.checkKnowledgeQuestionAndOpenPopup() && !ETRegulationUtils.checkQuestionnaireAndOpenScreen() && !ETRegulationUtils.checkThresholdAndOpenPopup()){
				super.insertDepositCallBack(resultObj);
			}
		} else {
			Log.d(TAG, "Insert deposit failed");
			ETErrorUtils.handleResultError(result);
			isSubmitButtonEnabled = true;
			setSubmitButtonEnabled(true);
		}
	}
}

