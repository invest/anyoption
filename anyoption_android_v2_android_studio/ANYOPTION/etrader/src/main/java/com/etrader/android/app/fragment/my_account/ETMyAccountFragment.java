package com.etrader.android.app.fragment.my_account;

import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.my_account.MyAccountFragment;
import com.etrader.android.app.ETApplication.ETScreen;
import com.etrader.android.app.R;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ETMyAccountFragment extends MyAccountFragment {

	/**
	 * Use this method to get new instance of the fragment.
	 * 
	 * @return
	 */
	public static ETMyAccountFragment newInstance(Bundle bundle) {
		ETMyAccountFragment myAccountFragment = new ETMyAccountFragment();
		myAccountFragment.setArguments(bundle);
		return myAccountFragment;
	}

	private View bonusesSeparator;
	private View bonusesButton;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = super.onCreateView(inflater, container, savedInstanceState);
		rootView.findViewById(R.id.myAccountButtonBalances).setOnClickListener(this);

		bonusesSeparator = rootView.findViewById(R.id.my_account_bonuses_separator);
		bonusesButton = rootView.findViewById(R.id.myAccountButtonBonuse);
		
		if(!application.getUser().getIsBonusTabDisplayAllow()){
			bonusesSeparator.setVisibility(View.GONE);
			bonusesButton.setVisibility(View.GONE);
		}
		
		return rootView;
	}

	@Override
	protected void setupDocumentsButton(View rootView){
		//Do nothing
	}
	
	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.myAccountButtonBalances) {
			NavigationEvent navigationEvent = new NavigationEvent(ETScreen.BALANCES,NavigationType.DEEP);
			application.postEvent(navigationEvent);
		} else {
			super.onClick(v);
		}
	}

}
