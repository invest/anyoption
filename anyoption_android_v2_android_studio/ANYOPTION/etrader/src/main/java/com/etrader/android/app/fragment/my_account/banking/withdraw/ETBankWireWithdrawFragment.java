package com.etrader.android.app.fragment.my_account.banking.withdraw;

import java.util.ArrayList;
import java.util.List;

import com.anyoption.android.app.Application;
import com.anyoption.android.R;
import com.anyoption.android.app.fragment.my_account.banking.withdraw.BankWireWithdrawFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.android.app.widget.form.FormSelector.ItemListener;
import com.anyoption.android.app.widget.form.FormStringSelector;
import com.anyoption.common.bl_vos.WireBase;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.json.results.OptionsMethodResult;
import com.anyoption.json.util.OptionItem;
import com.etrader.android.app.popup.ETWithdrawSurveyConfirmationDialogFragment;
import com.etrader.android.app.popup.ETWithdrawSurveyDialogFragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class ETBankWireWithdrawFragment extends BankWireWithdrawFragment {

	@SuppressWarnings("hiding")
	private static final String TAG = ETBankWireWithdrawFragment.class.getSimpleName();

	private OptionItem[] banks;

	public static ETBankWireWithdrawFragment newInstance(Bundle bundle) {
		ETBankWireWithdrawFragment bankwireWithdrawFragment = new ETBankWireWithdrawFragment();
		bankwireWithdrawFragment.setArguments(bundle);
		return bankwireWithdrawFragment;
	}

	@Override
	protected void init() {
		bankNameSpinner = (FormStringSelector) rootView.findViewById(R.id.bank_name_selector);
		accountNumber = (FormEditText) rootView.findViewById(R.id.account_edittext);
		accountNumber.addValidator(ValidatorType.EMPTY, ValidatorType.DIGITS);
		branchNumber = (FormEditText) rootView.findViewById(R.id.branch_number_edittext);
		branchNumber.addValidator(ValidatorType.EMPTY, ValidatorType.DIGITS);
		branchNumber = (FormEditText) rootView.findViewById(R.id.branch_number_edittext);
		amount = ((FormEditText) rootView.findViewById(R.id.amount));
		amount.addValidator(ValidatorType.EMPTY, ValidatorType.CURRENCY_AMOUNT);
		amount.setFieldName(Constants.FIELD_AMOUNT);

		TextView currencySymbol = ((TextView) rootView.findViewById(R.id.currency_symbol_view));
		currencySymbol.setText(user.getCurrencySymbol());

		form.addItem(accountNumber, branchNumber, amount);

		Application.get().getCommunicationManager()
				.requestService(ETBankWireWithdrawFragment.this, "getBanksCallBack",
						Constants.SERVICE_GET_BANKS_ET, new MethodRequest(),
						OptionsMethodResult.class, false, false);
	}

	public void getBanksCallBack(Object resultObj) {
		OptionsMethodResult result = (OptionsMethodResult) resultObj;

		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			banks = result.getOptions();

			List<String> list = new ArrayList<String>();
			for (OptionItem o : result.getOptions()) {
				list.add(o.getValue());
			}

			bankNameSpinner.setItems(list, 0, new ItemListener<String>() {

				@Override
				public void onItemSelected(String selectedItem) {
					bankNameSpinner.showText(selectedItem);
				}

				@Override
				public String getDisplayText(String selectedItem) {
					return selectedItem;
				}

				@Override
				public Drawable getIcon(String selectedItem) {
					return null;
				}

			});

			// Display the first item of the selector as the default one::
			bankNameSpinner.showText(list.get(0));
		} else {
			Log.e(TAG, "getBanksCallBack -> Could not get ET banks!");
		}

	}

	@Override
	protected void setWireParams(WireBase wire) {
		wire.setAccountNum(accountNumber.getValue());
		wire.setBranch(branchNumber.getValue());
		wire.setAmount(amount.getValue());

		// Set bank id
		if (banks != null) {
			String selectedBank = bankNameSpinner.getValue();
			for (OptionItem bank : banks) {
				if (bank.getValue().equals(selectedBank)) {
					wire.setBankId(String.valueOf(bank.getId()));
					break;
				}
			}
		}
	}

	@Override
	protected void showWithdrawSurvey(){
		resetWithdrawSurveyParameters();
		ETWithdrawSurveyDialogFragment withdrawSurveyDialogFragment = ETWithdrawSurveyDialogFragment.newInstance(null);
		withdrawSurveyDialogFragment.setOnWithdrawSurveyClosedListener(ETBankWireWithdrawFragment.this);
		application.getFragmentManager().showDialogFragment(withdrawSurveyDialogFragment);
	}

	@Override
	public void onWithdrawSurveyClosed(boolean isSkipped, long chosenAnswer, String answerText) {
		isWithdrawSurveySkipped = isSkipped;
		withdrawSurveyUserAnswer = chosenAnswer;
		withdrawSurveyUserAnswerText = answerText;

		if (!isSkipped) {
			ETWithdrawSurveyConfirmationDialogFragment withdrawSurveyConfirmationDialogFragment = ETWithdrawSurveyConfirmationDialogFragment.newInstance(null);
			withdrawSurveyConfirmationDialogFragment.setOnDismissListener(ETBankWireWithdrawFragment.this);
			application.getFragmentManager().showDialogFragment(withdrawSurveyConfirmationDialogFragment);
		} else {
			callValidateWithdrawService();
		}

	}

}
