package com.etrader.android.app.fragment.login_register;

import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.LoginLogoutEvent.Type;
import com.anyoption.android.app.fragment.login_register.RegisterFragment;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.form.Form;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.beans.base.Register;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Platform;
import com.anyoption.common.service.results.UserMethodResult;
import com.etrader.android.app.R;
import com.etrader.android.app.popup.AdditionalFieldsDialogFragment;

import android.os.Bundle;
import android.view.View;

/**
 * Custom {@link RegisterFragment} that support the ETRADER logic.
 * @author Anastas Arnaudov
 *
 */
public class ETRegisterFragment extends RegisterFragment {

	private long currencyId;
	
	/** 
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static RegisterFragment newInstance(Bundle bundle){
		ETRegisterFragment etRegisterFragment = new ETRegisterFragment();	
		etRegisterFragment.setArguments(bundle);
		return etRegisterFragment;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		currencyId = Currency.CURRENCY_ILS_ID;
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void init() {
		//Do nothing
	}
	
	@Override
	protected void setupForm(){
		firstNameField = (FormEditText) rootView.findViewById(R.id.register_first_name_edittext); 
		firstNameField.addValidator(ValidatorType.EMPTY);
		firstNameField.setFieldName(Constants.FIELD_FIRST_NAME);
		 
		lastNameField = (FormEditText) rootView.findViewById(R.id.register_last_name_edittext); 
		lastNameField.addValidator(ValidatorType.EMPTY);
		lastNameField.setFieldName(Constants.FIELD_LAST_NAME);
		 
		emailField = (FormEditText) rootView.findViewById(R.id.register_email_edittext); 
		emailField.addValidator(ValidatorType.EMPTY, ValidatorType.EMAIL);
		emailField.setFieldName(Constants.FIELD_EMAIL);
		emailField.addFocusChangeListener(new FormEditText.FocusChangeListener() {
			@Override 
			public void onFocusChanged(boolean hasFocus) {
				if(!hasFocus && emailField.checkValidity()) {
					insertRegisterAttempt();
				}
			} 
		}); 
		
		phoneField = (FormEditText) rootView.findViewById(R.id.register_phone_edittext); 
		phoneField.addValidator(ValidatorType.EMPTY, ValidatorType.PHONE_ISRAEL);
		phoneField.setFieldName(Constants.FIELD_MOBILE_PHONE);
		phoneField.addFocusChangeListener(new FormEditText.FocusChangeListener() {

			@Override
			public void onFocusChanged(boolean hasFocus) {
				if (!hasFocus) {
					boolean phoneValid = phoneField.checkValidity();
					if (phoneValid) {
						insertRegisterAttempt();
					}
				}
			}
		});

		passwordField = (FormEditText) rootView.findViewById(R.id.register_pass_edittext); 
		passwordField.addValidator(ValidatorType.EMPTY, ValidatorType.ALNUM, ValidatorType.PASSWORD_MINIMUM_CHARACTERS);
		passwordField.setFieldName(Constants.FIELD_PASSWORD);

		form = new Form(new Form.OnSubmitListener() { 
			@Override 
			public void onSubmit() {
				insertRegister();
			} 
		}); 

		form.addItem(firstNameField, lastNameField, emailField, phoneField, passwordField);
	}
	
	@Override
	protected void setupTermsAndConditions() {
		//Do nothing
	}
	
	@Override
	protected void underlineTryWithoutAccountText(){
		//TextUtils.underlineText(tryWithoutAccountTextView);
		tryWithoutAccountTextView.setVisibility(View.GONE);
	}
	
	@Override
	protected void setUserDetalis(Register user) {
		super.setUserDetalis(user);
		user.setCurrencyId(currencyId);
		String mobilePrefix = ((String)phoneField.getValue()).substring(0, 3);
		String mobile		= ((String)phoneField.getValue()).substring(3);
		user.setMobilePhonePrefix(mobilePrefix);
		user.setMobilePhone(mobile);
		user.setPlatformId(Platform.ETRADER);
	}

	protected void handleOKRegister(UserMethodResult result){
		Bundle bundle = new Bundle();
		bundle.putBoolean(Constants.EXTRA_MENU_BUTTON, false);
		application.getFragmentManager().showDialogFragment(AdditionalFieldsDialogFragment.class, null);
		application.postEvent(new LoginLogoutEvent(Type.REGISTER, result));
	}
}
