package com.etrader.android.app.fragment.login_register;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.fragment.login_register.ForgotPasswordFragment;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.RequestButton.InitAnimationListener;
import com.anyoption.android.app.widget.form.Form;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.beans.base.Register;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.json.requests.InsertUserMethodRequest;

/**
 * A simple {@link Fragment} subclass.
 * <p>It manages the "Forgot Password" screen and functionality.
 *
 * @author Anastas Arnaudov
 */
public class ETForgotPasswordFragment extends ForgotPasswordFragment {


    public static ETForgotPasswordFragment newInstance() {
        return ETForgotPasswordFragment.newInstance(new Bundle());
    }

    /**
     * Use this method to obtain new instance of the fragment.
     *
     * @return
     */
    public static ETForgotPasswordFragment newInstance(Bundle bundle) {
        ETForgotPasswordFragment forgotPasswordFragment = new ETForgotPasswordFragment();
        forgotPasswordFragment.setArguments(bundle);
        return forgotPasswordFragment;
    }

    protected void boldTexts() {
        TextUtils.decorateInnerText(emailField, null, emailField.getText().toString(), emailField.getText().toString(), FontsUtils.Font.NOTO_SANS_BOLD, 0, 0);
        TextUtils.decorateInnerText(phoneField, null, phoneField.getText().toString(), phoneField.getText().toString(), FontsUtils.Font.NOTO_SANS_BOLD, 0, 0);
    }

}

