package com.etrader.android.app.util;

public class ETConstants extends com.anyoption.android.app.util.constants.Constants{

	//################################################################################################
	//////////////////			ETRADER TEST ENVIRONMENT		//////////////////////////////////////
	//################################################################################################ 
	
//	public static final String SERVICE_URL = "http://www.testenv.etrader.co.il/jsonService/AnyoptionService/";	
//	public static final String LS_URL = "http://lstest.etrader.co.il:80";	
//	public static final String MERCHANT_ID = "000203AYP";
//	public static final String ONE_CLICK_URL = "http://test.envoytransfers.com";
//	public static final String WEB_SITE_LINK = "http://www.testenv.etrader.co.il/mobile/";

	//################################################################################################
	
	//################################################################################################
	//////////////////			LOCAL ENVIRONMENT (Vankata)		//////////////////////////////////////
	//################################################################################################ 

//	public static final String SERVICE_URL = "http://192.168.100.56/jsonServiceET/AnyoptionService/";
//	public static final String LS_URL = "http://ls.bgtestenv.etrader.co.il:80";
//	public static final String MERCHANT_ID = "000203AYP";
//	public static final String ONE_CLICK_URL = "http://test.envoytransfers.com";
//	public static final String WEB_SITE_LINK = "http://www.bgtestenv.etrader.co.il/mobile/";

	//################################################################################################
	
	//################################################################################################
	//////////////////			LOCAL ENVIRONMENT (Jimi)		//////////////////////////////////////
	//################################################################################################ 

//	public static final String SERVICE_URL = "http://192.168.100.62/jsonServiceET/AnyoptionService/";
//	public static final String LS_URL = "http://ls.bgtestenv.etrader.co.il:80";
//	public static final String MERCHANT_ID = "000203AYP";
//	public static final String ONE_CLICK_URL = "http://test.envoytransfers.com";
//	public static final String WEB_SITE_LINK = "http://www.bgtestenv.etrader.co.il/mobile/";

	//################################################################################################
	
	//################################################################################################
	//////////////////			LOCAL ENVIRONMENT (Pavel)		//////////////////////////////////////
	//################################################################################################ 

//	public static final String SERVICE_URL = "http://192.168.100.175/jsonServiceET/AnyoptionService/";
//	public static final String LS_URL = "http://ls.bgtestenv.etrader.co.il:80";
//	public static final String MERCHANT_ID = "000203AYP";
//	public static final String ONE_CLICK_URL = "http://test.envoytransfers.com";
//	public static final String WEB_SITE_LINK = "http://www.bgtestenv.etrader.co.il/mobile/";

	//################################################################################################
	
	//################################################################################################
	//////////////////			ETRADER BG TEST ENVIRONMENT		//////////////////////////////////////
	//################################################################################################ 

	public static final String SERVICE_URL = "https://www.bgtestenv.etrader.co.il/jsonService/AnyoptionService/";
	public static final String LS_URL = "http://ls.bgtestenv.etrader.co.il:80";;//"https://ls.bgtestenv.etrader.co.il:443";//TODO 443 for https, 80 for http
	public static final String MERCHANT_ID = "000203AYP";
	public static final String ONE_CLICK_URL = "https://test.envoytransfers.com";
	public static final String WEB_SITE_LINK = "https://www.bgtestenv.etrader.co.il/mobile/";

	//################################################################################################
	
	//################################################################################################
    //////////////////         ETRADER LIVE ENVIRONMENT		//////////////////////////////////////////
	//################################################################################################
	
//	public static final String SERVICE_URL = "https://www.etrader.co.il/jsonService/AnyoptionService/";
//	public static final String LS_URL = "http://ls.etrader.co.il:80";
//	public static final String MERCHANT_ID = "000266AYP";
//	public static final String ONE_CLICK_URL = "http://www.envoytransfers.com";
//	public static final String WEB_SITE_LINK = "http://www.etrader.co.il/mobile/";

	//################################################################################################
	
	/**
	 * 27.05.2015
	 */
	public static final long BONUSES_DATE_CRITERIA_TIMESTAMP = 1435415410146l;	//27.05.2015
	
	//################################################################################################
	///////////////////					SERVICES			/////////////////////////////////////////
	//################################################################################################
	
	public static final String SERVICE_KNOWLEDGE_QUESTION					= "insertKnowledgeConfirm";
	public static final String SERVICE_GET_QUESTIONNAIRE					= "getUserCapitalQuestionnaire";
	public static final String SERVICE_SUBMIT_QUESTIONNAIRE					= "updateCapitalQuestionnaireDone";
	public static final String SERVICE_UNSUSPEND_USER						= "unsuspendUserRequest";
	
	//################################################################################################
	
	//################################################################################################
	///////////////////					ERROR CODES			/////////////////////////////////////////
	//################################################################################################
	
    public static final int ERROR_CODE_ET_REGULATION_ADDITIONAL_INFO					= 700;
    public static final int ERROR_CODE_NOT_ACCEPTED_TERMS								= 701;
    public static final int ERROR_CODE_ET_REGULATION_KNOWLEDGE_CONFIRM					= 702;
    public static final int ERROR_CODE_ET_REGULATION_CAPITAL_QUESTIONNAIRE_NOT_DONE		= 703;
    public static final int ERROR_CODE_ET_REGULATION_SUSPENDED_LOW_SCORE				= 704;
    public static final int ERROR_CODE_ET_REGULATION_SUSPENDED_LOW_TRESHOLD				= 705;
    public static final int ERROR_CODE_ET_REGULATION_SUSPENDED_MEDIUM_TRESHOLD			= 706;
	
	//################################################################################################
    
	//################################################################################################
	///////////////////						EXTRAs			/////////////////////////////////////////
	//################################################################################################
	
    public static final String EXTRA_THRESHOLD_SHOW_THRESHOLD								= "threshold_show";
	public static final String EXTRA_CITY_ARRAY												= "city_array";
	//################################################################################################
}
