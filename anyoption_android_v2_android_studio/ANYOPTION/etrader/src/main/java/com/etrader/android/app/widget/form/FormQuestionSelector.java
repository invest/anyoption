package com.etrader.android.app.widget.form;

import java.util.Arrays;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.LayoutDirection;
import com.anyoption.android.app.widget.form.FormItem;
import com.etrader.android.app.R;
import com.etrader.android.app.model.Answer;
import com.etrader.android.app.model.Question;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class FormQuestionSelector extends LinearLayout implements FormItem{

	private static final String TAG = FormQuestionSelector.class.getSimpleName();
	
	private static final float SCALE_HINT_RATIO = 0.7f;
	
	public interface OnItemSelectedListener{
		public void onItemSelected(Answer answer);
	}
	
	private LayoutDirection direction; 

	private Question question;
	private Answer answer;
	
	private AlertDialog dialog;
	private ArrayAdapter<Answer> adapter;
	private TextView textTextView;
	private TextView hintTextView;
	private OnItemSelectedListener onItemSelectedListener; 
	
	private boolean isHintUp = false;
	
	public FormQuestionSelector(Context context) {
		super(context);
		setup(context, null, 0);
	}
	
	public FormQuestionSelector(Context context, AttributeSet set) {
		super(context, set);
		setup(context, set, 0);
	}
	
	public FormQuestionSelector(Context context, AttributeSet set, int defStyle) {
		super(context, set, defStyle);
		setup(context, set, defStyle);
	}

	public void setup(Context context, AttributeSet set, int defStyle) {
		setDirection(Application.get().getLayoutDirection());

		View rootView = View.inflate(context, R.layout.form_question_layout, this);
		hintTextView = (TextView) rootView.findViewById(R.id.form_question_hint);
		textTextView = (TextView) findViewById(R.id.form_question_text);
		
		setupUIForDirection();
		
		//Get custom attributes:
		TypedArray attributesTypedArray = context.obtainStyledAttributes(set,R.styleable.FormQuestionSelector);
		String hintText = attributesTypedArray.getString(R.styleable.FormQuestionSelector_hintText) != null ? attributesTypedArray.getString(R.styleable.FormQuestionSelector_hintText) : "";	
		attributesTypedArray.recycle();
		////////////////////////
		
		hintTextView.setText(hintText);
	    
		this.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(dialog != null){
					dialog.show();
				}
			}
		});
		
	}
	
	public void setQuestion(Question question){
		this.question = question;
		hintTextView.setText(question.getDescription());
		
		int selectedIndex = question.getAnswerPosition(getValue());
		setupDialog(selectedIndex);
	}

	public Question getQuestion(){
		return question;
	}
	
	@SuppressLint("RtlHardcoded")
	private void setupDialog(int defaultSelectedItem){
		AlertDialog.Builder builder = new AlertDialog.Builder(Application.get().getCurrentActivity());
		adapter = new ArrayAdapter<Answer>(getContext(),R.layout.form_selector_list_item, Arrays.asList(question.getAnswers())){
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				LinearLayout view;
				TextView text;
				if (convertView == null) {
					view =  (LinearLayout) ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.form_selector_list_item, parent, false);
				} else {
					view = (LinearLayout) convertView;
				}
				text = (TextView) view.findViewById(R.id.form_selector_list_item_text);
				Answer item = question.getAnswers()[position];
				text.setText(item.getDescription());
				//Align text according to the Direction:
				if(getDirection() == LayoutDirection.LEFT_TO_RIGHT){
					text.setGravity(Gravity.LEFT);
				}else if(getDirection() == LayoutDirection.RIGHT_TO_LEFT){
					text.setGravity(Gravity.RIGHT);
				}
				return view;
			}
		};
	    builder.setSingleChoiceItems(adapter, defaultSelectedItem, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int position) {
				Answer answer = question.getAnswers()[position];
				setValue(answer);
				if(onItemSelectedListener != null){
					onItemSelectedListener.onItemSelected(answer);
				}
				dialog.dismiss();
			}	
	    });
	    
	    dialog = builder.create();
		dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}
	
	@SuppressLint("RtlHardcoded")
	public void setupUIForDirection() {
		switch (getDirection()) {
			case LEFT_TO_RIGHT:
				textTextView.setGravity(Gravity.LEFT);
				hintTextView.setGravity(Gravity.LEFT);
				break;
			case RIGHT_TO_LEFT:
				textTextView.setGravity(Gravity.RIGHT);
				hintTextView.setGravity(Gravity.RIGHT);
				break;
			default:
				break;
		}
	}
	
	private void moveHint() {
		float hinttextSize = hintTextView.getTextSize();
		hintTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, hinttextSize*SCALE_HINT_RATIO);
	}

	@Override
	public void setValue(Object value){
		if(value instanceof Answer){
			answer = (Answer) value;
			if(!isHintUp){
				isHintUp = true;
				moveHint();
			}
			textTextView.setText(answer.getDescription());	
			textTextView.setVisibility(View.VISIBLE);	
		}else{
			Log.e(TAG, "setValue -> value is not of type Answer");
		}
	}
	
	@Override
	public Answer getValue() {
		return answer;
	}

	@Override
	public boolean isEditable() {
		//Do nothing
		return false;
	}

	@Override
	public void setEditable(boolean isEditable) {
		//Do nothing
	}

	@Override
	public boolean checkValidity() {
		//Do nothing
		return false;
	}

	@Override
	public void addOnEditorActionListener(OnEditorActionListener onEditorActionListener) {
		//Do nothing
	}

	@Override
	public void clear() {
		//Do nothing
	}

	@Override
	public void refreshUI() {
		//Do nothing
	}

	@Override
	public void setFieldName(String fieldName) {
		//Do nothing
	}

	@Override
	public String getFieldName() {
		return null;
	}

	@Override
	public void displayError(String error) {
		//Do nothing
	}

	@Override
	public void setDirection(LayoutDirection direction) {
		this.direction = direction;
	}

	@Override
	public LayoutDirection getDirection() {
		return this.direction;
	}

	public OnItemSelectedListener getOnItemSelectedListener() {
		return onItemSelectedListener;
	}

	public void setOnItemSelectedListener(OnItemSelectedListener onItemSelectedListener) {
		this.onItemSelectedListener = onItemSelectedListener;
	}
	
}
