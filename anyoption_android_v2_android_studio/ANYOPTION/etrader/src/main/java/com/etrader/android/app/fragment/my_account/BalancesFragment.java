package com.etrader.android.app.fragment.my_account;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.UserUpdatedEvent;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.common.beans.base.User;
import com.etrader.android.app.ETApplication.ETScreen;
import com.etrader.android.app.R;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class BalancesFragment extends NavigationalFragment {

	private static final String DATE_FORMAT_PATTERN = "dd/MM/yyyy";

	private TextView balanceView;
	private TextView taxBalanceView;

	/**
	 * Use this method to get new instance of the fragment.
	 * 
	 * @return
	 */
	public static BalancesFragment newInstance(Bundle bundle) {
		BalancesFragment balancesFragment = new BalancesFragment();
		balancesFragment.setArguments(bundle);
		return balancesFragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.balances_layout, container, false);

		balanceView = (TextView) rootView.findViewById(R.id.balances_balance);
		taxBalanceView = (TextView) rootView.findViewById(R.id.balances_tax_balance);
		setUpBalances();

		TextView taxBalanceNoteView = (TextView) rootView
				.findViewById(R.id.balances_tax_balance_note);

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.MONTH, 0);

		SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_PATTERN);
		String formatFrom = format.format(calendar.getTime());
		String formattedTaxBalanceNote = getResources().getString(R.string.balancesTaxBalanceNote,
				formatFrom);
		taxBalanceNoteView.setText(formattedTaxBalanceNote);

		return rootView;
	}

	@Override
	public void onResume() {
		application.registerForEvents(this, UserUpdatedEvent.class);
		super.onResume();
	}

	@Override
	public void onPause() {
		application.unregisterForAllEvents(this);
		super.onPause();
	}

	@Override
	public void onDestroy() {
		application.unregisterForAllEvents(this);
		super.onDestroy();
	}

	@Override
	protected Screenable setupScreen() {
		return ETScreen.BALANCES;
	}

	/**
	 * Called when a {@link UserUpdatedEvent} occurs.
	 * 
	 * @param event
	 */
	public void onEventMainThread(UserUpdatedEvent event) {
		setUpBalances();
	}

	private void setUpBalances() {
		User user = application.getUser();
		balanceView.setText(getString(R.string.balancesBalance, user.getBalanceWF()));
		taxBalanceView.setText(getString(R.string.balancesTaxBalance, user.getTaxBalanceWF()));
	}

}
