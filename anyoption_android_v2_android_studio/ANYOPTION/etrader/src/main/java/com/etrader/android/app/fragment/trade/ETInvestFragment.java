package com.etrader.android.app.fragment.trade;

import com.anyoption.android.app.fragment.trade.InvestFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.service.results.InvestmentMethodResult;
import com.etrader.android.app.popup.AgreementDialogFragment;
import com.etrader.android.app.util.ETErrorUtils;
import com.etrader.android.app.util.ETRegulationUtils;

import android.os.Bundle;
import android.util.Log;

/**
 * Fragment with put and call buttons which handles invest events.
 * 
 * @author Mario Kutlev
 * 
 */
public class ETInvestFragment extends InvestFragment {

	private static final String TAG = ETInvestFragment.class.getSimpleName();
	
	public static ETInvestFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new ETInvestFragment.");
		ETInvestFragment fragment = new ETInvestFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	public static ETInvestFragment newInstance(Market market) {
		Bundle args = new Bundle();
		args.putSerializable(Constants.EXTRA_MARKET, market);
		return newInstance(args);
	}

	@Override
	protected void onPut(){
		if(application.isLoggedIn() && !application.getUser().isAcceptedTerms()){
			application.getFragmentManager().showDialogFragment(AgreementDialogFragment.class, null);
		}else{
			if(ETRegulationUtils.checkFirstDepositAndOpenScreen() && !ETRegulationUtils.checkKnowledgeQuestionAndOpenPopup() && !ETRegulationUtils.checkQuestionnaireAndOpenScreen()){
				super.onPutAction();
			}
		}
	}
	
	@Override
	protected void onCall(){
		if(application.isLoggedIn() && !application.getUser().isAcceptedTerms()){
			application.getFragmentManager().showDialogFragment(AgreementDialogFragment.class, null);
		}else{
			if(ETRegulationUtils.checkFirstDepositAndOpenScreen() && !ETRegulationUtils.checkKnowledgeQuestionAndOpenPopup() && !ETRegulationUtils.checkQuestionnaireAndOpenScreen()){
				super.onCallAction();
			}
		}
	}
	
	@Override
	public void insertInvestmentCallBack(Object resultObj) {
		super.insertInvestmentCallBack(resultObj);
		InvestmentMethodResult result = (InvestmentMethodResult) resultObj;
		if (result == null || result.getErrorCode() != Constants.ERROR_CODE_SUCCESS) {
			ETErrorUtils.handleResultError(result);
		}
	}
}
