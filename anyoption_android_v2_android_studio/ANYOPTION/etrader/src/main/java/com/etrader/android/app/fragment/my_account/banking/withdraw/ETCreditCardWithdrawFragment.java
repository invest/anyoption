package com.etrader.android.app.fragment.my_account.banking.withdraw;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.WithdrawEvent;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.fragment.my_account.banking.withdraw.CreditCardWithdrawFragment;
import com.anyoption.android.app.manager.PopUpManager.OnAlertPopUpButtonPressedListener;
import com.anyoption.android.app.manager.PopUpManager.OnConfirmPopUpButtonPressedListener;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.popup.LowWithdrawDialogFragment;
import com.anyoption.android.app.popup.LowWithdrawDialogFragment.LowWithdrawListener;
import com.anyoption.android.app.popup.WithdrawOkDialogFragment;
import com.anyoption.android.app.popup.WithdrawSurveyConfirmationDialogFragment;
import com.anyoption.android.app.popup.WithdrawSurveyDialogFragment;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.RequestButton.InitAnimationListener;
import com.anyoption.android.app.widget.form.Form;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.common.beans.base.CreditCard;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.CcDepositMethodRequest;
import com.anyoption.json.results.TransactionMethodResult;
import com.etrader.android.app.popup.ETWithdrawSurveyConfirmationDialogFragment;
import com.etrader.android.app.popup.ETWithdrawSurveyDialogFragment;

/**
 * A simple {@link Fragment} subclass.
 * <p>It manages the "Existing Card" screen and functionality.
 *
 * @author Eyal O
 */
public class ETCreditCardWithdrawFragment extends CreditCardWithdrawFragment {

    public static ETCreditCardWithdrawFragment newInstance() {
        return ETCreditCardWithdrawFragment.newInstance(new Bundle());
    }

    /**
     * Use this method to obtain new instance of the fragment.
     *
     * @return
     */
    public static ETCreditCardWithdrawFragment newInstance(Bundle bundle) {
        ETCreditCardWithdrawFragment creditCardWithdrawFragment = new ETCreditCardWithdrawFragment();
        creditCardWithdrawFragment.setArguments(bundle);
        return creditCardWithdrawFragment;
    }

    @Override
    protected void showWithdrawSurvey(){
        resetWithdrawSurveyParameters();
        ETWithdrawSurveyDialogFragment withdrawSurveyDialogFragment = ETWithdrawSurveyDialogFragment.newInstance(null);
        withdrawSurveyDialogFragment.setOnWithdrawSurveyClosedListener(ETCreditCardWithdrawFragment.this);
        application.getFragmentManager().showDialogFragment(withdrawSurveyDialogFragment);
    }


    @Override
    public void onWithdrawSurveyClosed(boolean isSkipped, long chosenAnswer, String answerText) {
        isWithdrawSurveySkipped = isSkipped;
        withdrawSurveyUserAnswer = chosenAnswer;
        withdrawSurveyUserAnswerText = answerText;

        if (!isSkipped) {
            ETWithdrawSurveyConfirmationDialogFragment withdrawSurveyConfirmationDialogFragment = ETWithdrawSurveyConfirmationDialogFragment.newInstance(null);
            withdrawSurveyConfirmationDialogFragment.setOnDismissListener(ETCreditCardWithdrawFragment.this);
            application.getFragmentManager().showDialogFragment(withdrawSurveyConfirmationDialogFragment);
        } else {
            callValidateWithdrawService();
        }

    }
}

