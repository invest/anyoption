package com.etrader.android.app.popup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.popup.OkDialogFragment;
import com.anyoption.android.app.popup.WithdrawSurveyConfirmationDialogFragment;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;

public class ETWithdrawSurveyConfirmationDialogFragment extends WithdrawSurveyConfirmationDialogFragment {

	public static final String TAG = ETWithdrawSurveyConfirmationDialogFragment.class.getSimpleName();
	
	protected TextView messageTextView;
	
	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static ETWithdrawSurveyConfirmationDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new ETWithdrawSurveyConfirmationDialogFragment.");
		ETWithdrawSurveyConfirmationDialogFragment fragment = new ETWithdrawSurveyConfirmationDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		fragment.setArguments(bundle);
		return fragment;
	}

	/**
	 * Set up content.
	 * 
	 * @param contentView
	 */
	@Override
	protected void onCreateContentView(View contentView) {
		messageTextView = (com.anyoption.android.app.widget.TextView) contentView.findViewById(R.id.withdraw_survey_confirmation_popup_message);

		TextUtils.decorateInnerText(messageTextView, null, messageTextView.getText().toString(), messageTextView.getText().toString(), FontsUtils.Font.NOTO_SANS_BOLD, 0, 0);

		View closeButton = contentView.findViewById(R.id.close_button);
		closeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}

}
