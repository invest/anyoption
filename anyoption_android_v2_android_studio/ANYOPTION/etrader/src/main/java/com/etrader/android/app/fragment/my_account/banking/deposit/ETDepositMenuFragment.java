package com.etrader.android.app.fragment.my_account.banking.deposit;

import com.anyoption.android.app.fragment.my_account.banking.deposit.DepositMenuFragment;
import com.etrader.android.app.R;
import com.etrader.android.app.util.ETRegulationUtils;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ETDepositMenuFragment extends DepositMenuFragment{

	public static ETDepositMenuFragment newInstance(Bundle bundle){
		ETDepositMenuFragment fragment = new ETDepositMenuFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = super.onCreateView(inflater, container, savedInstanceState);
		
		View funnelStepsLayout = rootView.findViewById(R.id.deposit_menu_funnel_steps_layout);
		boolean isRegulationFunnel = ETRegulationUtils.isRegulated() && !ETRegulationUtils.checkFirstDeposit();
		funnelStepsLayout.setVisibility((isRegulationFunnel) ? View.VISIBLE : View.GONE);
		
		return rootView;
	}
}