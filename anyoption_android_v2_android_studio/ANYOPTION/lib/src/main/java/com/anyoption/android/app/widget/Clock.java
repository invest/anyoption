package com.anyoption.android.app.widget;

import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.TimeOffsetUpdateEvent;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.android.app.util.constants.Constants;

import android.content.Context;
import android.os.Handler;
import android.text.format.DateFormat;
import android.util.AttributeSet;

/**
 * Custom Digital Clock like {@link android.widget.DigitalClock} but without seconds.
 * 
 * @author Anastas Arnaudov
 */
public class Clock extends TextView {

	private ClockRunnable ticker;

	private Handler handler;
	private long timeOffset;

	Timer timer;
	TimerTask timerTask;

	Context context;

	public Clock(Context context) {
		super(context);
		initClock();
	}

	public Clock(Context context, AttributeSet attrs) {
		super(context, attrs);
		initClock();
	}

	public Clock(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initClock();
	}

	private void initClock() {
		if (!isInEditMode()) {
			timeOffset = Application.get().getSharedPreferencesManager().getLong(Constants.PREFERENCES_TIME_OFFSET, 0l);
			Application.get().registerForEvents(this, TimeOffsetUpdateEvent.class);
		} else {
			setText("Clock");
		}
	}

	@Override
	protected void onAttachedToWindow() {
		if (!isInEditMode()) {
			super.onAttachedToWindow();
			handler = new Handler();
			ticker = new ClockRunnable(handler, timeOffset);

			ticker.run();
			updateTime();
		}
	}

	@Override
	protected void onDetachedFromWindow() {
		Application.get().unregisterForAllEvents(this);
		ticker.setViewAttached(false);
		
		super.onDetachedFromWindow();
	}

	public void onEvent(TimeOffsetUpdateEvent event) {
		timeOffset = event.getTimeOffsetInMilliseconds();

		if (ticker != null) {
			ticker.setTimeOffset(timeOffset);
		}

		postInvalidate();
	}

	public void updateTime(){
		timeOffset = Application.get().getSharedPreferencesManager().getLong(Constants.PREFERENCES_TIME_OFFSET, 0l);
		ticker.setTimeOffset(timeOffset);
		postInvalidate();
	}
	
	private class ClockRunnable implements Runnable {

		private final static String CLOCK_FORMAT_24 = "k:mm";

		private WeakReference<Handler> handlerReference;
		private Calendar clockCalendar;
		private boolean isViewAttached;
		private long clockTimeOffset;

		public ClockRunnable(Handler handler, long timeOffset) {
			handlerReference = new WeakReference<Handler>(handler);
			this.clockTimeOffset = timeOffset;

			clockCalendar = Calendar.getInstance(Application.get().getLocale());
			clockCalendar.setTimeInMillis(TimeUtils.getCurrentServerTime());
			isViewAttached = true;
		}

		@Override
		public void run() {
			if (!isViewAttached) {
				return;
			}

			setupTime();
			setText(DateFormat.format(CLOCK_FORMAT_24, clockCalendar));
			invalidate();
			// Every minute
			if (handlerReference.get() != null) {
				handlerReference.get().postDelayed(this,
						60 * 1000 - System.currentTimeMillis() % (60 * 1000));
				// Every second
				// handler.postDelayed(ticker, 1000 - System.currentTimeMillis() % (1000));
			}
		}

		public void setViewAttached(boolean isAttached) {
			isViewAttached = isAttached;
		}

		public void setTimeOffset(long timeOffset) {
			this.clockTimeOffset = timeOffset;
		}

		private void setupTime() {
			if (clockCalendar == null) {
				clockCalendar = Calendar.getInstance(Application.get().getLocale());
			}

			clockCalendar.setTimeInMillis(System.currentTimeMillis() + clockTimeOffset);
		}
	}

}
