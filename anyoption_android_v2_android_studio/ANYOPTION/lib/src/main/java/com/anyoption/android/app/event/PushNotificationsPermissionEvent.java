package com.anyoption.android.app.event;

/**
 * Event that occurs when the user changes whether the application should receive Push Notification or not.
 * @author Anastas Arnaudov
 *
 */
public class PushNotificationsPermissionEvent {

	private boolean isPushNotificationsAllowed;
	
	/**
	 * {@link PushNotificationsPermissionEvent}
	 * @param isPushNotificationsAllowed Whether the application should receive Push Notification or not.
	 */
	public PushNotificationsPermissionEvent(boolean isPushNotificationsAllowed){
		setPushNotificationsAllowed(isPushNotificationsAllowed);
	}

	public boolean isPushNotificationsAllowed() {
		return isPushNotificationsAllowed;
	}

	public void setPushNotificationsAllowed(boolean isPushNotificationsAllowed) {
		this.isPushNotificationsAllowed = isPushNotificationsAllowed;
	}
	
}
