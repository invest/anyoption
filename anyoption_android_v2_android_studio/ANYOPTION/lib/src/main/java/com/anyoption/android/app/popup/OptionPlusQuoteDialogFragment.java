package com.anyoption.android.app.popup;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import com.anyoption.android.app.Application;
import com.anyoption.android.R;
import com.anyoption.android.app.event.InvestmentEvent;
import com.anyoption.android.app.event.LightstreamerUpdateEvent;
import com.anyoption.android.app.manager.CommunicationManager;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.InvestmentUtil;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.json.requests.OptionPlusMethodRequest;
import com.anyoption.json.results.OptionPlusMethodResult;
import com.lightstreamer.ls_client.UpdateInfo;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Fullscreen Dialog Fragment for option plus get quote.
 * 
 * @author Mario Kutlev
 */
public class OptionPlusQuoteDialogFragment extends BaseDialogFragment {

	private static final String TAG = OptionPlusQuoteDialogFragment.class.getSimpleName();

	private static final String ARG_INVESTMENT_KEY = "arg_investment_key";
	private static final String ARG_DECIMAL_POINT_DIGITS_KEY = "arg_dec_point_digits_key";
	private static final String ARG_CURRENT_LEVEL_KEY = "arg_current_level_key";

	private static final double LEVEL_UNKNOWN = -1;
	private static final int DEFAULT_DECIMAL_POINT_DIGITS = 2;

	private static final long TIMER_TIME = 7000;
	private static final long TIMER_UPDATE_INVERVAL = 10;

	private static final String CLOSING_TIME_FORMAT_PATTERN = "dd/MM/yy HH:mm";
	private static final String INVESTED_TIME_FORMAT_PATTERN = "HH:mm";

	private View contentRoot;
	private TextView currentLevelView;
	private View profitableImageView;

	// Buttons
	private View okButton;
	private View notNowButton;
	private View sellButton;
	private View getQuoteButton;

	private TextView offerValidTimeBackTextView;
	private TextView offerValidTimeFrontTextView;
	private View offerValidFrontLayout;

	private View expiredView;
	private View soldView;

	private TextView infoLargeText;
	private TextView infoSmallText;

	private User user;
	private Application application;
	private SkinGroup skinGroup;
	private CommunicationManager communicationManager;
	private Investment investment;

	private double currentLevel;
	private DecimalFormat levelFormat;

	private double price;
	private CountDownTimer timer;

	private RelativeLayout progressRootLayout;
	private TextView offerValidLabelFrontTextView;

	private boolean isGetQuoteLoading;
	private boolean isSellRequested;

	public static OptionPlusQuoteDialogFragment newInstance(Bundle bundle) {
		OptionPlusQuoteDialogFragment fragment = new OptionPlusQuoteDialogFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	public static OptionPlusQuoteDialogFragment newInstance(Investment investment,
			int decimalPointDigits, double currentLevel) {
		OptionPlusQuoteDialogFragment fragment = new OptionPlusQuoteDialogFragment();
		Bundle args = new Bundle();
		args.putSerializable(ARG_INVESTMENT_KEY, investment);
		args.putInt(ARG_DECIMAL_POINT_DIGITS_KEY, decimalPointDigits);
		args.putDouble(ARG_CURRENT_LEVEL_KEY, currentLevel);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		investment = (Investment) args.getSerializable(ARG_INVESTMENT_KEY);

		if (investment == null) {
			throw new IllegalArgumentException("Can not create the dialog without investment!");
		}

		application = Application.get();
		user = application.getUser();
		Skin skin = application.getSkins().get(application.getSkinId());
		skinGroup = skin.getSkinGroup();
		communicationManager = application.getCommunicationManager();

		int decimalPointDigits = args.getInt(ARG_DECIMAL_POINT_DIGITS_KEY,
				DEFAULT_DECIMAL_POINT_DIGITS);
		levelFormat = Utils.getDecimalFormat(decimalPointDigits);
		currentLevel = args.getDouble(ARG_CURRENT_LEVEL_KEY, LEVEL_UNKNOWN);

		isGetQuoteLoading = false;
		isSellRequested = false;
		price = -1;
	}

	@Override
	protected void onCreateContentView(View contentView) {
		contentRoot = contentView;
		setBackgroundColor(ColorUtils.getColor(R.color.opt_plus_quote_dialog_backgr));
		setUpButtons();
		setInvestmentInfo();

		currentLevelView = (TextView) contentRoot.findViewById(R.id.opt_plus_quote_current_level);
		if (currentLevel != LEVEL_UNKNOWN) {
			currentLevelView.setText(levelFormat.format(currentLevel));
		}

		profitableImageView = contentRoot.findViewById(R.id.opt_plus_quote_profitable_image);

		((TextView) contentRoot.findViewById(R.id.opt_plus_quote_offer_valid_label_back))
				.setText(getString(R.string.optionPlusOfferValid) + ":");

		// Progress Bar:
		progressRootLayout = (RelativeLayout) contentRoot
				.findViewById(R.id.opt_plus_quote_progress_root);
		offerValidTimeBackTextView = (TextView) contentRoot
				.findViewById(R.id.opt_plus_quote_offer_valid_time_back);

		offerValidFrontLayout = contentRoot
				.findViewById(R.id.opt_plus_quote_offer_valid_layout_front);
		offerValidLabelFrontTextView = (TextView) contentRoot
				.findViewById(R.id.opt_plus_quote_offer_valid_label_front);
		offerValidLabelFrontTextView.setText(getString(R.string.optionPlusOfferValid) + ":");
		offerValidTimeFrontTextView = (TextView) contentRoot
				.findViewById(R.id.opt_plus_quote_offer_valid_time_front);

		OnLayoutChangeListener onLayoutChangeListener = new OnLayoutChangeListener() {

			@Override
			public void onLayoutChange(View v, int left, int top, int right, int bottom,
					int oldLeft, int oldTop, int oldRight, int oldBottom) {
				if (offerValidLabelFrontTextView.getWidth() > 0) {
					offerValidLabelFrontTextView.getLayoutParams().width = progressRootLayout
							.getWidth();
					offerValidTimeFrontTextView.getLayoutParams().width = progressRootLayout
							.getWidth();
				}
			}
		};
		offerValidLabelFrontTextView.addOnLayoutChangeListener(onLayoutChangeListener);
		offerValidTimeFrontTextView.addOnLayoutChangeListener(onLayoutChangeListener);
		// ///////////////////////////////

		expiredView = contentRoot.findViewById(R.id.opt_plus_quote_expired);
		soldView = contentRoot.findViewById(R.id.opt_plus_quote_sold);

		infoLargeText = (TextView) contentRoot.findViewById(R.id.opt_plus_quote_info_large);
		infoSmallText = (TextView) contentRoot.findViewById(R.id.opt_plus_quote_info_small);

		getQuote();
	}

	@Override
	protected int getContentLayout() {
		return R.layout.option_plus_quote_dialog;
	}

	@Override
	public void onResume() {
		super.onResume();
		// Register for events
		application.registerForEvents(this, LightstreamerUpdateEvent.class);
	}

	@Override
	public void onPause() {
		application.unregisterForAllEvents(this);

		super.onPause();
	}

	@Override
	public void onDestroy() {
		application.unregisterForAllEvents(this);

		super.onDestroy();
	}

	/**
	 * Initialize and set click listeners to the buttons
	 */
	private void setUpButtons() {
		okButton = contentRoot.findViewById(R.id.opt_plus_quote_button_ok);
		okButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});

		notNowButton = contentRoot.findViewById(R.id.opt_plus_quote_button_not_now);
		notNowButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});

		sellButton = contentRoot.findViewById(R.id.opt_plus_quote_button_sell);
		sellButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				sell();
			}
		});

		getQuoteButton = contentRoot.findViewById(R.id.opt_plus_quote_button_get_quote);
		getQuoteButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				getQuote();
			}
		});

	}

	/**
	 * Requests the price for this option.
	 */
	private void getQuote() {
		if (isGetQuoteLoading) {
			return;
		}

		isGetQuoteLoading = true;

		OptionPlusMethodRequest request = new OptionPlusMethodRequest();
		request.setInvestmentId(investment.getId());
		communicationManager.requestService(this, "getQuoteCallBack", Constants.SERVICE_GET_PRICE,
				request, OptionPlusMethodResult.class, true, true);
	}

	public void getQuoteCallBack(Object result) {
		isGetQuoteLoading = false;

		OptionPlusMethodResult optionPlusResult = (OptionPlusMethodResult) result;

		if (optionPlusResult != null
				&& optionPlusResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			infoLargeText.setText(optionPlusResult.getPriceTxt());
			price = Double.valueOf(optionPlusResult.getPrice());
			infoSmallText.setText(R.string.optionPlusBuyOptionFor);
			startTimer();

			showExpiringStateLayout();
		} else {
			Log.e(TAG, "getQuoteCallBack error -> ErrorCode: " + optionPlusResult.getErrorCode());

			// Close the dialog in case of error.
			dismiss();
		}
	}

	private void sell() {
		if (isSellRequested || price < 0) {
			return;
		}

		isSellRequested = true;

		if (timer != null) {
			timer.cancel();
		}

		OptionPlusMethodRequest request = new OptionPlusMethodRequest();
		request.setPrice(price);
		request.setInvestmentId(investment.getId());
		communicationManager.requestService(this, "sellCallBack",Constants.SERVICE_OPTION_PLUS_FLASH_SELL, request, OptionPlusMethodResult.class,true, true);
	}

	public void sellCallBack(Object resultObj) {
		isSellRequested = false;

		OptionPlusMethodResult result = (OptionPlusMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "sellCallBack - Success");
			showSoldStateLayout();

			// Update the Balance:
			long newBalance = user.getBalance() + (long) (price * 100);
			user.setBalance(newBalance);
			user.setBalanceWF(AmountUtil.getFormattedAmount(newBalance, user.getCurrency()));

			// Updated user's investmentCount
			long investmentsCount = user.getOpenedInvestmentsCount() - 1;
			user.setOpenedInvestmentsCount(investmentsCount);
			application.setUser(user);

			application.postEvent(new InvestmentEvent(investment.getMarketId(),InvestmentEvent.Type.INVESTMENT_SOLD, investment));
		} else {
			Log.e(TAG, "sellCallBack - Sell failed -> " + result != null ? "ErrorCode: "+ result.getErrorCode() : "the result is null");

			// Close the dialog in case of error.
			dismiss();
		}
	}

	/**
	 * Called when a {@link LightstreamerUpdateEvent} occurs.
	 * 
	 * @param event
	 */
	// TODO Update: Use onEvent to call this method on LS thread!
	public void onEventMainThread(LightstreamerUpdateEvent event) {
		if (event.getMarketId() == investment.getMarketId()) {
			updateItem(event.getUpdateInfo());
		}
	}

	public void updateItem(UpdateInfo update) {
		long oppId = Long.parseLong(update.getNewValue("ET_OPP_ID"));

		if (oppId != investment.getOpportunityId()) {
			// The update is not for this opportunity.
			return;
		}

		try {
			if (update.isValueChanged("ET_STATE")) {
				int oppState = Integer.parseInt(update.getNewValue("ET_STATE"));
				if (oppState >= Opportunity.STATE_CLOSING_1_MIN) {
					Log.d(TAG, "got new state = " + oppState);
					Log.d(TAG, "Waiting for Expiry!");
					// TODO Will we show waiting for expiry info or close the dialog? The user won't
					// be able to sell his option.
				}
			}

			if (update.isValueChanged(skinGroup.getLevelUpdateKey())) {
				String levelStr = update.getNewValue(skinGroup.getLevelUpdateKey());
				currentLevel = Double.parseDouble(levelStr.replaceAll(",", ""));
				currentLevelView.setText(levelFormat.format(currentLevel));
				if (InvestmentUtil.isOpenInvestmentProfitable(investment, currentLevel)) {
					profitableImageView.setVisibility(View.VISIBLE);
				} else {
					profitableImageView.setVisibility(View.GONE);
				}
			}
		} catch (Exception e) {
			Log.e(TAG, "Problem processing updateItem().", e);
		}
	}

	@SuppressLint("SimpleDateFormat")
	private void setInvestmentInfo() {
		TextView assetNameTextView = (TextView) contentRoot
				.findViewById(R.id.opt_plus_quote_asset_name);
		assetNameTextView.setText(investment.getAsset());

		// Set the expiry time with smaller AM/PM font size.
		TextView expiryTimeView = (TextView) contentRoot
				.findViewById(R.id.opt_plus_quote_expire_time);
		SimpleDateFormat timeFormat = new SimpleDateFormat(CLOSING_TIME_FORMAT_PATTERN);
		expiryTimeView.setText(timeFormat.format(investment.getTimeEstClosing()));

		TextView timeInvestedView = (TextView) contentRoot
				.findViewById(R.id.opt_plus_quote_invest_time);
		timeFormat.applyPattern(INVESTED_TIME_FORMAT_PATTERN);
		timeInvestedView.setText(timeFormat.format(investment.getTimeCreated()));

		TextView levelInvestedView = (TextView) contentRoot
				.findViewById(R.id.opt_plus_quote_invest_level);
		levelInvestedView.setText(investment.getLevel());

		TextView amountInvestedView = (TextView) contentRoot
				.findViewById(R.id.opt_plus_quote_amount_invested);
		amountInvestedView.setText(AmountUtil.getFormattedAmount(investment.getAmount(),
				user.getCurrency()));

		ImageView putCallIcon = (ImageView) contentRoot
				.findViewById(R.id.opt_plus_quote_put_call_icon);
		if (investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL) {
			putCallIcon.setImageResource(R.drawable.oplus_circle_arrow_up);
		} else if (investment.getTypeId() == Investment.INVESTMENT_TYPE_PUT) {
			putCallIcon.setImageResource(R.drawable.oplus_circle_arrow_down);
		}
	}

	private void showExpiringStateLayout() {
		infoSmallText.setText(R.string.optionPlusBuyOptionFor);

		soldView.setVisibility(View.GONE);
		okButton.setVisibility(View.GONE);
		getQuoteButton.setVisibility(View.GONE);

		notNowButton.setVisibility(View.VISIBLE);
		sellButton.setVisibility(View.VISIBLE);
	}

	private void showExpiredStateLayout() {
		infoSmallText.setText(R.string.optionPlusOfferNoValid);
		infoLargeText.setText(R.string.optionPlusGetNewQuote);

		soldView.setVisibility(View.GONE);
		expiredView.setVisibility(View.VISIBLE);

		okButton.setVisibility(View.GONE);
		sellButton.setVisibility(View.GONE);

		notNowButton.setVisibility(View.VISIBLE);
		getQuoteButton.setVisibility(View.VISIBLE);
	}

	private void showSoldStateLayout() {
		soldView.setVisibility(View.VISIBLE);
		infoSmallText.setText(R.string.optionPlusAmountAdded);

		notNowButton.setVisibility(View.GONE);
		sellButton.setVisibility(View.GONE);
		getQuoteButton.setVisibility(View.GONE);

		okButton.setVisibility(View.VISIBLE);
	}

	private void startTimer() {

		expiredView.setVisibility(View.GONE);

		timer = new CountDownTimer(TIMER_TIME, TIMER_UPDATE_INVERVAL) {

			@Override
			public void onTick(long millisUntilFinished) {
				String time = TimeUtils.formatTimeToSecondsAndMilliseconds(millisUntilFinished);
				offerValidTimeBackTextView.setText(time);
				offerValidTimeFrontTextView.setText(time);
			}

			@Override
			public void onFinish() {
				showExpiredStateLayout();
			}
		}.start();
		
		ValueAnimator progressAnimation = ValueAnimator.ofInt(0, progressRootLayout.getWidth());
		progressAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

			@Override
			public void onAnimationUpdate(ValueAnimator valueAnimator) {
				int newWidth = (Integer) valueAnimator.getAnimatedValue();
				ViewGroup.LayoutParams layoutParams = offerValidFrontLayout.getLayoutParams();
				layoutParams.width = newWidth;
				offerValidFrontLayout.setLayoutParams(layoutParams);
			}
		});
		progressAnimation.setDuration(TIMER_TIME);
		progressAnimation.start();
	}

}
