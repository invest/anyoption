package com.anyoption.android.app.popup;

import java.text.SimpleDateFormat;

import com.anyoption.android.app.Application;
import com.anyoption.android.R;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.InvestmentUtil;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.User;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Fullscreen Dialog Fragment showing investment receipt.
 * 
 * @author Mario Kutlev
 */
public class ReceiptDialogFragment extends BaseDialogFragment {

	private static final String TAG = ReceiptDialogFragment.class.getSimpleName();

	public static final String ARG_INVESTMENT_KEY = "arg_investment_key";
	protected static final String TIME_DATE_FORMAT_PATTERN = "HH:mm, dd/MM/yyyy";

	protected User user;
	protected Investment investment;

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static ReceiptDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new ReceiptDialogFragment.");
		ReceiptDialogFragment fragment = new ReceiptDialogFragment();
		fragment.setArguments(bundle);

		return fragment;
	}

	public static ReceiptDialogFragment newInstance(Investment investment) {
		Bundle args = new Bundle();
		args.putSerializable(ARG_INVESTMENT_KEY, investment);
		return newInstance(args);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		investment = (Investment) args.getSerializable(ARG_INVESTMENT_KEY);

		user = Application.get().getUser();
	}

	/**
	 * Set up content.
	 * 
	 * @param contentView
	 */
	@Override
	protected void onCreateContentView(View contentView) {
		View okButton = contentView.findViewById(R.id.receipt_ok_button);
		okButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});

		TextView levelTextView = (TextView) contentView.findViewById(R.id.receipt_level);
		levelTextView.setText(investment.getLevel());

		TextView assetNameTextView = (TextView) contentView.findViewById(R.id.receipt_asset_name);
		assetNameTextView.setText(investment.getAsset());

		TextView expiryTimeLabelTextView = (TextView) contentView.findViewById(R.id.receipt_expiry_time_label);
		expiryTimeLabelTextView.setText(expiryTimeLabelTextView.getText().toString() + ":");
		TextView expiryTimeTextView = (TextView) contentView.findViewById(R.id.receipt_expiry_time);
		SimpleDateFormat expiryTimeFormat = new SimpleDateFormat(TIME_DATE_FORMAT_PATTERN);
		String expriryTimeFormatted = expiryTimeFormat.format(investment.getTimeEstClosing());
		expiryTimeTextView.setText(expriryTimeFormatted);

		Currency currency = user.getCurrency();
		TextView amountTextView = (TextView) contentView.findViewById(R.id.receipt_amount);
		amountTextView.setText(AmountUtil.getFormattedAmount(investment.getAmount(), currency));

		TextView profitLabelTextView = (TextView) contentView
				.findViewById(R.id.receipt_profit_label);
		profitLabelTextView.setText(getString(R.string.receiptProfit) + ":");

		TextView profitTextView = (TextView) contentView.findViewById(R.id.receipt_profit_percent);
		profitTextView.setText(Math.round((investment.getOddsWin() - 1) * 100) + "%");

		TextView expireAboveLevelTextView = (TextView) contentView
				.findViewById(R.id.receipt_expire_above);
		expireAboveLevelTextView.setText(getString(R.string.expAboveLevel,
				investment.getLevel()));

		TextView expireBelowLevelTextView = (TextView) contentView
				.findViewById(R.id.receipt_expire_below);
		expireBelowLevelTextView.setText(getString(R.string.expBelowLevel,
				investment.getLevel()));

		// +/- 1 is used to find the return for lower and higher level.
		double amount = InvestmentUtil.getOpenInvestmentReturn(investment,
				investment.getCurrentLevel() + 1);
		TextView expireAboveAmountTextView = (TextView) contentView
				.findViewById(R.id.receipt_expire_above_amount);
		expireAboveAmountTextView.setText(AmountUtil.getFormattedAmount(amount, currency));

		amount = InvestmentUtil.getOpenInvestmentReturn(investment, investment.getCurrentLevel() - 1);
		TextView expireBelowAmountTextView = (TextView) contentView
				.findViewById(R.id.receipt_expire_below_amount);
		expireBelowAmountTextView.setText(AmountUtil.getFormattedAmount(amount, currency));

		TextView idTextView = (TextView) contentView.findViewById(R.id.receipt_id);
		idTextView.setText(getString(R.string.receiptId, investment.getId()));

		ImageView putCallIcon = (ImageView) contentView.findViewById(R.id.receipt_put_call_icon);

		// Display yellow ribbon and add yellow text color
		if (investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL
				|| (investment.getTypeId() == Investment.INVESTMENT_TYPE_ONE && investment
						.getOneTouchUpDown() == Investment.INVESTMENT_TYPE_CALL)) {
			putCallIcon.setImageResource(R.drawable.circle_with_triangle_up_white);
			contentView.findViewById(R.id.receipt_expire_above_ribbon).setVisibility(View.VISIBLE);
			expireAboveLevelTextView.setTextColor(ColorUtils.getColor(
					R.color.receipt_win_text_color));
			expireAboveAmountTextView.setTextColor(ColorUtils.getColor(
					R.color.receipt_win_text_color));
		} else if (investment.getTypeId() == Investment.INVESTMENT_TYPE_PUT
				|| (investment.getTypeId() == Investment.INVESTMENT_TYPE_ONE && investment
						.getOneTouchUpDown() == Investment.INVESTMENT_TYPE_PUT)) {
			putCallIcon.setImageResource(R.drawable.circle_with_triangle_down_white);
			contentView.findViewById(R.id.receipt_expire_below_ribbon).setVisibility(View.VISIBLE);
			expireBelowLevelTextView.setTextColor(ColorUtils.getColor(
					R.color.receipt_win_text_color));
			expireBelowAmountTextView.setTextColor(ColorUtils.getColor(
					R.color.receipt_win_text_color));
		}

	}

	@Override
	protected int getContentLayout() {
		return R.layout.receipt_dialog;
	}

}
