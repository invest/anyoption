package com.anyoption.android.app.model;

import java.util.Date;

/**
 * @author Anastas Arnaudov
 */
public class OptionDynamicsItem {

    public enum InvestType{
        CALL,
        PUT
    }

    public long id;
    public boolean isClosed             = false;
    public boolean isShowOptionClosed   = false;
    public boolean isClosingOption      = false;
    public boolean isCloseEnabled       = false;
    public boolean isAvailable          = false;
    public InvestType investType        = InvestType.CALL;
    public boolean isInTheMoneyForClose = false;
    public boolean isInTheMoney         = false;

    public Date time                    = new Date();
    public double investAmount;
    public double returnAmount;
    public double closeForAmount;
    public double closedForAmount;
    public double price;
    public double profitPercent;
    public double investLevel;
    public String returnAmountFormated;
}
