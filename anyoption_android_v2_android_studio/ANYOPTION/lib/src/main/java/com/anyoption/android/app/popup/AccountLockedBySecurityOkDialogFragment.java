package com.anyoption.android.app.popup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.anyoption.android.R;
import com.anyoption.android.app.util.constants.Constants;

public class AccountLockedBySecurityOkDialogFragment extends LowBalanceOkDialogFragment {

	public static final String TAG = AccountLockedBySecurityOkDialogFragment.class.getSimpleName();

	protected ProgressBar loading;

	public static AccountLockedBySecurityOkDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new AOLowBalanceOkDialogFragment.");
		AccountLockedBySecurityOkDialogFragment fragment = new AccountLockedBySecurityOkDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if(args != null){
			setCancelable(args.getBoolean(Constants.EXTRA_CANCELABLE, false));
		}
	}

	@Override
	protected int getContentLayout() {
		return 0;
	}

	@Override
	protected void onCreateContentView(View contentView) {

	}
	
}
