package com.anyoption.android.app.event;

import com.anyoption.common.service.results.UserMethodResult;

/**
 * Event that occurs when the user logs in/out in the Application.
 * 
 * @author Anastas Arnaudov
 * 
 */
public class LoginLogoutEvent {

	/**
	 * Login event type
	 */
	public enum Type {
		LOGIN, 
		LOGOUT,
		REGISTER
	}

	private Type type;
	private UserMethodResult userResult;
	private boolean shouldChangeLocale = true;
	
	/**
	 * {@link LoginLogoutEvent}
	 * 
	 * @param type Login or Logout.
	 * @param userResult The result object that holds information about the user. Null for Logout.
	 */
	public LoginLogoutEvent(Type type, UserMethodResult userResult) {
		this.type = type;
		this.setUserResult(userResult);
	}
	
	/**
	 * {@link LoginLogoutEvent}
	 * 
	 * @param type Login or Logout.
	 * @param userResult The result object that holds information about the user. Null for Logout.
	 * @param shouldChangeLocale Whether should change the Locale or NOT.
	 */
	public LoginLogoutEvent(Type type, UserMethodResult userResult, boolean shouldChangeLocale) {
		this.type = type;
		this.setUserResult(userResult);
		this.setShouldChangeLocale(shouldChangeLocale);
	}
	
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public UserMethodResult getUserResult() {
		return userResult;
	}

	public void setUserResult(UserMethodResult userResult) {
		this.userResult = userResult;
	}

	public boolean isShouldChangeLocale() {
		return shouldChangeLocale;
	}

	public void setShouldChangeLocale(boolean shouldChangeLocale) {
		this.shouldChangeLocale = shouldChangeLocale;
	}

}
