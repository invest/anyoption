package com.anyoption.android.app.receiver;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.ConnectionStateChangeEvent;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Network Receiver that listens to network state changes.
 * @author Anastas Arnaudov
 *
 */
public class ConnectionStateReceiver extends BroadcastReceiver{

	public static final String TAG = ConnectionStateReceiver.class.getSimpleName();

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "onReceive");

		Application.get().postEvent(new ConnectionStateChangeEvent());
	}

}
