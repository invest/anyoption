package com.anyoption.android.app.manager;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.DepositEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.util.AmountUtil;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;

import android.util.Log;

/**
 * Manages calls to the Google Analytics SDK.
 *
 * @author anastasa
 */
public class GoogleAnalyticsManager {

    public static final String TAG = GoogleAnalyticsManager.class.getSimpleName();

    public static final String EVENT_CATEGORY_NAVIGATION = "Navigation";
    public static final String EVENT_CATEGORY_FIRST_DEPOSIT = "First Deposit";
    public static final String EVENT_CATEGORY_DEPOSIT = "Deposit";

    public static final String EVENT_REGISTER_SCREEN = "Register Screen";
    public static final String EVENT_FIRST_DEPOSIT = "First Deposit";
    public static final String EVENT_DEPOSIT = "Deposit";

    protected Tracker generalTracker;
    protected Tracker additionalTracker;
    protected Tracker oldTracker;

    public GoogleAnalyticsManager(int additionalTrackerConfigResId) {
        Log.d(TAG, "Init.");
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(Application.get());
        analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
        generalTracker = analytics.newTracker(R.xml.general_app_tracker);
        additionalTracker = analytics.newTracker(additionalTrackerConfigResId);

        setUserId("");

        registerForEvents();
    }

    public GoogleAnalyticsManager(int additionalTrackerConfigResId, int oldTrackerConfigResId) {
        Log.d(TAG, "Init.");
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(Application.get());
        analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
        generalTracker = analytics.newTracker(R.xml.general_app_tracker);
        additionalTracker = analytics.newTracker(additionalTrackerConfigResId);
        oldTracker = analytics.newTracker(oldTrackerConfigResId);

        setUserId("");

        registerForEvents();
    }


    private void registerForEvents() {
        Application.get().registerForEvents(this, NavigationEvent.class, DepositEvent.class, LoginLogoutEvent.class);
    }


    public void setUserId(String userId) {
        generalTracker.setClientId(userId);
        additionalTracker.setClientId(userId);

        if(oldTracker != null && oldTracker.isInitialized()) {
            oldTracker.setClientId(userId);
        }
    }


    //TODO ADD OR CHANGE DIFFERENT DIMENSIONS
    private void setDimensions() {
        if (true) {
            generalTracker.send(new HitBuilders.ScreenViewBuilder()
                    .setCustomDimension(1, "")
                    .setCustomDimension(2, "")
                    .setCustomDimension(3, "")
                    .setCustomDimension(4, "")
                    .setCustomDimension(5, "")
                    .setCustomDimension(6, "")
                    .build());
        } else if (false) {
            generalTracker.send(new HitBuilders.ScreenViewBuilder()
                    .setCustomDimension(1, "")
                    .setCustomDimension(2, "")
                    .setCustomDimension(3, "")
                    .setCustomDimension(4, "")
                    .setCustomDimension(5, "")
                    .setCustomDimension(6, "")
                    .build());
        }
    }


    ///////////				CATCHING	EVENTS				////////////////////
    public void onEvent(LoginLogoutEvent event) {
        if (event.getType() == LoginLogoutEvent.Type.REGISTER) {
            setUserId(String.valueOf(event.getUserResult().getUser().getId()));
            Log.d(TAG, "Set user id to: " + String.valueOf(event.getUserResult().getUser().getId()));
        } else if (event.getType() == LoginLogoutEvent.Type.LOGIN) {
            setUserId(String.valueOf(event.getUserResult().getUser().getId()));
            Log.d(TAG, "Set user id to: " + String.valueOf(event.getUserResult().getUser().getId()));
        } else if (event.getType() == LoginLogoutEvent.Type.LOGOUT) {
            setUserId("");
            Log.d(TAG, "Set user id to: \"\"");
        }
    }


    //TODO SCREEN EVENTS
    public void onEvent(NavigationEvent event) {
        Screenable screenable = event.getToScreen();

        if (screenable == Screen.REGISTER) {
            Log.d(TAG, "Send Register event");
            generalTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(EVENT_CATEGORY_NAVIGATION)
                    .setAction(EVENT_REGISTER_SCREEN)
                    .build());

            additionalTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(EVENT_CATEGORY_NAVIGATION)
                    .setAction(EVENT_REGISTER_SCREEN)
                    .build());

            if(oldTracker != null && oldTracker.isInitialized()) {
                oldTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(EVENT_CATEGORY_NAVIGATION)
                        .setAction(EVENT_REGISTER_SCREEN)
                        .build());
            }

            Log.d(TAG, "Sent Register event");
        }
    }


    //TODO ACTION EVENTS
    public void onEvent(DepositEvent event) {
        if (event.isFirstDeposit()) {
            Log.d(TAG, "Sending First Deposit Event.");
            generalTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(EVENT_CATEGORY_FIRST_DEPOSIT)
                    .setAction(EVENT_FIRST_DEPOSIT + " : " + AmountUtil.getUserFormattedAmount(event.getAmount()))
                    .build());

            additionalTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(EVENT_CATEGORY_FIRST_DEPOSIT)
                    .setAction(EVENT_FIRST_DEPOSIT + " : " + AmountUtil.getUserFormattedAmount(event.getAmount()))
                    .build());

            if(oldTracker != null && oldTracker.isInitialized()) {
                oldTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(EVENT_CATEGORY_FIRST_DEPOSIT)
                        .setAction(EVENT_FIRST_DEPOSIT + " : " + AmountUtil.getUserFormattedAmount(event.getAmount()))
                        .build());
            }
            Log.d(TAG, "Sent First Deposit Event");
        } else {
            Log.d(TAG, "Sending Deposit Event.");
            generalTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(EVENT_CATEGORY_DEPOSIT)
                    .setAction(EVENT_DEPOSIT + " : " + AmountUtil.getUserFormattedAmount(event.getAmount()))
                    .build());

            additionalTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(EVENT_CATEGORY_FIRST_DEPOSIT)
                    .setAction(EVENT_FIRST_DEPOSIT + " : " + AmountUtil.getUserFormattedAmount(event.getAmount()))
                    .build());

            if(oldTracker != null && oldTracker.isInitialized()) {
                oldTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(EVENT_CATEGORY_FIRST_DEPOSIT)
                        .setAction(EVENT_FIRST_DEPOSIT + " : " + AmountUtil.getUserFormattedAmount(event.getAmount()))
                        .build());
            }

            Log.d(TAG, "Sent Deposit Event");
        }
    }

    ///////////////////////////////////////////////////////////////////
}
