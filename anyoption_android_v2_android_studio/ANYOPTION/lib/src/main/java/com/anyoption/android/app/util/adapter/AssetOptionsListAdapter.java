package com.anyoption.android.app.util.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.anyoption.android.app.Application;
import com.anyoption.android.R;
import com.anyoption.android.app.manager.FragmentManager;
import com.anyoption.android.app.popup.BaseDialogFragment.OnDialogDismissListener;
import com.anyoption.android.app.popup.OptionPlusQuoteDialogFragment;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.InvestmentUtil;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.Opportunity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AssetOptionsListAdapter extends BaseExpandableListAdapter implements OnClickListener{

	private static final String TAG = AssetOptionsListAdapter.class.getSimpleName();

	public static final int OPEN_OPTIONS_GROUP_ID = 0;
	public static final int SETTLED_OPTIONS_GROUP_ID = 1;

	public static final int GROUP_COUNT_OPEN_AND_SETTLED_OPTIONS = 2;
	public static final int GROUP_COUNT_OPEN_OR_SETTLED_OPTIONS = 1;

	private static final String OPEN_OPTION_FORMAT_PATTERN = "HH:mm";
	private static final String SETTLED_OPTION_FORMAT_PATTERN = "dd/MM | HH:mm";

	private static final SimpleDateFormat openOptionTimeFormat = new SimpleDateFormat(OPEN_OPTION_FORMAT_PATTERN);
	private static final SimpleDateFormat settledOptionTimeFormat = new SimpleDateFormat(SETTLED_OPTION_FORMAT_PATTERN);

	protected static final double LEVEL_UNKNOWN = -1;
	private static final int DEFAULT_DECIMAL_POINT_DIGITS = 2;

	protected Application application;
	private FragmentManager fragmentManager;
	protected Currency currency;

	private LayoutInflater inflater;
	private Market market;

	private ArrayList<Investment> openOptions;
	private ArrayList<Investment> settledOptions;
	protected double currentLevel;
	private int opportunityState;
	private int decimalPointDigits;

	private int openOptionsGroupPosition;
	private int settledOptionsGroupPosition;

	private Investment investmentGetQuate;
	private boolean isOptPlusDialogShown;

	public AssetOptionsListAdapter(Market market, ArrayList<Investment> openOptions,
			ArrayList<Investment> settledOptions) {
		this.market = market;
		this.openOptions = openOptions;
		this.settledOptions = settledOptions;

		setGroupPositions();
		currentLevel = LEVEL_UNKNOWN;
		opportunityState = 0;

		application = Application.get();
		fragmentManager = application.getFragmentManager();
		currency = application.getCurrency();
		inflater = LayoutInflater.from(application.getCurrentActivity());

		decimalPointDigits = DEFAULT_DECIMAL_POINT_DIGITS;
		isOptPlusDialogShown = false;
	}

	public AssetOptionsListAdapter(Market market) {
		this(market, new ArrayList<Investment>(), new ArrayList<Investment>());
	}
	
	private void setGroupPositions() {
		boolean areOpenOptionsDisplayed = false;
		if (openOptions != null && openOptions.size() > 0) {
			areOpenOptionsDisplayed = true;
		}

		if (areOpenOptionsDisplayed) {
			Log.d(TAG, "setGroupPositions -> Display 2 groups!");
			// The two groups will be displayed.
			openOptionsGroupPosition = 0;
			settledOptionsGroupPosition = 1;
		} else {
			Log.d(TAG, "setGroupPositions -> Display 1 group!");
			// The open options group won't be displayed.
			openOptionsGroupPosition = -1;
			settledOptionsGroupPosition = 0;
		}
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		if (groupPosition == openOptionsGroupPosition) {
			return openOptions.get(childPosition);
		} else if (groupPosition == settledOptionsGroupPosition) {
			return settledOptions.get(childPosition);
		} else {
			// This case should never happen.
			Log.e(TAG, "getChild -> Mama my stara!");
			return null;
		}
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return 0;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild,View convertView, ViewGroup parent) {

		View childView = convertView;
		if (childView == null) {
			childView = inflater.inflate(R.layout.asset_option_item_row, parent, false);
		}

		if((groupPosition == openOptionsGroupPosition && openOptions.size() == 0) || (groupPosition == settledOptionsGroupPosition && settledOptions.size() == 0)){
			Log.e(TAG, "getChildView -> Mama my stara!");
			return childView;
		}

		final Investment investment = groupPosition == openOptionsGroupPosition ? openOptions.get(childPosition) : settledOptions.get(childPosition);

		TextView levelInvestedView = (TextView) childView.findViewById(R.id.asset_option_level_invested);
		levelInvestedView.setText(investment.getLevel());

		TextView amountInvestedView = (TextView) childView.findViewById(R.id.asset_option_amount_invested);
		amountInvestedView.setText(investment.getAmountTxt());
		View profitLayout = childView.findViewById(R.id.asset_option_profit_layout);
		TextView profitView = (TextView) childView.findViewById(R.id.asset_option_profit);
		profitView.setText(investment.getAmountReturnWF());

		ImageView putCallIndicatorView = (ImageView) childView.findViewById(R.id.asset_option_put_call_indicator);
		TextView timeInvestedView = (TextView) childView.findViewById(R.id.asset_option_time_invested);
		View profitableImageView = childView.findViewById(R.id.option_profitable_image);
		ImageView liveDotView = (ImageView) childView.findViewById(R.id.asset_option_blue_live_dot);
		View getQuoteLayout = childView.findViewById(R.id.asset_option_get_quote_layout);

		// Set the fields which are different for open and settled options.
		if (groupPosition == openOptionsGroupPosition) {
			setUpPutCallIndicator(investment, putCallIndicatorView);
			liveDotView.setVisibility(View.VISIBLE);
			timeInvestedView.setText(openOptionTimeFormat.format(investment.getTimeCreated()));
			View getQuote = childView.findViewById(R.id.asset_option_get_quote_button);

			if (market != null && market.isOptionPlusMarket() && opportunityState < Opportunity.STATE_CLOSING_1_MIN) {
				profitLayout.setVisibility(View.GONE);
				
				profitableImageView = childView.findViewById(R.id.asset_option_get_quote_profitable_image);
				// Hide the other ribbon
				childView.findViewById(R.id.option_profitable_image).setVisibility(View.GONE);

				// Show get quote button layout
				getQuoteLayout.setVisibility(View.VISIBLE);
				getQuote.setOnClickListener(AssetOptionsListAdapter.this);
				getQuote.setTag(childPosition);
			} else {
				// Hide get quote button layout
				getQuoteLayout.setVisibility(View.GONE);
				childView.findViewById(R.id.option_profitable_image).setVisibility(View.GONE);
				getQuote.setOnClickListener(null);
				getQuote.setTag(null);
				profitLayout.setVisibility(View.VISIBLE);
			}

			// Changes the visibility of the yellow ribbon to the right depending on the current
			// level.
			if (currentLevel != LEVEL_UNKNOWN && InvestmentUtil.isOpenInvestmentProfitable(investment, currentLevel)) {
				profitableImageView.setVisibility(View.VISIBLE);
			} else {
				profitableImageView.setVisibility(View.GONE);
			}

		} else {
			liveDotView.setVisibility(View.GONE);
			getQuoteLayout.setVisibility(View.GONE);
			timeInvestedView.setText(settledOptionTimeFormat.format(investment.getTimeCreated()));

			setUpSettledOptPutCallIndicator(investment, putCallIndicatorView);
			if (InvestmentUtil.isSettledInvestmentProfitable(investment, currency)) {
				profitableImageView.setVisibility(View.VISIBLE);
			} else {
				profitableImageView.setVisibility(View.GONE);
			}

			profitLayout.setVisibility(View.VISIBLE);
			// Hide get quote button layout
			childView.findViewById(R.id.asset_option_get_quote_layout).setVisibility(View.GONE);

			if (investment.getAdditionalInfoType() == Investment.INVESTMENT_ADDITIONAL_INFO_RU) {
				// ROLL FORWARD
				profitView.setText(childView.getResources().getString(R.string.cancelled));
			}
		}

		return childView;
	}

	/**
	 * Sets up the arrow for call/put for an investment.
	 */
	protected void setUpPutCallIndicator(Investment investment, ImageView indicatorImageView) {
		if (currentLevel != LEVEL_UNKNOWN) {
			indicatorImageView.setImageDrawable(DrawableUtils.getDrawable(InvestmentUtil.getInvestmentIcon(investment, currentLevel)));
		} else {
			// If the level is unknown, we display the yellow image.
			if (investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL) {
				indicatorImageView.setImageDrawable(DrawableUtils.getDrawable(R.drawable.triangle_small_up_yellow));
			} else if (investment.getTypeId() == Investment.INVESTMENT_TYPE_PUT) {
				indicatorImageView.setImageDrawable(DrawableUtils.getDrawable(R.drawable.triangle_small_down_yellow));
			}
		}
	}

	/**
	 * Sets up the arrow for call/put for settled investments.
	 */
	protected void setUpSettledOptPutCallIndicator(Investment investment, ImageView indicatorImageView) {
		indicatorImageView.setImageDrawable(DrawableUtils.getDrawable(InvestmentUtil.getSettledInvestmentIcon(investment, currency)));
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		if (groupPosition == openOptionsGroupPosition) {
			return openOptions.size();
		} else {
			return settledOptions.size();
		}
	}

	@Override
	public Object getGroup(int groupPosition) {
		if (groupPosition == openOptionsGroupPosition) {
			return openOptions;
		} else {
			return settledOptions;
		}
	}

	@Override
	public int getGroupCount() {
		if (openOptions != null && openOptions.size() > 0 && settledOptions != null && settledOptions.size() > 0) {
			return GROUP_COUNT_OPEN_AND_SETTLED_OPTIONS;
		} else {
			return GROUP_COUNT_OPEN_OR_SETTLED_OPTIONS;
		}
	}

	@Override
	public long getGroupId(int groupPosition) {
		if(groupPosition == openOptionsGroupPosition) {
			return OPEN_OPTIONS_GROUP_ID;
		} else if(groupPosition == settledOptionsGroupPosition) {
			return SETTLED_OPTIONS_GROUP_ID;
		}
		
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		View groupView = null;
		if (convertView != null) {
			groupView = convertView;
		} else {
			groupView = inflater.inflate(R.layout.asset_option_group_row, parent, false);
		}
		
		View profitView = groupView.findViewById(R.id.asset_options_group_profit_layout);

		TextView groupNameView = (TextView) groupView.findViewById(R.id.asset_options_group_text);
		if (groupPosition == openOptionsGroupPosition) {
			profitView.setVisibility(View.GONE);
			groupNameView.setText(groupView.getResources().getString(R.string.openOptions));
		} else {
			groupNameView.setText(groupView.getResources().getString(
					R.string.settledOptions));
			profitView.setVisibility(View.VISIBLE);
		}

		return groupView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		if (getGroupId(groupPosition) == OPEN_OPTIONS_GROUP_ID) {
			return true;
		}

		return false;
	}

	public ArrayList<Investment> getOpenOptions() {
		return openOptions;
	}

	public void setOpenOptions(ArrayList<Investment> openOptions) {
		this.openOptions = openOptions;
		setGroupPositions();
		notifyDataSetChanged();
	}

	public ArrayList<Investment> getSettledOptions() {
		return settledOptions;
	}

	public void setSettledOptions(ArrayList<Investment> settledOptions) {
		this.settledOptions = settledOptions;
		setGroupPositions();
		notifyDataSetChanged();
	}

	public Market getMarket() {
		return market;
	}

	public void setMarket(Market market) {
		this.market = market;
	}

	public double getCurrentLevel() {
		return currentLevel;
	}

	public void setCurrentLevel(double currentLevel) {
		this.currentLevel = currentLevel;
	}
	
	public int getOpportunityState() {
		return opportunityState;
	}

	public void setOpportunityState(int opportunityState) {
		this.opportunityState = opportunityState;
	}
	
	public void addInvestment(Investment investment) {
		for (int i = 0; i < openOptions.size(); i++) {
			if (openOptions.get(i).getId() == investment.getId()) {
				// Remove the investment in case it is added before.
				openOptions.remove(i);
				break;
			}
		}

		openOptions.add(0, investment);
		setOpenOptions(openOptions);
	}
	
	public void removeInvestment(Investment investment) {
		Investment investmentToBeRemoved = null;
		for (int i = 0; i < openOptions.size(); i++) {
			if (openOptions.get(i).getId() == investment.getId()) {
				investmentToBeRemoved = openOptions.get(i);
				break;
			}
		}

		if(investmentToBeRemoved != null){
			openOptions.remove(investmentToBeRemoved);
		}

		setOpenOptions(openOptions);
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public int getDecimalPointDigits() {
		return decimalPointDigits;
	}

	public void setDecimalPointDigits(int decimalPointDigits) {
		this.decimalPointDigits = decimalPointDigits;
	}

	@Override
	public void onClick(View v) {
		if (isOptPlusDialogShown) {
			return;
		}

		if(v.getTag() != null){
			int childPosition = (int) v.getTag();
			isOptPlusDialogShown = true;

			OptionPlusQuoteDialogFragment dialog = OptionPlusQuoteDialogFragment.newInstance(openOptions.get(childPosition), decimalPointDigits, currentLevel);
			dialog.setOnDismissListener(new OnDialogDismissListener() {

				@Override
				public void onDismiss() {
					isOptPlusDialogShown = false;
				}
			});

			fragmentManager.showDialogFragment(dialog);
		}

	}
}
