package com.anyoption.android.app.popup;

import com.anyoption.android.R;
import com.anyoption.android.app.manager.PopUpManager.OnConfirmPopUpButtonPressedListener;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class WaiveBonusConfirmDialogFragment extends BaseDialogFragment {

	public static final String TAG = WaiveBonusConfirmDialogFragment.class.getSimpleName();

	private OnConfirmPopUpButtonPressedListener onConfirmPopUpButtonPressedListener;

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static WaiveBonusConfirmDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new WaiveBonusAlertDialogFragment.");
		WaiveBonusConfirmDialogFragment fragment = new WaiveBonusConfirmDialogFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	/**
	 * Set up content.
	 * 
	 * @param contentView
	 */
	@Override
	protected void onCreateContentView(View contentView) {
		View noButton = contentView.findViewById(R.id.waive_bonus_confirm_no_button);
		View yesButton = contentView.findViewById(R.id.waive_bonus_confirm_yes_button);
		
		OnClickListener clickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				if(onConfirmPopUpButtonPressedListener != null){
					onConfirmPopUpButtonPressedListener.onNegative();
				}
			}
		};

		noButton.setOnClickListener(clickListener);
		
		yesButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				if(onConfirmPopUpButtonPressedListener != null){
					onConfirmPopUpButtonPressedListener.onPositive();
				}
			}
		});
		
		setCancelable(false);
	}

	
	@Override
	protected int getContentLayout() {
		return R.layout.waive_bonus_confirm_popup_layout;
	}

	public OnConfirmPopUpButtonPressedListener getOnConfirmPopUpButtonPressedListener() {
		return onConfirmPopUpButtonPressedListener;
	}

	public void setOnConfirmPopUpButtonPressedListener(OnConfirmPopUpButtonPressedListener onConfirmPopUpButtonPressedListener) {
		this.onConfirmPopUpButtonPressedListener = onConfirmPopUpButtonPressedListener;
	}


}
