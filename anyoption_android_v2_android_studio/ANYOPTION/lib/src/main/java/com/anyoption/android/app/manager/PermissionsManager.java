package com.anyoption.android.app.manager;

/**
 * Created by veselin.hristozov on 6.6.2016 г..
 */

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.Utils;

import java.util.LinkedHashSet;

/**
 * Created by veselinh on 11.9.2015..
 */
public class PermissionsManager {

    public static final String TAG = PermissionsManager.class.getSimpleName();

    public static final int REQUEST_PERMISSIONS_REQUEST_CODE = 127;
    public static final int REQUEST_DRAW_OVERLAYS_PERMISSION_REQUEST_CODE = 121;

    protected Application application;

    protected LinkedHashSet<OnPermissionsGrantResultListener> onPermissionsGrantResultListeners = new LinkedHashSet<>();


    public PermissionsManager(Application application) {
        this.application = application;
    }

    public void setOnPermissionsGrantResultListener(OnPermissionsGrantResultListener onPermissionsGrantResultListener) {
        this.onPermissionsGrantResultListeners.add(onPermissionsGrantResultListener);
    }

    public void removeOnPermissionsGrantResultListener(OnPermissionsGrantResultListener onPermissionsGrantResultListener) {
        this.onPermissionsGrantResultListeners.remove(onPermissionsGrantResultListener);
    }

    public boolean isOnPermissionsGrantResultListener(Object onPermissionsGrantResultListener) {
        if (onPermissionsGrantResultListener instanceof OnPermissionsGrantResultListener && this.onPermissionsGrantResultListeners.contains(onPermissionsGrantResultListener)) {
            return true;
        }
        return false;
    }

    @TargetApi(23)
    public boolean hasPermission(String permission) {
        if (permission != null && permission.length() > 0) {
            return ContextCompat.checkSelfPermission(application.getCurrentActivity(),
                    permission)
                    == PackageManager.PERMISSION_GRANTED;
        }
        return false;
    }

    @TargetApi(23)
    public boolean hasPermissions(String[] permissions) {
        if (permissions != null && permissions.length > 0) {
            for (String permission : permissions) {
                if (!hasPermission(permission)) {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }

    @TargetApi(23)
    public void requestPermission(String permission) {
        if (permission != null && permission.length() > 0) {
            if (!hasPermission(permission)) {
                ActivityCompat.requestPermissions(application.getCurrentActivity(),
                        new String[] {permission},
                        REQUEST_PERMISSIONS_REQUEST_CODE);
            }
        }
    }

    @TargetApi(23)
    public void requestPermissions(String[] permissions) {
        if (permissions != null && permissions.length > 0) {
            LinkedHashSet<String> permissionsSet = new LinkedHashSet<>();

            for (String permission : permissions) {
                if (!hasPermission(permission)) {
                    permissionsSet.add(permission);
                }
            }

            if (permissionsSet.size() > 0) {
                String[] filteredPermissions = new String[permissionsSet.size()];
                permissionsSet.toArray(filteredPermissions);

                ActivityCompat.requestPermissions(application.getCurrentActivity(),
                        filteredPermissions,
                        REQUEST_PERMISSIONS_REQUEST_CODE);
            }
        }
    }

    @TargetApi(23)
    public void onPermissionsResult(String[] permissions, int[] grantResults) {
        if (onPermissionsGrantResultListeners != null && onPermissionsGrantResultListeners.size() > 0) {
            for (OnPermissionsGrantResultListener onPermissionsGrantResultListener : onPermissionsGrantResultListeners) {
                onPermissionsGrantResultListener.onPermissionsGrantResult(permissions, grantResults);
            }
        }
    }

    @TargetApi(23)
    public boolean canDrawOverlays() {
        if (!Settings.canDrawOverlays(application)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + application.getPackageName()));
            return false;
        }
        return true;
    }

    @TargetApi(23)
    public void askForDrawOverlaysPermission() {
        if (!Settings.canDrawOverlays(application)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + application.getPackageName()));
            application.getCurrentActivity().startActivityForResult(intent, REQUEST_DRAW_OVERLAYS_PERMISSION_REQUEST_CODE);
        }
    }

}
