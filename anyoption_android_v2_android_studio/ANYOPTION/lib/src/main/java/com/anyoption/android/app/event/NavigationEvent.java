package com.anyoption.android.app.event;

import com.anyoption.android.app.Application.Screenable;

import android.os.Bundle;
import android.util.Log;

/**
 * Event that occurs when the user navigates from one screen to another.
 * Every Application components that get affected should register for this type of event.
 * @author Anastas Arnaudov
 *
 */
public class NavigationEvent {

	public static final String TAG  = NavigationEvent.class.getSimpleName();

	/**
	 * The type of the navigation.
	 * See {@link NavigationType}
	 */
	private NavigationType navigationType;
	
	/**
	 * The type of Navigation. Whether the user navigates deep into the application structure, goes back or navigates within the same level.
	 * @author Anastas Arnaudov
	 *
	 */
	public enum NavigationType{
		
		/**
		 * Initial navigation. Entry point or start of a new back stack. 
		 */
		INIT,
		
		/**
		 * We stay on the same level of the application navigation tree.
		 */
		HORIZONTAL,
		
		/**
		 * We go deep in the application navigation tree.
		 */
		DEEP,
		
		/**
		 * We go up in the application navigation tree (same as back).
		 */
		UP;
	}

	/**
	 * The Screenable the Navigation flow should take the user to.
	 */
	private Screenable toScreen;
	
	/**
	 * The id of the view to which the Screenable will be displayed.
	 */
	private int parentViewID;
	
	/**
	 * Bundle to store arguments.
	 */
	private Bundle bundle;
	
	/**
	 * Additional details for the Screen.
	 */
	private int[] flags;
	
	private boolean isToNewActivity;
	
	/**
	 * {@link NavigationEvent}
	 * @param screen The {@link Screenable} we want to navigate to.
	 * @param navigationType The {@link NavigationType}.
	 * @param paretnViewID The view ID of the view in which the Screenable will be displayed.
	 * @param bundle Bundle with arguments.
	 */
	public NavigationEvent(Screenable screen, NavigationType navigationType, int paretnViewID, boolean isToNewActivity, Bundle bundle, int... flags){
		Log.d(TAG, "New NavigationEvent");
		setToScreen(screen);
		setNavigationType(navigationType);
		setParentViewID(paretnViewID);
		setToNewActivity(isToNewActivity);
		setBundle( (bundle == null) ? new Bundle() : bundle );
		setFlags(flags);
	}
	
	/**
	 * {@link NavigationEvent}
	 * @param screen The {@link Screenable} we want to navigate to.
	 * @param navigationType The {@link NavigationType}.
	 * @param bundle Bundle with arguments.
	 */
	public NavigationEvent(Screenable screen, NavigationType navigationType, Bundle bundle){
		this(screen, navigationType, 0, false, bundle);
	}

	/**
	 * {@link NavigationEvent}
	 * @param screen The {@link Screenable} we want to navigate to.
	 * @param navigationType The {@link NavigationType}.
	 */
	public NavigationEvent(Screenable screen, NavigationType navigationType){
		this(screen, navigationType, 0, false,  new Bundle());
	}
	
	/**
	 * {@link NavigationEvent}
	 * @param screen The {@link Screenable} we want to navigate to.
	 * @param navigationType The {@link NavigationType}.
	 * @param flags Additional instructions.
	 */
	public NavigationEvent(Screenable screen, NavigationType navigationType, int... flags){
		this(screen, navigationType, 0, false, new Bundle(), flags);
	}
	
	/**
	 * {@link NavigationEvent}
	 * @param screen The {@link Screenable} we want to navigate to.
	 * @param navigationType The {@link NavigationType}.
	 * @param paretnViewID The view ID of the view in which the Screenable will be displayed.
	 */
	public NavigationEvent(Screenable screen, NavigationType navigationType, int parentViewID){
		this(screen, navigationType, 0,false,  new Bundle());
	}
	
	/**
	 * Gets the Screenable the Navigation flow should take the user to.
	 * @return
	 */
	public Screenable getToScreen() {
		return toScreen;
	}

	/**
	 * Sets the Screenable the Navigation flow should take the user to.
	 */
	public NavigationEvent setToScreen(Screenable screen) {
		this.toScreen = screen;
		return this;
	}


	/**
	 * Returns the Bundle with arguments.
	 * @return
	 */
	public Bundle getBundle() {
		return bundle;
	}

	/**
	 * Sets a Bundle with arguments.
	 * @param bundle
	 */
	public NavigationEvent setBundle(Bundle bundle) {
		this.bundle = bundle;
		return this;
	}

	/**
	 * Returns the {@link NavigationType}.
	 * @return
	 */
	public NavigationType getNavigationType() {
		return navigationType;
	}

	/**
	 * Sets the {@link NavigationType}.
	 * @return
	 */
	public NavigationEvent setNavigationType(NavigationType navigationType) {
		this.navigationType = navigationType;
		return this;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Navigation Type : " + getNavigationType())
				.append("\n")
				.append("Screenable : " + getToScreen())
				.append("\n")
				.append("Arguments : ")
				.append("\n");
		
		if(bundle != null){
			for(String key : bundle.keySet()){
				builder.append(key + " = " + bundle.get(key).toString())
						.append("\n");
			}			
		}
		return builder.toString();
	}

	public int getParentViewID() {
		return parentViewID;
	}

	public NavigationEvent setParentViewID(int parentViewID) {
		this.parentViewID = parentViewID;
		return this;
	}

	public int[] getFlags() {
		return flags;
	}

	public NavigationEvent setFlags(int... flags) {
		this.flags = flags;
		return this;
	}

	public boolean isToNewActivity() {
		return isToNewActivity;
	}

	public NavigationEvent setToNewActivity(boolean isToNewActivity) {
		this.isToNewActivity = isToNewActivity;
		return this;
	}
	
}
