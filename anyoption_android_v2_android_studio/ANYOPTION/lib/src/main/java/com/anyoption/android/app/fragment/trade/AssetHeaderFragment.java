package com.anyoption.android.app.fragment.trade;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.anyoption.android.R;
import com.anyoption.android.app.event.AssetMarketInfoEvent;
import com.anyoption.android.app.event.ChartCommonEvent;
import com.anyoption.android.app.event.ChartEvent;
import com.anyoption.android.app.event.LightstreamerRegisterEvent;
import com.anyoption.android.app.event.LightstreamerUpdateEvent;
import com.anyoption.android.app.event.LongTermTradingTimeChosenEvent;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.anyoption.android.app.popup.LongTermTradingDialogFragment;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.MarketsUtils;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.TextView;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.MarketAssetIndex;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.service.results.ChartDataResult;
import com.anyoption.json.results.ChartDataMethodResult;
import com.lightstreamer.ls_client.UpdateInfo;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TreeMap;

/**
 * Asset Header
 *
 * @author Mario Kutlev
 */
public class AssetHeaderFragment extends BaseAssetNestedFragment implements Callback {

    public static final String TAG = AssetHeaderFragment.class.getSimpleName();

    private static final long SECOND_MS = 1000;
    private static final String HOUR_FORMAT = "H:mm";
    private static final long LAST_TEN_MINUTES_TO_INVEST_LENGTH_MS = 10 * 60 * 1000;
    private static final long LAST_FIVE_MINUTES_TO_INVEST_LENGTH_MS = 5 * 60 * 1000;

    private static final int MESSAGE_SHOW_UNAVAILABLE_INFO      = 1;
    private static final int MESSAGE_SHOW_OPEN_INFO             = 2;
    private static final int MESSAGE_SHOW_LAST_10_MIN           = 3;
    private static final int MESSAGE_SHOW_CLOSING               = 4;
    private static final int MESSAGE_SHOW_CLOSED                = 5;
    private static final int MESSAGE_SCHEDULE_HOURLY            = 6;
    private static final int MESSAGE_SCHEDULE_LONG_TERM         = 7;

    private View root;
    private View availableInfoLayout;
    private TextView expiredView;
    private TextView titleView;
    private TextView expiryView;
    private TextView timeLeftView;
    private TextView textMinToInvestView;
    private TextView waitingForExpiryView;
    private TextView titleUnavailableView;
    private TextView availableAtView;

    private long opportunityId;
    private int opportunityState;
    private Date timeLastInvest;
    private Date timeEstClosing;

    private CountDownTimer timeLeftTimer;
    private SimpleDateFormat timeClosingFormat;

    private String availableAtText;

    private int timeLeftTextColor;
    private int timeLeftLastMinsTextColor;

    private Handler lsHandler;
    private boolean isDynamics;
    private boolean isBinary;
    private String centerLevelText;

    private String[] nextOpportunitiesTimes = new String[7];
    private String[] nextOpportunitiesFixedTimes = new String[7];
    private long[] nextOpportunitiesIds = new long[7];

    private LongTermTradingTimeChosenEvent.ChosenTime longTermTradingChosenTime = LongTermTradingTimeChosenEvent.ChosenTime.NEAREST;
    private MarketAssetIndex marketAssetIndex;

    /**
     * Use this method to create new instance of the fragment.
     *
     * @return
     */
    public static AssetHeaderFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new AssetHeaderFragment.");
        AssetHeaderFragment fragment = new AssetHeaderFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public static AssetHeaderFragment newInstance(Market market) {
        Bundle args = new Bundle();
        args.putSerializable(Constants.EXTRA_MARKET, market);
        return newInstance(args);
    }

    /**
     * Use {@link #newInstance()} method instead this constructor.
     */
    public AssetHeaderFragment() {
        // Empty constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        application.registerForEvents(this, AssetMarketInfoEvent.class);

        setMarketType();

        lsHandler = new Handler(this);
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.asset_header_fragment, container, false);
        titleView = (TextView) root.findViewById(R.id.asset_header_title);
        availableInfoLayout = root.findViewById(R.id.asset_header_available_info);
        expiryView = (TextView) root.findViewById(R.id.asset_header_expiry);
        timeLeftView = (TextView) root.findViewById(R.id.asset_header_time_left_text_view);
        textMinToInvestView = (TextView) root.findViewById(R.id.asset_header_min_to_inv_text);
        waitingForExpiryView = (TextView) root.findViewById(R.id.asset_header_waiting_for_expiry);
        titleUnavailableView = (TextView) root.findViewById(R.id.asset_header_title_unavailable);
        availableAtView = (TextView) root.findViewById(R.id.asset_header_available_at);
        expiredView = (TextView) root.findViewById(R.id.asset_header_expired);

        updateTitle();

        timeClosingFormat = new SimpleDateFormat(HOUR_FORMAT);

        timeLeftTextColor = ColorUtils.getColor(R.color.asset_header_text);
        timeLeftLastMinsTextColor = ColorUtils.getColor(R.color.asset_header_time_left_last_min_text);

        setupUI();

        reset();

        return root;
    }

    public void setMarketType() {
        if (market != null && MarketsUtils.isDynamics(market)) {
            isDynamics = true;
        }
        if (market != null && MarketsUtils.isBinary(market)) {
            isBinary = true;
        }
    }

    public void setupUI() {
        Log.d(TAG, "setupUI");

        setMarketType();

        if(root != null) {
            if (isBinary) {
                root.findViewById(R.id.header_expiry_space).setVisibility(View.VISIBLE);
                expiryView.setBackgroundResource(R.drawable.long_term_trading_asset_button);
                expiryView.setGravity(Gravity.CENTER);
                int topLeftRightPadding = (int) ScreenUtils.convertPxToDp(20, application);
                int bottomPadding = (int) ScreenUtils.convertPxToDp(14, application);
                availableInfoLayout.setPadding(topLeftRightPadding, topLeftRightPadding, topLeftRightPadding, bottomPadding);
                expiryView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle bundle = new Bundle();

                        TreeMap<Integer, String> longTermTimeItems = new TreeMap<Integer, String>();
                        TreeMap<Integer, Long> longTermIdsItems = new TreeMap<Integer, Long>();
                        longTermTimeItems.put(LongTermTradingTimeChosenEvent.ChosenTime.NEAREST.ordinal(), getString(R.string.long_term_trading_popup_item_nearest));

                        if (nextOpportunitiesTimes[0] != null && nextOpportunitiesTimes[0].length() > 0 && nextOpportunitiesIds[0] > 0L && nextOpportunitiesFixedTimes[0].length() > 0) {
                            longTermTimeItems.put(LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_1.ordinal(), nextOpportunitiesFixedTimes[0]);
                            longTermIdsItems.put(LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_1.ordinal(), nextOpportunitiesIds[0]);
                        }

                        if (nextOpportunitiesTimes[1] != null && nextOpportunitiesTimes[1].length() > 0 && nextOpportunitiesIds[1] > 0L && nextOpportunitiesFixedTimes[1].length() > 0) {
                            longTermTimeItems.put(LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_2.ordinal(), nextOpportunitiesFixedTimes[1]);
                            longTermIdsItems.put(LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_2.ordinal(), nextOpportunitiesIds[1]);
                        }

                        if (nextOpportunitiesTimes[2] != null && nextOpportunitiesTimes[2].length() > 0 && nextOpportunitiesIds[2] > 0L && nextOpportunitiesFixedTimes[2].length() > 0) {
                            longTermTimeItems.put(LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_3.ordinal(), nextOpportunitiesFixedTimes[2]);
                            longTermIdsItems.put(LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_3.ordinal(), nextOpportunitiesIds[2]);
                        }

                        if (market != null && marketAssetIndex != null) {
                            Market assetMarketsIndexesMarket = marketAssetIndex.getMarket();
                            if (assetMarketsIndexesMarket != null) {
                                if (assetMarketsIndexesMarket.isHaveDaily()) {
                                    longTermTimeItems.put(LongTermTradingTimeChosenEvent.ChosenTime.DAILY.ordinal(), getString(R.string.long_term_trading_popup_item_end_of_day));
                                }
                                if (assetMarketsIndexesMarket.isHaveWeekly()) {
                                    longTermTimeItems.put(LongTermTradingTimeChosenEvent.ChosenTime.WEEKLY.ordinal(), getString(R.string.long_term_trading_popup_item_end_of_week));
                                }
                                if (assetMarketsIndexesMarket.isHaveMonthly()) {
                                    longTermTimeItems.put(LongTermTradingTimeChosenEvent.ChosenTime.MONTHLY.ordinal(), getString(R.string.long_term_trading_popup_item_end_of_month));
                                }
                            }
                        }

                        bundle.putLong(Constants.EXTRA_MARKET_ID, market.getId());
                        bundle.putSerializable(Constants.EXTRA_LONG_TERM_TRADING_TIMES, longTermTimeItems);
                        bundle.putSerializable(Constants.EXTRA_LONG_TERM_TRADING_IDS, longTermIdsItems);
                        application.getFragmentManager().showDialogFragment(LongTermTradingDialogFragment.class, bundle);
                    }
                });
            } else {
                availableInfoLayout.setPadding(0, 0, 0, 0);
                expiryView.setGravity(Gravity.CENTER_VERTICAL|Gravity.RIGHT);
                root.findViewById(R.id.header_expiry_space).setVisibility(View.GONE);
                expiryView.setBackgroundResource(R.color.transparent);
                expiryView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                if (isDynamics) {
                    setupDynamicsTextStyle();
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (market != null) {
            registerForEvents();
        }
    }

    @Override
    public void onPause() {
        stopTimeLeftTimer();
        application.unregisterForAllEvents(this);
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        stopTimeLeftTimer();
        application.unregisterForAllEvents(this);
        super.onDestroyView();
    }

    private void formatCenterLevel(double centerLevel, int decimalPointDigits) {
        DecimalFormat levelFormat = Utils.getDecimalFormat(decimalPointDigits);
        levelFormat.applyPattern(Utils.getDecimalFormatPattern(decimalPointDigits));
        centerLevelText = levelFormat.format(centerLevel);
    }

    /**
     * Setup the text for the Dynamics(0-100) Markets.
     */
    private void setupDynamicsTextStyle() {
        Log.d(TAG, "setupDynamicsTextStyle");
        Context context = application;

        float titleTextSize = ScreenUtils.convertDpToPx(context, 17);
        float subtitleTextSize = ScreenUtils.convertDpToPx(context, 11);
        FontsUtils.Font fontNormal = FontsUtils.Font.ROBOTO_LIGHT;
        FontsUtils.Font fontLight = FontsUtils.Font.ROBOTO_REGULAR;

        titleView.setTextSize(TypedValue.COMPLEX_UNIT_PX, titleTextSize);
        titleView.setFont(fontNormal);

        expiryView.setTextSize(TypedValue.COMPLEX_UNIT_PX, subtitleTextSize);
        expiryView.setFont(fontLight);

        timeLeftView.setTextSize(TypedValue.COMPLEX_UNIT_PX, subtitleTextSize);
        timeLeftView.setFont(fontLight);

        textMinToInvestView.setTextSize(TypedValue.COMPLEX_UNIT_PX, subtitleTextSize);
        textMinToInvestView.setFont(fontLight);

        waitingForExpiryView.setText(getString(R.string.waitingForExpiryDynamics));
        waitingForExpiryView.setTextSize(TypedValue.COMPLEX_UNIT_PX, subtitleTextSize);
        waitingForExpiryView.setFont(fontLight);

        titleUnavailableView.setTextSize(TypedValue.COMPLEX_UNIT_PX, titleTextSize);
        titleUnavailableView.setFont(fontLight);

        availableAtView.setTextSize(TypedValue.COMPLEX_UNIT_PX, subtitleTextSize);
        availableAtView.setFont(fontLight);

        expiredView.setTextSize(TypedValue.COMPLEX_UNIT_PX, subtitleTextSize);
        expiredView.setFont(fontLight);
    }

    private void registerForEvents() {
        Log.d(TAG, "registerForEvents");
        application.registerForEvents(this, LightstreamerRegisterEvent.class, LightstreamerUpdateEvent.class, ChartEvent.class, ChartCommonEvent.class, LongTermTradingTimeChosenEvent.class);
    }

    ///////////////////////////////////////////////////////////////////////////
    //////////					EVENTs							///////////////
    ///////////////////////////////////////////////////////////////////////////

    //For Dynamics Markets:
    public void onEventMainThread(ChartCommonEvent event) {
        Log.d(TAG, "ChartCommonEvent");
        if (event.getMarketId() != market.getId()) {
            Log.d(TAG, "ChartCommonEvent NOT for the current Market.");
            return;
        }

        if (event.getType() == ChartCommonEvent.Type.DATA_REQUEST_SUCCESS) {
            Log.d(TAG, "ChartCommonEvent Type = SUCCESS.");
            ChartDataResult data = event.getChartData();
            Log.d(TAG, "Chart Data: \n opportunityId = " + data.getOpportunityId() + "\n Time Closing = " + data.getTimeEstClosing());

            opportunityId = data.getOpportunityId();
            double centerLevel = data.getEventLevel();
            int decimalPointDigits = (int) (long) market.getDecimalPoint();
            formatCenterLevel(centerLevel, decimalPointDigits);

            timeLastInvest = data.getTimeLastInvest();
            timeEstClosing = data.getTimeEstClosing();
            opportunityState = 0;
            if (timeEstClosing != null) {
                expiryView.setText(getString(R.string.atTimeToday, timeClosingFormat.format(timeEstClosing)).trim() + (!isBinary ? " | " : ""));
            }

            updateTitle();
        } else {
            Log.d(TAG, "ChartCommonEvent Type = " + event.getType());
        }
    }

    public void onEventMainThread(ChartEvent event) {
        Log.d(TAG, "ChartEvent");
        if (event.getMarketId() != market.getId()) {
            return;
        }

        if (event.getType() == ChartEvent.Type.DATA_REQUEST_SUCCESS) {
            ChartDataMethodResult data = event.getChartData();

            timeLastInvest = data.getTimeLastInvest();
            timeEstClosing = data.getTimeEstClosing();
            opportunityState = 0;
            if (timeEstClosing != null) {
                expiryView.setText(getString(R.string.byTimeToday, timeClosingFormat.format(timeEstClosing)).trim() + (!isBinary ? " | " : ""));
            }

            updateTitle();
        }
    }

    public void onEventMainThread(LongTermTradingTimeChosenEvent event) {
        if(market.getId() != event.getMarketId()){
            return;
        }
        Log.d(TAG, "LongTermTradingTimeChosenEvent");
        if(longTermTradingChosenTime != event.getTime()) {
            longTermTradingChosenTime = event.getTime();

            if (longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.NEAREST) {
                if (timeEstClosing != null) {
                    expiryView.setText(getString(R.string.byTimeToday, timeClosingFormat.format(timeEstClosing)));
                }
                lsHandler.obtainMessage(MESSAGE_SCHEDULE_HOURLY).sendToTarget();
            } else if (longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_1) {
                expiryView.setText(getString(R.string.byTimeToday, nextOpportunitiesFixedTimes[1]));
                lsHandler.obtainMessage(MESSAGE_SCHEDULE_HOURLY).sendToTarget();
            } else if (longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_2) {
                expiryView.setText(getString(R.string.byTimeToday, nextOpportunitiesFixedTimes[2]));
                lsHandler.obtainMessage(MESSAGE_SCHEDULE_HOURLY).sendToTarget();
            } else if (longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_3) {
                expiryView.setText(getString(R.string.byTimeToday, nextOpportunitiesFixedTimes[3]));
                lsHandler.obtainMessage(MESSAGE_SCHEDULE_HOURLY).sendToTarget();
            } else if (longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.DAILY) {
                expiryView.setText(getString(R.string.long_term_trading_popup_item_end_of_day));
                lsHandler.obtainMessage(MESSAGE_SCHEDULE_LONG_TERM).sendToTarget();
            } else if (longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.WEEKLY) {
                expiryView.setText(getString(R.string.long_term_trading_popup_item_end_of_week));
                lsHandler.obtainMessage(MESSAGE_SCHEDULE_LONG_TERM).sendToTarget();
            } else if (longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.MONTHLY) {
                expiryView.setText(getString(R.string.long_term_trading_popup_item_end_of_month));
                lsHandler.obtainMessage(MESSAGE_SCHEDULE_LONG_TERM).sendToTarget();

            }
        }
    }

    public void onEvent(LightstreamerRegisterEvent event) {
        Log.d(TAG, "LightstreamerRegisterEvent");
        opportunityId = 0L;
        opportunityState = 0;
    }

    public void onEvent(LightstreamerUpdateEvent event) {
        if (event.getMarketId() == market.getId()) {
            updateItem(event.getUpdateInfo());
        }
    }

    public void onEventMainThread(AssetMarketInfoEvent event) {
        marketAssetIndex = event.getMarketAssetIndex();
        setupUI();
    }

    ///////////////////////////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////////////////////////
    //////////					LIGHSTREAMER					///////////////
    ///////////////////////////////////////////////////////////////////////////

    public void updateItem(UpdateInfo update) {
        if(		longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.DAILY ||
                longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.WEEKLY ||
                longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.MONTHLY){
            lsHandler.obtainMessage(MESSAGE_SCHEDULE_LONG_TERM).sendToTarget();
            return;
        }

        //Check if the State is Closed. If so do not update and wait for a new ChartEvent to reset the New Opportunity.
        if (opportunityState == Opportunity.STATE_CLOSED) {
            return;
        }

        boolean isStateChanged;
        int newOpportunityState;
        String nextOpportunitiesInfo;

        if (isDynamics) {
            isStateChanged = update.isValueChanged(LightstreamerManager.COLUMN_DYNAMICS_STATE);
            newOpportunityState = Integer.parseInt(update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_STATE));
        } else {
            isStateChanged = update.isValueChanged(LightstreamerManager.COLUMN_ET_STATE);
            newOpportunityState = Integer.parseInt(update.getNewValue(LightstreamerManager.COLUMN_ET_STATE));
            nextOpportunitiesInfo = update.getNewValue(LightstreamerManager.COLUMN_AO_NH);

            if (nextOpportunitiesInfo != null) {

                String[] nextOpportunitiesArray = nextOpportunitiesInfo.split(";");
                try {
                    for(int currentHourlyOpportunityNumber = 0; currentHourlyOpportunityNumber < nextOpportunitiesArray.length; currentHourlyOpportunityNumber++) {
                        String[] nextOpportunityInfoArray = nextOpportunitiesArray[currentHourlyOpportunityNumber].split("\\|");

                        nextOpportunitiesTimes[currentHourlyOpportunityNumber] = nextOpportunityInfoArray[1];
                        nextOpportunitiesIds[currentHourlyOpportunityNumber] = Long.valueOf(nextOpportunityInfoArray[0]);
                        Date date = TimeUtils.parseTimeFromLSUpdate(nextOpportunitiesTimes[currentHourlyOpportunityNumber]);
                        nextOpportunitiesFixedTimes[currentHourlyOpportunityNumber] = TimeUtils.formatTimeToHoursMinutes(date.getTime());
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Could not parse COLUMN_AO_NH info");
                }
            }
        }

        if (opportunityState == 0 || isStateChanged) {
            Log.d(TAG, "LS update - State changed. New State=" + LightstreamerManager.getOpportunityStateDescription(newOpportunityState));
            opportunityState = newOpportunityState;

            Message message = null;
            if (opportunityState == Opportunity.STATE_CREATED || opportunityState == Opportunity.STATE_SUSPENDED) {

                String timeFormated = null;
                String timeEstClose = null;
                if (isDynamics) {
                    timeEstClose = update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_TIME_CLOSE);
                } else {
                    timeEstClose = update.getNewValue(LightstreamerManager.COLUMN_ET_EST_CLOSE);
                }
                if ((opportunityState == Opportunity.STATE_CREATED || opportunityState == Opportunity.STATE_SUSPENDED) && timeEstClose != null) {
                    try {
                        timeFormated = TimeUtils.reformatTimeFromLSUpdate(application.getCurrentActivity(), timeEstClose);
                    } catch (ParseException e) {
                        Log.e(TAG, "Could not parse ET_EST_CLOSE time");
                    }

                    availableAtText = getString(R.string.optionAvailableAt) + ": " + timeFormated;
                } else {
                    availableAtText = getString(R.string.availableNot);
                }

                message = lsHandler.obtainMessage(MESSAGE_SHOW_UNAVAILABLE_INFO);
            } else if (opportunityState == Opportunity.STATE_OPENED) {
                message = lsHandler.obtainMessage(MESSAGE_SHOW_OPEN_INFO);
            } else if (opportunityState == Opportunity.STATE_LAST_10_MIN) {
                message = lsHandler.obtainMessage(MESSAGE_SHOW_LAST_10_MIN);
            } else if (opportunityState == Opportunity.STATE_CLOSING_1_MIN || opportunityState == Opportunity.STATE_CLOSING) {
                message = lsHandler.obtainMessage(MESSAGE_SHOW_CLOSING);
            } else if (opportunityState == Opportunity.STATE_CLOSED) {
                message = lsHandler.obtainMessage(MESSAGE_SHOW_CLOSED);
            }

            if (message != null) {
                message.sendToTarget();
            }
        }
    }
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void loadData() {
        // Do nothing
    }

    @Override
    protected void onMarketChanged() {
        super.onMarketChanged();
        Log.d(TAG, "onMarketChanged");

        if (!application.isRegistered(this)) {
            registerForEvents();
        }

        updateTitle();
        availableInfoLayout.setVisibility(View.GONE);

        reset();
    }

    private void reset() {
        opportunityState = 0;
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case MESSAGE_SHOW_UNAVAILABLE_INFO:
                showUnavailbleInfo();
                break;
            case MESSAGE_SHOW_OPEN_INFO:
                startTimeLeftTimer();
                showOpenOptionInfo();
                break;
            case MESSAGE_SHOW_LAST_10_MIN:
                startTimeLeftTimer();
                showLastTenMinutes();
                break;
            case MESSAGE_SHOW_CLOSING:
                showWaitingForExpiry();
                stopTimeLeftTimer();
                break;
            case MESSAGE_SHOW_CLOSED:
                showClosingInfo();
                break;
            case MESSAGE_SCHEDULE_HOURLY:
                showLongTermTradingInfo(true);
                break;
            case MESSAGE_SCHEDULE_LONG_TERM:
                showLongTermTradingInfo(false);
                break;
        }

        return true;
    }

    private void showOpenOptionInfo() {
        Log.d(TAG, "showOpenOptionInfo");
        titleUnavailableView.setVisibility(View.GONE);
        availableAtView.setVisibility(View.GONE);
        waitingForExpiryView.setVisibility(View.GONE);
        expiredView.setVisibility(View.GONE);

        titleView.setVisibility(View.VISIBLE);
        timeLeftView.setVisibility(View.VISIBLE);
        timeLeftView.setBackgroundResource(0);
        timeLeftView.setTextColor(timeLeftTextColor);

        textMinToInvestView.setVisibility(View.VISIBLE);
        if (timeEstClosing != null) {
            availableInfoLayout.setVisibility(View.VISIBLE);
        } else {
            availableInfoLayout.setVisibility(View.GONE);
        }
    }

    private void showLastTenMinutes() {
        Log.d(TAG, "showLastTenMinutes");
        titleUnavailableView.setVisibility(View.GONE);
        availableAtView.setVisibility(View.GONE);
        waitingForExpiryView.setVisibility(View.GONE);
        expiredView.setVisibility(View.GONE);

        titleView.setVisibility(View.VISIBLE);
        timeLeftView.setVisibility(View.VISIBLE);
        timeLeftView.setBackgroundResource(R.drawable.left_time_backgr);
        timeLeftView.setTextColor(timeLeftLastMinsTextColor);
        textMinToInvestView.setVisibility(View.VISIBLE);

        if (timeEstClosing != null) {
            availableInfoLayout.setVisibility(View.VISIBLE);
        } else {
            availableInfoLayout.setVisibility(View.GONE);
        }
    }

    private void showWaitingForExpiry() {
        if(longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.NEAREST || longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_1 || longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_2 || longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_3) {
            Log.d(TAG, "showWaitingForExpiry");
            titleUnavailableView.setVisibility(View.GONE);
            availableAtView.setVisibility(View.GONE);
            timeLeftView.setVisibility(View.GONE);
            textMinToInvestView.setVisibility(View.GONE);
            expiredView.setVisibility(View.GONE);

            titleView.setVisibility(View.VISIBLE);
            if (timeEstClosing != null) {
                waitingForExpiryView.setVisibility(View.VISIBLE);
                availableInfoLayout.setVisibility(View.VISIBLE);
            } else {
                availableInfoLayout.setVisibility(View.GONE);
            }
        }
    }

    private void showClosingInfo() {
        if (longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.NEAREST || longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_1 || longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_2 || longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_3) {
            Log.d(TAG, "showClosingInfo");
            titleUnavailableView.setVisibility(View.GONE);
            availableAtView.setVisibility(View.GONE);
            waitingForExpiryView.setVisibility(View.GONE);
            timeLeftView.setVisibility(View.GONE);
            textMinToInvestView.setVisibility(View.GONE);

            titleView.setVisibility(View.VISIBLE);
            expiredView.setVisibility(View.VISIBLE);
            availableInfoLayout.setVisibility(View.VISIBLE);
        }
    }

    private void showLongTermTradingInfo(boolean isHourly) {
        if(!isAdded()){
            return;
        }
        if (isHourly) {
            timeLeftView.setVisibility(View.VISIBLE);
            textMinToInvestView.setVisibility(View.VISIBLE);
            titleView.setTextColor(ColorUtils.getColor(R.color.long_term_trading_asset_header_title_default_color));
        } else {
            timeLeftView.setVisibility(View.INVISIBLE);
            textMinToInvestView.setVisibility(View.INVISIBLE);
            titleView.setTextColor(ColorUtils.getColor(R.color.long_term_trading_asset_header_title_color));
        }
        titleView.setVisibility(View.VISIBLE);
        availableInfoLayout.setVisibility(View.VISIBLE);
        waitingForExpiryView.setVisibility(View.GONE);
        titleUnavailableView.setVisibility(View.GONE);
        availableAtView.setVisibility(View.GONE);
        expiredView.setVisibility(View.GONE);
    }

    private void showUnavailbleInfo() {
        Log.d(TAG, "showUnavailbleInfo");
        availableAtView.setText(availableAtText);

        titleView.setVisibility(View.GONE);
        availableInfoLayout.setVisibility(View.GONE);
        expiredView.setVisibility(View.GONE);

        titleUnavailableView.setVisibility(View.VISIBLE);
        availableAtView.setVisibility(View.VISIBLE);
    }

    private void startTimeLeftTimer() {
        if (timeLeftTimer != null) {
            timeLeftTimer.cancel();
        }

        if (timeLastInvest == null) {
            return;
        }

        long millisInFuture = timeLastInvest.getTime() - TimeUtils.getCurrentServerTime();

        timeLeftTimer = new CountDownTimer(millisInFuture, SECOND_MS) {

            private boolean isLastMinutes;

            @Override
            public void onTick(long millisUntilFinished) {
                //Check the Last 10 min invest time:
                if (millisUntilFinished <= ((isDynamics) ? LAST_FIVE_MINUTES_TO_INVEST_LENGTH_MS : LAST_TEN_MINUTES_TO_INVEST_LENGTH_MS)) {
                    if (!isLastMinutes) {
                        isLastMinutes = true;
                        showLastTenMinutes();
                    }
                }
                timeLeftView.setText(TimeUtils.formatTimeToMinutesAndSeconds(millisUntilFinished));
            }

            @Override
            public void onFinish() {
                timeLeftView.setText(TimeUtils.formatTimeToMinutesAndSeconds(0));
                showWaitingForExpiry();
            }
        };

        timeLeftTimer.start();
    }

    private void stopTimeLeftTimer() {
        if (timeLeftTimer != null) {
            timeLeftTimer.cancel();
        }
    }

    private void updateTitle() {
        Log.d(TAG, "updateTitle");
        if (isDynamics) {
            String marketName = market != null ? market.getDisplayName() : "";
            String closeTime = timeEstClosing != null ? timeClosingFormat.format(timeEstClosing) : "";
            String title = getString(R.string.assetHeader, marketName, centerLevelText);
            titleView.setText(title);
            if (centerLevelText != null) {
                String boldDigits = centerLevelText.substring(Math.max(centerLevelText.length() - 3, 1));
                TextUtils.decorateInnerText(titleView, null, titleView.getText().toString(), boldDigits, FontsUtils.Font.ROBOTO_BOLD, 0, 0);
            }
        } else {
            titleView.setText(getString(R.string.willAssetGoUpOrDown, market != null ? market.getDisplayName() : ""));
        }
        setupUI();
    }

}
