package com.anyoption.android.app.manager;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.activity.BubblesActivity;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.fragment.my_account.my_options.MyOptionsFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Market;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class PushNotificationsManager {

    private static final String TAG = PushNotificationsManager.class.getSimpleName();

    protected static final String EXTRA_SCREEN = "screen_dpl";
    protected static final String EXTRA_MARKET = "market_dpl";

    protected Application application;
    protected boolean isTablet;

    protected boolean isAppOpenedWithPush;
    protected boolean isLoginNeeded;
    protected boolean checkDeposit;

    protected Screenable toScreen;
    protected Bundle pushArguments;
    protected Bundle screenArguments;

    public PushNotificationsManager(Application application) {
        this.application = application;
        isTablet = application.getResources().getBoolean(R.bool.isTablet);
        setPushHandled();
    }

    public void handlePush(Bundle arguments) {
        Log.d(TAG, "handlePush");

        isAppOpenedWithPush = true;
        toScreen = null;
        setPushArguments(arguments);
        screenArguments = new Bundle();

        if (arguments.containsKey(EXTRA_SCREEN)) {
            String screenArg = arguments.getString(EXTRA_SCREEN, null);
            toScreen = getScreen(screenArg);
        }

        if (toScreen != null) {
            setUpScreenArguments();
            setUpLoginNeeded();
        } else {
            setPushHandled();
        }

        if ((toScreen == Screen.LOGIN || toScreen == Screen.REGISTER) && application.isLoggedIn()) {
            Log.d(TAG, "Register or Login screen won't be opened. The user is already logged in.");
            setPushHandled();
        }
    }

    public boolean isAppOpenedWithPush() {
        return isAppOpenedWithPush;
    }

    /**
     * Removes the info for the last push notification when it is handled from the last screen which
     * needs some data.
     */
    public void setPushHandled() {
        isAppOpenedWithPush = false;
        isLoginNeeded = true;
        checkDeposit = false;
        toScreen = null;
        pushArguments = null;
        screenArguments = null;
    }

    protected void setUpScreenArguments() {
        if (toScreen == Screen.ASSET_PAGE || toScreen == Screen.ASSET_VIEW_PAGER) {
            if (!isTablet) {
                toScreen = Screen.ASSET_VIEW_PAGER;
            }

            long marketId = 0l;
            if (pushArguments.containsKey(EXTRA_MARKET)) {
                marketId = getMarketId(pushArguments.getString(EXTRA_MARKET, ""));
            }

            if (marketId != 0l) {
                Market market = application.getMarketForID(marketId);
                if (market != null) {
                    screenArguments.putSerializable(Constants.EXTRA_MARKET, market);
                } else {
                    screenArguments.putLong(Constants.EXTRA_MARKET_ID, marketId);
                }
            } else {
                Log.e(TAG, "Cannot open market without marketId");
                setPushHandled();
            }
        } else if (toScreen == Screen.OPEN_OPTIONS) {
            toScreen = Screen.MY_OPTIONS;
            screenArguments.putByte(Constants.EXTRA_MY_OPTIONS_TAB,
                    MyOptionsFragment.TAB_OPEN_OPTIONS);
        } else if (toScreen == Screen.SETTLED_OPTIONS) {
            toScreen = Screen.MY_OPTIONS;
            screenArguments.putByte(Constants.EXTRA_MY_OPTIONS_TAB,
                    MyOptionsFragment.TAB_SETTLED_OPTIONS);
        }

    }

    private void setUpLoginNeeded() {
        if (application.isLoggedIn()) {
            isLoginNeeded = false;
            return;
        }

        isLoginNeeded = true;
        if (getToScreen() == Screen.LOGIN || getToScreen().equals(Screen.REGISTER)) {
            isLoginNeeded = false;
        }
    }

    public Screenable getToScreen() {
        return toScreen;
    }

    protected void setToScreen(Screenable toScreen) {
        this.toScreen = toScreen;
    }

    public Bundle getPushArguments() {
        return pushArguments;
    }

    protected void setPushArguments(Bundle arguments) {
        this.pushArguments = arguments;
    }

    public Bundle getScreenArguments() {
        return screenArguments;
    }

    protected void setScreenArguments(Bundle arguments) {
        this.screenArguments = arguments;
    }

    public boolean isLoginNeeded() {
        return isLoginNeeded;
    }

    public boolean shouldCheckDeposit() {
        return checkDeposit;
    }

    @SuppressWarnings("static-method")
    protected Screenable getScreen(String screenArg) {
        if (screenArg == null) {
            return null;
        }

        return Screen.getScreenByTag(screenArg);
    }

    /**
     * Returns marketId depending on the marketArg.
     *
     * @param marketArg market info in format -> Bin/Optplus + "_" + marketName + "_" + marketId
     * @param marketId
     */
    private static long getMarketId(String marketArg) {
        String[] marketInfo = marketArg.split("_");
        if (marketInfo.length < 3) {
            Log.e(TAG, "Market parameters are not correct: " + marketArg
                    + "\nCan not determine the market.");
            return 0l;
        }

        long marketId = 0l;
        try {
            marketId = Long.parseLong(marketInfo[2]);
        } catch (NumberFormatException ex) {
            Log.e(TAG, "Market id cannot be parsed!");
        }

        return marketId;
    }

    public void onResume(Activity activity) {
        Log.d(TAG, "onResumeMainActivity.");
        Log.d(TAG, "Checking push notifications...");
        Intent intent = activity.getIntent();
        if (intent != null && intent.getExtras() != null) {
            boolean isFromPush = intent.getExtras().getBoolean(Constants.EXTRA_PUSH_NOTIFICATION, false);
            if (isFromPush) {
                Log.d(TAG, "Coming from a Push Notification...");
                intent.removeExtra(Constants.EXTRA_PUSH_NOTIFICATION);
                handlePush(new Bundle(intent.getExtras()));
            }
        }

        if (isAppOpenedWithPush() && getToScreen() != null && application.isLoggedIn()) {
            Log.d(TAG, "App is opened by a push notification.");
            boolean openDeposit = false;
            Screenable toScreen = null;
            Bundle arguments = null;
            if (shouldCheckDeposit()) {
                Log.d(TAG, "Checking Deposit.");
                openDeposit = !(application.getUser().getBalance() > 0);
            }

            if (openDeposit) {
                Log.d(TAG, "Should go to Deposit Screen.");
                toScreen = Screen.DEPOSIT_MENU;
                arguments = new Bundle();
                setPushHandled();
            } else {
                Log.d(TAG, "Don't have to go to Deposit Screen.");
                toScreen = getToScreen();
                arguments = getScreenArguments();
            }

            if (toScreen == Screen.BUBBLES && !openDeposit) {
                application.setCurrentScreen(Application.Screen.BUBBLES);
                Intent bubblesIntent = new Intent(application.getApplicationContext(), application.getSubClassForClass(BubblesActivity.class));
                application.getCurrentActivity().startActivity(bubblesIntent);
            } else {
                application.postEvent(new NavigationEvent(toScreen, NavigationEvent.NavigationType.INIT, arguments));
            }

            setPushHandled();
        } else {
            Log.d(TAG, "No push notifications.");
        }
    }

}
