package com.anyoption.android.app.popup;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.anyoption.android.R;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.CheckBox;
import com.anyoption.android.app.widget.TextView;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.service.requests.CancelInvestmentCheckboxStateMethodRequest;
import com.anyoption.common.service.requests.CancelInvestmentMethodRequest;
import com.anyoption.common.service.results.InvestmentMethodResult;
import com.anyoption.common.service.results.MethodResult;

/**
 * @author Anastas Arnaudov
 */
@SuppressWarnings("unused")
public class CancelInvestmentDialogFragment extends BaseDialogFragment {

    public static final String TAG = CancelInvestmentDialogFragment.class.getSimpleName();

    public interface CancelInvestmentListener{
        void onFinished(boolean isCanceled);
    }

    public static final long TIMER_DEFAULT_LENGTH   = 3000L;
    public static final long TIMER_OFFSET           = 500L;
    public static final long TIMER_TICK_INTERVAL    = 500L;

    private CancelInvestmentListener cancelInvestmentListener;
    private CountDownTimer countDownTimer;
    protected Investment investment;
    private long time;
    private boolean isCancelingInvestment;

    public static CancelInvestmentDialogFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new CancelInvestmentDialogFragment.");
        CancelInvestmentDialogFragment fragment = new CancelInvestmentDialogFragment();
        bundle.putBoolean(BaseDialogFragment.ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getContentLayout() {
        return R.layout.popup_cancel_investment;
    }

    public void setCancelInvestmentListener(CancelInvestmentListener cancelInvestmentListener){
        this.cancelInvestmentListener = cancelInvestmentListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);

        Bundle bundle = getArguments();
        if(bundle != null){
            investment      = (Investment) bundle.getSerializable(Constants.EXTRA_INVESTMENT);
            long dev2CheckTime   = bundle.getLong(Constants.EXTRA_DEV2_SECONDS, 0);
            long cancelTime      = bundle.getLong(Constants.EXTRA_CANCEL_SECONDS, TIMER_DEFAULT_LENGTH);

            time = Math.max(cancelTime, dev2CheckTime);

        }else {
            Log.d(TAG, "No params! Closing!");
            dismiss();
        }
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("StringFormatMatches")
    @Override
    protected void onCreateContentView(View contentView) {
        ImageView investmentIconImageView   = (ImageView)   contentView.findViewById(R.id.investment_icon_image_view);
        TextView levelAmountTextView        = (TextView)    contentView.findViewById(R.id.level_amount_text_view);
        final TextView timerTextView        = (TextView)    contentView.findViewById(R.id.timer_text_view);
        TextView messageTextView            = (TextView)    contentView.findViewById(R.id.message_text_view);
        TextView okButtonTextView           = (TextView)    contentView.findViewById(R.id.ok_button);
        final CheckBox showAgainCheckBox    = (CheckBox)    contentView.findViewById(R.id.show_again_checkbox);

        //Set up investment icon:
        long investmentType = investment.getTypeId();
        if(investmentType == Investment.INVESTMENT_TYPE_CALL || investmentType == Investment.INVESTMENT_TYPE_DYNAMICS_BUY){
            investmentIconImageView.setImageDrawable(getResources().getDrawable(R.drawable.cancel_investment_icon_up));
        }else if(investmentType == Investment.INVESTMENT_TYPE_PUT || investmentType == Investment.INVESTMENT_TYPE_DYNAMICS_SELL){
            investmentIconImageView.setImageDrawable(getResources().getDrawable(R.drawable.cancel_investment_icon_down));
        }

        //Set up the investment level and amount:
        String levelAmount = investment.getLevel() + " | " + AmountUtil.getUserFormattedAmount(investment.getAmount());
        levelAmountTextView.setText(levelAmount);

        //Setup the timer:
        countDownTimer = new CountDownTimer(time + TIMER_OFFSET, TIMER_TICK_INTERVAL) {
            @Override
            public void onTick(long millisUntilFinished) {
                long secondsUntilFinished = millisUntilFinished / 1000;
                long minutes    = secondsUntilFinished / 60;
                long seconds    = secondsUntilFinished % 60;

                timerTextView.setText(String.format(application.getLocale(), "%02d.%02d", minutes, seconds));
            }
            @Override
            public void onFinish() {
                timerTextView.setText(String.format(application.getLocale(), "%02d.%02d", 0, 0));
                if(!isCancelingInvestment){
                    cancelInvestmentListener.onFinished(false);
                    dismiss();
                }
            }
        };
        getDialog().setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                startTimer();
            }
        });

        //Set up message:
        String feeAmount = AmountUtil.getUserFormattedAmount((investment.getAmount() - investment.getOptionPlusFee())/10);
        messageTextView.setText(getString(R.string.cancel_investment_popup_message, feeAmount));

        //Set up the OK button:
        TextUtils.underlineText(okButtonTextView);
        okButtonTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelInvestment();
            }
        });

        //Set up the show again checkbox:
        showAgainCheckBox.setChecked(false);
        showAgainCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                showAgainCheckBox.setChecked(isChecked);
                showAgainCancelInvestmentPopup(!isChecked);
            }
        });
    }

    protected void startTimer(){
        countDownTimer.start();
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //////                          REQUESTs & RESPONSEs                                //////
    //////////////////////////////////////////////////////////////////////////////////////////

    private void cancelInvestment(){
        CancelInvestmentMethodRequest request = new CancelInvestmentMethodRequest();

        Display display = ((WindowManager) application.getCurrentActivity().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int orientation = display.getRotation();
        boolean isFromGraph = orientation == Surface.ROTATION_90 || orientation == Surface.ROTATION_270;

        request.setInvestmentId(investment.getId());
        request.setChoice((int) investment.getTypeId());
        request.setOpportunityId(investment.getOpportunityId());
        request.setAmount(investment.getAmount());
        request.setPageLevel(String.valueOf(investment.getCurrentLevel()));
        request.setOddLose(String.valueOf((float)investment.getOddsLose()));
        request.setOddWin(String.valueOf((float)investment.getOddsWin()));
        request.setFromGraph(isFromGraph);

        isCancelingInvestment = application.getCommunicationManager().requestService(this, "cancelInvestmentCallback",Constants.SERVICE_CANCEL_INVESTMENT, request, InvestmentMethodResult.class, true, false);
    }

    public void cancelInvestmentCallback(Object resObj){

        InvestmentMethodResult result = (InvestmentMethodResult) resObj;
        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            application.getFragmentManager().showDialogFragment(InvestmentCanceledDialogFragment.class, null);
            application.getCurrentActivity().refreshUser();
        }

        cancelInvestmentListener.onFinished(true);
        dismiss();
    }

    private void showAgainCancelInvestmentPopup(boolean isShowAgain){
        CancelInvestmentCheckboxStateMethodRequest request = new CancelInvestmentCheckboxStateMethodRequest();
        request.setShowCancelInvestment(isShowAgain);

        application.getCommunicationManager().requestService(this, "showAgainCancelInvestmentPopupCallback", Constants.SERVICE_CANCEL_INVESTMENT_SHOW_AGAIN, request, MethodResult.class, true, false);
    }

    public void showAgainCancelInvestmentPopupCallback(Object resObj){
        MethodResult result = (MethodResult) resObj;
        if(result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS){
            application.setShowCancelInvestmentPopup(!application.isShowCancelInvestmentPopup());
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////

}
