package com.anyoption.android.app.lightstreamer;

import com.lightstreamer.ls_client.HandyTableListener;
import com.lightstreamer.ls_client.UpdateInfo;

import android.util.Log;

public class AOTPSTableListener implements HandyTableListener {
    private LightstreamerListener listener;
    
    public AOTPSTableListener(LightstreamerListener listener) {
        this.listener = listener;
    }
    
    public void onUpdate(int itemPos, String itemName, UpdateInfo update) {
        long t = System.currentTimeMillis();
        try {
            listener.updateItem(itemPos, itemName, update);
        } catch (Exception e) {
            Log.e("data", "Can't process update data: " + e.getMessage(), e);
        }
		// Log.v("time", "Time to process update: " + (System.currentTimeMillis() - t));
    }

    public void onRawUpdatesLost(int itemPos, String itemName, int lostUpdates) {
        Log.v("lightstreamer", "onRawUpdatesLost - itemPos: " + itemPos + " itemName: " + itemName + " lostUpdates: " + lostUpdates);
    }

    public void onSnapshotEnd(int itemPos, String itemName) {
        Log.v("lightstreamer", "onSnapshotEnd - itemPos: " + itemPos + " itemName: " + itemName);
    }

    public void onUnsubscr(int itemPos, String itemName) {
        Log.v("lightstreamer", "onUnsubscr - itemPos: " + itemPos + " itemName: " + itemName);
    }

    public void onUnsubscrAll() {
        Log.v("lightstreamer", "onUnsubscrAll");
    }
}