package com.anyoption.android.app.widget.form;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.LayoutDirection;
import com.anyoption.android.R;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.widget.CheckBox;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.util.AttributeSet;
import android.view.Gravity;

/**
 * Custom Form {@link CheckBox}.
 * 
 * @author Anastas Arnaudov
 * 
 */
public class FormCheckBox extends CheckBox implements FormItem {

	private LayoutDirection direction;
	private Drawable checkBoxSelector;

	public FormCheckBox(Context context) {
		super(context);
		init(context, null, 0);
	}

	public FormCheckBox(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}

	public FormCheckBox(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		if (!isInEditMode()) {
			TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.FormCheckBox,
					defStyle, defStyle);
			try {
				checkBoxSelector = a.getDrawable(R.styleable.FormCheckBox_checkBoxSelectorDrawable);
			} finally {
				a.recycle();
			}
	
			direction = Application.get().getLayoutDirection();
			// Clear the default button Drawable (As it will be custom drawable and it will depend on
			// the layout direction).
			setButtonDrawable(new StateListDrawable());
			setupUIForDirection();			
		}
	}

	@Override
	public void setDirection(LayoutDirection direction) {
		if (this.direction != direction) {
			this.direction = direction;
			setupUIForDirection();
		}
		this.direction = direction;
	}

	@Override
	public void setupUIForDirection() {
		Drawable drawable = null;
		if (checkBoxSelector != null) {
			drawable = checkBoxSelector;
		} else {
			drawable = DrawableUtils.getDrawable(R.drawable.checkbox);
		}
		setCompoundDrawablePadding((int)getResources().getDimension(R.dimen.checkbox_padding));
		switch (direction) {
			case LEFT_TO_RIGHT:
				setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
				setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
				break;
			case RIGHT_TO_LEFT:
				setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
				setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
				break;
			default:
				break;
			}
	}

	@Override
	public LayoutDirection getDirection() {
		return this.direction;
	}

	@Override
	public boolean isEditable() {
		return isEnabled();
	}

	@Override
	public void setEditable(boolean isEditable) {
		setEnabled(isEditable);
	}

	@Override
	public boolean checkValidity() {
		return true;
	}

	@Override
	public String getValue() {
		return getText().toString();
	}

	@Override
	public void setValue(Object value) {
		setText(value.toString());
	}

	@Override
	public void addOnEditorActionListener(OnEditorActionListener onEditorActionListener) {
		// Do nothing
	}

	@Override
	public void clear() {
		setChecked(false);
	}

	@Override
	public void refreshUI() {
		setupUIForDirection();
	}

	@Override
	public void setFieldName(String fieldName) {

	}

	@Override
	public String getFieldName() {
		return null;
	}

	@Override
	public void displayError(String error) {

	}

}
