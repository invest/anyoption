package com.anyoption.android.app.model;

import java.io.Serializable;

/**
 * Created by veselinh on 24.11.2015 г..
 */
public class DropDownPopupItem {

    private int position;
    private String title;
    private boolean isSelected = false;

    public DropDownPopupItem(int position, String title, boolean isSelected) {
        this.position = position;
        this.title = title;
        this.isSelected = isSelected;

    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
}
