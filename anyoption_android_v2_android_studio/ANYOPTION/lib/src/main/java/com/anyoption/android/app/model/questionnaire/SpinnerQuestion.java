package com.anyoption.android.app.model.questionnaire;

import java.util.List;

import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;

public class SpinnerQuestion extends SingleChoiceQuestion {

	public SpinnerQuestion(long orderId, String quetionText, QuestionnaireQuestion question,
			QuestionnaireUserAnswer answer) {
		super(orderId, quetionText, question, answer);
	}

	public SpinnerQuestion(long orderId, String questionText, List<String> answerTexts) {
		super(orderId, questionText, answerTexts);
	}

	public SpinnerQuestion(long orderId, String quetionText) {
		super(orderId, quetionText);
	}

	@Override
	public boolean isAnswered() {
		if (getUserAnswer().getAnswerId() != null) {
			return true;
		}
		return false;
	}

	/**
	 * TODO
	 * 
	 * @return
	 */
	public int getSelectedAnswerIndex() {
		if (getUserAnswer() != null && getUserAnswer().getAnswerId() != null) {
			for (int i = 0; i < question.getAnswers().size(); i++) {
				if (question.getAnswers().get(i).getId() == getUserAnswer().getAnswerId()) {
					return i;
				}
			}
		}

		return -1;
	}

}
