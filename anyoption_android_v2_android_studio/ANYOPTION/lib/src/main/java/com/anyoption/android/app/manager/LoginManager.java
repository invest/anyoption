package com.anyoption.android.app.manager;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.activity.BubblesActivity;
import com.anyoption.android.app.event.LoginFailedEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.LoginLogoutEvent.Type;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.popup.BonusesToClaimDialogFragment;
import com.anyoption.android.app.popup.LowBalanceOkDialogFragment;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.RequestButton.InitAnimationListener;
import com.anyoption.android.app.widget.form.Form;
import com.anyoption.android.app.widget.form.FormCheckBox;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.beans.base.WithdrawUserDetails;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.common.service.withdrawuserdetails.WithdrawUserDetailsMethodRequest;
import com.anyoption.common.service.withdrawuserdetails.WithdrawUserDetailsMethodResult;
import com.anyoption.json.requests.BonusMethodRequest;
import com.anyoption.json.results.BonusMethodResult;

public class LoginManager {
    public static final String TAG = LoginManager.class.getSimpleName();

    protected Application application;
    protected SharedPreferencesManager sharedPreferencesManager;
    protected PushNotificationsManager pushNotificationsManager;
    protected DeepLinksManager deepLinksManager;

    protected Fragment fragment;
    private Form form;
    protected RequestButton loginButton;
    private boolean isTablet;
    protected String userName;
    protected String password;
    private boolean rememberPassword;
    protected boolean isShouldShowWithdrawDetails;
    protected UserMethodResult userMethodResult;
    protected boolean shouldChangeLocale;
    protected Bundle bundle;
    protected Screenable toScreen;

    public LoginManager() {
        application = Application.get();
        sharedPreferencesManager = application.getSharedPreferencesManager();
        pushNotificationsManager = application.getPushNotificationsManager();
        deepLinksManager = application.getDeepLinksManager();
    }

    public void onCreateView(Fragment fragment, boolean isTablet, final FormEditText emailField,
                             final FormEditText passwordField, final TextView forgotPassTextView, final RequestButton loginButton,
                             final FormCheckBox rememberPasswordCheckBox) {

        this.fragment = fragment;
        this.isTablet = isTablet;
        this.loginButton = loginButton;

        rememberPassword = false;

        setUpUsernameField(emailField);
        setUpPasswordFields(passwordField, rememberPasswordCheckBox);

        form = new Form(new Form.OnSubmitListener() {
            @Override
            public void onSubmit() {
                UserMethodRequest request = new UserMethodRequest();
                userName = emailField.getValue();
                request.setUserName(userName);
                request.setLogin(true);
                password = passwordField.getValue();
                request.setPassword(password);
                rememberPassword = rememberPasswordCheckBox.isChecked();

                if(application.getSharedPreferencesManager().getBoolean(Constants.PREFERENCES_IS_CAME_FROM_MARKETING, false)) {
                    request.getLogin().setCombinationId(Utils.getCombinationId());
                    request.getLogin().setDynamicParam(Utils.getDynamicParam());
                    request.getLogin().setAffSub1(Utils.getAffSub1(application.getCurrentActivity()));
                    request.getLogin().setAffSub2(Utils.getAffSub2(application.getCurrentActivity()));
                    request.getLogin().setAffSub3(Utils.getGclid(application.getCurrentActivity()));
                }

                Log.d(TAG, "getUser Request");
                if (!Application.get().getCommunicationManager().requestService(LoginManager.this, "getUserCallBack",
                        getServiceMethodName(), request, getResultClass(), isHandleError(), false)) {
                    loginButton.stopLoading();
                }
            }
        });
        form.addItem(emailField, passwordField);

        forgotPassTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Application.get().postEvent(new NavigationEvent(Screen.FORGOT_PASSWORD, NavigationType.DEEP));
            }
        });

        loginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (form.checkValidity()) {
                    loginButton.startLoading(new InitAnimationListener() {
                        @Override
                        public void onAnimationFinished() {
                            form.submit();
                        }
                    });
                }
            }
        });

    }

    protected String getServiceMethodName() {
        return application.getServiceMethodGetUser();
    }

    protected Class<? extends MethodResult> getResultClass() {
        return UserMethodResult.class;
    }

    protected boolean isHandleError() {
        return true;
    }

    public void getUserCallBack(Object result) {
        Log.d(TAG, "getUser Response");

        userMethodResult = (UserMethodResult) result;
        processLoginAttemptResult(loginButton, fragment, isTablet, form);
    }

    public void processLoginAttemptResult(RequestButton loginButton,
                                          Fragment fragment, boolean isTablet, Form form) {
        if (userMethodResult == null || userMethodResult.getErrorCode() != Constants.ERROR_CODE_SUCCESS) {
            if (userMethodResult != null) {
                Log.e(TAG, "Could not log in -> Error_Code: " + userMethodResult.getErrorCode());
            } else {
                Log.e(TAG, "Could not log in! Result is null.");
            }
            loginButton.stopLoading();
            application.postEvent(new LoginFailedEvent());
            return;
        }

        application.getSharedPreferencesManager().putBoolean(Constants.PREFERENCES_IS_CAME_FROM_MARKETING, false);

        // Etrader users should not log in AO app and vice versa. This check
        // does not allow it in
        // case the service does not return error.
        if ((application.getSkinId() != Skin.SKIN_ETRADER
                && userMethodResult.getUser().getSkinId() == Skin.SKIN_ETRADER)
                || (application.getSkinId() == Skin.SKIN_ETRADER
                && userMethodResult.getUser().getSkinId() != Skin.SKIN_ETRADER)) {
            loginButton.stopLoading();

            application.getNotificationManager()
                    .showNotificationError(fragment.getResources().getString(R.string.error));
            return;
        }

        ///////////////////////////////
        if (!sharedPreferencesManager.getBoolean(Constants.PREFERENCES_IS_LOGGED_ATLEAST_ONCE, false)) {
            sharedPreferencesManager.putBoolean(Constants.PREFERENCES_IS_LOGGED_ATLEAST_ONCE, true);
            sharedPreferencesManager.putBoolean(Constants.PREFERENCES_IS_TRADE_TOOLTIP_SHOWN, false);
        }

        sharedPreferencesManager.putBoolean(Constants.PREFERENCES_IS_FIRST_LOGIN, false);

        shouldChangeLocale = false;
        if (application.getLocale() == null) {
            shouldChangeLocale = true;
        } else if (!application.getLocale().getLanguage().equals(application.getSupportedLanguageForLocale(userMethodResult.getUser().getLocale().getLanguage()))) {
            shouldChangeLocale = true;
        }

        // Save the User:
        if (userMethodResult.getmId() != null && userMethodResult.getmId().length() > 0) {
            sharedPreferencesManager.putString(Constants.PREFERENCES_MID, userMethodResult.getmId());
        }
        if (userMethodResult.getEtsMid() != null && userMethodResult.getEtsMid().length() > 0) {
            sharedPreferencesManager.putString(Constants.PREFERENCES_ETS_MID, userMethodResult.getEtsMid());
        }

        if (rememberPassword) {
            sharedPreferencesManager.putString(Constants.PREFERENCES_PASSWORD_LOGIN, password);
        } else {
            sharedPreferencesManager.putString(Constants.PREFERENCES_PASSWORD_LOGIN, null);
        }

        ScreenUtils.hideKeyboard();
        application.postEvent(new LoginLogoutEvent(Type.LOGIN, userMethodResult, shouldChangeLocale));
        // Save the login count:
        int loginCount = application.getSharedPreferencesManager().getInt(Constants.PREFERENCES_LOGIN_COUNT, 0);
        application.getSharedPreferencesManager().putInt(Constants.PREFERENCES_LOGIN_COUNT, ++loginCount);
        ////////////////////////////////////////////////////

        bundle = new Bundle();
        toScreen = configAfterLoginScreen(userMethodResult, bundle);

        // Check the Appoxee Rate:
        if (loginCount == Constants.LOGIN_APPOXEE_RATE_US_COUNT) {
            application.getAppoxeeManager().showFeedback();
        }
        /////////////////////////

        //set user minimum investment amount in case of restart
        application.setMinInvestmentAmount(userMethodResult.getMinIvestmentAmount());
        form.clear();

        if (toScreen != application.getHomeScreen()) {
            finishLoginProcess();
        }else{
            checkWithdrawDetails();
        }
    }

    protected void finishLoginProcess(){
        // Change the Locale (which means we have to restart the Application, to
        // load appropriate resources)
        // after the next Screenable is chosen (depending on the Login flow)
        if (shouldChangeLocale) {
            application.changeLocale(userMethodResult.getUser().getLocale().getLanguage(), toScreen);
        } else {
            doAfterLoginNavigation(toScreen, bundle);
        }
    }

    protected Screenable configAfterLoginScreen(UserMethodResult userMethodResult, Bundle bundle) {
        Screenable toScreen = null;

        boolean canShowBonusesPopup = false;

        if (!(application.isLoggedIn() && application.getUser().getIsNeedChangePassword())) {
            User user = userMethodResult.getUser();
            if (!user.isAcceptedTerms()) {
                // The user has not accepted the Terms&Conditions (Example: He
                // was created on the Back-end):
                toScreen = Screen.AGREEMENT;
            } else if (pushNotificationsManager.isAppOpenedWithPush()) {
                Log.d(TAG, "App is opened by push notification.");
                boolean openDeposit = false;
                if (pushNotificationsManager.shouldCheckDeposit()) {
                    Log.d(TAG, "Checking Deposit Screen.");
                    openDeposit = !(userMethodResult.getUser().getBalance() > 0);
                }

                if (openDeposit) {
                    Log.d(TAG, "Should go to Deposit Screen.");
                    toScreen = Screen.DEPOSIT_MENU;
                } else {
                    toScreen = pushNotificationsManager.getToScreen();

                    if (toScreen == Screen.BUBBLES) {
                        application.setCurrentScreen(Application.Screen.BUBBLES);
                        Intent bubblesIntent = new Intent(application.getApplicationContext(), application.getSubClassForClass(BubblesActivity.class));
                        application.getCurrentActivity().startActivity(bubblesIntent);
                    }

                    if (isTablet && toScreen == Screen.ASSET_PAGE) {
                        // Closes the popup for login screen.
                        application.postEvent(new NavigationEvent(application.getHomeScreen(), NavigationType.INIT));
                        canShowBonusesPopup = true;
                    }

                    bundle.putAll(pushNotificationsManager.getScreenArguments());
                }
                pushNotificationsManager.setPushHandled();
            } else if (deepLinksManager.isDeepLink()) {
                Log.d(TAG, "App is opened by Facebook Deep Link.");
                Bundle deepLinkBundle = deepLinksManager.getDeepLinkBundle();
                toScreen = (Screenable) deepLinkBundle.getSerializable(Constants.EXTRA_SCREEN);
                if (toScreen == Screen.REGISTER) {
                    toScreen = application.getHomeScreen();
                    canShowBonusesPopup = true;
                } else if (toScreen == Screen.ASSET_VIEW_PAGER) {
                    long marketId = deepLinkBundle.getLong(Constants.EXTRA_MARKET_ID);
                    //Check if there is such Market:
                    if (application.getMarketForID(marketId) == null) {
                        Log.e(TAG, "There is no Market for the given id.");
                        //Go to default screen:
                        toScreen = Application.Screen.TRADE;
                        deepLinkBundle.putSerializable(Constants.EXTRA_SCREEN, Application.Screen.TRADE);
                        canShowBonusesPopup = true;
                    }
                } else if(toScreen == Screen.BUBBLES) {
                    application.setCurrentScreen(Screen.BUBBLES);
                    Intent intent = new Intent(application.getApplicationContext(), application.getSubClassForClass(BubblesActivity.class));
                    intent.putExtra(Constants.EXTRA_GO_TO_MAIN, true);
                    application.getCurrentActivity().startActivity(intent);
                    deepLinksManager.handledDeepLink();
                    return null;
                }
                bundle.putAll(deepLinkBundle);
                deepLinksManager.handledDeepLink();
            } else if (!RegulationUtils.showQuestionnaire()) {
                // The user has not filled in the mandatory questionnaire:
                return null;
            } else if (!application.isCopyop() && userMethodResult.getUser().getFirstDepositId() == 0) {
                // The user has not made a Deposit and we show the First New
                // Credit Card Screen:
                toScreen = Screen.FIRST_NEW_CARD;
            } else if(!application.isEtrader() && userMethodResult.getMinIvestmentAmount() > 0L) {
                application.setMinInvestmentAmount(userMethodResult.getMinIvestmentAmount());

                Bundle minInvestmentAmountBundle = new Bundle();
                minInvestmentAmountBundle.putLong(Constants.EXTRA_AMOUNT, userMethodResult.getMinIvestmentAmount());
                application.getFragmentManager().showDialogFragment(LowBalanceOkDialogFragment.class, minInvestmentAmountBundle);
                return application.getHomeScreen();
            } else if (RegulationUtils.isQuestionnaireFilled()) {
				/*
				 * 
				 * SAME AS IN MainActivity.java : handleRestart()
				 * 
				 */

                toScreen = application.getHomeScreen();

                if (RegulationUtils.showBlockedOrPEPOrRestrictedOrThresholdPopups()) {
                    if (RegulationUtils.showAccountBlocked()) {
                        if (RegulationUtils.showUploadDocuments()) {
                            canShowBonusesPopup = true;
                        } else {
                            return null;
                        }
                    } else {
                        return null;
                    }
                }
            } else {
                toScreen = application.getHomeScreen();
                canShowBonusesPopup = true;
            }

            // Final Check:
            if (toScreen == null) {
                Log.d(TAG, "The After Login Screen could NOT be configured. Setting the Home Screen.");
                toScreen = application.getHomeScreen();
            }

            if (!application.isEtrader() && showSeenBonuses(userMethodResult) && canShowBonusesPopup && user.isAcceptedTerms()) {
                //START OF OLD LOGIC
                // The user has new bonuses, show bonuses screen.
                // toScreen = Screen.BONUSES;
                //END OF OLD LOGIC
                getNewBonuses();
            }
        } else {
            application.postEvent(new NavigationEvent(Screen.UPDATE_PASSWORD, NavigationType.DEEP, Screenable.SCREEN_FLAG_REMOVE_MENU, Screenable.SCREEN_FLAG_REMOVE_BACK));
        }
        return toScreen;
    }

    protected boolean showBonuses(UserMethodResult userMethodResult) {
        return userMethodResult.getUser().isHasBonus();
    }

    protected boolean showSeenBonuses(UserMethodResult userMethodResult) {
        return userMethodResult.getUser().hasSeenBonuses();
    }

    protected void getNewBonuses() {
        BonusMethodRequest request = new BonusMethodRequest();
        request.setBonusStateId(0);
        request.setShowOnlyNotSeenBonuses(1);
        application.getCommunicationManager().requestService(LoginManager.this, "getNewBonusesCallback", Constants.SERVICE_GET_BONUSES, request, BonusMethodResult.class);
    }

    public void getNewBonusesCallback(Object result) {
        Bundle bundle = new Bundle();

        boolean isSingle = false;
        BonusUsers bonusUsers = null;
        String[] turnoverFactor = {};

        if (result != null) {
            BonusMethodResult bonusResult = (BonusMethodResult) result;
            if (bonusResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
                if (bonusResult.getBonuses().length == 1) {
                    isSingle = true;
                    bonusUsers = bonusResult.getBonuses()[0];
                }
            }
        }
        bundle.putBoolean(Constants.EXTRA_IS_SINGLE_BONUS, isSingle);
        bundle.putSerializable(Constants.EXTRA_BONUS, bonusUsers);
        bundle.putStringArray(Constants.EXTRA_BONUS_FACTOR, turnoverFactor);
        application.getFragmentManager().showDialogFragment(BonusesToClaimDialogFragment.class, bundle);
    }

    private void checkWithdrawDetails(){
        Log.d(TAG, "Checking the withdraw details from server...");
        WithdrawUserDetailsMethodRequest request = new WithdrawUserDetailsMethodRequest();
        WithdrawUserDetails details = new WithdrawUserDetails();
        details.setUserId(userMethodResult.getUser().getId());
        request.setWithdrawUserDetails(details);

        application.getCommunicationManager().requestService(this, "checkWithdrawDetailsCallback", Constants.SERVICE_SHOW_WITHDRAW_DETAILS, request, WithdrawUserDetailsMethodResult.class, false, false);
    }

    public void checkWithdrawDetailsCallback(Object resultObj){
        Log.d(TAG, "Withdraw details callback.");
        WithdrawUserDetailsMethodResult result = (WithdrawUserDetailsMethodResult) resultObj;
        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            Log.d(TAG, "Withdraw details callback. OK");
            isShouldShowWithdrawDetails = result.isShowPopup();
        } else {
            Log.e(TAG, "Withdraw details callback. ERROR");
        }

        if (isShouldShowWithdrawDetails && toScreen == application.getHomeScreen()) {
            toScreen = Screen.WITHDRAW_ASSIST;
        }
        finishLoginProcess();
    }

    protected void doAfterLoginNavigation(Screenable toScreen, Bundle bundle) {
        if (toScreen != null) {
            application.postEvent(new NavigationEvent(toScreen, NavigationType.INIT, bundle));
        }
    }

    public Form getForm() {
        return form;
    }

    private void setUpUsernameField(FormEditText field) {
        field.addValidator(ValidatorType.EMPTY, ValidatorType.USERNAME_MINIMUM_CHARACTERS);
        // Set the email of the previously logged user:
        field.setValue(sharedPreferencesManager.getString(Constants.PREFERENCES_USER_NAME, null));
        String emailHint = fragment.getString(R.string.username) + " / " + fragment.getString(R.string.email);
        field.getHintTextView().setText(emailHint);
    }

    private void setUpPasswordFields(FormEditText passwordField, FormCheckBox rememberPasswordCheckBox) {
        passwordField.addValidator(ValidatorType.EMPTY, ValidatorType.ALNUM, ValidatorType.PASSWORD_MINIMUM_CHARACTERS);

        String savedPassword = sharedPreferencesManager.getString(Constants.PREFERENCES_PASSWORD_LOGIN, null);
        if (savedPassword != null) {
            passwordField.setValue(savedPassword);
        }

        rememberPasswordCheckBox.setChecked(savedPassword != null);
    }

}