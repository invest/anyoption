package com.anyoption.android.app.fragment.my_account.banking.deposit;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView.OnEditorActionListener;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.OnActivityResultListener;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.RequestButton.InitAnimationListener;
import com.anyoption.android.app.widget.form.FormDatePicker;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.android.app.widget.form.FormEditText.FocusChangeListener;
import com.anyoption.common.beans.base.CreditCard;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.json.requests.CardMethodRequest;
import com.anyoption.json.requests.CcDepositMethodRequest;
import com.anyoption.json.requests.CreditCardValidatorRequest;
import com.anyoption.json.results.CardMethodResult;
import com.anyoption.json.results.CreditCardValidatorResponse;
import com.anyoption.json.results.TransactionMethodResult;

import java.util.Calendar;
import java.util.StringTokenizer;

import io.card.payment.CardIOActivity;

/**
 * @author liors
 *
 */
public class FirstNewCardFragment extends BaseDepositFragment implements OnActivityResultListener, TextWatcher {
	private static final String TAG = FirstNewCardFragment.class.getSimpleName();
	private View rootView;
	private FormEditText amountField;
	protected FormEditText cardNumberField;
	private FormDatePicker expiryDateField;
	private FormEditText cvvField;
	protected FormEditText cardHolder;
	private ImageView logoCc;
	private boolean isShouldSubmitForm;


	@Override
	protected Screenable setupScreen() {
		return Screen.FIRST_NEW_CARD;
	}
	
	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static FirstNewCardFragment newInstance(Bundle bundle){
		FirstNewCardFragment firstNewCardFragment = new FirstNewCardFragment();	
		firstNewCardFragment.setArguments(bundle);
		return firstNewCardFragment;
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.first_new_card_layout, container, false);	

		View scanButton = rootView.findViewById(R.id.scan_button);
		scanButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				scanCard();
			}
		});
		
		submitButton = (RequestButton) rootView.findViewById(R.id.new_card_deposit_button);
		if (application.getUser() != null && application.getUser().getFirstDepositId() == 0L) {
			submitButton.setText(R.string.newCardScreenTradeNowButton);
		}

		setCurrency(rootView, R.id.currency_selector);

		onCreateViewBase(rootView.findViewById(R.id.content));

		amountField = (FormEditText) rootView.findViewById(R.id.amount);
		amountField.setShouldShowOKImage(false);
		amountField.addValidator(ValidatorType.EMPTY, ValidatorType.CURRENCY_AMOUNT);
		amountField.setFieldName(Constants.FIELD_AMOUNT);
		setupDefaultDepositAmount();
		amountField.setAddTextChangedListener(this);
		
		logoCc = (ImageView) rootView.findViewById(R.id.first_new_card_logo_cc);
		cardNumberField = (FormEditText) rootView.findViewById(R.id.card_number);
		cardNumberField.setShouldShowOKImage(false);
		cardNumberField.addValidator(ValidatorType.EMPTY, ValidatorType.DIGITS);
		cardNumberField.setFieldName(Constants.FIELD_CARD_NUMBER);
		cardNumberField.setAddTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// Logo CC by type
		        Drawable imgCard = DrawableUtils.getImageByCCType(Utils.getCcTypeByNumber(cardNumberField.getValue()));
		        if (null != imgCard) {
		        	logoCc.setImageDrawable(imgCard);
				}
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
			@Override
			public void afterTextChanged(Editable s) {
				cardNumberField.setupHint(true);
			}
		});
		cardNumberField.getEditText().setImeOptions(EditorInfo.IME_ACTION_NEXT);
		cardNumberField.addOnEditorActionListener(new OnEditorActionListener() {
		    @Override
		    public boolean onEditorAction(android.widget.TextView view, int actionId, KeyEvent event) {
		        int result = actionId & EditorInfo.IME_MASK_ACTION;
		        switch(result) {
			        case EditorInfo.IME_ACTION_NEXT:
			        	Log.d(TAG, "IME_ACTION_NEXT");
			        	ScreenUtils.hideKeyboard(cardNumberField.getEditText());
			        	expiryDateField.performClick();
			            break;
		        }
		        return true;
		    }
		});
		cardNumberField.addFocusChangeListener(new FocusChangeListener() {
			@Override
			public void onFocusChanged(boolean hasFocus) {
				if(!hasFocus){
					validateCCNumber();
				}
			}
		});


		expiryDateField = (FormDatePicker) rootView.findViewById(R.id.expiry_date);
		expiryDateField.setMonthAndYearOnly(true);		
		Calendar initTime = Calendar.getInstance();
		initTime.set(Calendar.MONTH, Calendar.JANUARY);
		initTime.set(Calendar.YEAR, initTime.get(Calendar.YEAR) + 2);
		expiryDateField.setInitialDate(initTime, false);
		expiryDateField.addValidator(ValidatorType.EMPTY, ValidatorType.CC_EXP_DATE);
		expiryDateField.setFieldName(Constants.FIELD_EXPIRY_DATE);
		
		cvvField = (FormEditText) rootView.findViewById(R.id.cvv); 
		cvvField.addValidator(ValidatorType.EMPTY, ValidatorType.DIGITS, ValidatorType.CC_PASS_MINIMUM_CHARACTERS);
		cvvField.setFieldName(Constants.FIELD_CVV);
		cvvField.addFocusChangeListener(new FocusChangeListener() {
			@Override
			public void onFocusChanged(boolean hasFocus) {
				if(hasFocus){
					logoCc.setImageResource(R.drawable.cvv);
				}else{
					// Logo CC by type
			        Drawable imgCard = DrawableUtils.getImageByCCType(Utils.getCcTypeByNumber(cardNumberField.getValue()));
			        if (null != imgCard) {
			        	logoCc.setImageDrawable(imgCard);
					}
				}
			}
		});

		cardHolder = (FormEditText) rootView.findViewById(R.id.holderName);
		if (application.getSkinId() == Skin.SKIN_ETRADER) {
			cardHolder.addValidator(ValidatorType.EMPTY);
		} else {
			cardHolder.addValidator(ValidatorType.EMPTY, ValidatorType.LETTERS_AND_SPACE_ONLY);
		}
		cardHolder.setFieldName(Constants.FIELD_HOLDER_NAME);

		formBase.addItem(amountField, cardNumberField, expiryDateField, cardHolder, cvvField);

		return rootView;
	}

	protected void scanCard(){
		Intent scanIntent = new Intent(application.getCurrentActivity(), CardIOActivity.class);

        // customize these values to suit your needs.
        scanIntent.putExtra(CardIOActivity.EXTRA_USE_PAYPAL_ACTIONBAR_ICON, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false

        // hides the manual entry button
        // if set, developers should provide their own manual entry mechanism in the app
        scanIntent.putExtra(CardIOActivity.EXTRA_SUPPRESS_MANUAL_ENTRY, false); // default: false

        // matches the theme of your application
        scanIntent.putExtra(CardIOActivity.EXTRA_KEEP_APPLICATION_THEME, false); // default: false

	    // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
	    application.addOnActivityResultListener(this);
	    application.getCurrentActivity().startActivityForResult(scanIntent, Constants.REQUEST_CODE_CREDIT_CARD_SCAN);
	}
	
	@Override
	public void onActivityResultEvent(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	    if (requestCode == Constants.REQUEST_CODE_CREDIT_CARD_SCAN) {
	        if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
	        	Log.d(TAG, "Scan Credit Card was OK");
	            io.card.payment.CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
	            handleScanResult(scanResult);
	        }
	        else {
	        	Log.d(TAG, "Scan Credit Card was canceled");
	        }
	    }
	}
	
	protected void handleScanResult(io.card.payment.CreditCard scanResult){
		//Card number:
        cardNumberField.setValue(scanResult.cardNumber);
        
        //Expire Date:
        Calendar expiryTime = Calendar.getInstance();
        expiryTime.set(Calendar.MONTH, scanResult.expiryMonth);
        expiryTime.set(Calendar.YEAR, scanResult.expiryYear);
        expiryDateField.setInitialDate(expiryTime, true);
        
        //CVV:
        cvvField.setValue(scanResult.cvv);
	}
	
	@Override
	public void submitDeposit() {

		Log.d(TAG, "prepare insert card service call");
		CardMethodRequest request = new CardMethodRequest();           
        CreditCard card = new CreditCard();
        StringTokenizer expiryDate = new StringTokenizer(expiryDateField.getValue(), FormDatePicker.DATE_FORMAT_DELIMITER);
        
        card.setHolderId(Constants.NO_ID_NUM);
        card.setCcNumber(Long.valueOf(cardNumberField.getValue()));
        card.setCcPass(cvvField.getValue());
        card.setHolderName(cardHolder.getValue());
		card.setExpMonth(expiryDate.nextElement().toString());
		card.setExpYear(expiryDate.nextElement().toString().substring(2));
		card.setTypeId(Utils.getCcTypeByNumber(cardNumberField.getValue()));

        request.setCard(card);
        
        Log.d(TAG, "about to make service call to " + Constants.SERVICE_INSERT_CARD);
		if(!Application.get().getCommunicationManager().requestService(FirstNewCardFragment.this, "insertCardCallBack", Constants.SERVICE_INSERT_CARD, request , CardMethodResult.class, false, false)){
			isSubmitButtonEnabled = true;
			setSubmitButtonEnabled(true);
		}
		
	}
	
	public void insertCardCallBack(Object resultObj){
		Log.d(TAG, "insertCardCallBack");
		CardMethodResult result = (CardMethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Insert card succefully");
			CcDepositMethodRequest request = new CcDepositMethodRequest();
			request.setCcPass(cvvField.getValue());
			request.setAmount(amountField.getValue());
			request.setCardId(result.getCard().getId());
			request.setError(result.getError());
			request.setFirstNewCardChanged(false); //For repeat failure  
			 Log.d(TAG, "about to make service call to " + Constants.SERVICE_INSERT_DEPOSIT_CARD);
			if(!Application.get().getCommunicationManager().requestService(FirstNewCardFragment.this, "insertDepositCallBack", Constants.SERVICE_INSERT_DEPOSIT_CARD, request , TransactionMethodResult.class, false,false)){
				isSubmitButtonEnabled = true;
				setSubmitButtonEnabled(true);
			}

			amountField.setOkViewVisible(true);
		} else {
			Log.d(TAG, "Error inserting failed");
			isSubmitButtonEnabled = true;
			setSubmitButtonEnabled(true);
			ErrorUtils.displayFieldError(formBase, result);
		}
	}

	@Override
	public void firstDepositCallBack(Object resultObj) {
		Log.d(TAG, "firstDepositCallBack");
		super.firstDepositCallBack(resultObj);
		rootView.findViewById(R.id.currency_selector).setVisibility(View.VISIBLE);

		if (isFirstEverDeposit) {
			Log.d(TAG, "firstDepositCallBack + First Ever Deposit");
			currency.setEditable(true);
			rootView.findViewById(R.id.common_fields).setVisibility(View.VISIBLE);
		} else {
			Log.d(TAG, "firstDepositCallBack + not First Ever Deposit");
			currency.setEditable(false);
		}

		setUpSubmitButtonListener(isFirstEverDeposit);
	}

	protected void setUpSubmitButtonListener(boolean isFirstEverDeposit) {
		if (isFirstEverDeposit) {
			submitButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (isSubmitButtonEnabled) {
						if (formBase.checkValidity()) {
							Log.d(TAG, "Done submit common fields. about to submit the rest");
							submitButton.startLoading(new InitAnimationListener() {
								@Override
								public void onAnimationFinished() {
									isSubmitButtonEnabled = false;
									setSubmitButtonEnabled(false);
									isShouldSubmitForm = true;
									validateCCNumber();
//									formBase.submit();
								}
							});
						}
					}
				}
			});
		} else {
			submitButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (isSubmitButtonEnabled) {
						if (formBase.checkValidity()) {
							submitButton.startLoading(new InitAnimationListener() {

								@Override
								public void onAnimationFinished() {
									isSubmitButtonEnabled = false;
									setSubmitButtonEnabled(false);
									isShouldSubmitForm = true;
									validateCCNumber();
//									formBase.submit();
								}
							});
						}
					}
				}
			});
		}
	}

	@Override
	public String getAmount() {
		double amount = Double.parseDouble(amountField.getValue());
		return AmountUtil.getFormattedAmount(amount, application.getCurrency());
	}

	@Override
	public String getCurrencySymbol() {
		return currency.getSelectedCurrency().getSymbol();
	}

	@Override
	public void setSubmitButtonEnabled(boolean isEnabled) {
		isSubmitButtonEnabled = isEnabled;
		if(submitButton != null){
			submitButton.setEnabled(isEnabled);	
			if(isEnabled){
				submitButton.stopLoading();
			}
		}
	}
	
	@Override
	protected void setupDefaultDepositAmount() {
		if(application.getUser().getPredefinedDepositAmount() != null){
			amountField.setValue(application.getUser().getPredefinedDepositAmount());
        }
	}
	
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		isAmountEdited = true;
	}
	
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
	
	@Override
	public void afterTextChanged(Editable s) {}
	
	@Override
	protected void changeDefaultAmountForCurrency(Currency currency) {
		amountField.removeTextChangedListener(this);
		amountField.setValue((int)this.currency.getPredefinedAmountForCurrencyId(currency.getId()));
		amountField.setAddTextChangedListener(this);
	}

	private void validateCCNumber() {
		Log.d(TAG, "validateCCNumber");
		CreditCardValidatorRequest request = new CreditCardValidatorRequest();
		request.setCcNumber(cardNumberField.getValue());
		request.setSkinId(application.getSkinId());

		application.getCommunicationManager().requestService(Constants.SERVICE_VALIDATE_CC_NUMBER, request, CreditCardValidatorResponse.class, "validateCCNumberCallback", this);
	}

	public void validateCCNumberCallback(Object resObj){
		Log.d(TAG, "validateCCNumberCallback");
		CreditCardValidatorResponse result = (CreditCardValidatorResponse) resObj;
		if(result != null && cardNumberField != null){
			if(!result.isValid()){
				cardNumberField.displayError(getString(R.string.errorInvalidCreditCardNumber));
			}else{
				if(isShouldSubmitForm){
					formBase.submit();
				}
				cardNumberField.setValid(true);
			}
		}

		setSubmitButtonEnabled(true);
	}

}