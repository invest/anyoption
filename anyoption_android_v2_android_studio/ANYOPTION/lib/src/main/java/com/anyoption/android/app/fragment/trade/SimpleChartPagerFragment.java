package com.anyoption.android.app.fragment.trade;

import java.util.ArrayList;

import com.anyoption.android.app.Application.LayoutDirection;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.lightstreamer.LightstreamerConnectionHandler;
import com.anyoption.android.app.lightstreamer.LightstreamerListener;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.json.requests.ChartDataMethodRequest;
import com.anyoption.json.results.ChartDataMethodResult;
import com.lightstreamer.ls_client.UpdateInfo;

import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

/**
 * Custom {@link BaseFragment} that displays several simple chart graphics.
 * 
 * @author Anastas Arnaudov
 * 
 */
public class SimpleChartPagerFragment extends NavigationalFragment implements
		LightstreamerConnectionHandler.StatusListener, LightstreamerListener, Callback {

	public final String TAG = SimpleChartPagerFragment.class.getSimpleName();

	private static final int MESSAGE_SHOW_OFF_STATE = 1;
	private static final int MESSAGE_SHOW_EMPTY_STATE = 2;
	private static final int MESSAGE_UPDATE_MARKET_BG = 3;
	
	private static final int NUMBER_OF_PAGES = 4;

	private Handler handler;

	private ArrayList<SimpleChartFragment> simpleChartFragmentsList;
	private ViewPager pager;
	private ImageView leftArrowImageView;
	private ImageView rightArrowImageView;
	private LinearLayout dotsLayout;
	private FragmentStatePagerAdapter adapter;
	private int currentPage = 0;
	private boolean isPageChanging = false;

	private View rootView;

	protected ImageView bgImageView;
	private boolean shouldSetupMarketBackground;
	
	/**
	 * Use this method to get new instance of the fragment.
	 * 
	 * @return
	 */
	public static SimpleChartPagerFragment newInstance(Bundle bundle){
		SimpleChartPagerFragment simpleChartsFragment = new SimpleChartPagerFragment();
		simpleChartsFragment.setArguments(bundle);
		return simpleChartsFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		handler = new Handler(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.simple_chart_pager_fragment, container, false);
	
		bgImageView = (ImageView) rootView.findViewById(R.id.simple_charts_chart_bg_image_view);
		pager = (ViewPager) rootView.findViewById(R.id.assets_featured_charts_view_pager);
		leftArrowImageView = (ImageView) rootView.findViewById(R.id.assets_featured_pager_left_arrow_image_view);
		rightArrowImageView = (ImageView) rootView.findViewById(R.id.assets_featured_pager_right_arrow_image_view);
		dotsLayout = (LinearLayout) rootView.findViewById(R.id.assets_featured_pages_dots);
		
		simpleChartFragmentsList = getSimpleChartFragments();
		
		adapter = new FragmentStatePagerAdapter(getChildFragmentManager()) {
			@Override
			public int getCount() {
				return simpleChartFragmentsList.size();
			}
			@Override
			public Fragment getItem(int index) {
				return simpleChartFragmentsList.get(index);
			}
		};
		
		pager.setAdapter(adapter);
		setupInitialItem();
		
		leftArrowImageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(currentPage == 0){
					// We see the first page:
					currentPage = adapter.getCount() - 1;
				}else{
					currentPage--;
				}
				isPageChanging = true;
				unsubscribeChartFromLS();
				pager.setCurrentItem(currentPage, true);					
			}
		});
		
		rightArrowImageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(currentPage == adapter.getCount() - 1){
					// We see the last page:
					currentPage = 0;
				}else{
					currentPage++;
				}
				isPageChanging = true;
				unsubscribeChartFromLS();
				pager.setCurrentItem(currentPage, true);
			}
		});
		
		setupDots();
		changeDots(currentPage);
		
		setupArrows();
		
		pager.addOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				Log.d(TAG, "onPageSelected");
				currentPage = position;
				changeDots(currentPage);
				setupArrows();
			}
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
				Log.d(TAG, "onPageScrolled");
			}
			@Override
			public void onPageScrollStateChanged(int state) {
				switch (state) {
					case ViewPager.SCROLL_STATE_SETTLING:
						Log.d(TAG, "START PAGE CHANGE");
						isPageChanging = true;
						unsubscribeChartFromLS();
						break;
					case ViewPager.SCROLL_STATE_DRAGGING:
						Log.d(TAG, "START PAGE CHANGE");
						isPageChanging = true;
						unsubscribeChartFromLS();
						break;
					case ViewPager.SCROLL_STATE_IDLE:
						Log.d(TAG, "END PAGE CHANGE");
						isPageChanging = false;
						getChartData();
						break;
				}
			}
		});

		getChartData();

		return rootView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if(isPaused){
			getChartData();
			isPaused = false;
		}
	}
	
	@Override
	public void onDestroyView() {
		unsubscribeChartFromLS();
		super.onDestroyView();
	}
	
	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if(!hidden){
			getChartData();
		}else{
			unsubscribeChartFromLS();
		}
	}

	@Override
	protected void refreshUser() {
		//Do nothing
	}

	private ArrayList<SimpleChartFragment> getSimpleChartFragments() {
		if(simpleChartFragmentsList == null){
			simpleChartFragmentsList = new ArrayList<SimpleChartFragment>();
		}
		
		for(int i=1; i <= NUMBER_OF_PAGES; i++){
			simpleChartFragmentsList.add(SimpleChartFragment.newInstance());			
		}

		return simpleChartFragmentsList;
	}

	private void setupInitialItem(){
		if(application.getLayoutDirection() == LayoutDirection.LEFT_TO_RIGHT){
			currentPage = 0;
		}else{
			currentPage = NUMBER_OF_PAGES - 1;
		}
		pager.setCurrentItem(currentPage);			
	}
	
	/**
	 * Create dots that represent page count.
	 */
	private void setupDots(){
		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		params.setMargins((int)ScreenUtils.convertDpToPx(application, 1), 0, (int)ScreenUtils.convertDpToPx(application, 1), 0);
		//Create dot for each page:
		for(int i = 0; i < adapter.getCount(); i++){
			ImageView dotImage = new ImageView(application.getCurrentActivity());
			dotImage.setImageResource(getOtherDot());
			
			dotsLayout.addView(dotImage, params);
		}
		dotsLayout.invalidate();
	}
	
	/**
	 * Change the dot that corresponds to the current page.
	 */
	private void changeDots(int page){
		for(int i = 0; i < adapter.getCount(); i++){
			int dotImageId = 0;
			if(i == page){
				dotImageId = getCurrentDot();
			}else{
				dotImageId = getOtherDot();
			}
			ImageView dotImage = (ImageView) dotsLayout.getChildAt(i);
			dotImage.setImageResource(dotImageId);
			
		}
		dotsLayout.invalidate();
	}
	
	@SuppressWarnings("static-method")
	protected int getCurrentDot(){
		return R.drawable.dot_white;
	}
	
	@SuppressWarnings("static-method")
	protected int getOtherDot(){
		return R.drawable.dot_filled_blue_dark;
	}
	
	/**
	 * Sets up the Left/Right page arrows.
	 */
	private void setupArrows(){
		if(currentPage == 0){
			leftArrowImageView.setVisibility(View.INVISIBLE);
			rightArrowImageView.setVisibility(View.VISIBLE);
		}else if(currentPage < adapter.getCount() - 1){
			leftArrowImageView.setVisibility(View.VISIBLE);
			rightArrowImageView.setVisibility(View.VISIBLE);
		}else{
			leftArrowImageView.setVisibility(View.VISIBLE);
			rightArrowImageView.setVisibility(View.INVISIBLE);
		}
	}

	protected void getChartData() {
		Log.d(TAG, "Getting Chart Data!");
		unsubscribeChartFromLS();//Detach the LIGHSTREAMER
		ChartDataMethodRequest request = new ChartDataMethodRequest();
		if(application.getLayoutDirection() == LayoutDirection.LEFT_TO_RIGHT){
			request.setBox(currentPage);			
		}else{
			request.setBox(NUMBER_OF_PAGES - 1 - currentPage);
		}
		request.setOpportunityTypeId(Opportunity.TYPE_REGULAR);
		if(!application.getCommunicationManager().requestService(this, "gotChartData", Constants.SERVICE_GET_CHART_DATA, request, ChartDataMethodResult.class, false, false)){
			Log.e(TAG, "Error. Could not get the Chart Data.");
			handler.obtainMessage(MESSAGE_SHOW_EMPTY_STATE).sendToTarget();
		}
	}

	public void gotChartData(Object result) {
		Log.d(TAG, "chartCallback: resultObj -> " + result);
		SimpleChartFragment simpleChartFragment = (SimpleChartFragment) adapter.getItem(currentPage);
		ChartDataMethodResult chartDataMethodResult = (ChartDataMethodResult) result;
		if (chartDataMethodResult != null && chartDataMethodResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Got Chart Data!");
			if(chartDataMethodResult.getOpportunityId() > 0){
				Market market = application.getMarketForID(chartDataMethodResult.getMarketId());
				if(market != null){
					rootView.findViewById(R.id.simple_charts_off_textview).setVisibility(View.GONE);
					simpleChartFragment.setChartData(chartDataMethodResult);
					shouldSetupMarketBackground = true;
					//Attach the LIGHSTREAMER:
					subscribeChartForLS();	
				}				
			}else{
				handler.obtainMessage(MESSAGE_SHOW_OFF_STATE).sendToTarget();
			}
		}else{
			Log.e(TAG, "Error with the Chart Data.");
			handler.obtainMessage(MESSAGE_SHOW_EMPTY_STATE).sendToTarget();
		}
	}
	
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	//
	//@@@@@@@@@@@@@					LIGHSTREAMER					@@@@@@@@@@@@
	//
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	/**
	 * Attach the LIGHSTREAMER.
	 */
	private void subscribeChartForLS() {
		SimpleChartFragment simpleChartFragmentadapter = (SimpleChartFragment) adapter.getItem(currentPage);
		int scheduled = simpleChartFragmentadapter.getOpportunity().getScheduled();
		long marketID = simpleChartFragmentadapter.getMarket().getId();
		long opportunityTypeID = simpleChartFragmentadapter.getOpportunity().getTypeId();
			
		String item;
		if (opportunityTypeID == Opportunity.TYPE_REGULAR) {
			item = "aotps_" + scheduled + "_" + marketID;
		} else {
			item = "op_" + marketID;
		}
			
		String[] items = new String[] {item};

		application.getLightstreamerManager().subscribeSimpleChartTable(this, items);
	}

	/**
	 * Detach the LIGHSTREAMER.
	 */
	private void unsubscribeChartFromLS() {
		application.getLightstreamerManager().unsubscribeTable(LightstreamerManager.TABLE_SIMPLE_CHART);
	}
	
	@Override
	public void onStatusChange(int status) {
		Log.d(TAG, "LIGHSTREAMER onStatusChange" + status);
	}

	@Override
	public void updateItem(int itemPos, String itemName, UpdateInfo update) {
		if (isPageChanging) {
            Log.d(TAG, "Page Changing... ignore update");
            return; 
        }
		
		boolean shouldSetupOffHours = false;
		boolean shouldGetDataAgain = false;
		
		//Check the opportunity state:
		if (update.isValueChanged("ET_STATE") && update.getOldValue("ET_STATE") != null) {
				int oppState = Integer.parseInt(update.getNewValue("ET_STATE"));
				if (oppState == Opportunity.STATE_CLOSING_1_MIN) {
					// If the oldValue is not null that means we were on the page before the state changed - this should save us from infinite market requests.
					// If the new state is waiting for expiry the 1 min of waiting for expiry just expired so we should check for new market.
					shouldSetupOffHours = false;
					shouldGetDataAgain = true;
				}else if(oppState == Opportunity.STATE_PAUSED || oppState == Opportunity.STATE_SUSPENDED || oppState == Opportunity.STATE_WAITING_TO_PAUSE){
					shouldSetupOffHours = false;
					shouldGetDataAgain = true;
				}
		}else{
			int oppState = Integer.parseInt(update.getNewValue("ET_STATE"));
			if(oppState == Opportunity.STATE_CREATED || oppState == Opportunity.STATE_PAUSED || oppState == Opportunity.STATE_SUSPENDED || oppState == Opportunity.STATE_WAITING_TO_PAUSE){
				shouldSetupOffHours = true;
				shouldGetDataAgain = false;
			}
		}
		
		if(shouldSetupOffHours || shouldGetDataAgain){
			if(shouldSetupOffHours){
				handler.obtainMessage(MESSAGE_SHOW_OFF_STATE).sendToTarget();
			}
			if(shouldGetDataAgain){
				getChartData();				
			}
			return;
		}
		
		
		//OK. Update the current chart: 
		SimpleChartFragment simpleChartFragment = (SimpleChartFragment) adapter.getItem(currentPage);
		simpleChartFragment.updateItem(update);
		if(shouldSetupMarketBackground){
			shouldSetupMarketBackground = false;
			if (!handler.hasMessages(MESSAGE_UPDATE_MARKET_BG)) {
				handler.obtainMessage(MESSAGE_UPDATE_MARKET_BG).sendToTarget();
			}
		}
	}

	@Override
	public boolean handleMessage(Message msg) {
		switch (msg.what) {
		case MESSAGE_SHOW_OFF_STATE:
			setupOffState();
			break;
		case MESSAGE_SHOW_EMPTY_STATE:
			setupEmptyState();
			break;
		case MESSAGE_UPDATE_MARKET_BG:
			setupMarketBackground();
			break;
		}
		return false;
	}

	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	
	@Override
	protected Screenable setupScreen() {
		return Screen.SIMPLE_CHART_PAGER;
	}

	private void setupOffState() {
		SimpleChartFragment simpleChartFragment = (SimpleChartFragment) adapter
				.getItem(currentPage);
		simpleChartFragment.clearData();
		bgImageView.setImageResource(R.drawable.asset_not_available_bg);
		rootView.findViewById(R.id.simple_charts_off_textview).setVisibility(View.VISIBLE);
	}

	private void setupEmptyState() {
		SimpleChartFragment simpleChartFragment = (SimpleChartFragment) adapter
				.getItem(currentPage);
		simpleChartFragment.clearData();
		bgImageView.setImageResource(R.drawable.default_asset_bg);
		rootView.findViewById(R.id.simple_charts_off_textview).setVisibility(View.GONE);
	}

	protected void setupMarketBackground() {
		SimpleChartFragment simpleChartFragment = (SimpleChartFragment) adapter
				.getItem(currentPage);
		Market market = simpleChartFragment.getMarket();
		bgImageView.setImageDrawable(DrawableUtils.getChartBackground(market));
	}

}
