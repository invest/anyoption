package com.anyoption.android.app.fragment.trade;

import java.util.Date;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.TextView;
import com.anyoption.android.app.widget.chart.BaseInteractiveView.ClickListener;
import com.anyoption.android.app.widget.chart.ChartView;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.json.results.ChartDataMethodResult;
import com.lightstreamer.ls_client.UpdateInfo;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Custom {@link BaseFragment} that displays a simple chart graphic for a given assets.
 * @author Anastas Arnaudov
 *
 */
public class SimpleChartFragment extends BaseFragment implements Callback {
	
	public static final String TAG = SimpleChartFragment.class.getSimpleName();
	
	private static final long SECOND_MS = 1000;

	private static final int MESSAGE_SHOW_INFO = 1;
	private static final int MESSAGE_UPDATE_PROFIT = 2;

	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static SimpleChartFragment newInstance(){
		SimpleChartFragment simpleChartFragment = new SimpleChartFragment();
		return simpleChartFragment;
	}
	
	private View rootView;
	private TextView marketNameLabel;
	private TextView marketNameTextView;
	private TextView oppProfitLabel;
	private TextView oppProfit;
	private TextView oppTimeLeftLabel;
	private TextView oppTimeLeft;
	private ChartView chartView;

	private Market market;
	private Opportunity opportunity;
	private CountDownTimer timer;
	private boolean isOpportunityDetailsShown;
	protected boolean isChartClicked;
	private boolean isTablet;

	private View marketInfoLayout;

	private Handler handler;
	private String profit;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		market = new Market();
		setOpportunity(new Opportunity());
		isChartClicked = false;
		isTablet = getResources().getBoolean(R.bool.isTablet);

		handler = new Handler(this);
		isOpportunityDetailsShown = false;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.simple_chart_fragment, container, false);
	
		marketInfoLayout = rootView.findViewById(R.id.simple_chart_market_info_layout);
		
		marketNameLabel = (TextView) rootView.findViewById(R.id.simple_chart_market_name_label);
		marketNameLabel.setText(getString(R.string.featured) + ":");
		marketNameTextView = (TextView) rootView.findViewById(R.id.simple_chart_market_name);
		
		oppProfitLabel = (TextView) rootView.findViewById(R.id.simple_chart_market_opportunity_profit_label);
		oppProfitLabel.setText(getString(R.string.profit) + ":");		
		oppProfit = (TextView) rootView.findViewById(R.id.simple_chart_market_opportunity_profit);
		
		oppTimeLeftLabel = (TextView) rootView.findViewById(R.id.simple_chart_market_opportunity_time_left_label);
		oppTimeLeftLabel.setText(getString(R.string.expires));
		oppTimeLeft = (TextView) rootView.findViewById(R.id.simple_chart_market_opportunity_time_left);
		
		chartView = (ChartView) rootView.findViewById(R.id.simple_chart_view);
		chartView.addClickListener(new ClickListener() {
			@Override
			public void onClick() {
				if ((market != null && market.getId() != 0) && (!isChartClicked || isTablet)) {
					isChartClicked = true;
					goToAsset();
				}
			}
		});
		
		hideShowMarketInfoLayout(false);
		hideShowMarketName(false);
		hideShowOpportunityDetails(false);
		hideShowChart(false);
		
		return rootView;
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		if(hidden){
			
		}else{
			isChartClicked = false;
		}
		super.onHiddenChanged(hidden);
	}
	
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		clearReferences();
	}
	
	private void clearReferences(){
		if(timer != null){
			timer.cancel();
			timer = null;
		}
		market = null;
		opportunity = null;
	}
	
	private void goToAsset() {

		Bundle bundle = new Bundle();
		if (isTablet) {
			bundle.putSerializable(Constants.EXTRA_MARKET, market);
			bundle.putString(Constants.EXTRA_ACTION_BAR_TITLE, market.getDisplayName());
			bundle.putLong(Constants.EXTRA_MARKET_PRODUCT_TYPE, market.getProductTypeId());

			application.postEvent(new NavigationEvent(Screen.ASSET_PAGE, NavigationType.HORIZONTAL,bundle));
		} else {
			bundle.putSerializable(Constants.EXTRA_MARKET, market);
			application.postEvent(new NavigationEvent(Screen.ASSET_VIEW_PAGER, NavigationType.DEEP,bundle));
		}
	}

	/**
	 * Hides or shows the Market's Info Layout.
	 * @param visible
	 */
	private void hideShowMarketInfoLayout(boolean visible){
		int visibility;
		if(visible){
			visibility = View.VISIBLE;
		}else{
			visibility = View.INVISIBLE;
		}
		
		if(marketInfoLayout != null){
			marketInfoLayout.setVisibility(visibility);			
		}
	}
	
	/**
	 * Hides or shows the Market's Name.
	 * @param visible
	 */
	private void hideShowMarketName(boolean visible){
		int visibility;
		if(visible){
			visibility = View.VISIBLE;
		}else{
			visibility = View.INVISIBLE;
		}
		
		if(marketNameLabel != null && marketNameTextView != null){
			marketNameLabel.setVisibility(visibility);
			marketNameTextView.setVisibility(visibility);			
		}
	}
	
	/**
	 * Hides or shows the Opportunity's Profit and Expire Time.
	 * @param visible
	 */
	private void hideShowOpportunityDetails(boolean visible){
		int visibility;
		if(visible){
			visibility = View.VISIBLE;
		}else{
			visibility = View.INVISIBLE;
		}
		
		if(oppProfitLabel != null && oppProfit != null && oppTimeLeftLabel != null && oppTimeLeft != null){
			oppProfitLabel.setVisibility(visibility);
			oppProfit.setVisibility(visibility);
			oppTimeLeftLabel.setVisibility(visibility);
			oppTimeLeft.setVisibility(visibility);
			isOpportunityDetailsShown = visible;			
		}
	}
	
	private void hideShowChart(boolean visible){
		int visibility;
		if(visible){
			visibility = View.VISIBLE;
		}else{
			visibility = View.INVISIBLE;
		}
		
		if(chartView != null){
			chartView.setVisibility(visibility);
		}
	}
	
	/**
	 * Make request to get the Chart Data for the given asset.
	 */
	public void setChartData(ChartDataMethodResult chartDataMethodResult) {

		setMarket(application.getMarketForID(chartDataMethodResult.getMarketId()));
		if(market != null){
			TimeUtils.fixDates(chartDataMethodResult);
			Date timeLastInvest = new Date(chartDataMethodResult.getTimeLastInvest().getTime());
			
			if (opportunity == null) {
				opportunity = new Opportunity();
			}
			
			opportunity.setId(chartDataMethodResult.getOpportunityId());
			opportunity.setScheduled(chartDataMethodResult.getScheduled());
			opportunity.setTimeLastInvestMillsec(timeLastInvest.getTime());
			opportunity.setTypeId(chartDataMethodResult.getOpportunityTypeId());
			
			if(marketNameTextView != null){				
				marketNameTextView.setText(market.getDisplayName());
			}
			
//			hideShowMarketInfoLayout(true);
//			hideShowMarketName(true);
//			hideShowOpportunityDetails(false);
			
			if(chartView != null){
				chartView.setChartData(chartDataMethodResult);			
			}			
		}

	}

	/**
	 * Called when there is a LIGHSTREAMER update.
	 * <p>
	 * <b>IMPORTANT:</b> The LIGHSTREAMER invokes this method in a separate Thread. Any UI updates
	 * must be made in the Main Thread.
	 * 
	 * @param update
	 */
	public void updateItem(UpdateInfo update) {
		if (!isOpportunityDetailsShown) {
			handler.obtainMessage(MESSAGE_SHOW_INFO).sendToTarget();
		}

		// Update the Header:
		if (update.isValueChanged(LightstreamerManager.COLUMN_ET_ODDS)) {
			profit = update.getNewValue(LightstreamerManager.COLUMN_ET_ODDS);
			handler.obtainMessage(MESSAGE_UPDATE_PROFIT).sendToTarget();
		}

		// Update the Chart:
		if (chartView != null) {
			chartView.updateItem(update.getItemPos(), update.getItemName(), update);
		}
	}

	@Override
	public boolean handleMessage(Message msg) {
		switch (msg.what) {
		case MESSAGE_SHOW_INFO:
			showInfo();
			break;
		case MESSAGE_UPDATE_PROFIT:
			if (oppProfit != null) {
				oppProfit.setText(profit);
			}
			break;
		}

		return true;
	}

	private void showInfo() {
		hideShowMarketInfoLayout(true);
		hideShowMarketName(true);
		hideShowOpportunityDetails(true);
		hideShowChart(true);
		startTimer();
	}

	private void startTimer() {
		if (timer != null) {
			timer.cancel();
		}

		if(opportunity == null){
			return;
		}
		
		long timeLeft = opportunity.getTimeLastInvestMillsec() - TimeUtils.getCurrentServerTime();

		timer = new CountDownTimer(timeLeft, SECOND_MS) {

			@Override
			public void onTick(long millisUntilFinished) {
				if(oppTimeLeft != null){
					oppTimeLeft.setText(TimeUtils.formatTimeToMinutesAndSeconds(millisUntilFinished));					
				}
			}

			@Override
			public void onFinish() {
				if(oppTimeLeft != null){
					oppTimeLeft.setText("0:00");
				}
			}
		};

		timer.start();
	}

	public Market getMarket() {
		return market;
	}

	public void setMarket(Market market) {
		this.market = market;
	}

	public Opportunity getOpportunity() {
		return opportunity;
	}

	public void setOpportunity(Opportunity opportunity) {
		this.opportunity = opportunity;
	}

	public ChartView getChartView() {
		return chartView;
	}

	public void setChartView(ChartView chartView) {
		this.chartView = chartView;
	}

	/**
	 * Clears the Market name, Opportunity details and Chart data.
	 */
	public void clearData(){
		hideShowMarketInfoLayout(false);
		hideShowMarketName(false);
		hideShowOpportunityDetails(false);
		if(chartView != null){
			chartView.clearData();			
		}
		clearReferences();
	}

}
