package com.anyoption.android.app.util.adapter;

import android.animation.Animator;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.Spannable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.manager.DataBaseManager;
import com.anyoption.android.app.model.MarketDB;
import com.anyoption.android.app.model.MarketListGroup;
import com.anyoption.android.app.model.MarketListItem;
import com.anyoption.android.app.util.BitmapUtils;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.MarketsUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.TextView;
import com.anyoption.common.beans.base.Market;

import java.util.ArrayList;
import java.util.List;

/**
 * Expandable Adapter for the Markets Expandable List View.
 * @author Anastas Arnaudov
 */
public class MarketsExpandableListAdapter extends BaseExpandableListAdapter {

    public static final String TAG = MarketsExpandableListAdapter.class.getSimpleName();

    public static final byte MODE_ALL_ASSETS 		    = 1;
    public static final byte MODE_FEATURED_ASSETS 	    = 2;

    public static final String FILTER_NONE 				= "none";
    public static final String FILTER_BINARY 			= "filter_binary";
    public static final String FILTER_OPTION_PLUS 		= "filter_binary_plus";
    public static final String FILTER_DYNAMICS   		= "filter_dynamics";
    public static final String FILTER_BUBBLES   		= "filter_bubbles";


    /**
     * Adapter Listener.
     */
    public interface AdapterListener{
        void onFilter(String filter);
        void onLoading(boolean isLoading);
        void onRefreshing(boolean isRefreshing);
    }

    /**
     * View holder for the Expandable List View Group.
     */
    public static class GroupViewHolder{
        private View group;
        private View moreCloseButton;

        public GroupViewHolder(View group){
            this.group = group;
        }

        public View getMoreCloseButton(){
            if (moreCloseButton == null){
                moreCloseButton = group.findViewById(R.id.assets_list_group_more_less_button);
            }
            return (moreCloseButton);
        }
    }

    /**
     * View holder for the Expandable List View Child.
     */
    private static class ChildViewHolder {
        public ImageView iconImageView;
        public ImageView openOptionImageView;
        public ImageView optionPlusImageView;
        public ImageView dynamicsImageView;
        public TextView nameTextView;
        public TextView statusLabelTextView;
        public TextView statusValueTextView;
        public TextView currentValueTextView;
        public TextView profitTextView;
    }

    protected Application application;
    private List<MarketListGroup> marketGroupsListALL;
    private List<MarketListGroup> marketGroupsList = new ArrayList<MarketListGroup>();
    protected boolean isAnimatingGroup;
    protected AdapterListener listener;
    protected ExpandableListView expandableListView;
    protected byte mode = MODE_FEATURED_ASSETS;
    protected String filter = FILTER_BINARY;

    public MarketsExpandableListAdapter(ExpandableListView expandableListView, List<MarketListGroup> marketGroupsListALL, AdapterListener listener){
        super();
        this.application            = Application.get();
        this.expandableListView     = expandableListView;
        this.marketGroupsListALL    = marketGroupsListALL;
        this.listener               = listener;

        filter(filter);
    }

    public void setMode(byte mode){
        this.mode = mode;
    }

    public byte getMode(){
        return mode;
    }

    public void setFilter(String filter){
        this.filter = filter;
    }

    public String getFilter(){
        return filter;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        MarketListGroup marketGroup = marketGroupsList.get(groupPosition);
        List<MarketListItem> marketList = marketGroup.getMarketListItems();
        MarketListItem marketItem = marketList.get(childPosition);
        return marketItem;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return marketGroupsList.get(groupPosition).getMarketListItems().get(childPosition).getMarket().getId();
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        MarketListGroup marketGroup = marketGroupsList.get(groupPosition);
        List<MarketListItem> marketList = marketGroup.getMarketListItems();
        MarketListItem marketItem = marketList.get(childPosition);
        Market market = marketItem.getMarket();
        final ChildViewHolder childViewHolder;
        if(convertView == null) {
            LayoutInflater inf = (LayoutInflater) application.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.assets_list_item_layout, parent, false);
            childViewHolder = new ChildViewHolder();
            childViewHolder.iconImageView 			= (ImageView) convertView.findViewById(R.id.assets_list_item_icon);
            childViewHolder.openOptionImageView 	= (ImageView) convertView.findViewById(R.id.assets_list_item_open_option_icon);
            childViewHolder.optionPlusImageView 	= (ImageView) convertView.findViewById(R.id.assets_list_item_option_plus_icon);
            childViewHolder.dynamicsImageView	 	= (ImageView) convertView.findViewById(R.id.assets_list_item_dynamics_icon);
            childViewHolder.nameTextView 			= (TextView) convertView.findViewById(R.id.assets_list_item_name);
            childViewHolder.statusLabelTextView 	= (TextView) convertView.findViewById(R.id.assets_list_item_status_label);
            childViewHolder.currentValueTextView 	= (TextView) convertView.findViewById(R.id.assets_list_item_current_value);

            convertView.setTag(childViewHolder);
        }
        else{
            childViewHolder = (ChildViewHolder) convertView.getTag();
        }

        //Set up the Icon:
        childViewHolder.iconImageView.setImageDrawable(null);
        int resID = DrawableUtils.getMarketIconResID(market);
        Bitmap icon = null;
        BitmapUtils.loadBitmap(resID, icon, childViewHolder.iconImageView);

        boolean hasOpenOptions = marketItem.isHasOpenOptions();
        if(hasOpenOptions){
            childViewHolder.openOptionImageView.setVisibility(View.VISIBLE);
        }else{
            childViewHolder.openOptionImageView.setVisibility(View.GONE);
        }

        //Check if the Market is Option+ or Dynamics:
        boolean isOptionPlus = MarketsUtils.isOptionPlus(marketItem.getMarket());
        boolean isDynamics   = MarketsUtils.isDynamics(marketItem.getMarket());
        if(isOptionPlus){
            childViewHolder.optionPlusImageView.setVisibility(View.VISIBLE);
            childViewHolder.dynamicsImageView.setVisibility(View.GONE);
        }
        else if(isDynamics){
            childViewHolder.optionPlusImageView.setVisibility(View.GONE);
            childViewHolder.dynamicsImageView.setVisibility(View.VISIBLE);
        }
        else{
            childViewHolder.optionPlusImageView.setVisibility(View.GONE);
            childViewHolder.dynamicsImageView.setVisibility(View.GONE);
        }

        //Set up the Name:
        childViewHolder.nameTextView.setText(marketItem.getMarket().getDisplayName());
        ///////////////////////////

        //Set up the Status:
        String statusText;
        String statusLabel = (marketItem.getStatusLabel() != null) ? marketItem.getStatusLabel() : "";
        String statusValue = (marketItem.getStatusValue() != null) ? marketItem.getStatusValue() : "";
        String profit      = (marketItem.getProfitText() != null) ? marketItem.getProfitText() : "";

        if(isDynamics){
            statusText = statusLabel + statusValue + profit;
        }else{
            statusText = statusLabel + statusValue;
        }
        childViewHolder.statusLabelTextView.setText(statusText);

        Spannable spannable = null;
        if (marketItem.getStatusValue() != null && marketItem.getStatusValue().length() > 0) {
            if (marketItem.isLastMinutesState()) {
                spannable = TextUtils.decorateInnerText(childViewHolder.statusLabelTextView, spannable, statusText, marketItem.getStatusValue(), null, R.color.assets_list_item_status_text_red, 0);
            } else {
                spannable = TextUtils.decorateInnerText(childViewHolder.statusLabelTextView, spannable, statusText, marketItem.getStatusValue(), null, R.color.assets_list_item_status_text, 0);
            }
        }

        if (isDynamics && profit.length() > 0) {
            if (marketItem.getProfitPercent() != null && marketItem.getProfitPercent().length() > 0) {
                spannable = TextUtils.decorateInnerText(childViewHolder.statusLabelTextView, spannable, statusText, "#", null, R.color.transparent, 0);
                spannable = TextUtils.decorateInnerText(childViewHolder.statusLabelTextView, spannable, statusText, marketItem.getProfitPercent(), FontsUtils.Font.ROBOTO_BOLD, 0, 0);
            }
        }

        //Set up the Current Level:
        String currentValue = marketItem.getCurrentLevel();
        childViewHolder.currentValueTextView.setText(currentValue);

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        MarketListGroup marketGroup = marketGroupsList.get(groupPosition);
        List<MarketListItem> marketList = marketGroup.getMarketListItems();
        int childrenCount = marketList.size();
        return childrenCount;
    }

    @Override
    public Object getGroup(int groupPosition) {
        MarketListGroup marketGroup = marketGroupsList.get(groupPosition);
        return marketGroup;
    }

    @Override
    public int getGroupCount() {
        return marketGroupsList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return marketGroupsList.get(groupPosition).getMarketGroup().getId();
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final MarketListGroup marketGroup = marketGroupsList.get(groupPosition);
        GroupViewHolder groupWrapper = null;
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) application.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.assets_list_group_layout, parent, false);
            groupWrapper = new GroupViewHolder(convertView);
            convertView.setTag(groupWrapper);
        }
        else{
            groupWrapper = (GroupViewHolder) convertView.getTag();
        }

        TextView marketGroupTextView = (TextView) convertView.findViewById(R.id.assets_list_group_text_view);
        marketGroupTextView.setText(marketGroup.getMarketGroup().getName());
        View moreCloseButton = convertView.findViewById(R.id.assets_list_group_more_less_button);

        if(marketGroup.hasInactiveMarkets()){
            moreCloseButton.setVisibility(View.VISIBLE);
            if(!isAnimatingGroup){
                if(marketGroup.isShowOnlyOpenMarkets()){
                    moreCloseButton.setRotation(0f);
                }else{
                    moreCloseButton.setRotation(45f);
                }
            }

            groupWrapper.getMoreCloseButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    if(!isAnimatingGroup){
                        isAnimatingGroup = true;
                        final float newAngle = v.getRotation() + 45f;
                        v.animate().rotation(newAngle).setDuration(500).setInterpolator(new LinearInterpolator()).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                v.setRotation(newAngle);
                                isAnimatingGroup = false;
                                notifyDataSetChanged();
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {
                            }
                        });
                        for(MarketListGroup marketListGroup : marketGroupsListALL){
                            if(marketListGroup.getMarketGroup().getId() == marketGroup.getMarketGroup().getId()){
                                marketListGroup.setShowOnlyOpenMarkets(!marketListGroup.isShowOnlyOpenMarkets());
                            }
                        }

                        listener.onFilter(filter);
                    }
                }
            });
        }else{
            moreCloseButton.setVisibility(View.GONE);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    /**
     * Method to expand all groups in the ListView.
     **/
    public void expandAll() {
        int count = getGroupCount();
        for (int i = 0; i < count; i++){
            expandableListView.expandGroup(i);
        }
    }

    public synchronized List<MarketListGroup> getMarketGroups(){
        return marketGroupsList;
    }
    public synchronized void setMarketGroups(List<MarketListGroup> marketGroups) {
        this.marketGroupsList = new ArrayList<MarketListGroup>();
        this.marketGroupsList.addAll(marketGroups);
        notifyDataSetChanged();
    }
    /**
     * Filters the List View.
     * @param constrain
     */
    public void filter(String constrain){
        Log.d(TAG, "Filter Asset List : " + constrain);
        listener.onLoading(true);
        listener.onRefreshing(true);
        marketGroupsList.clear();

        //Check the filter code:
        if (constrain.isEmpty() || constrain.equals(FILTER_NONE)) {
            marketGroupsList.addAll(marketGroupsListALL);
        } else {

            //Get the User favorites/deleted Markets:
            List<MarketDB> savedMarkets = application.getDataBaseManager().getMarkets(application.getUser());

            //Check if the filter is Binary or Option+ or Dynamics:
            if(constrain.equals(FILTER_BINARY) || constrain.equals(FILTER_OPTION_PLUS) || constrain.equals(FILTER_DYNAMICS)){
                //Go through each group and add items:
                for (MarketListGroup marketListGroup : marketGroupsListALL) {

                    MarketListGroup newMarketListGroup = new MarketListGroup();
                    newMarketListGroup.setMarketGroup(marketListGroup.getMarketGroup());
                    newMarketListGroup.setShowOnlyOpenMarkets(marketListGroup.isShowOnlyOpenMarkets());
                    boolean isOptionPlusGroup = (marketListGroup.getMarketGroup().getId() == Constants.MARKET_GROUP_ID_OPTION_PLUS);
                    boolean isDynamicsGroup   = (marketListGroup.getMarketGroup().getId() == Constants.MARKET_GROUP_ID_DYNAMICS);
                    boolean isBinaryGroup     = !isOptionPlusGroup && !isDynamicsGroup;

                    List<MarketListItem> marketItemsList = marketListGroup.getMarketListItems();
                    List<MarketListItem> newMarketItemsList = new ArrayList<MarketListItem>();

                    //Go through each Item and check the filter and the mode:
                    for (MarketListItem marketListItem : marketItemsList) {
                        boolean isOptionPlusMarket  = MarketsUtils.isOptionPlus(marketListItem.getMarket());
                        boolean isDynamicsMarket    = MarketsUtils.isDynamics(marketListItem.getMarket());
                        boolean isBinaryMarket      = MarketsUtils.isBinary(marketListItem.getMarket());
                        // If the filter is Binary   -> the Market must be Binary
                        // If the filter is Option+  -> the Market must be Option+
                        // If the filter is Dynamics -> the Market must be Dynamics
                        if ( (constrain.equals(FILTER_BINARY) && isBinaryMarket) || (constrain.equals(FILTER_OPTION_PLUS) && isOptionPlusMarket)|| (constrain.equals(FILTER_DYNAMICS) && isDynamicsMarket)){
                            //If the Mode == Featured we add only Top(Featured) Markets:
                            if(mode == MODE_FEATURED_ASSETS){
                                if(marketListItem.getMarket().isTopMarketForGroup()){
                                    newMarketItemsList.add(marketListItem);
                                }
                            }else if(mode == MODE_ALL_ASSETS){
                                newMarketItemsList.add(marketListItem);
                            }
                        }
                    }

                    //Merge with the Favorites/Deleted markets:
                    if(mode == MODE_FEATURED_ASSETS){
                        for(MarketDB marketDB : savedMarkets){
                            boolean isOptionPlusMarket  = MarketsUtils.isOptionPlus(marketDB.getMarket());
                            boolean isDynamicsMarket    = MarketsUtils.isDynamics(marketDB.getMarket());
                            boolean isBinaryMarket      = MarketsUtils.isBinary(marketDB.getMarket());
                            // Check if the Market is from the group of the current iteration,
                            // OR if it is a Option+ than we check if the group is Option+
                            // OR if it is a Dynamics than we check if the group is Dynamics
                            if(marketDB.getMarket().getGroupId() == marketListGroup.getMarketGroup().getId() || (isOptionPlusMarket && isOptionPlusGroup) || (isDynamicsMarket && isDynamicsGroup)){
                                MarketListItem savedMarketListItem = marketListGroup.getMarketListItemForID(marketDB.getMarket().getId());
                                if (savedMarketListItem == null) {
                                    continue;
                                }
                                //Check if the item is favorite or deleted:
                                boolean shouldAdd = true;
                                List<MarketListItem> deletedList = new ArrayList<MarketListItem>();
                                for (MarketListItem marketListItem : newMarketItemsList) {
                                    if(marketListItem.getMarket().getId() == marketDB.getMarket().getId()){
                                        shouldAdd = false;
                                        if(marketDB.getState().equals(DataBaseManager.MARKETS_STATE_DELETED)){
                                            deletedList.add(marketListItem);
                                        }
                                    }
                                }
                                //Remove the items which are Deleted from the Featured list:
                                newMarketItemsList.removeAll(deletedList);

                                if(shouldAdd && marketDB.getState().equals(DataBaseManager.MARKETS_STATE_FAVORITE)){
                                    if ( (constrain.equals(FILTER_BINARY) && isBinaryMarket || (constrain.equals(FILTER_OPTION_PLUS) && isOptionPlusMarket) || (constrain.equals(FILTER_DYNAMICS) && isDynamicsMarket))) {
                                        newMarketItemsList.add(savedMarketListItem);
                                    }
                                }

                            }
                        }
                    }
                    /////////////////////////////////////////

                    //Subtract the Active Markets:
                    List<MarketListItem> activeMarkets = new ArrayList<MarketListItem>();
                    List<MarketListItem> inActiveMarkets = new ArrayList<MarketListItem>();
                    for(MarketListItem marketListItem : newMarketItemsList){
                        if(marketListItem.isActive()){
                            activeMarkets.add(marketListItem);
                        }else{
                            inActiveMarkets.add(marketListItem);
                            newMarketListGroup.setHasInactiveMarkets(true);
                        }
                    }
                    newMarketItemsList.clear();
                    //Show All or ONLY Active Markets:
                    if(marketListGroup.isShowOnlyOpenMarkets()){
                        newMarketItemsList.addAll(activeMarkets);
                    }else{
                        //Show the Inactive ones first:
                        newMarketItemsList.addAll(inActiveMarkets);
                        newMarketItemsList.addAll(activeMarkets);
                    }
                    /////////////////////////////////////////

                    //We add the group only if it belongs to the current filter (even if there are no Markets in it):
                    if ( (constrain.equals(FILTER_BINARY) && isBinaryGroup) || (constrain.equals(FILTER_OPTION_PLUS) && isOptionPlusGroup) || (constrain.equals(FILTER_DYNAMICS) && isDynamicsGroup)) {
                        newMarketListGroup.setMarketListItems(newMarketItemsList);
                        marketGroupsList.add(newMarketListGroup);
                    }
                }

            }

        }

        //Refresh the list:
        notifyDataSetChanged();
        expandAll();
        listener.onLoading(false);
        listener.onRefreshing(false);
    }

}
