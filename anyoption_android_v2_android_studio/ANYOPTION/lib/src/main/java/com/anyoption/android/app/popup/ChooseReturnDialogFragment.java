package com.anyoption.android.app.popup;

import com.anyoption.android.R;
import com.anyoption.android.app.model.LoseWinItem;
import com.anyoption.android.app.util.constants.Constants;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Fullscreen Dialog Fragment used to change win and lose return percent.
 * 
 * @author Mario Kutlev
 */
public class ChooseReturnDialogFragment extends BaseDialogFragment {

	public interface OnReturnChangedListener {

		/**
		 * Called when the user submits the new win and lose percent with OK.
		 */
		public void onReturnChanged(int winPercent, int losePercent);

	}

	private LinearLayout returnsLayout;

	private OnReturnChangedListener onReturnChangeListener;
	private LoseWinItem[] items;

	public static ChooseReturnDialogFragment newInstance(Bundle bundle) {
		ChooseReturnDialogFragment chooseReturnDialogFragment = new ChooseReturnDialogFragment();
		chooseReturnDialogFragment.setArguments(bundle);
		return chooseReturnDialogFragment;
	}

	@Override
	protected void onCreateContentView(View contentView) {
		returnsLayout = (LinearLayout) contentView.findViewById(R.id.choose_return_items_layout);

		setupReturns();

		View okButton = contentView.findViewById(R.id.choose_return_ok_button);
		okButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				View chosenItem = returnsLayout.getFocusedChild();
				// We get the tag set during the creation of the view.
				int winLosePosition = (Integer) chosenItem.getTag();

				if (onReturnChangeListener != null) {
					onReturnChangeListener.onReturnChanged((int) items[winLosePosition].getWin(),
							(int) items[winLosePosition].getLose());
				}

				ChooseReturnDialogFragment.this.dismiss();
			}
		});

	}

	private void setupReturns() {
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);

		Bundle bundle = getArguments();
		if (bundle != null
				&& bundle.getSerializable(Constants.EXTRA_CHOOSE_RETURN_PAIRS) != null
				&& bundle.getSerializable(Constants.EXTRA_CHOOSE_RETURN_PAIRS) instanceof LoseWinItem[]) {
			String selectItem = bundle.getString(Constants.EXTRA_CHOOSE_RETURN_SELECTED_ITEM);

			items = (LoseWinItem[]) getArguments().getSerializable(
					Constants.EXTRA_CHOOSE_RETURN_PAIRS);

			// Add the items to the layout
			for (int i = 0; i < items.length; i++) {
				LoseWinItem item = items[i];
				View itemView = inflater.inflate(R.layout.choose_return_item, returnsLayout, false);
				// We set the position of the return percents.
				itemView.setTag(i);

				TextView winView = (TextView) itemView.findViewById(R.id.choose_return_win_text);
				String winText = item.getWin() + "%";
				if (winText.equals(selectItem)) {
					// Select this row
					itemView.requestFocus();
				}
				winView.setText(winText);
				TextView loseView = (TextView) itemView.findViewById(R.id.choose_return_lose_text);
				loseView.setText(item.getLose() + "%");

				returnsLayout.addView(itemView);
			}
		}

	}

	@Override
	protected int getContentLayout() {
		return R.layout.choose_return_dialog;
	}

	public OnReturnChangedListener getOnReturnChangeListener() {
		return onReturnChangeListener;
	}

	/**
	 * Set an event listener on this fragment.
	 */
	public void setOnReturnChangeListener(OnReturnChangedListener onReturnChangeListener) {
		this.onReturnChangeListener = onReturnChangeListener;
	}

}
