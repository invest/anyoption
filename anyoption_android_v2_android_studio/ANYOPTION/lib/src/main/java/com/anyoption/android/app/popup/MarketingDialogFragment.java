package com.anyoption.android.app.popup;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.util.constants.Constants;

/**
 * @author Anastas Arnaudov
 */
public class MarketingDialogFragment extends BaseDialogFragment{

    private static final String TAG = MarketingDialogFragment.class.getSimpleName();
    private WebView webView;
    private String page;

    public static MarketingDialogFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new MarketingDialogFragment.");
        MarketingDialogFragment fragment = new MarketingDialogFragment();
        if (bundle == null) {
            bundle = new Bundle();
        }

        bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
        bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
        bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle != null){
            page = bundle.getString(Constants.EXTRA_PAGE);
        }
    }

    @Override
    protected int getContentLayout() {
        return R.layout.marketing_popup_layout;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreateContentView(View contentView) {
        webView = (WebView) contentView.findViewById(R.id.marketing_popup_web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT <= 18) {
            webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        }
        webView.setWebViewClient(new MyWebViewClient());

        String url = application.getDeepLink() + "?" + "s=" + application.getSkinId() + "&pageR=" + page + "&p=";

        if(application.isCopyop()) {
            url += "co";
        } else {
            url += "ao";
        }

        webView.loadUrl(url);
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            //Check if the page is redirected to the anyoption site - this means that the Claim button was pressed.
            if(!url.contains("deeplinks")){
                goToRegister();
                dismiss();
                return true;
            }
            return false;
        }
    }

    private void goToRegister(){
        application.postEvent(new NavigationEvent(Application.Screen.REGISTER, NavigationEvent.NavigationType.INIT));
    }

}
