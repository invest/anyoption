package com.anyoption.android.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.AssetInfoDrawerEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.LoginLogoutEvent.Type;
import com.anyoption.android.app.event.MainMenuEvent;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.ActionFragment;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.fragment.MenuFragment;
import com.anyoption.android.app.fragment.launch.WelcomeFragment;
import com.anyoption.android.app.fragment.login_register.AgreementFragment;
import com.anyoption.android.app.fragment.login_register.ForgotPasswordFragment;
import com.anyoption.android.app.fragment.login_register.LoginFragment;
import com.anyoption.android.app.fragment.login_register.RegisterFragment;
import com.anyoption.android.app.fragment.login_register.TermsAndConditionsFragment;
import com.anyoption.android.app.fragment.login_register.UnregulatedCountryFragment;
import com.anyoption.android.app.fragment.my_account.AccountBlockedFragment;
import com.anyoption.android.app.fragment.my_account.MyAccountFragment;
import com.anyoption.android.app.fragment.my_account.PersonalDetailsFragment;
import com.anyoption.android.app.fragment.my_account.SettledOptionsFragment;
import com.anyoption.android.app.fragment.my_account.UpdateInfoFragment;
import com.anyoption.android.app.fragment.my_account.UpdatePasswordFragment;
import com.anyoption.android.app.fragment.my_account.UploadDocumentsFragment;
import com.anyoption.android.app.fragment.my_account.banking.BankingFragment;
import com.anyoption.android.app.fragment.my_account.banking.deposit.DepositMenuFragment;
import com.anyoption.android.app.fragment.my_account.banking.deposit.DepositWelcomeFragment;
import com.anyoption.android.app.fragment.my_account.banking.deposit.DomesticDepositFragment;
import com.anyoption.android.app.fragment.my_account.banking.deposit.DomesticPaymentsFragment;
import com.anyoption.android.app.fragment.my_account.banking.deposit.ExistingCardFragment;
import com.anyoption.android.app.fragment.my_account.banking.deposit.FirstNewCardFragment;
import com.anyoption.android.app.fragment.my_account.banking.deposit.WireDepositFragment;
import com.anyoption.android.app.fragment.my_account.banking.deposit.other.Direct24DepositFragment;
import com.anyoption.android.app.fragment.my_account.banking.deposit.other.EPSDepositFragment;
import com.anyoption.android.app.fragment.my_account.banking.deposit.other.GiropayDepositFragment;
import com.anyoption.android.app.fragment.my_account.banking.withdraw.BankWireWithdrawFragment;
import com.anyoption.android.app.fragment.my_account.banking.withdraw.CreditCardWithdrawFragment;
import com.anyoption.android.app.fragment.my_account.banking.withdraw.WithdrawAssistFragment;
import com.anyoption.android.app.fragment.my_account.banking.withdraw.WithdrawClosedFragment;
import com.anyoption.android.app.fragment.my_account.banking.withdraw.WithdrawMenuFragment;
import com.anyoption.android.app.fragment.my_account.bonuses.BonusDetailsFragment;
import com.anyoption.android.app.fragment.my_account.bonuses.BonusesFragment;
import com.anyoption.android.app.fragment.my_account.my_options.MyOptionsFragment;
import com.anyoption.android.app.fragment.my_account.questionnaire.MandatoryQuestionnaireFragment;
import com.anyoption.android.app.fragment.my_account.questionnaire.OptionalQuestionnaireFragment;
import com.anyoption.android.app.fragment.my_account.questionnaire_new.QuestionnaireFragment;
import com.anyoption.android.app.fragment.settings.SettingsFragment;
import com.anyoption.android.app.fragment.support.SupportFragment;
import com.anyoption.android.app.fragment.trade.AssetViewPagerFragment;
import com.anyoption.android.app.fragment.trade.AssetsListFragment;
import com.anyoption.android.app.fragment.trade.FeaturedAssetsFragment;
import com.anyoption.android.app.fragment.trade.OpenOptionsFragment;
import com.anyoption.android.app.fragment.trade.TabletTradeFragment;
import com.anyoption.android.app.fragment.trade.TradeFragment;
import com.anyoption.android.app.manager.DeepLinksManager;
import com.anyoption.android.app.manager.FragmentManager;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.anyoption.android.app.manager.PopUpManager;
import com.anyoption.android.app.manager.PopUpManager.OnConfirmPopUpButtonPressedListener;
import com.anyoption.android.app.manager.PushNotificationsManager;
import com.anyoption.android.app.popup.BonusesToClaimDialogFragment;
import com.anyoption.android.app.popup.LowBalanceOkDialogFragment;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.json.requests.BonusMethodRequest;
import com.anyoption.json.results.BonusMethodResult;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

/**
 * The Main Context of the application.
 *
 * @author Anastas Arnaudov
 */
public class MainActivity extends BaseActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    /**
     * The Root View of the screen.
     */
    private View rootLayout;

    /**
     * The Main Component of each screen.
     */
    protected View mainView;
    protected SlidingMenu slidingMenu;

    protected MenuFragment menuFragment;
    private View menuView;
    private View menuViewLayout;
    private View popUpScreenView;
    private View popUpContentView;
    protected boolean canShowBonusesPopup = false;

    protected LightstreamerManager lightstreamerManager;
    protected FragmentManager fragmentManager;
    protected PushNotificationsManager pushNotificationsManager;
    protected DeepLinksManager deepLinksManager;

    protected boolean isInitialScreenShown = false;

    protected boolean isSlidingMenuUsed;

    protected boolean isMarketInfoDrawerOpen;

    /**
     * Currently displayed screen. Used to know which was the last screen opened. In
     * {@link #onEventMainThread(NavigationEvent)} {@link Application#getCurrentScreen()} returns
     * the value from the event.
     *
     * @see {@link Application#getCurrentScreen()}
     * @see {@link Application#setCurrentScreen()}
     */
    protected Screenable currentScreen;
    private Bundle currentScreenBundle;
    private NavigationEvent eventFromAnotherActivity;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        application.getSharedPreferencesManager().putBoolean(Constants.PREFERENCES_IS_FIRST_MAIN_SCREEN, false);
        setContentView(R.layout.main_layout);
        getWindow().setBackgroundDrawable(null);

        rootLayout = findViewById(R.id.root_layout);

        mainView = findViewById(R.id.main_view);

        menuFragment = (MenuFragment) BaseFragment.newInstance(application.getSubClassForClass(MenuFragment.class), null);

        registerForEvents();

        lightstreamerManager = application.getLightstreamerManager();
        fragmentManager = application.getFragmentManager();
        pushNotificationsManager = application.getPushNotificationsManager();
        deepLinksManager = application.getDeepLinksManager();

        if (isTablet) {
            isSlidingMenuUsed = false;
            menuView = findViewById(R.id.menu_frame);
            popUpScreenView = findViewById(R.id.popup_screen_frame);
            popUpContentView = findViewById(R.id.popup_screen_content_frame);
            menuView.setVisibility(View.GONE);
            View popUpScreenLayout = findViewById(R.id.popup_screen_frame);
            popUpScreenLayout.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (currentScreen != Screen.AGREEMENT
                            && currentScreen != Screen.TERMS_AND_CONDITIONS) {
                        application.postEvent(new NavigationEvent(Screen.TABLET_TRADE,
                                NavigationType.INIT));
                    }
                }
            });

            menuViewLayout = findViewById(R.id.menu_frame_layout);
            menuViewLayout.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    closeMenu();
                    menuViewLayout.setClickable(false);
                }
            });
            menuViewLayout.setClickable(false);

            currentScreen = Screen.TABLET_TRADE;
            application.setCurrentScreen(Screen.TABLET_TRADE);
            fragmentManager.openScreen(TabletTradeFragment.class, mainView.getId(),
                    new NavigationEvent(Screen.TABLET_TRADE, NavigationType.INIT));
        } else {
            fragmentManager.replaceFragment(R.id.menu_frame, menuFragment);
            isSlidingMenuUsed = true;
            slidingMenu = (SlidingMenu) findViewById(R.id.slidingmenulayout);
        }

        //Open the Screenable passed with the Intent:
        Screenable startScreen = ((Screenable) getIntent().getSerializableExtra(Constants.EXTRA_SCREEN));
        Bundle bundle = getIntent().getBundleExtra(Constants.EXTRA_BUNDLE);

        if (application.isLoggedIn() && application.getUser().getIsNeedChangePassword()) {
            Log.d(TAG, "User is logged and Change pass is needed!");
            application.postEvent(new NavigationEvent(Screen.UPDATE_PASSWORD, NavigationType.DEEP, Screenable.SCREEN_FLAG_REMOVE_MENU, Screenable.SCREEN_FLAG_REMOVE_BACK));
        } else {
            openInitialScreen(startScreen, bundle);
            if (application.isLoggedIn()) {
                if (!RegulationUtils.showQuestionnaire()) {
                    //The user has not filled in the mandatory questionnaire:
                } else if (!application.isEtrader() && application.getMinInvestmentAmount() > 0l) {
                    Bundle minInvestmentAmountBundle = new Bundle();
                    minInvestmentAmountBundle.putLong(Constants.EXTRA_AMOUNT, application.getMinInvestmentAmount());
                    application.postEvent(new NavigationEvent(application.getHomeScreen(), NavigationType.INIT));
                    application.getFragmentManager().showDialogFragment(LowBalanceOkDialogFragment.class, minInvestmentAmountBundle);
                    return;
                } else if (RegulationUtils.showRestrictedPopup() && RegulationUtils.showPEPPopup() && RegulationUtils.showThresholdRestrictedPopup() && RegulationUtils.showAccountBlocked()) {
                    canShowBonusesPopup = true;
			/*
			 * 
			 * SAME AS IN configAfterLoginScreen
			 * 
			 */
                } else {
                    canShowBonusesPopup = true;
                }

                if (!application.isEtrader() && showSeenBonuses() && canShowBonusesPopup && application.getUser().isAcceptedTerms()) {
                    //START OF OLD LOGIC
                    // The user has new bonuses, show bonuses screen.
                    // toScreen = Screen.BONUSES;
                    //END OF OLD LOGIC
                    getNewBonuses();
                }
            }
        }

    }

    protected boolean showSeenBonuses() {
        return application.getUser().hasSeenBonuses();
    }

    protected void getNewBonuses() {
        BonusMethodRequest request = new BonusMethodRequest();
        request.setBonusStateId(0);
        request.setShowOnlyNotSeenBonuses(1);
        application.getCommunicationManager().requestService(MainActivity.this, "getNewBonusesCallback", Constants.SERVICE_GET_BONUSES, request, BonusMethodResult.class);
    }

    public void getNewBonusesCallback(Object result) {
        Bundle bundle = new Bundle();

        boolean isSingle = false;
        BonusUsers bonusUsers = null;
        String[] turnoverFactor = {};

        if (result != null) {
            BonusMethodResult bonusResult = (BonusMethodResult) result;
            if (bonusResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
                if (bonusResult.getBonuses().length == 1) {
                    isSingle = true;
                    bonusUsers = bonusResult.getBonuses()[0];
                }
            }
        }
        bundle.putBoolean(Constants.EXTRA_IS_SINGLE_BONUS, isSingle);
        bundle.putSerializable(Constants.EXTRA_BONUS, bonusUsers);
        bundle.putStringArray(Constants.EXTRA_BONUS_FACTOR, turnoverFactor);
        application.getFragmentManager().showDialogFragment(BonusesToClaimDialogFragment.class, bundle);
    }

    protected void registerForEvents() {
        application.registerForEvents(this, NavigationEvent.class, MainMenuEvent.class);
    }

    protected void openInitialScreen(Screenable screen, Bundle bundle) {
        Log.d(TAG, "OPEN INITIAL SCREEN = " + screen);
        Log.d(TAG, "Check if initial Screen is shown.");
        if (!isInitialScreenShown) {
            Log.d(TAG, "Initial Screen is not shown.");
            isInitialScreenShown = true;
            if (screen != null) {
                if (screen != Screen.REGISTER && screen != Screen.LOGIN) {
                    ActionFragment.isInitialLoginRegister = false;

                    if(getIntent().getExtras().getSerializable(Constants.EXTRA_MARKET) != null){
                        bundle.putSerializable(Constants.EXTRA_MARKET, getIntent().getExtras().getSerializable(Constants.EXTRA_MARKET));
                    }
                }
                goToScreen(screen, bundle);
            } else {
                Log.d(TAG, "Initial Screen is null. Checking other navigations...");
                if (application.isLoggedIn()) {
                    Log.d(TAG, "User is logged in.");
                    if (pushNotificationsManager.isAppOpenedWithPush()) {
                        Log.d(TAG, "App is opened by push notification.");
                        if (pushNotificationsManager.getToScreen() == Screen.BUBBLES) {
                            application.setCurrentScreen(Application.Screen.BUBBLES);
                            Intent bubblesIntent = new Intent(application.getApplicationContext(), application.getSubClassForClass(BubblesActivity.class));
                            application.getCurrentActivity().startActivity(bubblesIntent);
                        } else {
                            application.postEvent(new NavigationEvent(pushNotificationsManager.getToScreen(), NavigationType.INIT, pushNotificationsManager.getScreenArguments()));
                        }
                        pushNotificationsManager.setPushHandled();
                    } else if (deepLinksManager.handleNavigationLoggedIn()) {
                        //Do nothing.
                    } else {
                        Log.d(TAG, "Navigating to the default app home screen.");
                        application.postEvent(new NavigationEvent(application.getHomeScreen(), NavigationType.INIT, bundle));
                        canShowBonusesPopup = true;
                    }
                } else {
                    Log.d(TAG, "User is not logged in.");
                    if (pushNotificationsManager.isAppOpenedWithPush() && !pushNotificationsManager.isLoginNeeded()) {
                        Log.d(TAG, "App is opened by push notification.");
                        application.postEvent(new NavigationEvent(pushNotificationsManager.getToScreen(), NavigationType.INIT, pushNotificationsManager.getScreenArguments()));
                        pushNotificationsManager.setPushHandled();
                    } else if (deepLinksManager.handleNavigationNOTLoggedIn()) {
                        //Do nothing.
                    } else {
                        Log.d(TAG, "Navigating to the default app home screen.");
                        application.postEvent(new NavigationEvent(Screen.LOGIN, isTablet ? NavigationType.DEEP : NavigationType.INIT, bundle));
                    }
                }
            }
        }
    }

    protected void goToScreen(Screenable screen, Bundle bundle) {
        application.postEvent(new NavigationEvent(screen, isTablet ? NavigationType.DEEP : NavigationType.INIT, bundle));
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");

        if (isTablet && currentScreen == Screen.TABLET_TRADE) {
            hidePopUpScreen();
        }

        lightstreamerManager.startLightstreamer();
        subscribeLightstreamerTables();

        if (eventFromAnotherActivity != null) {
            Log.d(TAG, "Posting event from another Activity. Event = " + eventFromAnotherActivity);
            application.postEvent(eventFromAnotherActivity);
        }

        pushNotificationsManager.onResume(this);
        deepLinksManager.onResumeMainActivity();
    }

    protected void subscribeLightstreamerTables() {
        if (application.getUser() != null) {
            lightstreamerManager.subscribeInsurancesTable(lightstreamerManager);
            lightstreamerManager.subscribeShoppingBagTable(lightstreamerManager);
        }
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        eventFromAnotherActivity = null;
        deepLinksManager.onPauseMainActivity();
        lightstreamerManager.unsubscribeAllTables();
        lightstreamerManager.stopLightstreamer();

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        application.unregisterForAllEvents(this);
        super.onDestroy();
    }

   protected void handleMenuOnChangeScreen(NavigationEvent event) {
       // If the menu is opened, we only close it.
       if (isMenuOpened()) {
           closeMenu();
       }
   }


    /**
     * Called when a {@link NavigationEvent} occurs. This method controls the logic - the fragment
     * transactions for the Main View.
     *
     * @param event
     */
    public void onEventMainThread(NavigationEvent event) {
        Log.d(TAG, "on NavigationEvent : " + event.getToScreen());

        handleMenuOnChangeScreen(event);

        Screenable toScreen = event.getToScreen();

        // If this event is for the same screen, it won't be handled.
        if (currentScreen == toScreen && (currentScreenBundle == event.getBundle() || Utils.areBundlesEqual(currentScreenBundle, event.getBundle()))) {
            Log.e(TAG, "currentScreen == toScreen  && currScreenBundle == event.getBundle()");
            return;
        }

        ScreenUtils.hideKeyboard(application.getCurrentEditTextOnFocus());

        // Check if the activity is NOT being destroyed and if it is the current activity:
        if (!isFinishing() && this == application.getCurrentActivity()) {
            int parentViewID = event.getParentViewID();
            Bundle bundle = event.getBundle();
            boolean isBottomOfStack = bundle.getBoolean(Constants.EXTRA_NAVIGATION_TO_STACK_BOTTOM,
                    false);

            if (parentViewID == 0) {
                if (isTablet && !isBottomOfStack) {
                    parentViewID = popUpContentView.getId();
                } else {
                    parentViewID = mainView.getId();
                }

                if (toScreen == Screen.TABLET_TRADE) {
                    parentViewID = mainView.getId();
                }
            }

            // For tablets the trading screen will always be first in the backStack.
            if (isTablet && event.getToScreen() != Screen.TABLET_TRADE && event.getNavigationType() == NavigationType.INIT) {
                int stackSize = fragmentManager.getStackSize();
                if (stackSize > 1) {
                    // Removes all stack items except TABLET_TRADE
                    fragmentManager.removeStackItems(stackSize - 1);
                }

                event.setNavigationType(NavigationType.DEEP);
            }

            Class<? extends BaseFragment> fragmentClass = getFragmentClass(toScreen);
            if (toScreen == Screen.TABLET_TRADE) {
                hidePopUpScreen();
            }

            if (fragmentClass != null) {
                // If we don't handle the event, we don't have to enable / disable menu swipe.
                setUpMenuSlide(toScreen);

                if (isTablet && parentViewID == popUpContentView.getId()) {
                    showPopUpScreen();
                }

                currentScreen = toScreen;
                currentScreenBundle = event.getBundle();

                fragmentManager.openScreen(fragmentClass, parentViewID, event);
            }
        } else {
            if (this != application.getCurrentActivity()) {
                eventFromAnotherActivity = event;
            }
        }
    }

    /**
     * Finds the screen fragment which has to be displayed.
     *
     * @param screen
     * @return
     */
    @SuppressWarnings("static-method")
    protected Class<? extends BaseFragment> getFragmentClass(Screenable screen) {
        Class<? extends BaseFragment> fragmentClass = null;

        if (screen == Screen.FORGOT_PASSWORD) {
            fragmentClass = ForgotPasswordFragment.class;
        } else if (screen == Screen.LOGIN) {
            fragmentClass = LoginFragment.class;
        } else if (screen == Screen.REGISTER) {
            fragmentClass = RegisterFragment.class;
        } else if (screen == Screen.UNREGULATED_COUNTRY) {
            fragmentClass = UnregulatedCountryFragment.class;
        } else if (screen == Screen.ASSET_VIEW_PAGER) {
            fragmentClass = AssetViewPagerFragment.class;
        } else if (screen == Screen.TRADE) {
            fragmentClass = TradeFragment.class;
        } else if (screen == Screen.ASSETS_FEATURED) {
            fragmentClass = FeaturedAssetsFragment.class;
        } else if (screen == Screen.ASSETS_LIST) {
            fragmentClass = AssetsListFragment.class;
        } else if (screen == Screen.OPEN_OPTIONS) {
            fragmentClass = OpenOptionsFragment.class;
        } else if (screen == Screen.SETTINGS) {
            fragmentClass = SettingsFragment.class;
        } else if (screen == Screen.MANDATORY_QUESTIONNAIRE) {
            fragmentClass = MandatoryQuestionnaireFragment.class;
        } else if (screen == Screen.OPTIONAL_QUESTIONNAIRE) {
            fragmentClass = OptionalQuestionnaireFragment.class;
        } else if (screen == Screen.MY_ACCOUNT) {
            fragmentClass = MyAccountFragment.class;
        } else if (screen == Screen.PERSONAL_DETAILS_MENU) {
            fragmentClass = PersonalDetailsFragment.class;
        } else if (screen == Screen.UPDATE_PASSWORD) {
            fragmentClass = UpdatePasswordFragment.class;
        } else if (screen == Screen.DEPOSIT_WELCOME) {
            fragmentClass = DepositWelcomeFragment.class;
        } else if (screen == Screen.EXISTING_CARD) {
            fragmentClass = ExistingCardFragment.class;
        } else if (screen == Screen.UPDATE_INFO) {
            fragmentClass = UpdateInfoFragment.class;
        } else if (screen == Screen.DOMESTIC_PAYMENTS) {
            fragmentClass = DomesticPaymentsFragment.class;
        } else if (screen == Screen.DOMESTIC_DEPOSIT) {
            fragmentClass = DomesticDepositFragment.class;
        } else if (screen == Screen.WIRE_DEPOSIT) {
            fragmentClass = WireDepositFragment.class;
        } else if (screen == Screen.FIRST_NEW_CARD) {
            fragmentClass = FirstNewCardFragment.class;
        } else if (screen == Screen.WELCOME) {
            fragmentClass = WelcomeFragment.class;
        } else if (screen == Screen.SUPPORT) {
            fragmentClass = SupportFragment.class;
        } else if (screen == Screen.BONUSES) {
            fragmentClass = BonusesFragment.class;
        } else if (screen == Screen.BONUS_DETAILS) {
            fragmentClass = BonusDetailsFragment.class;
        } else if (screen == Screen.TABLET_TRADE) {
            fragmentClass = TabletTradeFragment.class;
        } else if (screen == Screen.BANKING) {
            fragmentClass = BankingFragment.class;
        } else if (screen == Screen.WITHDRAW_MENU) {
            fragmentClass = WithdrawMenuFragment.class;
        } else if (screen == Screen.DEPOSIT_MENU) {
            fragmentClass = DepositMenuFragment.class;
        } else if (screen == Screen.BANK_WIRE_WITHDRAW) {
            fragmentClass = BankWireWithdrawFragment.class;
        } else if (screen == Screen.CC_WITHDRAW) {
            fragmentClass = CreditCardWithdrawFragment.class;
        } else if (screen == Screen.TERMS_AND_CONDITIONS) {
            fragmentClass = TermsAndConditionsFragment.class;
        } else if (screen == Screen.AGREEMENT) {
            fragmentClass = AgreementFragment.class;
        } else if (screen == Screen.SETTLED_OPTIONS) {
            fragmentClass = SettledOptionsFragment.class;
        } else if (screen == Screen.MY_OPTIONS) {
            fragmentClass = MyOptionsFragment.class;
        } else if (screen == Screen.DIRECT24_DEPOSIT) {
            fragmentClass = Direct24DepositFragment.class;
        } else if (screen == Screen.GIROPAY_DEPOSIT) {
            fragmentClass = GiropayDepositFragment.class;
        } else if (screen == Screen.EPS_DEPOSIT) {
            fragmentClass = EPSDepositFragment.class;
        } else if (screen == Screen.WITHDRAW_CLOSED) {
            fragmentClass = WithdrawClosedFragment.class;
        } else if (screen == Screen.QUESTIONNAIRE) {
            fragmentClass = QuestionnaireFragment.class;
        } else if (screen == Screen.UPLOAD_DOCUMENTS) {
            fragmentClass = UploadDocumentsFragment.class;
        } else if (screen == Screen.ACCOUNT_BLOCKED) {
            fragmentClass = AccountBlockedFragment.class;
        } else if (screen == Screen.WITHDRAW_ASSIST) {
            fragmentClass = WithdrawAssistFragment.class;
        }

        return fragmentClass;
    }

    protected boolean setUpMenuSlide(Screenable screen) {
        boolean enableMenuSlide = true;
        enableMenuSlide = !checkInitialLoginRegister();
        if (screen == Screen.ASSET_VIEW_PAGER || screen == Screen.DEPOSIT_WELCOME
                || screen == Screen.TERMS_AND_CONDITIONS || screen == Screen.AGREEMENT || (screen == Screen.UPDATE_PASSWORD && application.isLoggedIn() && application.getUser().getIsNeedChangePassword())) {
            enableMenuSlide = false;
        }

        enableMenuSwipe(enableMenuSlide);
        return enableMenuSlide;
    }

    protected boolean checkInitialLoginRegister() {
        return ActionFragment.isInitialLoginRegister;
    }

    /**
     * Called when a {@link MainMenuEvent} occurs.
     *
     * @param event
     */
    public void onEventMainThread(MainMenuEvent event) {
        Log.d(TAG, "on MainMenuEvent");

        if (isMenuOpened()) {
            closeMenu();
        } else {
            openMenu();
        }
    }


    protected boolean handleMenuOnBackPressed() {
        // If the menu is opened, we only close it.
        if (isMenuOpened()) {
            closeMenu();
            return true;
        }
        return false;
    }

    protected void handleOnBackPressedBackToTrade() {
        application.postEvent(new NavigationEvent(Screen.TRADE, NavigationType.INIT));
    }

    protected boolean handleBackPressedFromLastScreen() {
        if (application.isLoggedIn() || (currentScreen == Screen.ASSET_VIEW_PAGER || currentScreen == Screen.ASSET_PAGE)) {
            handleOnBackPressedBackToTrade();
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (handleMenuOnBackPressed()) {
            return;
        }

        //If we are on the Trade Screen and the Market Info Drawer is opened -> we should close it.
        if(application.getCurrentScreen() == Screen.ASSET_VIEW_PAGER && isMarketInfoDrawerOpen){
            application.postEvent(new AssetInfoDrawerEvent(false));
            return;
        }

        if (!(application.isLoggedIn() && application.getUser().getIsNeedChangePassword())) {

            // Special cases:
            if (application.getCurrentScreen() == Screen.UNREGULATED_COUNTRY) {
                Log.d(TAG, "OPENING WELCOME SCREENS");
                application.postEvent(new NavigationEvent(Screen.WELCOME, NavigationType.INIT));
                return;
            }

            if (currentScreen == Screen.AGREEMENT) {
                application.setUser(null);
                application.setUserRegulation(null);
                application.postEvent(new LoginLogoutEvent(Type.LOGOUT, null));
                application.postEvent(new NavigationEvent(Screen.LOGIN, NavigationType.INIT));
                return;
            }

            if (currentScreen == Screen.TRADE || currentScreen == Screen.TABLET_TRADE) {
                if (application.isLoggedIn()) {
                    showLogoutPopUp();
                    return;
                }
            } else if (!isTablet && fragmentManager.getStackSize() == 1) {
                if (ActionFragment.isInitialLoginRegister) {
                    ActionFragment.isInitialLoginRegister = false;
                }

                if (handleBackPressedFromLastScreen()) {
                    return;
                }
            }
            super.onBackPressed();
        } else {
            finish();
        }
    }

    /**
     * Enables or disables the menu sliding if sliding menu is used. Otherwise does nothing.
     *
     * @param enabled
     */
    public void enableMenuSwipe(boolean enabled) {
        if (isSlidingMenuUsed) {
            slidingMenu.setSlidingEnabled(enabled);
        }
    }

    protected boolean isMenuOpened() {
        if (isSlidingMenuUsed) {
            return slidingMenu.isMenuShowing();
        } else {
            return menuFragment.isVisible();
        }
    }

    protected void closeMenu() {
        if (isSlidingMenuUsed) {
            slidingMenu.showContent();
        } else if (menuFragment.isVisible()) {
            hideMenuFragment();
        }
    }

    protected void openMenu() {
        if (!(application.isLoggedIn() && application.getUser().getIsNeedChangePassword())) {
            hideKeyboard();
            if (isSlidingMenuUsed) {
                slidingMenu.showMenu();
            } else if (!menuFragment.isVisible()) {
                showMenuFragment();
            }
        } else {
            closeMenu();
        }
    }

    protected void showMenuFragment() {
        if (!(application.isLoggedIn() && application.getUser().getIsNeedChangePassword())) {
            if (menuView.getVisibility() != View.VISIBLE) {
                menuView.setVisibility(View.VISIBLE);
            }

            fragmentManager.replaceFragment(R.id.menu_frame, menuFragment);
            menuViewLayout.setClickable(true);
        } else {
            hideMenuFragment();
        }
    }

    private void hideMenuFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.hide(menuFragment);
        transaction.commit();
    }

    /**
     * Hides the keyboard if it is visible.
     */
    protected void hideKeyboard() {
        // Hide the keyboard if it is opened.
        View focusedView = rootLayout.findFocus();
        if (focusedView != null && focusedView instanceof EditText) {
            ScreenUtils.hideKeyboard((EditText) focusedView);
        }
    }

    private void showPopUpScreen() {
        popUpScreenView.setVisibility(View.VISIBLE);
    }

    private void hidePopUpScreen() {
        application.getFragmentManager().removeAllFragmentsFromView(popUpContentView.getId());
        popUpScreenView.setVisibility(View.GONE);
    }

    public void setMarketInfoDrawerOpen(boolean marketInfoDrawerOpen) {
        isMarketInfoDrawerOpen = marketInfoDrawerOpen;
    }

    protected void showLogoutPopUp() {
        PopUpManager popUpManager = application.getPopUpManager();
        popUpManager.showConfirmPopUp("", getString(R.string.logoutConfirm),
                new OnConfirmPopUpButtonPressedListener() {

                    @Override
                    public void onPositive() {
                        application.setUser(null);
                        application.setUserRegulation(null);
                        finish();
                    }

                    @Override
                    public void onNegative() {
                        // Just cancel the dialog
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        application.onActivityResult(requestCode, resultCode, intent);
    }
}
