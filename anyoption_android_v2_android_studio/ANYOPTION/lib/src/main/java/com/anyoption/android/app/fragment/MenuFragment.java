package com.anyoption.android.app.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.activity.BubblesActivity;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.LoginLogoutEvent.Type;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.manager.PopUpManager.OnConfirmPopUpButtonPressedListener;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Custom {@link BaseFragment} that represents the Main Menu of the application.
 */
public class MenuFragment extends BaseFragment {

	private static final String TAG = MenuFragment.class.getSimpleName();

	private View root;
	private TextView greetingTextView;
	
	protected ListView menuList;
	private MenuArrayAdapter menuListAdapter;
	protected ArrayList<MenuItem> items;
	
	private boolean isTablet;

	/**
	 * Use this method to obtain new instance of the fragment.
	 */
	public static MenuFragment newInstance() {
		return newInstance(new Bundle());
	}

	/**
	 * Use this method to obtain new instance of the fragment.
	 */
	public static MenuFragment newInstance(Bundle bundle) {
		MenuFragment fragment = new MenuFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		root = inflater.inflate(R.layout.menu_fragment, container, false);
		isTablet = getResources().getBoolean(R.bool.isTablet);
		
		
		setUpGreeting();
		menuList = (ListView) root.findViewById(R.id.menu_items_list_view);
		setUpList();
		if(isTablet) {
			root.findViewById(R.id.menu_logo).setVisibility(View.GONE);
		}

		return root;
	}

	@Override
	public void onResume() {
		super.onResume();
		registerForEvents();
		updateUI();
	}

	@Override
	public void onPause() {
		application.unregisterForAllEvents(this);
		super.onPause();
	}
	
	protected void registerForEvents(){
		application.registerForEvents(this, LoginLogoutEvent.class, NavigationEvent.class);
	}

	protected void updateUI() {
		setUpItems();
		menuListAdapter.setItems(items);
		menuListAdapter.notifyDataSetChanged();

		setUpGreeting();
		setupSelecteditem(application.getCurrentScreen());
	}

	public void onEventMainThread(LoginLogoutEvent event) {
		Log.d(TAG, "Login event received.");

		updateUI();
	}

	public void onEventMainThread(NavigationEvent event){
		Log.d(TAG, "Navigation Event received.");
		setupSelecteditem(event.getToScreen());
	}
	
	private void setupSelecteditem(Screenable screenable){
		Log.d(TAG, "Selecting Menu Item:");
		if(menuList == null || menuListAdapter == null){
			Log.e(TAG, "Menu List is null");
			return;
		}
		MenuItem selectedItem = getMenuItemForScreen(screenable);
		for(MenuItem item : items){
			int position = items.indexOf(item);
			if(selectedItem != null && item == selectedItem){
				menuList.setItemChecked(position, true);
			}else{
				menuList.setItemChecked(position, false);
			}
		}
	}

	private MenuItem getMenuItemForScreen(Screenable toScreen) {
		MenuItem result = null;
		if(toScreen == Screen.TRADE){
			result = MenuItem.TRADE;
		}else if(toScreen == Screen.BUBBLES){
			result = MenuItem.BUBBLES;
		}else if(toScreen == Screen.DEPOSIT_MENU){
			result = MenuItem.DEPOSIT;
		}else if(toScreen == Screen.MY_ACCOUNT){
			result = MenuItem.MY_ACCOUNT;
		}else if(toScreen == Screen.MY_OPTIONS){
			result = MenuItem.MY_OPTIONS;
		}else if(toScreen == Screen.AGREEMENT){
			result = MenuItem.AGREEMENT_TERMS;
		}else if(toScreen == Screen.SUPPORT){
			result = MenuItem.SUPPORT;
		}else if(toScreen == Screen.SETTINGS){
			result = MenuItem.SETTINGS;
		}else if(toScreen == Screen.LOGIN){
			result = MenuItem.SIGN_UP;
		}else if(toScreen == Screen.REGISTER){
			result = MenuItem.SIGN_UP;
		}

		return result;
	}
	
	private void setUpList() {
		setUpItems();

		menuListAdapter = new MenuArrayAdapter(application.getCurrentActivity(), items);
		menuList.setAdapter(menuListAdapter);
		menuList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				MenuItem clickedItem = menuListAdapter.getItem(position);
				handlePressedItem(clickedItem);
			}

		});

		menuList.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
	}
	
	protected void handlePressedItem(MenuItem clickedItem){
		final NavigationEvent navigationEvent = new NavigationEvent(Screen.LOGIN, NavigationType.INIT);

		switch (clickedItem) {
			case SIGN_UP:
				navigationEvent.setToScreen(Screen.LOGIN);
				break;
			case TRADE:
				navigationEvent.setToScreen(Screen.TRADE);
				break;
			case BUBBLES:
				application.setCurrentScreen(Screen.BUBBLES);
				Intent intent = new Intent(getContext(), application.getSubClassForClass(BubblesActivity.class));
				startActivity(intent);
				return;
			case DEPOSIT:
				if(RegulationUtils.checkRegulationStatusAndDoNecessary(false)) {
				navigationEvent.setToScreen(Screen.DEPOSIT_MENU);
				} else {
					navigationEvent.setToScreen(application.getCurrentScreen());
				}
				break;
			case MY_ACCOUNT:
				navigationEvent.setToScreen(Screen.MY_ACCOUNT);
				break;
			case MY_OPTIONS:
				navigationEvent.setToScreen(Screen.MY_OPTIONS);
				break;
			case AGREEMENT_TERMS:
				navigationEvent.setToScreen(Screen.AGREEMENT_TERMS);
				break;
			case SUPPORT:
				navigationEvent.setToScreen(Screen.SUPPORT);
				break;
			case SETTINGS:
				navigationEvent.setToScreen(Screen.SETTINGS);
				break;
			case LOGOUT:
					application.getPopUpManager().showConfirmPopUp(
							null, 
							getString(R.string.logoutPopupQuestion),
							getString(R.string.logoutPopupPositiveButton), 
							getString(R.string.logoutPopupNegativeButton), 
							new OnConfirmPopUpButtonPressedListener() {
								@Override
								public void onPositive() {
									application.setUser(null);
									application.setUserRegulation(null);
									application.postEvent(new LoginLogoutEvent(Type.LOGOUT, null));
									navigationEvent.setToScreen(Screen.LOGIN);
									application.postEvent(navigationEvent);
								}
								@Override
								public void onNegative() {}
							}
					);				
					return;
			default:
				break;
		}

		application.postEvent(navigationEvent);
	}
	
	
	/**
	 * Set up the menu items.
	 */
	protected void setUpItems() {
		items = new ArrayList<MenuFragment.MenuItem>();

		if (!application.isLoggedIn()) {
			items.add(MenuItem.SIGN_UP);
		}

		if (!isTablet) {
			// Not shown on tablets
			items.add(MenuItem.TRADE);
		}

		items.add(MenuItem.BUBBLES);

		if (application.isLoggedIn()) {
			items.add(MenuItem.DEPOSIT);
			items.add(MenuItem.MY_ACCOUNT);
			items.add(MenuItem.MY_OPTIONS);
		}

		items.add(MenuItem.SUPPORT);
		if (application.isLoggedIn()) {
			//TODO Temporary solution:
			if(application.getUser().getSkinId() == Skin.SKIN_ETRADER_INT){
				items.add(MenuItem.AGREEMENT_TERMS);
			}
		}
		items.add(MenuItem.SETTINGS);

		if (application.isLoggedIn()) {
			items.add(MenuItem.LOGOUT);
		}
	}

	/**
	 * Menu items.
	 */
	protected enum MenuItem {
		SIGN_UP(R.string.menuItemRegisterLogin, R.drawable.register_login),
		TRADE(R.string.menuItemTrade, R.drawable.tradeMenuItem),
		BUBBLES(R.string.bubblesMenuItem, R.drawable.bubblesMenuItem),
		DEPOSIT(R.string.menuItemDeposit, R.drawable.deposit),
		MY_ACCOUNT(R.string.menuItemMyAccount, R.drawable.my_account),
		MY_OPTIONS(R.string.menuItemMyOptions, R.drawable.my_options),
		AGREEMENT_TERMS(R.string.menuItemAgreementTerms, R.drawable.menu_terms_icon),
		SUPPORT(R.string.menuItemSupport, R.drawable.support),
		SETTINGS(R.string.menuItemSettings, R.drawable.settings),
		LOGOUT(R.string.menuItemLogout, R.drawable.logout);

		private int titleId;
		private int iconId;

		MenuItem(int titleId, int iconId) {
			this.titleId = titleId;
			this.iconId = iconId;
		}

		public int getTitleId() {
			return titleId;
		}
		
		public int getIconId() {
			return iconId;
		}

	}

	/**
	 * Custom adapter for the menu list items
	 * @author Anastas Arnaudov
	 *
	 */
	private class MenuArrayAdapter extends ArrayAdapter<MenuItem> {

		private List<MenuItem> menuItems;
		private int itemHeight;
		private int itemBIGHeight;

		public MenuArrayAdapter(Context context, List<MenuItem> items) {
			super(context, R.layout.menu_item, items);
			this.menuItems = items;
			itemHeight 		= getResources().getDimensionPixelSize(R.dimen.menu_item_height);
			itemBIGHeight 	= getResources().getDimensionPixelSize(R.dimen.menu_item_big_height);
		}

		@SuppressLint("ViewHolder")
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(R.layout.menu_item, parent, false);
			MenuItem item = getItem(position);
			ImageView icon = (ImageView) rowView.findViewById(R.id.menu_item_icon);
			icon.setImageResource(item.getIconId());
			com.anyoption.android.app.widget.TextView title = (com.anyoption.android.app.widget.TextView) rowView.findViewById(R.id.menu_item_title);
			title.setText(item.getTitleId());
			if(item == MenuItem.TRADE) {
				rowView.setBackgroundResource(R.drawable.menu_item_trading);
			}

			android.widget.AbsListView.LayoutParams titleLayoutParams 	= (android.widget.AbsListView.LayoutParams) rowView.getLayoutParams();
			if(application.isLoggedIn() && (item == MenuItem.TRADE || item == MenuItem.BUBBLES)) {
				titleLayoutParams.height = itemBIGHeight;
				title.setFont(FontsUtils.Font.ROBOTO_MEDIUM);
				title.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.menu_item_big_text));
			}else{
				titleLayoutParams.height = itemHeight;
				title.setFont(FontsUtils.Font.ROBOTO_LIGHT);
				title.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.menu_item_text));
			}
			rowView.setLayoutParams(titleLayoutParams);

			return rowView;
		}
		
		@Override
		public MenuItem getItem(int position) {
			return this.menuItems.get(position);
		}
		
		@Override
		public int getCount() {
			return this.menuItems.size();
		}

		public void setItems(List<MenuItem> items) {
			this.menuItems = items;
		}
	
	}

	/**
	 * Setup the Hello user message.
	 */
	protected void setUpGreeting() {
		greetingTextView = (TextView) root.findViewById(R.id.menu_greeting_text_view);
		User user = application.getUser();
		if (user != null) {
			greetingTextView.setText(getString(R.string.menuGreetingMessage) + " " + user.getFirstName() + " " + user.getLastName());
		} else {
			greetingTextView.setText(R.string.menuNotLoggedInMessage);
		}
	}
	
}
