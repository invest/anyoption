package com.anyoption.android.app.manager;

/**
 * Created by veselin.hristozov on 6.6.2016 г..
 */
public interface OnPermissionsGrantResultListener {

    void onPermissionsGrantResult(String[] permissions, int[] grantResults);

}