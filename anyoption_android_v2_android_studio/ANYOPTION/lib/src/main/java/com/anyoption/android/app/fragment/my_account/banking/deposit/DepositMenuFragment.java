package com.anyoption.android.app.fragment.my_account.banking.deposit;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.CreditCardListView;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DepositMenuFragment extends NavigationalFragment implements OnClickListener {

	protected View rootView;

	private View domestic;
	private CreditCardListView ccListView;
	
	@Override
	protected Screenable setupScreen() {
		return Screen.DEPOSIT_MENU;
	}

	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static DepositMenuFragment newInstance(Bundle bundle){
		DepositMenuFragment depositMenuFragment = new DepositMenuFragment();
		depositMenuFragment.setArguments(bundle);
		return depositMenuFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.deposit_menu, container, false);
		ccListView = (CreditCardListView) rootView.findViewById(R.id.ccListView);
		domestic = rootView.findViewById(R.id.depositDomestic);
		User user = Application.get().getUser();

		//TODO
//		if (user.getSkinId() != Skin.SKIN_GERMAN && user.getSkinId() != Skin.SKIN_REG_DE && user.getSkinId() != Skin.SKIN_KOREAN && user.getSkinId() != Skin.SKIN_ETRADER) {
//			domestic.setVisibility(View.VISIBLE);
//		}else{
//		}
		domestic.setVisibility(View.GONE);

		domestic.setOnClickListener(this);
		rootView.findViewById(R.id.wireDeposit).setOnClickListener(this);
		rootView.findViewById(R.id.deposit_menu_new_card).setOnClickListener(this);

		setUpOtherFundingMethods();

		return rootView;
	}

	protected void setUpOtherFundingMethods() {

		//Check MONEYBOOKERS:
		if(checkMoneybookers()){
			rootView.findViewById(R.id.depositOtherFunding).setVisibility(View.VISIBLE);
			rootView.findViewById(R.id.depositMoneybookers).setVisibility(View.VISIBLE);
		}
		////////////////////
		
		//Check BAROPAY:
		if(checkBaropay()){
			rootView.findViewById(R.id.depositOtherFunding).setVisibility(View.VISIBLE);
			rootView.findViewById(R.id.depositBaropay).setVisibility(View.VISIBLE);
		}
		////////////////
		
		//Check for Direct24:
		if(checkDirect24()){
			View direct24View = rootView.findViewById(R.id.deposit_direct24_view);
			direct24View.setVisibility(View.VISIBLE);
			direct24View.setOnClickListener(this);
		}
		////////////////////
		
		//Check for GIROPAY:
		if(checkGiropay()){
			View giropayView = rootView.findViewById(R.id.deposit_giropay_view);
			giropayView.setVisibility(View.VISIBLE);
			giropayView.setOnClickListener(this);
		}
		////////////////////
		
		//Check for EPS:
		if(checkEPS()){
			View epsView = rootView.findViewById(R.id.deposit_eps_view);
			epsView.setVisibility(View.VISIBLE);
			epsView.setOnClickListener(this);
		}
		///////////////
	}

	private boolean checkMoneybookers(){
		boolean result = false;
		User user = application.getUser();
		long skinID = user.getSkinId();
		
		if (skinID != Skin.SKIN_KOREAN && skinID != Skin.SKIN_ETRADER) {
			result = true;
		}
		
		return result;
	}
	
	private boolean checkBaropay(){
		boolean result = false;
		User user = application.getUser();
		long skinID = user.getSkinId();
		long countryID = user.getCountryId();
		long currencyID = user.getCurrencyId();
		
		if (countryID == Constants.COUNTRY_SOUTH_KOREA_ID && currencyID == Constants.CURRENCY_KRW_ID && skinID == Skin.SKIN_KOREAN) {
			result = true;
		}
		
		return result;
	}
	
	private boolean checkDirect24(){
		boolean result = false;
		User user = application.getUser();
		long skinID = user.getSkinId();
		long countryID = user.getCountryId();
		long currencyID = user.getCurrencyId();
		
		//Austria, Germany, Netherlands, Switzerland 
		if(skinID != Skin.SKIN_EN_US && skinID != Skin.SKIN_ES_US && currencyID == Constants.CURRENCY_EUR_ID && (countryID == 15 || countryID == 80|| countryID == 145|| countryID == 201) && RegulationUtils.isWcApproval()){
			result = true;
		}
		
		return result;
	}
	
	private boolean checkGiropay(){
		boolean result = false;
		User user = application.getUser();
		long skinID = user.getSkinId();
		long countryID = user.getCountryId();
		long currencyID = user.getCurrencyId();
		
		//Germany:
		if(skinID != Skin.SKIN_EN_US && skinID != Skin.SKIN_ES_US && currencyID == Constants.CURRENCY_EUR_ID && countryID == 80){
			result = true;
		}
		
		return result;
	}
	
	private boolean checkEPS(){
		boolean result = false;
		User user = application.getUser();
		long skinID = user.getSkinId();
		long countryID = user.getCountryId();
		long currencyID = user.getCurrencyId();
		
		//Austria:
		if(skinID != Skin.SKIN_EN_US && skinID != Skin.SKIN_ES_US && currencyID == Constants.CURRENCY_EUR_ID && countryID == 15 && RegulationUtils.isWcApproval()){
			result = true;
		}
		
		return result;
	}

	private boolean checkNeteller(){
		boolean result = false;
		User user = application.getUser();
		long skinID = user.getSkinId();
		long countryID = user.getCountryId();
		long currencyID = user.getCurrencyId();

		List<Integer> notAcceptedCountriesList = Arrays.asList(38, 94, 1, 120, 184, 212, 2, 5, 26, 44, 46, 54, 61, 87, 106, 110, 116, 128, 134, 143, 158, 159, 168, 204, 213, 216, 222, 231, 141, 52, 49, 66, 89, 99, 100, 115, 154, 188, 196, 202, 235);

		if(/*skinID != Skin.SKIN_EN_US && skinID != Skin.SKIN_ES_US && */!notAcceptedCountriesList.contains(countryID)/* && RegulationUtils.isWcApproval()*/){
			result = true;
		}

		return result;
	}

	private boolean checkiDeal(){
		boolean result = false;
		User user = application.getUser();
		long skinID = user.getSkinId();
		long countryID = user.getCountryId();
		long currencyID = user.getCurrencyId();

		//NETHERLANDS:
		if(/*skinID != Skin.SKIN_EN_US && skinID != Skin.SKIN_ES_US && */currencyID == Constants.CURRENCY_EUR_ID && countryID == 145/* && RegulationUtils.isWcApproval()*/){
			result = true;
		}

		return result;
	}

	@Override
	public void onClick(View v) {
		NavigationEvent navigationEvent = setupNavigation(v);
		
		if (null != navigationEvent) {
			application.postEvent(navigationEvent);
		}
	}
	
	protected NavigationEvent setupNavigation(View v){
		NavigationEvent navigationEvent = null;
		if (v.getId() == R.id.wireDeposit) {
			navigationEvent = new NavigationEvent(Screen.WIRE_DEPOSIT, NavigationType.DEEP);
		}else if (v.getId() == R.id.depositDomestic) {
			navigationEvent = new NavigationEvent(Screen.DOMESTIC_DEPOSIT, NavigationType.DEEP);
		}else if (v.getId() == R.id.deposit_menu_new_card) {
			navigationEvent = new NavigationEvent(Screen.FIRST_NEW_CARD, NavigationType.DEEP);
		}else if (v.getId() == R.id.deposit_direct24_view) {
			navigationEvent = new NavigationEvent(Screen.DIRECT24_DEPOSIT, NavigationType.DEEP);
		}else if (v.getId() == R.id.deposit_giropay_view) {
			navigationEvent = new NavigationEvent(Screen.GIROPAY_DEPOSIT, NavigationType.DEEP);
		}else if (v.getId() == R.id.deposit_eps_view) {
			navigationEvent = new NavigationEvent(Screen.EPS_DEPOSIT, NavigationType.DEEP);
		}/*else if (v.getId() == R.id.deposit_neteller_view) {
			navigationEvent = new NavigationEvent(Screen.NETELLER_DEPOSIT, NavigationType.DEEP);
		}else if (v.getId() == R.id.deposit_ideal_view) {
			navigationEvent = new NavigationEvent(Screen.IDEAL_DEPOSIT, NavigationType.DEEP);
		}*/
		return navigationEvent;
	}
	
	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (!hidden) {
			ccListView.updateList();
		}
	}
}