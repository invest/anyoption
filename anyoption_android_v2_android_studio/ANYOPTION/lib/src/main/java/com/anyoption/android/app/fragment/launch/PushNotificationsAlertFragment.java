package com.anyoption.android.app.fragment.launch;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.fragment.NavigationalFragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Push Notofocations Permission Screen.
 * @author Anastas Arnaudov
 *
 */
public class PushNotificationsAlertFragment extends NavigationalFragment{

public static final String TAG = PushNotificationsAlertFragment.class.getSimpleName(); 
	
	/**
	 * Use this method to create new instance of the fragment.
	 * @return
	 */
	public static PushNotificationsAlertFragment newInstance(Bundle bundle){
		Log.d(TAG, "Created new LanguageSelectionFragment.");
		PushNotificationsAlertFragment notificationsPermissionFragment = new PushNotificationsAlertFragment();
		notificationsPermissionFragment.setArguments(bundle);
		return notificationsPermissionFragment;
	}
	
	/**
	 * Use this method to create new instance of the fragment.
	 * @return
	 */
	public static PushNotificationsAlertFragment newInstance(){
		return newInstance(new Bundle());
	}


	private Button okButton;

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.push_notification_permission_fragment_layout, container, false);
		
		okButton = (Button) rootView.findViewById(R.id.push_notifications_permission_screen_ok_button);
		okButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//TODO post
			}
		});

		return rootView;
	}

	@Override
	protected Screenable setupScreen() {
		return Screen.PUSH_NOTIFICATION_PERMISSION;
	}
	
}
