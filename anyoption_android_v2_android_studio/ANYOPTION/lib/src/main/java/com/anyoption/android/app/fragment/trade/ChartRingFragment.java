package com.anyoption.android.app.fragment.trade;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.ChartCommonEvent;
import com.anyoption.android.app.event.ChartEvent;
import com.anyoption.android.app.event.InvestmentEvent;
import com.anyoption.android.app.event.LightstreamerRegisterEvent;
import com.anyoption.android.app.event.LightstreamerUpdateEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.manager.DataBaseManager;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.popup.CancelInvestmentDialogFragment;
import com.anyoption.android.app.popup.LoginRegisterDialogfragment;
import com.anyoption.android.app.popup.LowCashBalanceDialogFragment;
import com.anyoption.android.app.popup.PredefinedAmountDialogFragment;
import com.anyoption.android.app.popup.QuestionnairePEPBlockedOkDialogFragment;
import com.anyoption.android.app.popup.QuestionnaireThresholdRestrictedOkDialogFragment;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.InvestmentUtil;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.InvestButton;
import com.anyoption.android.app.widget.TextView;
import com.anyoption.android.app.widget.chart.ChartRingView;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.service.requests.ChartDataRequest;
import com.anyoption.common.service.requests.InsertInvestmentMethodRequest;
import com.anyoption.common.service.results.ChartDataResult;
import com.anyoption.common.service.results.InvestmentMethodResult;
import com.lightstreamer.ls_client.UpdateInfo;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * @author Anastas Arnaudov
 */
public class ChartRingFragment extends ChartFragment implements View.OnClickListener {

    public static final String TAG = ChartRingFragment.class.getSimpleName();

    protected boolean isLoading;
    private BigDecimal investAmount;
    private Currency currency;

    private View investLayout;
    private InvestButton investButtonPut;
    private InvestButton investButtonCall;
    private TextView amountTextView;
    private TextView ifIncorrectTextView;
    private View waitingForExpiryLayout;
    private TextView amountLabelTextView;
    private TextView profitCallAmountTextView;
    private TextView profitPutAmountTextView;
    private TextView profitCallPercentTextView;
    private TextView profitPutPercentTextView;

    private double eventLevel;
    private double currentLevel;
    private double profitCallParam;
    private double profitPutParam;
    private double profitCallPercent;
    private double profitPutPercent;
    private double profitCallAmount;
    private double profitPutAmount;
    private DecimalFormat percentFormat;
    private View profitCallLayout;
    private View profitPutLayout;
    private View ifIncorrecrtAmountLayout;


    /**
     * Use this method to create new instance of the fragment.
     */
    public static ChartRingFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new ChartRingFragment.");
        ChartRingFragment fragment = new ChartRingFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        investAmount    = new BigDecimal(application.getDefaultInvestAmount());
        currency        = application.getCurrency();
        percentFormat   = new DecimalFormat("##.##");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        investLayout                =                rootView.findViewById(R.id.investLayout);
        investButtonPut             = (InvestButton) rootView.findViewById(R.id.investButtonPut);
        investButtonCall            = (InvestButton) rootView.findViewById(R.id.investButtonCall);
        amountLabelTextView         = (TextView)     rootView.findViewById(R.id.amountLabelTextView);
        amountTextView              = (TextView)     rootView.findViewById(R.id.amountTextView);
        ifIncorrectTextView         = (TextView)     rootView.findViewById(R.id.ifIncorrecrtAmountTextView);
        waitingForExpiryLayout      =                rootView.findViewById(R.id.waitingForExpiryLayout);
        profitCallLayout            =                rootView.findViewById(R.id.profitCallLayout);
        profitPutLayout             =                rootView.findViewById(R.id.profitPutLayout);
        profitCallAmountTextView    = (TextView)     rootView.findViewById(R.id.profitCallAmountTextView);
        profitCallPercentTextView   = (TextView)     rootView.findViewById(R.id.profitCallPercentTextView);
        profitPutAmountTextView     = (TextView)     rootView.findViewById(R.id.profitPutAmountTextView);
        profitPutPercentTextView    = (TextView)     rootView.findViewById(R.id.profitPutPercentTextView);
        ifIncorrecrtAmountLayout    =                rootView.findViewById(R.id.ifIncorrecrtAmountLayout);

        //Initial state of the buttons:
        investButtonPut.disable();
        investButtonCall.disable();

        investButtonPut.setOnClickListener(this);
        investButtonCall.setOnClickListener(this);
        amountTextView.setOnClickListener(this);

        formatInvestAmount();
        setupLoading(true);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        application.registerForEvents(this, LoginLogoutEvent.class, ChartCommonEvent.class);
    }

    @Override
    public void onDestroy() {
        application.unregisterForAllEvents(this);
        super.onDestroy();
    }

    @Override
    protected int getLayout(){
        return R.layout.chart_ring_fragment;
    }

    public void loadMarket(Market market){
        this.market = market;
        reload();
    }

    public void reload(){
        chartRequest();
    }

    protected void onPut() {
        if (RegulationUtils.checkRegulationAndPepAndThresholdStatusAndDoNecessary(false)) {
            if (investButtonPut.getState() == InvestButton.State.NORMAL) {
                invest(Investment.INVESTMENT_TYPE_DYNAMICS_SELL);
            }
        }
    }

    protected void onCall() {
        if (RegulationUtils.checkRegulationAndPepAndThresholdStatusAndDoNecessary(false)) {
            if (investButtonCall.getState() == InvestButton.State.NORMAL) {
                invest(Investment.INVESTMENT_TYPE_DYNAMICS_BUY);
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    ///////////                     REQUESTs                        ////////////////
    ////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void chartRequest() {
        Log.d(TAG, "chartRequest");
        if (isChartRequested) {
            // Cancel all previous requests.
            communicationManager.cancelAllRequests(this);
        }

        ChartDataRequest request = new ChartDataRequest();

        if(market != null) {
            request.setMarketId(market.getId());
            request.setOpportunityTypeId(Opportunity.TYPE_DYNAMICS);
        } else {
            request.setBox(FEATURED_MARKET_BOX);
            request.setOpportunityTypeId(Opportunity.TYPE_REGULAR);
        }

        isChartRequested = communicationManager.requestService(this, "chartCallback", Constants.SERVICE_GET_CHART_DATA_COMMON, request, ChartDataResult.class, false, false);

        application.postEvent(new ChartEvent(market != null ? market.getId() : 0, ChartEvent.Type.DATA_REQUESTED));

        setupLoading(true);
    }

    @Override
    public void chartCallback(Object resultObj) {
        Log.d(TAG, "chartCallback " + resultObj);
        isChartRequested = false;

        ChartDataResult result = (ChartDataResult) resultObj;
        ChartCommonEvent.Type resultType;
        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            if (market.getId() != result.getMarketId()) {
                // On tablets if the market is changed before chart data for previous market is
                // received, will be received all chart data results one by one. This check will
                // prevent using the wrong chart data.
                Log.w(TAG, "chartCallback -> Data for OTHER MARKET!");
                return;
            }

            Log.d(TAG, "chartCallback Success -> market: " + market.getDisplayName());
            TimeUtils.fixDates(result);
            opportunityState = 0;

            // Update chart info here
            if (getChartView() != null) {
                getChartView().setChartDataCommon(market, result);
            }

            // The user object will be in the result if he is logged in.
            if (application.isLoggedIn() && result.getUser() != null) {
                application.setUser(result.getUser());
            }

            resultType = ChartCommonEvent.Type.DATA_REQUEST_SUCCESS;
        } else {
            if (resultObj != null) {
                Log.e(TAG, "chartCallback error -> ErrorCode -> " + result.getErrorCode());
            } else {
                Log.e(TAG, "chartCallback error -> result is NULL");
            }

            resultType = ChartCommonEvent.Type.DATA_REQUEST_FAIL;
        }

        application.postEvent(new ChartCommonEvent(market != null ? market.getId() : 0, resultType, result));
    }

    protected void investLogout(){
        application.getFragmentManager().showDialogFragment(LoginRegisterDialogfragment.class, null);
    }

    private void invest(int investmentType) {
        if (!application.isLoggedIn()) {
            investLogout();
            return;
        }

        Display display = ((WindowManager) application.getCurrentActivity().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int orientation = display.getRotation();
        boolean isFromGraph = orientation == Surface.ROTATION_90 || orientation == Surface.ROTATION_270;

        double price = investmentType == Investment.INVESTMENT_TYPE_DYNAMICS_BUY ? profitCallParam : profitPutParam;

        InsertInvestmentMethodRequest request = new InsertInvestmentMethodRequest();
        request.setPageLevel(currentLevel);
        request.setChoice(investmentType);
        request.setOpportunityId(opportunityId);
        request.setFromGraph(isFromGraph);
        request.setRequestAmount(investAmount.doubleValue());
        request.setPrice(price);
        request.setDev2Second(false);

        Log.d(TAG, "Investment price = " + price);

        boolean isRequestOK = communicationManager.requestService(this, "investCallBack",Constants.SERVICE_INSERT_DYNAMICS_INVESTMENT, request, InvestmentMethodResult.class, false, false);
        application.postEvent(new InvestmentEvent(market.getId(), InvestmentEvent.Type.INVEST_REQUESTED));

        // Setup Invest Buttons:
        if (isRequestOK) {
            if (investmentType == Investment.INVESTMENT_TYPE_DYNAMICS_BUY) {
                investButtonCall.startLoading();
                investButtonPut.disable();
            } else if (investmentType == Investment.INVESTMENT_TYPE_DYNAMICS_SELL) {
                investButtonPut.startLoading();
                investButtonCall.disable();
            }
        }
        ///////////////
    }

    public void investCallBack(Object resultObj) {

        InvestmentMethodResult result = (InvestmentMethodResult) resultObj;

        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {

            final Investment investment = result.getInvestment();
            investment.setEventLevel(eventLevel);
            InvestmentUtil.fixDate(investment);
            //Save the Market as one of the user favorites:
            application.getDataBaseManager().saveOrUpdateMarket(market, DataBaseManager.MARKETS_STATE_FAVORITE);

            if (result.getUser() != null) {
                application.setUser(result.getUser());
            }

            if (result.getUserRegulation() != null) {
                application.setUserRegulation(result.getUserRegulation());
            }

            boolean isShowCancelInvestmentPopup = application.isShowCancelInvestmentPopup();
            if(isShowCancelInvestmentPopup){
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.EXTRA_INVESTMENT, investment);
                bundle.putLong(Constants.EXTRA_CANCEL_SECONDS, result.getCancelSeconds()*1000);
                CancelInvestmentDialogFragment cancelInvestmentDialogFragment = (CancelInvestmentDialogFragment) application.getFragmentManager().showDialogFragment(CancelInvestmentDialogFragment.class, bundle);
                cancelInvestmentDialogFragment.setCancelInvestmentListener(new CancelInvestmentDialogFragment.CancelInvestmentListener() {
                    @Override
                    public void onFinished(boolean isCanceled) {
                        if(isCanceled){
                            // Setup Buttons:
                            investButtonCall.stopLoading(false);
                            investButtonPut.stopLoading(false);
                            ///////////////
                        }else{
                            application.postEvent(new InvestmentEvent(market.getId(), InvestmentEvent.Type.INVEST_SUCCESS, investment));
                            application.getNotificationManager().showNotificationInvestOK(investment);

                            // Setup Buttons:
                            investButtonCall.stopLoading(true);
                            investButtonPut.stopLoading(true);
                            ///////////////
                        }
                    }
                });
            }else{
                application.postEvent(new InvestmentEvent(market.getId(), InvestmentEvent.Type.INVEST_SUCCESS, result.getInvestment()));
                application.getNotificationManager().showNotificationInvestOK(result.getInvestment());

                // Setup Buttons:
                investButtonCall.stopLoading(true);
                investButtonPut.stopLoading(true);
                ///////////////
            }

        } else {
            handleInvestFail(result);
        }

    }

    private void finishInvestD2(Investment investment){
        Log.d(TAG, "Finishing the Deviation2 check investment...");
        Display display = ((WindowManager) application.getCurrentActivity().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int orientation = display.getRotation();

        boolean isFromGraph = orientation == Surface.ROTATION_90 || orientation == Surface.ROTATION_270;

        InsertInvestmentMethodRequest request = new InsertInvestmentMethodRequest();
        request.setPageLevel(investment.getCurrentLevel());
        request.setPageOddsLose((float) investment.getOddsLose());
        request.setPageOddsWin((float) investment.getOddsWin());
        request.setChoice((int) investment.getTypeId());
        request.setOpportunityId(investment.getOpportunityId());
        request.setFromGraph(isFromGraph);
        request.setRequestAmount(AmountUtil.getDoubleAmount(investment.getAmount()));
        request.setDev2Second(true);

        communicationManager.requestService(this, "finishInvestD2CallBack",Constants.SERVICE_INSERT_INVESTMENT, request, InvestmentMethodResult.class, false, false);

    }

    public void finishInvestD2CallBack(Object resultObj){
        Log.d(TAG, "finishInvestD2CallBack");

        InvestmentMethodResult result = (InvestmentMethodResult) resultObj;

        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {

            final Investment investment = result.getInvestment();
            InvestmentUtil.fixDate(investment);

            if (result.getUser() != null) {
                application.setUser(result.getUser());
            }

            if (result.getUserRegulation() != null) {
                application.setUserRegulation(result.getUserRegulation());
            }

            application.postEvent(new InvestmentEvent(market.getId(), InvestmentEvent.Type.INVEST_SUCCESS, result.getInvestment()));
            application.getNotificationManager().showNotificationInvestOK(result.getInvestment());
        } else {
            handleInvestFail(result);
        }

    }

    private void handleInvestFail(InvestmentMethodResult result){
        Log.d(TAG, "handleInvestFail");
        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_NO_CASH_FOR_NIOU) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.EXTRA_CASH_BALANCE, AmountUtil.getUserFormattedAmount(Long.valueOf(result.getUserCashBalance())));
            Application.get().getFragmentManager().showDialogFragment(LowCashBalanceDialogFragment.class, bundle);
        } else if(result != null && result.getErrorCode() == Constants.ERROR_CODE_REGULATION_USER_IS_TRESHOLD_BLOCK) {
            Application.get().getFragmentManager().showDialogFragment(QuestionnaireThresholdRestrictedOkDialogFragment.class, null);
        } else {
            application.getEventBus().post(new InvestmentEvent(market.getId(), InvestmentEvent.Type.INVEST_FAIL));

            ErrorUtils.handleResultError(result);

            if (result != null) {
                Log.e(TAG, "Could not invest. ErrorCode: " + result.getErrorCode());
            } else {
                Log.e(TAG, "Could not invest! result == null");
            }
        }

        // Setup Buttons:
        investButtonCall.stopLoading(false);
        investButtonPut.stopLoading(false);
        ///////////////
    }

    ////////////////////////////////////////////////////////////////////////////////


    ////////////////////////////////////////////////////////////////////////////////
    ///////////                     EVENTs                          ////////////////
    ////////////////////////////////////////////////////////////////////////////////

    public void onEventMainThread(ChartCommonEvent event) {
        if (event.getMarketId() != market.getId()) {
            return;
        }

        if (event.getType() == ChartCommonEvent.Type.DATA_REQUEST_SUCCESS) {
            ChartDataResult data = event.getChartData();
            opportunityId = data.getOpportunityId();
            opportunityState = 0;
            eventLevel = data.getEventLevel();
        }
    }

    public void onEventMainThread(LoginLogoutEvent event) {
        currency = application.getCurrency();
        onInvestAmountChanged();
    }

    public void onEvent(LightstreamerUpdateEvent event) {
        if (market != null && event.getMarketId() == market.getId()) {
            if(isLoading){
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        setupLoading(false);
                    }
                });
            }
            updateItem(event.getUpdateInfo());
        }
    }

    public void onEvent(LightstreamerRegisterEvent event) {
      //Ignore
    }
    ////////////////////////////////////////////////////////////////////////////////


    ////////////////////////////////////////////////////////////////////////////////
    ///////////                     LIGHSTREAMER                    ////////////////
    ////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void updateItem(UpdateInfo update) {

        int stateBeforeUpdate   = opportunityState;
        int newOpportunityState = Integer.valueOf(update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_STATE));
        if (opportunityState == 0 || (update.isValueChanged(LightstreamerManager.COLUMN_DYNAMICS_STATE) && stateBeforeUpdate < newOpportunityState)) {
            opportunityState = newOpportunityState;
            handler.obtainMessage(MESSAGE_STATE_CHANGED).sendToTarget();

            if (opportunityState == Opportunity.STATE_CLOSED) {
                handler.obtainMessage(MESSAGE_SHOW_CLOSING_LEVEL, update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_LEVEL).replaceAll(",", "")).sendToTarget();
            } else {
                handler.obtainMessage(MESSAGE_HIDE_CLOSING_LEVEL).sendToTarget();
            }

        }

        currentLevel        = Double.parseDouble(update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_LEVEL).replaceAll(",", ""));

        profitCallParam     = Double.valueOf(update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_PROFIT_CALL));
        profitPutParam      = Double.valueOf(update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_PROFIT_PUT));
		profitCallPercent   = 100 * (100 - profitCallParam) / profitCallParam;
        profitPutPercent    = 100 * profitPutParam / (100 - profitPutParam);
        profitCallAmount    = investAmount.doubleValue() * profitCallPercent / 100;
        profitPutAmount     = investAmount.doubleValue() * profitPutPercent  / 100;

        if (opportunityState <= Opportunity.STATE_CLOSED || opportunityState != stateBeforeUpdate) {
            chartView.updateItem(update.getItemPos(), update.getItemName(), update);
        }

        handler.obtainMessage(MESSAGE_UPDATE_PROFIT).sendToTarget();
    }

    ////////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean handleMessage(Message msg) {
        super.handleMessage(msg);
        switch (msg.what) {
            case MESSAGE_STATE_CHANGED:
                Log.d(TAG, "State Changed. State = " + LightstreamerManager.getOpportunityStateDescription(opportunityState));

                //Open:
                if (opportunityState == Opportunity.STATE_OPENED || opportunityState == Opportunity.STATE_LAST_10_MIN) {
                    investButtonPut.enable();
                    investButtonCall.enable();
                    investLayout.setVisibility(View.VISIBLE);
                    amountLabelTextView.setVisibility(View.VISIBLE);
                    amountTextView.setVisibility(View.VISIBLE);
                    ifIncorrecrtAmountLayout.setVisibility(View.VISIBLE);
                    profitCallLayout.setVisibility(View.VISIBLE);
                    profitPutLayout.setVisibility(View.VISIBLE);
                    waitingForExpiryLayout.setVisibility(View.GONE);
                    getChartView().setVisibility(View.VISIBLE);
                }

                //Waiting for expiry:
                else if (opportunityState == Opportunity.STATE_CLOSING_1_MIN || opportunityState == Opportunity.STATE_CLOSING) {
                    investButtonPut.disable();
                    investButtonCall.disable();
                    investLayout.setVisibility(View.VISIBLE);
                    amountLabelTextView.setVisibility(View.GONE);
                    amountTextView.setVisibility(View.GONE);
                    ifIncorrecrtAmountLayout.setVisibility(View.GONE);
                    waitingForExpiryLayout.setVisibility(View.VISIBLE);
                    profitCallLayout.setVisibility(View.INVISIBLE);
                    profitPutLayout.setVisibility(View.INVISIBLE);
                    getChartView().setVisibility(View.VISIBLE);
                }

                //Expired:
                else if(opportunityState == Opportunity.STATE_CLOSED){
                    investLayout.setVisibility(View.GONE);
                    getChartView().setVisibility(View.GONE);
                }

                //Unavailable:
                else if(opportunityState == Opportunity.STATE_CREATED || opportunityState == Opportunity.STATE_SUSPENDED) {
                    investLayout.setVisibility(View.GONE);
                    getChartView().setVisibility(View.GONE);
                }
                break;
            case MESSAGE_UPDATE_PROFIT:
                updateProfits();
                break;
            case MESSAGE_SHOW_CLOSING_LEVEL:
                String level = (String) msg.obj;
                showClosingLevelInfo(true, level);
                break;
            case MESSAGE_HIDE_CLOSING_LEVEL:
                showClosingLevelInfo(false, null);
                break;
        }

        return false;
    }

    @Override
    protected ChartRingView getChartView(){
        if(chartView == null){
            chartView = new ChartRingView(getContext());
        }

        return (ChartRingView) chartView;
    }

    @Override
    protected void setupZoomButtons() {
        //Ignore
    }

    @Override
    protected void showZoomButtons(boolean show) {
        //Ignore
    }

    private void setupLoading(boolean isLoading){
        this.isLoading = isLoading;
        handler.post(new Runnable() {
            @Override
            public void run() {
                if(loadingView != null){
                    if(ChartRingFragment.this.isLoading){
                        loadingView.setVisibility(View.VISIBLE);
                    }else{
                        loadingView.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v == investButtonPut){
            onPut();
        }else if(v == investButtonCall){
            onCall();
        }else if(v == amountTextView){
            //Show the Amount Popup:
            PredefinedAmountDialogFragment predefinedAmountFragment = PredefinedAmountDialogFragment.newInstance(investAmount.doubleValue());
            predefinedAmountFragment.setOnAmountChangeListener(new PredefinedAmountDialogFragment.OnAmountChangeListener() {
                @Override
                public void onAmountChanged(double newAmount) {
                    investAmount = new BigDecimal(newAmount);
                    onInvestAmountChanged();
                }
            });
            predefinedAmountFragment.setOnDismissListener(new BaseDialogFragment.OnDialogDismissListener() {
                @Override
                public void onDismiss() {
                    amountTextView.setEnabled(true);
                }
            });
            amountTextView.setEnabled(false);
            application.getFragmentManager().showDialogFragment(predefinedAmountFragment);
        }
    }

    private void onInvestAmountChanged(){
        formatInvestAmount();
        updateProfits();
    }

    private void updateProfits(){
        AmountUtil.setTextViewAmount(profitCallAmountTextView, profitCallAmount, currency.getSymbol(), currency.getIsLeftSymbolBool(), currency.getDecimalPointDigits());
        AmountUtil.setTextViewAmount(profitPutAmountTextView, profitPutAmount, currency.getSymbol(), currency.getIsLeftSymbolBool(), currency.getDecimalPointDigits());
        String profitText = getResources().getString(R.string.profit).toLowerCase();
        profitCallPercentTextView.setText("(" + percentFormat.format(profitCallPercent) + "% " + profitText + ")");
        profitPutPercentTextView.setText("(" + percentFormat.format(profitPutPercent) + "% " + profitText + ")");

        //Set up the Buttons:
        if(profitCallPercent > 0 && opportunityState == Opportunity.STATE_OPENED){
            investButtonCall.enable();
        }else{
            investButtonCall.disable();
        }
        if(profitPutPercent > 0 && opportunityState == Opportunity.STATE_OPENED){
            investButtonPut.enable();
        }else{
            investButtonPut.disable();
        }
    }

    private void formatInvestAmount(){
        AmountUtil.setTextViewAmount(amountTextView, investAmount.doubleValue(),currency.getSymbol(), currency.getIsLeftSymbolBool(),currency.getDecimalPointDigits());
        ifIncorrectTextView.setText(AmountUtil.getFormattedAmount(0, currency));
    }

}
