package com.anyoption.android.app.util;

import com.anyoption.android.app.Application;
import com.anyoption.android.R;
import com.anyoption.common.beans.base.CreditCardType;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.Skin;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.View;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class DrawableUtils {
	
	private static final String TAG = DrawableUtils.class.getSimpleName();

	/**
	 * Returns the image for the given name.
	 * @param name
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static Drawable getImage(String name) {
		Context context = Application.get();
		Drawable drawable = null;
		try {
			//Escape names that are reserved words in Android
			if(name.equals("do")){
				name = name + "_";
			}
			///////////////////////////////////////////////
			
			int id = context.getResources().getIdentifier(name, "drawable", context.getPackageName());
			if (id != 0) {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
					drawable = context.getDrawable(id);
				} else {
					drawable = context.getResources().getDrawable(id);
				}
			}
		} catch (Exception e) {
			Log.d(TAG, "Could NOT get image for the name = " + name);
		}
	    return drawable;
	}

	/**
	 * Returns the image for the given name.
	 * @param name
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static Drawable getImage(int resID) {
		Context context = Application.get();
		Drawable drawable = null;
		try {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				drawable = context.getDrawable(resID);
			} else {
				drawable = context.getResources().getDrawable(resID);
			}
		} catch (Exception e) {
			Log.d(TAG, "Could NOT get image for the id = " + resID);
		}
	    return drawable;
	}

	/**
	 * Returns the image res id for the given name.
	 * @param name
	 * @return 0 if no res was found.
	 */
	public static int getImageResID(String name) {
		Context context = Application.get();
		int result = 0;
		try {
			//Escape names that are reserved words in Android
			if(name.equals("do")){
				name = name + "_";
			}
			///////////////////////////////////////////////
			
			result = context.getResources().getIdentifier(name, "drawable", context.getPackageName());
	
		} catch (Exception e) {
			Log.d(TAG, "NO re get image for the name = " + name);
		}
	    return result;
	}

	@SuppressWarnings("deprecation")
	public static Drawable getImageByCCType(int type) {
		Context context = Application.get();
		int imgCardId = 0;
		switch (type) {
			case CreditCardType.CC_TYPE_VISA:
				imgCardId = R.drawable.visa_icon;
				break;
			case CreditCardType.CC_TYPE_MASTERCARD:
				imgCardId = R.drawable.master_icon;
				break;
			case CreditCardType.CC_TYPE_AMEX:
				if(Application.get().getUser().getSkinId() != Skin.SKIN_ETRADER){
					imgCardId = 0;//The AMEX is removed.
				}
				break;
			case CreditCardType.CC_TYPE_DINERS:
				imgCardId = R.drawable.diners_icon;
				break;
			case CreditCardType.CC_TYPE_ISRACARD:
				imgCardId = R.drawable.isracard_icon;
				break;
			case CreditCardType.CC_TYPE_MAESTRO:
				imgCardId = R.drawable.maestro_icon;
				break;
			default:
				break;
		}
		
		if(imgCardId == 0) {
			return null;
		} else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			return context.getDrawable(imgCardId);
		} else {
			return context.getResources().getDrawable(imgCardId);
		}
	}

	/**
	 * Use this method to set the background of the view using the correct method depending on OS
	 * version.<br/>
	 * NOTE: The deprecated method is used only for older versions.
	 * 
	 * @param view
	 * @param drawable
	 */
	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	public static void setBackground(View view, Drawable drawable) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			view.setBackground(drawable);
		} else {
			view.setBackgroundDrawable(drawable);
		}
	}

	/**
	 * Gets the background of the Chart that is showing the given Market.
	 * <p>If no BG is found for the specific Market - then it gets the BG corresponding for the Market Exchange ID,
	 * <p>If no BG is found - then it gets the BG for the Market Group.
	 * <p>If no BG is found or market == null - then it gets the default BG.
	 * @param market
	 * @return
	 */
	public static Drawable getChartBackground(Market market){
		if(market == null){
			return getDefaultAssetBg();
		}
		
		Drawable icon = getImage("mbg" + market.getId());
		
		if(icon == null && market.getGroupId() == null){
			return getDefaultAssetBg();
		}

		//Get the image based on the exchanged id (but only if the group is "Indices")
		if(icon == null && market.getGroupId() == 2){
			icon = getImage("ebg" + market.getExchangeId());
		}
		if(icon == null){
			icon = getImage("gbg" + market.getGroupId());
		}
		if (icon == null) {
			return getDefaultAssetBg();
		}
		
		return icon;
	}
	
	@SuppressWarnings("deprecation")
	private static Drawable getDefaultAssetBg() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			return Application.get().getDrawable(R.drawable.default_asset_bg);
		} else {
			return Application.get().getResources().getDrawable(R.drawable.default_asset_bg);
		}
	}

	/**
	 * Gets the corresponding Market Icon.
	 * <p>If no icon is found - then it gets the group icon.
	 * <p>If no group icon is found - then it returns default icon.
	 * @param market
	 * @return The res id of the image.
	 */
	public static int getMarketIconResID(Market market){
		if(market == null){
			// TODO We need default icon!
			return R.drawable.ic_launcher;
		}
		int resID = getImageResID("micn" + market.getId());
		
		if(resID == 0){
			resID = getImageResID("gicn" + market.getGroupId());
		}
		if(resID == 0){
			resID = R.drawable.ic_launcher;//TODO We need default icon!
		}
		
		return resID;
	}

	/**
	 * Gets the corresponding Market Icon for Market id.
	 * <p>If no icon is found - then it gets the group icon.
	 * <p>If no group icon is found - then it returns default icon.
	 * @param market
	 * @return
	 */
	public static Drawable getMarketIconForId(long marketId){
		Drawable icon = getImage("micn" + marketId);
	
		if (icon == null) {
			// TODO We need default icon!
			/*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				icon = Application.get().getDrawable(R.drawable.ic_launcher);
			} else {
				icon = Application.get().getResources().getDrawable(R.drawable.ic_launcher);
			}*/
			icon = DrawableUtils.getDrawable(R.drawable.ic_launcher);
		}
		
		return icon;
	}

	/**
	 * Gets the corresponding Market Icon.
	 * <p>If no icon is found - then it gets the group icon.
	 * <p>If no group icon is found - then it returns default icon.
	 * @param market
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static Drawable getMarketIcon(Market market){
		if(market == null){
			// TODO We need default icon!
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				return Application.get().getDrawable(R.drawable.ic_launcher);
			} else {
				return Application.get().getResources().getDrawable(R.drawable.ic_launcher);
			}
		}
		
		Drawable icon = getImage("micn" + market.getId());
	
		if(icon == null){
			icon = getImage("gicn" + market.getGroupId());
		}
		if (icon == null) {
			// TODO We need default icon!
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				icon = Application.get().getDrawable(R.drawable.ic_launcher);
			} else {
				icon = Application.get().getResources().getDrawable(R.drawable.ic_launcher);
			}
		}
		
		return icon;
	}

	public static int[] getDrawableSize(int resId){
		int[] result = new int[2];
		try {
			BitmapDrawable bd = (BitmapDrawable) DrawableUtils.getDrawable(resId);
			result[0] = bd.getBitmap().getWidth();
			result[1] = bd.getBitmap().getHeight();
		} catch (Exception e) {
			Log.e(TAG, "getDrawableSize()", e);
		}
		return result;
	}

	public static Drawable getDrawable(int resId){
		if(Utils.getDeviceOSVersionCode() >= 21) {
			return Application.get().getResources().getDrawable(resId, Application.get().getTheme());
		} else {
			return Application.get().getResources().getDrawable(resId);
		}
	}

}
