package com.anyoption.android.app.util;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.ExpiryFormulaCalculation;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.MarketAssetIndex;
import com.anyoption.common.beans.base.MarketAssetIndexInfo;

import java.util.Date;

/**
 * Utils methods related with the Markets.
 * @author Anastas Arnaudov
 */
public class MarketsUtils {

    public static final long EXPIRY_TYPE_HOURLY     = 1;
    public static final long EXPIRY_TYPE_DAILY      = 2;
    public static final long EXPIRY_TYPE_LONG_TERM  = 3;

    /**
     * Gets the Trading Days of the Week and The Hours interval for the given {@link MarketAssetIndex}.
     */
    public static String getTradingTime(MarketAssetIndex marketAssetIndex){
        MarketAssetIndexInfo marketAssetIndexInfo = marketAssetIndex.getMarketAssetIndexInfo();
        Date startDate = new Date(marketAssetIndexInfo.getStartTime().getTime());
        Date endDate   = new Date(marketAssetIndexInfo.getEndTime().getTime());
        int gmtDayDeviation = TimeUtils.getDayDeviationFromGMT(startDate);
        TimeUtils.fixTime(startDate);
        TimeUtils.fixTime(endDate);

        //Example Mask : "0111110" -> Mon.-Fri. are active, Sat. and Sun. are not.
        String mask = marketAssetIndexInfo.getTradingDays();

        int firstIndex = gmtDayDeviation + ((mask.contains("1")) ? mask.indexOf("1") : 0);
        if(firstIndex < 0){
            firstIndex = 6;
        }else if(firstIndex > 6){
            firstIndex = 0;
        }
        int lastIndex  = gmtDayDeviation + ((mask.contains("1")) ? mask.lastIndexOf("1") : 0);
        if(lastIndex < 0){
            lastIndex = 6;
        }else if(lastIndex > 6){
            lastIndex = 0;
        }
        String firstActiveDay = MarketsUtils.getDayOfWeekForIndex(firstIndex);
        String lastActiveDay  = MarketsUtils.getDayOfWeekForIndex(lastIndex);

        String startHour = TimeUtils.formatTimeToHoursMinutes(startDate.getTime());
        String endHour   = TimeUtils.formatTimeToHoursMinutes(endDate.getTime());

        //Format the text:
        String result = firstActiveDay + " - " + lastActiveDay + "   " + startHour + " - " + endHour;

        return result;
    }

    private static String getDayOfWeekForIndex(int index){
        Application application = Application.get();
        String day = null;
        switch (index){
            case 0:
                day = application.getString(R.string.dayOfWeekShortSunday);
                break;
            case 1:
                day = application.getString(R.string.dayOfWeekShortMonday);
                break;
            case 2:
                day = application.getString(R.string.dayOfWeekShortTuesday);
                break;
            case 3:
                day = application.getString(R.string.dayOfWeekShortWednesday);
                break;
            case 4:
                day = application.getString(R.string.dayOfWeekShortThursday);
                break;
            case 5:
                day = application.getString(R.string.dayOfWeekShortFriday);
                break;
            case 6:
                day = application.getString(R.string.dayOfWeekShortSaturday);
                break;
        }

        return day;
    }

    /**
     * Gets the {@link ExpiryFormulaCalculation}  for the given {@link MarketAssetIndex} and the scheduled type.
     */
    public static ExpiryFormulaCalculation getExpiryFormulaCalculation(MarketAssetIndex marketAssetIndex, long expiryTypeId){

        for(ExpiryFormulaCalculation expiryFormulaCalculation : marketAssetIndex.getMarketAssetIndexInfo().getExpiryFormulaCalculations()){
            if(expiryFormulaCalculation.getExpiryTypeId() == expiryTypeId){
                return expiryFormulaCalculation;
            }
        }

        return null;
    }

    /**
     * Check if the given {@link Market} is Binary type.
     */
    public static boolean isBinary(Market market){
        return market.getProductTypeId() == Constants.PRODUCT_TYPE_ID_BINARY;
    }

    /**
     * Check if the given {@link Market} is Option+ type.
     */
    public static boolean isOptionPlus(Market market){
        return market.getProductTypeId() == Constants.PRODUCT_TYPE_ID_OPTION_PLUS;
    }

    /**
     * Check if the given {@link Market} is Dynamics type.
     */
    public static boolean isDynamics(Market market){
        return market.getProductTypeId() == Constants.PRODUCT_TYPE_ID_DYNAMICS;
    }

}
