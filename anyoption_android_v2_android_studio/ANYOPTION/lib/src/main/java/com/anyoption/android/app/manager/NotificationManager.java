package com.anyoption.android.app.manager;

import com.anyoption.android.app.Application;
import com.anyoption.android.R;
import com.anyoption.android.app.widget.notification.NotificationDepositOK;
import com.anyoption.android.app.widget.notification.NotificationError;
import com.anyoption.android.app.widget.notification.NotificationInvestOK;
import com.anyoption.android.app.widget.notification.NotificationNoInternet;
import com.anyoption.android.app.widget.notification.NotificationRollForward;
import com.anyoption.android.app.widget.notification.NotificationShoppingBag;
import com.anyoption.android.app.widget.notification.NotificationSuccess;
import com.anyoption.android.app.widget.notification.NotificationTakeProfit;
import com.anyoption.common.beans.base.Investment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.PixelFormat;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

/**
 * Notification Manager that manages the notification that will be displayed to the user.
 * @author Anastas Arnaudov
 *
 */
public class NotificationManager {

	public static final String TAG = NotificationManager.class.getSimpleName();
	private static final long TIME_TO_LIVE = 5000L;

	protected static LinearLayout notificationsContainer;
	protected android.view.WindowManager.LayoutParams notificationsContainerLayoutParams;
	private WindowManager manager;
	protected Application application;
	private CountDownTimer timer;

	
	/**
	 * Create new {@link NotificationManager}.
	 * @param application
	 */
	public NotificationManager(Application application){
		Log.d(TAG, "On Create");
		this.application = application;
	}
	
	protected void showNotification(final View notification){
		application.getCurrentActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				setupNotificationsContainer();
				addNotification(notification);
			}
		});
	}

	//*************************************************************************************
	//****************						PUBLIC Methods					***************
	//*************************************************************************************
	/**
	 * Displays No Internet Notification in the Notification bar.
	 */
	public void showNotificationNoInternet(){
		showNotification(new NotificationNoInternet(application));
	}
	
	/**
	 * Displays Error Notification in the Notification bar.
	 * @param message The message
	 */
	public void showNotificationError(String message){
		showNotification(new NotificationError(message));
	}
	
	/**
	 * Displays Success Notification in the Notification bar.
	 * @param message The message
	 */
	public void showNotificationSuccess(String message){
		showNotification(new NotificationSuccess(message));
	}
	
	/**
	 * Display Expired Options Notification in the notification bar.
	 * @param expiredOptionsCount The number of the expired options
	 * @param marketName the Asset name for the options.
	 * @param profitAmount The profit (if there is one).
	 * @param isWinning Flag indicating whether the money returned to user's balance are more than the invested money for the current market (opportunity); 
	 */
	public void showNotificationShoppingBag(int expiredOptionsCount, String marketName, long profitAmount, boolean isWinning){
		showNotification(new NotificationShoppingBag(expiredOptionsCount, marketName, profitAmount, isWinning));
	}
	
	/**
	 *  Display Take Profit Notification in the notification bar.
	 * @param closeTime
	 */
	public void showNotificationTakeProfit(String closeTime){
		showNotification(new NotificationTakeProfit(closeTime));
	}
	
	/**
	 *  Display Roll Forward Notification in the notification bar.
	 */
	public void showNotificationRollForward(){
		showNotification(new NotificationRollForward());
	}
	
	/**
	 * Display "Successful Investment" Notification in the notification bar.
	 * The notification has a "Receipt" button which opens the "Invest Receipt" POPUP.
	 * @param investment
	 */
	public void showNotificationInvestOK(Investment investment){
		showNotification(new NotificationInvestOK(investment));
	}

	/**
	 * Display DEPOSIT Successful Notification in the notification bar.
	 */
	public void showNotificationDepositOK(String amount, String date, long id){
		showNotification(new NotificationDepositOK(amount, date, id));
	}
	
	//*************************************************************************************
	//*************************************************************************************
	//*************************************************************************************
	
	/**
	 * Prepares the notifications container (if needed : if it is not created) for the layout view that will hold each notification.
	 */
	@SuppressLint("InflateParams")
	protected void setupNotificationsContainer(){

		if(manager == null){
			manager = (WindowManager) application.getSystemService(Context.WINDOW_SERVICE);
			notificationsContainerLayoutParams = new WindowManager.LayoutParams(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
					WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL|WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH|WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
					PixelFormat.TRANSLUCENT);
			notificationsContainerLayoutParams.gravity = Gravity.TOP;
			notificationsContainerLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
			notificationsContainerLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
			notificationsContainerLayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
			notificationsContainerLayoutParams.alpha = 1.0f;
			notificationsContainerLayoutParams.packageName = application.getPackageName();
			notificationsContainerLayoutParams.buttonBrightness = 1f;
			notificationsContainerLayoutParams.windowAnimations = android.R.style.Animation_Dialog;
			
		}
		
		if(notificationsContainerLayoutParams == null){
			notificationsContainerLayoutParams = new WindowManager.LayoutParams(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
					WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL|WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH|WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
					PixelFormat.TRANSLUCENT);
			notificationsContainerLayoutParams.gravity = Gravity.TOP;
			notificationsContainerLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
			notificationsContainerLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
			notificationsContainerLayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
			notificationsContainerLayoutParams.alpha = 1.0f;
			notificationsContainerLayoutParams.packageName = application.getPackageName();
			notificationsContainerLayoutParams.buttonBrightness = 1f;
			notificationsContainerLayoutParams.windowAnimations = android.R.style.Animation_Dialog;
		}
		
		if(notificationsContainer == null){
			Activity currentActivity = application.getCurrentActivity();
			LayoutInflater inflater = currentActivity.getLayoutInflater();
			notificationsContainer = (LinearLayout) inflater.inflate(R.layout.notifications_container_layout, null);
		}		
	}

	/**
	 * Sets up the visibility of the container that holds the Notifications to VISIBLE.
	 */
	public void showAllNotifications(){
		application.getCurrentActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(notificationsContainer != null){
					notificationsContainer.setVisibility(View.VISIBLE);
					try {
						manager.updateViewLayout(notificationsContainer, notificationsContainerLayoutParams);
					} catch (Exception e) {}
				}				
			}
		});
	}
	
	/**
	 * Hides all Notifications. The method hides the container that holds all notifications, so if a pending one arrives later it won't be visible.
	 */
	public void hideAllNotifications(){
		application.getCurrentActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {				
				if(notificationsContainer != null){
					notificationsContainer.setVisibility(View.INVISIBLE);
					try {
						manager.updateViewLayout(notificationsContainer, notificationsContainerLayoutParams);
					} catch (Exception e) {}	
				}
			}
		});
	}
	
	/**
	 * Remove all active (visible) and pending notifications.
	 */
	public void clearAllNotifications() {
		application.getCurrentActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(notificationsContainer != null){
					try {
						manager.removeView(notificationsContainer);
					} catch (Exception e) {}	
				}
				notificationsContainer = null;				
			}
		});
	}

	protected void addNotification(final View notification) {
		application.getCurrentActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				setupNotificationsContainer();
				if(notificationsContainer != null){
					removeAllNotifications();
					notificationsContainer.addView(notification, 0);
					try {
						manager.removeView(notificationsContainer);
					} catch (Exception e) {}
					try {
						manager.addView(notificationsContainer, notificationsContainerLayoutParams);
						startTimer(notification);
					} catch (Exception e) {}
				}			
			}
		});
	}
	
	protected void removeNotification(final View notification) {
		application.getCurrentActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				stopTimer();
				setupNotificationsContainer();
				if(notificationsContainer != null){
					notificationsContainer.removeView(notification);
					try {
						manager.removeView(notificationsContainer);
					} catch (Exception e) {}
				}		
			}
		});
	}
	
	protected void removeAllNotifications() {
		application.getCurrentActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				stopTimer();
				setupNotificationsContainer();
				if(notificationsContainer != null){
					notificationsContainer.removeAllViews();
					try {
						manager.removeView(notificationsContainer);
					} catch (Exception e) {}
				}		
			}
		});
	}
	
	private void startTimer(final View notification){
		timer = new CountDownTimer(TIME_TO_LIVE, TIME_TO_LIVE) {
			@Override
			public void onTick(long millisUntilFinished) {}
			@Override
			public void onFinish() {
				removeNotification(notification);
			}
		};
		timer.start();
	}

	private void stopTimer(){
		if(timer != null){
			timer.cancel();
		}
	}

}
