package com.anyoption.android.app.model.questionnaire;

import java.util.List;

public abstract class BaseQuestionItem {
	
	/**
	 * The order for this item in the questions list.
	 */
	protected long orderId; 
	
	protected String questionText;
	
	/**
	 * The userAnswers as localized texts.
	 */
	protected List<String> answerTexts; 
	
	/**
	 * True if the user should see this question, false otherwise.
	 */
	protected boolean isVisible;

/*	public interface OnAnswerChangedListener {

		void onAnswerChanged();

	}

	private OnAnswerChangedListener listener;*/
	
	public BaseQuestionItem(long orderId, String questionText, List<String> answerTexts) {
		this.orderId = orderId;
		this.questionText = questionText;
		this.answerTexts = answerTexts;
		isVisible = true;
	}

	public BaseQuestionItem(long orderId, String questionText) {
		this(orderId, questionText, null);
	}
	
	public abstract boolean isAnswered();

/*	protected OnAnswerChangedListener getOnAnswerChangedListener() {
		return listener;
	}

	public void setOnAnswerChangedListener(OnAnswerChangedListener listener) {
		this.listener = listener;
	}*/

	public static boolean isAnswerChanged(Long oldId, Long newId) {
		if (oldId == null && newId == null) {
			return false;
		} else if ((oldId == null && newId != null)
				|| (oldId != null && newId == null)) {
			return true;
		} else {
			return !oldId.equals(newId);
		}
	}
	
	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public List<String> getAnswerTexts() {
		return answerTexts;
	}

	public void setAnswerTexts(List<String> answerTexts) {
		this.answerTexts = answerTexts;
	}

	public boolean isVisible() {
		return isVisible;
	}

	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}
	
}
