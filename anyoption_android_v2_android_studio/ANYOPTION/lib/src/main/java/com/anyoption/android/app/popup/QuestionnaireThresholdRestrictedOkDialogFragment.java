package com.anyoption.android.app.popup;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.manager.PopUpManager;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.TextUtils.OnInnerTextClickListener;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.requests.IsKnowledgeQuestionMethodRequest;
import com.anyoption.common.service.requests.UnblockUserMethodRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.UserMethodResult;

public class QuestionnaireThresholdRestrictedOkDialogFragment extends OkDialogFragment {

	public static final String TAG = QuestionnaireThresholdRestrictedOkDialogFragment.class.getSimpleName();

	protected TextView titleTextView;
	protected TextView firstMessageTextView;
	protected TextView secondMessageTextView;
	protected TextView thirdMessageTextView;

	protected boolean isClicked = false;

	/**
	 * Use this method to create new instance of the fragment.
	 *
	 * @return
	 */
	public static QuestionnaireThresholdRestrictedOkDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new QuestionnaireRestrictedOkDialogFragment.");
		QuestionnaireThresholdRestrictedOkDialogFragment fragment = new QuestionnaireThresholdRestrictedOkDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);

		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if(args != null){
			setCancelable(args.getBoolean(Constants.EXTRA_CANCELABLE, false));
		}

		application.getCommunicationManager().requestService(this, "getUserCallback", application.getServiceMethodGetUser(), new UserMethodRequest(), application.getUserMethodResultClass(), false, false);
	}


	/**
	 * Set up content.
	 *
	 * @param contentView
	 */
	@Override
	protected void onCreateContentView(View contentView) {

		titleTextView = (TextView) contentView.findViewById(R.id.questionnaire_threshold_restricted_popup_title);
		firstMessageTextView = (TextView) contentView.findViewById(R.id.questionnaire_threshold_restricted_popup_first_message);
		secondMessageTextView = (TextView) contentView.findViewById(R.id.questionnaire_threshold_restricted_popup_second_message);
		thirdMessageTextView = (TextView) contentView.findViewById(R.id.questionnaire_threshold_restricted_popup_third_message);

		setupCloseImage(contentView);

		decrateText();

		View activateButton = contentView.findViewById(R.id.activate_button);
		activateButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if (!isClicked) {
					UnblockUserMethodRequest request = new UnblockUserMethodRequest();
					request.setUserId(application.getUser().getId());
					request.setUserName(application.getUser().getUserName());
					application.getCommunicationManager().requestService(QuestionnaireThresholdRestrictedOkDialogFragment.this, "unblockUserForTresholdCallback",
							Constants.SERVICE_UBLOCK_USER_FOR_THRESHOLD, request, MethodResult.class, false,
							false);
					isClicked = true;
				}
			}
		});
	}

	@SuppressWarnings("unchecked")
	public void unblockUserForTresholdCallback(Object resultObj) {
		application.getCommunicationManager().requestService(this, "getUserCallback", application.getServiceMethodGetUser(), new UserMethodRequest(), application.getUserMethodResultClass(), false, false);
	}

	public void getUserCallback(Object resultObj) {
		UserMethodResult result = (UserMethodResult) resultObj;

		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			application.saveUser(result);
		}

		if(firstMessageTextView != null && titleTextView != null && !isClicked) {
			decrateText();
		}

		if(isClicked) {
			isClicked = false;
			dismiss();
		}

	}

	protected void setupCloseImage(View contentView) {
		ImageView closeImage = (ImageView) contentView.findViewById(R.id.questionnaire_threshold_restricted_popup_close_image);
		closeImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});

	}

	public void decrateText() {
		String thresholdSum = AmountUtil.getFormattedAmountWithoutDecimalPointDigits(RegulationUtils.getThresholdBlock(), application.getUser().getCurrency());
		firstMessageTextView.setText(firstMessageTextView.getText().toString().replace("{threshold}", thresholdSum));
		TextUtils.decorateInnerText(firstMessageTextView, null, firstMessageTextView.getText().toString(), getString(R.string.questionnaire_threshold_restricted_popup_first_message_bold_text).replace("{threshold}", thresholdSum), Font.ROBOTO_BOLD, 0, 0);
		TextUtils.decorateInnerText(titleTextView, null, titleTextView.getText().toString(), titleTextView.getText().toString(), Font.ROBOTO_BOLD, 0, 0);
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);

	}

	@Override
	protected int getContentLayout() {
		return R.layout.popup_questionnaire_threshold_restricted;
	}

	@Override
	public int getPopupCode(){
		return PopUpManager.POPUP_QUEST_THRESHOLD_RESTRICTED;
	}
}
