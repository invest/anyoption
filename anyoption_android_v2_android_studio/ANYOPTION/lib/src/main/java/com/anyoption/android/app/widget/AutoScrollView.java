package com.anyoption.android.app.widget;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.HorizontalScrollView;

/**
 * Custom {@link HorizontalScrollView} that auto scrolls left-right Infinite.
 * 
 * @author Anastas Arnaudov
 * 
 */
public class AutoScrollView extends HorizontalScrollView {

//	public static final float VELOCITY = 0.1f;
	
	public static final long DURATION = 2500l;
	
	private boolean isScrolling = false;
	private ObjectAnimator animation;
	private View child;
	private LinearInterpolator interpolator;

	public AutoScrollView(Context context) {
		super(context);
		init(context, null, 0);
	}

	public AutoScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}

	public AutoScrollView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		setHorizontalScrollBarEnabled(false);
		setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				//Do nothing
				return true;
			}
		});
		interpolator = new LinearInterpolator();
	}

	@Override
	public void setVisibility(int visibility) {
		removeAnimation();
		if(visibility == View.VISIBLE){
			startAnimation();
		}
		super.setVisibility(visibility);
	}
	
	private void removeAnimation(){
		isScrolling = false;
		if(animation != null){
			animation.cancel();			
		}
		setScrollX(0);
	}
	
	@SuppressLint("DrawAllocation")
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {

		child = getChildAt(0);//As ScrollView has only 1 child.
		child.addOnLayoutChangeListener(new OnLayoutChangeListener() {
			@Override
			public void onLayoutChange(View v, int left, int top, int right,int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
				if(child.getWidth() > 0 && child.getWidth() > AutoScrollView.this.getWidth()){
					removeAnimation();
					startAnimation();
				}
			}
		});
		
		if(child.getWidth() > 0){
			startAnimation();			
		}

		super.onLayout(changed, l, t, r, b);
	}

	private void startAnimation(){
		
		if (child != null && child.getWidth() > 0 && !isScrolling()) {
			setScrolling(true);

			int padding = getPaddingRight();
			
			int delta = child.getWidth() + padding - getWidth();

			if (delta > 0) {
				animation = ObjectAnimator.ofInt(this, "scrollX", delta);
				animation.setDuration(DURATION);
				animation.setInterpolator(interpolator);
				animation.setRepeatCount(ValueAnimator.INFINITE);
				animation.setRepeatMode(ValueAnimator.REVERSE);

				animation.start();
			}

		}
	}
	
	public boolean isScrolling() {
		return isScrolling;
	}

	public void setScrolling(boolean isScrolling) {
		this.isScrolling = isScrolling;
	}
}