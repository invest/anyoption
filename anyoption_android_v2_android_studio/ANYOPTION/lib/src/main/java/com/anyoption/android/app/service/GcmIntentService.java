package com.anyoption.android.app.service;

import org.json.JSONObject;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.constants.Constants;
import com.appoxee.Appoxee;
import com.appoxee.AppoxeeManager;
import com.appoxee.gcm.GCMIntentService;
import com.appoxee.gcm.MessageIntentHandler;
import com.appoxee.push.PushOpenIntentService;
import com.appoxee.push.notificationbuilder.NotificationBuilder;
import com.appoxee.utils.Utils;

import android.app.Activity;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import java.util.Random;

/**
 * Custom {@link IntentService} that processes Push Notifications and displays notification.
 * @author Anastas Arnaudov
 *
 */
public class GcmIntentService extends GCMIntentService {

	private static final String TAG = GcmIntentService.class.getSimpleName();
	
	@Override
	public void onMessage(Context context, Intent intent) {
		Log.d(TAG, "onMessage");
		intent.putExtra(Constants.EXTRA_PUSH_NOTIFICATION, true);
		//Check if the user has disable the Push Notifications:
		if(!Application.get().getSharedPreferencesManager().getBoolean(Constants.PREFERENCES_IS_NOTIFICATIONS_ON, true)){
			return;
		}
		
		//Check if the Application is visible:
		if(Application.get().isApplicationVisible()){
			//The Application is visible so we do NOT display the push notification:
			Log.d(TAG, "onMessage" + " -> The Application is visible so dont't show the push notification!!!");
			return;
		}
		
		handleAppoxeeMessage(context, intent);
//		super.onMessage(context, intent);
	}

	private class MyMessageIntentHandler extends MessageIntentHandler{

		@Override
		public void extractMessageAndHandle(Context context, Bundle extras) {
			AppoxeeManager.setmContext(context.getApplicationContext());
			boolean isRichMsg = false;
			if(extras != null && !extras.containsKey("p") && !extras.containsKey("apbcd")) {
				Utils.Warn("GCM Message was not generated from Appoxee");
				boolean ex1 = AppoxeeManager.getSharedPreferences().getBoolean("REGISTERED_ON_APPOXEE", false);
				if(extras.containsKey("from") && extras.getString("from").contains("google.com/iid") && ex1) {
					Utils.Debug("Message from Google.com/iid, probably refresh Token");
					Appoxee.OptOut("pushToken", true);
				} else {
					Utils.Debug("Message from Google.com/iid, registration wasn\'t performed yet,will continue registration at GCM");
				}

			} else {
				try {
					SharedPreferences ex = context.getApplicationContext().getSharedPreferences("appoxee_pref", 0);
					String appDefaultActivityClass = ex.getString("mSetAppDefaultActivityClass", (String)null);
					String pushOpenActivityClass = ex.getString("mPushOpenActivity", appDefaultActivityClass);

					//TODO OVERRIDEN part. CONFIGURING THE Activity which will be opened:
					Activity currentActivity = Application.get().getCurrentActivity();
					if (Application.get().isApplicationRunning() && currentActivity != null) {
						// The Application is running so we will resume it:
						pushOpenActivityClass = currentActivity.getClass().getName();
					}
					///////////////////////////////////

					String mSetAppSDKKey = ex.getString("mSetAppSDKKey", (String)null);
					AppoxeeManager.isAirVersion = ex.getBoolean("isAirVersion", false);
					Log.i("mSetAppSDKKey", mSetAppSDKKey + "");
					if(extras != null) {
						boolean hasMessage = false;
						Utils.Debug("MessageIntentHandler Extra Bundle : " + extras.toString());
						String alert = extras.getString("alert");
						String sound = extras.getString("sound");
						if(!AppoxeeManager.getSharedPreferences().getBoolean("soundEnabled", true)) {
							sound = "";
						}

						long messageId = 0L;
						String str = null;
						if(extras.containsKey("apbcd")) {
							try {
								str = extras.getString("apbcd");
								messageId = Long.parseLong(str);
								isRichMsg = true;
								Utils.Log("MessageID:" + str + " isRichMsg=" + isRichMsg);
							} catch (NumberFormatException var27) {
								Utils.Error("MessageIntentHandler error: cannot parse message id=" + str);
								Utils.DebugException(var27);
							} catch (Exception var28) {
								Utils.DebugException(var28);
							}

							hasMessage = true;
						} else if(extras.containsKey("p")) {
							try {
								str = extras.getString("p");
								AppoxeeManager.setConfiguration("p_from_killed", str);
								messageId = Long.parseLong(str);
								isRichMsg = false;
							} catch (NumberFormatException var25) {
								Utils.Error("MessageIntentHandler error: cannot parse message p= " + str);
								Utils.DebugException(var25);
							} catch (Exception var26) {
								Utils.DebugException(var26);
							}
						}

						if(messageId == 0L) {
							messageId = (long)(new Random()).nextInt();
						}

						String push_description = null;
						if(extras.containsKey("push_description")) {
							push_description = extras.getString("push_description");
							push_description = Utils.checkForDynamicText(push_description);
						}

						if(extras.containsKey("collapse_key") && extras.getString("collapse_key").equalsIgnoreCase("do_not_collapse")) {
							Utils.Warn("field collapse_key does not contain messageId, SDK will extract it from payload  -p/apbcd- field");
						}

						String title = AppoxeeManager.getAppName();
						if(extras.containsKey("push_title")) {
							title = extras.getString("push_title");
						}

						Utils.Debug("title :" + title);
						Intent notificationIntent = new Intent(context.getApplicationContext(), PushOpenIntentService.class);
						String notificationIntentClass = pushOpenActivityClass;
						Boolean isInboxEnabled = Boolean.valueOf(AppoxeeManager.getSharedPreferences().getBoolean("inboxEnabled", false));
						Boolean isCustomInboxEnabled = Boolean.valueOf(Appoxee.isCustomInboxEnabled());
						if(isRichMsg && isInboxEnabled.booleanValue()) {
							//TODO OVERRIDEN part. We do NOT want the Appoxee to open its Activity.
							//TODO This is for PUSH+INBOX messages
//							notificationIntentClass = "com.appoxee.activities.InboxMessage";//TODO
							notificationIntent.putExtra("fromCustomInbox", isCustomInboxEnabled);
						}

						notificationIntent.putExtra("PushOpenIntentService.activityName", notificationIntentClass);
						notificationIntent.setFlags(872415232);
						notificationIntent.putExtras(extras);
						notificationIntent.putExtra("messageId", messageId);
						notificationIntent.putExtra("hasMessage", hasMessage);
						Utils.Log("GCM: got new message:" + messageId + ", message=" + alert);
						Utils.Log("GCM: notification class name:" + notificationIntentClass);
						Utils.Log("GCM: has message ? value = " + hasMessage);
						if(extras.containsKey("apx_rp")) {
							NotificationBuilder.SendCustomNotification(context, messageId, title, push_description, alert, sound, notificationIntent, "");
						} else if(Build.VERSION.SDK_INT >= 16) {
							Utils.Debug("Android Version JELLY_BEAN or Above , but below Lollipop");
							JSONObject jsonRichPushMsg1 = new JSONObject();
							JSONObject jsonRichPushMsg_Subject1 = new JSONObject();
							JSONObject jsonRichPushMsg_Body1 = new JSONObject();

							try {
								if(AppoxeeManager.getCustomPushBuilder() != null) {
									NotificationBuilder.BuildAndSendFullCustomNotification(context, messageId, title, push_description, alert, sound, notificationIntent);
									return;
								}

								if(AppoxeeManager.getPushStyle() != NotificationBuilder.getStyleNone()) {
									NotificationBuilder.SendStyledNotification(context, messageId, title, push_description, alert, sound, notificationIntent, AppoxeeManager.getPushStyle());
									return;
								}

								jsonRichPushMsg_Subject1.put("text", AppoxeeManager.getAppName());
								jsonRichPushMsg_Body1.put("text", push_description);
								jsonRichPushMsg1.put("Subject", jsonRichPushMsg_Subject1);
								jsonRichPushMsg1.put("Desc", jsonRichPushMsg_Body1);
								jsonRichPushMsg1.put("noLines", 2);
								notificationIntent.putExtra("apx_rp", jsonRichPushMsg1.toString());
								String e = "";
								if(Build.VERSION.SDK_INT >= 21) {
									e = "_white";
								}

								NotificationBuilder.SendCustomNotification(context, messageId, title, push_description, alert, sound, notificationIntent, e);
							} catch (Exception var24) {
								Utils.Error("Error building CustomNotification, Publishing regular Notification");
								NotificationBuilder.SendNotification(context, messageId, title, push_description, alert, sound, notificationIntent);
							}
						} else {
							Utils.Debug("Android Version Lower than JELLY_BEAN or Lollipop and above");
							NotificationBuilder.SendNotification(context, messageId, title, push_description, alert, sound, notificationIntent);
						}
					}
				} catch (Exception var29) {
					Utils.Error("Appoxee GCM MessageIntentHandler Exception : " + var29.getMessage());
					Utils.DebugException(var29);
				}

			}
		}

	}

	/**
	 * Overrides the Appoxee onMessage method in order to avoid their custom Inbox Activity to be opened after a push notification is pressed.
	 */
	public void handleAppoxeeMessage(Context context, Intent intent) {
		Log.e(TAG, "handleAppoxeeMessage: intent = " + intent);
		MyMessageIntentHandler handler = new MyMessageIntentHandler();
		handler.handleMessage(context, intent);
	}
	
	@Override
	public void onError(Context context, String errorId) {
		Log.d(TAG, "onError errorId=" + errorId);
		super.onError(context, errorId);
	}
	
	@Override
	public void onRegistered(Context context, String registrationId) {
		Log.d(TAG, "onRegistered");
		super.onRegistered(context, registrationId);
	}
	
	@Override
	public void onUnregistered(Context context, String registrationId) {
		Log.d(TAG, "onRegistered");
		super.onUnregistered(context, registrationId);
	}
}
