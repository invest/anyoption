package com.anyoption.android.app.widget.questionnaire;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.activity.BubblesActivity;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.model.Answer;
import com.anyoption.android.app.model.Question;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.TextUtils;

/**
 * Created by veselin.hristozov on 10.10.2016 г..
 */

public abstract class QuestionnaireBaseView extends LinearLayout {

    public static final String TAG = QuestionnaireBaseView.class.getSimpleName();

    protected boolean isEnabled;
    protected OnAnswerSelectedListener answerSelectedListener;

    protected Question question;
    protected Answer answer;

    protected TextView questionTextView;
    protected TextView errorTextView;

    protected Question.QuestionType questionType;

    public interface OnAnswerSelectedListener {
        void onAnswerSelected(Question question, Answer answer);
    }

    public QuestionnaireBaseView(Context context) {
        super(context);
        init();
    }

    public QuestionnaireBaseView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public QuestionnaireBaseView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public void setEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public Question.QuestionType getQuestionType() {
        return questionType;
    }

    protected abstract void init();

    public abstract void showError();

    public abstract boolean isAnswered();

    public TextView getQuestionTextView() {
        return questionTextView;
    }

    public static void decorateText(TextView questionTextView, String htmlString) {
        if (questionTextView != null) {

            questionTextView.setText(Html.fromHtml(htmlString).toString());

            try {
                Spannable spannableString = new SpannableString(questionTextView.getText());

                htmlString = htmlString.replaceAll(" target=\"_blank\"", "");

                if (htmlString.contains("<b>")) {
                    String[] boldStrings = htmlString.split("<b>");

                    for (int currentPositionOfStartBoldString = 1; currentPositionOfStartBoldString < boldStrings.length; currentPositionOfStartBoldString++) {
                        String[] endBoldString = boldStrings[currentPositionOfStartBoldString].split("</b>");

                        spannableString = makeLinkBold(questionTextView, spannableString, endBoldString[0]);
                    }
                }

                if (htmlString.contains("<u>")) {
                    String[] underlinedStrings = htmlString.split("<u>");

                    for (int currentPositionOfStartUnderlineString = 1; currentPositionOfStartUnderlineString < underlinedStrings.length; currentPositionOfStartUnderlineString++) {
                        String[] endUnderlineString = underlinedStrings[currentPositionOfStartUnderlineString].split("</u>");

                        spannableString = makeLinkUnderlined(questionTextView, spannableString, endUnderlineString[0]);
                    }
                }

                if (htmlString.contains("<a href=\"")) {
                    String[] linkStrings = htmlString.split("href=\"");

                    for (int currentPositionOfStartLinkString = 1; currentPositionOfStartLinkString < linkStrings.length; currentPositionOfStartLinkString++) {
                        String[] endLinkKeyString = linkStrings[currentPositionOfStartLinkString].split("\">");
                        String[] endLinkString = endLinkKeyString[1].split("</a");

                        spannableString = makeLinkClickable(questionTextView, spannableString, endLinkString[0], endLinkKeyString[0]);
                    }
                }

                questionTextView.setText(spannableString);

            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
            }

        }
    }


    public static void navigateToLink(String key) {
        if (key.equals("link_terms") || key.equals("link_risk") || key.equals("link_agreement") || key.equals("link_privacy")) {
            Application.get().postEvent(new NavigationEvent(Application.Screen.TERMS_AND_CONDITIONS, NavigationEvent.NavigationType.DEEP));
        } else if (key.equals("link_trade")) {
            if (Application.get().isTablet()) {
                Application.get().postEvent(new NavigationEvent(Application.Screen.TABLET_TRADE, NavigationEvent.NavigationType.DEEP));
            } else {
                Application.get().postEvent(new NavigationEvent(Application.Screen.TRADE, NavigationEvent.NavigationType.DEEP));
            }
        } else if (key.equals("link_bubbles")) {
            Application.get().setCurrentScreen(Application.Screen.BUBBLES);
            Intent intent = new Intent(Application.get(), Application.get().getSubClassForClass(BubblesActivity.class));
            Application.get().startActivity(intent);
        } else if (key.equals("link_deposit")) {
            Application.get().postEvent(new NavigationEvent(Application.Screen.DEPOSIT_MENU, NavigationEvent.NavigationType.DEEP));
        } else if (key.equals("link_withdraw")) {
            Application.get().postEvent(new NavigationEvent(Application.Screen.WITHDRAW_MENU, NavigationEvent.NavigationType.DEEP));
        } else if (key.equals("link_support")) {
            Application.get().postEvent(new NavigationEvent(Application.Screen.SUPPORT, NavigationEvent.NavigationType.DEEP));
        } else if (key.equals("link_bonuses")) {
            Application.get().postEvent(new NavigationEvent(Application.Screen.BONUSES, NavigationEvent.NavigationType.DEEP));
        }
    }

    public static Spannable makeLinkClickable(TextView questionTextView, Spannable spannable, String innerText, final String key) {
        if (questionTextView != null) {
            TextUtils.OnInnerTextClickListener linkListener = new TextUtils.OnInnerTextClickListener() {
                @Override
                public void onInnerTextClick(TextView textView, String text, String innerText) {
                    navigateToLink(key);
                }
            };

            spannable = TextUtils.makeClickableInnerText(questionTextView, spannable, questionTextView.getText().toString(), innerText, null, 0, 0, true, linkListener);
        }

        return spannable;
    }

    public static Spannable makeLinkBold(TextView questionTextView, Spannable spannable, String innerText) {
        if (questionTextView != null) {
            spannable = TextUtils.decorateInnerText(questionTextView, spannable, questionTextView.getText().toString(), innerText, FontsUtils.Font.ROBOTO_BOLD, 0, 0);
        }

        return spannable;
    }

    public static Spannable makeLinkUnderlined(TextView questionTextView, Spannable spannable, String innerText) {
        if (questionTextView != null) {
            spannable = TextUtils.underlineInnerText(questionTextView, spannable, questionTextView.getText().toString(), innerText);
        }

        return spannable;
    }
}
