package com.anyoption.android.app.fragment.my_account.banking.deposit;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.RequestButton.InitAnimationListener;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

/**
 * @author liors
 *
 */
public class DomesticDepositFragment extends BaseDepositFragment {
	private static final String TAG = DomesticDepositFragment.class.getSimpleName();
	private View rootView;

	@Override
	protected Screenable setupScreen() {
		return Screen.DOMESTIC_DEPOSIT;
	}
	
	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static DomesticDepositFragment newInstance(Bundle bundle){
		DomesticDepositFragment domesticDepositFragment = new DomesticDepositFragment();	
		domesticDepositFragment.setArguments(bundle);
		return domesticDepositFragment;
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.domestic_deposit_layout, container, false);	
		onCreateViewBase(rootView.findViewById(R.id.content));
		return rootView;
	}
	
	@Override
	public void submitDeposit() {
		// TODO 
		application.postEvent(new NavigationEvent(Screen.DOMESTIC_PAYMENTS, NavigationType.DEEP)); //TODO navigate type?
	}
	
	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if(hidden){
			
		}else{
			onCreateViewBase(rootView.findViewById(R.id.content));
		}
	}
	
	@Override
	public void firstDepositCallBack(Object resultObj) {
		Log.d(TAG, "firstDepositCallBack");
		super.firstDepositCallBack(resultObj);
		submitButton = (RequestButton) rootView.findViewById(R.id.submit);
		if (isFirstEverDeposit) {
			Log.d(TAG, "firstDepositCallBack + isFirstEverDeposit");
			rootView.findViewById(R.id.common_fields).setVisibility(View.VISIBLE);
			rootView.findViewById(R.id.currency_selector).setVisibility(View.VISIBLE);
			rootView.findViewById(R.id.info).setVisibility(View.VISIBLE);
			rootView.findViewById(R.id.line_up).setVisibility(View.GONE);
			rootView.findViewById(R.id.line_down).setVisibility(View.GONE);
			setCurrency(rootView, R.id.currency_selector);
			submitButton.setOnClickListener(new OnClickListener() { 
				@Override 
				public void onClick(View v) { 
					if(formBase.checkValidity()){
						submitButton.startLoading(new InitAnimationListener(){
							@Override
							public void onAnimationFinished() {
								formBase.submit();
							}
						});
					}
					
				} 
			});
		} else {
			Log.d(TAG, "firstDepositCallBack + not isFirstEverDeposit");
			rootView.findViewById(R.id.common_fields).setVisibility(View.GONE);
			submitButton.setOnClickListener(new OnClickListener() { 
				@Override 
				public void onClick(View v) { 
					submitDeposit();
				} 
			});
		}		
	}

	@Override
	public String getAmount() {
		return null;
	}

	@Override
	public String getCurrencySymbol() {
		return null;
	}

	@Override
	public void setSubmitButtonEnabled(boolean isEnabled) {
		isSubmitButtonEnabled = isEnabled;
		if(submitButton != null){
			submitButton.setEnabled(isEnabled);	
			if(isEnabled){
				submitButton.stopLoading();
			}
		}
	}
}