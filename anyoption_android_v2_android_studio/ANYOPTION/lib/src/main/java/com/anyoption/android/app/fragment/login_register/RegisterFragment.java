package com.anyoption.android.app.fragment.login_register;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.LoginLogoutEvent.Type;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.event.RegisterFailEvent;
import com.anyoption.android.app.event.TryWithoutAccountEvent;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.manager.PopUpManager.OnConfirmPopUpButtonPressedListener;
import com.anyoption.android.app.popup.ExistingEmailDialogfragment;
import com.anyoption.android.app.popup.TryWithoutAccountDialogfragment;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.RequestButton.InitAnimationListener;
import com.anyoption.android.app.widget.form.Form;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.android.app.widget.form.FormPhoneCodeSelector;
import com.anyoption.android.app.widget.form.FormSelector.ItemSelectedListener;
import com.anyoption.beans.base.Register;
import com.anyoption.common.beans.base.Country;
import com.anyoption.common.beans.base.Platform;
import com.anyoption.common.enums.CountryStatus;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.json.requests.InsertUserMethodRequest;
import com.anyoption.json.requests.RegisterAttemptMethodRequest;
import com.anyoption.json.results.CountryIdMethodResult;
import com.anyoption.json.results.RegisterAttemptMethodResult;

import java.util.Date;

/**
 * {@link BaseFragment} that represents the Register Screen.
 * @author Anastas Arnaudov
 *
 */
/**
 * @author liors
 *
 */
public class RegisterFragment extends NavigationalFragment {

	public static final String TAG = RegisterFragment.class.getSimpleName();
	
	public static final boolean DEFAULT_CONTACT_BY_EMAIL_SMS = true;
	
	protected Form form;
	protected FormEditText firstNameField;
	protected FormEditText lastNameField;
	protected FormEditText emailField;
	protected FormPhoneCodeSelector phoneCodeButton; 
	protected FormEditText phoneField;
	protected FormEditText passwordField;
	protected TextView termsAndConditionsTextView;
	protected RequestButton registerButton;
	protected long registerAttemptId;
	protected TextView tryWithoutAccountTextView;
	protected View rootView;
	
	protected long countryId = 1;
	
	protected boolean isTermsAndConditionsButtonEnabled = false;
	
	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static RegisterFragment newInstance(){	
		return newInstance(new Bundle());
	}
	
	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static RegisterFragment newInstance(Bundle bundle){
		RegisterFragment registerFragment = new RegisterFragment();	
		registerFragment.setArguments(bundle);
		return registerFragment;
	}

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = inflater.inflate(R.layout.register_fragment_layout, container, false); 

		init();
		 
		setupForm();
 
		setupTermsAndConditions();
		
		setupRegisterButton();
		
		tryWithoutAccountTextView = (TextView) rootView.findViewById(R.id.register_try_without_account_text);
		underlineTryWithoutAccountText();
		tryWithoutAccountTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {	
				TryWithoutAccountDialogfragment tryWithoutAccountDialogfragment = (TryWithoutAccountDialogfragment) application.getFragmentManager().showDialogFragment(TryWithoutAccountDialogfragment.class, null);
				tryWithoutAccountDialogfragment.setOnConfirmPopUpButtonPressedListener(new OnConfirmPopUpButtonPressedListener() {
					@Override
					public void onPositive() {
						application.postEvent(new TryWithoutAccountEvent());
						if(getResources().getBoolean(R.bool.isTablet)) {
							application.getFragmentManager().onBack();
		    			} else {
							goToTradeScreen();
		    			}	
					}
					@Override
					public void onNegative() {}
				});
			} 
		});	
		return rootView; 
	} 

	protected void goToTradeScreen() {
		application.postEvent(new NavigationEvent(Screen.TRADE, NavigationType.INIT));
	}

	protected void underlineTryWithoutAccountText(){
		TextUtils.underlineText(tryWithoutAccountTextView);
	}
	
	protected void setupForm(){
		firstNameField = (FormEditText) rootView.findViewById(R.id.register_first_name_edittext);
		firstNameField.addValidator(ValidatorType.EMPTY, ValidatorType.LETTERS_AND_SPACE_ONLY);
		firstNameField.setFieldName(Constants.FIELD_FIRST_NAME);

		lastNameField = (FormEditText) rootView.findViewById(R.id.register_last_name_edittext);
		lastNameField.addValidator(ValidatorType.EMPTY, ValidatorType.LETTERS_AND_SPACE_ONLY);
		lastNameField.setFieldName(Constants.FIELD_LAST_NAME);

		emailField = (FormEditText) rootView.findViewById(R.id.register_email_edittext); 
		emailField.addValidator(ValidatorType.EMPTY, ValidatorType.EMAIL); 
		emailField.setFieldName(Constants.FIELD_EMAIL);
		emailField.addFocusChangeListener(new FormEditText.FocusChangeListener() {
			@Override 
			public void onFocusChanged(boolean hasFocus) {
				if(!hasFocus && emailField.checkValidity()) {
					insertRegisterAttempt();
				}
			} 
		}); 
		phoneCodeButton = (FormPhoneCodeSelector) rootView.findViewById(R.id.register_phone_code_button);
		phoneCodeButton.addItemListener(new ItemSelectedListener<Country>() {
			@Override
			public void onItemSelected(Country selectedItem) {
				countryId = selectedItem.getId();
			}
		});
		phoneField = (FormEditText) rootView.findViewById(R.id.register_phone_edittext); 
		phoneField.addValidator(ValidatorType.EMPTY, ValidatorType.PHONE, ValidatorType.PHONE_MINIMUM_CHARACTERS); 
		phoneField.setFieldName(Constants.FIELD_MOBILE_PHONE);
		phoneField.addFocusChangeListener(new FormEditText.FocusChangeListener() {
			@Override 
			public void onFocusChanged(boolean hasFocus) {
				if(!hasFocus && phoneField.checkValidity()) {
					insertRegisterAttempt();
				}
			} 
		}); 
		passwordField = (FormEditText) rootView.findViewById(R.id.register_pass_edittext); 
		passwordField.addValidator(ValidatorType.EMPTY, ValidatorType.ALNUM, ValidatorType.PASSWORD_MINIMUM_CHARACTERS); 
		passwordField.setFieldName(Constants.FIELD_PASSWORD);
		form = new Form(new Form.OnSubmitListener() { 
			@Override 
			public void onSubmit() {
				insertRegister();
			} 
		}); 
		form.addItem(firstNameField, lastNameField, emailField, phoneCodeButton, phoneField, passwordField);
	}
	
	protected void setupTermsAndConditions(){
		termsAndConditionsTextView = (TextView) rootView.findViewById(R.id.register_terms_and_conditions_text);
		TextUtils.underlineText(termsAndConditionsTextView);
		termsAndConditionsTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(isTermsAndConditionsButtonEnabled){
					Bundle bundle  = new Bundle();	                
					bundle.putLong(Constants.EXTRA_COUNTRY_ID, countryId);
					application.postEvent(new NavigationEvent(Screen.TERMS_AND_CONDITIONS, NavigationType.DEEP, bundle));					
				}
			}
		});	
	}
	
	protected void setupRegisterButton() {
		registerButton = (RequestButton) rootView.findViewById(R.id.register_button); 
		registerButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				submitForm();
			}
		});		
	}

	protected void submitForm() {
		if (form.checkValidity()) {
			registerButton.startLoading(new InitAnimationListener() {
				@Override
				public void onAnimationFinished() {
					form.submit();
				}
			});
		}
	}

	@Override
	protected Screenable setupScreen() {
		return Screen.REGISTER;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////							REQUESTs and RESPONSEs										/////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	protected void init() {
		if(!application.getCommunicationManager().requestService(RegisterFragment.this, "getCountryIdCallBack", Constants.SERVICE_GET_COUNTRY_ID, new MethodRequest() , CountryIdMethodResult.class, false, false)){
			isTermsAndConditionsButtonEnabled = true;
			if(registerButton != null){
				registerButton.stopLoading();
			}
		}
	}

	public void getCountryIdCallBack(Object result) {
		Log.d(TAG, "getCountryIdCallBack");
		CountryIdMethodResult countryIdMethodResult = (CountryIdMethodResult) result;
		if(countryIdMethodResult != null && countryIdMethodResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS){
			countryId = countryIdMethodResult.getCountryId();

			//Check if the registration is allowed:
			if (countryIdMethodResult.getCountryStatus() == CountryStatus.REJECTED.getId() || countryIdMethodResult.getCountryStatus() == CountryStatus.BLOCKED.getId()) {
				//Show the "Sorry" Screen:
				application.postEvent(new NavigationEvent(Screen.UNREGULATED_COUNTRY, NavigationType.INIT));
				return;
			}

			phoneCodeButton.setPhoneCode(countryId);
			application.setCountryId(countryId);
			isTermsAndConditionsButtonEnabled = true;
			registerButton.stopLoading();
		}
	}

	public void insertRegisterAttempt() {
		Log.d(TAG, "firstNameField = " + firstNameField.getValue() + "\n");
		Log.d(TAG, "lastNameField = " + lastNameField.getValue() + "\n");
		Log.d(TAG, "emailField = " + emailField.getValue() + "\n");
		Log.d(TAG, "phoneCodeButton + phoneField = " + countryId +  phoneField.getValue() + "\n");
		Log.d(TAG, "passwordField = " + passwordField.getValue() + "\n");

		RegisterAttemptMethodRequest request = setRegisterAttemptRequest();

		Log.d(TAG, "-------insertRegisterAttempt--------");
		Log.d(TAG, "request user combId = " + request.getCombinationId());
		Log.d(TAG, "request user dp = " + request.getDynamicParameter());
		Log.d(TAG, "request aff_sub1 = " + request.getAff_sub1());
		Log.d(TAG, "request aff_sub2 = " + request.getAff_sub2());
		Log.d(TAG, "request aff_sub3 = " + request.getAff_sub3());
		Log.d(TAG, "request mid = " + request.getmId());
		Log.d(TAG, "request etsmid = " + request.getEtsMId());

		Application.get().getCommunicationManager().requestService(RegisterFragment.this, "registerAttemptCallBack", Constants.SERVICE_INSERT_REGISTER_ATTEMPT, request , RegisterAttemptMethodResult.class, false, false);
	}

	public void registerAttemptCallBack(Object resultObj) {
		Log.d(TAG, "registerAttemptCallBack");
		RegisterAttemptMethodResult result = (RegisterAttemptMethodResult) resultObj;
		registerAttemptId = result.getRegisterAttemptId();
		Log.d(TAG, "result mid = " + result.getmId());
		Log.d(TAG, "result etsmid = " + result.getEtsMId());

		if(result.getmId() != null && result.getmId().length() > 0) {
			application.getSharedPreferencesManager().putString(Constants.PREFERENCES_MID, result.getmId());
		}
		if(result.getEtsMId() != null && result.getEtsMId().length() > 0) {
			application.getSharedPreferencesManager().putString(Constants.PREFERENCES_ETS_MID, result.getEtsMId());
		}

	}

	public void insertRegister() {
		Register user = new Register();
		setUserDetalis(user);
    	InsertUserMethodRequest request = new InsertUserMethodRequest();      
        request.setRegister(user);
        request.setmId(application.getSharedPreferencesManager().getString(Constants.PREFERENCES_MID, null));
        request.setEtsMId(application.getSharedPreferencesManager().getString(Constants.PREFERENCES_ETS_MID, null));

        Log.d(TAG, "-------insertUser--------");
		Log.d(TAG, "user combId = " + user.getCombinationId());
		Log.d(TAG, "user dp = " + user.getDynamicParam());
		Log.d(TAG, "user aff_sub1 = " + user.getAff_sub1());
		Log.d(TAG, "user aff_sub2 = " + user.getAff_sub2());
		Log.d(TAG, "user aff_sub3 = " + user.getAff_sub3());
		Log.d(TAG, "request mid = " + request.getmId());
		Log.d(TAG, "request etsmid = " + request.getEtsMId());
		Log.d(TAG, "--------------------------");
		if(!Application.get().getCommunicationManager().requestService(RegisterFragment.this, "insertUserCallBack", Constants.SERVICE_INSERT_USER, request , UserMethodResult.class, false, false)){
			registerButton.stopLoading();
		}
	}

	public void insertUserCallBack(Object resultObj){
		UserMethodResult result = (UserMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Insert user successfully");

			if(result.getmId() != null && result.getmId().length() > 0) {
				application.getSharedPreferencesManager().putString(Constants.PREFERENCES_MID, result.getmId());
			}
			if(result.getEtsMid() != null && result.getEtsMid().length() > 0) {
				application.getSharedPreferencesManager().putString(Constants.PREFERENCES_ETS_MID, result.getEtsMid());
			}
			Log.d(TAG, "result user combId = " + result.getUser().getCombinationId());
			Log.d(TAG, "result user dp = " + result.getUser().getDynamicParam());
			Log.d(TAG, "result mid = " + result.getmId());
			Log.d(TAG, "result etsmid = " + result.getEtsMid());
			Log.d(TAG, "result aff_sub1 = " + result.getUser().getAffSub1());
			Log.d(TAG, "result aff_sub2 " + result.getUser().getAffSub2());
			Log.d(TAG, "result aff_sub3 = " + result.getUser().getAffSub3());

			form.clear();
			handleOKRegister(result);
		} else {
			if(result.getErrorCode() == Constants.ERROR_CODE_EMAIL_ALREADY_IN_USE || result.getErrorCode() == Constants.ERROR_CODE_EMAIL_EXISTING_IN_AO){
				showExistingEmailPopup();
			}else{
				ErrorUtils.displayFieldError(form, result);
			}
			registerButton.stopLoading();
			application.postEvent(new RegisterFailEvent());

		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	protected void showExistingEmailPopup(){
		ExistingEmailDialogfragment existingEmailDialogfragment = (ExistingEmailDialogfragment) application.getFragmentManager().showDialogFragment(ExistingEmailDialogfragment.class, null);
		existingEmailDialogfragment.setExistingEmailPopupListener(new ExistingEmailDialogfragment.ExistingEmailPopupListener() {
			@Override
			public void onChangeEmailButtonPressed() {
				emailField.requestFocus();
			}
			@Override
			public void onRecoverPassButtonPressed() {
				application.postEvent(new NavigationEvent(Screen.FORGOT_PASSWORD, NavigationType.DEEP));
			}
		});
	}

	protected void setUserDetalis(Register user) {	
    	user.setFirstName(firstNameField.getValue());
    	user.setLastName(lastNameField.getValue());
    	user.setEmail(emailField.getValue());
		user.setPassword(passwordField.getValue());
		user.setPassword2(passwordField.getValue());
		user.setMobilePhone(phoneField.getValue());
		user.setUserName(user.getEmail());
		user.setCountryId(countryId);
		user.setContactByEmail(DEFAULT_CONTACT_BY_EMAIL_SMS);
		user.setContactBySms(DEFAULT_CONTACT_BY_EMAIL_SMS);
		user.setTerms(true);
    	user.setSkinId(1);
    	user.setCombinationId(Utils.getCombinationId());
    	user.setDynamicParam(Utils.getDynamicParam());
    	user.setAff_sub1(Utils.getAffSub1(getActivity()));
    	user.setAff_sub2(Utils.getAffSub2(getActivity()));
    	user.setAff_sub3(Utils.getGclid(getActivity()));
    	user.setHttpReferer(application.getSharedPreferencesManager().getString(Constants.PREFERENCES_REFERRER_PARAM, null));
    	user.setTimeFirstVisit(new Date(application.getSharedPreferencesManager().getLong(Constants.PREFERENCES_FIRST_APPLICATION_OPEN_TIME, 0L)));
    	user.setRegisterAttemptId(registerAttemptId);
    	user.setPlatformId(Platform.ANYOPTION); //Default. it should be override in each client.
	}

	protected void handleOKRegister(UserMethodResult result){
		Screenable afterRegisterScreen = configureAfterRegisterScreen(result);

		application.postEvent(new NavigationEvent(afterRegisterScreen, NavigationType.INIT));
		application.postEvent(new LoginLogoutEvent(Type.REGISTER, result));

		application.getDeepLinksManager().fireServerPixelRegister();
	}

	protected Screenable configureAfterRegisterScreen(UserMethodResult result) {
		Screenable afterRegisterScreen = getFirstNewCardScreen();
		Log.d(TAG,"Checking Facebook Deep Link Second Screen...");
		if(application.getDeepLinksManager().getAfterRegisterScreen() != null){
			Log.d(TAG,"Facebook Deep Link Second Screen = " + application.getDeepLinksManager().getAfterRegisterScreen());
			afterRegisterScreen = application.getDeepLinksManager().getAfterRegisterScreen();
			application.getDeepLinksManager().setSecondScreen(null);
		}else{
			Log.d(TAG,"NO Facebook Deep Link Second Screen set.");
		}
		return afterRegisterScreen;
	}

	protected Screen getFirstNewCardScreen() {
		return Screen.FIRST_NEW_CARD;
	}

	protected RegisterAttemptMethodRequest setRegisterAttemptRequest() {
		RegisterAttemptMethodRequest request = new RegisterAttemptMethodRequest();
		request.setEmail(emailField.getValue());
    	request.setFirstName(firstNameField.getValue());
		request.setLastName(lastNameField.getValue());
		request.setMobilePhone(phoneField.getValue());
    	request.setCombinationId(Utils.getCombinationId()); 
    	request.setDynamicParameter(Utils.getDynamicParam());
    	request.setAff_sub1(Utils.getAffSub1(getActivity()));
    	request.setAff_sub2(Utils.getAffSub2(getActivity()));
    	request.setAff_sub3(Utils.getGclid(getActivity()));
    	request.setHttpReferer(application.getSharedPreferencesManager().getString(Constants.PREFERENCES_REFERRER_PARAM, null));
        request.setmId(application.getSharedPreferencesManager().getString(Constants.PREFERENCES_MID, null));
		request.setEtsMId(application.getSharedPreferencesManager().getString(Constants.PREFERENCES_ETS_MID, null));
		request.setCountryId(countryId);
		request.setContactByEmail(DEFAULT_CONTACT_BY_EMAIL_SMS);
		request.setContactBySMS(DEFAULT_CONTACT_BY_EMAIL_SMS);
		return request;
	}

}