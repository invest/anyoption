package com.anyoption.android.app.fragment.my_account.banking.withdraw;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.CreditCardListView;
import com.anyoption.android.app.widget.CreditCardListView.EmptyListListener;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.json.results.CupWithdrawalMethodResult;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Toast;

public class WithdrawMenuFragment extends NavigationalFragment implements OnClickListener {
	
	private View wire;
	private View cup;
	private CreditCardListView ccListView;
	
	@Override
	protected Screenable setupScreen() {
		return Screen.WITHDRAW_MENU;
	}

	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static WithdrawMenuFragment newInstance(Bundle bundle){
		WithdrawMenuFragment withdrawMenuFragment = new WithdrawMenuFragment();
		withdrawMenuFragment.setArguments(bundle);
		return withdrawMenuFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.withdraw_menu, container, false);
		wire = rootView.findViewById(R.id.wireWithraw);
		cup = rootView.findViewById(R.id.WithrawCup);
		ccListView = (CreditCardListView) rootView.findViewById(R.id.ccListView);
		ccListView.setEmptyListListener(new EmptyListListener() {
			@Override
			public void onEmptyList() {
				wire.setVisibility(View.VISIBLE);
			}
		});
		if(Application.get().getUser().getCurrencyId() == Currency.CURRENCY_CNY_ID){
        	// cup may be available - check the amount
            checkCup();
        }
		wire.setOnClickListener(this);
		cup.setOnClickListener(this);
		return rootView;
	}
	
	private void checkCup() {
		Application.get().getCommunicationManager().requestService(WithdrawMenuFragment.this, "getMaxCupWithdrawalCallback", Constants.SERVICE_GET_MAX_CUP_WITHDRAWAL,  new UserMethodRequest() , CupWithdrawalMethodResult.class);
	}
	
	public void getMaxCupWithdrawalCallback(Object resultObj){
		Log.d(TAG, "getMaxCupWithdrawalCallback");
		CupWithdrawalMethodResult result = (CupWithdrawalMethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			long maxWithdrawAmount = result.getMaxCupWithdrawal();
			if (maxWithdrawAmount == 0) {
				if (ccListView.isEmpty() && ccListView.isCallBackRecived()) {
					wire.setVisibility(View.VISIBLE);
				}
			} else {
				// cup available - disable wire
				cup.setVisibility(View.VISIBLE);
				wire.setVisibility(View.GONE);
			}
		} 
	}

	@Override
	public void onClick(View v) {
		NavigationEvent navigationEvent = null;
		//TODO navigate to the real screens
		if (v.getId() == R.id.wireWithraw) {					
			navigationEvent = new NavigationEvent(Screen.BANK_WIRE_WITHDRAW, NavigationType.DEEP);
		} else if (v.getId() == R.id.WithrawCup) {
			Toast.makeText(application, "WithrawCup", Toast.LENGTH_SHORT).show();
			//navigationEvent = new NavigationEvent(Screen.BONUSES, NavigationType.DEEP);
		}
		if (null != navigationEvent) {
			application.postEvent(navigationEvent);
		}
	}
	
	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (!hidden) {
			ccListView.updateList();
		}
	}
}
