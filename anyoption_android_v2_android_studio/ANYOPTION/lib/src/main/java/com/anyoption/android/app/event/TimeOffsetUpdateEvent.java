package com.anyoption.android.app.event;

/**
 * Custom Event that occurs when the Device time offset (related to the server time) is changed.
 * @author Anastas Arnaudov
 *
 */
public class TimeOffsetUpdateEvent {

	private long timeOffsetInMilliseconds;

	public TimeOffsetUpdateEvent(long timeOffsetInMilliseconds){
		this.timeOffsetInMilliseconds = timeOffsetInMilliseconds;
	}
	
	public long getTimeOffsetInMilliseconds() {
		return timeOffsetInMilliseconds;
	}

	public void setTimeOffsetInMilliseconds(long timeOffsetInMilliseconds) {
		this.timeOffsetInMilliseconds = timeOffsetInMilliseconds;
	}
	
	
	
}
