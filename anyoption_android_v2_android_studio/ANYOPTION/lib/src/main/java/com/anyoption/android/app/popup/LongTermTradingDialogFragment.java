package com.anyoption.android.app.popup;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.anyoption.android.R;
import com.anyoption.android.app.event.LongTermTradingTimeChosenEvent;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.anyoption.android.app.util.ListViewUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.TextView;
import com.anyoption.common.beans.base.Market;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class LongTermTradingDialogFragment extends BaseDialogFragment {

    protected ListView timeItemsListView;
    protected TreeMap<Integer, String> timeItems;

    protected long marketId;
    protected int chosenTime = 0;
    protected TreeMap<Integer, Long> opportunitiesId = new TreeMap<Integer, Long>();
    protected int selectedItemPosition = 0;

    public static LongTermTradingDialogFragment newInstance(Bundle bundle) {
        LongTermTradingDialogFragment fragment = new LongTermTradingDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            marketId = bundle.getLong(Constants.EXTRA_MARKET_ID);
            timeItems = (TreeMap<Integer, String>) bundle.getSerializable(Constants.EXTRA_LONG_TERM_TRADING_TIMES);
            opportunitiesId = (TreeMap<Integer, Long>) bundle.getSerializable(Constants.EXTRA_LONG_TERM_TRADING_IDS);
        } else {
            dismiss();
        }
    }

    @Override
    protected void onCreateContentView(View contentView) {
        timeItemsListView = (ListView) contentView
                .findViewById(R.id.long_term_trading_time_items);

        View okButton = contentView.findViewById(R.id.long_term_trading_ok_button);
        okButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                chooseTime();
            }
        });

        timeItemsListView.setAdapter(new TimeItemsAdapter(timeItems));

        timeItemsListView.postDelayed(new Runnable() {
            public void run() {
                ListViewUtils.setListViewHeightBasedOnChildren(timeItemsListView);
            }
        }, 1);

        timeItemsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                chosenTime = (int) timeItems.keySet().toArray()[position];
                /*for (int i = 0; i < timeItemsListView.getAdapter().getCount(); i++) {
                    timeItemsListView.getChildAt(i).setBackgroundResource(R.color.predefined_amount_dialog_item_backgr);
                }*/

                if(selectedItemPosition >= 0 && selectedItemPosition < timeItemsListView.getCount()) {
                    parent.getChildAt(selectedItemPosition).setBackgroundResource(R.color.predefined_amount_dialog_item_backgr);
                }

                parent.getChildAt(position).setBackgroundResource(R.color.predefined_amount_dialog_item_selected_backgr);

                selectedItemPosition = position;
            }
        });
    }


    @Override
    protected int getContentLayout() {
        return R.layout.long_term_trading_time_dialog;
    }

    private void chooseTime() {
        LongTermTradingTimeChosenEvent.ChosenTime chosenTimeType = LongTermTradingTimeChosenEvent.ChosenTime.NEAREST;

        long opportunityId = 0;

        switch (chosenTime) {
            case 0: chosenTimeType = LongTermTradingTimeChosenEvent.ChosenTime.NEAREST; break;
            case 1: chosenTimeType = LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_1; opportunityId = opportunitiesId.get(chosenTime); break;
            case 2: chosenTimeType = LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_2; opportunityId = opportunitiesId.get(chosenTime); break;
            case 3: chosenTimeType = LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_3; opportunityId = opportunitiesId.get(chosenTime); break;
            case 4: chosenTimeType = LongTermTradingTimeChosenEvent.ChosenTime.DAILY; break;
            case 5: chosenTimeType = LongTermTradingTimeChosenEvent.ChosenTime.WEEKLY; break;
            case 6: chosenTimeType = LongTermTradingTimeChosenEvent.ChosenTime.MONTHLY; break;
        }

        application.postEvent(new LongTermTradingTimeChosenEvent(marketId, chosenTimeType, opportunityId));
        dismiss();
    }

    class TimeItemsAdapter extends BaseAdapter {

        private TreeMap<Integer, String> timeItems;

        public TimeItemsAdapter(TreeMap<Integer, String> timeItems) {
            this.timeItems = timeItems;
        }

        @Override
        public int getCount() {
            return timeItems.size();
        }

        @Override
        public String getItem(int position) {
            return (String)timeItems.values().toArray()[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater li = (LayoutInflater) application.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = li.inflate(R.layout.long_term_trading_time_item, null);

            TextView timeItem = (TextView) view.findViewById(R.id.long_term_trading_time_item_text_view);
            timeItem.setText(getItem(position));
            return view;
        }
    }

}
