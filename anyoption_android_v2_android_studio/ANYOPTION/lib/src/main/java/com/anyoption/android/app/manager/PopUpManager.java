package com.anyoption.android.app.manager;

import java.util.ArrayList;
import java.util.List;

import org.jcodec.PictureParameterSet.PPSExt;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.LayoutDirection;
import com.anyoption.android.R;
import com.anyoption.android.app.popup.BalanceDialogfragment;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.form.FormEditText;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

/**
 * PopUP Manager that defines and manages the pop-ups that will be displayed to the user.
 * @author Anastas Arnaudov
 *
 */
public class PopUpManager {

	public static final String TAG = PopUpManager.class.getSimpleName();
	
	protected Application application;
	private FullScreenDialog loadingPopUp;

	public static final int POPUP_QUEST_BLOCKED 				= 1;
	public static final int POPUP_QUEST_RESTRICTED 				= 2;
	public static final int POPUP_QUEST_PEP_BLOCKED 			= 3;
	public static final int POPUP_QUEST_THRESHOLD_RESTRICTED 	= 4;
	public static final int POPUP_LOW_BALANCE				 	= 5;
	public static final int POPUP_BONUSES_TO_CLAIM				= 6;
	
	public interface PopupListener{
		void onPopupShown(int popupCode);
		void onPopupDismissed(int popupCode);
	}
	
	/**
	 * Listener for pop-up with 2 buttons.
	 * @author Anastas Arnaudov
	 *
	 */
	public interface OnConfirmPopUpButtonPressedListener{
		/**
		 * Called when the positive button is pressed.
		 */
		public void onPositive();
		/**
		 * Called when the negative button is pressed.
		 */
		public void onNegative();
	}
	
	/**
	 * Listener for pop-up with 1 button.
	 * @author Anastas Arnaudov
	 *
	 */
	public interface OnAlertPopUpButtonPressedListener{
		/**
		 * Called when the button is pressed.
		 */
		public void onButtonPressed();
	}
	
	/**
	 * Listener for password confirm pop-up.
	 * @author Anastas Arnaudov
	 *
	 */
	public interface OnPasswordPopUpCommitPressedListener{
		/**
		 * Called when the commit button is pressed.
		 */
		public void onCommit(String password);
	}
	
	private List<PopupListener> popupListeners;
	private List<Integer> popupShownCodes;
	
	public PopUpManager(Application application){
		Log.d(TAG, "On Create");
		this.application = application;
		popupListeners = new ArrayList<PopUpManager.PopupListener>();
		popupShownCodes = new ArrayList<Integer>();
	}
	
	public void addPopupListener(PopUpManager.PopupListener popupListener){
		if(!popupListeners.contains(popupListener)){
			popupListeners.add(popupListener);
		}
	}
	
	public void removePopupListener(PopUpManager.PopupListener popupListener){
		popupListeners.remove(popupListener);
	}
	
	public void onPopupShown(int popupCode){
		if(!popupListeners.isEmpty()){
			for(PopUpManager.PopupListener popupListener : popupListeners){
				popupListener.onPopupShown(popupCode);
			}
		}
		if (!popupShownCodes.contains(popupCode)) {
			popupShownCodes.add(popupCode);
		}
	}
	
	public void onPopupDismissed(int popupCode){
		if(!popupListeners.isEmpty()){
			for(PopUpManager.PopupListener popupListener : popupListeners){
				popupListener.onPopupDismissed(popupCode);
			}
		}
		popupShownCodes.remove(Integer.valueOf(popupCode));
	}
	
	public boolean isPopupShown(int popupCode){
		return popupShownCodes.contains(popupCode);
	}
	
	/**
	 * Starts the loading dialog.
	 */
	public void startLoading() {
		FullScreenDialog loadingPopUp = getLoadingPopUp();
		loadingPopUp.show();			
	}
	
	/**
	 * Stops the loading dialog.
	 */
	public void stopLoading(){
		if (loadingPopUp != null){
			loadingPopUp.hide();			
		}
	}
	
	/**
	 * Returns 
	 * @return
	 */
	private FullScreenDialog getLoadingPopUp(){
		if(loadingPopUp == null || loadingPopUp.getContext() != application.getCurrentActivity()){
			Activity currentActivity = application.getCurrentActivity();
			loadingPopUp = new FullScreenDialog(currentActivity, R.layout.loading_dialog_layout);
		}
		loadingPopUp.setCancelable(false);
		return loadingPopUp;
	}
	
	/**
	 * Returns new Loading Dialog.
	 * @return
	 */
	public Dialog getNewLoadingDialog(){
		Activity currentActivity = application.getCurrentActivity();
		Dialog loadingDialog = new FullScreenDialog(currentActivity, R.layout.loading_dialog_layout);
		loadingDialog.setCancelable(false);
		return loadingDialog;
	}
	
	/**
	 * Returns Dialog that represent list of items.
	 * @param <T> The type of the items in the list.
	 * @param items The items array.
	 * @param defaultSelectedItem the default selected item.
	 * @param onClickListener Item Click Listener.
	 * @return
	 */
	public <T> AlertDialog getListPopUp(List<T> items, int defaultSelectedItem, OnClickListener onClickListener){
		AlertDialog.Builder builder = new AlertDialog.Builder(application.getCurrentActivity());
		ArrayAdapter<T> adapter = new ArrayAdapter<T>(application.getCurrentActivity(), android.R.layout.simple_spinner_item, items);
	    builder.setSingleChoiceItems(adapter, defaultSelectedItem, onClickListener);	
		return builder.create();
	}
	
	/**
	 * Displays a password confirmation popup.
	 */
	public void showPasswordPopUp(final OnPasswordPopUpCommitPressedListener onPasswordPopUpCommitPressedListener){
		LayoutDirection direction = application.getLayoutDirection();
		AlertDialog.Builder builder = new AlertDialog.Builder(application.getCurrentActivity());
		LayoutInflater inflater = application.getCurrentActivity().getLayoutInflater();
	    View view = inflater.inflate(R.layout.password_dialog_layout, null);
	    final FormEditText passwordField = (FormEditText) view.findViewById(R.id.password_dialog_password_edittext);
	    passwordField.addValidator(ValidatorType.EMPTY, ValidatorType.ALNUM, ValidatorType.PASSWORD_MINIMUM_CHARACTERS);
	    passwordField.getEditText().setImeActionLabel(application.getString(R.string.submit), EditorInfo.IME_ACTION_DONE);
	    passwordField.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);
	   
	    builder.setView(view);
	    if(direction == LayoutDirection.LEFT_TO_RIGHT){
	    	builder.setPositiveButton(application.getString(R.string.submit), new OnClickListener() {
	    		@Override
	    		public void onClick(DialogInterface dialog, int which) {}
	    	});
	    	builder.setNegativeButton(application.getString(R.string.cancel), new OnClickListener() {
	    		@Override
	    		public void onClick(DialogInterface dialog, int which) {
	    			ScreenUtils.hideKeyboard(passwordField.getEditText());
	    			dialog.dismiss();
	    		}
	    	});	    	
	    }else{
	      	builder.setNegativeButton(application.getString(R.string.submit), new OnClickListener() {
	    		@Override
	    		public void onClick(DialogInterface dialog, int which) {}
	    	});
	    	builder.setPositiveButton(application.getString(R.string.cancel), new OnClickListener() {
	    		@Override
	    		public void onClick(DialogInterface dialog, int which) {
	    			ScreenUtils.hideKeyboard(passwordField.getEditText());
	    			dialog.dismiss();
	    		}
	    	});
	    }
		
		final AlertDialog dialog = builder.create();
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
		passwordField.getEditText().setOnEditorActionListener(new OnEditorActionListener() {	
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					if(actionId == EditorInfo.IME_ACTION_DONE && passwordField.checkValidity()){
						ScreenUtils.hideKeyboard(passwordField.getEditText());
						dialog.dismiss();
						onPasswordPopUpCommitPressedListener.onCommit(passwordField.getValue().toString());	
						return false;
					}else{
						return true;
					}
				}
			});
		    
		dialog.show();
		ScreenUtils.openKeyboard();
		
		//The dialog must be shown before we get the button:
		View.OnClickListener confirmListener = new View.OnClickListener() {
			@Override
			public void onClick(View paramView) {
				if(passwordField.checkValidity()){
					ScreenUtils.hideKeyboard(passwordField.getEditText());
					dialog.dismiss();
					onPasswordPopUpCommitPressedListener.onCommit(passwordField.getValue().toString());	
				}
			}
		};
		if(direction == LayoutDirection.LEFT_TO_RIGHT){
			Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
			positiveButton.setOnClickListener(confirmListener);			
		}else{
			Button negativeButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
			negativeButton.setOnClickListener(confirmListener);
		}
	}
	
	/**
	 * Displays a Confirmation Pop-up with title, message and 2 buttons : positive and negative.
	 * @param title
	 * @param message
	 * @param onPopUpButtonPressedListener
	 */
	public void showConfirmPopUp(String title, String message, OnConfirmPopUpButtonPressedListener onPopUpButtonPressedListener){
		showConfirmPopUp(title, message, application.getString(R.string.ok), application.getString(R.string.cancel), onPopUpButtonPressedListener);
	}
	
	/**
	 * Displays a Confirmation Pop-up with title, message and 2 buttons : positive and negative.
	 * @param title
	 * @param message
	 * @param positiveButtonText
	 * @param negativeButtontext
	 * @param onPopUpButtonPressedListener
	 */
	public void showConfirmPopUp(String title, String message, String positiveButtonText, String negativeButtontext, final OnConfirmPopUpButtonPressedListener onPopUpButtonPressedListener){
		LayoutDirection direction = application.getLayoutDirection();
		AlertDialog.Builder builder = new AlertDialog.Builder(application.getCurrentActivity());
		if(title != null && !title.isEmpty()){
			builder.setTitle(title);			
		}
		builder.setMessage(message);
		if(direction == LayoutDirection.LEFT_TO_RIGHT){
			builder.setPositiveButton(positiveButtonText, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					onPopUpButtonPressedListener.onPositive();
				}
			});
			builder.setNegativeButton(negativeButtontext, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();	
					onPopUpButtonPressedListener.onNegative();
				}
			});			
		}else{
			builder.setNegativeButton(positiveButtonText, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					onPopUpButtonPressedListener.onPositive();
				}
			});
			builder.setPositiveButton(negativeButtontext, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();	
					onPopUpButtonPressedListener.onNegative();
				}
			});	
		}
		
		Dialog dialog = builder.create();
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				onPopUpButtonPressedListener.onNegative();
			}
		});
		dialog.show();
	}

	/**
	 * Displays an Alert Pop-up.
	 * @param message
	 * @param onAlertPopUpButtonPressedListener Listener for the button.
	 */
	public void showAlertPopUp(String message, final OnAlertPopUpButtonPressedListener onAlertPopUpButtonPressedListener){
		showAlertPopUp(null, message, application.getResources().getString(R.string.ok), onAlertPopUpButtonPressedListener);
	}
	
	/**
	 * Displays an Alert Pop-up.
	 * @param title
	 * @param message
	 * @param onAlertPopUpButtonPressedListener Listener for the button.
	 */
	public void showAlertPopUp(String title, String message,final OnAlertPopUpButtonPressedListener onAlertPopUpButtonPressedListener){
		showAlertPopUp(title, message, application.getResources().getString(R.string.ok), onAlertPopUpButtonPressedListener);
	}
	
	/**
	 * Displays an Alert Pop-up.
	 * @param title
	 * @param message
	 * @param buttonText text for the button.
	 * @param onAlertPopUpButtonPressedListener Listener for the button.
	 */
	public void showAlertPopUp(String title, String message, String buttonText, final OnAlertPopUpButtonPressedListener onAlertPopUpButtonPressedListener){
		AlertDialog.Builder builder = new AlertDialog.Builder(application.getCurrentActivity());
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setPositiveButton(buttonText, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(onAlertPopUpButtonPressedListener != null){
					onAlertPopUpButtonPressedListener.onButtonPressed();					
				}
				dialog.dismiss();
			}
		});
		Dialog dialog = builder.create();
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
		
		dialog.show();
	}

	public void showInsufficienFundsPopUp() {
		application.getFragmentManager().showDialogFragment(BalanceDialogfragment.class,
				new Bundle());
	}

	/**
	 * Custom Dialog.
	 * 
	 * @author anastas.arnaudov
	 * 
	 */
	public class FullScreenDialog extends Dialog{

		private Activity context;
		private int layoutID;
		
		public FullScreenDialog(Activity context, int layoutID){
			super(context);
			this.context = context;
			this.layoutID = layoutID;
		}

		@Override
		protected void onCreate (Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			LayoutInflater inflater = context.getLayoutInflater();
			View rootView = inflater.inflate(layoutID, null);
			getWindow().requestFeature(Window.FEATURE_NO_TITLE);// !!! Must be called before setContentView !!!
			
			setContentView(rootView);
			
			getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			
			int width = (int)context.getWindow().getDecorView().getWidth();
			int height = (int)context.getWindow().getDecorView().getHeight();
			getWindow().setLayout(width, height);
		}


		public void show() {
			super.show();
		}

	}
	
}
