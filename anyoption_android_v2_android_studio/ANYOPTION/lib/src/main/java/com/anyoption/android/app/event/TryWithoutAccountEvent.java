package com.anyoption.android.app.event;

/**
 * Custom event that occurs when the user enters the main part of the application without account.
 * @author Anastas Arnaudov
 *
 */
public class TryWithoutAccountEvent {

}
