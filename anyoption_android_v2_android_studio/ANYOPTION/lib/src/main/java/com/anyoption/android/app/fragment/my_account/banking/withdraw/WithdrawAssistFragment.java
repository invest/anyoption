package com.anyoption.android.app.fragment.my_account.banking.withdraw;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.BitmapUtils;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.RadioButton;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.TextView;
import com.anyoption.android.app.widget.form.Form;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.common.beans.base.WithdrawUserDetails;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.withdrawuserdetails.WithdrawUserDetailsMethodRequest;
import com.anyoption.common.service.withdrawuserdetails.WithdrawUserDetailsMethodResult;

/**
 * Future Withdrawal Assistance screen.
 * @author anastas.arnaudov
 *
 */
public class WithdrawAssistFragment extends NavigationalFragment implements CompoundButton.OnCheckedChangeListener, View.OnClickListener, Application.OnActivityResultListener {
	
	public static final String TAG = WithdrawAssistFragment.class.getSimpleName();

	public static WithdrawAssistFragment newInstance(){
		return newInstance(new Bundle());
	}

	public static WithdrawAssistFragment newInstance(Bundle bundle){
		Log.d(TAG, "Create new WithdrawAssistFragment");
		WithdrawAssistFragment fragment = new WithdrawAssistFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	protected Screenable setupScreen() {
		return Screen.WITHDRAW_ASSIST;
	}

	private View rootView;

	private RadioButton skrillRadioButton;
	private View skrillRadioButtonImage;
	private RadioButton bankWireRadioButton;

	private TextView skrillDetailsHeaderTextView;
	private TextView bankWireDetailsHeaderTextView;

	private View skrillFormLayout;
	private View bankWireFormLayout;

	private Form form;

	private FormEditText emailFormEditText;

	private FormEditText beneficiaryFormEditText;
	private FormEditText swiftFormEditText;
	private FormEditText ibanFormEditText;
	private FormEditText accountNumberFormEditText;
	private FormEditText bankNameFormEditText;
	private FormEditText branchNumberFormEditText;
	private FormEditText branchAddressFormEditText;
	private View uploadBankProtocolButton;
	private TextView uploadBankProtocolFileTextView;

	private RequestButton submitButton;
	private View skipButton;

	private String fileName;
	private Bitmap bitmap;
	private WithdrawUserDetails withdrawDetails;
	private boolean isDetailsAlreadyFilled;
	private boolean isDetailsRequestArrived;
	private boolean isSkipRequestCalled;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		form = new Form();

		application.addOnActivityResultListener(this);

		getDetailsFromServer();
	}

	@Override
	public void onDestroy() {
		application.removeOnActivityResultListener(this);

		super.onDestroy();
	}

	@Override
	public void onResume() {
		super.onResume();
		application.registerForEvents(this, NavigationEvent.class);
	}

	@Override
	public void onPause() {
		application.unregisterForAllEvents(this);
		super.onPause();
	}

	public void onEventMainThread(NavigationEvent event){
		if(event.getNavigationType() == NavigationEvent.NavigationType.UP){
			onBackPressed();
		}
	}

	private void onBackPressed() {
		Log.d(TAG, "Handle Back button press.");
		if(!isSkipRequestCalled){
			enableForms(false);
			skip();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.withdraw_assist_layout, container, false);

		skrillRadioButton 		= (RadioButton) rootView.findViewById(R.id.withdraw_assist_skrill_radio_button);
		skrillRadioButtonImage  = 				rootView.findViewById(R.id.withdraw_assist_skrill_radio_button_image);
		bankWireRadioButton 	= (RadioButton) rootView.findViewById(R.id.withdraw_assist_bank_wire_radio_button);
		bankWireRadioButton.setCompoundDrawablePadding((int) ScreenUtils.convertDpToPx(getContext(), 5));

		skrillRadioButton.setOnCheckedChangeListener(this);
		bankWireRadioButton.setOnCheckedChangeListener(this);
		skrillRadioButtonImage.setOnClickListener(this);

		skrillDetailsHeaderTextView 	= (TextView) rootView.findViewById(R.id.withdraw_assist_skrill_form_header_text_view);
		bankWireDetailsHeaderTextView 	= (TextView) rootView.findViewById(R.id.withdraw_assist_bank_wire_form_header_text_view);

		skrillFormLayout 	= rootView.findViewById(R.id.withdraw_assist_skrill_form_layout);
		bankWireFormLayout 	= rootView.findViewById(R.id.withdraw_assist_bank_wire_form_layout);

		//EMAIL:
		emailFormEditText = (FormEditText) rootView.findViewById(R.id.withdraw_assist_email_edittext);
		emailFormEditText.addValidator(ValidatorType.EMPTY, ValidatorType.EMAIL);

		//BENEFICARY NAME:
		beneficiaryFormEditText = (FormEditText) rootView.findViewById(R.id.withdraw_assist_beneficiary_name_edittext);
		beneficiaryFormEditText.addValidator(ValidatorType.EMPTY);

		//SWIFT:
		swiftFormEditText = (FormEditText) rootView.findViewById(R.id.withdraw_assist_swift_edittext);
		swiftFormEditText.addValidator(ValidatorType.EMPTY);

		//IBAN:
		ibanFormEditText = (FormEditText) rootView.findViewById(R.id.withdraw_assist_iban_edittext);
		ibanFormEditText.addValidator(ValidatorType.EMPTY);

		//ACCOUNT NUMBER:
		accountNumberFormEditText = (FormEditText) rootView.findViewById(R.id.withdraw_assist_account_number_edittext);
		accountNumberFormEditText.addValidator(ValidatorType.EMPTY, ValidatorType.DIGITS);

		//BANK NAME:
		bankNameFormEditText = (FormEditText) rootView.findViewById(R.id.withdraw_assist_bank_name_edittext);
		bankNameFormEditText.addValidator(ValidatorType.EMPTY);

		//BRANCH NUMBER:
		branchNumberFormEditText = (FormEditText) rootView.findViewById(R.id.withdraw_assist_account_branch_number_edittext);
		branchNumberFormEditText.addValidator(ValidatorType.EMPTY);

		//BRANCH ADDRESS:
		branchAddressFormEditText = (FormEditText) rootView.findViewById(R.id.withdraw_assist_account_branch_address_edittext);
		branchAddressFormEditText.addValidator(ValidatorType.EMPTY);

		//UPLOAD BANK PROTOCOL:
		uploadBankProtocolButton 		= rootView.findViewById(R.id.withdraw_assist_upload_bank_protocol_button);
		uploadBankProtocolFileTextView 	= ((TextView)rootView.findViewById(R.id.withdraw_assist_upload_bank_protocol_file_text_view));
		uploadBankProtocolButton.setOnClickListener(this);

		//Submit button:
		submitButton = (RequestButton) rootView.findViewById(R.id.withdraw_assist_submit_button);
		submitButton.setOnClickListener(this);

		//Skip button:
		skipButton = rootView.findViewById(R.id.withdraw_assist_skip_button);
		skipButton.setOnClickListener(this);

		//Initial state:
		skrillRadioButton.setChecked(true);

		if(withdrawDetails != null && withdrawDetails.getId() != 0){
			populateFormsWithExistingValues();
		}

		enableForms(isDetailsRequestArrived);

		return rootView;
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if(buttonView == skrillRadioButton){
			if(isChecked){
				Log.d(TAG, "Skrill Radio button pressed!");
				bankWireRadioButton.setChecked(false);
				setupSkrillForm();
			}
		}else if(buttonView == bankWireRadioButton){
			if(isChecked){
				Log.d(TAG, "Bank Wire Radio button pressed!");
				skrillRadioButton.setChecked(false);
				setupBankWireForm();
			}
		}
	}

	@Override
	public void onClick(View v) {
		if(v == skrillRadioButtonImage){
			Log.d(TAG, "Skrill Radio button pressed!");
			skrillRadioButton.setChecked(true);
		}else if(v == submitButton){
			Log.d(TAG, "Submit button pressed!");
			boolean isFormValid 		  = form.checkValidity();
			boolean isSkrill    		  = skrillRadioButton.isChecked();
			boolean isBitmapOK  		  = bitmap != null;
			boolean isFileAlreadyUploaded = withdrawDetails != null && withdrawDetails.getFileId() != 0;

			if(isFormValid && (isSkrill || isFileAlreadyUploaded || isBitmapOK)){
				enableForms(false);
				if(isDetailsAlreadyFilled){
					updateDetails();
				}else{
					insertDetails();
				}
			}

			if(!isSkrill && !isFileAlreadyUploaded && !isBitmapOK){
				uploadBankProtocolFileTextView.setTextColor(getResources().getColor(R.color.form_item_error, null));
				uploadBankProtocolFileTextView.setText(getString(R.string.errorMandatoryField));
			}

		}else if(v == skipButton){
			Log.d(TAG, "Skip button pressed!");
			enableForms(false);
			skip();
		}else if(v == uploadBankProtocolButton){
			Log.d(TAG, "Upload protocol button pressed!");
			openImageGallery();
		}
	}

	private void setupSkrillForm(){
		Log.d(TAG, "Setting up Skrill form.");
		bankWireDetailsHeaderTextView.setVisibility(View.GONE);
		bankWireFormLayout.setVisibility(View.GONE);

		skrillDetailsHeaderTextView.setVisibility(View.VISIBLE);
		skrillFormLayout.setVisibility(View.VISIBLE);

		form.getItems().clear();
		form.addItem(emailFormEditText);
	}

	private void setupBankWireForm(){
		Log.d(TAG, "Setting up Bank Wire form.");
		skrillDetailsHeaderTextView.setVisibility(View.GONE);
		skrillFormLayout.setVisibility(View.GONE);

		bankWireDetailsHeaderTextView.setVisibility(View.VISIBLE);
		bankWireFormLayout.setVisibility(View.VISIBLE);

		form.getItems().clear();
		form.addItem(beneficiaryFormEditText, swiftFormEditText, ibanFormEditText, accountNumberFormEditText, bankNameFormEditText, branchNumberFormEditText, branchAddressFormEditText);
	}

	private void enableForms(boolean isShouldEnable){
		if(rootView == null){
			return;
		}

		if(isShouldEnable){
			Log.d(TAG, "Enable form.");
		}else{
			Log.d(TAG, "Disable form.");
		}

		skrillRadioButton.setClickable(isShouldEnable);
		skrillRadioButtonImage.setClickable(isShouldEnable);
		bankWireRadioButton.setClickable(isShouldEnable);

		form.setEditable(isShouldEnable);

		if(isShouldEnable){
			submitButton.enable();
		}else{
			submitButton.disable();
		}

		skipButton.setClickable(isShouldEnable);
	}

	private static void openImageGallery() {
		Log.d(TAG, "Opening the gallery...");
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		Application.get().getCurrentActivity().startActivityForResult(Intent.createChooser(intent, ""), 666);
	}

	@Override
	public void onActivityResultEvent(int requestCode, int resultCode, Intent intent) {
		Log.d(TAG, "onActivityResultEvent");
		if (resultCode == Activity.RESULT_OK && requestCode == 666) {
			Log.d(TAG, "resultCode is OK");
			Uri uri = intent.getData();
			try {
				String fileName = Utils.getFileNameFromURI(application.getCurrentActivity(), uri);
				String fileExtension = Utils.getFileNameExtension(fileName);
				Log.d(TAG, "file = " + fileName);
				if (fileName.length() > 2 && (fileExtension.equalsIgnoreCase("jpg") || fileExtension.equalsIgnoreCase("png") || fileExtension.equalsIgnoreCase("jpeg"))) {
					bitmap = MediaStore.Images.Media.getBitmap(application.getCurrentActivity().getContentResolver(), uri);
					if (bitmap != null) {
						Log.d(TAG, "Bitmap is OK.");
						bitmap = BitmapUtils.resizeAndScaleDocumentBitmap(bitmap);
						this.fileName = fileName;
						uploadBankProtocolFileTextView.setTextColor(getResources().getColor(R.color.withdraw_assist_protocol_file_text_color, null));
						uploadBankProtocolFileTextView.setText(fileName);
					} else {
						Log.e(TAG, "Problem getting the bitmap.");
						application.getNotificationManager().showNotificationError(getString(R.string.error));
					}
				} else {
					Log.e(TAG, "Error. Wrong file extension.");
					application.getNotificationManager().showNotificationError(getString(R.string.error));
				}
			} catch (Exception e) {
				Log.e(TAG, "Problem getting the file. " + e.getLocalizedMessage());
			}
		}
	}

	private void setupSkrillDetailsRequest(WithdrawUserDetailsMethodRequest request){
		Log.d(TAG, "Setting up the Skrill details.");
		request.setWithdrawUserDetailsType(WithdrawUserDetailsMethodRequest.WITHDRAW_USER_DETAILS_SKRILL_TYPE);

		WithdrawUserDetails details = new WithdrawUserDetails();
		details.setUserId(application.getUser().getId());
		details.setSkrillEmail(emailFormEditText.getValue());

		request.setWithdrawUserDetails(details);
	}

	private void setupBankWireDetailsRequest(WithdrawUserDetailsMethodRequest request){
		Log.d(TAG, "Setting up the Skrill details.");
		request.setWithdrawUserDetailsType(WithdrawUserDetailsMethodRequest.WITHDRAW_USER_DETAILS_WIRE_TYPE);

		WithdrawUserDetails details = new WithdrawUserDetails();
		details.setUserId(application.getUser().getId());
		details.setBeneficiaryName(beneficiaryFormEditText.getValue());
		details.setSwift(swiftFormEditText.getValue());
		details.setIban(ibanFormEditText.getValue());
		details.setAccountNum(Long.valueOf(accountNumberFormEditText.getValue()));
		details.setBankName(bankNameFormEditText.getValue());
		details.setBranchNumber(Long.valueOf(branchNumberFormEditText.getValue()));
		details.setBranchAddress(branchAddressFormEditText.getValue());

		request.setWithdrawUserDetails(details);
	}

	private void populateFormsWithExistingValues(){
		Log.d(TAG, "Populating form with existing details.");
		emailFormEditText.setValue(withdrawDetails.getSkrillEmail());
		beneficiaryFormEditText.setValue(withdrawDetails.getBeneficiaryName());
		swiftFormEditText.setValue(withdrawDetails.getSwift());
		ibanFormEditText.setValue(withdrawDetails.getIban());
		accountNumberFormEditText.setValue(withdrawDetails.getAccountNum());
		bankNameFormEditText.setValue(withdrawDetails.getBankName());
		branchNumberFormEditText.setValue(withdrawDetails.getBranchNumber());
		branchAddressFormEditText.setValue(withdrawDetails.getBranchAddress());

		if(withdrawDetails.getFileName() != null){
			uploadBankProtocolFileTextView.setText(withdrawDetails.getFileName());
		}
	}

	private void goBack(){
		application.getCurrentActivity().onBackPressed();
	}

	//////////////////////////////////////////////////////////////////////////////////
	//							REQUESTs and RESPONSEs								//
	//////////////////////////////////////////////////////////////////////////////////

	private void getDetailsFromServer(){
		Log.d(TAG, "Getting the withdraw details from server...");
		WithdrawUserDetailsMethodRequest request = new WithdrawUserDetailsMethodRequest();
		WithdrawUserDetails details = new WithdrawUserDetails();
		details.setUserId(application.getUser().getId());
		request.setWithdrawUserDetails(details);

		application.getCommunicationManager().requestService(this, "getDetailsCallback", Constants.SERVICE_GET_WITHDRAW_DETAILS, request, WithdrawUserDetailsMethodResult.class, false, false);
	}

	public void getDetailsCallback(Object resultObj){
		Log.d(TAG, "Withdraw details callback.");
		isDetailsRequestArrived = true;

		WithdrawUserDetailsMethodResult result = (WithdrawUserDetailsMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			enableForms(true);
			withdrawDetails = result.getWithdrawUserDetails();
			if(withdrawDetails != null){
				Log.d(TAG, "Saving details from server.");
				Log.d(TAG, "Checking if there are already some details filled in...");
				if(withdrawDetails.getId() != 0){
					Log.d(TAG, "There are already some details filled in!");
					isDetailsAlreadyFilled = true;
					if(rootView != null){
						populateFormsWithExistingValues();
					}
				}else{
					Log.d(TAG, "The details are NOT filled, yet!");
					isDetailsAlreadyFilled = false;
				}
			}else{
				Log.d(TAG, "No details from server.");
			}
		} else {
			ErrorUtils.handleResultError(result);
		}
	}

	private void insertDetails(){
		Log.d(TAG, "Inserting details to server...");
		WithdrawUserDetailsMethodRequest request = new WithdrawUserDetailsMethodRequest();

		if(skrillRadioButton.isChecked()){
			setupSkrillDetailsRequest(request);
		}else{
			setupBankWireDetailsRequest(request);
		}

		if(!application.getCommunicationManager().requestService(this, "insertDetailsCallback", Constants.SERVICE_INSERT_WITHDRAW_DETAILS, request, MethodResult.class, false, false)){
			enableForms(true);
		}
	}

	public void insertDetailsCallback(Object resultObj){
		Log.d(TAG, "Insert details callback.");
		MethodResult result = (MethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Insert details callback OK.");
			//Check if Bank Wire:
			if(bankWireRadioButton.isChecked()){
				uploadFile();
			}else{
				goBack();
			}
		} else {
			enableForms(true);
			ErrorUtils.handleResultError(result);
		}
	}

	private void updateDetails(){
		Log.d(TAG, "Updating details to server...");
		WithdrawUserDetailsMethodRequest request = new WithdrawUserDetailsMethodRequest();

		if(skrillRadioButton.isChecked()){
			setupSkrillDetailsRequest(request);
		}else{
			setupBankWireDetailsRequest(request);
		}

		if(!application.getCommunicationManager().requestService(this, "updateDetailsCallback", Constants.SERVICE_UPDATE_WITHDRAW_DETAILS, request, MethodResult.class, false, false)){
			enableForms(true);
		}
	}

	public void updateDetailsCallback(Object resultObj){
		Log.d(TAG, "Updating details callback.");
		MethodResult result = (MethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Updating details callback OK.");
			//Check if Bank Wire:
			if(bankWireRadioButton.isChecked()){
				uploadFile();
			}else{
				goBack();
			}
		} else {
			enableForms(true);
			ErrorUtils.handleResultError(result);
		}
	}

	private void uploadFile(){
		Log.d(TAG, "Uploading file to server...");
		application.getCommunicationManager().uploadDocumentImage(Constants.UPLOAD_DOCUMENTS_TYPE_BANK_PROTOCOL, fileName, 0, bitmap, this, "uploadFileCallback");
	}

	public void uploadFileCallback(Object resultObj){
		Log.d(TAG, "Uploading file callback.");
		MethodResult result = (MethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Uploading file callback OK.");
			goBack();
		} else {
			enableForms(true);
			ErrorUtils.handleResultError(result);
		}
	}

	private void skip(){
		Log.d(TAG, "Skipping details...");
		WithdrawUserDetailsMethodRequest request = new WithdrawUserDetailsMethodRequest();
		WithdrawUserDetails details = new WithdrawUserDetails();
		details.setUserId(application.getUser().getId());
		request.setWithdrawUserDetails(details);

		if(!application.getCommunicationManager().requestService(this, "skipCallback", Constants.SERVICE_SKIP_WITHDRAW_DETAILS, request, MethodResult.class, false, false)){
			enableForms(true);
		}
	}

	public void skipCallback(Object resultObj){
		Log.d(TAG, "Skip details callback.");
		isSkipRequestCalled = true;
		MethodResult result = (MethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Skip details callback OK.");
		} else {
			if(result == null){
				Log.e(TAG, "Skip details callback result is null");
			}else{
				Log.e(TAG, "Skip details callback errorCode=" + result.getErrorCode());
			}
		}
		goBack();
	}

	//////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////

}
