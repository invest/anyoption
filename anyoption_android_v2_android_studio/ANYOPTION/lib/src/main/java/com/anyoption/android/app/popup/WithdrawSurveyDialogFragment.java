package com.anyoption.android.app.popup;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.CompoundButton;
import android.widget.ScrollView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.AnimationUtils;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.EditText;
import com.anyoption.android.app.widget.RadioButton;

public class WithdrawSurveyDialogFragment extends BaseDialogFragment implements CompoundButton.OnCheckedChangeListener{

    public static final String TAG = WithdrawSurveyDialogFragment.class.getSimpleName();

    protected static final long FIRST_ANSWER_ID     = 402;
    protected static final long SECOND_ANSWER_ID    = 403;
    protected static final long THIRD_ANSWER_ID     = 404;
    protected static final long FOURTH_ANSWER_ID    = 405;

    protected com.anyoption.android.app.widget.TextView firstMessageTextView;
    protected com.anyoption.android.app.widget.TextView secondMessageTextView;
    protected com.anyoption.android.app.widget.TextView thirdMessageTextView;

    protected com.anyoption.android.app.widget.TextView questionTextView;

    protected View errorMessageView;
    protected ScrollView scrollView;

    protected View submitButton;
    protected View skipButton;

    protected RadioButton firstAnswerRadioButton;
    protected RadioButton secondAnswerRadioButton;
    protected RadioButton thirdAnswerRadioButton;
    protected RadioButton fourthAnswerRadioButton;

    protected EditText fourthAnswerEditText;

    protected long currentAnswer = 0;

    protected boolean isOnWithdrawSurveyClosedCalled = false;

    protected int thirdMessageTextViewHeightSize;
    protected int fourthAnswerEditTextHeightSize;

    public OnWithdrawSurveyClosed onWithdrawSurveyClosedListener;

    public interface OnWithdrawSurveyClosed {
        void onWithdrawSurveyClosed(boolean isSkiped, long chosenAnswer, String answerText);
    }

    public static WithdrawSurveyDialogFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new WithdrawSurveyDialogFragment.");
        WithdrawSurveyDialogFragment fragment = new WithdrawSurveyDialogFragment();
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
        bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
        bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            setCancelable(args.getBoolean(Constants.EXTRA_CANCELABLE, false));
        }
    }

    /**
     * Set up content.
     *
     * @param contentView
     */
    @Override
    protected void onCreateContentView(View contentView) {
        firstMessageTextView = (com.anyoption.android.app.widget.TextView) contentView.findViewById(R.id.withdraw_survey_popup_first_message);
        secondMessageTextView = (com.anyoption.android.app.widget.TextView) contentView.findViewById(R.id.withdraw_survey_popup_second_message);
        thirdMessageTextView = (com.anyoption.android.app.widget.TextView) contentView.findViewById(R.id.withdraw_survey_popup_third_message);
        questionTextView = (com.anyoption.android.app.widget.TextView) contentView.findViewById(R.id.withdraw_survey_popup_question);
        errorMessageView =  contentView.findViewById(R.id.withdraw_survey_popup_error_message);

        scrollView =  (ScrollView) contentView.findViewById(R.id.withdraw_survey_popup_scroll_view);

        submitButton = contentView.findViewById(R.id.withdraw_survey_popup_submit_button);
        skipButton = contentView.findViewById(R.id.withdraw_survey_popup_skip_button);

        firstAnswerRadioButton = (com.anyoption.android.app.widget.RadioButton) contentView.findViewById(R.id.withdraw_survey_popup_first_answer);
        secondAnswerRadioButton = (com.anyoption.android.app.widget.RadioButton) contentView.findViewById(R.id.withdraw_survey_popup_second_answer);
        thirdAnswerRadioButton = (com.anyoption.android.app.widget.RadioButton) contentView.findViewById(R.id.withdraw_survey_popup_third_answer);
        fourthAnswerRadioButton = (com.anyoption.android.app.widget.RadioButton) contentView.findViewById(R.id.withdraw_survey_popup_fourth_answer);

        fourthAnswerEditText = (com.anyoption.android.app.widget.EditText) contentView.findViewById(R.id.withdraw_survey_popup_fourth_answer_text);

        setupOtherAnswerEditTextFocus(contentView);

        fourthAnswerEditTextHeightSize = (int) getResources().getDimension(R.dimen.withdraw_survey_popup_fourth_answer_edit_text_size);

        fillTheFirstName(contentView);

        boldTexts();

        firstAnswerRadioButton.setOnCheckedChangeListener(this);
        secondAnswerRadioButton.setOnCheckedChangeListener(this);
        thirdAnswerRadioButton.setOnCheckedChangeListener(this);
        fourthAnswerRadioButton.setOnCheckedChangeListener(this);

        isOnWithdrawSurveyClosedCalled = false;

        submitButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentAnswer == FIRST_ANSWER_ID || currentAnswer == SECOND_ANSWER_ID || currentAnswer == THIRD_ANSWER_ID || currentAnswer == FOURTH_ANSWER_ID) {
                    if(onWithdrawSurveyClosedListener != null) {
                        onWithdrawSurveyClosedListener.onWithdrawSurveyClosed(false, currentAnswer, currentAnswer == FOURTH_ANSWER_ID ? fourthAnswerEditText.getText().toString().trim() : "");
                        isOnWithdrawSurveyClosedCalled = true;
                    } else {
                        onWithdrawSurveyClosedListener.onWithdrawSurveyClosed(true, 0l, "");
                        isOnWithdrawSurveyClosedCalled = true;
                    }
                    errorMessageView.setVisibility(View.GONE);
                    dismiss();
                } else {
                    errorMessageView.setVisibility(View.VISIBLE);
                    errorMessageView.getHandler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            scrollView.fullScroll(ScrollView.FOCUS_DOWN);
                        }
                    }, 500);
                    scrollView.fullScroll(ScrollView.FOCUS_DOWN);

                }
            }
        });


        skipButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        setCancelable(false);
    }

    protected void fillTheFirstName(View contentView) {
        firstMessageTextView.setText(firstMessageTextView.getText().toString().replace("{username}", Application.get().getUser().getFirstName()));
    }

    protected void boldTexts(){
        TextUtils.decorateInnerText(firstMessageTextView, null, firstMessageTextView.getText().toString(), firstMessageTextView.getText().toString(), FontsUtils.Font.ROBOTO_MEDIUM, 0, 0);
        TextUtils.decorateInnerText(questionTextView, null, questionTextView.getText().toString(), questionTextView.getText().toString(), FontsUtils.Font.ROBOTO_MEDIUM, 0, 0);
    }

    protected void setupOtherAnswerEditTextFocus(View contentView) {
        firstMessageTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ScreenUtils.hideKeyboard(fourthAnswerEditText);
            }
        });
        secondMessageTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ScreenUtils.hideKeyboard(fourthAnswerEditText);
            }
        });
        thirdMessageTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ScreenUtils.hideKeyboard(fourthAnswerEditText);
            }
        });
        questionTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ScreenUtils.hideKeyboard(fourthAnswerEditText);
            }
        });
        errorMessageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ScreenUtils.hideKeyboard(fourthAnswerEditText);
            }
        });
        contentView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v != fourthAnswerEditText) {
                    ScreenUtils.hideKeyboard(fourthAnswerEditText);
                }
            }
        });
    }

    @Override
    protected int getContentLayout() {
        return R.layout.popup_withdraw_survey;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            if (buttonView.getId() == firstAnswerRadioButton.getId()) {
                if (currentAnswer == FOURTH_ANSWER_ID) {
                    hideLastAnswerEditText(buttonView);
                }
                currentAnswer = FIRST_ANSWER_ID;
            } else if (buttonView.getId() == secondAnswerRadioButton.getId()) {
                if (currentAnswer == FOURTH_ANSWER_ID) {
                    hideLastAnswerEditText(buttonView);
                }
                currentAnswer = SECOND_ANSWER_ID;
            } else if (buttonView.getId() == thirdAnswerRadioButton.getId()) {
                if (currentAnswer == FOURTH_ANSWER_ID) {
                    hideLastAnswerEditText(buttonView);
                }
                currentAnswer = THIRD_ANSWER_ID;
            } else if (buttonView.getId() == fourthAnswerRadioButton.getId()) {
                if (currentAnswer != FOURTH_ANSWER_ID) {
                    showLastAnswerEditText();
                } else {
                    ScreenUtils.hideKeyboard(fourthAnswerEditText);
                }
                currentAnswer = FOURTH_ANSWER_ID;
            } else {
                currentAnswer = 0;
            }
        }
    }

    protected void showLastAnswerEditText() {
        if (thirdMessageTextViewHeightSize == 0) {
            thirdMessageTextViewHeightSize = thirdMessageTextView.getHeight();
        }

        Animation collapseAnimation = new AnimationUtils.HeightAnimation(thirdMessageTextView, thirdMessageTextViewHeightSize, 0);
        collapseAnimation.setDuration(1000);

        Animation expandAnimation = new AnimationUtils.HeightAnimation(fourthAnswerEditText, 0, fourthAnswerEditTextHeightSize);
        expandAnimation.setDuration(1000);

        thirdMessageTextView.startAnimation(collapseAnimation);
        fourthAnswerEditText.startAnimation(expandAnimation);

        fourthAnswerEditText.requestFocus();
        ScreenUtils.hideKeyboard(fourthAnswerEditText);

    }

    protected void hideLastAnswerEditText(final View view) {
        Animation expandAnimation = new AnimationUtils.HeightAnimation(thirdMessageTextView, 0, thirdMessageTextViewHeightSize);
        expandAnimation.setDuration(1000);

        Animation collapseAnimation = new AnimationUtils.HeightAnimation(fourthAnswerEditText, fourthAnswerEditTextHeightSize, 0);
        collapseAnimation.setDuration(1000);

        thirdMessageTextView.startAnimation(expandAnimation);
        fourthAnswerEditText.startAnimation(collapseAnimation);

        ScreenUtils.hideKeyboard(fourthAnswerEditText);

        view.requestFocus();
    }

    public OnWithdrawSurveyClosed getOnWithdrawSurveyClosedListener() {
        return onWithdrawSurveyClosedListener;
    }

    public void setOnWithdrawSurveyClosedListener(OnWithdrawSurveyClosed onWithdrawSurveyClosedListener) {
        this.onWithdrawSurveyClosedListener = onWithdrawSurveyClosedListener;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if(!isOnWithdrawSurveyClosedCalled) {
            if(onWithdrawSurveyClosedListener != null) {
                onWithdrawSurveyClosedListener.onWithdrawSurveyClosed(true, 0l, "");
            }
            isOnWithdrawSurveyClosedCalled = true;
        }
        super.onDismiss(dialog);
    }
}
