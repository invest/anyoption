package com.anyoption.android.app.event;

/**
 * Custom event that is fired when the user is updated.
 * @author Anastas Arnaudov
 *
 */
public class UserUpdatedEvent {

}
