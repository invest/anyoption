package com.anyoption.android.app.event;

/**
 * Event that occurs when the USER manually changes the language using the Language Selector.
 * @author Anastas Arnaudov
 *
 */
public class LanguageChangeEvent {

	private String languageCode;
	private boolean shouldGoToMain = false;
	/**
	 * Event that occurs when the USER manually changes the language using the Language Selector.
	 * @author Anastas Arnaudov
	 *
	 */
	public LanguageChangeEvent(String language){
		this.setLanguageCode(language);
	}

	/**
	 * Event that occurs when the USER manually changes the language using the Language Selector.
	 * @author Anastas Arnaudov
	 *
	 */
	public LanguageChangeEvent(String language, boolean shouldGoToMain){
		this.setLanguageCode(language);
		this.setShouldGoToMain(shouldGoToMain);
	}
	
	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public boolean isShouldGoToMain() {
		return shouldGoToMain;
	}

	public void setShouldGoToMain(boolean shouldGoToMain) {
		this.shouldGoToMain = shouldGoToMain;
	}
	
}
