package com.anyoption.android.app.util;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.popup.QuestionnairePEPBlockedOkDialogFragment;
import com.anyoption.android.app.popup.QuestionnaireRestrictedOkDialogFragment;
import com.anyoption.android.app.popup.QuestionnaireThresholdRestrictedOkDialogFragment;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.beans.base.UserRegulationBase;

public class RegulationUtils {

	private static final int REGULATION_STEP_DONE = 7;

	public static User getUser() {
		return Application.get().getUser();
	}

	public static UserRegulationBase getUserRegulation() {
		return Application.get().getUserRegulation();
	}

	public static boolean isSuspended() {
		return getUserRegulation().isSuspended();
	}

	public static boolean isSuspendedForDocuments() {
		return getUserRegulation().isSuspendedDueDocuments();
	}

	public static boolean isAfterFirstDeposit() {
		UserRegulationBase userRegulationBase = getUserRegulation();
		if (userRegulationBase != null) {
			Integer regulationStep = getUserRegulation().getApprovedRegulationStep();
			if (regulationStep == null) {
				return false;
			}
			return regulationStep >= UserRegulationBase.REGULATION_FIRST_DEPOSIT;
		}
		return false;
	}

	public static void setAfterFirstDeposit() {
		UserRegulationBase regulation = getUserRegulation();
		Integer regulationStep = regulation.getApprovedRegulationStep();
		if (regulationStep == null || regulationStep < UserRegulationBase.REGULATION_FIRST_DEPOSIT) {
			regulation.setApprovedRegulationStep(UserRegulationBase.REGULATION_FIRST_DEPOSIT);
		}
	}

	public static boolean isMandQuestionnaireAnswered() {
		int regulationStep = getUserRegulation().getApprovedRegulationStep();
		return regulationStep >= UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE;
	}

	public static boolean isOptionalQuestionnaireAnswered() {
		return getUserRegulation().isOptionalQuestionnaireDone();
	}

	public static boolean isWcApproval() {
		if (getUserRegulation() != null) {
			return getUserRegulation().isWcApproval();
		}
		return true;
	}

	public static long getThresholdBlock() {
		if (getUserRegulation() != null) {
			return getUserRegulation().getThresholdBlock();
		}
		return 0;
	}

	public static boolean isQuestionnaireFilled() {
		UserRegulationBase regulationBase = Application.get().getUserRegulation();
		if (regulationBase != null) {
			if (isMandQuestionnaireAnswered()) {
				return true;
			}
		}
		return false;
	}

	public static boolean showQuestionnaire() {
		if (!Application.get().isEtrader()) {
			UserRegulationBase regulationBase = Application.get().getUserRegulation();
			if (regulationBase != null) {
				if (Application.get().isRegulated() && !isQuestionnaireFilled() && (isAfterFirstDeposit() || regulationBase.getApprovedRegulationStep() >= UserRegulationBase.REGULATION_FIRST_DEPOSIT)) {
					if (regulationBase.getRegulationVersion() == 2) {
						Application.get().postEvent(new NavigationEvent(Screen.QUESTIONNAIRE, NavigationType.INIT));
					} else {
						Application.get().postEvent(new NavigationEvent(Screen.MANDATORY_QUESTIONNAIRE, NavigationType.INIT));
					}
					return false;
				}
			}
		}
		return true;
	}

	public static boolean showBlockedPopup() {
		/*if (!Application.get().isEtrader()) {
			UserRegulationBase regulationBase = Application.get().getUserRegulation();
			if (regulationBase != null) {
				if (regulationBase.isRestricted()) {
					Application.get().getFragmentManager().showDialogFragment(QuestionnaireRestrictedOkDialogFragment.class,
							null);//CHANGED FROM QuestionnaireBlockedOkDialogFragment
					return false;
				}
			}
		}
		return true;*/
		return showRestrictedPopup();
	}

	public static boolean showRestrictedPopup() {
		if (!Application.get().isEtrader()) {
			UserRegulationBase regulationBase = Application.get().getUserRegulation();
			if (regulationBase != null) {
				if (regulationBase.isRestricted() && isQuestionnaireFilled()) {
					Application.get().getFragmentManager().showDialogFragment(QuestionnaireRestrictedOkDialogFragment.class,
							null);
					return false;
				}
			}
		}
		return true;
	}

	public static boolean showPEPPopup() {
		if (!Application.get().isEtrader()) {
			UserRegulationBase regulationBase = Application.get().getUserRegulation();
			if (regulationBase != null) {
				if ((regulationBase.getPepState() == 1 || regulationBase.getPepState() == 2) && isQuestionnaireFilled()) {
					Application.get().getFragmentManager()
							.showDialogFragment(QuestionnairePEPBlockedOkDialogFragment.class, null);
					return false;
				}
			}
		}
		return true;
	}

	public static boolean showThresholdRestrictedPopup() {
		if (!Application.get().isEtrader()) {
			UserRegulationBase regulationBase = Application.get().getUserRegulation();
			if (regulationBase != null) {
				if (regulationBase.isTresholdBlock() && isQuestionnaireFilled()) {
					Application.get().getFragmentManager().showDialogFragment(QuestionnaireThresholdRestrictedOkDialogFragment.class,
							null);
					return false;
				}
			}
		}
		return true;
	}

	public static boolean showAccountBlocked() {
		if (!Application.get().isEtrader()) {
			UserRegulationBase regulationBase = Application.get().getUserRegulation();
			if (regulationBase != null) {
				if (regulationBase.isSuspendedDueDocuments() && isQuestionnaireFilled()) {
					Application.get().postEvent(new NavigationEvent(Screen.ACCOUNT_BLOCKED, NavigationType.INIT));
					return false;
				}
			}
		}
		return true;
	}

	public static boolean showUploadDocuments() {
		if (!Application.get().isEtrader()) {
			UserRegulationBase regulationBase = Application.get().getUserRegulation();
			if (regulationBase != null) {
				if (regulationBase.showWarningDueDocuments() && isQuestionnaireFilled()) {
					Application.get().postEvent(new NavigationEvent(Screen.UPLOAD_DOCUMENTS, NavigationType.INIT));
					return false;
				}
			}
		}
		return true;
	}

	public static boolean showBlockedOrPEPOrRestrictedPopups() {
		if (!Application.get().isEtrader()) {
			UserRegulationBase regulationBase = Application.get().getUserRegulation();
			if (regulationBase != null) {
				/*if (regulationBase.isRestricted()) {
					Application.get().getFragmentManager().showDialogFragment(QuestionnaireRestrictedOkDialogFragment.class,
							null);//CHANGED FROM QuestionnaireBlockedOkDialogFragment
					return false;
				} else */
				if (regulationBase.isRestricted() && isQuestionnaireFilled()) {
					Application.get().getFragmentManager().showDialogFragment(QuestionnaireRestrictedOkDialogFragment.class,
							null);
					return false;
				} else if (regulationBase.getPepState() != null && (regulationBase.getPepState() == 1 || regulationBase.getPepState() == 2) && isQuestionnaireFilled()) {
					Application.get().getFragmentManager().showDialogFragment(QuestionnairePEPBlockedOkDialogFragment.class,
							null);
					return false;
				}
			}
		}
		return true;
	}

	public static boolean showBlockedOrPEPOrRestrictedOrThresholdPopups() {
		if (!Application.get().isEtrader()) {
			UserRegulationBase regulationBase = Application.get().getUserRegulation();
			if (regulationBase != null) {
				/*if (regulationBase.isRestricted()) {
					Application.get().getFragmentManager().showDialogFragment(QuestionnaireRestrictedOkDialogFragment.class,
							null);//CHANGED FROM QuestionnaireBlockedOkDialogFragment
					return false;
				} else */
				if (regulationBase.isRestricted() && isQuestionnaireFilled()) {
					Application.get().getFragmentManager().showDialogFragment(QuestionnaireRestrictedOkDialogFragment.class,
							null);
					return false;
				} else if (regulationBase.getPepState() != null && (regulationBase.getPepState() == 1 || regulationBase.getPepState() == 2) && isQuestionnaireFilled()) {
					Application.get().getFragmentManager().showDialogFragment(QuestionnairePEPBlockedOkDialogFragment.class,
							null);
					return false;
				} else if (regulationBase.isTresholdBlock() && isQuestionnaireFilled()) {
					Application.get().getFragmentManager().showDialogFragment(QuestionnaireThresholdRestrictedOkDialogFragment.class,
							null);
					return false;
				}
			}
		}
		return true;
	}

	public static boolean checkRegulationStatusAndDoNecessary(boolean isWithdraw) {
		if (!Application.get().isEtrader()) {
			UserRegulationBase regulationBase = Application.get().getUserRegulation();
			if (regulationBase != null) {
				if (regulationBase.getApprovedRegulationStep() == REGULATION_STEP_DONE) {
					return true;
				} else if (!showQuestionnaire()) {
					return false;
				} else if (regulationBase.isRestricted() && isQuestionnaireFilled()) {
					Application.get().getFragmentManager()
							.showDialogFragment(QuestionnaireRestrictedOkDialogFragment.class, null);
					return false;
				}/* else if (regulationBase.isRestricted()) {
					if (isWithdraw) {
						return true;
					} else {
						Application.get().getFragmentManager()
								.showDialogFragment(QuestionnaireRestrictedOkDialogFragment.class, null);//CHANGED FROM QuestionnaireBlockedOkDialogFragment
						return false;
					}
				}*/ else if (regulationBase.isSuspendedDueDocuments() && isQuestionnaireFilled()) {
					Application.get().postEvent(new NavigationEvent(Screen.ACCOUNT_BLOCKED, NavigationType.INIT));
					return false;
				} /*else if (regulationBase.showWarningDueDocuments() && isQuestionnaireFilled()) {
                    Application.get().postEvent(new NavigationEvent(Screen.UPLOAD_DOCUMENTS, NavigationType.INIT));
					return false;
				}*/
			}
		}
		return true;
	}

	public static boolean checkRegulationAndPepAndThresholdStatusAndDoNecessary(boolean isWithdraw) {
		if (!Application.get().isEtrader()) {
			UserRegulationBase regulationBase = Application.get().getUserRegulation();
			if (regulationBase != null) {
				if (regulationBase.getApprovedRegulationStep() != REGULATION_STEP_DONE) {
					if (!showQuestionnaire()) {
						return false;
					}/* else if (regulationBase.isRestricted()) {
						if (isWithdraw) {
							return true;
						} else {
							Application.get().getFragmentManager()
									.showDialogFragment(QuestionnaireRestrictedOkDialogFragment.class, null);//CHANGED FROM QuestionnaireBlockedOkDialogFragment
							return false;
						}
					}*/ else if (regulationBase.isRestricted() && isQuestionnaireFilled()) {
						Application.get().getFragmentManager()
								.showDialogFragment(QuestionnaireRestrictedOkDialogFragment.class, null);
						return false;
					} else if (regulationBase.getPepState() != null && (regulationBase.getPepState() == 1 || regulationBase.getPepState() == 2) && isQuestionnaireFilled()) {
						Application.get().getFragmentManager()
								.showDialogFragment(QuestionnairePEPBlockedOkDialogFragment.class, null);
						return false;
					}
				}

				if (regulationBase.isTresholdBlock() && isQuestionnaireFilled()) {
					Application.get().getFragmentManager()
							.showDialogFragment(QuestionnaireThresholdRestrictedOkDialogFragment.class, null);
					return false;
				}

				if (regulationBase.getApprovedRegulationStep() != REGULATION_STEP_DONE) {
					if (regulationBase.isSuspendedDueDocuments() && isQuestionnaireFilled()) {
						Application.get().postEvent(new NavigationEvent(Screen.ACCOUNT_BLOCKED, NavigationType.INIT));
						return false;
					} /*else if (regulationBase.showWarningDueDocuments() && isQuestionnaireFilled()) {
                    Application.get().postEvent(new NavigationEvent(Screen.UPLOAD_DOCUMENTS, NavigationType.INIT));
					return false;
				}*/
				}
			}
		}
		return true;
	}

	public static boolean isRegulationDone() {
		if (!Application.get().isEtrader()) {
			UserRegulationBase regulationBase = Application.get().getUserRegulation();
			if (regulationBase != null) {
				if (regulationBase.getApprovedRegulationStep() == REGULATION_STEP_DONE) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean shouldShowRegulationDialogs() {
		if (!Application.get().isEtrader()) {
			UserRegulationBase regulationBase = Application.get().getUserRegulation();
			if (regulationBase != null) {
				if (regulationBase.getApprovedRegulationStep() != REGULATION_STEP_DONE) {
					if (!isQuestionnaireFilled() && (isAfterFirstDeposit() || regulationBase.getApprovedRegulationStep() >= UserRegulationBase.REGULATION_FIRST_DEPOSIT)) {
						return true;
					}/* else if (regulationBase.isRestricted()) {
						return true;
					}*/ else if (regulationBase.isRestricted() && isQuestionnaireFilled()) {
						return true;
					} else if (regulationBase.getPepState() != null && (regulationBase.getPepState() == 1 || regulationBase.getPepState() == 2) && isQuestionnaireFilled()) {
						return true;
					}
				}

				if (regulationBase.isTresholdBlock() && isQuestionnaireFilled()) {
					return true;
				}

				if (regulationBase.getApprovedRegulationStep() != REGULATION_STEP_DONE) {
					if (regulationBase.isSuspendedDueDocuments() && isQuestionnaireFilled()) {
						return true;
					} else if (regulationBase.showWarningDueDocuments() && isQuestionnaireFilled()) {
						return true;
					}
				}
			}
		}
		return false;
	}

}
