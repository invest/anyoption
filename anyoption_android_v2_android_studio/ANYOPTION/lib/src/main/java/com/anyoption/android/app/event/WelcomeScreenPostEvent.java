package com.anyoption.android.app.event;

import com.anyoption.android.app.Application.Screenable;

/**
 * Event that occurs after the user has read the welcome screens.
 * @author Anastas Arnaudov
 *
 */
public class WelcomeScreenPostEvent {

	private Screenable screen;
	
	/**
	 * 
	 * @param screen the Screenable to navigate to after the welcome screens.
	 */
	public WelcomeScreenPostEvent(Screenable screen){
		this.setScreen(screen);
	}

	public Screenable getScreen() {
		return screen;
	}

	public void setScreen(Screenable screen) {
		this.screen = screen;
	}
	
}
