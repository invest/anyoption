package com.anyoption.android.app.widget;

import com.anyoption.android.R;
import com.anyoption.android.app.util.ScreenUtils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.text.Layout;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Custom {@link Switch}.
 * @author Anastas Arnaudov
 *
 */
public class Switch extends RelativeLayout{
	
	public interface StateChangeListener{
		public void onCheckedChange(boolean isChecked);
	}

	private View rootView;
	private android.widget.Switch switchView;
	private TextView textView;
	private StateChangeListener stateChangeListener;
	private boolean isChecked;
	private String textOn;
	private String textOff;
	private Layout mOnLayout;
	private Layout mOffLayout;
	
	public Switch(Context context) {
		super(context);
		init(context, null, 0);
	}
	
	public Switch(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	
	public Switch(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle){
		rootView = View.inflate(context, R.layout.switch_layout, this);
		textView = (TextView) rootView.findViewById(R.id.textView);
		switchView = (android.widget.Switch) rootView.findViewById(R.id.switchView);
		switchView.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				setupState(isChecked);
				if(stateChangeListener != null){
					stateChangeListener.onCheckedChange(isChecked);
				}
			}
		});

		//Parse custom attributes:
		TypedArray attributesTypedArray = context.obtainStyledAttributes(attrs,R.styleable.Switch);
		String textOn = attributesTypedArray.getString(R.styleable.Switch_textOn);
		if(textOn == null){
			//Default text
			this.textOn = getResources().getString(R.string.on);
		}
		String textOff = attributesTypedArray.getString(R.styleable.Switch_textOff);
		if(textOff == null){
			//Default text
			this.textOff = getResources().getString(R.string.off);
		}
		setChecked(attributesTypedArray.getBoolean(R.styleable.Switch_isChecked, false));
		
		attributesTypedArray.recycle();
	
		setupState(isChecked());
	}
	
	@Override
	@SuppressLint({ "NewApi", "DrawAllocation" })
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		if (mOnLayout == null) {
            mOnLayout = ScreenUtils.makeLayout(textOn, (int)textView.getTextSize());
            mOffLayout = ScreenUtils.makeLayout(textOff, (int)textView.getTextSize());
            int maxTextWidth = Math.max(mOnLayout.getWidth(), mOffLayout.getWidth());
            if(maxTextWidth > switchView.getMeasuredWidth()/2){
            	int currentapiVersion = android.os.Build.VERSION.SDK_INT;
            	if (currentapiVersion >= android.os.Build.VERSION_CODES.JELLY_BEAN){
            		switchView.setSwitchMinWidth(maxTextWidth*2);
            	} else{
            	    //TODO
            	}
            	switchView.setLayoutParams(new LayoutParams(maxTextWidth*2, LayoutParams.WRAP_CONTENT));
            	LayoutParams params = new LayoutParams(maxTextWidth*2, LayoutParams.WRAP_CONTENT);
            	params.addRule(RelativeLayout.CENTER_VERTICAL);
            	textView.setLayoutParams(params);
            }
		
		}

	}
	
	private void setupState(boolean isChecked){
		if(isChecked){
			textView.setText(textOn);
			textView.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);					
		}else{
			textView.setText(textOff);
			textView.setGravity(Gravity.RIGHT|Gravity.CENTER_VERTICAL);	
		}
		textView.invalidate();
	}

	public StateChangeListener getStateChangeListener() {
		return stateChangeListener;
	}

	public void setStateChangeListener(StateChangeListener stateChangeListener) {
		this.stateChangeListener = stateChangeListener;
	}

	public boolean isChecked() {
		return isChecked;
	}

	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
		this.switchView.setChecked(isChecked);
	}
	
	public void toggle(){
		setChecked(!this.isChecked);
	}
}
