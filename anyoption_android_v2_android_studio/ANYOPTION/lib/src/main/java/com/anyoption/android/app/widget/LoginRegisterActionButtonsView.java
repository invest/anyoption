package com.anyoption.android.app.widget;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.util.ColorUtils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Custom View that represents the "Register/Login" buttons in the Action Bar.
 * @author Anastas Arnaudov
 *
 */
public class LoginRegisterActionButtonsView extends LinearLayout {

	private TextView registerButton;
	private TextView loginButton;

	private State state;
	
	enum State{
		REGISTRATION, LOGIN;
	}
	
	public LoginRegisterActionButtonsView(Context context) {
		super(context);
		setup(context, null);
	}
	
	public LoginRegisterActionButtonsView(Context context, AttributeSet set) {
		super(context, set);
		setup(context, set);
	}
	
	/**
	 * Setup the layout.
	 * @param context
	 * @param set
	 */
	private void setup(Context context, AttributeSet set) {
		View.inflate(context, R.layout.login_register_action_buttons_layout, this);
		
		registerButton = (TextView) findViewById(R.id.login_register_action_left);
		registerButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(state != State.REGISTRATION){	
					Application.get().postEvent(new NavigationEvent(Screen.REGISTER, NavigationType.HORIZONTAL));
					setupRegister();					
				}
			}
		});
		
		loginButton = (TextView) findViewById(R.id.login_register_action_right);
		loginButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(state != State.LOGIN){
					Application.get().postEvent(new NavigationEvent(Screen.LOGIN, NavigationType.HORIZONTAL));
					setupLogin();					
				}
			}
		});
		
	}
	
	/**
	 * Sets up the Register state.
	 */
	public void setupRegister(){
		
		this.state = State.REGISTRATION;
		
		registerButton.setBackgroundResource(R.drawable.round_left_selected_state_bg);
		registerButton.setTextColor(ColorUtils.getColor(R.color.white));
		
		loginButton.setBackgroundResource(R.drawable.round_right_normal_state_bg);
		loginButton.setTextColor(ColorUtils.getColor(R.color.blue_dark_2));
	}
	
	/**
	 * Sets up the Login state.
	 */
	public void setupLogin(){
		
		this.state = State.LOGIN;
		
		registerButton.setBackgroundResource(R.drawable.round_left_normal_state_bg);
		registerButton.setTextColor(ColorUtils.getColor(R.color.blue_dark_2));
		
		loginButton.setBackgroundResource(R.drawable.round_right_selected_state_bg);
		loginButton.setTextColor(ColorUtils.getColor(R.color.white));
	}
}
