package com.anyoption.android.app.model;

/**
 * @author Anastas Arnaudov
 */
public class RingChartInvestment {
    public long id;
    public float level;
    public long type;
    public float pointX;
    public float pointY;
}