package com.anyoption.android.app.receiver;

import java.util.Map;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.InstallEvent;
import com.anyoption.android.app.manager.SharedPreferencesManager;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * This custom {@link BroadcastReceiver} listens for Installation events.
 */
public class ReferrerReceiver extends BroadcastReceiver{
	
	public static final String TAG = ReferrerReceiver.class.getSimpleName();
	
	public static final String REFERRER 					= "referrer";
	public static final String KEY_COMBINATION_ID 			= "af_siteid";
	public static final String KEY_DYNAMIC_PARAM 			= "af_sub1";
	public static final String KEY_AFF_SUB1		 			= "aff_sub1";
	public static final String KEY_AFF_SUB2		 			= "aff_sub2";
	public static final String KEY_GCLID					= "gclid";
	
	private static final int MAX_AFF_SUB_PARAM_LENGTH = 20;
	private static final int MAX_GCLID_PARAM_LENGTH = 30;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "Received intent action  = " + intent.getAction());
		Log.i(TAG, "----------------------------------------");
		Log.i(TAG, "RECEIVED REFERRAL INFO");

		String uri = intent.toUri(0);
		
		Log.i(TAG, "referrer: [" + uri + "]");
		Log.i(TAG, intent.getAction());
		
		if (uri != null && uri.length() > 0) {
			int index = uri.indexOf(REFERRER+"=");
			
			if (index > -1) {
				// Gets the referrer decoded. 
				String referrer = intent.getStringExtra(REFERRER);

				Log.i(TAG, "Referrer: [" + referrer + "]");
				
				//referralURL contains both referrer parameter and its value
				//Example: referralURL="referrer=com.package.app"
				Map<String, String> map = TextUtils.parseUrlToStringPairs(referrer);
				SharedPreferencesManager sharedPreferencesManager = Application.get().getSharedPreferencesManager();

				Log.i(TAG, "Printing map...");
				for(Map.Entry entry : map.entrySet()){
					Log.i(TAG, "key = " + entry.getKey() + "  value = " + entry.getValue());
				}

				Long mCombId = null;
				String mDynamicParam = null;
				String mAffSub1 = null;
				String mAffSub2 = null;
				String mGclid = null;

				Log.i(TAG, "combId = " + map.get(KEY_COMBINATION_ID));
				if (map.get(KEY_COMBINATION_ID) != null) {
					try {
						Long combinationID = Long.valueOf(map.get(KEY_COMBINATION_ID));
						//sharedPreferencesManager.putLong(Constants.PREFERENCES_COMBINATION_ID, combinationID);
						mCombId = combinationID;
					}catch(NumberFormatException numberEx){
						Log.e(TAG, "Combination ID is NOT of type Long");						
					}
				}
				Log.i(TAG, "dp = " + map.get(KEY_DYNAMIC_PARAM));
				if (map.get(KEY_DYNAMIC_PARAM) != null) {
					mDynamicParam = map.get(KEY_DYNAMIC_PARAM);
				}
				
				if (map.get(KEY_AFF_SUB1) != null) {
					String affSub1 = map.get(KEY_AFF_SUB1);
					if (affSub1.length() > MAX_AFF_SUB_PARAM_LENGTH) {
						affSub1 = affSub1.substring(0, MAX_AFF_SUB_PARAM_LENGTH);
					}
					mAffSub1 = affSub1;
				}
				
				if (map.get(KEY_AFF_SUB2) != null) {
					String affSub2 = map.get(KEY_AFF_SUB2);
					if (affSub2.length() > MAX_AFF_SUB_PARAM_LENGTH) {
						affSub2 = affSub2.substring(0, MAX_AFF_SUB_PARAM_LENGTH);
					}
					mAffSub2 = affSub2;
				}
				
				if (map.get(KEY_GCLID) != null) {
					String gclid = map.get(KEY_GCLID);
					if (gclid.length() > MAX_GCLID_PARAM_LENGTH) {
						gclid = gclid.substring(0, MAX_GCLID_PARAM_LENGTH);
					}
					mGclid = gclid;
				}

				if(!Application.get().getSharedPreferencesManager().getBoolean(Constants.PREFERENCES_IS_CAME_FROM_MARKETING, false)) {
					Log.d(TAG, "Invoke setMarketingParams from ReferrerReceiver with combid: " + mCombId);
					Utils.setMarketingParams(mCombId, mDynamicParam, mAffSub1, mAffSub2, mGclid);
				}
				sharedPreferencesManager.putString(Constants.PREFERENCES_REFERRER_PARAM, referrer);

				Log.i(TAG, "Saved combId = " + Utils.getCombinationId());
				Log.i(TAG, "Saved dp = " + Utils.getDynamicParam());
				Log.i(TAG, "Saved aff_sub1 = " + Utils.getAffSub1(context));
				Log.i(TAG, "Saved aff_sub2 = " + Utils.getAffSub2(context));
				Log.i(TAG, "Saved gclid = " + Utils.getGclid(context));
			} else {
				Log.i(TAG, "No Referral URL.");
			}
		}
		
		Log.i(TAG, "----------------------------------------");
		
		Application.get().postEvent(new InstallEvent());
	}
}
