package com.anyoption.android.app.widget.form;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.anyoption.android.app.Application;
import com.anyoption.android.R;
import com.anyoption.android.app.popup.DatePickerDialogFragment;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.constants.Constants;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

/**
 * Custom Form Date Picker that on click opens a Date Picker Dialog.
 * @author Anastas Arnaudov
 *
 */
public class FormDatePicker extends FormEditText implements FormItem, com.anyoption.android.app.popup.DatePickerDialogFragment.OnDateSetListener{

	public static final String DATE_FORMAT_DAY_MONTH_YEAR = "dd/MM/yyyy";
	public static final String DATE_FORMAT_MONTH_YEAR = "MM/yyyy";
	public static final String DATE_FORMAT_DELIMITER = "/";
	private boolean isMin18;
	
	private DatePickerDialogFragment dialog;
	private Calendar calendar;

	private TextView textTextView;
	
	private String dateFormat;

	public FormDatePicker(Context context) {
		super(context);
	}
	public FormDatePicker(Context context, AttributeSet attrs) {
		super(context, attrs);
	}	
	public FormDatePicker(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	@Override
	protected void setup(Context context, AttributeSet attrs, int defStyle){
		View.inflate(context, R.layout.form_date_picker_layout, this);
		
		//Get custom attributes:
		TypedArray attributesTypedArray = context.obtainStyledAttributes(attrs,R.styleable.FormEditText);
		String hintText = attributesTypedArray.getString(R.styleable.FormEditText_hintText) != null ? attributesTypedArray.getString(R.styleable.FormEditText_hintText) : "";	
		////////////////////////
		
		dateFormat = DATE_FORMAT_DAY_MONTH_YEAR;
		setHintTextView((TextView) findViewById(R.id.form_date_picker_hint));
		setErrorTextView((TextView) findViewById(R.id.form_date_picker_error));
		textTextView = (TextView) findViewById(R.id.form_date_picker_text);
		
		getHintTextView().setText(hintText);
		    
		textTextView.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,int count) {}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
			@Override
			public void afterTextChanged(Editable s) {
				setupHint(false);
			}
		});
		
		attributesTypedArray.recycle();
		
		if(!isInEditMode()){
			setDirection(Application.get().getLayoutDirection());
			
			setupUIForDirection();
			setupDialog();
			
			this.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if(dialog != null){
						if(!dialog.isAdded()){
							dialog.show(Application.get().getCurrentActivity().getFragmentManager(), null);					
						}
					}
				}
			});				
		}
	}
	
	/**
	 * Depending on the validity we setup the layout.
	 */
	protected void setupValid(){
		getErrorTextView().setText(getErrorMessage());
	}
	
	private void setupDialog() {
		if(calendar == null){
			calendar =  Calendar.getInstance();
		}
		Bundle bundle = new Bundle();
		bundle.putSerializable(Constants.EXTRA_CALENDAR, calendar);
		dialog = DatePickerDialogFragment.newInstance(bundle);
		dialog.addOnDateSetListener(this);
		dialog.setMin18(this.isMin18);
	}

	/**
	 * Sets the Initial Date which is shown when the Date Picker is opened.
	 * @param calendar The time.
	 * @param shouldSetValue Whether to set the value to be this initial date or not.
	 */
	public void setInitialDate(Calendar calendar, boolean shouldSetValue){
		this.calendar = calendar;
		if(calendar != null){
			if(shouldSetValue){
				refreshUI();				
			}
			dialog.setDate(calendar);
		}
	}
	
	@Override
	public void onDateSet(Calendar calendar) {
		this.calendar = calendar;
		refreshUI();
	}

	@Override
	public boolean isEditable() {
		return isEnabled();
	}
	
	@Override
	public void setEditable(boolean isEditable) {
		setEnabled(isEditable);
	}

	@Override
	public String getValue() {
		return textTextView.getText().toString();
	}
	@Override
	public void setValue(Object value) {
		textTextView.setText(value.toString());
		getHintTextView().setTextColor(ColorUtils.getColor(R.color.form_item_hint_focused));
	}

	@Override
	public void clear() {
		textTextView.setText("");
		setupHint(false);
		setupValid();
	}
	@Override
	public void refreshUI() {
        SimpleDateFormat format = new SimpleDateFormat(dateFormat, Application.get().getLocale());
		setValue(format.format(calendar.getTime()));
	}
	@Override
	public void setupUIForDirection() {
		switch (getDirection()) {
			case LEFT_TO_RIGHT:
				textTextView.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
				getHintTextView().setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
				getErrorTextView().setGravity(Gravity.BOTTOM | Gravity.RIGHT);
				break;
			case RIGHT_TO_LEFT:
				textTextView.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
				getHintTextView().setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
				getErrorTextView().setGravity(Gravity.BOTTOM | Gravity.LEFT);
				break;
			default:
				break;
		}
	}
	@Override
	public void addOnEditorActionListener(OnEditorActionListener onEditorActionListener) {}
	
	/**
	 * set if need date picker with month and year only.
	 * @param isMonthAndYearOnly
	 */
	public void setMonthAndYearOnly(boolean isMonthAndYearOnly) {
		dialog.setMonthAndYearOnly(isMonthAndYearOnly);
		dateFormat = DATE_FORMAT_MONTH_YEAR;
	}
	public boolean isMin18() {
		return isMin18;
	}
	public void setMin18(boolean isMin18) {
		this.isMin18 = isMin18;
		dialog.setMin18(isMin18);
	}
	
}
