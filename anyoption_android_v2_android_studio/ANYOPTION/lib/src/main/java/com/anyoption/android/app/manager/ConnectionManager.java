package com.anyoption.android.app.manager;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.ConnectionStateChangeEvent;
import com.anyoption.android.app.receiver.ConnectionStateReceiver;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Connection Manager that manages the network connections.
 * @author Anastas Arnaudov
 *
 */
public class ConnectionManager {

	public static final String TAG = ConnectionManager.class.getSimpleName();
	
	private Application application;
	private ConnectivityManager connectivityManager;
	
	public ConnectionManager(Application application){
		Log.d(TAG, "On Create");
		this.application = application;
		
		this.connectivityManager = (ConnectivityManager) this.application.getSystemService(Context.CONNECTIVITY_SERVICE);

		application.registerForEvents(this, ConnectionStateChangeEvent.class);
	}
	
	/**
	 * Checks the Internet connection.
	 */
    public boolean checkConnection() {
        boolean isNetwork = isInternetConnectionAvailable();
        return isNetwork;
    }
	
	/**
	 * Checks the Internet connection.
	 * If there is no Internet we setup a Receiver to listen for connection change.
	 */
    public boolean checkConnectionAndSetupReceiver() {
        boolean isNetwork = isInternetConnectionAvailable();
        setupConnectionStateReceiver(isNetwork);
        return isNetwork;
    }
    
    /**
     * Checks the Internet connection.
     */
    private boolean isInternetConnectionAvailable(){
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        if (info != null){
        	return info.isConnected();        	
        }
        else{
        	return false;        	
        }
    }
	
    /**
     * Setup the {@link ConnectionStateReceiver}.
     * <li>If there is no connection - start the receiver.
     * <li>If there is a connection  - stop the receiver to save battery power.
     */
    private void setupConnectionStateReceiver(boolean isNetwork){
    	ComponentName receiverName = new ComponentName(application, ConnectionStateReceiver.class);
    	PackageManager packageManager = application.getPackageManager();
        if(isNetwork){
        	//We disable the receiver to save battery power:
        	packageManager.setComponentEnabledSetting(receiverName, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
        }else{
        	//We enable the receiver to notify us when connection is available:
        	packageManager.setComponentEnabledSetting(receiverName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
        }
    }
    
    /**
     * Called when a {@link ConnectionStateChangeEvent} occurs.
     */
    public void onEvent(ConnectionStateChangeEvent event){
    	
    	boolean isNetwork = isInternetConnectionAvailable();
		setupConnectionStateReceiver(isNetwork);
    	
    }
}
