package com.anyoption.android.app.event;

import com.lightstreamer.ls_client.UpdateInfo;

public class LightstreamerUpdateEvent {

	private long marketId;
	private UpdateInfo updateInfo;

	public LightstreamerUpdateEvent(long marketId, UpdateInfo updateInfo) {
		this.marketId = marketId;
		this.updateInfo = updateInfo;
	}

	public long getMarketId() {
		return marketId;
	}

	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	public UpdateInfo getUpdateInfo() {
		return updateInfo;
	}

	public void setUpdateInfo(UpdateInfo updateInfo) {
		this.updateInfo = updateInfo;
	}

}
