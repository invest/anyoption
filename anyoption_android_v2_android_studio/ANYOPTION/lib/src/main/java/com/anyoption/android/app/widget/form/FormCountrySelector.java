package com.anyoption.android.app.widget.form;

import java.util.ArrayList;
import java.util.List;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.LayoutDirection;
import com.anyoption.android.R;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.common.beans.base.Country;
import com.anyoption.common.enums.CountryStatus;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Custom {@link FormSelector} that holds list of {@link Country} and displays their names and country flags.
 * @author Anastas Arnaudov
 */
public class FormCountrySelector extends FormSelector<Country> {

	public static final String TAG = FormCountrySelector.class.getSimpleName();
	
	private List<Country> countryList;
	private View view;
	private LinearLayout rootLayout;
	private TextView text;
	private ImageView flagImage;
	private ImageView dropDownImage;
	private long selectedCountryId;

	public FormCountrySelector(Context context) {
		super(context);
	}

	public FormCountrySelector(Context context, AttributeSet attrs) { 
		super(context, attrs);
	}

	public FormCountrySelector(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void setupUI() {
		if(view == null){
			view = View.inflate(getContext(), R.layout.country_selector, this);
			rootLayout = (LinearLayout)view.findViewById(R.id.country_selector_root_layout);
			text = (TextView) findViewById(R.id.country_selector_text);
			flagImage = (ImageView) findViewById(R.id.country_selector_flag_image);
			dropDownImage = (ImageView) findViewById(R.id.country_selector_dropdown_image);
		}
		if(!isInEditMode()){
			setupUIForDirection();
			setupCountries();
			
			String defaultCountry = countryList.get(0).getName();
			text.setText(defaultCountry);
			setItems(countryList, 0, new ItemListener<Country>() {
				@Override
				public void onItemSelected(Country selectedItem) {
					text.setText(selectedItem.getName());
					selectedItem.getId();
				}
				@Override
				public String getDisplayText(Country selectedItem) {
					return selectedItem.getName();
				}
				@Override
				public Drawable getIcon(Country selectedItem) {
					//Set the country flag:
					String countryCode = selectedItem.getA2().toLowerCase(Application.get().getLocale());
					Drawable drawable = DrawableUtils.getImage(countryCode);
					return drawable;
				}
			});			
		}else{
			text.setText("Country Selector");
		}
		
	}

	private void setupCountries() {
		countryList = new ArrayList<Country>();
		Country[] countriesArray = Application.get().getCountriesArray();
		for(Country country : countriesArray){
			//Do NOT display countries with Blocked or Rejected State:
			if(country.getCountryStatus() == CountryStatus.NOTBLOCKED){
				countryList.add(country);
			}
		}
	}
	
	@Override
	public void setValue(Object value){
		if(value instanceof Country){
			super.setValue(value);
			Country country = (Country) value;
			setCountryId(country.getId());
		}else{
			Log.e(TAG, "The value must be of type Country");
		}
	}
	
	/**
	 * Set selected country id
	 * @param countryId
	 */
	public void setCountryId(long countryId) {
		int count = 0;
		for (Country country : countryList) {
			if (country.getId() == countryId) {
				super.setValue(country);
				text.setText(country.getName());
				//Set the country flag:
				String countryCode = country.getA2().toLowerCase(Application.get().getLocale());
				Drawable drawable = DrawableUtils.getImage(countryCode);
				flagImage.setImageDrawable(drawable);
				
				selectedCountryId = country.getId();  
				setItems(countryList, count, new ItemListener<Country>() {
					@Override
					public void onItemSelected(Country selectedItem) {
						text.setText(selectedItem.getName());
						//Set the country flag:
						String countryCode = selectedItem.getA2().toLowerCase(Application.get().getLocale());
						Drawable drawable = DrawableUtils.getImage(countryCode);
						flagImage.setImageDrawable(drawable);
						selectedCountryId = selectedItem.getId();  
					}
					@Override
					public String getDisplayText(Country selectedItem) {
						return selectedItem.getName();
					}
					@Override
					public Drawable getIcon(Country selectedItem) {
						//Set the country flag:
						String countryCode = selectedItem.getA2().toLowerCase(Application.get().getLocale());
						Drawable drawable = DrawableUtils.getImage(countryCode);
						return drawable;
					}
				});
			}
			++count;
		}	
	}
	
	/**
	 * @return the selectedCountryId
	 */
	public long getSelectedCountryId() {
		return selectedCountryId;
	}

	/**
	 * @param selectedCountryId the selectedCountryId to set
	 */
	public void setSelectedCountryId(long selectedCountryId) {
		this.selectedCountryId = selectedCountryId;
	}

	@Override
	public void setupUIForDirection() {
		rootLayout.removeAllViews();
		if(getDirection() == LayoutDirection.LEFT_TO_RIGHT){
			rootLayout.addView(text);
			rootLayout.addView(flagImage);
			rootLayout.addView(dropDownImage);
			text.setGravity(Gravity.LEFT);
		} else if (getDirection() == LayoutDirection.RIGHT_TO_LEFT) {
			rootLayout.addView(dropDownImage);
			rootLayout.addView(flagImage);
			rootLayout.addView(text);
			text.setGravity(Gravity.RIGHT);
		}
	}

	@Override
	public void setFieldName(String fieldName) {}

	@Override
	public String getFieldName() {
		return null;
	}

	@Override
	public void displayError(String error) {}

}
