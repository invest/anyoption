package com.anyoption.android.app.popup;

import java.lang.reflect.Method;

import com.anyoption.android.app.Application;
import com.anyoption.android.R;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public abstract class BaseDialogFragment extends DialogFragment {

	private static final String TAG = BaseDialogFragment.class.getSimpleName();

	/**
	 * Use this argument key to change the behavior of background click. Otherwise this value is set
	 * depending on application's default settings.
	 * 
	 * @see {@link R.bool#closeDialogOnBgClick}
	 */
	public static final String ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY = "arg_close_dialog_on_bg_click_key";

	/**
	 * Use this argument key to show (true value) / hide (false value) close button. Otherwise its
	 * visibility depends on application's default settings.
	 * 
	 * @see {@link R.bool#isCloseButtonShown}
	 * @see #showCloseButton(boolean)
	 */
	public static final String ARG_SHOW_CLOSE_BUTTON_KEY = "arg_show_close_button_key";

	/**
	 * The resource should refer to a Drawable object or 0 to remove the default background.
	 * 
	 * @see #setBackgroundResource(int)
	 * @see #setBackgroundColor(int)
	 */
	public static final String ARG_BACKGROUND_RESOURCE_ID_KEY = "arg_background_res_id_key";

	/**
	 * The color to set as background.
	 * 
	 * @see #setBackgroundColor(int)
	 * @see #setBackgroundResource(int)
	 */
	public static final String ARG_BACKGROUND_COLOR_KEY = "arg_background_color_key";

	public interface OnDialogDismissListener {

		/**
		 * Called when the dialog fragment is dismissed.
		 */
		public void onDismiss();

	}

	protected Application application;
	protected boolean isTablet;
	private RelativeLayout root;
	protected View content;
	protected View closeButton;

	protected OnDialogDismissListener onDismissListener;

	private LayoutInflater layoutInflater;
	
	/**
	 * This method uses reflection to create new instance of the given BaseDialogFragment sub-class.
	 * 
	 * @param dialogClass
	 *            The class of the BaseDialogFragment subclass.
	 * @param bundle
	 *            The bundle with arguments.
	 * @return
	 */
	public static BaseDialogFragment newInstance(
			Class<? extends BaseDialogFragment> dialogClass, Bundle bundle) {
		BaseDialogFragment fragment = null;
		try {
			Method method = dialogClass.getDeclaredMethod("newInstance", Bundle.class);
			fragment = (BaseDialogFragment) method.invoke(null, bundle);
		} catch (Exception e) {
			Log.e(TAG, "Reflection error.", e);
		}
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NO_TITLE, R.style.FullscreenDialog);
		
		application = Application.get();
		isTablet = getResources().getBoolean(R.bool.isTablet);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		layoutInflater = inflater;
		root = (RelativeLayout) inflater.inflate(R.layout.base_dialog_layout, container, false);

		content = inflater.inflate(getContentLayout(), root, false);
		root.addView(content);

		Bundle arguments = getArguments();
		if (arguments != null && arguments.containsKey(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY)) {
			setCloseOnRootClick(arguments.getBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY));
		} else {
			setCloseOnRootClick(getResources().getBoolean(R.bool.closeDialogOnBgClick));
		}

		if (arguments != null && arguments.containsKey(ARG_SHOW_CLOSE_BUTTON_KEY)) {
			if (arguments.getBoolean(ARG_SHOW_CLOSE_BUTTON_KEY)) {
				setUpCloseButton();
			}
		} else if (getResources().getBoolean(R.bool.isCloseButtonShown)) {
			setUpCloseButton();
		}

		if (arguments != null) {
			if (arguments.containsKey(ARG_BACKGROUND_RESOURCE_ID_KEY)) {
				setBackgroundResource(arguments.getInt(ARG_BACKGROUND_RESOURCE_ID_KEY));
			} else if (arguments.containsKey(ARG_BACKGROUND_COLOR_KEY)) {
				setBackgroundColor(arguments.getInt(ARG_BACKGROUND_COLOR_KEY));
			}
		}

		onCreateContentView(content);

		return root;
	}

	/**
	 * Override this method to make some changes on content view.
	 * 
	 * @param contentView
	 */
	protected void onCreateContentView(View contentView) {
		// Do nothing.
	}

	/**
	 * Returns layout's resource id
	 * 
	 * @return
	 */
	protected abstract int getContentLayout();

	@Override
	public void onDismiss(DialogInterface dialog) {
		if (onDismissListener != null) {
			onDismissListener.onDismiss();
		}

		super.onDismiss(dialog);
	}

	public OnDialogDismissListener getOnDismissListener() {
		return onDismissListener;
	}

	public void setOnDismissListener(OnDialogDismissListener onDismissListener) {
		this.onDismissListener = onDismissListener;
	}

	@SuppressLint("NewApi")
	private void setUpCloseButton() {
		if (isCloseButtonShown()) {
			return;
		}

		if (closeButton == null) {
			// Set id of the content view
			if (content.getId() < 0) {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
					content.setId(View.generateViewId());
				} else {
					content.setId(1000);
				}
			}

			closeButton = layoutInflater.inflate(R.layout.base_dialog_close_button, root, false);
			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) closeButton
					.getLayoutParams();
			params.addRule(RelativeLayout.ALIGN_LEFT, content.getId());
			params.addRule(RelativeLayout.ALIGN_TOP, content.getId());
			closeButton.setLayoutParams(params);
			root.addView(closeButton);

			closeButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					dismiss();
				}
			});
		} else {
			closeButton.setVisibility(View.VISIBLE);
		}
	}

	private void setCloseOnRootClick(boolean close) {
		if (close) {
			root.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					dismiss();
				}
			});
		} else {
			// Disables background views clicking.
			root.setClickable(true);
		}
	}

	public boolean isCloseButtonShown() {
		if (closeButton == null || closeButton.getVisibility() == View.GONE) {
			return false;
		}

		return true;
	}

	/**
	 * Use this method to manually show/hide close button
	 * 
	 * @param show
	 */
	public void showCloseButton(boolean show) {
		if (show) {
			setUpCloseButton();
		} else {
			if (closeButton != null) {
				closeButton.setVisibility(View.GONE);
			}
		}
	}

	/**
	 * Changes the background color of the root view.
	 * 
	 * @param color
	 */
	public void setBackgroundColor(int color) {
		root.setBackgroundColor(color);
	}

	/**
	 * Set the background to a given resource. The resource should refer to a Drawable object or 0
	 * to remove the background.
	 * 
	 * @param resId
	 *            The identifier of the resource.
	 */
	public void setBackgroundResource(int resId) {
		root.setBackgroundResource(resId);
	}

	/**
	 * Override point.
	 * @return
	 */
	public int getPopupCode(){
		return 0;
	}
	
	@Override
	public void onDestroy() {
		Log.d(this.getClass().getSimpleName(), "On Destroy");
		application.getPopUpManager().onPopupDismissed(getPopupCode());
		super.onDestroy();
	}
	
}
