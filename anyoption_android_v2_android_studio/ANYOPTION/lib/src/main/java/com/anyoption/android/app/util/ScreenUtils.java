package com.anyoption.android.app.util;

import com.anyoption.android.app.Application;
import com.anyoption.android.R;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Point;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 *Utility class with useful methods related with the UI. 
 *
 */
public final class ScreenUtils {

	public static final String TAG = ScreenUtils.class.getSimpleName();
	
	/**
	 * Converts density independent pixels to device specific pixels.
	 * @param context
	 * @param dip
	 * @return
	 */
	public static float convertDpToPx(Context context, float dip) {
		Resources r = context.getResources();
		return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, r.getDisplayMetrics());
	}

	/**
	 * Converts device specific pixels to density independent pixels.
	 * @param px A value in px (pixels) unit. Which we need to convert into dp
	 * @param context Context to get resources and device specific display metrics
	 * @return A float value to represent dp equivalent to px value
	 */
	public static float convertPxToDp(float px, Context context){
	    Resources resources = context.getResources();
	    DisplayMetrics metrics = resources.getDisplayMetrics();
	    float dp = px / (metrics.densityDpi / 160f);
	    return dp;
	}
	
	/**
	 * Converts Sp units to Pixels.
	 * @param context
	 * @param sp
	 * @return
	 */
	public static float convertSpToPx(Context context, float sp) {
		Resources r = context.getResources();
		return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, r.getDisplayMetrics());
	}

	public static Layout makeLayout(CharSequence text, int textSize) {
	     TextPaint mTextPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
	     mTextPaint.setTextSize(textSize);
        return new StaticLayout(text, mTextPaint,(int) Math.ceil(Layout.getDesiredWidth(text, mTextPaint)), Layout.Alignment.ALIGN_NORMAL, 1.f, 0, true);
    }
	
	/**
	 * Hides the keyboard if it is visible.
	 */
	public static void hideKeyboard(EditText editText) {
		try {
			InputMethodManager input = (InputMethodManager) Application.get().getSystemService(
					Activity.INPUT_METHOD_SERVICE);
			if (input != null && editText != null) {
				input.hideSoftInputFromWindow(editText.getWindowToken(), 0);
				if (Application.get().getCurrentEditTextOnFocus() != null) {
					Application.get().setCurrentEditTextOnFocus(null);
				}
			} else {
				Log.d(TAG, "Could NOT hide the Kyboard!");
			}
		} catch (Exception e) {
			Log.e(TAG, "Could NOT hide the Kyboard!");
		}
	}

	/**
	 * Hides the keyboard if it is visible.
	 */
	public static void hideKeyboard() {
		ScreenUtils.hideKeyboard(Application.get().getCurrentEditTextOnFocus());
	}
	
	
	/**
	 * Opens the keyboard.
	 */
	public static void openKeyboard(){
		try {
            InputMethodManager inputManager = (InputMethodManager) Application.get().getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }catch(Exception e) {
        	Log.e(TAG, "Could NOT hide the Kyboard!");
        }
	}
	
	public static boolean isTablet(){
		return Application.get().getResources().getBoolean(R.bool.isTablet);
	}
	
	/**
	 * Return the device Screen dimensions : <br>
	 * int[0] : Screen width<br>
	 * int[1] : Screen height<br>
	 */
	public static int[] getScreenWidth(){
		WindowManager wm = (WindowManager) Application.get().getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int[] dimens = new int[2];
		dimens[0] = size.x;
		dimens[1] = size.y;
		return dimens;
	}
	
	
	
}
