package com.anyoption.android.app.fragment;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.manager.FragmentManager;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.UserMethodResult;

import android.os.Bundle;
import android.util.Log;

/**
 * Custom {@link BaseFragment} that has a Screenable alias mapped to it. This
 * type of Fragments are used in the navigation system of the application : when
 * navigating to a particular screen or changing part of the screen - the
 * navigation manager ({@link FragmentManager}) of the application makes sure
 * that the navigation change is added to the history ({Back Stack}) and handles
 * the back button logic reasonably.
 * 
 * @author Anastas Arnaudov
 *
 */
public abstract class NavigationalFragment extends BaseFragment {

	public static final String TAG = NavigationalFragment.class.getSimpleName();

	private Screenable screen;

	public NavigationalFragment() {
		super();
		this.screen = setupScreen();
	}

	protected void refreshUser() {
		if (application.getUser() != null) {
			Log.d(TAG, "refreshUser");
			application.getCommunicationManager().requestService(this, "getUserCallback",
					application.getServiceMethodGetUser(), new UserMethodRequest(),
					application.getUserMethodResultClass(), false, false);
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		refreshUser();
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		if (!hidden) {
			refreshUser();
		}
		super.onHiddenChanged(hidden);
	}

	/**
	 * CallBack for the getUser request.
	 * 
	 * @param resultObj
	 */
	public void getUserCallback(Object resultObj) {
		Log.d(TAG, "getUserCallback");
		UserMethodResult result = (UserMethodResult) resultObj;

		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Saving refreshed user.");
			application.saveUser(result);
		}else{
			Log.e(TAG, "Error refreshing user.");
			ErrorUtils.handleResultError(result);
		}
		
		if (application.isLoggedIn() && application.getUser().getIsNeedChangePassword()) {
			application.postEvent(new NavigationEvent(Screen.UPDATE_PASSWORD, NavigationType.DEEP, Screenable.SCREEN_FLAG_REMOVE_MENU, Screenable.SCREEN_FLAG_REMOVE_BACK));
		}
	}

	/**
	 * Gets the {@link Screenable} which is mapped with this Fragment.
	 * 
	 * @return
	 */
	public Screenable getScreen() {
		return screen;
	}

	/**
	 * Override point. Maps the Fragment with a {@link Screenable}.
	 * <p>
	 * Here we must set the {@link Screenable} for this fragment.
	 */
	protected abstract Screenable setupScreen();

}
