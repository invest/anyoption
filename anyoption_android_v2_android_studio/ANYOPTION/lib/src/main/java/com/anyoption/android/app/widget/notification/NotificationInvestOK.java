package com.anyoption.android.app.widget.notification;

import com.anyoption.android.app.Application;
import com.anyoption.android.R;
import com.anyoption.android.app.popup.ReceiptDialogFragment;
import com.anyoption.android.app.popup.ReceiptDynamicsDialogFragment;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.InvestmentUtil;
import com.anyoption.common.beans.base.Investment;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NotificationInvestOK extends LinearLayout{

	protected Investment investment;
	protected View view;
	
	public NotificationInvestOK(Investment investment) {
		super(Application.get());
		this.investment = investment;
		init(Application.get(), null, 0);
	}
	
	public NotificationInvestOK(Context context) {
		super(context);
		init(context, null, 0);
	}
	public NotificationInvestOK(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	public NotificationInvestOK(Context context, AttributeSet attrs,int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		view = View.inflate(context, R.layout.notification_invest_ok_layout, this);
		setupText();
		setupButton();
	}
	
	protected void setupText(){
		ImageView imageView = (ImageView) view.findViewById(R.id.notification_invest_ok_image);
		TextView messageTextView = (TextView) view.findViewById(R.id.notification_message_text_view);
		long type = investment.getTypeId();
		
		if(type == Investment.INVESTMENT_TYPE_CALL || type == Investment.INVESTMENT_TYPE_DYNAMICS_BUY){
			imageView.setImageResource(R.drawable.triangle_small_up_white);
		}else if(type == Investment.INVESTMENT_TYPE_PUT || type == Investment.INVESTMENT_TYPE_DYNAMICS_SELL){
			imageView.setImageResource(R.drawable.triangle_small_down_white);
		}

		StringBuilder message = new StringBuilder();
		
		message.
		append(investment.getLevel()).
		append("   ").
		append(Application.get().getString(R.string.notificationInvestOKMessage)); 
		
		messageTextView.setText(message.toString());
	}

	protected void setupButton(){
		Button receiptButton = (Button) view.findViewById(R.id.notification_purchase_receipt_button);
		long type = investment.getTypeId();
		//Check Dynamics:
		if(type == Investment.INVESTMENT_TYPE_DYNAMICS_BUY || type == Investment.INVESTMENT_TYPE_DYNAMICS_SELL){
			receiptButton.setGravity(Gravity.CENTER);
		}else{
			receiptButton.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
		}
		receiptButton.setOnClickListener(new OnClickListener() {
			private boolean isPressed = false;
			@Override
			public void onClick(View v) {
				if(!isPressed){
					isPressed = true;

					Bundle args = new Bundle();
					args.putSerializable(ReceiptDialogFragment.ARG_INVESTMENT_KEY, investment);
					boolean isDynamics = investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY || investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_SELL;
					if(isDynamics){
						Application.get().getFragmentManager().showDialogFragment(ReceiptDynamicsDialogFragment.class, args);
					}else{
						Application.get().getFragmentManager().showDialogFragment(ReceiptDialogFragment.class, args);
					}
				}
			}
		});
	}
}
