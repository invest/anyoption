package com.anyoption.android.app.model;

import java.util.ArrayList;
import java.util.List;

import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.MarketGroup;

public class MarketListGroup {
	private MarketGroup marketGroup;
	private List<MarketListItem> marketListItems = new ArrayList<MarketListItem>();
	private boolean isShowOnlyOpenMarkets = true;
	private boolean hasInactiveMarkets = false;
	
	public MarketListGroup(){
		
	}
	
	public MarketListGroup(MarketGroup marketGroup){
		this.setMarketGroup(marketGroup);
		for(Market market : marketGroup.getMarkets()){
			if(market.getGroupId() == null){
				market.setGroupId(marketGroup.getId());//If not added on the server side				
			}
			market.setGroupName(marketGroup.getName());
			getMarketListItems().add(new MarketListItem(market));
		}
	}

	public MarketGroup getMarketGroup() {
		return marketGroup;
	}
	public void setMarketGroup(MarketGroup marketGroup) {
		this.marketGroup = marketGroup;
	}
	public List<MarketListItem> getMarketListItems() {
		return marketListItems;
	}
	public void setMarketListItems(List<MarketListItem> marketListItems) {
		this.marketListItems = marketListItems;
	}
	
	/**
	 * Returns the market which has the given id or null if the market is not found.
	 * 
	 * @param marketID
	 * @return
	 */
	public MarketListItem getMarketListItemForID(long marketID){
		MarketListItem result = null;
		for(MarketListItem marketListItem : getMarketListItems()){
			if(marketListItem.getMarket().getId() == marketID){
				result = marketListItem;
				break;
			}
		}
		return result;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Group ID = " + getMarketGroup().getId() + "\n")
		.append("Group Name = " + getMarketGroup().getName());
		return builder.toString();
	}

	public boolean isShowOnlyOpenMarkets() {
		return isShowOnlyOpenMarkets;
	}

	public void setShowOnlyOpenMarkets(boolean isShowOnlyOpenMarkets) {
		this.isShowOnlyOpenMarkets = isShowOnlyOpenMarkets;
	}

	public boolean hasInactiveMarkets() {
		return hasInactiveMarkets;
	}

	public void setHasInactiveMarkets(boolean hasInactiveMarkets) {
		this.hasInactiveMarkets = hasInactiveMarkets;
	}
}
