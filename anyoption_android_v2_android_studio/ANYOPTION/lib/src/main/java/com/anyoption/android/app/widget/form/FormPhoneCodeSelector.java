package com.anyoption.android.app.widget.form;

import java.util.ArrayList;
import java.util.List;

import com.anyoption.android.app.Application;
import com.anyoption.android.R;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.DrawableUtils;
/**
 * Custom form selector that opens a List of Country phone codes to choose form.
 */
import com.anyoption.common.beans.base.Country;
import com.anyoption.common.enums.CountryStatus;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

/**
 * Custom {@link FormSelector} that holds list of {@link Country} and displays their phone codes and
 * country flags.
 * 
 */
public class FormPhoneCodeSelector extends FormSelector<Country> {

	public static final String TAG = FormPhoneCodeSelector.class.getSimpleName();

	private List<Country> countryList;
	private View rootView;
	private TextView text;

	public FormPhoneCodeSelector(Context context) {
		this(context, null);
	}

	public FormPhoneCodeSelector(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public FormPhoneCodeSelector(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void setupUI() {
		if (rootView == null) {
			rootView = View.inflate(getContext(), R.layout.phone_code_selector, this);
			text = (TextView) findViewById(R.id.phone_code_selector_text);
			if(isEnabled){
				text.setTextColor(ColorUtils.getColorStateList(R.color.form_item_selector_text));
			}else{
				text.setTextColor(ColorUtils.getColorStateList(R.color.form_item_hint));
			}
		}

		if (!isInEditMode()) {
			setupUIForDirection();
			setupCountries();
			String defaultPhoneCode = countryList.get(0).getPhoneCode();
			text.setText("+" + defaultPhoneCode);

			setItems(countryList, 0, new ItemListener<Country>() {

				@Override
				public void onItemSelected(Country selectedItem) {
					text.setText("+" + selectedItem.getPhoneCode());
				}

				@Override
				public String getDisplayText(Country selectedItem) {
					return selectedItem.getName() + " " + selectedItem.getPhoneCode();
				}

				@Override
				public Drawable getIcon(Country selectedItem) {
					// Set the country flag:
					String countryCode = selectedItem.getA2().toLowerCase(
							Application.get().getLocale());
					Drawable drawable = DrawableUtils.getImage(countryCode);
					return drawable;
				}
			});

		} else {
			text.setText("Prefix");
		}

	}

	@Override
	public void setEditable(boolean isEditable) {
		super.setEditable(isEditable);
		if(text != null){
			if(isEnabled){
				text.setTextColor(ColorUtils.getColorStateList(R.color.form_item_selector_text));
			}else{
				text.setTextColor(ColorUtils.getColorStateList(R.color.form_item_hint));
			}			
		}
	}
	
	private void setupCountries() {
		countryList = new ArrayList<Country>();
		Country[] countriesArray = Application.get().getCountriesArray();
		for (Country country : countriesArray) {
			// Do NOT display countries with Blocked or Rejected State:
			if (country.getCountryStatus() == CountryStatus.NOTBLOCKED) {
				countryList.add(country);
			}
		}
	}

	@Override
	public void setupUIForDirection() {
		// Not needed for this form.
	}

	@Override
	public void setValue(Object value) {
		if (value instanceof Country) {
			super.setValue(value);
			Country country = (Country) value;
			setPhoneCode(country.getId());
		} else {
			Log.e(TAG, "The value must be of type Country");
		}
	}

	public void setPhoneCode(long countryId) {
		int count = 0;
		for (Country country : countryList) {
			if (country.getId() == countryId) {
				super.setValue(country);
				text.setText("+" + country.getPhoneCode());
				setItems(countryList, count, new ItemListener<Country>() {

					@Override
					public void onItemSelected(Country selectedItem) {
						text.setText("+" + selectedItem.getPhoneCode());
					}

					@Override
					public String getDisplayText(Country selectedItem) {
						return selectedItem.getName() + " " + selectedItem.getPhoneCode();
					}

					@Override
					public Drawable getIcon(Country selectedItem) {
						// Set the country flag:
						String countryCode = selectedItem.getA2().toLowerCase(
								Application.get().getLocale());
						Drawable drawable = DrawableUtils.getImage(countryCode);
						return drawable;
					}
				});

			}
			++count;
		}
	}

	public String getPhoneCode() {
		return getValue().getPhoneCode();
	}

	public long getCountryId() {
		return getValue().getId();
	}

	@Override
	public void setFieldName(String fieldName) {

	}

	@Override
	public String getFieldName() {
		return null;
	}

	@Override
	public void displayError(String error) {
		// This form does not show error.
	}

}