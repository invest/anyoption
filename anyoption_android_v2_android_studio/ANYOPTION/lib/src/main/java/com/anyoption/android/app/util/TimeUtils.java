package com.anyoption.android.app.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.anyoption.android.app.Application;
import com.anyoption.android.R;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.results.ChartDataResult;
import com.anyoption.json.results.ChartDataMethodResult;

import android.content.Context;

public class TimeUtils {

	public static final SimpleDateFormat hoursMinutesSecondsFormat 		= new SimpleDateFormat("HH:mm:ss");
	public static final SimpleDateFormat hoursMinutesFormat 			= new SimpleDateFormat("HH:mm");
	public static final SimpleDateFormat minutesAndSecondsFormat 		= new SimpleDateFormat("mm:ss");
	public static final SimpleDateFormat secondsAndMillisecondFormat 	= new SimpleDateFormat("ss:SSS");

	/**
	 * Returns the GMT offset of the device in milliseconds.
	 * Example: GMT+10 -> -10
     */
	public static long getGMTOffset(){
		Calendar calendar = Calendar.getInstance();
		return -(calendar.get(Calendar.ZONE_OFFSET) + calendar.get(Calendar.DST_OFFSET));
	}

	public static long getCurrentServerTime(){
		Date date = new Date();
		long serverTimeOffset = Application.get().getSharedPreferencesManager().getLong(Constants.PREFERENCES_TIME_OFFSET, 0L);
		date.setTime(date.getTime() + serverTimeOffset);
		return date.getTime();
	}
	
	public static long getServerOffset(){
		return Application.get().getSharedPreferencesManager().getLong(Constants.PREFERENCES_TIME_OFFSET, 0L);
	}
	
	/**
	 * Returns the date with fixed offset depending on timeOffset.
	 * 
	 * @param date
	 * @param timeOffset
	 */
	public static void fixTime(Date date, long timeOffset) {
		date.setTime(date.getTime() + timeOffset);
	}
	
	/**
	 * Returns the date with fixed UTC offset.
	 * 
	 * @param date
	 */
	public static void fixTime(Date date) {
		fixTime(date, getUtcOffset());
	}

	/**
	 * Returns the time with fixed server offset.
	 * 
	 * @param time in milliseconds
	 * @return
	 */
	public static long getFixedTime(long time) {
		long serverTimeOffset = Application.get().getSharedPreferencesManager().getLong(Constants.PREFERENCES_TIME_OFFSET, 0L);
		return time + serverTimeOffset;
	}

	public static int getUtcOffset() {
		int utcOffset = TimeZone.getDefault().getRawOffset();
		if (TimeZone.getDefault().inDaylightTime(new Date())) {
			// TODO: see if there is a better way to include the summer time
			utcOffset += 3600000;
		}
		return utcOffset;
	}
	
	public static Date parseTimeFromLSUpdate(String timeStr) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm", Locale.US);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

		return sdf.parse(timeStr);
	}

	public static String reformatTimeFromLSUpdate(Context context, String timeStr) throws ParseException {
		Date time = parseTimeFromLSUpdate(timeStr);
		Calendar now = Calendar.getInstance();
		Calendar timeCal = Calendar.getInstance();
		timeCal.setTime(time);
		if (now.get(Calendar.YEAR) == timeCal.get(Calendar.YEAR) && now.get(Calendar.DAY_OF_YEAR) == timeCal.get(Calendar.DAY_OF_YEAR)) {
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			return sdf.format(time) + " " + context.getResources().getString(R.string.timeToday);
		} else {
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm | dd.MM.yy");
			return sdf.format(time);
		}
	}

	public static String formatTimeToHoursMinutesSeconds(long millsecond) {
		return hoursMinutesSecondsFormat.format(millsecond);
	}

	public static String formatTimeToHoursMinutes(long millsecond) {
		return hoursMinutesFormat.format(millsecond);
	}
	
	public static String formatTimeToMinutesAndSeconds(long millsecond) {
		return minutesAndSecondsFormat.format(millsecond);
	}

	public static String formatTimeToSecondsAndMilliseconds(long millsecond) {
		String out = secondsAndMillisecondFormat.format(millsecond);
		return out.substring(0, out.length() - 1);
	}

	/**
	 * Changes result's dates to be with correct offset.<br/>
	 * <b>NOTE:</b> This method must be called only once for an object. Otherwise the date will be
	 * incorrect again.
	 * 
	 * @param investments
	 */
	public static void fixDates(ChartDataMethodResult data) {
		if (data.getTimeFirstInvest() != null) {
			TimeUtils.fixTime(data.getTimeFirstInvest());
		}
		if (data.getTimeLastInvest() != null) {
			TimeUtils.fixTime(data.getTimeLastInvest());
		}
		if (data.getTimeEstClosing() != null) {
			TimeUtils.fixTime(data.getTimeEstClosing());
		}

		if (data.getInvestments() != null) {
			InvestmentUtil.fixDate(data.getInvestments());
		}
	}

	public static void fixDates(ChartDataResult data) {
		if (data.getTimeFirstInvest() != null) {
			TimeUtils.fixTime(data.getTimeFirstInvest());
		}
		if (data.getTimeLastInvest() != null) {
			TimeUtils.fixTime(data.getTimeLastInvest());
		}
		if (data.getTimeEstClosing() != null) {
			TimeUtils.fixTime(data.getTimeEstClosing());
		}

		if (data.getInvestments() != null) {
			InvestmentUtil.fixDate(data.getInvestments());
		}
	}

	public static long get18YearsAgoTimestamp(){
		Calendar past18Date = Calendar.getInstance();
		past18Date.add(Calendar.YEAR, -18);
		long date = past18Date.getTimeInMillis();
		return date;
	}
	
	public static Calendar get18YearsAgoCalendar(){
		Calendar past18Date = Calendar.getInstance();
		past18Date.add(Calendar.YEAR, -18);
		return past18Date;
	}

	/**
	 * Checks if the given GMT Date is from the previous, the current or the next day compared to the Device time.
	 * @return -1 : if the Date is from the previous day. <br/>
	 * 			0 : if the Date is from the current day. <br/>
	 * 		   +1 : if the Date is from the next day.
     */
	public static int getDayDeviationFromGMT(Date dateGMT){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dateGMT);

		int minutesOfTheDay  = calendar.get(Calendar.HOUR_OF_DAY)*60 + calendar.get(Calendar.MINUTE);
		int minutesGMTOffset = (int) (getGMTOffset() / 60000);

		float offsetHours = (float)(minutesOfTheDay - minutesGMTOffset) / 60;
		if(offsetHours < 0){
			return -1;
		}else if(offsetHours > 24){
			return 1;
		}else{
			return 0;
		}
	}

}
