package com.anyoption.android.app.fragment.my_account.banking.deposit;

import android.os.Bundle;
import android.util.Log;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.DepositEvent;
import com.anyoption.android.app.event.DepositFailEvent;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.popup.BaseDialogFragment.OnDialogDismissListener;
import com.anyoption.android.app.popup.OkDialogFragment;
import com.anyoption.android.app.popup.ThreeDSecureDialogFragment;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.ThreeDParams;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.results.TransactionMethodResult;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author liors
 */
public abstract class ReceiptFragment extends NavigationalFragment implements OnDialogDismissListener {

    public static final String TRANSACTION_DATE_FORMAT_PATTERN = "HH:mm | dd/MM/yyyy";

    public static final String TAG = ReceiptFragment.class.getSimpleName();

    protected long transId;

    @Deprecated
    protected boolean isSubmitButtonEnabled = true;

    @Override
    protected Screenable setupScreen() {
        return null;
    }

    /**
     * Returns amount + currency symbol.
     *
     * @return
     */
    public abstract String getAmount();

    public abstract String getCurrencySymbol();

    public abstract void setSubmitButtonEnabled(boolean isEnabled);

    public void insertDepositCallBack(Object resultObj) {
        Log.d(TAG, "insertDepositCallBack");

        TransactionMethodResult result = (TransactionMethodResult) resultObj;
        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            Log.d(TAG, "Insert deposit succefully");
            handleSuccessfulDeposit(result);
        } else {
            Log.d(TAG, "Insert deposit failed");
            if (result.getApiCode() != null && result.getApiCode().equals("error.min.deposit")) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.EXTRA_MESSAGE, result.getUserMessages()[0].getMessage());
                BaseDialogFragment minDepositDialog = application.getFragmentManager().showDialogFragment(OkDialogFragment.class, bundle);
                minDepositDialog.setOnDismissListener(this);
            } else {
                ErrorUtils.handleResultError(result);
            }
            isSubmitButtonEnabled = true;
            setSubmitButtonEnabled(true);
            application.postEvent(new DepositFailEvent(result));
        }
    }

    protected void handle3DSecureOnEPG(ThreeDParams threeDParams) {
        Bundle threeDBundle = new Bundle();
        threeDBundle.putString(Constants.EXTRA_THREE_D_ACS_URL, threeDParams.getAcsUrl());
        threeDBundle.putString(Constants.EXTRA_THREE_D_TERM_URL, threeDParams.getTermUrl());
        threeDBundle.putString(Constants.EXTRA_THREE_D_MD, threeDParams.getMD());
        application.getFragmentManager().showDialogFragment(ThreeDSecureDialogFragment.class, threeDBundle);
    }

    private void handleSuccessfulDeposit(TransactionMethodResult result) {
        transId = result.getTransactionId();
        User user = application.getUser();

        double newBalance = result.getBalance();
        double amount = AmountUtil.getDoubleAmount(AmountUtil.parseAmountToLong(getAmount(),
                user.getCurrency()));
        double dollarAmount = result.getDollarAmount();

        // Update the user with new balance.
        user.setBalance(AmountUtil.getLongAmount(newBalance));
        user.setBalanceWF(AmountUtil.getFormattedAmount(user.getBalance(), user.getCurrency()));
        if (application.isRegulated() && !RegulationUtils.isAfterFirstDeposit()) {
            RegulationUtils.setAfterFirstDeposit();
        }
        application.setUser(user);
        ///////////////////////////////////////

        application.postEvent(new DepositEvent(amount, dollarAmount, result.isFirstDeposit(), result.getTransactionId()));

        SimpleDateFormat dateFormat = new SimpleDateFormat(TRANSACTION_DATE_FORMAT_PATTERN,
                application.getLocale());
        String dateFormatted = dateFormat.format(new Date(TimeUtils.getCurrentServerTime()));
        application.getNotificationManager().showNotificationDepositOK(getAmount(), dateFormatted,
                result.getTransactionId());

        // Navigate to next screen
        if (result.getThreeDParams() != null) {
            handle3DSecureOnEPG(result.getThreeDParams());
        } else {
            handleSuccessfulDepositRedirect();
        }


        application.getDeepLinksManager().fireServerPixelDeposit(transId);
    }

    protected void handleSuccessfulDepositRedirect() {
        NavigationEvent navigationEvent = null;
        if (application.isEtrader()) {
            if (application.isRegulated() && !RegulationUtils.isMandQuestionnaireAnswered()) {
                navigationEvent = new NavigationEvent(Screen.MANDATORY_QUESTIONNAIRE,
                        NavigationType.INIT);
            }
        } else {
            if (RegulationUtils.checkRegulationStatusAndDoNecessary(false) && RegulationUtils.showUploadDocuments()) {
                navigationEvent = new NavigationEvent(application.getHomeScreen(), NavigationType.INIT);
            }
        }
        application.postEvent(navigationEvent);
    }

    @Override
    public void onDismiss() {
        setupDefaultDepositAmount();
    }

    //Override point
    protected void setupDefaultDepositAmount() {
    }

}