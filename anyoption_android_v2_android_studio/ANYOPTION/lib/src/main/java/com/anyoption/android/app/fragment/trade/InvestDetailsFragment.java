package com.anyoption.android.app.fragment.trade;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

import com.anyoption.android.R;
import com.anyoption.android.app.event.ChartEvent;
import com.anyoption.android.app.event.InvestmentEvent;
import com.anyoption.android.app.event.InvestmentEvent.Type;
import com.anyoption.android.app.event.LightstreamerUpdateEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.OptionsDrawerEvent;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.InvestmentUtil;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.json.results.ChartDataMethodResult;
import com.lightstreamer.ls_client.UpdateInfo;

import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * Displays information about the open options (investments).
 * 
 * @author Mario Kutlev
 */
public class InvestDetailsFragment extends BaseAssetNestedFragment implements Callback {
	
	private static final String INVESTMENT_TIME_FORMAT_PATTERN = "HH:mm";

	private static final int MESSAGE_STATE_CHANGED = 1;
	private static final int MESSAGE_LEVEL_CHANGED = 2;

	private View root;
	private View openOptionsLayout;
	private View investmentTimeLayout;
	private View currentLevelLayout;
	private View investmentLevelLayout;
	
	private TextView openOptionsView;
	private View blueDotView;
	private TextView currentLevelView;
	private TextView currentLevelLabelView;
	private TextView investedAmountView;
	private View profitLabelView;
	private TextView profitAmountView;
	private View yellowRibbonView;
	
	// Views used for selected investment information
	private TextView investmentTimeView;
	private TextView investmentLevelView;
	private TextView investmentAmountView;
	private TextView investmentProfitAmountView;
	protected ImageView investmentIcon;

	private View getQuotesButton;

	private SkinGroup skinGroup;
	private Currency currency;

	// Investment information fields
	protected double currentLevel;
	protected ArrayList<Investment> investments;
	protected int focusedInvestmentIndex;
	private SimpleDateFormat investmentTimeFormat;
	private double totalInvestedAmount;
	private double totalExpectedProfit;

	private int decimalPointDigits;
	private DecimalFormat levelFormat;

	private int opportunityState;

	private boolean isTablet;
	private boolean isGetQuotesVisible;
	
	private Handler handler;

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static InvestDetailsFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new InvestDetailsFragment.");
		InvestDetailsFragment fragment = new InvestDetailsFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	public static InvestDetailsFragment newInstance(Market market) {
		Bundle args = new Bundle();
		args.putSerializable(Constants.EXTRA_MARKET, market);
		return newInstance(args);
	}

	/**
	 * Use {@link #newInstance()} method instead this constructor.
	 */
	public InvestDetailsFragment() {
		// Empty constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Skin skin = application.getSkins().get(application.getSkinId());
		skinGroup = skin.getSkinGroup();
		
		setUpUserDependentFields();

		investments = new ArrayList<Investment>();
		investmentTimeFormat = new SimpleDateFormat(INVESTMENT_TIME_FORMAT_PATTERN);
		decimalPointDigits = 0;
		levelFormat = Utils.getDecimalFormat(decimalPointDigits);

		handler = new Handler(Looper.getMainLooper(), this);

		isTablet = getResources().getBoolean(R.bool.isTablet);
		resetFields();
	}
	
	private void resetFields() {
		opportunityState = 0;
		investments.clear();
		currentLevel = 0.0;
		focusedInvestmentIndex = -1;
		isGetQuotesVisible = false;

		totalInvestedAmount = 0.0;
		totalExpectedProfit = 0.0;
	}

	private void setUpUserDependentFields() {
		// Update the currency
		currency = application.getCurrency();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		root = inflater.inflate(R.layout.invest_details_fragment, container, false);
		
		openOptionsLayout = root.findViewById(R.id.invest_details_open_options_layout);
		investmentTimeLayout = root.findViewById(R.id.invest_details_time_layout);
		currentLevelLayout = root.findViewById(R.id.invest_details_current_level_layout);
		investmentLevelLayout = root.findViewById(R.id.invest_details_level_layout);
		
		openOptionsView = (TextView) root.findViewById(R.id.invest_details_open_options);
		blueDotView = root.findViewById(R.id.invest_details_blue_circle);
		
		currentLevelView = (TextView) root.findViewById(R.id.invest_details_current_level);
		currentLevelLabelView = (TextView) root.findViewById(R.id.invest_details_current_level_label);
		investedAmountView = (TextView) root.findViewById(R.id.invest_details_invested);
		
		profitLabelView = root.findViewById(R.id.invest_details_return_label);
		profitAmountView = (TextView) root.findViewById(R.id.invest_details_return);
		yellowRibbonView = root.findViewById(R.id.invest_details_profitable_image);
		
		investmentTimeView = (TextView) root.findViewById(R.id.invest_details_time);
		investmentLevelView = (TextView) root.findViewById(R.id.invest_details_level);
		investmentAmountView = (TextView) root.findViewById(R.id.invest_details_inv_invested);
		investmentProfitAmountView = (TextView) root.findViewById(R.id.invest_details_inv_profit);
		investmentIcon = (ImageView) root.findViewById(R.id.invest_details_inv_icon);
		
		getQuotesButton = root.findViewById(R.id.invest_details_get_quotes_button);
		if (market != null && market.isOptionPlusMarket()) {
			setUpGetQuoteButtons();
		}
		
		return root;
	}

	@Override
	public void onResume() {
		super.onResume();

		registerForEvents();
	}

	@Override
	public void onPause() {
		application.unregisterForAllEvents(this);
		super.onPause();
	}

	@Override
	public void onDestroyView() {
		application.unregisterForAllEvents(this);
		super.onDestroyView();
	}

	/**
	 * Register for events depending on is market null or not.
	 */
	private void registerForEvents() {
		application.registerForEvents(this, LoginLogoutEvent.class);

		if (market != null) {
			application.registerForEvents(this, LightstreamerUpdateEvent.class, ChartEvent.class,
					InvestmentEvent.class);
		}
	}

	/**
	 * Called when a {@link ChartEvent} occurs.
	 * 
	 * @param event
	 */
	public void onEventMainThread(ChartEvent event) {
		if (event.getMarketId() != market.getId()) {
			return;
		}

		if (event.getType() == ChartEvent.Type.DATA_REQUEST_SUCCESS) {
			ChartDataMethodResult data = event.getChartData();
			Investment[] investmentsArr = data.getInvestments();
			investments.clear();
			if (investmentsArr != null && investmentsArr.length > 0) {
				investments.addAll(Arrays.asList(investmentsArr));
			}

			decimalPointDigits = data.getDecimalPoint();
			levelFormat.applyPattern(Utils.getDecimalFormatPattern(decimalPointDigits));
			
			showAllInvestmentsInfo();
			updateInvestmentsInfo();

			if (market.isOptionPlusMarket()) {
				updateGetQuote();
			}

		} else if (event.getType() == ChartEvent.Type.DATA_REQUEST_FAIL) {
			investments.clear();
			showAllInvestmentsInfo();
			updateInvestmentsInfo();
		}
	}

	/**
	 * Called when a {@link LightstreamerUpdateEvent} occurs.
	 * 
	 * @param event
	 */
	public void onEvent(LightstreamerUpdateEvent event) {
		if (event.getMarketId() == market.getId()) {
			updateItem(event.getUpdateInfo());
		}
	}

	public void updateItem(UpdateInfo update) {

		if (update.isValueChanged(skinGroup.getLevelUpdateKey())) {
			// Update the level
			currentLevel = Double.parseDouble(update.getNewValue(skinGroup.getLevelUpdateKey())
					.replaceAll(",", ""));
			if (!handler.hasMessages(MESSAGE_LEVEL_CHANGED)) {
				handler.obtainMessage(MESSAGE_LEVEL_CHANGED).sendToTarget();
			}
		}

		if (opportunityState == 0 || update.isValueChanged(LightstreamerManager.COLUMN_ET_STATE)) {
			opportunityState = Integer.parseInt(update.getNewValue(LightstreamerManager.COLUMN_ET_STATE));
			if (!handler.hasMessages(MESSAGE_STATE_CHANGED)) {
				handler.obtainMessage(MESSAGE_STATE_CHANGED).sendToTarget();
			}
		}
	}

	@Override
	public boolean handleMessage(Message msg) {
		switch (msg.what) {
		case MESSAGE_STATE_CHANGED:
			onStateChanged();
			break;
		case MESSAGE_LEVEL_CHANGED:
			onLevelChanged();
			break;
		}

		return true;
	}

	/**
	 * Called when the state is changed to update the UI
	 */
	private void onStateChanged() {
		updateGetQuote();
		
		if (opportunityState == Opportunity.STATE_CLOSED) {
			currentLevelLabelView.setText(R.string.closingLevel);
		} else {
			currentLevelLabelView.setText(R.string.currentLevel);
		}
	}

	/**
	 * Called when the level is changed to update the UI
	 */
	private void onLevelChanged() {
		currentLevelView.setText(levelFormat.format(currentLevel));
		
		updateExpectedProfit();
		updateInvestmentViews();
		updateYellowRibbon();
	}

	/**
	 * Called when a {@link InvestmentEvent} occurs.
	 * 
	 * @param event
	 */
	public void onEventMainThread(InvestmentEvent event) {
		if(event.getMarketId() != market.getId()) {
			return;
		}
		
		if (event.getType() == Type.INVEST_SUCCESS) {
			investments.add(event.getInvestment());
			updateInvestmentsInfo();
			
			if (market.isOptionPlusMarket() && investments.size() == 1) {
				// We've added the first investment
				updateGetQuote();
			}
		} else if(event.getType() == Type.INVESTMENT_FOCUSED || (event.getType() == Type.INVESTMENT_CHANGE_FOCUS
				&& (focusedInvestmentIndex == -1 || event.getInvestment().getId() != investments.get(focusedInvestmentIndex).getId()))) {
			for (int i = 0; i < investments.size(); i++) {
				if (investments.get(i).getId() == event.getInvestment().getId()) {
					showInvestmentInfo(i);
					break;
				}
			}
		} else if (event.getType() == Type.INVESTMENT_UNFOCUSED || (event.getType() == Type.INVESTMENT_CHANGE_FOCUS)) {
			showAllInvestmentsInfo();
		} else if (event.getType() == Type.INVESTMENT_SOLD) {
			Investment investment = event.getInvestment();
			for (int i = 0; i < investments.size(); i++) {
				if (investments.get(i).getId() == investment.getId()) {
					investments.remove(i);
					break;
				}
			}
			updateInvestmentsInfo();
			
			if (market.isOptionPlusMarket() && investments.isEmpty()) {
				// There aren't investments.
				updateGetQuote();
			}
		}
	}
	
	/**
	 * Called when a {@link LoginLogoutEvent} occurs.
	 * 
	 * @param event
	 */
	public void onEventMainThread(LoginLogoutEvent event) {
		setUpUserDependentFields();
		updateInvestmentsInfo();
	}
	
	private void updateInvestmentsInfo() {
		openOptionsView.setText(String.valueOf(investments.size()));
		blueDotView.setVisibility(investments.size() > 0 ? View.VISIBLE : View.GONE);

		updateTotalInvestedAmount();
		updateExpectedProfit();
	}

	private void updateExpectedProfit() {
		double expectedProfit = 0;
		for (int i = 0; i < investments.size(); i++) {
			expectedProfit += InvestmentUtil.getOpenInvestmentReturn(investments.get(i), currentLevel);
		}

		totalExpectedProfit = expectedProfit;
		AmountUtil.setTextViewAmount(profitAmountView, totalExpectedProfit, currency.getSymbol(),
				currency.getIsLeftSymbolBool(), currency.getDecimalPointDigits());
	}

	private void updateTotalInvestedAmount() {
		long totalAmount = 0;
		for (int i = 0; i < investments.size(); i++) {
			totalAmount += investments.get(i).getAmount();
		}

		totalInvestedAmount = AmountUtil.getDoubleAmount(totalAmount);
		AmountUtil.setTextViewAmount(investedAmountView, totalInvestedAmount, currency.getSymbol(),
				currency.getIsLeftSymbolBool(), currency.getDecimalPointDigits());
	}
	
	/**
	 * Updates the yellow ribbon when there is not selected investment.
	 */
	private void updateYellowRibbon() {
		if (focusedInvestmentIndex < 0) {
			// All investments info is displayed.
			if (totalExpectedProfit > totalInvestedAmount) {
				yellowRibbonView.setVisibility(View.VISIBLE);
			} else {
				yellowRibbonView.setVisibility(View.GONE);
			}
		}
	}

	/**
	 * Updates the investment's views depending on currentLevel. If no investment is focused,
	 * nothing happens.
	 */
	private void updateInvestmentViews() {
		if (focusedInvestmentIndex >= 0 && focusedInvestmentIndex < investments.size()) {
			double expectedProfit = InvestmentUtil.getOpenInvestmentReturn(
					investments.get(focusedInvestmentIndex), currentLevel);
			AmountUtil.setTextViewAmount(investmentProfitAmountView, expectedProfit,
					currency.getSymbol(), currency.getIsLeftSymbolBool(),
					currency.getDecimalPointDigits());
			setUpInvestmentIcon();

			if (InvestmentUtil.isOpenInvestmentProfitable(investments.get(focusedInvestmentIndex),
					currentLevel)) {
				yellowRibbonView.setVisibility(View.VISIBLE);
			} else {
				yellowRibbonView.setVisibility(View.GONE);
			}
		}
	}

	protected void setUpInvestmentIcon() {
		investmentIcon.setImageResource(InvestmentUtil.getInvestmentIcon(
				investments.get(focusedInvestmentIndex), currentLevel));
	}
	
	private void showInvestmentInfo(int investmentIndex) {
		if (focusedInvestmentIndex < 0) {
			// Focused the first investment so the layout must be updated.
			openOptionsLayout.setVisibility(View.GONE);
			currentLevelLayout.setVisibility(View.GONE);
			investedAmountView.setVisibility(View.GONE);
			profitAmountView.setVisibility(View.GONE);

			investmentTimeLayout.setVisibility(View.VISIBLE);
			investmentLevelLayout.setVisibility(View.VISIBLE);
			investmentAmountView.setVisibility(View.VISIBLE);
			investmentProfitAmountView.setVisibility(View.VISIBLE);

			if (!isGetQuotesVisible) {
				profitAmountView.setVisibility(View.GONE);
				investmentProfitAmountView.setVisibility(View.VISIBLE);
			}
		}

		focusedInvestmentIndex = investmentIndex;
		Investment investment = investments.get(investmentIndex);
		investmentLevelView.setText(investment.getLevel());
		AmountUtil.setTextViewAmount(investmentAmountView, investment.getAmount(),
				currency.getSymbol(), currency.getIsLeftSymbolBool(),
				currency.getDecimalPointDigits());

		// Set the time with smaller AM/PM font size.
		String timeFormatted = investmentTimeFormat.format(investment.getTimeCreated());
		investmentTimeView.setText(timeFormatted);

		updateInvestmentViews();
	}
	
	private void showAllInvestmentsInfo() {
		focusedInvestmentIndex = -1;
		
		investmentTimeLayout.setVisibility(View.GONE);
		investmentLevelLayout.setVisibility(View.GONE);
		investmentAmountView.setVisibility(View.GONE);

		openOptionsLayout.setVisibility(View.VISIBLE);
		currentLevelLayout.setVisibility(View.VISIBLE);
		investedAmountView.setVisibility(View.VISIBLE);
		
		if(!isGetQuotesVisible) {
			investmentProfitAmountView.setVisibility(View.GONE);
			profitAmountView.setVisibility(View.VISIBLE);
		}
	}

	private void setUpGetQuoteButtons() {
		getQuotesButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				application.postEvent(new OptionsDrawerEvent(market.getId(),
						OptionsDrawerEvent.Type.OPEN_DRAWER_REQUEST));
			}
		});
	}

	private void showGetQuote(boolean show) {
		isGetQuotesVisible = show;
		if (isGetQuotesVisible) {
			profitLabelView.setVisibility(View.GONE);
			profitAmountView.setVisibility(View.GONE);
			investmentProfitAmountView.setVisibility(View.GONE);
			getQuotesButton.setVisibility(View.VISIBLE);
		} else {
			getQuotesButton.setVisibility(View.GONE);
			profitLabelView.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * Show or hide Get quote button depending on the market type and investments size.
	 */
	private void updateGetQuote() {
		if (market != null && market.isOptionPlusMarket() && !investments.isEmpty() && !isTablet
				&& opportunityState < Opportunity.STATE_CLOSING_1_MIN) {
			showGetQuote(true);
		} else {
			showGetQuote(false);
		}
	}

	@Override
	protected void onMarketChanged() {
		super.onMarketChanged();

		registerForEvents();

		currentLevel = 0.0;
		resetFields();
		updateInvestmentsInfo();
		updateGetQuote();
	}

	@Override
	public void loadData() {
		// Do nothing
	}

}
