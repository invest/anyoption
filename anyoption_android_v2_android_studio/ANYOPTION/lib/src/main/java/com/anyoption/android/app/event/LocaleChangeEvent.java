package com.anyoption.android.app.event;

import com.anyoption.android.app.Application.Screenable;

/**
 * Event that occurs when the language of the application is going to be changed.
 * @author Anastas Arnaudov
 *
 */
public class LocaleChangeEvent {

	private Screenable toScreen;
	
	/**
	 * Event that occurs when the language of the application is going to be changed.
	 * @author Anastas Arnaudov
	 *@param The Screenable we want to navigate to after the Locale change.
	 */
	public LocaleChangeEvent(Screenable toScreen) {
		this.toScreen = toScreen;
	}

	public Screenable getToScreen() {
		return toScreen;
	}

	public void setToScreen(Screenable toScreen) {
		this.toScreen = toScreen;
	}

}
