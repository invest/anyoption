package com.anyoption.android.app.event;

import com.anyoption.json.results.ChartDataMethodResult;

public class ChartEvent {

	/**
	 * Chart event type
	 */
	public enum Type {
		DATA_REQUESTED, DATA_REQUEST_SUCCESS, DATA_REQUEST_FAIL
	}

	private long marketId;
	private Type type;
	private ChartDataMethodResult chartData;

	public ChartEvent(long marketId, Type type, ChartDataMethodResult chartData) {
		this.marketId = marketId;
		this.type = type;
		this.chartData = chartData;
	}

	public ChartEvent(long marketId, Type type) {
		this(marketId, type, null);
	}

	public long getMarketId() {
		return marketId;
	}

	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	/**
	 * Returns the chart data. It can be null in case of some errors.
	 * 
	 * @return chart data.
	 */
	public ChartDataMethodResult getChartData() {
		return chartData;
	}

	public void setChartData(ChartDataMethodResult chartData) {
		this.chartData = chartData;
	}

}
