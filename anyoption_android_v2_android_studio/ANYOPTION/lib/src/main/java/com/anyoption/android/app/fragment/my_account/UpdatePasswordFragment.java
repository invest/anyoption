package com.anyoption.android.app.fragment.my_account;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.R;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.manager.SharedPreferencesManager;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.RequestButton.InitAnimationListener;
import com.anyoption.android.app.widget.form.Form;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.json.requests.ChangePasswordMethodRequest;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

/**
 * @author liors
 *
 */
public class UpdatePasswordFragment extends NavigationalFragment {

	public static final String TAG = UpdatePasswordFragment.class.getSimpleName();

	private Form form;
	private FormEditText password;
	private FormEditText newPassword;
	private FormEditText reNewPassword;
	private RequestButton updatePasswordButton;

	private static boolean fromIsNeedPasswordChanged = false;

	@Override
	protected Screenable setupScreen() {
		return Screen.UPDATE_PASSWORD;
	}

	public static UpdatePasswordFragment newInstance(Bundle bundle) {
		UpdatePasswordFragment updatePassword = new UpdatePasswordFragment();
		updatePassword.setArguments(bundle);
		return updatePassword;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.update_password_layout, container, false);
		password = (FormEditText) rootView.findViewById(R.id.old_password);
		password.addValidator(ValidatorType.EMPTY, ValidatorType.PASSWORD_MINIMUM_CHARACTERS, ValidatorType.ALNUM);
		password.setFieldName(Constants.FIELD_PASSWORD);

		newPassword = (FormEditText) rootView.findViewById(R.id.new_password);
		newPassword.addValidator(ValidatorType.EMPTY, ValidatorType.PASSWORD_MINIMUM_CHARACTERS, ValidatorType.ALNUM);
		newPassword.setFieldName(Constants.FIELD_PASSWORD_NEW);

		reNewPassword = (FormEditText) rootView.findViewById(R.id.re_new_password);
		reNewPassword.addValidator(ValidatorType.EMPTY, ValidatorType.PASSWORD_MINIMUM_CHARACTERS, ValidatorType.ALNUM);
		reNewPassword.addValidatorForEquality(newPassword, getString(R.string.errorRetypedPasswordNOTMatch));
		reNewPassword.setFieldName(Constants.FIELD_PASSWORD_RE_TYPE);

		updatePasswordButton = (RequestButton) rootView.findViewById(R.id.update_password_button);
		updatePasswordButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (form.checkValidity()) {
					updatePasswordButton.startLoading(new InitAnimationListener() {
						@Override
						public void onAnimationFinished() {
							form.submit();
						}
					});
				}
			}
		});

		form = new Form(new Form.OnSubmitListener() {
			@Override
			public void onSubmit() {
				submitUpdatePassword();
			}
		});
		form.addItem(password, newPassword, reNewPassword);

		return rootView;
	}

	protected void submitUpdatePassword() {
		ChangePasswordMethodRequest request = new ChangePasswordMethodRequest();
		if (password.getValue().equals(newPassword.getValue())) {
			application.getNotificationManager().showNotificationError(getString(R.string.update_password_change));
			updatePasswordButton.stopLoading();
		} else {
			request.setPassword(password.getValue());
			request.setPasswordNew(newPassword.getValue());
			request.setPasswordReType(reNewPassword.getValue());

			if (application.isLoggedIn() && application.getUser().getIsNeedChangePassword()) {
				fromIsNeedPasswordChanged = true;
			} else {
				fromIsNeedPasswordChanged = false;
			}

			if (!application.getCommunicationManager().requestService(UpdatePasswordFragment.this,
					"changePasswordCallBack", Constants.SERVICE_GET_CHANGE_PASSWORD, request, UserMethodResult.class)) {
				updatePasswordButton.stopLoading();
			}
		}
	}

	public void changePasswordCallBack(Object resultObj) {
		Log.d(TAG, "changePasswordCallBack");
		UserMethodResult result = (UserMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "update password successfully");
			application.getNotificationManager().showNotificationSuccess(result.getUserMessages()[0].getMessage());

			// Save the new password
			SharedPreferencesManager preferencesManager = application.getSharedPreferencesManager();
			if (preferencesManager.getString(Constants.PREFERENCES_PASSWORD_LOGIN, null) != null) {
				preferencesManager.putString(Constants.PREFERENCES_PASSWORD_LOGIN, newPassword.getValue());
			}
			String newPasswordValue = result.getUser().getEncryptedPassword();
			preferencesManager.putString(Constants.PREFERENCES_PASSWORD, newPasswordValue);

			if (fromIsNeedPasswordChanged) {
				application.postEvent(new NavigationEvent(application.getHomeScreen(), NavigationType.INIT));
			} else {
				application.getFragmentManager().onBack();
			}
			return;
		} else {
			ErrorUtils.displayFieldError(form, result);
			updatePasswordButton.stopLoading();
		}
	}
}