package com.anyoption.android.app.manager;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.activity.BubblesActivity;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.popup.MarketingDialogFragment;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.adapter.MarketsExpandableListAdapter;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.requests.FireServerPixelMethodRequest;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Manages the Deep Links that opens the application. Parse URI, get parameters, save navigation flows, etc.
 *
 * @author Anastas Arnaudov
 */
public class DeepLinksManager {

    public static final String TAG = DeepLinksManager.class.getSimpleName();

    public static final String DEEP_LINK_DEPOSIT = "Deposit";
    public static final String DEEP_LINK_REGISTER = "Register";
    public static final String DEEP_LINK_TRADE = "Trade";
    public static final String DEEP_LINK_BONUS = "Bonus";
    public static final String DEEP_LINK_ASSET_BINARY = "Bin";
    public static final String DEEP_LINK_ASSET_OPTION_PLUS = "Optplus";
    public static final String DEEP_LINK_MARKETING = "Marketing";
    public static final String DEEP_LINK_PIXEL = "Pixel";
    public static final String DEEP_LINK_DYNAMICS = "Dynamics";
    public static final String DEEP_LINK_BUBBLES = "Bubbles";

    public static final String PARAM_PAGE = "pager";
    public static final String PARAM_PUBLISHER_ID = "pid";
    public static final String PARAM_PUBLISHER_IDENTIFIER = "aff_sub1";
    public static final String PARAM_COMBINATION_ID = "combid";

    private Application application;
    protected boolean isDeepLink;
    protected Bundle deepLinkBundle;
    protected Application.Screenable secondScreen;
    protected String marketingPage;
    protected boolean isTargetingCampaign;
    protected boolean isLoginNeeded;
    private boolean isFacebookDeepLink;

    public DeepLinksManager(Application application) {
        Log.d(TAG, "Init.");
        this.application = application;
    }

    private void reset() {
        Log.d(TAG, "Clearing data.");
        handledDeepLink();
        clearBundle();
        marketingPage = null;
        isTargetingCampaign = false;
    }

    public void onResumeMainActivity() {
        Log.d(TAG, "onResumeMainActivity");
        Log.d(TAG, "Checking the Deep Linking...");
        if (isDeepLink()) {
            Log.d(TAG, "We have a Deep Link.");
            Log.d(TAG, "Checking if it is a Marketing or Targeting campaign...");
            if (getMarketingPage() != null) {
                Log.d(TAG, "It is a Marketing campaign.");
                Bundle marketingBundle = new Bundle();
                marketingBundle.putString(Constants.EXTRA_PAGE, getMarketingPage());
                application.getFragmentManager().showDialogFragment(MarketingDialogFragment.class, marketingBundle);
            } else {
                Log.d(TAG, "It is a normal Deep Link.");
                Application.Screenable deepLinkScreen = (Application.Screenable) getDeepLinkBundle().getSerializable(Constants.EXTRA_SCREEN);
                Log.d(TAG, "Screen = " + deepLinkScreen);
                Log.d(TAG, "Checking if user is logged in...");
                if (application.isLoggedIn()) {
                    Log.d(TAG, "User is logged in.");
                    if (deepLinkScreen != Application.Screen.BUBBLES) {
                        application.postEvent(new NavigationEvent(deepLinkScreen, NavigationEvent.NavigationType.INIT, getDeepLinkBundle()));
                    } else {
                        application.setCurrentScreen(Application.Screen.BUBBLES);
                        Intent intent = new Intent(application.getApplicationContext(), application.getSubClassForClass(BubblesActivity.class));
                        application.getCurrentActivity().startActivity(intent);
                    }

                    reset();
                } else {
                    Log.d(TAG, "User is NOT logged in.");
                    if (deepLinkScreen == getRegisterScreen()) {
                        application.postEvent(new NavigationEvent(deepLinkScreen, NavigationEvent.NavigationType.INIT, getDeepLinkBundle()));
                        reset();
                    } else {
                        application.postEvent(new NavigationEvent(getLoginScreen(), NavigationEvent.NavigationType.INIT));
                    }
                }
            }
        } else {
            Log.d(TAG, "No Deep Links.");
        }
    }

    public void onPauseMainActivity() {
        Log.d(TAG, "onPauseMainActivity");
        reset();
    }

    public void handleDeferredDeepLinks(Uri targetUri) {
        if (targetUri != null) {
            Log.d(TAG, "Clearing previous deep links data.");
            reset();

            deepLinkBundle = new Bundle();
            try {
                if (targetUri.toString().contains("fb.me")) {
                    Log.d(TAG, "There is a stored link for Invites or Shares! Ignore it as it should only open the app!");
                } else {
                    parseUri(targetUri);
                    setupLoginNeeded();
                    isDeepLink = deepLinkBundle != null;
                }
            } catch (Exception e) {
                Log.e(TAG, "Error parsing the Deep Link.", e);
                reset();
            }
        }
    }

    /**
     * Parse the Intent to figure out whether it is a FB Deep Link.
     * The data bundle is saved in the {@link DeepLinksManager}.
     *
     * @return True if it is a Deep Link.
     */
    public boolean parseIntent(Context context, Intent intent) {
        Log.d(TAG, "Parsing Intent...");
        isFacebookDeepLink = application.getFacebookManager().isFacebookUri(context, intent);
        Uri uri = intent.getData();
        if (uri != null) {
            Log.d(TAG, "Is from Deep Link = true");

            Log.d(TAG, "Clearing previous deep links data.");
            reset();

            Log.d(TAG, "Extracting the target Screen...");
            deepLinkBundle = new Bundle();
            try {
                if (uri != null && !uri.toString().equals("null")) {
                    parseUri(intent.getData());
                } else {
                    Log.e(TAG, "Uri is null");
                    reset();
                }
            } catch (Exception e) {
                Log.e(TAG, "Error parsing the Deep Link.", e);
                reset();
            }
        } else {
            Log.d(TAG, "Uri is empty.");
        }
        setupLoginNeeded();
        isDeepLink = deepLinkBundle != null;
        return isDeepLink;
    }

    protected void parseUri(Uri uri) {
        Log.d(TAG, "Parsing Uri = " + uri);
        String data = uri.toString();
        String scheme = uri.getScheme();
        //Extracting the screen from the data : scheme://screen?parameters
        boolean isParams = data.contains("?");
        Log.d(TAG, "Uri data: " + data);
        Log.d(TAG, "Uri scheme: " + scheme);
        Log.d(TAG, "Uri isParams: " + (isParams ? "there are params" : "there are no params"));

        String screen = data.substring(scheme.length() + 3, (isParams) ? data.indexOf('?') : data.length());
        Log.d(TAG, "Screen = " + screen);
        boolean isScreen = screen.length() > 0;
        if (isScreen) {
            Map<String, String> paramsMap = new HashMap<String, String>();
            if (isParams) {
                String[] paramsPairs = data.substring(scheme.length() + 3 + screen.length() + 1).split("&");
                Log.d(TAG, "Printing URI parameters map...");
                for (String pair : paramsPairs) {
                    String[] pairMap = pair.split("=");
                    String key = pairMap[0].toLowerCase();//Set the key to be only upper case in case it is misspelled
                    String value = pairMap[1];

                    Log.d(TAG, "URI: key = " + key + "  value = " + value);

                    paramsMap.put(key, value);
                }
                Log.d(TAG, "End of URI parameters map...");
            }

            Log.d(TAG, "Target Screen = " + screen);
            if (screen.startsWith(DEEP_LINK_DEPOSIT)) {
                deepLinkBundle.putSerializable(Constants.EXTRA_SCREEN, Application.Screen.DEPOSIT_MENU);
            } else if (screen.startsWith(DEEP_LINK_REGISTER)) {
                if (screen.contains("_")) {
                    //A second Screen is attached:
                    Log.d(TAG, "Second Screen is attached.");
                    String secondScreenString = screen.split("_")[1];
                    if (secondScreenString.startsWith(DEEP_LINK_TRADE)) {
                        Log.d(TAG, "Second Screen = " + Application.Screen.TRADE);
                        setSecondScreen(Application.Screen.TRADE);
                    }
                }
                deepLinkBundle.putSerializable(Constants.EXTRA_SCREEN, getRegisterScreen());
            } else if (screen.startsWith(DEEP_LINK_TRADE)) {
                deepLinkBundle.putSerializable(Constants.EXTRA_SCREEN, Application.Screen.TRADE);
            } else if (screen.startsWith(DEEP_LINK_BONUS)) {
                deepLinkBundle.putSerializable(Constants.EXTRA_SCREEN, Application.Screen.BONUSES);
            } else if (screen.startsWith(DEEP_LINK_ASSET_BINARY)) {
                deepLinkBundle.putSerializable(Constants.EXTRA_SCREEN, Application.Screen.ASSET_VIEW_PAGER);
                deepLinkBundle.putLong(Constants.EXTRA_MARKET_PRODUCT_TYPE, Constants.PRODUCT_TYPE_ID_BINARY);
                String[] assetDetails = screen.split("_");
                String assetId = assetDetails[assetDetails.length - 1];
                long marketId = Long.valueOf(assetId);
                deepLinkBundle.putLong(Constants.EXTRA_MARKET_ID, marketId);
            } else if (screen.startsWith(DEEP_LINK_ASSET_OPTION_PLUS)) {
                deepLinkBundle.putSerializable(Constants.EXTRA_SCREEN, Application.Screen.ASSET_VIEW_PAGER);
                deepLinkBundle.putLong(Constants.EXTRA_MARKET_PRODUCT_TYPE, Constants.PRODUCT_TYPE_ID_OPTION_PLUS);
                String[] assetDetails = screen.split("_");
                String assetId = assetDetails[assetDetails.length - 1];
                long marketId = Long.valueOf(assetId);
                deepLinkBundle.putLong(Constants.EXTRA_MARKET_ID, marketId);
            } else if (screen.startsWith(DEEP_LINK_MARKETING)) {
                if (marketingPage != null) {
                    Log.d(TAG, "The app has already been opened with a Marketing link. Do not parse it again.");
                } else {
                    marketingPage = paramsMap.get(PARAM_PAGE);
                    if (paramsMap.get(PARAM_COMBINATION_ID) != null) {
                        long combId = Long.valueOf(paramsMap.get(PARAM_COMBINATION_ID));

                        Log.d(TAG, "Marketing page combination id: " + paramsMap.get(PARAM_COMBINATION_ID));
                        Log.d(TAG, "Invoke setMarketingParams from DeepLinksManager with combid: " + combId);
                        Utils.setMarketingParams(combId, null, null, null, null);
                        Log.d(TAG, "Marketing page parsed combination id: " + combId);
                    } else {
                        Log.d(TAG, "Marketing page combination id is missing");
                        Log.d(TAG, "Invoke setMarketingParams from DeepLinksManager with combid: null");
                        Utils.setMarketingParams(null, null, null, null, null);
                    }
                }
            } else if (screen.startsWith(DEEP_LINK_PIXEL)) {
                isTargetingCampaign = true;

                Long combId = null;
                String affSub1 = null;

                if (paramsMap.get(PARAM_PUBLISHER_ID) != null) {
                    int publisherId = Integer.valueOf(paramsMap.get(PARAM_PUBLISHER_ID));
                    application.getSharedPreferencesManager().putInt(Constants.PREFERENCES_CAMPAIGN_PUBLISHER_ID, publisherId);
                }
                if (paramsMap.get(PARAM_PUBLISHER_IDENTIFIER) != null) {
                    String publisherIdentifier = paramsMap.get(PARAM_PUBLISHER_IDENTIFIER);
                    affSub1 = publisherIdentifier;
                }
                if (paramsMap.get(PARAM_COMBINATION_ID) != null) {
                    combId = Long.valueOf(paramsMap.get(PARAM_COMBINATION_ID));

                }
                Log.d(TAG, "Invoke setMarketingParams from DeepLinksManager with combid: " + combId);
                Utils.setMarketingParams(combId, null, affSub1, null, null);
                deepLinkBundle.putSerializable(Constants.EXTRA_SCREEN, getRegisterScreen());
                fireServerPixelOpen();
            } else if (screen.startsWith(DEEP_LINK_DYNAMICS)) {
                    if((application.isAnyoption() || application.isBDA()) && application.isTablet()) {
                        deepLinkBundle.putSerializable(Constants.EXTRA_SCREEN, Application.Screen.TABLET_TRADE);
                    } else {
                        deepLinkBundle.putSerializable(Constants.EXTRA_SCREEN, Application.Screen.TRADE);
                    }
                    deepLinkBundle.putString(Constants.EXTRA_ASSETS_LIST_FILTER, MarketsExpandableListAdapter.FILTER_DYNAMICS);
            } else if (screen.startsWith(DEEP_LINK_BUBBLES)) {
                deepLinkBundle.putSerializable(Constants.EXTRA_SCREEN, Application.Screen.BUBBLES);
            } else {
                Log.d(TAG, "There is no known Screen or it is incorrect!");
                reset();
            }
        } else {
            Log.d(TAG, "There is no known Screen or it is incorrect!");
            reset();
        }
    }

    public boolean isDeepLink() {
        return isDeepLink;
    }

    public Bundle getDeepLinkBundle() {
        return deepLinkBundle;
    }

    public void clearBundle() {
        deepLinkBundle = null;
    }

    public void handledDeepLink() {
        isDeepLink = false;
    }

    public boolean isTargetingCampaign() {
        return isTargetingCampaign;
    }

    public Application.Screenable getAfterRegisterScreen() {
        return secondScreen;
    }

    public void setSecondScreen(Application.Screenable secondScreen) {
        this.secondScreen = secondScreen;
    }

    public String getMarketingPage() {
        return marketingPage;
    }

    private void setupLoginNeeded() {
        if (application.isLoggedIn()) {
            isLoginNeeded = false;
            return;
        }

        isLoginNeeded = true;
        if (deepLinkBundle != null) {
            Application.Screenable screen = (Application.Screenable) deepLinkBundle.getSerializable(Constants.EXTRA_SCREEN);
            if (screen == null) return;
            if (screen == getLoginScreen() || screen == getRegisterScreen()) {
                isLoginNeeded = false;
            }
        }
    }

    public boolean isLoginNeeded() {
        return isLoginNeeded;
    }

    public boolean handleNavigationLoggedIn() {
        if (isDeepLink()) {
            Log.d(TAG, "App is opened by Deep Link.");
            Application.Screen deepLinkScreen = (Application.Screen) getDeepLinkBundle().getSerializable(Constants.EXTRA_SCREEN);
            if (deepLinkScreen != Application.Screen.BUBBLES) {
                application.postEvent(new NavigationEvent(deepLinkScreen, NavigationEvent.NavigationType.INIT, getDeepLinkBundle()));
            } else {
                application.setCurrentScreen(Application.Screen.BUBBLES);
                Intent intent = new Intent(application.getApplicationContext(), application.getSubClassForClass(BubblesActivity.class));
                application.getCurrentActivity().startActivity(intent);
            }
            handledDeepLink();
            return true;
        } else {
            return false;
        }
    }

    public boolean handleNavigationNOTLoggedIn() {
        if (isDeepLink() && !isLoginNeeded()) {
            Application.Screen deepLinkScreen = (Application.Screen) getDeepLinkBundle().getSerializable(Constants.EXTRA_SCREEN);
            application.postEvent(new NavigationEvent(deepLinkScreen, NavigationEvent.NavigationType.INIT, getDeepLinkBundle()));
            handledDeepLink();
            return true;
        } else {
            return false;
        }
    }

    protected Application.Screenable getRegisterScreen() {
        return Application.Screen.REGISTER;
    }

    protected Application.Screenable getLoginScreen() {
        return Application.Screen.LOGIN;
    }

    //////////////              REQUESTs and Callbacks          /////////////////////////

    public void fireServerPixelOpen() {
        Log.d(TAG, "fireServerPixelOpen.");
        FireServerPixelMethodRequest request = new FireServerPixelMethodRequest();
        request.setDeviceId(Utils.getUDID());
        request.setPixelTypeId(Constants.PIXEL_TYPE_OPEN);
        request.setPublisherId(application.getSharedPreferencesManager().getInt(Constants.PREFERENCES_CAMPAIGN_PUBLISHER_ID, 0));
        request.setPublisherIdentifier(application.getSharedPreferencesManager().getString(Constants.PREFERENCES_AFF_SUB1, null));

        application.getCommunicationManager().requestService(this, "fireServerPixelCallback", Constants.SERVICE_FIRE_SERVER_PIXEL, request, MethodResult.class, false, false);
    }

    public void fireServerPixelRegister() {
        if (application.getSharedPreferencesManager().getInt(Constants.PREFERENCES_CAMPAIGN_PUBLISHER_ID, 0) != 0) {
            Log.d(TAG, "fireServerPixelRegister.");
            FireServerPixelMethodRequest request = new FireServerPixelMethodRequest();
            request.setDeviceId(Utils.getUDID());
            request.setPixelTypeId(Constants.PIXEL_TYPE_REGISTER);
            request.setPublisherId(application.getSharedPreferencesManager().getInt(Constants.PREFERENCES_CAMPAIGN_PUBLISHER_ID, 0));
            request.setPublisherIdentifier(application.getSharedPreferencesManager().getString(Constants.PREFERENCES_AFF_SUB1, null));
            request.setUserId(application.getUser().getId());

            application.getCommunicationManager().requestService(this, "fireServerPixelCallback", Constants.SERVICE_FIRE_SERVER_PIXEL, request, MethodResult.class, false, false);
        }
    }

    public void fireServerPixelDeposit(long transactionId) {
        if (application.getSharedPreferencesManager().getInt(Constants.PREFERENCES_CAMPAIGN_PUBLISHER_ID, 0) != 0) {
            Log.d(TAG, "fireServerPixelDeposit.");
            FireServerPixelMethodRequest request = new FireServerPixelMethodRequest();
            request.setDeviceId(Utils.getUDID());
            request.setPixelTypeId(Constants.PIXEL_TYPE_DEPOSIT);
            request.setPublisherId(application.getSharedPreferencesManager().getInt(Constants.PREFERENCES_CAMPAIGN_PUBLISHER_ID, 0));
            request.setPublisherIdentifier(application.getSharedPreferencesManager().getString(Constants.PREFERENCES_AFF_SUB1, null));
            request.setTransactionId(transactionId);

            application.getCommunicationManager().requestService(this, "fireServerPixelCallback", Constants.SERVICE_FIRE_SERVER_PIXEL, request, MethodResult.class, false, false);
        }
    }

    public void fireServerPixelInstall() {
        if (application.isBgTest()) {
            Log.d(TAG, "fireServerPixelInstall for Adquant is not fired up because the app is not for live but bg_test.");
        } else {
            Log.d(TAG, "fireServerPixelInstall for Adquant.");

            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        String url = "";
                        URL u = null;

                        String adId = Utils.getAndroidAdvertisingID();

                        if (application.isAnyoption() || application.isBDA()) {
                            url = "https://s.mb4ao.com/cb/8-711610_10-3_s3w?ifcontext=&order_id=" + adId + "&mmp=true&user_device=" + Uri.encode(Utils.getDeviceManufacturer() + " " + Utils.getDeviceModel()) + "&user_os=" + Uri.encode(Utils.getDeviceOSVersion()) + "&user_country=";
                        } else if (application.isCopyop()) {
                            url = "https://s.mb4ao.com/cb/8-792574_10-3_qmd?ifcontext=&order_id=" + adId + "&mmp=true&user_device=" + Uri.encode(Utils.getDeviceManufacturer() + " " + Utils.getDeviceModel()) + "&user_os=" + Uri.encode(Utils.getDeviceOSVersion()) + "&user_country=";
                        }

                        u = new URL(url);
                        HttpsURLConnection https = (HttpsURLConnection) u.openConnection();
                        https.connect();
                        Log.d(TAG, "fireServerPixelInstall for Adquant successfully fired: " + url);

                    } catch (Exception exception) {
                        Log.d(TAG, "fireServerPixelInstall for Adquant failed");
                    }
                }
            });
        }
    }


    public void fireServerPixelCallback(Object resultObject) {
        Log.d(TAG, "fireServerPixelCallback.");
        //Don't care of the result!
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
}
