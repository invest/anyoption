package com.anyoption.android.app.widget.form;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.anyoption.android.app.Application;
import com.anyoption.android.R;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.json.results.CurrenciesResult;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class FormCurrencySelector extends FormSelector<Currency> {
	public static final String TAG = FormCurrencySelector.class.getSimpleName();
	protected List<Currency> currencyList;
	protected View rootView;
	protected TextView text;
	protected Currency selectedCurrency;
	protected Map<Long, String> predefinedAmountsMap;
	
	public FormCurrencySelector(Context context) {
		super(context);
	}
	public FormCurrencySelector(Context context, AttributeSet attrs) {
		super(context, attrs);
	}	
	public FormCurrencySelector(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void setupUI() {
		if(rootView == null){
			rootView = View.inflate(getContext(), R.layout.currency_code_selector, this);
			text = (TextView) findViewById(R.id.currency_code_selector_text);
		}
		
		if(!isInEditMode()){
			setupUIForDirection();
			setupCurrency();								
		}else{
			setVisibility(View.VISIBLE);
			text.setText("Currency Selector");
		}
	}
		
	private void setupCurrency() {
		Application.get().getCommunicationManager().requestService(FormCurrencySelector.this, "currencyCallBack", Constants.SERVICE_GET_SKIN_CURRENCIES, new MethodRequest(), CurrenciesResult.class);
	}
	
	public void currencyCallBack(Object result) {
		Log.d(TAG, "currencyCallBack");
		CurrenciesResult currency = (CurrenciesResult) result;
		predefinedAmountsMap = currency.getPredefinedDepositAmountMap();
		currencyList = Arrays.asList(currency.getCurrency());

		setDefualtCurrency(Application.get().getUser().getCurrencyId());
	}	

	public void setDefualtCurrency(long currencyId) {
		int count = 0;
		for (Currency currency : currencyList) {
			if (currency.getId() == currencyId) {
				text.setText(currency.getSymbol());
				selectedCurrency = currency;
				setItems(currencyList, count, new ItemListener<Currency>() {
					@Override
					public void onItemSelected(Currency selectedItem) {
						text.setText(selectedItem.getSymbol());
						selectedCurrency = selectedItem;
					}
					@Override
					public String getDisplayText(Currency selectedItem) {
						return selectedItem.getCode();
					}
					@Override
					public Drawable getIcon(Currency selectedItem) {
						return null;
					}
				});
			}
			++count;
		}	
	}
	
	@Override
	public void setupUIForDirection() {}
	
	@Override
	public void setFieldName(String fieldName) {
		
	}
	@Override
	public String getFieldName() {
		return null;
	}
	@Override
	public void displayError(String error) {
	
	}
	/**
	 * @return the selectedCurrency
	 */
	public Currency getSelectedCurrency() {
		return selectedCurrency;
	}
	
	public double getPredefinedAmountForCurrencyId(long id){
		double predefinedAmount = 0;
		if(predefinedAmountsMap != null && !predefinedAmountsMap.isEmpty()){
			try {
				predefinedAmount = Double.valueOf(predefinedAmountsMap.get(id));
			} catch (Exception e) {
				Log.e(TAG, "Could NOT get the predefined amount for this id.", e);
			}
		}
		return predefinedAmount;
	}
} 