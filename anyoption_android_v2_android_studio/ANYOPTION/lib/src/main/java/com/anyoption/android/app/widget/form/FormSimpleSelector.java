package com.anyoption.android.app.widget.form;

import java.util.List;

import com.anyoption.android.app.Application.LayoutDirection;
import com.anyoption.android.R;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

/**
 * Simple string selector.
 * @author Anastas Arnaudov
 *
 */
public class FormSimpleSelector extends FormSelector<String> {

	protected View view;
	protected TextView text;
	
	public FormSimpleSelector(Context context) {
		super(context);
	}

	public FormSimpleSelector(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public FormSimpleSelector(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	@Override
	protected void setupUI() {
		if (view == null) {
			view = View.inflate(getContext(), R.layout.form_simple_selector, this);
			text = (TextView) view.findViewById(R.id.form_simple_selector_text);
		}

		if(!isInEditMode()){
			setupUIForDirection();			
		}else{
			text.setText("Selector");
		}
	}

	@Override
	public void setupUIForDirection() {
		if (getDirection() == LayoutDirection.LEFT_TO_RIGHT) {
			text.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		} else if (getDirection() == LayoutDirection.RIGHT_TO_LEFT) {
			text.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
		}
	}

	public void setItems(List<String> items){
		setItems(items, 0, new ItemListener<String>() {
			@Override
			public void onItemSelected(String selectedItem) {
				setValue(selectedItem);
			}
			@Override
			public String getDisplayText(String selectedItem) {
				return selectedItem;
			}
			@Override
			public Drawable getIcon(String selectedItem) {
				return null;
			}
		});
	}
	
	@Override
	public void setItems(List<String> items, int defaultSelectedItem, ItemListener<String> onItemSelectedListener) {
		super.setItems(items, defaultSelectedItem, onItemSelectedListener);
		refreshUI();
	}

	@Override
	public void setValue(Object value) {
		super.setValue(value);
		refreshUI();
	}
	
	@Override
	public void refreshUI() {
		super.refreshUI();
		text.setText(getValue());
	}

	@Override
	public void setFieldName(String fieldName) {}

	@Override
	public String getFieldName() {
		return null;
	}

	@Override
	public void displayError(String error) {}

}
