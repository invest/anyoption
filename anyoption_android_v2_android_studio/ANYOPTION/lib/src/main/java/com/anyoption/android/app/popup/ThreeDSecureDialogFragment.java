package com.anyoption.android.app.popup;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Anastas Arnaudov
 */
public class ThreeDSecureDialogFragment extends BaseDialogFragment{

    private static final String TAG = ThreeDSecureDialogFragment.class.getSimpleName();
    private WebView webView;
    private String threeDAcsUrl;
    private String threeDTermUrl;
    private String threeDMD;


    public static ThreeDSecureDialogFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new MarketingDialogFragment.");
        ThreeDSecureDialogFragment fragment = new ThreeDSecureDialogFragment();
        if (bundle == null) {
            bundle = new Bundle();
        }

        bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
        bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
        bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle != null){
            threeDAcsUrl = bundle.getString(Constants.EXTRA_THREE_D_ACS_URL);
            threeDTermUrl = bundle.getString(Constants.EXTRA_THREE_D_TERM_URL);
            threeDMD = bundle.getString(Constants.EXTRA_THREE_D_MD);
        }
    }

    @Override
    protected int getContentLayout() {
        return R.layout.three_d_secure_popup_layout;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreateContentView(View contentView) {
        //TODO
        if(threeDAcsUrl.trim().length() > 0 && threeDTermUrl.trim().length() > 0  && threeDMD.trim().length() > 0) {
            webView = (WebView) contentView.findViewById(R.id.three_d_secure_popup_web_view);
            webView.getSettings().setJavaScriptEnabled(true);
            if (Build.VERSION.SDK_INT <= 18) {
                webView.getSettings().setPluginState(WebSettings.PluginState.ON);
            }
            webView.setWebViewClient(new MyWebViewClient());

            String text = " <html> " +
                    "           <head> " +
                    "               <meta HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html;charset=UTF-8\" /> " +
                    "               <META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\" /> " +
                    "           </head> " +
                    "           <body onload=\"document.threeDSecure.submit();\" dir=\"ltr\"> " +
                    " <form method=\"POST\" style=\"display:none;\" id=\"threeDSecure\" name=\"threeDSecure\" action=\"" + threeDAcsUrl +"\"> " +
                    "   <input name=\"TermUrl\" id=\"TermUrl\" value=\"{termUrl}\"/> " +
                    "   <input name=\"MD\" id=\"MD\"  value=\"{mD}\"/> " +
                    " </form> " +
                    "           </body> " +
                    "       </html>";

            text = text.replace("{termUrl}", threeDTermUrl);
            text = text.replace("{mD}", threeDMD);

            webView.loadData(text, "text/html", "utf-8");
        } else {
            dismiss();
            goToTrade();
        }
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if(url.contains("anyoption.com") || url.contains("copyop.com") || url.contains("etrader.co.il")){
                dismiss();
                goToTrade();
                return true;
            }
            return false;
        }
    }

    private void goToTrade(){
        if(!getResources().getBoolean(R.bool.isTablet)) {
            application.postEvent(new NavigationEvent(Application.Screen.TRADE, NavigationEvent.NavigationType.INIT));
        } else {
            application.postEvent(new NavigationEvent(Application.Screen.TABLET_TRADE, NavigationEvent.NavigationType.INIT));
        }
    }

}
