package com.anyoption.android.app.util.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.anyoption.android.app.Application;
import com.anyoption.android.R;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;

/**
 * Utility class with validation form methods.
 * @author Anastas Arnaudov
 *
 */
public class FormValidation {

	///////////////////////////////////////////////////////////////////////
	/**
	 * Validator Type.
	 * @author Anastas Arnaudov
	 */
	public enum ValidatorType{
		
		/**
		 * Vacuous truth validation
		 */
		VACUOUS(Constants.EMPTY_STRING) {
		
		},

		/**
		 * Empty string.
		 */
		EMPTY(Application.get().getString(R.string.errorMandatoryField)){
			@Override
			public boolean validate(String text) {
				return FormValidation.validateEmpty(text);
			}
		},
		
		/**
		 * Email validation..
		 */
		EMAIL(Application.get().getString(R.string.errorInvalidEmail)){
			@Override
			public boolean validate(String text) {
				return FormValidation.validateEmail(text);
			}
		},
		
		/**
		 * English Letters and digits only validation.
		 */
		ALNUM(Application.get().getString(R.string.errorEnglishLettersAndNumbersOnly)){
			@Override
			public boolean validate(String text) {
				return FormValidation.validateEnglishLettersAndNumbersOnly(text);
			}
		},
		
		/**
		 * Letters only validation.
		 */
		LETTERS_ONLY(Application.get().getString(R.string.errorLettersOnly)){
			@Override
			public boolean validate(String text) {
				return FormValidation.validateLettersOnly(text);
			}
		},
		
		/**
		 * Letters only validation. "'" and " " are allowed too.
		 */
		LETTERS_AND_SPACE_ONLY(Application.get().getString(R.string.errorLettersOnly)){
			@Override
			public boolean validate(String text) {
				return FormValidation.validateLettersUnicodeOnly(text);
			}
		},
		
		/**
		 * English letters only validation.
		 */
		LETTERS_ENGLISH_ONLY(Application.get().getString(R.string.errorEnglishLettersOnly)){
			@Override
			public boolean validate(String text) {
				return FormValidation.validateEnglishLettersOnly(text);
			}
		},
		
		/**
		 * Digits only validation.
		 */
		DIGITS(Application.get().getString(R.string.errorDigitsOnly)){
			@Override
			public boolean validate(String text) {
				return FormValidation.validateDigitsOnly(text);
			}
		},
		
		/**
		 * Double validation.
		 */
		DOUBLE(Application.get().getString(R.string.errorDigitsOnly)){
			@Override
			public boolean validate(String text) {
				return FormValidation.validateDoubleOnly(text);
			}
		},
		
		/**
		 * Phone validation.
		 */
		PHONE(Application.get().getString(R.string.errorPhoneInvalid)){
			@Override
			public boolean validate(String text) {
				return FormValidation.validateDigitsOnly(text);
			}
		},
		
		/**
		 * Phone minimum characters validation.
		 */
		PHONE_MINIMUM_CHARACTERS(Application.get().getString(R.string.errorPhoneMinimumCharacters)){
			@Override
			public boolean validate(String text) {
				return FormValidation.validateCharactersMinimum(text,7);
			}
		},
		
		/**
		 * Israel phone number validation.
		 */
		PHONE_ISRAEL(Application.get().getString(R.string.errorPhoneIsrael)){
			@Override
			public boolean validate(String text) {
				return FormValidation.validatePhoneIsrael(text);
			}
		},
		
		USERNAME_MINIMUM_CHARACTERS(Application.get().getString(R.string.errorUsernameMinimumCharacters)){
			@Override
			public boolean validate(String text) {
				return FormValidation.validateCharactersMinimum(text,6);
			}
		},
		
		PASSWORD_MINIMUM_CHARACTERS(Application.get().getString(R.string.errorPasswordMinimumCharacters)) {
			@Override
			public boolean validate(String text) {
				return FormValidation.validateCharactersMinimum(text,6);
			}
		},
		
		ADDRESS(Application.get().getString(R.string.errorEnglishLettersAndNumbersOnly)) {
			@Override
			public boolean validate(String text) {
				return FormValidation.validateAddress(text);
			}
		},
		
		ADDRESS_NUM(Application.get().getString(R.string.errorEnglishLettersAndNumbersOnly)) {
			@Override
			public boolean validate(String text) {
				return FormValidation.validateAddressNum(text);
			}
		},		

		BIRTH_DAY(Application.get().getString(R.string.errorUnder18)) {
			@Override
			public boolean validate(String date) {
				return FormValidation.validateBirthDay(date);
			}
		},
		
		CC_PASS_MINIMUM_CHARACTERS(Application.get().getString(R.string.errorCVVLength)) {
			@Override
			public boolean validate(String text) {
				return FormValidation.validateCharactersMinimum(text,3);
			}
		},
		
		CC_EXP_DATE(Application.get().getString(R.string.errorExpireDateInThePast)) {
			@Override
			public boolean validate(String date) {
				return FormValidation.validateExpDate(date);
			}
		},

		CURRENCY_AMOUNT(Application.get().getString(R.string.errorInvalidNumber)) {
			@Override
			public boolean validate(String text) {
				return FormValidation.validateCurrencyAmount(text);
			}
		},
		
		LETTERS_ENGLISH_AND_SPACE(Application.get().getString(R.string.errorEnglishLettersOnly)) {
			@Override
			public boolean validate(String text) {
				return FormValidation.validateEnglishLettersAndSpace(text);
			}
		},

		//TODO move to etrader project
		LETTERS_HEBREW(Application.get().getString(R.string.errorHebrewLettersOnly)) {
			@Override
			public boolean validate(String text) {
				return FormValidation.validateHebrewLetters(text);
			}
		},

		//TODO move to etrader project
		ID_NUMBER(Application.get().getString(R.string.errorInvalidIdNumber)) {
			@Override
			public boolean validate(String text) {
				return FormValidation.validateIdNumber(text);
			}
		},

		//TODO move to copyop project
		NICKNAME(Application.get().getString(R.string.errorNickname)) {
			@Override
			public boolean validate(String text) {
				return FormValidation.validateNickname(text);
			}
		},
		
		LONG(Application.get().getString(R.string.errorLong)) {
			@Override
			public boolean validate(String text) {
				return FormValidation.validateLong(text);
			}
		},
		
		ZIP(Application.get().getString(R.string.error)) {
			@Override
			public boolean validate(String text) {
				return FormValidation.validateZip(text);
			}
		}
		
		;

		private String errorMessage;
		
		ValidatorType(String errorMessage){
			this.setErrorMessage(errorMessage);
		}

		public String getErrorMessage() {
			return errorMessage;
		}

		public void setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
		}

		public boolean validate(String text){
			return true;
		}
		
	}
	///////////////////////////////////////////////////////////
	
	/**
	 * Validates whether the text is empty string.
	 * @param text
	 * @return
	 */
	public static boolean validateEmpty(String text){
		if(text.length() == 0 || text.trim().length() == 0){
			return false;
		}
		return true;
	}
	

	/**
	 * Validates whether the text is a valid email address.
	 * @param text
	 * @return
	 */
	public static boolean validateEmail(String text){
		String pattern = Application.get().getString(R.string.patternEmail);			
		if (!text.matches(pattern)) {
			return false;						
		}
		return true;
	}
	
	/**
	 * Validates whether the text contains only letters and/or numbers.
	 * @param text
	 * @return
	 */
	public static boolean validateEnglishLettersAndNumbersOnly(String text) {		
		String pattern = Application.get().getString(R.string.patternEnglishLettersAndDigits);
		if (text.matches(pattern)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Validates whether the text contains only letters.
	 * @param text
	 * @return
	 */
	public static boolean validateLettersOnly(String text) {		
		String pattern = Application.get().getString(R.string.patternLettersOnly);
		if (text.matches(pattern)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Validates whether the text contains only letters.
	 * @param text
	 * @return
	 */
	public static boolean validateLettersUnicodeOnly(String text) {		
		String pattern = Application.get().getString(R.string.patternLettersUnicodeOnly);
		if (text.matches(pattern)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Validates whether the text contains only english letters.
	 * @param text
	 * @return
	 */
	public static boolean validateEnglishLettersOnly(String text) {		
		String pattern = Application.get().getString(R.string.patternEnglishLetters);
		if (text.matches(pattern)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Validates whether the text contains only english letters and space intervals.
	 * @param text
	 * @return
	 */
	public static boolean validateEnglishLettersAndSpace(String text) {		
		String pattern = Application.get().getString(R.string.patternEnglishLettersAndSpaces);
		if (text.matches(pattern)) {
			return true;
		}
		return false;
	}

	/**
	 * Validates whether the text contains only hebrew letters and space intervals.
	 * 
	 * @param text
	 * @return
	 */
	public static boolean validateHebrewLetters(String text) {
		String pattern = Application.get().getString(R.string.patternHebrewLettersOnly);
		if (text.matches(pattern)) {
			return true;
		}

		return false;
	}

	/**
	 * Validates whether the text contains only letters.
	 * @param text
	 * @return
	 */
	public static boolean validateDigitsOnly(String text) {		
		String pattern = Application.get().getString(R.string.patternDigits);
		if (text.matches(pattern)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Validates whether the text is a real currency amount (double with max 2 digits after the decimal point).
	 * @param text
	 * @return
	 */
	public static boolean validateCurrencyAmount(String text) {		
		String pattern = Application.get().getString(R.string.patternCurrencyNumber);
		if (text.matches(pattern)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Validates whether the text is double. 
	 * @param text
	 * @return
	 */
	protected static boolean validateDoubleOnly(String text) {
		String pattern = Application.get().getString(R.string.patternDouble);
		if (text.matches(pattern)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Validates whether the text is shorter than the given number of characters.
	 * @param text
	 * @return
	 */
	public static boolean validateCharactersMinimum(String text, int numberOfCharacters){			
		if (text.length() < numberOfCharacters) {
			return false;						
		}
		return true;
	}
	
	/**
	 * Validates the address
	 * @param text
	 * @return
	 */
	public static boolean validateAddress(String text) {
		String pattern = Application.get().getString(R.string.patternStreet);
		if (text.matches(pattern)) {
			return true;
		}
		return false;
	}

	/**
	 * Validates the address number
	 * @param text
	 * @return
	 */
	public static boolean validateAddressNum(String text) {
		String pattern = Application.get().getString(R.string.patternStreetNumber);
		if (text.matches(pattern)) {
			return true;
		}
		return false;
	}

	
	/**
	 * Validate birthday of user - whether is younger then 18 
	 * @param date
	 * @return
	 */
	public static boolean validateBirthDay(String dateStr) {
		Date date = null;
		try {
			if (dateStr == null || dateStr.equalsIgnoreCase("")) { // in case user didn't fill date of birth
				return true;
			}
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			date = sdf.parse(dateStr);
		} catch (ParseException e) {			
			return false;
		}
    	//Check if under 18
    	GregorianCalendar gc = new GregorianCalendar();
    	gc.setTime(date);    	       
    	gc.add(Calendar.YEAR, 18);
        if (gc.after(Calendar.getInstance())) {
			return false;						
		}
		return true;
	}
	
	/**
	 * Validates CC expire date - whether the date is in future time.
	 * @param text
	 * @return
	 */
	public static boolean validateExpDate(String dateStr){			
		Date date = null;
		try {
			if (!Utils.isParameterEmptyOrNull(dateStr)) {
				SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
				date = sdf.parse(dateStr);
		    	GregorianCalendar gc = new GregorianCalendar();
		    	gc.setTime(date);    	       
		        if (gc.after(Calendar.getInstance())) {
					return true;						
				}
			}
		} catch (ParseException e) {			
			return false;
		}
		return false;
	}

	/**
	 * Validates ID number. This number is used only for Israel users(ET).
	 * 
	 * @param idNumber
	 * @return
	 */
	public static boolean validateIdNumber(String idNumber) {
		if (Utils.isParameterEmptyOrNull(idNumber)) {
			return false;
		}

		try {
			long id = Long.parseLong(idNumber);
			if (id < 1000) {
				return false;
			}
		} catch (NumberFormatException e) {
			return false;
		}

		if (idNumber.length() < 9) {
			return false;
		}

		int idNum1 = Integer.parseInt(idNumber.substring(0, 1)) * 1;
		int idNum2 = Integer.parseInt(idNumber.substring(1, 2)) * 2;
		int idNum3 = Integer.parseInt(idNumber.substring(2, 3)) * 1;
		int idNum4 = Integer.parseInt(idNumber.substring(3, 4)) * 2;
		int idNum5 = Integer.parseInt(idNumber.substring(4, 5)) * 1;
		int idNum6 = Integer.parseInt(idNumber.substring(5, 6)) * 2;
		int idNum7 = Integer.parseInt(idNumber.substring(6, 7)) * 1;
		int idNum8 = Integer.parseInt(idNumber.substring(7, 8)) * 2;
		int idNum9 = Integer.parseInt(idNumber.substring(8, 9)) * 1;

		if (idNum1 > 9) {
			idNum1 = (idNum1 % 10) + 1;
		}
		if (idNum2 > 9) {
			idNum2 = (idNum2 % 10) + 1;
		}
		if (idNum3 > 9) {
			idNum3 = (idNum3 % 10) + 1;
		}
		if (idNum4 > 9) {
			idNum4 = (idNum4 % 10) + 1;
		}
		if (idNum5 > 9) {
			idNum5 = (idNum5 % 10) + 1;
		}
		if (idNum6 > 9) {
			idNum6 = (idNum6 % 10) + 1;
		}
		if (idNum7 > 9) {
			idNum7 = (idNum7 % 10) + 1;
		}
		if (idNum8 > 9) {
			idNum8 = (idNum8 % 10) + 1;
		}
		if (idNum9 > 9) {
			idNum9 = (idNum9 % 10) + 1;
		}

		int sumval = idNum1 + idNum2 + idNum3 + idNum4 + idNum5 + idNum6 + idNum7 + idNum8 + idNum9;

		sumval = sumval % 10;

		if (sumval > 0) {
			return false;
		}

		return true;
	}

	public static boolean validateNickname(String text) {
		String pattern = Application.get().getString(R.string.patternNickname);
		if (text.matches(pattern)) {
			return true;
		}

		return false;
	}
	
	public static boolean validateLong(String text) {		
		try {
			Long.valueOf(text);
			return true;
		} catch (Exception e) {
			return false;
		}
		
	}
	
	public static boolean validatePhoneIsrael(String text) {		
		boolean isDigits = validateDigitsOnly(text);
		int numberOfDigits = text.length();
		return isDigits && numberOfDigits == 10;
	}
	
	public static boolean validateZip(String text) {		
		String pattern = Application.get().getString(R.string.patternZip);
		if (text.matches(pattern)) {
			return true;
		}
		return false;
	}
	
	
}

