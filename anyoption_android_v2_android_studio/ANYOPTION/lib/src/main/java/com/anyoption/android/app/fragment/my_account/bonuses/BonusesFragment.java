package com.anyoption.android.app.fragment.my_account.bonuses;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.json.requests.BonusMethodRequest;
import com.anyoption.json.results.BonusMethodResult;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Custom {@link NavigationalFragment} that displays list of user bonuses.
 * 
 * @author Anastas Arnaudov
 *
 */
public class BonusesFragment extends NavigationalFragment {

	public static final String TAG = BonusesFragment.class.getSimpleName();
	public static final int PAGE_SIZE = 15;

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static BonusesFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new BonusesFragment.");
		BonusesFragment bonusesFragment = new BonusesFragment();
		bonusesFragment.setArguments(bundle);
		return bonusesFragment;
	}

	protected ListView listView;
	protected ListViewAdapter listAdapter;
	private TextView emptyListTextView;
	protected List<BonusUsers> bonusesList = new ArrayList<BonusUsers>();
	private boolean hasBonuses = true;
	private int startRow = 0;
	private ProgressBar loading;
	private BonusMethodResult bonusResult;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = getArguments();
		if (bundle != null) {
			// Get arguments passed in the bundle
		}

		listAdapter = createAdapter();

		getBonuses();
	}

	protected ListViewAdapter createAdapter() {
		return new ListViewAdapter(application, bonusesList);
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (!hidden) {
			hasBonuses = true;
			startRow = 0;
			bonusesList.clear();
			getBonuses();
		}
	}

	/**
	 * Get the user bonuses from the server.
	 */
	private void getBonuses() {
		showHideLoading(true);
		BonusMethodRequest request = new BonusMethodRequest();
		request.setBonusStateId(0);
		request.setStartRow(startRow);
		request.setPageSize(PAGE_SIZE);
		application.getCommunicationManager().requestService(this, "gotBonuses", Constants.SERVICE_GET_BONUSES, request,
				BonusMethodResult.class);
	}

	/**
	 * Callback method for the "getBonuses" request.
	 * 
	 * @param result
	 */
	public void gotBonuses(Object result) {
		showHideLoading(false);
		hasBonuses = false;
		if (result != null) {
			bonusResult = (BonusMethodResult) result;
			if (bonusResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {

				bonusesList.addAll(Arrays.asList(bonusResult.getBonuses()));
				if (bonusesList.size() > 0) {
					hasBonuses = true;
				}
				// Refresh list:
				listAdapter.setItems(bonusesList);
				listAdapter.notifyDataSetChanged();
				startRow += PAGE_SIZE;
			}
		}

		if (listView != null && emptyListTextView != null) {
			if (hasBonuses) {
				listView.setVisibility(View.VISIBLE);
				emptyListTextView.setVisibility(View.GONE);
			} else {
				listView.setVisibility(View.GONE);
				emptyListTextView.setVisibility(View.VISIBLE);
			}
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.bonuses_layout, container, false);

		listView = (ListView) rootView.findViewById(R.id.bonuses_list_view);
		listView.setAdapter(listAdapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (RegulationUtils.checkRegulationStatusAndDoNecessary(false)) {
					// Go to Bonus Details screen:
					Bundle bundle = new Bundle();
					BonusUsers bonus = listAdapter.getItem(position);
					bundle.putSerializable(Constants.EXTRA_BONUS, bonus);
					bundle.putStringArray(Constants.EXTRA_BONUS_FACTOR, bonusResult.getTurnoverFactor());
					application.postEvent(new NavigationEvent(Screen.BONUS_DETAILS, NavigationType.DEEP, bundle));
				}
			}
		});
		listView.setOnScrollListener(new OnScrollListener() {

			private int currentLastVisibleItem;
			private int currentVisibleItemCount;
			private int currentScrollState;

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				this.currentScrollState = scrollState;
				isScrollCompleted();
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				this.currentLastVisibleItem = firstVisibleItem + visibleItemCount;
				this.currentVisibleItemCount = visibleItemCount;

			}

			private void isScrollCompleted() {
				if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
					// Detect if there's been a scroll which has completed.
					// And check if the list is scrolled to the bottom.
					if (currentLastVisibleItem == listAdapter.getCount()) {
						getBonuses();
					}
				}
			}

		});

		emptyListTextView = (TextView) rootView.findViewById(R.id.bonuses_empty_text_view);

		if (!hasBonuses) {
			listView.setVisibility(View.GONE);
			emptyListTextView.setVisibility(View.VISIBLE);
		} else {
			listView.setVisibility(View.VISIBLE);
			emptyListTextView.setVisibility(View.GONE);
		}

		loading = (ProgressBar) rootView.findViewById(R.id.bonuses_loading);

		return rootView;
	}

	private void showHideLoading(boolean isVisible) {
		if (loading != null) {
			if (isVisible) {
				loading.setVisibility(View.VISIBLE);
			} else {
				loading.setVisibility(View.INVISIBLE);
			}
		}
	}

	// ##################################################################################################
	// ######### ADAPTER for the List View ################
	// ##################################################################################################

	public class BonusItemHolder {
		public int position;
		public View separator;
		public TextView descriptionTextView;
		public TextView dateTextView;
		public TextView stateTextView;
	}

	/**
	 * Custom list adapter with BonusUsers objects.
	 * 
	 * @author Anastas Arnaudov
	 *
	 */
	public class ListViewAdapter extends ArrayAdapter<BonusUsers> {

		private List<BonusUsers> items;

		public ListViewAdapter(Context context, List<BonusUsers> items) {
			super(context, R.layout.bonus_list_item_layout, items);
			this.items = items;
		}

		protected BonusItemHolder createItemHolder() {
			return new BonusItemHolder();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			BonusUsers bonus = getItem(position);

			BonusItemHolder bonusItemHolder;
			if (convertView == null) {
				bonusItemHolder = createItemHolder();
				LayoutInflater inflater = (LayoutInflater) getContext()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.bonus_list_item_layout, parent, false);

				bonusItemHolder.separator = convertView.findViewById(R.id.bonus_list_item_separator_top);
				bonusItemHolder.descriptionTextView = (TextView) convertView
						.findViewById(R.id.bonus_list_item_description);
				bonusItemHolder.dateTextView = (TextView) convertView.findViewById(R.id.bonus_list_item_date);
				bonusItemHolder.stateTextView = (TextView) convertView.findViewById(R.id.bonus_list_item_state);

				convertView.setTag(bonusItemHolder);
			} else {
				bonusItemHolder = (BonusItemHolder) convertView.getTag();
			}
			bonusItemHolder.position = position;

			bonusItemHolder.descriptionTextView.setText(bonus.getBonusDescriptionTxt());
			bonusItemHolder.dateTextView.setText(bonus.getEndDateTxt());
			bonusItemHolder.stateTextView.setText(bonus.getBonusStateTxt().toUpperCase(application.getLocale()));

			long stateID = bonus.getBonusStateId();
			int backgroundColor;
			int separatorColor;
			int stateColor;
			int textColor;

			if (stateID == Constants.BONUS_STATE_GRANTED || stateID == Constants.BONUS_STATE_ACTIVE
					|| stateID == Constants.BONUS_STATE_PANDING) {
				backgroundColor = ColorUtils.getColor(R.color.bonuses_list_item_active_bg);
				textColor = ColorUtils.getColor(R.color.bonuses_list_item_active_text);
				stateColor = ColorUtils.getColor(R.color.bonuses_list_item_active_state_text);
				separatorColor = ColorUtils.getColor(R.color.bonuses_list_item_active_separator);
			} else if (stateID == Constants.BONUS_STATE_USED || stateID == Constants.BONUS_STATE_DONE) {
				backgroundColor = ColorUtils.getColor(R.color.bonuses_list_item_done_bg);
				textColor = ColorUtils.getColor(R.color.bonuses_list_item_done_text);
				stateColor = ColorUtils.getColor(R.color.bonuses_list_item_done_state_text);
				separatorColor = ColorUtils.getColor(R.color.bonuses_list_item_done_separator);
			} else {
				backgroundColor = ColorUtils.getColor(R.color.bonuses_list_item_canceled_bg);
				textColor = ColorUtils.getColor(R.color.bonuses_list_item_canceled_text);
				stateColor = ColorUtils.getColor(R.color.bonuses_list_item_canceled_state_text);
				separatorColor = ColorUtils.getColor(R.color.bonuses_list_item_canceled_separator);
			}

			convertView.setBackgroundColor(backgroundColor);
			bonusItemHolder.descriptionTextView.setTextColor(textColor);
			bonusItemHolder.dateTextView.setTextColor(textColor);
			bonusItemHolder.separator.setBackgroundColor(separatorColor);
			bonusItemHolder.stateTextView.setTextColor(stateColor);

			return convertView;
		}

		@Override
		public BonusUsers getItem(int position) {
			return this.items.get(position);
		}

		@Override
		public int getCount() {
			return this.items.size();
		}

		public void setItems(List<BonusUsers> items) {
			this.items = items;
		}

	}
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	@Override
	protected Screenable setupScreen() {
		return Screen.BONUSES;
	}

}
