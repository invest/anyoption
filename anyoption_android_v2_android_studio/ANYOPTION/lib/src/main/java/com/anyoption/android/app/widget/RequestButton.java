package com.anyoption.android.app.widget;

import com.anyoption.android.R;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.FontsUtils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ViewSwitcher;

/**
 * Custom {@link Button} that when clicked performs special animation while the request is made and reacts with another animation according to the response.
 * @author Anastas Arnaudov
 *
 */
public class RequestButton extends ViewSwitcher {

	public static long DURATION_OUT_ANIMATION = 300l;
	public static long DURATION_IN_ANIMATION = 300l;
	public static long DELAY_IN_ANIMATION = 200l;

	public static int STATE_NORMAL = 1;
	public static int STATE_LOADING = 2;
	public static int STATE_STOPING_LOADING = 3;

	private static int NO_FONT_ID = -1;

	/**
	 * Listener for the loading animation.
	 * @author Anastas Arnaudov
	 *
	 */
	public interface InitAnimationListener{
		/**
		 * Called when the loading animation has finished.
		 */
		public void onAnimationFinished();
	}
	
	
	private int state = STATE_NORMAL;
	private ViewSwitcher viewSwitcher;
	private String text;
	protected int textSize;
	private int font;
	private InitAnimationListener animationListener;
	private Drawable background;
	protected TextView textView;
	
	public RequestButton(Context context) {
		super(context);
		init(context, null);
	}

	public RequestButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs);
	}

	private void init(Context context, AttributeSet attrs){
		View rootView = View.inflate(context, R.layout.request_button_layout, this);
		viewSwitcher = (ViewSwitcher) rootView.findViewById(R.id.request_button_viewSwitcher);
		textView = (TextView) rootView.findViewById(R.id.request_button_textview);
		
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RequestButton);
		try {
			text = a.getString(R.styleable.RequestButton_request_button_text);
			textSize = a.getInt(R.styleable.RequestButton_request_button_text_size, 18);
			font = a.getInt(R.styleable.RequestButton_font, NO_FONT_ID);
		} finally {
			a.recycle();
		}
		background = getBackground();
		
		textView.setText(text);
		textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
		if (font != NO_FONT_ID) {
			textView.setFont(FontsUtils.getFontForFontID(font));
		}

		if(!isInEditMode()){
			initAnimations(context);
		}
		
	}

	protected void initAnimations(Context context){
		Animation outAnimation = AnimationUtils.loadAnimation(context, R.anim.slide_out_fade_out_left);
		outAnimation.setDuration(DURATION_OUT_ANIMATION);
		outAnimation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation paramAnimation) {}
			@Override
			public void onAnimationRepeat(Animation paramAnimation) {}
			@Override
			public void onAnimationEnd(Animation paramAnimation) {
				if(state == STATE_LOADING){
					setupLoadingBG();
				}else{
					setupNormalBG();
				}
			}
		});
		
		Animation inAnimation = AnimationUtils.loadAnimation(context, R.anim.slide_in_fade_in_right);
		inAnimation.setDuration(DURATION_IN_ANIMATION);
		inAnimation.setStartOffset(DELAY_IN_ANIMATION);
		inAnimation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation paramAnimation) {}
			@Override
			public void onAnimationRepeat(Animation paramAnimation) {}
			@Override
			public void onAnimationEnd(Animation paramAnimation) {
				if(state == STATE_LOADING  && animationListener != null){
					animationListener.onAnimationFinished();						
				}else if(state == STATE_STOPING_LOADING){
					state = STATE_NORMAL;
					setEnabled(true);
					setClickable(true);
					onAnimationFinished();
				}
			}
		});
		
		viewSwitcher.setInAnimation(inAnimation);
		viewSwitcher.setOutAnimation(outAnimation);	
	}
	
	public void startLoading(InitAnimationListener animationListener){
		setAnimationListener(animationListener);
		startLoading();
	}
	
	public void startLoading(){
		if(state == STATE_NORMAL){
			state = STATE_LOADING;
			setEnabled(false);
			setClickable(false);
			viewSwitcher.showNext();			
		}
	}
	
	public void stopLoading(){
		if(state == STATE_LOADING){
			state = STATE_STOPING_LOADING;
			viewSwitcher.showNext();				
		}
	}

	protected void setupLoadingBG(){
		DrawableUtils.setBackground(RequestButton.this, DrawableUtils.getDrawable(R.drawable.button_grey));
	}
	
	protected void setupNormalBG(){
		DrawableUtils.setBackground(RequestButton.this, background);
	}
	
	protected void setupDisableBG(){
		DrawableUtils.setBackground(RequestButton.this, DrawableUtils.getDrawable(R.drawable.button_grey));
	}
	
	protected void onAnimationFinished(){
		
	}
	
	public InitAnimationListener getAnimationListener() {
		return animationListener;
	}

	public void setAnimationListener(InitAnimationListener animationListener) {
		this.animationListener = animationListener;
	}

	public void setText(int resID) {
		if(textView != null){
			textView.setText(resID);
		}
	}
	
	public void setText(String text) {
		if(textView != null){
			textView.setText(text);
		}
	}
	
	public void disable(){
		setupDisableBG();
		setEnabled(false);
	}
	
	public void enable(){
		setupNormalBG();
		setEnabled(true);
	}
	
}
