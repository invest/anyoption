package com.anyoption.android.app.popup;

import java.util.ArrayList;
import java.util.List;

import com.anyoption.android.R;
import com.anyoption.android.app.model.Answer;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class QuestionnaireAnswersDialogFragment extends BaseDialogFragment implements OnItemClickListener {

	public static final String TAG = QuestionnaireAnswersDialogFragment.class.getSimpleName();

	public interface OnItemSelectedListener{
		void onItemSelected(Answer item, int position);
	}
	
	private OnItemSelectedListener onItemSelectedListener;
	private List<Answer> items = new ArrayList<Answer>();
	private ListView listView;
	private ListViewAdapter adapter;
	
	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static QuestionnaireAnswersDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new QuestionnaireAnswersDialogFragment.");
		QuestionnaireAnswersDialogFragment fragment = new QuestionnaireAnswersDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	protected int getContentLayout() {
		return R.layout.questionnaire_answers_popup;
	}

	@Override
	protected void onCreateContentView(View contentView) {
		listView = (ListView) contentView.findViewById(R.id.questionnaire_answers_popup_list_view);
		adapter = new ListViewAdapter();
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
	}

	public void setOnItemSelectedListener(OnItemSelectedListener onItemSelectedListener){
		this.onItemSelectedListener = onItemSelectedListener;
	}
	
	public void setItems(List<Answer> items){
		this.items = items;
		if(adapter != null){
			adapter.notifyDataSetChanged();
		}
	}

	private class ListViewItemHolder {
		public TextView textView;
	}

    private class ListViewAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
        	Answer item = items.get(position);
            ListViewItemHolder holder;
            if (convertView == null) {
            	LayoutInflater inf = (LayoutInflater) application.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inf.inflate(R.layout.questionnaire_answers_popup_item, parent, false);
                holder = new ListViewItemHolder();
                holder.textView = (TextView) convertView.findViewById(R.id.questionnaire_answers_popup_item_text_view);

                convertView.setTag(holder);
            }else{
                holder = (ListViewItemHolder) convertView.getTag();
            }

            holder.textView.setText(item.getDescription());

            return convertView;
        }
    }

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if(onItemSelectedListener != null){
			onItemSelectedListener.onItemSelected(items.get(position), position);
			dismiss();
		}
	}

}
