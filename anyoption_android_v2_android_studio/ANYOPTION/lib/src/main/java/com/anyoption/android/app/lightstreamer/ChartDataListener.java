package com.anyoption.android.app.lightstreamer;

import com.anyoption.common.beans.base.Investment;
import com.anyoption.json.results.ChartDataMethodResult;

public interface ChartDataListener extends LightstreamerListener {

	public void setChartData(ChartDataMethodResult data);

	public void addInvestment(Investment inv);

	public void removeInvestment(long invId);

	public Investment getFocusedInvestment();

	public void setChartDataPreviousHour(ChartDataMethodResult data);

	public void requestInvestmentFocus(Investment inv);
}