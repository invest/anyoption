package com.anyoption.android.app.model;

import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Investment;

public class InsuranceItem {

	private long investmentId;
	private long insuranceCloseTime;

	/**
	 * One of {@link Investment#INVESTMENT_TYPE_CALL} and {@link Investment#INVESTMENT_TYPE_PUT}
	 */
	private int investmentType;

	private long marketId;
	private String marketName;

	private long investmentTime;
	private String investmentTimeFormatted;
	private String investmentLevel;
	private double investmentAmount;
	private String investmentAmountFormatted;
	private double premiumPrice;
	private String premiumPriceFormatted;
	private String currentLevel;
	private String pType;
	private long opportunityCloseTime;

	public InsuranceItem(Currency currency, long invId, long insuranceCloseTime, int invType,
						 long marketId, String marketName, long invTime, String invTimeFormatted,
						 String invLevel, double invAmount, double premiumPrice, String currentLevel, String pType, long opportunityCloseTime) {
		this.investmentId = invId;
		this.insuranceCloseTime = insuranceCloseTime;
		this.investmentType = invType;
		this.marketId = marketId;
		this.marketName = marketName;
		this.investmentTime = invTime;
		this.investmentTimeFormatted = invTimeFormatted;
		this.investmentLevel = invLevel;
		this.investmentAmount = invAmount;
		this.premiumPrice = premiumPrice;
		this.currentLevel = currentLevel;
		this.pType = pType;
		this.opportunityCloseTime = opportunityCloseTime;

		investmentAmountFormatted = AmountUtil.getFormattedAmount(invAmount, currency);
		premiumPriceFormatted = AmountUtil.getFormattedAmount(premiumPrice, currency);
	}

	public long getInvestmentId() {
		return investmentId;
	}

	public void setInvestmentId(long investmentId) {
		this.investmentId = investmentId;
	}


	public long getInsuranceCloseTime() {
		return insuranceCloseTime;
	}


	public void setInsuranceCloseTime(long insuranceCloseTime) {
		this.insuranceCloseTime = insuranceCloseTime;
	}

	/**
	 * Returns one of {@link Investment#INVESTMENT_TYPE_CALL} and
	 * {@link Investment#INVESTMENT_TYPE_PUT}
	 */
	public int getInvestmentType() {
		return investmentType;
	}

	/**
	 * Sets the investment type
	 *
	 * @param investmentType
	 *            must be one of {@link Investment#INVESTMENT_TYPE_CALL} and
	 *            {@link Investment#INVESTMENT_TYPE_PUT}
	 */
	public void setInvestmentType(int investmentType) {
		this.investmentType = investmentType;
	}

	public long getMarketId() {
		return marketId;
	}

	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	public String getMarketName() {
		return marketName;
	}

	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}

	public long getInvestmentTime() {
		return investmentTime;
	}

	public void setInvestmentTime(long investmentTime) {
		this.investmentTime = investmentTime;
	}

	public String getInvestmentTimeFormatted() {
		return investmentTimeFormatted;
	}

	public void setInvestmentTimeFormatted(String investmentTimeFormatted) {
		this.investmentTimeFormatted = investmentTimeFormatted;
	}

	public String getInvestmentLevel() {
		return investmentLevel;
	}

	public void setInvestmentLevel(String investmentLevel) {
		this.investmentLevel = investmentLevel;
	}

	public double getInvestmentAmount() {
		return investmentAmount;
	}

	public void setInvestmentAmount(double investmentAmount) {
		this.investmentAmount = investmentAmount;
	}

	public String getInvestmentAmountFormatted() {
		return investmentAmountFormatted;
	}

	public void setInvestmentAmountFormatted(String investmentAmountFormatted) {
		this.investmentAmountFormatted = investmentAmountFormatted;
	}

	public double getPremiumPrice() {
		return premiumPrice;
	}

	public void setPremiumPrice(double premiumPrice) {
		this.premiumPrice = premiumPrice;
	}

	public String getPremiumPriceFormatted() {
		return premiumPriceFormatted;
	}

	public void setPremiumPriceFormatted(String premiumPriceFormatted) {
		this.premiumPriceFormatted = premiumPriceFormatted;
	}

	public String getCurrentLevel() {
		return currentLevel;
	}

	public void setCurrentLevel(String currentLevel) {
		this.currentLevel = currentLevel;
	}

	public String getpType() {
		return pType;
	}

	public void setpType(String pType) {
		this.pType = pType;
	}

	public long getOpportunityCloseTime() {
		return opportunityCloseTime;
	}

	public void setOpportunityCloseTime(long opportunityCloseTime) {
		this.opportunityCloseTime = opportunityCloseTime;
	}
}