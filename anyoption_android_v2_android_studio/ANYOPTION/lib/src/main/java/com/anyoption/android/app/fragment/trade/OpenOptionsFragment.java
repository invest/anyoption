package com.anyoption.android.app.fragment.trade;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.event.ExpiredOptionsEvent;
import com.anyoption.android.app.event.InsuranceBoughtEvent;
import com.anyoption.android.app.event.InsuranceEvent;
import com.anyoption.android.app.event.InsuranceEvent.InsuranceType;
import com.anyoption.android.app.event.InvestmentEvent;
import com.anyoption.android.app.event.InvestmentEvent.Type;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.lightstreamer.LightstreamerConnectionHandler;
import com.anyoption.android.app.lightstreamer.LightstreamerListener;
import com.anyoption.android.app.manager.CommunicationManager;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.anyoption.android.app.model.InsuranceItem;
import com.anyoption.android.app.popup.InsuranceDialogFragment;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.InvestmentUtil;
import com.anyoption.android.app.util.MarketsUtils;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.adapter.TradingOptionsListAdapter;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.service.requests.InvestmentsMethodRequest;
import com.anyoption.common.service.results.InvestmentsMethodResult;
import com.lightstreamer.ls_client.UpdateInfo;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class OpenOptionsFragment extends NavigationalFragment implements
		LightstreamerConnectionHandler.StatusListener, LightstreamerListener, Callback {

	@SuppressWarnings("hiding")
	private static final String TAG = OpenOptionsFragment.class.getSimpleName();

	private static final long TIMER_UPDATE_INTERVAL_MS = 1000;

	protected static final int MESSAGE_UPDATE_INVESTMENTS = 1;

	protected View root;
	private ImageView dollarDownView;
	private TextView investedAmountView;
	private TextView profitAmountView;
	private View profitableImage;
	private View noOptionsView;
	private View progressView;

	protected View takeProfitButton;
	private TextView takeProfitLabel;
	private ImageView takeProfitIcon;
	protected TextView takeProfitTimeView;

	protected View rollForwardButton;
	private TextView rollForwardLabel;
	private ImageView rollForwardIcon;
	protected TextView rollForwardTimeView;

	protected List<Investment> investments;
	private boolean areOptionsLoading;

	protected ListView optionsListView;
	protected TradingOptionsListAdapter optionsListAdapter;

	private SkinGroup skinGroup;
	protected Currency currency;

	protected ArrayList<ArrayList<InsuranceItem>> takeProfitGroups;
	protected ArrayList<ArrayList<InsuranceItem>> rollForwardGroups;

	private double totalInvestedAmount;
	private double totalExpectedProfit;

	private CountDownTimer takeProfitTimer;
	private CountDownTimer rollForwardTimer;

	private boolean isTablet;

	protected Handler handler;

	public static OpenOptionsFragment newInstance(Bundle bundle) {
		OpenOptionsFragment fragment = new OpenOptionsFragment();
		fragment.setArguments(bundle);
		return fragment;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		handler = new Handler(Looper.getMainLooper(), this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		isTablet = getResources().getBoolean(R.bool.isTablet);

		takeProfitGroups = application.getTakeProfitGroups();
		rollForwardGroups = application.getRollForwardGroups();
		
		root = inflater.inflate(R.layout.trading_open_options_fragment, container, false);
		dollarDownView = (ImageView) root.findViewById(R.id.trading_open_options_dollar_down);
		investedAmountView = (TextView) root.findViewById(R.id.trading_open_options_sum_invested);
		profitAmountView = (TextView) root.findViewById(R.id.trading_open_options_sum_profit);
		profitableImage = root.findViewById(R.id.trading_open_options_main_profitable_image);
		noOptionsView = root.findViewById(R.id.trading_open_options_no_options);
		progressView = root.findViewById(R.id.trading_open_options_progress);

		takeProfitButton = root.findViewById(R.id.trading_open_options_take_profit_button);
		takeProfitButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Bundle arguments = new Bundle();
				arguments.putInt(InsuranceDialogFragment.ARG_INSURANCE_TYPE_KEY,
						Investment.INSURANCE_TAKE_PROFIT);
				application.getFragmentManager().showDialogFragment(InsuranceDialogFragment.class,
						arguments);
			}
		});
		takeProfitButton.setClickable(false);

		takeProfitLabel = (TextView) root
				.findViewById(R.id.trading_open_options_take_profit_button_label);
		takeProfitIcon = (ImageView) root
				.findViewById(R.id.trading_open_options_take_profit_button_icon);
		takeProfitTimeView = (TextView) root
				.findViewById(R.id.trading_open_options_take_profit_button_time);

		rollForwardButton = root.findViewById(R.id.trading_open_options_roll_forward_button);
		rollForwardButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Bundle arguments = new Bundle();
				arguments.putInt(InsuranceDialogFragment.ARG_INSURANCE_TYPE_KEY,
						Investment.INSURANCE_ROLL_FORWARD);
				application.getFragmentManager().showDialogFragment(InsuranceDialogFragment.class,
						arguments);
			}

		});
		rollForwardButton.setClickable(false);

		rollForwardLabel = (TextView) root
				.findViewById(R.id.trading_open_options_roll_forward_button_label);
		rollForwardIcon = (ImageView) root
				.findViewById(R.id.trading_open_options_roll_forward_button_icon);
		rollForwardTimeView = (TextView) root
				.findViewById(R.id.trading_open_options_roll_forward_button_time);

		investments = new ArrayList<Investment>();

		setupListView();

		setUpUserDependentFields();
		
		return root;
	}

	@Override
	public void onResume() {
		super.onResume();

		setUpInsurances();
		requestInvestments();
		registerForEvents();
	}

	@Override
	public void onPause() {
		application.unregisterForAllEvents(this);
		stopTakeProfitTimer();
		stopRollForwardTimer();
		unsubscribeLs();
		super.onPause();
	}

	@Override
	public void onDestroyView() {
		application.unregisterForAllEvents(this);
		stopTakeProfitTimer();
		stopRollForwardTimer();
		unsubscribeLs();
		super.onDestroyView();
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		
		if (!hidden) {
			if(!areOptionsLoading) {
				requestInvestments();	
			}
		} else {
			unsubscribeLs();
		}
	}
	
	protected void registerForEvents(){
		application.registerForEvents(this, NavigationEvent.class, InsuranceEvent.class,
				LoginLogoutEvent.class, InvestmentEvent.class, ExpiredOptionsEvent.class,
				InsuranceBoughtEvent.class);
	}
	
	private void setUpUserDependentFields() {
		// Update the currency
		currency = application.getCurrency();

		Skin skin = application.getSkins().get(application.getSkinId());
		skinGroup = skin.getSkinGroup();
	}

	protected void setupListView(){
		optionsListView = (ListView) root.findViewById(R.id.trading_open_options_listview);

		setUpListAdapter();
		optionsListView.setAdapter(optionsListAdapter);

		setupItemClickListener();
	}

	protected void setupItemClickListener(){
		optionsListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Investment investment = investments.get(position);
				//Get the Market which is pressed from the cached Markets list:
				Market marketPressed = application.getMarketForID(investment.getMarketId());
				if(marketPressed != null){
					Bundle bundle = new Bundle();
					if (isTablet) {
						bundle.putSerializable(Constants.EXTRA_MARKET, marketPressed);
						bundle.putString(Constants.EXTRA_ACTION_BAR_TITLE, marketPressed.getDisplayName());
						bundle.putLong(Constants.EXTRA_MARKET_PRODUCT_TYPE, marketPressed.getProductTypeId());

						application.postEvent(new NavigationEvent(Screen.ASSET_PAGE, NavigationType.HORIZONTAL, bundle));
						application.postEvent(new NavigationEvent(Screen.TABLET_TRADE, NavigationType.INIT));
					} else {
						bundle.putSerializable(Constants.EXTRA_MARKET, marketPressed);
						application.postEvent(new NavigationEvent(Screen.ASSET_VIEW_PAGER, NavigationType.DEEP, bundle));
					}
				}

			}
		});
	}

	protected void setUpListAdapter() {
		optionsListAdapter = new TradingOptionsListAdapter(investments);
	}

	protected void requestInvestments() {
		if (!application.isLoggedIn()) {
			// We can not receive investments.
			return;
		}
		
		CommunicationManager communicationManager = application.getCommunicationManager();
		InvestmentsMethodRequest request = new InvestmentsMethodRequest();

		areOptionsLoading = true;
		communicationManager.requestService(this, "investmentsCallBack", Constants.SERVICE_GET_INVESTMENTS, request, InvestmentsMethodResult.class, false, true);
	}

	public void investmentsCallBack(Object result) {
		progressView.setVisibility(View.GONE);

		areOptionsLoading = false;
		InvestmentsMethodResult investmentsResult = (InvestmentsMethodResult) result;

		if (investmentsResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Just got open options: " + investmentsResult.getInvestments().length);
			unsubscribeLs();
			investments.clear();

			if (investmentsResult.getInvestments() != null && investmentsResult.getInvestments().length > 0) {
				InvestmentUtil.fixDate(investmentsResult.getInvestments());
				getInvestmentsFromResult(investmentsResult);
			}
			
			updateInvestmentsInfo();
			gotInvestmentsForAdapter();
			subscribeLs();
			if (investmentsResult.getUser() != null) {
				application.setUser(investmentsResult.getUser());
			}
		} else {
			Log.e(TAG, "Could not get open options! :)");
		}
	}

	protected void getInvestmentsFromResult(InvestmentsMethodResult investmentsResult){
		Collections.addAll(investments, investmentsResult.getInvestments());
	}

	private void setUpInsurances() {
		if (!takeProfitGroups.isEmpty()) {
			startTakeProfitTimer(takeProfitGroups.get(0).get(0).getInsuranceCloseTime());
		}
		updateTakeProfit();

		if (!rollForwardGroups.isEmpty()) {
			startRollForwardTimer(rollForwardGroups.get(0).get(0).getInsuranceCloseTime());

		}
		updateRollForward();
	}

	//////////////////////////////////////////////////////////////////////////////
	//							LIGHSTREAMER									//
	//////////////////////////////////////////////////////////////////////////////

	/**
	 * Subscribe for investment updates
	 */
	private void subscribeLs() {
		Log.v(TAG, "Subscribe Lighstreamer!");
		if (investments != null && investments.size() > 0) {
			StringBuilder sb = new StringBuilder();

			for (Investment investment : investments) {

				Market market = application.getMarketForID(investment.getMarketId());

				if(market == null){
					continue;
				}

				boolean isBinary 		= MarketsUtils.isBinary(market);
				boolean isOptionPlus 	= MarketsUtils.isOptionPlus(market);
				boolean isDynamics 		= MarketsUtils.isDynamics(market);

				if(isBinary){
					sb.append("aotps_1_").append(market.getId());
				}else if(isOptionPlus){
					sb.append("op_").append(market.getId());
				}else if(isDynamics){
					sb.append("bz_").append(market.getId()).append("_").append(Constants.DYNAMICS_OPPORTUNITY_TYPE_ID);
				}

				sb.append(" ");
			}

			application.getLightstreamerManager().subscribeOpenOptionsTable(this, sb.toString().split(" "));
		}
	}

	private void unsubscribeLs() {
		application.getLightstreamerManager().unsubscribeTable(LightstreamerManager.TABLE_OPEN_OPTIONS);
	}

	@Override
	public void updateItem(int itemPos, String itemName, UpdateInfo update) {
		try {
			long marketID;
			if(itemName.startsWith("bz")){
				marketID = Long.parseLong(itemName.split("_")[1]);
			}else{
				marketID = Long.valueOf(update.getNewValue(LightstreamerManager.COLUMN_ET_NAME));
			}

			Market market = application.getMarketForID(marketID);
			if(market == null){
				Log.e(TAG, "No Market found for this update! Market ID = " + marketID);
				return;
			}

			boolean isBinary 		= MarketsUtils.isBinary(market);
			boolean isOptionPlus 	= MarketsUtils.isOptionPlus(market);
			boolean isDynamics 		= MarketsUtils.isDynamics(market);

			long opportunityId;
			if(isDynamics){
				opportunityId = Long.valueOf(update.getNewValue(LightstreamerManager.COLUMN_ET_KEY));
			}else{
				opportunityId = Long.parseLong(update.getNewValue(LightstreamerManager.COLUMN_ET_OPP_ID));
			}

			Investment investment = getInvestmentForOpportunity(opportunityId);
			if (investment == null) {
				return;
			}

			String stateKey;
			if(isDynamics){
				stateKey = LightstreamerManager.COLUMN_DYNAMICS_STATE;
			}else{
				stateKey = LightstreamerManager.COLUMN_ET_STATE;
			}

			if (update.isValueChanged(stateKey)) {
				int opportunityState = Integer.parseInt(update.getNewValue(stateKey));
				if (opportunityState >= Opportunity.STATE_DONE) {
					removeInvestmentsForOpportunity(opportunityId);
					return;
				}
			}

			String currentLevelTxt;
			if(isDynamics){
				currentLevelTxt = update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_LEVEL);
			}else{
				currentLevelTxt = update.getNewValue(skinGroup.getLevelUpdateKey());
			}
			double currentLevel = Double.parseDouble(currentLevelTxt.replaceAll(",", ""));
			boolean updated = false;

			if (currentLevel == 0) {
				// Do not use this update
				return;
			}

			for (int i = 0; i < investments.size(); i++) {
				investment = investments.get(i);
				if ((investment.getOpportunityId() == opportunityId || (investment.getMarketId() == marketID
						&& investment.getAdditionalInfoType() == Investment.INVESTMENT_ADDITIONAL_INFO_NOT_HOURLY))
						&& (investment.getCurrentLevel() != currentLevel
						|| investment.getAmountReturnWF() == null
						|| investment.getAmountReturnWF().isEmpty())) {

					investment.setCurrentLevel(currentLevel);
					investment.setCurrentLevelTxt(currentLevelTxt);

					// Sets the return for this level
					String returnFormatted = AmountUtil
							.getFormattedAmount(
									InvestmentUtil.getOpenInvestmentReturn(investment, currentLevel),
									currency.getSymbol(), currency.getIsLeftSymbolBool(),
									currency.getDecimalPointDigits(), currency.getId());
					investment.setAmountReturnWF(returnFormatted);
					updated = true;
				}
			}

			if (updated) {
				updateInvestmentsInfo();
			}
		} catch (Exception e) {
			Log.e(TAG, "Error processing updateItem.", e);
		}
	}

	//////////////////////////////////////////////////////////////////////////////

	public void addInvestment(Investment investment) {
		// Clones the investment so the same object is not used from other fragment.
		Investment newInvestment = InvestmentUtil.cloneInvestment(investment);
		investments.add(0, newInvestment);

		boolean mustUnsubuscribe = true;
		for (Investment inv : investments) {
			if (inv.getMarketId() == newInvestment.getMarketId()) {
				mustUnsubuscribe = false;
				break;
			}
		}

		if (mustUnsubuscribe) {
			unsubscribeLs();
			subscribeLs();
		}

		updateInvestmentsInfo();
	}

	public void removeInvestment(long investmentId) {
		unsubscribeLs();

		// Find the position of the investment.
		int investmentPosition = -1;

		for (int i = 0; i < investments.size(); i++) {
			if (investments.get(i).getId() == investmentId) {
				investmentPosition = i;
				break;
			}
		}

		if (investmentPosition >= 0) {
			investments.remove(investmentPosition);
		} else {
			return;
		}

		updateInvestmentsInfo();
		subscribeLs();
	}
	
	/**
	 * Removes all investments with the same opportunityId.
	 * 
	 * @param opportunityId
	 */
	public void removeInvestmentsForOpportunity(long opportunityId) {
		unsubscribeLs();
		for (int i = 0; i < investments.size(); i++) {
			if (investments.get(i).getOpportunityId() == opportunityId) {
				investments.remove(i);
				i--;
			}
		}
		updateInvestmentsInfo();
		subscribeLs();
	}
	
	private Investment getInvestmentForOpportunity(long opportunityId){
		Investment result = null;
		for (int i = 0; i < investments.size(); i++) {
			Investment investment = investments.get(i);
			if (investment.getOpportunityId() == opportunityId) {
				result = investment;
				break;
			}
		}
		return result;
	}
	
	//#######################################################################
	//##################		EVENT BUS CALLBACKs			#################
	//#######################################################################
	
	/**
	 * Called when a {@link InsuranceEvent} occurs.
	 * 
	 * @param event
	 */
	public void onEventMainThread(InsuranceEvent event) {
		if(event.getInsuranceType() == InsuranceType.TAKE_PROFIT) {
			if(event.getType() == InsuranceEvent.Type.ADD) {
				if(takeProfitTimer == null && !takeProfitGroups.isEmpty()) {
					startTakeProfitTimer(takeProfitGroups.get(0).get(0).getInsuranceCloseTime());
					updateTakeProfit();
				}
			} else if(event.getType() == InsuranceEvent.Type.DELETE) {
				if(takeProfitGroups.isEmpty()) {
					stopTakeProfitTimer();
					updateTakeProfit();	
				}
			}
		} else if(event.getInsuranceType() == InsuranceType.ROLL_FORWARD){
			if(event.getType() == InsuranceEvent.Type.ADD) {
				if(rollForwardTimer == null && !rollForwardGroups.isEmpty()) {
					startRollForwardTimer(rollForwardGroups.get(0).get(0).getInsuranceCloseTime());
					updateRollForward();	
				}
			} else if(event.getType() == InsuranceEvent.Type.DELETE) {
				if(rollForwardGroups.isEmpty()) {
					stopRollForwardTimer();
					updateRollForward();	
				}	
			}
		}
	}
	
	/**
	 * Called when a {@link LoginLogoutEvent} occurs.
	 * 
	 * @param event
	 */
	public void onEventMainThread(LoginLogoutEvent event) {
		Log.d(TAG, "onEventMainThread : login event");
		setUpUserDependentFields();
		
		// Restarts the light streamer so it uses the user if logged in.
		unsubscribeLs();
		
		if (event.getType() == LoginLogoutEvent.Type.LOGIN
				|| event.getType() == LoginLogoutEvent.Type.REGISTER) {
			requestInvestments();
		} else if (event.getType() == LoginLogoutEvent.Type.LOGOUT) {
			investments.clear();
			updateInvestmentsInfo();
			updateTakeProfit();
			updateRollForward();
		}
	}
	
	/**
	 * Called when a {@link InvestmentEvent} occurs.
	 * 
	 * @param event
	 */
	public void onEventMainThread(InvestmentEvent event) {
		if (isHidden()) {
			return;
		}

		if (event.getType() == Type.INVEST_SUCCESS) {
			addInvestment(event.getInvestment());
		} else if (event.getType() == Type.INVESTMENT_SOLD) {
			removeInvestment(event.getInvestment().getId());
		}
		
	}
	
	/**
	 * Called when a {@link ExpiredOptionsEvent} occurs.
	 * 
	 * @param event
	 */
	public void onEventMainThread(ExpiredOptionsEvent event) {
		if (isHidden()) {
			return;
		}
		
		//Refresh the list:
		requestInvestments();
	}
	
	/**
	 * Callback for the {@link InsuranceBoughtEvent} events.
	 * 
	 * @param event
	 */
	public void onEvent(InsuranceBoughtEvent event) {
		// Updates the investments.
		requestInvestments();
	}

	@Override
	protected Screenable setupScreen() {
		return Screen.OPEN_OPTIONS;
	}

	@Override
	public void onStatusChange(int status) {
		// Do nothing
	}

	@Override
	public boolean handleMessage(Message msg) {
		if (msg.what == MESSAGE_UPDATE_INVESTMENTS) {
			updateInvestmentsUI();
			return true;
		}

		return false;
	}

	private void updateInvestmentsInfo() {
		long investedAmount = 0;
		double expectedProfit = 0;
		for (int i = 0; i < investments.size(); i++) {
			Investment investment = investments.get(i);
			boolean isBubbles = investment.getTypeId() == Investment.INVESTMENT_TYPE_BUBBLES;
			boolean isDynamics = investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY || investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_SELL;
			if(!isBubbles){
				investedAmount += investment.getAmount();
				if(!isDynamics || InvestmentUtil.isOpenInvestmentProfitable(investment, investment.getCurrentLevel())){
					expectedProfit += InvestmentUtil.getOpenInvestmentReturn(investment, investment.getCurrentLevel());
				}
			}
		}

		totalInvestedAmount = AmountUtil.getDoubleAmount(investedAmount);
		totalExpectedProfit = expectedProfit;

		if (!handler.hasMessages(MESSAGE_UPDATE_INVESTMENTS)) {
			handler.obtainMessage(MESSAGE_UPDATE_INVESTMENTS).sendToTarget();
		}
	}

	protected void refreshList(){
		optionsListAdapter.notifyDataSetChanged();
	}

	protected void gotInvestmentsForAdapter(){}

	private void updateInvestmentsUI() {
		refreshList();

		updateAmountValue(investedAmountView, totalInvestedAmount);
		updateAmountValue(profitAmountView, totalExpectedProfit);

		if (investments.size() > 0) {
			noOptionsView.setVisibility(View.GONE);
			if (totalExpectedProfit > totalInvestedAmount) {
				profitableImage.setVisibility(View.VISIBLE);
				dollarDownView.setImageResource(R.drawable.dollar_down);
			} else {
				profitableImage.setVisibility(View.GONE);
				dollarDownView.setImageResource(R.drawable.dollar_down_disabled);
			}
		} else {
			profitableImage.setVisibility(View.GONE);
			noOptionsView.setVisibility(View.VISIBLE);
			dollarDownView.setImageResource(R.drawable.dollar_down_disabled);
		}
	}

	protected void updateTakeProfit() {
		Log.d(TAG, "updateTakeProfit");

		if (!takeProfitGroups.isEmpty()) {
			takeProfitLabel.setTextColor(ColorUtils.getColor(R.color.white));
			takeProfitTimeView.setVisibility(View.VISIBLE);
			takeProfitButton.setClickable(true);
			takeProfitIcon.setImageResource(R.drawable.take_profit);
		} else {
			takeProfitLabel.setTextColor(ColorUtils.getColor(R.color.blue_dark_1));
			takeProfitTimeView.setVisibility(View.GONE);
			takeProfitButton.setClickable(false);
			takeProfitIcon.setImageResource(R.drawable.take_profit_disabled);
		}
	}

	protected void updateRollForward() {
		Log.d(TAG, "updateRollForward");

		if (!rollForwardGroups.isEmpty()) {
			rollForwardLabel.setTextColor(ColorUtils.getColor(R.color.white));
			rollForwardTimeView.setVisibility(View.VISIBLE);
			rollForwardButton.setClickable(true);
			rollForwardIcon.setImageResource(R.drawable.roll_forward);
		} else {
			rollForwardLabel.setTextColor(ColorUtils.getColor(R.color.blue_dark_1));
			rollForwardTimeView.setVisibility(View.GONE);
			rollForwardButton.setClickable(false);
			rollForwardIcon.setImageResource(R.drawable.roll_forward_disabled);
		}
	}

	private void startTakeProfitTimer(long closigTime) {
		long timerTime = closigTime - TimeUtils.getCurrentServerTime();

		takeProfitTimer = new CountDownTimer(timerTime, TIMER_UPDATE_INTERVAL_MS) {

			@Override
			public void onTick(long millisUntilFinished) {
				takeProfitTimeView.setText(TimeUtils
						.formatTimeToMinutesAndSeconds(millisUntilFinished));
			}

			@Override
			public void onFinish() {
				takeProfitTimeView.setText(TimeUtils.formatTimeToMinutesAndSeconds(0));
				updateTakeProfit();
			}
		};

		takeProfitTimer.start();
	}

	private void stopTakeProfitTimer() {
		if (takeProfitTimer != null) {
			takeProfitTimer.cancel();
			takeProfitTimer = null;
		}
	}

	private void startRollForwardTimer(long closigTime) {
		long timerTime = closigTime - TimeUtils.getCurrentServerTime();

		rollForwardTimer = new CountDownTimer(timerTime, TIMER_UPDATE_INTERVAL_MS) {

			@Override
			public void onTick(long millisUntilFinished) {
				rollForwardTimeView.setText(TimeUtils
						.formatTimeToMinutesAndSeconds(millisUntilFinished));
			}

			@Override
			public void onFinish() {
				rollForwardTimeView.setText(TimeUtils.formatTimeToMinutesAndSeconds(0));
				updateRollForward();
			}
		};

		rollForwardTimer.start();
	}

	private void stopRollForwardTimer() {
		if (rollForwardTimer != null) {
			rollForwardTimer.cancel();
			rollForwardTimer = null;
		}
	}

	/**
	 * Sets the formatted amount value to the given view.
	 * 
	 * @param view
	 * @param amount
	 */
	protected void updateAmountValue(TextView view, double amount) {
		AmountUtil.setTextViewAmount(view, amount, currency.getSymbol(),
				currency.getIsLeftSymbolBool(), currency.getDecimalPointDigits());
	}

}
