package com.anyoption.android.app.model;

import com.anyoption.common.beans.base.Market;

/**
 * Custom Market's wrapper that represents the markets table in the DataBase.
 * @author Anastas Arnaudov
 *
 */
public class MarketDB {

	private Market market;
	private String state;
	private long userID;
	
	public MarketDB(Market market){
		this.market = market;
	}
	
	
	public Market getMarket() {
		return market;
	}

	public void setMarket(Market market) {
		this.market = market;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public long getUserID() {
		return userID;
	}


	public void setUserID(long userID) {
		this.userID = userID;
	}
	
}
