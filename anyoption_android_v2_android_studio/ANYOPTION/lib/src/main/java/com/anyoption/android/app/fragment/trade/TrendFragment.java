package com.anyoption.android.app.fragment.trade;

import java.util.LinkedList;
import java.util.Queue;

import com.anyoption.android.R;
import com.anyoption.android.app.event.LightstreamerUpdateEvent;
import com.anyoption.android.app.lightstreamer.LightstreamerListener;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Country;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.lightstreamer.ls_client.UpdateInfo;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TrendFragment extends BaseAssetNestedFragment implements LightstreamerListener,
		Callback {

	private static final int DEFAULT_CALL_PERCENT = 50;
	public static final long NOTIFICATION_TIME = 5000l;

	private static final int MESSAGE_UPDATE_PERCENT = 1;
	private static final int MESSAGE_ADD_NOTIFICATION = 2;

	private View root;
	private TextView callPercentView;
	private TextView putPercentView;
	private View callLayout;
	private View putLayout;

	private Queue<Notification> notificationsQueue = new LinkedList<Notification>();
	private Handler handler;

	private int callPercent;
	private boolean isCallPercentLoaded;

	private View notificationLayout;
	private ImageView notificationCountryFlagImageView;
	private TextView notificationCountryTextView;
	private TextView notificationLevelTextView;
	private ImageView notificationInvestmentTypeImageView;
	private TextView notificationAmountTextView;

	private String countryID = "";
	private String investmentType = "";
	private String level = "";
	private String amount = "";

	/**
	 * Use this method to create new instance of the fragment.
	 *
	 * @return
	 */
	public static TrendFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new TrendFragment.");
		TrendFragment fragment = new TrendFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	/**
	 * Create new instance of this Fragment
	 *
	 * @param marketId
	 * @return
	 */
	public static TrendFragment newInstance(Market market) {
		TrendFragment fragment = new TrendFragment();
		Bundle args = new Bundle();
		args.putSerializable(Constants.EXTRA_MARKET, market);
		fragment.setArguments(args);
		return fragment;
	}

	/**
	 * Use {@link #newInstance(Market)} method instead this constructor.
	 */
	public TrendFragment() {
		// Empty constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		callPercent = DEFAULT_CALL_PERCENT;
		isCallPercentLoaded = false;

		handler = new Handler(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		root = inflater.inflate(R.layout.trend_fragment, container, false);
		callPercentView = (TextView) root.findViewById(R.id.trend_call_percent);
		putPercentView = (TextView) root.findViewById(R.id.trend_put_percent);
		callLayout = root.findViewById(R.id.call_layout);
		putLayout = root.findViewById(R.id.put_layout);

		notificationLayout = root.findViewById(R.id.trend_notification_layout);
		notificationCountryFlagImageView = (ImageView) root.findViewById(R.id.trend_notification_country_flag);
		((TextView) root.findViewById(R.id.trend_notification_country_label)).setText(getString(R.string.liveTradeFrom) + ":");
		notificationCountryTextView = (TextView) root.findViewById(R.id.trend_notification_country);
		((TextView) root.findViewById(R.id.trend_notification_level_label)).setText(getString(R.string.level) + ":");
		notificationInvestmentTypeImageView = (ImageView) root.findViewById(R.id.trend_notification_investment_type_image);
		notificationLevelTextView = (TextView) root.findViewById(R.id.trend_notification_level);
		((TextView) root.findViewById(R.id.trend_notification_amount_label)).setText(getString(R.string.receipt_amount));
		notificationAmountTextView = (TextView) root.findViewById(R.id.trend_notification_amount);

		updatePutCallPercent();

		return root;
	}


	private void clean(){
		for(Notification notification : notificationsQueue){
			notification.clean();
		}
		notificationsQueue.clear();
		notificationsQueue = null;
		amount = null;
		countryID = null;
		investmentType = null;
		level = null;
	}

	@Override
	public void onDestroy() {
		clean();
		super.onDestroy();
	}

	/**
	 * Subscribe the SOCIAL FEED Table in the LS.
	 */
	protected void subscribeSocialFeedLightstreamer() {
		application.getLightstreamerManager().subscribeLiveInvestmentsTable(this);
	}

	/**
	 * UNsubscribe the SOCIAL FEED Table from the LS.
	 */
	protected void unsubscribeSocialFeedLightstreamer() {
		application.getLightstreamerManager().unsubscribeTable(LightstreamerManager.ITEM_LIVE_INVESTMENTS);
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		if(hidden){
			unsubscribeSocialFeedLightstreamer();
		}else{
			subscribeSocialFeedLightstreamer();
		}
		super.onHiddenChanged(hidden);
	}

	@Override
	public void onResume() {
		super.onResume();
		subscribeSocialFeedLightstreamer();
		if (market != null) {
			registerForEvents();
		}
	}

	@Override
	public void onPause() {
		unsubscribeSocialFeedLightstreamer();
		application.unregisterForAllEvents(this);
		super.onPause();
	}

	@Override
	public void onDestroyView() {
		application.unregisterForAllEvents(this);
		super.onDestroyView();
	}

	private void registerForEvents() {
		application.registerForEvents(this, LightstreamerUpdateEvent.class);
	}

	/**
	 * Called when a {@link LightstreamerUpdateEvent} occurs.
	 *
	 * @param event
	 */
	public void onEvent(LightstreamerUpdateEvent event) {
		if (event.getMarketId() == market.getId()) {
			updateItem(event.getUpdateInfo());
		}
	}

	private void updateItem(UpdateInfo update) {
		if (!isCallPercentLoaded || update.isValueChanged(LightstreamerManager.COLUMN_AO_CALLS_TREND)) {
			isCallPercentLoaded = true;
			callPercent = Math.round(Float.parseFloat(update.getNewValue(LightstreamerManager.COLUMN_AO_CALLS_TREND)) * 100);
			if (!handler.hasMessages(MESSAGE_UPDATE_PERCENT)) {
				handler.obtainMessage(MESSAGE_UPDATE_PERCENT).sendToTarget();
			}
		}
	}

	public void reload(UpdateInfo update){
		if(update != null){
			updateItem(update);
		}else{
			callPercent = DEFAULT_CALL_PERCENT;
			isCallPercentLoaded = false;
			if (!handler.hasMessages(MESSAGE_UPDATE_PERCENT)) {
				handler.obtainMessage(MESSAGE_UPDATE_PERCENT).sendToTarget();
			}
		}
	}

	@Override
	public boolean handleMessage(Message msg) {
		switch (msg.what) {
			case MESSAGE_UPDATE_PERCENT:
				updatePutCallPercent();
				break;
			case MESSAGE_ADD_NOTIFICATION:
				Country country = application.getCountriesMap().get(Long.valueOf(countryID));
				addNotification(new Notification(country, investmentType, level, amount));
				break;
		}

		return true;
	}

	private void updatePutCallPercent() {
		callPercentView.setText(callPercent + "%");
		putPercentView.setText(100 - callPercent + "%");

		LinearLayout.LayoutParams callParams = (LinearLayout.LayoutParams) callLayout
				.getLayoutParams();
		callParams.weight = callPercent;
		callLayout.setLayoutParams(callParams);
		callLayout.invalidate();

		LinearLayout.LayoutParams putParams = (LinearLayout.LayoutParams) putLayout
				.getLayoutParams();
		putParams.weight = 100 - callPercent;
		putLayout.setLayoutParams(putParams);
		putLayout.invalidate();
	}

	@Override
	protected void onMarketChanged() {
		super.onMarketChanged();

		registerForEvents();
	}

	@Override
	public void loadData() {
		// Do nothing
	}

	@Override
	public void updateItem(int itemPos, String itemName, UpdateInfo update) {
		if(market != null && update.isValueChanged(LightstreamerManager.COLUMN_LI_ASSET)){
			String marketID = update.getNewValue(LightstreamerManager.COLUMN_LI_ASSET);
			if(marketID != null && market.getId() == Long.valueOf(marketID)){
				String newCountryID = update.getNewValue(LightstreamerManager.COLUMN_LI_COUNTRY);
				String newInvestmentType = update.getNewValue(LightstreamerManager.COLUMN_LI_INV_TYPE);
				String newLevel = update.getNewValue(LightstreamerManager.COLUMN_LI_LEVEL);
				String newAmount = update.getNewValue(LightstreamerManager.COLUMN_LI_AMOUNT);

				//Check if there are any changes in the arguments:
				if(!countryID.equals(newCountryID) || !investmentType.equals(newInvestmentType) || !level.equals(newLevel) || !amount.equals(newAmount)){
					countryID = newCountryID;
					investmentType = newInvestmentType;
					level = newLevel;
					amount = newAmount;

					if (!handler.hasMessages(MESSAGE_ADD_NOTIFICATION)) {
						handler.obtainMessage(MESSAGE_ADD_NOTIFICATION).sendToTarget();
					}
				}

			}
		}

	}

	/**
	 * Adds the {@linkNotification} to the notification queue.
	 * @param notification
	 */
	private void addNotification(Notification notification){
		if(notificationsQueue != null){
			notificationsQueue.add(notification);
			if(notificationsQueue.size() == 1){
				notification.show();
			}
		}
	}

	//######################################################################
	//########					NOTIFICATION						########
	//######################################################################
	/**
	 * Social Feed Notification.
	 * @author Anastas Arnaudov
	 *
	 */
	private class Notification{

		private Country country;
		private String investmentType;
		private String level;
		private String amount;

		private CountDownTimer notificationTimer;

		public Notification(Country country, String investmentType, String level, String amount){
			this.country = country;
			this.investmentType = investmentType;
			this.level = level;
			this.amount = amount;
		}

		/**
		 * Setup the timer for the Social Feed Notification.
		 */
		private void setupTimer() {
			notificationTimer = new CountDownTimer(NOTIFICATION_TIME, NOTIFICATION_TIME) {

				@Override
				public void onTick(long millisUntilFinished) {}

				@Override
				public void onFinish() {
					notificationLayout.setVisibility(View.GONE);
					notificationsQueue.poll();
					//Check if there are any notifications waiting in the queue and show the oldest one:
					if(!notificationsQueue.isEmpty()){
						notificationsQueue.peek().show();
					}
				}
			};
		}

		/**
		 * Shows the SOCIAL FEED Notification.
		 */
		@SuppressLint("DefaultLocale")
		public void show(){

			if(this.country != null){
				//Set the country flag:
				String countryCode = country.getA2().toLowerCase();
				Drawable image = DrawableUtils.getImage(countryCode);
				notificationCountryFlagImageView.setImageDrawable(image);

				//Set country name:
				notificationCountryTextView.setText(country.getName());
			}else{
				return;
			}

			// Set the triangle image:
			int investmentTypeImageId = 0;
			if (investmentType.equals(String.valueOf(Investment.INVESTMENT_TYPE_PUT))) {
				investmentTypeImageId = R.drawable.trendPutIcon;
			} else if (investmentType.equals(String.valueOf(Investment.INVESTMENT_TYPE_CALL))) {
				investmentTypeImageId = R.drawable.trendCallIcon;
			}
			notificationInvestmentTypeImageView.setImageResource(investmentTypeImageId);

			//Set the level:
			notificationLevelTextView.setText(level);

			//Set the amount:
			notificationAmountTextView.setText(amount);

			notificationLayout.setVisibility(View.VISIBLE);

			if(notificationTimer == null){
				setupTimer();
			}
			notificationTimer.start();
		}

		public void clean(){
			if(this.notificationTimer != null){
				this.notificationTimer.cancel();
			}
			this.notificationTimer = null;
			this.amount = null;
			this.country = null;
			this.investmentType = null;
			this.level = null;
		}

	}
	//######################################################################

}
