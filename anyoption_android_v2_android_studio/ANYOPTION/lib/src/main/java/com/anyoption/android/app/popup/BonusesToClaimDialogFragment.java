package com.anyoption.android.app.popup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.manager.PopUpManager;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.BonusUsers;

public class BonusesToClaimDialogFragment extends OkDialogFragment {

	public static final String TAG = BonusesToClaimDialogFragment.class.getSimpleName();

	protected TextView firstMessageTextView;
	protected TextView secondMessageTextView;
	protected TextView bonusTitle;
	protected View bonusesInfo;
	protected View bonusesInfoHorizontalLine;

	protected static boolean isSingle = false;
	protected static BonusUsers bonus;
	protected static String[] turnoverFactor;

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static BonusesToClaimDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new BonusesToClaimDialogFragment.");
		BonusesToClaimDialogFragment fragment = new BonusesToClaimDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if(args != null){
			setCancelable(args.getBoolean(Constants.EXTRA_CANCELABLE, false));
			isSingle = args.getBoolean(Constants.EXTRA_IS_SINGLE_BONUS, false);
			turnoverFactor = args.getStringArray(Constants.EXTRA_BONUS_FACTOR);
			bonus = (BonusUsers) args.getSerializable(Constants.EXTRA_BONUS);
		} else {
			isSingle = false;
		}
	}

	@Override
	protected int getContentLayout() {
		return 0;
	}

	@Override
	public int getPopupCode(){
		return PopUpManager.POPUP_BONUSES_TO_CLAIM;
	}

}
