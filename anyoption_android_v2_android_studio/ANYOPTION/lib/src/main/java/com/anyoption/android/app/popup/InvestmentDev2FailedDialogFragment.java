package com.anyoption.android.app.popup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.anyoption.android.R;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.TextView;

/**
 * @author Anastas Arnaudov
 */
public class InvestmentDev2FailedDialogFragment extends BaseDialogFragment {

    public static final String TAG = InvestmentDev2FailedDialogFragment.class.getSimpleName();
    private String message;

    public static InvestmentDev2FailedDialogFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new InvestmentCanceldDialogFragment.");
        InvestmentDev2FailedDialogFragment fragment = new InvestmentDev2FailedDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if(bundle != null){
            message = bundle.getString(Constants.EXTRA_MESSAGE);
        }
    }

    @Override
    protected int getContentLayout() {
        return R.layout.popup_investment_dev2_failed;
    }

    @Override
    protected void onCreateContentView(View contentView) {

        //Set up the message:
        TextView messageTextView = (TextView) contentView.findViewById(R.id.messageTextView);
        messageTextView.setText(message);

        //Set up the button:
        View okButton = contentView.findViewById(R.id.ok_button);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

}
