package com.anyoption.android.app.fragment.my_account.questionnaire;

import java.util.ArrayList;
import java.util.List;

import com.anyoption.android.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.manager.CommunicationManager;
import com.anyoption.android.app.model.questionnaire.BaseQuestionItem;
import com.anyoption.android.app.model.questionnaire.CheckboxQuestion;
import com.anyoption.android.app.model.questionnaire.MultipleChoiceQuestion;
import com.anyoption.android.app.model.questionnaire.SingleChoiceQuestion;
import com.anyoption.android.app.model.questionnaire.SpinnerQuestion;
import com.anyoption.android.app.model.questionnaire.YesNoQuestion;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.TextView;
import com.anyoption.android.app.widget.form.FormCheckBox;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.android.app.widget.form.FormRadioButton;
import com.anyoption.android.app.widget.form.FormRadioGroup;
import com.anyoption.android.app.widget.form.FormSelector.ItemListener;
import com.anyoption.android.app.widget.form.FormStringSelector;
import com.anyoption.common.beans.base.QuestionnaireAnswer;
import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.json.requests.UpdateUserQuestionnaireAnswersRequest;
import com.anyoption.json.results.UserQuestionnaireResult;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

public abstract class BaseQuestionnaireFragment extends NavigationalFragment {

	@SuppressWarnings("hiding")
	private static final String TAG = BaseQuestionnaireFragment.class.getSimpleName();

	CommunicationManager communicationManager;

	protected List<BaseQuestionItem> questions;

	/**
	 * A list keeping references to all user userAnswers.
	 */
	protected List<QuestionnaireUserAnswer> userAnswers;

	private LayoutInflater layoutInflater;
	private TextView titleView;
	private View submitButton;
	private View notNowButton;
	private LinearLayout questionsLayout;

	protected boolean isTablet;
	protected boolean areAnswersSubmitting;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		communicationManager = application.getCommunicationManager();

		questions = new ArrayList<BaseQuestionItem>();
		isTablet = getResources().getBoolean(R.bool.isTablet);

		initQuestions(questions);
		areAnswersSubmitting = false;
	}

	protected abstract void initQuestions(List<BaseQuestionItem> questionsList);

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		layoutInflater = inflater;
		View root = layoutInflater.inflate(R.layout.questionnaire_fragment, container, false);
		titleView = (TextView) root.findViewById(R.id.questionnaire_title);
		titleView.setText(getTitle());

		submitButton = root.findViewById(R.id.questionnaire_submit_button);
		submitButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!areAnswersSubmitting) {
					submitAnswers();
				}
			}
		});
		submitButton.setClickable(false);
		submitButton.setEnabled(false);
		
		notNowButton = root.findViewById(R.id.questionnaire_not_now_button);
		questionsLayout = (LinearLayout) root.findViewById(R.id.questionnaire_questions_layout);

		return root;
	}

	@Override
	public void onResume() {
		super.onResume();

		communicationManager.requestService(this, "getCurrentUserAnswersCallback",
				getCurrentUserAnswersMethod(), new UserMethodRequest(),
				UserQuestionnaireResult.class, true, true);
	}

	public void getCurrentUserAnswersCallback(Object resultObj) {
		Log.d(TAG, "getCurrentUserAnswersCallback");
		UserQuestionnaireResult result = (UserQuestionnaireResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			userAnswers = result.getUserAnswers();
			updateQuestions(result.getQuestions(), userAnswers);
			submitButton.setClickable(true);
			submitButton.setEnabled(true);
		} else {
			Log.e(TAG, "Getting user userAnswers failed");
			openHomeScreen();
		}
	}

	protected abstract String getTitle();

	protected abstract String getCurrentUserAnswersMethod();

	protected abstract String getSubmitRequestMethod();

	public void submitAnswers() {
		if (areAllAnswered()) {
			areAnswersSubmitting = true;
			communicationManager.requestService(this, "getSubmitResultCallback",
					getSubmitRequestMethod(), new UserMethodRequest(), UserMethodResult.class,
					true, true);
		} else {
			application.getNotificationManager().showNotificationError(
					getString(R.string.questionnaire_error_allquestionsmandatory));
		}
	}

	public void getSubmitResultCallback(Object resultObj) {
		areAnswersSubmitting = false;
		UserMethodResult result = (UserMethodResult) resultObj;

		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			application.setUser(result.getUser());
			application.setUserRegulation(result.getUserRegulation());
		} else if (result.getErrorCode() == Constants.ERROR_CODE_QUESTIONNAIRE_ALREADY_DONE) {
			openHomeScreen();
			return;
		}else {
			application.getNotificationManager().showNotificationError(getString(R.string.error));
		}
		
		getSubmitResultCallback(result);
	}

	/**
	 * Handle the result from submit request.
	 * 
	 * @param result
	 */
	protected abstract void getSubmitResultCallback(UserMethodResult result);

	protected void updateUserAnswers() {
		if (areAnswersSubmitting) {
			// Prevents updating userAnswers after they've been submitted.
			return;
		}
		
		UpdateUserQuestionnaireAnswersRequest request = new UpdateUserQuestionnaireAnswersRequest();

		request.setUserAnswers(userAnswers);
		communicationManager.requestService(this, "updateUserAnswersCallback",
				Constants.SERVICE_UPDATE_QUESTIONNAIRIE_ANSWERS, request,
				UserQuestionnaireResult.class, false, false);
	}

	public void updateUserAnswersCallback(Object resultObj) {
		Log.d(TAG, "updateUserAnswersCallback");
		UserQuestionnaireResult result = (UserQuestionnaireResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			// Do nothing. All the userAnswers should be updated at this point.
		} else {
			if (result != null) {
				Log.e(TAG, "Updating user userAnswers failed: ResultCode: " + result.getErrorCode());
			} else {
				Log.e(TAG, "Updating user userAnswers failed");
			}

			if (result.getErrorCode() == Constants.ERROR_CODE_QUESTIONNAIRE_ALREADY_DONE) {
				Log.e(TAG, "Questionnaire already done");
				openHomeScreen();
			}
		}
	}

	protected boolean areAllAnswered() {
		for (BaseQuestionItem question : questions) {
			if (!question.isAnswered()) {
				Log.d(TAG, "NOT ALL QUESTIONS ANSWERED");
				return false;
			}
		}

		return true;
	}

	/**
	 * Updates all question items and recreate the layout using the new information.
	 * 
	 * @param receivedQuestions
	 * @param receivedAnswers
	 */
	protected void updateQuestions(List<QuestionnaireQuestion> receivedQuestions,
			List<QuestionnaireUserAnswer> receivedAnswers) {
		for (int i = 0; i < questions.size(); i++) {
			BaseQuestionItem currentQuestion = questions.get(i);
			if (currentQuestion instanceof SingleChoiceQuestion) {
				for (QuestionnaireQuestion receivedQuestion : receivedQuestions) {
					if (receivedQuestion.getOrderId() == currentQuestion.getOrderId()) {
						((SingleChoiceQuestion) currentQuestion).setQuestion(receivedQuestion);

						for (QuestionnaireUserAnswer receivedAnswer : receivedAnswers) {
							if (receivedAnswer.getQuestionId() == receivedQuestion.getId()) {
								((SingleChoiceQuestion) currentQuestion)
										.setUserAnswer(receivedAnswer);
								break;
							}
						}

						break;
					}
				}
			} else {
				// MultipleChoiceQuestion
				for (QuestionnaireQuestion receivedQuestion : receivedQuestions) {
					if (receivedQuestion.getOrderId() == currentQuestion.getOrderId()) {
						((MultipleChoiceQuestion) currentQuestion).setQuestion(receivedQuestion);
						List<QuestionnaireUserAnswer> questionUserAnswers = new ArrayList<QuestionnaireUserAnswer>();

						for (QuestionnaireUserAnswer receivedAnswer : receivedAnswers) {
							if (receivedAnswer.getQuestionId() == receivedQuestion.getId()) {
								questionUserAnswers.add(receivedAnswer);
							}
						}

						((MultipleChoiceQuestion) currentQuestion)
								.setUserAnswers(questionUserAnswers);
						break;
					}
				}
			}

		}

		updateQuestionsLayout();
	}

	public void setQuestions(List<BaseQuestionItem> questions) {
		this.questions = questions;
	}

	protected List<BaseQuestionItem> getQuestions() {
		return questions;
	}

	private void updateQuestionsLayout() {
		questionsLayout.removeAllViews();
		boolean isOddItem = true;
		int grayColor = ColorUtils.getColor(R.color.questionnaire_odd_item_bg);

		for (BaseQuestionItem questionItem : questions) {
			if (questionItem.isVisible()) {
				View questionView = getQuestionView(questionItem);
				if (isOddItem) {
					questionView.setBackgroundColor(grayColor);
					isOddItem = false;
				} else {
					isOddItem = true;
				}
				questionsLayout.addView(questionView);
			}
		}
	}

	public View getQuestionView(BaseQuestionItem question) {
		if (question instanceof YesNoQuestion) {
			return getYesNoView((YesNoQuestion) question);
		} else if (question instanceof CheckboxQuestion) {
			return getCheckboxView((CheckboxQuestion) question);
		} else if (question instanceof MultipleChoiceQuestion) {
			return getMultipleChoiceView((MultipleChoiceQuestion) question);
		} else {
			return getSpinnerView((SpinnerQuestion) question);
		}
	}

	private View getYesNoView(final YesNoQuestion question) {
		View rootView = layoutInflater.inflate(R.layout.questionnaire_yes_no_question,
				questionsLayout, false);

		TextView questionView = (TextView) rootView.findViewById(R.id.questionnaire_question_text);
		questionView.setText(question.getQuestionText());

		final FormRadioGroup radioGroup = (FormRadioGroup) rootView
				.findViewById(R.id.questionnaire_answer_radio_group);

		final FormRadioButton yesButton = (FormRadioButton) rootView
				.findViewById(R.id.questionnaire_yes_radio_button);

		final FormRadioButton noButton = (FormRadioButton) rootView
				.findViewById(R.id.questionnaire_no_radio_button);

		if (question.isAnswered()) {
			if (question.getYesAnswer().getId() == question.getUserAnswer().getAnswerId()) {
				yesButton.setChecked(true);
			} else {
				noButton.setChecked(true);
			}
		}

		radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				QuestionnaireAnswer newAnswer;
				if (checkedId == yesButton.getId()) {
					newAnswer = question.getYesAnswer();
				} else {
					newAnswer = question.getNoAnswer();
				}

				QuestionnaireUserAnswer currentAnswer = question.getUserAnswer();
				if (BaseQuestionItem.isAnswerChanged(currentAnswer.getAnswerId(),
						newAnswer.getId())) {
					question.getUserAnswer().setAnswerId(newAnswer.getId());

					updateUserAnswers();
				}

			}
		});

		return rootView;
	}

	private View getCheckboxView(final CheckboxQuestion question) {
		View rootView = layoutInflater.inflate(R.layout.questionnaire_checkbox_question,
				questionsLayout, false);

		FormCheckBox checkBox = (FormCheckBox) rootView
				.findViewById(R.id.questionnaire_checkbox_question);
		checkBox.setText(question.getQuestionText());
		checkBox.setChecked(question.isChecked());

		checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				QuestionnaireAnswer newAnswer;
				if (isChecked) {
					newAnswer = question.getCheckedAnswer();
				} else {
					newAnswer = question.getNotCheckedAnswer();
				}

				QuestionnaireUserAnswer currentAnswer = question.getUserAnswer();
				if (BaseQuestionItem.isAnswerChanged(currentAnswer.getAnswerId(),
						newAnswer.getId())) {
					question.setChecked(isChecked);

					updateUserAnswers();
				}

			}
		});

		return rootView;
	}

	private View getSpinnerView(final SpinnerQuestion question) {
		View rootView = layoutInflater.inflate(R.layout.questionnaire_spinner_question,
				questionsLayout, false);

		TextView questionView = (TextView) rootView
				.findViewById(R.id.questionnaire_spinner_question_text);
		questionView.setText(question.getQuestionText());
		final FormStringSelector answerSelector = (FormStringSelector) rootView
				.findViewById(R.id.questionnaire_spinner_answer_selector);
		answerSelector.setNotSelectedText("");

		int selectedAnswerIndex = question.getSelectedAnswerIndex();
		final List<String> answerTexts = question.getAnswerTexts();

		if (selectedAnswerIndex >= 0) {
			answerSelector.showText(answerTexts.get(selectedAnswerIndex));
		}

		answerSelector.setItems(answerTexts, selectedAnswerIndex, new ItemListener<String>() {

			@Override
			public void onItemSelected(String selectedItem) {
				// Find the index
				int selectedIndex = 0;
				for (int i = 0; i < answerTexts.size(); i++) {
					if (answerTexts.get(i).equals(selectedItem)) {
						selectedIndex = i;
						answerSelector.showText(selectedItem);
					}
				}

				Long oldAnswerId = question.getUserAnswer().getAnswerId();
				Long newAnswerId = question.getQuestion().getAnswers().get(selectedIndex).getId();
				if (BaseQuestionItem.isAnswerChanged(oldAnswerId, newAnswerId)) {
					question.getUserAnswer().setAnswerId(newAnswerId);
					updateUserAnswers();
				}
			}

			@Override
			public String getDisplayText(String selectedItem) {
				return selectedItem;
			}

			@Override
			public Drawable getIcon(String selectedItem) {
				Drawable drawable = DrawableUtils.getDrawable(R.drawable.transparent_selector);
				return drawable;
			}

		});

		return rootView;
	}

	private View getMultipleChoiceView(final MultipleChoiceQuestion question) {
		View rootView = layoutInflater.inflate(R.layout.questionnaire_multiple_choice_question,
				questionsLayout, false);

		TextView questionView = (TextView) rootView
				.findViewById(R.id.questionnaire_multiple_choice_question_text);
		questionView.setText(question.getQuestionText());

		LinearLayout answersLayout = (LinearLayout) rootView
				.findViewById(R.id.questionnaire_multiple_choice_answers_layout);
		int answersCount = answersLayout.getChildCount();

		for (int i = 0; i < answersCount; i++) {
			FormCheckBox checkBox = (FormCheckBox) answersLayout.getChildAt(i);
			checkBox.setText(question.getAnswerTexts().get(i));
			if (question.getUserAnswers().get(i).getAnswerId() != null
					&& question.getUserAnswers().get(i).getAnswerId() == question.getQuestion()
							.getAnswers().get(i).getId()) {
				checkBox.setChecked(true);
			} else {
				checkBox.setChecked(false);
			}

			final int answerIndex = i;
			checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if (isChecked) {
						question.getUserAnswers()
								.get(answerIndex)
								.setAnswerId(
										question.getQuestion().getAnswers().get(answerIndex)
												.getId());
					} else {
						question.getUserAnswers().get(answerIndex).setAnswerId(null);
					}

					updateUserAnswers();
				}
			});
		}

		final FormCheckBox checkBoxOther = (FormCheckBox) rootView
				.findViewById(R.id.questionnaire_multiple_choice_other);

		final FormEditText otherTextForm = (FormEditText) rootView
				.findViewById(R.id.questionnaire_multiple_choice_other_text);
		otherTextForm.getEditText().setText(
				question.getUserAnswers().get(answersCount - 1).getTextAnswer());

		final EditText otherEditText = otherTextForm.getEditText();
		// otherText.setText(question.getUserAnswers().get(answersCount - 1).getTextAnswer());

		otherEditText.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				QuestionnaireUserAnswer otherUserAnswer = question.getUserAnswers().get(
						question.getUserAnswers().size() - 1);
				if (!hasFocus) {
					if (checkBoxOther.isChecked()) {
						otherUserAnswer.setAnswerId(question.getQuestion().getAnswers()
								.get(question.getQuestion().getAnswers().size() - 1).getId());
					} else {
						otherUserAnswer.setAnswerId(null);
					}
					otherUserAnswer.setTextAnswer(otherEditText.getText().toString());
					updateUserAnswers();
				}

			}
		});

		otherEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (count > 0) {
					checkBoxOther.setChecked(true);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// Do nothing
			}

			@Override
			public void afterTextChanged(Editable s) {
				// Do nothing
			}
		});
		
		return rootView;
	}

	/**
	 * Hide the questionnaire and open trade page
	 */
	protected void openHomeScreen() {
		application
				.postEvent(new NavigationEvent(application.getHomeScreen(), NavigationType.INIT));
	}

	/**
	 * Makes "Not now" button visible and sets its click listener.
	 */
	protected void showNotNow() {
		notNowButton.setVisibility(View.VISIBLE);
		notNowButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				openHomeScreen();
			}
		});
	}

}
