package com.anyoption.android.app.widget.notification;

import com.anyoption.android.app.Application;
import com.anyoption.android.R;
import com.anyoption.android.app.popup.DepositReceiptDialogFragment;
import com.anyoption.android.app.util.constants.Constants;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class NotificationDepositOK extends LinearLayout{

	private String amount;
	private String date;
	private long id;
	
	public NotificationDepositOK(String amount, String date, long id){
		super(Application.get());
		this.amount = amount;
		this.date = date;
		this.id = id;
		init(Application.get(), null, 0);
	}
	public NotificationDepositOK(Context context) {
		super(context);
		init(context, null, 0);
	}
	public NotificationDepositOK(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	public NotificationDepositOK(Context context, AttributeSet attrs,int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		View view = View.inflate(context, R.layout.notification_deposit_ok_layout, this);

		Button receiptButton = (Button) view.findViewById(R.id.notification_purchase_receipt_button);
		
		receiptButton.setOnClickListener(new OnClickListener() {
			private boolean isPressed = false;
			@Override
			public void onClick(View v) {
				if(!isPressed){
					isPressed = true;
					Bundle bundle = new Bundle();
					bundle.putString(Constants.EXTRA_DEPOSIT_AMOUNT, amount);
					bundle.putString(Constants.EXTRA_DEPOSIT_DATE, date);
					bundle.putLong(Constants.EXTRA_DEPOSIT_ID, id);
					Application.get().getFragmentManager().showDialogFragment(DepositReceiptDialogFragment.class, bundle);

					Application.get().sendReceiptDeposit(id);
				}
				
			}
		});
	}

}
