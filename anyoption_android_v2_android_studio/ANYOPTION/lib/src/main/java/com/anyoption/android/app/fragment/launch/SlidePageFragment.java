package com.anyoption.android.app.fragment.launch;

import com.anyoption.android.R;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.util.BitmapUtils;
import com.anyoption.android.app.util.constants.Constants;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class SlidePageFragment extends BaseFragment {

	public static final String TAG = SlidePageFragment.class.getSimpleName();
	
	public static SlidePageFragment newInstance(Bundle bundle){
		SlidePageFragment slidePageFragment = new SlidePageFragment();
		slidePageFragment.setArguments(bundle);
		return slidePageFragment;
	}
	
	public static SlidePageFragment newInstance(int layoutID, int imageResId){
		SlidePageFragment slidePageFragment = new SlidePageFragment();
		Bundle bundle = new Bundle();
		bundle.putInt(Constants.EXTRA_SLIDE_VIEW_ID, layoutID);
		bundle.putInt(Constants.EXTRA_IMAGE_ID, imageResId);
		slidePageFragment.setArguments(bundle);
		return slidePageFragment;
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup rootView;
		
		int viewID = getArguments().getInt(Constants.EXTRA_SLIDE_VIEW_ID);
		int imageId = getArguments().getInt(Constants.EXTRA_IMAGE_ID);
		
		if(viewID > 0){
			rootView = (ViewGroup) inflater.inflate(viewID, container, false);
			if(imageId > 0){
				try {
					ImageView imageView = (ImageView) rootView.findViewById(R.id.imageView);
					if(imageView != null){
						BitmapUtils.loadBitmap(imageId, imageView);
					}
				} catch (Exception e) {
					Log.e(TAG, "could not inflate view ", e);
				}				
			}
		}else{
			rootView = (ViewGroup) inflater.inflate(R.layout.slide_page_fragment_default_layout, container, false);
		}

        return rootView;
    }

}
