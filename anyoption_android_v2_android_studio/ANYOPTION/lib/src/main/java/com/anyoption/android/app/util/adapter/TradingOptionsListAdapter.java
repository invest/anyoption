package com.anyoption.android.app.util.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.InvestmentUtil;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.Opportunity;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class TradingOptionsListAdapter extends BaseAdapter {

	private static final String TIME_FORMAT_PATTERN 		= "dd/MM/yyyy | HH:mm";
	private static final String TIME_FORMAT_PATTERN_BUBBLES = "dd/MM/yyyy | HH:mm:ss";
	private final DecimalFormat percentDecimalFormat;

	private LayoutInflater inflater;
	private Resources resources;

	protected Application application;
	protected Currency currency;
	protected List<Investment> investments;

	protected boolean isInvestmentOpen;

	private SimpleDateFormat timeFormat;
	private SimpleDateFormat timeFormatBubbles;

	public TradingOptionsListAdapter(List<Investment> investments, boolean isInvestmentOpen) {
		this.investments = investments;
		this.isInvestmentOpen = isInvestmentOpen;

		percentDecimalFormat = new DecimalFormat("##.##");
		application = Application.get();
		resources = application.getResources();

		if (application.isLoggedIn()) {
			currency = application.getCurrency();
		}

		timeFormat 			= new SimpleDateFormat(TIME_FORMAT_PATTERN);
		timeFormatBubbles 	= new SimpleDateFormat(TIME_FORMAT_PATTERN_BUBBLES);

		inflater = (LayoutInflater) application.getCurrentActivity().getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
	}

	public TradingOptionsListAdapter(List<Investment> investments) {
		this(investments, true);
	}

	public TradingOptionsListAdapter() {
		this(new ArrayList<Investment>());
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		Investment currentInvestment = investments.get(position);
		Market market = application.getMarketForID(currentInvestment.getMarketId());

		boolean isDynamics = currentInvestment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY || currentInvestment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_SELL;
		boolean isBubbles  = currentInvestment.getTypeId() == Investment.INVESTMENT_TYPE_BUBBLES;

		View rootView = convertView;
		if (convertView == null) {
			rootView = inflater.inflate(R.layout.trading_options_item_row, parent, false);
		}

		ImageView assetIcon = (ImageView) rootView.findViewById(R.id.trading_options_asset_image);
		if (market == null) {
			assetIcon.setImageDrawable(DrawableUtils.getMarketIconForId(currentInvestment.getMarketId()));
		} else {
			assetIcon.setImageDrawable(DrawableUtils.getMarketIcon(market));
		}

		ImageView plusIcon = (ImageView) rootView.findViewById(R.id.trading_options_option_plus_icon);
		if (currentInvestment.getOpportunityTypeId() == Opportunity.TYPE_OPTION_PLUS) {
			plusIcon.setImageResource(R.drawable.option_plus_icon_1);
			plusIcon.setVisibility(View.VISIBLE);
		}else if(isDynamics){
			plusIcon.setImageResource(R.drawable.dynamics_icon_1);
			plusIcon.setVisibility(View.VISIBLE);
		}else if(isBubbles){
			plusIcon.setImageResource(R.drawable.bubblesMarketIcon);
			plusIcon.setVisibility(View.VISIBLE);
		}else {
			plusIcon.setVisibility(View.GONE);
		}

		TextView assetNameView = (TextView) rootView.findViewById(R.id.trading_options_asset_name);
		assetNameView.setText(currentInvestment.getAsset());
		TextView timeInvestedView = (TextView) rootView.findViewById(R.id.trading_options_time_invested);
		if(isBubbles){
			timeInvestedView.setText(timeFormatBubbles.format(currentInvestment.getTimeCreated()));
		}else{
			timeInvestedView.setText(timeFormat.format(currentInvestment.getTimeCreated()));
		}

		TextView levelInvestedView = (TextView) rootView.findViewById(R.id.trading_options_level_invested);
		levelInvestedView.setText(currentInvestment.getLevel());
		TextView amountInvestedView = (TextView) rootView.findViewById(R.id.trading_options_amount_invested);
		if(isDynamics){
			formatDynamicsProfit(amountInvestedView, currentInvestment);
		}else{
			String newAmount = AmountUtil.insertIntervalBetweenAmountAndSymbol(currentInvestment.getAmountTxt(), application.getUser().getCurrency());
			amountInvestedView.setText(newAmount);
		}

		TextView currentReturnView = (TextView) rootView.findViewById(R.id.trading_options_current_return);
		if(isBubbles && isInvestmentOpen){
			currentReturnView.setText("");
		}else{
			currentReturnView.setText(currentInvestment.getAmountReturnWF());
		}

		View profitableImageView = rootView.findViewById(R.id.trading_options_profitable_image);
		View currentInfoLayout = rootView.findViewById(R.id.trading_options_current_info_layout);
		ImageView putCallIcon = (ImageView) rootView.findViewById(R.id.trading_options_put_call_icon);

		if ((isInvestmentOpen && InvestmentUtil.isOpenInvestmentProfitable(currentInvestment,currentInvestment.getCurrentLevel())) || (!isInvestmentOpen && InvestmentUtil.isSettledInvestmentProfitable(currentInvestment, currency))) {
			profitableImageView.setVisibility(View.VISIBLE);
			currentInfoLayout.setBackgroundColor(ColorUtils.getColor(R.color.trading_options_in_the_money));
			setPutCallIcon(currentInvestment, putCallIcon, true);
		} else {
			profitableImageView.setVisibility(View.GONE);
			currentInfoLayout.setBackgroundColor(ColorUtils.getColor(R.color.trading_options_out_of_the_money));
			setPutCallIcon(currentInvestment, putCallIcon, false);
		}

		TextView levelView = (TextView) rootView.findViewById(R.id.trading_options_level);

		if (isInvestmentOpen) {
			levelView.setText(currentInvestment.getCurrentLevelTxt());
		} else {
			// Settled options
			levelView.setText(currentInvestment.getExpiryLevel());
			boolean isCanceled 		= InvestmentUtil.isCanceled(currentInvestment);
			boolean isRollForward 	= InvestmentUtil.isRollForward(currentInvestment);
			boolean isTakeProfit 	= InvestmentUtil.isTakeProfit(currentInvestment);

			if(!isDynamics){
				if (isRollForward) {
					// ROLL FORWARD
					levelView.setText(resources.getString(R.string.rollForwardLabel));
					currentReturnView.setText(resources.getString(R.string.cancelled));
				} else if (isTakeProfit) {
					// TAKE PROFIT
					levelView.setText(resources.getString(R.string.takeProfitLabel));
				}
			}

			if (currentInvestment.getOpportunityTypeId() == Opportunity.TYPE_OPTION_PLUS) {
				if (currentInvestment.getTimeSettled() != null && currentInvestment.getTimeSettled().before(currentInvestment.getTimeEstClosing())){
					levelView.setText(resources.getString(R.string.optionPlus));
				}
			}else if(isDynamics){
				if (currentInvestment.getTimeSettled() != null && currentInvestment.getTimeSettled().before(currentInvestment.getTimeEstClosing())){
					levelView.setText(resources.getString(R.string.dynamics));
				}
			}

			//Check if the Investment has been Canceled:
			if(isCanceled && !isRollForward && !isTakeProfit){
				levelView.setText(resources.getString(R.string.cancelled));
			}

		}

		return rootView;
	}

	@Override
	public int getCount() {
		return investments.size();
	}

	@Override
	public Object getItem(int position) {
		if (position < investments.size() && position >= 0) {
			return investments.get(position);
		}

		return null;
	}

	@Override
	public long getItemId(int position) {
		if (position < investments.size() && position >= 0) {
			return investments.get(position).getId();
		}

		return 0;
	}

	protected void formatDynamicsProfit(TextView textView, Investment currentInvestment){
		//Format Profit Percent. Example : (+26.23%)
		if(currentInvestment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY || currentInvestment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_SELL){
			double profitPercent = InvestmentUtil.getSettledInvestmentProfitPercent(currentInvestment);
			String sign = "";
			if(profitPercent>0){
				sign = "+";
			}else if(profitPercent<0){
				sign = "-";
			}
			String percentFormatted = " (" + sign + percentDecimalFormat.format(profitPercent) + "%)";
			String amountAndPercent = AmountUtil.insertIntervalBetweenAmountAndSymbol(currentInvestment.getAmountTxt(), application.getUser().getCurrency()) + percentFormatted;
			textView.setText(amountAndPercent);
			int percentTextSize 	= (int) (textView.getTextSize() * 6 / 7);
			TextUtils.decorateInnerText(textView, null, amountAndPercent, percentFormatted, null, 0, percentTextSize);
		}
	}

	@SuppressWarnings("static-method")
	protected void setPutCallIcon(Investment investment, ImageView view, boolean isProfitable) {
		long investmentType = investment.getTypeId();

		boolean isBubbles = investmentType == Investment.INVESTMENT_TYPE_BUBBLES;

		if(isBubbles){
			view.setVisibility(View.GONE);
		}else{
			view.setVisibility(View.VISIBLE);
			if ((investmentType == Investment.INVESTMENT_TYPE_CALL || investmentType == Investment.INVESTMENT_TYPE_DYNAMICS_BUY) && isProfitable) {
				view.setImageResource(R.drawable.trading_options_call_win);
			} else if ((investmentType == Investment.INVESTMENT_TYPE_CALL  || investmentType == Investment.INVESTMENT_TYPE_DYNAMICS_BUY) && !isProfitable) {
				view.setImageResource(R.drawable.trading_options_call_lose);
			} else if ((investmentType == Investment.INVESTMENT_TYPE_PUT || investmentType == Investment.INVESTMENT_TYPE_DYNAMICS_SELL) && isProfitable) {
				view.setImageResource(R.drawable.trading_options_put_win);
			} else {
				view.setImageResource(R.drawable.trading_options_put_lose);
			}
		}

	}

	public List<Investment> getInvestments() {
		return investments;
	}

	public void setInvestments(List<Investment> investments) {
		this.investments = investments;
	}

}
