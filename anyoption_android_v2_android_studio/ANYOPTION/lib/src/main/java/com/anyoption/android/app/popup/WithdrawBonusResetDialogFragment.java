package com.anyoption.android.app.popup;

import com.anyoption.android.R;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.TextUtils;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class WithdrawBonusResetDialogFragment extends BaseDialogFragment implements OnClickListener{
	
	private static final String TAG = WithdrawBonusResetDialogFragment.class.getSimpleName();

	protected WithdrawBonusResetPopupListener listener;
	protected View yesButton;
	protected View noButton;
	
	public interface WithdrawBonusResetPopupListener{
		void onYes();
		void onNo();
	}
	
	public static WithdrawBonusResetDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new WithdrawBonusResetDialogFragment.");
		WithdrawBonusResetDialogFragment fragment = new WithdrawBonusResetDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	protected int getContentLayout() {
		return 0;
	}

	@Override
	public void onClick(View view) {
		if(view == yesButton){
			if(listener != null){
				listener.onYes();
			}
		}else if(view == noButton){
			if(listener != null){
				listener.onNo();
			}
		}
	}

	public WithdrawBonusResetPopupListener getListener() {
		return listener;
	}

	public void setListener(WithdrawBonusResetPopupListener listener) {
		this.listener = listener;
	}
	
	
}
