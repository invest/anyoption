package com.anyoption.android.app.util;

import java.io.ByteArrayOutputStream;
import java.lang.ref.WeakReference;

import com.anyoption.android.app.Application;
import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.widget.ImageView;

/**
 * Utility class that provides useful methods for handling bitmaps.
 * @author Anastas Arnaudov
 *
 */
public class BitmapUtils {

	public static final String TAG = BitmapUtils.class.getSimpleName();
	
	/**
	 * A Bitmap Cache.
	 */
	protected static LruCache<String, Bitmap> bitmapCache;
	
	//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	//&&&&&&&&&&&&&&&					PUBLIC METHODS						&&&&&&&&&&&&&&&&&%%
	//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	/**
	 * Loads a Bitmap for the given resource id into the given ImageView.
	 * @param resId
	 * @param bitmap 
	 * @param imageView
	 */
	public static void loadBitmap(int resId, ImageView imageView) {
		Bitmap bitmap = null;
		BitmapUtils.loadBitmap(resId, bitmap, imageView);
	}
	
	/**
	 * Loads a Bitmap for the given resource id into the given ImageView.
	 * @param resId
	 * @param bitmap Reference
	 * @param imageView
	 */
	public static void loadBitmap(int resId, Bitmap bitmap, ImageView imageView) {
		BitmapUtils.initBitmapCache();
		final String imageKey = String.valueOf(resId);

	    bitmap = BitmapUtils.getBitmapFromMemCache(imageKey);
	    if (bitmap != null) {
	    	imageView.setImageBitmap(bitmap);
	    }else{
	    	if (BitmapUtils.cancelPotentialWork(resId, imageView)) {
	    		final BitmapWorkerTask task = new BitmapWorkerTask(imageView);
	    		final AsyncDrawable asyncDrawable = new AsyncDrawable(Application.get().getResources(), bitmap, task);
	    		imageView.setImageDrawable(asyncDrawable);
	    		task.execute(resId);
	    	}	    	
	    }
	}
	
	public static byte[] convertBitmapToByteArray(Bitmap bitmap){
		ByteArrayOutputStream baos = new ByteArrayOutputStream();  
		bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
		byte[] byteArr = baos.toByteArray(); 
		return byteArr;
	}
	
	/**
	 * Clears the Bitmap Cache.
	 */
	public static void clearCache(){
		if(bitmapCache != null){
			bitmapCache.evictAll();
		}
	}
	//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

	/**
	 * Creates the Bitmap Cache.
	 */
	protected static void initBitmapCache(){
		if(BitmapUtils.bitmapCache == null){
			// Get max available VM memory, exceeding this amount will throw an
		    // OutOfMemory exception. Stored in kilobytes as LruCache takes an
		    // int in its constructor.
		    final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

		    // Use 1/8th of the available memory for this memory cache.
		    final int cacheSize = maxMemory / 8;//TODO analyze

		    bitmapCache = new LruCache<String, Bitmap>(cacheSize) {
		        @Override
		        protected int sizeOf(String key, Bitmap bitmap) {
		            // The cache size will be measured in kilobytes rather than
		            // number of items.
		            return bitmap.getByteCount() / 1024;
		        }
		    };
		}
	}
	
	/**
	 * Adds the Bitmap to the Bitmap Cache.
	 * @param key
	 * @param bitmap
	 */
	protected static void addBitmapToMemoryCache(String key, Bitmap bitmap) {
	    if (BitmapUtils.getBitmapFromMemCache(key) == null) {
	    	if(key != null && bitmap != null){
	    		BitmapUtils.bitmapCache.put(key, bitmap);
	    	}else{
	    		Log.e(TAG, "addBitmapToMemoryCache ---> key or value is null");
	    	}
	    }
	}

	/**
	 * Returns the Bitmap for this key from the Bitmap Cache.
	 * @param key
	 * @return
	 */
	protected static Bitmap getBitmapFromMemCache(String key) {
	    return BitmapUtils.bitmapCache.get(key);
	}
	
	/**
	 * Calculates the "inSampleSize" property of the Bitmap Options.
	 * If image is going to be displayed in its real size then don't use method (inSampleSize = 1)
	 * @param options
	 * @param reqWidth
	 * @param reqHeight
	 * @return
	 */
	protected static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
	    // Raw height and width of image
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;
	
	    if (height > reqHeight || width > reqWidth) {
	
	        final int halfHeight = height / 2;
	        final int halfWidth = width / 2;
	
	        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
	        // height and width larger than the requested height and width.
	        while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
	            inSampleSize *= 2;
	        }
	    }
	
	    return inSampleSize;
	}
	
	/**
	 * Decodes the Bitmap for the given resource id.
	 * If image is going to be displayed in its real size then use {@link #decodeSampledBitmapFromResource(Resources, int)}
	 * @param res
	 * @param resId
	 * @param reqWidth
	 * @param reqHeight
	 * @return
	 */
	protected static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {

	    // First decode with inJustDecodeBounds=true to check dimensions
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeResource(res, resId, options);

	    // Calculate inSampleSize
	    options.inSampleSize = BitmapUtils.calculateInSampleSize(options, reqWidth, reqHeight);

	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	    return BitmapFactory.decodeResource(res, resId, options);
	}
	
	/**
	 * Decodes the Bitmap for the given resource id.
	 * The image is going to be in its real size.
	 * @param res
	 * @param resId
	 * @return
	 */
	protected static Bitmap decodeSampledBitmapFromResource(Resources res, int resId) {

	    // First decode with inJustDecodeBounds=true to check dimensions
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeResource(res, resId, options);

	    // The inSampleSize is 1 as we do not scale the image:
	    options.inSampleSize = 1;

	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	    return BitmapFactory.decodeResource(res, resId, options);
	}
	
	/**
	 * Cancel the {@link BitmapWorkerTask} of the given ImageView.
	 * @param data
	 * @param imageView
	 * @return
	 */
	protected static boolean cancelPotentialWork(int data, ImageView imageView) {
	    final BitmapWorkerTask bitmapWorkerTask = BitmapUtils.getBitmapWorkerTask(imageView);

	    if (bitmapWorkerTask != null) {
	        final int bitmapData = bitmapWorkerTask.data;
	        // If bitmapData is not yet set or it differs from the new data
	        if (bitmapData == 0 || bitmapData != data) {
	            // Cancel previous task
	            bitmapWorkerTask.cancel(true);
	        } else {
	            // The same work is already in progress
	            return false;
	        }
	    }
	    // No task associated with the ImageView, or an existing task was cancelled
	    return true;
	}
	
	/**
	 * Returns the Worker for this ImageView.
	 * @param imageView
	 * @return
	 */
	protected static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView) {
		   if (imageView != null) {
		       final Drawable drawable = imageView.getDrawable();
		       if (drawable instanceof AsyncDrawable) {
		           final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
		           return asyncDrawable.getBitmapWorkerTask();
		       }
		    }
		    return null;
	}
	

	/**
	 * Worker that decodes the bitmap on the background and adds it to the ImageView. 
	 * @author Anastas Arnaudov
	 *
	 */
	static class BitmapWorkerTask extends AsyncTask<Integer, Void, Bitmap>{

		private final WeakReference<ImageView> imageViewReference;
	    private int data = 0;
		
	    public BitmapWorkerTask(ImageView imageView) {
	        // Use a WeakReference to ensure the ImageView can be garbage collected
	        imageViewReference = new WeakReference<ImageView>(imageView);
	    }

		@Override
		protected Bitmap doInBackground(Integer... params) {
			data = params[0];
			final Bitmap bitmap = BitmapUtils.decodeSampledBitmapFromResource(Application.get().getResources(), data);
			BitmapUtils.addBitmapToMemoryCache(String.valueOf(params[0]), bitmap);
	        return BitmapUtils.decodeSampledBitmapFromResource(Application.get().getResources(), data);
		}

		// Once complete, see if ImageView is still around and set bitmap.
	    @Override
	    protected void onPostExecute(Bitmap bitmap) {
	        if (isCancelled()) {
	            bitmap = null;
	        }

	        if (imageViewReference != null && bitmap != null) {
	            final ImageView imageView = imageViewReference.get();
	            final BitmapWorkerTask bitmapWorkerTask = BitmapUtils.getBitmapWorkerTask(imageView);
	            if (this == bitmapWorkerTask && imageView != null) {
	                imageView.setImageBitmap(bitmap);
	            }
	        }
	    }
		
	}

	/**
	 * Custom {@link BitmapDrawable} that keeps weak reference to its {@link BitmapWorkerTask}.
	 * @author Anastas Arnaudov
	 *
	 */
	static class AsyncDrawable extends BitmapDrawable {
	    private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;

	    public AsyncDrawable(Resources res, Bitmap bitmap, BitmapWorkerTask bitmapWorkerTask) {
	        super(res, bitmap);
	        bitmapWorkerTaskReference = new WeakReference<BitmapWorkerTask>(bitmapWorkerTask);
	    }

	    public BitmapWorkerTask getBitmapWorkerTask() {
	        return bitmapWorkerTaskReference.get();
	    }
	}
	
	public static Bitmap resizeAndScaleDocumentBitmap(Bitmap bitmap) {
		try {
			Bitmap result = bitmap;
			int size = byteSizeOf(result);
			Log.e(TAG, "Bitmap, size: " + size);
			while(size > 700 || bitmap == null) {
				result = Bitmap.createScaledBitmap(result, (int)(result.getWidth()/1.1), (int)(result.getHeight()/1.1), true);
				
				
				/* TODO COMPRESSING BITMAP
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				result.compress(Bitmap.CompressFormat.JPEG, 100, out);
				result = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
				*/
				
				size = byteSizeOf(result);
				Log.e(TAG, "Resizing bitmap, current size: " + size);
			}
			return result;
		} catch (IllegalArgumentException e) {
			Log.e(TAG, "resizeBitmap()", e);
		}
		return bitmap;
	}
	
	@SuppressLint("NewApi")
	public static int byteSizeOf(Bitmap bitmap) {
	    if (Build.VERSION.SDK_INT >= 19) {
	        return bitmap.getAllocationByteCount()/22000;
	    } else if (Build.VERSION.SDK_INT >= 12) {
	        return bitmap.getByteCount()/22000;
	    } else {
	        return (bitmap.getRowBytes() * bitmap.getHeight())/22000;
	    }
	}
	
}
