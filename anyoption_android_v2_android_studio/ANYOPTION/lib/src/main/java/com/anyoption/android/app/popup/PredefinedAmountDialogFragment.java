package com.anyoption.android.app.popup;

import java.text.DecimalFormat;
import java.util.List;

import com.anyoption.android.R;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.BalanceStepPredefValue;
import com.anyoption.common.beans.base.Currency;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

/**
 * Fullscreen Dialog Fragment used to change invest amount.
 * 
 * @author Mario Kutlev
 */
public class PredefinedAmountDialogFragment extends BaseDialogFragment {

	private static final String SELECTED_AMOUNT_ARG = "selected_amount_arg";

	public interface OnAmountChangeListener {

		/**
		 * Called when the user submits the new invest amount with OK.
		 */
		public void onAmountChanged(double newAmount);

	}

	// In this layout we add predefined amount items.
	private LinearLayout investAmountsLayout;
	private EditText customAmountEditText;

	private Currency currency;
	private double selectedAmount;
	
	private String hintText; 

	private OnAmountChangeListener onAmountChangeListener;

	public static PredefinedAmountDialogFragment newInstance(Bundle bundle) {
		PredefinedAmountDialogFragment fragment = new PredefinedAmountDialogFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	public static PredefinedAmountDialogFragment newInstance(double selectedAmount) {
		PredefinedAmountDialogFragment fragment = new PredefinedAmountDialogFragment();
		Bundle bundle = new Bundle();
		bundle.putDouble(SELECTED_AMOUNT_ARG, selectedAmount);
		fragment.setArguments(bundle);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		currency = application.getCurrency();
		selectedAmount = getArguments().getDouble(SELECTED_AMOUNT_ARG, 0);
	}

	@Override
	protected void onCreateContentView(View contentView) {
		customAmountEditText = (EditText) contentView.findViewById(R.id.custom_amount_edit_text);
		customAmountEditText.setImeActionLabel(getString(R.string.ok), EditorInfo.IME_ACTION_DONE);
		customAmountEditText.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					ScreenUtils.hideKeyboard(customAmountEditText);
					chooseAmount();
				}
				return true;
			}
		});

		hintText = (String) customAmountEditText.getHint();
		customAmountEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					customAmountEditText.setText("");
					customAmountEditText.setHint("");
				} else {
					ScreenUtils.hideKeyboard(customAmountEditText);
					if (customAmountEditText.getText().toString().isEmpty()) {
						customAmountEditText.setHint(hintText);
					}
				}
			}
		});

		investAmountsLayout = (LinearLayout) contentView
				.findViewById(R.id.predefined_amount_items_layout);

		setUpInvestAmounts();

		View okButton = contentView.findViewById(R.id.predefined_amount_ok_button);
		okButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ScreenUtils.hideKeyboard(customAmountEditText);
				chooseAmount();
			}
		});

		OnClickListener closeListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				ScreenUtils.hideKeyboard(customAmountEditText);
				dismiss();
			}
		};

		contentView.findViewById(R.id.predefined_amount_scroll_view).setOnClickListener(
				closeListener);
		contentView.findViewById(R.id.predefined_amount_keyboard_offset).setOnClickListener(closeListener);
	}

	@Override
	protected int getContentLayout() {
		return R.layout.predefined_amount_dialog;
	}

	private void chooseAmount() {
		View chosen = investAmountsLayout.getFocusedChild();
		if (chosen == null) {
			// Custom amount is chosen.
			chosen = customAmountEditText;
		}

		double amount = 0;
		if (chosen instanceof EditText) {
			String amountStr = customAmountEditText.getText().toString();
			if (!amountStr.isEmpty()) {
				amount = Double.parseDouble(amountStr);
				// TODO Validate the amount - it must have minimum value and maximum value!
			} else {
				dismiss();
				return;
			}
		} else {
			// We get the tag set during the creation of the view.
			amount = (Double) chosen.getTag();
		}

		// Persist the chosen invest amount
		application.setDefaultInvestAmount(amount);

		if (onAmountChangeListener != null) {
			onAmountChangeListener.onAmountChanged(amount);
		}

		dismiss();
	}

	private void setUpInvestAmounts() {
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);

		// Read the invest amounts
		List<BalanceStepPredefValue> predefinedInvestAmounts = application
				.getPredefinedInvestAmounts();
		double[] investAmounts = new double[predefinedInvestAmounts.size()];
		for (int i = 0; i < predefinedInvestAmounts.size(); i++) {
			investAmounts[i] = AmountUtil.getDoubleAmount(predefinedInvestAmounts.get(i)
					.getPredefValue());
		}

		boolean isItemSelected = false;

		// Add the items to the layout
		for (int i = 0; i < investAmounts.length; i++) {
			View item = inflater.inflate(R.layout.predefined_amount_item, investAmountsLayout,
					false);
			item.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View paramView) {
					ScreenUtils.hideKeyboard(customAmountEditText);
				}
			});

			if (investAmounts[i] == selectedAmount) {
				item.requestFocus();
				isItemSelected = true;
			}

			item.setTag(investAmounts[i]);
			TextView amountView = (TextView) item.findViewById(R.id.predefined_amount_text_view);
			amountView.setText(AmountUtil.getFormattedAmount(investAmounts[i],
					currency.getSymbol(), currency.getIsLeftSymbolBool(), 0, currency.getId()));
			investAmountsLayout.addView(item);
		}

		if (!isItemSelected) {
			DecimalFormat format = Utils.getDecimalFormat(currency.getDecimalPointDigits());
			format.setGroupingUsed(false);
			customAmountEditText.setText(format.format(selectedAmount));
			customAmountEditText.requestFocus();
		}
	}

	public OnAmountChangeListener getOnAmountChangeListener() {
		return onAmountChangeListener;
	}

	/**
	 * Set an event listener on this fragment.
	 */
	public void setOnAmountChangeListener(OnAmountChangeListener onAmountChangeListener) {
		this.onAmountChangeListener = onAmountChangeListener;
	}

}
