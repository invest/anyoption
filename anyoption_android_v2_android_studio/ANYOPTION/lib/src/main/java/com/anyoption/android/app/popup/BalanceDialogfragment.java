package com.anyoption.android.app.popup;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class BalanceDialogfragment extends BaseDialogFragment {
	public static final String TAG = BalanceDialogfragment.class.getSimpleName();

	public static BalanceDialogfragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new BalanceDialogfragment.");
		BalanceDialogfragment fragment = new BalanceDialogfragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	protected void onCreateContentView(View contentView) {
		View noButton = contentView.findViewById(R.id.balance_popup_negative_button);
		View yesButton = contentView.findViewById(R.id.balance_popup_positive_button);
		
		noButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
		
		yesButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				Application.get().postEvent(new NavigationEvent(Screen.DEPOSIT_MENU,NavigationType.INIT));
			}
		});
		
//		setCancelable(false);
	}

	@Override
	protected int getContentLayout() {
		return R.layout.balance_popup_layout;
	}

}
