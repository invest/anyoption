package com.anyoption.android.app.event;

import com.anyoption.android.app.fragment.trade.AssetsListFragment;

/**
 * Custom Event that occurs when asset list filter is changed.
 */
public class AssetsListFilterChangeEvent {

	private String filter;

	public AssetsListFilterChangeEvent(String filter) {
		this.filter = filter;
	}

	/**
	 * Returns the filter which has to be one of {@link AssetsListFragment#FILTER_BINARY},
	 * {@link AssetsListFragment#FILTER_OPTION_PLUS} or {@link AssetsListFragment#FILTER_NONE}.
	 * 
	 * @return
	 */
	public String getFilter() {
		return filter;
	}

	/**
	 * Set the filter to one of {@link AssetsListFragment#FILTER_BINARY},
	 * {@link AssetsListFragment#FILTER_OPTION_PLUS} or {@link AssetsListFragment#FILTER_NONE}.
	 * 
	 * @param filter
	 */
	public void setFilter(String filter) {
		this.filter = filter;
	}

}
