package com.anyoption.android.app.popup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.manager.PopUpManager;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.requests.UserMethodRequest;

public class WithdrawSurveyConfirmationDialogFragment extends OkDialogFragment {

	public static final String TAG = WithdrawSurveyConfirmationDialogFragment.class.getSimpleName();
	
	protected TextView messageTextView;
	
	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static WithdrawSurveyConfirmationDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new LowBalanceOkDialogFragment.");
		WithdrawSurveyConfirmationDialogFragment fragment = new WithdrawSurveyConfirmationDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if(args != null){

		}
		setCancelable(args.getBoolean(Constants.EXTRA_CANCELABLE, false));
	}

	@Override
	protected int getContentLayout() {
		return R.layout.popup_withdraw_survey_confirmation;
	}

	/**
	 * Set up content.
	 * 
	 * @param contentView
	 */
	@Override
	protected void onCreateContentView(View contentView) {
		messageTextView = (com.anyoption.android.app.widget.TextView) contentView.findViewById(R.id.withdraw_survey_confirmation_popup_message);

		TextUtils.decorateInnerText(messageTextView, null, messageTextView.getText().toString(), messageTextView.getText().toString(), FontsUtils.Font.ROBOTO_MEDIUM, 0, 0);

		View closeButton = contentView.findViewById(R.id.close_button);
		closeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}

}
