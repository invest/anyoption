package com.anyoption.android.app.widget.form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.LayoutDirection;
import com.anyoption.android.app.event.LayoutDirectionChangeEvent;
import com.anyoption.android.app.util.ScreenUtils;

import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

/**
 * Custom Form class.
 * <p>It manages {@link FormItem}
 * @author Anastas Arnaudov
 *
 */
public class Form{
	
	public static final String TAG = Form.class.getSimpleName();
	
	/**
	 * Listens for submit events.
	 * @author Anastas Arnaudov
	 *
	 */
	public interface OnSubmitListener{
		public void onSubmit();
	}
	
	private LayoutDirection direction;
	private OnEditorActionListener onEditorActionListener;
	private OnSubmitListener onSubmitListener;
	private boolean isEditable = true;	
	private HashMap<String,FormItem> itemFields;

	/**
	 * List with {@link FormItem}.
	 */
	private List<FormItem> items;

	/**
	 * Create new {@link Form}.
	 */
	public Form(){
		init();
	}	

	/**
	 * Create new {@link Form}.
	 */
	public Form(OnSubmitListener onSubmitListener){
		this();
		setOnSubmitListener(onSubmitListener);
	}
	
	
	/**
	 * Initial setup.
	 */
	private void init() {
		setItems(new ArrayList<FormItem>());
		itemFields = new HashMap<String, FormItem>();
		setupDirection(Application.get().getLayoutDirection());
		this.onEditorActionListener = new OnEditorActionListener() {	
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId == EditorInfo.IME_ACTION_DONE){
					//submit();
					ScreenUtils.hideKeyboard(Application.get().getCurrentEditTextOnFocus());
				}
				return false;
			}
		};
		Application.get().registerForEvents(LayoutDirectionChangeEvent.class);
	}
	
	/**
	 * Adds {@link FormItem} to this Form.
	 * @param items
	 */
	public void addItem(FormItem... items){
		for(FormItem item : items){
			if(!this.getItems().contains(item)){
				this.getItems().add(item);
				if(item instanceof FormEditText && ((FormEditText)item).hasEditorActionListener()){
					//Do not add listener//TODO perfect it : add custom listeners
				}else{
					item.addOnEditorActionListener(onEditorActionListener);					
				}
				item.setDirection(direction);
				this.getItemFields().put(item.getFieldName(), item);				
			}
		}
	}
	
	/**
	 * Removes the {@link FormItem} from this Form.
	 * @param item
	 */
	public void removeItem(FormItem item){
		this.getItems().remove(item);
	}

	/**
	 * Returns the items list.
	 * @return
	 */
	public List<FormItem> getItems() {
		return items;
	}

	/**
	 * Sets the items list.
	 * @param items
	 */
	private void setItems(List<FormItem> items) {
		this.items = items;
	}
	
	/**
	 * @return the itemFields
	 */
	public HashMap<String, FormItem> getItemFields() {
		return itemFields;
	}

	/**
	 * @param itemFields the itemFields to set
	 */
	public void setItemFields(HashMap<String, FormItem> itemFields) {
		this.itemFields = itemFields;
	}

	/**
	 * Checks Forms validity. It checks each form item for validity.
	 * @return
	 */
	public boolean checkValidity(){
		boolean isValid = true;
		for(FormItem item : items){
			if(!item.checkValidity()){
				isValid = false;				
			}
		}
		return isValid;
	}

	/**
	 * Clears the Form.
	 */
	public void clear(){
		for(FormItem item : items){
			item.clear();
		}
	}
	
	/**
	 * Sets the OnSubmitListener.
	 * @param onSubmitListener
	 */
	public void setOnSubmitListener(OnSubmitListener onSubmitListener){
		this.onSubmitListener = onSubmitListener;
	}
	
	/**
	 * Checks whether all items are valid and the form is editable and then executes the logic set in the OnSubmitListener.
	 */
	public boolean submit(){
		if(isEditable() && checkValidity()){
			if(onSubmitListener != null){
				onSubmitListener.onSubmit();
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the Layout Direction of the form.
	 * @return
	 */
	public LayoutDirection getDirection() {
		return direction;
	}

	/**
	 * Sets the direction. The default is LEFT_TO_RIGHT.
	 * This sets the direction of all Form Items.
	 * @param direction
	 */
	public void setupDirection(LayoutDirection direction) {
		this.direction = direction;
		for(FormItem item : items){
			item.setDirection(direction);
		}
	}
	
	/**
	 * Checks whether the form can be edit or not.
	 * @return
	 */
	public boolean isEditable() {
		return isEditable;
	}
	
	/**
	 * Sets whether the form can be edit or not.
	 * It sets all items in  the form.
	 * @param isEditable
	 */
	public void setEditable(boolean isEditable) {
		this.isEditable = isEditable;
		for(FormItem item : items){
			item.setEditable(isEditable);
		}
	}
	
	/**
	 * Called when there is a {@link LayoutDirectionChangeEvent}.
	 * @param event
	 */
	public void onEventMainThread(LayoutDirectionChangeEvent event){
		Log.d(TAG, "on LayoutDirectionChangeEvent");
		setupDirection(event.getLayoutDirection());
	}
	
	/**
	 * Called when there is a {@link LayoutDirectionChangeEvent}.
	 * @param event
	 */
	public void onEventPostThread(LayoutDirectionChangeEvent event){
		Log.d(TAG, "on LayoutDirectionChangeEvent");
		setupDirection(event.getLayoutDirection());
	}
	
}
