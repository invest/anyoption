package com.anyoption.android.app.model;

public class ChartMarketRate {

	private float rate;
	private long time;

	public ChartMarketRate(float rate, long time) {
		this.rate = rate;
		this.time = time;
	}

	public float getRate() {
		return rate;
	}

	public void setRate(float rate) {
		this.rate = rate;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

}
