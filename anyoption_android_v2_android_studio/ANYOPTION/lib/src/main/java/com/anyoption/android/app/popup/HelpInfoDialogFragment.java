package com.anyoption.android.app.popup;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.ScreenUtils;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

/**
 * Custom Dialog that displays help info for the specific screen.
 * It contains title and message.
 * @author Anastas Arnaudov
 */
public class HelpInfoDialogFragment extends BaseDialogFragment {

	private static final String SCREEN_ARG_KEY 				= "screen_arg";
	private static final String IS_OPTION_PLUS_ARG_KEY 		= "is_opt_plus_arg";
	private static final String IS_DYNAMICS_ARG_KEY 		= "is_dynamics_arg";

	private View rootView;

	/**
	 * Use this method to get help info dialog for the given screen.
	 */
	public static HelpInfoDialogFragment newInstance(Bundle bundle) {
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);

		HelpInfoDialogFragment fragment = new HelpInfoDialogFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	/**
	 * Use this method to get help info dialog for the given screen.
	 *
	 * @param screen
	 * @param isOptionPlus
	 * @return
	 */
	public static HelpInfoDialogFragment newInstance(Screenable screen, boolean isOptionPlus, boolean isDynamics) {
		Bundle args = new Bundle();
		args.putBoolean(IS_OPTION_PLUS_ARG_KEY, isOptionPlus);
		args.putBoolean(IS_DYNAMICS_ARG_KEY, isDynamics);

		if (screen != null) {
			args.putSerializable(SCREEN_ARG_KEY, screen);
		}
		args.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);

		HelpInfoDialogFragment fragment = new HelpInfoDialogFragment();
		fragment.setArguments(args);
		return fragment;
	}

	/**
	 * Use this method to get help info dialog for the current screen.
	 *
	 * @param isOptionPlus
	 * @return
	 */
	public static HelpInfoDialogFragment newInstance(boolean isOptionPlus, boolean isDynamics) {
		return newInstance(null, isOptionPlus, isDynamics);
	}

	private View okButton;
	private String title = "";
	private String message = "";
	private TextView titleTextView;
	private WebView messageWebView;

	private boolean isOptionPlus;
	private boolean isDynamics;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NO_TITLE, R.style.FullscreenDialog);

		Screenable currentScreen;
		Bundle args = getArguments();
		if (args != null) {
			currentScreen = Application.get().getCurrentScreen();
			currentScreen = (Screenable) args.getSerializable(SCREEN_ARG_KEY);
			if (currentScreen == null) {
				currentScreen = Application.get().getCurrentScreen();
			}
			isOptionPlus = args.getBoolean(IS_OPTION_PLUS_ARG_KEY, false);
			isDynamics   = args.getBoolean(IS_DYNAMICS_ARG_KEY, false);
		} else {
			isOptionPlus = false;
			isDynamics   = false;
			currentScreen = Application.get().getCurrentScreen();
		}

		if(currentScreen == Screen.TRADE || currentScreen == Screen.TABLET_TRADE || currentScreen == Screen.ASSETS_LIST){
			if (isOptionPlus) {
				title = getString(R.string.assetPagePlusHelpTitle);
				message = getString(R.string.commonHelpInfoHtml, getString(R.string.assetPagePlusHelpContent));
			} else if(isDynamics){
				title = getString(R.string.assetDynamicsHelpTitle);
				message = getString(R.string.commonHelpInfoHtml, getString(R.string.assetPageDynamicsHelpContent));
			} else {
				title = getString(R.string.assetPageHelpTitle);
				message = getString(R.string.commonHelpInfoHtml, getString(R.string.assetPageHelpContent));
			}
		} else if (currentScreen == Screen.ASSET_VIEW_PAGER || currentScreen == Screen.ASSET_PAGE) {
			Activity activity = Application.get().getCurrentActivity();
			String image1 = getString(R.string.helpInfoCallIconPath) + " width=\"" + ScreenUtils.convertDpToPx(activity, 5) +"\"";
			String image2 = getString(R.string.helpInfoPutIconPath) + " width=\"" + ScreenUtils.convertDpToPx(activity, 5) +"\"";

			if (isOptionPlus) {
				title = getString(R.string.assetTradePlusHelpTitle);
				message = getString(R.string.commonHelpInfoHtml, getString(R.string.assetTradePlusHelpContent, image1, image2));
			} else if(isDynamics){
				title = getString(R.string.assetDynamicsHelpTitle);
				message = getString(R.string.commonHelpInfoHtml, getString(R.string.assetPageDynamicsHelpContent));
			} else {
				title = getString(R.string.assetTradeHelpTitle);
				message = getString(R.string.commonHelpInfoHtml, getString(R.string.assetTradeHelpContent, image1, image2));
			}
		}else if(currentScreen == Screen.BANKING){
			title = getString(R.string.bankingPageHelpTitle);
			message = getString(R.string.commonHelpInfoHtml, getString(R.string.bankingPageHelpContent));
		}else if(currentScreen == Screen.DEPOSIT || currentScreen == Screen.DEPOSIT_MENU){
			title = getString(R.string.depositHelpTitle);
			message = getString(R.string.commonHelpInfoHtml, getString(R.string.depositHelpContent));
		}else if(currentScreen == Screen.DOMESTIC_DEPOSIT || currentScreen == Screen.FIRST_NEW_CARD || currentScreen == Screen.EXISTING_CARD){
			title = getString(R.string.depositAllCardHelpTitle);
			message = getString(R.string.commonHelpInfoHtml, getString(R.string.depositAllCardHelpConent));
		}else if(currentScreen == Screen.WIRE_DEPOSIT){
			title = getString(R.string.wireTransferHelpTitle);
            String supportFax;
            if (Application.get().getCountriesMap().containsKey(Application.get().getUser().getCountryId())) {
                supportFax = Application.get().getCountriesMap().get(Application.get().getUser().getCountryId()).getSupportFax();
            } else {
                supportFax = getString(R.string.supportScreenDefaultFax);
            }
			message = getString(R.string.commonHelpInfoHtml, getString(R.string.wireTransferHelpContent, supportFax));
		}else if(currentScreen == Screen.WITHDRAW_MENU || currentScreen == Screen.CC_WITHDRAW || currentScreen == Screen.BANK_WIRE_WITHDRAW || currentScreen == Screen.WITHDRAW_CLOSED){
			title = getString(R.string.withdrawHelpTitle);
			message = getString(R.string.commonHelpInfoHtml, getString(R.string.withdrawHelpContent));
		}

	}

	@Override
	protected void onCreateContentView(View contentView) {
		rootView = contentView;

		titleTextView = (TextView) contentView.findViewById(R.id.help_dialog_title);
		titleTextView.setText(title);

		messageWebView = (WebView) contentView.findViewById(R.id.help_dialog_message);
		messageWebView.setBackgroundColor(ColorUtils.getColor(R.color.help_info_dialog_webview_background));
		messageWebView.loadDataWithBaseURL(null, message, "text/html", "UTF-8", null);

		okButton = contentView.findViewById(R.id.help_dialog_ok_button);
		okButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}

	@Override
	protected int getContentLayout() {
		return R.layout.help_dialog_layout;
	}

}
