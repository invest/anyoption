package com.anyoption.android.app.popup;

import android.view.View;

import com.anyoption.android.R;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.widget.TextView;

/**
 * @author Anastas Arnaudov
 */
public class ExistingEmailDialogfragment extends BaseDialogFragment  implements View.OnClickListener  {

    public interface ExistingEmailPopupListener{
        void onChangeEmailButtonPressed();
        void onRecoverPassButtonPressed();
    }

    protected ExistingEmailPopupListener existingEmailPopupListener;

    protected View mainLayout;
    protected TextView messageTextView;
    protected View changeEmailButton;
    protected View recoverPassButton;

    @Override
    protected int getContentLayout() {
        return 0;
    }

    @Override
    protected void onCreateContentView(View contentView) {
        TextUtils.underlineInnerText(messageTextView, null, messageTextView.getText().toString(), getString(R.string.existingEmailPopupMessage2Underline));
        mainLayout.setOnClickListener(null);
        changeEmailButton.setOnClickListener(this);
        recoverPassButton.setOnClickListener(this);
    }

    public void setExistingEmailPopupListener(ExistingEmailPopupListener existingEmailPopupListener){
        this.existingEmailPopupListener = existingEmailPopupListener;
    }

    @Override
    public void onClick(View v) {
        if(v == closeButton){
            dismiss();
        }else if(v == changeEmailButton){
            dismiss();
            onChangeEmail();
        }else if(v == recoverPassButton){
            dismiss();
            onRecoverPass();
        }
    }

    protected void onChangeEmail(){
        existingEmailPopupListener.onChangeEmailButtonPressed();
    }

    protected void onRecoverPass(){
        existingEmailPopupListener.onRecoverPassButtonPressed();
    }

}
