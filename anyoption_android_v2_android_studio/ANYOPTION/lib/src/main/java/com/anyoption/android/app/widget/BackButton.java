package com.anyoption.android.app.widget;

import com.anyoption.android.app.Application;
import com.anyoption.android.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

public class BackButton extends RelativeLayout {

	public BackButton(Context context) {
		super(context);
		init(context, null, 0);
	}

	public BackButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}

	public BackButton(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		View.inflate(context,R.layout.back_button_layout, this);
		setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Application.get().getCurrentActivity().onBackPressed();
			}
		});
	}


}
