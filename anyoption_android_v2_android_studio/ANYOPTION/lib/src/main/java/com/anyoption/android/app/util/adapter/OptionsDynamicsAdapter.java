package com.anyoption.android.app.util.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.fragment.trade.AssetOptionsDynamicsFragment;
import com.anyoption.android.app.model.OptionDynamicsGroup;
import com.anyoption.android.app.model.OptionDynamicsItem;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.widget.TextView;
import com.anyoption.common.beans.base.Currency;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author Anastas Arnaudov
 */
public class OptionsDynamicsAdapter extends BaseExpandableListAdapter implements View.OnClickListener {

    public static final String TAG = OptionsDynamicsAdapter.class.getSimpleName();

    public interface CloseOptionListener{
        void onCloseOptionButtonClicked(OptionDynamicsItem item);
        void onCloseOptionClosed();
    }

    public static class GroupViewHolder {
        public int position;
        public TextView titleTextView;
        public View subtitleOpenOptionsLayout;
        public View dynamicsOptionsTitleClosingView;
    }

    public static class ItemViewHolder {
        public int groupPosition;
        public int position;

        //Open options items:
        public ViewGroup dynamicsOpenOptionLayout;
        public TextView optionClosedTextView;
        public View investInfoLayout;
        public TextView timeTextView;
        public TextView amountTextView;
        public TextView returnTextView;
        public TextView closeButton;
        public View separator1;
        public View separator2;
        public ImageView inOutTheMoneyView;

        //Settled options items:
        public ViewGroup dynamicsSettledOptionLayout;
        public TextView expiredTextView;
        public TextView purchasedTextView;
        public TextView closingTextView;
        public ImageView settledInOutTheMoneyView;
    }

    private Application application;
    private final Drawable callIconDrawable;
    private final Drawable putIconDrawable;
    private SpannableStringBuilder groupSpannable;
    private SpannableStringBuilder childSpannable;
    private List<OptionDynamicsGroup> optionsGroupsList;
    private LayoutInflater inflater;
    private final int currencyTextSize;
    private final int closeForAmountTextSize;
    private SimpleDateFormat purchaseTimeFormat;
    private SimpleDateFormat expireTimeFormat;
    private Currency currency;
    private DecimalFormat percentDecimalFormat;
    private CloseOptionListener closeOptionListener;

    public OptionsDynamicsAdapter() {
        application = Application.get();
        currency = application.getCurrency();
        this.optionsGroupsList = new ArrayList<OptionDynamicsGroup>();
        inflater = (LayoutInflater) Application.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        percentDecimalFormat = new DecimalFormat("##.##");
        purchaseTimeFormat = new SimpleDateFormat("HH:mm");
        expireTimeFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm", Locale.US);
        groupSpannable = new SpannableStringBuilder();
        childSpannable = new SpannableStringBuilder();
        callIconDrawable = DrawableUtils.getDrawable(R.drawable.call_green_icon);
        putIconDrawable = DrawableUtils.getDrawable(R.drawable.put_red_icon);
        currencyTextSize = application.getResources().getDimensionPixelSize(R.dimen.asset_page_dynamics_options_list_item_text_size) * 4 / 5;
        closeForAmountTextSize = application.getResources().getDimensionPixelSize(R.dimen.asset_page_dynamics_invest_details_close_button_amount_text_size);
    }

    public void setCloseOptionListener(CloseOptionListener closeOptionListener){
        this.closeOptionListener = closeOptionListener;
    }

    public void setGroups(List<OptionDynamicsGroup> optionsGroupsList) {
        this.optionsGroupsList = optionsGroupsList;
        notifyDataSetChanged();
    }

    public List<OptionDynamicsGroup> getGroups() {
       return this.optionsGroupsList;
    }

    @Override
    public int getGroupCount() {
        return optionsGroupsList.size();
    }

	@Override
    public int getChildrenCount(int groupPosition) {
        OptionDynamicsGroup group = optionsGroupsList.get(groupPosition);
        int size = group != null ? group.getItemsList().size() : 0;
        return size;
    }

	@Override
    public OptionDynamicsGroup getGroup(int groupPosition) {
        return optionsGroupsList.get(groupPosition);
    }

	@Override
    public OptionDynamicsItem getChild(int groupPosition, int childPosition) {
        return optionsGroupsList.get(groupPosition).getItemsList().get(childPosition);
    }

	@Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

	@Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

	@Override
    public boolean hasStableIds() {
        return true;
    }

	@Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

	@Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        OptionDynamicsGroup group = optionsGroupsList.get(groupPosition);
        GroupViewHolder groupViewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.asset_page_dynamics_options_group, parent, false);
            groupViewHolder = new GroupViewHolder();

            groupViewHolder.titleTextView                   = (TextView) convertView.findViewById(R.id.dynamicsOptionsTitleTextView);
            groupViewHolder.subtitleOpenOptionsLayout       =            convertView.findViewById(R.id.dynamicsOpenOptionsSubtitleLayout);
            groupViewHolder.dynamicsOptionsTitleClosingView =            convertView.findViewById(R.id.dynamicsOptionsTitleClosingView);

            convertView.setTag(groupViewHolder);
        } else {
            groupViewHolder = (GroupViewHolder) convertView.getTag();
        }
        groupViewHolder.position = groupPosition;

        //Setup Title and Subtitle:
        groupSpannable.clear();
        groupSpannable.clearSpans();

        if(group != null){
            if (group.getTpe() == OptionDynamicsGroup.Type.OPEN_ABOVE) {
                String above = application.getString(R.string.aboveOptions).toUpperCase();
                String openAboveOptionsTitleText = application.getString(R.string.openOptionsAbove, group.getItemsList().size()).toUpperCase();
                groupViewHolder.titleTextView.setText(openAboveOptionsTitleText);
                TextUtils.decorateInnerText(groupViewHolder.titleTextView, groupSpannable, openAboveOptionsTitleText, above, null, R.color.asset_page_dynamics_options_title_above_text_color, 0);
                groupViewHolder.dynamicsOptionsTitleClosingView.setVisibility(View.GONE);
            } else if (group.getTpe() == OptionDynamicsGroup.Type.OPEN_BELLOW) {
                String below = application.getString(R.string.belowOptions).toUpperCase();
                String openBellowOptionsTitleText = application.getString(R.string.openOptionsBelow, group.getItemsList().size()).toUpperCase();
                groupViewHolder.titleTextView.setText(openBellowOptionsTitleText);
                TextUtils.decorateInnerText(groupViewHolder.titleTextView, groupSpannable, openBellowOptionsTitleText, below, null, R.color.asset_page_dynamics_options_title_bellow_text_color, 0);
                groupViewHolder.dynamicsOptionsTitleClosingView.setVisibility(View.GONE);
            } else {
                String settledOptionsTitleText = application.getString(R.string.settledOptions).toUpperCase();
                groupViewHolder.titleTextView.setText(settledOptionsTitleText);
                groupViewHolder.dynamicsOptionsTitleClosingView.setVisibility(View.VISIBLE);
            }

            //Setup Subtitle:
            if (group.getTpe() == OptionDynamicsGroup.Type.OPEN_ABOVE || group.getTpe() == OptionDynamicsGroup.Type.OPEN_BELLOW) {
                groupViewHolder.subtitleOpenOptionsLayout.setVisibility(View.VISIBLE);
            } else {
                groupViewHolder.subtitleOpenOptionsLayout.setVisibility(View.GONE);
            }
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        OptionDynamicsGroup group = optionsGroupsList.get(groupPosition);
        OptionDynamicsItem item = group.getItemsList().get(childPosition);
        ItemViewHolder holder;

        if (convertView == null || convertView.getTag() == null) {
            holder = new ItemViewHolder();
            convertView = inflater.inflate(R.layout.asset_page_dynamics_options_item, parent, false);

            //Open options items:
            holder.dynamicsOpenOptionLayout     = (ViewGroup)   convertView.findViewById(R.id.dynamicsOptionItemOpenOptionsLayout);
            holder.optionClosedTextView         = (TextView)    convertView.findViewById(R.id.dynamicsOptionItemClosedTextView);
            holder.investInfoLayout             =               convertView.findViewById(R.id.dynamicsOptionItemInvestInfoLayout);
            holder.timeTextView                 = (TextView)    convertView.findViewById(R.id.dynamicsOptionItemInvestTimeTextView);
            holder.amountTextView               = (TextView)    convertView.findViewById(R.id.dynamicsOptionItemInvestAmountTextView);
            holder.returnTextView               = (TextView)    convertView.findViewById(R.id.dynamicsOptionItemInvestReturnTextView);
            holder.closeButton                  = (TextView)    convertView.findViewById(R.id.dynamicsOptionItemCloseButton);
            holder.inOutTheMoneyView            = (ImageView)   convertView.findViewById(R.id.dynamicsOptionItemInTheMoneyImageView);
            holder.separator1                   =               convertView.findViewById(R.id.dynamicsOptionItemSeparator1);
            holder.separator2                   =               convertView.findViewById(R.id.dynamicsOptionItemSeparator2);

            //Settled options items:
            holder.dynamicsSettledOptionLayout  = (ViewGroup) convertView.findViewById(R.id.dynamicsOptionItemSettledOptionsLayout);
            holder.expiredTextView              = (TextView)  convertView.findViewById(R.id.dynamicsOptionItemExpiredTimeTextView);
            holder.purchasedTextView            = (TextView)  convertView.findViewById(R.id.dynamicsOptionItemPurchasedTextView);
            holder.closingTextView              = (TextView)  convertView.findViewById(R.id.dynamicsOptionItemClosingTextView);
            holder.settledInOutTheMoneyView     = (ImageView) convertView.findViewById(R.id.dynamicsOptionItemSettledInTheMoneyImageView);

            convertView.setTag(holder);
        } else {
            holder = (ItemViewHolder) convertView.getTag();
        }
        holder.groupPosition = groupPosition;
        holder.position = childPosition;

        //SETTLED OPTION:
        if (group.getTpe() == OptionDynamicsGroup.Type.SETTLED) {
            holder.dynamicsSettledOptionLayout.setVisibility(View.VISIBLE);
            holder.dynamicsOpenOptionLayout.setVisibility(View.GONE);

            //Setup Expiry Time:
            holder.expiredTextView.setText(expireTimeFormat.format(item.time));

            //Setup Purchased Level and Amount:
            clearChildSpanable();
            formatExpiryAmount(holder.purchasedTextView, item.investLevel, item.investAmount, item.profitPercent);

            //Setup the invest type icon:
            if (item.investType == OptionDynamicsItem.InvestType.CALL) {
                holder.purchasedTextView.setCompoundDrawablesWithIntrinsicBounds(callIconDrawable, null, null, null);
            } else {
                holder.purchasedTextView.setCompoundDrawablesWithIntrinsicBounds(putIconDrawable, null, null, null);
            }

            //Setup Closing Level and Amount:
            clearChildSpanable();
            formatClosingLevel(holder.closingTextView, item.returnAmountFormated);

            //Setup "In the money" ribbon:
            if (item.isInTheMoney) {
                holder.settledInOutTheMoneyView.setVisibility(View.VISIBLE);
            } else {
                holder.settledInOutTheMoneyView.setVisibility(View.GONE);
            }

        }

        //OPEN OPTION:
        else {
            holder.dynamicsSettledOptionLayout.setVisibility(View.GONE);
            holder.dynamicsOpenOptionLayout.setVisibility(View.VISIBLE);

            //Setup the Option Closed text:
            if (item.isShowOptionClosed) {
                formatOptionClosedAmount(holder.optionClosedTextView, item.investAmount);
                holder.investInfoLayout.setVisibility(View.GONE);
                holder.optionClosedTextView.setVisibility(View.VISIBLE);
            } else {
                holder.optionClosedTextView.setVisibility(View.GONE);
                holder.investInfoLayout.setVisibility(View.VISIBLE);
            }

            //Setup Purchased Time:
            holder.timeTextView.setText(purchaseTimeFormat.format(item.time));

            //Setup Invest Amount:
            clearChildSpanable();
            formatAmount(holder.amountTextView, item.investAmount);

            //Setup the invest type icon:
            if (group.getTpe() == OptionDynamicsGroup.Type.OPEN_ABOVE) {
                holder.amountTextView.setCompoundDrawablesWithIntrinsicBounds(callIconDrawable, null, null, null);
            } else {
                holder.amountTextView.setCompoundDrawablesWithIntrinsicBounds(putIconDrawable, null, null, null);
            }
            //Setup Return Amount:
            clearChildSpanable();
            formatReturnAmount(holder.returnTextView, item.returnAmount, item.profitPercent);

            //Setup Close button:
            boolean isCloseEnabled = item.isCloseEnabled && !item.isClosed;
            holder.closeButton.setEnabled(isCloseEnabled);
            clearChildSpanable();
            formatCloseForAmount(holder.closeButton, item.closeForAmount, item.isAvailable);
            holder.closeButton.setTag(holder);
            holder.closeButton.setOnClickListener(this);

            //Setup "In the money" ribbon:
            if (item.isInTheMoneyForClose) {
                holder.inOutTheMoneyView.setVisibility(View.VISIBLE);
            } else {
                holder.inOutTheMoneyView.setVisibility(View.GONE);
            }
        }

        return convertView;
    }

    private void clearChildSpanable() {
        childSpannable.clear();
        childSpannable.clearSpans();
    }

    private void formatAmount(TextView textView, double amount) {
        textView.setText(AmountUtil.getFormattedAmount(amount, currency));
        TextUtils.decorateInnerText(textView, childSpannable, textView.getText().toString(), currency.getSymbol(), null, 0, currencyTextSize);
    }

    private void formatReturnAmount(TextView textView, double amount, double percent) {
        //Format Percent like : (+26.23%)
        String sign = "";
        if(percent>0){
            sign = "+";
        }else if(percent<0){
            sign = "-";
        }
        String percentFormatted = "(" + sign + percentDecimalFormat.format(percent) + "%)";
        String amountFormatted = AmountUtil.getFormattedAmount(amount, currency);
        textView.setText(amountFormatted + " " + percentFormatted);
        //Format the currency text size:
        TextUtils.decorateInnerText(textView, childSpannable, textView.getText().toString(), currency.getSymbol(), null, 0, currencyTextSize);
        //Format the percent text size:
        TextUtils.decorateInnerText(textView, childSpannable, textView.getText().toString(), percentFormatted, null, 0, currencyTextSize);
    }

    private void formatExpiryAmount(TextView textView, double level, double amount, double percent) {
        String levelFormatted = String.valueOf(level);
        //Format Percent like : (+26.23%)
        String sign = "";
        if(percent>0){
            sign = "+";
        }else if(percent<0){
            sign = "-";
        }
        String percentFormatted = "(" + sign + percentDecimalFormat.format(percent) + "%)";
        String amountFormatted = AmountUtil.getFormattedAmount(amount, currency);
        textView.setText(levelFormatted + " " + amountFormatted + " " + percentFormatted);
        //Format the currency text size:
        TextUtils.decorateInnerText(textView, childSpannable, textView.getText().toString(), currency.getSymbol(), null, 0, currencyTextSize);
        //Format the percent text size:
        TextUtils.decorateInnerText(textView, childSpannable, textView.getText().toString(), percentFormatted, null, 0, currencyTextSize);
    }

    private void formatClosingLevel(TextView textView, String amount) {
        textView.setText(amount);
        //Format the currency text size:
        TextUtils.decorateInnerText(textView, childSpannable, textView.getText().toString(), currency.getSymbol(), null, 0, currencyTextSize);
    }

    private void formatCloseForAmount(TextView textView, double amount, boolean isAvaiable) {
        if(isAvaiable){
            String amountFormatted = AmountUtil.getFormattedAmount(amount, currency);
            textView.setText(application.getString(R.string.closeForAmount, amountFormatted));
            TextUtils.decorateInnerText(textView, childSpannable, textView.getText().toString(), amountFormatted, FontsUtils.Font.ROBOTO_MEDIUM, 0, closeForAmountTextSize);
        }else{
            textView.setText(application.getString(R.string.closeForAmount, ""));
        }
    }

    private void formatOptionClosedAmount(TextView textView, double amount) {
        String amountFormatted = AmountUtil.getFormattedAmount(amount, currency);
        textView.setText(application.getString(R.string.optionClosed, amountFormatted));
    }

    public void closeOptionCallback(boolean isSuccess, final OptionDynamicsItem item){
        if(isSuccess){
            OptionDynamicsGroup openOptionDynamicsGroup = null;
            GroupLoop :
            for(OptionDynamicsGroup group : optionsGroupsList){
                for(OptionDynamicsItem optionDynamicsItem : group.getItemsList()){
                    if(item.id == optionDynamicsItem.id){
                        openOptionDynamicsGroup = group;
                        break GroupLoop;
                    }
                }
            }
            item.isShowOptionClosed = true;
            notifyDataSetChanged();
            final OptionDynamicsGroup finalOpenOptionDynamicsGroup = openOptionDynamicsGroup;
            CountDownTimer timer = new CountDownTimer(AssetOptionsDynamicsFragment.OPTION_CLOSED_TIME, AssetOptionsDynamicsFragment.OPTION_CLOSED_TIME) {
                @Override
                public void onTick(long millisUntilFinished) {
                }
                @Override
                public void onFinish() {
                    finalOpenOptionDynamicsGroup.getItemsList().remove(item);
                    if(finalOpenOptionDynamicsGroup.getItemsList().isEmpty()){
                        getGroups().remove(finalOpenOptionDynamicsGroup);
                    }
                    notifyDataSetChanged();
                    closeOptionListener.onCloseOptionClosed();
                }
            };
            timer.start();

        }else{
            item.isCloseEnabled = true;
            notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getTag() instanceof ItemViewHolder) {
            ItemViewHolder holder = (ItemViewHolder) v.getTag();
            final int groupPosition = holder.groupPosition;
            final int position = holder.position;
            OptionDynamicsItem item = optionsGroupsList.get(groupPosition).getItemsList().get(position);
            item.isCloseEnabled = false;
            item.closedForAmount = item.closeForAmount;
            notifyDataSetChanged();
            closeOptionListener.onCloseOptionButtonClicked(item);
        }
    }

}
