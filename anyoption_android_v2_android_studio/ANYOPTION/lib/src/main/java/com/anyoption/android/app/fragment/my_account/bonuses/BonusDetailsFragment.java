package com.anyoption.android.app.fragment.my_account.bonuses;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.manager.PopUpManager.OnAlertPopUpButtonPressedListener;
import com.anyoption.android.app.manager.PopUpManager.OnConfirmPopUpButtonPressedListener;
import com.anyoption.android.app.popup.WaiveBonusAlertDialogFragment;
import com.anyoption.android.app.popup.WaiveBonusConfirmDialogFragment;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.DepositBonusBalanceBase;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.service.requests.DepositBonusBalanceMethodRequest;
import com.anyoption.common.service.results.DepositBonusBalanceMethodResult;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.requests.BonusMethodRequest;
import com.anyoption.json.results.BonusMethodResult;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class BonusDetailsFragment extends NavigationalFragment implements OnClickListener {

    protected BonusUsers bonus;
    protected String[] bonusFactor;
    protected WebView webView;
    protected User user;
    protected View loadingView;
    protected long cashBalance;

    @Override
    protected Screenable setupScreen() {
        return Screen.BONUS_DETAILS;
    }

    /**
     * Use this method to get new instance of the fragment.
     *
     * @return
     */
    public static BonusDetailsFragment newInstance(Bundle bundle) {
        BonusDetailsFragment bonusDetailsFragment = new BonusDetailsFragment();
        bonusDetailsFragment.setArguments(bundle);
        return bonusDetailsFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = application.getUser();
        Bundle bundle = getArguments();
        if (bundle != null) {
            bonus = (BonusUsers) bundle.getSerializable(Constants.EXTRA_BONUS);
            bonusFactor = bundle.getStringArray(Constants.EXTRA_BONUS_FACTOR);
        }

        getSplitBalance();
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.bonus_details_layout, container, false);
        loadingView = rootView.findViewById(R.id.loading_view);
        setupLoading(true);

        ((TextView) rootView.findViewById(R.id.textViewBonusGeneralCaption)).setText(bonus.getGeneralTermsCaption());
        ((TextView) rootView.findViewById(R.id.textViewBonusId)).setText(Long.toString(bonus.getId()));
        ((TextView) rootView.findViewById(R.id.textViewEndDate)).setText(bonus.getEndDateTxt());
        ((TextView) rootView.findViewById(R.id.textViewBonusStatus)).setText(bonus.getBonusStateTxt());
        ((TextView) rootView.findViewById(R.id.textViewBonusDescription)).setText(bonus.getBonusDescriptionTxt());
        webView = (WebView) rootView.findViewById(R.id.webViewBonusTerms);
        webView.getSettings().setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT <= 18) {
            webView.getSettings().setPluginState(PluginState.ON);
        }
        webView.setWebViewClient(new MyWebViewClient());

        if(cashBalance != 0){
            getBonusDescription();
        }

        if (bonus.getBonusStateId() == Constants.BONUS_STATE_GRANTED || bonus.getBonusStateId() == Constants.BONUS_STATE_ACTIVE) {
            rootView.findViewById(R.id.buttonWaiveBonus).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.buttonWaiveBonus).setOnClickListener(this);
        }
        return rootView;
    }


    protected void setupLoading(boolean isLoading) {
        if (loadingView != null) {
            if (isLoading) {
                loadingView.setVisibility(View.VISIBLE);
            } else {
                loadingView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onClick(final View v) {
        if (v.getId() == R.id.buttonWaiveBonus) {
            // Show confirm POPUP:
            WaiveBonusConfirmDialogFragment waiveBonusConfirmDialogFragment = (WaiveBonusConfirmDialogFragment) application.getFragmentManager().showDialogFragment(WaiveBonusConfirmDialogFragment.class, null);
            waiveBonusConfirmDialogFragment.setOnConfirmPopUpButtonPressedListener(new OnConfirmPopUpButtonPressedListener() {
                @Override
                public void onPositive() {
                    updateBonus();
                }
                @Override
                public void onNegative() {}
            });
        }
    }

    protected void updateBonus() {
        BonusMethodRequest request = new BonusMethodRequest();
        request.setBonusId(bonus.getId());
        setupBonusState(request);
        application.getCommunicationManager().requestService(this, "updateBonusCallBack", Constants.SERVICE_UPDATE_BONUS_STATUS, request, BonusMethodResult.class);
    }

    protected void setupBonusState(BonusMethodRequest request){
        request.setBonusStateId(Constants.BONUS_STATE_REFUSED);
    }

    public void updateBonusCallBack(Object result) {
        MethodResult methodResult = (MethodResult) result;
        if (methodResult.getErrorCode() != Constants.ERROR_CODE_SUCCESS) {
            Log.d(TAG, "Error Waive Bonus");
            return;
        }

        //Show alert POPUP:
        WaiveBonusAlertDialogFragment waiveBonusAlertDialogFragment = (WaiveBonusAlertDialogFragment) application.getFragmentManager().showDialogFragment(WaiveBonusAlertDialogFragment.class, null);
        waiveBonusAlertDialogFragment.setOnButtonPressedListener(new OnAlertPopUpButtonPressedListener() {
            @Override
            public void onButtonPressed() {
                application.getCurrentActivity().onBackPressed();
            }
        });
    }

    protected void getBonusDescription() {

        String web_site_link = Application.get().getWebSiteLink();
        String actionURL;
        actionURL = web_site_link + "termsBonus" + bonus.getTypeId() + ".jsf?s=" + application.getSkinId() + "&fromApp=true&version=2-mobile";
        Log.d("URL REQUEST", actionURL);
        String text = " <html> " +
                "           <head> " +
                "               <meta HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html;charset=UTF-8\" /> " +
                "               <META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\" /> " +
                "           </head> " +
                "           <body onload=\"document.termsForm.submit();\" dir=\"ltr\"> " +
                " <form method=\"POST\" style=\"display:none;\" id=\"termsForm\" name=\"termsForm\" action=\"" + actionURL +"\"> " +
                "   <input name=\"bonusAmount\" id=\"bonusAmount\" value=\"{bonusAmount}\"/> " +
                "   <input name=\"numOfActions\" id=\"numOfActions\"  value=\"{numOfActions}\"/> " +
                "   <input name=\"startDate\" id=\"startDate\" value=\"{startDate}\"/> " +
                "   <input name=\"endDate\" id=\"endDate\" value=\"{endDate}\"/> " +
                "   <input name=\"minDeposit\" id=\"minDeposit\" value=\"{minDeposit}\"/> " +
                "   <input name=\"maxDeposit\" id=\"maxDeposit\" value=\"{maxDeposit}\"/> " +
                "   <input name=\"wageringParam\" id=\"wageringParam\" value=\"{wageringParam}\"/> " +
                "   <input name=\"bonusPercent\" id=\"bonusPercent\" value=\"{bonusPercent}\"/> " +
                "   <input name=\"sumInvQualify\" id=\"sumInvQualify\" value=\"{sumInvQualify}\"/> " +
                "   <input name=\"bonusStateId\" id=\"bonusStateId\" value=\"{bonusStateId}\"/> " +
                "   <input name=\"bonusExpired\" id=\"bonusExpired\" value=\"{bonusExpired}\"/> " +
                "   <input name=\"hasSteps\" id=\"hasSteps\" value=\"{hasSteps}\"/> " +
                "   <input name=\"bonusSteps\" id=\"bonusSteps\" value=\"{bonusSteps}\"/> " +
                "   <input name=\"symbol\" id=\"symbol\" value=\"{symbol}\"/> " +
                "   <input name=\"bonusAmountNoFormat\" id=\"bonusAmountNoFormat\" value=\"{bonusAmountNoFormat}\"/> " +
                "   <input name=\"minInvestAmount\" id=\"minInvestAmount\" value=\"{minInvestAmount}\"/> " +
                "   <input name=\"maxInvestAmount\" id=\"maxInvestAmount\" value=\"{maxInvestAmount}\"/> " +
                "   <input name=\"bonusId\" id=\"bonusId\" value=\"{bonusId}\"/> " +
                "   <input name=\"sumInvQualifyNoFormat\" id=\"sumInvQualifyNoFormat\" value=\"{sumInvQualifyNoFormat}\"/> " +
                "   <input name=\"cashBalance\" id=\"cashBalance\" value=\"{cashBalance}\"/> " +
                "   <input name=\"turnoverParam\" id=\"turnoverParam\" value=\"{turnoverParam}\"/> " +
                "   <input name=\"turnOverParamBinaryOneHundred\" id=\"turnOverParamBinaryOneHundred\" value=\"{turnOverParamBinaryOneHundred}\"/> " +
                "   <input name=\"turnOverParamOptionPlus\" id=\"turnOverParamOptionPlus\" value=\"{turnOverParamOptionPlus}\"/> " +
                " </form> " +
                "           </body> " +
                "       </html>";

        text = text.replace("{bonusPercent}", String.valueOf(bonus.getBonusPercent() * 100));
        text = text.replace("{minDeposit}", AmountUtil.getFormattedAmount(bonus.getMinDepositAmount(), user.getCurrency()));

        String bonusAmount = formatBonus();

        text = text.replace("{bonusAmount}", bonusAmount);
        text = text.replace("{endDate}", String.valueOf(bonus.getEndDateTxt()));
        text = text.replace("{numOfActions}", String.valueOf(bonus.getNumOfActions()));
        text = text.replace("{startDate}", String.valueOf(bonus.getStartDateTxt()));
        text = text.replace("{maxDeposit}", String.valueOf(bonus.getMaxDepositAmount()));
        text = text.replace("{wageringParam}", String.valueOf(bonus.getWageringParam()));
        text = text.replace("{sumInvQualify}", AmountUtil.getFormattedAmount(bonus.getSumInvQualify(), user.getCurrency()));
        text = text.replace("{bonusStateId}", String.valueOf(bonus.getBonusStateId()));
        text = text.replace("{bonusExpired}", String.valueOf(bonus.isBonusExpired()));
        text = text.replace("{hasSteps}", String.valueOf(bonus.isHasSteps()));
        text = text.replace("{bonusSteps}", String.valueOf(bonus.getBonusStepsForHTML()));
        text = text.replace("{symbol}", user.getCurrencySymbol());
        text = text.replace("{bonusAmountNoFormat}", String.valueOf(bonus.getBonusAmount()));
        text = text.replace("{minInvestAmount}", AmountUtil.getFormattedAmount(bonus.getMinInvestAmount(), user.getCurrency()));
        text = text.replace("{maxInvestAmount}", AmountUtil.getFormattedAmount(bonus.getMaxInvestAmount(), user.getCurrency()));
        text = text.replace("{bonusId}", String.valueOf(bonus.getBonusId()));
        text = text.replace("{sumInvQualifyNoFormat}", String.valueOf(bonus.getSumInvQualify()));
        text = text.replace("{cashBalance}", String.valueOf(cashBalance));
        text = text.replace("{turnoverParam}", String.valueOf(bonus.getTurnoverParam()));
        text = text.replace("{turnOverParamBinaryOneHundred}", String.valueOf(bonus.getTurnOverParamBinaryOneHundred()));
        text = text.replace("{turnOverParamOptionPlus}", String.valueOf(bonus.getTurnOverParamOptionPlus()));

        webView.loadData(text, "text/html", "utf-8");
    }



    protected String formatBonus(){
        return AmountUtil.getFormattedAmount(bonus.getBonusAmount(), user.getCurrency());
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public void onPageFinished(android.webkit.WebView view, String url) {
            super.onPageFinished(view, url);
            setupLoading(false);
        }
    }

    protected void getSplitBalance() {
        DepositBonusBalanceMethodRequest request = new DepositBonusBalanceMethodRequest();
        request.setUserId(application.getUser().getId());
        request.setBalanceCallType(1);
        if (!application.getCommunicationManager().requestService(BonusDetailsFragment.this, "getSplitBalanceCallback", Constants.SERVICE_GET_SPLIT_BALANCE, request, DepositBonusBalanceMethodResult.class, false, false)) {

        }
    }


    public void getSplitBalanceCallback(Object resObj) {
        DepositBonusBalanceMethodResult result = (DepositBonusBalanceMethodResult) resObj;
        if (result != null && result instanceof DepositBonusBalanceMethodResult && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            if (result.getDepositBonusBalanceBase() != null) {
                DepositBonusBalanceBase depositBonusBalanceBase = result.getDepositBonusBalanceBase();
                cashBalance = depositBonusBalanceBase.getDepositCashBalance();
                if (webView != null) {
                    getBonusDescription();
                }
            }
        }
    }
}
