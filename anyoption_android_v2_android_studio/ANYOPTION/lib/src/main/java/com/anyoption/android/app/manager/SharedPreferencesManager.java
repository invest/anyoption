package com.anyoption.android.app.manager;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.constants.Constants;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Shared Preferences Manager that manages the data stored on the preference files on the device.
 * @author Anastas Arnaudov
 *
 */
public class SharedPreferencesManager {

	public static final String TAG = SharedPreferencesManager.class.getSimpleName();

	private Application application;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
	
	public SharedPreferencesManager(Application application){
		Log.d(TAG, "On Create");
		this.application = application;
		this.sharedPreferences = this.application.getSharedPreferences(Constants.PREFERENCES, Context.MODE_PRIVATE);
	    editor = sharedPreferences.edit();
	}
	
	/**
	 * Saves a int value for the given key in the Shared Preferences.
	 * @param key
	 * @param value
	 */
	public void putInt(String key, int value) {
        editor.putInt(key, value);
        editor.commit();
    }
	
    /**
     * Returns the int value for the given key from the Shared Preferences.
     * @param key
     * @param defValue
     * @return
     */
    public Integer getInt(String key, Integer defValue) {
    	try {
    		return sharedPreferences.getInt(key, defValue);			
		} catch (Exception e) {
			Log.e(TAG, "Could not get saved int value for the key=" + key);
		}
    	return defValue;
    }
    
	/**
	 * Saves a String value for the given key in the Shared Preferences.
	 * @param key
	 * @param value
	 */
    public void putString(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }
    
    /**
     * Returns the String value for the given key from the Shared Preferences.
     * @param key
     * @param defValue
     * @return
     */
    public String getString(String key, String defValue) {
    	try {
    		return sharedPreferences.getString(key, defValue);			
		} catch (Exception e) {
			Log.e(TAG, "Could not get saved String value for the key=" + key);
		}
    	return defValue;
    }
    
	/**
	 * Saves a boolean value for the given key in the Shared Preferences.
	 * @param key
	 * @param value
	 */
    public void putBoolean(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.commit();
    }
    
    /**
     * Returns the boolean value for the given key from the Shared Preferences.
     * @param key
     * @param defValue
     * @return
     */
    public Boolean getBoolean(String key, Boolean defValue) {
    	try {
            return sharedPreferences.getBoolean(key, defValue);		
		} catch (Exception e) {
			Log.e(TAG, "Could not get saved boolean value for the key=" + key);
		}
    	return defValue;
    }
    
	/**
	 * Saves a long value for the given key in the Shared Preferences.
	 * @param key
	 * @param value
	 */
    public void putLong(String key, long value) {
    	editor.putLong(key, value);
    	editor.commit();
    }
    
	/**
	 * Returns the long value for the given key from the Shared Preferences.
	 * 
	 * @param key
	 * @param defValue
	 * @return
	 */
	public Long getLong(String key, Long defValue) {
		try {
			if (defValue != null) {
				return sharedPreferences.getLong(key, defValue);
			} else {
				Long value = sharedPreferences.getLong(key, 0L);
				if (value == 0L) {
					return null;
				}else{
					return value;
				}
			}
		} catch (Exception e) {
			Log.e(TAG, "Could not get saved long value for the key=" + key);
		}
		return defValue;
	}
    
	/**
	 * Saves a float value for the given key in the Shared Preferences.
	 * @param key
	 * @param value
	 */
    public void putFloat(String key, float value) {
    	editor.putFloat(key, value);
    	editor.commit();
    }

    /**
     * Returns the float value for the given key from the Shared Preferences.
     * @param key
     * @param defValue
     * @return
     */
    public Float getFloat(String key, Float defValue) {
    	try {
    		return sharedPreferences.getFloat(key, defValue);	
    	} catch (Exception e) {
    		Log.e(TAG, "Could not get saved float value for the key=" + key);
    	}
    	return defValue;
    }

    /**
     * Removes the value for the given key from the Shared Preferences.
     * @param key
     */
    public void remove(String key) {
    	Log.d(TAG, "Removing value for key=" + key);
        editor.remove(key);
        editor.commit();
    }

	
}
