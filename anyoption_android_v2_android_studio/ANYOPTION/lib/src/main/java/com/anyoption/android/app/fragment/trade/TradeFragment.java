package com.anyoption.android.app.fragment.trade;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.event.ExpiredOptionsEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.event.UserUpdatedEvent;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.constants.Constants;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Present List of Markets.
 *
 * @author Anastas Arnaudov
 *
 */
public class TradeFragment extends NavigationalFragment {

	public static final String TAG = TradeFragment.class.getSimpleName();

	public String filterTab;

	BaseFragment featuredAssetsFragment;

	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static TradeFragment newInstance(Bundle bundle){
		TradeFragment tradeFragment = new TradeFragment();
		tradeFragment.setArguments(bundle);
		return tradeFragment;
	}

	private View rootView;

	@Override
	protected Screenable setupScreen() {
		return Screen.TRADE;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = getArguments();
		if(bundle != null){
			filterTab = bundle.getString(Constants.EXTRA_ASSETS_LIST_FILTER);
		}
		application.registerForEvents(this, NavigationEvent.class);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.trade_fragment_layout, container, false);
		//TODO move this to the Fragment Manager
		FragmentManager childFragmentManager = getChildFragmentManager();
		FragmentTransaction fragmentTransaction = childFragmentManager.beginTransaction();

		if (childFragmentManager.findFragmentByTag(Screen.ASSETS_FEATURED.getTag()) == null) {
			// TradeFragment creation
			featuredAssetsFragment = FeaturedAssetsFragment.newInstance(null);
			fragmentTransaction.add(R.id.trade_featured_assets_layout, featuredAssetsFragment, Screen.ASSETS_FEATURED.getTag());

			if(filterTab != null && filterTab.length() > 0) {
				Bundle filterBundle = new Bundle();
				filterBundle.putString(Constants.EXTRA_ASSETS_LIST_FILTER, filterTab);
				featuredAssetsFragment.setArguments(filterBundle);
				filterTab = "";
				getArguments().putString(Constants.EXTRA_ASSETS_LIST_FILTER, "");
			}
		} else {
			featuredAssetsFragment = (FeaturedAssetsFragment) childFragmentManager.findFragmentByTag(Screen.ASSETS_FEATURED.getTag());
		}

		fragmentTransaction.commit();

		View openOptionsLayout = rootView.findViewById(R.id.trade_open_options_layout);
		openOptionsLayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				application.postEvent(new NavigationEvent(Screen.MY_OPTIONS, NavigationType.DEEP));
			}
		});
		setupOpenOptions();


		return rootView;
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if(!hidden){
			application.registerForEvents(this, UserUpdatedEvent.class, LoginLogoutEvent.class, ExpiredOptionsEvent.class, NavigationEvent.class);
		}else{
			application.unregisterForAllEvents(this);
		}
	}

	@Override
	public void onResume() {
		application.registerForEvents(this, UserUpdatedEvent.class, LoginLogoutEvent.class, ExpiredOptionsEvent.class, NavigationEvent.class);
		super.onResume();
	}

	public void onEventMainThread(NavigationEvent event) {
		if ((event.getToScreen() == Screen.TRADE || event.getToScreen() == Screen.TABLET_TRADE) && event.getBundle().getString(Constants.EXTRA_ASSETS_LIST_FILTER, "").length() > 0) {
			filterTab = event.getBundle().getString(Constants.EXTRA_ASSETS_LIST_FILTER);
			if (filterTab != null && filterTab.length() > 0) {
				if (featuredAssetsFragment instanceof FeaturedAssetsFragment) {
					((FeaturedAssetsFragment) featuredAssetsFragment).changeFilter(filterTab);
					getArguments().putString(Constants.EXTRA_ASSETS_LIST_FILTER, "");
					filterTab = "";
				}
			}
		}
	}

	@Override
	public void onPause() {
		application.unregisterForAllEvents(this);
		application.registerForEvents(this, NavigationEvent.class);
		super.onPause();
	}

	@Override
	public void onDestroy() {
		application.unregisterForAllEvents(this);
		super.onDestroy();
	}

	public void onEventMainThread(LoginLogoutEvent event) {
		setupOpenOptions();
	}

	public void onEventMainThread(UserUpdatedEvent event) {
		setupOpenOptions();
	}

	public void onEventMainThread(ExpiredOptionsEvent event) {
		setupOpenOptions();
	}

	private void setupOpenOptions(){
		View openOptionsLayout = rootView.findViewById(R.id.trade_open_options_layout);
		if(application.isLoggedIn() && application.getUser().getOpenedInvestmentsCount() > 0){
			openOptionsLayout.setVisibility(View.VISIBLE);
			TextView openOptionTextView = (TextView) rootView.findViewById(R.id.trade_open_options);
			if(application.getUser().getOpenedInvestmentsCount() == 1){
				openOptionTextView.setText(getString(R.string.openOption) + " »");
			}else{
				openOptionTextView.setText(application.getUser().getOpenedInvestmentsCount() + " " + getString(R.string.openOptions) + " »");
			}
		}else{
			openOptionsLayout.setVisibility(View.GONE);
		}
	}

}
