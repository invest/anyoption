package com.anyoption.android.app.widget;


import com.anyoption.android.R;
import com.anyoption.android.app.util.AnimationUtils;
import com.anyoption.android.app.util.DrawableUtils;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * Invest button, that perform several animations - that represent loading while the investment is completed.
 * @author Anastas Arnaudov
 *
 */
public class InvestButton extends RelativeLayout{

	private static final String TAG = InvestButton.class.getSimpleName();
	
	public static final int BUTTON_TYPE_CALL = 0;
	public static final int BUTTON_TYPE_PUT = 1;

	public static final int TYPE_BINARY 		= 1;
	public static final int TYPE_OPTION_PLUS 	= 2;
	public static final int TYPE_DYNAMICS 		= 3;

	/**
	 * Type of the button : PUT or CALL.
	 */
	private int buttonType;
	/**
	 * Type of the button depending on the product.
	 */
	private int type;
	private State state;

	private boolean isTriangleCentered = false;

	private ImageView circleBgImageView;
	private ImageView triangleImageView;
	private ImageView circleSplashImageView;
	private ImageView circleBorderImageView;
	private ImageView circleBgDisabled;


	// ANIMATIONS VALUES
	private AnimatorSet loadingAnimation;

	private long scaleDuration 			= 300l;
	private long alphaDuration 			= 1000l;
	private long rotateDuration 		= 3000l;
	private long fadeOutDuration 		= 1000l;
	private long fadeOutDelay 			= 1000l;
	private float scaleFactor 			= 0.75f;
	private float alphaINFactor 		= 0.85f;
	private float alphaOUTFactor 		= 0.50f;
	private float rotateAngle 			= 360f;
	private float disabledAlpha 		= 0.2f;
	private float disabledTriangleAlpha = 0.6f;
	//////////////////////////////////

	private boolean isOKEndAnimation;
	private AnimatorSet animatorSet;

	/**
	 * The State of the Invest button.
	 */
	public enum State{
		/**
		 * The Normal State of the button.
		 */
		NORMAL,
		/**
		 * The Button is running a Loading Animation.
		 */
		LOADING,
		/**
		 * The button is disabled and cannot receive touch events.
		 */
		DISABLED
	}
	
	public InvestButton(Context context) {
		super(context);
		init(context, null, 0);
	}
	public InvestButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	public InvestButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	public void init(Context context, AttributeSet attrs, int defStyle){
		View.inflate(context, R.layout.invest_button_layout, this);
		TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.InvestButton, defStyle, defStyle);
		try {
			type 		= a.getInt(R.styleable.InvestButton_investButtonType, TYPE_BINARY);
			buttonType 	= a.getInt(R.styleable.InvestButton_buttonType, BUTTON_TYPE_CALL);
		} finally {
			a.recycle();
		}

		circleBgImageView 		= (ImageView) findViewById(R.id.circle_bg_image_view);
		triangleImageView 		= (ImageView) findViewById(R.id.triangle_image_view);
		circleBorderImageView 	= (ImageView) findViewById(R.id.circle_border_image_view);
		circleSplashImageView 	= (ImageView) findViewById(R.id.circle_splash);
		circleBgDisabled 		= (ImageView) findViewById(R.id.circle_bg_disabled_image_view);


		setupUI();
		
		state = State.NORMAL;
	}

	private void setupUI() {
		if(type == TYPE_DYNAMICS){
			circleBgImageView.setImageDrawable(DrawableUtils.getDrawable(R.drawable.investButtonDynamicsBg));
			circleSplashImageView.setImageDrawable(DrawableUtils.getDrawable(R.drawable.investButtonDynamicsSplash));
			int loadingDynamicsPadding = getResources().getDimensionPixelSize(R.dimen.asset_page_dynamics_invest_button_loading_padding);
			circleBorderImageView.setPadding(loadingDynamicsPadding, loadingDynamicsPadding, loadingDynamicsPadding, loadingDynamicsPadding);
			circleSplashImageView.setPadding(loadingDynamicsPadding, loadingDynamicsPadding, loadingDynamicsPadding, loadingDynamicsPadding);
		}else if (type ==  TYPE_OPTION_PLUS) {
			circleBgImageView.setImageDrawable(DrawableUtils.getDrawable(R.drawable.investButtonBgOptionPlus));
			circleSplashImageView.setImageDrawable(DrawableUtils.getDrawable(R.drawable.investButtonSplashOptionPlus));
		} else {
			circleBgImageView.setImageDrawable(DrawableUtils.getDrawable(R.drawable.investButtonBg));
			circleSplashImageView.setImageDrawable(DrawableUtils.getDrawable(R.drawable.investButtonSplash));
		}

		if (buttonType == BUTTON_TYPE_CALL) {
			if(type == TYPE_DYNAMICS){
				triangleImageView.setImageDrawable(DrawableUtils.getDrawable(R.drawable.investButtonDynamicsCallTriangle));
			}else{
				triangleImageView.setImageDrawable(DrawableUtils.getDrawable(R.drawable.investButtonCallTriangle));
			}
		} else {
			if(type == TYPE_DYNAMICS){
				triangleImageView.setImageDrawable(DrawableUtils.getDrawable(R.drawable.investButtonDynamicsPutTriangle));
			}else{
				triangleImageView.setImageDrawable(DrawableUtils.getDrawable(R.drawable.investButtonPutTriangle));
			}
		}
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
		if(!isTriangleCentered){
			//Center the triangle:
			float delta = triangleImageView.getHeight()/6;
			switch (buttonType) {
			case BUTTON_TYPE_CALL:
					triangleImageView.setY(triangleImageView.getY() - delta);
				break;
			case BUTTON_TYPE_PUT:
					triangleImageView.setY(triangleImageView.getY() + delta);
				break;
			}		
			isTriangleCentered = true;
		}
	}
	
	private void startLoadingAnimation() {
		animatorSet = new AnimatorSet();
		animatorSet.addListener(new AnimatorListener() {
			@Override
			public void onAnimationStart(Animator animation) {
				state = State.LOADING;
			}
			@Override
			public void onAnimationRepeat(Animator animation) {}
			@Override
			public void onAnimationEnd(Animator animation) {
				state = State.NORMAL;
			}
			@Override
				public void onAnimationCancel(Animator animation) {}
		});
			
		animatorSet.play(createLoadingAnimator())
			.with(createTriangleSmallAnimator())
			.before(createTriangleBigAnimator())
			.before(createFadeOutAnimator());
			
		animatorSet.start();
	}
	
	private void stopLoadingAnimation(){
		if(animatorSet != null){
			animatorSet.cancel();
		}
	}
	
	private Animator createTriangleSmallAnimator(){
		return AnimationUtils.createScaleAnimator(triangleImageView, scaleFactor, scaleFactor, scaleDuration);
	}
	
	private Animator createTriangleBigAnimator(){
		return AnimationUtils.createScaleAnimator(triangleImageView, 1f, 1f, scaleDuration);
	}
	
	private Animator createRotateAnimator(){
		ObjectAnimator rotateAnimator = (ObjectAnimator) AnimationUtils.createRotateAnimator(circleBorderImageView, rotateAngle, rotateDuration);
		rotateAnimator.setInterpolator(new LinearInterpolator());
		rotateAnimator.setRepeatCount(ObjectAnimator.INFINITE);
		return rotateAnimator;
	}
	
	private Animator createPulsateAnimator(){
		ObjectAnimator pulsateAnimation = (ObjectAnimator) AnimationUtils.createAlphaAnimator(circleSplashImageView, alphaDuration / 2, alphaINFactor, alphaOUTFactor);
		pulsateAnimation.setInterpolator(new LinearInterpolator());
		pulsateAnimation.setRepeatMode(ObjectAnimator.REVERSE);
		pulsateAnimation.setRepeatCount(ObjectAnimator.INFINITE);
		return pulsateAnimation;
	}
	
	private Animator createLoadingAnimator(){
		if(loadingAnimation == null){
			loadingAnimation = new AnimatorSet();
			loadingAnimation.play(createRotateAnimator()).with(createPulsateAnimator());
			loadingAnimation.addListener(new AnimatorListener() {
				@Override
				public void onAnimationStart(Animator animation) {
					Log.d(TAG, "Start Loading Animation");
					if (type == TYPE_OPTION_PLUS) {
						circleBgImageView.setImageDrawable(DrawableUtils.getDrawable(R.drawable.investButtonBgOptionPlusYellow));
					}
					triangleImageView.setImageDrawable(getButtonDrawable(true));
					circleBorderImageView.setImageDrawable(getLoadingDrawable());
					circleBorderImageView.setAlpha(1f);
				}
				@Override
				public void onAnimationRepeat(Animator animation) {}
				@Override
				public void onAnimationEnd(Animator animation) {
					Log.d(TAG, "Stop Loading Animation");
					animation.setupStartValues();
					if (isOKEndAnimation) {
						circleBorderImageView.setImageDrawable(getSuccessDrawable());
					} else {
						circleBorderImageView.setImageDrawable(getErrorDrawable());
					}
					circleSplashImageView.setAlpha(0f);

					if (type == TYPE_OPTION_PLUS) {
						circleBgImageView.setImageDrawable(DrawableUtils.getDrawable(R.drawable.investButtonBgOptionPlus));
					}

					triangleImageView.setImageDrawable(getButtonDrawable(false));
				}
				@Override
				public void onAnimationCancel(Animator animation) {
					
				}
			});			
		}
		return loadingAnimation;
	}

	private Drawable getLoadingDrawable(){
		if(type == TYPE_DYNAMICS){
			return DrawableUtils.getDrawable(R.drawable.investButtonBorderDynamicsLoading);
		}else{
			return DrawableUtils.getDrawable(R.drawable.investButtonBorderLoading);
		}
	}

	private Drawable getSuccessDrawable(){
		if(type == TYPE_DYNAMICS){
			if(buttonType == BUTTON_TYPE_CALL){
				return DrawableUtils.getDrawable(R.drawable.investButtonBorderCircleGreenDynamics);
			}else{
				return DrawableUtils.getDrawable(R.drawable.investButtonBorderCircleRedDynamics);
			}
		}else{
			return DrawableUtils.getDrawable(R.drawable.investButtonBorderCircleBlue);
		}
	}

	private Drawable getErrorDrawable(){
		if(type == TYPE_DYNAMICS){
			return DrawableUtils.getDrawable(R.drawable.investButtonBorderCircleErrorDynamics);
		}else{
			return DrawableUtils.getDrawable(R.drawable.investButtonBorderCircleRed);
		}
	}

	private Drawable getButtonDrawable(boolean isAnimationStarted){
		if(buttonType == BUTTON_TYPE_CALL){
			if(type == TYPE_DYNAMICS){
				return DrawableUtils.getDrawable(R.drawable.investButtonDynamicsCallTriangle);
			}else
			if(type == TYPE_OPTION_PLUS){
				if(isAnimationStarted){
					return DrawableUtils.getDrawable(R.drawable.investButtonCallTriangleOptionPlus);
				}else{
					return DrawableUtils.getDrawable(R.drawable.investButtonCallTriangle);
				}
			}else{
				return DrawableUtils.getDrawable(R.drawable.investButtonCallTriangle);
			}
		}else{
			if(type == TYPE_DYNAMICS){
				return DrawableUtils.getDrawable(R.drawable.investButtonDynamicsPutTriangle);
			}else
			if(type == TYPE_OPTION_PLUS){
				if(isAnimationStarted){
					return DrawableUtils.getDrawable(R.drawable.investButtonPutTriangleOptionPlus);
				}else{
					return DrawableUtils.getDrawable(R.drawable.investButtonPutTriangle);
				}
			}else{
				return DrawableUtils.getDrawable(R.drawable.investButtonPutTriangle);
			}
		}
	}

	private Animator createFadeOutAnimator(){
		Animator fadeOutAnimator = AnimationUtils.createAlphaAnimator(circleBorderImageView, fadeOutDuration, 0f);
		fadeOutAnimator.setStartDelay(fadeOutDelay);
		fadeOutAnimator.setInterpolator(new LinearInterpolator()); 
		return fadeOutAnimator;
	} 

	public void startLoading(){
		if(state == State.NORMAL){
			stopLoadingAnimation();
			startLoadingAnimation();			
		}
	}
	public void stopLoading(boolean isOK){
		isOKEndAnimation = isOK;
		if(loadingAnimation != null){
			loadingAnimation.end();
		}
		enable();
	}
	public void enable(){
		setClickable(true);
		state = State.NORMAL;
		if(type == TYPE_DYNAMICS){
			circleBgDisabled.setVisibility(View.GONE);
			triangleImageView.setAlpha(1.0f);
			circleBgImageView.setAlpha(1.0f);
		}else{
			setAlpha(1.0f);
		}
	}
	public void disable(){
		setClickable(false);
		state = State.DISABLED;
		if(type == TYPE_DYNAMICS){
			circleBgDisabled.setVisibility(View.VISIBLE);
			triangleImageView.setAlpha(disabledTriangleAlpha);
			circleBgImageView.setAlpha(disabledTriangleAlpha);
		}else{
			setAlpha(disabledAlpha);
		}
	}
	
	public State getState(){
		return state;
	}

	/**
	 * Sets the isOptionsPlus flag and refreshes the UI.
	 */
	public void setupOptionPlus(boolean isOptionPlus) {
		setupUI();
	}
	
}