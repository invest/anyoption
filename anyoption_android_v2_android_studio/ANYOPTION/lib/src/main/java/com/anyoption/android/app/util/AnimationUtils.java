package com.anyoption.android.app.util;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import android.widget.TextView;

/**
 * Class that provides custom animations.
 * @author Anastas Arnaudov
 *
 */
public class AnimationUtils {

	/**
	 * Starts left-right horizontal scroll of the text in the given textView.
	 * @param textView
	 */
	public static void startHorizontalScrollAnimation(TextView textView, float velocity){

//		int textWidth = ScreenUtils.getTextWidth(textView);

		int textWidth = textView.getWidth();
		
		ViewGroup.MarginLayoutParams params = (MarginLayoutParams) textView.getLayoutParams();
		int margin = params.leftMargin + params.rightMargin;
		int padding = textView.getPaddingLeft() + textView.getPaddingRight();
	
		View parent = (View) textView.getParent();
		int parentWidth = parent.getWidth();
		int parentPadding = parent.getPaddingLeft() + parent.getPaddingRight();
		
//		int extra = 10;
		int delta = Math.abs(textWidth + margin + padding + parentPadding - parentWidth);

		ObjectAnimator animation = ObjectAnimator.ofInt(textView, "scrollX",delta);
		animation.setRepeatCount(ValueAnimator.INFINITE);
		animation.setRepeatMode(ValueAnimator.REVERSE);
		animation.setInterpolator(new LinearInterpolator());
		animation.setDuration((long) (delta / velocity));

		//Initial scroll:
		textView.setScrollX(0);
		
		animation.start();
	}

	/**
	 * Creates Scale Animator.
	 * @param view
	 * @param scaleX
	 * @param scaleY
	 * @param duration
	 * @return
	 */
	public static Animator createScaleAnimator(View view, float scaleX, float scaleY, long duration){
		ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(view, "scaleX", scaleX);
		ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(view, "scaleY", scaleY);
		AnimatorSet scaleAnimatorSet = new AnimatorSet();
		scaleAnimatorSet.setDuration(duration);
		scaleAnimatorSet.playTogether(scaleXAnimator, scaleYAnimator);
		return scaleAnimatorSet;
	}
	
	/**
	 * Creates Alpha Animator.
	 * @param view
	 * @param alpha
	 * @param duration
	 * @return
	 */
	public static Animator createAlphaAnimator(View view, long duration , float... alphas){
		ObjectAnimator alphaAnimator = ObjectAnimator.ofFloat(view, "alpha", alphas);
		alphaAnimator.setDuration(duration);
		return alphaAnimator;
	}
	
	/**
	 * Creates Rotate Animator.
	 * @param view
	 * @param rotation
	 * @param duration
	 * @return
	 */
	public static Animator createRotateAnimator(View view, float rotation, long duration){
		ObjectAnimator rotateAnimator = ObjectAnimator.ofFloat(view, "rotation", rotation);
		rotateAnimator.setDuration(duration);
		return rotateAnimator;
	}

	public static class HeightAnimation extends Animation {
		protected int originalHeight;
		protected View view;
		protected float perValue;

		public HeightAnimation(View view, int fromHeight, int toHeight) {
			this.view = view;
			this.perValue = (toHeight - fromHeight);
			this.originalHeight = fromHeight;
		}

		@Override
		protected void applyTransformation(float interpolatedTime, Transformation t) {
			view.getLayoutParams().height = (int) (originalHeight + perValue * interpolatedTime);
			view.requestLayout();
		}

		@Override
		public boolean willChangeBounds() {
			return true;
		}
	}
}
