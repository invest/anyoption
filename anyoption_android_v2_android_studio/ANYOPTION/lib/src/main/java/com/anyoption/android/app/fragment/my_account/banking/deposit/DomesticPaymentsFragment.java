package com.anyoption.android.app.fragment.my_account.banking.deposit;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.CurrencyMethodRequest;
import com.anyoption.json.results.CurrencyMethodResult;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * A simple {@link Fragment} subclass. 
 * <p>It manages the "DomesticPayments" screen and functionality.
 * @author Eyal G
 *
 */
public class DomesticPaymentsFragment extends NavigationalFragment {
	
	protected static final String TAG = "DomesticPaymentsActivity";
	protected final String MERCHANT_REFERENCE = "AYP";
	public static final int USER_ID_REFERENCE_DIGITS_NUMBER = 7;
	protected final String PAGE_DIRECTION = "ltr";
	protected static final String DEFAULT_CURRENCY_SYMBOL = "cur";
	
    // for Envoy

    protected String currencyLetter;
    protected String url;
    protected WebView webView;
    protected String userCurrencyCode;
	protected View rootView;
	
	/**
	 * Use this method to obtain new instance of the fragment.
	 * @return
	 */
	public static DomesticPaymentsFragment newInstance(){	
		return DomesticPaymentsFragment.newInstance(new Bundle());
	}

	/**
	 * Use this method to obtain new instance of the fragment.
	 * @return
	 */
	public static DomesticPaymentsFragment newInstance(Bundle bundle){
		DomesticPaymentsFragment depositWelcomeFragment = new DomesticPaymentsFragment();
		depositWelcomeFragment.setArguments(bundle);
		return depositWelcomeFragment;
	}
	


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.domestic_payments_layout, container, false);
		
		User user = application.getUser();
		
		userCurrencyCode = getUserCurrencyCode(user);
		
		setupWebView();
	    
	    CurrencyMethodRequest request = new CurrencyMethodRequest();
	    request.setCurrencyId(user.getCurrencyId());
	    Application.get().getCommunicationManager().requestService(DomesticPaymentsFragment.this, "getCurrencyLettersCallBack", Constants.SERVICE_GET_CURRENCY_LETTERS, 
	    		request, CurrencyMethodResult.class);
	    
		return rootView;
	}

	@SuppressLint("SetJavaScriptEnabled")
	@SuppressWarnings("deprecation")
	protected void setupWebView(){
		webView = (WebView) rootView.findViewById(R.id.webViewDomesticPayments);
		webView.getSettings().setJavaScriptEnabled(true);
	    webView.getSettings().setPluginState(PluginState.ON);
	    webView.setWebViewClient(new MyWebViewClient());
	}
	
	@Override
	protected Screenable setupScreen() {
		return Screen.DOMESTIC_PAYMENTS;
	}
	
	protected String getUserCurrencyCode(User user) {
		if(user != null && user.getCurrency() != null){
			return user.getCurrency().getNameKey();
		} else {
			Log.d(TAG, "Couldn't set customer's prefered currency. Setting default value - cur");
			return DEFAULT_CURRENCY_SYMBOL;
		}
	}
	
	public String getReferencedUserId(){
		User user = application.getUser();
		String userIdString = String.valueOf(user.getId());

		while (userIdString.length() < USER_ID_REFERENCE_DIGITS_NUMBER){
			userIdString = "0" + userIdString;
		}

		return userIdString;
	}
	
	
    public void getCurrencyLettersCallBack(Object result) {
    	Log.d(TAG, "getCurrencyLettersCallBack");
    	CurrencyMethodResult res = (CurrencyMethodResult) result;
    	currencyLetter = res.getCurrencyLetters();
    	
    	url = 	" <html> " +
		" 			<head> " +
		" 				<meta HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html;charset=UTF-8\" /> " +
		" 				<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\" /> " +
		" 			</head> " +
		" 			<body onload=\"document.envoyDepositForm.submit();\" dir=\"ltr\"> " +
		" 				<form name=\"envoyDepositForm\" method=\"post\" action=\"#{envoyInfo.redirectUrl}\"> " +
		" 					<input name=\"merchantId\" type=\"hidden\" value=\"#{envoyInfo.merchantId}\"/> " +
		" 					<input name=\"country\" type=\"hidden\" value=\"#{envoyInfo.countryA2}\"/> " +
		" 					<input name=\"language\" type=\"hidden\" value=\"#{envoyInfo.language}\"/> " +
		" 					<input name=\"customerRef\" type=\"hidden\" value=\"#{envoyInfo.customerRef}\"/> " +
		"					<input name=\"receiveCurrency\" type=\"hidden\" value=\"#{envoyInfo.currencySymbol}\"/> " +
		" 					<input name=\"email\" type=\"hidden\" value=\"#{envoyInfo.email}\"/> " +
		" 					<input name=\"service\" type=\"hidden\" value=\"#{envoyInfo.service}\"/> " +
		" 					<input type=\"image\" src=\"#{applicationData.imagesPath}/#{applicationData.userLocale.language}_#{applicationData.skinId}/redirecting.jpg\" name=\"continue\" value=\"Continue\"></input> " +
		" 				</form> " +
		" 			</body> " +
		" 		</html>";	    	
 			
		
		
		
		
		User user = application.getUser();
		String countryA2 = application.getCountriesMap().get(user.getCountryId()).getA2();
		user.getLocale().toString();
		
		url = url.replace("#{applicationData.pageDirection}", PAGE_DIRECTION);
		url = url.replace("#{envoyInfo.redirectUrl}", Application.get().getOneClickURL());
		url = url.replace("#{envoyInfo.merchantId}", Application.get().getMerchantID());		
		url = url.replace("#{envoyInfo.countryA2}", countryA2 );
		url = url.replace("#{envoyInfo.language}", user.getLocale().toString());
		url = url.replace("#{envoyInfo.customerRef}", MERCHANT_REFERENCE + userCurrencyCode + getReferencedUserId());
		url = url.replace("#{envoyInfo.currencySymbol}", currencyLetter);
		url = url.replace("#{envoyInfo.email}", user.getEmail());
		url = url.replace("#{envoyInfo.service}","");
		Log.d(TAG, url);
		webView.loadData(url , "text/html", "utf-8");
    }
    
	private class MyWebViewClient extends WebViewClient {
	    @Override
	    public boolean shouldOverrideUrlLoading(WebView view, String url) {
	    	if(url.contains("www.anyoption.com")){
	    		application.getCurrentActivity().onBackPressed();
	    	}else{
	    		view.loadUrl(url);
	    	}
	        return true;
	    }
	}
}

