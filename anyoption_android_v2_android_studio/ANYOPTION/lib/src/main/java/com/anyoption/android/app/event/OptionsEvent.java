package com.anyoption.android.app.event;


import com.anyoption.common.service.results.InvestmentsMethodResult;

public class OptionsEvent {

	/**
	 * Chart event type
	 */
	public enum Type {
		SETTLED_OPTION_RECEIVED
	}

	private long marketId;
	private Type type;
	private InvestmentsMethodResult result;

	public OptionsEvent(long marketId, Type type, InvestmentsMethodResult result) {
		this.marketId = marketId;
		this.type = type;
		this.result = result;
	}

	public long getMarketId() {
		return marketId;
	}

	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public InvestmentsMethodResult getResult() {
		return result;
	}

	public void setResult(InvestmentsMethodResult chartData) {
		this.result = chartData;
	}

}
