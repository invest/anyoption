package com.anyoption.android.app.widget.form;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.LayoutDirection;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioGroup;
import android.widget.TextView.OnEditorActionListener;

/**
 * Custom Radio Group.
 * @author Anastas Arnaudov
 *
 */
public class FormRadioGroup extends RadioGroup implements FormItem{

	private LayoutDirection direction;
	
	public FormRadioGroup(Context context) {
		super(context);
		init(context, null);
	}

	public FormRadioGroup(Context context, AttributeSet attrs) {
		super(context, attrs);
		if(!isInEditMode()){
			init(context, attrs);			
		}
	}

	/**
	 * Initial setup.
	 * @param context
	 * @param attrs
	 */
	private void init(Context context, AttributeSet attrs) {
		setDirection(Application.get().getLayoutDirection());
	}

 
	@Override
	public void setDirection(LayoutDirection direction) {
		if(this.direction != direction){
			this.direction = direction;
			setupUIForDirection();
		}
		this.direction = direction;
	}

	@Override
	public void setupUIForDirection() {
		for(int i = 0; i < getChildCount(); i++){
			FormRadioButton radioButton = (FormRadioButton) getChildAt(i);
			radioButton.setDirection(this.direction);
		}
	}

	@Override
	public LayoutDirection getDirection() {
		return this.direction;
	}

	@Override
	public boolean isEditable() {
		return isEnabled();
	}

	@Override
	public void setEditable(boolean isEditable) {
		setEnabled(isEditable);
	}

	@Override
	public boolean checkValidity() {
		return true;
	}

	@Override
	public String getValue() {
		int checkedRadioButtonID = getCheckedRadioButtonId();
		FormRadioButton checkedRadioButton = (FormRadioButton) this.findViewById(checkedRadioButtonID);
		return checkedRadioButton.getValue();
	}

	@Override
	public void setValue(Object value) {
		for(int i = 0; i < getChildCount(); i++){
			FormRadioButton radioButton = (FormRadioButton) getChildAt(i);
			if(radioButton.getValue().equals(value)){
				radioButton.setChecked(true);
			}
		}
		
	}

	@Override
	public void addOnEditorActionListener(OnEditorActionListener onEditorActionListener) {}

	@Override
	public void clear() {
		for(int i = 0; i < getChildCount(); i++){
			FormRadioButton radioButton = (FormRadioButton) getChildAt(i);
			radioButton.setChecked(false);
		}
	}

	@Override
	public void refreshUI() {
		setupUIForDirection();
		for(int i = 0; i < getChildCount(); i++){
			FormRadioButton radioButton = (FormRadioButton) getChildAt(i);
			radioButton.refreshUI();
		}
	}

	@Override
	public void setFieldName(String fieldName) {		
	}

	@Override
	public String getFieldName() {
		return null;
	}

	@Override
	public void displayError(String error) {

	}
}
