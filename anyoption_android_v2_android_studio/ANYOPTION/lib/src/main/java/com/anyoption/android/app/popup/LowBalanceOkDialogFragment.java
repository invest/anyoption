package com.anyoption.android.app.popup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.manager.PopUpManager;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.TextUtils.OnInnerTextClickListener;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.UserMethodResult;

public class LowBalanceOkDialogFragment extends OkDialogFragment {

	public static final String TAG = LowBalanceOkDialogFragment.class.getSimpleName();
	
	protected TextView firstMessageTextView;
	protected TextView secondMessageTextView;
	protected ImageView image;
	protected ProgressBar loading;
	protected long minInvestmentAmount;
	
	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static LowBalanceOkDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new LowBalanceOkDialogFragment.");
		LowBalanceOkDialogFragment fragment = new LowBalanceOkDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if(args != null){
			setCancelable(args.getBoolean(Constants.EXTRA_CANCELABLE, false));
			minInvestmentAmount = args.getLong(Constants.EXTRA_AMOUNT, 0l);
		}
	}

	@Override
	protected int getContentLayout() {
		return 0;
	}

	/**
	 * Set up content.
	 * 
	 * @param contentView
	 */
	@Override
	protected void onCreateContentView(View contentView) {
		
		View depositButton = contentView.findViewById(R.id.deposit_button);
		depositButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				application.postEvent(new NavigationEvent(Screen.DEPOSIT_MENU, NavigationType.DEEP));
				dismiss();
			}
		});
	}

	public void getUser() {
		application.getCommunicationManager().requestService(this, "getUserCallback", application.getServiceMethodGetUser(), new UserMethodRequest(), application.getUserMethodResultClass(), false, false);
	}

	@Override
	public int getPopupCode(){
		return PopUpManager.POPUP_LOW_BALANCE;
	}
}
