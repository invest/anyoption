package com.anyoption.android.app.model.questionnaire;

import java.util.List;

import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;

public class MultipleChoiceQuestion extends BaseQuestionItem {

	private QuestionnaireQuestion question;
	private List<QuestionnaireUserAnswer> userAnswers;

	public MultipleChoiceQuestion(long orderId, QuestionnaireQuestion question,
			List<QuestionnaireUserAnswer> userAnswers, String questionText, List<String> answerTexts) {
		super(orderId, questionText, answerTexts);
		this.question = question;
		this.userAnswers = userAnswers;
	}

	public MultipleChoiceQuestion(long orderId, String questionText, List<String> answerTexts) {
		this(orderId, null, null, questionText, answerTexts);
	}

	@Override
	public boolean isAnswered() {
		if (userAnswers == null) {
			return false;
		}

		for (QuestionnaireUserAnswer answer : userAnswers) {
			if (answer.getAnswerId() != null) {
				return true;
			}
		}

		return false;
	}

	public QuestionnaireQuestion getQuestion() {
		return question;
	}

	public void setQuestion(QuestionnaireQuestion question) {
		this.question = question;
	}

	public List<QuestionnaireUserAnswer> getUserAnswers() {
		return userAnswers;
	}

	/**
	 * TODO
	 * 
	 * @param userAnswers
	 */
	public void setUserAnswers(List<QuestionnaireUserAnswer> userAnswers) {
		this.userAnswers = userAnswers;
	}

}
