package com.anyoption.android.app.fragment.my_account.my_options;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.fragment.my_account.SettledOptionsFragment;
import com.anyoption.android.app.fragment.trade.OpenOptionsFragment;
import com.anyoption.android.app.model.DropDownPopupItem;
import com.anyoption.android.app.popup.DropdownPopup;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.AutoResizeTextView;

import java.util.ArrayList;

public class MyOptionsFragment extends NavigationalFragment implements DropdownPopup.DropdownPopupClickListener {

    public static MyOptionsFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created fragment MyOptionsFragment");
        MyOptionsFragment fragment = new MyOptionsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    public static final byte TAB_OPEN_OPTIONS = 1;
    public static final byte TAB_SETTLED_OPTIONS = 2;

    protected static final int PERIOD_DAY = 1;
    protected static final int PERIOD_WEEK = 2;
    protected static final int PERIOD_MONTH = 3;

    protected byte currentTab = TAB_OPEN_OPTIONS;
    protected int currentDropDownPosition = PERIOD_DAY;

    protected View rootView;
    protected View openOptionsTab;
    protected View settledOptionsTab;
    protected View currentTabLayout;
    protected ImageView dropDownImageView;
    protected AutoResizeTextView settledOptionsTextView;
    protected ArrayList<DropDownPopupItem> dropDownPopupItems;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "On Create");
        Bundle bundle = getArguments();
        if (bundle != null) {
            //Get arguments:
            currentTab = bundle.getByte(Constants.EXTRA_MY_OPTIONS_TAB, currentTab);
        }

        application.registerForEvents(this, NavigationEvent.class);
    }

    @Override
    public void onDestroy() {
        application.unregisterForAllEvents(this);
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.my_options_fragment, container, false);


        setupTabs();

        //Initial Tab:
        selectTab();
        openTab();

        return rootView;
    }

    //////////////////////////////////////////////////////////////////////
    //                            EVENTs                                //
    //////////////////////////////////////////////////////////////////////
    
    public void onEventMainThread(NavigationEvent event) {
        if (event.getToScreen() != Screen.MY_OPTIONS) {
            return;
        }

        Bundle args = event.getBundle();
        if (args != null && args.containsKey(Constants.EXTRA_MY_OPTIONS_TAB)) {
            byte newTab = args.getByte(Constants.EXTRA_MY_OPTIONS_TAB, currentTab);
            if (currentTab != newTab) {
                currentTab = newTab;
                selectTab();
                openTab();
            }
        }
    }

    //////////////////////////////////////////////////////////////////////

    protected void setupTabs() {
        openOptionsTab = rootView.findViewById(R.id.my_options_open_options_tab);
        settledOptionsTab = rootView.findViewById(R.id.my_options_settled_options_tab);
        dropDownImageView = (ImageView) rootView.findViewById(R.id.my_options_settled_dropdown_image_view);
        settledOptionsTextView = (AutoResizeTextView) rootView.findViewById(R.id.my_options_settled_text);
        currentTabLayout = rootView.findViewById(R.id.my_options_current_tab_layout);

        if (dropDownPopupItems == null || dropDownPopupItems.size() < 1) {
            currentDropDownPosition = PERIOD_DAY;
            changeSettledOptionsSelectedFilter(currentDropDownPosition);
            dropDownPopupItems = new ArrayList<DropDownPopupItem>();
            dropDownPopupItems.add(new DropDownPopupItem(PERIOD_DAY, getString(R.string.settled_options_dropdown_day_text), true));
            dropDownPopupItems.add(new DropDownPopupItem(PERIOD_WEEK, getString(R.string.settled_options_dropdown_week_text), false));
            dropDownPopupItems.add(new DropDownPopupItem(PERIOD_MONTH, getString(R.string.settled_options_dropdown_month_text), false));
        }

        OnClickListener tabClickListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == openOptionsTab) {
                    if (currentTab == TAB_OPEN_OPTIONS) {
                        return;
                    }
                    currentTab = TAB_OPEN_OPTIONS;
                } else if (v == settledOptionsTab) {
                    if (currentTab == TAB_SETTLED_OPTIONS) {
                        return;
                    }
                    currentTab = TAB_SETTLED_OPTIONS;
                }

                selectTab();
                openTab();
            }
        };

        OnClickListener dropDownClickListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentTab == TAB_OPEN_OPTIONS) {
                    currentTab = TAB_SETTLED_OPTIONS;
                    selectTab();
                    openTab();
                }

                if (currentTab == TAB_SETTLED_OPTIONS) {
                    dropDownImageView.setImageResource(R.drawable.dd_arrow_up);
                    DropdownPopup.showPopup(dropDownPopupItems, dropDownImageView, MyOptionsFragment.this);
                }
            }
        };

        openOptionsTab.setOnClickListener(tabClickListener);
        settledOptionsTab.setOnClickListener(tabClickListener);
        dropDownImageView.setOnClickListener(dropDownClickListener);
    }

    protected void changeSettledOptionsSelectedFilter(int periodDay) {
        if(periodDay == PERIOD_DAY) {
            settledOptionsTextView.setText(getString(R.string.settledUpper) + "(" + getString(R.string.settled_options_dropdown_day_short_text) + ")");
        } else if(periodDay == PERIOD_WEEK) {
            settledOptionsTextView.setText(getString(R.string.settledUpper) + "(" + getString(R.string.settled_options_dropdown_week_short_text) + ")");
        } else if(periodDay == PERIOD_MONTH) {
            settledOptionsTextView.setText(getString(R.string.settledUpper) + "(" + getString(R.string.settled_options_dropdown_month_short_text) + ")");
        } else {
            settledOptionsTextView.setText(getString(R.string.settledUpper) + "(" + getString(R.string.settled_options_dropdown_day_short_text) + ")");
        }
    }

    @Override
    public void onDropdownPopupClick(DropDownPopupItem element) {
        dropDownImageView.setImageResource(R.drawable.dd_arrow_down);
        if(element.getPosition() == PERIOD_DAY) {
            currentDropDownPosition = PERIOD_DAY;
        } else if(element.getPosition() == PERIOD_WEEK) {
            currentDropDownPosition = PERIOD_WEEK;
        } else if(element.getPosition() == PERIOD_MONTH) {
            currentDropDownPosition = PERIOD_MONTH;
        }
        changeSettledOptionsSelectedFilter(currentDropDownPosition);
        openFilteredSettledTab();
    }

    @Override
    public void onDropdownPopupDismissed() {
        dropDownImageView.setImageResource(R.drawable.dd_arrow_down);
    }

    protected void selectTab() {
        if (currentTab == TAB_OPEN_OPTIONS) {
            openOptionsTab.setBackgroundColor(ColorUtils.getColor(R.color.my_options_tab_pressed_bg));
            settledOptionsTab.setBackgroundColor(ColorUtils.getColor(R.color.my_options_tab_bg));
        } else if (currentTab == TAB_SETTLED_OPTIONS) {
            settledOptionsTab.setBackgroundColor(ColorUtils.getColor(R.color.my_options_tab_pressed_bg));
            openOptionsTab.setBackgroundColor(ColorUtils.getColor(R.color.my_options_tab_bg));
        }
    }

    protected void openTab() {
        if (currentTab == TAB_OPEN_OPTIONS) {
            application.getFragmentManager().replaceInnerFragment(currentTabLayout.getId(), OpenOptionsFragment.class, this);
        } else if (currentTab == TAB_SETTLED_OPTIONS) {
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.EXTRA_PERIOD, currentDropDownPosition);
            application.getFragmentManager().replaceInnerFragment(currentTabLayout.getId(), SettledOptionsFragment.class, this, bundle);
        }
    }

    protected void openFilteredSettledTab() {
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.EXTRA_PERIOD, currentDropDownPosition);
        application.getFragmentManager().replaceInnerFragment(currentTabLayout.getId(), SettledOptionsFragment.class, this, bundle);
    }

    @Override
    protected Screenable setupScreen() {
        return Screen.MY_OPTIONS;
    }
}
