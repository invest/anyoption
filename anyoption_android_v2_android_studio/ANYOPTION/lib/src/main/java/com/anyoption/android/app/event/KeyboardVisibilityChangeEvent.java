package com.anyoption.android.app.event;

/**
 * Custom Event that occurs when the virtual keyboard changes its visibility.
 * @author Anastas Arnaudov
 *
 */
public class KeyboardVisibilityChangeEvent {

	private boolean isVisible;

	public KeyboardVisibilityChangeEvent(boolean isVisible){
		this.isVisible = isVisible;
	}
	
	public boolean isVisible() {
		return isVisible;
	}

	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}
	
}
