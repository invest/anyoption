package com.anyoption.android.app.fragment.my_account.questionnaire;

import java.util.List;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.manager.PopUpManager.OnConfirmPopUpButtonPressedListener;
import com.anyoption.android.app.model.questionnaire.BaseQuestionItem;
import com.anyoption.android.app.model.questionnaire.CheckboxQuestion;
import com.anyoption.android.app.model.questionnaire.YesNoQuestion;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.json.results.UserQuestionnaireResult;

import android.os.Bundle;
import android.util.Log;

public class MandatoryQuestionnaireFragment extends BaseQuestionnaireFragment {
	
	public static final String TAG = MandatoryQuestionnaireFragment.class.getSimpleName();

	private static final long[] QUSTION_ORDER_IDs = new long[] { 1, 2, 3, 4, 5, 6, 7 };

	private static final int[] QUSTION_TEXT_IDs = new int[] {
			R.string.questionnaire_mandatory_question1, R.string.questionnaire_mandatory_question2,
			R.string.questionnaire_mandatory_question3, R.string.questionnaire_mandatory_question4,
			R.string.questionnaire_mandatory_question5, R.string.questionnaire_mandatory_question6,
			R.string.questionnaire_mandatory_question7 };

	private static final long INVISIBLE_QUESTION_ORDER_ID = 5;

	public static MandatoryQuestionnaireFragment newInstance() {
		return new MandatoryQuestionnaireFragment();
	}

	public static MandatoryQuestionnaireFragment newInstance(Bundle bundle) {
		MandatoryQuestionnaireFragment fragment = new MandatoryQuestionnaireFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	protected String getTitle() {
		return getString(R.string.questionnaire_mandatory_subheader);
	}

	@Override
	protected String getCurrentUserAnswersMethod() {
		return Constants.SERVICE_GET_MANDATORY_QUESTIONNAIRIE;
	}

	@Override
	protected String getSubmitRequestMethod() {
		return Constants.SERVICE_UPDATE_MANDATORY_QUESTIONNAIRIE;
	}

	@Override
	protected void initQuestions(List<BaseQuestionItem> questionsList) {
		for (int i = 0; i < QUSTION_ORDER_IDs.length - 1; i++) {
			BaseQuestionItem question = new YesNoQuestion(QUSTION_ORDER_IDs[i],
					getString(QUSTION_TEXT_IDs[i]));
			questionsList.add(question);

			// Set the question visibility to false, so it won't be added in the list.
			if (QUSTION_ORDER_IDs[i] == INVISIBLE_QUESTION_ORDER_ID) {
				question.setVisible(false);
			}
		}

		questionsList.add(new CheckboxQuestion(QUSTION_ORDER_IDs[QUSTION_ORDER_IDs.length - 1],
				getString(QUSTION_TEXT_IDs[QUSTION_TEXT_IDs.length - 1])));
	}

	@Override
	protected void getSubmitResultCallback(UserMethodResult result) {
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			if (result.getUserRegulation().getApprovedRegulationStep() >= UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE) {
				showThankYouPopup();
			} else if (result.getUserRegulation().isSuspended()
					&& !result.getUserRegulation().isSuspendedDueDocuments()) {
				ErrorUtils.handleError(Constants.ERROR_CODE_REGULATION_SUSPENDED);
				Log.d(TAG, "The user is suspended!");
			} else if (result.getUserRegulation().isSuspendedDueDocuments()) {
				ErrorUtils.handleError(Constants.ERROR_CODE_REGULATION_SUSPENDED_DOCUMENTS);
				Log.d(TAG, "The user is suspended due documents.");
			}
		}
	}

	@Override
	protected Screenable setupScreen() {
		return Screen.MANDATORY_QUESTIONNAIRE;
	}
	
	private void showThankYouPopup() {
		application.getPopUpManager().showConfirmPopUp(null,
				getString(R.string.questionnaire_mandatory_thankyou),
				getString(R.string.questionnaire_fill), getString(R.string.notNow),
				new OnConfirmPopUpButtonPressedListener() {

					@Override
					public void onPositive() {
						application.postEvent(new NavigationEvent(
								Screen.OPTIONAL_QUESTIONNAIRE, NavigationType.HORIZONTAL));
					}

					@Override
					public void onNegative() {
						openHomeScreen();
					}
				});
	}

	@Override
	public void getCurrentUserAnswersCallback(Object resultObj) {
		super.getCurrentUserAnswersCallback(resultObj);

		UserQuestionnaireResult result = (UserQuestionnaireResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			userAnswers = result.getUserAnswers();

			// Set the default answer for every question if none is selected.
			boolean answersChanged = false;
			if (userAnswers.get(0).getAnswerId() == null) {
				YesNoQuestion question = (YesNoQuestion) questions.get(0);
				userAnswers.get(0).setAnswerId(question.getQuestion().getAnswers().get(0).getId());
				answersChanged = true;
			}

			if (userAnswers.get(1).getAnswerId() == null) {
				YesNoQuestion question = (YesNoQuestion) questions.get(1);
				userAnswers.get(1).setAnswerId(question.getQuestion().getAnswers().get(0).getId());
				answersChanged = true;
			}

			if (userAnswers.get(2).getAnswerId() == null) {
				YesNoQuestion question = (YesNoQuestion) questions.get(2);
				userAnswers.get(2).setAnswerId(question.getQuestion().getAnswers().get(0).getId());
				answersChanged = true;
			}

			if (userAnswers.get(3).getAnswerId() == null) {
				YesNoQuestion question = (YesNoQuestion) questions.get(3);
				userAnswers.get(3).setAnswerId(question.getQuestion().getAnswers().get(0).getId());
				answersChanged = true;
			}

			if (userAnswers.get(4).getAnswerId() == null) {
				// This is the hidden answer.
				YesNoQuestion question = (YesNoQuestion) questions.get(4);
				userAnswers.get(4).setAnswerId(question.getQuestion().getAnswers().get(0).getId());
				answersChanged = true;
			}

			if (userAnswers.get(5).getAnswerId() == null) {
				YesNoQuestion question = (YesNoQuestion) questions.get(5);
				userAnswers.get(5).setAnswerId(question.getQuestion().getAnswers().get(1).getId());
				answersChanged = true;
			}

			if (userAnswers.get(6).getAnswerId() == null) {
				CheckboxQuestion question = (CheckboxQuestion) questions.get(6);
				userAnswers.get(6).setAnswerId(question.getQuestion().getAnswers().get(0).getId());
				answersChanged = true;
			}

			if (answersChanged) {
				updateQuestions(result.getQuestions(), userAnswers);

				Log.d(TAG, "Request to update the aswers with the default.");
				updateUserAnswers();
			}
		} 
	}

}
