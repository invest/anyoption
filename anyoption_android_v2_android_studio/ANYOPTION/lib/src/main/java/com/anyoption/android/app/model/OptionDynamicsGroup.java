package com.anyoption.android.app.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Anastas Arnaudov
 */
public class OptionDynamicsGroup {

    public enum Type{
        OPEN_ABOVE,
        OPEN_BELLOW,
        SETTLED
    }

    private Type type;
    private List<OptionDynamicsItem> itemsList;


    public OptionDynamicsGroup(Type type){
        this.type = type;
        itemsList = new ArrayList<OptionDynamicsItem>();
    }

    public Type getTpe(){
        return type;
    }

    public List<OptionDynamicsItem> getItemsList(){
        return itemsList;
    }
}
