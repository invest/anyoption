package com.anyoption.android.app.widget.chart;

import com.anyoption.android.app.lightstreamer.ChartDataListener;

/**
 * @author Anastas Arnaudov
 */
public interface ChartDrawable extends ChartDataListener {

    public void clearData();
    public void zoomIn();
    public void zoomOut();
    public void removeZoom();
    public boolean isZoomedIn();
    public void setOnScrollToLeftEdgeListener(OnScrollToLeftEdgeListener onScrollToLeftEdgeListener);

}
