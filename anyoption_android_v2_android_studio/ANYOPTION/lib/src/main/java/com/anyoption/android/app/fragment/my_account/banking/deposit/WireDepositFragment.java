package com.anyoption.android.app.fragment.my_account.banking.deposit;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.common.beans.base.Country;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;

public class WireDepositFragment extends NavigationalFragment implements OnClickListener {

    private User user;
    @Deprecated
    private boolean isSendButtonEnabled = true;
    private RequestButton sendButton;

    @Override
    protected Screenable setupScreen() {
        return Screen.WIRE_DEPOSIT;
    }

    /**
     * Use this method to get new instance of the fragment.
     *
     * @return
     */
    public static WireDepositFragment newInstance(Bundle bundle) {
        WireDepositFragment wireDepositFragment = new WireDepositFragment();
        wireDepositFragment.setArguments(bundle);
        return wireDepositFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        user = application.getUser();
        View rootView = inflater.inflate(R.layout.wire_deposit_layout, container, false);
        setParamsGeneral(user, rootView);
        return rootView;
    }

    public void setParamsGeneral(User u, View rootView) {
        // Fill bank details
        long skinId     = user.getSkinId();
        long userCurId  = user.getCurrencyId();

        // Choose bank details to be displayed.
        if (application.isRegulated()) {
            if (userCurId == Constants.CURRENCY_EUR_ID) {
                setDetailsRegulated(u, rootView);
            } else if (userCurId == Constants.CURRENCY_CZK_ID) {
                setDetailsRegulatedNonEur(rootView);
            } else {
                setDetailsRegulatedNonEur(rootView);
            }

        } else if (
                userCurId == Constants.CURRENCY_USD_ID ||
                userCurId == Constants.CURRENCY_GBP_ID ||
                userCurId == Constants.CURRENCY_TRY_ID ||
                userCurId == Constants.CURRENCY_CNY_ID ||
                userCurId == Constants.CURRENCY_RUB_ID ||
                userCurId == Constants.CURRENCY_CZK_ID ||
                userCurId == Constants.CURRENCY_KRW_ID    ) {
            if(skinId == Skin.SKIN_ENGLISH_NON_REG || skinId == Skin.SKIN_SPAIN_NON_REG){
                setDetailsNotRegEnglishSpanishUSD(rootView);
            }else{
                setDetailsUSDBank(rootView);
            }
        } else if (userCurId == Constants.CURRENCY_EUR_ID) {
            if(skinId == Skin.SKIN_ENGLISH_NON_REG || skinId == Skin.SKIN_SPAIN_NON_REG){
                setDetailsNotRegEnglishSpanishEUR(rootView);
            }else{
                setDetailsEURGBPTRYBank(rootView);
            }
        }

        sendButton = (RequestButton) rootView.findViewById(R.id.buttonWireDepositSendDetails);
        sendButton.setOnClickListener(this);

        String supportEmail = getString(R.string.wireTransferScreenSupportEmail);
        Country country = application.getCountriesMap().get(u.getCountryId());
        String supportFax = country != null ? country.getSupportFax() : "";
        TextView textViewConfirmation = (TextView) rootView.findViewById(R.id.textViewWireDepositTransferConfirmation);
        Spanned textConfirmation = Html.fromHtml(getString(R.string.wireTransferScreenFooter2, supportFax, supportEmail));
        SpannableStringBuilder clickableText = TextUtils.makeUnderlineClickable(textConfirmation, new ClickableSpan() {

            @Override
            public void onClick(View widget) {
                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{getString(R.string.wireTransferScreenSupportEmail)});
                emailIntent.setType("plain/text");
                startActivity(Intent.createChooser(emailIntent, ""));

            }
        }, 0);
        if (textViewConfirmation != null) {
            textViewConfirmation.setMovementMethod(LinkMovementMethod.getInstance());
            textViewConfirmation.setText(clickableText);
        }
    }

    @Override
    public void onClick(final View v) {
        if (v.getId() == R.id.buttonWireDepositSendDetails) {
            if (isSendButtonEnabled) {
                sendButton.setEnabled(false);
                isSendButtonEnabled = false;

                if (RegulationUtils.checkRegulationStatusAndDoNecessary(false)) {
                    sendBankTransferDetailsUser(v);
                }

            }
        }
    }

    public void sendBankTransferDetailsUser(View target) {
        UserMethodRequest request = new UserMethodRequest();
        request.setUserName(user.getUserName());
        request.setPassword(user.getEncryptedPassword());
        request.setEncrypt(true);
        request.setSkinId(user.getSkinId());
        if (!application.getCommunicationManager().requestService(this, "sendBankTransferDetailsUserCallBack", Constants.SERVICE_SEND_BANK_TRANSFER_DETAILS_USER, request, MethodResult.class)) {
            isSendButtonEnabled = true;
            sendButton.setEnabled(true);
        }
    }

    public void sendBankTransferDetailsUserCallBack(Object result) {
        MethodResult methodResult = (MethodResult) result;
        if (methodResult.getErrorCode() != Constants.ERROR_CODE_SUCCESS) {
            Log.d(TAG, "Error Send Bank Details");
            isSendButtonEnabled = true;
            sendButton.setEnabled(true);
            return;
        }
        application.getCurrentActivity().onBackPressed();
    }

    public void setDetailsRegulated(User u, View rootView) {
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBeneficaryName)). setText(getString(R.string.wireTransferScreenRegulatedBeneficiaryName));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositAccountNumber)).  setText(getString(R.string.wireTransferScreenRegulatedAccountNumber));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositIBAN)).           setText(getString(R.string.wireTransferScreenRegulatedIban));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositSwift)).          setText(getString(R.string.wireTransferScreenRegulatedSwift));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankName)).       setText(getString(R.string.wireTransferScreenRegulatedBankName));
        if (user.getCountryId() == Constants.COUNTRY_GERMANY_ID) {
            ((TextView) rootView.findViewById(R.id.textViewWireDeposittextViewBankCode)).setText(getString(R.string.wireTransferScreenRegulatedBankCode));
            rootView.findViewById(R.id.textViewWireDeposittextViewBankCode).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.textViewWireDeposittextViewBankCodeHead).setVisibility(View.VISIBLE);
        }
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankAddress1)).   setText(getString(R.string.wireTransferScreenRegulatedAddressLine1));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankAddress2)).   setText(getString(R.string.wireTransferScreenRegulatedAddressLine2));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankAddress3)).   setText(getString(R.string.wireTransferScreenRegulatedAddressLine3));
    }

    public void setDetailsRegulatedNonEur(View rootView) {
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBeneficaryName)). setText(getString(R.string.wireTransferScreenRegulatedNonEuroBeneficiaryName));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositAccountNumber)).  setText(getString(R.string.wireTransferScreenRegulatedNonEuroAccountNumber));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositIBAN)).           setText(getString(R.string.wireTransferScreenRegulatedNonEuroIban));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositSwift)).          setText(getString(R.string.wireTransferScreenRegulatedNonEuroSwift));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankName)).       setText(getString(R.string.wireTransferScreenRegulatedNonEuroBankName));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankAddress1)).   setText(getString(R.string.wireTransferScreenRegulatedNonEuroAddressLine1));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankAddress2)).   setText(getString(R.string.wireTransferScreenRegulatedNonEuroAddressLine2));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankAddress3)).   setText(getString(R.string.wireTransferScreenRegulatedNonEuroAddressLine3));
    }

    public void setDetailsUSDBank(View rootView) {
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBeneficaryName)). setText(getString(R.string.wireTransferScreenUSDBeneficiaryName));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositAccountNumber)).  setText(getString(R.string.wireTransferScreenUSDAccountNumber));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositIBAN)).           setText(getString(R.string.wireTransferScreenUSDIban));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositSwift)).          setText(getString(R.string.wireTransferScreenUSDSwift));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankName)).       setText(getString(R.string.wireTransferScreenUSDBankName));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankAddress1)).   setText(getString(R.string.wireTransferScreenUSDAddressLine1));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankAddress2)).   setText(getString(R.string.wireTransferScreenUSDAddressLine2));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankAddress3)).   setText(getString(R.string.wireTransferScreenUSDAddressLine3));
    }

    public void setDetailsEURGBPTRYBank(View rootView) {
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBeneficaryName)). setText(getString(R.string.wireTransferScreenBeneficiaryName));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositAccountNumber)).  setText(getString(R.string.wireTransferScreenAccountNumber));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositIBAN)).           setText(getString(R.string.wireTransferScreenIban));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositSwift)).          setText(getString(R.string.wireTransferScreenSwift));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankName)).       setText(getString(R.string.wireTransferScreenBankName));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankAddress1)).   setText(getString(R.string.wireTransferScreenAddressLine1));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankAddress2)).   setText(getString(R.string.wireTransferScreenAddressLine2));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankAddress3)).   setText(getString(R.string.wireTransferScreenAddressLine3));
    }

    public void setDetailsNotRegEnglishSpanishEUR(View rootView) {
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBeneficaryName)). setText(getString(R.string.wireTransferScreenBeneficiaryName));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositAccountNumber)).  setText(getString(R.string.wireTransferScreenAccountNumber));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositIBAN)).           setText(getString(R.string.wireTransferScreenIban));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositSwift)).          setText(getString(R.string.wireTransferScreenRegulatedSwift));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankName)).       setText(getString(R.string.wireTransferScreenBankName));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankAddress1)).   setText(getString(R.string.wireTransferScreenRegulatedNonEuroBankName) + ",");
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankAddress2)).   setText(getString(R.string.wireTransferScreenNotRegulatedAddressPostCode) + " " + getString(R.string.wireTransferScreenRegulatedNonEuroAddressLine1) + ",");
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankAddress3)).   setText(getString(R.string.wireTransferScreenRegulatedNonEuroAddressLine3));
    }

    public void setDetailsNotRegEnglishSpanishUSD(View rootView) {
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBeneficaryName)). setText(getString(R.string.wireTransferScreenBeneficiaryName));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositAccountNumber)).  setText(getString(R.string.wireTransferScreenUSDAccountNumber));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositIBAN)).           setText(getString(R.string.wireTransferScreenUSDIban));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositSwift)).          setText(getString(R.string.wireTransferScreenRegulatedSwift));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankName)).       setText(getString(R.string.wireTransferScreenUSDBankName));
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankAddress1)).   setText(getString(R.string.wireTransferScreenRegulatedNonEuroBankName) + ",");
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankAddress2)).   setText(getString(R.string.wireTransferScreenNotRegulatedAddressPostCode) + " " + getString(R.string.wireTransferScreenRegulatedNonEuroAddressLine1) + ",");
        ((TextView) rootView.findViewById(R.id.textViewWireDepositBankAddress3)).   setText(getString(R.string.wireTransferScreenRegulatedNonEuroAddressLine3));
    }
}
