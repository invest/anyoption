package com.anyoption.android.app.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;
import java.util.Set;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.manager.DataBaseManager;
import com.anyoption.android.app.manager.SharedPreferencesManager;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.CreditCardType;
import com.anyoption.common.beans.base.Market;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.Log;

/**
 * Class for common utility methods.
 * 
 * @author Anastas Arnaudov
 * 
 */
public class Utils {

	public static final String TAG = Utils.class.getSimpleName();

	private static final String DECIMAL_PATTERN_NO_DIGITS = "###,###,##0";

	/**
	 * Returns a Unique Device ID (UDID).
	 * 
	 * @return
	 */
	public static String getUDID() {

		Context context = Application.get();

		// Requires READ_PHONE_STATE
		TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

		// gets the imei (GSM) or MEID/ESN (CDMA)
		String imei = telephonyManager.getDeviceId();
		if (null != imei) {
			return imei;
		}

		// Get some of the hardware information
		String buildParams = Build.BOARD + Build.BRAND + Build.CPU_ABI + Build.DEVICE + Build.DISPLAY
				+ Build.FINGERPRINT + Build.HOST + Build.ID + Build.MANUFACTURER + Build.MODEL + Build.PRODUCT
				+ Build.TAGS + Build.TYPE + Build.USER;

		// gets the android-assigned id
		String androidId = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);

		// requires ACCESS_WIFI_STATE
		WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

		// gets the MAC address
		String macAddress = wifiManager.getConnectionInfo().getMacAddress();

		// concatenate the string
		String fullHash = buildParams + imei + androidId + macAddress;

		return Utils.md5(fullHash);
	}


	public static String getImei() {
		TelephonyManager telephonyManager = (TelephonyManager) Application.get().getSystemService(Context.TELEPHONY_SERVICE);
		String imei = telephonyManager.getDeviceId();
		if (imei != null) {
			return imei;
		}
		return "";
	}

	/**
	 * Encrypts the given text with MD5 algorithm.
	 * 
	 * @param toConvert
	 * @return
	 */
	public static String md5(String toConvert) {
		String retVal = "";
		MessageDigest algorithm;
		try {
			algorithm = MessageDigest.getInstance("MD5");
			algorithm.reset();
			algorithm.update(toConvert.getBytes());
			byte messageDigest[] = algorithm.digest();

			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++) {
				hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
			}

			retVal = hexString + "";
		} catch (NoSuchAlgorithmException e) {
			Log.d(TAG, "cant md5 string " + toConvert);
		}

		return retVal;
	}

	/**
	 * Check if some variable is empty or not.
	 *
	 * @param par
	 *            The parameter that need to check if he is empty or not
	 */
	public static boolean isParameterEmptyOrNull(String par) {
		if ((par == null || par.length() == 0)) {
			return true;
		}
		return false;
	}

	/**
	 * <b>THIS METHOD MUST NOT BE USED!<br/>
	 * This info is returned from json request.</b><br/>
	 * <br/>
	 * 
	 * Gets the corresponding Market details.
	 * 
	 * @param market
	 * @return The res id of the text or 0 if there isn't details for this
	 *         market.
	 */
	@Deprecated
	public static int getMarketDetailsTextResID(Market market) {
		if (market == null) {
			return 0;
		}

		StringBuilder sb = new StringBuilder("CMS.assetsIndex.text.market");
		sb.append(market.getId()).append(".t1");

		return TextUtils.getTextResID(sb.toString());
	}

	/**
	 * Get credit card's type by credit card's number.
	 */
	public static int getCcTypeByNumber(String ccNumber) {
		if (!Utils.isParameterEmptyOrNull(ccNumber) && ccNumber.trim().length() > 1) {
			if (ccNumber.substring(0, 1).equals(String.valueOf(CreditCardType.CC_TYPE_IDENTIFY_VISA))) {
				return CreditCardType.CC_TYPE_VISA;
			}
			String ccNumFirstDigitsStr = ccNumber.substring(0, 2);
			long ccNumFirstDigits = Long.valueOf(ccNumFirstDigitsStr);
			if (ccNumFirstDigits >= CreditCardType.CC_TYPE_IDENTIFY_MASTERCARD_START
					&& ccNumFirstDigits <= CreditCardType.CC_TYPE_IDENTIFY_MASTERCARD_END) {
				return CreditCardType.CC_TYPE_MASTERCARD;
			} else if (ccNumFirstDigits == CreditCardType.CC_TYPE_IDENTIFY_AMEX_A
					|| ccNumFirstDigits == CreditCardType.CC_TYPE_IDENTIFY_AMEX_B) {
				return CreditCardType.CC_TYPE_AMEX;
			}
		}
		return CreditCardType.CC_TYPE_MASTERCARD;
	}

	/**
	 * Get support phone.
	 * 
	 * @return supportPhone
	 */
	public static String getSupportPhone() {
		String supportPhone = null;
		DataBaseManager databaseManager = null;
		Cursor cursor = null;
		Application application = null;
		try {
			application = Application.get();
			databaseManager = application.getDataBaseManager();
			databaseManager.openReadableDB();
			cursor = databaseManager.getSupportPhone(application.getSkinId());
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				supportPhone = cursor.getString(0);
			}
		} finally {
			if (cursor != null) {
				cursor.close();
			}
			if (databaseManager != null) {
				databaseManager.close();
			}
		}
		return supportPhone;
	}

	public static void setMarketingParams(Long combId, String dynamicParam, String affSub1, String affSub2, String gclid) {
		SharedPreferencesManager spManager = Application.get().getSharedPreferencesManager();
		spManager.putString(Constants.PREFERENCES_DYNAMIC_PARAM, dynamicParam);
		spManager.putString(Constants.PREFERENCES_AFF_SUB1, affSub1);
		spManager.getString(Constants.PREFERENCES_AFF_SUB2, affSub2);
		spManager.putString(Constants.PREFERENCES_GCLID, gclid);
		spManager.putLong(Constants.PREFERENCES_COMBINATION_ID, (combId != null) ? combId : 0L);

		Log.d(TAG, "setMarketingParams: combid: " + String.valueOf((combId != null) ? combId : 0L));

		Application.get().getSharedPreferencesManager().putBoolean(Constants.PREFERENCES_IS_CAME_FROM_MARKETING, true);
	}

	/**
	 * Get combination id if we have it, else return null.
	 * 
	 * @return combination id or null if we don't have.
	 */
	public static Long getCombinationId() {
		SharedPreferencesManager spManager = Application.get().getSharedPreferencesManager();
		Long combId = spManager.getLong(Constants.PREFERENCES_COMBINATION_ID, null);
		if (null != combId) {
			return combId;
		}
		return null;
	}

	/**
	 * get dp if we have it else return null
	 * 
	 * @return dp or null if we dont have
	 */
	public static String getDynamicParam() {
		SharedPreferencesManager spManager = Application.get().getSharedPreferencesManager();
		return spManager.getString(Constants.PREFERENCES_DYNAMIC_PARAM, null);
	}

	/**
	 * get Referrer if we have it else return null
	 * 
	 * @param context
	 * @return Referrer or null if we don't have
	 */
	public static String getReferrerParam(Context context) {
		SharedPreferencesManager spManager = Application.get().getSharedPreferencesManager();
		return spManager.getString(Constants.PREFERENCES_REFERRER_PARAM, null);
	}

	/**
	 * get MID if we have it else return null
	 * 
	 * @param context
	 * @return MID or null if we don't have
	 */
	public static String getmId(Context context) {
		SharedPreferencesManager spManager = Application.get().getSharedPreferencesManager();
		return spManager.getString(Constants.PREFERENCES_MID, null);
	}

	/**
	 * Gets EtsMId value.
	 * 
	 * @param context
	 * @return EtsMId or null if we don't have it saved.
	 */
	public static String getEtsMId(Context context) {
		SharedPreferencesManager spManager = Application.get().getSharedPreferencesManager();
		return spManager.getString(Constants.PREFERENCES_ETS_MID, null);
	}

	public static void setmIdIfValid(String newmId) {
		if (newmId != null && newmId.length() > 0) {
			SharedPreferencesManager spManager = Application.get().getSharedPreferencesManager();
			spManager.putString(Constants.PREFERENCES_MID, newmId);
		}
	}

	/**
	 * Sets the new EtsMId value if it is valid.
	 * 
	 * @param context
	 * @param newEtsMId
	 *            the value to set.
	 */
	public static void setEtsMId(Context context, String newEtsMId) {
		if (newEtsMId != null && newEtsMId.length() > 0) {
			SharedPreferencesManager spManager = Application.get().getSharedPreferencesManager();
			spManager.putString(Constants.PREFERENCES_ETS_MID, newEtsMId);
		}
	}

	/**
	 * Gets affSub1 value.
	 * 
	 * @param context
	 * @return affSub1 or null if we don't have it saved.
	 */
	public static String getAffSub1(Context context) {
		SharedPreferencesManager spManager = Application.get().getSharedPreferencesManager();
		return spManager.getString(Constants.PREFERENCES_AFF_SUB1, null);
	}

	/**
	 * Sets the new affSub1 value if it is valid.
	 * 
	 * @param context
	 * @param newAffSub1
	 *            the value to set.
	 */
	public static void setAffSub1(Context context, String newAffSub1) {
		if (newAffSub1 != null && newAffSub1.length() > 0) {
			SharedPreferencesManager spManager = Application.get().getSharedPreferencesManager();
			spManager.putString(Constants.PREFERENCES_AFF_SUB1, newAffSub1);
		}
	}

	/**
	 * Gets affSub2 value.
	 * 
	 * @param context
	 * @return affSub2 or null if we don't have it saved.
	 */
	public static String getAffSub2(Context context) {
		SharedPreferencesManager spManager = Application.get().getSharedPreferencesManager();
		return spManager.getString(Constants.PREFERENCES_AFF_SUB2, null);
	}

	/**
	 * Sets the new affSub2 value if it is valid.
	 * 
	 * @param context
	 * @param newAffSub2
	 *            the value to set.
	 */
	public static void setAffSub2(Context context, String newAffSub2) {
		if (newAffSub2 != null && newAffSub2.length() > 0) {
			SharedPreferencesManager spManager = Application.get().getSharedPreferencesManager();
			spManager.putString(Constants.PREFERENCES_AFF_SUB2, newAffSub2);
		}
	}

	/**
	 * Gets gclid value.
	 * 
	 * @param context
	 * @return gclid or null if we don't have it saved.
	 */
	public static String getGclid(Context context) {
		SharedPreferencesManager spManager = Application.get().getSharedPreferencesManager();
		return spManager.getString(Constants.PREFERENCES_GCLID, null);
	}

	/**
	 * Sets the new gclid value if it is valid.
	 * 
	 * @param context
	 * @param newGclid
	 *            the value to set.
	 */
	public static void setGclid(Context context, String newGclid) {
		if (newGclid != null && newGclid.length() > 0) {
			SharedPreferencesManager spManager = Application.get().getSharedPreferencesManager();
			spManager.putString(Constants.PREFERENCES_GCLID, newGclid);
		}
	}

	public static String getDecimalFormatPattern(int decimalPointDigits) {
		if (decimalPointDigits <= 0) {
			return DECIMAL_PATTERN_NO_DIGITS;
		}

		StringBuilder pattern = new StringBuilder(DECIMAL_PATTERN_NO_DIGITS);
		pattern.append('.');
		for (int i = 0; i < decimalPointDigits; i++) {
			pattern.append('0');
		}

		return pattern.toString();
	}

	/**
	 * Returns decimal format with decimal separator '.' and grouping separator
	 * ','.
	 * 
	 * @param decimalPointDigits
	 *            the digits after the decimal point.
	 * 
	 * @return
	 */
	public static DecimalFormat getDecimalFormat(int decimalPointDigits) {
		// Explicitly setting the separators
		DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols();
		formatSymbols.setDecimalSeparator('.');
		formatSymbols.setGroupingSeparator(',');

		DecimalFormat format = new DecimalFormat(getDecimalFormatPattern(decimalPointDigits), formatSymbols);
		format.setCurrency(Currency.getInstance(Locale.US));
		return format;
	}

	/**
	 * Returns the Credit Card Last 4 digits.
	 * 
	 * @param Credit
	 *            card number.
	 * @return
	 */
	public static String getCCLastDigits(String number) {
		String result = null;
		if (number.length() >= 4) {
			result = number.substring(number.length() - 4);
		}
		return result;
	}

	/**
	 * Returns the Credit Card Short Hidden string.
	 * 
	 * @param Credit
	 *            card number.
	 * @return
	 */
	public static String getCCHiddenNumberShort(String number) {
		return "XXX" + number.substring(number.length() - 4);
	}

	/**
	 * Returns the Credit Card Long Hidden string.
	 * 
	 * @param Credit
	 *            card number.
	 * @return
	 */
	public static String getCCHiddenNumber(String number) {
		return "xxxx-xxxx-xxxx-" + number.substring(number.length() - 4);
	}

	/**
	 * Returns the Credit Card CVV hidden String.
	 * 
	 * @return
	 */
	public static String getCVVHidden() {
		return "xxx";
	}

	/**
	 * Returns the Device OS version (Example : 4.3.3).
	 * 
	 * @return
	 */
	public static String getDeviceOSVersion() {
		return android.os.Build.VERSION.RELEASE;
	}

	public static int getDeviceOSVersionCode() {
		return android.os.Build.VERSION.SDK_INT;
	}

	/**
	 * Returns the Device Manufacturer (Example : LG).
	 * 
	 * @return
	 */
	public static String getDeviceManufacturer() {
		return android.os.Build.MANUFACTURER;
	}

	/**
	 * Returns the Device Model (Example : Nexus 5).
	 * 
	 * @return
	 */
	public static String getDeviceModel() {
		return android.os.Build.MODEL;
	}

	/**
	 * Returns the Device Fingerprint.
	 * 
	 * @return
	 */
	public static String getDeviceFingerPrint() {
		return android.os.Build.FINGERPRINT;
	}

	/**
	 * Checks if all the values in the bundles are equal.
	 * 
	 * @param one
	 * @param two
	 * @return
	 */
	public static boolean areBundlesEqual(Bundle one, Bundle two) {
		if(one == null || two == null){
			return false;
		}

		if (one.size() != two.size())
			return false;

		Set<String> setOne = one.keySet();
		Object valueOne;
		Object valueTwo;

		for (String key : setOne) {
			valueOne = one.get(key);
			valueTwo = two.get(key);
			if (valueOne instanceof Bundle && valueTwo instanceof Bundle
					&& !areBundlesEqual((Bundle) valueOne, (Bundle) valueTwo)) {
				return false;
			} else if (valueOne == null) {
				if (valueTwo != null || !two.containsKey(key)) {
					return false;
				}
			} else if (!valueOne.equals(valueTwo)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Gets the value for the given key from the URL. Or null if no such mapping
	 * is found.
	 */
	public static String getParameterFromUrl(String url, String key) {
		String result = null;
		try {
			Uri uri = Uri.parse(url);
			result = uri.getQueryParameter(key);
		} catch (Exception e) {
			Log.e(TAG, "Cannot get param from url", e);
		}
		return result;
	}

	public static String getFileNameFromURI(Context context, Uri uri) {
		String result = null;
		if (uri.getScheme().equals("content")) {
			Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
			try {
				if (cursor != null && cursor.moveToFirst()) {
					result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
				}
			} finally {
				cursor.close();
			}

			if (result == null) {
				result = uri.getPath();
				int cut = result.lastIndexOf('/');
				if (cut != -1) {
					result = result.substring(cut + 1);
				}
			}
		} else if (uri.getScheme().equals("file")) {
			result = uri.getLastPathSegment();
		}

		return result;
	}

	public static String getFileNameExtension(String fileName) {
		String extension = "";
		int cut = fileName.lastIndexOf('.');
		if (cut != -1) {
			extension = fileName.substring(cut + 1);
		}
		return extension;
	}

	public static double parseMarketLevel(String levelFormatted){
		String levelString = levelFormatted.replaceAll(",", "");
		if(levelFormatted.indexOf(".") != levelFormatted.lastIndexOf(".")){
			levelString = levelString.replace(".", "");
		}

		return Double.valueOf(levelString);
	}

	/**
	 * Checks if the Application is open for the first time and save in the Shared Preferences.
	 */
	public static boolean checkFirstOpening() {
		SharedPreferencesManager sharedPreferencesManager = Application.get().getSharedPreferencesManager();
		boolean isFirstOpening = sharedPreferencesManager.getBoolean(Constants.PREFERENCES_IS_FIRST_APPLICATION_OPEN, true);
		if(isFirstOpening){
			sharedPreferencesManager.putBoolean(Constants.PREFERENCES_IS_FIRST_APPLICATION_OPEN, false);
			sharedPreferencesManager.putLong(Constants.PREFERENCES_FIRST_APPLICATION_OPEN_TIME, new Date().getTime());
		}
		return isFirstOpening;
	}

	public static String getFormattedLevel(double level, long decimalPoints){
		DecimalFormat levelFormat = Utils.getDecimalFormat((int) decimalPoints);
		levelFormat.applyPattern(Utils.getDecimalFormatPattern((int) decimalPoints));
		return levelFormat.format(level);
	}

	public static void setAndroidAdvertisingID() {
		AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
				@Override
				protected String doInBackground(Void... params) {
					String adId = "";

					try {
						AdvertisingIdClient.Info adInfo = AdvertisingIdClient.getAdvertisingIdInfo(Application.get());
						adId = adInfo != null ? adInfo.getId() : "";
					} catch (Exception exception) {
						adId = "";
					}

					return adId;
				}

				@Override
				protected void onPostExecute(String advertId) {
					Application.get().getSharedPreferencesManager().putString(Constants.PREFERENCES_ANDROID_ADVERTISING_ID, advertId);
					Log.d(TAG, "AAID(SETID): " + Utils.getAndroidAdvertisingID());
				}

			};
			task.execute();
	}

	public static String getAndroidAdvertisingID() {
		return Application.get().getSharedPreferencesManager().getString(Constants.PREFERENCES_ANDROID_ADVERTISING_ID, "");
	}

}