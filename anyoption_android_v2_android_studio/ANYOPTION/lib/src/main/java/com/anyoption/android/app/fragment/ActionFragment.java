package com.anyoption.android.app.fragment;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.anyoption.android.R;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.AssetsListFilterChangeEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.event.TryWithoutAccountEvent;
import com.anyoption.android.app.event.UpdateTitleEvent;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.adapter.MarketsExpandableListAdapter;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.BackButton;
import com.anyoption.android.app.widget.HelpButton;
import com.anyoption.android.app.widget.LoginRegisterActionButtonsView;
import com.anyoption.android.app.widget.MenuButton;
import com.anyoption.android.app.widget.TextView;

/**
 * Custom {@link BaseFragment} that displays current screen title and action buttons.
 * @author Anastas Arnaudov
 *
 */
public class ActionFragment extends BaseFragment{
	
	@SuppressWarnings("hiding")
	private static final String TAG = ActionFragment.class.getSimpleName();
	
	public static final int TYPE_GENERAL 	= 1;
	public static final int TYPE_MAIN 		= 2;
	public static final int TYPE_ASSET 		= 3;
	public static final int TYPE_INNER 		= 4;
	
	public static boolean isInitialLoginRegister = true;

	protected Screenable toScreen;
	protected boolean showTitleOnly;
	private int type;
	protected boolean isTablet;
	protected boolean isOptionPlusHelp;
	protected boolean isDynamicsHelp;
	protected boolean isBubblesHelp;

	protected String title;
	protected Drawable drawable;
	
	protected boolean isMenuButton = true;
	protected boolean isBackbutton;
	protected boolean isHelpButton;
	
	protected View rootView;
	protected RelativeLayout leftView;
	protected LinearLayout middleView;
	protected LinearLayout rightView;	
	protected TextView titleTextView;
	protected LoginRegisterActionButtonsView loginRegisterActionButtonsView;
	protected View backButton;
	protected HelpButton helpButton;
	protected MenuButton menuButton;

	protected long marketProductType;

	protected boolean shouldRefresh;

	public static ActionFragment newInstance(){
		return new ActionFragment();
	}

	public static ActionFragment newInstance(Bundle bundle){
		ActionFragment actionFragment = new ActionFragment();
		actionFragment.setArguments(bundle);
		return actionFragment;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		isOptionPlusHelp = false;
		isDynamicsHelp 	 = false;
		isBubblesHelp 	 = false;
		registerForEvents();
		Log.d(TAG, "ActionFragment created.");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.action_fragment_layout, container, false);

		leftView = (RelativeLayout) rootView.findViewById(R.id.action_fragment_left_layout);
		middleView = (LinearLayout) rootView.findViewById(R.id.action_fragment_middle_layout);
		rightView = (LinearLayout) rootView.findViewById(R.id.action_fragment_right_layout);
		titleTextView = (TextView) rootView.findViewById(R.id.action_fragment_title);
		
		if(!rootView.isInEditMode()){
			isTablet = getResources().getBoolean(R.bool.isTablet);
			
			if (getType() == TYPE_GENERAL) {
				if (showTitleOnly) {
					hideAllChildViews(leftView);
					hideAllChildViews(rightView);
				}
			} else if (getType() == TYPE_MAIN) {
				drawable = DrawableUtils.getImage(R.drawable.logo_1);
				setupTitle();
				hideAllChildViews(leftView);
				hideAllChildViews(rightView);
				setupMenuButton();
			} else if (getType() == TYPE_ASSET) {
				hideAllChildViews(leftView);
			}
			registerForEvents();			
		}
		

		return rootView;
	}
	
	@Override
	public void onResume() {
		registerForEvents();
		if(shouldRefresh){
			setupUI();
		}
		super.onResume();
	}

	@Override
	public void onPause() {
		shouldRefresh = true;

		super.onPause();
	}

	@Override
	public void onDestroyView() {
		application.unregisterForAllEvents(this);
		super.onDestroyView();
	}

	/*@SuppressWarnings("deprecation")
	@Override
	public void onInflate(Activity activity, AttributeSet set, Bundle savedInstanceState) {
		super.onInflate(activity, set, savedInstanceState);
		TypedArray attributes = activity.obtainStyledAttributes(set,R.styleable.ActionFragment);
		showTitleOnly = attributes.getBoolean(R.styleable.ActionFragment_showTitleOnly, false);
		setType(attributes.getInt(R.styleable.ActionFragment_actionFrType, TYPE_GENERAL));
		attributes.recycle();			
	}*/

	@TargetApi(23)
	@Override
	public void onInflate(Context context, AttributeSet set, Bundle savedInstanceState) {
		super.onInflate(context, set, savedInstanceState);
		TypedArray attributes = context.obtainStyledAttributes(set,R.styleable.ActionFragment);
		showTitleOnly = attributes.getBoolean(R.styleable.ActionFragment_showTitleOnly, false);
		setType(attributes.getInt(R.styleable.ActionFragment_actionFrType, TYPE_GENERAL));
		attributes.recycle();
	}
	
	public void registerForEvents() {
		application.unregisterForAllEvents(this);
		if (getType() == TYPE_GENERAL || getType() == TYPE_INNER) {
			application.registerForEvents(this, NavigationEvent.class, UpdateTitleEvent.class);
		} else if (getType() == TYPE_ASSET) {
			application.registerForEvents(this, NavigationEvent.class);
		}

		application.registerForEvents(this, LoginLogoutEvent.class, TryWithoutAccountEvent.class,AssetsListFilterChangeEvent.class);
	}

	//=====================================================================================
	//======================			CALLBACKs			 ==============================
	//=====================================================================================
	public void onEventMainThread(LoginLogoutEvent event){
		ActionFragment.isInitialLoginRegister = false;
	}
	
	public void onEventMainThread(TryWithoutAccountEvent event){
		ActionFragment.isInitialLoginRegister = false;
	}

	public void onEventMainThread(AssetsListFilterChangeEvent event) {
		if (event.getFilter().equals(MarketsExpandableListAdapter.FILTER_OPTION_PLUS)) {
			isOptionPlusHelp = true;
			isDynamicsHelp   = false;
			isBubblesHelp	 = false;
		}else if(event.getFilter().equals(MarketsExpandableListAdapter.FILTER_DYNAMICS)){
			isOptionPlusHelp = false;
			isDynamicsHelp   = true;
			isBubblesHelp    = false;
		}else if(event.getFilter().equals(MarketsExpandableListAdapter.FILTER_BUBBLES)){
			isOptionPlusHelp = false;
			isDynamicsHelp   = false;
			isBubblesHelp    = true;
		}else{
			isOptionPlusHelp = false;
			isDynamicsHelp   = false;
			isBubblesHelp    = false;
		}
		setupHelpButton();
	}

	public void onEventMainThread(UpdateTitleEvent event){
		if(event.getTitle() != null){
			title = event.getTitle();
		}
		if(event.getImageID() > 0){
			drawable = DrawableUtils.getImage(event.getImageID());
		}
		setupTitle();
	}
	
	public void onEventMainThread(NavigationEvent event){
		if(event.isToNewActivity()){
			return;
		}
		//Reset parameters:
		isMenuButton = false;
		isHelpButton = false;
		////////////////////

		NavigationType navigationType = event.getNavigationType();
		Bundle bundle = event.getBundle();
		toScreen = getScreenFromEvent(event.getToScreen());
		title = toScreen.getTitle();
		
		int defaultImageID = toScreen.getImageID();
		drawable = DrawableUtils.getImage(defaultImageID);
		int[] flagsScreenDefaults = toScreen.getFlags();
		int[] flagsEventAdditional = event.getFlags();
		
		if ((getType() == TYPE_ASSET && toScreen != Screen.ASSET_PAGE) || 
			(getType() == TYPE_INNER && toScreen == Screen.ASSET_PAGE)) {
			// The event is not for this type of ActionBar.
			return;
		}

		marketProductType = Constants.PRODUCT_TYPE_ID_BINARY;

		if(bundle != null && !bundle.isEmpty()){
			title = bundle.getString(Constants.EXTRA_ACTION_BAR_TITLE, title);
			int imageID = bundle.getInt(Constants.EXTRA_ACTION_BAR_TITLE_IMAGE);
			boolean isBotomOfStack = bundle.getBoolean(Constants.EXTRA_NAVIGATION_TO_STACK_BOTTOM);
			marketProductType = bundle.getLong(Constants.EXTRA_MARKET_PRODUCT_TYPE, marketProductType);
			isMenuButton = bundle.getBoolean(Constants.EXTRA_MENU_BUTTON, isMenuButton);
			isBackbutton = !isMenuButton;
			
			if(imageID != 0){
				drawable = DrawableUtils.getImage(imageID);
			}

			// Set up the Menu button:
			if (isBotomOfStack && getType() != TYPE_INNER) {
				isMenuButton = true;
				isBackbutton = false;
			}

		}

		switch (navigationType) {
			case INIT:
				if (getType() != TYPE_INNER) {
					isMenuButton = true;
					isBackbutton = false;
				} else {
					isMenuButton = false;
					isBackbutton = true;
				}
				break;
			case DEEP:
				isMenuButton = false;
				isBackbutton = true;
				break;
			default:
				break;
		}

		setupScreenPreferences(flagsScreenDefaults);
		setupScreenPreferences(flagsEventAdditional);
		
		setupActionBarTypePreferences();

		
		if(isHelpButton){
			isOptionPlusHelp = marketProductType == Constants.PRODUCT_TYPE_ID_OPTION_PLUS;
			isDynamicsHelp   = marketProductType == Constants.PRODUCT_TYPE_ID_DYNAMICS;
		}
		///////////////////////////////
		
		setupUI();
		
		// Handle Special Cases:
		setupSpecialCases(toScreen);
		////////////////////////////
	}
	//=====================================================================================
	
	protected Screenable getScreenFromEvent(Screenable screenable){
		return screenable;
	}
	
	protected void setupUI(){
		if(!isResumed()){
			shouldRefresh = true;
			return;
		}
		shouldRefresh = false;
		
		//Setup Title + Image:
		setupTitle();
		///////////////////////
		//Set up the Menu Button:
		if(isMenuButton){
			setupMenuButton();
		}else{
			hideMenuButton();
		}
		///////////////////////
		//Set up the Back Button:
		if(isBackbutton){
			setupBackButton();
		}else{
			hideBackButton();
		}
		///////////////////////		
		//Set up the Help Button:
		if(isHelpButton){
			setupHelpButton();
		}else{
			hideHelpButton();
		}
		///////////////////////
	}
	
	protected void setupSpecialCases(Screenable screen){
		if(screen == Screen.LOGIN){
			setupLogin(isMenuButton);
		}else if(screen == Screen.REGISTER){
			setupRegister(isMenuButton);
		}else if(screen == Screen.AGREEMENT){
			setupBackButton();
		}else if(screen == Screen.WELCOME){
			isBackbutton = false;
			hideBackButton();
		}
	}
	
	protected void setupScreenPreferences(int... flags){
		for(int flag : flags){
			if(flag == Screenable.SCREEN_FLAG_ADD_MENU){
				isMenuButton = true;
			}else if(flag == Screenable.SCREEN_FLAG_REMOVE_MENU){
				isMenuButton = false;
			}else if(flag == Screenable.SCREEN_FLAG_ADD_BACK){
				isBackbutton = true;
			}else if(flag == Screenable.SCREEN_FLAG_REMOVE_BACK){
				isBackbutton = false;
			}else if(flag == Screenable.SCREEN_FLAG_ADD_HELP){
				isHelpButton = true;
			}else if(flag == Screenable.SCREEN_FLAG_REMOVE_HELP){
				isHelpButton = false;
			}
		}
	}
	
	protected void setupActionBarTypePreferences(){
		switch (getType()) {
			case TYPE_ASSET:
				isMenuButton = false;
				isBackbutton = false;
				break;
			case TYPE_INNER:
				if(toScreen == Screen.LOGIN || toScreen == Screen.REGISTER){
					isBackbutton = false;
					hideBackButton();
				}
				break;
		}
	}
	
	protected boolean shouldSetupTitle(Screenable screenable){
		if(screenable == Screen.LOGIN){
			return false;
		}else if(screenable == Screen.REGISTER){
			return false;
		}else{
			return true;
		}
	}
	
	protected void setupTitle(){
		if(!shouldSetupTitle(toScreen)){
			return;
		}
		
		if(titleTextView.getVisibility() != View.VISIBLE){
			hideAllChildViews(middleView);
			titleTextView.setVisibility(View.VISIBLE);
		}
		titleTextView.setText(title);
		titleTextView.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
		// Set up plus icon:
		Drawable plusImage = null;
		if (marketProductType == Constants.PRODUCT_TYPE_ID_OPTION_PLUS) {
			plusImage = DrawableUtils.getDrawable(R.drawable.option_plus_icon_1);
		}else if(marketProductType == Constants.PRODUCT_TYPE_ID_DYNAMICS){
			plusImage = DrawableUtils.getDrawable(R.drawable.dynamics_icon_1);
		}
		titleTextView.setCompoundDrawablesWithIntrinsicBounds(drawable, null, plusImage, null);
		middleView.invalidate();
	}

	protected void setupMenuButton(){
		if(menuButton == null){
			menuButton = new MenuButton(application);
			hideAllChildViews(leftView);
			leftView.addView(menuButton);
			leftView.invalidate();
		}

		if(menuButton.getVisibility() != View.VISIBLE){
			hideAllChildViews(leftView);
			menuButton.setVisibility(View.VISIBLE);
			leftView.invalidate();			
		}	
	}

	protected void hideMenuButton(){
		if(menuButton != null){
			menuButton.setVisibility(View.GONE);
		}
	}
	
	protected void setupBackButton(){
		if(backButton == null){
			backButton = new BackButton(application);
			hideAllChildViews(leftView);
			leftView.addView(backButton);
			leftView.invalidate();
		}		
		if(backButton.getVisibility() != View.VISIBLE){
			hideAllChildViews(leftView);
			backButton.setVisibility(View.VISIBLE);
			leftView.invalidate();			
		}
	}

	protected void hideBackButton(){
		if(backButton != null){
			backButton.setVisibility(View.GONE);
		}
	}
	
	protected void setupHelpButton(){
		if(helpButton == null){
			helpButton = new HelpButton(application);
			helpButton.setType(getType());
			helpButton.setOptionPlusHelp(isOptionPlusHelp);
			rightView.addView(helpButton);
			rightView.invalidate();
		}
		helpButton.setOptionPlusHelp(isOptionPlusHelp);
		helpButton.setDynamicsHelp(isDynamicsHelp);

		if(isBubblesHelp){
			hideHelpButton();
			return;
		}

		if(helpButton.getVisibility() != View.VISIBLE){
			helpButton.setVisibility(View.VISIBLE);
			rightView.invalidate();
		}
	}

	protected void hideHelpButton(){
		if(helpButton != null){
			helpButton.setVisibility(View.GONE);
		}
	}
	
	protected void setupLogin(boolean withMenuButton){
		setupLoginRegisterActionButtons(withMenuButton);
		loginRegisterActionButtonsView.setupLogin();	
	}

	protected void setupRegister(boolean withMenuButton){
		setupLoginRegisterActionButtons(withMenuButton);
		loginRegisterActionButtonsView.setupRegister();	
	}
	
	protected void setupLoginRegisterActionButtons(boolean withMenuButton){	
		if (!isTablet) {
			if(withMenuButton && !ActionFragment.isInitialLoginRegister){
				setupMenuButton();				
			}else{
				hideMenuButton();
			}
		}
		
		hideAllChildViews(rightView);
		if(loginRegisterActionButtonsView == null){
			loginRegisterActionButtonsView = new LoginRegisterActionButtonsView(application.getCurrentActivity());
			hideAllChildViews(middleView);
			middleView.addView(loginRegisterActionButtonsView);
			middleView.invalidate();
		}
		if(loginRegisterActionButtonsView.getVisibility() != View.VISIBLE){
			hideAllChildViews(middleView);
			loginRegisterActionButtonsView.setVisibility(View.VISIBLE);
			middleView.invalidate();
		}
	}

	protected void hideAllChildViews(ViewGroup viewGroup){
		for(int i=0; i<viewGroup.getChildCount(); i++){
			viewGroup.getChildAt(i).setVisibility(View.GONE);
		}
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

}
