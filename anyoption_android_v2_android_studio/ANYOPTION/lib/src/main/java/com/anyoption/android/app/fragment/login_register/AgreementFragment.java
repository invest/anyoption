package com.anyoption.android.app.fragment.login_register;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.manager.PopUpManager;
import com.anyoption.android.app.popup.BonusesToClaimDialogFragment;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.json.requests.BonusMethodRequest;
import com.anyoption.json.requests.UpdateUserMethodRequest;
import com.anyoption.json.results.BonusMethodResult;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
//import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

/**
 * {@link BaseFragment} that represents the Agreement Screen.
 * 
 * @author Mario Kutlev
 */
public class AgreementFragment extends NavigationalFragment {

	@SuppressWarnings("hiding")
	private static final String TAG = AgreementFragment.class.getSimpleName();

	private User user;

	private CheckBox acceptTermsCheckBox;
	private TextView readMoreView;
	private Button submitButton;
	private View rootView;

	/**
	 * Use this method to get new instance of the fragment.
	 * 
	 * @return
	 */
	public static AgreementFragment newInstance() {
		return newInstance(new Bundle());
	}

	/**
	 * Use this method to get new instance of the fragment.
	 * 
	 * @return
	 */
	public static AgreementFragment newInstance(Bundle bundle) {
		AgreementFragment registerFragment = new AgreementFragment();
		registerFragment.setArguments(bundle);
		return registerFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		user = application.getUser();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.agreement_fragment, container, false);

		acceptTermsCheckBox = (CheckBox) rootView.findViewById(R.id.agreement_accept_terms);

		TextView detailsView = (TextView) rootView.findViewById(R.id.agreement_details);
		if (application.isRegulated()) {
			detailsView.setText(R.string.agreementScreenTermsDetailsReg);
		}

		submitButton = (Button) rootView.findViewById(R.id.agreement_submit_button);
		submitButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (acceptTermsCheckBox.isChecked()) {
					user.setAcceptedTerms(true);
					UpdateUserMethodRequest request = new UpdateUserMethodRequest();
					request.setUser(user);
					application.getCommunicationManager().requestService(AgreementFragment.this,
							"updateUserCallBack", Constants.SERVICE_UPDATE_USER, request,
							MethodResult.class, true, true);
				} else {
					application.getPopUpManager().showAlertPopUp(
							getString(R.string.agreementScreenTermsNotAcceptedError),
							new PopUpManager.OnAlertPopUpButtonPressedListener() {

								@Override
								public void onButtonPressed() {
									// Just closes the dialog.
								}
							});
				}
			}
		});

		readMoreView = (TextView) rootView.findViewById(R.id.agreement_read_more);
		readMoreView.setText(Html.fromHtml(getString(R.string.agreementScreenReadMore)));
		TextUtils.underlineText(readMoreView);
		readMoreView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Bundle bundle = new Bundle();
				bundle.putLong(Constants.EXTRA_COUNTRY_ID, user.getCountryId());
				application.postEvent(new NavigationEvent(Screen.TERMS_AND_CONDITIONS,
						NavigationType.DEEP, bundle));
			}
		});

		return rootView;
	}

	public void updateUserCallBack(Object resultObj) {
		Log.d(TAG, "updateUserCallBack");
		MethodResult result = (MethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			application.setUser(user);

			if (!RegulationUtils.showQuestionnaire()) {
				//shows questionnaire
			} else {
				application.postEvent(new NavigationEvent(application.getHomeScreen(),
						NavigationType.INIT));

				if (!application.isEtrader() && showSeenBonuses()) {
					//START OF OLD LOGIC
					// The user has new bonuses, show bonuses screen.
					// toScreen = Screen.BONUSES;
					//END OF OLD LOGIC
					getNewBonuses();
				}

			}
		}
	}

	protected boolean showBonuses() {
		return application.getUser().isHasBonus();
	}

	protected boolean showSeenBonuses() {
		return application.getUser().hasSeenBonuses();
	}

	protected void getNewBonuses() {
		BonusMethodRequest request = new BonusMethodRequest();
		request.setBonusStateId(0);
		request.setShowOnlyNotSeenBonuses(1);
		application.getCommunicationManager().requestService(AgreementFragment.this, "getNewBonusesCallback", Constants.SERVICE_GET_BONUSES, request, BonusMethodResult.class);
	}

	public void getNewBonusesCallback(Object result) {
		Bundle bundle = new Bundle();

		boolean isSingle = false;
		BonusUsers bonusUsers = null;
		String[] turnoverFactor = {};

		if (result != null) {
			BonusMethodResult bonusResult = (BonusMethodResult) result;
			if (bonusResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
				if (bonusResult.getBonuses().length == 1) {
					isSingle = true;
					bonusUsers = bonusResult.getBonuses()[0];
				}
			}
		}
		bundle.putBoolean(Constants.EXTRA_IS_SINGLE_BONUS, isSingle);
		bundle.putSerializable(Constants.EXTRA_BONUS, bonusUsers);
		bundle.putStringArray(Constants.EXTRA_BONUS_FACTOR, turnoverFactor);
		application.getFragmentManager().showDialogFragment(BonusesToClaimDialogFragment.class, bundle);
	}



	@Override
	protected Screenable setupScreen() {
		return Screen.AGREEMENT;
	}

}