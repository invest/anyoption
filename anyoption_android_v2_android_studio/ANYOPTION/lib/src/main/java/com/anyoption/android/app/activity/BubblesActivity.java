package com.anyoption.android.app.activity;

import android.annotation.TargetApi;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.results.UserMethodResult;

/**
 * @author Anastas Arnaudov
 */
public class BubblesActivity extends BaseActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Go Fullscreen:
        goFullScreen();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus){
            if(Utils.getDeviceOSVersionCode() >= Build.VERSION_CODES.KITKAT){
                goFullScreen();
            }
        }
    }

    protected void setupDefaultScreenOrientation(){
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }


    protected void goFullScreen(){
        if(Utils.getDeviceOSVersionCode() >= Build.VERSION_CODES.KITKAT){
            hideSystemUI(getWindow().getDecorView());
        }else{
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void hideSystemUI(View view) {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        view.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    protected void clearScreensStack(){
        //Do nothing
    }

    @Override
    public void getUserCallback(Object resultObj) {
        super.getUserCallback(resultObj);
        UserMethodResult result = (UserMethodResult) resultObj;
        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SESSION_EXPIRED) {
            application.postEvent(new NavigationEvent(Application.Screen.LOGIN, NavigationEvent.NavigationType.INIT));
        }
    }
}
