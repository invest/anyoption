package com.anyoption.android.app.widget.questionnaire;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.model.Answer;
import com.anyoption.android.app.model.Question;
import com.anyoption.android.app.popup.QuestionnaireAnswersDialogFragment;

import java.util.Arrays;

/**
 * Created by veselin.hristozov on 10.10.2016 г..
 */

public class QuestionnairePopupAndTextFieldAnswerView extends QuestionnaireBaseView implements QuestionnaireAnswersDialogFragment.OnItemSelectedListener, View.OnClickListener, TextWatcher {

    private QuestionnaireAnswersDialogFragment dialog;

    private LinearLayout rootView;
    private TextView answerTextView;
    private EditText textAnswerEditText;

    public QuestionnairePopupAndTextFieldAnswerView(Context context) {
        super(context);
    }

    public QuestionnairePopupAndTextFieldAnswerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public QuestionnairePopupAndTextFieldAnswerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void init() {
        questionType = Question.QuestionType.POPUP_WITH_TEXT_FIELD;

        rootView = (LinearLayout) View.inflate(getContext(), R.layout.questionanaire_popup_and_text_field_answer_item, this);
        questionTextView = (TextView) rootView.findViewById(R.id.questionnaire_item_question_text_view);
        answerTextView = (TextView) rootView.findViewById(R.id.questionnaire_item_answer_text_view);
        errorTextView = (TextView) rootView.findViewById(R.id.questionnaire_item_error_text_view);
        textAnswerEditText = (EditText) rootView.findViewById(R.id.questionnaire_item_answer_field_edit_text);
        textAnswerEditText.addTextChangedListener(this);

        if (!isInEditMode()) {
            rootView.setOnClickListener(this);
            questionTextView.setOnClickListener(this);
            answerTextView.setOnClickListener(this);
        }
    }

    @Override
    public void setAnswer(Answer answer) {
        super.setAnswer(answer);

        if(question.getValidationAnswer() != null && answer.getId() == question.getValidationAnswer().getId()) {
            textAnswerEditText.setVisibility(View.VISIBLE);
            answerTextView.setVisibility(View.GONE);

            if(answer.getTextAnswer() != null && answer.getTextAnswer().length() > 0) {
                textAnswerEditText.setText(answer.getTextAnswer() != null ? answer.getTextAnswer() : "");
            }

        } else {
            textAnswerEditText.setVisibility(View.GONE);
            answerTextView.setVisibility(View.VISIBLE);
            answerTextView.setText(answer.getDescription());
            answer.setTextAnswer("");
        }

        errorTextView.setVisibility(View.GONE);
    }

    public void showError(){
        answerTextView.setVisibility(View.GONE);
        errorTextView.setVisibility(View.VISIBLE);
    }

    public void setQuestion(Question question){
        super.setQuestion(question);

        decorateText(questionTextView, question.getDescription());
        textAnswerEditText.setVisibility(View.GONE);
    }

    public void setAnswerSelectedListener(OnAnswerSelectedListener answerSelectedListener) {
        this.answerSelectedListener = answerSelectedListener;
    }

    private void showDialog(){
        if(dialog == null){
            dialog = (QuestionnaireAnswersDialogFragment) Application.get().getFragmentManager().showDialogFragment(QuestionnaireAnswersDialogFragment.class, null);
            dialog.setOnItemSelectedListener(this);
            dialog.setItems(Arrays.asList(question.getAnswers()));
        }else{
            Application.get().getFragmentManager().showDialogFragment(dialog);
        }
    }

    @Override
    public void onItemSelected(Answer item, int position) {
        setAnswer(item);

        if(answerSelectedListener != null){
            answerSelectedListener.onAnswerSelected(question, item);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == rootView || v == questionTextView || v == answerTextView) {
            if (isEnabled) {
                showDialog();
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if(s.length() != 0) {
            answer.setTextAnswer(textAnswerEditText.getText().toString());
        } else {
            answer.setTextAnswer("");
        }

        if(answerSelectedListener != null){
            answerSelectedListener.onAnswerSelected(question, answer);
        }
    }

    @Override
    public boolean isAnswered() {
        if (question.isMandatory()) {
            return getAnswer() != null && !((question.getValidationAnswer() != null && question.getValidationAnswer().getId() != 0L && question.getValidationAnswer().getId() == getAnswer().getId()) && (getAnswer().getTextAnswer() == null || getAnswer().getTextAnswer().length() == 0));
        } else {
            return true;
        }
    }
}
