package com.anyoption.android.app.popup;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.model.DropDownPopupItem;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.ScreenUtils;


import java.util.ArrayList;

public class DropdownPopup extends PopupWithAnchor implements OnItemClickListener {

    private DropdownPopupClickListener dropdownPopupClickListener;
    private ArrayList<DropDownPopupItem> elements;
    private static DropDownPopupItem selectedItem;

    public interface DropdownPopupClickListener {
        public void onDropdownPopupClick(DropDownPopupItem element);

        public void onDropdownPopupDismissed();
    }

    public static void showPopup(ArrayList<DropDownPopupItem> elements, View anchor, DropdownPopupClickListener dropdownPopupClickListener) {
        View contentView = View.inflate(Application.get(), getDropDownLayout(), null);

        int[] coordinates = new int[2];
        anchor.getLocationOnScreen(coordinates);
        int screenWidth = ScreenUtils.getScreenWidth()[0];
        int anchorRight = coordinates[0];
        int anchorWidth = DrawableUtils.getDrawableSize(getAnchorDrawable())[0];
        int pointerMarginRight = Application.get().getResources().getDimensionPixelSize(getPointerMarginRight());
        int pointerWidth = Application.get().getResources().getDimensionPixelSize(getPointerIconWidth());

        int paddingRight = screenWidth - anchorRight - anchorWidth/2 - pointerMarginRight - pointerWidth / 2;

        contentView.setPadding(0, 0, paddingRight, 0);


             final DropdownPopup popup = new DropdownPopup(elements, anchor, contentView, dropdownPopupClickListener);
            contentView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    popup.dismiss();
                }
            });
            if (elements != null && elements.size() > 0) {
                selectedItem = elements.get(0);
            }
        popup.show();
    }


    public static int getDropDownLayout() {
        return R.layout.dropdown_popup;
    }

    public static int getDropDownListView() {
        return R.id.dropdown_list_view;
    }

    public static int getAnchorDrawable() {
        return R.drawable.dd_arrow_down;
    }

    public static int getPointerIconWidth() {
        return R.dimen.dropdown_pointer_icon_width;
    }

    public static int getPointerMarginRight() {
        return R.dimen.dropdown_pointer_icon_margin_right;
    }

    public static int getDropDownItemTextView() {
        return R.id.dropdown_item_text_view;
    }

    public static int getDropDownItemLayout() {
        return R.layout.dropdown_item;
    }

public static int getDropDownItemHeight() {
    return R.dimen.dropdown_item_height;
}

    private DropdownPopup(ArrayList<DropDownPopupItem> elements, View anchor, View content, DropdownPopupClickListener dropdownPopupClickListener) {
        super(anchor, content);
        this.dropdownPopupClickListener = dropdownPopupClickListener;
        this.elements = elements;
        ListView listView = (ListView) content.findViewById(getDropDownListView());
        listView.setAdapter(new DropdownAdapter(elements));
        listView.setOnItemClickListener(this);
    }

    public DropdownPopupClickListener getDropdownPopupClickListener() {
        return dropdownPopupClickListener;
    }

    public void setDropdownPopupClickListener(DropdownPopupClickListener dropdownPopupClickListener) {
        this.dropdownPopupClickListener = dropdownPopupClickListener;
    }

    public static class DropdownAdapter extends BaseAdapter {

        public final String TAG = DropdownAdapter.class.getSimpleName();

        public static class ItemViewHolder {
            public int position;
            public TextView textView;
        }

        private Context context;
        private LayoutInflater inflater;
        private int itemHeight;
        private ArrayList<DropDownPopupItem> elements;

        public DropdownAdapter(ArrayList<DropDownPopupItem> elements) {
            context = Application.get();
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.itemHeight = (int) context.getResources().getDimension(getDropDownItemHeight());
            this.elements = elements;
        }

        @Override
        public int getCount() {
            return elements.size();
        }

        @Override
        public Object getItem(int position) {
            return elements.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            DropDownPopupItem item = elements.get(position);

            ItemViewHolder itemViewHolder;
            if (convertView == null) {
                convertView = inflater.inflate(getDropDownItemLayout(), parent, false);
                itemViewHolder = new ItemViewHolder();
                itemViewHolder.textView = (TextView) convertView.findViewById(getDropDownItemTextView());

                convertView.setTag(itemViewHolder);
            } else {
                itemViewHolder = (ItemViewHolder) convertView.getTag();
            }

            itemViewHolder.position = position;

            //Hide the current filter from the list:
            android.widget.AbsListView.LayoutParams params = (android.widget.AbsListView.LayoutParams) convertView.getLayoutParams();
            if (item.isSelected()) {
                params.height = 0;
                hideAllChildren((ViewGroup) convertView);
                convertView.setLayoutParams(params);
                convertView.setVisibility(View.GONE);
            } else {
                showAllChildren((ViewGroup) convertView);
                params.height = itemHeight;
                convertView.setLayoutParams(params);
                convertView.setVisibility(View.VISIBLE);
                try {
                    itemViewHolder.textView.setText(item.getTitle());
                } catch (Exception e) {
                    Log.e(TAG, "getView", e);
                }
            }

            return convertView;
        }


        private void hideAllChildren(ViewGroup viewGroup) {
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View view = viewGroup.getChildAt(i);
                view.setVisibility(View.GONE);
            }
        }

        private void showAllChildren(ViewGroup viewGroup) {
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View view = viewGroup.getChildAt(i);
                view.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        DropDownPopupItem element = elements.get(position);
        for(DropDownPopupItem currentElement : elements) {
            currentElement.setIsSelected(false);
        }
        element.setIsSelected(true);
        if (dropdownPopupClickListener != null) {
            this.dropdownPopupClickListener.onDropdownPopupClick(element);
            super.dismiss();
        }
    }

    @Override
    public void dismiss() {
        if (dropdownPopupClickListener != null) {
            this.dropdownPopupClickListener.onDropdownPopupDismissed();
        }
        super.dismiss();
    }

}
