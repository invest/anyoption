package com.anyoption.android.app.widget.form;

import java.util.ArrayList;
import java.util.List;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.LayoutDirection;
import com.anyoption.android.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

/**
 * Custom  Selector that on click shows Dialog with a list of items to choose from.
 * @author Anastas Arnaudov
 *@param <T> The type object that the list will contain. The list will use the object toString() method for the display text.
 */
public abstract class FormSelector<T> extends RelativeLayout implements FormItem, View.OnClickListener {

	private LayoutDirection direction;
	
	/**
	 * Implement this to listen when item from the list is selected.
	 * @author Anastas Arnaudov
	 *
	 * @param <T> The type of the items in the list.
	 */
	public interface ItemListener<T>{
		/**
		 * Called when item is selected.
		 * @param selectedItem The selected item.
		 */
		public void onItemSelected(T selectedItem);
	
		/**
		 * Returns the display text that will be used to represent each item. 
		 * If null is returned than the toString() method will be used to display the item.
		 * @return
		 */
		public String getDisplayText(T selectedItem);
		
		/**
		 * Returns the icon for this item. If null - no icon will be displayed. 
		 * @param selectedItem
		 * @return
		 */
		public Drawable getIcon(T selectedItem);
	}
	
	/**
	 * Listener for items selection.
	 * @author Anastas Arnaudov
	 *
	 */
	public interface ItemSelectedListener<T>{
		/**
		 * Called when item is selected.
		 * @param selectedItem The selected item.
		 */
		public void onItemSelected(T selectedItem);
	}
	
	protected List<T> items;
	private AlertDialog dialog;
	private ArrayAdapter<T> adapter;
	private T value;
	protected boolean isSetDefaultItem;
	
	private List<ItemSelectedListener<T>> listeners = new ArrayList<FormSelector.ItemSelectedListener<T>>();
	protected boolean isEnabled;
	
	public FormSelector(Context context) {
		super(context);
		setup(context, null, 0);
	}
	public FormSelector(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, 0);
	}	
	public FormSelector(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup(context, attrs, defStyle);
	}

	/**
	 * Sets up the UI.
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	protected void setup(Context context, AttributeSet attrs, int defStyle){
		// Get custom attributes:
		TypedArray attributesTypedArray = context.obtainStyledAttributes(attrs, R.styleable.FormSelector);
		isEnabled = attributesTypedArray.getBoolean(R.styleable.FormSelector_isEnabled, true);
		setEditable(isEnabled);
		attributesTypedArray.recycle();
		///////////////////////
		if(!isInEditMode()){
			direction = Application.get().getLayoutDirection();
		}
		isEnabled = true;
		setBackgroundResource(R.color.form_item_selector_bg);
		setupUI();
		setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if(getDialog() != null){
			getDialog().show();
		}
	}

	/**
	 * Sets up the UI. Here we must create (or inflate) the ui elements.
	 */
	protected abstract void setupUI();
	
	/**
	 * Sets the items that will be displayed on selector clicked.
	 * @param items The items.
	 * @param defaultSelectedItem The default selected item. 
	 * <p>If there is already a set value this will not take effect;
	 * <p>If < 0 => the first item will be selected;
	 * @param onItemSelectedListener
	 */
	public void setItems(List<T> items, int defaultSelectedItem, final ItemListener<T> onItemSelectedListener){
		this.items = items;
		int selectedIndex = defaultSelectedItem;
		if(value == null){
			if(defaultSelectedItem > 0 && defaultSelectedItem < items.size()){
				value = items.get(defaultSelectedItem);				
			}else{
				if(isSetDefaultItem){
					value = items.get(0);
					selectedIndex = items.indexOf(value);
				}
			}
		}else{
			selectedIndex = items.indexOf(value);
		}
		setupDialog(selectedIndex, onItemSelectedListener);	
	}

	protected int getSelectorItemLayout() {
		return R.layout.form_selector_list_item;
	}

	/**
	 * Sets up the Dialog.
	 * @param defaultSelectedItem
	 * @param itemListener
	 */
	private void setupDialog(int defaultSelectedItem, final ItemListener<T> itemListener){
		AlertDialog.Builder builder = new AlertDialog.Builder(Application.get().getCurrentActivity());
		adapter = new ArrayAdapter<T>(getContext(), getSelectorItemLayout(), items){
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				
				LinearLayout view;
				TextView text;
				ImageView icon;
				
				if (convertView == null) {
					view =  (LinearLayout) ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(getSelectorItemLayout(), parent, false);
				} else {
					view = (LinearLayout) convertView;
				}
	
				text = (TextView) view.findViewById(R.id.form_selector_list_item_text);
				icon = (ImageView) view.findViewById(R.id.form_selector_list_item_image);

				T item = items.get(position);
				String displayText = itemListener.getDisplayText(item);
				if(displayText != null){
					text.setText(displayText);
				}
				Drawable iconDrawable = itemListener.getIcon(item);
				if(iconDrawable == null){
					icon.setVisibility(View.GONE);
				}else{
					icon.setVisibility(View.VISIBLE);
					icon.setImageDrawable(iconDrawable);
				}
				
				//Align text and icon according to the Direction:
				if(direction == LayoutDirection.LEFT_TO_RIGHT){
					text.setGravity(Gravity.LEFT);
					if(icon.getVisibility() == View.VISIBLE){
						view.removeAllViews();
						view.addView(text);
						view.addView(icon);						
					}
				}else if(direction == LayoutDirection.RIGHT_TO_LEFT){
					text.setGravity(Gravity.RIGHT);
					if(icon.getVisibility() == View.VISIBLE){
						view.removeAllViews();
						view.addView(icon);
						view.addView(text);						
					}
				}
				
				return view;
			}
		};
	    builder.setSingleChoiceItems(adapter, defaultSelectedItem, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int position) {
				T item = items.get(position);
				value = item;
				if(itemListener != null){
					itemListener.onItemSelected(item);
					
					//Inform all listeners:
					for(ItemSelectedListener<T> itemSelectedListener : listeners){
						itemSelectedListener.onItemSelected(item);
					}
				}
				dialog.dismiss();
			}	
	    });
	    
		setDialog(builder.create());
		getDialog().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}
	
	public AlertDialog getDialog() {
		return dialog;
	}
	
	public void setDialog(AlertDialog dialog) {
		this.dialog = dialog;
	}
	
	@Override
	public void setDirection(LayoutDirection direction) {
		if(this.direction != direction){
			this.direction = direction;
			setupUIForDirection();			
		}
	}
	
	@Override
	public LayoutDirection getDirection() {
		return this.direction;
	}
	@Override
	public boolean isEditable() {
		return isClickable();
	}
	@Override
	public void setEditable(boolean isEditable) {
		isEnabled = isEditable;
		super.setClickable(isEditable);
		super.setEnabled(isEditable);
	}
	@Override
	public void setEnabled(boolean enabled) {
		setEditable(enabled);
	}
	@Override
	public boolean checkValidity() {
		return true;
	}
	@Override
	public T getValue() {
		return value;
	}
	@SuppressWarnings("unchecked")
	@Override
	public void setValue(Object value) {
		this.value = (T) value;
	}

	@Override
	public void clear() {}

	@Override
	public void addOnEditorActionListener(OnEditorActionListener onEditorActionListener) {}
	
	@Override
	public void refreshUI() {}
	
	/**
	 * Adds {@link ItemSelectedListener}.
	 * @param itemListener
	 */
	public void addItemListener(ItemSelectedListener<T> itemListener){
		listeners.add(itemListener);
	}
}
