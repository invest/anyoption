package com.anyoption.android.app.popup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;

/**
 * A POPUP that is shown when the user attempts to make a withdrawal that is lower than a MIN amount. 
 * @author Anastas Arnaudov
 *
 */
public class LowWithdrawDialogFragment extends BaseDialogFragment implements OnClickListener {

	public static final String TAG = LowWithdrawDialogFragment.class.getSimpleName();

	public interface LowWithdrawListener{
		void onOK();
		void onCancel();
	}
	
	/**
	 * Use this method to create new instance of the fragment.
	 * @return
	 */
	public static LowWithdrawDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new LowWithdrawDialogFragment.");
		LowWithdrawDialogFragment fragment = new LowWithdrawDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		fragment.setArguments(bundle);
		return fragment;
	}

	private LowWithdrawListener listener;
	private View okButton;
	private View cancelButton;
	protected String fee;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if(args != null){
			fee = args.getString(Constants.EXTRA_FEE);
		}
	}
	
	@Override
	protected void onCreateContentView(View contentView) {
		
		String firstName = application.getUser().getFirstName();
		TextView headerTextView = (TextView) contentView.findViewById(R.id.low_withdraw_popup_header_text_view);
		TextView message_2_TextView = (TextView) contentView.findViewById(R.id.low_withdraw_popup_message_2_text_view);
		
		headerTextView.setText(getString(R.string.lowWithdrawPopupHeader, firstName));
		message_2_TextView.setText((getString(R.string.lowWithdrawPopupMessage_2, fee)));

		TextUtils.decorateInnerText(message_2_TextView, null, message_2_TextView.getText().toString(), fee, Font.ROBOTO_MEDIUM, 0, 0);
		
		okButton = contentView.findViewById(R.id.ok_button);
		cancelButton = contentView.findViewById(R.id.cancel_button);
		
		okButton.setOnClickListener(this);
		cancelButton.setOnClickListener(this);
			
	}

	@Override
	protected int getContentLayout() {
		return R.layout.popup_low_withdraw;
	}

	@Override
	public void onClick(View v) {
		if(v == okButton){
			if(listener != null){
				listener.onOK();
			}
			dismiss();
		}else if(v == cancelButton){
			if(listener != null){
				listener.onCancel();
			}
			dismiss();
		}
	}

	public void setListener(LowWithdrawListener listener) {
		this.listener = listener;
	}

}
