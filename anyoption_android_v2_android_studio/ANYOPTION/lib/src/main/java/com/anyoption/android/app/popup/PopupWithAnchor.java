package com.anyoption.android.app.popup;

import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.PopupWindow;

/**
 * Custom {@link PopupWindow} that is displayed as a DROPDOWN below the given anchorView View.
 * @author Anastas Arnaudov
 */
public class PopupWithAnchor extends PopupWindow{

	protected View anchorView;
	protected View contentView;
	protected int xOffset;
	protected int yOffset;
	
	public PopupWithAnchor(View anchor, View content){
		this(anchor, 0, 0, content);
	}
	
	public PopupWithAnchor(View anchor, View content, boolean isFillScreen){
		super(anchor, (isFillScreen) ? LayoutParams.MATCH_PARENT : LayoutParams.WRAP_CONTENT, (isFillScreen) ? LayoutParams.MATCH_PARENT : LayoutParams.WRAP_CONTENT);
		this.anchorView = anchor;
		this.contentView = content;
		init();
	}
	
	public PopupWithAnchor(View anchor, int xOffset, int yOffset, View content){
		super(anchor, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		this.anchorView = anchor;
		this.contentView = content;
		this.xOffset = xOffset;
		this.yOffset = yOffset;
		init();
	}
	
	@SuppressWarnings("deprecation")
	private void init(){
		setBackgroundDrawable(new BitmapDrawable());//This removes the default background which covers the entire Screen.
		setContentView(contentView);

		setFocusable(true);
		setTouchable(true);
	}
	
	public void show(){
		showAsDropDown(anchorView, xOffset, yOffset);
	}
}
