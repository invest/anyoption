package com.anyoption.android.app.fragment.my_account.banking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.event.ReverseWithdrawEvent;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.manager.PopUpManager.OnConfirmPopUpButtonPressedListener;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.RequestButton.InitAnimationListener;
import com.anyoption.common.beans.base.Transaction;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.requests.FormsMethodRequest;
import com.anyoption.json.requests.TransactionMethodRequest;
import com.anyoption.json.results.TransactionMethodResult;
import com.anyoption.json.results.TransactionsMethodResult;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Banking Screen. It has buttons that lead to Deposit and Withdraw. Also it
 * shows the banking history in a list , each item of which offers the Reverse
 * withdraw functionality.
 * 
 * @author Anastas Arnaudov
 *
 */
public class BankingFragment extends NavigationalFragment {

	public static final String TAG = BankingFragment.class.getSimpleName();
	public static final int PAGE_SIZE = 15;

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static BankingFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new BankingFragment.");
		BankingFragment bankingFragment = new BankingFragment();
		bankingFragment.setArguments(bundle);
		return bankingFragment;
	}

	private ListView listView;
	private ListViewAdapter listAdapter;
	private TextView emptyListTextView;
	private List<Transaction> transactionsList = new ArrayList<Transaction>();
	private boolean hasTransactions = true;
	private int startRow = 0;
	private boolean isReverseWithdrawalConfirmButtonEnabled = true;
	private ProgressBar loading;
	protected boolean isWithdrawClosed;
	protected boolean isWithdrawWeekendsCkeched;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = getArguments();
		if (bundle != null) {
			// Get arguments passed in the bundle
		}

		listAdapter = new ListViewAdapter(application, transactionsList);

		checkWithdrawOnWeekends();
		getTransactions();
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (!hidden) {
			hasTransactions = true;
			startRow = 0;
			transactionsList.clear();
			if (listAdapter != null) {
				listAdapter.notifyDataSetChanged();
			}
			checkWithdrawOnWeekends();
			getTransactions();
		}
	}

	private void showHideLoading(boolean isVisible) {
		if (loading != null) {
			if (isVisible) {
				loading.setVisibility(View.VISIBLE);
			} else {
				loading.setVisibility(View.INVISIBLE);
			}
		}
	}

	/**
	 * Get the user transactions from the server (with paging).
	 */
	private void getTransactions() {
		Log.d(TAG, "Load from row: " + startRow + " pageSize: " + PAGE_SIZE);
		FormsMethodRequest request = new FormsMethodRequest();
		request.setStartRow(startRow);
		request.setPageSize(PAGE_SIZE);
		isReverseWithdrawalConfirmButtonEnabled = !application.getCommunicationManager().requestService(this,
				"gotTransactions", Constants.SERVICE_GET_TRANSACTIONS, request, TransactionsMethodResult.class);
		showHideLoading(isReverseWithdrawalConfirmButtonEnabled);
	}

	/**
	 * Callback method for the "getTransactions" request.
	 * 
	 * @param result
	 */
	public void gotTransactions(final Object result) {
		application.getCurrentActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				showHideLoading(false);
				hasTransactions = false;
				if (result != null) {
					TransactionsMethodResult transactionsMethodResult = (TransactionsMethodResult) result;
					if (transactionsMethodResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {

						transactionsList.addAll(Arrays.asList(transactionsMethodResult.getTransactions()));
						if (transactionsList.size() > 0) {
							hasTransactions = true;
						}
						// Refresh list:
						listAdapter.setItems(transactionsList);
						listAdapter.notifyDataSetChanged();
						startRow += PAGE_SIZE;
					}
				}

				if (listView != null && emptyListTextView != null) {
					if (hasTransactions) {
						listView.setVisibility(View.VISIBLE);
						listView.setClickable(true);
						emptyListTextView.setVisibility(View.GONE);
					} else {
						listView.setVisibility(View.GONE);
						emptyListTextView.setVisibility(View.VISIBLE);
					}
				}

				isReverseWithdrawalConfirmButtonEnabled = true;

			}
		});
	}

	private void checkWithdrawOnWeekends() {
		Log.d(TAG, "checkWithdrawOnWeekends");
		UserMethodRequest request = new UserMethodRequest();
		if (!application.getCommunicationManager().requestService(this, "checkWithdrawOnWeekendsCallback",
				Constants.SERVICE_IS_WITHDRAW_WEEKENDS, request, MethodResult.class, false, false)) {
			isWithdrawWeekendsCkeched = false;
		}
	}

	public void checkWithdrawOnWeekendsCallback(final Object resObj) {
		MethodResult result = (MethodResult) resObj;
		if (result != null && result instanceof MethodResult) {
			int errorCode = result.getErrorCode();
			if (errorCode == Constants.ERROR_CODE_SUCCESS) {
				isWithdrawWeekendsCkeched = true;
				isWithdrawClosed = false;
			} else if (errorCode == Constants.ERROR_CODE_WITHDRAWAL_OPEN) {
				isWithdrawWeekendsCkeched = true;
				isWithdrawClosed = true;
			} else {
				isWithdrawWeekendsCkeched = false;
				ErrorUtils.handleResultError(result);
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.banking_layout, container, false);

		View depositButton = rootView.findViewById(R.id.banking_deposit_button);
		depositButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (RegulationUtils.checkRegulationStatusAndDoNecessary(false)) {
					application.postEvent(new NavigationEvent(Screen.DEPOSIT_MENU, NavigationType.DEEP));
				}
			}
		});
		View withdrawButton = rootView.findViewById(R.id.banking_withdraw_button);
		withdrawButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (RegulationUtils.checkRegulationStatusAndDoNecessary(true)) {
					if (isWithdrawWeekendsCkeched && !isWithdrawClosed) {
						application.postEvent(new NavigationEvent(Screen.WITHDRAW_MENU, NavigationType.DEEP));
					} else if (isWithdrawClosed) {
						application.postEvent(new NavigationEvent(Screen.WITHDRAW_CLOSED, NavigationType.DEEP));
					}
				}
			}
		});

		listView = (ListView) rootView.findViewById(R.id.banking_list_view);
		listView.setAdapter(listAdapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			}
		});
		listView.setOnScrollListener(new OnScrollListener() {

			private int currentLastVisibleItem;
			private int currentVisibleItemCount;
			private int currentScrollState;

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				this.currentScrollState = scrollState;
				isScrollCompleted();
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				this.currentLastVisibleItem = firstVisibleItem + visibleItemCount;
				this.currentVisibleItemCount = visibleItemCount;

			}

			private void isScrollCompleted() {
				if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
					// Detect if there's been a scroll which has completed.
					// And check if the list is scrolled to the bottom.
					if (currentLastVisibleItem == listAdapter.getCount()) {
						getTransactions();
					}
				}
			}

		});

		emptyListTextView = (TextView) rootView.findViewById(R.id.banking_empty_text_view);

		if (!hasTransactions) {
			listView.setVisibility(View.GONE);
			emptyListTextView.setVisibility(View.VISIBLE);
		} else {
			listView.setVisibility(View.VISIBLE);
			emptyListTextView.setVisibility(View.GONE);
		}

		loading = (ProgressBar) rootView.findViewById(R.id.banking_loading);

		return rootView;
	}

	// ##################################################################################################
	// ######### ADAPTER for the List View ################
	// ##################################################################################################

	/**
	 * Custom list adapter with Transaction objects.
	 * 
	 * @author Anastas Arnaudov
	 *
	 */
	private class ListViewAdapter extends ArrayAdapter<Transaction> {

		private List<Transaction> items;

		private ReverseWithdrawalClickListener reverseWithdrawalClickListener;

		public ListViewAdapter(Context context, List<Transaction> items) {
			super(context, R.layout.banking_list_item_layout, items);
			this.items = items;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			Transaction transaction = getItem(position);

			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) getContext()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.banking_list_item_layout, parent, false);
			}

			TextView descriptionTextView = (TextView) convertView.findViewById(R.id.banking_list_item_description);
			TextView idTextView = (TextView) convertView.findViewById(R.id.banking_list_item_id);
			RequestButton reverseWithrawalButton = (RequestButton) convertView
					.findViewById(R.id.banking_list_item_reverse_withdraw_button);
			TextView dateTextView = (TextView) convertView.findViewById(R.id.banking_list_item_date);
			TextView timeTextView = (TextView) convertView.findViewById(R.id.banking_list_item_time);
			TextView amountTextView = (TextView) convertView.findViewById(R.id.banking_list_item_amount);
			TextView typeTextView = (TextView) convertView.findViewById(R.id.banking_list_item_type);

			// Set the description:
			descriptionTextView.setText(transaction.getTypeName());

			// Set the ID:
			idTextView.setText(getString(R.string.bankingScreenListItemID) + ": " + transaction.getId());

			// Set up the Reverse Withdrawal button:
			if (!transaction.getCredit() && (transaction.getStatusId() == Constants.TRANSACTION_STATUS_REQUESTED
					|| transaction.getStatusId() == Constants.TRANSACTION_STATUS_APPROVED)) {

				User user = application.getUser();
				if (user != null && user.isStopReverseWithdrawOption()) {
					reverseWithrawalButton.setVisibility(View.GONE);
				} else {
					reverseWithrawalButton.setVisibility(View.VISIBLE);
					reverseWithrawalButton.setEnabled(isReverseWithdrawalConfirmButtonEnabled);
					reverseWithrawalButton.setOnClickListener(new ReverseWithdrawalClickListener(transaction));
				}

			} else {
				reverseWithrawalButton.setVisibility(View.GONE);
			}

			// Set the date and time:
			String[] dateTime = transaction.getTimeCreatedTxt().split(" ");
			String date = dateTime[0];
			String time = dateTime[1];

			dateTextView.setText(date);
			timeTextView.setText(time);

			// Set the amount:
			amountTextView.setText(transaction.getAmountWF());

			// Set the type (credit or Debit):
			String creditDebit;
			if (transaction.getCredit() == true) {
				creditDebit = getString(R.string.bankingScreenListItemTypeCredit);
			} else {
				creditDebit = getString(R.string.bankingScreenListItemTypeDebit);
			}
			typeTextView.setText(creditDebit);

			return convertView;
		}

		@Override
		public Transaction getItem(int position) {
			return this.items.get(position);
		}

		@Override
		public int getCount() {
			return this.items.size();
		}

		public void setItems(List<Transaction> items) {
			this.items = items;
		}

		@Override
		public boolean isEnabled(int position) {
			return false;
		}

	}

	/**
	 * Custom Listener for Reverse Withdraw button clicks.
	 * 
	 * @author Anastas Arnaudov
	 *
	 */
	private class ReverseWithdrawalClickListener implements View.OnClickListener {

		private Transaction transaction;

		public ReverseWithdrawalClickListener(Transaction transaction) {
			super();
			this.transaction = transaction;
		}

		@Override
		public void onClick(View v) {

			if (v instanceof RequestButton) {
				final RequestButton requestButton = (RequestButton) v;
				requestButton.startLoading(new InitAnimationListener() {
					@Override
					public void onAnimationFinished() {
						Application.get().getCurrentActivity().runOnUiThread(new Runnable() {

							@Override
							public void run() {
								// TODO read more link from the old app ?????
								if (isReverseWithdrawalConfirmButtonEnabled) {
									isReverseWithdrawalConfirmButtonEnabled = false;

									application.getPopUpManager().showConfirmPopUp(
											getString(R.string.bankingScreenListItemReverseWithdrawPopupTitle),
											getString(R.string.bankingScreenListItemReverseWithdrawPopupMessage,
													transaction.getAmountWF()),
											new OnConfirmPopUpButtonPressedListener() {
										@Override
										public void onPositive() {
											TransactionMethodRequest transRequest = new TransactionMethodRequest();
											transRequest.setTrans(new Transaction[] { transaction });
											isReverseWithdrawalConfirmButtonEnabled = !application
													.getCommunicationManager().requestService(BankingFragment.this,
															"reverseWithdrawalCallBack",
															Constants.SERVICE_INSERT_WITHDRAWS_REVERSE, transRequest,
															TransactionMethodResult.class);
										}

										@Override
										public void onNegative() {
											isReverseWithdrawalConfirmButtonEnabled = true;
											requestButton.stopLoading();
										}
									});
								}

							}
						});
					}
				});
			}

		}

	};
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	/**
	 * Callback method for the "reverse withdrawal" request.
	 * 
	 * @param result
	 */
	public void reverseWithdrawalCallBack(Object result) {

		TransactionMethodResult transactionResult = (TransactionMethodResult) result;
		if (transactionResult != null && transactionResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			application.getNotificationManager()
					.showNotificationSuccess(transactionResult.getUserMessages()[0].getMessage());

			User user = application.getUser();
			// Update the Balance:
			long newBalance = (long) (transactionResult.getBalance());
			user.setBalance(newBalance);
			user.setBalanceWF(AmountUtil.getFormattedAmount(newBalance, user.getCurrency()));
			application.postEvent(new ReverseWithdrawEvent());

			// Get Transaction again:
			hasTransactions = true;
			startRow = 0;
			transactionsList.clear();
			getTransactions();
			// Refresh Balance
			// TODO post User Update event
			transactionResult.getBalance();
		} else {
			isReverseWithdrawalConfirmButtonEnabled = true;
		}
	}

	@Override
	protected Screenable setupScreen() {
		return Screen.BANKING;
	}

}
