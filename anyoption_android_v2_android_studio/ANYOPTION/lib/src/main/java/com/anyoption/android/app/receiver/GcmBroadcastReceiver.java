package com.anyoption.android.app.receiver;

import com.appoxee.gcm.GCMBroadcastReceiver;

import android.content.Context;

/**
 * Receiver for the GCM (Google Cloud Messaging) messages.
 * @author Anastas Arnaudov
 *
 */
public class GcmBroadcastReceiver extends GCMBroadcastReceiver{

    /**
     * Gets the class name of the intent service that will handle GCM messages.
     */
    @Override
    protected String getGCMIntentServiceClassName(Context context) {
        String className = "com.anyoption.android.app.service.GcmIntentService";
        return className;
    }
	
}
