package com.anyoption.android.app.popup;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.manager.PopUpManager;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.TextUtils.OnInnerTextClickListener;
import com.anyoption.android.app.util.constants.Constants;

public class QuestionnairePEPBlockedOkDialogFragment extends OkDialogFragment {

	public static final String TAG = QuestionnairePEPBlockedOkDialogFragment.class.getSimpleName();
	
	protected TextView firstMessageTextView;
	protected TextView secondMessageTextView;
	protected TextView thirdMessageTextView;
	protected OnInnerTextClickListener supportTeamLinkListener;

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static QuestionnairePEPBlockedOkDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new QuestionnaireRestrictedOkDialogFragment.");
		QuestionnairePEPBlockedOkDialogFragment fragment = new QuestionnairePEPBlockedOkDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if(args != null){
			setCancelable(args.getBoolean(Constants.EXTRA_CANCELABLE, false));
		}
	}


	/**
	 * Set up content.
	 * 
	 * @param contentView
	 */
	@Override
	protected void onCreateContentView(View contentView) {

		supportTeamLinkListener = new OnInnerTextClickListener() {
			@Override
			public void onInnerTextClick(TextView textView, String text, String innerText) {
				dismiss();
				application.postEvent(new NavigationEvent(Screen.SUPPORT, NavigationType.DEEP));
			}
		};

		firstMessageTextView = (TextView) contentView.findViewById(R.id.questionnaire_pep_blocked_popup_first_message);
		secondMessageTextView = (TextView) contentView.findViewById(R.id.questionnaire_pep_blocked_popup_second_message);
		thirdMessageTextView = (TextView) contentView.findViewById(R.id.questionnaire_pep_blocked_popup_third_message);

		decrateText();

		View okButton = contentView.findViewById(R.id.ok_button);
		okButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}

	public void decrateText() {
		String userEmail = application.getUser().getEmail();
		secondMessageTextView.setText(secondMessageTextView.getText().toString() + " " + userEmail);
		TextUtils.decorateInnerText(secondMessageTextView, null, secondMessageTextView.getText().toString(), userEmail, Font.ROBOTO_BOLD, 0, 0);
		TextUtils.makeClickableInnerText(thirdMessageTextView, null, thirdMessageTextView.getText().toString(), getString(R.string.questionnaire_pep_blocked_popup_third_message_support_link), Font.ROBOTO_REGULAR, 0, R.color.link_color_1, true, supportTeamLinkListener);
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);
		RegulationUtils.showUploadDocuments();
	}

	@Override
	protected int getContentLayout() {
		return R.layout.popup_questionnaire_pep_blocked;
	}
	
	@Override
	public int getPopupCode(){
		return PopUpManager.POPUP_QUEST_PEP_BLOCKED;
	}
}
