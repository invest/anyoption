package com.anyoption.android.app.widget.form;

import com.anyoption.android.R;
import com.anyoption.android.app.util.constants.Constants;

import android.R.drawable;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.View.OnClickListener;

public class FormDocumentsUploadItem extends LinearLayout implements OnClickListener {

	public interface OnDocumentTypeSelectedListener {
		void onDocumentTypeSelected(View view);
	}

	private boolean isEnabled;

	private long creditCardID = 0l;
	private long documentType = 0l;
	private long currentStatus = 1l;
	private OnDocumentTypeSelectedListener documentTypeSelectedListener;

	private LinearLayout rootView;
	private ImageView imageImageView;
	private TextView nameTextView;
	private TextView fileNameTextView;
	private TextView waitingTextView;
	private TextView missingTextView;
	private TextView approvedTextView;
	private TextView invalidTextView;
	private ImageView arrowImageView;

	public FormDocumentsUploadItem(Context context) {
		super(context);
		init(context, null, 0);
	}

	public FormDocumentsUploadItem(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}

	public FormDocumentsUploadItem(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	protected void init(Context context, AttributeSet attrs, int defStyle) {
		rootView = (LinearLayout) View.inflate(getContext(), R.layout.upload_documents_item, this);
		imageImageView = (ImageView) rootView.findViewById(R.id.upload_documents_item_image);
		nameTextView = (TextView) rootView.findViewById(R.id.upload_documents_item_name);
		fileNameTextView = (TextView) rootView.findViewById(R.id.upload_documents_item_file_name);
		waitingTextView = (TextView) rootView.findViewById(R.id.upload_documents_item_waiting_message);
		missingTextView = (TextView) rootView.findViewById(R.id.upload_documents_item_missing_message);
		approvedTextView = (TextView) rootView.findViewById(R.id.upload_documents_item_approved_message);
		invalidTextView = (TextView) rootView.findViewById(R.id.upload_documents_item_invalid_message);
		arrowImageView = (ImageView) rootView.findViewById(R.id.upload_documents_item_arrow);

		if (!isInEditMode()) {
			rootView.setOnClickListener(this);
			nameTextView.setOnClickListener(this);
			imageImageView.setOnClickListener(this);
			fileNameTextView.setOnClickListener(this);
			waitingTextView.setOnClickListener(this);
			missingTextView.setOnClickListener(this);
			approvedTextView.setOnClickListener(this);
			invalidTextView.setOnClickListener(this);
		}
	}

	public void showWaiting() {
		missingTextView.setVisibility(View.GONE);
		approvedTextView.setVisibility(View.GONE);
		invalidTextView.setVisibility(View.GONE);
		fileNameTextView.setVisibility(View.VISIBLE);
		waitingTextView.setVisibility(View.VISIBLE);
		setCurrentStatus(Constants.UPLOAD_DOCUMENTS_DOCUMENT_STATUS_IN_PROGRESS);
	}

	public void showMissing() {
		approvedTextView.setVisibility(View.GONE);
		fileNameTextView.setVisibility(View.GONE);
		waitingTextView.setVisibility(View.GONE);
		invalidTextView.setVisibility(View.GONE);
		missingTextView.setVisibility(View.VISIBLE);
		setCurrentStatus(Constants.UPLOAD_DOCUMENTS_DOCUMENT_STATUS_REQUESTED);
	}

	public void showApproved() {
		missingTextView.setVisibility(View.GONE);
		waitingTextView.setVisibility(View.GONE);
		invalidTextView.setVisibility(View.GONE);
		fileNameTextView.setVisibility(View.VISIBLE);
		approvedTextView.setVisibility(View.VISIBLE);
		setCurrentStatus(Constants.UPLOAD_DOCUMENTS_DOCUMENT_STATUS_DONE);
	}

	public void showInvalid() {
		missingTextView.setVisibility(View.GONE);
		waitingTextView.setVisibility(View.GONE);
		approvedTextView.setVisibility(View.GONE);
		invalidTextView.setVisibility(View.VISIBLE);
		fileNameTextView.setVisibility(View.VISIBLE);
		setCurrentStatus(Constants.UPLOAD_DOCUMENTS_DOCUMENT_STATUS_INVALID);
	}

	public void hideAll() {
		missingTextView.setVisibility(View.GONE);
		waitingTextView.setVisibility(View.GONE);
		fileNameTextView.setVisibility(View.GONE);
		approvedTextView.setVisibility(View.GONE);
		invalidTextView.setVisibility(View.GONE);
		setCurrentStatus(0l);
	}

	public void hideAllStatuses() {
		missingTextView.setVisibility(View.GONE);
		waitingTextView.setVisibility(View.GONE);
		approvedTextView.setVisibility(View.GONE);
		invalidTextView.setVisibility(View.GONE);
		setCurrentStatus(0l);
	}

	public void setLocked(boolean isLocked) {
		setEnabled(!isLocked);
		if(isLocked) {
			hideArrow();
		} else {
			showArrow();
		}
	}

	public long getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(long currentStatus) {
		this.currentStatus = currentStatus;
	}

	public void showArrow() {
		arrowImageView.setVisibility(View.VISIBLE);
	}

	public void hideArrow() {
		arrowImageView.setVisibility(View.GONE);
	}

	public void setImage(int imageID) {
		imageImageView.setImageResource(imageID);
	}

	public void setName(String name) {
		nameTextView.setText(name);
	}

	public String getName() {
		return nameTextView.getText().toString();
	}

	public void setCreditCardID(long creditCardID) {
		this.creditCardID = creditCardID;
	}

	public long getCreditCardID() {
		return creditCardID;
	}

	public void setDocumentType(long documentType) {
		this.documentType = documentType;
	}

	public long getDocumentType() {
		return documentType;
	}

	public void setFileName(String fileName) {
		if (fileName.length() > 30) {
			fileName = fileName.substring(0, 30) + "..";
		}

		fileNameTextView.setText(fileName);
		if (!fileName.equals("")) {
			if (!getName().endsWith(":")) {
				setName(getName() + ":");
			}
		} else {
			setName(getName().replace(":", ""));
		}
	}

	public String getFileName() {
		return fileNameTextView.getText().toString();
	}

	public void setDocumentTypeSelectedListener(OnDocumentTypeSelectedListener documentTypeSelectedListener) {
		this.documentTypeSelectedListener = documentTypeSelectedListener;
	}

	@Override
	public void onClick(View v) {
		if (v == rootView || v == waitingTextView || v == missingTextView || v == approvedTextView || v == invalidTextView
				|| v == fileNameTextView || v == nameTextView || v == imageImageView) {
			if (isEnabled) {
				documentTypeSelectedListener.onDocumentTypeSelected(this);
			}
		}
	}

	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}
}
