package com.anyoption.android.app.model;

import java.io.Serializable;

public class LoseWinItem implements Serializable {

	private static final long serialVersionUID = -1886613507874003891L;

	private long lose;
	private long win;

	public LoseWinItem(long lose, long win) {
		this.lose = lose;
		this.win = win;
	}

	public long getWin() {
		return win;
	}

	public void setWin(long win) {
		this.win = win;
	}

	public long getLose() {
		return lose;
	}

	public void setLose(long lose) {
		this.lose = lose;
	}

}
