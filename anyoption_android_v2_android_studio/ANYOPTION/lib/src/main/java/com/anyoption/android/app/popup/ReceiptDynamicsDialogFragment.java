package com.anyoption.android.app.popup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.User;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

/**
 * Fullscreen Dialog Fragment showing investment receipt.
 * 
 * @author Mario Kutlev
 */
public class ReceiptDynamicsDialogFragment extends BaseDialogFragment {

	private static final String TAG = ReceiptDynamicsDialogFragment.class.getSimpleName();

	public static final String ARG_INVESTMENT_KEY = "arg_investment_key";
	protected static final String TIME_FORMAT_PATTERN = "HH:mm";

	protected User user;
	private Currency currency;
	protected Investment investment;
	private Market market;
	private DecimalFormat levelFormat;
	private DecimalFormat percentFormat;

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static ReceiptDynamicsDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new ReceiptDynamicsDialogFragment.");
		ReceiptDynamicsDialogFragment fragment = new ReceiptDynamicsDialogFragment();
		fragment.setArguments(bundle);

		return fragment;
	}

	public static ReceiptDynamicsDialogFragment newInstance(Investment investment) {
		Bundle args = new Bundle();
		args.putSerializable(ARG_INVESTMENT_KEY, investment);
		return newInstance(args);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		user = Application.get().getUser();
		currency = user.getCurrency();

		Bundle args = getArguments();
		investment = (Investment) args.getSerializable(ARG_INVESTMENT_KEY);
		market = application.getMarketForID(investment.getMarketId());

		int decimalPointDigits = (int)(long) market.getDecimalPoint();
		levelFormat         = Utils.getDecimalFormat(decimalPointDigits);
		levelFormat.applyPattern(Utils.getDecimalFormatPattern(decimalPointDigits));
		percentFormat   = new DecimalFormat("##.##");
	}

	/**
	 * Set up content.
	 * 
	 * @param contentView
	 */
	@Override
	protected void onCreateContentView(View contentView) {
		View okButton = contentView.findViewById(R.id.receipt_ok_button);
		okButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});

		//Set up the Investment type icon:
		ImageView investTypeImageView = (ImageView) contentView.findViewById(R.id.receipt_put_call_icon);
		if(investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_SELL){
			investTypeImageView.setImageResource(R.drawable.circle_with_triangle_down_white);
		}else{
			investTypeImageView.setImageResource(R.drawable.circle_with_triangle_up_white);
		}

		//Set up the Market name:
		TextView marketNameTextView = (TextView) contentView.findViewById(R.id.marketNameTextView);
		marketNameTextView.setText(market.getDisplayName());

		//Set up the Level and Time:
		TextView levelTextView = (TextView) contentView.findViewById(R.id.optionLevelTimeTextView);
		String type = investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY ? getString(R.string.aboveUpper) : getString(R.string.belowUpper);
		SimpleDateFormat expiryTimeFormat = new SimpleDateFormat(TIME_FORMAT_PATTERN);
		String timeFormatted = expiryTimeFormat.format(investment.getTimeCreated());
		String levelTimeLabel = "(" + type + ") " + levelFormat.format(investment.getEventLevel()) + " " + getString(R.string.atTimeToday, timeFormatted);
		levelTextView.setText(levelTimeLabel);

		//Set up the invested amount:
		TextView investAmountTextView = (TextView) contentView.findViewById(R.id.receipt_amount);
		investAmountTextView.setText(AmountUtil.getFormattedAmount(investment.getAmount(), currency));

		//Set up the profit percent and amount:
		TextView profitPercentTextView 		= (TextView) contentView.findViewById(R.id.receipt_profit_percent);
		TextView returnIfCorrectTextView 	= (TextView) contentView.findViewById(R.id.returnCorrectTextView);
		TextView returnIfIncorrectTextView 	= (TextView) contentView.findViewById(R.id.returnIncorrectTextView);
		double profitPercent;
		if(investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY){
			profitPercent	= (investment.getPrice() == 0) ? 100 : 100 * (100 - investment.getPrice()) / investment.getPrice();
		}else{
			profitPercent	= (investment.getPrice() == 100) ? 100 : 100 * investment.getPrice() / (100 - investment.getPrice());
		}

		profitPercentTextView.setText(percentFormat.format(profitPercent) + "%");
		returnIfCorrectTextView.setText(AmountUtil.getFormattedAmount(AmountUtil.getDoubleAmount(investment.getAmount()) * (1 + profitPercent / 100), currency));
		returnIfIncorrectTextView.setText(AmountUtil.getFormattedAmount(0, currency));
	}

	@Override
	protected int getContentLayout() {
		return R.layout.receipt_dynamics_dialog;
	}

}
