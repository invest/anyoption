package com.anyoption.android.app.event;

/**
 * Custom Event that occurs when the user makes Successful Deposit.
 * @author Anastas Arnaudov
 *
 */
public class DepositEvent {

	private double amount;
	private double dollarAmount;
	private boolean isFirstDeposit;
	private long transactionId;

	public DepositEvent(double amount,double dollarAmount, boolean isFirstDeposit){
		this.setAmount(amount);
		this.setDollarAmount(dollarAmount);
		this.setFirstDeposit(isFirstDeposit);
	}

	public DepositEvent(double amount,double dollarAmount, boolean isFirstDeposit, long transactionId){
		this.setAmount(amount);
		this.setDollarAmount(dollarAmount);
		this.setFirstDeposit(isFirstDeposit);
		this.setTransactionId(transactionId);
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public boolean isFirstDeposit() {
		return isFirstDeposit;
	}

	public void setFirstDeposit(boolean isFirstDeposit) {
		this.isFirstDeposit = isFirstDeposit;
	}

	public double getDollarAmount() {
		return dollarAmount;
	}

	public void setDollarAmount(double dollarAmount) {
		this.dollarAmount = dollarAmount;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
}
