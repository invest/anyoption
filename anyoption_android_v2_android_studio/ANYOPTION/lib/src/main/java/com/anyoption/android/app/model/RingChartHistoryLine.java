package com.anyoption.android.app.model;

/**
 * @author Anastas Arnaudov
 */
public class RingChartHistoryLine {
    public float level;
    public float startPointX;
    public float startPointY;
    public float endPointX;
    public float endPointY;
    public int alpha;
}