package com.anyoption.android.app.event;

/**
 * Event that occurs when the Action Bar title or image needs to be changed.
 * @author Anastas Arnaudov
 *
 */
public class UpdateTitleEvent {

	private String title;
	private int imageID;
	
	public UpdateTitleEvent(String title) {
		this.title = title;
	}

	public UpdateTitleEvent(int imageID) {
		this.setImageID(imageID);
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getImageID() {
		return imageID;
	}

	public void setImageID(int imageID) {
		this.imageID = imageID;
	}

}
