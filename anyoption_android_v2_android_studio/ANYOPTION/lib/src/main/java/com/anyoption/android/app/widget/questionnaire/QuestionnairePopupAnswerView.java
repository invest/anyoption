package com.anyoption.android.app.widget.questionnaire;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.model.Answer;
import com.anyoption.android.app.model.Question;
import com.anyoption.android.app.popup.QuestionnaireAnswersDialogFragment;

import java.util.Arrays;

/**
 * Created by veselin.hristozov on 10.10.2016 г..
 */

public class QuestionnairePopupAnswerView extends QuestionnaireBaseView implements QuestionnaireAnswersDialogFragment.OnItemSelectedListener, View.OnClickListener {

    private QuestionnaireAnswersDialogFragment dialog;

    private LinearLayout rootView;
    private TextView answerTextView;

    public QuestionnairePopupAnswerView(Context context) {
        super(context);
    }

    public QuestionnairePopupAnswerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public QuestionnairePopupAnswerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void init() {
        questionType = Question.QuestionType.POPUP;

        rootView = (LinearLayout) View.inflate(getContext(), R.layout.questionanaire_popup_item, this);
        questionTextView = (TextView) rootView.findViewById(R.id.questionnaire_item_question_text_view);
        answerTextView = (TextView) rootView.findViewById(R.id.questionnaire_item_answer_text_view);
        errorTextView = (TextView) rootView.findViewById(R.id.questionnaire_item_error_text_view);

        if (!isInEditMode()) {
            rootView.setOnClickListener(this);
            questionTextView.setOnClickListener(this);
            answerTextView.setOnClickListener(this);
        }

    }

    @Override
    public void setAnswer(Answer answer) {
        super.setAnswer(answer);

        answerTextView.setText(answer.getDescription());
        answerTextView.setVisibility(View.VISIBLE);

        errorTextView.setVisibility(View.GONE);
    }

    public void showError(){
        answerTextView.setVisibility(View.GONE);
        errorTextView.setVisibility(View.VISIBLE);
    }

    public void setQuestion(Question question){
        super.setQuestion(question);

        decorateText(questionTextView, question.getDescription());
    }

    public void setAnswerSelectedListener(OnAnswerSelectedListener answerSelectedListener) {
        this.answerSelectedListener = answerSelectedListener;
    }

    private void showDialog(){
        if(dialog == null){
            dialog = (QuestionnaireAnswersDialogFragment) Application.get().getFragmentManager().showDialogFragment(QuestionnaireAnswersDialogFragment.class, null);
            dialog.setOnItemSelectedListener(this);
            dialog.setItems(Arrays.asList(question.getAnswers()));
        }else{
            Application.get().getFragmentManager().showDialogFragment(dialog);
        }
    }

    @Override
    public void onItemSelected(Answer item, int position) {
        setAnswer(item);

        if(answerSelectedListener != null){
            answerSelectedListener.onAnswerSelected(question, item);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == rootView || v == questionTextView || v == answerTextView) {
            if (isEnabled) {
                showDialog();
            }
        }
    }

    @Override
    public boolean isAnswered() {
        if(question.isMandatory()) {
            return getAnswer() != null;
        } else {
            return true;
        }
    }
}
