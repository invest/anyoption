package com.anyoption.android.app.widget.questionnaire;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.model.Answer;
import com.anyoption.android.app.model.Question;
import com.anyoption.android.app.widget.CheckBox;

/**
 * Created by veselin.hristozov on 10.10.2016 г..
 */

public class QuestionnaireCheckBoxAnswerView extends QuestionnaireBaseView implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private LinearLayout rootView;
    private CheckBox questionCheckBox;
    private Answer correctAnswer;

    public QuestionnaireCheckBoxAnswerView(Context context) {
        super(context);
    }

    public QuestionnaireCheckBoxAnswerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public QuestionnaireCheckBoxAnswerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void init() {
        questionType = Question.QuestionType.CHECKBOX;

        rootView = (LinearLayout) View.inflate(getContext(), R.layout.questionanaire_checkbox_item, this);
        questionTextView = (TextView) rootView.findViewById(R.id.questionnaire_item_checkbox_text);
        questionCheckBox = (CheckBox) rootView.findViewById(R.id.questionnaire_item_checkbox);
        errorTextView = (TextView) rootView.findViewById(R.id.questionnaire_item_error_text_view);

        if (!isInEditMode()) {
            questionTextView.setOnClickListener(this);
            questionCheckBox.setOnCheckedChangeListener(this);
        }
    }

    @Override
    public void setAnswer(Answer answer) {
        super.setAnswer(answer);

        if(answer.getId() == question.getAnswers()[0].getId()) {
            questionCheckBox.setChecked(true);
        } else {
            questionCheckBox.setChecked(false);
        }

        errorTextView.setVisibility(View.GONE);
    }

    public void showError(){
        errorTextView.setVisibility(View.VISIBLE);
    }

    public void setQuestion(Question question){
        super.setQuestion(question);

        decorateText(questionTextView, question.getDescription());
    }

    public void setAnswerSelectedListener(OnAnswerSelectedListener answerSelectedListener) {
        this.answerSelectedListener = answerSelectedListener;
    }

    public void onAnswerChanged(boolean isChecked) {
        Answer chosenAnswer;

        if(isChecked) {
            chosenAnswer = question.getAnswers()[0];
        } else {
            chosenAnswer = question.getAnswers()[1];
        }

        setAnswer(chosenAnswer);

        if(answerSelectedListener != null){
            answerSelectedListener.onAnswerSelected(question, chosenAnswer);
        }
    }

    @Override
    public void onClick(View v) {
        if(v == questionCheckBox) {
            onAnswerChanged(questionCheckBox.isChecked());
        } else if(v == questionTextView) {
            onAnswerChanged(!questionCheckBox.isChecked());
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        onAnswerChanged(isChecked);
    }

    public TextView getQuestionTextView() {
        return questionTextView;
    }

    @Override
    public boolean isAnswered() {
        //TODO NOT IN USE
        /*if(question.isMandatory()) {
            if (correctAnswer != null && correctAnswer.getId() != 0L) {
                return getAnswer().getId() == correctAnswer.getId();
            } else if (correctAnswer != null && correctAnswer.getId() == 0L) {
                return true;
            }
        } else {
            return true;
        }
        return false;*/

        return true;
    }

    public Answer getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(Answer correctAnswer) {
        this.correctAnswer = correctAnswer;
    }
}
