package com.anyoption.android.app.widget.chart;

/**
 * @author Anastas Arnaudov
 */
public interface OnScrollToLeftEdgeListener {

    /**
     * Called when the view is swiped to the left edge.
     *
     * @return true if the event is handled, false otherwise.
     */
    boolean onScrollToLeftEdge();

}
