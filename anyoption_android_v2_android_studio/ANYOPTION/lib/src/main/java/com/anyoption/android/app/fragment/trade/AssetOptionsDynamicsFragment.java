package com.anyoption.android.app.fragment.trade;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ExpandableListView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.ChartCommonEvent;
import com.anyoption.android.app.event.InvestmentEvent;
import com.anyoption.android.app.event.LightstreamerUpdateEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.UserUpdatedEvent;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.anyoption.android.app.model.OptionDynamicsGroup;
import com.anyoption.android.app.model.OptionDynamicsItem;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.popup.ExpiredDynamicsOptionDialogFragment;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.InvestmentUtil;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.android.app.util.adapter.OptionsDynamicsAdapter;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.TextView;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.service.requests.InvestmentsMethodRequest;
import com.anyoption.common.service.requests.SellDynamicsInvestmentRequest;
import com.anyoption.common.service.results.ChartDataResult;
import com.anyoption.common.service.results.InvestmentMethodResult;
import com.anyoption.common.service.results.InvestmentsMethodResult;
import com.lightstreamer.ls_client.UpdateInfo;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AssetOptionsDynamicsFragment extends BaseAssetNestedFragment implements View.OnClickListener, Callback, OptionsDynamicsAdapter.CloseOptionListener {

	public static final String TAG = AssetOptionsDynamicsFragment.class.getSimpleName();

	private static final int MESSAGE_UPDATE_OPTIONS = 1;
	private static final int MESSAGE_STATE_CHANGED 	= 2;

	private static final int PAGE_SIZE 	= Application.get().getResources().getBoolean(R.bool.isTablet) ? 15 : 6;


	private com.anyoption.common.beans.base.Currency currency;

	public static final long OPTION_CLOSED_TIME = 3000L;

	private static final int PERIOD_MONTH = 3;

	protected Handler handler;
	protected int opportunityState;

	protected boolean isTablet;
	private double investedAmount;
	private double returnAboveAmount;
	private double returnBelowAmount;
	private double closeAllAmount;
	private double closeAllOptionsAmount;

	private View root;
	private View investDetailsLayout;
	private TextView closedAllTextView;
	private TextView openOptionsTextView;
	private TextView expireAboveTextView;
	private TextView expireBellowTextView;
	private TextView closeAllTextView;

	protected OptionsDynamicsAdapter adapter;
	private List<Investment> openOptionsList;
	protected List<Investment> settledOptionsList;
	private boolean isSettleOptionsLoaded;
	private boolean isOpenOptionsLoaded;
	private View noOptionsView;
	private OptionDynamicsGroup openAboveGroup;
	private OptionDynamicsGroup openBellowGroup;
	private OptionDynamicsGroup settledGroup;
	protected ExpandableListView listView;
	private View allOptionsLayout;
	private long opportunityId;
	private long opportunityTypeId;
	private boolean isExpiredOptionsPopupShown;
	private BaseDialogFragment expiredOptionsPopup;
	private boolean isClosingOption;
	private View closedAllInTheMoney;
	private double profitCallParam;
	private double profitPutParam;
	private int startRow;
	private View loadingView;
	private boolean isLastSettledOptionsReached;

	/**
	 * Use this method to create new instance of the fragment.
	 *
	 * @return
	 */
	public static AssetOptionsDynamicsFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new AssetOptionsDynamicsFragment.");
		AssetOptionsDynamicsFragment fragment = new AssetOptionsDynamicsFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	public AssetOptionsDynamicsFragment() {
		// Empty constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		isTablet = getResources().getBoolean(R.bool.isTablet);

		Bundle args = getArguments();
		if (args != null) {
			market = (Market) args.getSerializable(Constants.EXTRA_MARKET);
		}

		currency 			= application.getCurrency();
		handler 			= new Handler(this);
		openOptionsList		= new ArrayList<Investment>();
		settledOptionsList	= new ArrayList<Investment>();

		startRow = 0;
		isLastSettledOptionsReached = false;

		if(application.isLoggedIn()){
			getSettledInvestments();
		}
	}

	@Override
	protected void onMarketChanged() {
		if(isTablet){
			if(openOptionsList == null){
				openOptionsList		= new ArrayList<Investment>();
			}else{
				openOptionsList.clear();
			}
			if(settledOptionsList == null){
				settledOptionsList	= new ArrayList<Investment>();
			}else{
				settledOptionsList.clear();
			}

			startRow = 0;
			isLastSettledOptionsReached = false;

			if(adapter != null){
				adapter.notifyDataSetChanged();
			}

			if(application.isLoggedIn()){
				getSettledInvestments();
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		root = inflater.inflate(R.layout.asset_options_dynamics_fragment, container, false);

		allOptionsLayout 	  = 		   root.findViewById(R.id.allOptionsLayout);
		closedAllTextView	  =	(TextView) root.findViewById(R.id.dynamicsOptionClosedAllTextView);
		closedAllInTheMoney	  =			   root.findViewById(R.id.inTheMoneyImageView);
		investDetailsLayout   = 		   root.findViewById(R.id.dynamicsInvestDetailsLayout);
		openOptionsTextView   = (TextView) root.findViewById(R.id.investDetailsOpenOptionsTextView);
		expireAboveTextView   = (TextView) root.findViewById(R.id.investDetailsExpireAboveTextView);
		expireBellowTextView  = (TextView) root.findViewById(R.id.investDetailsExpireBellowTextView);
		closeAllTextView      = (TextView) root.findViewById(R.id.investDetailsCloseAllTextView);
		noOptionsView 	  	  = 		   root.findViewById(R.id.dynamicsOptionsNoOptionsView);
		loadingView			  =			   root.findViewById(R.id.loadingView);

		closeAllTextView.setOnClickListener(this);

		formatOpenOptionsAmountText();
		formatExpireAboveAmountText();
		formatExpireBellowAmountText();
		formatCloseAllButton();

		listView = (ExpandableListView) root.findViewById(R.id.dynamicsOptionsListView);
		adapter  = new OptionsDynamicsAdapter();
		adapter.setCloseOptionListener(this);
		listView.setAdapter(adapter);
		listView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
			@Override
			public void onGroupCollapse(int groupPosition) {
				listView.expandGroup(groupPosition);
			}
		});
		listView.setOnScrollListener(new AbsListView.OnScrollListener() {

			private int currentLastVisibleItem;
			private int currentVisibleItemCount;
			private int currentScrollState;

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				this.currentScrollState = scrollState;
				isScrollCompleted();
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,int totalItemCount) {
				this.currentLastVisibleItem = firstVisibleItem + visibleItemCount;
				this.currentVisibleItemCount = visibleItemCount;
			}

			private void isScrollCompleted() {
				if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
					// Detect if there's been a scroll which has completed.
					// And check if the list is scrolled to the bottom.
					if (currentLastVisibleItem >= settledOptionsList.size()) {
						if(!isLastSettledOptionsReached){
							Log.d(TAG, "List is scrolled to the bottom. Request more investments!");
							getSettledInvestments();
						}
					}
				}
			}

		});

		setupListChildClickListenber();

		expandListGroups();

		return root;
	}

	protected void setupListChildClickListenber(){
		//Override point
	}

	@Override
	public void onResume() {
		super.onResume();
		registerForEvents();
	}

	@Override
	public void onPause() {
		application.unregisterForAllEvents(this);
		super.onPause();
	}

	@Override
	public void onDestroyView() {
		application.unregisterForAllEvents(this);
		super.onDestroyView();
	}

	protected void setupLoading(){
		if(loadingView != null){
			if(!isOpenOptionsLoaded || !isSettleOptionsLoaded){
				loadingView.setVisibility(View.VISIBLE);
			}else{
				loadingView.setVisibility(View.GONE);
			}
		}
	}

	private void registerForEvents() {
		application.registerForEvents(this, LoginLogoutEvent.class);

		if (market != null) {
			application.registerForEvents(this, LightstreamerUpdateEvent.class, ChartCommonEvent.class, InvestmentEvent.class);
		}
	}

	@Override
	public void loadData() {
	}

	private void expandListGroups(){
		//Expand list groups:
		for (int i = 0; i < adapter.getGroups().size(); i++) {
			listView.expandGroup(i);
		}
	}

	private void setupOptionsLists(){
		if(isOpenOptionsLoaded && isSettleOptionsLoaded){
			Log.d(TAG, "setupOptionsLists");

			//Clear groups:
			adapter.getGroups().clear();

			openAboveGroup 		= null;
			openBellowGroup 	= null;
			settledGroup 		= null;

			//Check Open Options List:
			if(!openOptionsList.isEmpty()){
				Log.d(TAG, "Set Open Options List.");

				for(Investment investment : openOptionsList){
					if(investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY){
						Log.d(TAG, "Investment ABOVE price = " + investment.getPrice());
						//There is atleast 1 Above Open Option so we create the Above Group:
						if(openAboveGroup == null){
							openAboveGroup = new OptionDynamicsGroup(OptionDynamicsGroup.Type.OPEN_ABOVE);
						}
						OptionDynamicsItem optionAbove = new OptionDynamicsItem();
						optionAbove.id				= investment.getId();
						optionAbove.investType 		= OptionDynamicsItem.InvestType.CALL;
						optionAbove.price			= investment.getPrice();
						optionAbove.time 			= investment.getTimeCreated();
						optionAbove.investAmount 	= AmountUtil.getDoubleAmount(investment.getAmount());
						if(investment.getPrice() <= 0){
							optionAbove.profitPercent = 100;
						}else if(investment.getPrice() >= 100){
							optionAbove.profitPercent = 0;
						}else{
							optionAbove.profitPercent	= (investment.getPrice() == 0) ? 100 : 100 * (100 - investment.getPrice()) / investment.getPrice();
						}
						optionAbove.returnAmount = optionAbove.investAmount * (1 + optionAbove.profitPercent / 100);
						optionAbove.isCloseEnabled  = false;

						openAboveGroup.getItemsList().add(optionAbove);
					}else if(investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_SELL){
						Log.d(TAG, "Investment BELLOW price = " + investment.getPrice());
						//There is atleast 1 Bellow Open Option so we create the Bellow Group:
						if(openBellowGroup == null){
							openBellowGroup = new OptionDynamicsGroup(OptionDynamicsGroup.Type.OPEN_BELLOW);
						}
						OptionDynamicsItem optionBellow = new OptionDynamicsItem();
						optionBellow.id				= investment.getId();
						optionBellow.investType 	= OptionDynamicsItem.InvestType.PUT;
						optionBellow.price			= investment.getPrice();
						optionBellow.time 			= investment.getTimeCreated();
						optionBellow.investAmount 	= AmountUtil.getDoubleAmount(investment.getAmount());
						if(investment.getPrice() <= 0){
							optionBellow.profitPercent = 0;
						}else if(investment.getPrice() >= 100){
							optionBellow.profitPercent = 100;
						}else{
							optionBellow.profitPercent	= (investment.getPrice() == 100) ? 100 : 100 * investment.getPrice() / (100 - investment.getPrice());
						}
						optionBellow.returnAmount = optionBellow.investAmount * (1 + optionBellow.profitPercent / 100);
						optionBellow.isCloseEnabled = false;

						openBellowGroup.getItemsList().add(optionBellow);
					}
				}
			}else{
				Log.d(TAG, "No Open Options List.");
			}

			//Check Settled Options List:
			if(!settledOptionsList.isEmpty()){
				settledGroup = new OptionDynamicsGroup(OptionDynamicsGroup.Type.SETTLED);
				Log.d(TAG, "Set Settled Options List.");
				for(Investment investment : settledOptionsList){
					if(investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY || investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_SELL){
						OptionDynamicsItem optionSettled = new OptionDynamicsItem();
						optionSettled.investType = (investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY) ? OptionDynamicsItem.InvestType.CALL : OptionDynamicsItem.InvestType.PUT;
						optionSettled.id			 		= investment.getId();
						optionSettled.time          		= investment.getTimeSettled();
						optionSettled.investAmount  		= AmountUtil.getDoubleAmount(investment.getAmount());
						optionSettled.investLevel   		= Double.valueOf(investment.getLevel().replaceAll(",", ""));
						optionSettled.profitPercent			= InvestmentUtil.getSettledInvestmentProfitPercent(investment);
						optionSettled.returnAmountFormated  = investment.getAmountReturnWF();
						optionSettled.isInTheMoney 			= InvestmentUtil.isSettledInvestmentProfitable(investment, currency);

						settledGroup.getItemsList().add(optionSettled);
					}
				}
			}else{
				Log.d(TAG, "No Settled Options List.");
			}

			if(openAboveGroup != null){
				adapter.getGroups().add(openAboveGroup);
			}
			if(openBellowGroup != null){
				adapter.getGroups().add(openBellowGroup);
			}
			if(settledGroup != null){
				adapter.getGroups().add(settledGroup);
			}

			adapter.notifyDataSetChanged();
			expandListGroups();

			//Check if there are no Options:
			if(adapter.getGroups().isEmpty()){
				showNoOptionsView(true);
			}else{
				showNoOptionsView(false);
			}
		}
	}

	private void setupAllAmounts(){
		if(!isAdded()){
			return;
		}

		//Update the ALL Options:
		double investAmount 		= 0;
		double profitAboveAmount 	= 0;
		double profitBellowAmount 	= 0;
		double closeAmount			= 0;

		if(openAboveGroup != null && !openAboveGroup.getItemsList().isEmpty()){
			for(OptionDynamicsItem optionDynamicsItem : openAboveGroup.getItemsList()){
				investAmount 		+= optionDynamicsItem.investAmount;
				profitAboveAmount 	+= optionDynamicsItem.returnAmount;
				closeAmount			+= optionDynamicsItem.closeForAmount;
			}
		}
		if(openBellowGroup != null && !openBellowGroup.getItemsList().isEmpty()){
			for(OptionDynamicsItem optionDynamicsItem : openBellowGroup.getItemsList()){
				investAmount 		+= optionDynamicsItem.investAmount;
				profitBellowAmount 	+= optionDynamicsItem.returnAmount;
				closeAmount			+= optionDynamicsItem.closeForAmount;
			}
		}
		investedAmount = investAmount;
		returnAboveAmount = profitAboveAmount;
		returnBelowAmount = profitBellowAmount;
		closeAllAmount 		= AmountUtil.roundToFourthDigitFormat(closeAmount);

		formatOpenOptionsAmountText();
		formatExpireAboveAmountText();
		formatExpireBellowAmountText();
		formatCloseAllButton();
		formatAllOptionsClosedText();
	}

	private void showNoOptionsView(boolean isShow){
		if(noOptionsView != null){
			if(isShow){
				noOptionsView.setVisibility(View.VISIBLE);
			}else{
				noOptionsView.setVisibility(View.GONE);
			}
		}
	}

	private void formatOpenOptionsAmountText(){
		AmountUtil.setTextViewAmount(openOptionsTextView, investedAmount, currency.getSymbol(), currency.getIsLeftSymbolBool(), currency.getDecimalPointDigits());
	}

	private void formatExpireAboveAmountText(){
		AmountUtil.setTextViewAmount(expireAboveTextView, returnAboveAmount, currency.getSymbol(), currency.getIsLeftSymbolBool(), currency.getDecimalPointDigits());
	}

	private void formatExpireBellowAmountText(){
		AmountUtil.setTextViewAmount(expireBellowTextView, returnBelowAmount, currency.getSymbol(), currency.getIsLeftSymbolBool(), currency.getDecimalPointDigits());
	}

	private void formatCloseAllButton(){
		if(!isAdded()){
			return;
		}
		if(opportunityState == Opportunity.STATE_OPENED){
			if(!isClosingOption && closeAllAmount > 0){
				closeAllTextView.setEnabled(true);
			}else{
				closeAllTextView.setEnabled(false);
			}
			String amountFormatted = AmountUtil.getFormattedAmount(closeAllAmount, currency);
			closeAllTextView.setText(getString(R.string.investDetailsCloseAll, amountFormatted));
			TextUtils.decorateInnerText(closeAllTextView, null, closeAllTextView.getText().toString(), amountFormatted, FontsUtils.Font.ROBOTO_MEDIUM, 0, getResources().getDimensionPixelSize(R.dimen.asset_page_dynamics_invest_details_close_button_amount_text_size));
		}else{
			closeAllTextView.setText(getString(R.string.investDetailsCloseAll, ""));
			closeAllTextView.setEnabled(false);
		}

		//Check in the Money Ribbon:
		if(closeAllAmount > investedAmount && closeAllTextView.isEnabled()){
			closedAllInTheMoney.setVisibility(View.VISIBLE);
		}else{
			closedAllInTheMoney.setVisibility(View.GONE);
		}
	}

	private void formatAllOptionsClosedText(){
		String amountFormatted = AmountUtil.getFormattedAmount(closeAllOptionsAmount, currency);
		closedAllTextView.setText(getString(R.string.optionClosed, amountFormatted));
	}

	@Override
	public void onClick(View v) {
		if(v == closeAllTextView){
			isClosingOption = true;
			//Disable the Close buttons:
			closeAllTextView.setEnabled(false);

			if(openAboveGroup != null && !openAboveGroup.getItemsList().isEmpty()){
				for(OptionDynamicsItem optionDynamicsItem : openAboveGroup.getItemsList()){
					optionDynamicsItem.isCloseEnabled = false;
				}
			}

			if(openBellowGroup != null && !openBellowGroup.getItemsList().isEmpty()){
				for(OptionDynamicsItem optionDynamicsItem : openBellowGroup.getItemsList()){
					optionDynamicsItem.isCloseEnabled = false;
				}
			}
			adapter.notifyDataSetChanged();

			closeAllOptions();
		}
	}

	//////////////////////////////////////////////////////////////////////////////////
	//                        REQUESTs and CALLBACKs                          		//
	//////////////////////////////////////////////////////////////////////////////////

	private void getSettledInvestments() {
		Log.d(TAG, "getSettledInvestments");
		isSettleOptionsLoaded = false;
		setupLoading();
		InvestmentsMethodRequest request = new InvestmentsMethodRequest();
		request.setSettled(true);
		request.setMarketId(market.getId());
		request.setStartRow(startRow);
		request.setPageSize(PAGE_SIZE);
		request.setPeriod(PERIOD_MONTH);

		application.getCommunicationManager().requestService(this, "gotSettledInvestments", Constants.SERVICE_GET_INVESTMENTS, request, InvestmentsMethodResult.class, false, false);
	}

	public void gotSettledInvestments(Object result) {
		Log.d(TAG, "gotSettledInvestments");
		InvestmentsMethodResult investmentsResult = (InvestmentsMethodResult) result;
		if (investmentsResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Investment[] investmentsArr = investmentsResult.getInvestments();
			if(investmentsArr != null && investmentsArr.length > 0){
				InvestmentUtil.fixDate(investmentsResult.getInvestments());
				Collections.addAll(settledOptionsList, investmentsResult.getInvestments());
			}
			isSettleOptionsLoaded = true;
			setupLoading();
			startRow += PAGE_SIZE;
			if(investmentsResult.getInvestments().length < PAGE_SIZE){
				isLastSettledOptionsReached = true;
			}
			setupOptionsLists();
		} else {
			Log.e(TAG, "Could not get settled options!");
			ErrorUtils.handleResultError(investmentsResult);
		}
	}

	private void closeOption(OptionDynamicsItem optionDynamicsItem){
		isClosingOption = true;
		SellDynamicsInvestmentRequest request = new SellDynamicsInvestmentRequest();
		request.setInvestmentId(optionDynamicsItem.id);
		request.setOpportunityId(opportunityId);
		request.setOpportunityTypeId(opportunityTypeId);
		request.setFullAmount(AmountUtil.getLongAmount(optionDynamicsItem.investAmount));
		request.setSellAmount(AmountUtil.getLongAmount(optionDynamicsItem.investAmount));
		request.setOffer(profitCallParam);
		request.setBid(profitPutParam);

		if(!application.getCommunicationManager().requestService(this, "closedOption", Constants.SERVICE_SELL_DYNAMICS_INVESTMENT, request, InvestmentMethodResult.class, false, false)){
			adapter.closeOptionCallback(false, optionDynamicsItem);
			isClosingOption = false;
		}else{
			if(openAboveGroup != null && !openAboveGroup.getItemsList().isEmpty()){
				for(OptionDynamicsItem item : openAboveGroup.getItemsList()){
					item.isCloseEnabled = false;
				}
			}
			if(openBellowGroup != null && !openBellowGroup.getItemsList().isEmpty()){
				for(OptionDynamicsItem item : openBellowGroup.getItemsList()){
					item.isCloseEnabled = false;
				}
			}
			adapter.notifyDataSetChanged();

			formatCloseAllButton();
		}

	}

	public void closedOption(Object result) {
		InvestmentMethodResult investmentsResult = (InvestmentMethodResult) result;
		if (investmentsResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Option was closed!");
			Investment investment = investmentsResult.getInvestment();
			if(investment != null){
				Investment investmentToRemove = null;
				for(Investment openOption : openOptionsList){
					if(investment.getId() == openOption.getId()){
						investmentToRemove = openOption;
						break;
					}
				}
				if(investmentToRemove != null){
					openOptionsList.remove(investmentToRemove);
				}

				openOptionsList.remove(investment);
				OptionDynamicsItem optionDynamicsItem = null;
				if(openAboveGroup != null){
					//Search in the ABOVE options:
					for(OptionDynamicsItem item : openAboveGroup.getItemsList()){
						if(item.id == investment.getId()){
							optionDynamicsItem = item;
							optionDynamicsItem.isClosed = true;
							break;
						}
					}
				}
				if(openBellowGroup != null){
					//Search in the BELLOW options:
					for(OptionDynamicsItem item : openBellowGroup.getItemsList()){
						if(item.id == investment.getId()){
							optionDynamicsItem = item;
							optionDynamicsItem.isClosed = true;
							break;
						}
					}
				}

				if (optionDynamicsItem != null) {
					adapter.closeOptionCallback(true, optionDynamicsItem);

					investedAmount 			-= optionDynamicsItem.investAmount;
					if(optionDynamicsItem.investType == OptionDynamicsItem.InvestType.CALL){
						returnAboveAmount -= optionDynamicsItem.returnAmount;
					}else{
						returnBelowAmount -= optionDynamicsItem.returnAmount;
					}
					closeAllAmount 			-= optionDynamicsItem.closedForAmount;

					formatOpenOptionsAmountText();
					formatExpireAboveAmountText();
					formatExpireBellowAmountText();
					formatCloseAllButton();

					//Update the balance:
					User user = application.getUser();
					long currentBalance = user.getBalance();
					long newBalance = currentBalance + AmountUtil.getLongAmount(optionDynamicsItem.closedForAmount);
					user.setBalance(newBalance);
					user.setBalanceWF(AmountUtil.getFormattedAmount(newBalance, user.getCurrency()));
					application.postEvent(new UserUpdatedEvent());
					///////////////////////
				}
			}

			application.postEvent(new InvestmentEvent(market.getId(), InvestmentEvent.Type.INVESTMENT_SOLD, investment));
			startRow = 0;
			settledOptionsList.clear();
			getSettledInvestments();
		} else {
			Log.e(TAG, "Could not close option!");
			isClosingOption = false;
			ErrorUtils.handleResultError(investmentsResult);
		}
	}

	private void closeAllOptions(){
		boolean isOpen = opportunityState == Opportunity.STATE_OPENED;
		SellDynamicsInvestmentRequest request = new SellDynamicsInvestmentRequest();
		request.setOpportunityId(opportunityId);
		closeAllOptionsAmount = investedAmount;
		request.setOffer(profitCallParam);
		request.setBid(profitPutParam);

		if(!application.getCommunicationManager().requestService(this, "closedAllOptions", Constants.SERVICE_SELL_ALL_DYNAMICS_INVESTMENT, request, InvestmentMethodResult.class, false, false)){
			isClosingOption = false;
			formatCloseAllButton();

			if(openAboveGroup != null && !openAboveGroup.getItemsList().isEmpty()){
				for(OptionDynamicsItem optionDynamicsItem : openAboveGroup.getItemsList()){
					optionDynamicsItem.isCloseEnabled = isOpen && optionDynamicsItem.closeForAmount > 0 ? true : false;
				}
			}

			if(openBellowGroup != null && !openBellowGroup.getItemsList().isEmpty()){
				for(OptionDynamicsItem optionDynamicsItem : openBellowGroup.getItemsList()){
					optionDynamicsItem.isCloseEnabled = isOpen && optionDynamicsItem.closeForAmount > 0 ? true : false;
				}
			}

			adapter.notifyDataSetChanged();
		}
	}

	public void closedAllOptions(Object result) {
		InvestmentMethodResult investmentsResult = (InvestmentMethodResult) result;
		if (investmentsResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "All options were closed!");

			investedAmount 	  = 0;
			returnAboveAmount = 0;
			returnBelowAmount = 0;
			closeAllAmount	  = 0;

			formatAllOptionsClosedText();

			investDetailsLayout.setVisibility(View.GONE);
			closedAllTextView.setVisibility(View.VISIBLE);

			CountDownTimer timer = new CountDownTimer(OPTION_CLOSED_TIME, OPTION_CLOSED_TIME) {
				@Override
				public void onTick(long millisUntilFinished) {}
				@Override
				public void onFinish() {
					if(!isAdded()){
						return;
					}

					closedAllTextView.setVisibility(View.GONE);
					investDetailsLayout.setVisibility(View.VISIBLE);

					openOptionsList.clear();
					setupOptionsLists();

					isClosingOption = false;

					formatOpenOptionsAmountText();
					formatExpireAboveAmountText();
					formatExpireBellowAmountText();
					formatCloseAllButton();

				}
			}.start();

			//Update the balance:
			User user = application.getUser();
			long currentBalance = user.getBalance();
			long newBalance = currentBalance + AmountUtil.getLongAmount(closeAllOptionsAmount);
			user.setBalance(newBalance);
			user.setBalanceWF(AmountUtil.getFormattedAmount(newBalance, user.getCurrency()));
			application.postEvent(new UserUpdatedEvent());
			///////////////////////

			application.postEvent(new InvestmentEvent(market.getId(), InvestmentEvent.Type.INVESTMENT_SOLD_ALL));
			startRow = 0;
			settledOptionsList.clear();
			getSettledInvestments();
		} else {
			Log.e(TAG, "Could not close All options!");
			isClosingOption = false;
			boolean isOpen = opportunityState == Opportunity.STATE_OPENED;
			formatCloseAllButton();

			if(openAboveGroup != null && !openAboveGroup.getItemsList().isEmpty()){
				for(OptionDynamicsItem optionDynamicsItem : openAboveGroup.getItemsList()){
					optionDynamicsItem.isCloseEnabled = isOpen && optionDynamicsItem.closeForAmount > 0 ? true : false;
				}
			}

			if(openBellowGroup != null && !openBellowGroup.getItemsList().isEmpty()){
				for(OptionDynamicsItem optionDynamicsItem : openBellowGroup.getItemsList()){
					optionDynamicsItem.isCloseEnabled = isOpen && optionDynamicsItem.closeForAmount > 0 ? true : false;
				}
			}

			adapter.notifyDataSetChanged();

			ErrorUtils.handleResultError(investmentsResult);
		}
	}
	//////////////////////////////////////////////////////////////////////////////////


	//////////////////////////////////////////////////////////////////////////////////
	//                              EVENTs                                          //
	//////////////////////////////////////////////////////////////////////////////////

	public void onEventMainThread(LoginLogoutEvent event) {
		if(event.getType() == LoginLogoutEvent.Type.LOGOUT){
			startRow = 0;
			isLastSettledOptionsReached = false;

			if(openOptionsList != null){
				openOptionsList.clear();
			}
			if(settledOptionsList != null){
				settledOptionsList.clear();
			}

			setupOptionsLists();
			setupAllAmounts();
		}
	}

	public void onEventMainThread(ChartCommonEvent event) {
		if (event.getMarketId() != market.getId()) {
			return;
		}

		if (event.getType() == ChartCommonEvent.Type.DATA_REQUEST_SUCCESS) {
			ChartDataResult chartData = event.getChartData();
			opportunityState	= 0;
			opportunityId 		= chartData.getOpportunityId();
			opportunityTypeId 	= chartData.getOpportunityTypeId();
			Investment[] investmentsArr = chartData.getInvestments();
			openOptionsList.clear();
			if(investmentsArr != null){
				//Save the open options:
				for(Investment investment : investmentsArr){
					//Check if the Investment is for the current Market:
					if(investment.getMarketId() == market.getId()){
						openOptionsList.add(investment);
					}
				}
				Collections.reverse(openOptionsList);
			}

			isExpiredOptionsPopupShown = false;
			isOpenOptionsLoaded = true;
			setupLoading();
			setupOptionsLists();
			setupAllAmounts();
		}
	}

	public void onEvent(LightstreamerUpdateEvent event) {
		if (event.getMarketId() == market.getId()) {
			updateItem(event.getUpdateInfo());
		}
	}

	public void onEventMainThread(InvestmentEvent event) {
		if (event.getMarketId() != market.getId()) {
			return;
		}

		//Check the Invest Action Type:
		if (event.getType() == InvestmentEvent.Type.INVEST_SUCCESS) {
			showNoOptionsView(false);
			Investment investment = event.getInvestment();
			if(investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY || investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_SELL){
				OptionDynamicsItem optionDynamicsItem = new OptionDynamicsItem();
				optionDynamicsItem.id = investment.getId();
				optionDynamicsItem.price = investment.getPrice();
				optionDynamicsItem.isCloseEnabled 	= false;
				optionDynamicsItem.time 			= investment.getTimeCreated();
				optionDynamicsItem.investAmount 	= AmountUtil.getDoubleAmount(investment.getAmount());
				if(investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY){
					optionDynamicsItem.investType 		= OptionDynamicsItem.InvestType.CALL;
					optionDynamicsItem.profitPercent	= (investment.getPrice() == 0) ? 100 : 100 * (100 - investment.getPrice()) / investment.getPrice();
				}else{
					optionDynamicsItem.investType 		= OptionDynamicsItem.InvestType.PUT;
					optionDynamicsItem.profitPercent	= (investment.getPrice() == 100) ? 100 : 100 * investment.getPrice() / (100 - investment.getPrice());
				}
				optionDynamicsItem.returnAmount = optionDynamicsItem.investAmount * (1 + optionDynamicsItem.profitPercent / 100);

				if(investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY){
					//Check the ABOVE group. It should be added at first position:
					if(openAboveGroup == null || !adapter.getGroups().contains(openAboveGroup)){
						openAboveGroup = new OptionDynamicsGroup(OptionDynamicsGroup.Type.OPEN_ABOVE);
						adapter.getGroups().add(0, openAboveGroup);
					}
					openAboveGroup.getItemsList().add(0, optionDynamicsItem);
				}else{
					//Check the BELLOW group. It should be added after the ABOVE and before the SETTLED:
					if(openBellowGroup == null || !adapter.getGroups().contains(openBellowGroup)){
						openBellowGroup = new OptionDynamicsGroup(OptionDynamicsGroup.Type.OPEN_BELLOW);
						int index = openAboveGroup == null ? 0 : 1;
						adapter.getGroups().add(index, openBellowGroup);
					}
					openBellowGroup.getItemsList().add(0, optionDynamicsItem);
				}

				openOptionsList.add(investment);
				setupAllAmounts();
				adapter.notifyDataSetInvalidated();
				expandListGroups();
			}
		}
	}
	//////////////////////////////////////////////////////////////////////////////////


	//////////////////////////////////////////////////////////////////////////////////
	//                              LIGHTSTREAMER                                   //
	//////////////////////////////////////////////////////////////////////////////////

	public void updateItem(UpdateInfo update) {
		//Check if the State has changed:
		int newOpportunityState = Integer.valueOf(update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_STATE));
		if (opportunityState == 0 || (update.isValueChanged(LightstreamerManager.COLUMN_DYNAMICS_STATE) && opportunityState < newOpportunityState)) {
			opportunityState = Integer.valueOf(update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_STATE));
			handler.obtainMessage(MESSAGE_STATE_CHANGED, update).sendToTarget();
		}
		//Check if the Market is Available:
		if(opportunityState != Opportunity.STATE_CREATED && opportunityState != Opportunity.STATE_SUSPENDED) {
			handler.obtainMessage(MESSAGE_UPDATE_OPTIONS, update).sendToTarget();
		}
	}

	//////////////////////////////////////////////////////////////////////////////////

	@Override
	public boolean handleMessage(Message msg) {
		switch (msg.what) {
			case MESSAGE_STATE_CHANGED:
				UpdateInfo update = (UpdateInfo) msg.obj;
				boolean isCloseOptionsEnabled 	= false;
				boolean isShowAllOptions 		= false;

				//Open:
				if (opportunityState == Opportunity.STATE_OPENED || opportunityState == Opportunity.STATE_LAST_10_MIN) {
					isCloseOptionsEnabled 	= true;
					isShowAllOptions 		= true;
				}

				//Waiting for Expiry:
				else if(opportunityState == Opportunity.STATE_CLOSING_1_MIN || opportunityState == Opportunity.STATE_CLOSING){
					isCloseOptionsEnabled 	= false;
					isShowAllOptions 		= true;
				}

				//Expired:
				else if(opportunityState == Opportunity.STATE_CLOSED){

					isCloseOptionsEnabled 	= false;
					isShowAllOptions 		= true;

					boolean isOpenoptions = openOptionsList != null && !openOptionsList.isEmpty();
					if(isOpenoptions && !isExpiredOptionsPopupShown){
						isExpiredOptionsPopupShown = true;
						//Show the Expired Options Popup:
						updateOptions(update);
						double centralLevel = Double.valueOf(update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_EVENT_LEVEL).replaceAll(",", ""));
						double currentLevel = Double.valueOf(update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_LEVEL).replaceAll(",", ""));
						long timeClosed		= 0;
						try {
							timeClosed		= TimeUtils.parseTimeFromLSUpdate(update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_TIME_CLOSE)).getTime();
						} catch (ParseException e) {
							e.printStackTrace();
						}
						boolean isAbove 			= currentLevel > centralLevel;
						double investedAmount   	= this.investedAmount;
						double returnAmount 		= ((isAbove) ? returnAboveAmount : returnBelowAmount);
						boolean isWinning   		= returnAmount > investedAmount;

						showExpiredOptionsPopup(isWinning, isAbove, market.getDisplayName(), centralLevel, currentLevel, timeClosed, investedAmount, returnAmount);

					}
				}

				//Unavailable:
				else if(opportunityState == Opportunity.STATE_CREATED || opportunityState == Opportunity.STATE_SUSPENDED) {
					isCloseOptionsEnabled 	= false;
					isShowAllOptions 		= false;
				}

				//Update All Open ABOVE options:
				if(openAboveGroup != null && !openAboveGroup.getItemsList().isEmpty()){
					for(OptionDynamicsItem optionDynamicsItem : openAboveGroup.getItemsList()){
						if(isCloseOptionsEnabled && !isClosingOption && optionDynamicsItem.closeForAmount > 0){
							optionDynamicsItem.isCloseEnabled = true;
						}else{
							optionDynamicsItem.isCloseEnabled = false;
						}
					}
				}

				//Update All Open BELLOW options:
				if(openBellowGroup != null && !openBellowGroup.getItemsList().isEmpty()){
					for(OptionDynamicsItem optionDynamicsItem : openBellowGroup.getItemsList()){
						if(isCloseOptionsEnabled && !isClosingOption && optionDynamicsItem.closeForAmount > 0){
							optionDynamicsItem.isCloseEnabled = true;
						}else{
							optionDynamicsItem.isCloseEnabled = false;
						}
					}
				}

				adapter.notifyDataSetChanged();
				formatCloseAllButton();
				allOptionsLayout.setVisibility(isShowAllOptions ? View.VISIBLE : View.GONE);

				break;
			case MESSAGE_UPDATE_OPTIONS:
				UpdateInfo u = (UpdateInfo) msg.obj;
				updateOptions(u);
				break;
		}

		return false;
	}

	private void updateOptions(UpdateInfo update){

		//Check if there are any changes:
		boolean isOppurtunityOpen  = opportunityState == Opportunity.STATE_OPENED;
		boolean isCallParamChanged = update.isValueChanged(LightstreamerManager.COLUMN_DYNAMICS_PROFIT_CALL);
		boolean isPutParamChanged  = update.isValueChanged(LightstreamerManager.COLUMN_DYNAMICS_PROFIT_PUT);
		if(!isClosingOption && isOppurtunityOpen && (isCallParamChanged || isPutParamChanged)) {

			profitCallParam = Double.valueOf(update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_PROFIT_CALL));
			profitPutParam 	= Double.valueOf(update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_PROFIT_PUT));

			//Update All Open ABOVE options:
			if (openAboveGroup != null && !openAboveGroup.getItemsList().isEmpty()) {
				for (OptionDynamicsItem optionDynamicsItem : openAboveGroup.getItemsList()) {
					double contracts;
					double price = optionDynamicsItem.price;
					if(price <= 0){
						contracts = optionDynamicsItem.investAmount;
					}else if(price > 100){
						contracts = 0;
					}else{
						contracts = AmountUtil.roundToFourthDigitFormat(optionDynamicsItem.investAmount / price);
					}
					optionDynamicsItem.closeForAmount 		= AmountUtil.roundToFourthDigitFormat(contracts * profitPutParam);
					optionDynamicsItem.isInTheMoneyForClose = (optionDynamicsItem.closeForAmount > optionDynamicsItem.investAmount) ? true : false;
					if(!isClosingOption && optionDynamicsItem.closeForAmount > 0){
						optionDynamicsItem.isCloseEnabled = true;
					}else{
						optionDynamicsItem.isCloseEnabled = false;
					}
					optionDynamicsItem.isAvailable = true;
				}
			}

			//Update All Open BELLOW options:
			if (openBellowGroup != null && !openBellowGroup.getItemsList().isEmpty()) {
				for (OptionDynamicsItem optionDynamicsItem : openBellowGroup.getItemsList()) {
					double contracts;
					double price = optionDynamicsItem.price;
					if(price >= 100){
						contracts = optionDynamicsItem.investAmount;
					}else if(price <= 0){
						contracts = 0;
					}else{
						contracts = AmountUtil.roundToFourthDigitFormat(optionDynamicsItem.investAmount / (100 - price));
					}
					optionDynamicsItem.closeForAmount 		= AmountUtil.roundToFourthDigitFormat(contracts * (100 - profitCallParam));
					optionDynamicsItem.isInTheMoneyForClose = (optionDynamicsItem.closeForAmount > optionDynamicsItem.investAmount) ? true : false;
					if(!isClosingOption && optionDynamicsItem.closeForAmount > 0){
						optionDynamicsItem.isCloseEnabled = true;
					}else{
						optionDynamicsItem.isCloseEnabled = false;
					}
					optionDynamicsItem.isAvailable = true;
				}
			}

			adapter.notifyDataSetChanged();

			//Update the ALL Amounts:
			setupAllAmounts();
		}

		if(!isClosingOption && !isOppurtunityOpen){
			//Update All Open ABOVE options:
			if (openAboveGroup != null && !openAboveGroup.getItemsList().isEmpty()) {
				for (OptionDynamicsItem optionDynamicsItem : openAboveGroup.getItemsList()) {
					optionDynamicsItem.isAvailable = false;
				}
			}

			//Update All Open BELLOW options:
			if (openBellowGroup != null && !openBellowGroup.getItemsList().isEmpty()) {
				for (OptionDynamicsItem optionDynamicsItem : openBellowGroup.getItemsList()) {
					optionDynamicsItem.isAvailable = false;
				}
			}
		}
	}

	private void showExpiredOptionsPopup(boolean isWinning, boolean isAbove, String marketName, double centralLevel, double expiryLevel, long timeExpired, double investedAmount, double returnAmount){
		Bundle bundle = new Bundle();
		bundle.putBoolean(Constants.EXTRA_IS_IN_THE_MONEY, isWinning);
		bundle.putBoolean(Constants.EXTRA_IS_EXPIRED_ABOVE, isAbove);
		bundle.putString(Constants.EXTRA_MARKET_NAME, marketName);
		bundle.putDouble(Constants.EXTRA_CENTRAL_LEVEL, centralLevel);
		bundle.putDouble(Constants.EXTRA_EXPIRY_LEVEL, expiryLevel);
		bundle.putLong(Constants.EXTRA_TIME, timeExpired);
		bundle.putDouble(Constants.EXTRA_INVESTED_AMOUNT, investedAmount);
		bundle.putDouble(Constants.EXTRA_RETURN_AMOUNT, returnAmount);

		if(expiredOptionsPopup == null){
			application.getFragmentManager().showDialogFragment(ExpiredDynamicsOptionDialogFragment.class, bundle);
		}else{
			expiredOptionsPopup.setArguments(bundle);
			application.getFragmentManager().showDialogFragment(expiredOptionsPopup);
		}

	}

	@Override
	public void onCloseOptionButtonClicked(OptionDynamicsItem item) {
		if(!isAdded()){
			return;
		}
		closeOption(item);
	}

	@Override
	public void onCloseOptionClosed() {
		if(!isAdded()){
			return;
		}
		isClosingOption = false;
		setupOptionsLists();
		setupAllAmounts();
	}
}
