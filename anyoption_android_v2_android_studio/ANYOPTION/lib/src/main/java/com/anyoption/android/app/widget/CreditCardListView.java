package com.anyoption.android.app.widget;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.CreditCard;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.json.results.CreditCardsMethodResult;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;


/**
 * Custom View that represents the "credit cards list" in the withdraw/deposit menu.
 * @author Eyal Goren
 *
 */
public class CreditCardListView extends LinearLayout implements OnClickListener {

	public static final String TAG = CreditCardListView.class.getSimpleName();
	
	private static final int DEPOSIT = 1;
	private static final int WITHDRAW = 2;
	
	private CcListType listType;
	private CreditCard[] ccList;
	private EmptyListListener emptyListListener;
	private boolean isCallBackRecived;
	private boolean isEmpty;
	
	public enum CcListType {
		DEPOSIT(1, Constants.SERVICE_GET_AVAILABLE_CREDIT_CARDS_WITH_EMPTY_LINE, 1),
		WITHDRAW(2, Constants.SERVICE_GET_WITHDRAWAL_CC_LIST, 0)
		;
		private int type;
		private String serverMethodName;
		private int listMinLength;
		
		CcListType(int type, String serverMethodName, int listMinLength){
			this.type = type;
			this.serverMethodName = serverMethodName;
			this.listMinLength = listMinLength;
		}

		public int getType() {
			return type;
		}

		public String getServerMethodName() {
			return serverMethodName;
		}

		public int getListMinLength() {
			return listMinLength;
		}
		
	}
	
	@Override
	protected void onVisibilityChanged(View changedView, int visibility) {
		super.onVisibilityChanged(changedView, visibility);
		if (visibility == View.VISIBLE) {
			removeAllViews();
			updateList();
		}
	}
	
	/**
	 * Custom Listener that listen for Empty list event.
	 * @author Eyal Goren
	 *
	 */
	public interface EmptyListListener {
		/**
		 * Called when the list from server return empty
		 */
		public void onEmptyList();
	}
	
	public CreditCardListView(Context context) {
		super(context);
		setup(context, null);
	}
	
	public CreditCardListView(Context context, AttributeSet set) {
		super(context, set);
		setup(context, set);
	}
	
	/**
	 * Setup the layout.
	 * @param context
	 * @param set
	 */
	private void setup(Context context, AttributeSet set) {
		TypedArray attributesTypedArray = context.obtainStyledAttributes(set, R.styleable.CreditCardListView);
		int type = attributesTypedArray.getInt(R.styleable.CreditCardListView_listType, DEPOSIT);
		attributesTypedArray.recycle();
		
		if (type == DEPOSIT) {
			listType = CcListType.DEPOSIT;
		} else { //withdraw
			listType = CcListType.WITHDRAW;
		}
		
		if(!isInEditMode()){
			updateList();			
		}else{
			TextView textView = new TextView(getContext());
			textView.setGravity(Gravity.CENTER);
			textView.setText("Cards List for " + listType);
			addView(textView, new LayoutParams(LayoutParams.MATCH_PARENT, 50));
		}
		
	}
	
	public void updateList() {
		Application.get().getCommunicationManager().requestService(CreditCardListView.this, "ccListCallBack", listType.serverMethodName,  new UserMethodRequest() , CreditCardsMethodResult.class);
	}

	/**
	 * get user countryId from the service and set it up.
	 * Change screen if it's unregulated country.
	 * @param result
	 */
	public void ccListCallBack(Object result) {
    	Log.d(TAG, "ccListCallBack");
    	CreditCardsMethodResult creditCardsMethodResult = (CreditCardsMethodResult) result;
    	if (creditCardsMethodResult != null && creditCardsMethodResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
    		Log.d(TAG, "Getting Credit Cards form the server.");
	    	ccList = creditCardsMethodResult.getOptions();
	    	removeAllViews();
	    	if (null != ccList && ccList.length > listType.listMinLength) {
	    		Log.d(TAG, "Credit Cards count = " + ccList.length);
		    	LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		    	for (CreditCard cc : ccList) {
		    		if (null != cc && cc.getCcNumber() > 0) {
		    			Drawable imgCard = DrawableUtils.getImageByCCType((int) (cc.getTypeIdByBin() == 0 ? cc.getTypeId() : cc.getTypeIdByBin()));
			    		LinearLayout row = (LinearLayout) inflater.inflate(R.layout.credit_card_list_row, this, false);
			    		if (null != imgCard) {
			    			((ImageView)row.findViewById(R.id.cc_list_row_icon)).setImageDrawable(imgCard);
			    		}
			    		((TextView)row.findViewById(R.id.cc_list_row_text)).setText(cc.getTypeName() + " (" + Utils.getCCHiddenNumberShort(String.valueOf(cc.getCcNumber())) + ")");
			    		row.setTag(cc);
			    		row.setOnClickListener(this);
			    		addView(row);
		    		}
				}
	    	} else {
	    		Log.d(TAG, "NO Credit Cards.");
	    		isEmpty = true;
	    		callOnEmptyList();
	    	}
    	}else{
    		Log.e(TAG, "Could NOT get the Credit Cards form the server.");
    	}
    	isCallBackRecived = true;
    }
	
	private void callOnEmptyList() {
		if (null != emptyListListener) {
			emptyListListener.onEmptyList();
		}
	}

	@Override
	public void onClick(View v) {
		CreditCard cc = (CreditCard) v.getTag();
		Bundle b = new Bundle();
		b.putSerializable(Constants.EXTRA_CREDIT_CARD, cc);
		NavigationEvent navigationEvent = new NavigationEvent(Screen.CC_WITHDRAW, NavigationType.DEEP, b);
		if (listType.type == DEPOSIT) {
			//TODO deposit with card
			navigationEvent.setToScreen(Screen.EXISTING_CARD);
		}
		Application.get().postEvent(navigationEvent);
	}

	public EmptyListListener getEmptyListListener() {
		return emptyListListener;
	}

	public void setEmptyListListener(EmptyListListener emptyListListener) {
		this.emptyListListener = emptyListListener;
	}

	public boolean isEmpty() {
		return isEmpty;
	}

	public boolean isCallBackRecived() {
		return isCallBackRecived;
	}
	
}
