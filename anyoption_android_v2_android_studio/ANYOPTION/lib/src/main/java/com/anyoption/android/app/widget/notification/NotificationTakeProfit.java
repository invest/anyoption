package com.anyoption.android.app.widget.notification;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.popup.InsuranceDialogFragment;
import com.anyoption.common.beans.base.Investment;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NotificationTakeProfit extends LinearLayout{

	private String closeTime;
	
	public NotificationTakeProfit(String closeTime) {
		super(Application.get());
		this.closeTime = closeTime;
		init(Application.get(), null, 0);
	}	
	public NotificationTakeProfit(Context context) {
		super(context);
		init(context, null, 0);
	}
	public NotificationTakeProfit(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	public NotificationTakeProfit(Context context, AttributeSet attrs,int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		View view = View.inflate(context, R.layout.notification_take_profit_layout, this);
		
		TextView closeTimeTextView = (TextView) view.findViewById(R.id.notification_close_time);
		if (closeTime != null && !closeTime.isEmpty()) {
			closeTimeTextView.setVisibility(View.VISIBLE);
			closeTimeTextView.setText(closeTime);
		}else{
			closeTimeTextView.setVisibility(View.GONE);
		}
		
		OnClickListener clickListener = new OnClickListener() {
			private boolean isPressed = false;
			@Override
			public void onClick(View v) {
				if(!isPressed){
					isPressed = true;

					Application.get().postEvent(new NavigationEvent(Screen.MY_OPTIONS, NavigationType.INIT));

					Bundle arguments = new Bundle();
					arguments.putInt(InsuranceDialogFragment.ARG_INSURANCE_TYPE_KEY,
							Investment.INSURANCE_TAKE_PROFIT);
					Application.get().getFragmentManager().showDialogFragment(
							InsuranceDialogFragment.class, arguments);
				}
			}
		};
		view.setOnClickListener(clickListener);
		view.findViewById(R.id.notification_take_profit_text_layout).setOnClickListener(clickListener);
		view.findViewById(R.id.notification_message_text_view).setOnClickListener(clickListener);
		closeTimeTextView.setOnClickListener(clickListener);
	}

}
