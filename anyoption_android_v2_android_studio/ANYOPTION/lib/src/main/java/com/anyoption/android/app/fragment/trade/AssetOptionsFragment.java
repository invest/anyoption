package com.anyoption.android.app.fragment.trade;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.anyoption.android.R;
import com.anyoption.android.app.event.ChartEvent;
import com.anyoption.android.app.event.InvestmentEvent;
import com.anyoption.android.app.event.InvestmentEvent.Type;
import com.anyoption.android.app.event.LightstreamerUpdateEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.OptionsDrawerEvent;
import com.anyoption.android.app.event.OptionsEvent;
import com.anyoption.android.app.manager.CommunicationManager;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.InvestmentUtil;
import com.anyoption.android.app.util.MarketsUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.SlidingDrawer;
import com.anyoption.android.app.widget.SlidingDrawer.OnDrawerCloseListener;
import com.anyoption.android.app.widget.SlidingDrawer.OnDrawerOpenListener;
import com.anyoption.android.app.util.adapter.AssetOptionsListAdapter;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.service.requests.InvestmentsMethodRequest;
import com.anyoption.common.service.results.InvestmentsMethodResult;
import com.anyoption.json.results.ChartDataMethodResult;
import com.lightstreamer.ls_client.UpdateInfo;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class AssetOptionsFragment extends BaseAssetNestedFragment implements Callback {

	@SuppressWarnings("hiding")
	private static final String TAG = AssetOptionsFragment.class.getSimpleName();

	private static final int SETTLED_OPTIONS_COUNT = 10;

	private static final int MESSAGE_UPDATE_LIST = 1;

	private static final int SETTLED_PAGE_SIZE = 6;
	private static final int PERIOD_MONTH = 3;

	private CommunicationManager communicationManager;
	private View root;
	private SlidingDrawer drawer;
	private View handle;
	private ImageView handleImageView;
	private TextView totalOptionsView;
	private ExpandableListView optionsListView;
	protected AssetOptionsListAdapter optionsListAdapter;
	private boolean isTablet;
	private SkinGroup skinGroup;
	private Currency currency;
	private boolean areOpenOptionsLoading = false;
	private Handler lsHandler;
	private View footerView;
	private int startRow;
	private View loadingView;
	private boolean isSettledOptionsLoaded;
	private boolean isLastSettledOptionsReached;

	/**
	 * Use this method to create new instance of the fragment.
	 *
	 * @return
	 */
	public static AssetOptionsFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new AssetOptionsFragment.");
		AssetOptionsFragment fragment = new AssetOptionsFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	/**
	 * Create new instance of this Fragment
	 *
	 * @param market
	 * @param autoLoad
	 *            if true, the fragment will start loading the fragment immediately. Otherwise it
	 *            will load the data when {@link #loadData()} method is called.
	 * @return
	 */
	public static AssetOptionsFragment newInstance(Market market, boolean autoLoad) {
		Bundle args = new Bundle();
		args.putSerializable(Constants.EXTRA_MARKET, market);
		args.putBoolean(Constants.EXTRA_AUTO_LOAD_MARKET, autoLoad);
		return newInstance(args);
	}

	/**
	 * Create new instance of this Fragment
	 *
	 * @param market
	 * @return
	 */
	public static AssetOptionsFragment newInstance(Market market) {
		return newInstance(market, true);
	}

	public AssetOptionsFragment() {
		// Empty constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Skin skin = application.getSkins().get(application.getSkinId());
		skinGroup = skin.getSkinGroup();
		isTablet = getResources().getBoolean(R.bool.isTablet);

		currency = application.getCurrency();

		communicationManager = application.getCommunicationManager();
		lsHandler = new Handler(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		root = inflater.inflate(R.layout.asset_options_fragment, container, false);

		totalOptionsView = (TextView) root.findViewById(R.id.asset_options_total_options);
		totalOptionsView.setText(getString(R.string.totalOptions) + ": 0");

		loadingView = root.findViewById(R.id.loadingView);

		footerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.invest_settled_options_listview_footer_layout, null, false);

		if (!isTablet) {
			handle = root.findViewById(R.id.asset_options_handle);
			handleImageView = (ImageView) root.findViewById(R.id.asset_options_handle_image);

			// Set up sliding drawer
			drawer = (SlidingDrawer) root.findViewById(R.id.asset_options_drawer);
			drawer.setHandleAboveContent(true);
			drawer.setOnDrawerOpenListener(new OnDrawerOpenListener() {

				@Override
				public void onDrawerOpened() {
					handle.setRotation(180f);
				}
			});

			drawer.setOnDrawerCloseListener(new OnDrawerCloseListener() {

				@Override
				public void onDrawerClosed() {
					handle.setRotation(0f);
				}
			});
		}

		optionsListView = (ExpandableListView) root.findViewById(R.id.asset_options_content_list);

		setUpList();

		setupLoading();

		return root;
	}

	@Override
	public void onResume() {
		super.onResume();
		expandListGroups();

		registerForEvents();
	}

	@Override
	public void onPause() {
		application.unregisterForAllEvents(this);
		super.onPause();
	}

	@Override
	public void onDestroyView() {
		application.unregisterForAllEvents(this);
		super.onDestroyView();
	}

	protected void setupLoading(){
		if(loadingView != null){
			if(!isSettledOptionsLoaded){
				loadingView.setVisibility(View.VISIBLE);
			}else{
				loadingView.setVisibility(View.GONE);
			}
		}
	}

	private void setUpList() {
		optionsListView.setOnChildClickListener(new OnChildClickListener() {

			@Override
			public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
					int childPosition, long id) {
				if (optionsListAdapter.getGroupId(groupPosition) == AssetOptionsListAdapter.OPEN_OPTIONS_GROUP_ID) {
					List<Investment> openOptions = optionsListAdapter.getOpenOptions();
					application.postEvent(new InvestmentEvent(market.getId(),
							Type.INVESTMENT_CHANGE_FOCUS, openOptions.get(childPosition)));
					return true;
				}

				return false;
			}
		});
		optionsListView.setOnScrollListener(new AbsListView.OnScrollListener() {

			private int currentLastVisibleItem;
			private int currentVisibleItemCount;
			private int currentScrollState;

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				this.currentScrollState = scrollState;
				isScrollCompleted();
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,int totalItemCount) {
				this.currentLastVisibleItem = firstVisibleItem + visibleItemCount;
				this.currentVisibleItemCount = visibleItemCount;
			}

			private void isScrollCompleted() {
				if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
					// Detect if there's been a scroll which has completed.
					// And check if the list is scrolled to the bottom.
					if (currentLastVisibleItem >= optionsListAdapter.getSettledOptions().size()) {
						if(!isLastSettledOptionsReached){
							Log.d(TAG, "List is scrolled to the bottom. Request more investments!");
							requestInvestments(true);
						}
					}
				}
			}

		});

		setUpListAdapter();
		optionsListView.setAdapter(optionsListAdapter);

		// Keeps all groups open.
		optionsListView.setOnGroupClickListener(new OnGroupClickListener() {

			@Override
			public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition,
					long id) {
				optionsListView.expandGroup(groupPosition);
				return true;
			}
		});
	}

	protected void setUpListAdapter() {
		optionsListAdapter = new AssetOptionsListAdapter(market);
	}

	private void registerForEvents() {
		if (market != null) {
			application.registerForEvents(this, ChartEvent.class, LightstreamerUpdateEvent.class,
					InvestmentEvent.class, LoginLogoutEvent.class);
			if (!isTablet) {
				application.registerForEvents(this, OptionsDrawerEvent.class);
			}
		} else {
			application.registerForEvents(this, LoginLogoutEvent.class);
		}
	}

	/**
	 * Called when a {@link LoginLogoutEvent} occurs.
	 *
	 * @param event
	 */
	public void onEventMainThread(LoginLogoutEvent event) {
		if (event.getType() == com.anyoption.android.app.event.LoginLogoutEvent.Type.LOGIN) {
			// Update the currency
			currency = application.getCurrency();
			optionsListAdapter.setCurrency(currency);
		}
	}

	/**
	 * Called when a {@link LightstreamerUpdateEvent} occurs.
	 *
	 * @param event
	 */
	public void onEvent(LightstreamerUpdateEvent event) {
		if (event.getMarketId() == market.getId()) {
			updateItem(event.getUpdateInfo());
		}
	}

	/**
	 * Called when a {@link InvestmentEvent} occurs.
	 *
	 * @param event
	 */
	public void onEventMainThread(InvestmentEvent event) {
		if(event.getMarketId() != market.getId()) {
			return;
		}

		if (event.getType() == Type.INVEST_SUCCESS) {
			Investment invesment = event.getInvestment();
			String returnFormatted = AmountUtil.getFormattedAmount(
					InvestmentUtil.getOpenInvestmentReturn(invesment,
							optionsListAdapter.getCurrentLevel()), currency);
			invesment.setAmountReturnWF(returnFormatted);

			optionsListAdapter.addInvestment(invesment);
			expandListGroups();
		} else if(event.getType() == Type.INVESTMENT_SOLD) {
			optionsListAdapter.removeInvestment(event.getInvestment());
			expandListGroups();

			// Updates the settled investments to have the sold investment in the list.
			startRow = 0;
			isLastSettledOptionsReached = false;
			optionsListAdapter.getSettledOptions().clear();
			requestInvestments(true);
		}
	}

	/**
	 * Called when a {@link ChartEvent} occurs.
	 *
	 * @param event
	 */
	public void onEventMainThread(ChartEvent event) {
		if (event.getMarketId() != market.getId()) {
			return;
		}

		if (event.getType() == ChartEvent.Type.DATA_REQUEST_SUCCESS) {
			ChartDataMethodResult chartData = event.getChartData();
			Investment[] investmentsArr = chartData.getInvestments();
			ArrayList<Investment> investments = optionsListAdapter.getOpenOptions();
			investments.clear();
			if (investmentsArr != null) {
				//Check if the Investments are for the current Market:
				for(int i=0; i< investmentsArr.length; i++){
					Investment investment = investmentsArr[i];
					if(investment.getMarketId() == market.getId()){
						investments.add(investment);
					}
				}
				Collections.reverse(investments);
			}
			optionsListAdapter.setDecimalPointDigits(chartData.getDecimalPoint());
			optionsListAdapter.setOpenOptions(investments);
			optionsListAdapter.notifyDataSetChanged();

			expandListGroups();
		}
	}

	/**
	 * Called when a {@link OptionsDrawerEvent} occurs.
	 *
	 * @param event
	 */
	public void onEventMainThread(OptionsDrawerEvent event) {
		if (event.getMarketId() == market.getId()) {
			if (event.getType() == OptionsDrawerEvent.Type.OPEN_DRAWER_REQUEST) {
				drawer.animateOpen();
			} else if (event.getType() == OptionsDrawerEvent.Type.CLOSE_DRAWER_REQUEST) {
				drawer.animateClose();
			}
		}
	}

	public void updateItem(UpdateInfo update) {
		if (areOpenOptionsLoading) {
			return;
		}

		boolean needsUpdate = false;

		if (update.isValueChanged(skinGroup.getLevelUpdateKey())) {
			String levelStr = update.getNewValue(skinGroup.getLevelUpdateKey());
			double currentLevel = Double.parseDouble(levelStr.replaceAll(",", ""));
			optionsListAdapter.setCurrentLevel(currentLevel);

			List<Investment> openOptions = optionsListAdapter.getOpenOptions();
			if (!openOptions.isEmpty()) {
				needsUpdate = true;

				long oppId = Long.parseLong(update
						.getNewValue(LightstreamerManager.COLUMN_ET_OPP_ID));

				// Update all open options
				for (int i = 0; i < openOptions.size(); i++) {
					if (openOptions.get(i).getOpportunityId() == oppId) {
						String returnFormatted = AmountUtil
								.getFormattedAmount(
                                        InvestmentUtil.getOpenInvestmentReturn(openOptions.get(i),
                                                currentLevel),
                                        currency);
						openOptions.get(i).setAmountReturnWF(returnFormatted);
					}
				}

			}
		}

		if (optionsListAdapter.getOpportunityState() == 0 || update.isValueChanged(LightstreamerManager.COLUMN_ET_STATE)) {
			needsUpdate = true;
			optionsListAdapter.setOpportunityState(Integer.parseInt(update.getNewValue(LightstreamerManager.COLUMN_ET_STATE)));
		}

		if (needsUpdate && !lsHandler.hasMessages(MESSAGE_UPDATE_LIST)) {
			lsHandler.obtainMessage(MESSAGE_UPDATE_LIST).sendToTarget();
		}
	}

	@Override
	public boolean handleMessage(Message msg) {
		if (msg.what == MESSAGE_UPDATE_LIST) {
			updateList();
			return true;
		}

		return false;
	}

	private void updateList() {
		optionsListAdapter.notifyDataSetChanged();
	}

	private void requestInvestments(boolean areSettled) {
		InvestmentsMethodRequest request = new InvestmentsMethodRequest();
		request.setSettled(areSettled);
		request.setMarketId(market.getId());

		if (areSettled) {
			isSettledOptionsLoaded = false;
			setupLoading();
			request.setPeriod(PERIOD_MONTH);
			request.setStartRow(startRow);
			request.setPageSize(SETTLED_PAGE_SIZE);
			communicationManager.requestService(this, "settledOptionsCallBack",Constants.SERVICE_GET_INVESTMENTS, request, InvestmentsMethodResult.class, false, false);
		} else {
			areOpenOptionsLoading = true;
			communicationManager.requestService(this, "openOptionsCallBack", Constants.SERVICE_GET_INVESTMENTS,request, InvestmentsMethodResult.class, false, false);
		}
	}

	public void openOptionsCallBack(Object result) {
		if (!isAdded()) {
			return;
		}

		areOpenOptionsLoading = false;
		InvestmentsMethodResult investmentsResult = (InvestmentsMethodResult) result;

		if (investmentsResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Just got open options: " + investmentsResult.getInvestments().length);
			ArrayList<Investment> investments = optionsListAdapter.getOpenOptions();
			investments.clear();
			Collections.addAll(investments, investmentsResult.getInvestments());
			optionsListAdapter.setOpenOptions(investments);
			optionsListAdapter.notifyDataSetChanged();

			expandListGroups();

			optionsListView.removeFooterView(footerView);

			if (investmentsResult.getUser() != null) {
				application.setUser(investmentsResult.getUser());
			}
		} else {
			Log.e(TAG, "Could not get open options! :)");
		}
	}

	public void settledOptionsCallBack(Object result) {
		if (!isAdded()) {
			return;
		}
		isSettledOptionsLoaded = true;
		setupLoading();
		InvestmentsMethodResult investmentsResult = (InvestmentsMethodResult) result;
		if (investmentsResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			ArrayList<Investment> investments = optionsListAdapter.getSettledOptions();
			InvestmentUtil.fixDate(investmentsResult.getInvestments());
			Collections.addAll(investments, investmentsResult.getInvestments());
			optionsListAdapter.setSettledOptions(investments);
			optionsListAdapter.notifyDataSetChanged();

			startRow += SETTLED_PAGE_SIZE;
			if(investmentsResult.getInvestments().length < SETTLED_PAGE_SIZE){
				isLastSettledOptionsReached = true;
			}

			totalOptionsView.setText(getString(R.string.totalOptions) + ": " + investments.size());
			expandListGroups();

			optionsListView.removeFooterView(footerView);
			if(investments.size() > 0) {
                optionsListView.addFooterView(footerView);
            }
			application.postEvent(new OptionsEvent(market.getId(),
					OptionsEvent.Type.SETTLED_OPTION_RECEIVED, investmentsResult));
			Log.d(TAG, "Just got settled options: " + investmentsResult.getInvestments().length);
		} else {
			Log.e(TAG, "Could not get settled options! :)");
		}
	}

	protected void expandListGroups() {
		for (int i = 0; i < optionsListAdapter.getGroupCount(); i++) {
			optionsListView.expandGroup(i);
		}
	}

	@Override
	public void loadData() {
		Log.d(TAG, "Load() market = " + market);
		communicationManager.cancelAllRequests(this);

		if (application.isLoggedIn() && market != null) {
			// Request settled options
			startRow = 0;
			isLastSettledOptionsReached = false;
			optionsListAdapter.getSettledOptions().clear();
			requestInvestments(true);
		} else {
			clearOptions();
		}
	}

	@Override
	protected void onMarketChanged() {
		super.onMarketChanged();

		if(isTablet){
			View bidAskView = root.findViewById(R.id.investBidAskTextView);
			if(market != null && MarketsUtils.isBinary(market)){
				bidAskView.setVisibility(View.VISIBLE);
			}else{
				bidAskView.setVisibility(View.GONE);
			}
		}

		registerForEvents();

		optionsListAdapter.setMarket(market);
		clearOptions();

		if (isAutoLoad()) {
			loadData();
		}
	}

	public boolean isDrawerOpened() {
		if (isTablet) {
			return true;
		}

		return drawer.isOpened();
	}

	public void openDrawer() {
		if (!isTablet) {
			drawer.open();
		}
	}

	public void closeDrawer() {
		if (!isTablet) {
			drawer.close();
		}
	}

	/**
	 * Changes the visibility of the handle.
	 *
	 * @param visibility One of {@link View#VISIBLE}, {@link View#INVISIBLE}, {@link View#GONE}.
	 */
	public void setHandleVisibility(int visibility) {
		if (!isTablet) {
			handleImageView.setVisibility(visibility);
		}
	}

	/**
	 * Changes the visibility of the total option view.
	 *
	 * @param visibility One of {@link View#VISIBLE}, {@link View#INVISIBLE}, {@link View#GONE}.
	 */
	public void setTotalOptionsVisibility(int visibility) {
		totalOptionsView.setVisibility(visibility);
	}

	public void clearOptions() {
		ArrayList<Investment> openOptions = optionsListAdapter.getOpenOptions();
		openOptions.clear();
		optionsListAdapter.setOpenOptions(openOptions);

		ArrayList<Investment> settledOptions = optionsListAdapter.getSettledOptions();
		settledOptions.clear();
		optionsListAdapter.setSettledOptions(settledOptions);
	}

}
