package com.anyoption.android.app.fragment.login_register;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.RequestButton.InitAnimationListener;
import com.anyoption.android.app.widget.form.Form;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.beans.base.Register;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.UserFPResult;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.json.requests.InsertUserMethodRequest;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 * <p>It manages the "Forgot Password" screen and functionality.
 *
 * @author Anastas Arnaudov
 */
public class ForgotPasswordFragment extends NavigationalFragment {

    private FormEditText emailEditText;

    private RequestButton emailMeButton;
    private RequestButton textMeButton;
    private RequestButton submitButton;

    private View emailFieldLayout;
    private View emailAndPhoneFieldLayout;

    private TextView emailText;
    private TextView phoneText;
    private TextView headerText;

    protected TextView emailField;
    protected TextView phoneField;

    private boolean isSmsSubmit;
    private Form form;
    private View view;

    /**
     * Use this method to obtain new instance of the fragment.
     *
     * @return
     */
    public static ForgotPasswordFragment newInstance() {
        return ForgotPasswordFragment.newInstance(new Bundle());
    }

    /**
     * Use this method to obtain new instance of the fragment.
     *
     * @return
     */
    public static ForgotPasswordFragment newInstance(Bundle bundle) {
        ForgotPasswordFragment forgotPasswordFragment = new ForgotPasswordFragment();
        forgotPasswordFragment.setArguments(bundle);
        return forgotPasswordFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.forgot_password_layout, container, false);

        form = new Form(null);

        emailEditText = (FormEditText) view.findViewById(R.id.forgot_pass_email_edittext);
        emailEditText.addValidator(ValidatorType.EMPTY);

        form.addItem(emailEditText);

        emailFieldLayout = view.findViewById(R.id.email_field_layout);
        emailAndPhoneFieldLayout = view.findViewById(R.id.email_phone_layout);

        emailText = (TextView) view.findViewById(R.id.email_text);
        emailField = (TextView) view.findViewById(R.id.email_field);
        phoneText = (TextView) view.findViewById(R.id.phone_text);
        phoneField = (TextView) view.findViewById(R.id.phone_field);
        headerText = (TextView) view.findViewById(R.id.header);

        submitButton = (RequestButton) view.findViewById(R.id.forgot_pass_submit_button);
        submitButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (form.checkValidity()) {
                    submitButton.startLoading(new InitAnimationListener() {
                        @Override
                        public void onAnimationFinished() {
                            getUserDetails();
                        }
                    });
                }
            }
        });

        emailMeButton = (RequestButton) view.findViewById(R.id.forgot_pass_email_me_button);
        emailMeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                    isSmsSubmit = false;
                    emailMeButton.startLoading(new InitAnimationListener() {
                        @Override
                        public void onAnimationFinished() {
                            sendPassword();
                        }
                    });
            }
        });

        textMeButton = (RequestButton) view.findViewById(R.id.forgot_pass_text_me_button);
        textMeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                isSmsSubmit = true;
                    textMeButton.startLoading(new InitAnimationListener() {
                        @Override
                        public void onAnimationFinished() {
                            sendPassword();
                        }
                    });
                }
        });
        showFirstStep();
        return view;
    }

    protected void getUserDetails() {
        UserMethodRequest request = new UserMethodRequest();
        request.setUserName(emailEditText.getEditText().getText().toString());
        if (!Application.get().getCommunicationManager().requestService(ForgotPasswordFragment.this, "getUserDetailsCallBack", Constants.SERVICE_GET_FORGET_PASSWORD_CONTACTS, request, UserFPResult.class, false, false)) {
            submitButton.stopLoading();
            emailMeButton.stopLoading();
            textMeButton.stopLoading();
        }
    }

    public void getUserDetailsCallBack(Object resultObj) {
        UserFPResult result = (UserFPResult) resultObj;
        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            showSecondStep(result.getObfuscatedEmail(), result.getObfuscatedPhone());
        } else {
            if (result != null) {
                ErrorUtils.handleError(Constants.ERROR_CODE_INVALID_ACCOUNT, getString(R.string.forgot_password_incorrect_username_text));
            } else {
                ErrorUtils.handleError(-1);
            }
            submitButton.stopLoading();
            emailMeButton.stopLoading();
            textMeButton.stopLoading();
        }
    }

    protected void sendPassword() {
        InsertUserMethodRequest request = new InsertUserMethodRequest();
        Register user = new Register();
        user.setEmail(emailEditText.getValue());
        user.setMobilePhone("");
        user.setMobilePhonePrefix("");
        user.setSkinId(Application.get().getSkinId());
        request.setSendMail(!isSmsSubmit); // is email.
        request.setSendSms(isSmsSubmit);
        request.setRegister(user);
        if (!Application.get().getCommunicationManager().requestService(ForgotPasswordFragment.this, "sendPasswordCallBack", Constants.SERVICE_SEND_PASSWORD, request, UserMethodResult.class, false, false)) {
            submitButton.stopLoading();
            emailMeButton.stopLoading();
            textMeButton.stopLoading();
        }
    }

    public void sendPasswordCallBack(Object resultObj) {
        Log.d(TAG, "insertUserCallBack");
        UserMethodResult result = (UserMethodResult) resultObj;
        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            Log.d(TAG, "Password sent successfully");
            String msg = result.getUserMessages()[0].getMessage();
            application.getNotificationManager().showNotificationSuccess(msg);
            application.postEvent(new NavigationEvent(Screen.LOGIN, NavigationEvent.NavigationType.INIT));
        } else {
            if (result != null) {
                String error = result.getUserMessages()[0].getMessage();
                String field = result.getUserMessages()[0].getField();
                Log.d(TAG, "Error Forgot Password \nfield: " + field + " \nerror: " + error + "\n");
                ErrorUtils.displayFieldError(form, result);
            } else {
                ErrorUtils.handleError(-1);
            }
            submitButton.stopLoading();
            emailMeButton.stopLoading();
            textMeButton.stopLoading();
        }
    }

    @Override
    protected Screenable setupScreen() {
        return Screen.FORGOT_PASSWORD;
    }

    private void showFirstStep() {
        headerText.setText(R.string.forgot_password_first_screen_text);
        emailFieldLayout.setVisibility(View.VISIBLE);
        emailAndPhoneFieldLayout.setVisibility(View.GONE);
        submitButton.setVisibility(View.VISIBLE);
        emailMeButton.setVisibility(View.GONE);
        textMeButton.setVisibility(View.GONE);

    }

    protected void showSecondStep(String email, String phone) {
        headerText.setText(R.string.forgot_password_second_screen_text);
        emailFieldLayout.setVisibility(View.GONE);
        emailAndPhoneFieldLayout.setVisibility(View.VISIBLE);
        submitButton.setVisibility(View.GONE);
        emailMeButton.setVisibility(View.VISIBLE);
        textMeButton.setVisibility(View.VISIBLE);
        emailField.setText(email);
        phoneField.setText(phone);
        boldTexts();
    }

    protected void boldTexts() {
        TextUtils.decorateInnerText(emailField, null, emailField.getText().toString(), emailField.getText().toString(), FontsUtils.Font.ROBOTO_MEDIUM, 0, 0);
        TextUtils.decorateInnerText(phoneField, null, phoneField.getText().toString(), phoneField.getText().toString(), FontsUtils.Font.ROBOTO_MEDIUM, 0, 0);
    }

}

