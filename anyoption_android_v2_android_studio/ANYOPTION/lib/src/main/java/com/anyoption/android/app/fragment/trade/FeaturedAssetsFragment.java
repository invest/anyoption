package com.anyoption.android.app.fragment.trade;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.adapter.MarketsExpandableListAdapter;
import com.anyoption.android.app.util.constants.Constants;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Custom {@link BaseFragment} that displays featured assets.
 * @author Anastas Arnaudov
 *
 */
public class FeaturedAssetsFragment extends NavigationalFragment {

	public static final String TAG = FeaturedAssetsFragment.class.getSimpleName();

	String filterTab;

	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static FeaturedAssetsFragment newInstance(Bundle bundle){
		FeaturedAssetsFragment assetsFeatured = new FeaturedAssetsFragment();
		assetsFeatured.setArguments(bundle);
		return assetsFeatured;
	}

	@SuppressWarnings("unused")
	private SimpleChartPagerFragment simpleChartPagerFragment;
	private AssetsListFragment assetsListFragment;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = getArguments();
		if(bundle != null){
			filterTab = bundle.getString(Constants.EXTRA_ASSETS_LIST_FILTER);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.featured_assets_fragment, container, false);
	
		if (getResources().getBoolean(R.bool.isTablet)) {
			rootView.findViewById(R.id.assets_featured_simple_chart_layout).setVisibility(View.GONE);
		} else {
			simpleChartPagerFragment = (SimpleChartPagerFragment) application.getFragmentManager().addFragment(R.id.assets_featured_simple_charts_fragment_layout, this, SimpleChartPagerFragment.class, SimpleChartPagerFragment.class.getSimpleName());
		}

		Bundle filterBundle = null;
		if(filterTab != null && filterTab.length() > 0) {
			if(application.isHasDynamics()) {
				filterBundle = new Bundle();
				filterBundle.putString(Constants.EXTRA_ASSETS_LIST_FILTER, filterTab);
			}
		}

		assetsListFragment = (AssetsListFragment) application.getFragmentManager().addFragment(R.id.assets_featured_assets_list_layout, this, AssetsListFragment.class, AssetsListFragment.class.getSimpleName(), filterBundle);
		assetsListFragment.getArguments().putByte(Constants.EXTRA_ASSETS_LIST_MODE, MarketsExpandableListAdapter.MODE_FEATURED_ASSETS);

		return rootView;
	}

	public void changeFilter(String filterTab) {
		if(assetsListFragment != null) {
			if(filterTab != null && filterTab.length() > 0) {
				assetsListFragment.changeFilter(filterTab);
			}
		}
	}

	@Override
	protected Screenable setupScreen() {
		return Screen.ASSETS_FEATURED;
	}

	@Override
	protected void refreshUser() {
		//Do nothing
	}
}
