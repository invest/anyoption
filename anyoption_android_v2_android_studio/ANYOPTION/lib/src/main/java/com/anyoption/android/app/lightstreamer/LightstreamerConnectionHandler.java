package com.anyoption.android.app.lightstreamer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import com.lightstreamer.ls_client.ConnectionInfo;
import com.lightstreamer.ls_client.ConnectionListener;
import com.lightstreamer.ls_client.ExtendedTableInfo;
import com.lightstreamer.ls_client.HandyTableListener;
import com.lightstreamer.ls_client.LSClient;
import com.lightstreamer.ls_client.PushConnException;
import com.lightstreamer.ls_client.PushServerException;
import com.lightstreamer.ls_client.PushUserException;
import com.lightstreamer.ls_client.SubscrException;
import com.lightstreamer.ls_client.SubscribedTableKey;

import android.util.Log;

/**
 * this class wraps the LSClient class to enhance it with automatic re-connections and automatic
 * re-subscriptions; when start is called from outside, until stop is called, this class will continuously
 * try to connect to a Lightstreamer server. 
 * If while connected the connection is dropped the class will try to connect again.
 * Moreover this class keeps a list of table to be subscribed as soon as a connection is established.
 */
public class LightstreamerConnectionHandler {
    
    public static final String TAG = LightstreamerConnectionHandler.class.getSimpleName();
    
    public static final int DISCONNECTED = 0;
    public static final int CONNECTING = 1;
    public static final int STREAMING = 2;
    public static final int POLLING = 3;
    public static final int STALLED = 4;
    
    final private LSClient client = new LSClient();
    private StatusListener statusListener;
    private ConnectionInfo info;
    private LinkedList<TableAndListener> tables = new LinkedList<TableAndListener>();
    private HashMap<ExtendedTableInfo, SubscribedTableKey> subscribedTables;
//    private ExecutorService connectionThread;
    private int status = DISCONNECTED;
  
    //  the phase will change on each connection effort so that calls from older StatusListener will be ignored
    private int phase = 0;
    
    public LightstreamerConnectionHandler(ConnectionInfo info, StatusListener statusListener) {
        this.statusListener = statusListener;
        this.info = info;
        this.subscribedTables = new HashMap<ExtendedTableInfo, SubscribedTableKey>();
        //  prepare an ExecutorService that will handle our connection efforts
//        connectionThread = Executors.newSingleThreadExecutor();
    }
    
    public synchronized void addTable(final ExtendedTableInfo table, final HandyTableListener listener) {
       new Thread(new Runnable() {//TODO  TEST
			@Override
			public void run() {
				synchronized (tables) {
		            TableAndListener toSub = new TableAndListener(table, listener);
		            tables.add(toSub);
//		            Log.v(TAG, "Table added " + Utils.stringArrayToString(table.getItems()));
		            
		            if (LightstreamerConnectionHandler.this.status == DISCONNECTED || LightstreamerConnectionHandler.this.status == CONNECTING) {
		                return;
		            }
		        
		            // if we're already connected the table will be subscribed immediately
		            try {
		            	LightstreamerConnectionHandler.this.subscribeOne(toSub, LightstreamerConnectionHandler.this.phase);
		            	//TODO sometimes return ERROR
		            } catch (Exception e) {
		            	stop();
		            	start();
		            }
		        }
			}
       }).start();
    	
    	
    }
    
    public synchronized void removeTable(final ExtendedTableInfo table) {
    	 new Thread(new Runnable() {//TODO  TEST
    		 @Override
 			public void run() {
		    	synchronized (tables) {
		            Log.v(TAG, "before tables.size(): " + tables.size());
		            for (Iterator<TableAndListener> i = tables.iterator(); i.hasNext();) {
		                if (i.next().table == table) {
		//                    Log.v(TAG, "Table removed " + Utils.stringArrayToString(table.getItems()));
		                    i.remove();
		                    break;
		                }
		            }
		            Log.v(TAG, "after tables.size(): " + tables.size());
		            
		            if (LightstreamerConnectionHandler.this.status == DISCONNECTED || LightstreamerConnectionHandler.this.status == CONNECTING) {
		                return;
		            }
		
		            unSubscribeOne(table);
		        }
 			}
         }).start();
    }
    
    private synchronized void start(int ph) {
        if (ph != this.phase) {
            // we ignore old calls
            return;
        }
        this.start();
    }
    
    public synchronized void start() {
        Log.v(TAG, "start");
        // this method starts a connection effort
        this.phase++;
        try {
        	this.changeStatus(this.phase,CONNECTING);
        } catch (Exception e) {
        	Log.e("anyoption", "Can't change status.", e);
        }
//        connectionThread.execute(new ConnectionThread(this.phase));
        new ConnectionThread(this.phase).start();
    }
    
    public synchronized void stop() {
        Log.v(TAG, "stop");
        // this method closes the connection and automatic re-connections
        this.phase++;
        this.client.closeConnection();
        try {
        	this.changeStatus(this.phase,DISCONNECTED);
        } catch (Exception e) {
        	Log.e("anyoption", "Can't change status.", e);
        }
    }
    
    private synchronized void changeStatus(final int ph, final int status) throws SubscrException, PushServerException, PushUserException, PushConnException {
        if (ph != this.phase) {
            // we ignore old calls
            return;
        }
        boolean shouldSubscribe = false;
        if (this.status == CONNECTING && (status == STREAMING || status == POLLING)) {
            shouldSubscribe = true;
        }
        this.status = status;
        if (status == DISCONNECTED || status == CONNECTING) {
            subscribedTables.clear();
        }
        // this method passes the current status to a simple listener
        if (this.statusListener != null) {
            this.statusListener.onStatusChange(status);
        }
        if (shouldSubscribe) {
            this.subscribe(this.phase);
        }
    }
    
    private void execute(int ph) {
        synchronized(this) {
            if (ph != this.phase) {
                return;
            }
            this.phase++;
            Log.v(TAG, "execute - phase: " + phase);
        }
       
            // we connect first
            ph = this.connect(this.phase); 
//            if (ph > -1) {
                // and then subscribe
//                this.subscribe(ph);
//            } // else someone called stop while we were connecting 
    }
    
    private int connect(int connPhase) {
        boolean connected = false;
        int pause = 2000;
        
        // this method will not exit until the openConnection returns without throwing an exception 
        // that means until we obtain a session from a Lightstreamer server 
        while (!connected) {
            synchronized(this) {
                if (connPhase != this.phase) {
                    Log.w(TAG, "connPhast: " + connPhase + " this.phase: " + this.phase);
                    //something changed the phase (maybe a stop call) while we were waiting; exit now
                    return -1;
                }
                
                connPhase = ++this.phase;
                Log.v(TAG, "connect - phase: " + phase);
            }    
                
            // move the client.openConnectin out of the synch block because when no net it
            // blocks for long time and make the app unresponsive in some cases
            try {
                client.openConnection(this.info, new LSConnectionListener(connPhase));
                connected = true;
            } catch (PushConnException e) {
                Log.e(TAG, "connect error: " + e.getMessage());
            } catch (PushServerException e) {
                Log.e(TAG, "connect error: " + e.getMessage());
            } catch (PushUserException e) {
                // wrong user password notifications will pass from here
                Log.e(TAG, "connect error: " + e.getMessage());
            }
            if (!connected) {
                // if we fail to connect we make a pause before the next effort
                try {
                    Thread.sleep(pause);
//                    wait(pause);
                } catch (InterruptedException e) {
                }
            }
            // each time a connection fails the pause will grow
//            pause*=2;
         }
        
        return connPhase;
    }
    
    private void subscribe(int ph) throws SubscrException, PushServerException, PushUserException, PushConnException {
        Log.v(TAG, "subscribe - ph: " + ph);
        synchronized (tables) {
            for (TableAndListener toSub : this.tables) {
//                Log.v(TAG, "subscribe table " + Utils.stringArrayToString(toSub.table.getItems()));
                if (!this.subscribeOne(toSub, ph)) {
                    return;
                }
            }
        }
    }
    
    private boolean subscribeOne(TableAndListener toSub, int subscriptionPhase) throws SubscrException, PushServerException, PushUserException, PushConnException {
        // most exceptions will be caused by problems that will let the entire connection to the server to fail
        // this means that probably, after an exception, we'll soon find a different session (and the subscription will be then reissued)
        // other exceptions are "static" exceptions that means that should not arise after the development phase is closed
        // (note that our metadata adapter does not deny anything to anyone)
        
        synchronized(this) {
//        Log.v(TAG, "subscribeOne - " + Utils.stringArrayToString(toSub.table.getItems()) + " this.phase: " + this.phase + " subscriptionPhase: " + subscriptionPhase);
            if (subscriptionPhase != this.phase) {
                //something changed the phase (maybe a stop call) while we were waiting; exit
                return false;
            }
            
            try {
                SubscribedTableKey tableKey = client.subscribeTable(toSub.table, toSub.listener, false);
                subscribedTables.put(toSub.table, tableKey);
                return true;
            } catch (SubscrException e) {
                Log.e(TAG, "subscribeOne", e);
            } catch (PushServerException e) {
                Log.e(TAG, "subscribeOne", e);
                //TODO TEST
                removeTable(toSub.table);
                stop();
                start();
                ////////////
            } catch (PushUserException e) {
                Log.e(TAG, "subscribeOne", e);
            } catch (PushConnException e) {
                Log.e(TAG, "subscribeOne", e);
            }
        }

        return false;
    }
    
    private void unSubscribeOne(final ExtendedTableInfo table) {
    	final SubscribedTableKey tableKey = subscribedTables.get(table);
    	if (tableKey != null) {
    		
    		// TODO There is always exception. Try with the new library.
    		// The unsubscribing should not run on UI thread.
    		new Thread(new Runnable() {
				
				@Override
				public void run() {
					try {
						client.unsubscribeTable(tableKey);
						subscribedTables.remove(table);
					} catch (SubscrException e) {
						Log.e(TAG, "unSubscribeOne", e);
					} catch (PushServerException e) {
		                Log.e(TAG, "unSubscribeOne", e);
					} catch (PushConnException e) {
		                Log.e(TAG, "unSubscribeOne", e);
					}
				}
			}).start();
   
    	}
    }

	/**
	 * Sends the message in a background thread.
	 * 
	 * @param msg
	 */
	public void sendMessage(final String msg) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					// This method must run in a background thread because it creates network
					// connection.
					client.sendMessage(msg);
				} catch (PushConnException e) {
					Log.e(TAG, "sendMessage", e);
				} catch (PushServerException e) {
					Log.e(TAG, "sendMessage", e);
				} catch (PushUserException e) {
					Log.e(TAG, "sendMessage", e);
				}
			}
		}).start();
	}
    
    /**
     * this class will receive the notifications on the status of the connection. 
     * Such information will be used to automatically reconnect and to send the status 
     * to a listener
     */
    class LSConnectionListener implements ConnectionListener {
        
        private boolean isPolling;
        private int ph = 0;        

        public LSConnectionListener(int phase) {
            super();
            this.ph = phase;
        }

        private void onConnection() {
            Log.v(TAG, "LSConnectionListener.onConnection");
            try {
	            if (this.isPolling) {
	                changeStatus(this.ph, POLLING);
	            } else {
	                changeStatus(this.ph, STREAMING);
	            }
            } catch (Exception e) {
            	Log.e("anyoption", "Can't change status.", e);
            	onDisconnection();
            }
        }
        
        private void onDisconnection() {
            Log.v(TAG, "LSConnectionListener.onDisconnection");
            try {
            	changeStatus(this.ph,DISCONNECTED);
            } catch (Exception e) {
            	Log.e("anyoption", "Can't change status.", e);
            }
            start(this.ph);
        }

        public void onActivityWarning(boolean warningOn) {
            Log.v(TAG, "LSConnectionListener.onActivityWarning");
            if (warningOn) {
            	try {
            		changeStatus(this.ph, STALLED);
                } catch (Exception e) {
                	Log.e("anyoption", "Can't change status.", e);
                }
            } else {
                this.onConnection();
            }
        }

        public void onClose() {
            Log.v(TAG, "LSConnectionListener.onClose");
            this.onDisconnection();
        }
        
        public void onConnectionEstablished() {   
            Log.v(TAG, "LSConnectionListener.onConnectionEstablished");
        }
        
        public void onDataError(PushServerException e) {
            Log.v(TAG, "LSConnectionListener.onDataError");
        }
        
        public void onEnd(int cause) {
            Log.v(TAG, "LSConnectionListener.onEnd");
            this.onDisconnection();
        }

        public void onFailure(PushServerException e) {
            Log.v(TAG, "LSConnectionListener.onFailure");
            this.onDisconnection();
        }

        public void onFailure(PushConnException e) {
            Log.v(TAG, "LSConnectionListener.onFailure");
            this.onDisconnection();
        }
        
        public void onNewBytes(long b) {
        }
        
        public void onSessionStarted(boolean isPolling) {
            Log.v(TAG, "LSConnectionListener.onSessionStarted - isPolling: " + isPolling);
            this.isPolling = isPolling;
            this.onConnection();
        }
    }
    
    private class ConnectionThread extends Thread {
        private final int ph;

        public ConnectionThread(int ph) {
            Log.v(TAG, "ConnectionThread - ph: " + ph);
            this.ph = ph;
        }

        public void run() {
            execute(this.ph);
            Log.v(TAG, "ConnectionThread - done.");
        }
    }
    
    class TableAndListener {
        ExtendedTableInfo table;
        HandyTableListener listener;
        
        public TableAndListener(ExtendedTableInfo table,
                HandyTableListener listener) {
           this.table = table;
           this.listener = listener;
        }
    }
    
    public interface StatusListener {
        public void onStatusChange(int status);
    }
}