package com.anyoption.android.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Application.ActivityLifecycleCallbacks;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.text.BidiFormatter;
import android.util.Log;
import android.widget.EditText;

import com.anyoption.android.R;
import com.anyoption.android.app.activity.BaseActivity;
import com.anyoption.android.app.activity.ShortcutActivity;
import com.anyoption.android.app.event.InstallEvent;
import com.anyoption.android.app.event.InvestmentEvent;
import com.anyoption.android.app.event.InvestmentEvent.Type;
import com.anyoption.android.app.event.LanguageChangeEvent;
import com.anyoption.android.app.event.LayoutDirectionChangeEvent;
import com.anyoption.android.app.event.LocaleChangeEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.event.UserUpdatedEvent;
import com.anyoption.android.app.eventbus.EventBus;
import com.anyoption.android.app.manager.AppoxeeManager;
import com.anyoption.android.app.manager.AppsFlyerManager;
import com.anyoption.android.app.manager.AppseeManager;
import com.anyoption.android.app.manager.CommunicationManager;
import com.anyoption.android.app.manager.ConnectionManager;
import com.anyoption.android.app.manager.DataBaseManager;
import com.anyoption.android.app.manager.DeepLinksManager;
import com.anyoption.android.app.manager.FacebookManager;
import com.anyoption.android.app.manager.FragmentManager;
import com.anyoption.android.app.manager.GCMManager;
import com.anyoption.android.app.manager.GoogleAdWordsManager;
import com.anyoption.android.app.manager.GoogleAnalyticsManager;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.anyoption.android.app.manager.NotificationManager;
import com.anyoption.android.app.manager.PermissionsManager;
import com.anyoption.android.app.manager.PopUpManager;
import com.anyoption.android.app.manager.PushNotificationsManager;
import com.anyoption.android.app.manager.SharedPreferencesManager;
import com.anyoption.android.app.manager.TelephonyManager;
import com.anyoption.android.app.model.InsuranceItem;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.beans.base.City;
import com.anyoption.common.beans.base.BalanceStepPredefValue;
import com.anyoption.common.beans.base.Country;
import com.anyoption.common.beans.base.CountryMiniBean;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.MarketGroup;
import com.anyoption.common.beans.base.PaymentMethod;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.enums.CountryStatus;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MarketsGroupsMethodResult;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.json.requests.CheckUpdateMethodRequest;
import com.anyoption.json.requests.DepositReceiptMethodRequest;
import com.anyoption.json.results.LogoutBalanceStepMethodResult;
import com.anyoption.json.results.UpdateMethodResult;
import com.google.android.gms.analytics.Tracker;

import java.io.Serializable;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * Base class for holding global state of the application.
 * @author Anastas Arnaudov
 *
 */
public abstract class Application extends android.app.Application implements ActivityLifecycleCallbacks {

    public static final String TAG = Application.class.getSimpleName();

	/**
	 * The amount to show to the logout users in case of error during the request.
	 */
	private static final double DEFAULT_INVEST_AMOUNT = 500.0;


	/**
	 * Default Skin Id set in the setupDefaultLanguageAndLocale
	 */
	protected static long DEFAULT_SKIN_ID = Skin.SKIN_REG_EN;

	/**
	 * Array of languages supported from this application.
	 */
	private static final String[] LANGUAGES_ARR = new String[] { "en", "es", "de", "fr", "it", "ru", "tr", "sv", "nl" };
	private static final Set<String> LANGUAGES_SET = new HashSet<String>(Arrays.asList(LANGUAGES_ARR));

	/**
	 * Default language which is used if skin's language is not one of {@link #LANGUAGES_ARR} array.
	 */
	private static final String DEFAULT_LANGUAGE = "en";

	protected static Application singletonAplication;
	private EventBus eventBus;
	private String sessionId;

    ///////////		MANAGERS	///////////////////////////////
    protected ConnectionManager connectionManager;
    protected CommunicationManager communicationManager;
    protected DataBaseManager dataBaseManager;
    protected FragmentManager fragmentManager;
    protected GCMManager gcmManager;
    protected AppsFlyerManager appsFlyerManager;
    protected NotificationManager notificationManager;
    protected PopUpManager popUpManager;
    protected SharedPreferencesManager sharedPreferencesManager;
    protected TelephonyManager telephonyManager;
    protected LightstreamerManager lightstreamerManager;
    protected AppoxeeManager appoxeeManager;
    protected GoogleAdWordsManager googleAdWordsManager;
    protected PushNotificationsManager pushNotificationsManager;
    protected AppseeManager appseeManager;
    protected FacebookManager facebookManager;
    protected GoogleAnalyticsManager googleAnalyticsManager;
    protected DeepLinksManager deepLinksManager;
    protected PermissionsManager permissionsManager;
    //////////////////////////////////////////////////////////

	/**
	 * Reference to the currently active Activity.
	 */
	private BaseActivity currentActivity;
	/**
	 * The current Screen.
	 */
	private Screenable currentScreen;
	/**
	 * The Screenable before recreation.
	 */
	private Screenable lastScreen;
	private Bundle lastBundle;
	
	private int numberOfCreatedActivities 		= 0;
	private int numberOfDestroyedActivities 	= 0;
	private int numberOfStartedActivities 		= 0;
	private int numberOfStoppedActivities 		= 0;
	private int numberOfResumedActivities 		= 0;
	private int numberOfPausedActivities 		= 0;
	private String newLocale;
	private List<Country> countriesList;

	public interface OnActivityResultListener{
		public void onActivityResultEvent(int requestCode, int resultCode, Intent intent);
	}
	List<OnActivityResultListener> onActivityResultListeners;
	
	private User user;
	private UserRegulationBase userRegulation;
	private long skinId;
	private long countryId;
	private boolean isRegulated;
	private Double defaultInvestAmount;
	private Currency defaultSkinCurrency;
	private List<BalanceStepPredefValue> predefinedInvestAmounts;
	private long minInvestmentAmount = 0L;

	private HashMap<Long, Skin> skinsMap;
	private HashMap<Long, Skin> visibleSkinsMap;
	private List<Long> visibleSkinsList;
	private MarketGroup[] marketGroups;
	private List<Investment> investments;
	private Map<Long, Country> countriesMap;
	private Country[] countriesArray;
	private City[] citiesArray;
	
	private ArrayList<ArrayList<InsuranceItem>> takeProfitGroups;
	private ArrayList<ArrayList<InsuranceItem>> rollForwardGroups;
	
	private Locale locale;
	private LayoutDirection layoutDirection;
	
	private boolean isMarketsLoaded = false;
	
	private EditText currentEditTextOnFocus;
	
	protected boolean isTablet;
	
	protected Tracker tracker;

	protected boolean isHasDynamicsLogout;

	protected boolean isShowCancelInvestmentPopup;

	protected CookieManager cookieManager;

	public CookieManager getCookieManager() {
		return this.cookieManager;
	}

	//#################################################################################################################
	//######################							SCREENs						###################################
	//#################################################################################################################

	
	
	public interface Screenable extends Serializable{
		public static final int SCREEN_FLAG_ADD_MENU 			= 1;
		public static final int SCREEN_FLAG_REMOVE_MENU 		= 2;
		public static final int SCREEN_FLAG_ADD_BACK			= 3;
		public static final int SCREEN_FLAG_REMOVE_BACK			= 4;
		public static final int SCREEN_FLAG_ADD_HELP 			= 5;
		public static final int SCREEN_FLAG_REMOVE_HELP 		= 6;
		
		public String getTag();
		public String getTitle();
		public int getImageID();
		public int[] getFlags();
	}
	
	/**
	 * Constant names for each main screen (Map each screen to a Constant).
	 * This is used in the navigation flow.
	 * @author Anastas Arnaudov
	 *
	 */
	public enum Screen implements Screenable{

		LOGIN("login", get().getResources().getString(R.string.loginScreenTitle), 0, Screenable.SCREEN_FLAG_REMOVE_MENU),
		REGISTER("register", get().getResources().getString(R.string.registerScreenTitle), 0, Screenable.SCREEN_FLAG_REMOVE_MENU),
		FORGOT_PASSWORD("forgot_password", get().getResources().getString(R.string.forgotPasswordScreenTitle), 0),
		UPDATE_APPLICATION_VERSION("update_application_version", get().getResources().getString(R.string.updateScreenTitle), 0),
		WELCOME("welcome", "", R.drawable.logo_1), 
		PUSH_NOTIFICATION_PERMISSION("notification_permission", "", R.drawable.logo_1),
		UNREGULATED_COUNTRY("unregulated_country", get().getResources().getString(R.string.unregulatedCountryScreenTitle), 0),
		ASSET_PAGE("asset_page", "", 0, Screenable.SCREEN_FLAG_ADD_HELP), 
		MY_ACCOUNT("my_account", get().getResources().getString(R.string.myAccountScreenTitle), 0),
		DEPOSIT("deposit", get().getResources().getString(R.string.deposit), 0, Screenable.SCREEN_FLAG_ADD_HELP),
		SUPPORT("support", get().getResources().getString(R.string.supportScreenTitle), 0),
		SETTINGS("settings", get().getResources().getString(R.string.settingsScreenTitle), 0),
		ASSET_VIEW_PAGER("assets_view_pager", "", 0, Screenable.SCREEN_FLAG_ADD_HELP),
		TRADE("trade", "", R.drawable.logo_1, Screenable.SCREEN_FLAG_ADD_HELP),
		ASSETS_FEATURED("assets_featured", "", R.drawable.logo_1),
		OPEN_OPTIONS("open_options", "", R.drawable.logo_1),
		ASSETS_LIST("assets_list", get().getResources().getString(R.string.investment_header_market_slosestToExp), 0, Screenable.SCREEN_FLAG_ADD_HELP),
		SIMPLE_CHART_PAGER("simple_chart_pager", "", 0),
		MANDATORY_QUESTIONNAIRE("mandatory_questionnaire", get().getResources().getString(R.string.questionnaire_title), 0),
		OPTIONAL_QUESTIONNAIRE("optional_questionnaire", get().getResources().getString(R.string.questionnaire_title), 0),
		PERSONAL_DETAILS_MENU("personal_details_menu", get().getResources().getString(R.string.personalDetailsScreenTitle), 0),
		UPDATE_PASSWORD("update_password", get().getResources().getString(R.string.changePasswordScreenTitle),0),
		DEPOSIT_WELCOME("deposit_welcome", get().getResources().getString(R.string.deposit), 0),
		EXISTING_CARD("existing_card", get().getResources().getString(R.string.existingCardScreenTitle), 0, Screenable.SCREEN_FLAG_ADD_HELP),
		DOMESTIC_PAYMENTS("domestic_payments", get().getResources().getString(R.string.domesticPaymentsScreenTitle), 0),
		DOMESTIC_DEPOSIT("domestic deposit", get().getResources().getString(R.string.domesticDepositScreenTitle), 0, Screenable.SCREEN_FLAG_ADD_HELP),
		WIRE_DEPOSIT("wire deposit", get().getResources().getString(R.string.wireTransferScreenTitle), 0, Screenable.SCREEN_FLAG_ADD_HELP),
		FIRST_NEW_CARD("new card", get().getResources().getString(R.string.newCardScreenTitle), 0, Screenable.SCREEN_FLAG_ADD_HELP),
		WITHDRAW_MENU("withdraw_menu", get().getResources().getString(R.string.withdrawScreenTitle), 0, Screenable.SCREEN_FLAG_ADD_HELP),
		BANK_WIRE_WITHDRAW("bank_wire_withdraw", get().getResources().getString(R.string.header_withdraw), 0, Screenable.SCREEN_FLAG_ADD_HELP),
		CC_WITHDRAW("CC_withdraw", get().getResources().getString(R.string.header_withdraw), 0, Screenable.SCREEN_FLAG_ADD_HELP),
		UPDATE_INFO("update_info", get().getResources().getString(R.string.updateInfoScreenTitle), 0),
		BONUSES("bonuses", get().getResources().getString(R.string.bonuses), 0),
		BONUS_DETAILS("bonus_details", get().getResources().getString(R.string.bonus_header_detail), 0),
		DEPOSIT_MENU("deposit_menu", get().getResources().getString(R.string.depositScreenTitle), 0, Screenable.SCREEN_FLAG_ADD_HELP),
		TABLET_TRADE("tablet_trade", "", R.drawable.logo_1, Screenable.SCREEN_FLAG_ADD_HELP),
		BANKING("banking", get().getResources().getString(R.string.bankingScreenTitle), 0, Screenable.SCREEN_FLAG_ADD_HELP),
		TERMS_AND_CONDITIONS("terms_and_conditions", get().getResources().getString(R.string.termsAndConditionsScreenTitle), 0),
		AGREEMENT("agreement", get().getResources().getString(R.string.agreementScreenTitle), 0),
		SETTLED_OPTIONS("settled_options", get().getResources().getString(R.string.settledOptions), 0),
		MY_OPTIONS("my_options", get().getResources().getString(R.string.myOptions), 0),
		DIRECT24_DEPOSIT("direct24_deposit", get().getResources().getString(R.string.depositOtherScreenDirect24Title), 0),		
		GIROPAY_DEPOSIT("giropay_deposit", get().getResources().getString(R.string.depositOtherScreenGiropayTitle), 0),
		EPS_DEPOSIT("eps_deposit", get().getResources().getString(R.string.depositOtherScreenEPSTitle), 0),
		AGREEMENT_TERMS("agreement_terms", get().getResources().getString(R.string.agreementScreenTitle), 0),
		WITHDRAW_CLOSED("withdraw_closed", get().getResources().getString(R.string.withdrawScreenTitle), 0, Screenable.SCREEN_FLAG_ADD_HELP),
		QUESTIONNAIRE("questionnaire", get().getResources().getString(R.string.questionnaireScreen1Title), 0, Screenable.SCREEN_FLAG_ADD_BACK),
		UPLOAD_DOCUMENTS("upload_documents", get().getResources().getString(R.string.upload_documents_screen_title), 0, Screenable.SCREEN_FLAG_ADD_BACK),
		ACCOUNT_BLOCKED("account_blocked", get().getResources().getString(R.string.account_blocked_screen_title), 0, Screenable.SCREEN_FLAG_ADD_BACK),
		BUBBLES("", "", 0),
		WITHDRAW_ASSIST("withdraw_assist", get().getResources().getString(R.string.withdrawAssistScreenTitle), 0, Screenable.SCREEN_FLAG_REMOVE_BACK)
		;

		/**
		 * Tag used to search.
		 */
		private String tag;
		/**
		 * A default title. "" - if no default title is needed.
		 */
		private String title;
		/**
		 * Id of an image used instead of title. 0 - if no image is needed.
		 */
		private int imageID;
		/**
		 * Additional preferences.
		 */
		private int[] flags;

		/**
		 * Map of all screens
		 */
		private static HashMap<String, Screenable> screensMap;

		Screen(String tag, String title, int imageID, int... flags) {
			this.tag = tag;
			this.title = title;
			this.imageID = imageID;
			this.flags = flags;
		}

		@Override
		public String getTag() {
			return tag;
		}
		@Override
		public String getTitle() {
			return title;
		}
		@Override
		public int getImageID() {
			return imageID;
		}
		@Override
		public int[] getFlags(){
			return this.flags;
		}

		public static Screenable getScreenByTag(String tag) {
			if (screensMap == null) {
				Screenable[] screens = values();
				screensMap = new HashMap<String, Screenable>(screens.length);
				for (Screenable screen : screens) {
					screensMap.put(screen.getTag(), screen);
				}
			}

			return screensMap.get(tag);
		}

	}
	//#################################################################################################################
	
	/**
	 * The direction of the layout elements.
	 * @author Anastas Arnaudov
	 */
	public enum LayoutDirection{
		LEFT_TO_RIGHT,
		RIGHT_TO_LEFT
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		Log.d(TAG, "onCreate");
		singletonAplication = this;
		onActivityResultListeners = new ArrayList<Application.OnActivityResultListener>();

		Utils.setAndroidAdvertisingID();

		cookieManager = new CookieManager();

		eventBus = EventBus.getDefault();
		// Must be assigned before lightstreamerManager
		takeProfitGroups = new ArrayList<ArrayList<InsuranceItem>>();
		rollForwardGroups = new ArrayList<ArrayList<InsuranceItem>>();

        registerForEvents(this, LoginLogoutEvent.class, NavigationEvent.class, LanguageChangeEvent.class, InstallEvent.class);

        deepLinksManager = getDeepLinksManager();
        connectionManager = getConnectionManager();
        communicationManager = getCommunicationManager();
        dataBaseManager = getDataBaseManager();
        fragmentManager = getFragmentManager();
        gcmManager = getGCMManager();
        appsFlyerManager = getAppsFlyerManager();
        notificationManager = getNotificationManager();
        popUpManager = getPopUpManager();
        sharedPreferencesManager = getSharedPreferencesManager();
        telephonyManager = getTelephonyManager();
        lightstreamerManager = getLightstreamerManager();
        appoxeeManager = getAppoxeeManager();
        facebookManager = getFacebookManager();
        googleAdWordsManager = getGoogleAdWordsManager();
        appseeManager = getAppseeManager();
        googleAnalyticsManager = getGoogleAnalyticsManager();
        permissionsManager = getPermissionsManager();

        setupDefaultLanguageAndLocale();
        updateIsRegulated();

        marketGroups = new MarketGroup[0];
        investments = new ArrayList<Investment>();
        predefinedInvestAmounts = new ArrayList<BalanceStepPredefValue>();

        registerActivityLifecycleCallbacks(this);

        //Check if this is the first opening and create desktop shortcut icon:
        if (getSharedPreferencesManager().getBoolean(Constants.PREFERENCES_IS_FIRST_APPLICATION_OPEN, true)) {
            createDesktopShortcut();
        }

        isTablet = getResources().getBoolean(R.bool.isTablet);
		CookieHandler.setDefault(getCookieManager());
    }

	public void setSessionId(String sessionId){
		this.sessionId = sessionId;
	}

	/**
	 * 	Get the stored server session id.
	 * @return
     */
	public String getSessionId(){
		return sessionId;
	}

	@Override     
	public void onConfigurationChanged(Configuration newConfig) {         
		super.onConfigurationChanged(newConfig);         
		if (locale != null)  {             
		    Configuration config = new Configuration(newConfig);
		    config.locale = locale;
			Locale.setDefault(locale);             
			getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
		}     
	}
	
	/**
	 * Sets up the default SkinId and Locale.
	 */
	protected void setupDefaultLanguageAndLocale(){
		Configuration config = getBaseContext().getResources().getConfiguration();
		String lang;

		setSkinId(getSharedPreferencesManager().getLong(Constants.PREFERENCES_SKINID, DEFAULT_SKIN_ID));
		lang = getSharedPreferencesManager().getString(Constants.PREFERENCES_LOCALE, getDefaultLanguage());

		locale = new Locale(lang);
		if (!lang.equals("") && !lang.equals(config.locale.getLanguage())) {
			Locale.setDefault(locale);
			config.locale = locale;
			getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
		}
		setupDefaultLayoutDirection();
	}
	
	/**
	 * Sets up the default layout direction.
	 */
	private void setupDefaultLayoutDirection(){
		BidiFormatter bidiFormatter = BidiFormatter.getInstance(locale);
		if(bidiFormatter.isRtlContext()){
			this.layoutDirection = LayoutDirection.RIGHT_TO_LEFT;
		}else{
			this.layoutDirection = LayoutDirection.LEFT_TO_RIGHT;
		}
	}
	
	/**
	 * Returns the singleton instance of the {@link Application}.
	 * @return
	 */
	public static Application get(){
		return singletonAplication;
	}

	public abstract String getServiceURL();

	public abstract String getLSURL();

	public abstract String getMerchantID();

	public abstract String getOneClickURL();

	public abstract String getWebSiteLink();

	public String getDeepLink(){return null;}

	@SuppressWarnings("static-method")
	public long getWriterId() {
		return Constants.MOBILE_WRITER_ID;
	}

	@SuppressWarnings("static-method")
	public String getServiceMethodGetUser() {
		return Constants.SERVICE_GET_USER;
	}

	@SuppressWarnings({ "static-method", "rawtypes" })
	public Class getUserMethodResultClass() {
		return UserMethodResult.class;
	}

	/**
	 * Returns the {@link EventBus}.
	 * 
	 * @return
	 */
	public EventBus getEventBus(){
		return eventBus;
	}
	
	/**
	 * Posts the given event to the event bus. 
	 */
	public void postEvent(Object event){
		getEventBus().post(event);
	}
	
	/**
	 * Calls the {@link EventBus#register(Object)}.
	 * <p><b>HINT : </b> Consider registering for events to be the first thing to do in the creation process of the subscriber.
	 * Also don't forget to unregister when the  subscriber no longer needs to listen for events.
	 * <p><b>HINT : </b> If the subscriber is an Android component consider register in the onCreate() and onResumeMainActivity() methods
	 * and unregister in onPauseMainActivity() and onDestroy() methods.
	 * @param subscriber The object that will listen to the events of the desired type.
	 * @param eventTypes The event types.
	 */
	public void registerForEvents(Object subscriber, Class<?>... eventTypes){
		for(Class<?> eventType : eventTypes){
			if(!isRegistered(subscriber, eventType)){
				eventBus.register(subscriber, eventType);
			}
		}
	}
	
	/**
	 * Calls the {@link EventBus#unregister(Object)}.
	 */
	public void unregisterForAllEvents(Object subscriber){
		if(isRegistered(subscriber)){
			eventBus.unregister(subscriber);			
		}
	}
	
	/**
	 * Checks whether the subscriber is registered to receive any events.
	 * @param subscriber
	 * @return
	 */
	public boolean isRegistered(Object subscriber){
		return getEventBus().isRegistered(subscriber);
	}
	
	/**
	 * Checks whether the subscriber is registered for the given event type.
	 * @param subscriber
	 * @param eventType
	 * @return
	 */
	public boolean isRegistered(Object subscriber, Class<?> eventType){
		return getEventBus().isRegistered(subscriber, eventType);
	}

	/**
	 * Returns the {@link ConnectionManager}
	 * @return
	 */
	public synchronized ConnectionManager getConnectionManager(){
		if(connectionManager == null){
			connectionManager = new ConnectionManager(this);
		}
		return connectionManager;
	}
	
	/**
	 * Returns the {@link CommunicationManager}
	 * @return
	 */
	public synchronized CommunicationManager getCommunicationManager(){
		if(communicationManager == null){
			communicationManager = new CommunicationManager(this);
		}
		return communicationManager;
	}
	
	/**
	 * Returns the {@link DataBaseManager}
	 * @return
	 */
	public synchronized DataBaseManager getDataBaseManager(){
		if(dataBaseManager == null){
			dataBaseManager = new DataBaseManager(this);
		}
		return dataBaseManager;
	}
	/**
	 * Returns the {@link FragmentManager}
	 * @return
	 */
	public synchronized FragmentManager getFragmentManager(){
		if(fragmentManager == null){
			fragmentManager = new FragmentManager(this);
		}
		return fragmentManager;
	}
	
	/**
	 * Returns the {@link AppsFlyerManager}
	 * @return
	 */
	public synchronized AppsFlyerManager getAppsFlyerManager(){
		if(appsFlyerManager == null){
			appsFlyerManager = new AppsFlyerManager(this);
		}
		return appsFlyerManager;
	}

	/**
	 * Returns the {@link GCMManager}
	 * @return
	 */
	@Deprecated
	public synchronized GCMManager getGCMManager(){
		if(gcmManager == null){
			gcmManager = new GCMManager(this);
		}
		return gcmManager;
	}
	
	/**
	 * Returns the {@link NotificationManager}
	 * @return
	 */
	public synchronized NotificationManager getNotificationManager(){
		if(notificationManager == null){
			notificationManager = new NotificationManager(this);
		}
		return notificationManager;
	}
	
	/**
	 * Returns the {@link PopUpManager}
	 * @return
	 */
	public synchronized PopUpManager getPopUpManager(){
		if(popUpManager == null){
			popUpManager = new PopUpManager(this);
		}
		return popUpManager;
	}
	
	/**
	 * Returns the {@link SharedPreferencesManager}
	 * @return
	 */
	public synchronized SharedPreferencesManager getSharedPreferencesManager(){
		if(sharedPreferencesManager == null){
			sharedPreferencesManager = new SharedPreferencesManager(this);
		}
		return sharedPreferencesManager;
	}
	
	/**
	 * Returns the {@link TelephonyManager}
	 * @return
	 */
	public synchronized TelephonyManager getTelephonyManager(){
		if(telephonyManager == null){
			telephonyManager = new TelephonyManager(this);
		}
		return telephonyManager;
	}
	
	/**
	 * Returns the {@link LightstreamerManager}
	 * 
	 * @return
	 */
	public synchronized LightstreamerManager getLightstreamerManager() {
		if (lightstreamerManager == null) {
			lightstreamerManager = new LightstreamerManager(this);
		}

		return lightstreamerManager;
	}
	
	public synchronized AppoxeeManager getAppoxeeManager() {
		if (null == appoxeeManager) {
			appoxeeManager = new AppoxeeManager(this);
		}
		return appoxeeManager;
	}
	
	public synchronized PushNotificationsManager getPushNotificationsManager() {
		if (null == pushNotificationsManager) {
			pushNotificationsManager = new PushNotificationsManager(this);
		}
		return pushNotificationsManager;
	}
	
	public synchronized FacebookManager getFacebookManager() {
		if (null == facebookManager) {
			facebookManager = new FacebookManager(this);
		}
		return facebookManager;
	}

    public synchronized AppseeManager getAppseeManager() {
        if (null == appseeManager) {
            appseeManager = new AppseeManager();
        }
        return appseeManager;
    }

    public synchronized PermissionsManager getPermissionsManager() {
        if (permissionsManager == null) {
            permissionsManager = new PermissionsManager(this);
        }
        return permissionsManager;
    }

    public abstract GoogleAdWordsManager getGoogleAdWordsManager();

	public abstract GoogleAnalyticsManager getGoogleAnalyticsManager();

	public synchronized DeepLinksManager getDeepLinksManager() {
		if (null == deepLinksManager) {
			deepLinksManager = new DeepLinksManager(this);
		}
		return deepLinksManager;
	}

    /**
     * Gets the Application's version code.
     */
	public int getApplicationVersionCode() {
		try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            //Should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * Gets the Application's version name.
     */
	public String getApplicationVersionName() {
		try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            return packageInfo.versionName;
        } catch (NameNotFoundException e) {
            //Should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
	
	/**
	 * Gets the currently logged {@link User}.
	 * @return
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Sets the logged {@link User}.
	 * @param user
	 */
	public void setUser(User user) {
		Log.d(TAG, "setUser");
		this.user = user;
		postEvent(new UserUpdatedEvent());
	}

	/**
	 * Checks whether the user is logged in.
	 */
	public boolean isLoggedIn(){
		return (user == null) ? false : true;
	}
	
	/**
	 * Checks whether the User is a tester or not (or if it is null).
	 */
	public boolean isUserTester(){
		if(user != null && user.getClassId() == 0L){
			return true;
		}
		return false;
	}
	
	/**
	 * Gets the {@link UserRegulationBase}.
	 * @return
	 */
	public UserRegulationBase getUserRegulation() {
		return userRegulation;
	}

	/**
	 * Sets the {@link UserRegulationBase}.
	 * @param userRegulation
	 */
	public void setUserRegulation(UserRegulationBase userRegulation) {
		this.userRegulation = userRegulation;
	}
	
	private void updateIsRegulated(){
		Skin skin = getSkins().get(skinId);
		isRegulated = skin.isRegulated();
    }

	public HashMap<Long, Skin> getSkins() {
		return skinsMap;
	}
	
	public long getSkinId() {
		return skinId;
	}

	public long checkIfChineseOrKorean(long skinId){
		if(skinId == Skin.SKIN_CHINESE || skinId == Skin.SKIN_CHINESE_VIP || skinId == Skin.SKIN_KOREAN){
			return Skin.SKIN_ENGLISH;
		}else{
			return skinId;
		}
	}

	public void setCountryList(HashMap<Long, CountryMiniBean> countryMap){
		new AsyncTask<Object,Void,Void>(){
			@Override
			protected Void doInBackground(Object... params) {
				HashMap<Long, CountryMiniBean> countryBeanMap = (HashMap<Long, CountryMiniBean>) params[0];
				Map<Long, Country> countryMap 	= getCountriesMap();
				List<Country> countryList 		= getCountryList();
				countryMap.clear();
				countryList.clear();

				for (Map.Entry<Long,CountryMiniBean> entry : countryBeanMap.entrySet()) {
					long key = entry.getKey();
					CountryMiniBean value = entry.getValue();

					Country country = new Country();
					country.setId(key);
					country.setCountryStatus(CountryStatus.NOTBLOCKED);
					country.setA2(value.getA2());
					country.setName(value.getDisplayName());
					country.setPhoneCode(value.getPhoneCode());
					country.setSupportPhone(value.getSupportPhone());
					country.setSupportFax(value.getSupportFax());

					countryMap.put(key, country);
					countryList.add(country);
				}

				setCountriesArray(countryList.toArray(new Country[countryList.size()]));

				return null;
			}
		}.execute(countryMap);
	}

	public void setCountryList(List<Country> countryList){

	}

	public List<Country> getCountryList(){
		return countriesList;
	}

	public void setupCountriesCollections(Country[] countriesArray){
		setCountriesArray(countriesArray);
		Map<Long, Country> countryMap 	= new HashMap<Long, Country>();
		List<Country> countryList 		= new ArrayList<Country>();
		for(Country country : countriesArray){
			countryList.add(country);
			countryMap.put(country.getId(), country);
		}
		setCountryList(countryList);
		setCountriesMap(countryMap);
	}

	public void setVisibleSkins(List<Long> visibleSkinsList){
		this.visibleSkinsList = visibleSkinsList;
		setVisibleSkinsMap();
	}

	public List<Long> getVisibleSkinsList() {
		return visibleSkinsList;
	}

	public void setVisibleSkinsMap(){
		visibleSkinsMap = new HashMap<Long, Skin>();

		for(Long visibleSkinId : getVisibleSkinsList()){
			visibleSkinsMap.put(visibleSkinId, skinsMap.get(visibleSkinId));
		}
	}

	public HashMap<Long, Skin> getVisibleSkinsMap() {
		return visibleSkinsMap;
	}

	@SuppressWarnings("resource")
	@SuppressLint({ "UseValueOf", "UseSparseArrays" })
	public void setSkinId(long skinId) {
		Log.d(TAG, "setSkinId to: " + skinId);

		this.skinId = skinId;
		getSharedPreferencesManager().putLong(Constants.PREFERENCES_SKINID, skinId);

		try {
            DataBaseManager.DB_PATH = getPackageManager().getPackageInfo(getPackageName(), 0).applicationInfo.dataDir + "/databases/";
            Log.i(getPackageName(), "db path = " + DataBaseManager.DB_PATH);
        } catch (NameNotFoundException e1) {
            Log.d(getPackageName(), "cant take db path", e1);
        }
        // Create DB and load lists
        DataBaseManager databaseManager = getDataBaseManager();
        Cursor cursor = null;
        try {
            databaseManager.createDataBase();
            databaseManager.openReadableDB();

            skinsMap = new HashMap<Long, Skin>();
            countriesMap = new HashMap<Long, Country>();
            countriesList = new ArrayList<Country>();

            cursor = databaseManager.getSkins();
            while (cursor.moveToNext()) {
                Skin s = new Skin();
                s.setId(cursor.getInt(cursor.getColumnIndex("skin_id")));
                s.setName(cursor.getString(cursor.getColumnIndex("display_name")));
                s.setDefaultCountryId(cursor.getInt(cursor.getColumnIndex("defaultCountryId")));
                s.setDefaultCurrencyId(cursor.getInt(cursor.getColumnIndex("defaultCurrencyId")));
                s.setDefaultLanguageId(cursor.getInt(cursor.getColumnIndex("defaultLanguageId")));
                s.setSupportStartTime(cursor.getString(cursor.getColumnIndex("SUPPORT_START_TIME")));
                s.setSupportEndTime(cursor.getString(cursor.getColumnIndex("SUPPORT_END_TIME")));
                s.setSkinGroup(SkinGroup.getById(cursor.getLong(cursor.getColumnIndex("skin_group_id"))));
                s.setLocale(cursor.getString(cursor.getColumnIndex("locale")));
                s.setIsRegulated(cursor.getInt(cursor.getColumnIndex("IS_REGULATED")) != 0);
                skinsMap.put((long) s.getId(), s);
            }

			defaultSkinCurrency = AmountUtil.getDefaultCurrency();
			updateIsRegulated();
        } catch (Exception e) {
            Log.e(TAG, "Problem with DB creation/loading ", e);
        } finally {
            if (null != cursor) {
                cursor.close();
            }
            if (null != databaseManager) {
                databaseManager.close();
            }
        }
	}

	public void addPaymentMethod(long paymentTypeId, String paymentTypeName, String displayName,String smallLogo, String bigLogo, int typeId, Country country){
		PaymentMethod pm = new PaymentMethod();
		pm.setId(paymentTypeId);
		pm.setName(paymentTypeName);
		pm.setDisplayName(displayName);
		pm.setSmallLogo(smallLogo);
		pm.setBigLogo(bigLogo);
		pm.setTypeId(typeId);
		country.getPayments().add(pm);
	}

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public boolean isRegulated() {
		return isRegulated;
	}

	public Double getDefaultInvestAmount() {
		if (defaultInvestAmount == null) {
			defaultInvestAmount = DEFAULT_INVEST_AMOUNT;
		}

		return defaultInvestAmount;
	}

	public void setDefaultInvestAmount(Double defaultInvestAmount) {
		if (defaultInvestAmount == 0.0d) {
			// If the amount is 0.0, we should not update the value.
			return;
		}
		this.defaultInvestAmount = defaultInvestAmount;
	}

	public List<BalanceStepPredefValue> getPredefinedInvestAmounts() {
		return predefinedInvestAmounts;
	}

	public void setPredefinedInvestAmounts(List<BalanceStepPredefValue> predefinedInvestAmounts) {
		Collections.sort(predefinedInvestAmounts, new Comparator<BalanceStepPredefValue>() {

			@Override
			public int compare(BalanceStepPredefValue predefinedValue1,
					BalanceStepPredefValue predefinedValue2) {
				if (predefinedValue1.getPriority() > predefinedValue2.getPriority()) {
					return 1;
				} else if (predefinedValue1.getPriority() < predefinedValue2.getPriority()) {
					return -1;
				}

				return 0;
			}
		});

		this.predefinedInvestAmounts = predefinedInvestAmounts;
	}

	/**
	 * Returns user's currency if he's logged in or the default currency for the skin.
	 * 
	 * @return
	 */
	public Currency getCurrency() {
		if (isLoggedIn() && user.getCurrency() != null) {
			return user.getCurrency();
		} else {
			return getDefaultSkinCurrency();
		}
	}

	/**
	 * Returns the default currency for the skin.
	 * 
	 * @return
	 */
	public Currency getDefaultSkinCurrency() {
		return defaultSkinCurrency;
	}

	/**
	 * Change the Default Locale.
	 * @param locale
	 * @param toScreen The Screenable we want to navigate to after the change.
	 */
	public void changeLocale(String locale, Screenable toScreen) {
		Log.d(TAG, "CHANGING THE LANGUAGE OF THE APPLICATION!");
		String newLocale = getSupportedLanguageForLocale(locale);

		Configuration config = getBaseContext().getResources().getConfiguration();
		this.locale = new Locale(newLocale);
		Locale.setDefault(this.locale);
		config.locale = this.locale;
		changeLayoutDirection();
		getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
		getSharedPreferencesManager().putString(Constants.PREFERENCES_LOCALE, newLocale);
		setLastScreen(getCurrentScreen());
		postEvent(new LocaleChangeEvent(toScreen));
	}

	public String getSupportedLanguageForLocale(String locale){
		String supportedLang = null;
		if (getSupportedLanguages().contains(locale)) {
			supportedLang = locale;
		} else {
			supportedLang = getDefaultLanguage();
		}

		return supportedLang;
	}

	/**
	 * Returns the supported languages for this application
	 * 
	 * @return
	 */
	@SuppressWarnings("static-method")
	protected Set<String> getSupportedLanguages() {
		return LANGUAGES_SET;
	}

	/**
	 * Returns the default language for this application
	 * 
	 * @return
	 */
	@SuppressWarnings("static-method")
	protected String getDefaultLanguage() {
		return DEFAULT_LANGUAGE;
	}

	/**
	 * Change the Layout Direction according to the Locale.
	 * And post event to notify all application components that have layout directions. 
	 */
	private void changeLayoutDirection() {
		BidiFormatter bidiFormatter = BidiFormatter.getInstance(locale);
		LayoutDirection newLayoutDirection;
		if(bidiFormatter.isRtlContext()){
			newLayoutDirection = LayoutDirection.RIGHT_TO_LEFT;
		}else{
			newLayoutDirection = LayoutDirection.LEFT_TO_RIGHT;
		}
		
		if(this.layoutDirection != newLayoutDirection){
			this.layoutDirection = newLayoutDirection;
			postEvent(new LayoutDirectionChangeEvent(this.layoutDirection));			
		}
	}

	/**
	 * Returns the {@link LayoutDirection}.
	 * @return
	 */
	public LayoutDirection getLayoutDirection(){
		return this.layoutDirection;
	}
	
	/**
	 * Returns the {@link Locale}. 
	 * @return
	 */
	public Locale getLocale(){
		return this.locale;
	}
	
	/**
	 * Returns the current active {@link Activity}
	 */
	public synchronized BaseActivity getCurrentActivity() {
		return currentActivity;
	}

	/**
	 * Sets the current active {@link Activity}
	 * @param currentActivity The Activity.
	 */
	public synchronized void setCurrentActivity(BaseActivity currentActivity) {
		this.currentActivity = currentActivity;
		facebookManager.changeActivity(currentActivity);
	}
	
	//##########################################################################################
	//##################			EVENT BUS CALLBACKs			################################
	//##########################################################################################

	public void onEventMainThread(LoginLogoutEvent event){
		
		if(	event.getType() == com.anyoption.android.app.event.LoginLogoutEvent.Type.LOGIN || 
			event.getType() == com.anyoption.android.app.event.LoginLogoutEvent.Type.REGISTER){

			saveUser(event.getUserResult(), false);
			getSharedPreferencesManager().putBoolean(Constants.PREFERENCES_IS_INTROS_ON, true);

			//Save the "Show Cancel Investment Popup" settings:
			setShowCancelInvestmentPopup(user.isShowCancelInvestment());

			if (user.getDefaultAmountValue() != null) {
				setDefaultInvestAmount(AmountUtil.getDoubleAmount(user.getDefaultAmountValue()));
			}
			
			if(user.getPredefValues() != null) {
				setPredefinedInvestAmounts(user.getPredefValues());
			}

			updateIsRegulated();

			getSharedPreferencesManager().putBoolean(Constants.PREFERENCES_IS_LOGGED_ATLEAST_ONCE, true);
			getSharedPreferencesManager().putBoolean(Constants.PREFERENCES_IS_FIRST_LOGIN, true);
		}else if(event.getType() == com.anyoption.android.app.event.LoginLogoutEvent.Type.LOGOUT){
			
			getCommunicationManager().cancelAllRequests();
			logoutUser();
			requestLogoutPredefinedAmounts();
		}
	}

	public void onEventMainThread(NavigationEvent event) {
		if (event.getToScreen() == Screen.ASSET_PAGE) {
			// This screen is always part of other screen.
			return;
		}

		setCurrentScreen(event.getToScreen());
		setLastBundle(new Bundle(event.getBundle()));
	}

	public void onEventMainThread(LanguageChangeEvent event){
		String newLanguageCode = event.getLanguageCode();
		
		String[] stringArray = newLanguageCode.split("_");
		newLocale = stringArray[0];
		long skinId = Long.parseLong(stringArray[1]);

		setSkinId(skinId);
		getSharedPreferencesManager().putString(Constants.PREFERENCES_LANGUAGE_CODE, newLanguageCode);

		updateSkinFromServer();
	}

	public void onEventMainThread(InvestmentEvent event) {
		Investment investment = event.getInvestment();
		
		//Refresh the Investments list:
		if (event.getType() == Type.INVEST_SUCCESS) {
			investments.add(investment);
		} else if (event.getType() == Type.INVESTMENT_SOLD) {
			// Find the position of the investment.
			int investmentPosition = -1;

			for (int i = 0; i < investments.size(); i++) {
				if (investments.get(i).getId() == investment.getId()) {
					investmentPosition = i;
					break;
				}
			}

			if (investmentPosition >= 0) {
				investments.remove(investmentPosition);
			}
		}
	}

	//##########################################################################################

	private void updateSkinFromServer(){

		CheckUpdateMethodRequest request = new CheckUpdateMethodRequest();
		request.setApkVersion(getApplicationVersionCode());
		request.setApkName(getString(R.string.apkName));
		request.setCombinationId(Utils.getCombinationId());
		request.setDynamicParam(Utils.getDynamicParam());
		request.setAffSub1(Utils.getAffSub1(this));
		request.setAffSub2(Utils.getAffSub2(this));
		request.setAffSub3(Utils.getGclid(this));
		request.setmId(getSharedPreferencesManager().getString(Constants.PREFERENCES_MID, null));
		request.setEtsMId(getSharedPreferencesManager().getString(Constants.PREFERENCES_ETS_MID, null));
		request.setFirstOpening(Utils.checkFirstOpening());
		request.setOsTypeId(1);
		request.setAppsflyerId(getAppsFlyerManager().getAppsFlyerUID());
		request.setDeviceFamily(ScreenUtils.isTablet() ? "1" : "0");

		request.setAdvertisingId(Utils.getAndroidAdvertisingID());

		getCommunicationManager().requestService(this, "updateSkinFromServerCallback", Constants.SERVICE_SKIN_UPDATE, request, UpdateMethodResult.class, false, false);
	}

	public void updateSkinFromServerCallback(Object resObj){
		UpdateMethodResult result = (UpdateMethodResult) resObj;
		if(result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS){
			Log.d(TAG, "updateSkinFromServerCallback -> OK");
			setSkinId(result.getSkinId());
		}

		if(!this.locale.getLanguage().equals(newLocale)){
			changeLocale(newLocale, null);
		}
	}

	private void logoutUser() {
		sharedPreferencesManager.putString(Constants.PREFERENCES_PASSWORD_LOGIN, null);

		if(!getCommunicationManager().requestService(this, "logoutUserCallback", Constants.SERVICE_LOGOUT, new UserMethodRequest(), MethodResult.class, false, false)){
			removeSavedUser();
			goToAfterLogoutScreen();
		}
	}
	
	public void logoutUserCallback(Object result){
		if(result != null && result instanceof MethodResult && ((MethodResult) result).getErrorCode() == Constants.ERROR_CODE_SUCCESS){
			Log.d(TAG, "User was successfully logged out on the Server");
		}else{
			Log.e(TAG, "Could NOT logged out the user on the Server!!!");
		}
		removeSavedUser();
		goToAfterLogoutScreen();
	}
	
	protected void removeSavedUser(){
		Log.d(TAG, "Removing saved user");
		setUser(null);
		setUserRegulation(null);
	}
	
	protected void goToAfterLogoutScreen(){
		postEvent(new NavigationEvent(Screen.LOGIN, NavigationType.INIT));
	}
	
	/**
	 * Gets the current {@link Screenable}.
	 * @return
	 */
	public Screenable getCurrentScreen() {
		return currentScreen;
	}

	/**
	 * Sets the current {@link Screenable}.
	 * @param currentScreen
	 */
	public void setCurrentScreen(Screenable currentScreen) {
		this.currentScreen = currentScreen;
	}

	public ArrayList<ArrayList<InsuranceItem>> getTakeProfitGroups() {
		return takeProfitGroups;
	}

	public void setTakeProfitGroups(ArrayList<ArrayList<InsuranceItem>> takeProfitGroups) {
		this.takeProfitGroups = takeProfitGroups;
	}

	public ArrayList<ArrayList<InsuranceItem>> getRollForwardGroups() {
		return rollForwardGroups;
	}

	public void setRollForwardGroups(ArrayList<ArrayList<InsuranceItem>> rollForwardGroups) {
		this.rollForwardGroups = rollForwardGroups;
	}

	/**
	 * Saves the user.
	 * 
	 * @param result The response object from a "Get User Request".
	 */
	public void saveUser(UserMethodResult result, boolean shouldChangeLocale){
		
		//Save to the instance variables:
		setUser(result.getUser());
		if (this.user != null && user.getSkinId() != this.skinId) {
		
			setSkinId(user.getSkinId());//(checkIfChineseOrKorean(user.getSkinId()));

			sharedPreferencesManager.putLong(Constants.PREFERENCES_SKINID, this.skinId);
			if(shouldChangeLocale){
				if(!this.locale.getLanguage().equals(result.getUser().getLocale().getLanguage())){
					changeLocale(user.getLocale().getLanguage(), null);									
				}
			}
		}
		setUserRegulation(result.getUserRegulation());
		
		//Save in SharedPreferences:
		String userNameSP = getSharedPreferencesManager().getString(Constants.PREFERENCES_USER_NAME, null);
		String userName = result.getUser().getUserName();
		String pass = result.getUser().getEncryptedPassword();
		//Check if the user is saved in the Shared Preferences (no need to save him again):
		if (userNameSP == null || (userNameSP != null && !userNameSP.equalsIgnoreCase(userName))) {
		    getSharedPreferencesManager().putString(Constants.PREFERENCES_USER_NAME, userName);
		    getSharedPreferencesManager().putString(Constants.PREFERENCES_PASSWORD, pass);
		}
		if (user.getDefaultAmountValue() != null) {
			setDefaultInvestAmount(AmountUtil.getDoubleAmount(user.getDefaultAmountValue()));
		}
		if(user.getPredefValues() != null) {
			setPredefinedInvestAmounts(user.getPredefValues());
		}
	}

	/**
	 * Saves the user without changing the locale
	 * 
	 * @param result
	 *            The response object from a {@link #getServiceMethodGetUser()} request.
	 */
	public void saveUser(UserMethodResult result) {
		saveUser(result, false);
	}

	/**
	 * Saves the user.
	 */
	public void saveUser(User user, UserRegulationBase userRegulations){
		
		//Save to the instance variables:
		setUser(user);
		if (this.user != null && this.user.getSkinId() != this.skinId) {
		
			setSkinId(this.user.getSkinId());//(checkIfChineseOrKorean(this.user.getSkinId()));

		}
		setUserRegulation(userRegulations);
		
		//Save in SharedPreferences:
		String userNameSP = getSharedPreferencesManager().getString(Constants.PREFERENCES_USER_NAME, null);
		String userName = user.getUserName();
		String pass = user.getEncryptedPassword();
		//Check if the user is saved in the Shared Preferences (no need to save him again):
		if (userNameSP == null || (userNameSP != null && !userNameSP.equalsIgnoreCase(userName))) {
		    getSharedPreferencesManager().putString(Constants.PREFERENCES_USER_NAME, userName);
		    getSharedPreferencesManager().putString(Constants.PREFERENCES_PASSWORD, pass);
		}
		if (this.user.getDefaultAmountValue() != null) {
			setDefaultInvestAmount(AmountUtil.getDoubleAmount(this.user.getDefaultAmountValue()));
		}
		if(this.user.getPredefValues() != null) {
			setPredefinedInvestAmounts(this.user.getPredefValues());
		}
	}
	
	
	public Screenable getLastScreen() {
		return lastScreen;
	}

	public void setLastScreen(Screenable lastScreen) {
		this.lastScreen = lastScreen;
	}

	public Bundle getLastBundle() {
		return lastBundle;
	}

	public void setLastBundle(Bundle lastBundle) {
		this.lastBundle = lastBundle;
	}

	public Country[] getCountriesArray() {
		return countriesArray;
	}

	public void setCountriesArray(Country[] countriesArray) {
		this.countriesArray = countriesArray;
	}

	public void recreateLaunchActivity(Activity activity){
		Log.d(TAG, "Recreate "+ activity.getClass().getSimpleName());
		Intent intent = activity.getIntent();
		intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

		Bundle bundle = new Bundle();
		setupRestartIntent(intent, bundle, null, false);
		intent.putExtras(bundle);
		intent.putExtra(Constants.EXTRA_IS_ACTIVITY_RECREATED, true);

		activity.finish();
		activity.startActivity(intent);
//		activity.recreate();
	}

	public void restartApplication(Screenable screen, boolean shouldGoToMain) {
		try {
			PackageManager packageManager = getPackageManager();
			if (packageManager != null) {
				Intent launchIntent = packageManager.getLaunchIntentForPackage(getPackageName());
				if (launchIntent != null) {
					launchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					
					// We use an AlarmManager to open the Application in 100ms:
					int pendingIntentId = 223344;
					PendingIntent pendingIntent;
					if (Build.VERSION.SDK_INT >= 21 && shouldGoToMain) {
						pendingIntent = PendingIntent.getActivity(this, pendingIntentId, launchIntent,PendingIntent.FLAG_CANCEL_CURRENT|PendingIntent.FLAG_ONE_SHOT);
					} else {
						pendingIntent = PendingIntent.getActivity(this, pendingIntentId, launchIntent,PendingIntent.FLAG_CANCEL_CURRENT);
					}
					//Put the logged user in order to continue the current session:
					Intent intent = new Intent();
					Bundle bundle = new Bundle();
					setupRestartIntent(intent, bundle, screen, shouldGoToMain);
					intent.putExtras(bundle);
					pendingIntent.send(this, 111, intent);

					getCurrentActivity().finish();
					getCurrentActivity().overridePendingTransition(0,0);
					getSharedPreferencesManager().putString(Constants.PREFERENCES_RESTARTED_APP_USER_SESSION_ID, getSessionId());

					AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
					alarmManager.set(AlarmManager.RTC, System.currentTimeMillis() + 100, pendingIntent);

					System.exit(0);
				} else {
					Log.e(TAG, "Was not able to restart application, launch Activity null");
				}
			} else {
				Log.e(TAG, "Was not able to restart application, Package Manager null");
			}
		} catch (Exception ex) {
			Log.e(TAG, "Was not able to restart application", ex);
		}
	}

	protected void setupRestartIntent(Intent intent, Bundle bundle, Screenable screen, boolean shouldGoToMain){
		Log.d(TAG, "Setting up the restart Intent...");
		Log.d(TAG, "User = " + user);
		intent.putExtra(Constants.EXTRA_IS_APP_RESTARTED, true);
		bundle.putSerializable(Constants.EXTRA_USER, user);
		bundle.putSerializable(Constants.EXTRA_USER_REGULATIONS, userRegulation);
		intent.putExtra(Constants.EXTRA_SCREEN, screen);
		intent.putExtra(Constants.EXTRA_MIN_INV_AMOUNT, getMinInvestmentAmount());
		intent.putExtra(Constants.EXTRA_BUNDLE, (getLastBundle() != null) ? new Bundle(getLastBundle()) : new Bundle());
		intent.putExtra(Constants.EXTRA_GO_TO_MAIN, shouldGoToMain);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
	}
	
	public Map<Long, Country> getCountriesMap() {
		return countriesMap;
	}

	public void setCountriesMap(Map<Long, Country> countriesMap) {
		this.countriesMap = countriesMap;
	}

	/**
	 * Gets the Markets from the server and cached them for the current session.
	 */
	public void loadMarketsFromServer(){
		isMarketsLoaded = false;
		getMarketGroupsFromServer();
	}

	/**
	 * Gets the Markets from the server and cached them for the current session.
	 */
	private void getMarketGroupsFromServer(){
		Log.d(TAG, "Getting Markets from server.");
		if(!isMarketsLoaded){
			if(!getCommunicationManager().requestService(this, "gotMarketGroups", Constants.SERVICE_GET_MARKET_GROUPS, new MethodRequest(), MarketsGroupsMethodResult.class, false, false)){
				isMarketsLoaded = false;
			}			
		}
	}	
	
	/**
	 * Callback method for the "getMarketGroups" request.
	 * @param result
	 */
    public void gotMarketGroups(Object result) {
    	if(result != null && result instanceof MarketsGroupsMethodResult && ((MethodResult) result).getErrorCode() == Constants.ERROR_CODE_SUCCESS){
    		Log.d(TAG, "Got Market Groups from the server.");
    		MarketsGroupsMethodResult marketsGroupsResult = (MarketsGroupsMethodResult) result;
    		
    		for(int i=0; i < marketsGroupsResult.getGroups().length; i++){
    			MarketGroup marketGroup = marketsGroupsResult.getGroups()[i];
   				for(Market market : marketGroup.getMarkets()){
   					if(market.getGroupId() == null){
   						market.setGroupId(marketGroup.getId());//If not added on the server side   						
   					}
					//Check if the Decimal point is not set:
					if(market.getDecimalPoint() == null){
						market.setDecimalPoint(5L);
					}

   					market.setGroupName(marketGroup.getName());
   				}
     		}
    		
    		setMarketGroups(marketsGroupsResult.getGroups());
    		isMarketsLoaded = true;
    	}else{
			Log.e(TAG, "Could NOT get the Market Groups from the server.");
    		isMarketsLoaded = false;
    		getMarketGroupsFromServer();
    	}
    }

	public void setMarketGroups(MarketGroup[] marketGroups) {
		this.marketGroups = marketGroups;		
	}
	
	public MarketGroup[] getMarketGroups() {
		return this.marketGroups;		
	}
	
	public List<Investment> getInvestments(){
		return investments;
	}
	
	public void setInvestments(List<Investment> investments){
		this.investments = investments;
	}
	
	public List<Investment> getInvestmentsForOpportunity(long opportunityId){
		List<Investment> invList = null;
		for(Investment investment : investments){
			if(investment.getOpportunityId() == opportunityId){
				if(invList == null){
					invList = new ArrayList<Investment>();
				}
				invList.add(investment);
			}
		}
		return invList;
	}
	
	/**
	 * Returns the Market for the given ID.
	 * It searches in the cashed markets.
	 * @param id
	 * @return
	 */
	public Market getMarketForID(long id){
		Market result = null;
		for(MarketGroup group : getMarketGroups()){
			for(Market market : group.getMarkets()){
				if(id == market.getId()){
					result = market;
					break;
				}
			}
		}
		
		return result;
	}

	/**
	 * Requests predefined amounts for not logged in user.
	 */
	public void requestLogoutPredefinedAmounts() {
		communicationManager.requestService(this, "logoutAmountsCallback",
				Constants.SERVICE_GET_LOGOUT_BALANCE_STEP, new MethodRequest(),
				LogoutBalanceStepMethodResult.class, false, false);
	}

	public void logoutAmountsCallback(Object resultObj) {
		LogoutBalanceStepMethodResult result = (LogoutBalanceStepMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Got predefined amounts!");	
			setDefaultInvestAmount(AmountUtil.getDoubleAmount(result.getDefaultAmountValue()));
			setPredefinedInvestAmounts(result.getPredefValues());
		} else {
			setDefaultInvestAmount(DEFAULT_INVEST_AMOUNT);

			Log.e(TAG, "Could not get predefined amounts! ErrorCode=" + ((result != null) ? result.getErrorCode() : ""));

			if(result != null) {
				Log.e(TAG, "Could not get predefined amounts! ErrorCode=" + result.getErrorCode());	
			} else {
				Log.e(TAG, "Could not get predefined amounts! result == null");
			}

		}
	}
	
	/**
	 * Returns a List with all Markets that have Open Options.
	 * @return
	 */
	public List<Market> getMarketWithOpenOptions(){
		List<Market> marketsWithOpenOptions = new ArrayList<Market>();
		
		for(Investment investment : investments){
			long marketID = investment.getMarketId();
			boolean isAdded = false;
			for(Market market : marketsWithOpenOptions){
				if(marketID == market.getId()){
					isAdded = true;
				}
			}

			if(!isAdded){
				Market newMarket = getMarketForID(marketID);
				marketsWithOpenOptions.add(newMarket);
			}
		}
		
		return marketsWithOpenOptions;
	}
	
	
	/**
	 * Sends a Deposit Receipt request to the server to send email to the user.
	 */
	public void sendReceiptDeposit(long transactionID) {
    	DepositReceiptMethodRequest request = new DepositReceiptMethodRequest();
    	request.setTransactionId(transactionID);
    	getCommunicationManager().requestService(this, "sendReceiptDepositCallback", Constants.SERVICE_SEND_DEPOSIT_RECEIPT, request, MethodResult.class, false, false);
    }
	
	public void sendReceiptDepositCallback(Object result){}
	
	
	//#################################################################################################################
	//############################			Activities LifeCycle			 ##########################################
	//#################################################################################################################
	@Override
	public void onActivitySaveInstanceState(Activity activity, Bundle outState) {}
	@Override
	public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
		Log.d(TAG, "onActivityCreated " + activity.getClass().getSimpleName());
		if(activity instanceof BaseActivity){
			++numberOfCreatedActivities;			
		}
	}
	@Override
	public void onActivityDestroyed(Activity activity) {
		Log.d(TAG, "onActivityDestroyed " + activity.getClass().getSimpleName());
		if(activity instanceof BaseActivity){
			++numberOfDestroyedActivities;
			
			if(!isApplicationRunning()){
				//Remove all notifications:
				getNotificationManager().clearAllNotifications();							
			}
		}
	}
	@Override
	public void onActivityPaused(Activity activity) {
		Log.d(TAG, "onActivityPaused " + activity.getClass().getSimpleName());
		if(activity instanceof BaseActivity){
			++numberOfPausedActivities;			
		}
	}
	@Override
	public void onActivityResumed(Activity activity) {
		Log.d(TAG, "onActivityResumed " + activity.getClass().getSimpleName());
		if(activity instanceof BaseActivity){
			++numberOfResumedActivities;
			
			//Show the notifications (if they were hidden on app paused):
			getNotificationManager().showAllNotifications();			
		}
	}
	@Override
	public void onActivityStarted(Activity activity) {
		Log.d(TAG, "onActivityStarted " + activity.getClass().getSimpleName());
		if(activity instanceof BaseActivity){
			++numberOfStartedActivities;			
		}
	}
	@Override
	public void onActivityStopped(Activity activity) {
		Log.d(TAG, "onActivityStopped " + activity.getClass().getSimpleName());
		if(activity instanceof BaseActivity){
			++numberOfStoppedActivities;

			if(!isApplicationVisible()){
				//Hide the notifications as there is no need to be shown:
				Log.d(TAG, "Application is Not visible");
				getSharedPreferencesManager().putBoolean(Constants.PREFERENCES_IS_CAME_FROM_MARKETING, false);
				getNotificationManager().hideAllNotifications();
			}
		}
	}
	
	/**
	 * Checks whether the Application is running.
	 * @return
	 */
	public boolean isApplicationRunning(){
		if(numberOfCreatedActivities > numberOfDestroyedActivities){
			//The Application is running:
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Checks whether the Application is visible to the user.
	 * @return
	 */
	public boolean isApplicationVisible(){
		if(numberOfStartedActivities > numberOfStoppedActivities){
			//The Application is visible:
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Checks whether the Application is in the Foreground (is visible and the user can interact with it).
	 * @return
	 */
	public boolean isApplicationInForeground(){
		if(numberOfResumedActivities > numberOfPausedActivities){
			//The Application is visible and is in the Foreground:
			return true;
		}else{
			return false;
		}
	}
	//#################################################################################################################

	public EditText getCurrentEditTextOnFocus() {
		return currentEditTextOnFocus;
	}

	public void setCurrentEditTextOnFocus(EditText currentEditTextOnFocus) {
		this.currentEditTextOnFocus = currentEditTextOnFocus;
	}

	/**
	 * Creates Home shortcut icon.
	 */
	public void createDesktopShortcut(){
		//Remove the shortcut if there is already one:
		removeDesktopShortcut();

        Intent shortcutIntent = new Intent(getApplicationContext(), ShortcutActivity.class);
        shortcutIntent.setAction(Intent.ACTION_MAIN);
        
        Intent intent = new Intent();
        intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);

        intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.appName));
        intent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(getApplicationContext(), R.drawable.app_icon));

        intent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        sendBroadcast(intent);
	}

	/**
	 * Removes the Home shortcut icon.
	 */
	public void removeDesktopShortcut(){
	
		Intent shortcutIntent = new Intent(getApplicationContext(), ShortcutActivity.class);
		shortcutIntent.setAction(Intent.ACTION_MAIN);

        Intent intent = new Intent();
        intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);

        intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.appName));
        intent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(getApplicationContext(), R.drawable.app_icon));

        intent.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
        sendBroadcast(intent);
	}

	/**
	 * Checks if there is a class that extends the given class. If no subclass is found it returns the given class.
	 * <p><b>NOTE:</b> The subclass package must follow the library project pattern (ignoring the package name of the project):
	 * <p>Example : The sub class of <b>lib_package_name.activity.MyClass</b> should be <b>project_package_name.activity.SubMyClass</b>
	 * <p><b>NOTE:</b> The extended class must have a prefix defined in the {@link getSubclassPrefix}
	 * <p>Example : AOMainActivity
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Class getSubClassForClass(Class clazz) {
		// Check for extended class:
	    String extClassName = clazz.getName().replace("com.anyoption.android.app", getPackageName()).replace(clazz.getSimpleName(), "") + getSubclassPrefix() + clazz.getSimpleName();
	    try {
	        Class extClass = Class.forName(extClassName);
	        return extClass;
	    } catch (ClassNotFoundException e) {
	        Log.d(TAG, "Could not find class " + extClassName);
	    }catch (Exception e) {
	    	Log.d(TAG, "Could not find class " + extClassName);
		}
	    // Extended class is not found return base class:
	    return clazz;
	}
	
	/**
	 * Override point. Specify the prefix that all subclasses (for Activities, Fragments etc) 
	 * which want to extend or override functionalities in their parents must have in their names.
	 * 
	 * @return
	 */
	protected abstract String getSubclassPrefix();
	
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		for(OnActivityResultListener listener : onActivityResultListeners){
			listener.onActivityResultEvent(requestCode, resultCode, intent);
		}
	}
	
	public void addOnActivityResultListener(OnActivityResultListener onActivityResultListener){
		onActivityResultListeners.add(onActivityResultListener);
	}
	
	public void removeOnActivityResultListener(OnActivityResultListener onActivityResultListener){
		onActivityResultListeners.remove(onActivityResultListener);
	}

	public Screenable getHomeScreen() {
		if (isTablet) {
			return Screen.TABLET_TRADE;
		} else {
			return Screen.TRADE;
		}
	}

	public long getMinInvestmentAmount() {
		return minInvestmentAmount;
	}

	public void setMinInvestmentAmount(long minInvestmentAmount) {
		this.minInvestmentAmount = minInvestmentAmount;
	}

	public boolean isAnyoption() {
		return Application.get().getClass().getSimpleName().equals("AOApplication");
	}

	public boolean isCopyop() {
		return Application.get().getClass().getSimpleName().equals("COApplication");
	}

	public boolean isEtrader() {
		return Application.get().getClass().getSimpleName().equals("ETApplication");
	}

	public boolean isBDA() {
		return Application.get().getClass().getSimpleName().equals("BUApplication");
	}

	public boolean isTablet() {
		return isTablet;
	}

	public boolean isHasDynamicsLogout() {
		return getSharedPreferencesManager().getBoolean(Constants.PREFERENCES_IS_DYNAMICS_IN_LOGOUT, true);
	}

	public void setIsHasDynamisLogout(boolean isHasDynamicsLogout){
		Log.d(TAG, "Set if has Dynamics for Logout State = " + isHasDynamicsLogout);
		getSharedPreferencesManager().putBoolean(Constants.PREFERENCES_IS_DYNAMICS_IN_LOGOUT, isHasDynamicsLogout);
	}

	/**
	 * Check if the Dynamics Markets are allowed to be shown.
	 */
	public boolean isHasDynamics(){
		if(getSkinId() == Skin.SKIN_ETRADER_INT){
			return false;
		}else{
			if(isLoggedIn()){
				return getUser().getProductViewFlags().get(ConstantsBase.HAS_DYNAMICS_FLAG);
			}else{
				return isHasDynamicsLogout();
			}
		}
	}

	public boolean isBgTest() {
		if(getServiceURL().toLowerCase().contains("test") || getLSURL().toLowerCase().contains("test")) {
			return true;
		}
		return false;
	}

	public void setShowCancelInvestmentPopup(boolean isShowCancelInvestmentPopup){
		this.isShowCancelInvestmentPopup = isShowCancelInvestmentPopup;
	}

	public boolean isShowCancelInvestmentPopup(){
		return isShowCancelInvestmentPopup;
	}

}
