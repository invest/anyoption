package com.anyoption.android.app.popup;

import com.anyoption.android.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.form.FormRadioButton;
import com.anyoption.android.app.widget.form.FormDocumentsUploadItem.OnDocumentTypeSelectedListener;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class DocumentsProofOfIdDialogFragment extends OkDialogFragment {

	public static final String TAG = DocumentsProofOfIdDialogFragment.class.getSimpleName();

	private OnPopupDocumentTypeSelectedListener popupDocumentTypeSelectedListener;

	private FormRadioButton firstChoice;
	private FormRadioButton secondChoice;
	private FormRadioButton thirdChoice;
	private static long chosenDocumentType = 0l;

	public void setChosenDocumentType(long chosenDocumentType) {
	this.chosenDocumentType = chosenDocumentType;
	}

	public long getChosenDocumentType() {
		return chosenDocumentType;
	}

	public interface OnPopupDocumentTypeSelectedListener {
		void onPopupDocumentTypeSelected(long documentType);
	}

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static DocumentsProofOfIdDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new DocumentsProofOfIdDialogFragment.");
		DocumentsProofOfIdDialogFragment fragment = new DocumentsProofOfIdDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);

		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if (args != null) {

		}
	}

	/**
	 * Set up content.
	 * 
	 * @param contentView
	 */
	@Override
	protected void onCreateContentView(View contentView) {

		firstChoice = (FormRadioButton) contentView.findViewById(R.id.documents_proof_of_id_popup_first_choice);

		secondChoice = (FormRadioButton) contentView.findViewById(R.id.documents_proof_of_id_popup_second_choice);

		thirdChoice = (FormRadioButton) contentView.findViewById(R.id.documents_proof_of_id_popup_third_choice);

		setChosenDocumentTypeUI();

		View uploadButton = contentView.findViewById(R.id.upload_button);
		uploadButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (firstChoice.isChecked()) {
					popupDocumentTypeSelectedListener.onPopupDocumentTypeSelected(Constants.UPLOAD_DOCUMENTS_TYPE_PROOF_OF_ID_PASSPORT);
				} else if (secondChoice.isChecked()) {
					popupDocumentTypeSelectedListener.onPopupDocumentTypeSelected(Constants.UPLOAD_DOCUMENTS_TYPE_PROOF_OF_ID_ID);
				} else if (thirdChoice.isChecked()) {
					popupDocumentTypeSelectedListener.onPopupDocumentTypeSelected(Constants.UPLOAD_DOCUMENTS_TYPE_PROOF_OF_ID_DRIVER_LICENSE);
				}
				dismiss();
			}
		});
	}

	@Override
	public void dismiss() {
		setChosenDocumentTypeUI();
		super.dismiss();
	}

	private void setChosenDocumentTypeUI() {
		if(firstChoice != null && secondChoice != null && thirdChoice != null) {
			if (getChosenDocumentType() == Constants.UPLOAD_DOCUMENTS_TYPE_PROOF_OF_ID_PASSPORT) {
				firstChoice.setChecked(true);
				secondChoice.setChecked(false);
				thirdChoice.setChecked(false);
			} else if (getChosenDocumentType() == Constants.UPLOAD_DOCUMENTS_TYPE_PROOF_OF_ID_ID) {
				firstChoice.setChecked(false);
				secondChoice.setChecked(true);
				thirdChoice.setChecked(false);
			} else if (getChosenDocumentType() == Constants.UPLOAD_DOCUMENTS_TYPE_PROOF_OF_ID_DRIVER_LICENSE) {
				firstChoice.setChecked(false);
				secondChoice.setChecked(false);
				thirdChoice.setChecked(true);
			}
		}
	}

	public void setPopupDocumentTypeSelectedListener(OnPopupDocumentTypeSelectedListener popupDocumentTypeSelectedListener) {
		this.popupDocumentTypeSelectedListener = popupDocumentTypeSelectedListener;
	}

	
	@Override
	protected int getContentLayout() {
		return R.layout.popup_documents_proof_of_id;
	}

}
