package com.anyoption.android.app.util;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Opportunity;

import android.util.Log;

public class InvestmentUtil {

	private static final String TAG = InvestmentUtil.class.getSimpleName();

	/**
	 * Gets the investments return for the given level.
	 *
	 * @param investment
	 * @param currentLevel
	 * @return the return amount. The cents are after the decimal point.
	 */
	public static double getOpenInvestmentReturn(Investment investment, double currentLevel) {

		double amount = 0;

		if(investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL || investment.getTypeId() == Investment.INVESTMENT_TYPE_PUT){
			double investmentLevel = Double.parseDouble(investment.getLevel().replaceAll(",", "").replaceAll("\\s", ""));
			if ((investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL && currentLevel > investmentLevel) || (investment.getTypeId() == Investment.INVESTMENT_TYPE_PUT && currentLevel < investmentLevel)) {
				// oddsWin and oddsLose are in different format when we get them with getChartData and
				// insertInvestment. This fixes the problem
				double oddsWin = investment.getOddsWin();
				if (oddsWin < 1) {
					oddsWin = oddsWin + 1;
				}
				// Win
				amount = (investment.getAmount() - investment.getInsuranceAmountRU() - investment.getOptionPlusFee()) * oddsWin;
			} else if ((investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL && currentLevel <= investmentLevel)|| (investment.getTypeId() == Investment.INVESTMENT_TYPE_PUT && currentLevel >= investmentLevel)) {
				// oddsWin and oddsLose are in different format when we get them with getChartData and
				// insertInvestment. This fixes the problem
				double oddsLose = investment.getOddsLose();
				if (investment.getOddsWin() < 1) {
					oddsLose = 1 - oddsLose;
				}
				// Lose
				amount = (investment.getAmount() - investment.getInsuranceAmountRU() - investment.getOptionPlusFee()) * oddsLose;
			}

			// The amount's cents are before the decimal point because we've calculated the amount from
			// long values. This is fixed applying devision to 100.
			amount =  amount / 100;

		} else if(investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY || investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_SELL){
			amount = getDynamicsOpenInvestmentReturnAmount(investment);
		} else if(investment.getTypeId() == Investment.INVESTMENT_TYPE_BUBBLES){
			amount = InvestmentUtil.getBubblesOpenInvestmentReturnAmount(investment);
		}

		return amount;
	}

	public static double getDynamicsOpenInvestmentReturnAmount(Investment investment){
		double profitPercent = InvestmentUtil.getReturnProfitPercent(investment);
		return AmountUtil.getDoubleAmount(investment.getAmount()) * (1 + profitPercent / 100);
	}

	public static double getBubblesOpenInvestmentReturnAmount(Investment investment){
		double profitPercent = investment.getOddsWin();
		return AmountUtil.getDoubleAmount((long) (investment.getAmount() * (1 + investment.getOddsWin())));
	}

	/**
	 * Returns true if the user wins for the given level, or false if he loses or the level is the
	 * same as investment level.
	 */
	public static boolean isOpenInvestmentProfitable(Investment investment, double currentLevel) {
		double investmentLevel = Double.parseDouble(investment.getLevel().replaceAll(",", "").replaceAll("\\s", ""));

		if (	(investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL && currentLevel > investmentLevel) ||
				(investment.getTypeId() == Investment.INVESTMENT_TYPE_PUT && currentLevel < investmentLevel)  ||
				(investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY && currentLevel > investment.getEventLevel()) ||
				(investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_SELL && currentLevel < investment.getEventLevel())) {
			return true;
		} else {
			// Lose or get the invested amount, if the level is the same.
			return false;
		}
	}

	/**
	 * Returns true if the user wins for the given level, or false if he loses or the level is the
	 * same as investment level.
	 */
	public static boolean isSettledInvestmentProfitable(Investment investment, Currency currency) {
		long returnAmount = -1;

		try {
			returnAmount = AmountUtil.parseAmountToLong(investment.getAmountReturnWF(), currency);
		} catch (Exception e) {
			Log.d(TAG, "Could not parse amount: " + investment.getAmountReturnWF());
		}

		if (returnAmount != -1) {
			return returnAmount > investment.getAmount();
		} else {
			double expiryLevel = Double.parseDouble(investment.getExpiryLevel().replaceAll(",", ""));
			return isOpenInvestmentProfitable(investment, expiryLevel);
		}
	}

	/**
	 * Finds the call/put arrow icon for an investment.
	 */
	public static int getInvestmentIcon(Investment investment, double currentLevel) {
		if (investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL) {
			if (InvestmentUtil.isOpenInvestmentProfitable(investment, currentLevel)) {
				return R.drawable.arrow_call_win;
			} else {
				return R.drawable.arrow_call_lose;
			}
		} else if (investment.getTypeId() == Investment.INVESTMENT_TYPE_PUT) {
			if (InvestmentUtil.isOpenInvestmentProfitable(investment, currentLevel)) {
				return R.drawable.arrow_put_win;
			} else {
				return R.drawable.arrow_put_lose;
			}
		}

		return 0;
	}

	/**
	 * Finds the call/put arrow icon for an investment.
	 */
	public static int getSettledInvestmentIcon(Investment investment, Currency currency) {
		if (investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL) {
			if (InvestmentUtil.isSettledInvestmentProfitable(investment, currency)) {
				return R.drawable.arrow_call_win;
			} else {
				return R.drawable.arrow_call_lose;
			}
		} else if (investment.getTypeId() == Investment.INVESTMENT_TYPE_PUT) {
			if (InvestmentUtil.isSettledInvestmentProfitable(investment, currency)) {
				return R.drawable.triangle_small_down_yellow;
			} else {
				return R.drawable.triangle_small_down_grey;
			}
		}

		return R.drawable.transparent_selector;
	}

	/**
	 * Changes time created date to be with correct offset. The investment's dates are received
	 * without any offset.<br/>
	 * <b>NOTE:</b> This method must be called only once for an object. Otherwise the date will be
	 * incorrect again.
	 *
	 * @param investments
	 */
	public static void fixDate(Investment... investments) {
		int utcOffset = TimeUtils.getUtcOffset();
		for (int i = 0; i < investments.length; i++) {
			TimeUtils.fixTime(investments[i].getTimeCreated(), utcOffset);
			TimeUtils.fixTime(investments[i].getTimeEstClosing(), utcOffset);
			if (investments[i].getTimeSettled() != null) {
				TimeUtils.fixTime(investments[i].getTimeSettled(), utcOffset);
			}
			if(investments[i].getBubbleStartTime() != null){
				TimeUtils.fixTime(investments[i].getBubbleStartTime(), utcOffset);
			}
			if(investments[i].getBubbleEndTime() != null){
				TimeUtils.fixTime(investments[i].getBubbleEndTime(), utcOffset);
			}
		}
	}

	/**
	 * Creates new investment object with the same field values.
	 *
	 * @param investment
	 * @return
	 */
	public static Investment cloneInvestment(Investment investment) {
		Investment newInvestment = new Investment();
		newInvestment.setAdditionalInfoText(investment.getAdditionalInfoText());
		newInvestment.setAdditionalInfoType(investment.getAdditionalInfoType());
		newInvestment.setAmount(investment.getAmount());
		newInvestment.setAmountReturnWF(investment.getAmountReturnWF());
		newInvestment.setAmountTxt(investment.getAmountTxt());
		newInvestment.setApiExternalUserId(investment.getApiExternalUserId());
		newInvestment.setAsset(investment.getAsset());
		newInvestment.setBonusDisplayName(investment.getBonusDisplayName());
		newInvestment.setChartXPosition(investment.getChartXPosition());
		newInvestment.setCurrentLevel(investment.getCurrentLevel());
		newInvestment.setCurrentLevelTxt(investment.getCurrentLevelTxt());
		newInvestment.setExpiryLevel(investment.getExpiryLevel());
		newInvestment.setId(investment.getId());
		newInvestment.setInsuranceAmountRU(investment.getInsuranceAmountRU());
		newInvestment.setLevel(investment.getLevel());
		newInvestment.setMarketId(investment.getMarketId());
		newInvestment.setMarketName(investment.getMarketName());
		newInvestment.setOddsLose(investment.getOddsLose());
		newInvestment.setOddsWin(investment.getOddsWin());
		newInvestment.setOneTouchUpDown(investment.getOneTouchUpDown());
		newInvestment.setOpportunityId(investment.getOpportunityId());
		newInvestment.setOpportunityTypeId(investment.getOpportunityTypeId());
		newInvestment.setOptionPlusFee(investment.getOptionPlusFee());
		newInvestment.setTimeCreated(investment.getTimeCreated());
		newInvestment.setTimeEstClosing(investment.getTimeEstClosing());
		newInvestment.setTimeEstClosingMillsec(investment.getTimeEstClosingMillsec());
		newInvestment.setTimeEstClosingTxt(investment.getTimeEstClosingTxt());
		newInvestment.setTimePurchased(investment.getTimePurchased());
		newInvestment.setTimePurchasedMillsec(investment.getTimePurchasedMillsec());
		newInvestment.setTimePurchaseTxt(investment.getTimePurchaseTxt());
		newInvestment.setTimeSettled(investment.getTimeSettled());
		newInvestment.setTypeId(investment.getTypeId());

		return newInvestment;
	}

	public static double getReturnProfitPercent(Investment investment){
		boolean isBinary     = investment.getOpportunityTypeId() == Opportunity.TYPE_REGULAR;
		boolean isOptionPlus = investment.getOpportunityTypeId() == Opportunity.TYPE_OPTION_PLUS;
		boolean isDynamics   = investment.getOpportunityTypeId() == Opportunity.TYPE_DYNAMICS;
		boolean isBubbles    = investment.getTypeId() == Investment.INVESTMENT_TYPE_BUBBLES;

		double profitPercent = 0;

		if(isBinary || isOptionPlus || isBubbles){
			long investAmount = investment.getAmount() - investment.getInsuranceAmountRU() - investment.getOptionPlusFee();
			long returnAmount = AmountUtil.parseAmountToLong(investment.getAmountReturnWF(), Application.get().getCurrency());

			if(returnAmount > investAmount){
				profitPercent = 100 * Math.abs(returnAmount - investAmount) / investAmount;
			}else{
				profitPercent = 0;
			}
		}else if(isDynamics){
			profitPercent = getDynamicsOpenInvestmentProfitPercent(investment);
		}

		return profitPercent;
	}

	public static double getSettledInvestmentProfitPercent(Investment investment){
		double profitPercent = 0;

		long investAmount = investment.getAmount();
		long returnAmount;
		if(investment.getAmountReturnWF() != null && !investment.getAmountReturnWF().equalsIgnoreCase("")){
			returnAmount = AmountUtil.parseAmountToLong(investment.getAmountReturnWF(), Application.get().getCurrency());
		}else{
			returnAmount = 0;
		}

		if(returnAmount > investAmount){
			profitPercent = (double) 100 * Math.abs(returnAmount - investAmount) / investAmount;
		}else{
			profitPercent = 0;
		}

		return profitPercent;
	}

	public static double getDynamicsOpenInvestmentProfitPercent(Investment investment){
		double price = investment.getPrice();
		double profitPercent;
		if(investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY){
			if(price <= 0){
				profitPercent = 100;
			}else if(price >= 100){
				profitPercent = 0;
			}else{
				profitPercent = 100 * (100 - price) / price;
			}
		}else{
			if(price <= 0){
				profitPercent = 0;
			}else if(investment.getPrice() >= 100){
				profitPercent = 100;
			}else{
				profitPercent= 100 * price / (100 - price);
			}
		}

		return profitPercent;
	}

	public static double getDynamicsProfitAmount(Investment investment){
		double profitPercent = InvestmentUtil.getDynamicsOpenInvestmentProfitPercent(investment);
		return AmountUtil.getDoubleAmount(investment.getAmount()) * profitPercent / 100;
	}

	public static boolean isRollForward(Investment investment){
		return 	 investment.getAdditionalInfoType() == Investment.INVESTMENT_ADDITIONAL_INFO_RU ||
				(investment.getAdditionalInfoType() == Investment.INVESTMENT_ADDITIONAL_INFO_RU_BOUGHT && isCanceled(investment));
	}

	public static boolean isTakeProfit(Investment investment){
		return investment.getAdditionalInfoType() == Investment.INVESTMENT_ADDITIONAL_INFO_GM;
	}

	public static boolean isCanceled(Investment investment){
		return investment.getIsCanceled() == 1;
	}

	public static boolean isBinaryOrOptionPlus(Investment investment){
		long typeId = investment.getTypeId();
		return typeId == Investment.INVESTMENT_TYPE_CALL || typeId == Investment.INVESTMENT_TYPE_PUT;
	}

	public static boolean isDynamics(Investment investment){
		long typeId = investment.getTypeId();
		return typeId == Investment.INVESTMENT_TYPE_DYNAMICS_BUY || typeId == Investment.INVESTMENT_TYPE_DYNAMICS_SELL;
	}

	public static boolean isBubbles(Investment investment){
		long typeId = investment.getTypeId();
		return typeId == Investment.INVESTMENT_TYPE_BUBBLES;
	}
}
