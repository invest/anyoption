package com.anyoption.android.app.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import com.anyoption.android.app.Application;
import com.anyoption.android.R;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Skin;

import android.content.res.Resources;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.widget.TextView;

public class AmountUtil {

	protected static final String AMOUNT_PATTERN_NO_DIGITS = "###,###,##0";
	private static final String AMOUNT_PATTERN_NO_DIGITS_CURRENCY_WON = "#,##0";

	public static String getUserFormattedAmount(double amount) {
		if (Application.get().getUser() != null) {
			return getFormattedAmount(amount, getCurrency(Application.get().getUser().getCurrencyId()));
		}
		return "";
	}

	public static String getUserFormattedAmount(long amount) {
		if (Application.get().getUser() != null) {
			return getFormattedAmount(amount, getCurrency(Application.get().getUser().getCurrencyId()));
		}
		return "";
	}

	/**
	 * Converts the amount from type long to double.
	 *
	 * @param amount
	 *            The amount as long. Its last two digits are the cents.
	 * @return the amount as double
	 */
	public static double getDoubleAmount(long amount) {
		return ((double) amount) / 100;
	}

	/**
	 * Converts the amount from type double to long.
	 * The converted amount is in cents.
	 * @param amount
	 * @return the amount as long
	 */
	public static long getLongAmount(double amount) {
		return (long) (amount * 100);
	}

	public static String getFormattedAmount(double amount, String currencySymbol,
											boolean currencyLeftSymbol, int decimalPointDigits, long currencyId) {
		String patternNoDigits = currencyId != Constants.CURRENCY_KRW_ID ? AMOUNT_PATTERN_NO_DIGITS
				: AMOUNT_PATTERN_NO_DIGITS_CURRENCY_WON;
		StringBuilder pattern = new StringBuilder(patternNoDigits);
		if (decimalPointDigits > 0) {
			pattern.append('.');
			for (int i = 0; i < decimalPointDigits; i++) {
				pattern.append('0');
			}
		}

		// Explicitly setting the separators
		DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols();
		formatSymbols.setDecimalSeparator('.');
		formatSymbols.setGroupingSeparator(',');

		DecimalFormat format = new DecimalFormat(pattern.toString(), formatSymbols);
		String out = format.format(amount);
		return currencyLeftSymbol ? currencySymbol + out : out + currencySymbol;
	}

	public static String getFormattedAmount(long amount, String currencySymbol,
											boolean currencyLeftSymbol, int decimalPointDigits, long currencyId) {
		double doubleAmount = getDoubleAmount(amount);
		return getFormattedAmount(doubleAmount, currencySymbol, currencyLeftSymbol,
				decimalPointDigits, currencyId);
	}

	public static String getFormattedAmount(double amount, Currency currency) {
		return getFormattedAmount(amount, currency.getSymbol(), currency.getIsLeftSymbolBool(),
				currency.getDecimalPointDigits(), currency.getId());
	}

	public static String getFormattedAmountWithoutDecimalPointDigits(double amount, Currency currency) {
		return getFormattedAmount(amount, currency.getSymbol(), currency.getIsLeftSymbolBool(),
				0, currency.getId());
	}

	public static String getFormattedAmountWithoutSymbol(double amount, Currency currency) {
		String amountWithSymbol = getFormattedAmount(amount, currency.getSymbol(), currency.getIsLeftSymbolBool(), currency.getDecimalPointDigits(), currency.getId());
		return amountWithSymbol.replace(currency.getSymbol(), "");
	}

	public static String getFormattedAmount(long amount, Currency currency) {
		double doubleAmount = getDoubleAmount(amount);
		return getFormattedAmount(doubleAmount, currency);
	}

	public static String getFormattedAmountWithoutDecimalPointDigits(long amount, Currency currency) {
		double doubleAmount = getDoubleAmount(amount);
		return getFormattedAmountWithoutDecimalPointDigits(doubleAmount, currency);
	}

	public static String getFormattedAmountWithoutSymbol(long amount, Currency currency) {
		double doubleAmount = getDoubleAmount(amount);
		String amountWithSymbol = getFormattedAmount(doubleAmount, currency);
		return amountWithSymbol.replace(currency.getSymbol(), "");
	}

	public static String insertIntervalBetweenAmountAndSymbol(String amountTxt, Currency currency) {
		String amountWithoutSymbol = amountTxt.replace(currency.getSymbol(), "");
		return currency.getIsLeftSymbolBool() ? currency.getSymbol() + amountWithoutSymbol : amountWithoutSymbol + " " +currency.getSymbol();
	}

	/**
	 * Sets the text of the TextView using smaller font for the currencySymbol and cents.
	 * Superscript is used for the cents.
	 *
	 * @param textView
	 *            the view to be updated.
	 * @param amount
	 * @param currencySymbol
	 * @param isCurrencyLeftSymbol
	 * @param decimalPointDigits
	 *            digits after the decimal point.
	 */
	public static void setTextViewAmount(TextView textView, double amount, String currencySymbol,
										 boolean isCurrencyLeftSymbol, int decimalPointDigits) {
		StringBuilder pattern = new StringBuilder("###,###,##0");
		if (decimalPointDigits > 0) {
			pattern.append('.');
			for (int i = 0; i < decimalPointDigits; i++) {
				pattern.append('0');
			}
		}

		// Explicitly setting the separators
		DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols();
		formatSymbols.setDecimalSeparator('.');
		formatSymbols.setGroupingSeparator(',');

		DecimalFormat format = new DecimalFormat(pattern.toString(), formatSymbols);
		String amountString = format.format(amount);

		// Format the winning amount.
		// Set the currency symbol size and side (left or right).
		// Set the cents to superscript and size.
		float fontSizeCurrencyRelative = 0.55f;
		float fontSizeCentsRelative = 0.5f;
		String[] ar = amountString.split("\\.");
		String sumInteger = ar[0];

		String sumCents = "";
		if (ar.length > 1) {
			sumCents = ar[1];
		}

		if (isCurrencyLeftSymbol) {
			Spanned parseText = Html.fromHtml(currencySymbol + sumInteger + "<sup>" + sumCents
					+ "</sup>");
			SpannableString ss1 = new SpannableString(parseText);
			ss1.setSpan(new RelativeSizeSpan(fontSizeCurrencyRelative), 0, currencySymbol.length(),
					0);
			ss1.setSpan(new RelativeSizeSpan(fontSizeCentsRelative),
					parseText.length() - sumCents.length(), parseText.length(), 0);
			textView.setText(ss1);
		} else {
			Spanned parseText = Html.fromHtml(sumInteger + "<sup>" + sumCents + "</sup>"
					+ currencySymbol);
			SpannableString ss1 = new SpannableString(parseText);
			ss1.setSpan(new RelativeSizeSpan(fontSizeCurrencyRelative), parseText.length()
					- currencySymbol.length(), parseText.length(), 0);
			ss1.setSpan(new RelativeSizeSpan(fontSizeCentsRelative), sumInteger.length(),
					sumInteger.length() + sumCents.length(), 0);
			textView.setText(ss1);
		}
	}

	/**
	 * Sets the text of the TextView using smaller font for the currencySymbol and cents.
	 * Superscript is used for the cents. The amount's last two digits will be the cents.
	 *
	 * @param textView
	 *            the view to be updated.
	 * @param amount
	 *            the amount in long. Its last two digits are the cents.
	 * @param currencySymbol
	 * @param isCurrencyLeftSymbol
	 * @param decimalPointDigits
	 *            digits after the decimal point.
	 */
	public static void setTextViewAmount(TextView textView, long amount, String currencySymbol,
										 boolean isCurrencyLeftSymbol, int decimalPointDigits) {
		double doubleAmount = getDoubleAmount(amount);

		setTextViewAmount(textView, doubleAmount, currencySymbol, isCurrencyLeftSymbol,
				decimalPointDigits);
	}

	/**
	 * Gets the amount as long from returnAmount string.<br/>
	 * Example: $12,343.50 -> 1234350
	 *
	 * @param amountStr
	 * @param currency
	 * @return
	 */
	public static long parseAmountToLong(String amountStr, Currency currency) {
		return getLongAmount(Double.parseDouble(amountStr.replace(currency.getSymbol(), "").replace(",", "")));
	}

	public static double parseAmountToDouble(String amountStr, Currency currency) {
		return Double.parseDouble(amountStr.replace(currency.getSymbol(), "").replace(",", ""));
	}

	/**
	 * TODO: Currencies can be get with {@link Constants#SERVICE_GET_SKIN_CURRENCIES} request.
	 */
	public static Currency getCurrency(long currencyId) {
		Currency currency = new Currency();
		currency.setId(currencyId);
		currency.setIsLeftSymbol(1);
		currency.setDecimalPointDigits(2);

		Application application = Application.get();
		Resources resources = application.getResources();

		int currencyIdInt = (int) currencyId;

		switch (currencyIdInt) {
			case (int) Constants.CURRENCY_USD_ID:
				currency.setSymbol(resources.getString(R.string.currencySymbolUSD));
				break;
			case (int) Constants.CURRENCY_EUR_ID:
				currency.setSymbol(resources.getString(R.string.currencySymbolEUR));
				break;
			case (int) Constants.CURRENCY_GBP_ID:
				currency.setSymbol(resources.getString(R.string.currencySymbolGBP));
				break;
			case (int) Constants.CURRENCY_RUB_ID:
				currency.setSymbol(resources.getString(R.string.currencySymbolRUB));
				currency.setIsLeftSymbol(0);
				currency.setDecimalPointDigits(0);
				break;
			case (int) Constants.CURRENCY_TRY_ID:
				currency.setSymbol(resources.getString(R.string.currencySymbolTRY));
				currency.setIsLeftSymbol(0);
				break;
			case (int) Constants.CURRENCY_CNY_ID:
				currency.setSymbol(resources.getString(R.string.currencySymbolCNY));
				break;
			case (int) Constants.CURRENCY_KRW_ID:
				currency.setSymbol(resources.getString(R.string.currencySymbolKRW));
				break;
			case (int) Constants.CURRENCY_ILS_ID:
				currency.setSymbol(resources.getString(R.string.currencySymbolILS));
				break;
			case (int) Constants.CURRENCY_SEK_ID:
				currency.setSymbol(resources.getString(R.string.currencySymbolSEK));
				currency.setIsLeftSymbol(0);
				break;
			case (int) Constants.CURRENCY_CZK_ID:
				currency.setSymbol(resources.getString(R.string.currencySymbolCZK));
				currency.setIsLeftSymbol(0);
				break;
			default:
				currency.setSymbol(resources.getString(R.string.currencySymbolUSD));
				break;
		}

		return currency;
	}

	/**
	 * Returns new object of the default currency for selected skin.
	 *
	 * @return currency object
	 *
	 * @see {@link Application#getDefaultSkinCurrency()} and {@link Application#getCurrency()}
	 */
	public static Currency getDefaultCurrency() {
		Application application = Application.get();
		Skin skin = application.getSkins().get(application.getSkinId());
		int currencyId = skin.getDefaultCurrencyId();

		return getCurrency(currencyId);
	}

	/**
	 * Round the amount to the fourth digit after the coma.
	 * <br>Example : "0.00025"  -> "0.0003"
	 * <br>Example : "0.00024"  -> "0.0002"
     */
	public static double roundToFourthDigitFormat(double amount){
		return BigDecimal.valueOf(amount).setScale(4, RoundingMode.HALF_UP).doubleValue();
	}

}
