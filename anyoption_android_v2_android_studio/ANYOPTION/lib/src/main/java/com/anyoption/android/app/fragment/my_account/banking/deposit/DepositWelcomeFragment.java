package com.anyoption.android.app.fragment.my_account.banking.deposit;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.NavigationalFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

/**
 * A simple {@link Fragment} subclass. 
 * <p>It manages the "Deposit Welcome" screen and functionality.
 * @author Eyal O
 *
 */
public class DepositWelcomeFragment extends NavigationalFragment implements OnClickListener {
	
	private boolean isTablet;
	
	/**
	 * Use this method to obtain new instance of the fragment.
	 * @return
	 */
	public static DepositWelcomeFragment newInstance(){	
		return DepositWelcomeFragment.newInstance(new Bundle());
	}

	/**
	 * Use this method to obtain new instance of the fragment.
	 * @return
	 */
	public static DepositWelcomeFragment newInstance(Bundle bundle){
		DepositWelcomeFragment depositWelcomeFragment = new DepositWelcomeFragment();
		depositWelcomeFragment.setArguments(bundle);
		return depositWelcomeFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.deposit_welcome_layout, container, false);
		view.findViewById(R.id.not_now_deposit_button).setOnClickListener(this);
		view.findViewById(R.id.deposit_now_button).setOnClickListener(this);
		isTablet = getResources().getBoolean(R.bool.isTablet);
		
		return view;
	}

	@Override
	protected Screenable setupScreen() {
		return Screen.DEPOSIT_WELCOME;
	}

	@Override
	public void onClick(View v) {
		NavigationEvent navigationEvent = null;
		if (v.getId() == R.id.not_now_deposit_button) {
			navigationEvent = new NavigationEvent(application.getHomeScreen(), NavigationType.INIT);
		} else if (v.getId() == R.id.deposit_now_button) {
			navigationEvent = new NavigationEvent(Screen.FIRST_NEW_CARD, NavigationType.INIT);
		}

		if (navigationEvent != null) {
			application.postEvent(navigationEvent);
		}
	}
}

