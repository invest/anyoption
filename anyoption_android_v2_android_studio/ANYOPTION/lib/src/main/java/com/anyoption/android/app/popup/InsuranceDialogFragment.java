package com.anyoption.android.app.popup;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;

import com.anyoption.android.app.Application;
import com.anyoption.android.R;
import com.anyoption.android.app.event.InsuranceBoughtEvent;
import com.anyoption.android.app.event.InsuranceEvent;
import com.anyoption.android.app.event.InsuranceEvent.InsuranceType;
import com.anyoption.android.app.event.InsuranceEvent.Type;
import com.anyoption.android.app.lightstreamer.LightstreamerConnectionHandler;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.anyoption.android.app.model.InsuranceItem;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.form.FormCheckBox;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.util.GoldenMinute;
import com.anyoption.json.requests.InsurancesMethodRequest;
import com.anyoption.json.results.InsurancesMethodResult;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Fullscreen DialogFragment showing take profit/roll forward insurance.
 *
 * @author Mario Kutlev
 */
public class InsuranceDialogFragment extends BaseDialogFragment {

	private static final String TAG = InsuranceDialogFragment.class.getSimpleName();

	public static final String ARG_INSURANCE_TYPE_KEY = "arg_ins_type_key";
	private static final String TIME_FORMAT = "HH:mm";
	private static final long TIMER_UPDATE_INTERVAL_MS = 1000;

	private ExpandableListView listView;
	private InsuranceAdapter listAdapter;
	private TextView totalAmountView;
	private FormCheckBox allCheckBox;
	private View buyButton;

	private User user;
	private LightstreamerConnectionHandler ls;

	protected int insurancesType;
	protected long insuranceCloseTime;
	protected long opportunityCloseTime;

	private TextView timeLeftView;
	private CountDownTimer timeLeftTimer;

	protected ArrayList<ArrayList<InsuranceItem>> insuranceGroups;
	private String totalAmountText;

	private HashSet<Long> checkedItemsSet;

	/**
	 * Create new instance of the dialog. Must contain type argument with key
	 * {@link #ARG_INSURANCE_TYPE_KEY} and value one of {@link Investment#INSURANCE_TAKE_PROFIT} and
	 * {@link Investment#INSURANCE_ROLL_FORWARD}
	 */
	public static InsuranceDialogFragment newInstance(Bundle bundle) {
		if (!bundle.containsKey(ARG_INSURANCE_TYPE_KEY)) {
			throw new IllegalArgumentException(
					"The bundle MUST contain type key with value Investment.INSURANCE_TAKE_PROFIT or Investment.INSURANCE_ROLL_FORWARD)");
		}

		InsuranceDialogFragment fragment = new InsuranceDialogFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	/**
	 * Use {@link InsuranceDialogFragment#newInstance(InsuranceType, long)} instead this
	 * constructor.
	 */
	public InsuranceDialogFragment() {
		// Empty constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		insurancesType = args.getInt(ARG_INSURANCE_TYPE_KEY);

		application = Application.get();
		user = application.getUser();
		ls = application.getLightstreamerManager().getLs();
		checkedItemsSet = new HashSet<Long>();
		setInsuranceGroups();

		if (!insuranceGroups.isEmpty()) {
			insuranceCloseTime = insuranceGroups.get(0).get(0).getInsuranceCloseTime();
			opportunityCloseTime = insuranceGroups.get(0).get(0).getOpportunityCloseTime();
		} else {
			Log.e(TAG, "Dialog dismiss! - There aren't insurances.");
			dismiss();
		}

	}

	private void setInsuranceGroups() {
		if (insurancesType == Investment.INSURANCE_TAKE_PROFIT) {
			insuranceGroups = application.getTakeProfitGroups();
		} else {
			insuranceGroups = application.getRollForwardGroups();
		}
	}

	@Override
	protected void onCreateContentView(View contentView) {
		listView = (ExpandableListView) contentView.findViewById(R.id.insurance_insurances_list);

		listAdapter = new InsuranceAdapter();
		listView.setAdapter(listAdapter);
		listView.setDivider(null);
		listView.setChildDivider(null);
		listView.setSelector(R.drawable.insurance_item_selector);
		setUpListListeners();

		totalAmountView = (TextView) contentView.findViewById(R.id.insurance_total_amount);
		allCheckBox = (FormCheckBox) contentView.findViewById(R.id.insurance_total_checkbox);

		View totalAmountLayout = contentView.findViewById(R.id.insurance_total_layout);
		totalAmountLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!allCheckBox.isChecked()) {
					allCheckBox.setChecked(true);
					checkAllInsurances(true);
				} else {
					allCheckBox.setChecked(false);
					checkAllInsurances(false);
				}
			}
		});

		setUpTimer(contentView);

		ImageView insuranceIcon = (ImageView) contentView.findViewById(R.id.insurance_icon);
		TextView insuranceTitle = (TextView) contentView.findViewById(R.id.insurance_title);
		TextView insuranceDescription = (TextView) contentView
				.findViewById(R.id.insurance_description);

		Calendar calendar = Calendar.getInstance();
		//TODO not sure if it is the same for Take Profit
		if (insurancesType == Investment.INSURANCE_TAKE_PROFIT) {
			int min = calendar.get(Calendar.MINUTE);
			if (min < 30) {
				calendar.set(Calendar.MINUTE, 30);
			} else {
				calendar.add(Calendar.HOUR_OF_DAY, 1);
				calendar.set(Calendar.MINUTE, 0);
			}
		}else{
			calendar.setTimeInMillis(opportunityCloseTime);
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat(TIME_FORMAT);
		if (insurancesType == Investment.INSURANCE_TAKE_PROFIT) {
			insuranceTitle.setText(R.string.takeProfitLabel);
			insuranceDescription.setText(getString(R.string.takeProfitDescription,
					dateFormat.format(calendar.getTime())));
			insuranceIcon.setImageResource(R.drawable.insurance_take_profit);
		} else {
			// Investment.INSURANCE_ROLL_FORWARD
			insuranceTitle.setText(R.string.rollForwardLabel);
			String currentExpiration = dateFormat.format(calendar.getTime());
			calendar.add(Calendar.HOUR, 1);
			insuranceDescription.setText(getString(R.string.rollForwardDescription,
					currentExpiration, dateFormat.format(calendar.getTime())));
			insuranceIcon.setImageResource(R.drawable.insurance_roll_forward);
		}

		Button cancelButton = (Button) contentView.findViewById(R.id.insurance_cancel);
		cancelButton
				.setText(cancelButton.getText().toString().toUpperCase(application.getLocale()));
		cancelButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				stopTimeLeftTimer();
				dismiss();
			}
		});

		buyButton = contentView.findViewById(R.id.insurance_buy);
		buyButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				buyInsurances();
			}
		});
	}

	@Override
	protected int getContentLayout() {
		return R.layout.insurance_dialog;
	}

	@Override
	public void onResume() {
		super.onResume();
		application.registerForEvents(this, InsuranceEvent.class);

		startTimeLeftTimer();
		expandListGroups();
	}

	@Override
	public void onPause() {
		stopTimeLeftTimer();
		application.unregisterForAllEvents(this);

		super.onPause();
	}

	@Override
	public void onDestroy() {
		stopTimeLeftTimer();
		application.unregisterForAllEvents(this);

		super.onDestroy();
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		stopTimeLeftTimer();
		application.unregisterForAllEvents(this);

		super.onDismiss(dialog);
	}

	private void setUpListListeners() {
		listView.setOnGroupClickListener(new OnGroupClickListener() {

			@Override
			public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition,
										long id) {
				return true;
			}
		});

		listView.setOnChildClickListener(new OnChildClickListener() {

			@Override
			public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
										int childPosition, long id) {
				InsuranceItem insurance = insuranceGroups.get(groupPosition).get(childPosition);

				if (checkedItemsSet.contains(insurance.getInvestmentId())) {
					checkedItemsSet.remove(insurance.getInvestmentId());
				} else {
					checkedItemsSet.add(insurance.getInvestmentId());
				}

				allCheckBox.setChecked(areAllInsurancesChecked());

				updateTotal();
				updateList();

				return true;
			}
		});
	}

	protected void setUpTimer(View contentView) {
		timeLeftView = (TextView) contentView.findViewById(R.id.insurance_time_left);
	}

	/**
	 * Called when a {@link InsuranceEvent} occurs.
	 *
	 * @param event
	 */
	public void onEventMainThread(InsuranceEvent event) {
		Log.d(TAG, "InsuranceEvent received - eventType:" + event.getType());
		setInsuranceGroups();

		if (insuranceGroups.isEmpty()) {
			dismiss();
		}

		updateTotal();
		updateList();
	}

	private void buyInsurances() {
		ArrayList<Long> investmentsToSend = new ArrayList<Long>();
		ArrayList<Double> amountsToSend = new ArrayList<Double>();
		GoldenMinute.GoldenMinuteType goldenMinuteType = GoldenMinute.GoldenMinuteType.STANDARD;

		for (int i = 0; i < insuranceGroups.size(); i++) {
			ArrayList<InsuranceItem> currentList = insuranceGroups.get(i);

			if (currentList.size() > 0) {
				for (int j = 0; j < currentList.size(); j++) {
					InsuranceItem insurance = currentList.get(j);
					if(insurance.getpType().equalsIgnoreCase(LightstreamerManager.PTYPE_ADDITIONAL)){
						goldenMinuteType = GoldenMinute.GoldenMinuteType.ADDITIONAL;
					}else{
						goldenMinuteType = GoldenMinute.GoldenMinuteType.STANDARD;
					}
					checkedItemsSet.contains(insurance.getInvestmentId());

					if (checkedItemsSet.contains(insurance.getInvestmentId())) {
						investmentsToSend.add(insurance.getInvestmentId());
						amountsToSend.add(insurance.getPremiumPrice());
					}
				}
			}
		}

		if (investmentsToSend.size() > 0) {
			buyButton.setEnabled(false);

			InsurancesMethodRequest request = new InsurancesMethodRequest();
			request.setGmType(goldenMinuteType);
			request.setInsuranceType(insurancesType);
			request.setInvestmentIds(investmentsToSend.toArray(new Long[investmentsToSend.size()]));
			request.setInsuranceAmounts(amountsToSend.toArray(new Double[amountsToSend.size()]));
			stopTimeLeftTimer();

			application.getCommunicationManager().requestService(this, "buyInsurancesCallback",
					Constants.SERVICE_BUY_INSURANCES, request, InsurancesMethodResult.class, true,
					true);
		} else {
			application.getNotificationManager().showNotificationError(getString(R.string.insurance_select_option));
			Log.d(TAG, "No options selected - buyInsurances");
		}
	}

	public void buyInsurancesCallback(Object resultObj) {
		buyButton.setEnabled(true);

		InsurancesMethodResult result = (InsurancesMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			ArrayList<Long> boughtInvestmentIds = new ArrayList<Long>();
			for (int i = 0; i < result.getResults().length; i++) {
				long invId = result.getInvestmentIds()[i];
				if (result.getResults()[i].startsWith("OK")) {
					boughtInvestmentIds.add(invId);
				}
			}

			if (result.getUser() != null) {
				// Update the user with the new balance
				application.setUser(result.getUser());
			}

			if (boughtInvestmentIds.size() > 0) {
				deleteInsurances(boughtInvestmentIds);

				for (long invId : boughtInvestmentIds) {
					ls.sendMessage(LightstreamerManager.MESSAGE_BOUGHT_INVESTMENT_ID + "_" + invId);
				}

				if (insurancesType == Investment.INSURANCE_TAKE_PROFIT) {
					application
							.postEvent(new InsuranceEvent(InsuranceType.TAKE_PROFIT, Type.DELETE));
				} else {
					application.postEvent(new InsuranceEvent(InsuranceType.ROLL_FORWARD,
							Type.DELETE));
				}

				application.postEvent(new InsuranceBoughtEvent(insurancesType));

				String successMessage;
				if (insurancesType == Investment.INSURANCE_TAKE_PROFIT) {
					successMessage = getString(R.string.takeProfitSuccess, result.getCredit());
				} else {
					successMessage = getString(R.string.rollForwardPostponed);
				}
				application.getNotificationManager().showNotificationSuccess(successMessage);
			} else {
				application.getNotificationManager().showNotificationError(getString(R.string.errorTryAgain));
			}

			Log.d(TAG, "buyInsurancesCallback - Success");
		} else if(result != null && result.getErrorCode() == Constants.ERROR_CODE_NO_CASH_FOR_NIOU) {
			Application.get().getFragmentManager().showDialogFragment(LowCashBalanceDialogFragment.class, null);
		} else {
			Log.e(TAG, "buyInsurancesCallback - Fail");
			ErrorUtils.handleResultError(result);
		}

		startTimeLeftTimer();
	}

	private void updateList() {
		listAdapter.notifyDataSetChanged();
		expandListGroups();
	}

	private void updateTotal() {
		double totalAmount = 0;
		for (int i = 0; i < insuranceGroups.size(); i++) {
			ArrayList<InsuranceItem> currentList = insuranceGroups.get(i);
			for (int j = 0; j < currentList.size(); j++) {
				if (checkedItemsSet.contains(currentList.get(j).getInvestmentId())) {
					totalAmount = totalAmount + currentList.get(j).getPremiumPrice();
				}
			}
		}

		totalAmountText = AmountUtil.getFormattedAmount(totalAmount, user.getCurrency());
		totalAmountView.setText(totalAmountText);
	}

	private void checkAllInsurances(boolean checked) {
		if (checked) {
			for (int i = 0; i < insuranceGroups.size(); i++) {
				ArrayList<InsuranceItem> currentList = insuranceGroups.get(i);
				for (int j = 0; j < currentList.size(); j++) {
					checkedItemsSet.add(currentList.get(j).getInvestmentId());
				}
			}
		} else {
			checkedItemsSet.clear();
		}

		updateTotal();
		updateList();
	}

	private boolean areAllInsurancesChecked() {
		int checkedCount = 0;
		for (int i = 0; i < insuranceGroups.size(); i++) {
			checkedCount = checkedCount + insuranceGroups.get(i).size();
		}

		if (checkedItemsSet.size() == checkedCount) {
			return true;
		}

		return false;
	}

	private void expandListGroups() {
		for (int i = 0; i < listAdapter.getGroupCount(); i++) {
			listView.expandGroup(i);
		}
	}

	protected void startTimeLeftTimer() {
		long timerTime = insuranceCloseTime - TimeUtils.getCurrentServerTime();

		timeLeftTimer = new CountDownTimer(timerTime, TIMER_UPDATE_INTERVAL_MS) {

			@Override
			public void onTick(long millisUntilFinished) {
				timeLeftView.setText(TimeUtils.formatTimeToMinutesAndSeconds(millisUntilFinished));
			}

			@Override
			public void onFinish() {
				timeLeftView.setText(TimeUtils.formatTimeToMinutesAndSeconds(0));

				// Close the dialog
				dismissAllowingStateLoss();
			}
		};

		timeLeftTimer.start();
	}

	protected void stopTimeLeftTimer() {
		if (timeLeftTimer != null) {
			timeLeftTimer.cancel();
			timeLeftTimer = null;
		}
	}

	private void deleteInsurances(List<Long> ids) {
		for (long id : ids) {
			for (int i = 0; i < insuranceGroups.size(); i++) {
				ArrayList<InsuranceItem> currentList = insuranceGroups.get(i);

				for (int j = 0; j < currentList.size(); j++) {
					if (currentList.get(j).getInvestmentId() == id) {
						currentList.remove(j);
						if (currentList.size() == 0) {
							insuranceGroups.remove(i);
						}
						break;
					}
				}
			}
		}
	}

	private class InsuranceAdapter extends BaseExpandableListAdapter {

		private LayoutInflater inflater;

		public InsuranceAdapter() {
			this.inflater = (LayoutInflater) getActivity().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getGroupCount() {
			return insuranceGroups.size();
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			return insuranceGroups.get(groupPosition).size();
		}

		@Override
		public Object getGroup(int groupPosition) {
			return insuranceGroups.get(groupPosition);
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			return insuranceGroups.get(groupPosition).get(childPosition);
		}

		@Override
		public long getGroupId(int groupPosition) {
			return insuranceGroups.get(groupPosition).get(0).getMarketId();
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return insuranceGroups.get(groupPosition).get(childPosition).getInvestmentId();
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
								 ViewGroup parent) {
			View groupView = convertView;
			if (convertView == null) {
				groupView = inflater.inflate(R.layout.insurance_group_row, parent, false);
			}

			TextView assetName = (TextView) groupView.findViewById(R.id.insurance_group_asset_name);
			assetName.setText(insuranceGroups.get(groupPosition).get(0).getMarketName());
			TextView currentLevel = (TextView) groupView
					.findViewById(R.id.insurance_group_asset_level);
			currentLevel.setText(insuranceGroups.get(groupPosition).get(0).getCurrentLevel());

			return groupView;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
								 View convertView, ViewGroup parent) {
			View childView = convertView;
			if (convertView == null) {
				childView = inflater.inflate(R.layout.insurance_item_row, parent, false);
			}

			InsuranceItem insurance = insuranceGroups.get(groupPosition).get(childPosition);

			TextView timeInvested = (TextView) childView
					.findViewById(R.id.insurance_investment_time);
			timeInvested.setText(insurance.getInvestmentTimeFormatted());
			TextView invLevel = (TextView) childView.findViewById(R.id.insurance_investment_level);
			invLevel.setText(insurance.getInvestmentLevel());
			TextView invAmount = (TextView) childView
					.findViewById(R.id.insurance_investment_amount);
			invAmount.setText(insurance.getInvestmentAmountFormatted());
			TextView premiumPrice = (TextView) childView
					.findViewById(R.id.insurance_premium_amount);
			premiumPrice.setText(insurance.getPremiumPriceFormatted());
			FormCheckBox checkBox = (FormCheckBox) childView
					.findViewById(R.id.insurance_investment_checkbox);
			checkBox.setChecked(checkedItemsSet.contains(insurance.getInvestmentId()));

			ImageView invIcon = (ImageView) childView
					.findViewById(R.id.insurance_investment_put_call_icon);
			if (insurance.getInvestmentType() == Investment.INVESTMENT_TYPE_CALL) {
				invIcon.setImageResource(R.drawable.triangle_up_small_blue);
			} else {
				// INVESTMENT_TYPE_PUT
				invIcon.setImageResource(R.drawable.triangle_down_small_blue);
			}

			return childView;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}

	}

}
