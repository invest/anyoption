package com.anyoption.android.app.manager;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.BitmapUtils;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.results.UpdateMethodResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Class that manages the Internet communications (like requests to the Back-end
 * , etc).
 * 
 * @author Anastas Arnaudov
 *
 */
public class CommunicationManager {

	public final String TAG = CommunicationManager.class.getSimpleName();

	protected Application application;

	/**
	 * {@link CommunicationManager}
	 * 
	 * @param application
	 */
	public CommunicationManager(Application application) {
		Log.d(TAG, "On Create");

		this.application = application;
	}

	/**
	 * Same as
	 * {@link #requestService(Object, String, String, MethodRequest, Class, boolean, boolean)
	 * requestService} but with handleError = true and showLoading = true
	 */
	public boolean requestService(Object resultObject, String resultMethodName, String requestMethodName,
			MethodRequest request, Class<? extends MethodResult> resultClass) {
		return requestService(resultObject, resultMethodName, requestMethodName, request, resultClass, true, true);
	}

	/**
	 * Calling service requests.
	 * <p>
	 * The method uses the Class object and the callback method name with Java
	 * Reflection in order to call the desired method in the class that made the
	 * request call.
	 *
	 * @param resultObject
	 *            The class we have called the service from.
	 * @param resultMethodName
	 *            The name of the callback method in the class we have called
	 *            the service from.
	 * @param requestMethodName
	 *            The name of the request method in the service we want to call.
	 * @param request
	 *            Object of type {@link MethodRequest} to hold the data for the
	 *            request.
	 * @param resultClass
	 *            The class of the result object.
	 * @param handleError
	 *            Should the Task handle the case when there is an error in the
	 *            result or not.
	 * @param showLoading
	 *            Should there be a Loading animation showing.
	 * @return
	 * 		<li>True - if the Request Task is started.
	 *         <li>False - if there were errors (No Internet connection,
	 *         Exception, etc).
	 */
	public boolean requestService(final Object resultObject, final String resultMethodName,
			final String requestMethodName, final MethodRequest request,
			final Class<? extends MethodResult> resultClass, final boolean handleError, final boolean showLoading) {
		try {
			// Check Internet:
			if (!application.getConnectionManager().checkConnection()) {
				application.getNotificationManager().showNotificationNoInternet();
				return false;
			}
			Handler handler = new Handler(Looper.getMainLooper());
			handler.post(new Runnable() {
				@Override
				public void run() {
					setupCommonRequestParams(request);

					RequestServiceTask.startServiceRequest(resultObject, resultMethodName, requestMethodName, request,
							resultClass, handleError, showLoading);
				}
			});
		} catch (Exception e) {
			Log.d(TAG, "Cannot complete service request for " + requestMethodName, e);
			return false;
		}
		return true;
	}

	@SuppressWarnings("deprecation")
	protected void setupCommonRequestParams(MethodRequest request) {
		request.setTablet(ScreenUtils.isTablet());
		request.setAppVersion(String.valueOf(application.getApplicationVersionName()));
		request.setDeviceType(Utils.getDeviceManufacturer() + " " + Utils.getDeviceModel());
		request.setOsVersion(Utils.getDeviceOSVersion());
		request.setDeviceId(Utils.getUDID());
		request.setWriterId(application.getWriterId());
		request.setSkinId(application.getSkinId());
		Date date = new Date();
		request.setUtcOffset(date.getTimezoneOffset());
		if (application.getUser() != null && request instanceof UserMethodRequest) {
			UserMethodRequest userMethodRequest = (UserMethodRequest) request;
			if (!userMethodRequest.isLogin() && userMethodRequest.getPassword() == null) {
				userMethodRequest.setEncrypt(true);
			}
		}
	}

	/**
	 * See {@link RequestServiceTask#cancelAllRequests()}.
	 */
	public void cancelAllRequests() {
		Log.d(TAG, "Cancel all requests.");
		RequestServiceTask.cancelAllRequests();
	}

	/**
	 * See {@link RequestServiceTask#cancelAllRequests(Object)}.
	 */
	public void cancelAllRequests(Object resultObject) {
		Log.d(TAG, "Cancel all requests for the given result object.");
		RequestServiceTask.cancelAllRequests(resultObject);
	}

	///////////////////////////////////////////////////////////////////////////

	/**
	 * A sub-class of {@link AsyncTask}.
	 * <p>
	 * It manages the requests to the back-end.
	 * <p>
	 * To start a request call the static method
	 * {@link RequestServiceTask#startServiceRequest(Object, String, String, MethodRequest, Class)}
	 */
	public static class RequestServiceTask extends AsyncTask<MethodRequest, Void, MethodResult> {

		public static final String TAG = RequestServiceTask.class.getSimpleName();

		private static final String GSON_DATE_FORMAT = "MMM d, yyyy hh:mm:ss aaa";

		/**
		 * List with the running tasks.
		 */
		private static List<RequestServiceTask> runningRequestServiceTasks = new ArrayList<RequestServiceTask>();

		/**
		 * The Class object of the class in which the callback method will be
		 * called.
		 */
		private Object resultClass;

		/**
		 * The name of the callback method.
		 */
		private String resultMethodName;

		/**
		 * The name of the method we want to call on the server.
		 */
		private String requestMethodName;

		/**
		 * The Class of the result object which will hold the requested data.
		 */
		private Class<? extends MethodResult> result;

		/**
		 * Flag that says whether to handle a possible error or to leave the
		 * handling to the {@link #resultClass}
		 */
		private boolean handleError;

		/**
		 * {@link RequestServiceTask}
		 * 
		 * @param resultObject
		 *            {@link RequestServiceTask#resultClass}
		 * @param resultMethodName
		 *            {@link RequestServiceTask#resultMethodName}
		 * @param requestMethodName
		 *            {@link RequestServiceTask#requestMethodName}
		 * @param resultClass
		 *            {@link RequestServiceTask#result}
		 * @param showLoading
		 *            {@link RequestServiceTask#showLoading}
		 */
		public RequestServiceTask(Object resultObject, String resultMethodName, String requestMethodName,
				Class<? extends MethodResult> resultClass, boolean handleError, boolean showLoading) {
			Log.d(TAG, "Create new " + RequestServiceTask.class.getSimpleName());

			this.resultClass = resultObject;
			this.resultMethodName = resultMethodName;
			this.requestMethodName = requestMethodName;
			this.result = resultClass;

			this.handleError = handleError;
		}

		/**
		 * TODO TEMP method before every service is migrated to the new Servlet.
         */
		private String getNewURL(String oldURL, String serviceName){
			String newURL = oldURL;
			if(			serviceName.equals(Constants.SERVICE_GET_ASSET_INFO) 			||
						serviceName.equals(Constants.SERVICE_SHOW_WITHDRAW_DETAILS) 	||
						serviceName.equals(Constants.SERVICE_GET_WITHDRAW_DETAILS) 		||
						serviceName.equals(Constants.SERVICE_INSERT_WITHDRAW_DETAILS) 	||
						serviceName.equals(Constants.SERVICE_SKIP_WITHDRAW_DETAILS)			){
				newURL = oldURL.replace("AnyoptionService", "CommonService");
			}
			newURL = newURL + serviceName;

			return newURL;
		}

		@Override
		protected void onPreExecute() {
			Log.v(TAG, "onPreExecute : " + requestMethodName);
		}

		@Override
		protected MethodResult doInBackground(MethodRequest... params) {
			Log.v(TAG, "doInBackground - " + requestMethodName);
			MethodResult result = null;
			HttpURLConnection httpCon = null;
			try {

				//Set Cookie Manager in order to fetch the session id from the cookies:
				//CookieManager cookieManager = (CookieManager) CookieManager.getDefault();
				CookieManager cookieManager = (CookieManager) Application.get().getCookieManager();
				CookieHandler.setDefault(cookieManager);
				//////////////////////////////////////////////////////////////////////

				String url = getNewURL(Application.get().getServiceURL(), requestMethodName);
				URL u = new URL(url);
				httpCon = (HttpURLConnection) u.openConnection();
				httpCon.setDoOutput(true);

				String savedSession = Application.get().getSharedPreferencesManager().getString(Constants.PREFERENCES_RESTARTED_APP_USER_SESSION_ID, "");
				Log.d("FIX_FOR_LOST_SESSIONS", "Request " + requestMethodName + ". Saved session : " + savedSession);
				if(!savedSession.equals("") && Application.get().getUser()!= null) {
					Log.d("FIX_FOR_LOST_SESSIONS", "Request " + requestMethodName + ". Setting saved session to the connection.");
					httpCon.setRequestProperty("Cookie", Constants.COOKIE_SESSION_ID + "=" + savedSession);
					Application.get().setSessionId(savedSession);
				} else if(!savedSession.equals("") && Application.get().getUser()==null) {
					Log.d("FIX_FOR_LOST_SESSIONS", "Request " + requestMethodName + ". User == null. Deleting saved session.");
					Application.get().getSharedPreferencesManager().putString(Constants.PREFERENCES_RESTARTED_APP_USER_SESSION_ID, "");
				}

				Gson gson = new GsonBuilder().setDateFormat(GSON_DATE_FORMAT).create();
				String json = gson.toJson(params[0]);
				Log.d(TAG,"Request : " + requestMethodName + ", Caller : " + resultClass.getClass().getSimpleName() + ", JSON = " + json);
				byte[] data = json.getBytes("UTF-8");
				httpCon.getOutputStream().write(data, 0, data.length);

				if (!isCancelled()) {
					int respCode = httpCon.getResponseCode();
					Log.d(TAG, "Request : " + requestMethodName + ", ResponseCode: " + String.valueOf(respCode));

					result = gson.fromJson(new InputStreamReader(httpCon.getInputStream(), "UTF-8"), this.result);
					Log.v(TAG, "Request : " + requestMethodName + ", Done parsing response.");
					Log.v(TAG, "Request : " + requestMethodName + ", Result : " + result.toString());

					//Fetch the Session id from the cookies:
					CookieStore cookieJar = cookieManager.getCookieStore();
					List<HttpCookie> cookies = cookieJar.getCookies();
					for (HttpCookie cookie : cookies) {
						if(cookie.getName().equals(Constants.COOKIE_SESSION_ID)){
							String sessionId = cookie.getValue();
							Application.get().setSessionId(sessionId);
//							Application.get().getSharedPreferencesManager().putString(Constants.PREFERENCES_RESTARTED_APP_USER_SESSION_ID, sessionId);
							Log.d("FIX_FOR_LOST_SESSIONS", "Request " + requestMethodName + ". Session from result : " + sessionId);
							break;
						}
					}
					/////////////////////////////////////////
				}
			} catch (Exception e) {
				Log.e(TAG, "Problem executing Request: " + requestMethodName, e);
			} finally {
				if (null != httpCon) {
					httpCon.disconnect();
				}
			}
			return result;
		}

		@Override
		protected void onCancelled() {
			Log.v(TAG, "onCancelled - " + requestMethodName);
			removeCurrentTask();
			invokeCallback(null);
			handleError(null);
			super.onCancelled();
		}

		@Override
		protected void onPostExecute(MethodResult result) {
			Log.v(TAG, "onPostExecute : " + requestMethodName);
			removeCurrentTask();
			invokeCallback(result);
			handleError(result);
		}

		/**
		 * Method that invokes the callback method of the result class via
		 * reflection.
		 * 
		 * @param result
		 */
		private void invokeCallback(MethodResult result) {
			Log.v(TAG, "Invoking callback for Request : " + requestMethodName);
			if (resultClass != null && resultMethodName != null) {
				try {
					Method resultMethod = resultClass.getClass().getMethod(resultMethodName, Object.class);
//					Method resultMethod = resultClass.getClass().getMethod(resultMethodName, this.result);
					resultMethod.invoke(resultClass, result);
				} catch (Exception e) {
					Log.e(TAG, "invokeCallback -> Exception: " + e.getClass().getName());
				}
			}
		}

		/**
		 * Handles the error (if there is any and if it is requested to do so).
		 */
		private void handleError(MethodResult result) {
			if (handleError) {
				if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
					// The request is successful.
					return;
				}

				ErrorUtils.handleResultError(result);
			}
		}

		/**
		 * Removes the current request task from the running tasks list.
		 */
		private void removeCurrentTask() {
			synchronized (runningRequestServiceTasks) {
				runningRequestServiceTasks.remove(this);
			}
		}

		public Object getResultClass() {
			return resultClass;
		}

		/**
		 * Starts a {@link RequestServiceTask} to call the back-end Service. The
		 * task is added to a list of request running tasks.
		 * <p>
		 * Via Java Reflection the given method (in the class that made the
		 * request call) is called.
		 * 
		 * @param resultObject
		 *            The class in which the request was made.
		 * @param resultMethodName
		 *            The method name in the class in which the request was
		 *            made.
		 * @param requestMethodName
		 *            The name of the service method we want to call.
		 * @param request
		 *            A object of type {@link MethodRequest} that holds the data
		 *            to be send.
		 * @param resultClass
		 *            The class for the response object.
		 * @param handleError
		 *            Should the task handle the Error or Not.
		 * @param showLoading
		 *            Should there be a Loading animation showing.
		 */
		public static void startServiceRequest(Object resultObject, String resultMethodName, String requestMethodName,
				MethodRequest request, Class<? extends MethodResult> resultClass, boolean handleError,
				boolean showLoading) {
			synchronized (runningRequestServiceTasks) {
				RequestServiceTask task = new RequestServiceTask(resultObject, resultMethodName, requestMethodName,resultClass, handleError, showLoading);
				task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, request);
				runningRequestServiceTasks.add(task);
			}
			Log.v(TAG, "Running tasks: " + runningRequestServiceTasks.size());
		}

		/**
		 * Cancel all request tasks in the "Running Request tasks" list.
		 */
		public static void cancelAllRequests() {
			synchronized (runningRequestServiceTasks) {
				for (Iterator<RequestServiceTask> i = runningRequestServiceTasks.iterator(); i.hasNext();) {
					i.next().cancel(true);
				}
				runningRequestServiceTasks.clear();
			}
		}

		public static void cancelAllRequests(Object resultObject) {
			synchronized (runningRequestServiceTasks) {
				for (Iterator<RequestServiceTask> i = runningRequestServiceTasks.iterator(); i.hasNext();) {
					RequestServiceTask task = i.next();
					if (task.getResultClass() == resultObject) {
						task.cancel(true);
						i.remove();
						Log.d(TAG, "cancelAllRequests -> taskRemoved=" + task.requestMethodName);
					}
				}
			}

		}
	}

	////////////////////////////////////////////////////////////////////////////////////
	/////////////////					UPLOAD DOCUMENTS				////////////////
	//////////////////////////// ///////////////////////////////////////////////////////
	
	public void uploadDocumentImage(long type, String fileName, long ccId, Bitmap bitmap, Object callback, String callbackMethodName) {
		DocumentUploadTask documentUploadTask = new DocumentUploadTask(type, fileName, ccId, bitmap, callback, callbackMethodName);
		documentUploadTask.execute();
	}

	public class DocumentUploadTask extends AsyncTask<Void, Void, MethodResult> {

		public final String TAG = DocumentUploadTask.class.getSimpleName();

		private Bitmap bitmap;
		private Object callback;
		private String callbackMethodName;
		private long type;
		private String fileName;
		private long ccId;

		public DocumentUploadTask(long type, String fileName, long ccId, Bitmap bitmap, Object callback, String callbackMethodName) {
			super();
			this.bitmap = bitmap;
			this.callback = callback;
			this.callbackMethodName = callbackMethodName;
			this.type = type;
			this.fileName = fileName;
			this.ccId = ccId;
			
		}

		@Override
		protected MethodResult doInBackground(Void... params) {
			Log.d(TAG, "doInBackground");
			MethodResult result = null;
			HttpURLConnection connection = null;
			
			try {

				CookieManager cookieManager = (CookieManager) Application.get().getCookieManager();
				CookieHandler.setDefault(cookieManager);

				String addressQuery = "?";
				        addressQuery += "fileType=" + type;
				        addressQuery += "&fileName=" + Uri.encode(fileName);
				        addressQuery += "&writerId=" + String.valueOf(Application.get().getWriterId());
				
				if(ccId != 0) {
					addressQuery += "&ccId=" + String.valueOf(ccId);
				}

				URL url = new URL(getBaseURL() + Constants.SERVICE_UPLOAD_DOCUMENTS + addressQuery);
				byte[] bytes = BitmapUtils.convertBitmapToByteArray(bitmap);

				connection = (HttpURLConnection) url.openConnection();
				connection.setDoInput(true);
				connection.setDoOutput(true);
				connection.setRequestMethod("POST");
				connection.setRequestProperty("Content-Type", "image/jpeg");
				connection.setRequestProperty("Content-Length", String.valueOf(bytes.length));

				if(Utils.getDeviceOSVersionCode() >= 23) {
					connection.setRequestProperty("Cookie", Constants.COOKIE_SESSION_ID + "=" +
							application.getSessionId());
					Log.d("FIX_FOR_LOST_SESSIONS", "Upload " + "Session: " + Application.get().getSessionId());
				}

				OutputStream outputStream = connection.getOutputStream();
				
				
				outputStream.write(bytes);

				outputStream.flush();
				outputStream.close();

				Gson gson = new Gson();

				if (!isCancelled()) {
					int responseCode = connection.getResponseCode();
					Log.d(TAG, "UploadMobileService, ResponseCode: " + String.valueOf(responseCode));

					result = gson.fromJson(new InputStreamReader(connection.getInputStream(), "UTF-8"),
							UpdateMethodResult.class);
					Log.v(TAG, "UploadMobileService, Done parsing response.");
				}
			} catch (Exception e) {
				Log.e(TAG, "Problem executing UploadMobileService", e);
			} finally {
				if (null != connection) {
					connection.disconnect();
				}
			}
			return result;
		}

		@Override
		protected void onCancelled() {
			Log.v(TAG, "onCancelled");
			invokeCallback(null);
			super.onCancelled();
		}

		@Override
		protected void onPostExecute(MethodResult result) {
			Log.d(TAG, "onPostExecute");
			invokeCallback(result);
		}

		private void invokeCallback(MethodResult result) {
			if (callback != null && callbackMethodName != null) {
				try {
					Method resultMethod = callback.getClass().getMethod(callbackMethodName, Object.class);
					resultMethod.invoke(callback, result);
				} catch (Exception e) {
					Log.e(TAG, "Cannot invoke the callback class method", e);
				}
			}
		}

	}

	public String getBaseURL() {
		String url = application.getServiceURL();
		url = url.substring(0, url.lastIndexOf("AnyoptionService/"));
		return url;
	}

	/**
	 * TEMPORARY method for Services that still hasn't MethodRequest and MethodResult objcet as parameters.
     */
	//TODO
	public void requestService(final String serviceName, final Object request, final Class resultClass, final String callbackMethodName, final Object callback){
		Log.v(TAG, "requestService" + serviceName);
		new AsyncTask<Void, Void, Object>(){

			@Override
			protected Object doInBackground(Void... params) {
				Log.v(TAG, "doInBackground - " + serviceName);
				Object result = null;
				HttpURLConnection httpCon = null;
				try {
					URL u = new URL(Application.get().getServiceURL() + serviceName);
					httpCon = (HttpURLConnection) u.openConnection();
					httpCon.setDoOutput(true);

					//TODO FIND ANOTHER WAY, SET IN THE COOKIE MANAGER(Tried but not working)
					String savedSession = Application.get().getSharedPreferencesManager().getString(Constants.PREFERENCES_RESTARTED_APP_USER_SESSION_ID, "");
					if(!savedSession.equals("") && Application.get().getUser()!= null) {
						httpCon.setRequestProperty("Cookie", Constants.COOKIE_SESSION_ID + "=" + savedSession);
						Application.get().setSessionId(savedSession);
						Log.d("FIX_FOR_LOST_SESSIONS", "Have saved session. User is not null. Saved Session: " + savedSession);
					} else if(!savedSession.equals("") && Application.get().getUser()==null) {
						Log.d("FIX_FOR_LOST_SESSIONS", "Have saved session. User is null. Deleting saved session from before restarting. Saved Session: " + savedSession);
						Application.get().getSharedPreferencesManager().putString(Constants.PREFERENCES_RESTARTED_APP_USER_SESSION_ID, "");
					}

					Gson gson = new GsonBuilder().create();
					String json = gson.toJson(request);
					Log.d(TAG, json);
					byte[] data = json.getBytes("UTF-8");
					httpCon.getOutputStream().write(data, 0, data.length);

					if (!isCancelled()) {
						int respCode = httpCon.getResponseCode();
						Log.d(TAG, "ServiceCall: " + serviceName + ", ResponseCode: " + String.valueOf(respCode));

						result = gson.fromJson(new InputStreamReader(httpCon.getInputStream(), "UTF-8"), resultClass);
						Log.v(TAG, "ServiceCall: " + serviceName + ", Done parsing response.");
						Log.v(TAG, result.toString());
					}
				} catch (Exception e) {
					Log.e(TAG, "Problem executing Service: " + serviceName, e);
				} finally {
					if (null != httpCon) {
						httpCon.disconnect();
					}
				}
				return result;
			}

			@Override
			protected void onPostExecute(Object result) {
				super.onPostExecute(result);
				if (callback != null && callbackMethodName != null) {
					try {
						Method resultMethod = callback.getClass().getMethod(callbackMethodName, Object.class);
						resultMethod.invoke(callback, result);
					} catch (Exception e) {
						Log.e(TAG, "invokeCallback -> Exception: " + e.getClass().getName());
					}
				}
			}


		}.execute();
	}
}
