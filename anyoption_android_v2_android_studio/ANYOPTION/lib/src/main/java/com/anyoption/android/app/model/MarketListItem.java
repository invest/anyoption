package com.anyoption.android.app.model;

import com.anyoption.common.beans.base.Market;

/**
 * Custom Market list item.
 * 
 * @author Anastas Arnaudov
 */
public class MarketListItem {

	private String statusLabel;
	private String statusValue;
	private String currentLevel;
	private String profitPercent;
	private String profitText;
	private Market market;
	private long opportunityID;
	private int opportunityState;
	private boolean showTimer;
	
	private long timeCreated;
	private long timeClose;
	private long timeLastInvest;
	
	private boolean isLastMinutesState;
	private boolean hasOpenOptions;
	
	private boolean isActive = true;

	public MarketListItem(Market market) {
		this.market = market;
	}

	public synchronized String getStatusLabel() {
		return statusLabel;
	}

	public synchronized void setStatusLabel(String statusLabel) {
		this.statusLabel = statusLabel;
	}

	public synchronized String getCurrentLevel() {
		return currentLevel;
	}

	public synchronized void setCurrentLevel(String currentLevel) {
		this.currentLevel = currentLevel;
	}

	public synchronized Market getMarket() {
		return market;
	}

	public synchronized void setMarket(Market market) {
		this.market = market;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Market ID = " + getMarket().getId() + "\n")
		.append("Market Name = " + getMarket().getDisplayName() + "\n")
		.append("Is Option Plus = " + getMarket().isOptionPlusMarket() +"\n")
		.append("Opportunity ID = " + getOpportunityID() + "\n")
		.append("Opportunity State = " + getOpportunityState() + "\n")
		.append("Status = " + getStatusLabel() + getStatusValue() + "\n")
		.append("Current Level = " + getCurrentLevel());
		return builder.toString();
	}

	public synchronized String getStatusValue() {
		return statusValue;
	}

	public synchronized void setStatusValue(String statusValue) {
		this.statusValue = statusValue;
	}

	public synchronized long getOpportunityID() {
		return opportunityID;
	}

	public synchronized void setOpportunityID(long opportunityID) {
		this.opportunityID = opportunityID;
	}

	public synchronized int getOpportunityState() {
		return opportunityState;
	}

	public synchronized void setOpportunityState(int opportunityState) {
		this.opportunityState = opportunityState;
	}

	public synchronized long getTimeCreated() {
		return timeCreated;
	}

	public synchronized void setTimeCreated(long timeCreated) {
		this.timeCreated = timeCreated;
	}

	public synchronized long getTimeClose() {
		return timeClose;
	}

	public synchronized void setTimeClose(long timeClose) {
		this.timeClose = timeClose;
	}

	public synchronized boolean isShowTimer() {
		return showTimer;
	}

	public synchronized void setShowTimer(boolean showTimer) {
		this.showTimer = showTimer;
	}

	public synchronized long getTimeLastInvest() {
		return timeLastInvest;
	}

	public synchronized void setTimeLastInvest(long timeLastInvest) {
		this.timeLastInvest = timeLastInvest;
	}

	public synchronized boolean isLastMinutesState() {
		return isLastMinutesState;
	}

	public synchronized void setLastMinutesState(boolean isLastMinutesState) {
		this.isLastMinutesState = isLastMinutesState;
	}

	public boolean isHasOpenOptions() {
		return hasOpenOptions;
	}

	public void setHasOpenOptions(boolean hasOpenOptions) {
		this.hasOpenOptions = hasOpenOptions;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public synchronized String getProfitPercent() {
		return profitPercent;
	}

	public synchronized void setProfitPercent(String profitPercent) {
		this.profitPercent = profitPercent;
	}

	public synchronized String getProfitText() {
		return profitText;
	}

	public synchronized void setProfitText(String profitText) {
		this.profitText = profitText;
	}
}
