package com.anyoption.android.app.fragment.trade;

import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.ChartEvent;
import com.anyoption.android.app.event.InvestmentEvent;
import com.anyoption.android.app.event.LightstreamerRegisterEvent;
import com.anyoption.android.app.event.LightstreamerUpdateEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.LongTermTradingTimeChosenEvent;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.event.OptionsEvent;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.lightstreamer.LightstreamerConnectionHandler;
import com.anyoption.android.app.lightstreamer.LightstreamerListener;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.anyoption.android.app.popup.TradeTooltipDialogFragment;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.service.results.InvestmentsMethodResult;
import com.anyoption.json.results.ChartDataMethodResult;
import com.lightstreamer.ls_client.UpdateInfo;

import java.text.ParseException;
import java.util.List;

public class AssetPageFragment extends NavigationalFragment implements
		LightstreamerConnectionHandler.StatusListener, LightstreamerListener, Callback {

	@SuppressWarnings("hiding")
	private static final String TAG = AssetPageFragment.class.getSimpleName();

	private static final String HEADER_FRAGMENT_TAG 		= "header_fragment_tag";
	private static final String CHART_FRAGMENT_TAG 			= "chart_fragment_tag";
	private static final String INFO_FRAGMENT_TAG 			= "info_fragment_tag";
	private static final String INVEST_FRAGMENT_TAG 		= "invest_fragment_tag";
	private static final String INVEST_DETAILS_FRAGMENT_TAG = "invest_details_fragment_tag";
	private static final String OPTIONS_FRAGMENT_TAG 		= "options_fragment_tag";
	private static final String TREND_FRAGMENT_TAG 			= "trend_fragment_tag";

	private static final int MESSAGE_SHOW_OPEN_OPTIONS 			= 1;
	private static final int MESSAGE_SHOW_UNAVAILABLE_INFO 		= 2;
	private static final int MESSAGE_SHOW_TOOLTIP 				= 3;

	private Market market;
	private long opportunityId;
	private int opportunityState;
	private long opportunityTimeClose;

	private boolean autoLoad;
	private boolean isTablet;
	private boolean isReloaded;
	private boolean isInvesting;
	private boolean reloadMarket;

	protected View root;
	protected ImageView assetBackgroundImageView;

	private FragmentManager childFragmentManager;
	protected ChartFragment chartFragment;
	private AssetOptionsFragment optionsFragment;
	protected AssetHeaderFragment headerFragment;
	private InvestFragment investFragment;
	private BaseAssetNestedFragment investDetailsFragment;
	private TrendFragment trendFragment;
	private AssetInfoFragment infoFragment;
	private View tooltipLayout;

	private Handler handler;
	private UpdateInfo nextUpdate;
	private LongTermTradingTimeChosenEvent.ChosenTime longTermTradingChosenTime;


	/**
	 * Creates new instance of this fragment.
	 */
	public static AssetPageFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Create new AssetPageFragment.");
		AssetPageFragment fragment = new AssetPageFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	/**
	 * Creates a bundle with arguments which have to be used to create fragment with arguments.
	 *
	 * @param market
	 * @param autoLoad
	 *            if true, the fragment will start loading the fragment immediately. Otherwise it
	 *            will wait for navigation event to Asset_PAGE.
	 * @return
	 */
	public static Bundle createArgsBundle(Market market, boolean autoLoad) {
		Bundle args = new Bundle();
		if (market == null) {
			Application application = Application.get();
			long lastOpenedMarketId = application.getSharedPreferencesManager().getLong(Constants.PREFERENCES_LAST_OPENED_MARKET, 0L);

			if (lastOpenedMarketId != 0L) {
				Market lastOpenedMarket = application.getMarketForID(lastOpenedMarketId);
				args.putSerializable(Constants.EXTRA_MARKET, lastOpenedMarket);

				if (lastOpenedMarket != null) {
					// Post event to Update Action Bar
					Bundle bundle = new Bundle();
					bundle.putSerializable(Constants.EXTRA_MARKET, lastOpenedMarket);
					bundle.putString(Constants.EXTRA_ACTION_BAR_TITLE, lastOpenedMarket.getDisplayName());
					bundle.putLong(Constants.EXTRA_MARKET_PRODUCT_TYPE, lastOpenedMarket.getProductTypeId());

					application.postEvent(new NavigationEvent(Screen.ASSET_PAGE,NavigationType.HORIZONTAL, bundle));
				}
			}
		} else {
			args.putSerializable(Constants.EXTRA_MARKET, market);
		}

		args.putBoolean(Constants.EXTRA_AUTO_LOAD_MARKET, autoLoad);

		return args;
	}

	/**
	 * Use {@link #newInstance(Market)} instead of this constructor.
	 */
	public AssetPageFragment() {
		// Do nothing
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		autoLoad = true;

		Bundle args = getArguments();
		if (args != null) {
			market = (Market) args.getSerializable(Constants.EXTRA_MARKET);
			autoLoad = args.getBoolean(Constants.EXTRA_AUTO_LOAD_MARKET, autoLoad);
		}

		isTablet = getResources().getBoolean(R.bool.isTablet);
		isReloaded = false;
		isInvesting = false;
		if (market != null) {
			reloadMarket = market.getDisplayName() == null;
		}

		handler = new Handler(this);
	}

	@SuppressWarnings("unchecked")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		root = inflater.inflate(R.layout.asset_page_fragment, container, false);

		assetBackgroundImageView = (ImageView) root.findViewById(R.id.asset_page_asset_bg_image_view);

		setUpAssetBackground();

		// Register for this event here before chart fragment is created.
		registerForEvents();

		childFragmentManager = getChildFragmentManager();
		FragmentTransaction fragmentTransaction = childFragmentManager.beginTransaction();


		if (childFragmentManager.findFragmentByTag(CHART_FRAGMENT_TAG) == null) {
			// ChartFragment creation
			Bundle args = new Bundle();
			args.putSerializable(Constants.EXTRA_MARKET, market);
			args.putBoolean(Constants.EXTRA_AUTO_LOAD_MARKET, autoLoad);
			if (reloadMarket) {
				args.putBoolean(Constants.EXTRA_RELOAD_MARKET_KEY, reloadMarket);
			}
			chartFragment = (ChartFragment) BaseFragment.newInstance(application.getSubClassForClass(ChartFragment.class), args);
			fragmentTransaction.add(R.id.chart_fragment, chartFragment, CHART_FRAGMENT_TAG);
		} else {
			chartFragment = (ChartFragment) childFragmentManager.findFragmentByTag(CHART_FRAGMENT_TAG);
		}

		if (childFragmentManager.findFragmentByTag(HEADER_FRAGMENT_TAG) == null) {
			// AssetHeaderFragment creation
			Bundle args = new Bundle();
			args.putSerializable(Constants.EXTRA_MARKET, market);
			headerFragment = (AssetHeaderFragment) BaseFragment.newInstance(application.getSubClassForClass(AssetHeaderFragment.class), args);
			fragmentTransaction.add(R.id.asset_header_fragment, headerFragment, HEADER_FRAGMENT_TAG);
		} else {
			headerFragment = (AssetHeaderFragment) childFragmentManager.findFragmentByTag(HEADER_FRAGMENT_TAG);
		}

		if (childFragmentManager.findFragmentByTag(INVEST_FRAGMENT_TAG) == null) {
			// InvestFragment creation
			Bundle args = new Bundle();
			args.putSerializable(Constants.EXTRA_MARKET, market);
			investFragment = (InvestFragment) BaseFragment.newInstance(application.getSubClassForClass(InvestFragment.class), args);
			fragmentTransaction.add(R.id.invest_fragment, investFragment, INVEST_FRAGMENT_TAG);
		} else {
			investFragment = (InvestFragment) childFragmentManager.findFragmentByTag(INVEST_FRAGMENT_TAG);
		}

		if (childFragmentManager.findFragmentByTag(INVEST_DETAILS_FRAGMENT_TAG) == null) {
			// InvestDetailsFragment creation
			Bundle args = new Bundle();
			args.putSerializable(Constants.EXTRA_MARKET, market);
			investDetailsFragment = (InvestDetailsFragment) BaseFragment.newInstance(application.getSubClassForClass(InvestDetailsFragment.class), args);
			fragmentTransaction.add(R.id.invest_details_fragment, investDetailsFragment, INVEST_DETAILS_FRAGMENT_TAG);
		} else {
			investDetailsFragment = (BaseAssetNestedFragment) childFragmentManager.findFragmentByTag(INVEST_DETAILS_FRAGMENT_TAG);
		}

		if (childFragmentManager.findFragmentByTag(TREND_FRAGMENT_TAG) == null) {
			// TrendFragment creation
			Bundle args = new Bundle();
			args.putSerializable(Constants.EXTRA_MARKET, market);
			trendFragment = (TrendFragment) BaseFragment.newInstance(application.getSubClassForClass(TrendFragment.class), args);
			fragmentTransaction.add(R.id.trend_view_fragment, trendFragment, TREND_FRAGMENT_TAG);
		} else {
			trendFragment = (TrendFragment) childFragmentManager.findFragmentByTag(TREND_FRAGMENT_TAG);
		}

		if (childFragmentManager.findFragmentByTag(OPTIONS_FRAGMENT_TAG) == null) {
			// AssetOptionsFragment creation
			Bundle args = new Bundle();
			args.putSerializable(Constants.EXTRA_MARKET, market);
			args.putBoolean(Constants.EXTRA_AUTO_LOAD_MARKET, autoLoad);
			optionsFragment = (AssetOptionsFragment) BaseFragment.newInstance(application.getSubClassForClass(AssetOptionsFragment.class), args);
			fragmentTransaction.add(R.id.options_drawer_fragment, optionsFragment, OPTIONS_FRAGMENT_TAG);
		} else {
			optionsFragment = (AssetOptionsFragment) childFragmentManager.findFragmentByTag(OPTIONS_FRAGMENT_TAG);
		}
		if (!isTablet) {
			fragmentTransaction.hide(optionsFragment);
		}

		if (childFragmentManager.findFragmentByTag(INFO_FRAGMENT_TAG) == null) {
			// AssetOptionsFragment creation
			Bundle args = new Bundle();
			args.putSerializable(Constants.EXTRA_MARKET, market);
			args.putBoolean(Constants.EXTRA_AUTO_LOAD_MARKET, autoLoad);
			infoFragment = (AssetInfoFragment) BaseFragment.newInstance(application.getSubClassForClass(AssetInfoFragment.class), args);
			fragmentTransaction.add(R.id.info_drawer_fragment, infoFragment, INFO_FRAGMENT_TAG);
		} else {
			infoFragment = (AssetInfoFragment) childFragmentManager.findFragmentByTag(INFO_FRAGMENT_TAG);
		}

		fragmentTransaction.commit();

		opportunityState = 0;

		return root;
	}

	private void setupTooltip() {
		boolean shouldShow = !application.getSharedPreferencesManager().getBoolean(
				Constants.PREFERENCES_IS_TRADE_TOOLTIP_SHOWN, false);

		if (shouldShow) {
			handler.obtainMessage(MESSAGE_SHOW_TOOLTIP).sendToTarget();
		}
	}

	private void showTooltip() {
		if (isTablet) {
			if (tooltipLayout == null) {
				ViewStub stub = (ViewStub) root.findViewById(R.id.asset_page_tooltip);
				stub.setLayoutResource(R.layout.trade_tooltip_layout);
				tooltipLayout = stub.inflate();
			} else {
				tooltipLayout.setVisibility(View.VISIBLE);
			}

			tooltipLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					tooltipLayout.setVisibility(View.GONE);
					application.getSharedPreferencesManager().putBoolean(Constants.PREFERENCES_IS_TRADE_TOOLTIP_SHOWN, true);
				}
			});
		} else {
			application.getFragmentManager().showDialogFragment(TradeTooltipDialogFragment.class, new Bundle());
			application.getSharedPreferencesManager().putBoolean(Constants.PREFERENCES_IS_TRADE_TOOLTIP_SHOWN, true);
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		if (isPaused) {
			isPaused = false;
			reload();
		}
	}

	@Override
	public void onPause() {
		unregisterLightStreamer();
		super.onPause();
	}

	@Override
	public void onDestroy() {
		unregisterLightStreamer();
		application.unregisterForAllEvents(this);
		super.onDestroy();
	}

	private void registerForEvents() {
		if (!application.isRegistered(this)) {
			application.registerForEvents(this, NavigationEvent.class, ChartEvent.class, LoginLogoutEvent.class, OptionsEvent.class, InvestmentEvent.class, LongTermTradingTimeChosenEvent.class);
		}
	}

	//////////////////////////////////////////////////////////////////////////////////
	//                              LIGHTSTREAMER                                   //
	//////////////////////////////////////////////////////////////////////////////////

	private void registerLightstreamer(ChartDataMethodResult chartData) {
		Log.d(TAG, "registerLightstreamer -> market: " + chartData.getMarketName());
		String group = null;
//		opportunityId = 0L;
//		opportunityState = 0;
		if (chartData.getOpportunityTypeId() == Opportunity.TYPE_REGULAR) {
			group = "aotps_" + chartData.getScheduled() + "_" + chartData.getMarketId();
		}
		else {
			group = "op_" + chartData.getMarketId();
		}

		application.getLightstreamerManager().subscribeAssetPageTable(market.getId(), this, new String[] { group });
	}

	private void registerLightstreamer(LongTermTradingTimeChosenEvent.ChosenTime chosenTime) {
		Log.d(TAG, "registerLightstreamer -> market: " + market.getDisplayName());

		application.postEvent(new LightstreamerRegisterEvent(LightstreamerRegisterEvent.Type.SUBSCRIBE));

		opportunityId = 0L;
		opportunityState = 0;

		int aotpsTime = LightstreamerManager.OPPORTUNITY_SCHEDULED_HOURLY;

		if(chosenTime == LongTermTradingTimeChosenEvent.ChosenTime.NEAREST) {
			aotpsTime = LightstreamerManager.OPPORTUNITY_SCHEDULED_HOURLY;
		} else if(chosenTime == LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_1) {
			aotpsTime = LightstreamerManager.OPPORTUNITY_SCHEDULED_HOURLY;
		} else if(chosenTime == LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_2) {
			aotpsTime = LightstreamerManager.OPPORTUNITY_SCHEDULED_HOURLY;
		} else if(chosenTime == LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_3) {
			aotpsTime = LightstreamerManager.OPPORTUNITY_SCHEDULED_HOURLY;
		} else if(chosenTime == LongTermTradingTimeChosenEvent.ChosenTime.DAILY) {
			aotpsTime = LightstreamerManager.OPPORTUNITY_SCHEDULED_DAILY;
		} else if(chosenTime == LongTermTradingTimeChosenEvent.ChosenTime.WEEKLY) {
			aotpsTime = LightstreamerManager.OPPORTUNITY_SCHEDULED_WEEKLY;
		} else if(chosenTime == LongTermTradingTimeChosenEvent.ChosenTime.MONTHLY) {
			aotpsTime = LightstreamerManager.OPPORTUNITY_SCHEDULED_MONTHLY;
		}

		String group = "aotps_" + aotpsTime + "_" + market.getId();

		application.getLightstreamerManager().subscribeAssetPageTable(market.getId(), this, new String[] { group });
	}

	private void unregisterLightStreamer() {
		Log.d(TAG, "unregisterLightStreamer");
		application.postEvent(new LightstreamerRegisterEvent(LightstreamerRegisterEvent.Type.UNSUBSCRIBE));
		if(market != null){
			application.getLightstreamerManager().unsubscribeAssetPageTable(market.getId());
		}
	}

	@Override
	public void updateItem(int itemPos, String itemName, UpdateInfo update) {

		if (market == null || isInvesting) {
			return;
		}

		long updateMarketId = Long.parseLong(update.getNewValue(LightstreamerManager.COLUMN_ET_NAME));
		if (updateMarketId != market.getId()) {
			Log.w(TAG, "LS Update for OTHER MARKET. id=" + updateMarketId);
			return;
		}

		String opportunitySuspendState = update.getNewValue(LightstreamerManager.COLUMN_AO_OPP_STATE);
		if (opportunitySuspendState.equals(LightstreamerManager.OPPORTUNITY_SUSPEND_STATE_TRUE)) {
			//Market is susspended:
			handler.obtainMessage(MESSAGE_SHOW_UNAVAILABLE_INFO).sendToTarget();
			application.postEvent(new LightstreamerUpdateEvent(market.getId(), update));
			return;
		}

		long newOpportunityId = Long.parseLong(update.getNewValue(LightstreamerManager.COLUMN_ET_OPP_ID));
		int newOpportunityState = Integer.parseInt(update.getNewValue(LightstreamerManager.COLUMN_ET_STATE));
		String timeCloseString = update.getNewValue(LightstreamerManager.COLUMN_ET_EST_CLOSE);

		if (opportunityId == 0L) {
			opportunityId = newOpportunityId;
			try {
				opportunityTimeClose = TimeUtils.parseTimeFromLSUpdate(timeCloseString).getTime();
			} catch (ParseException e) {
				Log.e(TAG, "Could not parse ET_EST_CLOSE time");
			}

		} else if (opportunityId != newOpportunityId) {
			// The update is for other opportunity.
			// Save the other Update:
			nextUpdate = update;

			// Check if the current opportunity is in Waiting for Expire state:
			if (opportunityState == Opportunity.STATE_CLOSING || opportunityState == Opportunity.STATE_CLOSING_1_MIN) {
				long timeClose = 0L;
				try {
					timeClose = TimeUtils.parseTimeFromLSUpdate(timeCloseString).getTime();
				} catch (ParseException e) {
					Log.e(TAG, "Could not parse ET_EST_CLOSE time");
				}
				if (opportunityTimeClose < timeClose) {
					//The update is for the Next opportunity:
					//Check if the User has any open options in the current opportunity:
					List<Investment> investments = application.getInvestmentsForOpportunity(opportunityId);
					if (investments == null || investments.isEmpty()) {
						opportunityId = newOpportunityId;
						opportunityState = 0;
						opportunityTimeClose = 0L;
						application.getCurrentActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								reload();
							}
						});
						return;
					}
				} else {
					return;
				}
			} else {
				return;
			}
		}

		if (opportunityState == 0 || update.isValueChanged(LightstreamerManager.COLUMN_ET_STATE)) {
			opportunityState = newOpportunityState;
			try {
				opportunityTimeClose = TimeUtils.parseTimeFromLSUpdate(timeCloseString).getTime();
			} catch (ParseException e) {
				Log.e(TAG, "Could not parse ET_EST_CLOSE time");
			}
			if (opportunityState == Opportunity.STATE_CREATED || opportunityState == Opportunity.STATE_SUSPENDED) {
				handler.obtainMessage(MESSAGE_SHOW_UNAVAILABLE_INFO).sendToTarget();
			} else {
				setupTooltip();
				int opportunityOldState;
				try {
					opportunityOldState = Integer.parseInt(update.getOldValue(LightstreamerManager.COLUMN_ET_STATE));
				} catch (NumberFormatException ex) {
					// Could not parse old value of ET_STATE
					opportunityOldState = Opportunity.STATE_CREATED;
				}

				if (opportunityOldState == Opportunity.STATE_CREATED) {
					// Show open option info if the asset state changes from unavailable to
					// available.
					handler.obtainMessage(MESSAGE_SHOW_OPEN_OPTIONS).sendToTarget();
				}
			}
		}

		application.postEvent(new LightstreamerUpdateEvent(market.getId(), update));

	}

	@Override
	public void onStatusChange(int status) {}

	//////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////

	@Override
	public boolean handleMessage(Message msg) {
		switch (msg.what) {
			case MESSAGE_SHOW_OPEN_OPTIONS:
				showOpenOptionInfo();
				break;
			case MESSAGE_SHOW_UNAVAILABLE_INFO:
				showUnavailableInfo();
				break;
			case MESSAGE_SHOW_TOOLTIP:
				showTooltip();
				break;
		}

		return true;
	}


	//////////////////////////////////////////////////////////////////////////////////
	//                              EVENTs                                          //
	//////////////////////////////////////////////////////////////////////////////////

	public void onEventMainThread(NavigationEvent event) {
		Market eventMarket = null;
		if (event.getToScreen() == Screen.ASSET_PAGE) {
			Bundle args = event.getBundle();

			if (args != null) {
				eventMarket = (Market) args.getSerializable(Constants.EXTRA_MARKET);
			}
		}

		if (isTablet) {
			if (event.getToScreen() == Screen.ASSET_PAGE) {
				if(eventMarket != null && eventMarket.getId() != market.getId()){
					longTermTradingChosenTime = LongTermTradingTimeChosenEvent.ChosenTime.NEAREST;
					unregisterLightStreamer();
					loadMarket(eventMarket);
					setUpAssetBackground();
					reloadMarket = eventMarket.getDisplayName() == null;
					application.getSharedPreferencesManager().putLong(Constants.PREFERENCES_LAST_OPENED_MARKET, eventMarket.getId());
				}
			}
		} else {
			if (event.getToScreen() == Screen.ASSET_PAGE) {
				if (market != null && eventMarket.getId() == market.getId() && !autoLoad) {
					chartFragment.loadData();
				}
			} else {
				unregisterLightStreamer();
			}
		}
	}

	public void onEventMainThread(ChartEvent event) {
		if(market == null || reloadMarket) {
			if (chartFragment.getMarket() != null) {
				reloadMarket = false;
				market = chartFragment.getMarket();
				headerFragment.setMarket(market);
				investDetailsFragment.setMarket(market);
				if(trendFragment != null){
					trendFragment.setMarket(market);
				}
				infoFragment.setMarket(market);
				if (investFragment != null) {
					investFragment.setMarket(market);
					investFragment.loadData();
				}
				optionsFragment.setMarket(market);
				optionsFragment.loadData();
				infoFragment.loadData();
			}
		} else if (event.getMarketId() != market.getId()) {
			return;
		}

		if (event.getType() == ChartEvent.Type.DATA_REQUESTED) {
			unregisterLightStreamer();
		} else if (event.getType() == ChartEvent.Type.DATA_REQUEST_SUCCESS) {
			ChartDataMethodResult data = event.getChartData();
			opportunityId = data.getOpportunityId();
			opportunityState = 0;

			registerLightstreamer(data);
		}

		if (event.getType() == ChartEvent.Type.DATA_REQUEST_SUCCESS
				|| event.getType() == ChartEvent.Type.DATA_REQUEST_FAIL) {
			if (event.getType() == ChartEvent.Type.DATA_REQUEST_SUCCESS
					&& event.getChartData().getInvestments() != null
					&& event.getChartData().getInvestments().length > 0) {
				showOptionsFragment();
			}

			// Load the information for these components.
			if (!autoLoad || isReloaded) {
				isReloaded = false;
				if(investFragment != null){
					investFragment.loadData();
				}
				infoFragment.loadData();
			}

			// Reload the investments on every new chart data.
			// This way opened options will go to the list with settled.
			optionsFragment.loadData();
		}
	}

	public void onEventMainThread(LoginLogoutEvent event){
		reload();
	}

	public void onEventMainThread(LongTermTradingTimeChosenEvent event) {
		if(market.getId() != event.getMarketId()){
			return;
		}

		longTermTradingChosenTime = event.getTime();

		unregisterLightStreamer();
		registerLightstreamer(event.getTime());

		if (longTermTradingChosenTime != LongTermTradingTimeChosenEvent.ChosenTime.NEAREST && longTermTradingChosenTime != LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_1 && longTermTradingChosenTime != LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_2 && longTermTradingChosenTime != LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_3) {
			setUpLongTermTradingBackground();
		} else {
			setUpAssetBackground();
		}
	}

	public void onEventMainThread(OptionsEvent event) {
		if (market == null || event.getMarketId() != market.getId()) {
			return;
		}

		if (event.getType() == OptionsEvent.Type.SETTLED_OPTION_RECEIVED) {
			InvestmentsMethodResult result = event.getResult();
			if (result.getInvestments() != null && result.getInvestments().length > 0) {
				showOptionsFragment();
			}
		}
	}

	public void onEventMainThread(InvestmentEvent event) {
		Log.d(TAG, "New InvestmentEvent");
		if (event.getMarketId() != market.getId()) {
			return;
		}

		if (event.getType() == InvestmentEvent.Type.INVEST_SUCCESS && !optionsFragment.isVisible()) {
			showOptionsFragment();
		}

		if (event.getType() == InvestmentEvent.Type.INVEST_REQUESTED) {
			isInvesting = true;
		} else if (event.getType() == InvestmentEvent.Type.INVEST_SUCCESS
				|| event.getType() == InvestmentEvent.Type.INVEST_FAIL) {
			isInvesting = false;
		}
	}

	///////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////

	@Override
	protected Screenable setupScreen() {
		return Screen.ASSET_PAGE;
	}


	public void loadMarket(Market newMarket) {
		if (market == null || (newMarket != null && newMarket.getId() != market.getId())) {
			opportunityId = 0L;
			opportunityState = 0;
			isInvesting = false;

			isReloaded = true;
			infoFragment.closeDrawer();

			market = newMarket;
			unregisterLightStreamer();
			headerFragment.setMarket(newMarket);
			headerFragment.setAutoLoad(false);
			if(investFragment != null){
				investFragment.setMarket(newMarket);
				investFragment.setAutoLoad(false);
			}
			investDetailsFragment.setMarket(newMarket);
			investDetailsFragment.setAutoLoad(false);
			optionsFragment.setMarket(newMarket);
			optionsFragment.setAutoLoad(false);
			if(trendFragment != null){
				trendFragment.setMarket(newMarket);
				trendFragment.setAutoLoad(false);
			}
			infoFragment.setMarket(newMarket);
			infoFragment.setAutoLoad(false);
			chartFragment.setMarket(newMarket);
			chartFragment.setAutoLoad(false);
		}
	}

	public void reload() {
		if (optionsFragment != null || chartFragment != null) {
			isReloaded = true;
			optionsFragment.clearOptions();
			chartFragment.loadData();
		}
		if (trendFragment != null) {
			trendFragment.reload(nextUpdate);
		}
	}

	/**
	 * Sets the layout for open state.
	 */
	private void showOpenOptionInfo() {
		if (!isAdded()) {
			return;
		}

		if(		longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.DAILY ||
				longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.WEEKLY ||
				longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.MONTHLY){
			setUpLongTermTradingBackground();
		} else {
			setUpAssetBackground();
		}

		if (isTablet) {
			FragmentTransaction transaction = childFragmentManager.beginTransaction();
			if(investFragment != null){
				transaction.show(investFragment);
			}
			if(trendFragment != null){
				transaction.show(trendFragment);
			}
			transaction.show(investDetailsFragment);
			transaction.commitAllowingStateLoss();

			root.findViewById(R.id.invest_details_fragment).setVisibility(View.VISIBLE);
			root.findViewById(R.id.options_drawer_right_border).setVisibility(View.VISIBLE);

			optionsFragment.setTotalOptionsVisibility(View.GONE);
		} else {
			optionsFragment.closeDrawer();
			optionsFragment.setTotalOptionsVisibility(View.GONE);
			optionsFragment.setHandleVisibility(View.VISIBLE);
			View assetOptionsView = root.findViewById(R.id.options_drawer_fragment);
			LayoutParams optionsViewParams = (LayoutParams) assetOptionsView.getLayoutParams();
			int optionsViewHeight = (int) getResources().getDimension(R.dimen.asset_options_fragment_height);
			int optionsViewMargin = (int) getResources().getDimension(R.dimen.trend_fragment_height);
			optionsViewParams.height = optionsViewHeight;
			optionsViewParams.bottomMargin = optionsViewMargin;
			assetOptionsView.setLayoutParams(optionsViewParams);
			assetOptionsView.invalidate();
		}
	}

	/**
	 * Sets the layout for unavailable state.
	 */
	private void showUnavailableInfo() {
		if (!isAdded()) {
			return;
		}

		setUpUnavailableBackground();
		showOptionsFragment();
		optionsFragment.setTotalOptionsVisibility(View.VISIBLE);

		if (isTablet) {
			FragmentTransaction transaction = childFragmentManager.beginTransaction();
			if(investFragment != null){
				transaction.hide(investFragment);
			}
			if(trendFragment != null){
				transaction.hide(trendFragment);
			}
			transaction.hide(investDetailsFragment);
			transaction.commit();

			root.findViewById(R.id.invest_details_fragment).setVisibility(View.GONE);
			root.findViewById(R.id.options_drawer_right_border).setVisibility(View.GONE);
		} else {
			optionsFragment.openDrawer();
			optionsFragment.setHandleVisibility(View.GONE);

			View assetOptionsView = root.findViewById(R.id.options_drawer_fragment);
			LayoutParams optionsViewParams = (LayoutParams) assetOptionsView
					.getLayoutParams();

			int newOptionsViewHeight = (int) getResources().getDimension(
					R.dimen.asset_options_unavailable_fragment_height);

			optionsViewParams.height = newOptionsViewHeight;
			optionsViewParams.bottomMargin = 0;
			assetOptionsView.setLayoutParams(optionsViewParams);
			assetOptionsView.invalidate();
		}
	}

	private void showOptionsFragment() {
		if (optionsFragment != null && !optionsFragment.isVisible()) {
			childFragmentManager.beginTransaction().show(optionsFragment).commit();
		}
	}

	/**
	 * Changes the background depending on the asset.
	 */
	protected void setUpAssetBackground() {
		assetBackgroundImageView.setImageDrawable(DrawableUtils.getChartBackground(market));
	}

	/**
	 * Changes the background to unavailable.
	 */
	protected void setUpUnavailableBackground() {
		assetBackgroundImageView.setImageResource(R.drawable.trade_asset_background_not_available);
	}

	/**
	 * Changes the background to long term trading.
	 */
	protected void setUpLongTermTradingBackground() {
		assetBackgroundImageView.setImageResource(R.drawable.bg_longterms);
	}

}
