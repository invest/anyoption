package com.anyoption.android.app.fragment.trade;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.constants.Constants;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TabletTradeFragment extends NavigationalFragment {

	public static final String TAG = TabletTradeFragment.class.getSimpleName();

	private static final String MAIN_TRADE_FRAGMENT_TAG 			= "main_trade_fragment_tag";
	private static final String ASSET_PAGE_FRAGMENT_TAG 			= "asset_page_fragment_tag";
	private static final String ASSET_DYNAMICS_PAGE_FRAGMENT_TAG 	= "asset_dynamics_page_fragment_tag";

	private View root;

	private TradeFragment mainTradeFragment;
	private AssetPageFragment assetPageFragment;
	private AssetPageDynamicsFragment assetDynamicsPageFragment;

	private boolean isDynamics;

	public String filterTab;

	@Override
	protected Screenable setupScreen() {
		return Screen.TABLET_TRADE;
	}

	/**
	 * Create new instance of this Fragment
	 */
	public static TabletTradeFragment newInstance() {
		return newInstance(new Bundle());
	}

	public static TabletTradeFragment newInstance(Bundle bundle) {
		TabletTradeFragment fragment = new TabletTradeFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = getArguments();
		if(bundle != null){
			filterTab = bundle.getString(Constants.EXTRA_ASSETS_LIST_FILTER);
		}
		application.registerForEvents(this, NavigationEvent.class);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView");
		// Inflate the layout for this fragment
		root = inflater.inflate(R.layout.tablet_trade_fragment, container, false);

		FragmentManager childFragmentManager = getChildFragmentManager();
		FragmentTransaction fragmentTransaction = childFragmentManager.beginTransaction();

		if (childFragmentManager.findFragmentByTag(MAIN_TRADE_FRAGMENT_TAG) == null) {
			// TradeFragment creation
			mainTradeFragment = TradeFragment.newInstance(null);
			fragmentTransaction.add(R.id.trade_fragment, mainTradeFragment, MAIN_TRADE_FRAGMENT_TAG);

			if(filterTab != null && filterTab.length() > 0) {
				Bundle filterBundle = new Bundle();
				filterBundle.putString(Constants.EXTRA_ASSETS_LIST_FILTER, filterTab);
				mainTradeFragment.setArguments(filterBundle);
				filterTab = "";
				getArguments().putString(Constants.EXTRA_ASSETS_LIST_FILTER, "");
			}
		} else {
			mainTradeFragment = (TradeFragment) childFragmentManager.findFragmentByTag(MAIN_TRADE_FRAGMENT_TAG);
		}

		if (childFragmentManager.findFragmentByTag(ASSET_PAGE_FRAGMENT_TAG) == null) {
			// AssetPageFragment creation
			assetPageFragment = (AssetPageFragment) BaseFragment.newInstance(application.getSubClassForClass(AssetPageFragment.class), new Bundle());
			fragmentTransaction.add(R.id.asset_page_fragment, assetPageFragment,ASSET_PAGE_FRAGMENT_TAG);
			isDynamics = false;
		} else {
			assetPageFragment = (AssetPageFragment) childFragmentManager.findFragmentByTag(ASSET_PAGE_FRAGMENT_TAG);
		}

		// Commit all fragment changes.
		fragmentTransaction.commit();

		return root;
	}

	public void onEventMainThread(NavigationEvent event) {
		Log.d(TAG, "On NavigationEvent : " + event.getToScreen());

		if(event.getToScreen() == Screen.ASSET_PAGE){

			Bundle bundle = event.getBundle();

			FragmentManager childFragmentManager = getChildFragmentManager();
			FragmentTransaction fragmentTransaction = childFragmentManager.beginTransaction();
			long marketProductType = event.getBundle().getLong(Constants.EXTRA_MARKET_PRODUCT_TYPE);

			if(marketProductType == Constants.PRODUCT_TYPE_ID_DYNAMICS){

				if (childFragmentManager.findFragmentByTag(ASSET_DYNAMICS_PAGE_FRAGMENT_TAG) == null) {
					// AssetPageFragment creation
					assetDynamicsPageFragment = (AssetPageDynamicsFragment) BaseFragment.newInstance(application.getSubClassForClass(AssetPageDynamicsFragment.class), bundle);
				} else {
					assetDynamicsPageFragment = (AssetPageDynamicsFragment) childFragmentManager.findFragmentByTag(ASSET_DYNAMICS_PAGE_FRAGMENT_TAG);
					assetDynamicsPageFragment.loadMarket(bundle);
				}

				if(!isDynamics){
					fragmentTransaction.remove(assetPageFragment);
					fragmentTransaction.add(R.id.asset_page_fragment, assetDynamicsPageFragment,ASSET_DYNAMICS_PAGE_FRAGMENT_TAG);
				}

				isDynamics = true;
			}else{

				if (childFragmentManager.findFragmentByTag(ASSET_PAGE_FRAGMENT_TAG) == null) {
					// AssetPageFragment creation
					assetPageFragment = (AssetPageFragment) BaseFragment.newInstance(application.getSubClassForClass(AssetPageFragment.class), bundle);
				} else {
					assetPageFragment = (AssetPageFragment) childFragmentManager.findFragmentByTag(ASSET_PAGE_FRAGMENT_TAG);
					assetPageFragment.getArguments().putAll(bundle);
//					assetPageFragment.setArguments(bundle);
				}

				if(isDynamics){
					fragmentTransaction.remove(assetDynamicsPageFragment);
					fragmentTransaction.add(R.id.asset_page_fragment, assetPageFragment,ASSET_PAGE_FRAGMENT_TAG);
				}

				isDynamics = false;
			}
			// Commit all fragment changes.
			fragmentTransaction.commit();

		}
	}

}

