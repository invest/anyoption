package com.anyoption.android.app.model.questionnaire;

import com.anyoption.common.beans.base.QuestionnaireAnswer;
import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;

public class YesNoQuestion extends SingleChoiceQuestion {

	private static final int YES_ANSWER_POSITION = 0;
	private static final int NO_ANSWER_POSITION = 1;

	public YesNoQuestion(long orderId, String quetionText, QuestionnaireQuestion question,
			QuestionnaireUserAnswer answer) {
		super(orderId, quetionText, question, answer);
	}

	public YesNoQuestion(long orderId, String quetionText) {
		super(orderId, quetionText);
	}

	@Override
	public boolean isAnswered() {
		if (userAnswer != null && userAnswer.getAnswerId() != null) {
			return true;
		}

		return false;
	}

	public QuestionnaireAnswer getYesAnswer() {
		if (question != null) {
			return question.getAnswers().get(YES_ANSWER_POSITION);
		}

		return null;
	}

	public QuestionnaireAnswer getNoAnswer() {
		if (question != null) {
			return question.getAnswers().get(NO_ANSWER_POSITION);
		}

		return null;
	}

}
