package com.anyoption.android.app.fragment;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import com.anyoption.android.app.Application;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

/**
 * Custom {@link Fragment} class that contains the basic and common features of all fragments in the application.
 * @author Anastas Arnaudov
 *
 */
public abstract class BaseFragment extends Fragment{

	public static final String TAG = BaseFragment.class.getSimpleName();
	
	protected Application application;
	protected boolean isPaused = false;
	
	public BaseFragment(){
		super();
		this.application = Application.get();
	}

	/**
	 * This method uses reflection to create new instance of the given BaseFragment sub-class.
	 * @param fragmentClass The class of the BaseFragment subclass.
	 * @param bundle The bundle with arguments.
	 * @return
	 */
	public static BaseFragment newInstance(Class<? extends BaseFragment> fragmentClass, Bundle bundle){
		BaseFragment fragment = null;
		try {
			Method method = fragmentClass.getDeclaredMethod("newInstance", Bundle.class);
			fragment = (BaseFragment) method.invoke(null, bundle);
		} catch (Exception e) {
			Log.e(TAG, "Reflection error.", e);
		}
		return fragment;
	}

	@Override
	public void onDetach() {
		//There seems to be a bug in the newly added support for nested fragments. 
		//Basically, the child FragmentManager ends up with a broken internal state when it is detached from the activity.
		try {
		    Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
		    childFragmentManager.setAccessible(true);
		    childFragmentManager.set(this, null);

		} catch (NoSuchFieldException e) {
		    throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
		    throw new RuntimeException(e);
		}
		super.onDetach();
	}
	
	
	@Override
	public void onPause() {
		super.onPause();
		isPaused  = true;
	}
}
