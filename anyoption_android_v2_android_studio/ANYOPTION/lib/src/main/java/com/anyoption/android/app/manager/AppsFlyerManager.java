package com.anyoption.android.app.manager;

import java.util.Map;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.DepositEvent;
import com.anyoption.android.app.event.InstallEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.LoginLogoutEvent.Type;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;

import android.util.Log;

/**
 * Manager for the AppsFlyer logic : integration, messaging etc.
 * <p>AppsFlyer API is used for tracking several events that occur in the application : 
 * <li>Application Installation 
 * <li>User Registration
 * <li>User Deposit
 * 
 * @author Anastas Arnaudov
 *
 */
public class AppsFlyerManager implements AppsFlyerConversionListener{

	public static final String TAG = AppsFlyerManager.class.getSimpleName();

	public static final String DEV_KEY_KEY 						= "AppsFlyerKey";
	public static final String DEV_KEY_VALUE 					= "yKuSU5NT3GncHqJypahqBS";
	
	public static final String REGISTRATION_KEY 				= "registration";
	public static final String LOGIN_KEY		 				= "login";
	public static final String DEPOSIT_KEY 						= "deposit";
	
	public static final String PARAM_IS_FACEBOOK 				= "is_fb";
	public static final String PARAM_MEDIA_SOURCE				= "media_source";
	public static final String PARAM_CAMPAIGN_ID 				= "campaign_id";
	public static final String PARAM_FACEBOOK_CAMPAIGN_ID 		= "FB_campaign_id";
	public static final String PARAM_ADSET_ID 					= "adset_id";
	public static final String PARAM_FACEBOOK_ADSET_ID 			= "fb_adset_id";
	public static final String PARAM_AD_ID 						= "ad_id";
	public static final String PARAM_FACEBOOK_AD_ID 			= "fb_ad_id";
	public static final String PARAM_ADSET						= "adset";
	public static final String PARAM_CAMPAIGN					= "campaign";
	public static final String PARAM_FACEBOOK_ADSET				= "fb_Adset";
	public static final String PARAM_COMBINATION_ID				= "af_siteid";
	public static final String PARAM_ADGROUP_ID					= "adgroup_id";
	public static final String PARAM_FACEBOOK_ADGROUP_ID		= "fb_adgroup_id";

	public static final String MEDIA_SOURCE_TWITTER				= "Twitter";
	public static final String MEDIA_SOURCE_FACEBOOK				= "Facebook";
	
	private Application application;
	
	/**
	 * {@link AppsFlyerManager}
	 * @param application
	 */
	public AppsFlyerManager(Application application){
		Log.d(TAG, "On Create");
		this.application = application;
		application.registerForEvents(this, LoginLogoutEvent.class, DepositEvent.class);
		
		init();	
	}
	
	
	/**
	 * Initial setup.
	 * <p><li>Sets the DevKey if it is not set. 
	 */
	private void init() {
		if(!isDevKeySet()){
			setDevKey();
		}
		AppsFlyerLib.sendTracking(application); 
		AppsFlyerLib.registerConversionListener(application, this);
	}

	public String getAppsFlyerUID(){
		return AppsFlyerLib.getAppsFlyerUID(application);
	}
	
	/**
	 * Returns the Dev Key.
	 * @return
	 */
	public String getDevKey(){
		return AppsFlyerManager.DEV_KEY_VALUE;
	}
	
	/**
	 * Checks whether the DevKey is set.
	 * @return
	 */
	public boolean isDevKeySet(){
		return ( AppsFlyerLib.getProperty(DEV_KEY_KEY) != null && AppsFlyerLib.getProperty(DEV_KEY_KEY).length() > 0 );
	}
	
	/**
	 * Sets the DevKey.
	 */
	public void setDevKey(){
		Log.d(TAG, "Setting the Dev Key.");
		AppsFlyerLib.setAppsFlyerKey(getDevKey());
	}

	/**
	 * Sends event message to the AppsFlyer tracker.
	 * @param key
	 * @param value
	 */
	public void sendEvent(String key, String value){
		Log.d(TAG, "Sending Event " + key);
		AppsFlyerLib.sendTrackingWithEvent(application, key, value);
	}

	/**
	 * Sends a Register Event to the AppsFlyer tracker.
	 */
	public void sendDepositEvent(String amount){
		sendEvent(DEPOSIT_KEY, amount);
	}

	/**
	 * Sends a Register Event to the AppsFlyer tracker.
	 */
	public void sendRegisterEvent(){
		sendEvent(REGISTRATION_KEY, String.valueOf(application.getUser().getId()));
	}

	/**
	 * Sends a Login Event to the AppsFlyer tracker.
	 */
	public void sendLoginEvent(){
		sendEvent(LOGIN_KEY, String.valueOf(application.getUser().getId()));
	}

	/**
	 * Called when a {@link LoginLogoutEvent} occurs.
	 */
	public void onEvent(LoginLogoutEvent event) {
		if(event.getType() == Type.REGISTER){
			sendRegisterEvent();
		}else if (event.getType() == Type.LOGIN){
			sendLoginEvent();
		}
	}
	
	/**
	 * Called when a {@link DepositEvent} occurs.
	 */
	public void onEvent(DepositEvent event) {
		//Checks if the user is a TESTER:
		if(!application.isUserTester()){
			//Send only the First Deposit:
			if(event.isFirstDeposit()){
				//Send the dollar amount:
				sendDepositEvent(String.valueOf(event.getDollarAmount()));
			}			
		}
	}

	@Override
	public void onInstallConversionDataLoaded(Map<String, String> map) {
		Log.d(TAG, "onInstallConversionDataLoaded");
		Log.d(TAG, "Printing map...");
		for(Map.Entry entry : map.entrySet()){
			Log.d(TAG, "key = " + entry.getKey() + "  value = " + entry.getValue());
		}

		Log.d(TAG, "Checking if it is a FACEBOOK campaign...");

		/*boolean isFacebook = map.containsKey(PARAM_IS_FACEBOOK) && Boolean.valueOf(map.get(PARAM_IS_FACEBOOK));

		if(!isFacebook) {
			isFacebook = map.containsKey(PARAM_MEDIA_SOURCE) && map.get(PARAM_MEDIA_SOURCE).toLowerCase().contains(MEDIA_SOURCE_FACEBOOK.toLowerCase());
		}*/

		boolean isFacebook = map.containsKey(PARAM_MEDIA_SOURCE) && map.get(PARAM_MEDIA_SOURCE).toLowerCase().contains(MEDIA_SOURCE_FACEBOOK.toLowerCase());

		boolean isTwitter = map.containsKey(PARAM_MEDIA_SOURCE) && map.get(PARAM_MEDIA_SOURCE).equalsIgnoreCase(MEDIA_SOURCE_TWITTER);
		
		if(isFacebook || isTwitter){
			Log.d(TAG, isFacebook ? "FACEBOOK campaign." : "TWITTER campaign.");
			SharedPreferencesManager sharedPreferencesManager = Application.get().getSharedPreferencesManager();
			
			Log.d(TAG, "Getting the parameters we need...");
			String campaignId = map.get(PARAM_CAMPAIGN_ID);
			String adsetId = map.get(PARAM_ADSET_ID);
			String adId = map.get(PARAM_AD_ID);
			String adset = map.get(PARAM_ADSET);
			String campaign = map.get(PARAM_CAMPAIGN);
			String adgroupId = map.get(PARAM_ADGROUP_ID);
			
			
			Log.d(TAG, "Building the DP...");
			StringBuilder dpBuilder = new StringBuilder().
			append(PARAM_FACEBOOK_CAMPAIGN_ID).append("=").append(campaignId).append("&").
			append(PARAM_FACEBOOK_ADSET_ID).append("=").append(adsetId).append("&").
			append(PARAM_FACEBOOK_AD_ID).append("=").append(adId).append("&").
			append(PARAM_FACEBOOK_ADGROUP_ID).append("=").append(adgroupId).append("&").
			
			append(PARAM_FACEBOOK_ADSET).append("=");
			if (isTwitter) {
				dpBuilder.append(campaign);
			} else {
				dpBuilder.append(adset);
			}

			long combinationID = 0l;

			try {
				Log.d(TAG, "Extracting the combinationID...");
				char separator = '^';
				int firstSepIndex;
				int secondSepIndex;
				String combId;

				if (isTwitter) {
					firstSepIndex = campaign.indexOf(separator);
					secondSepIndex = campaign.indexOf(separator, firstSepIndex + 1);
					combId = campaign.substring(firstSepIndex + 1, secondSepIndex);
				} else {
					firstSepIndex = adset.indexOf(separator);
					secondSepIndex = adset.indexOf(separator, firstSepIndex + 1);
					combId = adset.substring(firstSepIndex + 1, secondSepIndex);
				}
				
				Log.i(TAG, "combId = " + combId);
				combinationID = Long.valueOf(combId);
			}catch(NumberFormatException numberEx){
				Log.e(TAG, "Combination ID is NOT of type Long");						
			}catch (Exception e) {
				Log.e(TAG, "Could NOT extract the Combination ID");	
			}
			
			Log.i(TAG, "dp = " + dpBuilder.toString());

			Log.d(TAG, "Invoke setMarketingParams from AppsFlyerManager with combid: " + combinationID);
			Utils.setMarketingParams(combinationID, dpBuilder.toString(), null, null, null);

			Log.i(TAG, "Saved combId = " + Utils.getCombinationId());
			Log.i(TAG, "Saved dp = " + Utils.getDynamicParam());

			Application.get().postEvent(new InstallEvent());
		}else{
			Log.d(TAG, "NOT a FACEBOOK or TWITTER campaign.");
		}

	}

	@Override
	public void onInstallConversionFailure(String arg0) {}


	@Override
	public void onAppOpenAttribution(Map<String, String> arg0) {
		
	}

	@Override
	public void onAttributionFailure(String s) {}
}
