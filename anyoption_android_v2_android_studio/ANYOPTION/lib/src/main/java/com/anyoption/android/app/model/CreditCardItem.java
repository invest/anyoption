package com.anyoption.android.app.model;

import android.graphics.drawable.Drawable;

public class CreditCardItem {

	private long id;
	private String name;
	private Drawable image;
	
	public CreditCardItem(){}
	
	public CreditCardItem(long id, String name, Drawable image){
		this.id = id;
		this.name = name;
		this.image = image;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Drawable getDrawable() {
		return image;
	}
	public void setDrawable(Drawable image) {
		this.image = image;
	}
}
