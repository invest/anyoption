package com.anyoption.android.app.widget;

import com.anyoption.android.R;
import com.anyoption.android.app.util.Utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

/**
 * Top to bottom sliding drawer.
 * 
 * @author Mario Kutlev
 */
public class SlidingDrawer extends ViewGroup {

	public static final String TAG = SlidingDrawer.class.getSimpleName();

	private static final int TAP_THRESHOLD = 6;
	private static final float MAXIMUM_TAP_VELOCITY = 100.0f;
	private static final float MAXIMUM_MINOR_VELOCITY = 150.0f;
	private static final float MAXIMUM_MAJOR_VELOCITY = 200.0f;
	private static final float MAXIMUM_ACCELERATION = 2000.0f;
	private static final int VELOCITY_UNITS = 1000;
	private static final int MSG_ANIMATE = 1000;
	private static final int ANIMATION_FRAME_DURATION = 1000 / 60;

	private static final int EXPANDED_FULL_OPEN = -10001;
	private static final int COLLAPSED_FULL_CLOSED = -10002;

	private final int mHandleId;
	private final int mContentId;

	private View mHandle;
	private View mContent;

	private final Rect mFrame = new Rect();
	private final Rect mInvalidate = new Rect();
	private boolean mTracking;
	private boolean mLocked;

	private VelocityTracker mVelocityTracker;

	protected boolean mExpanded;
	protected int mHandleHeight;

	private OnDrawerOpenListener mOnDrawerOpenListener;
	private OnDrawerCloseListener mOnDrawerCloseListener;
	private OnDrawerScrollListener mOnDrawerScrollListener;

	private final Handler mHandler = new SlidingHandler();
	private float mAnimatedAcceleration;
	private float mAnimatedVelocity;
	private float mAnimationPosition;
	private long mAnimationLastTime;
	private long mCurrentAnimationTime;
	private int mTouchDelta;
	private boolean mAnimating;
	private boolean mAllowSingleTap;
	private boolean mAnimateOnClick;

	private final int mTapThreshold;
	private final int mMaximumTapVelocity;
	private int mMaximumMinorVelocity;
	private int mMaximumMajorVelocity;
	private int mMaximumAcceleration;
	private final int mVelocityUnits;
	
	// TODO
	private boolean mHandleFlipEnabled;
	private int mHandleFlippedImageRes;
	
	private boolean handleAboveContent = false;
	private boolean isDrawerEverOpened;
	
	// TODO remove
	private boolean showLogs = false;

	/**
	 * Callback invoked when the drawer is opened.
	 */
	public static interface OnDrawerOpenListener {

		/**
		 * Invoked when the drawer becomes fully open.
		 */
		public void onDrawerOpened();
	}

	/**
	 * Callback invoked when the drawer is closed.
	 */
	public static interface OnDrawerCloseListener {

		/**
		 * Invoked when the drawer becomes fully closed.
		 */
		public void onDrawerClosed();
	}

	/**
	 * Callback invoked when the drawer is scrolled.
	 */
	public static interface OnDrawerScrollListener {

		/**
		 * Invoked when the user starts dragging/flinging the drawer's handle.
		 */
		public void onScrollStarted();

		/**
		 * Invoked when the user stops dragging/flinging the drawer's handle.
		 */
		public void onScrollEnded();
	}

	/**
	 * Creates a new SlidingDrawer from a specified set of attributes defined in
	 * XML.
	 * 
	 * @param context
	 *            The application's environment.
	 * @param attrs
	 *            The attributes defined in XML.
	 */
	public SlidingDrawer(Context context, AttributeSet attrs)
	{
		this(context, attrs, 0);
	}

	/**
	 * Creates a new SlidingDrawer from a specified set of attributes defined in
	 * XML.
	 * 
	 * @param context
	 *            The application's environment.
	 * @param attrs
	 *            The attributes defined in XML.
	 * @param defStyle
	 *            The style to apply to this widget.
	 */
	@SuppressWarnings("deprecation")
	public SlidingDrawer(Context context, AttributeSet attrs,
			int defStyle)
	{
		super(context, attrs, defStyle);
		// TODO
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.SlidingDrawer, defStyle, 0);
		mAllowSingleTap = a.getBoolean(
				R.styleable.SlidingDrawer_allowSingleTap, true);
		mAnimateOnClick = a.getBoolean(
				R.styleable.SlidingDrawer_animateOnClick, true);
		
		int handleId = a.getResourceId(
				R.styleable.SlidingDrawer_handle, 0);
		if (handleId == 0) {
			throw new IllegalArgumentException(
					"The handle attribute is required and must refer "
							+ "to a valid child.");
		}

		int contentId = a.getResourceId(
				R.styleable.SlidingDrawer_content, 0);
		if (contentId == 0) {
			throw new IllegalArgumentException(
					"The content attribute is required and must refer "
							+ "to a valid child.");
		}

		if (handleId == contentId) {
			throw new IllegalArgumentException(
					"The content and handle attributes must refer "
							+ "to different children.");
		}
		mHandleId = handleId;
		mContentId = contentId;

		final float density = getResources().getDisplayMetrics().density;
		mTapThreshold = (int) (TAP_THRESHOLD * density + 0.5f);
		mMaximumTapVelocity = (int) (MAXIMUM_TAP_VELOCITY * density + 0.5f);
		mMaximumMinorVelocity = (int) (MAXIMUM_MINOR_VELOCITY * density + 0.5f);
		mMaximumMajorVelocity = (int) (MAXIMUM_MAJOR_VELOCITY * density + 0.5f);
		mMaximumAcceleration = (int) (MAXIMUM_ACCELERATION * density + 0.5f);
		mVelocityUnits = (int) (VELOCITY_UNITS * density + 0.5f);

		mMaximumAcceleration = -mMaximumAcceleration;
		mMaximumMajorVelocity = -mMaximumMajorVelocity;
		mMaximumMinorVelocity = -mMaximumMinorVelocity;

		a.recycle();
		if(Utils.getDeviceOSVersionCode() < 23) {
			setAlwaysDrawnWithCacheEnabled(false);
		}
		
		// TODO Must be set to false in one more place.
		// If you lock the phone and unlock it the handle is not positioned correctly (Nexus 5).
		isDrawerEverOpened = false;
	}

	@Override
	protected void onFinishInflate()
	{
		if(showLogs && getId() == R.id.asset_options_drawer) {
			Log.e(TAG, "onFinishInflate opened=" + isDrawerEverOpened);
		}
		
		
		mHandle = findViewById(mHandleId);
		if (mHandle == null) {
			throw new IllegalArgumentException(
					"The handle attribute is must refer to an"
							+ " existing child.");
		}
		mHandle.setOnClickListener(new DrawerToggler());

		mContent = findViewById(mContentId);
		if (mContent == null) {
			throw new IllegalArgumentException(
					"The content attribute is must refer to an"
							+ " existing child.");
		}
		mContent.setVisibility(View.GONE);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		if(showLogs && getId() == R.id.asset_options_drawer) {
			Log.e(TAG, "onMeasure opened=" + isDrawerEverOpened);
		}
		
		int widthSpecMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSpecSize = MeasureSpec.getSize(widthMeasureSpec);

		int heightSpecMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSpecSize = MeasureSpec.getSize(heightMeasureSpec);

		// TODO This is the original code. Revert if there are problems
	/*	if (widthSpecMode == MeasureSpec.UNSPECIFIED
				|| heightSpecMode == MeasureSpec.UNSPECIFIED) {
			throw new RuntimeException(
					"SlidingDrawer cannot have UNSPECIFIED dimensions");
		}*/
		
		if (widthSpecMode == MeasureSpec.UNSPECIFIED) {
			widthSpecMode = 1;
		} else if (heightSpecMode == MeasureSpec.UNSPECIFIED) {
			heightSpecMode = 1;
		}

		final View handle = mHandle;
		measureChild(handle, widthMeasureSpec, heightMeasureSpec);

		if (handleAboveContent) {
			int height = heightSpecSize;
			mContent.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(height, heightSpecMode));

			heightSpecSize = mContent.getMeasuredHeight();
			widthSpecSize = mContent.getMeasuredWidth();
			if (handle.getMeasuredWidth() > widthSpecSize) {
				widthSpecSize = handle.getMeasuredWidth();
			}
		} else {
			int height = heightSpecSize - handle.getMeasuredHeight();
			mContent.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(height, heightSpecMode));
			heightSpecSize = handle.getMeasuredHeight() + mContent.getMeasuredHeight();
			widthSpecSize = mContent.getMeasuredWidth();
			if (handle.getMeasuredWidth() > widthSpecSize)
				widthSpecSize = handle.getMeasuredWidth();
		}

		setMeasuredDimension(widthSpecSize, heightSpecSize);
	}
	
	@Override
	protected void dispatchDraw(Canvas canvas) {
		if(showLogs && getId() == R.id.asset_options_drawer) {
			Log.e(TAG, "dispatchDraw opened=" + isDrawerEverOpened);
		}
		
		final long drawingTime = getDrawingTime();
		final View handle = mHandle;
		
		if (mTracking || mAnimating) {
			final Bitmap cache = mContent.getDrawingCache();

			if (cache != null) {
				// Move the handle down (open the drawer).
				if (handleAboveContent) {
					// TODO With the if handle is ok for 4.4 (bug with lock/unlock)
//					if (!isDrawerEverOpened) {
						canvas.drawBitmap(cache, 0, handle.getTop() - (getBottom() - getTop()),
								null);
//					} else {
//						canvas.drawBitmap(cache, 0, handle.getTop() - (getBottom() - getTop())
//								+ mHandleHeight, null);
//					}
				} else {
					canvas.drawBitmap(cache, 0, handle.getTop()
							- (getBottom() - getTop()) + mHandleHeight, null);
				}
			} else {
				// Move handle up (close the drawer)
				canvas.save();
				if (handleAboveContent) {
					// Remove "+ mHandleHeight" if you want the handle to be below the content.
					canvas.translate(0, handle.getTop() - mContent.getMeasuredHeight()
							+ mHandleHeight);
				} else {
					canvas.translate(0, handle.getTop() - mContent.getMeasuredHeight());
				}

				drawChild(canvas, mContent, drawingTime);
				canvas.restore();
			}
			invalidate();
		} else if (mExpanded) {
			// Draw the content
			drawChild(canvas, mContent, drawingTime);
		}

		// Draw the handle last to be above the content.
		drawChild(canvas, handle, drawingTime);
	}

	

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b)
	{
		if (mTracking) {
			return;
		}
		
		if(showLogs && getId() == R.id.asset_options_drawer) {
			Log.e(TAG, "onLayout opened=" + isDrawerEverOpened);
		}

		final int width = r - l;
		final int height = b - t;

		final View handle = mHandle;

		int handleWidth = handle.getMeasuredWidth();
		int handleHeight = handle.getMeasuredHeight();

		int handleLeft;
		int handleTop;

		final View content = mContent;
		handleLeft = (width - handleWidth) / 2;
		handleTop = mExpanded ? height - handleHeight : 0;
		content.layout(0, 0, content.getMeasuredWidth(), content.getMeasuredHeight());
		
		handle.layout(handleLeft, handleTop, handleLeft + handleWidth,
				handleTop + handleHeight);
		
		mHandleHeight = handle.getHeight();
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent event)
	{
		if (mLocked) {
			return false;
		}
		
		if(showLogs && getId() == R.id.asset_options_drawer) {
			Log.e(TAG, "onInterceptTouchEvent opened=" + isDrawerEverOpened);
		}

		final int action = event.getAction();

		float x = event.getX();
		float y = event.getY();

		final Rect frame = mFrame;
		final View handle = mHandle;

		handle.getHitRect(frame);
		if (!mTracking && !frame.contains((int) x, (int) y)) {
			return false;
		}

		if (action == MotionEvent.ACTION_DOWN) {
			mTracking = true;

			handle.setPressed(true);
			// Must be called before prepareTracking()
			prepareContent();

			// Must be called after prepareContent()
			if (mOnDrawerScrollListener != null) {
				mOnDrawerScrollListener.onScrollStarted();
			}

			final int top = mHandle.getTop();
			mTouchDelta = (int) y - top;
			prepareTracking(top);
			mVelocityTracker.addMovement(event);
		}

		return true;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (mLocked) {
			return true;
		}
		
		if(showLogs && getId() == R.id.asset_options_drawer) {
			Log.e(TAG, "onTouchEvent opened=" + isDrawerEverOpened);
		}

		if (mTracking) {
			mVelocityTracker.addMovement(event);
			final int action = event.getAction();
			switch (action) {
			case MotionEvent.ACTION_MOVE:
				moveHandle((int) (event.getY()) - mTouchDelta);
				break;
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_CANCEL: {
				final VelocityTracker velocityTracker = mVelocityTracker;
				velocityTracker.computeCurrentVelocity(mVelocityUnits);

				float yVelocity = velocityTracker.getYVelocity();
				float xVelocity = velocityTracker.getXVelocity();
				boolean negative;

				negative = yVelocity < 0;
				if (xVelocity < 0) {
					xVelocity = -xVelocity;
				}

				if (xVelocity < mMaximumMinorVelocity) {
					xVelocity = mMaximumMinorVelocity;
				}

				float velocity = (float) Math.hypot(xVelocity, yVelocity);
				if (negative) {
					velocity = -velocity;
				}

				final int handleTop = mHandle.getTop();

				if (Math.abs(velocity) < mMaximumTapVelocity) {
					boolean cond1 = (mExpanded && handleTop > getBottom() - getTop() - mHandleHeight - mTapThreshold);
					boolean cond2 = (!mExpanded && handleTop < mHandleHeight - mTapThreshold);
					
					Log.d(TAG, "ACTION_UP: " + "c1: " + cond1 + ", c2: " + cond2);

					if (cond1 || cond2) {

						if (mAllowSingleTap) {
							playSoundEffect(SoundEffectConstants.CLICK);

							if (mExpanded) {
								animateClose(handleTop);
							} else {
								animateOpen(handleTop);
							}
						} else {
							performFling(handleTop, velocity, false);
						}
					} else {
						performFling(handleTop, velocity, false);
					}
				} else {
					performFling(handleTop, velocity, false);
				}
			}
				break;
			}
		}

		return mTracking || mAnimating || super.onTouchEvent(event);
	}

	private void animateClose(int position) {
		prepareTracking(position);
		performFling(position, mMaximumAcceleration, true);
	}

	private void animateOpen(int position) {
		prepareTracking(position);
		performFling(position, -mMaximumAcceleration, true);
	}

	private void performFling(int position, float velocity, boolean always) {
		mAnimationPosition = position;
		mAnimatedVelocity = velocity;

		boolean c1;
		boolean c2;
		boolean c3;

		if (mExpanded) {
			int bottom = getBottom();
			int handleHeight = mHandleHeight;

			Log.d(TAG, "position: " + position + ", velocity: " + velocity
					+ ", mMaximumMajorVelocity: " + mMaximumMajorVelocity);
			c1 = velocity < mMaximumMajorVelocity;
			c2 = bottom - (position + handleHeight) > handleHeight;
			c3 = velocity < -mMaximumMajorVelocity;

			Log.d(TAG, "EXPANDED. c1: " + c1 + ", c2: " + c2 + ", c3: "
					+ c3);
			if (always || (c1 || (c2 && c3))) {
				// We are expanded, So animate to CLOSE!
				mAnimatedAcceleration = mMaximumAcceleration;
				if (velocity > 0) {
					mAnimatedVelocity = 0;
				}
			} else {
				// We are expanded, but they didn't move sufficiently to cause
				// us to retract. Animate back to the expanded position. so
				// animate BACK to expanded!
				mAnimatedAcceleration = -mMaximumAcceleration;

				if (velocity < 0) {
					mAnimatedVelocity = 0;
				}
			}
		} else {
			// WE'RE COLLAPSED
			c1 = velocity < mMaximumMajorVelocity;
			c2 = position < getHeight() / 2;
			c3 = velocity < -mMaximumMajorVelocity;

			Log.d(TAG, "COLLAPSED. position: " + position + ", velocity: "
					+ velocity + ", mMaximumMajorVelocity: "
					+ mMaximumMajorVelocity);
			Log.d(TAG, "COLLAPSED. always: " + always + ", c1: " + c1
					+ ", c2: " + c2 + ", c3: " + c3);

			if (!always && (c1 || (c2 && c3))) {
				mAnimatedAcceleration = mMaximumAcceleration;

				if (velocity > 0) {
					mAnimatedVelocity = 0;
				}
			} else {
				mAnimatedAcceleration = -mMaximumAcceleration;

				if (velocity < 0) {
					mAnimatedVelocity = 0;
				}
			}
		}

		long now = SystemClock.uptimeMillis();
		mAnimationLastTime = now;
		mCurrentAnimationTime = now + ANIMATION_FRAME_DURATION;
		mAnimating = true;
		mHandler.removeMessages(MSG_ANIMATE);
		mHandler.sendMessageAtTime(mHandler.obtainMessage(MSG_ANIMATE),
				mCurrentAnimationTime);
		stopTracking();
	}

	private void prepareTracking(int position)
	{
		mTracking = true;
		mVelocityTracker = VelocityTracker.obtain();
		boolean opening = !mExpanded;

		if (opening) {
			mAnimatedAcceleration = mMaximumAcceleration;
			mAnimatedVelocity = mMaximumMajorVelocity;
			mAnimationPosition = 0;
			mAnimating = true;
			mHandler.removeMessages(MSG_ANIMATE);
			long now = SystemClock.uptimeMillis();
			mAnimationLastTime = now;
			mCurrentAnimationTime = now + ANIMATION_FRAME_DURATION;
			mAnimating = true;
		} else {
			if (mAnimating) {
				mAnimating = false;
				mHandler.removeMessages(MSG_ANIMATE);
			}
			moveHandle(position);
		}
	}

	private void moveHandle(int position)
	{
		final View handle = mHandle;

		if (position == EXPANDED_FULL_OPEN) {
			handle.offsetTopAndBottom(getBottom() - getTop() - mHandleHeight);
			
			invalidate();
		} else if (position == COLLAPSED_FULL_CLOSED) {
			handle.offsetTopAndBottom(-handle.getTop());
			invalidate();
		} else {
			final int top = handle.getTop();
			int deltaY = position - top;
			if (position < 0) {
				deltaY = -top;
			} else if (deltaY > getBottom() - getTop() - mHandleHeight - top) {
				deltaY = getBottom() - getTop() - mHandleHeight - top;
			}

			handle.offsetTopAndBottom(deltaY);

			final Rect frame = mFrame;
			final Rect region = mInvalidate;

			handle.getHitRect(frame);
			region.set(frame);

			region.union(frame.left, frame.top - deltaY, frame.right,
					frame.bottom - deltaY);
			region.union(0, frame.bottom - deltaY, getWidth(), frame.bottom
					- deltaY + mContent.getHeight());

			invalidate(region);
		}
	}

	private void prepareContent() {
		if(showLogs && getId() == R.id.asset_options_drawer) {
			Log.e(TAG, "prepareContent opened=" + isDrawerEverOpened);
		}
		
		if (mAnimating) {
			return;
		}

		// Something changed in the content, we need to honor the layout request
		// before creating the cached bitmap
		final View content = mContent;
		if (content.isLayoutRequested()) {
			final int handleHeight = mHandleHeight;
			
			int height = getBottom() - getTop() - handleHeight;
			content.measure(MeasureSpec.makeMeasureSpec(getRight() - getLeft(), MeasureSpec.EXACTLY), 
					MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));

			content.layout(0, 0, content.getMeasuredWidth(), content.getMeasuredHeight());
		}
		
		// Try only once... we should really loop but it's not a big deal
		// if the draw was cancelled, it will only be temporary anyway
		content.getViewTreeObserver().dispatchOnPreDraw();
		content.buildDrawingCache();

		content.setVisibility(View.GONE);
	}

	private void stopTracking()
	{
		mHandle.setPressed(false);
		mTracking = false;

		if (mOnDrawerScrollListener != null) {
			mOnDrawerScrollListener.onScrollEnded();
		}

		if (mVelocityTracker != null) {
			mVelocityTracker.recycle();
			mVelocityTracker = null;
		}
	}

	private void doAnimation() {
		if (mAnimating) {
			incrementAnimation();

			if (mAnimationPosition < 0) {
				mAnimating = false;
				closeDrawer();
			} else if (mAnimationPosition >= getHeight() - 1) {
				mAnimating = false;
				openDrawer();
			} else {
				moveHandle((int) mAnimationPosition);
				mCurrentAnimationTime += ANIMATION_FRAME_DURATION;
				mHandler.sendMessageAtTime(
						mHandler.obtainMessage(MSG_ANIMATE),
						mCurrentAnimationTime);
			}
		}
	}

	private void incrementAnimation()
	{
		long now = SystemClock.uptimeMillis();
		float t = (now - mAnimationLastTime) / 1000.0f; // ms -> s
		final float position = mAnimationPosition;
		final float v = mAnimatedVelocity; // px/s
		final float a = mAnimatedAcceleration; // px/s/s
		mAnimationPosition = position + (v * t) + (0.5f * a * t * t); // px
		mAnimatedVelocity = v + (a * t); // px/s
		mAnimationLastTime = now; // ms
	}

	/**
	 * Toggles the drawer open and close. Takes effect immediately.
	 * 
	 * @see #open()
	 * @see #close()
	 * @see #animateClose()
	 * @see #animateOpen()
	 * @see #animateToggle()
	 */
	public void toggle()
	{
		if (!mExpanded) {
			openDrawer();
		} else {
			closeDrawer();
		}
		invalidate();
		requestLayout();
	}

	/**
	 * Toggles the drawer open and close with an animation.
	 * 
	 * @see #open()
	 * @see #close()
	 * @see #animateClose()
	 * @see #animateOpen()
	 * @see #toggle()
	 */
	public void animateToggle()
	{
		if (!mExpanded) {
			animateOpen();
		} else {
			animateClose();
		}
	}

	/**
	 * Opens the drawer immediately.
	 * 
	 * @see #toggle()
	 * @see #close()
	 * @see #animateOpen()
	 */
	public void open()
	{
		openDrawer();
		invalidate();
		requestLayout();

		sendAccessibilityEvent(AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED);
	}

	/**
	 * Closes the drawer immediately.
	 * 
	 * @see #toggle()
	 * @see #open()
	 * @see #animateClose()
	 */
	public void close()
	{
		closeDrawer();
		invalidate();
		requestLayout();
	}

	/**
	 * Closes the drawer with an animation.
	 * 
	 * @see #close()
	 * @see #open()
	 * @see #animateOpen()
	 * @see #animateToggle()
	 * @see #toggle()
	 */
	public void animateClose()
	{
		prepareContent();
		final OnDrawerScrollListener scrollListener = mOnDrawerScrollListener;
		if (scrollListener != null) {
			scrollListener.onScrollStarted();
		}
		animateClose(mHandle.getTop());

		if (scrollListener != null) {
			scrollListener.onScrollEnded();
		}
	}

	/**
	 * Opens the drawer with an animation.
	 * 
	 * @see #close()
	 * @see #open()
	 * @see #animateClose()
	 * @see #animateToggle()
	 * @see #toggle()
	 */
	public void animateOpen()
	{
		prepareContent();
		final OnDrawerScrollListener scrollListener = mOnDrawerScrollListener;
		if (scrollListener != null) {
			scrollListener.onScrollStarted();
		}
		animateOpen(mHandle.getTop());

		sendAccessibilityEvent(AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED);

		if (scrollListener != null) {
			scrollListener.onScrollEnded();
		}
	}
	
	public boolean isHandleAboveContent() {
		return handleAboveContent;
	}

	
	public void setHandleAboveContent(boolean handleAboveContent) {
		this.handleAboveContent = handleAboveContent;
	}
	
	

	private void closeDrawer()
	{
		moveHandle(COLLAPSED_FULL_CLOSED);
		mContent.setVisibility(View.GONE);
		mContent.destroyDrawingCache();

		if (!mExpanded) {
			return;
		}

		mExpanded = false;
		if (mOnDrawerCloseListener != null) {
			mOnDrawerCloseListener.onDrawerClosed();
		}
	}

	private void openDrawer()
	{
		moveHandle(EXPANDED_FULL_OPEN);
		mContent.setVisibility(View.VISIBLE);

		if (mExpanded) {
			return;
		}

		mExpanded = true;

		isDrawerEverOpened = true;
		 
		if (mOnDrawerOpenListener != null) {
			mOnDrawerOpenListener.onDrawerOpened();
		}
	}

	/**
	 * Sets the listener that receives a notification when the drawer becomes
	 * open.
	 * 
	 * @param onDrawerOpenListener
	 *            The listener to be notified when the drawer is opened.
	 */
	public void setOnDrawerOpenListener(
			OnDrawerOpenListener onDrawerOpenListener)
	{
		mOnDrawerOpenListener = onDrawerOpenListener;
	}

	/**
	 * Sets the listener that receives a notification when the drawer becomes
	 * close.
	 * 
	 * @param onDrawerCloseListener
	 *            The listener to be notified when the drawer is closed.
	 */
	public void setOnDrawerCloseListener(
			OnDrawerCloseListener onDrawerCloseListener)
	{
		mOnDrawerCloseListener = onDrawerCloseListener;
	}

	/**
	 * Sets the listener that receives a notification when the drawer starts or
	 * ends a scroll. A fling is considered as a scroll. A fling will also
	 * trigger a drawer opened or drawer closed event.
	 * 
	 * @param onDrawerScrollListener
	 *            The listener to be notified when scrolling starts or stops.
	 */
	public void setOnDrawerScrollListener(
			OnDrawerScrollListener onDrawerScrollListener)
	{
		mOnDrawerScrollListener = onDrawerScrollListener;
	}

	/**
	 * Returns the handle of the drawer.
	 * 
	 * @return The View reprenseting the handle of the drawer, identified by the
	 *         "handle" id in XML.
	 */
	public View getHandle()
	{
		return mHandle;
	}

	/**
	 * Returns the content of the drawer.
	 * 
	 * @return The View reprenseting the content of the drawer, identified by
	 *         the "content" id in XML.
	 */
	public View getContent()
	{
		return mContent;
	}

	/**
	 * Unlocks the SlidingDrawer so that touch events are processed.
	 * 
	 * @see #lock()
	 */
	public void unlock()
	{
		mLocked = false;
	}

	/**
	 * Locks the SlidingDrawer so that touch events are ignores.
	 * 
	 * @see #unlock()
	 */
	public void lock()
	{
		mLocked = true;
	}

	/**
	 * Indicates whether the drawer is currently fully opened.
	 * 
	 * @return True if the drawer is opened, false otherwise.
	 */
	public boolean isOpened()
	{
		return mExpanded;
	}

	/**
	 * Indicates whether the drawer is scrolling or flinging.
	 * 
	 * @return True if the drawer is scroller or flinging, false otherwise.
	 */
	public boolean isMoving()
	{
		return mTracking || mAnimating;
	}

	private class DrawerToggler implements OnClickListener {

		@Override
		public void onClick(View v)
		{
			if (mLocked) {
				return;
			}
			// mAllowSingleTap isn't relevant here; you're *always*
			// allowed to open/close the drawer by clicking with the
			// trackball.

			if (mAnimateOnClick) {
				animateToggle();
			} else {
				toggle();
			}
		}
	}

	private class SlidingHandler extends Handler {

		@Override
		public void handleMessage(Message m)
		{
			switch (m.what) {
			case MSG_ANIMATE:
				doAnimation();
				break;
			default:
				break;
			}
		}
	}
}
