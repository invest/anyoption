package com.anyoption.android.app.widget.notification;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.popup.InsuranceDialogFragment;
import com.anyoption.common.beans.base.Investment;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

public class NotificationRollForward extends RelativeLayout{

	public NotificationRollForward() {
		super(Application.get());
		init(Application.get(), null, 0);
	}
	public NotificationRollForward(Context context) {
		super(context);
		init(context, null, 0);
	}
	public NotificationRollForward(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	public NotificationRollForward(Context context, AttributeSet attrs,int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		View view = View.inflate(context, R.layout.notification_roll_forward_layout, this);

		OnClickListener clickListener = new OnClickListener() {
			private boolean isPressed = false;
			@Override
			public void onClick(View v) {
				if(!isPressed){
					isPressed = true;
					
					Application.get().postEvent(new NavigationEvent(Screen.MY_OPTIONS, NavigationType.INIT));

					Bundle arguments = new Bundle();
					arguments.putInt(InsuranceDialogFragment.ARG_INSURANCE_TYPE_KEY,Investment.INSURANCE_ROLL_FORWARD);
					Application.get().getFragmentManager().showDialogFragment(InsuranceDialogFragment.class, arguments);
				}
			}
		};
		
		view.setOnClickListener(clickListener);
		view.findViewById(R.id.notification_message_text_view).setOnClickListener(clickListener);
	}

}
