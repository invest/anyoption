package com.anyoption.android.app.fragment.trade;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.ExpandableListContextMenuInfo;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.RelativeLayout;

import com.anyoption.android.R;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.activity.BubblesActivity;
import com.anyoption.android.app.event.AssetsListFilterChangeEvent;
import com.anyoption.android.app.event.ExpiredOptionsEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.lightstreamer.LightstreamerConnectionHandler;
import com.anyoption.android.app.lightstreamer.LightstreamerListener;
import com.anyoption.android.app.manager.DataBaseManager;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.anyoption.android.app.model.MarketListGroup;
import com.anyoption.android.app.model.MarketListItem;
import com.anyoption.android.app.util.MarketsUtils;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.android.app.util.adapter.MarketsExpandableListAdapter;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.MarketGroup;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.service.requests.InvestmentsMethodRequest;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.results.InvestmentsMethodResult;
import com.anyoption.common.service.results.MarketsGroupsMethodResult;
import com.anyoption.common.service.results.MethodResult;
import com.lightstreamer.ls_client.UpdateInfo;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Custom {@link NavigationalFragment} that displays a list of Assets.
 *
 * @author Anastas Arnaudov
 *
 */
public class AssetsListFragment extends NavigationalFragment implements
		LightstreamerConnectionHandler.StatusListener, LightstreamerListener,
		OnGroupCollapseListener, OnChildClickListener, MarketsExpandableListAdapter.AdapterListener, OnClickListener {


	public final String TAG = AssetsListFragment.class.getSimpleName();

	// Handler messages:
	public static final int MSG_REFRESH_LIST = 1;
	////////////////////

	public static final int MENU_DELETE 	= 1;
	public static final int MENU_DETAILS 	= 2;

	protected long openMarketsTimestampUpdate = 0;
	protected List<Long> openMarketIDsList = new ArrayList<Long>();
	protected List<MarketListGroup> marketGroupsList = new ArrayList<MarketListGroup>();
	protected Investment[] investments;
	protected ExpandableListView listView;
	protected MarketsExpandableListAdapter adapter;
	protected View footerView;
	protected View footerponterView;
	protected byte mode 		= MarketsExpandableListAdapter.MODE_FEATURED_ASSETS;
	protected String filter 	= MarketsExpandableListAdapter.FILTER_BINARY;

	//@@@@@@@@@@@@@@@@@@@@@@@@
	//		LIGHTSTREAMER	//
	//@@@@@@@@@@@@@@@@@@@@@@@@
	protected AssetTimer timer;	// Updater for the list.
	protected Handler timerHandler; //Handler for the List updater (in order to update the list from the UI Thread).
	//@@@@@@@@@@@@@@@@@@@@@@@@@

	//Bottom buttons:
	protected View binary;
	protected View optionPlus;

	protected SkinGroup skinGroup;
	protected boolean isTablet;
	protected boolean isListRefreshing;
	protected boolean isActiveMarketsLoaded;
	protected View loadingView;
	protected View rootView;
	protected View dynamicsTab;
	protected View bubblesTab;
	private View bubblesView;
	private View bubblesButton;
	private View bubblesBkg;
	protected View tabSelector;
	private int numberOfTabs;
	private int tabWidth;

	private DecimalFormat profitPercentFormat = new DecimalFormat("##.##");

	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static AssetsListFragment newInstance(Bundle bundle){
		AssetsListFragment assetsListFragment = new AssetsListFragment();
		assetsListFragment.setArguments(bundle);
		return assetsListFragment;
	}

	@Override
	protected Screenable setupScreen() {
		return Screen.ASSETS_LIST;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = getArguments();
		timerHandler = new Handler();
		if(bundle != null){
			byte mode = bundle.getByte(Constants.EXTRA_ASSETS_LIST_MODE);
			String filter = bundle.getString(Constants.EXTRA_ASSETS_LIST_FILTER);
			if(mode != 0){
				this.mode = mode;
			}
			if(filter != null){
				this.filter = filter;
			}
		}

		Skin skin = application.getSkins().get(application.getSkinId());
		skinGroup = skin.getSkinGroup();
		isTablet = getResources().getBoolean(R.bool.isTablet);

		getMarketGroups();

		application.registerForEvents(this, ExpiredOptionsEvent.class, LoginLogoutEvent.class);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.assets_list_fragment, container, false);

		loadingView = rootView.findViewById(R.id.assets_list_loading);
		listView = (ExpandableListView) rootView.findViewById(R.id.assets_list_view);

		adapter = new MarketsExpandableListAdapter(listView, marketGroupsList, this);
		adapter.setMode(mode);
		adapter.setFilter(filter);

		setupFooter();

		listView.setAdapter(adapter);
		registerForContextMenu(listView);
		adapter.expandAll();
		listView.setOnGroupCollapseListener(this);
		listView.setOnChildClickListener(this);

		setupBubblesTabLayout();
		setupTabs();

		return rootView;
	}

	protected void setupBubblesTabLayout(){
		bubblesView = rootView.findViewById(R.id.assets_list_bubbles_layout);
		bubblesButton = rootView.findViewById(R.id.assets_list_bubbles_button);
		bubblesBkg = rootView.findViewById(R.id.assets_list_bubbles_bkg);

		bubblesButton.setOnClickListener(this);
		bubblesView.setOnClickListener(null);
		bubblesBkg.setOnClickListener(null);
	}

	@Override
	public void onResume() {
		super.onResume();
		if(isPaused){
			application.registerForEvents(this, ExpiredOptionsEvent.class, LoginLogoutEvent.class);
			getMarketGroups();
			isPaused = false;
		}

		if(marketGroupsList == null || marketGroupsList.isEmpty()){
			showHideLoading(true);
			showHideList(false);
		}

		updateFooterVisibility();

		listView.setOnGroupCollapseListener(this);
		listView.setOnChildClickListener(this);
	}

	@Override
	public void onPause() {
		unsubscribeAssetsFromLS();
		application.unregisterForAllEvents(this);
		listView.setOnGroupCollapseListener(null);
		listView.setOnChildClickListener(null);
		super.onPause();
	}

	@Override
	public void onDestroyView() {
		unsubscribeAssetsFromLS();
		application.unregisterForAllEvents(this);
		listView.setOnGroupCollapseListener(null);
		listView.setOnChildClickListener(null);
		super.onDestroyView();
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if(hidden){
			unsubscribeAssetsFromLS();
			application.unregisterForAllEvents(this);
		}else{
			showHideLoading(true);
			showHideList(false);

			application.registerForEvents(this, ExpiredOptionsEvent.class, LoginLogoutEvent.class);
			getMarketGroups();
		}
	}

	protected void setupTabs(){
		binary      = rootView.findViewById(R.id.assets_featured_binary_button);
		optionPlus  = rootView.findViewById(R.id.assets_featured_binary_plus_button);
		dynamicsTab = rootView.findViewById(R.id.assets_dynamics_button);
		bubblesTab  = rootView.findViewById(R.id.assets_bubbles_button);

		tabSelector = rootView.findViewById(R.id.assets_tab_selector);
		tabSelector.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				if(binary.getWidth() > 0){
					tabSelector.getViewTreeObserver().removeOnGlobalLayoutListener(this);

					tabWidth = binary.getWidth();
					//Setup the tab selector width:
					RelativeLayout.LayoutParams tabSelectorLayoutParams = (RelativeLayout.LayoutParams) tabSelector.getLayoutParams();
					tabSelectorLayoutParams.width = tabWidth;
					tabSelector.setLayoutParams(tabSelectorLayoutParams);

					//Set initial state:
					if(filter.equals(MarketsExpandableListAdapter.FILTER_BINARY)){
						selectBinaryTab();
					}else if(filter.equals(MarketsExpandableListAdapter.FILTER_OPTION_PLUS)){
						selectOptionplusTab();
					}else if(filter.equals(MarketsExpandableListAdapter.FILTER_DYNAMICS)){
						selectDynamicsTab();
					}

				}
			}
		});

		//Check if the user should see he Dynamics Markets:
		showHideDynamicsTab();

		application.postEvent(new AssetsListFilterChangeEvent(filter));

		OnClickListener onTabClickListener = new OnClickListener() {
			@Override
			public void onClick(View view) {
				if(!isListRefreshing){
					if(view == binary){
						filter = MarketsExpandableListAdapter.FILTER_BINARY;
						selectBinaryTab();
					}else if(view == optionPlus){
						filter = MarketsExpandableListAdapter.FILTER_OPTION_PLUS;
						selectOptionplusTab();
					}else if(view == dynamicsTab){
						filter = MarketsExpandableListAdapter.FILTER_DYNAMICS;
						selectDynamicsTab();
					}

					if(view == bubblesTab){
						filter = MarketsExpandableListAdapter.FILTER_BUBBLES;
						selectBubblesTab();
						unsubscribeAssetsFromLS();
					}else{
						filter(filter);
					}
					application.postEvent(new AssetsListFilterChangeEvent(filter));
				}
			}
		};

		binary		.setOnClickListener(onTabClickListener);
		optionPlus	.setOnClickListener(onTabClickListener);
		dynamicsTab	.setOnClickListener(onTabClickListener);
		bubblesTab	.setOnClickListener(onTabClickListener);
	}

	public void showHideDynamicsTab() {
		boolean hasDynamics = application.isHasDynamics();
		if(hasDynamics){
			dynamicsTab.setVisibility(View.VISIBLE);
		}else{
			dynamicsTab.setVisibility(View.GONE);
		}
	}

	public void changeFilter(String filterTab) {
		if(!isListRefreshing){
			if(filterTab.equals(MarketsExpandableListAdapter.FILTER_BINARY)){
				filter = MarketsExpandableListAdapter.FILTER_BINARY;
				selectBinaryTab();
			}else if(filterTab.equals(MarketsExpandableListAdapter.FILTER_OPTION_PLUS)){
				filter = MarketsExpandableListAdapter.FILTER_OPTION_PLUS;
				selectOptionplusTab();
			}else if(filterTab.equals(MarketsExpandableListAdapter.FILTER_DYNAMICS)){
				if(application.isHasDynamics()) {
					filter = MarketsExpandableListAdapter.FILTER_DYNAMICS;
					selectDynamicsTab();
				} else {
					filter = MarketsExpandableListAdapter.FILTER_BINARY;
					selectBinaryTab();
				}
			}

			filter(filter);
			application.postEvent(new AssetsListFilterChangeEvent(filter));
		}
	}

	protected void selectBinaryTab(){
		int x = 0;
		startTabSelectionAnimation(x);

		listView.setVisibility(View.VISIBLE);
		bubblesView.setVisibility(View.GONE);
	}

	protected void selectOptionplusTab(){
		int x = (dynamicsTab.getVisibility()==View.VISIBLE) ? 2*tabWidth : tabWidth;
		startTabSelectionAnimation(x);

		listView.setVisibility(View.VISIBLE);
		bubblesView.setVisibility(View.GONE);
	}

	protected void selectDynamicsTab(){
		int x = tabWidth;
		startTabSelectionAnimation(x);

		listView.setVisibility(View.VISIBLE);
		bubblesView.setVisibility(View.GONE);
	}

	protected void selectBubblesTab(){
		int x = (dynamicsTab.getVisibility()==View.VISIBLE) ? 3*tabWidth : 2*tabWidth;
		startTabSelectionAnimation(x);

		bubblesView.setVisibility(View.VISIBLE);
		listView.setVisibility(View.GONE);
	}

	private void startTabSelectionAnimation(final int selectorX){

		float scaleX = 1f;
		float delta = Math.abs(selectorX - tabSelector.getX());

		if(delta <= 0){
			scaleX = 1f;
		}else if(delta <= tabWidth){
			scaleX = 1.5f;
		}else if(delta <= 2*tabWidth){
			scaleX = 2f;
		}else if(delta <= 3*tabWidth){
			scaleX = 3f;
		}

		if(selectorX >= tabSelector.getX()){
			tabSelector.setPivotX(0f);
		}else{
			tabSelector.setPivotX(tabWidth);
		}
		tabSelector.animate().setDuration(150L).scaleX(scaleX).setInterpolator(new AccelerateInterpolator()).withEndAction(new Runnable() {
			@Override
			public void run() {
				tabSelector.animate().setDuration(150L).scaleX(1f).x(selectorX).setInterpolator(new DecelerateInterpolator()).start();
			}
		}).start();
	}

	private void showHideLoading(boolean shouldShow){
		if(loadingView != null){
			if(shouldShow){
				loadingView.setVisibility(View.VISIBLE);
			}else{
				loadingView.setVisibility(View.GONE);
			}
		}
	}

	private void showHideList(boolean shouldShow){
		if(listView != null){
			if(shouldShow){
				listView.setVisibility(View.VISIBLE);
			}else{
				listView.setVisibility(View.INVISIBLE);
			}
		}
	}

	@SuppressLint("RtlHardcoded")
	protected void setupFooterView(){
		footerView 		 = ((LayoutInflater) application.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.assets_list_footer_layout, null, false);
		footerponterView = footerView.findViewById(R.id.footerPointerView);
	}

	/**
	 * Sets up the List View footer.
	 */
	protected void setupFooter() {
		setupFooterView();
		footerponterView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(!isListRefreshing){
					if (isTablet) {
						setMode(MarketsExpandableListAdapter.MODE_ALL_ASSETS);
					} else {
						Bundle bundle = new Bundle();
						bundle.putByte(Constants.EXTRA_ASSETS_LIST_MODE, MarketsExpandableListAdapter.MODE_ALL_ASSETS);
						bundle.putString(Constants.EXTRA_ASSETS_LIST_FILTER, filter);
						application.postEvent(new NavigationEvent(Screen.ASSETS_LIST,NavigationType.DEEP, bundle));
					}
				}
			}
		});

		listView.addFooterView(footerView, null, false);
		listView.invalidate();
	}


	private void updateFooterVisibility() {
		if (mode == MarketsExpandableListAdapter.MODE_FEATURED_ASSETS) {
			if (listView.getFooterViewsCount() == 0) {
				listView.addFooterView(footerView);
			}
		} else {
			listView.removeFooterView(footerView);
		}
	}

	/**
	 * Refreshes the List.
	 */
	private void refreshList() {
		if(adapter != null){
			adapter.notifyDataSetChanged();
		}
	}

	@Override
	protected void refreshUser() {
		//Do nothing
	}

	private void getMarketGroups() {
		showHideLoading(true);
		application.getCommunicationManager().requestService(this, "gotMarketGroups", Constants.SERVICE_GET_MARKET_GROUPS, new MethodRequest(), MarketsGroupsMethodResult.class, false, false);
	}

	public void gotMarketGroups(Object result) {
		Log.d(TAG, "gotMarketGroups");
		if(result != null && result instanceof MarketsGroupsMethodResult && ((MethodResult) result).getErrorCode() == Constants.ERROR_CODE_SUCCESS){
			isListRefreshing = true;
			MarketsGroupsMethodResult marketsGroupsResult = (MarketsGroupsMethodResult) result;
			marketGroupsList.clear();

			//Convert the Market Groups object into MarketListGroups:
			for(int i=0; i < marketsGroupsResult.getGroups().length; i++){
				MarketGroup marketGroup = marketsGroupsResult.getGroups()[i];
				marketGroupsList.add(new MarketListGroup(marketGroup));
			}

			//Update the cached markets in the application:
			application.gotMarketGroups(marketsGroupsResult);

			adapter.setMarketGroups(marketGroupsList);
			adapter.filter(filter);

			//Get the user Open Options in order to display for each Market whether the user has any Open Options there.
			getOpenOptions();

			unsubscribeAssetsFromLS();
			subscribeAssetsToLS();
		}

		// Shows the list even in case of error.
		showHideList(true);
	}

	/**
	 * Gets all user Open Options (Investments). The request is sent only if the user is logged in.
	 *
	 * @return
	 */
	private boolean getOpenOptions() {
		if (!application.isLoggedIn()) {
			return false;
		}

		InvestmentsMethodRequest request = new InvestmentsMethodRequest();
		request.setSettled(false);
		return application.getCommunicationManager().requestService(this, "gotOpenOptions", Constants.SERVICE_GET_INVESTMENTS, request, InvestmentsMethodResult.class, false, false);
	}

	/**
	 * Callback for the "getOpenOption".
	 * @param result
	 */
	public void gotOpenOptions(Object result) {
		Log.d(TAG, "gotMarketGroups");
		if(result != null && result instanceof InvestmentsMethodResult && ((MethodResult) result).getErrorCode() == Constants.ERROR_CODE_SUCCESS){
			InvestmentsMethodResult investmentsMethodResult = (InvestmentsMethodResult) result;

			// Update the user
			if (investmentsMethodResult.getUser() != null) {
				application.setUser(investmentsMethodResult.getUser());
			}

			investments = investmentsMethodResult.getInvestments();

			if(investments.length > 0){
				application.setInvestments(Arrays.asList(investments));

				for (MarketListGroup marketListGroup : marketGroupsList) {
					for (MarketListItem marketItem : marketListGroup.getMarketListItems()) {
						long marketID = marketItem.getMarket().getId();
						marketItem.setHasOpenOptions(false);
						for (Investment investment : investments) {
							long investmentMarketID = investment.getMarketId();
							if (marketID == investmentMarketID) {
								marketItem.setHasOpenOptions(true);
								break;
							}
						}
					}

				}

				filter(filter);
			}

		}
	}

	/**
	 * Called when a {@link ExpiredOptionsEvent} occurs.
	 *
	 * @param event
	 */
	public void onEventMainThread(ExpiredOptionsEvent event) {
		getOpenOptions();
	}

	/**
	 * Called when a {@link LoginLogoutEvent} occurs.
	 *
	 * @param event
	 */
	public void onEventMainThread(LoginLogoutEvent event) {
		if(!isTablet){
			if(event.isShouldChangeLocale()){
				unsubscribeAssetsFromLS();
			}
		}
		showHideDynamicsTab();
	}

	@Override
	public void onClick(View v) {
		if(v == bubblesButton){
			goToBubbles();
		}
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View view, int groupPosition, int childPosition, long id) {
		MarketListItem marketListItem = (MarketListItem) adapter.getChild(groupPosition, childPosition);

		Market market = marketListItem.getMarket();
		goToAsset(market);
		if (isTablet) {
			setMode(MarketsExpandableListAdapter.MODE_FEATURED_ASSETS);
		}

		return true;
	}
	@Override
	public void onGroupCollapse(int groupPosition) {
		listView.expandGroup(groupPosition);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View view, ContextMenuInfo menuInfo) {
		//Check if the user is logged in:
		if(application.getUser() != null && menuInfo instanceof ExpandableListView.ExpandableListContextMenuInfo){
			ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) menuInfo;
			int positionType = ExpandableListView.getPackedPositionType(info.packedPosition);
			int groupPosition = ExpandableListView.getPackedPositionGroup(info.packedPosition);
			int childPosition = ExpandableListView.getPackedPositionChild(info.packedPosition);

			//Only create a context menu for child items
			if (positionType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
				MarketListItem item = ((MarketListItem) adapter.getChild(groupPosition, childPosition));
				String title = item.getMarket().getDisplayName();
				menu.setHeaderTitle(title);
				if(mode == MarketsExpandableListAdapter.MODE_FEATURED_ASSETS){
					menu.add(Menu.NONE, MENU_DELETE, 1, "DELETE");
				}
			}
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem menuItem) {
		ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListContextMenuInfo) menuItem.getMenuInfo();
		int positionType = ExpandableListView.getPackedPositionType(info.packedPosition);
		int groupPosition = ExpandableListView.getPackedPositionGroup(info.packedPosition);
		int childPosition = ExpandableListView.getPackedPositionChild(info.packedPosition);

		if (positionType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
			MarketListItem marketListItem = (MarketListItem) adapter.getChild(groupPosition, childPosition);
			switch (menuItem.getItemId()) {
				case MENU_DELETE:
					application.getNotificationManager().showNotificationSuccess("DELETED FROM FAVORITES");
					application.getDataBaseManager().saveOrUpdateMarket(marketListItem.getMarket(), DataBaseManager.MARKETS_STATE_DELETED);
					filter(filter);
					return true;
				default:
					return super.onContextItemSelected(menuItem);
			}
		}
		return false;
	}

	/**
	 * Opens the Asset Trade screen (The Play Area).
	 * It adds the Market to User Favorites Markets.
	 * Also , we get the List of all Markets with Open Options in them and pass it to the Play Area screen.
	 * @param market
	 */
	private void goToAsset(Market market) {

		Bundle bundle = new Bundle();

		if (isTablet) {
			bundle.putSerializable(Constants.EXTRA_MARKET, market);
			bundle.putString(Constants.EXTRA_ACTION_BAR_TITLE, market.getDisplayName());
			bundle.putLong(Constants.EXTRA_MARKET_PRODUCT_TYPE, market.getProductTypeId());

			application.postEvent(new NavigationEvent(Screen.ASSET_PAGE, NavigationType.HORIZONTAL, bundle));
		} else {
			bundle.putSerializable(Constants.EXTRA_MARKET, market);
			application.postEvent(new NavigationEvent(Screen.ASSET_VIEW_PAGER, NavigationType.DEEP, bundle));
		}
	}

	private void goToBubbles(){
		application.setCurrentScreen(Screen.BUBBLES);
		Intent intent = new Intent(getContext(), application.getSubClassForClass(BubblesActivity.class));
		startActivity(intent);
	}

	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	//@@@@@@@@@@@@@					LIGHSTREAMER					@@@@@@@@@@@@
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	/**
	 * Subscribes the assets that needs to be updated to the LS.
	 */
	private void subscribeAssetsToLS() {
		Log.d(TAG, "Subscribe to LS.");
		List<MarketListItem> marketList = getMarketsToUpdate();
		if(marketList.size() > 0){
			String[] items = new String[marketList.size()];

			int i = 0;
			for (MarketListItem marketItem : marketList) {

				boolean isBinary   		= MarketsUtils.isBinary(marketItem.getMarket());
				boolean isOptionPlus    = MarketsUtils.isOptionPlus(marketItem.getMarket());
				boolean isDynamics 		= MarketsUtils.isDynamics(marketItem.getMarket());;

				String itemName = "";
				if(isBinary){
					itemName = "aotps_1_" + marketItem.getMarket().getId();
				}
				else if (isOptionPlus) {
					itemName = "op_" + marketItem.getMarket().getId();
				}
				else if(isDynamics){
					itemName = "bz_" + marketItem.getMarket().getId() + "_" + Constants.DYNAMICS_OPPORTUNITY_TYPE_ID;
				}

				items[i] = itemName;
				i++;
			}

			application.getLightstreamerManager().subscribeAssetsListTable(this, items);

			startTimer();
		}else{
			showHideList(true);
		}
	}

	/**
	 * UnSubscribes the assets from the LS.
	 */
	private void unsubscribeAssetsFromLS() {
		Log.d(TAG, "Unsubscribe LS.");
		application.getLightstreamerManager().unsubscribeTable(LightstreamerManager.TABLE_ASSETS_LIST);

		clearActiveMarketsList();
		stopTimer();
	}

	private void clearActiveMarketsList(){
		isActiveMarketsLoaded = false;
		openMarketsTimestampUpdate = 0;
		openMarketIDsList.clear();
	}

	@Override
	public void onStatusChange(int status) {
		Log.d(TAG, "Lighstreamer onStatusChange");
	}

	private void updateActiveMarketIDsList(UpdateInfo update){
		if(filter.equals(MarketsExpandableListAdapter.FILTER_DYNAMICS)){
			isActiveMarketsLoaded = true;
			return;
		}

		boolean shouldRefreshListView = false;

		if(update.getNewValue(LightstreamerManager.COLUMN_AO_TS) != null && update.isValueChanged(LightstreamerManager.COLUMN_AO_TS)){
			long newOpenMarketsTimestampUpdate = Long.valueOf(update.getNewValue(LightstreamerManager.COLUMN_AO_TS));
			if(openMarketsTimestampUpdate < newOpenMarketsTimestampUpdate){
				if(openMarketsTimestampUpdate == 0){
					shouldRefreshListView = true;
				}
				openMarketsTimestampUpdate = newOpenMarketsTimestampUpdate;
				if(update.getNewValue(LightstreamerManager.COLUMN_AO_STATES) != null && update.isValueChanged(LightstreamerManager.COLUMN_AO_STATES)){

					String openMarketIDsString = update.getNewValue(LightstreamerManager.COLUMN_AO_STATES);
					boolean isListChanged = false;
					if(openMarketIDsString != null && openMarketIDsString.length() > 0){
						//Parse the update and populate our Open Market IDs List:
						for(String openMarketID : openMarketIDsString.split("\\|")){
							try {
								Long openMarketIDInteger = Long.valueOf(openMarketID);
								if(!this.openMarketIDsList.contains(openMarketIDInteger)){
									this.openMarketIDsList.add(openMarketIDInteger);
									isListChanged = true;
								}
							} catch (NumberFormatException e) {
								Log.e(TAG, "updateOpenMarketIDsList -> Open Market ID is not Long.");
							}
						}
					}
					shouldRefreshListView = isListChanged;

					if(shouldRefreshListView){
						//Update the List Items:
						for (MarketListGroup marketListGroup : marketGroupsList) {
							for (MarketListItem marketItem : marketListGroup.getMarketListItems()) {
								if(this.openMarketIDsList.contains(Long.valueOf(marketItem.getMarket().getId()))){
									marketItem.setActive(true);
								}else{
									marketItem.setActive(false);
								}
							}
						}

						//Refresh the List:
						application.getCurrentActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								filter(filter);
								isActiveMarketsLoaded = true;
								showHideList(true);
								showHideLoading(false);
							}
						});
					}

				}else{
					//Update the List Items:
					for (MarketListGroup marketListGroup : marketGroupsList) {
						for (MarketListItem marketItem : marketListGroup.getMarketListItems()) {
							marketItem.setActive(false);
						}
					}
					application.getCurrentActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							filter(filter);
							isActiveMarketsLoaded = true;
							showHideList(true);
							showHideLoading(false);
						}
					});
				}
			}
		}
	}

	@Override
	public void updateItem(int itemPos, String itemName, final UpdateInfo update) {
		if(isAdded() && !isListRefreshing){

			if(!isActiveMarketsLoaded){
				updateActiveMarketIDsList(update);
				return;
			}

			boolean isDynamics = filter.equals(MarketsExpandableListAdapter.FILTER_DYNAMICS);
			final long marketID;
			if(isDynamics){
				marketID = Long.parseLong(itemName.split("_")[1]);
			}else{
				marketID = Long.valueOf(update.getNewValue(LightstreamerManager.COLUMN_ET_NAME));
			}

			MarketListItem marketItem = getMarketItem(marketID);
			if (marketItem != null) {

				String opportunitySuspendState = update.getNewValue(LightstreamerManager.COLUMN_AO_OPP_STATE);
				if(opportunitySuspendState != null && opportunitySuspendState.equals(LightstreamerManager.OPPORTUNITY_SUSPEND_STATE_TRUE)){
					//Market is susspended:
					marketItem.setActive(false);
					marketItem.setShowTimer(false);
					marketItem.setStatusLabel(application.getString(R.string.availableNot));
					marketItem.setStatusValue("");
					marketItem.setCurrentLevel("");
					return;
				}

				long updateOpportunityID;
				int newOpportunityState;
				String timeCloseString;
				String nextOpportunityInfo;

				if(isDynamics){
					updateOpportunityID = Long.valueOf(update.getNewValue(LightstreamerManager.COLUMN_ET_KEY));
					newOpportunityState = Integer.parseInt(update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_STATE));
					timeCloseString 	= update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_TIME_CLOSE);
					nextOpportunityInfo = update.getNewValue(LightstreamerManager.COLUMN_ET_NH);
				}else{
					updateOpportunityID = Long.parseLong(update.getNewValue(LightstreamerManager.COLUMN_ET_OPP_ID));
					newOpportunityState = Integer.parseInt(update.getNewValue(LightstreamerManager.COLUMN_ET_STATE));
					timeCloseString 	= update.getNewValue(LightstreamerManager.COLUMN_ET_EST_CLOSE);
					nextOpportunityInfo = update.getNewValue(LightstreamerManager.COLUMN_ET_NH);
				}

				if(nextOpportunityInfo != null){
					//The Market has hourly opportunities.
					String[] nextOpportunityInfoArray = nextOpportunityInfo.split("\\|");
					long nextOpportunityId = 0l;
					try {
						nextOpportunityId = Long.valueOf(nextOpportunityInfoArray[0]);
					}catch (Exception e){
						Log.e(TAG, "Could not parse COLUMN_ET_NH info");
					}

					if(nextOpportunityId == updateOpportunityID && marketItem.getOpportunityID() == 0l){
						return;
					}else{
						int state;
						if(nextOpportunityId == updateOpportunityID){
							state = marketItem.getOpportunityState();
						}else{
							state = newOpportunityState;
						}
						//Check if the current opportunity is waiting for expiry:
						if(state == Opportunity.STATE_CLOSING || state == Opportunity.STATE_CLOSING_1_MIN){
							//Check if the User has any open options in the current opportunity:
							List<Investment> investments = application.getInvestmentsForOpportunity(updateOpportunityID);
							if (investments == null || investments.isEmpty()) {
								marketItem.setOpportunityID(nextOpportunityId);
								marketItem.setOpportunityState(0);
								return;
							}
						}
					}
				}else{
					//The Market does NOT have hourly opportunity.
					if (marketItem.getOpportunityID() == 0l){
						marketItem.setOpportunityID(updateOpportunityID);
					}else if(marketItem.getOpportunityID() != updateOpportunityID){
						//The update is for another opportunity. Ignore it.
						return;
					}
				}
				String currentLevel;
				String profitText 	 = "";
				String profitPercent = "";
				if(isDynamics){
					currentLevel  			 = update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_EVENT_LEVEL).replaceAll(",", "");
					double profitCallParam   = Double.valueOf(update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_PROFIT_CALL));
					double profitPutParam  	 = Double.valueOf(update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_PROFIT_PUT));
					double profitCallPercent = 100 * (100 - profitCallParam) / profitCallParam;
					double profitPutPercent  = 100 * profitPutParam / (100 - profitPutParam);
					profitPercent 			 = profitPercentFormat.format(Math.max(profitCallPercent, profitPutPercent)) + "%";
					profitText    			 = " | " + getString(R.string.maxProfitWithSpecalChar) + ":#" + profitPercent;
				}else{
					currentLevel = update.getNewValue(skinGroup.getLevelUpdateKey());
				}
				String level;
				try {
					if(Double.parseDouble(currentLevel) <= 0){
						level = "";
					}else{
						level = currentLevel;
					}
				} catch (NumberFormatException e) {
					level = currentLevel;
				}

				if(isDynamics){
					level = getString(R.string.optionLevel) + ":\n" + currentLevel;
					currentLevel = getString(R.string.optionLevel) + ":\n" + currentLevel;
				}

				boolean isStateChanged;
				if(isDynamics){
					isStateChanged = update.isValueChanged(LightstreamerManager.COLUMN_DYNAMICS_STATE);
				}else{
					isStateChanged = update.isValueChanged(LightstreamerManager.COLUMN_ET_STATE);
				}

				if (marketItem.getOpportunityState() == 0 || isStateChanged) {
					// Changes the opportunity state only when its new value is higher than the old.
					// That way we fix the problems with the states after waiting for expiry. We receive
					// opened, closed, waiting for expiry states in random order.
					if (newOpportunityState > marketItem.getOpportunityState()) {
						marketItem.setOpportunityState(newOpportunityState);

						//Set Opportunity Close time:
						String timeCloseFormated = "";
						long timeClose = 0l;
						try {
							timeCloseFormated = TimeUtils.reformatTimeFromLSUpdate(application.getCurrentActivity(),timeCloseString);
							timeClose = TimeUtils.parseTimeFromLSUpdate(timeCloseString).getTime();
						} catch (ParseException e) {
							Log.e(TAG, "Could not parse ET_EST_CLOSE time");
						}
						marketItem.setTimeClose(timeClose);
						//////////////////////////////////

						//Set Opportunity Last Invest Time:
						String timeLastInvestString;
						if(isDynamics){
							timeLastInvestString = update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_TIME_LAST_INV);
						}else{
							timeLastInvestString = update.getNewValue(LightstreamerManager.COLUMN_ET_LAST_INV);
						}
						long timeLastInvest = 0l;
						try {
							timeLastInvest = TimeUtils.parseTimeFromLSUpdate(timeLastInvestString).getTime();
						} catch (ParseException e) {
							Log.e(TAG, "Could not parse ET_EST_CLOSE time");
						}
						marketItem.setTimeLastInvest(timeLastInvest);
						//////////////////////////////////////

						switch (marketItem.getOpportunityState()) {

							case Opportunity.STATE_BINARY0100_UPDATING_PRICES:
								Log.d(TAG, "Market : " + marketItem.getMarket().getDisplayName() + " has moved to State : BINARY0100_UPDATING_PRICES");
								break;
							case Opportunity.STATE_CLOSED:
								Log.d(TAG, "Market : " + marketItem.getMarket().getDisplayName() + " has moved to State : CLOSED");
								marketItem.setActive(false);
								marketItem.setStatusLabel(application.getString(R.string.closingLevel) + " " + currentLevel);
								marketItem.setCurrentLevel("");
								marketItem.setOpportunityID(0);
								marketItem.setOpportunityState(0);
								marketItem.setShowTimer(false);
								marketItem.setProfitText("");
								marketItem.setProfitPercent("");
								break;
							case Opportunity.STATE_CLOSING:
								Log.d(TAG, "Market : " + marketItem.getMarket().getDisplayName() + " has moved to State : CLOSING");
								marketItem.setActive(true);
								marketItem.setStatusLabel(application.getString(R.string.waitingForExpiry));
								marketItem.setShowTimer(false);
								marketItem.setStatusValue("");
								marketItem.setCurrentLevel(currentLevel);
								marketItem.setProfitText("");
								marketItem.setProfitPercent("");
								break;
							case Opportunity.STATE_CLOSING_1_MIN:
								Log.d(TAG, "Market : " + marketItem.getMarket().getDisplayName() + " has moved to State : CLOSING_1_MIN");
								marketItem.setActive(true);
								marketItem.setShowTimer(false);
								marketItem.setStatusLabel(application.getString(R.string.waitingForExpiry));
								marketItem.setStatusValue("");
								marketItem.setCurrentLevel(currentLevel);
								marketItem.setProfitText("");
								marketItem.setProfitPercent("");
								break;
							case Opportunity.STATE_CREATED:
								Log.d(TAG, "Market : " + marketItem.getMarket().getDisplayName() + " has moved to State : CREATED");
								marketItem.setActive(false);
								marketItem.setShowTimer(false);
								marketItem.setStatusLabel(application.getString(R.string.availableAt, ""));// Uses empty string for the parameter
								marketItem.setStatusValue(timeCloseFormated);
								marketItem.setCurrentLevel("");
								marketItem.setProfitText("");
								marketItem.setProfitPercent("");
								break;
							case Opportunity.STATE_DONE:
								Log.d(TAG, "Market : " + marketItem.getMarket().getDisplayName() + " has moved to State : DONE");
								break;
							case Opportunity.STATE_HIDDEN_WAITING_TO_EXPIRY:
								Log.d(TAG, "Market : " + marketItem.getMarket().getDisplayName() + " has moved to State : HIDDEN_WAITING_TO_EXPIRY");
								break;
							case Opportunity.STATE_LAST_10_MIN:
								Log.d(TAG, "Market : " + marketItem.getMarket().getDisplayName() + " has moved to State : LAST_10_MIN");
								marketItem.setActive(true);
								marketItem.setStatusLabel(application.getString(R.string.timeLeft) + ": ");
								marketItem.setShowTimer(true);
								marketItem.setCurrentLevel(currentLevel);
								marketItem.setProfitText(profitText);
								marketItem.setProfitPercent(profitPercent);
								break;
							case Opportunity.STATE_OPENED:
								Log.d(TAG, "Market : " + marketItem.getMarket().getDisplayName() + " has moved to State : OPENED");
								marketItem.setActive(true);
								marketItem.setStatusLabel(application.getString(R.string.timeLeft) + ": ");
								marketItem.setShowTimer(true);
								marketItem.setCurrentLevel(currentLevel);
								marketItem.setProfitText(profitText);
								marketItem.setProfitPercent(profitPercent);
								break;
							case Opportunity.STATE_PAUSED:
								Log.d(TAG, "Market : " + marketItem.getMarket().getDisplayName() + " has moved to State : PAUSED");
								marketItem.setActive(false);
								marketItem.setShowTimer(false);
								marketItem.setStatusLabel(application.getString(R.string.availableAt, ""));// Uses empty string for the parameter
								marketItem.setStatusValue(timeCloseFormated);
								marketItem.setCurrentLevel("");
								marketItem.setProfitText("");
								marketItem.setProfitPercent("");
								break;
							case Opportunity.STATE_SUSPENDED:
								Log.d(TAG, "Market : " + marketItem.getMarket().getDisplayName() + " has moved to State : SUSPENDED");
								marketItem.setActive(false);
								marketItem.setShowTimer(false);
								marketItem.setStatusLabel(application.getString(R.string.availableNot));
								marketItem.setStatusValue("");
								marketItem.setCurrentLevel("");
								marketItem.setProfitText("");
								marketItem.setProfitPercent("");
								break;
							case Opportunity.STATE_WAITING_TO_PAUSE:
								Log.d(TAG, "Market : " + marketItem.getMarket().getDisplayName() + " has moved to State : STATE_WAITING_TO_PAUSE");
								marketItem.setActive(true);
								marketItem.setStatusLabel(application.getString(R.string.closingLevel) + " " + currentLevel);
								marketItem.setCurrentLevel("");
								marketItem.setOpportunityID(0);
								marketItem.setOpportunityState(0);
								marketItem.setShowTimer(false);
								marketItem.setProfitText("");
								marketItem.setProfitPercent("");
								break;
							default:
								Log.d(TAG, "Market : " + marketItem.getMarket().getDisplayName() + " NO SUCH STATE");
								break;
						}

					}
				}else{
					// There is no change in the Opportunity's State. So we update only the current level.
					//But only if the State allows this:
					if( 	marketItem.getOpportunityState() == Opportunity.STATE_OPENED 		||
							marketItem.getOpportunityState() == Opportunity.STATE_LAST_10_MIN 	||
							marketItem.getOpportunityState() == Opportunity.STATE_CLOSING 		||
							marketItem.getOpportunityState() == Opportunity.STATE_CLOSING_1_MIN 	){

						marketItem.setCurrentLevel(level);
						marketItem.setProfitText(profitText);
						marketItem.setProfitPercent(profitPercent);
					}
				}

			}
		}

	}

	/**
	 * Returns the Market which are going to be updated by the LIGHSTREAMER.
	 * @return
	 */
	private List<MarketListItem> getMarketsToUpdate(){
		List<MarketListItem> marketList = new ArrayList<MarketListItem>();

		for(MarketListGroup marketGroup : adapter.getMarketGroups()){
			List<MarketListItem> markets = marketGroup.getMarketListItems();
			marketList.addAll(markets);
		}

		return marketList;
	}


	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
		adapter.setFilter(filter);
	}

	public void filter(String filter){
		if(!isListRefreshing){
			setFilter(filter);
			unsubscribeAssetsFromLS();
			adapter.filter(filter);
			subscribeAssetsToLS();
			isListRefreshing = false;
		}
	}

	public byte getMode() {
		return mode;
	}

	public void setMode(byte mode) {
		if (this.mode == mode) {
			return;
		}

		this.mode = mode;
		adapter.setMode(mode);

		// Call this method to reload the markets.
		filter(filter);
		updateFooterVisibility();
	}

	private synchronized MarketListItem getMarketItem(long marketID){
		MarketListItem result = null;
		try {
			for(MarketListGroup marketListGroup : adapter.getMarketGroups()){
				result = marketListGroup.getMarketListItemForID(marketID);
				if(result != null){
					break;
				}
			}
		} catch (Exception e) {
			Log.e(TAG, "SHIT", e);
		}
		return result;
	}

	/**
	 * Starts the List updater.
	 */
	private void startTimer(){
		try {
			if(timer == null){
				timer = new AssetTimer();
			}
			timer.start();
		} catch (Exception e) {}
	}

	/**
	 * Stops the List updater.
	 */
	private void stopTimer(){
		try {
			if(timer != null){
				timer.stop();
				timer = null;
			}
		} catch (Exception e) {}
	}

	///////////////////////////////////////////////////////////////////
	///						ADAPTER Listener Methods				///
	///////////////////////////////////////////////////////////////////

	@Override
	public void onFilter(String filter) {
		filter(filter);
	}

	@Override
	public void onLoading(boolean isLoading) {
		showHideLoading(isLoading);
	}

	@Override
	public void onRefreshing(boolean isRefreshing) {
		this.isListRefreshing = isRefreshing;
	}

	///////////////////////////////////////////////////////////////////

	/**
	 * Custom Timer. It has a List of {@link MarketListItem} that are updated every second.
	 * The timer also updates each Market's "Left Time" timer.
	 * @author Anastas Arnaudov
	 *
	 */
	private class AssetTimer{

		/**
		 * The update interval in milliseconds.
		 */
		private static final long UPDATE_INTERVAL = 1000l;
		private Timer timer;
		private AssetsTimertask task;
		private List<MarketListItem> marketItemsList;
		private String waitingForExpiryLabel = application.getString(R.string.waitingForExpiry);
		private String emptyLabel			 = "";
		private long redTimeInterval 		 = 10*60*1000l;
		private long redTimeIntervalDynamics =  5*60*1000l;

		public AssetTimer(){
			marketItemsList = getMarketsToUpdate();
		}

		public void start(){
			timer = new Timer();
			task = new AssetsTimertask();
			timer.scheduleAtFixedRate(task, 0, UPDATE_INTERVAL);
		}

		public void stop(){
			try {
				timer.cancel();
				timer = null;
				task = null;
			} catch (Exception e) {}
		}

		public synchronized List<MarketListItem> getMarketItemsList() {
			return marketItemsList;
		}

		/**
		 * Background task that updates the List.
		 * It also updates the "Time left:" of each Market Item.
		 * @author Anastas Arnaudov
		 *
		 */
		private class AssetsTimertask extends TimerTask{

			@Override
			public void run() {
				if(AssetsListFragment.this.isAdded()){

					long currentTime = TimeUtils.getCurrentServerTime();

					//Iterates over the list items:
					for(MarketListItem marketListItem : getMarketItemsList()){
						boolean isDynamics = marketListItem.getMarket().getProductTypeId() == Constants.PRODUCT_TYPE_ID_DYNAMICS;
						long redTime;
						if(isDynamics){
							redTime = redTimeIntervalDynamics;
						}else{
							redTime = redTimeInterval;
						}

						if(marketListItem.isShowTimer()){

							long timeLeft = marketListItem.getTimeLastInvest() - currentTime;

							//If the timer reaches zero we do nothing and wait for the "Wait For EXPIRY" state from the LIGHSTREAMER. 
							if(timeLeft <= 0){
								marketListItem.setStatusLabel(waitingForExpiryLabel);
								marketListItem.setStatusValue(emptyLabel);
								continue;
							}

							//If the timer enters the last 5/10 minutes the label should become RED:
							if(timeLeft <= redTime){
								marketListItem.setLastMinutesState(true);
							}else{
								marketListItem.setLastMinutesState(false);
							}
							marketListItem.setStatusValue(TimeUtils.formatTimeToMinutesAndSeconds(timeLeft));
						}
					}

					//Update the list:
					timerHandler.post(new Runnable() {
						@Override
						public void run() {
							refreshList();
						}
					});
				}

			}
		}
	}

}
