package com.anyoption.android.app.util;

import java.util.Hashtable;

import android.content.Context;
import android.graphics.Typeface;

public final class Typefaces {

	public static final String TAG = Typefaces.class.getSimpleName();

	private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();

	public static Typeface get(Context c, String assetPath) {
		synchronized (cache) {
			if (!cache.containsKey(assetPath)) {
				try {
					Typeface t = Typeface.createFromAsset(c.getAssets(),assetPath);
					cache.put(assetPath, t);
				} catch (Exception e) {
					return null;
				}
			}
			return cache.get(assetPath);
		}
	}

}