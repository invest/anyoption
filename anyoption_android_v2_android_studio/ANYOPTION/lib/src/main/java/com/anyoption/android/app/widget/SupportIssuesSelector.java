/**
 * 
 */
package com.anyoption.android.app.widget;

import java.util.Arrays;

import com.anyoption.android.R;
import com.anyoption.android.app.widget.form.FormSelector;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.RelativeLayout;


/**
 * @author kirilim
 *
 */
public class SupportIssuesSelector extends FormSelector<String> {
	
	private RelativeLayout rootView;
	private TextView textView;

	public SupportIssuesSelector(Context context) {
		super(context);
	}
	
	public SupportIssuesSelector(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public SupportIssuesSelector(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	@Override
	protected void setupUI() {
		if (rootView == null) {
			rootView = (RelativeLayout) View.inflate(getContext(), R.layout.support_issues_selector, this);
			textView = (TextView) rootView.findViewById(R.id.support_issues_selector_text);
			textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
			setBackgroundResource(android.R.color.transparent);
		}
		
		if(!isInEditMode()){			
			String issue1 = getResources().getString(R.string.supportScreenIssue1);
			String issue2 = getResources().getString(R.string.supportScreenIssue2);
			String issue3 = getResources().getString(R.string.supportScreenIssue3);
			String issue4 = getResources().getString(R.string.supportScreenIssue4);
			String issue5 = getResources().getString(R.string.supportScreenIssue5);
			
			String[] issues = new String[]{issue1, issue2, issue3, issue4, issue5};
			textView.setText(issues[0]);
			setItems(Arrays.asList(issues), 0, new ItemListener<String>() {
				
				@Override
				public void onItemSelected(String selectedItem) {
					textView.setText(selectedItem);				
				}
				
				@Override
				public String getDisplayText(String selectedItem) {
					return selectedItem;
				}
				
				@Override
				public Drawable getIcon(String selectedItem) {
					return null;
				}});
		}else{
			textView.setText("Issues Selector");
		}
		
	}

	@Override
	public void setupUIForDirection() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setFieldName(String fieldName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getFieldName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void displayError(String error) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @return the selected issue type
	 */
	public String getSelectedIssueType() {
		return textView.getText().toString();
	}
}