package com.anyoption.android.app.activity;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.event.InsuranceBoughtEvent;
import com.anyoption.android.app.event.KeyboardVisibilityChangeEvent;
import com.anyoption.android.app.event.LocaleChangeEvent;
import com.anyoption.android.app.manager.AppoxeeManager;
import com.anyoption.android.app.manager.GCMManager.GooglePlayServicesCode;
import com.anyoption.android.app.manager.PermissionsManager;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.UserMethodResult;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;

/**
 * Base Activity class that implements basic functionality that will be common to all other Activities.
 * @author Anastas Arnaudov
 *
 */
public class BaseActivity extends FragmentActivity{

	public static final String TAG = BaseActivity.class.getSimpleName();

	public static GooglePlayServicesCode isPushNotificationsSupported;

	protected Application application;
	protected AppoxeeManager appoxeeManager;
	protected boolean isTablet;


	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		isTablet = getResources().getBoolean(R.bool.isTablet);

		setupDefaultScreenOrientation();

		application = Application.get();
		appoxeeManager = application.getAppoxeeManager();

		application.setCurrentActivity(this);
		application.registerForEvents(this, LocaleChangeEvent.class, InsuranceBoughtEvent.class);

		// Clear the stack when the new activity is started.
		clearScreensStack();

		//Check if the device supports Push Notifications:
		if(isPushNotificationsSupported == null){
			Log.d(TAG, "Checking if Push Notifications are supported!");
			BaseActivity.isPushNotificationsSupported = application.getGCMManager().checkGooglePlayServicesAvailable();
		}

		application.getAppoxeeManager().init(this);
	}

	protected void setupDefaultScreenOrientation(){
		if(getResources().getBoolean(R.bool.isTablet)) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		} else {
			// This is mobile phone
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		application.setCurrentActivity(this);
		refreshUser();
		setupKeyboardListener();
		application.registerForEvents(this, LocaleChangeEvent.class, InsuranceBoughtEvent.class);
		appoxeeManager.onResume();
		application.getFacebookManager().activateAppLogger(this);
	}

	@Override
	protected void onPause() {
		stopRequests();
		super.onPause();
		application.getFacebookManager().deactivateAppLogger(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		appoxeeManager.onStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
		appoxeeManager.onStop();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		if(application.getDeepLinksManager().parseIntent(this, intent)){
			//The navigation will be configured by the DEEP LINKS Manager.
		}else{
			appoxeeManager.onNewIntent(intent);
		}
	}

	/**
	 * Called when a {@link LocaleChangeEvent} occurs.
	 * @param event
	 */
	public void onEventMainThread(LocaleChangeEvent event){

		if(this != application.getCurrentActivity()){
			Log.d(TAG,  "Ignore event. This is not the current Activity.");
			return;
		}

		//RESTART THE APPLICATION (because the strings get messed up):
		Screenable toScreen = event.getToScreen();
		if(toScreen == null){
			toScreen = application.getCurrentScreen();
		}
		if(this instanceof LaunchActivity){
			application.recreateLaunchActivity(this);
		}else{
			application.restartApplication(toScreen, true);
		}

	}

	/**
	 * Callback for the {@link InsuranceBoughtEvent} events.
	 *
	 * @param event
	 */
	public void onEvent(InsuranceBoughtEvent event) {
		refreshUser();
	}

	/**
	 * Stops the requests.
	 */
	public void stopRequests(){
		application.getCommunicationManager().cancelAllRequests();
	}

	@Override
	public void onBackPressed() {
		application.getFragmentManager().onBack();
	}

	public void refreshUser() {
		if (application.getUser() != null) {
			Log.d(TAG, "refreshUser");
			application.getCommunicationManager().requestService(this, "getUserCallback", application.getServiceMethodGetUser(), new UserMethodRequest(), UserMethodResult.class, false, false);
		}
	}

	public void getUserCallback(Object resultObj) {
		UserMethodResult result = (UserMethodResult) resultObj;

		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			application.setUser(result.getUser());
		}
	}

	/**
	 * Sets up a Listener for the keyboard shown/hidden states.
	 */
	public final void setupKeyboardListener() {
		final View activityRootView = ((ViewGroup) findViewById(android.R.id.content)).getChildAt(0);
		activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

			private boolean wasOpened;
			private final Rect r = new Rect();
			private int heightDiff;

			@Override
			public void onGlobalLayout() {
				activityRootView.getWindowVisibleDisplayFrame(r);
				int heightDiff = activityRootView.getRootView().getHeight() - (r.bottom - r.top);

				boolean isOpen = heightDiff > 100;
				if (isOpen == wasOpened) {
					if(this.heightDiff != heightDiff){
						//The keyboard is still open, but has changed its appearance (and accordingly size):
						this.heightDiff = heightDiff;
						application.postEvent(new KeyboardVisibilityChangeEvent(isOpen));
					}
					return;
				}
				wasOpened = isOpen;
				Log.d(TAG, "Keyboard is " + ((isOpen) ? "visible" : "invisible"));
				application.postEvent(new KeyboardVisibilityChangeEvent(isOpen));
			}
		});
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		Log.d(TAG, "Getting results from permissions request");
		if (requestCode == PermissionsManager.REQUEST_PERMISSIONS_REQUEST_CODE) {
			application.getPermissionsManager().onPermissionsResult(permissions, grantResults);
		}
	}

	protected void clearScreensStack(){
		application.getFragmentManager().clearStack();
	}

}
