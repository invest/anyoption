package com.anyoption.android.app.popup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;

public class LoginRegisterDialogfragment extends BaseDialogFragment {

	public static final String TAG = LoginRegisterDialogfragment.class.getSimpleName();

	public static LoginRegisterDialogfragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new LoginRegisterDialogfragment.");
		LoginRegisterDialogfragment fragment = new LoginRegisterDialogfragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	/**
	 * Set up content.
	 * 
	 * @param contentView
	 */
	@Override
	protected void onCreateContentView(View contentView) {
		View rootView = contentView.findViewById(R.id.login_register_popup_root_view);
		rootView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});

		boolean isTablet = getResources().getBoolean(R.bool.isTablet);
		final NavigationType navigationType;
		if(isTablet){
			navigationType = NavigationType.DEEP;
		}else{
			navigationType = NavigationType.INIT;
		}

		TextView titleTextView = (TextView) contentView.findViewById(R.id.login_register_popup_header);

		View registerButton = contentView.findViewById(R.id.login_register_popup_register_button);
		registerButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				Application.get().postEvent(new NavigationEvent(getRegisterScreen(), navigationType));
			}
		});
		
		View loginButton = contentView.findViewById(R.id.login_register_popup_login_button);
		loginButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				Application.get().postEvent(new NavigationEvent(getLoginScreen(), navigationType));
			}
		});

	}

	@Override
	protected int getContentLayout() {
		return R.layout.login_register_popup_layout;
	}

	protected Screenable getRegisterScreen(){
		return Screen.REGISTER;
	}
	protected Screenable getLoginScreen(){
		return Screen.LOGIN;
	}
}
