package com.anyoption.android.app.popup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.anyoption.android.R;

/**
 * @author Anastas Arnaudov
 */
public class InvestmentCanceledDialogFragment extends BaseDialogFragment {

    public static final String TAG = InvestmentCanceledDialogFragment.class.getSimpleName();

    public static InvestmentCanceledDialogFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new InvestmentCanceldDialogFragment.");
        InvestmentCanceledDialogFragment fragment = new InvestmentCanceledDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getContentLayout() {
        return R.layout.popup_investment_canceled;
    }

    @Override
    protected void onCreateContentView(View contentView) {

        //Set up the button:
        View okButton = contentView.findViewById(R.id.ok_button);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

}
