package com.anyoption.android.app.widget;

import com.anyoption.android.R;
import com.anyoption.android.app.util.ColorUtils;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;

/**
 * Custom Separator.
 * @author Anastas Arnaudov
 *
 */
public class Separator extends View {

	public Separator(Context context) {
		super(context);
		init(context, null, 0);
	}
	
	public Separator(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	
	public Separator(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs, defStyleAttr);
	}

	private void init(Context context, AttributeSet attrs, int defStyleAttr){
		//Parse custom attributes:
		TypedArray attributesTypedArray = context.obtainStyledAttributes(attrs,R.styleable.Separator);
		int colorBG = attributesTypedArray.getColor(R.styleable.Separator_colorBG, 0);
		attributesTypedArray.recycle();

		//Set BG:
		if(colorBG == 0){
			//Default color:
			colorBG = ColorUtils.getColor(R.color.separator);
		}
		setBackgroundColor(colorBG);

		
	}
	
}
