package com.anyoption.android.app.fragment.trade;

import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Market;

import android.os.Bundle;

/**
 * TODO
 * 
 * @author Mario Kutlev
 */
public abstract class BaseAssetNestedFragment extends BaseFragment {
	
	/**
	 * Use {@link #ARG_MARKET_KEY} to set its value in fragment's arguments.
	 */
	protected Market market;

	/**
	 * Use {@link #ARG_AUTO_LOAD_KEY} to set its value in fragment's arguments.
	 */
	protected boolean isAutoLoad;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		if (args != null) {
			market = (Market) args.getSerializable(Constants.EXTRA_MARKET);
			isAutoLoad = args.getBoolean(Constants.EXTRA_AUTO_LOAD_MARKET, true);
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		if (isAutoLoad) {
			loadData();
		}
	}

	/**
	 * Loads fragment's data in {@link #onStart()} if {@link #isAutoLoad} is true. Use this method if
	 * you've set autoLoad to false or you want to reload the data.
	 */
	public abstract void loadData();

	public Market getMarket() {
		return market;
	}

	public void setMarket(Market market) {
		if(this.market == null || (this.market != null && this.market.getId() != market.getId())) {
			this.market = market;
			onMarketChanged();
		}
	}
	
	
	public boolean isAutoLoad() {
		return isAutoLoad;
	}

	
	public void setAutoLoad(boolean isAutoLoad) {
		this.isAutoLoad = isAutoLoad;
	}

	/**
	 * Override this method to listen to market change events.
	 */
	protected void onMarketChanged() {
		// Do nothing
	}

}
