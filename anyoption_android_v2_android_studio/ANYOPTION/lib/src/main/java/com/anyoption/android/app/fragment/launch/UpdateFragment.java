package com.anyoption.android.app.fragment.launch;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.event.UpdateSkipEvent;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.constants.Constants;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Update Screen.
 * @author Anastas Arnaudov
 *
 */
public class UpdateFragment extends NavigationalFragment {

	private Button updateButton;
	private String updateDownloadLink;
	private boolean isUpdateMandatory;
	private TextView remindMeLatertextView;
	
	/**
	 * Use this method to create new instance of the fragment.
	 * @return
	 */
	public static UpdateFragment newInstance(Bundle bundle){
		UpdateFragment updateFragment = new UpdateFragment();
	    updateFragment.setArguments(bundle);
		return updateFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.update_fragment_layout, container, false);
		
		updateDownloadLink = getArguments().getString(Constants.EXTRA_UPDATE_DOWNLOAD_LINK);
		isUpdateMandatory = getArguments().getBoolean(Constants.EXTRA_UPDATE_MANDATORY);
		
		//TODO
		//Layout Design difference based on the "isUpdateMandatory"
		remindMeLatertextView = (TextView) rootView.findViewById(R.id.update_remind_me_later_textview);
		remindMeLatertextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				application.postEvent(new UpdateSkipEvent());
			}
		});
		if(isUpdateMandatory){
			remindMeLatertextView.setVisibility(View.GONE);
		}else{
			remindMeLatertextView.setVisibility(View.VISIBLE);
		}
		
		
		updateButton = (Button) rootView.findViewById(R.id.update_update_button);
		updateButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(updateDownloadLink != null){
					Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateDownloadLink));
					startActivity(intent);
					Application.get().getCurrentActivity().finish();
				}
			}
		});
		
		return rootView;
	}

	@Override
	protected Screenable setupScreen() {
		return Screen.UPDATE_APPLICATION_VERSION;
	}

	
}
