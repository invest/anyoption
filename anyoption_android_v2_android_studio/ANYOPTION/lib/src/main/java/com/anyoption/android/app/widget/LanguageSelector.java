package com.anyoption.android.app.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.LanguageChangeEvent;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.widget.form.FormSelector;
import com.anyoption.common.beans.base.Skin;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Custom Button that opens a List of Languages to choose from.
 * @author Anastas Arnaudov
 */
public class LanguageSelector extends FormSelector<com.anyoption.common.beans.base.Language> {
	
	public static final String TAG = LanguageSelector.class.getSimpleName();
	
	public static final int MODE_LAUNCH 		= 1;
	public static final int MODE_SETTINGS 		= 2;
	
	private Long skinID = null;
	private List<com.anyoption.common.beans.base.Language> languageList;

	protected TextView text;

	private RelativeLayout rootView;
	private int mode = MODE_LAUNCH;
	/**
	 * Extend the {@link com.anyoption.common.beans.base.Language} to provide more handy constructor.
	 * @author Anastas Arnaudov
	 *
	 */
	private class Language extends com.anyoption.common.beans.base.Language{

		private static final long serialVersionUID = 1L;

		public Language(String displayName, String code){
			super.setDisplayName(displayName);
			super.setCode(code);
		}
	}
	
	public LanguageSelector(Context context) {
		super(context);
	}
	public LanguageSelector(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	public LanguageSelector(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}


	protected int getSelectorLayout() {
		return R.layout.language_selector;
	}

	protected int getSelectorTextColor() {
		return R.color.language_selector_text;
	}

	protected int getSelectorTextSize() {
		return R.dimen.language_selector_text;
	}

	protected Font getSelectorTextFont() {
		return Font.ROBOTO_LIGHT;
	}
	
	@Override
	protected void setupUI() {
		if(rootView == null){
			rootView = (RelativeLayout) View.inflate(getContext(), getSelectorLayout(), this);
			rootView.setGravity(Gravity.CENTER_VERTICAL|Gravity.LEFT);
			text = (TextView) rootView.findViewById(R.id.language_selector_text);
			text.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(getSelectorTextSize()));
			text.setTextColor(ColorUtils.getColor(getSelectorTextColor()));
			text.setFont(getSelectorTextFont());
			setBackgroundResource(android.R.color.transparent);
		}
	
		if(!isInEditMode()){
			setupUIForDirection();
			setupLanguages();
			
			//Set the default language:
			//Get the language saved for the current locale:
			com.anyoption.common.beans.base.Language defaultLanguage = getLanguageForLocale(Application.get().getLocale().toString());
			if(defaultLanguage == null){
				//Get the default language:
				defaultLanguage = getLanguageForDisplayName(Application.get().getSkins().get(Skin.SKIN_ENGLISH).getName());
			}
			if(defaultLanguage == null){
				//Get the default language:
				defaultLanguage = getLanguageForDisplayName(Application.get().getSkins().get(Skin.SKIN_ENGLISH_NON_REG).getName());
			}
			if(defaultLanguage == null && languageList != null){
				//Get the first language:
				defaultLanguage = languageList.get(0);
			}
			////////////////////////////////////
			if(defaultLanguage != null){
				text.setText(defaultLanguage.getDisplayName());
				setItems(languageList, languageList.indexOf(defaultLanguage), new ItemListener<com.anyoption.common.beans.base.Language>() {
					@Override
					public void onItemSelected(com.anyoption.common.beans.base.Language selectedItem) {
						text.setText(selectedItem.getDisplayName());
						Log.d(TAG, "Language Changed");
						boolean shouldGoToMain = (getMode() == MODE_SETTINGS) ? true : false;
						Application.get().postEvent(new LanguageChangeEvent(selectedItem.getCode(), shouldGoToMain));
						setDirection(Application.get().getLayoutDirection());
					}
					@Override
					public String getDisplayText(com.anyoption.common.beans.base.Language selectedItem) {
						return selectedItem.getDisplayName();
					}
					@Override
					public Drawable getIcon(com.anyoption.common.beans.base.Language selectedItem) {
						return null;
					}
				});
			}

		}else{
			text.setTextColor(ColorUtils.getColor(R.color.black));
			text.setText("Language Selector");
		}
	}
	
	private void setupLanguages() {
		languageList = new ArrayList<com.anyoption.common.beans.base.Language>();
		Application application = Application.get();
		Map<Long, Skin> skinsMap = application.getVisibleSkinsMap();
		if(skinID != null && (skinID == Skin.SKIN_EN_US || skinID == Skin.SKIN_ES_US)){
			languageList.add(new Language(application.getString(R.string.languageEnglish), skinsMap.get(Skin.SKIN_EN_US).getLocale()));
			languageList.add(new Language(application.getString(R.string.languageSpanish), skinsMap.get(Skin.SKIN_ES_US).getLocale()));
		}else if((skinID != null && skinID == Skin.SKIN_ETRADER) || application.getSkinId() == Skin.SKIN_ETRADER){
			languageList.add(new Language(skinsMap.get(Skin.SKIN_ETRADER).getName(), skinsMap.get(Skin.SKIN_ETRADER).getLocale()));
		}else{
			//English:
			if(skinsMap.get(Skin.SKIN_ENGLISH) != null){
				languageList.add(new Language(application.getString(R.string.languageEnglish), skinsMap.get(Skin.SKIN_ENGLISH).getLocale()));
			}
			if(skinsMap.get(Skin.SKIN_REG_EN) != null){
				languageList.add(new Language(application.getString(R.string.languageEnglish), skinsMap.get(Skin.SKIN_REG_EN).getLocale()));
			}
			if(skinsMap.get(Skin.SKIN_ENGLISH_NON_REG) != null){
				languageList.add(new Language(application.getString(R.string.languageEnglish), skinsMap.get(Skin.SKIN_ENGLISH_NON_REG).getLocale()));
			}

			//Turkish:
			if(skinsMap.get(Skin.SKIN_TURKISH) != null){
				languageList.add(new Language(application.getString(R.string.languageTurkish), skinsMap.get(Skin.SKIN_TURKISH).getLocale()));
			}

			//Spanish:
			if(skinsMap.get(Skin.SKIN_SPAIN) != null){
				languageList.add(new Language(application.getString(R.string.languageSpanish), skinsMap.get(Skin.SKIN_SPAIN).getLocale()));
			}
			if(skinsMap.get(Skin.SKIN_REG_ES) != null){
				languageList.add(new Language(application.getString(R.string.languageSpanish), skinsMap.get(Skin.SKIN_REG_ES).getLocale()));
			}
			if(skinsMap.get(Skin.SKIN_SPAIN_NON_REG) != null){
				languageList.add(new Language(application.getString(R.string.languageSpanish), skinsMap.get(Skin.SKIN_SPAIN_NON_REG).getLocale()));
			}

			//German:
			if(skinsMap.get(Skin.SKIN_GERMAN) != null){
				languageList.add(new Language(application.getString(R.string.languageGerman), skinsMap.get(Skin.SKIN_GERMAN).getLocale()));
			}
			if(skinsMap.get(Skin.SKIN_REG_DE) != null){
				languageList.add(new Language(application.getString(R.string.languageGerman), skinsMap.get(Skin.SKIN_REG_DE).getLocale()));
			}

			//Italian:
			if(skinsMap.get(Skin.SKIN_ITALIAN) != null) {
				languageList.add(new Language(application.getString(R.string.languageItalian), skinsMap.get(Skin.SKIN_ITALIAN).getLocale()));
			}
			if(skinsMap.get(Skin.SKIN_REG_IT) != null) {
				languageList.add(new Language(application.getString(R.string.languageItalian), skinsMap.get(Skin.SKIN_REG_IT).getLocale()));
			}

			//Russian:
			if(skinsMap.get(Skin.SKIN_RUSSIAN) != null){
				languageList.add(new Language(application.getString(R.string.languageRussian), skinsMap.get(Skin.SKIN_RUSSIAN).getLocale()));
			}

			//French:
			if(skinsMap.get(Skin.SKIN_FRANCE) != null){
				languageList.add(new Language(application.getString(R.string.languageFrench), skinsMap.get(Skin.SKIN_FRANCE).getLocale()));
			}
			if(skinsMap.get(Skin.SKIN_REG_FR) != null){
				languageList.add(new Language(application.getString(R.string.languageFrench), skinsMap.get(Skin.SKIN_REG_FR).getLocale()));
			}

			//Dutch:
			if(skinsMap.get(Skin.SKIN_DUTCH_REG) != null){
				languageList.add(new Language(application.getString(R.string.languageDutch), skinsMap.get(Skin.SKIN_DUTCH_REG).getLocale()));
			}

			//Swedish:
			if(skinsMap.get(Skin.SKIN_SWEDISH_REG) != null){
				languageList.add(new Language(application.getString(R.string.languageSwedish), skinsMap.get(Skin.SKIN_SWEDISH_REG).getLocale()));
			}
		}
		
	}
	
	public Long getSkinID() {
		return skinID;
	}
	
	public void setSkinID(Long skinID) {
		this.skinID = skinID;
		setupUI();
	}
	
	public com.anyoption.common.beans.base.Language getLanguageForSkinID(Long skinID){		
		com.anyoption.common.beans.base.Language result = null;
		if(skinID != null && skinID > 0L){
			Skin skin = Application.get().getSkins().get(skinID);
			if(skin != null){
				String name = skin.getName();
				result = getLanguageForDisplayName(name);		
			}
		}
		return result;
	}
	
	public com.anyoption.common.beans.base.Language getLanguageForLocale(String locale){
		com.anyoption.common.beans.base.Language result = null;
		if(locale != null){
			for(com.anyoption.common.beans.base.Language language : languageList){
				if(language.getCode().startsWith(locale)){
					result = language;
					break;
				}
			}			
		}
		return result;
	}
	
	public com.anyoption.common.beans.base.Language getLanguageForCode(String code){
		com.anyoption.common.beans.base.Language result = null;
		if(code != null){
			for(com.anyoption.common.beans.base.Language language : languageList){
				if(language.getCode().equals(code)){
					result = language;
					break;
				}
			}			
		}
		return result;
	}
	
	public com.anyoption.common.beans.base.Language getLanguageForDisplayName(String displayName){
		com.anyoption.common.beans.base.Language result = null;
		if(displayName != null){
			for(com.anyoption.common.beans.base.Language language : languageList){
				if(language.getDisplayName().equals(displayName)){
					result = language;
					break;
				}
			}			
		}
		return result;
	}
	
	@Override
	public void setupUIForDirection() {
		if(mode == MODE_LAUNCH){
			rootView.findViewById(R.id.language_selector_image).setVisibility(View.VISIBLE);
			rootView.findViewById(R.id.language_selector_image_right).setVisibility(View.GONE);
		}else if(mode == MODE_SETTINGS){
			rootView.findViewById(R.id.language_selector_image).setVisibility(View.GONE);
			rootView.findViewById(R.id.language_selector_image_right).setVisibility(View.VISIBLE);
		}
	}
	@Override
	public void setFieldName(String fieldName) {
 
	}
	@Override
	public String getFieldName() {
		return null;
	}
	@Override
	public void displayError(String error) {
 
	}
	public int getMode() {
		return mode;
	}
	public void setMode(int mode) {
		this.mode = mode;
		setupUIForDirection();
	}
	
}
