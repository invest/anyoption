package com.anyoption.android.app.popup;

import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application.*;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.event.UserUpdatedEvent;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.DepositBonusBalanceBase;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.service.requests.DepositBonusBalanceMethodRequest;
import com.anyoption.common.service.results.DepositBonusBalanceMethodResult;

public class LowCashBalanceDialogFragment extends BaseDialogFragment {

    public static final String TAG = LowCashBalanceDialogFragment.class.getSimpleName();

    public TextView messageTextView;

    private String cashBalance = "";

    /**
     * Use this method to create new instance of the fragment.
     *
     * @return
     */
    public static LowCashBalanceDialogFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new LowCashBalanceDialogFragment.");
        LowCashBalanceDialogFragment fragment = new LowCashBalanceDialogFragment();
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
        bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            cashBalance = args.getString(Constants.EXTRA_CASH_BALANCE, "");
            setCancelable(args.getBoolean(Constants.EXTRA_CANCELABLE, true));
        }
    }

    protected void getSplitBalance() {
        DepositBonusBalanceMethodRequest request = new DepositBonusBalanceMethodRequest();
        request.setUserId(application.getUser().getId());
        request.setBalanceCallType(1);
        if (!application.getCommunicationManager().requestService(LowCashBalanceDialogFragment.this, "getSplitBalanceCallback", Constants.SERVICE_GET_SPLIT_BALANCE, request, DepositBonusBalanceMethodResult.class, false, false)) {
        }
    }


    public void getSplitBalanceCallback(Object resObj) {
        DepositBonusBalanceMethodResult result = (DepositBonusBalanceMethodResult) resObj;
        if (result != null && result instanceof DepositBonusBalanceMethodResult && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {

            if (result.getDepositBonusBalanceBase() != null) {
                DepositBonusBalanceBase depositBonusBalanceBase = result.getDepositBonusBalanceBase();
                cashBalance = AmountUtil.getUserFormattedAmount(depositBonusBalanceBase.getDepositCashBalance());
                if (messageTextView != null) {
                    String cashBalanceText = messageTextView.getText().toString().replace("%cash_balance_parameter%", cashBalance);
                    messageTextView.setText(cashBalanceText);
                }
            }
        }
    }

    /**
     * Set up content.
     *
     * @param contentView
     */
    @Override
    protected void onCreateContentView(View contentView) {
        messageTextView = (TextView) contentView.findViewById(R.id.low_cash_balance_popup_message_text);

        if(cashBalance.equals("")) {
        getSplitBalance();
        } else {
            String cashBalanceText = messageTextView.getText().toString().replace("%cash_balance_parameter%", cashBalance);
            messageTextView.setText(cashBalanceText);
        }

        View depositButton = contentView.findViewById(R.id.deposit_button);
        depositButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                application.postEvent(new NavigationEvent(Screen.DEPOSIT_MENU, NavigationType.DEEP));
                dismiss();
            }
        });
    }

    @Override
    protected int getContentLayout() {
        return R.layout.popup_low_cash_balance;
    }

}
