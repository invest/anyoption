package com.anyoption.android.app.fragment.login_register;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.manager.LoginManager;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.form.Form;
import com.anyoption.android.app.widget.form.FormCheckBox;
import com.anyoption.android.app.widget.form.FormEditText;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A simple {@link BaseFragment} subclass. 
 * <p>It manages the Login screen and functionality.
 * 
 */
public class LoginFragment extends NavigationalFragment {
	public static final String TAG = LoginFragment.class.getSimpleName();
	
	private Form form;
	private LoginManager loginManager;
	
	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static LoginFragment newInstance(Bundle bundle){
		LoginFragment loginFragment = new LoginFragment();
		loginFragment.setArguments(bundle);
		return loginFragment;
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if(!hidden){
			if(form != null){
				form.setupDirection(application.getLayoutDirection());
			}
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.login_fragment_layout, container, false);
		
		boolean isTablet = getResources().getBoolean(R.bool.isTablet);
		FormEditText emailField = (FormEditText) rootView.findViewById(R.id.login_email_edittext);
		FormEditText passwordField = (FormEditText) rootView.findViewById(R.id.login_password_edittext);
		TextView forgotPassTextView = (TextView) rootView.findViewById(R.id.login_forgot_pass_link_textview);
		RequestButton loginButton = (RequestButton) rootView.findViewById(R.id.login_button_test);
		FormCheckBox rememberPasswordCheckBox = (FormCheckBox) rootView.findViewById(R.id.login_remember_password);
		underlineForgotPass(forgotPassTextView);
		loginManager = setupLoginManager();
		loginManager.onCreateView(this, isTablet, emailField, passwordField, forgotPassTextView, loginButton, rememberPasswordCheckBox);
		form = loginManager.getForm();
		
		return rootView;
	}

	protected void underlineForgotPass(TextView textView){
		TextUtils.underlineText(textView);
	}
	
	protected LoginManager setupLoginManager(){
		return new LoginManager();
	}
	
	@Override
	protected Screenable setupScreen() {
		return Screen.LOGIN;
	}
}