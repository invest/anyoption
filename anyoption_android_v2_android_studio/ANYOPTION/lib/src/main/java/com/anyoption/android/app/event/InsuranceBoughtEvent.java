package com.anyoption.android.app.event;


/**
 * Event that occurs when an Insurance was bought successfully.
 * @author Anastas Arnaudov
 *
 */
public class InsuranceBoughtEvent {

	private int insuranceType;
	
	public InsuranceBoughtEvent(int insuranceType){
		this.setInsuranceType(insuranceType);
	}

	public int getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(int insuranceType) {
		this.insuranceType = insuranceType;
	}
	
}
