package com.anyoption.android.app.fragment;

import com.anyoption.android.R;
import com.anyoption.android.app.event.ExpiredOptionsEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.LoginLogoutEvent.Type;
import com.anyoption.android.app.event.ReverseWithdrawEvent;
import com.anyoption.android.app.event.UserUpdatedEvent;
import com.anyoption.android.app.event.WithdrawEvent;
import com.anyoption.android.app.popup.SplitBalanceDropdownDialogFragment;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.Clock;
import com.anyoption.common.beans.base.User;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A simple {@link android.app.Fragment} subclass.
 * <p>It represents the status component of each screen.
 * 
 */
public class StatusFragment extends BaseFragment  implements OnClickListener{
	
	public static final long ALERT_BALANCE_VALUE = 25l;
	
	/**
	 * Use this method to obtain new instance of the fragment.
	 * @return
	 */
	public static StatusFragment newInstance(){
		return StatusFragment.newInstance(new Bundle());
	}
	
	/**
	 * Use this method to obtain new instance of the fragment.
	 * @return
	 */
	public static StatusFragment newInstance(Bundle bundle){
		StatusFragment statusfragment = new StatusFragment();
		statusfragment.setArguments(bundle);
		return statusfragment;
	}

/*	protected ImageView dotOpenOptions;
	protected TextView openOptionsTextView;
	protected TextView openOptionsLabelTextView;*/

	protected Clock clock;
	
	protected TextView balanceLabelTextView;
	protected TextView balanceTextView;
	protected View balanceInfoImageView;
	protected View balanceView;
	
	protected User user;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = getArguments();
		if(bundle != null){
			// Get arguments passed in the bundle
		}
		
		user = application.getUser();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.status_fragment_layout, container, false);

		//CLOCK:
		clock = (Clock) rootView.findViewById(R.id.status_time_digital_clock);
		///////////
		
		//BALANCE:
		balanceTextView = (TextView) rootView.findViewById(R.id.status_balance_text_view);
		balanceLabelTextView = (TextView) rootView.findViewById(R.id.status_balance_label_text_view);
		balanceLabelTextView.setText(getString(R.string.balance) + ": ");
		//////////////
		
		refresh();

		return rootView;
	}
	
	@Override
	public void onResume() {
		application.registerForEvents(this, UserUpdatedEvent.class, LoginLogoutEvent.class, ExpiredOptionsEvent.class, WithdrawEvent.class, ReverseWithdrawEvent.class);
		super.onResume();
	}
	
	@Override
	public void onPause() {
		application.unregisterForAllEvents(this);
		super.onPause();
	}

	@Override
	public void onClick(View v) {
		if(v == balanceView){
			int[] coordinates = new int[2];
			balanceInfoImageView.getLocationOnScreen(coordinates);
			int screenWidth = ScreenUtils.getScreenWidth()[0];
			int anchorRight = coordinates[0];
			int anchorWidth = DrawableUtils.getDrawableSize(R.drawable.split_balance_popup_info_icon)[0];
			int pointerWidth = DrawableUtils.getDrawableSize(R.drawable.split_balance_popup_pointer)[0];
			int paddingRight = screenWidth - anchorRight - anchorWidth/2 - pointerWidth/2;
			
			Bundle bundle = new Bundle();
			bundle.putInt(Constants.EXTRA_AMOUNT, paddingRight);
			bundle.putInt(Constants.EXTRA_TYPE, 1);

			application.getFragmentManager().showDialogFragment(SplitBalanceDropdownDialogFragment.class, bundle);
		}
	}
	
	//###########################################################################
	//##################			EVENT CALLBACKs			#####################
	//###########################################################################

	public void onEventMainThread(UserUpdatedEvent event){
		user = application.getUser();

		refresh();
	}

	public void onEventMainThread(LoginLogoutEvent event){
		if(event.getType() == Type.LOGIN || event.getType() == Type.REGISTER){
			user = event.getUserResult().getUser();			
		}else{
			user = null;
		}
		
		refresh();
	}

	public void onEventMainThread(ExpiredOptionsEvent event){
		user = application.getUser();
		
		refresh();
	}

	public void onEventMainThread(WithdrawEvent event){
		user = application.getUser();
		
		refresh();
	}

	public void onEventMainThread(ReverseWithdrawEvent event){
		user = application.getUser();
		
		refresh();
	}
	
	//############################################################################
	
	/**
	 * Updates the UI.
	 */
	protected void refresh() {
		refreshOpenOptions();
		refreshBalance();
	}

	/**
	 * Updates the Open Options.
	 */
	private void refreshOpenOptions() {
		/*if (user != null && user.getOpenedInvestmentsCount() > 0) {
			dotOpenOptions.setVisibility(View.VISIBLE);
			openOptionsTextView.setVisibility(View.VISIBLE);
			openOptionsLabelTextView.setVisibility(View.VISIBLE);
			openOptionsTextView.setText(String.valueOf(user.getOpenedInvestmentsCount()));
		} else {
			dotOpenOptions.setVisibility(View.GONE);
			openOptionsTextView.setVisibility(View.GONE);
			openOptionsLabelTextView.setVisibility(View.GONE);
		}*/
	}

	/**
	 * Updates the balance.
	 */
	protected void refreshBalance() {
		if(user != null){
			balanceLabelTextView.setVisibility(View.VISIBLE);
			balanceTextView.setVisibility(View.VISIBLE);
			
			double balanceValue = AmountUtil.getDoubleAmount(user.getBalance());
			String balanceText = user.getBalanceWF();
			
			balanceTextView.setText(balanceText);
			if(balanceValue <= ALERT_BALANCE_VALUE){ 
				balanceTextView.setTextColor(ColorUtils.getColor(R.color.red));
			}else{
				balanceTextView.setTextColor(ColorUtils.getColor(R.color.status_bar_text));
			}
			
		}else{
			balanceLabelTextView.setVisibility(View.INVISIBLE);
			balanceTextView.setVisibility(View.INVISIBLE);
		}
		
		if(balanceInfoImageView != null){
			if(user != null){
				balanceInfoImageView.setVisibility(View.VISIBLE);
				balanceView.setOnClickListener(this);
			}else{
				balanceInfoImageView.setVisibility(View.GONE);
				balanceView.setOnClickListener(null);
			}
		}
	}

	
}

