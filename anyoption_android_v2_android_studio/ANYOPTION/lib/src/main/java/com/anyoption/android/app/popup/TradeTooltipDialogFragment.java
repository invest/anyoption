package com.anyoption.android.app.popup;

import com.anyoption.android.app.Application;
import com.anyoption.android.R;
import com.anyoption.android.app.util.ScreenUtils;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

public class TradeTooltipDialogFragment extends BaseDialogFragment {

	public static TradeTooltipDialogFragment newInstance(Bundle bundle) {
		TradeTooltipDialogFragment fragment = new TradeTooltipDialogFragment();
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, true);
		fragment.setArguments(bundle);
		return fragment;
	}

	private Application application;
	private RelativeLayout rootView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		application = Application.get();
	}

	@Override
	protected void onCreateContentView(View contentView) {
		rootView = (RelativeLayout) contentView;
		setBackgroundColor(0);

		setupUI();

		rootView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}

	@Override
	protected int getContentLayout() {
		return R.layout.trade_tooltip_fragment;
	}

	private void setupUI() {
		//Set up the Asset Info ToolTip: 
		View assetInfoView = rootView.findViewById(R.id.asset_info_arrow_layout);
		RelativeLayout.LayoutParams assetInfoViewParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		assetInfoViewParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		assetInfoViewParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		int assetInfoViewMarginExtra = 5;
		float assetInfoViewMarginLeft = getResources().getDimension(R.dimen.asset_info_flag_width) / 2 + assetInfoViewMarginExtra;
		float assetInfoViewMarginTop = 	getResources().getDimension(R.dimen.status_bar_height) +
										getResources().getDimension(R.dimen.action_bar_height) +
										getResources().getDimension(R.dimen.asset_info_flag_height) +
										assetInfoViewMarginExtra;
		assetInfoViewParams.setMargins((int)assetInfoViewMarginLeft, (int)assetInfoViewMarginTop, 0, 0);
		assetInfoView.setLayoutParams(assetInfoViewParams);				
		/////////////////////////////////////
		
		//Set up the Help Info ToolTip: 
		View helpInfoView = rootView.findViewById(R.id.help_info_arrow_layout);
		RelativeLayout.LayoutParams helpInfoViewParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		helpInfoViewParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		helpInfoViewParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		int helpInfoViewMarginExtra = 10;
		float helpInfoViewMarginRight = ScreenUtils.convertDpToPx(application, 25);
		float helpInfoViewMarginTop = 	getResources().getDimension(R.dimen.status_bar_height) +
										getResources().getDimension(R.dimen.action_bar_height) -
										helpInfoViewMarginExtra;
		helpInfoViewParams.setMargins(0, (int)helpInfoViewMarginTop, (int)helpInfoViewMarginRight, 0);
		helpInfoView.setLayoutParams(helpInfoViewParams);				
		/////////////////////////////////////
		
		//Set up the Trading Info ToolTip:
		View tradingInfoView = rootView.findViewById(R.id.trading_info_arrow_layout);
		if(application.isLoggedIn()){
			RelativeLayout.LayoutParams tradingInfoViewParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			tradingInfoViewParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
			tradingInfoViewParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			float tradingInfoViewBottomMargin = getResources().getDimension(R.dimen.invest_fragment_height) +
												getResources().getDimension(R.dimen.trend_fragment_height);
			tradingInfoViewParams.setMargins(0, 0, 0, (int) tradingInfoViewBottomMargin);
			tradingInfoView.setLayoutParams(tradingInfoViewParams);				
		}else{
			tradingInfoView.setVisibility(View.GONE);
		}
		///////////////////////////////////
	
		//Set up the Invest Info Left ToolTip:
		View investInfoLeftView = rootView.findViewById(R.id.invest_button_left_info_arrow_layout);
		RelativeLayout.LayoutParams investInfoLeftViewParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		investInfoLeftViewParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		investInfoLeftViewParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		float investInfoLeftViewMarginLeft = ScreenUtils.convertDpToPx(application, 5);
		float investInfoLeftViewMarginBottomExtra = - ScreenUtils.convertDpToPx(application, 40);
		float investInfoLeftViewMarginBottom = 	getResources().getDimension(R.dimen.invest_fragment_height) +
												getResources().getDimension(R.dimen.trend_fragment_height) +
												investInfoLeftViewMarginBottomExtra;
		investInfoLeftViewParams.setMargins((int)investInfoLeftViewMarginLeft, 0, 0, (int) investInfoLeftViewMarginBottom);
		investInfoLeftView.setLayoutParams(investInfoLeftViewParams);				
		///////////////////////////////////
		
		//Set up the Invest Info Right ToolTip:
		View investInfoRightView = rootView.findViewById(R.id.invest_button_right_info_arrow_layout);
		RelativeLayout.LayoutParams investInfoRightViewParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		investInfoRightViewParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		investInfoRightViewParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		float investInfoRightViewMarginRight = ScreenUtils.convertDpToPx(application, 5);
		float investInfoRightViewMarginBottomExtra = - ScreenUtils.convertDpToPx(application, 40);
		float investInfoRightViewMarginBottom = 	getResources().getDimension(R.dimen.invest_fragment_height) +
													getResources().getDimension(R.dimen.trend_fragment_height) +
													investInfoRightViewMarginBottomExtra;
		investInfoRightViewParams.setMargins(0, 0, (int)investInfoRightViewMarginRight, (int) investInfoRightViewMarginBottom);
		investInfoRightView.setLayoutParams(investInfoRightViewParams);				
		///////////////////////////////////
	
		//Set up the Invest Info Right ToolTip:
		View changeAmountInfoView = rootView.findViewById(R.id.change_amount_arrow_layout);
		RelativeLayout.LayoutParams changeAmountInfoViewParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		changeAmountInfoViewParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
		changeAmountInfoViewParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		float changeAmountInfoViewMarginBottomExtra = ScreenUtils.convertDpToPx(application, 25);
		float changeAmountInfoViewMarginBottom = 	getResources().getDimension(R.dimen.trend_fragment_height) +
													changeAmountInfoViewMarginBottomExtra;
		changeAmountInfoViewParams.setMargins(0, 0, 0, (int) changeAmountInfoViewMarginBottom);
		changeAmountInfoView.setLayoutParams(changeAmountInfoViewParams);				
		///////////////////////////////////
	}

}
