package com.anyoption.android.app.popup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.User;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Fullscreen Dialog Fragment showing investment receipt.
 *
 * @author Mario Kutlev
 */
public class ExpiredDynamicsOptionDialogFragment extends BaseDialogFragment {

    private static final String TAG = ExpiredDynamicsOptionDialogFragment.class.getSimpleName();

    public static final String ARG_INVESTMENT_KEY = "arg_investment_key";
    protected static final String TIME_FORMAT_PATTERN = "HH:mm";
    private DecimalFormat levelFormat;
    protected User user;
    private boolean isWinning;
    private boolean isExpiredAbove;
    private String marketName;
    private double centralLevel;
    private double expiryLevel;
    private long timestamp;
    private double investedAmount;
    private double returnAmount;
    private Currency currency;
    private int decimalPointDigits;

    /**
     * Use this method to create new instance of the fragment.
     *
     * @return
     */
    public static ExpiredDynamicsOptionDialogFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new ReceiptDialogFragment.");
        ExpiredDynamicsOptionDialogFragment fragment = new ExpiredDynamicsOptionDialogFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    public static ExpiredDynamicsOptionDialogFragment newInstance(Investment investment) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_INVESTMENT_KEY, investment);
        return newInstance(args);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        user     = Application.get().getUser();
        currency = user.getCurrency();

        Bundle args = getArguments();
        if (args != null) {

            isWinning           = args.getBoolean(Constants.EXTRA_IS_IN_THE_MONEY, false);
            isExpiredAbove      = args.getBoolean(Constants.EXTRA_IS_EXPIRED_ABOVE, false);
            marketName          = args.getString(Constants.EXTRA_MARKET_NAME, null);
            centralLevel        = args.getDouble(Constants.EXTRA_CENTRAL_LEVEL, 0);
            expiryLevel         = args.getDouble(Constants.EXTRA_EXPIRY_LEVEL, 0);
            timestamp           = args.getLong(Constants.EXTRA_TIME, 0);
            investedAmount      = args.getDouble(Constants.EXTRA_INVESTED_AMOUNT, 0);
            returnAmount        = args.getDouble(Constants.EXTRA_RETURN_AMOUNT, 0);
            decimalPointDigits  = args.getInt(Constants.EXTRA_DECIMAL_POINTS, 5);

            levelFormat         = Utils.getDecimalFormat(decimalPointDigits);
            levelFormat.applyPattern(Utils.getDecimalFormatPattern(decimalPointDigits));
        } else {
            dismiss();
        }
    }

    /**
     * Set up content.
     *
     * @param contentView
     */
    @Override
    protected void onCreateContentView(View contentView) {
        View okButton = contentView.findViewById(R.id.expiredDynamicsOptionOKButton);
        okButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        //Set up the In / Out of the money ribbon:
        ImageView inTheMoneyImageView = (ImageView) contentView.findViewById(R.id.expiredDynamicsOptionIntheMoneyImageView);
        if (isWinning) {
            inTheMoneyImageView.setImageDrawable(DrawableUtils.getDrawable(R.drawable.expired_ribbon_in_the_money));
        } else {
            inTheMoneyImageView.setImageDrawable(DrawableUtils.getDrawable(R.drawable.expired_ribbon_out_of_the_money));
        }

        //Set up the Above / Bellow icon:
        ImageView aboveBellowImageView = (ImageView) contentView.findViewById(R.id.expiredDynamicsOptionPutCallImageView);
        if (isExpiredAbove) {
            aboveBellowImageView.setImageDrawable(DrawableUtils.getDrawable(R.drawable.circle_with_triangle_up_white));
        } else {
            aboveBellowImageView.setImageDrawable(DrawableUtils.getDrawable(R.drawable.circle_with_triangle_down_white));
        }

        //Set up the Market name:
        TextView marketNameTextView = (TextView) contentView.findViewById(R.id.expiredDynamicsOptionMarketNameTextView);
        marketNameTextView.setText(marketName);

        //Set up the Level_Time lable:
        TextView levelTimeTextView = (TextView) contentView.findViewById(R.id.expiredDynamicsOptionLevelTimeTextView);
        String type = isExpiredAbove ? getString(R.string.aboveUpper) : getString(R.string.belowUpper);
        SimpleDateFormat expiryTimeFormat = new SimpleDateFormat(TIME_FORMAT_PATTERN);
        String expriryTimeFormatted = expiryTimeFormat.format(new Date(timestamp));
        String levelTimeLabel = "(" + type + ") " + levelFormat.format(centralLevel) + " " + getString(R.string.atTimeToday, expriryTimeFormatted);
        levelTimeTextView.setText(levelTimeLabel);

        //Set up the Profit:
        TextView profitTextView = (TextView) contentView.findViewById(R.id.expiredDynamicsOptionProfitTextView);
        if (isWinning) {
            profitTextView.setVisibility(View.VISIBLE);
            String profit = ((isWinning)? "+" : "") + AmountUtil.getFormattedAmount((isWinning) ? returnAmount - investedAmount : 0, currency) + " " + getString(R.string.profit);
            profitTextView.setText(profit);
        } else {
            profitTextView.setVisibility(View.GONE);
        }

        //Set up the Expiry Level:
        TextView expiryLevelTextView = (TextView) contentView.findViewById(R.id.expiredDynamicsOptionLevelTextView);
        expiryLevelTextView.setText(String.valueOf(levelFormat.format(expiryLevel)));

        //Set up the Invested Amount:
        TextView totalOptionsAmountTextView = (TextView) contentView.findViewById(R.id.expiredDynamicsOptionTotalAmountTextView);
        totalOptionsAmountTextView.setText(AmountUtil.getFormattedAmount(investedAmount, currency));

        //Set up the Return Amount:
        TextView returnAmountTextView = (TextView) contentView.findViewById(R.id.expiredDynamicsOptionReturnAmountTextView);
        returnAmountTextView.setText(AmountUtil.getFormattedAmount(returnAmount, currency));
    }

    @Override
    protected int getContentLayout() {
        return R.layout.expired_dynamics_option_dialog;
    }

}
