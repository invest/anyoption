package com.anyoption.android.app.widget;

import com.anyoption.android.R;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.Typefaces;
import com.anyoption.android.app.util.constants.Constants;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class Button extends android.widget.Button {

	private Font font;
	
	public Button(Context context) {
		super(context);
		init(context, null, 0);
	}

	public Button(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}

	public Button(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	/**
	 * Initial setup.
	 * 
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	private void init(Context context, AttributeSet attrs, int defStyle) {
		if (!isInEditMode()) {
			initFont(context, attrs, defStyle);
			// TODO Some other custom configurations.
		}
	}

	/**
	 * Initial Font setup.
	 * 
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	private void initFont(Context context, AttributeSet attrs, int defStyle) {
		// Get custom attributes:
		TypedArray attributesTypedArray = context.obtainStyledAttributes(attrs,R.styleable.Button);
		int fontID = attributesTypedArray.getInt(R.styleable.Button_font, 0);

		Font font = FontsUtils.getFontForFontID(fontID);
		setFont(font);

		attributesTypedArray.recycle();
	}

	public void setFont(Font font) {
		if (font != null) {
			this.font = font;
			Typeface typeface = Typefaces.get(getContext(), Constants.FONTS_DIR + font.getFontName());
			setTypeface(typeface);
		}
	}
	
	public Font getFont() {
		return font;
	}
}
