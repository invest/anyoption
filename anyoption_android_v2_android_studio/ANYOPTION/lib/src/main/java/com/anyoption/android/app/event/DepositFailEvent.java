package com.anyoption.android.app.event;

import com.anyoption.json.results.TransactionMethodResult;

/**
 * Event that occurs when the user fail to deposit.
 * 
 * @author Anastas Arnaudov
 * 
 */
public class DepositFailEvent {

    private TransactionMethodResult result;

    public DepositFailEvent(TransactionMethodResult result){
        this.result = result;
    }

    public TransactionMethodResult getResult() {
        return result;
    }
}
