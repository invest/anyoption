package com.anyoption.android.app.fragment.my_account;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.common.beans.base.User;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class MyAccountFragment extends NavigationalFragment implements OnClickListener {

	private TextView textviewHelloUser;
	
	@Override
	protected Screenable setupScreen() {
		return Screen.MY_ACCOUNT;
	}

	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static MyAccountFragment newInstance(Bundle bundle){
		MyAccountFragment myAccountFragment = new MyAccountFragment();
		myAccountFragment.setArguments(bundle);
		return myAccountFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.my_account_layout, container, false);
		textviewHelloUser = (TextView) rootView.findViewById(R.id.textViewHelloUserName);
		rootView.findViewById(R.id.myAccountButtonPersonaldetails).setOnClickListener(this);
		rootView.findViewById(R.id.myAccountButtonBanking).setOnClickListener(this);
		rootView.findViewById(R.id.myAccountButtonBonuse).setOnClickListener(this);
		rootView.findViewById(R.id.myAccountButtonMyOptions).setOnClickListener(this);
		
		setupDocumentsButton(rootView);
		
		setUpGreeting();
		return rootView;
	}
	
	protected void setupDocumentsButton(View rootView){
		if(RegulationUtils.isQuestionnaireFilled()) {
			rootView.findViewById(R.id.myAccountButtonDocuments).setVisibility(View.VISIBLE);;
			rootView.findViewById(R.id.myAccountButtonDocuments).setOnClickListener(this);
		} else {
			rootView.findViewById(R.id.myAccountButtonDocuments).setVisibility(View.GONE);;
			rootView.findViewById(R.id.myAccountButtonDocuments).setOnClickListener(null);
		}
	}
	
	private void setUpGreeting() {
		User user = application.getUser();
		if (user != null) {
			textviewHelloUser.setText(getString(R.string.myAccountScreenHeader) + " " + user.getFirstName() + " " + user.getLastName());
		} else {
			textviewHelloUser.setText(R.string.notloggedin);
		}
	}

	@Override
	public void onClick(View v) {
		NavigationEvent navigationEvent = null;
		if (v.getId() == R.id.myAccountButtonPersonaldetails) {
			navigationEvent = new NavigationEvent(Screen.PERSONAL_DETAILS_MENU, NavigationType.DEEP);
		} else if (v.getId() == R.id.myAccountButtonBanking) {
			navigationEvent = new NavigationEvent(Screen.BANKING, NavigationType.DEEP);
		} else if (v.getId() == R.id.myAccountButtonBonuse) {
			navigationEvent = new NavigationEvent(Screen.BONUSES, NavigationType.DEEP);
		} else if (v.getId() == R.id.myAccountButtonMyOptions) {
			navigationEvent = new NavigationEvent(Screen.MY_OPTIONS, NavigationType.DEEP);
		} else if (v.getId() == R.id.myAccountButtonDocuments) {
			navigationEvent = new NavigationEvent(Screen.UPLOAD_DOCUMENTS, NavigationType.DEEP);
		}

		if (navigationEvent != null) {
			application.postEvent(navigationEvent);
		}
	}
	
}
