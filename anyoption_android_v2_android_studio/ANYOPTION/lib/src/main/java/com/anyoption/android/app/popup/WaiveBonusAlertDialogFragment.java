package com.anyoption.android.app.popup;

import com.anyoption.android.R;
import com.anyoption.android.app.manager.PopUpManager.OnAlertPopUpButtonPressedListener;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class WaiveBonusAlertDialogFragment extends BaseDialogFragment {

	private static final String TAG = WaiveBonusAlertDialogFragment.class.getSimpleName();

	private OnAlertPopUpButtonPressedListener onButtonPressedListener;

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static WaiveBonusAlertDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new WaiveBonusAlertDialogFragment.");
		WaiveBonusAlertDialogFragment fragment = new WaiveBonusAlertDialogFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	/**
	 * Set up content.
	 * 
	 * @param contentView
	 */
	@Override
	protected void onCreateContentView(View contentView) {
		View okButton = contentView.findViewById(R.id.waive_bonus_alert_button);
		
		OnClickListener clickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				if(onButtonPressedListener != null){
					onButtonPressedListener.onButtonPressed();
				}
			}
		};

		okButton.setOnClickListener(clickListener);
		setCancelable(false);
	}

	
	@Override
	protected int getContentLayout() {
		return R.layout.waive_bonus_alert_popup_layout;
	}

	public OnAlertPopUpButtonPressedListener getOnButtonPressedListener() {
		return onButtonPressedListener;
	}

	public void setOnButtonPressedListener(OnAlertPopUpButtonPressedListener onButtonPressedListener) {
		this.onButtonPressedListener = onButtonPressedListener;
	}

}
