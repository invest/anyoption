package com.anyoption.android.app.fragment.my_account.banking.deposit;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.manager.PopUpManager.OnConfirmPopUpButtonPressedListener;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.RequestButton.InitAnimationListener;
import com.anyoption.android.app.widget.form.Form;
import com.anyoption.android.app.widget.form.FormDatePicker;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.common.beans.base.CreditCard;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.requests.CardMethodRequest;
import com.anyoption.json.requests.CcDepositMethodRequest;
import com.anyoption.json.requests.CreditCardRequest;
import com.anyoption.json.results.CardMethodResult;
import com.anyoption.json.results.TransactionMethodResult;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass. 
 * <p>It manages the "Existing Card" screen and functionality.
 * @author Eyal O
 *
 */
public class ExistingCardFragment extends ReceiptFragment {
	
	private View linearLayoutEditCard;
	private FormEditText depositAmountEditText;
	private FormEditText ccPass;
	private FormDatePicker expiryDatePicker;
	private RequestButton depositButton;
	private View editButton;
	private ImageView logoCc;
	private Form formDeposit;
	private Form formEditCard;
	private CreditCard creditCard;
	private boolean isEditCc;
	protected long transId;
	private TextView deleteCardTextView;

	
	/**
	 * Use this method to obtain new instance of the fragment.
	 * @return
	 */
	public static ExistingCardFragment newInstance(){	
		return ExistingCardFragment.newInstance(new Bundle());
	}

	/**
	 * Use this method to obtain new instance of the fragment.
	 * @return
	 */
	public static ExistingCardFragment newInstance(Bundle bundle){
		ExistingCardFragment existingCardFragment = new ExistingCardFragment();
		existingCardFragment.setArguments(bundle);
		return existingCardFragment;
	}
	
	@Override
	protected Screenable setupScreen() {
		return Screen.EXISTING_CARD;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.existing_card_layout, container, false);
		isEditCc = false;
		
		// Get Credit Card
		Bundle bundle = getArguments();
		creditCard = (CreditCard) bundle.getSerializable(Constants.EXTRA_CREDIT_CARD);
		
		// CC number
		TextView creditCardNum = (TextView) rootView.findViewById(R.id.existing_card_num_textview);
		creditCardNum.setText(Utils.getCCHiddenNumber(String.valueOf(creditCard.getCcNumber())));

		// Currency symbol
        User user = Application.get().getUser();
        TextView currencySymbol = ((TextView)rootView.findViewById(R.id.currency_symbol_view));
        currencySymbol.setText(user.getCurrencySymbol());
        
        // Logo CC by type
        logoCc = (ImageView) rootView.findViewById(R.id.existing_card_logo_cc);
        Drawable imgCard = DrawableUtils.getImageByCCType((int) (creditCard.getTypeIdByBin() == 0 ? creditCard.getTypeId() : creditCard.getTypeIdByBin()));
        if (null != imgCard) {
        	logoCc.setImageDrawable(imgCard);
        }
        
        // CVV
        ccPass = (FormEditText) rootView.findViewById(R.id.editTextCcPass);
        ccPass.getEditText().setTransformationMethod(new PasswordTransformationMethod(){
        	@Override
            public CharSequence getTransformation(CharSequence source, View view) {
                return new PasswordCharSequence(source);
            }

        });
        ccPass.addValidator(ValidatorType.EMPTY, ValidatorType.CC_PASS_MINIMUM_CHARACTERS);
        
        // Display date and CVV only when click on Edit.
        linearLayoutEditCard = rootView.findViewById(R.id.existing_card_edit_card);
        linearLayoutEditCard.setVisibility(View.GONE);
        
        // Date picker
        expiryDatePicker = (FormDatePicker) rootView.findViewById(R.id.existing_card_expiry_date);
        expiryDatePicker.addValidator(ValidatorType.EMPTY, ValidatorType.CC_EXP_DATE);
        expiryDatePicker.setMonthAndYearOnly(true);
        Calendar cal = Calendar.getInstance();
        String expMonth = creditCard.getExpMonth();
        String expYear = creditCard.getExpYear();
        if (!Utils.isParameterEmptyOrNull(expMonth) && !Utils.isParameterEmptyOrNull(expYear)) {
        	cal.set(Calendar.MONTH, (Integer.valueOf(expMonth) - 1));
        	cal.set(Calendar.YEAR, 2000 + Integer.valueOf(expYear));//TODO The year is NOT correctly formated , so we have to add 2000.
        	expiryDatePicker.setInitialDate(cal, true);
        }
        
        // Deposit amount
        depositAmountEditText = (FormEditText) rootView.findViewById(R.id.existing_card_amount_edittext);
		depositAmountEditText.setShouldShowOKImage(false);
        depositAmountEditText.addValidator(ValidatorType.EMPTY, ValidatorType.CURRENCY_AMOUNT);
        setupDefaultDepositAmount();
        
		// Deposit
		depositButton = (RequestButton) rootView.findViewById(R.id.existing_card_deposit_button);
		if (application.getUser() != null && application.getUser().getFirstDepositId() == 0L) {
			depositButton.setText(R.string.existingCardScreenTradeNowButton);
		}

        depositButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(isSubmitButtonEnabled){
					if (isEditCc) {
						if(formEditCard.checkValidity()){
							depositButton.startLoading(new InitAnimationListener(){
								@Override
								public void onAnimationFinished() {
									isSubmitButtonEnabled = false;
									setSubmitButtonEnabled(false);
									formEditCard.submit();
								}	
							});
						}
					} else {
						if(formDeposit.checkValidity()){
							tryToDeposit();
						}
					}					
				}
			}
		});
        
        // Edit card
        editButton = rootView.findViewById(R.id.existing_card_edit);
        editButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				isEditCc = true;
				editButton.setVisibility(View.GONE);
				logoCc.setVisibility(View.VISIBLE);
				linearLayoutEditCard.setVisibility(View.VISIBLE);
				deleteCardTextView.setVisibility(View.VISIBLE);
			}
		});
        
        // Delete card
        deleteCardTextView = (TextView) rootView.findViewById(R.id.existing_card_delete_card);
        TextUtils.underlineText(deleteCardTextView);
        deleteCardTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				application.getPopUpManager().showConfirmPopUp(
						null, 
						getString(R.string.existingCardScreenDeleteCardPopupMessage) + " " + Utils.getCCLastDigits(String.valueOf(creditCard.getCcNumber())) + "?", 
						getString(R.string.existingCardScreenDeleteCardPopupPositive), 
						getString(R.string.existingCardScreenDeleteCardPopupNegative), 
						new OnConfirmPopUpButtonPressedListener() {
							@Override
							public void onPositive() {						
								Log.d(TAG, "Going to delete credit card.  Credit Card: id=" + creditCard.getId());
								CreditCardRequest request = new CreditCardRequest();
								request.setCardId((int) creditCard.getId());
								Application.get().getCommunicationManager().requestService(ExistingCardFragment.this, "deleteCreditCardCallBack", Constants.SERVICE_DELETE_CREDIT_CARD, request, MethodResult.class);
							}
							
							@Override
							public void onNegative() {}
						});
			}
		});
        
        formEditCard = new Form(new Form.OnSubmitListener() {
			@SuppressWarnings("deprecation")
			@Override
			public void onSubmit() {
				if (!Utils.isParameterEmptyOrNull(expiryDatePicker.getValue())) {
					SimpleDateFormat sdf = new SimpleDateFormat(FormDatePicker.DATE_FORMAT_MONTH_YEAR, Application.get().getLocale());
					Date timeCcExpDate = null;
					try {
						timeCcExpDate = sdf.parse(expiryDatePicker.getValue());
						int month = timeCcExpDate.getMonth() + 1;
						creditCard.setExpMonth((month < 10) ? String.valueOf("0" + month) : String.valueOf(month));
						int yearLastTwoDigits = timeCcExpDate.getYear() % 100;
						creditCard.setExpYear(String.valueOf(yearLastTwoDigits));
					} catch (ParseException e) {
						Log.d(TAG, "Error to parse expiry month and year.");
					}
				}
				Log.d(TAG, "Going to update credit card.  Credit Card: id=" + creditCard.getId() + ", expiryMonth=" + creditCard.getExpMonth() + ", expiryYear=" + creditCard.getExpYear());
				CardMethodRequest request = new CardMethodRequest();
 				request.setCard(creditCard);
 				if(!Application.get().getCommunicationManager().requestService(ExistingCardFragment.this, "updateCardCallBack", Constants.SERVICE_UPDATE_CARD, request, CardMethodResult.class, false, false)){
 					isSubmitButtonEnabled = true;
 					setSubmitButtonEnabled(true);
 				}
			}
		});
        formEditCard.addItem(depositAmountEditText, expiryDatePicker, ccPass);
        
        formDeposit = new Form(new Form.OnSubmitListener() {
			@Override
			public void onSubmit() {
				Log.d(TAG, "Going to deposit by credit card.  Credit card: id=" + creditCard.getId() + ", amount=" + depositAmountEditText.getValue());
				CcDepositMethodRequest request = new CcDepositMethodRequest();
				request.setAmount(depositAmountEditText.getValue());
				request.setCardId(creditCard.getId());
				request.setCcPass(ccPass.getValue());
				request.setFirstNewCardChanged(false); //For repeat failure
				if(!Application.get().getCommunicationManager().requestService(ExistingCardFragment.this, "insertDepositCallBack", Constants.SERVICE_INSERT_DEPOSIT_CARD, request, TransactionMethodResult.class, false, false)){
					isSubmitButtonEnabled = true;
					setSubmitButtonEnabled(true);
				}
			}
		});
        formDeposit.addItem(depositAmountEditText, ccPass);
		return rootView;
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	
	protected void tryToDeposit(){
		depositButton.startLoading(new InitAnimationListener(){
			@Override
			public void onAnimationFinished() {
			
				if (RegulationUtils.checkRegulationStatusAndDoNecessary(false)) {
					isSubmitButtonEnabled = false;
					setSubmitButtonEnabled(false);
					formDeposit.submit();
				}
				
			}	
		});
	}
	
	/**
	 * update card callback.
	 * @param resultObj
	 */
	public void updateCardCallBack(Object resultObj){
		Log.d(TAG, "updateCardCallBack");
		CardMethodResult result = (CardMethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {			
			Log.d(TAG, "Update card successfully. " + " Credit card: id=" + creditCard.getId());
			formDeposit.submit();
		} else {
			isSubmitButtonEnabled = true;
			setSubmitButtonEnabled(true);
			String error = result.getUserMessages()[0].getMessage();
			Log.d(TAG, "Error updating card.  Credit card: id=" + creditCard.getId() + ", error=" + error);
			application.getNotificationManager().showNotificationError(error);		
		}		    	
    }
	
	public void deleteCreditCardCallBack(Object resultObj) {
		Log.d(TAG, "deleteCreditCardCallBack");
		MethodResult result = (MethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Card deleted successfully. " + " Credit card: id=" + creditCard.getId());
			application.getNotificationManager().showNotificationSuccess(getString(R.string.existingCardScreenDeleteCardSuccessMessage));
		} else {
			String error = result.getUserMessages()[0].getMessage();
			Log.d(TAG, "Error with delete credit card. " + " Credit card: id=" + creditCard.getId() + ", error=" + error);
			application.getNotificationManager().showNotificationError(getString(R.string.existingCardScreenDeleteCardErrorMessage));
		}
		application.postEvent(new NavigationEvent(Screen.DEPOSIT_MENU, NavigationType.INIT));
	}

	@Override
	public String getAmount() {
		double amount = Double.valueOf(((FormEditText) getView().findViewById(
				R.id.existing_card_amount_edittext)).getValue().toString());
		return AmountUtil.getFormattedAmount(amount, application.getCurrency());
	}

	@Override
	public String getCurrencySymbol() {
		return ((TextView)getView().findViewById(R.id.currency_symbol_view)).getText().toString();
	}
	
	private class PasswordCharSequence implements CharSequence {
        private CharSequence mSource;
        public PasswordCharSequence(CharSequence source) {
            mSource = source; // Store char sequence
        }
        public char charAt(int index) {
            return 'x'; // This is the important part
        }
        public int length() {
            return mSource.length(); // Return default
        }
        public CharSequence subSequence(int start, int end) {
            return mSource.subSequence(start, end); // Return default
        }
    }

	@Override
	public void setSubmitButtonEnabled(boolean isEnabled) {
		isSubmitButtonEnabled = isEnabled;
		if(depositButton != null){
			depositButton.setEnabled(isEnabled);
			if(isEnabled){
				depositButton.stopLoading();
			}
		}
	}
	
	@Override
	protected void setupDefaultDepositAmount() {
		if(application.getUser().getPredefinedDepositAmount() != null){
        	depositAmountEditText.setValue(application.getUser().getPredefinedDepositAmount());
        }
	}
}

