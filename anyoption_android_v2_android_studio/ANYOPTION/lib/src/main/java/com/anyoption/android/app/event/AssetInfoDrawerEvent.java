package com.anyoption.android.app.event;

/**
 * @author Anastas Arnaudov
 */
public class AssetInfoDrawerEvent {

    private boolean isShouldOpen;

    public AssetInfoDrawerEvent(boolean isShouldOpen){
        this.isShouldOpen = isShouldOpen;
    }

    public boolean isShouldOpen() {
        return isShouldOpen;
    }
}
