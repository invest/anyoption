package com.anyoption.android.app.fragment.my_account;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.event.UpdateUserInfoEvent;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.RequestButton.InitAnimationListener;
import com.anyoption.android.app.widget.form.Form;
import com.anyoption.android.app.widget.form.FormCheckBox;
import com.anyoption.android.app.widget.form.FormCountrySelector;
import com.anyoption.android.app.widget.form.FormDatePicker;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.android.app.widget.form.FormPhoneCodeSelector;
import com.anyoption.android.app.widget.form.FormRadioButton;
import com.anyoption.android.app.widget.form.FormSelector;
import com.anyoption.common.beans.base.Country;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.requests.UpdateUserMethodRequest;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

/**
 * {@link BaseFragment} that represents the Update info screen.
 * @author EranL
 */
public class UpdateInfoFragment extends NavigationalFragment {

	public static final String TAG = UpdateInfoFragment.class.getSimpleName();
	
	protected Form form;
	protected FormEditText firstNameField;
	protected FormEditText lastNameField;
	protected FormEditText emailField;
	protected FormEditText zipField;
	protected FormEditText cityField;
	protected FormEditText streetField;
	protected FormEditText houseNumber;
	protected FormEditText userName;
	protected FormCountrySelector countrySelector;
	protected FormPhoneCodeSelector phoneCodeButton;
	protected FormEditText mobilePhoneField;
	protected FormDatePicker birthDateField;
	protected FormRadioButton genderWoman;
	protected FormRadioButton genderMan;
	protected FormCheckBox isContactByMail;
	protected FormCheckBox isContactBySMS;
	protected RequestButton updateUserButton;

	protected User user;

	protected View rootView;
	
	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static UpdateInfoFragment newInstance(){	
		return newInstance(new Bundle());
	}
	
	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static UpdateInfoFragment newInstance(Bundle bundle){
		UpdateInfoFragment updateInfoFragment = new UpdateInfoFragment();	
		updateInfoFragment.setArguments(bundle);
		return updateInfoFragment;
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		user = application.getUser();					
		rootView = inflater.inflate(R.layout.update_info_fragment_layout, container, false);		
		
		form = new Form(new Form.OnSubmitListener() {
			@Override 
			public void onSubmit() { 
				submitUpdateUser();
			} 
		});
		
		setupForm();
			
		//is contact by SMS
		isContactBySMS = (FormCheckBox) rootView.findViewById(R.id.update_formCheckBoxSMS);
		// The logic is reversed because of the tricky question:
		isContactBySMS.setChecked(!user.isContactBySMS());		
		//is contact by mail
		isContactByMail = (FormCheckBox) rootView.findViewById(R.id.update_formCheckBoxEmail);
		// The logic is reversed because of the tricky question:
		isContactByMail.setChecked(user.getIsContactByEmail() == 0 ? true : false);
		
		updateUserButton = (RequestButton) rootView.findViewById(R.id.update_user_button);		
		updateUserButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(form.checkValidity()){
					updateUserButton.startLoading(new InitAnimationListener() {
						@Override
						public void onAnimationFinished() {
							form.submit();
						}
					});									
				}
			}
		});

		return rootView;
	}
	
	protected void setupForm(){
		// first name
		firstNameField = (FormEditText) rootView
				.findViewById(R.id.update_first_name_edittext);
		firstNameField.addValidator(ValidatorType.VACUOUS);
		firstNameField.getEditText().setText(user.getFirstName());
		firstNameField.setEditable(false);
		// last name
		lastNameField = (FormEditText) rootView
				.findViewById(R.id.update_last_name_edittext);
		lastNameField.addValidator(ValidatorType.VACUOUS);
		lastNameField.getEditText().setText(user.getLastName());
		lastNameField.setEditable(false);
		// user name
		userName = (FormEditText) rootView
				.findViewById(R.id.update_username_edittext);
		userName.getEditText().setText(user.getUserName());
		userName.addValidator(ValidatorType.VACUOUS);
		userName.setEditable(false);
		// birth day
		birthDateField = (FormDatePicker) rootView
				.findViewById(R.id.update_formDatePicker);
		if (user.getTimeBirthDate() != null) {
			birthDateField.setEditable(false);
			SimpleDateFormat formatDate = new SimpleDateFormat(
					FormDatePicker.DATE_FORMAT_DAY_MONTH_YEAR,
					application.getLocale());
			birthDateField.setValue(formatDate.format(user.getTimeBirthDate()));
			birthDateField.addValidator(ValidatorType.VACUOUS);
		} else {
			birthDateField.addValidator(ValidatorType.VACUOUS, ValidatorType.BIRTH_DAY);
			birthDateField.setMin18(true);
		}
		// gender
		String genederStr = user.getGender();
		genderMan = (FormRadioButton) rootView
				.findViewById(R.id.update_formRadioButtonMan);
		genderWoman = (FormRadioButton) rootView
				.findViewById(R.id.update_formRadioButtonWoman);
		if (genederStr != null && !genederStr.equals("")) {
			genderMan.setEditable(false);
			genderWoman.setEditable(false);
			if (genederStr.equalsIgnoreCase("M")) {
				genderMan.setChecked(true);
			} else { // Female
				genderWoman.setChecked(true);
			}
		}
		// country
		countrySelector = (FormCountrySelector) rootView
				.findViewById(R.id.update_formCountrySelector);
		countrySelector.setCountryId(user.getCountryId());

		// city
		cityField = (FormEditText) rootView
				.findViewById(R.id.update_city_edittext);
		cityField.getEditText().setText(user.getCityName());
		cityField.addValidator(ValidatorType.EMPTY,
				ValidatorType.LETTERS_AND_SPACE_ONLY);
		// street
		streetField = (FormEditText) rootView
				.findViewById(R.id.update_street_edittext);
		streetField.getEditText().setText(user.getStreet());
		streetField.addValidator(ValidatorType.EMPTY);// ,
														// ValidatorType.ADDRESS);
		// house
		houseNumber = (FormEditText) rootView
				.findViewById(R.id.update_house_edittext);
		houseNumber.getEditText().setText(user.getStreetNo());
		houseNumber.addValidator(ValidatorType.VACUOUS,
				ValidatorType.ADDRESS_NUM);
		// zip
		zipField = (FormEditText) rootView
				.findViewById(R.id.update_zip_edittext);
		zipField.getEditText().setText(user.getZipCode());
		zipField.addValidator(ValidatorType.VACUOUS);
		// phone code
		phoneCodeButton = (FormPhoneCodeSelector) rootView
				.findViewById(R.id.update_phone_code_button);
		phoneCodeButton.setPhoneCode(user.getCountryId());
		phoneCodeButton.setEditable(false);
		countrySelector
				.addItemListener(new FormSelector.ItemSelectedListener<Country>() {
					@Override
					public void onItemSelected(Country selectedItem) {
						phoneCodeButton.setPhoneCode(selectedItem.getId());
					}
				});
		// phone
		mobilePhoneField = (FormEditText) rootView
				.findViewById(R.id.update_phone_edittext);
		mobilePhoneField.addValidator(ValidatorType.EMPTY, ValidatorType.PHONE,
				ValidatorType.PHONE_MINIMUM_CHARACTERS);
		mobilePhoneField.getEditText().setText(user.getMobilePhone());
		// email
		emailField = (FormEditText) rootView
				.findViewById(R.id.update_email_edittext);
		emailField.addValidator(ValidatorType.VACUOUS);
		emailField.getEditText().setText(user.getEmail());
		// Check whether it is an 'OLD' user (which has EMAIL field separate
		// from the USERNAME field)
		if (user.getUserName().equalsIgnoreCase(user.getEmail())) {
			// The User is 'NEW' (The USERNAME is his email):
			emailField.setEditable(false);
		} else {
			// The User is 'OLD' so he has separate email:
			emailField.setEditable(true);
		}

		form.addItem(firstNameField, lastNameField, emailField,
				countrySelector, phoneCodeButton, mobilePhoneField,
				birthDateField, cityField, streetField, houseNumber, zipField);
	
	}
	/**
	 * Submit update user
	 */
	protected void submitUpdateUser() {
		Log.d(TAG, "Going to submit UpdateUser");
		UpdateUserMethodRequest request = new UpdateUserMethodRequest();
		User user = application.getUser();
		setupUpdatedUser(user);
		request.setUser(user);
        if(!Application.get().getCommunicationManager().requestService(UpdateInfoFragment.this, "updateUserCallBack", Constants.SERVICE_UPDATE_USER, request , MethodResult.class)){
        	updateUserButton.stopLoading();
        }
	}
	
	protected void setupUpdatedUser(User user) {
		if (user.getGender() == null || user.getGender().equalsIgnoreCase("")) {
			if (genderMan.isChecked()) {
				user.setGender("M");	
			} else if (genderWoman.isChecked()) {
				user.setGender("F");
			}
		}
		user.setStreet(streetField.getValue());
		user.setCityName(cityField.getValue());
		user.setZipCode(zipField.getValue());
		user.setStreetNo(houseNumber.getValue());
		user.setMobilePhonePrefix(phoneCodeButton.getValue().getPhoneCode());
		user.setMobilePhone(mobilePhoneField.getValue());
		user.setEmail(emailField.getValue());
		// The logic is reversed because of the tricky question:
		user.setIsContactByEmail(isContactByMail.isChecked() ? 0 : 1);
		user.setIsContactBySMS(isContactBySMS.isChecked() ? 0 : 1);
		/////////////////////////////////////////
		user.setCountryId(countrySelector.getSelectedCountryId());
		if (birthDateField.getValue() != null && !birthDateField.getValue().equalsIgnoreCase("")) {			
			SimpleDateFormat sdf = new SimpleDateFormat(FormDatePicker.DATE_FORMAT_DAY_MONTH_YEAR, application.getLocale()); 
			Date timeBirthDate = null;
			try {			
				timeBirthDate = sdf.parse(birthDateField.getValue());
			} catch (ParseException e) {			
				timeBirthDate = null;
			}
			user.setTimeBirthDate(timeBirthDate);
		}
	}

	/**
	 * Call back from the service call
	 * @param resultObj
	 */
	public void updateUserCallBack(Object resultObj) {			
		Log.d(TAG, "Update user call back");
		MethodResult result = (MethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "update user info successfully");
			application.getNotificationManager().showNotificationSuccess(result.getUserMessages()[0].getMessage());
			application.getFragmentManager().onBack();
			application.postEvent(new UpdateUserInfoEvent());
    		return;
		} else {
			Log.d(TAG, "update user info failed");
			ErrorUtils.displayFieldError(form, result);
			updateUserButton.stopLoading();
		}
	}
	
	@Override
	protected Screenable setupScreen() {
		return Screen.UPDATE_INFO;
	}

}
