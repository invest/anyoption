package com.anyoption.android.app.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

/**
 * Created by veselin.hristozov on 27.4.2016 г..
 */
public class LeftHandleSliderDrawer extends SlidingDrawer {

    public LeftHandleSliderDrawer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public LeftHandleSliderDrawer(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);

        final int height = b - t;

        View handle = (View) getHandle();
        int childLeft = 0;
        int childWidth = handle.getWidth();
        int topOffset = 0;
        int bottomOffest = 0;
        int childHeight = handle.getHeight();

        int childTop = this.mExpanded ? height - handle.getMeasuredHeight() : 0;;

        handle.layout(childLeft, childTop , childLeft + childWidth, childTop+childHeight);
    }
}
