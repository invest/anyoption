package com.anyoption.android.app.widget.form;

import com.anyoption.android.app.Application.LayoutDirection;

import android.widget.TextView.OnEditorActionListener;

/**
 * Interface that all Form widget must implement.
 * @author Anastas Arnaudov
 *
 */
public interface FormItem {
	
	/**
	 * Sets the Direction.
	 */
	public void setDirection(LayoutDirection direction);
	
	/**
	 * Sets up the UI according to the direction.
	 */
	public void setupUIForDirection();
	
	/**
	 * Returns the direction.
	 * @return
	 */
	public LayoutDirection getDirection();
	
	/**
	 * Checks whether the item can be edit or not.
	 * @return
	 */
	public boolean isEditable();
	
	/**
	 * Sets whether the item can be edit or not.
	 * @param isEditable
	 */
	public void setEditable(boolean isEditable);
	
	/**
	 * Checks whether the items value is a valid one.
	 * @return
	 */
	public boolean checkValidity();
	
	/**
	 * Returns the value set for this item.
	 * @return
	 */
	public Object getValue();
	
	/**
	 * Sets the value for this item
	 * @param value
	 */
	public void setValue(Object value);
	
	/**
	 * Adds {@link OnEditorActionListener} that listens for Keyboard events.
	 * @param onEditorActionListener
	 */
	public void addOnEditorActionListener(OnEditorActionListener onEditorActionListener);
	
	/**
	 * Clears the set items value.
	 */
	public void clear();
	
	/**
	 * Refreshes the UI.
	 */
	public void refreshUI();
	
	/**
	 * @param fieldName
	 */
	public void setFieldName(String fieldName);
	
	/**
	 * @return fieldname
	 */
	public String getFieldName();
	
	/**
	 * DisplayError
	 * @param error
	 */
	public void displayError(String error);
	
}
