package com.anyoption.android.app.fragment.my_account.banking.withdraw;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.WithdrawEvent;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.manager.PopUpManager.OnAlertPopUpButtonPressedListener;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.popup.LowWithdrawDialogFragment;
import com.anyoption.android.app.popup.LowWithdrawDialogFragment.LowWithdrawListener;
import com.anyoption.android.app.popup.WithdrawSurveyConfirmationDialogFragment;
import com.anyoption.android.app.popup.WithdrawSurveyDialogFragment;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.RequestButton.InitAnimationListener;
import com.anyoption.android.app.widget.form.Form;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.common.beans.base.CreditCard;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.CcDepositMethodRequest;
import com.anyoption.json.results.TransactionMethodResult;

/**
 * A simple {@link Fragment} subclass.
 * <p>It manages the "Existing Card" screen and functionality.
 *
 * @author Eyal O
 */
public class CreditCardWithdrawFragment extends NavigationalFragment implements WithdrawSurveyDialogFragment.OnWithdrawSurveyClosed, BaseDialogFragment.OnDialogDismissListener {


    private FormEditText withdrawAmountEditText;
    private RequestButton withdrawButton;
    private Form form;
    protected CreditCard creditCard;
    @Deprecated
    private boolean clickedWithdrawal;
    private CcDepositMethodRequest request;
    private String fee = "";

    protected boolean isWithdrawSurveySkipped = true;
    protected long withdrawSurveyUserAnswer = 0l;
    protected String withdrawSurveyUserAnswerText = null;

    /**
     * Use this method to obtain new instance of the fragment.
     *
     * @return
     */
    public static CreditCardWithdrawFragment newInstance() {
        return CreditCardWithdrawFragment.newInstance(new Bundle());
    }

    /**
     * Use this method to obtain new instance of the fragment.
     *
     * @return
     */
    public static CreditCardWithdrawFragment newInstance(Bundle bundle) {
        CreditCardWithdrawFragment creditCardWithdrawFragment = new CreditCardWithdrawFragment();
        creditCardWithdrawFragment.setArguments(bundle);
        return creditCardWithdrawFragment;
    }

    @Override
    protected Screenable setupScreen() {
        return Screen.CC_WITHDRAW;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.credit_card_withdraw_layout, container, false);

        // Get Credit Card
        Bundle bundle = getArguments();
        creditCard = (CreditCard) bundle.getSerializable(Constants.EXTRA_CREDIT_CARD);


        // Currency symbol
        User user = Application.get().getUser();
        TextView currencySymbol = ((TextView) rootView.findViewById(R.id.currency_symbol_view));
        currencySymbol.setText(user.getCurrencySymbol());

        TextView ccMaxAmount = ((TextView) rootView.findViewById(R.id.cc_max_withdraw_textview));
        Log.d(TAG, "creditCard.isCftAvailable() " + creditCard.isCftAvailable());
        if (creditCard.getId() != 0 && !creditCard.isCftAvailable()) {
            ccMaxAmount.setVisibility(View.VISIBLE);
            ccMaxAmount.setText(getString(R.string.withdrawals_credit_option_msg, creditCard.getCreditAmountTxt()));
        } else {
            ccMaxAmount.setVisibility(View.GONE);
        }

        // Withdraw amount
        withdrawAmountEditText = (FormEditText) rootView.findViewById(R.id.existing_card_amount_edittext);
        withdrawAmountEditText.addValidator(ValidatorType.EMPTY, ValidatorType.CURRENCY_AMOUNT);
        withdrawAmountEditText.setFieldName(Constants.FIELD_AMOUNT);

        // Deposit
        withdrawButton = (RequestButton) rootView.findViewById(R.id.cc_withdraw_button);
        withdrawButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tryToWithdraw();
            }
        });

        form = new Form(new Form.OnSubmitListener() {
            @Override
            public void onSubmit() {
                showWithdrawSurvey();
            }
        });
        form.addItem(withdrawAmountEditText);
        return rootView;
    }

    protected void showWithdrawSurvey(){
        resetWithdrawSurveyParameters();
        WithdrawSurveyDialogFragment withdrawSurveyDialogFragment = WithdrawSurveyDialogFragment.newInstance(null);
        withdrawSurveyDialogFragment.setOnWithdrawSurveyClosedListener(CreditCardWithdrawFragment.this);
        application.getFragmentManager().showDialogFragment(withdrawSurveyDialogFragment);
    }

    protected void tryToWithdraw() {
        if (RegulationUtils.checkRegulationStatusAndDoNecessary(true)) {
            if (form.checkValidity()) {
                withdrawButton.startLoading(new InitAnimationListener() {
                    @Override
                    public void onAnimationFinished() {
                        form.submit();
                    }
                });
            }
        }
    }

    /**
     * Validation call back from the service call
     *
     * @param resultObj
     */
    public void validateWithdrawCardCallBack(Object resultObj) {
        Log.d(TAG, "validate Withdraw Card CallBack");
        TransactionMethodResult result = (TransactionMethodResult) resultObj;
        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            Log.d(TAG, "Show important note popup");
            // show important note popup
            fee = result.getFee();
                Bundle bundle = new Bundle();
                bundle.putString(Constants.EXTRA_FEE, fee);
                LowWithdrawDialogFragment lowWithdrawDialogFragment = (LowWithdrawDialogFragment) application.getFragmentManager().showDialogFragment(LowWithdrawDialogFragment.class, bundle);
                lowWithdrawDialogFragment.setListener(new LowWithdrawListener() {
                    @Override
                    public void onOK() {
                        if (!Application.get().getCommunicationManager().requestService(CreditCardWithdrawFragment.this, "insertWithdrawCardCallBack", Constants.SERVICE_INSERT_WITHDRAW_CARD, request, TransactionMethodResult.class)) {
                            withdrawButton.stopLoading();
                        }
                    }

                    @Override
                    public void onCancel() {
                        withdrawButton.stopLoading();
                        resetWithdrawSurveyParameters();
                    }
                });
        } else {
            Log.d(TAG, "Display error");
            ErrorUtils.displayFieldError(form, result);
            withdrawButton.stopLoading();
            resetWithdrawSurveyParameters();
        }
    }

    /**
     * Call back from the service call
     *
     * @param resultObj
     */
    public void insertWithdrawCardCallBack(Object resultObj) {
        Log.d(TAG, "insertWithdrawCardCallBack");
        TransactionMethodResult result = (TransactionMethodResult) resultObj;
        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            Log.d(TAG, "Insert withdrawal: " + result.getTransactionId());
            User user = application.getUser();
            //Update the Balance:
            long newBalance = (long) (result.getBalance() * 100);
            user.setBalance(newBalance);
            user.setBalanceWF(AmountUtil.getFormattedAmount(newBalance, user.getCurrency()));
            application.postEvent(new WithdrawEvent());

            //Show important note POPUP:
                String msg = getString(R.string.withdraw_cc_success, result.getFormattedAmount());
                if (fee.equals(Constants.NO_FEE)) {
                    msg = getString(R.string.rewards_withdraw_cc_success, result.getFormattedAmount());
                }
                application.getPopUpManager().showAlertPopUp(getString(R.string.banking_withdrawal_note_header), msg, new OnAlertPopUpButtonPressedListener() {
                    @Override
                    public void onButtonPressed() {
                        application.getFragmentManager().onBack();
                    }
                });

        } else {
            Log.d(TAG, "Insert bank withdraw failed");
            ErrorUtils.displayFieldError(form, result);
            withdrawButton.stopLoading();
            resetWithdrawSurveyParameters();
        }
    }

    protected void callValidateWithdrawService() {
        request = new CcDepositMethodRequest();
        request.setAmount(withdrawAmountEditText.getValue());
        request.setCardId(creditCard.getId());

        if (isWithdrawSurveySkipped) {
            resetWithdrawSurveyParameters();
        }

        request.setUserAnswerId(withdrawSurveyUserAnswer);
        request.setTextAnswer(withdrawSurveyUserAnswerText);

        if (!Application.get().getCommunicationManager().requestService(CreditCardWithdrawFragment.this, "validateWithdrawCardCallBack", Constants.SERVICE_VALIDATE_WITHDRAW_CARD, request, TransactionMethodResult.class)) {
            withdrawButton.stopLoading();
        }
        resetWithdrawSurveyParameters();
    }

    @Override
    public void onWithdrawSurveyClosed(boolean isSkipped, long chosenAnswer, String answerText) {
        isWithdrawSurveySkipped = isSkipped;
        withdrawSurveyUserAnswer = chosenAnswer;
        withdrawSurveyUserAnswerText = answerText;

        if (!isSkipped) {
            WithdrawSurveyConfirmationDialogFragment withdrawSurveyConfirmationDialogFragment = WithdrawSurveyConfirmationDialogFragment.newInstance(null);
            withdrawSurveyConfirmationDialogFragment.setOnDismissListener(CreditCardWithdrawFragment.this);
            application.getFragmentManager().showDialogFragment(withdrawSurveyConfirmationDialogFragment);
        } else {
            callValidateWithdrawService();
        }

    }

    @Override
    public void onDismiss() {
        Log.d(TAG, "Going to withdraw by credit card.  Credit card: id=" + creditCard.getId() + ", amount=" + withdrawAmountEditText.getValue());
        callValidateWithdrawService();
    }

    protected void resetWithdrawSurveyParameters() {
        isWithdrawSurveySkipped = true;
        withdrawSurveyUserAnswer = 0l;
        withdrawSurveyUserAnswerText = null;
    }
}

