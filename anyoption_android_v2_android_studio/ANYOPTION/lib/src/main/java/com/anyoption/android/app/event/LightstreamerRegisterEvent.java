package com.anyoption.android.app.event;

public class LightstreamerRegisterEvent {

	public enum Type{
		SUBSCRIBE,
		UNSUBSCRIBE
	}

	public LightstreamerRegisterEvent(Type type){
		this.type = type;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	private Type type;

}
