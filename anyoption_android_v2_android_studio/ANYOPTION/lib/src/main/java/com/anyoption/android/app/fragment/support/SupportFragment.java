package com.anyoption.android.app.fragment.support;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.manager.PopUpManager.OnConfirmPopUpButtonPressedListener;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.EditText;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.RequestButton.InitAnimationListener;
import com.anyoption.android.app.widget.SupportIssuesSelector;
import com.anyoption.android.app.widget.TextView;
import com.anyoption.android.app.widget.form.Form;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.android.app.widget.form.FormPhoneCodeSelector;
import com.anyoption.beans.base.Contact;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.json.requests.ContactMethodRequest;
import com.anyoption.json.results.ContactMethodResult;
import com.anyoption.json.results.CountryIdMethodResult;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class SupportFragment extends NavigationalFragment {

	private User user;
	protected Form form;
	protected FormEditText firstNameFormEditText;
	protected FormEditText lastNameFormEditText;
	protected FormEditText emailFormEditText;
	protected FormPhoneCodeSelector phoneCodeButton;
	protected FormEditText phoneField;
	private SupportIssuesSelector issuesSelector;
	private EditText emailContentEditText;
	protected long countryId;

	private RequestButton sendButton;
	protected View rootView;
	
	/**
	 * Use this method to get new instance of the fragment.
	 * 
	 * @return
	 */
	public static SupportFragment newInstance(Bundle bundle) {
		SupportFragment supportFragment = new SupportFragment();
		supportFragment.setArguments(bundle);
		return supportFragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.support_layout, container, false);

		init();
		
		form = new Form(new Form.OnSubmitListener() {
			@Override
			public void onSubmit() {
				insertContact();
			}
		});
		if (application.getUser() != null) {
			LinearLayout notLoggedLayout = (LinearLayout) rootView.findViewById(R.id.support_not_logged_user_layout);
			notLoggedLayout.setVisibility(View.GONE);
			user = application.getUser();
		} else {
			firstNameFormEditText = (FormEditText) rootView.findViewById(R.id.support_first_name);
			firstNameFormEditText.setFieldName(Constants.FIELD_FIRST_NAME);
			lastNameFormEditText = (FormEditText) rootView.findViewById(R.id.support_last_name);
			lastNameFormEditText.setFieldName(Constants.FIELD_LAST_NAME);
			emailFormEditText = (FormEditText) rootView.findViewById(R.id.support_email);
			emailFormEditText.setFieldName(Constants.FIELD_EMAIL);
			setupPhonePrefixSelector();
			phoneField = (FormEditText) rootView.findViewById(R.id.support_phone_edittext);
			phoneField.setFieldName(Constants.FIELD_PHONE);
			setupUserFieldsValidators();
			form.addItem(firstNameFormEditText, lastNameFormEditText, emailFormEditText, phoneField);
		}
		
		setupCallFields();
		
		emailContentEditText = (EditText) rootView.findViewById(R.id.support_email_text);
		emailContentEditText.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					application.setCurrentEditTextOnFocus(emailContentEditText);
				}
			}
		});
		
		issuesSelector = (SupportIssuesSelector) rootView.findViewById(R.id.support_issues_selector);
		sendButton = (RequestButton) rootView.findViewById(R.id.contact_us_send_button);
		sendButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(form.checkValidity()){
					sendButton.startLoading(new InitAnimationListener() {
						@Override
						public void onAnimationFinished() {
							form.submit();
						}
					});									
				}
			}
		});
		
		return rootView;
	}

	protected void init(){
		Application.get().getCommunicationManager().requestService(SupportFragment.this, "getCountryIdCallBack", Constants.SERVICE_GET_COUNTRY_ID, new MethodRequest(),CountryIdMethodResult.class);
	}
	
	protected void setupPhonePrefixSelector(){
		phoneCodeButton = (FormPhoneCodeSelector) rootView.findViewById(R.id.support_phone_code_button);
	}
	
	@Override
	protected Screenable setupScreen() {
		return Screen.SUPPORT;
	}
	
	protected void setupUserFieldsValidators() {
		firstNameFormEditText.addValidator(ValidatorType.EMPTY, ValidatorType.LETTERS_ENGLISH_ONLY);
		lastNameFormEditText.addValidator(ValidatorType.EMPTY, ValidatorType.LETTERS_ENGLISH_ONLY);
		emailFormEditText.addValidator(ValidatorType.EMPTY, ValidatorType.EMAIL);
		phoneField.addValidator(ValidatorType.EMPTY, ValidatorType.PHONE, ValidatorType.PHONE_MINIMUM_CHARACTERS);
	}
	
	protected void setupCallFields() {
		View callUsButton = rootView.findViewById(R.id.call_us_button);
		callUsButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final String supportPhone;
				if (user != null) {
					if(application.getCountriesMap().containsKey(user.getCountryId())) {
						supportPhone = application.getCountriesMap().get(user.getCountryId()).getSupportPhone();
					} else {
						supportPhone = getString(R.string.supportScreenDefaultPhone);
					}
				} else {
					supportPhone = getString(R.string.supportScreenDefaultPhone);
				}
				
				application.getPopUpManager().showConfirmPopUp(getResources().getString(R.string.supportScreenCallUsPopUpTitle), supportPhone,
																getResources().getString(R.string.supportScreenCallUsPopUpPositiveButton),
																getResources().getString(R.string.supportScreenCallUsPopUpNegativeButton),
																new OnConfirmPopUpButtonPressedListener() {

																	@Override
																	public void onPositive() {
																		application.getTelephonyManager().callService(supportPhone);
																	}

																	@Override
																	public void onNegative() {
																		// Just close the button.
																	}
																});
			}
		});

		TextView workHours = (TextView) rootView.findViewById(R.id.support_work_hours);
		Skin skin = application.getSkins().get(application.getSkinId());
		workHours.setText(Html.fromHtml(getResources().getString(R.string.supportScreenWorkHours,
				skin.getSupportStartTime(), skin.getSupportEndTime(), "", "")));
	}

	public void getCountryIdCallBack(Object result) {
		Log.d(TAG, "getCountryIdCallBack");
		CountryIdMethodResult countryIdMethodResult = (CountryIdMethodResult) result;
		if(countryIdMethodResult != null && countryIdMethodResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS){
			countryId = countryIdMethodResult.getCountryId();
			if(phoneCodeButton != null){
				phoneCodeButton.setPhoneCode(countryId);							
			}
		}
	}

	public void callMeCallBack(Object result) {
		Log.d(TAG, "callMeCallBack");
		sendButton.stopLoading();
		ContactMethodResult contactResult = (ContactMethodResult) result;
		if (contactResult != null && contactResult.getErrorCode() != Constants.ERROR_CODE_SUCCESS) {
			if (contactResult.getContact() != null) { // contact null means no contact inserted in db
				String newmId = contactResult.getContact().getmId();
				Utils.setmIdIfValid(newmId);
				String newEtsMId = contactResult.getContact().getEtsMId();
				Utils.setEtsMId(application, newEtsMId);
			}
			ErrorUtils.displayFieldError(form, contactResult);
		} else {
			Log.d(TAG, "Contanct Name: " + contactResult.getContact().getName() + " Inserted");
			application.getNotificationManager().showNotificationSuccess(getResources().getString(R.string.supportScreenSendEmailSuccessMessage));
			clearForm();
		}
	}

	protected void insertContact() {
		String emailContent = emailContentEditText.getText().toString();
		if (Utils.isParameterEmptyOrNull(emailContent.trim())) {
			sendButton.stopLoading();
			return;
		}
		String selectedIssueType = issuesSelector.getSelectedIssueType();
		ContactMethodRequest request = new ContactMethodRequest();
		Contact contact = new Contact();
		contact.setType(Contact.CONTACT_US_TYPE);
		if (user != null) {
			contact.setCountryId(user.getCountryId());
			contact.setName(user.getFirstName() + user.getLastName());
			contact.setEmail(user.getEmail());
			contact.setPhone(user.getMobilePhone());
		} else {
			contact.setName(firstNameFormEditText.getValue() + " " + lastNameFormEditText.getValue());
			contact.setEmail(emailFormEditText.getValue());
			contact.setCountryId(countryId);
			setContactPhone(contact);
		}

		contact.setCombId(Utils.getCombinationId());
		contact.setDynamicParameter(Utils.getDynamicParam());
		contact.setAff_sub1(Utils.getAffSub1(getActivity()));
		contact.setAff_sub2(Utils.getAffSub2(getActivity()));
		contact.setAff_sub3(Utils.getGclid(getActivity()));
		contact.setHttpReferer(Utils.getReferrerParam(application));
		contact.setmId(Utils.getmId(application));
		contact.setEtsMId(Utils.getEtsMId(application));

		request.setContact(contact);
		request.setComments(emailContent);
		request.setIssue(selectedIssueType);
		if(!Application.get().getCommunicationManager().requestService(SupportFragment.this, "callMeCallBack", Constants.SERVICE_INSERT_CONTACT, request,ContactMethodResult.class)){
			sendButton.stopLoading();
		}
	}

	protected void setContactPhone(Contact contact){
		contact.setPhone(phoneCodeButton.getPhoneCode() + phoneField.getValue());
	}
	
	private void clearForm() {
		form.clear();
		emailContentEditText.setText("");
	}
}