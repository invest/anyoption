package com.anyoption.android.app.event;

public class LongTermTradingTimeChosenEvent {

    private long marketId;
    private ChosenTime time;
    private long opportunityId;

    public enum ChosenTime {
        NEAREST, HOURLY_1, HOURLY_2, HOURLY_3, DAILY, WEEKLY, MONTHLY
    }

    public LongTermTradingTimeChosenEvent(long marketId, ChosenTime time, long opportunityId) {
        this.marketId = marketId;
        this.time = time;
        this.opportunityId = opportunityId;
    }

    public ChosenTime getTime() {
        return time;
    }

    public void setTime(ChosenTime time) {
        this.time = time;
    }

    public long getOpportunityId() {
        return opportunityId;
    }

    public void setOpportunityId(long opportunityId) {
        this.opportunityId = opportunityId;
    }

    public long getMarketId(){
        return this.marketId;
    }
}
