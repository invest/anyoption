package com.anyoption.android.app.widget;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.KeyboardVisibilityChangeEvent;
import com.anyoption.android.app.util.ScreenUtils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * Custom View that is used to enlarge its parent layout when the keyboard appears. This is done
 * because in FullScreen theme when the keyboard appears the layout is not adjustResized.
 * <p>
 * Put this View in bottom of layouts that will get partially hidden by the keyboard when it
 * appears.
 * 
 * @author Anastas Arnaudov
 * 
 */
public class KeyboardOffsetView extends View {

	public KeyboardOffsetView(Context context) {
		super(context);
	}

	public KeyboardOffsetView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public KeyboardOffsetView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		setVisibility(View.GONE);
		if (!isInEditMode()) {
			getLayoutParams().height = (int) ScreenUtils.convertDpToPx(getContext(), 300);
		}
	}

	/**
	 * Called when a {@link KeyboardVisibilityChangeEvent} occurs.
	 * 
	 * @param event
	 */
	public void onEventMainThread(KeyboardVisibilityChangeEvent event) {
		if (event.isVisible()) {
			setVisibility(View.VISIBLE);
		} else {
			setVisibility(View.GONE);
		}

		invalidate();
	}

	@Override
	protected void onAttachedToWindow() {
		if (!isInEditMode()) {
			super.onAttachedToWindow();
			Application.get().registerForEvents(this, KeyboardVisibilityChangeEvent.class);
		}
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		Application.get().unregisterForAllEvents(this);
	}

}
