package com.anyoption.android.app.event;


/**
 * Event that occurs when there are changes in user's insurances.
 * 
 * @author Mario Kutlev
 */
public class InsuranceEvent {

	/**
	 * Insurance event type
	 */
	public enum Type {
		ADD, DELETE, UPDATE
	}
	
	/**
	 * The type of the insurance.
	 */
	public enum InsuranceType {
		TAKE_PROFIT, ROLL_FORWARD
	};

	private InsuranceType insuranceType;
	private Type type;

	/**
	 * {@link InsuranceEvent}
	 * 
	 * @param insuranceType the type of the insurance
	 * @param type event's type
	 */
	public InsuranceEvent(InsuranceType insuranceType, Type type) {
		this.insuranceType = insuranceType;
		this.type = type;
	}

	
	public InsuranceType getInsuranceType() {
		return insuranceType;
	}

	
	public void setInsuranceType(InsuranceType insuranceType) {
		this.insuranceType = insuranceType;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

}
