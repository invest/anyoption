package com.anyoption.android.app.manager;

import android.annotation.SuppressLint;
import android.util.Log;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.ExpiredOptionsEvent;
import com.anyoption.android.app.event.InsuranceEvent;
import com.anyoption.android.app.event.InsuranceEvent.InsuranceType;
import com.anyoption.android.app.event.InsuranceEvent.Type;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.UserUpdatedEvent;
import com.anyoption.android.app.lightstreamer.AOTPSTableListener;
import com.anyoption.android.app.lightstreamer.LightstreamerConnectionHandler;
import com.anyoption.android.app.lightstreamer.LightstreamerListener;
import com.anyoption.android.app.model.InsuranceItem;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.enums.SkinGroup;
import com.lightstreamer.ls_client.ConnectionInfo;
import com.lightstreamer.ls_client.ExtendedTableInfo;
import com.lightstreamer.ls_client.SimpleTableInfo;
import com.lightstreamer.ls_client.UpdateInfo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

/**
 *Manager for the LIGHTSTREAMER.
 */
public class LightstreamerManager implements LightstreamerConnectionHandler.StatusListener, LightstreamerListener {

	public static final String TAG = LightstreamerManager.class.getSimpleName();

	public static final String LS_ADAPTER 		= "REUTERSJMS";
	public static final String LS_DATA_ADAPTER 	= "JMS_ADAPTER";

	private static final String INSURANCE_TIME_FORMAT = "HH:mm";

	//#####################################################################
	//#########						MAX FREQUENCIES				###########
	//#####################################################################

	public static final double MAX_FREQUENCIES_INSURANCE 			= 1.1;
	public static final double MAX_FREQUENCIES_SHOPPING_BAG 		= 1.1;
	public static final double MAX_FREQUENCIES_LIVE_INVESTMENTS 	= 1.1;
	public static final double MAX_FREQUENCIES_ASSETS_LIST		 	= 1.1;
	public static final double MAX_FREQUENCIES_OPEN_OPTIONS		 	= 1.1;
	public static final double MAX_FREQUENCIES_ASSET_PAGE		 	= 1.1;
	public static final double MAX_FREQUENCIES_CHART 				= 1.1;
	//#####################################################################

	//#####################################################################
	//#########					COOMAND TYPES 					###########
	//#####################################################################

	public static final String COMMAND_ADD 			= "ADD";
	public static final String COMMAND_UPDATE 		= "UPDATE";
	public static final String COMMAND_DELETE 		= "DELETE";

	//#####################################################################

	//#################################################################################
	//#########								MESSAGES						###########
	//#################################################################################

	public static final String MESSAGE_VIEWED_INVESTMENT_ID 		= "viewedInvId";
	public static final String MESSAGE_BOUGHT_INVESTMENT_ID 		= "invId";

	//#################################################################################

	//#####################################################################
	//#########					TABLES							###########
	//#####################################################################

	public static final String TABLE_INSURANCES 			= "insurances";

	/**
	 * With suffix <b> _username </b> at the end.
	 */
	public static final String ITEM_INSURANCES 				= "insurances";
	// ********************************************************************
	// ********* 				COLUMNS 						**********
	// ********************************************************************
	public static final String COLUMN_INS_KEY				= "key";
	public static final String COLUMN_INS_COMMAND			= "command";

	public static final String COLUMN_INS_MARKET 			= "INS_MARKET";
	public static final String COLUMN_INS_LEVEL 			= "INS_LEVEL";
	public static final String COLUMN_INS_AMOUNT 			= "INS_AMOUNT";
	public static final String COLUMN_INS_WIN 				= "INS_WIN";
	public static final String COLUMN_INS_INS 				= "INS_INS";
	public static final String COLUMN_INS_TIME_CREATED 		= "INS_TIME_CREATED";
	public static final String COLUMN_INS_TYPE 				= "INS_TYPE";
	public static final String COLUMN_INS_POSITION 			= "INS_POSITION";
	public static final String COLUMN_INS_INS_TYPE 			= "INS_INS_TYPE";
	public static final String COLUMN_INS_LEVEL_CLR 		= "INS_LEVEL_CLR";
	public static final String COLUMN_INS_CLOSE_TIME 		= "INS_CLOSE_TIME";
	public static final String COLUMN_INS_MARKET_NAME 		= "INS_MARKET_NAME";
	public static final String COLUMN_INS_PTYPE 			= "INS_PTYPE";
	public static final String COLUMN_INS_TIME_EST_CLOSE 	= "INS_TIME_EST_CLOSE";

	public static final String PTYPE_STANDARD 				= "STANDARD";
	public static final String PTYPE_ADDITIONAL 			= "ADDITIONAL";

	// ********************************************************************

	public static final String TABLE_SHOPPING_BAG 				= "shoppingbag";
	/**
	 * With suffix <b> _username_userID </b> at the end.
	 */
	public static final String ITEM_SHOPPING_BAG 				= "shoppingbag";
	//********************************************************************
	//*********					COLUMNS							**********
	//********************************************************************
	/**
	 * Opportunity ID.
	 */
	public static final String COLUMN_SB_KEY					= "key";
	/**
	 * ADD, UPDATE or DELETE
	 */
	public static final String COLUMN_SB_COMMAND				= "command";
	/**
	 * Market ID.
	 */
	public static final String COLUMN_SB_MARKET_ID 				= "SB_MARKET_ID";
	/**
	 * Opportunity type.
	 */
	public static final String COLUMN_SB_OPP_TYPE 				= "SB_OPP_TYPE";
	/**
	 * Formated closing level.
	 */
	public static final String COLUMN_SB_LEVEL 					= "SB_LEVEL";
	/**
	 * Return amount in cents.
	 */
	public static final String COLUMN_SB_AMOUNT 				= "SB_AMOUNT";
	/**
	 * Amount returned to balance for each market (opportunity).
	 */
	public static final String COLUMN_SB_MARKET_RETURN_AMOUNT 	= "SB_MARKET_RETURN_AMOUNT";
	/**
	 * Estimated closing time in format : MM/dd/yyyy HH:mm
	 */
	public static final String COLUMN_SB_TIME_CLOSE 			= "SB_TIME_CLOSE";
	/**
	 * 0 - remain open in mobile, 1 - close in mobile
	 */
	public static final String COLUMN_SB_CLOSE_MOBILE			= "SB_CLOSE_MOBILE";
	/**
	 * Investments count for each opportunity.
	 */
	public static final String COLUMN_SB_INV_COUNT				= "SB_INV_COUNT";
	/**
	 * 1 - the money returned to user's balance are more than the invested money for the current market (opportunity);
	 * <p>0 - the user lost.
	 */
	public static final String COLUMN_SB_MARKET_WINNING			= "SB_MARKET_WINNING";


	//********************************************************************

	public static final String TABLE_LIVE_INVESTMENTS 				= "live_investmentsall";
	public static final String ITEM_LIVE_INVESTMENTS 				= "live_investmentsall";
	//********************************************************************
	//*********					COLUMNS							**********
	//********************************************************************
	public static final String COLUMN_LI_KEY					= "key";
	public static final String COLUMN_LI_COMMAND				= "command";

	public static final String COLUMN_LI_CITY 					= "LI_CITY";
	public static final String COLUMN_LI_COUNTRY 				= "LI_COUNTRY";
	public static final String COLUMN_LI_ASSET 					= "LI_ASSET";
	public static final String COLUMN_LI_PROD_TYPE 				= "LI_PROD_TYPE";
	public static final String COLUMN_LI_INV_TYPE 				= "LI_INV_TYPE";
	public static final String COLUMN_LI_LEVEL 					= "LI_LEVEL";
	public static final String COLUMN_LI_AMOUNT 				= "LI_AMOUNT";
	public static final String COLUMN_LI_TIME 					= "LI_TIME";
	public static final String COLUMN_LI_INV_KEY 				= "LI_INV_KEY";
	public static final String COLUMN_LI_OPP_ID 				= "LI_OPP_ID";
	public static final String COLUMN_LI_CONVERTED_AMOUNT 		= "LI_CONVERTED_AMOUNT";
	//********************************************************************

	public static final String TABLE_ASSETS_LIST 				= "assets_list";
	public static final String TABLE_OPEN_OPTIONS 				= "open_options";
	/**
	 * TABLE_ASSET_PAGE + marketId must be table's name.
	 */
	public static final String TABLE_ASSET_PAGE			 		= "asset_page_";
	public static final String TABLE_SIMPLE_CHART		 		= "chart";

	//********************************************************************
	//*********					COLUMNS							**********
	//********************************************************************
	public static final String COLUMN_ET_KEY 					= "key";
	public static final String COLUMN_ET_COMMAND 				= "command";

	public static final String COLUMN_ET_NAME 					= "ET_NAME";
	public static final String COLUMN_ET_ODDS 					= "ET_ODDS";
	public static final String COLUMN_ET_EST_CLOSE 				= "ET_EST_CLOSE";
	public static final String COLUMN_ET_OPP_ID 				= "ET_OPP_ID";
	public static final String COLUMN_ET_ODDS_GROUP 			= "ET_ODDS_GROUP";
	public static final String COLUMN_ET_ODDS_WIN 				= "ET_ODDS_WIN";
	public static final String COLUMN_ET_ODDS_LOSE 				= "ET_ODDS_LOSE";
	public static final String COLUMN_ET_STATE 					= "ET_STATE";
	public static final String COLUMN_ET_PRIORITY 				= "ET_PRIORITY";
	public static final String COLUMN_ET_GROUP_CLOSE 			= "ET_GROUP_CLOSE";
	public static final String COLUMN_ET_SCHEDULED 				= "ET_SCHEDULED";
	public static final String COLUMN_ET_RND_FLOOR 				= "ET_RND_FLOOR";
	public static final String COLUMN_ET_RND_CEILING 			= "ET_RND_CEILING";
	public static final String COLUMN_ET_GROUP 					= "ET_GROUP";
	public static final String COLUMN_ET_SUSPENDED_MESSAGE 		= "ET_SUSPENDED_MESSAGE";
	public static final String COLUMN_ET_LAST_INV 				= "ET_LAST_INV";
	/**
	 * With suffix <b> _skinID </b> at the end.
	 */
	public static final String COLUMN_AO_HP_MARKETS_ 			= "AO_HP_MARKETS";
	public static final String COLUMN_AO_STATES 				= "AO_STATES";
	public static final String COLUMN_AO_TS 					= "AO_TS";
	public static final String COLUMN_AO_FLAGS 					= "AO_FLAGS";
	public static final String COLUMN_AO_CALLS_TREND 			= "AO_CALLS_TREND";
	public static final String COLUMN_ET_CALLS_TREND 			= "ET_CALLS_TREND";
	public static final String COLUMN_FILTER_SCHEDULED 			= "FILTER_SCHEDULED";
	public static final String COLUMN_FILTER_NEW_SCHEDULED		= "FILTER_NEW_SCHEDULED";
	public static final String COLUMN_ET_NH 					= "ET_NH";
	public static final String COLUMN_AO_NH 					= "AO_NH";
	public static final String COLUMN_AO_OPP_STATE 				= "AO_OPP_STATE";

	public static final String COLUMN_DYNAMICS_ITEM_PREFIX		= "bz";

	public static final String COLUMN_DYNAMICS_PROFIT_CALL		= "BZ_OFFER";
	public static final String COLUMN_DYNAMICS_PROFIT_PUT		= "BZ_BID";
	public static final String COLUMN_DYNAMICS_STATE			= "BZ_STATE";
	public static final String COLUMN_DYNAMICS_LEVEL			= "BZ_CURRENT";
	public static final String COLUMN_DYNAMICS_SUSPEND_MSG		= "BZ_SUSPENDED_MESSAGE";
	public static final String COLUMN_DYNAMICS_EVENT_LEVEL		= "BZ_EVENT";
	public static final String COLUMN_DYNAMICS_TIME_LAST_INV	= "BZ_LAST_INV";
	public static final String COLUMN_DYNAMICS_TIME_CLOSE		= "BZ_EST_CLOSE";


	//********************************************************************

	public static final int OPPORTUNITY_SCHEDULED_HOURLY	= 1;
	public static final int OPPORTUNITY_SCHEDULED_DAILY		= 2;
	public static final int OPPORTUNITY_SCHEDULED_WEEKLY	= 3;
	public static final int OPPORTUNITY_SCHEDULED_MONTHLY	= 4;

	public static final String OPPORTUNITY_SUSPEND_STATE_TRUE	= "1";
	public static final String OPPORTUNITY_SUSPEND_STATE_FALSE	= "0";
	//#####################################################################



	protected Application application;
	protected User user;
	protected SkinGroup skinGroup;

	protected LightstreamerConnectionHandler ls;

	protected Map<String, ExtendedTableInfo> tablesMap;

	protected ArrayList<ArrayList<InsuranceItem>> takeProfitGroups;
	protected ArrayList<ArrayList<InsuranceItem>> rollForwardGroups;

	protected SimpleDateFormat insuranceTimeFormat;

	/**
	 * {@link FragmentManager}
	 *
	 * @param application
	 */
	@SuppressLint("SimpleDateFormat")
	public LightstreamerManager(Application application) {
		Log.d(TAG, "On Create");
		this.application = application;
		tablesMap = new Hashtable<String, ExtendedTableInfo>();
		insuranceTimeFormat = new SimpleDateFormat(INSURANCE_TIME_FORMAT);
		application.registerForEvents(this, LoginLogoutEvent.class);
	}

	public LightstreamerConnectionHandler getLs() {
		return ls;
	}

	/**
	 * Starts the LIGHSTREAMER.
	 */
	public void startLightstreamer() {
		// Stop it if it's already started.
		stopLightstreamer();

		user = application.getUser();
		skinGroup = application.getSkins().get(application.getSkinId()).getSkinGroup();
		ls = getLightstreamerConnectionHandler();
		ls.start();
	}

	/**
	 * Stops the LIGHSTREAMER.
	 */
	public void stopLightstreamer() {
		if (ls != null) {
			ls.stop();
			ls = null;
			tablesMap.clear();
		}
	}

	/**
	 * Called when a {@link LoginLogoutEvent} occurs.
	 *
	 * @param event
	 */
	public void onEventMainThread(LoginLogoutEvent event) {
		Log.d(TAG, "onEventMainThread : login event");

		user = application.getUser();
		stopLightstreamer();

		if (event.getType() == LoginLogoutEvent.Type.LOGIN || event.getType() == LoginLogoutEvent.Type.REGISTER) {
			subscribeInsurancesTable(this);
			subscribeShoppingBagTable(this);
		} else if (event.getType() == LoginLogoutEvent.Type.LOGOUT) {
			clearInsurances();
		}
	}

	private LightstreamerConnectionHandler getLightstreamerConnectionHandler() {
		ConnectionInfo cInfo = new ConnectionInfo();
		cInfo.pushServerUrl = Application.get().getLSURL();
		cInfo.adapter = LS_ADAPTER;
		cInfo.isPolling = true;
		cInfo.enableStreamSense = false;
		cInfo.reconnectionTimeoutMillis = 500L;
		///////////////////////

		if (user != null) {
			// Leading and trailing "a"s are added because if the name starts with digits
			// Lightstreamer is dropping them
			cInfo.user = "a" + user.getUserName() + "a";
			cInfo.password = user.getHashedPassword();
		}


		return new LightstreamerConnectionHandler(cInfo, this);
	}

	@Override
	public void onStatusChange(int status) {
		// Do nothing
	}

	/**
	 * Subscribe the Insurance Table to the LS.
	 * @return
	 */
	public boolean subscribeInsurancesTable(LightstreamerListener listener) {
		if(user == null){
			return false;
		}

		return subscribeTable(	listener,
				TABLE_INSURANCES,
				MAX_FREQUENCIES_INSURANCE,
				new String[]{ITEM_INSURANCES + "_" + user.getUserName()},
				SimpleTableInfo.COMMAND,
				true,
				new String[] { 	COLUMN_INS_KEY, COLUMN_INS_COMMAND, COLUMN_INS_MARKET,
						COLUMN_INS_LEVEL, skinGroup.getInsCurrentLevelUpdateKey(),
						COLUMN_INS_AMOUNT, COLUMN_INS_TIME_CREATED, COLUMN_INS_WIN, COLUMN_INS_INS,
						COLUMN_INS_TYPE, COLUMN_INS_POSITION, COLUMN_INS_INS_TYPE, COLUMN_INS_LEVEL_CLR,
						COLUMN_INS_CLOSE_TIME, COLUMN_INS_MARKET_NAME, COLUMN_INS_PTYPE, COLUMN_INS_TIME_EST_CLOSE });
	}

	/**
	 * Subscribe the Shopping bag Table to the LS.
	 * @return
	 */
	@SuppressLint("DefaultLocale")
	public boolean subscribeShoppingBagTable(LightstreamerListener listener) {
		if(user == null){
			return false;
		}
		return subscribeTable(	listener,
				TABLE_SHOPPING_BAG,
				MAX_FREQUENCIES_SHOPPING_BAG,
				new String[]{ITEM_SHOPPING_BAG + "_" + user.getUserName() + "_" + user.getId()},
				SimpleTableInfo.COMMAND,
				true,
				new String[] {	COLUMN_SB_KEY, COLUMN_SB_COMMAND, COLUMN_SB_AMOUNT,
						COLUMN_SB_CLOSE_MOBILE, COLUMN_SB_LEVEL, COLUMN_SB_MARKET_ID,
						COLUMN_SB_INV_COUNT,
						COLUMN_SB_MARKET_RETURN_AMOUNT,
						COLUMN_SB_MARKET_WINNING,
						COLUMN_SB_OPP_TYPE, COLUMN_SB_TIME_CLOSE });
	}

	/**
	 * Subscribe the LIVE INVESTMENTS Table to the LS.
	 * @return
	 */
	public boolean subscribeLiveInvestmentsTable(LightstreamerListener listener) {
		return subscribeTable(	listener,
				TABLE_LIVE_INVESTMENTS,
				MAX_FREQUENCIES_LIVE_INVESTMENTS,
				new String[]{ITEM_LIVE_INVESTMENTS},
				SimpleTableInfo.COMMAND,
				true,
				new String[] {	COLUMN_LI_KEY, COLUMN_LI_COMMAND, COLUMN_LI_AMOUNT, COLUMN_LI_ASSET, COLUMN_LI_CITY,
						COLUMN_LI_CONVERTED_AMOUNT, COLUMN_LI_COUNTRY, COLUMN_LI_INV_KEY, COLUMN_LI_INV_TYPE,
						COLUMN_LI_LEVEL, COLUMN_LI_OPP_ID, COLUMN_LI_PROD_TYPE, COLUMN_LI_TIME});
	}

	/**
	 * Subscribe the ASSETS LIST to the LS.
	 * @return
	 */
	public boolean subscribeAssetsListTable(LightstreamerListener listener, String[] items) {
		return subscribeTable(	listener,
				TABLE_ASSETS_LIST,
				MAX_FREQUENCIES_ASSETS_LIST,
				items,
				SimpleTableInfo.COMMAND,
				true,
				new String[] {  skinGroup.getLevelUpdateKey(), COLUMN_ET_COMMAND, COLUMN_ET_KEY, COLUMN_ET_NAME , COLUMN_ET_OPP_ID , COLUMN_ET_STATE,
						COLUMN_ET_EST_CLOSE, COLUMN_ET_LAST_INV, COLUMN_AO_STATES, COLUMN_AO_TS, COLUMN_ET_NH, COLUMN_AO_NH,
						COLUMN_FILTER_NEW_SCHEDULED, COLUMN_AO_OPP_STATE,
						COLUMN_DYNAMICS_LEVEL, COLUMN_DYNAMICS_PROFIT_CALL, COLUMN_DYNAMICS_PROFIT_PUT,
						COLUMN_DYNAMICS_STATE,  COLUMN_DYNAMICS_SUSPEND_MSG, COLUMN_DYNAMICS_EVENT_LEVEL,
						COLUMN_DYNAMICS_TIME_LAST_INV, COLUMN_DYNAMICS_TIME_CLOSE});
	}

	/**
	 * Subscribe open options table to the LS.
	 *
	 * @param listener
	 * @param items
	 * @return
	 */
	public boolean subscribeOpenOptionsTable(LightstreamerListener listener, String[] items) {
		String[] columns = new String[] { COLUMN_ET_NAME, COLUMN_ET_OPP_ID, COLUMN_ET_STATE,COLUMN_ET_KEY, COLUMN_DYNAMICS_STATE, COLUMN_DYNAMICS_LEVEL, skinGroup.getLevelUpdateKey() };

		return subscribeTable(listener, TABLE_OPEN_OPTIONS, MAX_FREQUENCIES_OPEN_OPTIONS, items, SimpleTableInfo.COMMAND, true, columns);
	}

	/**
	 * Subscribe the CHART to the LS.
	 * @param listener
	 * @param items
	 * @return
	 */
	public boolean subscribeSimpleChartTable(LightstreamerListener listener, String[] items){
		return subscribeTable(	listener,
				TABLE_SIMPLE_CHART,
				MAX_FREQUENCIES_CHART,
				items,
				SimpleTableInfo.COMMAND,
				true,
				new String[] { 	skinGroup.getLevelUpdateKey(), COLUMN_ET_STATE, COLUMN_AO_TS, COLUMN_ET_ODDS, COLUMN_FILTER_NEW_SCHEDULED, COLUMN_AO_OPP_STATE});
	}

	/**
	 * Subscribe the Asset Page to the LS.
	 */
	public boolean subscribeAssetPageTable(long marketId, LightstreamerListener listener, String[] items) {
		// Note: COLUMN_AO_CALLS_TREND must be COLUMN_ET_CALLS_TREND for Etrader app
		String[] columns = new String[] { COLUMN_ET_KEY, COLUMN_ET_COMMAND, COLUMN_ET_OPP_ID, COLUMN_ET_STATE,
				skinGroup.getLevelUpdateKey(), COLUMN_ET_EST_CLOSE, COLUMN_ET_NAME,
				COLUMN_AO_CALLS_TREND, COLUMN_ET_ODDS_GROUP, COLUMN_ET_ODDS_LOSE,
				COLUMN_ET_ODDS_WIN, COLUMN_AO_TS, COLUMN_FILTER_NEW_SCHEDULED, COLUMN_AO_OPP_STATE ,
				COLUMN_DYNAMICS_LEVEL, COLUMN_DYNAMICS_PROFIT_CALL, COLUMN_DYNAMICS_PROFIT_PUT, COLUMN_DYNAMICS_STATE,
				COLUMN_DYNAMICS_SUSPEND_MSG, COLUMN_DYNAMICS_EVENT_LEVEL, COLUMN_DYNAMICS_TIME_LAST_INV, COLUMN_DYNAMICS_TIME_CLOSE, COLUMN_SB_OPP_TYPE, COLUMN_ET_NH, COLUMN_AO_NH};

		return subscribeTable(listener, TABLE_ASSET_PAGE + marketId, MAX_FREQUENCIES_ASSET_PAGE,
				items, SimpleTableInfo.COMMAND, true, columns);
	}

	/**
	 * Subscribe the Table to the LS.
	 *
	 * @param listener
	 *            The Listener which listens for update events from the LS. <b>IMPORTANT:</b>The
	 *            updates will come from another THREAD. Any UI changes MUST be made in the MAIN
	 *            Thread.
	 * @param tableName
	 *            The name of the Table. It is used in the Table Map as key.
	 * @param items
	 * @param maxFrequency
	 * @param mode
	 * @param snapShot
	 * @param columns
	 * @return
	 */
	public boolean subscribeTable(LightstreamerListener listener, String tableName, double maxFrequency , String[] items, String mode, boolean snapShot, String[] columns) {
		Log.d(TAG, "SUBCRIBE Table = " + tableName);
		if(ls == null) {
			startLightstreamer();
		}
		if(isTableSubscribed(tableName)){
			unsubscribeTable(tableName);
		}
		try {
			ExtendedTableInfo extendedTableInfo = new ExtendedTableInfo(items, mode, columns, snapShot);
			extendedTableInfo.setDataAdapter(LS_DATA_ADAPTER);
			extendedTableInfo.setRequestedMaxFrequency(maxFrequency);
			ls.addTable(extendedTableInfo, new AOTPSTableListener(listener));
			tablesMap.put(tableName, extendedTableInfo);
		} catch (Exception e) {
			Log.e(TAG, "Can't subscribe tables : " + items, e);
			return false;
		}

		return true;
	}

	/**
	 * UNsubscribe the table from the LS.
	 * @param table
	 */
	public void unsubscribeTable(String... tables) {
		for(String table : tables){
			Log.d(TAG, "UNSUBSCRIBE Table = " + table);
			final ExtendedTableInfo extendedTableInfo = tablesMap.get(table);
			if(ls != null) {
				ls.removeTable(extendedTableInfo);
			}
			tablesMap.remove(table);
		}
	}

	/**
	 * UNsubscribe all tables from the lS.
	 */
	public void unsubscribeAllTables(){
		synchronized (tablesMap) {
			for(Map.Entry<String, ExtendedTableInfo> entry : tablesMap.entrySet()){
				String table = entry.getKey();
				Log.d(TAG, "UNSUBSCRIBE Table = " + table);
				final ExtendedTableInfo extendedTableInfo = entry.getValue();
				if(ls != null) {
					ls.removeTable(extendedTableInfo);
				}
			}
			tablesMap.clear();
		}
	}

	/**
	 * Checks whether the table is subscribed to the LS.
	 * @param table
	 * @return
	 */
	public boolean isTableSubscribed(String table){
		return tablesMap.get(table) != null;
	}

	/**
	 * UNsubscribe asset page table from the LS.
	 *
	 * @param table
	 */
	public void unsubscribeAssetPageTable(Long... marketIds) {
		for (Long marketId : marketIds) {
			String table = TABLE_ASSET_PAGE + marketId;
			Log.d(TAG, "UNSUBSCRIBE Table = " + table);
			final ExtendedTableInfo extendedTableInfo = tablesMap.get(table);
			if (ls != null) {
				ls.removeTable(extendedTableInfo);
			}
			tablesMap.remove(table);
		}
	}

	@Override
	public void updateItem(int itemPos, String itemName, UpdateInfo update) {
		try {

			//Check if the update is for the Insurances:
			if (itemName.startsWith(ITEM_INSURANCES)) {
				Log.d(TAG, "Insurance update.");
				handleInsuranceUpdate(update);
			}

			//Check if the update is for the Shopping bag:
			if(itemName.startsWith(ITEM_SHOPPING_BAG)){
				Log.d(TAG, "Shopping bag update.");
				handleShoppingBagUpdate(update);
			}
		} catch (Exception e) {
			Log.e(TAG + " lightstreamer", "Problem processing updateItem.", e);
		}
	}


	/**
	 * Parse the Insurances update from the LS.
	 * <p>See {@link #updateItem(int, String, UpdateInfo)}
	 * @param update
	 */
	private void handleInsuranceUpdate(UpdateInfo update) {
		int updateInsurancesType = Integer.parseInt(update.getNewValue(COLUMN_INS_INS_TYPE));

		ArrayList<ArrayList<InsuranceItem>> insuranceGroups = null;
		InsuranceType insuranceType = null;

		if (updateInsurancesType == Investment.INSURANCE_TAKE_PROFIT) {
			insuranceType = InsuranceType.TAKE_PROFIT;
			insuranceGroups = application.getTakeProfitGroups();
		} else {
			// Investment.INSURANCE_ROLL_FORWARD
			insuranceType = InsuranceType.ROLL_FORWARD;
			insuranceGroups = application.getRollForwardGroups();
		}

		String investmentId = update.getNewValue(COLUMN_INS_KEY);
		String cmd = update.getNewValue(COLUMN_INS_COMMAND);
		if (cmd.equals(COMMAND_ADD)) {
			Log.d(TAG,
					"lightstreamer - Adding insurance - market: "
							+ update.getNewValue(COLUMN_INS_MARKET) + " invs: "
							+ update.getNewValue(COLUMN_INS_KEY) + " type: "
							+ update.getNewValue(COLUMN_INS_INS_TYPE) + " close: "
							+ update.getNewValue(COLUMN_INS_CLOSE_TIME));
			ls.sendMessage(MESSAGE_VIEWED_INVESTMENT_ID + "_" + investmentId);
			addInsurance(insuranceType, insuranceGroups, update);
		} else if (cmd.equals(COMMAND_UPDATE)) {
			Log.d(TAG, "lightstreamer - UPDATE inv: " + investmentId);
			updateInsuranceGroup(insuranceType, insuranceGroups, update);
		} else {
			// DELETE command
			Log.d(TAG,
					"lightstreamer - Removing insurance - market: "
							+ update.getNewValue(COLUMN_INS_MARKET) + " invs: "
							+ update.getNewValue(COLUMN_INS_KEY) + " type: "
							+ update.getNewValue(COLUMN_INS_INS_TYPE) + " close: "
							+ update.getNewValue(COLUMN_INS_CLOSE_TIME));
			removeInsurance(insuranceType, insuranceGroups, update);
		}
	}

	/**
	 * Handle the LS update for the Shopping Bag.
	 * @param update
	 */
	private void handleShoppingBagUpdate(UpdateInfo update) {
		if(update.getNewValue(COLUMN_SB_COMMAND).equals(COMMAND_ADD)){
			Log.d(TAG, "New Shopping Bag update!");

			double amount = 0;
			long amountLong = 0;
			long marketID = 0;
			String marketName;
			int expiredOptionsCount = 0;
			boolean isWinning = false;
			try {
				marketID = Long.valueOf(update.getNewValue(COLUMN_SB_MARKET_ID));
				amountLong = Long.valueOf(update.getNewValue(COLUMN_SB_MARKET_RETURN_AMOUNT));
				amount = AmountUtil.getDoubleAmount(amountLong);
				expiredOptionsCount = Integer.valueOf(update.getNewValue(COLUMN_SB_INV_COUNT));
				isWinning = (Integer.valueOf(update.getNewValue(COLUMN_SB_MARKET_WINNING)) == 1) ? true : false;
			} catch (Exception e) {
				Log.e(TAG, "Could not parse shopping bag update", e);
			}

			Market market = application.getMarketForID(marketID);
			marketName = market.getDisplayName();

			user = application.getUser();
			//Update the Balance:
			if(amount > 0){
				long currentBalance = user.getBalance();
				long newBalance = currentBalance + Long.valueOf(update.getNewValue(COLUMN_SB_MARKET_RETURN_AMOUNT));
				user.setBalance(newBalance);
				user.setBalanceWF(AmountUtil.getFormattedAmount(newBalance, user.getCurrency()));
				application.postEvent(new UserUpdatedEvent());
			}

			if(expiredOptionsCount > 0){
				user.setOpenedInvestmentsCount(user.getOpenedInvestmentsCount() - expiredOptionsCount);
				application.postEvent(new ExpiredOptionsEvent(amount, marketID, expiredOptionsCount, isWinning));
			}

			application.getNotificationManager().showNotificationShoppingBag(expiredOptionsCount, marketName, amountLong, isWinning);

		}

	}

	/**
	 * Adds Insurance.
	 * <p>See {@link #updateItem(int, String, UpdateInfo)}
	 * @param insuranceType
	 * @param insuranceGroups
	 * @param update
	 */
	private void addInsurance(InsuranceType insuranceType, ArrayList<ArrayList<InsuranceItem>> insuranceGroups, UpdateInfo update) {
		Log.d(TAG, "Adding Insurance...");
		if(insuranceGroups == null){
			Log.d(TAG, "Insurances Groups is null.");
			return;
		}
		long invId = Long.parseLong(update.getNewValue(COLUMN_INS_KEY));
		long insuranceCloseTime = Long.parseLong(update.getNewValue(COLUMN_INS_CLOSE_TIME));
		long opportunityCloseTime = Long.parseLong(update.getNewValue(COLUMN_INS_TIME_EST_CLOSE));
		int invType = Integer.parseInt(update.getNewValue(COLUMN_INS_TYPE));
		String pType = update.getNewValue(COLUMN_INS_PTYPE);
		String marketName = update.getNewValue(COLUMN_INS_MARKET_NAME);
		long marketId = Long.parseLong(update.getNewValue(COLUMN_INS_MARKET));

		long invTime = Long.parseLong(update.getNewValue(COLUMN_INS_TIME_CREATED));
		Date investmentTime = new Date(invTime);
		String invTimeFormatted = insuranceTimeFormat.format(investmentTime);

		String invLevel = update.getNewValue(COLUMN_INS_LEVEL);
		double invAmount = Double.parseDouble(update.getNewValue(COLUMN_INS_AMOUNT).replaceAll(",", ""));
		double premiumPrice = Double.parseDouble(update.getNewValue(COLUMN_INS_INS).replaceAll(",", ""));
		String currentLevel = update.getNewValue(skinGroup.getInsCurrentLevelUpdateKey());

		InsuranceItem insurance = new InsuranceItem(user.getCurrency(), invId, insuranceCloseTime, invType, marketId,
				marketName, invTime, invTimeFormatted, invLevel, invAmount, premiumPrice,
				currentLevel, pType, opportunityCloseTime);

		boolean insuranceAdded = false;
		boolean insuranceDuplicated = false;
		for (int i = 0; i < insuranceGroups.size(); i++) {
			ArrayList<InsuranceItem> currentList = insuranceGroups.get(i);

			if (currentList.size() > 0
					&& insurance.getMarketId() == currentList.get(0).getMarketId()) {
				for (int j = 0; j < currentList.size(); j++) {
					if(currentList.get(j).getInvestmentId() == insurance.getInvestmentId()) {
						insuranceDuplicated = true;
						insuranceAdded = true;
					}
				}

				if(!insuranceAdded) {
					currentList.add(insurance);
					insuranceAdded = true;
				}
				break;
			}
		}

		Log.d(TAG, "Checking if the Insurance is already added...");
		if (!insuranceAdded) {
			Log.d(TAG, "There was no group for this market so we add a new one.");
			// There was no group for this market so we add a new one.
			ArrayList<InsuranceItem> newGroup = new ArrayList<InsuranceItem>();
			newGroup.add(insurance);
			insuranceGroups.add(newGroup);
		}

		Log.d(TAG, "Checking if the Insurance is duplicated...");
		if (!insuranceDuplicated) {
			Log.d(TAG, "Insurance is NOT duplicated.");
			application.postEvent(new InsuranceEvent(insuranceType, Type.ADD));

			Log.d(TAG, "Showing notification for the first received insurance.");
			if (insuranceGroups.size() == 1 && insuranceGroups.get(0).size() == 1) {
				NotificationManager notificationManager = application.getNotificationManager();
				if (insuranceType == InsuranceType.TAKE_PROFIT) {
					Log.d(TAG, "Showing Take Profit notification.");
					InsuranceItem insuranceItem = insuranceGroups.get(0).get(0);
					notificationManager
							.showNotificationTakeProfit(
									insuranceTimeFormat.format(new Date(insuranceItem
											.getInsuranceCloseTime())));
				} else {
					Log.d(TAG, "Showing Take Roll Forward notification.");
					notificationManager.showNotificationRollForward();
				}
			}
		}
	}

	/**
	 * Updates the Insurance group.
	 * <p>See {@link #updateItem(int, String, UpdateInfo)}
	 * @param insuranceType
	 * @param insuranceGroups
	 * @param update
	 */
	private void updateInsuranceGroup(InsuranceType insuranceType, ArrayList<ArrayList<InsuranceItem>> insuranceGroups, UpdateInfo update) {
		if(insuranceGroups == null){
			return;
		}
		long marketId = Long.parseLong(update.getNewValue(COLUMN_INS_MARKET));
		String currentLevel = update.getNewValue(skinGroup.getInsCurrentLevelUpdateKey());
		for (int i = 0; i < insuranceGroups.size(); i++) {
			ArrayList<InsuranceItem> currentList = insuranceGroups.get(i);

			if (currentList.size() > 0 && currentList.get(0).getMarketId() == marketId) {
				if (currentList.get(0).getCurrentLevel().equals(currentLevel)) {
					// We have updated the insurances for this market.
					return;
				}

				// Update all insurances for this group with the given current level value.
				for (int j = 0; j < currentList.size(); j++) {
					currentList.get(j).setCurrentLevel(currentLevel);
				}

				// send event
				application.postEvent(new InsuranceEvent(insuranceType, Type.UPDATE));
				break;
			}
		}
	}

	/**
	 * Removes the Insurance.
	 * <p>See {@link #updateItem(int, String, UpdateInfo)}
	 * @param insuranceType
	 * @param insuranceGroups
	 * @param update
	 */
	private void removeInsurance(InsuranceType insuranceType, ArrayList<ArrayList<InsuranceItem>> insuranceGroups, UpdateInfo update) {
		if(insuranceGroups == null){
			return;
		}
		long invId = Long.parseLong(update.getNewValue(COLUMN_INS_KEY));
		long marketId = Long.parseLong(update.getNewValue(COLUMN_INS_MARKET));

		for (int i = 0; i < insuranceGroups.size(); i++) {
			ArrayList<InsuranceItem> currentList = insuranceGroups.get(i);

			if (currentList.size() > 0 && currentList.get(0).getMarketId() == marketId) {

				for (int j = 0; j < currentList.size(); j++) {
					if (currentList.get(j).getInvestmentId() == invId) {
						currentList.remove(j);
						if (currentList.size() == 0) {
							insuranceGroups.remove(i);
						}

						// send event
						application.postEvent(new InsuranceEvent(insuranceType, Type.DELETE));
						return;
					}
				}
			}
		}
	}

	/**
	 * Clear All Insurances.
	 */
	private void clearInsurances() {
		// Clear All insurances
		application.getTakeProfitGroups().clear();
		application.getRollForwardGroups().clear();
	}

	/**
	 * Get a simple description for a Opportunity State Id.
	 */
	public static String getOpportunityStateDescription(int stateId){
		String description = null;
		switch (stateId){
			case Opportunity.STATE_CREATED:
				description = "CREATED. Id = " + Opportunity.STATE_CREATED;
				break;
			case Opportunity.STATE_OPENED:
				description = "OPENED. Id = " + Opportunity.STATE_OPENED;
				break;
			case Opportunity.STATE_LAST_10_MIN:
				description = "LAST 10 MIN. Id = " + Opportunity.STATE_LAST_10_MIN;
				break;
			case Opportunity.STATE_CLOSING_1_MIN:
				description = "CLOSING 1 MIN. Id = " + Opportunity.STATE_CLOSING_1_MIN;
				break;
			case Opportunity.STATE_CLOSING:
				description = "CLOSING. Id = " + Opportunity.STATE_CLOSING;
				break;
			case Opportunity.STATE_CLOSED:
				description = "CLOSED. Id = " + Opportunity.STATE_CLOSED;
				break;
			case Opportunity.STATE_DONE:
				description = "DONE. Id = " + Opportunity.STATE_DONE;
				break;
			case Opportunity.STATE_PAUSED:
				description = "PAUSED. Id = " + Opportunity.STATE_PAUSED;
				break;
			case Opportunity.STATE_SUSPENDED:
				description = "SUSPENDED. Id = " + Opportunity.STATE_SUSPENDED;
				break;
			case Opportunity.STATE_WAITING_TO_PAUSE:
				description = "WAITING TO PAUSE. Id = " + Opportunity.STATE_WAITING_TO_PAUSE;
				break;
			case Opportunity.STATE_HIDDEN_WAITING_TO_EXPIRY:
				description = "HIDDEN WAITING TO EXPIRY. Id = " + Opportunity.STATE_HIDDEN_WAITING_TO_EXPIRY;
				break;
			case Opportunity.STATE_BINARY0100_UPDATING_PRICES:
				description = "BINARY0100 UPDATING PRICES. Id = " + Opportunity.STATE_BINARY0100_UPDATING_PRICES;
				break;
		}
		return description;
	}

}
