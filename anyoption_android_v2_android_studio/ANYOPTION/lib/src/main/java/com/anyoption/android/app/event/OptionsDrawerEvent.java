package com.anyoption.android.app.event;

public class OptionsDrawerEvent {

	/**
	 * Options drawer event type
	 */
	public enum Type {
		OPEN_DRAWER_REQUEST, CLOSE_DRAWER_REQUEST
	}

	private long marketId;
	private Type type;

	public OptionsDrawerEvent(long marketId, Type type) {
		this.marketId = marketId;
		this.type = type;
	}

	public long getMarketId() {
		return marketId;
	}

	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

}
