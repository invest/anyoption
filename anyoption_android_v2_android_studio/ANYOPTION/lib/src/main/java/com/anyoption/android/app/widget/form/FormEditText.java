package com.anyoption.android.app.widget.form;

import java.util.ArrayList;
import java.util.List;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.LayoutDirection;
import com.anyoption.android.R;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

/**
 * Custom Form Edit Text.
 * 
 * @author Anastas Arnaudov
 * 
 */
public class FormEditText extends RelativeLayout implements FormItem {

	public static final String TAG = FormEditText.class.getSimpleName();
	public static final float SCALE_HINT_RATIO = 0.70f;

	private LayoutDirection direction;

	private List<ValidatorType> validators;
	private List<FocusChangeListener> focusChangeListenersList;
	private String errorMessage;
	private FormEditText equalityField;
	private String equalityError;

	private boolean isValid = false;
	private boolean isHintUp = false;
	private boolean isShouldShowOKImage = true;

	private EditText editText;
	private TextView hintTextView;
	private TextView errorTextView;
	private ImageView okImageView;
	private String fieldName;
	private String value = "";
	private boolean hasMask;
	private boolean hasEditorActionListener;
	private String hintText;
	
	private boolean isOkViewVisible;
	private boolean isShouldNotBeValidated;

	public FormEditText(Context context) {
		super(context);
		setup(context, null, 0);
	}

	/**
	 * Custom Listener that listens for focus change events.
	 * 
	 * @author Anastas Arnaudov
	 * 
	 */
	public interface FocusChangeListener {
		/**
		 * Called when the field's focus has changed
		 * 
		 * @param hasFocus
		 */
		public void onFocusChanged(boolean hasFocus);
	}

	public FormEditText(Context context, AttributeSet set) {
		super(context, set);
		setup(context, set, 0);
	}

	public FormEditText(Context context, AttributeSet set, int defStyle) {
		super(context, set, defStyle);
		setup(context, set, defStyle);
	}

	protected void setup(Context context, AttributeSet set, int defStyle) {
		isOkViewVisible = true;
		
		View.inflate(context, R.layout.form_edittext_layout, this);
		setEditText((EditText) findViewById(R.id.form_edittext_edittext));
		okImageView = (ImageView) findViewById(R.id.form_edittext_ok_imageview);
		setHintTextView((TextView) findViewById(R.id.form_edittext_hint));
		setErrorTextView((TextView) findViewById(R.id.form_edittext_error_textview));
		// Get custom attributes:
		TypedArray attributesTypedArray = context.obtainStyledAttributes(set,
				R.styleable.FormEditText);
		int type = attributesTypedArray
				.getInt(R.styleable.FormEditText_type, 1);
		int maxCharacters = attributesTypedArray.getInt(
				R.styleable.FormEditText_maxCharacters, 100);
		hintText = attributesTypedArray
				.getString(R.styleable.FormEditText_hintText) != null ? attributesTypedArray
						.getString(R.styleable.FormEditText_hintText) : "";
		////////////////////////
		
		if(!isInEditMode()){

			direction = Application.get().getLayoutDirection();
	
	
	
			// Set values from custom attributes:
			// Set Keyboard Appearance:
			switch (type) {
			case 1:
				getEditText().setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
				break;
			case 2:
				getEditText().setInputType(InputType.TYPE_CLASS_TEXT| InputType.TYPE_TEXT_VARIATION_PASSWORD);
				break;
			case 3:
				getEditText().setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
				break;
			case 4:
				getEditText().setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
				break;
			case 5:
				getEditText().setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
				break;
			case 6:
				getEditText().setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_VARIATION_PASSWORD);
				break;
			default:
				break;
			}
	
			// Sets the Max number of characters:
			InputFilter[] filterArray = new InputFilter[1];
			filterArray[0] = new InputFilter.LengthFilter(maxCharacters);
			getEditText().setFilters(filterArray);
			// //
	
			getHintTextView().setText(hintText);
			///////////////////
	
			getEditText().setOnFocusChangeListener(new OnFocusChangeListener() {
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if (hasFocus) {
						Application.get().setCurrentEditTextOnFocus(getEditText());
						setupHint(hasFocus);
					} else {
						setupHint(hasFocus);
						// Validate the field after it looses focus:
						validate();
					}
	
					for (FocusChangeListener focusChangeListener : getFocusChangeListenersList()) {
						focusChangeListener.onFocusChanged(hasFocus);
					}
				}
			});
			
			getEditText().addTextChangedListener(new TextWatcher() {
				@Override
				public void onTextChanged(CharSequence s, int start, int before,int count) {
					Log.d(TAG, "");
				}
	
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,int after) {
					Log.d(TAG, "");
					if(hasMask && count > 0 && count != after){
						hasMask = false;
					}
				}
	
				@Override
				public void afterTextChanged(Editable s) {
					if(!hasMask){
						value = s.toString();					
					}
					setupHint(true);
				}
			});
	
			attributesTypedArray.recycle();
			setupUIForDirection();
		}else{
			getHintTextView().setText(hintText);
		}
			
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
		if(!isInEditMode()){
			if (this.direction == LayoutDirection.LEFT_TO_RIGHT) {
				getHintTextView().setPivotX(0);
				getHintTextView().setPivotY(0);
			} else {
				getHintTextView().setPivotX(getHintTextView().getWidth());
				getHintTextView().setPivotY(0);
			}
			
		}
	}

	/**
	 * Setup the Hint visibility.
	 * 
	 * @param hasFocus
	 *            Whether the Form Edit Text has focus.
	 * @param shouldAnimate
	 *            Whether the change should be animated or not.
	 */
	public void setupHint(boolean hasFocus) {
		if (getValue().length() == 0) {
			isHintUp = false;
			getHintTextView().setTextColor(getHintTextColor());
			// Move to the middle and scale:
			if (isShown()) {
				animateHintMiddle();
			} else {
				hintMiddle();
			}
		} else {
			if (!isHintUp) {
				isHintUp = true;
				// Move to the top and scale:
				if (isShown()) {
					animateHintUp();
				} else {
					hintUp();
				}
			}
			if (hasFocus) {
				getHintTextView().setTextColor(getHintFocusedTextColor());
			} else {
				getHintTextView().setTextColor(getHintTextColor());
			}
		}
	}

	protected int getHintFocusedTextColor(){
		return ColorUtils.getColor(R.color.form_item_hint_focused);
	}
	
	protected int getHintTextColor(){
		return ColorUtils.getColor(R.color.form_item_hint);
	}
	
	/**
	 * Animates the hint to up and scales it small.
	 */
	protected void animateHintUp() {
		float scale = SCALE_HINT_RATIO;
		getHintTextView().animate().translationY(-getHintTextView().getTop())
				.scaleX(scale).scaleY(scale);
		getHintTextView().animate().start();
	}

	/**
	 * Animates the hint to the middle and scales it to normal size.
	 */
	private void animateHintMiddle() {
		View parent = (View) getHintTextView().getParent();
		float scale = 1f;
		getHintTextView().animate()
				.y(parent.getHeight() / 2 - getHintTextView().getHeight() / 2)
				.scaleX(scale).scaleY(scale);
		getHintTextView().animate().start();
	}

	/**
	 * Moves the hint to the top of its parent and scales it small. Without
	 * animation.
	 */
	protected void hintUp() {
		RelativeLayout.LayoutParams params = (LayoutParams) getHintTextView().getLayoutParams();
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		getHintTextView().setLayoutParams(params);
		getHintTextView().setScaleX(SCALE_HINT_RATIO);
		getHintTextView().setScaleY(SCALE_HINT_RATIO);
	}

	/**
	 * Moves the hint to the middle of its parent and scales it to normal size.
	 * Without animation.
	 */
	protected void hintMiddle() {
		RelativeLayout.LayoutParams params = (LayoutParams) getHintTextView().getLayoutParams();
		params.addRule(RelativeLayout.CENTER_VERTICAL);
		getHintTextView().setLayoutParams(params);
		getHintTextView().setScaleX(1f);
		getHintTextView().setScaleY(1f);
	}

	/**
	 * Depending on the validity we setup the layout:
	 * <p>
	 * Show the ERROR or the OK.
	 */
	protected void setupValid() {
		getErrorTextView().setText(getErrorMessage());
		if (isOkViewVisible && isShouldShowOKImage) {
			okImageView.setVisibility(isValid ? View.VISIBLE : View.INVISIBLE);
		} else {
			okImageView.setVisibility(View.GONE);
		}
	}

	/**
	 * Executes all validators that have been assigned to the field and performs
	 * the layout changes.
	 */
	protected void validate() {

		if(isShouldNotBeValidated){
			return;
		}

		setErrorMessage("");

		// Check each Validator:
		if (validators != null) {
			for (ValidatorType validator : validators) {
				if (validator.validate(getValue())) {
					isValid = true;
					setErrorMessage("");
				} else {
					isValid = false;
					setErrorMessage(validator.getErrorMessage());
					break;
				}
			}
		}

		// Check the equality validation (if there is one set)
		validateEquality();

		// Refresh the layout:
		setupValid();
	}

	/**
	 * Checks the equality validation.
	 */
	private void validateEquality() {
		// Check the equality validator (if there is any):
		if (getErrorMessage().length() == 0 && equalityField != null) {
			if (!getValue().equals(equalityField.getValue())) {
				isValid = false;
				setErrorMessage((equalityError != null) ? equalityError
						: getResources().getString(R.string.error));
			} else {
				isValid = true;
			}
		}
	}

	/**
	 * Adds Validator to validate the field.
	 * 
	 * @param validatorType
	 */
	public void addValidator(ValidatorType... validatorType) {
		if (validators == null) {
			validators = new ArrayList<ValidatorType>();
		}
		for (int i = 0; i < validatorType.length; i++) {
			validators.add(validatorType[i]);
		}
	}

	/**
	 * Adds Validator to validate the field.
	 * 
	 * @param validatorType
	 */
	public void addFocusChangeListener(
			FocusChangeListener... focusChangeListener) {
		if (getFocusChangeListenersList() == null) {
			setFocusChangeListenersList(new ArrayList<FocusChangeListener>());
		}
		for (int i = 0; i < focusChangeListener.length; i++) {
			getFocusChangeListenersList().add(focusChangeListener[i]);
		}
	}

	/**
	 * Adds Validator to check equality with the given field.
	 * 
	 * @param field
	 *            The {@link FormEditText} field which this field is going to be
	 *            compared to.
	 */
	public void addValidatorForEquality(FormEditText field, String errorMessage) {
		if (field != null) {
			equalityField = field;
			equalityError = errorMessage;
		}
	}

	public EditText getEditText() {
		return editText;
	}

	public void setEditText(EditText editText) {
		this.editText = editText;
	}

	@Override
	public boolean checkValidity() {
		validate();
		return isValid;
	}

	@Override
	public boolean isEditable() {
		return getEditText().isEnabled();
	}

	@Override
	public void setEditable(boolean isEditable) {
		getEditText().setEnabled(isEditable);
	}

	@Override
	public String getValue() {
		if(hasMask){
			return value;
		}
		return getEditText().getText().toString();
	}

	@Override
	public void setValue(Object value) {
		if (value != null) {
			if(hasMask){
				this.value = value.toString();							
			}else{
				this.value = value.toString();
				getEditText().setText(value.toString());
			}
		}

	}

	/**
	 * Set the value of the field and a Mask to be displayed instead of the value.
	 * <p><b>IMPORTANT:</b> The Mask MUST be with the same size as the value.
	 * @param value
	 * @param mask
	 * @throws Exception 
	 */
	public void setValueWithMask(Object value, String mask){		
		hasMask = true;
		this.value = value.toString();
		getEditText().setText(mask);
	}

	@Override
	public void clear() {
		getEditText().setText("");
		setupHint(false);
		setupValid();
		if (isOkViewVisible) {
			okImageView.setVisibility(View.INVISIBLE);
		} else {
			okImageView.setVisibility(View.GONE);
		}
	}

	@Override
	public void addOnEditorActionListener(OnEditorActionListener onEditorActionListener) {
		getEditText().setOnEditorActionListener(onEditorActionListener);
		hasEditorActionListener = true;
	}

	public boolean hasEditorActionListener(){
		return hasEditorActionListener;
	}
	
	@Override
	public void setDirection(LayoutDirection direction) {
		if (this.direction != direction) {
			this.direction = direction;
			setupUIForDirection();
		}
	}

	/**
	 * Sets up the UI according to the Direction.
	 */
	public void setupUIForDirection() {
		LayoutParams okParams = new LayoutParams(
				okImageView.getLayoutParams().width,
				okImageView.getLayoutParams().height);
		okParams.addRule(RelativeLayout.CENTER_VERTICAL);
		okParams.setMargins((int)getResources().getDimension(R.dimen.form_item_padding_left),
		0, (int)getResources().getDimension(R.dimen.form_item_padding_right), 0);
		switch (direction) {
		case LEFT_TO_RIGHT:
			getEditText().setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
			getHintTextView()
					.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
			okParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			getErrorTextView().setGravity(Gravity.BOTTOM | Gravity.RIGHT);
			break;
		case RIGHT_TO_LEFT:
			getEditText().setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
			getHintTextView().setGravity(
					Gravity.CENTER_VERTICAL | Gravity.RIGHT);
			okParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			getErrorTextView().setGravity(Gravity.BOTTOM | Gravity.LEFT);
			break;
		default:
			break;
		}
		okImageView.setLayoutParams(okParams);

	}

	@Override
	public LayoutDirection getDirection() {
		return direction;
	}

	@Override
	public void refreshUI() {
		setupUIForDirection();

	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public TextView getHintTextView() {
		return hintTextView;
	}

	public void setHintTextView(TextView hintTextView) {
		this.hintTextView = hintTextView;
	}

	public TextView getErrorTextView() {
		return errorTextView;
	}

	public void setErrorTextView(TextView errorTextView) {
		this.errorTextView = errorTextView;
	}

	public void displayError(String error) {
		if (error != null && !error.isEmpty()) {
			isValid = false;
		} else {
			isValid = true;
		}
		setErrorMessage(error);
		setupValid();
	}

	public List<FocusChangeListener> getFocusChangeListenersList() {
		if (focusChangeListenersList == null) {
			focusChangeListenersList = new ArrayList<FormEditText.FocusChangeListener>();
		}
		return focusChangeListenersList;
	}

	public void setFocusChangeListenersList(
			List<FocusChangeListener> focusChangeListenersList) {
		this.focusChangeListenersList = focusChangeListenersList;
	}

	/**
	 * @return the fieldName
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * @param fieldName
	 *            the fieldName to set
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public void setAddTextChangedListener(TextWatcher textWatcher) {
		getEditText().addTextChangedListener(textWatcher);
	}

	public void removeTextChangedListener(TextWatcher textWatcher){
		getEditText().removeTextChangedListener(textWatcher);
	}
	
	public boolean isOkViewVisible() {
		return isOkViewVisible;
	}

	public void setOkViewVisible(boolean isOkViewVisible) {
		this.isOkViewVisible = isOkViewVisible;
		if (okImageView != null) {
			if(isOkViewVisible){
				okImageView.setVisibility(View.VISIBLE);
			}else{
				okImageView.setVisibility(View.GONE);
			}
		}
	}

	public void setShouldShowOKImage(boolean isShouldShowOKImage) {
		this.isShouldShowOKImage = isShouldShowOKImage;
	}

	public void setShouldNotBeValidated(boolean isShouldNotBeValidated) {
		this.isShouldNotBeValidated = isShouldNotBeValidated;
	}

	public void setValid(boolean isValid){
		this.isValid = isValid;
		displayError("");
		setOkViewVisible(isValid);
	}
}
