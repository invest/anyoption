package com.anyoption.android.app.event;

/**
 * Event that occurs when the user decides to skip application update.
 * @author Anastas Arnaudov
 *
 */
public class UpdateSkipEvent {

}
