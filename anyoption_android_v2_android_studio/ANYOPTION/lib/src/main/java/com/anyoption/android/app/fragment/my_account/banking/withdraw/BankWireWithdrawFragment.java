package com.anyoption.android.app.fragment.my_account.banking.withdraw;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.manager.PopUpManager.OnAlertPopUpButtonPressedListener;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.popup.LowWithdrawDialogFragment;
import com.anyoption.android.app.popup.LowWithdrawDialogFragment.LowWithdrawListener;
import com.anyoption.android.app.popup.WithdrawSurveyConfirmationDialogFragment;
import com.anyoption.android.app.popup.WithdrawSurveyDialogFragment;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.RequestButton.InitAnimationListener;
import com.anyoption.android.app.widget.TextView;
import com.anyoption.android.app.widget.form.Form;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.android.app.widget.form.FormSelector.ItemListener;
import com.anyoption.android.app.widget.form.FormStringSelector;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.bl_vos.WireBase;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.json.requests.WireMethodRequest;
import com.anyoption.json.results.OptionsMethodResult;
import com.anyoption.json.results.TransactionMethodResult;
import com.anyoption.json.util.OptionItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents the bank wire withdraw screen.
 * @author EranL
 *
 */
public class BankWireWithdrawFragment extends NavigationalFragment implements BaseDialogFragment.OnDialogDismissListener, WithdrawSurveyDialogFragment.OnWithdrawSurveyClosed {
	
	public static final String TAG = BankWireWithdrawFragment.class.getSimpleName();

	protected Form form;

	protected View rootView;
	protected FormEditText accountNumber;
	protected FormEditText branchNumber;
	protected FormEditText amount;
	protected FormStringSelector bankNameSpinner;

	private FormEditText beneficiary;
	private FormEditText swift;
	private FormEditText iban;
	private FormEditText bankName;
	private FormEditText branchAddress;
	private FormEditText branchName;
	private RequestButton submitButton;
	private WireMethodRequest request;

	private String fee;

	protected User user;
	private long skinId;

	protected boolean isWithdrawSurveySkipped = true;
	protected long withdrawSurveyUserAnswer = 0l;
	protected String withdrawSurveyUserAnswerText = null;

	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static BankWireWithdrawFragment newInstance(){	
		return newInstance(new Bundle());
	}
	
	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static BankWireWithdrawFragment newInstance(Bundle bundle){
		BankWireWithdrawFragment bankwireWithdrawFragment = new BankWireWithdrawFragment();	
		bankwireWithdrawFragment.setArguments(bundle);
		return bankwireWithdrawFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		user = application.getUser();	 
		skinId = application.getSkinId();		
		
		rootView = inflater.inflate(R.layout.wire_withdraw_layout, container, false);
				       
        submitButton = (RequestButton) rootView.findViewById(R.id.bank_wire_withdraw_button);		
        submitButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				tryToWithdraw();
			}
		});
        
		form = new Form(new Form.OnSubmitListener() {
			@Override 
			public void onSubmit() {
				showWithdrawSurvey();
			} 
		});

		init();
		
		return rootView;			
	}

	protected void showWithdrawSurvey(){
		resetWithdrawSurveyParameters();
		WithdrawSurveyDialogFragment withdrawSurveyDialogFragment = WithdrawSurveyDialogFragment.newInstance(null);
		withdrawSurveyDialogFragment.setOnWithdrawSurveyClosedListener(BankWireWithdrawFragment.this);
		application.getFragmentManager().showDialogFragment(withdrawSurveyDialogFragment);
	}

	protected void tryToWithdraw(){
		if (RegulationUtils.checkRegulationStatusAndDoNecessary(true)) {
			if(form.checkValidity()){
				submitButton.startLoading(new InitAnimationListener(){
					@Override
					public void onAnimationFinished() {
						form.submit();
					};
				});
			}
		}
	}
	
	protected void init() {
		//Beneficiary:
		beneficiary = (FormEditText) rootView.findViewById(R.id.beneficiary_name_edittext);	
		beneficiary.addValidator(ValidatorType.EMPTY);	
		//SWIFT:
		swift = (FormEditText) rootView.findViewById(R.id.swift_edittext);
		View swiftLayout = rootView.findViewById(R.id.bank_wire_swift_field_layout);
		//IBAN:
		iban = (FormEditText) rootView.findViewById(R.id.iban_edittext);
		View ibanLayout = rootView.findViewById(R.id.bank_wire_iban_field_layout);
		//Account Number:
		accountNumber = (FormEditText) rootView.findViewById(R.id.account_edittext);
		accountNumber.addValidator(ValidatorType.EMPTY, ValidatorType.DIGITS);
		//Bank Name:
		bankName = (FormEditText) rootView.findViewById(R.id.bank_name_edittext);
		View bankNameLayout = rootView.findViewById(R.id.bank_wire_bank_name_field_layout);
		//Chinese/ET Bank Wire spinner
		bankNameSpinner = (FormStringSelector) rootView.findViewById(R.id.bank_name_selector);
		View bankNameSelectorLayout = rootView.findViewById(R.id.bank_wire_bank_name_selector_layout);
		//Branch Name:
		branchName = (FormEditText) rootView.findViewById(R.id.branch_name_edittext);
		View branchNameLayout = rootView.findViewById(R.id.bank_wire_branch_name_field_layout);
		//Branch Number:
		branchNumber = (FormEditText) rootView.findViewById(R.id.branch_number_edittext);
		View branchNumberLayout = rootView.findViewById(R.id.bank_wire_branch_number_field_layout);
		//Branch Address:
		branchAddress = (FormEditText) rootView.findViewById(R.id.branch_address_edittext);
		View branchAddressLayout = rootView.findViewById(R.id.bank_wire_branch_address_field_layout);
		//Currency:
		TextView currencySymbol = ((TextView)rootView.findViewById(R.id.currency_symbol_view));
        currencySymbol.setText(user.getCurrencySymbol());
        //Amount:
		amount = ((FormEditText)rootView.findViewById(R.id.amount));
		amount.addValidator(ValidatorType.EMPTY, ValidatorType.CURRENCY_AMOUNT);
		amount.setFieldName(Constants.FIELD_AMOUNT);		
		
		//Check the SKIN:
		if (skinId == Skin.SKIN_REG_DE || skinId == Skin.SKIN_GERMAN) {
			//GERMAN SKINS:
			
			//SWIFT:
			swiftLayout.setVisibility(View.VISIBLE);
			swift.addValidator(ValidatorType.EMPTY);
			//IBAN:
			ibanLayout.setVisibility(View.VISIBLE);
			iban.addValidator(ValidatorType.EMPTY);
			//Bank Name:
			bankNameLayout.setVisibility(View.VISIBLE);
			bankName.addValidator(ValidatorType.EMPTY);
			//Bank Name Selector:
			bankNameSelectorLayout.setVisibility(View.GONE);
			//Branch Name:
			branchNameLayout.setVisibility(View.GONE);
			branchName.addValidator(ValidatorType.VACUOUS);
			//Branch Number:
			branchNumberLayout.setVisibility(View.GONE);
			branchNumber.addValidator(ValidatorType.VACUOUS);
			//Branch Address:
			branchAddressLayout.setVisibility(View.GONE);
			branchAddress.addValidator(ValidatorType.VACUOUS);	
			
		} else if (skinId == Skin.SKIN_CHINESE || skinId == Skin.SKIN_CHINESE_VIP) {	
			//CHINESE SKINS:

			//SWIFT:
			swiftLayout.setVisibility(View.GONE);
			swift.addValidator(ValidatorType.VACUOUS);
			//IBAN:
			ibanLayout.setVisibility(View.GONE);
			iban.addValidator(ValidatorType.VACUOUS);	
			//Bank Name:
			bankNameLayout.setVisibility(View.GONE);
			bankName.addValidator(ValidatorType.VACUOUS);			
			//Bank Name Selector:
			bankNameSelectorLayout.setVisibility(View.VISIBLE);
			//Branch Name:
			branchNameLayout.setVisibility(View.VISIBLE);
			branchName.addValidator(ValidatorType.EMPTY);
			//Branch Number:
			branchNumberLayout.setVisibility(View.VISIBLE);
			branchNumber.addValidator(ValidatorType.EMPTY, ValidatorType.DIGITS);
			//Branch Address:
			branchAddressLayout.setVisibility(View.GONE);
			branchAddress.addValidator(ValidatorType.VACUOUS);

			// Get the Chinese Bank Name:
			Application.get().getCommunicationManager().requestService(BankWireWithdrawFragment.this, "getBankWireWithdrawOptionsCallBack", Constants.SERVICE_GET_BANK_WIRE_WITHDRAW_OPTIONS, new MethodRequest(), OptionsMethodResult.class, false, false);
			
		}else{
			//OTHER SKINS:

			//SWIFT:
			swiftLayout.setVisibility(View.VISIBLE);
			swift.addValidator(ValidatorType.EMPTY);
			//IBAN:
			ibanLayout.setVisibility(View.VISIBLE);
			iban.addValidator(ValidatorType.EMPTY);	
			//Bank Name:
			bankNameLayout.setVisibility(View.VISIBLE);
			bankName.addValidator(ValidatorType.EMPTY);
			//Bank Name Selector:
			bankNameSelectorLayout.setVisibility(View.GONE);
			//Branch Name:
			branchNameLayout.setVisibility(View.GONE);
			branchName.addValidator(ValidatorType.VACUOUS);
			//Branch Number:
			branchNumberLayout.setVisibility(View.VISIBLE);
			branchNumber.addValidator(ValidatorType.EMPTY, ValidatorType.DIGITS);
			//Branch Address:
			branchAddressLayout.setVisibility(View.VISIBLE);
			branchAddress.addValidator(ValidatorType.EMPTY);

		}
		
		form.addItem(beneficiary, iban, swift, accountNumber, bankName, branchNumber,
				branchAddress, branchName, amount);
	}
	
	/**
	 * Insert bank wire withdraw service call
	 */
	public void insertBankWireWithdraw() {

	}

	protected void setWireParams(WireBase wire) {
		wire.setBeneficiaryName(beneficiary.getValue());
		wire.setSwift(swift.getValue());
		wire.setIban(iban.getValue());
		wire.setAccountNum(accountNumber.getValue());

		if (skinId == Skin.SKIN_CHINESE || skinId == Skin.SKIN_CHINESE_VIP) {
			wire.setBankNameTxt(bankNameSpinner.getValue());
		} else {
			wire.setBankNameTxt(bankName.getValue());
		}

		wire.setBranch(branchNumber.getValue());
		wire.setBranchAddress(branchAddress.getValue());
		wire.setBranchName(branchName.getValue());
		wire.setAmount(amount.getValue());
	};

	/**
	 * Get bank wire withdraw options call back from the service call
	 * @param resultObj
	 */
	public void getBankWireWithdrawOptionsCallBack(Object resultObj) {
		Log.d(TAG, "validate withdraw bank CallBack");
		OptionsMethodResult result = (OptionsMethodResult) resultObj;
		if(result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS){
			List<String> list = new ArrayList<String>();
			for (OptionItem o: result.getOptions()) {
				list.add(o.getValue());
			}		
			bankNameSpinner.setItems(list, 0, new ItemListener<String>() {
				@Override
				public void onItemSelected(String selectedItem) {
					bankNameSpinner.showText(selectedItem);
				}
				
				@Override
				public String getDisplayText(String selectedItem) {			
					return selectedItem;
				}
				
				@Override
				public Drawable getIcon(String selectedItem) {
					return null;
				}
				
			});
			//Display the first item of the selector as the default one::
			bankNameSpinner.showText(list.get(0));
		}
	}
	
	/**
	 * Validation call back from the service call
	 * @param resultObj
	 */
	public void validateWithdrawBankCallBack(Object resultObj) {
		Log.d(TAG, "validate withdraw bank CallBack");
		TransactionMethodResult result = (TransactionMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Show important note popup");
			fee = result.getFee();
			// show important note popup
				Bundle bundle = new Bundle();
				bundle.putString(Constants.EXTRA_FEE, fee);
				LowWithdrawDialogFragment lowWithdrawDialogFragment = (LowWithdrawDialogFragment) application.getFragmentManager().showDialogFragment(LowWithdrawDialogFragment.class, bundle);
				lowWithdrawDialogFragment.setListener(new LowWithdrawListener() {
					@Override
					public void onOK() {
						if(!Application.get().getCommunicationManager().requestService(BankWireWithdrawFragment.this, "insertWithdrawBankCallBack",Constants.SERVICE_INSERT_WITHDRAW_BANK, request , TransactionMethodResult.class)){
							submitButton.setEnabled(true);
							submitButton.stopLoading();
						}
					}
					@Override
					public void onCancel() {
						submitButton.setEnabled(true);
						submitButton.stopLoading();
						resetWithdrawSurveyParameters();
					}
				});
		} else {
			Log.d(TAG, "Display error");
			ErrorUtils.displayFieldError(form, result);
			submitButton.setEnabled(true);
			submitButton.stopLoading();
			resetWithdrawSurveyParameters();
		}
	}
	
	/**
	 * Call back from the service call
	 * @param resultObj
	 */
	public void insertWithdrawBankCallBack(Object resultObj) {
		Log.d(TAG, "Insert withdraw bank callBack");
		TransactionMethodResult result = (TransactionMethodResult) resultObj;
		if (result != null &&result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Insert withdrawal: " + result.getTransactionId());
			// show important note popup
				String msg = getString(R.string.bank_wire_success, result.getFormattedAmount());
				if (fee.equals(Constants.NO_FEE)) {
					msg = getString(R.string.rewards_bank_wire_success, result.getFormattedAmount());
				}
				application.getPopUpManager().showAlertPopUp(getString(R.string.banking_withdrawal_note_header), msg, new OnAlertPopUpButtonPressedListener() {				
					@Override
					public void onButtonPressed() {
						application.getFragmentManager().onBack();						
					}
				} );
		} else {
			Log.d(TAG, "Insert bank withdraw failed");
			ErrorUtils.displayFieldError(form, result);	
			submitButton.setEnabled(true);
			resetWithdrawSurveyParameters();
		}
	}

	protected void callValidateWithdrawService() {
		Log.d(TAG, "Going to submit insert bank wire withdraw");
		WireBase wire = new WireBase();
		request = new WireMethodRequest();
		setWireParams(wire);

		if (isWithdrawSurveySkipped) {
			resetWithdrawSurveyParameters();
		}

		request.setUserAnswerId(withdrawSurveyUserAnswer);
		request.setTextAnswer(withdrawSurveyUserAnswerText);

		request.setWire(wire);
		if(!Application.get().getCommunicationManager()
				.requestService(BankWireWithdrawFragment.this, "validateWithdrawBankCallBack"
						, Constants.SERVICE_VALIDATE_WITHDRAW_BANK, request, TransactionMethodResult.class)){
			submitButton.stopLoading();
		}

		resetWithdrawSurveyParameters();
	}

	@Override
	public void onWithdrawSurveyClosed(boolean isSkipped, long chosenAnswer, String answerText) {
		isWithdrawSurveySkipped = isSkipped;
		withdrawSurveyUserAnswer = chosenAnswer;
		withdrawSurveyUserAnswerText = answerText;

		if (!isSkipped) {
			WithdrawSurveyConfirmationDialogFragment withdrawSurveyConfirmationDialogFragment = WithdrawSurveyConfirmationDialogFragment.newInstance(null);
			withdrawSurveyConfirmationDialogFragment.setOnDismissListener(BankWireWithdrawFragment.this);
			application.getFragmentManager().showDialogFragment(withdrawSurveyConfirmationDialogFragment);
		} else {
			callValidateWithdrawService();
		}

	}

	@Override
	public void onDismiss() {
		callValidateWithdrawService();
	}

	protected void resetWithdrawSurveyParameters() {
		isWithdrawSurveySkipped = true;
		withdrawSurveyUserAnswer = 0l;
		withdrawSurveyUserAnswerText = null;
	}

	@Override
	protected Screenable setupScreen() {		
		return Screen.BANK_WIRE_WITHDRAW;
	}
 
}
