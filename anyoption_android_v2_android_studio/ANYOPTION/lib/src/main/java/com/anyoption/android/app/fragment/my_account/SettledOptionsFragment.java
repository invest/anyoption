package com.anyoption.android.app.fragment.my_account;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.InvestmentUtil;
import com.anyoption.android.app.util.adapter.TradingOptionsListAdapter;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.service.requests.InvestmentsMethodRequest;
import com.anyoption.common.service.results.InvestmentsMethodResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SettledOptionsFragment extends NavigationalFragment {

	public static final String TAG = SettledOptionsFragment.class.getSimpleName();

	protected static final int PAGE_SIZE 			= 15;
	protected static final int PERIOD_DAY 		= 1;
	protected static final int PERIOD_WEEK 		= 2;
	protected static final int PERIOD_MONTH 	= 3;

	protected View root;
	protected ListView optionsListView;
	protected TradingOptionsListAdapter optionsListAdapter;

	protected List<Investment> investments;
	protected int period;
	protected int startRow;
	protected boolean isLastSettledOptionsReached;
	protected boolean isTablet;

	protected boolean isLoading;
	private View loadingView;

	protected View footerView;

	public static SettledOptionsFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created fragment SettledOptionsFragment");
		SettledOptionsFragment fragment = new SettledOptionsFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		isTablet = getResources().getBoolean(R.bool.isTablet);

		root = inflater.inflate(R.layout.settled_options_fragment, container, false);
		loadingView = root.findViewById(R.id.settled_options_loading_view);
		investments = new ArrayList<Investment>();
		footerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.settled_options_listview_footer_layout, null, false);

		setupListView();

		startRow = 0;
		isLastSettledOptionsReached = false;
		isLoading = false;
		setupLoading();
		requestInvestments();

		return root;
	}

	protected ListView getListView(){
		return optionsListView;
	}

	protected void setupListView(){

		optionsListView = (ListView) root.findViewById(R.id.settled_options_listview);
		setUpListAdapter();
		optionsListView.setAdapter(optionsListAdapter);
		optionsListView.setOnScrollListener(new OnScrollListener() {

			private int currentLastVisibleItem;
			private int currentVisibleItemCount;
			private int currentScrollState;

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				this.currentScrollState = scrollState;
				isScrollCompleted();
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,int totalItemCount) {
				this.currentLastVisibleItem = firstVisibleItem + visibleItemCount;
				this.currentVisibleItemCount = visibleItemCount;
			}

			private void isScrollCompleted() {
				if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
					// Detect if there's been a scroll which has completed.
					// And check if the list is scrolled to the bottom.
					if (currentLastVisibleItem >= optionsListAdapter.getCount()) {
						if(!isLastSettledOptionsReached){
							Log.d(TAG, "List is scrolled to the bottom. Request more investments!");
							requestInvestments();
						}
					}
				}
			}

		});

		setupItemClickListener();
	}

	protected void setupItemClickListener(){
		optionsListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Investment investment = investments.get(position);
				Market marketPressed = application.getMarketForID(investment.getMarketId());

				if (marketPressed != null) {
					Bundle bundle = new Bundle();

					if (isTablet) {
						bundle.putSerializable(Constants.EXTRA_MARKET, marketPressed);
						bundle.putString(Constants.EXTRA_ACTION_BAR_TITLE, marketPressed.getDisplayName());
						bundle.putLong(Constants.EXTRA_MARKET_PRODUCT_TYPE, marketPressed.getProductTypeId());

						application.postEvent(new NavigationEvent(Screen.ASSET_PAGE, NavigationType.HORIZONTAL, bundle));
						application.postEvent(new NavigationEvent(Screen.TABLET_TRADE, NavigationType.INIT, bundle));
					} else {
						bundle.putSerializable(Constants.EXTRA_MARKET, marketPressed);
						application.postEvent(new NavigationEvent(Screen.ASSET_VIEW_PAGER, NavigationType.DEEP, bundle));
					}
				}
			}
		});
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (!hidden) {
			startRow = 0;
			isLastSettledOptionsReached = false;
			investments.clear();
			getListView().setFooterDividersEnabled(true);
			getListView().removeFooterView(footerView);
			requestInvestments();
		}
	}

	protected void setupLoading(){
		if(loadingView != null){
			if(isLoading){
				loadingView.setVisibility(View.VISIBLE);
			}else{
				loadingView.setVisibility(View.GONE);
			}
		}
	}

	protected void setUpListAdapter() {
		optionsListAdapter = new TradingOptionsListAdapter(investments, false);
	}

	private void requestInvestments() {
		if (isLoading) {
			return;
		}
		isLoading = true;
		setupLoading();

		InvestmentsMethodRequest request = new InvestmentsMethodRequest();
		request.setSettled(true);
		request.setStartRow(startRow);
		request.setPageSize(PAGE_SIZE);
		int period = getArguments().getInt(Constants.EXTRA_PERIOD, PERIOD_DAY);
		if(this.period != period){
			investments.clear();
		}
		this.period = period;
		request.setPeriod(period);

		application.getCommunicationManager().requestService(this, "investmentsCallBack",Constants.SERVICE_GET_INVESTMENTS, request, InvestmentsMethodResult.class, false,false);
	}

	public void investmentsCallBack(Object result) {
		isLoading = false;
		setupLoading();

		InvestmentsMethodResult investmentsResult = (InvestmentsMethodResult) result;
		if (investmentsResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Investment[] newInvestments = investmentsResult.getInvestments();
			InvestmentUtil.fixDate(newInvestments);
			getInvestmentsFromResult(investmentsResult);
			gotInvestmentsForAdapter();
			startRow += PAGE_SIZE;
			if(newInvestments.length < PAGE_SIZE){
				isLastSettledOptionsReached = true;
			}
			Log.d(TAG, "Just got settled options: " + newInvestments.length);

			int period = getArguments().getInt(Constants.EXTRA_PERIOD, PERIOD_DAY);
			if(period == PERIOD_MONTH) {
				getListView().setFooterDividersEnabled(true);
				getListView().removeFooterView(footerView);
				if(investments.size() > 0) {
					getListView().setFooterDividersEnabled(false);
					getListView().addFooterView(footerView);
				}
			} else {
				getListView().setFooterDividersEnabled(true);
				getListView().removeFooterView(footerView);
			}

		} else {
			Log.e(TAG, "Could not get settled options! :)");
		}
	}

	protected void getInvestmentsFromResult(InvestmentsMethodResult investmentsResult){
		Collections.addAll(investments, investmentsResult.getInvestments());
	}

	protected void gotInvestmentsForAdapter(){
		optionsListAdapter.setInvestments(investments);
		refreshList();
	}

	protected void refreshList(){
		optionsListAdapter.notifyDataSetChanged();
	}

	@Override
	protected Screenable setupScreen() {
		return Screen.SETTLED_OPTIONS;
	}

}
