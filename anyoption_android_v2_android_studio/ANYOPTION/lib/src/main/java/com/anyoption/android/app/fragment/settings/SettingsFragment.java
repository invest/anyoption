package com.anyoption.android.app.fragment.settings;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.activity.LaunchActivity;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.LoginLogoutEvent.Type;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.event.PushNotificationsPermissionEvent;
import com.anyoption.android.app.fragment.ActionFragment;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.manager.PopUpManager.OnConfirmPopUpButtonPressedListener;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.Button;
import com.anyoption.android.app.widget.LanguageSelector;
import com.anyoption.android.app.widget.Switch;
import com.anyoption.android.app.widget.Switch.StateChangeListener;
import com.anyoption.common.service.requests.CancelInvestmentCheckboxStateMethodRequest;
import com.anyoption.common.service.results.MethodResult;

/**
 * Settings screen.
 * @author Anastas Arnaudov
 *
 */
public class SettingsFragment extends NavigationalFragment {

	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static SettingsFragment newInstance(Bundle bundle){
		SettingsFragment settingsFragment = new SettingsFragment();
		settingsFragment.setArguments(bundle);
		return settingsFragment;
	}

	private LanguageSelector languageSelector;
	private View languageLayout;
	protected View cancelInvestmentLayout;
	private Button logoutButton;
	private View notificationsLayout;
	private Switch notificationsSwitch;
	private Switch cancelInvestmentSwitch;
	protected View resetIntrosButton;
	private TextView versionTextView;
	private boolean isUserLoggedIn;
	private boolean isNotificationsOn;
	protected View rootView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Get the saved settings
		isUserLoggedIn = application.getUser() != null;
		isNotificationsOn = application.getSharedPreferencesManager().getBoolean(Constants.PREFERENCES_IS_NOTIFICATIONS_ON, true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.settings_fragment_layout, container, false);

		setupUI();

		return rootView;
	}

	protected void setupAppVersionText(){
		versionTextView = (TextView) rootView.findViewById(R.id.settings_app_version);
		//Set up the Version:
		versionTextView.setText("V " + application.getApplicationVersionName());
	}

	protected void setupLoginLogoutButton(){
		logoutButton = (Button) rootView.findViewById(R.id.settings_logout_button);
		logoutButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//TODO TEST
				/////////
				if(isUserLoggedIn){
					application.getPopUpManager().showConfirmPopUp(
							null,
							getString(R.string.logoutPopupQuestion),
							getString(R.string.logoutPopupPositiveButton),
							getString(R.string.logoutPopupNegativeButton),
							new OnConfirmPopUpButtonPressedListener() {
								@Override
								public void onPositive() {
									application.setUser(null);
									application.setUserRegulation(null);
									application.postEvent(new LoginLogoutEvent(Type.LOGOUT, null));
								}
								@Override
								public void onNegative() {}
							}
					);
				}else{
					ActionFragment.isInitialLoginRegister = false;
					application.postEvent(new NavigationEvent(Screen.LOGIN, NavigationType.INIT));
				}
			}
		});
		//Set up the button text:
		logoutButton.setText((isUserLoggedIn) ? getString(R.string.settingsScreenLogoutButton) : getString(R.string.settingsScreenLoginButton));
	}

	protected Screenable getLoginScreen(){
		return Screen.LOGIN;
	}

	protected void setupCancelInvestmentButton(){
		if(cancelInvestmentLayout == null){
			cancelInvestmentLayout = rootView.findViewById(R.id.settings_cancel_investment_layout);
		}
		if(application.isLoggedIn()){
			cancelInvestmentLayout.setVisibility(View.VISIBLE);
			cancelInvestmentSwitch = (Switch) rootView.findViewById(R.id.settings_cancel_investment_switch);

			//Set up the Notifications layout:
			cancelInvestmentSwitch.setChecked(application.isShowCancelInvestmentPopup());

			cancelInvestmentSwitch.setStateChangeListener(new StateChangeListener() {
				@Override
				public void onCheckedChange(boolean isChecked) {
					showAgainCancelInvestmentPopup(isChecked);
				}
			});

			cancelInvestmentLayout.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					cancelInvestmentSwitch.toggle();
				}
			});
		}else{
			cancelInvestmentLayout.setVisibility(View.GONE);
		}
	}


	protected void setupNotificationsButton(){
		notificationsLayout = rootView.findViewById(R.id.settings_notifications_layout);
		notificationsSwitch = (Switch) rootView.findViewById(R.id.settings_notifications_switch);
		notificationsSwitch.setStateChangeListener(new StateChangeListener() {
			@Override
			public void onCheckedChange(boolean isChecked) {
				application.getSharedPreferencesManager().putBoolean(Constants.PREFERENCES_IS_NOTIFICATIONS_ON, isChecked);
				application.postEvent(new PushNotificationsPermissionEvent(isChecked));
			}
		});

		notificationsLayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				notificationsSwitch.toggle();
			}
		});

		//Set up the Notifications layout:
		notificationsSwitch.setChecked(isNotificationsOn);
	}

	protected void setupLanguageButton(){
		if(languageLayout == null){
			languageLayout = rootView.findViewById(R.id.settings_language_layout);
			languageSelector = (LanguageSelector)rootView.findViewById(R.id.settings_language_selector);
			languageSelector.setMode(LanguageSelector.MODE_SETTINGS);
			languageLayout.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					languageSelector.performClick();
				}
			});
		}

		//Set up the Language layout visibility:
		if (isUserLoggedIn) {
			// If the user is logged in, we do NOT provide the opportunity to change the language:
			languageLayout.setVisibility(View.GONE);
		} else {
			languageLayout.setVisibility(View.VISIBLE);
		}
	}

	protected void setupResetIntrosButton(){
		resetIntrosButton = rootView.findViewById(R.id.settings_reset_intros_button);
		resetIntrosButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				application.getSharedPreferencesManager().putBoolean(Constants.PREFERENCES_IS_INTROS_ON, false);
				application.getSharedPreferencesManager().putBoolean(Constants.PREFERENCES_IS_TRADE_TOOLTIP_SHOWN, false);

				resetIntros();
			}
		});
	}

	protected void resetIntros(){
		Intent intent = new Intent(application, application.getSubClassForClass(LaunchActivity.class));
		intent.putExtra(Constants.EXTRA_IS_SHOULD_SHOW_WELCOME, true);
		intent.putExtra(Constants.EXTRA_SCREEN, Screen.WELCOME);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		Log.d(TAG, "Start Launch Activity due to reset intros event!");
		application.getCurrentActivity().finish();
		application.getCurrentActivity().startActivity(intent);
	}

	/**
	 * Sets up the UI.
	 */
	protected void setupUI(){

		setupLoginLogoutButton();

		setupCancelInvestmentButton();

		setupNotificationsButton();

		setupLanguageButton();

		setupResetIntrosButton();

		setupAppVersionText();

	}

	@Override
	protected Screenable setupScreen() {
		return Screen.SETTINGS;
	}

	private void showAgainCancelInvestmentPopup(boolean isShowAgain){
		CancelInvestmentCheckboxStateMethodRequest request = new CancelInvestmentCheckboxStateMethodRequest();
		request.setShowCancelInvestment(isShowAgain);

		application.getCommunicationManager().requestService(this, "showAgainCancelInvestmentPopupCallback", Constants.SERVICE_CANCEL_INVESTMENT_SHOW_AGAIN, request, MethodResult.class, true, false);
	}

	public void showAgainCancelInvestmentPopupCallback(Object resObj){
		MethodResult result = (MethodResult) resObj;
		if(result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS){
			application.setShowCancelInvestmentPopup(!application.isShowCancelInvestmentPopup());
		}
	}

}
