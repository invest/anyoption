package com.anyoption.android.app.event;

import com.anyoption.common.beans.base.Investment;

public class InvestmentEvent {

	/**
	 * Investment event type
	 */
	public enum Type {
		INVEST_REQUESTED, INVEST_SUCCESS, INVEST_FAIL, INVESTMENT_FOCUSED, 
		INVESTMENT_UNFOCUSED, INVESTMENT_SOLD, INVESTMENT_SOLD_ALL, INVESTMENT_CHANGE_FOCUS
	}

	private long marketId;
	private Type type;
	private Investment investment;

	public InvestmentEvent(long marketId, Type type, Investment investment) {
		this.marketId = marketId;
		this.type = type;
		this.investment = investment;
	}

	public InvestmentEvent(long marketId, Type type) {
		this(marketId, type, null);
	}

	public long getMarketId() {
		return marketId;
	}

	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	/**
	 * Gets the investment. It will be null if the type isn't {@link Type#INVEST_SUCCESS}.
	 * 
	 * @return investment.
	 */
	public Investment getInvestment() {
		return investment;
	}

	public void setInvestment(Investment investment) {
		this.investment = investment;
	}

}
