package com.anyoption.android.app.fragment.my_account.banking.deposit;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.anyoption.android.app.Application;
import com.anyoption.android.R;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.form.Form;
import com.anyoption.android.app.widget.form.Form.OnSubmitListener;
import com.anyoption.android.app.widget.form.FormCurrencySelector;
import com.anyoption.android.app.widget.form.FormDatePicker;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.android.app.widget.form.FormSelector.ItemSelectedListener;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.json.requests.FirstEverDepositCheckRequest;
import com.anyoption.json.requests.UpdateUserMethodRequest;
import com.anyoption.json.results.FirstEverDepositCheckResult;

import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

/**
 * @author liors
 *
 */
public abstract class BaseDepositFragment extends ReceiptFragment implements ItemSelectedListener<Currency> {
	private static final String TAG = BaseDepositFragment.class.getSimpleName();
	protected Form formBase;
	protected FormEditText addressField;
	protected FormEditText cityField;
	protected FormEditText zipField;
	protected FormDatePicker birthDateField;
	protected FormCurrencySelector currency;
	protected RequestButton submitButton;
	protected boolean isFirstEverDeposit;
	protected boolean isAmountEdited;
	
	/**
	 * 
	 */
	protected abstract void submitDeposit();
	
	public void onCreateViewBase(View view) {
		View rootView = View.inflate(application.getCurrentActivity(), R.layout.deposit_common_fields_layout, (ViewGroup) view);
		init();
		setupCommonFields(rootView);

		formBase = new Form(new OnSubmitListener() {

			@Override
			public void onSubmit() {
				if (isFirstEverDeposit) {
					submitCommonFields();
				} else {
					submitDeposit();
				}
			}
		});
	}
	
	private void init() {
		isSubmitButtonEnabled = false;
		setSubmitButtonEnabled(false);
		FirstEverDepositCheckRequest request = new FirstEverDepositCheckRequest();
		request.setUserId(Application.get().getUser().getId());
		Log.d(TAG, "about to make service call to " + Constants.SERVICE_GET_FIRST_EVER_DEPOSIT_CHECK);
		if(!Application.get().getCommunicationManager().requestService(BaseDepositFragment.this, "firstDepositCallBack", Constants.SERVICE_GET_FIRST_EVER_DEPOSIT_CHECK, request , FirstEverDepositCheckResult.class)){
			isSubmitButtonEnabled = true;
			setSubmitButtonEnabled(true);
		}	
	}
	
	protected void setupCommonFields(View rootView){
		addressField = (FormEditText) rootView.findViewById(R.id.address); 
		addressField.addValidator(ValidatorType.EMPTY);//, ValidatorType.ADDRESS); 
		addressField.setFieldName(Constants.FIELD_STREET);
		
		cityField = (FormEditText) rootView.findViewById(R.id.city); 
		cityField.addValidator(ValidatorType.EMPTY, ValidatorType.LETTERS_AND_SPACE_ONLY);//, ValidatorType.LETTERS); 
		cityField.setFieldName(Constants.FIELD_CITY_NAME);

		cityField.getEditText().setImeOptions(EditorInfo.IME_ACTION_NEXT);

		zipField = (FormEditText) rootView.findViewById(R.id.zip);
		zipField.addValidator(ValidatorType.EMPTY);
		zipField.setFieldName(Constants.FIELD_ZIP_CODE);
		if(application.getUser().getTimeBirthDate() != null){
			zipField.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);
		}else{
			zipField.getEditText().setImeOptions(EditorInfo.IME_ACTION_NEXT);
		}
		zipField.addOnEditorActionListener(new OnEditorActionListener() {
		    @Override
		    public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
		        int result = actionId & EditorInfo.IME_MASK_ACTION;
		        switch(result) {
			        case EditorInfo.IME_ACTION_DONE:
			        	Log.d(TAG, "IME_ACTION_DONE");
			        	ScreenUtils.hideKeyboard(zipField.getEditText());
			            zipField.clearFocus();
			            break;
			        case EditorInfo.IME_ACTION_NEXT:
			        	Log.d(TAG, "IME_ACTION_NEXT");
			        	ScreenUtils.hideKeyboard(zipField.getEditText());
			        	birthDateField.performClick();
			            break;
		        }
		        return true;
		    }
		});
		
		//birth day
		birthDateField = (FormDatePicker) rootView.findViewById(R.id.date_of_birth);
		if (application.getUser().getTimeBirthDate() != null) {
			birthDateField.setEditable(false);
			birthDateField.setValue(new SimpleDateFormat(FormDatePicker.DATE_FORMAT_DAY_MONTH_YEAR, application.getLocale()).format(application.getUser().getTimeBirthDate()));
			birthDateField.addValidator(ValidatorType.EMPTY, ValidatorType.VACUOUS);
		} else {
			birthDateField.addValidator(ValidatorType.EMPTY, ValidatorType.VACUOUS, ValidatorType.BIRTH_DAY);
			birthDateField.setMin18(true);
		}
	}
	
	protected void setupCommonFieldsInForm(){
		if(addressField != null && cityField != null && zipField != null && birthDateField != null){
			if(!isFirstEverDeposit){
				//Remove the unneeded items from the from:
				formBase.removeItem(addressField);
				formBase.removeItem(cityField);
				formBase.removeItem(zipField);
				formBase.removeItem(birthDateField);
			}else{
				formBase.addItem(addressField, cityField, birthDateField, zipField);
			}
		}
	}
	
	public void firstDepositCallBack(Object resultObj) {
		Log.d(TAG, "firstDepositCallBack");
		isSubmitButtonEnabled = true;
		setSubmitButtonEnabled(true);
		FirstEverDepositCheckResult result = (FirstEverDepositCheckResult) resultObj;
		setFirstEverDeposit(false);
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			setFirstEverDeposit(result.isFirstEverDeposit());
			setupCommonFieldsInForm();
		}
	}

	protected void setCurrency(View view, int currencyId) {
		currency = (FormCurrencySelector) view.findViewById(currencyId);
		currency.setEditable(false);
		currency.addItemListener(this);
	}
	
	/**
	 * validateAndUpdate
	 * 
	 * @param methodName
	 */
	public void submitCommonFields() {
		Log.d(TAG, "about to submitCommonFields");
    	User user = new User();
    	setUserCommonFields(user);
		user.setId(application.getUser().getId());
		
		UpdateUserMethodRequest request = new UpdateUserMethodRequest();
		request.setUser(user);
		
		Log.d(TAG, "about to make service call to " + Constants.SERVICE_UPDATE_USER_ADDITIONAL_FIELDS);
		if(!Application.get().getCommunicationManager().requestService(BaseDepositFragment.this, "validateAndUpdateCallBack", Constants.SERVICE_UPDATE_USER_ADDITIONAL_FIELDS, request, UserMethodResult.class, false, false)){
			isSubmitButtonEnabled = true;
			setSubmitButtonEnabled(true);
		}
	}
	
	protected void setUserCommonFields(User user) {
		user.setCurrencyId(currency.getSelectedCurrency().getId());
		user.setCurrency(currency.getSelectedCurrency());
		user.setStreet(addressField.getValue());
		user.setCityName(cityField.getValue());
		user.setZipCode(zipField.getValue());
		if (birthDateField.getValue() != null && !birthDateField.getValue().equalsIgnoreCase("")) {			
			Date timeBirthDate = null;
			try {			
				timeBirthDate = new SimpleDateFormat(FormDatePicker.DATE_FORMAT_DAY_MONTH_YEAR, application.getLocale()).parse(birthDateField.getValue());
			} catch (ParseException e) {			
				timeBirthDate = null;
			}
			user.setTimeBirthDate(timeBirthDate);
		}
	}
	
	public void validateAndUpdateCallBack(Object resultObj) {
		Log.d(TAG, "about to validateAndUpdateCallBack");
		UserMethodResult result = (UserMethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			User user = Application.get().getUser();
			setUserCommonFields(user);
			submitDeposit();
		} else {
			isSubmitButtonEnabled = true;
			setSubmitButtonEnabled(true);
			ErrorUtils.displayFieldError(formBase, result);
			Log.d(TAG, "Error Update user");
		}
	}

	/**
	 * @return the isFirstEverDeposit
	 */
	public boolean isFirstEverDeposit() {
		return isFirstEverDeposit;
	}

	/**
	 * @param isFirstEverDeposit the isFirstEverDeposit to set
	 */
	public void setFirstEverDeposit(boolean isFirstEverDeposit) {
		this.isFirstEverDeposit = isFirstEverDeposit;
	}
	
	@Override
	public void onItemSelected(Currency selectedItem) {
		//Check if the user has edited the amount value:
		if(!isAmountEdited){
			changeDefaultAmountForCurrency(selectedItem);
		}
	}

	protected void changeDefaultAmountForCurrency(Currency currency) {}
	
	
}