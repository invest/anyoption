package com.anyoption.android.app.event;


/**
 * Event that occurs when some open options expired.
 * @author Anastas Arnaudov
 *
 */
public class ExpiredOptionsEvent {

	private double amount = 0;
	private long marketID = 0;
	private String marketName;
	private int expiredOptionsCount = 0;
	private boolean isWinning = false;
	
	public ExpiredOptionsEvent(){}
	
	public ExpiredOptionsEvent(double amount, long marketID, int expiredOptionsCount, boolean isWinning){
		this.amount = amount;
		this.marketID = marketID;
		this.expiredOptionsCount = expiredOptionsCount;
		this.isWinning = isWinning;
	}
	
	
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public long getMarketID() {
		return marketID;
	}
	public void setMarketID(long marketID) {
		this.marketID = marketID;
	}
	public String getMarketName() {
		return marketName;
	}
	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}
	public int getExpiredOptionsCount() {
		return expiredOptionsCount;
	}
	public void setExpiredOptionsCount(int expiredOptionsCount) {
		this.expiredOptionsCount = expiredOptionsCount;
	}
	public boolean isWinning() {
		return isWinning;
	}
	public void setWinning(boolean isWinning) {
		this.isWinning = isWinning;
	}
	
	
	
}
