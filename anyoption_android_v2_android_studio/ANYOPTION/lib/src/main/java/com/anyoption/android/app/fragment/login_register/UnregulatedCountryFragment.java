package com.anyoption.android.app.fragment.login_register;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.fragment.NavigationalFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * {@link BaseFragment} that represents the screen that shows after the user tries to register from Unregulated country.
 * @author Anastas Arnaudov
 *
 */
public class UnregulatedCountryFragment extends NavigationalFragment {

	public static final String TAG = UnregulatedCountryFragment.class.getSimpleName();
	
	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static UnregulatedCountryFragment newInstance(){	
		return newInstance(new Bundle());
	}
	
	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static UnregulatedCountryFragment newInstance(Bundle bundle){
		UnregulatedCountryFragment unregulatedCountryFragment = new UnregulatedCountryFragment();	
		unregulatedCountryFragment.setArguments(bundle);
		return unregulatedCountryFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.unregulated_country_fragment_layout, container, false);
	
		return rootView;
	}
	
	@Override
	protected Screenable setupScreen() {
		return Screen.UNREGULATED_COUNTRY;
	}

}
