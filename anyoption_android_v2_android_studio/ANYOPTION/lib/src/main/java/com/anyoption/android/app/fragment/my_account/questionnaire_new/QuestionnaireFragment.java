package com.anyoption.android.app.fragment.my_account.questionnaire_new;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.model.Answer;
import com.anyoption.android.app.model.Question;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;

import com.anyoption.android.app.widget.Button;
import com.anyoption.android.app.widget.questionnaire.QuestionnaireBaseView;
import com.anyoption.android.app.widget.questionnaire.QuestionnaireCheckBoxAnswerView;
import com.anyoption.android.app.widget.questionnaire.QuestionnairePopupAndTextFieldAnswerView;
import com.anyoption.android.app.widget.questionnaire.QuestionnairePopupAnswerView;
import com.anyoption.android.app.widget.questionnaire.QuestionnaireTextFieldAnswerView;
import com.anyoption.common.beans.base.QuestionnaireAnswer;
import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.json.requests.UpdateUserQuestionnaireAnswersRequest;
import com.anyoption.json.results.UserQuestionnaireResult;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class QuestionnaireFragment extends NavigationalFragment implements QuestionnaireBaseView.OnAnswerSelectedListener {

    private final String TAG = QuestionnaireFragment.class.getSimpleName();

    private static List<QuestionnaireUserAnswer> userAnswers;
    private static List<QuestionnaireQuestion> questions;
    private int currentPage = 1;

    private Map<Long, QuestionnaireBaseView> questionsViewsMap = new HashMap<Long, QuestionnaireBaseView>();

    private boolean isLoading = true;
    private View loadingView;
    private View rootView;
    private ScrollView scrollView;
    private Button submitButton;

    private boolean areQuestionsListFilled = false;
    private boolean areQuestionsViewsCreated = false;

    private static boolean isQuestionnaireButtonClicked = false;


    public static QuestionnaireFragment newInstance(Bundle bundle) {
        QuestionnaireFragment fragment = new QuestionnaireFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            currentPage = getArguments().getInt(Constants.EXTRA_QUESTIONNAIRE_PAGE, 1);
        } else {
            currentPage = 1;
        }

        getQuestionnaire();
    }

    @Override
    protected Application.Screenable setupScreen() {
        return Application.Screen.QUESTIONNAIRE;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(getQuestionnaireLayout(), container, false);

        loadingView = rootView.findViewById(R.id.loadingView);
        scrollView = (ScrollView) rootView.findViewById(R.id.scrollView);

        isQuestionnaireButtonClicked = false;

        isLoading = true;
        setupLoading();

        if (currentPage == 1) {
            TextView headerTextView = (TextView) rootView.findViewById(R.id.questionnaire_1_header_text_view);
            TextUtils.decorateInnerText(headerTextView, null, headerTextView.getText().toString(), getString(R.string.questionnaireScreen1HeaderNote), FontsUtils.Font.ROBOTO_BOLD, 0, 0);
        }

        submitButton = (Button) rootView.findViewById(R.id.questionnaire_button);

        submitButton.setVisibility(View.GONE);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isQuestionnaireButtonClicked) {
                    isQuestionnaireButtonClicked = true;
                    submitPage();
                }
            }
        });

        if(areQuestionsListFilled && !areQuestionsViewsCreated) {
            loadQuestions();

            setupSubmitButton();
        }

        return rootView;
    }

    private void loadQuestions() {
        if (rootView != null) {
            LinearLayout questionsLayout = (LinearLayout) rootView.findViewById(R.id.questions_layout);

            insertQuestions(currentPage, questionsLayout);

            scrollView.fullScroll(View.FOCUS_UP);

            areQuestionsViewsCreated = true;
        }
    }

    private void submitPage() {
        boolean isFormValid = true;

        for (Map.Entry<Long, QuestionnaireBaseView> entry : questionsViewsMap.entrySet()) {
            QuestionnaireBaseView item = entry.getValue();
            if (!item.isAnswered()) {
                isFormValid = false;
                item.showError();
            }
        }

        if (isFormValid) {
            updateQuestionnaire();
        }
    }

    @Override
    public void onAnswerSelected(Question question, Answer answer) {
        isQuestionnaireButtonClicked = false;

        updateUserAnswers();
        updateQuestionnaire();
    }

    private void setupLoading() {
        if (loadingView != null) {
            if (isLoading) {
                loadingView.setVisibility(View.VISIBLE);
            } else {
                loadingView.setVisibility(View.GONE);
            }
        }
    }

    private int getQuestionnaireLayout() {
        if (currentPage == 1) {
            return R.layout.questionnaire_1_fragment;
        } else if (currentPage > 1) {
            return R.layout.questionnaire_2_fragment;
        }

        return R.layout.questionnaire_2_fragment;
    }

    private void setupSubmitButton() {
        int numberOfPages = getNumberOfPages();

        if (submitButton != null) {

            if (currentPage == numberOfPages) {
                submitButton.setText(getString(R.string.questionnaireScreen2SubmitButton));
            } else if (currentPage < numberOfPages) {
                submitButton.setText(getString(R.string.questionnaireScreen1NextButton));
            } else {
                submitButton.setText(getString(R.string.questionnaireScreen2SubmitButton));
            }

            submitButton.setVisibility(View.VISIBLE);
        }
    }

    private void enableQuestions(boolean isEnabled) {
        if (!questionsViewsMap.isEmpty()) {
            for (Map.Entry<Long, QuestionnaireBaseView> entry : questionsViewsMap.entrySet()) {
                QuestionnaireBaseView item = entry.getValue();
                item.setEnabled(isEnabled);
            }
        }
    }

    private void updateQuestionsSelectors() {
        if (userAnswers != null && !questionsViewsMap.isEmpty()) {
            for (QuestionnaireUserAnswer answer : userAnswers) {
                QuestionnaireBaseView formQuestionSelector = questionsViewsMap.get(answer.getQuestionId());

                if (formQuestionSelector != null) {
                    Question question = formQuestionSelector.getQuestion();
                    Answer value = question.getAnswerForId(answer.getAnswerId());

                    if (value != null) {
                        if (formQuestionSelector.getQuestionType() == Question.QuestionType.POPUP_WITH_TEXT_FIELD) {
                            value.setTextAnswer(answer.getTextAnswer() != null ? answer.getTextAnswer() : "");
                        } else if (formQuestionSelector.getQuestionType() == Question.QuestionType.TEXT_FIELD) {
                            value.setTextAnswer(answer.getTextAnswer() != null ? answer.getTextAnswer() : "");
                        }
                        formQuestionSelector.setAnswer(value);
                    }
                }
            }
        }
    }

    private void updateUserAnswers() {
        if (userAnswers != null) {
            for (QuestionnaireUserAnswer answer : userAnswers) {
                QuestionnaireBaseView formQuestionSelector = questionsViewsMap.get(answer.getQuestionId());

                if (formQuestionSelector != null) {
                    Answer value = formQuestionSelector.getAnswer();

                    if (value != null) {
                        answer.setAnswerId(value.getId());

                        if (formQuestionSelector.getQuestionType() == Question.QuestionType.POPUP_WITH_TEXT_FIELD) {
                            answer.setTextAnswer(value.getTextAnswer() != null ? value.getTextAnswer() : "");
                        } else if (formQuestionSelector.getQuestionType() == Question.QuestionType.TEXT_FIELD) {
                            answer.setTextAnswer(value.getTextAnswer() != null ? value.getTextAnswer() : "");
                        }
                    }
                }
            }
        }
    }

    private int getNumberOfPages() {
        int numberOfPages = 0;
        if (questions != null) {
            for (QuestionnaireQuestion question : questions) {
                if (question.getScreen() > numberOfPages) {
                    numberOfPages = question.getScreen();
                }
            }
        }

        return numberOfPages;
    }

    private Question getQuestion(QuestionnaireQuestion questionnaireQuestion) {
        List<QuestionnaireAnswer> questionnaireAnswers = questionnaireQuestion.getAnswers();

        Question question = new Question();
        question.setId(questionnaireQuestion.getId());
        question.setDescription(questionnaireQuestion.getTranslatation());

        Answer[] answers = new Answer[questionnaireAnswers.size()];

        boolean hasPopupInputAnswer = false;
        Answer validationAnswer = null;

        for (int currentAnswerNumber = 0; currentAnswerNumber < questionnaireAnswers.size(); currentAnswerNumber++) {
            Answer answer = new Answer();
            answer.setId(questionnaireAnswers.get(currentAnswerNumber).getId());
            answer.setQuestion(question);
            answer.setDescription(questionnaireAnswers.get(currentAnswerNumber).getTranslation());

            answers[currentAnswerNumber] = answer;

            if (questionnaireAnswers.get(currentAnswerNumber).getAnswerType() == QuestionnaireAnswer.UserAnswerType.FREETEXT) {
                hasPopupInputAnswer = true;
                validationAnswer = answer;
            }
        }

        if (questionnaireQuestion.getQuestionType() == QuestionnaireQuestion.QuestionType.DROPDOWN) {
            if (hasPopupInputAnswer) {
                question.setQuestionType(Question.QuestionType.POPUP_WITH_TEXT_FIELD);
                question.setValidationAnswer(validationAnswer);
            } else {
                question.setQuestionType(Question.QuestionType.POPUP);
            }
        } else if (questionnaireQuestion.getQuestionType() == QuestionnaireQuestion.QuestionType.CHECKBOX) {
            question.setQuestionType(Question.QuestionType.CHECKBOX);

            //TODO NOT IN USE
            /*if(question.isMandatory()) {
                validationAnswer = answers[0];

                question.setValidationAnswer(validationAnswer);
            }*/
        } else if (questionnaireQuestion.getQuestionType() == QuestionnaireQuestion.QuestionType.INPUT) {
            question.setQuestionType(Question.QuestionType.TEXT_FIELD);
        }

        question.setMandatory(questionnaireQuestion.isMandatory());

        question.setAnswers(answers);

        return question;
    }

    private void insertQuestionView(LinearLayout linearLayout, QuestionnaireQuestion questionnaireQuestion) {
        Question question = getQuestion(questionnaireQuestion);

        if (question.getQuestionType() == Question.QuestionType.POPUP) {
            QuestionnairePopupAnswerView questionnaireItem = new QuestionnairePopupAnswerView(application);

            questionnaireItem.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            linearLayout.addView(questionnaireItem);

            questionnaireItem.setQuestion(question);
            questionnaireItem.setAnswerSelectedListener(this);

            questionsViewsMap.put(questionnaireQuestion.getId(), questionnaireItem);
        } else if (question.getQuestionType() == Question.QuestionType.POPUP_WITH_TEXT_FIELD) {
            QuestionnairePopupAndTextFieldAnswerView questionnaireItem = new QuestionnairePopupAndTextFieldAnswerView(application);

            questionnaireItem.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            linearLayout.addView(questionnaireItem);

            questionnaireItem.setQuestion(question);
            questionnaireItem.setAnswerSelectedListener(this);

            questionsViewsMap.put(questionnaireQuestion.getId(), questionnaireItem);
        } else if (question.getQuestionType() == Question.QuestionType.CHECKBOX) {
            QuestionnaireCheckBoxAnswerView questionnaireItem = new QuestionnaireCheckBoxAnswerView(application);

            questionnaireItem.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            linearLayout.addView(questionnaireItem);

            questionnaireItem.setQuestion(question);
            questionnaireItem.setAnswerSelectedListener(this);

            questionsViewsMap.put(questionnaireQuestion.getId(), questionnaireItem);
        } else if (question.getQuestionType() == Question.QuestionType.TEXT_FIELD) {
            QuestionnaireTextFieldAnswerView questionnaireItem = new QuestionnaireTextFieldAnswerView(application);

            questionnaireItem.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            linearLayout.addView(questionnaireItem);

            questionnaireItem.setQuestion(question);
            questionnaireItem.setAnswerSelectedListener(this);

            questionsViewsMap.put(questionnaireQuestion.getId(), questionnaireItem);
        }
    }

    private void insertQuestions(int screenNumber, LinearLayout questionsLayout) {
        for (QuestionnaireQuestion questionnaireQuestion : questions) {
            if (questionnaireQuestion.getScreen() == screenNumber) {
                insertQuestionView(questionsLayout, questionnaireQuestion);
            }


        }

        updateQuestionsSelectors();

        if (userAnswers == null) {
            enableQuestions(false);
        } else {
            enableQuestions(true);
        }

        isLoading = false;
        setupLoading();
    }

    ///////////				REQUESTs and RESPONSEs			////////////////////
    private void getQuestionnaire() {
        Log.d(TAG, "getQuestionnaire");
        if (application.getCommunicationManager().requestService(this, "getQuestionnaireCallback", Constants.SERVICE_GET_SINGLE_QUESTIONNAIRE_ANSWERS_DYNAMIC, new UserMethodRequest(), UserQuestionnaireResult.class, false, false)) {
        }
    }

    public void getQuestionnaireCallback(Object resultObj) {
        Log.d(TAG, "getQuestionnaireCallback");

        UserQuestionnaireResult result = (UserQuestionnaireResult) resultObj;
        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            Log.d(TAG, "Got user saved userAnswers.");
            userAnswers = result.getUserAnswers();
            questions = result.getQuestions();

            areQuestionsListFilled = true;

            if(submitButton != null) {
                loadQuestions();

                setupSubmitButton();
            }

            if (RegulationUtils.isQuestionnaireFilled()) {
                goToMain();
            }
        } else {
            Log.e(TAG, "Getting user userAnswers failed");
        }
    }

    private void updateQuestionnaire() {
        Log.d(TAG, "updateQuestionnaire");
        UpdateUserQuestionnaireAnswersRequest request = new UpdateUserQuestionnaireAnswersRequest();
        updateUserAnswers();
        request.setUserAnswers(userAnswers);

        if (application.getCommunicationManager().requestService(this, "updateQuestionnaireCallback", Constants.SERVICE_UPDATE_QUESTIONNAIRIE_ANSWERS, request, UserMethodResult.class, false, false)) {
        }
    }

    public void updateQuestionnaireCallback(Object resultObj) {
        Log.d(TAG, "updateQuestionnaireCallback");
        UserMethodResult result = (UserMethodResult) resultObj;
        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            Log.d(TAG, "updateQuestionnaireCallback OK.");
            application.setUserRegulation(result.getUserRegulation());
            UpdateUserQuestionnaireAnswersRequest request = new UpdateUserQuestionnaireAnswersRequest();
            updateUserAnswers();
            request.setUserAnswers(userAnswers);

            if (isQuestionnaireButtonClicked) {
                if (currentPage == getNumberOfPages()) {
                    if (application.getCommunicationManager().requestService(this, "submitQuestionnaireCallback", Constants.SERVICE_UPDATE_SINGLE_QUESTIONNAIRIE, request, UserQuestionnaireResult.class, false, false)) {
                    }
                } else {
                    goToNextPage();
                }
            }
        } else if (result != null && result.getErrorCode() == Constants.ERROR_CODE_QUESTIONNAIRE_ALREADY_DONE) {
            goToMain();
        } else {
            Log.e(TAG, "updateQuestionnaireCallback failed");
            ErrorUtils.handleResultError(result);

            isQuestionnaireButtonClicked = false;
        }
    }

    public void submitQuestionnaireCallback(Object resultObj) {
        Log.d(TAG, "submitQuestionnaireCallback");
        UserMethodResult result = (UserMethodResult) resultObj;
        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            Log.d(TAG, "submitQuestionnaireCallback OK.");
            application.setUserRegulation(result.getUserRegulation());
            boolean goToMain = false;
            if (RegulationUtils.showBlockedOrPEPOrRestrictedPopups()) {
                if (RegulationUtils.showAccountBlocked()) {
                    if (RegulationUtils.showUploadDocuments()) {
                        goToMain = true;
                    }
                }
            } else {
                goToMain = true;
            }

            if (goToMain) {
                goToMain();
            }
        } else if (result != null && result.getErrorCode() == Constants.ERROR_CODE_QUESTIONNAIRE_ALREADY_DONE) {
            goToMain();
        } else if (result != null && result.getErrorCode() == Constants.ERROR_CODE_REG_SUSPENDED_QUEST_INCORRECT) {
            application.setUserRegulation(result.getUserRegulation());
            RegulationUtils.showBlockedPopup();
            goToMain();
        } else if (result != null && result.getErrorCode() == Constants.ERROR_CODE_REGULATION_USER_PEP_BLOCKED) {
            application.setUserRegulation(result.getUserRegulation());
            RegulationUtils.showPEPPopup();
            goToMain();
        } else if (result != null && result.getErrorCode() == Constants.ERROR_CODE_REGULATION_USER_RESTRICTED) {
            application.setUserRegulation(result.getUserRegulation());
            RegulationUtils.showRestrictedPopup();
            goToMain();
        } else if (result != null && result.getErrorCode() == Constants.ERROR_CODE_REGULATION_SUSPENDED_DOCUMENTS) {
            application.setUserRegulation(result.getUserRegulation());
            if (RegulationUtils.showAccountBlocked()) {
                goToMain();
            }
        } else if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SHOW_WARNING_DUE_DOCUMENTS) {
            application.setUserRegulation(result.getUserRegulation());
            if (RegulationUtils.showUploadDocuments()) {
                goToMain();
            }
        } else {
            Log.e(TAG, "submitQuestionnaireCallback failed");
            ErrorUtils.handleResultError(result);

            isQuestionnaireButtonClicked = false;
        }
    }

    protected void goToMain() {
        application.postEvent(new NavigationEvent(application.getHomeScreen(), NavigationType.INIT));
    }

    private void goToNextPage() {
        if (currentPage < getNumberOfPages()) {
            Bundle nextPageBundle = new Bundle();
            nextPageBundle.putInt(Constants.EXTRA_QUESTIONNAIRE_PAGE, currentPage + 1);
            application.postEvent(new NavigationEvent(Application.Screen.QUESTIONNAIRE, NavigationType.DEEP, nextPageBundle));
        }

        isQuestionnaireButtonClicked = false;
    }
    //////////////////////////////////////////////////////////////////////////////

}
