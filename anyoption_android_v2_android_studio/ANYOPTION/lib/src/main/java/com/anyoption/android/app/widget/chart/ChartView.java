package com.anyoption.android.app.widget.chart;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.ViewParent;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.InvestmentEvent;
import com.anyoption.android.app.event.InvestmentEvent.Type;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.anyoption.android.app.model.ChartMarketRate;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.json.results.ChartDataMethodResult;
import com.lightstreamer.ls_client.UpdateInfo;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class ChartView extends BaseInteractiveView implements ChartDrawable {

	private static final String TAG = ChartView.class.getSimpleName();
    
	private static final int DEFAULT_MAX_ZOOM_MINUTES = 5;
	private static final int NO_ZOOM_PERCENT = 100;
	private static final float MIN_CHART_SIZE_DIP = 100;
	private static final float CHART_PADDING_DIP = 2f;
	
    private static final float DEFAULT_LEVEL_TEXT_SIZE_SP = 15f;
    private static final float DEFAULT_LEVEL_POINTER_TEXT_SIZE_SP = 12f;
    private static final float DEFAULT_CURRENT_LEVEL_LINE_THICKNESS_DIP = 1f;
    private static final float DEFAULT_CURRENT_LEVEL_TEXT_OFFSET_DIP = 10f;
    private static final float INVESTMENT_HEIGHT_DIP = 20f;
    
    private static final float INVESTMENT_TRIANGLE_WIDTH_DIP = 50f;
    private static final float INVESTMENT_TRIANGLE_HEIGHT_DIP = 25f;
    private static final float INVESTMENT_LINE_WIDTH = 2;
    
    // Constants used for events related with the investments
	private static final float INVESTMENT_CLICK_RADIUS_DIP = 24f;
	private static final long MAX_CLICK_TOUCH_DOWNTIME_MS = 100;
	private static final float CHART_SWIPE_MIN_WIDTH_DIP = 80f;
	private static final float INVESTMENT_SWIPE_MIN_WIDTH_DIP = 80f;
	private static final float CLICKED_INVESTMENT_PADDING_DIP = 60f;
    
    private static final float MIN_MAX_LEVEL_PADDING_TOP_BOTTOM = 10f;
    private static final float MIN_MAX_LEVEL_PADDING_LEFT_RIGHT = 8f;
    
    private static final float CURRENT_LEVEL_POINTER_BACKGR_PADDING_DIP = 2f;
    
    private static final float DEFAULT_LEFT_RIGHT_WIDTH_TOUCH_PARENT_ENABLED_DIP = 50f;
    
    private static final String HOUR_FORMAT = "HH:mm";
    private static final String HOUR_LABEL_STRING_FORMAT = "00:00";
    private static final float HOUR_TEXT_SIZE_SP = 12f;
    private static final float HOURS_LINE_WIDTH_DIP = 5f;
    
    private static final int HOURS_LINE_COLOR_TIME_PAST = Color.parseColor("#20000000");
    private static final float HOURS_VERTICAL_LINES_WIDTH_DIP = 5f;
    private static final float HOUR_LABEL_PADDING_DIP = 2f;

	private static final int LENGTH_IN_MS_REGULAR = 60 * 60 * 1000;
	private static final int LENGTH_IN_MS_OPTION_PLUS = 30 * 60 * 1000;
	private static final int HOUR_IN_MS = 60 * 60 * 1000;

	private static final long LAST_TEN_MINUTES_TO_INVEST_LENGTH_MS = 10 * 60 * 1000;
	
	// This is space for 25 rates
	private static final int BUFFER_ADDITIONAL_SPACE = 200;

	private Application application;
	private Context context;
	
	protected double currentLevel;
    protected long currentLevelTime;
    private String currentLevelText;

	private long startTime;
	private long endTime;
	private int scheduled;

	/**
	 * The time interval in milliseconds
	 */
	private long timeLength;

	private float minLevel;
	private float maxLevel;
	private String minLevelText;
	private String maxLevelText;
	private float minMaxLevelTopBottomPadding;
	private float minMaxLevelLeftRightPadding;
		
	// Used to scale the chart y values depending on the level.
	private float yScale;
	// Used to scale the chart x values depending on the time in seconds.
	private double xScale;
	private float chartPaddingPx;
	private float chartBottomOffset;

    // Current attribute values and Paints.
    private float levelTextSize;
    private int levelTextColor;
    private Paint levelTextPaint;
    
    private float currentLevelTextSize;
    private Paint currentLevelTextPaint;
	private Rect currentLevelTextBounds;
	private float currentLevelTextOffset;
	private RectF currentLevelTextDrawnBounds = new RectF();

	private Paint currentLevelPointerTextPaint;
    private float currentLevelPointerTextSize;
    private int currentLevelPointerTextColor;
    private Rect currentLevelPointerTextBounds;
    private float currentLevelPointerTextPadding;
    private Bitmap currentLevelPointerLeftBitmap;
    private Bitmap currentLevelPointerRightBitmap;
    private Rect currentLevelPointerDest;
    private Bitmap currentLevelBitmap;
    
    private int currentLevelPointerExtraPadding;
    private int currentLevelPointerArrowExtraPadding;
    
    private float leftRightTouchParentWidhtPx;
    
    private float currentLevelLineThickness;
    private Paint currentLevelLinePaint;
    
    private int levelTextHeight;
    private float dataThickness;
    private int dataColor;
    private Paint dataPaint;

    // Buffers used during chart drawing. These are defined as fields to avoid allocation during
    // draw calls.
    private float[] chartPointsBuffer;
	private int drawnRatesOffset;
	private int drawnRatesCount;
	
	// Fields used for hour labels drawing.
	private SimpleDateFormat timeFormat;
	private Date timeTempDate;
	
	/**
	 * The bounds of an hour label. Used to calculate how much space needs the label.
	 */
	private int hourLabelColor;
	private Rect hourLabelBounds;
    private Paint hourLabelPaint;
    private float hourLabelPadding;
    
    private int timelineOpenColor;
    private int timelineClosingColor;
    private int timelineExpiringColor;
    private Paint timelinePaint;
    private float timelineWidthPx;

	// Investment bitmaps
	protected Bitmap investmentWinCallBitmap;
	protected Bitmap investmentWinPutBitmap;
	protected Bitmap investmentLoseCallBitmap;
	protected Bitmap investmentLosePutBitmap;
	private Bitmap investmentEllipseBitmap;
	private Bitmap investmentTriangleBitmap;

    private float investmentHeight;
    
    private Rect investmentTriangleDest;
    private float investmentTriangleWidth;
    private float investmentTriangleHeight;
	
    private Paint investmentPaint;
	private Path investmentLinePath;
	
    /**
     * A list containing the positions for the investments.
     */
    private ArrayList<PointF> investmentPoints;
    private int clickedInvestmentIndex;
	private float investmentClickRadius;
    
    private PointF startChartSwipePoint = new PointF();
    private float chartSwipeMinWidth;
    private PointF startInvestmentSwipePoint = new PointF();
    private float investmentSwipeMinWidth;
    private float clickedInvestmentPadding;
    
	/**
	 * Drawable for the repeatable image for hours' vertical lines.
	 */
	private BitmapDrawable hoursVerticalLines;
	/**
	 * Bounds for the vertical lines under the hours.
	 */
	private Rect hoursVerticalLinesBounds;
	private float hoursVerticalLinesWidthPx;
	
    
	protected boolean portrait;
	
	private Bitmap stripesBitmap;
	private Rect stripesDest;

	private long marketId;
	private SkinGroup skinGroup;
	private long oppId;
	private long oppTypeId;
	private int оppState;

	private Date timeEstClosing;
	private ArrayList<ChartMarketRate> rates;
	private boolean ratesLoaded;

	protected Date timeFirstInvest;
	protected Date timeLastInvest;
	private ArrayList<Investment> investments;
	protected boolean investmentsLoaded;
	private int decimalPointDigits;
	private DecimalFormat levelFormat;

	private int maxZoomMinutes;

	private boolean isAvailable;
	private Bitmap spikesBitmap;

	// Flags that indicates which should be drawn:
	private boolean isSimpleChart = false;
	
	private boolean shouldDrawChart = true;
	private boolean shouldDrawMaxLevel = true;
	private boolean shouldDrawMinLevel = true;
	private boolean shouldDrawCurrentLevel = true;
	private boolean shouldDrawTimeLine = true;
	private boolean shouldDrawInvestments = true;
	private boolean shouldDrawStripes = true;
	private boolean isZoomableVertically = false;
	private boolean isZoomableHorizontally = true;
	private boolean isPageSlideEnabled = false;
	
	private OnScrollToLeftEdgeListener onScrollToLeftEdgeListener;

	private boolean isAboveJellyBean;
	private StringBuilder lsStringBuilder;

    public ChartView(Context context) {
        this(context, null, 0);
    }

    public ChartView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    @SuppressLint("SimpleDateFormat")
	public ChartView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if(!isInEditMode()){

	        this.context = context;
	        application = Application.get();
	        Skin skin = application.getSkins().get(application.getSkinId());
			skinGroup = skin.getSkinGroup();
	        
	        TypedArray a = context.getTheme().obtainStyledAttributes(
	                attrs, R.styleable.ChartView, defStyle, defStyle);
	
	        try {
	            levelTextColor = a.getColor(R.styleable.ChartView_levelTextColor, Color.WHITE);
	            levelTextSize = a.getDimension(R.styleable.ChartView_levelTextSize, 
	            		ScreenUtils.convertSpToPx(context, DEFAULT_LEVEL_TEXT_SIZE_SP));
	            currentLevelTextSize = a.getDimension(R.styleable.ChartView_currentLevelTextSize, 
	            		ScreenUtils.convertSpToPx(context, DEFAULT_LEVEL_TEXT_SIZE_SP));
	            currentLevelPointerTextSize = ScreenUtils.convertSpToPx(context, DEFAULT_LEVEL_POINTER_TEXT_SIZE_SP);
	            currentLevelPointerTextColor = ColorUtils.getColor(R.color.blue_dark_1);
	            currentLevelLineThickness = a.getDimension(R.styleable.ChartView_currentLevelLineThickness, 
	            		ScreenUtils.convertDpToPx(context, DEFAULT_CURRENT_LEVEL_LINE_THICKNESS_DIP));
	            dataThickness = a.getDimension(R.styleable.ChartView_dataThickness, dataThickness);
	            dataColor = a.getColor(R.styleable.ChartView_dataColor, Color.WHITE);
	            isSimpleChart = a.getBoolean(R.styleable.ChartView_isSimpleChart, false);
	            isZoomableVertically = a.getBoolean(R.styleable.ChartView_isZoomableVertically, false);
	            isZoomableHorizontally = a.getBoolean(R.styleable.ChartView_isZoomableHorizontally, true);
	            isPageSlideEnabled = a.getBoolean(R.styleable.ChartView_enablePageSlide, false);

				hourLabelColor = a.getColor(R.styleable.ChartView_timeTextColor, Color.WHITE);
				timelineOpenColor	 = a.getColor(R.styleable.ChartView_timelineOpenColor, Color.parseColor("#51c4f5"));
				timelineClosingColor = a.getColor(R.styleable.ChartView_timelineClosingColor, Color.parseColor("#e64210"));
				timelineExpiringColor = a.getColor(R.styleable.ChartView_timelineExpiringColor, Color.parseColor("#c2e0f2"));

				int currentLevelDrawableId = a.getResourceId(R.styleable.ChartView_currentLevelDot, 0);
				if (currentLevelDrawableId != 0) {
					currentLevelBitmap = BitmapFactory.decodeResource(getResources(), currentLevelDrawableId);
				}

				if (currentLevelBitmap == null) {
					currentLevelBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.dot_glowing_white);
				}
	        } finally {
	            a.recycle();
	        }

			if (isSimpleChart) {
				shouldDrawChart = true;
				shouldDrawCurrentLevel = true;
				shouldDrawInvestments = false;
				shouldDrawMaxLevel = false;
				shouldDrawMinLevel = false;
				shouldDrawTimeLine = false;
				shouldDrawStripes = false;
			}

	        reset();
	
	        maxZoomMinutes = DEFAULT_MAX_ZOOM_MINUTES;
	        enableZoom(false);
	        
	        timelineWidthPx = ScreenUtils.convertDpToPx(context, HOURS_LINE_WIDTH_DIP);
	        hourLabelPadding = ScreenUtils.convertDpToPx(context, HOUR_LABEL_PADDING_DIP);
	        
	        // Set Horizontal ZOOM:
	        setHorizontalInteractionEnabled(isZoomableHorizontally);
	        
	        // Set Vertical ZOOM:
	        setVerticalInteractionEnabled(isZoomableVertically);
	        
			initPaints();
	
			timeFormat = new SimpleDateFormat(HOUR_FORMAT);
			timeTempDate = new Date();
			levelFormat = Utils.getDecimalFormat(decimalPointDigits);

			leftRightTouchParentWidhtPx = ScreenUtils.convertDpToPx(context, DEFAULT_LEFT_RIGHT_WIDTH_TOUCH_PARENT_ENABLED_DIP);
	
			stripesBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.stripes);
			stripesDest = new Rect();
			currentLevelPointerLeftBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.current_level_bg_arrow_left);
			currentLevelPointerRightBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.current_level_bg_arrow_right);
			currentLevelPointerDest = new Rect();
			currentLevelPointerTextPadding = ScreenUtils.convertDpToPx(context, CURRENT_LEVEL_POINTER_BACKGR_PADDING_DIP);
			currentLevelTextOffset = ScreenUtils.convertDpToPx(context, DEFAULT_CURRENT_LEVEL_TEXT_OFFSET_DIP);
			currentLevelBitmap.setDensity(Bitmap.DENSITY_NONE);
			
			spikesBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.spikes);
			
			hoursVerticalLinesBounds = new Rect();
	        hoursVerticalLines = (BitmapDrawable) DrawableUtils.getDrawable(R.drawable.spikes);
	        
	        hoursVerticalLines.setTileModeX(TileMode.REPEAT);
	        hoursVerticalLines.setBounds(hoursVerticalLinesBounds);
	        hoursVerticalLinesWidthPx = ScreenUtils.convertDpToPx(context, HOURS_VERTICAL_LINES_WIDTH_DIP);
	        
	        chartPaddingPx = ScreenUtils.convertDpToPx(context, CHART_PADDING_DIP);
			chartBottomOffset = timelineWidthPx + hoursVerticalLinesWidthPx + hourLabelPadding + hourLabelBounds.height() + chartPaddingPx;
			
			minMaxLevelTopBottomPadding = ScreenUtils.convertDpToPx(context, MIN_MAX_LEVEL_PADDING_TOP_BOTTOM);
			minMaxLevelLeftRightPadding = ScreenUtils.convertDpToPx(context, MIN_MAX_LEVEL_PADDING_LEFT_RIGHT);
			chartSwipeMinWidth = ScreenUtils.convertDpToPx(context, CHART_SWIPE_MIN_WIDTH_DIP);
			
			isAvailable = true;
			initInvestmentFields();
			rates = new ArrayList<ChartMarketRate>();

			// Make sure we're running on Jelly Bean or higher version
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
				isAboveJellyBean = true;
			} else {
				isAboveJellyBean = false;
			}

			lsStringBuilder = new StringBuilder();
		} else {
			setBackground(DrawableUtils.getDrawable(R.drawable.default_asset_bg));
		}
    }

	private void reset() {
		ratesLoaded = false;
		investmentsLoaded = false;
		оppState = Opportunity.STATE_OPENED;
		decimalPointDigits = 0;
		scheduled = Opportunity.SCHEDULED_HOURLY;
	}

    /**
     * (Re)initializes {@link Paint} objects based on current attribute values.
     */
    private void initPaints() {
    	String simpleLevelText = "###.#####";
    	
        levelTextPaint = new Paint();
        levelTextPaint.setAntiAlias(true);
        levelTextPaint.setTextSize(levelTextSize);
        levelTextPaint.setColor(levelTextColor);
        
        Rect levelBounds = new Rect();
        levelTextPaint.getTextBounds(simpleLevelText, 0, simpleLevelText.length(),levelBounds);
        levelTextHeight = levelBounds.height();

        dataPaint = new Paint();
        dataPaint.setStrokeWidth(dataThickness);
        dataPaint.setColor(dataColor); 
        dataPaint.setStyle(Paint.Style.STROKE);
        dataPaint.setAntiAlias(false);
        
        currentLevelLinePaint = new Paint();
        currentLevelLinePaint.setStrokeWidth(currentLevelLineThickness);
        currentLevelLinePaint.setColor(levelTextColor);
        currentLevelLinePaint.setStyle(Paint.Style.STROKE);
        currentLevelLinePaint.setAntiAlias(true);
        
        currentLevelTextPaint = new Paint(levelTextPaint);
        currentLevelTextPaint.setTextSize(currentLevelTextSize);
        
		currentLevelTextBounds = new Rect();
		currentLevelTextPaint.getTextBounds(simpleLevelText, 0, simpleLevelText.length(),currentLevelTextBounds);

		currentLevelPointerTextPaint = new Paint(levelTextPaint);
		currentLevelPointerTextPaint.setTextSize(currentLevelPointerTextSize);
		currentLevelPointerTextPaint.setColor(currentLevelPointerTextColor);
		currentLevelPointerExtraPadding = (int) ScreenUtils.convertDpToPx(application, 3);
		currentLevelPointerArrowExtraPadding = (int) ScreenUtils.convertDpToPx(application, 8);

		currentLevelPointerTextBounds = new Rect();
		currentLevelPointerTextPaint.getTextBounds(simpleLevelText, 0, simpleLevelText.length(),
				currentLevelPointerTextBounds);
        
        timelinePaint = new Paint();
        timelinePaint.setStrokeWidth(timelineWidthPx);
        timelinePaint.setStyle(Paint.Style.FILL);
        timelinePaint.setAntiAlias(true);
        
        hourLabelPaint = new Paint();
        hourLabelPaint.setAntiAlias(true);
        hourLabelPaint.setTextSize(ScreenUtils.convertSpToPx(context, HOUR_TEXT_SIZE_SP));
        hourLabelPaint.setColor(hourLabelColor);
        
		hourLabelBounds = new Rect();
		hourLabelPaint.getTextBounds(HOUR_LABEL_STRING_FORMAT, 0, HOUR_LABEL_STRING_FORMAT.length(), hourLabelBounds);
		
		investmentPaint = new Paint();
		investmentPaint.setAntiAlias(true);
		investmentPaint.setColor(ColorUtils.getColor(android.R.color.black));
		investmentPaint.setStrokeWidth(INVESTMENT_LINE_WIDTH);
		investmentPaint.setStyle(Style.STROKE);
		investmentPaint.setPathEffect(new DashPathEffect(new float[]{2,6},0));
		
		investmentLinePath = new Path();
    }

	private void initInvestmentFields() {
		initInvestmentIconBitmaps();

		investmentHeight = ScreenUtils.convertDpToPx(context, INVESTMENT_HEIGHT_DIP);

		investmentEllipseBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.circle_glowing_transperant);
		investmentEllipseBitmap.setDensity(Bitmap.DENSITY_NONE);

		investmentTriangleBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.triangle_up_grey);
		investmentTriangleBitmap.setDensity(Bitmap.DENSITY_NONE);
		investmentTriangleDest = new Rect();
		investmentTriangleWidth = ScreenUtils.convertDpToPx(context, INVESTMENT_TRIANGLE_WIDTH_DIP);
		investmentTriangleHeight = ScreenUtils.convertDpToPx(context, INVESTMENT_TRIANGLE_HEIGHT_DIP);

		setInvestments(new ArrayList<Investment>());
		setInvestmentPoints(new ArrayList<PointF>());
		clickedInvestmentIndex = -1;
		investmentClickRadius = ScreenUtils.convertDpToPx(context, INVESTMENT_CLICK_RADIUS_DIP);
		
		investmentSwipeMinWidth = ScreenUtils.convertDpToPx(context, INVESTMENT_SWIPE_MIN_WIDTH_DIP);
	    clickedInvestmentPadding = ScreenUtils.convertDpToPx(context, CLICKED_INVESTMENT_PADDING_DIP);
	}

	/**
	 * Initializes investment bitmaps
	 */
	protected void initInvestmentIconBitmaps() {
		investmentWinCallBitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.chart_call_win);
		investmentWinCallBitmap.setDensity(Bitmap.DENSITY_NONE);
		investmentWinPutBitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.chart_put_win);
		investmentWinPutBitmap.setDensity(Bitmap.DENSITY_NONE);
		investmentLoseCallBitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.chart_call_lose);
		investmentLoseCallBitmap.setDensity(Bitmap.DENSITY_NONE);
		investmentLosePutBitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.chart_put_lose);
		investmentLosePutBitmap.setDensity(Bitmap.DENSITY_NONE);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		if(!isInEditMode()){
			int minChartSize = (int) ScreenUtils.convertDpToPx(context, MIN_CHART_SIZE_DIP);
			setMeasuredDimension(
					Math.max(getSuggestedMinimumWidth(),
							resolveSize(minChartSize + getPaddingLeft() + getPaddingRight(),
									widthMeasureSpec)),
									Math.max(getSuggestedMinimumHeight(),
											resolveSize(minChartSize + getPaddingTop() + getPaddingBottom(),
													heightMeasureSpec)));			
		}
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		if(!isInEditMode()){
			super.onSizeChanged(w, h, oldw, oldh);
			
			// Set bounds for the vertical lines under the hours
			hoursVerticalLinesBounds.left = (int) getDrawX(AXIS_Y_MIN);
			hoursVerticalLinesBounds.top = (int) (contentRect.bottom - timelineWidthPx - hoursVerticalLinesWidthPx);
			hoursVerticalLinesBounds.right = (int) getDrawX(AXIS_X_MAX);
			hoursVerticalLinesBounds.bottom = (int) (contentRect.bottom - timelineWidthPx);
			hoursVerticalLines.setBounds(hoursVerticalLinesBounds);
			hoursVerticalLines.setGravity(Gravity.TOP);			
		}
	}

	/**
	 * Finds and sets {@link #minLevel} and {@link #maxLevel}.
	 */
	private void initRatesCornerValues() {
		Log.d("ChartView", "initRatesCornerValues()");

		if (rates == null || rates.size() == 0) {
			minLevel = -1;
			maxLevel = -1;
			return;
		}

		// Find min and max levels
		minLevel = rates.get(0).getRate();
		maxLevel = minLevel;

		for (ChartMarketRate rate : rates) {
			if (rate.getRate() < minLevel) {
				minLevel = rate.getRate();
			} else if (rate.getRate() > maxLevel) {
				maxLevel = rate.getRate();
			}
		}

		minLevelText = levelFormat.format(minLevel);
		maxLevelText = levelFormat.format(maxLevel);
	}

	private void initScaleValues() {
		if (!ratesLoaded || oppId == 0) {
			return;
		}

		if (maxLevel != -1 && minLevel != -1) {
			xScale = VIEWPORT_MAX_WIDTH / (getTimeLengthInSec());
			yScale = VIEWPORT_MAX_HEIGHT / (maxLevel - minLevel);
		} else {
			xScale = -1;
			yScale = -1;
		}
	}
	  
    ////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //     Methods and objects related to drawing
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	
    @Override
    protected void onDraw(Canvas canvas) {
    	super.onDraw(canvas);
        if(!isInEditMode()){
        	
        	if (!isAvailable) {
        		return;
        	}
        	
        	// Clips the next few drawing operations to the content area
        	int clipRestoreCount = canvas.save();
        	canvas.clipRect(contentRect);
        	
        	if(shouldDrawStripes){
        		drawStripes(canvas);
        	}
        	
        	if (ratesLoaded && oppId != 0 && rates.size() > 0) {
        		if (shouldDrawChart) { drawChart(canvas); }
        		if (shouldDrawMinLevel) { drawMinLevel(canvas); }
        		if (shouldDrawMaxLevel) { drawMaxLevel(canvas); }
        		if (shouldDrawCurrentLevel) { drawCurrentLevel(canvas); }
        		if (shouldDrawTimeLine) { drawTimeline(canvas); }
        		if (shouldDrawInvestments) { drawInvestments(canvas); }
        	}
        	
        	// Removes clipping rectangle
        	canvas.restoreToCount(clipRestoreCount);        	
        }
    }

	private void drawChart(Canvas canvas) {
		if (rates.size() == 0) {
			return;
		}

		drawnRatesOffset = 0;
		drawnRatesCount = 0;

		for (int i = 0; i < rates.size() - 1; i++) {

			// startX
			long startX = rates.get(i).getTime();
			chartPointsBuffer[i * 4 + 0] = getXByTime(startX);
			// startY
			float startY = rates.get(i).getRate();
			chartPointsBuffer[i * 4 + 1] = getYByLevel(startY);
			// endX
			long endX = rates.get(i + 1).getTime();
			chartPointsBuffer[i * 4 + 2] = getXByTime(endX);
			// endY
			float endY = rates.get(i + 1).getRate();
			chartPointsBuffer[i * 4 + 3] = getYByLevel(endY);

			if (chartPointsBuffer[i * 4 + 2] < contentRect.left) {
				// Do not draw the invisible lines to the left of the visible area.
				drawnRatesOffset += 4;
			} else if (chartPointsBuffer[i * 4 + 0] > contentRect.right) {
				// Do not draw the invisible lines to the right of the visible area.
				drawnRatesCount = i * 4 + 0 - drawnRatesOffset;
				break;
			}
		}

		if (drawnRatesCount == 0) {
			drawnRatesCount = chartPointsBuffer.length - drawnRatesOffset;
		}

		canvas.drawLines(chartPointsBuffer, drawnRatesOffset, drawnRatesCount, dataPaint);
	}

	private void drawStripes(Canvas canvas) {
		// Set stripes' bounds
		if (timeLastInvest != null && timeLastInvest.getTime() > 0) {
			stripesDest.left = (int) getXByTime(timeLastInvest.getTime());
		} else {
			stripesDest.left = 3 * getWidth() / 4;
		}
		stripesDest.top = contentRect.top;
		stripesDest.right = contentRect.right;
		stripesDest.bottom = contentRect.bottom;

		// Draw the STRIPES on the right
		canvas.drawBitmap(stripesBitmap, null, stripesDest, null);
	}
	
	private void drawMinLevel(Canvas canvas) {
		if (minLevel != -1) {
			canvas.drawText(minLevelText, contentRect.left + minMaxLevelLeftRightPadding,
					contentRect.bottom - chartBottomOffset - minMaxLevelTopBottomPadding,
					levelTextPaint);
		}
	}

	private void drawMaxLevel(Canvas canvas) {
		if (maxLevel != -1) {
			canvas.drawText(maxLevelText, contentRect.left + minMaxLevelLeftRightPadding,
					contentRect.top + minMaxLevelTopBottomPadding + levelTextHeight,
					levelTextPaint);
		}
	}

	private void drawCurrentLevel(Canvas canvas) {
		if (rates.size() == 0) {
			return;
		}

		float currLevelX = getXByTime(currentLevelTime);
		float currLevelY = getYByLevel((float) currentLevel);

		// Draw current level horizontal line
		canvas.drawLine(contentRect.left, currLevelY, contentRect.right, currLevelY, currentLevelLinePaint);

		float levelTextX = 0;
		float levelTextY = 0;

		//Check whether the current level dot is in the visible part of the chart or not:
		if (contentRect.left - currentLevelBitmap.getWidth() / 2 <= currLevelX && currLevelX <= contentRect.right + currentLevelBitmap.getWidth() / 2) {
			//It is Visible:
			currentLevelTextPaint.getTextBounds(currentLevelText, 0, currentLevelText.length(), currentLevelTextBounds);
			
			levelTextY = currLevelY - currentLevelTextOffset;
			
			//Check if the we are close to the Top edge:
			if (levelTextY < contentRect.top + currentLevelTextBounds.height() + currentLevelTextOffset) {
				// The text will be under the level line.
				levelTextY = currLevelY + currentLevelTextBounds.height() + currentLevelTextOffset;
			}
			
			levelTextX = currLevelX;
		
			if(currLevelX + currentLevelTextBounds.width() >=  contentRect.width()){
				//The text goes
				levelTextX = contentRect.width() - currentLevelTextBounds.width();
			}

			currentLevelTextDrawnBounds.set(levelTextX, levelTextY - currentLevelTextBounds.height(), levelTextX + currentLevelTextBounds.width(), levelTextY);

			canvas.drawText(currentLevelText, levelTextX, levelTextY, currentLevelTextPaint);
			canvas.drawBitmap(currentLevelBitmap, currLevelX - currentLevelBitmap.getWidth()/2 , currLevelY - currentLevelBitmap.getHeight()/2, null);
		
		} else {
			//It is Invisible:
			currentLevelPointerTextPaint.getTextBounds(currentLevelText, 0, currentLevelText.length(), currentLevelPointerTextBounds);
			
			levelTextY = currLevelY + currentLevelPointerTextBounds.height() / 2;
			
			currentLevelPointerDest.top 	= (int) (currLevelY - currentLevelPointerTextBounds.height() / 2 - currentLevelPointerTextPadding);
			currentLevelPointerDest.bottom 	= (int) (currLevelY + currentLevelPointerTextBounds.height() / 2 + currentLevelPointerTextPadding); 
			
			//Check if the we are close to the Top edge:
			if (currentLevelPointerDest.top < contentRect.top) {
				currentLevelPointerDest.top 	= contentRect.top;
				currentLevelPointerDest.bottom 	= (int) (contentRect.top + currentLevelPointerTextBounds.height() + 2*currentLevelPointerTextPadding);
				levelTextY = contentRect.top + currentLevelPointerTextBounds.height() + currentLevelPointerTextPadding;
			}
			
			Bitmap currentLevelPointerBitmap;

			if (currLevelX < contentRect.left - currentLevelBitmap.getWidth() / 2) {
				//The current level is LEFT from the visible part of the chart:
				currentLevelPointerBitmap = currentLevelPointerLeftBitmap;
				currentLevelPointerDest.left = contentRect.left;
				currentLevelPointerDest.right = contentRect.left + currentLevelPointerTextBounds.width() + currentLevelPointerExtraPadding + currentLevelPointerArrowExtraPadding;

				levelTextX = currentLevelPointerDest.left + currentLevelPointerArrowExtraPadding;
			} else {
				//The current level is RIGHT from the visible part of the chart:
				currentLevelPointerBitmap = currentLevelPointerRightBitmap;
				currentLevelPointerDest.left = contentRect.right - currentLevelPointerTextBounds.width() - currentLevelPointerExtraPadding - currentLevelPointerArrowExtraPadding;
				currentLevelPointerDest.right = contentRect.right;
				levelTextX = currentLevelPointerDest.left + currentLevelPointerExtraPadding;
			}

			currentLevelTextDrawnBounds.set(currentLevelPointerDest);
			canvas.drawBitmap(currentLevelPointerBitmap, null, currentLevelPointerDest, null);
			canvas.drawText(currentLevelText, levelTextX, levelTextY, currentLevelPointerTextPaint);
		}

	}

	private void drawInvestments(Canvas canvas) {
		for (int i = 0; i < getInvestments().size(); i++) {
			Investment inv = getInvestments().get(i);

			float investmentX = getXByTime(inv.getTimeCreated().getTime());
			float investmentY = getYByLevel(inv.getCurrentLevel().floatValue());
			getInvestmentPoints().get(i).set(investmentX, investmentY);

			Bitmap investmentBitmap = getInvestmentBitmap(inv);

			// Calculate the left and top of the triangle image:
			float investmentBitmapLeft;
			float investmentBitmapTop;
			
			if (inv.getTypeId() == Investment.INVESTMENT_TYPE_CALL) {
				// Triangle UP:
				investmentBitmapLeft = investmentX - investmentBitmap.getWidth()/2;
				investmentBitmapTop = investmentY;
			} else {
				// Triangle DOWN:
				investmentBitmapLeft = investmentX - investmentBitmap.getWidth()/2;
				investmentBitmapTop = investmentY - investmentBitmap.getHeight();
			}

			canvas.drawBitmap(investmentBitmap, investmentBitmapLeft, investmentBitmapTop, null);

			// Draw the horizontal dashed line for this Investment:
			investmentLinePath.moveTo(getX(), investmentY);
			investmentLinePath.lineTo(getWidth(), investmentY);
			canvas.drawPath(investmentLinePath, investmentPaint);
			investmentLinePath.reset();
			
			////////////////////
			
			// Draw the ellipse and triangle above the Timeline if this investment is clicked.
			if (clickedInvestmentIndex == i) {
				
				float investmentEllipseBitmapLeft;
				float investmentEllipseBitmapTop;
				
				if (inv.getTypeId() == Investment.INVESTMENT_TYPE_CALL) {
					//Triangle UP:
					investmentEllipseBitmapLeft = investmentX - investmentEllipseBitmap.getWidth()/2;
					investmentEllipseBitmapTop = investmentY - investmentEllipseBitmap.getHeight()/2 + investmentBitmap.getHeight()/2;
				} else {
					// Triangle DOWN:
					investmentEllipseBitmapLeft = investmentX - investmentEllipseBitmap.getWidth()/2;
					investmentEllipseBitmapTop = investmentY - investmentEllipseBitmap.getHeight()/2 - investmentBitmap.getHeight()/2;
				}

				canvas.drawBitmap(investmentEllipseBitmap, investmentEllipseBitmapLeft, investmentEllipseBitmapTop, null);
				
				investmentTriangleDest.left = (int) (investmentX - investmentTriangleWidth / 2);
				investmentTriangleDest.top = (int) (contentRect.bottom - investmentTriangleHeight);
				investmentTriangleDest.right = (int) (investmentX + investmentTriangleWidth / 2);
				investmentTriangleDest.bottom = contentRect.bottom;

				canvas.drawBitmap(investmentTriangleBitmap, null, investmentTriangleDest, null);
			}
		}
	}

	/**
	 * Returns the image which has to be shown for the given investment.
	 * 
	 * @param inv
	 * @return
	 */
	protected Bitmap getInvestmentBitmap(Investment inv) {
		Bitmap investmentBitmap = null;

		if (inv.getTypeId() == Investment.INVESTMENT_TYPE_CALL && currentLevel > inv.getCurrentLevel()) {
			// Win - call
			investmentBitmap = investmentWinCallBitmap;
		} else if (inv.getTypeId() == Investment.INVESTMENT_TYPE_PUT && currentLevel < inv.getCurrentLevel()) {
			// Win - put
			investmentBitmap = investmentWinPutBitmap;
		} else if (inv.getTypeId() == Investment.INVESTMENT_TYPE_CALL && currentLevel <= inv.getCurrentLevel()) {
			// Lose - call
			investmentBitmap = investmentLoseCallBitmap;
		} else if (inv.getTypeId() == Investment.INVESTMENT_TYPE_PUT && currentLevel >= inv.getCurrentLevel()) {
			// Lose - put
			investmentBitmap = investmentLosePutBitmap;
		}

		return investmentBitmap;
	}

	private void drawTimeline(Canvas canvas) {
		// DRAW TIMELINE:
		timelinePaint.setColor(timelineOpenColor);
		canvas.drawRect(getXByTime(startTime), contentRect.bottom - timelineWidthPx,
				getXByTime(timeLastInvest.getTime() - LAST_TEN_MINUTES_TO_INVEST_LENGTH_MS),
				contentRect.bottom,
				timelinePaint);

		timelinePaint.setColor(timelineClosingColor);
		canvas.drawRect(
				getXByTime(timeLastInvest.getTime() - LAST_TEN_MINUTES_TO_INVEST_LENGTH_MS),
				contentRect.bottom - timelineWidthPx, getXByTime(timeLastInvest.getTime()),
				contentRect.bottom, timelinePaint);

		timelinePaint.setColor(timelineExpiringColor);
		canvas.drawRect(getXByTime(timeLastInvest.getTime()),
				contentRect.bottom - timelineWidthPx,
				getXByTime(endTime), contentRect.bottom,
				timelinePaint);

		timelinePaint.setColor(HOURS_LINE_COLOR_TIME_PAST);
		canvas.drawRect(getXByTime(startTime),
				contentRect.bottom - timelineWidthPx,
				getXByTime(rates.get(rates.size() - 1).getTime()),
				contentRect.bottom,
				timelinePaint);
		/////////////////////////////////////

		// DRAW the vertical SPIKES:

		int spikesCount = (int) (getActualWidth() / spikesBitmap.getWidth());
		float spikesLeft;
		float spikesTop = contentRect.bottom - (int) timelineWidthPx - spikesBitmap.getHeight();
		for (int i = 0; i <= spikesCount; i++) {
			spikesLeft = getActualX(0) + i * spikesBitmap.getWidth();
			canvas.drawBitmap(spikesBitmap, spikesLeft, spikesTop, null);
		}

		/////////////////////////////////////		
		
		// DRAW HOURS Labels 
		float hourLabelY = contentRect.bottom - timelineWidthPx - hourLabelBounds.bottom - hoursVerticalLinesWidthPx - hourLabelPadding;
		
		int initHoursLabelsCount = 5;
		
		int zoomCoeffAdditionalLabels = Math.round((getZoomCoefficient() - 1));
		int hoursLabelsCount = initHoursLabelsCount + zoomCoeffAdditionalLabels;
		int intervalsCount = hoursLabelsCount - 1;
		float intervalWidth = ( getActualWidth() - hoursLabelsCount * hourLabelBounds.width() ) / intervalsCount;
		long timeInteval = timeLength / intervalsCount;
		
		for (int i = 0; i < hoursLabelsCount; i++) {
        	float hoursLabelX = getActualX(0) + i * (hourLabelBounds.width() + intervalWidth); 
        	
        	long time = startTime + i * timeInteval;

			if (i == 0) {
				time = startTime;
			}

			if (i == hoursLabelsCount - 1) {
				time = endTime;
				hoursLabelX -= 1;
			}

        	timeTempDate.setTime(time);
			canvas.drawText(timeFormat.format(timeTempDate), hoursLabelX , hourLabelY, hourLabelPaint);
		}
		////////////////////////////////
	}

    /**
     * Returns the time in milliseconds for the given abstract x which is a fraction of the whole chart width.
     * @param abstractX Should be in [0,1] interval.
     * @return
     */
    @SuppressWarnings("unused")
	private long getTimeByAbstractX(float abstractX){
    	long timeInterval = timeLength;
    	return (long) (startTime + timeInterval * abstractX);
    }
    
    /**
     * Returns the time in milliseconds for the given actual x.
     * @param x The actual x coordinate.
     * @return The time in milliseconds.
     */
    @SuppressWarnings("unused")
	private long getTimeByX(float x){
    	float abstractX = getAbstractX(x);
    	long timeInterval = timeLength;
    	
    	return (long) (startTime + timeInterval * abstractX);
    }
    
    /**
     * Returns the actual x coordinate by the given time.
     * @param time The time in milliseconds.
     * @return
     */
	private float getXByTime(long time) {
		long timeInterval = time - startTime;
		float abstractX = (float )timeInterval / timeLength;
		return getActualX(abstractX);
	}

	/**
	 * Returns the actual y coordinate.
	 * @param level
	 * @return
	 */
	private float getYByLevel(float level) {
		return getDrawY(AXIS_Y_MIN + yScale * (level - minLevel), chartPaddingPx, chartBottomOffset);
	}
	
	/**
	 * Returns the start time of the visible part of the chart in milliseconds.
	 */
	protected long getVisibleStartTime() {
		return (long) ((getCurrentViewport().left - AXIS_X_MIN) * 1000 / xScale + startTime);
	}

	/**
	 * Returns the end time of the visible part of the chart in milliseconds.
	 */
	protected long getVisibleEndTime() {
		return (long) ((getCurrentViewport().right - AXIS_X_MIN) * 1000 / xScale + startTime);
	}
	
    ////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //     Methods related to custom attributes
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public float getLevelTextSize( ) {
        return levelTextSize;
    }

    public void setLevelTextSize(float levelTextSize) {
        this.levelTextSize = levelTextSize;
        initPaints();
        ViewCompat.postInvalidateOnAnimation(this);
    }

    public int getLevelTextColor() {
        return levelTextColor;
    }

    public void setLevelTextColor(int labelTextColor) {
        this.levelTextColor = labelTextColor;
        initPaints();
        ViewCompat.postInvalidateOnAnimation(this);
    }

    public float getDataThickness() {
        return dataThickness;
    }

    public void setDataThickness(float dataThickness) {
        this.dataThickness = dataThickness;
    }

    public int getDataColor() {
        return dataColor;
    }

    public void setDataColor(int dataColor) {
        this.dataColor = dataColor;
    }

	public void clearData() {
		reset();

		isAvailable = false;
		rates.clear();
		investments.clear();
		investmentPoints.clear();

		invalidate();
	}

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //     ChartDataListener's methods.
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////
    
	@Override
	public void updateItem(int itemPos, String itemName, UpdateInfo update) {
		if (!ratesLoaded) {
            return;
        }

		String level = update.getNewValue(skinGroup.getLevelUpdateKey());
		String state = update.getNewValue(LightstreamerManager.COLUMN_ET_STATE);
		String time = update.getNewValue(LightstreamerManager.COLUMN_AO_TS);

        double pLevel = Double.parseDouble(level.replaceAll(",", ""));
        
		if (update.isValueChanged(LightstreamerManager.COLUMN_ET_STATE)) {
			оppState = Integer.parseInt(state);

			if (pLevel == 0 || оppState < Opportunity.STATE_OPENED
					|| оppState > Opportunity.STATE_CLOSED) {
				isAvailable = false;
				invalidateView();
			} else if (оppState == Opportunity.STATE_CLOSED) {
				// Do not add rates when the option is closed or done.
				invalidateView();
			}
		}

		if (pLevel == 0 || оppState < Opportunity.STATE_OPENED
				|| оppState >= Opportunity.STATE_CLOSED) {
			// No need to update.
			return;
		}

		boolean needsFullUpdate = false;

		currentLevel = pLevel;
		currentLevelText = levelFormat.format(currentLevel);
		currentLevelTime = Long.parseLong(time);

		if (minLevel == -1 || pLevel < minLevel) {
			minLevel = (float) currentLevel;
			minLevelText = levelFormat.format(currentLevel);
			initScaleValues();
			needsFullUpdate = true;
		}
		if (maxLevel == -1 || pLevel > maxLevel) {
			maxLevel = (float) currentLevel;
			maxLevelText = levelFormat.format(currentLevel);
			initScaleValues();
			needsFullUpdate = true;
		}

		rates.add(new ChartMarketRate((float) currentLevel, currentLevelTime));

		// If the buffer is not big enough, we create new buffer.
		if (chartPointsBuffer != null && chartPointsBuffer.length < rates.size() * 4) {
			// Here we make space for the Investment point to be part of the graphic line:
			int invetsmentsPoints = 0;
			if (getInvestmentPoints() != null) {
				invetsmentsPoints = getInvestmentPoints().size();
			}
			chartPointsBuffer = new float[rates.size() * 4 + invetsmentsPoints
					+ BUFFER_ADDITIONAL_SPACE];
			needsFullUpdate = true;
		}

		if (needsFullUpdate) {
			invalidateView();
		} else {
			invalidateNewRateArea(rates.get(rates.size() - 2), rates.get(rates.size() - 1));
		}
	}

	@Override
	public void setChartData(ChartDataMethodResult data) {
        Log.v(TAG, "setChartData: " + data);
        reset();

		if (data.getTimeEstClosing() == null || data.getTimeLastInvest() == null) {
			// The asset is unavailable.
			isAvailable = false;
			invalidateView();
			return;
		} else {
			isAvailable = true;
		}
		
        marketId = data.getMarketId();
        oppId = data.getOpportunityId();
        oppTypeId = data.getOpportunityTypeId();
        
        timeFirstInvest = data.getTimeFirstInvest();
        timeLastInvest = data.getTimeLastInvest();
        timeEstClosing = data.getTimeEstClosing();
        scheduled = data.getScheduled();
		Log.d(TAG, "Scheduled: " + scheduled);

		setUpTimeLegth();
		startTime = timeEstClosing.getTime() - timeLength;
		endTime = timeEstClosing.getTime();

		decimalPointDigits = data.getDecimalPoint();
		levelFormat.applyPattern(Utils.getDecimalFormatPattern(decimalPointDigits));

		rates.clear();
		if (data.getRates() != null) {
			long[] rt = data.getRatesTimes();
			double[] rv = data.getRates();
			for (int i = 0; i < rv.length; i++) {
				rates.add(new ChartMarketRate((float) rv[i], rt[i]));
			}
		}
        Log.v(TAG, "setChartData: mRates.size = " + rates.size());
        
        List<Investment> investmentsForMarket = getInvestmentsForCurrentMarket(data.getInvestments());
        if (rates.size() > 0) {
        	int investmentPointsCount = 0;
        	if(investmentsForMarket != null){
        		investmentPointsCount = investmentsForMarket.size();
        	}
			chartPointsBuffer = new float[rates.size() * 4 + investmentPointsCount * 2
					+ BUFFER_ADDITIONAL_SPACE];
		} else {
			chartPointsBuffer = new float[0];
		}
        ratesLoaded = true;
        
        cleanUnneededRates();
        initRatesCornerValues();
        initScaleValues();

        if(rates.size() > 0) {
	        currentLevel = rates.get(rates.size() - 1).getRate();
	        currentLevelText = String.valueOf(rates.get(rates.size() - 1).getRate());
	        currentLevelTime = rates.get(rates.size() - 1).getTime();
        } else {
        	currentLevel = -1;
        	currentLevelTime = 0;
        }

		getInvestments().clear();
		getInvestmentPoints().clear();
		if (investmentsForMarket != null) {
			Log.v(TAG, "Got investments: " + investmentsForMarket.size());

			for (int i = 0; i < investmentsForMarket.size(); i++) {
				getInvestmentPoints().add(new PointF());
			}

			getInvestments().addAll(investmentsForMarket);

			// Add Investment Points to the rates in order to include them in the buffer points for
			// the graphic:
			addInvestmentToRates(getInvestments());
		} else {
			Log.v(TAG, "Got no investments.");
		}
		investmentsLoaded = true;

        // We enable the zoom when the data is loaded.
        enableZoom(true);

		invalidateView();
	}

	/**
	 * Get the Investments which are for the current Market.
	 */
	public List<Investment> getInvestmentsForCurrentMarket(Investment[] investments){
		List<Investment> result = new ArrayList<Investment>();
		if(investments != null && investments.length > 0){
			for(int i=0; i<investments.length; i++){
				Investment investment = investments[i];
				if(investment.getMarketId() == marketId){
					result.add(investment);
				}
			}
		}
		
		return result;
	}

	@Override
	public void setChartDataPreviousHour(ChartDataMethodResult data) {
		if (data.getErrorCode() == Constants.ERROR_CODE_SUCCESS && data.getRatesTimes() != null
				&& data.getRates() != null) {
			timeLength = timeLength + HOUR_IN_MS;

			long[] rt = data.getRatesTimes();
			double[] rv = data.getRates();
			for (int i = 0; i < rv.length; i++) {
				rates.add(i, new ChartMarketRate((float) rv[i], rt[i]));
			}

			if (rates.size() > 0) {
				int investmentPointsCount = 0;
				if (getInvestments() != null) {
					investmentPointsCount = getInvestments().size();
				}
				chartPointsBuffer = new float[rates.size() * 4 + investmentPointsCount * 2
						+ BUFFER_ADDITIONAL_SPACE];
			}

			startTime = timeEstClosing.getTime() - timeLength;

			cleanUnneededRates();
			initRatesCornerValues();
			initScaleValues();

			invalidateView();
		} else {
			Log.e("chart", "Got no investments.");
		}
	}

	/**
	 * Adds the Investment's points to the rates in order to include it in the graphic line.
	 */
	private void addInvestmentToRates(List<Investment> invs) {
		for (Investment investment : invs) {
			addInvestmentToRates(investment);
		}
	}

	/**
	 * Adds the Investment's points to the rates in order to include it in the graphic line.
	 */
	private void addInvestmentToRates(Investment investment) {
		long investmentTime = investment.getTimeCreated().getTime();
		double investmentLevel = investment.getCurrentLevel();

		// We iterate through the rates backwards because it is most likely that
		// there are little rates added after the new Investment was created.
		for (int i = rates.size() - 1; i >= 0; i--) {
			ChartMarketRate rate = rates.get(i);
			long rateTime = rate.getTime();
			if (rateTime <= investmentTime) {
				rates.add(i + 1, new ChartMarketRate((float) investmentLevel, investmentTime));
				break;
			}
		}
	}
	
	@Override
	public void addInvestment(Investment inv) {
        getInvestments().add(inv);       
		getInvestmentPoints().add(new PointF());
		addInvestmentToRates(inv);

		invalidateView();
	}

	@Override
	public void removeInvestment(long invId) {
		for (int i = 0; i < getInvestments().size(); i++) {
			if (getInvestments().get(i).getId() == invId) {
				getInvestments().remove(i);
				getInvestmentPoints().remove(i);
				break;
			}
		}

		invalidateView();
	}

	@Override
	public void requestInvestmentFocus(Investment inv) {
		if (inv == null) {
			clickedInvestmentIndex = -1;
			invalidateView();
			return;
		}

		List<Investment> allInvestments = getInvestments();
		if (allInvestments == null) {
			return;
		}

		int count = allInvestments.size();
		for (int i = 0; i < count; i++) {
			if (allInvestments.get(i).getId() == inv.getId()) {
				clickedInvestmentIndex = i;
				invalidateView();
				return;
			}
		}
	}

	@Override
	public Investment getFocusedInvestment() {
		if (clickedInvestmentIndex < 0) {
			return null;
		}

		return getInvestments().get(clickedInvestmentIndex);
	}

	private void setUpTimeLegth() {
		if (scheduled == Opportunity.SCHEDULED_HOURLY) {
			timeLength = timeEstClosing.getTime() - timeFirstInvest.getTime();
		} else {

			long hourlyOppInterval = oppTypeId == Opportunity.TYPE_REGULAR ? LENGTH_IN_MS_REGULAR : LENGTH_IN_MS_OPTION_PLUS;
			long currentTime = TimeUtils.getCurrentServerTime();
			long estClosing = timeEstClosing.getTime();

			//Initial interval:
			timeLength = hourlyOppInterval;
			long timeTemp = estClosing - timeLength;			
			//If the initial interval is not enough, we increment it until the current time is in the visible part:
			while (timeTemp > currentTime) {
				timeTemp -= timeLength;		
				timeLength += hourlyOppInterval;
			}
		}
	}

	/**
	 * Returns the time interval of the whole chart in seconds.
	 * 
	 * @return The time interval in seconds.
	 */
	private int getTimeLengthInSec() {
		return (int) (timeLength / 1000);
	}

	/**
	 * Cleans the rates which should not appear in the chart.
	 */
	private void cleanUnneededRates() {
		if (!ratesLoaded || oppId == 0) {
			return;
		}

		long currentTime = TimeUtils.getCurrentServerTime();
		long ratesStartTime;

		do {
			ratesStartTime = currentTime - timeLength;
		} while (currentTime < ratesStartTime);
		if (rates.size() > 0 && rates.get(0).getTime() < ratesStartTime) {
			// If there is previous hour in the history
			ratesStartTime = ratesStartTime - timeLength;
		}

		// Clean old hour rates
		while (rates.size() > 0 && rates.get(0).getTime() < ratesStartTime) {
			rates.remove(0);
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		
		if(!isPageSlideEnabled){
			// Disables parent's touch event. The parent's touch event is allowed only close to the left and right edges.
			if ((event.getX() > leftRightTouchParentWidhtPx) && (event.getX() < getWidth() - leftRightTouchParentWidhtPx)) {
				ViewParent parent = getParent();
				parent.requestDisallowInterceptTouchEvent(true);
			}			
		}

		// Handle swipe to the left edge event.
		if (onScrollToLeftEdgeListener != null) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				startChartSwipePoint.set(event.getX(), event.getY());
			} else if (event.getAction() == MotionEvent.ACTION_UP && isScrolledToLeftEdge()
					&& (event.getX() - startChartSwipePoint.x > chartSwipeMinWidth)) {
				if (onScrollToLeftEdgeListener.onScrollToLeftEdge()) {
					return true;
				}
			}
		}

		// Handle investment's motion events.
		if (getInvestments().size() > 0) {
			boolean handled = false;

			int action = event.getAction();
			switch (action) {
				case MotionEvent.ACTION_DOWN:
					if (clickedInvestmentIndex >= 0) {
						startInvestmentSwipePoint.set(event.getX(), event.getY());
					}
					break;
				case MotionEvent.ACTION_UP:
					if (!handled && event.getEventTime() - event.getDownTime() <= MAX_CLICK_TOUCH_DOWNTIME_MS) {
						float x = event.getX();
						float y = event.getY();
	
						handled = handleInvestmentClickEvent(x, y);
					}
					break;
				case MotionEvent.ACTION_MOVE:
					handled = handleInvestmentSwipeEvent(event.getX(), event.getY());
					break;
				default:
					break;
			}

			if (handled) {
				invalidate();
				return true;
			}
		}

		return super.onTouchEvent(event);
	}
	
	private boolean handleInvestmentClickEvent(float x, float y) {
		int oldClickedInvestmentIndex = clickedInvestmentIndex;
		clickedInvestmentIndex = -1;
		float clickedInvestmentDistance = -1;

		for (int i = 0; i < getInvestmentPoints().size(); i++) {
			float invIconCenterX = getInvestmentPoints().get(i).x;
			if (invIconCenterX < 0) {
				continue;
			}

			float invIconCenterY;
			if (getInvestments().get(i).getTypeId() == Investment.INVESTMENT_TYPE_CALL) {
				invIconCenterY = getInvestmentPoints().get(i).y + investmentHeight / 2;
			} else {
				// INVESTMENT_TYPE_PUT
				invIconCenterY = getInvestmentPoints().get(i).y - investmentHeight / 2;
			}

			// (x1 - x2)^2 + (y1 - y2)^2 = distance^2
			float distance = (float) Math.sqrt(Math.pow(x - invIconCenterX, 2)
					+ Math.pow(y - invIconCenterY, 2));
			if (distance < investmentClickRadius) {
				if ((clickedInvestmentIndex == -1)
						|| (clickedInvestmentIndex != -1 && clickedInvestmentDistance > distance)) {
					clickedInvestmentIndex = i;
					clickedInvestmentDistance = distance;
				}
			}
		}

		if (clickedInvestmentIndex != oldClickedInvestmentIndex) {
			InvestmentEvent event = null;
			if(clickedInvestmentIndex >= 0) {
				event = new InvestmentEvent(marketId, Type.INVESTMENT_FOCUSED, getInvestments().get(clickedInvestmentIndex));
			} else {
				event = new InvestmentEvent(marketId, Type.INVESTMENT_UNFOCUSED);
			}
			application.postEvent(event);
			return true;
		}
		
		return false;
	}

	private boolean handleInvestmentSwipeEvent(float eventX, float eventY) {
		if (clickedInvestmentIndex >= 0) {
			int oldClickedInvestmentIndex = clickedInvestmentIndex;
			
			if (eventX - startInvestmentSwipePoint.x > investmentSwipeMinWidth
					&& clickedInvestmentIndex < getInvestments().size() - 1) {
				// Move right
				if (getInvestmentPoints().get(clickedInvestmentIndex + 1).x > contentRect.right
						- clickedInvestmentPadding) {
					// Have to scroll the chart.
					startInvestmentSwipePoint.set(eventX, eventY);
					if (!isScrolling()) {
						clickedInvestmentIndex++;
						int dx = (int) (getInvestmentPoints().get(clickedInvestmentIndex).x - (contentRect.right - clickedInvestmentPadding));
						scrollHorizontally(dx);
					}
				} else {
					// Don't have to scroll
					startInvestmentSwipePoint.set(eventX, eventY);
					clickedInvestmentIndex++;
				}

			} else if (startInvestmentSwipePoint.x - eventX > investmentSwipeMinWidth
					&& clickedInvestmentIndex > 0) {
				// Move left
				if (getInvestmentPoints().get(clickedInvestmentIndex - 1).x < contentRect.left
						+ clickedInvestmentPadding) {
					// Have to scroll the chart.
					startInvestmentSwipePoint.set(eventX, eventY);
					if (!isScrolling()) {
						clickedInvestmentIndex--;
						int dx = (int) (getInvestmentPoints().get(clickedInvestmentIndex).x - (contentRect.left + clickedInvestmentPadding));
						scrollHorizontally(dx);
					}
				} else {
					// Don't have to scroll
					startInvestmentSwipePoint.set(eventX, eventY);
					clickedInvestmentIndex--;
				}
			}
			
			if (clickedInvestmentIndex != oldClickedInvestmentIndex) {
				InvestmentEvent event = new InvestmentEvent(marketId, Type.INVESTMENT_FOCUSED,
						getInvestments().get(clickedInvestmentIndex));
				application.postEvent(event);
				return true;
			}

			return true;
		}

		return false;
	}

	public boolean isZoomEnabled() {
		if (getMinimumZoomPercent() != NO_ZOOM_PERCENT) {
			return true;
		}

		return false;
	}

	public void enableZoom(boolean enable) {
		if (enable) {
			setMaxZoom(maxZoomMinutes);
		} else {
			// The view can not zoom
			setMinimumZoomInPercent(NO_ZOOM_PERCENT);
		}
	}

	/**
	 * The view can be zoomed maximum to the given minutes. The default value used is 5 minutes.
	 * If the zoom is disabled with {@link #enableZoom(boolean)}, we enable it again.
	 * 
	 * @param minutes
	 */
	public void setMaxZoom(int minutes) {
		maxZoomMinutes = minutes;
		int minPercent = minutes * 100 / (getTimeLengthInSec() / 60);
		if (minPercent > 100) {
			minPercent = 100;
		}

		setMinimumZoomInPercent(minPercent);
	}

	public boolean isScrolledToLeftEdge() {
		return getCurrentViewport().left == AXIS_X_MIN;
	}

	public ArrayList<Investment> getInvestments() {
		return investments;
	}

	public void setInvestments(ArrayList<Investment> investments) {
		this.investments = investments;
	}

	public ArrayList<PointF> getInvestmentPoints() {
		return investmentPoints;
	}

	public void setInvestmentPoints(ArrayList<PointF> investmentPoints) {
		this.investmentPoints = investmentPoints;
	}

	public OnScrollToLeftEdgeListener getOnScrollToLeftEdgeListener() {
		return onScrollToLeftEdgeListener;
	}

	public void setOnScrollToLeftEdgeListener(OnScrollToLeftEdgeListener onScrollToLeftEdgeListener) {
		this.onScrollToLeftEdgeListener = onScrollToLeftEdgeListener;
	}

	/**
	 * Invalidates view from Non-UI thread.
	 */
	private void invalidateView() {
		if (isAboveJellyBean) {
			postInvalidateOnAnimation();
		} else {
			postInvalidate();
		}
	}

	/**
	 * Invalidates specific area from Non-UI thread.
	 */
	private void invalidateView(int left, int top, int right, int bottom) {
		if (isAboveJellyBean) {
			postInvalidateOnAnimation(left, top, right, bottom);
		} else {
			postInvalidate(left, top, right, bottom);
		}
	}

	/**
	 * Invalidates the area when new rate is added.
	 * 
	 * @param rate1
	 *            previous rate
	 * @param rate2
	 *            new rate
	 */
	private void invalidateNewRateArea(ChartMarketRate rate1, ChartMarketRate rate2) {
		int left;
		int right;

		if (rate1.getTime() < rate2.getTime()) {
			left = (int) getXByTime(rate1.getTime());
			right = (int) getXByTime(rate2.getTime());
		} else {
			left = (int) getXByTime(rate2.getTime());
			right = (int) getXByTime(rate1.getTime());
		}

		int top;
		int bottom;
		if (rate1.getRate() > rate2.getRate()) {
			top = (int) getYByLevel(rate1.getRate());
			bottom = (int) getYByLevel(rate2.getRate());
		} else {
			top = (int) getYByLevel(rate2.getRate());
			bottom = (int) getYByLevel(rate1.getRate());
		}

		top = top - currentLevelBitmap.getWidth() / 2;
		bottom = bottom + currentLevelBitmap.getWidth() / 2;

		// Invalidate current level line and line between the two rates.
		invalidateView(contentRect.left, top, contentRect.right, bottom);

		// Invalidate current level text
		invalidateView((int) currentLevelTextDrawnBounds.left,
				(int) currentLevelTextDrawnBounds.top - (bottom - top),
				(int) currentLevelTextDrawnBounds.right + right - left,
				(int) currentLevelTextDrawnBounds.bottom + (bottom - top));
	}

}
