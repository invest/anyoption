package com.anyoption.android.app.popup;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.DepositBonusBalanceBase;
import com.anyoption.common.service.results.DepositBonusBalanceMethodResult;

import android.os.Bundle;
import android.text.Spannable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class SplitBalanceDropdownDialogFragment extends SplitBalanceDialogFragment {

	public static final String TAG = SplitBalanceDropdownDialogFragment.class.getSimpleName();

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static SplitBalanceDropdownDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new SplitBalanceDropdownDialogFragment.");
		SplitBalanceDropdownDialogFragment fragment = new SplitBalanceDropdownDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		
		fragment.setArguments(bundle);
		return fragment;
	}

	private int pointerPaddingRight;
	private TextView bonusProfitTextView;
	private String bonusProfitBalance;
	private ImageView infoArrowImageView;
	private View infoLayout;
	private TextView infoCashTextView;
	private TextView infoBonusProfitTextView;
	private TextView infoBonusBalanceTextView;
	private TextView infoTermsAndConditionsTextView;
	private View infoSelectorView;
	private TextView infoTextView;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Bundle args = getArguments();
		if(args != null){
			pointerPaddingRight = args.getInt(Constants.EXTRA_AMOUNT);
		}
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onCreateContentView(View contentView) {
		super.onCreateContentView(contentView);
		View pointerView = contentView.findViewById(R.id.popup_pointer);
		pointerView.setPadding(0, 0, pointerPaddingRight, 0);
		
		bonusProfitTextView = (TextView) contentView.findViewById(R.id.split_balance_popup_bonus_profit_text_view);
		infoSelectorView = contentView.findViewById(R.id.split_balance_popup_more_info_selector);
		infoSelectorView.setOnClickListener(new OnClickListener() {
			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {
				if(infoLayout.getVisibility() == View.VISIBLE){
					infoLayout.setVisibility(View.GONE);
					infoTextView.setText(getString(R.string.splitBalancePopupMoreInfo));
					infoArrowImageView.setImageDrawable(getResources().getDrawable(R.drawable.split_balance_popup_more_info_arrow_down));
				}else{
					infoLayout.setVisibility(View.VISIBLE);
					infoTextView.setText(getString(R.string.splitBalancePopupLessInfo));
					infoArrowImageView.setImageDrawable(getResources().getDrawable(R.drawable.split_balance_popup_more_info_arrow_up));
				}
			}
		});
		infoTextView = (TextView) contentView.findViewById(R.id.split_balance_popup_more_info_selector_text_view);
		infoArrowImageView = (ImageView) contentView.findViewById(R.id.split_balance_popup_more_info_arrow_image_view);
		infoLayout = contentView.findViewById(R.id.split_balance_popup_more_info_layout);
		
		infoCashTextView = (TextView) contentView.findViewById(R.id.split_balance_popup_more_info_message_1);
		TextUtils.decorateInnerText(infoCashTextView, null, infoCashTextView.getText().toString(), getString(R.string.splitBalancePopupInfoCashBold), Font.ROBOTO_BOLD, 0, 0);
		
		infoBonusProfitTextView = (TextView) contentView.findViewById(R.id.split_balance_popup_more_info_message_2);
		Spannable spannable2 = TextUtils.decorateInnerText(infoBonusProfitTextView, null, infoBonusProfitTextView.getText().toString(), getString(R.string.splitBalancePopupInfoBonusProfitBold1), Font.ROBOTO_BOLD, 0, 0);
		TextUtils.decorateInnerText(infoBonusProfitTextView, spannable2, infoBonusProfitTextView.getText().toString(), getString(R.string.splitBalancePopupInfoBonusProfitBold2), Font.ROBOTO_BOLD, 0, 0);
		TextUtils.decorateInnerText(infoBonusProfitTextView, spannable2, infoBonusProfitTextView.getText().toString(), getString(R.string.splitBalancePopupInfoAsterixBold), null, R.color.split_balance_popup_star, 0);
		
		infoBonusBalanceTextView = (TextView) contentView.findViewById(R.id.split_balance_popup_more_info_message_3);
		Spannable spannable3 = TextUtils.decorateInnerText(infoBonusBalanceTextView, null, infoBonusBalanceTextView.getText().toString(), getString(R.string.splitBalancePopupInfoBonusBalanceBold), Font.ROBOTO_BOLD, 0, 0);
		TextUtils.decorateInnerText(infoBonusBalanceTextView, spannable3, infoBonusBalanceTextView.getText().toString(), getString(R.string.splitBalancePopupInfoAsterixBold), null, R.color.split_balance_popup_star, 0);
		
		infoTermsAndConditionsTextView = (TextView) contentView.findViewById(R.id.split_balance_popup_more_info_terms_and_conditions);
		infoTermsAndConditionsTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!(application.isLoggedIn() && application.getUser().getIsNeedChangePassword())) {
					dismiss();
				application.postEvent(new NavigationEvent(Screen.TERMS_AND_CONDITIONS, NavigationType.DEEP));
				}
			}
		});
	}
	
	@Override
	protected void setupOKButton(){
		//Ignore
	}
	
	@Override
	protected int getContentLayout() {
		return R.layout.split_balance_dropdown_popup;
	}

	@Override
	public void getSplitBalanceCallback(Object resObj){
		super.getSplitBalanceCallback(resObj);
		DepositBonusBalanceMethodResult result = (DepositBonusBalanceMethodResult) resObj;
		if(result != null && result instanceof DepositBonusBalanceMethodResult && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS){
			if(result.getDepositBonusBalanceBase() != null){
				DepositBonusBalanceBase depositBonusBalanceBase = result.getDepositBonusBalanceBase();
				bonusProfitBalance = AmountUtil.getUserFormattedAmount(depositBonusBalanceBase.getBonusWinnings());
				if(bonusProfitTextView != null){
					bonusProfitTextView.setText(bonusProfitBalance);
				}
			}
		}
	}
	
}
