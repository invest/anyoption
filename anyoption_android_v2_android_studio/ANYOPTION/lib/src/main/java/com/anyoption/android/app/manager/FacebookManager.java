package com.anyoption.android.app.manager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.DepositEvent;
import com.anyoption.android.app.event.InvestmentEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.LoginLogoutEvent.Type;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.applinks.AppLinkData;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import bolts.AppLinks;


/**
 * Manages the FACEBOOK connection, friends, post, shares, links, etc.
 *
 * @author Anastas Arnaudov
 */
public class FacebookManager {

    private static final String TAG = FacebookManager.class.getSimpleName();

    public static final String EVENT_LOGIN          = "Login";
    public static final String EVENT_DEPOSIT        = "Deposit";
    public static final String EVENT_REGISTER       = "Register";
    public static final String EVENT_TRADE          = "Trade";

    protected Application application;
    protected boolean pendingPublishReauthorization;
    protected AppEventsLogger logger;
    public static CallbackManager callbackManager;

    public interface SessionOpenedListener {
        void onSessionOpened(AccessToken accessToken);
    }

    public interface UserListener {
        void onUser(JSONObject user);
    }

    public interface FriendsListener {
        void onFriends(JSONArray friends);
    }

    public FacebookManager(final Application application) {
        Log.d(TAG, "Initializing...");
        FacebookSdk.sdkInitialize(application);
        callbackManager = CallbackManager.Factory.create();
        this.application = application;
        this.pendingPublishReauthorization = false;
        AppLinkData.fetchDeferredAppLinkData(application,
                new AppLinkData.CompletionHandler() {
                    @Override
                    public void onDeferredAppLinkDataFetched(AppLinkData appLinkData) {
                        Log.d(TAG, "Deferred AppLinkData Fetched.");
                        if (appLinkData != null) {
                            Log.d(TAG, "Ref = " + appLinkData.getRef());
                            Log.d(TAG, "Target URI = " + appLinkData.getTargetUri());
                            Log.d(TAG, "Argument Bundle = " + appLinkData.getArgumentBundle());
                            Log.d(TAG, "Ref Data Bundle = " + appLinkData.getRefererData());

                            Uri targetUri = appLinkData.getTargetUri();

                            application.getDeepLinksManager().handleDeferredDeepLinks(targetUri);
                        } else {
                            Log.d(TAG, "No Deferred AppLinkData.");
                        }
                    }
                }
        );

        registerForEvents();
    }

    /**
     * Checks if the Uri in the Intent is from Facebook Deep link.
     * @param context
     * @param intent
     * @return
     */
    public boolean isFacebookUri(Context context, Intent intent){
        Log.d(TAG, "Parsing Intent...");
        Uri targetUri = AppLinks.getTargetUrlFromInboundIntent(context, intent);
        return targetUri != null;
    }

    public void changeActivity(Activity activity) {
        Log.d(TAG, "Setting logger.");
        logger = AppEventsLogger.newLogger(application.getCurrentActivity());
    }

    private void registerForEvents() {
        application.registerForEvents(this, LoginLogoutEvent.class, DepositEvent.class, InvestmentEvent.class);
    }

    ///////////             EVENTS          ////////////////

    public void onEvent(LoginLogoutEvent event) {
        if (event.getType() == Type.LOGIN) {
            Log.d(TAG, "Sending login event.");
            logger.logEvent(EVENT_LOGIN);
        } else if (event.getType() == Type.REGISTER) {
            Log.d(TAG, "Sending register event.");
            logger.logEvent(EVENT_REGISTER);
        }
    }

    public void onEvent(DepositEvent event) {
        Log.d(TAG, "Sending deposit event.");
        logger.logEvent(EVENT_DEPOSIT);

        if(event.isFirstDeposit()) {
            Bundle parameters = new Bundle();
            parameters.putString("fb_order_id", String.valueOf(event.getTransactionId()));
            parameters.putString("em", Application.get().getUser().getEmail());
            logger.logEvent(AppEventsConstants.EVENT_NAME_PURCHASED, 0, parameters);
        }
    }

    public void onEvent(InvestmentEvent event) {
        if (event.getType() == com.anyoption.android.app.event.InvestmentEvent.Type.INVEST_SUCCESS) {
            Log.d(TAG, "Sending trade event.");
            logger.logEvent(EVENT_TRADE);
        }
    }

    //////////////////////////////////////////////////////

    public static boolean hasActiveSession() {
        return null != AccessToken.getCurrentAccessToken();
    }

    protected static void openSession(Context context, Fragment fragment, final SessionOpenedListener listener) {
        List<String> permissions = new ArrayList<String>();
        permissions.add("public_profile");
        permissions.add("user_friends");
        permissions.add("email");

        LoginManager.getInstance().registerCallback(FacebookManager.callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "Login result onSuccess");
                Log.d(TAG, "LoginResult = " + loginResult);
                Log.d(TAG, "New Facebook session opened. User access tooken: " + loginResult.getAccessToken());
                listener.onSessionOpened(loginResult.getAccessToken());
            }
            @Override
            public void onCancel() {
                Log.d(TAG, "Login result onCancel");
            }
            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "Login result onError");
                Log.d(TAG, "FacebookException = " + error);
            }
        });
        LoginManager.getInstance().logInWithReadPermissions(fragment, permissions);
    }

    public static void getUser(Context context, Fragment fragment, final UserListener listener) {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if(accessToken == null || accessToken.isExpired()){
            FacebookManager.openSession(context, fragment, new SessionOpenedListener() {
                @Override
                public void onSessionOpened(AccessToken accessToken) {
                    FacebookManager.getUser(accessToken, listener);
                }
            });
        }else{
            FacebookManager.getUser(accessToken, listener);
        }
    }

    private static void getUser(AccessToken accessToken, final UserListener listener) {
        if(accessToken != null && !accessToken.isExpired()){
            GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                @Override
                public void onCompleted(JSONObject object, GraphResponse response) {
                    if (object != null) {
                        Log.d(TAG, "Got Facebook user - fb id: " + object.optString("id") + " name: " + object.optString("name") + " email: " + object.optString("email"));
                    } else {
                        Log.d(TAG, "Failed to get Facebook user - response: " + response);
                    }
                    listener.onUser(object);
                }
            }).executeAsync();
        }else{
            listener.onUser(null);
        }
    }

    public void getFriends(Context context, Fragment fragment, final FriendsListener listener) {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if(accessToken == null || accessToken.isExpired()){
            FacebookManager.openSession(context, fragment, new SessionOpenedListener() {
                @Override
                public void onSessionOpened(AccessToken accessToken) {
                    FacebookManager.getFriends(accessToken, listener);
                }
            });
        }else{
            FacebookManager.getFriends(accessToken, listener);
        }
    }

    private static void getFriends(AccessToken accessToken, final FriendsListener listener) {
        Log.d(TAG, "getFriends");
        if (accessToken != null && !accessToken.isExpired()) {
            Log.d(TAG, "Make newMyFriendsRequest");
            GraphRequest.newMyFriendsRequest(accessToken, new GraphRequest.GraphJSONArrayCallback() {
                        @Override
                        public void onCompleted(JSONArray objects, GraphResponse response) {
                            Log.d(TAG, "getFriends.onCompleted" + " response: " + response);
                            if (objects != null) {
                                Log.d(TAG, "Got friends: " + objects);
                            } else {
                                Log.d(TAG, "No friends received");
                            }
                            if (null != listener) {
                                listener.onFriends(objects);
                            }
                        }
                    }
            ).executeAsync();
        } else {
            listener.onFriends(null);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        FacebookManager.callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void activateAppLogger(Activity activity){
        // Logs 'install' and 'app activate' App Events.
        Log.d(TAG, "activateAppLogger");
        AppEventsLogger.activateApp(activity);
    }

    public void deactivateAppLogger(Activity activity){
        // Logs 'app deactivate' App Event.
        Log.d(TAG, "deactivateAppLogger");
        AppEventsLogger.deactivateApp(activity);
    }
}