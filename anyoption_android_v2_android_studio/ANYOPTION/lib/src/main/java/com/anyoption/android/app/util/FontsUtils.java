package com.anyoption.android.app.util;

import com.anyoption.android.app.Application;

import android.graphics.Typeface;

/**
 * Utility class that provides useful methods for accessing files in the assets folder.
 * @author Anastas Arnaudov
 *
 */
public class FontsUtils {

	/**
	 * Maps Fonts defined in the assets/fonts folder.
	 * @author Anastas Arnaudov
	 *
	 */
	public enum Font{
		
		ROBOTO_REGULAR(1,"Roboto-Regular.ttf"),
		ROBOTO_LIGHT(2,"Roboto-Light.ttf"),
		ROBOTO_MEDIUM(3,"Roboto-Medium.ttf"),
		ROBOTO_BOLD(4,"Roboto-Bold.ttf"),
		ROBOTO_BLACK(5,"Roboto-Black.ttf"),

		ROBOTO_REGULAR_ITALIC(11,"Roboto-Italic.ttf"),
		ROBOTO_LIGHT_ITALIC(21,"Roboto-LightItalic.ttf"),
		ROBOTO_MEDIUM_ITALIC(31,"Roboto-MediumItalic.ttf"),
		ROBOTO_BOLD_ITALIC(41,"Roboto-BoldItalic.ttf"),
		
		NOTO_SANS_REGULAR(5,"NotoSans-Regular.ttf"),
		NOTO_SANS_BOLD(6,"NotoSans-Bold.ttf"),
		NOTO_SANS_BOLD_ITALIC(7,"NotoSans-BoldItalic.ttf"),
		NOTO_SANS_ITALIC(8,"NotoSans-Italic.ttf"),
		
		NOTO_SANS_HEBREW_REGULAR(9,"NotoSansHebrew-Regular.ttf"),
		NOTO_SANS_HEBREW_BOLD(10,"NotoSansHebrew-Bold.ttf");
		
		
		private int fontID;
		private String fontName;
		
		Font(int fontID, String fontName){
			this.fontID = fontID;
			this.fontName = fontName;
		}

		public int getFontID() {
			return fontID;
		}
		public String getFontName() {
			return fontName;
		}	
	}
	
	/**
	 * Returns Typeface created from the given asset.
	 * 
	 * @param assetsPathName
	 * @return
	 */
	public static Typeface getTypefaceFromAsset(String assetsPathName) {
		Typeface typeface = Typefaces.get(Application.get(), assetsPathName);
		return typeface;
	}
	
	public static Font getFontForFontID(int fontID){
		Font result = null;
		for(Font font : Font.values()){
			if(font.getFontID() == fontID){
				result = font;
				break;
			}
		}
		return result;
	}
}
