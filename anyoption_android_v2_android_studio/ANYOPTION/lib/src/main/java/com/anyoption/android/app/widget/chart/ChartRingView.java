package com.anyoption.android.app.widget.chart;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.anyoption.android.app.model.RingChartHistoryLine;
import com.anyoption.android.app.model.RingChartInvestment;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.service.results.ChartDataResult;
import com.anyoption.json.results.ChartDataMethodResult;
import com.lightstreamer.ls_client.UpdateInfo;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Anastas Arnaudov
 */
public class ChartRingView extends View implements ChartDrawable {

    public static final String TAG = ChartRingView.class.getSimpleName();

    private static final String HOUR_FORMAT                         = "HH:mm";
    private static final int HOUR_IN_MS                             = 60 * 60 * 1000;
    private static final int LENGTH_IN_MS_REGULAR                   = 60 * 60 * 1000;
    private static final int LENGTH_IN_MS_OPTION_PLUS               = 30 * 60 * 1000;
    private static final long LAST_TEN_MINUTES_TO_INVEST_LENGTH_MS  = 10 * 60 * 1000;

    private final String hourLabelTextFormat = "00:00";
    private SimpleDateFormat timeFormat;

    private SkinGroup skinGroup;
    private boolean isDrawingsLoaded;
    private DecimalFormat levelFormat;
    private int decimalPointDigits;
    private int decimalPointBoldDigits;
    private List<RingChartHistoryLine> historyLinesList;
    private ArrayList<RingChartInvestment> investmentsPointsList;
    private Date timeFirstInvest;
    private Date timeLastInvest;
    private Date timeEstClosing;
    private int scheduled;
    private long timeLength;
    private long startTime;
    private long endTime;
    private long oppTypeId;
    private long currentLevelTime;
    private Date timeTempDate;
    private Double[] historyLevels;

    //Colors:
    private int whiteColor;
    private int chartRingGraphAboveDarkColor;
    private int chartRingGraphBellowDarkColor;
    private int chartRingGraphCurrentArrowDarkColor;
    private int chartRingCurrentLevelAboveTextColor;
    private int chartRingCurrentLevelBellowTextColor;
    private int chartRingInvestmentWinColor;
    private int chartRingInvestmentLoseColor;
    private int chartRingGraphAboveLightColor;
    private int chartRingGraphBellowLightColor;
    private int timelineOpenColor;
    private int timelineExpiringColor;
    private int timelineClosingColor;
    private int timelineCurrentColor;
    ////////////////

    private Bitmap spikesBitmap;

    //Coordinates, angles and widths:
    private float radius;
    private float ringWidth;
    private float smallRadius;

    private float centerX;
    private float centerY;
    private float currentOuterPointX;
    private float currentOuterPointY;
    private float currentInnerPointX;
    private float currentInnerPointY;
    private float previousOuterPointX;
    private float previousOuterPointY;
    private float previousInnerPointX;
    private float previousInnerPointY;
    private float startOuterPointX;
    private float startOuterPointY;
    private float centerOuterPointX;
    private float centerOuterPointY;
    private float centerInnerPointX;
    private float centerInnerPointY;
    private float endInnerPointX;
    private float endInnerPointY;
    private float centerLevelRotatePivotX;
    private float centerLevelRotatePivotY;
    private float centerLevelTextX;
    private float centerLevelTextY;
    private float centerLevelBellowTextX;
    private float centerLevelBellowTextY;
    private float centerLevelBoldTextX;
    private float centerLevelBoldTextY;
    private float centerLevelBoldBellowTextX;
    private float centerLevelBoldBellowTextY;
    private float currentLevelTextX;
    private float currentLevelTextY;
    private float currentLevelBoldTextX;
    private float currentLevelBoldTextY;

    private float centerAngle;
    private float startAngle;
    private float endAngle;
    private float sweepAngle;
    private float sweepHalfAngle;
    private float arrowWidthAngle;
    private float currentLevelTextRotateAngle;
    private float currentLevelTextMinAngle;

    private float centerLevel;
    private float minLevel;
    private float maxLevel;
    private float currentLevel;
    private float previousLevel;
    private float centerAngleInclination;
    private float centerLevelTextMarginTopBottom;
    private float currentLevelTextMarginH;
    private float currentLevelTextMarginV;
    private float boldTextMarging;
    private float boldTextBoldMarging;
    private float centerLevelTextBellowMarginTop;
    private float investmentPointRadius;
    private float investmentPointRadiusOffset;
    private float centralLevelStrokeWidth;
    private float centerLevelTextSize;
    private float currentLevelTextSize;
    private float centerLevelTextBellowSize;
    private float currentLevelArrowStrokeWidth;
    private float timelineWidth;
    private int spikesCount;
    private int spikesTop;
    private float hourLabelTextSize;
    private float hoursVerticalLinesWidth;
    private float hourLabelPadding;
    private boolean isTimelineSetup;
    private float hourLabelY;
    private int hoursLabelsCount;
    private int intervalWidth;
    private long timeInteval;
    private boolean isAvailable;
    private boolean isInitCalculated;
    private int historyLinesCount;
    private int historyLinesAlphaDelta;
    private float currentAngle;
    private float previousAngle;
    /////////////////////////////////////////

    private String currentLevelNormalText   = "";
    private String currentLevelBoldText     = "";
    private String centerLevelNormalText    = "";
    private String centerLevelBoldText      = "";

    private Rect currentLevelTextRect;
    private Rect currentLevelBoldTextRect;
    private Rect centerLevelTextRect;
    private Rect centerLevelBoldTextRect;
    private Rect centerLevelTextBellowRect;
    private Rect centerLevelBoldTextBellowRect;
    private Rect timelineOpenRect;
    private Rect timelineClosingRect;
    private Rect timelineExpiryRect;
    private Rect timelineCurrentRect;
    private Rect hourLabelRect;

    private RectF ringOvalBigRectF;
    private RectF ringOvalSmallRectF;

    private Path arcBellowDarkPath;
    private Path arcBellowLightPath;
    private Path arcAboveDarkPath;
    private Path arcAboveLightPath;
    private Path centerLevelTopPath;
    private Path centerLevelBottomPath;
    private Path arrowLeftPath;
    private Path arrowRightPath;

    private Paint bellowLightPaint;
    private Paint aboveLightPaint;
    private Paint centerLevelPaint;
    private Paint arrowRightPaint;
    private Paint arrowLeftPaint;
    private Paint bellowDarkPaint;
    private Paint aboveDarkPaint;
    private Paint centerLevelTextPaint;
    private Paint centerLevelBoldTextPaint;
    private Paint centerLevelTextBellowPaint;
    private Paint centerLevelBoldTextBellowPaint;
    private Paint currentLevelTextPaint;
    private Paint currentLevelBoldTextPaint;
    private Paint historyLinePaint;
    private Paint investmentPaint;
    private Paint timelineOpenPaint;
    private Paint timelineClosingPaint;
    private Paint timelineExpiryPaint;
    private Paint timelineCurrentPaint;
    private Paint hourLabelPaint;

    public ChartRingView(Context context) {
        super(context);
        init(context, null, 0);
    }

    public ChartRingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public ChartRingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        Skin skin = Application.get().getSkins().get(Application.get().getSkinId());
        skinGroup = skin.getSkinGroup();
        timeTempDate    = new Date();
        timeFormat      = new SimpleDateFormat(HOUR_FORMAT);

        historyLinesCount       = 6;
        historyLinesAlphaDelta  = 30;
        historyLinesList = new ArrayList<>(historyLinesCount);
        for(int i=0; i< historyLinesCount; i++){
            RingChartHistoryLine ringChartHistoryLine = new RingChartHistoryLine();
            ringChartHistoryLine.alpha = 250 - i*historyLinesAlphaDelta;
            historyLinesList.add(ringChartHistoryLine);
        }

        investmentsPointsList = new ArrayList<RingChartInvestment>();

        Resources res = getResources();
        whiteColor                              = ColorUtils.getColor(R.color.white);
        chartRingGraphCurrentArrowDarkColor     = ColorUtils.getColor(R.color.chartRingGraphCurrentArrowDark);
        chartRingGraphAboveDarkColor            = ColorUtils.getColor(R.color.chartRingGraphAboveDark);
        chartRingCurrentLevelAboveTextColor     = ColorUtils.getColor(R.color.chartRingCurrentLevelAboveText);
        chartRingGraphAboveLightColor           = ColorUtils.getColor(R.color.chartRingGraphAboveLight);
        chartRingGraphBellowDarkColor           = ColorUtils.getColor(R.color.chartRingGraphBellowDark);
        chartRingCurrentLevelBellowTextColor    = ColorUtils.getColor(R.color.chartRingCurrentLevelBellowText);
        chartRingGraphBellowLightColor          = ColorUtils.getColor(R.color.chartRingGraphBellowLight);
        chartRingInvestmentLoseColor            = ColorUtils.getColor(R.color.chartRingInvestmentLose);
        chartRingInvestmentWinColor             = ColorUtils.getColor(R.color.chartRingInvestmentWin);
        timelineOpenColor	                    = ColorUtils.getColor(R.color.chartRingTimelineOpen);
        timelineExpiringColor                   = ColorUtils.getColor(R.color.chartRingTimelineExpiring);
        timelineClosingColor                    = ColorUtils.getColor(R.color.chartRingTimelineClosing);
        timelineCurrentColor                    = ColorUtils.getColor(R.color.chartRingTimelineCurrent);

        spikesBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.spikes);

        decimalPointDigits      = 5;
        levelFormat             = Utils.getDecimalFormat(decimalPointDigits);
        levelFormat.applyPattern(Utils.getDecimalFormatPattern(decimalPointDigits));
        decimalPointBoldDigits  = 3;

        startAngle                      = 225.0f;
        sweepAngle                      = 90.0f;
        endAngle                        = startAngle + sweepAngle;
        sweepHalfAngle                  = sweepAngle / 2;
        centerAngle                     = startAngle + sweepHalfAngle;
        centralLevelStrokeWidth         = ScreenUtils.convertDpToPx(getContext(), 1);//3.0f;
        maxLevel                        = centerLevel + (centerLevel - minLevel);
        currentLevel                    = centerLevel;
        previousLevel                   = centerLevel;
        arrowWidthAngle                 = 4.0f;
        centerAngleInclination = 5.0f;
        centerLevelTextSize             = ScreenUtils.convertDpToPx(getContext(), 14);
        currentLevelTextSize            = ScreenUtils.convertDpToPx(getContext(), 12);
        centerLevelTextMarginTopBottom  = ScreenUtils.convertDpToPx(getContext(), 2);
        centerLevelTextBellowSize       = ScreenUtils.convertDpToPx(getContext(), 11);

        currentLevelTextMarginH         = ScreenUtils.convertDpToPx(getContext(), 3);
        currentLevelTextMarginV         = ScreenUtils.convertDpToPx(getContext(), 2);
        currentLevelTextMinAngle        = 6.5f;
        boldTextMarging                 = 2.0f;
        boldTextBoldMarging             = 1.0f;
        centerLevelTextBellowMarginTop  = ScreenUtils.convertDpToPx(getContext(), 14);
        investmentPointRadius           = ScreenUtils.convertDpToPx(getContext(), 2);
        investmentPointRadiusOffset     = ScreenUtils.convertDpToPx(getContext(), 5.5f);
        currentLevelArrowStrokeWidth    = 0.1f;
        timelineWidth                   = ScreenUtils.convertDpToPx(context, 5);
        hourLabelTextSize               = ScreenUtils.convertSpToPx(context, 8);
        hoursVerticalLinesWidth         = ScreenUtils.convertDpToPx(context, 5);
        hourLabelPadding                = ScreenUtils.convertDpToPx(context, 1);
    }

    @Override
    public void clearData() {
        isAvailable = false;

        historyLinesList.clear();
        investmentsPointsList.clear();

        invalidate();
    }

    public void setChartDataCommon(Market market, ChartDataResult data) {
        Log.d(TAG, "setChartDataCommon");
        //Check if the Market is Available:
        if (data.getTimeEstClosing() == null || data.getTimeLastInvest() == null) {
            // The Market is Unavailable.
            Log.d(TAG, "setChartDataCommon. Market is unavaiable.");
            isAvailable = false;
            invalidate();
            return;
        } else {
            isAvailable = true;
        }

        timeFirstInvest = data.getTimeFirstInvest();
        timeLastInvest  = data.getTimeLastInvest();
        timeEstClosing  = data.getTimeEstClosing();
        scheduled       = data.getScheduled();

        setupTimeLegth();

        startTime       = timeEstClosing.getTime() - timeLength;
        endTime         = timeEstClosing.getTime();

        decimalPointDigits = (int)(long) market.getDecimalPoint();
        levelFormat.applyPattern(Utils.getDecimalFormatPattern(decimalPointDigits));

        centerLevel  = (float) data.getEventLevel();

        historyLevels = data.getRates().toArray(new Double[data.getRates().size()]);

        if(historyLevels != null){
            currentLevel = (float)(double) historyLevels[historyLevels.length-1];

            //Set initial history lines:
            int historyLinesCount = Math.min(historyLinesList.size(), historyLevels.length);
            for (int i = 0; i <=historyLinesCount-1; i++) {
                float level = (float)(double) historyLevels[historyLevels.length - 2 - i];
                historyLinesList.get(i).level = level;
            }
        }

        //Get Investments:
        Investment[] investments = data.getInvestments();
        investmentsPointsList.clear();
        if(investments != null && investments.length > 0){
            for(Investment investment : investments){
                if(investment.getMarketId() == data.getMarketId()){
                    RingChartInvestment ringChartInvestment = new RingChartInvestment();
                    ringChartInvestment.id      = investment.getId();
                    ringChartInvestment.type    = investment.getTypeId();
                    ringChartInvestment.level   = Float.parseFloat(investment.getLevel().replace(",", ""));
                    investmentsPointsList.add(ringChartInvestment);
                }
            }
        }

        calculateInitScale();

        invalidate();
    }

    @Override
    public void updateItem(int itemPos, String itemName, UpdateInfo update) {
        String currentLevelString       = update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_LEVEL).replaceAll(",", "");
        String currentLevelTimeString   = update.getNewValue(LightstreamerManager.COLUMN_AO_TS);
        currentLevelTime                = Long.parseLong(currentLevelTimeString);

        //Save previuos values:
        previousLevel       = currentLevel;
        previousAngle       = currentAngle;
        previousOuterPointX = currentOuterPointX;
        previousOuterPointY = currentOuterPointY;
        previousInnerPointX = currentInnerPointX;
        previousInnerPointY = currentInnerPointY;

        //Update the new current level:
        currentLevel = Float.parseFloat(currentLevelString.replaceAll(",", ""));

        //Calculate new values:
        calculate(false);
    }

    @Override
    public void addInvestment(Investment inv) {
        //Create new Chart Investment Object:
        RingChartInvestment chartInvestment = new RingChartInvestment();
        chartInvestment.id      = inv.getId();

        chartInvestment.level   = (float) Utils.parseMarketLevel(inv.getLevel());
        chartInvestment.type    = inv.getTypeId();

        //Calculate the coordinates:
        boolean shouldRescale = false;
        if(chartInvestment.level > maxLevel){
            maxLevel = chartInvestment.level;
            minLevel = centerLevel - (maxLevel - centerLevel);
            shouldRescale = true;
        }else if(chartInvestment.level < minLevel){
            minLevel = chartInvestment.level;
            maxLevel = centerLevel + (centerLevel - minLevel);
            shouldRescale = true;
        }

        float angle = getAngleForLevel(chartInvestment.level);
        chartInvestment.pointX = getPointXForAngle(angle, radius + investmentPointRadiusOffset);
        chartInvestment.pointY = getPointYForAngle(angle, radius + investmentPointRadiusOffset);

        //Add it to the list and redraw:
        investmentsPointsList.add(chartInvestment);
        calculate(shouldRescale);
        invalidate();
    }

    @Override
    public void removeInvestment(long invId) {
        if(invId == 0L){
            //Remove All Investments:
            investmentsPointsList.clear();
            invalidate();
        }else{
            //Search for the investment by the given id:
            RingChartInvestment removeInvestment = null;
            for(RingChartInvestment chartInvestment : investmentsPointsList){
                if(chartInvestment.id == invId){
                    removeInvestment = chartInvestment;
                    break;
                }
            }
            if(removeInvestment != null){
                //Remove chart investment item and redraw:
                investmentsPointsList.remove(removeInvestment);
                invalidate();
            }
        }
    }

    /**
     * Init values related to the drawings.
     * The View must be measured (width and height should be set) in order to calculate these values.
     */
    private void initDrawingsValues() {

        centerX                 = getWidth() / 2;
        radius                  = getHeight() * 3 / 4;
        centerY                 = radius + ScreenUtils.convertDpToPx(getContext(), 10);
        ringWidth               = radius / 2;
        smallRadius             = radius - ringWidth;
        centerLevelRotatePivotX = centerX;
        centerLevelRotatePivotY = centerY - radius;

        startOuterPointX        = getPointXForAngle(startAngle, radius);
        startOuterPointY        = getPointYForAngle(startAngle, radius);
        float startInnerPointX  = getInnerPointXFromOuterPointX(startOuterPointX);
        float startInnerPointY  = getInnerPointYFromOuterPointY(startOuterPointY);
        centerOuterPointX       = getPointXForAngle(centerAngle, radius);
        centerOuterPointY       = getPointYForAngle(centerAngle, radius);
        centerInnerPointX       = getInnerPointXFromOuterPointX(centerOuterPointX);
        centerInnerPointY       = getInnerPointYFromOuterPointY(centerOuterPointY);
        float endOuterPointX    = getPointXForAngle(endAngle, radius);
        float endOuterPointY    = getPointYForAngle(endAngle, radius);
        endInnerPointX          = getInnerPointXFromOuterPointX(endOuterPointX);
        endInnerPointY          = getInnerPointYFromOuterPointY(endOuterPointY);
        previousOuterPointX     = centerOuterPointX;
        previousOuterPointY     = centerOuterPointY;
        previousInnerPointX     = centerInnerPointX;
        previousInnerPointY     = centerInnerPointY;

        centerLevelTextRect             = new Rect();
        centerLevelBoldTextRect         = new Rect();
        centerLevelTextBellowRect       = new Rect();
        centerLevelBoldTextBellowRect   = new Rect();
        currentLevelTextRect            = new Rect();
        currentLevelBoldTextRect        = new Rect();
        timelineOpenRect                = new Rect();
        timelineClosingRect             = new Rect();
        timelineExpiryRect              = new Rect();
        timelineCurrentRect             = new Rect();
        hourLabelRect                   = new Rect();


        ringOvalBigRectF    = new RectF(centerX - radius, centerY - radius, centerX + radius, centerY + radius);
        ringOvalSmallRectF  = new RectF(centerX - smallRadius, centerY - smallRadius, centerX + smallRadius, centerY + smallRadius);

        arcBellowDarkPath       = new Path();
        arcBellowLightPath      = new Path();
        arcAboveDarkPath        = new Path();
        arcAboveLightPath       = new Path();
        centerLevelTopPath      = new Path();
        centerLevelBottomPath   = new Path();
        arrowLeftPath           = new Path();
        arrowRightPath          = new Path();

        bellowLightPaint = new Paint();
        bellowLightPaint.setAntiAlias(true);
        bellowLightPaint.setColor(chartRingGraphBellowLightColor);
        bellowLightPaint.setStyle(Paint.Style.FILL);

        aboveLightPaint = new Paint();
        aboveLightPaint.setAntiAlias(true);
        aboveLightPaint.setColor(chartRingGraphAboveLightColor);
        aboveLightPaint.setStyle(Paint.Style.FILL);

        centerLevelPaint = new Paint();
        centerLevelPaint.setAntiAlias(true);
        centerLevelPaint.setColor(whiteColor);
        centerLevelPaint.setStrokeWidth(centralLevelStrokeWidth);
        centerLevelPaint.setStyle(Paint.Style.STROKE);
        centerLevelPaint.setPathEffect(new DashPathEffect(new float[]{centralLevelStrokeWidth, centralLevelStrokeWidth}, 0));

        arrowRightPaint = new Paint();
        arrowRightPaint.setAntiAlias(true);
        arrowRightPaint.setColor(whiteColor);
        arrowRightPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        arrowRightPaint.setStrokeWidth(currentLevelArrowStrokeWidth);

        arrowLeftPaint = new Paint();
        arrowLeftPaint.setAntiAlias(true);
        arrowLeftPaint.setColor(chartRingGraphCurrentArrowDarkColor);
        arrowLeftPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        arrowLeftPaint.setStrokeWidth(currentLevelArrowStrokeWidth);

        bellowDarkPaint = new Paint();
        bellowDarkPaint.setAntiAlias(true);
        bellowDarkPaint.setColor(chartRingGraphBellowDarkColor);
        bellowDarkPaint.setStyle(Paint.Style.FILL);

        aboveDarkPaint = new Paint();
        aboveDarkPaint.setAntiAlias(true);
        aboveDarkPaint.setColor(chartRingGraphAboveDarkColor);
        aboveDarkPaint.setStyle(Paint.Style.FILL);

        String sampleLevelText = "###.#####";
        centerLevelTextPaint = new Paint();
        centerLevelTextPaint.setAntiAlias(true);
        centerLevelTextPaint.setColor(whiteColor);
        centerLevelTextPaint.setTextSize(centerLevelTextSize);
        centerLevelTextPaint.getTextBounds(sampleLevelText, 0, sampleLevelText.length(), centerLevelTextRect);

        centerLevelBoldTextPaint = new Paint();
        centerLevelBoldTextPaint.setAntiAlias(true);
        centerLevelBoldTextPaint.setColor(whiteColor);
        centerLevelBoldTextPaint.setTextSize(centerLevelTextSize);
        centerLevelBoldTextPaint.getTextBounds(sampleLevelText, 0, sampleLevelText.length(), centerLevelBoldTextRect);
        centerLevelBoldTextPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

        centerLevelTextBellowPaint = new Paint(centerLevelTextPaint);
        centerLevelTextBellowPaint.setTextSize(centerLevelTextBellowSize);
        centerLevelTextBellowPaint.getTextBounds(sampleLevelText, 0, sampleLevelText.length(), centerLevelTextBellowRect);

        centerLevelBoldTextBellowPaint = new Paint(centerLevelBoldTextPaint);
        centerLevelBoldTextBellowPaint.setTextSize(centerLevelTextBellowSize);
        centerLevelBoldTextBellowPaint.getTextBounds(sampleLevelText, 0, sampleLevelText.length(), centerLevelBoldTextBellowRect);

        currentLevelTextPaint = new Paint();
        currentLevelTextPaint.setAntiAlias(true);
        currentLevelTextPaint.setColor(whiteColor);
        currentLevelTextPaint.setTextSize(currentLevelTextSize);
        currentLevelTextPaint.getTextBounds(sampleLevelText, 0, sampleLevelText.length(), currentLevelTextRect);

        currentLevelBoldTextPaint = new Paint();
        currentLevelBoldTextPaint.setAntiAlias(true);
        currentLevelBoldTextPaint.setColor(whiteColor);
        currentLevelBoldTextPaint.setTextSize(currentLevelTextSize);
        currentLevelBoldTextPaint.getTextBounds(sampleLevelText, 0, sampleLevelText.length(), currentLevelBoldTextRect);
        currentLevelBoldTextPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

        historyLinePaint = new Paint();
        historyLinePaint.setAntiAlias(true);
        historyLinePaint.setColor(whiteColor);
        historyLinePaint.setStyle(Paint.Style.STROKE);
        historyLinePaint.setStrokeWidth(0);
        historyLinePaint.setAlpha(255);

        investmentPaint = new Paint();
        investmentPaint.setAntiAlias(true);
        investmentPaint.setColor(chartRingInvestmentLoseColor);
        investmentPaint.setStyle(Paint.Style.FILL);

        timelineOpenPaint = new Paint();
        timelineOpenPaint.setAntiAlias(true);
        timelineOpenPaint.setStrokeWidth(timelineWidth);
        timelineOpenPaint.setStyle(Paint.Style.FILL);
        timelineOpenPaint.setColor(timelineOpenColor);

        timelineClosingPaint = new Paint(timelineOpenPaint);
        timelineClosingPaint.setColor(timelineClosingColor);

        timelineExpiryPaint = new Paint(timelineOpenPaint);
        timelineExpiryPaint.setColor(timelineExpiringColor);

        timelineCurrentPaint = new Paint(timelineOpenPaint);
        timelineCurrentPaint.setColor(timelineCurrentColor);

        hourLabelPaint = new Paint();
        hourLabelPaint.setAntiAlias(true);
        hourLabelPaint.setTextSize(hourLabelTextSize);
        hourLabelPaint.setColor(whiteColor);

        isDrawingsLoaded = true;

        invalidate();
    }

    /**
     * Calculate the Initial Scale of the Chart : based on history levels and investment levels.
     */
    private void calculateInitScale(){
        if(!isDrawingsLoaded){
            return;
        }
        //Calculate the initial scale (min/max level):
        //Get the min/max history level
        float maxHistoryDeviation = 0;
        if(historyLevels != null){
            for(int i=0; i<historyLevels.length; i++){
                float level = (float)(double) historyLevels[i];
                float levelDeviation = Math.abs(centerLevel-level);
                if(levelDeviation >= maxHistoryDeviation){
                    maxHistoryDeviation = levelDeviation;
                }
            }
        }

        //Get the min/max investment level:
        float maxInvestmentDeviation = 0;
        for(RingChartInvestment investment : investmentsPointsList){
            float level = investment.level;
            float levelDeviation = Math.abs(centerLevel-level);
            if(levelDeviation >= maxInvestmentDeviation){
                maxInvestmentDeviation = levelDeviation;
            }
        }

        //The maximum deviation from the central level:
        float maxDeviation = Math.max(maxHistoryDeviation, maxInvestmentDeviation);

        //Set some initial deviation if for some reason there was no history or investments:
        if(maxDeviation == 0){
            maxDeviation = 0.01f;
        }

        //Recalculate the min and max levels:
        minLevel = centerLevel - maxDeviation;
        maxLevel = centerLevel + maxDeviation;

        calculateHistoryLines(true);
        calculateInvestmentsPoints(true);
        calculateTimeline(true);

        isInitCalculated = true;
        calculate(false);
    }

    /**
     * Calculate the values.
     */
    private void calculate(boolean shouldRescale){
        if (!isDrawingsLoaded) {
            return;
        }

        //Format the Levels Texts:
        String currentLevelText  = levelFormat.format(currentLevel);
        String centerLevelText   = levelFormat.format(centerLevel);

        currentLevelNormalText   = currentLevelText.substring(0, Math.max(currentLevelText.length() - decimalPointBoldDigits, 1));
        currentLevelBoldText     = currentLevelText.substring(currentLevelNormalText.length(), currentLevelText.length());
        centerLevelNormalText    = centerLevelText.substring(0, Math.max(centerLevelText.length() - decimalPointBoldDigits, 1));
        centerLevelBoldText      = centerLevelText.substring(centerLevelNormalText.length(), centerLevelText.length());

        //Check if "Current Level" < "Min Level" or "Current Level" > "Max Level" :
        //That means we have to RESCALE the elements:
        if(currentLevel <= minLevel){
            minLevel = currentLevel;
            maxLevel = centerLevel + (centerLevel - minLevel);
            shouldRescale = true;
        }else if(currentLevel >= maxLevel){
            maxLevel = currentLevel;
            minLevel = centerLevel - (maxLevel - centerLevel);
            shouldRescale = true;
        }

        //Calculate angles:
        currentAngle = getAngleForLevel(currentLevel);

        if(currentLevel != previousLevel){
            calculateHistoryLines(shouldRescale);
        }
        calculateInvestmentsPoints(shouldRescale);

        float bellowLightSweepAngle = centerAngle - currentAngle;
        if (bellowLightSweepAngle >= sweepHalfAngle) {
            bellowLightSweepAngle = sweepHalfAngle;
        } else if (bellowLightSweepAngle <= 0.0f) {
            bellowLightSweepAngle = 0.0f;
        }
        float aboveLightSweepAngle = currentAngle - centerAngle;
        if (aboveLightSweepAngle >= sweepHalfAngle) {
            aboveLightSweepAngle = sweepHalfAngle;
        } else if (aboveLightSweepAngle <= 0.0f) {
            aboveLightSweepAngle = 0.0f;
        }

        //Calculate coordinates:
        currentOuterPointX  = getPointXForAngle(currentAngle, radius);
        currentOuterPointY  = getPointYForAngle(currentAngle, radius);
        currentInnerPointX  = getInnerPointXFromOuterPointX(currentOuterPointX);
        currentInnerPointY  = getInnerPointYFromOuterPointY(currentOuterPointY);
        float currentLevelInnerLeftPointX = getPointXForAngle(currentAngle - arrowWidthAngle, smallRadius);
        float currentLevelInnerLeftPointY = getPointYForAngle(currentAngle - arrowWidthAngle, smallRadius);
        float currentLevelInnerRightPointX = getPointXForAngle(currentAngle + arrowWidthAngle, smallRadius);
        float currentLevelInnerRightPointY = getPointYForAngle(currentAngle + arrowWidthAngle, smallRadius);


        //Reset Paths:
        arcAboveDarkPath.reset();
        arcAboveLightPath.reset();
        arcBellowDarkPath.reset();
        arcBellowLightPath.reset();
        centerLevelTopPath.reset();
        centerLevelBottomPath.reset();
        arrowLeftPath.reset();
        arrowRightPath.reset();

        //Bellow Dark Arc:
        arcBellowDarkPath.moveTo(startOuterPointX, startOuterPointY);
        arcBellowDarkPath.addArc(ringOvalBigRectF, startAngle, sweepHalfAngle);
        arcBellowDarkPath.lineTo(centerInnerPointX, centerInnerPointY);
        arcBellowDarkPath.addArc(ringOvalSmallRectF, centerAngle, -sweepHalfAngle);
        arcBellowDarkPath.lineTo(startOuterPointX, startOuterPointY);

        //Above Dark Arc:
        arcAboveDarkPath.moveTo(centerOuterPointX, centerOuterPointY);
        arcAboveDarkPath.addArc(ringOvalBigRectF, centerAngle, sweepHalfAngle);
        arcAboveDarkPath.lineTo(endInnerPointX, endInnerPointY);
        arcAboveDarkPath.addArc(ringOvalSmallRectF, endAngle, -sweepHalfAngle);
        arcAboveDarkPath.lineTo(centerOuterPointX, centerOuterPointY);

        //Bellow Light Arc:
        arcBellowLightPath.moveTo(currentOuterPointX, currentOuterPointY);
        arcBellowLightPath.addArc(ringOvalBigRectF, currentAngle, bellowLightSweepAngle);
        arcBellowLightPath.lineTo(centerInnerPointX, centerInnerPointY);
        arcBellowLightPath.addArc(ringOvalSmallRectF, centerAngle, -bellowLightSweepAngle);
        arcBellowLightPath.lineTo(currentOuterPointX, currentOuterPointY);

        //Above Light Arc:
        arcAboveLightPath.moveTo(centerOuterPointX, centerOuterPointY);
        arcAboveLightPath.addArc(ringOvalBigRectF, centerAngle, aboveLightSweepAngle);
        arcAboveLightPath.lineTo(currentInnerPointX, currentInnerPointY);
        arcAboveLightPath.addArc(ringOvalSmallRectF, currentAngle, -aboveLightSweepAngle);
        arcAboveLightPath.lineTo(centerOuterPointX, centerOuterPointY);

        //Center Level Text (Vertical):
        centerLevelTextPaint.getTextBounds(centerLevelNormalText, 0, centerLevelNormalText.length(), centerLevelTextRect);
        centerLevelBoldTextPaint.getTextBounds(centerLevelBoldText, 0, centerLevelBoldText.length(), centerLevelBoldTextRect);
        float centerLevelTextOffsetTop = (ringWidth - centerLevelTextRect.width() - centerLevelBoldTextRect.width()) / 2;
        centerLevelTextX = centerX + centerLevelTextOffsetTop;
        centerLevelTextY = centerY - radius + centerLevelTextRect.height() / 2;
        centerLevelBoldTextX = centerLevelTextX + centerLevelTextRect.width() + boldTextMarging;
        centerLevelBoldTextY = centerLevelTextY;
        //Center Level Text (Bellow Chart):
        centerLevelTextBellowPaint.getTextBounds(centerLevelNormalText, 0, centerLevelNormalText.length(), centerLevelTextBellowRect);
        centerLevelBoldTextBellowPaint.getTextBounds(centerLevelBoldText, 0, centerLevelBoldText.length(), centerLevelBoldTextBellowRect);
        centerLevelBellowTextX = centerX - centerLevelTextBellowRect.width()/2 - centerLevelBoldTextBellowRect.width()/2;
        centerLevelBellowTextY = centerY - radius/2 + centerLevelBoldTextBellowRect.height() + centerLevelTextBellowMarginTop;
        centerLevelBoldBellowTextX = centerLevelBellowTextX + centerLevelTextBellowRect.width() + boldTextBoldMarging;
        centerLevelBoldBellowTextY = centerLevelBellowTextY;

        //Center Level Lines:
        centerLevelTopPath.moveTo(centerX, centerY - radius);
        centerLevelTopPath.lineTo(centerX, centerY - radius + centerLevelTextOffsetTop - centerLevelTextMarginTopBottom);
        centerLevelBottomPath.moveTo(centerX, centerY - radius + centerLevelTextOffsetTop + centerLevelTextRect.width() + centerLevelBoldTextRect.width() + centerLevelTextMarginTopBottom);
        centerLevelBottomPath.lineTo(centerX, centerY - radius + ringWidth);

        //Current Level Text:
        currentLevelTextPaint.getTextBounds(currentLevelNormalText, 0, currentLevelNormalText.length(), currentLevelTextRect);
        currentLevelBoldTextPaint.getTextBounds(currentLevelBoldText, 0, currentLevelBoldText.length(), currentLevelBoldTextRect);
        if (currentAngle < centerAngle) {
            //Do not allow the text to be outside of the ring:
            if (sweepHalfAngle - bellowLightSweepAngle < currentLevelTextMinAngle) {
                currentLevelTextRotateAngle = currentAngle + 180.0f - arrowWidthAngle;
                currentLevelTextX = currentOuterPointX + currentLevelTextMarginH;
                currentLevelTextY = currentOuterPointY - currentLevelTextMarginV;
                currentLevelTextPaint.setColor(chartRingGraphBellowDarkColor);
                currentLevelBoldTextPaint.setColor(chartRingGraphBellowDarkColor);
            } else {
                currentLevelTextRotateAngle = currentAngle + 180.0f + arrowWidthAngle;
                currentLevelTextX = currentOuterPointX + currentLevelTextMarginH;
                currentLevelTextY = currentOuterPointY + currentLevelTextRect.height() + currentLevelTextMarginV;
                currentLevelTextPaint.setColor(chartRingCurrentLevelBellowTextColor);
                currentLevelBoldTextPaint.setColor(chartRingCurrentLevelBellowTextColor);
            }
            currentLevelBoldTextX = currentLevelTextX + currentLevelTextRect.width() + boldTextMarging;
            currentLevelBoldTextY = currentLevelTextY;
        } else {
            //Do not allow the text to be outside of the ring:
            if (sweepHalfAngle - aboveLightSweepAngle < currentLevelTextMinAngle) {
                currentLevelTextRotateAngle = currentAngle + arrowWidthAngle;
                currentLevelTextX = currentOuterPointX - currentLevelTextRect.width() - currentLevelBoldTextRect.width() - currentLevelTextMarginH;
                currentLevelTextY = currentOuterPointY - currentLevelTextMarginV;
                currentLevelTextPaint.setColor(chartRingGraphAboveDarkColor);
                currentLevelBoldTextPaint.setColor(chartRingGraphAboveDarkColor);
            } else {
                currentLevelTextRotateAngle = currentAngle - arrowWidthAngle;
                currentLevelTextX = currentOuterPointX - currentLevelTextRect.width() - currentLevelBoldTextRect.width() - currentLevelTextMarginH;
                currentLevelTextY = currentOuterPointY + currentLevelTextRect.height() + currentLevelTextMarginV;
                currentLevelTextPaint.setColor(chartRingCurrentLevelAboveTextColor);
                currentLevelBoldTextPaint.setColor(chartRingCurrentLevelAboveTextColor);
            }
            currentLevelBoldTextX = currentLevelTextX + currentLevelTextRect.width() + boldTextMarging;
            currentLevelBoldTextY = currentLevelTextY;
        }

        //Current Level Pointer:
        //Left half of the arrow:
        arrowLeftPath.moveTo(currentOuterPointX, currentOuterPointY);
        arrowLeftPath.lineTo(currentInnerPointX, currentInnerPointY);
        arrowLeftPath.lineTo(currentLevelInnerLeftPointX, currentLevelInnerLeftPointY);
        //Right half of the arrow:
        arrowRightPath.moveTo(currentOuterPointX, currentOuterPointY);
        arrowRightPath.lineTo(currentInnerPointX, currentInnerPointY);
        arrowRightPath.lineTo(currentLevelInnerRightPointX, currentLevelInnerRightPointY);

        if (currentAngle < centerAngle) {
            arrowLeftPaint.setColor(chartRingGraphCurrentArrowDarkColor);
            arrowRightPaint.setColor(whiteColor);
        } else {
            arrowLeftPaint.setColor(whiteColor);
            arrowRightPaint.setColor(chartRingGraphCurrentArrowDarkColor);
        }

        calculateTimeline(shouldRescale);

        //Redraw:
        postInvalidate();
    }

    /**
     * Calculate the history levels lines.
     * Also, set the previous level as the new line in the list.
     * @param isScaled If true : the coordinates of the lines need to be recalculated based on their levels.
     */
    private void calculateHistoryLines(boolean isScaled){

        RingChartHistoryLine lastLine = null;

        //Get the last line:
        for(RingChartHistoryLine line : historyLinesList){
            if(isScaled){
                //Calculate the new coordinates:
                float newAngle   = getAngleForLevel(line.level);
                line.startPointX = getPointXForAngle(newAngle, radius);
                line.startPointY = getPointYForAngle(newAngle, radius);
                line.endPointX   = getInnerPointXFromOuterPointX(line.startPointX);
                line.endPointY   = getInnerPointYFromOuterPointY(line.startPointY);
            }
            //Decrease the visibility:
            line.alpha -= historyLinesAlphaDelta;
            if(line.alpha <= 30){
                line.alpha = 0;
            }

            //Get the least visible one:
            if(line.alpha == 0){
                lastLine = line;
            }
        }

        if(lastLine != null && previousLevel != 0){
            //Update the last and least visible line:
            lastLine.level = previousLevel;
            if(isScaled){
                float newAngle = getAngleForLevel(lastLine.level);
                lastLine.startPointX = getPointXForAngle(newAngle, radius);
                lastLine.startPointY = getPointYForAngle(newAngle, radius);
                lastLine.endPointX = getInnerPointXFromOuterPointX(lastLine.startPointX);
                lastLine.endPointY = getInnerPointYFromOuterPointY(lastLine.startPointY);
            }else{
                lastLine.startPointX = previousOuterPointX;
                lastLine.startPointY = previousOuterPointY;
                lastLine.endPointX   = previousInnerPointX;
                lastLine.endPointY   = previousInnerPointY;
            }
            lastLine.alpha       = 250;//Full visible
        }
    }

    private void calculateTimeline(boolean shouldRescale){
        int left      = 0;
        int bottom    = getHeight();
        int top       = (int) (getHeight()-timelineWidth);
        if(shouldRescale){
            timelineOpenRect.set(left, top, (int) getXByTime(timeLastInvest.getTime() - LAST_TEN_MINUTES_TO_INVEST_LENGTH_MS), bottom);
            timelineClosingRect.set((int) getXByTime(timeLastInvest.getTime() - LAST_TEN_MINUTES_TO_INVEST_LENGTH_MS), top, (int) getXByTime(timeLastInvest.getTime()), bottom);
            timelineExpiryRect.set((int) getXByTime(timeLastInvest.getTime()), top, (int) getXByTime(endTime), bottom);

            //Calculate spikes:
            spikesCount = getWidth() / spikesBitmap.getWidth();
            spikesTop   = getHeight() - (int) timelineWidth - spikesBitmap.getHeight();

            //Hours Label Text:
            hourLabelPaint.getTextBounds(hourLabelTextFormat, 0, hourLabelTextFormat.length(), hourLabelRect);
            hourLabelY = getHeight() - timelineWidth - hourLabelRect.bottom - hoursVerticalLinesWidth - hourLabelPadding;
            hoursLabelsCount = 5;
            int intervalsCount = hoursLabelsCount - 1;
            intervalWidth = ( getWidth() - hoursLabelsCount * hourLabelRect.width() ) / intervalsCount;
            timeInteval = timeLength / intervalsCount;

            isTimelineSetup = true;
        }

        timelineCurrentRect.set(left, top, (int) getXByTime(currentLevelTime), bottom);
    }

    /**
     * Calculate the coordinates of every investment.
     * @param isScaled If true : the coordinates need to be recalculated based on the investments values.
     */
    private void calculateInvestmentsPoints(boolean isScaled){
        for(RingChartInvestment investment : investmentsPointsList){
            if(isScaled){
                //Calculate the new coordinates:
                float angle = getAngleForLevel(investment.level);
                investment.pointX = getPointXForAngle(angle, radius + investmentPointRadiusOffset);
                investment.pointY = getPointYForAngle(angle, radius + investmentPointRadiusOffset);
            }
        }
    }

    /**
     * Return the corresponding angle for the given level.
     */
    private float getAngleForLevel(float level){
        return startAngle + (level - minLevel) / (maxLevel - minLevel) * sweepAngle;
    }

    /**
     * Return the X coordinate on the outer arc of the ring.
     * @param angle The angle.
     * @param radius The radius.
     */
    private float getPointXForAngle(float angle, float radius){
        float angleInRadians    = (float) Math.toRadians(angle);
        float angleCos          = (float) Math.cos(angleInRadians);
        return centerX + radius * angleCos;
    }

    /**
     * Return the Y coordinate on the outer arc of the ring.
     * @param angle The angle.
     * @param radius The radius.
     */
    private float getPointYForAngle(float angle, float radius){
        float angleInRadians    = (float) Math.toRadians(angle);
        float angleSinAbs       = (float) Math.abs(Math.sin(angleInRadians));
        return centerY - radius * angleSinAbs;
    }

    /**
     * Return the X coordinate on the inner arc of the ring.
     * @param outerPointX The corresponding X outer coordinate.
     */
    private float getInnerPointXFromOuterPointX(float outerPointX){
        return centerX + (outerPointX - centerX)*smallRadius/radius;
    }

    /**
     * Return the Y coordinate on the inner arc of the ring.
     * @param outerPointY The corresponding Y outer coordinate.
     */
    private float getInnerPointYFromOuterPointY(float outerPointY){
        return centerY + (outerPointY - centerY)*smallRadius/radius;
    }

    private void setupTimeLegth() {
        if (scheduled == Opportunity.SCHEDULED_HOURLY) {
            timeLength = timeEstClosing.getTime() - timeFirstInvest.getTime();
        } else {

            long hourlyOppInterval = oppTypeId == Opportunity.TYPE_REGULAR ? LENGTH_IN_MS_REGULAR : LENGTH_IN_MS_OPTION_PLUS;
            long currentTime = TimeUtils.getCurrentServerTime();
            long estClosing = timeEstClosing.getTime();

            //Initial interval:
            timeLength = hourlyOppInterval;
            long timeTemp = estClosing - timeLength;
            //If the initial interval is not enough, we increment it until the current time is in the visible part:
            while (timeTemp > currentTime) {
                timeTemp -= timeLength;
                timeLength += hourlyOppInterval;
            }
        }
    }

    /**
     * Returns the actual x coordinate by the given time.
     * @param time The time in milliseconds.
     */
    private float getXByTime(long time) {
        long timeInterval = time - startTime;
        return getWidth() * timeInterval / timeLength;
    }

    ////////////////////////////////////////////////////////////////////////
    //                          Drawing methods                           //
    ////////////////////////////////////////////////////////////////////////

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!isAvailable) {
            Log.d(TAG, "onDraw : Market is unavaiable.");
            return;
        }

        //Check if initial calculations are made:
        if (!isInitCalculated) {
            Log.d(TAG, "onDraw : Calculate init values.");
            calculateInitScale();
        }

        //Check if the initial values are loaded:
        if (!isDrawingsLoaded) {
            Log.d(TAG, "onDraw : Load init drawings.");
            initDrawingsValues();
        }

        drawGraph(canvas);
        drawHistoryLines(canvas);
        drawTexts(canvas);
        drawInvestments(canvas);
        drawTimeline(canvas);
    }

    /**
     * Draw :   Ring Chart (Light and Dark parts),
     *          Current Level Pointer.
     */
    private void drawGraph(Canvas canvas) {
        //Draw Bellow Dark:
        canvas.drawPath(arcBellowDarkPath, bellowDarkPaint);

        //Draw Above Light:
        canvas.drawPath(arcAboveDarkPath, aboveDarkPaint);

        //Draw Bellow Light:
        canvas.drawPath(arcBellowLightPath, bellowLightPaint);

        //Draw Above Light:
        canvas.drawPath(arcAboveLightPath, aboveLightPaint);

        //Draw the Current Level Arrow:
        canvas.drawPath(arrowLeftPath, arrowLeftPaint);
        canvas.drawPath(arrowRightPath, arrowRightPaint);
    }

    /**
     * Draw the history levels as lines that fade away aftr every following update.
     */
    private void drawHistoryLines(Canvas canvas){
        for(RingChartHistoryLine line : historyLinesList){
            historyLinePaint.setAlpha(line.alpha);
            canvas.drawLine(line.startPointX, line.startPointY, line.endPointX, line.endPointY, historyLinePaint);
        }
    }

    /**
     * Draw :   Center Level Text,
     *          Center Level Lines,
     *          Current Level Text,
     */
    private void drawTexts(Canvas canvas) {

        //Check the Current Angle - if the inclination from the center angle is less than 5 degrees,
        //we draw the center level bellow the Chart. Otherwise, on the center vertical line.
        if(currentAngle >= centerAngle- centerAngleInclination && currentAngle <= centerAngle+ centerAngleInclination){
            //Draw the Center Level Text (Bellow Chart):
            canvas.drawText(centerLevelNormalText, centerLevelBellowTextX, centerLevelBellowTextY, centerLevelTextBellowPaint);
            canvas.drawText(centerLevelBoldText, centerLevelBoldBellowTextX, centerLevelBoldBellowTextY, centerLevelBoldTextBellowPaint);
        }else{
            //Draw the Center Level Text (Vertical):
            canvas.save();
            canvas.rotate(90.0f, centerLevelRotatePivotX, centerLevelRotatePivotY);
            canvas.drawText(centerLevelNormalText, centerLevelTextX, centerLevelTextY, centerLevelTextPaint);
            canvas.drawText(centerLevelBoldText, centerLevelBoldTextX, centerLevelBoldTextY, centerLevelBoldTextPaint);
            canvas.restore();
        }

        //Draw the Center Level Dotted Lines:
        canvas.drawPath(centerLevelTopPath, centerLevelPaint);
        canvas.drawPath(centerLevelBottomPath, centerLevelPaint);

        //Draw the Current Level Text:
        canvas.save();
        canvas.rotate(currentLevelTextRotateAngle, currentOuterPointX, currentOuterPointY);
        canvas.drawText(currentLevelNormalText, currentLevelTextX, currentLevelTextY, currentLevelTextPaint);
        canvas.drawText(currentLevelBoldText, currentLevelBoldTextX, currentLevelBoldTextY, currentLevelBoldTextPaint);
        canvas.restore();
    }

    /**
     * Draw the investments circles.
     */
    private void drawInvestments(Canvas canvas){
        for(RingChartInvestment investment : investmentsPointsList){
            float level = investment.level;
            long type   = investment.type;
            //Check if the investment is winning or not:
            if(     (type == Investment.INVESTMENT_TYPE_DYNAMICS_BUY  && currentLevel > centerLevel)  ||
                    (type == Investment.INVESTMENT_TYPE_DYNAMICS_SELL && currentLevel < centerLevel)    ){
                investmentPaint.setColor(chartRingInvestmentWinColor);
            }else{
                investmentPaint.setColor(chartRingInvestmentLoseColor);
            }
            canvas.drawCircle(investment.pointX, investment.pointY, investmentPointRadius, investmentPaint);
        }
    }

    /**
     * Draw the timeline and hours.
     */
    private void drawTimeline(Canvas canvas) {
        if(!isTimelineSetup){
            return;
        }

        //Draw the open time:
        canvas.drawRect(timelineOpenRect, timelineOpenPaint);

        //Draw the closing time:
        canvas.drawRect(timelineClosingRect, timelineClosingPaint);

        //Draw the expiry time:
        canvas.drawRect(timelineExpiryRect, timelineExpiryPaint);

        //Draw the current time overlay:
        canvas.drawRect(timelineCurrentRect, timelineCurrentPaint);
        /////////////////////////////////////

        // DRAW the vertical SPIKES:
        float spikesLeft;
        for (int i = 0; i <= spikesCount; i++) {
            spikesLeft = i * spikesBitmap.getWidth();
            canvas.drawBitmap(spikesBitmap, spikesLeft, spikesTop, null);
        }
        /////////////////////////////////////

        // DRAW HOURS Labels
        for (int i = 0; i < hoursLabelsCount; i++) {
            float hoursLabelX = i * (hourLabelRect.width() + intervalWidth);
            long time = startTime + i * timeInteval;
            if (i == 0) {
                time = startTime;
            }
            if (i == hoursLabelsCount - 1) {
                time = endTime;
                hoursLabelX -= 1;
            }

            timeTempDate.setTime(time);
            canvas.drawText(timeFormat.format(timeTempDate), hoursLabelX , hourLabelY, hourLabelPaint);
        }
        ////////////////////////////////
    }

    ////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////
    /////               IGNORE methods:         //////////
    //////////////////////////////////////////////////////
    @Override
    public void zoomIn() {}
    @Override
    public void zoomOut() {}
    @Override
    public void removeZoom() {}
    @Override
    public boolean isZoomedIn() {return false;}
    @Override
    public void setOnScrollToLeftEdgeListener(OnScrollToLeftEdgeListener onScrollToLeftEdgeListener) {}
    @Override
    public void setChartDataPreviousHour(ChartDataMethodResult data) {}
    @Override
    public Investment getFocusedInvestment() {return null;}
    @Override
    public void requestInvestmentFocus(Investment inv) {}
    @Override
    public void setChartData(ChartDataMethodResult data) {}
    /////////////////////////////////////////////////
}