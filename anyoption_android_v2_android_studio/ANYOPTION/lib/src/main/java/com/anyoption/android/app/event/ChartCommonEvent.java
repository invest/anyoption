package com.anyoption.android.app.event;

import com.anyoption.common.service.results.ChartDataResult;

public class ChartCommonEvent {

	/**
	 * Chart event type
	 */
	public enum Type {
		DATA_REQUESTED,
		DATA_REQUEST_SUCCESS,
		DATA_REQUEST_FAIL
	}

	private long marketId;
	private Type type;
	private ChartDataResult chartData;

	public ChartCommonEvent(long marketId, Type type, ChartDataResult chartData) {
		this.marketId = marketId;
		this.type = type;
		this.chartData = chartData;
	}

	public ChartCommonEvent(long marketId, Type type) {
		this(marketId, type, null);
	}

	public long getMarketId() {
		return marketId;
	}

	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public ChartDataResult getChartData() {
		return chartData;
	}

	public void setChartData(ChartDataResult chartData) {
		this.chartData = chartData;
	}

}
