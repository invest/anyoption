package com.anyoption.android.app.manager;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.ViewGroup;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.fragment.my_account.questionnaire_new.QuestionnaireFragment;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.util.constants.Constants;

import java.util.List;
import java.util.Stack;

/**
 * Fragment Manager that manages the fragments transactions and "onBackPressed" functionality.
 * @author Anastas Arnaudov
 *
 */
public class FragmentManager {

	public static final String TAG = FragmentManager.class.getSimpleName();

	private Application application;

	/**
	 * Stack that represents the navigation within the application structure.
	 */
	private Stack<FragmentStackEntry> backStack;
	
	/**
	 * {@link FragmentManager}
	 * @param application
	 */
	public FragmentManager(Application application){
		Log.d(TAG, "On Create");
		this.application = application;
	
		backStack = new Stack<FragmentManager.FragmentStackEntry>();
	}

	/**
	 * Displays the given Dialog Fragment.
	 * 
	 * @see {@link #showDialogFragment(Class, Bundle)}
	 */
	public void showDialogFragment(DialogFragment dialog) {
		dialog.show(application.getCurrentActivity().getSupportFragmentManager(), null);
	}

	/**
	 * Displays the given Dialog Fragment checking for its subclasses in the applications which uses
	 * this library.
	 * 
	 * @see {@link #showDialogFragment(DialogFragment)}
	 */
	public BaseDialogFragment showDialogFragment(Class<? extends BaseDialogFragment> dialogClass,
			Bundle bundle) {
		@SuppressWarnings("unchecked")
		BaseDialogFragment dialog = BaseDialogFragment.newInstance(
				application.getSubClassForClass(dialogClass), bundle);
		dialog.show(application.getCurrentActivity().getSupportFragmentManager(), null);
		
		application.getPopUpManager().onPopupShown(dialog.getPopupCode());
		
		return dialog;
	}

	/**
	 * Search for the given fragment. This searches through fragments that are currently added to the manager's activity.
	 * @param tag The tag that was used when the fragment was added.
	 * @return The Fragment or null if there is no such fragment.
	 */
	private BaseFragment findFragment(String tag, int parentViewID){
		BaseFragment fragment = null;
		android.support.v4.app.FragmentManager manager = getFragmentManagerForView(parentViewID);
		fragment = (BaseFragment) manager.findFragmentByTag(tag);
		return fragment;
	}
	
	
	/**
	 * Displays the given Fragment. 
	 * Use this method to perform fragment transaction when you want them to be put on the back stack and the back button to do its job. 
	 * @param fragmentClass The Fragment class. The method uses Reflection to instantiate the fragment.
	 * @param parentViewID The ID of the ViewGroup in which the Fragment will be added to.
	 * @param fragment The Fragment.
	 * @param navigationType The type of the Navigation that lead to this fragment transaction.
	 */
	@SuppressWarnings("unchecked")
	public void openScreen(Class<? extends BaseFragment> fragmentClass, int parentViewID, NavigationEvent event){
		BaseFragment fragment = findFragment(event.getToScreen().getTag(), parentViewID);
		if(fragment == null || event.getBundle().containsKey(Constants.EXTRA_QUESTIONNAIRE_PAGE)){
			if (event.getFlags() != null && event.getFlags().length > 0) {
				if (event.getBundle() == null) {
					event.setBundle(new Bundle());
				}

				// Adds the flags to the arguments so they are used on UP navigation
				event.getBundle().putSerializable(Constants.EXTRA_FLAGS, event.getFlags());
			}

			fragment = BaseFragment.newInstance(application.getSubClassForClass(fragmentClass), new Bundle(event.getBundle()));
		}else{
			fragment.getArguments().clear();
			fragment.getArguments().putAll(event.getBundle());
		}
		addFragment(parentViewID, fragment, event.getNavigationType(), event.getBundle());
	}

	/**
	 * Displays the given Fragment. 
	 * Use this method to perform fragment transaction when you want them to be put on the back stack and the back button to do its job. 
	 * @param parentViewID The ID of the ViewGroup in which the Fragment will be added to.
	 * @param fragment The Fragment.
	 * @param navigationType The type of the Navigation that lead to this fragment transaction.
	 */
	public void addFragment(int parentViewID, BaseFragment fragment, NavigationType navigationType, Bundle bundle) {
		
		android.support.v4.app.FragmentManager manager = getFragmentManagerForView(parentViewID);

		FragmentTransaction transaction = manager.beginTransaction();		
		
		switch (navigationType) {	
			case INIT:
				removeAllFragmentsFromViewExcept(fragment, parentViewID, manager, transaction);
				clearStack();
				backStack.push(new FragmentStackEntry(fragment, parentViewID, bundle));
				addFragment(parentViewID, fragment, transaction);
				break;		
			case DEEP:
				//TODO remove instead of hiding
//				removeAllFragmentsFromViewExcept(fragment, parentViewID, manager, transaction);
				hideAllFragmentsFromViewExcept(fragment, parentViewID, manager, transaction);
				backStack.push(new FragmentStackEntry(fragment, parentViewID, bundle));
				addFragment(parentViewID, fragment, transaction);
				break;			
			case HORIZONTAL:
				//TODO remove instead of hiding
//				removeAllFragmentsFromViewExcept(fragment, parentViewID, manager, transaction);
				hideAllFragmentsFromViewExcept(fragment, parentViewID, manager, transaction);
				if(!backStack.isEmpty()){
					backStack.pop();
				}
				backStack.push(new FragmentStackEntry(fragment, parentViewID, bundle));
				addFragment(parentViewID, fragment, transaction);
				break;	
			case UP:
				//TODO remove instead of hiding
//				removeAllFragmentsFromViewExcept(fragment, parentViewID, manager, transaction);
				hideAllFragmentsFromViewExcept(fragment, parentViewID, manager, transaction);
				if(!backStack.isEmpty()){
					FragmentStackEntry lastEntry = backStack.pop();
					if (!lastEntry.getFragment().equals(backStack.peek().getFragment())) {
						transaction.remove(lastEntry.getFragment());
					}
				}
				addFragment(parentViewID, fragment, transaction);
				break;
			default:
				break;	
		}
		
		transaction.commit();	
	}

	/**
	 * Gets the Fragment Manager. The method checks whether the View with the given ID belongs to a Fragment.
	 * If true - we get the Child Fragment Manager of the parent Fragment.
	 * If false - we get the Activity Fragment Manager.
	 * @param viewID
	 * @return
	 */
	private android.support.v4.app.FragmentManager getFragmentManagerForView(int viewID){
		android.support.v4.app.FragmentManager manager;
		// TODO There is problem with this method.
		Fragment parentFragment = getFragmentForView(viewID);
		if(parentFragment != null){
			manager = parentFragment.getChildFragmentManager();
		}else{
			manager = application.getCurrentActivity().getSupportFragmentManager();	
		}
		
		return manager;
	}
	
	/**
	 * Adds the given fragment to the View with the given ID. 
	 * If it was previously added we simply show it, if not - we add it. 
	 * @param parentViewID
	 * @param fragment
	 */
	@SuppressWarnings("static-access")
	public void addFragment(int parentViewID, BaseFragment fragment){
		android.support.v4.app.FragmentManager manager = getFragmentManagerForView(parentViewID);
		FragmentTransaction transaction = manager.beginTransaction();		
		if(fragment.isAdded()){
			transaction.show(fragment);
		}else{
			transaction.add(parentViewID, fragment, fragment.TAG);					
		}
		transaction.commit();
	}

	/**
	 * Use {@link #replaceFragment(int, BaseFragment, android.support.v4.app.FragmentManager)} instead of this method.
	 * 
	 * @param parentViewID
	 * @param fragment
	 */
	@Deprecated
	@SuppressWarnings("static-access")
	public void replaceFragment(int parentViewID, BaseFragment fragment) {
		// TODO getFragmentManagerForView method causes problems
		android.support.v4.app.FragmentManager manager = getFragmentManagerForView(parentViewID);
		FragmentTransaction transaction = manager.beginTransaction();
		hideAllFragmentsFromViewExcept(fragment, parentViewID, manager, transaction);
		
		if(fragment.isAdded()){
			showFragment(fragment, transaction);
		}else{
			transaction.add(parentViewID, fragment, fragment.TAG);	
		}
		
		transaction.commit();
	}
	
	/**
	 * Displays the given Fragment. It does not add the fragment to the back stack. 
	 * <p><b>NOTE: </b>If you want the back button to reverse this transaction use {@link FragmentManager#addFragment(int, BaseFragment, NavigationType)} method instead.  
	 * @param parentViewID The ID of the ViewGroup in which the Fragment will be added to.
	 * @param fragment The Fragment.
	 * @param fragmentManager
	 */
	@SuppressWarnings("static-access")
	public void replaceFragment(int parentViewID, BaseFragment fragment, android.support.v4.app.FragmentManager fragmentManager) {
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		hideAllFragmentsFromViewExcept(fragment, parentViewID, fragmentManager, transaction);
		
		if(fragment.isAdded()){
			showFragment(fragment, transaction);
		}else{
			transaction.add(parentViewID, fragment, fragment.TAG);	
		}
		
		transaction.commit();
	}
	
	/**
	 * Displays the given Fragment. It does not add the fragment to the back stack. 
	 * <p><b>NOTE: </b>If you want the back button to reverse this transaction use {@link FragmentManager#addFragment(int, BaseFragment, NavigationType)} method instead.  
	 * @param parentViewID The ID of the ViewGroup in which the Fragment will be added to.
	 * @param fragmentClass The fragment class
	 * @param parentfragment The Parent fragment of the nested fragment
	 */
	@SuppressWarnings("unchecked")
	public void replaceInnerFragment(int parentViewID, Class<? extends BaseFragment> fragmentClass, BaseFragment parentfragment) {
		android.support.v4.app.FragmentManager manager = parentfragment.getChildFragmentManager();
		FragmentTransaction transaction = manager.beginTransaction();

		BaseFragment fragment = BaseFragment.newInstance(application.getSubClassForClass(fragmentClass), new Bundle());
		transaction.replace(parentViewID, fragment);
		
		transaction.commit();
	}
	
	/**
	 * Displays the given Fragment. It does not add the fragment to the back stack. 
	 * <p><b>NOTE: </b>If you want the back button to reverse this transaction use {@link FragmentManager#addFragment(int, BaseFragment, NavigationType)} method instead.  
	 * @param parentViewID The ID of the ViewGroup in which the Fragment will be added to.
	 * @param fragmentClass The fragment class
	 * @param parentfragment The Parent fragment of the nested fragment
	 * @param bundle Bundle.
	 */
	@SuppressWarnings("unchecked")
	public void replaceInnerFragment(int parentViewID, Class<? extends BaseFragment> fragmentClass, BaseFragment parentfragment, Bundle bundle) {
		android.support.v4.app.FragmentManager manager = parentfragment.getChildFragmentManager();
		FragmentTransaction transaction = manager.beginTransaction();

		BaseFragment fragment = BaseFragment.newInstance(application.getSubClassForClass(fragmentClass), (bundle == null) ? new Bundle() : bundle);
		transaction.replace(parentViewID, fragment);
		
		transaction.commit();
	}
	
	/**
	 * Shows the given Fragment and all its nested fragments.
	 * The method iterates recursively through all nested fragments and shows them.
	 * @param fragment
	 */
	private void showFragment(Fragment fragment, FragmentTransaction transaction){
		transaction.show(fragment);
		List<Fragment> fragmentsList = fragment.getChildFragmentManager().getFragments();
		
		if(fragmentsList == null || fragmentsList.isEmpty()){
			return;
		}else{
			for(Fragment fr : fragmentsList){
				if(fr != null){
					showFragment(fr, transaction);					
				}
			}			
		}
	}
	
	/**
	 * Adds the given fragment. If it was previously added we simply show it, if not - we add it.
	 * <p>This is different from {@link FragmentManager#addFragment(int, BaseFragment)} as it accepts the transaction as parameter
	 * and do not perform the commit (it just do its job and lets others to do more actions and to do the commit).
	 * @param parentViewID
	 * @param fragment
	 * @param transaction
	 */
	private void addFragment(int parentViewID, BaseFragment fragment, FragmentTransaction transaction){		
		if(fragment.isAdded()){
			if(!fragment.isVisible()){
				showFragment(fragment, transaction);				
			}
		}else{
			if(fragment instanceof NavigationalFragment){
				if(fragment.getArguments().containsKey(Constants.EXTRA_QUESTIONNAIRE_PAGE)) {
					transaction.replace(parentViewID, fragment, ((NavigationalFragment)fragment).getScreen().getTag() + String.valueOf(fragment.getArguments().getInt(Constants.EXTRA_QUESTIONNAIRE_PAGE)));
					} else {
					transaction.add(parentViewID, fragment, ((NavigationalFragment) fragment).getScreen().getTag());
				}
			}
		}
	}
	
	public void removeAllFragmentsFromView(int parentViewID) {
		android.support.v4.app.FragmentManager manager = getFragmentManagerForView(parentViewID);
		FragmentTransaction transaction = manager.beginTransaction();

		List<Fragment> fragments = manager.getFragments();
		if (fragments != null) {
			for (int i = 0; i < manager.getFragments().size(); i++) {
				Fragment fr = manager.getFragments().get(i);
				if ((fr != null)
						&& ((fr.getView() != null && (((ViewGroup) fr.getView().getParent())
								.getId() == parentViewID)))) {
					transaction.remove(fr);
					hideNestedFragments(fr, transaction);
				}
			}
		}

		transaction.commit();
	}
	
	/**
	 * Hides all fragments (except for the given one) that are added to the View with the given id.
	 * <p>This methods accepts the transaction as parameter and do not perform the commit (it just do its job and lets others to do more actions and to do the commit).
	 * @param fragment The fragment which should not be hide.
	 * @param parentViewID The id of the view which has fragments added to it.
	 * @param manager
	 * @param transaction
	 */
	private void hideAllFragmentsFromViewExcept(BaseFragment fragment, int parentViewID, android.support.v4.app.FragmentManager manager, FragmentTransaction transaction){

/*		for(int i = 0; i < backStack.size(); i++){
			FragmentStackEntry entry = backStack.get(i);
			if((entry.getParentViewID() == parentViewID) && (entry.getFragment() != fragment)){
				transaction.hide(entry.getFragment());
				hideNestedFragments(entry.getFragment(), transaction);
			}
		}*/
		
		// The support Fragment Manager keeps references to Fragments that are added to the Activity:
		List<Fragment> fragments = manager.getFragments();
		if(fragments != null){
			for(int i = 0; i < manager.getFragments().size(); i++){
				Fragment fr = manager.getFragments().get(i);
				if( (fr != null) && ( ((fr != fragment) && fr.getView() != null && (((ViewGroup)fr.getView().getParent()).getId() == parentViewID)) )){
					transaction.hide(fr);
					hideNestedFragments(fr, transaction);
				}
			}			
		}
	}
	
	/**
	 * Removes all fragments that are added to the View with the given ID.
	 * <p>This is method accepts the transaction as parameter and do not perform the commit (it just do its job and lets others to do more actions and to do the commit).
	 * @param fragment
	 * @param parentViewID
	 * @param manager
	 * @param transaction
	 */
	private void removeAllFragmentsFromViewExcept(BaseFragment fragment, int parentViewID, android.support.v4.app.FragmentManager manager, FragmentTransaction transaction){
/*		for(int i = 0; i < backStack.size(); i++){
			FragmentStackEntry entry = backStack.get(i);
			if((entry.getParentViewID() == parentViewID) && (entry.getFragment() != fragment)){
				Log.d(TAG, "Remove fragment " + entry);
				transaction.remove(entry.getFragment());
				removeNestedFragments(entry.getFragment(), transaction);
			}
		}*/
		
		
		// The support Fragment Manager keeps references to Fragments that are added to the Activity:
		List<Fragment> fragments = manager.getFragments();
		if(fragments != null){
			for(int i = 0; i < manager.getFragments().size(); i++){
				Fragment fr = manager.getFragments().get(i);
				if( (fr != null && fr != fragment) && ( (fr.getView() != null && (((ViewGroup)fr.getView().getParent()).getId() == parentViewID)) || ( !fr.isInLayout() && !fr.isVisible()) )){
					transaction.remove(fr);
					removeNestedFragments(fr, transaction);
				} 
			}
		}
	}
	
	/**
	 * Adds the given fragment to the View with the given ID. 
	 * If it was previously added we simply show it, if not - we add it. 
	 * @param parentViewID
	 * @param parentFragment The Parent Fragment.
	 * @param fragmentClass
	 * @param tag
	 * @returns The Fragment. 
	 */
	@SuppressWarnings("unchecked")
	public Fragment addFragment(int parentViewID, Fragment parentFragment, Class<? extends BaseFragment> fragmentClass, String tag){
		android.support.v4.app.FragmentManager manager = parentFragment.getChildFragmentManager();
		FragmentTransaction transaction = manager.beginTransaction();		
		
		BaseFragment fragment = findFragment(tag, parentViewID);
		if(fragment == null){
			fragment = BaseFragment.newInstance(application.getSubClassForClass(fragmentClass), new Bundle());
		}
		if(fragment.isAdded()){
			transaction.show(fragment);
		}else{
			transaction.add(parentViewID, fragment, tag);					
		}
		transaction.commit();
		
		return fragment;
	}

	public Fragment addFragment(int parentViewID, Fragment parentFragment, Class<? extends BaseFragment> fragmentClass, String tag, Bundle bundle){
		android.support.v4.app.FragmentManager manager = parentFragment.getChildFragmentManager();
		FragmentTransaction transaction = manager.beginTransaction();

		BaseFragment fragment = findFragment(tag, parentViewID);
		if(fragment == null){
			if(bundle != null) {
				fragment = BaseFragment.newInstance(application.getSubClassForClass(fragmentClass), bundle);
			} else {
				fragment = BaseFragment.newInstance(application.getSubClassForClass(fragmentClass), new Bundle());
			}
		}
		if(fragment.isAdded()){
			transaction.show(fragment);
		}else{
			transaction.add(parentViewID, fragment, tag);
		}
		transaction.commit();

		return fragment;
	}

	/**
	 * Recursively removes all nested Fragments if the given Fragment.
	 * The method does not commit the transaction.
	 * @param fragment
	 * @param transaction
	 */
	private void removeNestedFragments(Fragment fragment, FragmentTransaction transaction){
		List<Fragment> fragmentsList = fragment.getChildFragmentManager().getFragments();
		
		if(fragmentsList == null || fragmentsList.isEmpty()){
			return;
		}else{
			for(Fragment fr : fragmentsList){
				if(fr != null){
					transaction.remove(fr);
					removeNestedFragments(fr, transaction);					
				}
			}			
		}
	}
	
	/**
	 * Recursively hides all nested Fragments if the given Fragment.
	 *  The method does not commit the transaction.
	 * @param fragment
	 * @param transaction
	 */
	private void hideNestedFragments(Fragment fragment, FragmentTransaction transaction){
		List<Fragment> fragmentsList = fragment.getChildFragmentManager().getFragments();
		
		if(fragmentsList == null || fragmentsList.isEmpty()){
			return;
		}else{
			for(Fragment fr : fragmentsList){
				if(fr != null){
					transaction.hide(fr);
					hideNestedFragments(fr, transaction);					
				}
			}			
		}
	}
	
	/**
	 * Returns the Fragment that holld the View with the given viewID.
	 * @param viewID
	 * @return
	 */
	private Fragment getFragmentForView(int viewID){
		Fragment result = null;
		List<Fragment> fragmentsList = application.getCurrentActivity().getSupportFragmentManager().getFragments();
		
		if(fragmentsList != null && !fragmentsList.isEmpty()){
			for(Fragment fragment : fragmentsList){
				if(fragment != null){
					if(fragment.getView() != null && fragment.getView().findViewById(viewID) != null){
						return fragment;
					}
					result = getFragmentForView(viewID, fragment);
					if(result != null){
						return result;
					}					
				}
			}
		}
		return result;
	}
	
	/**
	 * Recursively checks the nested fragments of the given fragment and returns the one which holds the VCiew with the given viewID.
	 * @param viewID
	 * @param fragment
	 * @return
	 */
	private Fragment getFragmentForView(int viewID, Fragment fragment){
		List<Fragment> fragmentsList = fragment.getChildFragmentManager().getFragments();
		Fragment result = null;
		if(fragmentsList == null || fragmentsList.isEmpty()){
			return result;
		}else{
			for(Fragment fr : fragmentsList){
				if(fr != null){
					if(fr.getView() != null && fr.getView().findViewById(viewID) != null){
						result = fr;
						return result;
					}
					result = getFragmentForView(viewID, fr);					
				}
			}	
			return result;
		}
	}

	public Screenable getPreviousScreen() {
		if (backStack.size() > 1) {
			FragmentStackEntry entry = backStack.elementAt(backStack.size() - 2);

			BaseFragment baseFragment = entry.getFragment();
			if (baseFragment instanceof NavigationalFragment) {
				NavigationalFragment screenFragment = (NavigationalFragment) baseFragment;
				return screenFragment.getScreen();
			}
		}
			return null;
	}

	public boolean hasPreviousScreen() {
		if (backStack.size() > 1) {
			return true;
		}
		return false;
	}

	/**
	 * Navigates UP in the application screen hierarchy.
	 */
	public void onBack() {
		if (backStack.size() > 1) {
			FragmentStackEntry entry = backStack.elementAt(backStack.size() - 2);

			BaseFragment baseFragment = entry.getFragment();
			if (baseFragment instanceof NavigationalFragment) {
				NavigationalFragment screenFragment = (NavigationalFragment) baseFragment;
				//Get the Screenable of the fragment in the last level and post Navigation event with that Screenable:
				Screenable screen = screenFragment.getScreen();

				Bundle bundle = null;
				if (entry.getBundle() != null) {
					bundle = entry.getBundle();
				} else {
					bundle = new Bundle();
				}

				if (backStack.size() == 2) {
					//We are navigating to the bottom element of the stack:
					bundle.putBoolean(Constants.EXTRA_NAVIGATION_TO_STACK_BOTTOM, true);
				}

				NavigationEvent event = new NavigationEvent(screen, NavigationType.UP, bundle);
				// Use the flags from arguments
				if (bundle.containsKey(Constants.EXTRA_FLAGS)) {
					int[] flags = (int[]) bundle.getSerializable(Constants.EXTRA_FLAGS);
					event.setFlags(flags);
				}
				application.postEvent(event);
			}
		} else {
			//There is only 1 (or 0) fragment in the Stack so we navigate back from this Activity:
			application.getCurrentActivity().finish();
			clearStack();
		}
	}

	/**
	 * Clears the {@link FragmentManager#backStack}.
	 */
	public void clearStack(){
		backStack.clear();
	}
	
	public int getStackSize() {
		return backStack.size();
	}

	public void removeStackItems(int count) {
		if (count < backStack.size()) {
			for (int i = 0; i < count; i++) {
				backStack.pop();
			}
		} else {
			backStack.clear();
		}
	}
	
	/**
	 * Element in the Custom Fragment Back Stack.
	 * 
	 * @author Anastas Arnaudov
	 * 
	 */
	public static class FragmentStackEntry {

		private BaseFragment fragment;
		private int parentViewID;
		private Bundle bundle;

		public FragmentStackEntry(BaseFragment fragment, int parentViewID, Bundle bundle) {
			this.fragment = fragment;
			this.parentViewID = parentViewID;
			this.bundle = bundle;
		}

		public FragmentStackEntry(BaseFragment fragment, int parentViewID) {
			this(fragment, parentViewID, null);
		}

		public BaseFragment getFragment() {
			return fragment;
		}

		public int getParentViewID() {
			return parentViewID;
		}

		public Bundle getBundle() {
			return bundle;
		}

		public void setBundle(Bundle bundle) {
			this.bundle = bundle;
		}

		@Override
		public String toString() {
			return fragment.getTag();
		}

	}
	
}
