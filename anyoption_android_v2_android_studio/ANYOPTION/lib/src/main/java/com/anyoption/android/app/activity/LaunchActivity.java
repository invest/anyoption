package com.anyoption.android.app.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.ConnectionStateChangeEvent;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.event.PushNotificationsPermissionEvent;
import com.anyoption.android.app.event.TimeOffsetUpdateEvent;
import com.anyoption.android.app.event.UpdateSkipEvent;
import com.anyoption.android.app.fragment.ActionFragment;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.fragment.StatusFragment;
import com.anyoption.android.app.fragment.launch.PushNotificationsAlertFragment;
import com.anyoption.android.app.fragment.launch.UpdateFragment;
import com.anyoption.android.app.fragment.launch.WelcomeFragment;
import com.anyoption.android.app.manager.AppseeManager;
import com.anyoption.android.app.manager.GCMManager.GooglePlayServicesCode;
import com.anyoption.android.app.manager.OnPermissionsGrantResultListener;
import com.anyoption.android.app.manager.PopUpManager.OnConfirmPopUpButtonPressedListener;
import com.anyoption.android.app.manager.PushNotificationsManager;
import com.anyoption.android.app.util.BitmapUtils;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.MarketGroup;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.results.CountriesListMethodResult;
import com.anyoption.common.service.results.MarketsGroupsMethodResult;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.VisibleSkinListMethodResult;
import com.anyoption.json.requests.CheckUpdateMethodRequest;
import com.anyoption.json.results.UpdateMethodResult;
import com.appoxee.inbox.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * Activity that is stared on Application start.
 * <p>It manages the Launch Process flow and makes some initial setup.
 * @author Anastas Arnaudov
 *
 */
public class LaunchActivity extends BaseActivity implements OnPermissionsGrantResultListener {

    public static final String TAG = LaunchActivity.class.getSimpleName();

    public static final String[] permissions = new String[]{Manifest.permission.INTERNET, Manifest.permission.CALL_PHONE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.GET_ACCOUNTS};

    public static final int MAX_ATTEMPTS_BEFORE_DIALOG_FOR_CONTINUE = 3;
    private boolean isShouldChangeLocale;
    private String locale;
    private boolean isAppRestarted;
    private boolean isActivityRecreated;
    private boolean isShouldShowWelcome;

    private enum InitialValuesLoadStatus {
        SERVICE_NOT_CALLED,
        SERVICE_CALLED,
        SERVICE_CALLED_RETURNED_ERROR_RESPONSE,
        SERVICE_CALLED_RETURNED_OK_RESPONSE
    }

    protected PushNotificationsManager pushNotificationsManager;

    private int initialValuesAttempts = 0;

    private static boolean isTryAgainPopupShown = false;
    private static boolean isAskAgainPopupShown = false;

    private RelativeLayout splashLayout;
    private View mainView;
    private View promotionLayout;
    protected View statusBarLayout;
    protected View actionBarLayout;
    protected View statusBarSeparator;
    protected View actionBarSeparator;

    private InitialValuesLoadStatus isCheckForUpdateLoaded  = InitialValuesLoadStatus.SERVICE_NOT_CALLED;
    private InitialValuesLoadStatus isCountriesLoaded       = InitialValuesLoadStatus.SERVICE_NOT_CALLED;
    private InitialValuesLoadStatus isSkinsLoaded           = InitialValuesLoadStatus.SERVICE_NOT_CALLED;
    private InitialValuesLoadStatus isMarketsLoaded         = InitialValuesLoadStatus.SERVICE_NOT_CALLED;

    protected boolean isFirstOpening;
    private Long skinID;


	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		AppseeManager.onStart();
		
		pushNotificationsManager = application.getPushNotificationsManager();
		
		application.getDeepLinksManager().parseIntent(this, getIntent());
		
		setContentView(R.layout.launch_screen_layout);
		getWindow().setBackgroundDrawable(null);
		StatusFragment statusFragment = (StatusFragment) BaseFragment.newInstance(application.getSubClassForClass(StatusFragment.class),null);
		application.getFragmentManager().addFragment(R.id.launch_status_layout, statusFragment);
		ActionFragment actionFragment = (ActionFragment) BaseFragment.newInstance(application.getSubClassForClass(ActionFragment.class),null);
		application.getFragmentManager().addFragment(R.id.launch_action_layout, actionFragment);
		actionFragment.setType(ActionFragment.TYPE_GENERAL);
		actionFragment.registerForEvents();
		
		splashLayout = (RelativeLayout) findViewById(R.id.launch_splash_layout);
		setupSplashScreen();
		TextView splashAppVersion = (TextView) findViewById(R.id.launch_splash_version);
		splashAppVersion.setText(" " + application.getApplicationVersionName());

        mainView = findViewById(R.id.launch_main_layout);

        statusBarLayout = findViewById(R.id.launch_status_layout);
        actionBarLayout = findViewById(R.id.launch_action_layout);
        statusBarSeparator = findViewById(R.id.status_bar_separator);
        actionBarSeparator = findViewById(R.id.action_bar_separator);

		promotionLayout = findViewById(R.id.launch_promotion_layout);
		promotionLayout.setVisibility(View.GONE);

		isFirstOpening = Utils.checkFirstOpening();
		application.registerForEvents(this, NavigationEvent.class, UpdateSkipEvent.class, ConnectionStateChangeEvent.class);

        //Check Intent parameters in case the Activity is Restarted:
        Intent intent = getIntent();
        Bundle intentBundle = getIntent().getExtras();
        if(intent != null){
            Log.d(TAG, "Intent = " + intent.toString());
            if(intentBundle != null){
                Log.d(TAG, "Intent Bundle = " + intentBundle.toString());
            }
        }
        isShouldShowWelcome = getIntent().getBooleanExtra(Constants.EXTRA_IS_SHOULD_SHOW_WELCOME, false);
        isAppRestarted      = getIntent().getBooleanExtra(Constants.EXTRA_IS_APP_RESTARTED, false);
        isActivityRecreated = getIntent().getBooleanExtra(Constants.EXTRA_IS_ACTIVITY_RECREATED, false);
        if (intentBundle != null && isAppRestarted && !isActivityRecreated) {
            Log.d(TAG, "App was restarted!");
            handleRestart(intentBundle);
        }

        long minInvestmentAmount = getIntent().getLongExtra(Constants.EXTRA_MIN_INV_AMOUNT, 0L);
        application.setMinInvestmentAmount(minInvestmentAmount);

        //Check if the Google Play Services Error Dialog will be displayed:
        if (BaseActivity.isPushNotificationsSupported == GooglePlayServicesCode.ERROR_RECOVERABLE) {
            return;
        }
        ////////////////////////////////
        isTryAgainPopupShown = false;
        isAskAgainPopupShown = false;

        checkForPermissions();
    }

    public void checkForPermissions() {
        //CHECK FOR NEW PERMISSIONS MANAGER, SINCE ANDROID 6(sdk 23)
        if (Utils.getDeviceOSVersionCode() >= 23) {
            Log.d(TAG, "Check for permissions.");
            if (application.getPermissionsManager().hasPermissions(permissions)) {
                if(application.getPermissionsManager().canDrawOverlays()) {
                    if(isShouldShowWelcome){
                        showWelcomeScreen();
                    }else{
                        getInitialValues();
                    }
                } else {
                    application.getPermissionsManager().askForDrawOverlaysPermission();
                }
            } else {
                if (!application.getPermissionsManager().isOnPermissionsGrantResultListener(this)) {
                    application.getPermissionsManager().setOnPermissionsGrantResultListener(this);
                }
                Log.d(TAG, "Asking for permissions.");
                application.getPermissionsManager().requestPermissions(permissions);
            }
        } else {
            if(isShouldShowWelcome){
                showWelcomeScreen();
            }else{
                getInitialValues();
            }
        }
    }

	protected void handleRestart(Bundle intentBundle) {
        Log.d(TAG, "handleRestart");
        Object objectUser = intentBundle.get(Constants.EXTRA_USER);
		Log.d(TAG, intentBundle.toString());
		if (objectUser != null && objectUser instanceof User) {
			User user = (User) intentBundle.get(Constants.EXTRA_USER);
			UserRegulationBase userRegulations = (UserRegulationBase) intentBundle.get(Constants.EXTRA_USER_REGULATIONS);
			Log.d(TAG, "#################");
			Log.d(TAG, "Getting Saved User");
			Log.d(TAG, "Username = " + user.getUserName());
			Log.d(TAG, "#################");

			application.saveUser(user, userRegulations);
		}
	}
	
	protected void setupSplashScreen() {
		ImageView splashForeground = (ImageView) findViewById(R.id.launch_splash_foreground);
		BitmapUtils.loadBitmap(R.drawable.splash_logo_and_graphics, splashForeground);
	}
	
	private void checkForPromotion() {
		// REMOVED BECAUSE CAUSES A LONG LOADING
		/*application.getAppoxeeManager().hasInboxMessages(new InboxListener() {
			@Override
			public void gotInboxMessages(List<Message> inboxMessagesList) {
				if(inboxMessagesList != null && !inboxMessagesList.isEmpty()) {
					removeSplash();
					promotionLayout.setVisibility(View.VISIBLE);
					showPromotion(inboxMessagesList.get(inboxMessagesList.size() - 1));
				}else {
					promotionLayout.setVisibility(View.GONE);
					handleWelcome();
				}
			}
		});*/
		handleWelcome();
	}

	private void showPromotion(Message message) {
		WebView webView = (WebView) findViewById(R.id.launch_promotion_web_view);
        webView.loadUrl(message.getLink());
		View skipButton = findViewById(R.id.launch_promotion_skip_button);
		skipButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				promotionLayout.setVisibility(View.GONE);
				handleWelcome();
			}
		});
	}
	
	@Override
	protected void onResume() {
		application.registerForEvents(this, NavigationEvent.class, UpdateSkipEvent.class, ConnectionStateChangeEvent.class);
		super.onResume();
		
		// Check if we navigate from a Push Notification:
		Intent intent = getIntent();
		if (intent != null && intent.getExtras() != null) {
			boolean isFromPush = intent.getExtras().getBoolean(Constants.EXTRA_PUSH_NOTIFICATION, false);
			if (isFromPush) {
				Log.d(TAG, "Comming from a Push Notification...");
				intent.removeExtra(Constants.EXTRA_PUSH_NOTIFICATION);
				pushNotificationsManager.handlePush(new Bundle(intent.getExtras()));
			}
		}
	}
	
	@Override
	protected void onPause() {
		application.unregisterForAllEvents(this);
        application.registerForEvents(this, NavigationEvent.class);
		super.onPause();
	}
	
	@Override
	protected void onDestroy() {
		application.unregisterForAllEvents(this);
		super.onDestroy();
	}

    /**
     * Called when a {@link ConnectionStateChangeEvent} occurs.
     *
     * @param event
     */
    public void onEvent(ConnectionStateChangeEvent event) {
        if (application.getConnectionManager().checkConnection()) {
            getInitialValues();
        }
    }

    public void getInitialValues() {
        Log.d(TAG, "Trying to load Initial Values.");
        if (isAppRestarted || isActivityRecreated) {
            Log.d(TAG, ((isAppRestarted) ? "App was restarted" : "Activity was recreated"));
            isCheckForUpdateLoaded = InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_OK_RESPONSE;
            isMarketsLoaded = InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_OK_RESPONSE;
        } else {
            checkForUpdate();
            getMarketsGroups();

            //Fire pixel for adquant
            if (isFirstOpening) {
                Log.d(TAG, "Application is installed.");
                application.getDeepLinksManager().fireServerPixelInstall();
            }
            application.requestLogoutPredefinedAmounts();
        }
        getCountriesForSkin(); //Depends on the skin
        getSkins();            //Depends on the skin
    }

    public void checkInitialValuesAndContinue() {
        if (    isCheckForUpdateLoaded == InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_OK_RESPONSE &&
                isCountriesLoaded      == InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_OK_RESPONSE &&
                isSkinsLoaded          == InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_OK_RESPONSE &&
                isMarketsLoaded        == InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_OK_RESPONSE   ) {

            //Do the restart here (if it is needed):
            if (isShouldChangeLocale) {
                application.changeLocale(locale, null);
            }else{
                Screenable startScreen = ((Screenable) getIntent().getSerializableExtra(Constants.EXTRA_SCREEN));
                Bundle bundle = getIntent().getBundleExtra(Constants.EXTRA_BUNDLE);
                boolean shouldGoToMain = getIntent().getBooleanExtra(Constants.EXTRA_GO_TO_MAIN, false);
                Log.d(TAG, "SHOULD OPEN MAIN ACTIVITY = " + shouldGoToMain);
                if (startScreen != null) {
                    Log.d(TAG, "START SCREEN = " + startScreen);
                    if (shouldGoToMain) {
                        Bundle bu = new Bundle();
                        bu.putSerializable(Constants.EXTRA_SCREEN, startScreen);
                        openMainActivity(bu);
                    } else {
                        application.postEvent(new NavigationEvent(startScreen, NavigationType.HORIZONTAL, bundle));
                    }
                    return;
                }
                Log.d(TAG, "NO START SCREEN");

                checkForWelcome();
            }
        }
    }

    /**
     * Check if there are any updates for the Application.
     */
    private void checkForUpdate() {
        application.getConnectionManager().checkConnectionAndSetupReceiver();

        if (isCheckForUpdateLoaded == InitialValuesLoadStatus.SERVICE_NOT_CALLED || isCheckForUpdateLoaded == InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_ERROR_RESPONSE) {
            CheckUpdateMethodRequest request = new CheckUpdateMethodRequest();
            request.setApkVersion(application.getApplicationVersionCode());
            request.setApkName(getString(R.string.apkName));
            request.setCombinationId(Utils.getCombinationId());
            request.setDynamicParam(Utils.getDynamicParam());
            request.setAffSub1(Utils.getAffSub1(this));
            request.setAffSub2(Utils.getAffSub2(this));
            request.setAffSub3(Utils.getGclid(this));
            request.setmId(application.getSharedPreferencesManager().getString(Constants.PREFERENCES_MID, null));
            request.setEtsMId(application.getSharedPreferencesManager().getString(Constants.PREFERENCES_ETS_MID, null));
            request.setFirstOpening(isFirstOpening);

            request.setOsTypeId(1);
            request.setAppsflyerId(application.getAppsFlyerManager().getAppsFlyerUID());
            request.setDeviceFamily(ScreenUtils.isTablet() ? "1" : "0");

            request.setAdvertisingId(Utils.getAndroidAdvertisingID());

            if (application.getCommunicationManager().requestService(this, "checkForUpdateCallBack", Constants.SERVICE_CHECK_FOR_UPDATE, request, UpdateMethodResult.class, false, false)) {
                isCheckForUpdateLoaded = InitialValuesLoadStatus.SERVICE_CALLED;
                initialValuesAttempts++;
            }
        }
    }

    /**
     * CallBack method for the Check For Update Request.
     *
     * @param result
     */
    public void checkForUpdateCallBack(Object result) {
        Log.d(TAG, "checkForUpdateCallBack");

        if (result == null && initialValuesAttempts < MAX_ATTEMPTS_BEFORE_DIALOG_FOR_CONTINUE) {
            isCheckForUpdateLoaded = InitialValuesLoadStatus.SERVICE_NOT_CALLED;
            //There is a problem with checkForUpdate request : sometimes the response is null and "Try again" fixes the issue.
            getInitialValues();
            return;
        } else if(result == null && initialValuesAttempts >= MAX_ATTEMPTS_BEFORE_DIALOG_FOR_CONTINUE) {
            isCheckForUpdateLoaded = InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_ERROR_RESPONSE;
            showNotLoadedInitialValuesError(null);
        }

        if (result != null) {

            UpdateMethodResult updateResult = (UpdateMethodResult) result;

            if (updateResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {

                application.setIsHasDynamisLogout(updateResult.isHasDynamics());
                isShouldChangeLocale = false;
                locale = null;
                //Setting the SKIN:
                skinID = updateResult.getSkinId();
                Skin skin = application.getSkins().get(skinID);
                if (skin != null) {
                    application.setSkinId(skinID);
                    application.getSkins().get(application.getSkinId()).setDefaultCurrencyId((int) (updateResult.getCurrencyId()));
                    locale = skin.getLocale().substring(0, 2);
                    //Check if we need to change the Locale:
                    isShouldChangeLocale = application.getLocale() == null || !application.getLocale().getLanguage().equals(application.getSupportedLanguageForLocale(locale));
                }

                //The Country ID should be != null only if the deviceID is not saved on the server:
                if (updateResult.getCountryId() != 0) {
                    application.setCountryId(updateResult.getCountryId());
                }
                if (updateResult.getCombId() > 0) {
                    if (!application.getSharedPreferencesManager().getBoolean(Constants.PREFERENCES_IS_CAME_FROM_MARKETING, false)) {
                        application.getSharedPreferencesManager().putLong(Constants.PREFERENCES_COMBINATION_ID, updateResult.getCombId());
                        Log.d(TAG, "Set combid from checkForUpdateResult with combid: " + updateResult.getCombId());
                    }
                }
                if (updateResult.getDynamicParam() != null) {
                    application.getSharedPreferencesManager().putString(Constants.PREFERENCES_DYNAMIC_PARAM, updateResult.getDynamicParam());
                }
                if (updateResult.getAff_sub1() != null) {
                    application.getSharedPreferencesManager().putString(Constants.PREFERENCES_AFF_SUB1, updateResult.getDynamicParam());
                }
                if (updateResult.getAff_sub2() != null) {
                    application.getSharedPreferencesManager().putString(Constants.PREFERENCES_AFF_SUB2, updateResult.getDynamicParam());
                }
                if (updateResult.getAff_sub3() != null) {
                    application.getSharedPreferencesManager().putString(Constants.PREFERENCES_GCLID, updateResult.getDynamicParam());
                }
                if (updateResult.getmId() != null && updateResult.getmId().length() > 0) {
                    application.getSharedPreferencesManager().putString(Constants.PREFERENCES_MID, updateResult.getmId());
                }
                if (updateResult.getEtsMid() != null && updateResult.getEtsMid().length() > 0) {
                    application.getSharedPreferencesManager().putString(Constants.PREFERENCES_ETS_MID, updateResult.getEtsMid());
                }
                if (updateResult.getServerTime() > 0) {
                    long timeOffset = updateResult.getServerTime() - System.currentTimeMillis();
                    application.getSharedPreferencesManager().putLong(Constants.PREFERENCES_TIME_OFFSET, timeOffset);

                    // Sets the server time during this update.
                    application.getSharedPreferencesManager().putLong(Constants.PREFERENCES_TIME_ON_UPDATE, updateResult.getServerTime());
                    application.postEvent(new TimeOffsetUpdateEvent(timeOffset));
                }

                boolean isUpdate = updateResult.isNeedAPKUpdate();
                if (isUpdate) {
                    //Open the Update Screenable :
                    String updateDownloadLink = (updateResult.getDownloadLink() != null) ? updateResult.getDownloadLink() : "market://details?id=" + LaunchActivity.this.getPackageName();
                    boolean isUpdateMandatory = updateResult.isForceAPKUpdate();
                    showUpdateScreen(updateDownloadLink, isUpdateMandatory);
                } else {
                    isCheckForUpdateLoaded = InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_OK_RESPONSE;
                    getCountriesForSkin();
                    getSkins();
                    checkInitialValuesAndContinue();
                }

            } else {
                isCheckForUpdateLoaded = InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_ERROR_RESPONSE;
                showNotLoadedInitialValuesError(updateResult);
            }
        }
    }

    private void getMarketsGroups() {
        if (isMarketsLoaded == InitialValuesLoadStatus.SERVICE_NOT_CALLED || isMarketsLoaded == InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_ERROR_RESPONSE) {
            Log.d(TAG, "Getting Markets from server.");
            if (application.getCommunicationManager().requestService(this, "getMarketGroupsCallBack", Constants.SERVICE_GET_MARKET_GROUPS, new MethodRequest(), MarketsGroupsMethodResult.class, false, false)) {
                isMarketsLoaded = InitialValuesLoadStatus.SERVICE_CALLED;
                initialValuesAttempts++;
            }
        }
    }

    public void getMarketGroupsCallBack(Object result) {
        if (result == null && initialValuesAttempts < MAX_ATTEMPTS_BEFORE_DIALOG_FOR_CONTINUE) {
            isMarketsLoaded = InitialValuesLoadStatus.SERVICE_NOT_CALLED;
            //There is a problem with checkForUpdate request : sometimes the response is null and "Try again" fixes the issue.
            getInitialValues();
            return;
        } else if(result == null && initialValuesAttempts >= MAX_ATTEMPTS_BEFORE_DIALOG_FOR_CONTINUE) {
            isMarketsLoaded = InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_ERROR_RESPONSE;
            showNotLoadedInitialValuesError(null);
        }

        if (result != null) {
            MarketsGroupsMethodResult marketsGroupsResult = (MarketsGroupsMethodResult) result;
            if (marketsGroupsResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
                Log.d(TAG, "Got Market Groups from the server.");

                for (int i = 0; i < marketsGroupsResult.getGroups().length; i++) {
                    MarketGroup marketGroup = marketsGroupsResult.getGroups()[i];
                    for (Market market : marketGroup.getMarkets()) {
                        if (market.getGroupId() == null) {
                            market.setGroupId(marketGroup.getId());//If not added on the server side
                        }
                        //Check if the Decimal point is not set:
                        if (market.getDecimalPoint() == null) {
                            market.setDecimalPoint(5L);
                        }

                        market.setGroupName(marketGroup.getName());
                    }
                }

                application.setMarketGroups(marketsGroupsResult.getGroups());

                isMarketsLoaded = InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_OK_RESPONSE;
                checkInitialValuesAndContinue();
            } else {
                isMarketsLoaded = InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_ERROR_RESPONSE;
                showNotLoadedInitialValuesError((MethodResult) result);
            }
        }
    }

    private void getCountriesForSkin() {
        if (isCheckForUpdateLoaded == InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_OK_RESPONSE && (isCountriesLoaded == InitialValuesLoadStatus.SERVICE_NOT_CALLED || isCountriesLoaded == InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_ERROR_RESPONSE)) {
            if (application.getCommunicationManager().requestService(this, "getCountriesForSkinCallBack", Constants.SERVICE_GET_SORTED_COUNTRIES, new MethodRequest(), CountriesListMethodResult.class, false, false)){
                isCountriesLoaded = InitialValuesLoadStatus.SERVICE_CALLED;
                initialValuesAttempts++;
            }
        }
    }

    public void getCountriesForSkinCallBack(Object resObj) {
        Log.d(TAG, "getCountriesCallBack");

        if (resObj == null && initialValuesAttempts < MAX_ATTEMPTS_BEFORE_DIALOG_FOR_CONTINUE) {
            isCountriesLoaded = InitialValuesLoadStatus.SERVICE_NOT_CALLED;
            //There is a problem with getCountriesForSkin request : sometimes the response is null and "Try again" fixes the issue.
            getInitialValues();
            return;
        } else if(resObj == null && initialValuesAttempts >= MAX_ATTEMPTS_BEFORE_DIALOG_FOR_CONTINUE) {
            isCountriesLoaded = InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_ERROR_RESPONSE;
            showNotLoadedInitialValuesError(null);
        }

        if (resObj != null) {
            CountriesListMethodResult result = (CountriesListMethodResult) resObj;
            if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
                isCountriesLoaded = InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_OK_RESPONSE;
                if(result.getIpDetectedCountryId() != null && result.getIpDetectedCountryId() != 0L){
                    application.setCountryId(result.getIpDetectedCountryId());
                }else{
                    application.setCountryId(result.getSkinDefautlCointryId());
                }
                application.setCountryList(result.getCountriesMap());
                checkInitialValuesAndContinue();
            } else {
                isCountriesLoaded = InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_ERROR_RESPONSE;
                showNotLoadedInitialValuesError(result);
            }
        }
    }

    private void getSkins() {
        if (isCheckForUpdateLoaded == InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_OK_RESPONSE && (isSkinsLoaded == InitialValuesLoadStatus.SERVICE_NOT_CALLED || isSkinsLoaded == InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_ERROR_RESPONSE)) {
            if (application.getCommunicationManager().requestService(this, "getSkinsCallBack", Constants.SERVICE_GET_SKINS, new MethodRequest(), VisibleSkinListMethodResult.class, false, false)){
                isSkinsLoaded = InitialValuesLoadStatus.SERVICE_CALLED;
                initialValuesAttempts++;
            }
        }
    }

    public void getSkinsCallBack(Object resObj) {
        Log.d(TAG, "getSkinsCallBack");

        if (resObj == null && initialValuesAttempts < MAX_ATTEMPTS_BEFORE_DIALOG_FOR_CONTINUE) {
            isSkinsLoaded = InitialValuesLoadStatus.SERVICE_NOT_CALLED;
            getInitialValues();
            return;
        } else if(resObj == null && initialValuesAttempts >= MAX_ATTEMPTS_BEFORE_DIALOG_FOR_CONTINUE) {
            isSkinsLoaded = InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_ERROR_RESPONSE;
            showNotLoadedInitialValuesError(null);
        }

        if (resObj != null) {
            VisibleSkinListMethodResult result = (VisibleSkinListMethodResult) resObj;
            if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
                isSkinsLoaded = InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_OK_RESPONSE;
                List<Long> visibleSkinsList = new ArrayList<Long>();
                for(VisibleSkinListMethodResult.VisibleSkinPair visibleSkinPair : result.getVisibleSkins()){
                    visibleSkinsList.add(visibleSkinPair.getSkinId());
                }
                application.setVisibleSkins(visibleSkinsList);

                checkInitialValuesAndContinue();
            } else {
                isSkinsLoaded = InitialValuesLoadStatus.SERVICE_CALLED_RETURNED_ERROR_RESPONSE;
                showNotLoadedInitialValuesError(result);
            }
        }
    }

    public void showNotLoadedInitialValuesError(MethodResult result) {
        if(!isTryAgainPopupShown) {
            String errorType;
            String errorMessage;

            if (result != null && result.getUserMessages() != null
                    && result.getUserMessages()[0].getField().length() != 0) {
                errorType = result.getUserMessages()[0].getField();
                errorMessage = result.getUserMessages()[0].getMessage();
            } else {
                errorType = Application.get().getString(R.string.error);
                errorMessage = getString(R.string.errorTryAgain);
            }

            if(application.getCurrentActivity() == this) {
                application.getPopUpManager().showConfirmPopUp(errorType, errorMessage, new OnConfirmPopUpButtonPressedListener() {
                    @Override
                    public void onPositive() {
                        isTryAgainPopupShown = false;
                        isAskAgainPopupShown = false;
                        initialValuesAttempts = 0;
                        getInitialValues();
                    }

                    @Override
                    public void onNegative() {
                        finish();
                    }
                });
            }
            isTryAgainPopupShown = true;
        }
    }

    /**
     * Checks whether a welcome screen should be shown.
     */
    protected void checkForWelcome() {
        checkForPromotion();
    }

	protected void handleWelcome(){
		removeSplash();
		if(!application.getSharedPreferencesManager().getBoolean(Constants.PREFERENCES_IS_LOGGED_ATLEAST_ONCE, false) && !application.getDeepLinksManager().isDeepLink()){
			showWelcomeScreen();
		} else {
			openMainActivity(null);
		}
	}

	/**
	 * Remove the Splash screen clearing all resources.
	 */
	protected void removeSplash() {
		splashLayout.setVisibility(View.GONE);
		splashLayout.removeAllViews();
		DrawableUtils.setBackground(splashLayout, null);
	}

	/**
	 * Opens the Update Screen.
	 */
	private void showUpdateScreen(String updateDownloadLink, boolean isUpdateMandatory){
		Bundle bundle = new Bundle();
		bundle.putString(Constants.EXTRA_UPDATE_DOWNLOAD_LINK, updateDownloadLink);
		bundle.putBoolean(Constants.EXTRA_UPDATE_MANDATORY, isUpdateMandatory);
		application.postEvent(new NavigationEvent(Screen.UPDATE_APPLICATION_VERSION, NavigationType.HORIZONTAL, bundle));
	}

	/**
	 * Opens the Welcome Screen.
	 */
	public void showWelcomeScreen(){
		if (!isFinishing() && this == application.getCurrentActivity()) {
			Log.d(TAG, "OPEN WELCOME SCREENS");
			Bundle bundle = new Bundle();
			if(skinID != null){			
				bundle.putLong(Constants.EXTRA_SKIN_ID, skinID);
			}
			application.postEvent(new NavigationEvent(Screen.WELCOME, NavigationType.HORIZONTAL, bundle));			
		}
	}


    protected void handleStatusAndActionBarOnWelcomeScreens() {

    }

	/**
	 * Called when a {@link NavigationEvent} occurs. 
	 * This method handles the navigation flow - the fragment transactions for the Main View.
	 * @param event
	 */
	public void onEventMainThread(NavigationEvent event){

		// Check if the activity is NOT being destroyed and if it is the current activity:
		if (!isFinishing() && this == application.getCurrentActivity()) {
			if (splashLayout.getVisibility() != View.GONE) {
				removeSplash();
			}

			Bundle bundle = new Bundle();
			Screenable screen = event.getToScreen();
			Class<? extends BaseFragment> fragmentClass = getFragmentClass(bundle, screen, event);
			if (bundle.containsKey(Constants.EXTRA_SCREEN)) {
				openMainActivity(bundle);
			}
	
			if (fragmentClass != null) {
                if(screen == Screen.WELCOME) {
                    handleStatusAndActionBarOnWelcomeScreens();
                }
				application.getFragmentManager().openScreen(fragmentClass, mainView.getId(), event);
			}
		}

	}

	/**
	 * Finds the screen fragment which has to be displayed.
	 * 
	 * @param screen
	 * @return
	 */
	@SuppressWarnings("static-method")
	protected Class<? extends BaseFragment> getFragmentClass(Bundle bundle, Screenable screen, NavigationEvent event) {
		Class<? extends BaseFragment> fragmentClass = null;

		if(screen == Screen.UPDATE_APPLICATION_VERSION){
			fragmentClass = UpdateFragment.class;
		}else if(screen == Screen.WELCOME){
			fragmentClass = WelcomeFragment.class;
		}else if(screen == Screen.PUSH_NOTIFICATION_PERMISSION){
			fragmentClass = PushNotificationsAlertFragment.class;
		}else if(screen == Screen.LOGIN){
			bundle.putSerializable(Constants.EXTRA_SCREEN, Screen.LOGIN);
		}else if(screen == Screen.REGISTER){
			bundle.putSerializable(Constants.EXTRA_SCREEN, Screen.REGISTER);
		}else if(screen == Screen.TRADE){
			bundle.putSerializable(Constants.EXTRA_SCREEN, Screen.TRADE);
		}else if(screen == Screen.TABLET_TRADE){
			bundle.putSerializable(Constants.EXTRA_SCREEN, Screen.TABLET_TRADE);
		} else if (screen == Screen.ASSET_VIEW_PAGER) {
			bundle.putSerializable(Constants.EXTRA_SCREEN, Application.Screen.ASSET_VIEW_PAGER);
			bundle.putSerializable(Constants.EXTRA_MARKET, event.getBundle().getSerializable(Constants.EXTRA_MARKET));
		}

		return fragmentClass;
	}
	
	/**
	 * Called when a {@link UpdateSkipEvent} occurs.
	 * @param event
	 */
	public void onEventMainThread(UpdateSkipEvent event){
		checkForWelcome();
	}
	
	/**
	 * Opens the MainAcivity.
	 */
	protected void openMainActivity(Bundle bundle){
		Intent intent = new Intent(getApplicationContext(), Application.get().getSubClassForClass(
				MainActivity.class));
		if(application.getUser() == null){
			ActionFragment.isInitialLoginRegister = true;			
		}else{
			ActionFragment.isInitialLoginRegister = false;		
		}

		Bundle params = null;
		
		if (bundle == null) {
			bundle = new Bundle();
			if (application.isLoggedIn()) {
				if (pushNotificationsManager.isAppOpenedWithPush()) {
					bundle.putSerializable(Constants.EXTRA_SCREEN, pushNotificationsManager.getToScreen());
					params = pushNotificationsManager.getScreenArguments();
					pushNotificationsManager.setPushHandled();
				}else if(application.getDeepLinksManager().isDeepLink()){
					Bundle deepLinkBundle = application.getDeepLinksManager().getDeepLinkBundle();
					Screenable fbScreen = (Screenable) deepLinkBundle.getSerializable(Constants.EXTRA_SCREEN);
					if(fbScreen == Screen.REGISTER){
						fbScreen = application.getCurrentScreen();
					}
					bundle.putSerializable(Constants.EXTRA_SCREEN, fbScreen);
					params = deepLinkBundle;
//					application.getDeepLinksManager().handledDeepLink();
				} else {
					bundle.putSerializable(Constants.EXTRA_SCREEN, application.getHomeScreen());
				}
			} else {
				if (pushNotificationsManager.isAppOpenedWithPush() && !pushNotificationsManager.isLoginNeeded()) {
					bundle.putSerializable(Constants.EXTRA_SCREEN, pushNotificationsManager.getToScreen());
					params = pushNotificationsManager.getScreenArguments();
					pushNotificationsManager.setPushHandled();
				} else if (application.getDeepLinksManager().isDeepLink() && !application.getDeepLinksManager().isLoginNeeded()) {
					Bundle deepLinkBundle = application.getDeepLinksManager().getDeepLinkBundle();
					bundle.putSerializable(Constants.EXTRA_SCREEN, deepLinkBundle.getSerializable(Constants.EXTRA_SCREEN));
					params = deepLinkBundle;
//					application.getDeepLinksManager().handledDeepLink();
				}else {
					bundle.putSerializable(Constants.EXTRA_SCREEN, Screen.LOGIN);
				}
			}
		}
		
		if(params == null) {
			params = new Bundle();	
		}
		
		bundle.putBundle(Constants.EXTRA_BUNDLE, params);
		intent.putExtras(bundle);
		startActivity(intent);
		finish();
		Log.d(TAG, "OPEN MAIN ACTIVITY with INIIAL SCREEN = " + bundle.getSerializable(Constants.EXTRA_SCREEN));
	}

	@Override
	public void stopRequests() {
		//Do nothing!
	}
    @SuppressWarnings("AccessStaticViaInstance")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult");
        if (requestCode == Constants.REQUEST_CODE_GOOGLE_PLAY_SERVICES) {
            if (resultCode == Activity.RESULT_OK) {
                application.postEvent(new PushNotificationsPermissionEvent(true));
            } else {
                application.postEvent(new PushNotificationsPermissionEvent(false));
                application.getNotificationManager().showNotificationError("The application needs Google Plays Services in order to work properly!");
            }
            //Continue with the launch flow:
            getInitialValues();
        } else if (requestCode == application.getPermissionsManager().REQUEST_DRAW_OVERLAYS_PERMISSION_REQUEST_CODE) {
            if (application.getPermissionsManager().canDrawOverlays()) {
                checkForPermissions();
            } else {
                if(!isAskAgainPopupShown) {
                    application.getPopUpManager().showConfirmPopUp("Permissions required for app to work", "This app needs all permissions to be enabled to work. Will you enable the disabled permissions or close the app?", "I will enable them", "Close the app", new OnConfirmPopUpButtonPressedListener() {
                        @Override
                        public void onPositive() {
                            isAskAgainPopupShown = false;
                            application.getPermissionsManager().askForDrawOverlaysPermission();
                        }

                        @Override
                        public void onNegative() {
                            finish();
                        }
                    });
                    isAskAgainPopupShown = true;
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPermissionsGrantResult(String[] permissions, int[] grantResults) {
        Log.d(TAG, "Getting results from permissions request");
        for (int grantResult : grantResults) {
            if (grantResult == PackageManager.PERMISSION_DENIED) {
                application.getPopUpManager().showConfirmPopUp("Permissions required for app to work", "This app needs all permissions to be enabled to work. Will you enable the disabled permissions or close the app?", "I will enable them", "Close the app", new OnConfirmPopUpButtonPressedListener() {
                    @Override
                    public void onPositive() {
                        checkForPermissions();
                    }

                    @Override
                    public void onNegative() {
                        finish();
                    }
                });
                break;
            }
        }


        if (application.getPermissionsManager().hasPermissions(permissions)) {
            if (application.getPermissionsManager().canDrawOverlays()) {
                getInitialValues();
            } else {
                application.getPermissionsManager().askForDrawOverlaysPermission();
            }
        }
    }
}
