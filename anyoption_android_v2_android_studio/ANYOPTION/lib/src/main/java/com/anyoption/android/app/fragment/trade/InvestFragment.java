package com.anyoption.android.app.fragment.trade;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.ChartEvent;
import com.anyoption.android.app.event.InvestmentEvent;
import com.anyoption.android.app.event.InvestmentEvent.Type;
import com.anyoption.android.app.event.LightstreamerRegisterEvent;
import com.anyoption.android.app.event.LightstreamerUpdateEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.LongTermTradingTimeChosenEvent;
import com.anyoption.android.app.manager.CommunicationManager;
import com.anyoption.android.app.manager.DataBaseManager;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.anyoption.android.app.model.LoseWinItem;
import com.anyoption.android.app.popup.BaseDialogFragment.OnDialogDismissListener;
import com.anyoption.android.app.popup.CancelInvestmentDialogFragment;
import com.anyoption.android.app.popup.ChooseReturnDialogFragment;
import com.anyoption.android.app.popup.ChooseReturnDialogFragment.OnReturnChangedListener;
import com.anyoption.android.app.popup.InvestmentDev2FailedDialogFragment;
import com.anyoption.android.app.popup.LoginRegisterDialogfragment;
import com.anyoption.android.app.popup.LowCashBalanceDialogFragment;
import com.anyoption.android.app.popup.PredefinedAmountDialogFragment;
import com.anyoption.android.app.popup.PredefinedAmountDialogFragment.OnAmountChangeListener;
import com.anyoption.android.app.popup.QuestionnairePEPBlockedOkDialogFragment;
import com.anyoption.android.app.popup.QuestionnaireThresholdRestrictedOkDialogFragment;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.InvestmentUtil;
import com.anyoption.android.app.util.MarketsUtils;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.InvestButton;
import com.anyoption.android.app.widget.InvestButton.State;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.beans.base.OpportunityOddsPair;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.service.requests.InsertInvestmentMethodRequest;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.results.InvestmentMethodResult;
import com.anyoption.json.results.ChartDataMethodResult;
import com.anyoption.json.results.OpportunityOddsPairResult;
import com.lightstreamer.ls_client.UpdateInfo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

/**
 * Fragment with put and call buttons which handles invest events.
 *
 * @author Mario Kutlev
 */
public class InvestFragment extends BaseAssetNestedFragment implements Callback {

    @SuppressWarnings("hiding")
    private static final String TAG = InvestFragment.class.getSimpleName();

    private static final int MESSAGE_STATE_CHANGED = 1;
    private static final int MESSAGE_UPDATE_WIN_LOSE = 2;

    private View root;
    private InvestButton putButton;
    private InvestButton callButton;
    private TextView winTextView;
    private TextView loseTextView;
    private TextView investAmountTextView;
    private TextView investProfitTextView;
    private View expiredView;
    private View winLoseAmountsLayout;
    private View waitingForExpiryView;

    private View commissionLayout;
    private TextView commissionView;

    private SkinGroup skinGroup;
    private CommunicationManager communicationManager;
    private Map<Long, LoseWinItem> chooseReturnMap;
    private Bundle chooseReturnBundle;
    private Currency currency;

    // Investment information fields
    private BigDecimal investAmount;
    private double level;

    private long opportunityId;
    private int opportunityState;
    private float loseOdds;
    private float winOdds;
    private int winPercent;
    private long commissionAmount;

    private String loseTextLabel;

    private Handler handler;

    private LongTermTradingTimeChosenEvent.ChosenTime longTermTradingChosenTime = LongTermTradingTimeChosenEvent.ChosenTime.NEAREST;
    private Investment investment;
    private boolean isTablet;

    /**
     * Use this method to create new instance of the fragment.
     *
     * @return
     */
    public static InvestFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new InvestFragment.");
        InvestFragment fragment = new InvestFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public static InvestFragment newInstance(Market market) {
        Bundle args = new Bundle();
        args.putSerializable(Constants.EXTRA_MARKET, market);
        return newInstance(args);
    }

    /**
     * Use {@link #newInstance()} method instead this constructor.
     */
    public InvestFragment() {
        // Empty constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isTablet = getResources().getBoolean(R.bool.isTablet);

        investAmount = new BigDecimal(application.getDefaultInvestAmount());

        communicationManager = application.getCommunicationManager();
        Skin skin = application.getSkins().get(application.getSkinId());
        skinGroup = skin.getSkinGroup();
        handler = new Handler(Looper.getMainLooper(), this);

        setUpUserDependentFields();

        chooseReturnMap = new HashMap<Long, LoseWinItem>();

        resetFields();
    }

    private void resetFields() {
        // We'll receive this values from the lightstreamer.
        winPercent = 0;
        winOdds = 0;
        loseOdds = 0;

        chooseReturnMap.clear();
    }

    private void setUpUserDependentFields() {
        // Update the currency
        currency = application.getCurrency();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.invest_fragment, container, false);

        putButton = (InvestButton) root.findViewById(R.id.invest_put_button);
        putButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onPut();
            }
        });
        putButton.disable();

        callButton = (InvestButton) root.findViewById(R.id.invest_call_button);
        callButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onCall();
            }
        });
        callButton.disable();

        winTextView = (TextView) root.findViewById(R.id.invest_win_sum_text);
        loseTextView = (TextView) root.findViewById(R.id.invest_if_incorrect_text);
        investAmountTextView = (TextView) root.findViewById(R.id.invest_amount);
        investProfitTextView = (TextView) root.findViewById(R.id.invest_profit);
        winLoseAmountsLayout = root.findViewById(R.id.invest_win_lose_amounts_layout);
        waitingForExpiryView = root.findViewById(R.id.invest_waiting_for_expiry_layout);
        expiredView = root.findViewById(R.id.invest_expired_text);

        commissionLayout = root.findViewById(R.id.invest_commission_layout);
        commissionView = (TextView) root.findViewById(R.id.invest_commission);

        opportunityState = 0;
        loseTextLabel = getResources().getString(R.string.ifIncorrect) + " ";

        if (market != null) {
            setUpDialogOpenButtons();
            updateWinLose();
            if (market.isOptionPlusMarket()) {
                showOptionPlusInfo();
            } else {
                showRegularOptionInfo();
            }
        }

        //Set up the BID, ASK message:
        if(!isTablet){
            View bidAskView = root.findViewById(R.id.investBidAskTextView);
            if(market != null && MarketsUtils.isBinary(market)){
                bidAskView.setVisibility(View.VISIBLE);
            }else{
                bidAskView.setVisibility(View.GONE);
            }
        }

        return root;
    }

    protected void onPut() {
        if (RegulationUtils.checkRegulationAndPepAndThresholdStatusAndDoNecessary(false)) {
            onPutAction();
        }
    }

    protected void onPutAction() {
        if (putButton.getState() == State.NORMAL) {
            invest(Investment.INVESTMENT_TYPE_PUT);
        }
    }

    protected void onCall() {
        if (RegulationUtils.checkRegulationAndPepAndThresholdStatusAndDoNecessary(false)) {
            onCallAction();
        }
    }

    protected void onCallAction() {
        if (callButton.getState() == State.NORMAL) {
            invest(Investment.INVESTMENT_TYPE_CALL);
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        registerForEvents();
    }

    @Override
    public void onPause() {
        application.unregisterForAllEvents(this);
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        application.unregisterForAllEvents(this);
        super.onDestroyView();
    }

    private void registerForEvents() {
        application.registerForEvents(this, LoginLogoutEvent.class);

        if (market != null) {
            application.registerForEvents(this, LightstreamerRegisterEvent.class, LightstreamerUpdateEvent.class, ChartEvent.class, InvestmentEvent.class, LongTermTradingTimeChosenEvent.class);
        }
    }

    protected void investLogout(){
        Log.d(TAG, "Trying to invest in logout state!");
        application.getFragmentManager().showDialogFragment(LoginRegisterDialogfragment.class, null);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    /////                                   EVENTs                                          /////
    /////////////////////////////////////////////////////////////////////////////////////////////

    public void onEvent(LightstreamerRegisterEvent event) {
        opportunityId = 0L;
        opportunityState = 0;
        if (callButton != null && putButton != null) {
            callButton.disable();
            putButton.disable();
        }
    }

    public void onEvent(LightstreamerUpdateEvent event) {
        if (market != null && event.getMarketId() == market.getId()) {
            updateItem(event.getUpdateInfo());
        }
    }

    public void onEventMainThread(ChartEvent event) {
        if (event.getMarketId() != market.getId()) {
            return;
        }

        if (event.getType() == ChartEvent.Type.DATA_REQUEST_SUCCESS) {
            ChartDataMethodResult data = event.getChartData();
            opportunityId = data.getOpportunityId();
            opportunityState = 0;
            if (data.getOpportunityTypeId() == Opportunity.TYPE_OPTION_PLUS) {
                commissionAmount = data.getCommission();
                commissionView.setText(AmountUtil.getFormattedAmount(commissionAmount, currency));
            }
        }
    }

    public void onEventMainThread(LongTermTradingTimeChosenEvent event) {
        if(market.getId() != event.getMarketId()){
            return;
        }
        if (longTermTradingChosenTime != event.getTime()) {
            longTermTradingChosenTime = event.getTime();
        }

        if(longTermTradingChosenTime != LongTermTradingTimeChosenEvent.ChosenTime.NEAREST && longTermTradingChosenTime != LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_1 && longTermTradingChosenTime != LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_2 && longTermTradingChosenTime != LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_3) {
            hideWaitingForExpiryAndClosedState();
        }
    }

    public void onEventMainThread(LoginLogoutEvent event) {
        setUpUserDependentFields();
        updateWinLose();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////                         REQUESTs & RESPONSEs                                     /////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void invest(int investmentType) {
        Log.d(TAG, "Trying to invest...");

        if (!application.isLoggedIn()) {
            investLogout();
            return;
        }

        Display display = ((WindowManager) application.getCurrentActivity().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int orientation = display.getRotation();

        boolean isFromGraph = orientation == Surface.ROTATION_90 || orientation == Surface.ROTATION_270;

        InsertInvestmentMethodRequest request = new InsertInvestmentMethodRequest();
        request.setPageLevel(level);
        request.setPageOddsLose(loseOdds);
        request.setPageOddsWin(winOdds);
        request.setChoice(investmentType);
        request.setOpportunityId(opportunityId);
        request.setFromGraph(isFromGraph);
        request.setRequestAmount(investAmount.doubleValue());
        request.setDev2Second(false);

        boolean isRequestOK = communicationManager.requestService(this, "insertInvestmentCallBack",Constants.SERVICE_INSERT_INVESTMENT, request, InvestmentMethodResult.class, false, false);
        application.postEvent(new InvestmentEvent(market.getId(), Type.INVEST_REQUESTED));

        // Setup Invest Buttons:
        if (isRequestOK) {
            //Save the Investment that is currently being inserted:
            investment = new Investment();
            investment.setTypeId(investmentType);
            investment.setCurrentLevel(level);
            investment.setLevel(Utils.getFormattedLevel(level, market.getDecimalPoint()));
            investment.setOddsLose(loseOdds);
            investment.setOddsWin(winOdds);
            investment.setOpportunityId(opportunityId);
            investment.setAmount((long) (investAmount.doubleValue()*100));
            //////////////////////////////////////////////////////

            if (investmentType == Investment.INVESTMENT_TYPE_PUT) {
                putButton.startLoading();
                callButton.disable();
            } else if (investmentType == Investment.INVESTMENT_TYPE_CALL) {
                callButton.startLoading();
                putButton.disable();
            }
        }
        ///////////////
    }

    public void insertInvestmentCallBack(Object resultObj) {
        Log.d(TAG, "insertInvestmentCallBack");

        InvestmentMethodResult result = (InvestmentMethodResult) resultObj;

        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {

            final Investment investment;
            //Check the Deviation2 Check:
            final int dev2Seconds = result.getDev2Seconds();
            if(dev2Seconds > 0){
                investment = this.investment;
            }else{
                investment = result.getInvestment();
                InvestmentUtil.fixDate(investment);
            }

            //Save the Market as one of the user favorites:
            application.getDataBaseManager().saveOrUpdateMarket(market, DataBaseManager.MARKETS_STATE_FAVORITE);

            if (result.getUser() != null) {
                application.setUser(result.getUser());
            }

            if (result.getUserRegulation() != null) {
                application.setUserRegulation(result.getUserRegulation());
            }

            boolean isShowCancelInvestmentPopup = application.isShowCancelInvestmentPopup();
            if(isShowCancelInvestmentPopup){
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.EXTRA_INVESTMENT, investment);
                bundle.putLong(Constants.EXTRA_DEV2_SECONDS, dev2Seconds*1000);
                bundle.putLong(Constants.EXTRA_CANCEL_SECONDS, result.getCancelSeconds()*1000);
                CancelInvestmentDialogFragment cancelInvestmentDialogFragment = (CancelInvestmentDialogFragment) application.getFragmentManager().showDialogFragment(CancelInvestmentDialogFragment.class, bundle);
                cancelInvestmentDialogFragment.setCancelInvestmentListener(new CancelInvestmentDialogFragment.CancelInvestmentListener() {
                    @Override
                    public void onFinished(boolean isCanceled) {
                        if(isCanceled){
                            // Setup Buttons:
                            callButton.stopLoading(false);
                            putButton.stopLoading(false);
                            ///////////////
                        }else{
                            if(dev2Seconds > 0){
                                //We have Deviation2 check and we have to finish the investment:
                                finishInvestD2(investment);
                            }else{
                                //No Deviation2 Check:
                                application.postEvent(new InvestmentEvent(market.getId(), Type.INVEST_SUCCESS, investment));
                                application.getNotificationManager().showNotificationInvestOK(investment);

                                // Setup Buttons:
                                callButton.stopLoading(true);
                                putButton.stopLoading(true);
                                ///////////////
                            }
                        }
                    }
                });
            }else{
                if(dev2Seconds > 0){
                    Log.d(TAG, "We have Deviation2 check. Waiting for " + dev2Seconds + " seconds to finish the investment!");
                    //We have Deviation2 check with the given number of seconds to wait until we can finish the investment:
                    new CountDownTimer(dev2Seconds * 1000, dev2Seconds * 1000){
                        @Override
                        public void onTick(long millisUntilFinished) {}
                        @Override
                        public void onFinish() {
                            if(isAdded()){
                                Log.d(TAG, "Deviation2 check time finished!");
                                finishInvestD2(investment);
                            }
                        }
                    }.start();
                }else{
                    //No Deviation2 Check:
                    application.postEvent(new InvestmentEvent(market.getId(), Type.INVEST_SUCCESS, result.getInvestment()));
                    application.getNotificationManager().showNotificationInvestOK(result.getInvestment());

                    // Setup Buttons:
                    callButton.stopLoading(true);
                    putButton.stopLoading(true);
                    ///////////////
                }
            }

        } else {
            handleInvestFail(result);
        }

    }

    private void finishInvestD2(Investment investment){
        Log.d(TAG, "Finishing the Deviation2 check investment...");
        Display display = ((WindowManager) application.getCurrentActivity().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int orientation = display.getRotation();

        boolean isFromGraph = orientation == Surface.ROTATION_90 || orientation == Surface.ROTATION_270;

        InsertInvestmentMethodRequest request = new InsertInvestmentMethodRequest();
        request.setPageLevel(investment.getCurrentLevel());
        request.setPageOddsLose((float) investment.getOddsLose());
        request.setPageOddsWin((float) investment.getOddsWin());
        request.setChoice((int) investment.getTypeId());
        request.setOpportunityId(investment.getOpportunityId());
        request.setFromGraph(isFromGraph);
        request.setRequestAmount(AmountUtil.getDoubleAmount(investment.getAmount()));
        request.setDev2Second(true);

        if(!communicationManager.requestService(this, "finishInvestD2CallBack",Constants.SERVICE_INSERT_INVESTMENT, request, InvestmentMethodResult.class, false, false)){
            // Setup Buttons:
            callButton.stopLoading(false);
            putButton.stopLoading(false);
            ///////////////
        }

    }

    public void finishInvestD2CallBack(Object resultObj){
        Log.d(TAG, "finishInvestD2CallBack");

        InvestmentMethodResult result = (InvestmentMethodResult) resultObj;

        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {

            final Investment investment = result.getInvestment();
            InvestmentUtil.fixDate(investment);

            if (result.getUser() != null) {
                application.setUser(result.getUser());
            }

            if (result.getUserRegulation() != null) {
                application.setUserRegulation(result.getUserRegulation());
            }

            application.postEvent(new InvestmentEvent(market.getId(), Type.INVEST_SUCCESS, result.getInvestment()));
            application.getNotificationManager().showNotificationInvestOK(result.getInvestment());

            // Setup Buttons:
            callButton.stopLoading(true);
            putButton.stopLoading(true);
            ///////////////
        } else {
            if(result != null && result.getErrorCode() == Constants.ERROR_CODE_INV_VALIDATION && application.isShowCancelInvestmentPopup()){
                Bundle bundle = new Bundle();
                String errorMessage;
                if(result.getUserMessages() != null && result.getUserMessages()[0].getMessage() != null){
                    errorMessage = result.getUserMessages()[0].getMessage();
                }else{
                    errorMessage = getString(R.string.error);
                }
                bundle.putString(Constants.EXTRA_MESSAGE, errorMessage);
                application.getFragmentManager().showDialogFragment(InvestmentDev2FailedDialogFragment.class, bundle);
            } else if(result != null && result.getErrorCode() == Constants.ERROR_CODE_REGULATION_USER_IS_TRESHOLD_BLOCK) {
                Application.get().getFragmentManager().showDialogFragment(QuestionnaireThresholdRestrictedOkDialogFragment.class, null);
            } else {
                ErrorUtils.handleResultError(result);
            }
            // Setup Buttons:
            callButton.stopLoading(false);
            putButton.stopLoading(false);
            ///////////////
        }

    }

    private void handleInvestFail(InvestmentMethodResult result){
        Log.d(TAG, "handleInvestFail");
        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_NO_CASH_FOR_NIOU) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.EXTRA_CASH_BALANCE, AmountUtil.getUserFormattedAmount(Long.valueOf(result.getUserCashBalance())));
            Application.get().getFragmentManager().showDialogFragment(LowCashBalanceDialogFragment.class, bundle);
        } else if(result != null && result.getErrorCode() == Constants.ERROR_CODE_REGULATION_USER_IS_TRESHOLD_BLOCK) {
            Application.get().getFragmentManager().showDialogFragment(QuestionnaireThresholdRestrictedOkDialogFragment.class, null);
        } else {
            application.getEventBus().post(new InvestmentEvent(market.getId(), Type.INVEST_FAIL));

            ErrorUtils.handleResultError(result);

            if (result != null) {
                Log.e(TAG, "Could not invest. ErrorCode: " + result.getErrorCode());
            } else {
                Log.e(TAG, "Could not invest! result == null");
            }
        }

        // Setup Buttons:
        callButton.stopLoading(false);
        putButton.stopLoading(false);
        ///////////////
    }

    private void requestOddsPairs() {
        communicationManager.requestService(this, "getReturnOddsCallBack",Constants.SERVICE_GET_ALL_RETURN_ODDS, new MethodRequest(),OpportunityOddsPairResult.class, false, false);
    }

    public void getReturnOddsCallBack(Object resultObj) {
        if (!isAdded()) {
            return;
        }

        OpportunityOddsPairResult result = (OpportunityOddsPairResult) resultObj;

        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            Log.d(TAG, "getReturnOddsCallBack received pairs");

            chooseReturnMap.clear();
            OpportunityOddsPair[] oddsPairs = result.getOpportunityOddsPair();
            for (int i = 0; i < oddsPairs.length; i++) {
                chooseReturnMap.put(
                        oddsPairs[i].getSelectorID(),
                        new LoseWinItem(oddsPairs[i].getRefundSelector(), oddsPairs[i]
                                .getReturnSelector()));
            }

            // If the market is unavailable, we won't receive the pairs.
            if (!chooseReturnMap.isEmpty()) {
                investProfitTextView.setEnabled(true);
            }

        } else {
            if (result != null) {
                Log.e(TAG, "Could not get return odds! ErrorCode: " + result.getErrorCode());
            } else {
                Log.e(TAG, "Could not get return odds! The result is NULL!");
            }
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////


    private void updateItem(UpdateInfo update) {
        if (opportunityId == 0L || update.isValueChanged(LightstreamerManager.COLUMN_ET_OPP_ID)) {
            opportunityId = Long.parseLong(update
                    .getNewValue(LightstreamerManager.COLUMN_ET_OPP_ID));
        }

        // Update the level
        level = Double.parseDouble(update.getNewValue(skinGroup.getLevelUpdateKey()).replaceAll(
                ",", ""));

        boolean winLoseUpdated = false;

        boolean isNotOptionPlusMarketAndOddsGroupUpdated = !market.isOptionPlusMarket()
                && (update.isValueChanged(LightstreamerManager.COLUMN_ET_ODDS_GROUP) || chooseReturnBundle == null)
                && !chooseReturnMap.isEmpty();

        String newValueOddsGroupString = "";

        if (isNotOptionPlusMarketAndOddsGroupUpdated) {
            newValueOddsGroupString = update
                    .getNewValue(LightstreamerManager.COLUMN_ET_ODDS_GROUP);
            Log.d(TAG, newValueOddsGroupString);
            if (chooseReturnBundle == null) {
                chooseReturnBundle = new Bundle();
            }
            chooseReturnBundle.clear();

            String[] valueOddsGroupArray = newValueOddsGroupString.trim().split(" ");
            LoseWinItem[] items = new LoseWinItem[valueOddsGroupArray.length];
            for (int i = 0; i < valueOddsGroupArray.length; i++) {
                String key = valueOddsGroupArray[i];
                if (key.contains("d")) {
                    key = key.replace("d", "");
                    winPercent = (int) chooseReturnMap.get(Long.valueOf(key)).getWin();
                    winOdds = 1 + (chooseReturnMap.get(Long.valueOf(key)).getWin()/((float)100.0));
                    loseOdds = (chooseReturnMap.get(Long.valueOf(key)).getLose()/((float)100.0));
                    winLoseUpdated = true;
                }
                LoseWinItem item = chooseReturnMap.get(Long.valueOf(key));
                if (item != null) {
                    items[i] = item;
                }
            }
            chooseReturnBundle.putSerializable(Constants.EXTRA_CHOOSE_RETURN_PAIRS, items);
        }



        //OLD LOGIC
        if (!isNotOptionPlusMarketAndOddsGroupUpdated || (isNotOptionPlusMarketAndOddsGroupUpdated && !newValueOddsGroupString.contains("d"))) {
            if (update.isValueChanged(LightstreamerManager.COLUMN_ET_ODDS_LOSE) || loseOdds == 0) {
                loseOdds = Float.parseFloat(update
                        .getNewValue(LightstreamerManager.COLUMN_ET_ODDS_LOSE));
                winLoseUpdated = true;
            }

            if (update.isValueChanged(LightstreamerManager.COLUMN_ET_ODDS_WIN) || winOdds == 0) {
                winOdds = Float.parseFloat(update.getNewValue(LightstreamerManager.COLUMN_ET_ODDS_WIN));
                winPercent = Math.round((winOdds - 1) * 100);
                winLoseUpdated = true;
            }
        }
        //END OF THE OLD LOGIC


        if (winLoseUpdated && !handler.hasMessages(MESSAGE_UPDATE_WIN_LOSE)) {
            handler.obtainMessage(MESSAGE_UPDATE_WIN_LOSE).sendToTarget();
        }

        if (opportunityState == 0 || update.isValueChanged(LightstreamerManager.COLUMN_ET_STATE)) {
            int newOpportunityState = Integer.parseInt(update.getNewValue(LightstreamerManager.COLUMN_ET_STATE));

            // Changes the opportunity state only when its new value is higher than the old.
            // That way we fix the problems with the states after waiting for expiry. We receive
            // opened, closed, waiting for expiry states in random order.
            if (newOpportunityState > opportunityState) {
                opportunityState = newOpportunityState;
                Message msg = handler.obtainMessage(MESSAGE_STATE_CHANGED);
                msg.sendToTarget();
            }
        }
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case MESSAGE_STATE_CHANGED:
                onStateChanged();
                break;
            case MESSAGE_UPDATE_WIN_LOSE:
                updateWinLose();
                break;
        }

        return false;
    }

    /**
     * Called when the state is changed to update the UI
     */
    private void onStateChanged() {
        if (opportunityState == Opportunity.STATE_OPENED
                || opportunityState == Opportunity.STATE_LAST_10_MIN) {
            showOpenOptionInfo();
        } else if (opportunityState == Opportunity.STATE_CLOSING_1_MIN
                || opportunityState == Opportunity.STATE_CLOSING) {
            showWaitingForExpiry();
        } else if (opportunityState == Opportunity.STATE_CLOSED) {
            showClosedStateInfo();
        }
    }

    private void setUpDialogOpenButtons() {
        investAmountTextView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Create the fragment and show it as a dialog.
                PredefinedAmountDialogFragment predefinedAmountFragment = PredefinedAmountDialogFragment.newInstance(investAmount.doubleValue());
                predefinedAmountFragment.setOnAmountChangeListener(new OnAmountChangeListener() {

                    @Override
                    public void onAmountChanged(double newAmount) {
                        investAmount = new BigDecimal(newAmount);
                        updateWinLose();
                    }
                });

                predefinedAmountFragment.setOnDismissListener(new OnDialogDismissListener() {

                    @Override
                    public void onDismiss() {
                        investAmountTextView.setEnabled(true);
                    }
                });

                investAmountTextView.setEnabled(false);
                application.getFragmentManager().showDialogFragment(predefinedAmountFragment);
            }
        });

        if (!market.isOptionPlusMarket()) {
            investProfitTextView.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (!market.isOptionPlusMarket() && chooseReturnBundle != null
                            && !chooseReturnBundle.isEmpty()) {
                        chooseReturnBundle.putString(Constants.EXTRA_CHOOSE_RETURN_SELECTED_ITEM,
                                investProfitTextView.getText().toString());

                        ChooseReturnDialogFragment chooseReturnFragment = ChooseReturnDialogFragment
                                .newInstance(chooseReturnBundle);
                        chooseReturnFragment
                                .setOnReturnChangeListener(new OnReturnChangedListener() {

                                    @Override
                                    public void onReturnChanged(int newWinPercent,
                                                                int newLosePercent) {
                                        winPercent = newWinPercent;
                                        winOdds = 1 + ((float) newWinPercent) / 100;
                                        loseOdds = ((float) newLosePercent) / 100;
                                        updateWinLose();
                                    }
                                });

                        chooseReturnFragment.setOnDismissListener(new OnDialogDismissListener() {

                            @Override
                            public void onDismiss() {
                                investProfitTextView.setEnabled(true);
                            }
                        });

                        investProfitTextView.setEnabled(false);
                        application.getFragmentManager().showDialogFragment(chooseReturnFragment);
                    }
                }
            });

            // Disables the button's click until odds pairs are received.
            investProfitTextView.setEnabled(false);
        }
    }

    /**
     * Updates the views for win and lose amount.
     */
    private void updateWinLose() {
        // Set invest amount
        AmountUtil.setTextViewAmount(investAmountTextView, investAmount.doubleValue(),
                currency.getSymbol(), currency.getIsLeftSymbolBool(),
                currency.getDecimalPointDigits());

        // Set the profit percent
        investProfitTextView.setText(winPercent + "%");

        double winAmount = investAmount.multiply(new BigDecimal(winOdds))
                .setScale(currency.getDecimalPointDigits(), RoundingMode.HALF_UP)
                .doubleValue();
        AmountUtil.setTextViewAmount(winTextView, winAmount, currency.getSymbol(),
                currency.getIsLeftSymbolBool(), currency.getDecimalPointDigits());

        double loseAmount = investAmount.multiply(new BigDecimal(loseOdds))
                .setScale(currency.getDecimalPointDigits(), RoundingMode.HALF_UP)
                .doubleValue();
        String loseAmountString = AmountUtil.getFormattedAmount(loseAmount, currency);
        String loseText = loseTextLabel + loseAmountString;
        loseTextView.setText(loseText);
    }

    @Override
    public void loadData() {
        if (market != null && !market.isOptionPlusMarket()) {
            requestOddsPairs();
        }
    }

    @Override
    protected void onMarketChanged() {
        super.onMarketChanged();

        resetFields();
        setUpDialogOpenButtons();

        putButton.disable();
        callButton.disable();

        updateWinLose();
        if (market.isOptionPlusMarket()) {
            showOptionPlusInfo();
        } else {
            showRegularOptionInfo();
        }

        investAmountTextView.setEnabled(false);
        investProfitTextView.setEnabled(false);
        registerForEvents();

        if (isAutoLoad) {
            loadData();
        }
    }

    private void showOpenOptionInfo() {
        Log.d(TAG, "showOpenOptionInfo");
        waitingForExpiryView.setVisibility(View.GONE);
        expiredView.setVisibility(View.GONE);

        callButton.enable();
        putButton.enable();
        winLoseAmountsLayout.setVisibility(View.VISIBLE);
        investAmountTextView.setEnabled(true);
        investProfitTextView.setEnabled(true);
        callButton.enable();
        putButton.enable();
    }

    private void showWaitingForExpiry() {
        Log.d(TAG, "showWaitingForExpiry");
        if(longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.NEAREST || longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_1 || longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_2 || longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_3) {
            callButton.disable();
            putButton.disable();
            investAmountTextView.setEnabled(false);
            investProfitTextView.setEnabled(false);
            winLoseAmountsLayout.setVisibility(View.INVISIBLE);
            expiredView.setVisibility(View.GONE);

            waitingForExpiryView.setVisibility(View.VISIBLE);
        }
    }

    private void hideWaitingForExpiryAndClosedState() {
        Log.d(TAG, "hideWaitingForExpiryAndClosedState");
        if(longTermTradingChosenTime != LongTermTradingTimeChosenEvent.ChosenTime.NEAREST && longTermTradingChosenTime != LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_1 && longTermTradingChosenTime != LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_2 && longTermTradingChosenTime != LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_3) {
            callButton.enable();
            putButton.enable();
            investAmountTextView.setEnabled(true);
            investProfitTextView.setEnabled(true);
            winLoseAmountsLayout.setVisibility(View.VISIBLE);
            expiredView.setVisibility(View.GONE);

            waitingForExpiryView.setVisibility(View.GONE);
        }
    }

    private void showClosedStateInfo() {
        Log.d(TAG, "showClosedStateInfo");
        if(longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.NEAREST || longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_1 || longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_2 || longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_3) {
            callButton.disable();
            putButton.disable();
            investAmountTextView.setEnabled(false);
            investProfitTextView.setEnabled(false);
            winLoseAmountsLayout.setVisibility(View.INVISIBLE);
            waitingForExpiryView.setVisibility(View.GONE);
            expiredView.setVisibility(View.VISIBLE);
        }
    }

    private void showRegularOptionInfo() {
        Log.d(TAG, "showRegularOptionInfo");
        putButton.setupOptionPlus(false);
        callButton.setupOptionPlus(false);
        commissionLayout.setVisibility(View.GONE);
    }

    private void showOptionPlusInfo() {
        Log.d(TAG, "showOptionPlusInfo");
        putButton.setupOptionPlus(true);
        callButton.setupOptionPlus(true);
        commissionLayout.setVisibility(View.VISIBLE);
    }

}
