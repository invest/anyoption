package com.anyoption.android.app.event;

import android.content.Intent;

/**
 * Event that occurs when the application receives new message from the GCM (Google Cloud Messaging).
 * @author Anastas Arnaudov
 *
 */
public class GCMEvent {

	/**
	 * Intent that holds the data.
	 */
	private Intent intent;
	
	/**
	 * {@link GCMEvent}
	 * @param intent
	 */
	public GCMEvent(Intent intent){
		this.setIntent(intent);
	}

	/**
	 * Gets the Intent that holds the data.
	 * @return
	 */
	public Intent getIntent() {
		return intent;
	}

	/**
	 * Sets the Intent that holds the data.
	 */
	public void setIntent(Intent intent) {
		this.intent = intent;
	}
	
}
