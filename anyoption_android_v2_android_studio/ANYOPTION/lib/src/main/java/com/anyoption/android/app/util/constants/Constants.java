package com.anyoption.android.app.util.constants;

import android.annotation.SuppressLint;

/**
 * Class for constants.
 * @author Anastas Arnaudov
 *
 */
@SuppressLint("UseSparseArrays")
public class Constants {

	public static final String COOKIE_SESSION_ID = "JSESSIONID";

	public static final String FONTS_DIR = "fonts/";
	public static final long MOBILE_WRITER_ID = 200;
	
	public static final long DEFAULT_LANGUAGE_ID = 2;
	
	// TODO These are temp constants. When Skin class is updated remove these.
	public static final long SKIN_DUTCH = 23;
	public static final long SKIN_SWEDISH = 24;

	public static final int LOGIN_APPOXEE_RATE_US_COUNT = 3;

	public static final int PIXEL_TYPE_OPEN 		= 3;
	public static final int PIXEL_TYPE_REGISTER 	= 4;
	public static final int PIXEL_TYPE_DEPOSIT		= 5;

	public static final int DYNAMICS_OPPORTUNITY_TYPE_ID = 7;
	public static final long MARKET_GROUP_ID_OPTION_PLUS = 99;
	public static final long MARKET_GROUP_ID_DYNAMICS 	 = 100;

	public static final int PRODUCT_TYPE_ID_BINARY			= 1;
	public static final int PRODUCT_TYPE_ID_OPTION_PLUS		= 3;
	public static final int PRODUCT_TYPE_ID_BUBBLES			= 5;
	public static final int PRODUCT_TYPE_ID_DYNAMICS		= 6;

	//################################################################################################
	///////////////////					SERVICES			/////////////////////////////////////////
	//################################################################################################
	
	public static final String SERVICE_GCM_UPDATE										= "c2dmUpdate";
	public static final String SERVICE_GET_USER 										= "getUser";
	public static final String SERVICE_GET_INVESTMENTS 									= "getUserInvestments";
	public static final String SERVICE_GET_MARKET_GROUPS    							= "getMarketGroups";
	public static final String SERVICE_GET_ASSET_INDEX_BY_MARKET						= "getAssetIndexByMarket";
	public static final String SERVICE_GET_PAST_EXPIRIES 								= "getPastExpiries";
	public static final String SERVICE_GET_CHART_DATA	 								= "getChartData";
	public static final String SERVICE_GET_TRANSACTIONS									= "getUserTransactions";
	public static final String SERVICE_GET_MIN_FIRST_DEPOSIT							= "getMinFirstDeposit";
	public static final String SERVICE_GET_FIRST_EVER_DEPOSIT_CHECK						= "getFirstEverDepositCheck";
	public static final String SERVICE_GET_BONUSES										= "getUserBonuses";
	public static final String SERVICE_GET_CHANGE_PASSWORD								= "getChangePassword";
	public static final String SERVICE_GET_CHART_DATA_PREVIOUS_HOUR						= "getChartDataPreviousHour";
	public static final String SERVICE_GET_CUP_INFO										= "getCUPInfo";
	public static final String SERVICE_GET_MAX_CUP_WITHDRAWAL							= "getMaxCupWithdrawal";
	public static final String SERVICE_GET_FEE_AMOUNT_BY_TYPE							= "getFeeAmountByType";
	public static final String SERVICE_GET_SKIN_CURRENCIES_OPTIONS						= "getSkinCurrenciesOptions";
	public static final String SERVICE_GET_SKIN_CURRENCIES								= "getSkinCurrencies";
	public static final String SERVICE_GET_AVAILABLE_CREDIT_CARDS_WITH_EMPTY_LINE		= "getAvailableCreditCardsWithEmptyLine";
	public static final String SERVICE_GET_AVAILABLE_CREDIT_CARDS						= "getAvailableCreditCards";
	public static final String SERVICE_GET_CURRENCY_LETTERS								= "getCurrencyLetters";
	public static final String SERVICE_GET_COUNTRY_ID									= "getCountryId";
	public static final String SERVICE_GET_INBOX_MAILS									= "getUserInboxMails";
	public static final String SERVICE_GET_PRICE										= "getPrice";
	public static final String SERVICE_GET_MARKET_LIST									= "getMarketList";
	public static final String SERVICE_GET_WITHDRAWAL_CC_LIST							= "getWithdrawCCList";
	public static final String SERVICE_GET_MANDATORY_QUESTIONNAIRIE						= "getUserMandatoryQuestionnaire";
	public static final String SERVICE_GET_OPTIONAL_QUESTIONNAIRIE_ANSWERS				= "getUserOptionalQuestionnaire";
	public static final String SERVICE_GET_SINGLE_QUESTIONNAIRIE_ANSWERS				= "getUserSingleQuestionnaire";
	public static final String SERVICE_GET_SINGLE_QUESTIONNAIRE_ANSWERS_DYNAMIC			= "getUserSingleQuestionnaireDynamic";
	public static final String SERVICE_GET_LOGOUT_BALANCE_STEP							= "getLogoutBalanceStep";
	public static final String SERVICE_GET_ALL_RETURN_ODDS								= "getAllReturnOdds";
	public static final String SERVICE_GET_BANK_WIRE_WITHDRAW_OPTIONS					= "getBankWireWithdrawOptions";
	public static final String SERVICE_GET_CITIES										= "getCitiesOptions";
	public static final String SERVICE_GET_BANKS_ET										= "getBanksET";
	public static final String SERVICE_GET_USER_DOCUMENTS								= "retrieveUserDocuments";
	public static final String SERVICE_GET_SPLIT_BALANCE								= "getUserDepositBonusBalance";
	public static final String SERVICE_GET_WITHDRAW_BONUS_REGULATION_STATE 				= "getWithdrawBonusRegulationState";
	public static final String SERVICE_GET_FORGET_PASSWORD_CONTACTS 					= "getForgetPasswordContacts";
	public static final String SERVICE_GET_CHART_DATA_COMMON							= "getChartDataCommon";
	public static final String SERVICE_CHECK_BAROPAY_WITHDRAW_AVAILABLE					= "checkBaroPayWithdrawAvailable";
	public static final String SERVICE_CHECK_FOR_UPDATE									= "checkForUpdate";
	public static final String SERVICE_IS_WITHDRAW_WEEKENDS								= "isWithdrawalWeekendsAvailable";
	public static final String SERVICE_INSERT_INVESTMENT	 							= "insertInvestment";
	public static final String SERVICE_INSERT_WITHDRAWS_REVERSE							= "insertWithdrawsReverse";
	public static final String SERVICE_INSERT_BAROPAY_DEPOSIT							= "insertBaropayDeposit";
	public static final String SERVICE_INSERT_CONTACT									= "insertContact";
	public static final String SERVICE_INSERT_DEPOSIT_CARD								= "insertDepositCard";
	public static final String SERVICE_INSERT_USER										= "insertUser";
	public static final String SERVICE_INSERT_REGISTER_ATTEMPT							= "insertRegisterAttempt";
	public static final String SERVICE_INSERT_WITHDRAW_BANK								= "insertWithdrawBank";
	public static final String SERVICE_INSERT_WITHDRAW_BAROPAY							= "insertWithdrawBaropay";
	public static final String SERVICE_INSERT_WITHDRAW_CARD								= "insertWithdrawCard";
	public static final String SERVICE_INSERT_WITHDRAW_DELTAPAY							= "insertWithdrawDeltapay";
	public static final String SERVICE_INSERT_CARD										= "insertCard";
	public static final String SERVICE_INSERT_DIRECT_DEPOSIT							= "insertDirectDeposit";
	public static final String SERVICE_FINISH_DIRECT_DEPOSIT							= "finishDirectDeposit";
	public static final String SERVICE_INSERT_WITHDRAW_CANCEL 							= "insertWithdrawCancel";
	public static final String SERVICE_UPDATE_QUESTIONNAIRIE_ANSWERS					= "updateUserQuestionnaireAnswers";
	public static final String SERVICE_UPDATE_MANDATORY_QUESTIONNAIRIE					= "updateMandatoryQuestionnaireDone";
	public static final String SERVICE_UPDATE_OPTIONAL_QUESTIONNAIRIE					= "updateOptionalQuestionnaireDone";
	public static final String SERVICE_UPDATE_SINGLE_QUESTIONNAIRIE						= "updateSingleQuestionnaireDone";	
	public static final String SERVICE_UPDATE_BONUS_STATUS								= "updateBonusStatus";
	public static final String SERVICE_UPDATE_CARD										= "updateCard";
	public static final String SERVICE_UPDATE_USER										= "updateUser";
	public static final String SERVICE_UPDATE_USER_ADDITIONAL_FIELDS					= "updateUserAdditionalFields";
	public static final String SERVICE_UPDATE_MAIL_BOX									= "updateMailBox";
	public static final String SERVICE_UPDATE_IS_KNOWLEDGE_QUESTION						= "updateIsKnowledgeQuestion";
	public static final String SERVICE_UBLOCK_USER_FOR_THRESHOLD						= "unblockUserForThreshold";
	public static final String SERVICE_VALIDATE_WITHDRAW_BANK							= "validateWithdrawBank";
	public static final String SERVICE_VALIDATE_BAROPAY_WITHDRAW						= "validateBaroPayWithdraw";
	public static final String SERVICE_VALIDATE_WITHDRAW_CARD							= "validateWithdrawCard";
	public static final String SERVICE_VALIDATE_EMAIL									= "validateEmail";	
	public static final String SERVICE_INSURANCE_BANNER_SEEN							= "insuranceBannerSeen";
	public static final String SERVICE_SEND_PASSWORD									= "sendPassword";
	public static final String SERVICE_SEND_BANK_TRANSFER_DETAILS_USER                  = "sendBankTransferDetailsUser";
	public static final String SERVICE_SKIN_UPDATE										= "skinUpdate";
	public static final String SERVICE_OPTION_PLUS_FLASH_SELL							= "optionPlusFlashSell";
	public static final String SERVICE_SEND_DEPOSIT_RECEIPT								= "SendDepositReceipt";
	public static final String SERVICE_BUY_INSURANCES									= "buyInsurances";
	public static final String SERVICE_DELETE_CREDIT_CARD								= "deleteCreditCard";
	public static final String SERVICE_LOGOUT											= "logoutUser";
	public static final String SERVICE_UPLOAD_DOCUMENTS									= "UploadDocumentsService";
	public static final String SERVICE_FIRE_SERVER_PIXEL								= "fireServerPixel";
	public static final String SERVICE_INSERT_DYNAMICS_INVESTMENT						= "insertDynamicsInvestment";
	public static final String SERVICE_SELL_DYNAMICS_INVESTMENT							= "sellDynamicsInvestment";
	public static final String SERVICE_SELL_ALL_DYNAMICS_INVESTMENT						= "sellAllDynamicsInvestments";
	public static final String SERVICE_CANCEL_INVESTMENT								= "cancelInvestment";
	public static final String SERVICE_CANCEL_INVESTMENT_SHOW_AGAIN 					= "changeCancelInvestmentCheckboxState";
	public static final String SERVICE_MARKETS_INDEXES									= "getAssetIndexMarketsPerSkin";
	public static final String SERVICE_VALIDATE_CC_NUMBER								= "validateCC";
	public static final String SERVICE_GET_HOLIDAY_MESSAGE								= "getUserHolidayMessage";
	public static final String SERVICE_GET_SORTED_COUNTRIES								= "getSortedCountries";
	public static final String SERVICE_GET_SKINS										= "getVisibleSkins";

	//################################################################################################

	//################################################################################################
	///////////////////					NEW SERVICES			/////////////////////////////////////////
	//################################################################################################

	public static final String SERVICE_GET_ASSET_INFO									= "AssetIndexServices/getAssetIndexMarketPerSkin";
	public static final String SERVICE_SHOW_WITHDRAW_DETAILS							= "WithdrawUserDetailsServices/showWithdrawUserDetails";
	public static final String SERVICE_GET_WITHDRAW_DETAILS								= "WithdrawUserDetailsServices/getWithdrawUserDetails";
	public static final String SERVICE_INSERT_WITHDRAW_DETAILS							= "WithdrawUserDetailsServices/insertWithdrawUserDetailas";
	public static final String SERVICE_UPDATE_WITHDRAW_DETAILS							= "WithdrawUserDetailsServices/updateWithdrawUserDetils";
	public static final String SERVICE_SKIP_WITHDRAW_DETAILS							= "WithdrawUserDetailsServices/skipedWithdrawBankUserDetils";

	//################################################################################################

	//################################################################################################
	/////////////////////////				EXTRAS					//////////////////////////////////
	//################################################################################################
	
	public static final String EXTRA_UPDATE_DOWNLOAD_LINK 		= "update_download_link";
	public static final String EXTRA_UPDATE_MANDATORY			= "update_mandatory";
	public static final String EXTRA_SLIDE_VIEW_ID 				= "slide_view_id";
	public static final String EXTRA_SKIN_ID 					= "skin_id";
	public static final String EXTRA_FRAGMENT_TO_BACK_STACK 	= "fragment_to_back_stack";
	public static final String EXTRA_FRAGMENT_FROM_BACK_STACK 	= "fragment_from_back_stack";
	public static final String EXTRA_NAVIGATION_HORIZONTAL 		= "nav_horizontal";
	public static final String EXTRA_NAVIGATION_DEEP 			= "nav_deep";
	public static final String EXTRA_ACTION_BAR_TITLE 			= "action_bar_title";
	public static final String EXTRA_ACTION_BAR_TITLE_IMAGE 	= "action_bar_title_image";
	public static final String EXTRA_CALENDAR 					= "calendar";
	public static final String EXTRA_NAVIGATION_TO_STACK_BOTTOM = "navigation_to_stack_bottom";
	public static final String EXTRA_SCREEN 					= "screen";
	public static final String EXTRA_SECOND_SCREEN 				= "second_screen";
	public static final String EXTRA_BUNDLE 					= "bundle";
	public static final String EXTRA_CHOOSE_RETURN_PAIRS 		= "choose_return_pairs";
	public static final String EXTRA_CHOOSE_RETURN_SELECTED_ITEM = "choose_return_selected_item";
	public static final String EXTRA_TRADE_TAB 					= "trade_tab";
	public static final String EXTRA_MARKET 					= "market";
	public static final String EXTRA_MARKET_NAME 				= "market_name";
	public static final String EXTRA_MARKET_ID 					= "market_id";
	public static final String EXTRA_MARKETS					= "markets";
	public static final String EXTRA_ASSETS_LIST_MODE 			= "assets_list_mode";
	public static final String EXTRA_ASSETS_LIST_FILTER 		= "assets_list_filter";
	public static final String EXTRA_MARKET_PRODUCT_TYPE		= "market_product_type";
	public static final String EXTRA_SHOW_HELP_BUTTON 			= "remove_help_button";
	public static final String EXTRA_HELP_BUTTON		     	= "help_button";
	public static final String EXTRA_MENU_BUTTON		     	= "menu_button";
	public static final String EXTRA_HELP_TITLE				    = "help_title";
	public static final String EXTRA_HELP_MESSAGE			    = "help_message";
	public static final String EXTRA_BONUS					    = "bonus";
	public static final String EXTRA_BONUS_FACTOR			    = "bonus-factor";
	public static final String EXTRA_DEPOSIT_AMOUNT				= "deposit_amount";
	public static final String EXTRA_DEPOSIT_DATE				= "deposit_date";
	public static final String EXTRA_DEPOSIT_TYPE			    = "deposit_type";
	public static final String EXTRA_DEPOSIT_ID			    	= "deposit_id";
	public static final String EXTRA_CREDIT_CARD                = "credit_card";
	public static final String EXTRA_COUNTRY_ID                 = "country_id";
	public static final String EXTRA_NOTIFICATION_BUNDLE        = "notification_bundle";
	public static final String EXTRA_IS_LANGUAGE_BUTTON        	= "is_lang_button";
	public static final String EXTRA_IS_LOCALE_CHANGE        	= "is_locale_change";
	public static final String EXTRA_USER			        	= "user";
	public static final String EXTRA_USER_REGULATIONS	    	= "user_regulations";
	public static final String EXTRA_GO_TO_MAIN			    	= "go_to_main";
	public static final String EXTRA_IMAGE_ID			    	= "image_id";
	public static final String EXTRA_IS_MIN_18			    	= "is_min_18";
	public static final String EXTRA_FLAGS				    	= "flags";
	public static final String EXTRA_MY_OPTIONS_TAB				= "my_options_tab";
	public static final String EXTRA_PUSH_NOTIFICATION			= "push_notification";
	public static final String EXTRA_AUTO_LOAD_MARKET 			= "auto_load_market";
	public static final String EXTRA_RELOAD_MARKET_KEY 			= "reload_market";
	public static final String EXTRA_IS_FROM_REG_FUNNEL			= "is_from_reg_funnel";
	public static final String EXTRA_MESSAGE					= "message";
	public static final String EXTRA_AMOUNT						= "amount";
	public static final String EXTRA_FEE						= "fee";
	public static final String EXTRA_TYPE						= "type";
	public static final String EXTRA_INVESTMENT_ID				= "investment_id";	
	public static final String EXTRA_CANCELABLE					= "is_cancelable";
	public static final String EXTRA_CASH_BALANCE				= "cash_balance";
	public static final String EXTRA_PERIOD						= "period";
	public static final String EXTRA_PAGE 						= "url";
	public static final String EXTRA_THREE_D_ACS_URL 			= "three_d_acs_url";
	public static final String EXTRA_THREE_D_TERM_URL 			= "three_d_term_url";
	public static final String EXTRA_THREE_D_MD		 			= "three_d_tmd";
	public static final String EXTRA_IS_FROM_LAUNCH		 		= "is_from_launch";
	public static final String EXTRA_IS_SINGLE_BONUS		 	= "is_signle_bonus";
	public static final String EXTRA_MIN_INV_AMOUNT		 		= "min_inv_amount";
	public static final String EXTRA_IS_IN_THE_MONEY		 	= "is_in_the_money";
	public static final String EXTRA_IS_EXPIRED_ABOVE 			= "is_expired_above";
	public static final String EXTRA_CENTRAL_LEVEL 				= "central_level";
	public static final String EXTRA_EXPIRY_LEVEL 				= "expiry_level";
	public static final String EXTRA_TIME		 				= "time";
	public static final String EXTRA_INVESTED_AMOUNT		 	= "invested_amount";
	public static final String EXTRA_RETURN_AMOUNT		 		= "return_amount";
	public static final String EXTRA_DECIMAL_POINTS		 		= "decimal_points";
	public static final String EXTRA_INVESTMENT			 		= "investment";
	public static final String EXTRA_IS_INVESTMENT_OPEN 		= "is_investment_open";
	public static final String EXTRA_LONG_TERM_TRADING_TIMES	= "long_term_trading_times";
	public static final String EXTRA_OPPORTUNITY_ID				= "opportunity_id";
	public static final String EXTRA_INVESTMENTS_LIST	 		= "investments_list";
	public static final String EXTRA_IS_TRY_TO_INVEST	 		= "is_try_to_invest";
	public static final String EXTRA_DEV2_SECONDS	 			= "dev2_seconds";
	public static final String EXTRA_CANCEL_SECONDS	 			= "cancel_seconds";
	public static final String EXTRA_IS_FROM_TRADE_SCREEN 		= "is_from_trade_screen";
	public static final String EXTRA_MARKET_GROUPS 				= "market_groups";
	public static final String EXTRA_MARKETS_INDEXES			= "markets_indexes";
	public static final String EXTRA_MARKETS_FORMULAS 			= "markets_formulas";
	public static final String EXTRA_COUNTRIES					= "countries";
	public static final String EXTRA_VISIBLE_SKINS				= "visible_skins";
	public static final String EXTRA_IS_APP_RESTARTED        	= "is_app_restarted";
	public static final String EXTRA_IS_ACTIVITY_RECREATED    	= "is_activity_recreated";
	public static final String EXTRA_IS_SHOULD_SHOW_WELCOME 	= "is_should_show_welcome";
	public static final String EXTRA_QUESTIONNAIRE_PAGE 		= "questionnaire_page";
	public static final String EXTRA_LONG_TERM_TRADING_IDS		= "long_term_trading_ids";

	//################################################################################################
	
	
	//################################################################################################
	///////////////////				PREFERENCES			/////////////////////////////////////////
	//################################################################################################
	
	public static final String PREFERENCES 										= "com.anyoption.android.app_preferences";
	
	public static final String PREFERENCES_GCM_REGISTER_ID 						= "pref_gcm_reg_id";
	public static final String PREFERENCES_GCM_REGISTER_EXPIRATION_TIME 		= "pref_gcm_reg_ex_time";
	public static final String PREFERENCES_APPLICATION_VERSION 					= "pref_app_v";
	public static final String PREFERENCES_USER_NAME 							= "pref_user_name";
	public static final String PREFERENCES_PASSWORD 							= "pref_pass";
	public static final String PREFERENCES_PASSWORD_LOGIN	 					= "pref_pass_login";
	public static final String PREFERENCES_REFERRER_PARAM 						= "pref_referrer_param";
	public static final String PREFERENCES_COMBINATION_ID 						= "pref_comb_id";
	public static final String PREFERENCES_DYNAMIC_PARAM 						= "pref_dyn_par";
	public static final String PREFERENCES_MID 									= "pref_mid";
	public static final String PREFERENCES_ETS_MID 								= "pref_ets_mid";
	public static final String PREFERENCES_AFF_SUB1 							= "pref_aff_sub1";
	public static final String PREFERENCES_AFF_SUB2 							= "pref_aff_sub2";
	public static final String PREFERENCES_GCLID 								= "pref_gclid";
	public static final String PREFERENCES_IS_FIRST_APPLICATION_OPEN 			= "pref_first_app_open";
	public static final String PREFERENCES_FIRST_APPLICATION_OPEN_TIME 			= "pref_first_app_open_time";
	public static final String PREFERENCES_TIME_OFFSET 							= "pref_time_offset";
	public static final String PREFERENCES_TIME_ON_UPDATE 						= "pref_time_on_update";
	public static final String PREFERENCES_LOCALE 								= "pref_locale";	
	public static final String PREFERENCES_SKINID								= "pref_skinid";
	public static final String PREFERENCES_LANGUAGE_CODE						= "pref_lang_code";
	public static final String PREFERENCES_LAST_OPENED_MARKET					= "pref_last_opened_market";
	public static final String PREFERENCES_IS_INTROS_ON 						= "pref_is_log_atleast_once";
	public static final String PREFERENCES_IS_NOTIFICATIONS_ON 					= "pref_is_notifications_on";
	public static final String PREFERENCES_IS_SOUNDSS_ON	 					= "pref_is_sounds_on";
	public static final String PREFERENCES_IS_TRADE_TOOLTIP_SHOWN	 			= "pref_is_trade_tooltip_shown";
	public static final String PREFERENCES_IS_LOGGED_ATLEAST_ONCE	 			= "pref_is_logged_atleast_once";
	public static final String PREFERENCES_IS_FIRST_LOGIN	 					= "pref_is_first_login";
	public static final String PREFERENCES_IS_FIRST_MAIN_SCREEN					= "pref_is_first_main_screen";
	public static final String PREFERENCES_LOGIN_COUNT							= "pref_login_count";
	public static final String PREFERENCES_CAMPAIGN_PUBLISHER_ID 				= "pref_target_publisher_id";
	public static final String PREFERENCES_CAMPAIGN_PUBLISHER_IDENTIFIER		= "pref_target_publisher_identifier";
	public static final String PREFERENCES_IS_CAME_FROM_MARKETING 				= "pref_is_came_from_marketing";
	public static final String PREFERENCES_IS_DYNAMICS_IN_LOGOUT 				= "pref_is_dynamics_in_logout";
	public static final String PREFERENCES_ANDROID_ADVERTISING_ID 				= "pref_android_advertising_id";
	public static final String PREFERENCES_RESTARTED_APP_USER_SESSION_ID		= "pref_restarted_app_user_sess_id";

	//################################################################################################

	//################################################################################################
	////////////////////			ERROR CODES				//////////////////////////////////////////
	//################################################################################################
    
    public static final int ERROR_CODE_SUCCESS          						= 0;
    public static final int ERROR_CODE_WITHDRAWAL_OPEN          				= 110;
    public static final int ERROR_CODE_INVALID_INPUT    						= 200;
    public static final int ERROR_CODE_INVALID_ACCOUNT  						= 201;
    public static final int ERROR_CODE_DUP_REQUEST      						= 202;
    public static final int ERROR_CODE_MISSING_PARAM    						= 203;
    public static final int ERROR_CODE_LOGIN_FAILED     						= 205;
    public static final int ERROR_CODE_INV_VALIDATION   						= 206;
    public static final int ERROR_CODE_GENERAL_VALIDATION 						= 207;
    public static final int ERROR_CODE_VALIDATION_WITHOUT_FIELD 				= 208;
    public static final int ERROR_CODE_INVEST_BALANCE   						= 209;
    public static final int ERROR_CODE_TRANSACTION_FAILED  						= 210;
    public static final int ERROR_CODE_CC_NOT_SUPPORTED  						= 211;
	public static final int ERROR_CODE_NO_CASH_FOR_NIOU							= 213;
	public static final int ERROR_CODE_EMAIL_ALREADY_IN_USE						= 214;
	public static final int ERROR_CODE_EMAIL_EXISTING_IN_AO						= 216;
	public static final int ERROR_CODE_REGULATION_MISSING_QUESTIONNAIRE			= 300;
    public static final int ERROR_CODE_REGULATION_SUSPENDED						= 301;
    public static final int ERROR_CODE_REGULATION_SUSPENDED_DOCUMENTS			= 302;
    public static final int ERROR_CODE_USER_NOT_REGULATED						= 303;
    public static final int ERROR_CODE_REG_SUSPENDED_QUEST_INCORRECT			= 305;
    public static final int ERROR_CODE_REGULATION_USER_RESTRICTED				= 304;
    public static final int ERROR_CODE_SHOW_WARNING_DUE_DOCUMENTS				= 306;
	public static final int ERROR_CODE_REGULATION_USER_PEP_BLOCKED				= 307;
	public static final int ERROR_CODE_REGULATION_USER_IS_TRESHOLD_BLOCK		= 308;
	public static final int ERROR_CODE_VALIDATION_LOW_CASH_BALANCE				= 309;
	public static final int ERROR_CODE_VALIDATION_MAXIMUM_EXPOSURE				= 310;
    public static final int ERROR_CODE_QUESTIONNAIRE_NOT_FULL					= 400;
    public static final int ERROR_CODE_QUESTIONNAIRE_NOT_SUPPORTED				= 401;
    public static final int ERROR_CODE_QUESTIONNAIRE_ALREADY_DONE				= 402;
	public static final int ERROR_CODE_SESSION_EXPIRED							= 419;
    public static final int ERROR_CODE_OPP_NOT_EXISTS							= 500;
	public static final int ERROR_CODE_NOT_ACCEPTED_TERMS 						= 701;
    public static final int ERROR_CODE_UNKNOWN          						= 999;
	//################################################################################################
	
	//################################################################################################
	////////////////////////////////         CURRENCIES         //////////////////////////////////
	//################################################################################################
	
	public static final long CURRENCY_ILS_ID = 1;
	public static final long CURRENCY_USD_ID = 2;
	public static final long CURRENCY_EUR_ID = 3;
	public static final long CURRENCY_GBP_ID = 4;
	public static final long CURRENCY_TRY_ID = 5;
	public static final long CURRENCY_RUB_ID = 6;
	public static final long CURRENCY_CNY_ID = 7;
	public static final long CURRENCY_KRW_ID = 8;
	public static final long CURRENCY_SEK_ID = 9;
	public static final long CURRENCY_ZAR_ID = 11;
	public static final long CURRENCY_CZK_ID = 12;
	//################################################################################################
	
	//################################################################################################
	///////////////////					REQUEST CODES		//////////////////////////////////////////
	//					(Used in the onActivityResult(....)  callback method)						//
	//################################################################################################
	
	public final static int REQUEST_CODE_GOOGLE_PLAY_SERVICES 	= 1;
	public final static int REQUEST_CODE_CREDIT_CARD_SCAN 		= 2000;
	
	//################################################################################################
	
	//################################################################################################
	//////////////////            BONUSES         //////////////////////////////
	//################################################################################################

	public static final long BONUS_STATE_ALL	 			= 0;
	public static final long BONUS_STATE_GRANTED 			= 1;
	public static final long BONUS_STATE_ACTIVE 			= 2;
	public static final long BONUS_STATE_USED 				= 3;
	public static final long BONUS_STATE_DONE			 	= 4;
	public static final long BONUS_STATE_REFUSED		 	= 5;
	public static final long BONUS_STATE_MISSED			 	= 6;
	public static final long BONUS_STATE_CANCELED 			= 7;
	public static final long BONUS_STATE_WITHDRAWN 			= 8;
	public static final long BONUS_STATE_WAGERING_WAIVED 	= 9;
	public static final long BONUS_STATE_PANDING		 	= 10;
	//################################################################################################	
		
	//################################################################################################
	//////////////////            TRANSACTIONS         //////////////////////////////
	//################################################################################################

	public static final int TRANSACTION_STATUS_REQUESTED 		= 4;
	public static final int TRANSACTION_STATUS_APPROVED 		= 9;
	//################################################################################################
	
	//################################################################################################
	//////////////				COUNTRY CODES		//////////////////////
	//################################################################################################
	
	public static final long COUNTRY_UNITED_STATES_ID 	= 220;
	public static final long COUNTRY_ISRAEL_ID 			= 1;
	public static final long COUNTRY_IRAN_ID 			= 99;
	public static final long COUNTRY_NORTH_KOREA_ID 	= 154;
	public static final long COUNTRY_SOUTH_KOREA_ID 	= 191;
	public static final long COUNTRY_CHINA_ID 			= 44;
	public static final long COUNTRY_GERMANY_ID 		= 80;
	public static final long COUNTRY_AUSTRALIA_ID 		= 14;
	
	
	//################################################################################################
	
	//################################################################################################
	////////////////////////					FORM FIELD NAMES				//////////////////////
	//################################################################################################
	public static final String EMPTY_STRING 						= "";
	public static final String FIELD_CARD_NUMBER 					= "ccNumber";
	public static final String FIELD_EMAIL 							= "email";
	public static final String FIELD_FIRST_NAME 					= "firstName";
	public static final String FIELD_LAST_NAME						= "lastName";
	public static final String FIELD_PHONE 							= "phone";
	public static final String FIELD_MOBILE_PHONE 					= "mobilePhone";
	public static final String FIELD_PASSWORD 						= "password";
	public static final String FIELD_PASSWORD_NEW 					= "passwordNew";
	public static final String FIELD_PASSWORD_RE_TYPE 				= "passwordReType";
	public static final String FIELD_STREET 						= "street";
	public static final String FIELD_STREET_NO 						= "streetNo";	
	public static final String FIELD_ZIP_CODE 						= "zipCode";
	public static final String FIELD_CITY_NAME 						= "cityName";
	public static final String FIELD_CURRENCY_ID 					= "currencyId";
	public static final String FIELD_AMOUNT 						= "amount";
	public static final String FIELD_HOLDER_NAME 					= "holderName";
	public static final String FIELD_CVV 							= "ccPass";
	public static final String FIELD_EXPIRY_MONTH 					= "expMonth";
	public static final String FIELD_EXPIRY_YEAR 					= "expYear";
	public static final String FIELD_EXPIRY_DATE 					= "exp";
	public static final String FIELD_BRANCH 						= "branch";
	
	public static final String NO_ID_NUM = "No-Id";
	public static final String NO_FEE = "0";
	//################################################################################################
	
	
	// ################################################################################################
	//////////////////////// UPLOAD DOCUMENTS DOCUMENT TYPES AND REQUEST CODES //////////////////////
	// ################################################################################################
	public static final long UPLOAD_DOCUMENTS_TYPE_UTILITY_BILL 				= 22;
	public static final long UPLOAD_DOCUMENTS_TYPE_PROOF_OF_ID 					= 4352;
	public static final long UPLOAD_DOCUMENTS_TYPE_PROOF_OF_ID_PASSPORT 		= 2;
	public static final long UPLOAD_DOCUMENTS_TYPE_PROOF_OF_ID_ID 				= 1;
	public static final long UPLOAD_DOCUMENTS_TYPE_PROOF_OF_ID_DRIVER_LICENSE 	= 21;
	public static final long UPLOAD_DOCUMENTS_TYPE_CREDIT_CARD 					= 4356;
	public static final long UPLOAD_DOCUMENTS_TYPE_CREDIT_CARD_FRONT 			= 23;
	public static final long UPLOAD_DOCUMENTS_TYPE_CREDIT_CARD_BACK 			= 24;
	public static final long UPLOAD_DOCUMENTS_TYPE_BANK_PROTOCOL	 			= 35;

	public static final long UPLOAD_DOCUMENTS_DOCUMENT_STATUS_REQUESTED			= 1;
	public static final long UPLOAD_DOCUMENTS_DOCUMENT_STATUS_IN_PROGRESS		= 2;
	public static final long UPLOAD_DOCUMENTS_DOCUMENT_STATUS_DONE 				= 3;
	public static final long UPLOAD_DOCUMENTS_DOCUMENT_STATUS_INVALID 			= 4;
	public static final long UPLOAD_DOCUMENTS_DOCUMENT_STATUS_NOT_NEEDED		= 5;
	// ################################################################################################
	
}

