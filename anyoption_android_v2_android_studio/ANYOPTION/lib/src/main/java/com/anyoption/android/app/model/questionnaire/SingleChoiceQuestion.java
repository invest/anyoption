package com.anyoption.android.app.model.questionnaire;

import java.util.List;

import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;

public abstract class SingleChoiceQuestion extends BaseQuestionItem {

	protected QuestionnaireQuestion question;
	protected QuestionnaireUserAnswer userAnswer;

	public SingleChoiceQuestion(long orderId, String quetionText, QuestionnaireQuestion question,
			QuestionnaireUserAnswer answer) {
		super(orderId, quetionText);
		this.question = question;
		this.userAnswer = answer;
	}

	public SingleChoiceQuestion(long orderId, String questionText, List<String> answerTexts) {
		super(orderId, questionText, answerTexts);
	}

	public SingleChoiceQuestion(long orderId, String quetionText) {
		this(orderId, quetionText, null, null);
	}

	public QuestionnaireQuestion getQuestion() {
		return question;
	}

	public void setQuestion(QuestionnaireQuestion question) {
		this.question = question;
	}

	public QuestionnaireUserAnswer getUserAnswer() {
		return userAnswer;
	}

	public void setUserAnswer(QuestionnaireUserAnswer userAnswer) {
		this.userAnswer = userAnswer;
	}

}
