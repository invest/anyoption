package com.anyoption.android.app.manager;

import com.anyoption.android.app.Application;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

/**
 * Telephony Manager that manages the calls.
 * @author Anastas Arnaudov
 *
 */
public class TelephonyManager {

	public static final String TAG = TelephonyManager.class.getSimpleName();
	
	private Application application;
	private android.telephony.TelephonyManager telephonyManager;
	
	public TelephonyManager(Application application){
		Log.d(TAG, "On Create");
		
		this.application = application;
		this.telephonyManager = (android.telephony.TelephonyManager) this.application.getSystemService(Context.TELEPHONY_SERVICE);
	}

	public void callService(String phoneNumber) {
		//TODO CHEck if the device can make phone calls
		Intent callIntent = new Intent(Intent.ACTION_CALL);
		callIntent.setData(Uri.parse("tel:" + phoneNumber));
		application.getCurrentActivity().startActivity(callIntent);
	}

	//TODO
}
