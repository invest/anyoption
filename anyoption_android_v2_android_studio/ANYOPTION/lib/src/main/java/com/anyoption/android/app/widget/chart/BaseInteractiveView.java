package com.anyoption.android.app.widget.chart;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.EdgeEffectCompat;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.OverScroller;

/**
 * Basic view which handles zoom, scroll and pinch.
 * 
 * @author Mario Kutlev
 * 
 */
public abstract class BaseInteractiveView extends View {
	
	private static final String TAG = BaseInteractiveView.class.getSimpleName();

    /**
     * Initial fling velocity for pan operations, in screen widths (or heights) per second.
     *
     * @see #panLeft()
     * @see #panRight()
     * @see #panUp()
     * @see #panDown()
     */
    private static final float PAN_VELOCITY_FACTOR = 2f;

    /**
     * The scaling factor for a single zoom 'step'.
     *
     * @see #zoomIn()
     * @see #zoomOut()
     */
    private static final float ZOOM_AMOUNT = 0.25f;

    // Viewport extremes. See mCurrentViewport for a discussion of the viewport.
    protected static final float AXIS_X_MIN = -1f;
    protected static final float AXIS_X_MAX = 1f;
    protected static final float AXIS_Y_MIN = -1f;
    protected static final float AXIS_Y_MAX = 1f;
    protected static final float VIEWPORT_MAX_WIDTH = 2f;
    protected static final float VIEWPORT_MAX_HEIGHT = 2f;
    
    /**
     * The current viewport. This rectangle represents the currently visible chart domain
     * and range. The currently visible chart X values are from this rectangle's left to its right.
     * The currently visible chart Y values are from this rectangle's top to its bottom.
     * <p>
     * Note that this rectangle's top is actually the smaller Y value, and its bottom is the larger
     * Y value. Since the chart is drawn onscreen in such a way that chart Y values increase
     * towards the top of the screen (decreasing pixel Y positions), this rectangle's "top" is drawn
     * above this rectangle's "bottom" value.
     *
     * @see #contentRect
     */
    private RectF currentViewport = new RectF(AXIS_X_MIN, AXIS_Y_MIN, AXIS_X_MAX, AXIS_Y_MAX);

    /**
     * The current destination rectangle (in pixel coordinates) into which the chart data should
     * be drawn. Chart labels are drawn outside this area.
     *
     * @see #currentViewport
     */
    protected Rect contentRect = new Rect();

    // State objects and values related to gesture tracking.
    private ScaleGestureDetector scaleGestureDetector;
    private GestureDetectorCompat gestureDetector;
    
    protected OverScroller scroller;
    private Zoomer zoomer;
    private PointF zoomFocalPoint = new PointF();
	// Used only for zooms and flings.
	private RectF scrollerStartViewport = new RectF();
	private int minZoomPercent = 0;

    // Edge effect / overscroll tracking objects.
    private EdgeEffectCompat edgeEffectLeft;
    private EdgeEffectCompat edgeEffectRight;

    private boolean edgeEffectLeftActive;
    private boolean edgeEffectRightActive;

    private Point surfaceSizeBuffer = new Point();
    
	/**
	 * Indicates if view can be scrolled or zoomed VERTICALLY.
	 */
	private boolean isVerticalInteractionEnabled;

	/**
	 * Indicates if view can be scrolled or zoomed HORIZONTALLY.
	 */
	private boolean isHorizontalInteractionEnabled;
	
	private List<ClickListener> clickListeners = new ArrayList<BaseInteractiveView.ClickListener>();
	
	/**
	 * Implement this to listen for click events on the Chart.
	 * @author Anastas Arnaudov
	 *
	 */
	public interface ClickListener{
		/**
		 * Called when a click event occurs.
		 */
		public void onClick();
	}
	
	
    public BaseInteractiveView(Context context) {
        this(context, null, 0);
    }

    public BaseInteractiveView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseInteractiveView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if(!isInEditMode()){
        	// Sets up interactions
        	scaleGestureDetector = new ScaleGestureDetector(context, scaleGestureListener);
        	gestureDetector = new GestureDetectorCompat(context, gestureListener);
        	
        	scroller = new OverScroller(context);
        	zoomer = new Zoomer(context);
        	
        	// Sets up edge effects
        	edgeEffectLeft = new EdgeEffectCompat(context);
        	edgeEffectRight = new EdgeEffectCompat(context);
        	
        	setHorizontalInteractionEnabled(true);
        	setVerticalInteractionEnabled(true);        	
        }
    }

	/**
	 * Returns true if the view can be scrolled and zoomed vertically, false otherwise.
	 */
	public boolean isVerticalInteractionEnabled() {
		return isVerticalInteractionEnabled;
	}

	/**
	 * Changes the view's vertical interaction state.
	 * 
	 * @param enabled
	 *            true to enable view's vertical scroll and zoom, false otherwise.
	 */
	public void setVerticalInteractionEnabled(boolean enabled) {
		isVerticalInteractionEnabled = enabled;
	}

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
    	super.onSizeChanged(w, h, oldw, oldh);
        if(!isInEditMode()){
        	contentRect.set(
        			getPaddingLeft(),
        			getPaddingTop(),
        			getWidth() - getPaddingRight(),
        			getHeight() - getPaddingBottom());        	
        }
    }
    	  
    ////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //     Methods and objects related to drawing
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if(!isInEditMode()){
			
			// Clips the next few drawing operations to the content area
			int clipRestoreCount = canvas.save();
			canvas.clipRect(contentRect);
			
			drawEdgeEffects(canvas);
			
			// Removes clipping rectangle
			canvas.restoreToCount(clipRestoreCount);			
		}
	}

	/**
	 * Returns the zoom coefficient.
	 * @return If there is no zoom -> 1.
	 */
	protected float getZoomCoefficient(){
		return getActualWidth() / contentRect.width();
	}
	
	/**
	 * Returns the actual width of the drawing area.
	 * @return
	 */
	protected float getActualWidth(){
		return VIEWPORT_MAX_WIDTH * contentRect.width() / currentViewport.width();
	}
	
	/**
	 * Returns the actual height of the drawing area.
	 * @return
	 */
	protected float getActualHeight(){
		return VIEWPORT_MAX_HEIGHT * contentRect.height() / currentViewport.height();
	}
	
	/**
	 * Returns the actual X coordinate base on the given abstract x which is a fraction of the abstract width.
	 * <p><b>NOTE:</b>The X may be negative if it is < {@link #contentRect}.left which has actual x = 0;
	 * @param abstractX Should be in the [0,1] interval.
	 * @return
	 */
	protected float getActualX(float abstractX){
		//Transform the argument into [-1,1] interval:
		float x = 2 * abstractX - 1; 
		return getDrawX(x);
	}
	
	/**
	 * Returns the abstract x for the given actual x coordinate.
	 * @param x The actual drawn x coordinate.
	 * @return The abstract x which is a fraction of the whole view width. It is in [0,1] interval.
	 */
	protected float getAbstractX(float x){
		//Get the abstract x in [-1,1] interval.
		float abstractX = ( x * currentViewport.width() - contentRect.left ) / contentRect.width() + currentViewport.left;             
		//Transform the abstract x in [0,1] interval.
		abstractX = ( abstractX + 1 ) / 2;
		return abstractX;
	}
	
	/**
	 * Returns the actual Y coordinate base on the given abstract y which is a fraction of the abstract width.
	 * @param abstractY Should be in the [0,1] interval.
	 * @return
	 */
	protected float getActualY(float abstractY){
		//Transform the argument into [-1,1] interval:
		float y = 2 * abstractY - 1; 
		return getDrawX(y);
	}
	
    /** 
     * Computes the pixel offset for the given X chart value. This may be outside the view bounds.
     * @param x Should be in the [-1,1] interval.
     */
	protected float getDrawX(float x) {
		return contentRect.left + contentRect.width() * (x - currentViewport.left) / currentViewport.width();
    }

	/**  
	 * Computes the pixel offset for the given Y chart value. This may be outside the view bounds.
	 * @param y Should be in the [-1,1] interval.
	 */
	protected float getDrawY(float y) {
		return contentRect.bottom - contentRect.height() * (y - currentViewport.top)
				/ currentViewport.height();
	}

	/**
	 * Computes the pixel offset for the given Y value using the top and bottom paddings. This may
	 * be outside the view bounds.
	 * 
	 * @param y
	 * @param paddingTop
	 * @param paddingBottom
	 */
	protected float getDrawY(float y, float paddingTop, float paddingBottom) {
		return contentRect.bottom - paddingBottom
				- (contentRect.height() - (paddingTop + paddingBottom))
				* (y - currentViewport.top) / currentViewport.height();
	}

    /**
     * Draws the overscroll "glow" at the four edges of the chart region, if necessary. The edges
     * of the chart region are stored in {@link #contentRect}.
     *
     * @see EdgeEffectCompat
     */
    private void drawEdgeEffects(Canvas canvas) {
        // The methods below rotate and translate the canvas as needed before drawing the glow,
        // since EdgeEffectCompat always draws a top-glow at 0,0.

        boolean needsInvalidate = false;

        if (!edgeEffectLeft.isFinished()) {
            final int restoreCount = canvas.save();
            canvas.translate(contentRect.left, contentRect.bottom);
            canvas.rotate(-90, 0, 0);
            edgeEffectLeft.setSize(contentRect.height(), contentRect.width());
            if (edgeEffectLeft.draw(canvas)) {
                needsInvalidate = true;
            }
            canvas.restoreToCount(restoreCount);
        }

        if (!edgeEffectRight.isFinished()) {
            final int restoreCount = canvas.save();
            canvas.translate(contentRect.right, contentRect.top);
            canvas.rotate(90, 0, 0);
            edgeEffectRight.setSize(contentRect.height(), contentRect.width());
            if (edgeEffectRight.draw(canvas)) {
                needsInvalidate = true;
            }
            canvas.restoreToCount(restoreCount);
        }

        if (needsInvalidate) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //     Methods and objects related to gesture handling
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Finds the chart point (i.e. within the chart's domain and range) represented by the
     * given pixel coordinates, if that pixel is within the chart region described by
     * {@link #contentRect}. If the point is found, the "dest" argument is set to the point and
     * this function returns true. Otherwise, this function returns false and "dest" is unchanged.
     */
    private boolean hitTest(float x, float y, PointF dest) {
        if (!contentRect.contains((int) x, (int) y)) {
            return false;
        }

        dest.set(
                currentViewport.left
                        + currentViewport.width()
                        * (x - contentRect.left) / contentRect.width(),
                currentViewport.top
                        + currentViewport.height()
                        * (y - contentRect.bottom) / -contentRect.height());
        return true;
     }

    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
    	// Handle the Click Events:
    	if(event.getAction() == MotionEvent.ACTION_UP){
    		for(ClickListener clickListener : clickListeners){
    			clickListener.onClick();
    		}
    	}
    	
        boolean retVal = scaleGestureDetector.onTouchEvent(event);
        retVal = gestureDetector.onTouchEvent(event) || retVal;
        return retVal || super.onTouchEvent(event);
    }

    /**
     * Adds a {@link ClickListener}.
     * @param clickListener
     */
    public void addClickListener(ClickListener clickListener){
    	clickListeners.add(clickListener);
    }
    
    /**
     * Removess a {@link ClickListener}.
     * @param clickListener
     */
    public void removeClickListener(ClickListener clickListener){
    	clickListeners.remove(clickListener);
    }
    
    /**
     * The scale listener, used for handling multi-finger scale gestures.
     */
    private final ScaleGestureDetector.OnScaleGestureListener scaleGestureListener
            = new ScaleGestureDetector.SimpleOnScaleGestureListener() {
        /**
         * This is the active focal point in terms of the viewport. Could be a local
         * variable but kept here to minimize per-frame allocations.
         */
        private PointF viewportFocus = new PointF();
        private float lastSpanX;
        private float lastSpanY;

        @Override
        public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
            lastSpanX = ScaleGestureDetectorCompat.getCurrentSpanX(scaleGestureDetector);
            lastSpanY = ScaleGestureDetectorCompat.getCurrentSpanY(scaleGestureDetector);
            return true;
        }

		// Handles pinch gesture to zoom in and out the view.
        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            float spanX = ScaleGestureDetectorCompat.getCurrentSpanX(scaleGestureDetector);
            float spanY = ScaleGestureDetectorCompat.getCurrentSpanY(scaleGestureDetector);

            float newWidth;
            if(isHorizontalInteractionEnabled()){
            	newWidth = lastSpanX / spanX * currentViewport.width();            	
            }else{
            	// Disable vertical scale using two fingers
            	newWidth = currentViewport.width();
            }
            
            float newHeight;
			if (isVerticalInteractionEnabled()) {
				newHeight = lastSpanY / spanY * currentViewport.height();
			} else {
				// Disable vertical scale using two fingers
				newHeight = currentViewport.height();
			}
            
            float focusX = scaleGestureDetector.getFocusX();
            float focusY = scaleGestureDetector.getFocusY();
            hitTest(focusX, focusY, viewportFocus);
            
            float left = viewportFocus.x - newWidth * (focusX - contentRect.left) / contentRect.width();
            float top = viewportFocus.y - newHeight * (contentRect.bottom - focusY) / contentRect.height();
            float right = left + newWidth;
            float bottom = top + newHeight;
            
			if ((spanX > lastSpanX) && isZoomedInMax()) {
				// Do not zoom in more.
				return false;
			}
            
			currentViewport.set(left, top, right, bottom);
            
            constrainViewport();
            ViewCompat.postInvalidateOnAnimation(BaseInteractiveView.this);

            lastSpanX = spanX;
            lastSpanY = spanY;
            return true;
        }
    };

    /**
     * Ensures that current viewport is inside the viewport extremes defined by {@link #AXIS_X_MIN},
     * {@link #AXIS_X_MAX}, {@link #AXIS_Y_MIN} and {@link #AXIS_Y_MAX}.
     */
    private void constrainViewport() {
        currentViewport.left = Math.max(AXIS_X_MIN, currentViewport.left);
        currentViewport.top = Math.max(AXIS_Y_MIN, currentViewport.top);
        currentViewport.bottom = Math.max(Math.nextUp(currentViewport.top),
                Math.min(AXIS_Y_MAX, currentViewport.bottom));
        currentViewport.right = Math.max(Math.nextUp(currentViewport.left),
                Math.min(AXIS_X_MAX, currentViewport.right));
    }

    /**
     * The gesture listener, used for handling simple gestures such as double touches, scrolls,
     * and flings.
     */
    private final GestureDetector.SimpleOnGestureListener gestureListener
            = new GestureDetector.SimpleOnGestureListener() {
        @Override
        public boolean onDown(MotionEvent e) {
            releaseEdgeEffects();
            scrollerStartViewport.set(currentViewport);
            scroller.forceFinished(true);
            ViewCompat.postInvalidateOnAnimation(BaseInteractiveView.this);
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
        	if(isZoomedInMax()) {
        		return false;
        	}

			zoomer.forceFinished(true);
			if (hitTest(e.getX(), e.getY(), zoomFocalPoint)) {
				zoomer.startZoom(ZOOM_AMOUNT);
			}
            ViewCompat.postInvalidateOnAnimation(BaseInteractiveView.this);
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            // Scrolling uses math based on the viewport (as opposed to math using pixels).
            /**
             * Pixel offset is the offset in screen pixels, while viewport offset is the
             * offset within the current viewport. For additional information on surface sizes
             * and pixel offsets, see the docs for {@link computeScrollSurfaceSize()}. For
             * additional information about the viewport, see the comments for
             * {@link mCurrentViewport}.
             */
            float viewportOffsetX = distanceX * currentViewport.width() / contentRect.width();
            float viewportOffsetY = -distanceY * currentViewport.height() / contentRect.height();
            computeScrollSurfaceSize(surfaceSizeBuffer);
            int scrolledX = (int) (surfaceSizeBuffer.x
                    * (currentViewport.left + viewportOffsetX - AXIS_X_MIN)
                    / (AXIS_X_MAX - AXIS_X_MIN));
            boolean canScrollX = currentViewport.left > AXIS_X_MIN
                    || currentViewport.right < AXIS_X_MAX;
            setViewportBottomLeft(
                    currentViewport.left + viewportOffsetX,
                    currentViewport.bottom + viewportOffsetY);

            if (canScrollX && scrolledX < 0) {
                edgeEffectLeft.onPull(scrolledX / (float) contentRect.width());
                edgeEffectLeftActive = true;
            }

            if (canScrollX && scrolledX > surfaceSizeBuffer.x - contentRect.width()) {
                edgeEffectRight.onPull((scrolledX - surfaceSizeBuffer.x + contentRect.width())
                        / (float) contentRect.width());
                edgeEffectRightActive = true;
            }
            
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            fling((int) -velocityX, (int) -velocityY);
            return true;
        }
    };

    private void releaseEdgeEffects() {
        edgeEffectLeftActive
                = edgeEffectRightActive
                = false;
        edgeEffectLeft.onRelease();
        edgeEffectRight.onRelease();
    }

    private void fling(int velocityX, int velocityY) {
        releaseEdgeEffects();
        // Flings use math in pixels (as opposed to math based on the viewport).
        computeScrollSurfaceSize(surfaceSizeBuffer);
        scrollerStartViewport.set(currentViewport);
        int startX = (int) (surfaceSizeBuffer.x * (scrollerStartViewport.left - AXIS_X_MIN) / (
                AXIS_X_MAX - AXIS_X_MIN));
        int startY = (int) (surfaceSizeBuffer.y * (AXIS_Y_MAX - scrollerStartViewport.bottom) / (
                AXIS_Y_MAX - AXIS_Y_MIN));
        scroller.forceFinished(true);
        scroller.fling(
                startX,
                startY,
                velocityX,
                velocityY,
                0, surfaceSizeBuffer.x - contentRect.width(),
                0, surfaceSizeBuffer.y - contentRect.height(),
                contentRect.width() / 2,
                contentRect.height() / 2);
        
        ViewCompat.postInvalidateOnAnimation(this);
    }

    /**
     * Computes the current scrollable surface size, in pixels. For example, if the entire chart
     * area is visible, this is simply the current size of {@link #contentRect}. If the chart
     * is zoomed in 200% in both directions, the returned size will be twice as large horizontally
     * and vertically.
     */
    private void computeScrollSurfaceSize(Point out) {
        out.set(
                (int) (contentRect.width() * (AXIS_X_MAX - AXIS_X_MIN)
                        / currentViewport.width()),
                (int) (contentRect.height() * (AXIS_Y_MAX - AXIS_Y_MIN)
                        / currentViewport.height()));
    }

    @Override
    public void computeScroll() {
        super.computeScroll();

        if(!isInEditMode()){
        	boolean needsInvalidate = false;
        	
        	if (scroller.computeScrollOffset()) {
        		// The scroller isn't finished, meaning a fling or programmatic pan operation is
        		// currently active.
        		
        		computeScrollSurfaceSize(surfaceSizeBuffer);
        		int currX = scroller.getCurrX();
        		int currY = scroller.getCurrY();
        		
        		boolean canScrollX = (currentViewport.left > AXIS_X_MIN
        				|| currentViewport.right < AXIS_X_MAX);
        		
        		if (canScrollX
        				&& currX < 0
        				&& edgeEffectLeft.isFinished()
        				&& !edgeEffectLeftActive) {
        			edgeEffectLeft.onAbsorb((int) OverScrollerCompat.getCurrVelocity(scroller));
        			edgeEffectLeftActive = true;
        			needsInvalidate = true;
        		} else if (canScrollX
        				&& currX > (surfaceSizeBuffer.x - contentRect.width())
        				&& edgeEffectRight.isFinished()
        				&& !edgeEffectRightActive) {
        			edgeEffectRight.onAbsorb((int) OverScrollerCompat.getCurrVelocity(scroller));
        			edgeEffectRightActive = true;
        			needsInvalidate = true;
        		}
        		
        		float currXRange = AXIS_X_MIN + (AXIS_X_MAX - AXIS_X_MIN)
        				* currX / surfaceSizeBuffer.x;
        		float currYRange = AXIS_Y_MAX - (AXIS_Y_MAX - AXIS_Y_MIN)
        				* currY / surfaceSizeBuffer.y;
        		setViewportBottomLeft(currXRange, currYRange);
        	}
        	
        	if (zoomer.computeZoom()) {
        		// Performs the zoom since a zoom is in progress (either programatically or via double-touch).
        		float newWidth;
        		if(isHorizontalInteractionEnabled()){
        			newWidth = (1f - zoomer.getCurrZoom()) * scrollerStartViewport.width();            	
        		}else{
        			newWidth = scrollerStartViewport.width();
        		}
        		
        		float newHeight;
        		if (isVerticalInteractionEnabled()) {
        			newHeight = (1f - zoomer.getCurrZoom()) * scrollerStartViewport.height();
        		} else {
        			// Disable vertical zoom
        			newHeight = scrollerStartViewport.height();
        		}
        		
        		float pointWithinViewportX = (zoomFocalPoint.x - scrollerStartViewport.left)
        				/ scrollerStartViewport.width();
        		float pointWithinViewportY = (zoomFocalPoint.y - scrollerStartViewport.top)
        				/ scrollerStartViewport.height();
        		currentViewport.set(
        				zoomFocalPoint.x - newWidth * pointWithinViewportX,
        				zoomFocalPoint.y - newHeight * pointWithinViewportY,
        				zoomFocalPoint.x + newWidth * (1 - pointWithinViewportX),
        				zoomFocalPoint.y + newHeight * (1 - pointWithinViewportY));
        		constrainViewport();
        		needsInvalidate = true;
        	}
        	
        	if (needsInvalidate) {
        		ViewCompat.postInvalidateOnAnimation(this);
        	}
        	
        }
    }

    /**
     * Sets the current viewport (defined by {@link #currentViewport}) to the given
     * X and Y positions. Note that the Y value represents the topmost pixel position, and thus
     * the bottom of the {@link #currentViewport} rectangle. For more details on why top and
     * bottom are flipped, see {@link #currentViewport}.
     */
    private void setViewportBottomLeft(float x, float y) {
        /**
         * Constrains within the scroll range. The scroll range is simply the viewport extremes
         * (AXIS_X_MAX, etc.) minus the viewport size. For example, if the extrema were 0 and 10,
         * and the viewport size was 2, the scroll range would be 0 to 8.
         */

        float curWidth = currentViewport.width();
        float curHeight = currentViewport.height();
        float validatedX = Math.max(AXIS_X_MIN, Math.min(x, AXIS_X_MAX - curWidth));
        float validatedY = Math.max(AXIS_Y_MIN + curHeight, Math.min(y, AXIS_Y_MAX));

        currentViewport.set(validatedX, validatedY - curHeight, validatedX + curWidth, validatedY);
        ViewCompat.postInvalidateOnAnimation(this);
    }
    
	/**
	 * Returns the percent to which we can zoom this view.
	 */
	public int getMinimumZoomPercent() {
		return minZoomPercent;
	}

	/**
	 * Sets the percent to which we can zoom this view in.
	 * 
	 * @param percent
	 *            must be with value [0, 100]
	 * @throws IllegalArgumentException
	 *             if value percent is not valid
	 */
	public void setMinimumZoomInPercent(int percent) {
		if (percent < 0 || percent > 100) {
			throw new IllegalArgumentException(
					TAG + "#setMinimumZoomPercent - percent must be with value [0, 100].");
		}
		minZoomPercent = percent;
	}

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //     Methods for programmatically changing the viewport
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the current viewport (visible extremes for the chart domain and range.)
     */
    public RectF getCurrentViewport() {
        return new RectF(currentViewport);
    }

    /**
     * Sets the chart's current viewport.
     *
     * @see #getCurrentViewport()
     */
    public void setCurrentViewport(RectF viewport) {
        currentViewport = viewport;
        constrainViewport();
        ViewCompat.postInvalidateOnAnimation(this);
    }

    /**
     * Smoothly zooms the chart in one step.
     */
    public void zoomIn() {
    	if(isZoomedInMax()) {
    		return;
    	}
    	
        scrollerStartViewport.set(currentViewport);
        zoomer.forceFinished(true);
        zoomer.startZoom(ZOOM_AMOUNT);
        zoomFocalPoint.set((currentViewport.right + currentViewport.left) / 2, (currentViewport.bottom + currentViewport.top) / 2);
        ViewCompat.postInvalidateOnAnimation(this);
    }

    /**
     * Smoothly zooms the chart out one step.
     */
    public void zoomOut() {
        scrollerStartViewport.set(currentViewport);
        zoomer.forceFinished(true);
        zoomer.startZoom(-ZOOM_AMOUNT);
        zoomFocalPoint.set((currentViewport.right + currentViewport.left) / 2, (currentViewport.bottom + currentViewport.top) / 2);
        ViewCompat.postInvalidateOnAnimation(this);
    }

	public boolean isZoomedIn() {
		if (currentViewport.width() < VIEWPORT_MAX_WIDTH || currentViewport.height() < VIEWPORT_MAX_HEIGHT) {
			return true;
		}

		return false;
	}

	/**
	 * Clears the zoom if the view is zoomed without animation.
	 */
	public void removeZoom() {
		currentViewport.set(AXIS_X_MIN, AXIS_Y_MIN, AXIS_X_MAX, AXIS_Y_MAX);
		invalidate();
	}

	public boolean isZoomedInMax() {
		return currentViewport.width() <= (VIEWPORT_MAX_WIDTH * minZoomPercent / 100);
	}

	public boolean isScrolling() {
		return !scroller.isFinished();
	}

	/**
	 * Smoothly scrolls the chart left or right.
	 * 
	 * @param dx
	 *            Horizontal distance to travel. Positive numbers will scroll the content to the
	 *            left.
	 */
	public void scrollHorizontally(int dx) {
		scroller.computeScrollOffset();
		scroller.startScroll(scroller.getCurrX(), 0, dx, 0);
	}

    
	/**
	 * Smoothly pans the chart up one step if the vertical interaction is enabled.
	 * 
	 * @see #isVerticalInteractionEnabled()
	 * @see #setVerticalInteractionEnabled(boolean)
	 */
	public void panUp() {
		if (isVerticalInteractionEnabled()) {
			fling(0, (int) (-PAN_VELOCITY_FACTOR * getHeight()));
		}
	}

	/**
	 * Smoothly pans the chart down one step if the vertical interaction is enabled.
	 * 
	 * @see #isVerticalInteractionEnabled()
	 * @see #setVerticalInteractionEnabled(boolean)
	 */
	public void panDown() {
		if (isVerticalInteractionEnabled()) {
			fling(0, (int) (PAN_VELOCITY_FACTOR * getHeight()));
		}
	}
	
    /**
     * Smoothly pans the chart left one step.
     * 
     * @see #isHorizontalInteractionEnabled()
	 * @see #setHorizontalInteractionEnabled(boolean)
     */
    public void panLeft() {
    	if(isHorizontalInteractionEnabled()){
    		fling((int) (-PAN_VELOCITY_FACTOR * getWidth()), 0);    		
    	}
    }

    /**
     * Smoothly pans the chart right one step.
     * 
     * @see #isHorizontalInteractionEnabled()
	 * @see #setHorizontalInteractionEnabled(boolean)
     */
    public void panRight() {
    	if(isHorizontalInteractionEnabled()){
    		fling((int) (PAN_VELOCITY_FACTOR * getWidth()), 0);    		
    	}
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //     Methods and classes related to view state persistence.
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public Parcelable onSaveInstanceState() {
		Parcelable superState = super.onSaveInstanceState();
		SavedState ss = new SavedState(superState);
		ss.viewport = currentViewport;
		return ss;
	}

	@Override
	public void onRestoreInstanceState(Parcelable state) {
		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}

		SavedState ss = (SavedState) state;
		super.onRestoreInstanceState(ss.getSuperState());

		currentViewport = ss.viewport;
	}

    public boolean isHorizontalInteractionEnabled() {
		return isHorizontalInteractionEnabled;
	}

	public void setHorizontalInteractionEnabled(
			boolean isHorizontalInteractionEnabled) {
		this.isHorizontalInteractionEnabled = isHorizontalInteractionEnabled;
	}

	/**
     * Persistent state that is saved by InteractiveChartView.
     */
    public static class SavedState extends BaseSavedState {
        private RectF viewport;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeFloat(viewport.left);
            out.writeFloat(viewport.top);
            out.writeFloat(viewport.right);
            out.writeFloat(viewport.bottom);
        }

        @Override
        public String toString() {
            return "InteractiveChartView.SavedState{"
                    + Integer.toHexString(System.identityHashCode(this))
                    + " viewport=" + viewport.toString() + "}";
        }

		public static final Parcelable.Creator<SavedState> CREATOR
                = ParcelableCompat.newCreator(new ParcelableCompatCreatorCallbacks<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel in, ClassLoader loader) {
                return new SavedState(in);
            }

            @Override
            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        });

        SavedState(Parcel in) {
            super(in);
            viewport = new RectF(in.readFloat(), in.readFloat(), in.readFloat(), in.readFloat());
        }
    }
    
}
