package com.anyoption.android.app.event;

import com.anyoption.common.beans.base.MarketAssetIndex;

/**
 * @author Anastas Arnaudov
 */
public class AssetMarketInfoEvent {

    private MarketAssetIndex marketAssetIndex;

    public AssetMarketInfoEvent(MarketAssetIndex marketAssetIndex){
        this.marketAssetIndex = marketAssetIndex;
    }

    public MarketAssetIndex getMarketAssetIndex() {
        return marketAssetIndex;
    }
}
