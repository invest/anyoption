package com.anyoption.android.app.widget.form;

import java.util.List;

import com.anyoption.android.app.Application.LayoutDirection;
import com.anyoption.android.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FormStringSelector extends FormSelector<String> {

	private static final String DEFAULT_NOT_SELECTED_TEXT = "";

	private List<String> stringList;

	private View view;
	private LinearLayout rootLayout;
	private TextView text;
	private ImageView dropDownImage;

	private int selectedIndex = -1;
	private String notSelectedText;

	public FormStringSelector(Context context) {
		super(context);
	}

	public FormStringSelector(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public FormStringSelector(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void setupUI() {
		if (view == null) {
			view = View.inflate(getContext(), R.layout.string_selector, this);
			rootLayout = (LinearLayout) view.findViewById(R.id.form_string_selector_root_layout);
			text = (TextView) findViewById(R.id.form_string_selector_text);
			dropDownImage = (ImageView) findViewById(R.id.form_string_selector_dropdown_image);
		}
		
		if(!isInEditMode()){
			setupUIForDirection();
			// The default state will be empty
			notSelectedText = DEFAULT_NOT_SELECTED_TEXT;
			text.setText(notSelectedText);			
		}else{
			text.setText("Selector");
		}
	}

	@Override
	public void setupUIForDirection() {
		rootLayout.removeAllViews();
		if (getDirection() == LayoutDirection.LEFT_TO_RIGHT) {
			rootLayout.addView(text);
			rootLayout.addView(dropDownImage);
			text.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		} else if (getDirection() == LayoutDirection.RIGHT_TO_LEFT) {
			rootLayout.addView(dropDownImage);
			rootLayout.addView(text);
			text.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
		}
	}

	@Override
	public void setItems(List<String> items, int defaultSelectedItem,ItemListener<String> onItemSelectedListener) {
		stringList = items;
		super.setItems(items, defaultSelectedItem, onItemSelectedListener);
	}

	public List<String> getStringList() {
		return stringList;
	}

	@Override
	public void refreshUI() {
		super.refreshUI();

		text.setText(getValue());
	}

	// TODO Bad way to change the text!
	public void showText(String spinnerText) {
		text.setText(spinnerText);
	}

	public String getNotSelectedText() {
		return notSelectedText;
	}

	public void setNotSelectedText(String notSelectedText) {
		if (selectedIndex < 0) {
			text.setText(notSelectedText);
		}

		this.notSelectedText = notSelectedText;
	}

	@Override
	public void setFieldName(String fieldName) {
		
	}

	@Override
	public String getFieldName() {
		return null;
	}

	@Override
	public void displayError(String error) {
	
	}

}
