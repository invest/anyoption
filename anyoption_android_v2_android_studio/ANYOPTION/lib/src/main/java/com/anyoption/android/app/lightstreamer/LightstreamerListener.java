package com.anyoption.android.app.lightstreamer;

import com.lightstreamer.ls_client.UpdateInfo;

public interface LightstreamerListener {
	/**
	 * Called when there is a LIGHSTREAMER update.
	 * <p><b>IMPORTANT:</b> The LIGHSTREAMER invokes this method in a separate Thread. Any UI updates must be made in the Main Thread.
	 * @param update
	 */
    public void updateItem(int itemPos, String itemName, UpdateInfo update);
}