package com.anyoption.android.app.fragment.login_register;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.enums.CountryStatus;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.json.results.CountryIdMethodResult;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.MailTo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class TermsAndConditionsFragment extends NavigationalFragment {

    protected WebView webView;
    private long countryId;
	private View loadingView;

    @Override
    protected Screenable setupScreen() {
        return Screen.TERMS_AND_CONDITIONS;
    }

    /**
     * Use this method to get new instance of the fragment.
     * 
     * @return
     */
    public static TermsAndConditionsFragment newInstance(Bundle bundle) {
        TermsAndConditionsFragment termsAndConditionsFragment = new TermsAndConditionsFragment();
        termsAndConditionsFragment.setArguments(bundle);
        return termsAndConditionsFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            countryId = bundle.getLong(Constants.EXTRA_COUNTRY_ID);
        }

        if(countryId == 0){
            getCountryId();
        }
    }

    
	@SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.terms_and_conditions_layout, container, false);
        loadingView = rootView.findViewById(R.id.loading_view);
        setupLoading(true);
        
        webView = (WebView) rootView.findViewById(R.id.webViewTermsAndConditions);
        webView.getSettings().setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT <= 18) {
            webView.getSettings().setPluginState(PluginState.ON);
        }
        Log.d("URLAAA", "Call url wiht:" + countryId);
        webView.setWebViewClient(new MyWebViewClient());

        if(countryId != 0){
            loadURL();
        }

        return rootView;
    }

    protected void loadURL(){
        webView.loadUrl(getTermsUrl(application.getSkinId(), countryId));
    }


	@SuppressWarnings("static-method")
	protected String getTermsUrl(long skinId, long currCountryId) {
		return Application.get().getWebSiteLink() + "/anyoption_agreement.jsf?fromApp=true&s="
				+ skinId + "&cid=" + currCountryId + "&version=2-mobile";
	}

	private void setupLoading(boolean isLoading) {
		if (loadingView != null) {
			if (isLoading) {
				loadingView.setVisibility(View.VISIBLE);
			} else {
				loadingView.setVisibility(View.GONE);
			}
		}
	}
	
	private class MyWebViewClient extends WebViewClient {

        private String[] fileTypesArray = new String[]{ ".doc", ".docx", ".log", ".msg", ".odt",
                                                        ".pages", ".rtf", ".tex", ".txt", ".wpd",
                                                        ".wps", ".indd", ".pct", ".pdf", ".xlr",
                                                        ".xls", "xlsx"};

		@Override
		public void onPageFinished(android.webkit.WebView view, String url) {
			super.onPageFinished(view, url);
			setupLoading(false);
		}

		@Override
		public boolean shouldOverrideUrlLoading(android.webkit.WebView view, String url) {
            //Check if the link is an email:
            if (url.startsWith("mailto:")) {
                startActivity(new Intent(Intent.ACTION_SENDTO, Uri.parse(url)));
            }
            //Check if the link is to a document file:
            else if(isDocumentFile(url)){
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            }
            else{
			    view.loadUrl(url);
            }
			return true;
		}

        private boolean isDocumentFile(String url){
            String urlLowerCase = url.toLowerCase();
            for(int i=0; i<fileTypesArray.length; i++){
                String type = fileTypesArray[i];
                if(urlLowerCase.endsWith(type)){
                    return true;
                }
            }
            return false;
        }
	}

    ///////////////////////////////////////////////////////////////////////////
    /////////////               Requsets and Callbacks              ///////////
    ///////////////////////////////////////////////////////////////////////////

    protected void getCountryId() {
        Application.get().getCommunicationManager().requestService(TermsAndConditionsFragment.this, "getCountryIdCallBack", Constants.SERVICE_GET_COUNTRY_ID, new MethodRequest() , CountryIdMethodResult.class, false, false);
    }

    public void getCountryIdCallBack(Object result) {
        Log.d(TAG, "getCountryIdCallBack");
        CountryIdMethodResult countryIdMethodResult = (CountryIdMethodResult) result;
        if(countryIdMethodResult != null && countryIdMethodResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS){
            countryId = countryIdMethodResult.getCountryId();
            if(webView != null){
                loadURL();
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
}
