package com.anyoption.android.app.popup;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.anyoption.android.app.Application;
import com.anyoption.android.R;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;

/**
 * Custom Date Picker Dialog.
 * @author Anastas Arnaudov
 *
 */
public class DatePickerDialogFragment extends DialogFragment{

	/**
	 * Implement this to listen for Date set events.
	 * @author Anastas Arnaudov
	 *
	 */
	public interface OnDateSetListener{
		/**
		 * Called when the Date is set.
		 * @param calendar
		 */
		public void onDateSet(Calendar calendar);
	}
	
	private Calendar calendar;
	private DatePicker datePicker;
	private List<OnDateSetListener> listeners = new ArrayList<DatePickerDialogFragment.OnDateSetListener>();
	private boolean isMonthAndYearOnly;
	private boolean isMin18;

	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static DatePickerDialogFragment newInstance(Bundle bundle){
		DatePickerDialogFragment datePickerFragment = new DatePickerDialogFragment();	
		datePickerFragment.setArguments(bundle);
		return datePickerFragment;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Serializable extraCalendar = getArguments().getSerializable(Constants.EXTRA_CALENDAR);
		if(calendar != null){
			//Do nothing and use the set calendar
		}else if(extraCalendar != null && extraCalendar instanceof Calendar){
			calendar = (Calendar) extraCalendar;
		}else{
			calendar = Calendar.getInstance();
		}
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		setupDatePicker();
		
		AlertDialog.Builder builder = new AlertDialog.Builder(Application.get().getCurrentActivity());
		builder.setTitle(R.string.setDate);
		builder.setPositiveButton(R.string.set, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				calendar.set(Calendar.YEAR, datePicker.getYear());
				calendar.set(Calendar.MONTH, datePicker.getMonth());
				calendar.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
				
				for(OnDateSetListener listener : listeners){
					listener.onDateSet(calendar);
					dismiss();
				}
			}
		});
		
		builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// Just cancel
			}
		});
		builder.setView(datePicker);
		
		if (isMonthAndYearOnly) {
			showMonthAndYearOnly();
		}

		AlertDialog dialog = builder.create();
		return dialog;
	}
	
	private void setupDatePicker() {
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		
		datePicker = new DatePicker(Application.get().getCurrentActivity());			

		if(isMin18()){
			Calendar past18Date = Calendar.getInstance();
			past18Date.add(Calendar.YEAR, -18);
			long date = TimeUtils.get18YearsAgoTimestamp(); 
			datePicker.setMaxDate(date);
		}else{
			datePicker.init(year, month, day, null);			
		}
		datePicker.setCalendarViewShown(false);
	}

	public Calendar getDate(){
		return calendar;
	}
	
	public void setDate(Calendar calendar){
		this.calendar = calendar;
		setupDatePicker();
	}
	
	/**
	 * Adds {@link OnDateSetListener}.
	 * @param onDateSetListener
	 */
	public void addOnDateSetListener(OnDateSetListener onDateSetListener){
		this.listeners.add(onDateSetListener);
	}

	/**
	 * Removes the {@link OnDateSetListener}.
	 * @param onDateSetListener
	 */
	public void removeOnDateSetListener(OnDateSetListener onDateSetListener){
		this.listeners.remove(onDateSetListener);
	}
	
	/**
	 * show month and year only.
	 */
	public void showMonthAndYearOnly() {
		if(Utils.getDeviceOSVersionCode() >= Build.VERSION_CODES.LOLLIPOP){
			Field f[] = datePicker.getClass().getDeclaredFields();
			for (Field field : f) {
				if (field.getName().equals("mDelegate")) {
					field.setAccessible(true);
					Object delegate = new Object();
					try {
						delegate = field.get(datePicker);
						Field f2[] = delegate.getClass().getDeclaredFields();
						for (Field field2 : f2) {
							if (field2.getName().equals("mDaySpinner")) {
								field2.setAccessible(true);
								Object dayPicker = new Object();
								dayPicker = field2.get(delegate);
								((View) dayPicker).setVisibility(View.GONE);
							}
						}
					} catch (Exception e) {
						Log.d("ERROR", e.getMessage());
					}
				}
			}
		}else{
			Field f[] = datePicker.getClass().getDeclaredFields();
			for (Field field : f) {
				if (field.getName().equals("mDaySpinner")) {
					field.setAccessible(true);
					Object dayPicker = new Object();
					try {
						dayPicker = field.get(datePicker);
						((View) dayPicker).setVisibility(View.GONE);
					} catch (Exception e) {
						Log.d("ERROR", e.getMessage());
					}
				}
			}
		}
	}

	/**
	 * @return the isMonthAndYearOnly
	 */
	public boolean isMonthAndYearOnly() {
		return isMonthAndYearOnly;
	}

	/**
	 * @param isMonthAndYearOnly the isMonthAndYearOnly to set
	 */
	public void setMonthAndYearOnly(boolean isMonthAndYearOnly) {
		this.isMonthAndYearOnly = isMonthAndYearOnly;
	}

	public boolean isMin18() {
		return isMin18;
	}

	public void setMin18(boolean isMin18) {
		this.isMin18 = isMin18;
	}
	
}
