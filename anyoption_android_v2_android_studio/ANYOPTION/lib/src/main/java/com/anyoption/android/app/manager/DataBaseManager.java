package com.anyoption.android.app.manager;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.model.MarketDB;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.User;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Data Base Manager that manages the local data base of the application.
 * The manager provides utility methods for interacting with the DB.
 * The class copies the database file stored in the assets folder to the local database :
 * this is useful because on change in the database structure before new release we just paste the db file in the assets folder).
 *
 * @author Anastas Arnaudov
 *
 */
public class DataBaseManager extends SQLiteOpenHelper{

	public static final String TAG = DataBaseManager.class.getSimpleName();
	private static final String DB_ASSETS_DIR = "database/";
	public static String DB_PATH;
	private static String DB_NAME = "anyoptionDB";

	private Application application;
	private SQLiteDatabase dataBase;

	//#############################
	//###		Markets	Table	###
	//#############################
	private static final String MARKETS_TABLE 			= "markets";
	private static final String MARKETS_ID 				= "id";
	private static final String MARKETS_NAME 			= "name";
	private static final String MARKETS_GROUP_ID 		= "group_id";
	private static final String MARKETS_GROUP_NAME 		= "group_name";
	private static final String MARKETS_IS_OPTION_PLUS 	= "is_option_plus";
	private static final String MARKETS_STATE 			= "state";
	public static final String MARKETS_STATE_FAVORITE 	= "favorite";
	public static final String MARKETS_STATE_DELETED 	= "deleted";
	private static final String MARKETS_STATE_NONE 		= "none";
	private static final String MARKETS_USER_ID 		= "user_id";
	private static final String MARKETS_DECIMAL_POINTS 	= "decimal_points";
	private static final String MARKETS_PRODUCT_TYPE_ID = "product_type_id";
	//#############################


	public DataBaseManager(Application application){
		super(application, DB_NAME, null, 1);
		Log.d(TAG, "On Create");
		this.application = application;
	}

    /**
     * Creates an empty database on the system and rewrites it with the database stored in the assets.
     */
    public void createDataBase() throws IOException {
     	boolean dbExist = checkDataBase();
     	SharedPreferencesManager sharedPreferencesManager = application.getSharedPreferencesManager();
     	int versionCode = application.getApplicationVersionCode();
     	int SPversionCode = sharedPreferencesManager.getInt(Constants.PREFERENCES_APPLICATION_VERSION, 0);
     	if (dbExist && versionCode == SPversionCode) {
    		//Do nothing - database already exist
    	} else {
    		if (dbExist) {
    			Log.d(TAG, "Got new version deleting existing db");
    			deleteDB();
    		}
        	getReadableDatabase();
        	try {
     			copyDataBase();
     		} catch (IOException e) {
         		throw new Error("Error copying database");
         	}
    	}

     	sharedPreferencesManager.putInt(Constants.PREFERENCES_APPLICATION_VERSION, versionCode);
     }

    /**
     * Check if the database already exists to avoid re-copying the file each time you open the application.
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase() {
    	SQLiteDatabase checkDB = null;
    	try {
    		String myPath = DB_PATH + DB_NAME;
    		checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
    	} catch(SQLiteException e) {
    		Log.e(TAG, "DataBase does not exist yet!");
    	}catch (Exception e) {
    		Log.e(TAG, "Error in checkDataBase!");
		}
    	if (checkDB != null) {
    		checkDB.close();
    	}
    	return checkDB != null ? true : false;
    }

    /**
     * Copies the database from the local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transfering bytestream.
     * */
    private void copyDataBase() throws IOException {
    	//Open your local db as the input stream
    	InputStream myInput = application.getAssets().open(DB_ASSETS_DIR + DB_NAME);
    	// Path to the just created empty db
    	String outFileName = DB_PATH + DB_NAME;
    	//Open the empty db as the output stream
    	OutputStream myOutput = new FileOutputStream(outFileName);
    	//transfer bytes from the inputfile to the outputfile
    	byte[] buffer = new byte[1024];
    	int length;
    	while ((length = myInput.read(buffer))>0){
    		myOutput.write(buffer, 0, length);
    	}
    	//Close the streams
    	myOutput.flush();
    	myOutput.close();
    	myInput.close();
    }

    /**
     * Opens the Database in readable mode.
     * @throws SQLException
     */
    public void openReadableDB() throws SQLException {
    	dataBase = SQLiteDatabase.openDatabase(DB_PATH + DB_NAME, null, SQLiteDatabase.OPEN_READONLY);
    }

    /**
     * Opens the Database in writable mode.
     * @throws SQLException
     */
	private void openWritableDB() throws SQLException {
    	dataBase = SQLiteDatabase.openDatabase(DB_PATH + DB_NAME, null, SQLiteDatabase.OPEN_READWRITE);
    }

    /**
     * Closes the Database.
     */
    @Override
	public synchronized void close() {
		if (dataBase != null) {
			dataBase.close();
		}
    	super.close();
	}

	public void onCreate(SQLiteDatabase db) {
	}

	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}

	public Cursor getSkins() {
		return dataBase.query("skins",
				new String[] {"skin_id",
					"display_name",
					"defaultCountryId",
					"defaultLanguageId",
					"defaultCurrencyId",
					"SUPPORT_START_TIME",
					"SUPPORT_END_TIME",
					"skin_group_id",
					"locale",
					"IS_REGULATED"},
				null,
				null,
				null,
				null,
				null);
	}

	public Cursor getSupportPhone(long skinId) {
		String sql = "SELECT " +
	            " support_phone from skins s, countries co "+
                " WHERE " +
                " s.defaultCountryId = co.id "+
                " and s.skin_id = ?";

		return dataBase.rawQuery(sql, new String[] {String.valueOf(skinId)});
	}

	public Cursor getCorrespondingNotRegulatedSkinId(long regulatedSkinId) {
		String sql = "SELECT " +
	            " not_regulated_skin_id from skins "+
                " WHERE " +
                " skin_id = ?";

		return dataBase.rawQuery(sql, new String[] {String.valueOf(regulatedSkinId)});
	}

	/**
	 * Saves or updates the given market into the Database.
	 * @param market The market to be saved or updated.
	 * @param state Should be {@link #MARKETS_STATE_FAVORITE} , {@link #MARKETS_STATE_DELETED} or {@link #MARKETS_STATE_NONE}.
	 * @return True if the transaction went OK. False otherwise.
	 */
	public boolean saveOrUpdateMarket(Market market, String state){
		boolean isSuccessful = false;
		User user = application.getUser();

		if(user != null){
			long insertOrUpdateResult = -1;

			try {
				openWritableDB();

				ContentValues values = new ContentValues();
				values.put(MARKETS_ID, market.getId());
				values.put(MARKETS_NAME, market.getDisplayName());
				values.put(MARKETS_IS_OPTION_PLUS, market.isOptionPlusMarket());
				values.put(MARKETS_STATE, state);
				values.put(MARKETS_GROUP_ID, market.getGroupId());
				values.put(MARKETS_GROUP_NAME, market.getGroupName());
				values.put(MARKETS_USER_ID, user.getId());
				values.put(MARKETS_DECIMAL_POINTS, market.getDecimalPoint());
				values.put(MARKETS_PRODUCT_TYPE_ID, market.getProductTypeId());

				insertOrUpdateResult = dataBase.insertWithOnConflict(MARKETS_TABLE, null, values, SQLiteDatabase.CONFLICT_REPLACE);

				isSuccessful = (insertOrUpdateResult >= 0) ? true : false;

			} catch (Exception e) {
				Log.e(TAG, "Error on saveOrUpdateMarket", e);
			}finally{
				close();
			}
		}

		return isSuccessful;
	}

	/**
	 * Returns the saved Markets for this user from the DB.
	 * @param user
	 * @return
	 */
	public List<MarketDB> getMarkets(User user){
		List<MarketDB> markets = new ArrayList<MarketDB>();
		if(user != null){
			openReadableDB();
			Cursor cursor = dataBase.query(MARKETS_TABLE, null, MARKETS_USER_ID + " = " + "\'" + user.getId() + "\'", null,null,null,null);
			while (cursor.moveToNext()) {
				Market market = new Market();
				market.setId(cursor.getLong(cursor.getColumnIndex(MARKETS_ID)));
				market.setDisplayName(cursor.getString(cursor.getColumnIndex(MARKETS_NAME)));
				market.setGroupId(cursor.getLong(cursor.getColumnIndex(MARKETS_GROUP_ID)));
				market.setGroupName(cursor.getString(cursor.getColumnIndex(MARKETS_GROUP_NAME)));
				market.setOptionPlusMarket(cursor.getInt(cursor.getColumnIndex(MARKETS_IS_OPTION_PLUS)) != 0);
				market.setDecimalPoint(cursor.getLong(cursor.getColumnIndex(MARKETS_DECIMAL_POINTS)));
				market.setProductTypeId(cursor.getLong(cursor.getColumnIndex(MARKETS_PRODUCT_TYPE_ID)));

				MarketDB marketDB = new MarketDB(market);
				marketDB.setState(cursor.getString(cursor.getColumnIndex(MARKETS_STATE)));
				marketDB.setUserID(user.getId());

				markets.add(marketDB);
			}
			close();
		}
		return markets;
	}

	/**
	 * Returns the saved Favorites Markets for this user from the DB.
	 * @param user
	 * @return
	 */
	public List<Market> getFavoritesMarkets(User user){
		List<Market> markets = new ArrayList<Market>();
		if(user != null){
			openReadableDB();
			Cursor cursor = dataBase.query(	MARKETS_TABLE,
											null,
											MARKETS_USER_ID + " = " + "\'" + user.getId() + "\'" + " AND " +
											MARKETS_STATE + " = " + "\'" + MARKETS_STATE_FAVORITE + "\'",
											null,null,null,null);
			while (cursor.moveToNext()) {
				Market market = new Market();
				market.setId(cursor.getLong(cursor.getColumnIndex(MARKETS_ID)));
				market.setDisplayName(cursor.getString(cursor.getColumnIndex(MARKETS_NAME)));
				market.setGroupId(cursor.getLong(cursor.getColumnIndex(MARKETS_GROUP_ID)));
				market.setGroupName(cursor.getString(cursor.getColumnIndex(MARKETS_GROUP_NAME)));
				market.setOptionPlusMarket(cursor.getInt(cursor.getColumnIndex(MARKETS_IS_OPTION_PLUS)) != 0);
				market.setDecimalPoint(cursor.getLong(cursor.getColumnIndex(MARKETS_DECIMAL_POINTS)));
				market.setProductTypeId(cursor.getLong(cursor.getColumnIndex(MARKETS_PRODUCT_TYPE_ID)));

				markets.add(market);
			}
			close();
		}
		return markets;
	}

	/**
	 * Checks whether the Market is one of the users favorites.
	 * @param market
	 * @param user
	 * @return
	 */
	public boolean isMarketFavorite(User user, Market market){
		boolean result = false;
		if(user != null){
			openReadableDB();
			Cursor cursor = dataBase.query(	MARKETS_TABLE,
											null,
											MARKETS_USER_ID + " = " + "\'" + user.getId() + "\'" + " AND " +
											MARKETS_ID + " = " + "\'" + market.getId() + "\'" + " AND " +
											MARKETS_STATE + " = " + "\'" + MARKETS_STATE_FAVORITE + "\'",
											null,null,null,null);
			result = cursor.moveToNext();

			close();
		}
		return result;
	}

	/**
	 * Deletes the DataBase from the device.
	 */
	private void deleteDB() {
		application.deleteDatabase(DB_NAME);
	}

}
