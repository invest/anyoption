package com.anyoption.android.app.fragment.my_account.questionnaire;

import java.util.Arrays;
import java.util.List;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.R;
import com.anyoption.android.app.manager.PopUpManager.OnAlertPopUpButtonPressedListener;
import com.anyoption.android.app.model.questionnaire.BaseQuestionItem;
import com.anyoption.android.app.model.questionnaire.MultipleChoiceQuestion;
import com.anyoption.android.app.model.questionnaire.SpinnerQuestion;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.results.UserMethodResult;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class OptionalQuestionnaireFragment extends BaseQuestionnaireFragment {

	public static final String TAG = OptionalQuestionnaireFragment.class.getSimpleName();

	private static final long[] QUSTION_ORDER_IDs = new long[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };

	private static final int[] QUSTION_TEXT_IDs = new int[] {
			R.string.questionnaire_optional_question1, R.string.questionnaire_optional_question2,
			R.string.questionnaire_optional_question3, R.string.questionnaire_optional_question4,
			R.string.questionnaire_optional_question5, R.string.questionnaire_optional_question6,
			R.string.questionnaire_optional_question7, R.string.questionnaire_optional_question8,
			R.string.questionnaire_optional_question9, R.string.questionnaire_optional_question10,
			R.string.questionnaire_optional_question11
	};

	private static final int[] ANSWERS_ARRAY_IDs = new int[] {
			R.array.questionnaire_optional_answers1, R.array.questionnaire_optional_answers2,
			R.array.questionnaire_optional_answers3, R.array.questionnaire_optional_answers4,
			R.array.questionnaire_optional_answers5, R.array.questionnaire_optional_answers6,
			R.array.questionnaire_optional_answers7, R.array.questionnaire_optional_answers8,
			R.array.questionnaire_optional_answers9, R.array.questionnaire_optional_answers10,
			R.array.questionnaire_optional_answers11
	};

	private static final int MULTICHOICE_QUESTION_ORDER_ID = 6;

	public static OptionalQuestionnaireFragment newInstance() {
		return new OptionalQuestionnaireFragment();
	}

	public static OptionalQuestionnaireFragment newInstance(Bundle bundle) {
		OptionalQuestionnaireFragment fragment = new OptionalQuestionnaireFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View root = super.onCreateView(inflater, container, savedInstanceState);
		showNotNow();
		return root;
	}

	@Override
	protected String getTitle() {
		return getString(R.string.questionnaire_optional_subheader);
	}

	@Override
	protected String getCurrentUserAnswersMethod() {
		return Constants.SERVICE_GET_OPTIONAL_QUESTIONNAIRIE_ANSWERS;
	}

	@Override
	protected String getSubmitRequestMethod() {
		return Constants.SERVICE_UPDATE_OPTIONAL_QUESTIONNAIRIE;
	}

	@Override
	protected void initQuestions(List<BaseQuestionItem> questionsList) {
		BaseQuestionItem question = null;
		for (int i = 0; i < QUSTION_ORDER_IDs.length; i++) {
			if (QUSTION_ORDER_IDs[i] != MULTICHOICE_QUESTION_ORDER_ID) {
				question = new SpinnerQuestion(QUSTION_ORDER_IDs[i],
						getString(QUSTION_TEXT_IDs[i]), getListFromResource(ANSWERS_ARRAY_IDs[i]));
			} else {
				question = new MultipleChoiceQuestion(QUSTION_ORDER_IDs[i],
						getString(QUSTION_TEXT_IDs[i]), getListFromResource(ANSWERS_ARRAY_IDs[i]));
			}
			questionsList.add(question);

		}
	}

	@Override
	protected void getSubmitResultCallback(UserMethodResult result) {
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			if (result.getUserRegulation().isOptionalQuestionnaireDone()) {
				showThankYouPopup();
			}
		}
	}

	@Override
	protected Screenable setupScreen() {
		return Screen.OPTIONAL_QUESTIONNAIRE;
	}

	private List<String> getListFromResource(int arrayResId) {
		String[] stringArr = getResources().getStringArray(arrayResId);
		return Arrays.asList(stringArr);
	}

	private void showThankYouPopup() {
		application.getPopUpManager().showAlertPopUp(null,
				getString(R.string.questionnaire_optional_thankyou), getString(R.string.ok).toUpperCase(application.getLocale()),
				new OnAlertPopUpButtonPressedListener() {

					@Override
					public void onButtonPressed() {
						openHomeScreen();
					}
				});
	}

}
