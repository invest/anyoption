package com.anyoption.android.app.fragment.trade;

import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.ChartEvent;
import com.anyoption.android.app.event.InvestmentEvent;
import com.anyoption.android.app.event.InvestmentEvent.Type;
import com.anyoption.android.app.event.LightstreamerRegisterEvent;
import com.anyoption.android.app.event.LightstreamerUpdateEvent;
import com.anyoption.android.app.event.LongTermTradingTimeChosenEvent;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.manager.CommunicationManager;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.chart.ChartDrawable;
import com.anyoption.android.app.widget.chart.ChartView;
import com.anyoption.android.app.widget.chart.OnScrollToLeftEdgeListener;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.json.requests.ChartDataMethodRequest;
import com.anyoption.json.results.ChartDataMethodResult;
import com.lightstreamer.ls_client.UpdateInfo;

public class ChartFragment extends BaseAssetNestedFragment implements Callback {

    public static final String TAG = ChartFragment.class.getSimpleName();

    protected static final int FEATURED_MARKET_BOX = 0;

    public static final int MESSAGE_SHOW_ZOOM_BUTTONS = 1;
    public static final int MESSAGE_HIDE_ZOOM_BUTTONS = 2;
    public static final int MESSAGE_SHOW_CLOSING_LEVEL = 3;
    public static final int MESSAGE_HIDE_CLOSING_LEVEL = 4;
    public static final int MESSAGE_STATE_CHANGED = 5;
    public static final int MESSAGE_UPDATE_PROFIT = 6;

    protected CommunicationManager communicationManager;
    protected Handler handler;

    protected View root;
    protected ChartDrawable chartView;
    protected View zoomInView;
    protected View zoomOutView;
    protected View loadingView;

    protected View closingLevelLayout;
    protected TextView closingLevelView;
    protected View closingLevelBg;

    protected View longTermTradingLayout;

    protected int opportunityState;
    protected long opportunityId;
    protected boolean hasPreviousHour;
    protected boolean previousHourLoaded;
    protected boolean reloadMarket;

    protected SkinGroup skinGroup;

    protected boolean isChartRequested;

    private long nextOpportunityId = 0L;
    private LongTermTradingTimeChosenEvent.ChosenTime longTermTradingChosenTime = LongTermTradingTimeChosenEvent.ChosenTime.NEAREST;

    /**
     * Use this method to create new instance of the fragment.
     *
     * @return
     */
    public static ChartFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new ChartFragment.");
        ChartFragment fragment = new ChartFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public ChartFragment() {
        // Do nothing
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        reloadMarket = false;
        if (args != null) {
            reloadMarket = args.getBoolean(Constants.EXTRA_RELOAD_MARKET_KEY, reloadMarket);
        }

        communicationManager = application.getCommunicationManager();
        handler = new Handler(this);
        isChartRequested = false;
        longTermTradingChosenTime = LongTermTradingTimeChosenEvent.ChosenTime.NEAREST;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(getLayout(), container, false);

        RelativeLayout chartLayout = (RelativeLayout) root.findViewById(R.id.chartLayout);
        chartView = getChartView();
        chartLayout.addView((View) chartView, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        chartView.setOnScrollToLeftEdgeListener(new OnScrollToLeftEdgeListener() {
            @Override
            public boolean onScrollToLeftEdge() {
                if (hasPreviousHour && !previousHourLoaded) {
                    chartRequestPreviousHour();
                    return true;
                }

                return false;
            }
        });

        setupZoomButtons();

        //Initial visibility of the zoom buttons:
        showZoomButtons(false);


        loadingView = root.findViewById(R.id.chart_loading);
        closingLevelLayout = root.findViewById(R.id.chart_closing_level_layout);
        closingLevelView = (TextView) root.findViewById(R.id.chart_closing_level);
        closingLevelBg = root.findViewById(R.id.chart_closing_level_bg);

        longTermTradingLayout = root.findViewById(R.id.long_term_trading_layout);

        Skin skin = application.getSkins().get(application.getSkinId());
        skinGroup = skin.getSkinGroup();

        hasPreviousHour = false;
        previousHourLoaded = false;

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

        // Register for events
        application.registerForEvents(this, LightstreamerRegisterEvent.class, LightstreamerUpdateEvent.class, InvestmentEvent.class, LongTermTradingTimeChosenEvent.class);
    }

    @Override
    public void onPause() {
        application.unregisterForAllEvents(this);
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        application.unregisterForAllEvents(this);
        super.onDestroyView();
    }

    protected int getLayout() {
        return R.layout.chart_fragment;
    }

    protected ChartDrawable getChartView() {
        return new ChartView(getContext(), null, R.style.chart_chart_view);
    }

    protected void setupZoomButtons() {
        zoomInView = root.findViewById(R.id.zoom_in);
        zoomInView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                chartView.zoomIn();
            }
        });

        zoomOutView = root.findViewById(R.id.zoom_out);
        zoomOutView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (hasPreviousHour && !chartView.isZoomedIn() && !previousHourLoaded) {
                    chartRequestPreviousHour();
                    return;
                }

                chartView.zoomOut();
            }
        });
    }

    /**
     * Requests chart data
     */
    protected void chartRequest() {
        Log.d(TAG, "chartRequest");
        if (isChartRequested) {
            // Cancel all previous requests.
            communicationManager.cancelAllRequests(this);
        }

        ChartDataMethodRequest request = new ChartDataMethodRequest();

        if (market != null) {
            request.setMarketId(market.getId());
        } else {
            request.setBox(FEATURED_MARKET_BOX);
            request.setOpportunityTypeId(Opportunity.TYPE_REGULAR);
        }

        isChartRequested = communicationManager.requestService(this, "chartCallback", Constants.SERVICE_GET_CHART_DATA, request, ChartDataMethodResult.class, false, false);

        application.postEvent(new ChartEvent(market != null ? market.getId() : 0, ChartEvent.Type.DATA_REQUESTED));
    }

    protected void nextOpportunityChartRequest(long nextOpportunityId) {
        Log.d(TAG, "chartRequest");
        if (isChartRequested) {
            // Cancel all previous requests.
            communicationManager.cancelAllRequests(this);
        }

        ChartDataMethodRequest request = new ChartDataMethodRequest();
        request.setMarketId(market.getId());
        request.setOpportunityId(nextOpportunityId);

        isChartRequested = communicationManager.requestService(this, "chartCallback", Constants.SERVICE_GET_CHART_DATA, request, ChartDataMethodResult.class, false, false);

        application.postEvent(new ChartEvent(market != null ? market.getId() : 0, ChartEvent.Type.DATA_REQUESTED));
    }

    public void chartCallback(Object resultObj) {
        Log.d(TAG, "chartCallback " + resultObj);
        isChartRequested = false;
        if (!isAdded()) {
            return;
        }

        ChartDataMethodResult result = (ChartDataMethodResult) resultObj;
        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            // This is used for tablets. The first time application is opened is loaded first
            // featured market.
            if (market == null || reloadMarket) {
                loadFeaturedMarket(result);
            }

            longTermTradingLayout.setVisibility(View.GONE);

            // TODO Must be added methods for request cancel instead of this check. This problem
            // will occur for other requests.
            if (market.getId() != result.getMarketId()) {
                // On tablets if the market is changed before chart data for previous market is
                // received, will be received all chart data results one by one. This check will
                // prevent using the wrong chart data.
                Log.w(TAG, "chartCallback -> Data for OTHER MARKET!");
                return;
            }

            Log.d(TAG, "chartCallback Success -> market: " + market.getDisplayName()
                    + " resultMarket:" + result.getMarketName());

            TimeUtils.fixDates(result);
            hasPreviousHour = result.isHasPreviousHour();
            opportunityState = 0;

            // Update chart info here
            if (chartView != null) {
                chartView.clearData();
                chartView.setChartData(result);
                // The asset is unavailable.
                if (result.getTimeEstClosing() == null || result.getTimeLastInvest() == null) {
                    Message msg = handler.obtainMessage(MESSAGE_HIDE_ZOOM_BUTTONS);
                    msg.sendToTarget();
                } else {
                    Message msg = handler.obtainMessage(MESSAGE_SHOW_ZOOM_BUTTONS);
                    msg.sendToTarget();
                }

            }

            // The user object will be received if he is logged in.
            if (application.isLoggedIn() && result.getUser() != null) {
                application.setUser(result.getUser());
            }

            application.postEvent(new ChartEvent(market.getId(), ChartEvent.Type.DATA_REQUEST_SUCCESS, result));
        } else {
            if (resultObj != null) {
                Log.e(TAG, "chartCallback error -> ErrorCode -> " + result.getErrorCode());
            } else {
                Log.e(TAG, "chartCallback error -> result is NULL");
            }

            // It is possible the result to be null.
            application.postEvent(new ChartEvent(market != null ? market.getId() : 0, ChartEvent.Type.DATA_REQUEST_FAIL, result));
        }

    }

    private void chartRequestPreviousHour() {
        if (!previousHourLoaded) {
            loadingView.setVisibility(View.VISIBLE);
            previousHourLoaded = true;
            ChartDataMethodRequest request = new ChartDataMethodRequest();
            request.setMarketId(market.getId());
            request.setOpportunityId(opportunityId);
            application.getCommunicationManager().requestService(this,
                    "chartPreviousHourRequestCallback",
                    Constants.SERVICE_GET_CHART_DATA_PREVIOUS_HOUR, request,
                    ChartDataMethodResult.class);
        }
    }

    public void chartPreviousHourRequestCallback(Object resultObj) {
        if (!isAdded()) {
            return;
        }

        Log.d("chart", "chartDataPreviousHourCallBack");
        loadingView.setVisibility(View.GONE);

        ChartDataMethodResult result = (ChartDataMethodResult) resultObj;
        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS
                && result.getRatesTimes() != null
                && result.getRates() != null) {
            TimeUtils.fixDates(result);
            chartView.setChartDataPreviousHour(result);
        } else {
            previousHourLoaded = false;
        }
    }

    //////////////////////////////////////////////////////////////////////////////////
    //                              EVENTs                                          //
    //////////////////////////////////////////////////////////////////////////////////

    public void onEvent(LightstreamerRegisterEvent event) {
        opportunityId = 0L;
        opportunityState = 0;
    }

    public void onEvent(LightstreamerUpdateEvent event) {
        if (market != null && event.getMarketId() == market.getId()) {
            updateItem(event.getUpdateInfo());
        }
    }

    //////////////////////////////////////////////////////////////////////////////////



    protected void updateItem(UpdateInfo update) {

        if (opportunityId == 0L || update.isValueChanged(LightstreamerManager.COLUMN_ET_OPP_ID)) {
            opportunityId = Long.parseLong(update.getNewValue(LightstreamerManager.COLUMN_ET_OPP_ID));
        }

        int stateBeforeUpdate = opportunityState;

        if (update.isValueChanged(LightstreamerManager.COLUMN_ET_STATE) || opportunityState == 0L) {
            opportunityState = Integer.parseInt(update.getNewValue(LightstreamerManager.COLUMN_ET_STATE));
            if (opportunityState > stateBeforeUpdate) {
                Message msg = handler.obtainMessage(MESSAGE_STATE_CHANGED);
                msg.sendToTarget();
            }

            if (update.getOldValue(LightstreamerManager.COLUMN_ET_STATE) != null) {
                int oldState = Integer.parseInt(update.getOldValue(LightstreamerManager.COLUMN_ET_STATE));
                if (oldState == Opportunity.STATE_CREATED || oldState == Opportunity.STATE_PAUSED || oldState == Opportunity.STATE_SUSPENDED) {
                    chartRequest();
                    return;
                }
            }

            if (opportunityState == Opportunity.STATE_CREATED || opportunityState == Opportunity.STATE_SUSPENDED) {
                Message msg = handler.obtainMessage(MESSAGE_HIDE_ZOOM_BUTTONS);
                msg.sendToTarget();
            } else {
                Message msg = handler.obtainMessage(MESSAGE_SHOW_ZOOM_BUTTONS);
                msg.sendToTarget();
            }

            Message message = null;
            if (opportunityState == Opportunity.STATE_CLOSED) {
                message = handler.obtainMessage(MESSAGE_SHOW_CLOSING_LEVEL, update.getNewValue(skinGroup.getLevelUpdateKey()));
            } else {
                message = handler.obtainMessage(MESSAGE_HIDE_CLOSING_LEVEL);
            }
            message.sendToTarget();

            if (opportunityState == Opportunity.STATE_DONE) {
                chartRequest();
            }
        }

        if (opportunityState <= Opportunity.STATE_CLOSED || opportunityState != stateBeforeUpdate) {
            chartView.updateItem(update.getItemPos(), update.getItemName(), update);
        }
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case MESSAGE_SHOW_ZOOM_BUTTONS:
                showZoomButtons(true);
                break;
            case MESSAGE_HIDE_ZOOM_BUTTONS:
                showZoomButtons(false);
                break;
            case MESSAGE_SHOW_CLOSING_LEVEL:
                String level = (String) msg.obj;
                showClosingLevelInfo(true, level);
                break;
            case MESSAGE_HIDE_CLOSING_LEVEL:
                showClosingLevelInfo(false, null);
                break;
            default:
                break;
        }

        return true;
    }

    /**
     * Called when a {@link InvestmentEvent} occurs.
     *
     * @param event
     */
    public void onEventMainThread(InvestmentEvent event) {
        if (event.getMarketId() != market.getId()) {
            return;
        }

        if (event.getType() == Type.INVEST_SUCCESS) {
            chartView.addInvestment(event.getInvestment());
        } else if (event.getType() == Type.INVESTMENT_SOLD) {
            chartView.removeInvestment(event.getInvestment().getId());
        } else if (event.getType() == Type.INVESTMENT_SOLD_ALL) {
            chartView.removeInvestment(0);
        } else if (event.getType() == Type.INVESTMENT_CHANGE_FOCUS) {
            Investment investment = event.getInvestment();
            Investment focusedInvestment = chartView.getFocusedInvestment();

            if (focusedInvestment == null || focusedInvestment.getId() != investment.getId()) {
                chartView.requestInvestmentFocus(investment);
            } else {
                // Remove the focus
                chartView.requestInvestmentFocus(null);
            }
        }
    }


    public void onEventMainThread(LongTermTradingTimeChosenEvent event) {
        if(market.getId() != event.getMarketId()){
            return;
        }
        if(longTermTradingChosenTime != event.getTime()) {
            longTermTradingChosenTime = event.getTime();
            nextOpportunityId = event.getOpportunityId();

            if (longTermTradingChosenTime != LongTermTradingTimeChosenEvent.ChosenTime.NEAREST && longTermTradingChosenTime != LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_1 && longTermTradingChosenTime != LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_2 && longTermTradingChosenTime != LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_3) {
                showLongTermTradingInfo(true);
            } else {
                showLongTermTradingInfo(false);
                loadData();
            }
        }
    }

    /**
     * Load the chart data. Use this method if you've set autoLoad to false or you want to reload
     * the data.
     */
    @Override
    public void loadData() {
        if(longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.NEAREST) {
            chartRequest();
        } else if(longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_1) {
            nextOpportunityChartRequest(nextOpportunityId);
        } else if(longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_2) {
            nextOpportunityChartRequest(nextOpportunityId);
        } else if(longTermTradingChosenTime == LongTermTradingTimeChosenEvent.ChosenTime.HOURLY_3) {
            nextOpportunityChartRequest(nextOpportunityId);
        }
    }

    @Override
    protected void onMarketChanged() {
        super.onMarketChanged();
        chartView.clearData();
        chartView.removeZoom();

        hasPreviousHour = false;
        previousHourLoaded = false;

        chartRequest();
    }

    protected void showZoomButtons(boolean show) {
        if (show) {
            zoomInView.setVisibility(View.VISIBLE);
            zoomOutView.setVisibility(View.VISIBLE);
        } else {
            zoomInView.setVisibility(View.GONE);
            zoomOutView.setVisibility(View.GONE);
        }
    }

    protected void loadFeaturedMarket(ChartDataMethodResult result) {
        if (market != null && !reloadMarket) {
            // This market is already loaded.
            return;
        }

        reloadMarket = false;
        market = application.getMarketForID(result.getMarketId());
        if (market == null) {
            market = new Market();
            market.setId(result.getMarketId());
            market.setDisplayName(result.getMarketName());
            market.setOptionPlusMarket(result.getOpportunityTypeId() == Opportunity.TYPE_OPTION_PLUS);
            market.setGroupId(0L);
            market.setExchangeId(0L);
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.EXTRA_MARKET, market);
        bundle.putString(Constants.EXTRA_ACTION_BAR_TITLE, market.getDisplayName());
        bundle.putLong(Constants.EXTRA_MARKET_PRODUCT_TYPE, market.getProductTypeId());

        application.postEvent(new NavigationEvent(Screen.ASSET_PAGE,
                NavigationType.HORIZONTAL, bundle));
    }

    protected void showClosingLevelInfo(boolean show, String level) {
        if (show) {
            if(longTermTradingLayout != null){
                longTermTradingLayout.setVisibility(View.GONE);
            }
            closingLevelView.setText(level);
            closingLevelBg.setVisibility(View.VISIBLE);
            closingLevelLayout.setVisibility(View.VISIBLE);
        } else {
            closingLevelLayout.setVisibility(View.GONE);
            closingLevelBg.setVisibility(View.GONE);
        }
    }

    protected void showLongTermTradingInfo(boolean show) {
        if (show) {
            chartView.clearData();
            chartView.removeZoom();
            showZoomButtons(false);
            longTermTradingLayout.setVisibility(View.VISIBLE);
        } else {
            longTermTradingLayout.setVisibility(View.GONE);
            showZoomButtons(true);
        }
    }

}
