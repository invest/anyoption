package com.anyoption.android.app.util;

import android.annotation.TargetApi;
import android.content.res.ColorStateList;

import com.anyoption.android.app.Application;

/**
 * Created by veselin.hristozov on 5.7.2016 г..
 */
public class ColorUtils {

    public static int getColor(int resId){
        if(Utils.getDeviceOSVersionCode() >= 23) {
            return Application.get().getResources().getColor(resId, Application.get().getTheme());
        } else {
            return Application.get().getResources().getColor(resId);
        }
    }

    public static ColorStateList getColorStateList(int resId){
        if(Utils.getDeviceOSVersionCode() >= 23) {
            return Application.get().getResources().getColorStateList(resId, Application.get().getTheme());
        } else {
            return Application.get().getResources().getColorStateList(resId);
        }
    }


}
