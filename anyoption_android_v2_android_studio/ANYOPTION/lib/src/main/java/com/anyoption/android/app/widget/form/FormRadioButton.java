package com.anyoption.android.app.widget.form;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.LayoutDirection;
import com.anyoption.android.R;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.widget.RadioButton;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.util.AttributeSet;
import android.view.Gravity;

public class FormRadioButton extends RadioButton implements FormItem{

	private LayoutDirection direction;
	
	public FormRadioButton(Context context) {
		super(context);
		init(context, null, 0);
	}

	public FormRadioButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}

	public FormRadioButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	/**
	 * Initial setup.
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	private void init(Context context, AttributeSet attrs, int defStyle){
		if(!isInEditMode()){
			//Clear the default button Drawable (As it will be custom drawable and it will depend on the layout direction).
			direction = Application.get().getLayoutDirection();
			setButtonDrawable(new StateListDrawable());
			setupUIForDirection();			
		}else{
			
		}
	}

	@Override
	public void setDirection(LayoutDirection direction) {
		if(this.direction != direction){
			this.direction = direction;
			refreshUI();
		}
		this.direction = direction;
	}

	@Override
	public void setupUIForDirection() {
		Drawable drawable = DrawableUtils.getDrawable(R.drawable.radio_button);
		switch (direction) {
		case LEFT_TO_RIGHT:
			setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
			setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
			break; 
		case RIGHT_TO_LEFT:
			setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
			setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
			break;
		default:
			break;
		}
		setCompoundDrawablePadding((int)getResources().getDimension(R.dimen.radiobutton_padding));
		setPadding(1, 0, 1, 0);
	}

	@Override
	public LayoutDirection getDirection() {
		return this.direction;
	}

	@Override
	public boolean isEditable() {
		return isEnabled();
	}

	@Override
	public void setEditable(boolean isEditable) {
		setEnabled(isEditable);
	}

	@Override
	public boolean checkValidity() {
		return true;
	}

	@Override
	public String getValue() {
		return getText().toString();
	}

	@Override
	public void setValue(Object value) {
		setText(value.toString());
	}

	@Override
	public void addOnEditorActionListener(OnEditorActionListener onEditorActionListener) {}

	@Override
	public void clear() {
		setChecked(false);
	}

	@Override
	public void refreshUI() {
		setupUIForDirection();
	}

	@Override
	public void setFieldName(String fieldName) {
	}

	@Override
	public String getFieldName() {
		return null;
	}

	@Override
	public void displayError(String error) {		
	}
		
}
