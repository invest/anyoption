package com.anyoption.android.app.event;

import com.anyoption.android.app.Application.LayoutDirection;

/**
 * Event that occurs when there is a change in the layout direction (For example : Language change with language that has Right  to Left orientation).
 * @author Anastas Arnaudov
 *
 */
public class LayoutDirectionChangeEvent {

	private LayoutDirection layoutDirection;
	
	/**
	 * Creates new {@link LayoutDirectionChangeEvent}.
	 * @param layoutDirection
	 */
	public LayoutDirectionChangeEvent(LayoutDirection layoutDirection) {
		this.layoutDirection = layoutDirection;
	}

	public LayoutDirection getLayoutDirection() {
		return layoutDirection;
	}

	public void setLayoutDirection(LayoutDirection layoutDirection) {
		this.layoutDirection = layoutDirection;
	}

}
