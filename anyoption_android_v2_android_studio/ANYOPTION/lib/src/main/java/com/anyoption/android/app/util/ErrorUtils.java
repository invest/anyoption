package com.anyoption.android.app.util;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.R;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.manager.NotificationManager;
import com.anyoption.android.app.manager.PopUpManager.OnConfirmPopUpButtonPressedListener;
import com.anyoption.android.app.popup.AccountLockedBySecurityOkDialogFragment;
import com.anyoption.android.app.popup.LoginRegisterDialogfragment;
import com.anyoption.android.app.popup.OkDialogFragment;
import com.anyoption.android.app.popup.QuestionnaireBlockedOkDialogFragment;
import com.anyoption.android.app.popup.QuestionnaireRestrictedOkDialogFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.form.Form;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.ErrorMessage;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;

public class ErrorUtils {
	public static final String TAG = ErrorUtils.class.getSimpleName();

	/**
	 * Displays pop up for this type of error with the given message. If the
	 * error is of the custom errors the message is not used.
	 * 
	 * @param errorCode
	 * @param message
	 */
	public static void handleError(int errorCode, String message) {
		Log.e(TAG, "handleError. errorCode=" + errorCode + " , message=" + message);
		final Application application = Application.get();
		Activity activity = application.getCurrentActivity();
		NotificationManager notificationManager = application.getNotificationManager();

		switch (errorCode) {
			case Constants.ERROR_CODE_REGULATION_MISSING_QUESTIONNAIRE:
				application.getPopUpManager().showConfirmPopUp(Constants.EMPTY_STRING,
						activity.getString(R.string.questionnaire_must_fill),
						activity.getString(R.string.questionnaire_fill), activity.getString(R.string.ok),
						new OnConfirmPopUpButtonPressedListener() {
							@Override
							public void onPositive() {
									RegulationUtils.showQuestionnaire();
							}

							@Override
							public void onNegative() {
							}
						});
				break;
			case Constants.ERROR_CODE_REGULATION_SUSPENDED:
				if (message != null) {
					notificationManager.showNotificationError(message);
				} else {
					notificationManager.showNotificationError(
							Html.fromHtml(application.getString(R.string.suspended_forquestionnaire, application.getUser().getFirstName(),
									application.getString(R.string.suspended_support_email), Utils.getSupportPhone())).toString().replace("\n", ""));
				}
				break;
			case Constants.ERROR_CODE_NOT_ACCEPTED_TERMS:
				if (message != null) {
					application.getNotificationManager().showNotificationError(message);
				} else {
					notificationManager.showNotificationError("You have not accepted our Terms and Conditions");// TODO
					// in
					// res
				}
				break;
			case Constants.ERROR_CODE_INVEST_BALANCE:
				application.getPopUpManager().showInsufficienFundsPopUp();
				break;
			case Constants.ERROR_CODE_VALIDATION_LOW_CASH_BALANCE:
				application.getPopUpManager().showInsufficienFundsPopUp();
				break;
			case Constants.ERROR_CODE_INV_VALIDATION:
				if (message != null) {
					application.getNotificationManager().showNotificationError(message);
				} else {
					notificationManager.showNotificationError(application.getString(R.string.error_investment));
				}
				break;
			case Constants.ERROR_CODE_VALIDATION_MAXIMUM_EXPOSURE:
				if (message != null) {
					long skinId = application.getSkinId();
					String secondText = null;
					if(skinId == 3){ 		//Turkish
						secondText = application.getString(R.string.trade_swipe_left_to_see_more_investment_options_tr);
					}else if(skinId == 10){ //Russian
						secondText = application.getString(R.string.trade_swipe_left_to_see_more_investment_options_ru);
					}else if(skinId == 23){ //Nederlands
						secondText = application.getString(R.string.trade_swipe_left_to_see_more_investment_options_nl);
					}else if(skinId == 24){ //Swedish
						secondText = application.getString(R.string.trade_swipe_left_to_see_more_investment_options_sv);
					}else{
						secondText = application.getString(R.string.trade_swipe_left_to_see_more_investment_options);
					}

					application.getNotificationManager().showNotificationError(message + " " + secondText);
				} else {
					notificationManager.showNotificationError(application.getString(R.string.error_investment));
				}
				break;
			case Constants.ERROR_CODE_UNKNOWN:
				if (message != null) {
					application.getNotificationManager().showNotificationError(message);
				} else {
					application.getNotificationManager().showNotificationError(application.getString(R.string.error));
				}
				break;
			case Constants.ERROR_CODE_CC_NOT_SUPPORTED:
				Bundle bundle = new Bundle();
				bundle.putString(Constants.EXTRA_MESSAGE,
						application.getString(R.string.errorExistingCreditCardNotSupported));
				application.getFragmentManager().showDialogFragment(OkDialogFragment.class, bundle);
				break;
			case Constants.ERROR_CODE_REGULATION_USER_RESTRICTED:
				Application.get().getFragmentManager()
						.showDialogFragment(QuestionnaireRestrictedOkDialogFragment.class, null);
				break;
			case Constants.ERROR_CODE_REG_SUSPENDED_QUEST_INCORRECT:
				Application.get().getFragmentManager()
						.showDialogFragment(QuestionnaireRestrictedOkDialogFragment.class, null);//CHANGED FROM QuestionnaireBlockedOkDialogFragment
				break;
			case Constants.ERROR_CODE_NO_CASH_FOR_NIOU:
				//SHOWING POPUP IN THE CALLBACK RESULT NOT HERE
				break;
			case Constants.ERROR_CODE_REGULATION_SUSPENDED_DOCUMENTS:
				Application.get().postEvent(new NavigationEvent(Screen.ACCOUNT_BLOCKED, NavigationType.INIT));
				break;
			case Constants.ERROR_CODE_LOGIN_FAILED:
				if ((Application.get().isAnyoption() || application.isBDA()) && message != null && message.endsWith("2501") ) {
					application.getFragmentManager().showDialogFragment(AccountLockedBySecurityOkDialogFragment.class, null);
				} else {
					if (message != null) {
						if (message.endsWith("2501")) {
							application.getNotificationManager().showNotificationError(message.substring(0, message.length() - 4));
						} else {
							application.getNotificationManager().showNotificationError(message);
						}
					} else {
						application.getNotificationManager().showNotificationError(application.getString(R.string.error));
					}
				}
				break;
			case Constants.ERROR_CODE_SESSION_EXPIRED:
				application.getCommunicationManager().cancelAllRequests();
				application.setUser(null);
				application.setUserRegulation(null);
				application.postEvent(new LoginLogoutEvent(LoginLogoutEvent.Type.LOGOUT, null));
				NavigationEvent navigationEvent = new NavigationEvent(Screen.LOGIN, NavigationType.INIT);
				navigationEvent.setToScreen(Screen.LOGIN);
				application.postEvent(navigationEvent);
				break;
			default:
				if (message != null) {
					application.getNotificationManager().showNotificationError(message);
				} else {
					application.getNotificationManager().showNotificationError(application.getString(R.string.error));
				}
				break;
		}
	}

	/**
	 * Displays pop up for this type of error.
	 * 
	 * @param errorCode
	 */
	public static void handleError(int errorCode) {
		Log.e(TAG, "handleError. errorCode=" + errorCode);
		handleError(errorCode, null);
	}

	/**
	 * Displays pop up which depends on the errorCode of the result. For message
	 * is used the first user message. The user message is not used for some of
	 * the error types.
	 * 
	 * @param result
	 */
	public static void handleResultError(MethodResult result) {
		Log.e(TAG, "handleResultError");
		if (result == null) {
			Log.e(TAG, "Result is null.");
			handleError(Constants.ERROR_CODE_UNKNOWN);
			return;
		}

		if (result.getUserMessages() != null && result.getUserMessages().length > 0) {
			if (result.getApiCode() != null && result.getApiCode().equals("error.min.deposit")) {
				Bundle bundle = new Bundle();
				bundle.putString(Constants.EXTRA_MESSAGE, result.getUserMessages()[0].getMessage());
				Application.get().getFragmentManager().showDialogFragment(OkDialogFragment.class, bundle);
			} else {
				handleError(result.getErrorCode(), result.getUserMessages()[0].getMessage());
			}
		} else {
			handleError(result.getErrorCode());
		}
	}

	public static void displayError() {
		handleError(-1);
	}

	public static void displayFieldError(Form form, MethodResult result) {
		try {
			if (result != null) {

				if (checkCardNotSupportedError(result)) {
					Bundle bundle = new Bundle();
					bundle.putString(Constants.EXTRA_MESSAGE,
							Application.get().getString(R.string.errorNewCreditCardNotSupported));
					Application.get().getFragmentManager().showDialogFragment(OkDialogFragment.class, bundle);
					return;
				}

				if (result.getUserMessages() != null) {
					String error = result.getUserMessages()[0].getMessage();
					String field = result.getUserMessages()[0].getField();
					Log.d(Utils.TAG, "displayFieldError, field: " + field + " error: " + error + "\n");

					if (!Utils.isParameterEmptyOrNull(field) && form.getItemFields().get(field) != null) {
						form.getItemFields().get(field).displayError(error);
					} else {
						handleResultError(result);
					}
				} else {
					handleResultError(result);
				}

			} else {
				handleError(-1);
			}
		} catch (Exception e) {
			Log.e(TAG, "Can't display error.", e);
		}
	}

	private static boolean checkCardNotSupportedError(MethodResult result) {
		if (result != null && result.getUserMessages() != null) {
			for (ErrorMessage errorMessage : result.getUserMessages()) {
				String field = errorMessage.getField();
				if (field.equals("212")) {
					return true;
				}
			}
		}
		return false;
	}


}
