package com.anyoption.android.app.manager;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.GCMEvent;
import com.anyoption.android.app.event.PushNotificationsPermissionEvent;
import com.anyoption.android.app.eventbus.EventBus;
import com.anyoption.android.app.service.GcmIntentService;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.requests.CheckUpdateMethodRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

/**
 * GCM Manager that manages the GCM(Google Cloud Messaging) configuration, messaging,  etc..
 * @author Anastas Arnaudov
 *
 */
@Deprecated
public class GCMManager {

	public static final String TAG = GCMManager.class.getSimpleName();

	public static final String SENDER_ID	= "322116969152";
	
	
	public static final String ACTION_REGISTRATION 		= "com.google.android.c2dm.intent.REGISTRATION";
	public static final String ACTION_RETRY 			= "com.google.android.c2dm.intent.RETRY";
	public static final String ACTION_RECEIVE 			= "com.google.android.c2dm.intent.RECEIVE";
	
	public static final String EXTRA_UNREGISTERED 		= "unregistered";
	public static final String EXTRA_ERROR 				= "error";
	public static final String EXTRA_REGISTRATION_ID 	= "registration_id";
	public static final String EXTRA_MESSAGE 			= "message";
	
	private Application application;
	private GoogleCloudMessaging gcm;
	
	public enum GooglePlayServicesCode{
		/**
		 * The Google Play Services are available.
		 */
		SUCCESS,
		/**
		 * The Google Play Services are not available, but the device supports them and they can be downloaded.
		 */
		ERROR_RECOVERABLE,
		/**
		 * The Google Play Services are not available and the device does not supports them.
		 */
		ERROR_ERROR_UNRECOVERABLE;
	}
	
	/**
	 * {@link GCMManager}
	 * @param application
	 */
	public GCMManager(Application application){
		Log.d(TAG, "On Create");
		this.application = application;
		application.registerForEvents(this, GCMEvent.class);
	}
	
	/**
	 * Check the device to make sure it has the Google Play Services APK. If
	 * it doesn't, display a dialog that allows users to download the APK from
	 * the Google Play Store or enable it in the device's system settings.
	 */
	public GooglePlayServicesCode checkGooglePlayServicesAvailable(){
		Log.i(TAG, "Checking Google Play Service availability!");
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(application);
		if(resultCode != ConnectionResult.SUCCESS) {
			Log.e(TAG, "Google Play Service are NOT available!");
			Log.i(TAG, "Checking if the error is recoverable!");
	        if(GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				Log.i(TAG, "The error is recoverable!");
				Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, application.getCurrentActivity(), Constants.REQUEST_CODE_GOOGLE_PLAY_SERVICES, new OnCancelListener() {		
					@Override
					public void onCancel(DialogInterface dialog) {
						application.postEvent(new PushNotificationsPermissionEvent(false));
					}
				});
				dialog.setCancelable(false);
				dialog.setCanceledOnTouchOutside(false);
				dialog.show();
				return GooglePlayServicesCode.ERROR_RECOVERABLE;
	        }else{
	        	Log.e(TAG, "The error is NOT recoverable!");
	            Log.e(TAG, "This device is not supported.");
	            application.getNotificationManager().showNotificationError("The device does not support Push Notifications!");
	            return GooglePlayServicesCode.ERROR_ERROR_UNRECOVERABLE;
	        }
	    }
		Log.i(TAG, "Google Play Service are available!");
	    return GooglePlayServicesCode.SUCCESS;	    
	}

	/**
	 * Sends Register Request to the GCM in a {@link AsyncTask}.
	 */
	private void registerOnBackground() {
		Log.d(TAG, "Registering the device to the GCM.");
		new AsyncTask<String, Void, String>() {
			@Override
			protected String doInBackground(String... params) {
				String result = "";
			    try {
			        if (gcm == null) {
			            gcm = GoogleCloudMessaging.getInstance(application);
			        }
			       
			        //Request GCM registerID:
			        result = gcm.register(SENDER_ID);
			    } catch (Exception ex) {
			    	Log.e(TAG, ex.getLocalizedMessage());
			    }
			    return result;
			}
		}.execute();
	}
	
	/**
	 * Checks if the registration has expired.
	 *
	 * <p>To avoid the scenario where the device sends the registration to the
	 * server but the server loses it, the app developer may choose to re-register
	 * after REGISTRATION_EXPIRY_TIME.
	 *
	 * @return true if the registration has expired.
	 */
	private boolean isRegistrationExpired() {
		//Checks if the information is not stale
		long expirationTime = application.getSharedPreferencesManager().getLong(Constants.PREFERENCES_GCM_REGISTER_EXPIRATION_TIME, -1l);
	    return System.currentTimeMillis() > expirationTime;
	}
	
	/**
	 * Gets the current registration id for application on GCM service.
	 * <p>If result is empty, the registration has failed.
	 *
	 * @return registration id, or empty string if the registration is not complete.
	 */
	public String getRegistrationId() {
		String registrationId = application.getSharedPreferencesManager().getString(Constants.PREFERENCES_GCM_REGISTER_ID, "");
	
	    if (registrationId.length() == 0) {
	        Log.v(TAG, "Registration not found.");
	        return "";
	    }
	    // Check if the Application was updated; if so, it must clear registration id to
	    // avoid a race condition if GCM sends a message
	    int registeredVersion = application.getSharedPreferencesManager().getInt(Constants.PREFERENCES_APPLICATION_VERSION, 1);
	    int currentVersion = application.getApplicationVersionCode();
	    if (registeredVersion != currentVersion || isRegistrationExpired()) {
	        Log.v(TAG, "App version changed or registration expired.");
	        return "";
	    }
	    return registrationId;
	}
	
	/**
	 * Send the obtained GCM Registration ID to our server.
	 */
	private void sendRegisterID(String registerID){
		Log.d(TAG, "Sending registerID to our server!!!");
		
		 CheckUpdateMethodRequest request = new CheckUpdateMethodRequest();
         request.setDeviceId(Utils.getUDID());
         request.setC2dmRegistrationId(registerID);
         application.getCommunicationManager().requestService(this, "sendRegisterIDCallback", Constants.SERVICE_GCM_UPDATE, request, MethodResult.class, false, false);
	}
	
	public void sendRegisterIDCallback(Object result){
		
	}
	
	/**
	 * Called by the {@link EventBus} when {@link GCMEvent} occurs.
	 * @param event
	 */
	public void onEvent(GCMEvent event){
		Log.d(TAG, "On event");
		Intent intent = event.getIntent();
		String action = intent.getAction();
		
		if(action.equals(ACTION_REGISTRATION)){
			onRegistration(intent);
		}else if(action.equals(ACTION_RETRY)){
			onRetry(intent);
		}else if(action.equals(ACTION_RECEIVE)){
			onReceive(intent);
		}
	}
	
	/**
	 * Called when we receive GCM message of type Registration.
	 * @param intent
	 */
	public void onRegistration(Intent intent){
		Log.d(TAG, "On Registration");
		String registrationId = intent.getStringExtra(EXTRA_REGISTRATION_ID);
	    String error = intent.getStringExtra(EXTRA_ERROR);
	    String removed = intent.getStringExtra(EXTRA_UNREGISTERED);
	    
	    if(removed != null){
	    	//We are unregistered:
	    	registerOnBackground();
	    }else if(error != null){
	    	if(error.equals(GoogleCloudMessaging.ERROR_SERVICE_NOT_AVAILABLE)){
	    		Log.e(TAG, "On Registration : GCM Service is not available!!!");  		
	    	}else{
	    		Log.e(TAG, "On Registration : unexpected error!!!"); 
	    	}
	    }else if(registrationId != null){
	    	Log.d(TAG, "On Registration event : success");
	    	//We are registered.
	        //Save the register id - no need to register again.
	    	application.getSharedPreferencesManager().putString(Constants.PREFERENCES_GCM_REGISTER_ID, registrationId);

	    	sendRegisterID(registrationId);
	    }
		
	}
	
	/**
	 * Called when we receive GCM message of type Retry.
	 * @param intent
	 */
	public void onRetry(Intent intent) {
		Log.d(TAG, "On Retry");
		registerOnBackground();
	}
	
	/**
	 * Called when we receive GCM message of type Receive.
	 * This is the point when we receive an actual message from our server.
	 * @param intent
	 */
	public void onReceive(Intent intent) {
		Log.d(TAG, "On Receive");
		//TODO Consider firstly to parse the message (as it is in json format)
	
		//Check if the user has disabled the Push Notifications:
		if(application.getSharedPreferencesManager().getBoolean(Constants.PREFERENCES_IS_NOTIFICATIONS_ON, true)){
			
			//Check if the Application is running:
			if(Application.get().isApplicationVisible()){
				//Show local notification:			
				Application.get().getNotificationManager().showNotificationSuccess(intent.getStringExtra("text"));
			}else{
				//If the Application is not visible : start service to show a notification:
				ComponentName comp = new ComponentName(application.getPackageName(), GcmIntentService.class.getName());
				WakefulBroadcastReceiver.startWakefulService(application, (intent.setComponent(comp)));	
			}
			
		}
		
	}
	
}
