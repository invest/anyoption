package com.anyoption.android.app.widget.questionnaire;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.model.Answer;
import com.anyoption.android.app.model.Question;

/**
 * Created by veselin.hristozov on 10.10.2016 г..
 */

public class QuestionnaireTextFieldAnswerView extends QuestionnaireBaseView implements TextWatcher {

    private LinearLayout rootView;
    private EditText answerEditText;

    public QuestionnaireTextFieldAnswerView(Context context) {
        super(context);
    }

    public QuestionnaireTextFieldAnswerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public QuestionnaireTextFieldAnswerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void init() {
        questionType = Question.QuestionType.CHECKBOX;

        rootView = (LinearLayout) View.inflate(getContext(), R.layout.questionanaire_text_field_item, this);
        questionTextView = (TextView) rootView.findViewById(R.id.questionnaire_item_checkbox_text);
        answerEditText = (EditText) rootView.findViewById(R.id.questionnaire_item_answer_field_edit_text);
        errorTextView = (TextView) rootView.findViewById(R.id.questionnaire_item_error_text_view);

        if (!isInEditMode()) {
            answerEditText.addTextChangedListener(this);
        }
    }

    public void showError(){
        errorTextView.setVisibility(View.VISIBLE);
    }

    public void setQuestion(Question question){
        super.setQuestion(question);

        decorateText(questionTextView, question.getDescription());
    }

    public void setAnswerSelectedListener(OnAnswerSelectedListener answerSelectedListener) {
        this.answerSelectedListener = answerSelectedListener;
    }

    public TextView getQuestionTextView() {
        return questionTextView;
    }

    @Override
    public boolean isAnswered() {
        if (question.isMandatory()) {
            return getAnswer() != null && !(getAnswer().getTextAnswer() == null || getAnswer().getTextAnswer().length() == 0);
        } else {
            return true;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if(s.length() != 0) {
            answer.setTextAnswer(answerEditText.getText().toString());
        } else {
            answer.setTextAnswer("");
        }

        if(answerSelectedListener != null){
            answerSelectedListener.onAnswerSelected(question, answer);
        }
    }
}
