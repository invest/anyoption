package com.anyoption.android.app.model.questionnaire;

import com.anyoption.common.beans.base.QuestionnaireAnswer;
import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;

public class CheckboxQuestion extends SingleChoiceQuestion {

	private static final int CHECKED_ANSWER_POSITION = 0;
	private static final int NOT_CHECKED_ANSWER_POSITION = 1;

	public CheckboxQuestion(long orderId, String quetionText, QuestionnaireQuestion question,
			QuestionnaireUserAnswer answer) {
		super(orderId, quetionText, question, answer);
	}

	public CheckboxQuestion(long orderId, String quetionText) {
		super(orderId, quetionText);
	}

	public boolean isChecked() {
		if (getUserAnswer().getAnswerId() == null) {
			// question not answered - make it checked by default
			return true;
		}

		int answerIndex = CHECKED_ANSWER_POSITION;
		for (int i = 0; i < getQuestion().getAnswers().size(); i++) {
			QuestionnaireAnswer answer = getQuestion().getAnswers().get(i);
			if (answer.getId() == getUserAnswer().getAnswerId()) {
				answerIndex = i;
				break;
			}
		}

		if (answerIndex == CHECKED_ANSWER_POSITION) {
			return true;
		} else if (answerIndex == NOT_CHECKED_ANSWER_POSITION) {
			return false;
		}

		return false;
	}

	/**
	 * Updates the user's answer with answer corresponding to checked value.
	 * 
	 * @param checked
	 */
	public void setChecked(boolean checked) {
		QuestionnaireAnswer newAnswer;
		if (checked) {
			newAnswer = getQuestion().getAnswers().get(CHECKED_ANSWER_POSITION);
		} else {
			newAnswer = getQuestion().getAnswers().get(NOT_CHECKED_ANSWER_POSITION);
		}

		userAnswer.setAnswerId(newAnswer.getId());
	}

	@Override
	public boolean isAnswered() {
		// This type of question is always considered answered.
		return true;
	}

	public QuestionnaireAnswer getCheckedAnswer() {
		if (question != null) {
			return question.getAnswers().get(CHECKED_ANSWER_POSITION);
		}

		return null;
	}

	public QuestionnaireAnswer getNotCheckedAnswer() {
		if (question != null) {
			return question.getAnswers().get(NOT_CHECKED_ANSWER_POSITION);
		}

		return null;
	}

}
