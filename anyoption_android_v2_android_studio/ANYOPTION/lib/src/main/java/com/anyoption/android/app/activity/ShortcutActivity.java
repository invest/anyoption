package com.anyoption.android.app.activity;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.manager.AppseeManager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Custom {@link Activity} that acts as a dispatcher.
 * After the desktop shortcut icon is clicked this Activity is started.
 *
 * @author Anastas Arnaudov
 */
public class ShortcutActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AppseeManager.onStart();
		Intent intent;

		//Check if the Application is running:
		Activity currentActivity = Application.get().getCurrentActivity();
		if (Application.get().isApplicationRunning() && currentActivity != null) {
			//The Application is running so we will resume it:
			intent = new Intent(getApplicationContext(), currentActivity.getClass());
		} else {
			//We launch the initial Activity:
			intent = new Intent(getApplicationContext(), Application.get().getSubClassForClass(LaunchActivity.class));
		}

		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

		finish();
		startActivity(intent);

		if(Application.get().getCurrentActivity() != null) {
			Application.get().getCurrentActivity().overridePendingTransition(0, 0);
		}
	}
}
