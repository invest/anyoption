package com.anyoption.android.app.fragment.launch;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application.LayoutDirection;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.manager.PopUpManager.OnConfirmPopUpButtonPressedListener;
import com.anyoption.android.app.popup.TryWithoutAccountDialogfragment;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.LanguageSelector;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.User;

import java.util.List;

/**
 * Welcom Screen.
 * @author Anastas Arnaudov
 *
 */
public abstract class WelcomeFragment extends NavigationalFragment {

	public static final String TAG = WelcomeFragment.class.getSimpleName(); 
	
	public static int NUMBER_OF_PAGES = 3;

	protected Long skinID;
	protected ViewPager pager;
	protected SlidePagerAdapter pagerAdapter;

	protected LinearLayout dotsLayout;
	protected int currentPage = 0;

	protected View nextButton;
	protected View loginButton;
	protected View registerButton;
	protected View tradeButton;

	protected LanguageSelector languagesButton;

	protected User user;
	protected boolean isTablet;

	protected View rootView;

	protected TextView tryWithoutAccountTextView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = getArguments();
		if(bundle != null){
			//Get args
		}
		
		user = application.getUser();
		isTablet = getResources().getBoolean(R.bool.isTablet);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.welcome_fragment_layout, container, false);
	
		skinID = getArguments().getLong(Constants.EXTRA_SKIN_ID);
		
		// Instantiate a ViewPager and a PagerAdapter.
        pager = (ViewPager) rootView.findViewById(R.id.welcome_screen_view_pager);
        pagerAdapter = new SlidePagerAdapter(getChildFragmentManager(), getFragments());
        pager.setAdapter(pagerAdapter);
        if(application.getLayoutDirection() == LayoutDirection.LEFT_TO_RIGHT){
        	currentPage = 0;
        }else{        	
        	currentPage = NUMBER_OF_PAGES - 1;
        }
        pager.setCurrentItem(currentPage);
        
        dotsLayout = (LinearLayout) rootView.findViewById(R.id.welcome_screen_pages_dots); 
       
        setupLanguageButton();
        
        nextButton = rootView.findViewById(R.id.welcome_screen_next_button);
        nextButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(application.getLayoutDirection() == LayoutDirection.LEFT_TO_RIGHT){
					pager.setCurrentItem(++currentPage, true);					
				}else{
					pager.setCurrentItem(--currentPage, true);
				}
			}
		});
        
        setupLoginRegisterButtons();
        
        tradeButton = rootView.findViewById(R.id.welcome_screen_trade_button);
        tradeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				goToNextLoggedInScreen();
			}
		});

        setupTryWithoutAccount();

        setupDots();
        changeDots(currentPage);
        setupNextButton();
        
        pager.addOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				
				Log.d(TAG, "onPageSelected");
				currentPage = position;
				changeDots(currentPage);
				setupLanguageButton();
				setupNextButton();
				
			}
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
			@Override
			public void onPageScrollStateChanged(int state) {}
		});
		  
		return rootView;
	}

	protected void goToNextLoggedInScreen() {
		application.postEvent(new NavigationEvent(getNextLoggedInScreen(), NavigationType.INIT).setToNewActivity(true));
	}

	protected void setupLoginRegisterButtons(){
        loginButton = rootView.findViewById(R.id.welcome_screen_login_button);
        loginButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				application.postEvent(new NavigationEvent(getLoginScreen(), NavigationType.INIT).setToNewActivity(true));
			}
		});
        
        registerButton = rootView.findViewById(R.id.welcome_screen_register_button);
        registerButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				application.postEvent(new NavigationEvent(getRegisterScreen(), NavigationType.INIT).setToNewActivity(true));
			}
		});
	}
	
	protected void setupLanguageButton() {
		languagesButton =  (LanguageSelector) rootView.findViewById(R.id.welcome_screen_language_button);
	    languagesButton.setSkinID(skinID);
		if(user == null){
			//If the user is logged in we do not show the language button:
			languagesButton.setVisibility(View.VISIBLE);
		}else{
			languagesButton.setVisibility(View.GONE);
		}
	}

	protected Screenable getLoginScreen() {
		return Screen.LOGIN;
	}
	
	protected Screenable getRegisterScreen() {
		return Screen.REGISTER;
	}
	
	protected void setupTryWithoutAccount() {
        tryWithoutAccountTextView = (TextView) rootView.findViewById(R.id.welcome_screen_try_without_account_text);
		TextUtils.underlineText(tryWithoutAccountTextView);
		tryWithoutAccountTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {	
				TryWithoutAccountDialogfragment tryWithoutAccountDialogfragment = (TryWithoutAccountDialogfragment) application.getFragmentManager().showDialogFragment(TryWithoutAccountDialogfragment.class, null);
				tryWithoutAccountDialogfragment.setOnConfirmPopUpButtonPressedListener(new OnConfirmPopUpButtonPressedListener() {
					@Override
					public void onPositive() {
						tryApp();
					}
					@Override
					public void onNegative() {}
				});
			} 
		});	
	}

	protected void tryApp(){
		application.postEvent(new NavigationEvent(application.getHomeScreen(), NavigationType.INIT));
	}

	protected void setupNextButton() {
		if( (application.getLayoutDirection() == LayoutDirection.LEFT_TO_RIGHT && currentPage == NUMBER_OF_PAGES - 1) ||
			(application.getLayoutDirection() == LayoutDirection.RIGHT_TO_LEFT && currentPage == 0)						){
			nextButton.setVisibility(View.GONE);
			if(user == null){
				loginButton.setVisibility(View.VISIBLE);
				registerButton.setVisibility(View.VISIBLE);	
				tradeButton.setVisibility(View.GONE);
				if (null != tryWithoutAccountTextView) {
					tryWithoutAccountTextView.setVisibility(View.VISIBLE);
				}
			}else{
				loginButton.setVisibility(View.GONE);
				registerButton.setVisibility(View.GONE);	
				tradeButton.setVisibility(View.VISIBLE);
				if (null != tryWithoutAccountTextView) {
					tryWithoutAccountTextView.setVisibility(View.INVISIBLE);
				}
			}
		}else{
			nextButton.setVisibility(View.VISIBLE);
			loginButton.setVisibility(View.GONE);
			registerButton.setVisibility(View.GONE);
			tradeButton.setVisibility(View.GONE);
			if (null != tryWithoutAccountTextView) {
				tryWithoutAccountTextView.setVisibility(View.INVISIBLE);
			}
		}
	}

	/**
	 * Returns the pages for the Slide Pager.
	 * @return
	 */
	protected abstract List<Fragment> getFragments();

	/**
	 * Create dots that represent page count.
	 */
	protected void setupDots(){
		//Create dot for each page:
		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		params.setMargins((int)ScreenUtils.convertDpToPx(application, 1), 0, (int)ScreenUtils.convertDpToPx(application, 1), 0);
		for(int i = 0; i < pagerAdapter.getCount(); i++){
			ImageView dotImage = new ImageView(application.getCurrentActivity());
			dotImage.setImageDrawable(getCurrentDot());
		
			
			dotImage.setLayoutParams(params);
			
			dotsLayout.addView(dotImage);
		}
		dotsLayout.invalidate();
	}
	
	/**
	 * Change the dot that corresponds to the current page.
	 */
	protected void changeDots(int page){
		for(int i = 0; i < pagerAdapter.getCount(); i++){
			ImageView dotImage = (ImageView) dotsLayout.getChildAt(i);
			if(i == page){
				dotImage.setImageDrawable(getCurrentDot());
			}else{
				dotImage.setImageDrawable(getOtherDot());
			}
		}
		dotsLayout.invalidate();
	}
	
	protected Drawable getCurrentDot() {
		return DrawableUtils.getDrawable(R.drawable.dot_filled_blue);
	}
	
	protected Drawable getOtherDot() {
		return DrawableUtils.getDrawable(R.drawable.dot_empty_blue);
	}
	
	////////////////////////////////////////////////////////////////////////////////
	
	/**
     * A simple pager adapter that represents Fragment objects, in sequence.
     */
    public class SlidePagerAdapter extends FragmentStatePagerAdapter {
        
    	private List<Fragment> fragmentList;
    	
    	public SlidePagerAdapter(FragmentManager fragmentManager, List<Fragment> fragmentList) {
            super(fragmentManager);
            this.fragmentList = fragmentList;
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

    }
    /////////////////////////////////////////////////////////////////////////////////

	@Override
	protected Screenable setupScreen() {
		return Screen.WELCOME;
	}

	protected Screenable getNextLoggedInScreen() {
		return application.getHomeScreen();
	}

}
