package com.anyoption.android.app.fragment.trade;

import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.anyoption.android.R;
import com.anyoption.android.app.event.ChartCommonEvent;
import com.anyoption.android.app.event.InvestmentEvent;
import com.anyoption.android.app.event.LightstreamerUpdateEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.OptionsEvent;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.lightstreamer.LightstreamerConnectionHandler;
import com.anyoption.android.app.lightstreamer.LightstreamerListener;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.service.results.ChartDataResult;
import com.lightstreamer.ls_client.UpdateInfo;

public class AssetPageDynamicsFragment extends BaseFragment implements LightstreamerConnectionHandler.StatusListener, LightstreamerListener, Callback {

	public static final String TAG = AssetPageDynamicsFragment.class.getSimpleName();

	private static final String INFO_FRAGMENT_TAG 					= "info_fragment_tag";
	private static final String HEADER_FRAGMENT_TAG 				= "header_fragment_tag";
	private static final String CHART_RING_FRAGMENT_TAG 			= "chart_rung_fragment_tag";
	private static final String OPTIONS_DYNAMICS_FRAGMENT_TAG 		= "options_dynamics_fragment_tag";

	private static final int MESSAGE_STATE_CHANGED = 4;

	private static final long CLOSING_TIME_PERIOD = 5 * 1000;

	private long closingLevelTime;
	private String lastLSCommand;

	private Market market;
	private long opportunityId;
	private int opportunityState;
	private long opportunityTimeClose;

	protected View root;
	private ImageView assetBackgroundImageView;

	private AssetInfoFragment infoFragment;
	private AssetHeaderFragment headerFragment;
	private ChartRingFragment chartRingFragment;
	private AssetOptionsDynamicsFragment optionsDynamicsFragment;

	private View tooltipLayout;

	private Handler handler;
	private UpdateInfo nextUpdate;
	private boolean isInvesting;

	/**
	 * Creates new instance of this fragment.
	 */
	public static AssetPageDynamicsFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Create new AssetPageDynamicsFragment.");
		AssetPageDynamicsFragment fragment = new AssetPageDynamicsFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	/**
	 * Use {@link #newInstance(Market)} instead of this constructor.
	 */
	public AssetPageDynamicsFragment() {
		// Do nothing
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate");

		// Register for this event here before chart fragment is created.
		registerForEvents();

		Bundle args = getArguments();
		if (args != null) {
			market = (Market) args.getSerializable(Constants.EXTRA_MARKET);
		}
		handler = new Handler(this);
	}

	@SuppressWarnings("unchecked")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView");
		root = inflater.inflate(R.layout.asset_dynamics_page_fragment, container, false);

		assetBackgroundImageView = (ImageView) root.findViewById(R.id.asset_page_asset_bg_image_view);

		Bundle args = new Bundle();
		args.putSerializable(Constants.EXTRA_MARKET, market);

		//Create inner Fragments:
		FragmentManager childFragmentManager = getChildFragmentManager();
		FragmentTransaction fragmentTransaction = childFragmentManager.beginTransaction();

		if (childFragmentManager.findFragmentByTag(INFO_FRAGMENT_TAG) == null) {
			infoFragment = (AssetInfoFragment) BaseFragment.newInstance(application.getSubClassForClass(AssetInfoFragment.class), args);
			fragmentTransaction.add(R.id.asset_page_dynamics_info_drawer_fragment, infoFragment, INFO_FRAGMENT_TAG);
		} else {
			infoFragment = (AssetInfoFragment) childFragmentManager.findFragmentByTag(INFO_FRAGMENT_TAG);
		}

		if (childFragmentManager.findFragmentByTag(HEADER_FRAGMENT_TAG) == null) {
			headerFragment = (AssetHeaderFragment) BaseFragment.newInstance(application.getSubClassForClass(AssetHeaderFragment.class), args);
			fragmentTransaction.add(R.id.asset_page_dynamics_header_fragment, headerFragment, HEADER_FRAGMENT_TAG);
		} else {
			headerFragment = (AssetHeaderFragment) childFragmentManager.findFragmentByTag(HEADER_FRAGMENT_TAG);
		}

		if (childFragmentManager.findFragmentByTag(CHART_RING_FRAGMENT_TAG) == null) {
			chartRingFragment = (ChartRingFragment) BaseFragment.newInstance(application.getSubClassForClass(ChartRingFragment.class), args);
			fragmentTransaction.add(R.id.asset_page_dynamics_chart_fragment, chartRingFragment, CHART_RING_FRAGMENT_TAG);
		} else {
			chartRingFragment = (ChartRingFragment) childFragmentManager.findFragmentByTag(CHART_RING_FRAGMENT_TAG);
		}

		if (childFragmentManager.findFragmentByTag(OPTIONS_DYNAMICS_FRAGMENT_TAG) == null) {
			optionsDynamicsFragment = (AssetOptionsDynamicsFragment) BaseFragment.newInstance(application.getSubClassForClass(AssetOptionsDynamicsFragment.class), args);
			fragmentTransaction.add(R.id.asset_page_dynamics_options_fragment, optionsDynamicsFragment, OPTIONS_DYNAMICS_FRAGMENT_TAG);
		} else {
			optionsDynamicsFragment = (AssetOptionsDynamicsFragment) childFragmentManager.findFragmentByTag(OPTIONS_DYNAMICS_FRAGMENT_TAG);
		}

		fragmentTransaction.commit();


		return root;
	}

	@Override
	public void onResume() {
		super.onResume();

		if (isPaused) {
			isPaused = false;
			reload();
		}
	}

	@Override
	public void onPause() {
		unregisterLightStreamer();
		super.onPause();
	}

	@Override
	public void onDestroy() {
		unregisterLightStreamer();
		application.unregisterForAllEvents(this);
		super.onDestroy();
	}

	public void loadMarket(Bundle bundle){
		market = (Market) bundle.getSerializable(Constants.EXTRA_MARKET);
		closingLevelTime = 0;
		chartRingFragment.loadMarket(market);
	}

	private void reload(){
		Log.d(TAG, "Reload");
		closingLevelTime = 0;
		chartRingFragment.reload();
	}

	private void registerForEvents() {
		application.registerForEvents(this, NavigationEvent.class, ChartCommonEvent.class, LoginLogoutEvent.class, OptionsEvent.class, InvestmentEvent.class);
	}

	private void showUnavailableInfoDynamics() {
		Log.d(TAG, "Showing Unavailable state.");
		//Set up the Unavaiable Background:
		assetBackgroundImageView.setImageResource(R.drawable.trade_asset_background_not_available);
	}

	private void showAvailableInfoDynamics() {
		Log.d(TAG, "Showing Available state.");
		//Setup the Market Background:
		assetBackgroundImageView.setImageResource(R.drawable.market_dynamics_bg);
	}

	//////////////////////////////////////////////////////////////////////////////////
	//                              EVENTs                                          //
	//////////////////////////////////////////////////////////////////////////////////

	public void onEventMainThread(ChartCommonEvent event) {
		Log.d(TAG, "new ChartCommonEvent. Type = " + event.getType());

		if(market == null) {
			Log.d(TAG, "Current market id is null");
			//Get the Market from the result:
			if (chartRingFragment.getMarket() != null) {
				market = chartRingFragment.getMarket();
			}
		} else if (event.getMarketId() != market.getId()) {
			Log.d(TAG, "Current market id is different from the result market id.");
			return;
		}

		//Update the inner fragments:
		infoFragment.setMarket(market);
		headerFragment.setMarket(market);
		optionsDynamicsFragment.setMarket(market);
		optionsDynamicsFragment.loadData();
		infoFragment.loadData();

		//Attach / Detach Lighstreamer:
		if (event.getType() == ChartCommonEvent.Type.DATA_REQUESTED) {
			unregisterLightStreamer();
		} else if (event.getType() == ChartCommonEvent.Type.DATA_REQUEST_SUCCESS) {
			ChartDataResult data = event.getChartData();
			opportunityId = data.getOpportunityId();
			opportunityState = 0;
			registerLightstreamerDynamics(data);
		}
	}

	public void onEventMainThread(LoginLogoutEvent event){
		Log.d(TAG, "New LoginLogoutEvent");
	}

	public void onEventMainThread(InvestmentEvent event) {
		Log.d(TAG, "New InvestmentEvent");
		//Not for the current Market:
		if (event.getMarketId() != market.getId()) {
			return;
		}

		//Check the type and set up the flag:
		if (event.getType() == InvestmentEvent.Type.INVEST_REQUESTED) {
			isInvesting = true;
		} else if (event.getType() == InvestmentEvent.Type.INVEST_SUCCESS || event.getType() == InvestmentEvent.Type.INVEST_FAIL) {
			isInvesting = false;
		}
	}
	//////////////////////////////////////////////////////////////////////////////////


	//////////////////////////////////////////////////////////////////////////////////
	//                              LIGHTSTREAMER                                   //
	//////////////////////////////////////////////////////////////////////////////////

	private void registerLightstreamerDynamics(ChartDataResult chartData) {
		Log.d(TAG, "registerLightstreamer for marketId=" + chartData.getMarketId());
		String group = LightstreamerManager.COLUMN_DYNAMICS_ITEM_PREFIX + "_" + chartData.getMarketId() + "_" + Constants.DYNAMICS_OPPORTUNITY_TYPE_ID;
		application.getLightstreamerManager().subscribeAssetPageTable(market.getId(), this, new String[]{group});
	}

	private void unregisterLightStreamer() {
		Log.d(TAG, "unregisterLightStreamer");
		if(market != null){
			application.getLightstreamerManager().unsubscribeAssetPageTable(market.getId());
		}
	}

	@Override
	public void updateItem(int itemPos, String itemName, UpdateInfo update) {
		//Check if Market is null or the user is investing at the moment:
		if (market == null || isInvesting) {
			if(market == null){
				Log.d(TAG, "LS update ignore - market is not yet set.");
			}else{
				Log.d(TAG, "LS update ignore - an Investment is being proccessed");
			}
			return;
		}

		//Check the LS command:
		String command = update.getNewValue(LightstreamerManager.COLUMN_ET_COMMAND);
		if(update.isValueChanged(LightstreamerManager.COLUMN_ET_COMMAND)){
			Log.d(TAG, "LS update NEW COMMAND = " + command);
			lastLSCommand = command;
			if (command.equals(LightstreamerManager.COMMAND_DELETE) && opportunityState == Opportunity.STATE_CLOSED) {
				reload();
				return;
			}
		}

		//Check if the update is for the current opportunity:
		long opportunityId = Long.valueOf(update.getNewValue(LightstreamerManager.COLUMN_ET_KEY));
		if(this.opportunityId != opportunityId){
			return;
		}

		//Get the Market id:
		long updateMarketId = Long.parseLong(itemName.split("_")[1]);
		if (updateMarketId != market.getId()) {
			Log.w(TAG, "LS update ignore - OTHER MARKET. id=" + updateMarketId);
			return;
		}

		//Check if there is any change of the State:
		int newOpportunityState = Integer.parseInt(update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_STATE));
		if (opportunityState == 0 || (update.isValueChanged(LightstreamerManager.COLUMN_DYNAMICS_STATE) && opportunityState < newOpportunityState)) {
			opportunityState = Integer.parseInt(update.getNewValue(LightstreamerManager.COLUMN_DYNAMICS_STATE));
			Log.d(TAG, "LS update - State changed. New State = " + LightstreamerManager.getOpportunityStateDescription(opportunityState));
			handler.obtainMessage(MESSAGE_STATE_CHANGED).sendToTarget();
		}

		//Send Event to the other inner Fragments:
		application.postEvent(new LightstreamerUpdateEvent(market.getId(), update));
	}

	@Override
	public void onStatusChange(int status) {}
	//////////////////////////////////////////////////////////////////////////////////

	//Handles messages on the UI Thread.(Like mesages from the Lighstreamer, which come from another Thread).
	@Override
	public boolean handleMessage(Message msg) {
		switch (msg.what) {
			case MESSAGE_STATE_CHANGED:
				//Suspended:
				if (opportunityState == Opportunity.STATE_CREATED || opportunityState == Opportunity.STATE_SUSPENDED) {
					showUnavailableInfoDynamics();
				}
				//Open, Waiting For Expiry or Expired:
				else{
					showAvailableInfoDynamics();
				}
				break;
		}
		return true;
	}
}
