package com.anyoption.android.app.fragment.trade;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anyoption.android.R;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Market;

import java.util.List;

/**
 * Viewpager showing asset pages.
 * 
 * @author Mario Kutlev
 * 
 */
public class AssetViewPagerFragment extends NavigationalFragment {

	@SuppressWarnings("hiding")
	private static final String TAG = AssetViewPagerFragment.class.getSimpleName();

	private View root;
	private List<Market> markets;
	

	/**
	 * The pager widget, which handles animation and allows swiping horizontally to access previous
	 * and next wizard steps.
	 */
	private ViewPager viewPager;

	/**
	 * The pager adapter, which provides the pages to the view pager widget.
	 */
	private PagerAdapter pagerAdapter;

	public static AssetViewPagerFragment newInstance() {
		AssetViewPagerFragment fragment = new AssetViewPagerFragment();
		return fragment;
	}

	public static AssetViewPagerFragment newInstance(Bundle bundle) {
		AssetViewPagerFragment fragment = new AssetViewPagerFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onResume() {
		super.onResume();

		Log.d(TAG, "onResume");

		if (viewPager != null) {
			Log.d(TAG, "getCurrentItem" + viewPager.getCurrentItem());
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = getArguments();
		if (bundle != null) {
			markets = application.getDataBaseManager().getFavoritesMarkets(application.getUser());
			
			Market market = (Market) bundle.getSerializable(Constants.EXTRA_MARKET);
			if(market == null && bundle.containsKey(Constants.EXTRA_MARKET_ID)) {
				long marketId = bundle.getLong(Constants.EXTRA_MARKET_ID);
				market = application.getMarketForID(marketId);
				if(market == null){
					market = new Market();
					market.setId(marketId);
					market.setOptionPlusMarket(bundle.getLong(Constants.EXTRA_MARKET_PRODUCT_TYPE) == Constants.PRODUCT_TYPE_ID_OPTION_PLUS);
				}
			}
			
			if (market != null) {
				boolean sholdAddToList = true;

				for (int i = 0; i < markets.size(); i++) {
					if (market.getId() == markets.get(i).getId()) {
						sholdAddToList = false;

						if (i != 0) {
							// Move the market in the beginning of the list.
							Market clickedMarket = markets.get(i);
							markets.remove(i);
							markets.add(0, clickedMarket);
						}

						break;
					}
				}

				if (sholdAddToList) {
					markets.add(0, market);
				}

			}
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		root = inflater.inflate(R.layout.asset_view_pager_fragment, container, false);
		viewPager = (ViewPager) root.findViewById(R.id.assets_view_pager);

		pagerAdapter = new ScreenSlidePagerAdapter(getChildFragmentManager());
		viewPager.setAdapter(pagerAdapter);
		viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				Log.d(TAG, "onPageSelected -> Page changed. Market = " + markets.get(position));

				postAssetNavigationEvent(markets.get(position));
			}
		});

		return root;
	}
	
	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	protected Screenable setupScreen() {
		return Screen.ASSET_VIEW_PAGER;
	}

	/**
	 * Posts navigation event when an asset is displayed.
	 * 
	 * @param market
	 */
	private void postAssetNavigationEvent(Market market) {
		Bundle args = new Bundle();
		args.putSerializable(Constants.EXTRA_MARKET, market);
		args.putString(Constants.EXTRA_ACTION_BAR_TITLE, market.getDisplayName());
		args.putLong(Constants.EXTRA_MARKET_PRODUCT_TYPE, market.getProductTypeId());
		boolean isDynamics = market != null && market.getProductTypeId() == Constants.PRODUCT_TYPE_ID_DYNAMICS;
		NavigationEvent event = new NavigationEvent(Screen.ASSET_PAGE, NavigationType.HORIZONTAL,args);
		application.postEvent(event);
	}

	private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

		public ScreenSlidePagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			Log.d(TAG, "getItem -> Market = " + markets.get(position));

			if (position == viewPager.getCurrentItem()) {
				postAssetNavigationEvent(markets.get(position));
				// If this is the first created fragment we want it to load the data when it's
				// created.
				return getAssetPage(position, true);
			}

			// We don't want the page to be loaded before the user opens the page.
			return getAssetPage(position, false);
		}

		@Override
		public int getCount() {
			return markets.size();
		}

		@SuppressWarnings("unchecked")
		private BaseFragment getAssetPage(int position, boolean autoload) {
			Market market = markets.get(position);
			Bundle args = AssetPageFragment.createArgsBundle(market, autoload);

			//Check if the Market is Dynamics Market:
			boolean isDynamics = market.getProductTypeId() == Constants.PRODUCT_TYPE_ID_DYNAMICS;
			if(isDynamics){
				return BaseFragment.newInstance(application.getSubClassForClass(AssetPageDynamicsFragment.class), args);
			}else{
				return BaseFragment.newInstance(application.getSubClassForClass(AssetPageFragment.class), args);
			}
		}

	}


	@Override
	protected void refreshUser() {
		//Do nothing
	}
}
