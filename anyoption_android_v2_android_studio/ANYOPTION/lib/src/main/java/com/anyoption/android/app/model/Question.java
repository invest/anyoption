package com.anyoption.android.app.model;

public class Question {
	
	private long id;
	private String description;
	private Answer[] answers;
	private QuestionType questionType;
	private Answer validationAnswer;
	private boolean mandatory;

	public enum QuestionType {
		POPUP, POPUP_WITH_TEXT_FIELD, CHECKBOX, TEXT_FIELD
	}
	
	public Question(){}

	public Question(long id, String description, QuestionType questionType, Answer...answers){
		this.id = id;
		this.description = description;
		this.answers = answers;
		this.questionType = questionType;
		for(Answer answer : answers){
			answer.setQuestion(this);
		}
	}

	public Question(long id, String description, QuestionType questionType, Answer validationAnswer, boolean mandatory, Answer...answers){
		this.id = id;
		this.description = description;
		this.answers = answers;
		this.questionType = questionType;
		this.validationAnswer = validationAnswer;
		this.mandatory = mandatory;
		for(Answer answer : answers){
			answer.setQuestion(this);
		}
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Answer[] getAnswers() {
		return answers;
	}
	public void setAnswers(Answer[] answers) {
		this.answers = answers;
	}

	public QuestionType getQuestionType() {
		return questionType;
	}

	public void setQuestionType(QuestionType questionType) {
		this.questionType = questionType;
	}

	public Answer getValidationAnswer() {
		return validationAnswer;
	}

	public void setValidationAnswer(Answer validationAnswer) {
		this.validationAnswer = validationAnswer;
	}

	public boolean isMandatory() {
		return mandatory;
	}

	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	public int getAnswerPosition(Answer answer){
		int position = -1;
		if(answer != null){
			for(int i=0; i<answers.length; i++){
				Answer a = answers[i];
				if(a.getId() == answer.getId()){
					position = i;
					break;
				}
			}
		}
		return position;
	}
	
	public Answer getAnswerForId(Long id){
		Answer answer = null;
		if(id != null){
			for (int i = 0; i < answers.length; i++) {
				Answer a = answers[i];
				if (a.getId() == id) {
					answer = a;
					break;
				}
			}
		}
		return answer;
	}
}
