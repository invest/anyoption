package com.anyoption.android.app.fragment.trade;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application.LayoutDirection;
import com.anyoption.android.app.activity.MainActivity;
import com.anyoption.android.app.event.AssetInfoDrawerEvent;
import com.anyoption.android.app.event.AssetMarketInfoEvent;
import com.anyoption.android.app.event.ChartEvent;
import com.anyoption.android.app.event.InvestmentEvent;
import com.anyoption.android.app.manager.CommunicationManager;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.MarketsUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.SlidingDrawer;
import com.anyoption.common.beans.base.ExpiryFormulaCalculation;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.MarketAssetIndex;
import com.anyoption.common.beans.base.MarketAssetIndexInfo;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.service.assetindex.AssetIndexMarketsResult;
import com.anyoption.common.service.assetindex.MarketRequest;
import com.anyoption.json.requests.PastExpiresMethodRequest;
import com.anyoption.json.results.OpportunitiesMethodResult;

public class AssetInfoFragment extends BaseAssetNestedFragment implements SlidingDrawer.OnDrawerOpenListener, SlidingDrawer.OnDrawerCloseListener {

    public static final String TAG = AssetInfoFragment.class.getSimpleName();

    private CommunicationManager communicationManager;

    private View root;
    private SlidingDrawer drawer;

    private ImageView smallIcon;
    private TextView symbol;
    private ImageView symbolPlusIcon;
    private TextView expiryType;
    private TextView reutersFields;
    private TextView expiryFormula;

    private TextView assetDetailsView;
    private ImageView assetIcon;
    private MarketAssetIndex marketAssetIndex;

    /**
     * Use this method to create new instance of the fragment.
     *
     * @return
     */
    public static AssetInfoFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new AssetInfoFragment.");
        AssetInfoFragment fragment = new AssetInfoFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    /**
     * Create new instance of this Fragment.
     *
     * @param market
     * @param autoLoad if true, the fragment will start loading the fragment immediately. Otherwise it
     *                 will load the data when {@link #loadData()} method is called.
     * @return
     */
    public static AssetInfoFragment newInstance(Market market, boolean autoLoad) {
        Bundle args = new Bundle();
        args.putSerializable(Constants.EXTRA_MARKET, market);
        args.putBoolean(Constants.EXTRA_AUTO_LOAD_MARKET, autoLoad);
        return newInstance(args);
    }

    /**
     * Create new instance of this Fragment.
     *
     * @param market
     * @return
     */
    public static AssetInfoFragment newInstance(Market market) {
        return newInstance(market, true);
    }

    public AssetInfoFragment() {
        // Empty constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        communicationManager = application.getCommunicationManager();

        getMarketInfoFromServer();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (market != null) {
            registerForEvents();
        }

        if(isDrawerOpen()){
            drawerGotOpened();
        }else{
            drawerGotClosed();
        }
    }

    private void registerForEvents() {
        application.registerForEvents(this, InvestmentEvent.class, ChartEvent.class, AssetInfoDrawerEvent.class);
    }

    @Override
    public void onPause() {
        application.unregisterForAllEvents(this);
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        application.unregisterForAllEvents(this);
        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.asset_info_fragment, container, false);

        smallIcon = (ImageView) root.findViewById(R.id.asset_info_small_icon);

        TextView symbolLabel = (TextView) root.findViewById(R.id.asset_info_symbol_label);
        symbolLabel.setText(getString(R.string.symbol) + ":");

        symbol = (TextView) root.findViewById(R.id.asset_info_symbol);
        symbolPlusIcon = (ImageView) root.findViewById(R.id.asset_info_symbol_plus_icon);

        assetIcon = (ImageView) root.findViewById(R.id.asset_info_asset_icon);
        expiryType = (TextView) root.findViewById(R.id.asset_info_expiry_type);

        TextView reutersFieldLabel = (TextView) root.findViewById(R.id.asset_info_reuters_field_label);
        reutersFieldLabel.setText(Html.fromHtml(getString(R.string.reutersField)).toString() + ":");

        reutersFields = (TextView) root.findViewById(R.id.asset_info_reuters_field);
        expiryFormula = (TextView) root.findViewById(R.id.asset_info_expiry_formula);
        assetDetailsView = (TextView) root.findViewById(R.id.asset_info_asset_details);
        updateIcons();

        drawer = (SlidingDrawer) root.findViewById(R.id.asset_info_drawer);
        drawer.setOnDrawerCloseListener(this);
        drawer.setOnDrawerOpenListener(this);

        if(market != null && marketAssetIndex != null){
            setupMarketInfo();
        }

        return root;
    }

    private void getMarketInfoFromServer(){
        if(market != null){
            MarketRequest request = new MarketRequest();
            request.setMarketId(market.getId());
            communicationManager.requestService(this, "getMarketInfoFromServerCallback", Constants.SERVICE_GET_ASSET_INFO, request, AssetIndexMarketsResult.class, false, false);
        }
    }

    public void getMarketInfoFromServerCallback(Object resObj){
        AssetIndexMarketsResult result = (AssetIndexMarketsResult) resObj;
        if(result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS){
            marketAssetIndex = result.getMarketList().get(0);
            application.postEvent(new AssetMarketInfoEvent(marketAssetIndex));

            if(assetDetailsView != null){
                setupMarketInfo();
            }
        }
    }

    public void setupMarketInfo() {
        if(market != null && marketAssetIndex != null) {

                MarketAssetIndexInfo marketAssetIndexInfo = marketAssetIndex.getMarketAssetIndexInfo();
                Market market = marketAssetIndex.getMarket();

                //Set Market description:
                assetDetailsView.setText(marketAssetIndex.getMarketAssetIndexInfo().getMarketDescription());

                //Set asset trading time:
                TextView tradingTimeView = (TextView) root.findViewById(R.id.asset_info_trading_time);
                tradingTimeView.setText(MarketsUtils.getTradingTime(marketAssetIndex));

                //Set Symbol, Expiry Type, Reuters Field and Formula , based on the Expiry Type(Hourly, Daily, Weekly, Monthly):
                StringBuilder reutersFieldStringBuilder = new StringBuilder();
                StringBuilder expiryTypesStringBuilder  = new StringBuilder();
                StringBuilder formulaStringBuilder      = new StringBuilder();

                if (market.isHaveHourly()) {
                    ExpiryFormulaCalculation expiryFormulaCalculationHOURLY = MarketsUtils.getExpiryFormulaCalculation(marketAssetIndex, MarketsUtils.EXPIRY_TYPE_HOURLY);
                    if (expiryFormulaCalculationHOURLY != null) {
                        String formula = expiryFormulaCalculationHOURLY.getExpiryFormulaTxt();
                        String hourly = getString(R.string.marketIndexScheduledHourly);

                        reutersFieldStringBuilder.append(hourly).append(" : ").append(expiryFormulaCalculationHOURLY.getReutersField());

                        expiryTypesStringBuilder.append(hourly);

                        if (formula != null) {
                            formulaStringBuilder.append("\n").append(hourly).append(" : ").append(formula);
                        }
                    }
                }
                if (market.isHaveDaily()) {
                    ExpiryFormulaCalculation expiryFormulaCalculationDAILY = MarketsUtils.getExpiryFormulaCalculation(marketAssetIndex, MarketsUtils.EXPIRY_TYPE_DAILY);
                    if (expiryFormulaCalculationDAILY != null) {
                        String formula = expiryFormulaCalculationDAILY.getExpiryFormulaTxt();

                        String daily = getString(R.string.marketIndexScheduledDaily);

                        if (expiryFormulaCalculationDAILY.getReutersField() != null) {
                            if (reutersFieldStringBuilder.length() > 0) {
                                reutersFieldStringBuilder.append("\n");
                            }
                            reutersFieldStringBuilder.append(daily).append(" : ").append(expiryFormulaCalculationDAILY.getReutersField());

                        }

                        if (expiryTypesStringBuilder.length() > 0) {
                            expiryTypesStringBuilder.append("/");
                        }
                        expiryTypesStringBuilder.append(daily);

                        if (formula != null) {
                            if (!formulaStringBuilder.toString().contains(formula)) {
                                if (formulaStringBuilder.length() > 0) {
                                    formulaStringBuilder.append("\n\n");
                                }
                                formulaStringBuilder.append(daily).append(" : ").append(formula);
                            }
                        }
                    }
                }
                if (market.isHaveWeekly() || market.isHaveMonthly()) {
                    ExpiryFormulaCalculation expiryFormulaCalculationLONGTERM = MarketsUtils.getExpiryFormulaCalculation(marketAssetIndex, MarketsUtils.EXPIRY_TYPE_LONG_TERM);
                    if (expiryFormulaCalculationLONGTERM != null) {
                        String formula = expiryFormulaCalculationLONGTERM.getExpiryFormulaTxt();

                        String longTerm = getString(R.string.marketIndexScheduledWeekly) + ", " + getString(R.string.marketIndexScheduledMonthly);

                        if(expiryFormulaCalculationLONGTERM.getReutersField() != null){
                            if (reutersFieldStringBuilder.length() > 0) {
                                reutersFieldStringBuilder.append("\n");
                            }
                            reutersFieldStringBuilder.append(longTerm).append(" : ").append(expiryFormulaCalculationLONGTERM.getReutersField());
                        }

                        if (expiryTypesStringBuilder.length() > 0) {
                            expiryTypesStringBuilder.append("/");
                        }
                        expiryTypesStringBuilder.append(longTerm);

                        if (formula != null) {
                            if (!formulaStringBuilder.toString().contains(formula)) {
                                if (formulaStringBuilder.length() > 0) {
                                    formulaStringBuilder.append("\n\n");
                                }
                                formulaStringBuilder.append(longTerm).append(" : ").append(formula);
                            }
                        }
                    }
                }
                /////////////////////////////////////////

                //Set the Symbol:
                symbol.setText(reutersFieldStringBuilder.toString());

                //Set the Reuters Field:
                reutersFields.setText(reutersFieldStringBuilder.toString());

                //Set Expiry Type:
                expiryType.setText(expiryTypesStringBuilder.toString());

                //Set Formula:
                expiryFormula.setText(formulaStringBuilder.toString());
        } else {
            Log.e(TAG, "There is no market!");
        }
    }

    @Override
    public void loadData() {
        if (market != null) {
            getMarketInfoFromServer();
            callInitRequests();
        }
    }

    @Override
    protected void onMarketChanged() {
        super.onMarketChanged();

        if (isAutoLoad) {
            loadData();
        }

        if (!application.isRegistered(this)) {
            registerForEvents();
        }

        updateIcons();
        setupMarketInfo();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////                                   EVENTs                                          ////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void onEventMainThread(InvestmentEvent event) {
        Log.d(TAG, "InvestmentEvent");
        if (market != null && event.getMarketId() == market.getId() && event.getType() == com.anyoption.android.app.event.InvestmentEvent.Type.INVEST_SUCCESS) {
            if (smallIcon != null) {
                updateIcons();
            }
        }
    }

    public void onEventMainThread(AssetInfoDrawerEvent event) {
        Log.d(TAG, "AssetInfoDrawerEvent");
        if(drawer != null){
            if(event.isShouldOpen()){
                openDrawer();
            }else {
                closeDrawer();
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////                           REQUESTs and RESPONSEs                                  ////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void callInitRequests() {
        // Request past expires
        PastExpiresMethodRequest pastExpRequest = new PastExpiresMethodRequest();
        pastExpRequest.setMarketId(market.getId());
        pastExpRequest.setPageSize(3);
        pastExpRequest.setStartRow(0);
        pastExpRequest.setDate(null);
        communicationManager.requestService(this, "pastExpireCallBack", Constants.SERVICE_GET_PAST_EXPIRIES, pastExpRequest, OpportunitiesMethodResult.class, false, false);
    }

    public void pastExpireCallBack(Object resultObj) {
        if (!isAdded()) {
            return;
        }

        OpportunitiesMethodResult opportunitiesResult = (OpportunitiesMethodResult) resultObj;

        if (opportunitiesResult != null && opportunitiesResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            Opportunity[] oppResult = opportunitiesResult.getOpportunities();

            StringBuilder recentExpires = new StringBuilder();
            for (int i = 0; i < oppResult.length; i++) {
                Opportunity opportunity = oppResult[i];

                recentExpires.append(getString(R.string.pastExpireItemHtml, opportunity.getTimeEstClosingTxt().split(" ")[0], opportunity.getCloseLevelTxt()));
                if (i < oppResult.length - 1) {
                    recentExpires.append(" | ");
                }
            }

            TextView recentExpiresView = (TextView) root.findViewById(R.id.asset_info_past_expiries);
            recentExpiresView.setText(Html.fromHtml(recentExpires.toString()));

            // Scrolls the view to right
            if (application.getLayoutDirection() == LayoutDirection.RIGHT_TO_LEFT) {
                final HorizontalScrollView scrollView = (HorizontalScrollView) root.findViewById(R.id.asset_info_past_expiries_scroll);
                scrollView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        scrollView.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                    }
                }, 100L);
            }
        } else {
            if (opportunitiesResult != null) {
                Log.e(TAG, "pastExpireCallBack -> Could not get past expires! ErrorCode: " + opportunitiesResult.getErrorCode());
            } else {
                Log.e(TAG, "pastExpireCallBack -> Could not get past expires! The result is null!");
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void updateIcons() {
        // If market is null, the default icon is used.
        assetIcon.setImageDrawable(DrawableUtils.getMarketIcon(market));

        if (market != null) {
            boolean isOptionPlus = MarketsUtils.isOptionPlus(market);
            boolean isDynamics   = MarketsUtils.isDynamics(market);

            if (isOptionPlus) {
                smallIcon.setImageResource(R.drawable.option_plus_icon_1);
                symbolPlusIcon.setVisibility(View.VISIBLE);
            }else if (application.getDataBaseManager().isMarketFavorite(application.getUser(), market)) {
                smallIcon.setVisibility(View.VISIBLE);
                smallIcon.setImageResource(R.drawable.star_small_blue);
                symbolPlusIcon.setVisibility(View.GONE);
            } else {
                smallIcon.setVisibility(View.GONE);
                symbolPlusIcon.setVisibility(View.GONE);
            }
        }

    }

    public void openDrawer() {
        drawer.open();
    }

    public void closeDrawer() {
        drawer.close();
    }

    public boolean isDrawerOpen(){
        return drawer.isOpened();
    }

    private void drawerGotOpened(){
        ((MainActivity)application.getCurrentActivity()).setMarketInfoDrawerOpen(true);
    }

    private void drawerGotClosed(){
        ((MainActivity)application.getCurrentActivity()).setMarketInfoDrawerOpen(false);
    }

    @Override
    public void onDrawerOpened() {
        drawerGotOpened();
    }

    @Override
    public void onDrawerClosed() {
        drawerGotClosed();
    }
}
