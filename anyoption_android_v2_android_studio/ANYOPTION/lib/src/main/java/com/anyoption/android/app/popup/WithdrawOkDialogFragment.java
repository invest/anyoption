package com.anyoption.android.app.popup;

import com.anyoption.android.R;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

/**
 * A POPUP that is shown when the user attempts to make a withdrawal that is lower than a MIN amount. 
 * @author Anastas Arnaudov
 *
 */
public class WithdrawOkDialogFragment extends BaseDialogFragment{

	public static final String TAG = WithdrawOkDialogFragment.class.getSimpleName();

	/**
	 * Use this method to create new instance of the fragment.
	 * @return
	 */
	public static WithdrawOkDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new WithdrawOkDialogFragment.");
		WithdrawOkDialogFragment fragment = new WithdrawOkDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		fragment.setArguments(bundle);
		return fragment;
	}

	protected String amount;
	protected String fee;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if(args != null){
			amount = args.getString(Constants.EXTRA_AMOUNT);
			fee = args.getString(Constants.EXTRA_FEE);
		}
	}
	
	@Override
	protected void onCreateContentView(View contentView) {
		TextView message_1_TextView = (TextView) contentView.findViewById(R.id.withdraw_ok_popup_message_1_text_view);
		TextView message_2_TextView = (TextView) contentView.findViewById(R.id.withdraw_ok_popup_message_2_text_view);
		
		message_1_TextView.setText((getString(R.string.withdrawOKPopupMessage_1, amount)));
		message_2_TextView.setText((getString(R.string.withdrawOKPopupMessage_2, fee)));

		TextUtils.decorateInnerText(message_1_TextView, null, message_1_TextView.getText().toString(), amount, Font.ROBOTO_MEDIUM, 0, 0);
		TextUtils.decorateInnerText(message_2_TextView, null, message_2_TextView.getText().toString(), fee, Font.ROBOTO_MEDIUM, 0, 0);
	}

	@Override
	protected int getContentLayout() {
		return R.layout.popup_withdraw_ok;
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);
		application.getCurrentActivity().onBackPressed();
	}
}
