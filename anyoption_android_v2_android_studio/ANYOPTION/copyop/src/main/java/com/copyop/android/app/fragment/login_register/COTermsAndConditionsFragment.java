package com.copyop.android.app.fragment.login_register;

import com.anyoption.android.app.fragment.login_register.TermsAndConditionsFragment;
import com.copyop.android.app.util.COConstants;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;

public class COTermsAndConditionsFragment extends TermsAndConditionsFragment {

	/**
	 * Use this method to get new instance of the fragment.
	 * 
	 * @return
	 */
	public static COTermsAndConditionsFragment newInstance(Bundle bundle) {
		COTermsAndConditionsFragment termsAndConditionsFragment = new COTermsAndConditionsFragment();
		termsAndConditionsFragment.setArguments(bundle);
		return termsAndConditionsFragment;
	}

	@Override
	protected String getTermsUrl(long skinId, long currCountryId) {
		return COConstants.TERMS_LINK + "?fromApp=true&s=" + skinId + "&cid=" + currCountryId
				+ "&version=2-mobile";
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = super.onCreateView(inflater, container, savedInstanceState);

		webView.setInitialScale(1);
		webView.getSettings().setLoadWithOverviewMode(true);
		webView.getSettings().setUseWideViewPort(true);
		webView.getSettings().setJavaScriptEnabled(true);

		return rootView;
	}
}
