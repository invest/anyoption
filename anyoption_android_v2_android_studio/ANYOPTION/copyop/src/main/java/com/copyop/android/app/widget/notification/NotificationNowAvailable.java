package com.copyop.android.app.widget.notification;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COConstants;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NotificationNowAvailable extends LinearLayout{
	
	private String message;
	private long userId;
	
	public NotificationNowAvailable(String message, long userId) {
		super(Application.get());
		this.message = message;
		this.userId = userId;
		init(Application.get(), null, 0);
	}
	public NotificationNowAvailable(Context context) {
		super(context);
		init(context, null, 0);
	}
	public NotificationNowAvailable(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	public NotificationNowAvailable(Context context, AttributeSet attrs,int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		View view = View.inflate(context, R.layout.notification_now_available, this);
		TextView messageTextView = (TextView) view.findViewById(R.id.notification_message_text_view);
		messageTextView.setText(message);
		Button tradeButton = (Button) view.findViewById(R.id.notification_show_off_button);
		tradeButton.setOnClickListener(new OnClickListener() {
			private boolean isPressed = false;
			@Override
			public void onClick(View v) {
				if(!isPressed){
					isPressed = true;
					goToProfile(userId);
				}
			}
		});
	}
	
	private void goToProfile(long profileId){
		Bundle bundle = new Bundle();
		bundle.putLong(COConstants.EXTRA_PROFILE_ID, profileId);
		Application.get().postEvent(new NavigationEvent(COScreen.USER_PROFILE, NavigationType.INIT, bundle));
	}

}
