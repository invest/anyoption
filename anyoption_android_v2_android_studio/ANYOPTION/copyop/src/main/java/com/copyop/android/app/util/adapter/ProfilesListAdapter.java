package com.copyop.android.app.util.adapter;

import java.util.ArrayList;
import java.util.List;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.manager.FragmentManager;
import com.anyoption.android.app.popup.BaseDialogFragment.OnDialogDismissListener;
import com.anyoption.android.app.util.BitmapUtils;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.manager.COPopUpManager;
import com.copyop.android.app.popup.CopyDialogFragment;
import com.copyop.android.app.popup.CopyDialogFragment.OnCopyStateChangedListener;
import com.copyop.android.app.popup.WatchDialogFragment;
import com.copyop.android.app.popup.WatchDialogFragment.OnWatchStateChangedListener;
import com.copyop.android.app.util.COAmountUtil;
import com.copyop.android.app.util.COBitmapUtils;
import com.copyop.android.app.util.COConstants;
import com.copyop.android.app.widget.CopyButton.CopyButtonState;
import com.copyop.android.app.widget.CopySmallButton;
import com.copyop.android.app.widget.WatchSmallButton;
import com.copyop.android.app.widget.WatchSmallButton.WatchButtonState;
import com.copyop.common.dto.base.Profile;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ProfilesListAdapter extends BaseAdapter implements OnClickListener,
		OnDialogDismissListener, OnCopyStateChangedListener, OnWatchStateChangedListener {

	public static final String TAG = ProfilesListAdapter.class.getSimpleName();

	private List<? extends Profile> profiles;

	private Application application;
	private FragmentManager fragmentManager;
	private COPopUpManager popUpManager;
	private LayoutInflater layoutInflater = null;

	private boolean isDialogShown;
	private long loggedInUserId;

	private boolean isProfitLossShown;
	
	public ProfilesListAdapter(List<Profile> profiles) {
		this.profiles = profiles;

		application = Application.get();
		fragmentManager = application.getFragmentManager();
		popUpManager = (COPopUpManager) application.getPopUpManager();
		layoutInflater = (LayoutInflater) application
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		isDialogShown = false;
		loggedInUserId = application.getUser().getId();
	}

	public ProfilesListAdapter() {
		this(new ArrayList<Profile>());
	}

	@Override
	public int getCount() {
		return profiles.size();
	}

	@Override
	public Object getItem(int position) {
		return profiles.get(position);
	}

	@Override
	public long getItemId(int position) {
		return profiles.get(position).getUserId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View itemView = convertView;
		if (itemView == null) {
			itemView = layoutInflater.inflate(R.layout.profile_item_layout, parent, false);
		}

		Profile profile = profiles.get(position);

		ImageView avatar = (ImageView) itemView.findViewById(R.id.profile_item_icon);
		avatar.setImageDrawable(null);
		if (profile.getAvatar() != null) {
			COBitmapUtils.loadBitmap(profile.getAvatar(), avatar);
		}

		TextView nickname = (TextView) itemView.findViewById(R.id.profile_item_nickname);
		nickname.setText(profile.getNickname());
		TextView hit = (TextView) itemView.findViewById(R.id.profile_item_hit);
		hit.setText(String.valueOf(profile.getHitRatePercentage()));
		ImageView costImageView = (ImageView) itemView.findViewById(R.id.profile_item_cost_earn_image_view);
		TextView costTextView = (TextView) itemView.findViewById(R.id.profile_item_cost_earn_text_view);
		
		CopySmallButton copyButton = (CopySmallButton) itemView
				.findViewById(R.id.profile_item_copy_button);
		WatchSmallButton watchButton = (WatchSmallButton) itemView
				.findViewById(R.id.profile_item_watch_button);

		double costEarnedAmount = profile.getCostEarned();
		if(costEarnedAmount < 0){
			BitmapUtils.loadBitmap(R.drawable.profile_list_cost_icon, costImageView);
			costTextView.setText(application.getString(R.string.amountFromLoss, COAmountUtil.formatCostEarnedAmount(costEarnedAmount)));
		}else if(costEarnedAmount >= 0){
			BitmapUtils.loadBitmap(R.drawable.profile_list_earned_icon, costImageView);
			costTextView.setText(application.getString(R.string.amountFromEarned, COAmountUtil.formatCostEarnedAmount(costEarnedAmount)));
		}
		if(isProfitLossShown){
			costImageView.setVisibility(View.VISIBLE);
			costTextView.setVisibility(View.VISIBLE);
		}else{
			costImageView.setVisibility(View.GONE);
			costTextView.setVisibility(View.GONE);
		}
		
		if (profile.getUserId() == loggedInUserId) {
			// Hide the buttons
			copyButton.setVisibility(View.GONE);
			watchButton.setVisibility(View.GONE);
		} else {
			copyButton.setVisibility(View.VISIBLE);
			watchButton.setVisibility(View.VISIBLE);
			
			if (profile.isFrozen() && !profile.isCopying()) {
				copyButton.setState(CopyButtonState.STATE_FROZEN);
			} else if (profile.isCopying()) {
				copyButton.setState(CopyButtonState.STATE_COPYING);
			} else {
				copyButton.setState(CopyButtonState.STATE_NORMAL);
			}
			copyButton.setTag(position);
			copyButton.setOnClickListener(this);

			if (profile.isCopying() || profile.isWatching()) {
				watchButton.setState(WatchButtonState.STATE_WATCHING);
			} else {
				watchButton.setState(WatchButtonState.STATE_NORMAL);
			}
			watchButton.setTag(position);
			watchButton.setOnClickListener(this);
		}


		
		return itemView;
	}

	@Override
	public void onClick(View v) {
		if (isDialogShown) {
			return;
		}

		if (v instanceof CopySmallButton) {
			int profilePosition = (Integer) v.getTag();
			Profile p = profiles.get(profilePosition);
			if (p.isFrozen() && !p.isCopying()) {
				showDozedOffDialog();
			} else {
				showCopyDialog(p);
			}
		} else if (v instanceof WatchSmallButton) {
			int profilePosition = (Integer) v.getTag();
			showWatchDialog(profiles.get(profilePosition));
		}
	}

	@Override
	public void onDismiss() {
		isDialogShown = false;
	}

	@Override
	public void onCopyStateChanged(long userId, boolean isCopying) {
		Profile profile = getProfileById(userId);

		if (isCopying) {
			profile.setCopying(true);
			profile.setWatching(true);
		} else {
			profile.setCopying(false);
			profile.setWatching(false);
		}

		notifyDataSetChanged();
	}

	@Override
	public void onWatchStateChanged(long userId, boolean isWatching) {
		Profile profile = getProfileById(userId);

		if (isWatching) {
			profile.setWatching(true);
		} else {
			profile.setWatching(false);
		}

		notifyDataSetChanged();
	}

	private void showCopyDialog(Profile profile) {
		if((!profile.isCopying() && RegulationUtils.checkRegulationAndPepAndThresholdStatusAndDoNecessary(false)) || profile.isCopying()) {
			isDialogShown = true;

			if (!profile.isCopying() && profile.getCopiers() >= COApplication.get().getMaxCopiersPerAccount()) {
				popUpManager.showFullyCopiedPopUp(profile.getUserId(), profile.getNickname(),
						profile.isWatching(), this, this);
				return;
			}

			Bundle args = new Bundle();
			args.putBoolean(COConstants.EXTRA_IS_COPYING, profile.isCopying());
			args.putBoolean(COConstants.EXTRA_IS_FROZEN, profile.isFrozen());
			args.putLong(COConstants.EXTRA_PROFILE_ID, profile.getUserId());
			args.putString(COConstants.EXTRA_NICKNAME, profile.getNickname());

			CopyDialogFragment dialog = (CopyDialogFragment) fragmentManager.showDialogFragment(
					CopyDialogFragment.class, args);
			dialog.setOnDismissListener(this);
			dialog.setOnCopyStateChangedListener(this);
		}
	}

	private void showWatchDialog(Profile profile) {
		isDialogShown = true;

		if (profile.isCopying()) {
			popUpManager.showCanNotUnwatchPopUp(profile.getNickname(), this);
			return;
		}

		Bundle args = new Bundle();
		args.putBoolean(COConstants.EXTRA_IS_WATCHING, profile.isWatching());
		args.putBoolean(COConstants.EXTRA_IS_COPYING, profile.isCopying());
		args.putLong(COConstants.EXTRA_PROFILE_ID, profile.getUserId());
		args.putString(COConstants.EXTRA_NICKNAME, profile.getNickname());

		WatchDialogFragment dialog = (WatchDialogFragment) fragmentManager.showDialogFragment(
				WatchDialogFragment.class, args);
		dialog.setOnDismissListener(this);
		dialog.setOnWatchStateChangedListener(this);
	}

	private void showDozedOffDialog() {
		isDialogShown = true;
		popUpManager.showDozedOffPopUp(this);
	}

	public List<? extends Profile> getProfiles() {
		return profiles;
	}

	public void setProfiles(List<? extends Profile> profiles) {
		this.profiles = profiles;

		for (int i = 0; i < profiles.size(); i++) {
			if (profiles.get(i).isCopying()) {
				profiles.get(i).setWatching(true);
			}
		}
	}

	private Profile getProfileById(long id) {
		for (Profile profile : profiles) {
			if (profile.getUserId() == id) {
				return profile;
			}
		}

		return null;
	}

	public void showProfile(int position) {
		Profile profile = profiles.get(position);

		Bundle args = new Bundle();
		args.putLong(COConstants.EXTRA_PROFILE_ID, profile.getUserId());
		args.putString(Constants.EXTRA_ACTION_BAR_TITLE, profile.getNickname());
		application
				.postEvent(new NavigationEvent(COScreen.USER_PROFILE, NavigationType.DEEP, args));
	}

	public boolean isProfitLossShown() {
		return isProfitLossShown;
	}

	public void setProfitLossShown(boolean isProfitLossShown) {
		this.isProfitLossShown = isProfitLossShown;
	}

}