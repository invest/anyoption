package com.copyop.android.app.fragment.walkthrough;

import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.TextUtils;
import com.copyop.android.app.R;
import com.copyop.android.app.event.HideMenuWalkthroughEvent;
import com.copyop.android.app.util.COConstants;

public class MenuWalkthroughDialog extends BaseDialogFragment implements OnClickListener {

	private static final String TAG = MenuWalkthroughDialog.class.getSimpleName();

	private View gotIt;

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static MenuWalkthroughDialog newInstance(Bundle bundle) {
		Log.d(TAG, "Created new MenuWalkthroughDialog.");
		MenuWalkthroughDialog fragment = new MenuWalkthroughDialog();
		bundle.putInt(ARG_BACKGROUND_COLOR_KEY, 0);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		fragment.setArguments(bundle);

		return fragment;
	}

	@Override
	protected void onCreateContentView(View contentView) {
		super.onCreateContentView(contentView);

		gotIt = contentView.findViewById(R.id.menu_walkthrough_got_it);
		gotIt.setOnClickListener(this);

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
			setCancelable(false);
	}

	@Override
	protected int getContentLayout() {
		return R.layout.menu_walkthrough_dialog;
	}


	@Override
	public void onClick(View v) {
		if (v.getId() == gotIt.getId()) {
			finishWalkthrough();
		}
	}

	private void finishWalkthrough() {
		application.getSharedPreferencesManager().putBoolean(
				COConstants.PREFERENCES_IS_MENU_WALKTHROUGH_SHOWN, true);
		application.postEvent(new HideMenuWalkthroughEvent());
		dismiss();
	}



}
