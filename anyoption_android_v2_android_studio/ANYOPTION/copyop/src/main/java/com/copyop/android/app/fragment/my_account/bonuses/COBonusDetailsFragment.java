package com.copyop.android.app.fragment.my_account.bonuses;



import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.fragment.my_account.bonuses.BonusDetailsFragment;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.requests.BonusMethodRequest;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class COBonusDetailsFragment extends BonusDetailsFragment {

	private static final String GSON_DATE_FORMAT = "MMM d, yyyy hh:mm:ss aaa";
	
	public static COBonusDetailsFragment newInstance(Bundle bundle) {
		COBonusDetailsFragment cOBonusDetailsFragment = new COBonusDetailsFragment();
		cOBonusDetailsFragment.setArguments(bundle);
		return cOBonusDetailsFragment;
	}

	private TextView acceptButton;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = super.onCreateView(inflater, container, savedInstanceState);

		acceptButton = (TextView) rootView.findViewById(R.id.bonus_details_screen_accept_button);
		View waiveButton = rootView.findViewById(R.id.buttonWaiveBonus);
	    if (bonus.getBonusStateId() == Constants.BONUS_STATE_PANDING) {
	    	acceptButton.setVisibility(View.VISIBLE);
	    	waiveButton.setVisibility(View.GONE);
            rootView.findViewById(R.id.buttonWaiveBonus).setOnClickListener(this);            
        }
		
		acceptButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				acceptButton.setEnabled(false);
				updateBonus();
			}
		});
		return rootView;
	}

	@Override
	protected void setupBonusState(BonusMethodRequest request) {
		if (bonus.getBonusStateId() == Constants.BONUS_STATE_PANDING) {
			request.setBonusStateId(Constants.BONUS_STATE_PANDING);
		} else {
			super.setupBonusState(request);
		}
	}

	@Override
	public void updateBonusCallBack(Object result) {
		acceptButton.setEnabled(true);
		MethodResult methodResult = (MethodResult) result;
		if (bonus.getBonusStateId() == Constants.BONUS_STATE_PANDING) {
	        if (methodResult != null && methodResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
				handleClaimedBonus();
			}else{
	        	ErrorUtils.handleResultError(methodResult);
	        }
		}else{
			super.updateBonusCallBack(result);
		}
	}

	private void handleClaimedBonus(){
		Log.d(TAG, "Checking if the user has got the Bonus through a Bonus campaign.");
		if(application.getDeepLinksManager().getMarketingPage() != null){
			Log.d(TAG, "The user has got the Bonus through a Bonus campaign.");
			application.postEvent(new NavigationEvent(Application.Screen.TRADE, NavigationEvent.NavigationType.INIT));
		}else{
			Log.d(TAG, "It was NOT a Bonus campaign.");
			acceptButton.setEnabled(false);
			application.getCurrentActivity().onBackPressed();
		}
	}

    protected void getBonusDescription() {
    	Gson gson = new GsonBuilder().setDateFormat(GSON_DATE_FORMAT).create();
		String jsonBonus = gson.toJson(bonus);
		String jsonBonusFactor = gson.toJson(bonusFactor);
		jsonBonus = jsonBonus.substring(0, jsonBonus.length()-1) + ",\"cashBalance\":" + cashBalance + "}";
		String url = COConstants.BONUS_LINK + "?s=" + application.getSkinId() + "&data=" + jsonBonus + "&turnoverFactor=" + jsonBonusFactor;
    	webView.loadUrl(url);
    }
	
}
