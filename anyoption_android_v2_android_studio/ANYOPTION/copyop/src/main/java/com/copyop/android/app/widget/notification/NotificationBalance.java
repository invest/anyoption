package com.copyop.android.app.widget.notification;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.widget.TextView;
import com.copyop.android.app.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

public class NotificationBalance extends LinearLayout{

	private String message;
	
	public NotificationBalance(String message) {
		super(Application.get());
		this.message = message;
		init(Application.get(), null, 0);
	}
	public NotificationBalance(Context context) {
		super(context);
		init(context, null, 0);
	}
	public NotificationBalance(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	public NotificationBalance(Context context, AttributeSet attrs,int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		View view = View.inflate(context, R.layout.notification_balance_error_layout, this);
		TextView messagetextView = (TextView) view.findViewById(R.id.notification_balance_error_text_view);
		messagetextView.setText(message);
		View depositButton = view.findViewById(R.id.notification_balance_error_deposit_button);
		depositButton.setOnClickListener(new OnClickListener() {
			private boolean isPressed = false;
			@Override
			public void onClick(View v) {
				if(!isPressed){
					isPressed = true;
					Application.get().postEvent(new NavigationEvent(Screen.DEPOSIT_MENU,NavigationType.INIT));
				}
			}
		});
	}

}
