package com.copyop.android.app.popup;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.popup.PopupWithAnchor;
import com.copyop.android.app.R;
import com.copyop.android.app.event.InboxReadingEvent;
import com.copyop.android.app.util.CODrawableUtils;
import com.copyop.android.app.widget.inbox.InboxView;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.PopupWindow;
import android.widget.RelativeLayout.LayoutParams;

public class InboxPopup extends PopupWithAnchor implements PopupWindow.OnDismissListener {

	public static void showInboxPopup(View anchor){
		InboxView contentView = new InboxView(Application.get());
		View pointerView = contentView.findViewById(R.id.inbox_popup_pointer);
		
		int[] coordinates = new int[2];
		anchor.getLocationOnScreen(coordinates);
		int marginLeft = coordinates[0] + (anchor.getWidth() - CODrawableUtils.getDrawableSize(R.drawable.inbox_popup_pointer)[0])/2;
		
		LayoutParams params = (LayoutParams) pointerView.getLayoutParams();
		params.setMargins(marginLeft, 0, 0, 0);
		pointerView.setLayoutParams(params);
		
		final InboxPopup popup = new InboxPopup(anchor, contentView);
		View dimmedView = contentView.findViewById(R.id.inbox_dimmed_bg_view);
		dimmedView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				popup.dismiss();
			}
		});
		popup.show();
		Application.get().postEvent(new InboxReadingEvent(true));
	}
	
	private InboxPopup(View anchor, View content) {
		super(anchor, content, true);
		setOnDismissListener(this);
		registerForEvents();
	}

	private void registerForEvents(){
		Application.get().registerForEvents(this, NavigationEvent.class);
	}
	
	private void unregisterForEvents(){
		Application.get().unregisterForAllEvents(this);
	}
	
	@Override
	public void onDismiss() {
		unregisterForEvents();
		Application.get().postEvent(new InboxReadingEvent(false));
	}
	
	//#####################################################################
	//#########					EVENTs CALLBACKs				###########
	//#####################################################################
	
	public void onEventMainThread(NavigationEvent event){
		dismiss();
	}
	
	//#####################################################################
	
}
