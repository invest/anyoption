package com.copyop.android.app.popup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.anyoption.android.R;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.popup.LowCashBalanceDialogFragment;
import com.anyoption.android.app.util.constants.Constants;

public class COLowCashBalanceDialogFragment extends LowCashBalanceDialogFragment {

	public static final String TAG = COLowCashBalanceDialogFragment.class.getSimpleName();

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static COLowCashBalanceDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new COLowCashBalanceDialogFragment.");
		COLowCashBalanceDialogFragment fragment = new COLowCashBalanceDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, true);
		bundle.putInt(ARG_BACKGROUND_COLOR_KEY, R.color.black_alpha_40);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, true);
		
		fragment.setArguments(bundle);
		return fragment;
	}

}
