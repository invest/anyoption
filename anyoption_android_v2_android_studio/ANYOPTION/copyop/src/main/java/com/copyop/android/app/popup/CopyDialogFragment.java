package com.copyop.android.app.popup;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.manager.CommunicationManager;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.popup.BaseDialogFragment.OnDialogDismissListener;
import com.anyoption.android.app.popup.LowCashBalanceDialogFragment;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.BitmapUtils;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.MarketGroup;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.service.results.MethodResult;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.R;
import com.copyop.android.app.fragment.walkthrough.CopyWalkthroughDialog;
import com.copyop.android.app.manager.COPopUpManager;
import com.copyop.android.app.popup.AssetsListDialogFragment.OnAssetsChangeListener;
import com.copyop.android.app.util.COConstants;
import com.copyop.android.app.widget.ChooseCountView;
import com.copyop.android.app.widget.SpecifyAmountView;
import com.copyop.common.dto.base.CopyConfig;
import com.copyop.common.enums.base.ProfileLinkCommandEnum;
import com.copyop.json.requests.CopyStepsEncourageMethodRequest;
import com.copyop.json.requests.CopyUserConfigMethodRequest;
import com.copyop.json.requests.ProfileMethodRequest;
import com.copyop.json.results.CopyStepsEncourageMethodResult;
import com.copyop.json.results.CopyUserConfigMethodResult;
import com.copyop.json.results.ProfileBestAssetMethodResult;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.TextView;

public class CopyDialogFragment extends BaseDialogFragment implements OnClickListener,
        OnCheckedChangeListener, OnDialogDismissListener, OnAssetsChangeListener, CopyWalkthroughDialog.OnCopyWalkthroughDialogDismissListener {

    private static final String TAG = CopyDialogFragment.class.getSimpleName();

    private static final int MAX_COUNT_VALUE = 0;

    public interface OnCopyStateChangedListener {

        /**
         * Called when the user copy / uncopy request finishes successfully.
         *
         * @param userId    the user which is/was copied
         * @param isCopying the new state
         */
        public void onCopyStateChanged(long userId, boolean isCopying);

    }

    private User user;
    private Currency currency;
    private CommunicationManager communicationManager;
    private OnCopyStateChangedListener onCopyStateChangedListener;

    private TextView titleView;
    private View helpButton;

    private View howManyButton;
    private View howManyContent;
    private RadioButton howManyMax;
    private RadioButton howManyLimit;
    private TextView howManyChoice;
    private TextView howManyMaxSafeMessage;

    private View howMuchButton;
    private View howMuchContent;
    private TextView howMuchAmount;
    private SpecifyAmountView howMuchSpecifyAmount;

    private View specificAssetButton;
    private View specificAssetContent;
    private RadioButton specificAssetAll;
    private RadioButton specificAssetSpecific;
    private TextView specificAssetChoice;
    private View specificAssetSpecificRow;

    private ChooseCountView chooseCountView;

    private View uncopyLayout;
    private CheckBox uncopyCheckBox;

    private TextView cancelButton;

    /**
     * Used for uncopy and OK button.
     */
    private TextView actionButton;
    private View copyButton;

    private View howManyDisabledView;
    private View howMuchDisabledView;
    private View specificAssetDisabledView;

    private boolean isDialogShown;

    private boolean isCopying;
    private boolean isFrozen;
    private long requestedUserId;
    private String nickname;
    private Market market;
    private boolean isViewCreated = false;

    private String titleText;

    private CopyConfig copyConfig;
    private List<Long> bestAssets;

    private COApplication coApplication;

    private int maxCountValue = MAX_COUNT_VALUE;

    private CopyStepsEncourageMethodResult copyStepsEncourageMethodResult;

    /**
     * Use this method to create new instance of the dialog.
     *
     * @return
     */
    public static CopyDialogFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new CopyDialogFragment.");
        CopyDialogFragment fragment = new CopyDialogFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        coApplication = COApplication.get();

        currency = application.getCurrency();
        user = application.getUser();
        if (coApplication.isSafeMode()) {
            maxCountValue = coApplication.getSafeModeMaxTrades();
        } else {
            maxCountValue = MAX_COUNT_VALUE;
        }

        Bundle args = getArguments();
        if (args != null) {
            isCopying = args.getBoolean(COConstants.EXTRA_IS_COPYING, false);
            isFrozen = args.getBoolean(COConstants.EXTRA_IS_FROZEN, false);
            requestedUserId = args.getLong(COConstants.EXTRA_PROFILE_ID, 0l);
            nickname = args.getString(COConstants.EXTRA_NICKNAME, null);
            market = (Market) args.getSerializable(COConstants.EXTRA_MARKET);
        }

        if (requestedUserId == 0l || nickname == null) {
            Log.e(TAG, "Can not create copy dialog without requestedUserId and nickname");
            dismiss();
        }

        /*if(!RegulationUtils.checkRegulationAndPepAndThresholdStatus(false) && !isCopying) {
            dismiss();
            RegulationUtils.checkRegulationAndPepAndThresholdStatusAndDoNecessary(false);
            return;
        }*/


        isDialogShown = false;
        communicationManager = application.getCommunicationManager();

        // Default copy config
        copyConfig = new CopyConfig(user.getId(), requestedUserId, maxCountValue, 0,
                new HashSet<Long>());
        if (isCopying && !isFrozen) {
            getCopyConfig();
        } else {

        }

        getBestAssets();
        getTooltip();

        coApplication.getAppseeManager().onCopyPopup();
    }

    /**
     * Set up content.
     *
     * @param contentView
     */
    @Override
    protected void onCreateContentView(View contentView) {
        cancelButton = (TextView) contentView.findViewById(R.id.copy_cancel_button);
        cancelButton.setOnClickListener(this);

        actionButton = (TextView) contentView.findViewById(R.id.copy_action_button);
        actionButton.setOnClickListener(this);

        copyButton = contentView.findViewById(R.id.copy_copy_button);
        copyButton.setOnClickListener(this);

        howManyButton = contentView.findViewById(R.id.copy_how_many_button);
        howManyButton.setOnClickListener(this);
        howManyContent = contentView.findViewById(R.id.copy_how_many_content);
        howManyChoice = (TextView) contentView.findViewById(R.id.copy_how_many_choice);

        howManyMax = (RadioButton) contentView.findViewById(R.id.copy_how_many_max);
        howManyMax.setOnCheckedChangeListener(this);
        howManyMaxSafeMessage = (TextView) contentView.findViewById(R.id.copy_how_many_max_safe_message);
        howManyLimit = (RadioButton) contentView.findViewById(R.id.copy_how_many_limit);
        howManyLimit.setOnCheckedChangeListener(this);
        chooseCountView = (ChooseCountView) contentView.findViewById(R.id.copy_choose_count_view);
        chooseCountView.setEnabled(false);

        howMuchButton = contentView.findViewById(R.id.copy_how_much_button);
        howMuchButton.setOnClickListener(this);
        howMuchContent = contentView.findViewById(R.id.copy_how_much_content);
        howMuchAmount = (TextView) contentView.findViewById(R.id.copy_how_much_amount);
        howMuchSpecifyAmount = (SpecifyAmountView) contentView.findViewById(R.id.copy_how_much_specify_amount);
        setupTooltip();

        specificAssetButton = contentView.findViewById(R.id.copy_specific_asset_button);
        specificAssetButton.setOnClickListener(this);
        specificAssetContent = contentView.findViewById(R.id.copy_specific_asset_content);
        specificAssetChoice = (TextView) contentView.findViewById(R.id.copy_specific_asset_choice);

        specificAssetAll = (RadioButton) contentView.findViewById(R.id.copy_specific_asset_all);
        specificAssetAll.setOnCheckedChangeListener(this);
        specificAssetSpecific = (RadioButton) contentView
                .findViewById(R.id.copy_specific_asset_specific);
        specificAssetSpecific.setOnCheckedChangeListener(this);
        specificAssetSpecificRow = contentView.findViewById(R.id.copy_specific_asset_specific_row);
        specificAssetSpecificRow.setOnClickListener(this);

        if (isCopying) {
            uncopyLayout = contentView.findViewById(R.id.copy_uncopy_layout);
            uncopyCheckBox = (CheckBox) contentView.findViewById(R.id.copy_uncopy_check_box);
            setUpUncopy();
        }

        titleView = (TextView) contentView.findViewById(R.id.copy_title);
        setUpTitle();

        helpButton = contentView.findViewById(R.id.copy_title_help);
        helpButton.setOnClickListener(this);

        howManyDisabledView = contentView.findViewById(R.id.copy_how_many_disabled_view);
        howMuchDisabledView = contentView.findViewById(R.id.copy_how_much_disabled_view);
        specificAssetDisabledView = contentView
                .findViewById(R.id.copy_specific_asset_disabled_view);

        updateChoiceViews();

        if (isFrozen) {
            uncopyCheckBox.setChecked(true);
            uncopyCheckBox.setEnabled(false);
        } else {
            boolean shouldShowWalkthrough = !application.getSharedPreferencesManager().getBoolean(
                    COConstants.PREFERENCES_IS_COPY_WALKTHROUGH_SHOWN, false);
            if (shouldShowWalkthrough) {
                showWalkthrough();
            }
        }

        TextView copyDescriptionTextView = (TextView) contentView.findViewById(R.id.copy_description);
        String oddsWin = String.valueOf(Double.valueOf(coApplication.getCopyOddsWin()).intValue());
        String oddsLose = String.valueOf(Double.valueOf(coApplication.getCopyOddsLose()).intValue());
        copyDescriptionTextView.setText(getString(R.string.copyDescription, oddsWin, oddsLose));


        isViewCreated = true;
        setHotAssetSpecialistsCopyAssets();

        if (coApplication.isSafeMode()) {
            setInSafeMode();
        }
    }


    public void setHotAssetSpecialistsCopyAssets() {
        if (isCopying && copyConfig.getAssets() != null && copyConfig.getAssets().size() == 0) {
            if (isViewCreated) {
                howMuchContent.setVisibility(View.GONE);
                howManyContent.setVisibility(View.GONE);
                specificAssetAll.setChecked(true);
                specificAssetSpecific.setChecked(false);
                specificAssetChoice.setText(R.string.all);
                updateChoiceViews();
            }
        } else {
            if (market != null) {
                if (copyConfig.getAssets() == null || copyConfig.getAssets().size() < 1) {
                    copyConfig.setAssets(new HashSet<Long>(Arrays.asList(market.getId())));
                } else {
                    if (!copyConfig.getAssets().contains(market.getId())) {
                        copyConfig.getAssets().add(market.getId());
                    }
                }

                if (isViewCreated) {
                    howMuchContent.setVisibility(View.GONE);
                    howManyContent.setVisibility(View.GONE);
                    specificAssetAll.setChecked(false);
                    specificAssetSpecific.setChecked(true);
                    specificAssetChoice.setText(getChosenAssets(copyConfig.getAssets()));
                    updateChoiceViews();
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (isCopying && copyConfig != null) {
            updateLayoutInfo();
        }
    }

    @Override
    protected int getContentLayout() {
        return R.layout.copy_dialog;
    }

    private void setUpUncopy() {
        uncopyLayout.setVisibility(View.VISIBLE);
        uncopyCheckBox.setOnCheckedChangeListener(this);
        copyButton.setVisibility(View.GONE);
        actionButton.setVisibility(View.VISIBLE);
    }

    private void setUpTitle() {
        if (isCopying) {
            titleText = getString(R.string.editCopy);
        } else {
            titleText = getString(R.string.copyCopy) + " " + nickname;
        }

        titleView.setText(titleText);
    }

    private void updateLayoutInfo() {
        if (copyConfig == null) {
            return;
        }

        if (!coApplication.isSafeMode()) {
            if (copyConfig.getCount() == maxCountValue) {
                howManyMax.setChecked(true);
                chooseCountView.setEnabled(false);
            } else {
                howManyLimit.setChecked(true);
                chooseCountView.setCount(copyConfig.getCount());

                chooseCountView.setEnabled(true);
            }
        } else {
            chooseCountView.setCount(copyConfig.getCount());
        }

        howMuchSpecifyAmount.selectAmountValue(copyConfig.getAmount());
        Set<Long> assetsSet = copyConfig.getAssets();
        if (assetsSet.isEmpty()) {
            specificAssetAll.setChecked(true);
        } else {
            specificAssetSpecific.setChecked(true);
        }
        updateChoiceViews();
    }

    private void updateChoiceViews() {
        if (copyConfig == null) {
            return;
        }

        //TODO
        if (howManyMax.isChecked()) {
            howManyChoice.setText(getString(R.string.max));
        } else {
            howManyChoice.setText(String.valueOf(chooseCountView.getCount()));
        }

        howMuchAmount.setText(AmountUtil.getFormattedAmount(
                (double) howMuchSpecifyAmount.getAmount(), currency));

        if (specificAssetAll.isChecked()) {
            specificAssetChoice.setText(R.string.all);
        } else {
            specificAssetChoice.setText(getChosenAssets(copyConfig.getAssets()));
        }
    }

    public OnCopyStateChangedListener getOnCopyStateChangedListener() {
        return onCopyStateChangedListener;
    }

    public void setOnCopyStateChangedListener(OnCopyStateChangedListener onCopyStateChangedListener) {
        this.onCopyStateChangedListener = onCopyStateChangedListener;
    }

    private void getCopyConfig() {
        ProfileMethodRequest request = new ProfileMethodRequest();
        request.setRequestedUserId(requestedUserId);

        communicationManager
                .requestService(this, "getConfigCallBack",
                        COConstants.SERVICE_GET_COPY_USER_CONFIG, request,
                        CopyUserConfigMethodResult.class, false, true);
    }

    public void getConfigCallBack(Object resultObj) {
        CopyUserConfigMethodResult result = (CopyUserConfigMethodResult) resultObj;

        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            copyConfig = result.getConfig();
            if (copyConfig.getCount() <= 0) {
                copyConfig.setCount(maxCountValue);
            }

            updateLayoutInfo();
        } else {
            if (result != null) {
                Log.e(TAG, "Could not get copy config. ErrorCode: " + result.getErrorCode());
                if (result.getErrorCode() == COConstants.ERROR_CODE_USER_REMOVED) {
                    Log.e(TAG, "This user is removed!");
                    dismiss();
                    return;
                }
            } else {
                Log.e(TAG, "Could not get copy config. Result = null");
            }

            // Set up the default config
            copyConfig = new CopyConfig(user.getId(), requestedUserId, maxCountValue, 0,
                    new HashSet<Long>());
        }

        setHotAssetSpecialistsCopyAssets();
    }

    private void copyUser() {
        if (!isCopying
                && AmountUtil.getLongAmount(howMuchSpecifyAmount.getAmount()) > user.getBalance()) {
            application.getPopUpManager().showInsufficienFundsPopUp();
            dismiss();
            return;
        }

        setUpCopyConfigInfo();
        CopyUserConfigMethodRequest request = new CopyUserConfigMethodRequest();
        request.setConfig(copyConfig);

        communicationManager.requestService(this, "copyUserCallBack",
                COConstants.SERVICE_COPY_USER_CONFIG, request, MethodResult.class);
    }

    public void copyUserCallBack(Object resultObj) {
        MethodResult result = (MethodResult) resultObj;

        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            if (!isCopying && onCopyStateChangedListener != null) {
                onCopyStateChangedListener.onCopyStateChanged(requestedUserId, true);
            }

            coApplication.refreshCopiers();
            coApplication.refreshWatchers();
        } else if (result != null && result.getErrorCode() == Constants.ERROR_CODE_NO_CASH_FOR_NIOU) {
            Application.get().getFragmentManager().showDialogFragment(LowCashBalanceDialogFragment.class, null);
        } else if (result != null
                && result.getErrorCode() == COConstants.ERROR_CODE_COPYOP_MAX_COPIERS) {
            ((COPopUpManager) application.getPopUpManager()).showFullyCopiedPopUp(requestedUserId,
                    nickname, false, null, null);
        } else {
            if (result != null) {
                Log.e(TAG, "Could not copy user. ErrorCode: " + result.getErrorCode());
            } else {
                Log.e(TAG, "Could not copy user. Result = null");
            }
        }

        dismiss();
    }

    private void uncopyUser() {
        CopyUserConfigMethodRequest request = new CopyUserConfigMethodRequest();
        copyConfig.setCommand(ProfileLinkCommandEnum.UNCOPY);
        request.setConfig(copyConfig);

        communicationManager.requestService(this, "uncopyUserCallBack",
                COConstants.SERVICE_COPY_USER_CONFIG, request, MethodResult.class);
    }

    public void uncopyUserCallBack(Object resultObj) {
        MethodResult result = (MethodResult) resultObj;

        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            if (onCopyStateChangedListener != null) {
                onCopyStateChangedListener.onCopyStateChanged(requestedUserId, false);
            }

            coApplication.refreshCopiers();
        } else {
            if (result != null) {
                Log.e(TAG, "Could not uncopy. ErrorCode: " + result.getErrorCode());
            } else {
                Log.e(TAG, "Could not uncopy. Result = null");
            }
        }

        dismiss();
    }

    private void getBestAssets() {
        ProfileMethodRequest request = new ProfileMethodRequest();
        request.setRequestedUserId(requestedUserId);

        communicationManager.requestService(this, "bestAssetsCallBack",
                COConstants.SERVICE_GET_PROFILE_BEST_ASSETS, request,
                ProfileBestAssetMethodResult.class, false, false);
    }

    public void bestAssetsCallBack(Object resultObj) {
        ProfileBestAssetMethodResult result = (ProfileBestAssetMethodResult) resultObj;

        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            bestAssets = result.getMarketIds();
        } else {
            if (result != null) {
                Log.e(TAG, "Could get best assets. ErrorCode: " + result.getErrorCode());
            } else {
                Log.e(TAG, "Could get best assets. Result = null");
            }
        }
    }

    private void setUpCopyConfigInfo() {
        copyConfig.setCommand(ProfileLinkCommandEnum.COPY);

        if (howManyMax.isChecked()) {
            copyConfig.setCount(maxCountValue);
        } else {
            copyConfig.setCount(chooseCountView.getCount());
        }

        copyConfig.setAmount(howMuchSpecifyAmount.getAmount());

        if (specificAssetAll.isChecked()) {
            copyConfig.getAssets().clear();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == cancelButton.getId()) {
            dismiss();
        } else if (v.getId() == copyButton.getId()) {
            copyUser();
        } else if (v.getId() == actionButton.getId()) {
            if (uncopyCheckBox.isChecked()) {
                uncopyUser();
            } else {
                // Update copy configuration
                copyUser();
            }
        } else if (v.getId() == howManyButton.getId()) {
            toggleHowMany();
        } else if (v.getId() == howMuchButton.getId()) {
            toggleHowMuch();
        } else if (v.getId() == specificAssetButton.getId()) {
            toggleSpecificAsset();
        } else if (v.getId() == specificAssetSpecificRow.getId()) {
            openAssetsDialog();
        } else if (v.getId() == helpButton.getId()) {
            showWalkthrough();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        // Handle uncopy checkbox events.
        if (isCopying && buttonView.getId() == uncopyCheckBox.getId()) {
            if (isChecked) {
                actionButton.setText(R.string.uncopyUpper);
                enableButtons(false);
            } else {
                actionButton.setText(R.string.ok);
                enableButtons(true);
            }

            return;
        }

        if (!isChecked) {
            return;
        }

        if (buttonView.getId() == howManyMax.getId()) {
            howManyLimit.setChecked(false);
            chooseCountView.setEnabled(false);
        } else if (buttonView.getId() == howManyLimit.getId()) {
            howManyMax.setChecked(false);
            chooseCountView.setEnabled(true);
        } else if (buttonView.getId() == specificAssetAll.getId()) {
            specificAssetSpecific.setChecked(false);
            specificAssetChoice.setText(R.string.all);
            //copyConfig.getAssets().clear();
        } else if (buttonView.getId() == specificAssetSpecific.getId()) {
            specificAssetAll.setChecked(false);
        }
    }

    /**
     * Called when assets are selected in Assets list dialog and it is closed.
     */
    @Override
    public void onAssetsChanged(Set<Long> assetsSet) {
        isDialogShown = false;
        copyConfig.setAssets(assetsSet);
        specificAssetSpecific.setChecked(true);
        specificAssetChoice.setText(getChosenAssets(assetsSet));
    }


    public String getChosenAssets(Set<Long> assetsSet) {
        int maxLength = 25;
        if (application.getLocale().getLanguage().equals("de")) {
            maxLength = 10;
        }

        String chosedAssets = "";
        if (assetsSet != null) {
            if (assetsSet.size() > 0) {
                for (Long item : assetsSet) {
                    chosedAssets += application.getMarketForID(item).getDisplayName();
                    if (assetsSet.size() > 1) {
                        chosedAssets += ", ";
                    }
                }
                if (chosedAssets.length() > maxLength) {
                    chosedAssets = chosedAssets.substring(0, maxLength);
                    if (chosedAssets.endsWith(", ")) {
                        chosedAssets = chosedAssets.substring(0, chosedAssets.length() - 2);
                    }
                    if (chosedAssets.endsWith(",")) {
                        chosedAssets = chosedAssets.substring(0, chosedAssets.length() - 1);
                    }
                    chosedAssets += "...";
                } else {
                    if (chosedAssets.endsWith(", ")) {
                        chosedAssets = chosedAssets.substring(0, chosedAssets.length() - 2);
                    }
                    if (chosedAssets.endsWith(",")) {
                        chosedAssets = chosedAssets.substring(0, chosedAssets.length() - 1);
                    }
                }
                chosedAssets += "  " + assetsSet.size();
            } else {
                chosedAssets = getString(R.string.specific);
            }
        } else {
            chosedAssets = getString(R.string.specific);
        }
        return chosedAssets;
    }


    /**
     * Called when Assets list dialog is closed without any assets selected.
     */
    @Override
    public void onDismiss() {
        isDialogShown = false;
        specificAssetAll.setChecked(true);
    }

    private void openAssetsDialog() {
        if (isDialogShown) {
            return;
        }

        Bundle args = new Bundle();
        if (bestAssets != null && !bestAssets.isEmpty()) {
            args.putSerializable(AssetsListDialogFragment.BEST_ASSETS_ARG_KEY,
                    (Serializable) bestAssets);
        }

        if (copyConfig.getAssets() != null && !copyConfig.getAssets().isEmpty()) {
            args.putSerializable(AssetsListDialogFragment.SELECTED_ASSETS_ARG_KEY,
                    (Serializable) copyConfig.getAssets());
        }

        AssetsListDialogFragment dialog = (AssetsListDialogFragment) application
                .getFragmentManager().showDialogFragment(AssetsListDialogFragment.class, args);

        dialog.setOnDismissListener(this);
        dialog.setOnAssetsChangeListener(this);
        isDialogShown = true;
    }

    private void toggleHowMany() {
        if (howManyContent.getVisibility() == View.VISIBLE) {
            howManyContent.setVisibility(View.GONE);
        } else {
            howMuchContent.setVisibility(View.GONE);
            specificAssetContent.setVisibility(View.GONE);
            howManyContent.setVisibility(View.VISIBLE);
        }

        updateChoiceViews();
    }

    private void toggleHowMuch() {
        if (howMuchContent.getVisibility() == View.VISIBLE) {
            howMuchContent.setVisibility(View.GONE);
        } else {
            howManyContent.setVisibility(View.GONE);
            specificAssetContent.setVisibility(View.GONE);
            howMuchContent.setVisibility(View.VISIBLE);
            howMuchSpecifyAmount.setVisibility(View.VISIBLE);
        }

        updateChoiceViews();
    }

    private void toggleSpecificAsset() {
        if (specificAssetContent.getVisibility() == View.VISIBLE) {
            specificAssetContent.setVisibility(View.GONE);
        } else {
            howMuchContent.setVisibility(View.GONE);
            howManyContent.setVisibility(View.GONE);
            specificAssetContent.setVisibility(View.VISIBLE);
        }

        updateChoiceViews();
    }

    /**
     * Enables or disables how many, how much and specific asset buttons.
     *
     * @param enabled
     */
    private void enableButtons(boolean enabled) {
        if (enabled) {
            howManyDisabledView.setVisibility(View.GONE);
            howMuchDisabledView.setVisibility(View.GONE);
            specificAssetDisabledView.setVisibility(View.GONE);
        } else {
            howManyContent.setVisibility(View.GONE);
            howMuchContent.setVisibility(View.GONE);
            specificAssetContent.setVisibility(View.GONE);

            howManyDisabledView.setVisibility(View.VISIBLE);
            howMuchDisabledView.setVisibility(View.VISIBLE);
            specificAssetDisabledView.setVisibility(View.VISIBLE);
        }
    }

    private void showWalkthrough() {
        howManyContent.setVisibility(View.GONE);
        howMuchContent.setVisibility(View.GONE);
        specificAssetContent.setVisibility(View.GONE);

        CopyWalkthroughDialog dialog = CopyWalkthroughDialog.newInstance(titleText, isCopying);
        application.getFragmentManager().showDialogFragment(dialog);
        dialog.setOnCopyWalkthroughDialogDismissListener(this);
        isDialogShown = true;
    }

    private void setupTooltip() {
        if (copyStepsEncourageMethodResult != null && howMuchSpecifyAmount != null) {
            int step = (int) copyStepsEncourageMethodResult.getFullyCopyStep();
            String text = copyStepsEncourageMethodResult.getMessage();
            String amount = copyStepsEncourageMethodResult.getStepAmount();
            String profit = copyStepsEncourageMethodResult.getProfitAmount();
            howMuchSpecifyAmount.setupTooltip(step, text, amount, profit);
        }
    }

    private void getTooltip() {
        CopyStepsEncourageMethodRequest request = new CopyStepsEncourageMethodRequest();
        request.setUserId(application.getUser().getId());
        request.setCopiedUserId(requestedUserId);

        application.getCommunicationManager().requestService(this, "getTooltipCallback", COConstants.SERVICE_GET_COPY_TOOLTIP_INFO, request, CopyStepsEncourageMethodResult.class, false, false);
    }

    public void getTooltipCallback(Object resObject) {
        CopyStepsEncourageMethodResult result = (CopyStepsEncourageMethodResult) resObject;
        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            this.copyStepsEncourageMethodResult = result;
            setupTooltip();
        }
    }

    public void onCopyWalkthroughDialogDismiss() {
        isDialogShown = false;
    }

    public void setInSafeMode() {
        //IT IS NOT COOL BUT...
        howManyMax.setCompoundDrawablesWithIntrinsicBounds(DrawableUtils.getDrawable(R.drawable.radio_button_big_disabled), null, null, null);
        howManyMax.setTextColor(ColorUtils.getColor(R.color.white_3));


        howManyMaxSafeMessage.setVisibility(View.VISIBLE);
        howManyLimit.setChecked(true);
        howManyMax.setEnabled(false);
        chooseCountView.setEnabled(true);
        chooseCountView.setSettings(5, 1, maxCountValue);
        chooseCountView.setCount(copyConfig.getCount());
        howManyChoice.setText(String.valueOf(chooseCountView.getCount()));

    }

}