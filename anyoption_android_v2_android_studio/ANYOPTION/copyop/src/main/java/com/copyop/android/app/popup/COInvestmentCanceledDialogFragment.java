package com.copyop.android.app.popup;

import android.os.Bundle;
import android.util.Log;

import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.popup.InvestmentCanceledDialogFragment;

/**
 * @author Anastas Arnaudov
 */
public class COInvestmentCanceledDialogFragment extends InvestmentCanceledDialogFragment {

    public static final String TAG = COInvestmentCanceledDialogFragment.class.getSimpleName();

    public static COInvestmentCanceledDialogFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new COInvestmentCanceldDialogFragment.");
        COInvestmentCanceledDialogFragment fragment = new COInvestmentCanceledDialogFragment();
        if(bundle == null){
            bundle = new Bundle();
        }
        bundle.putBoolean(BaseDialogFragment.ARG_SHOW_CLOSE_BUTTON_KEY, true);
        fragment.setArguments(bundle);
        return fragment;
    }

}
