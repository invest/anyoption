package com.copyop.android.app.fragment.news_feed;

import java.util.ArrayList;
import java.util.List;

import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.copyop.android.app.R;
import com.copyop.android.app.model.HotAssetSpecialist;
import com.copyop.android.app.model.HotCopier;
import com.copyop.android.app.model.HotGroup;
import com.copyop.android.app.model.HotGroup.Type;
import com.copyop.android.app.model.HotTrade;
import com.copyop.android.app.model.HotTrader;
import com.copyop.android.app.util.COConstants;
import com.copyop.android.app.util.ConvertUtils;
import com.copyop.android.app.util.adapter.HotAdapter;
import com.copyop.android.app.util.adapter.HotAdapter.HotHoursFilterListener;
import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.enums.base.UpdateTypeEnum;
import com.copyop.json.requests.CopyopHotDataMethodRequest;
import com.copyop.json.results.CoyopHotDataMethodResult;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupCollapseListener;

/**
 * Displays a List with best Traders, Copiers and Trades.
 *
 * @author Anastas Arnaudov
 */
public class HotFragment extends BaseFragment implements OnGroupCollapseListener, HotHoursFilterListener {

    public static HotFragment newInstance(Bundle bundle) {
        HotFragment fragment = new HotFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public final String TAG = HotFragment.class.getSimpleName();

    private HotGroup[] groupsArray;
    private HotGroup tradersGroup;
    private HotGroup tradesGroup;
    private HotGroup copiersGroup;
    private HotGroup assetSpecialistsGroup;
    private List<HotTrader> tradersList;
    private List<HotTrade> tradesList;
    private List<HotCopier> copiersList;
    private List<HotAssetSpecialist> assetSpecialistsList;

    private ExpandableListView listView;
    private HotAdapter adapter;

    private View loadingView;
    private boolean isTradersLoading;
    private boolean isTradesLoading;
    private boolean isCopiersLoading;
    private boolean isAssetSpecialistsLoading;

    private AsyncTask<List<FeedMessage>, Void, List<HotTrader>> parseHotTradersTask;
    private AsyncTask<List<FeedMessage>, Void, List<HotTrade>> parseHotTradesTask;
    private AsyncTask<List<FeedMessage>, Void, List<HotCopier>> parseHotCopiersTask;
    private AsyncTask<List<FeedMessage>, Void, List<HotAssetSpecialist>> parseHotAssetSpecialistsTask;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            //Get arguments:

        }

        groupsArray = new HotGroup[4];

        tradersList = new ArrayList<HotTrader>();
        tradesList = new ArrayList<HotTrade>();
        copiersList = new ArrayList<HotCopier>();
        assetSpecialistsList = new ArrayList<HotAssetSpecialist>();

        tradersGroup = new HotGroup()
                .setType(Type.TRADERS)
                .setName(application.getString(R.string.hotGroupNameTraders, 0))
                .setItems(tradersList);

        tradesGroup = new HotGroup()
                .setType(Type.TRADES)
                .setName(application.getString(R.string.hotGroupNameTrades, 0))
                .setItems(tradesList);

        copiersGroup = new HotGroup()
                .setType(Type.COPIERS)
                .setName(application.getString(R.string.hotGroupNameCopiers, 0))
                .setItems(copiersList);

        assetSpecialistsGroup = new HotGroup()
                .setType(Type.ASSET_SPECIALISTS)
                .setName(application.getString(R.string.hotGroupNameAssetSpecialists, 0))
                .setItems(assetSpecialistsList);

        groupsArray[0] = tradersGroup;
        groupsArray[1] = assetSpecialistsGroup;
        groupsArray[2] = tradesGroup;
        groupsArray[3] = copiersGroup;

        adapter = new HotAdapter(groupsArray, this);
        refresh();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (hidden) {
            cancelAllTasks();
        } else {
            refresh();
        }

        super.onHiddenChanged(hidden);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isPaused) {
            refresh();
            isPaused = false;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.hot_layout, container, false);
        loadingView = rootView.findViewById(R.id.loadingView);
        listView = (ExpandableListView) rootView.findViewById(R.id.hot_list_view);
        listView.setAdapter(adapter);
        listView.setOnGroupCollapseListener(this);
        expandAll();

        showHideLoading();

        return rootView;
    }

    protected void showHideLoading() {
        if (loadingView != null) {
            if (!isTradersLoading && !isTradesLoading && !isCopiersLoading) {
                loadingView.setVisibility(View.GONE);
            } else {
                loadingView.setVisibility(View.VISIBLE);
            }
        }
    }

    private void cancelAllTasks() {
        application.getCommunicationManager().cancelAllRequests(this);

        if (parseHotTradersTask != null && !parseHotTradersTask.isCancelled()) {
            parseHotTradersTask.cancel(true);
        }

        if (parseHotTradesTask != null && !parseHotTradesTask.isCancelled()) {
            parseHotTradesTask.cancel(true);
        }

        if (parseHotCopiersTask != null && !parseHotCopiersTask.isCancelled()) {
            parseHotCopiersTask.cancel(true);
        }

        if (parseHotAssetSpecialistsTask != null && !parseHotAssetSpecialistsTask.isCancelled()) {
            parseHotAssetSpecialistsTask.cancel(true);
        }
    }

    //#####################################################################
    //#########					REQUEST - RESPONSE				###########
    //#####################################################################

    public void getHots() {
        getHotTraders(UpdateTypeEnum.BEST_TRADERS);
        getHotTrades(UpdateTypeEnum.BEST_TRADES);
        getHotCopiers(UpdateTypeEnum.BEST_COPIERS);
        getHotAssetSpecialists(UpdateTypeEnum.BEST_ASSET_SPECIALISTS);
    }

    protected void getHotTraders(UpdateTypeEnum updateTypeEnum) {
        isTradersLoading = true;
        showHideLoading();
        this.tradersList.clear();
        CopyopHotDataMethodRequest request = new CopyopHotDataMethodRequest();
        request.setUpdateTypeEnumId(updateTypeEnum.getId());
        if (updateTypeEnum == UpdateTypeEnum.BEST_TRADERS) {
            request.setInitCall(true);
        }
        if (!application.getCommunicationManager().requestService(this, "gotHotTraders", COConstants.SERVICE_GET_COPYOP_HOT_TRADERS, request, CoyopHotDataMethodResult.class, false, false)) {
            isTradersLoading = false;
            showHideLoading();
        }
    }

    @SuppressWarnings("unchecked")
    public void gotHotTraders(Object result) {
        if (isHidden()) {
            return;
        }

        if (result != null && result instanceof CoyopHotDataMethodResult && ((CoyopHotDataMethodResult) result).getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            final CoyopHotDataMethodResult coyopHotDataMethodResult = (CoyopHotDataMethodResult) result;
            parseHotTradersTask = new AsyncTask<List<FeedMessage>, Void, List<HotTrader>>() {
                @Override
                protected List<HotTrader> doInBackground(List<FeedMessage>... params) {
                    //Set up the filters:
                    List<UpdateTypeEnum> filters = new ArrayList<UpdateTypeEnum>();
                    for (Integer filter : coyopHotDataMethodResult.getExistUpdateTypeEnumId()) {
                        filters.add(UpdateTypeEnum.getById(filter));
                    }
                    tradersGroup.setFilters(filters);
                    ////////////////

                    //Set up the items:
                    List<FeedMessage> feedMessagesList = params[0];
                    List<HotTrader> result = new ArrayList<HotTrader>();
                    if (feedMessagesList != null) {
                        for (FeedMessage feedMessage : feedMessagesList) {
                            HotTrader hotTrader = ConvertUtils.convertFeedMessageToHotTrader(feedMessage);
                            result.add(hotTrader);
                        }
                    } else {
                        cancel(true);
                    }
                    //////////////////
                    return result;
                }

                @Override
                protected void onCancelled() {
                    Log.e(TAG, "parseHotTradersTask - onCancelled");

                    isTradersLoading = false;
                    showHideLoading();
                }

                @Override
                protected void onPostExecute(List<HotTrader> result) {
                    tradersList.addAll(result);
                    setupTradersGroup();
                    isTradersLoading = false;
                    showHideLoading();
                }
            }.execute(coyopHotDataMethodResult.getFeed());
        } else {
            Log.e(TAG, COConstants.SERVICE_GET_COPYOP_HOT_TRADERS + " callback -> COULD NOT parse the result");
            isTradersLoading = false;
            showHideLoading();
        }
    }

    protected void getHotTrades(UpdateTypeEnum updateTypeEnum) {
        isTradesLoading = true;
        showHideLoading();
        this.tradesList.clear();
        CopyopHotDataMethodRequest request = new CopyopHotDataMethodRequest();
        request.setUpdateTypeEnumId(updateTypeEnum.getId());
        if (updateTypeEnum == UpdateTypeEnum.BEST_TRADES) {
            request.setInitCall(true);
        }
        if (!application.getCommunicationManager().requestService(this, "gotHotTrades", COConstants.SERVICE_GET_COPYOP_HOT_TRADES, request, CoyopHotDataMethodResult.class, false, false)) {
            isTradesLoading = false;
            showHideLoading();
        }
    }

    @SuppressWarnings("unchecked")
    public void gotHotTrades(Object result) {
        if (isHidden()) {
            return;
        }

        isTradesLoading = false;
        showHideLoading();
        if (result != null && result instanceof CoyopHotDataMethodResult && ((CoyopHotDataMethodResult) result).getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            final CoyopHotDataMethodResult coyopHotDataMethodResult = (CoyopHotDataMethodResult) result;
            parseHotTradesTask = new AsyncTask<List<FeedMessage>, Void, List<HotTrade>>() {
                @Override
                protected List<HotTrade> doInBackground(List<FeedMessage>... params) {
                    //Set up the filters:
                    List<UpdateTypeEnum> filters = new ArrayList<UpdateTypeEnum>();
                    for (Integer filter : coyopHotDataMethodResult.getExistUpdateTypeEnumId()) {
                        filters.add(UpdateTypeEnum.getById(filter));
                    }
                    tradesGroup.setFilters(filters);
                    ////////////////

                    //Set up the items:
                    List<FeedMessage> feedMessagesList = params[0];
                    List<HotTrade> result = new ArrayList<HotTrade>();
                    if (feedMessagesList != null) {
                        for (FeedMessage feedMessage : feedMessagesList) {
                            HotTrade hotTrade = ConvertUtils.convertFeedMessageToHotTrade(feedMessage);
                            result.add(hotTrade);
                        }
                    } else {
                        cancel(true);
                    }
                    ///////////////////
                    return result;
                }

                @Override
                protected void onCancelled() {
                    Log.e(TAG, "parseHotTradesTask - onCancelled");

                    isTradersLoading = false;
                    showHideLoading();
                }

                @Override
                protected void onPostExecute(List<HotTrade> result) {
                    tradesList.addAll(result);
                    setupTradesGroup();
                    isTradesLoading = false;
                    showHideLoading();
                }
            }.execute(coyopHotDataMethodResult.getFeed());
        } else {
            Log.e(TAG, COConstants.SERVICE_GET_COPYOP_HOT_TRADES + " callback -> COULD NOT parse the result");
            isTradesLoading = false;
            showHideLoading();
        }
    }


    protected void getHotAssetSpecialists(UpdateTypeEnum updateTypeEnum) {
        isAssetSpecialistsLoading = true;
        showHideLoading();
        this.assetSpecialistsList.clear();
        CopyopHotDataMethodRequest request = new CopyopHotDataMethodRequest();
        request.setUpdateTypeEnumId(updateTypeEnum.getId());
        if (updateTypeEnum == UpdateTypeEnum.BEST_ASSET_SPECIALISTS) {
            request.setInitCall(true);
        }
        if (!application.getCommunicationManager().requestService(this, "gotHotAssetSpecialists", COConstants.SERVICE_GET_COPYOP_HOT_ASSET_SPECIALISTS, request, CoyopHotDataMethodResult.class, false, false)) {
            isAssetSpecialistsLoading = false;
            showHideLoading();
        }

    }

    @SuppressWarnings("unchecked")
    public void gotHotAssetSpecialists(Object result) {
        if (isHidden()) {
            return;
        }

        isAssetSpecialistsLoading = false;
        showHideLoading();
        if (result != null && result instanceof CoyopHotDataMethodResult && ((CoyopHotDataMethodResult) result).getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            final CoyopHotDataMethodResult coyopHotDataMethodResult = (CoyopHotDataMethodResult) result;
            parseHotAssetSpecialistsTask = new AsyncTask<List<FeedMessage>, Void, List<HotAssetSpecialist>>() {
                @Override
                protected List<HotAssetSpecialist> doInBackground(List<FeedMessage>... params) {
                    //Set up the filters:
                    List<UpdateTypeEnum> filters = new ArrayList<UpdateTypeEnum>();
                    for (Integer filter : coyopHotDataMethodResult.getExistUpdateTypeEnumId()) {
                        filters.add(UpdateTypeEnum.getById(filter));
                    }
                    assetSpecialistsGroup.setFilters(filters);
                    ////////////////

                    //Set up the items:
                    List<FeedMessage> feedMessagesList = params[0];
                    List<HotAssetSpecialist> result = new ArrayList<HotAssetSpecialist>();
                    if (feedMessagesList != null) {
                        for (FeedMessage feedMessage : feedMessagesList) {
                            HotAssetSpecialist hotAssetSpecialist = ConvertUtils.convertFeedMessageToHotAssetSpecialist(feedMessage);
                            result.add(hotAssetSpecialist);
                        }
                    } else {
                        cancel(true);
                    }
                    ///////////////////
                    return result;
                }

                @Override
                protected void onCancelled() {
                    Log.e(TAG, "parseHotAssetSpecialistsTask - onCancelled");

                    isAssetSpecialistsLoading = false;
                    showHideLoading();
                }

                @Override
                protected void onPostExecute(List<HotAssetSpecialist> result) {
                    assetSpecialistsList.addAll(result);
                    setupAssetSpecialistsGroup();
                    isAssetSpecialistsLoading = false;
                    showHideLoading();
                }
            }.execute(coyopHotDataMethodResult.getFeed());
        } else {
            Log.e(TAG, COConstants.SERVICE_GET_COPYOP_HOT_ASSET_SPECIALISTS + " callback -> COULD NOT parse the result");
            isAssetSpecialistsLoading = false;
            showHideLoading();
        }
    }

    protected void getHotCopiers(UpdateTypeEnum updateTypeEnum) {
        isCopiersLoading = true;
        showHideLoading();
        this.copiersList.clear();
        CopyopHotDataMethodRequest request = new CopyopHotDataMethodRequest();
        request.setUpdateTypeEnumId(updateTypeEnum.getId());
        if (updateTypeEnum == UpdateTypeEnum.BEST_COPIERS) {
            request.setInitCall(true);
        }
        if (!application.getCommunicationManager().requestService(this, "gotHotCopiers", COConstants.SERVICE_GET_COPYOP_HOT_COPIERS, request, CoyopHotDataMethodResult.class, false, false)) {
            isCopiersLoading = false;
            showHideLoading();
        }
    }

    @SuppressWarnings("unchecked")
    public void gotHotCopiers(Object result) {
        if (isHidden()) {
            return;
        }

        isCopiersLoading = false;
        showHideLoading();
        if (result != null && result instanceof CoyopHotDataMethodResult && ((CoyopHotDataMethodResult) result).getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            final CoyopHotDataMethodResult coyopHotDataMethodResult = (CoyopHotDataMethodResult) result;
            parseHotCopiersTask = new AsyncTask<List<FeedMessage>, Void, List<HotCopier>>() {
                @Override
                protected List<HotCopier> doInBackground(List<FeedMessage>... params) {
                    //Set up the filters:
                    List<UpdateTypeEnum> filters = new ArrayList<UpdateTypeEnum>();
                    for (Integer filter : coyopHotDataMethodResult.getExistUpdateTypeEnumId()) {
                        filters.add(UpdateTypeEnum.getById(filter));
                    }
                    copiersGroup.setFilters(filters);
                    ////////////////

                    //Set up the items:
                    List<FeedMessage> feedMessagesList = params[0];
                    List<HotCopier> result = new ArrayList<HotCopier>();
                    if (feedMessagesList != null) {
                        for (FeedMessage feedMessage : feedMessagesList) {
                            HotCopier hotCopier = ConvertUtils.convertFeedMessageToHotCopier(feedMessage);
                            result.add(hotCopier);
                        }
                    } else {
                        cancel(true);
                    }
                    ///////////////////
                    return result;
                }

                @Override
                protected void onCancelled() {
                    Log.e(TAG, "parseHotCopiersTask - onCancelled");

                    isCopiersLoading = false;
                    showHideLoading();
                }

                @Override
                protected void onPostExecute(List<HotCopier> result) {
                    copiersList.addAll(result);
                    setupCopiersGroup();
                    isCopiersLoading = false;
                    showHideLoading();
                }
            }.execute(coyopHotDataMethodResult.getFeed());
        } else {
            Log.e(TAG, COConstants.SERVICE_GET_COPYOP_HOT_COPIERS + " callback -> COULD NOT parse the result");
            isCopiersLoading = false;
            showHideLoading();
        }
    }

    //#####################################################################

    @Override
    public void onGroupCollapse(int groupPosition) {
        listView.expandGroup(groupPosition);
    }

    private void expandAll() {
        int count = groupsArray.length;
        for (int i = 0; i < count; i++) {
            listView.expandGroup(i);
        }
    }

    private void setupTradersGroup() {
        if (tradersList != null && !tradersList.isEmpty()) {
            int assetLastTradingHours = tradersList.get(0).getLastHours();
            tradersGroup.setUpdateType(tradersList.get(0).getUpdateType());
            if (tradersList.get(0).getUpdateType() == UpdateTypeEnum.BEST_TRADERS) {
                //Set up INIT hours:
                tradersGroup.setLastInitHours(assetLastTradingHours);
            }
            tradersGroup.setLastHours(assetLastTradingHours);
            tradersGroup.setName(application.getString(R.string.hotGroupNameTraders, tradersGroup.getLastHoursFormated()));
        }
        adapter.notifyDataSetChanged();
    }

    private void setupTradesGroup() {
        if (tradesList != null && !tradesList.isEmpty()) {
            int assetLastTradingHours = tradesList.get(0).getLastHours();
            tradesGroup.setUpdateType(tradesList.get(0).getUpdateType());
            if (tradesList.get(0).getUpdateType() == UpdateTypeEnum.BEST_TRADES) {
                //Set up INIT hours:
                tradesGroup.setLastInitHours(assetLastTradingHours);
            }
            tradesGroup.setLastHours(assetLastTradingHours);
            tradesGroup.setName(application.getString(R.string.hotGroupNameTrades, tradesGroup.getLastHoursFormated()));
        }
        adapter.notifyDataSetChanged();
    }

    private void setupAssetSpecialistsGroup() {
        if (assetSpecialistsList != null && !assetSpecialistsList.isEmpty()) {
            int assetLastTradingHours = assetSpecialistsList.get(0).getLastHours();
            assetSpecialistsGroup.setUpdateType(assetSpecialistsList.get(0).getUpdateType());
            if (assetSpecialistsList.get(0).getUpdateType() == UpdateTypeEnum.BEST_ASSET_SPECIALISTS) {
                //Set up INIT hours:
                assetSpecialistsGroup.setLastInitHours(assetLastTradingHours);
            }
            assetSpecialistsGroup.setLastHours(assetLastTradingHours);
            assetSpecialistsGroup.setName(application.getString(R.string.hotGroupNameAssetSpecialists, assetSpecialistsGroup.getLastHoursFormated()));
        }
        adapter.notifyDataSetChanged();
    }

    private void setupCopiersGroup() {
        if (copiersList != null && !copiersList.isEmpty()) {
            int assetLastTradingHours = copiersList.get(0).getLastHours();
            copiersGroup.setUpdateType(copiersList.get(0).getUpdateType());
            if (copiersList.get(0).getUpdateType() == UpdateTypeEnum.BEST_COPIERS) {
                //Set up INIT hours:
                copiersGroup.setLastInitHours(assetLastTradingHours);
            }
            copiersGroup.setLastHours(assetLastTradingHours);
            copiersGroup.setName(application.getString(R.string.hotGroupNameCopiers, copiersGroup.getLastHoursFormated()));
        }
        adapter.notifyDataSetChanged();
    }

    private void setupEmptyList() {

    }

    private void refresh() {
        clean();
        getHots();
    }

    private void clean() {
        this.tradersList.clear();
        this.tradesList.clear();
        this.copiersList.clear();
        this.assetSpecialistsList.clear();

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onFilter(UpdateTypeEnum updateTypeEnum) {
        if (updateTypeEnum == UpdateTypeEnum.BEST_TRADERS || updateTypeEnum == UpdateTypeEnum.BEST_TRADERS_ONE_WEEK || updateTypeEnum == UpdateTypeEnum.BEST_TRADERS_ONE_MOTNH || updateTypeEnum == UpdateTypeEnum.BEST_TRADERS_TRHEE_MOTNH) {
            getHotTraders(updateTypeEnum);
        } else if (updateTypeEnum == UpdateTypeEnum.BEST_TRADES || updateTypeEnum == UpdateTypeEnum.BEST_TRADES_ONE_WEEK || updateTypeEnum == UpdateTypeEnum.BEST_TRADES_ONE_MOTNH || updateTypeEnum == UpdateTypeEnum.BEST_TRADES_TRHEE_MOTNH) {
            getHotTrades(updateTypeEnum);
        } else if (updateTypeEnum == UpdateTypeEnum.BEST_COPIERS || updateTypeEnum == UpdateTypeEnum.BEST_COPIERS_ONE_WEEK || updateTypeEnum == UpdateTypeEnum.BEST_COPIERS_ONE_MOTNH || updateTypeEnum == UpdateTypeEnum.BEST_COPIERS_TRHEE_MOTNH) {
            getHotCopiers(updateTypeEnum);
        } else if (updateTypeEnum == UpdateTypeEnum.BEST_ASSET_SPECIALISTS || updateTypeEnum == UpdateTypeEnum.BEST_ASSET_SPECIALISTS_ONE_WEEK || updateTypeEnum == UpdateTypeEnum.BEST_ASSET_SPECIALISTS_ONE_MOTNH || updateTypeEnum == UpdateTypeEnum.BEST_ASSET_SPECIALISTS_TRHEE_MOTNH) {
            getHotAssetSpecialists(updateTypeEnum);
        }
    }

}
