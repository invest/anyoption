package com.copyop.android.app.widget;

import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.android.app.widget.TextView;
import com.copyop.android.app.R;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;

/**
 * Timer showing loading with animation.
 * 
 * @author Mario Kutlev
 */
public class TimerView extends RelativeLayout {

	private static final String TAG = TimerView.class.getSimpleName();

	private static final int DEFAULT_UPDATE_INVERVAL = 1000;

	public static final int TIME_FORMAT_MINUTES_AND_SECONDS = 1;
	public static final int TIME_FORMAT_SECONDS_AND_MILLISECONDS = 2;

	public interface OnFinishListener {

		/**
		 * Called when the timer finish finishes.
		 */
		public void onFinish();

	}

	private RelativeLayout rootView;
	private TextView labelFrontTextView;

	private TextView timeBackTextView;
	private TextView timeFrontTextView;
	private View frontLayout;

	private CountDownTimer timer;
	private int timeFormat;
	private long duration;
	private long updateInterval;

	private ValueAnimator progressAnimation;
	private OnFinishListener finishListener;

	public TimerView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);

		init(context, attrs);
	}

	public TimerView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public TimerView(Context context) {
		this(context, null);
	}

	private void init(Context context, AttributeSet attrs) {
		String label = null;
		int progressBg = 0;
		boolean isProgressBgColor = false;
		int textColor = 0;
		int textColorProgressPast = 0;
		boolean isLabelShown = true;
		float textSize = 0.0f;
		float labelTextSize = 0.0f;
		int fontID = 0;

		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TimerView);
		try {
			label = a.getString(R.styleable.TimerView_label);
			progressBg = a.getResourceId(R.styleable.TimerView_probressBackground, 0);
			if (progressBg == 0) {
				progressBg = a.getColor(R.styleable.TimerView_probressBackground, 0);
				isProgressBgColor = true;
			}

			updateInterval = a
					.getInt(R.styleable.TimerView_updateInterval, DEFAULT_UPDATE_INVERVAL);

			label = a.getString(R.styleable.TimerView_label);

			textColor = a.getColor(R.styleable.TimerView_textColor, 0);
			textColorProgressPast = a.getColor(R.styleable.TimerView_textColorProgressPast, 0);
			textSize = a.getDimension(R.styleable.TimerView_textSize, textSize);
			labelTextSize = a.getDimension(R.styleable.TimerView_labelTextSize, labelTextSize);
			isLabelShown = a.getBoolean(R.styleable.TimerView_isLabelShown, true);
			fontID = a.getInt(R.styleable.Button_font, 0);
		} finally {
			a.recycle();
		}

		rootView = (RelativeLayout) View.inflate(context, R.layout.timer_view, this);

		if (isInEditMode()) {
			return;
		}

		frontLayout = rootView
				.findViewById(R.id.timer_layout_front);

		if (progressBg != 0) {
			if (isProgressBgColor) {
				frontLayout.setBackgroundColor(progressBg);
			} else {
				frontLayout.setBackgroundResource(progressBg);
			}
		}

		labelFrontTextView = (TextView) rootView
				.findViewById(R.id.timer_label_front);

		timeFrontTextView = (TextView) rootView
				.findViewById(R.id.timer_time_front);

		timeBackTextView = (TextView) rootView
				.findViewById(R.id.timer_time_back);
		TextView labelBackTextView = ((TextView) rootView.findViewById(R.id.timer_label_back));

		if (fontID != 0) {
			Font font = FontsUtils.getFontForFontID(fontID);
			labelFrontTextView.setFont(font);
			labelBackTextView.setFont(font);
			timeFrontTextView.setFont(font);
			timeBackTextView.setFont(font);
		}

		if (label != null) {
			labelFrontTextView.setText(label);
			labelBackTextView.setText(label);
		}

		if (textColor != 0) {
			timeBackTextView.setTextColor(textColor);
			labelBackTextView.setTextColor(textColor);
		}

		if (textColorProgressPast != 0) {
			timeFrontTextView.setTextColor(textColorProgressPast);
			labelFrontTextView.setTextColor(textColorProgressPast);
		}

		if (labelTextSize != 0.0f) {
			labelFrontTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, labelTextSize);
			labelBackTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, labelTextSize);
		}

		if (textSize != 0.0f) {
			timeFrontTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
			timeBackTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
		}

		if (!isLabelShown) {
			labelFrontTextView.setVisibility(View.GONE);
			labelBackTextView.setVisibility(View.GONE);
		}

		OnLayoutChangeListener onLayoutChangeListener = new OnLayoutChangeListener() {

			@Override
			public void onLayoutChange(View v, int left, int top, int right, int bottom,
					int oldLeft, int oldTop, int oldRight, int oldBottom) {
				if (timeFrontTextView.getWidth() > 0) {
					labelFrontTextView.getLayoutParams().width = rootView
							.getWidth();
					timeFrontTextView.getLayoutParams().width = rootView
							.getWidth();
				}
			}
		};
		labelFrontTextView.addOnLayoutChangeListener(onLayoutChangeListener);
		timeFrontTextView.addOnLayoutChangeListener(onLayoutChangeListener);

		duration = 0;
		timeFormat = TIME_FORMAT_MINUTES_AND_SECONDS;
	}

	/**
	 * Starts the timer with the specified duration.
	 */
	public void start(long timerDuration) {
		if (timerDuration <= 0) {
			Log.d(TAG, "Cannot start the timer without valid timerDuration! Must be > 0!");
			return;
		}

		stop();

		duration = timerDuration;

		timer = new CountDownTimer(timerDuration, updateInterval) {

			@Override
			public void onTick(long millisUntilFinished) {
				if (progressAnimation == null) {
					startLoadingAnimation();
				}

				showTime(millisUntilFinished);
			}

			@Override
			public void onFinish() {
				showTime(0);

				if (finishListener != null) {
					finishListener.onFinish();
				}
			}
		}.start();

	}

	private void showTime(long ms) {
		String time = null;

		if (timeFormat == TIME_FORMAT_SECONDS_AND_MILLISECONDS) {
			time = TimeUtils.formatTimeToSecondsAndMilliseconds(ms);
		} else {
			// The default format is used - TIME_FORMAT_MINUTES_AND_SECONDS
			time = TimeUtils.formatTimeToMinutesAndSeconds(ms);
		}

		timeBackTextView.setText(time);
		timeFrontTextView.setText(time);
	}

	/**
	 * Starts the timer with duration which is set with {{@link #setDuration(long)}.
	 */
	public void start() {
		if (duration <= 0) {
			Log.d(TAG, "Cannot start the timer without duration! Use setDuration()");
			return;
		}
		start(duration);
	}

	public void stop() {
		if (timer != null) {
			timer.cancel();
			timer = null;
		}

		if (progressAnimation != null) {
			progressAnimation.cancel();
			progressAnimation = null;
		}
	}

	private void startLoadingAnimation() {
		if (progressAnimation != null) {
			progressAnimation.cancel();
		}

		progressAnimation = ValueAnimator.ofInt(frontLayout.getWidth(), rootView.getWidth());
		progressAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

			@Override
			public void onAnimationUpdate(ValueAnimator valueAnimator) {
				int newWidth = (Integer) valueAnimator.getAnimatedValue();
				ViewGroup.LayoutParams params = frontLayout.getLayoutParams();
				params.width = newWidth;
				frontLayout.setLayoutParams(params);
			}
		});

		progressAnimation.setDuration(duration);
		progressAnimation.setInterpolator(new LinearInterpolator());
		progressAnimation.start();
	}

	public boolean isRunning() {
		return timer != null;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		if (duration <= 0) {
			Log.d(TAG, "Duration value is invalid! It must be > 0!");
			return;
		}

		this.duration = duration;

		if (timeBackTextView != null && timeFrontTextView != null) {
			showTime(duration);
		}
	}

	/**
	 * Returns time format. It will be one of {@link #TIME_FORMAT_MINUTES_AND_SECONDS} and
	 * {@link #TIME_FORMAT_SECONDS_AND_MILLISECONDS} constants.
	 * 
	 * @return
	 */
	public int getTimeFormat() {
		return timeFormat;
	}

	/**
	 * Sets time format. It must be one of {@link #TIME_FORMAT_MINUTES_AND_SECONDS} and
	 * {@link #TIME_FORMAT_SECONDS_AND_MILLISECONDS} constants.
	 */
	public void setTimeFormat(int timeFormat) {
		this.timeFormat = timeFormat;
	}

	public OnFinishListener getFinishListener() {
		return finishListener;
	}

	public void setFinishListener(OnFinishListener finishListener) {
		this.finishListener = finishListener;
	}

}