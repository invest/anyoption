package com.copyop.android.app.event;

import android.provider.ContactsContract.Profile;

/**
 * Event that is triggered when the cached {@link Profile} is changed. 
 * @author Anastas Arnaudov
 *
 */
public class ProfileUpdateEvent {

}
