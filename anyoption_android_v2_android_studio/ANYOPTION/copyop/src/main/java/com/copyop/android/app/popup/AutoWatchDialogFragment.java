package com.copyop.android.app.popup;

import java.util.ArrayList;
import java.util.List;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COBitmapUtils;
import com.copyop.android.app.util.COConstants;
import com.copyop.common.dto.base.Profile;
import com.copyop.json.requests.WatchListMethodRequest;
import com.copyop.json.results.WatchListMethodResult;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class AutoWatchDialogFragment extends BaseDialogFragment {
	private static final String TAG = AutoWatchDialogFragment.class.getSimpleName();

	private List<Profile> autoWatchList;
	private AutoWatchListAdapter listAdapter;
	private ListView listView;

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static AutoWatchDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new AutoWatchDialogFragment.");
		AutoWatchDialogFragment fragment = new AutoWatchDialogFragment();
		fragment.setArguments(bundle);
		return fragment;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		autoWatchList = new ArrayList<Profile>();
		listAdapter = new AutoWatchListAdapter(autoWatchList);
		
		fetchAutoWatchList();
	}

	/**
	 * Set up content.
	 * 
	 * @param contentView
	 */
	@Override
	protected void onCreateContentView(View contentView) {
		listView = (ListView) contentView.findViewById(R.id.auto_watch_list_view);
		listView.setAdapter(listAdapter);

		View doneButton = contentView.findViewById(R.id.auto_watch_done_button);
		doneButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}
	
	private void fetchAutoWatchList() {
		WatchListMethodRequest request = new WatchListMethodRequest();
		request.setAutoWatchRequired(true);
		Application.get().getCommunicationManager().requestService(this, "fetchAutoWatchListCallback", COConstants.SERVICE_GET_WATCHING, request, WatchListMethodResult.class);
	}

	public void fetchAutoWatchListCallback(Object resultObj) {
		WatchListMethodResult result = (WatchListMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			setAutoWatchList(result.getWatchList());
		} else {
			if (result != null) {
				Log.e(TAG, "Could not get copy config. ErrorCode: " + result.getErrorCode());
			} else {
				Log.e(TAG, "Could not get copy config. Result = null");
			}
		}
	}

	public List<Profile> getAutoWatchList() {
		return autoWatchList;
	}

	public void setAutoWatchList(List<? extends Profile> autoWatchList) {
		this.autoWatchList.addAll(autoWatchList);
		listAdapter.setList(this.autoWatchList);
		listAdapter.notifyDataSetChanged();
	}

	@Override
	protected int getContentLayout() {
		return R.layout.auto_watch_dialog;
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);

		application.getSharedPreferencesManager().putBoolean(
				COConstants.PREFERENCES_IS_AUTO_WATCHED_SHOWN, true);
	}
	
	static class AutoWatchListAdapter extends BaseAdapter {
		
		static class UpdateViewHolder {
			public int position;
			
			public ImageView avatarImageView;
			public TextView nameTextView;
			public TextView copiersTextView;
			public TextView watchersTextView;
		}
		
		private List<Profile> list;
		private Context context;
	    private LayoutInflater inflater;  

		public AutoWatchListAdapter(List<Profile> list) {
			this.list = list;
			this.context = Application.get();
			this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}
		
		@Override
		public int getCount() {
			return list.size();
		}

		@Override
		public Profile getItem(int position) {
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			return list.get(position).getUserId();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Profile autoWatch = list.get(position);
			UpdateViewHolder updateViewHolder = null;
			if (null == convertView) {
				updateViewHolder = new UpdateViewHolder();
				convertView = inflater.inflate(R.layout.auto_watch_list_item, parent, false);
				updateViewHolder.avatarImageView = (ImageView) convertView.findViewById(R.id.auto_watch_item_avatar_image_view);
				updateViewHolder.nameTextView = (TextView) convertView.findViewById(R.id.auto_watch_item_name_text_view);
				updateViewHolder.copiersTextView = (TextView) convertView.findViewById(R.id.auto_watch_item_copiers_text_view);
				updateViewHolder.watchersTextView = (TextView) convertView.findViewById(R.id.auto_watch_item_watchers_text_view);
				convertView.setTag(updateViewHolder);
			} else {
				updateViewHolder = (UpdateViewHolder) convertView.getTag();
			}
			updateViewHolder.position = position;
			
			try {
				setupListItem(autoWatch, updateViewHolder);
			} catch (Exception e) {
				Log.e(TAG, "Can't setup list item.", e);
			}
			
			return convertView;
		}
		
		private void setupListItem(Profile profile, UpdateViewHolder updateViewHolder) {
			int avatarSize = context.getResources().getDimensionPixelSize(R.dimen.auto_watch_item_avatar_size);
			COBitmapUtils.loadBitmap(profile.getAvatar(), updateViewHolder.avatarImageView, avatarSize, avatarSize);
			updateViewHolder.nameTextView.setText(profile.getNickname());
			updateViewHolder.copiersTextView.setText(context.getString(R.string.newsItemCopiers, profile.getCopiers()));
			updateViewHolder.watchersTextView.setText(context.getString(R.string.newsItemWatchers, profile.getWatchers()));
		}

		public List<Profile> getList() {
			return list;
		}

		public void setList(List<Profile> list) {
			this.list = list;
		}

		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}

		@Override
		public boolean isEnabled(int position) {
			return false;
		}

	}
}