package com.copyop.android.app.fragment.my_account;

import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.my_account.PersonalDetailsFragment;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.TextView;
import com.anyoption.common.service.results.ErrorMessage;
import com.anyoption.common.service.results.MethodResult;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.event.ProfileUpdateEvent;
import com.copyop.android.app.popup.EditAvatarDialogFragment;
import com.copyop.android.app.util.COBitmapUtils;
import com.copyop.android.app.util.COConstants;
import com.copyop.json.requests.UpdateAvatarMethodRequest;
import com.copyop.json.results.UserMethodResult;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class COPersonalDetailsFragment extends PersonalDetailsFragment{

	
	public static COPersonalDetailsFragment newInstance(Bundle bundle){
		COPersonalDetailsFragment coPersonalDetailsMenuFragment = new COPersonalDetailsFragment();
		coPersonalDetailsMenuFragment.setArguments(bundle);
		return coPersonalDetailsMenuFragment;
	}
	
	private String avatarURL;
	private Bitmap bitmap;
	private ImageView profilePictureImageView;
	private int avatarSize;
	
	@Override
	protected void setupUI() {
		super.setupUI();
		setupProfileAvatar();
		setupNicknameLayout();
	}
	
	@Override
	protected void setUpGreeting() {
		//Do nothing
	}
	
	@Override
	public void onHiddenChanged(boolean hidden) {
		if(!hidden){
			setupUI();
		}
		super.onHiddenChanged(hidden);
	}
	
	private void setupNicknameLayout(){
		View nickNameLayout = rootView.findViewById(R.id.personal_details_nickname_layout);
		View nickNameLayoutSeparatorTop = rootView.findViewById(R.id.personal_details_nickname_layout_separator_top);
		TextView nicknameTextView = (TextView) rootView.findViewById(R.id.personal_details_nickname_text_view);
		nicknameTextView.setText(COApplication.get().getProfile().getNickname());
		View nicknameWarning = rootView.findViewById(R.id.personal_details_nickname_warning);
		View nicknameEditArrow = rootView.findViewById(R.id.personal_details_nickname_arrow);
		int nicknameLayoutBG;
		if(COApplication.get().getProfile().isNicknameChanged()){
			nicknameLayoutBG = ColorUtils.getColor(R.color.transparent);
			nickNameLayoutSeparatorTop.setVisibility(View.GONE);
			nicknameWarning.setVisibility(View.GONE);
			nicknameEditArrow.setVisibility(View.GONE);
			nickNameLayout.setOnClickListener(null);
		}else{
			nicknameLayoutBG = ColorUtils.getColor(R.color.personal_details_nickname_layout_bg);
			nickNameLayoutSeparatorTop.setVisibility(View.VISIBLE);
			nicknameWarning.setVisibility(View.VISIBLE);
			nicknameEditArrow.setVisibility(View.VISIBLE);
			nickNameLayout.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					application.postEvent(new NavigationEvent(COScreen.CHANGE_NICKNAME, NavigationType.DEEP));
				}
			});
		}
		nickNameLayout.setBackgroundColor(nicknameLayoutBG);
		
	}
	
	private void setupProfileAvatar(){
		View profilePictureLayout = rootView.findViewById(R.id.personal_details_profile_picture_layout);
		profilePictureImageView = (ImageView) rootView.findViewById(R.id.personal_details_profile_picture);
		profilePictureImageView.setImageBitmap(null);
		avatarSize = application.getResources().getDimensionPixelSize(R.dimen.personal_details_profile_picture_size);
		profilePictureLayout.setOnClickListener(new OnClickListener() {		
			@Override
			public void onClick(View v) {
				EditAvatarDialogFragment editAvatarDialog = EditAvatarDialogFragment.newInstance(null);
				editAvatarDialog.setListener(new EditAvatarDialogFragment.EditAvatarDialogListener() {
					@Override
					public void onUpload(Bitmap bitmap){
						Log.d(TAG, "onUpload");
						COPersonalDetailsFragment.this.bitmap = bitmap;
						profilePictureImageView.setImageBitmap(bitmap);
						uploadAvatar(bitmap);
					}
					@Override
					public void onDrawMeMaleAvatar(String avatarURL) {
						COPersonalDetailsFragment.this.avatarURL = avatarURL;
						COApplication.get().setAvatarBitmap(null);
						updateProfileAvatar(avatarURL);
					}
					@Override
					public void onDrawMeFemaleAvatar(String avatarURL) {
						COPersonalDetailsFragment.this.avatarURL = avatarURL;
						COApplication.get().setAvatarBitmap(null);
						updateProfileAvatar(avatarURL);
						
					}
				});
				application.getFragmentManager().showDialogFragment(editAvatarDialog);
			}
		});
		
		if(COApplication.get().getAvatarBitmap() != null){
			profilePictureImageView.setImageBitmap(COApplication.get().getAvatarBitmap());
		}else{
			COBitmapUtils.loadBitmap(COApplication.get().getProfile().getAvatar(), profilePictureImageView, avatarSize, avatarSize);
		}
	}
	
	//#####################################################################
	//#########					REQUEST - RESPONSE				###########
	//#####################################################################	

	private void updateProfileAvatar(String avatarURL){
		UpdateAvatarMethodRequest request = new UpdateAvatarMethodRequest();
		request.setAvatar(avatarURL);
		application.getCommunicationManager().requestService(this, "updateProfileAvatarCallback", COConstants.SERVICE_UPDATE_COPYOP_VATAR, request, MethodResult.class, true, false);
	}
	
	public void updateProfileAvatarCallback(Object result){
		if(result != null && result instanceof MethodResult && ((MethodResult)result).getErrorCode() == Constants.ERROR_CODE_SUCCESS){
			COApplication.get().getProfile().setAvatar(avatarURL);
			COBitmapUtils.loadBitmap(avatarURL, profilePictureImageView, avatarSize, avatarSize);
			application.postEvent(new ProfileUpdateEvent());
		}
	}
	
	private void uploadAvatar(Bitmap bitmap){
		COApplication.get().getCommunicationManager().uploadAvatarImage(bitmap, this, "onAvatarUploaded");
	}
	
	public void onAvatarUploaded(UserMethodResult userMethodResult){
		Log.d(TAG, "onAvatarUploaded");
		if(userMethodResult != null){
			ErrorMessage message = userMethodResult.getErrorMessages()[0];
			String newAvatarURL = message.getMessage();
			COApplication.get().getProfile().setAvatar(newAvatarURL);
			COApplication.get().setAvatarBitmap(bitmap);
			application.postEvent(new ProfileUpdateEvent());
		}else{
			Log.e(TAG, "onAvatarUploaded -> UserMethodResult is null");
		}
	}
	
	//#####################################################################	
	
	
}
