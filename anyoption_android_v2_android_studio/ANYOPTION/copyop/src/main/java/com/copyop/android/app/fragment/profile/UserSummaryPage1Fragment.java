package com.copyop.android.app.fragment.profile;

import com.anyoption.android.app.fragment.BaseFragment;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COBitmapUtils;
import com.copyop.json.results.ProfileMethodResult;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class UserSummaryPage1Fragment extends BaseFragment {

	private ProfileMethodResult profileInfo;

	private View rootView;
	private ImageView avatarView;
	private TextView nameView;
	
	private boolean isMyProfile;

	/**
	 * Creates new instance of this fragment.
	 */
	public static UserSummaryPage1Fragment newInstance(Bundle bundle) {
		UserSummaryPage1Fragment fragment = new UserSummaryPage1Fragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.user_summary_page_1, container, false);
		avatarView = (ImageView) rootView.findViewById(R.id.user_profile_avatar);
		nameView = (TextView) rootView.findViewById(R.id.user_summary_name);

		return rootView;
	}

	public void setProfileInfo(ProfileMethodResult profileInfo, boolean isMyProfile) {
		this.profileInfo = profileInfo;
		this.isMyProfile = isMyProfile;
		updateInfo();
	}

	private void updateInfo() {
		if (profileInfo == null) {
			return;
		}

		if (profileInfo.getFirstName() != null && profileInfo.getLastName() != null) {
			nameView.setText(profileInfo.getFirstName() + " " + profileInfo.getLastName());
		} else {
			nameView.setText(profileInfo.getNickname());
		}
		
		setUpAvatar();
	}

	private void setUpAvatar() {
		avatarView.setImageDrawable(null);
		if (profileInfo.getAvatar() == null) {
			return;
		}

		if (isMyProfile) {
			setUpMyAvatar();
		} else {
			COBitmapUtils.loadBitmap(profileInfo.getAvatar(), avatarView);
		}
	}

	private void setUpMyAvatar() {
		avatarView.setImageDrawable(null);

		String uri = profileInfo.getAvatar();
		if (uri != null) {
			if (COApplication.get().getAvatarBitmap() != null) {
				avatarView.setImageBitmap(COApplication.get().getAvatarBitmap());
			} else {
				COBitmapUtils.loadBitmap(uri, avatarView);
			}
		}
	}

}
