package com.copyop.android.app.popup;

import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.popup.LoginRegisterDialogfragment;
import com.copyop.android.app.COApplication.COScreen;

import android.os.Bundle;

public class COLoginRegisterDialogfragment extends LoginRegisterDialogfragment {
	
	public final String TAG = COLoginRegisterDialogfragment.class.getSimpleName();

	/**
	 * Use this method to create new instance of the fragment.
	 * @return
	 */
	public static COLoginRegisterDialogfragment newInstance(Bundle bundle) {
		COLoginRegisterDialogfragment fragment = new COLoginRegisterDialogfragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	protected Screenable getRegisterScreen(){
		return COScreen.LSS_SIGNUP;
	}
	protected Screenable getLoginScreen(){
		return COScreen.LSS_LOGIN_SIGNUP;
	}
}
