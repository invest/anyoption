package com.copyop.android.app.fragment.login_register;

import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.widget.TextView;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.manager.COFacebookManager;

import org.json.JSONObject;

public class LSSSignupFragment extends NavigationalFragment {
	public static final String TAG = LSSSignupFragment.class.getSimpleName();
	
	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static LSSSignupFragment newInstance() {	
		return newInstance(new Bundle());
	}
	
	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static LSSSignupFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new LSSSignupFragment.");
		LSSSignupFragment signupFragment = new LSSSignupFragment();	
		signupFragment.setArguments(bundle);
		return signupFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		View rootView = inflater.inflate(R.layout.lss_signup_fragment, container, false);

		View fbButton = rootView.findViewById(R.id.lss_signup_fb_button);
		fbButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d(TAG, "Signup with fb");
				COFacebookManager.getUser(getActivity(), LSSSignupFragment.this, new COFacebookManager.UserListener() {
					@Override
					public void onUser(JSONObject user) {
						if (null != user) {
							Bundle bundle = new Bundle();
							bundle.putString(LSSSignupFinishFragment.FB_ID, user.optString("id"));
							//Split the full name:
							String[] names = user.optString("name").split(" ");
							String firstName = names[0];
							String lastName = (names.length > 1) ? names[1]:null;
							bundle.putString(LSSSignupFinishFragment.FB_FNAME, firstName);
							bundle.putString(LSSSignupFinishFragment.FB_LNAME, lastName);
							if (user.optString("email") != null) {
								bundle.putString(LSSSignupFinishFragment.FB_EMAIL, user.optString("email"));
							}
							application.postEvent(new NavigationEvent(COScreen.LSS_SIGNUP_FINISH, NavigationType.DEEP, bundle));
						}
					}
				});
			}
		});

		View continueButton = rootView.findViewById(R.id.lss_signup_continue_button);
		continueButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				application.postEvent(new NavigationEvent(COScreen.LSS_SIGNUP_EMPTY, NavigationType.DEEP));
			}
		});

		TextView aoLoginButton = (TextView) rootView.findViewById(R.id.lss_anyoption_account);
		Spannable spannable = TextUtils.decorateInnerText(aoLoginButton, null, aoLoginButton.getText().toString(), "any", Font.ROBOTO_BOLD, 0, 0);
		TextUtils.underlineText(aoLoginButton, spannable);
		aoLoginButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				application.postEvent(new NavigationEvent(COScreen.LSS_LOGIN_SIGNUP, NavigationType.HORIZONTAL));
			}
		});
		
		View supportButton = rootView.findViewById(R.id.lss_support);
		supportButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				application.postEvent(new NavigationEvent(Screen.SUPPORT, NavigationType.DEEP));
			}
		});

		return rootView;
	}
	
	@Override
	protected Screenable setupScreen() {
		return COScreen.LSS_SIGNUP;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	  	super.onActivityResult(requestCode, resultCode, data);
		application.getFacebookManager().onActivityResult(requestCode,resultCode,data);
	}
}