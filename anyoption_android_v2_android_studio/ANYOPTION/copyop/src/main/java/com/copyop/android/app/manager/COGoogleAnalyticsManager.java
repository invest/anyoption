package com.copyop.android.app.manager;

import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.manager.GoogleAnalyticsManager;
import com.copyop.android.app.COApplication.COScreen;
import com.google.android.gms.analytics.HitBuilders;

public class COGoogleAnalyticsManager extends GoogleAnalyticsManager {

    public COGoogleAnalyticsManager(int configResId) {
        super(configResId);
    }

    @Override
    public void onEvent(NavigationEvent event) {
        Screenable screenable = event.getToScreen();

        /*if (screenable == COScreen.LSS_LOGIN || screenable == COScreen.LSS_LOGIN_SIGNUP) {
            if (generalTracker != null && generalTracker.isInitialized()) {
                generalTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(EVENT_CATEGORY_NAVIGATION)
                        .setAction(EVENT_REGISTER_SCREEN)
                        .build());
            }
            if (additionalTracker != null && additionalTracker.isInitialized()) {
                additionalTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(EVENT_CATEGORY_NAVIGATION)
                        .setAction(EVENT_REGISTER_SCREEN)
                        .build());
            }
        }*/

        if (screenable == COScreen.LSS_SIGNUP || screenable == COScreen.LSS_SIGNUP_EMPTY) {
            if (generalTracker != null && generalTracker.isInitialized()) {
                generalTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(EVENT_CATEGORY_NAVIGATION)
                        .setAction(EVENT_REGISTER_SCREEN)
                        .build());
            }
            if (additionalTracker != null && additionalTracker.isInitialized()) {
                additionalTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(EVENT_CATEGORY_NAVIGATION)
                        .setAction(EVENT_REGISTER_SCREEN)
                        .build());
            }
        }
        super.onEvent(event);
    }


}
