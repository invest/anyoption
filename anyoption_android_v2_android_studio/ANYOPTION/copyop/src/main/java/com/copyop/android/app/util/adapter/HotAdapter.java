package com.copyop.android.app.util.adapter;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.TextView;
import com.anyoption.common.beans.base.Market;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.model.HotAssetSpecialist;
import com.copyop.android.app.model.HotCopier;
import com.copyop.android.app.model.HotGroup;
import com.copyop.android.app.model.HotGroup.Type;
import com.copyop.android.app.model.HotItem;
import com.copyop.android.app.model.HotTrade;
import com.copyop.android.app.model.HotTrader;
import com.copyop.android.app.popup.CopyDialogFragment;
import com.copyop.android.app.popup.DozedOffDialogFragment;
import com.copyop.android.app.popup.HotDropdownPopup;
import com.copyop.android.app.util.COBitmapUtils;
import com.copyop.android.app.util.COConstants;
import com.copyop.common.enums.base.UpdateTypeEnum;

import android.content.Context;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;

/**
 * Custom {@link BaseAdapter} for the Hot ListView.
 *
 * @author Anastas Arnaudov
 */
public class HotAdapter extends BaseExpandableListAdapter implements OnClickListener, TextUtils.OnInnerTextClickListener, BaseDialogFragment.OnDialogDismissListener {

    public final String TAG = HotAdapter.class.getSimpleName();

    public final int MAX_ITEMS_PER_GROUP = 3;

    public interface HotHoursFilterListener {
        public void onFilter(UpdateTypeEnum updateTypeEnum);
    }

    public static class GroupViewHolder {
        public int position;
        public TextView nameTextView;
        public View dropdownImageLayout;
        public ImageView dropdownImageView;
        public ImageView dropdownNewImageView;
        public TextView seeAllTextView;
    }

    public static class ChildViewHolder {
        public int groupPosition;
        public int position;
        public View borderTop;
        public View borderBottom;
        public TextView numberTextView;
        public ImageView avatarImageView;
        public TextView nicknameTextView;
        public TextView hitOrMarketTextView;
        public TextView profitTextView;
        public TextView copiersOrTimeTextView;
        public View lastTwoColumnsLayout;
        public View copyingButton;
        public View copyButton;
        public View lastSeparator;
    }

    private Context context;
    private HotGroup[] groupsArray;
    private LayoutInflater inflater;
    private SpannableStringBuilder groupSpannable;
    private int groupHeight;
    private HotHoursFilterListener hotHoursFilterListener;
    private View emptyView;

    public HotAdapter(HotGroup[] groupsArray, HotHoursFilterListener hotHoursFilterListener) {
        context = Application.get();
        inflater = (LayoutInflater) Application.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.hotHoursFilterListener = hotHoursFilterListener;
        this.groupsArray = groupsArray;
        this.groupSpannable = new SpannableStringBuilder();
        this.groupHeight = (int) context.getResources().getDimension(R.dimen.hot_group_layout_height);
        emptyView = new View(context);
        emptyView.setLayoutParams(new android.widget.AbsListView.LayoutParams(android.widget.AbsListView.LayoutParams.MATCH_PARENT, 0));
    }

    @Override
    public int getGroupCount() {
        return groupsArray.length;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int itemsCount = Math.min(MAX_ITEMS_PER_GROUP, groupsArray[groupPosition].getItems().size());

        HotGroup group = groupsArray[groupPosition];
        if (group.getType() == Type.COPIERS && itemsCount != 0) {
            // Add one more child for the footer of best copiers.
            itemsCount++;
        }

        return itemsCount;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupsArray[groupPosition];
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return groupsArray[groupPosition].getItems().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        HotGroup group = groupsArray[groupPosition];

        GroupViewHolder groupViewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.hot_group, parent, false);
            groupViewHolder = new GroupViewHolder();
            groupViewHolder.nameTextView = (TextView) convertView.findViewById(R.id.hot_group_name_text_view);
            groupViewHolder.dropdownImageLayout = convertView.findViewById(R.id.hot_group_dropdown_image_layout);
            groupViewHolder.dropdownImageView = (ImageView) convertView.findViewById(R.id.hot_group_dropdown_image_view);
            groupViewHolder.dropdownNewImageView = (ImageView) convertView.findViewById(R.id.hot_group_dropdown_new_image_view);
            groupViewHolder.seeAllTextView = (TextView) convertView.findViewById(R.id.hot_group_see_all_text_view);

            convertView.setTag(groupViewHolder);
        } else {
            groupViewHolder = (GroupViewHolder) convertView.getTag();
        }
        groupViewHolder.position = groupPosition;

        android.widget.AbsListView.LayoutParams params = (android.widget.AbsListView.LayoutParams) convertView.getLayoutParams();
        if (group.getItems() == null || group.getItems().isEmpty()) {
            params.height = 0;
            hideAllChildren((ViewGroup) convertView);
            convertView.setLayoutParams(params);
            convertView.setVisibility(View.GONE);
        } else {
            showAllChildren((ViewGroup) convertView);
            params.height = groupHeight;
            convertView.setLayoutParams(params);
            convertView.setVisibility(View.VISIBLE);
            try {
                setupGroup(group, groupViewHolder);
            } catch (Exception e) {
                Log.e(TAG, "getGroupView", e);
            }
        }

        return convertView;
    }

    private void hideAllChildren(ViewGroup viewGroup) {
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            View view = viewGroup.getChildAt(i);
            view.setVisibility(View.GONE);
        }
    }

    private void showAllChildren(ViewGroup viewGroup) {
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            View view = viewGroup.getChildAt(i);
            view.setVisibility(View.VISIBLE);
        }
    }

    private void setupGroup(HotGroup group, GroupViewHolder groupViewHolder) throws Exception {
        clearGroupSpannable();
        TextView nameTextView = groupViewHolder.nameTextView;
        View dropdownImageLayout = groupViewHolder.dropdownImageLayout;
        ImageView dropdownImageView = groupViewHolder.dropdownImageView;
        ImageView dropdownNewImageView = groupViewHolder.dropdownNewImageView;
        TextView seeAllTextView = groupViewHolder.seeAllTextView;

        HotAdapter.setupGroupName(context, group, nameTextView, groupSpannable, this);

        if (group.getType() == Type.ASSET_SPECIALISTS) {
            dropdownNewImageView.setVisibility(View.VISIBLE);
        } else {
            dropdownNewImageView.setVisibility(View.GONE);
        }

        TextUtils.underlineText(seeAllTextView);

        nameTextView.setTag(groupViewHolder);
        dropdownImageLayout.setTag(groupViewHolder);
        dropdownImageView.setTag(groupViewHolder);
        seeAllTextView.setTag(groupViewHolder);
        nameTextView.setOnClickListener(this);
        dropdownImageLayout.setOnClickListener(this);
        dropdownImageView.setOnClickListener(this);
        dropdownNewImageView.setOnClickListener(this);
        seeAllTextView.setOnClickListener(this);
    }

    public static void setupGroupName(Context context, HotGroup group, TextView nameTextView, Spannable groupSpannable, OnClickListener clickListener) throws Exception {
        String nameText = null;
        String nameBoldText = null;
        int iconResId = 0;

        //Check the group type:
        if (group.getType() == Type.TRADERS) {
            iconResId = R.drawable.hot_group_traders_icon;
            nameText = context.getString(R.string.hotGroupNameTraders, group.getLastHoursFormated());
            nameBoldText = context.getString(R.string.hotGroupTraders);
        } else if (group.getType() == Type.TRADES) {
            iconResId = R.drawable.hot_group_trades_icon;
            nameText = context.getString(R.string.hotGroupNameTrades, group.getLastHoursFormated());
            nameBoldText = context.getString(R.string.hotGroupTrades);
        } else if (group.getType() == Type.COPIERS) {
            iconResId = R.drawable.hot_group_copiers_icon;
            nameText = context.getString(R.string.hotGroupNameCopiers, group.getLastHoursFormated());
            nameBoldText = context.getString(R.string.hotGroupCopiers);
        } else if (group.getType() == Type.ASSET_SPECIALISTS) {
            iconResId = R.drawable.hot_group_asset_specialists_icon;
            nameText = context.getString(R.string.hotGroupNameAssetSpecialists, group.getLastHoursFormated());
            nameBoldText = context.getString(R.string.hotGroupAssetSpecialists);
        } else {
            throw new Exception("Unknown Hot Group Type!!!");
        }

        nameTextView.setCompoundDrawablesWithIntrinsicBounds(DrawableUtils.getDrawable(iconResId), null, null, null);
        TextUtils.decorateInnerText(nameTextView, groupSpannable, nameText, nameBoldText, Font.ROBOTO_MEDIUM, 0, 0);
        TextUtils.decorateInnerText(nameTextView, groupSpannable, nameText, group.getLastHoursFormated(), Font.ROBOTO_LIGHT, 0, context.getResources().getDimensionPixelSize(R.dimen.hot_group_name_hours_text));
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        HotGroup group = groupsArray[groupPosition];

        if (group.getType() == Type.COPIERS && isLastChild) {
            return getCopiersFooter(convertView, parent);
        }

        ChildViewHolder childViewHolder;
        // If the tag is null, this is the footer view and the layout must be recreated.
        if (convertView == null || convertView.getTag() == null) {
            convertView = inflater.inflate(R.layout.hot_item, parent, false);
            childViewHolder = new ChildViewHolder();
            childViewHolder.borderTop = convertView.findViewById(R.id.hot_item_border_top);
            childViewHolder.borderBottom = convertView.findViewById(R.id.hot_item_border_bottom);
            childViewHolder.numberTextView = (TextView) convertView.findViewById(R.id.hot_item_number_text_view);
            childViewHolder.avatarImageView = (ImageView) convertView.findViewById(R.id.hot_item_icon_image_view);
            childViewHolder.nicknameTextView = (TextView) convertView.findViewById(R.id.hot_item_nickname_text_view);
            childViewHolder.hitOrMarketTextView = (TextView) convertView.findViewById(R.id.hot_item_hit_or_market_text_view);
            childViewHolder.profitTextView = (TextView) convertView.findViewById(R.id.hot_item_profit_text_view);
            childViewHolder.copiersOrTimeTextView = (TextView) convertView.findViewById(R.id.hot_item_copiers_or_time_text_view);
            childViewHolder.lastTwoColumnsLayout = convertView.findViewById(R.id.hot_item_last_2_columns);
            childViewHolder.copyingButton = convertView.findViewById(R.id.hot_item_copying_button);
            childViewHolder.copyButton = convertView.findViewById(R.id.hot_item_asset_specialist_copy_button_layout);
            childViewHolder.lastSeparator = convertView.findViewById(R.id.last_hot_item_separator);

            convertView.setTag(childViewHolder);
        } else {
            childViewHolder = (ChildViewHolder) convertView.getTag();
        }

        childViewHolder.groupPosition = groupPosition;
        childViewHolder.position = childPosition;

        HotItem item = group.getItems().get(childPosition);
        try {
            HotAdapter.setupChild(context, group, item, childViewHolder, this);
        } catch (Exception e) {
            Log.e(TAG, "getChildView", e);
        }

        return convertView;
    }

    public static void setupChild(Context context, HotGroup group, HotItem item, ChildViewHolder childViewHolder, OnClickListener clickListener) throws Exception {
        //Set up the borders:
        int borderColor;
        int borderHeight;
        if (item.getUserId() == Application.get().getUser().getId()) {
            borderColor = ColorUtils.getColor(R.color.hot_item_border_horizontal_mine);
            borderHeight = context.getResources().getDimensionPixelSize(R.dimen.hot_item_border_horizontal_mine_size);
        } else {
            borderColor = ColorUtils.getColor(R.color.hot_item_border_horizontal);
            borderHeight = context.getResources().getDimensionPixelSize(R.dimen.hot_item_border_horizontal_size);
        }
        childViewHolder.borderTop.setBackgroundColor(borderColor);
        LayoutParams borderTopParams = (LayoutParams) childViewHolder.borderTop.getLayoutParams();
        if (borderTopParams != null) {
            borderTopParams.height = borderHeight;
            childViewHolder.borderTop.setLayoutParams(borderTopParams);

        }
        childViewHolder.borderBottom.setBackgroundColor(borderColor);
        LayoutParams borderBottomParams = (LayoutParams) childViewHolder.borderBottom.getLayoutParams();
        if (borderBottomParams != null) {
            borderBottomParams.height = borderHeight;
            childViewHolder.borderBottom.setLayoutParams(borderBottomParams);

        }
        /////////////////////

        //Set up the row number:
        childViewHolder.numberTextView.setText(String.valueOf(childViewHolder.position + 1));
        /////////////////////

        //Set up the icon:
        int iconSize = context.getResources().getDimensionPixelSize(R.dimen.hot_item_avatar_icon_size);
        COBitmapUtils.loadBitmap(item.getIconURL(), childViewHolder.avatarImageView, iconSize, iconSize);
        childViewHolder.avatarImageView.setTag(childViewHolder);
        childViewHolder.avatarImageView.setOnClickListener(clickListener);
        /////////////////////

        //Set up the nickname:
        childViewHolder.nicknameTextView.setText(item.getNickname());
        childViewHolder.nicknameTextView.setTag(childViewHolder);
        childViewHolder.nicknameTextView.setOnClickListener(clickListener);
        ////////////////////

        //Set up the profit:
        childViewHolder.profitTextView.setText(item.getProfitFormated());
        ////////////////////


        //Set up the "Hit Percent" OR the "Market Name":
        if (group.getType() == Type.TRADERS) {
            HotTrader hotTrader = (HotTrader) item;
            int hitTextSize = (int) context.getResources().getDimension(R.dimen.hot_item_hit_text);
            childViewHolder.hitOrMarketTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, hitTextSize);
            childViewHolder.hitOrMarketTextView.setText(hotTrader.getHitPercentFormated());
            childViewHolder.lastSeparator.setVisibility(View.VISIBLE);
        } else if (group.getType() == Type.TRADES) {
            HotTrade hotTrade = (HotTrade) item;
            int marketTextSize = (int) context.getResources().getDimension(R.dimen.hot_item_market_text);
            childViewHolder.hitOrMarketTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, marketTextSize);
            childViewHolder.hitOrMarketTextView.setText(hotTrade.getMarketName());
            childViewHolder.hitOrMarketTextView.setTag(childViewHolder);
            childViewHolder.hitOrMarketTextView.setOnClickListener(clickListener);
            childViewHolder.lastSeparator.setVisibility(View.VISIBLE);
        } else if (group.getType() == Type.COPIERS) {
            HotCopier hotCopier = (HotCopier) item;
            childViewHolder.lastTwoColumnsLayout.setVisibility(View.GONE);
            childViewHolder.copyingButton.setVisibility(View.VISIBLE);
            childViewHolder.copyingButton.setTag(childViewHolder);
            childViewHolder.copyingButton.setOnClickListener(clickListener);
            childViewHolder.lastSeparator.setVisibility(View.GONE);
        } else if (group.getType() == Type.ASSET_SPECIALISTS) {
            HotAssetSpecialist hotAssetSpecialist = (HotAssetSpecialist) item;
            int marketTextSize = (int) context.getResources().getDimension(R.dimen.hot_item_market_text);
            childViewHolder.hitOrMarketTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, marketTextSize);
            childViewHolder.hitOrMarketTextView.setText(hotAssetSpecialist.getMarketName());
            childViewHolder.hitOrMarketTextView.setTag(childViewHolder);
            childViewHolder.hitOrMarketTextView.setOnClickListener(clickListener);
            childViewHolder.copiersOrTimeTextView.setVisibility(View.GONE);
            childViewHolder.copyButton.setVisibility(View.VISIBLE);
            childViewHolder.copyButton.setTag(childViewHolder);
            childViewHolder.copyButton.setOnClickListener(clickListener);
            childViewHolder.lastSeparator.setVisibility(View.GONE);
        } else {
            throw new Exception("No such Hot Group!!!");
        }

        if (group.getType() != Type.COPIERS) {
            childViewHolder.lastTwoColumnsLayout.setVisibility(View.VISIBLE);
            childViewHolder.copyingButton.setVisibility(View.GONE);
        } else {
            childViewHolder.lastTwoColumnsLayout.setVisibility(View.GONE);
            childViewHolder.copyingButton.setVisibility(View.VISIBLE);
        }

        if (group.getType() != Type.ASSET_SPECIALISTS) {
            childViewHolder.copyButton.setVisibility(View.GONE);
            childViewHolder.copiersOrTimeTextView.setVisibility(View.VISIBLE);
        } else {
            childViewHolder.copyButton.setVisibility(View.VISIBLE);
            childViewHolder.copiersOrTimeTextView.setVisibility(View.GONE);
        }

        ///////////////////////////////////////////////////

        // Set up the "Time" OR the "Copiers"
        if (group.getType() == Type.TRADERS) {
            HotTrader hotTrader = (HotTrader) item;
            int copiersTextSize = (int) context.getResources().getDimension(R.dimen.hot_item_copiers_text);
            childViewHolder.copiersOrTimeTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, copiersTextSize);
            childViewHolder.copiersOrTimeTextView.setText(String.valueOf(hotTrader.getCopiersCount()));
        } else if (group.getType() == Type.TRADES) {
            HotTrade hotTrade = (HotTrade) item;
            int timeTextSize = (int) context.getResources().getDimension(R.dimen.hot_item_time_text);
            childViewHolder.copiersOrTimeTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, timeTextSize);
            String timeFormated = hotTrade.getTimeFormated();
            String timeWithNewLine = timeFormated;
            if(timeFormated.length() > 6) {
                timeWithNewLine = timeFormated.substring(0, 6) + "\n" + timeFormated.substring(6, timeFormated.length());
            }
            childViewHolder.copiersOrTimeTextView.setText(timeWithNewLine);
        }
        //////////////////////
    }

    private View getCopiersFooter(View convertView, ViewGroup parent) {
        // Check if the view is layout for copier
        if (convertView == null || convertView.getTag() != null) {
            TextView footerView = (TextView) inflater.inflate(R.layout.hot_best_copiers_footer, parent, false);
            String boldText = context.getString(R.string.hotNoChainOfCopiedOptionsBold);
            TextUtils.makeClickableInnerText(footerView, null, footerView.getText().toString(), boldText, Font.ROBOTO_BOLD, 0, 0, true, null);

            return footerView;
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private void clearGroupSpannable() {
        groupSpannable.clear();
        groupSpannable.clearSpans();
    }

    @Override
    public void onClick(View view) {
        if (view.getTag() instanceof GroupViewHolder) {
            GroupViewHolder groupViewHolder = (GroupViewHolder) view.getTag();
            int position = groupViewHolder.position;
            HotGroup group = groupsArray[position];
            if (view.getId() == groupViewHolder.nameTextView.getId() || view.getId() == groupViewHolder.dropdownImageView.getId() || view.getId() == groupViewHolder.dropdownImageLayout.getId()) {
                if (group.getFilters() != null && group.getFilters().size() > 1) {
                    HotDropdownPopup.showPopup(group, groupViewHolder.dropdownImageView, hotHoursFilterListener);
                }
            } else if (view.getId() == groupViewHolder.seeAllTextView.getId()) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(COConstants.EXTRA_HOT_GROUP, group);
                Application.get().postEvent(new NavigationEvent(COScreen.HOT_ALL, NavigationType.DEEP, bundle));
            }
        } else if (view.getTag() instanceof ChildViewHolder) {
            ChildViewHolder childViewHolder = (ChildViewHolder) view.getTag();
            HotItem item = groupsArray[childViewHolder.groupPosition].getItems().get(childViewHolder.position);
            if (view.getId() == childViewHolder.avatarImageView.getId() || view.getId() == childViewHolder.nicknameTextView.getId()) {
                HotAdapter.goToProfile(item.getUserId());
            } else if (view.getId() == childViewHolder.hitOrMarketTextView.getId()) {
                if (item instanceof HotTrade) {
                    HotTrade hotTrade = (HotTrade) item;
                    HotAdapter.goToMarket(hotTrade.getMarket());
                } else if (item instanceof HotAssetSpecialist) {
                    HotAssetSpecialist hotAssetSpecialist = (HotAssetSpecialist) item;
                    HotAdapter.goToMarket(hotAssetSpecialist.getMarket());
                }
            } else if (view.getId() == childViewHolder.copyingButton.getId()) {
                if (item instanceof HotCopier) {
                    HotCopier hotCopier = (HotCopier) item;
                    HotAdapter.goToCopyingList(hotCopier.getUserId());
                }
            } else if (view.getId() == childViewHolder.copyButton.getId()) {
                if (item instanceof HotAssetSpecialist) {
                    HotAssetSpecialist hotAssetSpecialist = (HotAssetSpecialist) item;
                    HotAdapter.showCopy(hotAssetSpecialist);
                }
            }
        }
    }

    @Override
    public void onInnerTextClick(android.widget.TextView textView, String text, String innerText) {
        /*if(textView.getTag() instanceof GroupViewHolder){
            GroupViewHolder groupViewHolder = (GroupViewHolder) textView.getTag();
			HotGroup group = groupsArray[groupViewHolder.position];
			//TODO TESTING
			Button button = new Button(context);
			button.setText(group.getName());
			int rightOffset = TextUtils.getInnerTextWidth(textView, innerText);
			PopupWithAnchor popup = new PopupWithAnchor(textView, textView.getWidth() - rightOffset, 0, button);
			popup.show();
			///////////
		}*/
    }

    protected static void goToProfile(long userId) {
        Bundle bundle = new Bundle();
        bundle.putLong(COConstants.EXTRA_PROFILE_ID, userId);
        Application.get().postEvent(new NavigationEvent(COScreen.USER_PROFILE, NavigationType.DEEP, bundle));
    }

    protected static void goToMarket(Market market) {
        if (market != null) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.EXTRA_MARKET, market);
            Application.get().postEvent(new NavigationEvent(Screen.ASSET_VIEW_PAGER, NavigationType.DEEP, bundle));
        }
    }

    protected static void goToCopyingList(long userId) {
        Bundle args = new Bundle();
        args.putLong(COConstants.EXTRA_PROFILE_ID, userId);
        Application.get().postEvent(new NavigationEvent(COScreen.COPYING_LIST, NavigationType.DEEP, args));
    }

    protected static void showCopy(HotAssetSpecialist hotAssetSpecialist) {
            boolean isCopying = (COApplication.get().isCopying(hotAssetSpecialist.getUserId()));
        if((!isCopying && RegulationUtils.checkRegulationAndPepAndThresholdStatusAndDoNecessary(false)) || isCopying) {
            if (hotAssetSpecialist.isUserFrozen() && !isCopying) {
                Application.get().getFragmentManager()
                        .showDialogFragment(DozedOffDialogFragment.class, new Bundle());
            } else {
                Bundle args = new Bundle();
                args.putBoolean(COConstants.EXTRA_IS_COPYING, isCopying);
                args.putBoolean(COConstants.EXTRA_IS_FROZEN, hotAssetSpecialist.isUserFrozen());
                args.putSerializable(COConstants.EXTRA_MARKET, hotAssetSpecialist.getMarket());
                args.putLong(COConstants.EXTRA_PROFILE_ID, hotAssetSpecialist.getUserId());
                args.putString(COConstants.EXTRA_NICKNAME, hotAssetSpecialist.getNickname());
                Application.get().getFragmentManager().showDialogFragment(CopyDialogFragment.class, args);
            }
        }
    }

    @Override
    public void onDismiss() {

    }


}
