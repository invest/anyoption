package com.copyop.android.app.popup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.anyoption.android.R;
import com.anyoption.android.app.popup.QuestionnaireBlockedOkDialogFragment;
import com.anyoption.android.app.popup.QuestionnairePEPBlockedOkDialogFragment;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;

public class COQuestionnairePEPBlockedOkDialogFragment extends QuestionnairePEPBlockedOkDialogFragment {

	public static final String TAG = COQuestionnairePEPBlockedOkDialogFragment.class.getSimpleName();
	
	public static COQuestionnairePEPBlockedOkDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new COQuestionnaireRestrictedOkDialogFragment.");
		COQuestionnairePEPBlockedOkDialogFragment fragment = new COQuestionnairePEPBlockedOkDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, true);
		bundle.putInt(ARG_BACKGROUND_COLOR_KEY, R.color.black_alpha_40);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if(args != null){
			setCancelable(args.getBoolean(Constants.EXTRA_CANCELABLE, false));
		}
	}

	@Override
	public void decrateText() {
		String userEmail = application.getUser().getEmail();
		secondMessageTextView.setText(secondMessageTextView.getText().toString() + " " + userEmail);
		TextUtils.decorateInnerText(secondMessageTextView, null, secondMessageTextView.getText().toString(), userEmail, FontsUtils.Font.ROBOTO_BOLD, 0, 0);
		TextUtils.makeClickableInnerText(thirdMessageTextView, null, thirdMessageTextView.getText().toString(), getString(R.string.questionnaire_pep_blocked_popup_third_message_support_link), FontsUtils.Font.ROBOTO_REGULAR, 0, R.color.link_color_1, false, supportTeamLinkListener);
	}

	@Override
	protected void onCreateContentView(View contentView) {
		super.onCreateContentView(contentView);	
	}
	
}
