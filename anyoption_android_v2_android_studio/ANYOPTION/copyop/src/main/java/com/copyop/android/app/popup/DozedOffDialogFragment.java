package com.copyop.android.app.popup;

import com.anyoption.android.app.Application;
import com.copyop.android.app.R;

import android.os.Bundle;
import android.util.Log;

public class DozedOffDialogFragment extends TextDialogFragment {

	private static final String TAG = DozedOffDialogFragment.class.getSimpleName();

	/**
	 * Use this method to create new instance of the dialog.
	 * 
	 * @return
	 */
	public static DozedOffDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new DozedOffDialogFragment.");
		Application application = Application.get();
		bundle.putString(TITLE_ARG_KEY, application.getString(R.string.lowActivity));
		bundle.putString(TEXT_ARG_KEY, application.getString(R.string.lowActivityDesc));
		bundle.putString(DISMISS_TEXT_ARG_KEY,
				application.getString(R.string.dismiss));
		bundle.putBoolean(IS_DISMISS_GRAY_ARG_KEY, true);

		DozedOffDialogFragment fragment = new DozedOffDialogFragment();
		fragment.setArguments(bundle);

		return fragment;
	}

}