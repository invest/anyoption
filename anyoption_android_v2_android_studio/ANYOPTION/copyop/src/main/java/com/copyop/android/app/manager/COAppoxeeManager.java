package com.copyop.android.app.manager;

import com.anyoption.android.app.manager.AppoxeeManager;
import com.copyop.android.app.COApplication;

public class COAppoxeeManager extends AppoxeeManager {

	public static final String APP_KEY 					= "545a40ad25da09.24375255";
	public static final String SECRET_KEY 				= "545a40ad25db55.32500865";
	
	public COAppoxeeManager(COApplication application) {
		super(application);
	}
	
	protected String getApiKey(){
		return COAppoxeeManager.APP_KEY;
	}
	
	protected String getSecretKey(){
		return COAppoxeeManager.SECRET_KEY;
	}
	
}
