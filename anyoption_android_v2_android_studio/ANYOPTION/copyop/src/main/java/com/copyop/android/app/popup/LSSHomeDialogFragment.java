package com.copyop.android.app.popup;

import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.TextUtils;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Spannable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class LSSHomeDialogFragment extends BaseDialogFragment {
	private static final String TAG = LSSHomeDialogFragment.class.getSimpleName();
	private NavigationEvent nextScreenEvent;
	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static LSSHomeDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new LSSHomeDialogFragment.");
		if (null == bundle) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		LSSHomeDialogFragment fragment = new LSSHomeDialogFragment();
		fragment.setArguments(bundle);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		nextScreenEvent = null;
	}
	
	/**
	 * Set up content.
	 * 
	 * @param contentView
	 */
	@Override
	protected void onCreateContentView(View contentView) {
		View getStarted = contentView.findViewById(R.id.lss_get_started_button);
		getStarted.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				nextScreenEvent = new NavigationEvent(COScreen.LSS_SIGNUP, NavigationType.HORIZONTAL);
				dismiss();
			}
		});

		OnClickListener loginClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				nextScreenEvent = new NavigationEvent(COScreen.LSS_LOGIN_SIGNUP, NavigationType.HORIZONTAL);
				dismiss();
			}
		};

		View login = contentView.findViewById(R.id.lss_login_button);
		login.setOnClickListener(loginClickListener);

		TextView aoAccountView = (TextView) contentView.findViewById(R.id.lss_anyoption_account);
		Spannable spannable = TextUtils.decorateInnerText(aoAccountView, null, aoAccountView.getText().toString(), "any", Font.ROBOTO_BOLD, 0, 0);
		TextUtils.underlineText(aoAccountView, spannable);
		aoAccountView.setOnClickListener(loginClickListener);
	}
	
	@Override
	public void onDismiss(DialogInterface dialog) {
		if (nextScreenEvent != null) {
			application.postEvent(nextScreenEvent);
			super.onDismiss(dialog);
		}else{
			application.getCurrentActivity().finish();
		}
	}
	
	@Override
	protected int getContentLayout() {
		return R.layout.lss_home_dialog;
	}
	
}