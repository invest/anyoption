package com.copyop.android.app.popup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.popup.BonusesToClaimDialogFragment;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.copyop.android.app.R;

public class COBonusesToClaimDialogFragment extends BonusesToClaimDialogFragment {

	public static final String TAG = COBonusesToClaimDialogFragment.class.getSimpleName();

	private Button claimOrListButton;

	public static COBonusesToClaimDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new COBonusesToClaimDialogFragment.");
		COBonusesToClaimDialogFragment fragment = new COBonusesToClaimDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, true);
		bundle.putInt(ARG_BACKGROUND_COLOR_KEY, R.color.black_alpha_40);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if(args != null){
			setCancelable(args.getBoolean(Constants.EXTRA_CANCELABLE, false));
		}
	}

	@Override
	protected int getContentLayout() {
		return R.layout.popup_bonuses_to_claim;
	}

	@Override
	protected void onCreateContentView(View contentView) {
		setupClaimOrListButton(contentView);

		firstMessageTextView = (TextView) contentView.findViewById(R.id.bonuses_to_claim_popup_first_message);
		secondMessageTextView = (TextView) contentView.findViewById(R.id.bonuses_to_claim_popup_second_message);
		bonusTitle = (TextView) contentView.findViewById(R.id.bonuses_to_claim_bonus_info_text);

		bonusesInfo = contentView.findViewById(R.id.bonuses_to_claim_bonus_info);
		bonusesInfoHorizontalLine = contentView.findViewById(R.id.bonuses_to_claim_bonus_info_horizontal_line);

		if(isSingle) {
			firstMessageTextView.setText(R.string.bonuses_to_claim_popup_single_first_message);
			secondMessageTextView.setText(R.string.bonuses_to_claim_popup_single_second_message);
			claimOrListButton.setText(R.string.bonuses_to_claim_popup_single_button_text);
			bonusesInfo.setVisibility(View.VISIBLE);
			bonusesInfoHorizontalLine.setVisibility(View.VISIBLE);
			bonusTitle.setText(bonus.getBonusDescriptionTxt());
		} else {
			firstMessageTextView.setText(R.string.bonuses_to_claim_popup_multiple_first_message);
			secondMessageTextView.setText(R.string.bonuses_to_claim_popup_multiple_second_message);
			claimOrListButton.setText(R.string.bonuses_to_claim_popup_multiple_button_text);
			bonusesInfo.setVisibility(View.GONE);
			bonusesInfoHorizontalLine.setVisibility(View.GONE);
			bonusTitle.setText("");
		}

		TextUtils.decorateInnerText(firstMessageTextView, null, firstMessageTextView.getText().toString(), firstMessageTextView.getText().toString(), FontsUtils.Font.ROBOTO_BOLD, 0, 0);
	}

	private void setupClaimOrListButton(View contentView) {
		claimOrListButton = (Button) contentView.findViewById(R.id.claim_or_list_button);
		claimOrListButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				if (isSingle) {
					if (RegulationUtils.checkRegulationStatusAndDoNecessary(false)) {
						Bundle bundle = new Bundle();
						bundle.putSerializable(Constants.EXTRA_BONUS, bonus);
						bundle.putStringArray(Constants.EXTRA_BONUS_FACTOR, turnoverFactor);
						application.postEvent(new NavigationEvent(Application.Screen.BONUS_DETAILS, NavigationEvent.NavigationType.DEEP, bundle));
					}
				} else {
					application.postEvent(new NavigationEvent(Application.Screen.BONUSES, NavigationEvent.NavigationType.DEEP));
				}
			}
		});
	}
}
