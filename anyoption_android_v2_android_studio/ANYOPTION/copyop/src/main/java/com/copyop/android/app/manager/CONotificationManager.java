package com.copyop.android.app.manager;

import com.anyoption.android.app.event.UserUpdatedEvent;
import com.anyoption.android.app.manager.NotificationManager;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.User;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.event.InAppNotificationEvent;
import com.copyop.android.app.model.Update;
import com.copyop.android.app.util.ConvertUtils;
import com.copyop.android.app.widget.notification.CONotificationInvestOK;
import com.copyop.android.app.widget.notification.CONotificationShoppingBag;
import com.copyop.android.app.widget.notification.NotificationABSuccess;
import com.copyop.android.app.widget.notification.NotificationBalance;
import com.copyop.android.app.widget.notification.NotificationCoins;
import com.copyop.android.app.widget.notification.NotificationCopiedInvestment;
import com.copyop.android.app.widget.notification.NotificationNearlyFull;
import com.copyop.android.app.widget.notification.NotificationNewInvestment;
import com.copyop.android.app.widget.notification.NotificationNowAvailable;
import com.copyop.common.enums.base.QueueTypeEnum;
import com.lightstreamer.ls_client.UpdateInfo;

import android.util.Log;

public class CONotificationManager extends NotificationManager{

	public CONotificationManager(COApplication application) {
		super(application);
		registerForEvents();
	}

	private void registerForEvents() {
		application.registerForEvents(this, InAppNotificationEvent.class);
	}

	//#####################################################################
	//#########					EVENTS CALLBACKs				###########
	//#####################################################################
	
	public void onEvent(InAppNotificationEvent event) {
		UpdateInfo updateInfo = event.getUpdate();
		try {
			Update update = ConvertUtils.convertLSUpdateToUpdate(updateInfo, QueueTypeEnum.IN_APP_STRIP);
			String message = update.getBody();
			
			switch (update.getUpdateType()) {
		
				case AB_BALANCE_LESS_MIN_COPIED:
					showNotificationBalance(message);
					break;
				case AB_BALANCE_LESS_MIN_COPYING:
					showNotificationBalance(message);
					break;
				case AB_BALANCE_LESS_MIN_WARN:
					showNotificationBalance(message);
					break;
				case AB_IM_FULLY_COPIED:
					showNotificationIamFullyCopied(message);
					break;
				case AB_IM_TOP_PROFITABLE:
					showNotificationIamTopProfitable(message);
					break;
				case AB_I_COPIED_INVESTMENT:
					showNotificationCopiedInvestment(message, update.getMarket());
					if(application.getUser() != null){
						User user = application.getUser();
						user.setOpenedInvestmentsCount(user.getOpenedInvestmentsCount() + 1);
						application.postEvent(new UserUpdatedEvent());
					}
					break;
				case AB_MY_EXPIRY:
					Log.d(TAG, "Ignoring message AB_MY_EXPIRY. Shopping Bag is used instead of this.");
					break;
				case AB_REACHED_COINS:
					showNotificationCoins(message);
					break;
				case FB_NEW_INVESTMENT:
					showNotificationNewInvestment(message, update.getMarket(), update.getInvestmentId(), update.getDirection(), update.getAmount());
					break;
				case W_NEARLY_FULL:
					showNotificationNearlyFull(message, update.getUserId());
					break;
				case W_NEW_INVESTMENT:
					showNotificationNewInvestment(message, update.getMarket(), update.getInvestmentId(), update.getDirection(), update.getAmount());
					break;
				case W_NOW_AVAILABLE:
					showNotificationNowAvailable(message, update.getUserId());
					break;
				default:
					Log.e(TAG, "Unsupported Notification Type = " + update.getUpdateType());
					break;
			}
		} catch (Exception e) {
			Log.e(TAG + " lightstreamer", "Problem processing updateItem.", e);
		}
	}
	
	//#####################################################################
	
	/**
	 * Displays "Balance Warning" Notification in the Notification bar.
	 * The Notification has a "Deposit" button which navigates to the Deposit Screen.
	 */
	public void showNotificationBalance(String message){
		showNotification(new NotificationBalance(message));
	}
	
	/**
	 * Displays "Coins" Notification in the Notification bar.
	 * The Notification has a "View Coins" button which navigates to the Coins Screen.
	 */
	public void showNotificationCoins(String message){
		showNotification(new NotificationCoins(message));
	}
	
	/**
	 * Displays "Nearly Full" Notification in the Notification bar.
	 * The Notification has a "PROFILE" button.
	 */
	public void showNotificationNearlyFull(String message, long userId){
		showNotification(new NotificationNearlyFull(message, userId));
	}
	
	/**
	 * Displays "Now Available" Notification in the Notification bar.
	 * The Notification has a "PROFILE" button.
	 */
	public void showNotificationNowAvailable(String message, long userId){
		showNotification(new NotificationNowAvailable(message, userId));
	}

	@Override
	public void showNotificationInvestOK(final Investment investment){
		showNotification(new CONotificationInvestOK(investment));
	}

	/**
	 * Display "I am fully copied" Notification in the notification bar.
	 * @param investment
	 */
	public void showNotificationIamFullyCopied(String message){
		showNotification(new NotificationABSuccess(message));
	}
	
	/**
	 * Display "I am top profitable" Notification in the notification bar.
	 * @param investment
	 */
	public void showNotificationIamTopProfitable(String message){
		showNotification(new NotificationABSuccess(message));
	}
	
	/**
	 * Display "New Investment" Notification in the notification bar.
	 * @param investment
	 */
	public void showNotificationNewInvestment(String message, Market market, long investmentId, int direction, double amount){
		showNotification(new NotificationNewInvestment(message, market, investmentId, direction, amount));
	}
	
	/**
	 * Display "Copied Investment" Notification in the notification bar.
	 * @param investment
	 */
	public void showNotificationCopiedInvestment(String message, Market market){
		showNotification(new NotificationCopiedInvestment(message, market));
	}

	@Override
	public void showNotificationShoppingBag(int expiredOptionsCount, String marketName, long profitAmount, boolean isWinning){
		showNotification(new CONotificationShoppingBag(expiredOptionsCount, marketName, profitAmount, isWinning));
	}
}
