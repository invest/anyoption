package com.copyop.android.app.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.common.beans.base.Currency;


public class COAmountUtil extends AmountUtil {

	public static String patternDigits = "^[0-9]+$";

	/**
	 * Returns the formatted amount with + / - sign.
	 */
	public static String getFormattedProfitLossAmount(long amount, Currency currency) {
		StringBuilder sb = new StringBuilder();
		if (amount > 0) {
			sb.append("+");
		} else if (amount < 0) {
			sb.append("-");
		}

		String patternNoDigits = AMOUNT_PATTERN_NO_DIGITS;
		StringBuilder pattern = new StringBuilder(patternNoDigits);
		int decimalPointDigits = currency.getDecimalPointDigits();
		if (decimalPointDigits > 0) {
			pattern.append('.');
			for (int i = 0; i < decimalPointDigits; i++) {
				pattern.append('0');
			}
		}

		// Explicitly setting the separators
		DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols();
		formatSymbols.setDecimalSeparator('.');
		formatSymbols.setGroupingSeparator(',');

		DecimalFormat format = new DecimalFormat(pattern.toString(), formatSymbols);
		String formattedAmount = format.format(Math.abs(getDoubleAmount(amount)));

		if (currency.getIsLeftSymbolBool()) {
			sb.append(currency.getSymbol());
			sb.append(formattedAmount);
		} else {
			sb.append(formattedAmount);
			sb.append(currency.getSymbol());
		}

		return sb.toString();
	}

	/**
	 * Formats the profit, displayed in the Hot ListView.
	 * @param profit Example : "+$33,3K", "+$3,33M"
	 * @return
	 */
	public static String formatHotProfit(double profit){
		int numberOfDigits = 3;
		char thousandChar = 'K';
		char millionsChar = 'M';
		String currencySymbol = Application.get().getUser().getCurrency().getSymbol(); 
		
		//Format the number with separator:
		String profitFormatted = NumberFormat.getNumberInstance(Application.get().getLocale()).format(profit);
		/////////////////
		
		//Extract the first 3 digits:
		if(profitFormatted.length() > 3){
			if(profitFormatted.substring(0, numberOfDigits).matches(patternDigits)){
				profitFormatted = profitFormatted.substring(0, numberOfDigits);
			}else{
				profitFormatted = profitFormatted.substring(0, numberOfDigits+1);
			}
		}
		//////////////
		
		//Add "K" or "M" suffix:
		if(profit >= 1000000l){
			profitFormatted = profitFormatted + millionsChar;
		}else if(profit >= 1000l){
			profitFormatted = profitFormatted + thousandChar;
		}
		///////////////
		
		//Add the currency sign:
		if(Application.get().getUser().getCurrency().getIsLeftSymbolBool()){
			profitFormatted = currencySymbol + profitFormatted;
		}else{
			profitFormatted = profitFormatted + currencySymbol;
		}
		//////////////
		
		// Add the plus sign:
		profitFormatted = '+' + profitFormatted;
		////////////////
		
		//Final touch:
		profitFormatted = profitFormatted.replaceAll(",", ".");
		/////////////
		
		return profitFormatted;
	}
	
	public static String formatCostEarnedAmount(double amount){
		String formattedAmount = getUserFormattedAmount(amount);
		
		formattedAmount = formattedAmount.replace("-", "");
		if(amount > 0){
			formattedAmount = "+" + formattedAmount;
		}else if(amount < 0){
			formattedAmount = "-" + formattedAmount;
		}
		
		return formattedAmount;
	}
	
	/**
	 * Formats the profit based on user currency and the profit sign:
	 * Example : -$300.42
	 * @param profit
	 * @return
	 */
	public static String formatSummaryWinLossProfit(double profit){
		StringBuilder sb = new StringBuilder();
		String sign = "";
		if (profit > 0) {
			sign = "+";
		} else if (profit < 0) {
			sign = "-";
		}
		sb.append(sign);
		profit = Math.abs(profit);
		sb.append(getUserFormattedAmount(profit));
		return sb.toString();
	}
	
}
