package com.copyop.android.app.util.adapter;

import java.util.ArrayList;

import com.anyoption.android.app.popup.BaseDialogFragment.OnDialogDismissListener;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.adapter.AssetOptionsListAdapter;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COInvestmentUtil;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

public class COAssetOptionsListAdapter extends AssetOptionsListAdapter implements OnClickListener,
		OnDialogDismissListener {

	private boolean isDialogShown;

	public COAssetOptionsListAdapter(Market market, ArrayList<Investment> openOptions,
			ArrayList<Investment> settledOptions) {
		super(market, openOptions, settledOptions);
	}

	public COAssetOptionsListAdapter(Market market) {
		super(market);
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
			View convertView, ViewGroup parent) {
		View childView = super.getChildView(groupPosition, childPosition, isLastChild, convertView,
				parent);

		Investment investment = (Investment) getChild(groupPosition, childPosition);
		boolean isCopied = investment.getCopyopType() == CopyOpInvTypeEnum.COPY;

		if (isCopied) {
			View levelLayout = childView.findViewById(R.id.asset_option_level_layout);
			levelLayout.setTag(investment.getId());
			levelLayout.setOnClickListener(this);
		}

		return childView;
	}

	/**
	 * Sets up the call/put arrow for an investment.
	 */
	@Override
	protected void setUpPutCallIndicator(Investment investment, ImageView indicatorImageView) {
		if (currentLevel != LEVEL_UNKNOWN) {
			indicatorImageView.setImageDrawable(DrawableUtils.getDrawable(
					COInvestmentUtil.getInvestmentIcon(investment, currentLevel)));
		} else {
			// If the level is unknown, we display winning icon.
			if (investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL) {
				indicatorImageView.setImageDrawable(DrawableUtils.getDrawable(R.drawable.arrow_up_green));
			} else if (investment.getTypeId() == Investment.INVESTMENT_TYPE_PUT) {
				indicatorImageView.setImageDrawable(DrawableUtils.getDrawable(R.drawable.arrow_down_green));
			}
		}
	}

	/**
	 * Set up the arrow for call/put for settled investments.
	 */
	@Override
	protected void setUpSettledOptPutCallIndicator(Investment investment,
			ImageView indicatorImageView) {
		indicatorImageView.setImageDrawable(DrawableUtils.getDrawable(
				COInvestmentUtil.getSettledInvestmentIcon(investment, currency)));
	}

	@Override
	public void onClick(View v) {
		if (isDialogShown) {
			return;
		}

		Long investmentId = (Long) v.getTag();
		if (investmentId != null) {
			openCopiedOption(investmentId);
		}
	}

	@Override
	public void onDismiss() {
		isDialogShown = false;
	}

	private void openCopiedOption(long investmentId) {
		COApplication.get().getPopUpManager().showCopiedOptionPopUp(investmentId, this);
		isDialogShown = true;
	}
	
	@Override
	public boolean areAllItemsEnabled() {
		
		return true;
	}

}
