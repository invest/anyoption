package com.copyop.android.app.fragment.my_account.banking.deposit;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anyoption.android.app.fragment.my_account.banking.deposit.ExistingCardFragment;

/**
 * @author Anastas Arnaudov
 */
public class COExistingCardFragment extends ExistingCardFragment {

    public static COExistingCardFragment newInstance(Bundle bundle){
        COExistingCardFragment existingCardFragment = new COExistingCardFragment();
        existingCardFragment.setArguments(bundle);
        return existingCardFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater,container,savedInstanceState);
        application.getAppseeManager().hideView(rootView.findViewById(com.anyoption.android.R.id.existing_card_num_textview));
        return rootView;
    }


}
