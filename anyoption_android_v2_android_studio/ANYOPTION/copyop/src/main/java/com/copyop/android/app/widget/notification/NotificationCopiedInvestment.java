package com.copyop.android.app.widget.notification;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Market;
import com.copyop.android.app.R;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NotificationCopiedInvestment extends LinearLayout{

	private String message;
	private Market market;
	
	public NotificationCopiedInvestment(String message, Market market) {
		super(Application.get());
		this.message = message;
		this.market = market;
		init(Application.get(), null, 0);
	}
	public NotificationCopiedInvestment(Context context) {
		super(context);
		init(context, null, 0);
	}
	public NotificationCopiedInvestment(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	public NotificationCopiedInvestment(Context context, AttributeSet attrs,int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		View view = View.inflate(context, R.layout.notification_copied_investment, this);
		TextView messageTextView = (TextView) view.findViewById(R.id.notification_message_text_view);
		messageTextView.setText(message);
		Button tradeButton = (Button) view.findViewById(R.id.notification_buy_button);
		tradeButton.setOnClickListener(new OnClickListener() {
			private boolean isPressed = false;
			@Override
			public void onClick(View v) {
				if(!isPressed){
					isPressed = true;
					goToMarket(market);
				}
			}
		});
	}
	
	private void goToMarket(Market market){
		if(market != null){
			Bundle bundle = new Bundle();
			bundle.putSerializable(Constants.EXTRA_MARKET, market);
			Application.get().postEvent(new NavigationEvent(Screen.ASSET_VIEW_PAGER, NavigationType.DEEP, bundle));						
		}
	}
}
