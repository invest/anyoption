package com.copyop.android.app.fragment.facebook;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.TextView;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.manager.COFacebookManager;
import com.copyop.android.app.util.COBitmapUtils;
import com.copyop.android.app.util.COConstants;
import com.copyop.common.dto.base.Profile;
import com.copyop.json.requests.PagingUserMethodRequest;
import com.copyop.json.results.FacebookFriendsMethodResult;
//import com.facebook.Session;//TODO

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;

public class FacebookConnectedFragment extends NavigationalFragment {
	public static final String TAG = FacebookConnectedFragment.class.getSimpleName();

	private List<Profile> friends;
	private FriendsAdapter listAdapter;
	private ListView listView;
	private boolean resetPaging;
	
	public static FacebookConnectedFragment newInstance(Bundle bundle){
		FacebookConnectedFragment fragment = new FacebookConnectedFragment();
		fragment.setArguments(bundle);
		return fragment;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		friends = new ArrayList<Profile>();
		listAdapter = new FriendsAdapter(friends);
		
		resetPaging = true;
		getFriends();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.facebook_connected, container, false);
		
		View inviteButton = rootView.findViewById(R.id.facebook_connected_invite_button);
		inviteButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				COFacebookManager.inviteFriends(getActivity(), FacebookConnectedFragment.this);
			}
		});
		
		listView = (ListView) rootView.findViewById(R.id.facebook_connected_friends_list_view);
		listView.setAdapter(listAdapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Profile profile = listAdapter.getItem(position);

				Bundle args = new Bundle();
				args.putLong(COConstants.EXTRA_PROFILE_ID, profile.getUserId());
				args.putString(Constants.EXTRA_ACTION_BAR_TITLE, profile.getNickname());
				application.postEvent(new NavigationEvent(COScreen.USER_PROFILE, NavigationType.DEEP, args));
			}
		});
		listView.setOnScrollListener(new OnScrollListener() {
			private int currentLastVisibleItem;
			private int currentVisibleItemCount;
			private int currentScrollState;

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				this.currentScrollState = scrollState;
				isScrollCompleted();    
			}
			
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
				this.currentLastVisibleItem = firstVisibleItem + visibleItemCount;
			    this.currentVisibleItemCount = visibleItemCount;
			}
			
			private void isScrollCompleted() {
			    if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
			        //Detect if there's been a scroll which has completed.
			        //And check if the list is scrolled to the bottom.
			    	if(currentLastVisibleItem == listAdapter.getCount()){
			    		getFriends();
			    	}
			    }
			}
		});
		
		return rootView;
	}

	@Override
	protected Screenable setupScreen() {
		return COScreen.FACEBOOK_CONNECTED;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	  super.onActivityResult(requestCode, resultCode, data);
//	  Session.getActiveSession().onActivityResult(getActivity(), requestCode, resultCode, data);//TODO
	}
	
	private void getFriends() {
		PagingUserMethodRequest request = new PagingUserMethodRequest();
		request.setResetPaging(resetPaging);
		application.getCommunicationManager().requestService(this, "getFacebookFriendsCallback", COConstants.SERVICE_GET_FACEBOOK_FRIENDS, request, FacebookFriendsMethodResult.class);
		
		resetPaging = false;
	}
	
	public void getFacebookFriendsCallback(Object resultObj) {
		FacebookFriendsMethodResult result = (FacebookFriendsMethodResult) resultObj;
		if (null != result && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS && null != result.getFriends()) {
			friends.addAll(result.getFriends());
			listAdapter.setFriends(friends);
			listAdapter.notifyDataSetChanged();
		}
	}
	
	static class FriendsAdapter extends BaseAdapter {
		
		static class UpdateViewHolder {
			public int position;
			
			public TextView positionTextView;
			public ImageView avatarImageView;
			public TextView nameTextView;
			public TextView hitTextView;
			public TextView copiersTextView;
			public TextView watchersTextView;
		}
		
		private List<Profile> friends;
		private Context context;
	    private LayoutInflater inflater;  

		public FriendsAdapter(List<Profile> friends) {
			this.friends = friends;
			this.context = Application.get();
			this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}
		
		@Override
		public int getCount() {
			return friends.size();
		}

		@Override
		public Profile getItem(int position) {
			return friends.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Profile firend = friends.get(position);
			UpdateViewHolder updateViewHolder = null;
			if (null == convertView) {
				updateViewHolder = new UpdateViewHolder();
				convertView = inflater.inflate(R.layout.facebook_connected_list_item, parent, false);
				updateViewHolder.positionTextView = (TextView) convertView.findViewById(R.id.facebook_connected_item_position_text_view);
				updateViewHolder.avatarImageView = (ImageView) convertView.findViewById(R.id.facebook_connected_item_avatar_image_view);
				updateViewHolder.nameTextView = (TextView) convertView.findViewById(R.id.facebook_connected_item_name_text_view);
				updateViewHolder.hitTextView = (TextView) convertView.findViewById(R.id.facebook_connected_item_hit_text_view);
				updateViewHolder.copiersTextView = (TextView) convertView.findViewById(R.id.facebook_connected_item_copiers_text_view);
				updateViewHolder.watchersTextView = (TextView) convertView.findViewById(R.id.facebook_connected_item_watchers_text_view);
				convertView.setTag(updateViewHolder);
			} else {
				updateViewHolder = (UpdateViewHolder) convertView.getTag();
			}
			updateViewHolder.position = position;
			
			try {
				setupListItem(position, firend, updateViewHolder);
			} catch (Exception e) {
				Log.e(TAG, "Can't setup list item.", e);
			}
			
			return convertView;
		}
		
		private void setupListItem(int position, Profile profile, UpdateViewHolder updateViewHolder) {
			updateViewHolder.positionTextView.setText(String.valueOf(position + 1));
			int avatarSize = context.getResources().getDimensionPixelSize(R.dimen.social_network_connected_item_avatar_size);
			COBitmapUtils.loadBitmap(profile.getAvatar(), updateViewHolder.avatarImageView, avatarSize, avatarSize);
			updateViewHolder.nameTextView.setText(profile.getFirstName() + " " + profile.getLastName());
			NumberFormat nf = NumberFormat.getPercentInstance();
			updateViewHolder.hitTextView.setText(nf.format(profile.getHitRate()));
			updateViewHolder.copiersTextView.setText(profile.getCopiers() + "/" + COApplication.get().getMaxCopiersPerAccount());
			updateViewHolder.watchersTextView.setText(String.valueOf(profile.getWatchers()));
		}

		public List<Profile> getFriends() {
			return friends;
		}

		public void setFriends(List<Profile> friends) {
			this.friends = friends;
		}
	}
}