package com.copyop.android.app.manager;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.DepositEvent;
import com.anyoption.android.app.manager.FacebookManager;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.results.MethodResult;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COConstants;
import com.copyop.json.requests.UpdateFacebookFriendsMethodRequest;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.AppInviteDialog;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class COFacebookManager extends FacebookManager {
	
	private static final String TAG = COFacebookManager.class.getSimpleName();

	private static final String FACEBOOK_AVATAR_URL_PATTERN = "https://graph.facebook.com/fbId/picture?type=large";
	
	public COFacebookManager(Application application) {
		super(application);
	}
	
	public void updateFriends(String fbId, JSONArray friends, final Object resultObject, final String resultMethodName) {
		List<String> fIds = new ArrayList<String>();
		for (int i=0; i<friends.length(); i++) {
			try {
				JSONObject friend = friends.getJSONObject(i);
				fIds.add(friend.optString("id"));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		UpdateFacebookFriendsMethodRequest request = new UpdateFacebookFriendsMethodRequest();
		request.setFacebookId(fbId);
		request.setFriends(fIds);
		Application.get().getCommunicationManager().requestService(resultObject, resultMethodName, COConstants.SERVICE_UPDATE_FACEBOOK_FRIENDS, request, MethodResult.class, false, false);
	}
	
	public static void inviteFriends(final Context context, Fragment fragment) {
		Log.d(TAG,"inviteFriends");
		String appLinkUrl = fragment.getString(R.string.fb_app_link);
		if (AppInviteDialog.canShow()) {
			AppInviteContent content = new AppInviteContent.Builder()
					.setApplinkUrl(appLinkUrl)
					.build();
			AppInviteDialog dialog = new AppInviteDialog(fragment.getActivity());
			dialog.registerCallback(FacebookManager.callbackManager, new FacebookCallback<AppInviteDialog.Result>() {
				@Override
				public void onSuccess(AppInviteDialog.Result result) {
					Log.d(TAG, "Invite Dialog onSuccess. Result = " + result);
				}
				@Override
				public void onCancel() {
					Log.d(TAG, "Invite Dialog onCancel");
				}
				@Override
				public void onError(FacebookException error) {
					Log.d(TAG, "Invite Dialog onError. Error = " + error);
				}
			});
			dialog.show(content);
		}
	}

	public static String getAvatarURL(String fbId) {
		return FACEBOOK_AVATAR_URL_PATTERN.replace("fbId", fbId);
	}
	
	public static void publishStory(final Context context, final Fragment fragment, final String message) {
		if (ShareDialog.canShow(ShareLinkContent.class)) {
			ShareLinkContent linkContent = new ShareLinkContent.Builder()
					.setContentTitle(fragment.getString(R.string.showOff))
					.setContentDescription(message)
					.setContentUrl(Uri.parse("https://www.copyop.com"))
			.build();

			ShareDialog.show(fragment.getActivity(),linkContent);
		}
	}

	@Override
	public void onEvent(DepositEvent event) {
		Log.d(TAG, "Sending deposit event.");
		logger.logEvent(EVENT_DEPOSIT);
	}

}