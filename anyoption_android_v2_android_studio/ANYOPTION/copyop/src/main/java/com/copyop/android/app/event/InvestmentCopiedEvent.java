package com.copyop.android.app.event;

/**
 * Event that occurs when someone who is copying the current user makes an Investment.
 * @author Anastas Arnaudov
 *
 */
public class InvestmentCopiedEvent {

}
