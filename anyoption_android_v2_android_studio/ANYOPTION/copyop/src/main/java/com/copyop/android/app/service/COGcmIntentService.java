package com.copyop.android.app.service;

import com.anyoption.android.app.service.GcmIntentService;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class COGcmIntentService extends GcmIntentService{

	private final String TAG = COGcmIntentService.class.getSimpleName();
	
	@Override
	public void onMessage(Context context, Intent intent) {
		Log.d(TAG, "onMessage");
		super.onMessage(context, intent);
	}
	
}
