package com.copyop.android.app.util.adapter;

import java.util.ArrayList;
import java.util.List;

import com.anyoption.android.app.popup.BaseDialogFragment.OnDialogDismissListener;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.adapter.TradingOptionsListAdapter;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.R;
import com.copyop.android.app.popup.ShowOffDialogFragment;
import com.copyop.android.app.util.COTimeUtils;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

public class COTradingOptionsListAdapter extends TradingOptionsListAdapter implements
		OnDialogDismissListener {

	private OnClickListener showOffClickListener;
	private boolean isDialogShown;
	private OnClickListener copiedOptionClickListener;

	public COTradingOptionsListAdapter(List<Investment> investments, boolean areOptionsOpen) {
		super(investments, areOptionsOpen);

		isDialogShown = false;
		if (!areOptionsOpen) {
			setUpShowOffListener();
		}

		setCopiedOptionListener();
	}

	public COTradingOptionsListAdapter(List<Investment> investments) {
		this(investments, true);
	}

	public COTradingOptionsListAdapter() {
		this(new ArrayList<Investment>());
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rootView = super.getView(position, convertView, parent);
		Investment investment = getInvestments().get(position);

		View levelAmountView = rootView.findViewById(R.id.trading_options_item_level_amopunt_view);
		boolean isCopied = investment.getCopyopType() == CopyOpInvTypeEnum.COPY;
		if (isCopied) {
			levelAmountView.setTag(position);
			levelAmountView.setOnClickListener(copiedOptionClickListener);
		} else {
			levelAmountView.setOnClickListener(null);
			levelAmountView.setClickable(false);
		}

		if (!isInvestmentOpen) {
			View showOffIcon = rootView.findViewById(R.id.trading_options_show_off);
			View showOffLayout = rootView.findViewById(R.id.trading_options_current_info_layout);
			boolean isProfitable = rootView.findViewById(R.id.trading_options_profitable_image).getVisibility() == View.VISIBLE;
			if (isProfitable && !COTimeUtils.isOlderThan1Week(investment.getTimeEstClosing().getTime())) {
				showOffIcon.setVisibility(View.VISIBLE);
				showOffLayout.setTag(position);
				showOffLayout.setOnClickListener(showOffClickListener);
			} else {
				showOffIcon.setVisibility(View.INVISIBLE);
				showOffLayout.setOnClickListener(null);
			}
			
		}

		return rootView;
	}

	@Override
	protected void setPutCallIcon(Investment investment, ImageView view, boolean isProfitable) {
		boolean isCopied = investment.getCopyopType() == CopyOpInvTypeEnum.COPY;
		long investmentType = investment.getTypeId();

		if (investmentType == Investment.INVESTMENT_TYPE_CALL && isProfitable) {
			view.setImageResource(isCopied ? R.drawable.trading_options_call_win_copied
					: R.drawable.trading_options_call_win);
		} else if (investmentType == Investment.INVESTMENT_TYPE_CALL && !isProfitable) {
			view.setImageResource(isCopied ? R.drawable.trading_options_call_lose_copied
					: R.drawable.trading_options_call_lose);
		} else if (investmentType == Investment.INVESTMENT_TYPE_PUT && isProfitable) {
			view.setImageResource(isCopied ? R.drawable.trading_options_put_win_copied
					: R.drawable.trading_options_put_win);
		} else {
			view.setImageResource(isCopied ? R.drawable.trading_options_put_lose_copied
					: R.drawable.trading_options_put_lose);
		}
	}

	private void setUpShowOffListener() {
		showOffClickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (isDialogShown) {
					return;
				}

				int position = (Integer) v.getTag();
				openShowOff((Investment) getItem(position));
			}
		};
	}

	private void setCopiedOptionListener() {
		copiedOptionClickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (isDialogShown) {
					return;
				}

				int position = (Integer) v.getTag();
				openCopiedOption((Investment) getItem(position));
			}
		};
	}
	
	private void openShowOff(Investment investment) {
		long returnAmount = AmountUtil.parseAmountToLong(investment.getAmountReturnWF(), currency);
		long profitAmount = returnAmount - investment.getAmount(); 
		ShowOffDialogFragment dialog = ShowOffDialogFragment.newInstance(investment.getId(),
				profitAmount, investment.getMarketId(), investment.getTimeEstClosing().getTime());
		dialog.setOnDismissListener(this);
		application.getFragmentManager().showDialogFragment(dialog);

		isDialogShown = true;
	}

	private void openCopiedOption(Investment investment) {
		COApplication.get().getPopUpManager().showCopiedOptionPopUp(investment.getId(), this);
		isDialogShown = true;
	}
	
	@Override
	public void onDismiss() {
		isDialogShown = false;
	}


}
