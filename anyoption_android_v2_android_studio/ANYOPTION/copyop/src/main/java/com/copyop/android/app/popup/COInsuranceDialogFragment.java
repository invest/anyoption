package com.copyop.android.app.popup;

import com.anyoption.android.app.popup.InsuranceDialogFragment;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.common.beans.base.Investment;
import com.copyop.android.app.R;
import com.copyop.android.app.widget.TimerView;
import com.copyop.android.app.widget.TimerView.OnFinishListener;

import android.os.Bundle;
import android.view.View;

public class COInsuranceDialogFragment extends InsuranceDialogFragment {

	private TimerView timer;

	/**
	 * Create new instance of the dialog. Must contain type argument with key
	 * {@link #ARG_INSURANCE_TYPE_KEY} and value one of {@link Investment#INSURANCE_TAKE_PROFIT} and
	 * {@link Investment#INSURANCE_ROLL_FORWARD}
	 */
	public static COInsuranceDialogFragment newInstance(Bundle bundle) {
		if (!bundle.containsKey(ARG_INSURANCE_TYPE_KEY)) {
			throw new IllegalArgumentException(
					"The bundle MUST contain type key with value Investment.INSURANCE_TAKE_PROFIT or Investment.INSURANCE_ROLL_FORWARD)");
		}

		COInsuranceDialogFragment fragment = new COInsuranceDialogFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	protected void setUpTimer(View contentView) {
		timer = (TimerView) contentView.findViewById(R.id.insurance_timer);
		timer.setFinishListener(new OnFinishListener() {

			@Override
			public void onFinish() {
				dismissAllowingStateLoss();
			}
		});
	}

	@Override
	protected void startTimeLeftTimer() {
		timer.start(insuranceCloseTime - TimeUtils.getCurrentServerTime());
	}

	@Override
	protected void stopTimeLeftTimer() {
		timer.stop();
	}

}
