package com.copyop.android.app.model;

import java.io.Serializable;
import java.util.List;

import com.anyoption.android.app.Application;
import com.copyop.android.app.R;
import com.copyop.common.enums.base.UpdateTypeEnum;

/**
 * Model for the Group Item in the Hot list.
 * @author Anastas Arnaudov
 */
public class HotGroup implements Serializable{

	private static final long serialVersionUID = 1L;

	public enum Type{
		TRADERS,
		TRADES,
		COPIERS,
		ASSET_SPECIALISTS;
	}
	
	private UpdateTypeEnum updateTypeEnum;
	private Type type;
	private String name;
	private int lastHours;
	private int lastInitHours;
	private String lastHoursFormated;
	private List<? extends HotItem> items;
	private List<UpdateTypeEnum> filters;
	
	public Type getType() {
		return type;
	}
	public HotGroup setType(Type type) {
		this.type = type;
		return this;
	}
	public String getName() {
		return name;
	}
	public HotGroup setName(String name) {
		this.name = name;
		return this;
	}
	public int getLastHours() {
		return lastHours;
	}
	public HotGroup setLastHours(int lastHours) {
		this.lastHours = lastHours;
		setupHoursFormatted();
		return this;
	}
	public String getLastHoursFormated() {
		return lastHoursFormated;
	}
	public List<? extends HotItem> getItems() {
		return items;
	}
	public HotGroup setItems(List<? extends HotItem> items) {
		this.items = items;
		return this;
	}
	public UpdateTypeEnum getUpdateType() {
		return updateTypeEnum;
	}
	public HotGroup setUpdateType(UpdateTypeEnum updateTypeEnum) {
		this.updateTypeEnum = updateTypeEnum;
		setupHoursFormatted();
		return this;
	}
	private void setupHoursFormatted(){
		if(updateTypeEnum != null && lastHours > 0){
			if(updateTypeEnum == UpdateTypeEnum.BEST_TRADERS || updateTypeEnum == UpdateTypeEnum.BEST_TRADES || updateTypeEnum == UpdateTypeEnum.BEST_COPIERS || updateTypeEnum == UpdateTypeEnum.BEST_ASSET_SPECIALISTS){
				this.lastHoursFormated = "(" + Application.get().getString(R.string.hotGroupHours, lastHours) + ")";
			}else if(updateTypeEnum == UpdateTypeEnum.BEST_TRADERS_ONE_WEEK || updateTypeEnum == UpdateTypeEnum.BEST_TRADES_ONE_WEEK || updateTypeEnum == UpdateTypeEnum.BEST_COPIERS_ONE_WEEK || updateTypeEnum == UpdateTypeEnum.BEST_ASSET_SPECIALISTS_ONE_WEEK){
				this.lastHoursFormated = "(" + Application.get().getString(R.string.hotGroupWeeks, 1) + ")";
			}else if(updateTypeEnum == UpdateTypeEnum.BEST_TRADERS_ONE_MOTNH || updateTypeEnum == UpdateTypeEnum.BEST_TRADES_ONE_MOTNH || updateTypeEnum == UpdateTypeEnum.BEST_COPIERS_ONE_MOTNH || updateTypeEnum == UpdateTypeEnum.BEST_ASSET_SPECIALISTS_ONE_MOTNH){
				this.lastHoursFormated = "(" + Application.get().getString(R.string.hotGroupMonths, 1) + ")";
			}else if(updateTypeEnum == UpdateTypeEnum.BEST_TRADERS_TRHEE_MOTNH || updateTypeEnum == UpdateTypeEnum.BEST_TRADES_TRHEE_MOTNH || updateTypeEnum == UpdateTypeEnum.BEST_COPIERS_TRHEE_MOTNH || updateTypeEnum == UpdateTypeEnum.BEST_ASSET_SPECIALISTS_TRHEE_MOTNH){
				this.lastHoursFormated = "(" + Application.get().getString(R.string.hotGroupMonths, 3) + ")";
			}
		}
	}
	public List<UpdateTypeEnum> getFilters() {
		return filters;
	}
	public void setFilters(List<UpdateTypeEnum> filters) {
		this.filters = filters;
	}
	public int getLastInitHours() {
		return lastInitHours;
	}
	public void setLastInitHours(int lastInitHours) {
		this.lastInitHours = lastInitHours;
	}
	
}
