package com.copyop.android.app.fragment.trade;

import com.anyoption.android.app.fragment.trade.AssetsListFragment;
import com.anyoption.android.app.util.DrawableUtils;
import com.copyop.android.app.R;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

public class COAssetsListFragment extends AssetsListFragment {

	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static COAssetsListFragment newInstance(Bundle bundle){
		COAssetsListFragment coAssetsListFragment = new COAssetsListFragment();
		coAssetsListFragment.setArguments(bundle);
		return coAssetsListFragment;
	}

	@Override
	protected void setupBubblesTabLayout() {
		//Do nothing
	}

	@Override
	protected void setupTabs() {
		//Do nothing
	}
	
	@Override
	public void onDestroyView() {
		View bgView = footerView.findViewById(R.id.assets_list_footer_bg_layout);
		DrawableUtils.setBackground(bgView, null);
		listView.removeFooterView(footerView);
		super.onDestroyView();
	}
	
	
}
