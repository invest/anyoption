package com.copyop.android.app.widget.inbox;

import java.util.ArrayList;
import java.util.List;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.constants.Constants;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.R;
import com.copyop.android.app.event.InboxNewUpdateEvent;
import com.copyop.android.app.model.Update;
import com.copyop.android.app.util.COConstants;
import com.copyop.android.app.util.ConvertUtils;
import com.copyop.android.app.util.adapter.InboxAdapter;
import com.copyop.common.dto.base.FeedMessage;
import com.copyop.json.requests.PagingUserMethodRequest;
import com.copyop.json.results.FeedMethodResult;

import android.content.Context;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.RelativeLayout;

public class InboxView extends RelativeLayout{

	public final String TAG = InboxView.class.getSimpleName();
	
	public final int MAX_UPDATES_COUNT = 50;
	
	protected List<Update> updatesList;
	protected View rootView;
	protected ListView listView;
	protected InboxAdapter adapter;
	protected View loadingView;
	protected boolean isLoading;
	private boolean isFirstPage;
	
	public InboxView(Context context) {
		super(context);
		init(context, null, 0);
	}
	public InboxView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	public InboxView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		updatesList = new ArrayList<Update>();
		isFirstPage = true;
		refresh();
		
		View rootView = View.inflate(context, R.layout.inbox_view_layout, this);
		loadingView = rootView.findViewById(R.id.loadingView);
		listView = (ListView) rootView.findViewById(R.id.inbox_list_view);
		adapter = new InboxAdapter(updatesList);
		listView.setAdapter(adapter);
		listView.setItemsCanFocus(false);
		listView.setOnScrollListener(new OnScrollListener() {
			
			private int currentLastVisibleItem;
			private int currentVisibleItemCount;
			private int currentScrollState;

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				this.currentScrollState = scrollState;
				isScrollCompleted();    
			}
			
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
				this.currentLastVisibleItem = firstVisibleItem + visibleItemCount;
			    this.currentVisibleItemCount = visibleItemCount;

			}
			
			private void isScrollCompleted() {
			    if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
			        //Detect if there's been a scroll which has completed.
			        //And check if the list is scrolled to the bottom.
			    	if(currentLastVisibleItem == adapter.getCount()){
			    		getInbox();
			    	}
			    }
			}
			
		});
		
		registerForEvents();
		showHideLoading();
	}
	
	protected void showHideLoading(){
		if(loadingView != null){
			if(isLoading){
				loadingView.setVisibility(View.VISIBLE);
			}else{
				loadingView.setVisibility(View.GONE);
			}
		}
	}
	
	protected void registerForEvents(){
		Application.get().registerForEvents(this, InboxNewUpdateEvent.class);
	}
	
	protected void unregisterForEvents(){
		Application.get().unregisterForAllEvents(this);
	}
	
	protected void addUpdate(Update update){
		if(updatesList.size() >= MAX_UPDATES_COUNT){
			updatesList.remove(updatesList.size() - 1);
		}
		updatesList.add(0, update);
		adapter.notifyDataSetChanged();
	}
	
	private void setupEmptyList(){
		isLoading = false;
		showHideLoading();
		//TODO
	}
	
	private void refresh(){
		unregisterForEvents();
		clean();
		getInbox();
	}
	
	private void clean(){
		this.updatesList.clear();
	}
	
	//#####################################################################
	//#########					REQUEST - RESPONSE				###########
	//#####################################################################	
	
	protected void getInbox(){
		isLoading = true;
		showHideLoading();
		PagingUserMethodRequest request = new PagingUserMethodRequest();
		request.setResetPaging(isFirstPage);
		if(!Application.get().getCommunicationManager().requestService(this, "gotInbox", COConstants.SERVICE_GET_COPYOP_INBOX, request, FeedMethodResult.class, false, false)){
			isLoading = false;
			showHideLoading();
			setupEmptyList();
		}
	}

	@SuppressWarnings("unchecked")
	public void gotInbox(Object result){
		if(result != null && result instanceof FeedMethodResult  && ((FeedMethodResult) result).getErrorCode() == Constants.ERROR_CODE_SUCCESS){
			FeedMethodResult feedMethodResult = (FeedMethodResult) result;			
			
			new AsyncTask<List<FeedMessage>, Void, List<Update>>() {
				
				@Override
				protected List<Update> doInBackground(List<FeedMessage>... params) {
					List<FeedMessage> inboxList = params[0];
					List<Update> result = new ArrayList<Update>();
					if (inboxList != null) {
						int unreadCounter = 0;
						for (FeedMessage message : inboxList) {
							//TODO TESTING
//							Update update = ConvertUtils.createRandomInbox();
							///
							Update update = ConvertUtils.convertFeedMessageToUpdate(message);
							result.add(update);
							
							if(unreadCounter < COApplication.get().getInboxManager().getUnreadMessages()){
								update.setRead(false);
							}else{
								update.setRead(true);
							}
							unreadCounter++;
						}
					}else{
						cancel(true);
					}
					return result;
				}
				
				@Override
				protected void onCancelled() {
					isFirstPage = false;
					isLoading = false;
					showHideLoading();
				}
				
				@Override
				protected void onPostExecute(List<Update> result) {
					updatesList.addAll(result);
					if (adapter != null) {
						adapter.notifyDataSetChanged();
					}
					isFirstPage = false;
					isLoading = false;
					showHideLoading();
					
					COApplication.get().getInboxManager().clearUnreadMessages();
				}

			}.execute(feedMethodResult.getFeed());
		}else{
			Log.e(TAG, COConstants.SERVICE_GET_COPYOP_INBOX + " callback -> Could NOT parse result.");
			setupEmptyList();
		}
	}
	
	//#####################################################################
	
	
	//#####################################################################
	//#########					EVENTs CALLBACKs				###########
	//#####################################################################
	
	public void onEventMainThread(InboxNewUpdateEvent event){
		Update update = event.getUpdate();
		this.updatesList.add(0, update);
		this.adapter.notifyDataSetChanged();
	}
	
	//#####################################################################
	
}
