package com.copyop.android.app.event;

import com.copyop.android.app.model.Update;

/**
 * Event that is triggered when there is a new INBOX Update received.
 * @author Anastas Arnaudov
 *
 */
public class InboxNewUpdateEvent {

	private Update update;
	
	public InboxNewUpdateEvent(Update update){
		this.update = update;
	}

	public Update getUpdate() {
		return update;
	}
	
}
