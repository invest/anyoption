package com.copyop.android.app.model;

import java.io.Serializable;

public class TopLogTrader implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private long userId;
	private int investmentsCount;
	private double profit;
	private String avatar;
	private String nickname;
	private boolean isAfterLastLogOff;
	private boolean isShowIncreaseAmount;
	
	public TopLogTrader(com.copyop.json.results.LastLoginSummaryMethodResult.TopLogTrader topLogTrader){
		userId = topLogTrader.getUserId();
		investmentsCount = topLogTrader.getInvestmentsCount();
		profit = topLogTrader.getProfit();
		avatar = topLogTrader.getAvatar();
		nickname = topLogTrader.getNickname();
		isAfterLastLogOff = topLogTrader.isAfterLastLogOff();
		isShowIncreaseAmount = topLogTrader.isShowIncreaseAmount();
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public int getInvestmentsCount() {
		return investmentsCount;
	}

	public void setInvestmentsCount(int investmentsCount) {
		this.investmentsCount = investmentsCount;
	}

	public double getProfit() {
		return profit;
	}

	public void setProfit(double profit) {
		this.profit = profit;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public boolean isAfterLastLogOff() {
		return isAfterLastLogOff;
	}

	public void setAfterLastLogOff(boolean isAfterLastLogOff) {
		this.isAfterLastLogOff = isAfterLastLogOff;
	}

	public boolean isShowIncreaseAmount() {
		return isShowIncreaseAmount;
	}

	public void setShowIncreaseAmount(boolean isShowIncreaseAmount) {
		this.isShowIncreaseAmount = isShowIncreaseAmount;
	}

	
}
