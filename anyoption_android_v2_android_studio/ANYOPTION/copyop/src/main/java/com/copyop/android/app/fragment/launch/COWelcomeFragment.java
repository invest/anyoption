package com.copyop.android.app.fragment.launch;

import java.util.ArrayList;
import java.util.List;

import com.anyoption.android.app.Application.LayoutDirection;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.fragment.launch.SlidePageFragment;
import com.anyoption.android.app.fragment.launch.WelcomeFragment;
import com.anyoption.android.app.util.DrawableUtils;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

public class COWelcomeFragment extends WelcomeFragment {
	public static final String TAG = COWelcomeFragment.class.getSimpleName(); 
	
	/**
	 * Use this method to create new instance of the fragment.
	 * @return
	 */
	public static COWelcomeFragment newInstance(){
		return newInstance(new Bundle());
	}
	
	/**
	 * Use this method to create new instance of the fragment.
	 * @return
	 */
	public static COWelcomeFragment newInstance(Bundle bundle){
		Log.d(TAG, "Created new COWelcomeFragment.");
		NUMBER_OF_PAGES = 4;
		COWelcomeFragment welcomeFragment = new COWelcomeFragment();
		welcomeFragment.setArguments(bundle);	
		return welcomeFragment;
	}

	@Override
	protected Screenable getLoginScreen() {
		return COScreen.LSS_LOGIN_SIGNUP;
	}
	
	@Override
	protected Screenable getRegisterScreen() {
		return COScreen.LSS_SIGNUP;
	}
	
	@Override
	protected void setupTryWithoutAccount() {
		// Do nothing - no such button in copyop
	}

	/**
	 * Returns the pages for the Slide Pager.
	 * @return
	 */
	@Override
	protected List<Fragment> getFragments() {
		List<Fragment> fragmentList = new ArrayList<Fragment>();

		if(application.getLayoutDirection() == LayoutDirection.LEFT_TO_RIGHT) {			
			fragmentList.add(SlidePageFragment.newInstance(R.layout.welcome_screen_page_1, R.drawable.welcome_1));
			fragmentList.add(SlidePageFragment.newInstance(R.layout.welcome_screen_page_2, R.drawable.welcome_2));
			fragmentList.add(SlidePageFragment.newInstance(R.layout.welcome_screen_page_3, R.drawable.welcome_3));
			fragmentList.add(SlidePageFragment.newInstance(R.layout.welcome_screen_page_4, R.drawable.welcome_4));
		}
		
		return fragmentList;
	}
	
	@Override
	protected Drawable getCurrentDot() {
		return DrawableUtils.getDrawable(R.drawable.co_dot_filled_blue);
	}
	
	@Override
	protected Drawable getOtherDot() {
		return DrawableUtils.getDrawable(R.drawable.co_dot_empty_blue);
	}
	
	@Override
	protected void setupLanguageButton() {
		// Do nothing
	}

	@Override
	protected Screenable getNextLoggedInScreen() {
		return Screen.SETTINGS;
	}

}