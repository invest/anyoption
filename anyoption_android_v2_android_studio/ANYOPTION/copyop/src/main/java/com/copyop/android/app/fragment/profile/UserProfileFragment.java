package com.copyop.android.app.fragment.profile;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.event.UpdateTitleEvent;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.lightstreamer.LightstreamerListener;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.popup.BaseDialogFragment.OnDialogDismissListener;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.BitmapUtils;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.InvestmentUtil;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.fragment.walkthrough.ProfileWalkthroughDialog;
import com.copyop.android.app.manager.COPopUpManager;
import com.copyop.android.app.popup.CopyDialogFragment;
import com.copyop.android.app.popup.CopyDialogFragment.OnCopyStateChangedListener;
import com.copyop.android.app.popup.FollowDialogFragment;
import com.copyop.android.app.popup.HitDialogFragment;
import com.copyop.android.app.popup.WatchDialogFragment;
import com.copyop.android.app.popup.WatchDialogFragment.OnWatchStateChangedListener;
import com.copyop.android.app.util.COAmountUtil;
import com.copyop.android.app.util.COBitmapUtils;
import com.copyop.android.app.util.COConstants;
import com.copyop.android.app.widget.CopyButton;
import com.copyop.android.app.widget.CopyButton.CopyButtonState;
import com.copyop.android.app.widget.FollowButton;
import com.copyop.android.app.widget.FollowButton.FollowButtonState;
import com.copyop.android.app.widget.HitButton;
import com.copyop.android.app.widget.WatchButton;
import com.copyop.android.app.widget.WatchButton.WatchButtonState;
import com.copyop.common.dto.base.Profile;
import com.copyop.json.requests.ProfileMethodRequest;
import com.copyop.json.results.ProfileMethodResult;
import com.copyop.json.results.ProfileMethodResult.TopMarket;
import com.copyop.json.results.ProfileMethodResult.TopTrader;
import com.lightstreamer.ls_client.UpdateInfo;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class UserProfileFragment extends NavigationalFragment implements OnClickListener, LightstreamerListener,
		OnDialogDismissListener, OnCopyStateChangedListener, OnWatchStateChangedListener, Callback {

	@SuppressWarnings("hiding")
	private static final String TAG = UserProfileFragment.class.getSimpleName();

	private static final String SUMMARY_FRAGMENT_TAG = "summary_fragment_tag";
	private static final String FACEBOOK_FRAGMENT_TAG = "facebook_fragment_tag";

	private static final String TIME_FORMAT_PATTERN = "HH:mm";
	private static final String SETTLED_TIME_FORMAT_PATTERN = "dd/MM/yyyy\nHH:mm";
	private static final long TIMER_UPDATE_INVERVAL = 1000;

	private static final int MESSAGE_START_TIMER = 1;

	private boolean isMyProfile;
	private long profileUserId;
	private ProfileMethodResult profileInfo;
	private Currency currency;
	private SkinGroup skinGroup;
	private boolean isTablet;

	private com.anyoption.android.app.manager.FragmentManager fragmentManager;
	private UserSummaryPagerFragment summaryFragment;
	private UserProfileFacebookFragment facebookFragment;
	private COPopUpManager popUpManager;

	private View rootView;
	private LayoutInflater layoutInflater;
	private ScrollView pageScroll;
	private HorizontalScrollView copiersScroll;
	private View lastTradesLayout;
	private LinearLayout lastTradesItemsLayout;

	private HitButton hitButton;
	private View copiersButton;
	private View watchersButton;

	private CopyButton copyButton;
	private WatchButton watchButton;

	private View profitButton;
	private ImageView profitOpenIcon;
	private LinearLayout profitContent;

	private View copyingButton;
	private View watchingButton;

	private List<InvestmentFieldsWrapper> investmentFieldsWrappers;
	private SimpleDateFormat timeFormat;
	private SimpleDateFormat settledTimeFormat;

	private OnClickListener itemClickListener;

	/**
	 * Used for string concatenations.
	 */
	private StringBuilder tempSB;
	private boolean isDialogShown;
	private boolean isScreenVisible;

	private CountDownTimer timer;
	private long timerEndTime;

	private Handler handler;

	/**
	 * Creates new instance of this fragment.
	 */
	public static UserProfileFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Creating UserProfileFragment fragment");
		UserProfileFragment fragment = new UserProfileFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		fragmentManager = application.getFragmentManager();
		popUpManager = (COPopUpManager) application.getPopUpManager();
		currency = application.getCurrency();
		Skin skin = application.getSkins().get(application.getSkinId());
		skinGroup = skin.getSkinGroup();
		timeFormat = new SimpleDateFormat(TIME_FORMAT_PATTERN);
		settledTimeFormat = new SimpleDateFormat(SETTLED_TIME_FORMAT_PATTERN);
		isTablet = getResources().getBoolean(R.bool.isTablet);

		handler = new Handler(Looper.getMainLooper(), this);

		setUpProfileArgs();
		profileInfo = null;
		investmentFieldsWrappers = new ArrayList<InvestmentFieldsWrapper>();

		tempSB = new StringBuilder();
		isDialogShown = false;
		timerEndTime = 0l;

		isScreenVisible = true;

		requestProfile();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.user_profile_fragment, container, false);
		layoutInflater = inflater;

		addNestedFragments();
		pageScroll = (ScrollView) rootView.findViewById(R.id.user_profile_scroll_view);
		lastTradesLayout = rootView.findViewById(R.id.user_profile_last_trades_layout);
		lastTradesItemsLayout = (LinearLayout) rootView.findViewById(R.id.user_profile_last_trades_items_layout);

		hitButton = (HitButton) rootView.findViewById(R.id.user_profile_hit_button);
		copiersButton = rootView.findViewById(R.id.user_profile_copiers_button);
		watchersButton = rootView.findViewById(R.id.user_profile_watchers_button);

		copyButton = (CopyButton) rootView.findViewById(R.id.user_profile_copy_button);
		watchButton = (WatchButton) rootView.findViewById(R.id.user_profile_watch_button);

		profitButton = rootView.findViewById(R.id.user_profile_profit_button);
		copyingButton = rootView.findViewById(R.id.user_profile_coping_button);
		watchingButton = rootView.findViewById(R.id.user_profile_watching_button);

		itemClickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				Object item = v.getTag();

				if (item instanceof TopTrader) {
					// Clicked top trader
					TopTrader clickedTrader = (TopTrader) item;
					openProfile(clickedTrader.getUserId(), clickedTrader.getNickname());
				} else if (item instanceof Market) {
					// Clicked market from profit items or last trades
					Market clickedMarket = (Market) item;
					openMarket(clickedMarket);
				} else if (item instanceof Profile) {
					// Clicked profile from copiers list
					Profile clickedProfile = (Profile) item;
					openProfile(clickedProfile.getUserId(), clickedProfile.getNickname());
				} else if (item instanceof Investment) {
					// Clicked follow from last trades list
					Investment investment = (Investment) item;
					openFollow(investment);
				}
			}
		};

		setUpProfileInfo();
		setUpWalkthrough();

		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();

		application.registerForEvents(this, NavigationEvent.class);
		subscribeTradesLS();
		startTimer();
	}

	@Override
	public void onPause() {
		unsubscribeTradesLS();
		stopTimer();
		application.unregisterForAllEvents(this);

		super.onPause();
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		if (hidden) {
			unsubscribeTradesLS();
			stopTimer();
		} else {
			subscribeTradesLS();
			startTimer();

			COApplication.get().getAppseeManager().onProfile(isMyProfile);
		}

		super.onHiddenChanged(hidden);
	}

	private void requestProfile() {
		String requestMethod;
		UserMethodRequest request;
		if (isMyProfile) {
			requestMethod = COConstants.SERVICE_MY_PROFILE;
			request = new UserMethodRequest();
		} else {
			requestMethod = COConstants.SERVICE_GET_PROFILE;
			ProfileMethodRequest profileRequest = new ProfileMethodRequest();
			profileRequest.setRequestedUserId(profileUserId);
			request = profileRequest;
		}

		application.getCommunicationManager().requestService(this, "profileRequestCallback", requestMethod, request,
				ProfileMethodResult.class);
	}

	public void profileRequestCallback(Object resultObj) {
		ProfileMethodResult result = (ProfileMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			profileInfo = result;
			setUpProfileInfo();
		} else {
			if (result != null) {
				Log.e(TAG, "profileRequestCallback error: " + result.getErrorCode());
				if (result.getErrorCode() == COConstants.ERROR_CODE_USER_REMOVED) {
					Log.e(TAG, "This user is removed!");
					application.getFragmentManager().onBack();
				}
			} else {
				Log.e(TAG, "profileRequestCallback error: result is null");
			}
		}
	}

	/**
	 * Get info from the arguments.
	 */
	private void setUpProfileArgs() {
		Bundle arguments = getArguments();

		long userId = application.getUser().getId();
		if (arguments != null && arguments.containsKey(COConstants.EXTRA_PROFILE_ID)) {
			profileUserId = arguments.getLong(COConstants.EXTRA_PROFILE_ID);
			isMyProfile = profileUserId == userId;
		} else {
			isMyProfile = true;
			profileUserId = userId;
		}
	}

	/**
	 * Sets up all profile data.
	 */
	private void setUpProfileInfo() {
		if (!isScreenVisible || isRemoving() || !isLayoutCreated() || profileInfo == null) {
			return;
		}

		setUpTitle();
		summaryFragment.setProfileInfo(profileInfo, isMyProfile);
		setUpCopyWatchButton();
		setUpSmallWhiteButtons();
		setUpProfitButton();
		setUpProfitItems();
		setUpDataItems();
		setUpLastTrades();
		setUpCopiers();
		setUpCopyingWatchingButtons();
		facebookFragment.setProfileInfo(profileUserId, profileInfo);
	}

	/**
	 * Called when a {@link NavigationEvent} occurs.
	 * 
	 * @param event
	 */
	public void onEventMainThread(NavigationEvent event) {
		isScreenVisible = event.getToScreen() == COScreen.USER_PROFILE;

		if (event.getToScreen() != COScreen.USER_PROFILE
				|| event.getBundle().getLong(COConstants.EXTRA_PROFILE_ID) == profileUserId) {
			return;
		}

		unsubscribeTradesLS();
		stopTimer();

		if (pageScroll != null) {
			pageScroll.fullScroll(ScrollView.FOCUS_UP);
		}

		if (copiersScroll != null) {
			copiersScroll.fullScroll(HorizontalScrollView.FOCUS_LEFT);
		}

		setUpProfileArgs();
		requestProfile();

		setUpWalkthrough();
	}

	/**
	 * Checks if the layout is created in onCreateView.
	 * 
	 * @return
	 */
	private boolean isLayoutCreated() {
		return rootView != null;
	}

	private void setUpTitle() {
		if (isMyProfile) {
			application.postEvent(new UpdateTitleEvent(getString(R.string.userProfileScreenTitleMe)));
		} else if (profileInfo != null) {
			application.postEvent(new UpdateTitleEvent(profileInfo.getNickname()));
		}
	}

	@SuppressWarnings("unchecked")
	private void addNestedFragments() {
		FragmentManager childFragmentManager = getChildFragmentManager();
		FragmentTransaction fragmentTransaction = childFragmentManager.beginTransaction();

		if (childFragmentManager.findFragmentByTag(SUMMARY_FRAGMENT_TAG) == null) {
			// UserSummaryPagerFragment creation
			Bundle args = new Bundle();
			summaryFragment = (UserSummaryPagerFragment) BaseFragment
					.newInstance(application.getSubClassForClass(UserSummaryPagerFragment.class), args);
			fragmentTransaction.add(R.id.user_profile_summary_fr, summaryFragment, SUMMARY_FRAGMENT_TAG);
		} else {
			summaryFragment = (UserSummaryPagerFragment) childFragmentManager.findFragmentByTag(SUMMARY_FRAGMENT_TAG);
		}

		if (childFragmentManager.findFragmentByTag(FACEBOOK_FRAGMENT_TAG) == null) {
			// UserProfileFacebookFragment creation
			Bundle args = new Bundle();
			facebookFragment = (UserProfileFacebookFragment) BaseFragment
					.newInstance(application.getSubClassForClass(UserProfileFacebookFragment.class), args);
			fragmentTransaction.add(R.id.user_profile_facebook_fr, facebookFragment, FACEBOOK_FRAGMENT_TAG);
		} else {
			facebookFragment = (UserProfileFacebookFragment) childFragmentManager
					.findFragmentByTag(FACEBOOK_FRAGMENT_TAG);
		}

		fragmentTransaction.commit();
	}

	private void setUpCopyWatchButton() {
		View copyWatchLayout = rootView.findViewById(R.id.user_profile_copy_watch_buttons_layout);

		if (isMyProfile) {
			copyWatchLayout.setVisibility(View.GONE);
			return;
		}

		copyButton.setOnClickListener(this);
		watchButton.setOnClickListener(this);

		if (profileInfo.isFrozen() && !profileInfo.isCopying()) {
			copyButton.setState(CopyButtonState.STATE_FROZEN);
		} else {
			if (profileInfo.isCopying()) {
				copyButton.setState(CopyButtonState.STATE_COPYING);
			} else {
				copyButton.setState(CopyButtonState.STATE_NORMAL);
			}
		}

		if (profileInfo.isCopying() || profileInfo.isWatching()) {
			watchButton.setState(WatchButtonState.STATE_WATCHING);
		} else {
			watchButton.setState(WatchButtonState.STATE_NORMAL);
		}

		copyWatchLayout.setVisibility(View.VISIBLE);
	}

	/**
	 * Set up hit, copiers and watchers buttons.
	 */
	private void setUpSmallWhiteButtons() {
		hitButton.setOnClickListener(this);

		if (profileInfo.isShowHitRate()) {
			hitButton.setHit(String.valueOf(profileInfo.getHitRate()));
		} else {
			hitButton.setHit("- -");
		}

		if (profileInfo.isOpenHitRate()) {
			hitButton.setEnabled(true);
		} else {
			hitButton.setEnabled(false);
		}

		copiersButton.setOnClickListener(this);
		TextView copiersText = (TextView) rootView.findViewById(R.id.user_profile_copiers);
		tempSB.setLength(0);
		tempSB.append(profileInfo.getCopiersCount()).append("/").append(COApplication.get().getMaxCopiersPerAccount());
		copiersText.setText(tempSB.toString());

		if (profileInfo.getCopiersCount() > 0) {
			copiersButton.setEnabled(true);
		} else {
			copiersButton.setEnabled(false);
		}

		watchersButton.setOnClickListener(this);
		TextView watchersText = (TextView) rootView.findViewById(R.id.user_profile_watchers);
		watchersText.setText(String.valueOf(profileInfo.getWatchersCount()));
		if (profileInfo.getWatchersCount() > 0) {
			watchersButton.setEnabled(true);
		} else {
			watchersButton.setEnabled(false);
		}
	}

	private void setUpProfitButton() {
		profitButton.setOnClickListener(this);
		profitOpenIcon = (ImageView) rootView.findViewById(R.id.user_profile_profit_open_icon);

		if ((profileInfo.getTopTraders() != null && !profileInfo.getTopTraders().isEmpty())
				|| (profileInfo.getTopMarkets() != null && !profileInfo.getTopMarkets().isEmpty())) {
			// There is content
			profitButton.setClickable(true);
			profitOpenIcon.setVisibility(View.VISIBLE);
		} else {
			// The content is empty
			profitButton.setClickable(false);
			profitOpenIcon.setVisibility(View.GONE);
		}

		TextView profitButtonText = (TextView) rootView.findViewById(R.id.user_profile_profit_button_text);
		TextView profitButtonAmountSuccessText = (TextView) rootView
				.findViewById(R.id.user_profile_profit_button_success);
		View successLabel = rootView.findViewById(R.id.user_profile_profit_button_success_label);
		ImageView profitStatusIcon = (ImageView) rootView.findViewById(R.id.user_profile_profit_status_icon);

		if (isMyProfile) {
			if (profileInfo.getTopTraders() != null && !profileInfo.getTopTraders().isEmpty()) {
				profitButtonText.setText(getString(R.string.userProfileMyCopiedOptions));
				profitButtonAmountSuccessText.setText(profileInfo.getCopiedInvestmentsTotalSuccess() + "%");
				profitButtonAmountSuccessText.setVisibility(View.VISIBLE);
				successLabel.setVisibility(View.VISIBLE);
			} else {
				profitButtonText.setText(getString(R.string.userProfileYouHaveNotCopied));
				profitButtonAmountSuccessText.setVisibility(View.GONE);
				successLabel.setVisibility(View.GONE);
			}

			if (profileInfo.getCopiedInvestmentsTotalSuccess() > 0) {
				profitStatusIcon.setImageResource(R.drawable.profit_acordion_win);
			} else if (profileInfo.getCopiedInvestmentsTotalSuccess() < 0) {
				profitStatusIcon.setImageResource(R.drawable.profit_acordion_lose);
			} else {
				profitStatusIcon.setImageResource(R.drawable.profit_acordion);
			}
		} else {
			String buttonText = null;
			if (profileInfo.getTopMarkets().size() == 0) {
				buttonText = getString(R.string.userProfileYouDidntCopyFrom, profileInfo.getNickname());
				profitButtonAmountSuccessText.setVisibility(View.GONE);
			} else {
				if (profileInfo.getMarketsTotalResult() >= 0) {
					buttonText = getString(R.string.userProfileProfitFrom, profileInfo.getNickname());
				} else {
					buttonText = getString(R.string.userProfileLossFrom, profileInfo.getNickname());
				}

				profitButtonAmountSuccessText.setText(
						COAmountUtil.getFormattedProfitLossAmount(profileInfo.getMarketsTotalResult(), currency));
				profitButtonAmountSuccessText.setVisibility(View.VISIBLE);
			}

			profitButtonText.setText(buttonText);

			successLabel.setVisibility(View.GONE);

			if (profileInfo.getMarketsTotalResult() > 0) {
				profitStatusIcon.setImageResource(R.drawable.profit_acordion_win);
			} else if (profileInfo.getMarketsTotalResult() < 0) {
				profitStatusIcon.setImageResource(R.drawable.profit_acordion_lose);
			} else {
				profitStatusIcon.setImageResource(R.drawable.profit_acordion);
			}
		}
	}

	private void setUpProfitItems() {
		profitContent = (LinearLayout) rootView.findViewById(R.id.user_profile_profit_content);
		if (profitContent.getVisibility() == View.VISIBLE) {
			toggleProfit();
		}

		int itemsCount = 0;
		List<TopTrader> topTraders = null;
		List<TopMarket> topMarkets = null;
		if (isMyProfile) {
			itemsCount = profileInfo.getTopTraders().size();
			topTraders = profileInfo.getTopTraders();
		} else {
			itemsCount = profileInfo.getTopMarkets().size();
			topMarkets = profileInfo.getTopMarkets();
		}

		// Clear the items for other profiles
		profitContent.removeAllViews();

		for (int i = 0; i < itemsCount; i++) {
			View itemView = layoutInflater.inflate(R.layout.user_profile_profit_content_item, profitContent, false);
			ImageView icon = (ImageView) itemView.findViewById(R.id.user_profile_profit_item_icon);
			TextView name = (TextView) itemView.findViewById(R.id.user_profile_profit_item_name);
			TextView copied = (TextView) itemView.findViewById(R.id.user_profile_profit_item_copied);
			TextView amount = (TextView) itemView.findViewById(R.id.user_profile_profit_item_amount);
			ImageView gainLostIcon = (ImageView) itemView.findViewById(R.id.user_profile_profit_item_gain_lost_icon);
			TextView gainLost = (TextView) itemView.findViewById(R.id.user_profile_profit_item_gain_lost);

			if (isMyProfile) {
				TopTrader trader = topTraders.get(i);
				if (trader.getAvatar() != null) {
					COBitmapUtils.loadBitmap(trader.getAvatar(), icon);
				}
				if (trader.getNickname() != null) {
					name.setText(trader.getNickname());
				}

				tempSB.setLength(0);
				tempSB.append(trader.getInvestmentsCount()).append(" ").append(getString(R.string.copiedInvestments));
				copied.setText(tempSB.toString());

				amount.setText(COAmountUtil.getFormattedProfitLossAmount(trader.getProfit(), currency));
				if (trader.getProfitDisplay() > 0) {
					gainLostIcon.setImageResource(R.drawable.gain_icon);
					gainLost.setText(R.string.userProfileProfit);
				} else {
					gainLostIcon.setImageResource(R.drawable.lost_icon);
					gainLost.setText(R.string.userProfileLoss);
				}

				itemView.setTag(trader);
			} else {
				TopMarket topMarket = topMarkets.get(i);
				Market market = application.getMarketForID(topMarket.getMarketId());
				int resID = DrawableUtils.getMarketIconResID(market);
				BitmapUtils.loadBitmap(resID, icon);
				if (market != null) {
					name.setText(market.getDisplayName());
				}

				tempSB.setLength(0);
				tempSB.append(topMarket.getInvestmentsCount()).append(" ")
						.append(getString(R.string.copiedInvestments));
				copied.setText(tempSB.toString());

				amount.setText(COAmountUtil.getFormattedProfitLossAmount(topMarket.getResult(), currency));
				if (topMarket.getResultDisplay() > 0) {
					gainLostIcon.setImageResource(R.drawable.gain_icon);
					gainLost.setText(R.string.userProfileProfit);
				} else {
					gainLostIcon.setImageResource(R.drawable.lost_icon);
					gainLost.setText(R.string.userProfileLoss);
				}

				itemView.setTag(market);
			}

			if (i == itemsCount - 1) {
				View separator = itemView.findViewById(R.id.user_profile_profit_item_border);
				separator.setVisibility(View.GONE);
			}

			itemView.setOnClickListener(itemClickListener);
			profitContent.addView(itemView);
		}
	}

	private void setUpDataItems() {
		View bestAssetLayout = rootView.findViewById(R.id.user_profile_data_best_asset);
		if (profileInfo.getBestAssetMarketId() > 0) {
			bestAssetLayout.setVisibility(View.VISIBLE);

			Market bestAsset = application.getMarketForID(profileInfo.getBestAssetMarketId());
			if (bestAsset != null) {
				TextView assetName = (TextView) rootView.findViewById(R.id.user_profile_data_best_asset_asset_name);
				assetName.setText(bestAsset.getDisplayName());
			} else {
				// Could not find the asset
				bestAssetLayout.setVisibility(View.GONE);
			}
			TextView assetProfit = (TextView) rootView.findViewById(R.id.user_profile_data_best_asset_profit);
			tempSB.setLength(0);
			tempSB.append(COAmountUtil.getFormattedProfitLossAmount(profileInfo.getBestAssetMarketResult(), currency));
			tempSB.append(" ").append(getString(R.string.profit));
			assetProfit.setText(tempSB.toString());
		} else {
			bestAssetLayout.setVisibility(View.GONE);
		}

		View bestStreakLayout = rootView.findViewById(R.id.user_profile_data_best_streak);
		if (profileInfo.getStreakCount() >= 0) {
			bestStreakLayout.setVisibility(View.VISIBLE);

			TextView streakCount = (TextView) rootView.findViewById(R.id.user_profile_data_best_streak_count);
			streakCount.setText(String.valueOf(profileInfo.getStreakCount()));
			TextView streakProfit = (TextView) rootView.findViewById(R.id.user_profile_data_best_streak_profit);
			tempSB.setLength(0);
			tempSB.append(COAmountUtil.getFormattedProfitLossAmount(profileInfo.getStreakResult(), currency));
			tempSB.append(" ").append(getString(R.string.profit));
			streakProfit.setText(tempSB.toString());

		} else {
			bestStreakLayout.setVisibility(View.GONE);
		}

		TextView tradesCount = (TextView) rootView.findViewById(R.id.user_profile_data_trades_count);
		double averageTrades = profileInfo.getAvgTrades();
		// Round the average trades to 0.1 if they are something like :
		// 0.0000001 for example:
		if (averageTrades > 0 && averageTrades <= 0.1) {
			averageTrades = 0.1;
		}

		tradesCount.setText(String.format(Locale.US, "%.1f", averageTrades));
	}

	private void setUpLastTrades() {
		if (isMyProfile || profileInfo.getLastInvestments() == null || profileInfo.getLastInvestments().isEmpty()) {
			investmentFieldsWrappers.clear();
			lastTradesLayout.setVisibility(View.GONE);
			lastTradesItemsLayout.removeAllViews();
			return;
		}

		List<? extends Investment> investments = profileInfo.getLastInvestments();
		sortInvestments();
		for (int i = 0; i < investments.size(); i++) {
			InvestmentUtil.fixDate(investments.get(i));
		}

		investmentFieldsWrappers.clear();
		int count = investments.size();

		lastTradesItemsLayout.removeAllViews();

		for (int i = 0; i < count; i++) {
			View itemView = layoutInflater.inflate(R.layout.user_profile_last_trades_item, lastTradesItemsLayout,
					false);
			Investment investment = investments.get(i);

			Market market = application.getMarketForID(investment.getMarketId());
			ImageView assetIcon = (ImageView) itemView.findViewById(R.id.user_profile_last_trades_asset_icon);
			int resID = DrawableUtils.getMarketIconResID(market);
			BitmapUtils.loadBitmap(resID, assetIcon);

			TextView assetName = (TextView) itemView.findViewById(R.id.user_profile_last_trades_asset_name);
			assetName.setText(market.getDisplayName());

			TextView purchasedLevel = (TextView) itemView.findViewById(R.id.user_profile_last_trades_purchased_lavel);
			purchasedLevel.setText(investment.getLevel());
			TextView purchesedAmount = (TextView) itemView.findViewById(R.id.user_profile_last_trades_purchased_amount);
			purchesedAmount.setText(investment.getAmountTxt());

			TextView currentLevel = (TextView) itemView.findViewById(R.id.user_profile_last_trades_current_lavel);

			TextView currentReturn = (TextView) itemView.findViewById(R.id.user_profile_last_trades_current_return);
			currentReturn.setText(investment.getAmountReturnWF());
			View openOptionView = itemView.findViewById(R.id.user_profile_last_trades_open_option);

			TextView time = (TextView) itemView.findViewById(R.id.user_profile_last_trades_time);
			ImageView putCallIcon = (ImageView) itemView.findViewById(R.id.user_profile_last_trades_arrow);
			View ribbonView = itemView.findViewById(R.id.user_profile_last_trades_ribbon);

			FollowButton followButton = (FollowButton) itemView.findViewById(R.id.user_profile_last_trades_follow);

			// Update the values which depend on isSettled
			if (investment.getTimeSettled() == null) {
				// Open option
				InvestmentFieldsWrapper wrapper = new InvestmentFieldsWrapper(investment, time, currentLevel,
						currentReturn, putCallIcon, ribbonView, followButton, openOptionView);
				investmentFieldsWrappers.add(wrapper);

				// Set up follow button
				followButton.setTag(investment);
				followButton.setOnClickListener(itemClickListener);

				openOptionView.setVisibility(View.VISIBLE);
			} else {
				// Settled option
				Calendar today = Calendar.getInstance();
				Calendar estClosing = Calendar.getInstance();
				estClosing.setTime(investment.getTimeEstClosing());

				if (today.get(Calendar.YEAR) == estClosing.get(Calendar.YEAR)
						&& today.get(Calendar.DAY_OF_YEAR) == estClosing.get(Calendar.DAY_OF_YEAR)) {
					// This investment is from today.
					time.setText(timeFormat.format(investment.getTimeEstClosing()));
				} else {
					// This investment is NOT from today.
					time.setText(settledTimeFormat.format(investment.getTimeEstClosing()));
				}

				openOptionView.setVisibility(View.GONE);
				currentLevel.setText(investment.getExpiryLevel());
				currentLevel.setVisibility(View.VISIBLE);
				currentReturn.setVisibility(View.VISIBLE);
				followButton.setFollowButtonState(FollowButtonState.DISABLED);

				boolean isProfitable = InvestmentUtil.isSettledInvestmentProfitable(investment, currency);
				if (isProfitable) {
					ribbonView.setVisibility(View.VISIBLE);
				} else {
					ribbonView.setVisibility(View.GONE);
				}

				boolean isCall = investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL;
				updatePutCallIcon(putCallIcon, isCall, isProfitable);
			}

			itemView.setTag(market);
			itemView.setTag(R.id.investment_id_key, investment.getId());

			itemView.setOnClickListener(itemClickListener);
			lastTradesItemsLayout.addView(itemView);
		}

		lastTradesLayout.setVisibility(View.VISIBLE);
		subscribeTradesLS();
	}

	private void setUpCopiers() {
		copiersScroll = (HorizontalScrollView) rootView.findViewById(R.id.user_profile_copiers_list_content);

		TextView copiersTitle = (TextView) rootView.findViewById(R.id.user_profile_copiers_list_title);
		boolean areSimilarProfiles = profileInfo.getCopiedProfiles() == null;

		List<Profile> profiles = areSimilarProfiles ? profileInfo.getSimilarProfiles()
				: profileInfo.getCopiedProfiles();

		if (profiles == null || profiles.isEmpty()) {
			copiersTitle.setVisibility(View.GONE);
			copiersScroll.setVisibility(View.GONE);
			return;
		}

		copiersScroll.setVisibility(View.VISIBLE);
		copiersTitle.setVisibility(View.VISIBLE);

		String titleText = null;
		if (isMyProfile) {
			if (areSimilarProfiles) {
				titleText = getString(R.string.userProfileTradersYouMayLike);
			} else {
				titleText = getString(R.string.userProfileMyCopiersCopy);
			}
		} else {
			if (areSimilarProfiles) {
				titleText = getString(R.string.userProfileSimilarTo, profileInfo.getNickname());
			} else {
				titleText = getString(R.string.userProfileCopiersCopied, profileInfo.getNickname());
			}
		}

		copiersTitle.setText(titleText);

		LinearLayout copiersLayout = (LinearLayout) rootView.findViewById(R.id.user_profile_copiers_list);
		int itemsCount = profiles.size();

		// Removes copier views which are not needed
		if (copiersLayout.getChildCount() > itemsCount) {
			copiersLayout.removeViews(itemsCount, copiersLayout.getChildCount() - itemsCount);
		}

		for (int i = 0; i < itemsCount; i++) {
			Profile copierProfile = profiles.get(i);
			View itemView = copiersLayout.getChildAt(i);
			boolean isItemAdded;
			if (itemView == null) {
				isItemAdded = false;
				itemView = layoutInflater.inflate(R.layout.user_profile_copier_item, copiersLayout, false);
			} else {
				isItemAdded = true;
			}

			itemView.setTag(copierProfile);
			itemView.setOnClickListener(itemClickListener);

			ImageView avatarView = (ImageView) itemView.findViewById(R.id.user_profile_copiers_item_avatar);
			if (copierProfile.getAvatar() != null) {
				COBitmapUtils.loadBitmap(copierProfile.getAvatar(), avatarView);
			} else {
				avatarView.setImageDrawable(null);
			}

			TextView name = (TextView) itemView.findViewById(R.id.user_profile_copiers_item_name);
			if (copierProfile.getFirstName() != null && copierProfile.getLastName() != null) {
				tempSB.setLength(0);
				tempSB.append(copierProfile.getFirstName()).append("\n").append(copierProfile.getLastName());
				name.setText(tempSB.toString());
			} else if (copierProfile.getNickname() != null) {
				name.setText(copierProfile.getNickname());
			}
			
			TextView hitrate = (TextView) itemView.findViewById(R.id.user_profile_copiers_item_hitrate);
			TextView hitrateText = (TextView) itemView.findViewById(R.id.user_profile_copiers_item_hitrate_text);
			hitrate.setText(String.valueOf(copierProfile.getHitRatePercentage()));
			hitrateText.setText(" " + getString(R.string.hitPercent));
			if (!isItemAdded) {
				copiersLayout.addView(itemView);
			}
		}
	}

	private void setUpCopyingWatchingButtons() {
		copyingButton.setOnClickListener(this);
		watchingButton.setOnClickListener(this);

		TextView copyingText = (TextView) rootView.findViewById(R.id.user_profile_coping_button_text);
		TextView watchingText = (TextView) rootView.findViewById(R.id.user_profile_watching_button_text);

		if (isMyProfile) {
			copyingText.setText(R.string.iAmCopying);
			watchingText.setText(R.string.iAmWatching);
		} else {
			copyingText.setText(getString(R.string.userProfileNicknameIsCopying, profileInfo.getNickname()));
			watchingText.setText(getString(R.string.userProfileNicknameIsWatching, profileInfo.getNickname()));
		}

		TextView copyingCount = (TextView) rootView.findViewById(R.id.user_profile_copying_button_count);
		tempSB.setLength(0);
		tempSB.append("(").append(profileInfo.getCopyingCount()).append(")");
		copyingCount.setText(tempSB.toString());

		TextView watchingCount = (TextView) rootView.findViewById(R.id.user_profile_watching_button_count);
		tempSB.setLength(0);
		tempSB.append("(").append(profileInfo.getWatchingCount()).append(")");
		watchingCount.setText(tempSB.toString());

		if (profileInfo.getCopyingCount() > 0) {
			copyingButton.setClickable(true);
		} else {
			copyingButton.setClickable(false);
		}

		if (profileInfo.getWatchingCount() > 0) {
			watchingButton.setClickable(true);
		} else {
			watchingButton.setClickable(false);
		}
	}

	@Override
	protected Screenable setupScreen() {
		return COScreen.USER_PROFILE;
	}

	@Override
	public void onClick(View v) {
		if (profileInfo == null || isDialogShown) {
			return;
		}

		if (v.getId() == hitButton.getId()) {
			showHit();
		} else if (v.getId() == copiersButton.getId()) {
			showCopiers();
		} else if (v.getId() == watchersButton.getId()) {
			showWatchers();
		} else if (v.getId() == copyButton.getId()) {
				if (profileInfo.isFrozen() && !profileInfo.isCopying()) {
					showDozedOff();
				} else {
					showCopy();

				}
		} else if (v.getId() == watchButton.getId()) {

			if (RegulationUtils.checkRegulationStatusAndDoNecessary(false)) {
				showWatch();
			}

		} else if (v.getId() == profitButton.getId()) {
			toggleProfit();
		} else if (v.getId() == copyingButton.getId()) {
			showCopying();
		} else if (v.getId() == watchingButton.getId()) {
			showWatching();
		}
	}

	@Override
	public void onDismiss() {
		isDialogShown = false;
	}

	@Override
	public void onCopyStateChanged(long userId, boolean isCoping) {
		if (isCoping) {
			copyButton.setState(CopyButtonState.STATE_COPYING);
			watchButton.setState(WatchButtonState.STATE_WATCHING);
			profileInfo.setCopiersCount(profileInfo.getCopiersCount() + 1);
			if (profileInfo.isWatching()) {
				profileInfo.setWatchersCount(profileInfo.getWatchersCount() - 1);
			}
		} else {
			if (profileInfo.isFrozen()) {
				copyButton.setState(CopyButtonState.STATE_FROZEN);
			} else {
				copyButton.setState(CopyButtonState.STATE_NORMAL);
			}

			watchButton.setState(WatchButtonState.STATE_NORMAL);
			profileInfo.setCopiersCount(profileInfo.getCopiersCount() - 1);
		}

		profileInfo.setCopying(isCoping);
		profileInfo.setWatching(isCoping);

		setUpSmallWhiteButtons();
	}

	@Override
	public void onWatchStateChanged(long userId, boolean isWatching) {
		profileInfo.setWatching(isWatching);

		if (isWatching) {
			watchButton.setState(WatchButtonState.STATE_WATCHING);
			profileInfo.setWatchersCount(profileInfo.getWatchersCount() + 1);
		} else {
			watchButton.setState(WatchButtonState.STATE_NORMAL);
			profileInfo.setWatchersCount(profileInfo.getWatchersCount() - 1);
		}

		setUpSmallWhiteButtons();
	}

	private void subscribeTradesLS() {
		unsubscribeTradesLS();
		if (!isLayoutCreated() || profileInfo == null || profileInfo.getLastInvestments() == null
				|| profileInfo.getLastInvestments().size() == 0) {
			return;
		}

		List<? extends Investment> investments = profileInfo.getLastInvestments();
		if (investments != null && investments.size() > 0) {
			StringBuilder sb = new StringBuilder();

			for (int i = 0; i < investments.size(); i++) {
				if (sb.length() == 0 || sb.indexOf(Long.toString(investments.get(i).getMarketId())) == -1) {
					if (sb.length() != 0) {
						sb.append(" ");
					}

					sb.append("aotps_1_").append(investments.get(i).getMarketId());
				}
			}

			Log.v(TAG, "lightstreamer - Going to subscribe for " + sb.toString());
			application.getLightstreamerManager().subscribeAssetsListTable(this, sb.toString().split(" "));
		}
	}

	private void unsubscribeTradesLS() {
		application.getLightstreamerManager().unsubscribeTable(LightstreamerManager.TABLE_ASSETS_LIST);
	}

	@Override
	public void updateItem(int itemPos, String itemName, UpdateInfo update) {
		String currentLevelTxt = update.getNewValue(skinGroup.getLevelUpdateKey());
		double currentLevel = Double.parseDouble(currentLevelTxt.replaceAll(",", ""));

		if (currentLevel == 0) {
			// Do not use this update
			return;
		}

		long marketId = Long.parseLong(update.getNewValue(LightstreamerManager.COLUMN_ET_NAME));
		long opportunityId = Long.parseLong(update.getNewValue(LightstreamerManager.COLUMN_ET_OPP_ID));

		for (int i = 0; i < investmentFieldsWrappers.size(); i++) {
			InvestmentFieldsWrapper fieldsWrapper = investmentFieldsWrappers.get(i);
			Investment investment = fieldsWrapper.getInvestment();

			if ((investment.getOpportunityId() == opportunityId || (investment.getMarketId() == marketId
					&& investment.getAdditionalInfoType() == Investment.INVESTMENT_ADDITIONAL_INFO_NOT_HOURLY))
					&& (investment.getCurrentLevel() != currentLevel || investment.getAmountReturnWF() == null
							|| investment.getAmountReturnWF().isEmpty())) {
				investment.setCurrentLevel(currentLevel);
				investment.setCurrentLevelTxt(currentLevelTxt);

				// Sets the return for this level
				String returnFormatted = AmountUtil.getFormattedAmount(
						InvestmentUtil.getOpenInvestmentReturn(investment, currentLevel), currency.getSymbol(),
						currency.getIsLeftSymbolBool(), currency.getDecimalPointDigits(), currency.getId());
				investment.setAmountReturnWF(returnFormatted);

				if (InvestmentUtil.isOpenInvestmentProfitable(investment, investment.getCurrentLevel())) {
					fieldsWrapper.setProfitable(true);
				} else {
					fieldsWrapper.setProfitable(false);
				}

				fieldsWrapper.invalidateFields();
			}
		}

		if (update.isValueChanged(LightstreamerManager.COLUMN_ET_STATE)) {
			int opportunityState = Integer.parseInt(update.getNewValue(LightstreamerManager.COLUMN_ET_STATE));

			if (opportunityState == Opportunity.STATE_OPENED || opportunityState == Opportunity.STATE_LAST_10_MIN) {
				// The option is opened.
				String timeLastInvestString = update.getNewValue(LightstreamerManager.COLUMN_ET_LAST_INV);
				long timeLastInvest = 0l;
				try {
					timeLastInvest = TimeUtils.parseTimeFromLSUpdate(timeLastInvestString).getTime();
				} catch (ParseException e) {
					Log.e(TAG, "Could not parse ET_LAST_INV time");
				}

				if (timeLastInvest != 0l) {
					for (int i = 0; i < investmentFieldsWrappers.size(); i++) {
						InvestmentFieldsWrapper fieldsWrapper = investmentFieldsWrappers.get(i);
						if (fieldsWrapper.getInvestment().getMarketId() == marketId) {
							fieldsWrapper.setTimeLastInvest(timeLastInvest);
						}
					}
				}

				if (timeLastInvest > timerEndTime) {
					timerEndTime = timeLastInvest;

					if (isVisible()) {
						Message message = handler.obtainMessage(MESSAGE_START_TIMER);
						message.sendToTarget();
					}
				}
			} else if (opportunityState >= Opportunity.STATE_CLOSED) {
				// The option is closed
				for (int i = 0; i < investmentFieldsWrappers.size(); i++) {
					InvestmentFieldsWrapper fieldsWrapper = investmentFieldsWrappers.get(i);
					if (fieldsWrapper.getInvestment().getMarketId() == marketId) {
						fieldsWrapper.setSettled(true);
						fieldsWrapper.invalidateFields();
						investmentFieldsWrappers.remove(i);
						i--;
					}
				}
			} else if (opportunityState > Opportunity.STATE_LAST_10_MIN) {
				// The option is expiring. Can not follow.
				for (int i = 0; i < investmentFieldsWrappers.size(); i++) {
					InvestmentFieldsWrapper fieldsWrapper = investmentFieldsWrappers.get(i);
					if (fieldsWrapper.getInvestment().getMarketId() == marketId) {
						fieldsWrapper.setExpiring(true);
						fieldsWrapper.invalidateFields();
					}
				}
			}
		}
	}

	@Override
	public boolean handleMessage(Message msg) {
		if (msg.what == MESSAGE_START_TIMER) {
			startTimer();
			return true;
		}

		return false;
	}

	private void showHit() {
		Bundle bundle = new Bundle();
		bundle.putLong(COConstants.EXTRA_PROFILE_ID, profileUserId);
		application.getFragmentManager().showDialogFragment(HitDialogFragment.class, bundle);
	}

	private void showCopy() {

		if((!profileInfo.isCopying() && RegulationUtils.checkRegulationAndPepAndThresholdStatusAndDoNecessary(false)) || profileInfo.isCopying()) {
			isDialogShown = true;

			if (!profileInfo.isCopying()
					&& profileInfo.getCopiersCount() >= COApplication.get().getMaxCopiersPerAccount()) {
				popUpManager.showFullyCopiedPopUp(profileUserId, profileInfo.getNickname(), profileInfo.isWatching(), this,
						this);
				return;
			}

			Bundle args = new Bundle();
			args.putBoolean(COConstants.EXTRA_IS_COPYING, profileInfo.isCopying());
			args.putBoolean(COConstants.EXTRA_IS_FROZEN, profileInfo.isFrozen());
			args.putLong(COConstants.EXTRA_PROFILE_ID, profileUserId);
			args.putString(COConstants.EXTRA_NICKNAME, profileInfo.getNickname());

			CopyDialogFragment dialog = (CopyDialogFragment) fragmentManager.showDialogFragment(CopyDialogFragment.class, args);
			dialog.setOnDismissListener(this);
			dialog.setOnCopyStateChangedListener(this);
		}
	}

	private void showWatch() {
		isDialogShown = true;

		if (profileInfo.isCopying()) {
			popUpManager.showCanNotUnwatchPopUp(profileInfo.getNickname(), this);
			return;
		}

		Bundle args = new Bundle();
		args.putBoolean(COConstants.EXTRA_IS_WATCHING, profileInfo.isWatching());
		args.putLong(COConstants.EXTRA_PROFILE_ID, profileUserId);
		args.putString(COConstants.EXTRA_NICKNAME, profileInfo.getNickname());

		WatchDialogFragment dialog = (WatchDialogFragment) fragmentManager.showDialogFragment(WatchDialogFragment.class,
				args);
		dialog.setOnDismissListener(this);
		dialog.setOnWatchStateChangedListener(this);
	}

	private void toggleProfit() {
		if (profitContent.getVisibility() == View.VISIBLE) {
			profitOpenIcon.setImageResource(R.drawable.profit_acordion_plus);
			profitButton.setBackgroundResource(R.drawable.profile_white_big_button_bg);
			profitContent.setVisibility(View.GONE);
		} else {
			profitOpenIcon.setImageResource(R.drawable.profit_acordion_minus);
			profitButton.setBackgroundResource(R.drawable.profile_white_big_button_open_bg);
			profitContent.setVisibility(View.VISIBLE);
		}
	}

	private void showCopying() {
		Bundle args = new Bundle();
		if (!isMyProfile) {
			args.putLong(COConstants.EXTRA_PROFILE_ID, profileUserId);
		}

		application.postEvent(new NavigationEvent(COScreen.COPYING_LIST, NavigationType.DEEP, args));
	}

	private void showCopiers() {
		Bundle args = new Bundle();
		if (!isMyProfile) {
			args.putLong(COConstants.EXTRA_PROFILE_ID, profileUserId);
		}

		application.postEvent(new NavigationEvent(COScreen.COPIERS_LIST, NavigationType.DEEP, args));
	}

	private void showWatching() {
		Bundle args = new Bundle();
		if (!isMyProfile) {
			args.putLong(COConstants.EXTRA_PROFILE_ID, profileUserId);
		}

		application.postEvent(new NavigationEvent(COScreen.WATCHING_LIST, NavigationType.DEEP, args));
	}

	private void showWatchers() {
		Bundle args = new Bundle();
		if (!isMyProfile) {
			args.putLong(COConstants.EXTRA_PROFILE_ID, profileUserId);
		}

		application.postEvent(new NavigationEvent(COScreen.WATCHERS_LIST, NavigationType.DEEP, args));
	}

	private void showDozedOff() {
		isDialogShown = true;
		popUpManager.showDozedOffPopUp(this);
	}

	/**
	 * Used to hold references to the fields which will be updated from LS.
	 */
	private class InvestmentFieldsWrapper implements Callback {

		private static final int MESSAGE_UPDATE_FIELDS = 123;

		private Investment investment;
		private TextView time;
		private TextView currentLevel;
		private TextView currentReturn;
		private ImageView putCallIcon;
		private View ribbonView;
		private FollowButton followButton;
		private View openOptionView;

		private boolean isProfitable;
		private boolean isSettled;
		private boolean isExpiring;
		private long timeLastInvest;

		private boolean isCall;

		private Handler wrapperHandler;

		public InvestmentFieldsWrapper(Investment investment, TextView time, TextView currentLevel,
				TextView currentReturn, ImageView putCallIcon, View ribbonView, FollowButton followButton,
				View openOptionView) {

			this.investment = investment;
			this.time = time;
			this.currentLevel = currentLevel;
			this.currentReturn = currentReturn;
			this.putCallIcon = putCallIcon;
			this.ribbonView = ribbonView;
			this.followButton = followButton;
			this.openOptionView = openOptionView;

			isSettled = false;
			isExpiring = false;
			isProfitable = false;

			isCall = investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL;

			wrapperHandler = new Handler(Looper.getMainLooper(), this);
		}

		public void invalidateFields() {
			if (!wrapperHandler.hasMessages(MESSAGE_UPDATE_FIELDS)) {
				Message message = wrapperHandler.obtainMessage(MESSAGE_UPDATE_FIELDS);
				message.sendToTarget();
			}
		}

		@Override
		public boolean handleMessage(Message msg) {
			if (msg.what == MESSAGE_UPDATE_FIELDS) {
				updateFields();
				return true;
			}

			return false;
		}

		private void updateFields() {
			if (!isVisible()) {
				return;
			}

			if (isExpiring) {
				followButton.setFollowButtonState(FollowButtonState.DISABLED);
				time.setText(timeFormat.format(investment.getTimeEstClosing()));
			}

			if (isProfitable) {
				ribbonView.setVisibility(View.VISIBLE);
			} else {
				ribbonView.setVisibility(View.GONE);
			}
			updatePutCallIcon(putCallIcon, isCall, isProfitable);

			if (isSettled) {
				followButton.setFollowButtonState(FollowButtonState.DISABLED);
				investmentFieldsWrappers.remove(InvestmentFieldsWrapper.this);

				openOptionView.setVisibility(View.GONE);
				currentLevel.setText(investment.getCurrentLevelTxt());
				currentReturn.setText(investment.getAmountReturnWF());
				currentLevel.setVisibility(View.VISIBLE);
				currentReturn.setVisibility(View.VISIBLE);

				investment.setTimeSettled(investment.getTimeEstClosing());
				sortInvestmentViews();
			}
		}

		private void updateTimeLeft() {
			if (isExpiring || isSettled) {
				return;
			}

			long timeLeft = timeLastInvest - TimeUtils.getCurrentServerTime();
			if (timeLeft <= 0) {
				isExpiring = true;
				timeLeft = 0;
			}

			String timeLeftStr = TimeUtils.formatTimeToMinutesAndSeconds(timeLeft);
			time.setText(timeLeftStr);
		}

		public Investment getInvestment() {
			return investment;
		}

		public void setProfitable(boolean isProfitable) {
			this.isProfitable = isProfitable;
		}

		public void setSettled(boolean isSettled) {
			this.isSettled = isSettled;
		}

		public void setExpiring(boolean isExpiring) {
			this.isExpiring = isExpiring;
		}

		public void setTimeLastInvest(long timeLastInvest) {
			this.timeLastInvest = timeLastInvest;
		}

	}

	private void openProfile(long profileId, String nickname) {
		if (isDialogShown) {
			return;
		}

		Bundle args = new Bundle();
		args.putLong(COConstants.EXTRA_PROFILE_ID, profileId);
		args.putString(Constants.EXTRA_ACTION_BAR_TITLE, nickname);
		NavigationEvent navigationEvent = new NavigationEvent(COScreen.USER_PROFILE, NavigationType.DEEP, args);
		application.postEvent(navigationEvent);
	}

	private void openFollow(Investment investment) {
		if (isDialogShown) {
			return;
		}

		Bundle args = new Bundle();
		args.putLong(COConstants.EXTRA_MARKET_ID, investment.getMarketId());
		args.putLong(COConstants.EXTRA_INVESTMENT_ID, investment.getId());
		args.putInt(COConstants.EXTRA_INVESTMENT_TYPE, (int) investment.getTypeId());

		if (RegulationUtils.checkRegulationAndPepAndThresholdStatusAndDoNecessary(false)) {
			BaseDialogFragment dialog = fragmentManager.showDialogFragment(FollowDialogFragment.class, args);
			dialog.setOnDismissListener(UserProfileFragment.this);
			isDialogShown = true;
		}
	}

	private void openMarket(Market market) {
		if (market != null) {
			Bundle bundle = new Bundle();

			if (isTablet) {
				bundle.putSerializable(Constants.EXTRA_MARKET, market);
				bundle.putString(Constants.EXTRA_ACTION_BAR_TITLE, market.getDisplayName());
				bundle.putLong(Constants.EXTRA_MARKET_PRODUCT_TYPE, market.getProductTypeId());

				application.postEvent(new NavigationEvent(Screen.ASSET_PAGE, NavigationType.HORIZONTAL, bundle));
				application.postEvent(new NavigationEvent(Screen.TABLET_TRADE, NavigationType.INIT));
			} else {
				bundle.putSerializable(Constants.EXTRA_MARKET, market);
				application.postEvent(new NavigationEvent(Screen.ASSET_VIEW_PAGER, NavigationType.DEEP, bundle));
			}
		}
	}

	private void setUpWalkthrough() {
		if (isMyProfile) {
			return;
		}

		boolean shouldShowWalkthrough = !application.getSharedPreferencesManager()
				.getBoolean(COConstants.PREFERENCES_IS_PROFILE_WALKTHROUGH_SHOWN, false);
		if (shouldShowWalkthrough) {
			BaseDialogFragment dialog = application.getFragmentManager()
					.showDialogFragment(ProfileWalkthroughDialog.class, new Bundle());
			dialog.setOnDismissListener(new OnDialogDismissListener() {

				@Override
				public void onDismiss() {
					FragmentTransaction transaction = getFragmentManager().beginTransaction();
					transaction.setTransition(android.R.anim.fade_in);
					transaction.show(facebookFragment);
					transaction.commit();
				}
			});

			FragmentTransaction transaction = getFragmentManager().beginTransaction();
			transaction.setTransition(android.R.anim.fade_out);
			transaction.hide(facebookFragment);
			transaction.commit();
		}
	}

	private void sortInvestments() {
		List<Investment> investments = (List<Investment>)profileInfo.getLastInvestments();

		int size = investments.size();
		if (size <= 1) {
			return;
		}

		Investment temp = null;

		for (int i = 0; i < size; i++) {
			for (int j = 1; j < (size - i); j++) {
				boolean isOnlyFirstSettled = (investments.get(j - 1).getTimeSettled() != null
						&& investments.get(j).getTimeSettled() == null);
				boolean isOnlyFirstOpen = investments.get(j - 1).getTimeSettled() == null
						&& investments.get(j).getTimeSettled() != null;
				boolean isFirstOlder = investments.get(j - 1).getTimeCreated()
						.compareTo(investments.get(j).getTimeCreated()) < 0;

				if (isOnlyFirstSettled || (!isOnlyFirstOpen && isFirstOlder)) {
					// Swap the elements
					temp = investments.get(j - 1);
					investments.remove(j - 1);
					investments.add(j, temp);
				}
			}
		}
	}

	/**
	 * Use to sort last trades list after some of the investments is settled.
	 */
	private void sortInvestmentViews() {
		Log.d(TAG, "sortInvestmentViews");

		List<? extends Investment> investments = profileInfo.getLastInvestments();
		if (investments.size() <= 1) {
			// Sort not needed.
			return;
		}

		sortInvestments();

		for (int i = 0; i < investments.size() - 1; i++) {
			long investmentId = investments.get(i).getId();
			if (investmentId != (Long) lastTradesItemsLayout.getChildAt(i).getTag(R.id.investment_id_key)) {
				for (int j = i + 1; j < investments.size(); j++) {
					if (investmentId == (Long) lastTradesItemsLayout.getChildAt(j).getTag(R.id.investment_id_key)) {
						// Moves the view in its layout
						View temp = lastTradesItemsLayout.getChildAt(j);
						lastTradesItemsLayout.removeViewAt(j);
						lastTradesItemsLayout.addView(temp, i);
					}
				}
			}
		}
	}

	private static void updatePutCallIcon(ImageView putCallIcon, boolean isCall, boolean isProfitable) {
		int iconResId = 0;

		if (isProfitable) {
			if (isCall) {
				iconResId = R.drawable.trading_options_call_win;
			} else {
				iconResId = R.drawable.trading_options_put_win;
			}
		} else {
			if (isCall) {
				iconResId = R.drawable.trading_options_call_lose;
			} else {
				iconResId = R.drawable.trading_options_put_lose;
			}
		}

		putCallIcon.setImageResource(iconResId);
	}

	/**
	 * Starts the timer with the specified duration.
	 */
	public void startTimer() {
		if (timer != null) {
			timer.cancel();
		}

		if (timerEndTime == 0l) {
			return;
		}

		long duration = timerEndTime - TimeUtils.getCurrentServerTime();
		timer = new CountDownTimer(duration, TIMER_UPDATE_INVERVAL) {

			@Override
			public void onTick(long millisUntilFinished) {
				for (int i = 0; i < investmentFieldsWrappers.size(); i++) {
					investmentFieldsWrappers.get(i).updateTimeLeft();
				}
			}

			@Override
			public void onFinish() {
				timer = null;
				timerEndTime = 0l;
			}
		}.start();
	}

	public void stopTimer() {
		timerEndTime = 0l;

		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}

}