package com.copyop.android.app.widget;

import com.copyop.android.app.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

public class PoweredByAnyoptionLayout extends RelativeLayout{

	public PoweredByAnyoptionLayout(Context context) {
		super(context);
		init(context, null, 0);
	}
	public PoweredByAnyoptionLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	public PoweredByAnyoptionLayout(Context context, AttributeSet attrs,int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		View.inflate(context, R.layout.powered_by_anyoption_layout, this);
	}

}
