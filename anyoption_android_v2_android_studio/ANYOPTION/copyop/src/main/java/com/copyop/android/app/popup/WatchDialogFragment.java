package com.copyop.android.app.popup;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.manager.CommunicationManager;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.results.MethodResult;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COConstants;
import com.copyop.common.enums.base.ProfileLinkCommandEnum;
import com.copyop.json.requests.ProfileMethodRequest;
import com.copyop.json.requests.WatchUserConfigMethodRequest;
import com.copyop.json.results.WatchUserConfigMethodResult;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.TextView;

public class WatchDialogFragment extends BaseDialogFragment implements OnClickListener,
		OnCheckedChangeListener {

	private static final String TAG = WatchDialogFragment.class.getSimpleName();

	public interface OnWatchStateChangedListener {

		/**
		 * Called when the user watch / unwatch request finishes successfully.
		 * 
		 * @param userId
		 *            the user which is/was copied
		 * @param isWatching
		 *            the new state
		 */
		public void onWatchStateChanged(long userId, boolean isWatching);

	}

	private CommunicationManager communicationManager;
	private OnWatchStateChangedListener onWatchStateChangedListener;

	private boolean isWatching;
	private long requestedUserId;
	private String nickname;

	private TextView titleView;
	private View stopWatchingLayout;
	private CheckBox stopWatchingCheckBox;

	private TextView cancelButton;

	/**
	 * Used for watch and OK button.
	 */
	private TextView actionButton;

	private TextView sendMeAlertView;
	private View alertsEditView;
	private View alertsEditDisabledView;

	private View yesNoLayout;
	private RadioButton yesRadioButton;
	private RadioButton noRadioButton;
	private TextView alertsChoiceView;

	/**
	 * Use this method to create new instance of the dialog.
	 * 
	 * @return
	 */
	public static WatchDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new WatchDialogFragment.");
		WatchDialogFragment fragment = new WatchDialogFragment();
		fragment.setArguments(bundle);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		if (args != null) {
			isWatching = args.getBoolean(COConstants.EXTRA_IS_WATCHING, false);
			requestedUserId = args.getLong(COConstants.EXTRA_PROFILE_ID, 0l);
			nickname = args.getString(COConstants.EXTRA_NICKNAME, null);
		}

		if (requestedUserId == 0l || nickname == null) {
			Log.e(TAG, "Can not create watch dialog without requestedUserId and nickname");
		}

		application = Application.get();
		communicationManager = application.getCommunicationManager();

		COApplication.get().getAppseeManager().onWatchPopup();
	}

	/**
	 * Set up content.
	 * 
	 * @param contentView
	 */
	@Override
	protected void onCreateContentView(View contentView) {
		cancelButton = (TextView) contentView.findViewById(R.id.watch_cancel_button);
		cancelButton.setOnClickListener(this);

		actionButton = (TextView) contentView.findViewById(R.id.watch_action_button);
		actionButton.setOnClickListener(this);

		sendMeAlertView = (TextView) contentView.findViewById(R.id.watch_send_me_alert);
		alertsEditView = contentView.findViewById(R.id.watch_alerts_edit_layout);
		yesNoLayout = contentView.findViewById(R.id.watch_yes_no_layout);
		yesRadioButton = (RadioButton) contentView.findViewById(R.id.watch_notifications_yes);
		noRadioButton = (RadioButton) contentView.findViewById(R.id.watch_notifications_no);

		if (isWatching) {
			stopWatchingLayout = contentView.findViewById(R.id.watch_stop_watching_layout);
			stopWatchingCheckBox = (CheckBox) contentView
					.findViewById(R.id.watch_stop_watching_check_box);
			alertsChoiceView = (TextView) contentView.findViewById(R.id.watch_alerts_choice);
			setUpStopWatching();
		} else {

			String sendMeAlertText = getString(R.string.watchSendMeAlerts) + " "
					+ getString(R.string.watchSendMeAlertsComment);
			sendMeAlertView.setText(sendMeAlertText);
		}

		titleView = (TextView) contentView.findViewById(R.id.watch_title);
		setUpTitle();

		alertsEditDisabledView = contentView.findViewById(R.id.watch_notify_me_disabled_view);

		if (isWatching) {
			getWatchConfig();
		}
	}

	@Override
	protected int getContentLayout() {
		return R.layout.watch_dialog;
	}

	private void setUpStopWatching() {
		sendMeAlertView.setVisibility(View.GONE);
		alertsEditView.setVisibility(View.VISIBLE);
		alertsEditView.setOnClickListener(this);
		stopWatchingLayout.setVisibility(View.VISIBLE);
		yesNoLayout.setVisibility(View.GONE);
		stopWatchingCheckBox.setOnCheckedChangeListener(this);
		actionButton.setText(R.string.ok);
		yesRadioButton.setOnCheckedChangeListener(this);
		noRadioButton.setOnCheckedChangeListener(this);
	}

	private void setUpTitle() {
		if (isWatching) {
			titleView.setText(R.string.watchSettingsTitle);
		} else {
			titleView.setText(getString(R.string.watchWatch) + " " + nickname);
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == cancelButton.getId()) {
			dismiss();
		} else if (v.getId() == actionButton.getId()) {
			configWatch();
		} else if (v.getId() == alertsEditView.getId()) {
			if (yesNoLayout.getVisibility() == View.VISIBLE) {
				yesNoLayout.setVisibility(View.GONE);
			} else {
				yesNoLayout.setVisibility(View.VISIBLE);
			}
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (isWatching && buttonView.getId() == stopWatchingCheckBox.getId()) {
			if (isChecked) {
				yesNoLayout.setVisibility(View.GONE);
				alertsEditDisabledView.setVisibility(View.VISIBLE);
			} else {
				alertsEditDisabledView.setVisibility(View.GONE);
			}
		} else if (buttonView.getId() == yesRadioButton.getId() && isChecked) {
			alertsChoiceView.setText(R.string.watchYesLower);
		} else if (buttonView.getId() == noRadioButton.getId() && isChecked) {
			alertsChoiceView.setText(R.string.watchNoLower);
		}
	}

	private void getWatchConfig() {
		ProfileMethodRequest request = new ProfileMethodRequest();
		request.setRequestedUserId(requestedUserId);

		communicationManager.requestService(this, "getConfigCallBack",
				COConstants.SERVICE_GET_WATCH_USER_CONFIG, request,
				WatchUserConfigMethodResult.class);
	}

	public void getConfigCallBack(Object resultObj) {
		WatchUserConfigMethodResult result = (WatchUserConfigMethodResult) resultObj;

		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			if (result.isWatchingPushNotification()) {
				yesRadioButton.setChecked(true);
			} else {
				noRadioButton.setChecked(true);
			}
		} else {
			if (result != null) {
				Log.e(TAG, "Could not get watch config. ErrorCode: " + result.getErrorCode());
				if (result.getErrorCode() == COConstants.ERROR_CODE_USER_REMOVED) {
					Log.e(TAG, "This user is removed!");
					dismiss();
				}
			} else {
				Log.e(TAG, "Could not get watch config. Result = null");
			}
		}
	}

	private void configWatch() {
		if (!isWatching && application.getUser().getBalance() == 0) {
			application.getPopUpManager().showInsufficienFundsPopUp();
			dismiss();
			return;
		}

		WatchUserConfigMethodRequest request = new WatchUserConfigMethodRequest();
		request.setDestUserId(requestedUserId);
		request.setWatchingPushNotification(yesRadioButton.isChecked());

		if (!isWatching || !stopWatchingCheckBox.isChecked()) {
			request.setCommand(ProfileLinkCommandEnum.WATCH);
		} else {
			request.setCommand(ProfileLinkCommandEnum.UNWATCH);
		}

		communicationManager
				.requestService(this, "watchCallBack",
						COConstants.SERVICE_WATCH_CONFIG, request,
						MethodResult.class);

	}

	public void watchCallBack(Object resultObj) {
		MethodResult result = (MethodResult) resultObj;

		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			if (onWatchStateChangedListener != null) {
				if (!isWatching || !stopWatchingCheckBox.isChecked()) {
					onWatchStateChangedListener.onWatchStateChanged(requestedUserId, true);
				} else {
					onWatchStateChangedListener.onWatchStateChanged(requestedUserId, false);
				}
			}

			COApplication.get().refreshWatchers();
		} else {
			if (result != null) {
				Log.e(TAG, "Could not watch/unwatch user. ErrorCode: " + result.getErrorCode());
			} else {
				Log.e(TAG, "Could not watch/unwatch user. Result = null");
			}
		}

		dismiss();
	}

	public OnWatchStateChangedListener getOnWatchStateChangedListener() {
		return onWatchStateChangedListener;
	}

	public void setOnWatchStateChangedListener(
			OnWatchStateChangedListener onWatchStateChangedListener) {
		this.onWatchStateChangedListener = onWatchStateChangedListener;
	}

}
