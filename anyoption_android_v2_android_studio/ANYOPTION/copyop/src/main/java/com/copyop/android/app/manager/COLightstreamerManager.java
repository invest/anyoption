package com.copyop.android.app.manager;

import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.lightstreamer.AOTPSTableListener;
import com.anyoption.android.app.lightstreamer.LightstreamerConnectionHandler;
import com.anyoption.android.app.lightstreamer.LightstreamerListener;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.event.InAppNotificationEvent;
import com.copyop.android.app.event.InboxLightstreamerEvent;
import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.enums.base.QueueTypeEnum;
import com.lightstreamer.ls_client.ConnectionInfo;
import com.lightstreamer.ls_client.ExtendedTableInfo;
import com.lightstreamer.ls_client.SimpleTableInfo;
import com.lightstreamer.ls_client.UpdateInfo;

import android.util.Log;

public class COLightstreamerManager extends LightstreamerManager {

	public static final String TAG = COLightstreamerManager.class.getSimpleName();
	
	public static final String LS_CO_ADAPTER 		= "COPYOP";
	public static final String LS_CO_DATA_ADAPTER 	= "JMS_AMQ_ADAPTER";
	
	// ####################################################################
	// ######### 				MAX FREQUENCIES 				###########
	// ####################################################################

	public static final double MAX_FREQUENCIES_NEWS					 = 1.1;
	
	public static final double MAX_FREQUENCIES_FOLLOW				 = 1.1;

	// ####################################################################
	
	//#####################################################################
	//#########					TABLES							###########
	//#####################################################################

	public static final String TABLE_NEWS					= "table_news";
	public static final String TABLE_EXPLORE				= "table_explore";
	public static final String TABLE_INBOX					= "table_inbox";
	public static final String TABLE_FOLLOW					= "table_follow";
	public static final String TABLE_NOTIFICATIONS			= "table_notifications";
	
	//********************************************************************
	//*********					COLUMNS							**********
	//********************************************************************
	public static final String COLUMN_NEWS_KEY				= "key";
	
	//********************************************************************
	
	
	//#####################################################################
	
	
	protected LightstreamerConnectionHandler coLS;
	
	public COLightstreamerManager(COApplication application) {
		super(application);
	}

	public LightstreamerConnectionHandler getCopyopLS(){
		return coLS;
	}

	@Override
	public void startLightstreamer() {
		super.startLightstreamer();
		startCopyopLightstreamer();
	}
	
	@Override
	public void stopLightstreamer() {
		super.stopLightstreamer();
		stopCopyopLightstreamer();
	}
	
	/**
	 * Starts the COPYOP LIGHSTREAMER.
	 */
	public void startCopyopLightstreamer() {
		// Stop it if it's already started.
		stopCopyopLightstreamer();
		
		coLS = getCopyopLightstreamerConnectionHandler();
		coLS.start();
	}
	
	private LightstreamerConnectionHandler getCopyopLightstreamerConnectionHandler() {
		ConnectionInfo cInfo = new ConnectionInfo();
		cInfo.pushServerUrl = COApplication.get().getCopyopLSURL();
		cInfo.adapter = COLightstreamerManager.LS_CO_ADAPTER;
		if (user != null) {
			// Leading and trailing "a"s are added because if the name starts with digits
			// LIGHSTREAMER is dropping them
			cInfo.user = "a" + user.getUserName() + "a";
			cInfo.password = user.getHashedPassword();
		}
		return new LightstreamerConnectionHandler(cInfo, this);
	}
	
	//#####################################################################
	//#########					EVENTs CALLBACKs				###########
	//#####################################################################
	
	@Override
	public void onEventMainThread(LoginLogoutEvent event) {
		super.onEventMainThread(event);
		Log.d(TAG, "onEventMainThread : login event");
		if (event.getType() == LoginLogoutEvent.Type.LOGIN || event.getType() == LoginLogoutEvent.Type.REGISTER) {
			subscribeNotificationsTable(this);
			subscribeInboxTable(this);
		}
	}
	
	//#####################################################################
	
	//#####################################################################
	//#########					LIGHSTREAMER UPDATE				###########
	//#####################################################################
	
	@Override
	public void updateItem(int itemPos, String itemName, UpdateInfo update) {
		try {
			if(itemName.startsWith(QueueTypeEnum.IN_APP_STRIP.getId() + "_" + user.getUserName() + "_" + user.getId())){
				application.postEvent(new InAppNotificationEvent(update));
				return;
			}else if(itemName.startsWith(QueueTypeEnum.INBOX.getId() + "_" + user.getUserName() + "_" + user.getId())){
				application.postEvent(new InboxLightstreamerEvent(update));
				return;
			}
		} catch (Exception e) {
			Log.e(TAG, "Could NOT handle Lighstreamer update", e);
			return;
		}
		super.updateItem(itemPos, itemName, update);
	}
	
	//#####################################################################
	
	/**
	 * Stops the COPYOP LIGHSTREAMER.
	 */
	public void stopCopyopLightstreamer() {
		if(coLS != null) {
			coLS.stop();
		}
	}
	
	protected String[] getUpdateKeys(){
		return new String[] {
				FeedMessage.PROPERTY_KEY_UPDATE_TYPE,
				FeedMessage.PROPERTY_KEY_AMOUNT_IN_CURR + user.getCurrencyId(), 
				FeedMessage.PROPERTY_KEY_ASSET_LAST_TRADING_HOURS,
				FeedMessage.PROPERTY_KEY_ASSET_TREND, FeedMessage.PROPERTY_KEY_AVATAR, FeedMessage.PROPERTY_KEY_COPIED_COUNT, 
				FeedMessage.PROPERTY_KEY_COPIERS, FeedMessage.PROPERTY_KEY_DEST_USER_ID, FeedMessage.PROPERTY_KEY_DIRECTION, FeedMessage.PROPERTY_KEY_HIT_RATE, 
				FeedMessage.PROPERTY_KEY_INV_ID, FeedMessage.PROPERTY_KEY_INVESTMENT_TYPE, FeedMessage.PROPERTY_KEY_LEVEL, FeedMessage.PROPERTY_KEY_LEVEL,
				FeedMessage.PROPERTY_KEY_MARKET_ID, FeedMessage.PROPERTY_KEY_NICKNAME, FeedMessage.PROPERTY_KEY_ODDS_LOSE, 
				FeedMessage.PROPERTY_KEY_ODDS_WIN, FeedMessage.PROPERTY_KEY_OPPORTUNITY_EXPIRE, FeedMessage.PROPERTY_KEY_OPPORTUNITY_ID, 
				FeedMessage.PROPERTY_KEY_PROFIT + "_" + user.getCurrencyId(),
				FeedMessage.PROPERTY_KEY_TIME_SETTLED, FeedMessage.PROPERTY_KEY_TIME_CREATED, FeedMessage.PROPERTY_KEY_USER_ID, FeedMessage.PROPERTY_KEY_WATCHERS,
				FeedMessage.PROPERTY_KEY_DEST_NICKNAME, FeedMessage.PROPERTY_BONUS_TYPE_ID, FeedMessage.PROPERTY_BONUS_DESCRIPTION,
				FeedMessage.PROPERTY_KEY_TOP_RANK, FeedMessage.PROPERTY_KEY_RANKED_IN_TOP, FeedMessage.PROPERTY_KEY_COINS_BALANCE,
				FeedMessage.PROPERTY_KEY_SEATS_LEFT, FeedMessage.PROPERTY_KEY_FIRST_NAME, FeedMessage.PROPERTY_KEY_LAST_NAME, FeedMessage.PROPERTY_KEY_OLD_NICKNAME,
				FeedMessage.PROPERTY_KEY_UUID,FeedMessage.PROPERTY_KEY_INHERITED_UUID, FeedMessage.PROPERTY_KEY_TIME_LAST_INVEST, FeedMessage.PROPERTY_KEY_USER_FROZEN,
				FeedMessage.PROPERTY_KEY_DEST_USER_FROZEN
			};
	}
	
	public boolean subscribeNotificationsTable(LightstreamerListener listener) {
		if(user == null){
			return false;
		}
		
		return subscribeCopyopTable(	
				listener,
				TABLE_NOTIFICATIONS,
				MAX_FREQUENCIES_NEWS, 
				new String[]{QueueTypeEnum.IN_APP_STRIP.getId() + "_" + user.getUserName() + "_" + user.getId()},  
				SimpleTableInfo.MERGE, 
				false, 
				getUpdateKeys());
	}
	
	public void unSubscribeNotificationsTable() {
		unsubscribeCopyopTable(TABLE_NOTIFICATIONS);
	}
	
	public boolean subscribeNewsTable(LightstreamerListener listener) {
		if(user == null){
			return false;
		}
		
		return subscribeCopyopTable(	
				listener,
				TABLE_NEWS,
				MAX_FREQUENCIES_NEWS, 
				new String[]{QueueTypeEnum.NEWS.getId() + "_" + user.getUserName() + "_" + user.getId()},  
				SimpleTableInfo.MERGE, 
				false, 
				getUpdateKeys());
	}
	
	public void unSubscribeNewsTable() {
		unsubscribeCopyopTable(TABLE_NEWS);
	}
	
	public boolean subscribeExploreTable(LightstreamerListener listener) {
		if(user == null){
			return false;
		}
		
		return subscribeCopyopTable(	
				listener,
				TABLE_EXPLORE,
				MAX_FREQUENCIES_NEWS, 
				new String[]{QueueTypeEnum.EXPLORE.getId() + "_" + user.getUserName() + "_" + user.getId()},  
				SimpleTableInfo.MERGE, 
				false, 
				getUpdateKeys());
	}
	
	public void unSubscribeExploreTable() {
		unsubscribeCopyopTable(TABLE_EXPLORE);
	}
	
	public boolean subscribeInboxTable(LightstreamerListener listener) {
		if(user == null){
			return false;
		}
		
		return subscribeCopyopTable(	
				listener,
				TABLE_INBOX,
				MAX_FREQUENCIES_NEWS, 
				new String[]{QueueTypeEnum.INBOX.getId() + "_" + user.getUserName() + "_" + user.getId()},  
				SimpleTableInfo.MERGE, 
				false, 
				getUpdateKeys());
	}
	
	public void unSubscribeInboxTable() {
		unsubscribeCopyopTable(TABLE_INBOX);
	}
	
	public boolean subscribeFollowTable(LightstreamerListener listener, long marketId) {
		if (user == null) {
			return false;
		}

		return subscribeTable(
				listener,
				TABLE_FOLLOW,
				MAX_FREQUENCIES_FOLLOW,
				new String[] { "aotps_1_" + marketId },
				SimpleTableInfo.COMMAND,
				true,
				new String[] {
						COLUMN_ET_KEY, COLUMN_ET_OPP_ID, COLUMN_ET_STATE,
						skinGroup.getLevelUpdateKey(), COLUMN_ET_EST_CLOSE, COLUMN_ET_ODDS_LOSE,
						COLUMN_ET_ODDS_WIN
				});
	}

	public void unSubscribeFollowTable() {
		unsubscribeTable(TABLE_FOLLOW);
	}

	public boolean subscribeCopyopTable(LightstreamerListener listener, String tableName, double maxFrequency , String[] items, String mode, boolean snapShot, String[] columns) {
		Log.d(TAG, "SUBCRIBE COPYOP Table = " + tableName);
		if(coLS == null) {
			startCopyopLightstreamer();
		}
		if(isTableSubscribed(tableName)){
			unsubscribeCopyopTable(tableName);			
		}
		try {
			ExtendedTableInfo extendedTableInfo = new ExtendedTableInfo(items, mode, columns, snapShot);
			extendedTableInfo.setDataAdapter(COLightstreamerManager.LS_CO_DATA_ADAPTER);
			extendedTableInfo.setRequestedMaxFrequency(maxFrequency);
			coLS.addTable(extendedTableInfo, new AOTPSTableListener(listener));
			tablesMap.put(tableName, extendedTableInfo);				
		} catch (Exception e) {
			Log.e(TAG, "Can't subscribe tables : " + items, e);
			return false;
		}
		return true;
	}
	
	public void unsubscribeCopyopTable(String... tables) {
		for(String table : tables){
			Log.d(TAG, "UNSUBSCRIBE COPYOP Table = " + table);
			final ExtendedTableInfo extendedTableInfo = tablesMap.get(table);
			if(coLS != null) {
				coLS.removeTable(extendedTableInfo);						
			}
			tablesMap.remove(table);					
		}
	}
}
