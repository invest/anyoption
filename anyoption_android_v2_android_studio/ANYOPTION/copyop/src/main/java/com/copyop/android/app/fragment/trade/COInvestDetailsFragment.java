package com.copyop.android.app.fragment.trade;

import com.anyoption.android.app.fragment.trade.InvestDetailsFragment;
import com.anyoption.android.app.util.DrawableUtils;
import com.copyop.android.app.util.COInvestmentUtil;

import android.os.Bundle;
import android.util.Log;

public class COInvestDetailsFragment extends InvestDetailsFragment {

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static COInvestDetailsFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new COInvestDetailsFragment.");
		COInvestDetailsFragment fragment = new COInvestDetailsFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	protected void setUpInvestmentIcon() {
		investmentIcon.setImageDrawable(DrawableUtils.getDrawable(
				COInvestmentUtil.getInvestmentIcon(investments.get(focusedInvestmentIndex),
						currentLevel)));
	}

}
