package com.copyop.android.app.popup;

import java.util.List;

import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.widget.form.FormRadioButton;
import com.anyoption.android.app.widget.form.FormRadioGroup;
import com.anyoption.common.beans.base.Language;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COConstants;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.RadioGroup.LayoutParams;
import android.widget.RadioGroup.OnCheckedChangeListener;

/**
 * Custom Language Selector Dialog.
 * @author Anastas Arnaudov
 *
 */
public class LanguageSelectorDialogFragment extends BaseDialogFragment implements OnCheckedChangeListener {
	
	public static LanguageSelectorDialogFragment newInstance(Bundle bundle) {
		Log.d(LanguageSelectorDialogFragment.class.getSimpleName(), "Created new LanguageSelectorDialogFragment.");
		LanguageSelectorDialogFragment fragment = new LanguageSelectorDialogFragment();
		fragment.setArguments(bundle);
		return fragment;
	}
	
	public interface OnLanguageSelectedListener{
		public void onLanguageSelected(Language language);
	}
	
	public final String TAG = LanguageSelectorDialogFragment.class.getSimpleName();
	private OnLanguageSelectedListener onLanguageSelectedListener;
	
	private FormRadioGroup radioGroup;
	private List<Language> languagesList;
	private Language language;
	
	@Override
	protected int getContentLayout() {
		return R.layout.language_selector_dialog;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = getArguments();
		if(bundle != null){
			//Parse arguments:
			Object lang = bundle.getSerializable(COConstants.EXTRA_LANGUAGES_LIST);
			if(lang != null && lang instanceof List<?>){
				languagesList = (List<Language>) lang;
			}
			if(bundle.getSerializable(COConstants.EXTRA_LANGUAGE) instanceof Language){
				language = (Language) bundle.getSerializable(COConstants.EXTRA_LANGUAGE);
			}
		}
	}
	
	@Override
	protected void onCreateContentView(View contentView) {
		radioGroup = (FormRadioGroup) contentView.findViewById(R.id.language_dialog_radio_group);
		for(int i=0; i < languagesList.size(); i++){
			Language language = languagesList.get(i);
			FormRadioButton radioButton = new FormRadioButton(application);
			radioButton.setTextColor(ColorUtils.getColor(R.color.language_dialog_text));
			radioButton.setFont(Font.ROBOTO_REGULAR);
			radioButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.language_dialog_item_text));
			radioButton.setLines(1);
			LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, (int) getResources().getDimension(R.dimen.language_dialog_item_height));
			radioButton.setText(language.getDisplayName().toUpperCase(application.getLocale()));
			radioButton.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
			radioButton.setLayoutParams(params);
			radioButton.setPadding((int)ScreenUtils.convertDpToPx(application, 100), 0, 0, 0);
			radioButton.setCompoundDrawablesWithIntrinsicBounds(DrawableUtils.getDrawable(R.drawable.radio_button_big), null, null, null);
			radioButton.setCompoundDrawablePadding((int)getResources().getDimension(R.dimen.language_dialog_item_text_drawable_padding));
			radioGroup.addView(radioButton, i, params);
		}
		setupInitChecked();
		radioGroup.setOnCheckedChangeListener(this);
	}

	public OnLanguageSelectedListener getOnLanguageSelectedListener() {
		return onLanguageSelectedListener;
	}

	public void setOnLanguageSelectedListener(OnLanguageSelectedListener onLanguageSelectedListener) {
		this.onLanguageSelectedListener = onLanguageSelectedListener;
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		FormRadioButton radioButton = getRadioButtonForId(checkedId);
		if(onLanguageSelectedListener != null){
			final Language language = getLanguageForDisplayName(radioButton.getValue());
			disableRadioGroup();
			new Handler().postDelayed(new Runnable() {
			  @Override
			  public void run() {
				  onLanguageSelectedListener.onLanguageSelected(language);
			  }
			}, 300);
		}
		
	}
	
	private void setupInitChecked(){
		for(int i = 0; i < radioGroup.getChildCount(); i++){
			if(((FormRadioButton)radioGroup.getChildAt(i)).getValue().equalsIgnoreCase(language.getDisplayName())){
				radioGroup.check(radioGroup.getChildAt(i).getId());
			}
		}
	}
	
	private void disableRadioGroup(){
		for(int i = 0; i < radioGroup.getChildCount(); i++){
		    ((FormRadioButton)radioGroup.getChildAt(i)).setEnabled(false);
		}
	}
	
	public Language getLanguageForDisplayName(String displayName){
		Language result = null;
		if(displayName != null){
			for(Language language : languagesList){
				if(language.getDisplayName().equalsIgnoreCase(displayName)){
					result = language;
					break;
				}
			}			
		}
		return result;
	}

	private FormRadioButton getRadioButtonForId(int id){
		for(int i=0; i < radioGroup.getChildCount(); i++){
			FormRadioButton radioButton = (FormRadioButton) radioGroup.getChildAt(i);
			if(radioButton.getId() == id){
				return radioButton;
			}
		}
		return null;
	}

}
