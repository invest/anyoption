package com.copyop.android.app.widget;

import com.copyop.android.app.R;

import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class ChooseCountView extends FrameLayout implements OnClickListener, OnTouchListener {

	private static final int COUNT_INTERVAL_MS = 150;
	private static final int DEFAULT_COUNT = 10;
	private static final int MIN_VALUE = 1;
	private static final int MAX_VALUE = 99;

	private View rootView;
	private ImageView plusView;
	private ImageView minusView;
	private TextView countView;

	private ValueAnimator countAnimation;
	private int count;
	private int animationStartCount;

	private int defaultCount;
	private int defaultMinValue;
	private int defaultMaxValue;

	public ChooseCountView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public ChooseCountView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public ChooseCountView(Context context) {
		this(context, null);
	}

	private void init(Context context) {
		defaultCount = DEFAULT_COUNT;
		defaultMinValue = MIN_VALUE;
		defaultMaxValue = MAX_VALUE;


		rootView = View.inflate(context, R.layout.choose_count_view, this);

		if (isInEditMode()) {
			return;
		}

		plusView = (ImageView) rootView.findViewById(R.id.choose_count_plus);
		minusView = (ImageView) rootView.findViewById(R.id.choose_count_minus);
		countView = (TextView) rootView.findViewById(R.id.choose_count_count);

		count = defaultCount;
		updateCountView();

		setUpAnimation();
		setUpButtonListeners();
	}

	public void setSettings(int defaultCount, int defaultMinValue, int defaultMaxValue) {
		if(defaultCount >= 1) {
			this.defaultCount = defaultCount;
		}
		if(defaultMinValue >= 1) {
			this.defaultMinValue = defaultMinValue;
		}
		if(defaultMaxValue >= 2) {
			this.defaultMaxValue = defaultMaxValue;
		}
		count = defaultCount;
		updateCountView();
	}

	private void setUpAnimation() {
		countAnimation = ValueAnimator.ofInt();

		countAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

			@Override
			public void onAnimationUpdate(ValueAnimator valueAnimator) {
				count = (Integer) valueAnimator.getAnimatedValue();
				updateCountView();
			}
		});
	}

	private void setUpButtonListeners() {
		plusView.setOnTouchListener(this);
		minusView.setOnTouchListener(this);

		plusView.setOnClickListener(this);
		minusView.setOnClickListener(this);
	}

	private void startCounting(boolean isPlus) {
		stopCounting();

		if ((isPlus && count >= defaultMaxValue) || (!isPlus && count <= defaultMinValue)) {
			return;
		}

		animationStartCount = count;
		long duration = 0;

		if (isPlus) {
			countAnimation.setIntValues(count, defaultMaxValue);
			duration = (defaultMaxValue - count) * COUNT_INTERVAL_MS;
		} else {
			countAnimation.setIntValues(count, defaultMinValue);
			duration = (count - defaultMinValue) * COUNT_INTERVAL_MS;
		}

		countAnimation.setDuration(duration);
		countAnimation.start();
	}

	public void stopCounting() {
		if (countAnimation != null) {
			countAnimation.cancel();
		}
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
		updateCountView();
	}

	private void updateCountView() {
		countView.setText(String.valueOf(count));
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		plusView.setClickable(enabled);
		minusView.setClickable(enabled);

		if (enabled) {
			setAlpha(1f);
			plusView.setOnTouchListener(this);
			minusView.setOnTouchListener(this);
		} else {
			setAlpha(0.1f);
			plusView.setOnTouchListener(null);
			minusView.setOnTouchListener(null);
		}

	}

	@Override
	public void onClick(View v) {
		if (v.getId() == plusView.getId()) {
			if (count < defaultMaxValue) {
				count++;
				updateCountView();
			}
		} else {
			if (count > defaultMinValue) {
				count--;
				updateCountView();
			}
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			boolean isPlus = v.getId() == plusView.getId();
			startCounting(isPlus);
		} else if (event.getAction() == MotionEvent.ACTION_UP) {
			stopCounting();

			if (animationStartCount != count) {
				// Does not have to call click listener.
				return true;
			}
		}

		return false;
	}

}
