package com.copyop.android.app.fragment;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.ExpiredOptionsEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.event.ReverseWithdrawEvent;
import com.anyoption.android.app.event.UserUpdatedEvent;
import com.anyoption.android.app.event.WithdrawEvent;
import com.anyoption.android.app.fragment.StatusFragment;
import com.anyoption.android.app.widget.Clock;
import com.copyop.android.app.R;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * A simple {@link android.app.Fragment} subclass.
 * <p>It represents the status component of each screen.
 * 
 */
public class COStatusFragment extends StatusFragment{
	
	/**
	 * Use this method to obtain new instance of the fragment.
	 * @return
	 */
	public static COStatusFragment newInstance(){
		return COStatusFragment.newInstance(new Bundle());
	}
	
	/**
	 * Use this method to obtain new instance of the fragment.
	 * @return
	 */
	public static COStatusFragment newInstance(Bundle bundle){
		COStatusFragment coStatusfragment = new COStatusFragment();
		coStatusfragment.setArguments(bundle);
		return coStatusfragment;
	}

	protected ImageView dotOpenOptions;
	protected TextView openOptionsTextView;
	protected TextView openOptionsLabelTextView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.status_fragment_layout, container, false);

		//OPEN OPTIONS:
		dotOpenOptions = (ImageView) rootView.findViewById(R.id.status_open_options_dot_image_view);
		openOptionsTextView = (TextView) rootView.findViewById(R.id.status_open_options_text_view);
		openOptionsLabelTextView = (TextView) rootView.findViewById(R.id.status_open_options_label_text_view);
		openOptionsLabelTextView.setText(" " + getString(R.string.openOptions));
		
		dotOpenOptions.setOnClickListener(this);
		openOptionsTextView.setOnClickListener(this);
		openOptionsLabelTextView.setOnClickListener(this);
		//////////////
		
		//CLOCK:
		clock = (Clock) rootView.findViewById(R.id.status_time_digital_clock);
		///////////
		
		//BALANCE:
		balanceTextView = (TextView) rootView.findViewById(R.id.status_balance_text_view);
		balanceLabelTextView = (TextView) rootView.findViewById(R.id.status_balance_label_text_view);
		balanceLabelTextView.setText(getString(R.string.balance) + ": ");
		//////////////
		
		balanceView = rootView.findViewById(R.id.status_right_layout);
		balanceInfoImageView = rootView.findViewById(R.id.status_balance_info_view);
		///////////////////
		
		refresh();

		return rootView;
	}
	
	@Override
	public void onResume() {
		application.registerForEvents(this, UserUpdatedEvent.class, LoginLogoutEvent.class, ExpiredOptionsEvent.class, WithdrawEvent.class, ReverseWithdrawEvent.class);
		super.onResume();
	}
	
	@Override
	public void onPause() {
		application.unregisterForAllEvents(this);
		super.onPause();
	}

	//###########################################################################
	//##################			EVENT CALLBACKs			#####################
	//###########################################################################

	public void onEventMainThread(ExpiredOptionsEvent event){
		user = application.getUser();
		
		refresh();
	}

	//############################################################################
	
	/**
	 * Updates the UI.
	 */
	protected void refresh() {
		super.refresh();
		refreshOpenOptions();
	}

	/**
	 * Updates the Open Options.
	 */
	protected void refreshOpenOptions() {
		if (user != null && user.getOpenedInvestmentsCount() > 0) {
			dotOpenOptions.setVisibility(View.VISIBLE);
			openOptionsTextView.setVisibility(View.VISIBLE);
			openOptionsLabelTextView.setVisibility(View.VISIBLE);
			openOptionsTextView.setText(String.valueOf(user.getOpenedInvestmentsCount()));
		} else {
			dotOpenOptions.setVisibility(View.GONE);
			openOptionsTextView.setVisibility(View.GONE);
			openOptionsLabelTextView.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		if(v == dotOpenOptions || v == openOptionsTextView || v == openOptionsLabelTextView){
			application.postEvent(new NavigationEvent(Screen.MY_OPTIONS, NavigationType.DEEP));
		}
	}
	
}