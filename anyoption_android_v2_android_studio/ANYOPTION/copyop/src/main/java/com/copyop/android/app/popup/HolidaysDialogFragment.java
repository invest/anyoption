package com.copyop.android.app.popup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.anyoption.android.app.popup.OkDialogFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.copyop.android.app.R;

/**
 * @author Anastas Arnaudov
 */
public class HolidaysDialogFragment  extends OkDialogFragment {

    public static final String TAG = HolidaysDialogFragment.class.getSimpleName();
    private String message;

    public static HolidaysDialogFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new HolidaysDialogFragment.");
        HolidaysDialogFragment fragment = new HolidaysDialogFragment();
        if (bundle == null) {
            bundle = new Bundle();
        }
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if(bundle != null){
            message = bundle.getString(Constants.EXTRA_MESSAGE);
        }
    }

    @Override
    protected int getContentLayout() {
        return R.layout.popup_holidays;
    }

    @Override
    protected void onCreateContentView(View contentView) {
        TextView messageTextView = (TextView) contentView.findViewById(R.id.messageTextView);
        messageTextView.setText(message);

        View okButton = contentView.findViewById(com.anyoption.android.R.id.ok_button);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

}
