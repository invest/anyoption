package com.copyop.android.app.fragment.walkthrough;

import com.anyoption.android.app.popup.BaseDialogFragment;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COConstants;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class ProfileWalkthroughDialog extends BaseDialogFragment implements OnClickListener {

	private static final String TAG = ProfileWalkthroughDialog.class.getSimpleName();

	private View gotIt;

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static ProfileWalkthroughDialog newInstance(Bundle bundle) {
		Log.d(TAG, "Created new HotWalkthroughDialog.");
		ProfileWalkthroughDialog fragment = new ProfileWalkthroughDialog();
		bundle.putInt(ARG_BACKGROUND_COLOR_KEY, 0);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		fragment.setArguments(bundle);

		return fragment;
	}

	@Override
	protected void onCreateContentView(View contentView) {
		super.onCreateContentView(contentView);

		gotIt = contentView.findViewById(R.id.profile_walkthrough_got_it);
		gotIt.setOnClickListener(this);
	}

	@Override
	protected int getContentLayout() {
		return R.layout.profile_walkthrough_dialog;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == gotIt.getId()) {
			finishWalkthrough();
		}
	}

	private void finishWalkthrough() {
		application.getSharedPreferencesManager().putBoolean(
				COConstants.PREFERENCES_IS_PROFILE_WALKTHROUGH_SHOWN, true);

		dismiss();
	}

}
