package com.copyop.android.app.fragment.news_feed;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.TextView;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.model.HotAssetSpecialist;
import com.copyop.android.app.model.HotCopier;
import com.copyop.android.app.model.HotGroup;
import com.copyop.android.app.model.HotGroup.Type;
import com.copyop.android.app.model.HotTrade;
import com.copyop.android.app.model.HotTrader;
import com.copyop.android.app.popup.HotDropdownPopup;
import com.copyop.android.app.util.COConstants;
import com.copyop.android.app.util.ConvertUtils;
import com.copyop.android.app.util.adapter.HotAdapter;
import com.copyop.android.app.util.adapter.HotAdapter.HotHoursFilterListener;
import com.copyop.android.app.util.adapter.HotAllAdapter;
import com.copyop.android.app.util.adapter.HotAllAdapter.HotItemComparator;
import com.copyop.android.app.util.adapter.HotAllAdapter.HotItemComparator.SortField;
import com.copyop.android.app.util.adapter.HotAllAdapter.HotItemComparator.SortOrder;
import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.enums.base.UpdateTypeEnum;
import com.copyop.json.requests.CopyopHotDataMethodRequest;
import com.copyop.json.results.CoyopHotDataMethodResult;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

/**
 * Displays all "Hot" items from a given category ("Best Traders", "Best Trades", "Best Copiers")
 * @author Anastas Arnaudov
 *
 */
public class HotAllFragment extends NavigationalFragment implements OnClickListener, HotHoursFilterListener{

	public static HotAllFragment newInstance(Bundle bundle){
		HotAllFragment fragment = new HotAllFragment();
		fragment.setArguments(bundle);
		return fragment;
	}
	
	public final String TAG = HotAllFragment.class.getSimpleName();
	
	private View rootView;
	private HotGroup group;
	private ListView listView;
	private HotAllAdapter adapter;
	private TextView groupTextView;
	private View groupDropdownImageLayout;
	private ImageView groupDropdownImageView;
	private ImageView groupDropdownNewImageView;
	private boolean isLoading;
	private SpannableStringBuilder groupSpannable;
	private View loadingView;

	private AsyncTask<List<FeedMessage>, Void, List<HotTrader>> parseHotTradersTask;
	private AsyncTask<List<FeedMessage>, Void, List<HotTrade>> parseHotTradesTask;
	private AsyncTask<List<FeedMessage>, Void, List<HotCopier>> parseHotCopiersTask;
	private AsyncTask<List<FeedMessage>, Void, List<HotAssetSpecialist>> parseHotAssetSpecialistTask;

	@Override
	protected Screenable setupScreen() {
		return COScreen.HOT_ALL;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = getArguments();
		if(bundle != null){
			Object g = bundle.getSerializable(COConstants.EXTRA_HOT_GROUP);
			if(g != null && g instanceof HotGroup){
				group = (HotGroup) g;
				adapter = new HotAllAdapter(group);
			}
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.hot_all_layout, container, false);
		loadingView = rootView.findViewById(R.id.hot_all_progress_bar);
		//Set up the group name:
		groupTextView = (TextView) rootView.findViewById(R.id.hot_all_group_name_text_view);
		groupDropdownImageLayout = rootView.findViewById(R.id.hot_all_group_dropdown_image_layout);
		groupDropdownImageView = (ImageView) rootView.findViewById(R.id.hot_all_group_dropdown_image_view);
		groupDropdownNewImageView = (ImageView) rootView.findViewById(R.id.hot_all_group_dropdown_new_image_view);
		groupTextView.setOnClickListener(this);
		groupDropdownImageLayout.setOnClickListener(this);
		groupDropdownImageView.setOnClickListener(this);

		groupSpannable = new SpannableStringBuilder();
		try {
			HotAdapter.setupGroupName(application, group, groupTextView, groupSpannable, null);
		} catch (Exception e) {
			Log.e(TAG, "onCreateView -> Could NOT set Group Name", e);
		}
		/////////////////////
		
		setupSortButtons();
		
		setupHeader();
		
		listView = (ListView) rootView.findViewById(R.id.hot_all_list_view);
		listView.setAdapter(adapter);
		
		setupLoading();
		
		return rootView;	
		
	}

	private void setupSortButtons() {
		View sortLayout = rootView.findViewById(R.id.hot_all_sort_layout);

		if (group.getType() == Type.COPIERS) {
			// For Best copiers items are always sorted by profit.
			sortLayout.setVisibility(View.GONE);
			return;
		}

		sortLayout.setVisibility(View.VISIBLE);

		final ImageView sortProfitButton = (ImageView) rootView.findViewById(R.id.hot_all_sort_button_profit);
		final ImageView sortHitOrMarketButton = (ImageView) rootView.findViewById(R.id.hot_all_sort_button_hit_or_market);
		final ImageView sortCopiersOrTimeButton = (ImageView) rootView.findViewById(R.id.hot_all_sort_button_copiers_or_time);

		if(group.getType() == Type.ASSET_SPECIALISTS) {
			sortCopiersOrTimeButton.setVisibility(View.INVISIBLE);
		}


		final Drawable sortOnDrawable = DrawableUtils.getDrawable(R.drawable.hot_all_sort_active_icon);
		final Drawable sortOffDrawable = DrawableUtils.getDrawable(R.drawable.hot_all_sort_inactive_icon);
		
		OnClickListener sortClickListener = new OnClickListener() {
			@Override
			public void onClick(View view) {
				SortField field = null;
				SortOrder order = SortOrder.BIG_TO_SMALL;
				if(view == sortProfitButton){
					sortProfitButton.setImageDrawable(sortOnDrawable);
					sortHitOrMarketButton.setImageDrawable(sortOffDrawable);
					sortCopiersOrTimeButton.setImageDrawable(sortOffDrawable);
					
					field = SortField.PROFIT;
				}else if(view == sortHitOrMarketButton){
					sortProfitButton.setImageDrawable(sortOffDrawable);
					sortHitOrMarketButton.setImageDrawable(sortOnDrawable);
					sortCopiersOrTimeButton.setImageDrawable(sortOffDrawable);
					
					if(group.getType() == Type.TRADES || group.getType() == Type.ASSET_SPECIALISTS){
						field = SortField.MARKET;
					}else{
						field = SortField.HIT;
					}
				}else if(view == sortCopiersOrTimeButton){
					sortProfitButton.setImageDrawable(sortOffDrawable);
					sortHitOrMarketButton.setImageDrawable(sortOffDrawable);
					sortCopiersOrTimeButton.setImageDrawable(sortOnDrawable);
					
					if(group.getType() == Type.TRADES){
						field = SortField.TIME;
					}else{
						field = SortField.COPIERS;
					}
				}
				
				sortList(field, order);
			}
		};
		
		sortProfitButton.setOnClickListener(sortClickListener);
		sortHitOrMarketButton.setOnClickListener(sortClickListener);
		sortCopiersOrTimeButton.setOnClickListener(sortClickListener);
	}
	
	/**
	 * Set up the header (columns titles).
	 */
	private void setupHeader() {
		TextView headerProfitTextView = (TextView) rootView.findViewById(R.id.hot_all_header_profit);
		headerProfitTextView.setText(R.string.hotAllHeaderProtif);

		TextView headerHitOrMarketTextView = (TextView) rootView.findViewById(R.id.hot_all_header_hit_or_market);
		TextView headerCopiersOrTimeTextView = (TextView) rootView.findViewById(R.id.hot_all_header_copiers_or_time);

		if (group.getType() == Type.TRADERS) {
			headerHitOrMarketTextView.setText(R.string.hotAllHeaderHit);
			headerCopiersOrTimeTextView.setText(R.string.hotAllHeaderCopiers);
			groupDropdownNewImageView.setVisibility(View.GONE);
		} else if (group.getType() == Type.TRADES) {
			headerHitOrMarketTextView.setText(R.string.hotAllHeaderMarket);
			headerCopiersOrTimeTextView.setText(R.string.hotAllHeaderTime);
			groupDropdownNewImageView.setVisibility(View.GONE);
		} else if (group.getType() == Type.COPIERS) {
			headerHitOrMarketTextView.setText("");
			headerCopiersOrTimeTextView.setText("");
			groupDropdownNewImageView.setVisibility(View.GONE);
		} else if (group.getType() == Type.ASSET_SPECIALISTS) {
			headerHitOrMarketTextView.setText(R.string.hotAllHeaderMarket);
			headerCopiersOrTimeTextView.setText(R.string.asset_specialists_copy_text);
			groupDropdownNewImageView.setVisibility(View.VISIBLE);
		}
	}
	
	protected void setupLoading(){
		if(loadingView != null){
			if(isLoading){
				loadingView.setVisibility(View.VISIBLE);
			}else{
				loadingView.setVisibility(View.GONE);
			}
		}
	}
	
	private void sortList(SortField field, SortOrder order){
		Collections.sort(group.getItems(), new HotItemComparator(field, order));
		adapter.notifyDataSetChanged();
	}

	@Override
	public void onClick(View view) {
		if(view.getId() == groupTextView.getId() || view.getId() == groupDropdownImageView.getId() || view.getId() == groupDropdownImageLayout.getId()){
			if(group.getFilters() != null && group.getFilters().size() > 1){
				HotDropdownPopup.showPopup(group, groupDropdownImageView, this);
			}
		}
	}

	@Override
	public void onFilter(UpdateTypeEnum updateTypeEnum) {
		if(updateTypeEnum == UpdateTypeEnum.BEST_TRADERS || updateTypeEnum == UpdateTypeEnum.BEST_TRADERS_ONE_WEEK || updateTypeEnum == UpdateTypeEnum.BEST_TRADERS_ONE_MOTNH || updateTypeEnum == UpdateTypeEnum.BEST_TRADERS_TRHEE_MOTNH){
			getHotTraders(updateTypeEnum);
		}else if(updateTypeEnum == UpdateTypeEnum.BEST_TRADES || updateTypeEnum == UpdateTypeEnum.BEST_TRADES_ONE_WEEK || updateTypeEnum == UpdateTypeEnum.BEST_TRADES_ONE_MOTNH || updateTypeEnum == UpdateTypeEnum.BEST_TRADES_TRHEE_MOTNH){
			getHotTrades(updateTypeEnum);
		}else if(updateTypeEnum == UpdateTypeEnum.BEST_COPIERS || updateTypeEnum == UpdateTypeEnum.BEST_COPIERS_ONE_WEEK || updateTypeEnum == UpdateTypeEnum.BEST_COPIERS_ONE_MOTNH || updateTypeEnum == UpdateTypeEnum.BEST_COPIERS_TRHEE_MOTNH){
			getHotCopiers(updateTypeEnum);
		}else if(updateTypeEnum == UpdateTypeEnum.BEST_ASSET_SPECIALISTS || updateTypeEnum == UpdateTypeEnum.BEST_ASSET_SPECIALISTS_ONE_WEEK || updateTypeEnum == UpdateTypeEnum.BEST_ASSET_SPECIALISTS_ONE_MOTNH || updateTypeEnum == UpdateTypeEnum.BEST_ASSET_SPECIALISTS_TRHEE_MOTNH){
			getHotAssetSpecialists(updateTypeEnum);
		}
	}
	
	@Override
	public void onHiddenChanged(boolean hidden) {
		if (hidden) {
			cancelAllTasks();
		}
		
		super.onHiddenChanged(hidden);
	}

	private void cancelAllTasks() {
		application.getCommunicationManager().cancelAllRequests(this);

		if (parseHotTradersTask != null && !parseHotTradersTask.isCancelled()) {
			parseHotTradersTask.cancel(true);
		}

		if (parseHotTradesTask != null && !parseHotTradesTask.isCancelled()) {
			parseHotTradesTask.cancel(true);
		}

		if (parseHotCopiersTask != null && !parseHotCopiersTask.isCancelled()) {
			parseHotCopiersTask.cancel(true);
		}

		if (parseHotAssetSpecialistTask != null && !parseHotAssetSpecialistTask.isCancelled()) {
			parseHotAssetSpecialistTask.cancel(true);
		}
	}

	//#####################################################################
	//#########					REQUEST - RESPONSE				###########
	//#####################################################################	

	protected void getHotTraders(UpdateTypeEnum updateTypeEnum){
		isLoading = true;
		setupLoading();
		CopyopHotDataMethodRequest request = new CopyopHotDataMethodRequest();
		request.setUpdateTypeEnumId(updateTypeEnum.getId());
		if(updateTypeEnum == UpdateTypeEnum.BEST_TRADERS){
			request.setInitCall(true);
		}
		if(!application.getCommunicationManager().requestService(this, "gotHotTraders", COConstants.SERVICE_GET_COPYOP_HOT_TRADERS, request, CoyopHotDataMethodResult.class, false, false)){
			isLoading = false;
			setupLoading();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void gotHotTraders(Object result){
		if (isHidden()) {
			return;
		}

		if(result != null && result instanceof CoyopHotDataMethodResult && ((CoyopHotDataMethodResult) result).getErrorCode() == Constants.ERROR_CODE_SUCCESS){
			final CoyopHotDataMethodResult coyopHotDataMethodResult = (CoyopHotDataMethodResult) result;
			parseHotTradersTask = new AsyncTask<List<FeedMessage>, Void, List<HotTrader>>() {
				@Override
				protected List<HotTrader> doInBackground(List<FeedMessage>... params) {
					//Set up the filters:
					List<UpdateTypeEnum> filters = new ArrayList<UpdateTypeEnum>();
					for(Integer filter : coyopHotDataMethodResult.getExistUpdateTypeEnumId()){
						filters.add(UpdateTypeEnum.getById(filter));
					}
					group.setFilters(filters);
					////////////////
					
					//Set up the items:
					List<FeedMessage> feedMessagesList = params[0];
					List<HotTrader> result = new ArrayList<HotTrader>();
					if(feedMessagesList != null){
						for(FeedMessage feedMessage : feedMessagesList){
							HotTrader hotTrader = ConvertUtils.convertFeedMessageToHotTrader(feedMessage);
							result.add(hotTrader);
						}
					}else{
						cancel(true);
					}
					//////////////////
					return result;
				}
				@Override
				protected void onCancelled() {
					isLoading = false;
					setupLoading();
				}
				@Override
				protected void onPostExecute(List<HotTrader> result) {
					setupTradersGroup(result);
					isLoading = false;
					setupLoading();
				}
			}.execute(coyopHotDataMethodResult.getFeed());
		}else{
			Log.e(TAG, COConstants.SERVICE_GET_COPYOP_HOT_TRADERS + " callback -> COULD NOT parse the result");
			isLoading = false;
			setupLoading();
		}
	}
	
	protected void getHotTrades(UpdateTypeEnum updateTypeEnum){
		isLoading = true;
		setupLoading();
		CopyopHotDataMethodRequest request = new CopyopHotDataMethodRequest();
		request.setUpdateTypeEnumId(updateTypeEnum.getId());
		if(updateTypeEnum == UpdateTypeEnum.BEST_TRADES){
			request.setInitCall(true);
		}
		if(!application.getCommunicationManager().requestService(this, "gotHotTrades", COConstants.SERVICE_GET_COPYOP_HOT_TRADES, request, CoyopHotDataMethodResult.class, false, false)){
			isLoading = false;
			setupLoading();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void gotHotTrades(Object result){
		if (isHidden()) {
			return;
		}
		
		if(result != null && result instanceof CoyopHotDataMethodResult && ((CoyopHotDataMethodResult) result).getErrorCode() == Constants.ERROR_CODE_SUCCESS){
			final CoyopHotDataMethodResult coyopHotDataMethodResult = (CoyopHotDataMethodResult) result;
			parseHotTradesTask = new AsyncTask<List<FeedMessage>, Void, List<HotTrade>>() {
				@Override
				protected List<HotTrade> doInBackground(List<FeedMessage>... params) {
					//Set up the filters:
					List<UpdateTypeEnum> filters = new ArrayList<UpdateTypeEnum>();
					for(Integer filter : coyopHotDataMethodResult.getExistUpdateTypeEnumId()){
						filters.add(UpdateTypeEnum.getById(filter));
					}
					group.setFilters(filters);
					////////////////
					
					//Set up the items:
					List<FeedMessage> feedMessagesList = params[0];
					List<HotTrade> result = new ArrayList<HotTrade>();
					if(feedMessagesList != null){
						for(FeedMessage feedMessage : feedMessagesList){
							HotTrade hotTrade = ConvertUtils.convertFeedMessageToHotTrade(feedMessage);
							result.add(hotTrade);
						}
					}else{
						cancel(true);
					}
					///////////////////
					return result;
				}
				@Override
				protected void onCancelled() {
					isLoading = false;
					setupLoading();
				}
				@Override
				protected void onPostExecute(List<HotTrade> result) {
					setupTradesGroup(result);
					isLoading = false;
					setupLoading();
				}
			}.execute(coyopHotDataMethodResult.getFeed());
		}else{
			Log.e(TAG, COConstants.SERVICE_GET_COPYOP_HOT_TRADES + " callback -> COULD NOT parse the result");
			isLoading = false;
			setupLoading();
		}
	}

	protected void getHotAssetSpecialists(UpdateTypeEnum updateTypeEnum){
		isLoading = true;
		setupLoading();
		CopyopHotDataMethodRequest request = new CopyopHotDataMethodRequest();
		request.setUpdateTypeEnumId(updateTypeEnum.getId());
		if(updateTypeEnum == UpdateTypeEnum.BEST_ASSET_SPECIALISTS){
			request.setInitCall(true);
		}
		if(!application.getCommunicationManager().requestService(this, "getHotAssetSpecialists", COConstants.SERVICE_GET_COPYOP_HOT_ASSET_SPECIALISTS, request, CoyopHotDataMethodResult.class, false, false)){
			isLoading = false;
			setupLoading();
		}
	}

	@SuppressWarnings("unchecked")
	public void getHotAssetSpecialists(Object result){
		if (isHidden()) {
			return;
		}

		if(result != null && result instanceof CoyopHotDataMethodResult && ((CoyopHotDataMethodResult) result).getErrorCode() == Constants.ERROR_CODE_SUCCESS){
			final CoyopHotDataMethodResult coyopHotDataMethodResult = (CoyopHotDataMethodResult) result;
			parseHotAssetSpecialistTask = new AsyncTask<List<FeedMessage>, Void, List<HotAssetSpecialist>>() {
				@Override
				protected List<HotAssetSpecialist> doInBackground(List<FeedMessage>... params) {
					//Set up the filters:
					List<UpdateTypeEnum> filters = new ArrayList<UpdateTypeEnum>();
					for(Integer filter : coyopHotDataMethodResult.getExistUpdateTypeEnumId()){
						filters.add(UpdateTypeEnum.getById(filter));
					}
					group.setFilters(filters);
					////////////////

					//Set up the items:
					List<FeedMessage> feedMessagesList = params[0];
					List<HotAssetSpecialist> result = new ArrayList<HotAssetSpecialist>();
					if(feedMessagesList != null){
						for(FeedMessage feedMessage : feedMessagesList){
							HotAssetSpecialist hotAssetSpecialist = ConvertUtils.convertFeedMessageToHotAssetSpecialist(feedMessage);
							result.add(hotAssetSpecialist);
						}
					}else{
						cancel(true);
					}
					///////////////////
					return result;
				}
				@Override
				protected void onCancelled() {
					isLoading = false;
					setupLoading();
				}
				@Override
				protected void onPostExecute(List<HotAssetSpecialist> result) {
					setupAssetSpecialistsGroup(result);
					isLoading = false;
					setupLoading();
				}
			}.execute(coyopHotDataMethodResult.getFeed());
		}else{
			Log.e(TAG, COConstants.SERVICE_GET_COPYOP_HOT_ASSET_SPECIALISTS + " callback -> COULD NOT parse the result");
			isLoading = false;
			setupLoading();
		}
	}
	
	protected void getHotCopiers(UpdateTypeEnum updateTypeEnum){
		isLoading = false;
		setupLoading();
		CopyopHotDataMethodRequest request = new CopyopHotDataMethodRequest();
		request.setUpdateTypeEnumId(updateTypeEnum.getId());
		if(updateTypeEnum == UpdateTypeEnum.BEST_COPIERS){
			request.setInitCall(true);
		}
		if(!application.getCommunicationManager().requestService(this, "gotHotCopiers", COConstants.SERVICE_GET_COPYOP_HOT_COPIERS, request, CoyopHotDataMethodResult.class, false, false)){
			isLoading = false;
			setupLoading();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void gotHotCopiers(Object result){
		if (isHidden()) {
			return;
		}
		
		if(result != null && result instanceof CoyopHotDataMethodResult && ((CoyopHotDataMethodResult) result).getErrorCode() == Constants.ERROR_CODE_SUCCESS){
			final CoyopHotDataMethodResult coyopHotDataMethodResult = (CoyopHotDataMethodResult) result;
			parseHotCopiersTask = new AsyncTask<List<FeedMessage>, Void, List<HotCopier>>() {
				@Override
				protected List<HotCopier> doInBackground(List<FeedMessage>... params) {
					//Set up the filters:
					List<UpdateTypeEnum> filters = new ArrayList<UpdateTypeEnum>();
					for(Integer filter : coyopHotDataMethodResult.getExistUpdateTypeEnumId()){
						filters.add(UpdateTypeEnum.getById(filter));
					}
					group.setFilters(filters);
					////////////////
					
					//Set up the items:
					List<FeedMessage> feedMessagesList = params[0];
					List<HotCopier> result = new ArrayList<HotCopier>();
					if(feedMessagesList != null){
						for(FeedMessage feedMessage : feedMessagesList){
							HotCopier hotCopier = ConvertUtils.convertFeedMessageToHotCopier(feedMessage);
							result.add(hotCopier);
						}
					}else{
						cancel(true);
					}
					///////////////////
					return result;
				}
				@Override
				protected void onCancelled() {
					isLoading = false;
					setupLoading();
				}
				@Override
				protected void onPostExecute(List<HotCopier> result) {
					setupCopiersGroup(result);
					isLoading = false;
					setupLoading();
				}
			}.execute(coyopHotDataMethodResult.getFeed());
		}else{
			Log.e(TAG, COConstants.SERVICE_GET_COPYOP_HOT_COPIERS + " callback -> COULD NOT parse the result");
			isLoading = false;
			setupLoading();
		}
	}
	
	//#####################################################################
	
	private void setupTradersGroup(List<HotTrader> tradersList){
		if(tradersList != null && !tradersList.isEmpty()){
			int assetLastTradingHours = tradersList.get(0).getLastHours(); 
			group.setUpdateType(tradersList.get(0).getUpdateType());
			if(tradersList.get(0).getUpdateType() == UpdateTypeEnum.BEST_TRADERS){
				//Set up INIT hours:
				group.setLastInitHours(assetLastTradingHours);
			}
			group.setItems(tradersList);
			group.setLastHours(assetLastTradingHours);
			group.setName(application.getString(R.string.hotGroupNameTraders, group.getLastHoursFormated()));
		}
		try {
			clearSpan();
			HotAdapter.setupGroupName(application, group, groupTextView, groupSpannable, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		adapter.notifyDataSetChanged();
	}

	private void setupTradesGroup(List<HotTrade> tradesList){
		if(tradesList != null && !tradesList.isEmpty()){
			int assetLastTradingHours = tradesList.get(0).getLastHours();
			group.setUpdateType(tradesList.get(0).getUpdateType());
			if(tradesList.get(0).getUpdateType() == UpdateTypeEnum.BEST_TRADES){
				//Set up INIT hours:
				group.setLastInitHours(assetLastTradingHours);
			}
			group.setItems(tradesList);
			group.setLastHours(assetLastTradingHours);
			group.setName(application.getString(R.string.hotGroupNameTrades, group.getLastHoursFormated()));
		}
		try {
			clearSpan();
			HotAdapter.setupGroupName(application, group, groupTextView, groupSpannable, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		adapter.notifyDataSetChanged();
	}

	private void setupAssetSpecialistsGroup(List<HotAssetSpecialist> assetSpecialistsList){
		if(assetSpecialistsList != null && !assetSpecialistsList.isEmpty()){
			int assetLastTradingHours = assetSpecialistsList.get(0).getLastHours();
			group.setUpdateType(assetSpecialistsList.get(0).getUpdateType());
			if(assetSpecialistsList.get(0).getUpdateType() == UpdateTypeEnum.BEST_ASSET_SPECIALISTS){
				//Set up INIT hours:
				group.setLastInitHours(assetLastTradingHours);
			}
			group.setItems(assetSpecialistsList);
			group.setLastHours(assetLastTradingHours);
			group.setName(application.getString(R.string.hotGroupNameAssetSpecialists, group.getLastHoursFormated()));
		}
		try {
			clearSpan();
			HotAdapter.setupGroupName(application, group, groupTextView, groupSpannable, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		adapter.notifyDataSetChanged();
	}
	
	private void setupCopiersGroup(List<HotCopier> copiersList){
		if(copiersList != null && !copiersList.isEmpty()){
			int assetLastTradingHours = copiersList.get(0).getLastHours();
			group.setUpdateType(copiersList.get(0).getUpdateType());
			if(copiersList.get(0).getUpdateType() == UpdateTypeEnum.BEST_COPIERS){
				//Set up INIT hours:
				group.setLastInitHours(assetLastTradingHours);
			}
			group.setItems(copiersList);
			group.setLastHours(assetLastTradingHours);
			group.setName(application.getString(R.string.hotGroupNameCopiers, group.getLastHoursFormated()));
		}
		try {
			clearSpan();
			HotAdapter.setupGroupName(application, group, groupTextView, groupSpannable, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		adapter.notifyDataSetChanged();
	}

	private void clearSpan(){
		this.groupSpannable.clear();
		this.groupSpannable.clearSpans();
	}
}
