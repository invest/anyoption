package com.copyop.android.app.fragment.walkthrough;

import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.widget.TextView;
import com.copyop.android.app.R;
import com.copyop.android.app.model.HotGroup;
import com.copyop.android.app.model.HotGroup.Type;
import com.copyop.android.app.util.COConstants;
import com.copyop.android.app.util.adapter.HotAdapter;
import com.copyop.common.enums.base.UpdateTypeEnum;

import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ScrollView;

public class HotWalkthroughDialog extends BaseDialogFragment implements OnClickListener {

	private static final String TAG = HotWalkthroughDialog.class.getSimpleName();

	private static final int LAST_HOURS = 12;

	private View rootView;

	private View tradersContent;
	private View tradesContent;
	private View copiersContent;
	private View assetSpecialistsContent;

	private View tradersGotIt;
	private View tradesGotIt;
	private View copiersGotIt;
	private View assetSpecialistsGotIt;
	private ScrollView scrollView;

	private SpannableStringBuilder groupSpannable;

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static HotWalkthroughDialog newInstance(Bundle bundle) {
		Log.d(TAG, "Created new HotWalkthroughDialog.");
		HotWalkthroughDialog fragment = new HotWalkthroughDialog();
		bundle.putInt(ARG_BACKGROUND_COLOR_KEY, 0);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		fragment.setArguments(bundle);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		groupSpannable = new SpannableStringBuilder();
	}

	@Override
	protected void onCreateContentView(View contentView) {
		super.onCreateContentView(contentView);
		rootView = contentView;

		scrollView = (ScrollView) contentView.findViewById(R.id.hot_walkthrough_scrollview);

		TextView titleView = (TextView) contentView.findViewById(R.id.hot_walkthrough_title);
		TextUtils.underlineText(titleView);

		tradersContent = contentView.findViewById(R.id.hot_walkthrough_traders_content);
		tradersGotIt = contentView.findViewById(R.id.hot_walkthrough_traders_got_it);
		tradersGotIt.setOnClickListener(this);

		tradesContent = contentView.findViewById(R.id.hot_walkthrough_trades_content);
		tradesGotIt = contentView.findViewById(R.id.hot_walkthrough_trades_got_it);
		tradesGotIt.setOnClickListener(this);

		copiersContent = contentView.findViewById(R.id.hot_walkthrough_copiers_content);
		copiersGotIt = contentView.findViewById(R.id.hot_walkthrough_copiers_got_it);
		copiersGotIt.setOnClickListener(this);

		assetSpecialistsContent = contentView.findViewById(R.id.hot_walkthrough_asset_specialists_content);
		assetSpecialistsGotIt = contentView.findViewById(R.id.hot_walkthrough_asset_specialists_got_it);
		TextView assetSpecialistsContentText = (TextView) contentView.findViewById(R.id.hot_walkthrough_asset_specialists_content_text);
		TextUtils.insertImages(assetSpecialistsContentText, null, true, R.drawable.circle_plus_white);
		assetSpecialistsGotIt.setOnClickListener(this);

		try {
			setUpGroups();
		} catch (Exception e) {
			Log.e(TAG, "Could not set up hot groups!", e);
		}
	}

	@Override
	protected int getContentLayout() {
		return R.layout.hot_walkthrough_dialog;
	}

	private void setUpGroups() throws Exception {
		View tradersGroupView = rootView.findViewById(R.id.hot_walkthrough_traders_group);
		TextView tradersGroupName = (TextView) tradersGroupView
				.findViewById(R.id.hot_group_name_text_view);
		TextView tradersGroupSeeAll = (TextView) tradersGroupView
				.findViewById(R.id.hot_group_see_all_text_view);
		HotGroup tradersGroup = new HotGroup();
		tradersGroup.setType(Type.TRADERS);
		tradersGroup.setUpdateType(UpdateTypeEnum.BEST_TRADERS);
		tradersGroup.setLastHours(LAST_HOURS);
		setupGroup(tradersGroup, tradersGroupName, tradersGroupSeeAll);

		//ASSET SPECIALISTS
		View assetSpecialistsGroupView = rootView.findViewById(R.id.hot_walkthrough_asset_specialists_group);
		TextView assetSpecialistsGroupName = (TextView) assetSpecialistsGroupView
				.findViewById(R.id.hot_group_name_text_view);
		TextView assetSpecialistsGroupSeeAll = (TextView) assetSpecialistsGroupView
				.findViewById(R.id.hot_group_see_all_text_view);
		ImageView assetSpecialistsNewImageGroupSeeAll = (ImageView) assetSpecialistsGroupView
				.findViewById(R.id.hot_group_dropdown_new_image_view);
		assetSpecialistsNewImageGroupSeeAll.setVisibility(View.VISIBLE);
		HotGroup assetSpecialistsGroup = new HotGroup();
		assetSpecialistsGroup.setType(Type.ASSET_SPECIALISTS);
		assetSpecialistsGroup.setUpdateType(UpdateTypeEnum.BEST_ASSET_SPECIALISTS);
		assetSpecialistsGroup.setLastHours(LAST_HOURS);
		setupGroup(assetSpecialistsGroup, assetSpecialistsGroupName, assetSpecialistsGroupSeeAll);
		//ASSET SPECIALISTS

		View tradesGroupView = rootView.findViewById(R.id.hot_walkthrough_trades_group);
		TextView tradesGroupName = (TextView) tradesGroupView
				.findViewById(R.id.hot_group_name_text_view);
		TextView tradesGroupSeeAll = (TextView) tradesGroupView
				.findViewById(R.id.hot_group_see_all_text_view);
		HotGroup tradesGroup = new HotGroup();
		tradesGroup.setType(Type.TRADES);
		tradesGroup.setUpdateType(UpdateTypeEnum.BEST_TRADES);
		tradesGroup.setLastHours(LAST_HOURS);
		setupGroup(tradesGroup, tradesGroupName, tradesGroupSeeAll);

		View copiersGroupView = rootView.findViewById(R.id.hot_walkthrough_copiers_group);
		TextView copiersGroupName = (TextView) copiersGroupView
				.findViewById(R.id.hot_group_name_text_view);
		TextView copiersGroupSeeAll = (TextView) copiersGroupView
				.findViewById(R.id.hot_group_see_all_text_view);
		HotGroup copiersGroup = new HotGroup();
		copiersGroup.setType(Type.COPIERS);
		copiersGroup.setUpdateType(UpdateTypeEnum.BEST_COPIERS);
		copiersGroup.setLastHours(LAST_HOURS);
		setupGroup(copiersGroup, copiersGroupName, copiersGroupSeeAll);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == tradersGotIt.getId()) {
			tradersContent.setVisibility(View.INVISIBLE);
			assetSpecialistsContent.setVisibility(View.VISIBLE);
		} else if (v.getId() == assetSpecialistsGotIt.getId()) {
			assetSpecialistsContent.setVisibility(View.INVISIBLE);
			tradesContent.setVisibility(View.VISIBLE);
		} else if (v.getId() == tradesGotIt.getId()) {
			tradesContent.setVisibility(View.INVISIBLE);
			copiersContent.setVisibility(View.VISIBLE);
			scrollView.fullScroll(ScrollView.FOCUS_DOWN);
		} else if (v.getId() == copiersGotIt.getId()) {
			finishWalkthrough();
		}
	}

	private void finishWalkthrough() {
		application.getSharedPreferencesManager().putBoolean(
				COConstants.PREFERENCES_IS_HOT_WALKTHROUGH_SHOWN, true);

		dismiss();
	}

	private void clearGroupSpannable() {
		groupSpannable.clear();
		groupSpannable.clearSpans();
	}

	private void setupGroup(HotGroup group, TextView nameTextView, TextView seeAllTextView)
			throws Exception {
		clearGroupSpannable();

		HotAdapter.setupGroupName(getActivity(), group, nameTextView, groupSpannable, null);
		TextUtils.underlineText(seeAllTextView);
	}



}
