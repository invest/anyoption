package com.copyop.android.app.fragment.my_account.banking.deposit;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anyoption.android.app.fragment.my_account.banking.deposit.FirstNewCardFragment;

/**
 * @author Anastas Arnaudov
 */
public class COFirstNewCardFragment extends FirstNewCardFragment {

    public static COFirstNewCardFragment newInstance(Bundle bundle){
        COFirstNewCardFragment firstNewCardFragment = new COFirstNewCardFragment();
        firstNewCardFragment.setArguments(bundle);
        return firstNewCardFragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        application.getAppseeManager().hideView(cardNumberField);
        return rootView;
    }

}
