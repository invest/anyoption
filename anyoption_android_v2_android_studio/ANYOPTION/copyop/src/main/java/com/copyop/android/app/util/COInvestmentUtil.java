package com.copyop.android.app.util;

import com.anyoption.android.app.util.InvestmentUtil;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.copyop.android.app.R;

public class COInvestmentUtil extends InvestmentUtil {

	/**
	 * Finds the call/put arrow icon for an investment.
	 */
	public static int getInvestmentIcon(Investment investment, double currentLevel) {
		boolean isCopied = investment.getCopyopType() == CopyOpInvTypeEnum.COPY;

		if (investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL) {
			if (InvestmentUtil.isOpenInvestmentProfitable(investment, currentLevel)) {
				return isCopied ? R.drawable.arrow_call_win_copied : R.drawable.arrow_call_win;
			} else {
				return isCopied ? R.drawable.arrow_call_lose_copied : R.drawable.arrow_call_lose;
			}
		} else if (investment.getTypeId() == Investment.INVESTMENT_TYPE_PUT) {
			if (InvestmentUtil.isOpenInvestmentProfitable(investment, currentLevel)) {
				return isCopied ? R.drawable.arrow_put_win_copied : R.drawable.arrow_put_win;
			} else {
				return isCopied ? R.drawable.arrow_put_lose_copied : R.drawable.arrow_put_lose;
			}
		}

		return 0;
	}

	/**
	 * Finds the call/put arrow icon for an investment.
	 */
	public static int getSettledInvestmentIcon(Investment investment, Currency currency) {
		boolean isCopied = investment.getCopyopType() == CopyOpInvTypeEnum.COPY;

		if (investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL) {
			if (InvestmentUtil.isSettledInvestmentProfitable(investment, currency)) {
				return isCopied ? R.drawable.arrow_call_win_copied : R.drawable.arrow_call_win;
			} else {
				return isCopied ? R.drawable.arrow_call_lose_copied : R.drawable.arrow_call_lose;
			}
		} else if (investment.getTypeId() == Investment.INVESTMENT_TYPE_PUT) {
			if (InvestmentUtil.isSettledInvestmentProfitable(investment, currency)) {
				return isCopied ? R.drawable.arrow_put_win_copied : R.drawable.arrow_put_win;
			} else {
				return isCopied ? R.drawable.arrow_put_lose_copied : R.drawable.arrow_put_lose;
			}
		}

		return R.drawable.transparent_selector;
	}

}
