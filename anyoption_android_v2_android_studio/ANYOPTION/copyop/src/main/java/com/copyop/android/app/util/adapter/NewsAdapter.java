package com.copyop.android.app.util.adapter;

import java.util.ArrayList;
import java.util.List;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.util.BitmapUtils;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.TextUtils.OnInnerTextClickListener;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.TextView;
import com.anyoption.common.beans.base.Market;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.model.Update;
import com.copyop.android.app.model.Update.Type;
import com.copyop.android.app.popup.CopyDialogFragment;
import com.copyop.android.app.popup.CopyDialogFragment.OnCopyStateChangedListener;
import com.copyop.android.app.popup.FollowDialogFragment;
import com.copyop.android.app.popup.WatchDialogFragment;
import com.copyop.android.app.popup.WatchDialogFragment.OnWatchStateChangedListener;
import com.copyop.android.app.util.COBitmapUtils;
import com.copyop.android.app.util.COConstants;
import com.copyop.android.app.widget.CopyButton.CopyButtonState;
import com.copyop.android.app.widget.CopySmallButton;
import com.copyop.android.app.widget.FollowButton;
import com.copyop.android.app.widget.FollowButton.FollowButtonState;
import com.copyop.android.app.widget.WatchSmallButton;
import com.copyop.android.app.widget.WatchSmallButton.WatchButtonState;

import android.content.Context;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

/**
 * Custom {@link BaseAdapter} for the Update Feed ListView.
 * @author Anastas Arnaudov
 *
 */
public class NewsAdapter extends BaseAdapter implements OnClickListener, OnCopyStateChangedListener, OnWatchStateChangedListener, OnInnerTextClickListener{

	public static final String TAG = NewsAdapter.class.getSimpleName();
	
	protected Update.Type[] types = { Update.Type.PROMOTION, Update.Type.NEWS };
	
	public static class UpdateViewHolder{
		public int position;
		
		public View topBorderView;
		public View bottomBorderView;
		public ImageView iconImageView;
		public TextView titleTextView;
		public TextView copiersTextView;
		public TextView watchersTextView;
		public TextView bodyTextView;
		public TextView timestampTextView;
		public CopySmallButton copyButton;
		public WatchSmallButton watchButton;
		public FollowButton followButton;
	}

	protected Context context;  
	protected LayoutInflater layoutInflater = null;  
	protected List<Update> updatesList;
	protected SpannableStringBuilder spannableStringBuilder;
	protected int iconSize;

	public NewsAdapter(List<Update> updatesList) {
    	this.updatesList = updatesList;
    	this.context = Application.get();
    	this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	spannableStringBuilder = new SpannableStringBuilder();
    	iconSize = context.getResources().getDimensionPixelSize(R.dimen.news_item_image_size);
    }
    
	@Override
	public int getViewTypeCount() {
		return types.length;
	}

	@Override
	public int getItemViewType(int position) {
		Update update = updatesList.get(position);
		if(update.getType() == Type.PROMOTION){
			return 0;
		}else{
			return 1;
		}
	}

	@Override
	public int getCount() {
		return updatesList.size();
	}

	@Override
	public Object getItem(int position) {
		Update update = updatesList.get(position);
		return update;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Update update = updatesList.get(position);
		UpdateViewHolder updateViewHolder;
		if(convertView == null){
			updateViewHolder = new UpdateViewHolder();
			if(update.getType() == Type.PROMOTION){
				convertView = layoutInflater.inflate(R.layout.news_update_promotion_item, parent, false);
				updateViewHolder.iconImageView = (ImageView) convertView.findViewById(R.id.news_promotion_icon_image_view);
				updateViewHolder.titleTextView = (TextView) convertView.findViewById(R.id.news_promotion_title_text_view);
				updateViewHolder.bodyTextView = (TextView) convertView.findViewById(R.id.news_promotion_body_text_view);
			}else{
				convertView = layoutInflater.inflate(R.layout.news_item, parent, false);
				updateViewHolder.topBorderView = convertView.findViewById(R.id.news_item_divider_top);
				updateViewHolder.bottomBorderView = convertView.findViewById(R.id.news_item_divider_bottom);
				updateViewHolder.iconImageView = (ImageView) convertView.findViewById(R.id.news_item_icon_image_view);
				updateViewHolder.titleTextView = (TextView) convertView.findViewById(R.id.news_item_title_text_view);
				updateViewHolder.copiersTextView = (TextView) convertView.findViewById(R.id.news_item_copiers_text_view);
				updateViewHolder.watchersTextView = (TextView) convertView.findViewById(R.id.news_item_watchers_text_view);
				updateViewHolder.bodyTextView = (TextView) convertView.findViewById(R.id.news_item_body_text_view);
				updateViewHolder.timestampTextView = (TextView) convertView.findViewById(R.id.news_item_timestamp_text_view);
				updateViewHolder.copyButton = (CopySmallButton) convertView.findViewById(R.id.news_item_copy_button);
				updateViewHolder.watchButton = (WatchSmallButton) convertView.findViewById(R.id.news_item_watch_button);
				updateViewHolder.followButton = (FollowButton) convertView.findViewById(R.id.news_item_follow_button);
			}
			convertView.setTag(updateViewHolder);
		}else{
			updateViewHolder = (UpdateViewHolder) convertView.getTag();
		}
		updateViewHolder.position = position;
		
		try {
			setupListItem(update, updateViewHolder);
		} catch (Exception e) {
			Log.e(TAG, "ListView getView()", e);
		}
		
		return convertView;
	}

	protected List<Update> getUpdatesByUserId(long userId){
		List<Update> result = new ArrayList<Update>();
		for(Update update : updatesList){
			if(update.getSecondUserId() > 0 && update.getSecondUserId() == userId){
				result.add(update);
			}else if(update.getUserId() == userId){
				result.add(update);
			}
		}
		return result;
	}
	
	protected void setupListItem(Update update, UpdateViewHolder updateViewHolder) throws Exception{
		clearSpannable();
		if(update.getType() == Type.PROMOTION){
			setupPromotionItem(update, updateViewHolder);
		}else if(update.getType() == Type.NEWS){
			setupNewsItem(update, updateViewHolder);
		}else{
			throw new Exception("Unknown Update Type or it is NOT for this Screen");
		}
	}
	
	protected void setupPromotionItem(Update update, UpdateViewHolder updateViewHolder){
		BitmapUtils.loadBitmap(update.getPromotionIconResId(), updateViewHolder.iconImageView);
		updateViewHolder.titleTextView.setText(update.getTitle());
		updateViewHolder.bodyTextView.setText(update.getBody());
	}
	
	protected void setupNewsItem(Update update, UpdateViewHolder updateViewHolder){
		setupTitle(update, updateViewHolder);
		setupBody(update, updateViewHolder);
		setupBorders(update, updateViewHolder);
		setupIcon(update, updateViewHolder);
		setupCopiersWatchers(update, updateViewHolder);
		setupButtons(update, updateViewHolder);
		setupTimestamp(update, updateViewHolder);
	}

	protected void setupTitle(Update update, UpdateViewHolder updateViewHolder){
		updateViewHolder.titleTextView.setTag(updateViewHolder);
		updateViewHolder.titleTextView.setText(update.getTitle());
		if(update.getTitle() != null){
			if(update.getNickname() != null){
				createProfileInnerLink(updateViewHolder.titleTextView, spannableStringBuilder, update.getNickname(), update.getUserId());
			}
			if(update.getOldNickname() != null){
				createProfileInnerLink(updateViewHolder.titleTextView, spannableStringBuilder, update.getOldNickname(), update.getUserId());
			}
			if(update.getFullName() != null){
				createProfileInnerLink(updateViewHolder.titleTextView, spannableStringBuilder, update.getFullName(), update.getUserId());
			}
			if(update.getSecondNickname() != null){
				createProfileInnerLink(updateViewHolder.titleTextView, spannableStringBuilder, update.getSecondNickname(), update.getSecondUserId());
			}
			if(update.isShowOfIcon()){
				insertImages(updateViewHolder.titleTextView, spannableStringBuilder, R.drawable.showoff_icon_small_grey);
			}
		}
		clearSpannable();
	}
	
	protected void setupBody(Update update, UpdateViewHolder updateViewHolder){
		updateViewHolder.bodyTextView.setTag(updateViewHolder);
		updateViewHolder.bodyTextView.setText(update.getBody());
		if(update.getBody() != null){
			updateViewHolder.bodyTextView.setVisibility(View.VISIBLE);
			if(update.getNickname() != null && update.hasNicknameInBody()){
				createProfileInnerLink(updateViewHolder.bodyTextView, spannableStringBuilder, update.getNickname(), update.getUserId());
			}
			if(update.getSecondNickname() != null && update.hasNicknameInBody()){
				createProfileInnerLink(updateViewHolder.bodyTextView, spannableStringBuilder, update.getSecondNickname(), update.getSecondUserId());
			}
			if(update.getMarket() != null){
				createMarketInnerLink(updateViewHolder.bodyTextView, spannableStringBuilder, update.getMarket());
				if(update.getDirectionFormatted() != null || update.getLevelFormated() != null){
					decorateOrangeInnerText(updateViewHolder.bodyTextView, spannableStringBuilder, update.getDirectionFormatted(), update.getMarketName(), update.getLevelFormated());
				}
			}
		}else{
			updateViewHolder.bodyTextView.setVisibility(View.GONE);
		}
		clearSpannable();
	}
	
	protected void setupBorders(Update update, UpdateViewHolder updateViewHolder){
		int borderColor;
		if(update.getUserId() == Application.get().getUser().getId()){
			borderColor = ColorUtils.getColor(R.color.news_item_border_outer_mine);
		}else{
			borderColor = ColorUtils.getColor(R.color.news_item_border_outer);
		}
		updateViewHolder.topBorderView.setBackgroundColor(borderColor);
		updateViewHolder.bottomBorderView.setBackgroundColor(borderColor);
	}
	
	protected void setupIcon(Update update, UpdateViewHolder updateViewHolder){
		updateViewHolder.iconImageView.setImageDrawable(null);//clear the image
		if(update.isMarketIcon()){
			int resID = DrawableUtils.getMarketIconResID(update.getMarket());
			BitmapUtils.loadBitmap(resID, updateViewHolder.iconImageView);
		}else if(update.getIconURL() != null){
			COBitmapUtils.loadBitmap(update.getIconURL(), updateViewHolder.iconImageView, iconSize, iconSize);
		}
		
		updateViewHolder.iconImageView.setTag(updateViewHolder);
		updateViewHolder.iconImageView.setOnClickListener(this);
	}
	
	protected void setupCopiersWatchers(Update update, UpdateViewHolder updateViewHolder){
		if(update.isCopiersWatchers()){
			updateViewHolder.copiersTextView.setVisibility(View.VISIBLE);
			updateViewHolder.watchersTextView.setVisibility(View.VISIBLE);
			updateViewHolder.copiersTextView.setText(context.getString(R.string.newsItemCopiers, update.getCopiers()));
			updateViewHolder.watchersTextView.setText(context.getString(R.string.newsItemWatchers, update.getWatchers()));
		}else{
			updateViewHolder.copiersTextView.setVisibility(View.GONE);
			updateViewHolder.watchersTextView.setVisibility(View.GONE);
		}
	}
	
	protected void setupButtons(Update update, UpdateViewHolder updateViewHolder){
		updateViewHolder.copyButton.setTag(updateViewHolder);
		updateViewHolder.watchButton.setTag(updateViewHolder);
		updateViewHolder.followButton.setTag(updateViewHolder);
		
		updateViewHolder.copyButton.setOnClickListener(this);
		updateViewHolder.watchButton.setOnClickListener(this);
		updateViewHolder.followButton.setOnClickListener(this);
		
		if(update.isCopyWatchButtons()){
			updateViewHolder.copyButton.setVisibility(View.VISIBLE);
			updateViewHolder.watchButton.setVisibility(View.VISIBLE);
			if(update.isCopying()){
				updateViewHolder.copyButton.setState(CopyButtonState.STATE_COPYING);
				updateViewHolder.watchButton.setState(WatchButtonState.STATE_WATCHING);
			}else{
				if(update.isCopyingFrozen()){
					updateViewHolder.copyButton.setState(CopyButtonState.STATE_FROZEN);
				}else{
					updateViewHolder.copyButton.setState(CopyButtonState.STATE_NORMAL);
				}
				if(update.isWatching()){
					updateViewHolder.watchButton.setState(WatchButtonState.STATE_WATCHING);
				}else{
					updateViewHolder.watchButton.setState(WatchButtonState.STATE_NORMAL);
				}
			}
		}else{
			updateViewHolder.copyButton.setVisibility(View.GONE);
			updateViewHolder.watchButton.setVisibility(View.GONE);
		}
		
		if(update.isFollowButton()){
			updateViewHolder.followButton.setVisibility(View.VISIBLE);
			//Check if the option is open:
			long now = TimeUtils.getCurrentServerTime();
			if(update.getLastInvestTime() > 0 && update.getLastInvestTime() < now){
				updateViewHolder.followButton.setFollowButtonState(FollowButtonState.DISABLED);
			}else{
				updateViewHolder.followButton.setFollowButtonState(FollowButtonState.NORMAL);
			}
		}else{
			updateViewHolder.followButton.setVisibility(View.GONE);
		}

	}
	
	protected void setupTimestamp(Update update, UpdateViewHolder updateViewHolder){
		updateViewHolder.timestampTextView.setText(update.getTimestampFormated());
	}
	//******************************************************
	//******				STYLING					********
	//******************************************************
	
	protected void clearSpannable(){
		spannableStringBuilder.clear();
		spannableStringBuilder.clearSpans();
	}
	
	protected Spannable decorateOrangeInnerText(final TextView textview, Spannable spannable, String... innerTexts){
		for(String innerText : innerTexts){
			TextUtils.decorateInnerText(textview, spannable, textview.getText().toString(), innerText, Font.ROBOTO_MEDIUM, R.color.news_item_body_orange_link, 0);
		}
		return spannable;
	} 

	protected Spannable insertImages(TextView textView, Spannable spannable, int... resIds){
		return TextUtils.insertImages(textView, spannable, false, resIds);
	}
	
	protected Spannable createMarketInnerLink(final TextView textview, Spannable spannable, final Market market){
		if(market == null){
			return spannable;
		}
		TextUtils.makeClickableInnerText(textview, spannable, textview.getText().toString(), market.getDisplayName(), Font.ROBOTO_MEDIUM, 0, R.color.news_item_body_orange_link, false, this);
		decorateOrangeInnerText(textview, spannable, market.getDisplayName());
		return spannable;
	}

	protected Spannable createProfileInnerLink(final TextView textview, Spannable spannable, String name, final long profileId){
		if(name == null || name.isEmpty()){
			return spannable;
		}
		return TextUtils.makeClickableInnerText(textview, spannable, textview.getText().toString(), name, Font.ROBOTO_BOLD, 0, R.color.inbox_item_content_text, false, this);
	}
	//******************************************************
	
	//******************************************************
	//******				NAVIGATIONs				********
	//******************************************************
	
	protected void goToMarket(Market market){
		if(market != null){
			Bundle bundle = new Bundle();
			bundle.putSerializable(Constants.EXTRA_MARKET, market);
			Application.get().postEvent(new NavigationEvent(Screen.ASSET_VIEW_PAGER, NavigationType.DEEP, bundle));						
		}
	}

	protected void goToProfile(long profileId){
		if(profileId > 0){
			Bundle bundle = new Bundle();
			bundle.putLong(COConstants.EXTRA_PROFILE_ID, profileId);
			Application.get().postEvent(new NavigationEvent(COScreen.USER_PROFILE, NavigationType.DEEP, bundle));
		}else{
			Log.e(TAG, "User ID is null");
		}
	}
	
	//******************************************************
	
	//******************************************************
	//******				LISTENERS				********
	//******************************************************
	
	@Override
	public void onClick(View view) {
		if(view.getTag() instanceof UpdateViewHolder){
			UpdateViewHolder viewHolder = (UpdateViewHolder) view.getTag();
			int position = viewHolder.position;
			Update update = (Update) updatesList.get(position);
			handleOnClick(view, update, viewHolder);
		}
	}

	protected void handleOnClick(View view, Update update, UpdateViewHolder viewHolder){
		if(view.getId() == viewHolder.iconImageView.getId()){
			if(update.isMarketIcon()){
				goToMarket(update.getMarket());
				return;
			}else{
				goToProfile(update.getUserId());
				return;
			}
		}else if(view.getId() == viewHolder.copyButton.getId()){
			if((!update.isCopying() && RegulationUtils.checkRegulationAndPepAndThresholdStatusAndDoNecessary(false)) || update.isCopying()) {
				if (update.isCopyingFrozen()) {
					COApplication.get().getPopUpManager().showDozedOffPopUp(null);
					return;
				}

				long userId = update.getSecondUserId();
				String nickname = update.getSecondNickname();
				if (userId == 0) {
					userId = update.getUserId();
				}
				if (nickname == null) {
					nickname = update.getNickname();
				}

				Bundle bundle = new Bundle();
				bundle.putBoolean(COConstants.EXTRA_IS_COPYING, update.isCopying());
				bundle.putLong(COConstants.EXTRA_PROFILE_ID, userId);
				bundle.putString(COConstants.EXTRA_NICKNAME, nickname);
				CopyDialogFragment copyDialogFragment = CopyDialogFragment.newInstance(bundle);
				copyDialogFragment.setOnCopyStateChangedListener(this);
				Application.get().getFragmentManager().showDialogFragment(copyDialogFragment);
				return;
			}
		}else if(view.getId() == viewHolder.watchButton.getId()){
			if (RegulationUtils.checkRegulationStatusAndDoNecessary(false)) {
			String nickname = update.getSecondNickname();
			if(nickname == null){
				nickname = update.getNickname();
			}
			
			//Check if copying:
			if(update.isCopying()){
				COApplication.get().getPopUpManager().showCanNotUnwatchPopUp(nickname, null);
			}else{
				long userId = update.getSecondUserId();
				if(userId == 0){
					userId = update.getUserId();
				}
				
				Bundle bundle = new Bundle();
				bundle.putBoolean(COConstants.EXTRA_IS_COPYING, update.isCopying());
				bundle.putBoolean(COConstants.EXTRA_IS_WATCHING, update.isWatching());
				bundle.putLong(COConstants.EXTRA_PROFILE_ID, userId);
				bundle.putString(COConstants.EXTRA_NICKNAME, nickname);
				WatchDialogFragment watchDialogFragment = WatchDialogFragment.newInstance(bundle);
				watchDialogFragment.setOnWatchStateChangedListener(this);;
				Application.get().getFragmentManager().showDialogFragment(watchDialogFragment);
			}
			return;
			}
		}else if(view.getId() == viewHolder.followButton.getId()){
			if (RegulationUtils.checkRegulationAndPepAndThresholdStatusAndDoNecessary(false)) {
			Bundle bundle = new Bundle();
			long marketId = update.getMarket().getId();
			long investmentId = update.getInvestmentId();
			long direction = update.getDirection();
			bundle.putLong(COConstants.EXTRA_MARKET_ID, marketId);
			bundle.putLong(COConstants.EXTRA_INVESTMENT_ID, investmentId);
			bundle.putInt(COConstants.EXTRA_INVESTMENT_TYPE, (int) direction);
			FollowDialogFragment followInvestmentDialogFragment = FollowDialogFragment.newInstance(bundle);
			Application.get().getFragmentManager().showDialogFragment(followInvestmentDialogFragment);
			return;
			}
		}
	}
	
	@Override
	public void onCopyStateChanged(long userId, boolean isCopying) {
		List<Update> updates = getUpdatesByUserId(userId);
		if(!updates.isEmpty()){
			for(Update update : updates){
				update.setCopying(isCopying);
				update.setWatching(isCopying);
			}
			notifyDataSetChanged();
		}
	}

	@Override
	public void onWatchStateChanged(long userId, boolean isWatching) {
		List<Update> updates = getUpdatesByUserId(userId);
		if(!updates.isEmpty()){
			for(Update update : updates){
				update.setWatching(isWatching);
			}
			notifyDataSetChanged();
		}
	}

	@Override
	public void onInnerTextClick(android.widget.TextView textView, String text, String innerText) {
		if(textView.getTag() instanceof UpdateViewHolder){
			UpdateViewHolder updateViewHolder = (UpdateViewHolder) textView.getTag();
			Update update = (Update) updatesList.get(updateViewHolder.position);
			long profileID;
			if(innerText.equals(update.getNickname()) || innerText.equals(update.getOldNickname()) || innerText.equals(update.getFullName())){
				profileID = update.getUserId();
				goToProfile(profileID);
			}else if(innerText.equals(update.getSecondNickname())){
				profileID = update.getSecondUserId();
				goToProfile(profileID);
			}else if(innerText.equals(update.getMarketName())){
				Market market = update.getMarket();
				goToMarket(market);
			}
			
		}
	}
	
	//******************************************************
	
}
