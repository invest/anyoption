package com.copyop.android.app.manager;

import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COConstants;
import com.copyop.common.dto.base.Profile;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;

public class ShareRateManager {
	private static final String COPYOP_DOWNLOAD_URL = ShareRateManager.getShareURL();
	
	private static final String PLAY_STORE_APP_PAGE_URL = "market://details?id=";
	private static final String BROWSER_APP_PAGE_URL = "http://play.google.com/store/apps/details?id=";
	
	private static final int SHARE_LIMIT = 2;
	private static final int RATE_LIMIT = 1;
	
	private COApplication application;
	private String subject;
	private String chooserTitle;
	
	public ShareRateManager() {
		application = COApplication.get();
	}
	
	public void homeScreenShare() {
		Activity activity = application.getCurrentActivity();
		share(activity, activity.getString(R.string.shareHome), activity.getString(R.string.shareUpper));
	}
	
	public void receiptShare(String type, String market, String profit) {
		Activity activity = application.getCurrentActivity();
		String subject = activity.getString(R.string.shareReceipt, type, market, profit);
		share(activity, subject, activity.getString(R.string.shareUpper));
	}
	
	private void share(Activity activity, String subject, String chooserTitle) {
		this.subject = subject;
		this.chooserTitle = chooserTitle;
		if (application.getProfile().getShare() < SHARE_LIMIT) {
			callService("shareCopyop", "shareCopyopCallback");
		} else {
			openShareActivity();
		}
	}
	
	private void openShareActivity() {
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("text/plain");
		i.putExtra(Intent.EXTRA_SUBJECT, subject);
		i.putExtra(Intent.EXTRA_TEXT, COPYOP_DOWNLOAD_URL);
		Activity activity = application.getCurrentActivity();
		activity.startActivity(Intent.createChooser(i, chooserTitle));
	}
	
	public void shareCopyopCallback(Object resultObj) {
		MethodResult result = (MethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Profile profile = application.getProfile();
			profile.setShare(profile.getShare() + 1);
		}
		openShareActivity();
	}
	
	public void rate() {
		if (application.getProfile().getRate() < RATE_LIMIT) {
			callService("rateCopyop", "rateCopyopCallback");
		} else {
			openRateActivity();
		}
	}
	
	public void openRateActivity() {
		Activity activity = application.getCurrentActivity();
		try {
			activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(PLAY_STORE_APP_PAGE_URL + activity.getPackageName())));
		} catch (ActivityNotFoundException e) {
			activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(BROWSER_APP_PAGE_URL + activity.getPackageName())));
		}
	}
	
	public void rateCopyopCallback(Object resultObj) {
		MethodResult result = (MethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Profile profile = application.getProfile();
			profile.setRate(profile.getRate() + 1);
		}
		openRateActivity();
	}
	
	private void callService(String service, String callback) {
		UserMethodRequest request = new UserMethodRequest();
		application.getCommunicationManager().requestService(this, callback, service, request, MethodResult.class, false, false);
	}
	
	public static String getShareURL(){
		String url = COConstants.SERVICE_URL;
		url = url.substring(0, url.indexOf("/jsonService"));
		return url;
	}
}