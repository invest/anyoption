package com.copyop.android.app.popup;

/**
 * Created by veselinh on 22.1.2016 г..
 */

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.anyoption.android.app.popup.WithdrawBonusResetDialogFragment;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.TextUtils;
import com.copyop.android.app.R;

/**
 * Created by veselinh on 22.1.2016 г..
 */
public class COWithdrawBonusResetDialogFragment extends WithdrawBonusResetDialogFragment {

    public static COWithdrawBonusResetDialogFragment newInstance(Bundle bundle) {
        COWithdrawBonusResetDialogFragment coWithdrawBonusResetDialogFragment = new COWithdrawBonusResetDialogFragment();
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, true);
        bundle.putInt(ARG_BACKGROUND_COLOR_KEY, R.color.black_alpha_40);
        bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);

        coWithdrawBonusResetDialogFragment.setArguments(bundle);
        return coWithdrawBonusResetDialogFragment;
    }

    @Override
    protected int getContentLayout() {
        return R.layout.withdraw_bonus_reset_popup;
    }

    @Override
    protected void onCreateContentView(View contentView) {
        TextView messageTextView = (TextView) contentView.findViewById(R.id.withdraw_bonus_reset_popup_message_text_view);

        TextUtils.decorateInnerText(messageTextView, null, messageTextView.getText().toString(), getString(R.string.withdrawBonusResetPopupBold), FontsUtils.Font.ROBOTO_MEDIUM, 0, 0);

        yesButton = contentView.findViewById(R.id.yes_button);
        noButton = contentView.findViewById(R.id.no_button);

        yesButton.setOnClickListener(this);
        noButton.setOnClickListener(this);
    }
}
