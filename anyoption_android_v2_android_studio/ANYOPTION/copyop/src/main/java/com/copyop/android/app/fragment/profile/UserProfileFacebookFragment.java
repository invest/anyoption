package com.copyop.android.app.fragment.profile;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.manager.COFacebookManager;
import com.copyop.android.app.util.COBitmapUtils;
import com.copyop.android.app.util.COConstants;
import com.copyop.android.app.widget.CustomSlidingDrawer;
import com.copyop.android.app.widget.CustomSlidingDrawer.OnDrawerCloseListener;
import com.copyop.android.app.widget.CustomSlidingDrawer.OnDrawerOpenListener;
import com.copyop.common.dto.base.Profile;
import com.copyop.common.enums.base.ProfileLinkTypeEnum;
import com.copyop.json.results.ProfileMethodResult;
import com.copyop.json.results.ProfileMethodResult.ProfileWithLink;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserProfileFacebookFragment extends BaseFragment implements OnClickListener {

	private View rootView;
	private LayoutInflater layoutInflater;

	private CustomSlidingDrawer slidingDrawer;
	private ImageView toggleButton;
	private TextView friendsCountView;
	private View inviteButton;
	private ListView friendsList;
	private FriendsListAdapter listAdapter;
	private View notConnectedLayout;
	private View noFriendsView;

	private List<Profile> fbFriends;
	private Map<Profile, ProfileLinkTypeEnum> fbFriendsLinks;
	private boolean isFbConnected;

	@SuppressWarnings("hiding")
	protected COApplication application;
	private long profileUserId;
	private String nickname;
	private boolean isMyProfile;

	/**
	 * Creates new instance of this fragment.
	 */
	public static UserProfileFacebookFragment newInstance(Bundle bundle) {
		UserProfileFacebookFragment fragment = new UserProfileFacebookFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		application = COApplication.get();
		fbFriends = new ArrayList<Profile>();
		fbFriendsLinks = new HashMap<Profile, ProfileLinkTypeEnum>();
		isFbConnected = checkIsFbConnected();
	}

	@Override
	public void onResume() {
		super.onResume();

		application.registerForEvents(this, NavigationEvent.class);
	}

	@Override
	public void onPause() {
		application.unregisterForAllEvents(this);

		super.onPause();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.user_profile_fb_fragment, container, false);
		layoutInflater = inflater;

		toggleButton = (ImageView) rootView.findViewById(R.id.user_profile_fb_open_button);
		friendsCountView = (TextView) rootView.findViewById(R.id.user_profile_fb_count);

		inviteButton = rootView.findViewById(R.id.user_profile_fb_invite_button);
		inviteButton.setOnClickListener(this);
		setUpInviteButtonText();

		slidingDrawer = (CustomSlidingDrawer) rootView.findViewById(R.id.user_profile_fb_drawer);
		slidingDrawer.setOnDrawerOpenListener(new OnDrawerOpenListener() {

			@Override
			public void onDrawerOpened() {
				toggleButton.setImageResource(R.drawable.fb_popup_close_arrow);
				friendsCountView.setVisibility(View.GONE);
			}
		});

		slidingDrawer.setOnDrawerCloseListener(new OnDrawerCloseListener() {

			@Override
			public void onDrawerClosed() {
				toggleButton.setImageResource(R.drawable.fb_popup_open_arrow);
			}
		});

		noFriendsView = rootView.findViewById(R.id.user_profile_fb_no_friends);

		friendsList = (ListView) rootView.findViewById(R.id.user_profile_fb_list);
		listAdapter = new FriendsListAdapter();
		friendsList.setAdapter(listAdapter);
		friendsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				openProfilePage(fbFriends.get(position));
			}
		});

		setUpContent();

		return rootView;
	}

	/**
	 * Called when a {@link NavigationEvent} occurs.
	 * 
	 * @param event
	 */
	public void onEventMainThread(NavigationEvent event) {
		if (event.getToScreen() == COScreen.USER_PROFILE) {
			slidingDrawer.close();
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == inviteButton.getId()) {
			if (isFbConnected) {
				invite();
			} else {
				connect();
			}
		}
	}

	private void invite() {
		if (COFacebookManager.hasActiveSession()) {
			COFacebookManager.inviteFriends(getActivity(), this);
		} else {
			Log.e(TAG,"FacebookManager.hasActiveSession() == " + COFacebookManager.hasActiveSession());
		}
	}

	private void connect() {
		COFacebookManager.getUser(getActivity(), this, new COFacebookManager.UserListener() {

			@Override
			public void onUser(JSONObject user) {
				if (user != null) {
					Log.d(TAG, "The user is connected to Facebook");
					isFbConnected = true;
				} else {
					Log.d(TAG, "The user could not connect to Facebook");
					isFbConnected = false;
				}

				setUpInviteButtonText();
			}
		});
	}

	public void setProfileInfo(long userId, ProfileMethodResult profileInfo) {
		profileUserId = userId;
		nickname = profileInfo.getNickname();
		isMyProfile = profileUserId == application.getUser().getId();

		fbFriends.clear();
		fbFriendsLinks.clear();
		if (profileInfo.getFbFriends() != null && !profileInfo.getFbFriends().isEmpty()) {
			fbFriends.addAll(profileInfo.getFbFriends());
		} else if (profileInfo.getFbFriendsWithLink() != null
				&& !profileInfo.getFbFriendsWithLink().isEmpty()) {
			for (ProfileWithLink pl : profileInfo.getFbFriendsWithLink()) {
				fbFriends.add(pl.getProfile());
				fbFriendsLinks.put(pl.getProfile(), pl.getLink());
			}
		}

		setUpContent();
	}

	private void setUpInviteButtonText() {
		TextView inviteButtonText = (TextView) rootView
				.findViewById(R.id.user_profile_fb_button_text);
		if (isFbConnected) {
			inviteButtonText.setText(getString(R.string.inviteFriendsUpper));
		} else {
			inviteButtonText.setText(getString(R.string.connectUpper));
		}
	}

	private void setUpContent() {
		if (isFbConnected) {
			if (notConnectedLayout != null) {
				notConnectedLayout.setVisibility(View.GONE);
			}

			if (!fbFriends.isEmpty() || isMyProfile) {
				noFriendsView.setVisibility(View.GONE);
				friendsList.setVisibility(View.VISIBLE);
			} else {
				friendsList.setVisibility(View.GONE);
				noFriendsView.setVisibility(View.VISIBLE);
			}

			if (fbFriends.isEmpty() || isMyProfile) {
				friendsCountView.setVisibility(View.GONE);
			} else {
				friendsCountView.setText(String.valueOf(fbFriends.size()));
				friendsCountView.setVisibility(View.VISIBLE);
			}
		} else {
			friendsList.setVisibility(View.GONE);
			noFriendsView.setVisibility(View.GONE);
			friendsCountView.setVisibility(View.GONE);

			if (notConnectedLayout == null) {
				ViewStub stub = (ViewStub) rootView
						.findViewById(R.id.user_profile_fb_not_connected_content);
				stub.setLayoutResource(R.layout.user_profile_fb_not_connected_content);
				notConnectedLayout = stub.inflate();
			} else {
				notConnectedLayout.setVisibility(View.VISIBLE);
			}
		}

		listAdapter.notifyDataSetChanged();
	}

	private boolean checkIsFbConnected() {
		Profile profile = application.getProfile();
		if (profile != null && profile.getFbId() != null) {
			return true;
		}

		return false;
	}

	private void openProfilePage(Profile profile) {
		Bundle args = new Bundle();
		args.putLong(COConstants.EXTRA_PROFILE_ID, profile.getUserId());
		args.putString(Constants.EXTRA_ACTION_BAR_TITLE, profile.getNickname());
		NavigationEvent navigationEvent = new NavigationEvent(COScreen.USER_PROFILE,
				NavigationType.DEEP, args);
		application.postEvent(navigationEvent);
	}

	private class FriendsListAdapter extends BaseAdapter {

		public FriendsListAdapter() {
			super();
		}

		@Override
		public int getCount() {
			return fbFriends.size();
		}

		@Override
		public Object getItem(int position) {
			return fbFriends.get(position);
		}

		@Override
		public long getItemId(int position) {
			return fbFriends.get(position).getUserId();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View itemView = convertView;
			if (itemView == null) {
				itemView = layoutInflater.inflate(R.layout.user_profile_facebook_item, parent,
						false);
			}

			Profile profile = fbFriends.get(position);
			ImageView avatar = (ImageView) itemView.findViewById(R.id.user_profile_fb_item_icon);
			if (profile.getAvatar() != null) {
				COBitmapUtils.loadBitmap(profile.getAvatar(), avatar);
			} else {
				avatar.setImageDrawable(null);
			}

			TextView name = (TextView) itemView.findViewById(R.id.user_profile_fb_item_name);
			name.setText(profile.getFirstName() + " " + profile.getLastName());

			TextView copyingWatching = (TextView) itemView
					.findViewById(R.id.user_profile_fb_item_coping);
			ProfileLinkTypeEnum link = fbFriendsLinks.get(profile);
			if (link == ProfileLinkTypeEnum.COPY) {
				copyingWatching.setText(getString(R.string.userProfileCopying) + " " + nickname);
			} else if (link == ProfileLinkTypeEnum.WATCH) {
				copyingWatching.setText(getString(R.string.userProfileWatching) + " " + nickname);
			} else {
				copyingWatching.setText(null);
			}

			return itemView;
		}

	}

}
