package com.copyop.android.app.manager;

import android.os.Bundle;
import android.util.Log;

import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.manager.LoginManager;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.results.MethodResult;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.event.AcceptTermsAndConditionsEvent;
import com.copyop.android.app.util.COConstants;
import com.copyop.android.app.widget.CORequestButton;
import com.copyop.json.results.UserMethodResult;

import org.json.JSONArray;

public class COLoginManager extends LoginManager {

	private UserMethodResult result;

	public COLoginManager() {
		super();
	}

	@Override
	protected Class<? extends MethodResult> getResultClass() {
		return UserMethodResult.class;
	}

	@Override
	protected boolean isHandleError() {
		return true;
	}
	
	@Override
	public void getUserCallBack(Object resultObj) {
		Log.d(TAG,  "User = " + application.getUser());
		((CORequestButton)loginButton).stopLoading();
		result = (UserMethodResult) resultObj;
		if (null != result && result.getErrorCode() == COConstants.ERROR_CODE_NOT_ACCEPTED_TERMS) {

			//Check if a Language change is needed:
			if(result.getUser() != null){
				boolean shouldChangeLocale = false;
				if(application.getLocale() == null || !application.getLocale().getLanguage().equals(application.getSupportedLanguageForLocale(result.getUser().getLocale().getLanguage()))) {
					shouldChangeLocale = true;
				}
				if(shouldChangeLocale){
					COApplication.get().setSavedFromRestartUser(result.getUser());
					application.changeLocale(result.getUser().getLocale().getLanguage(), COScreen.LSS_AO_FINISH);
					return;
				}
			}
			////////////////////////////////////////

			Bundle bundle = new Bundle();
			bundle.putSerializable(Constants.EXTRA_USER, result.getUser());
			application.postEvent(new NavigationEvent(COScreen.LSS_AO_FINISH, NavigationType.DEEP, bundle));
			application.postEvent(new AcceptTermsAndConditionsEvent());
			return;
		}
		if (null != result && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			COApplication.get().setProfile(result.getCopyopProfile());
			COApplication.get().setCopyAmounts(result.getCopyAmounts());
			COApplication.get().setCopyOddsLose(100 - result.getOddsLose()*100);
			COApplication.get().setCopyOddsWin(result.getOddsWin()*100);
			COApplication.get().setMaxCopiersPerAccount(result.getMaxCopiersPerAccount());
			
			if (result.getCopyopProfile() != null && result.getCopyopProfile().getFbId() != null) {
				COApplication.get().getFacebookManager().getFriends(fragment.getActivity(), fragment, new COFacebookManager.FriendsListener() {
					@Override
					public void onFriends(JSONArray friends) {
						if (null != friends) {
							updateFacebookFriends(result.getCopyopProfile().getFbId(), friends);
						} else {
							finishLoginCallback();
						}
					}
				});
			} else {
				finishLoginCallback();
			}

		} else {
			finishLoginCallback();
		}
	}

	private void updateFacebookFriends(String fbId, JSONArray friends) {
		COApplication.get().getFacebookManager().updateFriends(fbId, friends, this, "updateFacebookFriendsCallback");
	}
	
	public void updateFacebookFriendsCallback(Object resultObj) {
		finishLoginCallback();
	}
	
	public void finishLoginCallback() {
		Log.d(TAG, "finishLoginCallback");
		super.getUserCallBack(result);
	}

}