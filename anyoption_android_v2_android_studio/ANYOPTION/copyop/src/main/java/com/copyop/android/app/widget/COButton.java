package com.copyop.android.app.widget;

import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.widget.Button;
import com.copyop.android.app.R;
import com.copyop.android.app.util.CODrawableUtils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class COButton extends Button implements View.OnTouchListener{

	protected int color = ColorUtils.getColor(R.color.transparent);
	protected int shadowColor = ColorUtils.getColor(R.color.transparent);;
	protected int shadowSize = (int) getResources().getDimension(R.dimen.button_shadow_width);
	protected boolean isBorder = true;
	protected int paddingLeft;
	protected int paddingTop;
	protected int paddingRight;
	protected int paddingBottom;
	protected Drawable normalBackground;
	protected Drawable pressedBackground;
	protected int iconBGColor = 0;
	protected int iconBGSize = 0;
	protected float radius = getResources().getDimension(R.dimen.button_radius);
	
	public COButton(Context context) {
		super(context);
		init(context, null, 0);
	}
	
	public COButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}

	public COButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs, 0);
	}
	
	@SuppressLint("ClickableViewAccessibility")
	protected void init(Context context, AttributeSet attrs, int defStyleAttr) {
		setupAttributes(context, attrs, defStyleAttr);
		
		setTextColor(ColorUtils.getColor(R.color.white));
		if(getFont() == null){
			setFont(Font.ROBOTO_REGULAR);
		}

		if(!isInEditMode()){
			normalBackground = CODrawableUtils.createClickableDrawable(context, color, shadowColor, iconBGColor, iconBGSize, shadowSize, isBorder, false, radius);
			pressedBackground = CODrawableUtils.createClickableDrawable(context, color, shadowColor, iconBGColor, iconBGSize,shadowSize, isBorder, true, radius);
		}
	
		this.setOnTouchListener(this);
	}

	protected void setupAttributes(Context context, AttributeSet attrs, int defStyleAttr){
		// Get custom attributes:
		TypedArray attributesTypedArray = context.obtainStyledAttributes(attrs,R.styleable.COButton);
		color = attributesTypedArray.getColor(R.styleable.COButton_color, color);
		shadowColor = attributesTypedArray.getColor(R.styleable.COButton_shadowColor, shadowColor);
		shadowSize = (int)attributesTypedArray.getDimension(R.styleable.COButton_shadowSize, shadowSize);
		isBorder = attributesTypedArray.getBoolean(R.styleable.COButton_isBorder, isBorder);
		iconBGColor = attributesTypedArray.getColor(R.styleable.COButton_iconBGColor, iconBGColor);
		iconBGSize = (int)attributesTypedArray.getDimension(R.styleable.COButton_iconBGSize, iconBGSize);
		radius = attributesTypedArray.getDimension(R.styleable.COButton_radius, radius);
		attributesTypedArray.recycle();
		//////////////////////////	
		//Get paddingLeft, paddingRight
        int[] attrsArray = new int[]{
                android.R.attr.paddingLeft,  // 0
                android.R.attr.paddingRight, // 1
        };
        TypedArray ta = context.obtainStyledAttributes(attrs, attrsArray);
        if (ta == null) return;
        paddingLeft = ta.getDimensionPixelSize(0, paddingLeft);
        paddingRight = ta.getDimensionPixelSize(1, paddingRight);
        ta.recycle();

        //Get paddingTop, paddingBottom
        int[] attrsArray2 = new int[]{
                android.R.attr.paddingTop,   // 0
                android.R.attr.paddingBottom,// 1
        };
        TypedArray ta1 = context.obtainStyledAttributes(attrs, attrsArray2);
        if (ta1 == null) return;
        paddingTop = ta1.getDimensionPixelSize(0, paddingTop);
        paddingBottom = ta1.getDimensionPixelSize(1, paddingBottom);
        paddingBottom += shadowSize;
        ta1.recycle();
		////////////////////////////////////////
	}
	
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if(!isInEditMode()){
        	updateBackground(normalBackground);
        }else{
        	setBackgroundColor(color);
        }
        //Set padding
        this.setPadding(paddingLeft, paddingTop + shadowSize, paddingRight, paddingBottom + shadowSize);
    }

	@SuppressLint("ClickableViewAccessibility")
	@Override
    public boolean onTouch(View view, MotionEvent event) {
		
		switch (event.getAction()) {
	        case MotionEvent.ACTION_DOWN:
	            updateBackground(pressedBackground);
	            this.setPadding(paddingLeft, paddingTop + shadowSize, paddingRight, paddingBottom);
	            break;
	        case MotionEvent.ACTION_MOVE:
	            Rect r = new Rect();
	            view.getLocalVisibleRect(r);
	            if (!r.contains((int) event.getX(), (int) event.getY() + 3 * shadowSize) &&
	                !r.contains((int) event.getX(), (int) event.getY() - 3 * shadowSize)) {
	            	updateBackground(normalBackground);
	            	this.setPadding(paddingLeft, paddingTop + shadowSize, paddingRight, paddingBottom + shadowSize);
	            }
	            break;
	        case MotionEvent.ACTION_OUTSIDE:
	        case MotionEvent.ACTION_CANCEL:
	        case MotionEvent.ACTION_UP:
	        	updateBackground(normalBackground);
	        	this.setPadding(paddingLeft, paddingTop + shadowSize, paddingRight, paddingBottom + shadowSize);
	            break;
		}
		return false;
	}
	
	public void setStyle(int colorRes, int shadowColorRes){
		this.color = ColorUtils.getColor(colorRes);
		this.shadowColor = ColorUtils.getColor(shadowColorRes);
		normalBackground = CODrawableUtils.createClickableDrawable(getContext(), color, shadowColor, iconBGColor, iconBGSize, shadowSize, isBorder, false);
		pressedBackground = CODrawableUtils.createClickableDrawable(getContext(), color, shadowColor, iconBGColor, iconBGSize, shadowSize, isBorder, true);
		updateBackground(normalBackground);
		
		setPadding(paddingLeft, paddingTop + shadowSize, paddingRight, paddingBottom + shadowSize);
	}
	
	private void updateBackground(Drawable drawable){
		DrawableUtils.setBackground(this, drawable);
	}
}
