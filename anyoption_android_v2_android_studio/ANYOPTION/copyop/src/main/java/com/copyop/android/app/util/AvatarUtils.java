package com.copyop.android.app.util;

import java.util.Random;

public class AvatarUtils {
	public static final int AVATAR_MALE_COUNT = 100;
	public static final int AVATAR_FEMALE_COUNT = 50;
	
	public static String generateRandomAvatar() {
		Random r = new Random();
		int gender = r.nextInt(2);
		if (gender == 0) {
			return generateRandomMaleAvatar();
		} else {
			return generateRandomFemaleAvatar();
		}
	}
	
	public static String generateRandomMaleAvatar() {
		Random r = new Random();
		return getBaseURL() + "avatar/m" + (r.nextInt(AVATAR_MALE_COUNT) + 1) + ".png";
	}
	
	public static String generateRandomFemaleAvatar() {
		Random r = new Random();
		return getBaseURL() + "avatar/f" + (r.nextInt(AVATAR_FEMALE_COUNT) + 1) + ".png";
	}
	
	public static String getBaseURL() {
		String url = COConstants.SERVICE_URL;
		url = url.substring(0, url.lastIndexOf("AnyoptionService/"));
		return url;
	}
}