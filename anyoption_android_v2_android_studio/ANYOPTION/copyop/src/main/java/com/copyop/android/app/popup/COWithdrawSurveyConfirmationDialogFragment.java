package com.copyop.android.app.popup;

import android.os.Bundle;
import android.util.Log;

import com.anyoption.android.R;
import com.anyoption.android.app.popup.WithdrawSurveyConfirmationDialogFragment;
import com.anyoption.android.app.popup.WithdrawSurveyDialogFragment;

/**
 * A POPUP that is shown when the user attempts to make a withdrawal that is lower than a MIN amount. 
 * @author Anastas Arnaudov
 *
 */
public class COWithdrawSurveyConfirmationDialogFragment extends WithdrawSurveyConfirmationDialogFragment {

	public static final String TAG = COWithdrawSurveyConfirmationDialogFragment.class.getSimpleName();
	/**
	 * Use this method to create new instance of the fragment.
	 * @return
	 */
	public static COWithdrawSurveyConfirmationDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new COWithdrawSurveyConfirmationDialogFragment.");
		COWithdrawSurveyConfirmationDialogFragment fragment = new COWithdrawSurveyConfirmationDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, true);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.black_alpha_40);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		fragment.setArguments(bundle);
		return fragment;
	}

}
