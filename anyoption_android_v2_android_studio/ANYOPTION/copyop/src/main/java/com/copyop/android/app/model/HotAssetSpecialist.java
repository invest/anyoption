package com.copyop.android.app.model;

import com.anyoption.common.beans.base.Market;
import com.copyop.android.app.util.COTimeUtils;

public class HotAssetSpecialist extends HotItem {

    private static final long serialVersionUID = 1L;
    private Market market;
    protected String marketName;
    private boolean isUserFrozen;
    private boolean isWatching;
    private boolean isCopying;

    public boolean isWatching() {
        return isWatching;
    }

    public void setIsWatching(boolean isWatching) {
        this.isWatching = isWatching;
    }

    public boolean isCopying() {
        return isCopying;
    }

    public void setIsCopying(boolean isCopying) {
        this.isCopying = isCopying;
    }

    public boolean isUserFrozen() {
        return isUserFrozen;
    }

    public void setIsUserFrozen(boolean isUserFrozen) {
        this.isUserFrozen = isUserFrozen;
    }

    public String getMarketName() {
        return marketName;
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
        if (market != null) {
            marketName = market.getDisplayName();
        }
    }
}
