package com.copyop.android.app.widget;

import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.copyop.android.app.R;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

public class WatchButton extends COButton {
	
	public enum WatchButtonState{
		STATE_NORMAL,
		STATE_WATCHING;
	}
	
	private WatchButtonState state = WatchButtonState.STATE_NORMAL;
	private Drawable drawable;
	private String text;
	private View view;
	
	public WatchButton(Context context) {
		super(context);
		init(context, null, 0);
	}
	
	public WatchButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	
	public WatchButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs, defStyleAttr);
	}
  		
	@Override
	protected void init(Context context, AttributeSet attrs, int defStyleAttr) {
		if(view == null){
			color = ColorUtils.getColor(R.color.copy_watch_button_normal);
			shadowColor = ColorUtils.getColor(R.color.copy_watch_button_normal_shadow);
			shadowSize = getResources().getDimensionPixelSize(R.dimen.button_shadow_width);
			isBorder = false;
			iconBGColor = ColorUtils.getColor(R.color.copy_watch_button_normal_icon_bg);
			iconBGSize = getResources().getDimensionPixelSize(R.dimen.button_copy_watch_icon_bg_width);
			paddingLeft = getResources().getDimensionPixelSize(R.dimen.button_watch_padding_left);
			setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
			setFont(Font.ROBOTO_REGULAR);
			setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.button_copy_watch_text));
			
			setState(WatchButtonState.STATE_NORMAL);
		}
		super.init(context, attrs, defStyleAttr);
	}

	public void setState(WatchButtonState copyButtonState) {
		this.state = copyButtonState;
		if (state == WatchButtonState.STATE_NORMAL) {
			text = getContext().getString(R.string.watchCap);
			drawable = DrawableUtils.getDrawable(R.drawable.watch_button_normal);
			color = R.color.copy_watch_button_normal;
			shadowColor = R.color.copy_watch_button_normal_shadow;
			iconBGColor = ColorUtils.getColor(R.color.copy_watch_button_normal_icon_bg);
		} else if (state == WatchButtonState.STATE_WATCHING) {
			text = getContext().getString(R.string.watchingCap);
			drawable = DrawableUtils.getDrawable(R.drawable.watch_button_active);
			color = R.color.copy_watch_button_active;
			shadowColor = R.color.copy_watch_button_active_shadow;
			iconBGColor = ColorUtils.getColor(R.color.copy_watch_button_active_icon_bg);
		}

		refresh();
	}

	public WatchButtonState getState() {
		return state;
	}

	private void refresh() {
		setText(text);
		setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
		setStyle(color, shadowColor);
		refreshDrawableState();
		invalidate();
	}

}
