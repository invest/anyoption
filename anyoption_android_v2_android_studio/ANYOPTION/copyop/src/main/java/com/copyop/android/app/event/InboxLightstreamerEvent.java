package com.copyop.android.app.event;

import com.lightstreamer.ls_client.UpdateInfo;

public class InboxLightstreamerEvent {

	private UpdateInfo update;
	
	public InboxLightstreamerEvent(UpdateInfo update){
		this.setUpdate(update);
	}

	public UpdateInfo getUpdateInfo() {
		return update;
	}

	public void setUpdate(UpdateInfo update) {
		this.update = update;
	}
	
}
