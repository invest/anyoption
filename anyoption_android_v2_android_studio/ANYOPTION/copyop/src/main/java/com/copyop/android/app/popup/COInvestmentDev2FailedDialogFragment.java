package com.copyop.android.app.popup;

import android.os.Bundle;
import android.util.Log;

import com.anyoption.android.app.popup.InvestmentDev2FailedDialogFragment;

/**
 * @author Anastas Arnaudov
 */
public class COInvestmentDev2FailedDialogFragment extends InvestmentDev2FailedDialogFragment {

    public static final String TAG = COInvestmentDev2FailedDialogFragment.class.getSimpleName();

    public static COInvestmentDev2FailedDialogFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new InvestmentCanceldDialogFragment.");
        COInvestmentDev2FailedDialogFragment fragment = new COInvestmentDev2FailedDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

}
