package com.copyop.android.app.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.Utils;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

public class COUtils extends Utils {

	public static File getTempImageFile(){
		return new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
	}
	
	public static Uri getTempImageUri(){
		return Uri.fromFile(getTempImageFile());
	}
	
	/**
	 * Opens the Device Image Gallery to pick an image.
	 * @param requestCode The request code used in the result (onActivityResult).
	 */
	public static void openImageGallery(int requestCode){
		Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		Application.get().getCurrentActivity().startActivityForResult(intent, requestCode);
	}
	
	/**
	 * Opens the device camera.
	 * @param requestCode The request code used in the result (onActivityResult).
	 */
	public static void openCamera(int requestCode) {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, getTempImageUri());
		Application.get().getCurrentActivity().startActivityForResult(intent, requestCode);
	}
	
	/**
	 * Try to open the Crop Image Application. If the device does NOT have one : return false.
	 * @param imageUri The image URI.
	 * @param outputWidth The desired output width of the Bitmap.
	 * @param outputHight The desired output height of the Bitmap.
	 * @param requestCode The request code used in the result (onActivityResult).
	 * @return False if the device does NOT support the action.
	 */
	public static void openCropImage(Uri imageUri, int outputWidth, int outputHight, int requestCode){
		Intent testIntent = new Intent("com.android.camera.action.CROP");
		testIntent.setType("image/*");

		List<ResolveInfo> list = Application.get().getPackageManager().queryIntentActivities(testIntent, 0);
		int size = list.size();

		if (size == 0) {           
			Log.e(TAG, "The device does NOT support Image Cropping!!! There are no apps to handle this!!!");
		}else{
			ResolveInfo res = list.get(0);

		    Intent intent = new Intent();
		    intent.setClassName(res.activityInfo.packageName, res.activityInfo.name);
			//intent.setClassName("com.google.android.gallery3d", "com.android.gallery3d.app.CropImage");
			intent.setDataAndType(imageUri, "image/*");
			intent.putExtra("outputX", outputWidth);
			intent.putExtra("outputY", outputHight);
			intent.putExtra("aspectX", 1);
			intent.putExtra("aspectY", 1);
			intent.putExtra("scale", true);
			intent.putExtra("return-data", true);    
			
			Application.get().getCurrentActivity().startActivityForResult(intent, requestCode);
		}
	}
	
	/**
	 * Checks whether the device supports Image Cropping.
	 * @return
	 */
	public static boolean isCropSupported(){
		boolean isCropSupported = true;
		Intent cropIntent = new Intent("com.android.camera.action.CROP");
		cropIntent.setType("image/*");

		List<ResolveInfo> list = Application.get().getPackageManager().queryIntentActivities(cropIntent, 0);
		int size = list.size();

		if (size == 0) {           
			isCropSupported = false;
		}
		return isCropSupported;
	}
	
	/**
	 *  Check if the device has a camera.
	*/
	public static boolean isCameraHardware() {
	    if (Camera.getNumberOfCameras() > 0){
	        // The device has a camera
	        return true;
	    } else {
	        // No camera on this device
	        return false;
	    }
	}
	
	/**
	 * Saves the file containing the image into the device Gallery.
	 * @param file
	 * @return null if the image file was not found.
	 */
	public static Uri saveImageToGallery(File file){
		Uri newUri = null;
		try {
			newUri = Uri.parse(MediaStore.Images.Media.insertImage(Application.get().getCurrentActivity().getContentResolver(), file.getAbsolutePath(), "temp" , ""));
		} catch (FileNotFoundException e) {
			Log.e(TAG, "Could NOT save image with path = " + file.getAbsolutePath(), e);
		}
		return newUri;
	}
	
}
