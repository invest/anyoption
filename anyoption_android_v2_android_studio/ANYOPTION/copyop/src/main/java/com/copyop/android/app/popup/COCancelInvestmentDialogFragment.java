package com.copyop.android.app.popup;

import android.os.Bundle;
import android.util.Log;

import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.popup.CancelInvestmentDialogFragment;

/**
 * @author Anastas Arnaudov
 */
public class COCancelInvestmentDialogFragment extends CancelInvestmentDialogFragment {

    public static final String TAG = COCancelInvestmentDialogFragment.class.getSimpleName();

    public static COCancelInvestmentDialogFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new CancelInvestmentDialogFragment.");
        COCancelInvestmentDialogFragment fragment = new COCancelInvestmentDialogFragment();
        bundle.putBoolean(BaseDialogFragment.ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
        bundle.putBoolean(BaseDialogFragment.ARG_SHOW_CLOSE_BUTTON_KEY, false);
        fragment.setArguments(bundle);
        return fragment;
    }


}
