package com.copyop.android.app.fragment.my_account.my_options;

import com.anyoption.android.app.fragment.my_account.my_options.MyOptionsFragment;
import com.copyop.android.app.R;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class COMyOptionsFragment extends MyOptionsFragment {

	public static COMyOptionsFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created fragment COMyOptionsFragment");
		COMyOptionsFragment fragment = new COMyOptionsFragment();
		fragment.setArguments(bundle);
		return fragment;
	}
	
	protected View openOptionsTabIndicator;
	protected View settledOptionsTabIndicator;
	
	
	@Override
	protected void setupTabs(){
		super.setupTabs();
		openOptionsTabIndicator = rootView.findViewById(R.id.my_options_open_options_tab_select_indicator);
		settledOptionsTabIndicator = rootView.findViewById(R.id.my_options_settled_options_tab_select_indicator);
	}
	
	@Override
	protected void selectTab(){
		super.selectTab();
		
		if(currentTab == TAB_OPEN_OPTIONS){
			openOptionsTabIndicator.setVisibility(View.VISIBLE);
			settledOptionsTabIndicator.setVisibility(View.INVISIBLE);
		}else if(currentTab == TAB_SETTLED_OPTIONS){
			settledOptionsTabIndicator.setVisibility(View.VISIBLE);
			openOptionsTabIndicator.setVisibility(View.INVISIBLE);
		}
	}
	
}
