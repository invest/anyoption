package com.copyop.android.app.manager;

import android.util.Log;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.manager.DeepLinksManager;
import com.copyop.android.app.COApplication;

/**
 * @author Anastas Arnaudov
 */
public class CODeepLinksManager extends DeepLinksManager {

    public static final String TAG = CODeepLinksManager.class.getSimpleName();

    public CODeepLinksManager(Application application) {
        super(application);
        Log.d(TAG, "Init.");
    }

    @Override
    protected Application.Screenable getRegisterScreen(){
        return COApplication.COScreen.LSS_SIGNUP;
    }

    @Override
    protected Application.Screenable getLoginScreen(){
        return COApplication.COScreen.LSS_LOGIN_SIGNUP;
    }
}
