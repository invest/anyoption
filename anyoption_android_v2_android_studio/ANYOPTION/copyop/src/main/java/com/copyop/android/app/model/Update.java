package com.copyop.android.app.model;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COTimeUtils;
import com.copyop.common.enums.base.QueueTypeEnum;
import com.copyop.common.enums.base.UpdateTypeEnum;

public class Update{

	public static enum Type{
		NEWS, PROMOTION, INBOX;
	}
	
	private Type type;
	private QueueTypeEnum queueType;
	private UpdateTypeEnum updateType;
	
	private long userId;
	private long secondUserId;
	private long copiers;
	private long watchers;
	private long newCopiers;
	private long timestamp;
	private long expiryTimestamp;
	private long lastInvestTime;
	private long settledTime;
	private long investmentId;
	private long investmentType;
	private long bonusTypeId;
	private long coinsBalance;
	private int lastHours;
	private int topProfitableTraders;
	private int seatsLeft;
	private int direction;
	private double amount;
	private double profit;
	private double level;
	private double hitPercent;
	private double trendPercent;
	private double oddsLose;
	private double oddsWin;
	private String nickname;
	private String firstName;
	private String lastName;
	private String secondNickname;
	private String oldNickname;
	private String marketName;
	private String amountFormatted;
	private String profitFormatted;
	private String levelFormated;
	private String expiryTimestampFormated;
	private String settledTimeFormatted;
	private String timestampFormated;
	private String iconURL;
	private String bonusDescription;
	private String directionFormatted;
	private String investmentTypeFormatted;
	private String lastInvestTimeFormatted;
	private boolean isRead;
	private boolean isCopying;
	private boolean isWatching;
	private boolean isCopyingFrozen;
	private Market market;
	
	//For the ListView:
	private String title;
	private String body;
	private int promotionIconResId;
	private boolean isMarketIcon;
	private boolean isCopiersWatchers;
	private boolean isCopyWatchButtons;
	private boolean isDepositButton;
	private boolean isFollowButton;
	private boolean isCoinsButton;
	private boolean isShowOfIcon;
	private boolean hasNicknameInBody;
	private boolean isShared;
	///////////////////
	
	public Type getType() {
		return type;
	}

	public Update setType(Type type) {
		this.type = type;
		return this;
	}
	
	public QueueTypeEnum getQueueType() {
		return queueType;
	}
	
	public Update setQueueType(QueueTypeEnum queueTypeEnum) {
		this.queueType = queueTypeEnum;
		return this;
	}
	
	public UpdateTypeEnum getUpdateType() {
		return updateType;
	}
	
	public Update setUpdateType(UpdateTypeEnum updateType) {
		this.updateType = updateType;
		return this;
	}
	public String getNickname() {
		return nickname;
	}

	public Update setNickname(String nickname) {
		this.nickname = nickname;
		return this;
	}

	public long getCopiers() {
		return copiers;
	}

	public Update setCopiers(long copiers) {
		this.copiers = copiers;
		return this;
	}

	public long getWatchers() {
		return watchers;
	}

	public Update setWatchers(long watchers) {
		this.watchers = watchers;
		return this;
	}

	public long getExpiryTimestamp() {
		return expiryTimestamp;
	}

	public Update setExpiryTimestamp(long expiryTimestamp) {
		this.expiryTimestamp = expiryTimestamp;
		expiryTimestampFormated = (COTimeUtils.formatNewsFeedExpireTime(expiryTimestamp));
		return this;
	}
	
	public long getSettledTime() {
		return settledTime;
	}

	public Update setSettledTime(long settledTime) {
		this.settledTime = settledTime;
		this.settledTimeFormatted = (COTimeUtils.formatNewsFeedExpireTime(settledTime));
		return this;
	}
	
	public double getLevel() {
		return level;
	}

	public Update setLevel(double level) {
		this.level = level;
		setLevelFormatted();
		return this;
	}
	
	private void setLevelFormatted(){
		if(market != null){
			int digits = (int)(long)market.getDecimalPoint();
			levelFormated = "(@" + Utils.getDecimalFormat(digits).format(level) + ")";
		}else{
			levelFormated = "(@" + level + ")";
		}
	}
	
	public String getLevelFormated() {
		return levelFormated;
	}
	
	public int getTopProfitableTraders() {
		return topProfitableTraders;
	}

	public Update setTopProfitableTraders(int topProfitableTraders) {
		this.topProfitableTraders = topProfitableTraders;
		return this;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public Update setTimestamp(long timestamp, boolean isFromGMT) {
		this.timestamp = timestamp;
		this.timestampFormated = COTimeUtils.formatNewsFeedTime(this.timestamp, isFromGMT);
		return this;
	}

	public Market getMarket() {
		return market;
	}

	public Update setMarket(Market market) {
		this.market = market;
		if(market != null){
			this.marketName = market.getDisplayName();
		}
		setLevelFormatted();
		return this;
	}

	public int getSeatsLeft() {
		return seatsLeft;
	}

	public Update setSeatsLeft(int seatsLeft) {
		this.seatsLeft = seatsLeft;
		return this;
	}

	public long getNewCopiers() {
		return newCopiers;
	}

	public Update setNewCopiers(long newCopiers) {
		this.newCopiers = newCopiers;
		return this;
	}

	public int getLastHours() {
		return lastHours;
	}

	public Update setLastHours(int lastHours) {
		this.lastHours = lastHours;
		return this;
	}

	public double getHitPercent() {
		return hitPercent;
	}

	public Update setHitPercent(double hitPercent) {
		this.hitPercent = hitPercent;
		return this;
	}

	public double getTrendPercent() {
		return trendPercent;
	}

	public Update setTrendPercent(double trendPercent) {
		this.trendPercent = trendPercent;
		return this;
	}

	public String getMarketName() {
		return marketName;
	}
	
	public boolean isCopying() {
		return isCopying;
	}

	public Update setCopying(boolean isCopying) {
		this.isCopying = isCopying;
		return this;
	}

	public boolean isWatching() {
		return isWatching;
	}

	public Update setWatching(boolean isWatching) {
		this.isWatching = isWatching;
		return this;
	}

	public String getExpiryTimestampFormated() {
		return expiryTimestampFormated;
	}

	public String getTimestampFormated() {
		return timestampFormated;
	}

	public String getIconURL() {
		return iconURL;
	}

	public Update setIconURL(String iconURL) {
		this.iconURL = iconURL;
		return this;
	}

	public long getInvestmentId() {
		return investmentId;
	}

	public Update setInvestmentId(long investmentId) {
		this.investmentId = investmentId;
		return this;
	}

	public long getInvestmentType() {
		return investmentType;
	}

	public Update setInvestmentType(long investmentType) {
		this.investmentType = investmentType;
		this.investmentTypeFormatted = (investmentType == Investment.INVESTMENT_TYPE_PUT) ? Application.get().getString(R.string.newsItemPut) : Application.get().getString(R.string.newsItemCall);
		return this;
	}

	public double getOddsLose() {
		return oddsLose;
	}

	public Update setOddsLose(double oddsLose) {
		this.oddsLose = oddsLose;
		return this;
	}

	public double getOddsWin() {
		return oddsWin;
	}

	public Update setOddsWin(double oddsWin) {
		this.oddsWin = oddsWin;
		return this;
	}

	public long getUserId() {
		return userId;
	}

	public Update setUserId(long userId) {
		this.userId = userId;
		return this;
	}

	public double getAmount() {
		return amount;
	}

	public Update setAmount(double amount) {
		this.amount = amount;
		this.amountFormatted = AmountUtil.getUserFormattedAmount(amount);
		return this;
	}
	
	public String getAmountFormatted() {
		return amountFormatted;
	}

	public double getProfit() {
		return profit;
	}

	public Update setProfit(double profit) {
		this.profit = profit;
		this.profitFormatted = AmountUtil.getUserFormattedAmount(profit);
		return this;
	}

	public String getProfitFormatted() {
		return profitFormatted;
	}

	public long getSecondUserId() {
		return secondUserId;
	}

	public Update setSecondUserId(long secondUserId) {
		this.secondUserId = secondUserId;
		return this;
	}

	public String getSecondNickname() {
		return secondNickname;
	}

	public Update setSecondNickname(String secondNickname) {
		this.secondNickname = secondNickname;
		return this;
	}

	public int getDirection() {
		return direction;
	}

	public Update setDirection(int direction) {
		this.direction = direction;
		this.directionFormatted = (direction == Investment.INVESTMENT_TYPE_PUT) ? Application.get().getString(R.string.newsItemPut) : Application.get().getString(R.string.newsItemCall);
		return this;
	}

	public String getTitle() {
		return title;
	}

	public Update setTitle(String title) {
		this.title = title;
		return this;
	}

	public String getBody() {
		return body;
	}

	public Update setBody(String body) {
		this.body = body;
		return this;
	}

	public boolean isCopyWatchButtons() {
		return isCopyWatchButtons;
	}

	public Update setCopyWatchButtons(boolean isCopyWatchButtons) {
		this.isCopyWatchButtons = isCopyWatchButtons;
		return this;
	}

	public boolean isFollowButton() {
		return isFollowButton;
	}

	public Update setFollowButton(boolean isFollowButton) {
		this.isFollowButton = isFollowButton;
		return this;
	}

	public boolean isRead() {
		return isRead;
	}

	public Update setRead(boolean isRead) {
		this.isRead = isRead;
		return this;
	}

	public long getBonusTypeId() {
		return bonusTypeId;
	}

	public Update setBonusTypeId(long bonusTypeId) {
		this.bonusTypeId = bonusTypeId;
		return this;
	}

	public String getBonusDescription() {
		return bonusDescription;
	}

	public Update setBonusDescription(String bonusDescription) {
		this.bonusDescription = bonusDescription;
		return this;
	}

	public boolean isCopiersWatchers() {
		return isCopiersWatchers;
	}

	public Update setCopiersWatchers(boolean isCopiersWatchers) {
		this.isCopiersWatchers = isCopiersWatchers;
		return this;
	}

	public String getDirectionFormatted() {
		return directionFormatted;
	}

	public boolean isShowOfIcon() {
		return isShowOfIcon;
	}

	public Update setShowOfIcon(boolean isShowOfIcon) {
		this.isShowOfIcon = isShowOfIcon;
		return this;
	}

	public long getCoinsBalance() {
		return coinsBalance;
	}

	public Update setCoinsBalance(long coinsBalance) {
		this.coinsBalance = coinsBalance;
		return this;
	}

	public String getInvestmentTypeFormatted() {
		return investmentTypeFormatted;
	}

	public boolean isMarketIcon() {
		return isMarketIcon;
	}

	public Update setMarketIcon(boolean isMarketIcon) {
		this.isMarketIcon = isMarketIcon;
		return this;
	}
	public boolean isDepositButton() {
		return isDepositButton;
	}

	public Update setDepositButton(boolean isDepositButton) {
		this.isDepositButton = isDepositButton;
		return this;
	}
	public boolean isCopyingFrozen() {
		return isCopyingFrozen;
	}

	public Update setCopyingFrozen(boolean isCopyingFrozen) {
		this.isCopyingFrozen = isCopyingFrozen;
		return this;
	}

	public String getSettledTimeFormatted() {
		return settledTimeFormatted;
	}

	public String getFirstName() {
		return firstName;
	}

	public Update setFirstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public String getLastName() {
		return lastName;
	}

	public Update setLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public boolean isCoinsButton() {
		return isCoinsButton;
	}

	public Update setCoinsButton(boolean isCoinsButton) {
		this.isCoinsButton = isCoinsButton;
		return this;
	}

	public int getPromotionIconResId() {
		return promotionIconResId;
	}

	public void setPromotionIconResId(int promotionIconResId) {
		this.promotionIconResId = promotionIconResId;
	}

	public String getOldNickname() {
		return oldNickname;
	}

	public void setOldNickname(String oldNickname) {
		this.oldNickname = oldNickname;
	}

	public String getFullName() {
		return getFirstName() + " " + getLastName();
	}

	public long getLastInvestTime() {
		return lastInvestTime;
	}

	public void setLastInvestTime(long lastInvestTime) {
		this.lastInvestTime = COTimeUtils.getFixedTime(lastInvestTime);
	}

	public String getLastInvestTimeFormatted() {
		return lastInvestTimeFormatted;
	}

	public boolean hasNicknameInBody() {
		return hasNicknameInBody;
	}

	public void setHasNicknameInBody(boolean hasNicknameInBody) {
		this.hasNicknameInBody = hasNicknameInBody;
	}

	public boolean isShared() {
		return isShared;
	}

	public void setShared(boolean isShared) {
		this.isShared = isShared;
	}
}
