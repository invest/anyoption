package com.copyop.android.app.popup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.anyoption.android.R;
import com.anyoption.android.app.popup.LowWithdrawDialogFragment;
import com.anyoption.android.app.popup.WithdrawSurveyDialogFragment;
import com.anyoption.android.app.util.ScreenUtils;

/**
 * A POPUP that is shown when the user attempts to make a withdrawal that is lower than a MIN amount. 
 * @author Anastas Arnaudov
 *
 */
public class COWithdrawSurveyDialogFragment extends WithdrawSurveyDialogFragment {

	public static final String TAG = COWithdrawSurveyDialogFragment.class.getSimpleName();
	/**
	 * Use this method to create new instance of the fragment.
	 * @return
	 */
	public static COWithdrawSurveyDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new COWithdrawSurveyDialogFragment.");
		COWithdrawSurveyDialogFragment fragment = new COWithdrawSurveyDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, true);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.black_alpha_40);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	protected void setupOtherAnswerEditTextFocus(View contentView) {
		super.setupOtherAnswerEditTextFocus(contentView);
		contentView.getRootView().setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ScreenUtils.hideKeyboard(fourthAnswerEditText);
			}
		});
	}
}
