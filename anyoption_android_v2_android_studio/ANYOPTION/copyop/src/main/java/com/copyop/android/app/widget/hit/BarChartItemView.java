package com.copyop.android.app.widget.hit;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.widget.TextView;
import com.copyop.android.app.R;

import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;

public class BarChartItemView extends RelativeLayout{

	public final String TAG = BarChartItemView.class.getSimpleName();
	
	private View rootView;
	private View chartLayout;
	private TextView valueInsideTextView;
	private TextView valueOutsideTextView;
	private TextView rangeTextView;

	// ANIMATION PARAMETERS:
	private final long DURATION		= 1500l;
	private final long START_DELAY 	= 200l;
	///////////////////////
	
	private float value = 0f;
	private String rangeFormatted = "";

	
	public BarChartItemView(Context context) {
		super(context);
		init(context, null, 0);
	}
	
	public BarChartItemView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	
	public BarChartItemView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs, defStyleAttr);
	}

	private void init(Context context, AttributeSet attrs, int defStyleAttr) {
		rootView = View.inflate(context, R.layout.bar_chart_item_layout, this);
		chartLayout = rootView.findViewById(R.id.bar_chart_chart_layout);
		valueOutsideTextView = (TextView) rootView.findViewById(R.id.bar_chart_value_outside_text_view);
		valueInsideTextView = (TextView) rootView.findViewById(R.id.bar_chart_value_inside_text_view);
		rangeTextView = (TextView) rootView.findViewById(R.id.bar_chart_range_text_view);
		
		if(!isInEditMode()){
			//INIT:
			ViewGroup.LayoutParams layoutParams = valueInsideTextView.getLayoutParams();
			layoutParams.height = 0;
			valueInsideTextView.setLayoutParams(layoutParams);
			valueOutsideTextView.setVisibility(View.GONE);
			rangeTextView.setText(rangeFormatted);
		}
	}

	public void startAnimation(){
		ViewGroup.LayoutParams layoutParams = valueInsideTextView.getLayoutParams();
		layoutParams.height = 0;
		valueInsideTextView.setLayoutParams(layoutParams);
		valueInsideTextView.invalidate();
		float newHeight = chartLayout.getHeight() * value / 100;
		if(newHeight < ScreenUtils.convertDpToPx(Application.get(), 30)){//TODO
			valueOutsideTextView.setVisibility(View.VISIBLE);
			valueOutsideTextView.setText(String.valueOf((int)value) + "%");
			valueInsideTextView.setText(null);
		}else{
			valueOutsideTextView.setVisibility(View.GONE);
			valueInsideTextView.setText(String.valueOf((int)value) + "%");
		}
		ValueAnimator animator = ValueAnimator.ofInt(0, (int) newHeight);
		animator.setDuration(DURATION);
		animator.setInterpolator(new LinearInterpolator());
		animator.setStartDelay(START_DELAY);
		animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator valueAnimator) {
				// Update Height
				int newHeight = (Integer) valueAnimator.getAnimatedValue();
				ViewGroup.LayoutParams layoutParams = valueInsideTextView.getLayoutParams();
				layoutParams.height = newHeight;
				valueInsideTextView.setLayoutParams(layoutParams);
			}
		});
		animator.start();

	}

	/**
	 * Must be between 0 and 100
	 * @param value
	 */
	public void setValue(float value) {
		if(value < 0){
			Log.e(TAG, "Passed value is < 0. Setting the value to 0.");
			this.value = 0;
		}else if(value > 100){
			Log.e(TAG, "Passed value is > 100. Setting the value to 100.");
			this.value = 100;
		}else{
			this.value = value;			
		}
	}
	
	/**
	 * Must be (start < end) and (start > 0) and (end > 0).
	 * @param start
	 * @param end
	 */
	public void setRange(int start, int end){
		if(start < 0 || end < 0){
			Log.e(TAG, "Passed range has wrong negative values. Setting the range to \"\"");
			this.rangeFormatted = "";
		}else if(start > end){
			Log.e(TAG, "Passed range has start value greater then the edn value. Setting the range to \"\"");
			this.rangeFormatted = "";
		}else{
			this.rangeFormatted = start + "-" + end;
		}
		
		if(rangeTextView != null){
			rangeTextView.setText(rangeFormatted);
		}
	}
}
