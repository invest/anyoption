package com.copyop.android.app.manager;

import android.util.Log;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.DepositFailEvent;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.manager.AppseeManager;
import com.appsee.Appsee;
import com.copyop.android.app.COApplication;

/**
 * @author Anastas Arnaudov
 */
public class COAppseeManager extends AppseeManager {

    public static final String TAG = COAppseeManager.class.getSimpleName();

    private static final String EVENT_DEPOSIT 				= "Deposit";
    private static final String EVENT_MY_ACCOUNT	 		= "My Account";
    private static final String EVENT_MY_OPTIONS	 		= "My options";
    private static final String EVENT_SUPPORT		 		= "Support";
    private static final String EVENT_SOCIAL_FEED_EVENT		= "Social Feed";
    private static final String EVENT_MY_PROFILE_EVENT		= "My Profile";
    private static final String EVENT_MY_COINS_EVENT		= "copyop coins";
    private static final String EVENT_FACEBOOK_EVENT        = "Facebook";
    private static final String EVENT_FACEBOOK_FINISH_EVENT = "facebook finish sign up";
    private static final String EVENT_NEW_EVENT             = "FEED";
    private static final String EVENT_HOT_EVENT             = "HOT";
    private static final String EVENT_EXPLORE_EVENT         = "EXPLORE";
    private static final String EVENT_OTHER_PROFILE_EVENT   = "Other user profile";
    private static final String EVENT_COPY_POPUP_EVENT      = "Copy";
    private static final String EVENT_WATCH_POPUP_EVENT     = "Watch";
    private static final String EVENT_BUY_NOW_POPUP_EVENT   = "Buy Now";
    private static final String EVENT_REGISTER 				= "Register";
    private static final String EVENT_LOGIN 				= "Login";

    public COAppseeManager(){
        super();
        Log.d(TAG, "On Create");
        Application.get().registerForEvents(this, NavigationEvent.class, DepositFailEvent.class);
    }

    public void onBuyNowPopup(){
        Log.d(TAG, "Sending Buy Now Popup event.");
        Appsee.addEvent(EVENT_BUY_NOW_POPUP_EVENT);
    }

    public void onCopyPopup(){
        Log.d(TAG, "Sending Copy Popup event.");
        Appsee.addEvent(EVENT_COPY_POPUP_EVENT);
    }

    public void onWatchPopup(){
        Log.d(TAG, "Sending Watch Popup event.");
        Appsee.addEvent(EVENT_WATCH_POPUP_EVENT);
    }

    public void onProfile(boolean isMyProfile){
        if(isMyProfile){
            Log.d(TAG, "Sending My Profile Screen event.");
            Appsee.addEvent(EVENT_MY_PROFILE_EVENT);
        }else{
            Log.d(TAG, "Sending Other Profile Screen event.");
            Appsee.addEvent(EVENT_OTHER_PROFILE_EVENT);
        }
    }

    public void onNews(){
        Log.d(TAG, "Sending News Screen event.");
        Appsee.addEvent(EVENT_NEW_EVENT);
    }

    public void onHot(){
        Log.d(TAG, "Sending Hot Screen event.");
        Appsee.addEvent(EVENT_HOT_EVENT);
    }

    public void onExplore(){
        Log.d(TAG, "Sending Explore Screen event.");
        Appsee.addEvent(EVENT_EXPLORE_EVENT);
    }

    public void onEvent(NavigationEvent event) {
        Application.Screenable screen = event.getToScreen();
        if(screen == COApplication.COScreen.DEPOSIT_MENU){
            Log.d(TAG, "Sending Deposit Screen event.");
            Appsee.addEvent(EVENT_DEPOSIT);
            Appsee.pause();
        }else {
            Appsee.resume();
            if (screen == Application.Screen.MY_ACCOUNT) {
                Log.d(TAG, "Sending My Account Screen event.");
                Appsee.addEvent(EVENT_MY_ACCOUNT);
            } else if (screen == Application.Screen.MY_OPTIONS) {
                Log.d(TAG, "Sending My Options Screen event.");
                Appsee.addEvent(EVENT_MY_OPTIONS);
            } else if (screen == Application.Screen.SUPPORT) {
                Log.d(TAG, "Sending Support Screen event.");
                Appsee.addEvent(EVENT_SUPPORT);
            } else if (screen == COApplication.COScreen.NEWS_HOT_EXPLORE) {
                Log.d(TAG, "Sending Social Feed Screen event.");
                Appsee.addEvent(EVENT_SOCIAL_FEED_EVENT);
            }else if (screen == COApplication.COScreen.COPYOP_COINS) {
                Log.d(TAG, "Sending copyop coins event.");
                Appsee.addEvent(EVENT_MY_COINS_EVENT);
            } else if (screen == COApplication.COScreen.FACEBOOK_CONNECTED || screen == COApplication.COScreen.FACEBOOK_NOT_CONNECTED) {
                Log.d(TAG, "Sending facebook event.");
                Appsee.addEvent(EVENT_FACEBOOK_EVENT);
            } else if (screen == COApplication.COScreen.LSS_SIGNUP_FINISH) {
                Log.d(TAG, "Sending Facebook Finish event.");
                Appsee.addEvent(EVENT_FACEBOOK_FINISH_EVENT);
            } else if(screen == COApplication.COScreen.LSS_SIGNUP || screen == COApplication.COScreen.LSS_SIGNUP_EMPTY) {
                Log.d(TAG, "Sending Register Screen event.");
                Appsee.addEvent(EVENT_REGISTER);
            } else if(screen == COApplication.COScreen.LSS_LOGIN || screen == COApplication.COScreen.LSS_LOGIN_SIGNUP) {
                Log.d(TAG, "Sending Login Screen event.");
                Appsee.addEvent(EVENT_LOGIN);
            }
        }
    }

}
