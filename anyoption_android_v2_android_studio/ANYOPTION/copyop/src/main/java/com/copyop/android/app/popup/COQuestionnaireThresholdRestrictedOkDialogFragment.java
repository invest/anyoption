package com.copyop.android.app.popup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.anyoption.android.R;
import com.anyoption.android.app.popup.QuestionnairePEPBlockedOkDialogFragment;
import com.anyoption.android.app.popup.QuestionnaireThresholdRestrictedOkDialogFragment;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;

public class COQuestionnaireThresholdRestrictedOkDialogFragment extends QuestionnaireThresholdRestrictedOkDialogFragment {

    public static final String TAG = COQuestionnaireThresholdRestrictedOkDialogFragment.class.getSimpleName();

    public static COQuestionnaireThresholdRestrictedOkDialogFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new COQuestionnaireThresholdRestrictedOkDialogFragment.");
        COQuestionnaireThresholdRestrictedOkDialogFragment fragment = new COQuestionnaireThresholdRestrictedOkDialogFragment();
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, true);
        bundle.putInt(ARG_BACKGROUND_COLOR_KEY, R.color.black_alpha_40);
        bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            setCancelable(args.getBoolean(Constants.EXTRA_CANCELABLE, false));
        }
    }

    @Override
    protected void onCreateContentView(View contentView) {
        super.onCreateContentView(contentView);
    }

    @Override
    protected void setupCloseImage(View contentView) {
        //DOESNT NEEDED HERE ONLY IN ANYOPTION
    }
}
