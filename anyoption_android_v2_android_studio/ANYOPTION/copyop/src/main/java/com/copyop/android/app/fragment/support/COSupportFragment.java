package com.copyop.android.app.fragment.support;

import com.anyoption.android.app.fragment.support.SupportFragment;

import android.os.Bundle;

public class COSupportFragment extends SupportFragment {

	/**
	 * Use this method to get new instance of the fragment.
	 * 
	 * @return
	 */
	public static COSupportFragment newInstance(Bundle bundle) {
		COSupportFragment supportFragment = new COSupportFragment();
		supportFragment.setArguments(bundle);
		return supportFragment;
	}

	@Override
	protected void setupCallFields() {
		// Call us option is not available for CopyOp.
	}

}
