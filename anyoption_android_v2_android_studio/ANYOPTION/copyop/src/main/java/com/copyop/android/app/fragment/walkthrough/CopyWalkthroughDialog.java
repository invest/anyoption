package com.copyop.android.app.fragment.walkthrough;

import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.TextUtils;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COConstants;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

public class CopyWalkthroughDialog extends BaseDialogFragment implements OnClickListener {

	private static final String TAG = CopyWalkthroughDialog.class.getSimpleName();

	private static final String TITLE_ARG_KEY = "title_arg_key";

	private String titleText;
	private boolean isCopying;

	private TextView titleView;
	private View uncopyView;
	private View howManyArrow;

	private View gotIt;

	private OnCopyWalkthroughDialogDismissListener onCopyWalkthroughDialogDismissListener;

	public interface OnCopyWalkthroughDialogDismissListener {
		public void onCopyWalkthroughDialogDismiss();
	}

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @param titleText
	 * @param isCopying
	 * @return
	 */
	public static CopyWalkthroughDialog newInstance(String titleText, boolean isCopying) {
		Bundle args = new Bundle();
		args.putString(TITLE_ARG_KEY, titleText);
		args.putBoolean(COConstants.EXTRA_IS_COPYING, isCopying);

		return newInstance(args);
	}

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static CopyWalkthroughDialog newInstance(Bundle bundle) {
		Log.d(TAG, "Created new CopyWalkthroughDialog.");
		CopyWalkthroughDialog fragment = new CopyWalkthroughDialog();
		bundle.putInt(ARG_BACKGROUND_COLOR_KEY, 0);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		fragment.setArguments(bundle);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		if (args != null) {
			isCopying = args.getBoolean(COConstants.EXTRA_IS_COPYING, false);
			titleText = args.getString(TITLE_ARG_KEY, null);
		}

		if (titleText == null) {
			Log.e(TAG, "Can not create copy walkthrough dialog without titleText");
			dismiss();
		}

	}

	@Override
	protected void onCreateContentView(View contentView) {
		super.onCreateContentView(contentView);

		titleView = (TextView) contentView.findViewById(R.id.copy_walkthrough_title);
		titleView.setText(titleText);

		uncopyView = contentView.findViewById(R.id.copy_walkthrough_uncopy);
		howManyArrow = contentView.findViewById(R.id.copy_walkthrough_how_many_arrow);

		gotIt = contentView.findViewById(R.id.copy_walkthrough_got_it);
		gotIt.setOnClickListener(this);

		// Underline the beginning of the text
		TextView noChainView = (TextView) contentView
				.findViewById(R.id.copy_walkthrough_no_chain_comment);
		String underlinedText = getString(R.string.copyWalkthroughCommentNoChainUnderlined);
		TextUtils.underlineInnerText(noChainView, null, noChainView.getText().toString(),
				underlinedText);

		setUpUncopy();
	}

	@Override
	protected int getContentLayout() {
		return R.layout.copy_walkthrough_dialog;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == gotIt.getId()) {
			finishWalkthrough();
		}
	}

	private void setUpUncopy() {
		if (isCopying) {
			uncopyView.setVisibility(View.VISIBLE);
			RelativeLayout.LayoutParams params = (LayoutParams) howManyArrow.getLayoutParams();
			int bottomMargin = (int) ScreenUtils.convertDpToPx(getActivity(), -50f);
			params.setMargins(params.leftMargin, params.topMargin, params.rightMargin, bottomMargin);
			howManyArrow.setLayoutParams(params);
		}
	}

	private void finishWalkthrough() {
		application.getSharedPreferencesManager().putBoolean(
				COConstants.PREFERENCES_IS_COPY_WALKTHROUGH_SHOWN, true);

		dismiss();
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		if(onCopyWalkthroughDialogDismissListener != null) {
			onCopyWalkthroughDialogDismissListener.onCopyWalkthroughDialogDismiss();
		}
		super.onDismiss(dialog);
	}

	public OnCopyWalkthroughDialogDismissListener getOnCopyWalkthroughDialogDismissListener() {
		return onCopyWalkthroughDialogDismissListener;
	}

	public void setOnCopyWalkthroughDialogDismissListener(OnCopyWalkthroughDialogDismissListener onCopyWalkthroughDialogDismissListener) {
		this.onCopyWalkthroughDialogDismissListener = onCopyWalkthroughDialogDismissListener;
	}
}
