package com.copyop.android.app.popup;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.LoginLogoutEvent.Type;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.form.FormCheckBox;
import com.anyoption.android.app.widget.form.FormEditText;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.event.AcceptTermsAndConditionsEvent;
import com.copyop.android.app.manager.COLoginManager;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class LSSLoginDialogFragment extends BaseDialogFragment {

	private static final String TAG = LSSLoginDialogFragment.class.getSimpleName();
	
	private boolean navigateToAcceptTermsAndConditions;

//	private Form form;
	private COLoginManager loginManager;
	
	private NavigationEvent nextScreenEvent;

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static LSSLoginDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new LSSLoginDialogFragment.");
		if (null == bundle) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);

		LSSLoginDialogFragment fragment = new LSSLoginDialogFragment();
		fragment.setNavigateToAcceptTermsAndConditions(false);
		fragment.setArguments(bundle);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		nextScreenEvent = null;
	}

	/**
	 * Set up content.
	 * 
	 * @param contentView
	 */
	@Override
	protected void onCreateContentView(View contentView) {
		boolean isTablet = getResources().getBoolean(R.bool.isTablet);
		FormEditText emailField = (FormEditText) contentView.findViewById(R.id.lss_email_edittext);
		FormEditText passwordField = (FormEditText) contentView.findViewById(R.id.lss_pass_edittext);
		RequestButton loginButton = (RequestButton) contentView.findViewById(R.id.lss_ao_login_button);
		TextView forgotPassTextView = (TextView) contentView.findViewById(R.id.lss_forgot_password);
		FormCheckBox rememberPasswordCheckBox = (FormCheckBox) contentView.findViewById(R.id.lss_rememeber_password);
		
		loginManager = new COLoginManager();
		loginManager.onCreateView(this, isTablet, emailField, passwordField, forgotPassTextView, loginButton, rememberPasswordCheckBox);
		
		View backButton = contentView.findViewById(R.id.lss_back_button);
		backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				nextScreenEvent = new NavigationEvent(COScreen.LSS_SIGNUP, NavigationType.INIT);
			}
		});
		
		forgotPassTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				nextScreenEvent = new NavigationEvent(Screen.FORGOT_PASSWORD, NavigationType.DEEP);
			}
		});
		
		View supportButton = contentView.findViewById(R.id.lss_support);
		supportButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				nextScreenEvent = new NavigationEvent(Screen.SUPPORT, NavigationType.DEEP);
			}
		});
		
		application.registerForEvents(this, LoginLogoutEvent.class);
		application.registerForEvents(this, AcceptTermsAndConditionsEvent.class);
	}
	
	@Override
	public void onDestroyView() {
		application.unregisterForAllEvents(this);
		super.onDestroyView();
	}
	
	public void onEventMainThread(LoginLogoutEvent event){
		if(event.getType() == Type.LOGIN){
			dismiss();
		}
	}
	
	public void onEventMainThread(AcceptTermsAndConditionsEvent event) {
		setNavigateToAcceptTermsAndConditions(true);
		dismiss();
	}
	
	@Override
	protected int getContentLayout() {
		return R.layout.lss_login_dialog;
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		if (nextScreenEvent != null) {
			application.postEvent(nextScreenEvent);
		} else if (!application.isLoggedIn() && !isNavigateToAcceptTermsAndConditions()) {
			application.postEvent(new NavigationEvent(COScreen.LSS_SIGNUP,
					NavigationType.HORIZONTAL));
		}

		super.onDismiss(dialog);
	}

	public boolean isNavigateToAcceptTermsAndConditions() {
		return navigateToAcceptTermsAndConditions;
	}

	public void setNavigateToAcceptTermsAndConditions(
			boolean navigateToAcceptTermsAndConditions) {
		this.navigateToAcceptTermsAndConditions = navigateToAcceptTermsAndConditions;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d(TAG, "onActivityResult requestCode: " + requestCode + " resultCode: " + resultCode + " data: " + data);
		super.onActivityResult(requestCode, resultCode, data);
		application.getFacebookManager().onActivityResult(requestCode,resultCode,data);
		if (resultCode != Activity.RESULT_OK) {
			loginManager.finishLoginCallback();
		}
	}
}