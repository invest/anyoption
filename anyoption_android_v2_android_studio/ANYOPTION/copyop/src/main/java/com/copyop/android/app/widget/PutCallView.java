package com.copyop.android.app.widget;

import com.copyop.android.app.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * View showing put or call icon with animation.
 * 
 * @author Mario Kutlev
 */
public class PutCallView extends RelativeLayout {

	private RelativeLayout rootView;
	private ImageView animatedView;

	private boolean isPut;

	private Animation firstAnimation;
	private Animation secondAnimation;

	public PutCallView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);

		init(context);
	}

	public PutCallView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public PutCallView(Context context) {
		this(context, null);
	}

	private void init(Context context) {
		if (isInEditMode()) {
			return;
		}

		rootView = (RelativeLayout) View.inflate(context, R.layout.put_call_view, this);
		animatedView = (ImageView) rootView.findViewById(R.id.put_call_animated_icon);

		isPut = false;
	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();

		if (!isInEditMode()) {
			startPutCallIconAnimation();
		}
	}

	/**
	 * Returns true if the icon is put and false if it is call.
	 * 
	 * @return
	 */
	public boolean isPut() {
		return isPut;
	}

	/**
	 * Changes the put/call icon
	 * 
	 * @param isPut
	 *            if true, the icon is put, else it is call.
	 */
	public void setPut(boolean isPut) {
		if (this.isPut == isPut) {
			// We don't need to make any changes.
			return;
		}

		this.isPut = isPut;

		if (isPut) {
			animatedView.setImageResource(R.drawable.put_call_view_put);
		} else {
			animatedView.setImageResource(R.drawable.put_call_view_call);
		}
	}

	private void startPutCallIconAnimation() {
		if (firstAnimation != null) {
			firstAnimation.cancel();
		}

		if (secondAnimation != null) {
			secondAnimation.cancel();
		}

		if (isPut) {
			// Put animation
			firstAnimation = AnimationUtils.loadAnimation(getContext(),
					R.anim.slide_top_bottom_repeat_part_1);
			secondAnimation = AnimationUtils.loadAnimation(getContext(),
					R.anim.slide_top_bottom_repeat_part_2);
		} else {
			// Call animation
			firstAnimation = AnimationUtils.loadAnimation(getContext(),
					R.anim.slide_bottom_top_repeat_part_1);
			secondAnimation = AnimationUtils.loadAnimation(getContext(),
					R.anim.slide_bottom_top_repeat_part_2);
		}

		firstAnimation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// Not used
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// Not used
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				animatedView.startAnimation(secondAnimation);
			}
		});

		secondAnimation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// Not used
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// Not used
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				animatedView.startAnimation(firstAnimation);
			}
		});

		animatedView.startAnimation(firstAnimation);
	}

}