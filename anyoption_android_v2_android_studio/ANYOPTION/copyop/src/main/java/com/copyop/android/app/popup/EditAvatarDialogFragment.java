package com.copyop.android.app.popup;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.OnActivityResultListener;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.copyop.android.app.R;
import com.copyop.android.app.util.AvatarUtils;
import com.copyop.android.app.util.COBitmapUtils;
import com.copyop.android.app.util.COConstants;
import com.copyop.android.app.util.COUtils;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Target;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class EditAvatarDialogFragment extends BaseDialogFragment implements OnActivityResultListener{
	private final String TAG = EditAvatarDialogFragment.class.getSimpleName();
	
	private final int avatarWidth	 = 304;
	private final int avatarHeight	 = 304;
	
	public interface EditAvatarDialogListener {
		public void onUpload(Bitmap bitmap);
		public void onDrawMeMaleAvatar(String avatarURL);
		public void onDrawMeFemaleAvatar(String avatarURL);
	}
	
	private EditAvatarDialogListener listener;
	protected String avatarURL;
	protected Bitmap bitmap;

	private Uri uri;

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static EditAvatarDialogFragment newInstance(Bundle bundle) {
		Log.d(EditAvatarDialogFragment.class.getSimpleName(), "Created new EditAvatarDialogFragment.");
		if (null == bundle) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		EditAvatarDialogFragment fragment = new EditAvatarDialogFragment();
		fragment.setArguments(bundle);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Application.get().addOnActivityResultListener(this);
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void onCreateContentView(View contentView) {
		View takePictureButton = contentView.findViewById(R.id.edit_avatar_take_picture_button);
		if(COUtils.isCameraHardware()){
			takePictureButton.setVisibility(View.VISIBLE);
			takePictureButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					COUtils.openCamera(COConstants.REQUEST_CODE_TAKE_PHOTO);
				}
			});
		}else{
			takePictureButton.setVisibility(View.GONE);
		}
		
		View upload = contentView.findViewById(R.id.edit_avatar_upload_button);
		upload.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				COUtils.openImageGallery(COConstants.REQUEST_CODE_PICK_FROM_GALLERY);
			}
		});

		View male = contentView.findViewById(R.id.edit_avatar_male_button);
		male.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				avatarURL = AvatarUtils.generateRandomMaleAvatar();
				dismiss();
				if (null != listener) {
					listener.onDrawMeMaleAvatar(avatarURL);
				}
			}
		});

		View female = contentView.findViewById(R.id.edit_avatar_female_button);
		female.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				avatarURL = AvatarUtils.generateRandomFemaleAvatar();
				dismiss();
				if (null != listener) {
					listener.onDrawMeFemaleAvatar(avatarURL);
				}
			}
		});

		View cancel = contentView.findViewById(R.id.edit_avatar_cancel_button);
		cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});

	}

	@Override
	public void onDestroy() {
		Application.get().removeOnActivityResultListener(this);
		super.onDestroy();
	}
	
	@Override
	protected int getContentLayout() {
		return R.layout.edit_avatar_dialog;
	}
	
	public EditAvatarDialogListener getListener() {
		return listener;
	}

	public void setListener(EditAvatarDialogListener listener) {
		this.listener = listener;
	}

	@Override
	public void onActivityResultEvent(int requestCode, int resultCode, Intent intent) {
		switch (requestCode) {
			case COConstants.REQUEST_CODE_PICK_FROM_GALLERY:
				Log.d(TAG, "REQUEST_CODE_PICK_FROM_GALLERY");
				if (resultCode == Activity.RESULT_OK) {
					Log.d(TAG, "RESULT_OK");
					uri = intent.getData();
					if(COUtils.isCropSupported()){
						COUtils.openCropImage(uri, avatarWidth, avatarHeight, COConstants.REQUEST_CODE_CROP_IMAGE);
					}else{
						getBitmapFromUri(uri);
					}
				}
				break;
			case COConstants.REQUEST_CODE_TAKE_PHOTO:
				Log.d(TAG, "REQUEST_CODE_TAKE_PHOTO");
				if (resultCode == Activity.RESULT_OK) {
					Log.d(TAG, "RESULT_OK");
					uri = COUtils.saveImageToGallery(COUtils.getTempImageFile());
					if(COUtils.isCropSupported()){
						COUtils.openCropImage(uri, avatarWidth, avatarHeight, COConstants.REQUEST_CODE_CROP_IMAGE);
					}else{
						getBitmapFromUri(uri);
					}
				}
				break;
	
			case COConstants.REQUEST_CODE_CROP_IMAGE:
				Log.d(TAG, "REQUEST_CODE_CROP_IMAGE");
				if (resultCode == Activity.RESULT_OK) {
					Log.d(TAG, "RESULT_OK");
					bitmap = (Bitmap) intent.getExtras().get("data");
					if (null != listener) {
						listener.onUpload(bitmap);
						dismiss();
					}
				}
				break;
		}
	}
	
	private void getBitmapFromUri(Uri uri){
		COBitmapUtils.loadBitmap(uri.toString(), avatarWidth, avatarHeight, new Target() {
			@Override
			public void onPrepareLoad(Drawable arg0) {}
			@Override
			public void onBitmapLoaded(Bitmap b, LoadedFrom arg1) {
				bitmap = b;
				if (null != listener) {
					listener.onUpload(bitmap);
					dismiss();
				}
			}
			@Override
			public void onBitmapFailed(Drawable arg0) {}
		});
	}
}