package com.copyop.android.app.util.adapter;

import java.util.Comparator;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.widget.TextView;
import com.copyop.android.app.R;
import com.copyop.android.app.model.HotAssetSpecialist;
import com.copyop.android.app.model.HotCopier;
import com.copyop.android.app.model.HotGroup;
import com.copyop.android.app.model.HotGroup.Type;
import com.copyop.android.app.model.HotItem;
import com.copyop.android.app.model.HotTrade;
import com.copyop.android.app.model.HotTrader;
import com.copyop.android.app.util.adapter.HotAdapter.ChildViewHolder;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

/**
 * Custom {@link BaseAdapter} for the list of a {@link HotItem} from a given {@link HotGroup}
 * @author Anastas Arnaudov
 *
 */
public class HotAllAdapter extends BaseAdapter implements OnClickListener {

	public final String TAG = HotAllAdapter.class.getSimpleName();
	
	private HotGroup hotGroup;
	private LayoutInflater inflater;
	
	public HotAllAdapter(HotGroup hotGroup){
		inflater = (LayoutInflater) Application.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.hotGroup = hotGroup;
	}

	@Override
	public int getCount() {
		int itemsCount = hotGroup.getItems().size();
		if (hotGroup.getType() == Type.COPIERS && itemsCount != 0) {
			// Add one more child for the footer of best copiers.
			itemsCount++;
		}

		return itemsCount;
	}

	@Override
	public Object getItem(int position) {
		return hotGroup.getItems().get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (hotGroup.getType() == Type.COPIERS && position == getCount() - 1) {
			return getCopiersFooter(convertView, parent);
		}

		ChildViewHolder childViewHolder;
		// If the tag is null, this is the footer view and the layout must be recreated.
		if(convertView == null || convertView.getTag() == null){
			convertView = inflater.inflate(R.layout.hot_item, parent, false);
			childViewHolder = new ChildViewHolder();
			childViewHolder.borderTop = convertView.findViewById(R.id.hot_item_border_top);
			childViewHolder.borderBottom = convertView.findViewById(R.id.hot_item_border_bottom);
			childViewHolder.numberTextView = (TextView) convertView.findViewById(R.id.hot_item_number_text_view);
			childViewHolder.avatarImageView = (ImageView) convertView.findViewById(R.id.hot_item_icon_image_view);
			childViewHolder.nicknameTextView =  (TextView) convertView.findViewById(R.id.hot_item_nickname_text_view);
			childViewHolder.hitOrMarketTextView =  (TextView) convertView.findViewById(R.id.hot_item_hit_or_market_text_view);
			childViewHolder.profitTextView =  (TextView) convertView.findViewById(R.id.hot_item_profit_text_view);
			childViewHolder.copiersOrTimeTextView =  (TextView) convertView.findViewById(R.id.hot_item_copiers_or_time_text_view);
			childViewHolder.lastTwoColumnsLayout = convertView.findViewById(R.id.hot_item_last_2_columns);
			childViewHolder.copyingButton = convertView.findViewById(R.id.hot_item_copying_button);
			childViewHolder.copyButton = convertView.findViewById(R.id.hot_item_asset_specialist_copy_button_layout);
			childViewHolder.lastSeparator = convertView.findViewById(R.id.last_hot_item_separator);
			convertView.setTag(childViewHolder);
		}else{
			childViewHolder = (ChildViewHolder) convertView.getTag();
		}
		
		childViewHolder.position = position;
		HotItem item = hotGroup.getItems().get(position);
		
		try {
			HotAdapter.setupChild(Application.get(), hotGroup, item, childViewHolder, this);
		} catch (Exception e) {
			Log.e(TAG, "getChildView", e);
		}

		return convertView;
	}
	
	private View getCopiersFooter(View convertView, ViewGroup parent) {
		// Check if the view is layout for copier
		if (convertView == null || convertView.getTag() != null) {
			TextView footerView = (TextView) inflater.inflate(R.layout.hot_best_copiers_footer, parent, false);
			String boldText = Application.get().getString(R.string.hotNoChainOfCopiedOptionsBold);
			TextUtils.makeClickableInnerText(footerView, null, footerView.getText().toString(), boldText, Font.ROBOTO_BOLD, 0, 0, true, null);

			return footerView;
		}

		return convertView;
	}

	/**
	 * Custom {@link Comparator} that compares different fields of the {@link HotItem}.
	 * @author Anastas Arnaudov
	 */
	public static class HotItemComparator implements Comparator<HotItem>{
		
		public final String TAG = HotItemComparator.class.getSimpleName();
		
		public enum SortField{
			PROFIT, HIT, MARKET, COPIERS, TIME;
		}
		
		public enum SortOrder{
			BIG_TO_SMALL, SMALL_TO_BIG;
		}
		
		private SortField sortField;
		private SortOrder sortOrder;
		
		public HotItemComparator(SortField sortField, SortOrder sortOrder){
			super();
			this.sortField = sortField;
			this.sortOrder = sortOrder;
		}
		
		@Override
		public int compare(HotItem item1, HotItem item2) {
			int result = 0;
			switch (sortField) {
				case PROFIT:
					result = (int) (item2.getProfit() - item1.getProfit());
					break;
				case COPIERS:
					if(item1 instanceof HotTrader && item2 instanceof HotTrader){
						result = (int) (((HotTrader)item2).getCopiersCount() - ((HotTrader)item1).getCopiersCount());
					}
					break;
				case HIT:
					if(item1 instanceof HotTrader && item2 instanceof HotTrader){
						result = (int) (((HotTrader)item2).getHitPercent()- ((HotTrader)item1).getHitPercent());
					}
					break;
				case MARKET:
					if(item1 instanceof HotTrade && item2 instanceof HotTrade){
						try {
							result = ((HotTrade) item1).getMarketName().compareToIgnoreCase(((HotTrade) item2).getMarketName());
						} catch (NullPointerException e) {
							Log.e(TAG, "Could NOT compare Markets names.", e);
						}
					} else if(item1 instanceof HotAssetSpecialist && item2 instanceof HotAssetSpecialist){
						try {
							result = ((HotAssetSpecialist) item1).getMarketName().compareToIgnoreCase(((HotAssetSpecialist) item2).getMarketName());
						} catch (NullPointerException e) {
							Log.e(TAG, "Could NOT compare Markets names.", e);
						}
					}
					break;
				case TIME:
					if(item1 instanceof HotTrade && item2 instanceof HotTrade){
						result = (int) (((HotTrade) item2).getTime() - ((HotTrade) item1).getTime());
					}
					break;
				default:
					return 0;
			}
			
			return (sortOrder == SortOrder.BIG_TO_SMALL) ? result : -result;
		}
	}

	@Override
	public void onClick(View view) {
		if(view.getTag() instanceof ChildViewHolder){
			ChildViewHolder childViewHolder = (ChildViewHolder) view.getTag();
			HotItem item = hotGroup.getItems().get(childViewHolder.position);
			if(view.getId() == childViewHolder.avatarImageView.getId() || view.getId() == childViewHolder.nicknameTextView.getId()){
				HotAdapter.goToProfile(item.getUserId());
			}else if(view.getId() == childViewHolder.hitOrMarketTextView.getId()){
				if(item instanceof HotTrade){
					HotTrade hotTrade = (HotTrade) item;
					HotAdapter.goToMarket(hotTrade.getMarket());
				} else if(item instanceof HotAssetSpecialist){
					HotAssetSpecialist hotAssetSpecialist = (HotAssetSpecialist) item;
					HotAdapter.goToMarket(hotAssetSpecialist.getMarket());
				}
			}else if(view.getId() == childViewHolder.copyingButton.getId()){
				if(item instanceof HotCopier){
					HotCopier hotCopier = (HotCopier) item;
					HotAdapter.goToCopyingList(hotCopier.getUserId());	
				}
			}else if(view.getId() == childViewHolder.copyButton.getId()){
				if(item instanceof HotAssetSpecialist){
					HotAssetSpecialist hotAssetSpecialist = (HotAssetSpecialist) item;
					HotAdapter.showCopy(hotAssetSpecialist);
				}
			}
		}
	}

}

