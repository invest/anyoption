package com.copyop.android.app.activity;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.activity.MainActivity;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.event.UpdateTitleEvent;
import com.anyoption.android.app.fragment.ActionFragment;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.event.ShowMenuWalkthroughEvent;
import com.copyop.android.app.fragment.CopyopCoinsFragment;
import com.copyop.android.app.fragment.facebook.FacebookConnectedFragment;
import com.copyop.android.app.fragment.facebook.FacebookNotConnectedFragment;
import com.copyop.android.app.fragment.login_register.CORegisterFragment;
import com.copyop.android.app.fragment.login_register.LSSAOFinishFragment;
import com.copyop.android.app.fragment.login_register.LSSLoginFragment;
import com.copyop.android.app.fragment.login_register.LSSSignupFinishFragment;
import com.copyop.android.app.fragment.login_register.LSSSignupFragment;
import com.copyop.android.app.fragment.my_account.ChangeNicknameFragment;
import com.copyop.android.app.fragment.news_feed.HotAllFragment;
import com.copyop.android.app.fragment.news_feed.NewsHotExploreFragment;
import com.copyop.android.app.fragment.profile.UserProfileFragment;
import com.copyop.android.app.fragment.profile.list.CopiersListFragment;
import com.copyop.android.app.fragment.profile.list.CopyingListFragment;
import com.copyop.android.app.fragment.profile.list.WatchersListFragment;
import com.copyop.android.app.fragment.profile.list.WatchingListFragment;
import com.copyop.android.app.fragment.walkthrough.MenuWalkthroughDialog;
import com.copyop.android.app.popup.LSSHomeDialogFragment;
import com.copyop.android.app.popup.LSSLoginDialogFragment;
import com.copyop.android.app.util.COConstants;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnCloseListener;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnOpenedListener;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class COMainActivity extends MainActivity {

	private static final String TAG = COMainActivity.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (slidingMenu != null && isSlidingMenuUsed) {
			final View contentShadowView = findViewById(R.id.menu_content_shadow);

			slidingMenu.setOnOpenedListener(new OnOpenedListener() {

				@Override
				public void onOpened() {
					contentShadowView.setVisibility(View.VISIBLE);
					showMenuWalkthrough();
				}
			});

			slidingMenu.setOnCloseListener(new OnCloseListener() {

				@Override
				public void onClose() {
					contentShadowView.setVisibility(View.GONE);
				}

			});
		}
	}

	private void showMenuWalkthrough() {
		if (!application.getSharedPreferencesManager().getBoolean(COConstants.PREFERENCES_IS_MENU_WALKTHROUGH_SHOWN, false)) {
			application.postEvent(new ShowMenuWalkthroughEvent());
			application.getFragmentManager().showDialogFragment(MenuWalkthroughDialog.class, new Bundle());
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		if(application.isLoggedIn()){
			COApplication.get().getLastSummary();
		}
	}
	
	@Override
	protected void subscribeLightstreamerTables(){
		super.subscribeLightstreamerTables();
		COApplication.get().getLightstreamerManager().subscribeNotificationsTable(lightstreamerManager);
		COApplication.get().getLightstreamerManager().subscribeInboxTable(lightstreamerManager);
	}
	
	@Override
	protected void openInitialScreen(Screenable screen, Bundle bundle) {
		Log.d(TAG, "OPEN INTIAL SCREEN = " + screen);
		if (!isInitialScreenShown) {
			if (screen != null && screen != COScreen.LSS_HOME) {
				super.openInitialScreen(screen, bundle);
			} else {
				isInitialScreenShown = true;
				openLoginScreen();
			}
		}
	}

	@Override
	protected Class<? extends BaseFragment> getFragmentClass(Screenable screen) {
		if (screen == Screen.REGISTER) {
			return LSSSignupFragment.class;
		} else if (screen == Screen.LOGIN) {
			return LSSLoginFragment.class;
		} else if (screen == COScreen.LSS_SIGNUP) {
			return LSSSignupFragment.class;
		} else if (screen == COScreen.LSS_LOGIN_SIGNUP) {
			return LSSLoginFragment.class;
		} else if (screen == COScreen.LSS_SIGNUP_EMPTY) {
			return CORegisterFragment.class;
		} else if (screen == COScreen.LSS_SIGNUP_FINISH) {
			return LSSSignupFinishFragment.class;
		} else if (screen == COScreen.LSS_AO_FINISH) {
			return LSSAOFinishFragment.class;
		} else if (screen == COScreen.CHANGE_NICKNAME) {
			return ChangeNicknameFragment.class;
		} else if (screen == COScreen.FACEBOOK_NOT_CONNECTED) {
			return FacebookNotConnectedFragment.class;
		} else if (screen == COScreen.FACEBOOK_CONNECTED) {
			return FacebookConnectedFragment.class;
		} else if (screen == COScreen.NEWS_HOT_EXPLORE) {
			return NewsHotExploreFragment.class;
		} else if (screen == COScreen.COPYOP_COINS) {
			return CopyopCoinsFragment.class;
		} else if (screen == COScreen.USER_PROFILE) {
			return UserProfileFragment.class;
		} else if (screen == COScreen.COPYING_LIST) {
			return CopyingListFragment.class;
		} else if (screen == COScreen.COPIERS_LIST) {
			return CopiersListFragment.class;
		} else if (screen == COScreen.WATCHING_LIST) {
			return WatchingListFragment.class;
		} else if (screen == COScreen.WATCHERS_LIST) {
			return WatchersListFragment.class;
		}else if(screen == COScreen.HOT_ALL){
			return HotAllFragment.class;
		}

		return super.getFragmentClass(screen);
	}

	@Override
	protected boolean setUpMenuSlide(Screenable screen) {
		boolean enabled = super.setUpMenuSlide(screen);
		
		if(enabled && !application.isLoggedIn()){
			enabled = false;
			closeMenu();
			enableMenuSwipe(enabled);
		}
		
		return enabled;
	}
	
	@Override
	protected boolean checkInitialLoginRegister(){
		return false;
	}
	
	@Override
	public void onBackPressed() {
		
		if(!(application.isLoggedIn() && application.getUser().getIsNeedChangePassword()))     {
		if(currentScreen == COScreen.LSS_LOGIN_SIGNUP || currentScreen == COScreen.LSS_SIGNUP){
			application.getFragmentManager().showDialogFragment(LSSHomeDialogFragment.class, null);
			return;
		}

		if (currentScreen == application.getHomeScreen()) {
			if (application.isLoggedIn()) {
				showLogoutPopUp();
				return;
			}
		} else if (!isTablet && fragmentManager.getStackSize() == 1) {
			if (ActionFragment.isInitialLoginRegister) {
				ActionFragment.isInitialLoginRegister = false;
			}

			if (application.isLoggedIn()) {
				application.postEvent(new NavigationEvent(application.getHomeScreen(),
						NavigationType.INIT));
			} else if (currentScreen == Screen.SUPPORT || currentScreen == Screen.FORGOT_PASSWORD) {
				openLoginScreen();
			} else {
				showLogoutPopUp();
				return;
			}

			return;
		}
		} else {
			finish();
		}
		
		super.onBackPressed();
	}

	private void openLoginScreen() {
		// Removes the screen which is behind the dialog.
		fragmentManager.removeStackItems(fragmentManager.getStackSize());
		fragmentManager.removeAllFragmentsFromView(mainView.getId());
		currentScreen = null;
		application.postEvent(new UpdateTitleEvent(""));

		String username = application.getSharedPreferencesManager()
				.getString(Constants.PREFERENCES_USER_NAME, null);
		if (username == null) {
			application.getFragmentManager().showDialogFragment(LSSHomeDialogFragment.class, null);
		} else {
			application.getFragmentManager().showDialogFragment(LSSLoginDialogFragment.class, null);
		}
	}

}