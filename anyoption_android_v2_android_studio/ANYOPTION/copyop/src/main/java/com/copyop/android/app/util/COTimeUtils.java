package com.copyop.android.app.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.TimeUtils;
import com.copyop.android.app.R;

import android.content.Context;
import android.util.Log;

public class COTimeUtils extends TimeUtils{

	public static final String TAG = COTimeUtils.class.getSimpleName();
	
	public static String formatFromGMT(long timestamp, SimpleDateFormat format){
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(timestamp + getUtcOffset());
		format.setCalendar(calendar);
		return format.format(calendar.getTimeInMillis());
	}
	
	/**
	 * Format the Update Feed Update TIMESTAMP.
	 * @param timestamp
	 * @return
	 */
	public static String formatNewsFeedTime(long timestamp, boolean isFromGMT){
		String result = null;
		Context context = Application.get();
		Calendar now = Calendar.getInstance();
		now.setTimeInMillis(getCurrentServerTime());
		Calendar timeToCheck = Calendar.getInstance();
		timeToCheck.setTimeInMillis(timestamp);
		SimpleDateFormat format = null;

		if(timeToCheck.get(Calendar.YEAR) < now.get(Calendar.YEAR)) {
			//It was not from the current year:
			format = new SimpleDateFormat("dd/MM/yyyy , HH:mm", Application.get().getLocale());
			result = (isFromGMT) ? formatFromGMT(timestamp, format) : format.format(timestamp);
		}else{
			int dayUpdate = timeToCheck.get(Calendar.DAY_OF_YEAR);
			int dayNow = now.get(Calendar.DAY_OF_YEAR);
			
			if(dayUpdate < dayNow){
				if(dayUpdate < dayNow - 1){
					//It was before yesterday:
					format = new SimpleDateFormat("dd/MM , HH:mm", Application.get().getLocale());
					result = (isFromGMT) ? formatFromGMT(timestamp, format) : format.format(timestamp);
				}else{
					//It was yesterday:
					format = new SimpleDateFormat("HH:mm", Application.get().getLocale());
					result = context.getString(R.string.newsItemTimestampYesterday, (isFromGMT) ? formatFromGMT(timestamp, format) : format.format(timestamp));
				}
			}else{
				//It was from today:
				format = new SimpleDateFormat("HH:mm", Application.get().getLocale());
				result = context.getString(R.string.newsItemTimestampToday, (isFromGMT) ? formatFromGMT(timestamp, format) : format.format(timestamp));
			}
		}
		
		return result;
	}

	//TODO ???
	public static String formatNewsFeedExpireTime(long expireTimestamp){
		Calendar time = Calendar.getInstance(Application.get().getLocale());
		time.setTimeInMillis(expireTimestamp);
		Calendar now = Calendar.getInstance(Application.get().getLocale());
		
		if (now.get(Calendar.YEAR) == time.get(Calendar.YEAR) && now.get(Calendar.DAY_OF_YEAR) == time.get(Calendar.DAY_OF_YEAR)) {
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Application.get().getLocale());
			return sdf.format(expireTimestamp);
		} else {
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm | dd.MM.yy", Application.get().getLocale());
			return sdf.format(expireTimestamp);
		}
	}

	//TODO ???
	public static String formatHotTime(long timestamp){
		Calendar time = Calendar.getInstance(Application.get().getLocale());
		time.setTimeInMillis(timestamp);
		Calendar now = Calendar.getInstance(Application.get().getLocale());
		
		if (now.get(Calendar.YEAR) == time.get(Calendar.YEAR) && now.get(Calendar.DAY_OF_YEAR) == time.get(Calendar.DAY_OF_YEAR)) {
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Application.get().getLocale());
			return sdf.format(timestamp);
		} else {
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm,dd/MM", Application.get().getLocale());
			return sdf.format(timestamp);
		}
	}
	
	public static long getTimeFromHotDate(String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		return sdf.parse(date).getTime();
	}

	public static String formatHitLastTradeDate(Date date){
		String result = "";
		if(date != null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			result = sdf.format(date);
		}else{
			Log.e(TAG, "formatHitLastTradeDate -> The Date is null");
		}
		return result;
	}
	
	public static boolean isOlderThan1Week(long timestamp){
		long now = getCurrentServerTime();
		long weekInterval = 1000*60*60*24*7l;
		if(now - timestamp > weekInterval){
			return true;
		}else{
			return false;
		}
	}
	
}
