package com.copyop.android.app.fragment;

import android.os.Bundle;
import android.text.Spannable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.LoginLogoutEvent.Type;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.MenuFragment;
import com.anyoption.android.app.manager.PopUpManager.OnConfirmPopUpButtonPressedListener;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.Button;
import com.anyoption.android.app.widget.TextView;
import com.anyoption.common.beans.base.Message;
import com.anyoption.common.service.requests.MethodRequest;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.event.HideMenuWalkthroughEvent;
import com.copyop.android.app.event.ProfileUpdateEvent;
import com.copyop.android.app.event.ShowMenuWalkthroughEvent;
import com.copyop.android.app.popup.HolidaysDialogFragment;
import com.copyop.android.app.util.COBitmapUtils;
import com.copyop.android.app.util.COConstants;
import com.copyop.android.app.widget.MenuItemLayout;
import com.copyop.common.dto.base.Profile;
import com.copyop.json.results.HolidayMessageResult;
import com.copyop.json.results.UserMethodResult;

public class COMenuFragment extends MenuFragment {

    private final String TAG = COMenuFragment.class.getSimpleName();

    /**
     * Use this method to obtain new instance of the fragment.
     */
    public static COMenuFragment newInstance(Bundle bundle) {
        COMenuFragment fragment = new COMenuFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private View depositLayout;
    private Button depositButton;

    private LinearLayout MenuItemLayoutsLayout;
    private MenuItemLayout myProfileMenuItemLayout;
    private MenuItemLayout newsMenuItemLayout;
    private MenuItemLayout tradeMenuItemLayout;
    private MenuItemLayout myAccountMenuItemLayout;
    private MenuItemLayout myOptionsMenuItemLayout;
    private MenuItemLayout coinsMenuItemLayout;
    private MenuItemLayout facebookMenuItemLayout;
    private MenuItemLayout supportMenuItemLayout;
    private MenuItemLayout settingsMenuItemLayout;
    private MenuItemLayout logoutMenuItemLayout;
    private ImageView avatarImageView;
    private ImageView tradeMenuItemLayoutGlow;
    private ScrollView menuScrollView;

    @Override
    protected void registerForEvents() {
        super.registerForEvents();
        application.registerForEvents(this, ProfileUpdateEvent.class, ShowMenuWalkthroughEvent.class, HideMenuWalkthroughEvent.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.menu_fragment, container, false);

        TextView poweredByTextView = (TextView) rootView.findViewById(R.id.menu_powered_by_text_view);
        TextUtils.decorateInnerText(poweredByTextView, null, poweredByTextView.getText().toString(), "any",
                Font.ROBOTO_BOLD, 0, 0);

        depositLayout = rootView.findViewById(R.id.menu_deposit_layout);
        depositButton = (Button) rootView.findViewById(R.id.menu_deposit_button);
        MenuItemLayoutsLayout = (LinearLayout) rootView.findViewById(R.id.menu_items_layout);

        myProfileMenuItemLayout = (MenuItemLayout) rootView.findViewById(R.id.menu_item_home);
        avatarImageView = (ImageView) myProfileMenuItemLayout.findViewById(R.id.menu_item_icon);
        newsMenuItemLayout = (MenuItemLayout) rootView.findViewById(R.id.menu_item_news);
        tradeMenuItemLayout = (MenuItemLayout) rootView.findViewById(R.id.menu_item_trade);
        myAccountMenuItemLayout = (MenuItemLayout) rootView.findViewById(R.id.menu_item_my_account);
        myOptionsMenuItemLayout = (MenuItemLayout) rootView.findViewById(R.id.menu_item_my_options);
        coinsMenuItemLayout = (MenuItemLayout) rootView.findViewById(R.id.menu_item_copyop_coins);
        facebookMenuItemLayout = (MenuItemLayout) rootView.findViewById(R.id.menu_item_facebook_networks);
        supportMenuItemLayout = (MenuItemLayout) rootView.findViewById(R.id.menu_item_support);
        settingsMenuItemLayout = (MenuItemLayout) rootView.findViewById(R.id.menu_item_settings);
        logoutMenuItemLayout = (MenuItemLayout) rootView.findViewById(R.id.menu_item_logout);
        tradeMenuItemLayoutGlow = (ImageView) tradeMenuItemLayout.findViewById(R.id.menu_item_glow);
        menuScrollView = (ScrollView) rootView.findViewById(R.id.menu_scroll_view);


        TextView termsText = (TextView) rootView.findViewById(R.id.menu_bottom_text);

        TextUtils.OnInnerTextClickListener termsLinkListener = new TextUtils.OnInnerTextClickListener() {
            @Override
            public void onInnerTextClick(android.widget.TextView textView, String text, String innerText) {
                application.postEvent(new NavigationEvent(Screen.TERMS_AND_CONDITIONS, NavigationEvent.NavigationType.DEEP));
            }
        };

        Spannable spannable = TextUtils.decorateInnerText(termsText, null, termsText.getText().toString(), getString(R.string.menu_bottom_text_risk_warning), FontsUtils.Font.ROBOTO_MEDIUM, 0, 0);
        TextUtils.makeClickableInnerText(termsText, spannable, termsText.getText().toString(), getString(R.string.menu_bottom_text_terms_link), null, 0, 0, true, termsLinkListener);

        setupTradeButtonGlow();

        setupAvatar();
        initMenuItemLayouts();
        setupItemsVisibility();

        return rootView;
    }

    public void onEventMainThread(ShowMenuWalkthroughEvent event) {
        if (menuScrollView != null) {
            menuScrollView.fullScroll(ScrollView.FOCUS_UP);
            menuScrollView.setOnTouchListener( new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    return true;
                }
            });
        }
    }

    public void onEventMainThread(HideMenuWalkthroughEvent event) {
        if (menuScrollView != null) {
            menuScrollView.setOnTouchListener(null);
        }
    }

    @Override
    protected void updateUI() {
        setupAvatar();
        setupItemsVisibility();
    }

    public void onEventMainThread(ProfileUpdateEvent event) {
        setupAvatar();
    }

    private void setupAvatar() {
        avatarImageView.setImageDrawable(null);
        if (COApplication.get().isLoggedIn() && COApplication.get().getProfile() != null) {
            setupAvatar(COApplication.get().getProfile().getAvatar());
        }
    }

    private void setupAvatar(String uri) {
        avatarImageView.setImageDrawable(null);
        if (uri != null) {
            if (COApplication.get().getAvatarBitmap() != null) {
                avatarImageView.setImageBitmap(COApplication.get().getAvatarBitmap());
            } else {
                COBitmapUtils.loadBitmap(uri, avatarImageView);
            }
        }
    }

    private void setupTradeButtonGlow() {
        if (!application.getSharedPreferencesManager().getBoolean(COConstants.PREFERENCES_IS_MENU_TRADE_GLOW_SHOWN, false)) {
            tradeMenuItemLayout.showHideGlow(true);
            tradeMenuItemLayoutGlow.animate().scaleY(1.12f);
        } else {
            tradeMenuItemLayout.showHideGlow(false);
        }
    }

    private void initMenuItemLayouts() {

        OnClickListener itemClickListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (application.getSharedPreferencesManager().getBoolean(COConstants.PREFERENCES_IS_MENU_WALKTHROUGH_SHOWN, false)) {
                    if (view instanceof MenuItemLayout) {
                        MenuItemLayout MenuItemLayout = (MenuItemLayout) view;
                        selectMenuItemLayout(MenuItemLayout);
                    }

                    NavigationEvent navigationEvent = new NavigationEvent(Screen.LOGIN, NavigationType.INIT);
                    if (view == depositButton) {
                        if (RegulationUtils.checkRegulationStatusAndDoNecessary(false)) {
                            navigationEvent.setToScreen(Screen.DEPOSIT_MENU);
                        } else {
                            navigationEvent.setToScreen(application.getCurrentScreen());
                        }

                    } else if (view == myProfileMenuItemLayout) {
                        navigationEvent.setToScreen(COScreen.USER_PROFILE);
                    } else if (view == newsMenuItemLayout) {
                        navigationEvent.setToScreen(COScreen.NEWS_HOT_EXPLORE);
                    } else if (view == tradeMenuItemLayout) {
                        navigationEvent.setToScreen(Screen.TRADE);
                        if (!application.getSharedPreferencesManager().getBoolean(COConstants.PREFERENCES_IS_MENU_TRADE_GLOW_SHOWN, false)) {
                            application.getSharedPreferencesManager().putBoolean(COConstants.PREFERENCES_IS_MENU_TRADE_GLOW_SHOWN, true);
                            tradeMenuItemLayout.showHideGlow(false);
                        }
                        //Check holidays:
                        if(!COApplication.get().isHolidayChecked()){
                            checkHolidays();
                        }
                    } else if (view == myAccountMenuItemLayout) {
                        navigationEvent.setToScreen(Screen.MY_ACCOUNT);
                    } else if (view == myOptionsMenuItemLayout) {
                        navigationEvent.setToScreen(Screen.MY_OPTIONS);
                    } else if (view == coinsMenuItemLayout) {
                        navigationEvent.setToScreen(COScreen.COPYOP_COINS);
                    } else if (view == facebookMenuItemLayout) {
                        Profile profile = COApplication.get().getProfile();
                        if (null != profile && null != profile.getFbId()) {
                            navigationEvent.setToScreen(COScreen.FACEBOOK_CONNECTED);
                        } else {
                            navigationEvent.setToScreen(COScreen.FACEBOOK_NOT_CONNECTED);
                        }
                    } else if (view == supportMenuItemLayout) {
                        navigationEvent.setToScreen(Screen.SUPPORT);
                    } else if (view == settingsMenuItemLayout) {
                        navigationEvent.setToScreen(Screen.SETTINGS);
                    } else if (view == logoutMenuItemLayout) {
                        application.getPopUpManager().showConfirmPopUp(null, getString(R.string.logoutPopupQuestion),
                                getString(R.string.logoutPopupPositiveButton),
                                getString(R.string.logoutPopupNegativeButton), new OnConfirmPopUpButtonPressedListener() {
                                    @Override
                                    public void onPositive() {
                                        application.setUser(null);
                                        application.setUserRegulation(null);
                                        application.postEvent(new LoginLogoutEvent(Type.LOGOUT, null));
                                    }

                                    @Override
                                    public void onNegative() {
                                    }
                                });

                        return;
                    }

                    application.postEvent(navigationEvent);
                }
            }
        };

        depositButton.setOnClickListener(itemClickListener);
        myProfileMenuItemLayout.setOnClickListener(itemClickListener);
        newsMenuItemLayout.setOnClickListener(itemClickListener);
        tradeMenuItemLayout.setOnClickListener(itemClickListener);
        myAccountMenuItemLayout.setOnClickListener(itemClickListener);
        myOptionsMenuItemLayout.setOnClickListener(itemClickListener);
        coinsMenuItemLayout.setOnClickListener(itemClickListener);
        facebookMenuItemLayout.setOnClickListener(itemClickListener);
        supportMenuItemLayout.setOnClickListener(itemClickListener);
        settingsMenuItemLayout.setOnClickListener(itemClickListener);
        logoutMenuItemLayout.setOnClickListener(itemClickListener);

    }

    private void setupItemsVisibility() {
        if (application.isLoggedIn()) {
            depositLayout.setVisibility(View.VISIBLE);

            myProfileMenuItemLayout.setVisibility(View.VISIBLE);
            myProfileMenuItemLayout
                    .setTitle(application.getUser().getFirstName() + " " + application.getUser().getLastName());
            newsMenuItemLayout.setVisibility(View.VISIBLE);
            myAccountMenuItemLayout.setVisibility(View.VISIBLE);
            myOptionsMenuItemLayout.setVisibility(View.VISIBLE);
            coinsMenuItemLayout.setVisibility(View.VISIBLE);
            facebookMenuItemLayout.setVisibility(View.VISIBLE);
            logoutMenuItemLayout.setVisibility(View.VISIBLE);
        } else {
            depositLayout.setVisibility(View.GONE);

            myProfileMenuItemLayout.setVisibility(View.GONE);
            newsMenuItemLayout.setVisibility(View.GONE);
            myAccountMenuItemLayout.setVisibility(View.GONE);
            myOptionsMenuItemLayout.setVisibility(View.GONE);
            coinsMenuItemLayout.setVisibility(View.GONE);
            facebookMenuItemLayout.setVisibility(View.GONE);
            logoutMenuItemLayout.setVisibility(View.GONE);
        }
        tradeMenuItemLayout.setVisibility(View.VISIBLE);
        supportMenuItemLayout.setVisibility(View.VISIBLE);
        settingsMenuItemLayout.setVisibility(View.VISIBLE);
    }

    private void selectMenuItemLayout(MenuItemLayout item) {
        for (int i = 0; i < MenuItemLayoutsLayout.getChildCount(); i++) {
            View view = MenuItemLayoutsLayout.getChildAt(i);
            if (view instanceof MenuItemLayout) {
                MenuItemLayout mi = (MenuItemLayout) view;
                if (mi == item) {
                    mi.setActivated(true);
                } else {
                    mi.setActivated(false);
                }
            }
        }
        MenuItemLayoutsLayout.invalidate();
    }

    @Override
    public void onEventMainThread(NavigationEvent event) {
        Log.d(TAG, "On Navigation event");
        Screenable screenable = event.getToScreen();
        MenuItemLayout MenuItemLayout = null;
        if (screenable == COScreen.USER_PROFILE) {
            MenuItemLayout = myProfileMenuItemLayout;
        } else if (screenable == COScreen.NEWS_HOT_EXPLORE) {
            MenuItemLayout = newsMenuItemLayout;
        } else if (screenable == Screen.TRADE) {
            MenuItemLayout = tradeMenuItemLayout;
        } else if (screenable == Screen.MY_ACCOUNT) {
            MenuItemLayout = myAccountMenuItemLayout;
        } else if (screenable == Screen.MY_OPTIONS) {
            MenuItemLayout = myOptionsMenuItemLayout;
        } else if (screenable == COScreen.COPYOP_COINS) {
            MenuItemLayout = coinsMenuItemLayout;
        } else if (screenable == COScreen.FACEBOOK_CONNECTED || screenable == COScreen.FACEBOOK_NOT_CONNECTED) {
            MenuItemLayout = facebookMenuItemLayout;
        } else if (screenable == Screen.SUPPORT) {
            MenuItemLayout = supportMenuItemLayout;
        } else if (screenable == Screen.SETTINGS) {
            MenuItemLayout = settingsMenuItemLayout;
        }

        selectMenuItemLayout(MenuItemLayout);
    }

    @Override
    public void onEventMainThread(LoginLogoutEvent event) {
        Log.d(TAG, "Login event received.");
        if (event.getType() != Type.LOGOUT) {
            setupAvatar(((UserMethodResult) event.getUserResult()).getCopyopProfile().getAvatar());
        }
        setupItemsVisibility();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////                                   REQUESTs & RESPONSEs                                            /////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void checkHolidays(){
        Log.d(TAG, "checkHolidays");
        application.getCommunicationManager().requestService(this, "checkHolidaysCallback", Constants.SERVICE_GET_HOLIDAY_MESSAGE, new MethodRequest(), HolidayMessageResult.class, false, false);
    }

    public void checkHolidaysCallback(Object resObj){
        Log.d(TAG, "checkHolidaysCallback");
        HolidayMessageResult result = (HolidayMessageResult) resObj;
        if(result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS){
            COApplication.get().setHolidayChecked(true);
            Message message = result.getMessage();
            if (message != null && message.getText() != null){
                Log.d(TAG, "Holiday message.");
                Bundle bundle = new Bundle();
                bundle.putString(Constants.EXTRA_MESSAGE, message.getText());
                application.getFragmentManager().showDialogFragment(HolidaysDialogFragment.class, bundle);
            }else{
                Log.d(TAG, "No holiday messages.");
            }
        }else{
            Log.e(TAG, "Error in checkHolidaysCallback");
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
