package com.copyop.android.app.fragment.trade;

import android.os.Bundle;
import android.util.Log;

import com.anyoption.android.app.fragment.trade.ChartFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.chart.ChartDrawable;
import com.anyoption.android.app.widget.chart.ChartView;
import com.anyoption.common.beans.base.Market;
import com.copyop.android.app.R;
import com.copyop.android.app.widget.chart.COChartView;

/**
 * @author Anastas Arnaudov
 */
public class COChartFragment extends ChartFragment{

    private static final String TAG = COChartFragment.class.getSimpleName();

    /**
     * Use this method to create new instance of the fragment.
     *
     * @return
     */
    public static COChartFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new COChartFragment.");
        COChartFragment fragment = new COChartFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected ChartDrawable getChartView(){
        return new COChartView(getContext(), null, R.style.chart_chart_view);
    }

}
