package com.copyop.android.app;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.LoginLogoutEvent.Type;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.manager.GoogleAdWordsManager;
import com.anyoption.android.app.manager.GoogleAnalyticsManager;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.UserMethodResult;
import com.copyop.android.app.event.CopiersUpdateEvent;
import com.copyop.android.app.event.WatchersUpdateEvent;
import com.copyop.android.app.manager.COAppoxeeManager;
import com.copyop.android.app.manager.COAppseeManager;
import com.copyop.android.app.manager.COCommunicationManager;
import com.copyop.android.app.manager.CODeepLinksManager;
import com.copyop.android.app.manager.COFacebookManager;
import com.copyop.android.app.manager.COGoogleAdWordsManager;
import com.copyop.android.app.manager.COGoogleAnalyticsManager;
import com.copyop.android.app.manager.COLightstreamerManager;
import com.copyop.android.app.manager.CONotificationManager;
import com.copyop.android.app.manager.COPopUpManager;
import com.copyop.android.app.manager.COPushNotificationsManager;
import com.copyop.android.app.manager.InboxManager;
import com.copyop.android.app.manager.ShareRateManager;
import com.copyop.android.app.model.TopLogTrader;
import com.copyop.android.app.popup.SummaryDialogFragment;
import com.copyop.android.app.util.COConstants;
import com.copyop.common.dto.base.Profile;
import com.copyop.json.requests.ProfileMethodRequest;
import com.copyop.json.requests.WatchListMethodRequest;
import com.copyop.json.results.CopyListMethodResult;
import com.copyop.json.results.LastLoginSummaryMethodResult;
import com.copyop.json.results.WatchListMethodResult;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;

public class COApplication extends Application{

	public static final String TAG = COApplication.class.getSimpleName();

	public static final String SUBCLASS_PREFIX = "CO";
	
	private static final String[] LANGUAGES_ARR = new String[] { "en", "es", "de", "fr", "it" };
	private static final Set<String> LANGUAGES_SET = new HashSet<String>(Arrays.asList(LANGUAGES_ARR));
	
	protected CONotificationManager 		notificationManager;
	protected COLightstreamerManager 		lightstreamerManager;
	protected COPopUpManager		 		popUpManager;
	protected COCommunicationManager 		communicationManager;
	protected COFacebookManager 			facebookManager;
	protected InboxManager 					inboxManager;
	protected ShareRateManager 				shareRateManager;
	protected COAppoxeeManager 				appoxeeManager;
	protected COPushNotificationsManager 	pushNotificationsManager;
	protected COGoogleAnalyticsManager 		googleAnalyticsManager;
	protected COAppseeManager 				appseeManager;
	protected CODeepLinksManager 			deepLinksManager;

	private Profile profile;
	private Bitmap avatar;
	private List<Profile> copyingProfilesList;
	private List<Profile> watchingProfilesList;
	private ArrayList<Long> copyAmounts;
	private double copyOddsWin;
	private double copyOddsLose;
	private int maxCopiersPerAccount;
	private int safeModeMaxTrades;
	private int safeModeMinCopyUsers;
	private int safeModePeriod;
	private boolean isSafeMode;

	private BaseDialogFragment summaryDialog;

	private boolean isHolidayChecked;
	private User savedFromRestartUser;

	//#################################################################################################################
	//######################					COPYOP	SCREENs						###################################
	//#################################################################################################################

	public interface COScreenable extends Screenable{
		public static final int SCREEN_FLAG_ADD_INBOX 			= 7;
		public static final int SCREEN_FLAG_REMOVE_INBOX 		= 8;
		public static final int SCREEN_FLAG_ADD_SHARE 			= 9;
		public static final int SCREEN_FLAG_REMOVE_SHARE 		= 10;
		public static final int SCREEN_FLAG_ADD_DONE 			= 11;
		public static final int SCREEN_FLAG_REMOVE_DONE 		= 12;
		public static final int SCREEN_FLAG_ADD_POWERED_BY 		= 13;
		public static final int SCREEN_FLAG_REMOVE_POWERED_BY 	= 14;
		public static final int SCREEN_FLAG_TITLE_ORANGE	 	= 15;
		public static final int SCREEN_FLAG_ADD_LANGUAGE 		= 16;
		public static final int SCREEN_FLAG_REMOVE_LANGUAGE 	= 17;
	}
	
	/**
	 * Constant names for each main screen (Map each screen to a Constant). This is used in the
	 * navigation flow.
	 */
	public enum COScreen implements COScreenable {
		WELCOME("welcome", "", R.drawable.logo_header, SCREEN_FLAG_ADD_POWERED_BY, SCREEN_FLAG_ADD_LANGUAGE), 
		DEPOSIT_MENU("deposit_menu", get().getResources().getString(R.string.depositScreenTitle), 0, COScreenable.SCREEN_FLAG_ADD_MENU ,COScreenable.SCREEN_FLAG_ADD_HELP),
		LSS_HOME("lss_home", "", 0),
		LSS_LOGIN("lss_login", "", 0),
		LSS_LOGIN_SIGNUP("lss_login_signup", Application.get().getString(R.string.lssLoginTitle), 0, SCREEN_FLAG_ADD_BACK),
		LSS_SIGNUP("lss_signup", Application.get().getString(R.string.lssSignup), 0, SCREEN_FLAG_ADD_BACK),
		LSS_SIGNUP_EMPTY("lss_signup_empty", Application.get().getString(R.string.lssSignup), 0),
		LSS_SIGNUP_FINISH("lss_signup_finish", Application.get().getString(R.string.lssFinishSignUp), 0),
		LSS_AO_FINISH("lss_ao_finish", Application.get().getString(R.string.lssAOFinish), 0),
		USER_PROFILE("user_profile", "", 0),
		NEWS_HOT_EXPLORE("news_feed", "", R.drawable.logo_header, SCREEN_FLAG_ADD_INBOX, SCREEN_FLAG_ADD_SHARE),
		TABLET_NEWS_HOT_EXPLORE("tablet_news_feed", "", R.drawable.logo_header, SCREEN_FLAG_ADD_INBOX, SCREEN_FLAG_ADD_SHARE),
		TRADE("trade", Application.get().getString(R.string.tradeScreenTitle), R.drawable.logo_header,SCREEN_FLAG_TITLE_ORANGE, SCREEN_FLAG_ADD_POWERED_BY),
		OPEN_OPTIONS("open_options", Application.get().getString(R.string.tradeScreenTitle), R.drawable.logo_header,SCREEN_FLAG_TITLE_ORANGE, SCREEN_FLAG_ADD_POWERED_BY),
		COPYOP_COINS("copyop_coins", Application.get().getString(R.string.coinsScreenTitle), R.drawable.logo_header, SCREEN_FLAG_TITLE_ORANGE),
		FACEBOOK_NOT_CONNECTED("social_networks_not_connected", Application.get().getString(R.string.socialNetworksNOTConnectedScreenTitle), 0),
		FACEBOOK_CONNECTED("social_networks_connected", Application.get().getString(R.string.socialNetworksNOTConnectedScreenTitle), 0),
		CHANGE_NICKNAME("change_nickname", Application.get().getString(R.string.changeNicknameScreenTitle), 0),
		COPYING_LIST("copying_list", Application.get().getString(R.string.copyingListTitle), 0),
		COPIERS_LIST("copiers_list", Application.get().getString(R.string.copiersListTitle), 0),
		WATCHING_LIST("watching_list", Application.get().getString(R.string.watchingListTitle), 0),
		WATCHERS_LIST("watchers_list", Application.get().getString(R.string.watchersListTitle), 0),
		HOT_ALL("hot_all", "", R.drawable.logo_header, SCREEN_FLAG_ADD_INBOX, SCREEN_FLAG_ADD_SHARE);
				
		/**
		 * Tag used to search.
		 */
		private String tag;

		/**
		 * A default title. "" - if no default title is needed.
		 */
		private String title;
		/**
		 * Id of an image used instead of title. 0 - if no image is needed.
		 */
		private int imageID;

		/**
		 * Additional details for the Screen
		 */
		private int[] flags;
		
		/**
		 * Map of all screens
		 */
		private static HashMap<String, Screenable> screensMap;
		
		COScreen(String tag, String title, int imageID, int... flags){
			this.tag = tag;
			this.title = title;
			this.imageID = imageID;
			this.flags = flags;
		}

		public String getTag() {
			return tag;
		}
		public String getTitle() {
			return title;
		}
		public int getImageID() {
			return imageID;
		}
		public boolean isHasHelpInfo() {
			return false;
		}
		public int[] getFlags(){
			return this.flags;
		}

		public static Screenable getScreenByTag(String tag) {
			if (screensMap == null) {
				Screenable[] baseScreens = Screen.values();
				// Add base screens
				screensMap = new HashMap<String, Screenable>(baseScreens.length);
				for (Screenable screen : baseScreens) {
					screensMap.put(screen.getTag(), screen);
				}

				// Add CO screens
				Screenable[] coScreens = COScreen.values();
				for (Screenable screen : coScreens) {
					screensMap.put(screen.getTag(), screen);
				}
			}

			return screensMap.get(tag);
		}

	}
	//#################################################################################################################
	//#################################################################################################################
	//#################################################################################################################
	
	
	@Override
	public void onCreate() {

		DEFAULT_SKIN_ID = Skin.SKIN_REG_EN;

		super.onCreate();
		
		inboxManager 		= getInboxManager();
		shareRateManager 	= getShareRateManager();
		
		copyingProfilesList 	= new ArrayList<Profile>();
		watchingProfilesList 	= new ArrayList<Profile>();
	}
	
	/**
	 * Return the singleton instance of the application context.
	 * @return
	 */
	public static COApplication get(){
		return (COApplication) singletonAplication;
	}

	@Override
	protected void setupRestartIntent(Intent intent, Bundle bundle, Screenable screen, boolean shouldGoToMain) {
		super.setupRestartIntent(intent, bundle, screen, shouldGoToMain);
		Log.d(TAG, "Setting up the restart Intent...");
		Log.d(TAG, "Profile = " + profile);
		bundle.putSerializable(COConstants.EXTRA_SAVED_FROM_RESTART_USER, savedFromRestartUser);
		bundle.putSerializable(COConstants.EXTRA_PROFILE, profile);
		bundle.putSerializable(COConstants.EXTRA_COPY_AMOUNTS, copyAmounts);
		bundle.putDouble(COConstants.EXTRA_COPY_ODDS_WIN, copyOddsWin);
		bundle.putDouble(COConstants.EXTRA_COPY_ODDS_LOSE, copyOddsLose);
		bundle.putInt(COConstants.EXTRA_MAX_COPIERS_PER_ACCOUNT, maxCopiersPerAccount);
	}
	
	//#################################################################################################################
	//###########################					MANAGERS						###################################
	//#################################################################################################################
	
	@Override
	public synchronized COPopUpManager getPopUpManager(){
		if(popUpManager == null){
			popUpManager = new COPopUpManager(this);
		}
		return popUpManager;
	}
	
	@Override
	public synchronized CONotificationManager getNotificationManager(){
		if(notificationManager == null){
			notificationManager = new CONotificationManager(this);
		}
		return notificationManager;
	}
	
	@Override
	public synchronized COLightstreamerManager getLightstreamerManager(){
		if(lightstreamerManager == null){
			lightstreamerManager = new COLightstreamerManager(this);
		}
		return lightstreamerManager;
	}
	
	@Override
	public synchronized COCommunicationManager getCommunicationManager(){
		if(communicationManager == null){
			communicationManager = new COCommunicationManager(this);
		}
		return communicationManager;
	}
	
	@Override
	public synchronized COFacebookManager getFacebookManager() {
		if (null == facebookManager) {
			facebookManager = new COFacebookManager(this);
		}
		return facebookManager;
	}

	@Override
	public synchronized CODeepLinksManager getDeepLinksManager() {
		if(deepLinksManager == null){
			deepLinksManager = new CODeepLinksManager(this);
		}
		return deepLinksManager;
	}

	public synchronized InboxManager getInboxManager() {
		if (inboxManager == null) {
			inboxManager = new InboxManager(this);
		}
		return inboxManager;
	}
	
	public synchronized ShareRateManager getShareRateManager() {
		if (null == shareRateManager) {
			shareRateManager = new ShareRateManager();
		}
		return shareRateManager;
	}

	@Override
	public synchronized COAppoxeeManager getAppoxeeManager(){
		if(appoxeeManager == null){
			appoxeeManager = new COAppoxeeManager(this);
		}
		return appoxeeManager;
	}

	@Override
	public synchronized COPushNotificationsManager getPushNotificationsManager() {
		if (pushNotificationsManager == null) {
			pushNotificationsManager = new COPushNotificationsManager(this);
		}
		return pushNotificationsManager;
	}

	@Override
	public GoogleAdWordsManager getGoogleAdWordsManager() {
		if (googleAdWordsManager == null) {
			googleAdWordsManager = new COGoogleAdWordsManager(this);
		}
		return googleAdWordsManager;
	}

	@Override
	public synchronized GoogleAnalyticsManager getGoogleAnalyticsManager() {
		return new COGoogleAnalyticsManager(R.xml.copyop_app_tracker);
	}

	@Override
	public synchronized COAppseeManager getAppseeManager(){
		if(appseeManager == null){
			appseeManager = new COAppseeManager();
		}
		return appseeManager;
	}
	//#################################################################################################################
	//#################################################################################################################
	//#################################################################################################################

	//##########################################################################################
	//##################			EVENT BUS CALLBACKs			################################
	//##########################################################################################

	@Override
	public void onEventMainThread(LoginLogoutEvent event) {
		super.onEventMainThread(event);
		if (event.getType() == Type.LOGIN || event.getType() == Type.REGISTER) {
			getCopying();
			getWatching();
			getLastSummary();
			if (event.getType() == Type.REGISTER) {
				getSharedPreferencesManager().putBoolean(
						COConstants.PREFERENCES_IS_AUTO_WATCHED_SHOWN, false);
			}
		} else if (event.getType() == Type.LOGOUT) {
			setAvatarBitmap(null);
		}
	}
	
	//##########################################################################################
	
	//##########################################################################################
	//##################			REQUESTs - RESPONSEs		################################
	//##########################################################################################
	
	protected void getCopying(){
		ProfileMethodRequest request = new ProfileMethodRequest();
		request.setRequestedUserId(getUser().getId());
		getCommunicationManager().requestService(this, "gotCopying", COConstants.SERVICE_GET_COPYING, request, CopyListMethodResult.class, false, false);
	}
	
	public void gotCopying(Object result){
		if(result != null && result instanceof CopyListMethodResult && ((CopyListMethodResult) result).getErrorCode() == Constants.ERROR_CODE_SUCCESS){
			CopyListMethodResult copyListMethodResult = (CopyListMethodResult) result;
			this.copyingProfilesList.clear();
			copyingProfilesList.addAll(copyListMethodResult.getCopyList());
			postEvent(new CopiersUpdateEvent());
		}else{
			Log.e(TAG, COConstants.SERVICE_GET_COPIERS + " callback -> Could NOT get the copiersIDsList.");
		}
	}
	
	protected void getWatching(){
		WatchListMethodRequest request = new WatchListMethodRequest();
		request.setRequestedUserId(getUser().getId());
		getCommunicationManager().requestService(this, "gotWatching", COConstants.SERVICE_GET_WATCHING, request, WatchListMethodResult.class, false, false);
	}
	
	public void gotWatching(Object result){
		if(result != null && result instanceof WatchListMethodResult && ((WatchListMethodResult) result).getErrorCode() == Constants.ERROR_CODE_SUCCESS){
			WatchListMethodResult watchListMethodResult = (WatchListMethodResult) result;
			watchingProfilesList.clear();
			watchingProfilesList.addAll(watchListMethodResult.getWatchList());
			postEvent(new WatchersUpdateEvent());
		}else{
			Log.e(TAG, COConstants.SERVICE_GET_WATCHERS + " callback -> Could NOT get the watchersIDsList.");
		}
	}

	public void getLastSummary(){
		Log.d(TAG, "getLastSummary");
		UserMethodRequest request = new UserMethodRequest();
		getCommunicationManager().requestService(this, "gotLastSummary", COConstants.SERVICE_GET_LAST_LOGIN_SUMMARY, request, LastLoginSummaryMethodResult.class, false, false);
	}
	
	public void gotLastSummary(Object resObject){
		Log.d(TAG, "gotLastSummary");
		LastLoginSummaryMethodResult result = (LastLoginSummaryMethodResult) resObject;
		if(result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS && result.getTopLogTraders() != null){
			//Open the Summary POPUP:
			Bundle bundle = new Bundle();
			List<TopLogTrader> tradersList = new ArrayList<TopLogTrader>();
			for(LastLoginSummaryMethodResult.TopLogTrader t : result.getTopLogTraders()){
				tradersList.add(new TopLogTrader(t));
			}
			bundle.putSerializable(COConstants.EXTRA_TOP_TRADERS, (Serializable) tradersList);
			//We close the Summary Dialog if it was opened before:
			if(summaryDialog != null){
				summaryDialog.dismiss();
			}
			fragmentManager.showDialogFragment(SummaryDialogFragment.class, bundle);
		}
	}
	
	//##########################################################################################
	
	
	@Override
	public String getServiceURL() {
		return COConstants.SERVICE_URL;
	}

	@Override
	public String getLSURL() {
		return COConstants.LS_URL;
	}

	public String getCopyopLSURL() {
		return COConstants.LS_COPYOP_URL;
	}
	
	@Override
	public String getMerchantID() {
		return COConstants.MERCHANT_ID;
	}

	@Override
	public String getOneClickURL() {
		return COConstants.ONE_CLICK_URL;
	}

	@Override
	public String getWebSiteLink() {
		return COConstants.WEB_SITE_LINK;
	}

	@Override
	public String getDeepLink(){
		return COConstants.DEEP_LINK;
	}

	@Override
	protected String getSubclassPrefix() {
		return COApplication.SUBCLASS_PREFIX;
	}
	
	@Override
	public long getWriterId(){
		return COConstants.MOBILE_WRITER_ID;
	}

	@Override
	public String getServiceMethodGetUser() {
		return COConstants.SERVICE_GET_COPYOP_USER;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class getUserMethodResultClass() {
		return com.copyop.json.results.UserMethodResult.class;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	@Override
	public Screenable getHomeScreen() {
		if (isTablet) {
			// TODO
//			return COScreen.TABLET_NEWS_HOT_EXPLORE;
			return COScreen.NEWS_HOT_EXPLORE;
		} else {
			return COScreen.NEWS_HOT_EXPLORE;
		}
	}

	@Override
	protected Set<String> getSupportedLanguages() {
		return LANGUAGES_SET;
	}

	@Override
	public void saveUser(UserMethodResult result, boolean shouldChangeLocale) {
		super.saveUser(result, shouldChangeLocale);

		com.copyop.json.results.UserMethodResult coResult = (com.copyop.json.results.UserMethodResult) result;
		if (coResult.getCopyopProfile() != null) {
			setProfile(coResult.getCopyopProfile());
		}
		if(coResult.getCopyAmounts() != null){
			setCopyAmounts(coResult.getCopyAmounts());
		}

		setMaxCopiersPerAccount(coResult.getMaxCopiersPerAccount());
		setSafeModeMaxTrades(coResult.getSafeModeMaxTrades());
		setSafeModeMinCopyUsers(coResult.getSafeModeMinCopyUsers());
		setSafeModePeriod(coResult.getSafeModePeriod());
		setIsSafeMode(coResult.isSafeMode());
	}

	@Override
	protected void removeSavedUser() {
		super.removeSavedUser();
		setProfile(null);
	}
	
	@Override
	protected void goToAfterLogoutScreen() {
		postEvent(new NavigationEvent(COScreen.LSS_LOGIN_SIGNUP, NavigationType.INIT));
	}

	public Bitmap getAvatarBitmap() {
		return avatar;
	}

	public void setAvatarBitmap(Bitmap avatar) {
		this.avatar = avatar;
	}
	
	public List<Profile> getCopiersProfilesList() {
		return copyingProfilesList;
	}

	public List<Profile> getWatchersProfilesList() {
		return watchingProfilesList;
	}

	public boolean isCopying(long userId){
		for(Profile profile : copyingProfilesList){
			if(userId == profile.getUserId()){
				return profile.isCopying();
			}
		}
		return false;
	}
	
	public boolean isWatching(long userId){
		for(Profile profile : watchingProfilesList){
			if(userId == profile.getUserId()){
				return profile.isWatching();
			}
		}
		return false;
	}

	public Profile getCopying(long userId){
		for(Profile profile : copyingProfilesList){
			if(userId == profile.getUserId()){
				return profile;
			}
		}
		return null;
	}
	
	public Profile getWatching(long userId){
		for(Profile profile : watchingProfilesList){
			if(userId == profile.getUserId()){
				return profile;
			}
		}
		return null;
	}
	
	public void refreshCopiers(){
		getCopying();
	}
	
	public void refreshWatchers(){
		getWatching();
	}

	public List<Long> getCopyAmounts() {
		return copyAmounts;
	}

	public void setCopyAmounts(List<Long> copyAmounts) {
		this.copyAmounts = new ArrayList<Long>(copyAmounts);
	}

	public double getCopyOddsWin() {
		return copyOddsWin;
	}

	public void setCopyOddsWin(double copyOddsWin) {
		this.copyOddsWin = copyOddsWin;
	}

	public double getCopyOddsLose() {
		return copyOddsLose;
	}

	public void setCopyOddsLose(double copyOddsLose) {
		this.copyOddsLose = copyOddsLose;
	}

	public int getMaxCopiersPerAccount() {
		return maxCopiersPerAccount;
	}

	// TODO Must be called on every user update.
	public void setMaxCopiersPerAccount(int maxCopiersPerAccount) {
		this.maxCopiersPerAccount = maxCopiersPerAccount;
	}


	public int getSafeModePeriod() {
		return safeModePeriod;
	}

	public void setSafeModePeriod(int safeModePeriod) {
		this.safeModePeriod = safeModePeriod;
	}

	public int getSafeModeMinCopyUsers() {
		return safeModeMinCopyUsers;
	}

	public void setSafeModeMinCopyUsers(int safeModeMinCopyUsers) {
		this.safeModeMinCopyUsers = safeModeMinCopyUsers;
	}

	public int getSafeModeMaxTrades() {
		return safeModeMaxTrades;
	}

	public void setSafeModeMaxTrades(int safeModeMaxTrades) {
		this.safeModeMaxTrades = safeModeMaxTrades;
	}

	public boolean isSafeMode() {
		return isSafeMode;
	}

	public void setIsSafeMode(boolean isSafeMode) {
		this.isSafeMode = isSafeMode;
	}


	public void setHolidayChecked(boolean isHolidayChecked){
		this.isHolidayChecked = isHolidayChecked;
	}

	public boolean isHolidayChecked(){
		return isHolidayChecked;
	}

	public User getSavedFromRestartUser() {
		return savedFromRestartUser;
	}

	public void setSavedFromRestartUser(User user){
		this.savedFromRestartUser = user;
	}
}