package com.copyop.android.app.fragment.login_register;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.LoginLogoutEvent.Type;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.login_register.RegisterFragment;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.form.FormCheckBox;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.beans.base.Register;
import com.anyoption.common.beans.base.Platform;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.popup.EditAvatarDialogFragment;
import com.copyop.android.app.util.AvatarUtils;
import com.copyop.android.app.util.COBitmapUtils;
import com.copyop.android.app.util.COConstants;
import com.copyop.json.requests.InsertUserMethodRequest;
import com.copyop.json.results.UserMethodResult;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class CORegisterFragment extends RegisterFragment {
	public static final String TAG = CORegisterFragment.class.getSimpleName();

	protected String avatarURL;
	protected ImageView avatarImage;
	protected Bitmap bitmap;
	protected FormEditText nicknameField;
	protected FormEditText retypePasswordField;
	protected FormCheckBox termsAndConditionsCheckBox;
	protected String fbId;

	protected EditAvatarDialogFragment editAvatarDialog;
	
	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static CORegisterFragment newInstance() {	
		return newInstance(new Bundle());
	}
	
	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static CORegisterFragment newInstance(Bundle bundle) {
		CORegisterFragment registerFragment = new CORegisterFragment();	
		registerFragment.setArguments(bundle);
		return registerFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = inflater.inflate(R.layout.register_fragment_layout, container, false); 

		init();
		initAvatar();
		
		setupForm();
 
		setupTermsAndConditions();
		
		setupRegisterButton();
		
		setupSupportButton();
		
		return rootView;
	}

	protected void initAvatar() {
		avatarURL = AvatarUtils.generateRandomAvatar();
	}
	
	protected void setupForm() {
		super.setupForm();

		setupCopyopForm();
	}
	
	protected void setupCopyopForm() {
		avatarImage = (ImageView) rootView.findViewById(R.id.lss_avatar_image);
		final int avatarSize = getActivity().getResources().getDimensionPixelSize(R.dimen.lss_register_avatar_size);
		COBitmapUtils.loadBitmap(avatarURL, avatarImage, avatarSize, avatarSize);
		
		View editAvatarButton = rootView.findViewById(R.id.lss_avatar_edit);
		OnClickListener avatarClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				editAvatarDialog = EditAvatarDialogFragment.newInstance(null);
				editAvatarDialog.setListener(new EditAvatarDialogFragment.EditAvatarDialogListener() {
					@Override
					public void onUpload(Bitmap bitmap){
						Log.d(TAG, "onUpload");
						CORegisterFragment.this.bitmap = bitmap;
						avatarImage.setImageBitmap(bitmap);
					}
					@Override
					public void onDrawMeMaleAvatar(String avatarURL) {
						CORegisterFragment.this.avatarURL = avatarURL;
						COBitmapUtils.loadBitmap(avatarURL, avatarImage, avatarSize, avatarSize);
					}
					@Override
					public void onDrawMeFemaleAvatar(String avatarURL) {
						CORegisterFragment.this.avatarURL = avatarURL;
						COBitmapUtils.loadBitmap(avatarURL, avatarImage, avatarSize, avatarSize);
					}
				});
				application.getFragmentManager().showDialogFragment(editAvatarDialog);
			}
		};
		avatarImage.setOnClickListener(avatarClickListener);
		editAvatarButton.setOnClickListener(avatarClickListener);

		nicknameField = (FormEditText) rootView.findViewById(R.id.lss_register_nickname_edittext); 
		nicknameField.addValidator(ValidatorType.EMPTY, ValidatorType.NICKNAME); 
		nicknameField.setFieldName(COConstants.FIELD_NICKNAME);
		form.addItem(nicknameField);

		retypePasswordField = (FormEditText) rootView.findViewById(R.id.lss_register_retype_pass_edittext);
		if (null != retypePasswordField) {
			retypePasswordField.addValidator(ValidatorType.EMPTY, ValidatorType.PASSWORD_MINIMUM_CHARACTERS, ValidatorType.ALNUM);
			retypePasswordField.addValidatorForEquality(passwordField, getString(R.string.errorRetypedPasswordNOTMatch));
			retypePasswordField.setFieldName(Constants.FIELD_PASSWORD_RE_TYPE);
			form.addItem(retypePasswordField);
		}
	}
	
	protected void setupTermsAndConditions() {
		termsAndConditionsCheckBox = (FormCheckBox) rootView.findViewById(R.id.lss_register_terms_and_conditions_checkbox);
		
		termsAndConditionsTextView = (TextView) rootView.findViewById(R.id.lss_register_terms_and_conditions_textview);
		String tc = getString(R.string.lssTermsAndConditions);
		TextUtils.makeClickableInnerText(termsAndConditionsTextView, null, getString(R.string.lssCopyopTC, tc), tc, Font.ROBOTO_BOLD, R.dimen.lss_register_tc_text_size, R.color.black_1, false, new TextUtils.OnInnerTextClickListener() {
			@Override
			public void onInnerTextClick(TextView textView, String text, String innerText) {
				if(isTermsAndConditionsButtonEnabled){
					Bundle bundle  = new Bundle();	                
					bundle.putLong(Constants.EXTRA_COUNTRY_ID, countryId);
					application.postEvent(new NavigationEvent(Screen.TERMS_AND_CONDITIONS, NavigationType.DEEP, bundle));					
				}
			}
		});
	}
	
	protected void setupSupportButton() {
		View supportButton = rootView.findViewById(R.id.lss_support);
		supportButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				application.postEvent(new NavigationEvent(Screen.SUPPORT, NavigationType.DEEP));
			}
		});
	}
	
	@Override
	public void insertRegister() {
		Register user = new Register();
		setUserDetalis(user);
    	InsertUserMethodRequest request = new InsertUserMethodRequest();      
        request.setRegister(user);
        request.setmId(application.getSharedPreferencesManager().getString(Constants.PREFERENCES_MID, null));
        request.setEtsMId(application.getSharedPreferencesManager().getString(Constants.PREFERENCES_ETS_MID, null));

        Log.d(TAG, "-------insertUser--------");
		Log.d(TAG, "user combId = " + user.getCombinationId());
		Log.d(TAG, "user dp = " + user.getDynamicParam());
		Log.d(TAG, "user aff_sub1 = " + user.getAff_sub1());
		Log.d(TAG, "user aff_sub2 " + user.getAff_sub2());
		Log.d(TAG, "user aff_sub3 = " + user.getAff_sub3());
		Log.d(TAG, "request mid = " + request.getmId());
		Log.d(TAG, "request etsmid = " + request.getEtsMId());
		Log.d(TAG, "--------------------------");
		
		request.setAvatar(avatarURL);
		request.setNickname(nicknameField.getValue());
		request.setFbId(fbId);
		request.setAcceptedTermsAndConditions(termsAndConditionsCheckBox.isChecked());
		
		if (!Application.get().getCommunicationManager().requestService(CORegisterFragment.this, "insertProfileCallBack", COConstants.SERVICE_INSERT_PROFILE, request , UserMethodResult.class, false, false)) {
			registerButton.stopLoading();
		}
	}
	
	public void insertProfileCallBack(Object resultObj) {
		UserMethodResult result = (UserMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Insert user successfully");		
			
			if(result.getmId() != null && result.getmId().length() > 0) {	
				application.getSharedPreferencesManager().putString(Constants.PREFERENCES_MID, result.getmId());
			} 
			if(result.getEtsMid() != null && result.getEtsMid().length() > 0) {
	    		application.getSharedPreferencesManager().putString(Constants.PREFERENCES_ETS_MID, result.getEtsMid());
	    	}		
			Log.d(TAG, "result user combId = " + result.getUser().getCombinationId());
			Log.d(TAG, "result user dp = " + result.getUser().getDynamicParam());
			Log.d(TAG, "result mid = " + result.getmId());
			Log.d(TAG, "result etsmid = " + result.getEtsMid());
			Log.d(TAG, "result aff_sub1 = " + result.getUser().getAffSub1());
			Log.d(TAG, "result aff_sub2 " + result.getUser().getAffSub2());
			Log.d(TAG, "result aff_sub3 = " + result.getUser().getAffSub3());
			
			form.clear();

			COApplication coApplication = COApplication.get();
			coApplication.setProfile(result.getCopyopProfile());
			coApplication.setMaxCopiersPerAccount(result.getMaxCopiersPerAccount());
			if (result.getCopyAmounts() != null) {
				coApplication.setCopyAmounts(result.getCopyAmounts());
			}

			if(bitmap != null){
				coApplication.getCommunicationManager().uploadAvatarImage(bitmap);
			}
			
			application.postEvent(new LoginLogoutEvent(Type.REGISTER, result));
			application.getSharedPreferencesManager().putBoolean(Constants.PREFERENCES_IS_FIRST_LOGIN, true);

			Screenable afterRegisterScreen = null;
			Bundle bundle = new Bundle();
			NavigationEvent event = new NavigationEvent(afterRegisterScreen, NavigationType.INIT, bundle);
			Log.d(TAG,"Checking Facebook Deep Link Second Screen...");
			if(application.getDeepLinksManager().getAfterRegisterScreen() != null){
				Log.d(TAG,"Facebook Deep Link Second Screen = " + application.getDeepLinksManager().getAfterRegisterScreen());
				afterRegisterScreen = application.getDeepLinksManager().getAfterRegisterScreen();
				application.getDeepLinksManager().setSecondScreen(null);
			}else{
				Log.d(TAG,"NO Facebook Deep Link Second Screen set.");
				afterRegisterScreen = application.getHomeScreen();
			}

			if(result.getUser().isHasBonus()){
				Log.d(TAG, "The user has registered through a Bonus campaign.");
				afterRegisterScreen = Screen.BONUSES;
				event = new NavigationEvent(afterRegisterScreen, NavigationType.INIT, new Bundle());
			}

			event.setToScreen(afterRegisterScreen);
			application.postEvent(event);
		} else {
			if(result.getErrorCode() == Constants.ERROR_CODE_EMAIL_ALREADY_IN_USE || result.getErrorCode() == Constants.ERROR_CODE_EMAIL_EXISTING_IN_AO){
				showExistingEmailPopup();
			}else{
				ErrorUtils.displayFieldError(form, result);
			}
			registerButton.stopLoading();
		}
	}

	@Override
	protected Screenable setupScreen() {
		return COScreen.LSS_SIGNUP_EMPTY;
	}
	
	@Override
	protected void setUserDetalis(Register user) {
		super.setUserDetalis(user);
		user.setPlatformId(Platform.COPYOP);
	}

}