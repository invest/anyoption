package com.copyop.android.app.widget;

import java.util.List;

import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.User;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COTextUtils;

import android.content.Context;
import android.text.Spannable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * View showing amounts and the user can choose only one
 * 
 * @author Mario Kutlev
 */
public class SpecifyAmountView extends RelativeLayout {

	private static final String TAG = SpecifyAmountView.class.getSimpleName();

	private static final int DEFAULT_SELECTED_AMOUNT_INDEX = 1;
	private static final int PULSE_ANIMATION_REPEAT_COUNT  = 3;
	
	private COApplication application;
	private User user;
	private Currency currency;

	private RelativeLayout rootView;
	private LinearLayout smallCirclesLayout;
	private LinearLayout smallAmountsLayout;
	private LinearLayout bigCirclesLayout;

	private List<Long> amounts;

	private int selectedAmountIndex;

	private LinearLayout tooltipslayout;
	private TextView tooltipTextView;
	private LinearLayout pulseLayout;
	private int pulseAnimationRepeatCount;

	private int tooltipIndex;
	
	public SpecifyAmountView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);

		init(context);
	}

	public SpecifyAmountView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public SpecifyAmountView(Context context) {
		this(context, null);
	}

	private void init(Context context) {
		if (isInEditMode()) {
			return;
		}

		application = COApplication.get();
		user = application.getUser();
		currency = user.getCurrency();
		amounts = application.getCopyAmounts();

		rootView = (RelativeLayout) View.inflate(context, R.layout.specify_amount_view, this);

		smallCirclesLayout = (LinearLayout) rootView
				.findViewById(R.id.specify_amount_small_circles);
		smallAmountsLayout = (LinearLayout) rootView
				.findViewById(R.id.specify_amount_small_amounts);
		bigCirclesLayout = (LinearLayout) rootView.findViewById(R.id.specify_amount_big_circles);
		tooltipslayout = (LinearLayout) rootView.findViewById(R.id.specify_amount_tooltip_layout);
		tooltipTextView = (TextView) rootView.findViewById(R.id.specify_amount_tooltip_text);
		pulseLayout = (LinearLayout) rootView.findViewById(R.id.specify_amount_pulse_layout);


		inflateLayout();

		if(amounts != null )
		selectedAmountIndex = -1;
		selectAmount(DEFAULT_SELECTED_AMOUNT_INDEX);
	}

	private void inflateLayout() {

		if (amounts == null || amounts.size() == 0) {
			return;
		}

		int amountsCount = amounts.size();

		for (int i = 0; i < amountsCount; i++) {
			final int amountIndex = i;

			// Add small circle
			View smallCircle = View.inflate(getContext(), R.layout.specify_amount_small_circle,
					null);
			addViewToLayout(smallCircle, smallCirclesLayout);
			smallCircle.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					selectAmount(amountIndex);
				}
			});

			String formattedAmount = AmountUtil.getFormattedAmount(amounts.get(i),
					currency.getSymbol(), currency.getIsLeftSymbolBool(), 0, currency.getId());

			// Add small amount
			View smallAmountLayout = View.inflate(getContext(),
					R.layout.specify_amount_view_small_amount, null);
			TextView smallAmount = (TextView) smallAmountLayout
					.findViewById(R.id.specify_amount_amount_small);
			smallAmount.setText(formattedAmount);
			addViewToLayout(smallAmountLayout, smallAmountsLayout);

			// Add big circle and amount
			View bigCircleLayout = View.inflate(getContext(),
					R.layout.specify_amount_view_big_circle, null);
			TextView bigAmount = (TextView) bigCircleLayout
					.findViewById(R.id.specify_amount_amount_big);
			bigAmount.setText(formattedAmount);
			addViewWithPaddingToLayout(bigCircleLayout, bigCirclesLayout);
		}
	}

	@Override
	protected void onVisibilityChanged(View changedView, int visibility) {
		super.onVisibilityChanged(changedView, visibility);
		if(changedView == this){
			if(visibility == View.VISIBLE){
				pulseAnimationRepeatCount = 0;
				setupPulseAnimation(tooltipIndex);
			}
		}
	}
	
	private void selectAmount(int index) {
		if (selectedAmountIndex == index) {
			return;
		}

		selectedAmountIndex = index;

		for (int i = 0; i < bigCirclesLayout.getChildCount(); i++) {
			bigCirclesLayout.getChildAt(i).setVisibility(View.INVISIBLE);
		}

		bigCirclesLayout.getChildAt(index).setVisibility(View.VISIBLE);
	}

	public void setupTooltip(int index, String text, String amount, String profit){
		tooltipIndex = index;
		for (int i = 0; i < tooltipslayout.getChildCount(); i++) {
			tooltipslayout.getChildAt(i).setVisibility(View.INVISIBLE);
		}
		
		tooltipTextView.setVisibility(View.VISIBLE);
		tooltipTextView.setText(text);
		Spannable spannable = COTextUtils.decorateInnerText(tooltipTextView, null, text, amount, Font.ROBOTO_BOLD, R.color.orange_9, 0);
		COTextUtils.decorateInnerText(tooltipTextView, spannable, text, profit, Font.ROBOTO_BOLD, R.color.orange_9, 0);
		tooltipslayout.getChildAt(index-1).setVisibility(View.VISIBLE);
		
		setupPulseAnimation(index);
	}
	
	private void setupPulseAnimation(int index) {
		for (int i = 0; i < pulseLayout.getChildCount(); i++) {
			pulseLayout.getChildAt(i).setVisibility(View.INVISIBLE);
		}
		pulseLayout.getChildAt(index-1).setVisibility(View.VISIBLE);
		
		final RelativeLayout pulseViews = (RelativeLayout) pulseLayout.getChildAt(index-1);
		final Animation anim1 = AnimationUtils.loadAnimation(getContext(), R.anim.copy_amount_pulse);
		final Animation anim2 = AnimationUtils.loadAnimation(getContext(), R.anim.copy_amount_pulse);
		anim2.setStartOffset(400);
		anim2.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
				pulseAnimationRepeatCount++;
			}
			@Override
			public void onAnimationRepeat(Animation animation) {}
			@Override
			public void onAnimationEnd(Animation animation) {
				if(pulseAnimationRepeatCount <= PULSE_ANIMATION_REPEAT_COUNT){
					pulseViews.getChildAt(0).startAnimation(anim1);
					pulseViews.getChildAt(1).startAnimation(anim2);
				}
			}
		});
		
		pulseViews.getChildAt(0).startAnimation(anim1);
		pulseViews.getChildAt(1).startAnimation(anim2);
	}
	
	/**
	 * Adds the view to the layout. Negative margins are set to reduce view's inner padding.
	 * 
	 * @param view
	 * @param parent
	 */
	private void addViewWithPaddingToLayout(View view, LinearLayout parent) {
		android.widget.LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0,
				LayoutParams.WRAP_CONTENT, 1);

		int topBottomMargin = (int) ScreenUtils.convertDpToPx(getContext(), -8);
		int leftRightMargin = (int) ScreenUtils.convertDpToPx(getContext(), -30);
		params.setMargins(leftRightMargin, topBottomMargin, leftRightMargin, topBottomMargin);

		parent.addView(view, params);
	}

	/**
	 * Adds the view to the layout with the specified weight.
	 * 
	 * @param view
	 * @param weight
	 * @param parent
	 */
	private static void addViewToLayout(View view, LinearLayout parent) {
		parent.addView(view, new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1));
	}

	/**
	 * Returns the selected amount.
	 * 
	 * @return
	 */
	public int getAmount() {
		if (selectedAmountIndex >= 0) {
			// We send the amount without cents
			return (int) (amounts.get(selectedAmountIndex) / 100);
		}

		return 0;
	}

	/**
	 * Selects the given amount.
	 * 
	 * @return
	 */
	public void selectAmountValue(int amount) {
		for (int i = 0; i < amounts.size(); i++) {
			if (amount == amounts.get(i) / 100) {
				selectAmount(i);
				return;
			}
		}

		selectAmount(DEFAULT_SELECTED_AMOUNT_INDEX);
		Log.e(TAG, "selectAmountValue -> Could not select amount: " + amount
				+ "! It is not one of list's values!");
	}

}