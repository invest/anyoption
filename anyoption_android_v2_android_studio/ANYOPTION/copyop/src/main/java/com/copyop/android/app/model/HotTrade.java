package com.copyop.android.app.model;

import com.anyoption.common.beans.base.Market;
import com.copyop.android.app.util.COTimeUtils;

public class HotTrade extends HotItem{

	private static final long serialVersionUID = 1L;
	private Market market;
	protected String marketName;
	protected long time;
	protected String timeFormated;
	
	public String getMarketName() {
		return marketName;
	}
	public long getTime() {
		return time;
	}
	public HotTrade setTime(long time) {
		this.time = time;
		this.timeFormated = COTimeUtils.formatHotTime(this.time);
		return this;
	}
	public String getTimeFormated() {
		return timeFormated;
	}
	public Market getMarket() {
		return market;
	}
	public void setMarket(Market market) {
		this.market = market;
		if(market != null){
			marketName = market.getDisplayName();
		}
	}
}
