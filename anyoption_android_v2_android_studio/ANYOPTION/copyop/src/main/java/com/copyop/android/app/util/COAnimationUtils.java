package com.copyop.android.app.util;

import com.anyoption.android.app.Application;
import com.copyop.android.app.R;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;

public class COAnimationUtils extends AnimationUtils {

	/**
	 * Creates infinite repeating sliding animation from bottom to top.
	 * @param view The view which is going to be animated.
	 */
	public static void startFollowButtonAnimation(final View view){
		Context context = Application.get();
		final Animation anim1 = AnimationUtils.loadAnimation(context, R.anim.follow_button_1);
		final Animation anim2 = AnimationUtils.loadAnimation(context, R.anim.follow_button_2);
		anim1.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {}
			@Override
			public void onAnimationRepeat(Animation animation) {}
			@Override
			public void onAnimationEnd(Animation animation) {
				view.startAnimation(anim2);
			}
		});
		anim2.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {}
			@Override
			public void onAnimationRepeat(Animation animation) {}
			@Override
			public void onAnimationEnd(Animation animation) {
				view.startAnimation(anim1);
			}
		});
		view.startAnimation(anim1);
	}
	
}
