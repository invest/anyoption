package com.copyop.android.app.widget.chart;

import com.anyoption.android.app.widget.chart.ChartView;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.copyop.android.app.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.util.Log;

public class COChartView extends ChartView {

	public static final String TAG = COChartView.class.getSimpleName();

	// Investment bitmaps
	protected Bitmap investmentWinCallCopiedBitmap;
	protected Bitmap investmentWinPutCopiedBitmap;
	protected Bitmap investmentLoseCallCopiedBitmap;
	protected Bitmap investmentLosePutCopiedBitmap;

	public COChartView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public COChartView(Context context) {
		this(context, null);
	}

	public COChartView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

	}

	@Override
	protected void initInvestmentIconBitmaps() {
		Log.d(TAG, "initInvestmentIconBitmaps");
		investmentWinCallBitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.chart_call_win);
		investmentWinCallBitmap.setDensity(Bitmap.DENSITY_NONE);
		investmentWinPutBitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.chart_put_win);
		investmentWinPutBitmap.setDensity(Bitmap.DENSITY_NONE);
		investmentLoseCallBitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.chart_call_lose);
		investmentLoseCallBitmap.setDensity(Bitmap.DENSITY_NONE);
		investmentLosePutBitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.chart_put_lose);
		investmentLosePutBitmap.setDensity(Bitmap.DENSITY_NONE);

		investmentWinCallCopiedBitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.chart_call_win_copied);
		investmentWinCallCopiedBitmap.setDensity(Bitmap.DENSITY_NONE);
		investmentWinPutCopiedBitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.chart_put_win_copied);
		investmentWinPutCopiedBitmap.setDensity(Bitmap.DENSITY_NONE);
		investmentLoseCallCopiedBitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.chart_call_lose_copied);
		investmentLoseCallCopiedBitmap.setDensity(Bitmap.DENSITY_NONE);
		investmentLosePutCopiedBitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.chart_put_lose_copied);
		investmentLosePutCopiedBitmap.setDensity(Bitmap.DENSITY_NONE);
	}

	@Override
	protected Bitmap getInvestmentBitmap(Investment inv) {
		Bitmap investmentBitmap = null;
		boolean isCopied = inv.getCopyopType() == CopyOpInvTypeEnum.COPY;

		if (inv.getTypeId() == Investment.INVESTMENT_TYPE_CALL
				&& currentLevel > inv.getCurrentLevel()) {
			// Win - call
			investmentBitmap = isCopied ? investmentWinCallCopiedBitmap : investmentWinCallBitmap;
		} else if (inv.getTypeId() == Investment.INVESTMENT_TYPE_PUT
				&& currentLevel < inv.getCurrentLevel()) {
			// Win - put
			investmentBitmap = isCopied ? investmentWinPutCopiedBitmap : investmentWinPutBitmap;
		} else if (inv.getTypeId() == Investment.INVESTMENT_TYPE_CALL
				&& currentLevel <= inv.getCurrentLevel()) {
			// Lose - call
			investmentBitmap = isCopied ? investmentLoseCallCopiedBitmap : investmentLoseCallBitmap;
		} else if (inv.getTypeId() == Investment.INVESTMENT_TYPE_PUT
				&& currentLevel >= inv.getCurrentLevel()) {
			// Lose - put
			investmentBitmap = isCopied ? investmentLosePutCopiedBitmap : investmentLosePutBitmap;
		}

		return investmentBitmap;
	}

}
