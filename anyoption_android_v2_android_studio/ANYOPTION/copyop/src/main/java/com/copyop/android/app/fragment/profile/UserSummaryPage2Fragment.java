package com.copyop.android.app.fragment.profile;

import java.text.SimpleDateFormat;

import com.anyoption.android.app.fragment.BaseFragment;
import com.copyop.android.app.R;
import com.copyop.json.results.ProfileMethodResult;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class UserSummaryPage2Fragment extends BaseFragment {

	private static final String DATE_FORMAT = "dd.MM.yyyy";

	private ProfileMethodResult profileInfo;

	private View rootView;
	private TextView sinceView;
	private View riskAppetiteLabel;
	private ImageView riskAppetiteView;
	private ImageView onlineView;
	private TextView descriptionView;

	private boolean isMyProfile;

	/**
	 * Creates new instance of this fragment.
	 */
	public static UserSummaryPage2Fragment newInstance(Bundle bundle) {
		UserSummaryPage2Fragment fragment = new UserSummaryPage2Fragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.user_summary_page_2, container, false);
		sinceView = (TextView) rootView.findViewById(R.id.user_summary_since);
		riskAppetiteLabel = rootView.findViewById(R.id.user_summary_risk_appetite_label);
		riskAppetiteView = (ImageView) rootView.findViewById(R.id.user_summary_risk_appetite);
		onlineView = (ImageView) rootView.findViewById(R.id.user_summary_online);
		descriptionView = (TextView) rootView.findViewById(R.id.user_summary_desc);

		return rootView;
	}

	public void setProfileInfo(ProfileMethodResult profileInfo, boolean isMyProfile) {
		this.profileInfo = profileInfo;
		this.isMyProfile = isMyProfile;
		updateInfo();
	}

	private void updateInfo() {
		if (profileInfo.getTimeCreated() != null) {
			SimpleDateFormat formatDate = new SimpleDateFormat(DATE_FORMAT);
			sinceView.setText(formatDate.format(profileInfo.getTimeCreated()));
		}

		if (profileInfo.getRiskAppetite() > 0) {
			riskAppetiteLabel.setVisibility(View.VISIBLE);
			riskAppetiteView.setVisibility(View.VISIBLE);

			int riskImageId = 0;
			switch (profileInfo.getRiskAppetite()) {
			case 1:
				riskImageId = R.drawable.risk_lover_1;
				break;
			case 2:
				riskImageId = R.drawable.risk_lover_2;
				break;
			case 3:
				riskImageId = R.drawable.risk_lover_3;
				break;
			default:
				riskImageId = R.drawable.risk_lover_0;
				break;
			}

			riskAppetiteView.setImageResource(riskImageId);
		} else {
			riskAppetiteLabel.setVisibility(View.GONE);
			riskAppetiteView.setVisibility(View.GONE);
		}

		if (isMyProfile) {
			onlineView.setVisibility(View.GONE);
		} else {
			onlineView.setVisibility(View.VISIBLE);
			onlineView.setImageResource(profileInfo.isOnline() ? R.drawable.online_icon
					: R.drawable.offline_icon);
		}

		descriptionView.setText(profileInfo.getDescription());
	}

}
