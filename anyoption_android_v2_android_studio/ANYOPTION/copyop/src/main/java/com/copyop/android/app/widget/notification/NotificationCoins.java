package com.copyop.android.app.widget.notification;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NotificationCoins extends LinearLayout{

	private String message;
	
	public NotificationCoins(String message) {
		super(Application.get());
		this.message = message;
		init(Application.get(), null, 0);
	}
	public NotificationCoins(Context context) {
		super(context);
		init(context, null, 0);
	}
	public NotificationCoins(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	public NotificationCoins(Context context, AttributeSet attrs,int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		View view = View.inflate(context, R.layout.notification_coins, this);
		TextView messageTextView = (TextView) view.findViewById(R.id.notification_coins_message_text_view);
		messageTextView.setText(message);
		View viewCoinsButton = view.findViewById(R.id.notification_coins_button);
		viewCoinsButton.setOnClickListener(new OnClickListener() {
			private boolean isPressed = false;
			@Override
			public void onClick(View v) {
				if(!isPressed){
					isPressed = true;
					goToCoins();
				}
			}
		});
	}

	private void goToCoins(){
		Application.get().postEvent(new NavigationEvent(COScreen.COPYOP_COINS, NavigationType.INIT));
	}
}
