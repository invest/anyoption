package com.copyop.android.app.fragment.trade;

import com.anyoption.android.app.fragment.trade.SimpleChartPagerFragment;
import com.copyop.android.app.R;

import android.os.Bundle;

public class COSimpleChartPagerFragment extends SimpleChartPagerFragment {

	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static COSimpleChartPagerFragment newInstance(Bundle bundle){
		COSimpleChartPagerFragment coSimpleChartsFragment = new COSimpleChartPagerFragment();
		coSimpleChartsFragment.setArguments(bundle);
		return coSimpleChartsFragment;
	}
	
	@Override
	protected int getCurrentDot() {
		return R.drawable.simple_chart_pager_dot_current;
	}

	@Override
	protected int getOtherDot() {
		return R.drawable.simple_chart_pager_dot_other;
	}

}
