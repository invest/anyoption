package com.copyop.android.app.fragment.news_feed;

import com.copyop.android.app.COApplication;
import com.copyop.android.app.util.COConstants;
import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.enums.base.QueueTypeEnum;

import android.os.Bundle;

/**
 * Displays {@link FeedMessage} updates in a List.
 * @author Anastas Arnaudov
 *
 */
public class ExploreFragment extends NewsFragment{

	public final String TAG = ExploreFragment.class.getSimpleName();

	public static ExploreFragment newInstance(Bundle bundle){
		ExploreFragment fragment = new ExploreFragment();
		fragment.setArguments(bundle);
		return fragment;
	}
	
	//#####################################################################
	//#########					REQUEST - RESPONSE				###########
	//#####################################################################	
	protected String getNewsRequestName(){
		return COConstants.SERVICE_GET_COPYOP_EXPLORE;
	}
	
	//#####################################################################

	//#####################################################################
	//#########					LIGHSTREAMER					###########
	//#####################################################################
	
	@Override
	protected void attachLightstreamer(){
		COApplication.get().getLightstreamerManager().subscribeExploreTable(ExploreFragment.this);
	}
	
	@Override
	protected void detachLightstreamer(){
		COApplication.get().getLightstreamerManager().unSubscribeExploreTable();
	}
	
	protected QueueTypeEnum getQueueType(){
		return QueueTypeEnum.EXPLORE;
	}
	//#####################################################################
}
