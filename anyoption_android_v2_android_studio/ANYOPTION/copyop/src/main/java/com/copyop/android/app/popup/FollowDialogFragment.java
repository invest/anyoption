package com.copyop.android.app.popup;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.lightstreamer.LightstreamerListener;
import com.anyoption.android.app.manager.LightstreamerManager;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.popup.InvestmentDev2FailedDialogFragment;
import com.anyoption.android.app.popup.LowCashBalanceDialogFragment;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.BitmapUtils;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.InvestmentUtil;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.service.requests.InsertInvestmentMethodRequest;
import com.anyoption.common.service.results.InvestmentMethodResult;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.R;
import com.copyop.android.app.manager.COLightstreamerManager;
import com.copyop.android.app.util.COConstants;
import com.copyop.android.app.widget.ChooseAmountView;
import com.copyop.android.app.widget.ChooseAmountView.OnAmountChangeListener;
import com.copyop.android.app.widget.TimerView;
import com.copyop.android.app.widget.TimerView.OnFinishListener;
import com.lightstreamer.ls_client.UpdateInfo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FollowDialogFragment extends BaseDialogFragment implements OnClickListener,
        LightstreamerListener, OnAmountChangeListener, Callback, View.OnTouchListener {

    private static final String TAG = FollowDialogFragment.class.getSimpleName();

    private static final int UPDATE_VIEWS_MESSAGE = 1;
    private static final int DISABLE_BUY_MESSAGE = 2;
    private static final int START_TIMER_MESSAGE = 3;

    private static final String TIME_FORMAT_PATTERN = "HH:mm";
    private static final long TIMER_DURATION = 6 * 1000;

    private boolean closeOnRetry = false;
    private boolean isFollowButtonClicked = false;

    private User user;
    private SkinGroup skinGroup;
    private COLightstreamerManager lightstreamerManager;
    private Currency currency;

    /**
     * Used to keep all the info needed for an investment.
     */
    protected Investment investment;

    private long followedInvestmentId;
    private int investmentType;
    private Market market;
    private long marketId = 0;

    private long opportunityId;
    private int opportunityState;
    private double investAmount;
    private double currentLevel;
    private float loseOdds;
    private float winOdds;

    private String currentLevelTxt;
    private String expiryTimeTxt;
    private String winAmountTxt;
    private String loseAmountTxt;
    private int winPercent;

    private TimerView timer;
    private ChooseAmountView amountView;
    private View buyButton;
    private View retryLayout;
    private View shadowLayout;
    private View errorBoxLayout;
    private ImageView errorBoxPointer;
    private TextView errorBoxText;

    private float retryLayoutX;
    private float retryLayoutY;

    private TextView currentLevelView;
    private TextView expiryTimeView;
    private TextView profitView;
    private TextView ifCorrectAmountView;
    private TextView ifIncorrectAmountView;

    private boolean shouldDismiss;

    private Handler handler;

    /**
     * Use this method to create new instance of the fragment.
     *
     * @return
     */
    public static FollowDialogFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new FollowDialogFragment.");
        FollowDialogFragment fragment = new FollowDialogFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        closeOnRetry = false;

        user = Application.get().getUser();
        currency = user.getCurrency();
        Skin skin = application.getSkins().get(application.getSkinId());
        skinGroup = skin.getSkinGroup();
        lightstreamerManager = (COLightstreamerManager) application.getLightstreamerManager();

        Bundle args = getArguments();
        if (args != null) {
            marketId = args.getLong(COConstants.EXTRA_MARKET_ID, marketId);
            followedInvestmentId = args.getLong(COConstants.EXTRA_INVESTMENT_ID, 0);
            investmentType = args.getInt(COConstants.EXTRA_INVESTMENT_TYPE, -1);
        }

        shouldDismiss = false;

        if (marketId == 0 || followedInvestmentId == 0 || investmentType == -1) {
            Log.e(TAG,
                    "Follow dialog can not be created! You must add marketId, followedInvestmentId and investmentType to the arguments!");
            shouldDismiss = true;
            return;
        }

        market = application.getMarketForID(marketId);
        if (market == null) {
            Log.e(TAG, "Follow dialog can not be created! Can not find market with id: " + marketId);
            shouldDismiss = true;
            return;
        }

        investment = new Investment();
        investment.setTypeId(investmentType);
        investAmount = application.getDefaultInvestAmount();
        investment.setAmount(AmountUtil.getLongAmount(investAmount));

        currentLevel = -1;

        handler = new Handler(this);
    }

    /**
     * Set up content.
     *
     * @param contentView
     */
    @Override
    protected void onCreateContentView(View contentView) {
        if (shouldDismiss) {
            return;
        }

        buyButton = contentView.findViewById(R.id.follow_inv_buy_button);
        buyButton.setOnClickListener(this);
        retryLayout = contentView.findViewById(R.id.follow_dialog_retry_layout);
        retryLayout.setOnClickListener(this);
        retryLayout.setOnTouchListener(this);
        retryLayoutX = retryLayout.getX();
        retryLayoutY = retryLayout.getY();

        shadowLayout = contentView.findViewById(R.id.follow_dialog_shadow_layout);
        errorBoxText = (TextView) contentView.findViewById(R.id.follow_dialog_error_box_text_view);
        errorBoxLayout = contentView.findViewById(R.id.follow_dialog_error_box);
        errorBoxPointer = (ImageView) contentView.findViewById(R.id.follow_dialog_error_box_pointer_image_view);

        int resID = DrawableUtils.getMarketIconResID(market);
        ImageView assetIcon = (ImageView) contentView.findViewById(R.id.follow_inv_asset_icon);
        BitmapUtils.loadBitmap(resID, assetIcon);

        currentLevelView = (TextView) contentView.findViewById(R.id.follow_inv_level);

        TextView assetNameTextView = (TextView) contentView
                .findViewById(R.id.follow_inv_asset_name);
        assetNameTextView.setText(market.getDisplayName());

        expiryTimeView = (TextView) contentView
                .findViewById(R.id.follow_inv_expiry);

        amountView = (ChooseAmountView) contentView.findViewById(R.id.follow_inv_amount);
        amountView.setAmount(investAmount);
        amountView.setAmountChangeListener(this);

        TextView profitLabelTextView = (TextView) contentView
                .findViewById(R.id.follow_inv_profit_label);
        profitLabelTextView.setText(getString(R.string.followInvProfit) + ":");

        profitView = (TextView) contentView
                .findViewById(R.id.follow_inv_profit_percent);

        ImageView putCallViewSmall = (ImageView) contentView
                .findViewById(R.id.follow_inv_put_call_icon_small);
        ImageView putCallViewBig = (ImageView) contentView
                .findViewById(R.id.follow_inv_put_call_icon_big);

        if (investmentType == Investment.INVESTMENT_TYPE_CALL
                || (investmentType == Investment.INVESTMENT_TYPE_ONE && investmentType == Investment.INVESTMENT_TYPE_CALL)) {
            // Call icon
            putCallViewSmall.setImageResource(R.drawable.follow_inv_call_icon_small);
            putCallViewBig.setImageResource(R.drawable.follow_inv_call_icon_big);
        } else if (investmentType == Investment.INVESTMENT_TYPE_PUT
                || (investmentType == Investment.INVESTMENT_TYPE_ONE && investmentType == Investment.INVESTMENT_TYPE_PUT)) {
            // Put icon
            putCallViewSmall.setImageResource(R.drawable.follow_inv_put_icon_small);
            putCallViewBig.setImageResource(R.drawable.follow_inv_put_icon_big);
        }

        ifCorrectAmountView = (TextView) contentView
                .findViewById(R.id.follow_inv_if_correct_amount);
        ifIncorrectAmountView = (TextView) contentView
                .findViewById(R.id.follow_inv_if_incorrect_amount);

        setUpTimer(contentView);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (shouldDismiss) {
            dismiss();
            return;
        }

        registerLightstreamer();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        unregisterLightStreamer();
        stopTimer();

        super.onDismiss(dialog);
    }

    @Override
    protected int getContentLayout() {
        return R.layout.follow_investment_dialog;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == buyButton.getId()) {
            follow();
            //Send Buy Now event to Appsee
            ((COApplication)application).getAppseeManager().onBuyNowPopup();
        } else if (v.getId() == retryLayout.getId()) {
            reloadPopup();
        }
    }

    private void setUpTimer(View contentView) {
        timer = (TimerView) contentView.findViewById(R.id.follow_inv_timer);
        timer.setFinishListener(new OnFinishListener() {

            @Override
            public void onFinish() {
                dismissAllowingStateLoss();
            }
        });
        timer.setTimeFormat(TimerView.TIME_FORMAT_SECONDS_AND_MILLISECONDS);
        timer.setDuration(TIMER_DURATION);
    }

    private void startTimer() {
        timer.start();
        amountView.setEnabled(true);
    }

    private void stopTimer() {
        if (timer != null) {
            timer.stop();
        }
        if(amountView != null) {
            amountView.setEnabled(false);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////
    ///////////                     REQUESTs and RESPONSEs                          ///////////
    ///////////////////////////////////////////////////////////////////////////////////////////

    private void follow() {
        if (currentLevel < 0) {
            // Current level is not received from LS
            return;
        }

        stopTimer();
        //unregisterLightStreamer();
        isFollowButtonClicked = true;

        if (AmountUtil.getDoubleAmount(user.getBalance()) < investAmount) {
            application.getPopUpManager().showInsufficienFundsPopUp();
            dismiss();
            return;
        }

        InsertInvestmentMethodRequest request = new InsertInvestmentMethodRequest();
        request.setCopyopInvId(followedInvestmentId);
        request.setPageLevel(currentLevel);
        request.setPageOddsLose(loseOdds);
        request.setPageOddsWin(winOdds);
        request.setChoice(investmentType);
        request.setOpportunityId(opportunityId);
        request.setRequestAmount(investAmount);

        boolean isRequestOK = application.getCommunicationManager().requestService(this, "followCallBack", COConstants.SERVICE_FOLLOW, request, InvestmentMethodResult.class, false, false);

        if (isRequestOK) {
            buyButton.setClickable(false);

            //Save the Investment that is currently being inserted:
            investment.setTypeId(investmentType);
            investment.setCurrentLevel(currentLevel);
            investment.setLevel(Utils.getFormattedLevel(currentLevel, market.getDecimalPoint()));
            investment.setOddsLose(loseOdds);
            investment.setOddsWin(winOdds);
            investment.setOpportunityId(opportunityId);
            investment.setAmount(AmountUtil.getLongAmount(investAmount));
            //////////////////////////////////////////////////////
        } else {
            dismiss();
        }
    }

    public void followCallBack(Object resultObj) {
        InvestmentMethodResult result = (InvestmentMethodResult) resultObj;

        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            if (result.getUser() != null) {
                application.setUser(result.getUser());
            }

            final Investment investment;
            //Check the Deviation2 Check:
            final int dev2Seconds = result.getDev2Seconds();
            if(dev2Seconds > 0){
                investment = this.investment;
            }else{
                investment = result.getInvestment();
                InvestmentUtil.fixDate(investment);
            }

            if(dev2Seconds > 0){
                Log.d(TAG, "We have Deviation2 check. Waiting for " + dev2Seconds + " seconds to finish the investment!");
                //We have Deviation2 check with the given number of seconds to wait until we can finish the investment:
                new CountDownTimer(dev2Seconds * 1000, dev2Seconds * 1000){
                    @Override
                    public void onTick(long millisUntilFinished) {}
                    @Override
                    public void onFinish() {
                        if(isAdded()){
                            Log.d(TAG, "Deviation2 check time finished!");
                            finishInvestD2(investment);
                        }
                    }
                }.start();
            }else{
                //No Deviation2 Check:
                application.getNotificationManager().showNotificationInvestOK(result.getInvestment());
                dismiss();
            }


        } else if (result != null && result.getErrorCode() == Constants.ERROR_CODE_NO_CASH_FOR_NIOU) {
            dismiss();
            Bundle bundle = new Bundle();
            bundle.putString(Constants.EXTRA_CASH_BALANCE, AmountUtil.getUserFormattedAmount(Long.valueOf(result.getUserCashBalance())));
            Application.get().getFragmentManager().showDialogFragment(LowCashBalanceDialogFragment.class, bundle);
        } else {
            if (result != null) {
                Log.e(TAG, "Could not invest. ErrorCode: " + result.getErrorCode());
            } else {
                Log.e(TAG, "Could not invest. ErrorCode: null");
            }

            showShadowAndDisableBuyButton(result.getUserMessages()[0].getMessage());
        }

    }

    private void finishInvestD2(Investment investment){
        Log.d(TAG, "Finishing the Deviation2 check investment...");
        InsertInvestmentMethodRequest request = new InsertInvestmentMethodRequest();
        request.setCopyopInvId(followedInvestmentId);
        request.setPageLevel(investment.getCurrentLevel());
        request.setPageOddsLose((float) investment.getOddsLose());
        request.setPageOddsWin((float) investment.getOddsWin());
        request.setChoice((int) investment.getTypeId());
        request.setOpportunityId(investment.getOpportunityId());
        request.setRequestAmount(AmountUtil.getDoubleAmount(investment.getAmount()));
        request.setDev2Second(true);

        application.getCommunicationManager().requestService(this, "finishInvestD2CallBack", COConstants.SERVICE_FOLLOW, request, InvestmentMethodResult.class, false, false);

    }

    public void finishInvestD2CallBack(Object resultObj){
        Log.d(TAG, "finishInvestD2CallBack");
        InvestmentMethodResult result = (InvestmentMethodResult) resultObj;
        if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            final Investment investment = result.getInvestment();
            InvestmentUtil.fixDate(investment);
            if (result.getUser() != null) {
                application.setUser(result.getUser());
            }
            application.getNotificationManager().showNotificationInvestOK(result.getInvestment());
            dismiss();
        } else {
            String errorMessage;
            if(result.getUserMessages() != null && result.getUserMessages()[0].getMessage() != null){
                errorMessage = result.getUserMessages()[0].getMessage();
            }else{
                errorMessage = getString(com.anyoption.android.R.string.error);
            }
            showShadowAndDisableBuyButton(errorMessage);
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    private void registerLightstreamer() {
        unregisterLightStreamer();

        Log.d(TAG, "subscribeFollowTable -> market: " + market.getDisplayName());
        lightstreamerManager.subscribeFollowTable(this, market.getId());
    }

    private void unregisterLightStreamer() {
        lightstreamerManager.unSubscribeFollowTable();
    }

    @Override
    public void updateItem(int itemPos, String itemName, UpdateInfo update) {
        boolean updated = false;

        if (!isFollowButtonClicked) {
            if (opportunityId == 0L || update.isValueChanged(LightstreamerManager.COLUMN_ET_OPP_ID)) {
                opportunityId = Long.parseLong(update
                        .getNewValue(LightstreamerManager.COLUMN_ET_OPP_ID));
            }


            if (update.isValueChanged(skinGroup.getLevelUpdateKey())) {
                currentLevelTxt = update.getNewValue(skinGroup.getLevelUpdateKey());
                if (currentLevel < 0) {
                    handler.obtainMessage(START_TIMER_MESSAGE).sendToTarget();
                }
                currentLevel = Double.parseDouble(currentLevelTxt.replaceAll(",", ""));
                investment.setLevel(currentLevelTxt);
                investment.setCurrentLevel(currentLevel);
                updated = true;
            }

            if (update.isValueChanged(LightstreamerManager.COLUMN_ET_ODDS_LOSE) || loseOdds == 0
                    || update.isValueChanged(LightstreamerManager.COLUMN_ET_ODDS_WIN) || winOdds == 0) {
                // Odds lose
                loseOdds = Float.parseFloat(update
                        .getNewValue(LightstreamerManager.COLUMN_ET_ODDS_LOSE));
                investment.setOddsLose(loseOdds);

                // Odds win
                winOdds = Float.parseFloat(update.getNewValue(LightstreamerManager.COLUMN_ET_ODDS_WIN));
                investment.setOddsWin(winOdds);
                winPercent = Math.round((winOdds - 1) * 100);

                calculateWinLose();

                updated = true;
            }

            if (update.isValueChanged(LightstreamerManager.COLUMN_ET_EST_CLOSE)
                    || expiryTimeTxt == null) {
                String timeCloseString = update.getNewValue(LightstreamerManager.COLUMN_ET_EST_CLOSE);
                try {
                    Date timeClose = TimeUtils.parseTimeFromLSUpdate(timeCloseString);
                    SimpleDateFormat expiryTimeFormat = new SimpleDateFormat(TIME_FORMAT_PATTERN);
                    expiryTimeTxt = " | " + getString(R.string.followByTimeToday,
                            expiryTimeFormat.format(timeClose));
                } catch (ParseException e) {
                    Log.e(TAG, "Could not parse ET_EST_CLOSE time");
                }

                updated = true;
            }
        }
        if (update.isValueChanged(LightstreamerManager.COLUMN_ET_STATE) || opportunityState == 0) {
            opportunityState = Integer.parseInt(update
                    .getNewValue(LightstreamerManager.COLUMN_ET_STATE));
            if (opportunityState != Opportunity.STATE_OPENED
                    && opportunityState != Opportunity.STATE_LAST_10_MIN) {
                // The option is not opened.
                Log.d(TAG, "This market is not opened! -> market: " + market.getDisplayName());

                if (!isFollowButtonClicked) {
                    handler.obtainMessage(DISABLE_BUY_MESSAGE).sendToTarget();
                } else {
                    closeOnRetry = true;
                }
            } else {
                closeOnRetry = false;
            }
        }

        if (!isFollowButtonClicked) {
            if (updated && !handler.hasMessages(UPDATE_VIEWS_MESSAGE)) {
                handler.obtainMessage(UPDATE_VIEWS_MESSAGE).sendToTarget();
            }
        }

    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case UPDATE_VIEWS_MESSAGE:
                updateViews();
                break;
            case DISABLE_BUY_MESSAGE:
                disableBuy();
                break;
            case START_TIMER_MESSAGE:
                startTimer();
                break;
        }

        return true;
    }

    @Override
    public void onAmountChange(double newAmount) {
        investAmount = newAmount;
        investment.setAmount(AmountUtil.getLongAmount(investAmount));
        calculateWinLose();
        updateViews();
    }

    private void updateViews() {
        if (!isAdded()) {
            return;
        }

        currentLevelView.setText(currentLevelTxt);
        expiryTimeView.setText(expiryTimeTxt);
        profitView.setText(winPercent + "%");
        ifCorrectAmountView.setText(winAmountTxt);
        ifIncorrectAmountView.setText(loseAmountTxt);
    }

    private void calculateWinLose() {
        double winAmount = 0;
        double loseAmount = 0;

        if (investmentType == Investment.INVESTMENT_TYPE_CALL
                || (investmentType == Investment.INVESTMENT_TYPE_ONE && investmentType == Investment.INVESTMENT_TYPE_CALL)) {
            winAmount = InvestmentUtil.getOpenInvestmentReturn(investment,
                    investment.getCurrentLevel() + 1);
            loseAmount = InvestmentUtil.getOpenInvestmentReturn(investment,
                    investment.getCurrentLevel() - 1);
        } else if (investmentType == Investment.INVESTMENT_TYPE_PUT
                || (investmentType == Investment.INVESTMENT_TYPE_ONE && investmentType == Investment.INVESTMENT_TYPE_PUT)) {

            winAmount = InvestmentUtil.getOpenInvestmentReturn(investment,
                    investment.getCurrentLevel() - 1);
            loseAmount = InvestmentUtil.getOpenInvestmentReturn(investment,
                    investment.getCurrentLevel() + 1);
        }

        winAmountTxt = AmountUtil.getFormattedAmount(winAmount, currency);
        loseAmountTxt = AmountUtil.getFormattedAmount(loseAmount, currency);
    }

    private void disableBuy() {
        if (!isAdded()) {
            return;
        }

        amountView.setEnabled(false);

        buyButton.setClickable(false);
        buyButton.setBackgroundResource(R.drawable.dialog_action_button_orange_rounded_disabled);
    }

    private void showShadowAndDisableBuyButton(String message) {
        if (!isAdded()) {
            return;
        }
        amountView.setEnabled(false);
        errorBoxText.setText(message);
        buyButton.setClickable(false);
        shadowLayout.setVisibility(View.VISIBLE);
        errorBoxPointer.setVisibility(View.VISIBLE);
        errorBoxLayout.setVisibility(View.VISIBLE);

        buyButton.setBackgroundResource(R.drawable.dialog_action_button_orange_rounded_disabled_shadow);
    }

    private void reloadPopup() {
        if (closeOnRetry) {
            dismiss();
            application.getNotificationManager().showNotificationError(getString(R.string.error_investment_expired));
        } else {
            retryLayout.setBackgroundResource(R.drawable.circle_buy_now_retry_clicked);
            dismiss();
            Bundle bundle = new Bundle();
            bundle.putLong(COConstants.EXTRA_MARKET_ID, marketId);
            bundle.putLong(COConstants.EXTRA_INVESTMENT_ID, followedInvestmentId);
            bundle.putInt(COConstants.EXTRA_INVESTMENT_TYPE, investmentType);
            FollowDialogFragment followInvestmentDialogFragment = FollowDialogFragment.newInstance(bundle);
            Application.get().getFragmentManager().showDialogFragment(followInvestmentDialogFragment);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (v.getId() == retryLayout.getId()) {
            if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE) {
                retryLayout.animate().setDuration(0).translationY(retryLayoutY + 10);
                retryLayout.setBackgroundResource(R.drawable.circle_buy_now_retry_clicked);
            } else {
                retryLayout.animate().setDuration(0).translationY(retryLayoutY);
                retryLayout.setBackgroundResource(R.drawable.circle_buy_now_retry);
            }
        }
        return false;
    }
}
