package com.copyop.android.app.popup;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.results.MethodResult;
import com.copyop.android.app.R;
import com.copyop.android.app.manager.COFacebookManager;
import com.copyop.android.app.util.COConstants;
import com.copyop.android.app.util.COTimeUtils;
import com.copyop.json.requests.ShowOffMethodRequest;

import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class ShowOffDialogFragment extends TextDialogFragment {

	private static final String TAG = ShowOffDialogFragment.class.getSimpleName();

	private long investmentId;
	private long amount;
	private long marketId;
	private long expiryTime;
	private String showOffDesc;

	/**
	 * Use this method to create new instance of the dialog.
	 * 
	 * @return
	 */
	public static ShowOffDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new ShowOffDialogFragment.");

		Application application = Application.get();
		bundle.putString(TITLE_ARG_KEY, application.getString(R.string.showOff));
		bundle.putString(DISMISS_TEXT_ARG_KEY, application.getString(R.string.cancel));
		bundle.putString(ACTION_TEXT_ARG_KEY, application.getString(R.string.showOff));

		ShowOffDialogFragment fragment = new ShowOffDialogFragment();
		fragment.setArguments(bundle);

		return fragment;
	}

	public static ShowOffDialogFragment newInstance(long investmentId, long amount, long marketId,
			long expiryTime) {
		Bundle args = new Bundle();
		args.putLong(COConstants.EXTRA_INVESTMENT_ID, investmentId);
		args.putLong(COConstants.EXTRA_AMOUNT, amount);
		args.putLong(COConstants.EXTRA_MARKET_ID, marketId);
		args.putLong(COConstants.EXTRA_EXPIRY_TIME, expiryTime);

		return newInstance(args);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		investmentId = 0;
		amount = 0;
		marketId = 0;
		expiryTime = 0;

		Bundle args = getArguments();
		if (args != null) {
			investmentId = args.getLong(COConstants.EXTRA_INVESTMENT_ID, 0);
			amount = args.getLong(COConstants.EXTRA_AMOUNT, 0);
			marketId = args.getLong(COConstants.EXTRA_MARKET_ID, 0);
			expiryTime = args.getLong(COConstants.EXTRA_EXPIRY_TIME, 0);
		}

		if (investmentId == 0 || amount == 0 || marketId == 0 || expiryTime == 0) {
			Log.e(TAG,
					"Missing parameters!!! Make sure all parameters are provided: investmentId, amount, marketId or expiryTime");
		}
	}

	@Override
	protected void onCreateContentView(View contentView) {
		super.onCreateContentView(contentView);

		String amountText = AmountUtil.getUserFormattedAmount(AmountUtil.getDoubleAmount(amount));
		String marketName = application.getMarketForID(marketId).getDisplayName();
		String expiryText = COTimeUtils.formatNewsFeedExpireTime(expiryTime);
		showOffDesc = getString(R.string.newsJustMade, amountText, marketName, expiryText);

		descriptionView.setText(showOffDesc);
		makeTextBold(descriptionView, new SpannableStringBuilder(), amountText);
		makeTextBold(descriptionView, new SpannableStringBuilder(), marketName);

		setActionButtonListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showOff();
			}
		});
	}

	private static void makeTextBold(final TextView textview, Spannable spannable, String text) {
		if (text == null || text.isEmpty()) {
			return;
		}

		TextUtils.makeClickableInnerText(textview, spannable, textview.getText().toString(), text,
				Font.ROBOTO_BOLD, 0, R.color.inbox_item_content_text, false, null);
	}

	private void showOff() {
		ShowOffMethodRequest request = new ShowOffMethodRequest();
		request.setInvestmentId(investmentId);

		application.getCommunicationManager().requestService(this, "showOffCallBack",
				COConstants.SERVICE_SHOW_OFF, request, MethodResult.class, true, false);
	}

	public void showOffCallBack(Object resultObj) {
		MethodResult result = (MethodResult) resultObj;

		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Show off successful(invId: " + investmentId + ")");
		} else {
			if (result != null) {
				Log.e(TAG, "Show off error! ErrorCode: " + result.getErrorCode());
			} else {
				Log.e(TAG, "Show off error! Result = null");
			}
		}

		dismiss();
		COFacebookManager.publishStory(getActivity(), this, showOffDesc);
	}

}