package com.copyop.android.app.popup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.anyoption.android.app.popup.ExistingEmailDialogfragment;
import com.anyoption.android.app.widget.TextView;
import com.copyop.android.app.R;

/**
 * @author Anastas Arnaudov
 */
public class COExistingEmailDialogfragment extends ExistingEmailDialogfragment {

    public static final String TAG = COExistingEmailDialogfragment.class.getSimpleName();

    public static COExistingEmailDialogfragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new AOExistingEmailDialogfragment.");
        COExistingEmailDialogfragment fragment = new COExistingEmailDialogfragment();
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, true);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getContentLayout() {
        return R.layout.popup_existing_email;
    }

    @Override
    protected void onCreateContentView(View contentView) {
        mainLayout         =            contentView.findViewById(R.id.existingEmailPopupLayout);
        messageTextView    = (TextView) contentView.findViewById(R.id.existingEmailPopupMessageTextView);
        changeEmailButton  =            contentView.findViewById(R.id.existingEmailPopupChangeEmailButton);
        recoverPassButton  =            contentView.findViewById(R.id.existingEmailPopupRecoverPassButton);

        super.onCreateContentView(contentView);
    }
}

