package com.copyop.android.app.widget;

import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.widget.TextView;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COAnimationUtils;
import com.copyop.android.app.util.CODrawableUtils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.RelativeLayout;

public class FollowButton extends ClickableLayout {

	public enum FollowButtonState{
		NORMAL, DISABLED;
	}
	
	protected boolean isAnimating;
	protected TextView textView;
	protected View arrowImageView;
	protected View contentView;
	private FollowButtonState followButtonState;
	private Drawable disabledBackground;
	private View circleShadowImageView;
	private View circleForegroundView;
	
	public FollowButton(Context context) {
		super(context);
		init(context, null, 0);
	}
	
	public FollowButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	
	public FollowButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs, defStyleAttr);
	}

	protected int getContentViewId(){
		return R.layout.follow_button_content;
	}
	
	protected void init(Context context, AttributeSet attrs, int defStyleAttr) {
		if(contentView == null){
			contentView = View.inflate(context, getContentViewId(), null);
			LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.addRule(RelativeLayout.CENTER_IN_PARENT);
			
			addView(contentView, params);
			arrowImageView = contentView.findViewById(R.id.follow_button_image_view);
			circleShadowImageView = contentView.findViewById(R.id.follow_button_circle_shadow_image_view);
			circleForegroundView = contentView.findViewById(R.id.follow_button_circle_foreground_view);
			textView = (TextView) findViewById(R.id.follow_button_text_view);
			setupAttributes(context, attrs, defStyleAttr);
			if(!isInEditMode()){
				int disabledColorBG = ColorUtils.getColor(R.color.button_disabled_bg);
				int disabledShadowColor = ColorUtils.getColor(R.color.button_disabled_shadow);
				disabledBackground = CODrawableUtils.createClickableDrawable(context, disabledColorBG, disabledShadowColor, shadowSize, isBorder, false);
			}else{
				setBackgroundColor(color);
			}
		}
		super.init(context, attrs, defStyleAttr);
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
		startAnimation();
	}
	
	private void setupAttributes(Context context, AttributeSet attrs, int defStyleAttr){
		// Get custom attributes:
		TypedArray attributesTypedArray = context.obtainStyledAttributes(attrs,R.styleable.FollowButton);
		int textColor = attributesTypedArray.getColor(R.styleable.FollowButton_textColor, ColorUtils.getColor(R.color.default_text));
		int textSize = (int)attributesTypedArray.getDimension(R.styleable.FollowButton_textSize, getResources().getDimension(R.dimen.default_text));
		int fontID = attributesTypedArray.getInt(R.styleable.FollowButton_font, Font.ROBOTO_REGULAR.getFontID());
		Font font = FontsUtils.getFontForFontID(fontID);
		attributesTypedArray.recycle();
		
		textView.setTextColor(textColor);
		textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
		textView.setFont(font);
		//////////////////////////	
	}
	
	public void startAnimation(){
		if(!isInEditMode()){
			if(!isAnimating){
				isAnimating = true;
				COAnimationUtils.startFollowButtonAnimation(arrowImageView);
			}
		}
	}

	public FollowButtonState getFollowButtonState() {
		return followButtonState;
	}

	public void setFollowButtonState(FollowButtonState followButtonState) {
		this.followButtonState = followButtonState;
		if(this.followButtonState == FollowButtonState.NORMAL){
			updateBackground(normalBackground);
			circleShadowImageView.setVisibility(View.VISIBLE);
			circleForegroundView.setVisibility(View.INVISIBLE);
			setEnabled(true);
		}else if(this.followButtonState == FollowButtonState.DISABLED){
			updateBackground(disabledBackground);
			circleShadowImageView.setVisibility(View.INVISIBLE);
			circleForegroundView.setVisibility(View.VISIBLE);
			setEnabled(false);
		}
	}
	
}
