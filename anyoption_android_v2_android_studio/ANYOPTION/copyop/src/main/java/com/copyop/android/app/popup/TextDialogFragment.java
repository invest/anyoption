package com.copyop.android.app.popup;

import com.anyoption.android.app.popup.BaseDialogFragment;
import com.copyop.android.app.R;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

/**
 * Simple dialog which with title, text and one or two buttons.
 */
public class TextDialogFragment extends BaseDialogFragment implements OnClickListener {

	private static final String TAG = TextDialogFragment.class.getSimpleName();

	public static final String TITLE_ARG_KEY = "title_arg_key";
	public static final String TEXT_ARG_KEY = "text_arg_key";
	public static final String DISMISS_TEXT_ARG_KEY = "dismiss_text_arg_key";
	public static final String ACTION_TEXT_ARG_KEY = "action_text_arg_key";
	public static final String IS_DISMISS_GRAY_ARG_KEY = "is_dismiss_gray_arg_key";

	protected TextView titleView;
	protected TextView descriptionView;
	private TextView okButton;
	private TextView dismissButtonBig;
	private TextView dismissButton;
	private TextView actionButton;

	private String title;
	private String text;
	private String dismiss;
	private String action;
	private boolean isDismissGray;

	/**
	 * Use this method to create new instance of the dialog.
	 * 
	 * @return
	 */
	public static TextDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new TextDialogFragment.");
		TextDialogFragment fragment = new TextDialogFragment();
		fragment.setArguments(bundle);

		return fragment;
	}

	public static TextDialogFragment newInstance(String title, String text, String dismissButton,
			String actionButton, boolean isDismissGray) {
		Bundle args = new Bundle();
		args.putString(TITLE_ARG_KEY, title);
		args.putString(TEXT_ARG_KEY, text);
		args.putString(DISMISS_TEXT_ARG_KEY, dismissButton);
		args.putString(ACTION_TEXT_ARG_KEY, actionButton);
		args.putBoolean(IS_DISMISS_GRAY_ARG_KEY, isDismissGray);

		return newInstance(args);
	}

	public static TextDialogFragment newInstance(String title, String text, String dismissButton,
			String actionButton) {
		return newInstance(title, text, dismissButton, actionButton, false);
	}

	public static TextDialogFragment newInstance(String title, String text, String dismissButton) {
		return newInstance(title, text, dismissButton, null);
	}

	public static TextDialogFragment newInstance(String title, String text) {
		return newInstance(title, text, null, null);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		if (args != null) {
			title = args.getString(TITLE_ARG_KEY, "");
			text = args.getString(TEXT_ARG_KEY, "");
			dismiss = args.getString(DISMISS_TEXT_ARG_KEY, null);
			action = args.getString(ACTION_TEXT_ARG_KEY, null);
			isDismissGray = args.getBoolean(IS_DISMISS_GRAY_ARG_KEY, false);
		} else {
			Log.e(TAG, "Can not create the dialog without any info! Use newInstance method!");
			dismiss();
		}
	}

	@Override
	protected void onCreateContentView(View contentView) {
		super.onCreateContentView(contentView);

		titleView = (TextView) contentView.findViewById(R.id.text_dialog_title);
		View separator = contentView.findViewById(R.id.text_dialog_title_separator);
		descriptionView = (TextView) contentView.findViewById(R.id.text_dialog_desc);
		okButton = (TextView) contentView.findViewById(R.id.text_dialog_ok_button);
		okButton.setOnClickListener(this);
		
		dismissButtonBig = (TextView) contentView.findViewById(R.id.text_dialog_dismiss_button_big);
		dismissButtonBig.setOnClickListener(this);
		
		dismissButton = (TextView) contentView.findViewById(R.id.text_dialog_dismiss_button);
		dismissButton.setOnClickListener(this);
		actionButton = (TextView) contentView.findViewById(R.id.text_dialog_action_button);

		if(title != null && !title.isEmpty()){
			titleView.setText(title);			
		}else{
			titleView.setVisibility(View.GONE);
			separator.setVisibility(View.GONE);
		}
		descriptionView.setText(text);

		if (action == null) {
			TextView button = null;
			if (isDismissGray) {
				okButton.setVisibility(View.GONE);
				dismissButtonBig.setVisibility(View.VISIBLE);
				button = dismissButtonBig;
			} else {
				button = okButton;
			}

			if (dismiss != null) {
				button.setText(dismiss);
			}
		} else {
			okButton.setVisibility(View.GONE);
			actionButton.setVisibility(View.VISIBLE);
			actionButton.setText(action);
			dismissButton.setVisibility(View.VISIBLE);
			if (dismiss != null) {
				dismissButton.setText(dismiss);
			}
		}
	}

	@Override
	protected int getContentLayout() {
		return R.layout.text_dialog;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == okButton.getId() || v.getId() == dismissButton.getId()
				|| v.getId() == dismissButtonBig.getId()) {
			dismiss();
		}
	}

	public void setActionButtonListener(OnClickListener listener) {
		actionButton.setOnClickListener(listener);
	}

	/**
	 * Changes the state of action button.
	 * 
	 * @param enable
	 */
	public void enableActionButton(boolean enable) {
		if (enable) {
			actionButton.setClickable(true);
			actionButton.setBackgroundResource(R.drawable.dialog_action_button_orange);
		} else {
			actionButton.setClickable(false);
			actionButton.setBackgroundResource(R.drawable.dialog_action_button_disabled);
		}
	}

}