package com.copyop.android.app.widget;

import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.copyop.android.app.R;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

public class CopyButton extends COButton {

	public enum CopyButtonState {
		STATE_NORMAL,
		STATE_COPYING,
		STATE_FROZEN
	}
	
	private CopyButtonState state = CopyButtonState.STATE_NORMAL;
	private Drawable drawable;
	private String text;
	private View view;
	
	public CopyButton(Context context) {
		super(context);
		init(context, null, 0);
	}
	
	public CopyButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	
	public CopyButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs, defStyleAttr);
	}
  		
	@Override
	protected void init(Context context, AttributeSet attrs, int defStyleAttr) {
		if(view == null){
			color = ColorUtils.getColor(R.color.copy_watch_button_normal);
			shadowColor = ColorUtils.getColor(R.color.copy_watch_button_normal_shadow);
			shadowSize = getResources().getDimensionPixelSize(R.dimen.button_shadow_width);
			isBorder = false;
			iconBGColor = ColorUtils.getColor(R.color.copy_watch_button_normal_icon_bg);
			iconBGSize = getResources().getDimensionPixelSize(R.dimen.button_copy_watch_icon_bg_width);
			paddingLeft = getResources().getDimensionPixelSize(R.dimen.button_copy_padding_left);
			setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
			setFont(Font.ROBOTO_REGULAR);
			setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.button_copy_watch_text));
			
			setState(CopyButtonState.STATE_NORMAL);
		}
		super.init(context, attrs, defStyleAttr);
	}

	public void setState(CopyButtonState copyButtonState) {
		this.state = copyButtonState;
		if (state == CopyButtonState.STATE_NORMAL) {
			text = getContext().getString(R.string.copyCap);
			drawable = DrawableUtils.getDrawable(R.drawable.copy_button_normal);
			color = R.color.copy_watch_button_normal;
			shadowColor = R.color.copy_watch_button_normal_shadow;
			iconBGColor = ColorUtils.getColor(R.color.copy_watch_button_normal_icon_bg);
		} else if (state == CopyButtonState.STATE_COPYING) {
			text = getContext().getString(R.string.copyingCap);
			drawable = DrawableUtils.getDrawable(R.drawable.copy_button_active);
			color = R.color.copy_watch_button_active;
			shadowColor = R.color.copy_watch_button_active_shadow;
			iconBGColor = ColorUtils.getColor(R.color.copy_watch_button_active_icon_bg);
		} else if (state == CopyButtonState.STATE_FROZEN) {
			text = getContext().getString(R.string.copyCap);
			drawable = DrawableUtils.getDrawable(R.drawable.copy_button_frozen);
			color = R.color.copy_button_frozen;
			shadowColor = R.color.copy_button_frozen_shadow;
			iconBGColor = ColorUtils.getColor(R.color.copy_button_frozen_icon_bg);
		}

		refresh();
	}

	public CopyButtonState getState(){
		return state;
	}

	private void refresh() {
		setText(text);
		setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
		setStyle(color, shadowColor);
		refreshDrawableState();
		invalidate();
	}

}