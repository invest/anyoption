package com.copyop.android.app.popup;

import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.TextView;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COBitmapUtils;
import com.copyop.android.app.util.COConstants;
import com.copyop.json.requests.OriginalInvProfileMethodRequest;
import com.copyop.json.results.OriginalInvProfileMethodResult;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class CopiedOptionDialogFragment extends BaseDialogFragment implements OnClickListener {

	private static final String TAG = CopiedOptionDialogFragment.class.getSimpleName();

	private ImageView avatarImageView;
	private TextView nicknametextView;
	private View loadingView;
	private View okButton;

	private long investmentId;
	private boolean isLoading;

	private long userId;
	private String avatar;
	private String nickname;
	
	/**
	 * Use this method to create new instance of the dialog.
	 */
	public static CopiedOptionDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new CopiedOptionDialogFragment.");
		CopiedOptionDialogFragment fragment = new CopiedOptionDialogFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		if (args != null) {
			investmentId = args.getLong(COConstants.EXTRA_INVESTMENT_ID, 0);
		}

		getProfile();
	}

	@Override
	protected void onCreateContentView(View contentView) {
		avatarImageView = (ImageView) contentView.findViewById(R.id.copied_option_dialog_avatar_image_view);
		nicknametextView = (TextView) contentView.findViewById(R.id.copied_option_dialog_nickname_text_view);
		loadingView = contentView.findViewById(R.id.copied_option_dialog_progress_bar);
		okButton = contentView.findViewById(R.id.copied_option_dialog_ok_button);
		okButton.setOnClickListener(this);

		setupLoading();
		setupProfile();
	}

	private void setupLoading(){
		if(loadingView != null){
			if(isLoading){
				loadingView.setVisibility(View.VISIBLE);
			}else{
				loadingView.setVisibility(View.GONE);
			}
		}
	}

	private void setupProfile() {
		if (avatarImageView != null && nicknametextView != null) {
			COBitmapUtils.loadBitmap(avatar, avatarImageView);
			nicknametextView.setText(nickname);

			avatarImageView.setOnClickListener(this);
			nicknametextView.setOnClickListener(this);
		}
	}

	@Override
	protected int getContentLayout() {
		return R.layout.copied_option_dialog;
	}

	//#####################################################################
	//#########					REQUEST - RESPONSE				###########
	//#####################################################################	
	
	private void getProfile(){
		if(investmentId > 0){
			isLoading = true;
			setupLoading();
			OriginalInvProfileMethodRequest request = new OriginalInvProfileMethodRequest();
			request.setCopiedInvestmentId(investmentId);
			if(application.getCommunicationManager().requestService(this, "gotProfile", COConstants.SERVICE_GET_ORIGINAL_INVEST_PROFILE, request, OriginalInvProfileMethodResult.class, false, false)){
				isLoading = false;
				setupLoading();
			}
		}
	}

	public void gotProfile(Object resultObj) {
		isLoading = false;
		setupLoading();

		OriginalInvProfileMethodResult result = (OriginalInvProfileMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			userId = result.getUserId();
			avatar = result.getAvatar();
			nickname = result.getNickname();
			setupProfile();
		} else {
			if (result != null) {
				Log.e(TAG,
						"gotProfile - Could not get profile! ErrorCode: " + result.getErrorCode());
				if (result.getErrorCode() == COConstants.ERROR_CODE_USER_REMOVED) {
					Log.e(TAG, "This user is removed!");
				}
			} else {
				Log.e(TAG, "gotProfile - Could not get profile!");
			}
		}
	}

	//#####################################################################	

	@Override
	public void onClick(View view) {
		if (view.getId() == avatarImageView.getId() || view.getId() == nicknametextView.getId()) {
			goToProfile(userId);
			dismiss();
		} else if (view.getId() == okButton.getId()) {
			dismiss();
		}
	}

	private void goToProfile(long destUserId) {
		if (destUserId > 0) {
			Bundle bundle = new Bundle();
			bundle.putLong(COConstants.EXTRA_PROFILE_ID, destUserId);
			application.postEvent(new NavigationEvent(COScreen.USER_PROFILE, NavigationType.DEEP, bundle));
		}
	}

}
