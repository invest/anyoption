package com.copyop.android.app.fragment.my_account;

import com.anyoption.android.app.fragment.my_account.SettledOptionsFragment;
import com.anyoption.android.app.util.InvestmentUtil;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.service.results.InvestmentsMethodResult;
import com.copyop.android.app.util.adapter.COTradingOptionsListAdapter;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class COSettledOptionsFragment extends SettledOptionsFragment {

	public static COSettledOptionsFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created fragment COSettledOptionsFragment");
		COSettledOptionsFragment fragment = new COSettledOptionsFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	protected void setUpListAdapter() {
		optionsListAdapter = new COTradingOptionsListAdapter(investments, false);
	}

	@Override
	protected void getInvestmentsFromResult(InvestmentsMethodResult investmentsResult){
		for(Investment investment : investmentsResult.getInvestments()){
			boolean isBubbles = InvestmentUtil.isBubbles(investment);
			boolean isDynamics = InvestmentUtil.isDynamics(investment);
			if(!isBubbles && !isDynamics){
				investments.add(investment);
			}
		}
	}

}
