package com.copyop.android.app.widget;

import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.widget.TextView;
import com.copyop.android.app.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.RelativeLayout;

public class WatchSmallButton extends ClickableLayout {
	
	public enum WatchButtonState{
		STATE_NORMAL,
		STATE_WATCHING;
	}
	
	private WatchButtonState state = WatchButtonState.STATE_NORMAL;
	private TextView textView;
	private Drawable drawable;
	private String text;
	
	public WatchSmallButton(Context context) {
		super(context);
		init(context, null, 0);
	}
	
	public WatchSmallButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	
	public WatchSmallButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs, defStyleAttr);
	}

	protected void init(Context context, AttributeSet attrs, int defStyleAttr) {
		if(textView == null){
			textView = new TextView(context);
			LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.addRule(RelativeLayout.CENTER_IN_PARENT);
			setupAttributes(context, attrs, defStyleAttr);
			textView.setGravity(Gravity.CENTER_HORIZONTAL);
			textView.setCompoundDrawablePadding(getResources().getDimensionPixelSize(R.dimen.button_copy_small_text_drawable_padding));
			
			if(!isInEditMode()){
				setState(WatchButtonState.STATE_NORMAL);
			}else{
				textView.setText(getContext().getString(R.string.buttonWatch));
			}
			
			addView(textView, params);
		}
		super.init(context, attrs, defStyleAttr);
	}
	
	private void setupAttributes(Context context, AttributeSet attrs, int defStyleAttr){
		// Get custom attributes:
		TypedArray attributesTypedArray = context.obtainStyledAttributes(attrs,R.styleable.WatchSmallButton);
		int textColor = attributesTypedArray.getColor(R.styleable.WatchSmallButton_textColor, ColorUtils.getColor(R.color.default_text));
		int textSize = (int)attributesTypedArray.getDimension(R.styleable.WatchSmallButton_textSize, getResources().getDimension(R.dimen.default_text));
		int fontID = attributesTypedArray.getInt(R.styleable.WatchSmallButton_font, Font.ROBOTO_REGULAR.getFontID());
		Font font = FontsUtils.getFontForFontID(fontID);
		attributesTypedArray.recycle();
		
		textView.setTextColor(textColor);
		textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
		textView.setFont(font);
		//////////////////////////	
	}

	public void setState(WatchButtonState watchButtonState){
		this.state = watchButtonState;
		if(state == WatchButtonState.STATE_NORMAL){
			text = getContext().getString(R.string.buttonWatch);
			drawable = DrawableUtils.getDrawable(R.drawable.watch_button_small_normal);
			color = R.color.button_orange_bg;
			shadowColor = R.color.button_orange_shadow;
		}else if(state == WatchButtonState.STATE_WATCHING){
			text = getContext().getString(R.string.buttonWatching);
			drawable = DrawableUtils.getDrawable(R.drawable.watch_button_small_active);
			color = R.color.button_blue_bg;
			shadowColor = R.color.button_blue_shadow;
		}
		
		refresh();
	}
	
	public WatchButtonState getState(){
		return state;
	}
	
	private void refresh(){
		textView.setText(text);
		textView.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
		setStyle(color, shadowColor);
		invalidate();
		refreshDrawableState();
	}
}
