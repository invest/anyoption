package com.anyoption.android.app.fragment.launch;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anyoption.android.app.R;
import com.anyoption.android.app.fragment.BaseFragment;

/**
 * @author Anastas Arnaudov
 */
public class AOWelcome_3_Fragment extends BaseFragment{

    public static final String TAG = AOWelcome_3_Fragment.class.getSimpleName();

    public static AOWelcome_3_Fragment newInstance(){
        AOWelcome_3_Fragment fragment = new AOWelcome_3_Fragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.welcome_3_fragment, container, false);
        return rootView;
    }

}
