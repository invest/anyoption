package com.anyoption.android.app.popup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.anyoption.android.app.R;
import com.anyoption.android.app.widget.TextView;

/**
 * @author Anastas Arnaudov
 */
public class AOExistingEmailDialogfragment extends ExistingEmailDialogfragment{

    public static final String TAG = AOExistingEmailDialogfragment.class.getSimpleName();

    public static AOExistingEmailDialogfragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new AOExistingEmailDialogfragment.");
        AOExistingEmailDialogfragment fragment = new AOExistingEmailDialogfragment();
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getContentLayout() {
        return R.layout.popup_existing_email;
    }

    @Override
    protected void onCreateContentView(View contentView) {
        mainLayout         =            contentView.findViewById(R.id.existingEmailPopupLayout);
        messageTextView    = (TextView) contentView.findViewById(R.id.existingEmailPopupMessageTextView);
        closeButton        =            contentView.findViewById(R.id.existingEmailPopupCloseButton);
        changeEmailButton  =            contentView.findViewById(R.id.existingEmailPopupChangeEmailButton);
        recoverPassButton  =            contentView.findViewById(R.id.existingEmailPopupRecoverPassButton);

        super.onCreateContentView(contentView);
    }
}
