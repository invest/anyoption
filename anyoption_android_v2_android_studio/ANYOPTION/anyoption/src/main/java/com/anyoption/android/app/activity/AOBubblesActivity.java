package com.anyoption.android.app.activity;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.InputEvent;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.ClientCertRequest;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.GeolocationPermissions;
import android.webkit.HttpAuthHandler;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.PermissionRequest;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.anyoption.android.app.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.popup.LogoutInvestDialogFragment;
import com.anyoption.android.app.util.AOConstants;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.json.requests.CheckUpdateMethodRequest;
import com.anyoption.json.results.UpdateMethodResult;

/**
 * @author Anastas Arnaudov
 */
public class AOBubblesActivity extends BubblesActivity {

    public static final String TAG = AOBubblesActivity.class.getSimpleName();

    public static final String PARAM_IS_ACTIVE          = "isActive";
    public static final String PARAM_IS_BACK            = "back";
    public static final String PARAM_ERROR_CODE         = "errorCode";
    public static final String PARAM_ERROR_MESSAGE      = "errorMessages";
    public static final String PARAM_TRUE               = "true";
    public static final String PARAM_FALSE              = "false";

    private WebView webView;
    private String url;
    private View loadingView;
    private View comingSoonView;

    private boolean isFromLogin = false;
    private boolean isShouldReload = true;


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        setContentView(R.layout.activity_bubbles);

        application.registerForEvents(this, NavigationEvent.class);

        if(getIntent().getExtras() != null) {
            isFromLogin = getIntent().getExtras().getBoolean(Constants.EXTRA_GO_TO_MAIN, false);
        } else {
            isFromLogin = false;
        }

        url = AOConstants.BUBBLES_LINK;

        comingSoonView = findViewById(R.id.bubblesComingSoonView);
        showComingSoonView(false);

        View backButton = findViewById(R.id.bubblesBackButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        webView = (WebView) findViewById(R.id.bubblesWebView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebChromeClient(new MyWebChromeClient());
        webView.setWebViewClient(new MyWebViewClient());

        loadingView = findViewById(R.id.loadingView);
        setupLoading(true);

        if (!application.isLoggedIn()) {
            checkForUpdate();
        }
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        isShouldReload = true;
        super.onDestroy();
    }

    private void setupLoading(boolean isLoading) {
        if (loadingView != null) {
            if (isLoading) {
                Log.d(TAG, "Showing the loading.");
                loadingView.setVisibility(View.VISIBLE);
            } else {
                Log.d(TAG, "Hiding the loading.");
                loadingView.setVisibility(View.GONE);
            }
        }
    }

    private void showComingSoonView(boolean isShow) {
        if (isShow) {
            Log.d(TAG, "Showing the Coming Soon View.");
            comingSoonView.setVisibility(View.VISIBLE);
        } else {
            Log.d(TAG, "Hiding the Coming Soon View.");
            comingSoonView.setVisibility(View.GONE);
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //                                   Request and Responses                              //
    //////////////////////////////////////////////////////////////////////////////////////////

    private void checkForUpdate() {
        CheckUpdateMethodRequest request = new CheckUpdateMethodRequest();
        request.setApkVersion(application.getApplicationVersionCode());
        request.setApkName(getString(com.anyoption.android.R.string.apkName));
        request.setCombinationId(Utils.getCombinationId());
        request.setDynamicParam(Utils.getDynamicParam());
        request.setAffSub1(Utils.getAffSub1(this));
        request.setAffSub2(Utils.getAffSub2(this));
        request.setAffSub3(Utils.getGclid(this));
        request.setmId(application.getSharedPreferencesManager().getString(Constants.PREFERENCES_MID, null));
        request.setEtsMId(application.getSharedPreferencesManager().getString(Constants.PREFERENCES_ETS_MID, null));
        request.setFirstOpening(Utils.checkFirstOpening());
        request.setOsTypeId(1);
        request.setAppsflyerId(application.getAppsFlyerManager().getAppsFlyerUID());
        request.setDeviceFamily(ScreenUtils.isTablet() ? "1" : "0");

        application.getCommunicationManager().requestService(this, "checkForUpdateCallBack", Constants.SERVICE_CHECK_FOR_UPDATE, request, UpdateMethodResult.class, false, false);
    }

    public void checkForUpdateCallBack(Object resultObj) {
        CookieManager.getInstance().setCookie(url, Constants.COOKIE_SESSION_ID + "=" + application.getSessionId());
        webView.loadUrl(url);
    }

    public void getUserCallback(Object resultObj) {
        super.getUserCallback(resultObj);
        if(isShouldReload){
            //Set the cookie with the latest session id from the server:
            isShouldReload = false;
            CookieManager.getInstance().setCookie(url, Constants.COOKIE_SESSION_ID + "=" + application.getSessionId());
            webView.loadUrl(url);
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////
    ///////                     EVENTs                              //
    //////////////////////////////////////////////////////////////////

    public void onEventMainThread(NavigationEvent event) {
        Log.d(TAG, "NavigationEvent : " + event.getToScreen());
        finish();
    }

    //////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////
    ///                     CUSTOM CHROME CLIENT                   ///
    ///////////////////////////////////////////////////////////////////

    private class MyWebChromeClient extends WebChromeClient{

        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
            Log.d(TAG, "MyWebChromeClient -> onReceivedTitle : title=" + title);
            handleParams(title);
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            Log.d(TAG, "onProgressChanged");
        }

        @Override
        public void onReceivedIcon(WebView view, Bitmap icon) {
            super.onReceivedIcon(view, icon);
            Log.d(TAG, "onReceivedIcon");
        }

        @Override
        public void onReceivedTouchIconUrl(WebView view, String url, boolean precomposed) {
            super.onReceivedTouchIconUrl(view, url, precomposed);
            Log.d(TAG, "onReceivedTouchIconUrl -> " + url);
        }

        @Override
        public void onReachedMaxAppCacheSize(long requiredStorage, long quota, WebStorage.QuotaUpdater quotaUpdater) {
            super.onReachedMaxAppCacheSize(requiredStorage, quota, quotaUpdater);
            Log.d(TAG, "onReachedMaxAppCacheSize");
        }

        @Override
        public void onExceededDatabaseQuota(String url, String databaseIdentifier, long quota, long estimatedDatabaseSize, long totalQuota, WebStorage.QuotaUpdater quotaUpdater) {
            super.onExceededDatabaseQuota(url, databaseIdentifier, quota, estimatedDatabaseSize, totalQuota, quotaUpdater);
            Log.d(TAG, "onExceededDatabaseQuota -> " + url);
        }

        @Override
        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
//            Log.d(TAG, "onConsoleMessage -> " + consoleMessage);//TODO too much log
            return super.onConsoleMessage(consoleMessage);
        }

        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
            Log.d(TAG, "onCreateWindow -> " + resultMsg);
            return super.onCreateWindow(view, isDialog, isUserGesture, resultMsg);
        }

        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            Log.d(TAG, "onJsAlert -> " + url + " , " + message);
            return super.onJsAlert(view, url, message, result);
        }

        @Override
        public boolean onJsBeforeUnload(WebView view, String url, String message, JsResult result) {
            Log.d(TAG, "onJsBeforeUnload -> " + url + " , " + message);
            return super.onJsBeforeUnload(view, url, message, result);
        }

        @Override
        public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
            Log.d(TAG, "onJsConfirm -> " + url + " , " + message);
            return super.onJsConfirm(view, url, message, result);
        }

        @Override
        public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {
            Log.d(TAG, "onJsPrompt -> " + url + " , " + message);
            return super.onJsPrompt(view, url, message, defaultValue, result);
        }

        @Override
        public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
            Log.d(TAG, "onShowFileChooser");
            return super.onShowFileChooser(webView, filePathCallback, fileChooserParams);
        }

        @Override
        public void onCloseWindow(WebView window) {
            super.onCloseWindow(window);
            Log.d(TAG, "onCloseWindow");
        }

        @Override
        public void onGeolocationPermissionsHidePrompt() {
            super.onGeolocationPermissionsHidePrompt();
            Log.d(TAG, "onGeolocationPermissionsHidePrompt");
        }

        @Override
        public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
            super.onGeolocationPermissionsShowPrompt(origin, callback);
            Log.d(TAG, "onGeolocationPermissionsShowPrompt");
        }

        @Override
        public void onHideCustomView() {
            super.onHideCustomView();
            Log.d(TAG, "onHideCustomView");
        }

        @Override
        public void onPermissionRequest(PermissionRequest request) {
            super.onPermissionRequest(request);
            Log.d(TAG, "onPermissionRequest");
        }

        @Override
        public void onPermissionRequestCanceled(PermissionRequest request) {
            super.onPermissionRequestCanceled(request);
            Log.d(TAG, "onPermissionRequestCanceled");
        }

        @Override
        public void onRequestFocus(WebView view) {
            super.onRequestFocus(view);
            Log.d(TAG, "onRequestFocus");
        }

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {
            super.onShowCustomView(view, callback);
            Log.d(TAG, "onShowCustomView");
        }

        @Override
        public boolean onJsTimeout() {
            Log.d(TAG, "onJsTimeout");
            return super.onJsTimeout();
        }

        @Override
        public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
            super.onShowCustomView(view, requestedOrientation, callback);
            Log.d(TAG, "onShowCustomView");
        }

        @Override
        public void onConsoleMessage(String message, int lineNumber, String sourceID) {
            super.onConsoleMessage(message, lineNumber, sourceID);
//            Log.d(TAG, "onConsoleMessage");
        }

    }

    ///////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////////////////
    ///                     JAVASCRIPT CLIENT                   ///
    ///////////////////////////////////////////////////////////////////

    /* An instance of this class will be registered as a JavaScript interface */
    class MyJavaScriptInterface
    {
        @SuppressWarnings("unused")
        public void processHTML(String html)
        {
            // process the html as needed by the app
        }
    }


    ///////////////////////////////////////////////////////////////////
    ///                     CUSTOM WEBVIEW CLIENT                   ///
    ///////////////////////////////////////////////////////////////////

    private class MyWebViewClient extends WebViewClient {

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.d(TAG, "MyWebViewClient -> onPageFinished : url=" + url);
            setupLoading(false);
            handleParams(url);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.d(TAG, "shouldOverrideUrlLoading -> " + url);
            return true;
        }

        @Override
        public void onFormResubmission(WebView view, Message dontResend, Message resend) {
            super.onFormResubmission(view, dontResend, resend);
            Log.d(TAG, "onFormResubmission");
        }

        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
//            Log.d(TAG, "onLoadResource -> " + url); //TODO too much log
        }

        @Override
        public void onReceivedClientCertRequest(WebView view, ClientCertRequest request) {
            super.onReceivedClientCertRequest(view, request);
            Log.d(TAG, "onReceivedClientCertRequest");
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            Log.d(TAG, "onReceivedError");
        }

        @Override
        public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
            super.onReceivedHttpAuthRequest(view, handler, host, realm);
            Log.d(TAG, "onReceivedHttpAuthRequest");
        }

        @Override
        public void onReceivedLoginRequest(WebView view, String realm, String account, String args) {
            super.onReceivedLoginRequest(view, realm, account, args);
            Log.d(TAG, "onReceivedLoginRequest");
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);
            Log.d(TAG, "onReceivedSslError");
        }

        @Override
        public void onScaleChanged(WebView view, float oldScale, float newScale) {
            super.onScaleChanged(view, oldScale, newScale);
            Log.d(TAG, "onScaleChanged");
        }

        @Override
        public void onUnhandledInputEvent(WebView view, InputEvent event) {
            super.onUnhandledInputEvent(view, event);
            Log.d(TAG, "onUnhandledInputEvent");
        }

        @Override
        public void onTooManyRedirects(WebView view, Message cancelMsg, Message continueMsg) {
            super.onTooManyRedirects(view, cancelMsg, continueMsg);
            Log.d(TAG, "onTooManyRedirects");
        }

        @Override
        public void onUnhandledKeyEvent(WebView view, KeyEvent event) {
            super.onUnhandledKeyEvent(view, event);
            Log.d(TAG, "onUnhandledKeyEvent");
        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            super.onPageCommitVisible(view, url);
            Log.d(TAG, "onPageCommitVisible -> " + url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.d(TAG, "onPageStarted -> " + url);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            Log.d(TAG, "onPageStarted -> " + error);
        }

        @Override
        public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
            super.onReceivedHttpError(view, request, errorResponse);
            Log.d(TAG, "onPageStarted -> " + errorResponse);
        }

    }

    ///////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////


    private void handleParams(String url) {
        try {
            Log.d(TAG, "Parsing url params...");

            String isActive         = Utils.getParameterFromUrl(url, PARAM_IS_ACTIVE);
            String back             = Utils.getParameterFromUrl(url, PARAM_IS_BACK);
            String errorCodeString  = Utils.getParameterFromUrl(url, PARAM_ERROR_CODE);

            Log.d(TAG, "Params : isActive=" + isActive + " , back=" + back + " , errorCodeString=" + errorCodeString);

            if (isActive != null && isActive.equalsIgnoreCase(PARAM_FALSE)) {
                showComingSoonView(true);
            } else {
                if (errorCodeString != null) {
                    int errorCode = Integer.valueOf(errorCodeString);
                    String errorMessage = Utils.getParameterFromUrl(url, PARAM_ERROR_MESSAGE);
                    Log.d(TAG, "errorMessage = " + errorMessage);
                    if (errorCode == Constants.ERROR_CODE_SUCCESS) {
                        //Refresh the User:
                        refreshUser();
                    }else if(errorCode == Constants.ERROR_CODE_SESSION_EXPIRED){
                        application.getFragmentManager().showDialogFragment(LogoutInvestDialogFragment.class, null);
                    }else if(errorCode == Constants.ERROR_CODE_REGULATION_SUSPENDED){
                        //Do nothing
                    }else if(errorCode == Constants.ERROR_CODE_REGULATION_USER_PEP_BLOCKED){
                        RegulationUtils.showPEPPopup();
                    }else {
                        //Handle the Error:
                        if (errorMessage != null && !errorMessage.equalsIgnoreCase("null")) {
                            ErrorUtils.handleError(errorCode, errorMessage);
                        } else {
                            ErrorUtils.handleError(errorCode);
                        }
                    }
                }
                if (back != null && back.equalsIgnoreCase(PARAM_TRUE)) {
                    onBackPressed();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Could NOT parse Url params", e);
        }
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed");
        if (isFromLogin) {
            application.postEvent(new NavigationEvent(application.getHomeScreen(), NavigationEvent.NavigationType.INIT));
        } else {
            finish();
//            super.onBackPressed();
        }
    }
}
