package com.anyoption.android.app.popup;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.anyoption.android.app.R;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.TextUtils;

/**
 * Created by veselinh on 22.1.2016 г..
 */
public class AOWithdrawBonusResetDialogFragment extends WithdrawBonusResetDialogFragment{

    public static AOWithdrawBonusResetDialogFragment newInstance(Bundle bundle) {
        AOWithdrawBonusResetDialogFragment aoWithdrawBonusResetDialogFragment = new AOWithdrawBonusResetDialogFragment();
        aoWithdrawBonusResetDialogFragment.setArguments(bundle);
        return aoWithdrawBonusResetDialogFragment;
    }

    @Override
    protected int getContentLayout() {
        return R.layout.withdraw_bonus_reset_popup;

    }

    @Override
    protected void onCreateContentView(View contentView) {
        TextView messageTextView = (TextView) contentView.findViewById(R.id.withdraw_bonus_reset_popup_message_text_view);

        TextUtils.decorateInnerText(messageTextView, null, messageTextView.getText().toString(), getString(R.string.withdrawBonusResetPopupBold), FontsUtils.Font.ROBOTO_MEDIUM, 0, 0);

        yesButton = contentView.findViewById(R.id.yes_button);
        noButton = contentView.findViewById(R.id.no_button);

        yesButton.setOnClickListener(this);
        noButton.setOnClickListener(this);
    }
}
