package com.anyoption.android.app.fragment.trade;

import android.os.Bundle;
import android.util.Log;

import com.anyoption.android.app.popup.LoginRegisterDialogfragment;
import com.anyoption.android.app.popup.LogoutInvestDialogFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Market;

/**
 * @author Anastas Arnaudov
 */
public class AOInvestFragment extends InvestFragment {

    public static final String TAG = AOInvestFragment.class.getSimpleName();

    public static InvestFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new InvestFragment.");
        AOInvestFragment fragment = new AOInvestFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public static InvestFragment newInstance(Market market) {
        Bundle args = new Bundle();
        args.putSerializable(Constants.EXTRA_MARKET, market);
        return newInstance(args);
    }

    protected void investLogout(){
        application.getFragmentManager().showDialogFragment(LogoutInvestDialogFragment.class, null);
    }

}
