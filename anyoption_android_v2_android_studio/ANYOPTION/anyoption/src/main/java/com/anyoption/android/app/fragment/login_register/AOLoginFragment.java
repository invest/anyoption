package com.anyoption.android.app.fragment.login_register;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.popup.SplitBalanceDialogFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class AOLoginFragment extends LoginFragment{

	public static AOLoginFragment newInstance(Bundle bundle){
		AOLoginFragment loginFragment = new AOLoginFragment();
		loginFragment.setArguments(bundle);
		return loginFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = super.onCreateView(inflater, container, savedInstanceState);
		TextView registerLinkTextView = (TextView) rootView.findViewById(R.id.login_register_link_textview);
		registerLinkTextView.setText(getString(R.string.register) + " »");
		registerLinkTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				application.postEvent(new NavigationEvent(Screen.REGISTER, NavigationType.INIT));
			}
		});
		
		return rootView;
	}
	
	@Override
	protected void underlineForgotPass(TextView textView){
		//Ignore
	}
	
}
