package com.anyoption.android.app.manager;

import com.anyoption.android.app.Application;

public class AOGoogleAdWordsManager extends GoogleAdWordsManager {

	private static final String CONVERSION_ID = "985410182";
	private static final String REGISTRATION_EVENT_LABEL = "w1Y-CJq0oFYQhtXw1QM";
	private static final String DEPOSIT_EVENT_LABEL = "Ak8bCIi_zlYQhtXw1QM";

	public AOGoogleAdWordsManager(Application application) {
		super(application);
	}

	@Override
	protected String getConversionId() {
		return CONVERSION_ID;
	}

	@Override
	protected String getRegistrationEventLabel() {
		return REGISTRATION_EVENT_LABEL;
	}

	@Override
	protected String getDepositEventLabel() {
		return DEPOSIT_EVENT_LABEL;
	}

}
