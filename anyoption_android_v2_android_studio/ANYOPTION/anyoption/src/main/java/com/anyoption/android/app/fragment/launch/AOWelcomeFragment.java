package com.anyoption.android.app.fragment.launch;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Market;

import java.util.ArrayList;
import java.util.List;

public class AOWelcomeFragment extends WelcomeFragment {

	public static AOWelcomeFragment newInstance(Bundle bundle){
		Log.d(TAG, "Created new AOWelcomeFragment.");
		AOWelcomeFragment fragment = new AOWelcomeFragment();
		fragment.setArguments(bundle);	
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		NUMBER_OF_PAGES = 4;
	}

	@Override
	protected List<Fragment> getFragments() {
		List<Fragment> fragmentList = new ArrayList<Fragment>();

		fragmentList.add(AOWelcome_1_Fragment.newInstance());
		fragmentList.add(AOWelcome_2_Fragment.newInstance());
		fragmentList.add(AOWelcome_3_Fragment.newInstance());
		fragmentList.add(AOWelcome_4_Fragment.newInstance());

		return fragmentList;
	}

	protected void setupTryWithoutAccount() {
		tryWithoutAccountTextView = (TextView) rootView.findViewById(com.anyoption.android.R.id.welcome_screen_try_without_account_text);
		TextUtils.underlineText(tryWithoutAccountTextView);
		tryWithoutAccountTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tryApp();
			}
		});
	}

	@Override
	protected void tryApp() {
		Bundle bundle = new Bundle();
		Market market = application.getMarketForID(20L);
		if (isTablet) {
			bundle.putSerializable(Constants.EXTRA_MARKET, market);
			bundle.putString(Constants.EXTRA_ACTION_BAR_TITLE, market.getDisplayName());
			bundle.putLong(Constants.EXTRA_MARKET_PRODUCT_TYPE, market.getProductTypeId());
			application.postEvent(new NavigationEvent(Application.Screen.ASSET_PAGE, NavigationEvent.NavigationType.HORIZONTAL, bundle));
		} else {
			bundle.putSerializable(Constants.EXTRA_MARKET, market);
			application.postEvent(new NavigationEvent(Application.Screen.ASSET_VIEW_PAGER, NavigationEvent.NavigationType.INIT, bundle));
		}
	}

	@Override
	protected void setupLoginRegisterButtons() {
		super.setupLoginRegisterButtons();
		((TextView) loginButton).setText(getString(R.string.welcome_screen_login_link_1_text) + " " + getString(R.string.welcome_screen_login_link_2_text));
		TextUtils.underlineText((TextView) loginButton);
	}

	@Override
	protected void setupDots(){
		//Create dot for each page:
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen.welcome_screen_dot_size), getResources().getDimensionPixelSize(R.dimen.welcome_screen_dot_size));
		params.setMargins(getResources().getDimensionPixelSize(R.dimen.welcome_screen_dots_margin), 0, getResources().getDimensionPixelSize(R.dimen.welcome_screen_dots_margin), 0);
		for(int i = 0; i < pagerAdapter.getCount(); i++){
			ImageView dotImage = new ImageView(application.getCurrentActivity());
			dotImage.setImageDrawable(getCurrentDot());
			dotImage.setLayoutParams(params);
			dotsLayout.addView(dotImage);
		}
		dotsLayout.invalidate();
	}

	@Override
	protected void changeDots(int page){
		for(int i = 0; i < pagerAdapter.getCount(); i++){
			ImageView dotImage = (ImageView) dotsLayout.getChildAt(i);
			if(i <= page){
				dotImage.setImageDrawable(getCurrentDot());
			}else{
				dotImage.setImageDrawable(getOtherDot());
			}
		}
		dotsLayout.invalidate();
	}

	protected Drawable getCurrentDot() {
		return DrawableUtils.getDrawable(R.drawable.welcome_screen_dot_filled);
	}

	protected Drawable getOtherDot() {
		return DrawableUtils.getDrawable(R.drawable.welcome_screen_dot_emty);
	}
}