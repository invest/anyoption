package com.anyoption.android.app.fragment.trade;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;

import com.anyoption.android.app.model.OptionDynamicsGroup;
import com.anyoption.android.app.model.OptionDynamicsItem;
import com.anyoption.android.app.popup.MyOptionsReceiptDialogFragment;
import com.anyoption.android.app.util.adapter.OptionsDynamicsAdapter;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Investment;

/**
 * @author Anastas Arnaudov
 */
public class AOAssetOptionsDynamicsFragment extends AssetOptionsDynamicsFragment {

    public static final String TAG = AOAssetOptionsDynamicsFragment.class.getSimpleName();

    public static AOAssetOptionsDynamicsFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new AOAssetOptionsDynamicsFragment.");
        AOAssetOptionsDynamicsFragment fragment = new AOAssetOptionsDynamicsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void setupListChildClickListenber(){
        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                OptionDynamicsGroup group = adapter.getGroup(groupPosition);
                if(group.getTpe() == OptionDynamicsGroup.Type.SETTLED){
                    Investment investment = settledOptionsList.get(childPosition);
                    Bundle args = new Bundle();
                    args.putSerializable(Constants.EXTRA_INVESTMENT, investment);
                    args.putBoolean(Constants.EXTRA_IS_INVESTMENT_OPEN, false);
                    args.putBoolean(Constants.EXTRA_IS_FROM_TRADE_SCREEN, true);
                    application.getFragmentManager().showDialogFragment(MyOptionsReceiptDialogFragment.class, args);
                }
                return true;
            }
        });
    }

}
