package com.anyoption.android.app.util;

import com.anyoption.android.app.util.constants.Constants;

public class AOConstants extends Constants {

	//################################################################################################
	//////////////////			LOCAL ENVIRONMENT (TONI)		//////////////////////////////////////
	//################################################################################################

//	public static final String SERVICE_URL      = "http://www.antonba.bg.anyoption.com/jsonService/AnyoptionService/";
// 	public static final String LS_URL       	= "http://ls.bgtestenv.anyoption.com:80";
//	public static final String MERCHANT_ID      = "000203AYP";
//	public static final String ONE_CLICK_URL    = "http://test.envoytransfers.com";
//  public static final String WEB_SITE_LINK    = "http://www.bgtestenv.anyoption.com/mobile/";
//	public static final String DEEP_LINK        = "http://cdn.anyoption.com/deeplinks/marketing.shtml";
//  public static final String BUBBLES_LINK 	= "http://www.bgtestenv.anyoption.com/jsonService/jsp/bubbles.jsp";

	//################################################################################################

	//################################################################################################
	//////////////////			LOCAL ENVIRONMENT (JIMI)		//////////////////////////////////////
	//################################################################################################

//	public static final String SERVICE_URL      = "http://www.dzhamaldv.bg.anyoption.com/jsonService/AnyoptionService/";
//	public static final String LS_URL           = "http://ls.bgtestenv.anyoption.com:80";
//	public static final String MERCHANT_ID      = "000266AYP";
//	public static final String ONE_CLICK_URL    = "https://www.envoytransfers.com";
//  public static final String WEB_SITE_LINK    = "http://www.bgtestenv.anyoption.com/mobile/";
//	public static final String DEEP_LINK        = "http://cdn.anyoption.com/deeplinks/marketing.shtml";
//	public static final String BUBBLES_LINK 	= "http://www.bgtestenv.anyoption.com/jsonService/jsp/bubbles.jsp";

	//################################################################################################

	//################################################################################################
	//////////////////			LOCAL ENVIRONMENT (KIRO)		//////////////////////////////////////
	//################################################################################################

//	public static final String SERVICE_URL      = "http://192.168.100.64/jsonService/AnyoptionService/";
//	public static final String LS_URL           = "http://ls.bgtestenv.anyoption.com:80";
//	public static final String MERCHANT_ID      = "000203AYP";
//	public static final String ONE_CLICK_URL    = "https://test.envoytransfers.com";
//  public static final String WEB_SITE_LINK    = "http://www.bgtestenv.anyoption.com/mobile/";
//	public static final String DEEP_LINK        = "http://cdn.anyoption.com/deeplinks/marketing.shtml";
//  public static final String BUBBLES_LINK 	= "http://www.bgtestenv.anyoption.com/jsonService/jsp/bubbles.jsp";

	//################################################################################################

	//################################################################################################
	//////////////////			LOCAL ENVIRONMENT (PESHO)		//////////////////////////////////////
	//################################################################################################

//  public static final String SERVICE_URL      = "http://192.168.100.183/jsonService/AnyoptionService/";
//	public static final String LS_URL           = "http://ls.bgtestenv.anyoption.com:80";
//	public static final String MERCHANT_ID      = "000266AYP";
//	public static final String ONE_CLICK_URL    = "https://www.envoytransfers.com";
//  public static final String WEB_SITE_LINK    = "http://www.bgtestenv.anyoption.com/mobile/";
//	public static final String DEEP_LINK        = "http://cdn.anyoption.com/deeplinks/marketing.shtml";
//  public static final String BUBBLES_LINK     = "http://www.bgtestenv.anyoption.com/jsonService/jsp/bubbles.jsp";

	//################################################################################################

	//################################################################################################
	//////////////////			LOCAL ENVIRONMENT (PAVKATA)		//////////////////////////////////////
	//################################################################################################

//	public static final String SERVICE_URL      = "http://192.168.100.52/jsonService/AnyoptionService/";
//	public static final String LS_URL           = "http://ls.bgtestenv.anyoption.com:80";
//	public static final String MERCHANT_ID      = "000266AYP";
//	public static final String ONE_CLICK_URL    = "https://www.envoytransfers.com";
//  public static final String WEB_SITE_LINK    = "http://www.bgtestenv.anyoption.com/mobile/";
//	public static final String DEEP_LINK        = "http://cdn.anyoption.com/deeplinks/marketing.shtml";
//	public static final String BUBBLES_LINK 	= "http://www.bgtestenv.anyoption.com/jsonService/jsp/bubbles.jsp";

	//################################################################################################

	//################################################################################################
	//////////////////			LOCAL ENVIRONMENT (PAVLIN)		//////////////////////////////////////
	//################################################################################################

//	public static final String SERVICE_URL      = "http://192.168.100.58/jsonService/AnyoptionService/";
//	public static final String LS_URL           = "http://ls.bgtestenv.anyoption.com:80";
//	public static final String MERCHANT_ID      = "000266AYP";
//	public static final String ONE_CLICK_URL    = "https://www.envoytransfers.com";
//  public static final String WEB_SITE_LINK    = "http://www.bgtestenv.anyoption.com/mobile/";
//	public static final String DEEP_LINK        = "http://cdn.anyoption.com/deeplinks/marketing.shtml";
//	public static final String BUBBLES_LINK 	= "http://www.bgtestenv.anyoption.com/jsonService/jsp/bubbles.jsp";

	//################################################################################################

    //################################################################################################
    //////////////////			LOCAL ENVIRONMENT (VANKATA)		//////////////////////////////////////
    //################################################################################################

//	public static final String SERVICE_URL      = "http://192.168.100.181/jsonService/AnyoptionService/";
//	public static final String LS_URL           = "http://ls.bgtestenv.anyoption.com:80";
//	public static final String MERCHANT_ID      = "000266AYP";
//	public static final String ONE_CLICK_URL    = "https://www.envoytransfers.com";
//    public static final String WEB_SITE_LINK    = "http://www.bgtestenv.anyoption.com/mobile/";
//	public static final String DEEP_LINK        = "http://cdn.anyoption.com/deeplinks/marketing.shtml";
//	public static final String BUBBLES_LINK 	= "http://www.bgtestenv.anyoption.com/jsonService/jsp/bubbles.jsp";

    //################################################################################################

    //################################################################################################
    //////////////////			LOCAL ENVIRONMENT (KRISI)		//////////////////////////////////////
    //################################################################################################

//	public static final String SERVICE_URL      = "http://192.168.100.180/jsonService/AnyoptionService/";
//	public static final String LS_URL           = "http://ls.bgtestenv.anyoption.com:80";
//	public static final String MERCHANT_ID      = "000266AYP";
//	public static final String ONE_CLICK_URL    = "https://www.envoytransfers.com";
//  public static final String WEB_SITE_LINK    = "http://www.bgtestenv.anyoption.com/mobile/";
//	public static final String DEEP_LINK        = "http://cdn.anyoption.com/deeplinks/marketing.shtml";
//	public static final String BUBBLES_LINK 	= "http://www.bgtestenv.anyoption.com/jsonService/jsp/bubbles.jsp";

    //################################################################################################

    //################################################################################################
    //////////////////			LOCAL ENVIRONMENT (RADI)		//////////////////////////////////////
    //################################################################################################

//	public static final String SERVICE_URL      = "http://192.168.100.147/jsonService/AnyoptionService/";
//	public static final String LS_URL           = "http://ls.bgtestenv.anyoption.com:80";
//	public static final String MERCHANT_ID      = "000266AYP";
//	public static final String ONE_CLICK_URL    = "https://www.envoytransfers.com";
//    public static final String WEB_SITE_LINK    = "http://www.bgtestenv.anyoption.com/mobile/";
//	public static final String DEEP_LINK        = "http://cdn.anyoption.com/deeplinks/marketing.shtml";
//	public static final String BUBBLES_LINK 	= "http://www.bgtestenv.anyoption.com/jsonService/jsp/bubbles.jsp";

    //################################################################################################

	//################################################################################################
	////////////////////////		LOCAL ENVIRONMENT (YARIV - BUBBLES)		//////////////////////////
	//################################################################################################

//    public static final String SERVICE_URL      = "https://out.bgtestenv.anyoption.com/jsonService/AnyoptionService/";
//    public static final String LS_URL           = "http://ls.bgtestenv.anyoption.com:80";
//    public static final String MERCHANT_ID      = "000203AYP";
//    public static final String ONE_CLICK_URL    = "https://test.envoytransfers.com";
//    public static final String WEB_SITE_LINK    = "http://www.bgtestenv.anyoption.com/mobile/";
//    public static final String DEEP_LINK        = "http://cdn.bgtestenv.anyoption.com/deeplinks/marketing.shtml";
//    public static final String BUBBLES_LINK     = "http://192.168.1.14:9000";
//    public static final String BUBBLES_LINK     = "http://m.visualopt.com";
//    public static final String BUBBLES_LINK     = "http://out.bgtestenv.anyoption.com/jsonService/jsp/bubbles.jsp";

	//################################################################################################

	//################################################################################################
	////////////////////////		BG TEST ENVIRONMENT			//////////////////////////////////////
	//################################################################################################

    public static final String SERVICE_URL      = "https://www.bgtestenv.anyoption.com/jsonService/AnyoptionService/";
    public static final String LS_URL           = "http://ls.bgtestenv.anyoption.com:80";
    public static final String MERCHANT_ID      = "000203AYP";
    public static final String ONE_CLICK_URL    = "https://test.envoytransfers.com";
    public static final String WEB_SITE_LINK    = "http://www.bgtestenv.anyoption.com/mobile/";
    public static final String DEEP_LINK        = "http://cdn.bgtestenv.anyoption.com/deeplinks/marketing.shtml";
    public static final String BUBBLES_LINK     = "http://www.bgtestenv.anyoption.com/jsonService/jsp/bubbles.jsp";

	//################################################################################################

	//################################################################################################
	//////////////////         LIVE ENVIRONMENT		/////////////////////////////////////////////////
	//################################################################################################

//	public static final String SERVICE_URL 		= "https://json.anyoptionservice.com/jsonService/AnyoptionService/";
//	public static final String LS_URL 			= "http://ls.anyoptionservice.com:80";//TODO 443 for https
//	public static final String MERCHANT_ID 		= "000266AYP";
//	public static final String ONE_CLICK_URL 	= "https://www.envoytransfers.com";
//	public static final String WEB_SITE_LINK 	= "http://m.anyoption.com/mobile/";
//	public static final String DEEP_LINK 		= "http://cdn.anyoption.com/deeplinks/marketing.shtml";
//	public static final String BUBBLES_LINK 	= "http://www.anyoption.com/jsonService/jsp/bubbles.jsp";
////	public static final String BUBBLES_LINK 	= "http://json.anyoptionservice.com/jsonService/jsp/bubbles.jsp";

	//################################################################################################


}
