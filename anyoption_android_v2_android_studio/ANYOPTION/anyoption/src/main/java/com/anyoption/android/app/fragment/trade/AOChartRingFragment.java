package com.anyoption.android.app.fragment.trade;

import android.os.Bundle;
import android.util.Log;

import com.anyoption.android.app.popup.LogoutInvestDialogFragment;

/**
 * @author Anastas Arnaudov
 */
public class AOChartRingFragment extends ChartRingFragment {

    public static final String TAG = AOChartRingFragment.class.getSimpleName();

    public static AOChartRingFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new AOChartRingFragment.");
        AOChartRingFragment fragment = new AOChartRingFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    protected void investLogout(){
        application.getFragmentManager().showDialogFragment(LogoutInvestDialogFragment.class, null);
    }

}
