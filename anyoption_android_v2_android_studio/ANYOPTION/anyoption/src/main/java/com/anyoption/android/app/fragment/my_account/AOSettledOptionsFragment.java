package com.anyoption.android.app.fragment.my_account;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.anyoption.android.app.popup.MyOptionsReceiptDialogFragment;
import com.anyoption.android.app.util.adapter.AOTradingOptionsListAdapter;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Investment;

public class AOSettledOptionsFragment extends SettledOptionsFragment{

	public static AOSettledOptionsFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created fragment AOSettledOptionsFragment");
		AOSettledOptionsFragment fragment = new AOSettledOptionsFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	protected void setUpListAdapter() {
		optionsListAdapter = new AOTradingOptionsListAdapter(investments, false);
	}

	protected void setupItemClickListener(){
		optionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Investment investment = investments.get(position);
				Bundle args = new Bundle();
				args.putSerializable(Constants.EXTRA_INVESTMENT, investment);
				args.putBoolean(Constants.EXTRA_IS_INVESTMENT_OPEN, false);
				application.getFragmentManager().showDialogFragment(MyOptionsReceiptDialogFragment.class, args);
			}
		});
	}

}
