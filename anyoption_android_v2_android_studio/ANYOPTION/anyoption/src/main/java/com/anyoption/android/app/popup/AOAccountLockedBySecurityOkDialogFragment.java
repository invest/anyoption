package com.anyoption.android.app.popup;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Country;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.json.results.CountryIdMethodResult;

public class AOAccountLockedBySecurityOkDialogFragment extends AccountLockedBySecurityOkDialogFragment {

    public static final String TAG = AOAccountLockedBySecurityOkDialogFragment.class.getSimpleName();

    private TextView title;
    private TextView emailType;
    private TextView phoneNumberType;
    private TextView faxNumberType;
    private TextView workingHoursType;

    private TextView emailField;
    private TextView phoneNumberField;
    private TextView faxNumberField;
    private TextView workingHoursField;

    public static AOAccountLockedBySecurityOkDialogFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new AOAccountLockedBySecurityOkDialogFragment.");
        AOAccountLockedBySecurityOkDialogFragment fragment = new AOAccountLockedBySecurityOkDialogFragment();
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
        bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
        bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            setCancelable(args.getBoolean(Constants.EXTRA_CANCELABLE, false));
        }
    }

    @Override
    protected int getContentLayout() {
        return R.layout.popup_account_locked_by_security;
    }

    @Override
    protected void onCreateContentView(View contentView) {
        setupUI(contentView);
        setupCloseButton(contentView);
    }

    private void setupCloseButton(View contentView) {
        View okButton = contentView.findViewById(com.anyoption.android.R.id.ok_button);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void getUserCountry() {
        Application.get().getCommunicationManager().requestService(this, "getCountryIdCallBack", Constants.SERVICE_GET_COUNTRY_ID, new MethodRequest(), CountryIdMethodResult.class);
    }

    public void getCountryIdCallBack(Object result) {
        CountryIdMethodResult countryIdMethodResult = (CountryIdMethodResult) result;
        if (countryIdMethodResult != null && countryIdMethodResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {

            if(application.getCountriesMap().containsKey(countryIdMethodResult.getCountryId())) {
                Country country = application.getCountriesMap().get(countryIdMethodResult.getCountryId());
                String phoneNumber = country.getSupportPhone();
                String faxNumber = country.getSupportFax();

                emailField.setText(R.string.supportEmail);
                phoneNumberField.setText(phoneNumber.startsWith("+") ? phoneNumber : "+" + phoneNumber);
                faxNumberField.setText(faxNumber.startsWith("+") ? faxNumber : "+" + faxNumber);
            } else {
                emailField.setText(R.string.supportEmail);
                phoneNumberField.setText(R.string.supportScreenDefaultPhone);
                faxNumberField.setText(R.string.supportScreenDefaultFax);
            }
        } else {
            emailField.setText(R.string.supportEmail);
            phoneNumberField.setText(R.string.supportScreenDefaultPhone);
            faxNumberField.setText(R.string.supportScreenDefaultFax);
        }

        setupDynamicFieldsUI();
    }

    private void setupUI(View contentView) {
        title = (TextView) contentView.findViewById(R.id.account_locked_by_security_title);

        emailType = (TextView) contentView.findViewById(R.id.account_locked_by_security_type_email);
        phoneNumberType = (TextView) contentView.findViewById(R.id.account_locked_by_security_type_phone_number);
        faxNumberType = (TextView) contentView.findViewById(R.id.account_locked_by_security_type_fax_number);
        workingHoursType = (TextView) contentView.findViewById(R.id.account_locked_by_security_type_working_hours);

        emailField = (TextView) contentView.findViewById(R.id.account_locked_by_security_email);
        phoneNumberField = (TextView) contentView.findViewById(R.id.account_locked_by_security_phone_number);
        faxNumberField = (TextView) contentView.findViewById(R.id.account_locked_by_security_fax_number);
        workingHoursField = (TextView) contentView.findViewById(R.id.account_locked_by_security_working_hours);

        loading = (ProgressBar) contentView.findViewById(R.id.account_locked_by_security_popup_loading);

        showHideLoading(true);

        getUserCountry();

        TextUtils.decorateInnerText(title, null, title.getText().toString(), title.getText().toString(), FontsUtils.Font.ROBOTO_MEDIUM, 0, 0);
        TextUtils.decorateInnerText(emailType, null, emailType.getText().toString(), emailType.getText().toString(), FontsUtils.Font.ROBOTO_MEDIUM, 0, 0);
        TextUtils.decorateInnerText(phoneNumberType, null, phoneNumberType.getText().toString(), phoneNumberType.getText().toString(), FontsUtils.Font.ROBOTO_MEDIUM, 0, 0);
        TextUtils.decorateInnerText(faxNumberType, null, faxNumberType.getText().toString(), faxNumberType.getText().toString(), FontsUtils.Font.ROBOTO_MEDIUM, 0, 0);
        TextUtils.decorateInnerText(workingHoursType, null, workingHoursType.getText().toString(), workingHoursType.getText().toString(), FontsUtils.Font.ROBOTO_MEDIUM, 0, 0);

        Skin skin = application.getSkins().get(application.getSkinId());
        workingHoursField.setText(Html.fromHtml(getResources().getString(R.string.supportScreenWorkHours,
                skin.getSupportStartTime(), skin.getSupportEndTime(), "", "")));
    }


    private void setupDynamicFieldsUI() {
        TextUtils.decorateInnerText(emailField, null, emailField.getText().toString(), emailField.getText().toString(), FontsUtils.Font.ROBOTO_MEDIUM, R.color.link_color_1, 0);
        TextUtils.decorateInnerText(phoneNumberField, null, phoneNumberField.getText().toString(), phoneNumberField.getText().toString(), FontsUtils.Font.ROBOTO_MEDIUM, R.color.link_color_1, 0);
        TextUtils.decorateInnerText(faxNumberField, null, faxNumberField.getText().toString(), faxNumberField.getText().toString(), FontsUtils.Font.ROBOTO_MEDIUM, R.color.link_color_1, 0);

        emailField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{emailField.getText().toString()});
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "title");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "message");
                startActivity(Intent.createChooser(emailIntent, ""));
            }
        });

        phoneNumberField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                application.getTelephonyManager().callService(phoneNumberField.getText().toString());
            }
        });

        faxNumberField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                application.getTelephonyManager().callService(faxNumberField.getText().toString());
            }
        });

        showHideLoading(false);
    }

    private void showHideLoading(boolean isVisible) {
        if (loading != null) {
            if (isVisible) {
                loading.setVisibility(View.VISIBLE);
                emailType.setVisibility(View.INVISIBLE);
                phoneNumberType.setVisibility(View.INVISIBLE);
                faxNumberType.setVisibility(View.INVISIBLE);
                workingHoursType.setVisibility(View.INVISIBLE);
                emailField.setVisibility(View.INVISIBLE);
                phoneNumberField.setVisibility(View.INVISIBLE);
                faxNumberField.setVisibility(View.INVISIBLE);
                workingHoursField.setVisibility(View.INVISIBLE);
            } else {
                loading.setVisibility(View.INVISIBLE);
                emailType.setVisibility(View.VISIBLE);
                phoneNumberType.setVisibility(View.VISIBLE);
                faxNumberType.setVisibility(View.VISIBLE);
                workingHoursType.setVisibility(View.VISIBLE);
                emailField.setVisibility(View.VISIBLE);
                phoneNumberField.setVisibility(View.VISIBLE);
                faxNumberField.setVisibility(View.VISIBLE);
                workingHoursField.setVisibility(View.VISIBLE);
            }
        }
    }
}
