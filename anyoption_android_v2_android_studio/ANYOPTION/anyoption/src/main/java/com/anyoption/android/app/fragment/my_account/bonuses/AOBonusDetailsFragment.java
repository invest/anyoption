package com.anyoption.android.app.fragment.my_account.bonuses;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.requests.BonusMethodRequest;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class AOBonusDetailsFragment extends BonusDetailsFragment {

	public static AOBonusDetailsFragment newInstance(Bundle bundle) {
		AOBonusDetailsFragment aOBonusDetailsFragment = new AOBonusDetailsFragment();
		aOBonusDetailsFragment.setArguments(bundle);
		return aOBonusDetailsFragment;
	}

	private TextView acceptButton;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = super.onCreateView(inflater, container, savedInstanceState);
		
		acceptButton = (TextView) rootView.findViewById(R.id.bonus_details_screen_accept_button);
		View waiveButton = rootView.findViewById(R.id.buttonWaiveBonus);
	    if (bonus.getBonusStateId() == Constants.BONUS_STATE_PANDING) {
	    	acceptButton.setVisibility(View.VISIBLE);
	    	waiveButton.setVisibility(View.GONE);
            rootView.findViewById(R.id.buttonWaiveBonus).setOnClickListener(this);            
        }
		
		acceptButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				acceptButton.setEnabled(false);
				updateBonus();
			}
		});
		return rootView;
	}

	@Override
	protected void setupBonusState(BonusMethodRequest request) {
		if (bonus.getBonusStateId() == Constants.BONUS_STATE_PANDING) {
			request.setBonusStateId(Constants.BONUS_STATE_PANDING);
		} else {
			super.setupBonusState(request);
		}
	}

	@Override
	public void updateBonusCallBack(Object result) {
		acceptButton.setEnabled(true);
		MethodResult methodResult = (MethodResult) result;
		if (bonus.getBonusStateId() == Constants.BONUS_STATE_PANDING) {
	        if (methodResult != null && methodResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
				handleClaimedBonus();
	        }else{
	        	ErrorUtils.handleResultError(methodResult);
	        }
		}else{
			super.updateBonusCallBack(result);
		}
	}

	private void handleClaimedBonus(){
		Log.d(TAG, "Checking if the user has got the Bonus through a Bonus campaign.");
		if(application.getDeepLinksManager().getMarketingPage() != null){
			Log.d(TAG, "The user has got the Bonus through a Bonus campaign.");
			application.postEvent(new NavigationEvent(Application.Screen.TRADE, NavigationEvent.NavigationType.INIT));
		}else{
			Log.d(TAG, "It was NOT a Bonus campaign.");
			acceptButton.setEnabled(false);
			application.getCurrentActivity().onBackPressed();
		}
	}

}
