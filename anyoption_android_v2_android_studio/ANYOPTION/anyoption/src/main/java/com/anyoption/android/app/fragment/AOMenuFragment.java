package com.anyoption.android.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.Spannable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.widget.TextView;

/**
 * Created by veselinh on 23.11.2015 г..
 */
public class AOMenuFragment extends MenuFragment {

    private final String TAG = AOMenuFragment.class.getSimpleName();

    public static AOMenuFragment newInstance(Bundle bundle){
        AOMenuFragment fragment = new AOMenuFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        View footerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.menu_listview_footer_layout, null, false);

        TextView termsText = (TextView) footerView.findViewById(R.id.menu_bottom_text);

        TextUtils.OnInnerTextClickListener termsLinkListener = new TextUtils.OnInnerTextClickListener() {
            @Override
            public void onInnerTextClick(android.widget.TextView textView, String text, String innerText) {
                application.postEvent(new NavigationEvent(Application.Screen.TERMS_AND_CONDITIONS, NavigationEvent.NavigationType.DEEP));
            }
        };


        Spannable spannable = TextUtils.decorateInnerText(termsText, null, termsText.getText().toString(), getString(R.string.menu_bottom_text_risk_warning), FontsUtils.Font.ROBOTO_MEDIUM, 0, 0);
        TextUtils.makeClickableInnerText(termsText, spannable, termsText.getText().toString(), getString(R.string.menu_bottom_text_terms_link), null, 0, 0, true, termsLinkListener);

        menuList.addFooterView(footerView);
        return rootView;
    }

}
