package com.anyoption.android.app.fragment.my_account.banking.deposit;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.CreditCard;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.json.results.CreditCardsMethodResult;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class AOFirstNewCardFragment extends FirstNewCardFragment{

	public boolean haveCreditCardAdded = false;
	
	TextView skipLinkTextView;
	
	public static AOFirstNewCardFragment newInstance(Bundle bundle){
		AOFirstNewCardFragment firstNewCardFragment = new AOFirstNewCardFragment();	
		firstNewCardFragment.setArguments(bundle);
		return firstNewCardFragment;
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = super.onCreateView(inflater, container, savedInstanceState);
		cardHolder.setValue(application.getUser().getFirstName() + " " + application.getUser().getLastName());
		application.getAppseeManager().hideView(rootView.findViewById(R.id.card_number));
		skipLinkTextView = (TextView) rootView.findViewById(R.id.first_new_card_screen_skip_link);
		
		checkForHavingCC();
		
		return rootView;
	}
	
	public void checkForHavingCC() {
		skipLinkTextView.setVisibility(View.INVISIBLE);
		Application.get().getCommunicationManager().requestService(this, "haveCCCallBack", Constants.SERVICE_GET_AVAILABLE_CREDIT_CARDS_WITH_EMPTY_LINE,  new UserMethodRequest() , CreditCardsMethodResult.class);
	}

	/**
	 * get user countryId from the service and set it up.
	 * Change screen if it's unregulated country.
	 * @param result
	 */
	public void haveCCCallBack(Object result) {
    	CreditCardsMethodResult creditCardsMethodResult = (CreditCardsMethodResult) result;
    	if (creditCardsMethodResult != null && creditCardsMethodResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
    		CreditCard[] ccList;
    		ccList = creditCardsMethodResult.getOptions();
	    	if (null != ccList && ccList.length > 1) {
	    		haveCreditCardAdded = true;
	    	} else {
	    		haveCreditCardAdded = false;
	    	}
    	} else {
    		haveCreditCardAdded = false;
    	}
    	
    	if(!haveCreditCardAdded){
			skipLinkTextView.setVisibility(View.VISIBLE);
			skipLinkTextView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					application.postEvent(new NavigationEvent(application.getHomeScreen(), NavigationType.INIT));
				}
			});
		}else{
			skipLinkTextView.setVisibility(View.GONE);
		}
    }
	
}
