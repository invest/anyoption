package com.anyoption.android.app.util.adapter;

import java.util.List;

import com.anyoption.android.app.R;
import com.anyoption.android.app.popup.SplitBalanceDialogFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.service.requests.DepositBonusBalanceMethodRequest;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class AOTradingOptionsListAdapter extends TradingOptionsListAdapter implements OnClickListener{

	public AOTradingOptionsListAdapter(List<Investment> investments) {
		super(investments);
	}

	public AOTradingOptionsListAdapter(List<Investment> investments, boolean b) {
		super(investments, b);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rootView = super.getView(position, convertView, parent);
		Investment investment = investments.get(position);
		
		View balanceInfoButton = rootView.findViewById(R.id.trading_options_investment_info_layout);
		View balanceInfoImage = rootView.findViewById(R.id.trading_options_profitable_balance_info);
		if(isInvestmentOpen){
			balanceInfoImage.setVisibility(View.GONE);
		}else{
			balanceInfoImage.setVisibility(View.VISIBLE);
			balanceInfoButton.setTag(investment);
			balanceInfoButton.setOnClickListener(this);
		}
		
		View returnBalanceInfoButton = rootView.findViewById(R.id.trading_options_current_info_layout);
		View returnBalanceInfoImage = rootView.findViewById(R.id.trading_options_profitable_return_balance_info);
		returnBalanceInfoImage.setVisibility(View.VISIBLE);
		returnBalanceInfoButton.setTag(investment);
		returnBalanceInfoButton.setOnClickListener(this);
		
		return rootView;
	}

	@Override
	public void onClick(View v) {
		if(v.getTag() instanceof Investment){
			Investment investment = (Investment) v.getTag();
			Bundle bundle = new Bundle();
			bundle.putLong(Constants.EXTRA_INVESTMENT_ID, investment.getId());
		
			if(v.getId() == R.id.trading_options_current_info_layout){
				if(isInvestmentOpen){
					bundle.putInt(Constants.EXTRA_TYPE, DepositBonusBalanceMethodRequest.CALL_OPEN_INVESTMENTS_AMOUNT_BALANCE);
				}else{
					bundle.putInt(Constants.EXTRA_TYPE, DepositBonusBalanceMethodRequest.CALL_SETTLED_INVESTMENTS_RETURN_BALANCE);
				}
			}else if(v.getId() == R.id.trading_options_investment_info_layout){
				if(isInvestmentOpen){
					//Nothing
				}else{
					bundle.putInt(Constants.EXTRA_TYPE, DepositBonusBalanceMethodRequest.CALL_SETTLED_INVESTMENTS_AMOUNT_BALANCE);
				}
			}
			application.getFragmentManager().showDialogFragment(SplitBalanceDialogFragment.class, bundle);
		}
	}
	
}
