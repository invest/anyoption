package com.anyoption.android.app.popup;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.R;
import com.anyoption.android.app.activity.BubblesActivity;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.ColorUtils;
import com.anyoption.android.app.util.InvestmentUtil;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.enums.SkinGroup;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Anastas Arnaudov
 */
public class MyOptionsReceiptDialogFragment extends BaseDialogFragment {

    private static final String TAG = MyOptionsReceiptDialogFragment.class.getSimpleName();

    // Handler messages:
    public static final int MSG_REFRESH_LS = 1;
    ////////////////////

    protected static final String TIME_DATE_FORMAT_PATTERN = "HH:mm, dd/MM/yyyy";
    protected static final String TIME_BUBBLE_FORMAT_PATTERN    = "HH:mm:ss";

    protected User user;
    protected Investment investment;
    private Market market;
    private boolean isBinary;
    private boolean isOptionPlus;
    private boolean isDynamics;
    private boolean isBubbles;
    private String expriryTimeFormatted;
    private Currency currency;
    private DecimalFormat levelFormat;
    private DecimalFormat percentFormat;
    private double currentLevel;
    private SkinGroup skinGroup;
    private boolean isInvestmentOpen;
    private long marketId;
    private String marketName;
    private SimpleDateFormat bubbleTimeFormat;
    private Date bubblesStartTimeDate;

    private TextView returnCorrectAmountTextView;
    private TextView returnIncorrectAmountTextView;
    private View returnCorrectRibbon;
    private View belowRibbon;
    private TextView returnCorrectTextView;
    private TextView returnIncorrectTextView;
    private TextView returnAmountTextView;
    private View returnRibbon;
    private View expireCorrectLayout;
    private View expireIncorrectLayout;
    private View returnLayout;
    private TextView amountTextView;
    private TextView profitPercentTextView;
    private TextView idTextView;
    private TextView assetNameTextView;
    private Date bubblesEndTimeDate;
    private View expiryLevelLayout;
    private View expiryTimeLayout;
    private TextView returnAmountLabelTextView;
    private TextView goToMarketTextView;
    private boolean isFromTradeScreen;


    public static MyOptionsReceiptDialogFragment newInstance(Bundle bundle) {
        Log.d(TAG, "Created new AOMyOptionsReceiptDialogFragment.");
        MyOptionsReceiptDialogFragment fragment = new MyOptionsReceiptDialogFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    public static MyOptionsReceiptDialogFragment newInstance(Investment investment) {
        Bundle args = new Bundle();
        args.putSerializable(Constants.EXTRA_INVESTMENT, investment);
        return newInstance(args);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if(args != null){
            investment  = (Investment) args.getSerializable(Constants.EXTRA_INVESTMENT);
            isInvestmentOpen = args.getBoolean(Constants.EXTRA_IS_INVESTMENT_OPEN, false);
            isFromTradeScreen = args.getBoolean(Constants.EXTRA_IS_FROM_TRADE_SCREEN, false);

            user = Application.get().getUser();
            market      = application.getMarketForID(investment.getMarketId());
            marketId    = ((market != null) ? market.getId() : investment.getMarketId());
            marketName  = ((market != null) ? market.getDisplayName() : investment.getAsset());

            currency    = user.getCurrency();

            isBinary     = investment.getOpportunityTypeId() == Opportunity.TYPE_REGULAR;
            isOptionPlus = investment.getOpportunityTypeId() == Opportunity.TYPE_OPTION_PLUS;
            isDynamics   = investment.getOpportunityTypeId() == Opportunity.TYPE_DYNAMICS;
            isBubbles    = investment.getTypeId() == Investment.INVESTMENT_TYPE_BUBBLES;
            if(isBubbles){
                bubblesStartTimeDate  = new Date(investment.getBubbleStartTime().getTime());
                bubblesEndTimeDate    = new Date(investment.getBubbleEndTime().getTime());
            }

            SimpleDateFormat expiryTimeFormat = new SimpleDateFormat(TIME_DATE_FORMAT_PATTERN);
            expriryTimeFormatted    = expiryTimeFormat.format(investment.getTimeEstClosing());
            bubbleTimeFormat        = new SimpleDateFormat(TIME_BUBBLE_FORMAT_PATTERN);
            int decimalPointDigits  =  (market != null) ? (int)(long)market.getDecimalPoint() : 3;
            levelFormat             = Utils.getDecimalFormat(decimalPointDigits);
            levelFormat.applyPattern(Utils.getDecimalFormatPattern(decimalPointDigits));
            percentFormat           = new DecimalFormat("##.##");

            if(isInvestmentOpen){
                Skin skin = application.getSkins().get(application.getSkinId());
                skinGroup = skin.getSkinGroup();
            }
        }else{
            dismiss();
            Log.d(TAG, "No parameters!!! Closing the popup!!!");
        }

    }

    @Override
    protected void onCreateContentView(View contentView) {
        createUICommon(contentView);

        if(isBubbles){
            createUIBubbles(contentView);
        }else{
            createUIRegular(contentView);
        }

    }

    private void createUICommon(View contentView) {

        assetNameTextView                   = (TextView)  contentView.findViewById(R.id.receipt_asset_name);
        returnCorrectTextView               = (TextView)  contentView.findViewById(R.id.returnCorrectTextView);
        returnIncorrectTextView             = (TextView)  contentView.findViewById(R.id.returnIncorrectTextView);
        returnCorrectAmountTextView         = (TextView)  contentView.findViewById(R.id.returnCorrectAmountTextView);
        returnIncorrectAmountTextView       = (TextView)  contentView.findViewById(R.id.returnIncorrectAmountTextView);
        returnAmountLabelTextView           = (TextView)  contentView.findViewById(R.id.returnAmountLabelTextView);
        returnAmountTextView                = (TextView)  contentView.findViewById(R.id.returnAmountTextView);
        returnCorrectRibbon                 =             contentView.findViewById(R.id.returnCorrectRibbon);
        returnRibbon                        =             contentView.findViewById(R.id.returnAmountWinningImageView);
        expireCorrectLayout                 =             contentView.findViewById(R.id.expireCorrectLayout);
        expireIncorrectLayout               =             contentView.findViewById(R.id.expireIncorrectLayout);
        returnLayout                        =             contentView.findViewById(R.id.returnLayout);
        expiryLevelLayout                   =             contentView.findViewById(R.id.expiryLevelLayout);
        expiryTimeLayout                    =             contentView.findViewById(R.id.expiryTimeLayout);
        amountTextView                      = (TextView)  contentView.findViewById(R.id.receipt_amount);
        profitPercentTextView               = (TextView)  contentView.findViewById(R.id.receipt_profit_percent);
        idTextView                          = (TextView)  contentView.findViewById(R.id.receipt_id);
        goToMarketTextView                  = (TextView)  contentView.findViewById(R.id.goToMarketTextView);

        goToMarketTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMarket();
            }
        });

        View okButton = contentView.findViewById(R.id.receipt_ok_button);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        setupOpenSettledUI();
    }

    private void createUIBubbles(View contentView) {
        TextView bubblesBordersTextView     = (TextView)  contentView.findViewById(R.id.bubbleBordersTextView);
        TextView bubblesTimesTextView       = (TextView)  contentView.findViewById(R.id.bubbleTimesTextView);
        TextView investLevelTextView        = (TextView)  contentView.findViewById(R.id.investLevelTextView);
        TextView expiryLevelTextView        = (TextView)  contentView.findViewById(R.id.expiryLevelTextView);
        TextView expiryTimeTextView         = (TextView)  contentView.findViewById(R.id.expiryTimeTextView);

        bubblesBordersTextView.setText(investment.getBubbleLowLevel() + " - " + investment.getBubbleHighLevel());
        bubblesTimesTextView.setText(bubbleTimeFormat.format(bubblesStartTimeDate) + " - " + bubbleTimeFormat.format(bubblesEndTimeDate));
        assetNameTextView.setText(marketName);
        investLevelTextView.setText(investment.getLevel());
        if(!isInvestmentOpen){
            expiryLevelTextView.setText(String.valueOf(investment.getExpiryLevel()));
            expiryTimeTextView.setText(bubbleTimeFormat.format(investment.getTimeSettled()));
        }
        amountTextView.setText(AmountUtil.getFormattedAmount(investment.getAmount(), currency));
        idTextView.setText(getString(R.string.receiptId, investment.getId()));
    }

    private void createUIRegular(View contentView) {
        TextView epiryLabelTextView         = (TextView)  contentView.findViewById(R.id.epiryLabelTextView);
        TextView levelTextView              = (TextView)  contentView.findViewById(R.id.receipt_level);
        TextView expiryTimeTextView         = (TextView)  contentView.findViewById(R.id.receipt_expiry_time);
        ImageView marketIconImageView       = (ImageView) contentView.findViewById(R.id.receipt_asset_icon_image_view);
        ImageView putCallIcon               = (ImageView) contentView.findViewById(R.id.receipt_put_call_icon);

        //Add ":"
        epiryLabelTextView.setText(epiryLabelTextView.getText().toString() + ":");

        //Setup the Icon (UP / DOWN):
        if (    investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL ||
                (investment.getTypeId() == Investment.INVESTMENT_TYPE_ONE && investment.getOneTouchUpDown() == Investment.INVESTMENT_TYPE_CALL) ||
                investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY) {
            putCallIcon.setImageResource(R.drawable.circle_with_triangle_up_white);
        }else{
            putCallIcon.setImageResource(R.drawable.circle_with_triangle_down_white);
        }
        levelTextView.setText((isDynamics) ? String.valueOf(investment.getEventLevel()) : investment.getLevel());
        assetNameTextView.setText(marketName);
        expiryTimeTextView.setText(expriryTimeFormatted);
        amountTextView.setText(AmountUtil.getFormattedAmount(investment.getAmount(), currency));
        idTextView.setText(getString(R.string.receiptId, investment.getId()));

        //Setup the Market icon:
        if(isBinary){
            marketIconImageView.setVisibility(View.GONE);
        }else if(isDynamics){
            marketIconImageView.setVisibility(View.VISIBLE);
            marketIconImageView.setImageResource(R.drawable.dynamics_icon_1);
        }else if(isOptionPlus){
            marketIconImageView.setVisibility(View.VISIBLE);
            marketIconImageView.setImageResource(R.drawable.option_plus_icon_1);
        }

        goToMarketTextView.setText(getString(R.string.myOptionsGoToMarket, marketName));
    }

    private void setupOpenSettledUI(){
        if(isInvestmentOpen){
            expireCorrectLayout.setVisibility(View.VISIBLE);
            expireIncorrectLayout.setVisibility(View.VISIBLE);
            returnLayout.setVisibility(View.GONE);

            if(isBubbles){
                expiryLevelLayout.setVisibility(View.GONE);
                expiryTimeLayout.setVisibility(View.GONE);
            }

            if(isBinary || isOptionPlus){
                profitPercentTextView.setText(Math.round(investment.getOddsWin() * 100) + "%");
                if (    investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL ||
                        (investment.getTypeId() == Investment.INVESTMENT_TYPE_ONE && investment.getOneTouchUpDown() == Investment.INVESTMENT_TYPE_CALL)) {

                    double amount = InvestmentUtil.getOpenInvestmentReturn(investment, investment.getCurrentLevel()+1);
                    returnCorrectAmountTextView.setText(AmountUtil.getFormattedAmount(amount, currency));
                    amount = InvestmentUtil.getOpenInvestmentReturn(investment, investment.getCurrentLevel()-1);
                    returnIncorrectAmountTextView.setText(AmountUtil.getFormattedAmount(amount, currency));
                }else{
                    double amount = InvestmentUtil.getOpenInvestmentReturn(investment, investment.getCurrentLevel()-1);
                    returnCorrectAmountTextView.setText(AmountUtil.getFormattedAmount(amount, currency));
                    amount = InvestmentUtil.getOpenInvestmentReturn(investment, investment.getCurrentLevel()+1);
                    returnIncorrectAmountTextView.setText(AmountUtil.getFormattedAmount(amount, currency));
                }
            }else if(isDynamics){
                double profitPercent = InvestmentUtil.getDynamicsOpenInvestmentProfitPercent(investment);
                double profitAmount  = AmountUtil.getDoubleAmount(investment.getAmount()) + InvestmentUtil.getDynamicsProfitAmount(investment);
                profitPercentTextView.setText(percentFormat.format(profitPercent) + "%");
                returnCorrectAmountTextView.setText(AmountUtil.getFormattedAmount(profitAmount, currency));
                returnIncorrectAmountTextView.setText(AmountUtil.getFormattedAmount(0, currency));
            }else if(isBubbles){
                profitPercentTextView.setText(Math.round(investment.getOddsWin() * 100) + "%");
                double amount = InvestmentUtil.getBubblesOpenInvestmentReturnAmount(investment);
                returnCorrectAmountTextView.setText(AmountUtil.getFormattedAmount(amount, currency));
                returnIncorrectAmountTextView.setText(AmountUtil.getFormattedAmount(0, currency));
            }
        }else{
            expireCorrectLayout.setVisibility(View.GONE);
            expireIncorrectLayout.setVisibility(View.GONE);
            returnLayout.setVisibility(View.VISIBLE);

            if(isBubbles){
                expiryLevelLayout.setVisibility(View.VISIBLE);
                expiryTimeLayout.setVisibility(View.VISIBLE);
            }

            double profitPercent = InvestmentUtil.getSettledInvestmentProfitPercent(investment);
            profitPercentTextView.setText(percentFormat.format(profitPercent) + "%");

            returnAmountTextView.setText(investment.getAmountReturnWF());

            setupIsWining();
        }
    }

    private void setupIsWining(){
        long returnAmount = AmountUtil.parseAmountToLong(investment.getAmountReturnWF(), application.getCurrency());
        if (returnAmount > investment.getAmount()) {
            returnRibbon.setVisibility(View.VISIBLE);
            returnAmountLabelTextView.setTextColor(ColorUtils.getColor(R.color.receipt_win_text_color));
            returnAmountTextView.setTextColor(ColorUtils.getColor(R.color.receipt_win_text_color));
        } else {
            returnRibbon.setVisibility(View.GONE);
            returnAmountLabelTextView.setTextColor(ColorUtils.getColor(R.color.white));
            returnAmountTextView.setTextColor(ColorUtils.getColor(R.color.white));
        }
    }

    private void goToMarket(){
        dismiss();

        if(isFromTradeScreen){
            return;
        }

        if(isBubbles){
            goToBubbles();
            return;
        }

        Bundle bundle = new Bundle();
        bundle.putLong(Constants.EXTRA_MARKET_ID, marketId);
        if (isTablet) {
            bundle.putSerializable(Constants.EXTRA_MARKET, market);
            bundle.putString(Constants.EXTRA_ACTION_BAR_TITLE, marketName);
            bundle.putLong(Constants.EXTRA_MARKET_PRODUCT_TYPE, (market != null) ? market.getProductTypeId() : Constants.PRODUCT_TYPE_ID_BINARY);

            application.postEvent(new NavigationEvent(Application.Screen.ASSET_PAGE, NavigationEvent.NavigationType.HORIZONTAL, bundle));
        } else {
            bundle.putSerializable(Constants.EXTRA_MARKET, market);
            application.postEvent(new NavigationEvent(Application.Screen.ASSET_VIEW_PAGER, NavigationEvent.NavigationType.DEEP, bundle));
        }
    }

    private void goToBubbles(){
        application.setCurrentScreen(Application.Screen.BUBBLES);
        Intent intent = new Intent(getContext(), application.getSubClassForClass(BubblesActivity.class));
        startActivity(intent);
    }

    @Override
    protected int getContentLayout() {
        if(isBubbles){
            return R.layout.my_options_bubbles_receipt_dialog;
        }else{
            return R.layout.my_options_receipt_dialog;
        }
    }


}
