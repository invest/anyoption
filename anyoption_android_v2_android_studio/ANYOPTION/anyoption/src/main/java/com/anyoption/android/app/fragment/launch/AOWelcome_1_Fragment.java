package com.anyoption.android.app.fragment.launch;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anyoption.android.app.R;
import com.anyoption.android.app.fragment.BaseFragment;

/**
 * @author Anastas Arnaudov
 */
public class AOWelcome_1_Fragment extends BaseFragment{

    public static final String TAG = AOWelcome_1_Fragment.class.getSimpleName();

    public static AOWelcome_1_Fragment newInstance(){
        AOWelcome_1_Fragment fragment = new AOWelcome_1_Fragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.welcome_1_fragment, container, false);
        return rootView;
    }

}
