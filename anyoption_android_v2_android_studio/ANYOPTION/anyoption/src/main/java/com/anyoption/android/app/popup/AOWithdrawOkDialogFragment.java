package com.anyoption.android.app.popup;

import com.anyoption.android.app.R;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class AOWithdrawOkDialogFragment extends WithdrawOkDialogFragment{

	public static final String TAG = AOWithdrawOkDialogFragment.class.getSimpleName();

	/**
	 * Use this method to create new instance of the fragment.
	 * @return
	 */
	public static AOWithdrawOkDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new COWithdrawOkDialogFragment.");
		AOWithdrawOkDialogFragment fragment = new AOWithdrawOkDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		fragment.setArguments(bundle);
		return fragment;
	}
	
	@Override
	protected void onCreateContentView(View contentView) {
		super.onCreateContentView(contentView);
		View closeButton = contentView.findViewById(R.id.dialog_close);
		closeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}

}