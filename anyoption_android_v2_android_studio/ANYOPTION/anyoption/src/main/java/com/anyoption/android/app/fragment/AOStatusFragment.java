package com.anyoption.android.app.fragment;

import com.anyoption.android.app.R;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class AOStatusFragment extends StatusFragment{

	public static AOStatusFragment newInstance(){
		return AOStatusFragment.newInstance(new Bundle());
	}
	
	public static AOStatusFragment newInstance(Bundle bundle){
		AOStatusFragment statusfragment = new AOStatusFragment();
		statusfragment.setArguments(bundle);
		return statusfragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = super.onCreateView(inflater, container, savedInstanceState);
	
		balanceView = rootView.findViewById(R.id.status_left_layout);
		balanceInfoImageView = rootView.findViewById(R.id.status_balance_info_view);
		
		return rootView;
	}
	
}
