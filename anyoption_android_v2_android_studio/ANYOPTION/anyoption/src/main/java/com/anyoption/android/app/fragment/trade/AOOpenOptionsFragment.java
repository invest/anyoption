package com.anyoption.android.app.fragment.trade;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ListView;

import com.anyoption.android.app.R;
import com.anyoption.android.app.model.MyOptionsGroup;
import com.anyoption.android.app.popup.MyOptionsReceiptDialogFragment;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.android.app.util.adapter.MyOptionsExpandableListAdapter;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Investment;

public class AOOpenOptionsFragment extends OpenOptionsFragment {

	public static final String TAG = AOOpenOptionsFragment.class.getSimpleName();

	public static AOOpenOptionsFragment newInstance(Bundle bundle) {
		AOOpenOptionsFragment fragment = new AOOpenOptionsFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	private ExpandableListView expandableListView;
	private MyOptionsExpandableListAdapter adapter;
	protected CountDownTimer bubblesCountDownTimer;

	@Override
	protected void setUpListAdapter() {
		adapter = new MyOptionsExpandableListAdapter(expandableListView);
	}

	@Override
	protected void gotInvestmentsForAdapter(){
		adapter.setupGroups(investments);
		MyOptionsGroup bubblesMyOptionsGroup = adapter.getGroupForType(MyOptionsGroup.Type.BUBBLES);
		if(bubblesMyOptionsGroup != null && bubblesMyOptionsGroup.getInvestments().size() > 0){
			if(bubblesCountDownTimer != null){
				bubblesCountDownTimer.cancel();
			}
			long offset = 3000L;
			long time = bubblesMyOptionsGroup.getInvestments().get(0).getBubbleEndTime().getTime();
			long currentTime = TimeUtils.getCurrentServerTime();
			long period = time - currentTime + offset;
			if(period > 0){
				bubblesCountDownTimer = new CountDownTimer(period, period) {
					@Override
					public void onTick(long millisUntilFinished) {}
					@Override
					public void onFinish() {
						if(isAdded()){
							Log.d(TAG, "Bubbles Timer finished.");
							requestInvestments();
						}
					}
				}.start();
			}
		}
	}

	@Override
	protected void refreshList(){
		adapter.notifyDataSetChanged();
	}

	@Override
	protected void setupListView(){
		optionsListView 	= (ListView) root.findViewById(R.id.trading_open_options_listview);
		expandableListView  = (ExpandableListView) optionsListView;
		setUpListAdapter();
		expandableListView.setAdapter(adapter);
		expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
			@Override
			public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
				Investment investment = ((MyOptionsGroup)adapter.getGroup(groupPosition)).getInvestments().get(childPosition);
				Bundle args = new Bundle();
				args.putSerializable(Constants.EXTRA_INVESTMENT, investment);
				args.putBoolean(Constants.EXTRA_IS_INVESTMENT_OPEN, true);
				application.getFragmentManager().showDialogFragment(MyOptionsReceiptDialogFragment.class, args);
				return true;
			}
		});
		expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
			@Override
			public void onGroupCollapse(int groupPosition) {
				expandableListView.expandGroup(groupPosition);
			}
		});
	}

}
