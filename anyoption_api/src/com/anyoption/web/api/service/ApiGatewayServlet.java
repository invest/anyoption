package com.anyoption.web.api.service;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.ValidatorResources;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.anyoption.common.beans.base.ApiErrorCode;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.ApiUser;
import com.anyoption.common.beans.ApiUserService;
import com.anyoption.common.managers.ApiManager;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.util.JsonExclusionStrategy;
import com.anyoption.managers.SkinsManagerBase;
import com.anyoption.util.CommonUtil;

/**
 * @author EranL
 *
 */
public class ApiGatewayServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private static final String JSON_SERVICE_URL_PARAM_NAME_AO = "com.anyoption.web.api.ANYOPTION_SERVER_URL";
	private static final String JSON_SERVICE_URL_PARAM_NAME_COPYOP = "com.anyoption.web.api.COPYOP_SERVER_URL";
	
	public static String jsonServiceUrlAO;
	public static String jsonServiceUrlCopyop;
	private static Hashtable<String, Method> methods;	
	private static Hashtable<String, ApiErrorCode> errorCodes;
	private static ValidatorResources validatorResources;
	private static Hashtable<Long,  Hashtable<Long, Long>> apiUsersGroup;
	private static Hashtable<String, Hashtable<Long, String>> serviceRoles;
	private static Hashtable<String, ApiUserService> userServices;

	private static final Logger log = Logger.getLogger(ApiGatewayServlet.class);

	public void init() throws ServletException {
		jsonServiceUrlAO = getServletContext().getInitParameter(JSON_SERVICE_URL_PARAM_NAME_AO);
		jsonServiceUrlCopyop = getServletContext().getInitParameter(JSON_SERVICE_URL_PARAM_NAME_COPYOP);
		InputStream in = null;
		try {
			in = this.getClass().getClassLoader().getResourceAsStream("validation.xml");
			validatorResources = new ValidatorResources(in);
		} catch (Exception e) {
			log.error("Can't load ValidatorResources.", e);
		} finally {
			if (null != in) {
				try {
					in.close();
				} catch (Exception e) {
					log.error("Can't close InputStream", e);
				}
			}
		}

		log.info("JSON Service URL to Anyoption: " + jsonServiceUrlAO);
		log.info("JSON Service URL to Copyop: " + jsonServiceUrlCopyop);
		HashMap<String, Object> objectShare = null;
		try {
			Context initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			objectShare = (HashMap<String, Object>) envCtx.lookup("anyoption/share");
			log.info("Lookedup object share: " + objectShare);
		} catch (Exception e) {
			log.fatal("Cannot lookup object share", e);
		}
		
		try {
			userServices = ApiManager.getUserServices();
		} catch (SQLException e1) {
			log.fatal("Cannot load userServices", e1);
		}
		
		methods = new Hashtable<String, Method>();
		try {
			Class<?> serviceClass = Class.forName("com.anyoption.web.api.service.ApiGatewayService");
			for (Iterator<String> iter = userServices.keySet().iterator(); iter.hasNext();) {
				String serviceName = iter.next();
				ApiUserService apiUserService = userServices.get(serviceName);
				//pattern:  'function' in GatewayService : 'param 1' , 'param 2'
				methods.put(serviceName, serviceClass.getMethod(apiUserService.getMethodName() , new Class[] {Class.forName(apiUserService.getClassParam()), ApiUser.class, String.class}));
			}
			serviceRoles = ApiManager.getServiceRoles();
		} catch (Exception e) {
			log.fatal("Cannot load service methods! ", e);
		}
		try {
			errorCodes = ApiManager.getAPIErrorCodes();
			apiUsersGroup = ApiManager.getAPIUsersGroup();
		} catch (Exception e) {			
			log.fatal("Cannot load API error codes! ", e);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
		String uri = request.getRequestURI();
		String methodReq = uri.substring(uri.lastIndexOf("/") + 1);
		log.debug("URL request:" + request.getRequestURL() + " \n" +
				  "Request IP: " + CommonUtil.getIPAddress(request) + " \n" + 
				  "URI requested: " + uri + " \n" +
				  "Method: " + methodReq);
		Object result = null;
		ApiErrorCode apiErrorCode = errorCodes.get(ApiErrorCode.API_ERROR_CODE_GENERAL_SUCCESS);			
		try {
			Method m = methods.get(methodReq);
			if (null != m) {
				boolean validateApiUser = true;
				//check if the request for method is in the right protocol
				boolean protocolError = false;
				String port = request.getHeader("port");				
				log.debug("Port = " + port);						
				if (userServices.get(methodReq).isUseHttps()) {
					if (!CommonUtil.IsParameterEmptyOrNull(port) && !port.equals("443")) {
						protocolError = true;
					}
				} else {
					if (!CommonUtil.IsParameterEmptyOrNull(port) && port.equals("443")) {
						protocolError = true;
					}					
				}				
				if (CommonUtil.getConfig("system").equalsIgnoreCase("local")) { //cancel check in local environment
					protocolError = false;
				}				
				if (protocolError) {
					log.warn("Request is not in the right protocol");
					result = new MethodResult();
					((MethodResult)result).setApiCode(errorCodes.get(ApiErrorCode.API_ERROR_CODE_GENERAL_REQUEST_IS_NOT_IN_THE_RIGHT_PROTOCOL).getCode());
					((MethodResult)result).setApiCodeDescription(ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_GENERAL_REQUEST_IS_NOT_IN_THE_RIGHT_PROTOCOL).getDisplayName());
					responseToClient(response, result, request);	
					return;
				}								
				Class[] params = m.getParameterTypes();
				// deserialize the JSON request from client
				Object[] requestParams = new Object[params.length];
//				GsonBuilder builder = new GsonBuilder();
//				builder.registerTypeAdapter(Double.class, new CustomDoubleDeserializer());
//				builder.registerTypeAdapter(double.class, new CustomDoubleDeserializer());
//				builder.registerTypeAdapter(Integer.class, new CustomIntegerDeserializer());
//				builder.registerTypeAdapter(int.class, new CustomIntegerDeserializer());
//				builder.registerTypeAdapter(Long.class, new CustomLongDeserializer());
//				builder.registerTypeAdapter(long.class, new CustomLongDeserializer());
//				Gson gson = builder.create();
				Gson gson = new GsonBuilder().setExclusionStrategies(new JsonExclusionStrategy()).serializeNulls().create();
				requestParams[0] = gson.fromJson(new InputStreamReader(request.getInputStream(), "UTF-8"), params[0]);
				if (requestParams[0] instanceof MethodRequest) {
					if (!methodReq.equalsIgnoreCase("insertBinaryInvestment")) {
						((MethodRequest) requestParams[0]).setIp(CommonUtil.getIPAddress(request));
					}
				}
				if (methodReq.contains("simulate")) {
					validateApiUser = false;
				}
				if (validateApiUser) {
					//check if API user is valid
					ApiUser apiUser = new ApiUser();
					apiErrorCode = ApiGatewayService.validateApiUser((MethodRequest)requestParams[0], apiUser, serviceRoles.get(methodReq));
					if (apiErrorCode.getCode().equals(ApiErrorCode.API_ERROR_CODE_GENERAL_SUCCESS)) {
						//check for locale
						String locale = ((MethodRequest)requestParams[0]).getLocale();
						if (CommonUtil.IsParameterEmptyOrNull(locale)) {
							result = new MethodResult();
							((MethodResult)result).setApiCode(errorCodes.get(ApiErrorCode.API_ERROR_CODE_GENERAL_NO_LOCALE_WAS_FOUND).getCode());
							((MethodResult)result).setApiCodeDescription(ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_GENERAL_NO_LOCALE_WAS_FOUND).getDisplayName());
							log.debug("Locale is missing: " + locale);		
						} else {
							int skinId = Skin.SKINS_DEFAULT;
							if (SkinsManagerBase.getSkinIdByLocale().get(locale.toLowerCase()) != null) {
								skinId = SkinsManagerBase.getSkinIdByLocale().get(locale.toLowerCase()).getId();
							} else {
								((MethodRequest) requestParams[0]).setLocale(Locale.ENGLISH.getLanguage());
								log.debug("Locale " + locale + " was not found taking default locale: " + Locale.ENGLISH.getLanguage());
							}														
							((MethodRequest) requestParams[0]).setSkinId(skinId);
							String jsonServiceUrl =  jsonServiceUrlAO;
							if (userServices.get(methodReq).getJsonService() == 2) { // copyop
								jsonServiceUrl = jsonServiceUrlCopyop;
							}
							requestParams[1] = apiUser;
							requestParams[2] = jsonServiceUrl;
							result = m.invoke(ApiGatewayService.class, requestParams);	//reflection 
						}						
					} else { // API user was not found set error code					
						result = new MethodResult();
						((MethodResult)result).setApiCode(apiErrorCode.getCode());
						((MethodResult)result).setApiCodeDescription(apiErrorCode.getDescription());
					}
				} else {
					result = m.invoke(ApiGatewayService.class, requestParams);	//reflection 
				}
				// serialize the JSON response for client
				responseToClient(response, result, request);
			} else {
				log.warn("Method: " + methodReq + " not found!");
				result = new MethodResult();
				((MethodResult)result).setApiCode(errorCodes.get(ApiErrorCode.API_ERROR_CODE_GENERAL_CANT_FIND_METHOD).getCode());
				((MethodResult)result).setApiCodeDescription(ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_GENERAL_CANT_FIND_METHOD).getDisplayName());
				responseToClient(response, result, request);
			}
		} catch (Exception e) {
			result = new MethodResult();
			log.error("Problem executing " + methodReq + " Method! ", e);
			((MethodResult)result).setApiCode(errorCodes.get(ApiErrorCode.API_ERROR_CODE_GENERAL_CANT_PARSE_JSON).getCode());
			((MethodResult)result).setApiCodeDescription(ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_GENERAL_CANT_PARSE_JSON).getDisplayName());
			responseToClient(response, result, request);
		}
		
	}
	
	private static void responseToClient(HttpServletResponse response, Object result, HttpServletRequest request) {
		try {									 
			GsonBuilder builder = new GsonBuilder();
			builder.excludeFieldsWithoutExposeAnnotation();
			Gson gson = builder.create();
			// serialize the JSON response for client
			String jsonResponse = gson.toJson(result);
			if (null != jsonResponse && jsonResponse.length() > 0) {
				byte[] data = jsonResponse.getBytes("UTF-8");
				OutputStream os = response.getOutputStream();				
				response.setHeader("Content-Type", "application/json");
				response.setHeader("Content-Length", String.valueOf(data.length));
				os.write(data, 0, data.length);
				os.flush();
				os.close();				
			}
		} catch (Exception e) {
			log.error("Error while trying to send respomse to client ", e);
		}
	}

	/**
	 * @return the validatorResources
	 */
	public static ValidatorResources getValidatorResources() {
		return validatorResources;
	}

	/**
	 * @param validatorResources the validatorResources to set
	 */
	public static void setValidatorResources(ValidatorResources validatorResources) {
		ApiGatewayServlet.validatorResources = validatorResources;
	}
	

	/**
	 * @return the errorCodes
	 */
	public static Hashtable<String, ApiErrorCode> getErrorCodes() {
		return errorCodes;
	}

	/**
	 * @param errorCodes the errorCodes to set
	 */
	public static void setErrorCodes(Hashtable<String, ApiErrorCode> errorCodes) {
		ApiGatewayServlet.errorCodes = errorCodes;
	}

	/**
	 * @return the methods
	 */
	public static Hashtable<String, Method> getMethods() {
		return methods;
	}

	/**
	 * @return the apiUsersGroup
	 */
	public static Hashtable<Long, Hashtable<Long, Long>> getApiUsersGroup() {
		return apiUsersGroup;
	}

	/**
	 * @param apiUsersGroup the apiUsersGroup to set
	 */
	public static void setApiUsersGroup(
			Hashtable<Long, Hashtable<Long, Long>> apiUsersGroup) {
		ApiGatewayServlet.apiUsersGroup = apiUsersGroup;
	}
	
}