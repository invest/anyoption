package com.anyoption.web.api.service.helper;

import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.ApiUser;
import com.anyoption.common.managers.ApiManager;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.util.CommonUtil;
import com.anyoption.web.api.service.ApiGatewayService;
import com.anyoption.web.api.service.util.Constants;
import com.google.gson.Gson;

/**
 * @author EranL
 *
 */
public class ApiGatewayExamplesServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private static Hashtable<String, Method> methods;	
	private static final Logger log = Logger.getLogger(ApiGatewayExamplesServlet.class);

	public void init() throws ServletException {		
		methods = new Hashtable<String, Method>();
		try {
			Class<?> serviceClass = Class.forName("com.anyoption.web.api.service.helper.ApiGatewayExamplesService");
			//pattern:  'function' in ApiGatewayService : 'param 1' , 'param 2'						
			methods.put("getCapitalUserExtraFields", serviceClass.getMethod("getCapitalUserExtraFields", new Class[] {HttpServletRequest.class, String.class}));
			methods.put("getUser", serviceClass.getMethod("getUser", new Class[] {HttpServletRequest.class, String.class}));
			methods.put("insertCallMeContact", serviceClass.getMethod("insertCallMeContact", new Class[] {HttpServletRequest.class, String.class}));
			methods.put("insertUser", serviceClass.getMethod("insertUser", new Class[] {HttpServletRequest.class, String.class}));
			methods.put("getUserDetails", serviceClass.getMethod("getUserDetails", new Class[] {HttpServletRequest.class, String.class}));
			methods.put("userSettledInvestment", serviceClass.getMethod("userSettledInvestment", new Class[] {HttpServletRequest.class, String.class}));
			methods.put("userOpenInvestment", serviceClass.getMethod("userOpenInvestment", new Class[] {HttpServletRequest.class, String.class}));
			methods.put("openMarkets", serviceClass.getMethod("openMarkets", new Class[] {HttpServletRequest.class, String.class}));			
			methods.put("levelData", serviceClass.getMethod("levelData", new Class[] {HttpServletRequest.class, String.class}));			
			methods.put("currentOpportunityByMarket", serviceClass.getMethod("currentOpportunityByMarket", new Class[] {HttpServletRequest.class, String.class}));			
			methods.put("getLoginToken", serviceClass.getMethod("getLoginToken", new Class[] {HttpServletRequest.class, String.class}));
			methods.put("getCurrentLevelByOpportunityId", serviceClass.getMethod("getCurrentLevelByOpportunityId", new Class[] {HttpServletRequest.class, String.class}));
			methods.put("insertBinaryInvestmentByCurrentLevel", serviceClass.getMethod("insertBinaryInvestmentByCurrentLevel", new Class[] {HttpServletRequest.class, String.class}));			
			methods.put("insertBinaryInvestmentWithLevel", serviceClass.getMethod("insertBinaryInvestmentWithLevel", new Class[] {HttpServletRequest.class, String.class}));
			methods.put("getAllReturnOdds", serviceClass.getMethod("getAllReturnOdds", new Class[] {HttpServletRequest.class, String.class}));
			methods.put("getAllBinaryMarkets", serviceClass.getMethod("getAllBinaryMarkets", new Class[] {HttpServletRequest.class, String.class}));
			methods.put("getOpenHourlyBinaryMarkets", serviceClass.getMethod("getOpenHourlyBinaryMarkets", new Class[] {HttpServletRequest.class, String.class}));
			methods.put("getBinaryHomePageMarkets", serviceClass.getMethod("getBinaryHomePageMarkets", new Class[] {HttpServletRequest.class, String.class}));
			methods.put("insertBinaryInvestmentWithLevelExternal", serviceClass.getMethod("insertBinaryInvestmentWithLevelExternal", new Class[] {HttpServletRequest.class, String.class}));
			methods.put("userSettledInvestmentExternal", serviceClass.getMethod("userSettledInvestmentExternal", new Class[] {HttpServletRequest.class, String.class}));
			methods.put("userOpenInvestmentExternal", serviceClass.getMethod("userOpenInvestmentExternal", new Class[] {HttpServletRequest.class, String.class}));
			methods.put("getLastCurrenciesRate", serviceClass.getMethod("getLastCurrenciesRate", new Class[] {HttpServletRequest.class, String.class}));
			methods.put("getFTDUsersByDate", serviceClass.getMethod("getFTDUsersByDate", new Class[] {HttpServletRequest.class, String.class}));
			methods.put("getMarketingUsers", serviceClass.getMethod("getMarketingUsers", new Class[] {HttpServletRequest.class, String.class}));
			methods.put("insertCopyopUser", serviceClass.getMethod("insertCopyopProfile", new Class[] {HttpServletRequest.class, String.class}));
		} catch (Exception e) {
			log.fatal("Cannot load service methods! ", e);
		}
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {	
		String serviceUrl = request.getParameter("serviceUrl").trim();	
		boolean isLocal = CommonUtil.getConfig("system").equalsIgnoreCase("local");
		if (isLocal) {
			serviceUrl.replace("https", "http");
		}
		String methodReq = serviceUrl.substring(serviceUrl.lastIndexOf("/") + 1);		
		log.debug("Method: " + methodReq);
		if (methodReq.equals("login")) {
			login(request, response);
		} else {					
			Object result = null;
			try {
				Method m = methods.get(methodReq);
				if (null != m) {
					Class[] params = m.getParameterTypes();
					Object[] requestParams = new Object[params.length];
					requestParams[0] = request;
					requestParams[1] = serviceUrl;
					result = m.invoke(ApiGatewayService.class, requestParams);
				}
			} catch (Exception e) {
				result = new MethodResult();
				log.error("Problem executing " + methodReq + " Method! ", e);
			}	
			response.sendRedirect(Constants.API_GATEWAY_EXAMPLE_RESULT_URL + "result.jsp");
			responseToClient(response, result, request);
		}
	}
	
	/**
	 * Login to API in order to save API user, password and locale
	 * @param request
	 * @param response
	 */
	private static void login(HttpServletRequest request, HttpServletResponse response) {
		request.getParameter("locale");
		ApiUser apiUser =  new ApiUser();		
		try {
			String error = null;
			String page = "login.jsp";
			String userName = request.getParameter("apiUserName");
			String password = request.getParameter("apiPassword");
			ApiManager.getAPIUser(userName, password, apiUser);
    		if (apiUser.getId() == 0) {
    			log.debug("API User " + userName + " wrong user name or password");
    			error = "API User " + userName + " wrong user name or password";   			
    		} 
    		if (error == null && apiUser.getId() != 0 && !apiUser.isActive()) {
    			log.warn("API User " + userName + " is not active");
    			error = "API User " + userName + " is not active";    			
    		}        		
    		if (error == null) {
    			log.debug("API User loaded successfully" + apiUser.toString());	    		    		
    			page = "index.jsp";
    		}
    		request.getSession().setAttribute("errorMessage", error);
//    		response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
//    	    response.setHeader( "Location", Constants.API_GATEWAY_EXAMPLE_RESULT_URL + page);
//    	    response.setHeader( "Connection", "close" );
//    		//response.sendRedirect(Constants.API_GATEWAY_EXAMPLE_RESULT_URL + page);
    		if (request.isUserInRole("web")) {
//    		 RequestDispatcher dispatcher = request.getRequestDispatcher(Constants.API_GATEWAY_EXAMPLE_RESULT_URL + page);
//    		 dispatcher.forward(request, response);
//    		 return;
    		}
	
		} catch (Exception e) {
			// TODO Auto-generated catch block			
		}		
	}
	
	private static void responseToClient(HttpServletResponse response, Object result, HttpServletRequest request) {
		try {						
			Gson gson = new Gson();
			// serialize the JSON response for client
			String jsonResponse = gson.toJson(result);
			if (null != jsonResponse && jsonResponse.length() > 0) {
				byte[] data = jsonResponse.getBytes("UTF-8");
				OutputStream os = response.getOutputStream();				
				response.setHeader("Content-Type", "application/json");
				response.setHeader("Content-Length", String.valueOf(data.length));
				response.setCharacterEncoding("UTF-8");
				os.write(data, 0, data.length);
				os.flush();
				os.close();				
			}
		} catch (Exception e) {
			log.error("Error while trying to send respomse to client ", e);
		}
	}

}