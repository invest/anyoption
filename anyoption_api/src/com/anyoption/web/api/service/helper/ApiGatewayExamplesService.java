package com.anyoption.web.api.service.helper;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.log4j.Logger;

import com.anyoption.beans.base.Contact;
import com.anyoption.common.beans.base.ApiUser;
import com.anyoption.common.beans.base.Register;
import com.anyoption.common.service.requests.InsertInvestmentMethodRequest;
import com.anyoption.common.service.requests.InsertUserMethodRequest;
import com.anyoption.common.service.requests.InvestmentsMethodRequest;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.InvestmentMethodResult;
import com.anyoption.common.service.results.InvestmentsMethodResult;
import com.anyoption.common.service.results.MarketsMethodResult;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.UserAnycapitalMethodResult;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.json.requests.ChartDataMethodRequest;
import com.anyoption.json.requests.ContactMethodRequest;
import com.anyoption.json.requests.FTDUsersRequest;
import com.anyoption.json.requests.MarketingUsersMethodRequest;
import com.anyoption.json.results.ChartDataMethodResult;
import com.anyoption.json.results.ContactMethodResult;
import com.anyoption.json.results.FTDUsersMethodResult;
import com.anyoption.json.results.LastCurrenciesRateMethodResult;
import com.anyoption.json.results.MarketingUsersMethodResult;
import com.anyoption.json.results.OpportunityMethodResult;
import com.anyoption.json.results.OpportunityOddsPairResult;
import com.anyoption.util.CommonUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ApiGatewayExamplesService {
	
	private static final Logger log = Logger.getLogger(ApiGatewayExamplesService.class);
	
	/**
	 * Insert call me service
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult insertCallMeContact(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "insertCallMeContact";
		ContactMethodRequest request =  new ContactMethodRequest();
		ApiUser au = new ApiUser();
		au.setUserName(servletRequest.getParameter("apiUserName"));
		au.setPassword(servletRequest.getParameter("apiPassword"));		
		request.setApiUser(au);
		request.setLocale(servletRequest.getParameter("locale"));		
		Contact c = new Contact();
		c.setName(servletRequest.getParameter("name"));
		c.setEmail(servletRequest.getParameter("email"));
		c.setPhone(servletRequest.getParameter("phone"));
		c.setCountryName(servletRequest.getParameter("countries"));
		c.setIp(servletRequest.getParameter("ip"));		
		String combIdStr = servletRequest.getParameter("combId");
		int utcOffset = 0;
		try {
			utcOffset = Integer.parseInt(servletRequest.getParameter("utcOffset"));
		} catch (Exception e) {
			utcOffset = 0;
		}						
		if (!CommonUtil.IsParameterEmptyOrNull(combIdStr)) {
			try {
				c.setCombId(Long.valueOf(combIdStr));
			} catch (Exception e) {
				log.error("Can't cast combination Id with value:" + combIdStr);
			}
		}
		request.setUtcOffset(utcOffset);
		c.setDynamicParameter(servletRequest.getParameter("dynamicParameter"));
		c.setHttpReferer(servletRequest.getParameter("httpReferer"));
		c.setUserAgent(servletRequest.getParameter("userAgent"));		
		request.setContact(c);
		log.debug("Going to call serviceURL = " + serviceUrl);
		ContactMethodResult result = (ContactMethodResult)forwardServiceCall(serviceName, request, new ContactMethodResult(), serviceUrl);
        HttpSession session = servletRequest.getSession();
        session.setAttribute("methodResult", result);
        session.setAttribute("serviceName", serviceName);
		return result;
	}
	
	/**
	 * Insert user service
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult insertUser(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "insertUser";
		InsertUserMethodRequest request =  new InsertUserMethodRequest();
		//API user
		ApiUser au = new ApiUser();	
		au.setUserName(servletRequest.getParameter("apiUserName"));
		au.setPassword(servletRequest.getParameter("apiPassword"));
		//Register required fields
		request.setApiUser(au);
		request.setLocale(servletRequest.getParameter("locale"));
		Register register = new Register();	
		register.setFirstName(servletRequest.getParameter("firstName"));
		register.setLastName(servletRequest.getParameter("lastName"));
		register.setEmail(servletRequest.getParameter("email"));
		register.setCountryName(servletRequest.getParameter("countries"));
		register.setMobilePhone(servletRequest.getParameter("mobilePhone"));
		register.setPassword(servletRequest.getParameter("password"));
		register.setPassword2(servletRequest.getParameter("password2"));
		register.setIp(servletRequest.getParameter("ip"));
		register.setTerms(Boolean.parseBoolean(servletRequest.getParameter("terms")));
		int utcOffset = 0;
		try {
			utcOffset = Integer.parseInt(servletRequest.getParameter("utcOffset"));
		} catch (Exception e) {
			utcOffset = 0;
		}		
		request.setUtcOffset(utcOffset);		
		//Register optional fields		
		String combIdStr = servletRequest.getParameter("combinationId");
		if (!CommonUtil.IsParameterEmptyOrNull(combIdStr)) {
			try {
				register.setCombinationId(Long.valueOf(combIdStr));
			} catch (Exception e) {
				log.error("Can't cast combination Id with value:" + combIdStr);
			}
		}
		register.setDynamicParam(servletRequest.getParameter("dynamicParam"));
		register.setHttpReferer(servletRequest.getParameter("httpReferer"));
		register.setUserAgent(servletRequest.getParameter("userAgent"));		
		request.setRegister(register);
		log.debug("Going to call serviceURL = " + serviceUrl);
		UserMethodResult result = (UserMethodResult)forwardServiceCall(serviceName, request, new UserMethodResult(), serviceUrl);
        HttpSession session = servletRequest.getSession();
        session.setAttribute("methodResult", result);
        session.setAttribute("serviceName", serviceName);
		return result;
	}	
	
	/**
	 * Get user details 
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult getUserDetails(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "getUserDetails";
		UserMethodRequest request =  new UserMethodRequest();
		//API user
		ApiUser au = new ApiUser();	
		au.setUserName(servletRequest.getParameter("apiUserName"));
		au.setPassword(servletRequest.getParameter("apiPassword"));		
		request.setApiUser(au);		
		request.setLocale(servletRequest.getParameter("locale"));
		request.setUserName(servletRequest.getParameter("userName"));
		request.setPassword(servletRequest.getParameter("password"));
		int utcOffset = 0;
		try {
			utcOffset = Integer.parseInt(servletRequest.getParameter("utcOffset"));
		} catch (Exception e) {
			utcOffset = 0;
		}						
		request.setUtcOffset(utcOffset);
		log.debug("Going to call serviceURL = " + serviceUrl);
		UserMethodResult result = (UserMethodResult)forwardServiceCall(serviceName, request, new UserMethodResult(), serviceUrl);
        HttpSession session = servletRequest.getSession();
        session.setAttribute("methodResult", result);
        session.setAttribute("serviceName", serviceName);
		return result;		
	}
	
	/**
	 * @param servletRequest
	 * @param serviceURL
	 * @return
	 */
	public static MethodResult getUser(HttpServletRequest servletRequest, String serviceURL) {
		String serviceName = "getUser";
		UserMethodRequest request =  new UserMethodRequest();
		//API user
		ApiUser au = new ApiUser();	
		au.setUserName(servletRequest.getParameter("apiUserName"));
		au.setPassword(servletRequest.getParameter("apiPassword"));		
		request.setApiUser(au);		
		request.setLocale(servletRequest.getParameter("locale"));
		request.setUserName(servletRequest.getParameter("userName"));
		request.setPassword(servletRequest.getParameter("password"));
		int utcOffset = 0;
		try {
			utcOffset = Integer.parseInt(servletRequest.getParameter("utcOffset"));
		} catch (Exception e) {
			utcOffset = 0;
		}						
		request.setUtcOffset(utcOffset);
		log.debug("about to call serviceURL = " + serviceURL);
		UserMethodResult result = (UserMethodResult)forwardServiceCall(serviceName, request, new UserMethodResult(), serviceURL);
        HttpSession session = servletRequest.getSession();
        session.setAttribute("methodResult", result);
        session.setAttribute("serviceName", serviceName);
		return result;		
	}
	
	/**
	 * @param servletRequest
	 * @param serviceURL
	 * @return
	 */
	public static MethodResult getCapitalUserExtraFields(HttpServletRequest servletRequest, String serviceURL) {
		String serviceName = "getCapitalUserExtraFields";
		UserMethodRequest request =  new UserMethodRequest();
		//API user
		ApiUser au = new ApiUser();	
		au.setUserName(servletRequest.getParameter("apiUserName"));
		au.setPassword(servletRequest.getParameter("apiPassword"));		
		request.setApiUser(au);		
		request.setLocale(servletRequest.getParameter("locale"));
		request.setUserName(servletRequest.getParameter("userName"));
		request.setPassword(servletRequest.getParameter("password"));
		int utcOffset = 0;
		try {
			utcOffset = Integer.parseInt(servletRequest.getParameter("utcOffset"));
		} catch (Exception e) {
			utcOffset = 0;
		}						
		request.setUtcOffset(utcOffset);
		log.debug("about to call serviceURL = " + serviceURL);
		UserAnycapitalMethodResult result = (UserAnycapitalMethodResult)forwardServiceCall(serviceName, request, new UserAnycapitalMethodResult(), serviceURL);
        HttpSession session = servletRequest.getSession();
        session.setAttribute("methodResult", result);
        session.setAttribute("serviceName", serviceName);
		return result;		
	}
	
	/**
	 * Get user settled investments
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult userSettledInvestment(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "userSettledInvestment";
		return getUserInvestment(servletRequest, serviceName, serviceUrl);
	}
	
	/**
	 * Get user open investments
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult userOpenInvestment(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "userOpenInvestment";
		return getUserInvestment(servletRequest, serviceName, serviceUrl);
	}

	/**
	 * Get user open investments for external user
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult userOpenInvestmentExternal(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "userOpenInvestmentExternal";
		return getUserInvestment(servletRequest, serviceName, serviceUrl);
	}
	
	/**
	 * Get user settled investments for external user
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult userSettledInvestmentExternal(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "userSettledInvestmentExternal";
		return getUserInvestment(servletRequest, serviceName, serviceUrl);
	}
	
	/**
	 * Get user open/settled investments
	 * @param servletRequest
	 * @param serviceName
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult getUserInvestment(HttpServletRequest servletRequest, String serviceName, String serviceUrl) {
		InvestmentsMethodRequest request =  new InvestmentsMethodRequest();
		//API user
		ApiUser au = new ApiUser();	
		au.setUserName(servletRequest.getParameter("apiUserName"));
		au.setPassword(servletRequest.getParameter("apiPassword"));		
		request.setApiUser(au);		
		request.setLocale(servletRequest.getParameter("locale"));
		request.setUserName(servletRequest.getParameter("userName"));
		request.setPassword(servletRequest.getParameter("password"));
		request.setSettled(true);
		if (serviceName.endsWith("External")) {
			request.setApiExternalUserReference(servletRequest.getParameter("externalUser"));
		}
		log.debug("Going to call serviceURL = " + serviceUrl);
		InvestmentsMethodResult result = (InvestmentsMethodResult)forwardServiceCall(serviceName, request, new InvestmentsMethodResult(), serviceUrl);
        HttpSession session = servletRequest.getSession();
        session.setAttribute("methodResult", result);
        session.setAttribute("serviceName", serviceName);
		return result;
	}
	
	/**
	 * Get open hourly binary markets
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult openMarkets(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "openMarkets";
		return getMarketsService(serviceName, servletRequest, serviceUrl);
	}	
	
	/**
	 * Get all binary markets
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult getAllBinaryMarkets(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "getAllBinaryMarkets";
		return getMarketsService(serviceName, servletRequest, serviceUrl);
	}
	
	/**
	 * Get open hourly markets
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult getOpenHourlyBinaryMarkets(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "getOpenHourlyBinaryMarkets";
		return getMarketsService(serviceName, servletRequest, serviceUrl);
	}
	
	/**
	 * Get Binary homepage markets
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult getBinaryHomePageMarkets(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "getBinaryHomePageMarkets";
		return getMarketsService(serviceName, servletRequest, serviceUrl);
	}
		
	/**
	 * Get markets
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult getMarketsService(String serviceName, HttpServletRequest servletRequest, String serviceUrl) {
		UserMethodRequest request =  new UserMethodRequest();
		//API user
		ApiUser au = new ApiUser();	
		au.setUserName(servletRequest.getParameter("apiUserName"));
		au.setPassword(servletRequest.getParameter("apiPassword"));		
		request.setApiUser(au);		
		request.setLocale(servletRequest.getParameter("locale"));
		log.debug("Going to call serviceURL = " + serviceUrl);
		MarketsMethodResult result = (MarketsMethodResult)forwardServiceCall(serviceName, request, new MarketsMethodResult(), serviceUrl);
        HttpSession session = servletRequest.getSession();
        session.setAttribute("methodResult", result);
        session.setAttribute("serviceName", serviceName);
		return result;
	}
	
	/**
	 * Get level data by opportunity id 
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult levelData(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "levelData";
		ChartDataMethodRequest request =  new ChartDataMethodRequest();
		//API user
		ApiUser au = new ApiUser();	
		au.setUserName(servletRequest.getParameter("apiUserName"));
		au.setPassword(servletRequest.getParameter("apiPassword"));		
		request.setApiUser(au);		
		request.setLocale(servletRequest.getParameter("locale"));		
		long marketId = 0;
		try {
			marketId = Long.valueOf(servletRequest.getParameter("marketId"));
		} catch (Exception e) {
			marketId = 0;
		}
		long opportunityId = 0;
		try {
			opportunityId = Long.valueOf(servletRequest.getParameter("opportunityId"));
		} catch (Exception e) {
			opportunityId = 0;
		}
		request.setMarketId(marketId);
		request.setOpportunityId(opportunityId);		
		log.debug("Going to call serviceURL = " + serviceUrl);
		ChartDataMethodResult result = (ChartDataMethodResult)forwardServiceCall(serviceName, request, new ChartDataMethodResult(), serviceUrl);
        HttpSession session = servletRequest.getSession();
        session.setAttribute("methodResult", result);
        session.setAttribute("serviceName", serviceName);
		return result;
	}
	
	/**
	 * Insert binary investment with level 
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult insertBinaryInvestmentWithLevel(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "insertBinaryInvestmentWithLevel";
		return insertBinaryInvestment(serviceName, servletRequest, serviceUrl);
	}
	
	/**
	 * Insert binary investment by our level - default pagelevel will be -1
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult insertBinaryInvestmentByCurrentLevel(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "insertBinaryInvestmentByCurrentLevel";
		return insertBinaryInvestment(serviceName, servletRequest, serviceUrl);
	}
	
		
	/**
	 * Insert binary investment with level and external user
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult insertBinaryInvestmentWithLevelExternal(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "insertBinaryInvestmentWithLevelExternal";
		return insertBinaryInvestment(serviceName, servletRequest, serviceUrl);
	}
	
	/**
	 * Insert binary investment
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult insertBinaryInvestment(String serviceName, HttpServletRequest servletRequest, String serviceUrl) {
		InsertInvestmentMethodRequest request =  new InsertInvestmentMethodRequest();
		//API user
		ApiUser au = new ApiUser();	
		au.setUserName(servletRequest.getParameter("apiUserName"));
		au.setPassword(servletRequest.getParameter("apiPassword"));		
		request.setApiUser(au);		
		request.setLocale(servletRequest.getParameter("locale"));
		request.setUserName(servletRequest.getParameter("userName"));
		request.setPassword(servletRequest.getParameter("password"));
		request.setIpAddress(servletRequest.getParameter("ipAddress"));
		int utcOffset = 0;
		try {
			utcOffset = Integer.parseInt(servletRequest.getParameter("utcOffset"));
		} catch (Exception e) {
			utcOffset = 0;
		}
		long oppId = 0;
		try {
			oppId = Long.parseLong(servletRequest.getParameter("opportunityId"));
		} catch (Exception e) {
			oppId = 0;
		}
		double requestAmount = 0;
		try {
			requestAmount = Double.parseDouble(servletRequest.getParameter("requestAmount"));
		} catch (Exception e) {
			requestAmount = 0;
		}
		float pageOddsWin = 0;
		try {
			pageOddsWin = Float.parseFloat(servletRequest.getParameter("pageOddsWin"));
		} catch (Exception e) {
			pageOddsWin = 0;
		}
		float pageOddsLose = 0;
		try {
			pageOddsLose = Float.parseFloat(servletRequest.getParameter("pageOddsLose"));
		} catch (Exception e) {
			pageOddsLose = 0;
		}		
		int choice = 0;
		try {
			choice = Integer.parseInt(servletRequest.getParameter("choice"));
		} catch (Exception e) {
			choice = 0;
		}
		if (serviceName.startsWith("insertBinaryInvestmentWithLevel")) { 
			double pageLevel = 0;
			try {
				pageLevel = Double.parseDouble(servletRequest.getParameter("pageLevel"));
			} catch (Exception e) {
				pageLevel = 0;
			}
			request.setPageLevel(pageLevel);
		}				
		request.setOpportunityId(oppId);
		request.setUtcOffset(utcOffset);
		request.setRequestAmount(requestAmount);
		request.setPageOddsWin(pageOddsWin);		
		request.setPageOddsLose(pageOddsLose);	
		request.setChoice(choice);
		request.setApiExternalUserReference(servletRequest.getParameter("externalUser"));		
		log.debug("Going to call serviceURL = " + serviceUrl);
		InvestmentMethodResult result = (InvestmentMethodResult)forwardServiceCall(serviceName, request, new InvestmentMethodResult(), serviceUrl);
        HttpSession session = servletRequest.getSession();
        session.setAttribute("methodResult", result);
        session.setAttribute("serviceName", serviceName);
		return result;
	}
	
	/**
	 * Get current opportunity by market id
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult currentOpportunityByMarket(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "currentOpportunityByMarket";
		ChartDataMethodRequest request =  new ChartDataMethodRequest();
		//API user
		ApiUser au = new ApiUser();	
		au.setUserName(servletRequest.getParameter("apiUserName"));
		au.setPassword(servletRequest.getParameter("apiPassword"));		
		request.setApiUser(au);		
		request.setLocale(servletRequest.getParameter("locale"));
		long marketId = 0;
		try {
			marketId = Long.valueOf(servletRequest.getParameter("marketId"));
		} catch (Exception e) {
			marketId = 0;
		}
		request.setMarketId(marketId);				
		log.debug("Going to call serviceURL = " + serviceUrl);
		OpportunityMethodResult result = (OpportunityMethodResult)forwardServiceCall(serviceName, request, new OpportunityMethodResult(), serviceUrl);
        HttpSession session = servletRequest.getSession();
        session.setAttribute("methodResult", result);
        session.setAttribute("serviceName", serviceName);
		return result;
	}
	
	/**
	 * Get login token
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult getLoginToken(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "getLoginToken";
		UserMethodRequest request =  new UserMethodRequest();
		//API user
		ApiUser au = new ApiUser();	
		au.setUserName(servletRequest.getParameter("apiUserName"));
		au.setPassword(servletRequest.getParameter("apiPassword"));		
		request.setApiUser(au);		
		request.setLocale(servletRequest.getParameter("locale"));
		request.setUserName(servletRequest.getParameter("userName"));
		request.setPassword(servletRequest.getParameter("password"));
		log.debug("Going to call serviceURL = " + serviceUrl);
		UserMethodResult result = (UserMethodResult)forwardServiceCall(serviceName, request, new UserMethodResult(), serviceUrl);
        HttpSession session = servletRequest.getSession();
        session.setAttribute("methodResult", result);
        session.setAttribute("serviceName", serviceName);
		return result;
	}
	
	/**
	 * Get Current Level By OpportunityId
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult getCurrentLevelByOpportunityId(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "getCurrentLevelByOpportunityId";
		ChartDataMethodRequest request =  new ChartDataMethodRequest();
		//API user
		ApiUser au = new ApiUser();	
		au.setUserName(servletRequest.getParameter("apiUserName"));
		au.setPassword(servletRequest.getParameter("apiPassword"));		
		request.setApiUser(au);		
		request.setLocale(servletRequest.getParameter("locale"));
		long oppId = 0;
		try {
			oppId = Long.parseLong(servletRequest.getParameter("opportunityId"));
		} catch (Exception e) {
			oppId = 0;
		}
		request.setOpportunityId(oppId);
		log.debug("Going to call serviceURL = " + serviceUrl);
		OpportunityMethodResult result = (OpportunityMethodResult)forwardServiceCall(serviceName, request, new OpportunityMethodResult(), serviceUrl);
        HttpSession session = servletRequest.getSession();
        session.setAttribute("methodResult", result);
        session.setAttribute("serviceName", serviceName);
		return result;
	}
	
	/**
	 * Get all return odds
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult getAllReturnOdds(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "getAllReturnOdds";
		MethodRequest request =  new MethodRequest();
		//API user
		ApiUser au = new ApiUser();	
		au.setUserName(servletRequest.getParameter("apiUserName"));
		au.setPassword(servletRequest.getParameter("apiPassword"));		
		request.setApiUser(au);		
		request.setLocale(servletRequest.getParameter("locale"));
		log.debug("Going to call serviceURL = " + serviceUrl);
		OpportunityOddsPairResult result = (OpportunityOddsPairResult)forwardServiceCall(serviceName, request, new OpportunityOddsPairResult(), serviceUrl);
        HttpSession session = servletRequest.getSession();
        session.setAttribute("methodResult", result);
        session.setAttribute("serviceName", serviceName);
		return result;
	}
	
	/**
	 * Get last currencies rate
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult getLastCurrenciesRate(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "getLastCurrenciesRate";
		MethodRequest request =  new MethodRequest();
		//API user
		ApiUser au = new ApiUser();	
		au.setUserName(servletRequest.getParameter("apiUserName"));
		au.setPassword(servletRequest.getParameter("apiPassword"));		
		request.setApiUser(au);		
		request.setLocale(servletRequest.getParameter("locale"));
		log.debug("Going to call serviceURL = " + serviceUrl);
		LastCurrenciesRateMethodResult result = (LastCurrenciesRateMethodResult)forwardServiceCall(serviceName, request, new LastCurrenciesRateMethodResult(), serviceUrl);
        HttpSession session = servletRequest.getSession();
        session.setAttribute("methodResult", result);
        session.setAttribute("serviceName", serviceName);
		return result;
	}
	
	/**
	 * Get first time deposit users by date and API user
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult getFTDUsersByDate(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "getFTDUsersByDate";
		FTDUsersRequest request =  new FTDUsersRequest();
		//API user
		ApiUser au = new ApiUser();	
		au.setUserName(servletRequest.getParameter("apiUserName"));
		au.setPassword(servletRequest.getParameter("apiPassword"));		
		request.setApiUser(au);		
		request.setLocale(servletRequest.getParameter("locale"));
		request.setDateRequest(servletRequest.getParameter("dateRequest"));
		
		log.debug("Going to call serviceURL = " + serviceUrl);
		FTDUsersMethodResult result = (FTDUsersMethodResult)forwardServiceCall(serviceName, request, new FTDUsersMethodResult(), serviceUrl);
        HttpSession session = servletRequest.getSession();
        session.setAttribute("methodResult", result);
        session.setAttribute("serviceName", serviceName);
		return result;
	}
	
    /**
	 * Get marketing users
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult getMarketingUsers(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "getMarketingUsers";
		MarketingUsersMethodRequest request =  new MarketingUsersMethodRequest();
		//API user
		ApiUser au = new ApiUser();	
		au.setUserName(servletRequest.getParameter("apiUserName"));
		au.setPassword(servletRequest.getParameter("apiPassword"));		
		request.setApiUser(au);		
		request.setLocale(servletRequest.getParameter("locale"));
		long affKey = 0;
		try {
			affKey = Long.parseLong(servletRequest.getParameter("affiliateKey"));
		} catch (Exception e) {
			affKey = 0;
		}
		request.setAffiliateKey(affKey);
		request.setDateRequest(servletRequest.getParameter("dateRequest"));
		log.debug("Going to call serviceURL = " + serviceUrl);
		MarketingUsersMethodResult result = (MarketingUsersMethodResult)forwardServiceCall(serviceName, request, new MarketingUsersMethodResult(), serviceUrl);
        HttpSession session = servletRequest.getSession();
        session.setAttribute("methodResult", result);
        session.setAttribute("serviceName", serviceName);
		return result;
	}
	
	/**
	 * Insert Copyop profile
	 * @param servletRequest
	 * @param serviceUrl
	 * @return
	 */
	public static MethodResult insertCopyopProfile(HttpServletRequest servletRequest, String serviceUrl) {
		String serviceName = "insertCopyopProfile";
		com.copyop.json.requests.InsertUserMethodRequest request =  new com.copyop.json.requests.InsertUserMethodRequest();
		//API user
		ApiUser au = new ApiUser();	
		au.setUserName(servletRequest.getParameter("apiUserName"));
		au.setPassword(servletRequest.getParameter("apiPassword"));
		//Register required fields
		request.setApiUser(au);
		request.setLocale(servletRequest.getParameter("locale"));
		request.setNickname(servletRequest.getParameter("nickname"));
		request.setGender(servletRequest.getParameter("gender"));
		request.setFbId(servletRequest.getParameter("fbId"));
		request.setAcceptedTermsAndConditions(Boolean.parseBoolean(servletRequest.getParameter("acceptedTermsAndConditions")));
		Register register = new Register();	
		register.setFirstName(servletRequest.getParameter("firstName"));
		register.setLastName(servletRequest.getParameter("lastName"));
		register.setEmail(servletRequest.getParameter("email"));
		register.setCountryName(servletRequest.getParameter("countries"));
		register.setMobilePhone(servletRequest.getParameter("mobilePhone"));
		register.setPassword(servletRequest.getParameter("password"));
		register.setPassword2(servletRequest.getParameter("password2"));
		register.setIp(servletRequest.getParameter("ip"));	
		//Register optional fields		
		String combIdStr = servletRequest.getParameter("combinationId");
		if (!CommonUtil.IsParameterEmptyOrNull(combIdStr)) {
			try {
				register.setCombinationId(Long.valueOf(combIdStr));
			} catch (Exception e) {
				log.error("Can't cast combination Id with value:" + combIdStr);
			}
		}
		register.setDynamicParam(servletRequest.getParameter("dynamicParam"));
		request.setRegister(register);
		log.debug("Going to call serviceURL = " + serviceUrl);
		UserMethodResult result = (UserMethodResult)forwardServiceCall(serviceName, request, new UserMethodResult(), serviceUrl);
        HttpSession session = servletRequest.getSession();
        session.setAttribute("methodResult", result);
        session.setAttribute("serviceName", serviceName);
		return result;
	}
			
    /**
     * Forward Service Call to anyoptionService
     * @param serviceName
     * @param request
     * @param response
     * @return
     */
    private static Object forwardServiceCall(String serviceName, Object request, Object response, String url) {
    	try {
    		
	        byte[] buffer = new byte[2048]; // 2K read/write buffer. This might need change for better performance
	        URL u = new URL(url);
	        HttpURLConnection httpCon = (HttpURLConnection)u.openConnection();
	        httpCon.setDoOutput(true);	 
	        Gson prettyGson = new GsonBuilder().setPrettyPrinting().create();
	        String pretJson = prettyGson.toJson(request);
	        log.info("Pretty printing: " + pretJson);
	        Gson gson = new GsonBuilder().setPrettyPrinting().create();     
	        httpCon.getOutputStream().write(gson.toJson(request).getBytes("UTF-8"));
	        int readCount = -1;
	        InputStream is = httpCon.getInputStream(); 
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        OutputStream os = baos;
	        while ((readCount = is.read(buffer)) != -1) {
	            log.trace("readCount: " + readCount);
	            os.write(buffer, 0, readCount);
	        }	       
	        os.flush();
	        os.close();
            
            byte[] resp = baos.toByteArray();
            String json = new String(resp, "UTF-8");
            log.trace("json: " + json);           
            return gson.fromJson(json, response.getClass());
	    } catch (Exception e) {
	    	log.error("ERROR! problem forword service call to API gateway ", e); 
		}   
    	return null;
    }

}
