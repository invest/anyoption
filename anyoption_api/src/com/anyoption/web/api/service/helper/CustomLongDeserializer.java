package com.anyoption.web.api.service.helper;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

/**
 * Custom Long Deserializer in order to prevent null values when parsing JSON request
 * @author EranL
 *
 */
public class CustomLongDeserializer implements JsonDeserializer<Long> { 

    @Override
    public Long deserialize(JsonElement element, Type type, JsonDeserializationContext context) throws JsonParseException {
        try {
        	return element.getAsLong();          
        } catch (NumberFormatException e) {           
           return new Long(0); 
        }
    }

}
