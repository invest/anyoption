package com.anyoption.web.api.service.helper;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

/**
 * Custom Integer Deserializer in order to prevent null values when parsing JSON request
 * @author EranL
 *
 */
public class CustomIntegerDeserializer implements JsonDeserializer<Integer> { 

    @Override
    public Integer deserialize(JsonElement element, Type type, JsonDeserializationContext context) throws JsonParseException {
        try {
        	return element.getAsInt();          
        } catch (NumberFormatException e) {
        	return new Integer(0); 
        }
    }

}
