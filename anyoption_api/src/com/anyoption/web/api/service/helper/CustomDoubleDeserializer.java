package com.anyoption.web.api.service.helper;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

/**
 * Custom Double Deserializer in order to prevent null values when parsing JSON request
 * @author EranL
 *
 */
public class CustomDoubleDeserializer implements JsonDeserializer<Double> { 

    @Override
    public Double deserialize(JsonElement element, Type type, JsonDeserializationContext context) throws JsonParseException {
        try {
    		return element.getAsDouble();
        } catch (NumberFormatException e) {
        	return new Double(0); 
        }
    }

}
