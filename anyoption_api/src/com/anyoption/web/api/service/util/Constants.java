package com.anyoption.web.api.service.util;

import com.anyoption.util.CommonUtil;

public class Constants {
	
	public static final String API_GATEWAY_EXAMPLE_URL = CommonUtil.getConfig("api.gateway.exmaple.url");
	public static final String API_GATEWAY_EXAMPLE_RESULT_URL = CommonUtil.getConfig("api.gateway.exmaple.result.url");
	public static final String API_GATEWAY_EXAMPLE_RESULT_URL_COPYOP = CommonUtil.getConfig("api.gateway.exmaple.result.url.copyop");
	public static final String API_GATEWAY_URL = CommonUtil.getConfig("api.gateway.url");
	public static final String ANYOPTION_HP_URL = CommonUtil.getConfig("anyoption.home.page.https");	
	public static final String COPYOP_HP_URL = CommonUtil.getConfig("copyop.home.page");	
	
}
