//package com.anyoption.web.api.service.util;
//
//import com.anyoption.common.annotations.AnyoptionNoJSON;
//import com.google.gson.ExclusionStrategy;
//import com.google.gson.FieldAttributes;
//
///**
// * Exclusion strategy class for exclusing anyoption json annotation fields
// *
// * @author EranL
// *
// */
//public class JsonExclusionStrategy implements ExclusionStrategy {
//
//    /* (non-Javadoc)
//     * @see com.google.gson.ExclusionStrategy#shouldSkipClass(java.lang.Class)
//     */
//    public boolean shouldSkipClass(Class<?> clazz) {
//    	return false;
//    }
//
//    /* (non-Javadoc)
//     * @see com.google.gson.ExclusionStrategy#shouldSkipField(com.google.gson.FieldAttributes)
//     */
//    public boolean shouldSkipField(FieldAttributes f) {
//    	return f.getAnnotation(AnyoptionNoJSON.class) != null;
//    }
//}
