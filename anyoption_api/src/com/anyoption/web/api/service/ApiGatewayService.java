package com.anyoption.web.api.service;

import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Locale;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.validator.Validator;
import org.apache.commons.validator.ValidatorException;
import org.apache.log4j.Logger;

import com.anyoption.beans.User;
import com.anyoption.beans.base.Contact;
import com.anyoption.common.beans.ApiUser;
import com.anyoption.common.beans.Notification;
import com.anyoption.common.beans.OpportunityType;
import com.anyoption.common.beans.ServerConfiguration;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.ApiErrorCode;
import com.anyoption.common.beans.base.ApiExternalUser;
import com.anyoption.common.beans.base.ApiNotification;
import com.anyoption.common.beans.base.ApiNotificationResponse;
import com.anyoption.common.beans.base.ApiUserActivity;
import com.anyoption.common.beans.base.ApiUserActivityType;
import com.anyoption.common.beans.base.Country;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.beans.base.Platform;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.UserAnycapital;
import com.anyoption.common.beans.base.UserAnycapitalExtraFields;
import com.anyoption.common.enums.DeviceFamily;
import com.anyoption.common.managers.ApiExternalUserManagerBase;
import com.anyoption.common.managers.ApiManager;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.LoginManager;
import com.anyoption.common.managers.LoginsManager;
import com.anyoption.common.service.requests.InsertInvestmentMethodRequest;
import com.anyoption.common.service.requests.InsertUserMethodRequest;
import com.anyoption.common.service.requests.InvestmentsMethodRequest;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.ErrorMessage;
import com.anyoption.common.service.results.InvestmentMethodResult;
import com.anyoption.common.service.results.InvestmentsMethodResult;
import com.anyoption.common.service.results.MarketsMethodResult;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.UserAnycapitalExtraFieldsMethodResult;
import com.anyoption.common.service.results.UserAnycapitalMethodResult;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.json.requests.ApproveNotificationMethodResponse;
import com.anyoption.json.requests.ChartDataMethodRequest;
import com.anyoption.json.requests.ClientNotificationMethodRequest;
import com.anyoption.json.requests.ContactMethodRequest;
import com.anyoption.json.requests.FTDUsersRequest;
import com.anyoption.json.requests.MarketingUsersMethodRequest;
import com.anyoption.json.results.ChartDataMethodResult;
import com.anyoption.json.results.ContactMethodResult;
import com.anyoption.json.results.FTDUsersMethodResult;
import com.anyoption.json.results.LastCurrenciesRateMethodResult;
import com.anyoption.json.results.MarketingUsersMethodResult;
import com.anyoption.json.results.OpportunityMethodResult;
import com.anyoption.json.results.OpportunityOddsPairResult;
import com.anyoption.json.util.ValidatorFieldChecks;
import com.anyoption.managers.OpportunitiesManagerBase;
import com.anyoption.managers.SkinsManagerBase;
import com.anyoption.managers.UsersManagerBase;
import com.anyoption.util.CommonUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author EranL
 *
 */
public class ApiGatewayService {
	private static final Logger log = Logger.getLogger(ApiGatewayService.class);

    /**
     * Insert Call me contact
     * @param request
     * @return
     */
    public static ContactMethodResult insertCallMeContact(ContactMethodRequest request, ApiUser apiUser, String jsonURL) {
    	String serviceName = "insertContact";
    	log.debug("Going to call service:" + serviceName + ".\n" +
    			"Details:\n" +
				"API user name: " + request.getApiUser().getUserName() + "\n" +
				"skinId: " + request.getSkinId() + "\n" +
				"ip:" + request.getContact().getIp() + "\n" +
				request.getContact().toString());

    	ContactMethodResult result = new ContactMethodResult();
    	result.setContact(new Contact());
    	//fields validation
    	String res = validateFields(request, result, serviceName, "call_me", request, request.getContact().getCountryName(), true);
    	if (res != null) { //error found
    		return result;
    	}

		//set additional values
    	long counrtyId = CountryManagerBase.getCountryByA3Code().get(request.getContact().getCountryName().toUpperCase()).getId();
		request.getContact().setCountryId(counrtyId);
    	request.getContact().setType(Contact.CONTACT_ME_TYPE);
    	long combinationId = SkinsManagerBase.getSkin(request.getSkinId()).getDefaultCombinationId();
    	if (request.getContact().getCombId() != null && request.getContact().getCombId() != new Long(0)) {
    		combinationId = request.getContact().getCombId();
    	}
    	//set Netrefer combination and DP to affiliate
    	if (apiUser.getType() == ApiUser.API_USER_TYPE_AFFILIATE) {
    		ArrayList<Long> netreferCombinationsIdList = SkinsManagerBase.getNetreferCombinationsIdList(request.getSkinId());
    		if (!netreferCombinationsIdList.isEmpty()) {
        		combinationId = netreferCombinationsIdList.get(0);
        		log.debug("Setting default combination id to affiliate " + combinationId);
    		}
    		String dynamicParameter = request.getContact().getDynamicParameter();
    		request.getContact().setDynamicParameter("API_" + dynamicParameter);
    	}

    	request.getContact().setCombId(combinationId);
    	ContactMethodResult jsonResult = (ContactMethodResult)forwardServiceCall(serviceName, request, new ContactMethodResult(), jsonURL);
    	result = (ContactMethodResult)handleResponseFromJSON(serviceName, result, request, jsonResult);
    	if (result.getApiCode() != ApiErrorCode.API_ERROR_CODE_GENERAL_SUCCESS) {
    		return result;
    	}
    	//success from JSON
    	long contactId = result.getContact().getId();
    	log.debug("Contact was created/updated successfully id:" + contactId);
    	if (contactId != 0) {
	    	ApiUserActivity apiUserActivity = new ApiUserActivity();
	    	apiUserActivity.setApiUserId(apiUser.getId());
	    	apiUserActivity.setTypeId(ApiUserActivityType.API_USER_ACTIVITY_TYPE_INSERT_CALL_ME);
	    	apiUserActivity.setKeyValue(result.getContact().getId());
	    	try {
				ApiManager.insertAPIUserActivity(apiUserActivity);
			} catch (SQLException e) {
				log.error("Error while trying to insert API user activity, contactId: " + contactId, e);
			}
    	}
    	return result;
    }

    /**
     * Insert user
     * @param request
     * @param apiUser
     * @return
     */
    public static UserMethodResult insertUser(InsertUserMethodRequest request, ApiUser apiUser, String jsonURL) {
    	String serviceName = "insertUser";
    	log.debug("Going to call service:" + serviceName + ".\n" +
    			"Details:\n" +
				"API user name: " + request.getApiUser().getUserName() + "\n" +
				request.getRegister().toString());
    	UserMethodResult result = new UserMethodResult();
    	//fields validation
    	String res = validateFields(request, result, serviceName, "insert_user", request, request.getRegister().getCountryName(), true);
    	if (res != null) { //error found
    		return result;
    	}

		//set additional values
    	long combinationId = SkinsManagerBase.getSkin(request.getSkinId()).getDefaultCombinationId();
    	if (request.getRegister().getCombinationId() != null && request.getRegister().getCombinationId() != new Long(0)) {
    		combinationId = request.getRegister().getCombinationId();
    	}
    	//set Netrefer combination and DP to affiliate
    	if (apiUser.getType() == ApiUser.API_USER_TYPE_AFFILIATE) {
    		ArrayList<Long> netreferCombinationsIdList = SkinsManagerBase.getNetreferCombinationsIdList(request.getSkinId());
    		if (!netreferCombinationsIdList.isEmpty()) {
        		combinationId = netreferCombinationsIdList.get(0);
        		log.debug("Setting default combination id to affiliate " + combinationId);
    		}
    		String dynamicParameter = request.getRegister().getDynamicParam();
    		request.getRegister().setDynamicParam("API_" + dynamicParameter);
    	}
    	request.getRegister().setCombinationId(combinationId);
		request.getRegister().setContactByEmail(true);
		request.getRegister().setContactBySms(true);
		long counrtyId = CountryManagerBase.getCountryByA3Code().get(request.getRegister().getCountryName().toUpperCase()).getId();
		request.getRegister().setCountryId(counrtyId);
		// new register funnel copy email to userName
		request.getRegister().setUserName(request.getRegister().getEmail());
		UserMethodResult jsonResult = (UserMethodResult)forwardServiceCall(serviceName, request, new UserMethodResult(), jsonURL);
    	result = (UserMethodResult)handleResponseFromJSON(serviceName, result, request, jsonResult);
    	if (result.getApiCode() != ApiErrorCode.API_ERROR_CODE_GENERAL_SUCCESS) {
    		return result;
    	}
    	//success from JSON
    	long userId = result.getUser().getId();
    	log.debug("User was created/updated successfully id:" + userId);
    	if (userId != 0) {
	    	ApiUserActivity apiUserActivity = new ApiUserActivity();
	    	apiUserActivity.setApiUserId(apiUser.getId());
	    	apiUserActivity.setTypeId(ApiUserActivityType.API_USER_ACTIVITY_TYPE_INSERT_USER);
	    	apiUserActivity.setKeyValue(userId);
	    	try {
				ApiManager.insertAPIUserActivity(apiUserActivity);
			} catch (SQLException e) {
				log.error("Error while trying to insert API user activity, userId: " + userId, e);
			}
    	}
    	return result;
    }

    /**
     * Get User Details
     * @param request
     * @return
     */
    public static UserMethodResult getUserDetails(UserMethodRequest request, ApiUser apiUser, String jsonURL) {
    	String serviceName = "insertLoginToken";
    	return getUserService(serviceName, request, apiUser, jsonURL);
    }

    /**
     * Get user details service
     * @param serviceName
     * @param request
     * @param apiUser
     * @return
     */
    private static UserMethodResult getUserService(String serviceName, UserMethodRequest request, ApiUser apiUser, String jsonURL) {
    	log.debug("Going to call service:" + serviceName + ". " + "\n" +
				"Details: \n " +
				"API user name: " + request.getApiUser().getUserName() + "\n" +
				"ip:" + request.getIp() + "\n" +
				"skinId: " + request.getSkinId() + "\n" +
				"userName: " + request.getUserName() + "\n" +
				"password: " + request.getPassword());
    	UserMethodResult result = new UserMethodResult();
    	//fields validation
    	String res = validateFields(request, result, serviceName, "login", request, null, false);
    	if (res != null) { //error found
    		return result;
    	}
    	UserMethodResult jsonResult = (UserMethodResult)forwardServiceCall(serviceName, request, new UserMethodResult(), jsonURL);
    	result = (UserMethodResult)handleResponseFromJSON(serviceName, result, request, jsonResult);
    	return result;
    }
    
    /**
     * @param request
     * @param apiUser
     * @param jsonURL
     * @return
     */
    public static UserAnycapitalMethodResult getUser(UserMethodRequest request, ApiUser apiUser, String jsonURL) {
    	String serviceName = "getUser";
    	log.debug("START getUser");
    	log.debug("service call :" + serviceName + ". " + "\n" +
				"Details: \n " +
				"API user name: " + request.getApiUser().getUserName() + "\n" +
				"ip:" + request.getIp() + "\n" +
				"skinId: " + request.getSkinId() + "\n" +
				"userName: " + request.getUserName() + "\n" +
				"password: " + request.getPassword());
    	UserAnycapitalMethodResult result = new UserAnycapitalMethodResult();
    	//fields validation
    	String error = validateFields(request, result, serviceName, "getUser", request, null, false);
    	log.debug("validateFields, error: " + error);
    	if (error == null) { 
    		try {
    			UserAnycapital user = UsersManagerBase.getCapitalUser(request);
				UserAnycapitalMethodResult jsonResult = new UserAnycapitalMethodResult();
				jsonResult.setData(user);
				result = (UserAnycapitalMethodResult)handleResponseFromJSON(serviceName, result, request, jsonResult);
			} catch (Exception e) {
				log.error("ERROR! getUser", e);
			}
    	}
    	log.debug("END getUser; " + result.toString());
    	return result;
    }
    
    
    /**
     * @param request
     * @param apiUser
     * @param jsonURL
     * @return
     */
    public static UserAnycapitalExtraFieldsMethodResult getCapitalUserExtraFields(UserMethodRequest request, ApiUser apiUser, String jsonURL) {
    	String serviceName = "getCapitalUserExtraFields";
    	log.debug("START getCapitalUserExtraFields");
    	log.debug("service call :" + serviceName + ". " + "\n" +
				"Details: \n " +
				"API user name: " + request.getApiUser().getUserName() + "\n" +
				"ip:" + request.getIp() + "\n" +
				"skinId: " + request.getSkinId() + "\n" +
				"userName: " + request.getUserName() + "\n" +
				"password: " + request.getPassword());
    	UserAnycapitalExtraFieldsMethodResult result = new UserAnycapitalExtraFieldsMethodResult();
    	//fields validation
    	String error = validateFields(request, result, serviceName, "getCapitalUserExtraFields", request, null, false);
    	log.debug("validateFields, error: " + error);
    	if (error == null) { 
    		try {
    			UserAnycapitalExtraFields user = UsersManagerBase.getCapitalUserExtraFields(request);
    			UserAnycapitalExtraFieldsMethodResult jsonResult = new UserAnycapitalExtraFieldsMethodResult();
				jsonResult.setData(user);
				result = (UserAnycapitalExtraFieldsMethodResult)handleResponseFromJSON(serviceName, result, request, jsonResult);
			} catch (Exception e) {
				log.error("ERROR! getCapitalUserExtraFields", e);
			}
    	}
    	log.debug("END getCapitalUserExtraFields; " + result.toString());
    	return result;
    }

    /**
     * Get  user settled investments
     * @param request
     * @return
     */
    public static InvestmentsMethodResult userSettledInvestment(InvestmentsMethodRequest request, ApiUser apiUser, String jsonURL) {
    	request.setSettled(true);
    	return userInvestments(request, apiUser, false, jsonURL);
    }

    /**
     * Get  user open investments
     * @param request
     * @return
     */
    public static InvestmentsMethodResult userOpenInvestment(InvestmentsMethodRequest request, ApiUser apiUser, String jsonURL) {
    	request.setSettled(false);
    	return userInvestments(request, apiUser, false, jsonURL);
    }

    /**
     * Get  user settled investments External
     * @param request
     * @return
     */
    public static InvestmentsMethodResult userSettledInvestmentExternal(InvestmentsMethodRequest request, ApiUser apiUser, String jsonURL) {
    	request.setSettled(true);
    	return userInvestments(request, apiUser, true, jsonURL);
    }

    /**
     * Get  user open investments External
     * @param request
     * @return
     */
    public static InvestmentsMethodResult userOpenInvestmentExternal(InvestmentsMethodRequest request, ApiUser apiUser, String jsonURL) {
    	request.setSettled(false);
    	return userInvestments(request, apiUser, true, jsonURL);
    }

    /**
     * Get settled or open investments of user
     * @param request
     * @param apiUser
     * @param validateExternalUser
     * @return
     */
    public static InvestmentsMethodResult userInvestments(InvestmentsMethodRequest request, ApiUser apiUser, boolean validateExternalUser, String jsonURL) {
    	String serviceName = "getUserInvestments";
    	boolean sendServiceToJSON = true;
    	InvestmentsMethodResult jsonResult = null;
    	log.debug("Going to call service:" + serviceName + ". " + "\n" +
				"Details: \n " +
				"API user name: " + request.getApiUser().getUserName() + "\n" +
				"ip:" + request.getIp() + "\n" +
				"skinId: " + request.getSkinId() + "\n" +
				"userName: " + request.getUserName() + "\n" +
				"password: " + request.getPassword());
    	InvestmentsMethodResult result = new InvestmentsMethodResult();
    	//fields validation
    	String res = validateFields(request, result, serviceName, "login", request, null, false);
    	if (res != null) { //error found
    		return result;
    	}
		// external user validation
		if (validateExternalUser) {
    		res = validateFields(request, result, serviceName, "external_user", request, null, false);
    		if (res != null) {
    			return result;
    		}
		}
    	//validate user service and encrypt password
    	validateUserAndPassword(request, result, validateExternalUser);
    	if (result.getApiCode() != null && !result.getApiCode().equalsIgnoreCase(ApiErrorCode.API_ERROR_CODE_GENERAL_SUCCESS)) {
        	log.debug("Validation error when trying to get user");
    		return result;
    	}
    	if (validateExternalUser) {
	    	try {
	    		ApiExternalUser apiExternalUser = ApiExternalUserManagerBase.getApiExternalUser(request.getApiExternalUserReference(), apiUser.getId());
	    		if (apiExternalUser == null || apiExternalUser.getId() == 0 ) { // external user was not found return empty list
	    			log.debug("API external user wasn't found");
	    			sendServiceToJSON = false;
	    			jsonResult = new InvestmentsMethodResult();
	    		}
			} catch (SQLException e) {
				log.error("Validation error when trying to get external user ", e);
				sendServiceToJSON = false;
				jsonResult = new InvestmentsMethodResult();
			}
    	}
    	//We want to retrieve only last 10 days for settled investment
    	if (request.isSettled()) {
	        Calendar c = Calendar.getInstance();
	        request.setTo(c.getInstance());
	        c.add(Calendar.DAY_OF_MONTH , -10);
	    	request.setFrom(c);
    	}
    	if (sendServiceToJSON) {
    		jsonResult = (InvestmentsMethodResult)forwardServiceCall(serviceName, request, new InvestmentsMethodResult(), jsonURL);
    	}
    	result = (InvestmentsMethodResult)handleResponseFromJSON(serviceName, result, request, jsonResult);
    	// return only binary investment
    	if (result.getInvestments() != null && result.getInvestments().length > 0) {
    		ArrayList<Investment> invList = new ArrayList<Investment>();
    		for (Investment i : result.getInvestments()) {
    			if (i.getOpportunityTypeId() == OpportunityType.PRODUCT_TYPE_BINARY) {
    				//set time in millisecond
    				if (i.getTimeEstClosing() != null) {
    					i.setTimeEstClosingMillsec(i.getTimeEstClosing().getTime());
    				}
    				if (i.getTimePurchased() != null) {
    					i.setTimePurchasedMillsec(i.getTimePurchased().getTimeInMillis());
    				}
    				invList.add(i);
    			}
    		}
    		result.setInvestments(invList.toArray(new Investment[invList.size()]));
    	}
    	return result;
    }

    /**
     * Get open hourly binary markets
     * @param request
     * @return
     */
    public static MarketsMethodResult getOpenHourlyBinaryMarkets(MethodRequest request, ApiUser apiUser, String jsonURL) {
    	String serviceName = "getOpenHourlyBinaryMarkets";
    	return marketsService(serviceName, request, apiUser, jsonURL);
    }

    /**
     * Get all binary markets
     * @param request
     * @return
     */
    public static MarketsMethodResult getAllBinaryMarkets(MethodRequest request, ApiUser apiUser, String jsonURL) {
    	String serviceName = "getAllBinaryMarkets";
    	return marketsService(serviceName, request, apiUser, jsonURL);
    }

    /**
     * Get open binary home page markets
     * @param request
     * @return
     */
    public static MarketsMethodResult getBinaryHomePageMarkets(MethodRequest request, ApiUser apiUser, String jsonURL) {
    	String serviceName = "getBinaryHomePageMarkets";
    	return marketsService(serviceName, request, apiUser, jsonURL);
    }

    /**
     * Get markets
     * @param request
     * @return
     */
    public static MarketsMethodResult marketsService(String serviceName, MethodRequest request, ApiUser apiUser, String jsonURL) {
    	log.debug("Going to call service:" + serviceName + ". " + "\n" +
				"Details: \n " +
				"API user name: " + request.getApiUser().getUserName() + "\n" +
				"ip:" + request.getIp() + "\n" +
				"skinId: " + request.getSkinId());
    	MarketsMethodResult result = new MarketsMethodResult();
    	MarketsMethodResult jsonResult = (MarketsMethodResult)forwardServiceCall(serviceName, request, new MarketsMethodResult(), jsonURL);
    	result = (MarketsMethodResult)handleResponseFromJSON(serviceName, result, request, jsonResult);
    	return result;
    }


    /**
     * Get level data by market or opportunity id
     * @param request
     * @return
     */
    public static ChartDataMethodResult levelData(ChartDataMethodRequest request, ApiUser apiUser, String jsonURL) {
    	String serviceName = "getChartData";
    	log.debug("Going to call service:" + serviceName + ". " + "\n" +
				"Details: \n " +
				"API user name: " + request.getApiUser().getUserName() + "\n" +
				"ip:" + request.getIp() + "\n" +
				"skinId: " + request.getSkinId() + "\n" +
				"OpportunityId: " + request.getOpportunityId());

    	ChartDataMethodResult result = new ChartDataMethodResult();
    	//validation
    	String res = validateFields(request, result, serviceName, "level_data", request, null, false);
    	if (res != null) { //error found
    		return result;
    	}
    	Opportunity opportunity = null;
    	try {
			opportunity = OpportunitiesManagerBase.getRunningById(request.getOpportunityId());
			if (opportunity.getMarketId() == 0) {
				log.warn("No market can be find for opportunityId: " + request.getOpportunityId());
	    		result.setApiCode(ApiErrorCode.API_ERROR_CODE_VALIDATION_CANT_FIND_OPPORTUNITY);
	    		result.setApiCodeDescription(ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_VALIDATION_CANT_FIND_OPPORTUNITY).getDisplayName());
				return result;
			}
		} catch (SQLException e) {
			log.error("Exception when trying to get opportunityId: " + request.getOpportunityId(), e);
    		result.setApiCode(ApiErrorCode.API_ERROR_CODE_VALIDATION_CANT_FIND_OPPORTUNITY);
    		result.setApiCodeDescription(ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_VALIDATION_CANT_FIND_OPPORTUNITY).getDisplayName());
			return result;
		}
    	request.setMarketId(opportunity.getMarketId());
    	ChartDataMethodResult jsonResult = (ChartDataMethodResult)forwardServiceCall(serviceName, request, new ChartDataMethodResult(), jsonURL);
    	result = (ChartDataMethodResult)handleResponseFromJSON(serviceName, result, request, jsonResult);
    	if (result.getApiCode() != ApiErrorCode.API_ERROR_CODE_GENERAL_SUCCESS) {
    		return result;
    	}
    	// convert time to millisecond
    	if (result.getTimeEstClosing() != null) {
    		result.setTimeEstClosingMillsec(result.getTimeEstClosing().getTime());
    	}
    	if (result.getTimeFirstInvest() != null) {
    		result.setTimeFirstInvestMillsec(result.getTimeFirstInvest().getTime());
    	}
    	if (result.getTimeLastInvest() != null) {
    		result.setTimeLastInvestMillsec(result.getTimeLastInvest().getTime());
    	}
    	return result;
    }

    /**
     * Insert Binary Investment By Current Level
     * @param request
     * @param apiUser
     * @return
     */
    public static InvestmentMethodResult insertBinaryInvestmentByCurrentLevel(InsertInvestmentMethodRequest request, ApiUser apiUser, String jsonURL) {
    	log.debug("Going to insert binary investment by current level");
    	request.setPageLevel(-1);
    	return insertBinaryInvestment(false, true, request, apiUser, jsonURL);
    }

    /**
     * Insert Binary Investment With Level
     * @param request
     * @param apiUser
     * @return
     */
    public static InvestmentMethodResult insertBinaryInvestmentWithLevel(InsertInvestmentMethodRequest request, ApiUser apiUser, String jsonURL) {
    	log.debug("Going to insert binary investment with level");
    	return insertBinaryInvestment(false, false, request, apiUser, jsonURL);
    }

    /**
     * Insert Binary Investment With Level and external user
     * @param request
     * @param apiUser
     * @return
     */
    public static InvestmentMethodResult insertBinaryInvestmentWithLevelExternal(InsertInvestmentMethodRequest request, ApiUser apiUser, String jsonURL) {
    	log.debug("Going to insert binary investment with level and external user");
    	return insertBinaryInvestment(true, false, request, apiUser, jsonURL);
    }


    /**
     * Insert Binary investment service
     * @param request
     * @return
     */
    public static InvestmentMethodResult insertBinaryInvestment(boolean validateExternalUser, boolean takeCurrentLevel, InsertInvestmentMethodRequest request, ApiUser apiUser, String jsonURL) {
    	String serviceName = "insertInvestment";
    	log.debug("Going to call service:" + serviceName + ". " + "\n" +
				"Details: \n " +
				"API user name: " + request.getApiUser().getUserName() + "\n" +
				"ip:" + request.getIp() + "\n" +
				"skinId: " + request.getSkinId() + "\n" +
				"Choice: " + request.getChoice() + "\n" +
				"PageOddsWin: " + request.getPageOddsWin() + "\n" +
				"PageOddsLose: " + request.getPageOddsLose() + "\n" +
				"Client IP: " + request.getIpAddress() + "\n" +
				"Opportunity Id: " + request.getOpportunityId() + "\n" +
				"Utc Offset: " + request.getUtcOffset() + "\n" +
				"PageLevel: " + request.getPageLevel() + "\n" +
				"API external user reference: " + request.getApiExternalUserReference() + "\n" +
				"Request Amount: " + request.getRequestAmount());

    	InvestmentMethodResult result = new InvestmentMethodResult();
    	//user validation
    	String res = validateFields(request, result, serviceName, "login", request, null, false);
    	if (res != null) { //error found
    		return result;
    	}
    	//insert binary validation
    	res = validateFields(request, result, serviceName, "insert_binary_investment", request, null, false);
    	if (res != null) { //error found
    		return result;
    	}
    	if (takeCurrentLevel) {
    		request.setPageLevel(-1); // set to -1 - later on in the service we will take current level
    	} else {
    		// page level validation
    		res = validateFields(request, result, serviceName, "page_level", request, null, false);
    		if (res != null) {
    			return result;
    		}
    		// external user validation
    		if (validateExternalUser) {
	    		res = validateFields(request, result, serviceName, "external_user", request, null, false);
	    		if (res != null) {
	    			return result;
	    		}
    		}
    		if (request.getPageLevel() <= 0) {
    			log.debug("Validation error before calling " + serviceName  + " service call. page level is invalid: " + request.getPageLevel());
            	result.setApiCode(ApiErrorCode.API_ERROR_CODE_VALIDATION_ERROR);
            	result.setApiCodeDescription(ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_VALIDATION_ERROR).getDisplayName());
            	ErrorMessage em = new ErrorMessage("pageLevel", ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_VALIDATION_LEVEL_IS_INVALID).getDisplayName(), ApiErrorCode.API_ERROR_CODE_VALIDATION_LEVEL_IS_INVALID);
            	ArrayList<ErrorMessage> listEm = new ArrayList<ErrorMessage>();
            	listEm.add(em);
            	result.setUserMessages(listEm);
            	return result;
    		}
    	}
    	//validate user service and encrypt password
    	validateUserAndPassword(request, result, validateExternalUser);
    	if (result.getApiCode() != null && !result.getApiCode().equalsIgnoreCase(ApiErrorCode.API_ERROR_CODE_GENERAL_SUCCESS)) {
        	log.debug("Validation error when trying to get user");
    		return result;
    	}

    	if (validateExternalUser) {
	    	try {
	    		//check if the user belongs to the API user
	    		if (ApiGatewayServlet.getApiUsersGroup().get(apiUser.getId()) == null || ApiGatewayServlet.getApiUsersGroup().get(apiUser.getId()).get(result.getUser().getId()) == null) {
					log.error("Validation error invalid_user_for_API");
		    		result.setApiCode(ApiErrorCode.API_ERROR_CODE_VALIDATION_INVALID_USER_FOR_API);
		    		result.setApiCodeDescription(ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_VALIDATION_INVALID_USER_FOR_API).getDisplayName());
	    			return result;
	    		}
	    		ApiExternalUser apiExternalUser = ApiExternalUserManagerBase.getApiExternalUser(request.getApiExternalUserReference(), apiUser.getId());
	    		if (apiExternalUser == null || apiExternalUser.getId() == 0 ) { // external user was not found return empty list
	    			apiExternalUser = new ApiExternalUser();
	    			apiExternalUser.setUserId(result.getUser().getId());
	    			apiExternalUser.setReference(request.getApiExternalUserReference());
	    			apiExternalUser.setApiUserId(apiUser.getId());
	    			apiExternalUser.setIp(request.getIpAddress());
	    			apiExternalUser.setUtcOffset( CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset()));
	    			log.debug("API external user wasn't found. inserting new external user: " + request.getApiExternalUserReference() + " for APIUserName: " + apiUser.getUserName());
	    			ApiExternalUserManagerBase.InsertApiExternalUser(apiExternalUser);
	    		}
	    		request.setApiExternalUserId(apiExternalUser.getId());
			} catch (SQLException e) {
				log.error("Validation error when trying to get external user ", e);
	    		result.setApiCode(ApiErrorCode.API_ERROR_CODE_VALIDATION_ERROR);
	    		result.setApiCodeDescription(ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_VALIDATION_ERROR).getDisplayName());
    			return result;
			}
    	}
    	// set page odds win to fit the service (for example if we get 0.7 we set it to be 1.7)
    	request.setPageOddsWin(1 + request.getPageOddsWin());
    	InvestmentMethodResult jsonResult = (InvestmentMethodResult)forwardServiceCall(serviceName, request, new InvestmentMethodResult(), jsonURL);
    	result = (InvestmentMethodResult)handleResponseFromJSON(serviceName, result, request, jsonResult);
    	if (result.getApiCode() != ApiErrorCode.API_ERROR_CODE_GENERAL_SUCCESS) {
    		return result;
    	}
    	long investmentId = result.getInvestment().getId();
    	log.debug("Investment was created successfully id:" + investmentId);
    	if (investmentId != 0) {
	    	ApiUserActivity apiUserActivity = new ApiUserActivity();
	    	apiUserActivity.setApiUserId(apiUser.getId());
	    	apiUserActivity.setTypeId(ApiUserActivityType.API_USER_ACTIVITY_TYPE_INSERT_BINARY_INV);
	    	apiUserActivity.setKeyValue(investmentId);
	    	try {
				ApiManager.insertAPIUserActivity(apiUserActivity);
			} catch (SQLException e) {
				log.error("Error while trying to insert API user activity, investmentId: " + investmentId, e);
			}
    	}
    	return result;
    }

    /**
     * Get current open opportunity id by market
     * @param request
     * @return
     */
    public static OpportunityMethodResult currentOpportunityByMarket(ChartDataMethodRequest request, ApiUser apiUser, String jsonURL) {
    	String serviceName = "getCurrentHourlyOppByMarket";
    	log.debug("Going to call service:" + serviceName + ". " + "\n" +
				"Details: \n " +
				"API user name: " + request.getApiUser().getUserName() + "\n" +
				"ip:" + request.getIp() + "\n" +
				"skinId: " + request.getSkinId() + "\n" +
				"MarketId:" + request.getMarketId());
		OpportunityMethodResult result = new OpportunityMethodResult();
    	//validation
    	String res = validateFields(request, result, serviceName, "level_data", request, null, false);
    	if (res != null) { //error found
    		return result;
    	}
    	//set additional values
		request.setOpportunityTypeId(Opportunity.TYPE_REGULAR);
		OpportunityMethodResult jsonResult = (OpportunityMethodResult)forwardServiceCall(serviceName, request, new OpportunityMethodResult(), jsonURL);
    	result = (OpportunityMethodResult)handleResponseFromJSON(serviceName, result, request, jsonResult);
    	if (result.getApiCode() != ApiErrorCode.API_ERROR_CODE_GENERAL_SUCCESS) {
    		return result;
    	}
    	Opportunity opp = result.getOpportunity();
    	if (opp != null) {
    		//convert time to millsec
    		opp.setTimeEstClosingMillsec(opp.getTimeEstClosing().getTime());
    		opp.setTimeFirstInvestMillsec(opp.getTimeFirstInvest().getTime());
    		opp.setTimeLastInvestMillsec(opp.getTimeLastInvest().getTime());
    		//set page odds lose to be (for example if we get 0.85 we convert the odds 0.15 to fit insert investment page)
    		BigDecimal bd = new BigDecimal(1 - opp.getPageOddsLose());
    		opp.setPageOddsLose(bd.setScale(2,BigDecimal.ROUND_HALF_EVEN).floatValue());
    	}
    	return result;
    }

    /**
     * Get current opportunity id by market
     * @param request
     * @return
     */
    public static OpportunityMethodResult getCurrentLevelByOpportunityId(ChartDataMethodRequest request, ApiUser apiUser, String jsonURL) {
    	String serviceName = "getCurrentLevelByOpportunityId";
    	log.debug("Going to call service:" + serviceName + ". " + "\n" +
				"Details: \n " +
				"API user name: " + request.getApiUser().getUserName() + "\n" +
				"ip:" + request.getIp() + "\n" +
				"skinId: " + request.getSkinId() + "\n" +
				"OpportunityId: " + request.getOpportunityId());
		OpportunityMethodResult result = new OpportunityMethodResult();
    	//validation
    	String res = validateFields(request, result, serviceName, "level_data", request, null, false);
    	if (res != null) { //error found
    		return result;
    	}

		OpportunityMethodResult jsonResult = (OpportunityMethodResult)forwardServiceCall(serviceName, request, new OpportunityMethodResult(), jsonURL);
    	result = (OpportunityMethodResult)handleResponseFromJSON(serviceName, result, request, jsonResult);
    	if (result.getApiCode() != ApiErrorCode.API_ERROR_CODE_GENERAL_SUCCESS) {
    		return result;
    	}
    	Opportunity opp = result.getOpportunity();
    	if (opp != null) { //convert time to millsec
    		opp.setTimeEstClosingMillsec(opp.getTimeEstClosing().getTime());
    		opp.setTimeFirstInvestMillsec(opp.getTimeFirstInvest().getTime());
    		opp.setTimeLastInvestMillsec(opp.getTimeLastInvest().getTime());
    	}
    	return result;
    }

    /**
     * Get current opportunity id by market
     * @param request
     * @return
     */
    public static OpportunityOddsPairResult getAllReturnOdds(MethodRequest request, ApiUser apiUser, String jsonURL) {
    	String serviceName = "getAllReturnOdds";
    	log.debug("Going to call service:" + serviceName + ". " + "\n" + 
				"Details: \n " +
				"API user name: " + request.getApiUser().getUserName() + "\n" +    				
				"ip:" + request.getIp() + "\n" +
				"skinId: " + request.getSkinId());
    	OpportunityOddsPairResult result = new OpportunityOddsPairResult();    	
    	OpportunityOddsPairResult jsonResult = (OpportunityOddsPairResult)forwardServiceCall(serviceName, request, new OpportunityOddsPairResult(), jsonURL);
    	result = (OpportunityOddsPairResult)handleResponseFromJSON(serviceName, result, request, jsonResult); 
    	if (result.getApiCode() != ApiErrorCode.API_ERROR_CODE_GENERAL_SUCCESS) { 
    		return result;
    	}    	
    	return result;		    			
    } 

    /**
     * Validate user and password and set encrypted password for user in order to send later on to service
     * @param request
     * @param result
     */
    private static void validateUserAndPassword(UserMethodRequest request, UserMethodResult result, boolean validateExternalUser) {
    	log.info("getUser: userName=" + request.getUserName() + " pass=" + request.getPassword());
    	try {
    		boolean errorFound = false;
    		Locale locale = new Locale(request.getLocale());
    		User user = new User();
    		UsersManagerBase.getUserByName(request.getUserName(), user);
		    if (null == user || user.getId() == 0) {
		        log.info("User login failed.");
		        result.addUserMessage("", CommonUtil.getMessage(locale, "header.login.error", null), ApiErrorCode.API_ERROR_CODE_VALIDATION_ERROR);
		        errorFound = true;
		    }
    		if (!errorFound) { // user found
    				boolean successLogin = true;
        			ArrayList<String> keys = LoginManager.loginActionFromWeb(user, request.getPassword());
        			if (!keys.isEmpty()) {
        				successLogin = false;
        				for (String key : keys) {
        					result.addUserMessage("", CommonUtil.getMessage(locale, key, null), ApiErrorCode.API_ERROR_CODE_VALIDATION_ERROR);
        				}
        				errorFound = true;
        			}
//    				LoginsManager.insertLogin(user.getUserName(), CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset()), successLogin, null, null,
//    						ConstantsBase.LOGIN_FROM_REGULAR, Writer.WRITER_ID_API, ServerConfiguration.getInstance().getServerName(), 0L, "api", "api", "api", DeviceFamily.NONMOBILE);
    		}
    		if (errorFound) {
	    		result.setApiCode(ApiErrorCode.API_ERROR_CODE_VALIDATION_ERROR);
	    		result.setApiCodeDescription(ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_VALIDATION_ERROR).getDisplayName());
    		} else {
    	    		result.setApiCode(ApiErrorCode.API_ERROR_CODE_GENERAL_SUCCESS);
    	    		result.setApiCodeDescription(ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_GENERAL_SUCCESS).getDisplayName());
    	    		request.setPassword(user.getEncryptedPassword());
    	    		result.setUser(user);
    		}
    	} catch (Exception e) {
            log.error("Error in login user", e);
            result.setApiCode(ApiErrorCode.API_ERROR_CODE_VALIDATION_ERROR);
            result.setApiCodeDescription(ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_VALIDATION_ERROR).getDisplayName());
        }
    }

    /**
     * Simulate receiving client notifications
     * @param request
     * @return
     */
    public static ApproveNotificationMethodResponse simulateClientNotifications(ClientNotificationMethodRequest request) {
//    	String serviceName = "approveNotifications";
    	log.debug("Service call getClientNotifications");
    	ArrayList<ApiNotificationResponse> notificationResponseArray = new ArrayList<ApiNotificationResponse>();
    	for (ApiNotification notification : request.getNotifications()) {

    		ApiNotificationResponse nResponse = new ApiNotificationResponse();
    		nResponse.setId(notification.getId());
    		nResponse.setStatus(1 + (int)(Math.random() * ((2 - 1) + 1)));
    		notificationResponseArray.add(nResponse);
    	}
    	ApproveNotificationMethodResponse approveNotificationResponse = new ApproveNotificationMethodResponse();
    	approveNotificationResponse.setNotificationsResponse(notificationResponseArray);
    	//API user
		ApiUser au = new ApiUser();
		au.setUserName("anyoption_test");
		au.setPassword("q1w2e3");
//		approveNotificationResponse.setApiUser(au);
//		approveNotificationResponse.setLocale("en");
        return approveNotificationResponse;
//    	try {
//	        byte[] buffer = new byte[2048]; // 2K read/write buffer. This might need change for better performance
//	        URL u = new URL(Constants.API_GATEWAY_URL + serviceName);
//	        HttpURLConnection httpCon = (HttpURLConnection)u.openConnection();
//	        httpCon.setDoOutput(true);
//	        Gson gson = new GsonBuilder().setPrettyPrinting().create();
//	        httpCon.getOutputStream().write(gson.toJson(approveNotificationRequest).getBytes("UTF-8"));
//	        int readCount = -1;
//	        InputStream is = httpCon.getInputStream();
//	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//	        OutputStream os = baos;
//	        while ((readCount = is.read(buffer)) != -1) {
//	            log.trace("readCount: " + readCount);
//	            os.write(buffer, 0, readCount);
//	        }
//	        os.flush();
//	        os.close();
//            byte[] resp = baos.toByteArray();
//	    } catch (Exception e) {
//	    	log.error("ERROR! problem  ", e);
//		}
    }


    /**
     * Approve notifications
     * @param request
     * @return
     */
    public static MethodResult approveNotifications(ApproveNotificationMethodResponse request, ApiUser apiUser) {
//    	log.debug("Approve Notifications service \n" +
//				  "Details: \n " +
//				  "API user name: " + request.getApiUser().getUserName() + "\n" +
//				  "ip:" + request.getIp() + "\n" +
//				  "skinId: " + request.getSkinId() + "\n");
    	String statusApprove = ConstantsBase.EMPTY_STRING;
    	String statusFail	 = ConstantsBase.EMPTY_STRING;
    	String statusOther	 = ConstantsBase.EMPTY_STRING;
    	for (ApiNotificationResponse notification : request.getNotificationsResponse()) {
    		log.info("Notification id:" + notification.getId() + "\n" +
    				 "Notification status:" + notification.getStatus() + "\n"
    				);
    		if (notification.getId() != 0) {
	    		switch ((int)notification.getStatus()) {
					case (int)Notification.STATUS_SUCCEED:
						statusApprove += notification.getId()  + ",";
						break;
					case (int)Notification.STATUS_FAILED:
						statusFail += notification.getId()  + ",";
						break;
					default:
						statusOther += notification.getId()  + ",";
						break;
				}
    		}
    	}
    	try {
	    	// update status approve
	    	if (!CommonUtil.IsParameterEmptyOrNull(statusApprove)) {
	    		ApiManager.updateAPINotifications(apiUser.getId(), statusApprove.substring(0, statusApprove.length() - 1), Notification.COMMENT_SUCCESS_RECEIVED, Notification.STATUS_SUCCEED);
	    	}
	    	// update status fail
	    	if (!CommonUtil.IsParameterEmptyOrNull(statusFail)) {
	    		ApiManager.updateAPINotifications(apiUser.getId(), statusFail.substring(0, statusFail.length() - 1), Notification.COMMENT_FAILED_RECEIVED , Notification.STATUS_FAILED);
	    	}
	    	if (!statusOther.isEmpty()) {

	    	}
    	} catch (Exception e) {
    		log.error("error", e);
		}

    	MethodResult result = new MethodResult();
    	return result;
    }
    
    /**
     * Get last currencies rate
     * @param request
     * @param apiUser
     * @return
     */
    public static LastCurrenciesRateMethodResult getLastCurrenciesRate(MethodRequest request, ApiUser apiUser, String jsonURL) {
    	String serviceName = "getLastCurrenciesRate";
    	log.debug("Going to call service:" + serviceName + ". " + "\n" +
				"Details: \n " +
				"API user name: " + request.getApiUser().getUserName() + "\n" +
				"ip:" + request.getIp() + "\n" +
				"skinId: " + request.getSkinId() + "\n");
    	LastCurrenciesRateMethodResult result = new LastCurrenciesRateMethodResult(); 
    	LastCurrenciesRateMethodResult jsonResult = (LastCurrenciesRateMethodResult)forwardServiceCall(serviceName, request, new LastCurrenciesRateMethodResult(), jsonURL);
    	result = (LastCurrenciesRateMethodResult)handleResponseFromJSON(serviceName, result, request, jsonResult);  	
    	return result;    	
    }
    
    /**
     * Get API first time deposit users list by date  
     * @param request
     * @param apiUser
     * @return
     */
    public static FTDUsersMethodResult getFTDUsersByDate(FTDUsersRequest request, ApiUser apiUser, String jsonURL) {
    	String serviceName = "getFTDUsersByDate";
    	log.debug("Going to call service:" + serviceName + ". " + "\n" +
				"Details: \n " +
				"API user name: " + request.getApiUser().getUserName() + "\n" +
				"ip:" + request.getIp() + "\n" +
				"Date Request:" + request.getDateRequest() + "\n" +
				"skinId: " + request.getSkinId() + "\n");
    	FTDUsersMethodResult result = new FTDUsersMethodResult(); 
    	//fields validation
    	String res = validateFields(request, result, serviceName, "ftd_users", request, null, false);    	
    	if (res != null) { //error found
    		return result;
    	}   
    	request.setApiUserId(apiUser.getId());
    	FTDUsersMethodResult jsonResult = (FTDUsersMethodResult)forwardServiceCall(serviceName, request, new FTDUsersMethodResult(), jsonURL);
    	result = (FTDUsersMethodResult)handleResponseFromJSON(serviceName, result, request, jsonResult);  	
    	return result;    	
    }
    
    /**
     * Get marketing users
     * @param request
     * @param apiUser
     * @return result as AffiliateUsersMethodResult
     */
    public static MarketingUsersMethodResult getMarketingUsers(MarketingUsersMethodRequest request, ApiUser apiUser, String jsonURL) {
    	String serviceName = "getMarketingUsers";
    	log.debug("Going to call service:" + serviceName + ". " + "\n" +
				"Details: \n " +
				"API user name: " + request.getApiUser().getUserName() + "\n" +
				"ip:" + request.getIp() + "\n" +
				"skinId: " + request.getSkinId() + "\n" +
				"AffiliateKey: " + request.getAffiliateKey() + "\n" +
				"Date: " + request.getDateRequest());
    	MarketingUsersMethodResult result = new MarketingUsersMethodResult();
    	//validation
    	String res = validateFields(request, result, serviceName, "marketing_users", request, null, false);
    	if (res != null) { //error found
    		return result;
    	}
    	try {
    		if (!ApiManager.isAffKeyValidForApi(apiUser.getId(), request.getAffiliateKey())) {
    			log.info("Affiliate key is invalid for api");
                result.setApiCode(ApiErrorCode.API_ERROR_CODE_VALIDATION_INVALID_AFF_KEY_FOR_API);
                result.setApiCodeDescription(ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_VALIDATION_INVALID_AFF_KEY_FOR_API).getDisplayName());
                return result;
        	}
    	} catch (Exception e) {
            log.error("Error in get marketing users", e);
            result.setApiCode(ApiErrorCode.API_ERROR_CODE_VALIDATION_ERROR);
            result.setApiCodeDescription(ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_VALIDATION_ERROR).getDisplayName());
            return result;
        }
    	MarketingUsersMethodResult jsonResult = (MarketingUsersMethodResult)forwardServiceCall(serviceName, request, new MarketingUsersMethodResult(), jsonURL);
    	result = (MarketingUsersMethodResult)handleResponseFromJSON(serviceName, result, request, jsonResult);
    	return result;
    }
    
    /**
     * Insert Copyop user
     * @param request
     * @param apiUser
     * @return UserMethodResult
     */
    public static UserMethodResult insertCopyopProfile(com.copyop.json.requests.InsertUserMethodRequest request, ApiUser apiUser, String jsonURL) {
    	String serviceName = "insertCopyopProfile";
    	log.debug("Going to call service:" + serviceName + ".\n" +
    			"Details:\n" +
				"API user name: " + request.getApiUser().getUserName() + "\n" +
				request.getRegister().toString());
    	UserMethodResult result = new UserMethodResult();
    	//fields validation
    	String res = validateFields(request, result, serviceName, "insert_user", request, request.getRegister().getCountryName(), true);
    	if (res != null) { //error found
    		return result;
    	}
    	res = validateFields(request, result, serviceName, "insert_copyop_profile", request, request.getRegister().getCountryName(), true);
    	if (res != null) { //error found
    		return result;
    	}
		//set additional values
    	long skinId = Skin.SKIN_REG_EN;
    	long reqSkinId = request.getSkinId();
    	if (reqSkinId == Skin.SKIN_REG_ES || reqSkinId == Skin.SKIN_REG_DE || reqSkinId == Skin.SKIN_REG_IT || reqSkinId == Skin.SKIN_REG_FR) {
    		skinId = reqSkinId;
    	}
    	request.setSkinId(skinId);
    	long combinationId = SkinsManagerBase.getSkin(request.getSkinId()).getDefaultCombinationId();
    	if (request.getRegister().getCombinationId() != null && request.getRegister().getCombinationId() != new Long(0)) {
    		combinationId = request.getRegister().getCombinationId();
    	}
    	//set Netrefer combination and DP to affiliate
    	if (apiUser.getType() == ApiUser.API_USER_TYPE_AFFILIATE) {
    		ArrayList<Long> netreferCombinationsIdList = SkinsManagerBase.getNetreferCombinationsIdList(request.getSkinId());
    		if (!netreferCombinationsIdList.isEmpty()) {
        		combinationId = netreferCombinationsIdList.get(0);
        		log.debug("Setting default combination id to affiliate " + combinationId);
    		}
    		String dynamicParameter = request.getRegister().getDynamicParam();
    		request.getRegister().setDynamicParam("API_" + dynamicParameter);
    	}
    	request.getRegister().setCombinationId(combinationId);
		request.getRegister().setContactByEmail(true);
		request.getRegister().setContactBySms(true);
		long counrtyId = CountryManagerBase.getCountryByA3Code().get(request.getRegister().getCountryName().toUpperCase()).getId();
		request.getRegister().setCountryId(counrtyId);
		// new register funnel copy email to userName
		request.getRegister().setUserName(request.getRegister().getEmail());
		request.getRegister().setPlatformId(Platform.COPYOP);
		request.getRegister().setTerms(request.isAcceptedTermsAndConditions());
		String rand = (int)Math.floor((Math.random() * ConstantsBase.NUM_OF_AVATARS) + 1) + ".png";
		String avatar = CommonUtil.getProperty("copyop.home.page") + "avatar/";
		request.setAvatar(avatar + request.getGender() + rand);
		
		com.copyop.json.results.UserMethodResult jsonResult = (com.copyop.json.results.UserMethodResult)forwardServiceCall(serviceName, request, new com.copyop.json.results.UserMethodResult(), jsonURL);
    	result = (UserMethodResult)handleResponseFromJSON(serviceName, result, request, jsonResult);
    	if (result.getApiCode() != ApiErrorCode.API_ERROR_CODE_GENERAL_SUCCESS) {
    		return result;
    	}
    	//success from JSON
    	long userId = result.getUser().getId();
    	log.debug("User was created/updated successfully id:" + userId);
    	if (userId != 0) {
	    	ApiUserActivity apiUserActivity = new ApiUserActivity();
	    	apiUserActivity.setApiUserId(apiUser.getId());
	    	apiUserActivity.setTypeId(ApiUserActivityType.API_USER_ACTIVITY_TYPE_INSERT_USER);
	    	apiUserActivity.setKeyValue(userId);
	    	try {
				ApiManager.insertAPIUserActivity(apiUserActivity);
			} catch (SQLException e) {
				log.error("Error while trying to insert API user activity, userId: " + userId, e);
			}
    	}
    	return result;
    }
    
    /**
     * Insert user
     * @param request
     * @param apiUser
     * @return
     */
    public static UserMethodResult insertInitialUser(InsertUserMethodRequest request, ApiUser apiUser, String jsonURL) {
    	String pass = RandomStringUtils.randomAlphanumeric(9);
    	request.getRegister().setPassword(pass);
    	request.getRegister().setPassword2(pass);
    	return insertUser(request, apiUser, jsonURL);
    }

    /**
     * Validate fields from validation.xml file and add proper messages
     * @param request
     * @param result
     * @param serviceName
     * @param formName
     * @param formObj
     * @param countryName
     * @param checkCountry
     * @return
     */
    private static String validateFields(MethodRequest request, MethodResult result, String serviceName, String formName, Object formObj,
    		String countryName, boolean checkCountry) {
    	Locale locale = new Locale(request.getLocale());
    	String apiErrorCode = null;
    	String fieldError = null;
    	String fieldErrorCode = null;
    	ArrayList<ErrorMessage> msgs = null;
    	Country country = null;
    	try {
			msgs = validateInput(formName, formObj, locale);
	        if (!msgs.isEmpty()) {
	        	log.debug("Validation error before calling service call." + serviceName + "\n" + msgs.toString());
	        	apiErrorCode = ApiErrorCode.API_ERROR_CODE_VALIDATION_ERROR;
	        }
		} catch (ValidatorException e1) {
        	log.debug("Validation error before calling " + serviceName  + " service call", e1);
        	apiErrorCode = ApiErrorCode.API_ERROR_CODE_GENERAL_CANT_PARSE_JSON;
		}
    	if (apiErrorCode == null && checkCountry) {
        	//country validation
    		country = CountryManagerBase.getCountryByA3Code().get(countryName.toUpperCase());
    		if (country == null) {
            	log.debug("Validation error before calling " + serviceName  + " service call. country not found: " + countryName);
            	apiErrorCode = ApiErrorCode.API_ERROR_CODE_VALIDATION_ERROR;
            	fieldError = "countryName";
            	fieldErrorCode = ApiErrorCode.API_ERROR_CODE_VALIDATION_COUNTRY_NOT_EXISTS;
    		} else if (country.getId() == com.anyoption.util.ConstantsBase.COUNTRY_ID_US
    				   || country.getId() == com.anyoption.util.ConstantsBase.COUNTRY_ID_IL) {
            	log.debug("Validation error before calling " + serviceName  + " service call. country not supported: " + countryName);
            	result.setApiCode(ApiErrorCode.API_ERROR_CODE_VALIDATION_ERROR);
            	result.setApiCodeDescription(ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_VALIDATION_ERROR).getDisplayName());
            	ErrorMessage em = new ErrorMessage("countryName", CommonUtil.getMessage(locale, "error.register.israel1", null) , ApiErrorCode.API_ERROR_CODE_VALIDATION_FORBIDDEN_COUNTRY);
            	ArrayList<ErrorMessage> listEm = new ArrayList<ErrorMessage>();
            	listEm.add(em);
            	result.setUserMessages(listEm);
            	return ApiErrorCode.API_ERROR_CODE_VALIDATION_ERROR;
    		}
    	}
    	//error found
    	if (apiErrorCode != null) {
        	result.setApiCode(apiErrorCode);
        	result.setApiCodeDescription(ApiGatewayServlet.getErrorCodes().get(apiErrorCode).getDisplayName());
    		if (msgs != null && !msgs.isEmpty()) {
    			result.addUserMessages(msgs);
    		}
    		if (fieldError != null) {
            	ErrorMessage em = new ErrorMessage(fieldError, ApiGatewayServlet.getErrorCodes().get(fieldErrorCode).getDisplayName(), fieldErrorCode);
            	ArrayList<ErrorMessage> listEm = new ArrayList<ErrorMessage>();
            	listEm.add(em);
            	result.setUserMessages(listEm);
    		}
    	}
    	return apiErrorCode;
    }

    /**
     * Validate input method
     * @param formName
     * @param bean
     * @param locale
     * @return
     * @throws ValidatorException
     */
    private static ArrayList<ErrorMessage> validateInput(String formName, Object bean, Locale locale) throws ValidatorException {
        ArrayList<ErrorMessage> msgs = new ArrayList<ErrorMessage>();
        Validator validator = new Validator(ApiGatewayServlet.getValidatorResources(), formName);
        validator.setParameter(Validator.BEAN_PARAM, bean);
        validator.setParameter(ValidatorFieldChecks.MSGS_PARAM, msgs);
        validator.setParameter(Validator.LOCALE_PARAM, locale);
        validator.setClassLoader(ApiGatewayServlet.class.getClassLoader());
        validator.validate();
        return msgs;
    }

    /**
     * Validate API user.
     * 1. Check if user name and password is valid.
     * 2. check if user is active
     * @param request
     * @return
     */
    public static ApiErrorCode validateApiUser(MethodRequest request, ApiUser apiUser, Hashtable<Long, String> roles) {
    	ApiErrorCode apiErrorCode = ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_GENERAL_SUCCESS);
    	try {
    		String apiUserName = request.getApiUser().getUserName();
    		String password = request.getApiUser().getPassword();
    		log.debug("API User:" + apiUserName + " password:" + password + " is trying to make request");
    		ApiManager.getAPIUser(apiUserName, password, apiUser);
    		if (apiUser.getId() == 0) {
    			log.debug("API User " + apiUserName + " wrong user name or password");
    			apiErrorCode = ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_AUTHENTICATION_INCORRECT_USERNAME_OR_PASSWORD);
    			return apiErrorCode;
    		}
    		if (apiUser.getId() != 0 && !apiUser.isActive()) {
    			log.warn("API User " + apiUserName + " is not active");
    			apiErrorCode = ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_AUTHENTICATION_USER_NOT_ACTIVE);
    			return apiErrorCode;
    		}
    		if (null == roles || !roles.containsKey(apiUser.getRoleId())) {
    			log.warn("API User " + apiUserName + " is not has permission to the service.");
    			apiErrorCode = ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_AUTHENTICATION_PERMISSION_DENIED);
    			return apiErrorCode;
    		}
    		if (apiErrorCode.getCode().equals(ApiErrorCode.API_ERROR_CODE_GENERAL_SUCCESS)) {
    			log.debug("API User loaded successfully" + apiUser.toString());
    		}
    	} catch (Exception e) {
    		log.error("ERROR! User name or password were missing ", e);
    		apiErrorCode = ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_AUTHENTICATION_USER_OR_PASSWORD_MISSING);
		}
    	return apiErrorCode;
    }

    /**
     * Handle response from JSON and add proper code in case of success/fail cases
     * @param serviceName
     * @param result
     * @param request
     * @param jsonResult
     * @return
     */
    private static MethodResult handleResponseFromJSON(String serviceName, MethodResult result, MethodRequest request, MethodResult jsonResult) {
    	if (jsonResult == null) {
    		result.setApiCode(ApiErrorCode.API_ERROR_CODE_GENERAL_CANT_CONNECT_TO_SERVER);
    		result.setApiCodeDescription(ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_GENERAL_CANT_CONNECT_TO_SERVER).getDisplayName());
    		return result;
    	}
    	result = jsonResult;
    	//check if we receive error from the server
    	if (result.getErrorCode() != ConstantsBase.JSON_ERROR_CODE_SUCCESS) {
    		log.debug("Validation error from JSON calling " + serviceName + " service call \n" + result.getUserMessages() + " " + result.getUserMessages()[0]);
    		for (ErrorMessage em : result.getUserMessages()) {
    			String apiErrorCode = ApiErrorCode.API_ERROR_CODE_VALIDATION_ERROR;
    			if (em.getApiErrorCode() != null) {
    				apiErrorCode = em.getApiErrorCode();
    			}
    			em.setApiErrorCode(apiErrorCode);
    		}
    		result.setApiCode(ApiErrorCode.API_ERROR_CODE_VALIDATION_ERROR);
    		result.setApiCodeDescription(ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_VALIDATION_ERROR).getDisplayName());
    		result.setUserMessages(new ArrayList<ErrorMessage>(Arrays.asList(result.getUserMessages())));
    		return result;
    	}
    	//success from JSON
    	result.setApiCode(ApiErrorCode.API_ERROR_CODE_GENERAL_SUCCESS);
    	result.setApiCodeDescription(ApiGatewayServlet.getErrorCodes().get(ApiErrorCode.API_ERROR_CODE_GENERAL_SUCCESS).getDisplayName());
    	return result;
    }

    /**
     * Forward Service Call to anyoptionService
     * @param serviceName
     * @param request
     * @param response
     * @return
     */
    private static Object forwardServiceCall(String serviceName, Object request, Object response, String jsonURL) {
    	try {
    		((MethodRequest)request).setWriterId(Writer.WRITER_ID_API);
	        byte[] buffer = new byte[2048]; // 2K read/write buffer. This might need change for better performance
	        URL u = new URL(jsonURL + serviceName);
	        HttpURLConnection httpCon = (HttpURLConnection)u.openConnection();
	        httpCon.setDoOutput(true);
	        Gson gson = new GsonBuilder().setPrettyPrinting().create();
	        httpCon.getOutputStream().write(gson.toJson(request).getBytes("UTF-8"));
	        int readCount = -1;
	        InputStream is = httpCon.getInputStream();
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        OutputStream os = baos;
	        while ((readCount = is.read(buffer)) != -1) {
	            log.trace("readCount: " + readCount);
	            os.write(buffer, 0, readCount);
	        }
	        os.flush();
	        os.close();
            byte[] resp = baos.toByteArray();
            String json = new String(resp, "UTF-8");
            log.trace("json: " + json);
            return gson.fromJson(json, response.getClass());
	    } catch (Exception e) {
	    	log.error("ERROR! problem forword service call to JSON ", e);
		}
    	return null;
    }

}
