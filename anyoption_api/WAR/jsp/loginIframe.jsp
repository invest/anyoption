<%@page import="com.anyoption.web.api.service.util.Constants"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<!-- START HEAD -->
	<head>
		<meta HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=UTF-8" />
	   	<title>Binary Options | anyoption</title>
	    <meta name="Title" content="Binary Options | anyoption" />
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>	    	           		
		<link rel="stylesheet" type="text/css" href="../css/style.css" />
    	<link rel="shortcut icon" href="../images/favicon.ico" type="image/vnd.microsoft.icon" />		
	</head>
	<!-- END HEAD -->
	<body id="EN">
		<div id="bg_container">
        	<div id="page">
		    	<div id="logo">
		        	<a href="index.jsp"><img src="../images/logo.png" alt="anyoption" /></a>
		        </div>
		        <div id="main">
		            <div id="title">
		                <h1>
		                	<p id='dynamicHeader'> API login Iframe service </p>
		                </h1>
		            </div>
                    <div style="clear: both;">                    	
                    </div>
                    <div class="form_block">
                        <!-- START FORM -->                                               
                        <form id="form" action='<%= Constants.ANYOPTION_HP_URL %>jsp/APILogin.jsf' method="POST">
                         	<div class="form_cl" ><span class="star">*</span> Login token:                            
                            	<textarea id="login_token" name="login_token" style="resize:none;" spellcheck="false" cols="" rows="5" class="textarea1" dir="ltr"></textarea>                               
                            </div>                        
                            <div class="form_cl2" > userId:
                            <div class="input1">
                                <input id="user_id" type="text" name="user_id" value="" />
                            </div></div>
                            <div class="form_cl" ><img onclick="submit_form();" src="../images/submit_button_d.png" onmouseover="this.src='../images/submit_button_u.png'" onmouseout="this.src='../images/submit_button_d.png'" />
                            </div>                                                                                                                                                                                                                                                                                    	                           
                        </form>
                        <!-- END FORM -->
                    </div>
                    <div style="clear: both;">                    	
                    </div>
		        </div>
		    </div>
		</div>
		<script id="afterLoad" src="../js/js_after_load.js" ></script>
		<script>
			addURL('<%= Constants.API_GATEWAY_URL %>getLoginToken');
			function submit_form() {
				var  form = document.getElementById("form");
				form.submit();
			}
		</script>		
	</body>
</html>