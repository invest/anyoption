<%@page import="com.anyoption.web.api.service.util.Constants"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<!-- START HEAD -->
	<head>
		<meta HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=UTF-8" />
	   	<title>Binary Options | anyoption</title>
	    <meta name="Title" content="Binary Options | anyoption" />
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>	    	           		
		<link rel="stylesheet" type="text/css" href="../css/style.css" />
    	<link rel="shortcut icon" href="../images/favicon.ico" type="image/vnd.microsoft.icon" />		
	</head>
	<!-- END HEAD -->
	<body id="EN">
		<div id="bg_container">
        	<div id="page">
		    	<div id="logo">
		        	<a href="index.jsp"><img src="../images/logo.png" alt="anyoption" /></a>
		        </div>
		        <div id="main">
		            <div id="title">
		                <h1>
		                	<p id='dynamicHeader'> API lead service </p>
		                </h1>
		            </div>
                    <div style="clear: both;">                    	
                    </div>
                    <div class="form_block">
                        <!-- START FORM -->                       
                        <form id="form" action='<%= Constants.API_GATEWAY_EXAMPLE_URL %>request' method="POST">                         	
                         	<div class="form_cl" ><span class="star">*</span> Service URL:                            
                            	<textarea id="serviceUrl" name="serviceUrl" style="resize:none;" spellcheck="false" cols="" rows="5" class="textarea1" dir="ltr"></textarea>                               
                            </div>
                         	<div class="form_cl1" ><span class="star">*</span> API User name:
                            <div class="input1">
                                <input id="apiUserName" type="text" name="apiUserName" value="" />
                            </div></div>                         
                            <div class="form_cl2" ><span class="star">*</span> API Password:
                            <div class="input1">
                                <input id="apiPassword" type="text" name="apiPassword" value="" />
                            </div></div>
                            <div class="form_cl2" ><span class="star">*</span> Locale:
                            <div class="input1">
                                <input id="locale" type="text" name="locale" value="" />
                            </div></div>
                         	<div class="form_cl1" ><span class="star">*</span> Name:
                            <div class="input1">
                                <input id="name" type="text" name="name" value="" />
                            </div></div>
                         	<div class="form_cl2" ><span class="star">*</span> Email:
                            <div class="input1">
                                 <input id="email" type="text" name="email" value="" /><br />
                            </div></div>
                         	<div class="form_cl2" ><span class="star">*</span> Phone:
                            <div class="input1">
                                 <input id="phone" type="text" name="phone" value="" /><br />
                            </div></div>
                         	<div class="form_cl1" ><span class="star">*</span> Country:
                            <div class="input1">
                            	<div class="noborder">
	                            	<select id="countries" name="countries">
	                            		<!-- js -->
	                            	 </select>
                            	</div>                               	
                            </div></div>
                         	<div class="form_cl2" ><span class="star">*</span> IP:
                            <div class="input1">
                                <input id="ip" type="text" name="ip" value="" />
                            </div></div>                            
                         	<div class="form_cl2"> Combination Id:
                            <div class="input1">
                               <input id="combId" type="text" name="combId" value="" /><br />
                            </div></div>
                         	<div class="form_cl1"><span class="star">*</span> Time zone offset:
                            <div class="input1">
                               <input id="utcOffset" type="text" name="utcOffset" value="" /><br />
                            </div></div>                              
                         	<div class="form_cl" > Dynamic param:                            
                            	<textarea id="dynamicParameter" style="resize:none;" spellcheck="false" cols="" rows="5" class="textarea1" dir="ltr"></textarea>                               
                            </div>
                         	<div class="form_cl" > Http Referer:                            
                            	<textarea id="httpReferer" style="resize:none;" spellcheck="false" cols="" rows="5" class="textarea1" dir="ltr"></textarea>                               
                            </div>                            
                         	<div class="form_cl" > User Agent:                           
                            	<textarea id="userAgent" style="resize:none;" spellcheck="false" cols="" rows="5" class="textarea1" dir="ltr"></textarea>                               
                            </div>                                                        
                            <div style="clear: both;"></div>                      
                            <div class="form_btn" ><img onclick="submit_form();" src="../images/submit_button_u.png" onmouseover="this.src='../images/submit_button_d.png'" onmouseout="this.src='../images/submit_button_u.png'" /></div>
                            <div class="form_btn" ><img onclick="createJSON();" src="../images/GenerateJSON_button.png" onmouseover="this.src='../images/GenerateJSON_button_hover.png'" onmouseout="this.src='../images/GenerateJSON_button.png'" /></div>                                                                                                                                                                                                                                                                                                                	                           
                        </form>
                        <!-- END FORM -->
                    </div>
                    <div style="clear: both;">                    	
                    </div>
		        </div>
		    </div>
		</div>
		<pre id="json" style="display:none;"></pre>
		<script id="afterLoad" src="../js/js_after_load.js" ></script>
		<script>
			addURL('<%= Constants.API_GATEWAY_URL %>insertCallMeContact');
			function submit_form() {
				var  form = document.getElementById("form");
				form.submit();
			}
			function createJSON() {
				var apiUser = new Object();
				var request 				= new Object();							
				apiUser.userName 			= document.getElementById("apiUserName").value;
				apiUser.password 			= document.getElementById("apiPassword").value;				
				request.apiUser 			= apiUser;
				
				request.locale 				= document.getElementById("locale").value;  
				var contact					= new Object();								
				contact.name				= document.getElementById("name").value;				
				contact.email				= document.getElementById("email").value;
				contact.phone				= document.getElementById("phone").value;
				contact.countryName			= document.getElementById("countries").value;
				contact.ip					= document.getElementById("ip").value;				
				contact.combId				= document.getElementById("combId").value;
				contact.utcOffset			= document.getElementById("utcOffset").value;
				contact.dynamicParameter	= document.getElementById("dynamicParameter").value;
				contact.httpReferer			= document.getElementById("httpReferer").value;
				contact.userAgent			= document.getElementById("userAgent").value;
				request.contact				= contact;
															
				if ((request.phone + "").match(/^\d+$/) ) { //check if  number
 					request.phone	= Number(request.phone);
				}		
				if ((request.combId + "").match(/^\d+$/) ) { //check if  number
 					request.combId	= Number(request.combId);
				}																								 												 
				var str = JSON.stringify(request, undefined, 4);				
				document.getElementById("json").innerHTML = syntaxHighlight(str);
				document.getElementById("json").style.display = 'block';
			}							
		</script>		
	</body>
</html>