<%@page import="com.anyoption.web.api.service.util.Constants"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<!-- START HEAD -->
	<head>
		<meta HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=UTF-8" />
	   	<title>Binary Options | anyoption</title>
	    <meta name="Title" content="Binary Options | anyoption" />
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>	    	           		
		<link rel="stylesheet" type="text/css" href="../css/style.css" />
    	<link rel="shortcut icon" href="../images/favicon.ico" type="image/vnd.microsoft.icon" />		
	</head>
	<!-- END HEAD -->
	<body id="EN">
		<div id="bg_container">
        	<div id="page">
		    	<div id="logo">
		        	<a href="index.jsp"><img src="../images/logo.png" alt="anyoption" /></a>
		        </div>
		        <div id="main">
		            <div id="title">
		                <h1>
		                	<p id='dynamicHeader'>API get all binary markets service</p>
		                </h1>
		            </div>
                    <div style="clear: both;">                    	
                    </div>
                    <div class="form_block">
                        <!-- START FORM -->                       
                        <form id="form" action='<%= Constants.API_GATEWAY_EXAMPLE_URL %>request' method="POST">                         	
                         	<div class="form_cl" ><span class="star">*</span> Service URL:                            
                            	<textarea id="serviceUrl" name="serviceUrl" style="resize:none;" spellcheck="false" cols="" rows="5" class="textarea1" dir="ltr"></textarea>                               
                            </div>
                         	<div class="form_cl1" ><span class="star">*</span> API User name:
                            <div class="input1">
                                <input id="apiUserName" type="text" name="apiUserName" value="" />
                            </div></div>                         
                            <div class="form_cl2" ><span class="star">*</span> API Password:
                            <div class="input1">
                                <input id="apiPassword" type="text" name="apiPassword" value="" />
                            </div></div>
                            <div class="form_cl2" ><span class="star">*</span> Locale:
                            <div class="input1">
                                <input id="locale" type="text" name="locale" value="" />
                            </div></div>
                            <div style="clear: both;"></div>                      
                            <div class="form_btn" ><img onclick="submit_form();" src="../images/submit_button_u.png" onmouseover="this.src='../images/submit_button_d.png'" onmouseout="this.src='../images/submit_button_u.png'" /></div>
                            <div class="form_btn" ><img onclick="createJSON();" src="../images/GenerateJSON_button.png" onmouseover="this.src='../images/GenerateJSON_button_hover.png'" onmouseout="this.src='../images/GenerateJSON_button.png'" /></div>                                                                                                                                                                                                                                                                                                                	                                                                                                                                                                                                                                                                                                               	                           
                        </form>
                        <!-- END FORM -->
                    </div>
                    <div style="clear: both;">                    	
                    </div>
		        </div>
		    </div>
		</div>
		<pre id="json" style="display:none;"></pre>
		<script id="afterLoad" src="../js/js_after_load.js" ></script>
		<script>
			addURL('<%= Constants.API_GATEWAY_URL %>getAllBinaryMarkets');
			function submit_form() {
				var  form = document.getElementById("form");
				form.submit();
			}
			function createJSON() {
				var apiUser = new Object();							
				apiUser.userName 		= document.getElementById("apiUserName").value;
				apiUser.password 		= document.getElementById("apiPassword").value;
				var request 			= new Object();  
				request.apiUser 		= apiUser;
				request.locale 			= document.getElementById("locale").value;														 												 
				var str = JSON.stringify(request, undefined, 4);				
				document.getElementById("json").innerHTML = syntaxHighlight(str);
				document.getElementById("json").style.display = 'block';
			}			
		</script>		
	</body>
</html>