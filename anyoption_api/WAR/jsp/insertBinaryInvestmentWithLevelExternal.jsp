<%@page import="com.anyoption.web.api.service.util.Constants"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<!-- START HEAD -->
	<head>
		<meta HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=UTF-8" />
	   	<title>Binary Options | anyoption</title>
	    <meta name="Title" content="Binary Options | anyoption" />
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>	    	           		
		<link rel="stylesheet" type="text/css" href="../css/style.css" />
    	<link rel="shortcut icon" href="../images/favicon.ico" type="image/vnd.microsoft.icon" />		
	</head>
	<!-- END HEAD -->
	<body id="EN">
		<div id="bg_container">
        	<div id="page">
		    	<div id="logo">
		        	<a href="index.jsp"><img src="../images/logo.png" alt="anyoption" /></a>
		        </div>
		        <div id="main">
		            <div id="title">
		                <h1>
		                	<p id='dynamicHeader'> API insert binary investment with level and external user service </p>
		                </h1>
		            </div>
                    <div style="clear: both;">                    	
                    </div>
                    <div class="form_block">
                        <!-- START FORM -->                       
                        <form id="form" action='<%= Constants.API_GATEWAY_EXAMPLE_URL %>request' method="POST">                         	
                         	<div class="form_cl" ><span class="star">*</span> Service URL:                            
                            	<textarea id="serviceUrl" name="serviceUrl" style="resize:none;" spellcheck="false" cols="" rows="5" class="textarea1" dir="ltr"></textarea>                               
                            </div>
                         	<div class="form_cl1" ><span class="star">*</span> API User name:
                            <div class="input1">
                                <input id="apiUserName" type="text" name="apiUserName" value="" />
                            </div></div>                         
                            <div class="form_cl2" ><span class="star">*</span> API Password:
                            <div class="input1">
                                <input id="apiPassword" type="text" name="apiPassword" value="" />
                            </div></div>
                            <div class="form_cl2" ><span class="star">*</span> Locale:
                            <div class="input1">
                                <input id="locale" type="text" name="locale" value="" />
                            </div></div>
                         	<div class="form_cl1" ><span class="star">*</span> User Name:
                            <div class="input1">
                                <input id="userName" type="text" name="userName" value="" />
                            </div></div>
                         	<div class="form_cl2" ><span class="star">*</span> Password:
                            <div class="input1">
                                 <input id="password" type="text" name="password" value="" />
                            </div></div>
                            <div class="form_cl2" ><span class="star">*</span> Opportunity Id:
                            <div class="input1">
                                 <input id="opportunityId" type="text" name="opportunityId" value="" />
                            </div></div>
                         	<div class="form_cl1" ><span class="star">*</span> IP:
                            <div class="input1">
                                <input id="ipAddress" type="text" name="ipAddress" value="" />
                            </div></div>
                         	<div class="form_cl2" ><span class="star">*</span> Amount:
                            <div class="input1">
                                 <input id="requestAmount" type="text" name="requestAmount" value="" />
                            </div></div>
                         	<div class="form_cl2"><span class="star">*</span> Time zone offset:
                            <div class="input1">
                               <input id="utcOffset" type="text" name="utcOffset" value="" /><br />
                            </div></div> 
                         	<div class="form_cl1" ><span class="star">*</span> Odds win:
                            <div class="input1">
                                <input id="pageOddsWin" type="text" name="pageOddsWin" value="" />
                            </div></div>
                         	<div class="form_cl2" ><span class="star">*</span> Odds lose:
                            <div class="input1">
                                 <input id="pageOddsLose" type="text" name="pageOddsLose" value="" />
                            </div></div>
                         	<div class="form_cl2" ><span class="star">*</span> Choice:(1/2)
                            <div class="input1">
                                 <input id="choice" type="text" name="choice" value="" />
                            </div></div>  
                         	<div class="form_cl1" ><span class="star">*</span> Page Level:
                            <div class="input1">
                                <input id="pageLevel" type="text" name="pageLevel" value="" />
                            </div></div>
							<div class="form_cl2" ><span class="star">*</span> External user:
                            <div class="input1">
                                 <input id="externalUser" type="text" name="externalUser" value="" />
                            </div></div>                                                                                                                                                                                                      
                            <div style="clear: both;"></div>                      
                            <div class="form_btn" ><img onclick="submit_form();" src="../images/submit_button_u.png" onmouseover="this.src='../images/submit_button_d.png'" onmouseout="this.src='../images/submit_button_u.png'" /></div>
                            <div class="form_btn" ><img onclick="createJSON();" src="../images/GenerateJSON_button.png" onmouseover="this.src='../images/GenerateJSON_button_hover.png'" onmouseout="this.src='../images/GenerateJSON_button.png'" /></div>                                                                                                                                                                                                                                                                                                                	                           
                        </form>
                        <!-- END FORM -->
                    </div>
                    <div style="clear: both;">                    	
                    </div>
		        </div>
		    </div>
		</div>
		<pre id="json" style="display:none;"></pre>
		<script id="afterLoad" src="../js/js_after_load.js" ></script>
		<script>
			addURL('<%= Constants.API_GATEWAY_URL %>insertBinaryInvestmentWithLevelExternal');
			function submit_form() {
				var  form = document.getElementById("form");
				form.submit();
			}
			function createJSON() {
				var apiUser = new Object();							
				apiUser.userName 		= document.getElementById("apiUserName").value;
				apiUser.password 		= document.getElementById("apiPassword").value;
				var request 			= new Object();  
				request.apiUser 		= apiUser;
				request.locale 			= document.getElementById("locale").value;
				request.userName		= document.getElementById("userName").value;				
				request.password		= document.getElementById("password").value;
				request.opportunityId	= document.getElementById("opportunityId").value;
				request.requestAmount	= document.getElementById("requestAmount").value;
				request.pageOddsWin		= document.getElementById("pageOddsWin").value;
				request.pageOddsLose	= document.getElementById("pageOddsLose").value;
				request.choice			= document.getElementById("choice").value;
				request.ipAddress		= document.getElementById("ipAddress").value;
				request.utcOffset		= document.getElementById("utcOffset").value;
				request.pageLevel		= document.getElementById("pageLevel").value;
				request.externalUser	= document.getElementById("externalUser").value;								
				
				if ((request.opportunityId + "").match(/^\d+$/) ) { //check if  number
 					request.opportunityId	= Number(request.opportunityId);
				}
				if ((request.requestAmount + "").match(/^[0-9.]+$/) ) {	//check if  number			
 					request.requestAmount	=  Number(request.requestAmount);
 				}				
				if ((request.pageOddsWin + "").match(/^[0-9.]+$/) ) { //check if  number
 					request.pageOddsWin	= Number(request.pageOddsWin);
				}
				if ((request.pageOddsLose + "").match(/^[0-9.]+$/) ) { //check if  number
 					request.pageOddsLose	= Number(request.pageOddsLose);
				}
				if ((request.choice + "").match(/^\d+$/) ) { //check if  number
 					request.choice	= Number(request.choice);
				}																 												 
				var str = JSON.stringify(request, undefined, 4);				
				document.getElementById("json").innerHTML = syntaxHighlight(str);
				document.getElementById("json").style.display = 'block';
			}			
		</script>		
	</body>
</html> 