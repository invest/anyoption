<%@page import="com.anyoption.web.api.service.util.Constants"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<!-- START HEAD -->
	<head>
		<meta HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=UTF-8" />
	   	<title>Binary Options | anyoption</title>
	    <meta name="Title" content="Binary Options | anyoption" />
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>	    	           		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css" />
    	<link rel="shortcut icon" href="${pageContext.request.contextPath}/images/favicon.ico" type="image/vnd.microsoft.icon" />		
	</head>
	<!-- END HEAD -->
	<body id="EN">
		
		<div id="bg_container" >
        	<div id="page">
		    	<div id="logo">
		        	<a href="${pageContext.request.contextPath}/jsp/login.jsp"><img src="${pageContext.request.contextPath}/images/logo.png" alt="anyoption" /></a>
		        </div>
		        <div id="main">
		            <div id="title">
		                <h1>
		                	<p id='dynamicHeader'>API anyoption Services</p>
		                </h1>
		            </div>
	                <ul class="bottom_list">
                        <!-- START FORM -->                       
                        <form id="form" action='<%= Constants.API_GATEWAY_EXAMPLE_URL %>request' method="POST">
                        	<textarea id="serviceUrl" name="serviceUrl" style="resize:none;display:none;" spellcheck="false" cols="" rows="5" class="textarea1" dir="ltr"></textarea>                         	
                         	<div class="form_cl1" ><span class="star">*</span> API User name:
                            <div class="input1">
                                <input id="apiUserName" type="text" name="apiUserName" value="" />
                            </div></div>                         
                            <div class="form_cl2" ><span class="star">*</span> API Password:
                            <div class="input1">
                                <input id="apiPassword" type="text" name="apiPassword" value="" />
                            </div></div>
                            <div class="form_cl2" ><span class="star">*</span> Locale:
                            <div class="input1">
                                <input id="locale" type="text" name="locale" value="" />
                            </div></div>
                            <div style="clear: both;"></div>                      
                            <div class="form_btn" ><img onclick="submit_form();" src="${pageContext.request.contextPath}/images/submit_button_u.png" onmouseover="this.src='${pageContext.request.contextPath}/images/submit_button_d.png'" onmouseout="this.src='${pageContext.request.contextPath}/images/submit_button_u.png'" /></div>
                            <div style="color: #FF0000;">${errorMessage}</div>                                                                                                                                                                                                                                                                                                                	                                                                                                                                                                                                                                                                                                               	                           
                        </form>
                        <!-- END FORM -->	                	
	                </ul>
				</div>
			</div>
		</div>
		<script id="afterLoad" src="${pageContext.request.contextPath}/js/js_after_load.js" ></script>
		<script>	
			addURL('<%= Constants.API_GATEWAY_URL %>login');
			function submit_form() {
				var  form = document.getElementById("form");
				form.submit();
			}
		</script>			
																	
	</BODY>
</HTML>