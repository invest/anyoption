<%@page import="com.anyoption.web.api.service.util.Constants"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<!-- START HEAD -->
	<head>
		<meta HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=UTF-8" />
	   	<title>Binary Options | anyoption</title>
	    <meta name="Title" content="Binary Options | anyoption" />
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>	    	           		
		<link rel="stylesheet" type="text/css" href="../../css/style.css" />
    	<link rel="shortcut icon" href="../../images/favicon.ico" type="image/vnd.microsoft.icon" />		
	</head>
	<!-- END HEAD -->
	<body id="EN">
		<div id="bg_container">
        	<div id="page">
		    	<div id="logo">
		        	<a href="../index.jsp"><img src="../../images/logo.png" alt="anyoption" /></a>
		        </div>
		        <div id="main">
		            <div id="title">
		                <h1>
		                	<p id='dynamicHeader'> API Copyop user service </p>
		                </h1>
		            </div>
                    <div style="clear: both;">                    	
                    </div>
                    <div class="form_block">
                        <!-- START FORM -->                       
                        <form id="form" action='<%= Constants.API_GATEWAY_EXAMPLE_URL %>request' method="post">                         	
                         	<div class="form_cl" ><span class="star">*</span> Service URL:                            
                            	<textarea id="serviceUrl" name="serviceUrl" style="resize:none;" spellcheck="false" cols="" rows="5" class="textarea1" dir="ltr"></textarea>                               
                            </div>
                         	<div class="form_cl1" ><span class="star">*</span> API User name:
                            <div class="input1">
                                <input id="apiUserName" type="text" name="apiUserName" value="" />
                            </div></div>                         
                            <div class="form_cl2" ><span class="star">*</span> API Password:
                            <div class="input1">
                                <input id="apiPassword" type="text" name="apiPassword" value="" />
                            </div></div>
                            <div class="form_cl2" ><span class="star">*</span> Locale:
                            <div class="input1">
                                <input id="locale" type="text" name="locale" value="" />
                            </div></div>
                         	<div class="form_cl1" ><span class="star">*</span> First Name:
                            <div class="input1">
                                <input id="firstName" type="text" name="firstName" value="" />
                            </div></div>
                         	<div class="form_cl2" ><span class="star">*</span> Last Name:
                            <div class="input1">
                                 <input id="lastName" type="text" name="lastName" value="" />
                            </div></div>
                            <div class="form_cl2" ><span class="star">*</span> Nickname:
                            <div class="input1">
                                <input id="nickname" type="text" name="nickname" value="" />
                            </div></div>
                            <div class="form_cl1" ><span class="star">*</span> Gender:
                            <div class="input1">
                                <input id="gender" type="text" name="gender" value="" />
                            </div></div>
                         	<div class="form_cl2" ><span class="star">*</span> Email:
                            <div class="input1">
                                 <input id="email" type="text" name="email" value="" /><br />
                            </div></div>                            
                         	<div class="form_cl2" ><span class="star">*</span> Mobile phone:
                            <div class="input1">
                                 <input id="mobilePhone" type="text" name="mobilePhone" value="" /><br />
                            </div></div>
                         	<div class="form_cl1" ><span class="star">*</span> Password:
                            <div class="input1">
                                <input id="password" type="text" name="password" value="" />
                            </div></div>                               
                         	<div class="form_cl2" ><span class="star">*</span> Retype password:
                            <div class="input1">
                                <input id="password2" type="text" name="password2" value="" />
                            </div></div>                                                                                       
                         	<div class="form_cl2" ><span class="star">*</span> Country:
                            <div class="input1">
                            	<div class="noborder">
	                            	<select id="countries" name="countries">
	                            		<!-- js -->
	                            	 </select>
                            	</div>                               	
                            </div></div>
                         	<div class="form_cl1" ><span class="star">*</span> IP:
                            <div class="input1">
                                <input id="ip" type="text" name="ip" value="" />
                            </div></div>
                            <div class="form_cl2" ><span class="star">*</span> Accept terms:
                            <div class="input1">
                            	<div class="noborder">
	                            	<select id="acceptedTermsAndConditions" name="acceptedTermsAndConditions">
	                            		<option value="true">true</option>
        								<option value="false">false</option>
	                            	 </select>
                            	</div>                               	
                            </div></div>                                                        
                         	<div class="form_cl2"> Combination Id:
                            <div class="input1">
                               <input id="combinationId" type="text" name="combinationId" value="" /><br />
                            </div></div>
                         	<div class="form_cl" > Dynamic param:                            
                            	<textarea id="dynamicParam" name="dynamicParam" style="resize:none;" spellcheck="false" cols="" rows="5" class="textarea1" dir="ltr"></textarea>                               
                            </div>
                            <div style="clear: both;"></div>                      
                            <div class="form_btn" ><img onclick="submit_form();" src="../../images/submit_button_u.png" onmouseover="this.src='../../images/submit_button_d.png'" onmouseout="this.src='../../images/submit_button_u.png'" /></div>
                            <div class="form_btn" ><img onclick="createJSON();" src="../../images/GenerateJSON_button.png" onmouseover="this.src='../../images/GenerateJSON_button_hover.png'" onmouseout="this.src='../../images/GenerateJSON_button.png'" /></div>                                                                                                                                                                                                                                                                                                                	                                                                                                                                                                                                                                                                                                               	                                                                                                                                                                                                                                                                                                               	                           
                        </form>
                        <!-- END FORM -->
                    </div>
                    <div style="clear: both;">                    	
                    </div>
		        </div>
		    </div>
		</div>
		<pre id="json" style="display:none;"></pre>
		<script id="afterLoad" src="../../js/js_after_load.js" ></script>
		<script>
			addURL('<%= Constants.API_GATEWAY_URL %>insertCopyopUser');
			function submit_form() {
				var  form = document.getElementById("form");
				form.submit();
			}
			function createJSON() {
				var apiUser 				= new Object();							
				apiUser.userName 			= document.getElementById("apiUserName").value;
				apiUser.password 			= document.getElementById("apiPassword").value;
				var request 				= new Object();  
				request.apiUser 			= apiUser;
				request.locale 				= document.getElementById("locale").value;
				request.nickname			= document.getElementById("nickname").value;
				request.acceptedTermsAndConditions = Boolean(document.getElementById("acceptedTermsAndConditions").value);
				request.gender			    = document.getElementById("gender").value;
				var register 				= new Object(); 				
				register.firstName			= document.getElementById("firstName").value;				
				register.lastName			= document.getElementById("lastName").value;
				register.email				= document.getElementById("email").value;
				register.mobilePhone		= document.getElementById("mobilePhone").value;
				register.password			= document.getElementById("password").value;				
				register.password2			= document.getElementById("password2").value;
				register.countryName		= document.getElementById("countries").value;
				register.ip					= document.getElementById("ip").value;
				register.combinationId		= document.getElementById("combinationId").value;								
				register.dynamicParam		= document.getElementById("dynamicParam").value;
				request.register			= register;
				if ((request.mobilePhone + "").match(/^\d+$/) ) { //check if  number
 					request.mobilePhone	= Number(request.mobilePhone);
				}		
				if ((register.combinationId + "").match(/^\d+$/) ) { //check if  number
 					register.combinationId	= Number(register.combinationId);
				}																								 												 
				var str = JSON.stringify(request, undefined, 4);				
				document.getElementById("json").innerHTML = syntaxHighlight(str);
				document.getElementById("json").style.display = 'block';
			}									
		</script>		
	</body>
</html>