<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.anyoption.common.managers.ApiManager" %>		
<html xmlns="http://www.w3.org/1999/xhtml">
	<!-- START HEAD -->
	<head>
		<meta HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=UTF-8" />
	   	<title>Binary Options | anyoption</title>
	    <meta name="Title" content="Binary Options | anyoption" />
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>	    	           		
		<link rel="stylesheet" type="text/css" href="../css/style.css" />
    	<link rel="shortcut icon" href="../images/favicon.ico" type="image/vnd.microsoft.icon" />
    	<script id="afterLoad" src="../js/jquery.js" ></script>
		<script id="afterLoad" src="../js/json2.js" ></script>    			
	</head>
	<!-- END HEAD -->
	<body id="EN">
		<div id="bg_container">
        	<div id="page">
		    	<div id="logo">
		        	<a href="index.jsp"><img src="../images/logo.png" alt="anyoption" /></a>
		        </div>
		        <div id="main">
		            <div id="title">
		                <h1>
		                	<p id='dynamicHeader'>API test JSON service</p>
		                </h1>
		            </div>
                    <div style="clear: both;">                    	
                    </div>
                    <div class="form_block">
                        <!-- START FORM -->                       
                        <!-- END FORM -->
                    </div>
                    <div style="clear: both;">                    	
                    </div>
		        </div>
		    </div>
		</div>
		<pre id="json" style="display:none;"></pre>
		<script id="afterLoad" src="../js/js_after_load.js" ></script>		
		<% 
			String id = (String)request.getParameter("id");
			String userId = (String)request.getParameter("userId");
			String oppId = (String)request.getParameter("oppId");			
			if (id != null && userId != null && oppId != null) {
				ApiManager.insertAPITEST(Long.parseLong(id), Long.parseLong(userId), Long.parseLong(oppId));  
			}		
		%>
	</body>
</html>