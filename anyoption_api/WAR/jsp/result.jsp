<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.anyoption.json.results.*" %>
<%@page import="com.anyoption.common.service.results.*" %>
<%@page import="com.anyoption.common.service.util.*" %>
<%@page import="com.anyoption.json.util.*" %>
<%@page import="com.anyoption.beans.base.*" %>
<%@page import="com.anyoption.common.beans.base.Opportunity" %>
<%@page import="com.anyoption.common.beans.base.Market" %>		
<%@page import="com.anyoption.common.beans.base.User" %>
<%@page import="com.anyoption.json.results.UserMethodResult" %>
<%@page import="com.anyoption.json.results.LastCurrenciesRateMethodResult" %>
<%@page import="com.anyoption.common.beans.base.LastCurrencyRate" %>
<%@page import="com.anyoption.common.beans.base.OpportunityOddsPair" %>
<%@page import="com.anyoption.common.beans.base.MarketingInfo" %>
<%@page pageEncoding="UTF-8" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<!-- START HEAD -->
	<head>
		<meta HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=UTF-8" />
	   	<title>Binary Options | anyoption</title>
	    <meta name="Title" content="Binary Options | anyoption" />
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>	    	           		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css" />
    	<link rel="shortcut icon" href="${pageContext.request.contextPath}/images/favicon.ico" type="image/vnd.microsoft.icon" />		
	</head>
	<!-- END HEAD -->
	<body id="EN">
		<div id="bg_container" >
        	<div id="page">
		    	<div id="logo">
		        	<a href="index.jsp"><img src="${pageContext.request.contextPath}/images/logo.png" alt="anyoption" /></a>
		        </div>
		        <div id="main">
		            <div id="title">
		                <h1>
		                	<p id='dynamicHeader'>API anyoption Services result</p>
		                	<p>for <% out.println((String) request.getSession().getAttribute("serviceName")); %> service
		                </h1>
		            </div>
						<%
							MethodResult result = (MethodResult) request.getSession().getAttribute("methodResult");
											if (result != null) {
												String apiCode = result.getApiCode();
												String apiCodeDescription = result.getApiCodeDescription();			
												out.println("GENERAL error code=<span style='color:red;'>" + apiCode + "</span><br/>");
												out.println("GENERAL error description=<span style='color:red;'>" + apiCodeDescription + "</span><br/><br/>");
												if (result.getUserMessages() != null) {
													for (ErrorMessage em : result.getUserMessages()) {
														out.println("Api Error Code=<span style='color:red;'>" + em.getApiErrorCode() + "</span><br/>");
														out.println("Field=<span style='color:red;'>" + em.getField() + "</span><br/>");								
														out.println("Message=<span style='color:red;'>" + em.getMessage() + "</span><br/><br/>");					
													}
												}
												if (result instanceof ContactMethodResult) {
													Contact contact = ((ContactMethodResult)result).getContact();
													if (contact != null) {														
														long contactId = contact.getId();					
														out.println("Contact Id=<span style='color:red;'>" + contactId + "</span><br/>");
													}																	
												}
												if (result instanceof UserMethodResult) { 
													User user = ((UserMethodResult)result).getUser();
													if (user != null) {														
														long userId = user.getId();					
														out.println("User Id=<span style='color:red;'>" + userId + "</span><br/>");
														out.println("First Name=<span style='color:red;'>" + user.getFirstName() + "</span><br/>");
														out.println("Last Name=<span style='color:red;'>" + user.getLastName() + "</span><br/>");
														out.println("Email=<span style='color:red;'>" + user.getEmail() + "</span><br/>");
														out.println("Currency Symbol=<span style='color:red;'>" + user.getCurrencySymbol() + "</span><br/>");
														out.println("Balance in cent=<span style='color:red;'>" + user.getBalance() + "</span><br/>");										
														out.println("Balance display=<span style='color:red;'>" + user.getBalanceWF() + "</span><br/>");
														out.println("Login token=<span style='color:red;'>" + user.getLoginToken() + "</span><br/>");										
													}	 																
												}

												if (result instanceof ChartDataMethodResult) {		
													double[] rates = ((ChartDataMethodResult)result).getRates();
													long[] ratesTime = ((ChartDataMethodResult)result).getRatesTimes();					
													if (rates != null) { 
														out.println("OpportunityId=<span style='color:red;'>" + ((ChartDataMethodResult)result).getOpportunityId() + "</span><br/>");
														out.println("Market Name=<span style='color:red;'>" + ((ChartDataMethodResult)result).getMarketName() + "</span><br/>");
														out.println("Time First Invest millsec=<span style='color:red;'>" + ((ChartDataMethodResult)result).getTimeFirstInvestMillsec() + "</span><br/>");
														out.println("Time last Invest millsec=<span style='color:red;'>" + ((ChartDataMethodResult)result).getTimeLastInvestMillsec() + "</span><br/>");
														out.println("Time Est close=<span style='color:red;'>" + ((ChartDataMethodResult)result).getTimeEstClosingMillsec()+ "</span><br/>");
						%>
											<div class="table_nd" style="margin-top:30px;">
												<table  cellpadding="0" cellspacing="" width="100%" >
													<tr>
														<td class="table_line_nd subtitle_nd">
															Rate
														</td>
														<td class="table_line_nd subtitle_nd">
															Rate time
														</td>														
													</tr>	
													<% for (int i = 0; i < rates.length ;i++)  {%>									
													<tr>										
														<td class="table_line_nd" >
															<% out.println(rates[i]);	%>												
														</td>
														<td class="table_line_nd">
															<% out.println(ratesTime[i]);	%>												
														</td>
													</tr>
													<%}%>
												</table>
											</div>	
								<%																											
									}																
								}
								
								if (result instanceof OpportunityOddsPairResult) {		
									OpportunityOddsPair[] opportunityOddsPairList = ((OpportunityOddsPairResult)result).getOpportunityOddsPair();
													
									if (opportunityOddsPairList != null) { 
										%>
											<div class="table_nd" style="margin-top:30px;">
												<table  cellpadding="0" cellspacing="" width="100%" >
													<tr>
														<td class="table_line_nd subtitle_nd">
															Selector Id
														</td>
														<td class="table_line_nd subtitle_nd">
															returnSelector
														</td>
														<td class="table_line_nd subtitle_nd">
															refundSelector
														</td>																												
													</tr>	
													<% for (int i = 0; i < opportunityOddsPairList.length ;i++)  {%>									
													<tr>										
														<td class="table_line_nd" >
															<% out.println(opportunityOddsPairList[i].getSelectorID());	%>												
														</td>
														<td class="table_line_nd">
															<% out.println(opportunityOddsPairList[i].getReturnSelector()); %>												
														</td>
														<td class="table_line_nd">
															<% out.println(opportunityOddsPairList[i].getRefundSelector()); %>												
														</td>														
													</tr>
													<%}%>
												</table>
											</div>	
								<%																											
									}																
								}
								
								if (result instanceof OpportunityMethodResult) {									
									Opportunity opp = ((OpportunityMethodResult)result).getOpportunity();
									if (opp != null) { 									
										out.println("OpportunityId=<span style='color:red;'>" + opp.getId() + "</span><br/>");
										out.println("Market Id=<span style='color:red;'>" + opp.getMarketId()  + "</span><br/>");
										out.println("Time First Invest mills=<span style='color:red;'>" + opp.getTimeFirstInvestMillsec() + "</span><br/>");
										out.println("Time last Invest mills=<span style='color:red;'>" + opp.getTimeLastInvestMillsec() + "</span><br/>");
										out.println("Time est close mills=<span style='color:red;'>" + opp.getTimeEstClosingMillsec() + "</span><br/>");
										out.println("Odds Win=<span style='color:red;'>" + opp.getPageOddsWin() + "</span><br/>");
										out.println("Odds Lose=<span style='color:red;'>" + opp.getPageOddsLose() + "</span><br/>");
										out.println("Current level=<span style='color:red;'>" + opp.getCurrentLevel() + "</span><br/>");
									}
								}
								
								if (result instanceof InvestmentMethodResult) {
									Investment investment = ((InvestmentMethodResult)result).getInvestment();									
									if (investment != null) {
										out.println("<br/>Investment Id=<span style='color:red;'>" + investment.getId() + "</span><br/>");
									}
								}

								if (result instanceof MarketsMethodResult) {
									Market[] markets = ((MarketsMethodResult)result).getMarkets();
									if (markets != null) { %>
											<div class="table_nd" style="margin-top:30px;">
												<table  cellpadding="0" cellspacing="" width="100%" >
													<tr>
														<td class="table_line_nd subtitle_nd">
															Market Id
														</td>
														<td class="table_line_nd subtitle_nd">
															Market name
														</td>
														<td class="table_line_nd subtitle_nd">
															Group Id
														</td>																																										
														<td class="table_line_nd subtitle_nd">
															Group Name
														</td>														
													</tr>	
													<% for (Market m : markets)  {%>									
													<tr>									
														<td class="table_line_nd" >
															<% out.println(m.getId());	%>												
														</td>
														<td class="table_line_nd">
															<% out.println(m.getDisplayName());	%>												
														</td>
														<td class="table_line_nd">
															<% out.println(m.getGroupId());	%>												
														</td>
														<td class="table_line_nd">
															<% out.println(m.getGroupName()); %>												
														</td>																												
													</tr>
													<%}%>
												</table>
											</div>	
								<%																											
									}																	
								}	
								
								if (result instanceof LastCurrenciesRateMethodResult) {
									LastCurrencyRate[] currencyRateList = ((LastCurrenciesRateMethodResult)result).getLastCurrenciesRate();
									if (currencyRateList != null) { %>
											<div class="table_nd" style="margin-top:30px;">
												<table  cellpadding="0" cellspacing="" width="100%" >
													<tr>
														<td class="table_line_nd subtitle_nd">
															Currency Code
														</td>
														<td class="table_line_nd subtitle_nd">
															Rate
														</td>
														<td class="table_line_nd subtitle_nd">
															Time created
														</td>																																																																						
													</tr>	
													<% for (LastCurrencyRate c : currencyRateList)  {%>									
													<tr>									
														<td class="table_line_nd">
															<% out.println(c.getCurrencyCode());	%>												
														</td>
														<td class="table_line_nd">
															<% out.println(c.getRate());	%>												
														</td>
														<td class="table_line_nd">
															<% out.println(c.getTimeCreatedStr());	%>												
														</td>														
													</tr>
													<%}%>
												</table>
											</div>	
								<%																											
									}																	
								}
								
								if (result instanceof FTDUsersMethodResult) {
									com.anyoption.common.beans.base.FTDUser[] FTDUsersList = ((FTDUsersMethodResult)result).getFTDUsersList();
									if (FTDUsersList != null) { %>
											<div class="table_nd" style="margin-top:30px;">
												<table  cellpadding="0" cellspacing="" width="100%" >
													<tr>
														<td class="table_line_nd subtitle_nd">
															Transaction Id
														</td>
														<td class="table_line_nd subtitle_nd">
															Amount in USD
														</td>
														<td class="table_line_nd subtitle_nd">
															User Id
														</td>
														<td class="table_line_nd subtitle_nd">
															Sub affiliate Id
														</td>
														<td class="table_line_nd subtitle_nd">
															Dynamic parameter
														</td>																																																																																																															
													</tr>	
													<% for (com.anyoption.common.beans.base.FTDUser user : FTDUsersList)  {%>									
													<tr>									
														<td class="table_line_nd">
															<% out.println(user.getTransactionId());	%>												
														</td>
														<td class="table_line_nd">
															<% out.println(user.getAmountInUSD());	%>												
														</td>
														<td class="table_line_nd">
															<% out.println(user.getUserId());	%>												
														</td>
														<td class="table_line_nd">
															<% out.println(user.getSubAffId());	%>												
														</td>																
														<td class="table_line_nd">
															<% out.println(user.getDynamicParameter());	%>												
														</td>	
													</tr>
													<%}%>
												</table>
											</div>	
								<%																											
									}																	
								}									
																														
								if (result instanceof InvestmentsMethodResult) {
									Investment[] invList = ((InvestmentsMethodResult)result).getInvestments();									
									if (invList != null && invList.length > 0) {
										invList[0].setExpiryLevel(invList[0].getExpiryLevel().replaceAll(",",""));
										if (Double.valueOf(invList[0].getExpiryLevel()) == 0) { // case of open investment
											%>
											<div class="table_nd" style="margin-top:30px;">
												<table  cellpadding="0" cellspacing="" width="100%" >
													<tr>
														<td class="table_line_nd subtitle_nd">
															Option Id
														</td>
														<td class="table_line_nd subtitle_nd">
															Asset name
														</td>
														<td class="table_line_nd subtitle_nd">
															Type
														</td>																																										
														<td class="table_line_nd subtitle_nd">
															Level
														</td>														
														<td class="table_line_nd subtitle_nd">
															Purchased at
														</td>
														<td class="table_line_nd subtitle_nd">
															Expiry/Sale time
														</td>																																																							
														<td class="table_line_nd subtitle_nd">
															Amount
														</td>																																
													</tr>	
													<% for (Investment inv : invList)  {%>									
													<tr>										
														<td class="table_line_nd" >
															<% out.println(inv.getId());	%>												
														</td>
														<td class="table_line_nd">
															<% out.println(inv.getAsset());	%>												
														</td>
														<td class="table_line_nd">
															<% out.println(inv.getTypeId());	%>												
														</td>
														<td class="table_line_nd">
															<% out.println(inv.getCurrentLevelTxt());	%>												
														</td>																												
														<td class="table_line_nd">
															<% out.println(inv.getTimePurchaseTxt());	%>												
														</td>
														<td class="table_line_nd">
															<% out.println(inv.getTimeEstClosingTxt());	%>												
														</td>																																																																						
														<td class="table_line_nd">
															<% out.println(inv.getAmountTxt());	%>												
														</td>
													</tr>
													<%}%>
												</table>
											</div>
												<%
										} else { // case of settled investments 
										%>
											<div class="table_nd" style="margin-top:30px;">
												<table  cellpadding="0" cellspacing="" width="100%" >
													<tr>
														<td class="table_line_nd subtitle_nd">
															Option Id
														</td>
														<td class="table_line_nd subtitle_nd">
															Asset name
														</td>
														<td class="table_line_nd subtitle_nd">
															Type
														</td>																																										
														<td class="table_line_nd subtitle_nd">
															Level
														</td>
														<td class="table_line_nd subtitle_nd">
															Expiry Level
														</td>
														<td class="table_line_nd subtitle_nd">
															Purchased at
														</td>
														<td class="table_line_nd subtitle_nd">
															Expiry/Sale time
														</td>																													
														<td class="table_line_nd subtitle_nd">
															Amount
														</td>
														<td class="table_line_nd subtitle_nd">
															Return
														</td>																																	
													</tr>	
													<% for (Investment inv : invList)  {%>									
													<tr>										
														<td class="table_line_nd" >
															<% out.println(inv.getId());	%>												
														</td>
														<td class="table_line_nd">
															<% out.println(inv.getAsset());	%>												
														</td>
														<td class="table_line_nd">
															<% out.println(inv.getTypeId());	%>												
														</td>
														<td class="table_line_nd">
															<% out.println(inv.getCurrentLevelTxt());	%>												
														</td>	
														<td class="table_line_nd">
															<% out.println(inv.getExpiryLevel());	%>												
														</td>	
														<td class="table_line_nd">
															<% out.println(inv.getTimePurchaseTxt());	%>												
														</td>
														<td class="table_line_nd">
															<% out.println(inv.getTimeEstClosingTxt());	%>												
														</td>																												
														<td class="table_line_nd">
															<% out.println(inv.getAmountTxt());	%>												
														</td>
														<td class="table_line_nd">
															<% out.println(inv.getAmountReturnWF());	%>												
														</td>
													</tr>
													<%}%>
												</table>
											</div>
									<%	}																									
									}																	
								}
								
								if (result instanceof MarketingUsersMethodResult) {
									MarketingInfo marketingInfo = ((MarketingUsersMethodResult)result).getMarketingInfo();
									if (marketingInfo != null) { %>
										<div class="table_nd" style="margin-top:30px;">
											<table  cellpadding="0" cellspacing="" width="100%" >
												<tr>
													<td class="table_line_nd subtitle_nd">
														Signups
													</td>
													<td class="table_line_nd subtitle_nd">
														First Time Deposit
													</td>																																																																						
												</tr>								
												<tr>									
													<td class="table_line_nd">
														<% out.println(marketingInfo.getCountSignups());	%>												
													</td>
													<td class="table_line_nd">
														<% out.println(marketingInfo.getCountFtds());	%>												
													</td>
												</tr>
												
											</table>
										</div>
										<div class="table_nd" style="margin-top:30px;">
											<table  cellpadding="0" cellspacing="" width="100%" >
												<tr>
													<td class="table_line_nd subtitle_nd">
														DP
													</td>																																																																						
												</tr>	
												<% for (String dp : marketingInfo.getDynamicParams())  {%>									
												<tr>									
													<td class="table_line_nd">
														<% out.println(dp);	%>												
													</td>
												</tr>
												<%}%>
											</table>
										</div>	
								<%																											
									}														
								}
							} else {
								out.println("Service does not exists or invalid link");
							}
						%>		            					   
				</div>
			</div>
		</div>																
	</BODY>
</HTML>