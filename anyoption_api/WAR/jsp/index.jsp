<%@page import="com.anyoption.web.api.service.util.Constants"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<!-- START HEAD -->
	<head>
		<meta HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=UTF-8" />
	   	<title>Binary Options | anyoption</title>
	    <meta name="Title" content="Binary Options | anyoption" />
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>	    	           		
		<link rel="stylesheet" type="text/css" href="../css/style.css" />
    	<link rel="shortcut icon" href="../images/favicon.ico" type="image/vnd.microsoft.icon" />		
	</head>
	<!-- END HEAD -->
	<body id="EN">
		<div id="bg_container" >
        	<div id="page">
		    	<div id="logo">
		        	<a href="index.jsp"><img src="../images/logo.png" alt="anyoption" /></a>
		        </div>
		        <div id="main">
		            <div id="title">
		                <h1>
		                	<p id='dynamicHeader'>API anyoption Services</p>
		                </h1>
		            </div>
	                <ul class="bottom_list">
	                	<li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL %>userCapitalExtraFields.jsp">Get user Extra Fields - anycapital</a></li>
	                    <li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL %>user.jsp">Get user - anycapital</a></li>
	                    <li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL %>insertLead.jsp">Insert lead</a></li>
						<li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL %>insertUser.jsp">Insert user</a></li>						
						<li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL %>userDetails.jsp">Get user details</a></li>
						<li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL %>loginIframe.jsp">Login iframe</a></li>
						<li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL %>userSettledInvestment.jsp">Get user settled investment</a></li>
						<li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL %>userOpenInvestment.jsp">Get user open investment</a></li>						
						<li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL %>hourlyBinaryOpenMarkets.jsp">Get binary hourly open markets for trading</a></li>						
						<li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL %>allBinaryMarkets.jsp">Get all binary markets for trading</a></li>
						<li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL %>binaryHomePageMarkets.jsp">Get binary HomePage markets</a></li>						
						<li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL %>currentOppByMarket.jsp">Get current opportunity for open market</a></li>
						<!-- li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL %>returnOdds.jsp">Get all return odds</a></li -->
						<li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL %>levelData.jsp">Get level data for open markets</a></li>
						<li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL %>currentLevelByOpp.jsp">Get current level by opportunity</a></li>						
						<li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL %>insertBinaryInvestmentByCurrentLevel.jsp">Insert binary investment by current level</a></li>						
						<li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL %>insertBinaryInvestmentWithLevel.jsp">Insert binary investment with level</a></li>
						<li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL %>insertBinaryInvestmentWithLevelExternal.jsp">Insert binary investment with level and external user</a></li>
						<li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL %>returnCurrenciesRate.jsp">Return currencies rate</a></li>
						<li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL %>ftdUsers.jsp">Get API first time depoist users list</a></li>
						<li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL %>marketingUsers.jsp">Get marketing users</a></li>
						<li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL_COPYOP %>insertProfile.jsp">Copyop - Insert user</a></li>
						<li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL_COPYOP %>loginIframe.jsp">Copyop - Login iframe</a></li>
						<li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL_COPYOP %>userDetails.jsp">Copyop - Get user details</a></li>
						<li><a href="<%= Constants.API_GATEWAY_EXAMPLE_RESULT_URL %>testJson.html">Test JSON</a></li>
	                </ul>
				</div>
			</div>
		</div>																
	</BODY>
</HTML>