
//add service URL
function addURL(url) {
	document.getElementById("serviceUrl").value = url;
}		

//countries
var countries = document.getElementById("countries");
var countryCode = "GB";
var countriesList;
if (countries != null && countries != 'undefined') {
	countriesList = getCountriesList();
	var i = 0;				
	for (country in countriesList) {
		countries.options[i] = new Option(countriesList[country].country_name);
		countries.options[i].value = country;
		i++;			
	}	
	countries.selectedIndex = 0;	
}

//currencies
var currencies = document.getElementById("currencies");
var currenciesList;
if (currencies != null && currencies != 'undefined') {
	currenciesList = getCurrenciesList();
	var j = 0;				
	for (currency in currenciesList) {
		currencies.options[j] = new Option(currenciesList[currency].currency_code);
		currencies.options[j].value = currency;
		j++;			
	}	
	currencies.selectedIndex = 0;	
}

function getCurrenciesList() {
	var currenciesList = {
				'':{currency_code:""},
				AAA:{currency_code:"Currency not exists"},
				ILS:{currency_code:"ILS"},
				USD:{currency_code:"USD"},
				EUR:{currency_code:"EUR"},
				GBP:{currency_code:"GBP"},
				TRY:{currency_code:"TRY"},
				RUB:{currency_code:"RUB"},
				CNY:{currency_code:"CNY"},
				KRW:{currency_code:"KRW"}
				};
	return currenciesList;				
} 

function getCountriesList() {	
	var countriesList = {
				'':{country_name:""},
				AAA:{country_name:"Country not exists"},
				AFG:{country_name:"Afghanistan"},
				ALB:{country_name:"Albania"},
				DZA:{country_name:"Algeria"},
				ASM:{country_name:"American Samoa"},
				AND:{country_name:"Andorra"},
				AGO:{country_name:"Angola"},
				AIA:{country_name:"Anguilla"},
				ATA:{country_name:"Antarctica"},
				ATG:{country_name:"Antigua and Barbuda"},
				ARG:{country_name:"Argentina"},
				ARM:{country_name:"Armenia"},
				ABW:{country_name:"Aruba"},
				AUS:{country_name:"Australia"},
				AUT:{country_name:"Austria"},
				AZE:{country_name:"Azerbaijan"},
				BHS:{country_name:"Bahamas"},
				BHR:{country_name:"Bahrain"},
				BGD:{country_name:"Bangladesh"},
				BRB:{country_name:"Barbados"},
				BLR:{country_name:"Belarus"},
				BEL:{country_name:"Belgium"},
				BLZ:{country_name:"Belize"},
				BEN:{country_name:"Benin"},
				BMU:{country_name:"Bermuda"},
				BTN:{country_name:"Bhutan"},
				BOL:{country_name:"Bolivia"},
				BIH:{country_name:"Bosnia"},
				BWA:{country_name:"Botswana"},
				BVT:{country_name:"Bouvet Island"},
				BRA:{country_name:"Brazil"},
				BRN:{country_name:"Brunei Darussalam"},
				BGR:{country_name:"Bulgaria"},
				BFA:{country_name:"Burkina Faso"},
				BDI:{country_name:"Burundi"},
				KHM:{country_name:"Cambodia"},
				CMR:{country_name:"Cameroon"},
				CAN:{country_name:"Canada"},
				CPV:{country_name:"Cape Verde"},
				CYM:{country_name:"Cayman Islands"},
				CAF:{country_name:"Central Africa"},
				TCD:{country_name:"Chad"},
				CHL:{country_name:"Chile"},
				CHN:{country_name:"China"},
				CXR:{country_name:"Christmas Island"},
				CCK:{country_name:"Cocos Islands"},
				COL:{country_name:"Colombia"},
				COM:{country_name:"Comoros"},
				COG:{country_name:"Congo"},
				COK:{country_name:"Cook Islands"},
				CRI:{country_name:"Costa Rica"},
				CIV:{country_name:"Ivory Coast"},
				HRV:{country_name:"Croatia"},
				CUB:{country_name:"Cuba"},
				CYP:{country_name:"Cyprus"},
				CZE:{country_name:"Czech Republic"},
				DNK:{country_name:"Denmark"},
				DJI:{country_name:"Djibouti"},
				DMA:{country_name:"Dominica"},
				DOM:{country_name:"Dominican Republic"},
				TMP:{country_name:"East Timor"},
				ECU:{country_name:"Ecuador"},
				EGY:{country_name:"Egypt"},
				SLV:{country_name:"El Salvador"},
				GNQ:{country_name:"Equatorial Guinea"},
				ERI:{country_name:"Eritrea"},
				EST:{country_name:"Estonia"},
				ETH:{country_name:"Ethiopia"},
				FLK:{country_name:"Falkland Islands"},
				FRO:{country_name:"Faroe Islands"},
				FJI:{country_name:"Fiji"},
				FIN:{country_name:"Finland"},
				FRA:{country_name:"France"},
				GUF:{country_name:"French Guiana"},
				PYF:{country_name:"French Polynesia"},
				GAB:{country_name:"Gabon"},
				GMB:{country_name:"Gambia"},
				GEO:{country_name:"Georgia"},
				DEU:{country_name:"Germany"},
				GHA:{country_name:"Ghana"},
				GIB:{country_name:"Gibraltar"},
				GRC:{country_name:"Greece"},
				GRL:{country_name:"Greenland"},
				GRD:{country_name:"Grenada"},
				GLP:{country_name:"Guadeloupe"},
				GUM:{country_name:"Guam"},
				GTM:{country_name:"Guatemala"},
				GIN:{country_name:"Guinea"},
				GUY:{country_name:"Guyana"},
				HTI:{country_name:"Haiti"},
				HND:{country_name:"Honduras"},
				HKG:{country_name:"Hong Kong"},
				HUN:{country_name:"Hungary"},
				ISL:{country_name:"Iceland"},
				IND:{country_name:"India"},
				IDN:{country_name:"Indonesia"},
				IRN:{country_name:"Iran"},
				IRQ:{country_name:"Iraq"},
				IRL:{country_name:"Ireland"},
				ITA:{country_name:"Italy"},
				JAM:{country_name:"Jamaica"},
				JPN:{country_name:"Japan"},
				JOR:{country_name:"Jordan"},
				KAZ:{country_name:"Kazakhstan"},
				KEN:{country_name:"Kenya"},
				KIR:{country_name:"Kiribati"},
				KWT:{country_name:"Kuwait"},
				KGZ:{country_name:"Kyrgyzstan"},
				LAO:{country_name:"Laos"},
				LVA:{country_name:"Latvia"},
				LBN:{country_name:"Lebanon"},
				LSO:{country_name:"Lesotho"},
				LBR:{country_name:"Liberia"},
				LBY:{country_name:"Libya"},
				LIE:{country_name:"Liechtenstein"},
				LTU:{country_name:"Lithuania"},
				LUX:{country_name:"Luxembourg"},
				MAC:{country_name:"Macau"},
				MKD:{country_name:"Macedonia"},
				MDG:{country_name:"Madagascar"},
				MWI:{country_name:"Malawi"},
				MYS:{country_name:"Malaysia"},
				MDV:{country_name:"Maldives"},
				MLI:{country_name:"Mali"},
				MLT:{country_name:"Malta"},
				MHL:{country_name:"Marshal Islands"},
				MTQ:{country_name:"Martinique"},
				MRT:{country_name:"Mauritania"},
				MUS:{country_name:"Mauritius"},
				MYT:{country_name:"Mayotte Island"},
				MEX:{country_name:"Mexico"},
				FSM:{country_name:"Micronesia"},
				MDA:{country_name:"Moldova"},
				MCO:{country_name:"Monaco"},
				MNG:{country_name:"Mongolia"},
				MSR:{country_name:"Montserrat"},
				MAR:{country_name:"Morocco"},
				MOZ:{country_name:"Mozambique"},
				MMR:{country_name:"Myanmar"},
				NAM:{country_name:"Namibia"},
				NRU:{country_name:"Nauru"},
				NPL:{country_name:"Nepal"},
				NLD:{country_name:"Netherlands"},
				ANT:{country_name:"Netherlands Antilles"},
				NCL:{country_name:"New Caledonia"},
				NZL:{country_name:"New Zealand"},
				NIC:{country_name:"Nicaragua"},
				NER:{country_name:"Niger"},
				NGA:{country_name:"Nigeria"},
				NIU:{country_name:"Niue"},
				NFK:{country_name:"Norfolk Island"},
				PRK:{country_name:"North Korea"},
				NOR:{country_name:"Norway"},
				OMN:{country_name:"Oman"},
				PAK:{country_name:"Pakistan"},
				PLW:{country_name:"Palau"},
				PAN:{country_name:"Panama"},
				PNG:{country_name:"Papua New Guinea"},
				PRY:{country_name:"Paraguay"},
				PER:{country_name:"Peru"},
				PHL:{country_name:"Philippines"},
				PCN:{country_name:"Pitcairn"},
				POL:{country_name:"Poland"},
				PRT:{country_name:"Portugal"},
				PRI:{country_name:"Puerto Rico"},
				QAT:{country_name:"Qatar"},
				REU:{country_name:"Reunion Island"},
				ROM:{country_name:"Romania"},
				RUS:{country_name:"Russia"},
				RWA:{country_name:"Rwanda"},
				KNA:{country_name:"Saint Kitts and Nevis"},
				LCA:{country_name:"Saint Lucia"},
				WSM:{country_name:"Samoa"},
				SMR:{country_name:"San Marino"},
				SAU:{country_name:"Saudi Arabia"},
				SEN:{country_name:"Senegal"},
				SYC:{country_name:"Seychelles"},
				SLE:{country_name:"Sierra Leone"},
				SGP:{country_name:"Singapore"},
				SVK:{country_name:"Slovakia"},
				SVN:{country_name:"Slovenia"},
				SLB:{country_name:"Solomon Islands"},
				SOM:{country_name:"Somalia"},
				ZAF:{country_name:"South Africa"},
				KOR:{country_name:"South Korea"},
				ESP:{country_name:"Spain"},
				LKA:{country_name:"Sri Lanka"},
				SHN:{country_name:"ST. Helena"},
				SDN:{country_name:"Sudan"},
				SUR:{country_name:"Suriname"},
				SWZ:{country_name:"Swaziland"},
				SWE:{country_name:"Sweden"},
				CHE:{country_name:"Switzerland"},
				SYR:{country_name:"Syria"},
				TWN:{country_name:"Taiwan"},
				TJK:{country_name:"Tajikistan"},
				TZA:{country_name:"Tanzania"},
				THA:{country_name:"Thailand"},
				TGO:{country_name:"Togo"},
				TKL:{country_name:"Tokelau"},
				TON:{country_name:"Tonga Islands"},
				TTO:{country_name:"Trinidad and Tobago"},
				TUN:{country_name:"Tunisia"},
				TUR:{country_name:"Turkey"},
				TKM:{country_name:"Turkmenistan"},
				TUV:{country_name:"Tuvalu"},
				UGA:{country_name:"Uganda"},
				UKR:{country_name:"Ukraine"},
				ARE:{country_name:"United Arab Emirates"},
				GBR:{country_name:"United Kingdom"},
				USA:{country_name:"United States"},
				URY:{country_name:"Uruguay"},
				UZB:{country_name:"Uzbekistan"},
				VUT:{country_name:"Vanuato"},
				VAT:{country_name:"Vatican"},
				VEN:{country_name:"Venezuela"},
				VNM:{country_name:"Vietnam"},
				VGB:{country_name:"Virgin Islands UK"},
				VIR:{country_name:"Virgin Islands US"},
				ESH:{country_name:"Western Sahara"},
				YEM:{country_name:"Yemen"},
				ZAR:{country_name:"Zaire"},
				ZMB:{country_name:"Zambia"},
				ZWE:{country_name:"Zimbabwe"},
				SRB:{country_name:"Serbia"},
				MNE:{country_name:"Montenegro"}
				};	
	return countriesList;
	}

//JSON Highlight	
function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}	