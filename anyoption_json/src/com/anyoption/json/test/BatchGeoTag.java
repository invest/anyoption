package com.anyoption.json.test;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import com.anyoption.json.requests.GeoTagRequest;
import com.anyoption.json.results.GeoTagResult;
import com.anyoption.json.util.JsonExclusionStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class BatchGeoTag {

		private final String USER_AGENT = "Mozilla/5.0";
		String url = "http://www.testenv.anyoption.com/geoTagService/";
		String sql = "SELECT id, ip FROM users WHERE country_id=2";
		String connection = "put some connection string here";
		String user = "put some user";
		String pass = "put some pass";
		String fileName = "C://temp//afganGeoTag.txt";
		
		String s1 = "UPDATE users SET country_id = ( SELECT ID FROM countries WHERE a2 = '" ;
		String s2 =	"' ) WHERE ID = ";  
				
	 
		public static void main(String[] args) throws Exception {
			BatchGeoTag  http = new BatchGeoTag ();
			System.out.println("BatchGeoTag - GeoTag\n");
			http.query();
		}
		public void query() throws Exception
			  {
				Class.forName ("oracle.jdbc.OracleDriver");
				PrintStream out = null;
			    out = new PrintStream(new FileOutputStream(fileName));

			    Connection conn = DriverManager.getConnection (connection, user, pass);
			    try {
				    	Statement stmt = conn.createStatement();
						try {
							ResultSet rset = stmt.executeQuery(sql);
							try {
									while (rset.next()) {
										String geoTag = sendPost (rset.getString(2));
										out.println(s1 + geoTag + s2 + rset.getString(1)+";");
									}
							}
							finally {
								try { rset.close(); } catch (Exception ignore) {}
							}
						} 
						finally {
							try { stmt.close(); } catch (Exception ignore) {}
						}
					} 
					finally {
						try { conn.close(); out.close();} catch (Exception ignore) {}
					}
}

		// HTTP POST request
		private String sendPost(String ip) throws Exception {

			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	 
			//add request header
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

	    	Gson gson = new GsonBuilder().setExclusionStrategies(new JsonExclusionStrategy()).serializeNulls().create();
	    	
			// deserialize the JSON request from client
	    	GeoTagRequest geoTagRequest = new GeoTagRequest();
	    	geoTagRequest.setIp(ip);
					
			String json = gson.toJson(geoTagRequest);

			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(json);
			wr.flush();
			wr.close();
	 
			int responseCode = con.getResponseCode();
//			System.out.println("Sending 'POST' request to URL : " + url);
//			System.out.println("Post parameters : " + json);
//			System.out.println("Response Code : " + responseCode);
	 
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			GeoTagResult result = gson.fromJson(in, GeoTagResult.class);
			in.close();
	 
			//print result
			//System.out.println(result);
			return result.getCountryCode();
		}
	 
}

