package com.anyoption.json.test;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

import com.anyoption.beans.User;
import com.anyoption.common.service.requests.EmailValidatorRequest;
import com.anyoption.common.service.requests.UpdateUserMethodRequest;
import com.anyoption.common.service.results.EmailValidatorResult;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.requests.CreditCardValidatorRequest;
import com.anyoption.json.results.CreditCardValidatorResponse;
import com.anyoption.json.util.JsonExclusionStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonServiceTest {
	
		Gson gson; 
		private final String USER_AGENT = "Mozilla/5.0";
		//String serviceUrl = "http://www.testenv.anyoption.com/jsonService/AnyoptionService/";
		String serviceUrl = "http://pavlinsm.bg.anyoption.com/jsonService/AnyoptionService/";	 
		public static void main(String[] args) throws Exception {
			JsonServiceTest  http = new JsonServiceTest ();
			System.out.println("Testing - JSON Service \n");
			//http.testEmailValidation();
			//http.testCCValidation();
			http.testUpdateUser();
		}
	 
		public void testEmailValidation(){
			System.out.println("Testing - Email validation");
			EmailValidatorRequest request = new EmailValidatorRequest();
			request.setEmail("pavlinsm@anyoption.com");
			
			try {
				EmailValidatorResult result = test(request, EmailValidatorResult.class, "validateEmail");
				//print result
				System.out.println(result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		public void testUpdateUser(){
			System.out.println("Testing - Update User");
			UpdateUserMethodRequest request = new UpdateUserMethodRequest();
			User user = new User();
			user.setTimeBirthDate(new Date());
			request.setUser(user);
			
			try {
				MethodResult result = test(request, MethodResult.class, "updateUser");
				//print result
				System.out.println(result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		public void testCCValidation(){
			System.out.println("Testing - Credit card validation");
			CreditCardValidatorRequest request = new CreditCardValidatorRequest();
			request.setCcNumber("4556243173468576");
			
			try {
				CreditCardValidatorResponse response = test(request, CreditCardValidatorResponse.class, "validateCC");
				//print result
				System.out.println(response);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		// HTTP POST request
		private <T> T test(Object request, Class<T> clazz, String method) throws Exception {
			HttpURLConnection con = null;
			try{
				String methodUrl = serviceUrl+method; 
		 		URL url = new URL(methodUrl);
				con = (HttpURLConnection) url.openConnection();
				//add request header
				con.setRequestMethod("POST");
				con.setRequestProperty("User-Agent", USER_AGENT);
				con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
	
				String json =  getGson().toJson(request);
	
				// Send post request
				con.setDoOutput(true);
				DataOutputStream wr = new DataOutputStream(con.getOutputStream());
				wr.writeBytes(json);
				wr.flush();
				wr.close();
		 
				int responseCode = con.getResponseCode();
				System.out.println("Sending 'POST' request to URL : " + methodUrl);
				System.out.println("Post parameters : " + json);
				System.out.println("Response Code : " + responseCode);
		 
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				T result = gson.fromJson(in, clazz);
				in.close();
		 
				return result;
			} finally {
					con.disconnect();
			}
		}
		
		private Gson getGson(){
			if(gson == null) {
				gson = new GsonBuilder().setExclusionStrategies(new JsonExclusionStrategy()).serializeNulls().create();
			}
			return gson;
		}
		
}

