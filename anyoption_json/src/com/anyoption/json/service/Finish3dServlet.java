package com.anyoption.json.service;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.service.results.TransactionMethodResult;
import com.anyoption.json.requests.ThreeDMethodRequest;


public class Finish3dServlet extends HttpServlet { 
	/**
	 * 
	 */
	private static final long serialVersionUID = 8888384809760489308L;
	public static final Logger log = Logger.getLogger(Finish3dServlet.class);

	private static StringBuffer redirectServletURL;
	private static String redirectServletTo;
	
	@Override
	public void init() {
		redirectServletURL = new StringBuffer(getServletContext().getInitParameter("redirect.servlet.url"));
		redirectServletTo = getServletContext().getInitParameter("redirect.servlet.to.suffix");
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
		super.doGet(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
		String MD = request.getParameter("MD");
		String paRes = request.getParameter("PaRes");
		String error = "e=999&t=0";
		
		String  remoteAddr = request.getHeader("X-FORWARDED-FOR");
           if (remoteAddr == null || "".equals(remoteAddr)) {
               remoteAddr = request.getRemoteAddr();
           }
		String referrer = request.getHeader("referer");
		
		log.info("request from :" + remoteAddr + " referer:" + referrer);
		if(MD != null && paRes != null) {
			try {
				MD = URLDecoder.decode(MD, "UTF-8");
				String[] data = MD.split(";");
				
				Long transactionId	= null;
				Long writerId		= null;
				String utcOffset	= null;
				try {
					transactionId = Long.parseLong(data[0]);
					writerId = Long.parseLong(data[1]);
					utcOffset = data[2];
				} catch(NumberFormatException ex) {
					log.error("Wrong parameters:", ex);
				}
				if(transactionId != null && writerId!= null && utcOffset != null && paRes != null) {
		
					ThreeDMethodRequest threeD = new ThreeDMethodRequest();
					threeD.setTransactionId(transactionId);
					threeD.setWriterId(writerId);
					threeD.setUTC(utcOffset);
					threeD.setPaRes(paRes);

					TransactionMethodResult result = AnyoptionService.purchase3dTransaction(threeD, request);
					String cp = result.getClearingProvider() == null ? "": result.getClearingProvider();

					error = "action="+redirectServletTo +
							"&e="+result.getErrorCode()+
							"&t="+transactionId+
							"&c="+URLEncoder.encode(cp, "UTF-8") +
							"&fd="+result.isFirstDeposit() +
							"&b=" + result.getBalance() +
							"&a=" + result.getAmount() +
							"&da=" + result.getDollarAmount();
					log.info("Finished 3d: "+result);
				} else {
					log.error("Unhandled request :"+MD);
				}
			} catch(Throwable t) {
				log.error("Finish3dServlet unexpected error :", t);
			}
		}
		
		StringBuffer goTo = new StringBuffer(redirectServletURL);
		goTo.append("?").append(error);
		response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
		response.setHeader("Location", goTo.toString()); 
	}
}
