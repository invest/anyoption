package com.anyoption.json.service;

import java.io.InputStream;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.ValidatorResources;
import org.apache.log4j.Logger;

import com.anyoption.common.charts.ChartHistoryCache;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.jmx.AmqJMXMonitor;
import com.anyoption.common.jmx.DbJMXMonitor;
import com.anyoption.common.jmx.JMXCacheCleanPartsJson;
import com.anyoption.common.jmx.LevelServiceJMXMonitor;
import com.anyoption.common.jmx.cassandra.JMXCassandraConnectionReset;
import com.anyoption.common.jobs.JobsScheduler;
import com.anyoption.common.managers.DBUtil;
import com.anyoption.common.managers.JobsManagerBase;
import com.anyoption.common.service.CommonJSONServiceServlet;
import com.anyoption.common.service.requests.CancelBubbleInvestmentMethodRequest;
import com.anyoption.common.service.requests.CancelInvestmentCheckboxStateMethodRequest;
import com.anyoption.common.service.requests.CancelInvestmentMethodRequest;
import com.anyoption.common.service.requests.CardMethodRequest;
import com.anyoption.common.service.requests.CcDepositMethodRequest;
import com.anyoption.common.service.requests.DepositBonusBalanceMethodRequest;
import com.anyoption.common.service.requests.InsertInvestmentMethodRequest;
import com.anyoption.common.service.requests.InsertUserMethodRequest;
import com.anyoption.common.service.requests.InsertWithdrawCancelMethodRequest;
import com.anyoption.common.service.requests.InvestmentsMethodRequest;
import com.anyoption.common.service.requests.IsKnowledgeQuestionMethodRequest;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.requests.SellDynamicsInvestmentRequest;
import com.anyoption.common.service.requests.TermsFileMethodRequest;
import com.anyoption.common.service.requests.UnblockUserMethodRequest;
import com.anyoption.common.service.requests.UpdateUserMethodRequest;
import com.anyoption.common.service.requests.UpdateUserQuestionnaireAnswersRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.requests.WithdrawBonusRegulationStateMethodRequest;
import com.anyoption.common.util.AssetIndexMarketCacheJson;
import com.anyoption.common.util.OpportunityCache;
//import com.anyoption.common.util.CloseAllBubblesOpenInvestments;
import com.anyoption.common.util.PastExpiriesCache;
import com.anyoption.json.requests.AssetIndexMethodRequest;
import com.anyoption.json.requests.BonusMethodRequest;
import com.anyoption.json.requests.CUPInfoMethodRequest;
import com.anyoption.json.requests.ChangePasswordMethodRequest;
import com.anyoption.json.requests.ChartDataMethodRequest;
import com.anyoption.json.requests.CheckUpdateMethodRequest;
import com.anyoption.json.requests.ContactMethodRequest;
import com.anyoption.json.requests.CreditCardRequest;
import com.anyoption.json.requests.CreditCardValidatorRequest;
import com.anyoption.json.requests.CurrencyMethodRequest;
import com.anyoption.json.requests.DepositReceiptMethodRequest;
import com.anyoption.json.requests.EmailMethodRequest;
import com.anyoption.json.requests.FTDUsersRequest;
import com.anyoption.json.requests.FeeMethodRequest;
import com.anyoption.json.requests.FireServerPixelMethodRequest;
import com.anyoption.json.requests.FirstEverDepositCheckRequest;
import com.anyoption.json.requests.FormsMethodRequest;
import com.anyoption.json.requests.InatecDepositFinishMethodRequest;
import com.anyoption.json.requests.InatecDepositMethodRequest;
import com.anyoption.json.requests.InsertMarketingTrackerClickRequest;
import com.anyoption.json.requests.InsurancesMethodRequest;
import com.anyoption.json.requests.MailBoxMethodRequest;
import com.anyoption.json.requests.MarketingUsersMethodRequest;
import com.anyoption.json.requests.NetellerDepositMethodRequest;
import com.anyoption.json.requests.OptionPlusMethodRequest;
import com.anyoption.json.requests.PastExpiresMethodRequest;
import com.anyoption.json.requests.PixelMethodRequest;
import com.anyoption.json.requests.RegisterAttemptMethodRequest;
import com.anyoption.json.requests.SystemChecksRequest;
import com.anyoption.json.requests.ThreeDMethodRequest;
import com.anyoption.json.requests.TransactionMethodRequest;
import com.anyoption.json.requests.UserRegulationRequest;
import com.anyoption.json.requests.WireMethodRequest;
import com.anyoption.json.results.NetellerDepositMethodResult;
import com.anyoption.json.service.requests.BubblesGetDataMethodRequest;
import com.anyoption.json.service.requests.InsertBubbleInvestmentMethodRequest;
import com.anyoption.json.service.requests.SettleBubbleInvestmentMethodRequest;
import com.anyoption.json.util.JMXCacheClean;
import com.anyoption.json.util.PixelHitter;
import com.anyoption.managers.ClearingManager;
import com.anyoption.managers.PopulationsManagerBase;
import com.anyoption.population.PopulationHandlersException;
import com.anyoption.util.CommonUtil;
import com.copyop.common.managers.ConfigurationCopyopManager;
import com.copyop.common.managers.ManagerBase;
import com.copyop.json.requests.AcceptedTermsAndConditions;
import com.copyop.json.requests.CopyStepsEncourageMethodRequest;
import com.copyop.json.requests.CopyopHotDataMethodRequest;
import com.copyop.json.requests.OriginalInvProfileMethodRequest;
import com.copyop.json.requests.PagingUserMethodRequest;
import com.copyop.json.requests.ProfileMethodRequest;
import com.copyop.json.requests.ShowOffMethodRequest;
import com.copyop.json.requests.UpdateAvatarMethodRequest;
import com.copyop.json.requests.UpdateFacebookFriendsMethodRequest;
import com.copyop.json.requests.WatchUserConfigMethodRequest;

/**
 * @author Kobi
 */
public class AnyoptionServiceServlet extends CommonJSONServiceServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger.getLogger(AnyoptionServiceServlet.class);

//	private static Hashtable<String, Method> methods;
	private static ValidatorResources validatorResources;
//	private static ChartsUpdater chartsUpdater;
//	private static LevelHistoryCache levelHistoryCache;
//	private static ChartHistoryCache chartHistoryCache;
//	private static OpportunityCache opportunityCache;
//	private static String webLevelsCacheJMXName;
//	private static WebLevelsCache levelsCache;
	private static PixelHitter pixelHitter;
//	private static PastExpiriesCache expiriesCache;
	//private static CloseAllBubblesOpenInvestments closeAllBubblesOpenInv;

	private static JMXCacheClean jmxCacheClean;
	private static JMXCacheCleanPartsJson jmxCacheCleanParts;
	
	private static JMXCassandraConnectionReset jmxCassandraReset;
	private static AmqJMXMonitor amqMonitor; 
	private static DbJMXMonitor dbMonitor;
	public static String REDIRECT_SERVICE_SUFFIX;

	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 */
	@Override
	public void init() {
    	log.info("com.anyoption.json.service.AnyoptionServiceServlet starting...");
    	super.init();

        InputStream in = null;
        try {
            in = this.getClass().getClassLoader().getResourceAsStream("validation.xml");
            validatorResources = new ValidatorResources(in);
        } catch (Exception e) {
            log.error("Can't load ValidatorResources.", e);
        } finally {
            if (null != in) {
                try {
                    in.close();
                } catch (Exception e) {
                    log.error("Can't close InputStream", e);
                }
            }
        }

//        HashMap<String, Object> objectShare = null;
//        try {
//            Context initCtx = new InitialContext();
//            Context envCtx = (Context) initCtx.lookup("java:comp/env");
//            objectShare = (HashMap<String, Object>) envCtx.lookup("anyoption/share");
//            log.info("Lookedup object share: " + objectShare);
//        } catch (Exception e) {
//            log.fatal("Cannot lookup object share", e);
//        }

        try {
            ClearingManager.loadClearingProviders();
        } catch (Throwable t) {
            log.error("Failed to load clearing providers.", t);
        }

//        if (null != objectShare) {
//            chartsUpdater = (ChartsUpdater) objectShare.get(ConstantsBase.OBJECT_SHARE_CHARTS_UPDATER);
//            log.info("Got from share chartsUpdater: " + chartsUpdater);
//        }
//        if (null == chartsUpdater) {
//            log.info("Creating new chartsUpdater.");
//            chartsUpdater = new ChartsUpdater();
//            objectShare.put(ConstantsBase.OBJECT_SHARE_CHARTS_UPDATER, chartsUpdater);
//        }
//
//        if (null != objectShare) {
//            levelHistoryCache = (LevelHistoryCache) objectShare.get(ConstantsBase.OBJECT_SHARE_LEVELS_HISTORY_CACHE);
//            log.info("Got from share levelHistoryCache: " + levelHistoryCache);
//        }
//        if (null == levelHistoryCache) {
//            log.info("Creating new levelHistoryCache");
//            levelHistoryCache = new LevelHistoryCache();
//            chartsUpdater.addListener(levelHistoryCache);
//            objectShare.put(ConstantsBase.OBJECT_SHARE_LEVELS_HISTORY_CACHE, levelHistoryCache);
//        }
//
//        if (null != objectShare) {
//            chartHistoryCache = (ChartHistoryCache) objectShare.get(ConstantsBase.OBJECT_SHARE_CHARTS_HISTORY_CACHE);
//            log.info("Got from share chartHistoryCache: " + chartHistoryCache);
//        }
//        if (null == chartHistoryCache) {
//            log.info("Creating new chartHistoryCache");
//            chartHistoryCache = new ChartHistoryCache();
//            chartsUpdater.addListener(chartHistoryCache);
//            objectShare.put(ConstantsBase.OBJECT_SHARE_CHARTS_HISTORY_CACHE, chartHistoryCache);
//        }

        if (null == pixelHitter) {
        	pixelHitter = new PixelHitter();
        	pixelHitter.start();
        }
//
//        opportunityCache = new OpportunityCache();
//        expiriesCache = new PastExpiriesCache();
        //closeAllBubblesOpenInv = new CloseAllBubblesOpenInvestments();

        ServletContext servletContext = getServletContext();
//        try {
//            String initialContextFactory = servletContext.getInitParameter("il.co.etrader.service.level.INITIAL_CONTEXT_FACTORY");
//            String providerURL = servletContext.getInitParameter("il.co.etrader.service.level.PROVIDER_URL");
//            String connectionFactory = servletContext.getInitParameter("il.co.etrader.service.level.CONNECTION_FACTORY_NAME");
//            String topic = servletContext.getInitParameter("il.co.etrader.service.level.TOPIC_NAME");
//            String queue = servletContext.getInitParameter("il.co.etrader.service.level.QUEUE_NAME");
//            String msgPoolSizeStr = servletContext.getInitParameter("il.co.etrader.service.level.MSG_POOL_SIZE");
//            String recoveryPauseStr = servletContext.getInitParameter("il.co.etrader.service.level.RECOVERY_PAUSE");
//            int msgPoolSize = 15;
//            try {
//                msgPoolSize = Integer.parseInt(msgPoolSizeStr);
//            } catch (Throwable t) {
//                log.warn("Can't parse msgPoolSize. Set to 15.", t);
//            }
//            int recoveryPause = 2000;
//            try {
//                recoveryPause = Integer.parseInt(recoveryPauseStr);
//            } catch (Throwable t) {
//                log.warn("Can't parse recoveryPause. Set to 2000.", t);
//            }
//            webLevelsCacheJMXName = servletContext.getInitParameter("il.co.etrader.service.level.JMX_NAME");
//            if (log.isDebugEnabled()) {
//                String ls = System.getProperty("line.separator");
//                log.debug(ls + "WebLevelsCache Configuration:" + ls +
//                        "initialContextFactory: " + initialContextFactory + ls +
//                        "providerURL: " + providerURL + ls +
//                        "connectionFactory: " + connectionFactory + ls +
//                        "topic: " + topic + ls +
//                        "queue: " + queue + ls +
//                        "msgPoolSize: " + msgPoolSize + ls +
//                        "recoveryPause: " + recoveryPause + ls +
//                        "jmxName: " + webLevelsCacheJMXName + ls);
//            }
//
//            levelsCache = new WebLevelsCache(initialContextFactory, providerURL, connectionFactory, queue, topic, msgPoolSize, recoveryPause, null);
//            levelsCache.registerJMX(webLevelsCacheJMXName);
//            if (log.isInfoEnabled()) {
//                log.info("WebLevelsCahce bound to the context");
//            }
//        } catch (Exception e) {
//            log.error("Can't start WebLevelsCache.", e);
//        }

        String runJobs = servletContext.getInitParameter(JobsManagerBase.CONTEXT_PARAM_RUN_JOBS);
        if (null != runJobs && runJobs.equalsIgnoreCase("true")) {
        	String application = servletContext.getInitParameter(JobsManagerBase.CONTEXT_PARAM_RUN_APPLICATION);
            JobsScheduler js = new JobsScheduler(application);
            js.start();
            servletContext.setAttribute(JobsManagerBase.CONTEXT_PARAM_RUN_JOBS, js);
        }
        REDIRECT_SERVICE_SUFFIX =  getServletContext().getInitParameter("redirect.servlet.suffix");

        try {
        	AssetIndexMarketCacheJson.init();
        } catch (Exception e) {
            log.error("Can't load Asset Index  Cache.", e);
        }

//		methods = new Hashtable<String, Method>();
		try {
			Class<?> serviceClass = Class.forName("com.anyoption.json.service.AnyoptionService");
			methods.put("getUser", serviceClass.getMethod("getUser", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
			methods.put("insertUser", serviceClass.getMethod("insertUser", new Class[] {InsertUserMethodRequest.class, HttpServletRequest.class}));
			methods.put("getCurrenciesOptions", serviceClass.getMethod("getCurrenciesOptions", new Class[] {MethodRequest.class}));
			methods.put("getSkinCurrenciesOptions", serviceClass.getMethod("getSkinCurrenciesOptions", new Class[] {MethodRequest.class}));
			methods.put("getBonusStatesOptions", serviceClass.getMethod("getBonusStatesOptions", new Class[] {MethodRequest.class}));
			methods.put("getCountriesOptions", serviceClass.getMethod("getCountriesOptions", new Class[] {MethodRequest.class}));
			methods.put("getLanguagesOptions", serviceClass.getMethod("getLanguagesOptions", new Class[] {MethodRequest.class}));
			methods.put("getCreditCardTypesOptions", serviceClass.getMethod("getCreditCardTypesOptions", new Class[] {MethodRequest.class}));
			methods.put("getSkinsOptions", serviceClass.getMethod("getSkinsOptions", new Class[] {MethodRequest.class}));
			methods.put("updateUser", serviceClass.getMethod("updateUser", new Class[] {UpdateUserMethodRequest.class, HttpServletRequest.class}));
			methods.put("getUserTransactions", serviceClass.getMethod("getUserTransactions", new Class[] {FormsMethodRequest.class, HttpServletRequest.class}));
			methods.put("getChartData", serviceClass.getMethod("getChartData", new Class[] {ChartDataMethodRequest.class, OpportunityCache.class, ChartHistoryCache.class, WebLevelsCache.class, PastExpiriesCache.class, HttpServletRequest.class}));
            methods.put("getChartDataPreviousHour", serviceClass.getMethod("getChartDataPreviousHour", new Class[] {ChartDataMethodRequest.class, OpportunityCache.class, ChartHistoryCache.class}));
//            methods.put("getChartDataCommon", serviceClass.getMethod("getChartDataCommon", new Class[] {ChartDataRequest.class, OpportunityCache.class, ChartHistoryCache.class, WebLevelsCache.class, HttpServletRequest.class}));
            methods.put("getStatesOptions", serviceClass.getMethod("getStatesOptions", new Class[] {MethodRequest.class}));
            methods.put("getPastExpiries", serviceClass.getMethod("getPastExpiries", new Class[] {PastExpiresMethodRequest.class, PastExpiriesCache.class}));
            methods.put("insertCard", serviceClass.getMethod("insertCard", new Class[] {CardMethodRequest.class, HttpServletRequest.class}));
            methods.put("updateCard", serviceClass.getMethod("updateCard", new Class[] {CardMethodRequest.class, HttpServletRequest.class}));
            methods.put("getSkinIdByIp", serviceClass.getMethod("getSkinIdByIp", new Class[] {MethodRequest.class}));
            methods.put("getUserInvestments", serviceClass.getMethod("getUserInvestments", new Class[] {InvestmentsMethodRequest.class, HttpServletRequest.class}));
            methods.put("getAssetIndexByMarket", serviceClass.getMethod("getAssetIndexByMarket", new Class[] {AssetIndexMethodRequest.class}));
			methods.put("getUserEncryptedPass", serviceClass.getMethod("getUserEncryptedPass", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
			methods.put("getChangePassword", serviceClass.getMethod("getChangePassword", new Class[] {ChangePasswordMethodRequest.class, HttpServletRequest.class}));
			methods.put("optionPlusFlashSell", serviceClass.getMethod("optionPlusFlashSell", new Class[] {OptionPlusMethodRequest.class, WebLevelsCache.class, HttpServletRequest.class}));
			methods.put("getPrice", serviceClass.getMethod("getPrice", new Class[] {OptionPlusMethodRequest.class, WebLevelsCache.class, OpportunityCache.class, HttpServletRequest.class}));
			methods.put("insertInvestment", serviceClass.getMethod("insertInvestment", new Class[] {InsertInvestmentMethodRequest.class, OpportunityCache.class, HttpServletRequest.class}));
			methods.put("insertContact", serviceClass.getMethod("insertContact", new Class[] {ContactMethodRequest.class}));
			methods.put("getMarketList", serviceClass.getMethod("getMarketList", new Class[] {MethodRequest.class}));
			methods.put("getUserBonuses", serviceClass.getMethod("getUserBonuses", new Class[] {BonusMethodRequest.class, HttpServletRequest.class}));
			methods.put("getAvailableCreditCards", serviceClass.getMethod("getAvailableCreditCards", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
			methods.put("getAvailableCreditCardsWithEmptyLine", serviceClass.getMethod("getAvailableCreditCardsWithEmptyLine", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
			methods.put("insertDepositCard", serviceClass.getMethod("insertDepositCard", new Class[] {CcDepositMethodRequest.class, HttpServletRequest.class}));
			methods.put("insertWithdrawsReverse", serviceClass.getMethod("insertWithdrawsReverse", new Class[] {TransactionMethodRequest.class, HttpServletRequest.class}));
			methods.put("getWithdrawCCList", serviceClass.getMethod("getWithdrawCCList", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
			methods.put("checkForUpdate", serviceClass.getMethod("checkForUpdate", new Class[] {CheckUpdateMethodRequest.class, HttpServletRequest.class}));
			methods.put("c2dmUpdate", serviceClass.getMethod("c2dmUpdate", new Class[] {CheckUpdateMethodRequest.class}));
            methods.put("skinUpdate", serviceClass.getMethod("skinUpdate", new Class[] {CheckUpdateMethodRequest.class, HttpServletRequest.class}));
            methods.put("insertWithdrawBank", serviceClass.getMethod("insertWithdrawBank", new Class[] {WireMethodRequest.class, HttpServletRequest.class}));
            methods.put("insertWithdrawCard", serviceClass.getMethod("insertWithdrawCard", new Class[] {CcDepositMethodRequest.class, HttpServletRequest.class}));
            methods.put("getUserInboxMails", serviceClass.getMethod("getUserInboxMails", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("sendPassword", serviceClass.getMethod("sendPassword", new Class[] {InsertUserMethodRequest.class, HttpServletRequest.class}));
            methods.put("updateMailBox", serviceClass.getMethod("updateMailBox", new Class[] {MailBoxMethodRequest.class, HttpServletRequest.class}));
            methods.put("insertRegisterAttempt", serviceClass.getMethod("insertRegisterAttempt", new Class[] {RegisterAttemptMethodRequest.class}));
            methods.put("getCountryId", serviceClass.getMethod("getCountryId", new Class[] {MethodRequest.class}));
            methods.put("getCurrencyLetters", serviceClass.getMethod("getCurrencyLetters", new Class[] {CurrencyMethodRequest.class}));
            methods.put("SendDepositReceipt", serviceClass.getMethod("SendDepositReceipt", new Class[] {DepositReceiptMethodRequest.class, HttpServletRequest.class}));
            methods.put("buyInsurances", serviceClass.getMethod("buyInsurances", new Class[] {InsurancesMethodRequest.class, WebLevelsCache.class, OpportunityCache.class, HttpServletRequest.class}));
            methods.put("insuranceBannerSeen", serviceClass.getMethod("insuranceBannerSeen", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("validateWithdrawBank", serviceClass.getMethod("validateWithdrawBank", new Class[] {WireMethodRequest.class, HttpServletRequest.class}));
            methods.put("validateWithdrawCard", serviceClass.getMethod("validateWithdrawCard", new Class[] {CcDepositMethodRequest.class, HttpServletRequest.class}));
            methods.put("updateBonusStatus", serviceClass.getMethod("updateBonusStatus", new Class[] {BonusMethodRequest.class, HttpServletRequest.class}));
            methods.put("getCitiesOptions", serviceClass.getMethod("getCitiesOptions", new Class[] {MethodRequest.class}));
            methods.put("getBanksET", serviceClass.getMethod("getBanksET", new Class[] {MethodRequest.class}));
//            methods.put("getDeltapayInfo", serviceClass.getMethod("getDeltapayInfo", new Class[] {DeltapayInfoMethodRequest.class, HttpServletRequest.class}));
            methods.put("insertWithdrawDeltapay", serviceClass.getMethod("insertWithdrawDeltapay", new Class[] {CcDepositMethodRequest.class, HttpServletRequest.class}));
            methods.put("getFeeAmountByType", serviceClass.getMethod("getFeeAmountByType", new Class[] {FeeMethodRequest.class, HttpServletRequest.class}));
//            methods.put("insertBaropayDeposit", serviceClass.getMethod("insertBaropayDeposit", new Class[] {BaropayDepositMethodRequest.class, HttpServletRequest.class}));
            methods.put("insertWithdrawBaropay", serviceClass.getMethod("insertWithdrawBaropay", new Class[] {WireMethodRequest.class, HttpServletRequest.class}));
//            methods.put("getCdpayInfo", serviceClass.getMethod("getCdpayInfo", new Class[] {DeltapayInfoMethodRequest.class, HttpServletRequest.class}));
            methods.put("getCUPInfo", serviceClass.getMethod("getCUPInfo", new Class[] {CUPInfoMethodRequest.class, HttpServletRequest.class}));
            methods.put("checkBaroPayWithdrawAvailable", serviceClass.getMethod("checkBaroPayWithdrawAvailable", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("validateBaroPayWithdraw", serviceClass.getMethod("validateBaroPayWithdraw", new Class[] {WireMethodRequest.class, HttpServletRequest.class}));
            methods.put("getMaxCupWithdrawal", serviceClass.getMethod("getMaxCupWithdrawal", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("getUserSingleQuestionnaire", serviceClass.getMethod("getUserSingleQuestionnaire", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("updateMandatoryQuestionnaireDone", serviceClass.getMethod("updateMandatoryQuestionnaireDone", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("updateOptionalQuestionnaireDone", serviceClass.getMethod("updateOptionalQuestionnaireDone", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("updateCapitalQuestionnaireDone", serviceClass.getMethod("updateCapitalQuestionnaireDone", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("updateSingleQuestionnaireDone", serviceClass.getMethod("updateSingleQuestionnaireDone", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("getUserMandatoryQuestionnaire", serviceClass.getMethod("getUserMandatoryQuestionnaire", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("getUserOptionalQuestionnaire", serviceClass.getMethod("getUserOptionalQuestionnaire", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("getUserCapitalQuestionnaire", serviceClass.getMethod("getUserCapitalQuestionnaire", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("updateUserQuestionnaireAnswers", serviceClass.getMethod("updateUserQuestionnaireAnswers", new Class[] {UpdateUserQuestionnaireAnswersRequest.class, HttpServletRequest.class}));
            methods.put("getMinFirstDeposit", serviceClass.getMethod("getMinFirstDeposit", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("insertMarketingTrackerClick", serviceClass.getMethod("insertMarketingTrackerClick", new Class[] {InsertMarketingTrackerClickRequest.class}));
            methods.put("validateCC", serviceClass.getMethod("validateCC", new Class[]{CreditCardValidatorRequest.class}));
            methods.put("updateUserAdditionalFields", serviceClass.getMethod("updateUserAdditionalFields", new Class[]{UpdateUserMethodRequest.class, HttpServletRequest.class}));
            methods.put("getOpenHourlyBinaryMarkets", serviceClass.getMethod("getOpenHourlyBinaryMarkets", new Class[]{MethodRequest.class, WebLevelsCache.class}));
            methods.put("getCurrentHourlyOppByMarket", serviceClass.getMethod("getCurrentHourlyOppByMarket", new Class[]{ChartDataMethodRequest.class}));
            methods.put("insertLoginToken", serviceClass.getMethod("insertLoginToken", new Class[]{UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("getCurrentLevelByOpportunityId", serviceClass.getMethod("getCurrentLevelByOpportunityId", new Class[]{ChartDataMethodRequest.class}));
            methods.put("getFirstEverDepositCheck", serviceClass.getMethod("getFirstEverDepositCheck", new Class[]{FirstEverDepositCheckRequest.class}));
            methods.put("getBankWireWithdrawOptions", serviceClass.getMethod("getBankWireWithdrawOptions", new Class[] {MethodRequest.class}));
            methods.put("getAllReturnOdds", serviceClass.getMethod("getAllReturnOdds", new Class[]{MethodRequest.class}));
            methods.put("getBinaryHomePageMarkets", serviceClass.getMethod("getBinaryHomePageMarkets", new Class[]{MethodRequest.class, WebLevelsCache.class}));
            methods.put("deleteCreditCard", serviceClass.getMethod("deleteCreditCard", new Class[]{CreditCardRequest.class, HttpServletRequest.class}));
            methods.put("getSkinCurrencies", serviceClass.getMethod("getSkinCurrencies", new Class[]{MethodRequest.class}));
            methods.put("sendBankTransferDetailsUser", serviceClass.getMethod("sendBankTransferDetailsUser", new Class[]{UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("getLastCurrenciesRate", serviceClass.getMethod("getLastCurrenciesRate", new Class[]{ MethodRequest.class}));
            methods.put("getLogoutBalanceStep", serviceClass.getMethod("getLogoutBalanceStep", new Class[]{ MethodRequest.class}));
            methods.put("getFTDUsersByDate", serviceClass.getMethod("getFTDUsersByDate", new Class[]{ FTDUsersRequest.class}));
            methods.put("logoutUser", serviceClass.getMethod("logoutUser", new Class[]{UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("getTrackingPixel", serviceClass.getMethod("getTrackingPixel", new Class[]{PixelMethodRequest.class, HttpServletRequest.class}));
            methods.put("insertDirectDeposit", serviceClass.getMethod("insertDirectDeposit", new Class[]{InatecDepositMethodRequest.class, HttpServletRequest.class}));
            methods.put("finishDirectDeposit", serviceClass.getMethod("finishDirectDeposit", new Class[]{InatecDepositFinishMethodRequest.class, HttpServletRequest.class}));
            methods.put("insertKnowledgeConfirm", serviceClass.getMethod("insertKnowledgeConfirm", new Class[]{UserRegulationRequest.class, HttpServletRequest.class}));
            methods.put("unsuspendUserRequest", serviceClass.getMethod("unsuspendUserRequest", new Class[]{UserMethodRequest.class, HttpServletRequest.class}));
           //methods.put("unrestrictUserRequest", serviceClass.getMethod("unrestrictUserRequest", new Class[]{UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("getData", serviceClass.getMethod("getData", new Class[]{BubblesGetDataMethodRequest.class, WebLevelsCache.class}));
            methods.put("insertBubbleInvestment", serviceClass.getMethod("insertBubbleInvestment", new Class[]{InsertBubbleInvestmentMethodRequest.class, OpportunityCache.class, HttpServletRequest.class}));
            methods.put("settleBubbleInvestment", serviceClass.getMethod("settleBubbleInvestment", new Class[]{SettleBubbleInvestmentMethodRequest.class, HttpServletRequest.class}));
            methods.put("getMarketingUsers", serviceClass.getMethod("getMarketingUsers", new Class[]{MarketingUsersMethodRequest.class}));
            methods.put("isWithdrawalWeekendsAvailable", serviceClass.getMethod("isWithdrawalWeekendsAvailable", new Class[]{UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("getUserDepositBonusBalance", serviceClass.getMethod("getUserDepositBonusBalance", new Class[]{DepositBonusBalanceMethodRequest.class, HttpServletRequest.class}));            
            methods.put("updateIsKnowledgeQuestion", serviceClass.getMethod("updateIsKnowledgeQuestion", new Class[]{IsKnowledgeQuestionMethodRequest.class, HttpServletRequest.class}));
            methods.put("retrieveUserDocuments", serviceClass.getMethod("retrieveUserDocuments", new Class[]{UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("fireServerPixel", serviceClass.getMethod("fireServerPixel", new Class[]{FireServerPixelMethodRequest.class, HttpServletRequest.class}));
            methods.put("getWithdrawBonusRegulationState", serviceClass.getMethod("getWithdrawBonusRegulationState", new Class[]{WithdrawBonusRegulationStateMethodRequest.class, HttpServletRequest.class}));
            methods.put("unblockUserForThreshold", serviceClass.getMethod("unblockUserForThreshold", new Class[]{UnblockUserMethodRequest.class, HttpServletRequest.class}));
            methods.put("insertDynamicsInvestment", serviceClass.getMethod("insertDynamicsInvestment", new Class[]{InsertInvestmentMethodRequest.class, OpportunityCache.class, HttpServletRequest.class}));
            methods.put("sellDynamicsInvestment", serviceClass.getMethod("sellDynamicsInvestment", new Class[]{SellDynamicsInvestmentRequest.class, OpportunityCache.class, HttpServletRequest.class}));
            methods.put("sellAllDynamicsInvestments", serviceClass.getMethod("sellAllDynamicsInvestments", new Class[]{SellDynamicsInvestmentRequest.class, OpportunityCache.class, HttpServletRequest.class}));
            methods.put("insertWithdrawCancel", serviceClass.getMethod("insertWithdrawCancel", new Class[]{InsertWithdrawCancelMethodRequest.class, HttpServletRequest.class}));
        	methods.put("getForgetPasswordContacts", serviceClass.getMethod("getForgetPasswordContacts", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
        	methods.put("getTermsFiles", serviceClass.getMethod("getTermsFiles", new Class[] {TermsFileMethodRequest.class}));
        	methods.put("cancelInvestment", serviceClass.getMethod("cancelInvestment",new Class[] {CancelInvestmentMethodRequest.class , OpportunityCache.class , WebLevelsCache.class , HttpServletRequest.class }));
        	methods.put("changeCancelInvestmentCheckboxState", serviceClass.getMethod("changeCancelInvestmentCheckboxState", new Class[] {CancelInvestmentCheckboxStateMethodRequest.class, HttpServletRequest.class}));
            methods.put("cancelBubbleInvestment", serviceClass.getMethod("cancelBubbleInvestment", new Class[] {CancelBubbleInvestmentMethodRequest.class, HttpServletRequest.class, WebLevelsCache.class}));
            methods.put("getVisibleSkins", serviceClass.getMethod("getVisibleSkins", new Class[] {MethodRequest.class, HttpServletRequest.class}));
            methods.put("getUserSingleQuestionnaireDynamic", serviceClass.getMethod("getUserSingleQuestionnaireDynamic", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("insertContactMailForm", serviceClass.getMethod("insertContactMailForm", new Class[] {ContactMethodRequest.class, HttpServletRequest.class}));
            methods.put("finish3dTransaction", serviceClass.getMethod("finish3dTransaction", new Class[] {ThreeDMethodRequest.class, HttpServletRequest.class}));
            methods.put("insertNetellerDeposit", serviceClass.getMethod("insertNetellerDeposit", new Class[] {NetellerDepositMethodRequest.class, HttpServletRequest.class}));
            
            Class<?> copyopServiceClass = Class.forName("com.copyop.json.service.CopyopService");
            methods.put("getCopyopUser", copyopServiceClass.getMethod("getCopyopUser", new Class[] {com.anyoption.common.service.requests.UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("insertCopyopProfile", copyopServiceClass.getMethod("insertCopyopProfile", new Class[] {com.copyop.json.requests.InsertUserMethodRequest.class, HttpServletRequest.class}));
            methods.put("acceptedTermsAndConditions", copyopServiceClass.getMethod("acceptedTermsAndConditions", new Class[] {AcceptedTermsAndConditions.class, HttpServletRequest.class}));
            methods.put("getCopyopWatchers", copyopServiceClass.getMethod("getCopyopWatchers", new Class[] {com.copyop.json.requests.WatchListMethodRequest.class, HttpServletRequest.class}));
            methods.put("getCopyopWatching", copyopServiceClass.getMethod("getCopyopWatching", new Class[] {com.copyop.json.requests.WatchListMethodRequest.class, HttpServletRequest.class}));
            methods.put("getCopyopNews", copyopServiceClass.getMethod("getCopyopNews", new Class[] {com.anyoption.common.service.requests.UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("copyopExplore", copyopServiceClass.getMethod("copyopExplore", new Class[] {com.anyoption.common.service.requests.UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("getCopyopHotTraders", copyopServiceClass.getMethod("getCopyopHotTraders", new Class[] {CopyopHotDataMethodRequest.class, HttpServletRequest.class}));
            methods.put("getCopyopAssetSpecialists", copyopServiceClass.getMethod("getCopyopAssetSpecialists", new Class[] {CopyopHotDataMethodRequest.class, HttpServletRequest.class}));
            methods.put("getCopyopHotTrades", copyopServiceClass.getMethod("getCopyopHotTrades", new Class[] {CopyopHotDataMethodRequest.class, HttpServletRequest.class}));
            methods.put("getCopyopHotCopiers", copyopServiceClass.getMethod("getCopyopHotCopiers", new Class[] {CopyopHotDataMethodRequest.class, HttpServletRequest.class}));
            methods.put("copyopFollow", copyopServiceClass.getMethod("copyopFollow", new Class[] {com.copyop.json.requests.InsertInvestmentMethodRequest.class, OpportunityCache.class, HttpServletRequest.class}));
            methods.put("copyopCopyUserConfig", copyopServiceClass.getMethod("copyopCopyUserConfig", new Class[] {com.copyop.json.requests.CopyUserConfigMethodRequest.class, HttpServletRequest.class}));
            methods.put("changeCopyopNickname", copyopServiceClass.getMethod("changeCopyopNickname", new Class[] {com.copyop.json.requests.ChangeNicknameMethodRequest.class, HttpServletRequest.class}));
            methods.put("getCopyopProfile", copyopServiceClass.getMethod("getCopyopProfile", new Class[] {ProfileMethodRequest.class, HttpServletRequest.class}));
            methods.put("getCopyopOriginalInvProfile", copyopServiceClass.getMethod("getCopyopOriginalInvProfile", new Class[] {OriginalInvProfileMethodRequest.class, HttpServletRequest.class}));
            methods.put("myCopyopProfile", copyopServiceClass.getMethod("myCopyopProfile", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("getCopyopCopiers", copyopServiceClass.getMethod("getCopyopCopiers", new Class[] {ProfileMethodRequest.class, HttpServletRequest.class}));
            methods.put("getCopyopCopying", copyopServiceClass.getMethod("getCopyopCopying", new Class[] {ProfileMethodRequest.class, HttpServletRequest.class}));
            methods.put("getCopyopHits", copyopServiceClass.getMethod("getCopyopHits", new Class[] {ProfileMethodRequest.class, HttpServletRequest.class}));
            methods.put("getCopyopCopyUserConfig", copyopServiceClass.getMethod("getCopyopCopyUserConfig", new Class[] {ProfileMethodRequest.class, HttpServletRequest.class}));
            methods.put("getCopyopProfileBestAssets", copyopServiceClass.getMethod("getCopyopProfileBestAssets", new Class[] {ProfileMethodRequest.class, HttpServletRequest.class}));
            methods.put("copyopWatchConfig", copyopServiceClass.getMethod("copyopWatchConfig", new Class[] {WatchUserConfigMethodRequest.class, HttpServletRequest.class}));
            methods.put("selectSkin", copyopServiceClass.getMethod("selectSkin", new Class[] {MethodRequest.class, HttpServletRequest.class}));
            methods.put("getCopyopProfileCoins", copyopServiceClass.getMethod("getCopyopProfileCoins", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("depositCopyopCoins", copyopServiceClass.getMethod("depositCopyopCoins", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("updateFacebookFriends", copyopServiceClass.getMethod("updateFacebookFriends", new Class[] {UpdateFacebookFriendsMethodRequest.class, HttpServletRequest.class}));
            methods.put("getFacebookFriends", copyopServiceClass.getMethod("getFacebookFriends", new Class[] {PagingUserMethodRequest.class, HttpServletRequest.class}));
            methods.put("updateCopyopProfileAvatar", copyopServiceClass.getMethod("updateCopyopProfileAvatar", new Class[] {UpdateAvatarMethodRequest.class, HttpServletRequest.class}));
            methods.put("getLastViewedUsers", copyopServiceClass.getMethod("getLastViewedUsers", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("getCopyopWatchUserConfig", copyopServiceClass.getMethod("getCopyopWatchUserConfig", new Class[] {ProfileMethodRequest.class, HttpServletRequest.class}));
            methods.put("shareCopyop", copyopServiceClass.getMethod("shareCopyop", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("rateCopyop", copyopServiceClass.getMethod("rateCopyop", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("getCopyopInbox", copyopServiceClass.getMethod("getCopyopInbox", new Class[] {PagingUserMethodRequest.class, HttpServletRequest.class}));
            methods.put("copyopShowOff", copyopServiceClass.getMethod("copyopShowOff", new Class[] {ShowOffMethodRequest.class, HttpServletRequest.class}));
            methods.put("getSystemChecks", copyopServiceClass.getMethod("getSystemChecks", new Class[] {SystemChecksRequest.class, HttpServletRequest.class}));
            methods.put("getLastLoginSummary", copyopServiceClass.getMethod("getLastLoginSummary", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("sendAffiliateEmail", copyopServiceClass.getMethod("sendAffiliateEmail", new Class[] {EmailMethodRequest.class}));
            methods.put("getCopyStepsEncourageToolTip", copyopServiceClass.getMethod("getCopyStepsEncourageToolTip", new Class[] {CopyStepsEncourageMethodRequest.class, HttpServletRequest.class}));
            methods.put("getFavMarkets", copyopServiceClass.getMethod("getFavMarkets", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("getUserHolidayMessage", copyopServiceClass.getMethod("getUserHolidayMessage", new Class[] {MethodRequest.class}));
		} catch (Exception e) {
			log.fatal("Cannot load service methods! ", e);
		}

		//load the population handlers
		try {
			log.debug("Starting to load population handlers");
			PopulationsManagerBase.loadPopulatioHandlers();
		} catch (PopulationHandlersException pe) {
			log.error("can't load Population handlers" + pe);
		}
//
//		CurrencyRatesManagerBase.loadInvestmentRartesCache();

		try {
			String jmxCacheCleanJMXName = servletContext.getInitParameter("com.anyoption.json.util.JMX_NAME");
			jmxCacheClean = new JMXCacheClean();
			jmxCacheClean.registerJMX(jmxCacheCleanJMXName);
        } catch (Exception e) {
            log.error("Can't register jmxCacheClean.", e);
        }

		try {
			String jmxCacheCleanJMXName = servletContext.getInitParameter("com.anyoption.common.jmx.JMX_NAME");
			jmxCacheCleanParts = new JMXCacheCleanPartsJson();
			jmxCacheCleanParts.registerJMX(jmxCacheCleanJMXName);
        } catch (Exception e) {
            log.error("Can't register jmxCacheClean.", e);
        }

		String contactPointsStr = servletContext.getInitParameter("com.copyop.nodes.CONTACT_POINTS");
		if (contactPointsStr != null) {
			try {
				String[] contactPoints = contactPointsStr.split(",");
				String keyspace = servletContext.getInitParameter("com.copyop.session.keyspace");
				ManagerBase.init(contactPoints, keyspace);
			} catch (Exception e) {
				log.error("Can't initialize " + ManagerBase.class, e);
			}
			try {
				String jmxCassandraResetJMXName = servletContext.getInitParameter("com.anyoption.common.jmx.cassandra.JMX_NAME");
				jmxCassandraReset = new JMXCassandraConnectionReset();
				jmxCassandraReset.registerJMX(jmxCassandraResetJMXName);
			} catch (Exception e) {
				log.error("Cannot register jmxCassandraReset.", e);
			}
		}
		
		String monitoring = servletContext.getInitParameter("com.anyoption.monitoring.suffix");
		LevelServiceJMXMonitor ljm = new LevelServiceJMXMonitor(levelsCache, monitoring);
        ljm.registerJMX();
        
        amqMonitor = new AmqJMXMonitor(monitoring);
        AmqJMXMonitor.initConnectionFactory();
        amqMonitor.registerJMX();

		try {
			dbMonitor = new DbJMXMonitor(DBUtil.getDataSource(), monitoring);
			dbMonitor.registerJMX();
		} catch (SQLException e1) {
			log.error("Can't register db monitor", e1);
		}
        

        try {
        	if((CommonUtil.getProperty("copyop.isEnabled").equalsIgnoreCase("true"))) {
        		ConfigurationCopyopManager.loadCopyopCfg();
        	} else {
        		log.debug("Copyop is not enable and not will load cfg");
        	}
        } catch (Exception e) {
            log.error("When load copyop config", e);
        }
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
		super.doGet(request, response);
	}

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    	super.doPost(request, response);
//        String uri = request.getRequestURI();
//        String methodReq = uri.substring(uri.lastIndexOf("/") + 1);
//        log.debug("URI requested: " + uri + " Method: " + methodReq + " sessionId: " + request.getSession().getId());
//
//        if (log.isTraceEnabled()) {
//        	Enumeration<String> headerNames = request.getHeaderNames();
//        	String headerName = null;
//        	String ls = System.getProperty("line.separator");
//        	StringBuffer sb = new StringBuffer(ls);
//        	while (headerNames.hasMoreElements()) {
//        		headerName = headerNames.nextElement();
//        		sb.append(headerName).append(": ").append(request.getHeader(headerName)).append(ls);
//        	}
//        	log.trace(sb.toString());
//        }
//
//        Object result = null;
//        try {
//	        Method m = methods.get(methodReq);
//	        if (null != m) {
//	        	Gson gson = new GsonBuilder().setExclusionStrategies(new JsonExclusionStrategy()).serializeNulls().create();
//	        	Class[] params = m.getParameterTypes();
//	        	// deserialize the JSON request from client
//                Object[] requestParams = new Object[params.length];
//                requestParams[0] = gson.fromJson(new InputStreamReader(request.getInputStream(), "UTF-8"), params[0]);
//                for (int i = 1; i < params.length; i++) {
//                    if (params[i] == OpportunityCache.class) {
//                        requestParams[i] = opportunityCache;
//                    } else if (params[i] == ChartHistoryCache.class) {
//                        requestParams[i] = chartHistoryCache;
//                    } else if (params[i] == LevelHistoryCache.class) {
//                        requestParams[i] = levelHistoryCache;
//                    } else if (params[i] == WebLevelsCache.class) {
//                        requestParams[i] = levelsCache;
//                    } else if (params[i] == HttpServletRequest.class) {
//                    	requestParams[i] = request;
//                    } else if (params[i] == PastExpiriesCache.class) {
//                    	requestParams[i] = expiriesCache;
//                    }
//                }
//                if (requestParams[0] instanceof MethodRequest) {
//                	((MethodRequest)requestParams[0]).setIp(CommonUtil.getIPAddress(request));
//                }
//                // since the methods invoked are static, class instance isn't required
//                result = m.invoke(null, requestParams);
//
//	        	// serialize the JSON response for client
//	        	String jsonResponse = gson.toJson(result);
//		        if (null != jsonResponse && jsonResponse.length() > 0) {
//		        	log.debug(jsonResponse);
//		        	byte[] data = jsonResponse.getBytes("UTF-8");
//		        	OutputStream os = response.getOutputStream();
//		        	response.setHeader("Content-Type", "application/json");
//		        	response.setHeader("Content-Length", String.valueOf(data.length));
//		        	os.write(data, 0, data.length);
//		        	os.flush();
//		        	os.close();
//		        }
//	       } else {
//	    	   log.warn("Method: " + methodReq + " not found!");
//	       }
//        } catch (Exception e) {
//			log.error("Problem executing " + methodReq + " Method! ", e);
//		}
    }

    /* (non-Javadoc)
     * @see javax.servlet.GenericServlet#destroy()
     */
    @Override
	public void destroy() {
    	super.destroy();
//    	methods = null;

        ServletContext servletContext = getServletContext();
    	JobsScheduler js = (JobsScheduler) servletContext.getAttribute(JobsManagerBase.CONTEXT_PARAM_RUN_JOBS);
        if (null != js) {
            js.stopJobsScheduler();
        }
//
//        try {
//            levelsCache.close();
//            levelsCache.unregisterJMX();
//            if (log.isInfoEnabled()) {
//                log.info(levelsCache.getClass().getName() + " unbound from JNDI");
//            }
//        } catch (Exception e) {
//            log.error("Failed to unbind WebLevelsCache from JNDI", e);
//        }
//
//        opportunityCache.stopOpportunityCacheCleaner();
//        expiriesCache.stopPastExpiriesCache();
       // closeAllBubblesOpenInv.stopCloseAllBubblesOpenInvestments();

        if (null != pixelHitter) {
        	pixelHitter.stopSenderThread();
        }

        try {
            jmxCacheClean.unregisterJMX();
        } catch (Exception e) {
            log.error("Can't unregister LogLevelManager from JNDI.", e);
        }
        
        try {
        	jmxCassandraReset.unregisterJMX();
        } catch (Exception e) {
        	log.error("Cannot unregister Cassandra reset.", e);
        }

        try {
        	boolean result = ManagerBase.closeCluster();
        	log.info("Cluster closing result: " + result);
        } catch (Exception e) {
        	log.error("Problem closing cluster", e);
        }
        dbMonitor.unregisterJMX();
        amqMonitor.unregisterJMX();
    }
//
//    /**
//     * @return
//     */
//    public static Hashtable<String, Method> getMethods() {
//    	return methods;
//    }

    /**
     * @return
     */
    public static ValidatorResources getValidatorResources() {
    	return validatorResources;
    }
//
//	/**
//	 * @return the levelsCache
//	 */
//	public static WebLevelsCache getLevelsCache() {
//		return levelsCache;
//	}

	/**
	 * @return the pixelHitter
	 */
	public static PixelHitter getPixelHitter() {
		return pixelHitter;
	}
}