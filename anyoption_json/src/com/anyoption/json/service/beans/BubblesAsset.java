package com.anyoption.json.service.beans;

/**
 * @author kirilim
 */
public class BubblesAsset {

	private String code;
	private String name;
	private long opportunityId;
	private int pricingLevel;

	public BubblesAsset(String code, String name, long opportunityId, int pricingLevel) {
		this.code = code;
		this.name = name;
		this.opportunityId = opportunityId;
		this.pricingLevel = pricingLevel;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getOpportunityId() {
		return opportunityId;
	}

	public void setOpportunityId(long opportunityId) {
		this.opportunityId = opportunityId;
	}

	public int getPricingLevel() {
		return pricingLevel;
	}

	public void setPricingLevel(int pricingLevel) {
		this.pricingLevel = pricingLevel;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "BubblesAsset: " + ls
				+ super.toString() + ls
				+ "code: " + code + ls
				+ "name: " + name + ls
				+ "opportunityId: " + opportunityId + ls
				+ "pricingLevel: " + pricingLevel + ls;
	}
}