package com.anyoption.json.service;

import java.io.InputStreamReader;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.anyoption.common.beans.base.User.MobileNumberValidation;
import com.anyoption.common.sms.SMSException;
import com.anyoption.json.requests.SMSServiceRequest;
import com.anyoption.json.results.SMSServiceResponse;
import com.anyoption.json.util.JsonExclusionStrategy;
import com.anyoption.managers.SMSManagerBase;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SMSServiceServlet  extends HttpServlet {
	
	private static final long serialVersionUID = 7891752632015386207L;

	public void init() {
	
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
		super.doGet(request, response);
	}

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
    	Gson gson = new GsonBuilder().setExclusionStrategies(new JsonExclusionStrategy()).serializeNulls().create();

		// deserialize the JSON request from client
    	SMSServiceRequest req = gson.fromJson(new InputStreamReader(request.getInputStream(), "UTF-8"), SMSServiceRequest.class);
    	SMSServiceResponse res = new SMSServiceResponse(); 
		//System.out.println(validationRequest);
		try {
			SMSManagerBase.sendTextMessage(req.getSender(), req.getSenderNumber(), req.getPhone(), req.getMessage(), req.getKeyValue(), req.getKeyType(), req.getProviderId(), req.getDescriptionId(), MobileNumberValidation.NOTVALIDATED);
			res.setSuccessfull(true);
		} catch (SMSException e) {
			res.setSuccessfull(false);
		}
		// serialize the JSON response for client
		String jsonResponse = gson.toJson(res);
		if (null != jsonResponse && jsonResponse.length() > 0) {
			byte[] data = jsonResponse.getBytes("UTF-8");
			OutputStream os = response.getOutputStream();
			response.setHeader("Content-Type", "application/json");
			response.setHeader("Content-Length", String.valueOf(data.length));
			os.write(data, 0, data.length);
			os.flush();
			os.close();
		}
 
    }

    public void destroy() {
    	
    }

    
}