//package com.anyoption.json.service;
//
//import java.io.BufferedInputStream;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Date;
//import java.util.List;
//import java.util.Locale;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.beans.User;
//import com.anyoption.common.beans.File;
//import com.anyoption.common.beans.FileType;
//import com.anyoption.common.beans.Skin;
//import com.anyoption.common.managers.FilesManagerBase;
//import com.anyoption.common.managers.LanguagesManagerBase;
//import com.anyoption.common.managers.UsersPendingDocsManagerBase;
//import com.anyoption.common.service.CommonJSONService;
//import com.anyoption.common.service.results.MethodResult;
//import com.anyoption.managers.SkinsManagerBase;
//import com.anyoption.managers.UsersManagerBase;
//
///**
// * @author kirilim
// */
//public class UploadDocumentsService {
//
//	private static final Logger log = Logger.getLogger(UploadDocumentsService.class);
//	private static final List<Long> jointFileTypes = Arrays.asList(FileType.USER_ID_COPY, FileType.PASSPORT, FileType.DRIVER_LICENSE);
//
//	public static void uploadFile(BufferedInputStream bis, User user, long fileType, String fileName, long ccId, int writerId, MethodResult result) {
//		log.debug("uploadFile method, collected parameters: " + logParams(fileType, fileName, ccId, writerId));
//		Skin s = SkinsManagerBase.getSkin(user.getSkinId());
//		Locale locale = new Locale(LanguagesManagerBase.getLanguage(s.getDefaultLanguageId()).getCode());
//		File file = UsersManagerBase.getUserFileByTypeAndCc(user.getId(), fileType, ccId, locale);
//		if (file == null) {
//			log.debug("There is no file with type " + fileType + " for user with id " + user.getId() + ". This should not happen");
//			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
//			return;
//		}
//		FilesManagerBase.deleteFileByName(file.getFileName());
//		try {
//			fileName = FilesManagerBase.saveFile(fileName, bis, user.getId(), fileType, false, false);
//		} catch (IOException e) {
//			log.debug("Cannot save file", e);
//			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
//			return;
//		}
//		if (!com.anyoption.common.util.Utils.validateFileExtension(fileName)) {
//			log.debug("File extension not allowed: " + fileName);
//			FilesManagerBase.deleteFileByName(fileName);
//			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
//			return;
//		}
//		if (fileName != null) {
//			FilesManagerBase.setFileDefaultValues(file, fileName);
//			file.setUploaderId(writerId);
//			file.setUploadDate(new Date());
//			file.setExpDate(null);
//			file.setExpDay(null);
//			file.setExpMonth(null);
//			file.setExpYear(null);
//			boolean updated = FilesManagerBase.updateWhenUpload(file);
//			if (!updated) {
//				result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
//			} else {
//				FilesManagerBase.changeFileStatusesNotNeeded(user.getId(), fileType, locale);
//				FilesManagerBase.clearKSFileData(file.getId());
//			}
//			
//			try {
//				UsersPendingDocsManagerBase.calculatePendingDocs(file.getId(), UsersPendingDocsManagerBase.EMPTY_PARAM);
//				UsersPendingDocsManagerBase.updateLastUploadFileTime(file.getId());
//			} catch (Exception e) {
//				log.error("Can't calculate Pending docs ", e);
//			}
//		} else {
//			result.setErrorCode(CommonJSONService.ERROR_CODE_FILE_TOO_LARGE);
//		}
//	}
//
//	private static String logParams(long fileType, String fileName, long ccId, int writerId) {
//		String ls = System.getProperty("line.separator");
//		return ls + "fileType: " + fileType + ls
//				+ "fileName: " + fileName + ls
//				+ "ccId: " + ccId + ls
//				+ "writerId: " + writerId + ls;
//	}
//}