/**
 * 
 */
package com.anyoption.json.service.jobs;

import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.Job;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.managers.InvestmentsManagerBase;
import com.anyoption.common.util.ClearingUtil;

/**
 * @author pavelt
 *
 */
public class BubbleCloseAllOpenInvestments implements ScheduledJob {

	/* (non-Javadoc)
	 * @see com.anyoption.common.jobs.ScheduledJob#setJobInfo(com.anyoption.common.beans.Job)
	 */
	
	private static final Logger log = Logger.getLogger(BubbleCloseAllOpenInvestments.class);
	private final long addTimeToBubbleEndTime = 1; // 1->one minute, 60->one hour, 1440-> one day
	private final long addDaysToSearchPeriod = 1; // DAYS
	private final long queryRowLimit = 100; // max number of rows which query is set to retrieve
	private String url;
	private boolean running;
	
	@Override
	public void setJobInfo(Job job) {
		if (null != job.getConfig() && !job.getConfig().trim().equals("")){
			url = job.getConfig();
		}

	}

	/* (non-Javadoc)
	 * @see com.anyoption.common.jobs.ScheduledJob#run()
	 */
	@Override
	public boolean run() {
		log.debug("BubbleCloseAllOpenInvestments job starting... ");
		running = true;
		try {
			ArrayList<Long> bubbleIds = InvestmentsManagerBase.getBubbleInvestmentsForClose(addTimeToBubbleEndTime, addDaysToSearchPeriod, queryRowLimit);
			if (bubbleIds != null && !bubbleIds.isEmpty()) {
				for (long invId : bubbleIds) {
					try {
						if (!running) {
							return running;
						}
						String response = ClearingUtil.executeGETRequest(url + invId, null, false);
						parseJsonResponseAndSettleInvestment(response, invId);
					}catch (Exception e){
						log.error("Can not get respose for bubble investment with id : " + invId, e);
					}
				}
			}else{
				log.debug("No bubbles investments to settle.");
			}
		} catch (SQLException ex) {
			log.error("Can not get bubble Id's due to : ", ex);
		}
		return running;
	}
	
	private void parseJsonResponseAndSettleInvestment(String response, long invId){
		log.debug("Bubble investment json respose:  " + response);
		try{
			// Get json response from Bubbles
			JSONObject jsonObjRes = new JSONObject(response);
			String state = jsonObjRes.getString("state");
			Double closingLevel = jsonObjRes.getDouble("closingLevel");
			// load investment to settle
			Investment inv = com.anyoption.managers.InvestmentsManagerBase.getInvestmentToSettle(invId, false, false);
			if (inv != null) {
				long isWon = 0;
				if (state.equals("won")) {
					isWon = 1;
				}
				// going to settle bubble investment
				com.anyoption.managers.InvestmentsManagerBase.settleInvestment(invId, Writer.WRITER_ID_AUTO, 0l, inv, isWon, 0L);
				InvestmentsManagerBase.updateBubblesClosingLevel(invId, closingLevel);
			}
			
		}catch(JSONException ex){
			log.error("Response is not the correct format for investment with id: " + invId, ex);
		}catch(SQLException sqlEx){
			log.error("Fail to settle bubble investment with id: " + invId, sqlEx);
		}
		
	}
	
	@Override
	public void stop() {
        log.info("BubbleCloseAllOpenInvestments stopping...");
        running = false;
	}

}
