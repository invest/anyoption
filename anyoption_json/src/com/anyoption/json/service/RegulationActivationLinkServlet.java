/**
 * 
 */
package com.anyoption.json.service;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.ActivationMail;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.IssuesManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.util.CommonUtil;
import com.copyop.common.Constants;

/**
 * @author pavelt
 *
 */
public class RegulationActivationLinkServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3257609212931762565L;
	private static final Logger logger = Logger.getLogger(RegulationActivationLinkServlet.class);
	
	public static final String login = "login";
	public static final String ETRADER_PLATFORM = "etrader";
	
	private String activationKey;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {

		String homePageUrl = null;
		activationKey = (String) request.getParameter(ConstantsBase.REG_HASH_KEY);

		if (activationKey == null || activationKey.equals("")) {
			logger.error("Hash key param is null or empty!");
		} else {
			try {
				ActivationMail activation = UserRegulationManager.getUserHashTypeAndIdByHashKey(activationKey);
				if (activation != null) {
					UserRegulationBase ur = new UserRegulationBase();
					ur.setUserId(activation.getUserId());
					UserRegulationManager.getUserRegulation(ur);
					
					if (activation.getType() == UserRegulationBase.HASH_TYPE_ET_UNSUSPEND) {
						homePageUrl = CommonUtil.getConfig("homepage.url.etrader");
						try {
							UserRegulationManager.unsuspendUserViaActivationLink(activationKey);
						} catch (SQLException e) {
							logger.error("Can not unsuspend ETRADER regulation user", e);
						}
					} else {
						homePageUrl = CommonUtil.getConfig("homepage.url");
						try {
							if (activation.getType() == UserRegulationBase.HASH_TYPE_AO_UNRESTRICTED) {
								int issueActionType = (ur.getScoreGroup() == UserRegulationBase.AO_REGULATION_RESTRICTED_FAILED_Q_SCORE_GROUP_ID) ? IssuesManagerBase.ISSUE_ACTION_TYPE_SUCCESS_AFTER_FAILED_QUESTIONNAIRE :
										IssuesManagerBase.ISSUE_ACTION_TYPE_RESTRICTED_SUCCESS_AFTER_REG_LINK;								
								IssuesManagerBase.insertIssueUserRegulationRestricted(ur.getUserId(), issueActionType, Writer.WRITER_ID_AUTO);
							} else if (activation.getType() == UserRegulationBase.HASH_TYPE_AO_UNBLOCKTRESHOLD) {
								if (activation.getUserId() > 0) {
									IssuesManagerBase.insertUserRegulationUnblockIssueAction(activation.getUserId(),
											Writer.WRITER_ID_AUTO);
								}
							}
						} catch (SQLException e) {
							logger.error("Can not unrestrict Anyoption/COPYOP regulation user", e);
						}
					}
				} else {
					logger.error("No user for this hash code!");
				}
			} catch (SQLException e) {
				logger.error("Can not unrestrict/unsuspend/unblock ET/Anyoption/COPYOP regulation user", e);
			}
		}

		try {
			response.sendRedirect(homePageUrl + login);
		} catch (IOException e) {
			logger.debug("Error redirect from activation page.");
		}
	}

}
