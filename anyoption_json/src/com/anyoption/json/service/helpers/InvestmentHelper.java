//package com.anyoption.json.service.helpers;
//
//import java.util.Calendar;
//import java.util.Locale;
//import java.util.TimeZone;
//
//import com.anyoption.common.beans.Investment;
//import com.anyoption.common.beans.Market;
//import com.anyoption.managers.MarketsManagerBase;
//import com.anyoption.util.CommonUtil;
//
///**
// * @author kirilim
// */
//public class InvestmentHelper {
//
//	public static void initInvestment(Investment inv, Locale locale, String utcOffset) {
//		Market m = MarketsManagerBase.getMarket(inv.getMarketId());
//		inv.setAsset(CommonUtil.getMessage(locale, m.getDisplayNameKey(), null));
//		inv.setLevel(CommonUtil.formatLevel(inv.getCurrentLevel(), m.getDecimalPoint()));
//    	inv.setExpiryLevel(CommonUtil.formatLevel(inv.getClosingLevel(), m.getDecimalPoint() - m.getDecimalPointSubtractDigits()));
//    	inv.setTimePurchased(Calendar.getInstance(TimeZone.getTimeZone("GMT")));
//    	inv.getTimePurchased().setTime(inv.getTimeCreated());
//    	inv.setTimePurchaseTxt(CommonUtil.getTimeAndDateFormat(inv.getTimePurchased().getTime(), utcOffset));
//    	inv.setTimeEstClosingTxt(CommonUtil.getTimeAndDateFormat(inv.getTimeEstClosing(), utcOffset));
//    	inv.setCurrentLevelTxt(CommonUtil.formatLevel(inv.getCurrentLevel(), m.getDecimalPoint()));
//    	inv.setAmountTxt(CommonUtil.formatCurrencyAmount(inv.getAmount(), true, inv.getCurrencyId()));
//    	if (inv.getTimeSettled() != null) {
//    		inv.setAmountReturnWF(CommonUtil.formatCurrencyAmount(inv.getAmount() + inv.getWin() - inv.getLose(), true, inv.getCurrencyId()));
//    	} else {
//    		inv.setAmountReturnWF("");
//    	}
//	}
//}