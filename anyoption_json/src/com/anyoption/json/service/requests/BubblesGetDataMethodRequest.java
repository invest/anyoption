package com.anyoption.json.service.requests;

import com.anyoption.common.service.requests.MethodRequest;

/**
 * @author kirilim
 */
public class BubblesGetDataMethodRequest extends MethodRequest {

	private long userId;
	private String s;
	private String token;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getS() {
		return s;
	}

	public void setS(String s) {
		this.s = s;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "BubblesMarketsMethodRequest: " + ls
				+ super.toString() + ls
				+ "userId: " + userId + ls
				+ "token: " + token + ls
				+ "s: " + s + ls;
	}
}