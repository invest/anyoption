package com.anyoption.json.service.requests;

import com.anyoption.common.service.requests.MethodRequest;


/**
 *
 * @author EyalG
 *
 */
public class SettleBubbleInvestmentMethodRequest extends MethodRequest {

	private long userId;
	private long investmentId;
	private long returnAmount;
	private long settleTime;
	private double closingLevel;
	
	private String s;
	
	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public long getInvestmentId() {
		return investmentId;
	}
	
	public void setInvestmentId(long investmentId) {
		this.investmentId = investmentId;
	}
	
	public long getReturnAmount() {
		return returnAmount;
	}

	public void setReturnAmount(long returnAmount) {
		this.returnAmount = returnAmount;
	}

	public long getSettleTime() {
		return settleTime;
	}
	
	public void setSettleTime(long settleTime) {
		this.settleTime = settleTime;
	}
	
	public String getS() {
		return s;
	}
	
	public void setS(String s) {
		this.s = s;
	}
	
	public double getClosingLevel() {
		return closingLevel;
	}

	public void setClosingLevel(double closingLevel) {
		this.closingLevel = closingLevel;
	}

	@Override
	public String toString() {
		return "SettleBubbleInvestmentMethodRequest [userId=" + userId
				+ ", investmentId=" + investmentId + ", returnAmount="
				+ returnAmount + ", settleTime=" + settleTime + ", s=" + s
				+ ", skinId=" + skinId + ", ip=" + ip + ", utcOffset="
				+ utcOffset + ", deviceId=" + deviceId + ", writerId="
				+ writerId + ", apiUser=" + apiUser + ", locale=" + locale
				+ ", osVersion=" + osVersion + ", deviceType=" + deviceType
				+ ", appVersion=" + appVersion + ", isTablet=" + isTablet + ", closingLeveL=" + closingLevel + "]";
	}	
}