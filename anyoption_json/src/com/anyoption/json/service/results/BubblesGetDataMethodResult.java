package com.anyoption.json.service.results;

import java.util.List;

import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.service.beans.BubblesAsset;

/**
 * @author kirilim
 */
public class BubblesGetDataMethodResult extends MethodResult {

	private List<BubblesAsset> assets;
	private long defaultAmount;
	private List<Long> amounts;
	private long balance;
	private String currency;
	private int pricingLevel;
	private long classId;
	private long userRegistration;

	public List<BubblesAsset> getAssets() {
		return assets;
	}

	public void setAssets(List<BubblesAsset> assets) {
		this.assets = assets;
	}

	public long getDefaultAmount() {
		return defaultAmount;
	}

	public void setDefaultAmount(long defaultAmount) {
		this.defaultAmount = defaultAmount;
	}

	public List<Long> getAmounts() {
		return amounts;
	}

	public void setAmounts(List<Long> amounts) {
		this.amounts = amounts;
	}

	public long getBalance() {
		return balance;
	}

	public void setBalance(long balance) {
		this.balance = balance;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public int getPricingLevel() {
		return pricingLevel;
	}

	public void setPricingLevel(int pricingLevel) {
		this.pricingLevel = pricingLevel;
	}

	public long getClassId() {
		return classId;
	}

	public void setClassId(long classId) {
		this.classId = classId;
	}

	
	public long getUserRegistration() {
		return userRegistration;
	}

	public void setUserRegistration(long userRegistration) {
		this.userRegistration = userRegistration;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "BubblesMarketsMethodResult: " + ls
				+ super.toString() + ls
				+ "assets: " + assets + ls
				+ "defaultAmount: " + defaultAmount + ls
				+ "amounts: " + amounts + ls
				+ "balance: " + balance + ls
				+ "currency: " + currency + ls
				+ "pricingLevel: " + pricingLevel + ls
				+ "classId: " + classId + ls
				+ "userRegistration" + userRegistration;
	}
}