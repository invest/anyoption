package com.anyoption.json.service.results;

import com.anyoption.common.service.results.MethodResult;
import com.google.gson.annotations.Expose;

/**
 * @author stefanb
 *
 */
public class SettleBubblesInvestmentMethodResult extends MethodResult {

    /*user balance*/
    @Expose
    private long balance;

    @Override
    public String toString() {
	String ls = System.getProperty("line.separator");
	return ls + "SettleBubblesInvestmentMethodResult" + super.toString() + "user balance: " + balance + ls;
    }

    public long getBalance() {
	return balance;
    }

    public void setBalance(long balance) {
	this.balance = balance;
    }
}