package com.anyoption.json.service;


import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.Collator;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.SortedSet;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.Validator;
import org.apache.commons.validator.ValidatorException;
import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

import com.anyoption.beans.AssetIndex;
import com.anyoption.beans.Limit;
import com.anyoption.beans.MarketingAffiliatePixels;
import com.anyoption.beans.MarketingCombinationPixels;
import com.anyoption.beans.MarketingPixel;
import com.anyoption.beans.MobileApplication;
import com.anyoption.beans.User;
import com.anyoption.beans.base.City;
import com.anyoption.beans.base.Contact;
import com.anyoption.common.annotations.PrintLogAnnotations;
import com.anyoption.common.beans.BalanceStep;
import com.anyoption.common.beans.Bonus;
import com.anyoption.common.beans.BubblesToken;
import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.DeviceUniqueIdSkin;
import com.anyoption.common.beans.File;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.beans.FireServerPixelInfo;
import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.Login;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.MarketRate;
import com.anyoption.common.beans.MarketingAffiliate;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunityType;
import com.anyoption.common.beans.ServerPixel;
import com.anyoption.common.beans.Skin;
import com.anyoption.common.beans.SkinCurrency;
import com.anyoption.common.beans.State;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.UsersAwardBonus;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.ApiErrorCode;
import com.anyoption.common.beans.base.AssetIndexBase;
import com.anyoption.common.beans.base.BalanceStepPredefValue;
import com.anyoption.common.beans.base.BankBranches;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.CreditCardType;
import com.anyoption.common.beans.base.CurrenciesRules;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.FTDUser;
import com.anyoption.common.beans.base.Language;
import com.anyoption.common.beans.base.LastCurrencyRate;
import com.anyoption.common.beans.base.MailBoxUser;
import com.anyoption.common.beans.base.MarketingInfo;
import com.anyoption.common.beans.base.OpportunityOddsPair;
import com.anyoption.common.beans.base.Platform;
import com.anyoption.common.beans.base.QuestionnaireGroup;
import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.beans.base.Register;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.beans.base.UsersDetailsHistory;
import com.anyoption.common.bl_vos.BankBase;
import com.anyoption.common.bl_vos.OptionPlusQuote;
import com.anyoption.common.bl_vos.WireBase;
import com.anyoption.common.charts.ChartHistoryCache;
import com.anyoption.common.clearing.CDPayInfo;
import com.anyoption.common.clearing.DeltaPayInfo;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.anyoption.common.enums.DeviceFamily;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.jms.WLCOppsCacheBean;
import com.anyoption.common.jms.WebLevelLookupBean;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.managers.ApiManager;
import com.anyoption.common.managers.BaroPayManagerBase;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.CreditCardsManagerBase;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.managers.CurrenciesRulesManagerBase;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.managers.FeesManagerBase;
import com.anyoption.common.managers.FilesManagerBase;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.managers.LoginManager;
import com.anyoption.common.managers.LoginsManager;
import com.anyoption.common.managers.MarketingAffiliatesManagerBase;
import com.anyoption.common.managers.MarketingTrackingManager;
import com.anyoption.common.managers.QuestionnaireManagerBase;
import com.anyoption.common.managers.RewardUserTasksManager;
import com.anyoption.common.managers.RewardUserTasksManager.TaskGroupType;
import com.anyoption.common.managers.ServerPixelsManagerBase;
import com.anyoption.common.managers.TransactionsManagerBase.TransactionSource;
import com.anyoption.common.managers.UserBalanceStepManager;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.managers.UsersAwardBonusManagerBase;
import com.anyoption.common.payments.WithdrawEntryInfo;
import com.anyoption.common.payments.WithdrawEntryResult;
import com.anyoption.common.payments.WithdrawUtil;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.CancelBubbleInvestmentMethodRequest;
import com.anyoption.common.service.requests.CancelInvestmentCheckboxStateMethodRequest;
import com.anyoption.common.service.requests.CancelInvestmentMethodRequest;
import com.anyoption.common.service.requests.CardMethodRequest;
import com.anyoption.common.service.requests.CcDepositMethodRequest;
import com.anyoption.common.service.requests.DepositBonusBalanceMethodRequest;
import com.anyoption.common.service.requests.InsertInvestmentMethodRequest;
import com.anyoption.common.service.requests.InsertUserMethodRequest;
import com.anyoption.common.service.requests.InsertWithdrawCancelMethodRequest;
import com.anyoption.common.service.requests.InvestmentsMethodRequest;
import com.anyoption.common.service.requests.IsKnowledgeQuestionMethodRequest;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.requests.SellDynamicsInvestmentRequest;
import com.anyoption.common.service.requests.TermsFileMethodRequest;
import com.anyoption.common.service.requests.UnblockUserMethodRequest;
import com.anyoption.common.service.requests.UpdateUserMethodRequest;
import com.anyoption.common.service.requests.UpdateUserQuestionnaireAnswersRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.requests.WithdrawBonusRegulationStateMethodRequest;
import com.anyoption.common.service.results.CardMethodResult;
import com.anyoption.common.service.results.DepositBonusBalanceMethodResult;
import com.anyoption.common.service.results.ErrorMessage;
import com.anyoption.common.service.results.InvestmentMethodResult;
import com.anyoption.common.service.results.InvestmentsMethodResult;
import com.anyoption.common.service.results.MarketsMethodResult;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.SkinMethodResult;
import com.anyoption.common.service.results.TermsPartFilesMethodResult;
import com.anyoption.common.service.results.TransactionMethodResult;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.common.service.results.UserQuestionnaireResult;
import com.anyoption.common.service.results.VisibleSkinListMethodResult;
import com.anyoption.common.service.results.VisibleSkinListMethodResult.VisibleSkinPair;
import com.anyoption.common.service.results.WithdrawBonusRegulationStateMethodResult;
import com.anyoption.common.sms.RegisterSMSThread;
import com.anyoption.common.util.AESUtil;
import com.anyoption.common.util.AnyOptionCreditCardValidator;
import com.anyoption.common.util.AssetIndexMarketCacheJson;
import com.anyoption.common.util.InvestmentHelper;
import com.anyoption.common.util.InvestmentValidator;
import com.anyoption.common.util.InvestmentValidatorDynamics;
import com.anyoption.common.util.InvestmentValidatorParams;
import com.anyoption.common.util.MarketingTracker;
import com.anyoption.common.util.MarketingTrackerBase;
import com.anyoption.common.util.MessageToFormat;
import com.anyoption.common.util.OpportunityCache;
import com.anyoption.common.util.OpportunityCacheBean;
import com.anyoption.common.util.PastExpiriesCache;
import com.anyoption.common.util.Pixel;
import com.anyoption.common.util.Utils;
import com.anyoption.distribution.Distribution;
import com.anyoption.distribution.DistributionType;
import com.anyoption.json.requests.AssetIndexMethodRequest;
import com.anyoption.json.requests.BonusMethodRequest;
import com.anyoption.json.requests.CUPInfoMethodRequest;
import com.anyoption.json.requests.ChangePasswordMethodRequest;
import com.anyoption.json.requests.ChartDataMethodRequest;
import com.anyoption.json.requests.CheckUpdateMethodRequest;
import com.anyoption.json.requests.ContactMethodRequest;
import com.anyoption.json.requests.CreditCardRequest;
import com.anyoption.json.requests.CreditCardValidatorRequest;
import com.anyoption.json.requests.CurrencyMethodRequest;
import com.anyoption.json.requests.DepositReceiptMethodRequest;
import com.anyoption.json.requests.FTDUsersRequest;
import com.anyoption.json.requests.FeeMethodRequest;
import com.anyoption.json.requests.FireServerPixelMethodRequest;
import com.anyoption.json.requests.FirstEverDepositCheckRequest;
import com.anyoption.json.requests.FormsMethodRequest;
import com.anyoption.json.requests.InatecDepositFinishMethodRequest;
import com.anyoption.json.requests.InatecDepositMethodRequest;
import com.anyoption.json.requests.InboxMethodResult;
import com.anyoption.json.requests.InsertMarketingTrackerClickRequest;
import com.anyoption.json.requests.InsurancesMethodRequest;
import com.anyoption.json.requests.MailBoxMethodRequest;
import com.anyoption.json.requests.MarketingUsersMethodRequest;
import com.anyoption.json.requests.NetellerDepositMethodRequest;
import com.anyoption.json.requests.OptionPlusMethodRequest;
import com.anyoption.json.requests.PastExpiresMethodRequest;
import com.anyoption.json.requests.PixelMethodRequest;
import com.anyoption.json.requests.RegisterAttemptMethodRequest;
import com.anyoption.json.requests.ThreeDMethodRequest;
import com.anyoption.json.requests.TransactionMethodRequest;
import com.anyoption.json.requests.UserRegulationRequest;
import com.anyoption.json.requests.WireMethodRequest;
import com.anyoption.json.results.AssetIndexMethodResult;
import com.anyoption.json.results.BaroPayMethodResult;
import com.anyoption.json.results.BonusMethodResult;
import com.anyoption.json.results.CUPInfoMethodResult;
import com.anyoption.json.results.ChartDataMethodResult;
import com.anyoption.json.results.ContactMethodResult;
import com.anyoption.json.results.CountriesOptionsMethodResult;
import com.anyoption.json.results.CountryIdMethodResult;
import com.anyoption.json.results.CreditCardValidatorResponse;
import com.anyoption.json.results.CreditCardsMethodResult;
import com.anyoption.json.results.CupWithdrawalMethodResult;
import com.anyoption.json.results.CurrenciesResult;
import com.anyoption.json.results.CurrencyMethodResult;
import com.anyoption.json.results.FTDUsersMethodResult;
import com.anyoption.json.results.FirstEverDepositCheckResult;
import com.anyoption.json.results.InatecDepositMethodResult;
import com.anyoption.json.results.InsurancesMethodResult;
import com.anyoption.json.results.LastCurrenciesRateMethodResult;
import com.anyoption.json.results.LogoutBalanceStepMethodResult;
import com.anyoption.json.results.MarketingUsersMethodResult;
import com.anyoption.json.results.NetellerDepositMethodResult;
import com.anyoption.json.results.OpportunitiesMethodResult;
import com.anyoption.json.results.OpportunityMethodResult;
import com.anyoption.json.results.OpportunityOddsPairResult;
import com.anyoption.json.results.OptionPlusMethodResult;
import com.anyoption.json.results.OptionsMethodResult;
import com.anyoption.json.results.PixelMethodResult;
import com.anyoption.json.results.RegisterAttemptMethodResult;
import com.anyoption.json.results.RetrieveUserDocumentsMethodResult;
import com.anyoption.json.results.SkinsOptionsMethodResult;
import com.anyoption.json.results.TransactionsMethodResult;
import com.anyoption.json.results.UpdateMethodResult;
import com.anyoption.json.service.beans.BubblesAsset;
import com.anyoption.json.service.helpers.CreditCardHelper;
import com.anyoption.json.service.helpers.MailBoxUserHelper;
import com.anyoption.json.service.requests.BubblesGetDataMethodRequest;
import com.anyoption.json.service.requests.InsertBubbleInvestmentMethodRequest;
import com.anyoption.json.service.requests.SettleBubbleInvestmentMethodRequest;
import com.anyoption.json.service.results.BubblesGetDataMethodResult;
import com.anyoption.json.service.results.InvestmentBubbleMethodResult;
import com.anyoption.json.service.results.SettleBubblesInvestmentMethodResult;
import com.anyoption.json.util.CountryOptionItem;
import com.anyoption.json.util.OptionItem;
import com.anyoption.json.util.PixelGenerator;
import com.anyoption.json.util.SkinOptionItem;
import com.anyoption.json.util.UserHelper;
import com.anyoption.json.util.ValidatorFieldChecks;
import com.anyoption.managers.BanksManagerBase;
import com.anyoption.managers.BonusManagerBase;
import com.anyoption.managers.CitiesManagerBase;
import com.anyoption.managers.ClearingManager;
import com.anyoption.managers.ContactsManagerBase;
import com.anyoption.managers.CreditCardTypesManagerBase;
import com.anyoption.managers.DeviceUniqueIdsSkinManager;
import com.anyoption.managers.InvestmentsManagerBase;
import com.anyoption.managers.IssuesManagerBase;
import com.anyoption.managers.LimitsManagerBase;
import com.anyoption.managers.MarketingCombinationManagerBase;
import com.anyoption.managers.MarketsManagerBase;
import com.anyoption.managers.OpportunitiesManagerBase;
import com.anyoption.managers.SMSManagerBase;
import com.anyoption.managers.SkinsManagerBase;
import com.anyoption.managers.TransactionsManagerBase;
import com.anyoption.managers.UsersManagerBase;
import com.anyoption.util.BonusUserFormater;
import com.anyoption.util.CommonUtil;
import com.anyoption.util.ConstantsBase;
import com.anyoption.util.InvestmentValidatorBubble;
import com.anyoption.util.SendTemplateEmail;
import com.anyoption.util.TransactionFormater;

/**
 * AnyoptionService - logic functionality for the application usability.
/**
 * @author LioR SoLoMoN
 *
 */
public class AnyoptionService extends CommonJSONService {
	public static final Logger log = Logger.getLogger(AnyoptionService.class);
    
    /**
     * @param formName
     * @param bean
     * @param locale
     * @return
     * @throws ValidatorException
     */
    public static ArrayList<ErrorMessage> validateInput(String formName, Object bean, Locale locale) throws ValidatorException {
        ArrayList<ErrorMessage> msgs = new ArrayList<ErrorMessage>();
        Validator validator = new Validator(AnyoptionServiceServlet.getValidatorResources(), formName);
        validator.setParameter(Validator.BEAN_PARAM, bean);
        validator.setParameter(ValidatorFieldChecks.MSGS_PARAM, msgs);
        validator.setParameter(Validator.LOCALE_PARAM, locale);
        validator.setClassLoader(AnyoptionService.class.getClassLoader());
        validator.validate();
        return msgs;
    }


    /**
     * @param request - MethodRequest
     * @return the OptionsMethodResult with list of currencies options.
     */
    public static OptionsMethodResult getCurrenciesOptions(MethodRequest request) {
    	OptionsMethodResult result = new OptionsMethodResult();
    	log.info("getCurrenciesOptions: skinId=" + request.getSkinId());
    	try {
    		Hashtable<Long, Currency> currencies = CurrenciesManagerBase.getCurrencies();
    		ArrayList<OptionItem> options = new ArrayList<OptionItem>();
    		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
    		for (Iterator<Long> iter = currencies.keySet().iterator(); iter.hasNext();) {
    			Long id = iter.next();
    			options.add(new OptionItem(id.longValue(), CommonUtil.getMessage(locale, currencies.get(id).getDisplayName(), null), locale));
    		}
    		Collections.sort(options, new OptionItem.AlphaComparator());
    		result.setOptions(options.toArray(new OptionItem[options.size()]));
    	} catch (Exception e) {
            log.error("Error in getCurrenciesOptions", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
	}

    /**
     * @param request - Method Request . Containing the skin_id.
     * @return the OptionsMethodResult with a list of currencies available for the skin in the request.
     */
    public static OptionsMethodResult getSkinCurrenciesOptions(MethodRequest request) {
    	OptionsMethodResult result = new OptionsMethodResult();
    	log.info("getSkinCurrenciesOptions: skinId=" + request.getSkinId());
    	try {
    		ArrayList<SkinCurrency> sc = SkinsManagerBase.getSkinCurrencies(request.getSkinId());
    		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
    		ArrayList<OptionItem> options = new ArrayList<OptionItem>();
    		for (SkinCurrency c : sc) {
    			options.add(new OptionItem(c.getCurrencyId(), CommonUtil.getMessage(locale, c.getDisplayName(), null), locale));
    		}
    		Collections.sort(options, new OptionItem.AlphaComparator());
    		result.setOptions(options.toArray(new OptionItem[options.size()]));
    	} catch (Exception e) {
            log.error("Error in getSkinCurrenciesOptions", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
    }

    /**
     * @param request - Method Request .
     * @return the CurrenciesResult with a list of currencies available for the skin in the request.
     */
    public static CurrenciesResult getSkinCurrencies(MethodRequest request) {
    	log.debug("getSkinCurrencies: " + request);
    	CurrenciesResult result = new CurrenciesResult();
    	try {
    		ArrayList<SkinCurrency> sc = SkinsManagerBase.getSkinCurrencies(request.getSkinId());
    		ArrayList<com.anyoption.beans.Currency> cur = new ArrayList<com.anyoption.beans.Currency>();
    		for(int i = 0; i < sc.size(); i++){
    			com.anyoption.beans.Currency currency =  new com.anyoption.beans.Currency(CurrenciesManagerBase.getCurrency(sc.get(i).getCurrencyId()));
    			Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
    			currency.initFormattedValues(locale);
    			cur.add(currency);
    		}

    		result.setCurrency(cur.toArray(new com.anyoption.beans.Currency[cur.size()]));
    		if (request.getWriterId() != Writer.WRITER_ID_BUBBLES_DEDICATED_APP) {
    			result.setPredefinedDepositAmountMap(CommonUtil.getPredefinedDepositAmount());
    		} else {
				result.setPredefinedDepositAmountMap(new HashMap<>(com.anyoption.common.managers.GeneralManager.getBDAPredefinedDepositAmounts()));
    		}
    	} catch (Exception e) {
            log.error("Error in getSkinCurrenciesOptions", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
    }

    /**
     * @param request - MethodRequest
     * @return the OptionsMethodResult with list of languages options.
     */
    public static OptionsMethodResult getLanguagesOptions(MethodRequest request) {
    	OptionsMethodResult result = new OptionsMethodResult();
    	log.info("getLanguagesOptions: skinId=" + request.getSkinId());
    	try {
    		Hashtable<Long, Language> languages = LanguagesManagerBase.getLanguages();
    		ArrayList<OptionItem> options = new ArrayList<OptionItem>();
    		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
    		for (Iterator<Long> iter = languages.keySet().iterator(); iter.hasNext();) {
    			Long id = iter.next();
    			options.add(new OptionItem(id.longValue(), CommonUtil.getMessage(locale, languages.get(id).getDisplayName(), null), locale));
    		}
    		Collections.sort(options, new OptionItem.AlphaComparator());
    		result.setOptions(options.toArray(new OptionItem[options.size()]));
    	} catch (Exception e) {
            log.error("Error in getLanguagesOptions", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
	}

    /**
     * @param request - Method Request. Containing skin_id.
     * @return the OptionsMethodResult with a list of states options sorted in alphabetical order.
     */
    public static OptionsMethodResult getStatesOptions(MethodRequest request) {
    	OptionsMethodResult result = new OptionsMethodResult();
    	log.info("getStatesOptions: skinId=" + request.getSkinId());
    	try {
    		ArrayList<State> states = State.getStates();
    		ArrayList<OptionItem> options = new ArrayList<OptionItem>();
    		for (State s : states){
    			options.add(new OptionItem(s.getId(), s.getName()));
    		}
    		Collections.sort(options, new OptionItem.AlphaComparator());
    		result.setOptions(options.toArray(new OptionItem[options.size()]));
    	} catch (Exception e) {
            log.error("Error in getStatesOptions", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
	}

    /**
     *
     * @param request - Method Request
     * @return the OptionsMethodResult with list of bonus states options.
     */
    public static OptionsMethodResult getBonusStatesOptions(MethodRequest request) {
    	OptionsMethodResult result = new OptionsMethodResult();
    	log.info("getBonusStatesOptions: skinId=" + request.getSkinId());
    	try {
    		ArrayList<Bonus.BonusStateItem> bonusStates = Bonus.getBonusStates();
    		OptionItem[] options = new OptionItem[bonusStates.size()];
    		int i = 0;
    		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
    		for (Bonus.BonusStateItem o : bonusStates) {
    			options[i++] = new OptionItem(o.getId(), CommonUtil.getMessage(locale, o.getValue(), null), locale);
    		}
    		result.setOptions(options);
    	} catch (Exception e) {
            log.error("Error in getBonusStatesOptions", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
	}


    public static CupWithdrawalMethodResult getMaxCupWithdrawal(UserMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("getMaxCupWithdrawal: " + request);
    	CupWithdrawalMethodResult result = new CupWithdrawalMethodResult();
    	try {
    		User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
    		if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
    			log.debug("Unable to load user");
    			return result;
    		}
	    	long userId = user.getId();
    		long maxCupWithdrawal = TransactionsManagerBase.getCUPWithdrawalAmount(userId, false);
    		long userBalance = user.getBalance();
    		maxCupWithdrawal = Math.min(maxCupWithdrawal, userBalance);
    		log.info("getMaxCupWithdrawal: userId=" + userId + "maxCupWithdrawal:"+maxCupWithdrawal);
			result.setMaxCupWithdrawal(maxCupWithdrawal);
    	} catch (Exception e) {
            log.error("Error in getMaxCupWithdrawal", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
	}


    /**
     * @param request - Bonus Method Request
     * @return the BonusMethodResult with list of User Bonuses options.
     */
    public static BonusMethodResult getUserBonuses(BonusMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("getUserBonuses: " + request);
    	BonusMethodResult result = new BonusMethodResult();
        try {
        	User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
        	if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
    			log.debug("Unable to load user");
    			return result;
    		}
        	Date fromTime = null;
            Date toTime = null;
            if (null != request.getFrom()) {
            	fromTime = request.getFrom().getTime();
            }
            if (null != request.getTo()) {
            	toTime = request.getTo().getTime();
            }

        	List<BonusUsers> bonuses = BonusManagerBase.getAllUserBonus(request.getUtcOffset(), user.getId(), fromTime, toTime, request.getBonusStateId(), request.getStartRow(), request.getPageSize(), request.getShowOnlyNotSeenBonuses());
        	bonuses = BonusManagerBase.getTurnoverParamsForBonus(bonuses);
        	String[] turnoverFactor = new String[5];
        	for (int i=0; i<5; i++) {
        		turnoverFactor[i] = Double.toString(InvestmentsManagerBase.getTurnoverFactor(i));
        	}
        	result.setTurnoverFactor(turnoverFactor);
        	result.setBonuses(new BonusUsers[bonuses.size()]);
        	String utcOffset = CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset());
        	for (int i = 0 ; i < bonuses.size() ; i++) {
        		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
        		BonusUserFormater.initFormattedValues(bonuses.get(i),locale, utcOffset);
        		result.getBonuses()[i] = bonuses.get(i);
        	}
        	BonusManagerBase.updateUserBonuses(user.getId());
        } catch (Exception e) {
            log.error("Error in getUserBonuses.", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }

    public static MethodResult logoutUser(UserMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("logout: " + request);
    	MethodResult result = new MethodResult();
    	HttpSession session = httpRequest.getSession();
    	try {
    		if (request.isEncrypt()) {
	    		User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
	    		if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	    			log.debug("Unable to load user");
	    			return result;
	    		}
	    		Long userLastLoginId = user.getLastLoginId();
	    		if (null != userLastLoginId) {
	        		com.anyoption.managers.LoginsManager.logOut(userLastLoginId);
	        	}
    		}
    		session.invalidate();
    	} catch (Exception e) {
            log.error("Error in logout method", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
    }

	/**
	 * @param request - Bonus Method Request
	 * @return the MethodResult with list of update Bonus Status.
	 */
	public static MethodResult updateBonusStatus(BonusMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("updateBonusStatus: " + request);
		MethodResult result = new MethodResult();
		long loginId = 0L;
		try {
			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
			}
		} catch (Exception e) {
			log.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		if (request.getBonusStateId() != ConstantsBase.BONUS_STATE_REFUSED
				&& request.getBonusStateId() != ConstantsBase.BONUS_STATE_CANCELED
				&& request.getBonusStateId() != Bonus.STATE_PENDING) {
			log.error("Bonus state not in statuses refused(5) or canceled(7) or pending(10)");
			result.setErrorCode(ERROR_CODE_INV_VALIDATION);
			return result;
		}
		BonusUsers tempBu = null;
		try {
			User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
			if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
				return result;
			}
			try {				
				//AO Regulation Claim Bonus Action Check
				if(request.getBonusStateId() == Bonus.STATE_PENDING){
					//Check AO Regulation
					AnyoptionService.validateUserRegulationAndNonReg(user, result, AnyoptionService.VALIDATE_REGULATION_DEPOSIT);
					if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			            return result;
			        }
				}
				tempBu = BonusManagerBase.getBonusUserByBonusId(user.getId(), request.getBonusId());
			} catch (SQLException e) {
				log.error("Can't get bonus user by bonus id", e);
				result.setErrorCode(ERROR_CODE_UNKNOWN);
				return result;
			}

			if (null != tempBu
					&& (tempBu.getBonusStateId() == ConstantsBase.BONUS_STATE_ACTIVE || tempBu.getBonusStateId() == ConstantsBase.BONUS_STATE_GRANTED)) {
				try {
					tempBu.setWriterIdCancel(Writer.WRITER_ID_MOBILE);
					BonusManagerBase.cancelBonusToUser(	tempBu, request.getBonusStateId(), new Integer(request.getUtcOffset()).toString(),
														Writer.WRITER_ID_MOBILE, user.getSkinId(), request.getIp(), loginId);
					log.info("bonus id: " + tempBu.getId() + " canceled");
				} catch (SQLException e) {
					log.info("Can't cancel bonus id: " + tempBu.getId());
					result.setErrorCode(ERROR_CODE_UNKNOWN);
					return result;
				}
			} else if (null != tempBu && tempBu.getBonusStateId() == Bonus.STATE_PENDING) {
				try {
					BonusManagerBase.acceptBonusUser(tempBu, user, request.getIp(), loginId);
					log.info("bonus user id: " + tempBu.getId() + " accepted");
				} catch (SQLException e) {
					log.info("Can't accept bonus user id: " + tempBu.getId());
					result.setErrorCode(ERROR_CODE_UNKNOWN);
					return result;
				}
			} else {
				log.warn("Eigther the bonus is null or the bonus is not in active/granted state");
				result.setErrorCode(ERROR_CODE_UNKNOWN);
			}
			// BonusManagerBase.updateBonusUser(request.getBonusId(), request.getBonusStateId(), Writer.WRITER_ID_MOBILE);
		} catch (Exception e) {
			log.error("Error in updateBonusStatus", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
			return result;
		}

		return result;
	}

    /**
     * @param request - Method Request
     * @return the OptionsMethodResult with list of Credit Card Types Options.
     */
    public static OptionsMethodResult getCreditCardTypesOptions(MethodRequest request) {
    	OptionsMethodResult result = new OptionsMethodResult();
    	log.info("getCreditCardTypesOptions: skinId=" + request.getSkinId());
    	try {
    		HashMap<Long, CreditCardType> ccTypes = CreditCardTypesManagerBase.getCreditCardTypes(request.getSkinId());
    		ArrayList<OptionItem> options = new ArrayList<OptionItem>();
    		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
    		for (Iterator<Long> iter = ccTypes.keySet().iterator(); iter.hasNext();) {
    			Long id = iter.next();
    			options.add(new OptionItem(id.longValue(), CommonUtil.getMessage(locale, ccTypes.get(id).getName(), null), locale));
    		}
    		Collections.sort(options, new OptionItem.AlphaComparator());
    		result.setOptions(options.toArray(new OptionItem[options.size()]));
    	} catch (Exception e) {
            log.error("Error in getCreditCardTypesOptions", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
	}

    /**
     * @param request - Method Request
     * @return the CountriesOptionsMethodResult with list of countries.
     */
    public static CountriesOptionsMethodResult getCountriesOptions(MethodRequest request) {
    	CountriesOptionsMethodResult result = new CountriesOptionsMethodResult();
    	log.info("getCountriesOptions: skinId=" + request.getSkinId());
    	try {
    		Hashtable<Long, Country> countries = CountryManagerBase.getCountries();
    		ArrayList<CountryOptionItem> options = new ArrayList<CountryOptionItem>();
    		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
    		for (Iterator<Long> iter = countries.keySet().iterator(); iter.hasNext();) {
    			Long id = iter.next();
    			Country c = countries.get(id);
    			options.add(new CountryOptionItem(id.longValue(), CommonUtil.getMessage(locale, c.getDisplayName(), null), locale,
    					c.getPhoneCode(), c.getSupportPhone(), c.getSupportFax()));
    		}
    		Collections.sort(options, new OptionItem.AlphaComparator());
    		result.setOptions(options.toArray(new CountryOptionItem[options.size()]));
    	} catch (Exception e) {
            log.error("Error in getCountriesOptions", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
    }

    /**
     * @param request - Method Request
     * @return the OptionsMethodResult with list of cities.
     */
    public static OptionsMethodResult getCitiesOptions(MethodRequest request) {
    	OptionsMethodResult result = new OptionsMethodResult();
    	log.info("getCitiesOptions: skinId=" + request.getSkinId());
    	try {
    		ArrayList<City> cities = CitiesManagerBase.getAllCities();
    		ArrayList<OptionItem> options = new ArrayList<OptionItem>();
    		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
    		options.add(new OptionItem(0, CommonUtil.getMessage(locale, "register.city", null)));//for the first show in the client
    		for (int i = 0 ; i < cities.size() ; i++) {
    			options.add(new OptionItem(cities.get(i).getId(), cities.get(i).getName()));
    		}
    		result.setOptions(options.toArray(new OptionItem[options.size()]));
    	} catch (Exception e) {
            log.error("Error in getCountriesOptions", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
    }

    /**
     * @param request - Method Request
     * @return the OptionsMethodResult with list of banks.
     */
    public static OptionsMethodResult getBanksET(MethodRequest request) {
    	OptionsMethodResult result = new OptionsMethodResult();
    	log.info("getBanksOptions: skinId=" + request.getSkinId());
    	try {
    		HashMap<Long, BankBase> banks = BanksManagerBase.getBanksET(ConstantsBase.BANK_TYPE_DEPOSIT, ConstantsBase.BUSINESSCASE_ID_ET);
    		ArrayList<OptionItem> options = new ArrayList<OptionItem>();
    		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
    		options.add(new OptionItem(0, CommonUtil.getMessage(locale, "transactions.wire.deposit.bank", null)));//for the first show in the client
			Iterator iter = banks.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry e = (Map.Entry)iter.next();
				Object object = e.getValue();
				Class<? extends Object> classDefinition = object.getClass();
				Method mId = classDefinition.getMethod( "getId", (Class[])null);
				Method mName = classDefinition.getMethod("getName", (Class[])null);
				options.add(new OptionItem(Long.parseLong(String.valueOf((mId.invoke(object, (Object[])null)))),CommonUtil.getMessage(locale,(String)mName.invoke(object, (Object[])null),null)));
			}
//            Collections.sort(options, new selectItemComparator());
    		result.setOptions(options.toArray(new OptionItem[options.size()]));
    	} catch (Exception e) {
            log.error("Error in getBanksET", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
    }

    /**
     * @param request - Method Request
     * @return the OptionsMethodResult with list of banksBranches.
     */
    public static OptionsMethodResult getBanksBranchesET(MethodRequest request) {
//    	boolean isGoodbranchCode = false;
//		for (BankBranches bankBranch : BanksManagerBase.getBanksBranches()) {
//            if (request.getBankId().equalsIgnoreCase(bankBranch.getBankId().toString()) && //check that this bank and branch code are real
//            		request.getBranch().equals(bankBranch.getBranchCode().toString())) {
//                isGoodbranchCode = true;
//                break;
//            }
//    	}
//
//        for (BankBranches bankBranch : ApplicationData.getBankBranches()) {
//            if (wire.getBankId().equalsIgnoreCase(bankBranch.getBankId().toString()) && //check that this bank and branch code are real
//                    wire.getBranch().equals(bankBranch.getBranchCode().toString())) {
//                isGoodbranchCode = true;
//                break;
//            }
//        }
//        if (!isGoodbranchCode) {
//            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("error.branch.code",null),null);
//            context.addMessage("wireForm:branch", fm);
//            return null;
//        }


    	OptionsMethodResult result = new OptionsMethodResult();
    	log.info("getBanksBranchesOptions: skinId=" + request.getSkinId());
    	try {
    		ArrayList<BankBranches> banksBranches = BanksManagerBase.getBanksBranches();
    		ArrayList<OptionItem> options = new ArrayList<OptionItem>();
    		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
    		options.add(new OptionItem(0, CommonUtil.getMessage(locale, "transactions.wire.deposit.bank", null)));//for the first show in the client
    		for (int i = 1 ; i < banksBranches.size()  ; i++) {
    			options.add(new OptionItem(banksBranches.get(i).getId(), banksBranches.get(i).getBranchName()));
    		}
    		result.setOptions(options.toArray(new OptionItem[options.size()]));
    	} catch (Exception e) {
            log.error("Error in getBanksET", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
    }

    /**
     * @param request Method Request
     * @return the SkinsOptionsMethodResult with list of Skins.
     */
    public static SkinsOptionsMethodResult getSkinsOptions(MethodRequest request) {
    	SkinsOptionsMethodResult result = new SkinsOptionsMethodResult();
    	log.info("getSkinsOptions: skinId=" + request.getSkinId());
    	try {
    		Hashtable<Long, Skin> skins = SkinsManagerBase.getSkins();
    		ArrayList<SkinOptionItem> options = new ArrayList<SkinOptionItem>();
    		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());

    		if (request.getSkinId() == Skin.SKIN_EN_US ||
					request.getSkinId() == Skin.SKIN_ES_US) {

    			Skin enUs = skins.get(Skin.SKIN_EN_US);
    			Skin esUs = skins.get(Skin.SKIN_ES_US);
    			options.add(new SkinOptionItem(Skin.SKIN_EN_US, CommonUtil.getMessage(locale, enUs.getDisplayName(), null),
    					locale, (int) enUs.getDefaultCountryId(), enUs.getDefaultLanguageId(), enUs.getDefaultCurrencyId()));
    			options.add(new SkinOptionItem(Skin.SKIN_ES_US, CommonUtil.getMessage(locale, esUs.getDisplayName(), null),
    					locale, (int) esUs.getDefaultCountryId(), esUs.getDefaultLanguageId(), esUs.getDefaultCurrencyId()));
			} else {  // not US skins
	    		for (Iterator<Long> iter = skins.keySet().iterator(); iter.hasNext();) {
	    			Long id = iter.next();
	    			if (id.longValue() != Skin.SKIN_ETRADER &&
	    					id.longValue() != Skin.SKIN_API &&
	    					id.longValue() != Skin.SKIN_TLV &&
	    					id.longValue() != Skin.SKIN_SPAINMI) {
		    			Skin s = skins.get(id);
		    			options.add(new SkinOptionItem(id.longValue(), CommonUtil.getMessage(locale, s.getDisplayName(), null),
		    					locale, (int) s.getDefaultCountryId(), s.getDefaultLanguageId(), s.getDefaultCurrencyId()));
	    			}
	    		}
			}
    		Collections.sort(options, new OptionItem.AlphaComparator());
    		result.setOptions(options.toArray(new SkinOptionItem[options.size()]));
    	} catch (Exception e) {
            log.error("Error in SkinsOptionsMethodResult", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
    }

    /**
     * This method validate the user parameters by his user name, password, skin id and login activity.
     * The method initialize the user with the user set and all the data he need and return the result of it.
     * The user is initialize with all the data's if the error code from the user validation is equal to <code>ERROR_CODE_SUCCESS<code>
     * If the user validation is succeed the user login to the system.
     *
     * @param request - User Method Request
     * @return the User.
     * @see #validateAndLoginEncrypt(String, String, MethodResult, long, boolean)
     * @see #validateAndLogin(String, String, MethodResult, long, boolean)
     */
    public static UserMethodResult getUser(UserMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("getUser: " + request);
    	UserMethodResult result = new UserMethodResult();
    	synchronized (httpRequest.getSession()) {
		User sessionUser = (User) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_USER);
		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId())
				.getDefaultLanguageId()).getCode());
		if (sessionUser == null && request.isEncrypt()) {
			result.addUserMessage("", CommonUtil.getMessage(locale, "error.session.disconnected", null));
			result.setErrorCode(ERROR_CODE_SESSION_EXPIRED);
			return result;
		}
		if (request.getUserName() == null && request.getPassword() == null && request.isEncrypt() && sessionUser != null) {
    		request.setUserName(sessionUser.getUserName());
    		request.setPassword(sessionUser.getEncryptedPassword());
    	}
		// save the predefValues and default amount at the end if they're null
		List<BalanceStepPredefValue> predefValues = null;
		Long defaultAmountValue = null;
		Date sessionPreviousLoginTime = null;
		if (sessionUser != null) {
			predefValues = sessionUser.getPredefValues();
			defaultAmountValue = sessionUser.getDefaultAmountValue();
			sessionPreviousLoginTime = sessionUser.getPreviousLoginTime();
		}
			httpRequest.getSession().removeAttribute(ConstantsBase.BIND_SESSION_USER);
			User user = null;
			try {
				if (request.getUserName() != null && request.getPassword() != null && request.isEncrypt()) {
					user = validateAndLoginEncrypt(	request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
													httpRequest);
				} else {
					if (request.getUserName() != null && request.getPassword() != null && !request.isEncrypt()) {
						MethodResult logoutResult = logoutUser(request, httpRequest);
						if (logoutResult.getErrorCode() != ERROR_CODE_SUCCESS) {
							log.error("Unable to execute logoutUser, request: " + request);
							result.setErrorCode(logoutResult.getErrorCode());
							return result;
						}
					}
					user = validateAndLogin(request.getUserName(), request.getPassword(), result, request.getSkinId(), true, true,
											httpRequest);
				}
				if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
					return result;
				}
				if (user.getSkinId() == 0 || (request.getSkinId() == Skin.SKIN_ETRADER && user.getSkinId() != Skin.SKIN_ETRADER)
						|| (request.getSkinId() != Skin.SKIN_ETRADER && user.getSkinId() == Skin.SKIN_ETRADER)
						|| (user.getSkinId() == Skin.SKIN_API && request.getWriterId() != Writer.WRITER_ID_API)) {
					log.info("User login failed.");
					result.setErrorCode(ERROR_CODE_LOGIN_FAILED);
					result.addErrorMessage("", "User login failed");
					result.addUserMessage("", CommonUtil.getMessage(locale, "header.login.error", null));
				}
				
				if(user.isETRegulation() && isOldNonETErgualtioniOSApp(request.getAppVersion())){
	            	 log.debug("Can't login with ET Regulation user, the appVersion is OLD:" + request.getAppVersion());
	            	 result.setErrorCode(ERROR_CODE_LOGIN_FAILED);
	            	 result.addErrorMessage("", "User login failed");
	            	 result.addUserMessage("", CommonUtil.getMessage(locale, "error.login.regulation.old.app", null));
				}
				
				if(!CountryManagerBase.isCanLoginRegUser(user.getSkinId(), request.getIp(), user.getId())){
	            	 log.debug("Can't login with OB user in Block contry " + request.getIp());
	            	 result.setErrorCode(ERROR_CODE_LOGIN_FAILED_OB_BLOCKED_COUNTRY);
	            	 result.addErrorMessage("", "User login failed");
	            	 result.addUserMessage("", "Can't login");
				}
				
				if (result.getErrorCode() == ERROR_CODE_SUCCESS) {
					if (request.getUserName() != null && request.getPassword() != null && !request.isEncrypt()) {
						Date previousLoginTime = user.getTimeLastLogin();
						ArrayList<String> keys;
						if (request.getSkinId() != Skin.SKIN_ETRADER) {
							keys = LoginManager.loginActionFromWeb(user, request.getPassword());
						} else {
							keys = LoginManager.loginActionFromWeb(user, request.getPassword().toUpperCase());
						}
						DeviceFamily deviceFamily = DeviceFamily.getById(request.isTablet() ? 1l : 0l);
						Login login = new Login();
						login.setLoginOffset(CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset()));
						login.setSuccess(keys.isEmpty());
						login.setUserAgent(null);
						login.setDeviceUniqueId(request.getDeviceId());
						login.setLoginFrom(ConstantsBase.LOGIN_FROM_REGULAR);
						login.setWriterId(request.getWriterId());
						login.setServerId(CommonUtil.getServerName().split(":")[1]);
						login.setFingerPrint(request.getFingerPrint());
						login.setOsVersion(request.getOsVersion());
						login.setDeviceType(request.getDeviceType());
						login.setAppVersion(request.getAppVersion());
						login.setDeviceFamilyId(deviceFamily.getId());
						Long combId = request.getLogin().getCombinationId();
						if (combId == null || combId == 0 || 
								MarketingCombinationManagerBase.getById(combId) == null) {
			                combId = SkinsManagerBase.getSkin(user.getSkinId()).getDefaultCombinationId();
			            }
						login.setCombinationId(combId);
						login.setDynamicParam(request.getLogin().getDynamicParam());
						login.setAffSub1(request.getLogin().getAffSub1());
						login.setAffSub2(request.getLogin().getAffSub2());
						login.setAffSub3(request.getLogin().getAffSub3());
						
						if (!keys.isEmpty()) {
							// insert failed login in logins table
							long userLastLoginId = LoginsManager.insertLogin(user.getUserName(), login);
							UsersManagerBase.updateUserLastLoginId(user.getId(), userLastLoginId);
							result.setErrorCode(ERROR_CODE_LOGIN_FAILED);
							for (String key : keys) {
								result.addUserMessage("", CommonUtil.getMessage(locale, key, null));
							}
							return result;
						} else {
							// insert successful login in logins table
							long userLastLoginId = LoginsManager.insertLogin(user.getUserName(), login);
							httpRequest.getSession().setAttribute(ConstantsBase.LOGIN_ID, userLastLoginId);
							try {
								httpRequest.setAttribute(ConstantsBase.SESSION_SKIN, new Long(user.getSkinId()));
							} catch (Exception exc) {
								log.debug("can't init SESSION_SKIN ", exc);
							}
							UsersManagerBase.updateUserLastLoginId(user.getId(), userLastLoginId);
							// load user's balance steps on login
							UserBalanceStepManager.loadUserBalanceStep(user, !TransactionsManagerBase.isFirstDeposit(user.getId(), 0), request.getWriterId());
							try {
								long userMinInvestmentAmount = (request.getWriterId() != Writer.WRITER_ID_BUBBLES_DEDICATED_APP)	? InvestmentsManagerBase.getInvestmentLimit(ConstantsBase.OPPORTUNITY_TYPE_BINARY_,
																																												Opportunity.SCHEDULED_HOURLY,
																																												Market.MARKET_USD_EUR_ID,
																																												user.getCurrencyId(),
																																												user.getId())
																																							.getMinAmount()
																																	: InvestmentsManagerBase.getBDAInvestmentLimit(user.getCurrencyId())
																																							.getMinAmount();
								boolean isFirstDeposit = TransactionsManagerBase.isFirstPossibleDepositExcludeBonus(user.getId());
								if (user.getBalance() < userMinInvestmentAmount && !isFirstDeposit) {
									result.setMinIvestmentAmount(userMinInvestmentAmount);
								}
							}  catch (Exception e) {
								log.error("Error with checking user min investment amount: ", e);
							}
							// insert bonus
							if (request.isLogin()) {
								UsersAwardBonus usersAwardBonus = UsersAwardBonusManagerBase.getByUserId(user.getId());
								if (null != usersAwardBonus) {
									BonusManagerBase.insertBonusWithIssue(	user, usersAwardBonus.getId(),
																			usersAwardBonus.getUsersAwardBonusGroup().getBonusId(),
																			request.getWriterId(),
																			String.valueOf(IssuesManagerBase.ISSUE_SUBJECTS_INTERNAL),
																			String.valueOf(IssuesManagerBase.ISSUE_STATUS_CLOSED),
																			user.getId(),
																			String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW),
																			ConstantsBase.ISSUE_TYPE_REGULAR,
																			String.valueOf(IssuesManagerBase.ISSUE_ACTION_COMMENT), false,
																			new Date(), ConstantsBase.OFFSET_GMT,
																			usersAwardBonus.getUsersAwardBonusGroup().getDescription(),
																			request.getIp());
								}
								try {
									log.debug("Trying to insert auto bonus upon login for login id " + userLastLoginId);
					    	    	// Insert Automatic Bonus.
					    	    	long autoBonusId = BonusManagerBase.getAutoBonusByLoginId(userLastLoginId);

					    	    	if (autoBonusId > 0) {
					    	    		BonusUsers bonusUser = new BonusUsers();
					    	    		bonusUser.setUserId(user.getId());
					    	    		bonusUser.setWriterId(user.getWriterId());
					    	    		bonusUser.setBonusId(autoBonusId);
					    	    		BonusManagerBase.insertBonusUser(bonusUser, user, user.getWriterId(), 0, 0, request.getIp());
					    	    	}
					        	} catch (Exception e) {
					        		log.error("Failed to insert auto bonus upon login for login Id " + userLastLoginId, e);
					    		}
							}
							if (request.getWriterId() == Writer.WRITER_ID_COPYOP_WEB) {
								user.setPreviousLoginTime(previousLoginTime);
							}
						}
					}
					// add the user in session
					httpRequest.getSession().setAttribute(ConstantsBase.BIND_SESSION_USER, user);

					result = initUser(result, user, request.getUtcOffset(), request.getWriterId());

					// Marketing Tracking Set mId by UserId when user Login
					try {
						if (request.isLogin()) {
							log.debug("Marketing Tracking check mId when login for  UserId:" + user.getId());
							result.setmId(MarketingTrackingManager.getMidByUserId(user.getId()));
						}
					} catch (Exception e) {
						log.error("Marketing Tracking check mId when login", e);
					}
				}
				if (defaultAmountValue==null || defaultAmountValue==0L) {
					UserBalanceStepManager.loadUserBalanceStep(user, !TransactionsManagerBase.isFirstDeposit(user.getId(), 0), request.getWriterId());
					defaultAmountValue=user.getDefaultAmountValue();
				}
				if (predefValues==null) {
					predefValues=user.getPredefValues();
				}
				setUserAdditionalValues(user, predefValues, defaultAmountValue, sessionPreviousLoginTime);
			} catch (Exception e) {
				log.error("Error in login user", e);
				result.setErrorCode(ERROR_CODE_UNKNOWN);
			}
		}
    	return result;
    }
    
    private static void setUserAdditionalValues(User user, List<BalanceStepPredefValue> predefValues, Long defaultAmountValue, Date sessionPreviousLoginTime) {
    	if (user.getPredefValues() == null) {
    		user.setPredefValues(predefValues);
    	}
    	if (user.getDefaultAmountValue() == null) {
    		user.setDefaultAmountValue(defaultAmountValue);
    	}
    	if (user.getPreviousLoginTime() == null) {
    		user.setPreviousLoginTime(sessionPreviousLoginTime);
    	}
    }

    /**
     * @param request - User Method Request
     * @return the UserMethodResult by Encrypted Password.
     */
    public static UserMethodResult getUserEncryptedPass(UserMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("getUserEncryptedPass: " + request);
    	UserMethodResult result = new UserMethodResult();
    	try {
    		// decode password
    		request.setPassword(AESUtil.decrypt(request.getPassword()));
    		result = getUser(request, httpRequest);
    		if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
    			return result;
    		}
    	} catch (Exception e) {
            log.error("Error in login user with encrypted password", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
    }

    /**
     *
     * Insert a user to the data base.
     *
     * This method use Register object to set the user parameter, like skin id, ip, languageId, DeviceUniqueId, UtcOffset and etc.
     * The Register object contains all the relevant data of the user registration (see the link below).
     * This method validate the input and return with the result a message respectively according to the inputs validation.
     * After checking if the user is valid insert it to the data base.
     *
     *
     * @param request Contains Register object, SkinId, IP, DeviceId, UtcOffset,
     * @return UserMethodResult
     * @see com.anyoption.common.beans.base.Register
     */
    public static UserMethodResult insertUser(InsertUserMethodRequest request, HttpServletRequest httpRequest) {
    	UserMethodResult result = new UserMethodResult();
    	log.info("insertUser: " + request.getRegister());
    	request.getRegister().setSkinId(request.getSkinId());
    	boolean isAPIUser = request.getWriterId() == Writer.WRITER_ID_API? true : false;
    	Skin s = null;
    	if (null != request.getRegister()) {
    		s = SkinsManagerBase.getSkin(request.getRegister().getSkinId());
    	}
    	if (null == s) {
    		result.setErrorCode(ERROR_CODE_INVALID_INPUT);
    		result.addErrorMessage("skinId", "Invalid skin id");
    		return result;
    	}
    	Locale locale = new Locale(LanguagesManagerBase.getLanguage(s.getDefaultLanguageId()).getCode());
    	// check whether the user is from banned country and/or skin
    	String ip = request.getIp();
    	if (isAPIUser) {
    		ip = request.getRegister().getIp();
    	}
    	long countryIdByIp = CommonUtil.getCountryIdByIp(request.getSkinId(), ip);  
		if (request.getSkinId() == Skin.SKIN_EN_US || request.getSkinId() == Skin.SKIN_ES_US || request.getSkinId() == Skin.SKIN_ARABIC
				|| CountryManagerBase.isCountryForbidenAndIp(ip, request.getSkinId(), request.getRegister().getCountryId(), null)) {
			result = getForbiddenCountryMessage(request.getSkinId(), countryIdByIp, locale);
    		return result;
    	}

    	try {
    		 if (CommonUtil.IsParameterEmptyOrNull(request.getRegister().getIp())) {
    			 request.getRegister().setIp(request.getIp());
    		 }
             request.getRegister().setLanguageId(s.getDefaultLanguageId());
             request.getRegister().setDeviceUniqueId(request.getDeviceId());
             request.getRegister().setUtcOffsetCreated(CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset()));
             //  TODO: refactor UsersManagerBase.validateRegisterForm to commons validator framework
             ArrayList<ErrorMessage> msgs = validateInput("register", request.getRegister(), locale);
             if (!msgs.isEmpty()) {
                 result.setErrorCode(ERROR_CODE_INVALID_INPUT);
                 result.addUserMessages(msgs);
                 return result;
             }

             Map<String, MessageToFormat> mtf = UsersManagerBase.validateRegisterForm(request.getRegister(), isAPIUser, request.getWriterId());
             if (!mtf.isEmpty()) {
                 result.setErrorCode(ERROR_CODE_INVALID_INPUT);
                 for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
                     String key = i.next();
                     MessageToFormat m = mtf.get(key);
                     String apiErrorCode = null;
                     if (m.getErrorMsgType() == MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD) {
                    	 if (m.getKey() != null && m.getKey().equals("error.register.email.inuse")) {
                    		 result.setErrorCode(ERROR_CODE_EMAIL_IN_USE);
                    	 } else if (m.getKey() != null && m.getKey().equals("error.register.username.login.with.ao")) {
                    		 result.setErrorCode(ERROR_CODE_LOGIN_WITH_AO_ACCOUNT);
                    	 } else {
                        	 result.setErrorCode(ERROR_CODE_VALIDATION_WITHOUT_FIELD);
                    	 }
                     }
                     if (m.getKey() != null && m.getKey().equals("error.register.email.inuse")) {
                    	 apiErrorCode = ApiErrorCode.API_ERROR_CODE_VALIDATION_EMAIL_IN_USE;	 
                     }
                     result.addUserMessage(key, CommonUtil.getMessage(locale, m.getKey(), m.getParams()), apiErrorCode);
                 }
                 return result;
             }
             
             //Can't create ET user from iOS with Old app (ver <=  12.10.26)
             if(request.getRegister().getSkinId() == Skin.SKIN_ETRADER && isOldNonETErgualtioniOSApp(request.getAppVersion())){
            	 log.debug("Can't create user, the appVersion is OLD:" + request.getAppVersion());
            	 result.setErrorCode(ERROR_CODE_UNKNOWN);
            	 result.addUserMessage("", CommonUtil.getMessage(locale, "error.register.regulation.old.app", null));
            	 return result; 
             }
             request.getRegister().setIdNum(ConstantsBase.ID_NUM_DEFAULT_VALUE);
             
             //Define Skin by IP, select Country and current Skin
             long defineSkinId = SkinsManagerBase.getSkinIdInsertUser(request.getIp(), request.getRegister().getCountryId(), request.getRegister().getSkinId());
             request.setSkinId(defineSkinId);
    		 request.getRegister().setSkinId(defineSkinId);
             
    		 // Change user details.
             Register register = request.getRegister();
             ArrayList<UsersDetailsHistory> usersDetailsHistoryList = null;
             try {
                 HashMap<Long, String> userDetailsBeforeChangedHM = UsersManagerBase.getUserDetailsHM(register.getSkinId(), null, ConstantsBase.NON_SELECTED, null, null, null, null, false, false, null, null, null, null, null, ConstantsBase.NON_SELECTED, null, null, null, null);
                 HashMap<Long, String> userDetailsAfterChangedHM = UsersManagerBase.getUserDetailsHM(register.getSkinId(), register.getStreet(), register.getCountryId(), register.getGender(), register.getEmail(), register.getMobilePhone(),
                         register.getLandLinePhone(), register.isContactByEmail(), register.isContactBySms(), register.getBirthDay(), register.getBirthMonth(), register.getBirthYear(), register.getCityName(), register.getIdNum(), register.getStateId(),
                         register.getFirstName(), register.getLastName(), register.getPassword(), null);
                 usersDetailsHistoryList = UsersManagerBase.checkUserDetailsChange(userDetailsBeforeChangedHM, userDetailsAfterChangedHM);
             } catch (Exception e) {
                 log.error("Error when get user details HM", e);
             }
 			 DeviceFamily deviceFamily = DeviceFamily.getById(request.isTablet() ? 1l : 0l);

 			//TODO Refactor, remove this ugly code when all clients will send request with platform id.
             if (request.getRegister().getPlatformId() == 0) {
            	 register.setPlatformId(Platform.ANYOPTION);
            	 if (register.getSkinId() == Skin.SKIN_ETRADER) {
            		 register.setPlatformId(Platform.ETRADER);
            	 }
                 if (request.getWriterId() == Writer.WRITER_ID_COPYOP_MOBILE || request.getWriterId() == Writer.WRITER_ID_COPYOP_WEB || request.getWriterId() == Writer.WRITER_ID_CO_MINISITE) {
                	 register.setPlatformId(Platform.COPYOP);
                 }
             }
             
             //ET Regulation set false for terms we have additional accepting the agreement
             if (CommonUtil.isHebrewSkin(request.getSkinId())) {
            	 register.setTerms(false);
             }

             User user = UsersManagerBase.insertUser(register, request.getWriterId(), request.isBaidu(), request.getOsVersion(), request.getDeviceType(), request.getAppVersion(), deviceFamily, request.getFingerPrint());
             result = initUser(result, user, request.getUtcOffset(),request.getWriterId());
             // add the user in session
             httpRequest.getSession().setAttribute(ConstantsBase.BIND_SESSION_USER, user);
             httpRequest.getSession().setAttribute(ConstantsBase.LOGIN_ID, user.getLastLoginId());
             try {
				if (null != request.getRegister().getCombinationId()) {
					 PixelGenerator.runPixel(request.getRegister().getCombinationId(), PixelGenerator.PIXEL_TYPE_REGISTRATION, request.getDeviceId(), request.getIp());
				 }
             } catch (RuntimeException e) {
            	 log.error("cant run pixel", e);
             }
             // Change user details.
             if (null != usersDetailsHistoryList) {
                 UsersManagerBase.insertUsersDetailsHistory(request.getWriterId(), user.getId(), user.getUserName(), usersDetailsHistoryList, String.valueOf(user.getClassId()));
             }

             //Insert login token when register from API
             if (isAPIUser && user.getId() > 0) {
                 String uuid = ApiManager.insertLoginToken(result.getUser().getId());
                 log.info("Insert UUID:" + uuid + " for userId:" + result.getUser().getId());
                 result.getUser().setLoginToken(uuid);
             }

             if (user.getId() > 0) {
            	 RewardUserTasksManager.rewardTasksHandler(TaskGroupType.REGISTRAITION, user.getId(), 0, user.getId(), BonusManagerBase.class, Writer.WRITER_ID_MOBILE);
             }
             if (user.getWriterId() != Writer.WRITER_ID_MOBILE && user.getWriterId() != Writer.WRITER_ID_COPYOP_MOBILE
            		 && user.getWriterId() != Writer.WRITER_ID_BUBBLES_DEDICATED_APP) {
            	 new RegisterSMSThread(user, locale, user.getPlatformId(), user.getWriterId()).start();
             } else {
            	 SMSManagerBase.sendPhoneNumberVerificationMessage(user, user.getLocale(), user.getPlatformId());
             }
             // Insert Marketing Tracking
			try {
			    if (user.getId() > 0){
			        log.debug("Marketing Tracking begin track User inserting for userId: " + user.getId());
    			    //If Marketing Tracking param. is null set default
    	            if (CommonUtil.IsParameterEmptyOrNull(request.getMarketingStaticPart()) && CommonUtil.IsParameterEmptyOrNull(request.getmId())){
    	                log.debug("Marketing Tracking staticPart/mId is null set default ");
    	                String marketingStaticPart = MarketingTrackerBase.createDefaultStatciCookieData(user.getCombinationId(), user.getHttpReferer(), user.getDynamicParam(), request.getUtmSource(), user.getSkinId(), user.getAffSub1(), user.getAffSub2(), user.getAffSub3());
    	                String mId = MarketingTrackerBase.getMidFromStaticPart(marketingStaticPart);

    	                request.setMarketingStaticPart(marketingStaticPart);
    	                request.setmId(mId);
    	            }

//    	            if (!CommonUtil.IsParameterEmptyOrNull(request.getMarketingStaticPart())) {
//    	                log.debug("Marketing Tracking Check mId for userId: " + user.getId() + "  and current static part : " + request.getMarketingStaticPart());
//    	                String existMarketingStaticPart = MarketingTrackerBase.checkStaticPartByContactDetailsInsertingUser(user.getEmail(), user.getMobilePhone(), user.getLandLinePhone(), user.getContactId(), user.getId(), request.getMarketingStaticPart());
//
//    	                if (existMarketingStaticPart != null) {
//    	                    request.setMarketingStaticPart(existMarketingStaticPart);
//    	                    log.debug("Marketing Tracking found exist mId for userId: " + user.getId() + " and  replace with new value : " + existMarketingStaticPart);
//    	                }
//    	            }
//    	            if (!CommonUtil.IsParameterEmptyOrNull(request.getmId())) {
//    	                log.debug("Marketing Tracking Check mId for userId: " + user.getId() + "  and current static part : " + request.getmId());
//    	                String existMId = MarketingTrackerBase.checkMidByContactDetailsInsertingUser(user.getEmail(), user.getMobilePhone(), user.getLandLinePhone(), user.getContactId(), user.getId(), request.getmId(), request.getSkinId());
//    	                if (existMId != null) {
//    	                    request.setmId(existMId);
//    	                    log.debug("Marketing Tracking found exist mId for userId: " + user.getId() + " and replace wih new mId value : " + existMId);
//    	                }
//    	            }

//    	            //ETS Marketing
//    	            String etsMidExist = MarketingETSMobile.checkMarketingEtsMidInsertingUser(request.getEtsMId(), user.getEmail(), user.getMobilePhone(), user.getLandLinePhone(), user.getContactId(), user.getId());
//    	            if (etsMidExist != null){
//    	                request.setEtsMId(etsMidExist);
//    	            }

    	            log.debug("Marketing Tracking insert Activity fror userId:" + user.getId());
    					MarketingTracker.insertMarketingTrackerJson(request.getMarketingStaticPart(), request.getmId(), user.getContactId(), user.getId(), ConstantsBase.MARKETING_TRACKING_FULL_REG);
    					result.setmId(request.getmId());
    					result.setMarketingStaticPart(request.getMarketingStaticPart());
				}
			} catch (Exception e) {
				log.error("When inserting user Marketing Tracking", e);
			}
    	} catch (Exception e) {
            log.error("Error in registration", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
    }

    private static UserMethodResult getForbiddenCountryMessage(long skinId, long countryIdByIp, Locale locale) {
    	UserMethodResult result = new UserMethodResult();
    	result.setErrorCode(ERROR_CODE_VALIDATION_WITHOUT_FIELD);
		if (skinId == Skin.SKIN_EN_US || skinId == Skin.SKIN_ES_US || countryIdByIp == ConstantsBase.COUNTRY_ID_US) {
			result.addUserMessage("", CommonUtil.getMessage("us.citizens.not.accept", null, locale));
		} else if (skinId == Skin.SKIN_ARABIC) {
			result.addUserMessage("", CommonUtil.getMessage("CMS.no.register.msg", null, locale));
		} else {
			result.addUserMessage("", CommonUtil.getMessage("error.register.forbidden", null, locale));
		}
		return result;
	}

	/**
     * update an existing user.
     *
     * @param request - Update User Method Request
     * @return MethodResult.
     */
    public static MethodResult updateUser(UpdateUserMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("updateUser: " + request);
    	MethodResult result = new MethodResult();
    	try {
    	    User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
    		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
    		ArrayList<ErrorMessage> msgs = validateInput("updateUser", request.getUser(), locale);
            if (!msgs.isEmpty()) {
                result.setErrorCode(ERROR_CODE_INVALID_INPUT);
                result.addUserMessages(msgs);
                return result;
            }
            Map<String, MessageToFormat> mtf = UsersManagerBase.validateUpdateUserDetails(request.getUser(), locale);
            if (!mtf.isEmpty()) {
                result.setErrorCode(ERROR_CODE_INVALID_INPUT);
                for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
                    String key = i.next();
                    MessageToFormat m = mtf.get(key);
                    result.addUserMessage(key, CommonUtil.getMessage(locale, m.getKey(), m.getParams()));
                }
                return result;
            }

            Date timeBirthDate = user.getTimeBirthDate();
            if (request.getUser().getBirthYearUpdate() != null && request.getUser().getBirthMonthUpdate() != null && request.getUser().getBirthDayUpdate() != null) {
	            Calendar c = new GregorianCalendar(Integer.parseInt(request.getUser().getBirthYearUpdate()), Integer.parseInt( request.getUser().getBirthMonthUpdate()) - 1,
	                    Integer.parseInt(request.getUser().getBirthDayUpdate()), 0, 1);

	            request.getUser().setTimeBirthDate(new Date(c.getTimeInMillis()));
	            timeBirthDate = request.getUser().getTimeBirthDate();
            }
            // Change user details.
            ArrayList<UsersDetailsHistory> usersDetailsHistoryList = null;
            try {
                HashMap<Long, String> userDetailsBeforeChangedHM = UsersManagerBase.getUserDetailsHM(user.getSkinId(), user.getStreet(), user.getCountryId(), user.getGender(), user.getEmail(), user.getMobilePhone(),
                        user.getLandLinePhone(), user.getIsContactByEmail() == 1 ? true : false, user.isContactBySMS(), null, null, null, user.getCityName(), user.getIdNum(), user.getState(),
                        user.getFirstName(), user.getLastName(), user.getPassword(), user.getTimeBirthDate());
                HashMap<Long, String> userDetailsAfterChangedHM = UsersManagerBase.getUserDetailsHM(request.getUser().getSkinId(), request.getUser().getStreet(), request.getUser().getCountryId(), request.getUser().getGender(), request.getUser().getEmail(), request.getUser().getMobilePhone(),
                        request.getUser().getLandLinePhone(), request.getUser().getIsContactByEmail() == 1 ? true : false, request.getUser().isContactBySMS(), null, null, null, request.getUser().getCityName(), request.getUser().getIdNum(), request.getUser().getState(),
                        request.getUser().getFirstName(), request.getUser().getLastName(), request.getUser().getPassword(), timeBirthDate);
                usersDetailsHistoryList = UsersManagerBase.checkUserDetailsChange(userDetailsBeforeChangedHM, userDetailsAfterChangedHM);
            } catch (Exception e) {
                log.error("Error when get user details HM", e);
            }

            UsersManagerBase.updateUserDetails(request.getUser());
            UserRegulationBase userRegulation = new UserRegulationBase();
            userRegulation.setUserId(user.getId());
            userRegulation.setRegularReportMail(request.isRegularReportMail());
            UserRegulationManager.updateRegularReportMail(userRegulation);
            // Change user details.
            if (null != usersDetailsHistoryList) {
                UsersManagerBase.insertUsersDetailsHistory(request.getWriterId(), user.getId(), user.getUserName(), usersDetailsHistoryList, String.valueOf(user.getClassId()));
            }
			if (!user.getMobilePhone().equals(request.getUser().getMobilePhone())
					|| user.getCountryId() != request.getUser().getCountryId()) {
				int platformId = (request.getWriterId() == Writer.WRITER_ID_COPYOP_WEB
										|| request.getWriterId() == Writer.WRITER_ID_COPYOP_MOBILE
									|| request.getWriterId() == Writer.WRITER_ID_CO_MINISITE)	? ConstantsBase.PLATFORM_ID_COPYOP
																								: ConstantsBase.PLATFORM_ID_ANYOPTION;
				SMSManagerBase.sendPhoneNumberVerificationMessage(request.getUser(), locale, platformId);
			}
            refreshUserInSession(user.getId(), httpRequest);
            result.addUserMessage(null, CommonUtil.getMessage(locale, "user.update.success", null));
    	} catch (Exception e) {
            log.error("Error in updateUser", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
    }

    /**
     * This method gets all of the users transactions.
     * @param request - FormsMethodRequest. Contains UserName, Password, SkinId, From, To, StartRow, PageSize, UtcOffset.
     * @return user transactions.
     */
    public static TransactionsMethodResult getUserTransactions(FormsMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("getUserTransactions: " + request);
    	TransactionsMethodResult result = new TransactionsMethodResult();
    	try {
    		User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
            Date fromTime = null;
            Date toTime = null;
            if (null != request.getFrom()) {
            	fromTime = request.getFrom().getTime();
            }
            if (null != request.getTo()) {
            	toTime = request.getTo().getTime();
            }

            ArrayList<Transaction> transactions = TransactionsManagerBase.getTransactionsByUserAndDates(user.getId(), fromTime,
            		toTime, request.getStartRow(), request.getPageSize());
            Transaction[] arr = new Transaction[transactions.size()];
            String utcOffset = CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset());
            Locale l = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(user.getSkinId()).getDefaultLanguageId()).getCode());
            for (int i = 0; i < arr.length; i++) {
            	Transaction t = transactions.get(i);
            	TransactionFormater.initFormattedValues(t, l, utcOffset);
            	arr[i] = t;
            }
            result.setTransactions(arr);
    	} catch (Exception e) {
    		 log.error("Error in getUserTransactions", e);
             result.setErrorCode(ERROR_CODE_UNKNOWN);
    	}
    	return result;
    }

    /**
     *
     * @param request - Chart Data Method Request
     * @param oc - Opportunity Cache
     * @param chc - Chart History Cache
     * @param wlc - Web Levels Cache
     * @return chart with the data sets.
     * @see com.anyoption.json.results.ChartDataMethodResult
     * @see com.anyoption.common.beans.base.Opportunity
     */
    @PrintLogAnnotations(stopPrintDebugLog = true)
    public static ChartDataMethodResult getChartData(ChartDataMethodRequest request, OpportunityCache oc, ChartHistoryCache chc, WebLevelsCache wlc, PastExpiriesCache expiriesCache, HttpServletRequest httpRequest) {
    	log.debug("getChartData: " + request);
    	long t = System.currentTimeMillis();
        ChartDataMethodResult result = new ChartDataMethodResult();
        try {
            if (request.getMarketId() == 0) {
                if (0 <= request.getBox() && request.getBox() <= 3) {
                    if (request.getOpportunityTypeId() == Opportunity.TYPE_REGULAR) {
                        log.debug("marketId is 0. Taking home page market for box: " + request.getBox());
                        result.setMarketId(wlc.getMarketsOnHomePage(request.getSkinId())[request.getBox()]);
                    } else {
                        log.debug("marketId is 0. Calculating Option+ market for skin " + request.getSkinId());
                        ArrayList<Long> l = OpportunitiesManagerBase.getOptionPlusCurrentMarket(request.getSkinId());
                        if (l.size() > 0) {
                            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+0"));
                            int h = cal.get(Calendar.HOUR_OF_DAY);
                            int m = cal.get(Calendar.MINUTE);
                            cal.set(Calendar.HOUR_OF_DAY, 0);
                            cal.set(Calendar.MINUTE, 0);
                            cal.set(Calendar.SECOND, 0);
                            cal.set(Calendar.MILLISECOND, 0);
                            long seed = cal.getTimeInMillis() + (h * 60 + m) / 5;
                            if (log.isDebugEnabled()) {
                                log.debug("cal: " + cal.getTimeInMillis() + " h: " + h + " m: " + m + " seed: " + seed);
                            }
                            /**
                             * The "random" number generated by the Random class depends on the seed. With the above calculation
                             * we make sure that on all web servers we get the same seed for every 5 min period starting from
                             * 00:00 GMT (thus the same "random" number).
                             *
                             * NOTE: large groups of consecutive seed numbers generate the same random number for a small interval
                             */
                            Random rnd1 = new Random(seed);
                            Random rnd2 = new Random(rnd1.nextLong());
                            result.setMarketId(l.get(rnd2.nextInt(l.size())));
                        }
                    }
                } else {
                    log.warn("Invalid box number: " + request.getBox());
                }
            } else {
                result.setMarketId(request.getMarketId());
            }
            Market m = MarketsManagerBase.getMarket(result.getMarketId());
            if (null != m) {
                result.setDecimalPoint(m.getDecimalPoint().intValue());
                //set market name
                Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
                result.setMarketName(MarketsManagerBase.getMarketName(request.getSkinId(), m.getId()));
                result.setOpportunityTypeId(Opportunity.TYPE_REGULAR);
                if (m.isOptionPlus()) {
	                result.setOpportunityTypeId(Opportunity.TYPE_OPTION_PLUS);
	                long currencyId = UsersManagerBase.getCurrencyIdByUserName(request.getUserName());
	                if(currencyId == 0) {
	                	currencyId = new Long(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultCurrencyId());
	                }
	                result.setCommission(FeesManagerBase.getFee(FeesManagerBase.OPTION_PLUS_COMMISSION_FEE, currencyId));
                }
            }
            List<Investment> investments = null;
            MethodResult userResult = new MethodResult();
            User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), userResult, request.getSkinId(), false, httpRequest);
            if (userResult.getErrorCode() != ERROR_CODE_SUCCESS) {
            	log.trace("No user, this probably is normal, the error code is: " + userResult.getErrorCode());
            } else {
            	// get investments
            	String utcOffset = CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset());
				long marketIdForInvestments = (request.getOpportunityTypeId() != Opportunity.TYPE_OPTION_PLUS) ? request.getMarketId() : 0l;
				investments = InvestmentsManagerBase.getOpenedInvestmentsByUser(user.getId(), 0l, result.getOpportunityTypeId(),
																				marketIdForInvestments);
            	Investment[] investmentArr = new Investment[investments.size()];
                for (int i = 0 ; i < investments.size() ; i++) {
                    investmentArr[i] = investments.get(i);
                    InvestmentHelper.initInvestment(investmentArr[i], request.getSkinId(), utcOffset);
                }
                result.setInvestments(investmentArr);
                log.debug("Got " + investmentArr.length + " investments for user " + user.getUserName());
                result = (ChartDataMethodResult) initUser(result, user, request.getUtcOffset(), request.getWriterId());
            }
            SortedSet<WLCOppsCacheBean> marketOpportunities = wlc.getMarketOpportunities(m.getId());
            WLCOppsCacheBean opp = null;
            if (marketOpportunities != null && !marketOpportunities.isEmpty()) {
            	Iterator<WLCOppsCacheBean> marketOpportunitiesIterator = wlc.getMarketOpportunities(m.getId()).iterator();            	
            	if (request.getOpportunityId() == 0 && request.getScheduled() < Opportunity.SCHEDULED_DAYLY) {
            		opp = marketOpportunitiesIterator.next();
	            	if (opp.getState() > Opportunity.STATE_LAST_10_MIN) {
	            		boolean shouldChangeToNewOpp = false;
	            		WLCOppsCacheBean newOpp = null;
	            		if (marketOpportunitiesIterator.hasNext()) {
	            			newOpp = marketOpportunitiesIterator.next();
	            			if (newOpp.getSchedule() == 1) {
	            				shouldChangeToNewOpp = true;
	            			}
	            		}
	            		if (investments == null) {
	            			opp = shouldChangeToNewOpp ? newOpp : opp;
	            		} else {
	            			// investments check
	            			Iterator<Investment> investmentsIterator = investments.iterator();
	            			boolean changeOpp = true;
	            			while (investmentsIterator.hasNext()) {
	            				Investment investment = investmentsIterator.next();
	            				if (investment.getOpportunityId() == opp.getOpportunityId()) {
	            					changeOpp = false;
	            					break;
	            				}
	            			}
	            			if (changeOpp && shouldChangeToNewOpp) {
	            				opp = newOpp;
	            			}
	            		}
	            	}
            	} else if (request.getOpportunityId() > 0) {
            		WLCOppsCacheBean tmp = null;
            		do {
            			tmp = marketOpportunitiesIterator.next();
            			if (tmp.getOpportunityId() == request.getOpportunityId()) {
            				opp = tmp;
            				break;
            			}
            		} while (marketOpportunitiesIterator.hasNext());
				} else if (request.getScheduled() == Opportunity.SCHEDULED_DAYLY	|| request.getScheduled() == Opportunity.SCHEDULED_WEEKLY
							|| request.getScheduled() == Opportunity.SCHEDULED_MONTHLY
							|| request.getScheduled() == Opportunity.SCHEDULED_QUARTERLY) {
					// there is a case when the weekly ends on the next month, the monthly opp will be before the weekly opp
					boolean applicantFound = false;
					while (marketOpportunitiesIterator.hasNext()) {
						WLCOppsCacheBean cacheOpp = marketOpportunitiesIterator.next();
						if (cacheOpp.getSchedule() == request.getScheduled()) {
							opp = cacheOpp;
							break;
						}
						if (!applicantFound && cacheOpp.getSchedule() > request.getScheduled()) {
							opp = cacheOpp;
							applicantFound = true;
						}
					}
				}
            	result.setOpportunityId((opp != null) ? opp.getOpportunityId() : 0l);
            }

            if (log.isDebugEnabled()) {
                log.debug("getChartData: marketId: " + result.getMarketId() + " oppId: " + result.getOpportunityId() + " time to fill: " + (System.currentTimeMillis() - t));
            }
            result.setTimezoneOffset(0);
            if (null != opp) {
            	OpportunityCacheBean oppCacheBean = oc.getOpportunity(result.getOpportunityId());
                result.setTimeEstClosing(opp.getTimeEstClosing());
                result.setScheduled(opp.getSchedule());
                result.setTimeFirstInvest(oppCacheBean.getTimeFirstInvest());
                result.setTimeLastInvest(oppCacheBean.getTimeLastInvest());
                if (opp.getSchedule() < Opportunity.SCHEDULED_DAYLY) {
	                Map<SkinGroup,List<MarketRate>> ratesMap = chc.getMarketHistory(result.getMarketId());
	                List<MarketRate> l = null;
	                if (ratesMap != null) {
	                	l = ratesMap.get(SkinsManagerBase.getSkin(request.getSkinId()).getSkinGroup());
	                }
	                log.debug("got rates: " + (null != l ? l.size() : 0));
	                if (null != l) {
	                    // leave only current opp history
	                    long tfi = oppCacheBean.getTimeFirstInvest().getTime();
	                    if (l.size() > 0 && l.get(0).getRateTime().getTime() < tfi) {
	                        result.setHasPreviousHour(true);
	                    }
	                    while (l.size() > 0 && l.get(0).getRateTime().getTime() < tfi) {
	                        l.remove(0);
	                    }
	                    // trim the rates to 1 in 12 sec (in db we store 1 in 6 sec)
	                    for (int i = (l.size() % 2 == 0 ? l.size() - 1 : l.size() - 2); i > 0; i -= 2) {
	                        l.remove(i);
	                    }
	                    log.debug("return rates: " + l.size());
	
	                	// TODO: get rid of the ugly translation
	                	// result.setRates(l.toArray(new MarketRate[0]));
	//                    com.anyoption.beans.base.MarketRate[] rates = new com.anyoption.beans.base.MarketRate[l.size()];
	                    long[] ratesTimes = new long[l.size()];
	                    double[] ratesValues = new double[l.size()];
	                    double[] lasts = null;
	                    double[] asks = null;
	                    double[] bids = null;
	                    if (l.size() > 0 && l.get(0).getLast() != null) {
	                    	lasts = new double[l.size()];
	                    }
	                    if (l.size() > 0 && l.get(0).getAsk() != null) {
	                    	asks = new double[l.size()];
	                    }
	                    if (l.size() > 0 && l.get(0).getBid() != null) {
	                    	bids = new double[l.size()];
	                    }
	                    MarketRate mr = null;
	                    for (int i = 0; i < l.size(); i++) {
	                        mr = l.get(i);
	//                        rates[i] = new com.anyoption.beans.base.MarketRate(mr.getRateTime(), mr.getGraphRate().doubleValue(), mr.getDayRate().doubleValue(), 0f);
	                        ratesTimes[i] = mr.getRateTime().getTime();
	                        ratesValues[i] = mr.getGraphRate().doubleValue();
	                        if (lasts != null) {
	                        	lasts[i] = mr.getLast().doubleValue();
	                        }
	                        if (asks != null) {
	                        	asks[i] = mr.getAsk().doubleValue();
	                        }
	                        if (bids != null) {
	                        	bids[i] = mr.getBid().doubleValue();
	                        }
	                    }
	//                    result.setRates(rates);
	                    result.setRatesTimes(ratesTimes);
	                    result.setRates(ratesValues);
	                    result.setLasts(lasts);
	                    result.setAsks(asks);
	                    result.setBids(bids);
	                }
				} else if (opp.getSchedule() == Opportunity.SCHEDULED_DAYLY	|| opp.getSchedule() == Opportunity.SCHEDULED_WEEKLY
							|| opp.getSchedule() == Opportunity.SCHEDULED_MONTHLY || opp.getSchedule() == Opportunity.SCHEDULED_QUARTERLY) {
					Set<com.anyoption.common.beans.Opportunity> oppsCache;
					switch (opp.getSchedule()) {
					case Opportunity.SCHEDULED_DAYLY:
						oppsCache = expiriesCache.getEndOfDayExpiries(opp.getMarketId());
						break;

					case Opportunity.SCHEDULED_WEEKLY:
						oppsCache = expiriesCache.getEndOfWeekExpiries(opp.getMarketId());
						break;

					case Opportunity.SCHEDULED_MONTHLY:
						oppsCache = expiriesCache.getEndOfMonthExpiries(opp.getMarketId());
						break;

					case Opportunity.SCHEDULED_QUARTERLY:
						oppsCache = expiriesCache.getEndOfQuarterExpiries(opp.getMarketId(), oppCacheBean.getTimeFirstInvest());
						break;

					default:
						oppsCache = new TreeSet<>();
						break;
					}
					Iterator<com.anyoption.common.beans.Opportunity> iterator = oppsCache.iterator();
					long[] ratesTimes = new long[oppsCache.size()];
					double[] ratesValues = new double[oppsCache.size()];
					double[] lasts = null;
					double[] asks = null;
					double[] bids = null;
					int index = 0;
					while (iterator.hasNext()) {
						com.anyoption.common.beans.Opportunity cacheOpp = iterator.next();
						ratesTimes[index] = cacheOpp.getTimeEstClosing().getTime();
						if (cacheOpp.getLastUpdateQuote().getLast() != null) {
							if (lasts == null) {
								lasts = new double[oppsCache.size()];
							}
							lasts[index] = cacheOpp.getLastUpdateQuote().getLast().doubleValue();
						}
						if (cacheOpp.getLastUpdateQuote().getAsk() != null) {
							if (asks == null) {
								asks = new double[oppsCache.size()];
							}
							asks[index] = cacheOpp.getLastUpdateQuote().getAsk().doubleValue();
						}
						if (cacheOpp.getLastUpdateQuote().getBid() != null) {
							if (bids == null) {
								bids = new double[oppsCache.size()];
							}
							bids[index] = cacheOpp.getLastUpdateQuote().getBid().doubleValue();
						}
						ratesValues[index++] = cacheOpp.getClosingLevel();
					}
					result.setRatesTimes(ratesTimes);
					result.setRates(ratesValues);
					result.setLasts(lasts);
					result.setAsks(asks);
					result.setBids(bids);
				}
            } else {
                // make sure during off hours we request the aotps_1_xxx subscription
                result.setScheduled(Opportunity.SCHEDULED_HOURLY);
            }

            if (log.isDebugEnabled()) {
                log.debug("total time: " + (System.currentTimeMillis() - t));
            }
        } catch (Exception e) {
            log.error("Error in getChartData.", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }

    /**
     * @param request - Chart Data Method Request
     * @param oc - Opportunity Cache
     * @param chc - Chart History Cache
     * @return ChartDataMethodResult in the Previous Hour.
     */
    public static ChartDataMethodResult getChartDataPreviousHour(ChartDataMethodRequest request, OpportunityCache oc, ChartHistoryCache chc) {
        long t = System.currentTimeMillis();
        ChartDataMethodResult result = new ChartDataMethodResult();
        result.setMarketId(request.getMarketId());
        try {
            OpportunityCacheBean opp = null;
            if (request.getOpportunityId() != 0) {
                opp = oc.getOpportunity(request.getOpportunityId());
            }
            if (log.isDebugEnabled()) {
                log.debug("getChartDataPreviousHour: marketId: " + request.getMarketId() + " oppId: " + request.getOpportunityId() + " time to fill: " + (System.currentTimeMillis() - t));
            }
            if (null != opp) {
            	Map<SkinGroup,List<MarketRate>> ratesMap = chc.getMarketHistory(request.getMarketId());
                List<MarketRate> l = null;
                if (ratesMap != null) {
                	l = ratesMap.get(SkinsManagerBase.getSkin(request.getSkinId()).getSkinGroup());
                } else {
                	log.debug("ratesMap is [null]");
                }
                log.debug("got rates: " + (null != l ? l.size() : "null"));
                if (null != l) {
                    // remove this opp history (we already have it)
                    long tfi = opp.getTimeFirstInvest().getTime();
                    while (l.size() > 0 && l.get(l.size() - 1).getRateTime().getTime() > tfi) {
                        l.remove(l.size() - 1);
                    }
                    // the history keeps 2 hours. we don't need more than 1 hour from prev opp
                    tfi = tfi - 3600000; // 1 hour
                    while (l.size() > 0 && l.get(0).getRateTime().getTime() < tfi) {
                        l.remove(0);
                    }
                    // trim the rates to 1 in 12 sec (in db we store 1 in 6 sec)
                    for (int i = (l.size() % 2 == 0 ? l.size() - 1 : l.size() - 2); i > 0; i -= 2) {
                        l.remove(i);
                    }
                    log.debug("return rates: " + l.size());

                    // TODO: get rid of the ugly translation
                    // result.setRates(l.toArray(new MarketRate[0]));
//                    com.anyoption.beans.base.MarketRate[] rates = new com.anyoption.beans.base.MarketRate[l.size()];
                    long[] ratesTimes = new long[l.size()];
                    double[] ratesValues = new double[l.size()];
                    MarketRate mr = null;
                    for (int i = 0; i < l.size(); i++) {
                        mr = l.get(i);
//                        rates[i] = new com.anyoption.beans.base.MarketRate(mr.getRateTime(), mr.getGraphRate().doubleValue(), mr.getDayRate().doubleValue(), 0f);
                        ratesTimes[i] = mr.getRateTime().getTime();
                        ratesValues[i] = mr.getGraphRate().doubleValue();
                    }
//                    result.setRates(rates);
                    result.setRatesTimes(ratesTimes);
                    result.setRates(ratesValues);
                } else {
                	result.setRatesTimes(new long[] {});
                    result.setRates(new double[] {});
                }
            } else {
            	log.warn("Opportunity not found in cache");
            	result.setRatesTimes(new long[] {});
                result.setRates(new double[] {});
            }
        } catch (Exception e) {
            log.error("Error in getChartData.", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }

	/**
	 * validate user parameter. Login the user to the system if the parameters is valid. get city from google cities coordinates table.
	 *
	 * @param userName
	 * @param password
	 * @param result
	 * @param skinId
	 * @param isLoginActivity
	 * @return User.
	 * @throws Exception
	 */
	private static User validateAndLogin(String userName, String password, MethodResult result, long skinId, boolean isLoginActivity,
											boolean getCity, HttpServletRequest httpRequest) throws Exception {
		User user;
		synchronized (httpRequest.getSession()) {
			user = (User) httpRequest.getSession().getAttribute("user");
		}
		if (null == user || user.getId() == 0) {
			if (null != userName && null != password) {
				Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(skinId).getDefaultLanguageId())
																.getCode());
				ArrayList<ErrorMessage> msgs = validateInput("login", new UserHelper(userName, password), locale);
				if (!msgs.isEmpty()) {
					result.setErrorCode(ERROR_CODE_LOGIN_FAILED);
					result.addErrorMessages(msgs);
					result.addUserMessages(msgs);
					log.info("Error message collection: " + msgs.toString());
					log.info("Invalid input - " + result.getErrorMessages());
					return null;
				}
				user = new User();
				UsersManagerBase.getUserByName(userName, user);
				if (!isLoginActivity) {
					if (null == user || !user.getPassword().equalsIgnoreCase(password)) {
						log.info("User login failed.");
						result.setErrorCode(ERROR_CODE_LOGIN_FAILED);
						result.addErrorMessage("", "User login failed");
						return null;
					}
				}
			} else {
				result.setErrorCode(ERROR_CODE_SESSION_EXPIRED);
			}
		}
		
		if (user != null && user.getCityFromGoogle() == null) {
				boolean res = false;
				long countryId = user.getCountryId();
				if (countryId == 0) {
					countryId = SkinsManagerBase.getSkin(skinId).getDefaultCountryId();
				}
				Country c = CountryManagerBase.getCountry(countryId);
				if (skinId != Skin.SKIN_ETRADER && user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
					try {
						res = UsersManagerBase.getUserCityCoordinates(user, c.getName());
					} catch (Exception e) {
						log.error("Error while calculate city coordinates" + e);
					}
				}
				if (!res) {
					user.setCityLatitude(c.getCapitalCityLatitude());
					user.setCityLongitude(c.getCapitalCityLongitude());
					user.setCityFromGoogle(c.getCapitalCity());
				}
			
		}
		return user;
	}

	/**
	 * validate user with encrypt password. Login the user to the system if the parameters is valid.
	 *
	 * @param userName
	 * @param passwordEncrypt
	 * @param result
	 * @param skinId
	 * @param isLoginActivity
	 * @return user if success or null if we got error
	 * @throws Exception
	 */
	public static User validateAndLoginEncrypt(String userName, String passwordEncrypt, MethodResult result, long skinId,
												boolean isLoginActivity, HttpServletRequest httpRequest) throws Exception {
		String password = (passwordEncrypt != null) ? AESUtil.decrypt(passwordEncrypt) : null;
		return validateAndLogin(userName, password, result, skinId, isLoginActivity, false, httpRequest);
	}

    /**
	 * validate user with encrypt password. Login the user to the system if the parameters is valid.
	 * and also get city from cities google table.
	 *
	 * @param userName
	 * @param passwordEncrypt
	 * @param result
	 * @param skinId
	 * @param isLoginActivity
	 * @return user if success or null if we got error
	 * @throws Exception
	 */
	private static User
			validateAndLoginEncryptGetCity(String userName, String passwordEncrypt, MethodResult result, long skinId,
											boolean isLoginActivity, boolean getCity, HttpServletRequest httpRequest) throws Exception {
		String password = (passwordEncrypt != null) ? AESUtil.decrypt(passwordEncrypt) : null;
		return validateAndLogin(userName, password, result, skinId, isLoginActivity, true, httpRequest);
	}


    /**
     * @param request
     * @return OpportunitiesMethodResult - MethodResult with list of Opportunities.
     */
    public static OpportunitiesMethodResult getPastExpiries(PastExpiresMethodRequest request, PastExpiriesCache expiriesCache) {
    	OpportunitiesMethodResult result = new OpportunitiesMethodResult();
    	log.info("Action: getPastExpiries: " +
    			"userName: " + request.getUserName() +
    			", marketId: " + request.getMarketId() +
    			", marketGroupId: " + request.getMarketGroupId() +
    			", rows: " + request.getStartRow() + " - " + (request.getStartRow() + request.getPageSize()));
    	try {
    		Set<com.anyoption.common.beans.Opportunity> opps = expiriesCache.getPastExpiries(request.getMarketId());
            com.anyoption.common.beans.base.Opportunity[] arr = new com.anyoption.common.beans.base.Opportunity[opps.size()];
            String utcOffset = CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset());
            int index = 0;
            for (com.anyoption.common.beans.base.Opportunity opp : opps) {
            	OpportunitiesManagerBase.initFormattedValues(request.getSkinId(), utcOffset, opp);
            	arr[index++] = opp;
            }
            result.setOpportunities(arr);
    	} catch (Exception e) {
			log.error("Error in getPastExpiries.", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
    	}
    	return result;
    }

    /**
     * @param request - Method Request
     * @return MarketsMethodResult. market list.
     */
    public static MarketsMethodResult getMarketList(MethodRequest request) {
    	MarketsMethodResult result = new MarketsMethodResult();
        log.info("Action: getMarketList: skinId: " + request.getSkinId());
        try {
        	ArrayList<com.anyoption.common.beans.base.Market> markets = SkinsManagerBase.getSkinMarketsListSorted(request.getSkinId());
        	com.anyoption.common.beans.base.Market[] marketsClone = new com.anyoption.common.beans.base.Market[markets.size()];
            Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
            int i = 0;
            for (com.anyoption.common.beans.base.Market market : markets) {
            	com.anyoption.common.beans.base.Market marketColne = (com.anyoption.common.beans.base.Market)market.clone();
            	marketColne.setDisplayName(MarketsManagerBase.getMarketName(request.getSkinId(), marketColne.getId()));
            	marketsClone[i] = marketColne;
            	i++;
	        }
	       result.setMarkets(marketsClone);
        } catch (Exception e) {
              log.error("Error in getMarketList.", e);
              result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }

    /**
     * This method validate the user, validate input for credit card (if its a valid input), validate credit card number, validate card existence.
     * And then insert the card to the data base.
     *
     *
     * @param request - CardMethod Request
     * @return the CardMethodResult.
     */
    public static CardMethodResult insertCard(CardMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("insertCard: " + request);
    	CardMethodResult result = new CardMethodResult();
    	try {
    		User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
            
            //Chek for ET Regulation(additional info) and accepted terms
            if(UserRegulationManager.isNeedAdditionalInfo(user)
            		&& 
					((user.getFirstDepositId() == 0 && !user.isETRegulation()) || user.isETRegulation())){
            	log.error("Can't inert card need additional info (ET Regulation OR non-Regulation user with no FD) for user:" + user.getId());
            	result.setErrorCode(ERROR_CODE_ET_REGULATION_ADDITIONAL_INFO);
            	return result;
            }else if(!user.isAcceptedTerms()){
            	log.error("Can't inert card need Accept Terms for user:" + user.getId());
            	result.setErrorCode(ERROR_CODE_NOT_ACCEPTED_TERMS);
            	return result;
            }
            
            //Validate input for credit card
            ArrayList<ErrorMessage> msgs = validateInput("card", request.getCard(), user.getLocale());
            if (!msgs.isEmpty()) {
                result.setErrorCode(ERROR_CODE_INVALID_INPUT);
                result.addUserMessages(msgs);
                return result;
            }
            CreditCard cc = new CreditCard(request.getCard());
            ArrayList<com.anyoption.common.beans.base.User> users = CreditCardsManagerBase.checkCardExistence(cc.getCcNumber(), cc.getUserId());
            //must remove after all users are force updated with the proper client
            if (user.getSkinId() == Skin.SKIN_ETRADER && request.getCard().getHolderId().equals(ConstantsBase.NO_ID_NUM)) {
            	cc.setHolderIdNum(user.getIdNum());
            }
            cc.setUserId(user.getId());
            //Validate credit card number and cvv
            Map<String, MessageToFormat> mtf = CreditCardsManagerBase.validateCardForm(cc, user);
            if (!mtf.isEmpty()) {
                result.setErrorCode(ERROR_CODE_INVALID_INPUT);
                Locale locale = user.getLocale();
                for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
                    String key = i.next();
                    MessageToFormat m = mtf.get(key);
                    result.addUserMessage(key, CommonUtil.getMessage(locale, m.getKey(), m.getParams()));
                }
                return result;
            }
            //TODO: //for repeat failure
//    		if (!CommonUtil.IsParameterEmptyOrNull(ccNum) && user.getId() > 0){ //for repeat failure
//    			CreditCard currentCC = TransactionsManagerBase.getCreditCardByUserId(Long.valueOf(ccNum), Long.valueOf(user.getId()));
//    			if (null != currentCC){
//    				if ((CommonUtil.isHebrewSkin(user.getSkinId()) && !currentCC.getHolderIdNum().equals(holderIdNum)) ||
//    					!currentCC.getCcPass().equals(ccPass) ||
//    					!currentCC.getHolderName().equals(holderName) ||
//    					!currentCC.getExpMonth().equals(expMonth) ||
//    					!currentCC.getExpYear().equals(expYear)) {
//    					 firstNewCardChanged = true;
//    				}
//    			}
//    		}

            //Validate card existence - in that case we want to insert the card to the DB and create a fail transaction
            String error = null;
            error = CreditCardsManagerBase.validateCardExistens(cc, user.getSkinId(), user.getIdNum(), user.getUserName(), mtf, (null != users));
            result.setError(error);
            if (!mtf.isEmpty()) {
                Locale locale = user.getLocale();
                for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
                    String key = i.next();
                    MessageToFormat m = mtf.get(key);
                    result.addUserMessage(key, CommonUtil.getMessage(locale, m.getKey(), m.getParams()));
                }
            }
            UsersManagerBase.insertUsersDetailsHistoryOneField(request.getWriterId(), user.getId(), user.getUserName(), String.valueOf(user.getClassId()), UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_CREDIT_CARD_NUMBER, null, AESUtil.encrypt(cc.getCcNumber()));
            CreditCardsManagerBase.insertCard(cc, user, request.getWriterId(), ConstantsBase.WEB_CONST);
			try {
				if (users != null && error != null && error.equals(ConstantsBase.ERROR_CARD_NOT_ALLOWED_DUP_CARD)) {
					IssuesManagerBase.insertAutoBlockedCCIssue(user.getId(), ConstantsBase.OFFSET_GMT,
							cc.getCcNumberLast4(), cc.getId(), users);
				}
			} catch (Exception e) {
				log.error("Could not create blocking issue.", e);
			}
            result.setCard(cc);
    	} catch (Exception e) {
    		log.error("Error in insertCard.", e);
    		result.setErrorCode(ERROR_CODE_UNKNOWN);
    	}
    	return result;
    }

    /**
     * This method validate the user.
     * check for existence or different card holder.
     * insert deposit user card.
     *
     * @param request
     * @return TransactionMethodResult.
     */
    public static TransactionMethodResult insertDepositCard(CcDepositMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("insertDepositCard: " + request);
    	TransactionMethodResult result = new TransactionMethodResult();
        try {
        	long loginId = 0L;
    		try {
    			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
    				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
    			}
    		} catch (Exception e) {
    			log.error("Cannot get login ID from session!", e);
    			loginId = 0L;
    		}
        	String error = request.getError();
        	User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
            validateUserRegulationAndNonReg(user, result, AnyoptionService.VALIDATE_REGULATION_DEPOSIT);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
            	return result;
            }
                        
            //Chek for ET Regulation(additional info) and accepted terms
            if(UserRegulationManager.isNeedAdditionalInfo(user)
            		&&
            		((user.getFirstDepositId() == 0 && !user.isETRegulation()) || user.isETRegulation())){
            	log.error("Can't inert card need additional info (ET Regulation OR non-Regulation user with no FD) for user:" + user.getId());
            	result.setErrorCode(ERROR_CODE_ET_REGULATION_ADDITIONAL_INFO);
            	return result;
            }else if(!user.isAcceptedTerms()){
            	log.error("Can't inert card need Accept Terms for user:" + user.getId());
            	result.setErrorCode(ERROR_CODE_NOT_ACCEPTED_TERMS);
            	return result;
            }

            long amountInCents;
            try {
            	amountInCents = CommonUtil.calcAmount(request.getAmount());
            } catch (NumberFormatException e) {
            	log.error("Invalid amount = " + request.getAmount());
            	result.setErrorCode(ERROR_CODE_INVALID_INPUT);
            	result.addUserMessage("amount", CommonUtil.getMessage(user.getLocale(), "error.number.invalid", null));
            	return result;
            }
            CreditCard cc = CreditCardsManagerBase.getById(request.getCardId());
            cc.setCcPass(request.getCcPass());

           	
            // check if AO user is trying to deposit with AMEX CC
            if((cc.getTypeId() == CreditCardType.CC_TYPE_AMEX) && (request.getSkinId() != Skin.SKIN_ETRADER)){
            	TransactionsManagerBase.updateVisibleByUserId(user.getId(), cc.getId());
            	result.setErrorCode(ERROR_CODE_CC_NOT_SUPPORTED);
            	return result;
            }
            Transaction t = TransactionsManagerBase.createTransactionObject(
                    user,
                    amountInCents,
                    TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT,
                    cc,
                    request.getIp(),
                    request.getWriterId(),
                    error
            		);
            t.setLoginId(loginId);
            //If we didn't get an error for credit card (existence or different card holder) we want to validate deposit insertion
            Map<String, MessageToFormat> mtf = new HashMap<String, MessageToFormat>();
            if (null == error) {
	             error = TransactionsManagerBase.validateInsertDeposit(
	            		mtf,
	                    user,
	                    amountInCents,
	                    cc,
	                    t,
	                    request.getWriterId(),
	                    ConstantsBase.WEB_CONST);
	            if (!mtf.isEmpty()) {
	                result.setErrorCode(ERROR_CODE_INVALID_INPUT);
	                for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
	                    String key = i.next();
	                    MessageToFormat m = mtf.get(key);
	                    result.addUserMessage(key, CommonUtil.getMessage(user.getLocale(), m.getKey(), m.getParams()));
	                }
	            }
            } else { //
            	result.setErrorCode(ERROR_CODE_GENERAL_VALIDATION);
            	result.addUserMessage(null, CommonUtil.getMessage(user.getLocale(), "error.creditcard.notallowed", null));
            }
            String jsonContext = CommonUtil.getConfig("json.context");
            if(!jsonContext.endsWith("/")) {
            	jsonContext = jsonContext + "/";
            }
            String termUrl = jsonContext+AnyoptionServiceServlet.REDIRECT_SERVICE_SUFFIX;
            t = TransactionsManagerBase.insertDeposit(mtf, error, user, t, cc, request.getWriterId(), ConstantsBase.WEB_CONST, null, null, request.isFirstNewCardChanged(), loginId, termUrl);
            TransactionsManagerBase.afterDepositAction(user, t, TransactionSource.TRANS_FROM_MOBILE);
            
            if (t.getStatusId() == TransactionsManagerBase.TRANS_STATUS_PENDING || t.getStatusId() == TransactionsManagerBase.TRANS_STATUS_SUCCEED ) {
                refreshUserInSession(user.getId(), httpRequest);
                user = (User) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_USER);
                TransactionsManagerBase.updatePixelRunTime(t.getId());
                result.setBalance(user.getBalance() / 100.0);
                result.setTransactionId(t.getId());
                result.setAmount(t.getAmount() / 100d);
                result.setDollarAmount(CommonUtil.convertToBaseAmount(t.getAmount(), user.getCurrencyId(), new Date())/100);
                boolean isFirstSuccessDeposit = TransactionsManagerBase.countRealDeposit(user.getId()) == 1 ? true : false ;
                result.setFirstDeposit(isFirstSuccessDeposit);
                result.setClearingProvider(t.getClearingProviderDescriptor());
                try {
					if (0 != user.getCombinationId() && isFirstSuccessDeposit) {
						PixelGenerator.runPixel(user.getCombinationId(), PixelGenerator.PIXEL_TYPE_FIRST_DEPOSIT, request.getDeviceId(), request.getIp());
					}
				} catch (RuntimeException e) {
					log.error("cant run pixel", e);
				}
            } else if (t.getStatusId() == TransactionsManagerBase.TRANS_STATUS_AUTHENTICATE) {
            	result.setTransactionId(t.getId());
            	result.setThreeDParams(t.getThreeDParams());
            	httpRequest.getSession().setAttribute(ConstantsBase.BIND_SESSION_TRANSACTION, t.getId());            	
            } else {
            	result.setErrorCode(ERROR_CODE_GENERAL_VALIDATION);
	            if (!mtf.isEmpty()) {
	                for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
	                    String key = i.next();
	                    result.setApiCode(error);
						if (result.getUserMessages() == null || result.getUserMessages().length <= 0
								|| !isExistErrorForField(result.getUserMessages(), key)) {
							MessageToFormat m = mtf.get(key);
							result.addUserMessage(key, CommonUtil.getMessage(user.getLocale(), m.getKey(), m.getParams()));
						}
	                }
	            }
            }
        } catch (Exception e) {
            log.error("Error in insertDepositCard.", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }

    private static boolean isExistErrorForField(ErrorMessage[] userErrorMsg, String field){
    	boolean res  = false;
    	for (int i = 0; i < userErrorMsg.length; i++) {
    		  if(userErrorMsg[i].getField().equals(field)){
    			  res = true;
    		  }
    		}
    	return res;
    }

    /**
     * validate user withdraw Card.
     * validate the user.
     * And check if the card is validate to withdraw with.
     *
     * @param request
     * @return TransactionMethodResult.
     */
    public static TransactionMethodResult validateWithdrawCard(CcDepositMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("validateWithdrawCard: " + request);
    	TransactionMethodResult result = new TransactionMethodResult();
    	long loginId = 0L;
		try {
			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
			}
		} catch (Exception e) {
			log.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
        try {
            User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
            
            //AO Regulation
            int errCodeCanWithdraw = UserRegulationManager.canWithdraw(user.getId());
            if(errCodeCanWithdraw != ERROR_CODE_SUCCESS){
            	result.setErrorCode(errCodeCanWithdraw);
            	return result;
            }
            
            CreditCard cc = null;
        	long ccMinWithdraw = TransactionsManagerBase.getCreditCardMinWithdraw(user.getCurrencyId());
            long amountInCents = CommonUtil.calcAmount(request.getAmount());
            ArrayList<CreditCard> ccList = CreditCardsManagerBase.getDisplayWithrawalCreditCardsByUser(user.getId(), request.getCardId(),
            		ccMinWithdraw, request.getSkinId(), true);
            if (null != ccList) {
            	cc = ccList.get(0); // if we send to getDisplayWithrawalCreditCardsByUser the credit id and i find card i get back list with one item
            }
            if (cc.getUserId() != user.getId()) {
                log.warn("Try to withdraw from another users card!");
                result.setErrorCode(ERROR_CODE_INVALID_INPUT);
                return result;
            }
            String tranDescriptionError = null;
            Map<String, MessageToFormat> mtf = new HashMap<String, MessageToFormat>();
        	if(amountInCents <= ccMinWithdraw) {
        		 mtf.put("amount", new MessageToFormat("error.minwithdraw", null, MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
        		 tranDescriptionError = "error.transaction.withdraw.minamounnt";
        	}
        	
            if(tranDescriptionError == null) {
            	tranDescriptionError = TransactionsManagerBase.validateWithdrawForm(amountInCents, user, true, true,
            			false, cc, null, null, false, mtf);
            }
            
            if (tranDescriptionError == null) {
            	tranDescriptionError = TransactionsManagerBase.validateCardWithdraw(cc, mtf);
            }

            boolean preWithdrawError = false;
            if (tranDescriptionError == null) {
            	WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
            	if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
            		tranDescriptionError = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
            		result.setErrorCode(ERROR_CODE_INVALID_INPUT);
            		result.addUserMessage(null, CommonUtil.getMessage(user.getLocale(), tranDescriptionError, null));
            		preWithdrawError = true;
            	}
            }

			if (!preWithdrawError) {
				// in case of error insert transaction with failed status
				if (!mtf.isEmpty()) {
					TransactionsManagerBase.insertCreditCardWithdraw(	amountInCents, cc, tranDescriptionError, user, request.getWriterId(),
																		false, null, request.getIp(), loginId, null);
					result.setErrorCode(ERROR_CODE_INVALID_INPUT);
					for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
						String key = i.next();
						MessageToFormat m = mtf.get(key);
						if (m.getErrorMsgType() == MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD) {
							result.setErrorCode(ERROR_CODE_VALIDATION_WITHOUT_FIELD);
						}
						result.addUserMessage(key, CommonUtil.getMessage(user.getLocale(), m.getKey(), m.getParams()));
					}
				} else {
					// // Rewards plan
					// if (RewardManager.isHasBenefit(String.valueOf(RewardBenefit.REWARD_BENEFIT_EXEMPT_WITHDRAWAL_FEE), user.getId())) {
					// result.setFee(ConstantsBase.NO_FEE);
					// } else {
					long feeVal;
					feeVal = TransactionsManagerBase.getTransactionHomoFee(amountInCents, user.getCurrencyId(), TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW);

					if (user.getSkinId() != Skin.SKIN_ARABIC) {
						result.setFee(CommonUtil.formatCurrencyAmountAO(feeVal, true, user.getCurrency()));
					} else {
						result.setFee(CommonUtil.displayAmountForInput(feeVal, false, user.getCurrencyId().intValue())+ " "
										+ user.getCurrency().getSymbol() + " ");
					}
					// }
				}
			}
			refreshUserInSession(user.getId(), httpRequest);
        } catch (Exception e) {
            log.error("Error in validateWithdrawCard.", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }

    /**
     * We only gets to that function in success (after user confirm the important note PopUp).
     *
     * @param request
     * @return TransactionMethodResult
     */
    public static TransactionMethodResult insertWithdrawCard(CcDepositMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("insertWithdrawCard: " + request);
        TransactionMethodResult result = new TransactionMethodResult();
        long loginId = 0L;
		try {
			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
			}
		} catch (Exception e) {
			log.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
        try {
            User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
            
            CreditCard cc = null;//CreditCardsManagerBase.getByCardIdAndUserId(request.getCardId(), user.getId());

        	long ccMinWithdraw = TransactionsManagerBase.getCreditCardMinWithdraw(user.getCurrencyId());
            ArrayList<CreditCard> ccList = CreditCardsManagerBase.getDisplayWithrawalCreditCardsByUser(user.getId(), request.getCardId(),
            		ccMinWithdraw, request.getSkinId(), true);
            if (null != ccList) { //we sent credit card id only 1 cc can be in the list
            	cc = ccList.get(0);
            }
            long amountInCents;
            try {
            	amountInCents = CommonUtil.calcAmount(request.getAmount());
            } catch (NumberFormatException e) {
            	log.error("Invalid amount = " + request.getAmount());
            	result.setErrorCode(ERROR_CODE_INVALID_INPUT);
            	result.addUserMessage("amount", CommonUtil.getMessage(user.getLocale(), "error.number.invalid", null));
            	return result;
            }

            // cancel all bonuses that can be canceled or return error if something goes wrong
    		List<BonusUsers> bonusUsers = BonusManagerBase.getAllUserBonus(user.getId(), null, null, 0, user.getUtcOffset(), new ArrayList<>(Arrays.asList(CurrenciesManagerBase.getCurrency(user.getCurrencyId()))));
    		String error = null;
    		for (BonusUsers bu : bonusUsers) {
    			if (bu.isCanCanceled()) {
    				error = BonusManagerBase.cancelBonus(bu, user.getUtcOffset(), request.getWriterId(), user.getSkinId(), user, loginId, request.getIp());
    			}
    			if (error != null && error.equals(BonusManagerBase.ERROR_DUMMY)) {
    				log.error("Cannot cancel bonus with id: " + bu.getId());
    				result.setErrorCode(ERROR_CODE_GENERAL_VALIDATION);
    				return result;
    			} else {
    				error = "";
    			}
    		}
    		
    		QuestionnaireUserAnswer questUserAnswer = null;
    		if (request.getUserAnswerId() > 0) {
    			questUserAnswer = new QuestionnaireUserAnswer();
    			questUserAnswer.setAnswerId(request.getUserAnswerId());
    			questUserAnswer.setTimeCreated(new Date());
    			questUserAnswer.setGroupId(QuestionnaireGroup.WITHDRAW_SURVEY_ID);
    			questUserAnswer.setQuestionId(QuestionnaireQuestion.WITHDRAW_SURVEY_QUESTION);
    			if (!CommonUtil.isParameterEmptyOrNull(request.getTextAnswer())) {
    				questUserAnswer.setTextAnswer(request.getTextAnswer());
    			}
    			questUserAnswer.setUserId(user.getId());
    			questUserAnswer.setWriterId(request.getWriterId());
    		}

            synchronized (httpRequest.getSession()) {
	            //NO validations?
	            String tranDescriptionError = null;
	            boolean preWithdrawError = false;
	            if (tranDescriptionError == null) {
	            	// TODO synchronized here
	            	WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
	            	if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
	            		tranDescriptionError = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
	            		result.setErrorCode(ERROR_CODE_INVALID_INPUT);
	            		result.addUserMessage(null, CommonUtil.getMessage(user.getLocale(), tranDescriptionError, null));
	            		preWithdrawError = true;
	            	}
	            }
	            if (!preWithdrawError) {
		            TransactionsManagerBase.insertCreditCardWithdraw(amountInCents, cc, null,
		            		user, request.getWriterId(), false, null, request.getIp(), loginId, questUserAnswer);
		            UsersManagerBase.getUserByName(user.getUserName(), user); // refresh balance
		            result.setBalance(user.getBalance() / 100.0);
		            result.setFormattedAmount(CommonUtil.formatCurrencyAmountAO(amountInCents, user.getCurrencyId()));
	            }
	            refreshUserInSession(user.getId(), httpRequest);
            }
        } catch (Exception e) {
            log.error("Error in insertWithdrawCard.", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }

    /**
     * validate bank to withdraw from.
     * validate user that want to withdraw.
     * check and valid the withdraw amount.
     *
     *
     * @param request
     * @return TransactionMethodResult.
     * @see com.anyoption.common.service.results.TransactionMethodResult
     */
    public static TransactionMethodResult validateWithdrawBank(WireMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("validateWithdrawBank: " + request);
    	TransactionMethodResult result = new TransactionMethodResult();
    	long loginId = 0L;
		try {
			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
			}
		} catch (Exception e) {
			log.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
        try {
            User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
            
    		if (SkinsManagerBase.getSkin(user.getSkinId()).isRegulated() || SkinsManagerBase.getSkin(user.getSkinId()).getBusinessCaseId() == Skin.SKIN_BUSINESS_GOLDBEAM) {
    			// AO Regulation checks
    			int regErrorCode = UserRegulationManager.canWithdraw(user.getId());
    			if (regErrorCode != CommonJSONService.ERROR_CODE_SUCCESS) {
    				result.setErrorCode(regErrorCode);
    				return result;
    			}
    		}
            
            WireBase wire = request.getWire();
            long amountInCents = CommonUtil.calcAmount(Double.parseDouble(wire.getAmount()));
            Map<String, MessageToFormat> mtf = new HashMap<String, MessageToFormat>();
            String tranDescriptionError = TransactionsManagerBase.validateWireForm(amountInCents, user, true, true, false, mtf);

            boolean preWithdrawError = false;
            if (tranDescriptionError == null) {
            	WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
            	if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
            		tranDescriptionError = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
            		result.setErrorCode(ERROR_CODE_INVALID_INPUT);
            		result.addUserMessage(null, CommonUtil.getMessage(user.getLocale(), tranDescriptionError, null));
            		preWithdrawError = true;
            	}
            }
            
            if (!preWithdrawError) {
	            //Validate Bank Branch
	            long skinId =request.getSkinId();
	            if (skinId == Skin.SKIN_ETRADER) {
	            	boolean isGoodbranchCode = false;
	                for (BankBranches bankBranch : BanksManagerBase.getBanksBranches()) {
	                    if (wire.getBankId().equalsIgnoreCase(bankBranch.getBankId().toString()) && //check that this bank and branch code are real
	                            wire.getBranch().equals(bankBranch.getBranchCode().toString())) {
	                        isGoodbranchCode = true;
	                        break;
	                    }
	                }
	                if (!isGoodbranchCode) {
	                	mtf.put("branch", new MessageToFormat("error.branch", null, MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
	                	tranDescriptionError = "error.branch.code";
	                }
	            }
	            //in case of error insert transaction with failed status
	            if (!mtf.isEmpty()) {
	            	TransactionsManagerBase.insertBankWireWithdraw(wire, tranDescriptionError, user, request.getWriterId(), false, false, request.getIp(), loginId, null);
					result.setErrorCode(ERROR_CODE_INVALID_INPUT);
					for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
					    String key = i.next();
					    MessageToFormat m = mtf.get(key);
	                    if (m.getErrorMsgType() == MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD) {
	                    	result.setErrorCode(ERROR_CODE_VALIDATION_WITHOUT_FIELD);
	                    }
					    result.addUserMessage(key, CommonUtil.getMessage(user.getLocale(), m.getKey(), m.getParams()));
					}
	            } else {
	            	// get fee
	            	long feeVal;
        			feeVal = TransactionsManagerBase.getTransactionHomoFee(amountInCents, user.getCurrencyId(), TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW);

	            	if (user.getSkinId() != Skin.SKIN_ARABIC){
	            		result.setFee(CommonUtil.formatCurrencyAmountAO(feeVal, true, user.getCurrency()));
	        		} else {
	        			result.setFee(CommonUtil.displayAmountForInput(feeVal, false, user.getCurrencyId().intValue()) + " " + user.getCurrency().getSymbol() + " ");
	        		}
	            }
            }
            refreshUserInSession(user.getId(), httpRequest);
        } catch (Exception e) {
            log.error("Error in validateWithdrawBank", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }


    /**
     * We only gets to that function in success (after user confirm the important note PopUp)
     *
     * @param request
     * @return theTransactionMethodResult.
     * @see com.anyoption.common.service.results.TransactionMethodResult
     */
    public static TransactionMethodResult insertWithdrawBank(WireMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("insertWithdrawBank: " + request);
        TransactionMethodResult result = new TransactionMethodResult();
        try {
            User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }

            // cancel all bonuses that can be canceled or return error if something goes wrong
    		List<BonusUsers> bonusUsers = BonusManagerBase.getAllUserBonus(user.getId(), null, null, 0, user.getUtcOffset(), new ArrayList<>(Arrays.asList(CurrenciesManagerBase.getCurrency(user.getCurrencyId()))));
    		String error = null;
    		for (BonusUsers bu : bonusUsers) {
    			if (bu.isCanCanceled()) {
    				long loginId = 0L;
    				try {
    					if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
    						loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
    					}
    				} catch (Exception e) {
    					log.error("Cannot get login ID from session!", e);
    					loginId = 0L;
    				}
    				error = BonusManagerBase.cancelBonus(bu, user.getUtcOffset(), request.getWriterId(), user.getSkinId(), user, loginId, request.getIp());
    			}
    			if (error.equals(BonusManagerBase.ERROR_DUMMY)) {
    				log.error("Cannot cancel bonus with id: " + bu.getId());
    				result.setErrorCode(ERROR_CODE_GENERAL_VALIDATION);
    				return result;
    			} else {
    				error = "";
    			}
    		}

    		QuestionnaireUserAnswer questUserAnswer = null;
    		if (request.getUserAnswerId() > 0) {
    			questUserAnswer = new QuestionnaireUserAnswer();
    			questUserAnswer.setAnswerId(request.getUserAnswerId());
    			questUserAnswer.setTimeCreated(new Date());
    			questUserAnswer.setGroupId(QuestionnaireGroup.WITHDRAW_SURVEY_ID);
    			questUserAnswer.setQuestionId(QuestionnaireQuestion.WITHDRAW_SURVEY_QUESTION);
    			if (!CommonUtil.isParameterEmptyOrNull(request.getTextAnswer())) {
    				questUserAnswer.setTextAnswer(request.getTextAnswer());
    			}
    			questUserAnswer.setUserId(user.getId());
    			questUserAnswer.setWriterId(request.getWriterId());
    		}
    		
            synchronized (httpRequest.getSession()) {
            	long loginId = 0L;
        		try {
        			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
        				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
        			}
        		} catch (Exception e) {
        			log.error("Cannot get login ID from session!", e);
        			loginId = 0L;
        		}
	            //NO validations?
	            String tranDescriptionError = null;
	            boolean preWithdrawError = false;
				if (tranDescriptionError == null) {
	            	WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
	            	if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
	            		tranDescriptionError = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
	            		result.setErrorCode(ERROR_CODE_INVALID_INPUT);
	            		result.addUserMessage(null, CommonUtil.getMessage(user.getLocale(), tranDescriptionError, null));
	            		preWithdrawError = true;
	            	}
	            }
				if (!preWithdrawError) {
		            WireBase wire = request.getWire();
		            long amountInCents = CommonUtil.calcAmount(Double.parseDouble(request.getWire().getAmount()));
		        	TransactionsManagerBase.insertBankWireWithdraw(wire, null, user,
		        			request.getWriterId(), false, false, request.getIp(), loginId, questUserAnswer);
		            UsersManagerBase.getUserByName(user.getUserName(), user); // refresh balance
		            result.setBalance(user.getBalance() / 100.0);
		            result.setFormattedAmount(CommonUtil.formatCurrencyAmountAO(amountInCents, user.getCurrencyId()));
				}
				refreshUserInSession(user.getId(), httpRequest);
            }
        } catch (Exception e) {
            log.error("Error in insertWithdrawBank.", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }

    /**
     * get skin id by the ip.
     *
     * @param request
     * @return SkinMethodResult.
     * @see com.anyoption.common.service.results.SkinMethodResult
     */
    public static SkinMethodResult getSkinIdByIp(MethodRequest request) {
    	SkinMethodResult skinResult = new SkinMethodResult();
    	try {
    		String ip = request.getIp();
    		log.info("Action: getSkinIdByIp: ip: " + ip);
            skinResult.setSkinId(CommonUtil.getSkinIdByIp(ip));
            if (null == skinResult.getSkinId()) {
            	skinResult.setErrorCode(ERROR_CODE_UNKNOWN);
            }
    	} catch (Exception e) {
    		skinResult.setErrorCode(ERROR_CODE_UNKNOWN);
    		log.error("Error in getSkinIdByIp.", e);
    	}
    	return skinResult;
    }

    /**
     * get the server time
     *
     * @return ServerDateTime.
     * @see java.util.Calendar
     */
    public Calendar getServerDateTime() {
        return Calendar.getInstance(TimeZone.getTimeZone("GMT"));
    }

    /**
     * This method validate the user with is "old" credit card.
     * Get the new card and check if its valid.
     *
     * @param request
     * @return CardMethodResult.
     */
    public static CardMethodResult updateCard(CardMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("updateCard: " + request);
        CardMethodResult result = new CardMethodResult();
        try {
            User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
            CreditCard oldCard = CreditCardsManagerBase.getById(request.getCard().getId());
            if (oldCard.getUserId() != user.getId()) {
	       		 log.info("requested userId for cc update is wrong!");
	             result.setErrorCode(ERROR_CODE_INVALID_INPUT);
	             return result;
            }
            ArrayList<ErrorMessage> msgs = validateInput("updateCard", request.getCard(), user.getLocale());
            if (!msgs.isEmpty()) {
                result.setErrorCode(ERROR_CODE_INVALID_INPUT);
                result.addUserMessages(msgs);
                return result;
            }
            CreditCard cc = new CreditCard(request.getCard());
            Map<String, MessageToFormat> mtf = CreditCardsManagerBase.validateCvvAndDates(cc, user);
            if (!mtf.isEmpty()) {
                result.setErrorCode(ERROR_CODE_INVALID_INPUT);
                for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
                    String key = i.next();
                    MessageToFormat m = mtf.get(key);
                    result.addUserMessage(key, CommonUtil.getMessage(user.getLocale(), m.getKey(), m.getParams()));
                }
                return result;
            }
            cc.setUtcOffsetModified(user.getUtcOffset());
            CreditCardsManagerBase.updateCardDetails(cc);
            result.setCard(cc);
            result.addUserMessage(null, CommonUtil.getMessage(user.getLocale(), "card.update.success", null));
        } catch (Exception e) {
            log.error("Error in updateCard.", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }

    /**
     * Get the user list of his investments.
     *
     * @param request
     * @return InvestmentsMethodResult.
     */
    @PrintLogAnnotations(stopPrintDebugLog = true)
	public static InvestmentsMethodResult getUserInvestments(
			InvestmentsMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getUserInvestments: " + request);
		Locale locale = new Locale(LanguagesManagerBase
				.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
		InvestmentsMethodResult result = new InvestmentsMethodResult();
		try {
			User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result,
					request.getSkinId(), false, httpRequest);
			if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
				return result;
			}
			getUserInvestments(result, user, request.getFrom(), request.getTo(), request.isSettled(),
					request.getMarketId(), request.getSkinId(), request.getWriterId(), request.getUtcOffset(),
					request.getPeriod(), request.getStartRow(), request.getPageSize());
			result = (InvestmentsMethodResult) initUser(result, user,
					request.getUtcOffset(), request.getWriterId());
		} catch (Exception e) {
			log.error("Error process invest.", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
		return result;
	}

    /**
     * This method is triggered when the user wants to cancel his withdraws (reverse withdraw)
     *
     * @param request. Contains UserName, Password, SkinId, Trans, Ip.
     * @return TransactionMethodResult. If succeeded - return new users balance and corresponding message. If not return an error message.
     * @see com.anyoption.common.service.results.TransactionMethodResult
     */
    public static TransactionMethodResult insertWithdrawsReverse(TransactionMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("insertWithdrawsReverse: " + request);
        TransactionMethodResult result = new TransactionMethodResult();
        long loginId = 0L;
		try {
			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
			}
		} catch (Exception e) {
			log.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
        try {
        	User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
        	com.anyoption.common.beans.base.Transaction trans[] = request.getTrans();
            Transaction transactions[] = new Transaction[trans.length];
            //build the trans array
            for (int i = 0; i < transactions.length; i++) {
            	transactions[i] = TransactionsManagerBase.getById(trans[i].getId());
            }
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
            for (int i = 0; i < transactions.length; i++) {
                if (transactions[i].getUserId() != user.getId()) {
	   	       		log.info("requested userId for transaction withdrawal is wrong! trxId: " + transactions[i].getId());
	   	            result.setErrorCode(ERROR_CODE_INVALID_INPUT);
	   	            return result;
                }
            }
            Map<String, MessageToFormat> mtf = TransactionsManagerBase.reverseWithdrawsValidation(transactions);
            if (!mtf.isEmpty()) {
                result.setErrorCode(ERROR_CODE_INVALID_INPUT);
                for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
                    String key = i.next();
                    MessageToFormat m = mtf.get(key);
                    result.addUserMessage(key, CommonUtil.getMessage(user.getLocale(), m.getKey(), m.getParams()));
                }
                return result;
            }
            MessageToFormat m = TransactionsManagerBase.reverseWithdrawsWithDateLoading(transactions, user, request.getWriterId(), request.getIp(), loginId);
            result.addUserMessage("", CommonUtil.getMessage(user.getLocale(), m.getKey(), m.getParams()));
            result.setBalance(user.getBalance());
            refreshUserInSession(user.getId(), httpRequest);
        } catch (Exception e) {
            log.error("Error in insertWithdrawReverse.", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }

    /**
     * Get a list of asset index by the market.
     *
     * @param request
     * @return AssetIndexMethodResult. MethodResult with list of asset index.
     * @see com.anyoption.beans.base.AssetIndexBase
     */
    public static AssetIndexMethodResult getAssetIndexByMarket(AssetIndexMethodRequest request) {
    	log.info("get Assent index by market , marketId:" + request.getMarketId());
        AssetIndexMethodResult result = new AssetIndexMethodResult();
        try {
           // AssetIndexBase assetBase = AssetIndexManagerBase.getAssetyByMarket(request.getSkinId(), request.getMarketId());
        	AssetIndexBase assetBase = AssetIndexMarketCacheJson.getAssetIndexMarket().get(request.getSkinId()).get(request.getMarketId());
            AssetIndex assetIndex = new AssetIndex(assetBase);

            String assetIndexExpFormulaKey = null;
            assetIndexExpFormulaKey = MarketsManagerBase.getMarketAssetIndexExpLevelCalculation( request.getMarketId());

            assetIndex.getTradingTimeString(CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset()), request.getSkinId(), request.getMarketId(), assetIndexExpFormulaKey);
            result.setAssetIndex(assetIndex);
        } catch (Exception e) {
            log.error("Error process invest.", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }

    /**
     * insert new Investment.
     *
     * @param request
     * @return InvestmentMethodResult.
     */
    public static InvestmentMethodResult insertInvestment(InsertInvestmentMethodRequest request, OpportunityCache oc, HttpServletRequest httpRequest) {
    	log.debug("insertInvestment: " + request);
    	InvestmentMethodResult result = new InvestmentMethodResult();
    	result.setCancelSeconds(Integer.parseInt(Utils.getProperty("cancel.seconds.client")));
    	try {
    		User user = validateAndLoginEncryptGetCity(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, true, httpRequest);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
            validateUserRegulationAndNonReg(user, result, AnyoptionService.VALIDATE_REGULATION_INVESTMENT);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
            	return result;
            }
            // To set the user default amount value, because we get the user from db
            user.setDefaultAmountValue(request.getDefaultAmountValue());
            OpportunityCacheBean opportunity = oc.getOpportunity(request.getOpportunityId());
            if (!InvestmentsManagerBase.validateOpportunityType(opportunity, Opportunity.TYPE_REGULAR, Opportunity.TYPE_OPTION_PLUS)) {
            	result.setErrorCode(ERROR_CODE_INVALID_INPUT);
            	return result;
            }
            boolean isAPIUser = request.getWriterId() == Writer.WRITER_ID_API? true : false;
            double pageLevel = CommonUtil.roundDouble(request.getPageLevel(), opportunity.getMarketDecimalPoint());
            if (isAPIUser) {
            	if (opportunity == null || opportunity.getMarketId() == 0) { // in case the opportunity is closed
        			log.info("Opportunity does not exists:" + opportunity);
        			result.setErrorCode(ERROR_CODE_OPP_NOT_EXISTS);
        			result.addUserMessage("opportunityId", "Can't find opportunity", ApiErrorCode.API_ERROR_CODE_VALIDATION_CANT_FIND_OPPORTUNITY);
        			return result;
            	} else if (pageLevel == -1) { // in case we find the opportunity and we want to take set page level to be www level
                    WebLevelLookupBean wllb = new WebLevelLookupBean();
                    wllb.setSkinGroup(SkinsManagerBase.getSkin(user.getSkinId()).getSkinGroup());
                    wllb.setOppId(opportunity.getId());
                    Market market = MarketsManagerBase.getMarket(opportunity.getMarketId());                                        
                    wllb.setFeedName(market.getFeedName());
                    AnyoptionServiceServlet.getLevelsCache().getLevels(wllb);
                    pageLevel = new BigDecimal(wllb.getDevCheckLevel()).setScale(Integer.parseInt(String.valueOf(opportunity.getMarketDecimalPoint())), RoundingMode.HALF_UP).doubleValue();
            	}
            }

            Investment inv = new Investment();
            inv.setOpportunityId(request.getOpportunityId());
            inv.setAmount(new Double(request.getRequestAmount() * 100).longValue());
            inv.setCurrentLevel(pageLevel);
            inv.setOddsWin(request.getPageOddsWin());
            inv.setOddsLose(request.getPageOddsLose());
            inv.setTypeId(request.getChoice());
            inv.setUtcOffsetCreated(CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset()));
            inv.setApiExternalUserId(request.getApiExternalUserId());
            String ip = request.getIp();
            if (isAPIUser && !CommonUtil.IsParameterEmptyOrNull(request.getIpAddress())) {
            	ip = request.getIpAddress();
            }
            inv.setIp(ip);
            inv.setCurrencyId(user.getCurrencyId());
            inv.setTimeEstClosing(opportunity.getTimeEstClosing());
            if (opportunity.getOpportunityTypeId() == Opportunity.TYPE_OPTION_PLUS) {
//            	if (CommonUtil.isHebrewSkin(user.getSkinId())) {
//            		inv.setOptionPlusFee(opportunity.getMarket().getOptionPlusFee());
//            	} else if (user.getCurrencyId() == ConstantsBase.CURRENCY_RUB_ID) {
//            	    inv.setOptionPlusFee(Opportunity.OPTION_PLUS_COMMISSION_RUB);
//            	} else if (user.getCurrencyId() == ConstantsBase.CURRENCY_CNY_ID) {
//            	    inv.setOptionPlusFee(Opportunity.OPTION_PLUS_COMMISSION_YUAN);
//            	} else if (user.getCurrencyId() == ConstantsBase.CURRENCY_KR_ID) {
//            		inv.setOptionPlusFee(Opportunity.OPTION_PLUS_COMMISSION_KOREAN);
//            	} else {
//            		inv.setOptionPlusFee(Opportunity.OPTION_PLUS_COMMISSION);
//            	}
            	inv.setOptionPlusFee(FeesManagerBase.getFee(FeesManagerBase.OPTION_PLUS_COMMISSION_FEE, user.getCurrencyId()));
            }
            long destUserId = -1;
			if (request instanceof com.copyop.json.requests.InsertInvestmentMethodRequest) {
				if (CopyOpInvTypeEnum.FOLLOW == request.getCopyopTypeId()) {
					destUserId = ((com.copyop.json.requests.InsertInvestmentMethodRequest) request).getDestUserId();
				}
			}
            inv.setCopyopType((request.getCopyopTypeId() != null) ? request.getCopyopTypeId() : CopyOpInvTypeEnum.SELF);
            inv.setCopyOpInvId(request.getCopyopInvId());
            InvestmentValidatorParams ivp = new InvestmentValidatorParams();
            Market market = MarketsManagerBase.getMarket(opportunity.getMarketId());
            if (request.getWriterId() == Writer.WRITER_ID_COPYOP_WEB) {
            	ivp.setSecForDev2(market.getSecForDev2());
            } else {
            	ivp.setSecForDev2(market.getSecForDev2Mobile());
            }
            long loginId = 0L;
    		try {
    			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
    				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
    			}
    		} catch (Exception e) {
    			log.error("Cannot get login ID from session!", e);
    			loginId = 0L;
    		}
            ivp.setSecBetweenInvestmentsSameIp(market.getSecBetweenInvestmentsSameIpMobile());
            ivp.setSecBetweenInvestments(market.getSecBetweenInvestmentsMobile());
            ivp.setDev2Second(request.isDev2Second());
            Object sessionParams = httpRequest.getSession().getAttribute(InvestmentValidator.REQUEST_PARAMETER_ID + request.getOpportunityId());
            if (sessionParams instanceof Map<?, ?>) {
            	ivp.setSessionParams((Map<?, ?>) sessionParams);
            }
            InvestmentValidator iv = new InvestmentValidator(ivp);
            MessageToFormat m = InvestmentsManagerBase.submitInvestment (
                    user,
                    inv,
                    opportunity,
                    request.isFromGraph(),
                    false,
                    request.getWriterId(),
                    "SOAP request",
                    AnyoptionServiceServlet.getLevelsCache(),
                    iv,
                    destUserId,
                    loginId);
            String utcOffset = CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset());
            if (null == m) {
                inv.setAmount(inv.getAmount() + inv.getOptionPlusFee());
                InvestmentHelper.initInvestment(inv, request.getSkinId(), utcOffset);
                result.setInvestment(inv);
                refreshUserInSession(user.getId(), httpRequest);
                httpRequest.getSession().removeAttribute(InvestmentValidator.REQUEST_PARAMETER_ID + request.getOpportunityId());
            } else {
            	if (m.getKey() != null && m.getKey().equals(InvestmentValidator.DEV2_PARAMETER_NAME)) {
					HttpSession session = httpRequest.getSession();
					if (session.getAttribute(InvestmentValidator.REQUEST_PARAMETER_ID + request.getOpportunityId()) != null) {
						log.warn("There is already parameter in session with key: "+ InvestmentValidator.REQUEST_PARAMETER_ID
									+ request.getOpportunityId());
						if (session.getAttribute(InvestmentValidator.REQUEST_PARAMETER_ID
													+ request.getOpportunityId()) instanceof Map<?, ?>) {
							log.warn("Printing values");
							Map<?, ?> map = (Map<?, ?>) session.getAttribute(InvestmentValidator.REQUEST_PARAMETER_ID
																				+ request.getOpportunityId());
							for (Object key : map.keySet()) {
								log.warn(key + ": " + map.get(key));
							}
						} else {
							log.warn("The attribute is not a map");
						}
					}
					session.setAttribute(InvestmentValidator.REQUEST_PARAMETER_ID + request.getOpportunityId(), ivp.getRequestParams());
					result.setDev2Seconds((int) m.getParams()[0]);
            	} else {
	            	String apiErrorCode = null;
	            	int errorCode = ERROR_CODE_INV_VALIDATION;
	                if (m.getKey() != null && m.getKey().equalsIgnoreCase("error.investment.nomoney")) {
	                	apiErrorCode = ApiErrorCode.API_ERROR_CODE_VALIDATION_BALANCE_LOWER_REQ_INV_AMOUNT;
	                	errorCode = ERROR_CODE_VALIDATION_LOW_BALANCE;
	                } else if (m.getKey() != null && m.getKey().equalsIgnoreCase("error.investment.limit.min")) {
	                	apiErrorCode = ApiErrorCode.API_ERROR_CODE_VALIDATION_AMOUNT_BELOW_MIN_INVESTMENT;
	                } else if (m.getKey() != null && m.getKey().equalsIgnoreCase("error.investment.limit.max")) {
	                	apiErrorCode = ApiErrorCode.API_ERROR_CODE_VALIDATION_AMOUNT_ABOVE_MAX_INVESTMENT;
	                } else if (m.getKey() != null && m.getKey().equalsIgnoreCase("error.investment.notopened")) {
	                	apiErrorCode =  ApiErrorCode.API_ERROR_CODE_VALIDATION_OPTION_CURRENTLY_NOT_AVAILABLE;
	                } else if (m.getKey() != null && m.getKey().equalsIgnoreCase("error.investment.expired")) {
	                	apiErrorCode = ApiErrorCode.API_ERROR_CODE_VALIDATION_OPTION_NO_LONGER_AVAILABLE;
	                } else if (m.getKey() != null && m.getKey().startsWith(ConstantsBase.NO_CASH_FOR_NIOU_MSG)) {
	                	errorCode = ERROR_CODE_NO_CASH_FOR_NIOU;
	                	result.setUserCashBalance(m.getKey().split("=")[1]); //Quick fix...
	                }else if (m.getKey() != null && m.getKey().equalsIgnoreCase("error.investment.sum.limit")) {
	                	errorCode = ERROR_CODE_VALIDATION_EXPOSURE;
	                }
	                result.setErrorCode(errorCode);
	                result.addUserMessage("", CommonUtil.getMessage(user.getLocale(), m.getKey(), m.getParams()), apiErrorCode);
            	}
            }
            result = (InvestmentMethodResult) initUser(result, user, request.getUtcOffset(),request.getWriterId());
    	} catch (Exception e) {
    		log.error("Error in insertInvestment.", e);
    		result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
    	return result;
    }
 
    /**
     * change password.
     * @param request
     * @return UserMethodResult.
     */
    public static UserMethodResult getChangePassword(ChangePasswordMethodRequest request, HttpServletRequest httpRequest) {
    	log.info("getChnagePassword: Old pass=" + "*****" + " New pass=" + "*****");
    	UserMethodResult result = new UserMethodResult();
        try {
        	User user = null;
        	if (request.isEncrypt()) {
                user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
            } else {
    		    user = validateAndLogin(request.getUserName(), request.getPassword(), result, request.getSkinId(), true, false, httpRequest);
    		}

            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }

            long skinId = request.getSkinId();
            
            if (request.getWriterId() == Writer.WRITER_ID_COPYOP_WEB) {
            	if(!com.anyoption.common.util.Utils.validateCaptcha(request.getCaptcha(), null, ConstantsBase.PLATFORM_ID_COPYOP)) {
            		result.setErrorCode(ERROR_CODE_INVALID_CAPTCH);
            		return result;
            	}
            }
            
            ArrayList<ErrorMessage> msgs = validateInput("changePass", request, user.getLocale());
            if (!msgs.isEmpty()) {
                result.setErrorCode(ERROR_CODE_INVALID_INPUT);
                result.addUserMessages(msgs);
                return result;
            }
            Map<String, MessageToFormat> mtf = UsersManagerBase.changePassValidation(user, request.getPassword(), request.getPasswordNew());
            if (!mtf.isEmpty()) {
                result.setErrorCode(ERROR_CODE_INVALID_INPUT);
                Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(skinId).getDefaultLanguageId()).getCode());
                for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
                    String key = i.next();
                    MessageToFormat m = mtf.get(key);
                    result.addUserMessage(key, CommonUtil.getMessage(locale, m.getKey(), m.getParams()));
                }
                return result;
            }
            // Change user details.
            String password = request.getPassword();
            String newPassword = request.getPasswordNew();

            UsersManagerBase.changePass(user, request.getPassword(), request.getPasswordNew(),  request.getWriterId());
            if(skinId == Skin.SKIN_ETRADER){
                password = password.toUpperCase();
                newPassword = newPassword.toUpperCase();
            }
            try {
                UsersManagerBase.insertUsersDetailsHistoryOneField(request.getWriterId(), user.getId(), user.getUserName(), String.valueOf(user.getClassId()), UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_PASSWORD,
                        AESUtil.encrypt(password), AESUtil.encrypt(newPassword));
            } catch (CryptoException ce) {
                throw new SQLException("Unable to encrypt String" + ce.getMessage());
            }
            user.setPassword(AESUtil.encrypt(request.getPasswordNew()));
            
            
            com.anyoption.common.managers.UsersManagerBase.updateUserNeedChangePass(user.getId(), false);
            user.setIsNeedChangePassword(false);
                        
            
            result.setUser(user);

            setUserRegulationParameters(result, user);

            result.addUserMessage(null, CommonUtil.getMessage(user.getLocale(), "password.changed", null));
        } catch (Exception e) {
            log.error("Error in doChangePassword.", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }




    /**
     * set optionPlusFlashSell.
     *
     * @param request
     * @return MethodResult.
     */
    public static MethodResult optionPlusFlashSell(OptionPlusMethodRequest request, WebLevelsCache levelsCache, HttpServletRequest httpRequest){
    	log.debug("optionPlusFlashSell: " + request);
    	Date sellTime = new Date();
    	Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
    	UserMethodResult result = new UserMethodResult();
    	long loginId = 0L;
		try {
			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
			}
		} catch (Exception e) {
			log.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
    	MessageToFormat m = null;
        try {
            long invId = request.getInvestmentId();
            double price = request.getPrice();
            //checking if user is logged in

            User user = validateAndLoginEncryptGetCity(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, true, httpRequest);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	             m = new MessageToFormat("error.investment.login", null);
	            result.addUserMessage("", CommonUtil.getMessage(locale, m.getKey(), m.getParams()));
            	return result;
            }

            validateUserRegulationAndNonReg(user, result, AnyoptionService.VALIDATE_REGULATION_INVESTMENT);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }            

            Investment investment = InvestmentsManagerBase.getInvestmentToSettle(invId, false, false);
            if (investment != null) { //if the investment is not settled yet
            	//get the investment
	    		invId = investment.getId();
				OptionPlusQuote quote = InvestmentsManagerBase.getLastQuote(user.getId(), invId, price);
	            long quoteId = quote.getId();
				if (quoteId > 0) { //if the user have qoute for this inv in last X sec (x=4 now)
					Date timeQuoteExpired = quote.getTimeQuoteExpired();
					if (sellTime.before(timeQuoteExpired)) {
						Opportunity o = InvestmentsManagerBase.getRunningOpportunityById(investment.getOpportunityId());
						if (System.currentTimeMillis() < o.getTimeFirstInvest().getTime()) {
		                    if (log.isDebugEnabled()) {
		                        log.debug("Opportunity not opened.");
		                    }
		                    m = new MessageToFormat("error.investment.notopened", null);
		                    result.addUserMessage("", CommonUtil.getMessage(locale, m.getKey(), m.getParams()));
						} else if (System.currentTimeMillis() > o.getTimeLastInvest().getTime()) {
			                log.debug("Opportunity expired.");
				            m = new MessageToFormat("error.investment.expired", null);
				            result.addUserMessage("", CommonUtil.getMessage(locale, m.getKey(), m.getParams()));
				        }
						if (m == null) {
		                    long priceLong = (long)(price*100);
		                    investment.setAcceptedSms(false); // make sure no SMS is sent when sell
		                    investment.setClosingLevel(quote.getQuoteLevel());
		                    investment.setTimeQuoted(quote.getTimeQuoted());
		                    InvestmentsManagerBase.settleInvestment(investment.getId(), request.getWriterId(), 0, investment, priceLong, loginId);

		                    m = new MessageToFormat("error.investment.ok", null);
		                    result.addUserMessage("", CommonUtil.getMessage(locale, m.getKey(), m.getParams()));
		                    result.setErrorCode(ERROR_CODE_SUCCESS);
		                    refreshUserInSession(user.getId(), httpRequest);
		                    //reply = "OK";
		                    log.info("option plus for invId: " + invId + " sell successfully reply: OK");
		                    InvestmentsManagerBase.updatePurchasedQuote(quoteId);
		                    if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
		                    	double convertAmount =  (investment.getAmount() - investment.getOptionPlusFee()) * investment.getRate();
		                		CommonUtil.sendNotifyForInvestment(
		                						investment.getOpportunityId(),
		                						investment.getId(),
		                						(-1d) * convertAmount,
		                						(-1L) * investment.getAmount(),
		                						investment.getTypeId(),
		                						investment.getMarketId(),
		                						OpportunityType.PRODUCT_TYPE_OPTION_PLUS,
		                						investment.getCurrentLevel(),
		                						levelsCache,
		                						user,
		                						investment.getCopyopType());
		                    }

						}
					} else {
						String msg = "Quote is expired, params: \n" +
            					"User id: " + user.getId() + "\n" +
            					"Investment id: " + invId + "\n" +
            					"Price: " + price + "\n" +
            					"Quote Time: " + quote.getTimeQuoted() + "\n" +
            					"Sell Time: " + sellTime;
						log.debug(msg);
						result.setErrorCode(ERROR_CODE_UNKNOWN);
					}
				} else {
	        		String msg = "Couldn't find matching quote to the followin params: \n" +
				 					"User id: " + user.getId() + "\n" +
				 					"Investment id: " + invId + "\n" +
				 					"Price: " + price + "\n" +
				 					" or that quote from " + quote.getTimeQuoted() + "is expired";
					log.debug(msg);
					m = new MessageToFormat("error.investment.more.7", null);
					result.addUserMessage("", CommonUtil.getMessage(locale, m.getKey(), m.getParams()));
					result.setErrorCode(ERROR_CODE_UNKNOWN);
					//reply = "more then 7 sec left from the time we put the price in the session";
	            }
	        } else {
	            log.debug("investment already settled inv id: "  + invId);
	            m = new MessageToFormat("error.investment.expired", null);
	            result.addUserMessage("", CommonUtil.getMessage(locale, m.getKey(), m.getParams()));
				result.setErrorCode(ERROR_CODE_UNKNOWN);
	            //reply = CommonUtil.getMessage("error.investment.expired", null);
	        }
        } catch (Exception e) {
            log.error("Failed to process flash sell request.", e);
            m = new MessageToFormat("error.investment.expired", null);
            result.addUserMessage("", CommonUtil.getMessage(locale, m.getKey(), m.getParams()));
			result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }

    /**
     * insert new Contact to the data base.
     *
     * @param request
     * @return ContactMethodResult.
     */
    public static ContactMethodResult insertContact(ContactMethodRequest request) {
    	log.info("insertContact: email=" + request.getContact().getEmail());
    	ContactMethodResult result = new ContactMethodResult();
    	Contact contact = request.getContact();
    	try {
    		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
    		if (contact.getCountryId() <= 0) { //if i dont have a country.. not login case
    			String countryPrefix = "";
                String countryCode = CommonUtil.getCountryCodeByIp(request.getIp());
                log.debug("Get Counrty code from IP, Country code: " + countryCode);
                Country c = null;
                if (!CommonUtil.IsParameterEmptyOrNull(countryCode))  {
                	c = CountryManagerBase.getCountry(countryCode);
                } else { // get default country from skin
                	long countryId = SkinsManagerBase.getSkin(request.getSkinId()).getDefaultCountryId();
                	c = CountryManagerBase.getCountry(countryId);
                }
                if (null != c) {
                	countryPrefix = c.getPhoneCode();
                }
                contact.setPhone(countryPrefix + contact.getPhone());
                contact.setCountryId(c.getId());
    		}
    		boolean isAPIUser = request.getWriterId() == Writer.WRITER_ID_API? true : false;
    		Map<String, MessageToFormat> mtf = ContactsManagerBase.validateContact(request.getContact(), locale, isAPIUser);
    		if (!mtf.isEmpty()) {
                result.setErrorCode(ERROR_CODE_INVALID_INPUT);
                for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
                    String key = i.next();
                    MessageToFormat m = mtf.get(key);
                    result.addUserMessage(key, CommonUtil.getMessage(locale, m.getKey(), m.getParams()));
                }
                return result;
            }
    		//set the contact param
    		contact.setUtcOffset(CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset()));
    		contact.setWriterId(request.getWriterId());
    		contact.setSkinId(request.getSkinId());
    		if (null != contact.getDynamicParameter()) {
    			log.info("comb id " + request.getContact().getCombId());
    			log.info("Dynamic Parameter " + contact.getDynamicParameter());
    			long affKey = CommonUtil.getAffIdFromDynamicParameter(contact.getDynamicParameter(), request.getContact().getCombId(), request.getSkinId());
    			contact.setAffiliateKey(affKey);
    		}
    		ContactsManagerBase.insertContact(contact, request.getIp(), locale, request.getIssue(), request.getComments(), request.getSkinId(), (int)request.getWriterId());
    		result.setContact(request.getContact());
    		try {
				if (null != request.getContact().getCombId()) {
					PixelGenerator.runPixel(request.getContact().getCombId(), PixelGenerator.PIXEL_TYPE_CONTACTME, request.getDeviceId(), request.getIp());
				}
			} catch (RuntimeException e) {
				log.error("cant run pixel", e);
			}
    	} catch (Exception e) {
            log.error("Error in insertContact.", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
    }

    /**
     * @param request
     * @param levelsCache
     * @return OptionPlusMethodResult.
     */
    public static OptionPlusMethodResult getPrice(OptionPlusMethodRequest request, WebLevelsCache levelsCache, OpportunityCache oc, HttpServletRequest httpRequest) {
    	log.debug("getPrice: " + request);
    	Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
    	OptionPlusMethodResult result = new OptionPlusMethodResult();
    	try {
	    	User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
	    	if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	    		return result;
	    	}
	    	MessageToFormat m = null;
	        String reply = null;
	        long invId = request.getInvestmentId();
	        double price = 0;

            Investment investment = InvestmentsManagerBase.getInvestmentToSettle(invId, false, false);
            if (null != investment) {
            	OpportunityCacheBean opp = oc.getOpportunity(investment.getOpportunityId());
            	Market market = MarketsManagerBase.getMarket(investment.getMarketId());
                if (null != opp) {
                    WebLevelLookupBean wllb = new WebLevelLookupBean();
                    wllb.setSkinGroup(SkinsManagerBase.getSkin(user.getSkinId()).getSkinGroup());
                    wllb.setOppId(opp.getId());
                    wllb.setFeedName(market.getFeedName());
                    levelsCache.getLevels(wllb);
                    double promil = 0;
                    BigDecimal etLevel = new BigDecimal(String.valueOf(wllb.getDevCheckLevel()));
                    etLevel = etLevel.round(new MathContext(etLevel.precision() - etLevel.scale() + Integer.parseInt(String.valueOf(market.getDecimalPoint())), RoundingMode.HALF_UP));
                    log.debug("etrader level from service = " + wllb.getDevCheckLevel() + " after round = " + etLevel.doubleValue());
                    if (etLevel.doubleValue() != 0) { //if level from servies != 0
                        if (investment.getTypeId() == 1) {
                            promil = etLevel.doubleValue() - investment.getCurrentLevel();
                        } else {
                            promil = investment.getCurrentLevel() - etLevel.doubleValue();
                        }
                        promil = promil / investment.getCurrentLevel();
                        promil = promil * 1000;
                        Date timeQuoted = new Date();
                        double minPass = (timeQuoted.getTime() - opp.getTimeFirstInvest().getTime()) / 60000;
                        if (minPass >= 0 && minPass < 55) {
                        	log.debug("etrader level after round: " + etLevel.doubleValue() + " inv level: " + investment.getCurrentLevel() + " promil = " + promil + " minutes pass = " + minPass);
                            BigDecimal invAmount = (new BigDecimal(investment.getAmount()).subtract(new BigDecimal(String.valueOf(investment.getOptionPlusFee())))).divide(new BigDecimal(String.valueOf(100)));
                            price = InvestmentsManagerBase.calcOptionsPlusePrice(promil, minPass, opp.getMarketId(), investment.getUserId(), invId, timeQuoted, invAmount, etLevel.doubleValue());
                            reply = String.valueOf(price);
                            result.setPrice(reply);
                            result.setPriceTxt(CommonUtil.formatCurrencyAmountAO(price*100, true, user.getCurrencyId()));
                        } else {
                            log.debug("min pass error: " + minPass);
                            result.setErrorCode(ERROR_CODE_UNKNOWN);
                            m = new MessageToFormat("error.investment", null);
                            result.addUserMessage("", CommonUtil.getMessage(locale, m.getKey(), m.getParams()));
                        }
                    } else { //level from service = 0 return error
                	  	log.debug("level from service is equel to 0! level: " + etLevel.doubleValue());
                	  	m = new MessageToFormat("error.investment", null);
                        result.addUserMessage("", CommonUtil.getMessage(locale, m.getKey(), m.getParams()));
                    }
                } else {
                    log.debug("opp not found maybe settled opp id: " + investment.getOpportunityId());
                    m = new MessageToFormat("error.investment", null);
                    result.addUserMessage("", CommonUtil.getMessage(locale, m.getKey(), m.getParams()));
                }
            } else {
                log.debug("investment not found maybe settled investment id: " + invId);
                m = new MessageToFormat("error.investment", null);
                result.addUserMessage("", CommonUtil.getMessage(locale, m.getKey(), m.getParams()));
            }
        } catch (Exception e) {
            log.error("Failed to process flash get price request.", e);
        }
        return result;
    }

    /**
     *
     * @param request
     * @return CreditCardsMethodResult. Available Credit Cards With Empty Line.
     * @see #getAvailableCreditCards(UserMethodRequest, boolean)
     */
    public static CreditCardsMethodResult getAvailableCreditCardsWithEmptyLine(UserMethodRequest request, HttpServletRequest httpRequest) {
    	return getAvailableCreditCards(request, true, httpRequest);
    }


    /**
     * overloaded method.
     *
     * @param request
     * @return Available Credit Cards.
     * @see #getAvailableCreditCards(UserMethodRequest, boolean)
     */
    public static CreditCardsMethodResult getAvailableCreditCards(UserMethodRequest request, HttpServletRequest httpRequest) {
    	return getAvailableCreditCards(request, false, httpRequest);
    }

    /**
     * get the available credit cards for the user.
     *
     * @param request
     * @param isEmptyLine
     * @return CreditCardsMethodResult. MethodResult with list of credit cards.
     */
    public static CreditCardsMethodResult getAvailableCreditCards(UserMethodRequest request, boolean isEmptyLine, HttpServletRequest httpRequest) {
    	log.debug("getAvailableCreditCards: " + request);
    	Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
    	CreditCardsMethodResult result = new CreditCardsMethodResult();
    	try {
    		User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
	        if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	            return result;
	        }
    		ArrayList<CreditCard> ccList = CreditCardsManagerBase.getDisplayCreditCardsByUser(user.getId());
    		int index = 0;
    		int enlargeArraySize = 0;
    		if (isEmptyLine) {
    			enlargeArraySize = 1;
    		}
    		CreditCard[] ccListArray = new CreditCard[ccList.size() + enlargeArraySize];
    		CreditCard emptyRow = new CreditCard();
    		emptyRow.setId(0);
    		emptyRow.setCcNumber(0);
    		emptyRow.setTypeName(CommonUtil.getMessage(locale, "deposit.select", null));
    		if (isEmptyLine) {
    			index = 1;
    			ccListArray[0] = emptyRow;
    		}
    		long lastActiveCC = CreditCardsManagerBase.getLastActiveCreditCardsByUser(user.getId());
    		for (; index < ccList.size() + enlargeArraySize ; index++) {
    			ccListArray[index] = ccList.get(index - enlargeArraySize);
    			ccListArray[index].setTypeName(CommonUtil.getMessage(locale, ccList.get(index - enlargeArraySize).getType().getName(), null));
    			if (lastActiveCC == ccListArray[index].getId()) {    				
    				ccListArray[index].setSelected(true);
    			}
    		}
    		result.setOptions(ccListArray);
    	} catch (Exception e) {
            log.error("Error in getAvailableCreditCards", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
    }

    /**
     *
     * @param request
     * @return all the credit card that the user can withdraw with them.
     */
    public static CreditCardsMethodResult getWithdrawCCList(UserMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("getWithdrawCCList: " + request);
    	Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
    	CreditCardsMethodResult result = new CreditCardsMethodResult();
    	try {
    		User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
        	long ccMinWithdraw = TransactionsManagerBase.getCreditCardMinWithdraw(user.getCurrencyId());
        	
            ArrayList<CreditCard> ccWithdrawalListArray = CreditCardsManagerBase.getDisplayWithrawalCreditCardsByUser(user.getId(), null,
            		ccMinWithdraw, user.getSkinId(), true);

            int arraySize = 0;
            for (CreditCard c: ccWithdrawalListArray) {
            	if(c.getCreditAmount() > 0) {
            		arraySize++;
            	}
            }
           // List is empty
			if (ccWithdrawalListArray.size() == 0 || arraySize == 0) {
				return result;
			}
			CreditCard[] ccListArray = new CreditCard[arraySize + 1];
			CreditCard emptyRow = new CreditCard();
			emptyRow.setId(0);
			emptyRow.setCcNumber(0);
			emptyRow.setTypeName(CommonUtil.getMessage(locale, "deposit.select", null));
			ccListArray[0] = emptyRow;
			int index = 1;
			for (; index < ccListArray.length ; index++) {
				if(ccWithdrawalListArray.get(index-1).getCreditAmount() > 0){
					ccListArray[index] = ccWithdrawalListArray.get(index - 1);
					ccListArray[index].setTypeName(CommonUtil.getMessage(locale, ccWithdrawalListArray.get(index - 1).getType().getName(), null));
					CreditCardHelper.initCreditCardCurrencyAmount(ccListArray[index], user.getCurrencyId());
//					ccListArray[index].initFormattedValues(user.getCurrencyId());
				}
			}
            result.setOptions(ccListArray);
    	} catch (Exception e) {
    		 log.error("Error in getWithdrawCCList", e);
             result.setErrorCode(ERROR_CODE_UNKNOWN);
    	}
    	return result;
    }

    /**
     * This method check if the application need to be update in the user device.
     * Update will occur automatically if the update must to happen.
     *
     * This method check for device unique id skin, if it exist the method will insert to the data base the information
     * about deviceId, skinId, userAgent, ip, countryId, combId and  application version.
     *
     * @param request
     * @return UpdateMethodResult
     * @see com.anyoption.json.results.UpdateMethodResult
     */
    public static UpdateMethodResult checkForUpdate(CheckUpdateMethodRequest request, HttpServletRequest httpRequest) {
        log.debug("checkForUpdate: " + request);
        UpdateMethodResult result = new UpdateMethodResult();
        try {
        	long skinId = 0;
        	result.setHasDynamics(CommonUtil.getProperty("isHasDynamics.always.flag").equalsIgnoreCase("true"));
            DeviceUniqueIdSkin record = DeviceUniqueIdsSkinManager.getByDeviceId(request.getDeviceId());
            Long detecCountryIdbyIp = getCountryFromIp(request.getIp(), httpRequest);
            //define Skin
            Skin skin = defineSkinAndSetInSession(httpRequest, request, request.isFirstOpening(), false, null);
            if (null == record) {
                log.debug("Add new record for: " + request.getDeviceId());

                long countryId = 0;
                if(detecCountryIdbyIp != null){
                	countryId = detecCountryIdbyIp;
                }
                if (countryId != 0) {
                    result.setCountryId(countryId);
                }               
                skinId = skin.getId();
                
                result.setSkinId(skinId);
                //get default currency
                CurrenciesRules currenciesRules = new CurrenciesRules();
                currenciesRules.setCountryId(countryId);
                currenciesRules.setSkinId(skinId);
                result.setCurrencyId(CurrenciesRulesManagerBase.getDefaultCurrencyId(currenciesRules));
                try {
					if (null != request.getCombinationId()) {
						PixelGenerator.runPixel(request.getCombinationId(), PixelGenerator.PIXEL_TYPE_HOMEPAGE, request.getDeviceId(), request.getIp());
					}
				} catch (RuntimeException e) {
					log.error("cant run pixel", e);
				}
				long affKey = 0;
				if (null != request.getDynamicParam()) {
					affKey = CommonUtil.getAffIdFromDynamicParameter(request.getDynamicParam(), request.getCombinationId(), skinId);
				}
                DeviceUniqueIdsSkinManager.insert(request.getDeviceId(), skinId, request.getApkName(), request.getIp(), countryId == 0 ? null : countryId, request.getCombinationId(),
                		String.valueOf(request.getApkVersion()), request.getDynamicParam(), affKey, request.getAppsflyerId(), request.getOsTypeId(), request.getDeviceFamily(), request.getIdfa(), 
                		request.getAdvertisingId());
                //fire pixel 
                if (!CommonUtil.isParameterEmptyOrNull(request.getAdvertisingId()) || !CommonUtil.isParameterEmptyOrNull(request.getIdfa())) {
                	serverPixelThread(request.getWriterId(), request.getAdvertisingId(), request.getIdfa(), request.getDeviceId());
                }                                
            } else {
                //define Skin
                skinId = skin.getId();
                result.setSkinId(skinId);
                if(record.getSkinId() != skin.getId()){
                	DeviceUniqueIdsSkinManager.updateSkindId(request.getDeviceId(), skin.getId());
                }
                
                if (record.getCombId() > 0) {
                    result.setCombId(record.getCombId());
                }
                if (null != record.getDynamicParam()) {
                    result.setDynamicParam(record.getDynamicParam());
                }
                boolean isAppVersionChanged = null != record.getAppVer() 
                									&& !record.getAppVer().equals(String.valueOf(request.getApkVersion()));
                boolean isAdvertisingIdChanged = CommonUtil.isParameterEmptyOrNull(record.getAdvertisingId()) 
                									&& !CommonUtil.isParameterEmptyOrNull(request.getAdvertisingId());
                boolean isIdfaChanged = CommonUtil.isParameterEmptyOrNull(record.getIdfa()) 
													&& !CommonUtil.isParameterEmptyOrNull(request.getIdfa());
                if (isAppVersionChanged || isAdvertisingIdChanged || isIdfaChanged) {
                    log.debug("Update app version for: " + request.getDeviceId() + " to: " + request.getApkVersion());
                    DeviceUniqueIdsSkinManager.updateVersion(request.getDeviceId(), String.valueOf(request.getApkVersion()), request.getIdfa(), request.getAdvertisingId());
                    //fire pixel 
                    if (isAdvertisingIdChanged || isIdfaChanged) {
                    	serverPixelThread(request.getWriterId(), request.getAdvertisingId(), request.getIdfa(), request.getDeviceId());
                    }
                }
                //get default currency
                CurrenciesRules currenciesRules = new CurrenciesRules();
                currenciesRules.setCountryId(record.getCountryId());
                currenciesRules.setSkinId(record.getSkinId());
                result.setCurrencyId(CurrenciesRulesManagerBase.getDefaultCurrencyId(currenciesRules));
            }
            
            //bubbles logout temp user in session
            User logOutUser = new User();
            logOutUser.setId(0L);
            logOutUser.setSkinId(skinId);
            logOutUser.setUtcOffset("GMT+00:00");
            httpRequest.getSession().setAttribute(ConstantsBase.BIND_SESSION_USER, logOutUser);
            
        } catch (Exception e) {
            log.error("Error in processing DEVICE_UNIQUE_IDS_SKIN data.", e);
        }
        try {
            result.setNeedAPKUpdate(MobileApplication.isNeedUpdate(request.getApkName(), request.getApkVersion()));
        	if (result.isNeedAPKUpdate()) {
        		result.setForceAPKUpdate(MobileApplication.getForceUpdate(request.getApkName()));
        		result.setDownloadLink(MobileApplication.getDownloadLink(request.getApkName()));
        	}
        } catch (Exception e) {
            log.error("Error in checkForUpdate.", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }

        if(request.isFirstOpening()){
            //Marketing Tracking Add Click
            log.debug("Marketing Tracking first Open with params mId:" + request.getmId() +" and CombId:" + request.getCombinationId() + " and DynamicParam:" + request.getDynamicParam()
                    + " and Skind: " + result.getSkinId());
            try {
                String mIdResult = MarketingTrackerBase.insertMarketingTrackerClickFirstOpen(request.getmId(), request.getCombinationId(), request.getDynamicParam(), request.getUtmSource(),
                		result.getSkinId(), request.getAffSub1(), request.getAffSub2(), request.getAffSub3());
                result.setmId(mIdResult);
                log.debug("Marketing Tracking request.mId is:" + request.getmId() + " result.mId is :" + mIdResult);
            } catch (Exception e) {
                log.error("Marketing Tracking add click first Open ", e);
                result.setErrorCode(ERROR_CODE_UNKNOWN);
            }
        }
        
        result.setServerTime(System.currentTimeMillis());
        return result;
    }    

    /**
     * @param request
     * @return MethodResult
     */
    public static MethodResult c2dmUpdate(CheckUpdateMethodRequest request) {
        if (log.isTraceEnabled()) {
            log.trace("c2dmUpdate: " + request);
        }
        MethodResult result = new MethodResult();
        try {
            DeviceUniqueIdSkin record = DeviceUniqueIdsSkinManager.getByDeviceId(request.getDeviceId());
            if (null != record) {
                if (record.getC2dmRegistrationId() != request.getC2dmRegistrationId()) {
                    log.debug("Update c2dm Registration Id for: " + request.getDeviceId() + " to: " + request.getC2dmRegistrationId());
                    DeviceUniqueIdsSkinManager.updateC2dmRegistrationId(request.getDeviceId(), request.getC2dmRegistrationId());
                }
                result.setErrorCode(ERROR_CODE_SUCCESS);
            } else {
            	result.setErrorCode(ERROR_CODE_UNKNOWN);
            }
        } catch (Exception e) {
            log.error("Error in processing DEVICE_UNIQUE_IDS_SKIN data.", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }

    /**
     * @param request
     * @return InboxMethodResult
     */
    public static InboxMethodResult getUserInboxMails(UserMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("getUserInboxMails: " + request);
    	InboxMethodResult result = new InboxMethodResult();
    	 try {
         	User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
         	if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                 return result;
            }
         	String utcOffset = CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset());
         	ArrayList<MailBoxUser> emails = UsersManagerBase.getUserMailBox(user.getId());
         	result.setEmails(new ArrayList<MailBoxUser>());
         	for (int i = 0 ; i < emails.size() ; i++) {
         		MailBoxUserHelper.initMailBoxUser(emails.get(i), utcOffset);
         		result.getEmails().add(emails.get(i));
         	}
    	 } catch (Exception e) {
    		 log.error("Error in getting inbox data", e);
    		 result.setErrorCode(ERROR_CODE_UNKNOWN);
    	 }
    	 return result;
    }

    /**
     * @param request
     * @return MethodResult
     */
    public static MethodResult updateMailBox(MailBoxMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("updateMailBox: " + request);
    	MethodResult result = new MethodResult();
    	try {
    		User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
    		if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
    		String utcOffset = CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset());
    		UsersManagerBase.updateEmailsByUserId(user.getId(), request.getMailBoxUser().getStatusId(),
    				Long.toString(request.getMailBoxUser().getId()), utcOffset);
    	} catch (Exception e) {
   		 	log.error("Error in getting inbox data", e);
   		 	result.setErrorCode(ERROR_CODE_UNKNOWN);
    	}
    	return result;
    }

    /**
     * update Skin Id in the data base.
     * This method get the device user id from the data base, if its null an error will be added to the result.
     * If there is such device and the skin id of the request is unequal to the skin device from the data base
     * the updated will occur (in the data base).
     *
     *
     * @param request
     * @return MethodResult
     */
    public static UpdateMethodResult skinUpdate(CheckUpdateMethodRequest request, HttpServletRequest httpRequest) {
        if (log.isTraceEnabled()) {
            log.trace("skinUpdate: " + request);
        }
        UpdateMethodResult result = new UpdateMethodResult();
        try {
        	Skin skin = defineSkinAndSetInSession(httpRequest, request, false, true, null);
        	result.setSkinId(skin.getId());
            DeviceUniqueIdSkin record = DeviceUniqueIdsSkinManager.getByDeviceId(request.getDeviceId());
            if (null != record) {
                if (record.getSkinId() != result.getSkinId()) {
                    log.debug("Update skinId for: " + request.getDeviceId() + " to: " + result.getSkinId());
                    DeviceUniqueIdsSkinManager.updateSkindId(request.getDeviceId(), result.getSkinId());
                }
                result.setErrorCode(ERROR_CODE_SUCCESS);
            }
        } catch (Exception e) {
            log.error("Error in processing skinUpdate ", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }

    /**
     * @param request
     * @return MethodResult.
     */
    public static MethodResult sendPassword(InsertUserMethodRequest request, HttpServletRequest httpRequest) {
    	log.info("Action: sendPassword");
    	MethodResult result = new MethodResult();

    	long skinId = request.getSkinId();

//    	String resKey = UsersManagerBase.generateRandomKey();
    	try {
    		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(skinId).getDefaultLanguageId()).getCode());
    		
			String email = (String) httpRequest.getSession().getAttribute("emailForgotPass");
			String phone = (String) httpRequest.getSession().getAttribute("phoneForgotPass");
			String prefix = (String) httpRequest.getSession().getAttribute("prefixForgotPass");
			
    		Map<String, MessageToFormat> mtf = UsersManagerBase.sendResetPassword(email, phone, prefix, request.isSendMail(), request.isSendSms(), skinId);
//	    	if (skinId == ConstantsBase.SKIN_ETRADER){
//	    		mtf = UsersManagerBase.sendResetPassword(request.getRegister().getEmail(), birthDate, idNumLong, skinId);
//	    	} else {
//	    		mtf = UsersManagerBase.sendResetPassword(request.getRegister().getEmail(), birthDate, 0, skinId);
//	    	}
			if (!mtf.isEmpty()) {
	            result.setErrorCode(ERROR_CODE_INVALID_INPUT);
	            for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
	                String key = i.next();
	                MessageToFormat m = mtf.get(key);
	                result.addUserMessage(key, CommonUtil.getMessage(locale, m.getKey(), m.getParams()));
	            }
	            return result;
	        }
//			ResetPasswordManager.insert(request.getRegister().getUserName(), resKey);
			result.addUserMessage(null, CommonUtil.getMessage(locale, "password.reset.link.success", null));
			result.setErrorCode(ERROR_CODE_SUCCESS);
	    	} catch (Exception e) {
	            log.error("Error in forgotPassword", e);
	            result.setErrorCode(ERROR_CODE_UNKNOWN);
	        } finally {
				httpRequest.getSession().removeAttribute("emailForgotPass");
				httpRequest.getSession().removeAttribute("phoneForgotPass");
				httpRequest.getSession().removeAttribute("prefixForgotPass");
	        }
	    	return result;
    }

    /**
     * insert Register.
     *
     * @param request. Contains: DeviceId, UtcOffset, CombinationId, SkinId, Ip, Email, FirstName, LastName, MobilePhone, isContactByEmail, isContactBySMS, CountryId, DynamicParameter
     * @return RegisterAttemptMethodResult Containing the RegisterAttemptId or ErrorCode
     */
    public static MethodResult insertRegisterAttempt(RegisterAttemptMethodRequest request) {
    	log.info("insertRegisterAttempt: duid = " + request.getDeviceId());
    	RegisterAttemptMethodResult result = new RegisterAttemptMethodResult();
    	try {
    		String utcOffset = CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset());
            Long combId = request.getCombinationId();
            if (combId == null || MarketingCombinationManagerBase.getById(combId) == null) {
                combId = SkinsManagerBase.getSkin(request.getSkinId()).getDefaultCombinationId();
            }

            // Marketing Tracking Check if exist Mid by email or phone
            try {
                if (CommonUtil.IsParameterEmptyOrNull(request.getmId())){
                    log.debug("Marketing Tracking staticPart/mId is null set default ");
                    String marketingStaticPart = MarketingTrackerBase.createDefaultStatciCookieData(null, null, null, null, request.getSkinId(), null, null, null);
                    String mId = MarketingTrackerBase.getMidFromStaticPart(marketingStaticPart);

                    request.setMarketingStaticPart(marketingStaticPart);
                    request.setmId(mId);

//                }else {
//                    log.debug("Marketing Tracking Check mId for emai: " + request.getEmail() + " and pnoe: " + request.getMobilePhone() + " and current static part : " + request.getmId());
//                    String existMId = MarketingTrackerBase.checkMidByContactDetailsInsertingContacts(request.getEmail(), request.getMobilePhone(), request.getmId(), request.getSkinId());
//                    if (existMId != null) {
//                        String staticPart = MarketingTrackerBase.getStaticPartFromMid(existMId);
//
//                        request.setmId(existMId);
//                        result.setmId(existMId);
//                        if (staticPart != null){
//                            result.setMarketingStaticPart(staticPart);
//                        }
//                        log.debug("Marketing Tracking found exist mId for emai: " + request.getEmail() + " and pnoe: " + request.getMobilePhone() + " replace wih new value : " + existMId);
//                    }
                }
                //ETS Marketing
//                String etsMidExist = MarketingETSMobile.checkMarketingEtsMidInsertingContact(request.getEtsMId(), request.getEmail(), request.getMobilePhone());
//                if (etsMidExist != null){
//                    result.setEtsMId(etsMidExist);
//                }
            } catch (Exception e) {
                log.error("Marketing Tracking when check exist mId.", e);
            }

    		long regAttemptId = ContactsManagerBase.insertRegAttempt(request.getIp(), request.getEmail(), request.getFirstName(), request.getLastName(),
    				request.getMobilePhone(), request.getLandLinePhone(), request.isContactByEmail(), request.isContactBySMS(), request.getDeviceId(),
    				request.getSkinId(), request.getUserAgent(), (int)request.getWriterId(), false, utcOffset, combId, request.getCountryId(), request.getDynamicParameter(),
    				request.getId(), request.getHttpReferer(), request.getmId(), request.getAff_sub1(), request.getAff_sub2(), request.getAff_sub3());
    		result.setRegisterAttemptId(regAttemptId);
    		if (regAttemptId == 0) {
    			result.setErrorCode(ERROR_CODE_GENERAL_VALIDATION);
    		}
    	} catch (Exception e) {
            log.error("Error in insertRegisterAttempt.", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }

    /**
     *
     * @param request Containing skin_id an ip.
     * @return country id by IP and skin_id.
     */
    public static MethodResult getCountryId(MethodRequest request) {
    	log.info("getCountryId: skinId= " + request.getSkinId() + ", ip=" + request.getIp());
    	CountryIdMethodResult result = new CountryIdMethodResult();
    	try {
    		long countryId = CommonUtil.getCountryIdByIp(request.getSkinId(), request.getIp());
    		Country c = CountryManagerBase.getCountries().get(countryId);
    		if (c != null) {
    			result.setCountryStatus(c.getCountryStatus().getId());
    		}
    		result.setCountryId(countryId);
    	} catch (Exception e) {
    		log.error("Error in getCountryId.", e);
    		result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }

    /**
     * get in letters the currency.
     * @param request
     * @return currency by letters.
     */
    public static CurrencyMethodResult getCurrencyLetters(CurrencyMethodRequest request) {
    	log.info("getCurrencyLetters: skinId= " + request.getSkinId() + ", ip=" + request.getIp());
    	CurrencyMethodResult result = new CurrencyMethodResult();
    	try {
    		String currencyLetters = CommonUtil.getCurrencyLettersById(request.getCurrencyId());
    		result.setCurrencyLetters(currencyLetters);
    	} catch (Exception e) {
    		log.error("Error in getCountryId.", e);
    		result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }

    /**
     * send the receipt deposit.
     *
     * @param request
     * @return MethodResult.
     */
    public static MethodResult SendDepositReceipt(DepositReceiptMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("sendDepositReceipt: " + request);
    	MethodResult result = new MethodResult();
    	String footer = "";
    	try {
    		User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
    		if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
    		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
    		String utcOffset = CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset());
	    	Transaction t = TransactionsManagerBase.getById(request.getTransactionId());
	    	TransactionFormater.initFormattedValues(t, locale, utcOffset);
	    	//send the receipt deposit
        	String[] params = new String[1];
			params[0] = String.valueOf(t.getCc4digit());
			String receiptNum = t.getReceiptNumTxt();
			if ( user.getSkinId() != Skin.SKIN_ETRADER ) {  // for ao take transaction id
				receiptNum = String.valueOf(t.getId());
			}
			HashMap<String,String> emailParams = new HashMap<String, String>();
			emailParams.put(SendTemplateEmail.PARAM_USER_NAME, user.getFirstName() + " " + user.getLastName());

			footer=CommonUtil.getMessage("receipt.footer.creditcard.mb", params, user.getLocale());
			if(t.getClearingProviderId() == 26 || t.getClearingProviderId() == 27 || t.getClearingProviderId() == 28) //AMEX
            {
                footer=footer + System.getProperty("line.separator") + CommonUtil.getMessage("receipt.bank_descriptor", params, user.getLocale());
            }

			UsersManagerBase.sendMailTemplateFile(ConstantsBase.TEMPLATE_CC_RECEIPT_EMAIL, request.getWriterId(),
    				user, t.getAmountWF(), footer, receiptNum, emailParams);
    	} catch (Exception e) {
            log.error("can't send deposit receipt mail!!", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
    }

    /**
     * This method check user validation, validate insurance investments and set the investment and credit.
  	 *
     * @param request
     * @return InsurancesMethodResult.
     * @see com.anyoption.json.results.InsurancesMethodResult
     */
    public static InsurancesMethodResult buyInsurances(InsurancesMethodRequest request, WebLevelsCache levelsCache, OpportunityCache oc, HttpServletRequest httpRequest) {
    	log.debug("buyInsurances: " + request);
        InsurancesMethodResult result = new InsurancesMethodResult();
        try {
            User user = validateAndLoginEncryptGetCity(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, true, httpRequest);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
	            validateUserRegulationAndNonReg(user, result, AnyoptionService.VALIDATE_REGULATION_INVESTMENT);
	            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	                return result;
            }
            Long[] ids = request.getInvestmentIds();
            String[] results = new String[ids.length];
            long credit = 0;
            MessageToFormat mtf = null;
            for (int i = 0; i < ids.length; i++) {
                long amount = Math.round(request.getInsuranceAmounts()[i] * 100);
                mtf = InvestmentsManagerBase.validateBuyInsurance(user, ids[i], request.getInsuranceType(), amount, oc, request.getGmType());
                if (null == mtf) {
                	long loginId = 0L;
            		try {
            			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
            				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
            			}
            		} catch (Exception e) {
            			log.error("Cannot get login ID from session!", e);
            			loginId = 0L;
            		}
                    credit += InvestmentsManagerBase.buyInsurance(user, ids[i], request.getInsuranceType(), amount, request.getWriterId(), levelsCache, loginId);
                    RewardUserTasksManager.rewardTasksHandler(TaskGroupType.INVESTMENT_PREMIA, user.getId(),  amount, ids[i], BonusManagerBase.class, (int) request.getWriterId(), OpportunityType.PRODUCT_TYPE_BINARY, Opportunity.TYPE_REGULAR);
                    results[i] = "OK";
                    refreshUserInSession(user.getId(), httpRequest);
                } else {
                    results[i] = CommonUtil.getMessage(user.getLocale(), mtf.getKey(), mtf.getParams());
                }
            }
            result.setInvestmentIds(ids);
            result.setResults(results);
            result.setCredit(CommonUtil.formatCurrencyAmountAO(credit, true, user.getCurrencyId()));
        } catch (Exception e) {
            log.error("Error in buyInsurances.", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        if (log.isTraceEnabled()) {
            log.trace("buyInsurances result: " + result);
        }
        return result;
    }

    /**
     * @param request
     * @return MethodResult.
     */
    public static MethodResult insuranceBannerSeen(UserMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("insuranceBannerSeen: " + request);
        MethodResult result = new MethodResult();
        try {
        	User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
            boolean isViewed = InvestmentsManagerBase.isGMBannerViewed(user.getId());
            if (!isViewed) {
                if (InvestmentsManagerBase.insertGMBannerViewed(user.getId())) {
                	 result.setErrorCode(ERROR_CODE_SUCCESS);
                }
                result.setErrorCode(ERROR_CODE_UNKNOWN);
            }
           	result.setErrorCode(ERROR_CODE_UNKNOWN);
        } catch (Exception e) {
            log.error("Error in insert insurance Banner Seen.", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }

    /**
     * This method is used for the distribution mechanism for all CUP deposits
     * @param DeltapayInfoMethodRequest
     * @return DeltapayInfoMethodResult
     */
    public static CUPInfoMethodResult getCUPInfo(CUPInfoMethodRequest request, HttpServletRequest httpRequest) throws Exception {
    	log.debug("getCUPInfo: " + request);
    	CUPInfoMethodResult result = new CUPInfoMethodResult();
    	long loginId = 0L;
		try {
			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
			}
		} catch (Exception e) {
			log.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
    	User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
    	if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
            return result;
        }
    	validateUserRegulationAndNonReg(user, result, AnyoptionService.VALIDATE_REGULATION_DEPOSIT);
    	if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
    		return result;
    	}

    	Distribution distribution = TransactionsManagerBase.getDistributedProviderId(DistributionType.CUP, user.getId());
    	/*
		 * for testing purposes clearing providers with id 1022 will be treated as deltapay
		 */
		if(distribution.getProviderId() == ClearingManager.DELTAPAY_CHINA_PROVIDER_ID || distribution.getProviderId() == ClearingManager.DELTAPAY_CHINA_TEST_PROVIDER_ID) {
			result = getDeltapayInfo(distribution, request, httpRequest, loginId);
		} // currently cdpay is not supported on mobile device
		else if(distribution.getProviderId() == ClearingManager.CDPAY_PROVIDER_ID ) {
			result = getCdpayInfo(distribution, request, httpRequest);
		}
		// TODO add here the other CUP providers
		return result;
    }

    public static CUPInfoMethodResult getDeltapayInfo(Distribution distribution, CUPInfoMethodRequest request, HttpServletRequest httpRequest, long loginId) throws Exception{
    	log.debug("getDeltapayInfo: " + request);
    	CUPInfoMethodResult result = new CUPInfoMethodResult();
    	result.setClearingProviderId(ClearingManager.DELTAPAY_CHINA_PROVIDER_ID);
    	try{
	    	Map<String, MessageToFormat> mtf = new HashMap<String, MessageToFormat>();
	    	User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
	    	if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
	    	DeltaPayInfo info = new DeltaPayInfo();
    		String httpsUrl = CommonUtil.getConfig("homepage.url.https");
    		String homePageUrl = CommonUtil.getConfig("homepage.url");
    		String imagesDomain = CommonUtil.getConfig("images.sub.domain");
    		info = TransactionsManagerBase.insertDeltaPayChinaDeposit(request.getAmount(), request.getWriterId(), user, httpsUrl, imagesDomain, distribution.getSelectorId(), ClearingManager.DELTAPAY_CHINA_PROVIDER_ID, request.getIp(), mtf, info, loginId);
    		if (!mtf.isEmpty()) {
                result.setErrorCode(ERROR_CODE_INVALID_INPUT);
                for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
                    String key = i.next();
                    MessageToFormat m = mtf.get(key);
                    result.addUserMessage(key, CommonUtil.getMessage(user.getLocale(), m.getKey(), m.getParams()));
                }
            } else{
            	String params =  "affiliate=" + info.getAffiliate() +  "&paymethod=Credit Card" + "&processing_mode=sale" + "&location=AFF" + "&redirect=" + info.getRedirect() +
						 "&order_id=" + info.getTransactionId() + "&terminal_name=" + info.getTerminalName() + "&agent_name=" + info.getAgentName() + "&first_name=" + info.getFirstName() +
						 "&last_name=" + info.getLastName() + "&address1=" + info.getAddress() + "&city=" + info.getCity() + "&product_description=Anyoption" + "&state=NA" + "&zip=NA" +
						 "&country=" + info.getCountryA2() + "&telephone=" + info.getUserPhone() + "&amount=" + info.getFullAmountStr() + "&currency=" + info.getCurrencyName() + "&email=" + info.getEmail() +
						 "&card_type=" + info.getCardTypeName() + "&card_number=" + info.getCcn() + "&cvv=" + info.getCvv() + "&expiry_mo=" + info.getExpireMonth() + "&expiry_yr=" + info.getExpireYear() +
						 "&notification_url=" + homePageUrl + info.getSERVER_RETURN_URL();
            	result.setParams(params);
            	result.setPostURL(ClearingManager.getClearingProviders().get(ClearingManager.DELTAPAY_CHINA_PROVIDER_ID).getUrl());
            	refreshUserInSession(user.getId(), httpRequest);
            }
    		log.debug(info);
	    	} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				result.setErrorCode(ERROR_CODE_UNKNOWN);
			}
    	return result;
    }

    public static CUPInfoMethodResult getCdpayInfo(Distribution distribution, CUPInfoMethodRequest request, HttpServletRequest httpRequest) throws Exception{
    	log.debug("getCdpayInfo: " + request);
    	CUPInfoMethodResult result = new CUPInfoMethodResult();
    	result.setClearingProviderId(ClearingManager.CDPAY_PROVIDER_ID);
    	long loginId = 0L;
		try {
			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
			}
		} catch (Exception e) {
			log.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
    	try{
	    	Map<String, MessageToFormat> mtf = new HashMap<String, MessageToFormat>();
	    	User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
	    	if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
	    	CDPayInfo info = new CDPayInfo();
    		String httpsUrl = CommonUtil.getConfig("homepage.url.https");
    		String homePageUrl = CommonUtil.getConfig("homepage.url");
    		String imagesDomain = CommonUtil.getConfig("images.sub.domain");
    		info = TransactionsManagerBase.insertCDPayDeposit(request.getAmount(), request.getWriterId(), user, mtf, request.getIp(), distribution.getSelectorId(), homePageUrl, imagesDomain, null, loginId);
    		if (!mtf.isEmpty()) {
                result.setErrorCode(ERROR_CODE_INVALID_INPUT);
                for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
                    String key = i.next();
                    MessageToFormat m = mtf.get(key);
                    result.addUserMessage(key, CommonUtil.getMessage(user.getLocale(), m.getKey(), m.getParams()));
                }
            }else{
            	String params =  "version=" + info.getVersion() +
						 "&merchant_account=567654342"+ // info.getMerchantAccount() +
						 "&merchant_order=" + info.getMerchantOrder() +
						 "&merchant_product_desc=" + info.getMerchantProductDesc() +
						 "&first_name=" + info.getFirstName() +
						 "&last_name=" + info.getLastName() +
						 "&address1=" + info.getAddress() +
						 "&city=" + info.getCity() +
						 "&zip_code=" + info.getZip() +
						 "&country=" + info.getCountryA2() +
						 "&phone=" + info.getUserPhone() +
						 "&email=" + info.getEmail() +
						 "&amount=" + info.getAmount() +
						 "&currency=" + info.getCurrencySymbol() +
						 //"&bankcode=" + info.getBankcode() +
						 "&ipaddress=" + info.getIp() +
						 "&return_url=" + info.getReturnURL() +
						 "&amount=" + info.getAmount() +
						 "&server_return_url=" + info.getServerReturnURL() +
						 "&control=" + info.getControl();
		log.debug(params);
		result.setParams(params);
//		result.setPostURL("https://payment.ccp.boarding.dnlasia.com/");
		result.setPostURL(ClearingManager.getClearingProviders().get(ClearingManager.CDPAY_PROVIDER_ID).getUrl() + CDPayInfo.URL_POST_DEPOSIT);
		refreshUserInSession(user.getId(), httpRequest);
            }
    		log.debug(info);
	    	} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				result.setErrorCode(ERROR_CODE_UNKNOWN);
			}
    	return result;
    }

    public static TransactionMethodResult insertWithdrawDeltapay(CcDepositMethodRequest request, HttpServletRequest httpRequest){
    	log.debug("insertWithdrawDeltapay: " + request);
    	TransactionMethodResult result = new TransactionMethodResult();
    	long loginId = 0L;
		try {
			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
			}
		} catch (Exception e) {
			log.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
        try {
            User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }

            // cancel all bonuses that can be canceled or return error if something goes wrong
    		List<BonusUsers> bonusUsers = BonusManagerBase.getAllUserBonus(user.getId(), null, null, 0, user.getUtcOffset(), new ArrayList<>(Arrays.asList(CurrenciesManagerBase.getCurrency(user.getCurrencyId()))));
    		String error = null;
    		for (BonusUsers bu : bonusUsers) {
    			if (bu.isCanCanceled()) {
    				error = BonusManagerBase.cancelBonus(bu, user.getUtcOffset(), request.getWriterId(), user.getSkinId(), user, loginId, request.getIp());
    			}
    			if (error.equals(BonusManagerBase.ERROR_DUMMY)) {
    				log.error("Cannot cancel bonus with id: " + bu.getId());
    				result.setErrorCode(ERROR_CODE_GENERAL_VALIDATION);
    				return result;
    			} else {
    				error = "";
    			}
    		}

            CreditCard cc = null;
            long amountInCents = CommonUtil.calcAmount(request.getAmount());
            Map<String, MessageToFormat> mtf = new HashMap<String, MessageToFormat>();
            String tranDescriptionError = TransactionsManagerBase.validateWithdrawForm(amountInCents, user, true, true,
            		true, cc, null, null, false, mtf);

            synchronized (httpRequest.getSession()) {
            	boolean preWithdrawError = false;
	            if (tranDescriptionError == null) {
	            	WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
	            	if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
	            		tranDescriptionError = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
	            		result.setErrorCode(ERROR_CODE_INVALID_INPUT);
	            		result.addUserMessage(null, CommonUtil.getMessage(user.getLocale(), tranDescriptionError, null));
	            		preWithdrawError = true;
	            	}
	            }
	            if (!preWithdrawError) {
		            //in case of error insert transaction with failed status
		            if (!mtf.isEmpty()) {
		            	TransactionsManagerBase.insertDeltaPayChinaWithdraw(request.getAmount(), tranDescriptionError, user, Writer.WRITER_ID_MOBILE, false, request.getIp(), loginId);
		                result.setErrorCode(ERROR_CODE_INVALID_INPUT);
		                for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
		                    String key = i.next();
		                    MessageToFormat m = mtf.get(key);
		                    if (m.getErrorMsgType() == MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD) {
		                    	result.setErrorCode(ERROR_CODE_VALIDATION_WITHOUT_FIELD);
		                    }
		                    result.addUserMessage(key, CommonUtil.getMessage(user.getLocale(), m.getKey(), m.getParams()));
		                }
		            } else {
		            	TransactionsManagerBase.insertDeltaPayChinaWithdraw(request.getAmount(), tranDescriptionError, user, Writer.WRITER_ID_MOBILE, false, request.getIp(), loginId);
		            	long feeVal;
	            		feeVal = TransactionsManagerBase.getTransactionHomoFee(amountInCents, user.getCurrencyId(), TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW);

		            	if (user.getSkinId() != Skin.SKIN_ARABIC){
		            		result.setFee(CommonUtil.formatCurrencyAmountAO(feeVal, true, user.getCurrency()));
		        		} else {
		        			result.setFee(CommonUtil.displayAmountForInput(feeVal, false, user.getCurrencyId().intValue()) + " " + user.getCurrency().getSymbol() + " ");
		        		}
		                UsersManagerBase.getUserByName(user.getUserName(), user); // refresh balance
		                result.setBalance(user.getBalance() / 100.0);
		                result.setFormattedAmount(CommonUtil.formatCurrencyAmountAO(amountInCents, user.getCurrencyId()));
		            }
	            }
	            refreshUserInSession(user.getId(), httpRequest);
            }
        } catch (Exception e) {
            log.error("Error in validateWithdrawCard.", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
    }

    public static TransactionMethodResult getFeeAmountByType(FeeMethodRequest request, HttpServletRequest httpRequest){
    	log.debug("getFeeAmountByType: " + request);
    	TransactionMethodResult result = new TransactionMethodResult();
        try {
            User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
            Limit limit = UsersManagerBase.getLimitById(user.getLimitId());
            Integer i = (int) request.getTranTypeId();
            long feeVal = TransactionsManagerBase.getFeeByTranTypeId(limit.getId(), i);
            result.setFee(String.valueOf(feeVal));
        }	catch (Exception e){
        	log.error("error in getFeeAmountByType", e);
        	result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }

//    public static TransactionMethodResult insertBaropayDeposit(BaropayDepositMethodRequest request, HttpServletRequest httpRequest){
//    	log.debug("insertBaropayDeposit: " + request);
//    	TransactionMethodResult result = new TransactionMethodResult();
//    	Map<String, MessageToFormat> mtf = new HashMap<String, MessageToFormat>();
//    	long loginId = 0L;
//		try {
//			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
//				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
//			}
//		} catch (Exception e) {
//			log.error("Cannot get login ID from session!", e);
//			loginId = 0L;
//		}
//		try {
//			User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
//            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
//                return result;
//            }
//            if (CommonUtil.isUserRegulated(user) || user.isETRegulation()) {
//	            validateUserRegulation(user, result, AnyoptionService.VALIDATE_REGULATION_DEPOSIT);
//	            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
//	                return result;
//	            }
//            }
//			BaroPayInfo info = new BaroPayInfo();
//	    	info = TransactionsManagerBase.insertBaroPayDeposit(request.getAmount(), request.getWriterId(), user, request.getIp(), request.getLandPhone(), mtf, info, request.getSenderName(), loginId);
//	    	if(!mtf.isEmpty()){
//	    		result.setErrorCode(ERROR_CODE_INVALID_INPUT);
//                Locale locale = user.getLocale();
//                for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
//                    String key = i.next();
//                    MessageToFormat m = mtf.get(key);
//                    result.addUserMessage(key, CommonUtil.getMessage(locale, m.getKey(), m.getParams()));
//                }
//                return result;
//	    	}
//	    	if (result.getErrorCode() == ERROR_CODE_SUCCESS) {
//                UsersManagerBase.getUserByName(user.getUserName(), user); // refresh balance
//                result.setBalance(user.getBalance() / 100.0);
//                result.setTransactionId(result.getTransactionId());
//                result.setDollarAmount(CommonUtil.convertToBaseAmount(Long.parseLong(request.getAmount()), user.getCurrencyId(), new Date())/100);
//                result.setTransactionId(info.getTransactionId());
//                refreshUserInSession(user.getId(), httpRequest);
//            }
//
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			log.error("Error in insertBaropayDeposit.", e);
//            result.setErrorCode(ERROR_CODE_UNKNOWN);
//		}
//
//    	return result;
//    }

    public static TransactionMethodResult validateBaroPayWithdraw(WireMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("validateBaroPayWithdraw: " + request);
    	TransactionMethodResult result = new TransactionMethodResult();
    	Map<String, MessageToFormat> mtf = new HashMap<String, MessageToFormat>();
    	long loginId = 0L;
		try {
			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
			}
		} catch (Exception e) {
			log.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
       	try {
			User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
			if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	            return result;
	        }
			String tranDescriptionError = TransactionsManagerBase.validateWithdrawForm(CommonUtil.calcAmount(request.getWire().getAmount()), user, true, true, true, null, null, null, false, mtf);
            //in case of error insert transaction with failed status
            if (!mtf.isEmpty()) {
            	TransactionsManagerBase.insertBaroPayWithdraw(request.getWire().getAmount(), tranDescriptionError, user, Writer.WRITER_ID_MOBILE, false,
						request.getWire().getBankNameTxt(), request.getWire().getBeneficiaryName(), request.getWire().getAccountNum(), request.getIp(), mtf, loginId);
                result.setErrorCode(ERROR_CODE_INVALID_INPUT);
                for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
                    String key = i.next();
                    MessageToFormat m = mtf.get(key);
                    if (m.getErrorMsgType() == MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD) {
                    	result.setErrorCode(ERROR_CODE_VALIDATION_WITHOUT_FIELD);
                    }
                    result.addUserMessage(key, CommonUtil.getMessage(user.getLocale(), m.getKey(), m.getParams()));
                }
            } else {
            	long feeVal;
           		feeVal = TransactionsManagerBase.getTransactionHomoFee(CommonUtil.calcAmount(request.getWire().getAmount()), user.getCurrencyId(), TransactionsManagerBase.TRANS_TYPE_BAROPAY_WITHDRAW);

            	result.setFee(CommonUtil.formatCurrencyAmountAO(feeVal, true, user.getCurrency()));
            	refreshUserInSession(user.getId(), httpRequest);
            }
		} catch (Exception e) {
			log.error("Error in validateBaroPayWithdraw.", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
    	return result;
    }

    public static TransactionMethodResult insertWithdrawBaropay(WireMethodRequest request, HttpServletRequest httpRequest){
    	log.debug("insertWithdrawBaropay: " + request);
    	TransactionMethodResult result = new TransactionMethodResult();
    	Map<String, MessageToFormat> mtf = new HashMap<String, MessageToFormat>();
    	long loginId = 0L;
		try {
			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
			}
		} catch (Exception e) {
			log.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
    	try {
			User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
			if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	            return result;
	        }

			// cancel all bonuses that can be canceled or return error if something goes wrong
    		List<BonusUsers> bonusUsers = BonusManagerBase.getAllUserBonus(user.getId(), null, null, 0, user.getUtcOffset(), new ArrayList<>(Arrays.asList(CurrenciesManagerBase.getCurrency(user.getCurrencyId()))));
    		String error = null;
    		for (BonusUsers bu : bonusUsers) {
    			if (bu.isCanCanceled()) {
    				error = BonusManagerBase.cancelBonus(bu, user.getUtcOffset(), request.getWriterId(), user.getSkinId(), user, loginId, request.getIp());
    			}
    			if (error.equals(BonusManagerBase.ERROR_DUMMY)) {
    				log.error("Cannot cancel bonus with id: " + bu.getId());
    				result.setErrorCode(ERROR_CODE_GENERAL_VALIDATION);
    				return result;
    			} else {
    				error = "";
    			}
    		}

			synchronized (httpRequest.getSession()) {
				//NO validations?
				String tranDescriptionError = null;
				boolean preWithdrawError = false;
				if (tranDescriptionError == null) {
					WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
					if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
						tranDescriptionError = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
	            		result.setErrorCode(ERROR_CODE_INVALID_INPUT);
	            		result.addUserMessage(null, CommonUtil.getMessage(user.getLocale(), tranDescriptionError, null));
	            		preWithdrawError = true;
					}
				}
				if (!preWithdrawError) {
					Transaction tran = TransactionsManagerBase.insertBaroPayWithdraw(request.getWire().getAmount(), null, user, Writer.WRITER_ID_MOBILE, false,
										request.getWire().getBankNameTxt(), request.getWire().getBeneficiaryName(), request.getWire().getAccountNum(), request.getIp(), mtf, loginId);
			    	if(!mtf.isEmpty()){
			    		result.setErrorCode(ERROR_CODE_INVALID_INPUT);
		                Locale locale = user.getLocale();
		                for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
		                    String key = i.next();
		                    MessageToFormat m = mtf.get(key);
		                    result.addUserMessage(key, CommonUtil.getMessage(locale, m.getKey(), m.getParams()));
		                }
		                return result;
			    	}
					result.setTransactionId(tran.getId());
					UsersManagerBase.getUserByName(user.getUserName(), user); // refresh balance
		            result.setBalance(user.getBalance() / 100.0);
		            result.setFormattedAmount(CommonUtil.formatCurrencyAmountAO(CommonUtil.calcAmount(request.getWire().getAmount()), user.getCurrencyId()));
				}
				refreshUserInSession(user.getId(), httpRequest);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("Error in insertBaropayWithdraw.", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
    	return result;
    }

    public static BaroPayMethodResult checkBaroPayWithdrawAvailable(UserMethodRequest request, HttpServletRequest httpRequest){
    	log.debug("checkBaroPayWithdrawAvailable: " + request);
    	BaroPayMethodResult result = new BaroPayMethodResult();
    	try {
			User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
			if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	            return result;
	        }
			result.setAllowBaroPayWithdraw(BaroPayManagerBase.isHaveBaroPaySuccessDeposit(user.getId()));
		} catch (Exception e) {
			log.error("Error in checkBaroPayWithdrawAvailable.", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
    	return result;
    }

    /**
     * get minimum first deposit by skin and currency.
     * @param request as UserMethodRequest
     * @return result as TransactionMethodResult
     */
    public static TransactionMethodResult getMinFirstDeposit(UserMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("getMinFirstDeposit: " + request);
    	TransactionMethodResult result = new TransactionMethodResult();
    	 try {
    		User user = null;
    		if (request.getUserName() != null) {
    			user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
    		}
         	if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
         	Skin skin = SkinsManagerBase.getSkin(request.getSkinId());
         	long currencyId = skin.getDefaultCurrencyId();
         	if (null != user && user.getId() != 0) {
    			currencyId = user.getCurrencyId();
    		}
    		long limitId = SkinsManagerBase.getSkinCurrenciesHM(request.getSkinId()).get(currencyId).getLimitId();
    		result.setFormattedAmount(CommonUtil.formatCurrencyAmountAO(LimitsManagerBase.getLimit(limitId).getMinFirstDeposit(), currencyId));
    	 } catch (Exception e) {
             log.error("Error in getMinFirstDeposit", e);
             result.setErrorCode(ERROR_CODE_UNKNOWN);
         }
    	 return result;
    }

    public static MethodResult insertMarketingTrackerClick(InsertMarketingTrackerClickRequest request) {
        if (log.isTraceEnabled()) {
            log.trace("Click insert: " + request);
        }
        MethodResult result = new MethodResult();
        try {
        	MarketingTracker.insertMarketingTrackerClick(request.getStaticPart(), request.getDynamicPart());
        } catch (Exception e) {
            log.error("Error in processing insertMarketingTrackerClick.", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }

    public static UserMethodResult updateMandatoryQuestionnaireDone(UserMethodRequest request, HttpServletRequest httpRequest) {
    	UserMethodResult result = new UserMethodResult();
    	result.setErrorCode(ERROR_CODE_UNKNOWN);
    	return result;
    	//return updateQuestionnaireDone(request, QuestionnaireGroup.REGULATION_MANDATORY_QUESTIONNAIRE_NAME, httpRequest);
    }

    public static UserMethodResult updateOptionalQuestionnaireDone(UserMethodRequest request, HttpServletRequest httpRequest) {
    	UserMethodResult result = new UserMethodResult();
    	result.setErrorCode(ERROR_CODE_UNKNOWN);
    	return result;
    	//return updateQuestionnaireDone(request, QuestionnaireGroup.REGULATION_OPTIONAL_QUESTIONNAIRE_NAME, httpRequest);
    }
    
    public static UserMethodResult updateCapitalQuestionnaireDone(UserMethodRequest request, HttpServletRequest httpRequest) {
    	UserMethodResult result = new UserMethodResult();
    	result.setErrorCode(ERROR_CODE_UNKNOWN);
    	return result;
    	//return updateQuestionnaireDone(request, QuestionnaireGroup.REGULATION_CAPITAL_QUESTIONNAIRE_NAME, httpRequest);
    }
    
    public static UserMethodResult updateSingleQuestionnaireDone(UserMethodRequest request, HttpServletRequest httpRequest) {
    	UserMethodResult result = new UserMethodResult();
    	try {
	    	log.debug("updateQuestionnaireDone: " + request);
			User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
			if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	            return result;
	        }
			result = updateQuestionnaireDone(user, user.getWriterId(), request.getUtcOffset(), QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME, httpRequest);
			
			if(result.getErrorCode() == ERROR_CODE_REG_SUSPENDED_QUEST_INCORRECT ||
				 result.getErrorCode() == ERROR_CODE_REGULATION_USER_RESTRICTED) {
					UsersManagerBase.sendMailAfterQuestAORegulation(result.getUserRegulation());
			}
			
			if(result.getUserRegulation().getPepState() == UserRegulationBase.PEP_AUTO_PROHIBITED) {
					UsersManagerBase.sendMailTemplateFile(ConstantsBase.TEMPLATE_REGULATION_PEP, user.getWriterId(), user, null, null, null, null);
			}
			return result;
	    } catch (Exception e) {
			log.error("Error in update [" + QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME + "] questionnaire status", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
			return result;
		}
    }

    public static UserQuestionnaireResult getUserMandatoryQuestionnaire(UserMethodRequest request, HttpServletRequest httpRequest) {
    	return getUserQuestionnaire(request, QuestionnaireGroup.REGULATION_MANDATORY_QUESTIONNAIRE_NAME, httpRequest);
    }

    public static UserQuestionnaireResult getUserOptionalQuestionnaire(UserMethodRequest request, HttpServletRequest httpRequest) {
    	return getUserQuestionnaire(request, QuestionnaireGroup.REGULATION_OPTIONAL_QUESTIONNAIRE_NAME, httpRequest);
    }
    
    public static UserQuestionnaireResult getUserCapitalQuestionnaire(UserMethodRequest request, HttpServletRequest httpRequest) {
    	return getUserQuestionnaire(request, QuestionnaireGroup.REGULATION_CAPITAL_QUESTIONNAIRE_NAME, httpRequest);
    }
    
    public static UserQuestionnaireResult getUserSingleQuestionnaire(UserMethodRequest request, HttpServletRequest httpRequest) {
    	return getUserQuestionnaire(request, QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME, httpRequest);
    }
    
    public static UserQuestionnaireResult getUserSingleQuestionnaireDynamic(UserMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("getUserSingleQuestionnaireDynamic: " + request);
    	UserQuestionnaireResult result = new UserQuestionnaireResult();
    	try {
			User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
			if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	            return result;
	        }			
			if ((!CommonUtil.isUserRegulated(user) && user.getSkinId() != Skin.SKIN_ETRADER) 
					|| (!user.isETRegulation() && user.getSkinId() == Skin.SKIN_ETRADER)) {
				log.error("Requesting regulation questionnaire a non-regulated user: " + request.getUserName());
				result.setErrorCode(ERROR_CODE_USER_NOT_REGULATED);
				return result;
			} else {
				validateUserRegulationAndNonReg(user, result, AnyoptionService.VALIDATE_REGULATION_OTHER);
				if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	                return result;
	            }
			}
			
			result = getUserSingleQuestionnaireDynamic(user, request.getWriterId());

			initUser(result, user, request.getUtcOffset(),request.getWriterId());
		} catch (Exception e) {
			log.error("Error in get user questionnaire [" +  QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME + "]", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
    	return result;
    }
    


    private static UserQuestionnaireResult getUserQuestionnaire(UserMethodRequest request, String questionnaireName, HttpServletRequest httpRequest) {
    	log.debug("getUserQuestionnaire: " + request);
    	UserQuestionnaireResult result = new UserQuestionnaireResult();
    	try {
			User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
			if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	            return result;
	        }			
			if ((!CommonUtil.isUserRegulated(user) && user.getSkinId() != Skin.SKIN_ETRADER) 
					|| (!user.isETRegulation() && user.getSkinId() == Skin.SKIN_ETRADER)) {
				log.error("Requesting regulation questionnaire a non-regulated user: " + request.getUserName());
				result.setErrorCode(ERROR_CODE_USER_NOT_REGULATED);
				return result;
			} else {
				validateUserRegulationAndNonReg(user, result, AnyoptionService.VALIDATE_REGULATION_OTHER);
				if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	                return result;
	            }
			}

			List<QuestionnaireQuestion> questions = getQuestionnaireQuestions(questionnaireName);
			List<QuestionnaireUserAnswer> userAnswers = QuestionnaireManagerBase.getUserAnswers(user.getId(), questions, request.getWriterId());
			//NEED TO REFACTOR QUEST.
			//if is SingleQuest and iOS version is Old remove new 3 quest. from questions and userAnswers
			if(questionnaireName.contains(QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME) 
					&& CommonUtil.isOldiOSApp(request.getAppVersion(), request.getWriterId())
					){
				//Quest check
				int i = 0;		
				List<QuestionnaireQuestion> questionsRemoved =  new ArrayList<QuestionnaireQuestion>();
				for(QuestionnaireQuestion qq : questions){
					if(qq.getId() != SINGLE_QUEST_3_1 && qq.getId() != SINGLE_QUEST_3_2 && qq.getId() != SINGLE_QUEST_3_3){
						questionsRemoved.add(qq);						
					} else{
						log.debug("Remove quest with index:" + i + " and obj:" + qq);
					}
					i++;
				}
				//UserAnswer check
				i = 0;
				List<QuestionnaireUserAnswer> userAnswersRemoved =  new ArrayList<QuestionnaireUserAnswer>();
				for(QuestionnaireUserAnswer ua : userAnswers){
					if(ua.getQuestionId() != SINGLE_QUEST_3_1 && ua.getQuestionId() != SINGLE_QUEST_3_2 && ua.getQuestionId() != SINGLE_QUEST_3_3) {
						userAnswersRemoved.add(ua);						
					} else{
						log.debug("Remove userAnswers with index:" + i + " and obj:" + ua);
					}
					i++;
				}								
				result.setQuestions(questionsRemoved);
				result.setUserAnswers(userAnswersRemoved);
			}else{
				result.setQuestions(questions);
				result.setUserAnswers(userAnswers);
			}			

			initUser(result, user, request.getUtcOffset(),request.getWriterId());
		} catch (Exception e) {
			log.error("Error in get user questionnaire [" + questionnaireName + "]", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
    	return result;
    }



    public static UserMethodResult updateUserQuestionnaireAnswers(UpdateUserQuestionnaireAnswersRequest request, HttpServletRequest httpRequest) {
    	log.debug("updateUserQuestionnaireAnswers: " + request);
    	UserQuestionnaireResult result = new UserQuestionnaireResult();
    	try {
			User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
			if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	            return result;
	        }

			Collection<QuestionnaireGroup> groups = QuestionnaireManagerBase.getAllQuestionnaires().values();
			String questionnaireName = null;
			for (QuestionnaireGroup questionnaireGroup : groups) {
				if (questionnaireGroup.getId() == request.getUserAnswers().get(0).getGroupId()) {
					questionnaireName = questionnaireGroup.getName();
				}
			}
			UserRegulationBase ur = new UserRegulationBase();
				if ((!CommonUtil.isUserRegulated(user) && user.getSkinId() != Skin.SKIN_ETRADER) 
					|| (!user.isETRegulation() && user.getSkinId() == Skin.SKIN_ETRADER)){
					if (QuestionnaireGroup.REGULATION_MANDATORY_QUESTIONNAIRE_NAME.equals(questionnaireName)
							|| QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME.equals(questionnaireName)
							|| QuestionnaireGroup.REGULATION_OPTIONAL_QUESTIONNAIRE_NAME.equals(questionnaireName)
							|| QuestionnaireGroup.REGULATION_CAPITAL_QUESTIONNAIRE_NAME.equals(questionnaireName)) {
						log.error("Requesting regulation questionnaire of a non-regulated user: " + request.getUserName());
						result.setErrorCode(ERROR_CODE_USER_NOT_REGULATED);
						return result;
				} /* else {
					OK
				} */
			} else {
				validateUserRegulationAndNonReg(user, result, AnyoptionService.VALIDATE_REGULATION_OTHER);
				if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	                return result;
	            }
				
				
				
				ur.setUserId(user.getId());
				UserRegulationManager.getUserRegulation(ur);

				
				if(!user.isETRegulation()){
					//AO Regulation
					if(!(QuestionnaireGroup.REGULATION_MANDATORY_QUESTIONNAIRE_NAME.equals(questionnaireName) 
							|| QuestionnaireGroup.REGULATION_OPTIONAL_QUESTIONNAIRE_NAME.equals(questionnaireName) 
							|| QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME.equals(questionnaireName))){
						log.error("Wrong QUESTIONNAIRE_NAME! " + questionnaireName);
						result.setErrorCode(ERROR_CODE_QUESTIONNAIRE_NOT_SUPPORTED);
						return result;
					}
					if (((QuestionnaireGroup.REGULATION_MANDATORY_QUESTIONNAIRE_NAME.equals(questionnaireName) || QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME.equals(questionnaireName))
							&& ur.getApprovedRegulationStep() >= UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE)
						|| (QuestionnaireGroup.REGULATION_OPTIONAL_QUESTIONNAIRE_NAME.equals(questionnaireName)
							&& ur.isOptionalQuestionnaireDone())) {
					log.error("Trying to update answers of questionnaire [" + questionnaireName + "] that is already submitted");
					result.setErrorCode(ERROR_CODE_QUESTIONNAIRE_ALREADY_DONE);
					return result;
					}
				} else {
					// ET Regulation
					if(!QuestionnaireGroup.REGULATION_CAPITAL_QUESTIONNAIRE_NAME.equals(questionnaireName)){
						log.error("Wrong QUESTIONNAIRE_NAME! " + questionnaireName);
						result.setErrorCode(ERROR_CODE_QUESTIONNAIRE_NOT_SUPPORTED);
						return result;
					}
					if((QuestionnaireGroup.REGULATION_CAPITAL_QUESTIONNAIRE_NAME.equals(questionnaireName)
							&& ur.getApprovedRegulationStep() >= UserRegulationBase.ET_CAPITAL_MARKET_QUESTIONNAIRE)){
						log.error("Trying to update answers of questionnaire [" + questionnaireName + "] that is already submitted");
						result.setErrorCode(ERROR_CODE_QUESTIONNAIRE_ALREADY_DONE);
					}					
				}

				// TODO Remove this if statement when question 5 QUESTION_FUNDS_ORIGIN is viewable again.
				if (QuestionnaireGroup.REGULATION_MANDATORY_QUESTIONNAIRE_NAME.equals(questionnaireName)) {
					request
						.getUserAnswers()
						.get(QUESTION_FUNDS_ORIGIN_IDX)
						.setAnswerId(
								getQuestionnaireQuestions(QuestionnaireGroup.REGULATION_MANDATORY_QUESTIONNAIRE_NAME)
									.get(QUESTION_FUNDS_ORIGIN_IDX)
									.getAnswers()
									.get(ANSWER_YES_IDX)
									.getId());
				}

				result.setUserRegulation(ur);
			}
				
			QuestionnaireGroup questionnaireCapital = null;
			if (QuestionnaireGroup.REGULATION_CAPITAL_QUESTIONNAIRE_NAME.equals(questionnaireName)) {
				questionnaireCapital = QuestionnaireManagerBase.getAllQuestionnaires().get(QuestionnaireGroup.REGULATION_CAPITAL_QUESTIONNAIRE_NAME);
			}
			
			for (QuestionnaireUserAnswer userAnswer : request.getUserAnswers()) {
				userAnswer.setWriterId(request.getWriterId());
				if (QuestionnaireGroup.REGULATION_CAPITAL_QUESTIONNAIRE_NAME.equals(questionnaireName)) {
					userAnswer.setScore(QuestionnaireManagerBase.getAnswertScore(questionnaireCapital, userAnswer));
				}
			}
			QuestionnaireManagerBase.updateUserAnswers(request.getUserAnswers(), user.isETRegulation());			

			initUser(result, user, request.getUtcOffset(), request.getWriterId());
		} catch (Exception e) {
			log.error("Error in update questionnaire answers", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
    	return result;
    }

//    public static EmailValidatorResult validateEmail(EmailValidatorRequest request) {
//    	EmailValidatorResult result = new EmailValidatorResult();
//    	result.setReturnCode(EmailValidatorUtil.validate(request.getEmail()));
//    	return result;
//    }

    public static CreditCardValidatorResponse validateCC(CreditCardValidatorRequest ccValidatorReq) {
    	CreditCardValidatorResponse result = new CreditCardValidatorResponse();
    	try {
    		/*
    		 * Next long parse is added because JSON service validation shall
    		 * match exactly the JSF validation. In newCard_content.xhtml there
    		 * is a <f:validateLongRange/> tag for credit card number.
    		 */
    		Long.parseLong(ccValidatorReq.getCcNumber());
    		int returnCode = AnyOptionCreditCardValidator.validateCCNumber(ccValidatorReq.getCcNumber(), ccValidatorReq.getSkinId());
			result.setValid(returnCode == AnyOptionCreditCardValidator.CC_VALID);
	    	result.setDiners(returnCode == AnyOptionCreditCardValidator.CC_DINERS);
			result.setAmex(returnCode == AnyOptionCreditCardValidator.CC_AMEX);
    	} catch (NumberFormatException e) {
    		log.debug("CC number might not match the long range: " + ccValidatorReq.getCcNumber(), e);
		} catch (Exception e) {
			log.debug("Cannot validate cc number: " + ccValidatorReq.getCcNumber(), e);
		}

    	return result;
    }

    /**
     * Update additional fields for user
     * Update
     * @param request
     * @return
     */
    public static UserMethodResult updateUserAdditionalFields(UpdateUserMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("updateUserAdditionalFields: " + request);
    	UserMethodResult result = new UserMethodResult();
    	try {
			User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }

    		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
    		
            // Just for eTrader idNum validation
            if (request.getUser().getSkinId() == Skin.SKIN_ETRADER) {
            	if (request.getUser().getIdNum() == null || request.getUser().getIdNum().trim().isEmpty()) {
            		log.debug("failed update userAdditionalField: user ID is empty [" + request.getUser().getIdNum() + "]");
        			result.setErrorCode(ERROR_CODE_INVALID_INPUT);
                    result.addErrorMessage("idNum", CommonUtil.getMessage(locale, "error.idnum", null));
                    return result;
            	} else {
            		String errMsg = CommonUtil.validateIdNum(request.getUser().getIdNum());
            		if (errMsg != null) {
            			log.debug("failed userAdditionalField: user ID number not valid! ," + errMsg);
            			result.setErrorCode(ERROR_CODE_INVALID_INPUT);
                        result.addErrorMessage("idNum", CommonUtil.getMessage(locale, errMsg, null));
	                    result.addUserMessage("idNum", CommonUtil.getMessage(locale, "error.register.useridnum.inuse", null));
                        return result;
            		} else if (UsersManagerBase.isUserIdNumberInUse(request.getUser().getIdNum(), request.getUser().getSkinId(), request.getUser().getId())) {
	                    log.debug("failed userAdditionalField: user ID number in use!");
	                    result.setErrorCode(ERROR_CODE_INVALID_INPUT);
	                    result.addErrorMessage("idNum", CommonUtil.getMessage(locale, "error.register.useridnum.inuse", null));
	                    result.addUserMessage("idNum", CommonUtil.getMessage(locale, "error.register.useridnum.inuse", null));
	                    return result;
	                    }
	                }
            	} else{
            		request.getUser().setIdNum(ConstantsBase.ID_NUM_DEFAULT_VALUE);
            	}
    		
    		
    		ArrayList<ErrorMessage> msgs = validateInput("updateUserAdditionalFields", request.getUser(), locale);
            if (!msgs.isEmpty()) {
                result.setErrorCode(ERROR_CODE_INVALID_INPUT);
                result.addUserMessages(msgs);
                return result;
            }
            // check for existence of currency
            SkinCurrency sc =  SkinsManagerBase.getSkinCurrenciesHM(request.getSkinId()).get(request.getUser().getCurrencyId());
    		if (null == sc) {
    			result.setErrorCode(ERROR_CODE_INVALID_INPUT);
                result.addErrorMessage("currencyId", CommonUtil.getMessage(locale, "error.mandatory", null));
                return result;
    		}
    		if(request.getUser().getBirthDayUpdate() != null && request.getUser().getBirthMonthUpdate() != null && request.getUser().getBirthYearUpdate() != null
    				&& request.getUser().getTimeBirthDate() == null) {
    		    DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
    		    Date date = df.parse(request.getUser().getBirthDayUpdate() + "-" + request.getUser().getBirthMonthUpdate() + "-" + request.getUser().getBirthYearUpdate());
    		    log.debug("updateUserAdditionalFields date by parts: " + date);
    		    request.getUser().setTimeBirthDate(date);
    		}
    		log.debug("Setting user fields: currencyId [" + request.getUser().getCurrencyId()
					+ "], street [" +  request.getUser().getStreet()
					+ "], streetNo [" +  request.getUser().getStreetNo()
					+ "], zipCode [" + request.getUser().getZipCode()
					+ "], cityName [" + request.getUser().getCityName()
					+ "], birthDate [" + request.getUser().getTimeBirthDate() 
					+ "], idNum [" +  request.getUser().getIdNum() 
					+ "], gender [" +  request.getUser().getGender() + "]");
    		// change user details.
            ArrayList<UsersDetailsHistory> usersDetailsHistoryList = null;
            try {
                HashMap<Long, String> userDetailsBeforeChangedHM = UsersManagerBase.getUserDetailsHM(request.getSkinId(), null, ConstantsBase.NON_SELECTED, user.getGender(), null, null, null, false, false, null, null, null, null, user.getIdNum(),
                        ConstantsBase.NON_SELECTED, null, null, null, null);
                HashMap<Long, String> userDetailsAfterChangedHM = UsersManagerBase.getUserDetailsHM(request.getSkinId(), request.getUser().getStreet(), ConstantsBase.NON_SELECTED, request.getUser().getGender(), null, null, null, false, false, null, null, null, request.getUser().getCityName(),
                        request.getUser().getIdNum(), ConstantsBase.NON_SELECTED, null, null, null, request.getUser().getTimeBirthDate());
                usersDetailsHistoryList = UsersManagerBase.checkUserDetailsChange(userDetailsBeforeChangedHM, userDetailsAfterChangedHM);
            } catch (Exception e) {
                log.error("problem to save user details history.", e);
            }
    		long limitId = UsersManagerBase.updateUserFields(request.getUser().getId(),
    														 request.getUser().getCurrencyId(),
    														 request.getUser().getStreet(),
    														 request.getUser().getStreetNo(),
    														 request.getUser().getZipCode(),
    														 request.getUser().getCityName(),
															 request.getSkinId(),
															 request.getUser().getTimeBirthDate(),
															 request.getUser().getIdNum(),
															 request.getUser().getGender(),
															 request.getUser().getCityId());
    		 // change user details.
             if (null != usersDetailsHistoryList) {
                 UsersManagerBase.insertUsersDetailsHistory(request.getWriterId(), user.getId(), user.getUserName(), usersDetailsHistoryList, String.valueOf(user.getClassId()));
             }

			 user.setCurrencyId(request.getUser().getCurrencyId());
			 user.setStreet(request.getUser().getStreet());
			 user.setStreetNo(request.getUser().getStreetNo());
			 user.setZipCode(request.getUser().getZipCode());
			 user.setCityName(request.getUser().getCityName());
			 user.setGender(request.getUser().getGender());
			 user.setCityId(request.getUser().getCityId());
    		//request.getUser().setLimitId(String.valueOf(limitId));
    		result.setUser(request.getUser());
    		refreshUserInSession(user.getId(), httpRequest);
    		setUserRegulationParameters(result, user);
		} catch(Exception e) {
			log.error("Error while updating additional fields for user", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
			return result;
		}
    	return result;
    }

   /**
    * Get hourly binary open markets for trading
    * Update
    * @param request
    * @return
    */
    public static MarketsMethodResult getOpenHourlyBinaryMarkets(MethodRequest request, WebLevelsCache wlc) {
    	log.info("Action: getOpenHourlyBinaryMarkets: skinId: " + request.getSkinId());
    	MarketsMethodResult result = new MarketsMethodResult();
	   	try {
	   		Hashtable<Long, Market> binaryHourlyMarkets = SkinsManagerBase.getHourlyBinaryMarketsForSkinHM(request.getSkinId());
        	ArrayList<Market> marketsClone = new ArrayList<Market>();
	   		long[] openMarkets = wlc.getOpenedMarkets();
            Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
            for (long om  : openMarkets) {
            	if (binaryHourlyMarkets.get(om) != null) {
            		Market marketClone = binaryHourlyMarkets.get(om).clone();
            		marketClone.setDisplayName(MarketsManagerBase.getMarketName(request.getSkinId(), marketClone.getId()));
            		marketClone.setGroupName(CommonUtil.getMessage(locale, marketClone.getGroupName(), null));
                	marketsClone.add(marketClone);
            	}
            }
            class AlphaComparator implements Comparator<Market> {
            	private Locale locale;
            	public AlphaComparator(Locale locale) {
            		this.locale = locale;
            	}
                @Override
				public int compare(Market a, Market b) {
                    String aLabel = a.getDisplayName();
                    String bLabel = b.getDisplayName();
                    int returnVal = 0;
                    try {
                        Collator collator = Collator.getInstance(locale);
                        returnVal = collator.compare(aLabel, bLabel);
                    } catch (Exception e) {
                    }
        			return returnVal;
                }
            }
           Collections.sort(marketsClone, new AlphaComparator(locale));
	       result.setMarkets(marketsClone.toArray(new Market[marketsClone.size()]));
        } catch (Exception e) {
              log.error("Error in getOpenHourlyBinaryMarkets.", e);
              result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
	   	return result;
   }

    /**
     * Get all binary markets for trading
     * @param request
     * @return
     */
//     public static MarketsMethodResult getAllBinaryMarkets(MethodRequest request) {
//     	log.info("Action: getAllBinaryMarkets: skinId: " + request.getSkinId());
//     	MarketsMethodResult result = new MarketsMethodResult();
// 	   	try {
// 	   		Hashtable<Long, Market> binaryHourlyMarkets = SkinsManagerBase.getAllBinaryMarketsForSkinHM(request.getSkinId());
//         	ArrayList<Market> marketsClone = new ArrayList<Market>();
//            Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
//            for (Iterator<Long> iter = binaryHourlyMarkets.keySet().iterator(); iter.hasNext();) {
//            	 	Long marketId = iter.next();
//             		Market marketClone = binaryHourlyMarkets.get(marketId).clone();
//             		marketClone.setDisplayName(CommonUtil.getMessage(locale, marketClone.getDisplayName(), null));
//             		marketClone.setGroupName(CommonUtil.getMessage(locale, marketClone.getGroupName(), null));
//                 	marketsClone.add(marketClone);
//            }
//	         class AlphaComparator implements Comparator<Market> {
//	         	private Locale locale;
//	         	public AlphaComparator(Locale locale) {
//	         		this.locale = locale;
//	         	}
//	             @Override
//				public int compare(Market a, Market b) {
//	                 String aLabel = a.getDisplayName();
//	                 String bLabel = b.getDisplayName();
//	                 int returnVal = 0;
//	                 try {
//	                     Collator collator = Collator.getInstance(locale);
//	                     returnVal = collator.compare(aLabel, bLabel);
//	                 } catch (Exception e) {
//	                 }
//	     			return returnVal;
//	             }
//	        }
//            Collections.sort(marketsClone, new AlphaComparator(locale));
// 	        result.setMarkets(marketsClone.toArray(new Market[marketsClone.size()]));
//         } catch (Exception e) {
//        	 log.error("Error in getAllBinaryMarkets.", e);
//             result.setErrorCode(ERROR_CODE_UNKNOWN);
//         }
// 	   	return result;
//    }


    /**
     *
     * @param request
     * @return
     */
    public static OpportunityMethodResult getCurrentHourlyOppByMarket(ChartDataMethodRequest request) {
    	log.info("Action: getCurrentHourlyOppByMarket: skinId: " + request.getSkinId() + " marketId:" + request.getMarketId());
    	OpportunityMethodResult result = new OpportunityMethodResult();
    	try {
    		Opportunity opp = OpportunitiesManagerBase.getCurrentHourlyOppByMarket(request.getMarketId() , request.getOpportunityTypeId());
    		log.info("Found hourly opportunity id:" + opp + " for market");
    		result.setOpportunity(opp);
    	} catch (Exception e) {
            log.error("Error in getCurrentHourlyOppByMarket", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
  	   	return result;
    }

    /**
     * Insert login token for user (API)
     * Update
     * @param request
     * @return
     */
    public static UserMethodResult insertLoginToken(UserMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("insertLoginToken: " + request);
    	UserMethodResult result = new UserMethodResult();
    	try {
    		result = getUser(request, httpRequest);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
            String uuid = ApiManager.insertLoginToken(result.getUser().getId());
            log.info("Insert UUID:" + uuid + " for userId:" + result.getUser().getId());
            result.getUser().setLoginToken(uuid);
    	} catch (Exception e) {
            log.error("Error in insertLoginToken", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
    	}
      	return result;
    }

    /**
     * Get current level by opportunityId
     * Update
     * @param request
     * @return
     */
    public static OpportunityMethodResult getCurrentLevelByOpportunityId(ChartDataMethodRequest request) {
    	log.info("Action: getCurrentLevelByOpportunityId for oppId:" + request.getOpportunityId());
    	OpportunityMethodResult result = new OpportunityMethodResult();
    	try {
    		Opportunity opportunity = OpportunitiesManagerBase.getRunningById(request.getOpportunityId());
    		if (opportunity != null && opportunity.getMarketId() != 0 ) { //In case we find oppId
	            WebLevelLookupBean wllb = new WebLevelLookupBean();
	            wllb.setSkinGroup(SkinsManagerBase.getSkin(request.getSkinId()).getSkinGroup());
	            wllb.setOppId(opportunity.getId());
	            wllb.setFeedName(opportunity.getMarket().getFeedName());
	            AnyoptionServiceServlet.getLevelsCache().getLevels(wllb);
	            double pageLevel = new BigDecimal(wllb.getDevCheckLevel()).setScale(Integer.parseInt(String.valueOf(opportunity.getMarket().getDecimalPoint())), RoundingMode.HALF_UP).doubleValue();
            	result.setOpportunity(opportunity);
            	result.getOpportunity().setCurrentLevel(pageLevel);
    		} else {
    			log.info("Opportunity does not exists:" + opportunity);
    			result.setErrorCode(ERROR_CODE_OPP_NOT_EXISTS);
    			result.addUserMessage("opportunityId", "Can't find opportunity", ApiErrorCode.API_ERROR_CODE_VALIDATION_CANT_FIND_OPPORTUNITY);
    		}
    	} catch (Exception e) {
            log.error("Error in getCurrentLevelByOpportunityId", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
    	}
      	return result;
    }

    /**
     * Checks if the user with the given ID has ever done a deposit attempt.
     * It does not matter whether the attempt is successful or not.
     *
     * @param request
     * @return a response containing flag whether the user does not have attempts <code>true</code> or does have <code>false</code>.
     */
    public static FirstEverDepositCheckResult getFirstEverDepositCheck(FirstEverDepositCheckRequest request) {
    	FirstEverDepositCheckResult result = new FirstEverDepositCheckResult();
    	log.debug("Checking first ever deposit for user [" + request.getUserId() + "]");
    	try {
			result.setFirstEverDeposit(TransactionsManagerBase.isFirstPossibleDepositExcludeBonus(request.getUserId()));
		} catch (SQLException e) {
			log.error("Unable to check for first ever deposit", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
		}

    	return result;
    }

    /**
     * @param request - Method Request . Containing the skin_id.
     * @return the OptionsMethodResult with a list of bank wire withdraw available for the skin in the request.
     */
    public static OptionsMethodResult getBankWireWithdrawOptions(MethodRequest request) {
    	OptionsMethodResult result = new OptionsMethodResult();
    	try {
    		ArrayList<BankBase> bankWireList = SkinsManagerBase.getBankWireWithdraw(request.getSkinId());
    		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
    		ArrayList<OptionItem> options = new ArrayList<OptionItem>();
    		for (BankBase b : bankWireList) {
    			options.add(new OptionItem(b.getId(), CommonUtil.getMessage(locale, b.getName(), null), locale));
    		}
    		result.setOptions(options.toArray(new OptionItem[options.size()]));
    	} catch (Exception e) {
            log.error("Error in getBankWireWithdrawOptions", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
    }


    /**
     * Get all odds pair for selector
     * @param request
     * @return
     */
    public static OpportunityOddsPairResult getAllReturnOdds(MethodRequest request) {
    	log.info("Action: getAllReturnOdds");
    	OpportunityOddsPairResult result = new OpportunityOddsPairResult();
    	try {
    		ArrayList<OpportunityOddsPair> list = OpportunitiesManagerBase.getOpportunityOddsPairSelector();
    		result.setOpportunityOddsPair(list.toArray(new OpportunityOddsPair[list.size()]));
		} catch (Exception e) {
			log.error("Unable to get getAllReturnOdds", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
    	return result;
    }

    /**
     * Get home page markets
     * @param request
     * @param wlc
     * @return
     */
    public static MarketsMethodResult getBinaryHomePageMarkets(MethodRequest request, WebLevelsCache wlc) {
    	log.info("Action: getBinaryHomePageMarkets for skinId: " + request.getSkinId());
    	MarketsMethodResult result = new MarketsMethodResult();
    	try {
	    	Hashtable<Long, Market> binaryHourlyMarkets = SkinsManagerBase.getAllBinaryMarketsForSkinHM(request.getSkinId());
	   		long[] hpMarkets = wlc.getMarketsOnHomePage(request.getSkinId());
	   		ArrayList<Market> marketsClone = new ArrayList<Market>();
	        Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
	        for (long om  : hpMarkets) {
	    		Market marketClone = binaryHourlyMarkets.get(om).clone();
	    		marketClone.setDisplayName(MarketsManagerBase.getMarketName(request.getSkinId(), marketClone.getId()));
	    		marketClone.setGroupName(CommonUtil.getMessage(locale, marketClone.getGroupName(), null));
	        	marketsClone.add(marketClone);
	        }
	       result.setMarkets(marketsClone.toArray(new Market[marketsClone.size()]));
	    } catch (Exception e) {
	          log.error("Error in getBinaryHomePageMarkets.", e);
	          result.setErrorCode(ERROR_CODE_UNKNOWN);
	    }
    	return result;
    }

    public static MethodResult deleteCreditCard(CreditCardRequest creditCardrequest, HttpServletRequest httpRequest){
    	log.debug("deleteCreditCard: " + creditCardrequest);
    	MethodResult result = new MethodResult();
    	User user;
		try {

    		user = validateAndLoginEncrypt(creditCardrequest.getUserName(), creditCardrequest.getPassword(), result, creditCardrequest.getSkinId(), false, httpRequest);
    		if (!CreditCardsManagerBase.checkIfCardIsValid(creditCardrequest.getCardId(), user.getId())){
    				result.setErrorCode(ERROR_CODE_UNKNOWN);
    				log.error("Wrong cradit card number.");
    				return result;

			}
			CreditCardsManagerBase.deleteCreditCard(creditCardrequest.getCardId());
		} catch (Exception e) {
			result.setErrorCode(ERROR_CODE_UNKNOWN);
			log.error("", e);

		}
    	return result;

    }

    public static MethodResult sendBankTransferDetailsUser(UserMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("sendBankTransferDetailsUser: " + request);
    	MethodResult result = new MethodResult();
    	try {
    		User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
    		if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }

			UsersManagerBase.sendMailTemplateFile(ConstantsBase.TEMPLATE_BANK_TRANSFER_DETAIL, request.getWriterId(), user);
    	} catch (Exception e) {
            log.error("can't send deposit receipt mail!!", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
    	return result;
    }



    /**
     * Get last currencies rate
     * @param request
     */
    public static LastCurrenciesRateMethodResult getLastCurrenciesRate(MethodRequest request) {
    	log.info("Action: getCurrenciesRates");
    	LastCurrenciesRateMethodResult result = new LastCurrenciesRateMethodResult();
    	try {
    		ArrayList<LastCurrencyRate> lastCurrencyRateList = CurrencyRatesManagerBase.getLastCurrenciesRate();
    		result.setLastCurrenciesRate(lastCurrencyRateList.toArray(new LastCurrencyRate[lastCurrencyRateList.size()]));
	    } catch (Exception e) {
	          log.error("Error in getCurrenciesRates.", e);
	          result.setErrorCode(ERROR_CODE_UNKNOWN);
	    }
    	return result;
    }

	/**
	 * Takes the default currency for skin defined by the skin id in the given MethodRequest and returns the logout balance step information
	 * for that currency
	 *
	 * @param request base method request
	 * @return the logout default step
	 */
	public static LogoutBalanceStepMethodResult getLogoutBalanceStep(MethodRequest request) {
		LogoutBalanceStepMethodResult result = new LogoutBalanceStepMethodResult();
		try {
			BalanceStep bs = UserBalanceStepManager.getLogoutCurrencyDefaultBalanceStep()
													.get(new Long(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultCurrencyId()));
			result.setDefaultAmountValue(bs.getDefaultAmountValue());
			result.setPredefValues(bs.getPredefValues());
			result.setCurrencyId(bs.getCurrencyId());
		} catch (SQLException e) {
			log.error("Can't get logout balance step. Returning empty values", e);
			result.setDefaultAmountValue(0l);
			result.setPredefValues(new ArrayList<BalanceStepPredefValue>());
			result.setCurrencyId(0l);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		} catch (Exception e) {
			log.error("Unexpected exception in getLogoutBalanceStep. Returning empty values", e);
			result.setDefaultAmountValue(0l);
			result.setPredefValues(new ArrayList<BalanceStepPredefValue>());
			result.setCurrencyId(0l);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
		return result;
	}

    /**
     * Get first time deposit users by API user id
     * @param request
     */
    public static FTDUsersMethodResult getFTDUsersByDate(FTDUsersRequest request) {
    	FTDUsersMethodResult result = new FTDUsersMethodResult();
    	try {
        	log.info("Action: getFTDUsersByDate for API user:" + request.getApiUserId() + " date:" + request.getDateRequest());
    		ArrayList<FTDUser> list = ApiManager.getFTDUsersByDate(request.getApiUserId(), request.getDateRequest());
    		result.setFTDUsersList(list.toArray(new FTDUser[list.size()]));
	    } catch (Exception e) {
	          log.error("Error in getFTDUsersByDate.", e);
	          result.setErrorCode(ERROR_CODE_UNKNOWN);
	    }
    	return result;
    }

    /**
     * Reload User object in Session
     * @param userId
     * @param httpRequest
     */

    public static void refreshUserInSession(Long userId, HttpServletRequest httpRequest) {
    	User user;
    	try {
    		if (userId == 0){
    			 user = (User) httpRequest.getSession().getAttribute(ConstantsBase.BIND_USER);
    			 userId = user.getId();
    		}
			user = UsersManagerBase.getById(userId);
			if (user.getCurrencyId() != null &&  user.getCurrencyId() > 0) {
				user.setCurrency(CurrenciesManagerBase.getCurrency(user.getCurrencyId()));
            }
			User sessionUser = (User) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_USER);
			if (sessionUser != null) {
				user.setPredefValues(sessionUser.getPredefValues());
				user.setDefaultAmountValue(sessionUser.getDefaultAmountValue());
				user.setCityFromGoogle(sessionUser.getCityFromGoogle());
				user.setCityLatitude(sessionUser.getCityLatitude());
				user.setCityLongitude(sessionUser.getCityLongitude());
			} else {
				log.debug("No user in session, predefindet values and default amount value will not be saved in session");
			}
			httpRequest.getSession().setAttribute(ConstantsBase.BIND_SESSION_USER, user);
	    	log.debug("The user:" + userId + " was reload in Session");
		} catch (Exception e) {
			log.error("Can't reload user in Session", e);
		}

	}
    
	public static InvestmentMethodResult sellAllDynamicsInvestments(SellDynamicsInvestmentRequest request, OpportunityCache oc, HttpServletRequest httpRequest) {
    	User user;
    	InvestmentMethodResult result = new InvestmentMethodResult();
    	try {
			user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
	        if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	            return result;
	        }
    	} catch (Exception e) {
    		log.debug("Unable to validate user", e);
    		result.setErrorCode(ERROR_CODE_UNKNOWN);
    		return result;
    	}
    	OpportunityCacheBean opp;
		try {
			opp = oc.getOpportunity(request.getOpportunityId());
		} catch (SQLException e) {
			log.error("Cannot get opportunity:"+ request.getOpportunityId(), e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
			return result;
		}
		long loginId = 0L;
		try {
			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
			}
		} catch (Exception e) {
			log.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		// Deviation check for close
		WebLevelLookupBean wllb = new WebLevelLookupBean();
	    wllb.setSkinGroup(SkinsManagerBase.getSkin(user.getSkinId()).getSkinGroup());	            
        Market market = MarketsManagerBase.getMarket(opp.getMarketId());
        wllb.setOppId(opp.getId());
        wllb.setFeedName(market.getFeedName());
        AnyoptionServiceServlet.getLevelsCache().getLevels(wllb);

        MessageToFormat m1 = InvestmentValidatorDynamics.deviationCheck(wllb, opp, Investment.INVESTMENT_TYPE_DYNAMICS_SELL, request.getBid(), user.getUserName(), httpRequest.getSession().getId(), null);
        MessageToFormat m2 = InvestmentValidatorDynamics.deviationCheck(wllb, opp, Investment.INVESTMENT_TYPE_DYNAMICS_BUY,  request.getOffer(), user.getUserName(), httpRequest.getSession().getId(), null);
        
		if(m1 != null || m2 != null) {
			result.setErrorCode(ERROR_CODE_INV_VALIDATION);
			return result;
		}
		
		ArrayList<Long> list = InvestmentsManagerBase.getUserOpenedInvestments(user.getId(), opp.getId(), 0);
		for(int i =0;i<list.size();i++) {
			Long investmentId = list.get(i);
			if(InvestmentsManagerBase.sellDynamicsInvestment(investmentId, 0l, request.getOffer(), request.getBid(), Writer.WRITER_ID_WEB, loginId, user, wllb.getRealLevel(), AnyoptionServiceServlet.getLevelsCache())){
				log.debug("Successfuly closed: "+ investmentId);
			} else { 
				log.debug("can not close" + investmentId);
			}
        }
        
	    return result;
	}
    
    
	public static InvestmentMethodResult sellDynamicsInvestment(SellDynamicsInvestmentRequest request, OpportunityCache oc, HttpServletRequest httpRequest) {
    	User user;
    	InvestmentMethodResult result = new InvestmentMethodResult();
    	try {
			user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
	        if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	            return result;
	        }
    	} catch (Exception e) {
    		log.debug("Unable to validate user", e);
    		result.setErrorCode(ERROR_CODE_UNKNOWN);
    		return result;
    	}
    	OpportunityCacheBean opp;
		try {
			opp = oc.getOpportunity(request.getOpportunityId());
		} catch (SQLException e) {
			log.error("Cannot get opportunity:"+ request.getOpportunityId(), e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
			return result;
		}
		long loginId = 0L;
		try {
			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
			}
		} catch (Exception e) {
			log.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		// Deviation check for close
		WebLevelLookupBean wllb = new WebLevelLookupBean();
	    wllb.setSkinGroup(SkinsManagerBase.getSkin(user.getSkinId()).getSkinGroup());	            

        Market market = MarketsManagerBase.getMarket(opp.getMarketId());
        wllb.setOppId(opp.getId());
        wllb.setFeedName(market.getFeedName());
        AnyoptionServiceServlet.getLevelsCache().getLevels(wllb);
        
        double quote = request.getInvestmentTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_SELL ? request.getBid() : request.getOffer();
        MessageToFormat m = InvestmentValidatorDynamics.deviationCheck(wllb, opp, request.getInvestmentTypeId(), quote, user.getUserName(), httpRequest.getSession().getId(), null);
		
		if(m != null) {
			result.setErrorCode(ERROR_CODE_INV_VALIDATION);
			return result;
		}
		// close for
        if(InvestmentsManagerBase.sellDynamicsInvestment(request.getInvestmentId(), (long)(request.getSellAmount() / 100 ), request.getOffer(), request.getBid(), request.getWriterId(), loginId, user, wllb.getRealLevel(), AnyoptionServiceServlet.getLevelsCache())){
        	Investment inv = new Investment();
        	inv.setId(request.getInvestmentId());
        	result.setInvestment(inv);
        } else {
        	result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        
	    return result;
	}

    public static InatecDepositMethodResult insertDirectDeposit(InatecDepositMethodRequest request, HttpServletRequest httpRequest) {
   	log.debug("insertDirectDeposit: " + request);
    	InatecDepositMethodResult result = new InatecDepositMethodResult();
    	User user;
    	try {
			user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
	        if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	            return result;
	        }
    	} catch (Exception e) {
    		log.debug("Unable to validate user", e);
    		result.setErrorCode(ERROR_CODE_UNKNOWN);
    		return result;
    	}
    	if(user.getCurrencyId() !=  ConstantsBase.CURRENCY_EUR_ID) {
    		log.error("UNSUPPORTED CURRENCY FOR DIRECT DEPOSIT:"+user.getCurrencyId() );
    		result.setErrorCode(ERROR_CODE_UNKNOWN);
    		return result;
    	}
    	//Validate Regulations
    	log.debug("Valiudate Regulation InatecDepositMethodResult");
    	validateUserRegulationAndNonReg(user, result, AnyoptionService.VALIDATE_REGULATION_DEPOSIT);
    	if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
    		return result;
    	}
    	
    	// check if exists
    	Long inatecTransactionId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.INATEC_TRANSACTION_ID_SESSION_PARAM);
    	if (inatecTransactionId != null) {
    		log.info("Found Inatec transaction id in session, transactionId: " + inatecTransactionId);
    	}

    	String homePageUrl = CommonUtil.getConfig("homepage.url");
    	HashMap<String, MessageToFormat> mtf = new HashMap<String, MessageToFormat>();
    	Transaction t;
    	try {
    		long loginId = 0L;
    		try {
    			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
    				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
    			}
    		} catch (Exception e) {
    			log.error("Cannot get login ID from session!", e);
    			loginId = 0L;
    		}
    		// OLD API
			//t = TransactionsManagerBase.insertDirectDeposit(mtf, request.getIp(), request.getDepositAmount(), request.getWriterId(), user, request.getBankSortCode(), request.getAccountNum(), request.getPaymentTypeParam(), request.getBeneficiaryName(), homePageUrl, loginId);
    		t = TransactionsManagerBase.insertPowerpayDeposit(mtf, homePageUrl, request.getIp(),  request.getDepositAmount(), request.getWriterId(), user, request.getBankSortCode(), request.getAccountNum(), request.getPaymentTypeParam(), request.getBeneficiaryName(), homePageUrl, loginId);
		} catch (SQLException e) {
			log.debug("Unable to insert direct deposit successfully", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
			return result;
		}
    	if (t != null) {
    		if (t.getStatusId() == TransactionsManagerBase.TRANS_STATUS_AUTHENTICATE) {
    			result.setTransactionId(t.getId());
    			result.setRedirectUrl(t.getRedirectUrl());
    			httpRequest.getSession().setAttribute(ConstantsBase.INATEC_TRANSACTION_ID_SESSION_PARAM, t.getId());
    		} else {
    			result.setTransactionId(t.getId());
    			result.setSuccess(true);
    		}
    	} else {
    		log.debug("Transaction failed");
    		result.setErrorCode(ERROR_CODE_TRANSACTION_FAILED);
    		 for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
                 String key = i.next();
                 MessageToFormat m = mtf.get(key);
                 result.addUserMessage(key, CommonUtil.getMessage(user.getLocale(), m.getKey(), m.getParams()));
             }
    	}
    	return result;
    }
    
    public static NetellerDepositMethodResult insertNetellerDeposit(NetellerDepositMethodRequest request, HttpServletRequest httpRequest) {
       	log.debug("insertNetellerDeposit: " + request);
       	NetellerDepositMethodResult result = new NetellerDepositMethodResult();
        	User user;
        	try {
    			user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
    	        if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
    	            return result;
    	        }
        	} catch (Exception e) {
        		log.debug("Unable to validate user", e);
        		result.setErrorCode(ERROR_CODE_UNKNOWN);
        		return result;
        	}

        	if(CommonUtil.isParameterEmptyOrNull(request.getEmail()) 
        			||  CommonUtil.isParameterEmptyOrNull(request.getVerificationCode())
        					|| CommonUtil.isParameterEmptyOrNull(request.getAmount())) {
        		log.error("Empty neteller deposit params");
        		result.setErrorCode(ERROR_CODE_MISSING_PARAM);
        		return result;
        	}
        	
        	//Validate Regulations
        	log.debug("Validate Regulation Neteller");
        	validateUserRegulationAndNonReg(user, result, AnyoptionService.VALIDATE_REGULATION_DEPOSIT);
        	if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
        		return result;
        	}

        	HashMap<String, MessageToFormat> mtf = new HashMap<String, MessageToFormat>();
        	Transaction t;
        	try {
        		long loginId = 0L;
        		try {
        			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
        				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
        			}
        		} catch (Exception e) {
        			log.error("Cannot get login ID from session!", e);
        			loginId = 0L;
        		}
        		t = TransactionsManagerBase.insertNetellerDeposit(mtf, request.getIp(),  request.getAmount(), request.getWriterId(), user, request.getEmail(), request.getVerificationCode(), loginId);
    		} catch (SQLException e) {
    			log.debug("Unable to insert direct deposit successfully", e);
    			result.setErrorCode(ERROR_CODE_UNKNOWN);
    			return result;
    		}
        	if (t != null) {
        			result.setTransactionId(t.getId());
        			result.setSuccess(true);
        	} else {
        		log.debug("Transaction failed");
        		result.setErrorCode(ERROR_CODE_TRANSACTION_FAILED);
        		 for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
                     String key = i.next();
                     MessageToFormat m = mtf.get(key);
                     result.addUserMessage(key, CommonUtil.getMessage(user.getLocale(), m.getKey(), m.getParams()));
                 }
        	}
        	return result;
        }

    public static TransactionMethodResult finishDirectDeposit(InatecDepositFinishMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("finishDirectDeposit: " + request);
    	TransactionMethodResult result = new TransactionMethodResult();
    	User user;
    	try {
			user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
	        if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	            return result;
	        }
    	} catch (Exception e) {
    		log.debug("Unable to validate user", e);
    		result.setErrorCode(ERROR_CODE_UNKNOWN);
    		return result;
    	}
    	Long transactionId = null;
    	try {

    		transactionId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.INATEC_TRANSACTION_ID_SESSION_PARAM);
    		if (transactionId == null) {
    			log.error("No inatec transaction id found in session");
    			result.setErrorCode(ERROR_CODE_UNKNOWN);
    			return result;
    		}
    		// remove immediately because this method cannot be called twice
    		httpRequest.getSession().removeAttribute(ConstantsBase.INATEC_TRANSACTION_ID_SESSION_PARAM);    		

			Transaction t = TransactionsManagerBase.getById(transactionId);
			long startTime = System.nanoTime();
			while(t.getStatusId() == TransactionsManagerBase.TRANS_STATUS_AUTHENTICATE) {
				t = TransactionsManagerBase.getById(transactionId);
				if(TimeUnit.SECONDS.convert((System.nanoTime() - startTime), TimeUnit.NANOSECONDS)> 120) {
					break;
				}
			}
		
			if (t.getStatusId() == TransactionsManagerBase.TRANS_STATUS_AUTHENTICATE) {
				result.setTransactionId(t.getId());
			} else if (t.getStatusId() == TransactionsManagerBase.TRANS_STATUS_SUCCEED) {
				// the user is reloaded from db
				refreshUserInSession(user.getId(), httpRequest);
				user = (User) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_USER);
				result.setSuccess(true);
				result.setTransactionId(t.getId());
                result.setBalance(user.getBalance() / 100.0);
                result.setAmount(t.getAmount() / 100d);
                result.setDollarAmount(CommonUtil.convertToBaseAmount(t.getAmount(), user.getCurrencyId(), new Date())/100);
                boolean isFirstSuccessDeposit = TransactionsManagerBase.countRealDeposit(user.getId()) == 1 ? true : false ;
                result.setFirstDeposit(isFirstSuccessDeposit);
                result.setClearingProvider(t.getClearingProviderDescriptor());
			} else if (t.getStatusId() == TransactionsManagerBase.TRANS_STATUS_IN_PROGRESS) {
				result.setTransactionId(t.getId());
			} else {
				result.setErrorCode(ERROR_CODE_TRANSACTION_FAILED);
			}
		} catch (Exception e) {
			log.error("Failed to process Inatec bank response, transactionId: " + transactionId, e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
			return result;
		}
    	return result;
    }

    /**
     * This method should receive the dp, combinationid, pixeltype, skinId and return the pixel string.
     * If you have both, aff and combid pixel , concat them .
     *
     * This Method should NOT validate:
     * Validate IsLive
     * Validate firing pixel more than once.
     * Validate first deposit.
     *
     * This method should NOT authenticate user credentials.
     *
     * @param request
     * @param httpRequest
     * @return PixelMethodResult - contain httpCode & httpsCode - will contain data according to pixelType.
     */
    public static PixelMethodResult getTrackingPixel(PixelMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("Start - getTrackingPixel with: " + request.toString());
    	PixelMethodResult result = new PixelMethodResult();
    	long affiliateKey = 0;
		try {
        	StringBuilder httpCode = new StringBuilder();
        	StringBuilder httpsCode = new StringBuilder();
        	StringBuilder pixelHttp = new StringBuilder();
        	StringBuilder pixelHttps = new StringBuilder();
        	String dynamicParam = request.getDynamicParam();
        	long combinationId = request.getCombinationId();
        	long tranId = request.getTransactionId();
        	long userId = request.getUserId();
        	User user = (User) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_USER);
        	if (null == user) {
        		user = UsersManagerBase.getById(userId);
        	}
        	if (null != user && user.getId() > 0) {
        		log.debug("found user getting comb and dp from user");
        		dynamicParam = user.getDynamicParam();
        		combinationId = user.getCombinationId();
        	}
        	affiliateKey = CommonUtil.getAffIdFromDynamicParameter(dynamicParam, combinationId , request.getSkinId());
        	if (combinationId > 0) {
        		MarketingCombinationPixels combinationPixels = UsersManagerBase.getPixelsByCombId(combinationId);
	        	for (MarketingPixel pixel : combinationPixels.getPixels()) {
	        		appendPixelCode(pixel, httpCode, httpsCode, request);
				}
        	}

        	if (affiliateKey > 0) { 
        		MarketingAffiliate ma = MarketingAffiliatesManagerBase.getAffiliateByKey(affiliateKey);
        		if (ma != null && ma.getId() > 0) {
	        		MarketingAffiliatePixels affiliatePixels = UsersManagerBase.getPixelsByAffiliateId(ma.getId());
		        	for (MarketingPixel pixel : affiliatePixels.getPixels()) {
		        		appendPixelCode(pixel, httpCode, httpsCode, request);
					}
        		}
        	}
        	Transaction transaction = null;
        	if (tranId > 0) {
        		transaction = TransactionsManagerBase.getById(tranId);
        	}
        	pixelHttp.append(Pixel.replaceParametersPixel(httpCode.toString(), user, transaction));
        	pixelHttps.append(Pixel.replaceParametersPixel(httpsCode.toString(), user, transaction));
        	result.setPixelHttpCode(pixelHttp);
        	result.setPixelHttpsCode(pixelHttps);
		} catch (Exception e) {
			log.error("ERROR! in getTrackingPixel. ", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
    	log.debug("End - getTrackingPixel. with: " + result.toString());
    	return result;
    }

	/**
	 * @param pixel
	 * @param httpCode
	 * @param httpsCode
	 * @param request
	 */
	private static void appendPixelCode(MarketingPixel pixel, StringBuilder httpCode, StringBuilder httpsCode, PixelMethodRequest request) {
		if (pixel.getTypeId() == request.getPixelType()) {
			if (!CommonUtil.IsParameterEmptyOrNull(pixel.getHttpCode())) {
				httpCode.append(pixel.getHttpCode());
			}

			if (!CommonUtil.IsParameterEmptyOrNull(pixel.getHttpsCode())) {
				httpsCode.append(pixel.getHttpsCode());
			}
		}
	}
	
    public static UserMethodResult insertKnowledgeConfirm(UserRegulationRequest request, HttpServletRequest httpRequest) {
    	log.debug("insertKnowledgeConfirm: " + request);
    	UserMethodResult result = new UserMethodResult();
        try {
            User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
            
            if(!user.isETRegulation()){
            	result.setErrorCode(ERROR_CODE_UNKNOWN);
            	log.debug("The userId:" + user.getId() + " is not ET Regulation!");
            	return result;
            }
            
			UserRegulationBase ur = new UserRegulationBase();
			ur.setUserId(user.getId());
			UserRegulationManager.getUserRegulation(ur);
            
            if(!user.isAcceptedTerms()){
            	result.setErrorCode(ERROR_CODE_NOT_ACCEPTED_TERMS);
            	log.debug("Can't Knowledge Confirm  user is not Accept Tersm");
            	return result;
            }
            
            if(!(ur.getApprovedRegulationStep() == UserRegulationBase.REGULATION_FIRST_DEPOSIT 
            		||ur.getApprovedRegulationStep() == UserRegulationBase.ET_REGULATION_KNOWLEDGE_QUESTION_USER)){
            	result.setErrorCode(ERROR_CODE_NOT_ACCEPTED_TERMS);
            	log.debug("Can't Knowledge Confirm  user is not FD done or KNOWLEDGE QUESTION step:" + ur);
            	return result;
            }
            
            int answerId = -1;
            if(request.getUserRegulation().isKnowledgeQuestion()){
            	answerId = ConstantsBase.BO_KNOWLEDGE_QUESTION_ANSWER_IS_YES;
            } else {
            	answerId = ConstantsBase.BO_KNOWLEDGE_QUESTION_ANSWER_IS_NO;
            }            
            com.anyoption.common.managers.IssuesManagerBase.insertIssueUserRegulationKnowledgeQuestion(user.getId(), answerId);
            initUser(result, user, request.getUtcOffset(),request.getWriterId());
        } catch (Exception e) {
            log.error("Error in insertKnowledgeConfirm.", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }
    
    public static UserMethodResult unsuspendUserRequest(UserMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("unsuspendRequest: " + request);
    	UserMethodResult result = new UserMethodResult();
        try {
            User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
            
            if(!user.isETRegulation()){
            	result.setErrorCode(ERROR_CODE_UNKNOWN);
            	log.debug("The userId:" + user.getId() + " is not ET Regulation!");
            	return result;
            }
            
			UserRegulationBase ur = new UserRegulationBase();
			ur.setUserId(user.getId());
			UserRegulationManager.getUserRegulation(ur);
			if(ur.isSuspended()){
				if(UsersManagerBase.sendActivationMailForSuspend(user.getId(), true, user.getSkinId())){
					initUser(result, user, request.getUtcOffset(),request.getWriterId());
				} else {
					log.error("Can't send Activation Mail For Suspend userId:" + user.getId());
				}
			} else {
				result.setErrorCode(ERROR_CODE_UNKNOWN);
				log.error("User is not suspended " + ur);
			}
        } catch (Exception e) {
            log.error("Error in unsuspendRequest.", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }
    
    /*    public static UserMethodResult unrestrictUserRequest(UserMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("unrestrictRequest: " + request);
    	UserMethodResult result = new UserMethodResult();
        try {
            User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
            
            if(user.isETRegulation()){
            	result.setErrorCode(ERROR_CODE_UNKNOWN);
            	log.debug("The userId:" + user.getId() + " is not AO Regulation!");
            	return result;
            }
            
			UserRegulationBase ur = new UserRegulationBase();
			ur.setUserId(user.getId());
			UserRegulationManager.getUserRegulation(ur);
			
			if(UsersManagerBase.sendActivationMailForAORegulation(ur)){
				initUser(result, user, request.getUtcOffset(),request.getWriterId());
	
			} else {
				result.setErrorCode(ERROR_CODE_UNKNOWN);
				log.error("User is not restricted " + ur);
			}
        } catch (Exception e) {
            log.error("Error in unrestrictUserRequest", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }*/
    
    private static boolean isOldNonETErgualtioniOSApp(String appVersion) {
    	final int LAST_IOS_NON_ET_REGUALTION_VER = 26;
    	//Check for  ver before  12.10.26 
    	log.debug("BEGIN iOS app Ver. CHECK:" + appVersion);
    	try {
			String appNumber [] = appVersion.split("\\.");
			if(appNumber[0].contains("12") && appNumber[1].contains("10")){
				int ver = new Integer(appNumber[2]);
				if((ver%2)== 0 && ver <= LAST_IOS_NON_ET_REGUALTION_VER){
					log.debug("The iOS app is OLD Ver. Need Update:" + appVersion);
					return true;
				}
			}	
		} catch (Exception e) {
			log.error("Can't get the app version:" + appVersion);
		}
    	log.debug("APP ver is OK");
    	return false;
    }
    
    
    public static InvestmentMethodResult insertDynamicsInvestment(InsertInvestmentMethodRequest request, OpportunityCache oc, HttpServletRequest httpRequest) {
    	log.debug("insertDynamicsInvestment: " + request);
    	InvestmentMethodResult result = new InvestmentMethodResult();
    	result.setCancelSeconds(Integer.parseInt(Utils.getProperty("cancel.seconds.client")));
    	try {
    		User user = validateAndLoginEncryptGetCity(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, true, httpRequest);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }
            validateUserRegulationAndNonReg(user, result, AnyoptionService.VALIDATE_REGULATION_INVESTMENT);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
            	return result;
            }           
            long loginId = 0L;
    		try {
    			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
    				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
    			}
    		} catch (Exception e) {
    			log.error("Cannot get login ID from session!", e);
    			loginId = 0L;
    		}
            if(!user.getProductViewFlags().get(ConstantsBase.HAS_DYNAMICS_FLAG)) {
    	    	log.error("user :" + user.getId() + " is not dynamics enabled !");
    	    	result.setErrorCode(ERROR_CODE_UNKNOWN);
    	        return result;
    	    }
            // To set the user default amount value, because we get the user from db
            user.setDefaultAmountValue(request.getDefaultAmountValue());

           	result = CommonJSONService.insertInvestmentDynamics(request, oc, user, AnyoptionServiceServlet.getLevelsCache(), loginId, httpRequest.getSession().getId());

            result = (InvestmentMethodResult) initUser(result, user, request.getUtcOffset(),request.getWriterId());
    	} catch(Exception e) {
    	    log.error("Error in insert dynamicsInvestment.", e);
    	    result.setErrorCode(ERROR_CODE_UNKNOWN);
    	}
	    return result;
	}

    /**
     * insert new Investment.
     *
     * @param request
     * @return InvestmentMethodResult.
     */
    public static InvestmentBubbleMethodResult insertBubbleInvestment(InsertBubbleInvestmentMethodRequest request, OpportunityCache oc, HttpServletRequest httpRequest) {
    	log.debug("insertInvestment: " + request);
    	InvestmentBubbleMethodResult result = new InvestmentBubbleMethodResult();
    	try {
    		validateBubblesRequest(concatBubblesRequestParams(request.getWriterId(), request.getUserId(), request.getOpportunityId(),
    				request.getAmount(), request.getStartTime(), request.getEndTime(), request.getLowLevel(), request.getHighLevel(),
    				request.getIpAddress(), request.getUtcOffset(), request.getPageLevel(), request.getOddsWin(), request.getToken()), request.getS(), request.getWriterId(), result);
        	if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
        		return result;
        	}
        	BubblesToken bt = validateBubblesToken(request.getToken(), request.getUserId(), result);
        	if(result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
        		return result;
        	}
        	long loginId = 0L;
        	long writerId = 0L;
        	if (bt != null) {
        		loginId = bt.getLoginId();
        		writerId = bt.getWriterId();
        	}
          	User user = UsersManagerBase.getById(request.getUserId());
          	
          	validateUserRegulationAndNonReg(user, result, AnyoptionService.VALIDATE_REGULATION_INVESTMENT);
          	if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
          		return result;
          	}            
            
            if (user != null && user.getCityFromGoogle() == null) {
				boolean res = false;
				long countryId = user.getCountryId();
				if (countryId == 0) {
					countryId = SkinsManagerBase.getSkin(user.getSkinId()).getDefaultCountryId();
				}
				Country c = CountryManagerBase.getCountry(countryId);
				if (user.getSkinId() != Skin.SKIN_ETRADER && user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
					try {
						res = UsersManagerBase.getUserCityCoordinates(user, c.getName());
					} catch (Exception e) {
						log.error("Error while calculate city coordinates" + e);
					}
				}
				if (!res) {
					user.setCityLatitude(c.getCapitalCityLatitude());
					user.setCityLongitude(c.getCapitalCityLongitude());
					user.setCityFromGoogle(c.getCapitalCity());
				}
			
            }
            
            // To set the user default amount value, because we get the user from db
            user.setDefaultAmountValue(UsersManagerBase.getUserDefInvAmount(user.getId()));
            OpportunityCacheBean opportunity = oc.getOpportunity(request.getOpportunityId());
            if (!InvestmentsManagerBase.validateOpportunityType(opportunity, Opportunity.TYPE_BUBBLES)) {
            	result.setErrorCode(ERROR_CODE_INVALID_INPUT);
            	return result;
            }
            double pageLevel = request.getPageLevel();//CommonUtil.roundDouble(request.getPageLevel(), MarketsManagerBase.getMarket(opportunity.getMarketId()).getDecimalPoint());
            
            Investment inv = new Investment();
            inv.setOpportunityId(request.getOpportunityId());
            inv.setAmount(request.getAmount());
            inv.setCurrentLevel(pageLevel);
            inv.setOddsWin(request.getOddsWin());
            inv.setOddsLose(0);
            inv.setTypeId(Investment.TYPE_BUBBLE);
            inv.setUtcOffsetCreated(CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset()));
            inv.setApiExternalUserId(0);
            String ip = request.getIp();
            if (!CommonUtil.IsParameterEmptyOrNull(request.getIpAddress())) {
            	ip = request.getIpAddress();
            }
            inv.setIp(ip);
            inv.setCurrencyId(user.getCurrencyId());
            inv.setTimeEstClosing(opportunity.getTimeEstClosing());

            long destUserId = -1;
            inv.setCopyopType(CopyOpInvTypeEnum.SELF);
            
            inv.setCopyOpInvId(0);
            
            inv.setBubbleStartTime(new Date(request.getStartTime()));
            inv.setBubbleEndTime(new Date(request.getEndTime()));
            inv.setBubbleLowLevel(request.getLowLevel());
            inv.setBubbleHighLevel(request.getHighLevel());
            inv.setWriterId(writerId);
            
            InvestmentValidatorBubble iv = new InvestmentValidatorBubble();
            MessageToFormat m = InvestmentsManagerBase.submitInvestment (
                    user,
                    inv,
                    opportunity,
                    false,
                    false,
                    writerId,
                    "SOAP request",
                    AnyoptionServiceServlet.getLevelsCache(),
                    iv,
                    destUserId, loginId);
            
            if (null == m) {
                result.setInvestmentId(inv.getId());
                result.setErrorCode(ERROR_CODE_SUCCESS);
            } else {
            	String apiErrorCode = null;
            	int errorCode = ERROR_CODE_INV_VALIDATION;
                if (m.getKey() != null && m.getKey().equalsIgnoreCase("error.investment.nomoney")) {
                	apiErrorCode = ApiErrorCode.API_ERROR_CODE_VALIDATION_BALANCE_LOWER_REQ_INV_AMOUNT;
                	errorCode = ERROR_CODE_VALIDATION_LOW_BALANCE;
                } else if (m.getKey() != null && m.getKey().equalsIgnoreCase("error.investment.no.cash.money")) {
                	apiErrorCode = ApiErrorCode.API_ERROR_CODE_VALIDATION_BALANCE_LOWER_REQ_INV_AMOUNT;
                	errorCode = ERROR_CODE_VALIDATION_LOW_CASH_BALANCE;
                } else if (m.getKey() != null && m.getKey().equalsIgnoreCase("error.investment.limit.min")) {
                	apiErrorCode = ApiErrorCode.API_ERROR_CODE_VALIDATION_AMOUNT_BELOW_MIN_INVESTMENT;
                } else if (m.getKey() != null && m.getKey().equalsIgnoreCase("error.investment.limit.max")) {
                	apiErrorCode = ApiErrorCode.API_ERROR_CODE_VALIDATION_AMOUNT_ABOVE_MAX_INVESTMENT;
                } else if (m.getKey() != null && m.getKey().equalsIgnoreCase("error.investment.notopened")) {
                	apiErrorCode =  ApiErrorCode.API_ERROR_CODE_VALIDATION_OPTION_CURRENTLY_NOT_AVAILABLE;
                } else if (m.getKey() != null && m.getKey().equalsIgnoreCase("error.investment.expired")) {
                	apiErrorCode = ApiErrorCode.API_ERROR_CODE_VALIDATION_OPTION_NO_LONGER_AVAILABLE;
                }
                result.setErrorCode(errorCode);
                result.addUserMessage("", CommonUtil.getMessage(user.getLocale(), m.getKey(), m.getParams()), apiErrorCode);
            }
            result.setBalance(com.anyoption.common.managers.UsersManagerBase.getUserBalanceFromDB(null, request.getUserId()));
	} catch (Exception e) {
	    log.error("Error in insert bubbleInvestment.", e);
	    result.setErrorCode(ERROR_CODE_UNKNOWN);
	}
    	log.info(result);
    	return result;
    }
    
	/**
	 * insert new Investment.
	 *
	 * @param request
	 * @param httpRequest
	 * @return
	 */
	public static SettleBubblesInvestmentMethodResult settleBubbleInvestment(	SettleBubbleInvestmentMethodRequest request,
																				HttpServletRequest httpRequest) {
		log.debug("SettleBublesInvestment: " + request);
		SettleBubblesInvestmentMethodResult result = new SettleBubblesInvestmentMethodResult();
		long userBalance = 0;
		try {
			
			if(request.getClosingLevel() >0 ) {
				Object[] params = {request.getWriterId(), request.getUserId(), request.getInvestmentId(),	request.getReturnAmount(), request.getSettleTime(), request.getClosingLevel()};
				validateBubblesRequest(	concatBubblesRequestParams(	params), request.getS(), request.getWriterId(), result);
			} else {
				Object[] params = 	{request.getWriterId(), request.getUserId(), request.getInvestmentId(),	request.getReturnAmount(), request.getSettleTime()};
				validateBubblesRequest(	concatBubblesRequestParams(	params), request.getS(), request.getWriterId(), result);
			}
			
			if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
				return result;
			}
			Investment inv = InvestmentsManagerBase.getInvestmentById(	request.getInvestmentId(),
																		ConstantsBase.PROFILE_LAST_INVESTMENTS_COUNT_LIMIT);
			if (null == inv || inv.getTypeId() != Investment.TYPE_BUBBLE || inv.getIsSettled() == 1) {
				result.setErrorCode(ERROR_CODE_INV_VALIDATION);
				return result;
			}
			InvestmentsManagerBase.settleInvestment(request.getInvestmentId(), request.getWriterId(), 0l, inv, request.getReturnAmount(), 0l);
			// update inv with bubbles closing level
			InvestmentsManagerBase.updateBubblesClosingLevel(request.getInvestmentId(), request.getClosingLevel());
			userBalance = com.anyoption.common.managers.UsersManagerBase.getUserBalanceFromDB(null, request.getUserId());
			result.setBalance(userBalance);
			result.setErrorCode(ERROR_CODE_SUCCESS);
		} catch (Exception e) {
			log.error("Error in settleBubblesInvestment: ", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
		log.info(result);
		return result;
	}

	public static BubblesGetDataMethodResult getData(BubblesGetDataMethodRequest request, WebLevelsCache wlc) {
		log.debug("getData:" + request);
		BubblesGetDataMethodResult result = new BubblesGetDataMethodResult();
		// validate request
		List<Object> params = new ArrayList<>();
		params.add(request.getWriterId());
		params.add(request.getUserId());
		if (request.getToken() != null) {
			validateBubblesRequest(	concatBubblesRequestParams(request.getWriterId(), request.getUserId(), request.getToken()),
									request.getS(), request.getWriterId(), result);
		} else {
			validateBubblesRequest(	concatBubblesRequestParams(request.getWriterId(), request.getUserId()), request.getS(),
									request.getWriterId(), result);
		}
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			return result;
		}
		try {
			long writerId = request.getWriterId();
			if (request.getToken() != null) {
				BubblesToken bt = validateBubblesToken(request.getToken(), request.getUserId(), result);
	        	if(result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
	        		return result;
	        	}
	        	if (bt != null) {
	        		writerId = bt.getWriterId();
	        	}
			}
			com.anyoption.common.beans.User user = UsersManagerBase.getUserDetails(request.getUserId());
			UserBalanceStepManager.loadUserBalanceStep(user, !TransactionsManagerBase.isFirstDeposit(request.getUserId(), 0), writerId);
			result.setDefaultAmount(user.getDefaultAmountValue());
			List<Long> amounts = new ArrayList<Long>();
			for (BalanceStepPredefValue predefValue : user.getPredefValues()) {
				amounts.add(predefValue.getPredefValue());
			}
			result.setAmounts(amounts);

			ArrayList<com.anyoption.common.beans.base.Market> list = SkinsManagerBase.getSkinMarketsListSorted(Skin.SKIN_API, Market.PRODUCT_TYPE_BUBBLES);
			List<com.anyoption.common.beans.base.Market> openedMarkets = new ArrayList<com.anyoption.common.beans.base.Market>();
			for (com.anyoption.common.beans.base.Market market : list) {
				for (long openedMarketId : wlc.getOpenedMarkets()) {
					if (market.getId() == openedMarketId) {
						openedMarkets.add(market);
						break;
					}
				}
			}
			List<BubblesAsset> assets = new ArrayList<BubblesAsset>();
			final Locale locale = new Locale(LanguagesManagerBase.getLanguage(	SkinsManagerBase.getSkin(user.getSkinId())
																								.getDefaultLanguageId()).getCode());
			for (com.anyoption.common.beans.base.Market market : openedMarkets) {
				if (!UsersManagerBase.isUserMarketDisabled(user.getId(), market.getId(), 0, true, 0l)) {
					String assetCode = market.getDisplayName().replaceFirst("markets\\.", "").replaceFirst("\\.bubbles", "").replaceAll("\\.", "");
					String assetName = MarketsManagerBase.getMarketName(user.getSkinId(), market.getId());
					long opportunityId = OpportunitiesManagerBase.getMarketCurrentOpportunity(market.getId(), Opportunity.TYPE_BUBBLES);
					int pricingLevel = market.getBubblesPricingLevel();
					assets.add(new BubblesAsset(assetCode, assetName, opportunityId, pricingLevel));
				}
			}
			Collections.sort(assets, new Comparator<BubblesAsset>() {

				@Override
				public int compare(BubblesAsset asset1, BubblesAsset asset2) {
					String asset1Label = asset1.getName();
					String asset2Label = asset2.getName();
					int returnVal = 0;
					try {
						Collator collator = Collator.getInstance(locale);
						returnVal = collator.compare(asset1Label, asset2Label);
					} catch (Exception e) {}
					return returnVal;
				}
			});

			result.setAssets(assets);
			result.setBalance(user.getBalance());
			result.setCurrency(CurrenciesManagerBase.getCurrency(user.getCurrencyId()).getCode());
			result.setPricingLevel(user.getUserActiveData().getBubblesPricingLevel());
			result.setClassId(user.getClassId());
			if(user.getTimeCreated() != null) {
				result.setUserRegistration(user.getTimeCreated().getTime());
			}
		} catch (Exception e) {
			log.error("Cannot load bubbles markets", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
		return result;
	}

	/**
	 * Get marketing users
	 * @param request
	 * @return MarketingUsersMethodResult
	 */
	public static MarketingUsersMethodResult getMarketingUsers(MarketingUsersMethodRequest request) {
		log.info("Action: getMarketingUsers for affiliateKey: " + request.getAffiliateKey() + " date: " + request.getDateRequest());
		MarketingUsersMethodResult result = new MarketingUsersMethodResult();
		try {
			MarketingInfo marketingInfo = UsersManagerBase.getMarketingUsers(request.getAffiliateKey(), request.getDateRequest());
			result.setMarketingInfo(marketingInfo);
		} catch (Exception e) {
			log.error("Error in getMarketingUsers", e);
            result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
		return result;
	}
	
	/**
	 * set error code to ERROR_CODE_WITHDRAWAL_CLOSE (110) if the user cannot make withdraw because it's weekend. in addition, add suitable user message.
	 * set error code to ERROR_CODE_SUCCESS (0) if the user can make withdraw.
	 * set error code to ERROR_CODE_UNKNOWN (999) upon exception. 
	 * 
	 * @param request
	 * @param httpRequest
	 * @return
	 */
	public static MethodResult isWithdrawalWeekendsAvailable(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("Start isWithdrawalWeekendsAvailable");
		MethodResult result = new MethodResult();
		try {
			User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
			if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
				log.info("Unable to load user");
				return result;
			}
			if (CommonUtil.isWithdrawalWeekendsNotAvailable(user)) {
				result.setErrorCode(ERROR_CODE_WITHDRAWAL_CLOSE);
				result.addUserMessage(null, CommonUtil.getMessage(user.getLocale(), "unknown.error.contact.support", null));
			}
		} catch (Exception e) {
		    log.error("Error in isWithdrawalWeekendsAvailable", e);
		    result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
		return result;
	}
	
	public static DepositBonusBalanceMethodResult getUserDepositBonusBalance(DepositBonusBalanceMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getUserDepositBonusBalance:" + request);
		DepositBonusBalanceMethodResult result = new DepositBonusBalanceMethodResult();
		try {
			User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
			if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
				log.info("Unable to load user");
				return result;
			}
			
			if(user.getId() != request.getUserId()){
				log.debug("Requested user is not equal with logged user");
				result.setErrorCode(ERROR_CODE_INVALID_INPUT);
				return result;
			}
			
			result = getDepositBonusBalanceBaseBase(request);
		} catch (Exception e) {
		    log.error("Error in getUserDepositBonusBalance", e);
		    result.setErrorCode(ERROR_CODE_UNKNOWN);
		}		
		return result;
	}
	
	public static UserMethodResult updateIsKnowledgeQuestion(IsKnowledgeQuestionMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("updateIsKnowledgeQuestion:" + request);
		UserMethodResult result = new UserMethodResult();
		try {
			User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
			if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
				log.info("Unable to load user");
				return result;
			}
			
			if(user.getId() != request.getUserId()){
				log.debug("Requested user is not equal with logged user");
				result.setErrorCode(ERROR_CODE_INVALID_INPUT);
				return result;
			}
			result = updateIsKnowledgeQuestion(user.getId(), request.isKnowledge(), request.getWriterId());
			initUser(result, user, request.getUtcOffset(), request.getWriterId());
		} catch (Exception e) {
		    log.error("Error in updateIsKnowledgeQuestion", e);
		    result.setErrorCode(ERROR_CODE_UNKNOWN);
		}		
		return result;
	}

	public static RetrieveUserDocumentsMethodResult retrieveUserDocuments(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("retrieveUserDocuments:" + request);
		RetrieveUserDocumentsMethodResult result = new RetrieveUserDocumentsMethodResult();
		User user = null;
		try {
			user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
		} catch (Exception e) {
			log.debug("Unable to validate user", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
		if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
			log.info("Unable to load user");
			return result;
		}
		Map<Long, com.anyoption.common.beans.base.File> userDocuments = new HashMap<Long, com.anyoption.common.beans.base.File>();
		result.setUserDocuments(userDocuments);
		Map<Long, Map<Long, com.anyoption.common.beans.base.File>> ccFilesMap = new HashMap<Long, Map<Long, com.anyoption.common.beans.base.File>>();
		result.setCcFilesMap(ccFilesMap);
		Locale locale = new Locale(LanguagesManagerBase	.getLanguage(SkinsManagerBase.getSkin(user.getSkinId()).getDefaultLanguageId())
														.getCode());
		List<File> userFiles = UsersManagerBase.getUserFiles(user.getId(), locale);
		if (userFiles != null) {
			for (File file : userFiles) {
				
				file.setStatusMsgId(FilesManagerBase.initStatus(file.getFileName(), 
									SkinsManagerBase.getSkin(user.getSkinId()).isRegulated(), 
									file.isApproved(), 
									file.isControlApproved()));
				
				file.setLock(FilesManagerBase.initLock((file.isApproved() || file.isControlApproved()) , file.isControlReject()));

				if (file.getFileTypeId() == FileType.BANKWIRE_CONFIRMATION) {
					file.setHideField(TransactionsManagerBase.getTransactionCountByType(user.getId(),
																						TransactionsManagerBase.TRANS_TYPE_WIRE_DEPOSIT) == 0	? true
																																				: false);
					userDocuments.put(file.getFileTypeId(), file);
				} else if (file.getFileTypeId() == FileType.CC_COPY_FRONT) {
					Map<Long, com.anyoption.common.beans.base.File> map = ccFilesMap.get(file.getCcId());
					if (map == null) {
						map = new HashMap<Long, com.anyoption.common.beans.base.File>(2);
						ccFilesMap.put(file.getCcId(), map);
					}
					map.put(FileType.CC_COPY_FRONT, file);
				} else if (file.getFileTypeId() == FileType.CC_COPY_BACK) {
					Map<Long, com.anyoption.common.beans.base.File> map = ccFilesMap.get(file.getCcId());
					if (map == null) {
						map = new HashMap<Long, com.anyoption.common.beans.base.File>(2);
						ccFilesMap.put(file.getCcId(), map);
					}
					map.put(FileType.CC_COPY_BACK, file);
				} else {
					userDocuments.put(file.getFileTypeId(), file);
				}

				file.setFileNameForClient(FilesManagerBase.getFileName(file.getFileName()));
			}
		}
		return result;
	}

    private static BubblesToken validateBubblesToken(String token, long userId, MethodResult result) throws SQLException {
    	BubblesToken bt = null;
    	if(token != null && token.length()>0) {
    		bt = UsersManagerBase.tokenExists(token, userId);
			if(bt == null) {
				log.debug("invalid token :"+ token + " for user :"+ userId);
				result.setErrorCode(ERROR_CODE_SESSION_EXPIRED);
			}
    	} else {
    		log.debug("token is empty in request for user :"+userId);
    		result.setErrorCode(ERROR_CODE_UNKNOWN);
    	}
    	return bt;
	}
    
    public static MethodResult fireServerPixel(FireServerPixelMethodRequest request, HttpServletRequest httpRequest) {
		FireServerPixelInfo fireServerPixelInfo = new FireServerPixelInfo();
		fireServerPixelInfo.setServerPixelsPublisherId(request.getPublisherId());
		fireServerPixelInfo.setServerPixelsTypeId(request.pixelTypeId);
		fireServerPixelInfo.setPublisherIdentifier(request.getPublisherIdentifier());
		fireServerPixelInfo.setUserId(request.getUserId());
		fireServerPixelInfo.setTransactionId(request.getTransactionId());
		fireServerPixelInfo.setDeviceId(request.getDeviceId());
		fireServerPixelInfo.setPlatformId(0);
		fireServerPixelInfo.setOrderId(null);
		int errorCode = ServerPixelsManagerBase.fireServerPixel(fireServerPixelInfo);
		MethodResult result = new MethodResult();
		result.setErrorCode(errorCode);
		return result;
    }

    public static WithdrawBonusRegulationStateMethodResult getWithdrawBonusRegulationState(WithdrawBonusRegulationStateMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("getWithdrawBonusRegulationState: " + request);
    	WithdrawBonusRegulationStateMethodResult result = new WithdrawBonusRegulationStateMethodResult();
    	try {
    		User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
    		if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
    			return result;
    		}
			getWithdrawBonusRegulationState(result, user.getId(), user.getSkinId(), request.getTransactionTypeId(), request.getCcId(),
											request.getWriterId(), user.getUtcOffset(), request.getIp());
    	} catch (Exception e) {
    		 log.error("Error in getWithdrawBonusRegulationState.", e);
             result.setErrorCode(ERROR_CODE_UNKNOWN);
    	}
    	return result;
    }
    
//	public static ChartDataResult getChartDataCommon(ChartDataRequest request, com.anyoption.common.util.OpportunityCache oc, ChartHistoryCache chc, WebLevelsCache wlc, HttpServletRequest httpRequest) {
//		System.out.println("Start executing GetChartDataCommon from..........." + AnyoptionService.class);
//		return CommonJSONService.getChartDataCommon(request, oc, chc, wlc, httpRequest);
//	}
    
    public static MethodResult unblockUserForThreshold(UnblockUserMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("unblockUserForThreshold:" + request);
		UserMethodResult result = new UserMethodResult();
		try {
			User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
			if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
				log.info("Unable to load user");
				return result;
			}
			
			if(user.getId() != request.getUserId()){
				log.debug("Requested user is not equal with logged user");
				result.setErrorCode(ERROR_CODE_INVALID_INPUT);
				return result;
			}
			result = unblockUserForThreshold(user.getId(), request.getWriterId());
		} catch (Exception e) {
		    log.error("Error in unblockUserForThreshold", e);
		    result.setErrorCode(ERROR_CODE_UNKNOWN);
		}		
		return result;
    }

	public static MethodResult insertWithdrawCancel(InsertWithdrawCancelMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("insertWithdrawCancel: " + request);
		MethodResult result = new MethodResult();
		try {
			User user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
												httpRequest);
			if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
				return result;
			}
			insertWithdrawCancel(	result, user.getId(), user.getSkinId(), request.getTransactionTypeId(), request.getCcId(),
									request.getWriterId(), user.getUtcOffset(), request.getIp());
		} catch (Exception e) {
			log.error("Error in insertWithdrawCancel.", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
		return result;
	}

    protected static UserMethodResult initUser(UserMethodResult result, User user, int utcOffset, long writerId) throws SQLException {
    	CommonJSONService.initUser(result, user, utcOffset, writerId);
    	Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(user.getSkinId()).getDefaultLanguageId()).getCode());
    	user.initFormattedValues(CommonUtil.formatJSUTCOffsetToString(utcOffset), locale);
    	return result;
    }
    
	public static TermsPartFilesMethodResult getTermsFiles(TermsFileMethodRequest request) {
		log.debug("getTermsFile:" + request);
		TermsPartFilesMethodResult result = getTermsFilesBase(request, false);
		return result;
	}

	public static InvestmentMethodResult cancelInvestment(CancelInvestmentMethodRequest request, OpportunityCache oc, WebLevelsCache levelsCache, HttpServletRequest httpRequest) {
		InvestmentMethodResult result = new InvestmentMethodResult();
		User user;
		try {
			user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
	        if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	            return result;
	        }
    	} catch (Exception e) {
    		log.debug("Unable to validate user", e);
    		result.setErrorCode(ERROR_CODE_UNKNOWN);
    		return result;
    	}
		long loginId = 0L;
		try {
			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
			}
		} catch (Exception e) {
			log.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
	        
	    try {
	    	long cancelTimeCumulative = (Integer.parseInt(Utils.getProperty("cancel.seconds.client")) + Integer.parseInt(Utils.getProperty("cancel.seconds.server"))) * 1000 ;  
			boolean success = CommonJSONService.cancelInvestment(request, user, oc, levelsCache, httpRequest.getSession(), loginId, cancelTimeCumulative);
			if(!success) {
				result.setErrorCode(ERROR_CODE_UNKNOWN);
			}
		} catch (SQLException e) {
			log.error("Error canceling "+ request, e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
	    
	    return result;
	}

	public static MethodResult changeCancelInvestmentCheckboxState(CancelInvestmentCheckboxStateMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("changeCancelInvestmentCheckboxState:" + request);
		MethodResult result = new MethodResult();
		User user;
		try {
			user = validateAndLoginEncrypt(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
			if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	            return result;
	        }
		} catch (Exception e) {
			log.debug("Unable to validate user", e);
    		result.setErrorCode(ERROR_CODE_UNKNOWN);
    		return result;
		}
		changeCancelInvestmentCheckboxState(result, request, user);
		return result;
	}
	
	public static InvestmentBubbleMethodResult cancelBubbleInvestment(CancelBubbleInvestmentMethodRequest request, HttpServletRequest httpRequest, WebLevelsCache wlc)  {
		log.debug("cancelBubbleInvestment:" + request);
		InvestmentBubbleMethodResult result = new InvestmentBubbleMethodResult();

		validateBubblesRequest(concatBubblesRequestParams(request.getInvestmentId(), request.getUserId(), request.getWriterId(), request.getToken()), request.getS(), request.getWriterId(), result);
    	if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
    		return result;
    	}

		boolean canceled = false;
		try {
	    	BubblesToken bt = validateBubblesToken(request.getToken(), request.getUserId(), result);
	    	if(result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
	    		return result;
	    	}
	    	long writerId = 0L;
	    	if (bt != null) {
	    		writerId = bt.getWriterId();
	    	}
	      	User user = UsersManagerBase.getById(request.getUserId());
	      	validateUserRegulationAndNonReg(user, result, AnyoptionService.VALIDATE_REGULATION_INVESTMENT);
            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
                return result;
            }

			long cancelSecondsCumulative = (Integer.parseInt(CommonUtil.getProperty("cancel.seconds.client")) + Integer.parseInt(CommonUtil.getProperty("cancel.seconds.server")))* 1000;
			Investment i = InvestmentsManagerBase.getById(request.getInvestmentId());
			canceled = InvestmentsManagerBase.userCancelInvestment(i, writerId, wlc, user, cancelSecondsCumulative, false);
		} catch (SQLException e) {
			log.error("Error canceling investment:" + request, e);
			canceled = false;
		}
		if (!canceled) {
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}

		return result;
	}
	
	public static VisibleSkinListMethodResult getVisibleSkins (MethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getVisibleSkins:" + request);
		Long skinId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.SESSION_SKIN);
		if(skinId == null){
			skinId = request.getSkinId();
		}
		Skin s = SkinsManagerBase.getSkin(skinId);		
		Long skinListTypeDetect = SkinsManagerBase.getSkinListType(request.getIp(), httpRequest, s);
		VisibleSkinListMethodResult result = new VisibleSkinListMethodResult();
		
		try {
			ArrayList<VisibleSkinPair> list = new ArrayList<>();
			for (Long visibleSkinId : SkinsManagerBase.getVisibleSkins().get(skinListTypeDetect)) {
					list.add( result.new VisibleSkinPair(visibleSkinId, "", "", 
								CountryManagerBase.getCountry(visibleSkinId).getSupportPhone(), 
									SkinsManagerBase.getSkin(visibleSkinId).getSupportEmail()));
			}
			result.setVisibleSkins(list);			
			result.setRegulated(s.isRegulated());
		} catch (SQLException e) {
			log.error("Error when getVisibleSkins", e);
		}
		return result;
	}
	
	/**
	 * Fire server pixel to Adquant
	 * @param writerId
	 * @param advId
	 * @param idfa
	 * @param duid 
	 */
	public static void serverPixelThread(long writerId, String advId, String idfa, String duid) {
    	Thread thread = new Thread() { 
    		public void run() {
            	FireServerPixelInfo fireServerPixelInfo = new FireServerPixelInfo();
				fireServerPixelInfo.setServerPixelsPublisherId(ServerPixel.SERVER_PIXELS_PUBLISHER_ADQUANT);
				fireServerPixelInfo.setServerPixelsTypeId(ServerPixelsManagerBase.TYPE_OPEN_APP);   
				fireServerPixelInfo.setDeviceId(duid);
				int platformId = Platform.ANYOPTION;     	    				
				if (writerId == Writer.WRITER_ID_COPYOP_MOBILE) {
					platformId = Platform.COPYOP;
				}    	    				
				fireServerPixelInfo.setPlatformId(platformId);
				String orderId = advId;
				if (null == orderId) {
					orderId = idfa;
				}
				if (null != orderId) {
					fireServerPixelInfo.setOrderId(orderId);
					ServerPixelsManagerBase.fireServerPixel(fireServerPixelInfo);
				} else {
					log.info("Can't find idfa or android advertising id");
				}                			
    		} 
    	};
    	thread.start();
	}
	
	public static TransactionMethodResult finish3dTransaction(ThreeDMethodRequest request, HttpServletRequest httpRequest) {
		TransactionMethodResult result = new TransactionMethodResult();

		Long transactionId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_TRANSACTION);
		if (transactionId == null) {
			log.error("No transaction id found in session");
    		result.setErrorCode(ERROR_CODE_UNKNOWN);
    		return result;
		}
		// remove immediately because this method cannot be called twice
		httpRequest.getSession().removeAttribute(ConstantsBase.BIND_SESSION_TRANSACTION);
		
		log.info("finishind 3d transaction : " + transactionId);
		Transaction t = null;
		try {
			t = TransactionsManagerBase.getTransaction(transactionId);
			long startTime = System.nanoTime();
			while(t.getStatusId() == TransactionsManagerBase.TRANS_STATUS_AUTHENTICATE) {
				t = TransactionsManagerBase.getById(transactionId);
				if(TimeUnit.SECONDS.convert((System.nanoTime() - startTime), TimeUnit.NANOSECONDS)> 120) {
					break;
				}
			}

		} catch(Exception ex) {
    		log.error("Unable to load transaction" + transactionId, ex);
    		result.setErrorCode(ERROR_CODE_UNKNOWN);
    		return result;
		}
		
		boolean isFirstSuccessDeposit = false;
		double userBalance = 0l;
		double dollarAmount = 0;
		try {
			User user = UsersManagerBase.getById(t.getUserId());

			isFirstSuccessDeposit = TransactionsManagerBase.countRealDeposit(user.getId()) == 1 ? true : false ;
			userBalance = UsersManagerBase.getUserBalanceFromDB(null, t.getUserId())/100;
			dollarAmount = CommonUtil.convertToBaseAmount(t.getAmount(), user.getCurrencyId(), new Date())/100;
		} catch (Exception e1) {
			log.error("Can't finish 3d trx:", e1);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
    		return result;
		}

        if(t.getStatusId()==TransactionsManagerBase.TRANS_STATUS_SUCCEED || t.getStatusId()==TransactionsManagerBase.TRANS_STATUS_PENDING) {
            result.setTransactionId(t.getId());
            result.setClearingProvider(t.getClearingProviderDescriptor());
            result.setErrorCode(ERROR_CODE_SUCCESS);
            result.setFirstDeposit(isFirstSuccessDeposit);
            result.setBalance(userBalance);
            result.setDollarAmount(dollarAmount);
            result.setAmount(t.getAmount() / 100d);
        } else {
        	result.setTransactionId(t.getId());
        	result.setErrorCode(ERROR_CODE_TRANSACTION_FAILED);
           	result.setFirstDeposit(isFirstSuccessDeposit);
            result.setBalance(userBalance);
            result.setDollarAmount(dollarAmount);
            result.setClearingProvider("");
            result.setAmount(0d);
        }
        
        return result;
	}
	
	public static TransactionMethodResult purchase3dTransaction(ThreeDMethodRequest request, HttpServletRequest httpRequest) {
		TransactionMethodResult result = new TransactionMethodResult();

		Long transactionId = request.getTransactionId();
		
		log.info("finishind 3d transaction : " + transactionId);
		Transaction t = null;
		User user = null;
		try {
			t = TransactionsManagerBase.getTransaction(transactionId);
			user = UsersManagerBase.getById(t.getUserId());
			user.setCountry(CountryManagerBase.getCountry(user.getCountryId()));

            TransactionsManagerBase.finish3dSecureTransaction(t, request.getPaRes(), request.getWriterId(), user);
            TransactionsManagerBase.afterDepositAction(user, t, TransactionSource.TRANS_FROM_MOBILE);

		} catch(Exception ex) {
    		log.error("Unable to finish transaction" + transactionId, ex);
    		result.setErrorCode(ERROR_CODE_UNKNOWN);
    		return result;
		}
		
		boolean isFirstSuccessDeposit = false;
		double userBalance = 0l;
		double dollarAmount = 0;
		try {
			isFirstSuccessDeposit = TransactionsManagerBase.countRealDeposit(user.getId()) == 1 ? true : false ;
			userBalance = UsersManagerBase.getUserBalanceFromDB(null, t.getUserId())/100;
			dollarAmount = CommonUtil.convertToBaseAmount(t.getAmount(), user.getCurrencyId(), new Date())/100;
		} catch (Exception e1) {
			log.error("Can't finish 3d trx:", e1);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
    		return result;
		}

        if(t.getStatusId()==TransactionsManagerBase.TRANS_STATUS_SUCCEED || t.getStatusId()==TransactionsManagerBase.TRANS_STATUS_PENDING) {
            result.setTransactionId(t.getId());
            result.setClearingProvider(t.getClearingProviderDescriptor());
            result.setErrorCode(ERROR_CODE_SUCCESS);
            result.setFirstDeposit(isFirstSuccessDeposit);
            result.setBalance(userBalance);
            result.setDollarAmount(dollarAmount);
            result.setAmount(t.getAmount() / 100d);
        } else {
        	result.setTransactionId(t.getId());
        	result.setErrorCode(ERROR_CODE_TRANSACTION_FAILED);
           	result.setFirstDeposit(isFirstSuccessDeposit);
            result.setBalance(userBalance);
            result.setDollarAmount(dollarAmount);
            result.setClearingProvider("");
            result.setAmount(0d);
        }
        
        return result;
	}
	
	 /**
	 * Insert mail form (contact request) 
	 * @param request
	 * @return
	 */
	public static MethodResult insertContactMailForm(ContactMethodRequest request, HttpServletRequest httpRequest) {
		log.info("insertMailForm function " + request);
		MethodResult result = new MethodResult();
		if (request == null) {
			log.error("Request is null");
			result.setErrorCode(ERROR_CODE_INVALID_INPUT);
			return result;
		}
		try {
			// set skin 
			if (request.getContact().getSkinId() == 0) {
				request.getContact().setSkinId(Skin.SKIN_REG_EN);
			}
			// set default combination if not found
			Long combId = request.getContact().getCombId();
			if (combId == null || combId == 0 || 
					MarketingCombinationManagerBase.getById(combId) == null) {
                combId = SkinsManagerBase.getSkin(request.getContact().getSkinId()).getDefaultCombinationId();
                log.warn("Cannot find combId for contact, setting default combid for skin:" + combId);
                request.getContact().setCombId(combId);
            }			
			// set ip
			String ip = request.getIp();
			request.getContact().setIp(ip);
			// set country	
			String countryCode = CommonUtil.getCountryCodeByIp(ip);
			long countryId = SkinsManagerBase.getSkin(request.getContact().getSkinId()).getDefaultCountryId();
			if (!CommonUtil.isParameterEmptyOrNull(countryCode)) {
				try {
					countryId = CountryManagerBase.getIdByCode(countryCode);
				} catch (SQLException e) {
					log.warn("Cannot get countryId", e);		
				}
			}
			String offset = CountryManagerBase.getCountries().get(countryId).getGmtOffset();
			request.getContact().setUtcOffset(offset);
			request.getContact().setCountryId(countryId);
			request.getContact().setUserAgent(CommonUtil.getUserAgent(httpRequest));

			if (CommonUtil.isParameterEmptyOrNull(request.getContact().getEmail())) {
				log.error("Email is empty or null!");
				setErrorMsg(request, result);
				return result;
			}
			// validate email address			
			boolean valid = CommonUtil.isEmailValidUnix(request.getContact().getEmail());
			if (!valid) {
				log.error("Email is not valid!");
				setErrorMsg(request, result);
				return result;
			}
			try {
				long userId = com.anyoption.common.managers.UsersManagerBase.getUserIdByEmail(request.getContact().getEmail());
				if (userId > 0) {
					request.getContact().setUserId(userId);
				} else {
					log.warn("Can not find user ID with email: " + request.getContact().getEmail());
				}
			} catch (SQLException ex) {
				log.warn("Cannot get user id due to: ", ex);
			}
			// set affiliate key for Contact
			long affKey = CommonUtil.getAffIdFromDynamicParameter(request.getContact().getDynamicParameter(), request.getContact().getCombId(), request.getContact().getSkinId());
			request.getContact().setAffiliateKey(affKey);			
			request.getContact().setType(Contact.CONTACT_MAIL_FORM);
			ContactsManagerBase.insertContactMailForm(request.getContact());
		} catch (Exception e) {
			log.error("Error insertMailForm", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
		return result;
	 }
	
	private static void setErrorMsg(ContactMethodRequest request, MethodResult result) {
		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getContact().getSkinId()).getDefaultLanguageId()).getCode());
		ArrayList<ErrorMessage> errorList = new ArrayList<>();
		ErrorMessage errMsg = new ErrorMessage();
		errMsg.setMessage(CommonUtil.getMessage(locale, "error.banner.contactme.email", null));
		errorList.add(errMsg);
		result.setErrorCode(ERROR_CODE_INVALID_INPUT);
		result.setUserMessages(errorList);
	}

}