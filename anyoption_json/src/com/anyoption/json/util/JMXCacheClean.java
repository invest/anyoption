package com.anyoption.json.util;

import java.lang.management.ManagementFactory;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.log4j.Logger;

import com.anyoption.beans.MobileApplication;


public class JMXCacheClean implements JMXCacheCleanMBean {
	public static final Logger log = Logger.getLogger(JMXCacheClean.class);

	protected String jmxName;

	public String getApkInfo(String apkName) {
		return MobileApplication.getApkInfo(apkName);
	}

	public void clearApkInfo(String apkName) {
		MobileApplication.clearApkInfo(apkName);
	}

	/**
     * Register the instance in the platform MBean server under given name.
     *
     * @param name the name under which to register
     */
    public void registerJMX(String name) {
        jmxName = name;
        try {
            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
            ObjectName on = new ObjectName(jmxName);
            mbs.registerMBean(this, on);
            if (log.isInfoEnabled()) {
                log.info("JMXCacheClean MBean registered - name: " + jmxName);
            }
        } catch (Exception e) {
            log.error("Failed to register in the MBean server.", e);
        }
    }

    /**
     * Unregister the instance from the platform MBean server. Use the name
     * under which it was registered.
     */
    public void unregisterJMX() {
        try {
            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
            ObjectName on = new ObjectName(jmxName);
            mbs.unregisterMBean(on);
            if (log.isInfoEnabled()) {
                log.info("JMXCacheClean MBean unregistered - name: " + jmxName);
            }
        } catch (Exception e) {
            log.error("Failed to unregister from the MBean server.", e);
        }
    }
}
