package com.anyoption.json.util;

/**
 * 
 * @author liors
 *
 */
public class UserHelper {
    private String userName;
    private String password;
    private String email;

    /**
     * @param userName
     * @param password
     */
    public UserHelper(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    /**
     * @param userName
     * @param password
     * @param email
     */
    public UserHelper(String userName, String password, String email) {
        this.userName = userName;
        this.password = password;
        this.email = email;
    }

    /**
     * @return user Name
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

}
