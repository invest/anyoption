package com.anyoption.json.util;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.beans.MarketingCombination;
import com.anyoption.beans.MarketingPixel;
import com.anyoption.json.service.AnyoptionServiceServlet;
import com.anyoption.managers.UsersManagerBase;

/**
 * 
 * @author liors
 *
 */
public class PixelGenerator  {

	public static final Logger log = Logger.getLogger(PixelGenerator.class);

	public static final long PIXEL_TYPE_FIRST_DEPOSIT = 1;
	public static final long PIXEL_TYPE_REGISTRATION = 2;
	public static final long PIXEL_TYPE_CONTACTME = 3;
	public static final long PIXEL_TYPE_HOMEPAGE = 4;
	public static final long PIXEL_TYPE_REG_LANDINGPAGE = 5;

	public static final String MARKETING_PARAMETER_DUID = "DUIDParam";
	public static final String MARKETING_PARAMETER_IP = "IPParam";


	/**
	 * run pixels
	 */
	public static void runPixel(long combinationId, long pixelType, String DUID, String ip) {
		try {
			MarketingCombination combination = UsersManagerBase.getCombinationById(combinationId);
			if (null != combination && null != combination.getCombPixels()
					&& combination.getCombPixels().getId() != 0
						&& null != combination.getCombPixels().getPixels()) {
				for (MarketingPixel p : combination.getCombPixels().getPixels()) {
					if (p.getTypeId() == pixelType) {
						String pixel = p.getHttpCode();
						if (null != DUID) {
							pixel = pixel.replace(MARKETING_PARAMETER_DUID, DUID);
						}
						if (null != ip) {
							pixel = pixel.replace(MARKETING_PARAMETER_IP, ip);
						}
						log.debug("found pixel to hit " + pixel);
						AnyoptionServiceServlet.getPixelHitter().send(pixel);
					}
				}
			}
		} catch (SQLException e) {
			log.error("cant get pixel for combination Id = " + combinationId + " pixelType = " + pixelType, e);
		}
	}

//	/**
//	 * Return the registerlanding pages pixels
//	 * @return
//	 * @throws SQLException
//	 */
//	public String getRegLandingPixel() throws SQLException {
//		String pixel = "";
//		FacesContext context = FacesContext.getCurrentInstance();
//		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
//		String combinationId = ap.getSessionCombinationId();
//
//		if (null == combination || (null != combination && null != combinationId && combination.getId() != Long.parseLong(combinationId))) {
//			//	get combination from user when combinationId is not from the defualt combination
//			if (null != combinationId && !ap.getDefualtCombination().contains(combinationId)){
//				combination = UsersManager.getCombinationById(Long.valueOf(combinationId));
//			}
//		}
//		if (null != combination && null != combination.getCombPixels() && combination.getCombPixels().getId() != 0
//					&& null != combination.getCombPixels().getPixels()) {
//			String tmp = "";
//			for (MarketingPixel p : combination.getCombPixels().getPixels()) {
//				if (p.getTypeId() == ConstantsBase.PIXEL_TYPE_REG_LANDINGPAGE) {
//					tmp = p.getHttpsCode() + "\n";
//			 		pixel += putParametersPixel(tmp, ap);
//				}
//			}
//		}
//		return pixel;
//	}
//
//
//	/**
//	 * Convert parameters to real value
//	 * @param pixel
//	 * @param app
//	 * @return
//	 */
//	public String putParametersPixel(String pixel, ApplicationData app){
//		String newPixel = pixel;
//		// replace all parameters with values
// 		if (newPixel.indexOf(Constants.MARKETING_PARAMETER_CONTACTID) > -1) {
// 			newPixel = newPixel.replace(Constants.MARKETING_PARAMETER_CONTACTID, app.getRegisterAttemptId());
// 		}
// 		//change all & to &amp;
// 		newPixel = newPixel.replace("&", "&amp;");
// 		return newPixel;
//	}

}
