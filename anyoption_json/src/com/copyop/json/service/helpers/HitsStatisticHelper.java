//package com.copyop.json.service.helpers;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.LinkedHashMap;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Map;
//
//import org.apache.log4j.Logger;
//
//import com.copyop.common.dto.ProfileInvResultSelf;
//import com.copyop.json.results.HitsMethodResult;
//import com.copyop.json.results.HitsMethodResult.AssetResult;
//import com.copyop.json.results.HitsMethodResult.TradesGroup;
//
//public class HitsStatisticHelper {
//
//	public static final Logger log = Logger.getLogger(HitsStatisticHelper.class);
//	
//	private static final int MAX_MARKET_CHARTS = 3;
//	
//	public static List<TradesGroup> getHitStatisticTradersGroup(ProfileInvResultSelf pir) {		
//		List<TradesGroup> list = new ArrayList<TradesGroup>();
//		int resultListSize = pir.getResults().size();
//		
//		int startIndex =  1;
//		int endIndex =  0;
//		
//		for(Integer gr : getHistogramGroupsCount(resultListSize)){
//			TradesGroup tradesGroup = new HitsMethodResult(). new TradesGroup();
//			
//			int fromIndex = resultListSize - gr;
//			int toIndex = resultListSize;
//			ArrayList<Boolean> groupRes = new ArrayList<Boolean>(pir.getResults().subList(fromIndex , toIndex ));
//			 
//			int frequency = Collections.frequency(groupRes, Boolean.TRUE);
//			int hitRate = Math.round(((float)frequency/gr*100));
//			
//			tradesGroup.setStartIndex(startIndex);
//			endIndex = (startIndex + gr) - 1;
//			tradesGroup.setEndIndex(endIndex);
//			tradesGroup.setHitRate(hitRate);
//			
//			resultListSize = resultListSize - gr;
//			startIndex = endIndex + 1; 
//			
//			list.add(tradesGroup);
//		}		
//		return list;
//		
//	}
//	
//	private static List<Integer> getHistogramGroupsCount(int records){
//		List<Integer> list = new ArrayList<Integer>();		
//		int histogroumCount = getHistogramCount(records);
//		int size = records;
//		int index = records/histogroumCount;
//		
//		for (int i = 0; i < histogroumCount; i++) {
//			if(i == histogroumCount - 1 ){
//				list.add(size);
//			} else{
//				list.add(index);
//				size = size - index;
//			}
//		}
//		return list;
//	}
//	
//	private static int getHistogramCount(int records){
//		int res = 0;		
//		if(records <= 14){
//			res = 2;
//		} else if (records > 14 && records <= 19){
//			res = 3;
//		} else if (records > 20){
//			res = 4;
//		}		
//		return res;
//	}
//	
//	public static List<AssetResult> getHitStatisticAssetResult(ProfileInvResultSelf pir) {		
//		List<AssetResult> list = new ArrayList<AssetResult>();
//		
//		Map<Long, List<Boolean>> hmMarketResults = new HashMap<Long, List<Boolean>>();
//		int i = 0;
//		for(Long marketId : pir.getMarketIds()){
//			
//			if(hmMarketResults.containsKey(marketId)){				
//				hmMarketResults.get(marketId).add(pir.getResults().get(i));
//			} else {
//				List<Boolean> res = new ArrayList<Boolean>();	
//				res.add(pir.getResults().get(i));
//				
//				hmMarketResults.put(marketId, res);
//			}
//			
//			i++;
//		}
//		
//		HashMap<Long, Integer> hmMarketHit = new HashMap<Long, Integer>();
//		for (Map.Entry<Long, List<Boolean>> entry : hmMarketResults.entrySet()) {		    
//			long key = entry.getKey();
//		    
//			int frequency = Collections.frequency(entry.getValue(), Boolean.TRUE);
//		    int hit = Math.round(((float)frequency/entry.getValue().size()*100));
//		    hmMarketHit.put(key, hit);
//		}
//		
//		int numMarket = 0;		
//		int otherMarketHitSum = 0;
//		
//		hmMarketHit = sortByValuesDesc(hmMarketHit);
//		
//		for (Map.Entry<Long, Integer> entry : hmMarketHit.entrySet()) {
//			if(numMarket < MAX_MARKET_CHARTS){				
//				AssetResult assetResult = new HitsMethodResult(). new AssetResult();				
//				assetResult.setMarketId(entry.getKey());
//				assetResult.setHitRate(entry.getValue());
//				list.add(assetResult);
//			} else {
//				otherMarketHitSum = otherMarketHitSum + entry.getValue();
//			}
//			
//			numMarket ++;
//		}
//		
//		// Add others Market
//		if(otherMarketHitSum > 0){
//			AssetResult assetResultOther = new HitsMethodResult(). new AssetResult();
//			long otheMarketId = HitsMethodResult.ASSET_MARKET_OTHER;
//			int othersHits = otherMarketHitSum/(hmMarketHit.size() - MAX_MARKET_CHARTS);
//			assetResultOther.setMarketId(otheMarketId);
//			assetResultOther.setHitRate(othersHits);
//			list.add(assetResultOther);
//		}		
//		return list;		
//	}
//	
//	private static HashMap sortByValuesDesc(HashMap map) { 
//	       List list = new LinkedList(map.entrySet());
//	       Collections.sort(list, new Comparator() {
//	            public int compare(Object o1, Object o2) {
//	               return ((Comparable) ((Map.Entry) (o2)).getValue())
//	                  .compareTo(((Map.Entry) (o1)).getValue());
//	            }
//	       });
//
//	       HashMap sortedHashMap = new LinkedHashMap();
//	       for (Iterator it = list.iterator(); it.hasNext();) {
//	              Map.Entry entry = (Map.Entry) it.next();
//	              sortedHashMap.put(entry.getKey(), entry.getValue());
//	       } 
//	       return sortedHashMap;
//	  }
//	
//}