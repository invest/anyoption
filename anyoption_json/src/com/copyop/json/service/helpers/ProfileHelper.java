package com.copyop.json.service.helpers;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.User;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.util.InvestmentHelper;
import com.anyoption.json.service.AnyoptionService;
import com.anyoption.managers.InvestmentsManagerBase;
import com.anyoption.managers.SkinsManagerBase;
import com.anyoption.managers.UsersManagerBase;
import com.anyoption.util.CommonUtil;
import com.anyoption.util.ConstantsBase;
import com.copyop.common.Constants;
import com.copyop.common.dto.CopiedInvestment;
import com.copyop.common.dto.Profile;
import com.copyop.common.dto.ProfileLink;
import com.copyop.common.enums.base.ProfileLinkTypeEnum;
import com.copyop.common.enums.base.UserStateEnum;
import com.copyop.common.managers.ConfigurationCopyopManager;
import com.copyop.common.managers.CopyHistoryManager;
import com.copyop.common.managers.FbManager;
import com.copyop.common.managers.InvestmentManager;
import com.copyop.common.managers.ProfileCountersManager;
import com.copyop.common.managers.ProfileLinksManager;
import com.copyop.common.managers.ProfileManager;
import com.copyop.common.managers.ProfileStatisticsManager;
import com.copyop.common.managers.ProfilesOnlineManager;
import com.copyop.json.results.ProfileMethodResult;
import com.copyop.json.results.ProfileMethodResult.ProfileWithLink;
import com.copyop.json.results.ProfileMethodResult.TopMarket;
import com.copyop.json.results.ProfileMethodResult.TopTrader;

/**
 * @author kirilim
 */
public class ProfileHelper {

	public static final Logger log = Logger.getLogger(ProfileHelper.class);
	private static final int MINIMUM_COPIED_PROFILE_COUNT = 4;
	private static final int MAXIMUM_COPIED_PROFILE_COUNT = 10;
	private static final int PROFILE_SELF_INVESTMENTS_COUNT_FOR_OPEN_HIT = 10;
	private static final int PROFILE_SELF_INVESTMENTS_COUNT_FOR_SHOW_HIT = 3;
	private static final int PROFILE_SELF_INVESTMENTS_COUNT_FOR_BEST_STREAK = 4;
	private static final int PROFILE_LAST_INVESTMENTS_COUNT_LIMIT = 3;

	public static void initOtherProfileMethodResult(ProfileMethodResult result, Profile profile, User user, HttpServletRequest httpRequest,
													int utcOffset) {
		com.anyoption.beans.User profileUser = null;
		try {
			profileUser = UsersManagerBase.getById(profile.getUserId());
		} catch (SQLException e) {
			log.debug("Failed to load user from profile with userId:" + profile.getUserId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			return;
		}
		initCommonProfileMethodResultFields(result,
											profile,
											profileUser,
											user.getCurrencyId(),
											profileUser.getCurrencyId(),
											user.getSkinId(),
											true);
		TreeMap<Long, List<CopiedInvestment>> marketsByProfit
												= InvestmentManager.getMarketsByProfit(
																		user.getId(),
																		profile.getUserId());
		result.setTopMarkets(getTopMarkets(marketsByProfit, result));
		result.setMarketsTotalResult(getMarketsTotalResult(marketsByProfit));
		result.setLastInvestments(getLastInvestments(	profile.getLastInvestments(), profileUser.getSkinId(), utcOffset,
														user.getCurrencyId(), profileUser.getCurrencyId()));
		result.setFbFriendsWithLink(getFbFriendsWithLink(profile, user, result));
		result.setCopiedProfiles(getCopiersProfiles(user.getId(), profile.getUserId()));
		if (result.getCopiedProfiles() == null) {
			result.setSimilarProfiles(getSimilarProfiles());
		}
		setCopyingAndWatching(result, user.getId(), profile.getUserId());
	}

	public static void initMyProfileMethodResult(ProfileMethodResult result, Profile profile, User user, HttpServletRequest httpRequest) {
		initCommonProfileMethodResultFields(result,
											profile,
											user,
											user.getCurrencyId(),
											user.getCurrencyId(),
											user.getSkinId(),
											false);
		setTopTradersAndTotalSuccess(result, user.getId());
		result.setFbFriends(getFbFriends(user.getId()));
		result.setCopiedProfiles(getCopyingProfiles(user.getId()));
		if (result.getCopiedProfiles() == null) {
			result.setSimilarProfiles(getSimilarProfiles());
		}
	}

	private static void initCommonProfileMethodResultFields(ProfileMethodResult result,
															Profile profile,
															User user,
															long userCurrencyId,
															long profileCurrencyId,
															long profileSkinId,
															boolean loadBlockedDataIfBlocked) {
		ProfileCountersManager.loadAllProfileRelations(profile);
		result.setNickname(profile.getNickname());
		// check copyop terms and not show names
		if (profile.isAcceptedTermsAndConditions()) {
			result.setFirstName(user.getFirstName());
			result.setLastName(user.getLastName());
		}
		result.setAvatar(profile.getAvatar());
		result.setTimeCreated(user.getTimeCreated());
		result.setOnline(ProfilesOnlineManager.isUserOnline(profile.getUserId()));
		result.setFrozen(profile.isFrozen());
		result.setCopiersCount(profile.getCopiers());
		result.setCopyingCount(profile.getCopying());
		result.setWatchersCount(profile.getWatchers());
		result.setWatchingCount(profile.getWatching());
		result.setDescription(setDescription(profile, user, profileSkinId));
		result.setHitRate(Math.round(profile.getHitRate() * 100)); // hit rate between 0 and 1
		setBestAssetIdAndProfit(result, profile.getBestAsset(), profile.getAssetsResults().get(profile.getBestAsset()), userCurrencyId, profileCurrencyId);
		setAvgTradesAndPeriod(result, profile);
		result.setShareCount(profile.getShare());
		result.setRateCount(profile.getRate());
		long count = ProfileStatisticsManager.getSelfInvestmentsCount(
												user.getId(),
												PROFILE_SELF_INVESTMENTS_COUNT_FOR_OPEN_HIT,
												(profile.getUserState() == UserStateEnum.STATE_BLOCKED
																						&& loadBlockedDataIfBlocked));
		result.setRiskAppetite(getRiskAppetite(profile, count));
		setStreakCountAndResult(result, profile, count, userCurrencyId, profileCurrencyId);
		setShowAndOpenHit(result, count);
	}

	private static int getRiskAppetite(Profile profile, long count) {
		if (count < 1) {
			return -1;
		}
		List<Float> riskAppetite = profile.getRiskAppetite();
		float sum = 0;
		for (Float val : riskAppetite) {
			sum += val;
		}
		float avg = (!riskAppetite.isEmpty()) ? sum / riskAppetite.size() : 0f;
		if (avg < 0.2) {
			return 1;
		} else if (avg < 0.7) {
			return 2;
		} else {
			return 3;
		}
	}

	private static List<TopMarket> getTopMarkets(TreeMap<Long, List<CopiedInvestment>> marketsByProfit, ProfileMethodResult result) {
		List<TopMarket> topMarkets = new ArrayList<ProfileMethodResult.TopMarket>();
		Iterator<Long> descKeySetIterator = marketsByProfit.descendingKeySet().iterator();
		int topMarketsCounter = 0;
		while (topMarketsCounter < TopMarket.TOP_MARKETS_COUNT && descKeySetIterator.hasNext()) {
			Long profit = descKeySetIterator.next();
			List<CopiedInvestment> ciList = marketsByProfit.get(profit);
			for (CopiedInvestment ci : ciList) {
				topMarkets.add(result.new TopMarket(ci.getMarketId(), ci.getCopiedCount(), ci.getProfit()));
				topMarketsCounter++;
				if (topMarketsCounter >= TopMarket.TOP_MARKETS_COUNT) {
					break;
				}
			}
		}
		return topMarkets;
	}

	private static long getMarketsTotalResult(TreeMap<Long, List<CopiedInvestment>> marketsByProfit) {
		Iterator<Long> descKeySetIterator = marketsByProfit.descendingKeySet().iterator();
		long marketsTotalResult = 0;
		while (descKeySetIterator.hasNext()) {
			long profit = descKeySetIterator.next();
			marketsTotalResult += (profit * marketsByProfit.get(profit).size());
		}
		return marketsTotalResult;
	}

	private static void setAvgTradesAndPeriod(ProfileMethodResult result, Profile profile) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -7);
		Date lastWeek = cal.getTime();
		cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -28);
		Date lastMonth = cal.getTime();
		int monthSum = 0;
		int lastWeekSum = 0;
		SimpleDateFormat formatter = new SimpleDateFormat(Constants.TRADES_HISTORY_DATE_FORMAT);
		for (String dateStr : profile.getTradesHistory().keySet()) {
			try {
				Date date = formatter.parse(dateStr);
				if (date.before(lastMonth)) {
					continue;
				}
				monthSum += profile.getTradesHistory().get(dateStr);
				if (date.after(lastWeek)) {
					lastWeekSum += profile.getTradesHistory().get(dateStr);
				}
			} catch (ParseException e) {
				log.error("Unable to parse date = " + dateStr, e);
			}
		}
		double avgMonthTrades = monthSum / 24d;
		double avgLastWeekSum = lastWeekSum / 6d;
		if (avgMonthTrades < avgLastWeekSum) {
			result.setAvgTrades(avgLastWeekSum);
			result.setAvgTradesPeriod(6);
		} else {
			result.setAvgTrades(avgMonthTrades);
			result.setAvgTradesPeriod(24);
		}
	}

	private static void setStreakCountAndResult(ProfileMethodResult result, Profile profile, long count, long userCurrencyId, long profileCurrencyId) {
		if (count >= PROFILE_SELF_INVESTMENTS_COUNT_FOR_BEST_STREAK) {
			result.setStreakCount(profile.getBestStreak());
			result.setStreakResult(getUserConvertedAmount(userCurrencyId, profileCurrencyId, profile.getBestStreakAmount()));
		} else {
			result.setStreakCount(-1);
			result.setStreakResult(-1);
		}
	}

	private static void setShowAndOpenHit(ProfileMethodResult result, long count) {
		if (count == PROFILE_SELF_INVESTMENTS_COUNT_FOR_OPEN_HIT) {
			result.setShowHitRate(true);
			result.setOpenHitRate(true);
		} else if (count >= PROFILE_SELF_INVESTMENTS_COUNT_FOR_SHOW_HIT) {
			result.setShowHitRate(true);
		}
	}

	private static void setBestAssetIdAndProfit(ProfileMethodResult result, long bestAssetMarketId, Long bestAssetProfit, long userCurrencyId, long profileCurrencyId) {
		if (bestAssetProfit == null) {
			result.setBestAssetMarketId(-1);
			result.setBestAssetMarketResult(-1);
			return;
		}
		double convertAmount = getBaseAmount(profileCurrencyId, bestAssetProfit);
		Long minProfit = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("BEST_ASSET_MIN_PROFIT", "10000"));
		if (convertAmount < minProfit) {
			result.setBestAssetMarketId(-1);
			result.setBestAssetMarketResult(-1);
		} else {
			if (userCurrencyId != profileCurrencyId) {
				bestAssetProfit = convertFromBaseAmount(userCurrencyId, convertAmount);
			}
			result.setBestAssetMarketId(bestAssetMarketId);
			result.setBestAssetMarketResult(bestAssetProfit);
		}
	}

	private static List<Investment> getLastInvestments(List<Long> lastInvestments, long skinId, int utcOffset, long userCurrencyId, long profileCurrencyId) {
		List<Investment> investments;
		List<Investment> returnInvestments;
		if (lastInvestments.size() > ConstantsBase.PROFILE_LAST_INVESTMENTS_COUNT_LIMIT) {
			lastInvestments.subList(ConstantsBase.PROFILE_LAST_INVESTMENTS_COUNT_LIMIT, lastInvestments.size()).clear();
		}
		try {
			investments = InvestmentsManagerBase.getInvestmentsById(lastInvestments, ConstantsBase.PROFILE_LAST_INVESTMENTS_COUNT_LIMIT);
		} catch (SQLException e) {
			log.error("Unable to get investments", e);
			return new ArrayList<Investment>();
		}
		String utcOffsetStr = CommonUtil.formatJSUTCOffsetToString(utcOffset);
		returnInvestments = new ArrayList<Investment>();
		for (Investment inv : investments) {
			inv.setAmount(getUserConvertedAmount(userCurrencyId, profileCurrencyId, inv.getAmount()));
			inv.setWin(getUserConvertedAmount(userCurrencyId, profileCurrencyId, inv.getWin()));
			inv.setLose(getUserConvertedAmount(userCurrencyId, profileCurrencyId, inv.getLose()));
			inv.setCurrencyId(userCurrencyId);
			InvestmentHelper.initInvestment(inv, skinId, utcOffsetStr);
			returnInvestments.add(inv);
		}
		return returnInvestments;
	}

	private static List<ProfileWithLink> getFbFriendsWithLink(Profile profile, User user, ProfileMethodResult result) {
		List<Long> fbUids = FbManager.getFbFriendsUserIds(user.getId());
		if (fbUids.isEmpty()) {
			return new ArrayList<ProfileMethodResult.ProfileWithLink>();
		}
		List<ProfileLink> pll = new ArrayList<ProfileLink>();
		for (Long fbUserId : fbUids) {
			pll.addAll(ProfileLinksManager.getProfileLinks(	fbUserId,
															profile.getUserId(),
															Arrays.asList(new ProfileLinkTypeEnum[] {ProfileLinkTypeEnum.COPY,
																										ProfileLinkTypeEnum.WATCH})));
		}
		List<ProfileWithLink> fbFriendsLink = new ArrayList<ProfileMethodResult.ProfileWithLink>();
		List<com.copyop.common.dto.base.Profile> fbFriends = new ArrayList<com.copyop.common.dto.base.Profile>();
		List<Long> linkFbUserIds = new ArrayList<Long>();
		boolean testProfile = ProfileManager.isTestProfile(user.getId());
		for (ProfileLink pl : pll) {
			com.copyop.common.dto.base.Profile fbFriend = ProfileManager.getFbFriendProfile(pl.getUserId());
			if ((testProfile || !fbFriend.isTest()) && UserStateEnum.STATE_REMOVED != fbFriend.getUserState()) {
				fbFriendsLink.add(result.new ProfileWithLink(fbFriend, pl.getType()));
				fbFriends.add(fbFriend);
				linkFbUserIds.add(pl.getUserId());
			}
		}
		ProfileManager.setProfileFirstLastName(fbFriends, linkFbUserIds);
		return fbFriendsLink;
	}

	private static String setDescription(Profile profile, User user, long skinId) {
		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(skinId).getDefaultLanguageId())
														.getCode());
		String name;
		if (user.getFirstName() != null) {
			name = user.getFirstName();
		} else {
			name = profile.getNickname();
		}

		String city = null;
		if (user.getCityName() != null) {
			city = user.getCityName();
		}
		String country = null;
		if (user.getCountryId() > 0) {
			country = CommonUtil.getMessage(CountryManagerBase.getCountry(user.getCountryId()).getDisplayName(), null, locale);
		}
		String currency = null;
		if (user.getCurrencyId() > 0) {
			currency = CurrenciesManagerBase.getCurrencies().get(user.getCurrencyId()).getNameKey();
		}

		if( city == null && country == null) {
			return CommonUtil.getMessage("copyop.profile.description.none", new String[]{name}, locale);
		} else {
			// do not show name when not accepted terms
			String description = "";
			if( city != null && country != null) {
				if(profile.isAcceptedTermsAndConditions()) {
					description = CommonUtil.getMessage("copyop.profile.description.name", new String[]{name}, locale);
				}
				description = description + " " +CommonUtil.getMessage("copyop.profile.description.live", new String[]{city}, locale)+ ", " +country+".";
			} else {
				if(profile.isAcceptedTermsAndConditions()) {
					description = CommonUtil.getMessage("copyop.profile.description.name", new String[]{name}, locale)+ ".";
				}
				String location = city != null ? city : country;
				description = description + " " +CommonUtil.getMessage("copyop.profile.description.live", new String[]{location}, locale)+ ".";
			}

			if(currency != null) {
				String currencyMessage = CommonUtil.getMessage("copyop.profile.description.currency", new String[]{currency}, locale);
				description += " "+currencyMessage;
			}
			log.debug(description);
			return description;
		}
	}

	private static List<com.copyop.common.dto.base.Profile> getCopiersProfiles(long userId, long profileUserId) {
		List<Long> ids = ProfileLinksManager.getCopiersIds(profileUserId);
		List<ProfileLink> links = new ArrayList<ProfileLink>();
		for (long id : ids) {
			links.addAll(ProfileLinksManager.getProfileLinksByType(id, ProfileLinkTypeEnum.COPY));
		}
		List<Long> profileIds = new ArrayList<Long>();
		for (ProfileLink link : links) {
			if (link.getDestUserId() != profileUserId) {
				profileIds.add(link.getDestUserId());
			}
		}
		return getCopiedProfiles(userId, profileIds);
	}

	private static List<com.copyop.common.dto.base.Profile> getCopyingProfiles(long userId) {
		List<Long> ids = ProfileLinksManager.getCopyingIds(userId);
		List<ProfileLink> links = new ArrayList<ProfileLink>();
		for (long id : ids) {
			links.addAll(ProfileLinksManager.getProfileLinksByType(id, ProfileLinkTypeEnum.COPY));
		}
		List<Long> profileIds = new ArrayList<Long>();
		for (ProfileLink link : links) {
			if (link.getDestUserId() != userId) {
				profileIds.add(link.getDestUserId());
			}
		}
		return getCopiedProfiles(userId, profileIds);
	}

	private static List<com.copyop.common.dto.base.Profile> getCopiedProfiles(long userId, List<Long> profileIds) {
		if (profileIds.size() >= MINIMUM_COPIED_PROFILE_COUNT) {
			// load profiles, see fb friends and their friends, take the profiles from copying(watching?) list and sort the rest by hit
			ArrayList<Long> fbFriendsUserIds = FbManager.getFbFriendsUserIds(userId);
			Set<Long> fbFriendsCopiers = new HashSet<Long>();
			for (long fbFriendUserId : fbFriendsUserIds) {
				int index = profileIds.indexOf(fbFriendUserId);
				if (index > -1) {
					fbFriendsCopiers.add(profileIds.get(index));
				}
			}
			Set<Long> userAndProfileCopiers = new HashSet<Long>();
			if (fbFriendsCopiers.size() < MAXIMUM_COPIED_PROFILE_COUNT) {
				Set<Long> fbFriendsCopiers2 = new HashSet<Long>();
				for (long fbFriendCopier : fbFriendsCopiers) {
					ArrayList<Long> fbFriendsUserIds2 = FbManager.getFbFriendsUserIds(fbFriendCopier);
					for (long fbFriendUserId : fbFriendsUserIds2) {
						int index = profileIds.indexOf(fbFriendUserId);
						if (index > -1) {
							fbFriendsCopiers2.add(profileIds.get(index));
						}
					}
					fbFriendsCopiers2.removeAll(fbFriendsCopiers);
					if (fbFriendsCopiers.size() + fbFriendsCopiers2.size() >= MAXIMUM_COPIED_PROFILE_COUNT) {
						break;
					}
				}
				fbFriendsCopiers.addAll(fbFriendsCopiers2);
				if (fbFriendsCopiers.size() < MAXIMUM_COPIED_PROFILE_COUNT) {
					// copying list
					List<Long> userCopying = ProfileLinksManager.getCopyingIds(userId);
					for (long userCopy : userCopying) {
						int index = profileIds.indexOf(userCopy);
						if (index > -1) {
							userAndProfileCopiers.add(userCopy);
						}
					}
				}
			}
			userAndProfileCopiers.removeAll(fbFriendsCopiers);
			Set<Long> restProfiles = new HashSet<Long>();
			if (fbFriendsCopiers.size() + userAndProfileCopiers.size() < MAXIMUM_COPIED_PROFILE_COUNT) {
				if (profileIds.size() - fbFriendsCopiers.size() - userAndProfileCopiers.size() > 0) { // this means we have more copiers of
																										// the profile
					profileIds.removeAll(fbFriendsCopiers);
					profileIds.removeAll(userAndProfileCopiers);
					Collections.shuffle(profileIds);
					if (profileIds.size() < MAXIMUM_COPIED_PROFILE_COUNT - fbFriendsCopiers.size() - userAndProfileCopiers.size()) {
						restProfiles.addAll(profileIds);
					} else {
						restProfiles.addAll(profileIds.subList(	0,
															MAXIMUM_COPIED_PROFILE_COUNT - fbFriendsCopiers.size()
																	- userAndProfileCopiers.size() - 1));
					}
				}
			}
			List<Long> copiers = new ArrayList<Long>(fbFriendsCopiers);
			copiers.addAll(userAndProfileCopiers);
			return loadProfiles(copiers, new ArrayList<Long>(restProfiles), userId); // restProfiles should be sorted
		} else {
			// other flow, if not at least 4 traders found
			return null;
		}
	}

	private static List<com.copyop.common.dto.base.Profile> loadProfiles(List<Long> copierIds, List<Long> restProfileIds, long userId) {
		List<Profile> restProfiles = ProfileManager.getProfiles(restProfileIds, true);
		Collections.sort(restProfiles, new Comparator<Profile>() {

			@Override
			public int compare(Profile o1, Profile o2) {
				if (o1.getHitRate() < o2.getHitRate()) {
					return -1;
				} else if (o1.getHitRate() > o2.getHitRate()) {
					return 1;
				} else {
					return 0;
				}
			}
		});
		List<Profile> copiers = ProfileManager.getProfiles(copierIds, true);
		List<com.copyop.common.dto.base.Profile> result = new ArrayList<com.copyop.common.dto.base.Profile>(copiers);
		result.addAll(restProfiles);
		boolean testProfile = ProfileManager.isTestProfile(userId);
		Iterator<com.copyop.common.dto.base.Profile> iterator = result.iterator();
		while (iterator.hasNext()) {
			com.copyop.common.dto.base.Profile profile = iterator.next();
			if ((!testProfile && profile.isTest()) || UserStateEnum.STATE_REMOVED == profile.getUserState()) {
				iterator.remove();
			}
		}
		return result;
	}

	private static List<com.copyop.common.dto.base.Profile> getSimilarProfiles() {
		List<Long> profileIds = ProfileManager.getRandomHotTraderIds(Profile.RANDOM_SIMILAR_TRADERS_SIZE);
		List<com.copyop.common.dto.base.Profile> profiles = new ArrayList<com.copyop.common.dto.base.Profile>(ProfileManager.getProfiles(profileIds, true));
		Iterator<com.copyop.common.dto.base.Profile> iter = profiles.iterator();
		while (iter.hasNext()) {
			com.copyop.common.dto.base.Profile profile = iter.next();
			if (UserStateEnum.STATE_REMOVED == profile.getUserState()) {
				iter.remove();
			}
		}
		return profiles;
	}

	private static void setTopTradersAndTotalSuccess(ProfileMethodResult result, long userId) {
		// copied investors history
		List<Long> copyFromUserIds = CopyHistoryManager.getCopyHistoryOf(userId);

		Map<Long, List<CopiedInvestment>> copiedInvestmentsByUser = InvestmentManager.getCopiedInvestments(userId, copyFromUserIds);
		TreeMap<Long, List<ProfileMethodResult.TopTrader>> overallProfit = new TreeMap<Long, List<ProfileMethodResult.TopTrader>>();
		int totalSuccessCounter = 0;
		float totalCounter = 0;
		for (Long id : copiedInvestmentsByUser.keySet()) {
			List<CopiedInvestment> list = copiedInvestmentsByUser.get(id);
			long profit = 0l;
			int ciCount = 0;
			for (CopiedInvestment ci : list) {
				profit += ci.getProfit();
				totalSuccessCounter += ci.getCopiedCountSuccess();
				ciCount += ci.getCopiedCount();
				totalCounter += ci.getCopiedCount();
			}
			TopTrader trader = result.new TopTrader(id, ciCount, profit);
			List<TopTrader> traders = overallProfit.get(profit);
			if (traders == null) {
				traders = new ArrayList<ProfileMethodResult.TopTrader>();
				overallProfit.put(profit, traders);
			}
			traders.add(trader);
		}
		result.setCopiedInvestmentsTotalSuccess(Math.round((totalSuccessCounter / totalCounter) * 100));

		Iterator<Long> profitsIterator = overallProfit.descendingKeySet().iterator();
		List<TopTrader> topTradersList = new ArrayList<ProfileMethodResult.TopTrader>();
		while (profitsIterator.hasNext()) {
			Long profit = profitsIterator.next();
			List<TopTrader> traders = overallProfit.get(profit);
			for (TopTrader trader : traders) {
				Profile p = new Profile();
				ProfileManager.loadProfileDetailsForTrader(trader.getUserId(), p);
				if (UserStateEnum.STATE_REMOVED == p.getUserState()) {
					log.warn("User [" + trader.getUserId() + "] removed. Skipping from top traders");
					continue;
				}
				trader.setNickname(p.getNickname());
				trader.setAvatar(p.getAvatar());
				topTradersList.add(trader);
				trader.setProfit(trader.getProfit());
				if (topTradersList.size() >= ProfileMethodResult.TopTrader.TOP_TRADERS_COUNT) {
					break;
				}
			}
			if (topTradersList.size() >= ProfileMethodResult.TopTrader.TOP_TRADERS_COUNT) {
				break;
			}
		}
		result.setTopTraders(topTradersList);
	}

	private static List<com.copyop.common.dto.base.Profile> getFbFriends(long userId) {
		List<Long> fbUids = FbManager.getFbFriendsUserIds(userId);
		if (fbUids.isEmpty()) {
			return new ArrayList<com.copyop.common.dto.base.Profile>();
		}
		List<com.copyop.common.dto.base.Profile> fbProfiles = ProfileManager.getFbProfilesSimple(fbUids);
		boolean testProfile = ProfileManager.isTestProfile(userId);
		Iterator<com.copyop.common.dto.base.Profile> iterator = fbProfiles.iterator();
		while (iterator.hasNext()) {
			com.copyop.common.dto.base.Profile fbProfile = iterator.next();
			if ((!testProfile && fbProfile.isTest())
					|| UserStateEnum.STATE_REMOVED == fbProfile.getUserState()) {
				iterator.remove();
			}
		}
		return fbProfiles;
	}

	private static void setCopyingAndWatching(ProfileMethodResult result, long userId, long profileUserId) {
		ProfileLink link = ProfileLinksManager.getProfileLink(userId, profileUserId, ProfileLinkTypeEnum.COPY);
		if (link != null) {
			result.setCopying(true);
		} else {
			link = ProfileLinksManager.getProfileLink(userId, profileUserId, ProfileLinkTypeEnum.WATCH);
			if (link != null) {
				result.setWatching(true);
			}
		}
	}

	public static long getUserConvertedAmount(long userCurrencyId, long profileCurrencyId, long amountToConvert) {
		if (userCurrencyId != profileCurrencyId) {
			double convertAmount = getBaseAmount(profileCurrencyId, amountToConvert);
			return convertFromBaseAmount(userCurrencyId, convertAmount);
		} else {
			return amountToConvert;
		}
	}

	private static double getBaseAmount(long profileCurrencyId, long amountToConvert) {
		double rate = CurrencyRatesManagerBase.getInvestmentsRate(profileCurrencyId);
		return amountToConvert * rate;
	}

	private static long convertFromBaseAmount(long userCurrencyId, double baseAmount) {
		return Math.round((1.0d / CurrencyRatesManagerBase.getInvestmentsRate(userCurrencyId)) * baseAmount);
	}
}