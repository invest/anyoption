package com.copyop.json.service;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.validator.ValidatorException;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.Skin;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.AssetResult;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.CurrenciesRules;
import com.anyoption.common.beans.base.Message;
import com.anyoption.common.beans.base.OpportunityOddsPair;
import com.anyoption.common.beans.base.Transaction;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.CurrenciesRulesManagerBase;
import com.anyoption.common.managers.IssuesManagerBase;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.managers.MessagesManager;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.ErrorMessage;
import com.anyoption.common.service.results.InvestmentMethodResult;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.SkinMethodResult;
import com.anyoption.common.util.OpportunityCache;
import com.anyoption.common.util.Utils;
import com.anyoption.json.requests.EmailMethodRequest;
import com.anyoption.json.requests.SystemChecksRequest;
import com.anyoption.json.service.AnyoptionService;
import com.anyoption.managers.BonusManagerBase;
import com.anyoption.managers.CopyopCoinsConvertManager;
import com.anyoption.managers.InvestmentsManagerBase;
import com.anyoption.managers.OpportunitiesManagerBase;
import com.anyoption.managers.SkinsManagerBase;
import com.anyoption.util.BonusUserFormater;
import com.anyoption.util.CommonUtil;
import com.anyoption.util.ConstantsBase;
import com.copyop.common.Constants;
import com.copyop.common.dto.CopiedInvestment;
import com.copyop.common.dto.Profile;
import com.copyop.common.dto.ProfileInvResultSelf;
import com.copyop.common.dto.ProfileLink;
import com.copyop.common.dto.base.CopyConfig;
import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.enums.CoinsActionTypeEnum;
import com.copyop.common.enums.ProfileLinkChangeReasonEnum;
import com.copyop.common.enums.base.PriorityEnum;
import com.copyop.common.enums.base.ProfileLinkCommandEnum;
import com.copyop.common.enums.base.ProfileLinkTypeEnum;
import com.copyop.common.enums.base.QueueTypeEnum;
import com.copyop.common.enums.base.UpdateTypeEnum;
import com.copyop.common.enums.base.UserStateEnum;
import com.copyop.common.jms.CopyOpEventSender;
import com.copyop.common.jms.events.ProfileManageEvent;
import com.copyop.common.managers.AppoxeeManager;
import com.copyop.common.managers.CoinsManager;
import com.copyop.common.managers.ConfigurationCopyopManager;
import com.copyop.common.managers.CopiedManager;
import com.copyop.common.managers.FbManager;
import com.copyop.common.managers.FeedManager;
import com.copyop.common.managers.InboxManager;
import com.copyop.common.managers.LastViewedManager;
import com.copyop.common.managers.ProfileCountersManager;
import com.copyop.common.managers.ProfileLinksManager;
import com.copyop.common.managers.ProfileManager;
import com.copyop.common.managers.ProfileStatisticsManager;
import com.copyop.helper.HitsStatisticHelper;
import com.copyop.json.requests.AcceptedTermsAndConditions;
import com.copyop.json.requests.ChangeNicknameMethodRequest;
import com.copyop.json.requests.CopyStepsEncourageMethodRequest;
import com.copyop.json.requests.CopyUserConfigMethodRequest;
import com.copyop.json.requests.CopyopHotDataMethodRequest;
import com.copyop.json.requests.InsertInvestmentMethodRequest;
import com.copyop.json.requests.InsertUserMethodRequest;
import com.copyop.json.requests.OriginalInvProfileMethodRequest;
import com.copyop.json.requests.PagingUserMethodRequest;
import com.copyop.json.requests.ProfileMethodRequest;
import com.copyop.json.requests.ShowOffMethodRequest;
import com.copyop.json.requests.UpdateAvatarMethodRequest;
import com.copyop.json.requests.UpdateFacebookFriendsMethodRequest;
import com.copyop.json.requests.WatchListMethodRequest;
import com.copyop.json.requests.WatchUserConfigMethodRequest;
import com.copyop.json.results.CopyListMethodResult;
import com.copyop.json.results.CopyStepsEncourageMethodResult;
import com.copyop.json.results.CopyUserConfigMethodResult;
import com.copyop.json.results.CoyopHotDataMethodResult;
import com.copyop.json.results.FacebookFriendsMethodResult;
import com.copyop.json.results.FavMarketsMethodResult;
import com.copyop.json.results.FeedMethodResult;
import com.copyop.json.results.HitsMethodResult;
import com.copyop.json.results.HolidayMessageResult;
import com.copyop.json.results.LastLoginSummaryMethodResult;
import com.copyop.json.results.LastViewedMethodResult;
import com.copyop.json.results.LastViewedMethodResult.LastViewedUser;
import com.copyop.json.results.OriginalInvProfileMethodResult;
import com.copyop.json.results.ProfileBestAssetMethodResult;
import com.copyop.json.results.ProfileCoinsMethodResult;
import com.copyop.json.results.ProfileMethodResult;
import com.copyop.json.results.UserMethodResult;
import com.copyop.json.results.WatchListMethodResult;
import com.copyop.json.results.WatchUserConfigMethodResult;
import com.copyop.json.service.helpers.ProfileHelper;
import com.copyop.migration.UsersMigrationManager;
import com.datastax.driver.core.utils.UUIDs;

/**
 * @author kirilim
 */
public class CopyopService {

	public static final Logger log = Logger.getLogger(CopyopService.class);	
	
	public static UserMethodResult insertCopyopProfile(InsertUserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("insertCopyopProfile: " + request);
		long beginTime = System.currentTimeMillis();
		UserMethodResult result = new UserMethodResult();
		validateAvatarURL(request.getAvatar(), result);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.error("Not a valid avatar");
			//return result;
		}
		Skin s = SkinsManagerBase.getSkin(request.getSkinId());
		Locale locale = new Locale(LanguagesManagerBase.getLanguage(s.getDefaultLanguageId()).getCode());
		try {
			ArrayList<ErrorMessage> msgs = AnyoptionService.validateInput("nicknameForm", request, locale);
			if (!msgs.isEmpty()) {
				result.setErrorCode(AnyoptionService.ERROR_CODE_INVALID_INPUT);
				msgs.get(0).setMessage(CommonUtil.getMessage(locale, "copyop.nickname.invalid", null));
				result.addUserMessages(msgs);
				return result;
			}
			// TODO check if this nickname exists in the new nicknames table, if it does abort
			if (ProfileManager.isNicknameRegistered(request.getNickname())) {
				log.debug("This nickname is already registered, nickname = " + request.getNickname());
				result.addUserMessage("nickname", CommonUtil.getMessage(locale, "copyop.nickname.change.exists", null));
				result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
				return result;
			}
		} catch (ValidatorException e) {
			log.error("Unable to validate input", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			return result;
		} catch (Exception e) {
			log.error("Unable to determine is the nickname registered", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			return result;
		}
		com.anyoption.common.service.results.UserMethodResult aoResult = AnyoptionService.insertUser(request, httpRequest);
		int maxCopiersPerAccount = Integer.valueOf(ConfigurationCopyopManager.getCopyopPropertiesValue("MAX_COPIERS_PER_ACCOUNT", "" + 100));
		result = new UserMethodResult(aoResult, maxCopiersPerAccount);
		if (aoResult.getErrorCode() != 0) {
			log.error("Unable to insert user. Error code: " + aoResult.getErrorCode());
			if (aoResult.getErrorMessages() != null) {
				for (ErrorMessage errorMessage : aoResult.getErrorMessages()) {
					log.error(errorMessage.getField() + " " + errorMessage.getMessage());
				}
			}
			return result;
		}
		Profile profile = null;
		try {
			boolean res = ProfileManager.insertNicknameIfNotExists(request.getNickname(), aoResult.getUser().getId());
			if (!res) {
				log.debug("This nickname is already registered, nickname = " + request.getNickname());
				result.addUserMessage(null, CommonUtil.getMessage(locale, "copyop.nickname.change.exists", null));
				result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
				return result;
			}

			if (!request.isAcceptedTermsAndConditions()) {
				log.error("User not accepted terms and conditions ");
				result.setErrorCode(AnyoptionService.ERROR_CODE_LOGIN_FAILED);
				result.addUserMessage("", CommonUtil.getMessage("error.register.terms", null, locale));
				return result;
			}

			User user = aoResult.getUser();
			profile = new Profile(user.getId(), request.getNickname(), request.getAvatar(), request.getFbId(),
									request.isAcceptedTermsAndConditions(), user.getClassId() == ConstantsBase.USER_CLASS_TEST);
			profile.setFrozen(true);
			profile.setTimeUserCreated(user.getTimeCreated());
			ProfileManager.insertProfile(profile);
			if (request.getFbId() != null && !request.getFbId().isEmpty()) {
				FbManager.insertFbId(user.getId(), request.getFbId());
				log.debug("Insert FacebookId:" + request.getFbId() + " for userId:" + user.getId());
				log.debug("Send Fcebook Friends Notification for userId:" + user.getId());
				CopyOpEventSender.sendEvent(new ProfileManageEvent(ProfileLinkCommandEnum.FB_NEW_FRIEND, user.getId()),
											CopyOpEventSender.PROFILE_MANAGE_DESTINATION_NAME);
			}
		} catch (Exception e) {
			log.error("Unable to insert profile with userId = " + aoResult.getUser().getId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			return result;
		}
		result.setCopyopProfile(profile);
		httpRequest.getSession().setAttribute(Constants.BIND_SESSION_PROFILE, profile);
		AppoxeeManager.createSegment(request.getWriterId(), request.getDeviceId(), result.getUser().getId());
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: insertCopyopProfile " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static UserMethodResult acceptedTermsAndConditions(AcceptedTermsAndConditions request, HttpServletRequest httpRequest) {
		log.debug("acceptedTermsAndConditions: " + request);
		UserMethodResult result = new UserMethodResult();
		com.anyoption.common.beans.User user = new com.anyoption.common.beans.User();
		Skin s = SkinsManagerBase.getSkin(request.getSkinId());
		Locale locale = new Locale(LanguagesManagerBase.getLanguage(s.getDefaultLanguageId()).getCode());
		try {
			ArrayList<ErrorMessage> msgs = AnyoptionService.validateInput("nicknameForm", request, locale);
			if (!msgs.isEmpty()) {
				result.setErrorCode(AnyoptionService.ERROR_CODE_INVALID_INPUT);
				msgs.get(0).setMessage(CommonUtil.getMessage(locale, "copyop.nickname.invalid", null));
				result.addUserMessages(msgs);
				return result;
			}
			// TODO check if this nickname exists in the new nicknames table, if it does abort
			if (ProfileManager.isNicknameRegistered(request.getNickname())) {
				log.debug("This nickname is already registered, nickname = " + request.getNickname());
				result.addUserMessage("nickname", CommonUtil.getMessage(locale, "copyop.nickname.change.exists", null));
				result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
				return result;
			}
		} catch (ValidatorException e) {
			log.error("Unable to validate input", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			return result;
		} catch (Exception e) {
			log.error("Unable to determine is the nickname registered", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			return result;
		}
		user.setId(0);
		try {
			UsersManagerBase.loadUserByName(request.getUserName(), user);
		} catch (Exception e) {
			log.error("Unable to load user", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_LOGIN_FAILED);
		}

		if (user.getId() == 0) {
			log.debug("Unable to load user");
			result.setErrorCode(AnyoptionService.ERROR_CODE_LOGIN_FAILED);
			return result;
		}

		if (!request.isAcceptedTermsAndConditions()) {
			log.error("User not accepted terms and conditions ");
			result.setErrorCode(AnyoptionService.ERROR_CODE_LOGIN_FAILED);
			result.addUserMessage("", CommonUtil.getMessage("error.register.terms", null, locale));
			return result;
		} else {
			try {
				boolean res = ProfileManager.insertNicknameIfNotExists(request.getNickname(), user.getId());
				if (!res) {
					log.debug("This nickname is already registered, nickname = " + request.getNickname());
					result.addUserMessage(null, CommonUtil.getMessage(locale, "copyop.nickname.change.exists", null));
					result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
					return result;
				}
				String oldNickname = ProfileManager.getProfileNickname(user.getId());
				ProfileManager.updateAcceptedTersmNicknameAvatar(	user.getId(), request.isAcceptedTermsAndConditions(),
																	request.getNickname(), request.getAvatar());
				ProfileManager.deleteOldNicknameFromNicknames(oldNickname);
				return getCopyopUser(request, httpRequest);
			} catch (Exception e) {
				log.error("Unable to update terms, nickname and avatar for userId = " + user.getId(), e);
				result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
				return result;
			}
		}
	}

	public static MethodResult changeCopyopNickname(ChangeNicknameMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("changeCopyopNickname:" + request);
		long beginTime = System.currentTimeMillis();
		MethodResult result = new MethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
		try {
			ArrayList<ErrorMessage> msgs = AnyoptionService.validateInput("nicknameForm", request, locale);
			if (!msgs.isEmpty()) {
				result.setErrorCode(AnyoptionService.ERROR_CODE_INVALID_INPUT);
				msgs.get(0).setMessage(CommonUtil.getMessage(locale, "copyop.nickname.invalid", null));
				result.addUserMessages(msgs);
				return result;
			}
		} catch (ValidatorException e) {
			log.error("Unable to validate input", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			return result;
		}
		try {
			String oldNickname = ProfileManager.getProfileNickname(user.getId());
			if (request.getNickname().equalsIgnoreCase(oldNickname)) {
				log.debug("The new and old nickname are the same");
				result.addUserMessage("nickname", CommonUtil.getMessage(locale, "copyop.nickname.change.same_as_old", null));
				result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
				return result;
			}

			if (!ProfileManager.isCopyopNicknameChanged(user.getId())) {
				boolean res = ProfileManager.insertNicknameIfNotExists(request.getNickname(), user.getId());
				if (!res) {
					log.debug("This nickname is already registered, nickname = " + request.getNickname());
					result.addUserMessage("nickname", CommonUtil.getMessage(locale, "copyop.nickname.change.exists", null));
					result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
					return result;
				}
				ProfileManager.changeCopyopNickname(user.getId(), request.getNickname(), oldNickname);
				if (!ProfileManager.isTestProfile(user.getId())) {
					CopyOpEventSender.sendEvent(new ProfileManageEvent(user.getId(), oldNickname, request.getNickname(),
																		ProfileLinkCommandEnum.NEW_NICKNAME),
												CopyOpEventSender.PROFILE_MANAGE_DESTINATION_NAME);
				}
			} else {
				result.addUserMessage(null, CommonUtil.getMessage(locale, "copyop.nickname.change.changed", null));
				result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
				return result;
			}
		} catch (Exception e) {
			log.error("Problem during executing changeCopyopNickname", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: changeCopyopNickname " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static FeedMethodResult getCopyopNews(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getCopyopNews: " + request);
		long beginTime = System.currentTimeMillis();
		FeedMethodResult result = new FeedMethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		try {
			List<FeedMessage> newsFeed = retrieveFeed(user.getId(), QueueTypeEnum.NEWS, new Date(), new Integer(ConfigurationCopyopManager.getCopyopPropertiesValue(FeedMessage.NEWS_FEED_CAPACITY, "25")));
			FeedMessage specialOffer = getSpecialOfferFeed(request, user, QueueTypeEnum.NEWS.getId());
			if(specialOffer != null){
				List<FeedMessage> newsFeedAndSpecial = new ArrayList<FeedMessage>();
				newsFeedAndSpecial.add(specialOffer);
				for(FeedMessage news : newsFeed){
					newsFeedAndSpecial.add(news);
				}

				result.setFeed(newsFeedAndSpecial);
			} else{
				result.setFeed(newsFeed);
			}
		} catch (Exception e) {
			log.error("Unable to retrieve feed - news", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getCopyopNews " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static FeedMethodResult copyopExplore(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("copyopExplore: " + request);
		long beginTime = System.currentTimeMillis();
		FeedMethodResult result = new FeedMethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		try {
			List<FeedMessage> exploreFeed = retrieveFeed(user.getId(), QueueTypeEnum.EXPLORE, new Date(), new Integer(ConfigurationCopyopManager.getCopyopPropertiesValue(FeedMessage.EXPLORE_FEED_CAPACITY, "25")));
			FeedMessage specialOffer = getSpecialOfferFeed(request, user, QueueTypeEnum.EXPLORE.getId());
			if(specialOffer != null){
				List<FeedMessage> exploreFeedAndSpecial = new ArrayList<FeedMessage>();
				exploreFeedAndSpecial.add(specialOffer);
				for(FeedMessage news : exploreFeed){
					exploreFeedAndSpecial.add(news);
				}

				result.setFeed(exploreFeedAndSpecial);
			} else{
				result.setFeed(exploreFeed);
			}

		} catch (Exception e) {
			log.error("Unable to retrieve feed - explore", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: copyopExplore " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static FeedMethodResult getCopyopInbox(PagingUserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getCopyopInbox: " + request);
		long beginTime = System.currentTimeMillis();
		FeedMethodResult result = new FeedMethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		if (request.isResetPaging()) {
			httpRequest.getSession().removeAttribute(FeedMessage.INBOX_FEED_MESSAGE_TIMEUUID_KEY);
			httpRequest.getSession().removeAttribute(FeedMessage.INBOX_FEED_MESSAGE_PERIOD_KEY);
		}
		List<FeedMessage> inboxFeed = new ArrayList<FeedMessage>();
		String period = (String) httpRequest.getSession().getAttribute(FeedMessage.INBOX_FEED_MESSAGE_PERIOD_KEY);
		SimpleDateFormat formatter = new SimpleDateFormat(Constants.INBOX_PERIOD_FORMAT);
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
		if (period == null) {
			period = formatter.format(new Date());
		}
		String lastPeriod = null;
		String timeUUID = (String) httpRequest.getSession().getAttribute(FeedMessage.INBOX_FEED_MESSAGE_TIMEUUID_KEY);
		// There is a case that for a period will have only 5 messages and the other messages must be populated from another period
		while (inboxFeed.size() < FeedMessage.INBOX_FEED_MESSAGE_PAGE_SIZE) {
			lastPeriod = period;
			Date date;
			try {
				inboxFeed.addAll(InboxManager.getInboxFeed(	user.getId(), lastPeriod, timeUUID,
															FeedMessage.INBOX_FEED_MESSAGE_PAGE_SIZE - inboxFeed.size()));
				// decrease the period
				date = formatter.parse(period);
			} catch (ParseException e) {
				log.error("Unable to parse period = " + period, e);
				break;
			} catch (Exception e) {
				log.error("Unable to get feed - inbox ", e);
				result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
				return result;
			}
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.MONTH, -1);
			date = cal.getTime();
			if (date.before(user.getTimeCreated())) {
				break;
			}
			period = formatter.format(date);
		}
		for (FeedMessage inboxMsg : inboxFeed) {
			loadMessageDetails(inboxMsg, false);
		}
		result.setFeed(inboxFeed);
		if (!inboxFeed.isEmpty()) {
			// add in session period and timeuuid of the last feed msg
			timeUUID = inboxFeed.get(inboxFeed.size() - 1).getTimeCreatedUUID().toString();
			httpRequest.getSession().setAttribute(FeedMessage.INBOX_FEED_MESSAGE_TIMEUUID_KEY, timeUUID);
			httpRequest.getSession().setAttribute(FeedMessage.INBOX_FEED_MESSAGE_PERIOD_KEY, lastPeriod);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getCopyopInbox " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static ProfileMethodResult getCopyopProfile(ProfileMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getCopyopProfile: " + request);
		long beginTime = System.currentTimeMillis();
		ProfileMethodResult result = new ProfileMethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		if (request.getRequestedUserId() <= 0) {
			log.debug("Invalid requestedUserId = " + request.getRequestedUserId());
			result.setErrorCode(AnyoptionService.ERROR_CODE_INVALID_INPUT);
			return result;
		}
		try {
			Profile profile = ProfileManager.getProfile(request.getRequestedUserId(), true);

			if (profile.getUserState() != UserStateEnum.STATE_REMOVED) {
				ProfileHelper.initOtherProfileMethodResult(result, profile, user, httpRequest, request.getUtcOffset());
				if (request.getRequestedUserId() != user.getId()) {
					LastViewedManager.insertLastViewed(user.getId(), request.getRequestedUserId());
				}
			} else {
				log.error("User " + request.getRequestedUserId() + " is removed.");
				result.setErrorCode(AnyoptionService.ERROR_CODE_USER_REMOVED);
			}
			if (log.isDebugEnabled()) {
				log.debug("TIME: getCopyopProfile " + (System.currentTimeMillis() - beginTime) + "ms");
			}
		} catch (Exception e) {
			log.debug("Unable to get profile with userId = " + request.getRequestedUserId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		return result;
	}

	public static OriginalInvProfileMethodResult getCopyopOriginalInvProfile(OriginalInvProfileMethodRequest request,
																						HttpServletRequest httpRequest) {
		log.debug("getCopyopOriginalInvProfile: " + request);
		long beginTime = System.currentTimeMillis();
		OriginalInvProfileMethodResult result = new OriginalInvProfileMethodResult();
		@SuppressWarnings("unused")
		com.anyoption.common.beans.User user = initUser(request.getUserName(),
														request.getPassword(),
														result,
														request.getSkinId(),
														false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		if (request.getCopiedInvestmentId() <= 0L) {
			log.debug("Invalid copiedInvestmentId = " + request.getCopiedInvestmentId());
			result.setErrorCode(AnyoptionService.ERROR_CODE_INVALID_INPUT);
			return result;
		}
		try {
			Long userId = InvestmentsManagerBase.getOriginalInvestmentUser(request.getCopiedInvestmentId());

			if (userId != null) {
				Profile profile = new Profile();
				ProfileManager.loadProfileDetailsForTrader(userId, profile);
				if (UserStateEnum.STATE_REMOVED != profile.getUserState()) {
					result.setUserId(userId);
					result.setAvatar(profile.getAvatar());
					result.setNickname(profile.getNickname());
				} else {
					log.error("Copied user [" + userId + "] removed");
					result.setErrorCode(AnyoptionService.ERROR_CODE_USER_REMOVED);
				}
			} else {
				log.error("Unable to get original investment user ID for copied investment: " + request.getCopiedInvestmentId());
				result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			}

			if (log.isDebugEnabled()) {
				log.debug("TIME: getCopyopOriginalInvProfile " + (System.currentTimeMillis() - beginTime) + "ms");
			}
		} catch (Exception e) {
			log.debug("Unable to get OriginalInvProfile for copied investment = " + request.getCopiedInvestmentId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		return result;
	}

	public static ProfileMethodResult myCopyopProfile(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("myCopyopProfile: " + request);
		long beginTime = System.currentTimeMillis();
		ProfileMethodResult result = new ProfileMethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		try {
			// explicit call instead of overloaded method call
			Profile profile = ProfileManager.getProfile(user.getId(), false);
			ProfileHelper.initMyProfileMethodResult(result, profile, user, httpRequest);
			long endTime = System.currentTimeMillis();
			if (log.isDebugEnabled()) {
				log.debug("TIME: getCopyopProfile " + (endTime - beginTime) + "ms");
			}
		} catch (Exception e) {
			log.debug("Unable to get profile with userId = " + user.getId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		return result;
	}

	public static CopyListMethodResult getCopyopCopiers(ProfileMethodRequest request, HttpServletRequest httpRequest){
		log.debug("getCopyopCopiers: " + request);
		long beginTime = System.currentTimeMillis();
		CopyListMethodResult result = new CopyListMethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		if (request.getRequestedUserId() <= 0) {
			log.debug("Invalid requestedUserId:" + request.getRequestedUserId());
			result.setErrorCode(AnyoptionService.ERROR_CODE_INVALID_INPUT);
			return result;
		}

		try {
			List<Long> requestedUserIdCopiers = ProfileLinksManager.getUserIdLinksByType(request.getRequestedUserId(), ProfileLinkTypeEnum.COPIED_BY);

			List<Long> userIdsCopying = ProfileLinksManager.getUserIdLinksByType(user.getId(), ProfileLinkTypeEnum.COPY);
			List<Long> userIdsWatching = ProfileLinksManager.getUserIdLinksByType(user.getId(), ProfileLinkTypeEnum.WATCH);

			if (requestedUserIdCopiers.isEmpty()) {
				result.setCopyList(new ArrayList<Profile>());
			} else {
				List<Profile> profiles = ProfileManager.getProfiles(requestedUserIdCopiers, true);
				for(Profile pf : profiles){
					if(userIdsCopying.contains(pf.getUserId())){
						pf.setCopying(true);
					}
					if(userIdsWatching.contains(pf.getUserId())){
						pf.setWatching(true);
					}
					ProfileCountersManager.loadAllProfileRelations(pf);
				}
				if (!ProfileManager.isTestProfile(user.getId())) {
					Iterator<Profile> iterator = profiles.iterator();
					while (iterator.hasNext()) {
						com.copyop.common.dto.base.Profile profile = iterator.next();
						if (profile.isTest()) {
							iterator.remove();
						}
					}
				}
				result.setCopyList(profiles);
			}
		} catch (Exception e) {
			log.error("Unable to get copiers for userId = " + user.getId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getCopyopCopiers " + (endTime - beginTime) + "ms");
		}

		return result;
	}

	public static WatchListMethodResult getCopyopWatchers(WatchListMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getCopyopWatchers: " + request);
		long beginTime = System.currentTimeMillis();
		WatchListMethodResult result = new WatchListMethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		try {
			List<Long> requestedUserIdWatchers = ProfileLinksManager.getUserIdLinksByType(request.getRequestedUserId(), ProfileLinkTypeEnum.WATCHED_BY);

			List<Long> userIdsCopying = ProfileLinksManager.getUserIdLinksByType(user.getId(), ProfileLinkTypeEnum.COPY);
			List<Long> userIdsWatching = ProfileLinksManager.getUserIdLinksByType(user.getId(), ProfileLinkTypeEnum.WATCH);
			if (requestedUserIdWatchers.isEmpty()) {
				result.setWatchList(new ArrayList<Profile>());
			} else {
				List<Profile> profiles = ProfileManager.getProfiles(requestedUserIdWatchers, true);

				for(Profile pf : profiles){
					if(userIdsCopying.contains(pf.getUserId())){
						pf.setCopying(true);
					}
					if(userIdsWatching.contains(pf.getUserId())){
						pf.setWatching(true);
					}
					ProfileCountersManager.loadAllProfileRelations(pf);
				}
				if (!ProfileManager.isTestProfile(user.getId())) {
					Iterator<Profile> iterator = profiles.iterator();
					while (iterator.hasNext()) {
						com.copyop.common.dto.base.Profile profile = iterator.next();
						if (profile.isTest()) {
							iterator.remove();
						}
					}
				}
				result.setWatchList(profiles);
			}
		} catch (Exception e) {
			log.error("Unable to get watchers for userId = " + user.getId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getCopyopWatchers " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static CopyListMethodResult getCopyopCopying(ProfileMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getCopyopCopying: " + request);
		long beginTime = System.currentTimeMillis();
		CopyListMethodResult result = new CopyListMethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		if (request.getRequestedUserId() <= 0) {
			log.debug("Invalid requestedUserId = " + request.getRequestedUserId());
			result.setErrorCode(AnyoptionService.ERROR_CODE_INVALID_INPUT);
			return result;
		}

		try {
			List<Long> requestedUserIdCopying = ProfileLinksManager.getUserIdLinksByType(request.getRequestedUserId(), ProfileLinkTypeEnum.COPY);
			List<Long> userIdsCopying;
			List<Long> userIdsWatching;

			if(request.getRequestedUserId() == user.getId()){
				userIdsCopying = requestedUserIdCopying;
				userIdsWatching = new ArrayList<Long>();
			} else {
				userIdsCopying = ProfileLinksManager.getUserIdLinksByType(user.getId(), ProfileLinkTypeEnum.COPY);
				userIdsWatching = ProfileLinksManager.getUserIdLinksByType(user.getId(), ProfileLinkTypeEnum.WATCH);
			}

			if (requestedUserIdCopying.isEmpty()) {
				result.setCopyList(new ArrayList<Profile>());
			} else {

				List<Profile> profiles = ProfileManager.getProfiles(requestedUserIdCopying, true);

				HashMap<Long, CopiedInvestment> copiedInvestmentSum = new HashMap<Long, CopiedInvestment>();
				copiedInvestmentSum = ProfileManager.getCopiedInvestmentSum(user.getId(), profiles);

				for(Profile pf : profiles){
					if(userIdsCopying.contains(pf.getUserId())){
						pf.setCopying(true);
					}
					if(userIdsWatching.contains(pf.getUserId())){
						pf.setWatching(true);
					}
					//Set Cost/Earned
					CopiedInvestment ci = copiedInvestmentSum.get(pf.getUserId());
					if(ci != null){
						pf.setCostEarned(ci.getProfit() / 100f);
					}
				}
				if (!ProfileManager.isTestProfile(user.getId())) {
					Iterator<Profile> iterator = profiles.iterator();
					while (iterator.hasNext()) {
						com.copyop.common.dto.base.Profile profile = iterator.next();
						if (profile.isTest()) {
							iterator.remove();
						}
					}
				}
				for (Profile profile : profiles) {
					ProfileCountersManager.loadAllProfileRelations(profile);
				}
				result.setCopyList(profiles);
			}
		} catch (Exception e) {
			log.error("Unable to  getCopyopCopying for userId = " + user.getId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getCopyopCopying " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static WatchListMethodResult getCopyopWatching(WatchListMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getCopyopWatching: " + request);
		long beginTime = System.currentTimeMillis();
		WatchListMethodResult result = new WatchListMethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		try {
			List<Long> requestedUserIdWatchers = ProfileLinksManager.getUserIdLinksByType(request.getRequestedUserId(), ProfileLinkTypeEnum.WATCH);

			boolean testUser = ProfileManager.isTestProfile(user.getId());
			List<Profile> profiles;
			if (requestedUserIdWatchers.isEmpty() && request.isAutoWatchRequired()) {
				// if list is empty, add top 5 traders, randomly selected from the current HOT table
				List<Long> profileIds = ProfileManager.getRandomHotTraderIds(Profile.RANDOM_HOT_TRADERS_SIZE);
				if (profileIds.contains(user.getId())) { // make sure you don't watch yourself
					profileIds.remove(user.getId());
				}
				profiles = ProfileManager.getProfiles(profileIds, true);
				for (Profile profile : profiles) {
					if (profile.getUserState() != UserStateEnum.STATE_REMOVED) {
						ProfileLinksManager.insertWatchProfileLink(user.getId(), profile.getUserId(), testUser, profile.isTest());
					}
				}
			} else {
				profiles = ProfileManager.getProfiles(requestedUserIdWatchers, true);
			}

			List<Long> userIdsCopying = ProfileLinksManager.getUserIdLinksByType(user.getId(), ProfileLinkTypeEnum.COPY);
			List<Long> userIdsWatching = ProfileLinksManager.getUserIdLinksByType(user.getId(), ProfileLinkTypeEnum.WATCH);
			for(Profile pf : profiles){
				if(userIdsCopying.contains(pf.getUserId())){
					pf.setCopying(true);
				}
				if(userIdsWatching.contains(pf.getUserId())){
					pf.setWatching(true);
				}
			}
			if (!testUser) {
				Iterator<Profile> iterator = profiles.iterator();
				while (iterator.hasNext()) {
					com.copyop.common.dto.base.Profile profile = iterator.next();
					if (profile.isTest()) {
						iterator.remove();
					}
				}
			}
			for (Profile profile : profiles) {
				ProfileCountersManager.loadAllProfileRelations(profile);
			}
			result.setWatchList(profiles);
		} catch (Exception e) {
			log.error("Unable to get watching list for userId = " + user.getId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getCopyopWatching " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static HitsMethodResult getCopyopHits(ProfileMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getCopyopHits/Statistics: " + request);
		long beginTime = System.currentTimeMillis();
		HitsMethodResult result = new HitsMethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		if (request.getRequestedUserId() <= 0) {
			log.debug("Invalid requestedUserId = " + request.getRequestedUserId());
			result.setErrorCode(AnyoptionService.ERROR_CODE_INVALID_INPUT);
			return result;
		}
		UserStateEnum requestedUserState = ProfileManager.getUserState(request.getRequestedUserId());
		if (null != requestedUserState && requestedUserState == UserStateEnum.STATE_REMOVED) {
			log.error("User " + request.getRequestedUserId() + " is removed.");
			result.setErrorCode(AnyoptionService.ERROR_CODE_USER_REMOVED);
			return result;
		}
		if (request.getRequestedUserId() == user.getId()) {
			// changing the user state to see his actual stats
			requestedUserState = UserStateEnum.STATE_REGULAR;
		}

		try {
			int limit = new Integer(ConfigurationCopyopManager.getCopyopPropertiesValue("INV_SUCCESS_RATE_HISTORY_SIZE", "120"));
			ProfileInvResultSelf pir = ProfileStatisticsManager.getProfileInvResultSelf(request.getRequestedUserId(), limit,
																						requestedUserState);

			final int MIN_INV_FOR_SHOW = 10;
			if (pir.getResults().size() < MIN_INV_FOR_SHOW) {
				log.debug("The user have :" + pir.getResults().size() + ". Can't show the HOT statistics");
				return result;
			}

			int frequency = Collections.frequency(pir.getResults(), Boolean.TRUE);
			int hitRate = Math.round(((float) frequency / pir.getResults().size() * 100));
			result.setHitRate(hitRate);
			result.setInvestmentsCount(pir.getResults().size());
			result.setLastTradeTime(pir.getTimeCreated().get(0));
			result.setTradesGroups(HitsStatisticHelper.getHitStatisticTradersGroup(pir));
			result.setAssetResults(HitsStatisticHelper.getHitStatisticAssetResult(pir));

		} catch (Exception e) {
			log.error("Unable to get copyop hits statistics for userId = " + user.getId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getCopyopHits " + (endTime - beginTime) + "ms");
		}
		return result;
	}

        /**
         *  Gets the asset specialists, people who have made most profit, 
         *  have above or equal success (hit) ratio of given param for a given asset (market) type
         *  
         * @param request
         * @param httpRequest
         * @return CoyopHotDataMethodResult that has given records for such specialists
         */
        public static CoyopHotDataMethodResult getCopyopAssetSpecialists(CopyopHotDataMethodRequest request, HttpServletRequest httpRequest) {
        	log.debug("getCopyopAssetSpecialists: " + request);
        	long beginTime = System.currentTimeMillis();
        	
        	CoyopHotDataMethodResult result = new CoyopHotDataMethodResult();
        	
        	List<UpdateTypeEnum> listMsg = new ArrayList<UpdateTypeEnum>();
		listMsg.add(UpdateTypeEnum.BEST_ASSET_SPECIALISTS);
		listMsg.add(UpdateTypeEnum.BEST_ASSET_SPECIALISTS_ONE_WEEK);
		listMsg.add(UpdateTypeEnum.BEST_ASSET_SPECIALISTS_ONE_MOTNH);
		listMsg.add(UpdateTypeEnum.BEST_ASSET_SPECIALISTS_TRHEE_MOTNH);
		try {
			List<Integer> existUpdateTypeEnumId = new ArrayList<Integer>();
			for (UpdateTypeEnum msg : listMsg){
				if (FeedManager.isHaveHotBestRecodrs(msg)) {
					existUpdateTypeEnumId.add(msg.getId());
				}
			}
			result.setExistUpdateTypeEnumId(existUpdateTypeEnumId);

			//If init call get first exist records
			if (request.isInitCall()) {
				if(result.getExistUpdateTypeEnumId().size() > 0) {
					int updetTypeId = result.getExistUpdateTypeEnumId().get(0);
					result.setFeed(loadHotTableData(UpdateTypeEnum.getById(updetTypeId)));
				}
			} else {
				if (result.getExistUpdateTypeEnumId() != null && result.getExistUpdateTypeEnumId().contains(request.getUpdateTypeEnumId())) {
					result.setFeed(loadHotTableData(UpdateTypeEnum.getById(request.getUpdateTypeEnumId())));
				}
			}

		} catch (Exception e) {
			log.error("Unable to retrieve feed - asset specialists: ", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
        	long endTime = System.currentTimeMillis();
        	if (log.isDebugEnabled()) {
        	    log.debug("TIME: getCopyopAssetSpecialists " + (endTime - beginTime) + "ms");
        	}
        	
		return result;	
        }

	public static CoyopHotDataMethodResult getCopyopHotTraders(CopyopHotDataMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getCopyopHotTraders: " + request);
		long beginTime = System.currentTimeMillis();
		CoyopHotDataMethodResult result = new CoyopHotDataMethodResult();

		List<UpdateTypeEnum> listMsg = new ArrayList<UpdateTypeEnum>();
		listMsg.add(UpdateTypeEnum.BEST_TRADERS);
		listMsg.add(UpdateTypeEnum.BEST_TRADERS_ONE_WEEK);
		listMsg.add(UpdateTypeEnum.BEST_TRADERS_ONE_MOTNH);
		listMsg.add(UpdateTypeEnum.BEST_TRADERS_TRHEE_MOTNH);
		try {
			List<Integer> existUpdateTypeEnumId = new ArrayList<Integer>();
			for(UpdateTypeEnum msg : listMsg){
				if(FeedManager.isHaveHotBestRecodrs(msg)){
					existUpdateTypeEnumId.add(msg.getId());
				}
			}
			result.setExistUpdateTypeEnumId(existUpdateTypeEnumId);

			//If init call get first exist records
			if(request.isInitCall()){
				if(result.getExistUpdateTypeEnumId().size() > 0){
					int updetTypeId = result.getExistUpdateTypeEnumId().get(0);
					result.setFeed(loadHotTableData(UpdateTypeEnum.getById(updetTypeId)));
				}
			} else {
				if(result.getExistUpdateTypeEnumId() != null && result.getExistUpdateTypeEnumId().contains(request.getUpdateTypeEnumId())){
					result.setFeed(loadHotTableData(UpdateTypeEnum.getById(request.getUpdateTypeEnumId())));
				}
			}

		} catch (Exception e) {
			log.error("Unable to retrieve feed - hot traders", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getCopyopHotTraders " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static CoyopHotDataMethodResult getCopyopHotTrades(CopyopHotDataMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getCopyopHotTrades: " + request);
		long beginTime = System.currentTimeMillis();
		CoyopHotDataMethodResult result = new CoyopHotDataMethodResult();

		List<UpdateTypeEnum> listMsg = new ArrayList<UpdateTypeEnum>();
		listMsg.add(UpdateTypeEnum.BEST_TRADES);
		listMsg.add(UpdateTypeEnum.BEST_TRADES_ONE_WEEK);
		listMsg.add(UpdateTypeEnum.BEST_TRADES_ONE_MOTNH);
		listMsg.add(UpdateTypeEnum.BEST_TRADES_TRHEE_MOTNH);
		try {
			List<Integer> existUpdateTypeEnumId = new ArrayList<Integer>();
			for(UpdateTypeEnum msg : listMsg){
				if(FeedManager.isHaveHotBestRecodrs(msg)){
					existUpdateTypeEnumId.add(msg.getId());
				}
			}
			result.setExistUpdateTypeEnumId(existUpdateTypeEnumId);

			//If init call get first exist records
			if(request.isInitCall()){
				if(result.getExistUpdateTypeEnumId().size() > 0){
					int updetTypeId = result.getExistUpdateTypeEnumId().get(0);
					result.setFeed(loadHotTableData(UpdateTypeEnum.getById(updetTypeId)));
				}
			} else {
				if(result.getExistUpdateTypeEnumId() != null && result.getExistUpdateTypeEnumId().contains(request.getUpdateTypeEnumId())){
					result.setFeed(loadHotTableData(UpdateTypeEnum.getById(request.getUpdateTypeEnumId())));
				}
			}

		} catch (Exception e) {
			log.error("Unable to retrieve feed - hot trades", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getCopyopHotTrades " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static CoyopHotDataMethodResult getCopyopHotCopiers(CopyopHotDataMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getCopyopHotCopiers: " + request);
		long beginTime = System.currentTimeMillis();
		CoyopHotDataMethodResult result = new CoyopHotDataMethodResult();

		List<UpdateTypeEnum> listMsg = new ArrayList<UpdateTypeEnum>();
		listMsg.add(UpdateTypeEnum.BEST_COPIERS);
		listMsg.add(UpdateTypeEnum.BEST_COPIERS_ONE_WEEK);
		listMsg.add(UpdateTypeEnum.BEST_COPIERS_ONE_MOTNH);
		listMsg.add(UpdateTypeEnum.BEST_COPIERS_TRHEE_MOTNH);
		try {
			List<Integer> existUpdateTypeEnumId = new ArrayList<Integer>();
			for(UpdateTypeEnum msg : listMsg){
				if(FeedManager.isHaveHotBestRecodrs(msg)){
					existUpdateTypeEnumId.add(msg.getId());
				}
			}
			result.setExistUpdateTypeEnumId(existUpdateTypeEnumId);

			//If init call get first exist records
			if(request.isInitCall()){
				if(result.getExistUpdateTypeEnumId().size() > 0){
					int updetTypeId = result.getExistUpdateTypeEnumId().get(0);
					result.setFeed(loadHotTableData(UpdateTypeEnum.getById(updetTypeId)));
				}
			} else {
				if(result.getExistUpdateTypeEnumId() != null && result.getExistUpdateTypeEnumId().contains(request.getUpdateTypeEnumId())){
					result.setFeed(loadHotTableData(UpdateTypeEnum.getById(request.getUpdateTypeEnumId())));
				}
			}

		} catch (Exception e) {
			log.error("Unable to retrieve feed - hot copiers", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getCopyopHotCopiers " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static InvestmentMethodResult copyopFollow(InsertInvestmentMethodRequest request, OpportunityCache oc, HttpServletRequest httpRequest) {
		log.debug("copyopFollow: " + request);
		long beginTime = System.currentTimeMillis();
		InvestmentMethodResult result = new InvestmentMethodResult();
		result.setCancelSeconds(Integer.parseInt(Utils.getProperty("cancel.seconds.client")));
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		Investment inv;
		try {
			inv = InvestmentsManagerBase.getInvestmentById(request.getCopyopInvId(), ConstantsBase.PROFILE_LAST_INVESTMENTS_COUNT_LIMIT);
		} catch (SQLException e) {
			log.error("Unable to get investment with id = " + request.getCopyopInvId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			Skin s = SkinsManagerBase.getSkin(request.getSkinId());
			Locale locale = new Locale(LanguagesManagerBase.getLanguage(s.getDefaultLanguageId()).getCode());
			result.addUserMessage("", CommonUtil.getMessage("error.investment", null, locale));
			return result;
		}

		if (inv.getUserId() == user.getId()) {
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			Skin s = SkinsManagerBase.getSkin(request.getSkinId());
			Locale locale = new Locale(LanguagesManagerBase.getLanguage(s.getDefaultLanguageId()).getCode());
			result.addUserMessage("", CommonUtil.getMessage("error.investment", null, locale));
			return result;
		}

		UserStateEnum destinationUserState = ProfileManager.getUserState(inv.getUserId());
		if( destinationUserState != null && destinationUserState == UserStateEnum.STATE_REMOVED) {
			log.error("User "+inv.getUserId()+" is removed.");
			result.setErrorCode(AnyoptionService.ERROR_CODE_USER_REMOVED);
			return result;
		}

		// no validation, just take params from original inv
		request.setOpportunityId(inv.getOpportunityId());
		try {
			request.setChoice((int) inv.getTypeId());
		} catch (Exception e) {
			log.error("Unable to cast investment type id", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			Skin s = SkinsManagerBase.getSkin(request.getSkinId());
			Locale locale = new Locale(LanguagesManagerBase.getLanguage(s.getDefaultLanguageId()).getCode());
			result.addUserMessage("", CommonUtil.getMessage("error.investment", null, locale));
			return result;
		}
		Opportunity opp;
		try {
			opp = OpportunitiesManagerBase.getRunningById(inv.getOpportunityId());
		} catch (SQLException e) {
			log.error("Unable to load opportunity with id = " + inv.getOpportunityId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			Skin s = SkinsManagerBase.getSkin(request.getSkinId());
			Locale locale = new Locale(LanguagesManagerBase.getLanguage(s.getDefaultLanguageId()).getCode());
			result.addUserMessage("", CommonUtil.getMessage("error.investment", null, locale));
			return result;
		}
		if (opp == null) {
			log.error("Opportunity is not open. opportunityId = " + inv.getOpportunityId());
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			Skin s = SkinsManagerBase.getSkin(request.getSkinId());
			Locale locale = new Locale(LanguagesManagerBase.getLanguage(s.getDefaultLanguageId()).getCode());
			result.addUserMessage("", CommonUtil.getMessage("error.investment", null, locale));
			return result;
		}

		OpportunityOddsPair oop = OpportunitiesManagerBase.getDefaultOpportunityOddsPair(opp.getOddsGroup());
		if(oop != null){
			request.setPageOddsWin(1 + (new Float(oop.getReturnSelector())/100));
			request.setPageOddsLose(new Float(oop.getRefundSelector())/100);
		} else {
			request.setPageOddsWin(1 + opp.getOddsType().getOverOddsWin());
			request.setPageOddsLose(1 - opp.getOddsType().getOverOddsLose());
		}
		
		// explicitly set the copyop inv type as followed
		request.setCopyopTypeId(CopyOpInvTypeEnum.FOLLOW);
		request.setDestUserId(inv.getUserId());
		result = AnyoptionService.insertInvestment(request, oc, httpRequest);
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: copyopFollow " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static MethodResult copyopCopyUserConfig(CopyUserConfigMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("copyopCopyUserConfig: " + request);
		long beginTime = System.currentTimeMillis();
		MethodResult result = new MethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		if (request.getConfig().getDestUserId() == user.getId()) {
			log.error("User trying to copy himself " + "destUserId=" + request.getConfig().getDestUserId());
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			return result;
		}
		
		//Check AO Regulation if the command is not uncopy
		if(!request.getConfig().getCommand().equals(ProfileLinkCommandEnum.UNCOPY)) {
			AnyoptionService.validateUserRegulationAndNonReg(user, result, AnyoptionService.VALIDATE_REGULATION_INVESTMENT);
			if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
	            return result;
	        }
		}
		
		UserStateEnum destinationUserState = ProfileManager.getUserState(request.getConfig().getDestUserId());
		if( destinationUserState != null && destinationUserState == UserStateEnum.STATE_REMOVED) {
			log.error("User "+request.getConfig().getDestUserId()+" is removed.");
			result.setErrorCode(AnyoptionService.ERROR_CODE_USER_REMOVED);
			return result;
		}

		boolean testProfile = ProfileManager.isTestProfile(request.getConfig().getDestUserId());
		boolean testUser = ProfileManager.isTestProfile(user.getId());
		if (!testUser) { // check the copied profile if it's test
			try {
				if (testProfile) {
					log.error("Not a test user is trying to copy a test user. userId = " + user.getId() + ", destUserId = "
								+ request.getConfig().getDestUserId());
					result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
					return result;
				}
			} catch (Exception e) {
				log.error("Unable to determine whether the profile is test, userId = " + request.getConfig().getDestUserId(), e);
				result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
				return result;
			}
		}
		if (!request.getConfig().getCommand().equals(ProfileLinkCommandEnum.COPY)
				&& !request.getConfig().getCommand().equals(ProfileLinkCommandEnum.UNCOPY)) {
			log.error("Not a correct command. Command = " + request.getConfig().getCommand());
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			return result;
		}
		try {
			if (request.getConfig().getCommand().equals(ProfileLinkCommandEnum.COPY)) {
				if (ProfileManager.isFrozenProfile(request.getConfig().getDestUserId())) {
					log.error("Frozen profile with id: " + request.getConfig().getDestUserId());
					Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId())
																								.getDefaultLanguageId()).getCode());
					result.addErrorMessage("", "Frozen profile");
					result.addUserMessage("", CommonUtil.getMessage(locale, "copyop.profile.frozen", null));
					result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
					return result;
				}

				ProfileLink existRecords = ProfileLinksManager.getProfileLink(	user.getId(), request.getConfig().getDestUserId(),
																				ProfileLinkTypeEnum.COPY);
				boolean isInsert = true;
				boolean isChangedCountDown = false;
				final int MAX_COPIED_INV_COUNT_CFG = 0;
				if (existRecords != null) {
					log.debug("Found exist profileLinks:" + existRecords);
					isInsert = false;
					// Check if inv. countDown is changed
					if (request.getConfig().getCount() != MAX_COPIED_INV_COUNT_CFG) {
						if (existRecords.getCountDown() != request.getConfig().getCount()) {
							isChangedCountDown = true;
						}
					} else {
						if (existRecords.getCount() != MAX_COPIED_INV_COUNT_CFG) {
							isChangedCountDown = true;
						}
					}
				}
				log.debug("For COPY link with UserId:" + user.getId() + " and destUserId:" + request.getConfig().getDestUserId()
							+ " are: isInsert:" + isInsert + "; isChangedCountDown" + isChangedCountDown + ";");

				ProfileLink sessionViewedCopyProfile = (ProfileLink) httpRequest.getSession()
																				.getAttribute(Constants.BIND_SESSION_PROFILE_LINK);
				if (sessionViewedCopyProfile == null) {
					log.debug("BIND_SESSION_PROFILE_LINK is NULL");
				} else {
					log.debug("Found BIND_SESSION_PROFILE_LINK:" + sessionViewedCopyProfile);

					if (request.getConfig().getCount() != MAX_COPIED_INV_COUNT_CFG) {
						if ((sessionViewedCopyProfile.getDestUserId() == request.getConfig().getDestUserId()) && !isInsert) {
							log.debug("Session destUsers and Request destUsers are equal");
							// Check if countDown in session and existRec are equal when count <> MAX_CFG
							if (((sessionViewedCopyProfile.getCountDown() != existRecords.getCountDown()))
									&& existRecords.getCount() != MAX_COPIED_INV_COUNT_CFG) {
								result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
								log.debug("The userId:" + user.getId() + "can't change copy investments count for destUserId:"
											+ request.getConfig().getDestUserId() + ", beacuse session:"
											+ sessionViewedCopyProfile.getCountDown() + " and current:" + existRecords.getCountDown()
											+ " countDown counters are not equal!");
								return result;
							}
							log.debug("existRecord.count:" + existRecords.getCountDown() + " and request.count:"
										+ request.getConfig().getCount());
						} else {
							log.debug("Session destUsers and Request destUsers are NOT equal");
						}
					}
				}

				request.getConfig().setAmount(request.getConfig().getAmount() * 100);

				if (isInsert && ProfileLinksManager.isWithMaxCopiers(request.getConfig().getDestUserId())) {
					log.debug("The userId:" + user.getId() + "can't copy destUserId:" + request.getConfig().getDestUserId()
								+ ", beacuse have max copiers!");
					result.setErrorCode(AnyoptionService.ERROR_CODE_COPYOP_MAX_COPIERS);
					return result;
				}

				if (!ProfileLinksManager.insertCopyProfileLink(	user.getId(), request.getConfig(), isInsert, isChangedCountDown, testUser,
																testProfile, request.getWriterId())) {
					result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
					return result;
				}

				// If have Watch delete
				ProfileLink watchLink = ProfileLinksManager.getProfileLink(	user.getId(), request.getConfig().getDestUserId(),
																			ProfileLinkTypeEnum.WATCH);
				if (watchLink != null) {
					ProfileLinksManager.deleteWatchProfileLink(	user.getId(), request.getConfig().getDestUserId(),
																watchLink.isWatchingPushNotification(),
																ProfileLinkChangeReasonEnum.START_COPY, testUser, testProfile);
				}

				if (isInsert && ((testUser && testProfile) || (!testUser && !testProfile))) {
					CopyOpEventSender.sendEvent(new ProfileManageEvent(user.getId(), request.getConfig().getDestUserId(),
																		request.getConfig().getAmount(), request.getConfig().getCount(),
																		request.getConfig().getAssets(), request.getConfig().getCommand()),
												CopyOpEventSender.PROFILE_MANAGE_DESTINATION_NAME);
					CopiedManager.updateCopiedCounters(request.getConfig().getDestUserId());
				}
			} else {
				ProfileLink existCopy = ProfileLinksManager.getProfileLink(	user.getId(), request.getConfig().getDestUserId(),
																			ProfileLinkTypeEnum.COPY);
				if (existCopy == null) {
					log.error("User:" + user.getId() + " trying to unCopy destUserId = " + request.getConfig().getDestUserId()
								+ " ,but missing profile COPY link!");
					result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
					return result;
				}
				ProfileLinksManager.deleteCopyProfileLink(user.getId(), request.getConfig(), testUser, testProfile);
				if ((testUser && testProfile) || (!testUser && !testProfile)) {
					CopyOpEventSender.sendEvent(new ProfileManageEvent(user.getId(), request.getConfig().getDestUserId(),
																		request.getConfig().getAmount(), request.getConfig().getCount(),
																		request.getConfig().getAssets(), request.getConfig().getCommand()),
												CopyOpEventSender.PROFILE_MANAGE_DESTINATION_NAME);
				}
			}
		} catch (Exception e) {
			log.error("Unable to make copy/uncopy action", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: copyopCopyUserConfig " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static CopyUserConfigMethodResult getCopyopCopyUserConfig(ProfileMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getCopyopCopyUserConfig: " + request);
		long beginTime = System.currentTimeMillis();
		CopyUserConfigMethodResult result = new CopyUserConfigMethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		if (request.getRequestedUserId() <= 0) {
			log.debug("Invalid requestedUserId:" + request.getRequestedUserId());
			result.setErrorCode(AnyoptionService.ERROR_CODE_INVALID_INPUT);
			return result;
		}
		UserStateEnum destinationUserState = ProfileManager.getUserState(request.getRequestedUserId());
		if( destinationUserState != null && destinationUserState == UserStateEnum.STATE_REMOVED) {
			log.error("User "+request.getRequestedUserId()+" is removed.");
			result.setErrorCode(AnyoptionService.ERROR_CODE_USER_REMOVED);
			return result;
		}
		try {
			ProfileLink pfl = ProfileLinksManager.getProfileLink(user.getId(), request.getRequestedUserId(), ProfileLinkTypeEnum.COPY);
			result.setConfig(new CopyConfig(pfl.getUserId(), pfl.getDestUserId(), pfl.getCountDown(), pfl.getAmount() / 100, pfl.getAssets()));
			httpRequest.getSession().setAttribute(Constants.BIND_SESSION_PROFILE_LINK, pfl);
			log.debug("Set in session BIND_SESSION_PROFILE_LINK:" + pfl);
		} catch (Exception e) {
			log.error("Unable to get copy config for userId = " + user.getId() + " and destUserId = " + request.getRequestedUserId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getCopyopCopyUserConfig " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static ProfileBestAssetMethodResult getCopyopProfileBestAssets(ProfileMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getCopyopProfileBestAssets: " + request);
		long beginTime = System.currentTimeMillis();
		ProfileBestAssetMethodResult result = new ProfileBestAssetMethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		if (request.getRequestedUserId() <= 0) {
			log.debug("Invalid requestedUserId:" + request.getRequestedUserId());
			result.setErrorCode(AnyoptionService.ERROR_CODE_INVALID_INPUT);
			return result;
		}

		try {
//			Profile pf = ProfileManager.getProfile(request.getRequestedUserId());
//			Map<Long, Long> assetsMap = new TreeMap<Long, Long>(Collections.reverseOrder());
//			assetsMap.putAll(pf.getAssetsResults());
			Long countOfAssets = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("PROFILE_BEST_ASSET_SHOW_COUNT", "3"));

//			long i = 0;
			List<Long> marketIds = new ArrayList<Long>();
//			for (Entry<Long, Long> entry : assetsMap.entrySet()) {
//				if (i < countOfAssets) {
//					marketIds.add(entry.getKey());
//					i++;
//				} else {
//					break;
//				}
//			}

//			result.setMarketIds(marketIds);

			int limit = new Integer(ConfigurationCopyopManager.getCopyopPropertiesValue("INV_SUCCESS_RATE_HISTORY_SIZE", "120"));
			ProfileInvResultSelf pir = ProfileStatisticsManager.getProfileInvResultSelf(request.getRequestedUserId(),
																						limit,
																						ProfileManager.getUserState(request.getRequestedUserId()));
			List<AssetResult> assetResults = HitsStatisticHelper.getHitStatisticAssetResult(pir);
			for (AssetResult res : assetResults) {
				if (marketIds.size() >= countOfAssets) {
					break;
				}
				marketIds.add(res.getMarketId());
			}
			result.setMarketIds(marketIds);
		} catch (Exception e) {
			log.error("Unable to  getCopyopProfileBestAssets for userId = " + user.getId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getCopyopProfileBestAssets " + (endTime - beginTime) + "ms");
		}

		return result;
	}

	public static MethodResult copyopWatchConfig(WatchUserConfigMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("copyopWatchConfig: " + request);
		long beginTime = System.currentTimeMillis();
		MethodResult result = new MethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		if (request.getDestUserId() == user.getId()) {
			log.error("User trying to watch himself " + "destUserId = " + request.getDestUserId());
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			return result;
		}
		
		//Check AO Regulation
		AnyoptionService.validateUserRegulationAndNonReg(user, result, AnyoptionService.VALIDATE_REGULATION_COPYOP_WATCH);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
            return result;
        }
		
		UserStateEnum destinationUserState = ProfileManager.getUserState(request.getDestUserId());
		if( destinationUserState != null && destinationUserState == UserStateEnum.STATE_REMOVED) {
			log.error("User "+request.getDestUserId()+" is removed.");
			result.setErrorCode(AnyoptionService.ERROR_CODE_USER_REMOVED);
			return result;
		}

		boolean testProfile = ProfileManager.isTestProfile(request.getDestUserId());
		boolean testUser = ProfileManager.isTestProfile(user.getId());
		if (!testUser) { // check the copied profile if it's test
			try {
				if (testProfile) {
					log.error("Not a test user is trying to watch a test user. userId = " + user.getId() + ", destUserId = "
								+ request.getDestUserId());
					result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
					return result;
				}
			} catch (Exception e) {
				log.error("Unable to determine whether the profile is test, userId = " + request.getDestUserId(), e);
				result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
				return result;
			}
		}
		if (!request.getCommand().equals(ProfileLinkCommandEnum.WATCH) && !request.getCommand().equals(ProfileLinkCommandEnum.UNWATCH)) {
			log.error("Not a correct command. Command = " + request.getCommand());
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			return result;
		}
		if (ProfileLinksManager.getProfileLink(user.getId(), request.getDestUserId(), ProfileLinkTypeEnum.COPY) != null) {
			log.error("This profile is currently copied. destUserId = " + request.getDestUserId());
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			return result;
		}
		try {
			if (request.getCommand().equals(ProfileLinkCommandEnum.WATCH)) {
				ProfileLink existRecords = ProfileLinksManager.getProfileLink(user.getId(), request.getDestUserId(), ProfileLinkTypeEnum.WATCH);
				boolean isInsert = true;
				if(existRecords != null){
					isInsert = false;
				}

				ProfileLinksManager.insertUpdateWatchProfileLink(	user.getId(), request.getDestUserId(),
																	request.isWatchingPushNotification(), isInsert,
																	testUser, testProfile);
				if (isInsert && ((testUser && testProfile) || (!testUser && !testProfile))) {
					CopyOpEventSender.sendEvent(new ProfileManageEvent(user.getId(), request.getDestUserId(), request.getCommand()),
												CopyOpEventSender.PROFILE_MANAGE_DESTINATION_NAME);
				}
			} else {
				ProfileLink existWatch = ProfileLinksManager.getProfileLink(user.getId(), request.getDestUserId(), ProfileLinkTypeEnum.WATCH);
				if(existWatch == null){
					log.error("User:" + user.getId() + " trying to unWatch destUserId = " + request.getDestUserId() + " ,but missing profile WATCH link!");
					result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
					return result;
				}
				ProfileLinksManager.deleteWatchProfileLink(	user.getId(), request.getDestUserId(), request.isWatchingPushNotification(),
															ProfileLinkChangeReasonEnum.STOP_WATCH,
															testUser, testProfile);
			}
		} catch (Exception e) {
			log.error("Unable to make watch/unwatch action", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: copyopWatchConfig " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static WatchUserConfigMethodResult getCopyopWatchUserConfig(ProfileMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getWatchCopyUserConfig: " + request);
		long beginTime = System.currentTimeMillis();
		WatchUserConfigMethodResult result = new WatchUserConfigMethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		if (request.getRequestedUserId() <= 0) {
			log.debug("Invalid requestedUserId:" + request.getRequestedUserId());
			result.setErrorCode(AnyoptionService.ERROR_CODE_INVALID_INPUT);
			return result;
		}
		UserStateEnum destinationUserState = ProfileManager.getUserState(request.getRequestedUserId());
		if( destinationUserState != null && destinationUserState == UserStateEnum.STATE_REMOVED) {
			log.error("User "+request.getRequestedUserId()+" is removed.");
			result.setErrorCode(AnyoptionService.ERROR_CODE_USER_REMOVED);
			return result;
		}
		try {
			ProfileLink pfl = ProfileLinksManager.getProfileLink(user.getId(), request.getRequestedUserId(), ProfileLinkTypeEnum.WATCH);
			result.setWatchingPushNotification(pfl.isWatchingPushNotification());
		} catch (Exception e) {
			log.error("Unable to get copy config for userId = " + user.getId() + " and destUserId = " + request.getRequestedUserId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getWatchCopyUserConfig " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static ProfileCoinsMethodResult getCopyopProfileCoins(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getCopyopProfileCoins: " + request);
		long beginTime = System.currentTimeMillis();
		ProfileCoinsMethodResult result = new ProfileCoinsMethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);

		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		try {
			result.setRates(ConfigurationCopyopManager.getCopyopCoinsConversionRates(user.getCurrencyId()));
			result.setConvertRemainingTime(ProfileManager.getCoinsRemainingConvertTime(user.getId()));
			result.setBalance(ProfileCountersManager.getProfileCoinsBalance(user.getId()));

			result.setMinInvAmount(InvestmentsManagerBase.getUserMinInvestmentLimit(user.getCurrencyId()));
			result.setCoinsPerAmountInv(ConfigurationCopyopManager.getCopyopStepsAndCoinsPerCurrency(user.getCurrencyId()));
			result.setFollowCoins(Long.parseLong(ConfigurationCopyopManager.getCopyopPropertiesValue("COINS_PER_FOLLOWED_INV", "1")));
		} catch (Exception e) {
			log.error("Can't get coins convert rates, coins balance or other property for userId = " + user.getId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getCopyopProfileCoins " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static ProfileCoinsMethodResult depositCopyopCoins(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("depositCopyopCoins: " + request);
		long beginTime = System.currentTimeMillis();
		long loginId = 0L;
		try {
			if (httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID) != null) {
				loginId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.LOGIN_ID);
			}
		} catch (Exception e) {
			log.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		ProfileCoinsMethodResult result = new ProfileCoinsMethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}

		try {
			BonusUsers bonusUsers = CopyopCoinsConvertManager.convertCopyopCoins(user, request.getWriterId(), request.getIp());
			bonusUsers = BonusManagerBase.getBonusUserByBonusId(bonusUsers.getUserId(), bonusUsers.getId()); //Get it from the db with all the param's.
			// claim copyop coins immediate after convert them.
			if (bonusUsers != null && user != null) {
				BonusManagerBase.acceptBonusUser(bonusUsers, user, CommonUtil.getIPAddress(httpRequest), loginId);
			}
			AnyoptionService.refreshUserInSession(user.getId(), httpRequest);
			result = getCopyopProfileCoins(request, httpRequest);
		} catch (Exception e) {
			log.error("Can't convert coins", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getCopyopProfileCoins " + (endTime - beginTime) + "ms");
		}

		return result;
	}

	public static MethodResult updateFacebookFriends(UpdateFacebookFriendsMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("updateFacebookFriends: " + request);
		long beginTime = System.currentTimeMillis();
		boolean sendEvent = false;
		MethodResult result = new MethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		try {
			if (!CommonUtil.IsParameterEmptyOrNull(request.getFacebookId())) {
				if(!request.getFacebookId().equals(ProfileManager.getFaceBookId(user.getId()))){
					FbManager.insertFbId(user.getId(), request.getFacebookId());
					log.debug("Insert FcebookId:" + request.getFacebookId() + " for userId:" + user.getId());
					sendEvent = true;
				}
			}
			FbManager.insertFbFrinds(user.getId(), request.getFriends());
			log.debug("Insert Fcebook Friends Count:" + request.getFriends().size() + " for userId:" + user.getId());
			if (sendEvent && !ProfileManager.isTestProfile(user.getId())) {
				log.debug("Send Fcebook Friends Notification for userId:" + user.getId());
				CopyOpEventSender.sendEvent(new ProfileManageEvent(ProfileLinkCommandEnum.FB_NEW_FRIEND, user.getId()),
											CopyOpEventSender.PROFILE_MANAGE_DESTINATION_NAME);
			}
		} catch (Exception e) {
			log.error("Unable to update fb friends for userId = " + user.getId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: updateFacebookFriends " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static FacebookFriendsMethodResult getFacebookFriends(PagingUserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getFacebookFriends: " + request);
		long beginTime = System.currentTimeMillis();
		FacebookFriendsMethodResult result = new FacebookFriendsMethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		if (request.isResetPaging()) {
			httpRequest.getSession().removeAttribute(FbManager.LAST_FETCHED_FB_ID);
		}
		try {
			log.debug("get Fb Frinds UserIds for user:" + user.getId());
			ArrayList<Long> fbUids = FbManager.getFbFriendsUserIds(user.getId(), httpRequest);
			if (fbUids.size() > 0) {
				log.debug("get and set Fb Frinds Profiles for user:" + user.getId());
				long endTime = System.currentTimeMillis();
				if (log.isDebugEnabled()) {
					log.debug("TIME: getFacebookFriends " + (endTime - beginTime) + "ms");
				}
				List<com.copyop.common.dto.base.Profile> profiles = ProfileManager.getFbProfiles(fbUids);
				boolean testProfile = ProfileManager.isTestProfile(user.getId());
				Iterator<com.copyop.common.dto.base.Profile> iterator = profiles.iterator();
				while (iterator.hasNext()) {
					com.copyop.common.dto.base.Profile profile = iterator.next();
					if ((!testProfile && profile.isTest())
							|| UserStateEnum.STATE_REMOVED == profile.getUserState()) {
						iterator.remove();
					}
				}
				result.setFriends(profiles);
			} else {
				log.debug("Not found Fb Friends for userId:" + user.getId());
			}
		} catch (Exception e) {
			log.error("Unable to get fb friends for userId = " + user.getId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		return result;
	}

	public static UserMethodResult getCopyopUser(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getCopyopUser: " + request);
		long beginTime = System.currentTimeMillis();
		com.anyoption.common.service.results.UserMethodResult aoResult = AnyoptionService.getUser(request, httpRequest);
		int maxCopiersPerAccount = Integer.valueOf(ConfigurationCopyopManager.getCopyopPropertiesValue("MAX_COPIERS_PER_ACCOUNT", "" + 100));
		UserMethodResult result = new UserMethodResult(aoResult, maxCopiersPerAccount);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.error("Can't ger user properly. Error code: " + result.getErrorCode());
			return result;
		}
		try {
			result.setSkinGroupId(SkinsManagerBase.getSkin(request.getSkinId()).getSkinGroup().getId());
			if (result.getUser().getSkinId() == Skin.SKIN_ETRADER || result.getUser().getCountryId() == ConstantsBase.COUNTRY_ID_US ) {
				result.setErrorCode(AnyoptionService.ERROR_CODE_LOGIN_FAILED);
				log.error("Etrader user trying to login in copyop, userId: " + result.getUser().getId() + ", skinId: " + result.getUser().getSkinId());
				MethodResult logoutResult = AnyoptionService.logoutUser(request, httpRequest);
				if (logoutResult.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
					log.warn("Failed logout user. Error code " + logoutResult.getErrorCode());
				}
				return result;
			}
			Profile copyopProfile = ProfileManager.getProfile(result.getUser().getId());
			if (copyopProfile == null) { // create copyop user and logout
				UsersMigrationManager.exportUser(aoResult.getUser().getId());
				copyopProfile = ProfileManager.getProfile(result.getUser().getId());
				if (copyopProfile == null) {
					log.error("After exportUser there is no profile with userId = " + aoResult.getUser().getId());
					result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
					return result;
				}
			}
			if (copyopProfile.getUserState() == UserStateEnum.STATE_REMOVED) {
				boolean res = IssuesManagerBase.updateCopyopUserStatus(	result.getUser().getUserName(), result.getUser().getId(),
																		request.getWriterId(),
																		IssueAction.ISSUE_ACTION_USER_COPYOP_UNREMOVED);
				if (!res) {
					log.debug("Unable to change user state. Logging out user with id: " + result.getUser().getId());
					request.setPassword(result.getUser().getEncryptedPassword());
					MethodResult logoutResult = AnyoptionService.logoutUser(request, httpRequest);
					if (logoutResult.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
						log.warn("Failed logout user. Error code " + logoutResult.getErrorCode());
					}
					result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
				}
			}
			if (!copyopProfile.isAcceptedTermsAndConditions()) {
				request.setPassword(result.getUser().getEncryptedPassword());
				MethodResult logoutResult = AnyoptionService.logoutUser(request, httpRequest);
				if (logoutResult.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
					log.warn("Failed logout user. Error code " + logoutResult.getErrorCode());
				}
				result.setErrorCode(AnyoptionService.ERROR_CODE_NOT_ACCEPTED_TERMS);
				return result;
			}
			result.setCopyopProfile(copyopProfile);
			long type = ConfigurationCopyopManager.getUserRegistrationType(copyopProfile.getTimeCreated());
			result.setCopyAmounts(ConfigurationCopyopManager.getCopyoAmountsCfg(result.getUser().getCurrencyId(), type));

			double[] odds = ConfigurationCopyopManager.getCopyopInvOdds(Calendar.getInstance().getTime());
			result.setOddsWin(odds[0]);
			result.setOddsLose(odds[1]);
			// deprecated
			//result.setOddsWin(Double.parseDouble(ConfigurationCopyopManager.getCopyopPropertiesValue("COPIED_INVESTMENT_ODDS_WIN", "0.6")));
			//result.setOddsLose(Double.parseDouble(ConfigurationCopyopManager.getCopyopPropertiesValue("COPIED_INVESTMENT_ODDS_LOSE", "0.85")));

			result.setSafeModeMaxTrades(Integer.parseInt(ConfigurationCopyopManager.getCopyopPropertiesValue("SAFE_MODE_MAX_TRADES", "5")));
			result.setSafeModeMinCopyUsers(Integer.parseInt(ConfigurationCopyopManager.getCopyopPropertiesValue("SAFE_MODE_MIN_COPY_USERS", "5")));
			result.setSafeModePeriod(Integer.parseInt(ConfigurationCopyopManager.getCopyopPropertiesValue("SAFE_MODE_PERIOD", "48")));
			
			long firstDepositId = aoResult.getUser().getFirstDepositId();
			if(firstDepositId != 0 ) {
				Transaction firstDeposit = TransactionsManagerBase.getTransactionInfo(firstDepositId);
				if(firstDeposit != null) {
					long millis = Calendar.getInstance().getTimeInMillis() - firstDeposit.getTimeCreated().getTime();
					if(TimeUnit.MILLISECONDS.toHours(millis)<result.getSafeModePeriod()) {
							result.setSafeMode(true);
					} else {
						int numberOfStartedCopy = ProfileLinksManager.getProfileLinksHistory(copyopProfile.getUserId(), ProfileLinkTypeEnum.COPY).size();
						if(numberOfStartedCopy < result.getSafeModeMinCopyUsers()) {
							result.setSafeMode(true);
						}
					}
				}else {
					log.error("Exception - first deposit is missing" + firstDepositId + " for userId " + copyopProfile.getUserId());
				}
			} else {
				result.setSafeMode(true);
			}
				
			// right now we don't need this
			// httpRequest.getSession().setAttribute(Constants.BIND_SESSION_PROFILE, copyopProfile);
			AppoxeeManager.createSegment(request.getWriterId(), request.getDeviceId(), result.getUser().getId());
		} catch (Exception e) {
			log.error("Unable to load profile with userId = " + result.getUser().getId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getCopyopUser " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static SkinMethodResult selectSkin(MethodRequest request, HttpServletRequest httpRequest) {
		log.debug("selectSkin: " + request);
		SkinMethodResult result = new SkinMethodResult();
		ArrayList<String> list = new ArrayList<String>();
		long skinId = Skin.SKINS_DEFAULT;
		boolean isRegulated = false;
		long defaultCurrencyId = 2;
		long skinGroupId = 2;
		String currencySymbol = CommonUtil.getMessage("currency.usd", null, new Locale("en"));
		boolean currencyLeftSymbol = true;
		Calendar calendar = Calendar.getInstance();

		User user = (User) httpRequest.getSession().getAttribute("user");
		if (user != null && user.getId() > 0) {
			result.setSkinId(user.getSkinId());
			Skin s = SkinsManagerBase.getSkin(user.getSkinId());
			result.setRegulated(s.isRegulated());
			result.setCurrencyId(user.getCurrencyId());
			result.setSkinGroupId(s.getSkinGroup().getId());
			result.setLoggedIn(true);
			result.setCurrentTime(System.currentTimeMillis());
			result.setDstOffset(String.valueOf(calendar.get(Calendar.DST_OFFSET)));
		} else {
			try {
				if (request.getWriterId() == Writer.WRITER_ID_COPYOP_WEB) {
					long countryId = 0;
					String countryCode = CommonUtil.getCountryCodeByIp(CommonUtil.getIPAddress(httpRequest));
					if (null != countryCode) {
						log.debug("Get Counrty code from IP, Country code: " + countryCode);
						Country c = CountryManagerBase.getCountry(countryCode);
						if (null != c) {
							countryId = c.getId();
						}
					}
					if (countryId != 0) {
						list = SkinsManagerBase.getSkinIdIsRegulatedByCountryAndUrl(countryId, ConstantsBase.COPYOP_URL);
						if (list!=null && !list.isEmpty()){
							skinId = Long.parseLong(list.get(0));
							isRegulated = Boolean.parseBoolean(list.get(1));
							CurrenciesRules currenciesRules = new CurrenciesRules();
							currenciesRules.setCountryId(countryId);
							currenciesRules.setSkinId(skinId);
							defaultCurrencyId = CurrenciesRulesManagerBase.getDefaultCurrencyId(currenciesRules);//Long.parseLong(list.get(2));
							skinGroupId = Long.parseLong(list.get(3));
							currencyLeftSymbol = Boolean.parseBoolean(list.get(4));
							currencySymbol = CommonUtil.getMessage(list.get(5), null, new Locale("en"));
						}
					} else {
						skinId = Skin.SKINS_DEFAULT;
						isRegulated = false;
						defaultCurrencyId = 2;
						skinGroupId = 2;
						currencyLeftSymbol = true;
						currencySymbol = CommonUtil.getMessage("currency.usd", null, new Locale("en"));
					}
				}
				result.setSkinId(skinId);
				result.setRegulated(isRegulated);
				result.setCurrencyId(defaultCurrencyId);
				result.setSkinGroupId(skinGroupId);
				result.setCurrentTime(System.currentTimeMillis());
				result.setCurrencyLeftSymbol(currencyLeftSymbol);
				result.setCurrencySymbol(currencySymbol);
				result.setDstOffset(String.valueOf(calendar.get(Calendar.DST_OFFSET)));
			} catch (Exception e) {
				log.error("Unable to get skin", e);
			}
		}
		return result;
	}
	
	public static MethodResult updateCopyopProfileAvatar(UpdateAvatarMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("updateCopuopProfileAvatar: " + request);
		long beginTime = System.currentTimeMillis();
		MethodResult result = new MethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		validateAvatarURL(request.getAvatar(), result);
		if (result.getErrorCode() == AnyoptionService.ERROR_CODE_SUCCESS) {
			try {
				ProfileManager.updateAvatar(user.getId(), request.getAvatar(), null);
			} catch (Exception e) {
				log.error("Unable to update avatar for userId = " + user.getId(), e);
				result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			}
		}
		if (log.isDebugEnabled()) {
			long endTime = System.currentTimeMillis();
			log.debug("TIME: getCopyopHotTrades " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static LastViewedMethodResult getLastViewedUsers(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getLastViewedUsers: " + request);
		long beginTime = System.currentTimeMillis();
		LastViewedMethodResult result = new LastViewedMethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		try {
			List<LastViewedUser> lastViewedUser =  new ArrayList<LastViewedUser>();
			List<Long> lastViewedUsers = LastViewedManager.getLastViewedUserIds(user.getId());

			if(lastViewedUsers.size() > 0) {
				for(Profile pf : ProfileManager.getProfiles(lastViewedUsers, false)) {
					if (UserStateEnum.STATE_REMOVED != pf.getUserState()) {
						LastViewedUser lvu = new LastViewedMethodResult(). new LastViewedUser(pf.getUserId(),
																								pf.getAvatar(),
																								pf.getNickname());
						lastViewedUser.add(lvu);
					} else {
						log.trace("User [" + pf.getUserId() + "] in [" + UserStateEnum.STATE_REMOVED
																		+ "]. Will be skipped from last viewed list");
					}
				}
			} else {
				log.debug("Not found last viewd users for User:" + user.getId());
			}

			result.setLastViewedUser(lastViewedUser);
		} catch (Exception e) {
			log.error("Unable to get last viewed users - news", e);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getLastViewedUsers " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static MethodResult shareCopyop(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("shareCopyop: " + request);
		long beginTime = System.currentTimeMillis();
		MethodResult result = new MethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		try {
			long shareCount = ProfileCountersManager.getProfileProfileShare(user.getId());
			long minShare = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("SHARE_COPYOP_MIN_COUNT", "2"));
			if (shareCount < minShare) {
				log.debug("The shareCount will increase. Current shareCount:" + shareCount + " for UserID:" + user.getId());
				ProfileCountersManager.increaseProfileShare(user.getId());
				shareCount = shareCount + 1;

				long reciveCoins = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("SHARE_COPYOP_RECIVE_COINS", "5"));
				CoinsManager.updateCoins(user.getId(), reciveCoins, CoinsActionTypeEnum.SHARE, user.getSkinId() + "", request.getWriterId(),  "After " + shareCount + " share");
				log.debug("User:" + user.getId() + " recive:" + reciveCoins + " Coins after:" + shareCount + " shares");
			} else {
				log.debug("User there are enough sharCounts:" + shareCount);
			}
		} catch (Exception e) {
			log.error("Unable to share copyop from userId = " + user.getId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		if (log.isDebugEnabled()) {
			long endTime = System.currentTimeMillis();
			log.debug("TIME: shareCopyop " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static MethodResult rateCopyop(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("rateCopyop: " + request);
		long beginTime = System.currentTimeMillis();
		MethodResult result = new MethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false, httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		try {
			long rateCount = ProfileCountersManager.getProfileProfileRate(user.getId());
			long minRate = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("RATE_COPYOP_MIN_COUNT", "1"));
			if (rateCount < minRate) {
				log.debug("The rateCopyop will increase. Current rateCopyop:" + rateCount + " for UserID:" + user.getId());
				ProfileCountersManager.increaseProfileRate(user.getId());
				rateCount = rateCount + 1;

				if (rateCount == minRate) {
					long reciveCoins = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("RATE_COPYOP_RECIVE_COINS", "10"));
					CoinsManager.updateCoins(user.getId(), reciveCoins, CoinsActionTypeEnum.RATE, user.getSkinId() + "", request.getWriterId(),  "After " + rateCount + " rate");
					log.debug("User:" + user.getId() + " recive:" + reciveCoins + " coins after:" + rateCount + " rate");
				}
			} else {
				log.debug("User there are enough rateCopyop:" + rateCount);
			}
		} catch (Exception e) {
			log.error("Unable to rate copyop from userId = " + user.getId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		if (log.isDebugEnabled()) {
			long endTime = System.currentTimeMillis();
			log.debug("TIME: rateCopyop " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static MethodResult copyopShowOff(ShowOffMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("copyopShowOff: " + request);
		long beginTime = System.currentTimeMillis();
		MethodResult result = new MethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		if (ProfileManager.isTestProfile(user.getId())) {
			log.debug("No show off for test user, userId: " + user.getId());
			return result;
		}
		if (ProfileManager.isFrozenProfile(user.getId())) {
			log.debug("No show off for frozen profile, userId: " + user.getId());
			return result;
		}
		long invId = request.getInvestmentId();
		if (invId <= 0) {
			log.debug("Invalid investmentId = " + invId);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			return result;
		}
		try {
			Investment investment = InvestmentsManagerBase.getInvestmentById(invId, ConstantsBase.PROFILE_LAST_INVESTMENTS_COUNT_LIMIT);
			if (user.getId() != investment.getUserId()) {
				log.debug("The investment does not belong to the user logged in, userId = " + user.getId() + ", investment's userId = "
							+ investment.getUserId());
				result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
				return result;
			}
			if (investment.getHouseResult() > 0) {
				log.debug("This investment is not a winning one, houseResult = " + investment.getHouseResult());
				result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
				return result;
			}

			long profit = (investment.getAmount() - investment.getHouseResult()) - investment.getAmount();
			CopyOpEventSender.sendEvent(new ProfileManageEvent(user.getId(), profit, investment.getMarketId(),
																user.getCurrencyId(), investment.getRate(),investment.getTimeSettled(),
																ProfileLinkCommandEnum.SHOWOFF),
										CopyOpEventSender.PROFILE_MANAGE_DESTINATION_NAME);
		} catch (SQLException e) {
			log.error("Unable to load investment with id = " + invId, e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		} catch (Exception e) {
			log.error("Unable to execute copyopShowOff for userId = " + user.getId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		if (log.isDebugEnabled()) {
			long endTime = System.currentTimeMillis();
			log.debug("TIME: copyopShowOff " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	private static com.anyoption.common.beans.User initUser(String userName, String password, MethodResult result, long skinId,
															boolean isLoginActivity, HttpServletRequest httpRequest) {
		com.anyoption.common.beans.User user = null;
		try {
			user = AnyoptionService.validateAndLoginEncrypt(userName, password, result, skinId, isLoginActivity, httpRequest);
		} catch (ValidatorException ve) {
			log.debug("Problem loading user", ve);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		} catch (SQLException sqle) {
			log.debug("Problem loading user", sqle);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		} catch (Exception e) {
			log.debug("Problem loading user", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		return user;
	}

	private static List<FeedMessage> retrieveFeed(long profileUserId, QueueTypeEnum queueType, Date currentTime, int feedCapacity) {
		long startTime, endTime;

		startTime = System.currentTimeMillis();
		List<FeedMessage> feed = FeedManager.getFeed(profileUserId, queueType, currentTime, feedCapacity);
		endTime = System.currentTimeMillis();
		log.debug("TIME: retrieveFeed: FeedManager.getFeed(long, QueueTypeEnum, Date, int) " + (endTime - startTime) + "ms");

		startTime = System.currentTimeMillis();
		feed.addAll(retrieveSystemFeed(profileUserId, feedCapacity));
		endTime = System.currentTimeMillis();
		log.debug("TIME: retrieveFeed: retrieveSystemFeed(long, int) " + (endTime - startTime) + "ms");

		if (feed.isEmpty()) {
			return feed;
		}

		startTime = System.currentTimeMillis();
		// mix the feed
		Collections.sort(feed, new Comparator<FeedMessage>() {

			@Override
			public int compare(FeedMessage m1, FeedMessage m2) {
				return -m1.getTimeCreated().compareTo(m2.getTimeCreated());
			}
		});
		endTime = System.currentTimeMillis();
		log.debug("TIME: retrieveFeed: Collections.sort(List<T>, Comparator<? super T>) " + (endTime - startTime) + "ms");

		startTime = System.currentTimeMillis();
		ArrayList<FeedMessage> remainingFeed = null;
		if (feed.size() > feedCapacity) {
			remainingFeed = new ArrayList<FeedMessage>(feed.subList(feedCapacity, feed.size()));
			feed.subList(feedCapacity, feed.size()).clear();
		}
		Iterator<FeedMessage> feedIterator = feed.iterator();
		while (feedIterator.hasNext()) {
			FeedMessage message = feedIterator.next();
			loadMessageDetails(message, true);
			String frozenStr = message.getProperties().get(FeedMessage.PROPERTY_KEY_USER_FROZEN);
			if (Boolean.valueOf(frozenStr)) {
				feedIterator.remove();
			}
		}
		if (remainingFeed != null) {
			Iterator<FeedMessage> remainingFeedIterator = remainingFeed.iterator();
			while (feed.size() < feedCapacity && remainingFeedIterator.hasNext()) {
				FeedMessage message = remainingFeedIterator.next();
				loadMessageDetails(message, true);
				String frozenStr = message.getProperties().get(FeedMessage.PROPERTY_KEY_USER_FROZEN);
				if (!Boolean.valueOf(frozenStr)) {
					feed.add(message);
				}
			}
		}
		endTime = System.currentTimeMillis();
		log.debug("TIME: retrieveFeed: all loadMessageDetails calls " + (endTime - startTime) + "ms");
		return feed;
	}

	private static List<FeedMessage> retrieveSystemFeed(long profileUserId, int requiredSize) {
		long startTime, endTime;
		List<FeedMessage> result = new ArrayList<FeedMessage>();
		int limit = requiredSize * 2;
		Date timeCreated = new Date();
		long timeLastSystemMessage = 0;
		long millisecondsInterval = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue(	"SYSTEM_FEED_MILLISECONDS_INTERVAL",
																									"5000"));
		do {
			startTime = System.currentTimeMillis();
			List<FeedMessage> systemFeed = FeedManager.getFeed(	FeedMessage.HOT_SYSTEM_FEED_USER_ID, QueueTypeEnum.SYSTEM, timeCreated,
																limit);
			endTime = System.currentTimeMillis();
			log.debug("TIME: retrieveSystemFeed: FeedManager.getFeed " + (endTime - startTime) + "ms");

			if (systemFeed.isEmpty()) { // in case we don't have messages
				break;
			}
			List<Long> relatedProfileIds = ProfileLinksManager.getProfileLinkIdsByTypes(profileUserId,
																						Arrays.asList(	ProfileLinkTypeEnum.COPY,
																										ProfileLinkTypeEnum.WATCH));
			relatedProfileIds.add(profileUserId);
			startTime = System.currentTimeMillis();
			for (FeedMessage message : systemFeed) {
				String userIdStr = message.getProperties().get(FeedMessage.PROPERTY_KEY_USER_ID);
				if (userIdStr != null) {
					try {
						Long userId = new Long(userIdStr);
						if (!relatedProfileIds.contains(userId)) {
							if (UpdateTypeEnum.getById(message.getMsgType()).getPriority() == PriorityEnum.CATEGORY_3) {
								if (new Date(timeLastSystemMessage - millisecondsInterval).after(message.getTimeCreated())
										|| timeLastSystemMessage == 0) {
									result.add(message);
									timeLastSystemMessage = message.getTimeCreated().getTime();
								}
							} else {
								result.add(message);
							}
							if (result.size() >= requiredSize) {
								break;
							}
						}
					} catch (NumberFormatException nfe) {
						log.warn("Unable to parse to long string = " + userIdStr, nfe);
					}
				}
			}
			endTime = System.currentTimeMillis();
			log.debug("TIME: retrieveSystemFeed: Algorithm " + (endTime - startTime) + "ms");
			Date first = systemFeed.get(0).getTimeCreated();
			Date last = systemFeed.get(systemFeed.size() - 1).getTimeCreated();
			if (first.compareTo(last) == 0) {
				timeCreated = new Date(last.getTime() - 1);
			} else {
				timeCreated = new Date(last.getTime());
			}
		} while (result.size() < requiredSize);

		if (result.size() < requiredSize) {
			requiredSize = result.size();
		}
		return result.isEmpty() ? result : result.subList(0, requiredSize);
	}

	private static void loadMessageDetails(FeedMessage message, boolean loadProfileRelations) {
		String userIdStr = message.getProperties().get(FeedMessage.PROPERTY_KEY_USER_ID);
		if (userIdStr != null) {
			try {
				Long userId = new Long(userIdStr);
				ProfileManager.loadProfileDetailsForMessage(userId, message);
				if (loadProfileRelations) {
					ProfileCountersManager.loadAllProfileRelations(userId, message);
				}
			} catch (NumberFormatException nfe) {
				log.warn("Unable to parse to long string = " + userIdStr, nfe);
			}
		}
		String destUserId = message.getProperties().get(FeedMessage.PROPERTY_KEY_DEST_USER_ID);
		if (destUserId != null) {
			try {
				long lDestUserId = Long.parseLong(destUserId);
				ProfileManager.loadDestProfileDetailsForMessage(lDestUserId, message);
			} catch (NumberFormatException nfe) {
				log.warn("Unable to parse to long string = " + destUserId, nfe);
			}
		}
	}

	private static List<FeedMessage> loadHotTableData(UpdateTypeEnum msgType) {
		List<FeedMessage> fml = FeedManager.getLastHotTableData(msgType);
		Iterator<FeedMessage> fmlIterator = fml.iterator();
		while (fmlIterator.hasNext()) {
			FeedMessage feedMessage = fmlIterator.next();
			String userIdStr = feedMessage.getProperties().get(FeedMessage.PROPERTY_KEY_USER_ID);
			if (userIdStr != null) {
				try {
					Long userId = new Long(userIdStr);
					UserStateEnum userState = ProfileManager.getUserState(userId);
					if (null != userState && userState == UserStateEnum.STATE_REMOVED) {
						fmlIterator.remove();
						continue;
					}
					ProfileManager.loadProfileDetailsForMessage(userId, feedMessage);
					String frozenStr = feedMessage.getProperties().get(FeedMessage.PROPERTY_KEY_USER_FROZEN);
					if (Boolean.valueOf(frozenStr)) {
						fmlIterator.remove();
					} else {
						ProfileCountersManager.loadAllProfileRelations(userId, feedMessage);
					}
				} catch (NumberFormatException nfe) {
					log.warn("Unable to parse to long string = " + userIdStr, nfe);
				}
			}
		}
		return fml;
	}

	private static void validateAvatarURL(String avatar, MethodResult result) {
		ArrayList<ErrorMessage> msgs = new ArrayList<ErrorMessage>();
		if (null != avatar
				&& (avatar.startsWith(CommonUtil.getProperty(ConstantsBase.SERVER_URL_HOME_PAGE))
						|| avatar.startsWith(CommonUtil.getProperty(ConstantsBase.SERVER_URL_HOME_PAGE_HTTPS))
						|| avatar.startsWith("https://graph.facebook.com"))) {
			return;
		} else {
			result.setErrorCode(AnyoptionService.ERROR_CODE_INVALID_INPUT);
			ErrorMessage er = new ErrorMessage("avatar","Problem with avatar validation");
			msgs.add(er);
			result.addUserMessages(msgs);
		}
	}

	private static FeedMessage getSpecialOfferFeed(UserMethodRequest request, com.anyoption.common.beans.User user, int queueType) throws SQLException {
		FeedMessage specialOffer = null;
		Map<String, String> specialOfferProperties = null;
		BonusUsers bonusUser = BonusManagerBase.getLastGrantedActiveUserBonus(user.getId());
		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
		if (bonusUser != null) {
			String utcOffset = CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset());
			BonusUserFormater.initFormattedValues(bonusUser, locale, utcOffset);

			specialOfferProperties = new HashMap<String, String>();
			specialOfferProperties.put(FeedMessage.PROPERTY_BONUS_TYPE_ID, String.valueOf(bonusUser.getTypeId()));
			specialOfferProperties.put(FeedMessage.PROPERTY_BONUS_DESCRIPTION, bonusUser.getBonusDescriptionTxt());
		}

		if (specialOfferProperties == null) {
			long shareCount = ProfileCountersManager.getProfileProfileShare(user.getId());
			long minShare = new Long (ConfigurationCopyopManager.getCopyopPropertiesValue("SHARE_COPYOP_MIN_COUNT", "2"));
			if( shareCount < minShare){
				specialOfferProperties = new HashMap<String, String>();
				specialOfferProperties.put(FeedMessage.PROPERTY_BONUS_TYPE_ID, String.valueOf(0));

				String msg = "copyop.special.offer.share.tab";
				if(request.getWriterId() == Writer.WRITER_ID_COPYOP_WEB){
					msg = "copyop.special.offer.share.click";
				}
				specialOfferProperties.put(FeedMessage.PROPERTY_BONUS_DESCRIPTION,  CommonUtil.getMessage( msg, null, locale));
			}
		}

		if (specialOfferProperties != null) {
			specialOffer = new FeedMessage();
			specialOffer.setUserId(user.getId());
			UUID timeCreatedUUID = UUIDs.timeBased();
			specialOffer.setTimeCreatedUUID(timeCreatedUUID);
			specialOffer.setTimeCreated(new Date(UUIDs.unixTimestamp(timeCreatedUUID)));
			specialOffer.setQueueType(queueType);
			specialOffer.setMsgType(UpdateTypeEnum.SP_TODAYS_OFFER.getId());
			specialOffer.setProperties(specialOfferProperties);
		}
		return specialOffer;
	}

	public static MethodResult getSystemChecks(SystemChecksRequest request, HttpServletRequest httpRequest) {
		log.debug("systemChecks: " + request);
		String opperation=request.getService();
		MethodResult result = new MethodResult();
		result.setApiCode("undefined method");
		if (opperation.equalsIgnoreCase(ConstantsBase.DB_CHECK)) {
			result.setApiCode( CommonUtil.getRunDbCheck());
		}
		return result;
	}

	public static LastLoginSummaryMethodResult getLastLoginSummary(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getLastLoginSummary: " + request);
		long beginTime = System.currentTimeMillis();
		LastLoginSummaryMethodResult result = new LastLoginSummaryMethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}

		try {
			List<LastLoginSummaryMethodResult.TopLogTrader> traderLogList = new ArrayList<LastLoginSummaryMethodResult.TopLogTrader>();

			Long profitSum = 0l;
			Long topTraderProfitSum = 0l;
			int totalCounts = 0;
			int topTraderTotalCounts = 0;
			LastLoginSummaryMethodResult.TopLogTrader totalRecord = result.new TopLogTrader(0l, null, null, 0, profitSum, false, false, false);
			traderLogList.add(totalRecord);

			int br = 0;
			ArrayList<CopiedInvestment> ci = InvestmentsManagerBase.getCopiedInvFromLastLogout(user.getId());
			for(CopiedInvestment inv : ci) {
				profitSum = profitSum + inv.getProfit();
				totalCounts = totalCounts + inv.getCopiedCount();

				if(br < result.TOP_TRADERS_COUNT) {
					Profile p = new Profile();
					ProfileManager.loadProfileDetailsForTrader(inv.getCopyFromUserId(), p);
					if (UserStateEnum.STATE_REMOVED == p.getUserState()) {
						log.trace("User [" + inv.getCopyFromUserId() + "] in [" + UserStateEnum.STATE_REMOVED + "]. Will not be part of summary");
						continue;
					}

					boolean showIncreaseAmount = false;
					if(inv.getProfit() >= ConfigurationCopyopManager.getIncreaseAmountBtnShowCfg().get(user.getCurrencyId())) {
						showIncreaseAmount = true;
					}

					LastLoginSummaryMethodResult.TopLogTrader traderLog = result.new TopLogTrader(inv.getCopyFromUserId(), 
																								  p.getNickname(), 
																								  p.getAvatar(), 
																								  inv.getCopiedCount(), 
																								  inv.getProfit() / 100f, 
																								  showIncreaseAmount,
																								  inv.isAfterLastLogOff(),
																								  p.isFrozen());
					topTraderProfitSum = topTraderProfitSum + inv.getProfit();
					topTraderTotalCounts = topTraderTotalCounts + inv.getCopiedCount();
					traderLogList.add(traderLog);
				}
				br ++;
			}

			if(traderLogList.size() > 1){
				//Set totalRec
				traderLogList.get(0).setProfit(profitSum / 100f);
				traderLogList.get(0).setAfterLastLogOff(ci.get(0).isAfterLastLogOff());

				//Add othersRec
				if(ci.size() > result.TOP_TRADERS_COUNT){
					LastLoginSummaryMethodResult.TopLogTrader otherRecords = result.new TopLogTrader(1l, null, null, totalCounts - topTraderTotalCounts , 
																													 (profitSum - topTraderProfitSum) / 100f, 
																													 false, 
																													 ci.get(0).isAfterLastLogOff(),
																													 false);
					traderLogList.add(otherRecords);
				}

				result.setTopLogTraders(traderLogList);
			}

			long endTime = System.currentTimeMillis();
			if (log.isDebugEnabled()) {
				log.debug("TIME: getLastLoginSummary " + (endTime - beginTime) + "ms");
			}
		} catch (Exception e) {
			log.debug("Unable to get LastLoginSummary with userId = " + user.getId(), e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		return result;
	}
	
	public static MethodResult sendAffiliateEmail(EmailMethodRequest request) {
		MethodResult result = new MethodResult();
		CommonUtil.sendEmailMsg(request.getFrom(), CommonUtil.getConfig("email.new.affiliates"), CommonUtil.getConfig("email.new.affiliates.subject"), request.getMailBody());
		return result;
	}
	
	public static CopyStepsEncourageMethodResult getCopyStepsEncourageToolTip(CopyStepsEncourageMethodRequest request, HttpServletRequest httpRequest) {
		
		User user = (User) httpRequest.getSession().getAttribute("user");
		CopyStepsEncourageMethodResult result = new CopyStepsEncourageMethodResult();

		if (user == null || user.getId() != request.getUserId()) {
			result.setErrorCode(1);
			result.setMessage("User is null or is undefined!");
			return result;
		}

		int countTrue = 0;
		long profitFromLastFiveTrans = 0;
		long profitFromLastTenTrans = 0;
		double profitFromLastFiveTransD = 0.0;
		double profitFromLastTenTransD = 0.0;
		int listSize = 0;
		int copyStep = ConstantsBase.COPYOP_FULLY_COPY_STEP_SECOND;// get from Cassandra DB in future releases, for now is hardcoded
		String message = "";
		long stepAmount = 0;
		Profile copiedUserProfile = null;
		ArrayList<Long> stepAmounts = new ArrayList<Long>();
		double stepAmountCalculatedOddsLose = 0;
		double stepAmountCalculatedOddWin = 0;
		double[] odds = new double[2];
		if (request.getStepAmount() > 0) {
			try {
				odds = ConfigurationCopyopManager.getCopyopLastInvOdds();
			} catch (SQLException ex) {
				result.setErrorCode(1);
				result.setMessage("Can't load copyop odds !");
				return result;
			}
			stepAmountCalculatedOddsLose = request.getStepAmount() * odds[1];
			stepAmountCalculatedOddWin = request.getStepAmount() * odds[0];
		} else {
			result.setErrorCode(1);
			result.setMessage("Step amount is not valid");
			return result;
		}
		
		ProfileInvResultSelf profileInvResSelf = ProfileStatisticsManager.getProfileInvResultSelf(request.getCopiedUserId(), ConstantsBase.LAST_TEN_INVESTMENTS, UserStateEnum.STATE_REGULAR);
		ArrayList<Boolean> resultsList = profileInvResSelf.getResults();
		copiedUserProfile = ProfileManager.getProfile(request.getCopiedUserId());
		
		if(copiedUserProfile == null){
			result.setErrorCode(1);
			result.setMessage("Can not load COPIED user profile.");
			return result;
		}
		
		if (resultsList.isEmpty()) {
			result.setErrorCode(1);
			result.setMessage("List is empty");
		} else {
			listSize = resultsList.size();
			if (listSize < ConstantsBase.LAST_FIVE_INVESTMENTS) {
				result.setErrorCode(1);
				result.setMessage("List size is less than 5 expired investments");
			} else {

				for (int i = 0; i < ConstantsBase.LAST_FIVE_INVESTMENTS; i++) {
					if (resultsList.get(i)) {
						countTrue += 1;
					}
				}

				if (countTrue < ConstantsBase.MIN_SUCCESSFUL_LAST_INVESTMENTS) {
					result.setErrorCode(1);
					result.setMessage("Can not reach more than 60% profit in both (5 or 10) last expired investments)");
					return result;
				} 
				
				if (countTrue > ConstantsBase.SIXTY_PERCENT_HIT_IN_LAST_FIVE_INVESTMENTS) {
					if (copyStep == ConstantsBase.COPYOP_FULLY_COPY_STEP_SECOND) {
						profitFromLastFiveTrans = (long) Math.round(((countTrue * stepAmountCalculatedOddWin) - ((ConstantsBase.LAST_FIVE_INVESTMENTS - countTrue) * stepAmountCalculatedOddsLose)) * 100);
					}
					if (copyStep == ConstantsBase.COPYOP_FULLY_COPY_STEP_THIRD) {
						profitFromLastFiveTrans = (long) Math.round(((countTrue * stepAmountCalculatedOddWin) - ((ConstantsBase.LAST_FIVE_INVESTMENTS - countTrue) * stepAmountCalculatedOddsLose)) * 100);
					}
				}

				if (listSize == ConstantsBase.LAST_TEN_INVESTMENTS) {
					countTrue = 0;
					for (int i = 0; i < listSize; i++) {
						if (resultsList.get(i)) {
							countTrue += 1;
						}
					}

					if (countTrue > ConstantsBase.SIXTY_PERCENT_HIT_IN_LAST_TEN_INVESTMENTS) {
						if (copyStep == ConstantsBase.COPYOP_FULLY_COPY_STEP_SECOND) {
							profitFromLastTenTrans = (long) Math.round(((countTrue * stepAmountCalculatedOddWin) - ((ConstantsBase.LAST_TEN_INVESTMENTS - countTrue) * stepAmountCalculatedOddsLose)) * 100);
						}
						if (copyStep == ConstantsBase.COPYOP_FULLY_COPY_STEP_THIRD) {
							
							profitFromLastTenTrans = (long) Math.round(((countTrue * stepAmountCalculatedOddWin) - ((ConstantsBase.LAST_TEN_INVESTMENTS - countTrue) * stepAmountCalculatedOddsLose)) * 100);
						}
					}
				}
				
				if (profitFromLastFiveTrans == 0 && profitFromLastTenTrans == 0) {
					result.setErrorCode(1);
					result.setMessage("Profit from last 5 and 10 expired investments is 0.");
					return result;
				}
				
				try {
					Profile copyopProfile = ProfileManager.getProfile(request.getUserId());
					long type = ConfigurationCopyopManager.getUserRegistrationType(copyopProfile.getTimeCreated());
					stepAmounts = ConfigurationCopyopManager.getCopyoAmountsCfg(user.getCurrencyId(), type);

					if (stepAmounts.isEmpty()) {
						log.error("Step amount list is empty.");
						result.setErrorCode(1);
						result.setMessage("Step amount list is empty.");
						return result;
					}
				} catch (SQLException ex) {
					log.error("Can not get list of all step amounts ", ex);
					result.setErrorCode(1);
					result.setMessage("Can not get list of all step amounts.");
					return result;
				}
				
				if (profitFromLastFiveTrans == profitFromLastTenTrans || profitFromLastFiveTrans < profitFromLastTenTrans) {
					profitFromLastTenTransD = profitFromLastTenTrans;
					
					if (copyStep == ConstantsBase.COPYOP_FULLY_COPY_STEP_SECOND) {
						stepAmount = stepAmounts.get(ConstantsBase.COPYOP_FULLY_COPY_STEP_SECOND)/100;//ProfileHelper.getUserConvertedAmount(user.getCurrencyId(), ConstantsBase.CURRENCY_BASE_ID, 50);// convert copy stepAmount
																															// from USD to user currency
						message = getToolTipMessage(user, copiedUserProfile.getNickname(), stepAmount, profitFromLastTenTransD, ConstantsBase.LAST_TEN_INVESTMENTS);
						
						result.setFullyCopyStep(ConstantsBase.COPYOP_FULLY_COPY_STEP_THIRD);
						
					} else if (copyStep == ConstantsBase.COPYOP_FULLY_COPY_STEP_THIRD) {
						stepAmount = stepAmounts.get(ConstantsBase.COPYOP_FULLY_COPY_STEP_THIRD)/100;//ProfileHelper.getUserConvertedAmount(user.getCurrencyId(), ConstantsBase.CURRENCY_BASE_ID, 100);// convert copy stepAmount
																															// from USD to user currency
						message = getToolTipMessage(user, copiedUserProfile.getNickname(), stepAmount, profitFromLastTenTransD, ConstantsBase.LAST_TEN_INVESTMENTS);
						
						result.setFullyCopyStep(ConstantsBase.COPYOP_FULLY_COPY_STEP_FOURTH);
					}
					
					result.setErrorCode(AnyoptionService.ERROR_CODE_SUCCESS);
					result.setMessage(message);
					if (user.getCurrencyId() == ConstantsBase.CURRENCY_BASE_ID) {
						result.setStepAmount(user.getCurrencySymbol() + stepAmount);
						result.setProfitAmount(user.getCurrencySymbol() + profitFromLastTenTransD/100);
					} else {
						result.setStepAmount(stepAmount + user.getCurrencySymbol());
						result.setProfitAmount(profitFromLastTenTransD/100 + user.getCurrencySymbol());
					}

				} else if (profitFromLastFiveTrans > profitFromLastTenTrans) {
				profitFromLastFiveTransD = profitFromLastFiveTrans;
					if (copyStep == ConstantsBase.COPYOP_FULLY_COPY_STEP_SECOND) {
						stepAmount = stepAmounts.get(ConstantsBase.COPYOP_FULLY_COPY_STEP_SECOND)/100;//ProfileHelper.getUserConvertedAmount(user.getCurrencyId(), ConstantsBase.CURRENCY_BASE_ID, 50);// convert copy stepAmount
																															// from USD to user currency
						message = getToolTipMessage(user, copiedUserProfile.getNickname(), stepAmount, profitFromLastFiveTransD, ConstantsBase.LAST_FIVE_INVESTMENTS);
						
						result.setFullyCopyStep(ConstantsBase.COPYOP_FULLY_COPY_STEP_THIRD);
					} else if (copyStep == ConstantsBase.COPYOP_FULLY_COPY_STEP_THIRD) {
						stepAmount = stepAmounts.get(ConstantsBase.COPYOP_FULLY_COPY_STEP_THIRD)/100;//ProfileHelper.getUserConvertedAmount(user.getCurrencyId(), ConstantsBase.CURRENCY_BASE_ID, 100);//convert copy stepAmount
																															// from USD to user currency
						message = getToolTipMessage(user, copiedUserProfile.getNickname(), stepAmount, profitFromLastFiveTransD, ConstantsBase.LAST_FIVE_INVESTMENTS);
						
						result.setFullyCopyStep(ConstantsBase.COPYOP_FULLY_COPY_STEP_FOURTH);
					}
					
					result.setErrorCode(AnyoptionService.ERROR_CODE_SUCCESS);
					result.setMessage(message);
					if (user.getCurrencyId() == ConstantsBase.CURRENCY_BASE_ID) {
						result.setStepAmount(user.getCurrencySymbol() + stepAmount);
						result.setProfitAmount(user.getCurrencySymbol() + profitFromLastFiveTransD/100);
					} else {
						result.setStepAmount(stepAmount + user.getCurrencySymbol());
						result.setProfitAmount(profitFromLastFiveTransD/100 + user.getCurrencySymbol());
					}
				}
			}
		}
		return result;
	}
	
	private static String getToolTipMessage(User user, String userNickName, long stepAmount, double profit, int lastInvestments) {
		String message = "";
		Object[] params = new Object[6];

		if (user.getSkinId() == Skin.SKIN_ENGLISH ||
			user.getSkinId() == Skin.SKIN_API ||
			user.getSkinId() == Skin.SKIN_REG_EN ||
			user.getSkinId() == Skin.SKIN_TURKISH ||
			user.getSkinId() == Skin.SKIN_RUSSIAN ||
			user.getSkinId() == Skin.SKIN_DUTCH_REG ||
			user.getSkinId() == Skin.SKIN_SWEDISH_REG) {
			
			params[0] = userNickName;
			params[1] = lastInvestments;
		} else {
			params[0] = lastInvestments;
			params[1] = userNickName;
		}
		if (user.getCurrencyId() == ConstantsBase.CURRENCY_BASE_ID) {
			params[2] = user.getCurrencySymbol();
			params[3] = stepAmount;
			params[4] = user.getCurrencySymbol();
			params[5] = profit / 100;
			message = CommonUtil.getMessage(user.getLocale(), "tool.tip.message", params);
		} else {
			params[2] = stepAmount;
			params[3] = user.getCurrencySymbol();
			params[4] = profit / 100;
			params[5] = user.getCurrencySymbol();
			message = CommonUtil.getMessage(user.getLocale(), "tool.tip.message", params);
		}
		return message;
	}
	
	public static FavMarketsMethodResult getFavMarkets(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getFavMarkets: " + request);
		long beginTime = System.currentTimeMillis();
		FavMarketsMethodResult result = new FavMarketsMethodResult();
		com.anyoption.common.beans.User user = initUser(request.getUserName(), request.getPassword(), result, request.getSkinId(), false,
														httpRequest);
		if (result.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) {
			log.debug("Unable to load user");
			return result;
		}
		
		List<Long> lastMarkets = ProfileStatisticsManager.getLastSelfInvMarkets(user.getId());
		Map<Long, Integer> marketSize = new HashMap<Long, Integer>();
		
		for(int i =0 ;i<lastMarkets.size();i++) {
			Long market = lastMarkets.get(i);
			if(marketSize.get(market) == null ) {
				marketSize.put(market, Collections.frequency(lastMarkets, market));
			}
		}
		Map<Long, Integer> marketSizeSortedByValue = sortByValue(marketSize);
	
		ArrayList<Long> tenFavoritMarkets = new ArrayList<Long>();
		int i =0 ;
		for(Long market:marketSizeSortedByValue.keySet()) {
			tenFavoritMarkets.add(market);
			i++;
			if(i>=10) {
				break;
			}
		}
		
		result.setFavoriteMarkets(tenFavoritMarkets);
		
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getFavMarkets " + (endTime - beginTime) + "ms");
		}
		
		return result;
	}
	
	public static HolidayMessageResult getUserHolidayMessage(MethodRequest request) {
		log.debug("getUserHolidayMessage" + request);
		long beginTime = System.currentTimeMillis();
		HolidayMessageResult result = new HolidayMessageResult();
		Message message = null;
		try {
			if (request.getSkinId() > 0) {
				message = MessagesManager.getMessage(Message.WEB_SCREEN_HOME, request.getSkinId());
			}
		} catch (Exception e) {
			log.error("Cannot get message for user! " + e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		result.setMessage(message);
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getUserHolidayMessage " + (endTime - beginTime) + "ms");
		}
		return result;
	}
	
//	public static MarketsNamesMethodResult getMarketsNames(MethodRequest request, HttpServletRequest httpRequest) {
//		log.debug("getMarketsNames: " + request);
//		long beginTime = System.currentTimeMillis();
//		MarketsNamesMethodResult result = new MarketsNamesMethodResult();
//		try {
//			Hashtable<Long, Market> table = MarketsManagerBase.getAll();
//			HashMap<Long, String> marketList = new HashMap<Long, String>();
//			Set<Long> keys = table.keySet();
//			Iterator<Long> iter = keys.iterator();
//			while ( iter.hasNext() ) {
//				Long marketId = (Long)iter.next();
//				marketList.put(marketId, table.get(marketId).getName());
//			}
//			result.setMarketsList(marketList);
//			
//		} catch (SQLException ex) {
//			log.error("Can not get market names: ", ex);
//			result.setErrorCode(1);
//			return result;
//		}
//		long endTime = System.currentTimeMillis();
//		if (log.isDebugEnabled()) {
//			log.debug("TIME: getMarketsNames " + (endTime - beginTime) + "ms");
//		}
//		
//		return result;
//	}
	
	private static <K, V extends Comparable<V>> Map<K, V> sortByValue(Map<K, V> map) {
		List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(map.entrySet());
		
	    Collections.sort(list, new Comparator<Map.Entry<K, V>>() {

			@Override
			public int compare(Entry<K, V> o1, Entry<K, V> o2) {
				return o2.getValue().compareTo(o1.getValue());
			}

	    });

	   Map<K, V> result = new LinkedHashMap<K, V>();
	   for (Iterator<Entry<K, V>> it = list.iterator(); it.hasNext();) {
	       Map.Entry<K, V> entry = (Map.Entry<K, V>)it.next();
	       result.put(entry.getKey(), entry.getValue());
	   }
	   
	   return result;
	} 
	
	// TODO check all the methods to not make empty queries(e.g. empty lists)
	// TODO refresh profile
	// TODO log timestamps on method entry and exit and the execution time for the cassandra queries
	// TODO add javadoc and relevant comments
}