package com.copyop.json.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.copyop.common.managers.ProfileManager;

public class ShowAvatarServiceServlet  extends HttpServlet {

	
    private static final long serialVersionUID = -6233831873948879567L;
    private static final Logger log = Logger.getLogger(ShowAvatarServiceServlet.class);  
    private static final String slash = "/";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        String uri = request.getRequestURI();
        if (log.isDebugEnabled()) {
            log.debug("URI requested: " + uri);
        }
        
        int lastSlash = uri.lastIndexOf(slash);
        long userId = 0;
        if (lastSlash != -1 && lastSlash < uri.length()) {
        	String requestUser = uri.substring(lastSlash + 1);
        	try {
        		userId =  new Long(requestUser);
			} catch (Exception e) {
				log.error("Can't get userId from URL ", e);
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return;
			}
        	
        	String fileName = ProfileManager.getAvatarFileName(userId);
        	if(fileName == null){
        		log.debug("Can't finde fileName for userId:" + userId);
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return;
        	}
        	File file = new File(fileName);
			FileInputStream fis = new FileInputStream(file);

			long length = file.length();

			if (null == fis || length == 0) {  // file not found
                if (log.isDebugEnabled()) {
                    log.debug("404 for " + fileName);
                }
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
		    }

			 //	Create the byte array to hold the data
	         byte[] bytes = new byte[(int)length];

	         // Read in the bytes
	         int offset = 0;
	         int numRead = 0;
	         while ( (offset < bytes.length) &&
	        		 	( (numRead = fis.read(bytes, offset, bytes.length-offset)) >= 0) ) {

	            offset += numRead;
	        }

	        // Ensure all the bytes have been read in
	        if (offset < bytes.length) {
	           log.warn("Not all bytes have been readed!");
	        }
	        fis.close();
	        
            response.setHeader("Cache-control", "max-age=60, must-revalidate");
            response.setContentType("image/jpeg");
            response.setContentLength(bytes.length);

            OutputStream os = response.getOutputStream();
            os.write(bytes);
            os.flush();
            os.close();        	
        }
    }     
}