package com.copyop.json.service;

import java.io.BufferedInputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.log4j.Logger;

import com.anyoption.beans.User;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.service.AnyoptionService;
import com.anyoption.json.util.JsonExclusionStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class UploadAvatarServiceServlet  extends HttpServlet {

	
    private static final long serialVersionUID = -6233831873948879567L;
    private static final Logger log = Logger.getLogger(UploadAvatarServiceServlet.class);
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {      
        log.debug("doPost");
		
		MethodResult result = new MethodResult();
		User user = null;
		result.setErrorCode(AnyoptionService.ERROR_CODE_SUCCESS);
        try {			
				user = (User) request.getSession().getAttribute("user");
				
				if(user == null){
					log.debug("User is missing in Session");
					result.setErrorCode(AnyoptionService.ERROR_CODE_LOGIN_FAILED); 
					result.addErrorMessage("", "User login failed"); 
					response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				}
				
				String partName = "";
				String contentType ="";
				for (Part part : request.getParts()) {
					partName = part.getName();
					contentType = part.getContentType();
				}
				
				if(result.getErrorCode() == AnyoptionService.ERROR_CODE_SUCCESS){
					if(!UploadAvatarService.mimeTypes.contains(contentType)){
						log.debug(contentType + " mime type not found! File not save!");
						result.setErrorCode(AnyoptionService.ERROR_CODE_INVALID_INPUT);
						result.addErrorMessage("", "Inavlid file type");
						response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
					}
				}
				
				if (result.getErrorCode() == AnyoptionService.ERROR_CODE_SUCCESS) {
					BufferedInputStream bis = new BufferedInputStream(request.getPart(partName).getInputStream());
					result = UploadAvatarService.uploadFile(bis, user);
				}
		
        } catch (IllegalStateException e) {
			log.error("Can't upload file! The file too large!");		
			result.setErrorCode(AnyoptionService.ERROR_CODE_FILE_TOO_LARGE); 
			result.addErrorMessage("", "The file too large");			
			result.addUserMessage("", "copyop.avatar.err.file.too.large");
        }
			
		
		
		Gson gson = new GsonBuilder().setExclusionStrategies(new JsonExclusionStrategy()).serializeNulls().create();
    	String jsonResponse = gson.toJson(result);
        if (null != jsonResponse && jsonResponse.length() > 0) {
        	byte[] data = jsonResponse.getBytes("UTF-8");
        	OutputStream os = response.getOutputStream();
        	response.setHeader("Content-Type", "application/json");
        	response.setHeader("Content-Length", String.valueOf(data.length));
        	os.write(data, 0, data.length);
        	os.flush();
        	os.close();
        }
    }    
//	
//	private static String getFileName(Part filePart)
//	{
//	    String header = filePart.getHeader("content-disposition");
//	    if(header == null)
//	        return null;
//	    for(String headerPart : header.split(";"))
//	    {
//	        if(headerPart.trim().startsWith("filename"))
//	        {
//	            return headerPart.substring(headerPart.indexOf('=') + 1).trim().replace("\"", "");
//	        }
//	    }
//	    return null;
//	}
}