package com.copyop.json.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Skin;
import com.anyoption.common.beans.User;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.service.AnyoptionService;
import com.anyoption.managers.SkinsManagerBase;
import com.anyoption.util.CommonUtil;
import com.anyoption.util.ConstantsBase;
import com.copyop.common.managers.ProfileManager;

public class UploadAvatarService {

	private static final int FILE_LIMIT = 1000000;
	
    public static Set<String> mimeTypes = new HashSet<String>();
    static {
    	mimeTypes.add("image/gif");
    	mimeTypes.add("image/png");
    	mimeTypes.add("image/jpeg");	
    }
	public static final Logger log = Logger.getLogger(UploadAvatarService.class);
	
	public  static MethodResult uploadFile(BufferedInputStream bis, User user){
		MethodResult response = new MethodResult();
		Skin s = SkinsManagerBase.getSkin(user.getSkinId());
		Locale locale = new Locale(LanguagesManagerBase.getLanguage(s.getDefaultLanguageId()).getCode());
		BufferedOutputStream bos = null;
		FileInputStream in = null;
		
		try {
			String fileNamePath = getSubDirName(user.getId()) + user.getId();
			java.io.File targetFile = new java.io.File(fileNamePath); // destination
			
			bos = new BufferedOutputStream(new FileOutputStream(targetFile));
			int theChar;
			boolean fileTooLarge = false;
			while ((theChar = bis.read()) != -1) {
				bos.write(theChar);
				if (targetFile.length() > FILE_LIMIT) {
					fileTooLarge = true;
					break;
				}
			}
			if (fileTooLarge) {
				targetFile.delete();
				bos.close();
				bis.close();
				throw new IllegalStateException("The Large File! Size is:" + targetFile.length());
			}
			in = new FileInputStream(targetFile);
			in.read();
			in.close();	

			String avatar = CommonUtil.getProperty(ConstantsBase.SERVER_URL_AVATAR) + user.getId();
			ProfileManager.updateAvatar(user.getId(), avatar, fileNamePath);
			response.setErrorCode(AnyoptionService.ERROR_CODE_SUCCESS); 
			response.addErrorMessage("", avatar);
			log.debug("UserId:" + user.getId() + " upload user avatar");
	
		} catch (IllegalStateException ie) {
			log.error("Can't upload file! The file too large!");		
			response.setErrorCode(AnyoptionService.ERROR_CODE_FILE_TOO_LARGE); 
			response.addErrorMessage("", "The file too large");
			response.addUserMessage("", CommonUtil.getMessage("copyop.avatar.err.file.too.large", null, locale));
		} catch (Exception e) {
			log.error("Can't upload file!!", e);
			response.setErrorCode( AnyoptionService.ERROR_CODE_UNKNOWN);
			response.addErrorMessage("", "File upload failed");
			response.addUserMessage("", CommonUtil.getMessage("copyop.avatar.err.file.upload.faild", null, locale));
	}		
		try {
			bos.close();
			bis.close();
		} catch (IOException e) {
			log.error("Can't close file or Streams", e);
		}

		return response;
	}
	
	private static String getSubDirName(long userId) {
		String subDir =  Long.toString(userId).substring(0, 3);
		String dir = CommonUtil.getProperty(ConstantsBase.FILES_PATH_AVATAR) + subDir;
		java.io.File subDirFile = new java.io.File(dir);
		Boolean result = null;
		if (!subDirFile.exists()) {
			result = subDirFile.mkdir();
		}
		if (result != null && !result) {
			log.warn("Cannot create subdir");
			subDir = "";
		}		
		return dir + "/";
	}
	
}