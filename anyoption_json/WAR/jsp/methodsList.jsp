<%@page import="com.anyoption.json.service.AnyoptionServiceServlet"  %>
<%@page import="java.util.Iterator"%>
<%@page import="java.lang.reflect.Method" %>


<%@page import="java.io.PrintWriter"%>
<HTML>

<BODY>

<%
	out.println("<H1>Anyoption Service</H1><br/>");
	out.println("<H2><u>Methods List:</u></H2>");
	int idx = 1;
	for (Iterator<String> iter = AnyoptionServiceServlet.getMethods().keySet().iterator(); iter.hasNext();) {
		String name = iter.next();
		Method m = AnyoptionServiceServlet.getMethods().get(name);
		out.println(String.valueOf(idx) + ") <b>Method:</b> " + name + "<br/>");
		Class[] params = m.getParameterTypes();
		for (int i = 1; i <= params.length; i++) {
			out.println("&#160;&#160;&#160;&#160;<b>Parameter[" + i + "]:</b> " + params[i-1].toString()+ "<br/>");
		}
		out.println("&#160;&#160;&#160;&#160;<b>Return:</b> " + m.getReturnType().toString() + "<br/><br/>");
		idx++;
	}
%>

</BODY>

</HTML>