/*shorted document.getElementById() it's only g() in order to be used very easy even inline*/
function g(id){return document.getElementById(id);}
/*ajax call*/
function aJaxCall_fn(params){//url,params to post,type,callback,xml,async
	var type = (typeof params.type != 'undefined')?params.type:'GET';
	var post_params = (typeof params.params != 'undefined')?params.params:null;
	var async = (typeof params.async != 'undefined')?params.async:true;
	var xml = (typeof params.xml != 'undefined')?params.xml:false;
	
	var req = init();
	req.onreadystatechange = processRequest;

	function init() {
		if (window.XMLHttpRequest) {
			return new XMLHttpRequest();
		} else if (window.ActiveXObject) {
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
	}

    function processRequest () {
		// readyState of 4 signifies request is complete
		if (req.readyState == 4) {
			// status of 200 signifies sucessful HTTP call
			if (req.status == 200) {
				if (params.callback) {
					if(xml){
						params.callback(req.responseXML);
					}else{
						params.callback(req.responseText);
					}
				}
			}
		}
    }

    this.doGet = function() {
		req.open(type, params.url, async);//type[post,get]
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(post_params);
   	}
}
var MethodRequest = [
	{'name':'skinId','type':'long'},
	{'name':'ip','type':'String'},
	{'name':'utcOffset','type':'int'},
	{'name':'deviceId','type':'String'},
	{'name':'writerId','type':'long'},
	{'name':'apiUser','type':'object','elements':[
		{'name':'userName','type':'String'},
		{'name':'password','type':'String'}
	]},
	{'name':'locale','type':'String'}
];
var UserMethodRequest = MethodRequest.concat([
	{'name':'userName','type':'String'},
	{'name':'password','type':'String'},
	{'name':'encrypt','type':'boolean'},
	{'name':'isLogin','type':'boolean'},
	{'name':'afterDeposit','type':'boolean'}
]);
var FormsMethodRequest = UserMethodRequest.concat([
	{'name':'from','type':'Calendar'},
	{'name':'to','type':'Calendar'},
	{'name':'startRow','type':'Integer'},
	{'name':'pageSize','type':'Integer'}
]);
var BonusMethodRequest = FormsMethodRequest.concat([
	{'name':'bonusId','type':'long'},
	{'name':'bonusStateId','type':'long'}
]);
var InsertUserMethodRequest = MethodRequest.concat([
	{'name':'register','type':'object','elements':[
		{'name':'userName','type':'String'},
		{'name':'password','type':'String'},
		{'name':'password2','type':'String'},
		{'name':'email','type':'String'},
		{'name':'currencyId','type':'long'},
		{'name':'firstName','type':'String'},
		{'name':'lastName','type':'String'},
		{'name':'street','type':'String'},
		{'name':'streetNo','type':'String'},
		{'name':'cityName','type':'String'},
		{'name':'cityId','type':'long'},
		{'name':'zipCode','type':'String'},
		{'name':'countryId','type':'long'},
		{'name':'languageId','type':'long'},
		{'name':'birthYear','type':'String'},
		{'name':'birthMonth','type':'String'},
		{'name':'birthDay','type':'String'},
		{'name':'mobilePhonePrefix','type':'String'},
		{'name':'mobilePhone','type':'String'},
		{'name':'landLinePhonePrefix','type':'String'},
		{'name':'landLinePhone','type':'String'},
		{'name':'gender','type':'String'},
		{'name':'idNum','type':'String'},
		{'name':'contactByEmail','type':'boolean'},
		{'name':'contactBySms','type':'boolean'},
		{'name':'terms','type':'boolean'},
		{'name':'skinId','type':'long'},
		{'name':'cityNameAO','type':'String'},
		{'name':'utcOffsetCreated','type':'String'},
		{'name':'stateId','type':'long'},
		{'name':'calcalistGame','type':'boolean'},
		{'name':'combinationId','type':'Long'},
		{'name':'dynamicParam','type':'String'},
		{'name':'contactId','type':'long'},
		{'name':'ip','type':'ip'},
		{'name':'timeFirstVisit','type':'Date'},
		{'name':'deviceUniqueId','type':'String'},
		{'name':'registerAttemptId','type':'long'},
		{'name':'userAgent','type':'String'},
		{'name':'httpReferer','type':'String'},
		{'name':'timeFirstVisitTxt','type':'String'},
		{'name':'countryName','type':'String'},
		{'name':'currencyCode','type':'String'}
	]},
	{'name':'clientId','type':'String'},
	{'name':'isBaidu','type':'boolean'},
	{'name':'marketingStaticPart','type':'String'},
	{'name':'mId','type':'String'},
	{'name':'etsMId','type':'String'},
	{'name':'sendMail','type':'boolean'},
	{'name':'sendSms','type':'boolean'}
]);
var ChartDataMethodRequest = UserMethodRequest.concat([
	{'name':'box','type':'int'},
	{'name':'marketId','type':'long'},
	{'name':'opportunityId','type':'long'},
	{'name':'opportunityTypeId','type':'long'}
]);
// var OpportunityCache = UserMethodRequest.concat([{'name':'opportunityTypeId','type':'long'},'box','marketId','opportunityId','opportunityTypeId']);
var PastExpiresMethodRequest = UserMethodRequest.concat([
	{'name':'date','type':'Calendar'},
	{'name':'marketId','type':'long'},
	{'name':'marketGroupId','type':'long'},
	{'name':'startRow','type':'int'},
	{'name':'pageSize','type':'int'}
]);
var CardMethodRequest = UserMethodRequest.concat([
	{'name':'creditCard','type':'object','elements':[
		{'name':'id','type':'long'},
		{'name':'userId','type':'long'},
		{'name':'typeId','type':'long'},
		{'name':'ccNumber','type':'long'},
		{'name':'ccPass','type':'String'},
		{'name':'expMonth','type':'String'},
		{'name':'expYear','type':'String'},
		{'name':'holderName','type':'String'},
		{'name':'holderId','type':'String'},
		{'name':'countryId','type':'long'},
		{'name':'typeIdByBin','type':'long'},
		{'name':'type','type':'object','elements':[
			{'name':'id','type':'long'},
			{'name':'name','type':'String'},
			{'name':'threeDSecure','type':'boolean'},
			{'name':'typeName','type':'String'}
		]},
		{'name':'ccNumberXXXX','type':'String'},
		{'name':'creditAmount','type':'long'},
		{'name':'creditAmountTxt','type':'String'},
		{'name':'cftAvailable','type':'boolean'},
		{'name':'nonCftAvailable','type':'boolean'},
		{'name':'clearingProviderId','type':'long'},
		{'name':'cftClearingProviderId','type':'long'}
	]}
]);
var CcDepositMethodRequest = UserMethodRequest.concat([
	{'name':'amount','type':'String'},
	{'name':'cardId','type':'long'},
	{'name':'error','type':'String'},
	{'name':'firstNewCardChanged','type':'boolean'}
]);
var WireMethodRequest = UserMethodRequest.concat([
	{'name':'wire','type':'object','elements':[
		{'name':'id','type':'long'},
		{'name':'bankId','type':'String'},
		{'name':'branch','type':'String'},
		{'name':'accountNum','type':'String'},
		{'name':'accountName','type':'String'},
		{'name':'deposit','type':'String'},
		{'name':'beneficiaryName','type':'String'},
		{'name':'swift','type':'String'},
		{'name':'iban','type':'String'},
		{'name':'bankNameTxt','type':'String'},
		{'name':'branchAddress','type':'String'},
		{'name':'amount','type':'String'},
		{'name':'bankCode','type':'String'},
		{'name':'accountType','type':'String'},
		{'name':'checkDigits','type':'String'},
		{'name':'bankFeeAmount','type':'String'},
		{'name':'branchName','type':'String'}
	]}
]);
var InvestmentsMethodRequest = UserMethodRequest.concat([
	{'name':'accountId','type':'String'},
	{'name':'accountPassword','type':'String'},
	{'name':'clientId','type':'long'},
	{'name':'from','type':'Calendar'},
	{'name':'fromEpoch','type':'long'},
	{'name':'to','type':'Calendar'},
	{'name':'toEpoch','type':'long'},
	{'name':'isSettled','type':'boolean'},
	{'name':'marketId','type':'boolean'},
	{'name':'marketId','type':'long'},
	{'name':'startRow','type':'int'},
	{'name':'pageSize','type':'int'},
	{'name':'apiExternalUserReference','type':'String'}
]);
var TransactionMethodRequest = UserMethodRequest.concat([
	{'name':'trans','type':'array','elements':[
		{'name':'creditCardId','type':'long'},
		{'name':'typeId','type':'long'},
		{'name':'timeCreated','type':'Date'},
		{'name':'amount','type':'long'},
		{'name':'amountWF','type':'String'},
		{'name':'statusId','type':'long'},
		{'name':'timeSettled','type':'Date'},
		{'name':'receiptNum','type':'Long'},
		{'name':'authNumber','type':'String'},
		{'name':'typeName','type':'String'},
		{'name':'credit','type':'boolean'},
		{'name':'cc4digit','type':'String'},
		{'name':'utcOffsetCreated','type':'String'},
		{'name':'utcOffsetSettled','type':'String'},
		{'name':'timeCreatedTxt','type':'String'}
	]}
]);
var AssetIndexMethodRequest = MethodRequest.concat([
	{'name':'marketId','type':'long'}
]);
var InsertInvestmentMethodRequest = UserMethodRequest.concat([
	{'name':'opportunityId','type':'long'},
	{'name':'requestAmount','type':'double'},
	{'name':'pageLevel','type':'double'},
	{'name':'pageOddsWin','type':'float'},
	{'name':'pageOddsLose','type':'float'},
	{'name':'choice','type':'int'},
	{'name':'isFromGraph','type':'boolean'},
	{'name':'ipAddress','type':'String'},
	{'name':'apiExternalUserReference','type':'String'},
	{'name':'apiExternalUserId','type':'long'},
	{'name':'defaultAmountValue','type':'long'}
]);
var ChangePasswordMethodRequest = UserMethodRequest.concat([
	{'name':'passwordNew','type':'String'},
	{'name':'passwordReType','type':'String'}
]);
var OptionPlusMethodRequest = UserMethodRequest.concat([
	{'name':'investmentId','type':'long'},
	{'name':'price','type':'Double'}
]);
// var concatMethodRequest = MethodRequest.concat([{'name':'price','type':'Double'}'concat','comments','issue']);
var CheckUpdateMethodRequest = MethodRequest.concat([
	{'name':'apkVersion','type':'int'},
	{'name':'apkName','type':'String'},
	{'name':'combinationId','type':'Long'},
	{'name':'c2dmRegistrationId','type':'String'},
	{'name':'dynamicParam','type':'String'},
	{'name':'mId','type':'String'},
	{'name':'isFirstOpening','type':'boolean'},
	{'name':'etsMId','type':'String'}
]);
// var MailBoxMethodRequest = UserMethodRequest.concat([{'name':'mailBoxUser','type':'MailBoxUser'},'mailBoxUser/ (MailBoxUser)']);
var RegisterAttemptMethodRequest = MethodRequest.concat([
	{'name':'email','type':'String'},
	{'name':'firstName','type':'String'},
	{'name':'lastName','type':'String'},
	{'name':'mobilePhone','type':'String'},
	{'name':'landLinePhone','type':'String'},
	{'name':'combinationId','type':'Long'},
	{'name':'isconcatByEmail','type':'boolean'},
	{'name':'isconcatBySMS','type':'boolean'},
	{'name':'countryId','type':'long'},
	{'name':'dynamicParameter','type':'String'},
	{'name':'id','type':'long'},
	{'name':'userAgent','type':'String'},
	{'name':'httpReferer','type':'String'},
	{'name':'mId','type':'String'},
	{'name':'marketingStaticPart','type':'String'},
	{'name':'etsMId','type':'String'}
]);
var CurrencyMethodRequest = MethodRequest.concat([
	{'name':'currencyId','type':'long'}
]);
var DepositReceiptMethodRequest = UserMethodRequest.concat([
	{'name':'transactionId','type':'long'}
]);
var InsurancesMethodRequest = UserMethodRequest.concat([
	{'name':'insuranceType','type':'int'},
	{'name':'investmentIds','type':'array','type_els':'long'},
	{'name':'insuranceAmounts','type':'array','type_els':'Double'}
]);
var CUPInfoMethodRequest = UserMethodRequest.concat([
	{'name':'params','type':'String'},
	{'name':'amount','type':'String'},
	{'name':'userId','type':'long'},
	{'name':'clearingProviderId','type':'long'}
]);
var FeeMethodRequest = UserMethodRequest.concat([
	{'name':'tranTypeId','type':'long'}
]);
var BaropayDepositMethodRequest = UserMethodRequest.concat([
	{'name':'amount','type':'String'},
	{'name':'landPhone','type':'String'},
	{'name':'senderName','type':'String'}
]);
var InsertMarketingTrackerClickRequest = MethodRequest.concat([
	{'name':'staticPart','type':'String'},
	{'name':'dynamicPart','type':'String'}
]);
// var UpdateUserQuestionnaireAnswersRequest = UserMethodRequest.concat(['userAnswers']);//?
var EmailValidatorRequest = MethodRequest.concat([
	{'name':'email','type':'String'}
]);
var CreditCardValidatorRequest = [
	{'name':'ccNumber','type':'String'}
];
// var UpdateUserMethodRequest = UserMethodRequest.concat(['user/ (User)']);
var FirstEverDepositCheckRequest = MethodRequest.concat([
	{'name':'userId','type':'long'}
]);
var CreditCardRequest = UserMethodRequest.concat([
	{'name':'cardId','type':'int'}
]);

var listOfInputs = {
	getCurrenciesOptions: 						MethodRequest,
	getSkinCurrenciesOptions: 					MethodRequest,
	getSkinCurrencies:							MethodRequest,
	getLanguagesOptions:						MethodRequest,
	getStatesOptions:							MethodRequest,
	getBonusStatesOptions:						MethodRequest,
	getMaxCupWithdrawal:						UserMethodRequest,
	getUserBonuses:								BonusMethodRequest,
	updateBonusStatus:							BonusMethodRequest,
	getCreditCardTypesOptions:					MethodRequest,
	getCountriesOptions:						MethodRequest,
	getCitiesOptions:							MethodRequest,
	getBanksET:									MethodRequest,
	getBanksBranchesET:							MethodRequest,
	getSkinsOptions:							MethodRequest,
	getUser:									UserMethodRequest,
	getUserEncryptedPass:						UserMethodRequest,
	insertUser:									InsertUserMethodRequest,
	// updateUser:									UpdateUserMethodRequest,
	getUserTransactions:						FormsMethodRequest,
	getChartData:								ChartDataMethodRequest,
	getChartDataPreviousHour:					ChartDataMethodRequest,
	getPastExpiries:							PastExpiresMethodRequest,
	getMarketGroups:							MethodRequest,
	getMarketList:								MethodRequest,
	insertCard:									CardMethodRequest,
	insertDepositCard:							CcDepositMethodRequest,
	validateWithdrawCard:						CcDepositMethodRequest,
	insertWithdrawCard:							CcDepositMethodRequest,
	validateWithdrawBank:						WireMethodRequest,
	insertWithdrawBank:							WireMethodRequest,
	getSkinIdByIp:								MethodRequest,
	getServerDateTime:							[],
	updateCard:									CardMethodRequest,
	getUserInvestments:							InvestmentsMethodRequest,
	insertWithdrawsReverse:						TransactionMethodRequest,
	getAssetIndexByMarket:						AssetIndexMethodRequest,
	insertInvestment:							InsertInvestmentMethodRequest,
	getChangePassword:							ChangePasswordMethodRequest,
	optionPlusFlashSell:						OptionPlusMethodRequest,
	// insertconcat:								concatMethodRequest,
	getPrice:									OptionPlusMethodRequest,
	getAvailableCreditCardsWithEmptyLine:		UserMethodRequest,
	getAvailableCreditCards:					UserMethodRequest,
	getWithdrawCCList:							UserMethodRequest,
	checkForUpdate:								CheckUpdateMethodRequest,
	c2dmUpdate:									CheckUpdateMethodRequest,
	getUserInboxMails:							UserMethodRequest,
	// updateMailBox:								MailBoxMethodRequest,
	skinUpdate:									CheckUpdateMethodRequest,
	sendPassword:								InsertUserMethodRequest,
	insertRegisterAttempt:						RegisterAttemptMethodRequest,
	getCountryId:								MethodRequest,
	getCurrencyLetters:							CurrencyMethodRequest,
	SendDepositReceipt:							DepositReceiptMethodRequest,
	buyInsurances:								InsurancesMethodRequest,
	insuranceBannerSeen:						UserMethodRequest,
	getCUPInfo:									CUPInfoMethodRequest,
	getDeltapayInfo:							CUPInfoMethodRequest,
	getCdpayInfo:								CUPInfoMethodRequest,
	insertWithdrawDeltapay:						CcDepositMethodRequest,
	getFeeAmountByType:							FeeMethodRequest,
	insertBaropayDeposit:						BaropayDepositMethodRequest,
	validateBaroPayWithdraw:					WireMethodRequest,
	insertWithdrawBaropay:						WireMethodRequest,
	checkBaroPayWithdrawAvailable:				UserMethodRequest,
	getMinFirstDeposit:							UserMethodRequest,
	insertMarketingTrackerClick:				InsertMarketingTrackerClickRequest,
	updateMandatoryQuestionnaireDone:			UserMethodRequest,
	updateOptionalQuestionnaireDone:			UserMethodRequest,
	getUserMandatoryQuestionnaire:				UserMethodRequest,
	getUserOptionalQuestionnaire:				UserMethodRequest,
	// updateUserQuestionnaireAnswers:				UpdateUserQuestionnaireAnswersRequest,
	validateEmail:								EmailValidatorRequest,
	validateCC:									CreditCardValidatorRequest,
	// updateUserAdditionalFields:					UpdateUserMethodRequest,
	getOpenHourlyBinaryMarkets:					MethodRequest,
	getAllBinaryMarkets:						MethodRequest,
	getCurrentHourlyOppByMarket:				ChartDataMethodRequest,
	insertLoginToken:							UserMethodRequest,
	getCurrentLevelByOpportunityId:				ChartDataMethodRequest,
	getFirstEverDepositCheck:					FirstEverDepositCheckRequest,
	getBankWireWithdrawOptions:					MethodRequest,
	getAllReturnOdds:							MethodRequest,
	getBinaryHomePageMarkets:					MethodRequest,
	deleteCreditCard:							CreditCardRequest,
	sendBankTransferDetailsUser:				UserMethodRequest,
	getLastCurrenciesRate:						MethodRequest,
	getLogoutBalanceStep:						MethodRequest
}

function InitJsonMethods(){
	var select = g('jsonMethods');
	var count = 0;
	for(key in listOfInputs) {
		var option = document.createElement("option");
		option.text = key;
		option.value = key;
		select.add(option);
	}
}
var div_html = '';
function createInput(el){
	var val = el.value;
	div_html = g('jsonMethodsForm');
	div_html.innerHTML = '';
	var elements = listOfInputs[val];
	for(var i=0; i<elements.length; i++){
		xxx(elements[i]);
	}
}
function addInput(name, label, defValue){
	var row = document.createElement('div');
	row.className = "row_input";
	
	var span = document.createElement('label');
	span.innerHTML = label;
	span.className = 'label';
	row.appendChild(span);
	
	var inp = document.createElement('input');
	inp.name = name;
	inp.id = name;
	row.appendChild(inp);
	
	div_html.appendChild(row);
}
function xxx(object){
	if(object.type == "object"){
		var obj = object.elements;
		for(var j=0; j<obj.length; j++){
			obj[j].name = object.name + ":" + obj[j].name;
			xxx(obj[j]);
			// addInput(elements[i].name + ":" + obj[j].name, obj[j].name + "/ (" + obj[j].type + ") from " + elements[i].name, '', div);
		}
	}
	else{
		addInput(object.name, object.name + "/ (" + object.type + ") ", '');
	}
}
function submitJson(){
	var els = g('jsonMethodsForm').getElementsByTagName('input');
	var res = {};
	for(var i=0; i<els.length; i++){
		var tmp = els[i].name.split(':');
		if(tmp.length == 1){
			res[tmp[0]] = els[i].value;
		}
		else if(tmp.length == 2){
			if(typeof res[tmp[0]] == 'undefined'){
				res[tmp[0]] = {};
			}
			res[tmp[0]][tmp[1]] = els[i].value;		
		}
		else if(tmp.length == 3){
			if(typeof res[tmp[0]] == 'undefined'){
				res[tmp[0]][tmp[1]] = {};
			}
			res[tmp[0]][tmp[1]][tmp[2]] = els[i].value;
		}
	}
	var ajaxCall = new aJaxCall_fn({'type':'POST','url':'http://www.testenv.anyoption.com/jsonService/AnyoptionService/'+g('jsonMethods').value,'params':JSON.stringify(res),'callback':Callback});
	ajaxCall.doGet();
	g('requestDiv').innerHTML = JSON.stringify(JSON.parse(JSON.stringify(res)),null,2);
}
function Callback(response){
	// console.log(response);
	g('responseDiv').innerHTML = JSON.stringify(JSON.parse(response),null,2);
}