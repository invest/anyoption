var arr = window.location.hostname.split('.');

var isLive = true;
for (var i = 0; i < arr.length; i++) {
	if (arr[i].search('test') > -1) {
		isLive = false;
		break;
	}
}

function g(id){return document.getElementById(id);}
function isUndefined(el) {
	return (typeof el == 'undefined' || el == null);
}

window.addEventListener('message',function(event) {
	var bubblesServer = (isLive) ? 'https://m.visualtrade.com' : 'https://aom.visualtrade.com';
	if(event.origin !== bubblesServer) return;
	if (!isUndefined(event.data.respData)) {
		if (event.data.respData.errorCode == 99999) {
			backBtn()
		} else {
			var url = '?';
			url += 'errorCode=' + event.data.respData.errorCode;
			url += '&errorMessages=' + encodeURIComponent(event.data.respData.errorMessages);
			window.history.pushState({}, "Bubbles", pageUrl + url);
		}
	}
},false)

function backBtn() {
	var url = '?back=true';
	window.history.pushState({}, "Bubbles", pageUrl + url);
}

function showActive() {
	if (isActive) {
		g('bubbles_holder').style.display = "block";
	}
};

function showIframe() {
	if (isLive) {
		g('bubbles').src = linkProd + params;
	} else {
		g('bubbles').src = linkTest + params;
	}
}