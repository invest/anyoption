//package il.co.etrader.web.xml;
//
//import il.co.etrader.util.CommonUtil;
//import il.co.etrader.web.bl_managers.UsersManager;
//
//import java.util.Hashtable;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.log4j.Logger;
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
//
//import com.anyoption.common.util.ClearingUtil;
//
//public class XMLlogin extends XMLRequestProcessor {
//    private static final Logger log = Logger.getLogger(XMLlogin.class);
//    
//    public void processXMLRequest(HttpServletRequest request, HttpServletResponse response, Document xml) {
//        super.processXMLRequest(request, response, xml);
//        
//        if (errorCode == ERROR_CODE_SUCCESS) {
//            Element action = (Element) ClearingUtil.getNode(xml.getDocumentElement(), "request/action");
//            String userName = ClearingUtil.getElementValue(action, "userName/*");
//            String password = ClearingUtil.getElementValue(action, "password/*");
//            
//            if (log.isDebugEnabled()) {
//                log.debug("userName: " + userName);
//            }
//            
//            if (null == userName || null == password) {
//                errorCode = ERROR_CODE_MISSING_PARAM;
//                errorMessage = "User name and password mandatory.";
//                return;
//            }
//            userName = userName.toUpperCase();
//            password = password.toUpperCase();
//            
//            
//            Hashtable<String, String> ht = new Hashtable<String, String>();
//            ht.put("Cookie", "JSESSIONID=" + request.getSession().getId() + ";");
//
//            String app = CommonUtil.getProperty("localhost.url") + request.getContextPath();
//            UsersManager.sendRequest("", app + "/account/login.txt", ht, true);
//            UsersManager.sendRequest("", app + "/j_security_check?j_username=" + userName + "&j_password=" + password, ht, true);
//            String resp = UsersManager.sendRequest("", app + "/account/login.txt", ht, true);
//            if (resp.indexOf("xml-api-yes") == -1) {
//                errorCode = ERROR_CODE_LOGIN_FAILED;
//            }
//        }
//    }
//}