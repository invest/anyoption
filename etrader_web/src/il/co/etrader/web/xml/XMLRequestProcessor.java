//package il.co.etrader.web.xml;
//
//import il.co.etrader.web.bl_managers.XMLAccountManager;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.log4j.Logger;
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
//
//import com.anyoption.common.util.ClearingUtil;
//
//public abstract class XMLRequestProcessor {
//    public static final Logger log = Logger.getLogger(XMLRequestProcessor.class);
//    
//    public static final int ERROR_CODE_SUCCESS          = 0;
//    public static final int ERROR_CODE_UNKNOWN_REQUEST  = 200;
//    public static final int ERROR_CODE_PARSING_ERROR    = 201;
//    public static final int ERROR_CODE_INVALID_ACCOUNT  = 202;
//    public static final int ERROR_CODE_MISSING_PARAM    = 203;
//    public static final int ERROR_CODE_INVALID_VALUE    = 204;
//    public static final int ERROR_CODE_LOGIN_FAILED     = 205;
//    public static final int ERROR_CODE_INV_VALIDATION   = 206;
//    public static final int ERROR_CODE_UNKNOWN_ERR_PG   = 998;
//    public static final int ERROR_CODE_UNKNOWN          = 999;
//    
//    protected long id;
//    protected boolean testMode;
//    protected int errorCode;
//    protected String errorMessage;
//    protected long clientId;
//    
//    public XMLRequestProcessor() {
//        // it will be 0 anyway
//        // errorCode = ERROR_CODE_SUCCESS; 
//    }
//    
//    public void processXMLRequest(HttpServletRequest request, HttpServletResponse response, Document xml) {
//        try {
//            Element element = (Element) ClearingUtil.getNode(xml.getDocumentElement(), "request");
//            String strId = ClearingUtil.getAttribute(element, "", "id");
//            String mode = ClearingUtil.getAttribute(element, "", "mode");
//            testMode = null != mode && mode.equals("test") ? true : false;
//
//            String strClientId = ClearingUtil.getElementValue(element, "clientData/clientId/*");
//            
//            Element account = (Element) ClearingUtil.getNode(xml.getDocumentElement(), "request/account");
//            String accountId = ClearingUtil.getElementValue(account, "id/*");
//            String password = ClearingUtil.getElementValue(account, "password/*");
//            
//            if (null == strId || null == strClientId || null == accountId || null == password) {
//                errorCode = ERROR_CODE_MISSING_PARAM;
//                errorMessage = "Request id, client id, account id or account pass missing.";
//                return;
//            }
//
//            id = Long.parseLong(strId);
//            clientId = Long.parseLong(strClientId);
//            
//            if (!XMLAccountManager.isLogin(accountId, password)) {
//                errorCode = ERROR_CODE_INVALID_ACCOUNT;
//                errorMessage = "Invalid account";
//            }
//
//            if (log.isDebugEnabled()) {
//                log.debug("XMLRequest accountId: " + accountId + " request id: " + id + " testMode: " + testMode);
//            }
//        } catch (Exception e) {
//            log.error("Can't parse request id and mode.", e);
//            errorCode = ERROR_CODE_PARSING_ERROR;
//            errorMessage = "Can't parse request id or mode.";
//        }
//    }
//
//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }
//
//    public boolean isTestMode() {
//        return testMode;
//    }
//
//    public void setTestMode(boolean testMode) {
//        this.testMode = testMode;
//    }
//
//    public int getErrorCode() {
//        return errorCode;
//    }
//
//    public void setErrorCode(int errorCode) {
//        this.errorCode = errorCode;
//    }
//
//    public String getErrorMessage() {
//        return errorMessage;
//    }
//
//    public void setErrorMessage(String errorMessage) {
//        this.errorMessage = errorMessage;
//    }
//}