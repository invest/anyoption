//package il.co.etrader.web.xml;
//
//import java.io.IOException;
//
//import javax.servlet.Filter;
//import javax.servlet.FilterChain;
//import javax.servlet.FilterConfig;
//import javax.servlet.ServletException;
//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//
//import org.apache.log4j.Logger;
//import org.w3c.dom.Document;
//
//public class XMLRequestFilter implements Filter {
//    private static final Logger log = Logger.getLogger(XMLRequestFilter.class);
//    
//    public void init(FilterConfig filterConfig) throws ServletException {
//        // do nothing
//    }
//
//    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
//        HttpServletRequest httpRequest = (HttpServletRequest) request;
//        HttpServletResponse httpResponse = (HttpServletResponse) response;
//        String uri = httpRequest.getRequestURI();
//        if (null != uri && uri.endsWith(".jsp")) {
//            String method = uri;
//            if (method.indexOf('/') != -1) {
//                method = method.substring(method.lastIndexOf('/') + 1);
//            }
//            method = method.substring(0, method.length() - 4); // cut the .jsp
//
//            if (log.isDebugEnabled()) {
//                log.debug("URI: " + uri + " method: " + method);
//            }
//    
//            XMLRequestProcessor rp = null;
//            try {
//                Class c = Class.forName("il.co.etrader.web.xml.XML" + method);
//                rp = (XMLRequestProcessor) c.newInstance();
//            } catch (Exception e) {
//                log.error("Can't process XML request.", e);
//                rp = new XMLerror(XMLRequestProcessor.ERROR_CODE_UNKNOWN_REQUEST, method);
//            }
//    
//            // parse the request
//            Document xml = null;
//            if (rp.getErrorCode() == XMLRequestProcessor.ERROR_CODE_SUCCESS) {
//                try {
//                    request.setCharacterEncoding("UTF-8");
//                    DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
//                    DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
//                    xml = docBuilder.parse(request.getInputStream());
//                } catch (Exception e) {
//                    log.error("Can't parse XML request.", e);
//                    rp.setErrorCode(XMLRequestProcessor.ERROR_CODE_PARSING_ERROR);
//                    rp.setErrorMessage(e.getMessage());
//                }
//            }
//            
//            // process the request
//            if (rp.getErrorCode() == XMLRequestProcessor.ERROR_CODE_SUCCESS) {
//                try {
//                    rp.processXMLRequest(httpRequest, httpResponse, xml);
//                } catch (Exception e) {
//                    log.error("Can't process XML request.", e);
//                    rp.setErrorCode(XMLRequestProcessor.ERROR_CODE_UNKNOWN);
//                }
//            }
//            
//            // store the processor with its content in the request to be accessible for the JSPs
//            request.setAttribute("reqProc", rp);
//        }
//        
//        // Continue processing the rest of the filter chain.
//        chain.doFilter(request, response);
//    }
//
//    public void destroy() {
//        // do nothing
//    }
//}