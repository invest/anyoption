//package il.co.etrader.web.xml;
//
//import il.co.etrader.service.BaseServleetContextListener;
//
//import javax.servlet.ServletContext;
//import javax.servlet.ServletContextEvent;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.jms.WebLevelsCache;
//
///**
// * Handle the LevelsCache init and shutdown. We need the levels cache so we can
// * take from there the current opportunity level when we insert investment.
// *
// * @author Tony
// */
//public class XMLServletContextListener extends BaseServleetContextListener {
//    private static Logger log = Logger.getLogger(XMLServletContextListener.class);
//
//    /**
//     * Notification that the application is started
//     *
//     * @param event
//     */
//    public void contextInitialized(ServletContextEvent event) {
//        super.contextInitialized(event);
//
//        ServletContext servletContext = event.getServletContext();
//        try {
//            WebLevelsCache levelsCache = new WebLevelsCache(initialContextFactory, providerURL, connectionFactory, queue, topic, msgPoolSize, recoveryPause, null);
//            levelsCache.registerJMX(jmxName);
//            servletContext.setAttribute("levelsCache", levelsCache);
//            if (log.isInfoEnabled()) {
//                log.info("WebLevelsCahce bound to the context");
//            }
//        } catch (Exception e) {
//            log.error("Failed to init WebLevelsCache", e);
//        }
//    }
//}