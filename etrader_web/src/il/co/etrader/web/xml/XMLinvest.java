//package il.co.etrader.web.xml;
//
//import il.co.etrader.util.ConstantsBase;
//import il.co.etrader.web.mbeans.SlipForm;
//
//import java.text.DecimalFormat;
//import java.text.ParseException;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.log4j.Logger;
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
//
//import com.anyoption.common.util.ClearingUtil;
//
//public class XMLinvest extends XMLRequestProcessor {
//    private static final Logger log = Logger.getLogger(XMLinvest.class);
//    
//    protected String investmentId;
//    
//    public void processXMLRequest(HttpServletRequest request, HttpServletResponse response, Document xml) {
//        super.processXMLRequest(request, response, xml);
//        
//        if (errorCode == ERROR_CODE_SUCCESS) {
//            Element action = (Element) ClearingUtil.getNode(xml.getDocumentElement(), "request/action");
//            String oppId = ClearingUtil.getElementValue(action, "opportunityId/*");
//            String amount = ClearingUtil.getElementValue(action, "amount/*");
//            String level = ClearingUtil.getElementValue(action, "level/*");
//            String oddsWin = ClearingUtil.getElementValue(action, "oddsWin/*");
//            String oddsLose = ClearingUtil.getElementValue(action, "oddsLose/*");
//            String choice = ClearingUtil.getElementValue(action, "choice/*");
//            String utcOffset = ClearingUtil.getElementValue(action, "utcOffset/*");
//            
//            if (log.isDebugEnabled()) {
//                log.debug(
//                        "oppId: " + oppId +
//                        " amount: " + amount +
//                        " level: " + level +
//                        " oddsWin: " + oddsWin +
//                        " oddsLose: " + oddsLose +
//                        " choice: " + choice +
//                        " utcOffset: " + utcOffset);
//            }
//            
//            if (null == oppId || null == amount || null == level || null == oddsWin || null == oddsLose || null == choice || null == utcOffset) {
//                errorCode = ERROR_CODE_MISSING_PARAM;
//                errorMessage = "Missing mandatory param.";
//                return;
//            }
//
//            String reply = null;
//            try {
//                DecimalFormat numberFormat5 = new DecimalFormat("#,##0.00000");
//                reply = SlipForm.submitFromSlip(
//                        Long.parseLong(oppId),
//                        Float.parseFloat(amount),
//                        numberFormat5.parse(level).doubleValue(),
//                        Float.parseFloat(oddsWin),
//                        Float.parseFloat(oddsLose),
//                        Integer.parseInt(choice),
//                        1,
//                        Integer.parseInt(utcOffset),
//                        ConstantsBase.FROM_GRAPH_DEFAULT,
//                        null,
//                        0L);
//                if (reply.startsWith("OK")) {
//                    investmentId = reply.substring(2);
//                } else {
//                    errorCode = ERROR_CODE_INV_VALIDATION;
//                    errorMessage = reply;
//                }
//            } catch (Exception e) {
//                log.error("Failed to process XML submit request.", e);
//                if (e instanceof NumberFormatException || e instanceof ParseException) {
//                    errorCode = ERROR_CODE_INVALID_VALUE;
//                    errorMessage = e.getMessage();
//                } else {
//                    errorCode = ERROR_CODE_UNKNOWN;
//                }
//            }
//        }
//    }
//}