
package il.co.etrader.web.bl_managers;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.Collator;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.el.ValueExpression;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.myfaces.shared_tomahawk.util.ExceptionUtils;

import com.anyoption.common.beans.BalanceStep;
import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.Enumerator;
import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.LimitationDeposits;
import com.anyoption.common.beans.LiveGlobeCity;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.MarketGroup;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.SkinCurrency;
import com.anyoption.common.beans.SkinLanguage;
import com.anyoption.common.beans.base.ApiPage;
import com.anyoption.common.beans.base.BalanceStepPredefValue;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.CountryMiniBean;
import com.anyoption.common.beans.base.CurrenciesRules;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Language;
import com.anyoption.common.beans.base.Message;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.Ad;
import com.anyoption.common.bl_vos.DirectPaymentMapping;
import com.anyoption.common.bl_vos.InvestmentLimit;
import com.anyoption.common.daos.ApiDAOBase;
import com.anyoption.common.daos.CountryDAOBase;
import com.anyoption.common.daos.CurrenciesDAOBase;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.daos.LanguagesDAOBase;
import com.anyoption.common.daos.MarketingDictionaryParamsDAO;
import com.anyoption.common.daos.TournamentDAOBase;
import com.anyoption.common.daos.UrlsDAO;
import com.anyoption.common.enums.CountryStatus;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.jms.LevelsCache;
import com.anyoption.common.jms.WebLevelLookupBean;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.managers.CurrenciesRulesManagerBase;
import com.anyoption.common.managers.GeneralManager;
import com.anyoption.common.managers.LimitationDepositsManagerBase;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.managers.MessagesManager;
import com.anyoption.common.managers.UserBalanceStepManager;
import com.copyop.common.managers.ConfigurationCopyopManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
//import il.co.etrader.bl_vos.Enumerator;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_managers.SkinsManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.CachePages;
import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.DeviceUniqueIdSkin;
import il.co.etrader.bl_vos.Limit;
import il.co.etrader.bl_vos.MarketingAffilate;
import il.co.etrader.bl_vos.MarketingCombination;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.LimitsDAO;
import il.co.etrader.dao_managers.MarketingAffiliatesDAOBase;
import il.co.etrader.dao_managers.MarketingPixelsDAOBase;
import il.co.etrader.dao_managers.SkinUrlCountryMapDAOBase;
import il.co.etrader.dao_managers.TransactionsDAOBase;
import il.co.etrader.dao_managers.WritersDAO;
import il.co.etrader.helper.LiveHelper;
import il.co.etrader.rss.RssItem;
import il.co.etrader.rss.RssReader;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_vos.LandingMarketDetails;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.dao_managers.CachePagesDAO;
import il.co.etrader.web.dao_managers.MarketsDAO;
import il.co.etrader.web.dao_managers.PagesConfigDAO;
import il.co.etrader.web.dao_managers.TransactionsDAO;
import il.co.etrader.web.helper.TickerUpdater;
import il.co.etrader.web.helper.TradingPageBoxBean;
import il.co.etrader.web.mbeans.BankingForm;
import il.co.etrader.web.mbeans.PageNames;
import il.co.etrader.web.mbeans.SlipForm;
import il.co.etrader.web.util.Constants;
import il.co.etrader.web.util.Utils;


public class ApplicationData extends ApplicationDataBase {
	private static final Logger logger = Logger.getLogger(ApplicationData.class);

	protected static long writerId;
    protected static ArrayList<Message> messages;
    protected static long messagesLoadTime;
    protected static long messagesRefreshPeriod;

    // ticker load markets
//    protected static HashMap<Long,ArrayList<TradingPageBoxBean>> tickerMarkets;
//    protected static HashMap<Long,Calendar> lastLoadTimeTickerMarkets;
//    protected static int tickerMarketsRefreshPeriod;

    //is one touch open
    protected static HashMap<Long,Boolean> isHasOneTouch;
    protected static HashMap<Long,Calendar> lastLoadTimeIsHasOneTouch;
    protected static int isHasOneTouchRefreshPeriod;

    //is has open markets
    protected static HashMap<Long,Boolean> isHasOpenMarkets;
    protected static HashMap<Long,Calendar> lastLoadTimeIsHasOpenMarkets;

    // transactions direct deposit payment methods
    private static HashMap<Long,SelectItem> directDepositPaymentTypes;
    private static ArrayList<DirectPaymentMapping> directPaymentMapping;

    private long checkNumber;   // for stress function

    protected static ArrayList<SelectItem> bonusStates;

    //for insurance (GM/RU)
    private static int endOfHourStart;
    private static int endOfHourEnd;
    private static int midHourStart;
    private static int midHourEnd;
    private static int insuranceMinStart;
    private static int insuranceMinEnd;

    // rss reader
    private static HashMap<String, ArrayList<RssItem>> rssItems;
    private static HashMap<String, Long> rssLoadTime;
    private static HashMap<String, String> rssFeedUrl;
    private static long rssRefreshPeriod;

    // landing page markets
    private static HashMap<Long, LandingMarketDetails> marketsClosingLevels;
    private static long landingMarketsRefreshPeriod;
    private static long landingMarketsLoadTime;

    // for clearing providers
    private static Set<String> arabCountriesId;
    private static Map<Long, String> usMap; // for ACH

    private static ArrayList<String> defaultCombinations;

    private static HashMap<Integer, String> invType; //call/put/one touch English only
    private static HashMap<Integer, String> invScheduled; //hourly/daily/weekly/monthly English only

    private static HashMap<Long, ArrayList<Market>> ETMarketGroups ; //hourly/daily/weekly/monthly English only

    private static HashMap<String, Locale> localeList ;

    private static HashMap<Long, Long> webMoneyClearingProviderId; //Get clearing provider id by currencyId

    private static HashMap<Long, MarketingAffilate> affiliatesHM;

    // show off per each hour
    private static Calendar lastLoadTimeShowOff;
    private static Investment ShowOffInvestment;

    private static ArrayList<SelectItem> pixelPageName;

    private static ArrayList<SelectItem> investmentClosingHours;

    // top trades for each day
    private static Calendar lastLoadTimeTopTrades;
    private static HashMap<Long, ArrayList<Investment>> lastTopTrades;

    private static HashMap<String, String> subDomainsLocalesList;
    private static HashMap<String, String> subDomainsList;
    private static HashMap<String, String> subDomainsLocalesListRegulation;
    private static HashMap<String, String> subDomainsListRegulation;
    
    private static ArrayList<CachePages> cachePages;
    
    private static HashSet<Long> regulatedCountriesList;
    private static HashSet<String> regulatedSkinsList;
    private static HashSet<String> allowIPList;
    private static Hashtable<Long, ApiPage> APIPagesList;
    
    private static HashMap<Long, Limit> limitHM;

    private static HashMap<String, LinkedHashMap<Long, CountryMiniBean>> skinSortedCountriesMap = new HashMap<String, LinkedHashMap<Long,CountryMiniBean>>();
    private static String israelMarkets;

    private static ArrayList<String> minisitePages;
    
    private static HashMap<Long, HashMap<String, String>> MarketingDictionaryParamsHM;

	public static void init() throws SQLException {
        ApplicationDataBase.init();

		Connection con = getConnection();

		try {

			// Find the writer ID
			writerId = WritersDAO.getByUserName(con, Constants.WEB_ROLE).getId();
			messagesRefreshPeriod = CommonUtil.getPropertyLong("tradingpages.messages.reload.period");
            isHasOneTouch = new HashMap<Long,Boolean>();
            lastLoadTimeIsHasOneTouch = new HashMap<Long, Calendar>();
            isHasOneTouchRefreshPeriod = (int)CommonUtil.getPropertyLong("isHasOneTouch.reload.period");
            isHasOpenMarkets = new HashMap<Long,Boolean>();
            lastLoadTimeIsHasOpenMarkets = new HashMap<Long, Calendar>();
			ArrayList<Skins> skinList = getSkinsList();
			
    		// load ticker markets and check if we have one touch open only for AO aldo insert new markets into db market_name_skin table.
    		if (!getInitDefaultSkin().equalsIgnoreCase(String.valueOf(Skin.SKIN_ETRADER))){
    			Skins skin ;
//    			tickerMarketsRefreshPeriod = (int)CommonUtil.getPropertyLong("tickerMarkets.reload.period");
//    			tickerMarkets = new HashMap<Long,ArrayList<TradingPageBoxBean>>();
//    			lastLoadTimeTickerMarkets = new HashMap<Long, Calendar>();
    			for (int i = 0; i < skinList.size(); i++) {
    				skin = skinList.get(i);
    				if (skin.getId() != Skin.SKIN_ETRADER) {
//    					tickerMarkets.put(skinId, getTickerMarketsById(skinId));
//    					insert new markets name into db b4 we load them in any list!!
//    				    if (getInitDefaultSkin().equalsIgnoreCase(String.valueOf(Skin.SKIN_REG_EN))) { // this one is supposed to match just the settings of anyoption application. anyoption_it has default skin 20 and 168qiquan has default skin 22
//    				    	MarketsManager.updateMarketNameBySkin(skin.getId(), skin.getSkinMarketsList(), getLanguage(skin.getDefaultLanguageId()).getCode().toLowerCase());
//    				    }
    					isHasOneTouch.put((long)skin.getId(), isHasOpenOneTouchBySkinId(skin.getId()));
                        isHasOpenMarkets.put((long)skin.getId(), isHasOpenMarketBySkinId(skin.getId()));
    				}
    			}
    			// Rss feed
    			rssItems = new HashMap<String, ArrayList<RssItem>>();
    			rssLoadTime = new HashMap<String, Long>();
    			long time = 0 ;
    			rssLoadTime.put(Constants.RSS_URL, time);
    			rssLoadTime.put(Constants.TLV_RSS_URL, time);
    			rssLoadTime.put(Constants.ZH1_RSS_URL, time);
    			rssLoadTime.put(Constants.ZH2_RSS_URL, time);
    			rssLoadTime.put(Constants.ZH3_RSS_URL, time);
    			rssLoadTime.put(Constants.ZH4_RSS_URL, time);
    			rssFeedUrl = new HashMap<String, String>();
    			rssFeedUrl.put(Constants.RSS_URL, CommonUtil.getProperty(Constants.RSS_URL));
    			rssFeedUrl.put(Constants.TLV_RSS_URL, CommonUtil.getProperty(Constants.TLV_RSS_URL));
    			rssFeedUrl.put(Constants.ZH1_RSS_URL, CommonUtil.getProperty(Constants.ZH1_RSS_URL));
    			rssFeedUrl.put(Constants.ZH2_RSS_URL, CommonUtil.getProperty(Constants.ZH2_RSS_URL));
    			rssFeedUrl.put(Constants.ZH3_RSS_URL, CommonUtil.getProperty(Constants.ZH3_RSS_URL));
    			rssFeedUrl.put(Constants.ZH4_RSS_URL, CommonUtil.getProperty(Constants.ZH4_RSS_URL));

                rssRefreshPeriod = CommonUtil.getPropertyLong("rss.reload.period");

                // landing page markets
                landingMarketsRefreshPeriod = (long) CommonUtil.getPropertyLong("landing.markets.reload.period");
    		} else {
//    			MarketsManager.updateMarketNameBySkin(Skin.SKIN_ETRADER_INT, getSkinById(Skin.SKIN_ETRADER).getSkinMarketsList(), null);
    		    //check also if we have one touch open for ET
    		    isHasOneTouch.put(Skin.SKIN_ETRADER, isHasOpenOneTouchBySkinId(Skin.SKIN_ETRADER));
            }

		    bonusStates = new ArrayList<SelectItem>();
    		bonusStates.add(new SelectItem(new Long(0),"general.all"));
    		bonusStates.add(new SelectItem(new Long(4),"bonus.state1and2"));
    		for (int i = 1; i <= 3; i++) {
    			bonusStates.add(new SelectItem(new Long(i),"bonus.state" + i));
    		}
    		bonusStates.add(new SelectItem(new Long(5),"bonus.state5"));
    		bonusStates.add(new SelectItem(new Long(6),"bonus.state6"));
    		bonusStates.add(new SelectItem(new Long(7),"bonus.state7"));

    		directDepositPaymentTypes = TransactionsDAOBase.getDirectDepositPaymentTypes(con);
    		directPaymentMapping = TransactionsDAO.getDirectPaymentMapping(con);

            //init insurance times
            HashMap<String, Integer> a = InvestmentsManagerBase.getInsuranceTime();
            insuranceMinStart = a.get("insurance_period_start_time");
            insuranceMinEnd = a.get("insurance_period_end_time");
            endOfHourStart = 60 - insuranceMinStart;
            endOfHourEnd = 60 - insuranceMinEnd;
            midHourStart = (90 - insuranceMinStart) % 60;
            midHourEnd = (90 - insuranceMinEnd) % 60;

            arabCountriesId = CountryDAOBase.getAllArabCountriesId(con);
            usMap = CountryDAOBase.getAllUsStates(con);
            defaultCombinations = new ArrayList<String>();
            for (Skins s: skinsList){
            	defaultCombinations.add(String.valueOf(s.getDefaultCombinationId()));
            }

            invScheduled = new HashMap<Integer, String>();
            invScheduled.put(1, "Hourly");
            invScheduled.put(2, "Daily");
            invScheduled.put(3, "Weekly");
            invScheduled.put(4, "Monthly");
            invScheduled.put(5, "One touch");

            invType = new HashMap<Integer, String>();
            invType.put(1, "Call");
            invType.put(2, "Put");
            invType.put(3, "One touch");
            invType.put(4, "Buy");
            invType.put(5, "Sell");

            ETMarketGroups = MarketsDAO.getETgroups(con);

            localeList = createLocalesList();

            webMoneyClearingProviderId = setWebMoneyClearingProviders();

            affiliatesHM = MarketingAffiliatesDAOBase.getAffiliatesHM(con);

            pixelPageName = MarketingPixelsDAOBase.getAllPixelPageName(con);

            investmentClosingHours = getHoursInDayByHalf();

            cachePages = CachePagesDAO.getAllCachePages(con);
            
            lastTopTrades = new HashMap<Long, ArrayList<Investment>>();

            subDomainsLocalesList = SkinsManagerBase.getSubDomainsLocales(true, false);
            subDomainsLocalesList.put("zh", "zh");
            subDomainsList = SkinsManagerBase.getSubDomainsLocales(false, false);
            // Tony: Make sure that the zh locale brings you to the sg subdomain while we find a better solution
            subDomainsList.put("zh", "sg");
            subDomainsLocalesListRegulation = SkinsManagerBase.getSubDomainsLocales(true, true);
            subDomainsListRegulation = SkinsManagerBase.getSubDomainsLocales(false, true); 
            regulatedCountriesList = listOfRegulatedCountries();
            regulatedSkinsList = SkinsManagerBase.getRegulatedSkins();
            allowIPList = GeneralManager.getAllowIPList();
            APIPagesList = ApiDAOBase.getApiPages(con);
            
            HashMap<Long, Limit> limit = new HashMap<Long, Limit>();
            for (Limit lim : LimitsDAO.getAll(con)) {
            	limit.put(lim.getId(), lim);
            }
            limitHM = limit;

            ArrayList<Long> l = MarketsManager.getIsraelMarketsIds();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < l.size(); i++) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append(l.get(i));
            }
            israelMarkets = sb.toString();

            minisitePages = PagesConfigDAO.getMinisitePages(con);

            // fill AssetIndexMarket data
            initAssetIndexMarkets(con);
            //Copyop load cfg
            if((CommonUtil.getProperty("copyop.isEnabled").equalsIgnoreCase("true"))){
            	ConfigurationCopyopManager.loadCopyopCfg(con);
            }
            MarketingDictionaryParamsHM = MarketingDictionaryParamsDAO.getMarketingDictionaryParams(con);
		} finally {
			closeConnection(con);
		}
	}

	public ArrayList<SelectItem> getContactIssues() throws SQLException {
		ArrayList<Enumerator> l = CommonUtil.getEnum(Constants.ENUM_CONTACT_ISSUES);
		ArrayList<SelectItem> out = new ArrayList<SelectItem>();
		for (int i = 0; i < l.size(); i++) {
			Enumerator e = l.get(i);
			out.add(new SelectItem(CommonUtil.getMessage(e.getValue(), null), CommonUtil.getMessage(e.getValue(), null)));
		}
		return out;
	}

	public String getLoggedIn() {
		if (getBooleanLoggedIn()) {
			return "true";
		}
		return "false";
	}

	public boolean getBooleanLoggedIn() {
		FacesContext context = FacesContext.getCurrentInstance();
		String remoteUser = context.getExternalContext().getRemoteUser();
		if (!CommonUtil.isParameterEmptyOrNull(remoteUser)) {
			return true;
		}
		return false;
	}

	public boolean isActiveUser() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		String remoteUser = context.getExternalContext().getRemoteUser();
		if (remoteUser != null && !remoteUser.equals("")) {
			return InvestmentsManager.checkActiveUser(remoteUser);
		}
		return false;
	}


	public boolean getCheckIfDisconnected() {
		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		if (req.getRequestedSessionId() != null && req.getSession().getId() != null) {
			if (!req.getRequestedSessionId().equals(req.getSession().getId())) {
				return true;
            }
		}
		return false;
	}

	public String getSessionIdString() {
		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		return "jsessionid=" + req.getSession().getId();
	}

	/**
	 * Save cookies function
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public String getSaveCookie() throws IOException, SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();

		boolean cookieFound = false;
		boolean skinCookie = false;
		boolean combIdCookie = false;
		Cookie refererCookie = null;
		String skinValue = String.valueOf(Skin.SKIN_ETRADER);
		Cookie[] cs = request.getCookies();
		if (cs != null) {
			for (int i = 0; i < cs.length; i++) {
				if (cs[i].getName().equals(Constants.SKIN_ID)) {
					skinCookie = true;
					skinValue = cs[i].getValue();   // save skinId cookie value
				} else if (cs[i].getName().equals(Constants.COMBINATION_ID)) {
					logger.log(Level.DEBUG, "found combid cookie! combid:" + cs[i].getValue());
					combIdCookie = true;
				} else if (cs[i].getName().equals(Constants.REFERRER)) {
					//logger.log(Level.DEBUG, "found referrer cookie!");
					cookieFound = true;
					refererCookie = cs[i];
				}
			}
		}

		if ( skinCookie ) {  // found skin cookie
			logger.log(Level.DEBUG, "found skinId cookie, value: " + skinValue);
		}

		String skinId = (String)session.getAttribute(Constants.SKIN_ID);
		if ( null == skinId || !skinId.equals( String.valueOf(Skin.SKIN_ETRADER)) ) {  // skinId not in Session
			if ( skinCookie == false || !skinValue.equals(Skin.SKIN_ETRADER) ) {  // skinId not in cookie
				addCookie(Constants.SKIN_ID, String.valueOf(Skin.SKIN_ETRADER), response, Constants.SKIN_ID_DESCRIPTION);
			}
			session.setAttribute(Constants.SKIN_ID, String.valueOf(Skin.SKIN_ETRADER));
			logger.debug("Saving skinId values in Session!");
		}

		String referrer = (String) session.getAttribute(Constants.REFERRER);
		String combId = (String) session.getAttribute(Constants.COMBINATION_ID);

		if (null == referrer && null == combId) {
			session.setAttribute(Constants.COMBINATION_ID, String.valueOf(getSkinById(Skin.SKIN_ETRADER).getDefaultCombinationId()));
		}

		if (!cookieFound && !combIdCookie ) {  // no referer && combId cookie
			// prepare the combinationId cookie
			addCookie(Constants.COMBINATION_ID, String.valueOf(getSkinById(Skin.SKIN_ETRADER).getDefaultCombinationId()),
					response, Constants.COMBINATION_ID);

			if(session.getAttribute(Constants.FIRST_TIME_VISIT)==null) {
				session.setAttribute(Constants.FIRST_TIME_VISIT, "true");
			} else {
				if(session.getAttribute(Constants.CLEAR_FIRST_TIME_VISIT)!=null && session.getAttribute(Constants.CLEAR_FIRST_TIME_VISIT).equals("false")){
					session.setAttribute(Constants.FIRST_TIME_VISIT, "false");
					session.setAttribute(Constants.CLEAR_FIRST_TIME_VISIT,"false");
				}else{
					session.setAttribute(Constants.FIRST_TIME_VISIT, "true");
					session.setAttribute(Constants.CLEAR_FIRST_TIME_VISIT,"true");
				}
			}
		} else if ( cookieFound ) { // found r cookie
				deleteCookie(refererCookie, response);   // delete r cookie
				if ( !combIdCookie  ) {
					addCookie(Constants.COMBINATION_ID, UsersManager.getCombIdByAdId(refererCookie.getValue()) , response, Constants.COMBINATION_ID);
				}
		}

		if ( cookieFound || combIdCookie ) {
			if(session.getAttribute(Constants.CLEAR_FIRST_TIME_VISIT)!=null && session.getAttribute(Constants.CLEAR_FIRST_TIME_VISIT).equals("false")) {
				session.setAttribute(Constants.FIRST_TIME_VISIT, "true");
				session.setAttribute(Constants.CLEAR_FIRST_TIME_VISIT, "true");
			} else {
				session.setAttribute(Constants.FIRST_TIME_VISIT, "false");
			}
		}

		// handle show movie parameter
		String showMovieParam = (String) request.getParameter(Constants.LANDING_PAGE_SHOW_MOVIE);
		if (null != showMovieParam && showMovieParam.equals("false")) {
			session.setAttribute(Constants.LANDING_PAGE_SHOW_MOVIE, "false");
		}

		// handle http referer not from landing
		if (null == session.getAttribute(Constants.HTTP_REFERE) &&
				UsersManager.getHttpRefererCookie(request) == null) {
			String httpRefererH = request.getHeader(Constants.HTTP_REFERE);
			if (null != httpRefererH && !httpRefererH.startsWith(Utils.getPropertyByHost(request, "homepage.url"))) {
				try {
					httpRefererH = URLEncoder.encode(httpRefererH, "UTF-8");
					session.setAttribute(Constants.HTTP_REFERE, httpRefererH);
					addCookie(Constants.HTTP_REFERE_COOKIE, httpRefererH, response, Constants.HTTP_REFERE);
				} catch (Exception e) {
					logger.warn("Error! problem to encode http referer: " + httpRefererH);
				}
			}
		}

		// check if we have calcalist game domain
		if (new URL(request.getRequestURL().toString()).getHost().indexOf(getCalcalistGameDomain()) > -1) {
			session.setAttribute(Constants.CALCALIST_GAME, "true");
		} else {
			session.setAttribute(Constants.CALCALIST_GAME, "false");
		}

		// handle click from the session
		String clickId = (String)session.getAttribute(Constants.CLICK_ID);
		if (!CommonUtil.isParameterEmptyOrNull(clickId)) {
			logger.log(Level.DEBUG, "Found click id in session, clickId:" + clickId + " , save to cookie.");
			addCookie(Constants.CLICK_ID, clickId, response, Constants.CLICK_ID);
		}

		return null;
	}

	/**
	 * Check if we got sMovie attribute in the session for disable movie on the beginning.
	 * the sMovie is a parameter that we can add in the request for disable Daniela movie
	 * @return
	 */
	public boolean isShowMovie() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		String sMovie = (String)session.getAttribute(Constants.LANDING_PAGE_SHOW_MOVIE);
		if (null != sMovie && sMovie.equals("false")) {
			return false;
		}
		return false;
	}

	public String getFalseFirstTimeVisit(){
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		String clearFirstTimeVisit = (String)session.getAttribute(Constants.CLEAR_FIRST_TIME_VISIT);

		if(null != clearFirstTimeVisit && clearFirstTimeVisit.equals("true")){
			session.setAttribute(Constants.FIRST_TIME_VISIT, "false");
		}
		if(null == clearFirstTimeVisit){
			session.setAttribute(Constants.FIRST_TIME_VISIT, "false");
		}
		return null;

	}
	public String getFirstTimeVisit(){
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		boolean isFirstTimeVisit = false;

		String firstTimeVisit = (String)session.getAttribute(Constants.FIRST_TIME_VISIT);

		if(null == firstTimeVisit){
			String clearFirstTimeVisit = (String)session.getAttribute(Constants.CLEAR_FIRST_TIME_VISIT);

			if(null != clearFirstTimeVisit){
				session.setAttribute(Constants.CLEAR_FIRST_TIME_VISIT,"false");
				isFirstTimeVisit = true;
			}
		}else{
			if (firstTimeVisit.equals("true")){
				isFirstTimeVisit = true;
			}
		}

		if (isFirstTimeVisit){
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			addTimeFirstVisitCookie(response);

			return "true";
		} else {
			return "false";
		}

	}
	public String getFirstVisitAO(){
		boolean firstTimeVisit=true;
		FacesContext context = FacesContext.getCurrentInstance();

		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();

		Cookie[] cs = request.getCookies();
		if(cs!=null){
			for (int i = 0; i < cs.length; i++) {
				if (cs[i].getName().equals(Constants.MOVIE_COOKIE)) {
					logger.log(Level.DEBUG, "found movie cookie!");
					firstTimeVisit = false;
				}
			}
		}

		//	no cookie found or movie attrubute not found on the request - add cookie for later use
		// note: we had a problem that we are setting 3 times the same cookie because of the jsf phases
		// for solving that, we added the movie attr in the same request

		if(firstTimeVisit &&
				null == request.getAttribute(Constants.MOVIE_COOKIE) ) {
			Cookie cookie = new Cookie(Constants.MOVIE_COOKIE,"1");
			cookie.setPath("/");
			cookie.setMaxAge(365 * 24 * 60 * 60 * 1000); // about year
			//sub domain
			String property = CommonUtil.getProperty(ConstantsBase.COOKIE_DOMAIN, null);
			if (null != property) {
				cookie.setDomain(property);
			}
			response.addCookie(cookie);
			request.setAttribute(Constants.MOVIE_COOKIE, "1");
			addTimeFirstVisitCookie(response);
		}

		return firstTimeVisit+"";
	}
	
	
	public boolean isETRegulation(){		
		boolean res = false;
		try {
			FacesContext context = FacesContext.getCurrentInstance();			
			HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
			res = (Boolean) session.getAttribute(Constants.SESSION_IS_ET_REGULATION);
		} catch (Exception e) {
			res = false;
		}
		return res; 
	}
	
	public boolean isShowBlockLoginUser(){		
		boolean res = false;
		try {
			Long sessionSkinId = Skin.SKIN_REG_EN;
			try {				
				FacesContext context = FacesContext.getCurrentInstance();			
				HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
				User user = (User)session.getAttribute(Constants.BIND_SESSION_USER);
				sessionSkinId = (Long) session.getAttribute(Constants.SESSION_SKIN);
				//If user is loggin not show
				if(null != user && user.getId() > 0){
					return false;
				}
			} catch (Exception e) {
				logger.error("When get session skinId", e);
			}	
			if(sessionSkinId == null){
				logger.debug("When isShowBlockLoginUser sessionSkinId is null set default 16");
				sessionSkinId = Skin.SKIN_REG_EN;
			}
			String ip = CommonUtil.getIPAddress();
			if(!CountryManagerBase.isCanLoginRegUser(sessionSkinId, ip, 0l)){
				res = true;
				logger.debug("isShowBlockLoginUser TRUE for IP:" + ip);
			}
		} catch (Exception e) {
			logger.error("When check isShowBlockLoginUser", e);
			res = false;
		}
		return res; 
	}
	
	public int aoQuestRegulationRes(){		
		int res = 0;
		try {
			FacesContext context = FacesContext.getCurrentInstance();			
			HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
			res = (Integer) session.getAttribute(Constants.SESSION_AO_REGULATION_QUESTIONNAIRE_RESULT);
		} catch (Exception e) {
			//not found
			res = 0;
		}
		return res; 
	}


	/**
	 * Save Click function
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public String getSaveClick() throws IOException, SQLException {

		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();

		// defult url
		String urlPage="index.jsf";
		String path = getHomePageUrl() + "jsp/";

		// check if we got redirectPage in the request
		String pageR = request.getParameter(Constants.LANDING_PAGE_REDIRECT);
		if (null != pageR) {
			urlPage = pageR + ".jsf";
		}

		// all the paramters from the incomeing url
		String quer = request.getQueryString();
		if (quer == null) {
			quer = "";
		} else {
			try {
				String goToGroupPageStr = request.getParameter("goToGroupPage");
				if (goToGroupPageStr!=null) {
					Integer goToGroupPage = Integer.parseInt(goToGroupPageStr);
					if (goToGroupPage == 1) {
						//get url page from db by groupId
						String groupIdStr = request.getParameter("groupId");
						if (groupIdStr!=null) {
							Integer groupId = Integer.parseInt(groupIdStr);
							ArrayList<MarketGroup> marketsGroup = getMarketsGroup();
							for (MarketGroup group : marketsGroup) {
								if (group.getId() == groupId ) {
									urlPage = group.getUrl()+"?";
									break;
								}
							}
						}
					}
				}
				getIsDynamicBanner();
			}
			catch(Exception e ) {
				logger.error("exception in saveclick: "+e);
				quer = request.getQueryString();
			}
		}

		quer = parseQueryString(quer);

		boolean cookieFound = false;
		boolean skinCookie = false;
		Cookie refererCookie = null;
		boolean combIdCookieFound = false;
//		Cookie dPCookie = null;  // dynamic param
		String skinValue = String.valueOf(Skin.SKIN_ETRADER);
		String combIdCookieValue = "";
		String dynamicParamCookieValue = null;
		String httpRefererCookieValue = null;
		Cookie[] cs = request.getCookies();
		if (cs != null) {
			for (int i = 0; i < cs.length; i++) {
				if (cs[i].getName().equals(Constants.SKIN_ID)) {
					skinCookie = true;
					skinValue = cs[i].getValue();   // save skinId cookie value
				} else if (cs[i].getName().equals(Constants.COMBINATION_ID)) {
					logger.log(Level.DEBUG, "found cmobid cookie! combid:" + cs[i].getValue());
					combIdCookieFound = true;
					combIdCookieValue = cs[i].getValue();
				} else if (cs[i].getName().equals(Constants.REFERRER)) {
					//logger.log(Level.DEBUG, "found referrer cookie!");
					cookieFound = true;
					refererCookie = cs[i];
				} else if (cs[i].getName().equals(Constants.DYNAMIC_PARAM)) {
					dynamicParamCookieValue = cs[i].getValue();
				} else if (cs[i].getName().equals(Constants.HTTP_REFERE_COOKIE)) {
					httpRefererCookieValue = cs[i].getValue();
				}
			}
		}

		if ( skinCookie ) {  // found skin cookie
			logger.log(Level.DEBUG, "found skinId cookie, value: " + skinValue);
		}

		String skinId = (String)session.getAttribute(Constants.SKIN_ID);
		if ( null == skinId || !skinId.equals( String.valueOf(Skin.SKIN_ETRADER)) ) {  // skinId not in Session
			if ( skinCookie == false || !skinValue.equals(Skin.SKIN_ETRADER) ) {  // skinId not in cookie
				addCookie(Constants.SKIN_ID, String.valueOf(Skin.SKIN_ETRADER), response, Constants.SKIN_ID_DESCRIPTION);
			}
			session.setAttribute(Constants.SKIN_ID, String.valueOf(Skin.SKIN_ETRADER));
			logger.debug("Saving skinId values in Session!");
		}

		// handle dynamic parameter
		String dynamicParam = (String) session.getAttribute(Constants.DYNAMIC_PARAM);
		if ( null == dynamicParam ) {  // not in session
			// try to take cookie.
			dynamicParam = dynamicParamCookieValue;
		}
		String decodedDP = null;
		if (null != dynamicParam) {
			try {
				// decode dynamic Parameter.
				decodedDP = URLDecoder.decode(dynamicParam, "UTF-8");
			} catch (Exception e) {
				logger.error("Error decoding dynamic param", e);
			}
		}

		// handle http referer
		String httpReferer = (String) session.getAttribute(Constants.HTTP_REFERE_LANDING);
		if ( null == httpReferer ) {  // not in session
			// try to take cookie.
			httpReferer = httpRefererCookieValue;
		}

		// handle show movie parameter
		String showMovieParam = (String) request.getParameter(Constants.LANDING_PAGE_SHOW_MOVIE);
		if (null != showMovieParam && showMovieParam.equals("false")) {
			session.setAttribute(Constants.LANDING_PAGE_SHOW_MOVIE, "false");
		}

		String referrer = (String) session.getAttribute(Constants.REFERRER);
		String combId = (String) session.getAttribute(Constants.COMBINATION_ID);
		String remarketingParam = (String) session.getAttribute(Constants.FROM_REMARKETING);

		if (combId != null) {

			// combination id exist in session! dont do nothing...
			CommonUtil.sendPermanentRedirect(response, path + urlPage + quer);

			return null;
		}

		if (referrer != null) {

			// referrer exist in session! add combination id
			session.setAttribute(Constants.COMBINATION_ID, UsersManager.getCombIdByAdId(referrer));
			CommonUtil.sendPermanentRedirect(response, path + urlPage + quer);

			return null;
		}

		referrer = request.getParameter(Constants.REFERRER);
		combId = request.getParameter(Constants.COMBINATION_ID);

		// check referrer & combination
		if ( null == referrer && null == combId ) {
			CommonUtil.sendPermanentRedirect(response, path + urlPage + quer);
			return null;
		}

		String ip = CommonUtil.getIPAddress();
		long clickId = 0;

		if ( null != referrer ) {

			long r = 0;
			try {
				r = Long.valueOf(referrer);
			} catch (Exception e) {
				CommonUtil.sendPermanentRedirect(response, path + urlPage + quer);
				return null;
			}

			Ad ad = AdminManager.getAd(r);
			if (ad == null) {
				logger.debug("Ad ID:" + r + " does not exists! no record inserted.");
				CommonUtil.sendPermanentRedirect(response, path + urlPage + quer);
				return null;
			} else {
				String combIdMap = UsersManager.getCombIdByAdId(referrer);
				combId = combIdMap;  // save for cookie
				logger.debug("inserting click record. adID=" + r + " ,ip=" + ip + " ,combid=" + combIdMap);
				clickId = AdminManager.insertClick(Long.valueOf(combIdMap), ip, decodedDP, null, null, httpReferer, null, null, null);
			}

		} else if ( null != combId ) {

			long combination = 0;
			try {
				combination = Long.valueOf(combId);
			} catch (Exception e) {
				CommonUtil.sendPermanentRedirect(response, path + urlPage + quer);
				return null;
			}

			MarketingCombination comb = AdminManager.getCombinationId(combination);
			if ( null == comb ) {
				logger.debug("Combination ID:" + combination + " does not exists! no record inserted.");
				CommonUtil.sendPermanentRedirect(response, path + urlPage + quer);
				return null;
			} else {
				logger.debug("inserting click record. combid=" + combination + " ,ip=" + ip);
				clickId = AdminManager.insertClick(combination, ip, decodedDP, null, null, httpReferer, null, null, null);
			}
		}

		// save clickId in the session
		if (clickId != 0){
			session.setAttribute(Constants.CLICK_ID, String.valueOf(clickId));
		}

		// if we dont have combId cookie or we do have but it's different from the one on session and from default add cookie
		if (!combIdCookieFound || (combIdCookieFound && !combId.equals(combIdCookieValue) &&
									!combId.equals(String.valueOf(getSkinById(getSkinId()).getDefaultCombinationId()))) ){

			logger.log(Level.DEBUG, "saving combinations id in cookie and session");
			// add combid cookie
			addCookie(Constants.COMBINATION_ID, combId, response, Constants.COMBINATION_ID);

			// also put the combination in the session
			session.setAttribute(Constants.COMBINATION_ID, combId);
		}


		if (!cookieFound && !combIdCookieFound) {
			session.setAttribute(Constants.CLEAR_FIRST_TIME_VISIT,"false");
		} else if ( cookieFound ) { // found r cookie
			deleteCookie(refererCookie, response);   // delete r cookie
		}
		
	    //handle remarketing data
		if (null == remarketingParam) {
			try {			
				if (request.getParameter(Constants.FROM_REMARKETING) != null && request.getParameter(Constants.FROM_REMARKETING).equalsIgnoreCase("true")) {
			    	session.setAttribute(Constants.FROM_REMARKETING, "true");
			    	session.setAttribute(Constants.REMARKETING_COMBINATION_ID, combId);	
					session.setAttribute(Constants.REMARKETING_DYNAMIC_PARAM, decodedDP);
				}
			} catch (Exception e) {
				logger.warn("Error! problem in saving remarketingParam in the session");
			}
		}

		CommonUtil.sendPermanentRedirect(response, path + urlPage + quer);
		return null;
	}

	public boolean getNotOnHomePage() {

		FacesContext context = FacesContext.getCurrentInstance();
		String view=context.getViewRoot().getViewId();

		if (view==null) {
			return true;
		}

		if (view.equals("") || view.equals("/") || view.indexOf("index")>-1) {
			return false;
		}

		return true;

	}

	public String getWriteError() {

		FacesContext context = FacesContext.getCurrentInstance();
		Map<String, Object> requestMap = context.getExternalContext().getRequestMap();
		Throwable ex = (Throwable) requestMap.get("javax.servlet.error.exception");
		String ls = System.getProperty("line.separator");

		Integer statusCode = (Integer)requestMap.get("javax.servlet.error.status_code");

		if (statusCode == null){
			logger.log(Level.INFO, "error page got request with no status_code");
			return null;
		}

		if (logger.isEnabledFor(Level.FATAL)) {
			if (ex != null) {
				List exceptions = ExceptionUtils.getExceptions(ex);
				Throwable throwable = (Throwable) exceptions.get(exceptions.size() - 1);
				StackTraceElement[] st = throwable.getStackTrace();
				StringBuffer trace = new StringBuffer("");
				for (int i = 0; i < st.length; i++) {
					trace.append(st[i].toString());
					trace.append(ls);
				}
				if (statusCode==404) {
					logger.log(Level.INFO, "Status code= " + requestMap.get("javax.servlet.error.status_code") + ls + "Caused by:" + trace.toString() + ls + ExceptionUtils
							.getExceptionMessage(exceptions) + ls);
				} else {
					logger.log(Level.FATAL, " Fatal Error :" + ls + "Status code= " + requestMap.get("javax.servlet.error.status_code") + ls + "Caused by:" + trace.toString() + ls + ExceptionUtils
						.getExceptionMessage(exceptions) + ls);
				}
			} else { // not an error
				if (statusCode==404) {
					logger.log(Level.INFO, "Status code= " + requestMap.get("javax.servlet.error.status_code") + " " + requestMap.get("javax.servlet.error.message") + ls);
				} else {
					logger.log(Level.FATAL, " Error :" + ls + "Status code= " + requestMap.get("javax.servlet.error.status_code") + " " + requestMap.get("javax.servlet.error.message") + ls);
				}
			}
		}
		return null;
	}

	public boolean getIsReceipt() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String view = request.getRequestURL() + "";
		if (view.indexOf("/receipt.jsf") > -1 || view.indexOf("/receiptFirstDeposit.jsf") > -1) {
			return true;
		}
		return false;
	}

	public String getIsLiveString() {
		if (CommonUtil.getProperty("system").equals("live")) {
			return "true";
		}
		return "false";
	}

	public boolean getIsLocal() {
		return getIsLocalStatic();
	}

    public static boolean getIsLocalStatic() {
        if (CommonUtil.getProperty("system").equals("local")) {
            return true;
        }
        return false;
    }

	public boolean getIsTest() {
		String system = CommonUtil.getProperty("system"); 
		if ("test".equalsIgnoreCase(system) || "local".equalsIgnoreCase(system)) {
			return true;
		}
		return false;
	}
	
	public static boolean isForceHttps() {
		String forceHttps = CommonUtil.getProperty("force.https");
		if (forceHttps.equals("true")) {
			return true;
		}
		return false;
	}

	public String getThreeDEndpoint() {
		return CommonUtil.getProperty("threed.endpoint.url");
	}
	
	public String getHomePageUrl() {
		if (isCalcalistGame()) {
			return CommonUtil.getProperty("homepage.url.cal");
		}
		if(!isEtraderSkin()) {
			try {
				String urlSkinPrefix = getPrefixBySkin();
				if (!CommonUtil.isParameterEmptyOrNull(urlSkinPrefix)) {
					return Utils.getPropertyByHost("homepage.url").replace("www", urlSkinPrefix);
				}
				return Utils.getPropertyByHost("homepage.url");
			} catch (SQLException e) {
				logger.error("can't parse homepage url, return default with www.", e);
				return Utils.getPropertyByHost("homepage.url");
			}
		} else {
			return Utils.getPropertyByHost("homepage.url");
		}
	}

	public String getHomePageUrlWithoutLastSlash() {
		return getHomePageUrl().substring(0, getHomePageUrl().length()-1);
	}

	public String getRhomePageUrl() {
		if(!isEtraderSkin()) {
			try {
				String urlSkinPrefix = getPrefixBySkin();
				if (!CommonUtil.isParameterEmptyOrNull(urlSkinPrefix)) {
					return Utils.getPropertyByHost("homepage.url").replace("www", urlSkinPrefix);
				}
				return Utils.getPropertyByHost("homepage.url");
			} catch (SQLException e) {
				logger.error("can't parse homepage url, return default with www.", e);
				return Utils.getPropertyByHost("homepage.url");
			}
		} else {
			return Utils.getPropertyByHost("homepage.url");
		}
	}

	public String gethomePageWebUrl() {
		if(!isEtraderSkin()) {
			try {
				String urlSkinPrefix = getPrefixBySkin();
				if (!CommonUtil.isParameterEmptyOrNull(urlSkinPrefix)) {
					return Utils.getPropertyByHost("homepage.url.web").replace("www", urlSkinPrefix);
				}
				return Utils.getPropertyByHost("homepage.url.web");
			} catch (SQLException e) {
				logger.error("can't parse homepage url, return default with www.", e);
				return Utils.getPropertyByHost("homepage.url.web");
			}
		} else {
			return Utils.getPropertyByHost("homepage.url.web");
		}
	}

	public String gethomePageMobileUrl() {
		return Utils.getPropertyByHost("homepage.url.mobile");
	}

	public String getHomePageMinisiteUrl(){
		return Utils.getPropertyByHost("homepage.url.minisite");
	}

	public String getHomePageContinueToWebsite(){
		return Utils.getPropertyByHost("homepage.url.from.minisite") + "?isContinue=true";
	}
	public String getCalcalistGameDomain() {
		return CommonUtil.getProperty("calcalist.game.domain");
	}

	public String getBlogUrl() {
		return CommonUtil.getProperty("blog.url");
	}

	/**
	 * Get etrader home page url
	 * @return
	 */
	public static String getEtraderHomePageUrl() {
		return Utils.getPropertyByHost("homepage.etrader.url");
	}

	/**
	 * Get localhost url property
	 */
	public String getLocalHostUrl() {
		if (isCalcalistGame()) {
			return CommonUtil.getProperty("localhost.url.cal");
		}
		return Utils.getPropertyByHost("localhost.url");
	}

    /**
     * Get default skin id property from Host property
     */
    public static String getDefaultSkin(HttpServletRequest request) {
        return Utils.getPropertyByHost(request, "skin.default");
    }

	/**
	 * Get default skin id from server property file
	 * @return
	 */
	public static String getInitDefaultSkin(){
		return CommonUtil.getProperty("skin.default");
	}

	public String getImagesSubDomain() {
		return Utils.getPropertyByHost("images.sub.domain");
	}

	public String getImagesPath() {
		String imagesDomain = getImagesSubDomain();
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
		String protocol = "http://";
		String port = request.getHeader("port");
		if (null != port) {
			if (port.equals("443")) {
				protocol = "https://";
			}
		}
		if (isForceHttps()) {
			protocol = "https://";
		}
		return protocol + imagesDomain;
	}
	public String getRegulationImagesPath(){
		if(CommonUtil.isUserSkinRegulated()){
			return "regulation/";
		}
		return "";
	}

	public String getProtocolHttpOrHttps() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
		String protocol = "http://";
		String port = request.getHeader("port");
		if (null != port) {
			if (port.equals("443")) {
				protocol = "https://";
			}
		}
		if (isForceHttps()) {
			protocol = "https://";
		}
		return protocol;
	}
	
	public Boolean getProtocolSecure() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
		Boolean protocol = false;
		String port = request.getHeader("port");
		if (null != port) {
			if (port.equals("443")) {
				protocol = true;
			}
		}
		if (isForceHttps()) {
			protocol = true;
		}
		return protocol;
	}

	public String getHomePageUrlHttps() {
		if (isCalcalistGame()) {
			return CommonUtil.getProperty("homepage.url.https.cal");
		}
		if(!isEtraderSkin()) {
			try {
				String urlSkinPrefix = getPrefixBySkin();
				if (!CommonUtil.isParameterEmptyOrNull(urlSkinPrefix)) {
					if(urlSkinPrefix.equalsIgnoreCase("www1")){
						return Utils.getPropertyByHost("homepage.url.https").replace("www", urlSkinPrefix).replace("com", "it");
					} else {						
						return Utils.getPropertyByHost("homepage.url.https").replace("www", urlSkinPrefix);
					}
				}
				return Utils.getPropertyByHost("homepage.url.https");
			} catch (SQLException e) {
				logger.error("can't parse homepage url, return default with www.", e);
				return Utils.getPropertyByHost("homepage.url.https");
			}
		} else {
			return Utils.getPropertyByHost("homepage.url.https");
		}
	}

	public String getDomainUrl() {
		if (isCalcalistGame()) {
			return CommonUtil.getProperty("domain.url.cal");
		}
		return Utils.getPropertyByHost("domain.url");
	}

    public String getLsServer() {
        return Utils.getPropertyByHost("lightstreamer.url");
    }

    public String getTradingPagesStatus() {
        return CommonUtil.getProperty("tradingpages.status");
    }

    public static String getServerId() {
        return CommonUtil.getProperty("server.id");
    }

    public String getRssFeedReloadSec() {
    	return String.valueOf((rssRefreshPeriod / 1000));
    }

	public String getPageScheme() {
		String useHttps = CommonUtil.getProperty("use.https");
		
		if(isForceHttps()){
			return "https";
		}else{
			if (!useHttps.equals("true")) {
				return "http";
			}
			
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			
			Enumeration<String> e = request.getHeaderNames();
			while(e.hasMoreElements()) {
				String header = (String) e.nextElement();
				if (header.equalsIgnoreCase("port")) {
					int usingPort = Integer.parseInt(request.getHeader(header).trim());
					if (usingPort == Constants.PORT_HTTP) {
						return "http";
					} else {
						return "https";
					}
				}
			}
			return "error!";
		}
	}

	public String getScheme() {
		String useHttps = CommonUtil.getProperty("use.https");

		if(isForceHttps()){
			return "https";
		}else if (useHttps.equals("true")) {
			return "https";
		}
		return "http";

	}

	public static String getSchemeStatic() {
		String useHttps = CommonUtil.getProperty("use.https");

		if(isForceHttps()){
			return "https";
		}else if (useHttps.equals("true")) {
			return "https";
		}
		return "http";
	}



	public String getPort() {
		String useHttps = CommonUtil.getProperty("use.https");

		if(isForceHttps()){
			return String.valueOf(Constants.PORT_HTTPS);
		}else if (useHttps.equals("true")) {
			return String.valueOf(Constants.PORT_HTTPS);
		}
		return String.valueOf(Constants.PORT_HTTP);

	}

	public long getWriterId() {
		return writerId;
	}

	public long getWriter() {
		return writerId;
	}

	public String getTimeMilis() {
		return String.valueOf(new Date().getTime());
	}

	public String getZoneOffset() {
		Calendar calendar=Calendar.getInstance();
		return String.valueOf(calendar.get(Calendar.ZONE_OFFSET));
	}
	public String getDstOffset() {
		Calendar calendar=Calendar.getInstance();
		return String.valueOf(calendar.get(Calendar.DST_OFFSET));
	}

	public String getDay() {
		return String.valueOf(Calendar.getInstance().get(Calendar.DATE));
	}

	public String getMonth() {
		return String.valueOf(Calendar.getInstance().get(Calendar.MONTH));
	}

	public String getYear() {
		return String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
	}

	public String getPortString() {

		if (CommonUtil.getProperty("system").equals("local")) {
			return "8080";
		}
		return "";

	}

	public static User getUserFromSession() throws Exception{
		FacesContext context=FacesContext.getCurrentInstance();
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		if (user.getId()==0) {
			HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
			session.invalidate();
			throw new Exception("User ID=0 Exception!!!!");
		}
		return user;
	}

	public String getRunDbCheck()  {
		Connection con = null;
		long timeBefore = 0;
		long timeAfter = 0;

		try {
			 timeBefore = System.currentTimeMillis();
			 con = getConnection();
			 GeneralDAO.getTestQuery(con);
			 timeAfter = System.currentTimeMillis();
			 logger.debug("Db check: ok, time: " + String.valueOf(timeAfter-timeBefore) + "ms");
			 return "dbok";

		} catch (Exception e) {
			logger.debug("Db check: not ok, time: " + String.valueOf(timeAfter-timeBefore) + "ms");

		} finally {
			try {
			closeConnection(con);
			} catch (Exception e){}
		}
		return "";
	}

	/**
	 * Check Oci driver
	 * @return
	 */
	public String getRunDbOciCheck()  {
		Connection con = null;
		try {
			logger.debug(checkNumber + " gc Oci db check, before getting connection: " + System.currentTimeMillis());
			con = getConnection();
			logger.debug(checkNumber + " gc Oci db check, after getting connection: " + System.currentTimeMillis());
			logger.debug(checkNumber + " ss Oci db check, before select sql: " + System.currentTimeMillis());
			GeneralDAO.getTestQuery(con);
			logger.debug(checkNumber + " ss Oci db check, after select sql: " + System.currentTimeMillis());
			logger.debug(checkNumber + " si Oci db check, before insert sql: " + System.currentTimeMillis());
			GeneralDAO.getTestInsertQuery(con);
			logger.debug(checkNumber + " si Oci db check, after insert sql: " + System.currentTimeMillis());

			return "dbOciOk";

		} catch (Exception e) {

		} finally {
			checkNumber++;
			try {
				closeConnection(con);
			} catch (Exception e){}
		}
		return "";
	}

	public String getRunLcCheck()  {
		FacesContext context = FacesContext.getCurrentInstance();
        LevelsCache levelsCache = (LevelsCache) context.getExternalContext().getApplicationMap().get("levelsCache");
        if (levelsCache.isJMSConnectionOK()) {
        	logger.debug("Lc check: ok");
        	return "lcok";
        } else {
        	logger.debug("Lc check: not ok");
        }
		return "";
	}

    public String getRunWlcCheck()  {
        FacesContext context = FacesContext.getCurrentInstance();
        WebLevelsCache levelsCache = (WebLevelsCache) context.getExternalContext().getApplicationMap().get("levelsCache");
        WebLevelLookupBean wllb = new WebLevelLookupBean();
        wllb.setOppId(-1);
        wllb.setFeedName("WLCCHECK");
        wllb.setSkinGroup(SkinGroup.ETRADER); // HARDCODED to avoid NPE
        levelsCache.getLevels(wllb);
        if (wllb.getDevCheckLevel() != 0 && wllb.getRealLevel() != 0) {
            logger.debug("Wlc check: ok");
            return "wlcok";
        } else {
            logger.debug("Wlc check: not ok");
        }
        return "";
    }

    // delete
    //public String getCheckIfError() throws Exception {
    //	return getCheckIfErrorWithParamFrom(Constants.LOGIN_FROM_REGULAR);
    //}
    
    // delete
    /**
     * Check if we have error in the login
     * @return
     * @throws Exception
    public String getCheckIfErrorWithParamFrom(long loginFrom) throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		int numberOfTries = 0;
		HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
		RequestParameterMap pm = (RequestParameterMap) context.getExternalContext().getRequestParameterMap();
		String reloadParam = (String) req.getParameter("reloadP");  //Note: parameter that added when we are reloading header page again for solving IE6 bug - cause double call
		boolean secondCall = false;
		if (null != reloadParam && reloadParam.equals("true")) {
			secondCall = true;
		}
		if (req.getParameter("error") != null) {
			
			FacesMessage fm;
			String loginTries = (String) session.getAttribute("logintries");
			if (loginTries != null) {
				try {
					numberOfTries = Integer.parseInt(loginTries);
					if (numberOfTries > Constants.MAX_ATTEMPTS_WITHOUT_USER) { // more
						// then
						// four
						// tries
						fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("header.login.locksessionerror", null),null);
						context.addMessage("errorlogin", fm);
						return "";
						// return true;
					}
				} catch (NumberFormatException e) {
					numberOfTries = 0;
					session.setAttribute("logintries", String.valueOf(numberOfTries));
				}
			}

			if (pm.get("j_username")==null) {
				return "";
			}
			String username = (String)pm.get("j_username");

			int failedCount = 0;
			Connection con = getConnection();
			try {
				failedCount = UsersDAO.getFailedCount(con, username);
			} finally {
				closeConnection(con);
			}

			int isActive = 0;
			int isDocumentsRequired = 0;
			con = getConnection();
			try {
				isActive = UsersDAO.getIsActive(con, username);
				isDocumentsRequired = UsersDAO.getIsDocsRequired(con, username);
			} finally {
				closeConnection(con);
			}

			if (failedCount == -1) {
				numberOfTries++;
				session.setAttribute("logintries", String.valueOf(numberOfTries));
				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("header.login.usersessionerror", null),null);
			} else if ((failedCount < 4) && (isActive == 1)) {
				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("header.login.error", null),null);
			} else if ((failedCount == 4) && (isActive == 1)) {
				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("header.login.lasterror", null),null);
			} else if ( (isActive == 0 ) && (isDocumentsRequired == 1 ) &&  !CommonUtil.isHebrewSkin(getSkinId())) {
				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("header.login.documents.needed.lockerror", null),null);
			} else if ( (isActive == 0 ) && (isDocumentsRequired == 1 ) &&   CommonUtil.isHebrewSkin(getSkinId())) {
				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("header.login.error", null),null);
			}
			else {
				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("header.login.lockerror", null),null);
			}
			context.addMessage("errorlogin", fm);
			return "";
		}
		return "";
	}
    */

    // delete
    /**
     * Return the error message key when trying to login
     * @return
     * @throws SQLException
     * @throws Exception
     
    public String checkIfLoginError(String userName, long loginFrom) throws SQLException  {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		int numberOfTries = 0;
		
		String loginTries = (String) session.getAttribute("logintries");
		if (loginTries != null) {
			try {
				numberOfTries = Integer.parseInt(loginTries);
				if (numberOfTries > Constants.MAX_ATTEMPTS_WITHOUT_USER) { // more then four tries
					return "header.login.locksessionerror";
				}
			} catch (NumberFormatException e) {
				numberOfTries = 0;
				session.setAttribute("logintries", String.valueOf(numberOfTries));
			}
		}

		if (userName == null) {
			return "header.login.error";
		}

		int failedCount = 0;
		Connection con = getConnection();
		try {
			failedCount = UsersDAO.getFailedCount(con, userName);
		} finally {
			closeConnection(con);
		}

		int isActive = 0;
		int isDocumentsRequired = 0;
		con = getConnection();
		try {
			isActive = UsersDAO.getIsActive(con, userName);
			isDocumentsRequired = UsersDAO.getIsDocsRequired(con, userName);
		} finally {
			closeConnection(con);
		}

		if (failedCount == -1) {
			numberOfTries++;
			session.setAttribute("logintries", String.valueOf(numberOfTries));
			return "header.login.usersessionerror";
		} else if ((failedCount < 4) && (isActive == 1)) {
			return "header.login.error";
		} else if ((failedCount == 4) && (isActive == 1)) {
			return "header.login.lasterror";
		} else if ( (isActive == 0 ) && (isDocumentsRequired == 1 ) &&  !CommonUtil.isHebrewSkin(getSkinId())) {
			return "header.login.documents.needed.lockerror";
		} else if ( (isActive == 0 ) && (isDocumentsRequired == 1 ) && CommonUtil.isHebrewSkin(getSkinId())) {
			return "header.login.error";
		} else {
			return "header.login.lockerror";
		}
	}
*/
	public void getIsDynamicBanner() throws Exception{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();

		String isOpenSlipStr = request.getParameter("isOpenSlip");
		String oppIdStr = request.getParameter("oppId");
		String type_idStr = request.getParameter("typeId");

		if (isOpenSlipStr != null && oppIdStr != null && type_idStr != null) {
			int isOpenSlip = Integer.parseInt(isOpenSlipStr);
			Long oppId = Long.parseLong(oppIdStr);
			int type_id = Integer.parseInt(type_idStr);
			Opportunity o = InvestmentsManager.getRunningOpportunityById(oppId);

			if (isOpenSlip == 1 && oppId != 0 && type_id != 0 && o.getMarket() != null && o.getMarketId() != 0) {
			    SlipForm sf = (SlipForm) context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), "#{slipForm}", SlipForm.class).getValue(context.getELContext());
				sf.addToSlip(oppId, type_id);
//				context.getApplication().createValueBinding("#{slipForm}").setValue(context, sf);
			}
		}
	}

	public String getBannerSaveClick() throws IOException, SQLException {

		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();

		// defult url
		String urlPage="";
		String path = getHomePageUrl() + "jsp/";

		// all the paramters from the incomeing url
		String quer = request.getQueryString();
		if (quer == null) {
			quer = "";
		} else {
			try {
				String goToGroupPageStr = request.getParameter("goToGroupPage");
				if (goToGroupPageStr!=null) {
					Integer goToGroupPage = Integer.parseInt(goToGroupPageStr);
					if (goToGroupPage == 1) {
						//get url page from db by groupId
						String groupIdStr = request.getParameter("groupId");
						if (groupIdStr!=null) {
							Integer groupId = Integer.parseInt(groupIdStr);
							ArrayList<MarketGroup> marketsGroup = getMarketsGroup();
							for (MarketGroup group : marketsGroup) {
								if (group.getId() == groupId ) {
									urlPage = group.getUrl()+"?";
									break;
								}
							}
						}
					}
				}

			}
			catch(Exception e ) {
				logger.error("exception in saveclick: "+e);
				quer = request.getQueryString();
			}
		}

		quer = parseQueryString(quer);

		boolean cookieFound = false;
		boolean skinCookie = false;
		boolean combIdCookieFound = false;
		Cookie dPCookie = null;
		String combIdCookieValue = "";
		Cookie[] cs = request.getCookies();
		Cookie refererCookie = null;
		String skinValue = String.valueOf(Skin.SKIN_ETRADER);

		if (cs != null) {
			for (int i = 0; i < cs.length; i++) {
				if (cs[i].getName().equals(Constants.SKIN_ID)) {
					skinCookie = true;
					skinValue = cs[i].getValue();
				} else if (cs[i].getName().equals(Constants.COMBINATION_ID)) {
					combIdCookieFound = true;
					combIdCookieValue = cs[i].getValue();
					logger.log(Level.DEBUG, "found combif cookie! combid:" + combIdCookieValue);
				} else if (cs[i].getName().equals(Constants.REFERRER)) {
					//logger.log(Level.DEBUG, "found referrer cookie!");
					cookieFound = true;
					refererCookie = cs[i];
				}
			}
		}

		if ( skinCookie ) {  // found skin cookie
			logger.log(Level.DEBUG, "found skinId cookie, value: " + skinValue);
		}

		String skinId = (String)session.getAttribute(Constants.SKIN_ID);
		if ( null == skinId || !skinId.equals( String.valueOf(Skin.SKIN_ETRADER)) ) {  // skinId not in Session
			if ( skinCookie == false || !skinValue.equals(Skin.SKIN_ETRADER) ) {  // skinId not in cookie
				addCookie(Constants.SKIN_ID, String.valueOf(Skin.SKIN_ETRADER), response, Constants.SKIN_ID_DESCRIPTION);
			}
			session.setAttribute(Constants.SKIN_ID, String.valueOf(Skin.SKIN_ETRADER));
			logger.debug("Saving skinId values in Session!");
		}

		// handle dynamic parameter
		String dynamicParam = (String) session.getAttribute(Constants.DYNAMIC_PARAM);

		if ( null == dynamicParam ) {  // not in session
			if ( null == dPCookie ) {  // dp not in cookie
				dynamicParam = (String) request.getParameter(Constants.DYNAMIC_PARAM);
				if ( null != dynamicParam ) {
					// save in  cookie
					addCookie(Constants.DYNAMIC_PARAM, dynamicParam, response, Constants.DYNAMIC_PARAM_DESCRIPTION);
				}
			} else { // dp  in cookie
				dynamicParam = dPCookie.getValue();
			}

			if ( null != dynamicParam ) {
				// save in session
				session.setAttribute(Constants.DYNAMIC_PARAM, dynamicParam);
			}
		}

		// handle show movie parameter
		String showMovieParam = (String) request.getParameter(Constants.LANDING_PAGE_SHOW_MOVIE);
		if (null != showMovieParam && showMovieParam.equals("false")) {
			session.setAttribute(Constants.LANDING_PAGE_SHOW_MOVIE, "false");
		}

		String referrer = (String) session.getAttribute(Constants.REFERRER);
		String combId = (String) session.getAttribute(Constants.COMBINATION_ID);

		if (getFirstTimeVisit().equals("true") && isShowMovie() && isCalcalistGame() ){
			referrer = null;
			combId = null;
		}

		if (combId != null) {

			// combination id exist in session! dont do nothing...
			CommonUtil.sendPermanentRedirect(response, path + urlPage + quer);

			return null;
		}

		if (referrer != null) {

			// referrer exist in session! add combination id
			session.setAttribute(Constants.COMBINATION_ID, UsersManager.getCombIdByAdId(referrer));
			CommonUtil.sendPermanentRedirect(response, path + urlPage + quer);

			return null;
		}

		referrer = request.getParameter(Constants.REFERRER);
		combId = request.getParameter(Constants.COMBINATION_ID);

		// check referrer & combination
		if ( null == referrer && null == combId ) {
			//response.sendRedirect(urlPage + quer);
			return null;
		}

		String ip = CommonUtil.getIPAddress();
		long clickId = 0;

		if ( null != referrer ) {

			long r = 0;
			try {
				r = Long.valueOf(referrer);
			} catch (Exception e) {
				CommonUtil.sendPermanentRedirect(response, path + urlPage + quer);
				return null;
			}

			Ad ad = AdminManager.getAd(r);
			if (ad == null) {
				logger.debug("Ad ID BANNER:" + r + " does not exists! no record inserted.");
				//response.sendRedirect(urlPage + quer);
				return null;
			} else {
				String combIdMap = UsersManager.getCombIdByAdId(String.valueOf(r));
				combId = combIdMap;  // save for cookie
				logger.debug("inserting click record.  banner adID=" + r + " ,ip=" + ip + " ,combid=" + combIdMap);
				clickId = AdminManager.insertClick(Long.valueOf(combIdMap), ip, dynamicParam, null, null, null, null, null, null);
				session.setAttribute(Constants.CLICK_ID, String.valueOf(clickId));
			}

		} else if ( null != combId ) {

			long combination = 0;
			try {
				combination = Long.valueOf(combId);
			} catch (Exception e) {
				CommonUtil.sendPermanentRedirect(response, path + urlPage + quer);
				return null;
			}

			MarketingCombination comb = AdminManager.getCombinationId(combination);
			if ( null == comb ) {
				logger.debug("Combination ID:" + combination + " does not exists! no record inserted.");
				//response.sendRedirect(urlPage + quer);
				return null;
			} else {
				logger.debug("inserting click record. combid=" + combination + " ,ip=" + ip);
				clickId = AdminManager.insertClick(combination, ip, dynamicParam, null, null, null, null, null, null);
				session.setAttribute(Constants.CLICK_ID, String.valueOf(clickId));
			}
		}

		// if we dont have combId cookie or we do have but it's different from the one on session and from default add cookie
		if (!combIdCookieFound || (combIdCookieFound && !combId.equals(combIdCookieValue) &&
									!combId.equals(String.valueOf(getSkinById(getSkinId()).getDefaultCombinationId()))) ){
			logger.log(Level.DEBUG, "saving combinations id in cookie and session");
			// add combid cookie
			addCookie(Constants.COMBINATION_ID, combId, response, Constants.COMBINATION_ID);

			// also put the combination in the session
			session.setAttribute(Constants.COMBINATION_ID, combId);

		} else if ( cookieFound ) { // found r cookie
			deleteCookie(refererCookie, response);   // delete r cookie
		}

		//response.sendRedirect(urlPage + quer);
		return null;

	}

	/**
	 * Get market per skin for HomePage
	 * @return
	 * 		List of markets
	 * @throws SQLException
	 */
//    public ArrayList<SelectItem> getAssets() throws SQLException {
//    	long skinId = getSkinId();
//    	buildMarketsHomePageList();
//		Skins skin = getSkinById(skinId);
//    	ArrayList<SelectItem> assets = skin.getSkinMarketsHomePageList();
//        return assets;
//    }

    /**
     * Get active markets per skin for select box
     * @return
     *      List of markets
     * @throws SQLException
     */
//    public ArrayList<SelectItem> getMarketsListSI() throws SQLException {
//    	Skins skin = getSkinById(getSkinId());
//        ArrayList<SelectItem> assets = skin.getMarketsListSI();
//        if (null == assets) {
//            assets = SkinsManagerBase.getMarketsSI(skin.getId());
//            skin.setMarketsListSI(assets);
//        }
//        return assets;
//    }

    /**
     * Get the first message that should be showing now on the specified screen it runs at.
     *
     * @param webScreen
     * @return
     */
    protected Message getMessage(long webScreen) {
        if (logger.isDebugEnabled()) {
            logger.debug("Get message for: " + webScreen);
        }
    	if (System.currentTimeMillis() - messagesLoadTime > messagesRefreshPeriod) {
            if (logger.isDebugEnabled()) {
                logger.debug("Realod page messages. Refresh period: " + messagesRefreshPeriod);
            }
            try {
                messages = MessagesManager.getAllMessages();
                messagesLoadTime = System.currentTimeMillis();
            } catch (SQLException sqle) {
                logger.error("Can't load trading pages messages.", sqle);
            }
        }
        Message msg = null;
        try {
            if (null != messages) {
            	long skinId = getSkinId();
            	String languageCode = "";
            	if ((skinId == Skin.SKIN_ETRADER) || (skinId == Skin.SKIN_TLV)) {
            		languageCode = ConstantsBase.ETRADER_LOCALE;
            	} else {
            		languageCode = getUserLocale().getLanguage();
            	}
            	long userLanguageId = -1;
            	ArrayList<Language> langs = getLanguagesList();
            	for(int i=0; i<langs.size(); i++ ) {
            		Language l = langs.get(i);
            		if (l.getCode().equalsIgnoreCase(languageCode)) {
            			userLanguageId = l.getId();
            			break;
            		}
            	}
                Date now = new Date();
                Message m = null;
                for (int i = 0; i < messages.size(); i++) {
                    m = messages.get(i);
                    if (m.getWebScreen() == webScreen &&
                            m.getStartEffDate().before(now) &&
                            m.getEndEffDate().after(now) &&
                            m.getSkinId() == skinId && m.getLanguageId() == userLanguageId ) {
                        msg = m;
                        break;
                    }
                }
            }
        } catch (Exception e) {
        	 logger.error("Can't load msg for user.", e);
        }
        return msg;
    }

    /**
     * @return Home page message if there is one to be displayed now.
     */
    public Message getMessageHome() {
        return getMessage(Message.WEB_SCREEN_HOME);
    }

    /**
     * @return Indices page message if there is one to be displayed now.
     */
    public Message getMessageIndices() {
        return getMessage(Message.WEB_SCREEN_INDICES);
    }

    /**
     * @return Stocks page message if there is one to be displayed now.
     */
    public Message getMessageStocks() {
        return getMessage(Message.WEB_SCREEN_STOCKS);
    }

    /**
     * @return etrader indices page message if there is one to be displayed now.
     */
    public Message getMessageEtraderIndices() {
        return getMessage(Message.WEB_SCREEN_ETRADER_INDICES);
    }


    /**
     * @return Currencies page message if there is one to be displayed now.
     */
    public Message getMessageCurrencies() {
        return getMessage(Message.WEB_SCREEN_CURRENCIES);
    }

    /**
     * @return Commodities page message if there is one to be displayed now.
     */
    public Message getMessageCommodities() {
        return getMessage(Message.WEB_SCREEN_COMMODITIES);
    }

    /**
     * @return Foreign stocks page message if there is one to be displayed now.
     */
    public Message getMessageForeignStocks() {
        return getMessage(Message.WEB_SCREEN_FOREIGN_STOCKS);
    }

    /**
     * @return One touch page message if there is one to be displayed now.
     */
    public Message getMessageOneTouch() {
        return getMessage(Message.WEB_SCREEN_ONE_TOUCH);
    }

	/**
	 * Save cookie and session attributes for anyOption
	 * @return
	 * @throws SQLException
	 * @throws IOException
	 */
	public String getSaveAnyOptionCookie() throws SQLException, IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		return getSaveAnyOptionCookie(session, request, response);
	}

	/**
	 * This method save the parameters which transfered for tracking in cookies.
	 * @return
	 */
	public boolean saveMobileLandingCookie() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		Map<String, String> pm = context.getExternalContext().getRequestParameterMap();
		StringBuffer sb = new StringBuffer();
		String ls = System.getProperty("line.separator");

        for (Iterator<String> i = pm.keySet().iterator(); i.hasNext();) {
            String key = (String) i.next();
            String value = pm.get(key).toString();
            sb.append(key).append(": ").append(pm.get(key)).append(ls);

            addCookie(key, value, response, key);
        }
        logger.debug("save anyoption cookie from mobile: " + sb);
		return true;
	}

	/**
	 * @return cookies parameters in a query string.
	 */
	public StringBuffer getCookieQueryString() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		StringBuffer cookieQueryString = new StringBuffer();
		Cookie[] cookies = request.getCookies();

		if (cookies != null) {
			for (Cookie cookie : cookies) {
				cookieQueryString.append(cookie.getName()).append("=").append(cookie.getValue()).append("&");
			}
			cookieQueryString.deleteCharAt(cookieQueryString.length() - 1);
		}
		return cookieQueryString;
	}

	/**
	 * Save cookie and session attributes for anyOption
	 * @return
	 * @throws SQLException
	 * @throws IOException
	 */
	public static String getSaveAnyOptionCookie(HttpSession session, HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// Get the Locale and Check first if we came from israel and redirect to etrader
		String ip = CommonUtil.getIPAddress(request);
		logger.debug("Get ip to check if redirect to etrader needed, ip: " + ip);
		if (getIsLocalStatic()) {
			ip = "74.125.67.100";   // en us
			logger.debug("local system, change the ip hardcoded! to " + ip);
		}
		
		String dpn = (String) session.getAttribute(Constants.DPN);
		// handle trade doubler parameter
		if (null == dpn) {  // not in session
   			dpn = (String) request.getParameter(Constants.DPN);
			if (null != dpn) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.DPN, dpn);
			}
		}

		// handle redirect parameter
		String fromIsrael = "false";

		fromIsrael = (String)session.getAttribute(Constants.FROM_REDIRECT);
		if ( null == fromIsrael) {
			fromIsrael = request.getParameter(Constants.FROM_REDIRECT);
			if ( null != fromIsrael && fromIsrael.equalsIgnoreCase(Constants.FROM_REDIRECT_ISRAEL)) {  // save attribute in the session
				session.setAttribute(Constants.FROM_REDIRECT, fromIsrael);
			}
		}

		boolean combIdCookieFound = false;			  // for combid cookie
		boolean recognitionCookiesFound = false;  // for all recognition cookies
		HashMap<String, Boolean> recognitionCookies = new HashMap<String, Boolean>();
		recognitionCookies.put(Constants.SKIN_ID, false);
		recognitionCookies.put(Constants.CURRENCY_ID, false);
		recognitionCookies.put(Constants.LANGUAGE_ID, false);
		recognitionCookies.put(Constants.COUNTRY_ID, false);   // real country id
		String skinIdCookieValue = "";
		String languageIdCookieValue = "";
		String currencyIdCookieValue = "";
		String countryIdCookieValue = "";
		String combIdCookieValue = "";
		Cookie dpCookie = null;
		Cookie sAffCookie = null;
		boolean isHasFirstCombCookie = false;

		Cookie[] cs = request.getCookies();
		if (cs != null) {
			for (int i = 0; i < cs.length; i++) {
				if (cs[i].getName().equals(Constants.COMBINATION_ID)) {
					logger.log(Level.DEBUG, "found combid cookie! combid:" + cs[i].getValue());
					combIdCookieFound = true;
					combIdCookieValue = cs[i].getValue();
				} else if (cs[i].getName().equals(Constants.SKIN_ID) ) {
					recognitionCookies.put(Constants.SKIN_ID, true);
					skinIdCookieValue = cs[i].getValue();
				} else if (cs[i].getName().equals(Constants.CURRENCY_ID) ) {
					recognitionCookies.put(Constants.CURRENCY_ID, true);
					currencyIdCookieValue = cs[i].getValue();
				} else if (cs[i].getName().equals(Constants.LANGUAGE_ID) ) {
					recognitionCookies.put(Constants.LANGUAGE_ID, true);
					languageIdCookieValue = cs[i].getValue();
				} else if (cs[i].getName().equals(Constants.COUNTRY_ID) ) {
					recognitionCookies.put(Constants.COUNTRY_ID, true);
					countryIdCookieValue = cs[i].getValue();
				} else if (cs[i].getName().equals(Constants.DYNAMIC_PARAM)) {
					dpCookie = cs[i];
				} else if (cs[i].getName().equals(Constants.SUB_AFF_KEY_PARAM)) {
					sAffCookie = cs[i];
				} else if (cs[i].getName().equals(Constants.FIRST_COMBINATION_ID)) {
					isHasFirstCombCookie = true;
				} else if (cs[i].getName().equals(Constants.LANDING_QUARY_COOKIE)) {
				}
			}
		}

		// recognition cookies check
		if (recognitionCookies.get(Constants.SKIN_ID) &&  recognitionCookies.get(Constants.CURRENCY_ID)
				&& recognitionCookies.get(Constants.LANGUAGE_ID) && recognitionCookies.get(Constants.COUNTRY_ID) ) {

			logger.log(Level.DEBUG, "found recognition cookies!");
			recognitionCookiesFound = true;
			if (skinIdCookieValue.equals(String.valueOf(Skin.SKIN_API))) {  // replace skin 7 parameter to defualt skin
				recognitionCookiesFound = false;
			}
		}

		// handle skinId in the request
		boolean skinParameter = false;
		String skinIdParameter = request.getParameter(Constants.SKIN_ID);   // get skinId from request
		if (CommonUtil.isParameterEmptyOrNull(skinIdParameter)) {  // search for s param from landing
			skinIdParameter = (String) session.getAttribute(Constants.SKIN_ID_FROM_LANDING);
			if (null != skinIdParameter) { // clean the attribute
				session.setAttribute(Constants.SKIN_ID_FROM_LANDING, null);
			}
		}
		long sParam = 0;
		if (!CommonUtil.isParameterEmptyOrNull(skinIdParameter) && !skinIdParameter.equals(String.valueOf(Skin.SKIN_TLV))) {
			try {
				sParam = Integer.parseInt(skinIdParameter);
				if ( null != getSkinById(sParam) ) {   // real skinId
					if (sParam == Skin.SKIN_API) {  // replace skin 7 parameter to defualt skin
						sParam = Skin.SKINS_DEFAULT;
					}
					if (sParam == Skin.SKIN_SPAINMI) {  // replace skin 6 to skin 5
						sParam = Skin.SKIN_SPAIN;
					}
					skinParameter = true;
					logger.log(Level.DEBUG, "Found skinId parameter in the request queryString, s:" + sParam);
				}
			} catch (Exception e) {   // skinId not a number
				skinParameter = false;
			}
		}

		if (!skinParameter) {  // check skin change from Pretty URL
			Long skinFromURL = (Long) request.getAttribute(Constants.PRETTY_SKIN_ID_ATR);
			if (null != skinFromURL) {
				sParam = skinFromURL;
				skinParameter = true;
			}
		}

		String combId = (String) session.getAttribute(Constants.COMBINATION_ID);
		String skin = (String) session.getAttribute(Constants.SKIN_ID);
		String currency = (String) session.getAttribute(Constants.CURRENCY_ID);
		String language = (String) session.getAttribute(Constants.LANGUAGE_ID);
		String countryIdStr = (String) session.getAttribute(Constants.COUNTRY_ID);
		Locale locale = (Locale)session.getAttribute(Constants.USER_LOCALE);
		String queryString = (String) session.getAttribute(Constants.QUERY_STRING);
		// handle queryString
		if (null == queryString) {
			queryString = request.getQueryString();
			if (null != queryString) {
				session.setAttribute(Constants.QUERY_STRING, queryString);
			}
		}

		if (null != combId && null != skin && null != currency && null != countryIdStr
				&& null != language && null != locale && recognitionCookiesFound ) {

			if (skinParameter) {  // in case we have s parameter in the request
				if (sParam == Skin.SKIN_API) {  // replace skin 7 parameter to defualt skin
					sParam = Skin.SKINS_DEFAULT;
				}
				if (Long.valueOf(skin).longValue() == sParam) {   // the same skin
					return null;
				} else {   // other skin
					createLocaleBySkinId(session, request, response, sParam, true, null, null, null);
					return null;
				}
			}
			return null;
		}

		if (!recognitionCookiesFound || skinParameter) {
			logger.debug("Starting to calculate counrtyCode by ip: " + ip);
			// country recognition
			StringBuffer urlBuffer = request.getRequestURL();
			String[] urlSplit = urlBuffer.toString().split("/");
			String url = urlSplit[2];   // take domain

			Connection con = null;
			try {
				con = getConnection();
				int urlId  = UrlsDAO.getIdByUrl(con, url);
				if ( urlId == 0 ) {
					urlId = ConstantsBase.ANYOPTION_URL;
				}

				long skinId = Long.parseLong(getDefaultSkin(request));
				if (urlId == ConstantsBase.MIOPCIONES_URL) {
					skinId = Skin.SKIN_SPAINMI;
				}

				if (getIsLocalStatic() && url.indexOf("miopciones.com") > -1) {
					urlId = ConstantsBase.MIOPCIONES_URL;
					skinId = Skin.SKIN_SPAINMI;
				}

				long realCountryId = 0;  // not recognized
				logger.info("Url received: " + url + " UrlID:" + urlId);

			 	//Locale userLocale = InetAddressLocator.getLocale(ip);
				String countryCode = CommonUtil.getCountryCodeByIp(ip);
				String countryA2FromUrl = request.getParameter(Constants.FORCE_REGULATION_COUNTRY);
				if (!CommonUtil.isParameterEmptyOrNull(countryA2FromUrl) && (GeneralManager.getAllowIPList().contains(ip) || getIsLocalStatic())) { // In order to simulate regulated country
					countryCode = countryA2FromUrl;
				}
				if (!CommonUtil.isParameterEmptyOrNull(countryCode)) { // no need country recognition for skinEsMiop

					logger.debug("Get Counrty code from Ip, Country code: " + countryCode);

					// get Country id from Db
					long countryId = CountryDAOBase.getIdByCode(con, countryCode);
					if (countryId == 0) {  // country not in Db, set to default skin
//						skinId = Skin.SKINS_DEFAULT; // Tony: The skinId already is set to the default skin for this application that might be different from Skins.SKINS_DEFAULT
						logger.debug("Country not exists in the DB, Take default skin. ");
					} else { // country found
						realCountryId = countryId; // save country id
						long skinIdByCountryAndUrl = SkinUrlCountryMapDAOBase.getSkinIdByCountryAndUrl(con, countryId, urlId);
						if (skinIdByCountryAndUrl != 0) {
						    skinId = skinIdByCountryAndUrl;
						}
					}
			    }

				if (urlId == ConstantsBase.MIOPCIONES_URL) {
					skinId = Skin.SKIN_SPAINMI;
				}

			    if ( skinParameter ) {  // found skinId in queryString
			    	skinId = sParam;				
			    }

			    if (skinId == Skin.SKIN_SPAINMI) {  // replace skin 6 to skin 5
			    	skinId = Skin.SKIN_SPAIN;
				}
			    
			    ;
			    
			    Skin s = SkinsManagerBase.getById(skinId);
			    if(s.getBusinessCaseId() != Skin.SKIN_BUSINESS_GOLDBEAM && SkinsManagerBase.getRegulatedSkinIdByNonRegulatedSkinId(skinId) != skinId) {
		    		skinId = SkinsManagerBase.getRegulatedSkinIdByNonRegulatedSkinId(skinId);
		    	}
			    
				Skins skinObj = getSkinById(skinId);
				if (null != skinObj) {
					String defaultLanguageId = String.valueOf(skinObj.getDefaultLanguageId());
					CurrenciesRules currenciesRules = new CurrenciesRules();
					currenciesRules.setCountryId(realCountryId);
					currenciesRules.setSkinId(skinObj.getId());
					String defaultCurrencyId = String.valueOf(CurrenciesRulesManagerBase.getDefaultCurrencyId(currenciesRules));
					if (realCountryId == 0) {
					    realCountryId = skinObj.getDefaultCountryId();
					}

					// add cookies
					addCookie(Constants.SKIN_ID, String.valueOf(skinId), request, response, Constants.SKIN_ID_DESCRIPTION);
					addCookie(Constants.CURRENCY_ID, defaultCurrencyId, request, response, Constants.CURRENCY_ID_DESCRIPTION);
					addCookie(Constants.LANGUAGE_ID, defaultLanguageId, request, response, Constants.LANGUAGE_ID_DESCRIPTION);
					addCookie(Constants.COUNTRY_ID, String.valueOf(realCountryId), request, response, Constants.COUNTRY_ID_DESCRIPTION);
					addCookie(Constants.IS_REGULATED, String.valueOf(skinObj.isRegulated()), request, response, Constants.IS_REGULATED_DESCRIPTION);
					// add to session
					session.setAttribute(Constants.SKIN_ID, String.valueOf(skinId));
					session.setAttribute(Constants.CURRENCY_ID, defaultCurrencyId);
					session.setAttribute(Constants.LANGUAGE_ID, defaultLanguageId);
					session.setAttribute(Constants.COUNTRY_ID, String.valueOf(realCountryId));
					session.setAttribute(Constants.IS_REGULATED, String.valueOf(skinObj.isRegulated()));
					logger.debug("Saving recognition values in Session! , skinId: " + skinId);

					// 	save user Locale (by skin) in the session
					String counrtyCode = CountryDAOBase.getCodeById(con, skinObj.getDefaultCountryId());
					String languageCode = LanguagesDAOBase.getCodeById(con, skinObj.getDefaultLanguageId());
					if (skinObj.isRegulated()) {
					    session.setAttribute(Constants.USER_LOCALE, new Locale(languageCode, "EU"));
					} else {
                        session.setAttribute(Constants.USER_LOCALE, new Locale(languageCode, counrtyCode));
					}
					logger.debug("Saving user Locale in Session! languageCode: " + languageCode +
							" countryCode: " +counrtyCode);
				}
			} finally {
				closeConnection(con);
			}
		} else { // found cookies but Locale not in session
				session.setAttribute(Constants.SKIN_ID, skinIdCookieValue);
				session.setAttribute(Constants.CURRENCY_ID, currencyIdCookieValue);
				session.setAttribute(Constants.LANGUAGE_ID, languageIdCookieValue);
				session.setAttribute(Constants.COUNTRY_ID, countryIdCookieValue);
				logger.debug("Saving recognition values in Session!, skinId: " + skinIdCookieValue);

				// 	save user Locale (by skin) in the session
				Connection con = getConnection();
				try {
					Skins skinObj = getSkinById(Long.valueOf(skinIdCookieValue));
					String counrtyCode = CountryDAOBase.getCodeById(con, skinObj.getDefaultCountryId());
					String languageCode = LanguagesDAOBase.getCodeById(con, skinObj.getDefaultLanguageId());
					if (skinObj.isRegulated()) {
					    session.setAttribute(Constants.USER_LOCALE, new Locale(languageCode, "EU"));
					} else {
                        session.setAttribute(Constants.USER_LOCALE, new Locale(languageCode, counrtyCode));
					}
				}	finally {
						closeConnection(con);
				}
	    }

		String combIdToSetSession = null;
		String combIdToSetCookie = null;
		boolean isNewComb = false;
		//String redirectToLandingPage = null;

		if (null == combId) {  // not is session
			if (combIdCookieFound) {
				combIdToSetSession = combIdCookieValue;
			} else {
				combIdToSetSession = String.valueOf(getSkinById(getSkinId(session, request)).getDefaultCombinationId());
				combIdToSetCookie = combIdToSetSession;
/*				String host = new URL(request.getRequestURL().toString()).getHost();
				if (null != host && !host.startsWith(Constants.SUBDOMAIN_MOBILE_ANYOPTION + ".")) {
					if (combIdToSetCookie.equals(String.valueOf(getSkinById(Constants.SKIN_ENGLISH).getDefaultCombinationId()))) {  // redirect to landing by default en combid
						redirectToLandingPage = request.getContextPath() + "/jsp/newFullRegPL.jsf?s=" + Constants.SKIN_ENGLISH;
					} else if (combIdToSetCookie.equals(String.valueOf(getSkinById(Constants.SKIN_SPAIN).getDefaultCombinationId()))) {  // redirect to landing by default es combid
						redirectToLandingPage = request.getContextPath() + "/jsp/newFullRegPL.jsf?s=" + Constants.SKIN_SPAIN;
					}
				}
*/			}
		} else {  // in session. if we dont have combId cookie or we do have but it's different from the one on session and from default
			if (!combIdCookieFound || (combIdCookieFound && !combId.equals(combIdCookieValue) &&
										!combId.equals(String.valueOf(getSkinById(getSkinId(session, request)).getDefaultCombinationId()))) ) {
				combIdToSetCookie = combId;
				isNewComb = true;
			} else {  // don't override
				combIdToSetSession = combIdCookieValue;
			}
		}

		if (null != combIdToSetSession) {
			session.setAttribute(Constants.COMBINATION_ID, combIdToSetSession);
		}

		// Add COMBINATION_ID and DYNAMIC_PARAM cookie, if it's first time we come with combId
		// OR we come from Netrefer and in the cookie we have a value -> and this value different from Netrefer.
		boolean addDpToCookie = false;
		if (null != combIdToSetCookie) {
			if(!combIdCookieFound) {
				addDpToCookie = true;
				addCookie(Constants.COMBINATION_ID, combIdToSetCookie, request, response, Constants.COMBINATION_ID);
			} else if ((isNetreferCombination(Long.valueOf(combIdCookieValue)) || isReferpartnerCombination(Long.valueOf(combIdCookieValue))) ||
							((!isNetreferCombination(Long.valueOf(combIdToSetCookie)) && !isNetreferCombination(Long.valueOf(combIdCookieValue))) || (!isReferpartnerCombination(Long.valueOf(combIdToSetCookie)) && !isReferpartnerCombination(Long.valueOf(combIdCookieValue))))) {
				addDpToCookie = true;
				addCookie(Constants.COMBINATION_ID, combIdToSetCookie, request, response, Constants.COMBINATION_ID);
			}
		}


		// handle dp from the session
		String dpSession = (String)session.getAttribute(Constants.DYNAMIC_PARAM);
		if (!CommonUtil.isParameterEmptyOrNull(dpSession)) {
			logger.log(Level.DEBUG, "Found dp in session, dp:" + dpSession + " , save to cookie.");
			if (dpSession.indexOf(Constants.GOOGLE_DP_KWORD) > -1) {  // use UTF-8 encoding for google dp that contains kword with international characters
				dpSession = URLEncoder.encode(dpSession, "UTF-8");
			}
			if (addDpToCookie) {
				addCookie(Constants.DYNAMIC_PARAM, dpSession, request, response, Constants.DYNAMIC_PARAM_DESCRIPTION);
			}
		} else {
			if (isNewComb && null != dpCookie) {  // new combination without dp
				//deleteCookie(dpCookie, response);
				addCookie(Constants.DYNAMIC_PARAM, "", request, response, Constants.DYNAMIC_PARAM_DESCRIPTION);
			}
		}
		
		String affSub1 = (String)session.getAttribute(Constants.AFF_SUB1);
		if (!CommonUtil.isParameterEmptyOrNull(affSub1)) {		
			addCookie(Constants.AFF_SUB1, (String)request.getParameter(Constants.AFF_SUB1), request, response, Constants.AFF_SUB1);
		}
		
		String affSub2 = (String)session.getAttribute(Constants.AFF_SUB2);
		if (!CommonUtil.isParameterEmptyOrNull(affSub2)) {		
			addCookie(Constants.AFF_SUB2, (String)request.getParameter(Constants.AFF_SUB2), request, response, Constants.AFF_SUB2);
		}

		String affSub3 = (String)session.getAttribute(Constants.AFF_SUB3);
		if (!CommonUtil.isParameterEmptyOrNull(affSub3)) {		
			addCookie(Constants.AFF_SUB3, Constants.GCLID + "_"+(String)request.getParameter(Constants.GCLID), request, response, Constants.AFF_SUB3);
		}

		// handle click from the session
		String clickId = (String)session.getAttribute(Constants.CLICK_ID);
		if (!CommonUtil.isParameterEmptyOrNull(clickId)) {
			logger.log(Level.DEBUG, "Found click id in session, clickId:" + clickId + " , save to cookie.");
			addCookie(Constants.CLICK_ID, clickId, request, response, Constants.CLICK_ID);
		}

		// handle subAff from the session
		String subAffSession = (String)session.getAttribute(Constants.SUB_AFF_KEY_PARAM);
		if (!CommonUtil.isParameterEmptyOrNull(subAffSession)) {
			logger.log(Level.DEBUG, "Found sub affiliate id in session, subAffKey:" + subAffSession + " , save to cookie.");
			addCookie(Constants.SUB_AFF_KEY_PARAM, subAffSession, request, response, Constants.SUB_AFFILIATE_ID_DESCRIPTION);
		} else {
			if (isNewComb && null != sAffCookie) {  // new combination without subAffId
				deleteCookie(sAffCookie, response);
			}
		}

		// Handle first combId and dp cookie
		if (!isHasFirstCombCookie && !combIdCookieFound) {
			if (null != combIdToSetCookie) {
				addCookie(Constants.FIRST_COMBINATION_ID, combIdToSetCookie, request, response, Constants.FIRST_COMBINATION_ID);
			}
			if (null != dpSession) {
				addCookie(Constants.FIRST_DYNAMIC_PARAM, dpSession, request, response, Constants.FIRST_DYNAMIC_PARAM);
			}
		}

		// handle trade doubler uniqeId from session
		String tradeDoubler = (String)session.getAttribute(Constants.TRADE_DOUBLER);
		if (!CommonUtil.isParameterEmptyOrNull(tradeDoubler)) {
			logger.log(Level.DEBUG, "Found tradeDoublerUID id in session, tradeDoublerUID:" + tradeDoubler + " , save to cookie.");
			addCookie(Constants.TRADE_DOUBLER, tradeDoubler, request, response, Constants.TRADE_DOUBLER);
		} else {
			//TODO: need to remove ???
		}

		//FROM NOW ON WE SAVE HTTP_REFERE IN 'PhaseListener'
		// handle http referer from landing
//		String httpReferer = (String)session.getAttribute(Constants.HTTP_REFERE_LANDING);
//		if (!CommonUtil.IsParameterEmptyOrNull(httpReferer)) {
//			logger.log(Level.DEBUG, "Found httpReferer in session, httpReferer:" + httpReferer + " , save to cookie.");
//			session.setAttribute(Constants.HTTP_REFERE, httpReferer);
//			addCookie(Constants.HTTP_REFERE_COOKIE, httpReferer, response, Constants.HTTP_REFERE);
//		}

		// handle http referer not from landing
		if (null == session.getAttribute(Constants.HTTP_REFERE) &&
				UsersManager.getHttpRefererCookie(request) == null) {
			String httpRefererH = request.getHeader(Constants.HTTP_REFERE);
			if (null != httpRefererH && !httpRefererH.startsWith(Utils.getPropertyByHost(request, "homepage.url"))) {
				try {
					httpRefererH = URLEncoder.encode(httpRefererH, "UTF-8");
					session.setAttribute(Constants.HTTP_REFERE, httpRefererH);
					addCookie(Constants.HTTP_REFERE_COOKIE, httpRefererH, request, response, Constants.HTTP_REFERE);
				} catch (Exception e) {
					logger.warn("Error! problem to encode http referer: " + httpRefererH);
				}
			}
		}

		// handle dfaPlacmentId
		String dfaPlacmentId = (String)session.getAttribute(Constants.DFA_PLACEMENT_ID_VALUE);
		if (!CommonUtil.isParameterEmptyOrNull(dfaPlacmentId)) {
			logger.log(Level.DEBUG, "Found dfaPlacmentId in session, dfaPlacmentId:" + dfaPlacmentId + " , save to cookie.");
			addCookie(Constants.DFA_PLACEMENT_ID_VALUE, dfaPlacmentId, request, response, Constants.DFA_PLACEMENT_ID);
		}

		// handle dfaCreativeId
		String dfaCreativeId = (String)session.getAttribute(Constants.DFA_CREATIVE_ID_VALUE);
		if (!CommonUtil.isParameterEmptyOrNull(dfaCreativeId)) {
			logger.log(Level.DEBUG, "Found dfaCreativeId in session, dfaCreativeId:" + dfaCreativeId + " , save to cookie.");
			addCookie(Constants.DFA_CREATIVE_ID_VALUE, dfaCreativeId, request, response, Constants.DFA_CREATIVE_ID);
		}

		// handle dfaMacro
		String dfaMacro = (String)session.getAttribute(Constants.DFA_MACRO_VALUE);
		if (!CommonUtil.isParameterEmptyOrNull(dfaCreativeId)) {
			logger.log(Level.DEBUG, "Found dfaMacro in session, dfaMacro:" + dfaMacro + " , save to cookie.");
			addCookie(Constants.DFA_MACRO_VALUE, dfaMacro, request, response, Constants.DFA_MACRO);
		}

		session.setAttribute(Constants.IS_HAS_VERSION2_SKIN,"false");

		/*if (null != redirectToLandingPage && redirectToLandingPage.length() > 0) {
			response.sendRedirect(redirectToLandingPage);
		}*/

		return null;
	}

	/**
	 * Save click with index navigation for RU
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public String saveAoClickIndexRU() throws IOException, SQLException {
		return getSaveAnyOptionClick(Constants.AO_HOME_PAGE_DEFAULT);
	}

	/**
	 * Save click with index navigation
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public String getSaveAoClickIndex() throws IOException, SQLException {
		return getSaveAnyOptionClick(Constants.AO_HOME_PAGE_DEFAULT);
	}

	/**
	 * Save click with indexBanner navigation
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public String getSaveAoClickIndexBanner() throws IOException, SQLException {
		return getSaveAnyOptionClick(Constants.AO_HOME_PAGE_BANNER);
	}

	/**
	 * Save click with ultimateBanner navigation
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public String getSaveAoClickLPUltimateBanner() throws IOException, SQLException {
		return getSaveAnyOptionClick(Constants.AO_HOME_PAGE_ULTIMATE_BANNER);
	}

	/**
	 * Save click with editorialBanner navigation
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public String getSaveAoClickLpEditorialBanner() throws IOException, SQLException {
		return getSaveAnyOptionClick(Constants.AO_HOME_PAGE_EDITORIAL_BANNER);
	}

	/**
	 * Save click with editorial_2 navigation
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public String getSaveAoClickLpEditorial_2() throws IOException, SQLException {
		return getSaveAnyOptionClick(Constants.AO_HOME_PAGE_EDITORIAL_2);
	}

	/**
	 * Save click with Advertorial RU navigation
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public String getSaveAoClickAdvertorialRU() throws IOException, SQLException {
		return getSaveAnyOptionClick(Constants.AO_HOME_PAGE_ADVERTORIAL_RU);
	}

	/**
	 * Save click with landing
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public String getSaveAoClickLpFullRegisterPL() throws IOException, SQLException {
		return getSaveAnyOptionClick(Constants.AO_HOME_PAGE_FULL_REGISTER_PL);
	}

	/**
	 * Save click for landing 70 generic short v1
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public String getSaveAoClickContactus0311() throws IOException, SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		session.setAttribute(Constants.REGISTER_FROM_CONTACTUS_LANDING, "true");
		return getSaveAnyOptionClick(Constants.AO_HOME_PAGE_CONTACTUS_03_11);
	}

    /**
     *  save Click for anyOption
     * @return
     * @throws IOException
     * @throws SQLException
     */
	public String getSaveAnyOptionClick(String homePage) throws IOException, SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();

		//Prevent from AdsBot-Google to be insert as click
		if (null != request.getHeader("User-Agent") && request.getHeader("User-Agent").toLowerCase().indexOf("adsbot-google") > -1 ) {
			   return null;
		}

		/* Mobile use
		 * Redirect to mobile domain for saving session attributes.
		 * If we are in www.anyoption.com, saving session attributes and then redirecting to m.anyoption.com -> it's a new session
		 * and all the attributes are gone... so i'm redirecting to the same page just with the mobile domain.
		 */
		String host = new URL(request.getRequestURL().toString()).getHost();
		if (!(host.startsWith(Constants.SUBDOMAIN_MOBILE_ANYOPTION + "."))&&
				CommonUtil.isMobileUserAgentBehavior() && (request.getRequestURI() != null && !request.getRequestURI().contains("landingInternal.jsf"))) {
			String mobileHost = new URL(gethomePageMobileUrl()).getHost();
			String redirectToMobile = request.getRequestURL().toString().replace(host, mobileHost);
			if (null != request.getQueryString() && request.getQueryString().length() > 1) {
				redirectToMobile += "?" + request.getQueryString();
			}
			response.sendRedirect(redirectToMobile);
			return null;
		}

		// default url
		String urlPage = homePage;
		String path = "" ;
		
		String pageRTemp = request.getParameter(Constants.LANDING_PAGE_REDIRECT);
		
		if (null != pageRTemp && pageRTemp.startsWith("LP_")) {
			path =  Utils.getPropertyByHost("homepage.url") + "jsp/dynamicLP/AO/";
		} else {
			path =  Utils.getPropertyByHost("homepage.url") + "jsp/";
		}
		// going to landingInternal.jsf is directing to correct domain by skin
    	String skinStr = request.getParameter(Constants.SKIN_ID);
    	if (CommonUtil.isParameterEmptyOrNull(skinStr)) {
    		String ip = CommonUtil.getIPAddress(request);
    		logger.debug("Get ip, ip: " + ip);
    		if (getIsLocalStatic()) {
    			ip = "74.125.67.100";   // en us
    			logger.debug("local system, change the ip hardcoded! to " + ip);
    		}
        	// country recognition
    		StringBuffer urlBuffer = request.getRequestURL();
    		String[] urlSplit = urlBuffer.toString().split("/");
    		String url = urlSplit[2];   // take domain
    		Connection con = null;
    		try {
    			con = getConnection();
    			int urlId  = UrlsDAO.getIdByUrl(con, url);
    			if (urlId == 0) {
    				urlId = ConstantsBase.ANYOPTION_URL;
    			}
    			long skinId = Skin.SKINS_DEFAULT;
    			logger.info("Url received: " + url + " UrlID:" + urlId);
    			String countryCode = CommonUtil.getCountryCodeByIp(ip);
    			if (!CommonUtil.isParameterEmptyOrNull(countryCode)) { // no need country recognition for skinEsMiop
    				logger.debug("Get Counrty code from Ip, Country code: " + countryCode);
    				// get Country id from Db
    				long countryId = CountryDAOBase.getIdByCode(con, countryCode);
    				if (countryId == 0) {  // country not in Db, get default skin.
    					logger.debug("Country not exists in the DB, Take default skin. ");
    				} else { // country found
    					long skinIdByCountryAndUrl = SkinUrlCountryMapDAOBase.getSkinIdByCountryAndUrl(con, countryId, urlId);
    					if (skinIdByCountryAndUrl != 0) {
    					    skinId = skinIdByCountryAndUrl;
    					}
    				}
    		    }
    			if (skinId != Skin.SKIN_REG_IT) {
    				skinId = SkinsManagerBase.getNonRegulatedSkinIdByRegulatedSkinId(skinId);
    			}
    			skinStr = String.valueOf(skinId);
    		} finally {
    			closeConnection(con);
    		}
    	}
    	
    	String newPrefix = Constants.EMPTY_STRING;
		Long s = Long.valueOf(skinStr);
		if (null != s) {
			long languageId = ApplicationData.getSkinById(s).getDefaultLanguageId();
			String lang = ApplicationData.getLanguage(languageId).getCode();
			if (s == Skin.SKIN_EN_US || s == Skin.SKIN_ES_US) {
				long countryId = ApplicationData.getSkinById(s).getDefaultCountryId();
				newPrefix = lang + "-" + ApplicationData.getCountry(countryId).getA2().toLowerCase();
		    } else {
		    	newPrefix = lang;
		    }
			String subDomain = getSubDomainBySkin(newPrefix, ApplicationData.getSkinById(s).isRegulated());
			if (null != pageRTemp && pageRTemp.startsWith("LP_")) {
				path =  Utils.getPropertyByHost("homepage.url").replace("www", subDomain)  + "jsp/dynamicLP/AO/";
			} else {
				path =  Utils.getPropertyByHost("homepage.url").replace("www", subDomain)  + "jsp/";
			}
		
		}

//		if (CommonUtil.isMobile()) {
//			path = gethomePageMobileUrl();   // get mobile url
//			path = path.replace(Constants.AO_HOME_PAGE_DEFAULT, "");
//		}

		// check if we got redirectPage in the request
		String pageR = request.getParameter(Constants.LANDING_PAGE_REDIRECT);
		if (null != pageR) {
			urlPage = pageR + ".jsf";
			if (pageR.equals("70_generic_short") ||
					pageR.equals("70_generic_short_counter") ||
					pageR.equals("70_generic_short_survey") ||
					pageR.equals("70_generic_short_testimonials") ||
					pageR.equals("70_generic_full") ||
					pageR.equals("70_generic_full_counter")) {
			    String combId = request.getParameter(Constants.COMBINATION_ID);
			    if (combId != null) {
			    	MarketingCombination comb = AdminManager.getCombinationId(Long.parseLong(combId));
			    	if(comb.getPaymentRecipientId() == Constants.MARKETING_PAYMENT_RECIPIENT_ID_GOOGLE &&
			    			comb.getSourceId() == Constants.MARKETING_SOURCE_ID_GOOGLE) {
			    		urlPage = pageR + "_google.jsf";
			    	}
			    }
			}
		}

		//TODO: create landing pages for mobile like in the webSite
//		if (CommonUtil.isMobile()) {
//			urlPage = Constants.AO_HOME_PAGE_DEFAULT;
//		}


		// all the paramters from the incomeing url
		String quer = request.getQueryString();
		if (quer == null) {
			quer = "";
		}
		
		// create url by pretty (only if exist).
		String pretty = urlPage;
		String pathToRedirect = path;
		if (path.indexOf("dynamicLP") == -1) {
			pathToRedirect = path.substring(0, path.length() - 5); 
			String urlPageName = "/jsp/" + urlPage;
			if (!CommonUtil.isParameterEmptyOrNull((String)request.getSession().getAttribute("redirectToDeposit")) && 
					((String)request.getSession().getAttribute("redirectToDeposit")).equals(Constants.NAV_FIRST_DEPOSIT)) {
				urlPageName = "/jsp/pages/" + urlPage;
			}
			pretty = PageNames.getPageNameStatic(newPrefix, urlPageName);
			if (CommonUtil.isParameterEmptyOrNull(pretty)) {
				pretty = urlPageName;
			}
		} 
		
		// in case of assets page the path is different 
		if (pageRTemp != null && pageRTemp.startsWith("assets/")) {
			pretty = "/" + pageRTemp;			
		}
						
		session.setAttribute(Constants.LANDING_QUERY_STRING, quer);
		if (!CommonUtil.isParameterEmptyOrNull(quer)) {
			quer = "?" + quer;
		}

		String combId = (String) session.getAttribute(Constants.COMBINATION_ID);
		String dynamicParam = (String) session.getAttribute(Constants.DYNAMIC_PARAM);
		String subAffKey = (String) session.getAttribute(Constants.SUB_AFF_KEY_PARAM);
		String tradeDoubler = (String) session.getAttribute(Constants.TRADE_DOUBLER);
		String httpReferer = (String) session.getAttribute(Constants.HTTP_REFERE_LANDING);
		// Double click parameters
		String dfaPlacmentId = (String) session.getAttribute(Constants.DFA_PLACEMENT_ID_VALUE);
		String dfaCreativeId = (String) session.getAttribute(Constants.DFA_CREATIVE_ID_VALUE);
		String dfaMacro = (String) session.getAttribute(Constants.DFA_MACRO_VALUE);
		String remarketingParam = (String) session.getAttribute(Constants.FROM_REMARKETING);
		String affSub1 = (String) session.getAttribute(Constants.AFF_SUB1);
		String affSub2 = (String) session.getAttribute(Constants.AFF_SUB2);
		String affSub3 = (String) session.getAttribute(Constants.AFF_SUB3);
				
		// handle dynamic parameter
		if ( null == dynamicParam ) {  // not in session
			dynamicParam = (String) request.getParameter(Constants.DYNAMIC_PARAM);
			if ( null != dynamicParam ) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.DYNAMIC_PARAM, dynamicParam);
			}
		}

		// handle sub affiliate parameter
		if ( null == subAffKey ) {  // not in session
			subAffKey = (String)request.getParameter(Constants.SUB_AFF_KEY_PARAM);
			if ( null != subAffKey ) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.SUB_AFF_KEY_PARAM, subAffKey.toLowerCase());
			}
		}

		// handle trade doubler parameter
		if (null == tradeDoubler) {  // not in session
			tradeDoubler = (String)request.getParameter(Constants.TRADE_DOUBLER_PARAM);
			if (null != tradeDoubler) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.TRADE_DOUBLER, tradeDoubler);
			}
		}

		// handle http referer
		if (null == httpReferer) {
			httpReferer = request.getHeader(Constants.HTTP_REFERE);
			if (null != httpReferer) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				try {
					session.setAttribute(Constants.HTTP_REFERE_LANDING, URLEncoder.encode(httpReferer, "UTF-8"));
				} catch (Exception e) {
					logger.warn("Error! problem to encode http referer: " + httpReferer);
				}
			}
		}

		// DFA placement
		if (null == dfaPlacmentId) {
			dfaPlacmentId = request.getParameter(Constants.DFA_PLACEMENT_ID);
			if (null != dfaPlacmentId) {
				session.setAttribute(Constants.DFA_PLACEMENT_ID_VALUE, dfaPlacmentId);
			}
		}

		// DFA dfaCreative
		if (null == dfaCreativeId) {
			dfaCreativeId = request.getParameter(Constants.DFA_CREATIVE_ID);
			if (null != dfaCreativeId) {
				session.setAttribute(Constants.DFA_CREATIVE_ID_VALUE, dfaCreativeId);
			}
		}

		// DFA dfaMacro
		if (null == dfaMacro) {
			dfaMacro = request.getParameter(Constants.DFA_MACRO);
			if (null != dfaMacro) {
				session.setAttribute(Constants.DFA_MACRO_VALUE, dfaMacro);
			}
		}
		if (combId != null) {
			// combination id exist in session! dont do nothing...
			CommonUtil.sendPermanentRedirect(response, pathToRedirect + pretty + quer);
			return null;
		}

		String ip = CommonUtil.getIPAddress();

	    combId = request.getParameter(Constants.COMBINATION_ID);

		// check combination
		if (null == combId) {
			CommonUtil.sendPermanentRedirect(response, pathToRedirect + pretty + quer);
			return null;
		}

	    //handle remarketing data
		if (null == remarketingParam) {
			try {			
				if (request.getParameter(Constants.FROM_REMARKETING) != null && request.getParameter(Constants.FROM_REMARKETING).equalsIgnoreCase("true")) {
			    	session.setAttribute(Constants.FROM_REMARKETING, "true");
			    	session.setAttribute(Constants.REMARKETING_COMBINATION_ID, combId);	
					// encode dynamic parameter.
					String dynamicParamEncoded = URLEncoder.encode(dynamicParam, "UTF-8");
					session.setAttribute(Constants.REMARKETING_DYNAMIC_PARAM, dynamicParamEncoded);
				}
			} catch (Exception e) {
				logger.warn("Error! problem in saving remarketingParam in the session");
			}
		}
		
		// handle affSub1  parameter
		if ( null == affSub1 ) {  // not in session
			affSub1 = (String)request.getParameter(Constants.AFF_SUB1);
			if ( null != affSub1 ) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.AFF_SUB1, affSub1);
			}
		}
		
		// handle affSub2  parameter
		if ( null == affSub2 ) {  // not in session
			affSub2 = (String)request.getParameter(Constants.AFF_SUB2);
			if ( null != affSub2 ) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.AFF_SUB2, affSub2);
			}
		}
		
		// handle affSub3  parameter
		if ( null == affSub3 ) {  // not in session
			affSub3 = (String)request.getParameter(Constants.GCLID);
			if ( null != affSub3 ) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.AFF_SUB3, Constants.GCLID + "_" + affSub3);
			}
		}

		Long affKey = getAffIdFromDynamicParameter(dynamicParam);
		Long subAffKeyLong;
		try {
			subAffKeyLong = Long.valueOf(subAffKey);
		} catch (Exception e) {
			subAffKeyLong = null;
		}

		if ( null != combId ) {
			long combination = 0;
			try {
				combination = Long.valueOf(combId);
			} catch (Exception e) {
				CommonUtil.sendPermanentRedirect(response, pathToRedirect + pretty + quer);
				return null;
			}
			MarketingCombination comb = AdminManager.getCombinationId(combination);
			if (null == comb) {
				logger.debug("Combination ID:" + combination + " does not exists! no record inserted.");
				CommonUtil.sendPermanentRedirect(response, pathToRedirect + pretty + quer);
				return null;
			} else {
				long clickId = 0;
				logger.debug("inserting click record. combid=" + combId + " ,ip=" + ip);
				if ((isNetreferCombination(Long.valueOf(combId)) || isReferpartnerCombination(Long.valueOf(combId))) && affKey > 0) {
					clickId = AdminManager.insertClick(combination, ip, dynamicParam, affKey, subAffKeyLong, httpReferer, dfaPlacmentId, dfaCreativeId, dfaMacro);
				} else {
					clickId = AdminManager.insertClick(combination, ip, dynamicParam, null, subAffKeyLong, httpReferer, dfaPlacmentId, dfaCreativeId, dfaMacro);
				}
				session.setAttribute(Constants.COMBINATION_ID, combId);
				session.setAttribute(Constants.CLICK_ID, String.valueOf(clickId));
			}
		}
		CommonUtil.sendPermanentRedirect(response, pathToRedirect + pretty + quer);
		return null;
	}

	/**
	 * Save cookie and session attributes for anyOption TLV
	 * @return
	 * @throws SQLException
	 * @throws IOException
	 */
	public String getSaveTLVCookie() throws SQLException, IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		return getSaveTLVCookie(session, request, response);
	}

	/**
	 * Save cookie and session attributes for anyOption TLV
	 * @return
	 * @throws SQLException
	 * @throws IOException
	 */
	public static String getSaveTLVCookie(HttpSession session, HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		boolean combIdCookieFound = false;			  // for combid cookie
		boolean recognitionCookiesFound = false;  // for all recognition cookies
		HashMap<String, Boolean> recognitionCookies = new HashMap<String, Boolean>();
		recognitionCookies.put(Constants.SKIN_ID, false);
		String skinIdCookieValue = "";
		String combIdCookieValue = "";
		Cookie dpCookie = null;
		boolean isHasFirstCombCookie = false;


		Cookie[] cs = request.getCookies();
		if (cs != null) {
			for (int i = 0; i < cs.length; i++) {
				if (cs[i].getName().equals(Constants.COMBINATION_ID)) {
					logger.log(Level.DEBUG, "found combid cookie! combid:" + cs[i].getValue());
					combIdCookieFound = true;
					combIdCookieValue = cs[i].getValue();
				} else if (cs[i].getName().equals(Constants.SKIN_ID) ) {
					recognitionCookies.put(Constants.SKIN_ID, true);
					skinIdCookieValue = cs[i].getValue();
				} else if (cs[i].getName().equals(Constants.DYNAMIC_PARAM)) {
					dpCookie = cs[i];
				} else if (cs[i].getName().equals(Constants.FIRST_COMBINATION_ID)) {
					isHasFirstCombCookie = true;
				}
			}
		}

		// recognition cookies check
		if (recognitionCookies.get(Constants.SKIN_ID)) {
			logger.trace("found skinId cookie! skinId:" + skinIdCookieValue);
			recognitionCookiesFound = true;
		}

		String combId = (String) session.getAttribute(Constants.COMBINATION_ID);
		String skin = (String) session.getAttribute(Constants.SKIN_ID);
		Locale locale = (Locale)session.getAttribute(Constants.USER_LOCALE);

		if (null != combId && null != skin && null != locale && recognitionCookiesFound) {
			return null;
		}

		if (!recognitionCookiesFound) {
			logger.debug("Saving recognition attributes for TLV Trade");
			// add cookies
			addCookie(Constants.SKIN_ID, String.valueOf(Skin.SKIN_TLV), response, Constants.SKIN_ID_DESCRIPTION);
			// add to session
			session.setAttribute(Constants.SKIN_ID, String.valueOf(Skin.SKIN_TLV));
			session.setAttribute(Constants.USER_LOCALE, new Locale(Constants.ETRADER_LOCALE));
			logger.debug("Saving recognition values in Session! , skinId: " + Skin.SKIN_TLV);
		} else { // found cookies but Locale not in session
				session.setAttribute(Constants.USER_LOCALE, new Locale(Constants.ETRADER_LOCALE));
				logger.debug("Saving user locale values in Session!, skinId: " + skinIdCookieValue);
	    }

		String combIdToSetSession = null;
		String combIdToSetCookie = null;
		boolean isNewComb = false;

		if (null == combId) {  // not is session
			if (combIdCookieFound) {
				combIdToSetSession = combIdCookieValue;
			} else {
				combIdToSetSession = String.valueOf(getSkinById(Skin.SKIN_TLV).getDefaultCombinationId());
				combIdToSetCookie = combIdToSetSession;
			}
		} else {  // in session. if we dont have combId cookie or we do have but it's different from the one on session and from default
			if (!combIdCookieFound || (combIdCookieFound && !combId.equals(combIdCookieValue) &&
										!combId.equals(String.valueOf(getSkinById(Skin.SKIN_TLV).getDefaultCombinationId()))) ) {
				combIdToSetCookie = combId;
				isNewComb = true;
			} else {  // don't override
				combIdToSetSession = combIdCookieValue;
			}
		}

		if (null != combIdToSetSession) {
			session.setAttribute(Constants.COMBINATION_ID, combIdToSetSession);
		}

		if (null != combIdToSetCookie) {
			addCookie(Constants.COMBINATION_ID, combIdToSetCookie, response, Constants.COMBINATION_ID);
		}

		//	handle dp from the session
		String dpSession = (String)session.getAttribute(Constants.DYNAMIC_PARAM);
		if (!CommonUtil.isParameterEmptyOrNull(dpSession)) {
			logger.log(Level.DEBUG, "Found dp in session, dp:" + dpSession + " , save to cookie.");
			addCookie(Constants.DYNAMIC_PARAM, dpSession, response, Constants.DYNAMIC_PARAM_DESCRIPTION);
		} else {
			if (isNewComb && null != dpCookie) {  // new combination without dp
				deleteCookie(dpCookie, response);
			}
		}

		// handle click from the session
		String clickId = (String)session.getAttribute(Constants.CLICK_ID);
		if (!CommonUtil.isParameterEmptyOrNull(clickId)) {
			logger.log(Level.DEBUG, "Found click id in session, clickId:" + clickId + " , save to cookie.");
			addCookie(Constants.CLICK_ID, clickId, response, Constants.CLICK_ID);
		}

		// Handle first combId and dp cookie
		if (!isHasFirstCombCookie && !combIdCookieFound) {
			if (null != combIdToSetCookie) {
				addCookie(Constants.FIRST_COMBINATION_ID, combIdToSetCookie, response, Constants.FIRST_COMBINATION_ID);
			}
			if (null != dpSession) {
				addCookie(Constants.FIRST_DYNAMIC_PARAM, dpSession, response, Constants.FIRST_DYNAMIC_PARAM);
			}
		}

		return null;
	}

    /**
     *  save Click for anyoption TLV
     * @return
     * @throws IOException
     * @throws SQLException
     */
	public String getSaveTLVClick() throws IOException, SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();

		//Prevent from AdsBot-Google to be insert as click
		if (request.getHeader("User-Agent").toLowerCase().indexOf("adsbot-google") > -1 ) {
			   return null;
		}

		// default url
		String urlPage = Constants.AO_HOME_PAGE_DEFAULT;
		String path = getHomePageUrl() + "jsp/";

		// check if we got redirectPage in the request
		String pageR = request.getParameter(Constants.LANDING_PAGE_REDIRECT);
		if (null != pageR) {
			urlPage = pageR + ".jsf";
		}

		// all the paramters from the incomeing url
		String quer = request.getQueryString();
		if (quer == null) {
			quer = "";
		}

		quer = parseQueryString(quer);

		String combId = (String) session.getAttribute(Constants.COMBINATION_ID);
		String dynamicParam = (String) session.getAttribute(Constants.DYNAMIC_PARAM);

		// handle dynamic parameter
		if (null == dynamicParam) {  // not in session
			dynamicParam = (String) request.getParameter(Constants.DYNAMIC_PARAM);
			if ( null != dynamicParam ) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.DYNAMIC_PARAM, dynamicParam);
			}
		}

		if (combId != null) {
			// combination id exist in session! dont do nothing...
			CommonUtil.sendPermanentRedirect(response, path + urlPage + quer);
			return null;
		}

		String ip = CommonUtil.getIPAddress();

	    combId = request.getParameter(Constants.COMBINATION_ID);

		// check combination
		if (null == combId) {
			CommonUtil.sendPermanentRedirect(response, path + urlPage + quer);
			return null;
		}

		if (null != combId) {
			long combination = 0;
			try {
				combination = Long.valueOf(combId);
			} catch (Exception e) {
				CommonUtil.sendPermanentRedirect(response, path + urlPage + quer);
				return null;
			}
			MarketingCombination comb = AdminManager.getCombinationId(combination);
			if (null == comb) {
				logger.debug("Combination ID:" + combination + " does not exists! no record inserted.");
				CommonUtil.sendPermanentRedirect(response, path + urlPage + quer);
				return null;
			} else {
				long clickId = 0;
				logger.debug("inserting click record. combid=" + combId + " ,ip=" + ip);
				clickId = AdminManager.insertClick(combination, ip, dynamicParam, null, null, null, null, null, null);
				session.setAttribute(Constants.COMBINATION_ID, combId);
				session.setAttribute(Constants.CLICK_ID, String.valueOf(clickId));
			}
		}
		CommonUtil.sendPermanentRedirect(response, path + urlPage + quer);
		return null;
	}

    /**
     * Create cookie and add to response
     * @param name
     *      cookie name
     * @param value
     *      cookie value
     * @param description
     *      description for the log
     * @param response
     */
    public static void addCookie(String name, String value, HttpServletRequest request, HttpServletResponse response, String description) {
        Cookie cookie = new Cookie(name, value);
        cookie.setPath("/");
        cookie.setMaxAge(365 * 24 * 60 * 60 * 1000); // about year
        //sub domain
        String property = Utils.getPropertyByHost(request, ConstantsBase.COOKIE_DOMAIN, null);
        if (null != property) {
            cookie.setDomain(property);
        }
        response.addCookie(cookie);
        logger.log(Level.DEBUG, "saving " + description + " in cookie name: " + name + " value: " + value);
    }

	/**
	 * Create cookie and add to response
	 * @param name
	 * 		cookie name
	 * @param value
	 * 		cookie value
	 * @param description
	 * 		description for the log
	 * @param response
	 */
	public static void addCookie(String name, String value, HttpServletResponse response, String description) {
	    addCookie(name, value, (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest(), response, description);
	}

	/**
	 * Delete exists cookie, it's deleted when we add to the response the same
	 * cookie with no maxAge.
	 * @param name cookie name
	 * @param response
	 */
	public static void deleteCookie(Cookie cookie, HttpServletResponse response) {
		cookie.setPath("/");
		cookie.setMaxAge(0);
		response.addCookie(cookie);
	}

	/**
	 * Return skin from the SkinsList by id
     *
	 * @param skinId skin id
	 * @return Skins instance
	 */
	public Skins getSkin() {
		return getSkinById(getSkinId());
	}

	/**
	 * Return skin from the SkinsList by id
     *
	 * @param skinId skin id
	 * @return Skins instance
	 */
	public static Skins getSkinById(long skinId) {
		Skins skin = null;
		for (Skins s : skinsList) {
			if (s.getId() == skinId) {
				skin = s;
				break;
			}
		}
		return skin;
	}

	/**
	 * Get the Locale from the session
	 * If Locale not found, create default Locale and insert to session
	 * @return
	 * 		Local object
	 * @throws SQLException
	 */
	public Locale getUserLocale() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		return getUserLocale(session, request, response);
	}

	/**
	 * Get the Locale from the session
	 * If Locale not found, create default Locale and insert to session
	 * @return
	 * 		Local object
	 * @throws SQLException
	 */
	
    private static void funnelLocale(HttpSession session, HttpServletRequest request, HttpServletResponse response, Locale locale) {
        Locale newLocale = null;
        try {
            if (request.getParameter("locale") != null) {
                if (request.getParameter("locale").contains(Constants.NORWEGIAN_LOCALE_LANGUAGE)) {
                    newLocale = new Locale(Constants.NORWEGIAN_LOCALE_LANGUAGE,Constants.NORWEGIAN_LOCALE_COUNTRY);
                    session.setAttribute(Constants.USER_LOCALE, newLocale);
                }
            } else if (locale.getLanguage().contains(Constants.NORWEGIAN_LOCALE_LANGUAGE)) {
                long SkinId = getSkinId(session, request);
                createLocaleBySkinId(session, request, response, SkinId, true, null, null, null);
            }
        } catch (Exception e) {
            logger.error("Can't load funnel locale", e);
        }
    }
	
	public static Locale getUserLocale(HttpSession session, HttpServletRequest request, HttpServletResponse response) throws SQLException {
		
		Locale locale = null;
		String strCountryId = request.getParameter("cid");
		if (strCountryId == null || strCountryId.trim().length() == 0) {
			locale = (Locale) session.getAttribute(Constants.USER_LOCALE);
			if (null != locale) {
			    funnelLocale(session, request, response, locale);
			    locale = (Locale) session.getAttribute(Constants.USER_LOCALE);
				return locale;
			}
	
			logger.info("Locale not in session, trying to recognize per skin");
			try { 
				if(!(Long.parseLong(getDefaultSkin(request)) == Skin.SKIN_ETRADER) 
						&& (request.getRequestURI() != null && !request.getRequestURI().contains("/jsp/widget/"))) { // we don't want to save cookie when coming from the widget.
					if (Long.parseLong(getDefaultSkin(request)) == Skin.SKIN_TLV) {
						getSaveTLVCookie(session, request, response);
					} else {
						getSaveAnyOptionCookie(session, request, response);   // recognize client in case it's not done!
					}
				}
				locale = (Locale) session.getAttribute(Constants.USER_LOCALE);
				if ( null != locale ) {
				    funnelLocale(session, request, response, locale);
	                locale = (Locale) session.getAttribute(Constants.USER_LOCALE);
					return locale;
				}
			} catch (IOException e) {
				logger.warn("Error with save anyoption cookie " + e);
			}
		} else {
			logger.debug("Request cid parameter is  [" + strCountryId + "], bypassing user locale session attribute!");
		}

		// in a case locale is null
		Long skinId = getSkinId(session, request);
		if (null == skinId) {   // skinId not in the session & cookie, take default skinId
			skinId = Long.valueOf(getDefaultSkin(request));
			logger.debug("SkinId not in session and cookies, Take default skin id: " + skinId );
		}
		Skins skinObj = getSkinById(skinId);
		Connection con = null;
		try {
			con = getConnection();
			String counrtyCode = CountryDAOBase.getCodeById(con, skinObj.getDefaultCountryId());
			String languageCode = LanguagesDAOBase.getCodeById(con, skinObj.getDefaultLanguageId());
			if (skinObj.isRegulated()) {
			    locale = new Locale(languageCode, "EU");
			} else {
                locale = new Locale(languageCode, counrtyCode);
			}
			session.setAttribute(Constants.USER_LOCALE, locale);
			logger.debug("get and save Locale in Session! languageCode: " + languageCode +
					" countryCode: " +counrtyCode+ " skinId: " + skinId);
		} finally {
			closeConnection(con);
		}
		return locale;
	}

    /**
     * Get the locale prefix to be used in the links for not logged in pages. The prefix will be either 2 character
     * language code (small letters) or 2 character language code folowed by "-" and 2 character country code (small
     * letter) for certain skins (US skins for now).
     *
     * @return xx or xx-yy where xx is 2 small char language code and yy is 2 small char country code
     * @throws SQLException
     */
    public String getLinkLocalePrefix() throws SQLException {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        return getLinkLocalePrefix(session, request, response);
    }

    /**
     * Get the locale prefix to be used in the links for not logged in pages. The prefix will be either 2 character
     * language code (small letters) or 2 character language code folowed by "-" and 2 character country code (small
     * letter) for certain skins (US skins for now).
     *
     * @param session
     * @param request
     * @param response
     * @return xx or xx-yy where xx is 2 small char language code and yy is 2 small char country code
     * @throws SQLException
     */
    public static String getLinkLocalePrefix(HttpSession session, HttpServletRequest request, HttpServletResponse response) throws SQLException {
        Locale l = getUserLocale(session, request, response);
        long s = getSkinId(session, request);
        if (s == Skin.SKIN_EN_US || s == Skin.SKIN_ES_US) {
        	if (!CommonUtil.isParameterEmptyOrNull(l.getCountry())) {
        		return l.getLanguage() + "-" + l.getCountry().toLowerCase();
        	}
        }
        return l.getLanguage();
    }

	/**
	 * Get all languages by skinId
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getLanguagesBySkinId() throws SQLException {
		long userSkinId = getSkinId();
		Skins skin = getSkinById(userSkinId);
		ArrayList<SkinLanguage> skinLanguages = skin.getSkinLanguagesList();
		ArrayList<SelectItem> languages = new ArrayList<SelectItem>();
		Locale locale = getUserLocale();
		for (SkinLanguage sl : skinLanguages) {
			languages.add(new SelectItem(Long.valueOf(sl.getLanguageId()),
					CommonUtil.getMessage(sl.getDisplayName(), null, locale)));
		}
		return languages;
	}

    /**
     * Get all option+ markets by skinId
     * @return
     * @throws SQLException
     */
    public HashMap<String, String> getOptionPlusMarketsList() throws SQLException {
        Map<Long, String> optionPlusMarkets = MarketsManager.getOptionPlusMarket(getSkinId());
        HashMap<String, String> markets = new HashMap<String, String>();
        for (Map.Entry<Long, String> entry : optionPlusMarkets.entrySet()) {
            markets.put("0," + entry.getKey(), entry.getValue() );
        }
        return markets;
    }


	/**
	 * Get all currencies by skinId
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getCurrenciesBySkinId() throws SQLException {

		long userSkinId = getSkinId();
		Skins skin = getSkinById(userSkinId);
		ArrayList<SkinCurrency> skinCurrencies = skin.getSkinCurrenciesList();
		ArrayList<SelectItem> currencies = new ArrayList<SelectItem>();
		Locale locale = getUserLocale();
		for (SkinCurrency sc : skinCurrencies) {
			currencies.add(new SelectItem(Long.valueOf(sc.getCurrencyId()),
					CommonUtil.getMessage(sc.getDisplayName(), null, locale)));
		}
		return currencies;
	}

	/**
	 * Get all currencies by skinId
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getCurrenciesSignBySkinId() throws SQLException {

		long userSkinId = getSkinId();
		Skins skin = getSkinById(userSkinId);
		ArrayList<SkinCurrency> skinCurrencies = skin.getSkinCurrenciesList();
		ArrayList<SelectItem> currencies = new ArrayList<SelectItem>();

		for (SkinCurrency sc : skinCurrencies) {
			currencies.add(new SelectItem(Long.valueOf(sc.getCurrencyId()), CurrenciesManagerBase.getCurrency(sc.getCurrencyId()).getCode()));
		}
		return currencies;
	}
	
	/**
	 * Get countries list
	 * @return
	 * 		List of countries
	 * @throws SQLException
	 */
    public ArrayList<SelectItem> getCountriesList() throws SQLException {

		ArrayList<SelectItem> translatedList = new ArrayList<SelectItem>();
		Locale locale = getUserLocale();
		for ( SelectItem c : countriesList ) {
			Long value = (Long)c.getValue();
            CountryStatus countryStatus = countries.get(value).getCountryStatus();
			if (value != ConstantsBase.COUNTRY_ID_IL &&  countryStatus == CountryStatus.NOTBLOCKED) { // remove Israel from anyoption
				String lable = CommonUtil.getMessage(c.getLabel(), null, locale);
				translatedList.add(new SelectItem(value, lable));
			}
		}

        //need to sort the countries list
        Collections.sort(translatedList, new AlphaComparator());

        return translatedList;
    }

    /**
	 * Get regulated countries list
	 * @return
	 * 		List of regulated countries
	 * @throws SQLException
	 */
    public ArrayList<SelectItem> getCountriesRegulatedList() throws SQLException {

		ArrayList<SelectItem> translatedList = new ArrayList<SelectItem>();
		Locale locale = getUserLocale();
		for ( SelectItem c : countriesRegulatedList ) {
			Long value = (Long)c.getValue();
			CountryStatus countryStatus = countries.get(value).getCountryStatus();
			if (value != ConstantsBase.COUNTRY_ID_IL && countryStatus == CountryStatus.NOTBLOCKED) { // remove Israel from anyoption
				String lable = CommonUtil.getMessage(c.getLabel(), null, locale);
				translatedList.add(new SelectItem(value, lable));
			}
		}

        //need to sort the countries list
        Collections.sort(translatedList, new AlphaComparator());

        return translatedList;
    }
    
    /**
     * Get regulated countries list
     * @return
     *      List of regulated countries and user country
     * @throws SQLException
     */
    public ArrayList<SelectItem> getCountriesRegulatedList(long userCountryId) throws SQLException {

        ArrayList<SelectItem> translatedList = new ArrayList<SelectItem>();
        Locale locale = getUserLocale();
        boolean isAddedUserCountry = false;
        for ( SelectItem c : countriesRegulatedList ) {
            Long value = (Long)c.getValue();
            CountryStatus countryStatus = countries.get(value).getCountryStatus();
            if (value != ConstantsBase.COUNTRY_ID_IL && countryStatus == CountryStatus.NOTBLOCKED) { // remove Israel from anyoption
                String lable = CommonUtil.getMessage(c.getLabel(), null, locale);
                translatedList.add(new SelectItem(value, lable));
                if (value == userCountryId){
                    isAddedUserCountry = true;
                }
            }
        }
        
        if (!isAddedUserCountry){
            for ( SelectItem c : countriesList ) {
                Long value = (Long)c.getValue();
                CountryStatus countryStatus = countries.get(value).getCountryStatus();
                if (value != ConstantsBase.COUNTRY_ID_IL && countryStatus == CountryStatus.NOTBLOCKED) { // remove Israel from anyoption and other countries  and add missing userCountry
                    if (value == userCountryId){
                        String lable = CommonUtil.getMessage(c.getLabel(), null, locale);
                        translatedList.add(new SelectItem(value, lable));
                    }
                }
            } 
        }

        //need to sort the countries list
        Collections.sort(translatedList, new AlphaComparator());

        return translatedList;
    }
    
    /**
	 * Get not regulated countries list
	 * @return
	 * 		List of not regulated countries
	 * @throws SQLException
	 */
    public ArrayList<SelectItem> getCountriesNotRegulatedList() throws SQLException {

		ArrayList<SelectItem> translatedList = new ArrayList<SelectItem>();
		Locale locale = getUserLocale();
		for ( SelectItem c : countriesNotRegulatedList ) {
			Long value = (Long)c.getValue();
            CountryStatus countryStatus = countries.get(value).getCountryStatus();
			if (value != ConstantsBase.COUNTRY_ID_IL && countryStatus == CountryStatus.NOTBLOCKED) { // remove Israel from anyoption
				String lable = CommonUtil.getMessage(c.getLabel(), null, locale);
				translatedList.add(new SelectItem(value, lable));
			}
		}

        //need to sort the countries list
        Collections.sort(translatedList, new AlphaComparator());

        return translatedList;
    }
    
    /**
     * Get not regulated countries list
     * @return
     *      List of not regulated countries and user country
     * @throws SQLException
     */
    public ArrayList<SelectItem> getCountriesNotRegulatedList(long userCountryId) throws SQLException {

        ArrayList<SelectItem> translatedList = new ArrayList<SelectItem>();
        Locale locale = getUserLocale();
        boolean isAddedUserCountry = false;
        for ( SelectItem c : countriesNotRegulatedList ) {
            Long value = (Long)c.getValue();
            CountryStatus countryStatus = countries.get(value).getCountryStatus();
            if (value != ConstantsBase.COUNTRY_ID_IL && countryStatus == CountryStatus.NOTBLOCKED) { // remove Israel from anyoption 
                String lable = CommonUtil.getMessage(c.getLabel(), null, locale);
                translatedList.add(new SelectItem(value, lable));
                if (value == userCountryId){
                    isAddedUserCountry = true;
                }
            }
        }
        
        if (!isAddedUserCountry){
            for ( SelectItem c : countriesList ) {
                Long value = (Long)c.getValue();
                CountryStatus countryStatus = countries.get(value).getCountryStatus();
                if (value != ConstantsBase.COUNTRY_ID_IL && countryStatus == CountryStatus.NOTBLOCKED) { // remove Israel from anyoption and other countries  and add missing userCountry
                    if (value == userCountryId){
                        String lable = CommonUtil.getMessage(c.getLabel(), null, locale);
                        translatedList.add(new SelectItem(value, lable));
                    }
                }
            } 
        }

        //need to sort the countries list
        Collections.sort(translatedList, new AlphaComparator());

        return translatedList;
    }
    
    /**
     * Get countries list Regulation
     * @return
     *      List of countries
     * @throws SQLException
     */
    public ArrayList<SelectItem> getCountriesListRegulation() throws SQLException {

        ArrayList<SelectItem> translatedList = new ArrayList<SelectItem>();
        Locale locale = getUserLocale();
        for ( SelectItem c : countriesList ) {
            Long value = (Long)c.getValue();
            if (ArrayUtils.indexOf(ConstantsBase.COUNTRY_ID_REGULATED, (Long)c.getValue())>-1){ // remove Israel from anyoption
                String lable = CommonUtil.getMessage(c.getLabel(), null, locale);
                translatedList.add(new SelectItem(value, lable));
            }
        }

        //need to sort the countries list
        Collections.sort(translatedList, new AlphaComparator());

        return translatedList;
    }
    
    public 

    class AlphaComparator implements Comparator<SelectItem> {
        // Comparator interface requires defining compare method.
        public int compare(SelectItem a, SelectItem b) {
            //    Sort countries in alphabetical order considering user's Locale
            //
            String aLabel = a.getLabel();
            String bLabel = b.getLabel();
            int returnVal = 0;

            try {
                Collator collator = Collator.getInstance(getUserLocale());
                returnVal = collator.compare(aLabel, bLabel);
            } catch (SQLException e) {
                logger.fatal("Error in sorting countriesList :" + e.getMessage());
            }

            return returnVal;
        }
    }

    public String getSkinIdTxt() {
		String x = getSkinId().toString();
		return x;

	}

	/**
	 * Get skin id, first search in the session and after that in the cookies.
	 * @return skinId
	 */
	public Long getSkinId() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        Long skinId = (Long) session.getAttribute(Constants.SESSION_SKIN);
        if(skinId == null){
        	skinId = getSkinId(session, request);
        }
		return skinId;
	}
	
	public ArrayList<String> getTournamentBannersById() throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return TournamentDAOBase.getTournamentsBannersBySkinId(con, getSkinId());
		} finally {
			closeConnection(con);
		}
	}
	
	
	/**
	 * Get skin id, first search in the session and after that in the cookies.
	 * @return skinId
	 */
	public static Long getSkinId(HttpSession session, HttpServletRequest request) {
		Long skinId = null;
		String strSkinId = (String)session.getAttribute(Constants.SKIN_ID);
		if (null != strSkinId) {
			skinId = new Long(strSkinId);
		} else {
			// getting skin ID from cookie
			Cookie[] cs = request.getCookies();
			Cookie cookie = null;
			if (cs != null) {
				for (int i = 0; i < cs.length; i++) {
					if (cs[i].getName().equals(Constants.SKIN_ID)) {
						logger.log(Level.TRACE, "found skinId cookie!");
						cookie = cs[i];
						break;
					}
				}
			}
			if (null != cookie && cookie.getValue() != null
					&& !cookie.getValue().equals("")
					&& cookie.getValue().indexOf("null") == -1) {
				try {
					skinId = Long.valueOf(cookie.getValue());
				} catch (Exception e) {
					if (logger.isEnabledFor(Level.WARN)) {
						logger.warn("SkinId cookie with strange value!", e);
					}
				}
			}
		}

		// return default skinId in a case skinId not in session and cookie
		if (skinId == null) {
			skinId = Long.valueOf(getDefaultSkin(request));
			logger.debug("SkinId not in session and cookies, Take default skin id: " + skinId );
		}

		String strCountryId = request.getParameter("cid");
		if (strCountryId != null && strCountryId.trim().length() > 0) {
			logger.debug("Found country ID [" + strCountryId
						+ "] as request parameter, trying to change current skin id [" + skinId
						+ "] to a regulated one");
			try {
				long counrtyId = Long.parseLong(strCountryId);
				
				if (regulatedCountriesList.contains(counrtyId)) {
					skinId = SkinsManagerBase.getRegulatedSkinIdByNonRegulatedSkinId(skinId);
					logger.debug("Country [" + counrtyId + "] is regulated, skin ID reset to [" + skinId + "]");
				} else {
					skinId = SkinsManagerBase.getNonRegulatedSkinIdByRegulatedSkinId(skinId);
					logger.debug("Country [" + counrtyId + "] is not regulated, skin ID reset to [" + skinId + "]");
				}
			} catch (Exception e) {
				logger.error("Exception determining skin ID by countryID [" + strCountryId + "]", e);
			}
		}
		
		return skinId;
	}

	/**
	 * Check if the skin is of etrader
	 */
	public boolean isEtraderSkin() {
		return getSkinId() == Skin.SKIN_ETRADER;
	}



	/**
	 * Get real country id, first search in the session and after that in the cookies.
	 * @return
	 * 		countryId
	 */
	public String getRealCountryId() {

		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();

		HttpSession session = request.getSession();
		String countryId = (String)session.getAttribute(Constants.COUNTRY_ID);

		if ( null != countryId ) {
			return countryId;
		}

		Cookie[] cs = request.getCookies();
		Cookie cookie = null;
		if (cs != null) {
			for (int i = 0; i < cs.length; i++) {
				if (cs[i].getName().equals(Constants.COUNTRY_ID)) {
					//logger.log(Level.DEBUG, "found skinId cookie!");
					cookie = cs[i];
					break;
				}
			}
		}
		if (null != cookie && cookie.getValue() != null
				&& !cookie.getValue().equals("")
				&& cookie.getValue().indexOf("null") == -1) {
			try {
				return cookie.getValue();
			} catch (Exception e) {
				if (logger.isEnabledFor(Level.WARN)) {
					logger.warn("countryId cookie with strange value!", e);
				}
			}
		}

		return countryId;
	}

	/**
	 * Get real currency id, first search in the session and after that in the cookies.
	 * @return
	 * 		currencyId
	 */
	public long getRealCurrencyId() {
	    long skinId = getSkinId();

		long defultCurrencyId = ConstantsBase.CURRENCY_USD_ID;
		if (skinId == Skin.SKIN_ETRADER || skinId == Skin.SKIN_TLV) {
		    defultCurrencyId = ConstantsBase.CURRENCY_ILS_ID;
		}

		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpSession session = request.getSession();
		String currencyId = (String) session.getAttribute(Constants.CURRENCY_ID);
		if (null != currencyId) {
			return Long.parseLong(currencyId);
		}

		Cookie[] cs = request.getCookies();
		Cookie cookie = null;
		if (cs != null) {
			for (int i = 0; i < cs.length; i++) {
				if (cs[i].getName().equals(Constants.CURRENCY_ID)) {
					//logger.log(Level.DEBUG, "found skinId cookie!");
					cookie = cs[i];
					break;
				}
			}
		}
		if (null != cookie && cookie.getValue() != null
				&& !cookie.getValue().equals("")
				&& cookie.getValue().indexOf("null") == -1) {
			try {
				return Long.parseLong(cookie.getValue());
			} catch (Exception e) {
				if (logger.isEnabledFor(Level.WARN)) {
					logger.warn("CURRENCY ID cookie with strange value!", e);
				}
			}
		}
		return defultCurrencyId;
	}

	/**
	 * Get real currency symbol;
	 * @return
	 * 		currency symbol
	 */
	public String getRealCurrencySymbol() {
		try {
			long currencyId = getRealCurrencyId();
			String currencySymbol = CommonUtil.getCurrencySymbol(currencyId);
			return CommonUtil.getMessage(currencySymbol, null);
		} catch (Exception e) {
			logger.debug("cant get real currency symbol return defult", e);
		}
		return CommonUtil.getMessage(ConstantsBase.CURRENCY_USD, null);
	}
	
	public String getUserOrRealCurrencySymbol() {
		try {
			long currencyId = getUserCurrencyId();
			String currencySymbol = CommonUtil.getCurrencySymbol(currencyId);
			return CommonUtil.getMessage(currencySymbol, null);
		} catch (Exception e) {
			logger.debug("cant get real currency symbol return defult", e);
		}
		return CommonUtil.getMessage(ConstantsBase.CURRENCY_USD, null);
	}
	
	private boolean checkUserObjHaveCurr(User user){
	    boolean res = false;
	    if(user != null && user.getId() > 0){
	        if(user.getCurrencyId() != null){
	            res = true;
	        } else {
	            res = false;
	        }
	    }
	    return res;
	}
	
	public long getUserCurrencyId() {		
		long currencyId;
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		User user = (User)request.getSession().getAttribute(Constants.BIND_SESSION_USER);
		if(checkUserObjHaveCurr(user)){		    
			currencyId = user.getCurrencyId(); 
		} else {
			currencyId = getRealCurrencyId();
		}
		return currencyId;
	}

	/**
	 * Get all markets names by user skin.
	 * @return
	 * 		String[][] - array that contains id,display_name for each market
	 * 		in this skin
	 * @throws SQLException
	 */
	public String getDisplayMarketsBySkinId() throws SQLException {

	//	int j = 0;
		long userSkinId = getSkinId();
		Map<Long, String> list = MarketsManager.getSkinsMarkets(userSkinId);
		StringBuffer  markets = new StringBuffer();
		markets.append("{");

		Set<Long> keys = list.keySet();
		Iterator<Long> iter = keys.iterator();
		while ( iter.hasNext() ) {
			Long marketId = (Long)iter.next();
			markets.append(marketId.toString()).append(":\"");
			markets.append(MarketsManagerBase.getMarketName(userSkinId, marketId)).append("\",");
		}
		int x = markets.lastIndexOf(",");
		markets.deleteCharAt(x);
		markets.append("}");

		return markets.toString();
	}
	
	/**
	 * Gets all active markets name
	 * @return 
	 * @throws SQLException
	 */
	public String getMarketsNamesAndId() throws SQLException {

		Hashtable<Long, Market> list = MarketsManagerBase.getAll();
		StringBuffer  markets = new StringBuffer();
		markets.append("{");

		Set<Long> keys = list.keySet();
		Iterator<Long> iter = keys.iterator();
		while ( iter.hasNext() ) {
			Long marketId = (Long)iter.next();
			markets.append(marketId.toString()).append(":\"");
			markets.append(list.get(marketId).getName()).append("\",");
		}
		int x = markets.lastIndexOf(",");
		markets.deleteCharAt(x);
		markets.append("}");

		return markets.toString();
	}

    /**
     * Get all markets short names by user skin.
     * @return
     *      String[][] - array that contains id,display_name for each market
     *      in this skin
     * @throws SQLException
     */
    public String getShortDisplayMarketsBySkinId() throws SQLException {

    //  int j = 0;
        long userSkinId = getSkinId();
        Map<Long, String> list = MarketsManager.getSkinsMarkets(userSkinId);
        StringBuffer  markets = new StringBuffer();
        markets.append("[");

        Set<Long> keys = list.keySet();
        Iterator<Long> iter = keys.iterator();
        while ( iter.hasNext() ) {
            Long marketId = (Long)iter.next();
            markets.append("[\"" + marketId.toString()).append("\" , \"");
            markets.append(MarketsManagerBase.getMarketShortName(userSkinId, marketId)).append("\"], ");
        }
        int x = markets.lastIndexOf(",");
        markets.deleteCharAt(x);
        markets.append("]");

        return markets.toString();
    }

    /*
     * return array for js with market without long term (no monthly and weekly)
     */
    public String getMarketsWithoutLongTermBySkinId() throws SQLException {

    //  int j = 0;
        long userSkinId = getSkinId();
        Map<Long, String> list = MarketsManager.getSkinsMarkets(userSkinId);

        StringBuffer marketsString = new StringBuffer();
        marketsString.append("[");

        Set<Long> keys = list.keySet();
        Iterator<Long> iter = keys.iterator();
        while ( iter.hasNext() ) {
            Long marketId = (Long)iter.next();
            Market tempMarket = markets.get(marketId);
            if (!tempMarket.isHasLongTermOpp()) {
                marketsString.append("'" + marketId.toString()).append("', ");
            }
        }
        int x = marketsString.lastIndexOf(",");
        marketsString.deleteCharAt(x);
        marketsString.append("]");

        return marketsString.toString();
    }

	/**
	 * Get skins list for drop down list
	 * @return
	 * 		list of all skins
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getSkins() throws SQLException {
		Locale locale = getUserLocale();
		ArrayList<SelectItem> skins = new ArrayList<SelectItem>();
		for ( Skins s : skinsList ) {
			skins.add(new SelectItem(String.valueOf(s.getId()), CommonUtil.getMessage(s.getDisplayName(), null, locale)));
		}
		return skins;
	}

	/**
	 * Change Locale and save into the session
	 * This is done by crate Locale instance by skin id, depend on country and language.
	 * @param skinId
	 * 		skin id that the user selected
	 * @param takeDefault
	 * 		boolean variable for taking defaults country & language or not
	 * @param countryId
	 * 		country id in case takeDefault = false
	 * @param languageId
	 * 		language id in case takeDefault = false
	 * @return
	 * 		Locale instance
	 * @throws SQLException
	 */
	public Locale createLocaleBySkinId(long skinId, boolean takeDefault,
			Long countryId, Long languageId, Long currencyId ) throws SQLException {

		FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		return createLocaleBySkinId(session, request, response, skinId, takeDefault, countryId, languageId, currencyId);
	}

	/**
	 * Change Locale and save into the session
	 * This is done by crate Locale instance by skin id, depend on country and language.
	 * @param skinId
	 * 		skin id that the user selected
	 * @param takeDefault
	 * 		boolean variable for taking defaults country & language or not
	 * @param countryId
	 * 		country id in case takeDefault = false
	 * @param languageId
	 * 		language id in case takeDefault = false
	 * @return
	 * 		Locale instance
	 * @throws SQLException
	 */
    public static Locale createLocaleBySkinId(HttpSession session, HttpServletRequest request, HttpServletResponse response, long skinId, boolean takeDefault,
            Long countryId, Long languageId, Long currencyId ) throws SQLException {

		Locale locale = null;
		Skins skinObj = getSkinById(skinId);
		Connection con = null;

		try {

			con = getConnection();
			String counrtyCode = null;
			String languageCode = null;
			if (takeDefault) {
				counrtyCode = CountryDAOBase.getCodeById(con, skinObj.getDefaultCountryId());
				languageCode = LanguagesDAOBase.getCodeById(con, skinObj.getDefaultLanguageId());
			}
			else {  // from registration, build locale by country&language selection
				counrtyCode = CountryDAOBase.getCodeById(con, countryId);
				languageCode = LanguagesDAOBase.getCodeById(con, languageId);
			}

			if (skinObj.isRegulated()) {
			    locale = new Locale(languageCode, "EU");
			} else {
			    locale = new Locale(languageCode, counrtyCode);
			}
			session.setAttribute(Constants.USER_LOCALE, locale);
			logger.debug("change locale and save in Session! languageCode: " + languageCode +
					" countryCode: " +counrtyCode);
			} finally {
				closeConnection(con);
			}

			// set session and cookies attributes
			String defaultLanguageId = null;
			String defaultCurrencyId= null;
			if ( takeDefault ) {
				defaultLanguageId = String.valueOf(skinObj.getDefaultLanguageId());
				
				CurrenciesRules currenciesRules = new CurrenciesRules();
				currenciesRules.setCountryId(skinObj.getDefaultCountryId());
				currenciesRules.setSkinId(skinObj.getId());
				defaultCurrencyId = String.valueOf(CurrenciesRulesManagerBase.getDefaultCurrencyId(currenciesRules));
			}
			else {
				defaultLanguageId = String.valueOf(languageId);
				defaultCurrencyId = String.valueOf(currencyId);
			}

			Cookie[] cs = request.getCookies();
			if (cs != null) {
				for (int i = 0; i < cs.length; i++) {
					if (cs[i].getName().equals(Constants.SKIN_ID) ) {
						addCookie(Constants.SKIN_ID, String.valueOf(skinId), request, response, Constants.SKIN_ID_DESCRIPTION);
					} else if (cs[i].getName().equals(Constants.CURRENCY_ID) ) {
						addCookie(Constants.CURRENCY_ID, defaultCurrencyId, request, response, Constants.CURRENCY_ID_DESCRIPTION);
					} else if (cs[i].getName().equals(Constants.LANGUAGE_ID) ) {
						addCookie(Constants.LANGUAGE_ID, defaultLanguageId, request, response, Constants.LANGUAGE_ID_DESCRIPTION);
					}
				}
			}

			session.setAttribute(Constants.SKIN_ID, String.valueOf(skinId));
			session.setAttribute(Constants.CURRENCY_ID, defaultCurrencyId);
			session.setAttribute(Constants.LANGUAGE_ID, defaultLanguageId);

			logger.debug("change skinId, defaultCurrencyId, defaultLanguageId and Set cookies and Session attributes");
			logger.debug("skinId value: " + skinId);

		return locale;
	}

	/**
	 * Set currency id after registration
	 * @param currencyId
	 * 		currency id that the user select
	 */
	public void setCurrencyAfterRegistration(long currencyId ) {

		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();

		Cookie[] cs = request.getCookies();
		if (cs != null) {
			for (int i = 0; i < cs.length; i++) {
				if (cs[i].getName().equals(Constants.CURRENCY_ID) ) {
					addCookie(Constants.CURRENCY_ID, String.valueOf(currencyId), response, Constants.CURRENCY_ID_DESCRIPTION);
				}
			}
		}

		session.setAttribute(Constants.CURRENCY_ID, String.valueOf(currencyId));

		logger.debug("change currencyId and Set cookie and Session attribute");

	}


	/**
	 * Set Session attributes after user logged in
	 * The attrubutes are: locale, skinId, languageId, currencyId
	 * @param skinId
	 * 		user skin id
	 * @param countryId
	 * 		user country id
	 * @param languageId
	 * 		user language id
	 * @param currencyId
	 * 		user currenct id
	 * @throws SQLException
	 */
	public void setAttributesAfterLogin(long skinId, long countryId, long languageId,
			long currencyId, long combId) throws SQLException {

		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
		Connection con = null;

		try {

			con = getConnection();
			// To load the local by user selection in the registration
			//String counrtyCode = CountryDAOBase.getCodeById(con, countryId);
			//String languageCode = LanguagesDAOBase.getCodeById(con, languageId);
			Skins skinObj = getSkinById(skinId);
			String counrtyCode = CountryDAOBase.getCodeById(con, skinObj.getDefaultCountryId());
			String languageCode = LanguagesDAOBase.getCodeById(con, skinObj.getDefaultLanguageId());
			Locale locale = null;
			if (skinObj.isRegulated()) {
			    locale = new Locale(languageCode, "EU");
			} else {
                locale = new Locale(languageCode, counrtyCode);
			}
			session.setAttribute(Constants.USER_LOCALE, locale);
			logger.debug("Create locale instance after user logged-in and save in Session! languageCode: " + languageCode +
					" countryCode: " +counrtyCode);

			session.setAttribute(Constants.SKIN_ID, String.valueOf(skinId));
			session.setAttribute(Constants.CURRENCY_ID, String.valueOf(currencyId));
			session.setAttribute(Constants.LANGUAGE_ID, String.valueOf(skinObj.getDefaultLanguageId()));
			//session.setAttribute(Constants.LANGUAGE_ID, String.valueOf(languageId));
			session.setAttribute(Constants.COMBINATION_ID, String.valueOf(combId));
			session.setAttribute(Constants.IS_REGULATED, String.valueOf(skinObj.isRegulated()));
			
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			
			addCookie(Constants.SKIN_ID, String.valueOf(skinId), response, Constants.SKIN_ID_DESCRIPTION);
			addCookie(Constants.IS_REGULATED, String.valueOf(skinObj.isRegulated()), response, Constants.IS_REGULATED_DESCRIPTION);

			logger.debug("Load skinId, currencyId, languageId after user logged-in and save in Session, skinId: " + skinId);

		} finally {
			closeConnection(con);
		}

	}

    /**
     * Get UtcOffset with GMT+/-HH:MM format.
     * First take utcOffset from Session, if null return default.
     *
     * @return The passed UTC offset in format GMT+/-HH:MM.
     */
    public String getUtcOffset(String utcOffset) {
    	FacesContext fc = FacesContext.getCurrentInstance();
    	String utcOffsetSession = null;
    	if(null != fc) {
    		utcOffsetSession = (String) fc.getExternalContext().getSessionMap().get(Constants.UTC_OFFSET);
    	}

    	if ( null == utcOffsetSession || utcOffsetSession.length() == 0 ) {   // utcOffset attribute from session is empty
    		utcOffsetSession = CommonUtil.getDefualtUtcOffset();
            logger.trace("Using default GMT+2 offset");
    	}
    	return utcOffsetSession;
    }


    public String getUtcOffset() {
    	FacesContext fc = FacesContext.getCurrentInstance();
    	String utcOffsetSession = null;
    	if(null != fc) {
    		utcOffsetSession = (String) fc.getExternalContext().getSessionMap().get(Constants.UTC_OFFSET);
    	}

    	if ( null == utcOffsetSession || utcOffsetSession.length() == 0 ) {   // utcOffset attribute from session is empty
    		utcOffsetSession = CommonUtil.getDefualtUtcOffset();
            logger.trace("Using default GMT+2 offset");
    	}
    	return utcOffsetSession;
    }

    public long getTimeForETClock(){
    	return TimeZone.getTimeZone(CommonUtil.getUtcOffset()).getRawOffset()+(new Date().getTime());
    }

    /**
     * get ticker markets with user skinId.
     * from cach or from db if last time was more then half an hour
     *
     * @return
     */
    public ArrayList<TradingPageBoxBean> getTickerMarketsForUser() {
    	long skinId = getSkinId();
//    	Calendar now = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
//    	Calendar lastLoad = null;
//		if (lastLoadTimeTickerMarkets.containsKey(skinId)) {
//			lastLoad = lastLoadTimeTickerMarkets.get(skinId);
//		}
//        if ( !tickerMarkets.containsKey(skinId) || now.after(lastLoad) ) {
//            tickerMarkets.put(skinId, getTickerMarketsById(skinId));
//        }
//        ArrayList<TradingPageBoxBean> tickerMarketsForUser = tickerMarkets.get(skinId);
//        return tickerMarketsForUser;
    	TickerUpdater tickerUpdater = (TickerUpdater) FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().get(Constants.TICKER_UPDATER);
    	return tickerUpdater.getTickerMarkets(skinId);
    }

    /**
     * get ticker markets for skin by id
     * @param skinId
     * @return list of market for the skin
     */
//    protected  static ArrayList<TradingPageBoxBean> getTickerMarketsById(long skinId) {
//    	if (logger.isDebugEnabled()) {
//            logger.debug("Realod ticker markets skin id = " + skinId + " Refresh period: " + tickerMarketsRefreshPeriod);
//        }
//    	ArrayList<TradingPageBoxBean> tmp = null;
//    	try {
//    		tmp = TickerManager.getSkinTickerMarkets(skinId);
//    		Calendar now = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
//    		if (now.get(Calendar.MINUTE) >= 05 && now.get(Calendar.MINUTE) <= 35) {
//            	now.set(Calendar.MINUTE, 35);
//            } else {
//            	if (now.get(Calendar.MINUTE) > 35) {
//            		now.add(Calendar.HOUR_OF_DAY, 1);
//            	}
//            	now.set(Calendar.MINUTE, 05);
//            }
//    		logger.debug("next loading ticker time for skin " + skinId + " is: " + now.getTime().toString());
//            lastLoadTimeTickerMarkets.put(skinId, now);
//    	 } catch (SQLException sqle) {
//             logger.error("Can't load ticker markets for skin id = " + skinId , sqle);
//         }
//    	 return tmp;
//    }

    public String getRunStressSelectInsert() {
        Connection con = null;
        try {
            con = getConnection();
            long adId = GeneralDAO.runStressSelect(con);
            GeneralDAO.runStressInsert(con, adId);
            return "ok";
        } catch (Exception e) {
            logger.error("Stress error.", e);
        } finally {
            closeConnection(con);
        }
        return "";
    }

    /**
     * Get page direction
     * @return
     */
    public String getPageDirection() {
    	if (isRtlAlign()) {
    		return "rtl";
    	}
    	return "ltr";
    }

    public String getPageDirectionReverse() {
    	if (isRtlAlign()) {
    		return "ltr";
    	}
    	return "rtl";
    }

    /**
     * Get columns classes direction
     * @return
     */
    public String getColumnClassesDirection() {
    	if (isRtlAlign()) {
    		return "left_align,right_align";
    	}
    	return "right_align,left_align";
    }

    /**
     * Get columns classes direction register page
     * @return
     */
    public String getColumnClassesDirectionRegister() {
    	if (isRtlAlign()) {
    		return "left_align,center_align,right_align";
    	}
    	return "right_align,center_align,left_align";
    }


    public String getColumnClassesDirectionReverse() {
    	if (isRtlAlign()) {
    		return "right_align,left_align";
    	}
    	return "left_align,right_align";
    }

    /**
     * Get columns classes deposit direction
     * @return
     */
    public String getDepositColumnClassesDirection() {
    	if (isRtlAlign()) {
    		return "left_align_deposit,right_align_deposit";
    	}
    	return "right_align_deposit,left_align_deposit";
    }

    /**
     * Get page alignment
     * @return
     */
    public String getPageAlignment() {
    	if (isRtlAlign()) {
    		return "right";
    	}
    	return "left";
    }

    /**
     * Get page alignment reverse
     * @return
     */
    public String getPageAlignmentReverse() {
    	if (isRtlAlign()) {
    		return "left";
    	}
    	return "right";
    }

    /**
     * get is Has Open One Touch with user skinId.
     * from cach or from db if last time was more then half an hour
     *
     * @return
     */
    public Boolean getIsHasOpenOneTouchForUser() {
    	long skinId = getSkinId();
    	Calendar now = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
    	Calendar lastLoad = null;
		if (lastLoadTimeIsHasOneTouch.containsKey(skinId)) {
			lastLoad = lastLoadTimeIsHasOneTouch.get(skinId);
		}
        if ( !isHasOneTouch.containsKey(skinId) || now.after(lastLoad) ) {
        	isHasOneTouch.put(skinId, isHasOpenOneTouchBySkinId(skinId));
        }
        return isHasOneTouch.get(skinId);
    }

    /**
     * get is Has Open One Touch for skin by id
     * @param skinId
     * @return is Has Open One Touch for the skin
     */
	public static Boolean isHasOpenOneTouchBySkinId(long skinId) {
		if (logger.isDebugEnabled()) {
            logger.debug("Realod is Has Open One Touch for skin id = " + skinId + " Refresh period: " + isHasOneTouchRefreshPeriod);
        }
    	boolean tmp = false;
    	try {
    		tmp = InvestmentsManager.isHasOneTouchOpen(skinId);
    		Calendar now = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
    		if (now.get(Calendar.MINUTE) >= 05 && now.get(Calendar.MINUTE) <= 35) {
            	now.set(Calendar.MINUTE, 35);
            } else {
            	if (now.get(Calendar.MINUTE) > 35) {
            		now.add(Calendar.HOUR_OF_DAY, 1);
            	}
            	now.set(Calendar.MINUTE, 05);
            }
    		logger.debug("next loading time for is Has OneTouch open with skin id " + skinId + " is: " + now.getTime().toString());
    		lastLoadTimeIsHasOneTouch.put(skinId, now);
    	 } catch (SQLException sqle) {
             logger.error("Can't load is Has Open One Touch for skin id = " + skinId , sqle);
         }
    	 return tmp;
	}

	public String getDefaultCurrencySymbol()throws SQLException{
    	long skinId = getSkinId();
    	Connection con = getConnection();
    	Currency currency = CurrenciesDAOBase.getById(con, getSkinById(skinId).getDefaultCurrencyId());
    	return currency.getSymbol();
    }

    public Long getDefaultCurrencyId()throws SQLException{
    	long skinId = getSkinId();
    	Connection con = getConnection();
    	Currency currency = CurrenciesDAOBase.getById(con, getSkinById(skinId).getDefaultCurrencyId());
    	return currency.getId();
    }

	/**
	 * @return the directPaymentMapping
	 */
	public static ArrayList<DirectPaymentMapping> getDirectPaymentMapping() {
		if(directPaymentMapping == null) {
			Connection con = null;
			try {
				con = getConnection();	
				directPaymentMapping = TransactionsDAO.getDirectPaymentMapping(con);
			}catch(SQLException ex) {
				logger.error("Can't lazzy load directPaymentMapping", ex );
			} finally {
				closeConnection(con);
			}
		}
		return directPaymentMapping;
	}

	/**
	 * Get direct banking deposint payment types by country and currency
	 * @param coutnryId user country id for mapping
	 * @param currencyId user currency id for mapping
	 * @return
	 */
	public static ArrayList<SelectItem> getDirectPaymentByCountryACurrency(long coutnryId, long currencyId, long skinId) {
		ArrayList<SelectItem> paymentSI = new ArrayList<SelectItem>();
		ArrayList<Long> paymenList = new ArrayList<Long>();
		if (skinId == Skin.SKIN_GERMAN) {
			for (DirectPaymentMapping m : directPaymentMapping) {
				if (m.getCountryId() == coutnryId && m.getCurrencyId() == currencyId) {
					paymenList.add(new Long(m.getPaymentId()));
				}
			}
			for (Long p : paymenList) {
				paymentSI.add(directDepositPaymentTypes.get(p));
			}

			if (null != paymentSI && paymentSI.size() > 0) {
				paymentSI = CommonUtil.translateSI(paymentSI);
			}
		}
		return paymentSI;
	}

	/**
	 * Check if user have direct banking deposit options
	 * @param coutnryId user country id
	 * @param currencyId user currency id
	 * @return
	 */
	public static boolean isHaveDirectPaymentsOption(long coutnryId, long currencyId, long skinId) {
		if (skinId == Skin.SKIN_GERMAN ) {
			for (DirectPaymentMapping m : directPaymentMapping) {
				if (m.getCountryId() == coutnryId && m.getCurrencyId() == currencyId
						&& null != directDepositPaymentTypes.get(new Long(m.getPaymentId()))) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Check if user have direct banking deposit options
	 * @param paymentId paymant type id
	 * @param coutnryId user country id
	 * @param currencyId user currency id
	 * @return
	 */
	public static boolean isHaveDirectDepositOption(int paymentId, long coutnryId, long currencyId, long skinId) {
		if (skinId == Skin.SKIN_GERMAN ) {
			for (DirectPaymentMapping m : directPaymentMapping) {
				if (m.getPaymentId() == paymentId && m.getCountryId() == coutnryId && m.getCurrencyId() == currencyId
						&& null != directDepositPaymentTypes.get(new Long(m.getPaymentId()))) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @return the bonusStates
	 */
	public static ArrayList<SelectItem> getBonusStates() {
		return bonusStates;
	}

	/**
	 * @param bonusStates the bonusStates to set
	 */
	public static void setBonusStates(ArrayList<SelectItem> bonusStates) {
		ApplicationData.bonusStates = bonusStates;
	}

	public String getTemplatesDirectory(){
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
		String skinId = (String) session.getAttribute(Constants.SKIN_ID_FROM_LANDING);
		if(isTlv()){
			return "templates_tlv";
		} else if (isChineseSkin() || (null != skinId && skinId.equals(String.valueOf(Skin.SKIN_CHINESE))) || (null != skinId && skinId.equals(String.valueOf(Skin.SKIN_CHINESE_VIP)))) {
			return Constants.TEMPLATES_DIRECTORY_CHINESE;
		} else if (isArabicSkin() || (null != skinId && skinId.equals(String.valueOf(Skin.SKIN_ARABIC)))){
			return Constants.TEMPLATES_DIRECTORY_OLD_DESIGN;
		}
		return Constants.TEMPLATES_DIRECTORY;
	}

	public boolean getIsHasVersion2Skin() {
		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		String isHaveVer2 = (String)req.getSession().getAttribute(Constants.IS_HAS_VERSION2_SKIN);
		if(null != isHaveVer2 && isHaveVer2.equals("true") ) {
			return true;
		}
		else {
			return false;
		}
	}

    public String getInsuranceStartTime() {
        Calendar now = Calendar.getInstance();
        int crrMin = now.get(Calendar.MINUTE);
        int crrSec = now.get(Calendar.SECOND);
        if (((crrMin >= endOfHourStart && crrMin < endOfHourEnd ) || (crrMin >= midHourStart && crrMin < midHourEnd)) && !((crrMin == endOfHourStart || crrMin == midHourStart) && crrSec >= 5)) {
            return "now";
        } else if ((crrMin == endOfHourStart || crrMin == midHourStart) && crrSec < 5) { //for delay of 5 sec
            return String.valueOf((5 - crrSec) * 1000);
        } else if (crrMin > endOfHourEnd) {
            return String.valueOf(((90 - insuranceMinStart - crrMin) * 60 * 1000) - ((60 - crrSec) * 1000));
        } else if (crrMin > midHourEnd) {
            return String.valueOf((endOfHourStart - crrMin) * 60 * 1000 - ((60 - crrSec) * 1000));
        } else if (crrMin < midHourStart) {
            return String.valueOf((midHourStart - crrMin) * 60 * 1000 - ((60 - crrSec) * 1000));
        }
        return "error";
    }

    public static String getInsuranceInvesetmentsByUser() {
        String result = null;
        try {
        	FacesContext context = FacesContext.getCurrentInstance();
        	User u = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        	if (null != u && u.getId() != 0) {
	            result = InvestmentsManager.getInsuranceInvesetmentsByUser(u.getId(), Long.valueOf(CommonUtil.getEnum("insurance_period_start_time", "insurance_period_start_time")), Long.valueOf(CommonUtil.getEnum("insurance_period_end_time", "insurance_period_end_time")));
	            if (null != result) {
	                logger.debug("result " + result);
	                String[] resultArr = result.split("_");
	                logger.debug("resultArr " + resultArr[0] + " resultArr 1 " + resultArr[1] + " resultArr length " + resultArr.length);
	                result = getInsuranceClockTimeByInvId(Long.parseLong(resultArr[0])) + "_" + resultArr[1];
	            }
        	} else {
        		logger.warn("InsuranceFunc: user instance is null!!!");
        	}
        } catch (Exception e) {
            logger.debug("cant get user insurance investments", e);
        }
        return result;
    }

    public static String getInsuranceClockTimeByInvId(long id) {
        long timeLeft = 0;
        long minLeft = 0;
        long secLeft = 0;
        Date oppExp = new Date();
        Date nextOppExp = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        try {
            logger.info("getting exp time for opp of investment id = " + id);
            oppExp = InvestmentsManager.getRunningOpportunityExpTimeByInvId(id);
            Investment investment = InvestmentsManagerBase.getInvestmentToSettle(id, false, false, null);
            Opportunity nextOpp = InvestmentsManager.getNextHourlyOppToOpenByOppId(investment.getOpportunityId());
            if (nextOpp == null) {    // null means that it is golden minutes
                nextOppExp = new Date();
            } else {
                nextOppExp = nextOpp.getTimeEstClosing();
            }
            long insurancePeriodEndTime = Long.valueOf(CommonUtil.getEnum("insurance_period_end_time", "insurance_period_end_time")) * 60000;
            if (null != oppExp) {
                timeLeft = oppExp.getTime() - insurancePeriodEndTime - System.currentTimeMillis();
                minLeft = Math.round(timeLeft / 60000);
                secLeft = Math.round((timeLeft % 60000) / 1000);
                if (minLeft < 0 || secLeft < 0) {
                    logger.error("Stelled data in Lightstreamer!!! Requested time left for invId: " + id +
                        " minLeft: " + minLeft +
                        " secLeft: " + secLeft +
                        " est close time: " + dateFormat.format(oppExp));
                }
                if (minLeft < 0) {
                    minLeft = 0;
                }
                if (secLeft < 0) {
                    secLeft = 0;
                }
            } else {
                logger.warn("Time command for investment that is most likely past the last time to buy!!!");
            }
        } catch (Exception e) {
            logger.warn("cant find opp expration time for investment id " + id, e);
        }
        return id + "_" + minLeft + "_" + secLeft + "_" + dateFormat.format(oppExp) + "_" + dateFormat.format(nextOppExp);
    }

    private void addTimeFirstVisitCookie(HttpServletResponse response){
    	DateFormat SimpleDateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yy");
		String timeFirstVisitString = SimpleDateFormat.format(new Date());

		addCookie(Constants.TIME_FIRST_VISIT_COOKIE, timeFirstVisitString, response, Constants.TIME_FIRST_VISIT_COOKIE);
    }

    /**
     * Check if calcalist game allready started
     * @return
     */
    public boolean isGameStarted() {
    	Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, 2009);
    	c.set(Calendar.DAY_OF_MONTH, 1);
    	c.set(Calendar.MONTH, Calendar.NOVEMBER);
    	c.set(Calendar.HOUR_OF_DAY, 8);
    	c.set(Calendar.MINUTE, 0);
    	c.set(Calendar.SECOND, 0);
    	c.set(Calendar.MILLISECOND, 0);
    	Date current = CommonUtil.getDateTimeFormaByTz(new Date(), CommonUtil.getUtcOffset());
    	if (current.after(c.getTime())) {
    		return true;
    	}
    	return false;
    }

    /**
     * Check if calcalist game ended
     * @return
     */
    public boolean isGameEnded() {
    	Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, 2009);
    	c.set(Calendar.DAY_OF_MONTH, 28);
    	c.set(Calendar.MONTH, Calendar.NOVEMBER);
    	c.set(Calendar.HOUR_OF_DAY, 0);
    	c.set(Calendar.MINUTE, 0);
    	c.set(Calendar.SECOND, 0);
    	c.set(Calendar.MILLISECOND, 0);
    	Date current = CommonUtil.getDateTimeFormaByTz(new Date(), CommonUtil.getUtcOffset());
    	if (current.after(c.getTime())) {
    		return true;
    	}
    	return false;
    }

    /**
     * Get time for the game
     * @return
     */
    public String getTimeForGameStart() {
    	Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, 2009);
    	c.set(Calendar.DAY_OF_MONTH, 1);
    	c.set(Calendar.MONTH, Calendar.NOVEMBER);
    	c.set(Calendar.HOUR_OF_DAY, 8);
    	c.set(Calendar.MINUTE, 0);
    	c.set(Calendar.SECOND, 0);
    	c.set(Calendar.MILLISECOND, 0);

    	Date current = CommonUtil.getDateTimeFormaByTz(new Date(), CommonUtil.getUtcOffset());
    	long timeMi = c.getTimeInMillis() - current.getTime();
    	long sec = (timeMi / 1000) % 60;
    	long min = (timeMi / (1000 * 60)) % 60 ;
    	long hours = (timeMi / (1000 * 60 * 60)) % 24;
    	long days = (timeMi / (1000 * 60 * 60)) / 24;

    	return days + "|" + hours + "|" + min + "|" + sec;
    }

    public boolean isShowGraphBanner() {
    	FacesContext context=FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		if (null != user && user.getId() > 0) {
			Calendar startShowDate = Calendar.getInstance();
            startShowDate.set(Calendar.YEAR, 2009);
			startShowDate.set(Calendar.DAY_OF_MONTH, 19);
			startShowDate.set(Calendar.MONTH, Calendar.OCTOBER);
			startShowDate.set(Calendar.HOUR_OF_DAY, 0);
			startShowDate.set(Calendar.MINUTE, 0);
			startShowDate.set(Calendar.SECOND, 0);
			startShowDate.set(Calendar.MILLISECOND, 0);

			if (null == user.getTimeLastLoginFromDb()) {
				return false;
			}

			Calendar userLastLoginDate = Calendar.getInstance();
			userLastLoginDate.setTime(user.getTimeLastLoginFromDb());

			if (userLastLoginDate.before(startShowDate)) {
				return true;
			}
		}
		return false;
	}

    /**
     * Return if show bonus banner
     * @return
     * @throws SQLException
     * @throws ParseException
     */
    public boolean isShowBonusBanner() throws SQLException, ParseException {
    	FacesContext context=FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		if (null != user && user.getId() > 0) {
			Long bonusId = Long.valueOf(CommonUtil.getEnum(Constants.ENUM_BONUS_BANNER_ENUMERATOR,Constants.ENUM_BONUS_ID_CODE));
			BonusUsers bu = BonusManagerBase.getBonusUserByBonusId(user.getId(), bonusId);
			if (bu == null){ //bonus not found
				return false;
			}
			if (null == user.getTimeLastLoginFromDb()) {
				return false;
			}
			if (user.getTimeLastLoginFromDb().before(bu.getStartDate()) && bu.getBonusStateId() == ConstantsBase.BONUS_STATE_GRANTED) {
				return true;
			}
		}
		return false;
    }

    /**
     * Return if show tax popup banner
     * @return
     * @throws SQLException
     * @throws ParseException
     */
    public boolean isShowTaxPopUp() throws SQLException, ParseException {
    	FacesContext context=FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		if (null != user && user.getId() > 0) {
			String dateString = CommonUtil.getEnum(Constants.ENUM_TAX_POPUP_ENUMERATOR, Constants.ENUM_TAX_POPUP_CODE);
			if (CommonUtil.isParameterEmptyOrNull(dateString)){
				return false;
			}
			if (null == user.getTimeLastLoginFromDb()) {
				return false;
			}
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			Date startDate = null;
			Date currentDate = new Date();
			try {
				startDate = df.parse(dateString);
				GregorianCalendar gc = new GregorianCalendar();
				gc.setTime(startDate);
				gc.set(Calendar.HOUR, -2); //Israel time
				startDate = gc.getTime();
			} catch (ParseException e) {
				logger.error(e);
				return false;
			}
			// We want to activate the popup on 3 conditions:
			// 1. Current date is after 01/01/12 00:00 ISR time
			// 2. User time login is small than 01/01/12 00:00 ISR time - in order to display that only once
			// 3. All users that there time created is samller than 01/01/12 00:00 ISR time
			if (currentDate.after(startDate) && user.getTimeLastLoginFromDb().before(startDate) && user.getTimeCreated().before(startDate)) {
				return true;
			}
		}
		return false;
    }

    /**
     * Return if show tax popup banner
     * @return
     * @throws SQLException
     * @throws ParseException
     */
    public boolean isShowBirthday4PopUpLogin() throws SQLException, ParseException {
    	FacesContext context=FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		if (null != user && user.getId() > 0) {
			String dateString = CommonUtil.getEnum(Constants.ENUM_BIRTHDAY_POPUP_LOGIN_ENUMERATOR, Constants.ENUM_BIRTHDAY_POPUP_LOGIN_CODE);
			if (CommonUtil.isParameterEmptyOrNull(dateString)){
				return false;
			}
			if (null == user.getTimeLastLoginFromDb()) {
				return false;
			}
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date startDate = null;
			try {
				startDate = df.parse(dateString);
			} catch (ParseException e) {
				logger.error(e);
				return false;
			}
			// We want to activate the popup on 1 conditions:
			// User time login is small than dd/03/12 HH:mm ISR time - in order to display that only once
			if (user.getTimeLastLoginFromDb().before(startDate)) {
				return true;
			}
		}
		return false;
    }

    public boolean isShowPopUpLogin(String enumerator, String enumeratorCode) throws SQLException, ParseException {
    	FacesContext context=FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		if (null != user && user.getId() > 0) {
			String dateString = CommonUtil.getEnum(enumerator, enumeratorCode);
			if (CommonUtil.isParameterEmptyOrNull(dateString)){
				return false;
			}
			if (null == user.getTimeLastLoginFromDb()) {
				return false;
			}
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date startDate = null;
			try {
				startDate = df.parse(dateString);
			} catch (ParseException e) {
				logger.error(e);
				return false;
			}
			// We want to activate the popup on 1 conditions:
			// User time login is small than dd/mm/yy HH:mm - in order to display that only once
			if (user.getTimeLastLoginFromDb().before(startDate)) {
				return true;
			}
		}
		return false;
    }


    /**
     * Return if show New Features popup banner
     * @return
     * @throws SQLException
     * @throws ParseException
     */
    public boolean isShowNewFeaturesPopUpLogin() throws SQLException, ParseException {
    	return isShowPopUpLogin(Constants.ENUM_NEW_FEATURES_POPUP_LOGIN_ENUMERATOR, Constants.ENUM_NEW_FEATURES_POPUP_LOGIN_CODE);
    }

    /**
     * Get rss items from reuters news rss page
     * @return ArrayList of RssItem instances
     */
    public ArrayList<RssItem> getRssItemsTlv() {
    	refreshRssItems(isTlv(), false , 0);
    	return rssItems.get(Constants.TLV_RSS_URL);
    }

    public ArrayList<RssItem> getRssItemsZh1() {
    	refreshRssItems(false, isChineseSkin(), 1);
    	return rssItems.get(Constants.ZH1_RSS_URL);
    }

    public ArrayList<RssItem> getRssItemsZh2() {
    	refreshRssItems(false, isChineseSkin(), 2);
    	return rssItems.get(Constants.ZH2_RSS_URL);
    }

    public ArrayList<RssItem> getRssItemsZh3() {
    	refreshRssItems(false, isChineseSkin(), 3);
    	return rssItems.get(Constants.ZH3_RSS_URL);
    }

    public ArrayList<RssItem> getRssItemsZh4() {
    	refreshRssItems(false, isChineseSkin(), 4);
    	return rssItems.get(Constants.ZH4_RSS_URL);
    }

    public ArrayList<RssItem> getRssItems() {
    	refreshRssItems(false , false, 0);
    	return rssItems.get(Constants.RSS_URL);
    }

    private static void refreshRssItems(boolean isTlv , boolean isZh, int zhUrl) {
    	long loadTime;
    	String url;
    	if (isTlv) {
    		loadTime = rssLoadTime.get(Constants.TLV_RSS_URL);
    		url = rssFeedUrl.get(Constants.TLV_RSS_URL);

    	} else if (isZh){
    		loadTime = rssLoadTime.get("zh" + zhUrl + ".rss.feed.url");
    		url = rssFeedUrl.get("zh" + zhUrl + ".rss.feed.url");
    	}else{
    		loadTime = rssLoadTime.get(Constants.RSS_URL);
    		url = rssFeedUrl.get(Constants.RSS_URL);
    	}
		if (System.currentTimeMillis() - loadTime > rssRefreshPeriod) {
            if (logger.isDebugEnabled()) {
                logger.debug("going to process Get request for rss reading. Refresh period: " + rssRefreshPeriod);
            }
            try {
            	ArrayList<RssItem> res =  RssReader.getRssItems(url);
            	long newLoadTime = System.currentTimeMillis();
            	if (isTlv) {
            		rssItems.put(Constants.TLV_RSS_URL, res);
            		rssLoadTime.put(Constants.TLV_RSS_URL, newLoadTime);
            	} else if (isZh){
            		rssItems.put("zh" + zhUrl + ".rss.feed.url", res);
            		rssLoadTime.put("zh" + zhUrl + ".rss.feed.url", newLoadTime);
            	} else {
            		rssItems.put(Constants.RSS_URL, res);
	                rssLoadTime.put(Constants.RSS_URL, newLoadTime);
            	}
            } catch (Exception e) {
            	logger.error("Problem processing Rss get request! " + e);
            }
        }
    }

	public String getLoginUrl() {
		if (isCalcalistGame()) {
			return CommonUtil.getProperty("login.url.cal");
		}
		try {
			String urlSkinPrefix = getPrefixBySkin();
			if (!CommonUtil.isParameterEmptyOrNull(urlSkinPrefix)) {
				return Utils.getPropertyByHost("login.url").replace("www", urlSkinPrefix);
			}
			return Utils.getPropertyByHost("login.url");
		} catch (SQLException e) {
			return Utils.getPropertyByHost("login.url");
		}

	}

	/**
	 * Parse the queryString and take just relevant params
	 * (google and skin params)
	 * @param queryString
	 * @return
	 */
	public String parseQueryString(String queryString) {
		StringBuffer qs = new StringBuffer();
		String[] params = queryString.split("&");
		for (int i = 0 ; i < params.length ; i++) {
			if (params[i].startsWith(Constants.GOOGLE_PARAM_PREFIX) ||
					params[i].startsWith(Constants.SKIN_PARAM_PREFIX) ||
					params[i].startsWith(Constants.GOOGLE_PARAM_ADWORDS_FOR_ANALYTICS)) {
				qs.append(params[i]).append("&");
			}
		}
		if (qs.length() > 0) {
			return "?" + qs.substring(0, qs.length()-1);
		} else {
			return "";
		}
	}

    /**
     * get is Has bainery opp open with user skinId.
     * from cach or from db if last time was more then half an hour
     *
     * @return
     */
    public Boolean getIsHasOpenMarket() {
        long skinId = getSkinId();
        Calendar now = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        Calendar lastLoad = null;
        if (lastLoadTimeIsHasOpenMarkets.containsKey(skinId)) {
            lastLoad = lastLoadTimeIsHasOpenMarkets.get(skinId);
        }
        if ( !isHasOpenMarkets.containsKey(skinId) || now.after(lastLoad) ) {
            isHasOpenMarkets.put(skinId, isHasOpenMarketBySkinId(skinId));
        }
        return isHasOpenMarkets.get(skinId);
    }

    /**
     * get is Has Open binary opp for skin by skin id
     * @param skinId
     * @return is Has Open One Touch for the skin
     */
    public static Boolean isHasOpenMarketBySkinId(long skinId) {
        if (logger.isDebugEnabled()) {
            logger.debug("Realod is Has Open markets for skin id = " + skinId + " Refresh period: 30");
        }
        boolean tmp = false;
        try {
            tmp = InvestmentsManager.isHasMarketOpen(skinId);
            Calendar now = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
            if (now.get(Calendar.MINUTE) >= 02 && now.get(Calendar.MINUTE) <= 32) {
                now.set(Calendar.MINUTE, 32);
            } else {
                if (now.get(Calendar.MINUTE) > 32) {
                    now.add(Calendar.HOUR_OF_DAY, 1);
                }
                now.set(Calendar.MINUTE, 02);
            }
            logger.debug("next loading time for is Has markets open with skin id " + skinId + " is: " + now.getTime().toString());
            lastLoadTimeIsHasOpenMarkets.put(skinId, now);
         } catch (SQLException sqle) {
             logger.error("Can't load is Has Open markets for skin id = " + skinId , sqle);
         }
         return tmp;
    }

    /**
     * Get conbinationId
     * @return
     * @throws SQLException
     */
    public String getCombinationId() throws SQLException {
    	HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    	Long combId = UsersManager.getCombinationId(req);
    	if (null != combId) {
    		return combId.toString();
    	} else {
    		return "";
    	}
    }

    /**
     * Get conbinationId from Session
     * @return
     * @throws SQLException
     */
    public String getSessionCombinationId() throws SQLException {
    	HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    	Long combId = UsersManager.getSessionCombinationId(req);
    	if (null != combId) {
    		return combId.toString();
    	} else {
    		return "";
    	}
    }

	/**
	 * @return the marketsClosingLevels
	 * @throws SQLException
	 */
	public static HashMap<Long, LandingMarketDetails> getMarketsClosingLevels() throws SQLException {
        if (null == marketsClosingLevels || System.currentTimeMillis() - landingMarketsLoadTime > landingMarketsRefreshPeriod) {
    		Connection con = null;
    		try {
    			con = getConnection();
    			marketsClosingLevels = InvestmentsManager.getClosingLevels(Market.MARKET_OIL_ID + "," + Market.MARKET_NASDAQ_ID);
                landingMarketsLoadTime = System.currentTimeMillis();
    		} finally {
    			closeConnection(con);
    		}
        }
		return marketsClosingLevels;
	}

	/**
	 * @param marketsClosingLevels the marketsClosingLevels to set
	 */
	public static void setMarketsClosingLevels(HashMap<Long, LandingMarketDetails> marketsClosingLevels) {
		ApplicationData.marketsClosingLevels = marketsClosingLevels;
	}

	public String getOilLevel()throws Exception{
		getMarketsClosingLevels();
		LandingMarketDetails md = marketsClosingLevels.get(new Long(Market.MARKET_OIL_ID));
		return CommonUtil.formatLevelByMarket(md.getClosingLevel().doubleValue(), md.getMarketId());
	}

	public boolean isSmsAvaliableForUser() {
		FacesContext context=FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		return null != user.getCountry() && user.getCountry().isSmsAvailable() &&  null != user.getMobilePhone() && user.getMobilePhone().length() > 0;
	}

	public TimeZone getTimeZone() {
        return TimeZone.getTimeZone(CommonUtil.getUtcOffset());
    }

	public static boolean isArabCountry(String countryId) {
		return arabCountriesId.contains(countryId);
	}

	public static boolean isArabCountry(Long countryId) {
		return isArabCountry(String.valueOf(countryId));
	}

	public boolean isUserArabCountry() {
		FacesContext context=FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		return isArabCountry(user.getCountryId());
	}

	public static String getStateCode(long stateId) {
		return usMap.get(Long.valueOf(stateId));
	}

	/**
	 * Check if the user have specific payment method by country & currency (just for German PM)
	 * @param paymentId payment method id
	 * @param countryId user country id
	 * @param currencyId user currency id
	 * @param isFromBanking if we came from banking page / not
	 * @return
	 */
	public static boolean isHavePaymentMethod(long paymentId, long countryId, long currencyId, boolean isBanking) {
		Country c = countries.get(countryId);

		if (null != c && null != c.getPaymentsMethodCountry() && null != c.getPaymentsMethodCountry().get(paymentId) &&
				!c.getPaymentsMethodCountry().get(paymentId).isExcluded()) {

			//  payment methods including currency for myaccount
			if (!isBanking) {
					if (paymentId == TransactionsManagerBase.PAYMENT_TYPE_DIRECT24 ){
						//Germany, Austria, Switzerland, Netherlands
						if (currencyId == Constants.CURRENCY_EUR_ID && (countryId == 80 || countryId == 15|| countryId == 201|| countryId == 145)) {
							return true;
						} else {
							return false;
						}
					
					}
					else if(paymentId == TransactionsManagerBase.PAYMENT_TYPE_GIROPAY ){
						//Germany
						if (currencyId == Constants.CURRENCY_EUR_ID && countryId == 80 ) {
							return true;
						} else {
							return false;
						}
						
					}
					else if(paymentId == TransactionsManagerBase.PAYMENT_TYPE_EPS) {
						// Austria
						if (currencyId == Constants.CURRENCY_EUR_ID && countryId == 15) {
							return true;
						} else {
							return false;
						}
					} else if (paymentId == TransactionsManagerBase.PAYMENT_TYPE_ACH) {
							if (currencyId == Constants.CURRENCY_USD_ID) {
								return true;
							} else {
								return false;
							}
					} else if (paymentId == TransactionsManagerBase.PAYMENT_TYPE_UKASH) {
						if (currencyId != Constants.CURRENCY_TRY_ID) { // for now we don't support TRY cuurency for ukash
							return true;
						} else {
							return false;
						}
					} else if(paymentId == TransactionsManagerBase.PAYMENT_TYPE_INATEC_IDEAL ){
						// Netherlands
						if (currencyId == Constants.CURRENCY_EUR_ID && countryId == 145 ) {
							return true;
						} else {
							return false;
						}
					} else if(paymentId == TransactionsManagerBase.PAYMENT_TYPE_NETELLER){
						if (currencyId != Constants.CURRENCY_TRY_ID && currencyId != Constants.CURRENCY_CZK_ID 
								&& currencyId != Constants.CURRENCY_ZAR_ID && currencyId != Constants.CURRENCY_PLN_ID) {
							return true;
						} else {
							return false;
						}
					} else { // other payment methods
						return true;
					}
		   } else {
			   return true;
		  }
		}
		return false;
	}

	/**
	 * Check if user have payment method by paymentId
	 * Take countryId and currencyId from user if exists in the session, otherwise take it
	 * from the session attributes / cookies
	 * @param paymentId payment method id
	 * @return
	 */
	public boolean isHavePaymentMethod(long paymentId) {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		User user = (User)request.getSession().getAttribute(Constants.BIND_SESSION_USER);
		long countryId = 0;
		long currencyId = 0;
		boolean isBanking = context.getViewRoot().getViewId().indexOf("jsp/banking") > -1 ||
		context.getViewRoot().getViewId().indexOf("mobile/banking") > -1 ? true : false;
		if (null == user || (null != user && user.getId() == 0)) {   // user not in session
			Skins skin = getSkinById(getSkinId());
			currencyId = skin.getDefaultCurrencyId();
			countryId = Long.parseLong(getRealCountryId());
			if (countryId == 0) {
				countryId = skin.getDefaultCountryId();
			}
			if (isBanking) {  // handle banking countries filter
				BankingForm bForm = (BankingForm) context.getApplication().createValueBinding("#{bankingForm}").getValue(context);
				countryId = bForm.getCountryId();
			}
		} else { // user in session
			countryId = user.getCountryId();
			currencyId = user.getCurrencyId();
			if (isBanking) {  // handle banking countries filter
				BankingForm bForm = (BankingForm) context.getApplication().createValueBinding("#{bankingForm}").getValue(context);
				countryId = bForm.getCountryId();
			}
		}
		return isHavePaymentMethod(paymentId, countryId, currencyId, isBanking);
	}

	/**
	 * @return support fax by checking if user is loggin
	 */
	public String getSupportFax() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		User user = (User)request.getSession().getAttribute(Constants.BIND_SESSION_USER);
		String countryId;
		if (null == user || (null != user && user.getId() == 0)) {   // user not in session
			Skins skin = getSkinById(getSkinId());
			countryId = getRealCountryId();
			if (null == countryId) {
				countryId = String.valueOf(skin.getDefaultCountryId());
			}
		} else { // user in session
			countryId = String.valueOf(user.getCountryId());
		}
		return countrySupportFaxes.get(countryId);
	}

	public boolean isHaveGiropayOption() throws Exception {
		return isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_GIROPAY);
	}

	public boolean isHaveDeltaPayChina() throws Exception {
		return isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_DELTAPAY_CHINA);
	}

	public boolean isHaveDirect24Option() throws Exception {
		return isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_DIRECT24);
	}

	public boolean isHaveEpsOption() throws Exception {
		return isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_EPS);
	}

	public boolean isHaveEnvoyOption() throws Exception {
		return isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_ENVOY);
	}

	public boolean isHaveEnvoyBankSpainOption() throws Exception {
		return isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_ENV_SPAIN_BANK);
	}

	public boolean isHaveEnvoyBankMexicoOption() throws Exception {
		return isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_ENV_MEXICO_BANK);
	}

	public boolean isHaveACHOption() throws Exception {
		return isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_ACH);
	}

	public boolean isHaveCashUOption() throws Exception {
		return isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_CASHU);
	}

	public boolean isHaveTeleingresoOption() throws Exception {
		return isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_TELEINGRESO);
	}

	public boolean isHavePoil2Option() throws Exception {
		return isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_POLI2);
	}

	public boolean isHaveSantanderOption() throws Exception {
		return isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_SANTANDER);
	}

	public boolean isHaveIdealOption() throws Exception {
		return isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_IDEAL);
	}

	public boolean isHaveMonetaOption() throws Exception {
		return isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_MONETA);
	}

	public boolean isHaveWebmoneyOption() throws Exception {
		return isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_WEB_MONEY);
	}

	public boolean isHaveUkashOption() throws Exception {
		return isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_UKASH);
	}

	public boolean isHaveDineroMailOption() throws Exception {
		return isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_DINERO_MAIL);
	}

	public boolean isHaveLobanetOption() throws Exception {
		return isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_LOBANET);
	}
	
	public boolean isHaveInatecIdealOption() throws Exception {
		return isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_INATEC_IDEAL);
	}
	
	public boolean isHaveNetellerOption() throws Exception {
		return isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_NETELLER);
	}

	public boolean isHaveBaroPayOption() throws Exception {
		return false;
		//return isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_BAROPAY);
	}
	
	public boolean isHaveEPGNetellerOption() throws Exception {
		return isHavePaymentMethod(TransactionsManagerBase.TRANS_TYPE_EPG_CHECKOUT_DEPOSIT);
	}

	public boolean isHaveMonetaWithdrawalOption() throws Exception {
		return (isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_MONETA_WITHDRAWAL) &&
				isHasEnvoyMethodDeposits(TransactionsManagerBase.PAYMENT_TYPE_MONETA));
	}

	public boolean isHaveWebMoneyWithdrawalOption() throws Exception {
		return (isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_WEB_MONEY_WITHDRAWAL) &&
				(isHasEnvoyMethodDeposits(TransactionsManagerBase.PAYMENT_TYPE_WEB_MONEY)
						|| isHasWebMoneyDeposits())//WebMoney ruble despoit
				);
	}

	public boolean isCurrencyLeftSymbol() {
		return getCurrencyById(getRealCurrencyId()).getIsLeftSymbolBool();
	}

	public int getCurrencyDecimalPointDigits() {
	    return getCurrencyById(getRealCurrencyId()).getDecimalPointDigits();
	}

	public boolean isHaveMoneybookersOption() throws Exception {
		return isHavePaymentMethod(TransactionsManagerBase.PAYMENT_TYPE_MONEYBOOKERS);
	}

	private boolean isHasEnvoyMethodDeposits(long paymentMethodId){
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		User user = (User)request.getSession().getAttribute(Constants.BIND_SESSION_USER);
		ArrayList<String> list = null;

		if (null != user && user.getId() > 0) {
			long userId = user.getId();

			try {
				list = TransactionsManagerBase.getEnvoyAccountNumsByUserId(userId, paymentMethodId);
			} catch (SQLException e) {
				logger.error("Problem in finding isHasEnvoyDeposits for user id: "+ userId,e );
			}

			if(null != list && !list.isEmpty()){
				return true;
			}
		}
		return false;
	}

	private boolean isHasWebMoneyDeposits(){
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		User user = (User)request.getSession().getAttribute(Constants.BIND_SESSION_USER);
		ArrayList<SelectItem> list = null;

		if (null != user && user.getId() > 0 && user.getCurrencyId() == ConstantsBase.CURRENCY_RUB_ID) {
			long userId = user.getId();
			try {
				list = TransactionsManagerBase.getWebMoneyPursesByUserId(userId);
			} catch (SQLException e) {
				logger.error("Problem in finding isHasWebMoneyDeposits for user id: "+ userId,e );
			}

			if(null != list && !list.isEmpty()){
				return true;
			}
		}
		return false;
	}


	public String getCampaignName() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		User user = (User)request.getSession().getAttribute(Constants.BIND_SESSION_USER);
		Long combId = null;
		String campaignName = null;
		if (null != user && user.getId() > 0) {
			campaignName = user.getCombination().getCampaignName();
		} else {
			combId = getCombIdWithSPLogic(false, null);
			campaignName = UsersManager.getCombinationById(combId).getCampaignName();
		}
		if (null == campaignName) {
			return "";
		}
		return campaignName;
	}

	/**
	 * Get session campaignId
	 */
	public long getCampaignId() throws SQLException {
		Long combId = getCombIdWithSPLogic(false, null);
		if (null == combId) {
			return 0;
		}
		return UsersManager.getCombinationById(combId).getCampaignId();
	}

	/**
	 * Get registerAttemptId and when he is null , get current timestamp
	 * */
	public String getRegisterAttemptId() {
		FacesContext context = FacesContext.getCurrentInstance();
		ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.REGISTER_ATTEMPT, Contact.class);
        Contact regAttempt = (Contact) ve.getValue(context.getELContext());
        if ( null == regAttempt || (null != regAttempt && regAttempt.getId() == 0)) {
            return Long.toString(new Date().getTime());
        } else {
        	return Long.toString(regAttempt.getId());
        }
	}

	/**
	 * Get combination with NetRefer spicial code logic
	 * For registration and for livePerson
	 * @param displayLog
	 * @return
	 * @throws SQLException
	 */
	public Long getCombIdWithSPLogic(boolean displayLog, UserBase user) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
		Long combId = null;
		String sc = UsersManager.getSpecialCode(req);
		if (null != sc) {
			if (null != user && user.getId() > 0) {
				user.setSpecialCode(sc);
			}
			if (displayLog) {
				logger.debug("special code, " + Constants.SPECIAL_CODE_PARAM + " = " + sc);
			}
			combId = getSkinById(getSkinId()).getNetreferCombinationId().get(0);
		} else {
			if (displayLog) {
				logger.debug("special code not found");
			}
			combId = UsersManager.getCombinationId(req);
		}
		if (displayLog) {
			logger.log(Level.DEBUG, "combId = " + combId);
		}

		if (null == sc && (combId == null || UsersManager.getCombinationById(combId.longValue()) == null)) {
			combId = getSkinById(getSkinId()).getDefaultCombinationId();
		}
		return combId;
	}

	/**
	 * Get combination with NetRefer spicial code logic
	 * For registration and for livePerson
	 * @param displayLog
	 * @return
	 * @throws SQLException
	 */
	public static Long getCombIdWithSPLogicStatic(HttpServletRequest request, long skinId) throws SQLException {
		Long combId = null;
		String sc = UsersManager.getSpecialCode(request);
		if (null != sc) {
			combId = getSkinById(skinId).getNetreferCombinationId().get(0);
		} else {
			combId = UsersManager.getCombinationId(request);
		}
		if (null == sc && (combId == null || UsersManager.getCombinationById(combId.longValue()) == null)) {
			combId = getSkinById(skinId).getDefaultCombinationId();
		}
		return combId;
	}

	/**
	 * @return
	 */
	public ArrayList<String> getDefualtCombination(){
		return defaultCombinations;
	}

	public String getDate(){
		return CommonUtil.getDateFormat(new Date());
	}

	/**
	 * Get first combinationId from cookies
	 * @return firstCombId
	 */
	public Long getFirstCombIdCookie(HttpServletRequest request) {
		Cookie[] cs = request.getCookies();
		Cookie cookie = null;
		if (cs != null) {
			for (int i = 0; i < cs.length; i++) {
				if (cs[i].getName().equals(Constants.FIRST_COMBINATION_ID)) {
					logger.log(Level.DEBUG, "found firstCombId cookie!");
					cookie = cs[i];
					break;
				}
			}
		}
		if (null != cookie && cookie.getValue() != null
				&& !cookie.getValue().equals("")
				&& cookie.getValue().indexOf("null") == -1) {
			try {
				return Long.valueOf(cookie.getValue());
			} catch (Exception e) {
				if (logger.isEnabledFor(Level.WARN)) {
					logger.warn("firstCombId cookie with strange value!", e);
				}
			}
		}
		return null;
	}

	/**
	 * Get first dymanicParam from cookies
	 * @return firstDymanicParam
	 */
	public String getFirstDynamicParamCookie(HttpServletRequest request) throws SQLException {
		Cookie[] cs = request.getCookies();
		Cookie cookie = null;
		if ( null != cs ) {
			for ( int i = 0 ; i < cs.length ; i++ ) {
				if ( cs[i].getName().equals(Constants.FIRST_DYNAMIC_PARAM)) {
					logger.info("found firstDp cookie!");
					cookie = cs[i];
					break;
				}
			}
		}
		if (cookie != null && cookie.getValue() != null
				&& !cookie.getValue().equals("")
				&& cookie.getValue().indexOf("null") == -1) {
			try {
				return new String(cookie.getValue());
			} catch (Exception e) {
				if (logger.isEnabledFor(Level.WARN)) {
					logger.warn("firstDp cookie with strange value!", e);
				}
			}
		}
		return null;
	}

	/**
	 * Get tradeDoubler uniqeId from cookies
	 * @return tradeDoubler uniqeId
	 */
	public String getTradeDoublerUIDCookie() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		Cookie[] cs = request.getCookies();
		Cookie cookie = null;
		if ( null != cs ) {
			for ( int i = 0 ; i < cs.length ; i++ ) {
				if (cs[i].getName().equals(Constants.TRADE_DOUBLER)) {
					logger.info("found tradeDoubler cookie!");
					cookie = cs[i];
					break;
				}
			}
		}
		if (cookie != null && cookie.getValue() != null
				&& !cookie.getValue().equals("")
				&& cookie.getValue().indexOf("null") == -1) {
			try {
				return new String(cookie.getValue());
			} catch (Exception e) {
				if (logger.isEnabledFor(Level.WARN)) {
					logger.warn("tradeDoubler cookie with strange value!", e);
				}
			}
		}
		return null;
	}

	/**
	 * All hours of the day in within half hour.
	 * value and label are the same.
	 * @return
	 */
	public static ArrayList<SelectItem> getHoursInDayByHalf() {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		SelectItem roundHours;
		SelectItem halfHours;
		for(long  i = 0; i < 24; i++){
			roundHours = new SelectItem();
			halfHours = new SelectItem();
			String hour  = String.valueOf(i);
			if(i < 10) {
				hour = "0" + hour;

			}
			roundHours.setLabel(hour + ":" + "00");
			roundHours.setValue(hour + ":" + "00");
			halfHours.setLabel(hour + ":" + "30");
			halfHours.setValue(hour + ":" + "30");

			si.add(roundHours);
			si.add(halfHours);
		}
		return si;
	}

	/**
	 * Get Mobile images sub directory
	 * @return
	 * @throws SQLException
	 */
	public String getImagesSubDirectory() throws SQLException {
		return getUserLocale().getLanguage() + "_" + getSkinId();
	}

	public boolean isTlv() {
		if (getSkinId() == Skin.SKIN_TLV){
			return true;
		}
		return false;
	}

	public boolean isChineseSkin() {
		if (getSkinId() == Skin.SKIN_CHINESE || getSkinId() == Skin.SKIN_CHINESE_VIP) {
			return true;
		}
		return false;
	}

	public boolean isArabicSkin(){
		if(getSkinId() == Skin.SKIN_ARABIC){
			return true;
		}
		return false;
	}

	/**
	 * Check if skin is arabic or TLV for align
	 * @return
	 */
	public boolean isRtlAlign(){
		if (getSkinId() == Skin.SKIN_ARABIC || getSkinId() == Skin.SKIN_ETRADER_INT || isTlv()){
			return true;
		}
		return false;
	}

	/**
	 * Return TLV path
	 * @return
	 */
	public String getTlvPath(){
		if (isTlv()){
			return "iw/";
		}
		return "";
	}

	/**
	 * Return TLV images path
	 * @return
	 */
	public String getTlvImagesPath(){
		if (getSkinId() == Skin.SKIN_TLV){
			return "iw_11/";
		}
		return "";
	}

	/**
	 * Return relevant path
	 * @return
	 */
	public String getRelevantPath(){
		if (isTlv()){
			return "iw/";
		} else if (isChineseSkin()) {
			return "zh/";
		} else if (isArabicSkin()){
			return "ar/";
		}
		return "";
	}

	public String getCancelWithdrawalDaysBalance() {
		return String.valueOf(Constants.CANCEL_WITHDRAWAL_DAYS_FOR_EMAIL_NOTIFICATION + Constants.CANCEL_WITHDRAWAL_DAYS_FOR_CANCEL);
	}

	public String getCancelWithdrawalDaysEmail() {
		return String.valueOf(Constants.CANCEL_WITHDRAWAL_DAYS_FOR_CANCEL);
	}

	/**
	 * Get start time and end time with user offset
	 * @param startDate Start time "xx:xx" in GMT
	 * @param endDate end time "xx:xx" in GMT
	 * @return "startDate - endDate" with user utc offcet
	 */
	public String getStartEndTimeWithUserOffset(String startDate, String endDate){
		return getTimeWithUserOffset(startDate) + " - " + getTimeWithUserOffset(endDate);
	}

	/**
	 * Get  time with user offset
	 * @param time "xx:xx" in GMT
	 * @return time user utc offcet
	 */
	public String getTimeWithUserOffset(String date){
		String[] time = date.split(":");
		GregorianCalendar gc = new GregorianCalendar();
		gc.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
		gc.set(Calendar.MINUTE, Integer.valueOf(time[1]));
		String timeWithOffset = CommonUtil.getTimeFormat(gc.getTime() , getUtcOffset());
		return timeWithOffset;
	}


	public String getEuroUsdTimeOptionPlus(){
		return getStartEndTimeWithUserOffset(Constants.OPTION_PLUS_EURO_USD_START_DATE, Constants.OPTION_PLUS_EURO_USD_END_DATE);
	}

	public String getSpTimeOptionPlus(){
		return getStartEndTimeWithUserOffset(Constants.OPTION_PLUS_SP_START_DATE, Constants.OPTION_PLUS_SP_END_DATE);
	}

	public String getDaxTimeOptionPlus(){
		return getStartEndTimeWithUserOffset(Constants.OPTION_PLUS_DAX_START_DATE, Constants.OPTION_PLUS_DAX_END_DATE);
	}

	public String getOilTimeOptionPlus(){
		return getStartEndTimeWithUserOffset(Constants.OPTION_PLUS_OIL_START_DATE, Constants.OPTION_PLUS_OIL_END_DATE);
	}

	public String getIbexTimeOptionPlus(){
		return getStartEndTimeWithUserOffset(Constants.OPTION_PLUS_IBEX_START_DATE, Constants.OPTION_PLUS_IBEX_END_DATE);
	}

    public String getRtsFutTimeOptionPlus(){
        return getStartEndTimeWithUserOffset(Constants.OPTION_PLUS_RTS_FUT_START_DATE, Constants.OPTION_PLUS_RTS_FUT_END_DATE);
    }

    public String getAudUsdTimeOptionPlus(){
        return getStartEndTimeWithUserOffset(Constants.OPTION_PLUS_AUD_USD_START_DATE, Constants.OPTION_PLUS_AUD_USD_END_DATE);
    }
    public String getKospiTimeOptionPlus(){
        return getStartEndTimeWithUserOffset(Constants.OPTION_PLUS_KOSPI_START_DATE, Constants.OPTION_PLUS_KOSPI_END_DATE);
    }

	public  boolean isXmassBonusTime(){
		return UsersManager.isXmassBonusTime(getSkinId().intValue());
	}

    /**
     * @return the invScheduled
     */
    public static HashMap<Integer, String> getInvScheduled() {
        return invScheduled;
    }

    /**
     * @param invScheduled the invScheduled to set
     */
    public static void setInvScheduled(HashMap<Integer, String> invScheduled) {
        ApplicationData.invScheduled = invScheduled;
    }

    /**
     * @return the invType
     */
    public static HashMap<Integer, String> getInvType() {
        return invType;
    }

    /**
     * @param invType the invType to set
     */
    public static void setInvType(HashMap<Integer, String> invType) {
        ApplicationData.invType = invType;
    }

    public boolean isIphoneApp() {
		if (CommonUtil.isMobileApp()) {
			String userAgent = CommonUtil.getUserAgent();
			if (null != userAgent &&
					(userAgent.indexOf("iphone") > -1 ||
					 userAgent.indexOf("ipod") > -1)) {
				return true;
			} else {
				return false;
			}

		} else {
			return false;
		}
	}

    public boolean isAndroidApp() {
		if (CommonUtil.isMobileApp()) {
			String userAgent = CommonUtil.getUserAgent();
			if (null != userAgent &&
					(userAgent.indexOf("android") > -1)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

    public boolean isIphone() {
        if (CommonUtil.isMobile()) {
            String userAgent = CommonUtil.getUserAgent();
            if (null != userAgent &&
                    (userAgent.indexOf("iphone") > -1 ||
                     userAgent.indexOf("ipod") > -1)) {
                return true;
            }
        }
        return false;
    }

    public boolean isAndroid() {
        if (CommonUtil.isMobile()) {
            String userAgent = CommonUtil.getUserAgent();
            if (null != userAgent &&
                    (userAgent.indexOf("android") > -1)) {
                return true;
            }
        }
        return false;
    }

    public String getCssPath() {
    	String cssFile = Constants.MOBILE_IPHONE_CSS;
    	if (isAndroidApp()) {
    		cssFile = Constants.MOBILE_ANDROID_CSS;
    	}
    	return cssFile;
    }

    public String getDevicePath() {
    	String path = Constants.MOBILE_IPHONE_PATH;
    	if (isAndroidApp()) {
    		path = Constants.MOBILE_ANDROID_PATH;
    	}
    	return path;
    }

    /**
     * Handle mobileApp request from iphoneapp page
     * duid, combid, language selection and skin logic
     *
     * The code is taken from the EtraderPhaseListener for make it more cleaner
     *
     * @param request
     * @param response
     * @param quer
     * @param context
     * @param user  instance from the session
     */
    public void handleMobileAppRequest(HttpServletRequest request, HttpServletResponse response, String quer, FacesContext context, User user) {
		String deviceUniqueId = null;
		if (null != quer && quer.indexOf(Constants.DEVICE_UNIQUE_ID) > -1) {
			deviceUniqueId = request.getParameter(Constants.DEVICE_UNIQUE_ID);
			if (null != deviceUniqueId) {  // save in the user session
				request.getSession().setAttribute(Constants.DEVICE_UNIQUE_ID, deviceUniqueId);
			}
		}

		if (null == user && null != deviceUniqueId) {
			// handle skinId from the app
			String skinIdParameter = request.getParameter(Constants.SKIN_ID);
			long sParam = 0;
			if (!CommonUtil.isParameterEmptyOrNull(skinIdParameter)) {
				try {
					sParam = Integer.parseInt(skinIdParameter);
					if (null == getSkinById(sParam)) {   // not a real skinId
						sParam = 0;
						logger.log(Level.DEBUG, "MobileApp skinId not recognized, s:" + sParam);
					}
				} catch (Exception e) {   // skinId not a number
					logger.info("Problem parsing skinId for mobileApp", e);
				}
			}
			// handle conbId from the app
			String combId = request.getParameter(Constants.COMBINATION_ID);
			long combinationId = 0;
			if (!CommonUtil.isParameterEmptyOrNull(combId)) {
				try {
					long combination = Long.valueOf(combId);
					if (null == AdminManager.getCombinationId(combination)) {
						logger.log(Level.DEBUG, "MobileApp combId not recognized, combId:" + combination);
					} else {
						combinationId = combination;
						request.getSession().setAttribute(Constants.COMBINATION_ID, combId);
						addCookie(Constants.COMBINATION_ID, combId, request, response, Constants.COMBINATION_ID);
					}
				} catch (Exception e) {
					logger.info("Problem parsing combId for mobileApp", e);
				}
			}
			// get application version parameter
			String appVer = request.getParameter(Constants.MOBILE_APP_VER);
			if (null == appVer || appVer.trim().equals("")) {
				request.getSession().setAttribute(Constants.MOBILE_APP_VER, "1.0");
			} else {
				request.getSession().setAttribute(Constants.MOBILE_APP_VER, appVer);
			}
			try {
				DeviceUniqueIdSkin duidRecord = DeviceUniqueIdsSkinManager.getSkinId(deviceUniqueId);
				if (null != duidRecord &&
						duidRecord.getId() != 0 &&
						duidRecord.getSkinId() != 0) {  // duid already stored in our DB

					if (sParam != 0 && duidRecord.getSkinId() != sParam) {
						DeviceUniqueIdsSkinManager.update(deviceUniqueId, sParam);
						duidRecord.setSkinId(sParam);
						logger.debug("mobileApp update new skinId:" + sParam + " for duid: " + deviceUniqueId);
					}
					if (combinationId != 0 && duidRecord.getCombId() != combinationId) {
						DeviceUniqueIdsSkinManager.updateCombinationId(deviceUniqueId, combinationId);
						logger.debug("mobileApp update combId:" + combinationId + " for duid: " + deviceUniqueId);
					}
					if (null != appVer &&
							(null == duidRecord.getAppVer() || !duidRecord.getAppVer().equals(appVer))) {
						DeviceUniqueIdsSkinManager.updateVersion(deviceUniqueId, appVer);
						logger.debug("mobileApp update appVersion:" + appVer + " for duid: " + deviceUniqueId);
					}
					CommonUtil.sendPermanentRedirect(response, context.getExternalContext().getRequestContextPath() + "/mobile/index.jsf?s=" + duidRecord.getSkinId());
					context.responseComplete();
					return;
				} else {  // duid not in DB / without skinId - select language page (iphoneapp)
					long duidSkinId = 0;
					if (sParam == 0) {
						// set default locale for opening the language selection page
						request.getSession().setAttribute(Constants.USER_LOCALE, new Locale(Constants.LOCALE_DEFAULT));
						request.getSession().setAttribute(Constants.SKIN_ID, String.valueOf(Skin.SKINS_DEFAULT));
					} else {
						duidSkinId = sParam;
					}
					if (null == duidRecord) {
						String countryId = getRealCountryId();
						if (null == countryId || countryId.equals("0")) {
							countryId = String.valueOf(ApplicationData.getSkinById(getSkinId()).getDefaultCountryId());
						}
						DeviceUniqueIdsSkinManager.insert(deviceUniqueId,   duidSkinId, CommonUtil.getUserAgent(),
								CommonUtil.getIPAddress(), Long.parseLong(countryId), combinationId, appVer);
					}
					if (duidSkinId != 0) {
						CommonUtil.sendPermanentRedirect(response, context.getExternalContext().getRequestContextPath() + "/mobile/index.jsf?s=" + duidSkinId);
						context.responseComplete();
					}
					return;
				}
			} catch (SQLException e) {
				logger.error("Error! can't get/insert skinId by duid: " + deviceUniqueId);
			}
		}
		CommonUtil.sendPermanentRedirect(response, context.getExternalContext().getRequestContextPath() + "/mobile/index.jsf");
		context.responseComplete();
		return;
    }

    /**
     * Filter for banking banners
     * Return true just if banking banner should be available / ticker is clickable
     * @return
     */
    public boolean isDisplayBankingFooter() {
		FacesContext context = FacesContext.getCurrentInstance();
		if ((context.getViewRoot().getViewId().indexOf("register.xhtml") > -1) ||
				(context.getViewRoot().getViewId().indexOf("trade_binary_options.xhtml") > -1) ||
				(context.getViewRoot().getViewId().indexOf("deposit.xhtml") > -1) ||
				(context.getViewRoot().getViewId().indexOf("newCard.xhtml") > -1) ||
				(context.getViewRoot().getViewId().indexOf("firstNewCard.xhtml") > -1) ||
				(context.getViewRoot().getViewId().indexOf("direct24.xhtml") > -1) ||
				(context.getViewRoot().getViewId().indexOf("giropay.xhtml") > -1) ||
				(context.getViewRoot().getViewId().indexOf("eps.xhtml") > -1) ||
				(context.getViewRoot().getViewId().indexOf("envoyBankSpain.xhtml") > -1) ||
				(context.getViewRoot().getViewId().indexOf("ACH.xhtml") > -1) ||
				(context.getViewRoot().getViewId().indexOf("ukash.xhtml") > -1) ||
				(context.getViewRoot().getViewId().indexOf("envoyDepositCashU.xhtml") > -1) ||
				(context.getViewRoot().getViewId().indexOf("wireDeposit.xhtml") > -1) ||
				(context.getViewRoot().getViewId().indexOf("envoyBankMexico.xhtml") > -1) ||
				(context.getViewRoot().getViewId().indexOf("envoyDeposit.xhtml") > -1) ||
				(context.getViewRoot().getViewId().indexOf("envoyDepositWebMoney.xhtml") > -1) ||
				(context.getViewRoot().getViewId().indexOf("envoyDepositMoneta.xhtml") > -1) ||
				(context.getViewRoot().getViewId().indexOf("receipt.xhtml") > -1) ||
				(context.getViewRoot().getViewId().indexOf("receiptFirstDeposit.xhtml") > -1)) {
			return false;
		}
    	return true;
    }

    /**
     * Filter for register,
     * Return true just if arrive from register.
     * @return
     */
    public boolean isNewTemplateDesign() {
		FacesContext context = FacesContext.getCurrentInstance();
		if ((context.getViewRoot().getViewId().indexOf("register.xhtml") > -1) ||
				(context.getViewRoot().getViewId().indexOf("firstNewCard.xhtml") > -1)) {
			return true;
		}
    	return false;
    }

    public boolean isArriveFromContactusLanding() throws UnsupportedEncodingException {
    	FacesContext context = FacesContext.getCurrentInstance();
    	HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
    	HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
    	String isFromLanding = (String) session.getAttribute(Constants.REGISTER_FROM_CONTACTUS_LANDING);

    	// first time getting from the request
    	String firstNameR = request.getParameter("firstName");
    	String lastNameR = request.getParameter("last_Name");
    	if (!CommonUtil.isParameterEmptyOrNull(request.getQueryString())) {//for latin/hebrew charcters
    		String s = URLDecoder.decode(request.getQueryString(), "utf-8");
			//***********Function to break the NVP string into a HashMap*********************//
			 HashMap<String, String> nvp = new HashMap<String, String>();
		      StringTokenizer stTok = new StringTokenizer( s, "&");
		      while (stTok.hasMoreTokens())  {
		    	  try {
		    		  StringTokenizer stInternalTokenizer = new StringTokenizer(stTok.nextToken(), "=");
		    		  String key = URLDecoder.decode(stInternalTokenizer.nextToken(), "UTF-8");
		    		  String value = URLDecoder.decode( stInternalTokenizer.nextToken(), "UTF-8");
		    		  nvp.put(key.toUpperCase(), value);
		    	  } catch (Exception e) { // avoid receiving parameters without a value
		    		  logger.error("Parameter without value after landing page");
				}
		      }
			//*******************************************************************************//
			if (!CommonUtil.isParameterEmptyOrNull(nvp.get("FIRSTNAME"))) {
				firstNameR = nvp.get("FIRSTNAME");
			}
			if (!CommonUtil.isParameterEmptyOrNull(nvp.get("LAST_NAME"))) {
				lastNameR = nvp.get("LAST_NAME");
			}
    	}
    	// later use, getting from the session
    	String firstNameS = (String) session.getAttribute(Constants.REGISTER_FROM_CONTACTUS_LANDING_FN);
    	String lastNameS = (String) session.getAttribute(Constants.REGISTER_FROM_CONTACTUS_LANDING_LN);

    	if (null != isFromLanding && isFromLanding.equals("true")){
    		if ((null != firstNameR && null != lastNameR)||
    				(null != firstNameS && null != lastNameS)) {
    			if (null == firstNameS && null == lastNameS) {  // first time
	        		session.setAttribute(Constants.REGISTER_FROM_CONTACTUS_LANDING_FN, firstNameR);
	        		session.setAttribute(Constants.REGISTER_FROM_CONTACTUS_LANDING_LN, lastNameR);
    			}
        		return true;
    		}
    	}
		return false;
    }

    public String getContactusLandingMsg() {
    	String msg = "";
    	FacesContext context = FacesContext.getCurrentInstance();
    	HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
    	String firstName = (String) session.getAttribute(Constants.REGISTER_FROM_CONTACTUS_LANDING_FN);
    	String lastName = (String) session.getAttribute(Constants.REGISTER_FROM_CONTACTUS_LANDING_LN);
    	String name = firstName;
    	if (getSkinId() == Skin.SKIN_GERMAN) {
    		name = lastName;
    	}
    	if (null != name) {
    		msg = CommonUtil.getMessage("register.contactus.landing.1", new String[] {name}) +
    			CommonUtil.getMessage("register.contactus.landing.2", null) +
    			CommonUtil.getMessage("register.contactus.landing.3", null);
    	}
    	return msg;
    }

    public String getContactusLandingMsgE() {
    	String msg = "";
    	FacesContext context = FacesContext.getCurrentInstance();
    	HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
    	String firstName = (String) session.getAttribute(Constants.REGISTER_FROM_CONTACTUS_LANDING_FN);
    	msg = firstName;
    	return msg;
    }

    /**
     * get the indcies markets for et group page ET
     * @return market list of all the indcies
     */
    public ArrayList<Market> getEtIndices() {
        return ETMarketGroups.get(Constants.MARKET_GROUP_INDICES_LONG);
    }

    /**
     * get the stocks markets for et group page ET
     * @return market list of all the stocks
     */
    public ArrayList<Market> getEtStocks() {
        return ETMarketGroups.get(Constants.MARKET_GROUP_STOCKS_LONG);
    }

    /**
     * get the Currencies markets for et group page ET
     * @return market list of all the Currencies
     */
    public ArrayList<Market> getEtCurrencies() {
        return ETMarketGroups.get(Constants.MARKET_GROUP_CURRENCIES_LONG);
    }

    /**
     * get the Commodities markets for et group page ET
     * @return market list of all the Commodities
     */
    public ArrayList<Market> getEtCommodities() {
        return ETMarketGroups.get(Constants.MARKET_GROUP_COMMODITIES_LONG);
    }

    /**
     * get the Foreign Stocks markets for et group page ET
     * @return market list of all the Foreign Stocks
     */
    public ArrayList<Market> getEtForeignStocks() {
        return ETMarketGroups.get(Constants.MARKET_GROUP_ABROAD_STOCKS_LONG);
    }

    public boolean isUsSkins() {
    	long skinId = getSkinId();
    	if (skinId == Skin.SKIN_EN_US ||
    			skinId == Skin.SKIN_ES_US) {
    		return true;
    	}
    	return false;
    }

    public boolean isShowNonDepositorPopup() {
        /*FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
    	try {
	        String cvt = UsersManager.getCustomerVisitType(request);
	        if (null != cvt && cvt.equals(Constants.CUSTOMER_VISIT_TYPE_NON_DEPOSITOR)) {  // save cookie
	        	String nonDepositTime = UsersManager.getNonDepositTimeCookie(request);
	        	if (nonDepositTime != null && nonDepositTime.length() > 0) {
	        		long nonDepositMilis = Long.valueOf(nonDepositTime);
	        		Calendar cal = Calendar.getInstance();
	        		Long timeOut = Long.valueOf(CommonUtil.getEnum(Constants.ENUM_NON_DEP_ENUMERATOR,Constants.ENUM_NON_DEP_CODE));
		        	if ((nonDepositMilis + timeOut) < cal.getTimeInMillis()) {
		                logger.debug("show non depositor popup");
		                return true;
		        	}
	        	} else {
	        		logger.debug("show non depositor popup first time");
	                return true;
	        	}
	        }
        } catch (Exception e) {
            logger.warn("Problem geting is depositor popup ", e);
        }*/
        logger.debug("don't show non depositor popup  ");
        return false;
    }


    public String getShowedNonDepositorPopup() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        Calendar cal = Calendar.getInstance();
        addCookie(Constants.NON_DEPOSITOR_POPUP_TIME, String.valueOf(cal.getTimeInMillis()), response, Constants.NON_DEPOSITOR_POPUP_TIME);
        return "";
    }

    /**
     * For Ukash message we need to navigate users to Ukash site by there country code and locale
     * @return
     */
    public static HashMap<String, Locale> createLocalesList(){
    	Locale[] locales = Locale.getAvailableLocales();
    	HashMap<String, Locale> hm = new HashMap<String, Locale>();
    	for (Locale l : locales){
    		hm.put(l.getCountry(), l);
    	}
    	return hm;
    }

	/**
	 * @return the localeList
	 */
	public static HashMap<String, Locale> getLocaleList() {
		return localeList;
	}

	/**
	 * @return the webMoneyClearingProviderId
	 */
	public static HashMap<Long, Long> getWebMoneyClearingProviderId() {
		return webMoneyClearingProviderId;
	}

	/**
	 * Return WebMoney clearing provider Id by currency Id
	 * @return
	 */
	public static HashMap<Long, Long> setWebMoneyClearingProviders(){
		HashMap<Long, Long> hm = new HashMap<Long, Long>();
		hm.put(ConstantsBase.CURRENCY_USD_ID, ClearingManager.WEBMONEY_PROVIDER_ID_USD);
		hm.put(ConstantsBase.CURRENCY_EUR_ID, ClearingManager.WEBMONEY_PROVIDER_ID_EUR);
		hm.put(ConstantsBase.CURRENCY_RUB_ID, ClearingManager.WEBMONEY_PROVIDER_ID_RUB);
		return hm;
	}

	public long getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(long checkNumber) {
		this.checkNumber = checkNumber;
	}

	public boolean getChinese() {
		return isChineseSkinAndCountryAndCurrency();
	}

	public static boolean isChineseSkinAndCountryAndCurrency() {
		try {
			User user = getUserFromSession();
			if (user.getSkinId() == 15 &&
					user.getCountryId() == Constants.CHINA_COUNTRY_ID_INT &&
					user.getCurrencyId() == Constants.CURRENCY_CNY_ID) {
				return true;
			}
			return false;
		} catch (Exception e) {
			logger.info("The user was not loaded from session !!!!");
			return false;
		}
	}

	public String getSupportPhoneByIp(){
		return getSupportPhoneByCountry();
	}

	public static String getSupportPhoneByCountry(){
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String ip = CommonUtil.getIPAddress(request);
		String countryCode = CommonUtil.getCountryCodeByIp(ip);
		Connection con = null;
		long countryId = 0;
			try {
				con = getConnection();
				countryId = CountryDAOBase.getIdByCode(con, countryCode);
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				closeConnection(con);
			}
		if(countryCode == null){
			return CommonUtil.getMessage("contact.phone.default", null);
		}
		if (countryId == 0 && !CommonUtil.getProperty("system").equals("live")) {
			logger.error("error with geoIP check setup, setting default country id 219 UK" );
			countryId = 219;
		}
		return  getCountry(countryId).getSupportPhone();


	}

	/**
	 * Return URL prefix by skinId.
	 * Examples:
	 * 1. For Italian skin will return 'it'.
	 * 2. For English skin will return 'www'
	 * 3. For EN-US skin will return 'www'
	 * 4. For ES-US skin will return 'es'
	 * @return
	 * @throws SQLException
	 */
	public String getUrlPrefixBySkin() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
	    String protocol = getProtocolHttpOrHttps();
		String prefix = getPrefixBySkin();
		String domain = null;
		try{
			domain = request.getServerName().substring(request.getServerName().indexOf('.'));
		} catch(Exception e) {} 
		if (domain == null) {
			domain = ".anyoption.com";
		}
		return protocol + prefix + domain;
	}

	public static boolean isAllowAffilate(HttpServletRequest request) {
		String dynamicParam;
		Long affiliateKey;
		try {
			dynamicParam = UsersManager.getDynamicParam(request);
			affiliateKey = ApplicationData.getAffIdFromDynamicParameter(dynamicParam);
		} catch (Exception e) {
			logger.error("can not get dynamic parameter for is allow affilate" + e);
			return false;
		}
		MarketingAffilate ma = affiliatesHM.get(affiliateKey);
		if (null != ma) {
			return ma.isAllowAffilate();
		}
		return false;
	}

	public String getSubDomain(String skin, HttpServletRequest request) {
    	String res = "";    
        String isRegulatedCookie = ""; 
        boolean isRegulated = false;
        Cookie[] cs = request.getCookies();
        if (cs != null) {
            for (int i = 0; i < cs.length; i++) {
                if (cs[i].getName().equals(Constants.IS_REGULATED)) {
                    isRegulatedCookie = cs[i].getValue();
                }
            }
        }
        if(!CommonUtil.isParameterEmptyOrNull(isRegulatedCookie)) {
            isRegulated = Boolean.parseBoolean(isRegulatedCookie);
        }
        
    	String subDomainFromSkin = ApplicationData.getSubDomainBySkin(skin, isRegulated);
		if(!CommonUtil.isParameterEmptyOrNull(subDomainFromSkin)){
			res = CommonUtil.getProperty("homepage.url").replace("www", subDomainFromSkin);
		} else {
			res = CommonUtil.getProperty("homepage.url");
		}
    	return res.substring(0, res.length() - 1);
	}
	
	public String getSubDomain(String skin) {
        String res = "";
            String subDomainFromSkin = ApplicationData.getSubDomainBySkin(skin, CommonUtil.isUserSkinRegulated());
            if(!CommonUtil.isParameterEmptyOrNull(subDomainFromSkin)){
                res = CommonUtil.getProperty("homepage.url").replace("www", subDomainFromSkin);
            } else {
                res = CommonUtil.getProperty("homepage.url");
            }
        return res.substring(0, res.length() - 1);
    }

	public static boolean checkEnvoyIP(String ip) {
		try {
		String endIpString = ip.substring(ip.lastIndexOf(".") + 1 ,ip.length());
		int endIpInt = Integer.valueOf(endIpString);
		if (ip.equals(ConstantsBase.ENVOY_NOTIFICATIONS_IP_1) ||
		    ip.equals(ConstantsBase.ENVOY_NOTIFICATIONS_IP_2) ||
		    (ip.startsWith(ConstantsBase.ENVOY_NOTIFICATIONS_IPS) && endIpInt >= ConstantsBase.ENVOY_NOTIFICATIONS_IP_START && endIpInt <= ConstantsBase.ENVOY_NOTIFICATIONS_IP_END)) {
			   return true;
		   }

		} catch (Exception e) {
			logger.error("Can't check envoy IP" + e);
		}
		return false;
	}

	/**
     * get last show off by skinId.
     * from cach or from db if last time was more then one hour
     *
     * @return
     */
    public Investment isShowOff(long rowNum, boolean showOffLocale, String langCode, boolean shortText) {
    	long skinId = getSkinId();
    	Calendar now = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        if ( null == ShowOffInvestment || now.after(lastLoadTimeShowOff) ) {
        	ShowOffInvestment = isHasShowOffBySkinId(rowNum, showOffLocale, langCode, skinId, shortText);
        }
        return ShowOffInvestment;
    }

    /**
     * get show off
     * @param
     * @return
     */
	public static Investment isHasShowOffBySkinId(long rowNum, boolean showOffLocale, String langCode, long skinId, boolean shortText) {
    	List<Investment> invest = null;
    	try {
    		invest = InvestmentsManager.getShowOffInvestmentsList(1, showOffLocale, langCode, skinId, shortText);
    		Calendar now = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
    		now.add(Calendar.HOUR_OF_DAY, 1);
    		lastLoadTimeShowOff = now;
    	 } catch (SQLException sqle) {
             logger.error("Can't load is Has show off for skin id = " + skinId , sqle);
         }
    	 return invest.get(0);
	}

	public boolean isFirstTimePixel() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String firstTimePixel = "false";
		String requestPixelPageName = null;
		String url = request.getRequestURL().toString();
		for (int i = 0; i < pixelPageName.size(); i++) {
			if (url.indexOf(pixelPageName.get(i).getLabel()) > -1) {
				requestPixelPageName = pixelPageName.get(i).getLabel();
			}
		}
		firstTimePixel = (String)session.getAttribute(requestPixelPageName);
		if (null == firstTimePixel) { // save attribute in the session
			session.setAttribute(requestPixelPageName, "true");
			return true;
		}
		return false;
	}

	public static ArrayList<CachePages> getCachePages() {
		if(cachePages == null) {
			Connection con = null;
			try {
				con = getConnection();	
				cachePages = CachePagesDAO.getAllCachePages(con);
			}catch(SQLException ex) {
				logger.error("Can't lazzy load cachePages", ex );
			} finally {
				closeConnection(con);
			}
		}
		return cachePages;
	}

	/**
     * get list of countries in form of [id, country name]
     * @param
     * @return String in form of  [id, country name],[...]...
     */
	public String getDisplayCountry() {
		StringBuffer  countriesBuffer = new StringBuffer();
		countriesBuffer.append("[");
		Set<Long> keys = countries.keySet();
		Iterator<Long> iter = keys.iterator();
		while ( iter.hasNext() ) {
			Long countryId = (Long)iter.next();
			countriesBuffer.append("['" + countryId.toString()).append("' , '");
			countriesBuffer.append(CommonUtil.getMessage(countries.get(countryId).getDisplayName() , null).replace("'", "")).append("'], ");
		}
		int x = countriesBuffer.lastIndexOf(",");
		countriesBuffer.deleteCharAt(x);
		countriesBuffer.append("]");
		return countriesBuffer.toString();
	}
	
    /**
     * get list of countries in form of [id, a2 country display]
     * @param
     * @return String in form of  [id, a2 country display],[...]...
     */
	public String getDisplayCountryA2() {
		StringBuffer  countriesBuffer = new StringBuffer();
		countriesBuffer.append("[");
		Set<Long> keys = countries.keySet();
		Iterator<Long> iter = keys.iterator();
		while ( iter.hasNext() ) {
			Long countryId = (Long)iter.next();
			countriesBuffer.append("['" + countryId.toString()).append("' , '");
			countriesBuffer.append(countries.get(countryId).getA2().replace("'", "")).append("'], ");
		}
		int x = countriesBuffer.lastIndexOf(",");
		countriesBuffer.deleteCharAt(x);
		countriesBuffer.append("]");
		return countriesBuffer.toString();
	}

	/**
	 *
	 * @param dateOption
	 * @return last top trade by one of the options: day, week or month.
	 */
	public ArrayList<Investment> getLastTopTrades(String dateOption) {
		long dateOptionValue = 1; // last day as default.
		if (dateOption != null) {
			dateOptionValue = Long.valueOf(dateOption);
		}
		return lastTopTrades.get(dateOptionValue);
	}

	/**
	 *
	 * update last top Trades for last day, week and month.
	 */
	public String getUpdateLastTopTrades() {
		Calendar now  = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		if ( null == lastTopTrades || lastTopTrades.isEmpty() || now.after(lastLoadTimeTopTrades) ) {
			logger.debug("start get last top trades in : " + now);
			try {
				long skinBusinessCaseId = Skin.SKIN_BUSINESS_ANYOPTION;
				if(this.getSkinId() == Skin.SKIN_ETRADER) {
					skinBusinessCaseId = Skin.SKIN_BUSINESS_ETRADER;
				}
				HashMap<Long, ArrayList<Investment>> lastTopTradesTemp;
				int lastDay = ConstantsBase.LIVE_TOP_TRADES_TODAY;
				do {
					lastTopTradesTemp = InvestmentsManager.getTopTrades(getFromLastDays(lastDay), skinBusinessCaseId);
					if (lastTopTradesTemp != null && !lastTopTradesTemp.isEmpty()) {
						lastTopTrades = lastTopTradesTemp; 
					}
					lastDay--; // when no top trades today, take top trades from day before and so on.
				} while (null == lastTopTrades || lastTopTrades.isEmpty());
				now.add(Calendar.DAY_OF_MONTH, 1);
				now.set(Calendar.HOUR_OF_DAY, 00);
				now.set(Calendar.MINUTE, 30);
				lastLoadTimeTopTrades = now;
				logger.debug("finished get last top trades. next time we will get top trades will be in : " + lastLoadTimeTopTrades);
			} catch (SQLException e) {
				logger.error("Can't load is last top trades", e);
			}
        }
		return null;
	}

	/**
	 * Set default city name for users that their city name can't be found on google API
	 * @param list
	 * @return
	 */
	private void setDefaultCapitalCity(ArrayList<Investment> list) {				
		for (Investment i: list) {
			if (Long.valueOf(i.getSkin()) == Skin.SKIN_BUSINESS_ETRADER) {
				LiveGlobeCity randomCity = LiveHelper.getRandomCityForEtrader();
				i.setCountryId(randomCity.getCountryId());
				i.setCountryName(getCountry(randomCity.getCountryId()).getDisplayName());
				i.setCityName(randomCity.getCityName());
			} else if (CommonUtil.isParameterEmptyOrNull(i.getCityName())) {
				i.setCityName(getCountries().get(i.getCountryId()).getCapitalCity());
			}
		}
	}

	/**
	 *
	 * @return date of last day as SimpleDateFormat
	 */
	private static String getFromLastDays(int lastDays) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		gc.add(GregorianCalendar.DAY_OF_MONTH, lastDays);
		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
		return sd.format(gc.getTime());
	}

	/**
	 *
	 * @return date of today as SimpleDateFormat
	 
	private static String getTillLastDay() {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
		return sd.format(gc.getTime());
	}
*/
	/**
	 * Get Display Opportunity Product Type names.
	 * @return
	 * 		String[][] - array that contains opportunity product type id, display_name for each opportunity product type
	 * 		1. Binary
	 * 		2. One Touch
	 * 		3. Option +
	 * 		4. Binary 0-100
	 * @throws SQLException
	 */
	public String getDisplayOpportunityProductType() throws SQLException {
		StringBuffer  opportunityProductTypes = new StringBuffer();
		opportunityProductTypes.append("[");
		try {
			ArrayList<SelectItem> list1 =  getOpportunityProductTypes();
			for (int i = 0; i < list1.size(); i++) {
				String opportunityProductTypeId = list1.get(i).getLabel();
				String opportunityProductTypeName = list1.get(i).getValue().toString();
				opportunityProductTypes.append("['" + opportunityProductTypeId).append("' , '");
				opportunityProductTypes.append(opportunityProductTypeName).append("'], ");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	    int ind = opportunityProductTypes.lastIndexOf(",");
	    if (ind != -1) {
	    	opportunityProductTypes.deleteCharAt(ind);
	    }
	    opportunityProductTypes.append("]");
		return opportunityProductTypes.toString();
	}

	/**
	 * Return markets decimal point
	 * @return
	 * 		String[][] - array that contains decimal point for each markets
	 * @throws SQLException
	 */
	public String getMarketsDecimalPoint() throws SQLException {
		StringBuffer  marketsDecimalPointBuffer = new StringBuffer();
		marketsDecimalPointBuffer.append("[");
		try {
			Map<Long, String> map =  MarketsManager.getMarketsDecimalPoint();
			for (Map.Entry<Long, String> entry : map.entrySet()) {
				String decimalPoint  = entry.getKey().toString();
				String marketId = entry.getValue();
				marketsDecimalPointBuffer.append("['" + marketId).append("' , '");
				marketsDecimalPointBuffer.append(decimalPoint).append("'], ");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	    int ind = marketsDecimalPointBuffer.lastIndexOf(",");
	    if (ind != -1) {
	    	marketsDecimalPointBuffer.deleteCharAt(ind);
	    }
	    marketsDecimalPointBuffer.append("]");
		return marketsDecimalPointBuffer.toString();
	}

	/**
	 *
	 * @return date of last day as SimpleDateFormat
	 */
	private static String getFromLastDayOfWeek(int lastDays) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		gc.setFirstDayOfWeek(GregorianCalendar.TUESDAY);
		gc.add(GregorianCalendar.DAY_OF_MONTH, lastDays);
		if (gc.get(GregorianCalendar.DAY_OF_WEEK) == GregorianCalendar.SATURDAY || gc.get(GregorianCalendar.DAY_OF_WEEK) == GregorianCalendar.SUNDAY) {
			gc.set(GregorianCalendar.DAY_OF_WEEK, GregorianCalendar.FRIDAY);
		}
		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
		return sd.format(gc.getTime());
	}

	/**
	 *
	 * @return date of today as SimpleDateFormat
	 
	private static String getTillLastDayOfWeek(int tillDays) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		gc.setFirstDayOfWeek(GregorianCalendar.TUESDAY);
		gc.add(GregorianCalendar.DAY_OF_MONTH, tillDays);
		if (gc.get(GregorianCalendar.DAY_OF_WEEK) == GregorianCalendar.SUNDAY || gc.get(GregorianCalendar.DAY_OF_WEEK) == GregorianCalendar.MONDAY) {
			gc.set(GregorianCalendar.DAY_OF_WEEK, GregorianCalendar.SATURDAY);
		}
		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
		return sd.format(gc.getTime());
	}*/

	public String getPrefixBySkin() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
	    //String protocol = getProtocolHttpOrHttps();
		String prefix = ApplicationData.getLinkLocalePrefix(request.getSession(), request, response);
		String subDomainFromSkin = ApplicationData.getSubDomainBySkin(prefix, CommonUtil.isUserSkinRegulated());
		return subDomainFromSkin;
	}

	public static String getLocaleBySubDomain(String subdomain, boolean isRegulated) {
		if (isRegulated) {
			return subDomainsLocalesListRegulation.get(subdomain);
		} else {
			return subDomainsLocalesList.get(subdomain);
		}
	}

	public static String getSubDomainBySkin(String skin, boolean isRegulated) {
		if (isRegulated) {
			return subDomainsListRegulation.get(skin);
		} else {
			return subDomainsList.get(skin);
		}

	}

	public int getOneClickDisappearTime() throws SQLException {
    	String enumerator = null;
		enumerator = CommonUtil.getEnum(Constants.ENUM_ONE_CLICK_DISAPPEAR,Constants.ENUM_ONE_CLICK_DISAPPEAR_CODE);

		return Integer.parseInt(enumerator) * 1000;
    }

//	public String getAndroidMarketUrlWithParameters() {
//		FacesContext context = FacesContext.getCurrentInstance();
//		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
//		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
//		String eq = "%3D";
//		String and ="%26";
//		String androidMarketUrl = Constants.ANYOPTION_PLAY_STORE_ANDROID_APP;
//		String dynamicParam = (String) session.getAttribute(Constants.DYNAMIC_PARAM);
//		String combid = (String) session.getAttribute(Constants.COMBINATION_ID);
//		String etsParam = MarketingTrackerUtil.getMarketingTrackerAppLinkParam(request);
//		//String affiliateKey = (String) session.getAttribute(Constants.SUB_AFF_KEY_PARAM);
//		if(!CommonUtil.IsParameterEmptyOrNull(dynamicParam) || !CommonUtil.IsParameterEmptyOrNull(combid) || !CommonUtil.IsParameterEmptyOrNull(etsParam)) {
//
//			androidMarketUrl = androidMarketUrl + "&referrer=";
//			if(!CommonUtil.IsParameterEmptyOrNull(combid)) {
//				androidMarketUrl += "combid" + eq + combid;
//			}
//
//			if(!CommonUtil.IsParameterEmptyOrNull(dynamicParam)) {
//				androidMarketUrl +=  and + "dp" + eq + dynamicParam;
//			}
//			
//			if(!CommonUtil.IsParameterEmptyOrNull(etsParam)) {
//			    androidMarketUrl +=  and + etsParam;
//			}
//		}
//		return androidMarketUrl;
//	}
//
//	public String getAppStoreUrlWithParameters() {
//		FacesContext context = FacesContext.getCurrentInstance();
//		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
//		String appStoreUrl = Constants.ANYOPTION_ITUNES_IPHONE_APP;
//		String dynamicParam = (String) session.getAttribute(Constants.DYNAMIC_PARAM);
//		String combid = (String) session.getAttribute(Constants.COMBINATION_ID);
//		//String affiliateKey = (String) session.getAttribute(Constants.SUB_AFF_KEY_PARAM);
//		if(!CommonUtil.IsParameterEmptyOrNull(dynamicParam)) {
//			appStoreUrl += "&dp=" + dynamicParam;
//		}
//		if(!CommonUtil.IsParameterEmptyOrNull(combid)) {
//			appStoreUrl += "&combid=" + combid;
//		}
//
//		return appStoreUrl;
//	}
	
	/**
	 * @param url
	 * @return url with parameters
	 */
	public String getAppUrlWithParameters(String url) {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		String queryString = (String) session.getAttribute(Constants.QUERY_STRING);
		if (queryString == null) {
			queryString = ConstantsBase.EMPTY_STRING;
		}
		return url + "?" + getTranslatedQueryByMarketingDictionary(ConstantsBase.MARKETING_COMPANY_APPSFLYER, queryString);      		
	}

	public static long getCountryId(){
		FacesContext context=FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		return user.getCountryId();
	}

	public String getPrintLogs() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
		logger.info("Notification from delta pay: " + request.getRequestURI());
		return null;
	}

	public String getInputFieldPattern() {
		String pattern;
		if (getCurrencyDecimalPointDigits()<1) {
			pattern = "\\d+";
		} else {
			pattern = "\\d+|\\d+\\.\\d{1," + getCurrencyDecimalPointDigits() + "}";
		}
    	return pattern;
    }

	/**
	 * Only for loggin users
	 * Display BaroPay payment method if user from Korean skin and his country is from south korea and his currency is KRW
	 * @return
	 */
	public boolean isShowBaroPayPaymentMethod() {
//		try {
//			User user = getUserFromSession();
//			if (user.getSkinId() == Skin.SKIN_KOREAN &&
//					user.getCountryId() == Constants.COUNTRY_ID_SOUTH_KOREA &&
//					user.getCurrencyId() == Constants.CURRENCY_KRW_ID) {
//				return true;
//			}
//			return false;
//		} catch (Exception e) {
//			logger.error("Error Show BaroPay Payment Method", e);
//		}
		return false;
	}
	
	public static HashSet<Long> getRegulatedCountriesList() {
		return regulatedCountriesList;
	}

	public static void setRegulatedCountriesList(HashSet<Long> regulatedCountriesList) {
		ApplicationData.regulatedCountriesList = regulatedCountriesList;
	}
	
	private static HashSet<Long> listOfRegulatedCountries() {
		HashSet<Long> set = new HashSet<Long>();
		for (int i = 0; i < ConstantsBase.COUNTRY_ID_REGULATED.length; i++) {
			set.add(new Long(ConstantsBase.COUNTRY_ID_REGULATED[i]));
		}
		return set;
	}
	
	public static HashSet<String> getRegulatedSkinsList() {
		return regulatedSkinsList;
	}

	public static void setRegulatedSkinsList(HashSet<String> regulatedSkinsList) {
		ApplicationData.regulatedSkinsList = regulatedSkinsList;
	}

//	public static long getRegulatedSkinIdByNonRegulatedSkinId(long skinId) {
//		if (skinId == Skin.SKIN_ENGLISH) {
//			return Skin.SKIN_REG_EN;
//		} else if (skinId == Skin.SKIN_SPAIN) {
//			return Skin.SKIN_REG_ES;
//		} else if (skinId == Skin.SKIN_GERMAN){
//			return Skin.SKIN_REG_DE;
//		} else if (skinId == Skin.SKIN_ITALIAN){
//			return Skin.SKIN_REG_IT;
//		} else if (skinId == Skin.SKIN_FRANCE){
//			return Skin.SKIN_REG_FR;
//		} else {
//			return skinId;
//		}
//	}
//	
//	public static long getNonRegulatedSkinIdByRegulatedSkinId(long skinId) {
//		if (skinId == Skin.SKIN_REG_EN) {
//			return Skin.SKIN_ENGLISH;
//		} else if (skinId == Skin.SKIN_REG_ES) {
//			return Skin.SKIN_SPAIN;
//		} else if (skinId == Skin.SKIN_REG_DE){
//			return Skin.SKIN_GERMAN;
//		} else if (skinId == Skin.SKIN_REG_IT){
//			return Skin.SKIN_ITALIAN;
//		} else if (skinId == Skin.SKIN_REG_FR){
//			return Skin.SKIN_FRANCE;
//		} else {
//			return skinId;
//		}
//	}

	/**
	 * @return the allowIPList
	 */
	public static HashSet<String> getAllowIPList() {
		if(allowIPList == null) {
			Connection con = null;
			try {
				con = getConnection();	
				allowIPList = GeneralDAO.getAllowIPList(con);
			}catch(SQLException ex) {
				logger.error("Can't lazzy load allowIPList", ex );
			} finally {
				closeConnection(con);
			}
		}
		return allowIPList;
	}
	
	
	/**
	 * get minimum first deposit by skin and currency.
	 * @return minimum first deposit
	 * @throws SQLException
	 */
	public String getMinFirstDeposit() {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = null;
		String remoteUser = context.getExternalContext().getRemoteUser();
		if (remoteUser != null && !remoteUser.equals("")){			
			user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		}
		Skins skin = getSkinById(getSkinId());
		long currencyId = skin.getDefaultCurrencyId();
		if (null != user && user.getId() != 0) {
			currencyId = user.getCurrencyId();
		}		
		long limitId = skin.getSkinCurrenciesHM().get(currencyId).getLimitId();
		return CommonUtil.displayAmount(limitHM.get(limitId).getMinFirstDeposit(), currencyId);
	}
	public String getMinFirstDepositText() {
		String s =  getMinFirstDeposit();
		return CommonUtil.getMessage("CMS.anyoption_agreement.text.text49", new Object[]{s} );
	}
	
	/**
	 * get limit investment by currency.
	 * @return Limit investment
	 */
	public String getLimitInvestment(long limitInvGroup, boolean showCurrencySign) {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = null;
		String remoteUser = context.getExternalContext().getRemoteUser();
		if (remoteUser != null && !remoteUser.equals("")){
			user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		}
		Skins skin = getSkinById(getSkinId());
		long currencyId = skin.getDefaultCurrencyId();
		if (null != user && user.getId() != 0) {
			currencyId = user.getCurrencyId();
		}
		HashMap<Long, Long> investmentLimitsGroupAmountPerCurrency = getInvLimitisGroupsMap().get(new Long(limitInvGroup)).getAmountPerCurrency();
		if (showCurrencySign) {
			return CommonUtil.displayAmount(investmentLimitsGroupAmountPerCurrency.get(currencyId), showCurrencySign, currencyId);
		} else {
			return CommonUtil.displayAmountForInput(investmentLimitsGroupAmountPerCurrency.get(currencyId), false, (int) currencyId);
		}
	}
	
	public String getTurnOverFactorPercent(String oppTypeId) throws SQLException {
		int id = 0;
		double factor = 0.0;
		try {
			id = Integer.parseInt(oppTypeId);
			factor = InvestmentsManager.getTurnoverFactor(id) * 100;
		} catch (NumberFormatException e) {
			logger.error("ERROR! getTurnOverFactorPercent.", e);
			return "";
		}				
		return String.valueOf(factor);
	}
	
	public String getTurnOverFactorRes(String oppTypeId) throws SQLException {
		int id = 0;
		double res = 0.0;
		try {
			id = Integer.parseInt(oppTypeId);
			res = InvestmentsManager.getTurnoverFactor(id) * ConstantsBase.BONUS_TERMS_EXAMPLE;
		} catch (Exception e) {
			logger.error("ERROR! getTurnOverFactorRes", e);
			return "";
		}		
		return String.valueOf(res);
	}
	
	public String getTurnOverTerms(String oppTypeId, String isPopup) throws Exception {
		boolean popup = Boolean.parseBoolean(isPopup);
		String[] params = new String[3];        
		params[0] = getTurnOverFactorPercent(oppTypeId);
		params[1] = ConstantsBase.BONUS_TERMS_EXAMPLE + getRealCurrencySymbol();
		params[2] = getTurnOverFactorRes(oppTypeId) + getRealCurrencySymbol();
		if (isCurrencyLeftSymbol()) {
			params[1] = getRealCurrencySymbol() + ConstantsBase.BONUS_TERMS_EXAMPLE;
			params[2] = getRealCurrencySymbol() + getTurnOverFactorRes(oppTypeId);
		}
		String termsKey = "CMS.general_terms_content.text.text65";
		long id = Long.parseLong(oppTypeId);
		if (id == Opportunity.TYPE_BINARY_0_100_ABOVE) {
			termsKey = "CMS.general_terms_content.text.text85";
			if (popup) {
				termsKey = "binary.0-100.general.terms.part.11.3.9";
			}
		} else if (id == Opportunity.TYPE_BUBBLES) {
			termsKey = "CMS.general_terms_content.text.11.4.3";
		}
		return CommonUtil.getMessage(termsKey, params);
	}

	/**
	 * @return the first country ID for the current skin.
	 */
	public static long getFirstSortedCountry() {
		return getSortedCountries().iterator().next().getId();
	}
	
	/**
	 * @return collection of values in the linked hash map
	 */
	public static Collection<CountryMiniBean> getSortedCountries() {
		LinkedHashMap<Long, CountryMiniBean> sortedCountriesMap = getSortedCountriesMap();
		return sortedCountriesMap.values();
	}

	/**
	 * @return the serialized to Json countries map
	 */
	public static String getCountryMiniMap() {
		LinkedHashMap<Long, CountryMiniBean> sortedCountriesMap = getSortedCountriesMap();
		Gson gson = new GsonBuilder().create();
		String sortedCountriesJsonMap = gson.toJson(sortedCountriesMap);
		return sortedCountriesJsonMap;
	}

	public static LinkedHashMap<Long, CountryMiniBean> getSortedCountriesMap() {
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		HttpSession session = (HttpSession) context.getSession(true);
        HttpServletRequest request = (HttpServletRequest) context.getRequest();
        boolean isOpenAccountPage = request.getRequestURL().toString().indexOf("register.jsf") > -1;
		long skinId = getSkinId(session, request);
		LinkedHashMap<Long, CountryMiniBean> sortedCountriesMap = skinSortedCountriesMap.get(skinId + "" + isOpenAccountPage);
		if (sortedCountriesMap == null || sortedCountriesMap.isEmpty()) {
			if (skinId != Skin.SKIN_ETRADER) {
				LinkedList<CountryMiniBean> unsortedCountryMiniList = new LinkedList<CountryMiniBean>();
				HttpServletResponse response = (HttpServletResponse) context.getResponse();				
				Locale locale;
				try {
					locale = getUserLocale(session, request, response);
				} catch (SQLException e) {
					logger.error("Could not get the user locale. Taking a default one", e);
					locale = new Locale(ConstantsBase.LOCALE_DEFAULT);
				}
				Collator collator = Collator.getInstance(locale);
				for (Long cId : countries.keySet()) {
					Country c = countries.get(cId);
					if ((c.getCountryStatus() == CountryStatus.NOTBLOCKED && c.getId() != Constants.COUNTRY_ID_IL)
							|| (!isOpenAccountPage && (c.getId() == Constants.COUNTRY_ID_IL 
														|| c.getCountryStatus() == CountryStatus.REJECTED))) { // remove Israel from anyoption
						String countryName = CommonUtil.getMessage(c.getDisplayName(), null, locale);
						String phoneCode = c.getPhoneCode();
						if (phoneCode.length() > CountryMiniBean.COUNTRY_PHONE_CODE_PREFIX_LENGTH && c.getId()!=CountryMiniBean.COUNTRY_ID_BARBADOS) {
							phoneCode = phoneCode.substring(0, 1);
						}
						CountryMiniBean cmb = new CountryMiniBean(c.getId(), countryName, phoneCode, c.getA2(), collator.getCollationKey(countryName));
						unsortedCountryMiniList.add(cmb);
					}
				}
				Collections.sort(unsortedCountryMiniList, new CountryMiniBeanComparator<CountryMiniBean>());
				sortedCountriesMap = new LinkedHashMap<Long, CountryMiniBean>(unsortedCountryMiniList.size());
				for (CountryMiniBean cmb : unsortedCountryMiniList) {
					sortedCountriesMap.put(cmb.getId(), cmb);
				}
			} else { // for etrader we have only country IL, but we have phone codes in the combo
				sortedCountriesMap = new LinkedHashMap<Long, CountryMiniBean>(getMobilePrefPlain().size());
				for (String mobilePref : getMobilePrefPlain()) {
					CountryMiniBean cmb = new CountryMiniBean(Long.valueOf(mobilePref), mobilePref, mobilePref, "", null);
					sortedCountriesMap.put(cmb.getId(), cmb);
				}
			}
			skinSortedCountriesMap.put(skinId + "" + isOpenAccountPage, sortedCountriesMap);
		}

		return sortedCountriesMap;
	}
	
	/**
	 * @return returns serialized to Json map of faces context error messages
	 */
	public static String getJsonQuickSignUpErrorMessages() {
		FacesContext context = FacesContext.getCurrentInstance();
		List<FacesMessage> errorMessages = context.getMessageList();		
		HashMap<String, String> errorsMap = new HashMap<String, String>(errorMessages.size());
		for (FacesMessage fm : errorMessages) {
			String field = fm.getDetail();
			String message = fm.getSummary();
			errorsMap.put(field, message);
		}

		Gson gson = new Gson();
		String errorsJsonMap = gson.toJson(errorsMap);
		return errorsJsonMap;
	}

	private static class CountryMiniBeanComparator<T> implements Comparator<CountryMiniBean> {

		@Override
		public int compare(CountryMiniBean o1, CountryMiniBean o2) {
			return o1.getDisplayNameCollationKey().compareTo(o2.getDisplayNameCollationKey());
		}
	}
	
	public String getBannerFile()  {
		String bannerFile = "";
		try {
			bannerFile = this.getUserLocale().getLanguage()+"_"+this.getSkinId()+"/"+CommonUtil.getProperty(Constants.BANNER_NAME);
		} catch (SQLException e) {
			logger.error("ERROR! getTemplate", e);
			return "";
		}
		File bannerOnFileSystem = new File(CommonUtil.getProperty(Constants.BANNER_PATH)+bannerFile);
		if (bannerOnFileSystem.exists()) {
			return this.getImagesPath()+"/"+bannerFile;
		} else {
			return "";
		}	
	}
	
	/**
	 * Return the relevant GUI  
	 * 1. JSP
	 * 2. API 
	 * @return
	 */
	public String getGUI() {
		String GUI = "jsp";
		if (isAPIGUI()) {
			GUI = "API";
		}
		return GUI;
	}
	
	/**
	 * Check if it's API GUI
	 * @return
	 */
	public boolean isAPIGUI() {
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
			String GUI = (String) session.getAttribute(Constants.GUI);
			if (!CommonUtil.isParameterEmptyOrNull(GUI) && GUI.equals("API") ) {
				return true;
			}
		} catch (Exception e) {
			logger.warn("Error while trying to check if it's API GUI", e);
		}
		return false;
	}

	/**
	 * @return the aPIPagesList
	 */
	public static Hashtable<Long, ApiPage> getAPIPagesList() {
		if(APIPagesList == null) {
			Connection con = null;
			try {
				con = getConnection();	
				APIPagesList = ApiDAOBase.getApiPages(con);
			}catch(SQLException ex) {
				logger.error("Can't lazzy load APIPagesList", ex );
			} finally {
				closeConnection(con);
			}
		}
		return APIPagesList;
	}
	
	/**
	 * clear last top trades
	 */
	public static void clearLastTopTrades() {
		lastTopTrades = null;
	}
	
    public String getIsraelMarkets() {
        return israelMarkets;
    }

    public static ArrayList<String> getMinisitePages() {
		if(minisitePages == null) {
			Connection con = null;
			try {
				con = getConnection();	
				minisitePages = PagesConfigDAO.getMinisitePages(con);
			}catch(SQLException ex) {
				logger.error("Can't lazzy load minisitePages", ex );
			} finally {
				closeConnection(con);
			}
		}
        return minisitePages;
    }

    public static long getLogoutDefaultAmountValue() {
    	FacesContext context = FacesContext.getCurrentInstance();
    	ApplicationData appData = context.getApplication().evaluateExpressionGet(context, Constants.BIND_APPLICATION_DATA, ApplicationData.class);
    	BalanceStep bs;
		try {
			bs = UserBalanceStepManager.getLogoutCurrencyDefaultBalanceStep().get(new Long(appData.getSkin().getDefaultCurrencyId()));		
			return bs.getDefaultAmountValue();
		} catch (SQLException e) {
			logger.error("Can't get balance steps", e);
			return 0l;
		}  catch (Exception e) {
			logger.error("Can't get balance steps. Check balance steps to this currency: " + appData.getSkin().getDefaultCurrencyId() , e);
			return 0l;
		}
    }

    public static List<BalanceStepPredefValue> getLogoutDefaultPredefValues() {
    	FacesContext context = FacesContext.getCurrentInstance();
    	ApplicationData appData = context.getApplication().evaluateExpressionGet(context, Constants.BIND_APPLICATION_DATA, ApplicationData.class);
    	BalanceStep bs;
		try {
			bs = UserBalanceStepManager.getLogoutCurrencyDefaultBalanceStep().get(new Long(appData.getSkin().getDefaultCurrencyId()));		
			return bs.getPredefValues();
		} catch (SQLException e) {
			logger.error("Can't get balance steps", e);
			return null;
		} catch (Exception e) {
			logger.error("Can't get balance steps. Check balance steps to this currency: " + appData.getSkin().getDefaultCurrencyId() , e);
			return null;
		}
    }
    
    public boolean isLastLoginIsBeforeDeployDate(){
		
    	FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		User user = (User)request.getSession().getAttribute(Constants.BIND_SESSION_USER);
		Date lastLoginDate = user.getTimeCreated();
		//
		Date deployDate = new Date();
		Date afterThreeMonths = new Date();
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    try {
			 deployDate = dfm.parse("2014-04-27 00:00:00");
			 afterThreeMonths = dfm.parse("2014-07-27 08:00:00");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		Date dateNow = new Date();
		
		if((lastLoginDate.before(deployDate))&&(dateNow.before(afterThreeMonths))){
			return true;
		}
		
		return false;
    	
     }
    
    public static void reset(){
    	GeneralManager.setAllowedIP(null);
    	APIPagesList			= null;
		banks					= null;
		banksSI					= null;
		bankBranches			= null;
		cities					= null;
		cachePages				= null;
		clearingErrorCode		= null;
    	directPaymentMapping	= null;
    	invLimitisGroupsMap		= null;
    	marketsClosingLevels	= null;
    	minisitePages			= null;
		markets					= null;
		opportunityProductTypes	= null;
		popEntryHisStatusHash	= null;
		populationEntryType		= null;
		states					= null;
		statesCode				= null;
		tierPointsConvertion	= null;
		transStatuses			= null;		
    }
    
    public String getTranslatedQueryByMarketingDictionary(long companyId, String queryString) {
    	HashMap<String, String> translateParamsHM = MarketingDictionaryParamsHM.get(companyId);
        String[] params = queryString.split("&");
        String result = "";
        for (String param : params) {
            String[] pair = param.split("=");
            String translatedParam = translateParamsHM.get(pair[0]);
            if (translatedParam != null) {
                  result += translatedParam + "=" + pair[1] + "&";
            } else {
                  result += param + "&";
            }
        }
        result = result.substring(0, result.length() - 1);
        logger.log(Level.INFO, result);
        return result;
    }
    
    public static boolean getShowGoogleTradeBox() {
    	//always return true no mather the circumstances
    	return true;
//    	FacesContext context = FacesContext.getCurrentInstance();
//    	HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
//    	HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
//    	String parameter = (String) session.getAttribute(Constants.TRADE_BOX_GOOGLE_PATTERN);
//    	if(parameter != null && parameter != "" && parameter.equals(Constants.TRADE_BOX_GOOGLE_PATTERN)) {
//    		return true;
//    	}
//    	if(request.getQueryString() != null && request.getQueryString() != "" && request.getQueryString().indexOf(CommonUtil.getProperty(Constants.TRADE_BOX_GOOGLE_PATTERN)) > -1) { 
//    		session.setAttribute(Constants.TRADE_BOX_GOOGLE_PATTERN, Constants.TRADE_BOX_GOOGLE_PATTERN);
//    		return true;
//    		}
//    	return false;
  
    }      

    public static String getMinInvest() {
		FacesContext context = FacesContext.getCurrentInstance();
		User loggedUser = context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER, User.class);

		if (loggedUser!=null && loggedUser.getId()!=0) {
			return CommonUtil.displayAmount(loggedUser.getMinInvest(), loggedUser.getCurrencyId());
		} else {
			String savedMinInvest = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("minInvest");
			if(savedMinInvest == null ) {
		        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
		        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
				long skinId = getSkinId(session, request);
				long currencyId = getSkinById(skinId).getDefaultCurrencyId();
				InvestmentLimit limit = InvestmentsManagerBase.getUserLimit(0, currencyId, Opportunity.TYPE_REGULAR);
				if(limit != null) {
					savedMinInvest = CommonUtil.displayAmount(limit.getMinAmount(), currencyId);
				} else {
					logger.error("Can't load default investment limit !");
					savedMinInvest = "25";
				}
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("minInvest", savedMinInvest);
			}
			return savedMinInvest;
		}
    }
    
    public static String getFtd() {
		FacesContext context = FacesContext.getCurrentInstance();
		User loggedUser = context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER, User.class);

		if (loggedUser!=null && loggedUser.getId()!=0) {
			return CommonUtil.displayAmount(loggedUser.getFtd(), loggedUser.getCurrencyId());
		} else {
			String savedFtd = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("ftd");
			if(savedFtd == null ) {
		        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
		        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
				long skinId = getSkinId(session, request);
				long currencyId = getSkinById(skinId).getDefaultCurrencyId();
				LimitationDeposits ld = LimitationDepositsManagerBase.getLimit(skinId, currencyId);
				if(ld != null) {
					savedFtd = CommonUtil.displayAmount(ld.getMinimumFirstDeposit(), currencyId);
				} else {
					logger.error("Can't load default FTD !");
					savedFtd = "100";
				}
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("ftd", savedFtd);
			}
			return savedFtd;
		}
    }

	public static String getLoginProduct() {
		FacesContext context = FacesContext.getCurrentInstance();
		ConfigurableNavigationHandler navHandler = (javax.faces.application.ConfigurableNavigationHandler) context	.getApplication()
																													.getNavigationHandler();
		String loginProduct = navHandler.getNavigationCase(context, "/jsp/*", Constants.NAV_TRADE_PAGE).getToViewId(context)
										.replace(".xhtml", ".jsf");
		if (context.getExternalContext().getRemoteUser() != null) {
			User user = context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER, User.class);
			if (user.getLoginProduct() != null) { // may not be set due alp cookie
				loginProduct = user.getLoginProduct();
			}
		}
		return loginProduct;
	}
	
 }