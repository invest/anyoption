package il.co.etrader.web.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.bl_vos.Ad;
import com.anyoption.common.daos.GeneralDAO;

import il.co.etrader.bl_managers.AdminManagerBase;
import il.co.etrader.bl_vos.MarketingCombination;
import il.co.etrader.dao_managers.AdsDAO;
import il.co.etrader.dao_managers.ClicksDAOBase;
import il.co.etrader.dao_managers.MarketingCombinationsDAOBase;

public class AdminManager extends AdminManagerBase {

	public AdminManager() throws Exception {
	}

	public static Ad getAd(long adId) throws SQLException {
		Connection con = getConnection();
		try {
			return AdsDAO.getById(con,adId);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Get marketing combination by id
	 * @param combId
	 * @return
	 * @throws SQLException
	 */
	public static MarketingCombination getCombinationId(long combId) throws SQLException {
		Connection con = getConnection();
		try {
			return MarketingCombinationsDAOBase.getById(con, combId);
		} finally {
			closeConnection(con);
		}
	}

	public static long insertClick(long combId,String ip,String dynamicParam, Long affId, Long subAffId,
			String httpReferer, String dfaPlacementId, String dfaCreativeId, String dfaMacro) throws SQLException {
		Connection con = getConnection();
		try {

			return ClicksDAOBase.insertClick(con, combId, ip, dynamicParam, affId, subAffId, httpReferer, dfaPlacementId, dfaCreativeId, dfaMacro);

		} finally {
			closeConnection(con);
		}
	}


	public static void runTestQuery() throws SQLException {

		Connection con = getConnection();

		try {

			GeneralDAO.getTestQuery(con);

		} finally {
			closeConnection(con);
		}
	}

	public String getTestQuery() throws SQLException {

		Connection con = getConnection();

		try {

			GeneralDAO.getTestQuery(con);

		} finally {
			closeConnection(con);
		}
		return null;
	}

	public static long getEntriesCounter() throws SQLException{
		Connection con = getConnection();
		long entries = 0;
		try {

			entries = GeneralDAO.getSequenceNextVal(con,"SEQ_ENTRIES_AO");

		} finally {
			closeConnection(con);
		}
		return entries;
	}
}