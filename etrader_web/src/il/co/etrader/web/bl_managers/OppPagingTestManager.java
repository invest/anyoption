package il.co.etrader.web.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.util.DataPage;
import il.co.etrader.web.dao_managers.OppPagingTestDAO;

public class OppPagingTestManager extends BaseBLManager {
    public static DataPage<Opportunity> getOpportunities(int startRow, int pageSize) throws SQLException {
        DataPage<Opportunity> dataPage = null;
        Connection conn = null;
        try {
            conn = getConnection();
            int count = OppPagingTestDAO.getOpportunitiesCount(conn);
            ArrayList<Opportunity> l = OppPagingTestDAO.getOpportunities(conn, startRow, pageSize);
            dataPage = new DataPage<Opportunity>(count, startRow, l);
        } finally {
            closeConnection(conn);
        }
        return dataPage;
    }
}
