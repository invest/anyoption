package il.co.etrader.web.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.web.dao_managers.XMLAccountDAO;

public class XMLAccountManager extends BaseBLManager {
    public static boolean isLogin(String id, String password) throws SQLException {
        boolean login = false;
        Connection conn = null;
        try {
            conn = getConnection();
            login = XMLAccountDAO.isLogin(conn, id, password);
        } finally {
            closeConnection(conn);
        }
        return login;
    }
}