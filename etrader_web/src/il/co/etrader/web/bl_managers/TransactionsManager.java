package il.co.etrader.web.bl_managers;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.daos.QuestionnaireDAOBase;

import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.RiskAlertsManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.CreditCardsDAO;
import il.co.etrader.dao_managers.TransactionsDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.dao_managers.TransactionsDAO;
import il.co.etrader.web.dao_managers.UsersDAO;
import il.co.etrader.web.util.Constants;

public class TransactionsManager extends TransactionsManagerBase {

	private static final Logger log = Logger.getLogger(TransactionsManager.class);

	public static void deleteCard(long id) throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			log.log(Level.INFO, " deleting cards :");
		}

		FacesContext context = FacesContext.getCurrentInstance();

		Connection con = getConnection();

		try {
			con.setAutoCommit(false);

			if (log.isEnabledFor(Level.INFO)) {
				log.log(Level.INFO, " about to delete card: " + id);
			}

			CreditCardsDAO.updateCardNotVisible(con, id);

			if (log.isEnabledFor(Level.INFO)) {
				log.log(Level.INFO, " card deleted sucessfuly!");
			}

			con.commit();

		} catch (SQLException e) {
			log.log(Level.ERROR, "deleteCard:", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("creditcards.delete.success", null),null);
		context.addMessage(null, fm);


	}

	public static boolean reverseWithdraw(ArrayList tranList, User user, long loginId) throws Exception  {

		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();
		Transaction tran = null;

		try {
			UsersDAO.getByUserName(con, user.getUserName(), user);
			con.setAutoCommit(false);
			int count = 0;
			int totalAmount = 0;
			Iterator iter = tranList.iterator();
			while(iter.hasNext()) {
				tran = (Transaction) iter.next();

				if (tran.isSelected()) {

					if (log.isEnabledFor(Level.INFO)) {
						String ls = System.getProperty("line.separator");
						log.log(Level.INFO, "Insert ReverseWithdraw: " + ls + tran.toString() + ls);
					}

					if (TransactionsManagerBase.isStillPendingWithdraw(tran.getId())){

						UsersDAOBase.addToBalance(con, user.getId(), tran.getAmount());
						GeneralDAO.insertBalanceLog(con, CommonUtil.getWebWriterId(), tran.getUserId(), Constants.TABLE_TRANSACTIONS, tran.getId(), Constants.LOG_BALANCE_REVERSE_WITHDRAW, user.getUtcOffset());

						long newBalance = user.getBalance() + tran.getAmount();

						tran.setTimeSettled(new Date());
						tran.setProcessedWriterId(CommonUtil.getWebWriterId());
						tran.setStatusId(TRANS_STATUS_REVERSE_WITHDRAW);
						tran.setWriterId(CommonUtil.getWebWriterId());

						TransactionsDAOBase.updateReverseWithdraw(con, tran);
						
						tran.setReferenceId(new BigDecimal(tran.getId()));
						tran.setIp(CommonUtil.getIPAddress());
						tran.setTypeId(TRANS_TYPE_REVERSE_WITHDRAW);
						tran.setStatusId(TRANS_STATUS_SUCCEED);
						tran.setTimeSettled(new Date());
						tran.setUtcOffsetSettled(ApplicationData.getUserFromSession().getUtcOffset());
						tran.setLoginId(loginId);

						TransactionsDAOBase.insert(con, tran);

						if (log.isEnabledFor(Level.INFO)) {
							String ls = System.getProperty("line.separator");
							log.log(Level.INFO, "ReverseWithdraw insert successfully! " + ls + tran.toString());
						}

						user.setBalance(newBalance);
						totalAmount += tran.getAmount();
					}
					count++;
				}

			}

			if (count == 0) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.transaction.nonselected", null),null);
				context.addMessage(null, fm);

				return false;
			}
			con.commit();
			
			if (tranList.size() == count) {
				PopulationEntryBase pe = PopulationsManagerBase.getPopulationUserByUserId(user.getId());
				if(pe != null && pe.getCurrPopualtionTypeId() == PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW &&
						pe.getEntryTypeId() != PopulationsManagerBase.ENTRY_TYPE_REMOVE_PERMANENTLY) {
					PopulationsManagerBase.insertIntoPopulation(0, 0, user.getId(), 
							pe.getCurrPopulationName(), pe.getAssignWriterId(), PopulationsManagerBase.POP_TYPE_RETENTION, pe);
				}
			}
			
			ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
			ve.getValue(context.getELContext());
			ve.setValue(context.getELContext(), user);
//			context.getApplication().createValueBinding(Constants.BIND_USER).setValue(context, user);
			String[] params = new String[1];
			if (user.getSkinId() != Skin.SKIN_ARABIC){
	        	params[0] = CommonUtil.displayAmount(totalAmount, user.getCurrencyId());
			}else{
				params[0] = CommonUtil.displayAmountForInput(totalAmount, false, user.getCurrencyId().intValue()) + " " + CommonUtil.getMessage(user.getCurrency().getSymbol(),null) + " ";
			}

			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("reverseWithdraw.success", params),null);
			context.addMessage(null, fm);

			return true;

		} catch (SQLException e) {
			log.log(Level.ERROR, "Error on reverseWithdraw:", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

	}

	public static ArrayList getVisibleCreditCardsByUser(long id) throws SQLException {
		Connection con = getConnection();
		try {
			return CreditCardsDAO.getVisibleByUserId(con, id);
		} finally {
			closeConnection(con);
		}
	}

	public static void updateCardDetails(long id, String month, String year, String ccPass, String utcoffset) throws Exception {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, " updating credit card details :" + ls + "id: " + id + ls + "month: " + month + ls + "year: " + year + ls + "ccPass: " + "*****" + ls);
		}

		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();
		try {
			CreditCardsDAO.updateWebDetails(con, id, month, year, ccPass, utcoffset);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("card.update.success", null),null);
			context.addMessage(null, fm);

			if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO, " update finished successfully! :" + ls);
			}
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList<Transaction> getTransactionsByUserAndDates(long id, Date from, Date to, String utcOffset) throws SQLException {
		Connection con = getConnection();
		try {
			String status = TRANS_STATUS_SUCCEED + "," + TRANS_STATUS_PENDING + "," + TRANS_STATUS_REQUESTED + "," + TRANS_STATUS_APPROVED + ","
				+ TRANS_STATUS_REVERSE_WITHDRAW + "," + TRANS_STATUS_CANCELED_ETRADER + "," + TRANS_STATUS_IN_PROGRESS + "," +
					TRANS_STATUS_CANCELED_FRAUDS + "," + TRANS_STATUS_CANCEL_S_DEPOSIT + "," + TRANS_STATUS_CHARGE_BACK + "," +
					TRANS_STATUS_CANCEL_S_M + "," + TRANS_STATUS_CANCEL_M_N_R + "," + TRANS_STATUS_SECOND_APPROVAL;
			return TransactionsDAO.getByUserAndDates(con, id, from, to, status, TRANS_TYPE_INTERNAL_CREDIT, utcOffset);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Get is first time deposit to know if we need to run first deposit pixel
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static boolean isFirstDeposit(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAO.isFirstDeposit(con, userId);
		} finally {
			closeConnection(con);
		}
	}

	public static void updateUserByName(String name, User user) throws SQLException {

		Connection con = getConnection();
		try {
			UsersDAO.getByUserName(con, user.getUserName(), user);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Check if the transactions is Inatec online deposit and we didn't get notification from Inatec
	 * @param id transaction id
	 * @return
	 * @throws SQLException
	 */
	public static boolean isInatecNotNotifiedTrx(long id) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return TransactionsDAO.isInatecNotNotifiedTrx(con, id);
		} finally {
			closeConnection(con);
		}
	}

	public static long getDeltaPayWithdrawalAmount(long userId, boolean countBeforeCup) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return TransactionsDAO.getDeltaPayWithdrawalAmount(con, userId);
		} finally {
			closeConnection(con);
		}
	}

	public static long getCUPWithdrawalAmount(long userId, boolean countBeforeCUP) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return TransactionsDAO.getCUPWithdrawalAmount(con, userId, countBeforeCUP);
		} finally {
			closeConnection(con);
		}
	}



	 public static Transaction getLastDeltaPayDeposit(long userId) throws SQLException {
	    Connection con = getConnection();
		try {
			return TransactionsDAO.getLastDeltaPayDeposit(con, userId, TransactionsManager.TRANS_TYPE_CUP_CHINA_DEPOSIT);
		} finally {
			closeConnection(con);
		}
	 }
	 
	 public static void stopDinersWithdraw(Long cardId,Long userId) throws SQLException{
		 
		 Connection con = getConnection();
		 FacesContext context = FacesContext.getCurrentInstance();
		 CreditCardsDAO.updateVisibleByUserId(con,userId,cardId);
     	
     	if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO, "Diners club credit card. CardId: " + cardId + " UserId: "+ userId + ls);
			}
     	FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.diners.notsupported.withdraw", null), null);
			context.addMessage(null, fm);
		 
	 }

	public static ArrayList<Transaction> getDepositListForCredit(long user_id, long provider_id) throws SQLException {
			Connection con = getConnection();
			try{
				return TransactionsDAOBase.getDepositListForCredit(con, user_id,  provider_id);
			} finally {
				closeConnection(con);
			}
	}
	
	public static long getWithdrawalAmountForProvider(long user_id, long provider_id) throws SQLException {
		 Connection con = getConnection();
			try {
				return TransactionsDAO.getWithdrawalAmountForProvider(con, user_id, provider_id);
			} finally {
				closeConnection(con);
			}
	}
	
	
	 public static void insertCUPWithdraw(String amount, String error, UserBase user, long writerId, boolean feeCancel, long loginId, QuestionnaireUserAnswer questUserAnswer) throws SQLException {

			if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO,
							"insert CUP Withdraw " + ls +
							"User:" + user.getUserName() + ls +
							"amount:" + amount);
			}

			FacesContext context = FacesContext.getCurrentInstance();
			Connection con = getConnection();

			try {
				UsersDAOBase.getByUserName(con, user.getUserName(), user);
				long newBalance = user.getBalance() - CommonUtil.calcAmount(amount);

				Transaction tran = new Transaction();
				tran.setAmount(CommonUtil.calcAmount(amount));
				tran.setComments(null);
				tran.setCreditCardId(null);
				tran.setIp(CommonUtil.getIPAddress());
				tran.setProcessedWriterId(writerId);
				tran.setCreditWithdrawal(false);
				tran.setTimeCreated(new Date());
				tran.setUtcOffsetCreated(user.getUtcOffset());
	            tran.setTypeId(TRANS_TYPE_CUP_CHINA_WITHDRAW);
				tran.setUserId(user.getId());
				tran.setWriterId(writerId);
				tran.setChargeBackId(null);
				tran.setReceiptNum(null);
				tran.setChequeId(null);
				tran.setFeeCancel(feeCancel);
				tran.setSelectorId(0);
				tran.setLoginId(loginId);
				
				if (error == null) { // no error
					try {
						con.setAutoCommit(false);
						tran.setDescription(null);
						tran.setStatusId(TRANS_STATUS_REQUESTED);
						tran.setTimeSettled(null);
						UsersDAOBase.addToBalance(con, user.getId(), -CommonUtil.calcAmount(amount));
						user.setBalance(newBalance);
						TransactionsDAOBase.insert(con, tran);
						GeneralDAO.insertBalanceLog(con,writerId,tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_CUP_WITHDRAW, user.getUtcOffset());
						if (questUserAnswer != null) {
							questUserAnswer.setTransactionId(tran.getId());
							QuestionnaireDAOBase.insertUserAnswer(con, questUserAnswer, true);
						}
						con.commit();
						PopulationEntryBase popUser = PopulationsManagerBase.getPopulationUserByUserId(user.getId());
						if (popUser.getEntryTypeId() != PopulationsManagerBase.ENTRY_TYPE_REMOVE_PERMANENTLY) {
							PopulationsManagerBase.insertIntoPopulation(con, PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW, 0, 0, user.getId(), writerId, popUser);
						}
						log.info("insert CUP Withdraw finished successfully!");
					} catch (SQLException e) {
						log.error("ERROR during  CUP Withdraw! ", e);
						try {
							    con.rollback();
							} catch (SQLException ie) {
								log.log(Level.ERROR, "Can't rollback.", ie);
							}
							throw e;
					} finally {
						try {
							con.setAutoCommit(true);
						} catch (Exception e) {
							log.error("Cannot set back to autoCommit! " + e);
						}
					}
				} else {  // error
					tran.setWireId(null);
					tran.setDescription(error);
					tran.setStatusId(TRANS_STATUS_FAILED);
					tran.setTimeSettled(new Date());
					tran.setUtcOffsetSettled(user.getUtcOffset());
					TransactionsDAOBase.insert(con, tran);
					log.info("Insert CUP Withdraw finished with error!");
				}
				try {
					RiskAlertsManagerBase.riskAlertWithdrawalHandler(writerId,error, tran.getId(),tran.getAmount(),tran.getUserId(),user.getBalance());
				} catch (Exception e) {
					log.error("Problem with risk Alert handler ", e);
				}
			} catch (SQLException e) {
				log.error("ERROR during CUP Withdraw!! ", e);
			} finally {
				closeConnection(con);
			}
			
	        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_USER, UserBase.class);
	        ve.getValue(context.getELContext());
	        ve.setValue(context.getELContext(), user);
		}
	 
		/**
		 * Check if the transactions is Inatec online deposit and we didn't get notification from Inatec
		 * @param id transaction id
		 * @return
		 * @throws SQLException
		 */
		public static boolean isInatecIframeNotNotifiedTrx(long id) throws SQLException {
			Connection con = null;
			try {
				con = getConnection();
				return TransactionsDAO.isInatecIframeNotNotifiedTrx(con, id);
			} finally {
				closeConnection(con);
			}
		}
}