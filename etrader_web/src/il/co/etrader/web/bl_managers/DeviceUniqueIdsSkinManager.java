package il.co.etrader.web.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.bl_vos.DeviceUniqueIdSkin;
import il.co.etrader.web.dao_managers.DeviceUniqueIdsSkinDAO;

public class DeviceUniqueIdsSkinManager extends BaseBLManager {

	public DeviceUniqueIdsSkinManager() throws Exception {
	}

	public static void update(String duid, long skinId) throws SQLException {
		Connection conn = getConnection();
		try {
			DeviceUniqueIdsSkinDAO.update(conn, duid, skinId);
		} finally {
			closeConnection(conn);
		}
	}

	public static void insert(String duid, long skinId, String userAgent, String ip, long countryId, long combId, String appVer) throws SQLException {
		Connection conn = getConnection();
		try {
			DeviceUniqueIdsSkinDAO.insert(conn, duid, skinId, userAgent, ip, countryId, combId, appVer);
		} finally {
			closeConnection(conn);
		}
	}

	public static DeviceUniqueIdSkin getSkinId(String duid) throws SQLException {
		Connection conn = getConnection();
		try {
			return DeviceUniqueIdsSkinDAO.getSkinId(conn, duid);
		} finally {
			closeConnection(conn);
		}
	}

	public static void updateCombinationId(String duid, long combId) throws SQLException {
		Connection conn = getConnection();
		try {
			DeviceUniqueIdsSkinDAO.updateCombinationId(conn, duid, combId);
		} finally {
			closeConnection(conn);
		}
	}

	public static void updateVersion(String duid, String appVer) throws SQLException {
		Connection conn = getConnection();
		try {
			DeviceUniqueIdsSkinDAO.updateAppVer(conn, duid, appVer);
		} finally {
			closeConnection(conn);
		}
	}
}