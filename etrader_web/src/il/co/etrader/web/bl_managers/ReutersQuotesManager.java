package il.co.etrader.web.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import il.co.etrader.bl_managers.ReutersQuotesManagerBase;
import il.co.etrader.web.bl_vos.ReutersQuotesPopup;
import il.co.etrader.web.dao_managers.ReutersQuotesDAO;


public class ReutersQuotesManager extends ReutersQuotesManagerBase {
	private static final Logger log = Logger.getLogger(ReutersQuotesManager.class);

	public static ReutersQuotesPopup getQuotesByOppId(long oppId) throws SQLException {
		Connection conn = getConnection();
		ReutersQuotesPopup rqp = null;
		try {
			rqp = ReutersQuotesDAO.getQuotesByOppId(conn, oppId);
		} finally {
			closeConnection(conn);
		}
		return rqp;
	}
}