package il.co.etrader.web.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;

import il.co.etrader.web.dao_managers.LoginsDAO;

public class LoginsManager extends com.anyoption.common.managers.LoginsManager {

	public static boolean logOut(long loginId) throws SQLException {
		Connection conn = getConnection();
		boolean res;
		try {
			res = LoginsDAO.logOut(conn, loginId);
		} finally {
			closeConnection(conn);
		}
		return res;
	}
}