package il.co.etrader.web.bl_managers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.InvestmentRejects;
import com.anyoption.common.daos.InvestmentRejectsDAOBase;


/**
 * Investment Rejects web manager.
 *
 * @author Eyal
 */
public class InvestmentRejectsManager extends com.anyoption.common.managers.InvestmentRejectsManagerBase {
    protected static Logger log = Logger.getLogger(InvestmentRejectsManager.class);

	public static void insertInvestmentReject100(InvestmentRejects inv) {
        Connection conn = null;
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            String AdditionalInfo = inv.getRejectAdditionalInfo();
            long amount = inv.getAmount();
            float amountForClose = inv.getReturnInv();
            if (inv.getNeedToOpen() > 0) {
            	inv.setRejectAdditionalInfo(inv.getRejectAdditionalInfo() + ", contracts: " + inv.getNeedToOpen());
            	inv.setAmount(amount * inv.getNeedToOpen());
            	inv.setReturnInv(getWinOdds0100(amountForClose, inv.getNeedToOpen()));
            	inv.setRejectedDuringClose(false);
            	InvestmentRejectsDAOBase.insertInvestmentRejects(conn, inv);
            }
            if (inv.getNeedToClose() > 0) {
            	inv.setRejectAdditionalInfo(AdditionalInfo +  ", contracts: " + inv.getNeedToClose());
            	inv.setAmount(amount * inv.getNeedToClose());
            	inv.setReturnInv(getWinOdds0100(amountForClose, inv.getNeedToClose()));
            	inv.setRejectedDuringClose(true);
            	InvestmentRejectsDAOBase.insertInvestmentRejects(conn, inv);
            }
            conn.commit();
        } catch (Throwable t) {
            try {
                conn.rollback();
            } catch (Throwable it) {
                log.error("Can't rollback.", it);
            }
            log.debug("cant insert inv reject", t);
        } finally {
            try {
                conn.setAutoCommit(true);
            } catch (Throwable t) {
                log.error("Can't set back to autocommit.", t);
            }
            closeConnection(conn);
        }
	}
	
	public static float getWinOdds0100(double amount, int count) {
		BigDecimal profit = new BigDecimal(String.valueOf(amount)).multiply(new BigDecimal(String.valueOf(count)));
		BigDecimal maxReturn =  new BigDecimal("100").multiply(new BigDecimal(String.valueOf(count)));
		if (maxReturn.subtract(profit).doubleValue() == 0) {
			return 0;
		}
		BigDecimal oddsWin = new BigDecimal(String.valueOf(maxReturn.doubleValue() / (maxReturn.subtract(profit).doubleValue())));
		return oddsWin.setScale(2, RoundingMode.HALF_UP).floatValue();
	}
}