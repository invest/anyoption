package il.co.etrader.web.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.MarketsManagerBase;

import il.co.etrader.web.bl_vos.ProfitLineAssetSelectionBean;
import il.co.etrader.web.dao_managers.InvestmentsDAO;
import il.co.etrader.web.dao_managers.MarketsDAO;

public class MarketsManager extends il.co.etrader.bl_managers.MarketsManager {

	private static final Logger log = Logger.getLogger(MarketsManager.class);
	
	private static LocalDate dateMarketsDecimalPoint;
	
	private static Map<Long, String> marketsDecimalPoint;
	
    public static ArrayList<ProfitLineAssetSelectionBean> getProfitLineAssetSelection(long skinId, long opportunityTypeId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return MarketsDAO.getProfitLineAssetSelection(conn, skinId, opportunityTypeId);
        } finally {
            closeConnection(conn);
        }
    }

    public static void updateMarketNameBySkin(int skinId, HashMap<Long, String> skinMarketsList, String languageCode) throws SQLException {
    	Connection conn = null;
        try {
            conn = getConnection();
            String name;
        	String shortName;
        	Locale locale = null;
        	if (null != languageCode) {
        		locale = new Locale(languageCode);
        	}
            for (Long marketId : skinMarketsList.keySet()) {
	        	name = MarketsManagerBase.getMarketName(skinId, marketId);
        		shortName = MarketsManagerBase.getMarketShortName(skinId, marketId);
        		MarketsDAO.updateMarketNameBySkin(conn, skinId, marketId, name, shortName);
			}
        } finally {
            closeConnection(conn);
        }
    }

	public static Map<Long, String> getMarketsDecimalPoint() throws SQLException {
		Connection con = null;
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == dateMarketsDecimalPoint || currentDate.isAfter(dateMarketsDecimalPoint) || null == marketsDecimalPoint) {
			try {
				con = getConnection();
				if (null == marketsDecimalPoint) {
					marketsDecimalPoint = new HashMap<Long, String> ();
				}
				marketsDecimalPoint = InvestmentsDAO.getMarketsDecimalPoint(con);
			} catch (SQLException sqle) {
				log.error("Cannot load markets decimal point", sqle);
				throw sqle;
			} finally {
				closeConnection(con);
			}
		}
		return marketsDecimalPoint;
	}
}