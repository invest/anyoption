package il.co.etrader.web.bl_managers;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.InvestmentRejects;
import com.anyoption.common.beans.MarketNameBySkin;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunityType;
import com.anyoption.common.beans.TierUser;
import com.anyoption.common.beans.TreeItem;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.InvestmentType;
import com.anyoption.common.bonus.BonusHandlerBase;
import com.anyoption.common.bonus.BonusHandlerFactory;
import com.anyoption.common.bonus.BonusHandlersException;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.anyoption.common.jms.LevelsCache;
import com.anyoption.common.jms.WebLevelLookupBean;
import com.anyoption.common.managers.InvestmentRejectsManagerBase;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.managers.RewardUserTasksManager;
import com.anyoption.common.managers.RewardUserTasksManager.TaskGroupType;
import com.anyoption.common.util.GoldenMinute.GoldenMinuteType;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_managers.TiersManagerBase;
import il.co.etrader.bl_vos.InvestmentLimit;
import il.co.etrader.bl_vos.TierUserHistory;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.BonusDAOBase;
import il.co.etrader.dao_managers.InvestmentsDAOBase;
import il.co.etrader.dao_managers.OpportunitiesDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_vos.GameRank;
import il.co.etrader.web.bl_vos.LandingMarketDetails;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.dao_managers.InvestmentsDAO;
import il.co.etrader.web.dao_managers.OpportunitiesDAO;
import il.co.etrader.web.dao_managers.TiersDAO;
import il.co.etrader.web.dao_managers.UsersDAO;
import il.co.etrader.web.helper.CloseContractsSums;
import il.co.etrader.web.helper.SlipEntry;
import il.co.etrader.web.mbeans.SlipFormOneTouch;
import il.co.etrader.web.service.result.OpenPositionsMethodResult;
import il.co.etrader.web.util.Constants;

/**
 * web Investments page manager.
 */
public class InvestmentsManager extends InvestmentsManagerBase {
	private static final Logger log = Logger.getLogger(InvestmentsManager.class);

    /**
     * Get details for an opportunity that is currently running.
     *
     * @param id the id of the opportunity
     * @return <code>Opportunity</code> with the requested details
     * @throws SQLException
     */
    public static Opportunity getRunningOpportunityById(long id) throws SQLException {
        Opportunity o = null;
        Connection conn = null;
        try {
            conn = getConnection();
            o = OpportunitiesDAO.getRunningById(conn, id);
        } finally {
            closeConnection(conn);
        }
        return o;
    }

    /**
     * Get details for an opportunity that is currently running.
     *
     * @param id the id of the opportunity
     * @return <code>Opportunity</code> with the requested details
     * @throws SQLException
     */
    public static Opportunity getRunningOpportunityById(long id, boolean canBeSettled) throws SQLException {
        Opportunity o = null;
        Connection conn = null;
        try {
            conn = getConnection();
            o = new Opportunity();
            o.setId(id);
            OpportunitiesDAO.getRunningById(conn, o, canBeSettled);
        } finally {
            closeConnection(conn);
        }
        return o;
    }

    public static Opportunity getByIdForInvestmentValidation(long oppId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return OpportunitiesDAO.getByIdForInvestmentValidation(conn, oppId);
        } finally {
            closeConnection(conn);
        }
    }

    public static boolean checkActiveUser(String name) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return OpportunitiesDAO.checkActiveUser(conn, name);
        } finally {
            closeConnection(conn);
        }

    }

    /**
     * Insert investment.
     *
     * @param user the user to insert investment to (reloaded by user name)
     * @param slipEntry the investment details (opp id, amount, type (call/put))
     * @param level the current level of the opp
     * @param ip the IP from which the invest request came from
     * @param realLevel the current real level
     * @param wwwLevel the level the investment was checked against for acceptable deviation
     * @param utcOffset the UTC offset of the user when he did the investment (eg. GMT+02:00)
     * @return The id of the new investment.
     * @throws SQLException
     */
    public static long insertInvestment(User user, SlipEntry slipEntry, double level, String ip, double realLevel, double wwwLevel,
    		String utcOffset, double rate, int fromGraph, float oddsWin, float oddsLose, boolean isLikeHourly, long loginId) throws SQLException {


		oddsWin = (float) (Math.round(oddsWin*100.0)/100.0);
        if (log.isTraceEnabled()) {
            log.trace("Enter insertInvestment - user: " + user + " slipEntry: " + slipEntry + " level: " + level + " ip: " + ip + " realLevel: " + realLevel + " Rate: " +rate);
        }
        boolean isSelectedDeafultOdds;
        if(oddsWin != slipEntry.getOpportunity().getOddsType().getOverOddsWin()  || oddsLose != slipEntry.getOpportunity().getOddsType().getOverOddsLose()){
        	isSelectedDeafultOdds =  false;
        }else{
        	isSelectedDeafultOdds =  true;
        }

        long id = 0;
        long amount = new Float(slipEntry.getAmount() * 100).longValue() + slipEntry.getOptionPlusFee();
        Connection conn = getConnection();
        long s = System.currentTimeMillis();
        int isFreeTypeId = 0;
        long userId = user.getId();
        long nextInvestOnUsId = 0;
        try {
            String bonusId_typeId = BonusDAOBase.hasNextInvestOnUsWithBonusType(conn, userId, isSelectedDeafultOdds);
            String[] tmpArry = bonusId_typeId.split("_");
            nextInvestOnUsId = Long.parseLong(tmpArry[0]);
            isFreeTypeId = Integer.parseInt(tmpArry[1]);
            //all the next invest on us types should mark as 1
            if (isFreeTypeId == (int)ConstantsBase.BONUS_TYPE_INSTANT_NEXT_INVEST_ON_US ||
            	isFreeTypeId == (int)ConstantsBase.BONUS_TYPE_NEXT_INVEST_ON_US_AFTER_WAGERING ||
            	isFreeTypeId == (int)ConstantsBase.BONUS_TYPE_NEXT_INVEST_ON_US_AFTER_X_INVS) {
				isFreeTypeId = 1;
            }
            // we should have taken the current balance in the validation
//            UsersDAO.getByUserName(conn, user.getUserName(), user); // actually take the current balance
            conn.setAutoCommit(false);
            user.setBalance(user.getBalance() - amount);
            UsersDAOBase.addToBalance(conn, user.getId(), -amount);

            if (nextInvestOnUsId > 0){
            	HashMap<String, Long> freeInvValues = BonusDAOBase.getMinAndMaxFreeInv(conn, nextInvestOnUsId);

            	boolean isFree = freeInvValues.get(Constants.FREE_INVEST_MIN).longValue() <= amount &&
			                freeInvValues.get(Constants.FREE_INVEST_MAX).longValue() >= amount &&
			                slipEntry.getOpportunity().getOpportunityTypeId() == Opportunity.TYPE_REGULAR;

                if (isFree) {
		            isFree = BonusDAOBase.useNextInvestOnUs(conn, nextInvestOnUsId, amount) > 0;
		        }

//              check again that we realy update the rows in db if not or if its not good amount set the type to 0
                if (!isFree) {
                	isFreeTypeId = 0;
	            }

            }
            id = InvestmentsDAO.insertInvestment(conn, userId, slipEntry, amount, level, ip, realLevel, wwwLevel, isFreeTypeId, utcOffset, rate, nextInvestOnUsId, fromGraph, slipEntry.getOptionPlusFee(), user.getCountryId(), oddsWin, oddsLose, user.getDefaultAmountValue(), isLikeHourly, loginId);
//            if (isFree == 1) {
//                UsersDAO.setIsNextInvestOnUs(conn, user.getId(), 0);
//            }

            GeneralDAO.insertBalanceLog(conn, CommonUtil.getWebWriterId(), userId, ConstantsBase.TABLE_INVESTMENTS, id, ConstantsBase.LOG_BALANCE_INSERT_INVESTMENT, utcOffset);
            conn.commit();

            long etime = System.currentTimeMillis();
			log.log(Level.DEBUG, "TIME: insert investment + update user balance " + (etime - s));
        } catch (Throwable t) {
            try {
                conn.rollback();
            } catch (Throwable it) {
                log.error("Can't rollback.", it);
            }
            if (log.isTraceEnabled()) {
                log.trace("Leave insertInvestment with exception.");
            }
            SQLException sqle = new SQLException();
            sqle.initCause(t);
            throw sqle;
        } finally {
            try {
                conn.setAutoCommit(true);
            } catch (Throwable t) {
                log.error("Can't set back to autocommit.", t);
            }
            closeConnection(conn);
        }
        if (log.isTraceEnabled()) {
            log.trace("Leave insertInvestment - id: " + id);
        }

        try {
        	long opportunityTypeId = 0;

        	if (null != slipEntry){
        		Opportunity opp = slipEntry.getOpportunity();

        		if (null != opp){
        			opportunityTypeId = opp.getOpportunityTypeId();
        		}
        		if (opportunityTypeId == Opportunity.TYPE_OPTION_PLUS) {
        		    isFreeTypeId = 0;
        		}
        	}

            afterInvestmentSuccess(userId, user.getBalance(), id, amount, CommonUtil.getWebWriterId(), isFreeTypeId > 0, opportunityTypeId, slipEntry.getOptionPlusFee(), 0, loginId);
        } catch (Exception e) {
            log.error("Problem processing bonuses after successful investment.", e);
        }

//        if (user.getSkinId() == ConstantsBase.SKIN_ETRADER) {  // Loyalty just to etrader(now)
//	        try {
//	        	if (!isFree) { // not free invest
//	        		afterInvestLPointsCalc(id, amount, user, gm.getWriterId());
//	        	}
//	        } catch (Exception e) {
//	            log.error("Problem processing Loyalty points calculation after successful investment.", e);
//			}
//        }
        try {
            insertBonusInvestment(id, userId, user.getBalance(), amount, 1);
        } catch (Exception e) {
            log.error("BMS, Problem processing bonus investments.", e);
        }

        return id;
    }

    /**
     * Cehck if this user can make this investment. Check if he has enough money and that
     * the amount he want to invest is in the allowed interval (min, max).
     *
     * @param user the user who wants to make investment (the balance should be up to date)
     * @param slipEntry the slip entry of the investment
     * @param pageOddsWin the current odds win displayed on the client page
     * @param pageOddsLose the current odds lose displayed on the client page
     * @param invRej the reject investment detaiis
     * @return <code>null</code> if check is ok else error message.
     * @throws SQLException
     */
    public static String validateInvestmentLimits(User user, SlipEntry slipEntry, float pageOddsWin, float pageOddsLose, InvestmentRejects invRej) throws SQLException {
        long amount = new Float(slipEntry.getAmount() * 100).longValue();
        FacesContext context = FacesContext.getCurrentInstance();
        ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
        Connection conn = getConnection();
        try {
            InvestmentLimit il = InvestmentsDAO.getInvestmentLimit(conn, slipEntry.getOpportunity(), user);

            long minAmount = il.getMinAmount();
            long maxAmount = ap.isCalcalistGame() ? Constants.INVESTMENT_MAX_LIMIT_CALCALIST : il.getMaxAmount();

            if ( amount < minAmount || amount > maxAmount ) {

                if (log.isDebugEnabled()) {
                    log.debug("Limit reached. min: " + minAmount + " max: " + maxAmount);
                }

                int rejetcTypeId = Constants.INVESTMENT_REJECT_MIN_INV_LIMIT;
                String errorMsg = "";
                long amountType;

                if ( amount < minAmount ) {
                	invRej.setRejectAdditionalInfo("Min Limit:, amount: " + amount + " , minimum: " + minAmount);
                	errorMsg = "error.investment.limit.min";
                	amountType = minAmount;
                }
                else {  // amount > maxAmount
                	invRej.setRejectAdditionalInfo("Max Limit:, amount: " + amount + " , maximum: " + maxAmount);
                	rejetcTypeId = Constants.INVESTMENT_REJECT_MAX_INV_LIMIT;
                	errorMsg = "error.investment.limit.max";
                	amountType = maxAmount;
                }

            	invRej.setRejectTypeId(rejetcTypeId);
            	InvestmentRejectsManager.insertInvestmentReject(invRej);
            	String displayedAmount = "";
            	if (user.getSkinId() != Skin.SKIN_ARABIC){
            		displayedAmount = CommonUtil.displayAmount(amountType, user.getCurrencyId());

        		}else{
        			displayedAmount = CommonUtil.displayAmountForInput(amountType, false, user.getCurrencyId().intValue()) + " " + CommonUtil.getMessage(user.getCurrency().getSymbol(),null) + " ";
        		}

                return CommonUtil.getMessage(errorMsg, new Object[] {displayedAmount});
            } else if (invRej.getTypeId() != Investment.TYPE_BUBBLE) {
				long sumInv = InvestmentsManagerBase.getSumAllActiveInvestments(slipEntry.getOpportunity().getId(), user.getId());
				double invLimit = maxAmount * slipEntry.getOpportunity().getMaxInvAmountCoeffPerUser();
				if (amount + sumInv > invLimit) {
					log.debug("Max investments amount reached, userId: "+ user.getId() + ", active investments: " + sumInv + ", limit: "
								+ invLimit);
					int rejetcTypeId = ConstantsBase.INVESTMENT_REJECT_INV_SUM_LIMIT;
					invRej.setRejectTypeId(rejetcTypeId);
					InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
					String dateTimeParam = (slipEntry	.getOpportunity()
														.getScheduled() > Opportunity.SCHEDULED_DAYLY)	? CommonUtil.getDateTimeFormatDisplay(	slipEntry.getOpportunity()
																																						.getTimeEstClosing(),
																																				CommonUtil.getUtcOffset())
																										: CommonUtil.getTimeFormat(	slipEntry.getOpportunity()
																																			.getTimeEstClosing(),
																																	CommonUtil.getUtcOffset());
					return CommonUtil.getMessage(	"error.investment.sum.limit",
													new Object[] {CommonUtil.getMessage(MarketsManagerBase	.getMarket(slipEntry.getOpportunity()
																																.getMarketId())
																											.getDisplayNameKey(),
																						null),
																	dateTimeParam});
				}
            }
        } finally {
            closeConnection(conn);
        }
        return null;
    }

    /**
     * Check if this user can make this investment balance-wise.
     *
     * @param user the user who wants to make investment (the balance should be up to date)
     * @param slipEntry the slip entry of the investment
     * @return <code>null</code> if check is ok else error message.
     * @throws SQLException
     */
    public static String validateUsersBalance(User user, SlipEntry slipEntry) throws SQLException {
        long amount = new Float(slipEntry.getAmount() * 100).longValue();
        amount += slipEntry.getOptionPlusFee();
        return validateUsersBalance(user, amount, slipEntry.getOpportunity() );
    }

    /**
     * Check if this user can make this investment balance-wise.
     *
     * @param user the user who wants to make investment (the balance should be up to date)
     * @param amount the investment amount
     * @return <code>null</code> if check is ok else error message.
     * @throws SQLException
     */


    public static String validateUsersBalance(User user, long amount, Opportunity opp) throws SQLException {
        Connection conn = getConnection();
        String msgIfUsrInvestAndBalanceIsNull = "NODEPOSIT";
        String msgIfUsrInvestAndBalanceIsLower = "DEPOSITLOW";


        try {
            UsersDAO.getByUserName(conn, user.getUserName(), user); // actually take the current balance
			if (user.getBalance() < amount) {
				if (log.isDebugEnabled()) {
					log.debug("Not enough money. Balance is: "
							+ user.getBalance());
				}
				if (opp.getOpportunityTypeId() == Opportunity.TYPE_REGULAR || opp.getOpportunityTypeId() == Opportunity.TYPE_OPTION_PLUS) {
					if (user.getBalance() == 0) {
						return msgIfUsrInvestAndBalanceIsNull;
					}
					return msgIfUsrInvestAndBalanceIsLower;
				}
				return CommonUtil.getMessage("error.investment.nomoney", null);
			}
        } finally {
            closeConnection(conn);
        }
        return null;
    }
    /**
     * Check if this user can make this investment in this opportunity.
     *
     * @param user the user who wants to make investment (the balance should be up to date)
     * @param slipEntry the slip entry of the investment
     * @param pageOddsWin the current odds win displayed on the client page
     * @param pageOddsLose the current odds lose displayed on the client page
     * @param invRej the reject investment detaiis
     * @return <code>null</code> if check is ok else error message.
     * @throws SQLException
     */
    public static String validateOpportunity(User user, SlipEntry slipEntry, float pageOddsWin, float pageOddsLose, InvestmentRejects invRej) throws SQLException {
        Connection conn = getConnection();
        try {
//            long s = System.currentTimeMillis();
//            OpportunitiesDAO.getRunningById(conn, slipEntry.getOpportunity(), false);
//            if (log.isDebugEnabled()) {
//                log.log(Level.DEBUG, "TIME: get running by ID select " + (System.currentTimeMillis() - s));
//            }

			if (!slipEntry.getOpportunity().isPublished()) {
				if (log.isDebugEnabled()) {
					log.debug("Opportunity not published.");
				}
				return CommonUtil.getMessage("error.investment.notopened", null);
			}
            if (System.currentTimeMillis() < slipEntry.getOpportunity().getTimeFirstInvest().getTime()) {
                if (log.isDebugEnabled()) {
                    log.debug("Opportunity not opened.");
                }
                return CommonUtil.getMessage("error.investment.notopened", null);
            }
            if (System.currentTimeMillis() > slipEntry.getOpportunity().getTimeLastInvest().getTime()) {
                if (log.isDebugEnabled()) {
                    log.debug("Opportunity expired.");
                }
                return CommonUtil.getMessage("error.investment.expired", null);
            }
            long dbWin = Math.round(slipEntry.getOpportunity().getOverOddsWin() * 100);
            long dbLose = Math.round(slipEntry.getOpportunity().getOddsType().getOverOddsLose() * 100);
            long pageWin = Math.round((pageOddsWin - 1) * 100);
            long pageLose = Math.round((1 - pageOddsLose) * 100);

          if  (slipEntry.getOpportunity().getOpportunityTypeId() == 1){
        	  if(!isValidateOpportunityOddsGroup(slipEntry.getOpportunity().getOddsGroup(),pageWin,pageLose)){
        		  if (log.isDebugEnabled()) {
                      log.debug("db OddsGroupID: " + slipEntry.getOpportunity().getOddsGroup() +
                              " page win: " + pageWin +
                              " page lose: " + pageLose);
                  }
                invRej.setRejectAdditionalInfo("Odds Change:, dbwin: " + dbWin + " , pagewin: " + pageWin + " , db OddsGroupID: " + slipEntry.getOpportunity().getOddsGroup());
              	invRej.setRejectTypeId(Constants.INVESTMENT_REJECT_ODDS_CHANGE);
              	InvestmentRejectsManager.insertInvestmentReject(invRej);
                  return CommonUtil.getMessage("error.investment.odds.change", null);
        	  }

          }else{
            if (dbWin != pageWin || dbLose != pageLose) {
                if (log.isDebugEnabled()) {
                    log.debug("db win: " + dbWin +
                            " db lose: " + dbLose +
                            " page win: " + pageWin +
                            " page lose: " + pageLose);
                }
                invRej.setRejectAdditionalInfo("Odds Change:, dbwin: " + dbWin + " , pagewin: " + pageWin + " , dblose: " + dbLose + " , pagelose: " + pageLose);
            	invRej.setRejectTypeId(Constants.INVESTMENT_REJECT_ODDS_CHANGE);
            	InvestmentRejectsManager.insertInvestmentReject(invRej);
                return CommonUtil.getMessage("error.investment.odds.change", null);
            }
          }
        } finally {
            closeConnection(conn);
        }
        return null;
    }

    /**
     * Duplicated for etrader graph
     * Load todays opened investments for specified user. Only the fields needed for the trading page
     * right side history are loaded (market name, investment type, current level, est close time, market_id).
     *
     * @param userId
     * @return <code>ArrayList<Investment></code>
     * @throws SQLException
     */
    public static ArrayList<Investment> getTodaysOpenedInvestmentsProfit(long userId) throws SQLException {
        ArrayList<Investment> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = InvestmentsDAO.getTodaysOpenedInvestmentsProfit(conn, userId);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    /**
     * Load todays opened investments for specified user. Only the fields needed for the trading page
     * right side history are loaded (market name, investment type, current level, est close time, market_id).
     *
     * @param userId
     * @return <code>ArrayList<Investment></code>
     * @throws SQLException
     */
    public static ArrayList<Investment> getTodaysOpenedInvestments(long userId) throws SQLException {
        ArrayList<Investment> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = InvestmentsDAO.getTodaysOpenedInvestments(conn, userId);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    public static ArrayList<Opportunity> getClosedOpportunities(Date from, Date to, long marketId, long marketGroupId, String oppTypeId) throws SQLException {
        ArrayList<Opportunity> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = OpportunitiesDAO.getClosedOpportunities(conn, from, to, marketId, marketGroupId, oppTypeId);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    public static HashMap<Long, String> getClosedOpportunitiesMarkets(Date date, String oppTypeIdStr) throws SQLException,UnsupportedEncodingException {
        HashMap<Long, String> hm = null;
        Connection conn = null;
        try {
            conn = getConnection();
            hm = OpportunitiesDAO.getMarkets(conn,date, oppTypeIdStr);
        } finally {
            closeConnection(conn);
        }
        return hm;
    }
    public static ArrayList<TreeItem> getClosedOpportunitiesMarketsAnyoption() throws SQLException,UnsupportedEncodingException {
        ArrayList<TreeItem> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = OpportunitiesDAO.getMarketsAnyoption(conn, false);
        } finally {
            closeConnection(conn);
        }
        return l;
    }


    public static ArrayList<SelectItem> getAllMarketsAndGroupsByUser(long userId, boolean is0100) throws SQLException {
        ArrayList<SelectItem> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = OpportunitiesDAO.getAllMarketsAndGroupsByUser(conn,userId, is0100);
        } finally {
            closeConnection(conn);
        }
        return l;
    }
//    public static ArrayList<TreeItem> getAllMarketsAndGroupsByUserAnyoption(boolean show1Tmarkets, boolean is0100) throws SQLException,UnsupportedEncodingException{
//        ArrayList<TreeItem> l = null;
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            l = OpportunitiesDAO.getMarketsAnyoption(conn, is0100);
//        } finally {
//            closeConnection(conn);
//        }
//        return l;
//    }

    /**
     * Take opportunity disabled flag from the db.
     *
     * @param oppId the id of the opp to check
     * @return The value of the "is_disabled" flag for this opp.
     * @throws SQLException
     */
    public static boolean isOpportunityDisabled(long oppId) throws SQLException {
        boolean disabled = false;
        Connection conn = null;
        try {
            conn = getConnection();
            disabled = OpportunitiesDAO.isOpportunityDisabled(conn, oppId);
        } finally {
            closeConnection(conn);
        }
        return disabled;
    }

    /**
     * Take opportunity current acceptable level deviation when placing investment
     * (the difference between the current level from the cache and the level submitted
     * from the page that we tolerate and will accept the investment).
     *
     * @param oppId the id of the opp to check for
     * @return The opportunity curent acceptable level deviation.
     * @throws SQLException
     */
//    public static BigDecimal getOpportunityAcceptableLevelDeviation(long oppId) throws SQLException {
//        BigDecimal deviation = new BigDecimal(0);
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            deviation = OpportunitiesDAO.getOpportunityAcceptableLevelDeviation(conn, oppId);
//        } finally {
//            closeConnection(conn);
//        }
//        return deviation;
//    }

    public static ArrayList<com.anyoption.common.beans.Investment> getInvestmentsByUser(long userId,Date from,Date to,String isSettled,String marketGroup, boolean isBubble, long bubbleMarket, long skinId) throws SQLException {
        ArrayList<com.anyoption.common.beans.Investment> l = null;

        Connection conn = null;
        try {
            conn = getConnection();
            l = InvestmentsDAO.getByUser(conn,userId,from,to,isSettled,marketGroup, isBubble, bubbleMarket, skinId);

        } finally {
            closeConnection(conn);
        }
        return l;
    }

//    public static ArrayList<com.anyoption.common.beans.Investment> getOpenedInvestmentsByUser(long userId, long opportunityId, long typeId) throws SQLException {
//        ArrayList<com.anyoption.common.beans.Investment> l = null;
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            l = InvestmentsDAO.getOpenedByUser(conn, userId, opportunityId, typeId);
//        } finally {
//            closeConnection(conn);
//        }
//        return l;
//    }

    public static boolean isHasOpened(long userId) throws SQLException {
        boolean b = false;
        Connection conn = null;
        try {
            conn = getConnection();
            b = InvestmentsDAO.isHasOpened(conn, userId);
        } finally {
            closeConnection(conn);
        }
        return b;
    }

    /**
     * Get details for an One Touc opportunity that is currently running.
     *
     * @param id the id of the opportunity
     * @return <code>Opportunity</code> with the requested details
     * @throws SQLException
     */
    public static Opportunity getOneTouchOpportunityById(long id, Locale locale) throws SQLException {
        Opportunity o = null;
        Connection conn = null;
        try {
            conn = getConnection();
            o = OpportunitiesDAO.getOneTouchById(conn, id, locale);
        } finally {
            closeConnection(conn);
        }
        return o;
    }


    public static ArrayList<Opportunity> getOneTouch(long skinId, long marketId, Locale locale) throws SQLException {
    	ArrayList<Opportunity> o = null;

        Connection conn = null;
        try {
            conn = getConnection();
            o = OpportunitiesDAO.getOneTouch(conn, skinId, marketId, locale);

        } finally {
            closeConnection(conn);
        }
        return o;
    }

    /**
	 * Cehck if this user can make ONE TOUCH investment. Check if he has enough money and that the amount he want to invest is in the
	 * allowed interval (min, max).
	 *
	 * @param user the user who wants to make investment (the balance should be up to date)
	 * @param slipEntry the slip entry of the investment
	 * @param pageOddsWin the current odds win displayed on the client page
	 * @param pageOddsLose the current odds lose displayed on the client page
	 * @return <code>null</code> if check is ok else error message.
	 * @throws SQLException
	 */
    public static String validateOneTouchInvestment(User user, SlipEntry slipEntry, float pageOddsWin, float pageOddsLose, InvestmentRejects invRej) throws SQLException {
        long amount = new Float(slipEntry.getAmount() * 100).longValue();
        Connection conn = getConnection();
        try {
            UsersDAO.getByUserName(conn, user.getUserName(), user); // actually take the current balance
            if (user.getBalance() < amount) {
	            if (log.isDebugEnabled()) {
	                log.debug("Not enough money. Balance is: " + user.getBalance());
	            }
	            FacesContext context = FacesContext.getCurrentInstance();
	            SlipFormOneTouch slipForm1T = context.getApplication().evaluateExpressionGet(context, Constants.BIND_SLIP_FORM_ONE_TOUCH, SlipFormOneTouch.class);
	            slipForm1T.setLowBalanceError(true);
	            return CommonUtil.getMessage("CMS.tradeBox.text.insufficient_funds", null);
            }

            InvestmentLimit il = InvestmentsDAO.getInvestmentLimit(conn, slipEntry.getOpportunity(),user);

            long minAmount = il.getMinAmount();
        //    long maxAmount = Long.valueOf(CommonUtil.getEnum(ConstantsBase.ENUM_ONE_TOUCH, ConstantsBase.ENUM_ONE_TOUCH_USER_ASSET_MAX)).longValue();
            long maxAmount = il.getMaxAmount();

            String errorMsg = null;
            long amountType = 0;

            int rejetcTypeId = 0;

            if ( amount < minAmount ) {
            	errorMsg = "error.onetouch.limit.min";
            	amountType = minAmount;
            	invRej.setRejectAdditionalInfo("Min Limit:, amount: " + amount + " , minimum: " + minAmount);
                rejetcTypeId = Constants.INVESTMENT_REJECT_MIN_INV_LIMIT;
            }

            if ( amount > maxAmount ) {
                errorMsg = "error.onetouch.limit.max";
                amountType = maxAmount;
                invRej.setRejectAdditionalInfo("Max Limit:, amount: " + amount + " , maximum: " + maxAmount);
                rejetcTypeId = Constants.INVESTMENT_REJECT_MAX_INV_LIMIT;
            }

            if(amountType > 0/* && errorMsg == null*/) {
            	if (log.isInfoEnabled()) {
            		log.info("Wrong amount: " + amount);
            	}
            	invRej.setRejectTypeId(rejetcTypeId);
            	InvestmentRejectsManager.insertInvestmentReject(invRej);
            	return CommonUtil.getMessage(errorMsg, new Object[] {
            		CommonUtil.displayAmount(amountType, user.getCurrencyId()) });
            }

            if (invRej.getTypeId() != Investment.TYPE_BUBBLE) {
				long sumInv = InvestmentsManagerBase.getSumAllActiveInvestments(slipEntry.getOpportunity().getId(), user.getId());
				double invLimit = maxAmount * slipEntry.getOpportunity().getMaxInvAmountCoeffPerUser();
				if (amount + sumInv > invLimit) {
					log.debug("Max investments amount reached, userId: "+ user.getId() + ", active investments: " + sumInv + ", limit: "
								+ invLimit);
					rejetcTypeId = ConstantsBase.INVESTMENT_REJECT_INV_SUM_LIMIT;
					invRej.setRejectTypeId(rejetcTypeId);
					InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
					String dateTimeParam = (slipEntry	.getOpportunity()
														.getScheduled() > Opportunity.SCHEDULED_DAYLY)	? CommonUtil.getDateTimeFormatDisplay(	slipEntry.getOpportunity()
																																						.getTimeEstClosing(),
																																				CommonUtil.getUtcOffset())
																										: CommonUtil.getTimeFormat(	slipEntry.getOpportunity()
																																			.getTimeEstClosing(),
																																	CommonUtil.getUtcOffset());
					return CommonUtil.getMessage(	"error.investment.sum.limit",
													new Object[] {CommonUtil.getMessage(MarketsManagerBase	.getMarket(slipEntry.getOpportunity()
																																.getMarketId())
																											.getDisplayNameKey(),
																						null),
																	dateTimeParam});
				}
            }

            long s = System.currentTimeMillis();
            OpportunitiesDAO.getRunningById(conn, slipEntry.getOpportunity(), false);
            if (log.isDebugEnabled()) {
                log.log(Level.DEBUG, "TIME: get running by ID select " + (System.currentTimeMillis() - s));
            }

            if (!slipEntry.getOpportunity().isPublished()) {
            	if (log.isDebugEnabled()) {
                    log.debug("Opportunity not published.");
                }
                return CommonUtil.getMessage("error.investment.notopened", null);
            }
            if (System.currentTimeMillis() < slipEntry.getOpportunity().getTimeFirstInvest().getTime()) {
                if (log.isDebugEnabled()) {
                    log.debug("Opportunity not opened.");
                }
                return CommonUtil.getMessage("error.investment.notopened", null);
            }
            if (System.currentTimeMillis() > slipEntry.getOpportunity().getTimeLastInvest().getTime()) {
                if (log.isDebugEnabled()) {
                    log.debug("Opportunity expired.");
                }
                return CommonUtil.getMessage("error.investment.expired", null);
            }

            long dbWin = Math.round(slipEntry.getOpportunity().getOverOddsWin() * 100);
            long dbLose = Math.round(slipEntry.getOpportunity().getOddsType().getOverOddsLose() * 100);
            long pageWin = Math.round((pageOddsWin) * 100);
            long pageLose = Math.round((pageOddsLose) * 100);
            if (dbWin != pageWin || dbLose != pageLose) {
                if (log.isDebugEnabled()) {
                    log.debug("db win: " + dbWin +
                            " db lose: " + dbLose +
                            " page win: " + pageWin +
                            " page lose: " + pageLose);
                }
                invRej.setRejectAdditionalInfo("Odds Change:, dbwin: " + dbWin + " , pagewin: " + pageWin + " , dblose: " + dbLose + " , pagelose: " + pageLose);
            	invRej.setRejectTypeId(Constants.INVESTMENT_REJECT_ODDS_CHANGE);
            	InvestmentRejectsManager.insertInvestmentReject(invRej);
                return CommonUtil.getMessage("error.investment.odds.change", null);
            }
        } finally {
            closeConnection(conn);
        }
        return null;
    }


    /**
     * Get closed oneTouch opportunities by first and last invest
     * @param firstInv
     * 		Date of first invest
     * @param lastInv
     * 		Date of lsat invest
     * @param marketId
     * 		market id (for filter)
     * @param marketGroupId
     * 		market group id (for filter)
     * @return
     * 		list of opportunities
     * @throws SQLException
     */
    public static ArrayList<Opportunity> getClosedOneTouchOpportunities(Date firstInv, Date estClosing, long marketId) throws SQLException {

        ArrayList<Opportunity> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = OpportunitiesDAO.getClosedOneTouchOpportunities(conn,firstInv, estClosing, marketId);
        } finally {
            closeConnection(conn);
        }
        return l;
    }


    /**
     * Get list of oneTouch opportunities dates that settled
     * @return
     * @throws SQLException
     */
    public static ArrayList<Date> getClosedDatesOneOpportunities() throws SQLException {

        ArrayList<Date> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = OpportunitiesDAO.getClosedDatesOneOpportunities(conn);
        } finally {
            closeConnection(conn);
        }
        return l;
    }


    /**
     * Get one touch markets that settled
     * @param firstInv
     * 			first time investment
     * @param lastInv
     * 			last time investment
     * @return
     * @throws SQLException
     * @throws UnsupportedEncodingException
     */
    public static ArrayList<SelectItem> getClosedOneTouchOpportunitiesMarkets(Date firstInv, Date estClosing) throws SQLException,UnsupportedEncodingException {
        ArrayList<SelectItem> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = OpportunitiesDAO.getOneTouchMarkets(conn, firstInv, estClosing);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    public static void getInvestmentDetailsForPrint(SlipEntry se, long skinId) throws Exception {
        Connection conn = null;
        try {
            conn = getConnection();
            InvestmentsDAO.getInvestmentDetailsForPrint(conn, se, skinId);
        } finally {
            closeConnection(conn);
        }
    }

	public static boolean isHasOneTouchOpen(long skinId) throws SQLException {
    	boolean has = false;

        Connection conn = null;
        try {
            conn = getConnection();
            has = OpportunitiesDAO.isHasOneTouchOpen(conn, skinId);

        } finally {
            closeConnection(conn);
        }
        return has;
	}

	public static void afterInvestmentSuccess(long userId, long balance, long investmentId, long investmentAmount, long writerId, boolean isFree, long opportunityTypeId, long optionPlusFee, int insuranceType, long loginId) throws SQLException {
		// update one click user field
		if (insuranceType == 0 && (opportunityTypeId == Opportunity.TYPE_REGULAR || opportunityTypeId == Opportunity.TYPE_OPTION_PLUS)) {
			FacesContext context = FacesContext.getCurrentInstance();
			User user = context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER, User.class);
			user.setDefaultAmountValue(investmentAmount - optionPlusFee);
		}
        Connection conn = null;
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            afterInvestmentSuccess(conn, userId, balance, investmentId, investmentAmount, writerId, isFree, opportunityTypeId, loginId);
            conn.commit();
        } catch (SQLException sqle) {
            try {
                conn.rollback();
            } catch (SQLException sqlie) {
                log.error("Failed to rollback.", sqlie);
            }
            throw sqle;
        } finally {
            try {
                conn.setAutoCommit(true);
            } catch (SQLException sqle) {
                log.error("Can't return connection back to autocommit.", sqle);
            }
            closeConnection(conn);
        }
    }

	/**
	 * BMS.
	 * This method handle bonus management system logic for insert investment phase.
	 * This method calculate the adjusted amount to clean from user balance when needed.
	 * settle investment will complete the logic mechanism
	 * This method will invoke insertBonusInvestments for each bonus that was touched by investment.
	 * This method will invoke insertBonusInvestments for each investment that touch a bonus (like 0100, several investments can touch one or several bonuses.)
	 *
	 * @param investmentId unique number, generate by investment mechanism
	 * @param userId unique user identifier
	 * @param userBalance after investment
	 * @param investmentAmount amount of user investment expressed in the smallest unit of the specified currency.
	 * @param numberOfInvestments
	 * @throws SQLException
	 */
	private static void insertBonusInvestment(long investmentId, long userId, long userBalance, long investmentAmount, int numberOfInvestments) throws SQLException {
		String ls = System.getProperty("line.separator");
		Connection conn = null;
		ArrayList<Long> investmentsIds = new ArrayList<Long>();
		try {
			if (numberOfInvestments > 1) {//0100 with more than 1 inv.
        		investmentsIds = InvestmentsManager.getInvestmentsConnectedIDByInvId(investmentId);
        	}
			conn = getConnection();
        	conn.setAutoCommit(false);
        	//userTotalAdjustedAmount: contain sum of adjusted amount that the user have. user can have more than 1 bonus, therefore more than 1 adjusted amount.
        	long userTotalAdjustedAmount = BonusDAOBase.getUserTotalAdjustedAmount(conn, userId);
        	if (userBalance < userTotalAdjustedAmount) {
        		log.info(ls + "BMS, start insertBonusInvestment:" + ls +
                        "investmentId: " + investmentId + ls +
                        "userId: " + userId + ls +
                        "userBalance: " + userBalance + ls +
                        "investmentAmount: " + investmentAmount + ls +
                        "numberOfInvestments: " + numberOfInvestments + ls);
            	ArrayList<BonusUsers> bonuses = BonusDAOBase.getBonusUsed(conn, userId);
    	        BonusUsers bu = null;
    	        //The part of the amount that the user touch the bonus.
    	        //for binary0100 - the amount will be divided equally by numberOfInvestments
    	        long bonusUsed = userTotalAdjustedAmount - userBalance;
    	        long adjustmentAmountUsed = -1;
    	        for (Iterator<BonusUsers> i = bonuses.iterator(); i.hasNext() && bonusUsed > 0 && adjustmentAmountUsed < 0;) {
    	        	bu = i.next();
    	            BonusHandlerBase bh = BonusHandlerFactory.getInstance(bu.getTypeId());
    	            if (bh != null) {
    	            	try {
    	            		adjustmentAmountUsed = bu.getAdjustedAmount() - bonusUsed;
    	            		for (int j = 0; j < numberOfInvestments; j++) {
	            			   if (numberOfInvestments > 1 && investmentsIds.size() > 1) {//0100 with more than 1 inv.
    	                       		investmentId = investmentsIds.get(j);
    	                       	}
	    	            		bh.insertBonusInvestments(conn, bu, investmentId, adjustmentAmountUsed < 0 ?
	    	            				(BigDecimal.valueOf(bu.getAdjustedAmount() / (double)numberOfInvestments).setScale(0, RoundingMode.HALF_UP).longValue()) :
	    	            					(BigDecimal.valueOf(bonusUsed / (double)numberOfInvestments).setScale(0, RoundingMode.HALF_UP).longValue()),
	    	            					adjustmentAmountUsed < 0 ? 0 : adjustmentAmountUsed, ConstantsBase.BONUS_INVESTMENTS_TYPE_INSERT_INV);
    	            		}
            				bonusUsed = bh.updateAdjustedAmount(conn, bu, adjustmentAmountUsed);
    	                 } catch (BonusHandlersException e) {
    	                	 log.error("BMS, Error in insert for bonus investment manager ", e);
    	                	 throw new SQLException();
    	                 }
    	            }
    			}
            }
            conn.commit();
    		log.info(ls + "BMS, end insertBonusInvestment, success:" + ls +
                    "investmentId: " + investmentId + ls);
		} catch (SQLException esql) {
            try {
                conn.rollback();
            } catch (SQLException e_sql) {
                log.error("BMS, Failed to rollback.", e_sql);
            }
            throw esql;
        } finally {
        	try {
                conn.setAutoCommit(true);
            } catch (SQLException esql) {
                log.error("BMS, Can't return connection back to autocommit.", esql);
            }
            closeConnection(conn);
        }
    }

    public static double getTurnoverFactor(int opportunityTypeId) throws SQLException {
    	Connection conn = null;
    	double factor = 0;
    	try {
    		conn = getConnection();
    		factor = InvestmentsDAO.getTurnoverFactor(conn, opportunityTypeId);
    	} finally {
    		closeConnection(conn);
    	}
    	return factor;
    }

    public static long getSumAllActiveInvestmentsByOppId(long oppId) throws SQLException {
    	long sum = 0;

        Connection conn = null;
        try {
            conn = getConnection();
            sum = InvestmentsDAOBase.getSumAllActiveInvestmentsByOppId(conn, oppId);
        } finally {
            closeConnection(conn);
        }
        return sum;
	}

    /**
     * get opp expiration time by investment id
     * @param id - investment id
     * @return Date of the expiration
     */
    public static Date getRunningOpportunityExpTimeByInvId(long id) throws Exception {
        Date oppExp = null;

        Connection conn = null;
        try {
            conn = getConnection();
            oppExp = OpportunitiesDAO.getRunningOpportunityExpTimeByInvId(conn, id);

        } finally {
            closeConnection(conn);
        }
        return oppExp;
    }
    /**
     * roll up investment
     * @param inv the investment to roll up
     * @param user the user
     * @param amount roll up premia
     * @return the new investment id
     * @throws Exception
     */
    public static long rollUpInvestment(com.anyoption.common.beans.Investment inv, User user, long amount, long nextOppId, long loginId) throws Exception {
        Connection conn = null;
        long id = 0;
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            InvestmentsManagerBase.cancelInvestment(inv.getId(), Writer.WRITER_ID_WEB, conn);
            long insuranceAmountTotal = inv.getInsuranceAmountRU() + amount;
            long invAmount = inv.getAmount() + amount;
            UsersDAO.getByUserName(conn, user.getUserName(), user); // actually take the current balance
            user.setBalance(user.getBalance() - invAmount);
            UsersDAOBase.addToBalance(conn, user.getId(), -invAmount);
            id = InvestmentsDAO.insertInvestment(conn, user.getId(), nextOppId, inv.getTypeId(), invAmount, inv.getCurrentLevelValue(), inv.getIp(), inv.getRealLevel(), inv.getWwwLevel().doubleValue(), inv.getBonusOddsChangeTypeId(), user.getUtcOffset(), inv.getRate(), inv.getBonusUserId(), inv.getOddsWin(), inv.getOddsLose(), inv.getId(), insuranceAmountTotal, ConstantsBase.FROM_GRAPH_DEFAULT, 0, 1, user.getCountryId(), user.getDefaultAmountValue(), inv.isLikeHourly(), loginId);
            if (inv.isAcceptedSms()) { //need to update the sms column
                InvestmentsDAO.updateAcceptedSmsById(conn, id);
            }
            GeneralDAO.insertBalanceLog(conn, CommonUtil.getWebWriterId(), user.getId(), ConstantsBase.TABLE_INVESTMENTS, id, ConstantsBase.LOG_BALANCE_INSERT_INVESTMENT, user.getUtcOffset());
            afterInvestmentSuccess(conn, user.getId(), user.getBalance(), id, amount, CommonUtil.getWebWriterId(), false, inv.getOpportunityTypeId(), loginId);
            conn.commit();
        } catch (SQLException e) {
        	log.error("Exception in roll up investment! ", e);
        	try {
        		conn.rollback();
        		if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
    				FacesContext context = FacesContext.getCurrentInstance();
    				LevelsCache levelsCache = (LevelsCache) context.getExternalContext().getApplicationMap().get("levelsCache");
            		CommonUtil.sendNotifyForInvestment(inv.getOpportunityId(),
            											inv.getId(),
            											inv.getAmount() * inv.getRate(),
            											inv.getAmount(),
            											inv.getTypeId(),
            											inv.getMarketId(),
            											OpportunityType.PRODUCT_TYPE_BINARY,
            											inv.getCurrentLevel(),
            											levelsCache,
            											user,
            											CopyOpInvTypeEnum.of(inv.getCopyOpTypeId()));
    			}
        	} catch (Throwable it) {
        		log.error("Can't rollback.", it);
        	}
        	throw e;
        } finally {
        	try {
        		conn.setAutoCommit(true);
        	} catch (Exception e) {
        		log.error("Can't set back to autocommit.", e);
        	}
        	closeConnection(conn);
        }
        try {
        	log.debug("\nBMS,in rollUpInvestment, before insertBonusInvestment, userBalance: " + user.getBalance());
            insertBonusInvestment(id, user.getId(), user.getBalance(), amount, 1);
        } catch (Exception eb) {
            log.error("BMS, Problem processing rollUpInvestment.", eb);
        }
        return id;
    }

    /**
     * buy golden minute investment
     * @param investmentId the investment id to buy
     * @param amount golden minute premia
     * @param inv the investment to buy
     * @param user the user
     * @throws Exception
     */
    public static void boughtGoldenMinutesInvestment(long investmentId, long insuranceAmount, com.anyoption.common.beans.Investment inv, User user, long loginId) throws Exception {
        Connection conn = null;
        try {
            conn = getConnection();
            conn.setAutoCommit(false);

            user.setBalance(user.getBalance() - insuranceAmount);
            UsersDAOBase.addToBalance(conn, user.getId(), -insuranceAmount);
            InvestmentsManagerBase.settleInvestment(conn, investmentId, 1, insuranceAmount, inv, ConstantsBase.REGULAR_SETTLEMNT, ConstantsBase.LOG_BALANCE_SETTLE_INVESTMENT, loginId);
            conn.commit();
        } catch (SQLException e) {
        	log.error("Exception in bought GoldenMinutes Investment! ", e);
        	try {
        		conn.rollback();
        	} catch (Throwable it) {
        		log.error("Can't rollback.", it);
        	}
        	throw e;
        } finally {
        	try {
        		conn.setAutoCommit(true);
        	} catch (Exception e) {
        		log.error("Can't set back to autocommit.", e);
        	}
        	closeConnection(conn);
        }
        try {
        	afterInvestmentSuccess(user.getId(), user.getBalance(), investmentId, insuranceAmount, CommonUtil.getWebWriterId(), false, inv.getOpportunityTypeId(), 0l, Constants.INSURANCE_GOLDEN_MINUTES, loginId);
        } catch (SQLException e){
        	log.error("BMS, Error! cannot run afterInvestmentSuccess after bought Golden Minutes Investment ", e);
        }
    }

    /**
     * get next hourly or the daily opp id
     * @param id opp id
     * @return next opp to open id
     * @throws Exception
     */

    public static Opportunity getNextHourlyOppToOpenByOppId(long id) throws Exception {
        Opportunity nextOpp = null;

        Connection conn = null;
        try {
            conn = getConnection();
            nextOpp = OpportunitiesDAOBase.getNextHourlyOppToOpenByOppId(conn, id);

        } finally {
            closeConnection(conn);
        }
        return nextOpp;
    }

      /**
     * Handle points calaulation after succeed invest
     * @param investmentId investment id
     * @param investmentAmount investment amount
     * @param user current user for adding points to him
     * @param writerId
     * @throws SQLException
     */
    public static void afterInvestLPointsCalc(long investmentId, long investmentAmount, User user, long writerId) throws SQLException {
    	Connection con = null;
		try {
	        con = getConnection();
	        con.setAutoCommit(false);
	        TierUser tU = null;
	        FacesContext context = FacesContext.getCurrentInstance();

	        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_USER, User.class);
	        ve.getValue(context.getELContext());
	        if (!user.isInTier()) {  // user not qualified to any tier
	        	tU = new TierUser();
	        	tU.setUserId(user.getId());
	        	long blueTierId = ApplicationData.getSkinById(user.getSkinId()).getBlueTier();
	        	tU.setTierId(blueTierId);
	        	tU.setPoints(0);
	        	tU.setWriterId(writerId);
	        	log.debug("Insert user into Blue tier after investment, " + tU.toString());
	        	TiersDAO.insertIntoTierUser(con, tU);

	        	// update user tier details
	        	UsersDAO.setTierUSerId(con, user.getId(), tU.getId());

	        	tU.setTimeCreated(new Date());
	        	tU.setQualificationTime(new Date());
	        	user.setTierUser(tU);
	        	ve.setValue(context.getELContext(), user);
//	        	context.getApplication().createValueBinding(ConstantsBase.BIND_USER).setValue(context, user);

	        	TierUserHistory h = TiersManagerBase.getTierUserHisIns(tU.getTierId(), user.getId(), TiersManagerBase.TIER_ACTION_QUALIFIED, writerId,
		    			0, 0, investmentId, ConstantsBase.TABLE_INVESTMENTS, user.getUtcOffset());
	        	TiersManagerBase.insertIntoTierHistory(con, h);

	        } else {  // exists in tier
	        	tU = user.getTierUser();
	        }

			long tierUserId = tU.getId();
			double cashToPointsRate = ApplicationDataBase.getCashToPointsRate(user.getSkinId(), user.getCurrencyId());
	    	long points = (long)((investmentAmount/100) * cashToPointsRate);

	    	// update user loyalty points
	    	long updatedPoints = tU.getPoints() + points;
	    	TiersManagerBase.updateTierUser(con, tierUserId, writerId, updatedPoints);
	    	tU.setPoints(updatedPoints);
	    	tU.setWriterId(writerId);
	    	user.setTierUser(tU);
            ve.setValue(context.getELContext(), user);
//	    	context.getApplication().createValueBinding(ConstantsBase.BIND_USER).setValue(context, user);

	    	// insert new action to tierHistory
	    	TierUserHistory h = TiersManagerBase.getTierUserHisIns(tU.getTierId(), user.getId(), TiersManagerBase.TIER_ACTION_OBTAIN_POINTS, writerId,
	    			updatedPoints, points, investmentId, ConstantsBase.TABLE_INVESTMENTS, user.getUtcOffset());
	    	TiersManagerBase.insertIntoTierHistory(con, h);

	    	con.commit();
	} catch (SQLException e) {
		log.error("Got SQLException ",e);
		try {
			con.rollback();
		} catch (SQLException ie) {
			log.log(Level.ERROR,"Can't rollback.",ie);
		}
	} finally {
	    con.setAutoCommit(true);
	    closeConnection(con);
	}
   }

    /**
     * Update is_accepted_sms field by invesetment id
     * @param invId
     * @throws SQLException
     */
    public static void updateAcceptedSmsById(long invId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            InvestmentsDAO.updateAcceptedSmsById(conn, invId);
        } finally {
            closeConnection(conn);
        }
	}

    public static String getInsuranceInvesetmentsByUser(long id, long insuranceStartTime, long insuranceEndTime) throws SQLException {
        String result = null;
        Connection conn = getConnection();
        try {
            result = InvestmentsDAO.getInsuranceInvesetmentsByUser(conn, id, insuranceStartTime, insuranceEndTime);
        } finally {
            closeConnection(conn);
        }
        return result;
    }

    /*************************************************************/
    /////////////////////// Calcalist!!! //////////////////////////
    /*************************************************************/
    public static ArrayList<GameRank> getGameUsersRanking(long userId, int rowsNum,	String fromDate, String toDate) throws SQLException {
        Connection con = getConnection();
        try {
            return InvestmentsDAO.getGameUsersRanking(con, userId, rowsNum, fromDate, toDate);
        } finally {
            closeConnection(con);
        }

    }

	public static String validateInsuranceBuy(User user, long insuranceAmount, com.anyoption.common.beans.Investment i, GoldenMinuteType gmType) throws SQLException {
		long insuranceStartTime = Long.valueOf(CommonUtil.getEnum("insurance_period_start_time", "insurance_period_start_time")) * 60000;
		long insuranceEndTime = Long.valueOf(CommonUtil.getEnum("insurance_period_end_time", "insurance_period_end_time")) * 60000;
		if(gmType == GoldenMinuteType.ADDITIONAL) {
			insuranceStartTime = Long.valueOf(CommonUtil.getEnum("insurance_period_2_start_time", "insurance_period_2_start_time")) * 60000;
			insuranceEndTime = Long.valueOf(CommonUtil.getEnum("insurance_period_2_end_time", "insurance_period_2_end_time")) * 60000;
		}
		
		long insuranceToleranceDelaySeconds = Long.valueOf(CommonUtil.getEnum("insurance_tolerance_delay_seconds", "insurance_tolerance_delay_seconds")) * 1000;
//      FacesContext context = FacesContext.getCurrentInstance();
//      ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
        Connection conn = getConnection();
        try {
            UsersDAO.getByUserName(conn, user.getUserName(), user); // actually take the current balance
            if (user.getBalance() < insuranceAmount) {
                if (log.isDebugEnabled()) {
                    log.debug("Not enough money. Balance is: " + user.getBalance());
                }
                return CommonUtil.getMessage("error.investment.nomoney", null);
            }

            long s = System.currentTimeMillis();
            Opportunity o = OpportunitiesDAO.getRunningById(conn, i.getOpportunityId());
            if (log.isDebugEnabled()) {
                log.log(Level.DEBUG, "TIME: get running by ID select " + (System.currentTimeMillis() - s));
            }

            if (/*o.isDisabled() || */o.getMarket().isSuspended()) {
            	return CommonUtil.getMessage("error.investment.disabled", null);
            }

            if (System.currentTimeMillis() < o.getTimeFirstInvest().getTime()) {
                if (log.isDebugEnabled()) {
                    log.debug("Opportunity not opened.");
                }
                return CommonUtil.getMessage("error.investment.notopened", null);
            }

            if (System.currentTimeMillis() < o.getTimeEstClosing().getTime() - insuranceStartTime) {
                if (log.isDebugEnabled()) {
                    log.debug("insurance buy time not started.");
                }
                return CommonUtil.getMessage("error.investment.notopened", null);
            }

            if (System.currentTimeMillis() > o.getTimeEstClosing().getTime() - insuranceEndTime + insuranceToleranceDelaySeconds) {
                if (log.isDebugEnabled()) {
                    log.debug("insurance time Expired.");
                }
                return CommonUtil.getMessage("error.investment.expired", null);
            }


        } finally {
            closeConnection(conn);
        }
        return null;
	}

    public static boolean isHasMarketOpen(long skinId) throws SQLException {
        boolean has = false;

        Connection conn = null;
        try {
            conn = getConnection();
            has = OpportunitiesDAO.isHasMarketOpen(conn, skinId);

        } finally {
            closeConnection(conn);
        }
        return has;
    }

    public static HashMap<Long, LandingMarketDetails> getClosingLevels(String marketsList) throws SQLException {
    	HashMap<Long, LandingMarketDetails> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = OpportunitiesDAO.getClosingLevels(conn, marketsList);
            Long lastIdx = null;
            Long firstIdx = null;
            for (Iterator<Long> iter = l.keySet().iterator(); iter.hasNext();) {
            	Long marketId = iter.next();
            	if (null == lastIdx) { // first iteration
            		firstIdx = lastIdx = marketId;
            	} else if (null != lastIdx) {
            		l.get(lastIdx).setNextMarketId(marketId);
            		lastIdx = marketId;
            	}
            }
            //  set last entry
            l.get(lastIdx).setNextMarketId(l.get(firstIdx).getMarketId());
        } finally {
            closeConnection(conn);
        }
        return l;
    }

	public static long getSmsInvestmentLimit(Long currencyId) throws SQLException {
		long limit = 0;

        Connection conn = null;
        try {
            conn = getConnection();
            limit = InvestmentsDAO.getSmsInvestmentLimit(conn, currencyId);

        } finally {
            closeConnection(conn);
        }
		return limit;
	}

	public static long addShowOff(long investmentId, int showOffNumber) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return InvestmentsDAO.addShowOff(conn, investmentId, showOffNumber);
        } finally {
            closeConnection(conn);
        }
	}

	public static void updateShowOff(long investmentId, boolean facebook, boolean twitter) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            InvestmentsDAO.updateShowOff(conn, investmentId, facebook, twitter);
        } finally {
            closeConnection(conn);
        }
	}

	public static List<com.anyoption.common.beans.Investment> getShowOffInvestmentsList(long rowNum, boolean showOffLocale, String langCode, long skinId, boolean shortText) throws SQLException {
        Connection conn = null;
        List<com.anyoption.common.beans.Investment> l;
        try {
            conn = getConnection();
            l = InvestmentsDAO.getShowOffInvestments(conn, rowNum, showOffLocale, langCode, skinId, shortText);
        } finally {
            closeConnection(conn);
        }

        return l;
	}

	public static com.anyoption.common.beans.Investment getShowOffInvestmentByInvId(long invId) throws SQLException {
        Connection conn = null;
        com.anyoption.common.beans.Investment i;
        try {
            conn = getConnection();
            i = InvestmentsDAO.getShowOffInvestmentByInvId(conn, invId);
        } finally {
            closeConnection(conn);
        }

        return i;
	}

	public static com.anyoption.common.beans.Investment getShowOffInvestmentByShowOffIdForFacebook(long showOffId , boolean facebook) throws SQLException {
        Connection conn = null;
        com.anyoption.common.beans.Investment i;
        try {
            conn = getConnection();
            i = InvestmentsDAO.getShowOffInvestmentByShowOffIdForFacebook(conn, showOffId, facebook);
        } finally {
            closeConnection(conn);
        }

        return i;
	}

    public static double calcOptionsPlusePrice(double promil, double minPass, long marketId, long investmentId, Date timeQuoted, BigDecimal invAmount, double level) throws SQLException {
        Connection conn = null;
        double price = 0;
        try {
            conn = getConnection();
            price = InvestmentsDAO.calcOptionsPlusePrice(conn, promil, minPass, marketId, investmentId, timeQuoted, invAmount, level);
        } finally {
            closeConnection(conn);
        }

        return price;
    }

    /**
     * check if the user already saw the current banner (if he saw it in less then 5 min ago)
     * @param id - user id
     * @return
     */
    public static boolean isGMBannerViewed(long id) throws SQLException {
        Connection conn = null;
        boolean isViewed = false;
        try {
            conn = getConnection();
            isViewed = InvestmentsDAO.isGMBannerViewed(conn, id);
        } finally {
            closeConnection(conn);
        }
        return isViewed;
    }

    public static boolean insertGMBannerViewed(long id) throws SQLException {
        Connection conn = null;
        boolean isUpdate = false;
        try {
            conn = getConnection();
            isUpdate = InvestmentsDAO.insertGMBannerViewed(conn, id);
        } finally {
            closeConnection(conn);
        }
        return isUpdate;

    }

    public static void updateOneTouchExposureReached (Opportunity opp) throws SQLException {
        Connection conn = null;
        conn = getConnection();
        conn.setAutoCommit(false);
        try {
            if (opp.getOneTouchUpDown() == ConstantsBase.ONE_TOUCH_IS_UP) {          //if up/call

                    OpportunitiesDAO.updateCurrentlevel(conn, opp.getMarketId(), opp.getMarket().getUpStrikeRaiseLowerPercent(), ConstantsBase.ONE_TOUCH_IS_UP);
                    OpportunitiesDAO.updateCurrentlevel(conn, opp.getMarketId(), opp.getMarket().getDownStrikeRaiseLowerPercent(), ConstantsBase.ONE_TOUCH_IS_DOWN);
                    OpportunitiesDAO.updateOppMaxExposure(conn, opp.getId(), opp.getMarket().getRaiseExposureUpAmount() / 100);
                    OpportunitiesDAO.updateOpp1TExposureReached(conn, opp.getId(), opp.getMarket().getId());

            }
            if (opp.getOneTouchUpDown() == ConstantsBase.ONE_TOUCH_IS_DOWN){          //if down/put
                OpportunitiesDAO.updateCurrentlevel(conn, opp.getMarketId(), -(opp.getMarket().getDownStrikeRaiseLowerPercent()), ConstantsBase.ONE_TOUCH_IS_UP);
                OpportunitiesDAO.updateCurrentlevel(conn, opp.getMarketId(), -(opp.getMarket().getUpStrikeRaiseLowerPercent()), ConstantsBase.ONE_TOUCH_IS_DOWN);
                OpportunitiesDAO.updateOppMaxExposure(conn, opp.getId(), opp.getMarket().getRaiseExposureDownAmount() / 100);
                OpportunitiesDAO.updateOpp1TExposureReached(conn, opp.getId(), opp.getMarket().getId());
            }
            conn.commit();
        } catch (SQLException e) {
            log.error("Got SQLException ",e);
            try {
                conn.rollback();
            } catch (SQLException ie) {
                log.log(Level.ERROR,"Can't rollback.",ie);
            }
        } finally {
            conn.setAutoCommit(true);
            closeConnection(conn);
        }
    }

    /**
     * This method takes from DB the investment limits that correspond to the
     * given input parameters. If a parameter has inadequate value, it will be
     * ignored in the query (except from currency ID).
     *
     * @param oppTypeId
     * @param oppScheduled
     * @param oppMarketId
     * @param userCurrencyId
     * @param userId
     * @return
     * @throws SQLException
     */
    public static InvestmentLimit getInvestmentLimit(long oppTypeId,
    													long oppScheduled,
    													long oppMarketId,
    													long userCurrencyId,
    													long userId) throws SQLException {
        Connection conn = null;
        InvestmentLimit il = null;
        try {
            conn = getConnection();
            il = InvestmentsDAO.getInvestmentLimit(conn, oppTypeId, oppScheduled, oppMarketId, userCurrencyId, userId);
        } finally {
            closeConnection(conn);
        }
        return il;

    }

    public static boolean getHasOneTouchFacebookPlus() throws SQLException {
    	boolean hasOneTouchFacebookPlus = false;
        Connection conn = null;
        try {
            conn = getConnection();
            hasOneTouchFacebookPlus = OpportunitiesDAO.getHasOneTouchFacebookPlus(conn);

        } finally {
            closeConnection(conn);
        }

        return hasOneTouchFacebookPlus;

	}

    /**
     *
     *
     * @param userId
     * @return <code>ArrayList<Investment></code>
     * @throws SQLException
     */
    public static ArrayList<Investment> getLastUserInvestments(long userId, long skinId) throws SQLException {
        ArrayList<Investment> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = InvestmentsDAO.getLastUserInvestments(conn, userId, skinId);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    /*public static Investment getShowOffInvestmentHP(boolean showOffLocale, String langCode, long skinId, boolean shortText) throws SQLException {
        Connection conn = null;
        Investment l;
        try {
            conn = getConnection();
            l = InvestmentsDAO.getShowOffInvestmentsHP(conn, showOffLocale, langCode, skinId, shortText);
        } finally {
            closeConnection(conn);
        }

        return l;
	}*/

	/**
     * Insert binary 0 100 investment.
     *
     * @param user the user to insert investment to (reloaded by user name)
     * @param slipEntry the investment details (opp id, amount, type (call/put))
     * @param level the current level of the opp
     * @param ip the IP from which the invest request came from
     * @param realLevel the current real level
     * @param wwwLevel the level the investment was checked against for acceptable deviation
     * @param utcOffset the UTC offset of the user when he did the investment (eg. GMT+02:00)
     * @return The id of the new investment.
     * @throws SQLException
     */
    public static long insertInvestmentBinary0100(User user, long oppId, long choice, long amountDB, int numberOfInvestments, double level, String ip,
    		double realLevel, double wwwLevel, String utcOffset, double rate, int fromGraph, long fee, int step, long loginID) throws SQLException {
    	long id = 0;
        long s = System.currentTimeMillis();
        Connection conn = getConnection();
        try {
            conn.setAutoCommit(false);
            id = insertInvestmentBinary0100(conn, user, oppId, choice, amountDB, numberOfInvestments, level, ip, realLevel,
            		wwwLevel, utcOffset, rate, fromGraph, fee, step, loginID);
            conn.commit();

            long etime = System.currentTimeMillis();
			log.log(Level.DEBUG, "TIME: insert Binary0100 investment + update user balance " + (etime - s));
        } catch (Throwable t) {
            try {
                conn.rollback();
            } catch (Throwable it) {
                log.error("Can't rollback.", it);
            }
            if (log.isTraceEnabled()) {
                log.trace("Leave Binary0100 insertInvestment with exception.");
            }
            SQLException sqle = new SQLException();
            sqle.initCause(t);
            throw sqle;
        } finally {
            try {
                conn.setAutoCommit(true);
            } catch (Throwable t) {
                log.error("Can't set back to autocommit.", t);
            }
            closeConnection(conn);
        }
        if (log.isTraceEnabled()) {
            log.trace("Leave Binary0100 insertInvestment - id: " + id);
        }

        return id;
    }

    /**
     * Insert binary 0 100 investment.
     *
     * @param user the user to insert investment to (reloaded by user name)
     * @param slipEntry the investment details (opp id, amount, type (call/put))
     * @param level the current level of the opp
     * @param ip the IP from which the invest request came from
     * @param realLevel the current real level
     * @param wwwLevel the level the investment was checked against for acceptable deviation
     * @param utcOffset the UTC offset of the user when he did the investment (eg. GMT+02:00)
     * @return The id of the new investment.
     * @throws SQLException
     */
    public static long insertInvestmentBinary0100(Connection conn, User user, long oppId, long choice, long amountDB, int numberOfInvestments, double level, String ip,
    		double realLevel, double wwwLevel, String utcOffset, double rate, int fromGraph, long fee, int contractsStep, long loginID) throws SQLException {

        if (log.isDebugEnabled()) {
            log.debug("Enter Binary0100 insertInvestment - user: " + user +
        												" oppId: " + oppId +
        												" choice: " + choice +
        												" amount DB: " + amountDB +
        												" level: " + level +
        												" ip: " + ip +
        												" rate: " +rate +
        												" contractsStep " + contractsStep +
        												" fee " + fee
            		);
        }
        // TODO fee
        long amountDBTotal = amountDB * numberOfInvestments;
        long feeTotal = (contractsStep * numberOfInvestments * fee);
        long feeInvestment = contractsStep * fee;
        long userId = user.getId();
        user.setBalance(user.getBalance() - amountDBTotal - feeTotal );
        UsersDAOBase.addToBalance(conn, user.getId(), - amountDBTotal - feeTotal);
        if (log.isDebugEnabled()) {
            log.debug("insert investemnts");
        }
        long id = InvestmentsDAO.insertInvestment(conn, userId, oppId, choice, amountDB + feeInvestment, level, ip, realLevel, wwwLevel, 0, utcOffset, rate, 0, 0, 0, 0, 0, fromGraph, feeInvestment, numberOfInvestments, user.getCountryId(), user.getDefaultAmountValue(), false, loginID);
        if (log.isDebugEnabled()) {
            log.debug("insert Balance Log");
        }
        GeneralDAO.insertBalanceLog(conn, CommonUtil.getWebWriterId(), userId, ConstantsBase.TABLE_INVESTMENTS, id, ConstantsBase.LOG_BALANCE_INSERT_INVESTMENT, utcOffset);
        if (log.isDebugEnabled()) {
            log.debug("finish");
        }
        return id;
    }

    /**
     * update time settled for this binary0-100 investments
     * @param now time settled date
     * @param ids the investments ids to update the time settled
     * @throws SQLException
     */
	public static void updateTimeSettled(Date now, String ids) throws SQLException {

	}

	/**
     * get list of open investments for this user and oppId and not from this typeId.
     *
     * @param oppId the id of the opp.
     * @param user id the user id to bring his investments
     * @param invTypeId the type of the investments not to bring
     * @param time created if its not null get only investment from this date and type
     * @return the investment
     * @throws SQLException
     */
    public static ArrayList<com.anyoption.common.beans.Investment> getOpenBinary0100Investments(long userId, long oppId, long invTypeId, Date timeCreated) throws SQLException {
        Connection conn = null;
        ArrayList<com.anyoption.common.beans.Investment> list = null;
        try {
            conn = getConnection();
            list = InvestmentsDAO.getOpenBinary0100Investments(conn, userId, oppId, invTypeId, timeCreated);
        } finally {
            closeConnection(conn);
        }
        return list;
    }

	public static CloseContractsSums settleBinary0100Investments(int needToClose, ArrayList<com.anyoption.common.beans.Investment> openInvsToClose, double amount, double closeLevel, Opportunity opp) throws SQLException {
		Connection conn = null;
		CloseContractsSums total = new CloseContractsSums();
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            total = settleBinary0100Investments(conn, needToClose, openInvsToClose, amount, closeLevel, opp, 0L);
            conn.commit();
        } catch (Throwable t) {
            try {
                conn.rollback();
            } catch (Throwable it) {
                log.error("Can't rollback.", it);
            }
            if (log.isTraceEnabled()) {
                log.trace("Leave Binary0100 settle Investment with exception.");
            }
            SQLException sqle = new SQLException();
            sqle.initCause(t);
            throw sqle;
        } finally {
            try {
                conn.setAutoCommit(true);
            } catch (Throwable t) {
                log.error("Can't set back to autocommit.", t);
            }
            closeConnection(conn);
        }
        return total;
	}

	public static CloseContractsSums settleBinary0100Investments(Connection conn, int needToClose, ArrayList<com.anyoption.common.beans.Investment> openInvsToClose, double amount, double closeLevel, Opportunity opp, long loginId) throws SQLException {
        Date now = new Date();
        String ids = "";
        CloseContractsSums ccs = new CloseContractsSums();
        for (int i = 0; i < needToClose; i++) {//loop over the inv set time settled get the id settle inv and update time_settled
        	com.anyoption.common.beans.Investment inv = openInvsToClose.get(i);
        	inv.setTimeSettled(now);
        	ids += inv.getId() + ", ";
        	long amountDB = new Float(amount * 100).longValue();
            inv.setAcceptedSms(false); // make sure no SMS is sent when sell
            inv.setClosingLevel(closeLevel);
//                investment.setTimeQuoted(quote.getTimeQuoted());
            InvestmentsManager.settleInvestment(conn, inv.getId(), Writer.WRITER_ID_WEB, 0, inv, amountDB, ConstantsBase.LOG_BALANCE_SETTLE_INVESTMENT, loginId);
            ccs.setTotalAmount(ccs.getTotalAmount() + inv.getAmount());
            if (opp.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_ABOVE) {
    			if (inv.getTypeId() == InvestmentType.BUY) {
    				ccs.setTotalAbove(ccs.getTotalAbove() + (((100 - inv.getCurrentLevel()) * getBinary0100NumberOfContracts((Investment)inv) * inv.getRate()) * 100));
    				ccs.setTotalBelow(ccs.getTotalBelow() + (-1L * (inv.getAmount() * inv.getRate())));
    			} else if (inv.getTypeId() == InvestmentType.SELL) {
    				ccs.setTotalAbove(ccs.getTotalAbove() + (-1L * (inv.getAmount() * inv.getRate())));
    				ccs.setTotalBelow(ccs.getTotalBelow() + (((100 - inv.getCurrentLevel()) * getBinary0100NumberOfContracts((Investment)inv) * inv.getRate()) * 100));
    			}
    		} else { //below
    			if (inv.getTypeId() == InvestmentType.BUY) {
    				ccs.setTotalAbove(ccs.getTotalAbove() + (-1L * (inv.getAmount() * inv.getRate())));
    				ccs.setTotalBelow(ccs.getTotalBelow() + (((100 - inv.getCurrentLevel()) * getBinary0100NumberOfContracts((Investment)inv) * inv.getRate()) * 100));
    			} else if (inv.getTypeId() == InvestmentType.SELL) {
    				ccs.setTotalAbove(ccs.getTotalAbove() + (((100 - inv.getCurrentLevel()) * getBinary0100NumberOfContracts((Investment)inv) * inv.getRate()) * 100));
    				ccs.setTotalBelow(ccs.getTotalBelow() + (-1L * (inv.getAmount() * inv.getRate())));
    			}
    		}
        }
        if (!CommonUtil.isParameterEmptyOrNull(ids)) {
        	ids = ids.substring(0, ids.lastIndexOf(","));
            InvestmentsDAO.updateTimeSettled(conn, now, ids);
        }
        return ccs;
	}
	// 0100 fee
	public static CloseContractsSums openCloseBinary0100Investments(int needToClose, ArrayList<com.anyoption.common.beans.Investment> openInvsToClose, double amount,
			User user, long oppId, long contractType, long amountDB, int needToOpen, double currentLevel,
			WebLevelLookupBean wllb, int utcOffset, double rate, Opportunity opp, long fee, int contractsStep, long loginId) throws SQLException {
		Connection conn = null;
		CloseContractsSums total = new CloseContractsSums();
		long invId = 0;
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            if (needToClose > 0) {
            	total = settleBinary0100Investments(conn, needToClose, openInvsToClose, amount, wllb.getDevCheckLevel(), opp, loginId);
            }
            if (needToOpen > 0) {
	            invId = InvestmentsManager.insertInvestmentBinary0100(conn, user, oppId, contractType, amountDB, needToOpen, currentLevel,
	            		CommonUtil.getIPAddress(), wllb.getRealLevel(), contractType == InvestmentType.SELL ? wllb.getBid() : wllb.getOffer(),
	            		CommonUtil.formatJSUTCOffsetToString(utcOffset), rate, ConstantsBase.FROM_GRAPH_FLASH, fee, contractsStep, loginId);
            }
            conn.commit();
        } catch (Throwable t) {
            try {
                conn.rollback();
            } catch (Throwable it) {
                log.error("Can't rollback.", it);
            }
            if (log.isTraceEnabled()) {
                log.trace("Leave Binary0100 open close Investment with exception.");
            }
            SQLException sqle = new SQLException();
            sqle.initCause(t);
            throw sqle;
        } finally {
            try {
                conn.setAutoCommit(true);
            } catch (Throwable t) {
                log.error("Can't set back to autocommit.", t);
            }
            closeConnection(conn);
        }
        if (needToOpen > 0) {
        	try {
        	    afterInvestmentSuccess(user.getId(), user.getBalance(), invId, amountDB * needToOpen, CommonUtil.getWebWriterId(), false, opp.getOpportunityTypeId(), 0l, 0, loginId);
               // afterInvestmentBinary0100Success(user.getId(), user.getBalance());
            } catch (Exception e) {
                log.error("Problem processing bonuses after successful investment.", e);
            }
        	try {
                insertBonusInvestment(invId, user.getId(), user.getBalance(), amountDB, needToOpen);
            } catch (Exception e) {
                log.error("BMS,0100, Problem processing bonus investments.", e);
            }
        	RewardUserTasksManager.rewardTasksHandler(TaskGroupType.INVESTMENT, user.getId(),  (amountDB * needToOpen) + (contractsStep * needToOpen * fee) , invId, BonusManagerBase.class, Constants.WRITER_WEB_ID, OpportunityType.PRODUCT_TYPE_BINARY_0_100, opp.getOpportunityTypeId());
        }
        return total;
	}

	/**
     * Load todays opened investments for specified user. Only the fields needed for the trading page
     * right side history are loaded (market name, investment type, current level, est close time, market_id).
     *
     * @param userId
     * @return <code>ArrayList<Investment></code>
     * @throws SQLException
     */
    public static ArrayList<com.anyoption.common.beans.Investment> getAllBinary0100InvestmentsByOppId(long userId, long oppId) throws SQLException {
        ArrayList<com.anyoption.common.beans.Investment> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = InvestmentsDAO.getAllBinary0100InvestmentsByOppId(conn, userId, oppId);
        } finally {
            closeConnection(conn);
        }
        return l;
    }


    /*public static Investment getShowOffInvestmentHP(boolean showOffLocale, String langCode, long skinId, boolean shortText) throws SQLException {
        Connection conn = null;
        Investment l;
        try {
            conn = getConnection();
            l = InvestmentsDAO.getShowOffInvestmentsHP(conn, showOffLocale, langCode, skinId, shortText);
        } finally {
            closeConnection(conn);
        }

        return l;
	}*/

    /**
     * @param userId
     * @param isSettled
     * @param marketId
     * @param day
     * @param hour
     * @return
     * @throws Exception
     */
    public static ArrayList<Investment> getAllBinary0100InvestmentsByUserId(long userId, boolean isSettled, long marketId, Date day, String hour, long skinId) throws Exception {
        ArrayList<Investment> l = null;

        Connection conn = null;
        try {
            conn = getConnection();
            l = InvestmentsDAO.getAllBinary0100InvestmentsByUserId(conn, userId, isSettled, marketId, day, hour, skinId);

        } finally {
            closeConnection(conn);
        }
        return l;
    }

    /**
     * @param userId
     * @param skinId
     * @return
     * @throws Exception
     */
    public static ArrayList<SelectItem> getMarketsBinary0100UserInvestments(long userId, long skinId) throws Exception {
    	ArrayList<SelectItem> l = null;

    	Connection conn = null;
    	try {
    		conn = getConnection();
    		l = InvestmentsDAO.getMarketsBinary0100UserInvestments(conn, userId, skinId);

    	} finally {
    		closeConnection(conn);
    	}
    	return l;
    }

    /**
     * @param userId
     * @param skinId
     * @param isBinary0100
     * @return
     * @throws Exception
     */
    public static Date getLastUserInvestmentDate(long userId, long skinId, boolean isBinary0100) throws Exception {
    	Date userLastInvest = null;

    	Connection conn = null;
    	try {
    		conn = getConnection();
    		userLastInvest = InvestmentsDAO.getLastUserInvestmentDate(conn, userId, skinId, isBinary0100);

    	} finally {
    		closeConnection(conn);
    	}
    	return userLastInvest;
    }

	/**
	 * @param userId
	 * @param skinId
	 * @param marketId
	 * @param day
	 * @return
	 * @throws Exception
	 */
	public static ArrayList<SelectItem> getExpiryHourByUserInvestments(long userId, long skinId, long marketId, Date day) throws Exception {
		ArrayList<SelectItem> l = null;

    	Connection conn = null;
    	try {
    		conn = getConnection();
    		l = InvestmentsDAO.getExpiryHourByUserInvestments(conn, userId, skinId, marketId, day);

    	} finally {
    		closeConnection(conn);
    	}
    	return l;
	}

	/**
	 * @param userId
	 * @param skinId
	 * @param marketId
	 * @return
	 * @throws Exception
	 */
	public static String marketCurrentDisplayCloseHour(long userId, long skinId, long marketId) throws Exception {
		String marketDisplayHour = null;

    	Connection conn = null;
    	try {
    		conn = getConnection();
    		marketDisplayHour = InvestmentsDAO.marketCurrentDisplayCloseHour(conn, userId, skinId , marketId);

    	} finally {
    		closeConnection(conn);
    	}
    	return marketDisplayHour;
	}

	/**
	 * get top last trades,
	 * Highest profit generated in USD terms from single in-the-money expiry.
	 *
	 * @param fromDate
	 * @param tillDate
	 * @return
	 * @throws SQLException
	 */
    public static ArrayList<com.anyoption.common.beans.Investment> getLastTopTrades(String fromDate, String tillDate, long skinId) throws SQLException {
        ArrayList<com.anyoption.common.beans.Investment> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = InvestmentsDAO.getLastTopTrades(conn, fromDate, tillDate, skinId);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    /**
	 * Get top trades,
	 * Highest profit generated in USD terms from single in-the-money expiry.
	 *
	 * @param skinBusinessCaseId
	 * @return
	 * @throws SQLException
	 */
    public static HashMap<Long, ArrayList<com.anyoption.common.beans.Investment>> getTopTrades(String fromDate, long skinBusinessCaseId) throws SQLException {
        HashMap<Long, ArrayList<com.anyoption.common.beans.Investment>> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = InvestmentsDAO.getTopTrades(conn, fromDate, skinBusinessCaseId);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

	public static boolean isHasOpenedInvByOpp(long id, long oppId) throws SQLException {
		boolean isHasOpen = false;
		Connection conn = null;
        try {
            conn = getConnection();
            isHasOpen = InvestmentsDAO.isHasOpenedInvByOpp(conn, id, oppId);
        } finally {
            closeConnection(conn);
        }
        return isHasOpen;
	}

//	public static void afterInvestmentBinary0100Success(long userId, long balance) throws SQLException {
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            conn.setAutoCommit(false);
//            afterInvestmentSuccessTouchBonuses(conn, userId, balance);
//            conn.commit();
//        } catch (SQLException sqle) {
//            try {
//                conn.rollback();
//            } catch (SQLException sqlie) {
//                log.error("Failed to rollback.", sqlie);
//            }
//            throw sqle;
//        } finally {
//            try {
//                conn.setAutoCommit(true);
//            } catch (SQLException sqle) {
//                log.error("Can't return connection back to autocommit.", sqle);
//            }
//            closeConnection(conn);
//        }
//    }

	public static boolean isHas0100Investments(long userId) throws SQLException {
	    Connection conn = null;
	    try {
	        conn = getConnection();
	        return InvestmentsDAO.isHas0100Investments(conn, userId);
	    } finally {
	        closeConnection(conn);
	    }
	}

	public static boolean isShowOffAllowed(Investment i) {
		// just investments with timeSettled from last week
		if (i.getTimeSettled() == null) {
			return false;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, -7);
		return i.getTimeSettled().getTime() >= calendar.getTimeInMillis();
	}

	public static long getUserProfit(Investment i) {
		return i.getWin() - i.getLose();
	}

	public static ArrayList<Long> getInvestmentsCountArray(Investment i) {
		int step = Investment.BINARY0100_CONTRACTS_STEPS[(int) i.getCurrencyId()];
		int steps = (int) i.getInvestmentsCount() / step;
		ArrayList<Long> list = new ArrayList<Long>();
		for (long j = 1; j <= steps; j++) {
			list.add(j * step);
		}
		return list;
	}

	public static long getTotalFee(Investment i) {
		return (i.getInvestmentsCount() * i.getOptionPlusFee()) / getBinary0100NumberOfContracts(i);
	}

	/**
	 * Loads the opened investments for given user into the result object.
	 * 
	 * @param result resulting object that will contain the investments
	 * @param userId id of the user
	 * @param skinId skin id of the user
	 */
	public static void loadOpenPositions(OpenPositionsMethodResult result, long userId, long skinId) {
		Connection con = null;
		try {
			con = getConnection();
			HashMap<Long, HashMap<Long, MarketNameBySkin>> mnbs = MarketsManagerBase.getMarketNamesBySkin();
			if (mnbs != null) {
				InvestmentsDAO.loadOpenPositions(con, result, userId, mnbs, skinId);
			}
			result.clearHelperMap(); // for optimization
		} catch (SQLException e) {
			log.debug("Unable to add open positions", e);
		} finally {
			closeConnection(con);
		}
	}
}