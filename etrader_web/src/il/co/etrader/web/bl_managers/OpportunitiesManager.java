package il.co.etrader.web.bl_managers;

import il.co.etrader.web.bl_vos.Binary0100EventSelectionBean;
import il.co.etrader.web.dao_managers.OpportunitiesDAO;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.managers.BaseBLManager;

public class OpportunitiesManager extends BaseBLManager {
    public static long getMarketCurrentOpportunity(long marketId, long opportunityTypeId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return OpportunitiesDAO.getMarketCurrentOpportunity(conn, marketId, opportunityTypeId);
        } finally {
            closeConnection(conn);
        }
    }

    public static long getMarketCurrentOpportunityBinary0100(long marketId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return OpportunitiesDAO.getMarketCurrentOpportunityBinary0100(conn, marketId);
        } finally {
            closeConnection(conn);
        }
    }

    public static long getMarketCurrentOpenOpportunity(long marketId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return OpportunitiesDAO.getMarketCurrentOpenOpportunity(conn, marketId);
        } finally {
            closeConnection(conn);
        }
    }

//    public static ArrayList<Long> getOptionPlusCurrentMarket(long skinId, boolean onlyOpened) throws SQLException {
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            return OpportunitiesDAO.getOptionPlusCurrentMarket(conn, skinId, onlyOpened);
//        } finally {
//            closeConnection(conn);
//        }
//    }
//    
    public static List<Binary0100EventSelectionBean> getBinary0100PublishedEvents(long skinId, long oppId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return OpportunitiesDAO.getBinary0100PublishedEvents(conn, skinId, oppId);
        } finally {
            closeConnection(conn);
        }
    }
    
//    public static long getBinary0100CurrentMarket(long skinId) throws SQLException {
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            return OpportunitiesDAO.getBinary0100CurrentMarket(conn, skinId);
//        } finally {
//            closeConnection(conn);
//        }
//	}
    
    public static Opportunity getUsersLastTradeBinary0100Event(long userId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return OpportunitiesDAO.getUsersLastTradeBinary0100Event(conn, userId);
        } finally {
            closeConnection(conn);
        }
    }
}