package il.co.etrader.web.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import il.co.etrader.web.dao_managers.LoginProductDAO;
import il.co.etrader.web.util.Constants;

/**
 * @author kiril.mutafchiev
 */
public class LoginProductManager extends il.co.etrader.bl_managers.LoginProductManager {

	private static final Logger log = Logger.getLogger(LoginProductManager.class);
	private static final Map<LoginProductEnum, String> PRODUCT_TO_NAVIGATION_MAP = new HashMap<>();
	static {
		PRODUCT_TO_NAVIGATION_MAP.put(LoginProductEnum.BINARY, Constants.NAV_TRADE_PAGE);
		PRODUCT_TO_NAVIGATION_MAP.put(LoginProductEnum.ONE_TOUCH, Constants.NAV_AO_ONE_TOUCH);
		PRODUCT_TO_NAVIGATION_MAP.put(LoginProductEnum.OPTION_PLUS, Constants.NAV_AO_OPTION_PLUS);
		PRODUCT_TO_NAVIGATION_MAP.put(LoginProductEnum.BUBBLES, Constants.NAV_BUBBLES_PAGE);
		PRODUCT_TO_NAVIGATION_MAP.put(LoginProductEnum.DYNAMICS, Constants.NAV_DYNAMICS);
		PRODUCT_TO_NAVIGATION_MAP.put(LoginProductEnum.LONG_TERM, Constants.NAV_LONG_TERM);
	}

	/**
	 * Returns the navigation case for the product that should be loaded on login for the given skin id. If there is no manual login product
	 * set or there is an error during loading, a default product navigation will be returned.
	 * 
	 * @param skinId the skin id for the product to load on login
	 * @return the string representing the navigation case for the login product
	 */
	public static String getLoginProductNavigation(long skinId) {
		Connection con = null;
		try {
			try {
				con = getConnection();
				LoginProductEnum loginProduct = LoginProductDAO.getLoginProduct(con, skinId);
				if (loginProduct != null) {
					if (PRODUCT_TO_NAVIGATION_MAP.get(loginProduct) == null) {
						log.debug("No mapping for login product " + loginProduct + ". Returning default automatic product");
					} else {
						return PRODUCT_TO_NAVIGATION_MAP.get(loginProduct);
					}
				}
			} catch (SQLException e) {
				log.debug("Unable to load login product. Calculating default automatic product", e);
			}
		} finally {
			closeConnection(con);
		}
		// Either no manual login product set, or there was a problem during loading
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(LOGIN_PRODUCT_TIME_ZONE));
		Date now = cal.getTime();
		setCalendarDate(cal, AUTO_WEEKEND_START_DAY_OF_WEEK, AUTO_WEEKEND_START_HOUR, AUTO_WEEKEND_START_MINUTE);
		Date autoWeekendStart = cal.getTime();
		setCalendarDate(cal, AUTO_WEEKEND_END_DAY_OF_WEEK, AUTO_WEEKEND_END_HOUR, AUTO_WEEKEND_END_MINUTE);
		Date autoWeekendEnd = cal.getTime();
		if (now.after(autoWeekendStart) && now.before(autoWeekendEnd)) {
			return PRODUCT_TO_NAVIGATION_MAP.get(AUTO_WEEKEND_PRODUCT);
		} else {
			return PRODUCT_TO_NAVIGATION_MAP.get(AUTO_NOT_WEEKEND_PRODUCT);
		}
	}

	private static void setCalendarDate(Calendar cal, int dayOfWeek, int hourOfDay, int minute) {
		cal.set(Calendar.DAY_OF_WEEK, dayOfWeek);
		cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
		cal.set(Calendar.MINUTE, minute);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
	}
	
	public static Map<LoginProductEnum, String> getProductToNavigationMap() {
		return PRODUCT_TO_NAVIGATION_MAP;
	}
}