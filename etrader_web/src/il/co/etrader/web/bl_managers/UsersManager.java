package il.co.etrader.web.bl_managers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.File;
import com.anyoption.common.beans.InvestmentRejects;
import com.anyoption.common.beans.MailBoxTemplate;
import com.anyoption.common.beans.MailBoxUser;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.CurrenciesRules;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.bl_vos.Ad;
import com.anyoption.common.daos.CurrenciesRulesDAOBase;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.managers.UsersPendingDocsManagerBase;
import com.anyoption.common.util.MarketingTracker;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bl_managers.ContactsManager;
import il.co.etrader.bl_managers.EmailAuthenticationManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.SMSManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.EmailAuthentication;
import il.co.etrader.bl_vos.MarketingAffilate;
import il.co.etrader.bl_vos.MarketingCombination;
import il.co.etrader.bl_vos.MarketingSource;
import il.co.etrader.bl_vos.MarketingSubAffiliate;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.AdsDAO;
import il.co.etrader.dao_managers.BonusDAOBase;
import il.co.etrader.dao_managers.ContactsDAO;
import il.co.etrader.dao_managers.LimitsDAO;
import il.co.etrader.dao_managers.MailBoxTemplatesDAOBase;
import il.co.etrader.dao_managers.MailBoxUsersDAOBase;
import il.co.etrader.dao_managers.MarketingAffiliatesDAOBase;
import il.co.etrader.dao_managers.MarketingCombinationsDAOBase;
import il.co.etrader.dao_managers.TransactionsDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.sms.SMS;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_vos.MailBoxUserAttachment;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.dao_managers.FilesDAO;
import il.co.etrader.web.dao_managers.MailBoxUsersDAO;
import il.co.etrader.web.dao_managers.UsersDAO;
import il.co.etrader.web.mbeans.ContactUsForm;
import il.co.etrader.web.mbeans.LoginForm;
import il.co.etrader.web.mbeans.RegisterForm;
import il.co.etrader.web.util.Constants;
import il.co.etrader.web.util.RefererDecoderManager;
import il.co.etrader.web.util.Utils;

public class UsersManager extends UsersManagerBase {

	private static final Logger logger = Logger.getLogger(UsersManager.class);
//	private static final int STATUS_ID_WRONG_FETCH = 1;
//	private static final int STATUS_ID_WRONG_MOBILE = 2;
//	private static final int STATUS_ID_WRONG_EMAIL = 3;
	private static final int STATUS_ID_SUCCESS = 4;
//	private static final int STATUS_ID_WRONG_ID_NUM = 5;

	private static boolean validateRegisterForm(RegisterForm form)throws SQLException {

		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData) context.getApplication()
								.createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String formName = "registerForm";
		if (context.getViewRoot().getViewId().indexOf("newFullRegPL.xhtml") > -1){ //set the form name for landing pages(different logic)
			formName = "landingRegisterForm";
		}
		boolean validate = true;
		Connection con = getConnection();
		try {
			 // TODO when etrader mobile has reg funnel this check will be unnecessary
	        if (form.getBirthYear() != null && form.getBirthMonth() != null && form.getBirthDay() != null) {
				try {
					if (!Utils.birthDateValidation(form.getBirthYear(), form.getBirthMonth(), 
						form.getBirthDay())) {
						logger.debug("failed registerform: Illegal birth date!");
						validate = false;
						String birthField = "registerForm:birth";
						if (context.getViewRoot().getViewId().indexOf("70_generic_full.xhtml") > -1){ // add field name for 70_generic landing page
							birthField = formName + ":birthDay";
						}
						FacesMessage fm = new FacesMessage(
								FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage(
										"error.register.birthdate", null), null);
						context.addMessage(birthField, fm);

						if (!CommonUtil.isHebrewSkin(ap.getSkinId()) && form.getPageSection() == 4) {  // mobile
							form.setPageSection(RegisterForm.MOBILE_SECTION2);
						}
					}
				} catch (Exception e) {
					logger.debug("Error parsing birthdate", e);
				}
	        }

			if (form.getSkinId() != Skin.SKIN_ETRADER && !form.isTerms()) {
				logger.debug("failed registerform: terms not activated!");
				validate = false;
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage("error.register.terms", null),
						null);
				context.addMessage(formName + ":terms", fm);

				if (!CommonUtil.isHebrewSkin(ap.getSkinId()) && form.getPageSection() == 4) {  // mobile
					form.setPageSection(RegisterForm.MOBILE_SECTION3);
				}
			}

            if (ap.getSkinId() == Skin.SKIN_GERMAN) {
            	String[] params = new String[1];
                if ((null != form.getMobilePhone() && form.getMobilePhone().length() > 0) || (null != form.getLandLinePhone() && form.getLandLinePhone().length() > 0)) {
                    if (form.isContactBySms()) {
                        if (null == form.getMobilePhone() || form.getMobilePhone().length() == 0) {
                            logger.debug("failed registerform: no mobile phone and want contact by SMS");
                            validate = false;
                            FacesMessage fm;
                            if (context.getViewRoot().getViewId().indexOf("70_generic_full.xhtml") > -1){  // add field name for 70_generic landing page
                            	params[0] = CommonUtil.getMessage("register.mobilephone", null);
	                            fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
	                                    CommonUtil.getMessage("javax.faces.component.UIInput.REQUIRED",
	                                            params), form.getMobilePhoneField());
                            } else {
	                            fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
	                                    CommonUtil.getMessage("error.mandatory",
	                                            null), form.getMobilePhoneField());
                            }
                            context.addMessage(formName + ":mobilePhone", fm);
                        }
                    }
                } else {
                	FacesMessage fm;
                    if (context.getViewRoot().getViewId().indexOf("70_generic_full.xhtml") > -1){ // add field name for 70_generic landing page
                    	String phoneType = "register.phone";
                    	if(form.isContactBySms()){
                    		phoneType = "register.mobilephone";
                    	}
                    	params[0] = CommonUtil.getMessage(phoneType, null);
                        fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        		CommonUtil.getMessage("javax.faces.component.UIInput.REQUIRED", params), form.getMobilePhoneField());
                    } else {
	                    fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
	                            CommonUtil.getMessage("error.mandatory",
	                                    null), form.getMobilePhoneField());
                    }
                    if (form.isContactBySms()) {
                        logger.debug("failed registerform: no mobile phone and want contact by SMS");
                        context.addMessage(formName + ":mobilePhone", fm);
                    } else {
                        logger.debug("failed registerform: no mobile phone or landing phone");
                        context.addMessage(formName + ":landLinePhone", fm);
                    }
                    validate = false;
                }
            }

            // Just for anyoption - email is unique
            if (!CommonUtil.isHebrewSkin(ap.getSkinId())) {
    			if (UsersDAOBase.isEmailInUse(con, form.getEmail(), 0)) {
    				logger.debug("failed registerform: email in use!");
    				validate = false;
    				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
    						CommonUtil.getMessage("error.register.email.inuse",	null), form.getEmailField());
    				context.addMessage(formName + ":email", fm);

    				if (!CommonUtil.isHebrewSkin(ap.getSkinId()) && form.getPageSection() == 4) {  // mobile
    					form.setPageSection(RegisterForm.MOBILE_SECTION1);
    				}
    			}
            }

			if (UsersDAOBase.isUserNameInUse(con, form.getUserName(), true, null)) {
				logger.debug("failed registerform: user name in use!");
				validate = false;
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage("error.register.username.inuse",
								null), null);
				context.addMessage(formName + ":userName", fm);

				if (!CommonUtil.isHebrewSkin(ap.getSkinId()) && form.getPageSection() == 4) {  // mobile
					form.setPageSection(RegisterForm.MOBILE_SECTION1);
				}
			}

			// Just for etrader/TLV skins(city and numId validation)
			if (CommonUtil.isHebrewSkin(ap.getSkinId())) {
				
				try {
					// added for the new reg funnel to work
					if (form.getCityName() != null && !form.getCityName().isEmpty()) {
						CommonUtil.validateHebrewOnly(null, null, form.getCityName());
						if (form.getCityId() == 0) {
							logger.debug("failed registerform: Illegal city!");
							validate = false;
							FacesMessage fm = new FacesMessage(
									FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage(
											"error.register.city", null), null);
							context.addMessage(formName + ":cityId", fm);
						}
					}
				} catch (Exception e) {
					logger.debug("failed registerform: Illegal city - not hebrew!");
					validate = false;
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
							CommonUtil.getMessage("error.city.nothebrew", null),
							null);
					context.addMessage(formName + ":cityId", fm);
				}
			}

			//after landing validation change the zipcode to empty (if it's not changed)
			if (validate == true && formName.equalsIgnoreCase("landingRegisterForm")){
				Locale locale = ap.getUserLocale();
				if (form.getZipCode().equalsIgnoreCase(CommonUtil.getMessage("register.postcode", null, locale))){
					form.setZipCode("");
				}
			}


		} finally {
			closeConnection(con);
		}
		return validate;

	}
	
	/**
	 * This method validates the Israeli personal ID number for ETrader users.
	 * 
	 * @param idNum
	 * @param formName
	 * @param context
	 * @param con
	 * @param ap
	 * @return <code>true</code> if the ID number is OK, otherwise <code>false</code>.
	 * @throws SQLException
	 */
	private static boolean validateIdNum(String idNum,
											String formName,
											FacesContext context,
											Connection con,
											ApplicationData ap,
											long userId) throws SQLException {
		String errMsg = null;
		if (idNum == null
				|| "".equals(idNum.trim())
				|| Constants.ID_NUM_DEFAULT_VALUE.equals(idNum)) {
			logger.debug("failed registerform: user ID is empty [" + idNum + "]");
			errMsg = "javax.faces.component.UIInput.REQUIRED_detail";
		} else {
			errMsg = CommonUtil.validateIdNum(idNum);
			if (errMsg != null) {
				logger.debug("failed registerform: user ID number not valid!");
			} else if (UsersDAOBase.isUserIdNumberInUse(con, idNum, ap.getSkinId(), userId)) {
				logger.debug("failed registerform: user ID number in use!");
				errMsg = "error.register.useridnum.inuse";
				if (ap.isCalcalistGame()) {
					errMsg = "error.register.useridnum.inuse.cal";
				}
			}
		}
		
		if (errMsg != null) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage(errMsg, null), null);
			context.addMessage(formName + ":idNum", fm);
			return false;
		}
		return true;
	}

	public static String validateUserName(String value) throws SQLException {
		Connection con = getConnection();
		try {

			if (value.length() < 6 && value.length() > 0) {
				return CommonUtil.getMessage("error.username.ajaxerror", null);

			}
			if (!value.matches("[0-9a-zA-Z]*")) {

				return CommonUtil.getMessage("error.englishandnumbers.only",
						null);

			}

			if (UsersDAOBase.isUserNameInUse(con, value, true, null)) {

				return CommonUtil.getMessage("error.register.username.inuse",
						null);

			}

			return "&#160;";
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Add user from web and role for him to the db.
	 * @throws Exception
	 */

	public static boolean insertUser(RegisterForm f) throws Exception {

		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Insert new user: " + ls + f.toString());
		}

		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData gm = (ApplicationData) context.getApplication()
				.createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(
						context);

		String contextPath = context.getExternalContext()
				.getRequestContextPath();
		// contextPath=contextPath.replaceAll("/", "").trim();
		logger.log(Level.DEBUG, "context:" + contextPath);

		HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);

		if ( !validateRegisterForm(f) ) {
			return false;
		}

		User user = new User();

		Connection con = getConnection();

		try {

			Date timeFirstVisit = getTimeFirstVisit(req, true);
			logger.log(Level.DEBUG, "timeFirstVisit = " + timeFirstVisit.toString());
			user.setTimeFirstVisit(timeFirstVisit);

			String dp = getDynamicParam(req);
			if ( null != dp ) {
				logger.log(Level.DEBUG, "dynamic param, dp = " + dp);
				user.setDynamicParam(dp);
			} else {
				logger.log(Level.DEBUG, "dynamic param not found");
			}
			Long combId = gm.getCombIdWithSPLogic(true, user);

			long affId = -1;
			if (null != dp) {
				affId = ApplicationData.getAffIdFromDynamicParameter(dp);
			}
			if ((gm.isNetreferCombination(combId) || gm.isReferpartnerCombination(combId)) && affId > 0) {  
				logger.log(Level.DEBUG, "affKey = " + affId);
				user.setAffiliateKey(affId);
			}
			Long subAff = getSubbAffIdParam(req);
			if ( null != subAff ) {
				logger.log(Level.DEBUG, "sub affiliate id param, subAff = " + subAff);
				user.setSubAffiliateKey(subAff);
			} else {
				logger.log(Level.DEBUG, "sub affiliate id param not found");
			}

			String chatId = (String)session.getAttribute(Constants.VSP_CHAT_ID);
			if (null != chatId) {
				user.setChatId(Long.parseLong(chatId));
			}

			// LivePerson - session chat id
			String lpChatIdValue = getLPSessionKey(req);
			if (null != lpChatIdValue) {
				user.setLpSessionKey(lpChatIdValue);
			}

			String clickId = getClickId(req);
			if ( null != clickId ) {
				logger.log(Level.DEBUG, "clickId, click = " + clickId);
				user.setClickId(clickId);
			} else {
				logger.log(Level.DEBUG, "clickId not found");
			}


			user.setCombinationId(combId);
			user.setIp(CommonUtil.getIPAddress());
			user.setBalance(0);
			user.setCityId(f.getCityId());
			user.setCityName(f.getCityName());
			user.setComments("");
			user.setEmail(f.getEmail());
			user.setFirstName(f.getFirstName());
			user.setGender(f.getGender());
			user.setIdNum(f.getIdNum());
			user.setLastName(f.getLastName());
			user.setIsActive("1");

			if (f.isContactByEmail()) {
				user.setIsContactByEmail(1);
			} else {
				user.setIsContactByEmail(0);
			}

			if ( f.isContactBySms() ) {
				user.setIsContactBySMS(1);
			} else {
				user.setIsContactBySMS(0);
			}


			if (CommonUtil.isHebrewSkin(f.getSkinId())) {
				if (f.getLandLinePhonePrefix() != null && f.getLandLinePhone() != null) {
					user.setLandLinePhone(f.getLandLinePhonePrefix()+ f.getLandLinePhone());
				}
				// used for the new reg funnel returning the phone prefix as number and code as 050 is not a number it is 50
				if (!f.getMobilePhonePrefix().startsWith("0")) {
					f.setMobilePhonePrefix("0" + f.getMobilePhonePrefix());
				}
				user.setMobilePhone(f.getMobilePhonePrefix() + f.getMobilePhone());
			} else {  // for anyoption
				user.setMobilePhone(f.handleLeadingZero(f.getMobilePhone()));
				user.setLandLinePhone(f.handleLeadingZero(f.getLandLinePhone()));
				user.setTaxExemption(true);
			}

			user.setPassword(f.getPassword());
			user.setStreet(f.getStreet());
			user.setStreetNo(f.getStreetNo());
			user.setTaxBalance(0);
			user.setTimeLastLogin(new Date());

			// TODO when etrader mobile has reg funnel this check will be unnecessary
            if (f.getBirthYear() != null && f.getBirthMonth() != null && f.getBirthDay() != null) {
				Calendar c = new GregorianCalendar(Integer.parseInt(f
						.getBirthYear()), Integer.parseInt(f.getBirthMonth()) - 1,
						Integer.parseInt(f.getBirthDay()), 0, 1);
				user.setTimeBirthDate(new Date(c.getTimeInMillis()));
            }

            // for setting the skin by us country(if is chosen) in registration
            if (f.getCountryId() == ConstantsBase.COUNTRY_ID_US &&
                    f.getSkinId() != Skin.SKIN_EN_US &&
                    f.getSkinId() != Skin.SKIN_ES_US) {
                if (f.getSkinId() == Skin.SKIN_SPAIN) {
                    user.setSkinId(Skin.SKIN_ES_US);
                } else {
                    user.setSkinId(Skin.SKIN_EN_US);
                }
                user.setCurrencyId(ConstantsBase.CURRENCY_USD_ID);
            } else {
                user.setSkinId(f.getSkinId());
                user.setCurrencyId(f.getCurrencyId());
            }

            if (gm.isUsSkins() && f.getCountryId()!= Constants.COUNTRY_ID_US) {
            	long skinId = gm.getSkinId();
            	if(skinId == Skin.SKIN_EN_US) {
            		user.setSkinId(Skin.SKIN_ENGLISH);
            	} else if (skinId == Skin.SKIN_ES_US) {
            		user.setSkinId(Skin.SKIN_SPAIN);
            	}
            }

            CurrenciesRules currenciesRules = new CurrenciesRules();
            currenciesRules.setCountryId(f.getCountryId());
            currenciesRules.setSkinId(user.getSkinId());
            long defaultCurrencyId = CurrenciesRulesDAOBase.getDefaultCurrencyId(con, currenciesRules);
            user.setCurrencyId(defaultCurrencyId);
            logger.info("CurrenciesRules [countryId=" + currenciesRules.getCountryId() + ", skinId=" + currenciesRules.getSkinId() + "]. Default currency id by the table CurrenciesRules=" + defaultCurrencyId);
			user.setLimitId(LimitsDAO.getBySkinAndCurrency(con, user.getSkinId(), defaultCurrencyId));

			user.setUserName(f.getUserName());
			user.setZipCode(f.getZipCode());
			user.setClassId(UsersDAOBase.getDefaultClass(con).getId());
			user.setCountryId(f.getCountryId());
			user.setLanguageId(f.getLanguageId());
			user.setIdDocVerify(false);
			user.setUtcOffsetCreated(f.getUtcOffsetCreated());
			user.setState(f.getStateId());
			user.setUserAgent(CommonUtil.getUserAgent());
			if (null != session.getAttribute(Constants.DEVICE_UNIQUE_ID)) {
				user.setDeviceUniqueId(session.getAttribute(Constants.DEVICE_UNIQUE_ID).toString());
			}
			try {
				user.setHttpReferer(getHttpReferer(req));
			} catch (UnsupportedEncodingException e1) {
				logger.warn("Problem decoding http referer!");
			}
			if (user.getHttpReferer()!=null) {
				RefererDecoderManager rdm = new RefererDecoderManager(true);
			    user.setDecodedHttpRefferer(rdm.decodeAll(user.getHttpReferer()));
			}
			// set Double Click marketing values
			user.setDfaPlacementId(getDFAPlacementId(req));
			user.setDfaCreativeId(getDFACreativeId(req));
			user.setDfaMacro(getDFAMacro(req));

			long contactId = f.getContactId();

			user.setAcceptedTerms(f.isTerms());

			// CalcalistGame changes
			if (gm.isCalcalistGame()) {
				user.setUserName(f.getUserName() + ConstantsBase.CAL_GAME_USER_NAME_SUFFIX);
				user.setClassId(ConstantsBase.USER_CLASS_TEST);
				user.setTaxExemption(true);
				user.setCombinationId(ConstantsBase.CALCALIST_COMBINATION_ID);
				logger.debug("CalclistGame registration: change details for user: " + user.getUserName());
			}
			//user done register from web
			user.setFingerPrint(f.getFingerPrint());
			
			long writerId = CommonUtil.getWebWriterId();
	    	if (CommonUtil.isMobile()) {
	    		writerId = Writer.WRITER_ID_AO_MINISITE;
    		}
	    	user.setWriterId(writerId);

            boolean isMailAuth = false;
            if (CommonUtil.isHebrewSkin(f.getSkinId()) /*|| For now we removed the email authorization
					CommonUtil.isMobile()*/) {
            	isMailAuth = false;
            	user.setAuthorizedMail(false);
            } else {
            	isMailAuth = false;
            	user.setAuthorizedMail(true);
            }
            user.setPlatformId(f.getPlatformId());
            con.setAutoCommit(false);

			// Insert new user with contactId check.
            // Also, insert user in users_regulation if the user should be regulated.
			UsersManagerBase.insert(con, user, contactId);
			
			con.commit();

			// Insert first visit record (with first combId and dp)
			try {
				Long firstCombId = gm.getFirstCombIdCookie(req);
				if (null != firstCombId) {
					insertFirstVisitRecord(user.getId(), firstCombId.longValue(), gm.getFirstDynamicParamCookie(req));
				}
			} catch(Exception e) {
				logger.info("Problem inserting firstVisit record! userId: " + user.getId());
			}

			// add admin deposit for calcalist game users
			if (gm.isCalcalistGame()) {
				insertAdminDeposit(user, TransactionsManagerBase.TRANS_TYPE_ADMIN_DEPOSIT,
						user.getUtcOffsetCreated(), ConstantsBase.CAL_GAME_VIRTUAL_MONEY, CommonUtil.getWebWriterId(), 0L);
			}

			// Get the contact Id from the user in case it was change in the insert.
			contactId = user.getContactId();

			try {
				if (contactId > 0) {
					long userId = user.getId();
					UsersManager.decodeReferer(contactId, userId);
					ContactsManager.updateContactRequest(contactId, userId);
					PopulationsManagerBase.updateContactPopulation(contactId, userId, CommonUtil.getWebWriterId());
				}
			} catch (Exception e) {
				logger.warn("Problem with population removeUser event " + e);
			}

			//Create the user instance in the page and not in when creating user - its seems we creates the user instance twice (pixels problems)
//			if (!isMailAuth && (CommonUtil.isHebrewSkin(f.getSkinId()) ||CommonUtil.isMobile())) {
//			    ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
//			    ve.getValue(context.getELContext());
//			    ve.setValue(context.getELContext(), user);
//			}

			try {
				if (!isMailAuth) {
					if(combId != null && getMarketingSourceByCombinationId(combId).isSpecialMail()){
						///send google
						sendMailTemplateFile(Template.WELCOME_MAIL_GOOGLE_ID, CommonUtil.getWebWriterId(), user, null, new String(), new String(), null);
					} else {
						sendMailTemplateFile(Template.WELCOME_MAIL_ID, CommonUtil.getWebWriterId(), user, null, new String(), new String(), null);
					}
					
				} else {  // anyoption - Send Authentication email
					EmailAuthenticationManagerBase.sendActivationEmail(gm.getHomePageUrlHttps() + "jsp/loginAuth.jsf?", CommonUtil.getWebWriterId(), user);
					// Save to session
					EmailAuthentication ea = (EmailAuthentication) context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_EMAIL_AUTH, EmailAuthentication.class).getValue(context.getELContext());
					ea.setEmail(user.getEmail());
					ea.setUserId(user.getId());
				}
			} catch (Exception e2) {
				logger.error("can't send registration mail!! ", e2);
			}

			// next invest bonus
			if (f.getSkinId() == Skin.SKIN_ETRADER) {
				try {
					if (BonusDAOBase.isBonusUponRegistrationAvailable(con, Constants.REGISTRATION_BONUS_NEXT_INVEST)) {
						BonusUsers bonusUser = new BonusUsers();
						bonusUser.setBonusId(Constants.REGISTRATION_BONUS_NEXT_INVEST);
						bonusUser.setUserId(user.getId());
						bonusUser.setWriterId(CommonUtil.getWebWriterId());
						BonusManagerBase.insertBonusUser(bonusUser, user, CommonUtil.getWebWriterId(), 0, 0, false);
						// insert bonus to user
						user.setHaveRegistrationBonus(true);
		                ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
		                ve.getValue(context.getELContext());
		                ve.setValue(context.getELContext(), user);
//						context.getApplication().createValueBinding(Constants.BIND_USER).setValue(context, user);
					}
				} catch (Exception e3) {
					logger.error("can't insert bonus upon registration! ", e3);
				}
			} else if(isXmassBonusTime(f.getSkinId().intValue())) {  // AnyOption
				BonusUsers bonusUser = new BonusUsers();
				bonusUser.setBonusId(ConstantsBase.XMASS_BONUS_ID);
				bonusUser.setUserId(user.getId());
				bonusUser.setWriterId(CommonUtil.getWebWriterId());
				BonusManagerBase.insertBonusUser(bonusUser, user, CommonUtil.getWebWriterId(), 0, 0, false);
			}

			Contact regAttemp = (Contact)context.getApplication().createValueBinding(Constants.REGISTER_ATTEMPT).getValue(context);
			if (null != regAttemp && regAttemp.getId() != 0){
				regAttemp.setUserId(user.getId());
				ContactsManager.updateContact(regAttemp);
		    	UsersManagerBase.insertAffSub(user.getId(), regAttemp.getAff_sub1(), regAttemp.getAff_sub2(), regAttemp.getAff_sub3());
			}

			if (logger.isEnabledFor(Level.DEBUG)) {
				String ls = System.getProperty("line.separator");
				logger.log(Level.DEBUG, "Insert user successfully: " + ls
						+ user.toString() + ls);
			}

			if (!isMailAuth) {
			   HttpServletResponse resp = (HttpServletResponse) context.getExternalContext().getResponse();
			   MarketingTracker.checkExistCookieWhenInsertUser(req, resp, user.getEmail(), user.getMobilePhone(), user.getLandLinePhone(), user.getContactId(), user.getId());
			   MarketingTracker.insertMarketingTracker(req, resp, user.getContactId(), user.getId(), Constants.MARKETING_TRACKING_FULL_REG);
			   
			  // MarketingETS.etsMarketingActivity(req, resp, user.getId(), Constants.MARKETING_TRACKING_FULL_REG, null, null, user.getEmail(), user.getMobilePhone(), user.getLandLinePhone(), user.getContactId(), user.getIp());
			   LoginForm loginForm = new LoginForm();
			   loginForm.setUsername(user.getUserName());
			   loginForm.setPassword(user.getPassword());
			   loginForm.setFingerPrint(user.getFingerPrint());
			   loginForm.login();
			   user.setFirstTimeRegister(true);
			}
			
			
			/* 
			 * The skin id in the session/cookie may not be the same as the
			 * user's, so we should check the user skin.
			 */
			Skin userSkin = ApplicationDataBase.getSkinById(user.getSkinId());
			if (userSkin.isRegulated()) {
				// WC
				UserRegulationBase ur = new UserRegulationBase();
				ur.setUserId(user.getId());
				UserRegulationManager.getUserRegulation(ur);
				ur.setWcApproval(f.getTempWCApproval());
				ur.setApprovedRegulationStep(UserRegulationBase.REGULATION_WC_APPROVAL);
				ur.setWcComment("HARDCODED WC APPROVAL");
				ur.setWriterId(user.getWriterId());
				UserRegulationManager.updateWCApproval(ur);
				// suspend account due WC fail
				if (!ur.isWcApproval()) {
					ur.setSuspendedReasonId(UserRegulationBase.SUSPENDED_BY_WORLD_CHECK);
					ur.setComments("World check");
					UserRegulationManager.updateSuspended(ur);
				}
			}

		} catch (Exception e) {
			try {
				con.rollback();
			} catch (SQLException ie) {
				logger.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

		return true;

	}

	public static boolean duplicateGameUserToReal(String userName, String pass) throws SQLException {
		Connection con = null;
		try {
			String ls = System.getProperty("line.separator");
			FacesContext context = FacesContext.getCurrentInstance();
			HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();

			con = getConnection();

			UserBase user = new UserBase();
			UsersDAOBase.getByUserName(con, userName, user);

			if (user.getId() > 0) {  // user exists
					user.setUserName(user.getUserNameCalGame());
					user.setClassId(ConstantsBase.USER_CLASS_PRIVATE);
					user.setAcceptedTerms(false);
					user.setBalance(0);
					user.setTaxBalance(0);
					user.setTaxExemption(false);
					user.setReferenceId(new BigDecimal(user.getId()));
					// get first time visit from cookie
					Date timeFirstVisit = getTimeFirstVisit(req, true);
					logger.log(Level.DEBUG, "timeFirstVisit = " + timeFirstVisit.toString());
					user.setTimeFirstVisit(timeFirstVisit);
				try {
					UsersDAOBase.insert(con, user);
				} catch (Exception e2) {
					logger.error("can't insert new user!! ", e2);
				}

				try {
					UsersManagerBase.sendMailTemplateFile(Template.WELCOME_MAIL_ID, CommonUtil.getWebWriterId(),
							user, null, new String(), new String(), null);
				} catch (Exception e2) {
					logger.error("can't send registration mail!! ", e2);
				}
				logger.info("duplicate calcalist game user finished successfully. " + ls +
						user.toString() );
			} else { // user not found
				logger.debug("failed to deuplicate calcalist game user: user not exists!");
				return false;
			}
		} catch (SQLException e) {
			logger.log(Level.ERROR, "Error duplicate calcalist game user!" + e);
			throw e;
		} finally {
			closeConnection(con);
		}
		return true;
	}

	public static boolean updateUserDetails(UserBase user) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();

		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Update user details: " + ls
					+ user.toString());
		}
		if (CommonUtil.isHebrewSkin(user.getSkinId())) {
			
		    if (!CommonUtil.isParameterEmptyOrNull(user.getCityName())){
		        try {
	                CommonUtil.validateHebrewOnly(null, null, user.getCityName());
	            } catch (Exception e) {
	                FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
	                        CommonUtil.getMessage("error.city.nothebrew", null), null);
	                context.addMessage("personalForm:cityName", fm);
	                return false;
	            }
		    }		    

			if (user.getCityId() == 0) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage("error.register.city", null), null);
				context.addMessage("personalForm:cityName", fm);
				return false;
			}
		} else {

			try {
				if(user.getCityName()!=null && user.getCityName().length()>1) {
					CommonUtil.validateLettersOnly(null, null, user.getCityName());
				}
			} catch (Exception e) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage("error.city.nothebrew", null), null);
				context.addMessage("personalForm:cityId", fm);
				return false;
			}

			if (UsersManager.isEmailInUse(user.getEmail(), user.getId())) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage("error.register.email.inuse",	null), null);
				context.addMessage("personalForm:email", fm);
				return false;
			}

			ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);

			if (ap.getSkinId() == Skin.SKIN_GERMAN) {
                if ((null != user.getMobilePhone() && user.getMobilePhone().length() > 0) || (null != user.getLandLinePhone() && user.getLandLinePhone().length() > 0)) {
                    if (user.getIsContactBySMS() == 1) {
                        if (null == user.getMobilePhone() || user.getMobilePhone().length() == 0) {
                            logger.debug("failed registerform: no mobile phone and want contact by SMS");
                            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                    CommonUtil.getMessage("error.mandatory",
                                            null), null);
                            context.addMessage("personalForm:mobilePhone", fm);
                            return false;
                        }
                    }
                } else {
                    FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            CommonUtil.getMessage("error.mandatory",
                                    null), null);
                    if (user.getIsContactBySMS() == 1) {
                        logger.debug("failed registerform: no mobile phone and want contact by SMS");
                        context.addMessage("personalForm:mobilePhone", fm);
                    } else {
                        logger.debug("failed registerform: no mobile phone or landing phone");
                        context.addMessage("personalForm:landLinePhone", fm);
                    }
                    return false;
                }
            }
		}

		Connection con = getConnection();

		try {

			UsersDAOBase.update(con, user);

			if (logger.isEnabledFor(Level.DEBUG)) {
				logger.log(Level.DEBUG, "Updated successfully");
			}

		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
				CommonUtil.getMessage("user.update.success", null), null);
		context.addMessage(null, fm);

		return true;

	}

	public static boolean changePass(RegisterForm f, UserBase user)
			throws SQLException {
		return changePass(f.getOldPassword(), f.getPassword(), user, false);
	}

	public static boolean changePass(String oldPass, String newPass, UserBase user, boolean reset)
			throws SQLException {

		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Changing user password.  " + ls
					+ "User:" + user.getUserName() + ls + "new password:"
					+ "*****" + ls);
		}
		FacesContext context = FacesContext.getCurrentInstance();
		
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String reCaptch = request.getParameter(ConstantsBase.RECAPTCHA_REQUEST);		
		int platformID = ConstantsBase.PLATFORM_ID_ANYOPTION;	
		
		if (user.getSkinId() == Skin.SKIN_ETRADER) {
			platformID = ConstantsBase.PLATFORM_ID_ERTADER;
		}
		
		if (!reset) {
			
			try {
				if(!com.anyoption.common.util.Utils.validateCaptcha(reCaptch, null, platformID)) {
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
							CommonUtil.getMessage("golden.error.wrong.amount", null),
							null);
					context.addMessage("registerForm:recaptcha", fm);
					return false;
				}
			} catch (Exception e) {
				logger.error("cannot validate recaptcha", e);
				return false;
			}
			
			if ( user.getSkinId() == Skin.SKIN_ETRADER ) {
				if (!(user.getPassword().toUpperCase()).equals((oldPass).toUpperCase())) {

					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
							CommonUtil.getMessage("error.changePass.wrongPass", null),
							null);
					context.addMessage("registerForm:oldPassword", fm);
					return false;
				}
			} else {
				if (!(user.getPassword().equals((oldPass)))) { // in AO password is case-sensative

					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
							CommonUtil.getMessage("error.changePass.wrongPass", null),
							null);
					context.addMessage("registerForm:oldPassword", fm);
					return false;
				}
			}
		}
		if(user.getPassword().equals(newPass)){
			
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					CommonUtil.getMessage("CMS.resetPassword.text.error", null),
					null);
			context.addMessage("registerForm:password", fm);
			return false;
		}
		Connection con = getConnection();
		
		try {

			user.setPassword(newPass);
			UsersDAOBase.update(con, user);
			if (logger.isEnabledFor(Level.DEBUG)) {
				logger.log(Level.DEBUG, " password updated successfully! ");
			}

			try {
				ApplicationData gm = (ApplicationData) context.getApplication()
						.createValueBinding(Constants.BIND_APPLICATION_DATA)
						.getValue(context);
				sendMailTemplateFile(TEMPLATE_CONFIRM_MAIL_ID, CommonUtil.getWebWriterId(),
						user, null, new String(), new String(), null);
			} catch (Exception e2) {
				logger.error("can't send password confirm mail!! ", e2);
			}

		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}
		
		com.anyoption.common.managers.UsersManagerBase.updateUserNeedChangePass(user.getId(), false);
		user.setIsNeedChangePassword(false);
		try {
			ApplicationData.getUserFromSession().setIsNeedChangePassword(false);
		} catch (Exception e) {
			logger.error("Can't setIsNeedChangePassword in userSession ", e);
		}
		
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
				CommonUtil.getMessage("password.changed", null), null);
		context.addMessage(null, fm);
		
		return true;

	}

	public static void getUserByName(String name, UserBase user)
			throws SQLException {
		Connection con = getConnection();
		try {
			UsersDAOBase.getByUserName(con, name, user);
		} finally {
			closeConnection(con);
		}
	}

	public static long getBalance(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return UsersDAOBase.getBalance(con, userId);
		} finally {
			closeConnection(con);
		}
	}

	public static long getTaxBalance(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return UsersDAOBase.getTaxBalance(con, userId);
		} finally {
			closeConnection(con);
		}
	}

	public static void updateUser(UserBase user) throws SQLException {
		Connection con = getConnection();
		try {
			UsersDAOBase.update(con, user);
		} finally {
			closeConnection(con);
		}
	}

	public static void updateFreeSms(long userId, long freeSms) throws SQLException {
		Connection con = getConnection();
		try {
			UsersDAO.updateFreeSms(con, userId, freeSms);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Get combination instace by id
	 * @param combId
	 * @throws SQLException
	 */
	public static MarketingCombination getCombinationById(long combId) throws SQLException {
		Connection con = getConnection();
		try {
			return MarketingCombinationsDAOBase.getCombinationById(con, combId);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Get affiliate instace by Key
	 * @param affiliateKey
	 * @throws SQLException
	 */
	public static MarketingAffilate getAffiliateByKey(long affiliateKey) throws SQLException {
		Connection con = getConnection();
		try {
			return MarketingAffiliatesDAOBase.getAffiliateByKey(con, affiliateKey);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Get subAffiliate instace by Key
	 * @param subAffiliateKey
	 * @throws SQLException
	 */
	public static MarketingSubAffiliate getSubAffiliateByKey(long subAffiliateKey) throws SQLException {
		Connection con = getConnection();
		try {
			return MarketingAffiliatesDAOBase.getSubAffiliateByKey(con,subAffiliateKey);
		} finally {
			closeConnection(con);
		}
	}

	public static MarketingSource getMarketingSourceByCombinationId(long combId) throws SQLException{
		Connection conn = getConnection();
		try{
			return MarketingCombinationsDAOBase.getMarketingSourceByCombinationId(conn, combId);
		} finally {
			closeConnection(conn);
		}
	}
	
	/*
	/**
	 * This method will send the password email to user
	 */
	/* public static boolean sendResetPassword(String userName, String email, Date birthDate, long idNum, String resLink) throws Exception {

		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData gm = (ApplicationData) context.getApplication()
				.createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(
						context);

		Connection con = getConnection();
		long skinId=gm.getSkinId();
		try {
			userName = userName.toUpperCase();
			email = email.toUpperCase();

			HashMap map = null;
			if (gm.isCalcalistGame()) {
				map = UsersDAO.getPasswordDetails(con, userName + (ConstantsBase.CAL_GAME_USER_NAME_SUFFIX).toUpperCase(), skinId);
			} else {
				map = UsersDAO.getPasswordDetails(con, userName, skinId);
			}

			//String id = "";
			//String password = "";
			String realEmail = "";
			Date realBirthDate = null;
			long realIdNum = -1;

			if (map != null) {
				realEmail = ((String)map.get("email")).toUpperCase();
				realBirthDate = (Date)map.get("birthDate");
				String tin = (String)map.get("idNum");
				if (null != tin) {
					try {
						realIdNum = Long.valueOf(tin);
					} catch (Exception e) {
						// remains -1
					}
				}
			}
			if (map == null) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage("error.wrong.username", null),
						null);
				context.addMessage("passwordForm:userName", fm);
				return false;
			}

            if (!realEmail.equals(email)) {
                FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        CommonUtil.getMessage("error.wrong.email", null),
                        null);
                context.addMessage("passwordForm:email", fm);
                return false;
            }
            if (gm.isEtraderSkin()) {
            	if (idNum != realIdNum) {
 	            	FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
 	                        CommonUtil.getMessage("resetpass.error.wrong.idnum", null),
 	                        null);
 	                context.addMessage("passwordForm:idNum", fm);
 	                return false;
 	            }
            } else { // anyoption
            	if (!CommonUtil.getStartOfDay(realBirthDate).equals(CommonUtil.getStartOfDay(birthDate))) {
	            	FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
	                        CommonUtil.getMessage("error.wrong.birthDate", null),
	                        null);
	                context.addMessage("passwordForm:birthDay", fm);
	                return false;
	            }
            }

			try {
				User user = new User();
				user.setUserName(userName);

				if (!gm.isCalcalistGame()) {
					sendMailTemplateFile(TEMPLATE_PASSWORD_REMINDER_MAIL_ID, CommonUtil.getWebWriterId(), user, false, resLink);
				} else {
					user.setUserName(userName + (ConstantsBase.CAL_GAME_USER_NAME_SUFFIX).toUpperCase());
					sendMailTemplateFile(TEMPLATE_PASSWORD_REMINDER_MAIL_CAL_ID, CommonUtil.getWebWriterId(), user, true, resLink);
				}

			} catch (Exception e2) {
				logger.error("can't send password reminder mail!! ", e2);
			}

		} finally {
			closeConnection(con);
		}

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
				CommonUtil.getMessage("password.reset.link.success", null), null);
		context.addMessage(null, fm);

		return true;

	}*/

	public static boolean sendResetPassword(String email, String resLink, String phoneCode, String mobilePhone, boolean smsOrEmail) throws Exception {

		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData gm = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		Connection con = getConnection();
		long skinId = gm.getSkinId();
		ArrayList<User> usrArr = UsersDAO.getPasswordDetails(con, email, null, skinId);
		try {
				email = email.toUpperCase();
	
				if(!smsOrEmail) {
					 String senderName = "";
					 String senderNumber = "";
					 long key_value = 0;
					 long providerId = ConstantsBase.MOBIVATE_PROVIDER;//ConstantsBase.MBLOX_PROVIDER;
					 senderName = "anyoption";
	
					 try {
			            senderNumber = CountryManagerBase.getById(usrArr.get(0).getCountryId()).getSupportPhone();
			        } catch (Exception e) {
			            logger.error("Can't get country support number.", e);
		        	}
					key_value = usrArr.get(0).getId();
				 	SMSManagerBase.sendTextMessage(senderName, senderNumber, phoneCode + mobilePhone, resLink, key_value, SMSManagerBase.SMS_KEY_TYPE_USERID, providerId, SMS.DESCRIPTION_RESET_PASSWORD, usrArr.get(0).getMobileNumberValidation());
				} else {
					try {
						sendMailTemplateFile(TEMPLATE_PASSWORD_REMINDER_MAIL_ID, CommonUtil.getWebWriterId(), usrArr.get(0), false, resLink, null, null, null, null);
					} catch (Exception e2) {
						logger.error("can't send password reminder mail!! ", e2);
					}
				}
		} finally {
			closeConnection(con);
		}

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,	CommonUtil.getMessage("password.reset.link.success",null), null);
		context.addMessage("passwordForm:errorlogin", fm);
		UsersManager.insertPasswordRecoveryHistory(Constants.WRITER_WEB_ID, email, phoneCode + mobilePhone, STATUS_ID_SUCCESS);
		return true;

	}
	
	
	public static ArrayList<User> sendResetPassword(String email, long skinId)throws Exception {
		Connection con = null;
		ArrayList<User> arr = new ArrayList<User>();
		try {
			con = getConnection();
			arr = UsersDAO.getPasswordDetails(con, email, null, skinId);
		} finally {
			closeConnection(con);
		}
		return arr;
	}
//	public static Long getReferrerId(HttpServletRequest request)
//			throws SQLException {
//
//		Cookie[] cs = request.getCookies();
//		Cookie cookie = null;
//		if (cs != null) {
//			for (int i = 0; i < cs.length; i++) {
//				if (cs[i].getName().equals(Constants.REFERRER)) {
//					logger.log(Level.DEBUG, "found referrer cookie!");
//					cookie = cs[i];
//					break;
//				}
//			}
//		}
//		if (cookie != null && cookie.getValue() != null
//				&& !cookie.getValue().equals("")
//				&& cookie.getValue().indexOf("null") == -1) {
//			try {
//				return new Long(cookie.getValue());
//			} catch (Exception e) {
//				if (logger.isEnabledFor(Level.WARN)) {
//					logger.warn("Referrer cookie with strange value!", e);
//				}
//			}
//		}
//
//		HttpSession session = request.getSession();
//		String ref = (String) session.getAttribute(Constants.REFERRER);
//		logger.log(Level.DEBUG, "referrer: " + ref);
//		if (ref != null) {
//			return new Long(ref);
//		}
//
//		return null;
//
//	}

	/**
	 * Get combination id
	 * First search in the cookies, second search in the session
	 */
	public static Long getCombinationId(HttpServletRequest request)
			throws SQLException {

		Cookie[] cs = request.getCookies();
		Cookie cookie = null;
		if (cs != null) {
			for (int i = 0; i < cs.length; i++) {
				if (cs[i].getName().equals(Constants.COMBINATION_ID)) {
					logger.log(Level.DEBUG, "found combid cookie!");
					cookie = cs[i];
					//break;
				}
			}
		}
		if (cookie != null && cookie.getValue() != null
				&& !cookie.getValue().equals("")
				&& cookie.getValue().indexOf("null") == -1) {
			try {
				return new Long(cookie.getValue());
			} catch (Exception e) {
				if (logger.isEnabledFor(Level.WARN)) {
					logger.warn("combid cookie with strange value!", e);
				}
			}
		}

		HttpSession session = request.getSession();
		String combId = (String) session.getAttribute(Constants.COMBINATION_ID);
		logger.log(Level.DEBUG, "combid: " + combId);
		if (combId != null) {
			return new Long(combId);
		}

		return null;

	}

	/**
	 * Get combination id
	 * First search in the session, second search in the cookies,
	 */
	public static Long getSessionCombinationId(HttpServletRequest request) {

		HttpSession session = request.getSession();
		String combId = (String) session.getAttribute(Constants.COMBINATION_ID);
		logger.log(Level.DEBUG, "combid: " + combId);
		if (combId != null) {
			return new Long(combId);
		}

		Cookie[] cs = request.getCookies();
		Cookie cookie = null;
		if (cs != null) {
			for (int i = 0; i < cs.length; i++) {
				if (cs[i].getName().equals(Constants.COMBINATION_ID)) {
					logger.log(Level.DEBUG, "found combid cookie!");
					cookie = cs[i];
					//break;
				}
			}
		}
		if (cookie != null && cookie.getValue() != null
				&& !cookie.getValue().equals("")
				&& cookie.getValue().indexOf("null") == -1) {
			try {
				return new Long(cookie.getValue());
			} catch (Exception e) {
				if (logger.isEnabledFor(Level.WARN)) {
					logger.warn("combid cookie with strange value!", e);
				}
			}
		}

		return null;

	}

	/**
	 * Get dynamic paramter, search in cookies & session's attributes
	 * @param request
	 * @return
	 * @throws SQLException
	 */
	public static String getDynamicParam(HttpServletRequest request) throws SQLException {
		Cookie[] cs = request.getCookies();
		Cookie cookie = null;
		if ( null != cs ) {
			for ( int i = 0 ; i < cs.length ; i++ ) {
				if ( cs[i].getName().equals(Constants.DYNAMIC_PARAM)) {
					logger.info("found dp cookie!");
					cookie = cs[i];
					//break;
				}
			}
		}

		if (cookie != null && cookie.getValue() != null
				&& !cookie.getValue().equals("")
				&& cookie.getValue().indexOf("null") == -1) {
			try {
				String cookieValue = cookie.getValue();
				if (cookieValue.indexOf(Constants.GOOGLE_DP_KWORD) > -1) {  // use UTF-8 encoding for google dp that contains kword with international characters
					cookieValue = URLDecoder.decode(cookieValue, "UTF-8");
				}
				return new String(cookieValue);
			} catch (Exception e) {
				if (logger.isEnabledFor(Level.WARN)) {
					logger.warn("dp cookie with strange value!", e);
				}
			}
		}

		HttpSession session = request.getSession();
		String dp = (String) session.getAttribute(Constants.DYNAMIC_PARAM);
		if (null != dp) {
			logger.info("dp for session dp: " + dp);
			return dp;
		}

		return null;
	}

	/**
	 * Get special code, search in cookies & session's attributes
	 * @param request
	 * @return
	 * @throws SQLException
	 */
	public static String getSpecialCode(HttpServletRequest request) throws SQLException {

		Cookie[] cs = request.getCookies();
		Cookie cookie = null;
		if ( null != cs ) {
			for ( int i = 0 ; i < cs.length ; i++ ) {
				if ( cs[i].getName().equals(Constants.SPECIAL_CODE_PARAM)) {
					logger.info("found " + Constants.SPECIAL_CODE_PARAM + " cookie!");
					cookie = cs[i];
					//break;
				}
			}
		}

		if (null != cookie && null != cookie.getValue()
				&& !cookie.getValue().equals("")
				&& cookie.getValue().indexOf("null") == -1) {
			try {
				return new String(cookie.getValue());
			} catch (Exception e) {
				if (logger.isEnabledFor(Level.WARN)) {
					logger.warn(Constants.SPECIAL_CODE_PARAM + " cookie with strange value!", e);
				}
			}
		}

		HttpSession session = request.getSession();
		String sc = (String) session.getAttribute(Constants.SPECIAL_CODE_PARAM);
		if ( null != sc ) {
			logger.info(Constants.SPECIAL_CODE_PARAM + " for session " + Constants.SPECIAL_CODE_PARAM + ": " + cs);
			return sc;
		}

		return null;
	}

	public static String sendRequest(String requestStr, String url,
			Hashtable headers, boolean display) {

		logger.debug("sending request: url:" + url + " params:" + requestStr);

		System.setProperty("sun.net.client.defaultConnectTimeout", "60000");
		System.setProperty("sun.net.client.defaultReadTimeout", "60000");

		InputStream is = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		String response = "";
		String cookiValue = "";

		try {
			byte[] data = requestStr.getBytes("UTF-8");
			URL u = new URL(url);
			HttpURLConnection httpCon = (HttpURLConnection) u.openConnection();

			HttpURLConnection.setFollowRedirects(false);
			httpCon.setInstanceFollowRedirects(false);

			httpCon.setDoOutput(true);
			httpCon.setDoInput(true);

			httpCon.setRequestMethod("GET");
			httpCon.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			httpCon.setRequestProperty("Content-Length", String
					.valueOf(data.length));

			if (null != headers)
			{
				for (Enumeration e = headers.keys(); e.hasMoreElements();)
				{
					String key = (String) e.nextElement();
					httpCon.setRequestProperty(key, (String) headers.get(key));
					logger.debug("Headers: " + key + " - "
							+ (String) headers.get(key));
				}
			}
			else
			{
				logger.debug("No Headers!");
			}

			OutputStream os = httpCon.getOutputStream();
			os.write(data, 0, data.length);
			os.flush();
			try {
				os.close();
			} catch (Exception e) {
				logger.log(Level.ERROR, "Can't close output stream.", e);
			}
			if (logger.isEnabledFor(Level.DEBUG)) {
				logger.log(Level.DEBUG, "Request sent.");
			}
			int respCode = httpCon.getResponseCode();
			if (logger.isEnabledFor(Level.DEBUG)) {
				logger.log(Level.DEBUG, "ResponseCode: " + respCode);
			}
			is = httpCon.getInputStream();
			// isr = new InputStreamReader(is, "ISO-8859-8");
			isr = new InputStreamReader(is, "UTF-8");
			br = new BufferedReader(isr);
			StringBuffer xmlResp = new StringBuffer();
			String line = null;
			while ((line = br.readLine()) != null) {
				xmlResp.append(line);
			}
			response = xmlResp.toString();
			if (logger.isEnabledFor(Level.DEBUG) && display) {
				logger.log(Level.DEBUG, "Response received.");
				logger.log(Level.DEBUG, response);
			}

			// get the session ID

			String key = "";
			if (httpCon != null) {
				for (int i = 1; (key = httpCon.getHeaderFieldKey(i)) != null; i++) {
					if (logger.isEnabledFor(Level.DEBUG)) {
						logger.debug(httpCon.getHeaderFieldKey(i) + ": "
								+ httpCon.getHeaderField(key));
					}
					if (key.equalsIgnoreCase("set-cookie")) {
						cookiValue = httpCon.getHeaderField(key);
						if (headers.size() == 0) {
							headers.put("Cookie", cookiValue);
						}
					}
				}
			}

		} catch (IOException ioe) {

			logger.log(Level.ERROR, "No response!!!", ioe);

		} finally {
			if (null != br) {
				try {
					br.close();
				} catch (Exception e) {
					logger.log(Level.ERROR, "Can't close the buffered reader.",
							e);
				}
			}
			if (null != isr) {
				try {
					isr.close();
				} catch (Exception e) {
					logger.log(Level.ERROR,
							"Can't close the input stream reader.", e);
				}
			}
			if (null != is) {
				try {
					is.close();
				} catch (Exception e) {
					logger.log(Level.ERROR, "Can't close the input stream.", e);
				}
			}

		}
		return response;
	}


//	// new login
//
//	public static String login(String username, String password) {
//
//		boolean succeeded = true;
//		Connection conn = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//		ResultSet rs2 = null;
//		int isActive = 0;
//		int failedCount = -1;
//
//		FacesContext context = FacesContext.getCurrentInstance();
//
//
//		try {
//
//			if (logger.isEnabledFor(Level.DEBUG)) {
//				logger.log(Level.DEBUG, "User name <" + username
//						+ "> trying to login");
//			}
//
//			conn = getConnection();
//			// then check the username and active status
//			pstmt = conn
//					.prepareStatement("SELECT is_active, failed_count FROM users WHERE upper(user_name) = ?");
//			pstmt.setString(1, username.toUpperCase());
//			rs = pstmt.executeQuery();
//			if (rs.next()) { // user exist
//				isActive = rs.getInt("is_active");
//				failedCount = rs.getInt("failed_count");
//				if (isActive == 1) {
//					// check password
//					pstmt = conn
//							.prepareStatement("SELECT user_name "
//									+ "FROM users "
//									+ "WHERE upper(user_name) = ? AND password = ? AND is_active = 1");
//					pstmt.setString(1, username.toUpperCase());
//					pstmt.setString(2, AESUtil.encrypt(new String(password).toUpperCase()));
//					rs2 = pstmt.executeQuery();
//					close
//					if (rs2.next()) {
//						// user and password are OK
//						succeeded = true;
//						username = rs2.getString("user_name");
//						// initialize login attempts to zero
//						pstmt = conn
//								.prepareStatement("UPDATE USERS SET LAST_FAILED_TIME=NULL, FAILED_COUNT=0,time_last_login=sysdate WHERE UPPER(USER_NAME) = ?");
//						pstmt.setString(1, username.toUpperCase());
//						pstmt.executeQuery();
//
//						User user = (User) context.getApplication().createValueBinding(
//								Constants.BIND_USER).getValue(context);
//
//						UsersManager.getUserByName(username, user);
//
//						logger.debug("User "+username+" loaded on: " + new Date());
//
//						if (logger.isInfoEnabled()) {
//							logger.log(Level.INFO, "User '" + username
//									+ "' succeeded login");
//						}
//					} else {
//						// password failed
//						succeeded = false;
//						if (failedCount == 4) { // this is the fourth try
//							// lock the user by changing is_active
//							pstmt = conn
//									.prepareStatement("UPDATE USERS "
//											+ "SET LAST_FAILED_TIME=SYSDATE, FAILED_COUNT=FAILED_COUNT+1, IS_ACTIVE=0 "
//											+ "WHERE UPPER(USER_NAME) = ?");
//							pstmt.setString(1, username.toUpperCase());
//							pstmt.executeQuery();
//							if (logger.isInfoEnabled()) {
//								logger
//										.log(
//												Level.INFO,
//												"User '"
//														+ username
//														+ "' failed to login and changed to not active");
//							}
//						} else {
//							// update failed count
//							pstmt = conn
//									.prepareStatement("UPDATE USERS "
//											+ "SET LAST_FAILED_TIME=SYSDATE, FAILED_COUNT=FAILED_COUNT+1 "
//											+ "WHERE UPPER(USER_NAME) = ?");
//							pstmt.setString(1, username.toUpperCase());
//							pstmt.executeQuery();
//							if (logger.isInfoEnabled()) {
//								logger
//										.log(
//												Level.INFO,
//												"User '"
//														+ username
//														+ "' failed to login, 'failed count' updated");
//							}
//						}
//					}
//				} else { // user exist but not active
//					// update failed count
//					succeeded = false;
//					pstmt = conn
//							.prepareStatement("UPDATE USERS "
//									+ "SET LAST_FAILED_TIME=SYSDATE, FAILED_COUNT=FAILED_COUNT+1 "
//									+ "WHERE UPPER(USER_NAME) = ?");
//					pstmt.setString(1, username.toUpperCase());
//					pstmt.executeQuery();
//					if (logger.isInfoEnabled()) {
//						logger.log(Level.INFO, "User '" + username
//								+ "' is not active, 'failed count' updated");
//					}
//				}
//			} else {
//				// users doesn't exist
//				succeeded = false;
//				if (logger.isInfoEnabled()) {
//					logger.log(Level.INFO, "User '" + username
//							+ "' doesn't exist!");
//				}
//			}
//
//		} catch (Exception e) {
//			succeeded = false;
//			logger.log(Level.ERROR, "Exception while login user: "
//					+ e.toString());
//		} finally {
//			try {
//				rs2.close();
//				rs.close();
//			} catch (Exception e) {
//			}
//			try {
//				pstmt.close();
//			} catch (Exception e) {
//			}
//			try {
//				closeConnection(conn);
//			} catch (Exception e) {
//			}
//		}
//
//		if (!succeeded) {
//
//			logger.log(Level.INFO, "Login failed!");
//
//			HttpSession session = (HttpSession) context.getExternalContext()
//					.getSession(true);
//			int numberOfTries = 0;
//
//			String loginTries = (String) session.getAttribute("logintries");
//			if (loginTries != null) {
//				try {
//					numberOfTries = Integer.parseInt(loginTries);
//					if (numberOfTries > Constants.MAX_ATTEMPTS_WITHOUT_USER) { // more
//						// then
//						// four
//						// tries
//
//						return CommonUtil.getMessage("header.login.locksessionerror", null);
//
//					}
//				} catch (NumberFormatException e) {
//					numberOfTries = 0;
//					session.setAttribute("logintries", String
//							.valueOf(numberOfTries));
//				}
//			}
//
//			if (failedCount == -1) {
//				numberOfTries++;
//				session.setAttribute("logintries", String.valueOf(numberOfTries));
//				return CommonUtil.getMessage("header.login.usersessionerror",null);
//			} else if ((failedCount < 4) && (isActive == 1)) {
//				return CommonUtil.getMessage("header.login.error", null);
//			} else if ((failedCount == 4) && (isActive == 1)) {
//				return CommonUtil.getMessage("header.login.lasterror", null);
//			} else {
//				return CommonUtil.getMessage("header.login.lockerror", null);
//			}
//
//		}
//		return "";
//
//	}

    /**
     * Set value to users.is_next_invest_on_us flag.
     *
     * @param userId
     * @param isNextInvestOnUs
     * @throws SQLException
     */
    public static void setIsNextInvestOnUs(long userId, int isNextInvestOnUs) throws SQLException {
        Connection conn = getConnection();
        try {
            UsersDAO.setIsNextInvestOnUs(conn, userId, isNextInvestOnUs);
        } finally {
            closeConnection(conn);
        }
    }

    /**
     * Set value to users.is_next_invest_on_us flag.
     *
     * @param userId
     * @param isNextInvestOnUs
     * @throws SQLException
     */
    public static boolean getIsNextInvestOnUs(long userId) throws SQLException {
        Connection conn = getConnection();
        try {
            return BonusDAOBase.hasNextInvestOnUs(conn, userId) > 0;
        } finally {
            closeConnection(conn);
        }
    }

    /**
     * Get docs required field of the user
     * if flag true - user must to send us documents for deposit
     * @param userName
     * 		user name
     * @return
     * 		if true then user need to send documents
     * @throws SQLException
     */
    public static boolean getIsDocsRequired(String userName) throws SQLException {
        Connection con = getConnection();
        try {
        	int flag = UsersDAO.getIsDocsRequired(con, userName);
        	if ( flag == 1 ) {
        		return true;
        	}
        } finally {
            closeConnection(con);
        }
        return false;
    }


    /**
     * Get Ad name
     * @param id
     * 		ad id
     * @return
     * 		Ad name
     * @throws SQLException
     */
    public static String getAdNameById(long id) throws SQLException {

    	Ad a = new Ad();
    	String name = null;
    	Connection con = getConnection();

        try {
        	a = AdsDAO.getById(con, id);
        	if ( null != a ) {
        		name = a.getName();
        	}
        } finally {
            closeConnection(con);
        }

        return name;
    }

    public static long getUserIdByPhoneEmail(String phone, String email, long countryId) throws SQLException {
    	Connection con = getConnection();
    	try {
        	return UsersDAO.getUserByPhoneEmail(con, phone, email, countryId);
        } finally {
            closeConnection(con);
        }
    }

    public static String getUserNameByEmail(String email, long skinId) throws SQLException{
    	Connection con = getConnection();
    	try{
    		return UsersDAO.getUserNameByEmail(con, email,  skinId);
    	} finally {
    		closeConnection(con);
    	}
    }

//    public static Investment getLastInvest(long userId) throws SQLException {
//        Investment i = null;
//    	Connection conn = getConnection();
//        try {
//        	i = UsersDAO.getLastInvest(conn, userId);
//        } finally {
//            closeConnection(conn);
//        }
//        return i;
//    }

    public static boolean hasTheSameInvestmentInTheLastSec(long userId, long oppId, long typeId, InvestmentRejects invRej) throws SQLException {
        boolean has = false;
        Connection conn = getConnection();
        try {
            has = UsersDAO.hasTheSameInvestmentInTheLastSec(conn, userId, oppId, typeId,invRej);
        } finally {
            closeConnection(conn);
        }
        return has;
    }

    /**
     * Check if there is disabled record for the specified user / market covering the current moment.
     *
     * @param userId
     * @param marketId
     * @return
     * @throws SQLException
     */
    public static boolean isUserMarketDisabled(long userId, long marketId, int scheduled, boolean isDev3) throws SQLException {
        boolean b = false;
        Connection conn = getConnection();
        try {
            b = UsersDAO.isUserMarketDisabled(conn, userId, marketId, scheduled, isDev3);
        } finally {
            closeConnection(conn);
        }
        return b;
    }

    /**
     * Get combination id by Id
     * @param adId
     * @return
     * @throws SQLException
     */
    public static String getCombIdByAdId(String adId) throws SQLException {
    	Connection conn = getConnection();
    	String combinationId = null;
    	long combId = 0;
        try {
            combId = MarketingCombinationsDAOBase.getCombinationIdByAd(conn, Long.valueOf(adId));
            if ( combId > 0 ) {
            	combinationId = String.valueOf(combId);
            }
        } catch (Exception e) {
        	logger.warn("Problem to get combinationId by adId: " + adId);
		} finally {
            closeConnection(conn);
        }

        if ( CommonUtil.isParameterEmptyOrNull(combinationId)) {  // set default
        	FacesContext context = FacesContext.getCurrentInstance();
        	ApplicationData a = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
        	combinationId = String.valueOf(ApplicationData.getSkinById(a.getSkinId()).getDefaultCombinationId());
        	logger.warn("Problem to get combinationId by adId, take default: " + combinationId);
        }

        return combinationId;
    }

    public static boolean validatePass(UserBase user,String password) throws SQLException {

		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "validating user password.  " + ls
					+ "User:" + user.getUserName() + ls + "inserted password:"
					+ "*****" + ls);
		}

		if ( user.getSkinId() == Skin.SKIN_ETRADER ) {
			if (password == null || !(user.getPassword().toUpperCase()).equals((password.toUpperCase()))) {
				return false;
			}
		}else{
			if (password == null || !(user.getPassword().equals(password))) { // in AO password is case-sensative
				return false;
			}
		}

		return true;

    }

	/**
	 * Get First Visit Time
	 * get this date from the cookies, if doesn't exist get today's date
	 */
	public static Date getTimeFirstVisit(HttpServletRequest request, boolean isBasic)
			throws SQLException {

		Cookie[] cs = request.getCookies();
		Cookie timeCookie = null;
		if (cs != null) {
			for (int i = 0; i < cs.length; i++) {
				if (cs[i].getName().equals(Constants.TIME_FIRST_VISIT_COOKIE)) {
					logger.log(Level.DEBUG, "found firstTimeVisit cookie!");
					timeCookie = cs[i];
					//break;
				}
			}
		}
		if (timeCookie != null && timeCookie.getValue() != null
				&& !timeCookie.getValue().equals("")
				&& timeCookie.getValue().indexOf("null") == -1) {
			String[] dateFormatPatterns = {"HH:mm:ss dd/MM/yy", "MM/dd/yy hh:mm a", "HH:mm dd/MM/yy"};
			for (String dateFormatPattern : dateFormatPatterns) {
				try {
					//SimpleDateFormat myDateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yy");
					Date firstVisitTime = new SimpleDateFormat(dateFormatPattern).parse(timeCookie.getValue());
					return firstVisitTime;
				} catch (Exception e) {
					if (logger.isEnabledFor(Level.WARN)) {
						logger.warn("firstTimeVisit cookie with strange value!");
					}
				}
			}
		}
		if (isBasic) {
			return new Date();
		} else {
			return null;
		}

	}

	/**
	 * Get liveperson session key
	 * @param request
	 * @return
	 * @throws SQLException
	 */
	public static String getLPSessionKey(HttpServletRequest request) throws SQLException {
		Cookie[] cs = request.getCookies();
		Cookie cookie = null;
		if (cs != null) {
			for (int i = 0; i < cs.length; i++) {
				if (cs[i].getName().equals(Constants.LIVEPERSON_CHAT_ID)) {
					logger.log(Level.DEBUG, "found lpChatId cookie!");
					cookie = cs[i];
					//break;
				}
			}
		}
		if (cookie != null && cookie.getValue() != null
				&& !cookie.getValue().equals("")
				&& cookie.getValue().indexOf("null") == -1) {
			try {
				return cookie.getValue();
			} catch (Exception e) {
				if (logger.isEnabledFor(Level.WARN)) {
					logger.warn("lpChatId cookie with strange value!", e);
				}
			}
		}
		return null;
	}

	/**
	 * Insert admin deposit
	 * This function written for calcalist game users that should get virtual money
	 * @return
	 * @throws SQLException
	 */
	public static boolean insertAdminDeposit(User user, int transTypeId, String utcOffset, long amount, long writerId, long loginId) throws SQLException {

		if (logger.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.INFO, "Insert Admin deposit: " + ls + "user:"
					+ user.getUserName() + " amount: " + amount);
		}
        Transaction tran = null;
		Connection con = getConnection();
		try {
			con.setAutoCommit(false);
			tran = new Transaction();
			tran.setAmount(CommonUtil.calcAmount(amount));
			tran.setCurrency(user.getCurrency());
			tran.setComments("virtual game money");
			tran.setCreditCardId(null);
			tran.setDescription(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(writerId);
			tran.setChequeId(null);
			tran.setTimeSettled(new Date());
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated(utcOffset);
			tran.setUtcOffsetSettled(utcOffset);
			tran.setTypeId(transTypeId);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
			tran.setReceiptNum(null);

			UsersDAO.addToBalance(con, user.getId(), CommonUtil.calcAmount(amount));
			TransactionsDAOBase.insert(con, tran);
			GeneralDAO.insertBalanceLog(con, writerId,
					tran.getUserId(), Constants.TABLE_TRANSACTIONS, tran
							.getId(), Constants.LOG_BALANCE_ADMIN_DEPOSIT, utcOffset);

			con.commit();

			if (logger.isEnabledFor(Level.INFO)) {
				logger.log(Level.INFO, "Admin deposit inserted successfully!");
			}
		} catch (SQLException e) {
			logger.log(Level.ERROR, "insertAdminDeposit: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				logger.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

        return true;
	}

	public static void insertFirstVisitRecord(long userId, long combId, String dp) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			UsersDAO.insertUserFirstVisit(con, userId, combId, dp);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Get user MailBox
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<MailBoxUser> getUserMailBox(long userId, String searchFilter) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return UsersDAO.getUserMailBox(conn, userId, searchFilter);
		} finally {
			closeConnection(conn);
		}
	}

	public static void updateEmailsByUserId(long userId, long statusId, String emails) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			UsersDAO.updateEmailsByUserId(con, userId, emails, statusId);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Get unRead emails by userId
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static long getUnreadEmailsCount(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return UsersDAO.getUnreadEmailsCount(con, userId);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Get the first popup email that we should display
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static MailBoxUser getUserPopUpEmail(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return MailBoxUsersDAOBase.getUserPopUpEmail(con, userId);
		} finally {
			closeConnection(con);
		}
	}


	/**
	 * Update popup type id for specific email
	 * @param emailId
	 * @param popupTypeId
	 * @throws SQLException
	 */
	public static void updatePopTypeId(long emailId, long popupTypeId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			MailBoxUsersDAOBase.updatePopTypeId(con, emailId, popupTypeId);
		} finally {
			closeConnection(con);
		}
	}


	/**
	 * Get click id, search in cookies & session's attributes
	 * @param request
	 * @return
	 * @throws SQLException
	 */
	public static String getClickId(HttpServletRequest request) throws SQLException {

		Cookie[] cs = request.getCookies();
		Cookie cookie = null;
		if ( null != cs ) {
			for ( int i = 0 ; i < cs.length ; i++ ) {
				if ( cs[i].getName().equals(Constants.CLICK_ID)) {
					logger.info("found clickId cookie!");
					cookie = cs[i];
					//break;
				}
			}
		}

		if (cookie != null && cookie.getValue() != null
				&& !cookie.getValue().equals("")
				&& cookie.getValue().indexOf("null") == -1) {
			try {
				return new String(cookie.getValue());
			} catch (Exception e) {
				if (logger.isEnabledFor(Level.WARN)) {
					logger.warn("clickId cookie with strange value!", e);
				}
			}
		}

		HttpSession session = request.getSession();
		String clickId = (String) session.getAttribute(Constants.CLICK_ID);
		if ( null != clickId ) {
			logger.info("clickId for session clickId: " + clickId);
			return clickId;
		}

		return null;
	}

	/**
	 * Get customer visit type cookie value (cvt)
	 * @param request
	 * @return
	 * @throws SQLException
	 */
	public static String getCustomerVisitType(HttpServletRequest request) throws SQLException {
		Cookie[] cs = request.getCookies();
		Cookie cookie = null;
		if ( null != cs ) {
			for ( int i = 0 ; i < cs.length ; i++ ) {
				if ( cs[i].getName().equals(Constants.CUSTOMER_VISIT_TYPE)) {
					logger.info("found cvt cookie!");
					cookie = cs[i];
					//break;
				}
			}
		}
		if (cookie != null && cookie.getValue() != null
				&& !cookie.getValue().equals("")
				&& cookie.getValue().indexOf("null") == -1) {
			try {
				return new String(cookie.getValue());
			} catch (Exception e) {
				if (logger.isEnabledFor(Level.WARN)) {
					logger.warn("cvt cookie with strange value!", e);
				}
			}
		}
		return null;
	}

	/**
	 * Get .....
	 * @param request
	 * @return
	 * @throws SQLException
	 */
	public static String getNonDepositTimeCookie(HttpServletRequest request) throws SQLException {
		Cookie[] cs = request.getCookies();
		Cookie cookie = null;
		if ( null != cs ) {
			for ( int i = 0 ; i < cs.length ; i++ ) {
				if ( cs[i].getName().equals(Constants.NON_DEPOSITOR_POPUP_TIME)) {
					logger.info("found non deposit time cookie cookie!");
					cookie = cs[i];
					//break;
				}
			}
		}
		if (cookie != null && cookie.getValue() != null
				&& !cookie.getValue().equals("")
				&& cookie.getValue().indexOf("null") == -1) {
			try {
				return new String(cookie.getValue());
			} catch (Exception e) {
				if (logger.isEnabledFor(Level.WARN)) {
					logger.warn("non deposit cookie with strange value!", e);
				}
			}
		}
		return null;
	}

	public static String getHttpReferer(HttpServletRequest request) throws SQLException, UnsupportedEncodingException {
		Cookie[] cs = request.getCookies();
		Cookie cookie = null;
		if (null != cs) {
			for (int i = 0; i < cs.length; i++) {
				if ( cs[i].getName().equals(Constants.HTTP_REFERE_COOKIE)) {
					logger.info("found http referer cookie!");
					cookie = cs[i];
					//break;
				}
			}
		}
		if (cookie != null && cookie.getValue() != null
				&& !cookie.getValue().equals("")
				&& cookie.getValue().indexOf("null") == -1) {
			try {
				return new String(URLDecoder.decode(cookie.getValue(), "UTF-8"));
			} catch (Exception e) {
				if (logger.isEnabledFor(Level.WARN)) {
					logger.warn("http referer cookie with strange value!", e);
				}
			}
		}
		HttpSession session = request.getSession();
		String httpReferer = (String) session.getAttribute(Constants.HTTP_REFERE);
		if (null != httpReferer) {
			httpReferer = URLDecoder.decode(httpReferer, "UTF-8");
			logger.info("http referer: " + httpReferer);
			return httpReferer;
		}
		return null;
	}

	public static String getHttpRefererCookie(HttpServletRequest request) throws SQLException, UnsupportedEncodingException {
		Cookie[] cs = request.getCookies();
		Cookie cookie = null;
		if (null != cs) {
			for (int i = 0; i < cs.length; i++) {
				if ( cs[i].getName().equals(Constants.HTTP_REFERE_COOKIE)) {
					logger.info("found http referer cookie!");
					cookie = cs[i];
					//break;
				}
			}
		}
		if (cookie != null && cookie.getValue() != null
				&& !cookie.getValue().equals("")
				&& cookie.getValue().indexOf("null") == -1) {
			try {
				return new String(URLDecoder.decode(cookie.getValue(), "UTF-8"));
			} catch (Exception e) {
				if (logger.isEnabledFor(Level.WARN)) {
					logger.warn("http referer cookie with strange value!", e);
				}
			}
		}
		return null;
	}

	/**
	 * Get DFA placement id
	 * @param request
	 * @return
	 * @throws SQLException
	 * @throws UnsupportedEncodingException
	 */
	public static String getDFAPlacementId(HttpServletRequest request) {
		Cookie[] cs = request.getCookies();
		Cookie cookie = null;
		if (null != cs) {
			for (int i = 0; i < cs.length; i++) {
				if (cs[i].getName().equals(Constants.DFA_PLACEMENT_ID_VALUE)) {
					logger.info("found dfa placement cookie!");
					cookie = cs[i];
					//break;
				}
			}
		}
		if (cookie != null && cookie.getValue() != null
				&& !cookie.getValue().equals("")
				&& cookie.getValue().indexOf("null") == -1) {
			try {
				return new String(cookie.getValue());
			} catch (Exception e) {
				if (logger.isEnabledFor(Level.WARN)) {
					logger.warn("dfa cookie with strange value!", e);
				}
			}
		}
		HttpSession session = request.getSession();
		String dfa = (String) session.getAttribute(Constants.DFA_PLACEMENT_ID_VALUE);
		if (null != dfa) {
			logger.info("dfa placementId: " + dfa);
			return dfa;
		}
		return null;
	}

	/**
	 * Get DFA creative id
	 * @param request
	 * @return
	 * @throws SQLException
	 * @throws UnsupportedEncodingException
	 */
	public static String getDFACreativeId(HttpServletRequest request) {
		Cookie[] cs = request.getCookies();
		Cookie cookie = null;
		if (null != cs) {
			for (int i = 0; i < cs.length; i++) {
				if (cs[i].getName().equals(Constants.DFA_CREATIVE_ID_VALUE)) {
					logger.info("found dfa creativeId cookie!");
					cookie = cs[i];
					//break;
				}
			}
		}
		if (cookie != null && cookie.getValue() != null
				&& !cookie.getValue().equals("")
				&& cookie.getValue().indexOf("null") == -1) {
			try {
				return new String(cookie.getValue());
			} catch (Exception e) {
				if (logger.isEnabledFor(Level.WARN)) {
					logger.warn("dfa creativeId cookie with strange value!", e);
				}
			}
		}
		HttpSession session = request.getSession();
		String dfa = (String) session.getAttribute(Constants.DFA_CREATIVE_ID_VALUE);
		if (null != dfa) {
			logger.info("dfa creativeId: " + dfa);
			return dfa;
		}
		return null;
	}

	/**
	 * Get DFA macro
	 * @param request
	 * @return
	 * @throws SQLException
	 * @throws UnsupportedEncodingException
	 */
	public static String getDFAMacro(HttpServletRequest request) {
		Cookie[] cs = request.getCookies();
		Cookie cookie = null;
		if (null != cs) {
			for (int i = 0; i < cs.length; i++) {
				if (cs[i].getName().equals(Constants.DFA_MACRO_VALUE)) {
					logger.info("found dfa macro cookie!");
					cookie = cs[i];
					//break;
				}
			}
		}
		if (cookie != null && cookie.getValue() != null
				&& !cookie.getValue().equals("")
				&& cookie.getValue().indexOf("null") == -1) {
			try {
				return new String(cookie.getValue());
			} catch (Exception e) {
				if (logger.isEnabledFor(Level.WARN)) {
					logger.warn("dfa macro cookie with strange value!", e);
				}
			}
		}
		HttpSession session = request.getSession();
		String dfa = (String) session.getAttribute(Constants.DFA_MACRO_VALUE);
		if (null != dfa) {
			logger.info("dfa macro: " + dfa);
			return dfa;
		}
		return null;
	}
	public static boolean isXmassBonusTime(int skinId){
		if (skinId == Skin.SKINS_DEFAULT ||
				skinId == Skin.SKIN_GERMAN ||
				skinId == Skin.SKIN_SPAIN ||
				skinId == Skin.SKIN_ITALIAN ||
				skinId == Skin.SKIN_RUSSIAN){
			GregorianCalendar gc = new GregorianCalendar();
			gc.set(GregorianCalendar.HOUR_OF_DAY, 0);
			gc.set(GregorianCalendar.MINUTE, 0);
			gc.set(GregorianCalendar.SECOND, 0);
			gc.set(GregorianCalendar.YEAR, 2010);
			gc.set(GregorianCalendar.MONTH, GregorianCalendar.DECEMBER);
			gc.set(GregorianCalendar.DAY_OF_MONTH, 7);
			Date startDate = gc.getTime(); // 07/12/2010
			gc.add(GregorianCalendar.DAY_OF_MONTH, 25);
			Date endDate = gc.getTime();   // 01/01/2011

			Date now = new Date();

			if (now.after(startDate) && now.before(endDate)){
				return true;
			}
		}

		return false;

	}

    /**
     * check if we already sent email to this user with the current email
     * @param con
     * @param id user id
     * @param email user email
     * @return true if we sent email to this user or false
     * @throws SQLException
     */
    public static boolean isSentActivationEmail(long id, String email) throws SQLException{
        Connection con = null;
        boolean isSent = false;
        try {
            con = getConnection();
            isSent = UsersDAO.isSentActivationEmail(con, id,  email);
        } finally {
            closeConnection(con);
        }
        return isSent;
    }

    public static void increaseEmailRefused(long userId) throws SQLException {
    	Connection con = null;
    	try {
            con = getConnection();
            UsersDAO.increaseEmailRefused(con, userId);
    	} finally {
            closeConnection(con);
        }
    }

    public static ArrayList<Transaction> getDepositsForPixlRunning(long userId) throws SQLException {
    	Connection con = null;
    	try {
            con = getConnection();
            return UsersDAO.getDepositsForPixlRunning(con, userId);
    	} finally {
            closeConnection(con);
        }
    }

    public static Transaction getFirstDepositForPixlRunning(long userId) throws SQLException {
    	Connection con = null;
    	try {
            con = getConnection();
            return UsersDAO.getFirstDepositForPixlRunning(con, userId);
    	} finally {
            closeConnection(con);
        }
    }

    /**
     * Validate contact fields from landing pages
     * @param form
     * @return
     * @throws SQLException
     */
	public static boolean validateContactUsLandingForm(ContactUsForm form, boolean checkEmail)
		throws SQLException {

		FacesContext context = FacesContext.getCurrentInstance();
		boolean res = true;
		Connection con = getConnection();
		try {

			if (checkEmail && !CommonUtil.isEmailValidUnix(form.getEmail())) {
				res = false;
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage("error.banner.contactme.email",	null), null);
				context.addMessage("contactForm:email", fm);
			}

			if (checkEmail && !CommonUtil.isHebrewUserSkin() && UsersDAOBase.isEmailInUse(con, form.getEmail(), 0)) {
				res = false;
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage("error.register.email.inuse",	null), null);
				context.addMessage("contactForm:email", fm);
			}


		} finally {
			closeConnection(con);
		}
		return res;
	}
	/**
	 * Get attachment of the mailBox
	 * @param attId attachmentId
	 * @param os
	 * @throws SQLException
	 * @throws IOException
	 */
	public static MailBoxUserAttachment getMailBoxAttchment(long attId) throws SQLException, IOException {
		Connection conn = null;
		try {
			conn = getConnection();
			return MailBoxUsersDAO.getMailBoxAttachment(conn, attId);
		} finally {
            closeConnection(conn);
        }
	}
	
	/*
	 * take the http_referer from the contact, decode it
	 * and update the user field decoded_source_query
	 */
	public static void decodeReferer(long contactId, long userId) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			Contact c = ContactsDAO.getById(conn, contactId);
			String http_referer = c.getHttpReferer();
			if(http_referer != null) {
				RefererDecoderManager rdm = new RefererDecoderManager(true);
		
				String query = rdm.decodeAll(http_referer);
				if(query != null && query.length()>0){
					// update user 
					UsersDAOBase.updateUserQuery(conn, userId, query);
				}
			}
		} finally {
            closeConnection(conn);
        }
	}


	/**
	 * Check for city coordinates
	 * @param u
	 * @return
	 * @throws SQLException
	 */
	public static boolean getUserCityCoordinates(User u, String countryName) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return UsersDAO.getUserCityCoordinates(con, u, countryName);
		} finally {
			closeConnection(con);
		}
	}

	public static List<com.anyoption.common.beans.File> getUserFilesByType(long userId, long fileType, Locale locale) {
		Connection con = null;
		try {
			con = getConnection();
			return FilesDAO.getUserFilesByType(con, userId, fileType, locale);
		} catch (	SQLException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
					| NoSuchPaddingException | InvalidAlgorithmParameterException e) {
			logger.error("Can't get user files", e);
		} finally {
			closeConnection(con);
		}
		
		return null;
	}

	public static void updateFile(File file) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			file.setExpDate(null);
			file.setExpDay(null);
			file.setExpMonth(null);
			file.setExpYear(null);
			FilesDAO.updateBE(con, file);
			
			try {
				UsersPendingDocsManagerBase.calculatePendingDocs(file.getId(), UsersPendingDocsManagerBase.EMPTY_PARAM);
				UsersPendingDocsManagerBase.updateLastUploadFileTime(file.getId());
			} catch (Exception e) {
				logger.error("Can't calculate Pending docs ", e);
			}
		} finally {
			closeConnection(con);
		}
	}

	public static String isUserSuspend(String userName, String password)	throws SQLException, CryptoException, InvalidKeyException,
																			NoSuchAlgorithmException, NoSuchPaddingException,
																			IllegalBlockSizeException, BadPaddingException,
																			UnsupportedEncodingException,
																			InvalidAlgorithmParameterException {
    	Connection con = null;
    	try {
            con = getConnection();
            return UsersDAO.isUserSuspend(con, userName, password);
    	} finally {
            closeConnection(con);
        }
    }
	
    public static void insertPasswordRecoveryHistory(int writer, String email, String mobilePhone, int status) throws SQLException {
	    Connection con = null;
  	    try {
            con = getConnection();
            UsersDAOBase.insertPasswordRecoveryHistory(con, writer, email, mobilePhone, status);
  	    } finally {
            closeConnection(con);
        }
    }
    
	public static void updateAdditionalFieldsEtrader(UserBase user)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
																	NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
																	UnsupportedEncodingException, InvalidAlgorithmParameterException {
        Connection conn = getConnection();
        try {
            UsersDAOBase.updateAdditionalFieldsEtrader(conn, user);            
        } finally {
            closeConnection(conn);
        }
    }
    
    public static boolean testIfThisEmailExistInDB(String email) throws SQLException {
        Connection conn = getConnection();
        try {
        	return UsersDAOBase.isThisEmailExistInDB(conn, email);            
        } finally {
            closeConnection(conn);
        }
    }
    
    public static boolean validateIdNum(String idNum, String formName, FacesContext context, ApplicationData ap, long userId) throws SQLException {
        Connection conn = getConnection();
        try {
        	return validateIdNum(idNum, formName, context, conn, ap, userId);            
        } finally {
            closeConnection(conn);
        }
    }
    
    /**
	 * Get combination instace by id
	 * @param combId
	 * @throws SQLException
	 */
	public static MarketingCombination getCombinationByUserId(long userID) throws SQLException {
		Connection con = getConnection();
		try {
			return MarketingCombinationsDAOBase.getCombinationByUserId(con, userID);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * Get affiliate instace by user id
	 * @param userId
	 * @throws SQLException
	 */
	public static MarketingAffilate getAffiliateByUserId(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return MarketingAffiliatesDAOBase.getAffiliateByUserId(con, userId);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Updates mobile_phone_verified flag to true if there is a user with the given mobile phone
	 * 
	 * @param userId
	 * @param mobilePhone
	 * @return true if there is a registered user with the given mobile phone, false otherwise
	 */
	public static boolean updatePhoneVerifiedFlag(long userId, String mobilePhone) {
		Connection con = null;
		try {
			con = getConnection();
			return UsersDAO.updatePhoneVerifiedFlag(con, userId, mobilePhone);
		} catch (SQLException e) {
			logger.debug("Unable to update verified phone flag", e);
			return false;
		} finally {
			closeConnection(con);
		}
	}

	public static String[] getTCTokenParts(long userId) {
		Connection con = null;
		try {
			con = getConnection();
			return UsersDAO.getTCTokenParts(con, userId);
		} catch (SQLException e) {
			logger.debug("Unable to load TC token parts", e);
			return null;
		} finally {
			closeConnection(con);
		}
	}
}