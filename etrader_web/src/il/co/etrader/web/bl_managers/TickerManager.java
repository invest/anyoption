package il.co.etrader.web.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.web.dao_managers.TickerDAO;
import il.co.etrader.web.helper.TradingPageBoxBean;

/**
 * Ticker line manager
 * 
 * @author Tony
 */
public class TickerManager extends BaseBLManager {
    /**
     * Load the current markets to show on Any Option trading page for
     * this user.
     * 
     * @param skinId
     * @return <code>ArrayList<TradingPageBoxBean></code>
     * @throws SQLException
     */
    public static ArrayList<TradingPageBoxBean> getSkinTickerMarkets(long skinId) throws SQLException {
        ArrayList<TradingPageBoxBean> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = TickerDAO.getSkinTickerMarkets(conn, skinId);
        } finally {
            closeConnection(conn);
        }
        return l;
    }
}