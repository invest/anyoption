package il.co.etrader.web.bl_managers;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.anyoption.common.util.MarketingTracker;

import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.SendContactEmail;
import il.co.etrader.web.util.Constants;

public class ContactsManager extends il.co.etrader.bl_managers.ContactsManager {
	private static final Logger logger = Logger.getLogger(ContactsManager.class);


    /**
     * Insert register attempt/short register entry (contact)
     * @param email
     * @param firstName
     * @param lastName
     * @param countryId
     * @param mobilePhone
     * @param landLinePhone
     * @param contactEmailRegisterAttempts
     * @param contactSMSRegisterAttempts
     * @param isShortForm
     * @param session
     * @param isAjaxCall
     */
    public static void insertRegAttempt(String email, String firstName, String lastName,
    		String countryId, String mobilePhone, String landLinePhone, boolean contactEmailRegisterAttempts,
    		boolean contactSMSRegisterAttempts, boolean isShortForm, HttpSession session, boolean isAjaxCall, boolean isQuickStart){

    	FacesContext context = FacesContext.getCurrentInstance();
    	ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.REGISTER_ATTEMPT, Contact.class);
        Contact regAttempt = (Contact) ve.getValue(context.getELContext());
        ApplicationData app = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        
        String dynamicParam = null;
        String textName = "";
        Long combId = null;
        try {
        	dynamicParam = UsersManager.getDynamicParam(request);
        	combId = app.getCombIdWithSPLogic(false, null);
			regAttempt.setCombId(combId);
			regAttempt.setDynamicParameter(dynamicParam);
		} catch (SQLException e1) {
			logger.warn("Exception while parsing combId or dp !");
		}
        
        regAttempt.setAff_sub1(CommonUtil.getRequestParameter(request, Constants.AFF_SUB1));
        regAttempt.setAff_sub2(CommonUtil.getRequestParameter(request, Constants.AFF_SUB2));
        regAttempt.setAff_sub3(CommonUtil.getRequestParameter(request, Constants.AFF_SUB3));

	    if (isAjaxCall){
            boolean isEmailValid = false;
            boolean isMobileValid = false;
            boolean isLandLineValid = false;

        	if (!CommonUtil.isParameterEmptyOrNull(email) && !email.equalsIgnoreCase(regAttempt.getEmail())){ //mail validation and value changed
        		if (CommonUtil.isEmailValidUnix(email)){
        			isEmailValid = true;
        		}
        	}

        	int mobileLengVal = 6;
        	int langLengVal = 6;
        	if (CommonUtil.isHebrewUserSkin()) {
            	mobileLengVal = 9;
            	langLengVal = 8;
        	}
        	if (!CommonUtil.isParameterEmptyOrNull(mobilePhone) && !mobilePhone.equalsIgnoreCase(regAttempt.getMobilePhone())){//mobile validation and value changed
        		String numToken = "\\b\\d+\\b";
        		if (null != mobilePhone && mobilePhone.length() > mobileLengVal && mobilePhone.matches(numToken)){
        			isMobileValid = true;
        		}
        	}
        	if (!CommonUtil.isParameterEmptyOrNull(landLinePhone) && !landLinePhone.equalsIgnoreCase(regAttempt.getLandLinePhone())){//land line phone validation and value changed
        		String numToken = "\\b\\d+\\b";
        		if (null != landLinePhone && landLinePhone.length() > langLengVal && landLinePhone.matches(numToken)){
        			isLandLineValid = true;
        		}
        	}

    		if (isEmailValid){
    			regAttempt.setEmail(email);
    		}
    		if (isMobileValid){
    			regAttempt.setMobilePhone(mobilePhone);
    		}
    		if (isLandLineValid){
    			regAttempt.setLandLinePhone(landLinePhone);
    		}
    		if (null != firstName && !firstName.equalsIgnoreCase(CommonUtil.getMessage("register.firstname.caps", null))){
    			regAttempt.setFirstName(firstName);
    			textName += firstName;
    		}
    		if (null != lastName && !lastName.equalsIgnoreCase(CommonUtil.getMessage("register.lastname.caps", null))){
    			regAttempt.setLastName(lastName);
    			textName += " " + lastName;
    		}
    		regAttempt.setText(textName.trim());
	    } else { // after short form and not ajax call
	    	regAttempt.setEmail(email);
	    	regAttempt.setFirstName(firstName);
	    	regAttempt.setLastName(lastName);
	    	regAttempt.setMobilePhone(mobilePhone);
	    	//Affiliate
			Long affKey = ApplicationData.getAffIdFromDynamicParameter(dynamicParam);
			if (null != affKey && (ApplicationData.isNetreferCombination(combId) || ApplicationData.isReferpartnerCombination(combId))) {
				regAttempt.setAffiliateKey(affKey);
			}
	    	//Sub affiliate
			Long subAffKeyLong;
			try {
				subAffKeyLong = UsersManager.getSubbAffIdParam(request);
			} catch (Exception e) {
				logger.warn("Exception while parsing Sub affiliate!" );
				subAffKeyLong = null;
			}
			if (null != subAffKeyLong) {
				regAttempt.setAffiliateKey(subAffKeyLong);
			}
	    	//http referrer
    		String hr = null;
    		try {
    			hr = UsersManager.getHttpReferer(request);
			} catch (Exception e) {
				logger.warn("Exception while parsing Http Referrer!" );
				hr = null;
			}
			if (null != hr) {
				regAttempt.setHttpReferer(hr);
			}
			regAttempt.setText((firstName + " " + lastName).trim());
	    }

		String skinId = String.valueOf(app.getSkinId());
		String dui = (String)session.getAttribute(Constants.DEVICE_UNIQUE_ID);
		regAttempt.setUserAgent(CommonUtil.getUserAgent());
		regAttempt.setSkinId(Integer.valueOf(skinId));
		String ip = CommonUtil.getIPAddress();

		if (null != countryId && !countryId.equals("0")) {
			if(countryId.equalsIgnoreCase("-1")){
				regAttempt.setCountryId(Integer.parseInt(app.getRealCountryId()));
			}else {
				regAttempt.setCountryId(Integer.valueOf(countryId));
			}		
		} else {
			regAttempt.setCountryId(app.getSkinById(Integer.valueOf(skinId)).getDefaultCountryId());
		}
		regAttempt.setWriterId(CommonUtil.getWebWriterId());
		regAttempt.setDeviceUniqueId(dui);
		regAttempt.setIp(ip);
		regAttempt.setUtcOffset(CommonUtil.getUtcOffset());
        regAttempt.setUserAgent(CommonUtil.getUserAgent());
        regAttempt.setType(Contact.CONTACT_US_REGISTER_ATTEMPTS);
        if (isShortForm){
        	regAttempt.setType(Contact.CONTACT_US_SHORT_REG_FORM);
        }else if(isQuickStart){
        	regAttempt.setType(Contact.CONTACT_US_QUICK_START_ATTEMPTS);	
        }
        regAttempt.setDfaCreativeId(UsersManager.getDFACreativeId(request));
        regAttempt.setDfaPlacementId(UsersManager.getDFAPlacementId(request));
        regAttempt.setDfaMacro(UsersManager.getDFAMacro(request));
        regAttempt.setContactByEmail(contactEmailRegisterAttempts);
        regAttempt.setContactBySMS(contactSMSRegisterAttempts);
        if (regAttempt.getUserId() == 0 && isShortForm){ //connect userId in short form
        	long userId = 0;
        		try {
					userId = ContactsManager.searchUserIdFromContactDetails(regAttempt);
					regAttempt.setUserId(userId);
				} catch (SQLException e) {
					logger.log(Level.ERROR, "Can't get userId on register attempts or SQL EXCEPTION!!.", e);
					e.printStackTrace();
				}
        }
        try {
        	if (regAttempt.getId() == 0){
        		ContactsManager.insertContactRequest(regAttempt, false, true, null, null);
        		
                
        		// Marketing Tracking
        		try {
                    MarketingTracker.checkExistCookieWhenInsertContact(request, response, regAttempt.getEmail(), regAttempt.getMobilePhone());
                    MarketingTracker.insertMarketingTracker(request, response, regAttempt.getId(), 0, Constants.MARKETING_TRACKING_SHORT_REG);

                    //MarketingETS.etsMarketingActivity(request, response, regAttempt.getId(), Constants.MARKETING_TRACKING_SHORT_REG, regAttempt.getEmail(), regAttempt.getMobilePhone(), null, null, null, null, regAttempt.getIp());
                } catch (SQLException e) {
                    logger.debug("exception in MarketingTrackingInsert insert", e);
                }
        		
        	} else {
        		ContactsManager.updateContact(regAttempt);
        	}
        	logger.debug("Insert/Update Register Attempts table, id: " + regAttempt.getId());
        	ve.setValue(context.getELContext(), regAttempt);
		} catch (Exception e) {
			logger.log(Level.ERROR,
					"Can't write reply or SQL EXCEPTION!!.", e);
		}

    }

    /**
     * Insert contact from etrader Iframe
     * @param contactReq
     * @throws SQLException
     */
    public static void insertNewContactFromIframe(Contact contactReq) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
    	logger.info("Insert new contact from iframe");
		Contact contact = null;
		PopulationEntryBase popUser = null;
		long contactId = ContactsManager.isExist(contactReq.getPhone(), contactReq.getEmail());
		long userId = UsersManager.getUserIdByPhoneEmail(contactReq.getPhone(), contactReq.getEmail(), contactReq.getCountryId());
	    contactReq.setIp(CommonUtil.getIPAddress());
		if (userId != 0) {
			popUser =  PopulationsManagerBase.getPopulationUserByUserId(userId);
		}
		if (contactId == 0) { // new contact
			contactReq.setUserId(userId);
			contact = ContactsManager.insertContactRequest(contactReq, false, true, null, null);
			contactId = contact.getId();  // for storing contactID later in cookie
			try {
				HashMap<String, String> params = null;
				// collect the parameters we neeed for the email
				params = new HashMap<String, String>();
				params.put(SendContactEmail.PARAM_EMAIL, contactReq.getEmail());
				params.put(SendContactEmail.PARAM_FIRST_NAME, CommonUtil.capitalizeFirstLetters(contactReq.getFirstName()));
				params.put(SendContactEmail.PARAM_MOBILE, contactReq.getPhone());
				if(contactId != 0){
					params.put(SendContactEmail.PARAM_CONTACT_ID, contactId + "");
				}
		    	ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
	    		String language = ap.getUserLocale().getDisplayLanguage();
		    	params.put(SendContactEmail.PARAM_COUNTRY, CommonUtil.getMessage(ap.getCountry(contactReq.getCountryId()).getDisplayName(), null));

				// Populate also server related params, in order to take out common functionality
				// Use params

				//String subject = Constants.CONTACT_ME_SUBJECT + combId;
				String subject = Constants.CONTACT_ME_SUBJECT;
				subject += ", " + 	Constants.CONTACT_ME_SUBJECT_LANG;
				if (!CommonUtil.isParameterEmptyOrNull(language)) {
					subject += language;
				}
				else {
					subject += Constants.CONTACT_ME_LANG_ISRAEL;
				}

				params.put(SendContactEmail.MAIL_FROM, CommonUtil.getProperty("contact.first.email", null));
				params.put(SendContactEmail.MAIL_TO, CommonUtil.getProperty("contact.first.email", null));
				params.put(SendContactEmail.MAIL_SUBJECT ,subject);
				params.put(SendContactEmail.SUCCESS_MESSAGE, CommonUtil.getMessage("contact.mail.success", null));
				params.put(SendContactEmail.RETURN_PAGE, Constants.NAV_EMPTY_SSL);

				new SendContactEmail(params).start();
			} catch (Exception e) {
				logger.error("Could not send email.", e);
			}

		} else {   // existing contact, add update event
			if (userId != 0) {
				logger.info("going to update contactId: " + contactId + " with userId: " + userId);
				ContactsManager.updateContactRequest(contactId, userId);
			}
			if (null == popUser){
				popUser = PopulationsManagerBase.getPopulationUserByContactId(contactId);
			}
		}

		if (null != popUser){
			PopulationsManagerBase.updateContactMeEvent(popUser, contactId, contactReq.getSkinId(), userId,CommonUtil.getWebWriterId());
		} else {
			PopulationsManagerBase.insertIntoPopulation(contactId, contactReq.getSkinId(), userId, null, CommonUtil.getWebWriterId(),
					PopulationsManagerBase.POP_TYPE_CALLME, null);
		}
		logger.info("End function insertNewContact(...) that contain set Cookie");
    }

    public static void insertContactStaticLP(String email, String firstName, String lastName,
    		long countryId, String mobilePhone, boolean reciveUpdates, Long affiliateKey, Long subAffiliateKey,
    		String httpReferer, Date timeFirstVisit, int skinId, HttpServletRequest request, 
    		HttpServletResponse response, String ip, String utcOffset){

        Contact contact = new Contact();
        contact.setEmail(email);
        contact.setFirstName(firstName);
        contact.setLastName(lastName);
        contact.setMobilePhone(mobilePhone);
    	if (null != affiliateKey && affiliateKey != -1) {
    		contact.setAffiliateKey(affiliateKey);
    	}
    	if (null != subAffiliateKey) {
    		contact.setSubAffiliateKey(subAffiliateKey);
    	}
    	if (null != httpReferer) {
    		contact.setHttpReferer(httpReferer);
    	}
    	if (null != timeFirstVisit) {
    		contact.setTimeFirstVisit(timeFirstVisit);
    	}

		contact.setIp(ip);
    	contact.setText((firstName + " " + lastName).trim());
    	contact.setSkinId(skinId);
		contact.setCountryId(countryId);
		contact.setWriterId(Writer.WRITER_ID_WEB);
		contact.setUtcOffset(utcOffset);
		contact.setUserAgent(CommonUtil.getUserAgentWithRequest(request));
		contact.setType(Contact.CONTACT_US_SHORT_REG_FORM);

        try {
        	contact.setCombId(ApplicationData.getCombIdWithSPLogicStatic(request, skinId));
        	contact.setDynamicParameter(UsersManager.getDynamicParam(request));
		} catch (SQLException e1) {
			logger.warn("RegisterAtt problem parsing combId!");
		}
        
    	contact.setAff_sub1(CommonUtil.getRequestParameter(request, Constants.AFF_SUB1));
    	contact.setAff_sub2(CommonUtil.getRequestParameter(request, Constants.AFF_SUB2));
    	contact.setAff_sub3(CommonUtil.getRequestParameter(request, Constants.AFF_SUB3));

		contact.setDfaCreativeId(UsersManager.getDFACreativeId(request));
		contact.setDfaPlacementId(UsersManager.getDFAPlacementId(request));
		contact.setDfaMacro(UsersManager.getDFAMacro(request));
		contact.setContactByEmail(reciveUpdates);
		contact.setContactBySMS(reciveUpdates);
		//connect userId in short form
    	long userId = 0;
		try {
			userId = ContactsManager.searchUserIdFromContactDetails(contact);
			contact.setUserId(userId);
		} catch (SQLException e) {
			logger.log(Level.ERROR, "Can't get userId on register attempts or SQL EXCEPTION!!.", e);
			e.printStackTrace();
		}
        try {
       		ContactsManager.insertContactRequest(contact, true, true, null, request);
       		logger.debug("Insert contact from static landing page, id: " + contact.getId());
       		request.getSession().setAttribute(Constants.STATIC_LANDING_PAGE_CONTACT_ID, contact.getId());
		} catch (Exception e) {
			logger.log(Level.ERROR,
					"Can't write reply or SQL EXCEPTION!!.", e);
		}
        
		try {
		    MarketingTracker.checkExistCookieWhenInsertContact(request, response, contact.getEmail(), contact.getMobilePhone());
			MarketingTracker.insertMarketingTracker(request, response, contact.getId(), contact.getUserId(), Constants.MARKETING_TRACKING_SHORT_REG);
			
			//MarketingETS.etsMarketingActivity(request, response, contact.getId(), Constants.MARKETING_TRACKING_SHORT_REG, contact.getEmail(), contact.getMobilePhone(), null, null, null, null, contact.getIp());
		} catch (SQLException e) {
		    logger.debug("exception in MarketingTrackingInsert insert", e);
		}
    }

}