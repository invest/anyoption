package il.co.etrader.web.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.service.CommonJSONServiceServlet;
import com.anyoption.common.service.requests.CancelBubbleInvestmentMethodRequest;
import com.anyoption.common.service.requests.CancelInvestmentCheckboxStateMethodRequest;
import com.anyoption.common.service.requests.CancelInvestmentMethodRequest;
import com.anyoption.common.service.requests.CardMethodRequest;
import com.anyoption.common.service.requests.CcDepositMethodRequest;
import com.anyoption.common.service.requests.CrossSaleRequest;
import com.anyoption.common.service.requests.DepositBonusBalanceMethodRequest;
import com.anyoption.common.service.requests.InsertInvestmentMethodRequest;
import com.anyoption.common.service.requests.InsertUserMethodRequest;
import com.anyoption.common.service.requests.InsertWithdrawCancelMethodRequest;
import com.anyoption.common.service.requests.InvestmentsMethodRequest;
import com.anyoption.common.service.requests.IsKnowledgeQuestionMethodRequest;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.requests.SellDynamicsInvestmentRequest;
import com.anyoption.common.service.requests.TermsFileMethodRequest;
import com.anyoption.common.service.requests.UnblockUserMethodRequest;
import com.anyoption.common.service.requests.UpdateUserMethodRequest;
import com.anyoption.common.service.requests.UpdateUserQuestionnaireAnswersRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.requests.VisibleSkinMethodRequest;
import com.anyoption.common.service.requests.WithdrawBonusRegulationStateMethodRequest;
import com.anyoption.common.util.OpportunityCache;

/**
 * @author kirilim
 */
public class AnyoptionServiceServlet extends CommonJSONServiceServlet {

	private static final long serialVersionUID = -3536866919755220527L;
	private static final Logger log = Logger.getLogger(AnyoptionServiceServlet.class);
//	private static Map<String, Method> methods;

	@Override
	public void init() {
		log.info("il.co.etrader.web.service.AnyoptionServiceServlet starting...");
		super.init();
//		methods = new HashMap<String, Method>();
		try {
			Class<?> serviceClass = Class.forName("il.co.etrader.web.service.AnyoptionService");
			methods.put("cancelBubbleInvestment",
						serviceClass.getMethod("cancelBubbleInvestment", new Class[] {CancelBubbleInvestmentMethodRequest.class, HttpServletRequest.class}));
			methods.put("getUserDepositBonusBalance",
					serviceClass.getMethod("getUserDepositBonusBalance", new Class[] {DepositBonusBalanceMethodRequest.class, HttpServletRequest.class}));			
			methods.put("closeSuccessDoneQuestPopUp",
					serviceClass.getMethod("closeSuccessDoneQuestPopUp", new Class[] {MethodRequest.class, HttpServletRequest.class}));
			methods.put("updateIsKnowledgeQuestion",
					serviceClass.getMethod("updateIsKnowledgeQuestion", new Class[] {IsKnowledgeQuestionMethodRequest.class, HttpServletRequest.class}));
			methods.put("getWithdrawBonusRegulationState",
						serviceClass.getMethod(	"getWithdrawBonusRegulationState",
												new Class[] {WithdrawBonusRegulationStateMethodRequest.class, HttpServletRequest.class}));
			methods.put("unblockUserForThreshold",
					serviceClass.getMethod("unblockUserForThreshold", new Class[] {UnblockUserMethodRequest.class, HttpServletRequest.class}));
//			methods.put("getChartDataCommon", 
//					serviceClass.getMethod("getChartDataCommon", new Class[] {ChartDataRequest.class, OpportunityCache.class, ChartHistoryCache.class, WebLevelsCache.class, HttpServletRequest.class}));
            methods.put("insertDynamicsInvestment", serviceClass.getMethod("insertDynamicsInvestment", new Class[]{InsertInvestmentMethodRequest.class, OpportunityCache.class, WebLevelsCache.class, HttpServletRequest.class}));
            methods.put("sellDynamicsInvestment", serviceClass.getMethod("sellDynamicsInvestment", new Class[]{SellDynamicsInvestmentRequest.class, OpportunityCache.class, WebLevelsCache.class, HttpServletRequest.class}));
            methods.put("sellAllDynamicsInvestments", serviceClass.getMethod("sellAllDynamicsInvestments", new Class[]{SellDynamicsInvestmentRequest.class, OpportunityCache.class, WebLevelsCache.class, HttpServletRequest.class}));
			methods.put("insertWithdrawCancel",
						serviceClass.getMethod("insertWithdrawCancel", new Class[] {InsertWithdrawCancelMethodRequest.class, HttpServletRequest.class}));
			methods.put("getForgetPasswordContacts", serviceClass.getMethod("getForgetPasswordContacts", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
			methods.put("getTermsFiles", serviceClass.getMethod("getTermsFiles", new Class[] {TermsFileMethodRequest.class}));
			methods.put("getUserInvestments", serviceClass.getMethod("getUserInvestments", new Class[] {InvestmentsMethodRequest.class, HttpServletRequest.class}));
        	methods.put("cancelInvestment", serviceClass.getMethod("cancelInvestment",new Class[] {CancelInvestmentMethodRequest.class , OpportunityCache.class , WebLevelsCache.class , HttpServletRequest.class }));
			methods.put("changeCancelInvestmentCheckboxState",
						serviceClass.getMethod(	"changeCancelInvestmentCheckboxState",
												new Class[] {CancelInvestmentCheckboxStateMethodRequest.class, HttpServletRequest.class}));
			methods.put("getVisibleSkinsWeb",
					serviceClass.getMethod(	"getVisibleSkinsWeb",
											new Class[] {VisibleSkinMethodRequest.class, HttpServletRequest.class}));
            methods.put("getUserSingleQuestionnaireDynamic", serviceClass.getMethod("getUserSingleQuestionnaireDynamic", 
            								new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("updateSingleQuestionnaireDone" , serviceClass.getMethod("updateSingleQuestionnaireDone", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
            methods.put("updateUserQuestionnaireAnswers"  , serviceClass.getMethod("updateUserQuestionnaireAnswers", new Class[] {
            		UpdateUserQuestionnaireAnswersRequest.class , HttpServletRequest.class }));
			methods.put("getOpenPositions",
						serviceClass.getMethod("getOpenPositions", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));

			serviceClass = Class.forName("il.co.etrader.web.service.JSONJsfBridgeService");
			methods.put("insertUser",
						serviceClass.getMethod("insertUser", new Class[] {InsertUserMethodRequest.class, HttpServletRequest.class}));
			methods.put("getUser", serviceClass.getMethod("getUser", new Class[] {UserMethodRequest.class, HttpServletRequest.class}));
			methods.put("insertCard",
						serviceClass.getMethod("insertCard", new Class[] {CardMethodRequest.class, HttpServletRequest.class}));
			methods.put("insertDepositCard",
						serviceClass.getMethod("insertDepositCard", new Class[] {CcDepositMethodRequest.class, HttpServletRequest.class}));
			methods.put("updateUserAdditionalFields",
						serviceClass.getMethod(	"updateUserAdditionalFields",
												new Class[] {UpdateUserMethodRequest.class, HttpServletRequest.class}));

		} catch (Exception e) {
			log.fatal("Cannot load service methods! ", e);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		super.doPost(request, response);
//		String uri = request.getRequestURI();
//		String methodReq = uri.substring(uri.lastIndexOf("/") + 1);
//		log.debug("URI requested: " + uri + " Method: " + methodReq + " sessionId: " + request.getSession().getId());
//
//		if (log.isTraceEnabled()) {
//			Enumeration<String> headerNames = request.getHeaderNames();
//			String headerName = null;
//			String ls = System.getProperty("line.separator");
//			StringBuffer sb = new StringBuffer(ls);
//			while (headerNames.hasMoreElements()) {
//				headerName = headerNames.nextElement();
//				sb.append(headerName).append(": ").append(request.getHeader(headerName)).append(ls);
//			}
//			log.trace(sb.toString());
//		}
//
//		Object result = null;
//		try {
//			Method m = methods.get(methodReq);
//			if (m != null) {
//				Gson gson = new GsonBuilder().serializeNulls().create();
//				Class<?>[] params = m.getParameterTypes();
//				Object[] requestParams = new Object[params.length];
//				requestParams[0] = gson.fromJson(new InputStreamReader(request.getInputStream(), "UTF-8"), params[0]);
//				for (int i = 1; i < params.length; i++) {
//                    if (params[i] == HttpServletRequest.class) {
//                        requestParams[i] = request;
//                    }
//				}
//				if (requestParams[0] instanceof MethodRequest) {
//					((MethodRequest) requestParams[0]).setIp(CommonUtil.getIPAddress(request));
//				}
//				result = m.invoke(null, requestParams);
//
//				String jsonResponse = gson.toJson(result);
//				if (null != jsonResponse && jsonResponse.length() > 0) {
//					log.debug(jsonResponse);
//					byte[] data = jsonResponse.getBytes("UTF-8");
//					OutputStream os = response.getOutputStream();
//					response.setHeader("Content-Type", "application/json");
//					response.setHeader("Content-Length", String.valueOf(data.length));
//					os.write(data, 0, data.length);
//					os.flush();
//					os.close();
//				}
//			} else {
//				log.warn("Method: " + methodReq + " not found!");
//			}
//		} catch (Exception e) {
//			log.error("Problem executing " + methodReq + " Method! ", e);
//		}
	}

	public void outerDoPost(HttpServletRequest request, HttpServletResponse response) {
		doPost(request, response);
	}

	@Override
	public void destroy() {
		super.destroy();
//		methods.clear();
//		methods = null;
	}
}