package il.co.etrader.web.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.anyoption.common.beans.PaymentMethodService;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.TransactionRequestResponse;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.clearing.EPGClearingProvider;
import com.anyoption.common.managers.PaymentMethodServicesManager;
import com.anyoption.common.managers.TransactionRequestResponseManager;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.payments.FinalizeDeposit;
import com.anyoption.common.payments.epg.Operation;
import com.anyoption.common.payments.epg.Response;
import com.anyoption.common.util.CommonUtil;

/**
 * EpgServlet
 * @author eyalo
 */
public class EpgServlet extends HttpServlet {
	private static final long serialVersionUID = 8543999557874485107L;
	private static final Logger log = Logger.getLogger(EpgServlet.class);
	private static EPGClearingProvider clearingProvider;
	private static ArrayList<PaymentMethodService> services;
	private static HashMap<String, Method> methods;

	@Override
	public void init() throws ServletException {
		/* clearingProvider */
		log.info("Init EpgServlet - START.");
		clearingProvider = new EPGClearingProvider();
		/* services */
		log.info("Get all services.");
		try {
			services = PaymentMethodServicesManager.getAllServices();
		} catch (SQLException e) {
			log.fatal("Can't load services.", e);
		}
		
		/* Methods */
		log.info("Load service methods.");
		methods = new HashMap<String, Method>();
		try {
			Class<?> serviceClass = Class.forName("il.co.etrader.web.service.EpgService");
			for (PaymentMethodService pms : services) {
				try {
					methods.put(pms.getMethodName(), serviceClass.getMethod(pms.getMethodName(), new Class[] {FinalizeDeposit.class}));
				} catch (Exception e) {
					log.error("Can't load specific service method - remove it from the data base! ", e);
				}
			}
		} catch (Exception e) {
			log.fatal("Can't load all service methods! ", e);
		}
		log.info("Init EpgServlet - END.");
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		log.info("EpgServlet: Don't support get request from EPG.");
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		log.info("Post request from EPG - START.");
		String responseXML = null;
		//FIXME handle exceptions.
		//TODO handle 2 transactions at the same time - for update
		//
        try {
        	/* Get EPG's Response */
        	log.info("Get response from EPG - START.");
    	    InputStream is = request.getInputStream();
            InputStreamReader isr = new InputStreamReader(is, "UTF-8");
            BufferedReader br = new BufferedReader(isr);
            StringBuffer xmlResp = new StringBuffer();
            String line = null;
            while ((line = br.readLine()) != null) {
         	   xmlResp.append(line);
            }
            responseXML = xmlResp.toString();
            log.info("Response received from EPG: " + responseXML);
            Response responseObj = (Response) CommonUtil.generateXmlToObject(Response.class, responseXML);
            
            if (responseObj != null) {
	            //for each transaction that responded from EPG
	            for (Operation operation : responseObj.getOperations()) {       
		            TransactionRequestResponse trr = 
							new TransactionRequestResponse(Long.parseLong(operation.getMerchantTransactionId()), 
									"", responseXML, operation.getPaymentSolution(),
									operation.getPaymentDetails() != null ? operation.getPaymentDetails().getAccount() : "");
					TransactionRequestResponseManager.insert(trr);	
		
					Transaction t = TransactionsManagerBase.getTransaction(trr.getTransactionId());
		            PaymentMethodService pms = 
		            		new PaymentMethodService(t.getClassType(), t.getStatusId(), operation.getOperationType(), 
		            				operation.getStatus());
					String serviceName = PaymentMethodServicesManager.getService(pms);
		            log.info("The service name: " + serviceName + ". transaction id: " + t.getId());
		            Method m = methods.get(serviceName);
		            if (null != m) {
		            	String msg = responseObj.getMessage() + " | " + operation.getMessage();
		            	User user = UsersManagerBase.getUserById(t.getUserId());
		            	
						//FIXME "" - usermessage, result? 
		            	FinalizeDeposit fd = new FinalizeDeposit(user, operation.getStatus().equals("SUCCESS") ? true : false, 
		            			operation.getPaySolTransactionId(), "", msg , "",
		            			operation.getPayFrexTransactionId(), Writer.WRITER_ID_AUTO, 0L, t);
		            	fd.setUtcOffset(user.getUtcOffset()); //FIXME 
						m.invoke(EpgService.class, fd);
		            } else {
		            	log.info("Method isn't found.");
		            }
	            }
            }
        } catch (Exception e) {
        	log.error("Failed to to get post request from EPG.", e);
        }
        log.info("Post request from EPG - END.");
	}
}
