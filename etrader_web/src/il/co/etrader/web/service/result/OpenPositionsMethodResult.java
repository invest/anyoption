package il.co.etrader.web.service.result;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.anyoption.common.annotations.AnyoptionNoJSON;
import com.anyoption.common.service.results.MethodResult;

/**
 * @author kiril.mutafchiev
 */
public class OpenPositionsMethodResult extends MethodResult {

	/**
	 * Collection of open position objects. It holds data for opportunities and collection of investments made on these opportunities.
	 */
	private List<OpenPosition> openPositions;

	/**
	 * Helper map, that holds relation between opportunity and it's collection of investments. It is advisable to call
	 * {@code clearHelperMap} method in order to clear the map after all open positions are loaded for optimization. Implementation should
	 * guarantee that this field is not serialized.
	 * 
	 * @see #clearHelperMap()
	 */
	@AnyoptionNoJSON
	private Map<Long, List<InvestmentPosition>> oppInvestmentsMap;

	public OpenPositionsMethodResult() {
		openPositions = new ArrayList<>();
		oppInvestmentsMap = new HashMap<>();
	}

	/**
	 * Creates opportunity-like object which will hold a collection of open investments on this opportunity. The implementation handles when a
	 * holder object will be created and when only the given investment will be added to the collection of investments.
	 * 
	 * @param opportunityId id of the investment's opportunity
	 * @param marketId id of the investment's market
	 * @param marketName name of the investment's market
	 * @param timeEstClosing estimated closing time of the opportunity
	 * @param scheduled scheduled of the opportunity(hourly/daily/weekly/etc.)
	 * @param investmentId id of the investment
	 * @param amount amount of the investment
	 * @param typeId type of the investment(call/put)
	 * @param currentLevel level at which the investment was made
	 * @param oddsWin win odds of the investment
	 * @param oddsLose lose odds of the investment
	 * 
	 * @see #addOpenPosition(long, long, String, Date, int)
	 * @see #addInvestmentPosition(long, long, long, long, double, double, double)
	 */
	public void addOpenPosition(long opportunityId, long marketId, String marketName, Date timeEstClosing, int scheduled, long investmentId,
								long amount, long typeId, double currentLevel, double oddsWin, double oddsLose) {
		if (oppInvestmentsMap.get(opportunityId) == null) {
			addOpenPosition(opportunityId, marketId, marketName, timeEstClosing, scheduled);
		}
		addInvestmentPosition(opportunityId, investmentId, amount, typeId, currentLevel, oddsWin, oddsLose);
	}

	/**
	 * Adds opportunity-like object which will hold a collection of open investments on this opportunity. For each new opportunity, this
	 * method should be called once before the {@code addInvestmentPosition} method.
	 * 
	 * @param opportunityId id of the investment's opportunity
	 * @param marketId id of the investment's market
	 * @param marketName name of the investment's market
	 * @param timeEstClosing estimated closing time of the opportunity
	 * @param scheduled scheduled of the opportunity(hourly/daily/weekly/etc.)
	 * 
	 * @see #addInvestmentPosition(long, long, long, long, double, double, double)
	 * @see #openPositions
	 * @see OpenPosition
	 */
	private void addOpenPosition(long opportunityId, long marketId, String marketName, Date timeEstClosing, int scheduled) {
		OpenPosition op = new OpenPosition(opportunityId, marketId, marketName, timeEstClosing, scheduled);
		openPositions.add(op);
		oppInvestmentsMap.put(opportunityId, op.investments);
	}

	/**
	 * Adds investment-like object to the investments collection of the given opportunity. Before this method is called, a call to
	 * {@code addOpenPosition} method for this opportunity should be made. If there is no such call, the given investment will not be added.
	 * 
	 * @param opportunityId id of the investment's opportunity
	 * @param investmentId id of the investment
	 * @param amount amount of the investment
	 * @param typeId type of the investment(call/put)
	 * @param currentLevel level at which the investment was made
	 * @param oddsWin win odds of the investment
	 * @param oddsLose lose odds of the investment
	 * 
	 * @see #addOpenPosition(long, long, String, Date, int)
	 * @see #openPositions
	 * @see InvestmentPosition
	 */
	private void addInvestmentPosition(	long opportunityId, long investmentId, long amount, long typeId, double currentLevel, double oddsWin,
										double oddsLose) {
		List<InvestmentPosition> investments = oppInvestmentsMap.get(opportunityId);
		if (investments != null) {
			investments.add(new InvestmentPosition(investmentId, amount, typeId, currentLevel, oddsWin, oddsLose));
		}
	}

	/**
	 * Clears the helper map. This method should be called after all objects are loaded into the structure as a matter of optimization.
	 * 
	 * @see #openPositions
	 * @see #oppInvestmentsMap
	 */
	public void clearHelperMap() {
		oppInvestmentsMap.clear();
	}

	public List<OpenPosition> getOpenPositions() {
		return openPositions;
	}

	public void setOpenPositions(List<OpenPosition> openPositions) {
		this.openPositions = openPositions;
	}

	private class OpenPosition {

		@SuppressWarnings("unused")
		long opportunityId;
		@SuppressWarnings("unused")
		long marketId;
		@SuppressWarnings("unused")
		String marketName;
		@SuppressWarnings("unused")
		Date timeEstClosing;
		@SuppressWarnings("unused")
		int scheduled;
		List<InvestmentPosition> investments;

		OpenPosition(long opportunityId, long marketId, String marketName, Date timeEstClosing, int scheduled) {
			this.opportunityId = opportunityId;
			this.marketId = marketId;
			this.marketName = marketName;
			this.timeEstClosing = timeEstClosing;
			this.scheduled = scheduled;
			investments = new ArrayList<>();
		}
	}

	private class InvestmentPosition {

		@SuppressWarnings("unused")
		long id;
		@SuppressWarnings("unused")
		long amount;
		@SuppressWarnings("unused")
		long typeId;
		@SuppressWarnings("unused")
		double currentLevel;
		@SuppressWarnings("unused")
		double oddsWin;
		@SuppressWarnings("unused")
		double oddsLose;

		InvestmentPosition(long id, long amount, long typeId, double currentLevel, double oddsWin, double oddsLose) {
			this.id = id;
			this.amount = amount;
			this.typeId = typeId;
			this.currentLevel = currentLevel;
			this.oddsWin = oddsWin;
			this.oddsLose = oddsLose;
		}
	}
}