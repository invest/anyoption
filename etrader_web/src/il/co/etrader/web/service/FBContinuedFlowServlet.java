package il.co.etrader.web.service;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @author eranl
 * AR-1518 - AO + CO - Support FB continued flow (Web + Mobile)
 *
 */
public class FBContinuedFlowServlet extends com.anyoption.common.util.FBContinuedFlowServlet {

	private static final long serialVersionUID = 1L;	
	
	@Override
	public void insertContact(String email, String firstName, String lastName,
			long countryId, long skinId, HttpServletRequest request,
			HttpServletResponse response, String ip, String combId,
			String dynamicParam, String affSub1, String affSub2, String affSub3, long writerId) {
	}
	
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
    	saveData(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException  {
    	saveData(request, response);
    }
    
}
