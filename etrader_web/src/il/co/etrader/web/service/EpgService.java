package il.co.etrader.web.service;

import java.io.Serializable;
import org.apache.log4j.Logger;
import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.payments.FinalizeDeposit;


public class EpgService implements Serializable {
	
	private static final long serialVersionUID = -7657276074722820149L;
	private static final Logger log = Logger.getLogger(EpgService.class);
	
	/** 
	 * finalizeDeposit
	 * @param FinalizeDeposit
	 */ 
	public static void finalizeDeposit(FinalizeDeposit fd) {
		log.debug("finalizeDeposit - START.");
		try { 
			il.co.etrader.bl_managers.TransactionsManagerBase.finalizeDeposit(fd);
		} catch (ClearingException e) { 
			log.error("Error in finalizeDeposit", e);
		} 
		log.debug("finalizeDeposit - END."); 
	} 
	
	/**
	 * Cancel deposit
	 * @param trr
	 */
	/*public static void cancelDeposit(TransactionRequestResponse trr) {
		log.info("CancelDeposit - START.");
		CancelDeposit cancelDeposit = new CancelDeposit();
		cancelDeposit.setClearingInfo(trr.getClearingInfo());
		cancelDeposit.setStatus(TransactionsManagerBase.TRANS_STATUS_CANCELED_FRAUDS);
		cancelDeposit.setWriterId(Writer.WRITER_ID_AUTO);
		 Transaction 
		Transaction t = trr.getTransaction();
		cancelDeposit.setUserUtcOffset(t.getUtcOffsetCreated());
		cancelDeposit.getClearingInfo().setUserId(t.getUserId());
		cancelDeposit.getClearingInfo().setAmount(t.getAmount());
		try {
			 Cancel deposit 
			TransactionsManagerBase.cancelDeposit(cancelDeposit);
		} catch (SQLException e) {
			log.error("Error in cancelDeposit", e);
		}
		log.info("CancelDeposit - END.");
	}*/
	
	/**
	 * Capture Deposit
	 * @param trr
	 */
	/*public static void captureDeposit(TransactionRequestResponse trr) {
		log.info("captureDeposit - START.");
		try {
			 Transaction 
			Transaction t = trr.getTransaction();
			 Update transaction capture 
			TransactionsManagerBase.updateTransactionCapture(trr.getClearingInfo(), t.getUtcOffsetSettled());
		} catch (SQLException e) {
			log.error("Error in captureDeposit", e);
		}
		log.info("captureDeposit - END.");
	}*/
	
	/**
	 * Capture Withdrawal
	 * @param trr
	 */
	/*public static void captureWithdrawal(TransactionRequestResponse trr) {
		log.info("captureWithdrawal - START.");
		ClearingInfo info = trr.getClearingInfo();
		CaptureWithdrawal captureWithdrawal = new CaptureWithdrawal();
		captureWithdrawal.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
		captureWithdrawal.setComment(info.getResult() + " | " + info.getMessage() + " | " + info.getUserMessage() + " | " + info.getProviderTransactionId());
		captureWithdrawal.setDescription(ConstantsBase.EMPTY_STRING);
		captureWithdrawal.setTransactionId(info.getTransactionId());
		captureWithdrawal.setProviderTransactionId(info.getProviderTransactionId());
		try {
			 Transaction 
			Transaction t = trr.getTransaction();
			 User 
			User user = UsersManagerBase.getUserById(t.getUserId());
			captureWithdrawal.setUser(user);
			captureWithdrawal.setUtcOffsetSettled(t.getUtcOffsetCreated());
			captureWithdrawal.setClearingProviderId(t.getClearingProviderId());
			 Update transaction 
			TransactionsManagerBase.updateTransactionAfterCaptureWithdrawal(captureWithdrawal);
		} catch (SQLException e) {
			log.error("Error in captureWithdrawal", e);
		}
		log.info("captureWithdrawal - END.");
	}*/
} 
