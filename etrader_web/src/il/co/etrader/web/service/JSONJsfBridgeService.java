package il.co.etrader.web.service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.CreditCard;
import com.anyoption.common.beans.base.Register;
import com.anyoption.common.beans.base.ThreeDParams;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.CardMethodRequest;
import com.anyoption.common.service.requests.CcDepositMethodRequest;
import com.anyoption.common.service.requests.InsertUserMethodRequest;
import com.anyoption.common.service.requests.UpdateUserMethodRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.CardMethodResult;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.TransactionMethodResult;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.common.util.CommonUtil;

import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.mbeans.AbstractDepositForm;
import il.co.etrader.web.mbeans.DepositForm;
import il.co.etrader.web.mbeans.LoginForm;
import il.co.etrader.web.mbeans.NewCardForm;
import il.co.etrader.web.mbeans.RegisterForm;
import il.co.etrader.web.util.Constants;

/**
 * @author kiril.mutafchiev
 */
public class JSONJsfBridgeService extends CommonJSONService {

	private static final Logger log = Logger.getLogger(JSONJsfBridgeService.class);

	public static UserMethodResult insertUser(InsertUserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("insertUser:" + request);
		// TODO check whether a contact should be created and how
		// login form params
		Register register = request.getRegister();
		// emailField and mobilePhoneField are passed as nulls
		UIViewRoot vR = new UIViewRoot();
		vR.setViewId("3153596692497891920L");
		FacesContext.getCurrentInstance().setViewRoot(vR);
		RegisterForm registerForm = new RegisterForm(	register.getEmail(), null, register.getFirstName(), register.getLastName(),
														String.valueOf(register.getCountryId()), register.getMobilePhone(), null,
														register.getMobilePhonePrefix(), register.getPassword(), register.getPassword(),
														register.getIdNum(), String.valueOf(request.getFingerPrint()));
		String navCase = null;
		try {
			// validations are made in insert method
			navCase = registerForm.insertUser();
		} catch (Exception e) {
			log.error("Couldn't insert user", e);
		}

		UserMethodResult result = new UserMethodResult();
		if (isSuccessfulNavigationCase(navCase)) {
			il.co.etrader.web.bl_vos.User user = (il.co.etrader.web.bl_vos.User) httpRequest.getSession().getAttribute(Constants.BIND_SESSION_USER);
			result.setUser(user);
			result.setUserRegulation(user.getUserRegulation());
			result.setMinIvestmentAmount(user.getMinInvest());
		} else {
			log.debug("Insert or login was not successful for user: " + register.getEmail());
			result.setErrorCode(ERROR_CODE_UNKNOWN);
			addErrorMessages(result);
			
		}
		return result;
	}

	public static UserMethodResult getUser(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getUser: " + request);
		UserMethodResult result = new UserMethodResult();
		String navCase = null;
		UIViewRoot vR = new UIViewRoot();
		vR.setViewId("3153596692497891920L");
		FacesContext context = FacesContext.getCurrentInstance();
		context.setViewRoot(vR);
		if (context.getExternalContext().getRemoteUser() == null) {
			if (request.getUserName() != null && request.getPassword() != null && !request.isEncrypt()) {
				LoginForm loginForm = new LoginForm(request.getUserName(), request.getPassword());
				try {
					navCase = loginForm.login();
				} catch (Exception e) {
					log.debug("Unable to login user: " + request.getUserName(), e);
					result.setErrorCode(ERROR_CODE_UNKNOWN);
					return result;
				}
			} else {
				result.setErrorCode(ERROR_CODE_SESSION_EXPIRED);
				return result;
			}
		} else {
			navCase = httpRequest.getRequestURI();
		}
		if (isSuccessfulNavigationCase(navCase) && context.getExternalContext().getRemoteUser() != null) {
			il.co.etrader.web.bl_vos.User user = (il.co.etrader.web.bl_vos.User) httpRequest.getSession()
																							.getAttribute(Constants.BIND_SESSION_USER);
			result.setUser(user);
			result.setMinIvestmentAmount(user.getMinInvest());
			result.setUserRegulation(context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER_REGULATION,
																					UserRegulationBase.class));
		} else {
			log.debug("Login was not successful for user: " + request.getUserName());
			result.setErrorCode(ERROR_CODE_UNKNOWN);
			addErrorMessages(result);
		}
		return result;
	}

	public static CardMethodResult insertCard(CardMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("insertCard: " + request);
		CardMethodResult result = new CardMethodResult();
		FacesContext context = FacesContext.getCurrentInstance();
		UIViewRoot vR = new UIViewRoot();
		vR.setViewId("3153596692497891920L");
		FacesContext.getCurrentInstance().setViewRoot(vR);
		if (context.getExternalContext().getRemoteUser() != null) {
			CreditCard card = request.getCard();
			NewCardForm newCardForm;
			try {
				newCardForm = new NewCardForm(	card.getTypeId(), card.getCcNumber(), card.getCcPass(), card.getExpMonth(),
															card.getExpYear(), card.getHolderName(), card.getHolderId());
			} catch (SQLException e) {
				log.debug("Unable to initialize new card form", e);
				result.setErrorCode(ERROR_CODE_UNKNOWN);
				return result;
			}
			il.co.etrader.web.bl_vos.User user;
			try {
				user = ApplicationData.getUserFromSession();
			} catch (Exception e) {
				log.debug("Cannot load user from session", e);
				result.setErrorCode(ERROR_CODE_UNKNOWN);
				return result;
			}
			try {
				if (newCardForm.validateCCInput(context) && newCardForm.validateCreditCardNumber(context, user)) {
					long writerId = il.co.etrader.util.CommonUtil.getWebWriterId();
					if (il.co.etrader.util.CommonUtil.isMobile()) {
						writerId = com.anyoption.common.beans.Writer.WRITER_ID_AO_MINISITE;
					}
					result.setCard(TransactionsManagerBase.insertCreditCard(newCardForm.getTypeId(), newCardForm.getCcNum(),
																			newCardForm.getCcPass(), newCardForm.getExpMonth(),
																			newCardForm.getExpYear(), newCardForm.getHolderName(),
																			newCardForm.getHolderIdNum(), null, writerId, user,
																			Constants.WEB_CONST, new FacesMessage(), "newCardForm"));
				} else {
					result.setErrorCode(ERROR_CODE_INVALID_INPUT);
					addErrorMessages(result);
				}
			} catch (	SQLException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
						| BadPaddingException | UnsupportedEncodingException | InvalidAlgorithmParameterException e) {
				log.debug("Cannot insert credit card", e);
				result.setErrorCode(ERROR_CODE_UNKNOWN);
				return result;
			} catch (Exception e) {
				log.debug("Unable to validate credit card number", e);
				result.setErrorCode(ERROR_CODE_UNKNOWN);
				return result;
			}
		} else {
			result.setErrorCode(ERROR_CODE_SESSION_EXPIRED);
//			addErrorMessages(result);
		}
		return result;
	}

	public static TransactionMethodResult insertDepositCard(CcDepositMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("insertDepositCard: " + request);
		TransactionMethodResult result = new TransactionMethodResult();
		FacesContext context = FacesContext.getCurrentInstance();
		String navCase;
		UIViewRoot vR = new UIViewRoot();
		vR.setViewId("3153596692497891920L");
		FacesContext.getCurrentInstance().setViewRoot(vR);
		if (context.getExternalContext().getRemoteUser() != null) {
			try {
				DepositForm depositForm = new DepositForm(request.getAmount(), request.getCardId(), request.getCcPass());
				navCase = depositForm.insertDeposit();
			} catch (Exception e) {
				log.debug("Cannot insert credit card deposit", e);
				result.setErrorCode(ERROR_CODE_UNKNOWN);
				return result;
			}
		} else {
			result.setErrorCode(ERROR_CODE_SESSION_EXPIRED);
//			addErrorMessages(result);
			return result;
		}
		if (isSuccessfulNavigationCase(navCase)) {
			Transaction t = context.getApplication().evaluateExpressionGet(context, Constants.BIND_TRANSACTION, Transaction.class);
			il.co.etrader.web.bl_vos.User user = (il.co.etrader.web.bl_vos.User) httpRequest.getSession()
																							.getAttribute(Constants.BIND_SESSION_USER);
			if (t.getStatusId() == TransactionsManagerBase.TRANS_STATUS_PENDING
					|| t.getStatusId() == TransactionsManagerBase.TRANS_STATUS_SUCCEED) {
				try {
					TransactionsManagerBase.updatePixelRunTime(t.getId());
				} catch (SQLException e) {
					log.debug("Cannot update pixel run time", e);
					result.setErrorCode(ERROR_CODE_UNKNOWN);
					return result;
				}
				result.setBalance(user.getBalance() / 100.0);
				result.setTransactionId(t.getId());
				result.setAmount(t.getAmount() / 100d);
				result.setDollarAmount(CommonUtil.convertToBaseAmount(t.getAmount(), user.getCurrencyId(), new Date()) / 100);
				try {
					result.setFirstDeposit(TransactionsManagerBase.countRealDeposit(user.getId()) == 1 ? true : false);
				} catch (SQLException e) {
					log.debug("Cannot set first deposit", e);
					result.setErrorCode(ERROR_CODE_UNKNOWN);
					return result;
				}
				result.setClearingProvider(t.getClearingProviderDescriptor());
			} else if (t.getStatusId() == TransactionsManagerBase.TRANS_STATUS_AUTHENTICATE) { // 3d secure
				result.setTransactionId(t.getId());
				ClearingInfo ci = context.getApplication().evaluateExpressionGet(	context,
																					il.co.etrader.util.ConstantsBase.BIND_CLEARING_INFO,
																					ClearingInfo.class);
				ThreeDParams threeDParams = new ThreeDParams();
				threeDParams.setAcsUrl(ci.getAcsUrl());
				threeDParams.setPaReq(ci.getPaReq());
				long writerId = il.co.etrader.util.CommonUtil.getWebWriterId();
				if (il.co.etrader.util.CommonUtil.isMobile()) {
					writerId = com.anyoption.common.beans.Writer.WRITER_ID_AO_MINISITE;
				}
				threeDParams.setMD(String.valueOf(ci.getTransactionId()) + ";" + writerId + ";" + user.getUtcOffset());
				// termUrl will redirect to the json service servlet
				String termUrl = CommonUtil.getProperty("homepage.url");
				String jsonServiceUrl = CommonUtil.getProperty("web.jsonService.url");
				if (termUrl.endsWith("/") && jsonServiceUrl.startsWith("/")) {
					termUrl += jsonServiceUrl.substring(1) + "RedirectService";
				}
				threeDParams.setTermUrl(termUrl);
				threeDParams.setForm(ci.getFormData());
				t.setThreeDParams(threeDParams);
				t.setThreeD(true);
				httpRequest.getSession().setAttribute("transaction", t.getId());
			} else {
				result.setErrorCode(ERROR_CODE_GENERAL_VALIDATION);
				addErrorMessages(result);
			}
		} else {
			log.debug("Insert deposit was unsuccessful for user: " + context.getExternalContext().getRemoteUser());
			result.setErrorCode(ERROR_CODE_GENERAL_VALIDATION);
			addErrorMessages(result);
		}
		return result;
	}

	public static UserMethodResult updateUserAdditionalFields(UpdateUserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("updateUserAdditionalFields: " + request);
		UserMethodResult result = new UserMethodResult();
		FacesContext context = FacesContext.getCurrentInstance();
		UIViewRoot vR = new UIViewRoot();
		vR.setViewId("3153596692497891920L");
		FacesContext.getCurrentInstance().setViewRoot(vR);
		if (context.getExternalContext().getRemoteUser() != null) {
			try {
				com.anyoption.common.beans.base.User reqUser = request.getUser();
				Calendar cal = Calendar.getInstance();
				cal.set(Calendar.DAY_OF_MONTH, Integer.valueOf(reqUser.getBirthDayUpdate()));
				cal.set(Calendar.MONTH, Integer.valueOf(reqUser.getBirthMonthUpdate()));
				cal.set(Calendar.YEAR, Integer.valueOf(reqUser.getBirthYearUpdate()));
				AbstractDepositForm depositForm = new AbstractDepositForm(	reqUser.getCurrencyId(), reqUser.getStreet(),
																			reqUser.getCityName(), reqUser.getZipCode(), cal.getTime()) {

					private static final long serialVersionUID = -3153596692497891920L;
				};
				User user = context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER, User.class);
				depositForm.updateUserFields(user, false);
			} catch (SQLException e) {
				log.debug("Cannot update user additional fields", e);
				result.setErrorCode(ERROR_CODE_UNKNOWN);
				return result;
			}
		} else {
			result.setErrorCode(ERROR_CODE_SESSION_EXPIRED);
		}
		return result;
	}

	private static boolean isSuccessfulNavigationCase(String navCase) {
		return navCase != null && navCase != "";
	}

	private static void addErrorMessages(MethodResult result) {
		Iterator<FacesMessage> iterator = FacesContext.getCurrentInstance().getMessages();
		while (iterator.hasNext()) {
			FacesMessage fm = iterator.next();
			result.addErrorMessage(null, fm.getDetail());
		}
	}
}