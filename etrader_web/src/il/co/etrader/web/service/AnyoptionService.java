package il.co.etrader.web.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.annotations.PrintLogAnnotations;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.Skin;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.UserMigration;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.QuestionnaireGroup;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.jms.WebLevelLookupBean;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.CancelBubbleInvestmentMethodRequest;
import com.anyoption.common.service.requests.CancelInvestmentCheckboxStateMethodRequest;
import com.anyoption.common.service.requests.CancelInvestmentMethodRequest;
import com.anyoption.common.service.requests.CrossSaleRequest;
import com.anyoption.common.service.requests.DepositBonusBalanceMethodRequest;
import com.anyoption.common.service.requests.InsertInvestmentMethodRequest;
import com.anyoption.common.service.requests.InsertWithdrawCancelMethodRequest;
import com.anyoption.common.service.requests.InvestmentsMethodRequest;
import com.anyoption.common.service.requests.IsKnowledgeQuestionMethodRequest;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.requests.SellDynamicsInvestmentRequest;
import com.anyoption.common.service.requests.TermsFileMethodRequest;
import com.anyoption.common.service.requests.UnblockUserMethodRequest;
import com.anyoption.common.service.requests.UpdateUserQuestionnaireAnswersRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.requests.VisibleSkinMethodRequest;
import com.anyoption.common.service.requests.WithdrawBonusRegulationStateMethodRequest;
import com.anyoption.common.service.results.DepositBonusBalanceMethodResult;
import com.anyoption.common.service.results.InvestmentMethodResult;
import com.anyoption.common.service.results.InvestmentsMethodResult;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.TermsPartFilesMethodResult;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.common.service.results.UserQuestionnaireResult;
import com.anyoption.common.service.results.VisibleSkinListMethodResult;
import com.anyoption.common.service.results.VisibleSkinListMethodResult.VisibleSkinPair;
import com.anyoption.common.service.results.WithdrawBonusRegulationStateMethodResult;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.common.util.InvestmentValidatorDynamics;
import com.anyoption.common.util.MessageToFormat;
import com.anyoption.common.util.OpportunityCache;
import com.anyoption.common.util.OpportunityCacheBean;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.InvestmentsManager;
import il.co.etrader.web.mbeans.PageNames;
import il.co.etrader.web.service.result.OpenPositionsMethodResult;
import il.co.etrader.web.util.Constants;

/**
 * @author kirilim
 */
public class AnyoptionService extends CommonJSONService {

	private static final Logger log = Logger.getLogger(AnyoptionService.class);

	public static MethodResult cancelBubbleInvestment(CancelBubbleInvestmentMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("cancelBubbleInvestment:" + request);
		MethodResult result = new MethodResult();
		validateBubblesRequest(	concatBubblesRequestParams(request.getWriterId(), request.getUserId()), request.getS(),
								request.getWriterId(), result);
		if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
			return result;
		}
		ServletContext sc = httpRequest.getSession().getServletContext();
		WebLevelsCache wlc = (WebLevelsCache) sc.getAttribute("levelsCache");
		boolean canceled = InvestmentsManagerBase.cancelInvestment(request.getInvestmentId(), request.getWriterId(), true, wlc, request.getUserId());
		if (!canceled) {
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}

		return result;
	}
	
	public static DepositBonusBalanceMethodResult getUserDepositBonusBalance(DepositBonusBalanceMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getUserDepositBonusBalance:" + request);
		DepositBonusBalanceMethodResult result = new DepositBonusBalanceMethodResult();
		//httpRequest.isUserInRole("web")
		String remoteUser = httpRequest.getRemoteUser();
		if(remoteUser == null){
			log.debug("User is not logged");
			result.setErrorCode(ERROR_CODE_SESSION_EXPIRED);
			return result;
		}
		User user = (User) httpRequest.getSession().getAttribute("user");
		if(user.getId() != request.getUserId()){
			log.debug("Requested user is not equal with logged user");
			result.setErrorCode(ERROR_CODE_INVALID_INPUT);
			return result;
		}		
		result = getDepositBonusBalanceBaseBase(request);			
		return result;
	}
	
	public static MethodResult closeSuccessDoneQuestPopUp(MethodRequest request, HttpServletRequest httpRequest) {
		log.debug("closeSuccessDoneQuestPopUp:" + request);
		MethodResult result = new MethodResult();
		String remoteUser = httpRequest.getRemoteUser();
		if(remoteUser == null){
			log.debug("User is not logged");
			result.setErrorCode(ERROR_CODE_SESSION_EXPIRED);
			return result;
		}		
		httpRequest.getSession().removeAttribute(Constants.SESSION_AO_REGULATION_QUESTIONNAIRE_RESULT);
		log.debug("SESSION_AO_REGULATION_QUESTIONNAIRE_RESULT Attribute removed from session");
		
		return result;
	}
	
	
	public static MethodResult updateIsKnowledgeQuestion(IsKnowledgeQuestionMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("updateIsKnowledgeQuestion:" + request);
		MethodResult result = new MethodResult();
		String remoteUser = httpRequest.getRemoteUser();
		if(remoteUser == null){
			log.debug("User is not logged");
			result.setErrorCode(ERROR_CODE_SESSION_EXPIRED);
			return result;
		}
		User user = (User) httpRequest.getSession().getAttribute("user");
		if(user.getId() != request.getUserId()){
			log.debug("Requested user is not equal with logged user");
			result.setErrorCode(ERROR_CODE_INVALID_INPUT);
			return result;
		}		
		result = updateIsKnowledgeQuestion(user.getId(), request.isKnowledge(), Writer.WRITER_ID_WEB);
		return result;
	}

	public static WithdrawBonusRegulationStateMethodResult getWithdrawBonusRegulationState(	WithdrawBonusRegulationStateMethodRequest request,
																							HttpServletRequest httpRequest) {
		log.debug("getWithdrawBonusRegulationState: " + request);
		WithdrawBonusRegulationStateMethodResult result = new WithdrawBonusRegulationStateMethodResult();
		String remoteUser = httpRequest.getRemoteUser();
		if (remoteUser == null) {
			log.debug("User is not logged");
			result.setErrorCode(ERROR_CODE_SESSION_EXPIRED);
			return result;
		}
		User user = (User) httpRequest.getSession().getAttribute("user");
		getWithdrawBonusRegulationState(result, user.getId(), user.getSkinId(), request.getTransactionTypeId(), request.getCcId(),
										request.getWriterId(), user.getUtcOffset(), request.getIp());
		return result;
	}

    public static MethodResult insertWithdrawCancel(InsertWithdrawCancelMethodRequest request, HttpServletRequest httpRequest) {
    	log.debug("insertWithdrawCancel: " + request);
    	MethodResult result = new MethodResult();
    	String remoteUser = httpRequest.getRemoteUser();
    	if (remoteUser == null) {
			log.debug("User is not logged");
			result.setErrorCode(ERROR_CODE_SESSION_EXPIRED);
			return result;
		}
		User user = (User) httpRequest.getSession().getAttribute("user");
		insertWithdrawCancel(	result, user.getId(), user.getSkinId(), request.getTransactionTypeId(), request.getCcId(),
								request.getWriterId(), user.getUtcOffset(), request.getIp());
    	return result;
    }

	public static MethodResult unblockUserForThreshold(UnblockUserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("ublockUserForTreshold:" + request);
		MethodResult result = new MethodResult();
		String remoteUser = httpRequest.getRemoteUser();
		if (remoteUser == null) {
			log.debug("User is not logged");
			result.setErrorCode(ERROR_CODE_SESSION_EXPIRED);
			return result;
		}
		User user = (User) httpRequest.getSession().getAttribute("user");
		if (user.getId() != request.getUserId()) {
			log.debug("Requested user is not equal with logged user");
			result.setErrorCode(ERROR_CODE_INVALID_INPUT);
			return result;
		}
		result = unblockUserForThreshold(user.getId(), Writer.WRITER_ID_WEB);
		return result;
	}

//	public static ChartDataResult getChartDataCommon(ChartDataRequest request, OpportunityCache oc, ChartHistoryCache chc, WebLevelsCache wlc, HttpServletRequest httpRequest) {
//		System.out.println("Start executing GetChartDataCommon from..........." + AnyoptionService.class);
//		return CommonJSONService.getChartDataCommon(request, oc, chc, wlc, httpRequest);
//	}
	
	public static InvestmentMethodResult insertDynamicsInvestment(InsertInvestmentMethodRequest request, OpportunityCache oc, WebLevelsCache levelsCache, HttpServletRequest httpRequest) {
		long loginId = 0L;
		HttpSession session = httpRequest.getSession();
		// get user
		User user = (User) session.getAttribute("user");
		if(user != null && user.getId() >0 ) {
			final Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(user.getSkinId()).getDefaultLanguageId()).getCode());
			user.setLocale(locale);
			try {
				if (session.getAttribute(ConstantsBase.LOGIN_ID) != null) {
					loginId = (Long) session.getAttribute(ConstantsBase.LOGIN_ID);
				}
			} catch (Exception e) {
				log.error("Cannot get login ID from session!", e);
				loginId = 0L;
			}
		    String sessionId = session.getId();
	        
	        InvestmentMethodResult rez = validateRequest(user);
	        if(rez.getErrorCode() == ERROR_CODE_SUCCESS ) {
	        	rez = CommonJSONService.insertInvestmentDynamics(request, oc, user, levelsCache, loginId, sessionId);
	        }
	        return rez;
		} else {
	    	InvestmentMethodResult result = new InvestmentMethodResult();
	    	result.setErrorCode(ERROR_CODE_SESSION_EXPIRED);
	        return result;

		}
	}
	
	public static InvestmentMethodResult sellAllDynamicsInvestments(SellDynamicsInvestmentRequest request, OpportunityCache oc, WebLevelsCache levelsCache, HttpServletRequest httpRequest) {
		long loginId = 0L;
		HttpSession session = httpRequest.getSession();
		// get user
		User user = (User) session.getAttribute("user");
		try {
			if (session.getAttribute(ConstantsBase.LOGIN_ID) != null) {
				loginId = (Long) session.getAttribute(ConstantsBase.LOGIN_ID);
			}
		} catch (Exception e) {
			log.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}

        InvestmentMethodResult rez = validateRequest(user);
        OpportunityCacheBean opp;
		try {
			opp = oc.getOpportunity(request.getOpportunityId());
		} catch (SQLException e) {
			log.error("Cannot get opportunity:"+ request.getOpportunityId(), e);
			rez.setErrorCode(ERROR_CODE_UNKNOWN);
			return rez;
		}
		if(rez.getErrorCode() == 0 && opp != null) {
			// Deviation check for close
			WebLevelLookupBean wllb = new WebLevelLookupBean();
		    wllb.setSkinGroup(SkinsManagerBase.getSkin(user.getSkinId()).getSkinGroup());	            
	        Market market = MarketsManagerBase.getMarket(opp.getMarketId());
	        wllb.setOppId(opp.getId());
	        wllb.setFeedName(market.getFeedName());
	        AnyoptionServiceServlet.getLevelsCache().getLevels(wllb);

	        MessageToFormat m1 = InvestmentValidatorDynamics.deviationCheck(wllb, opp, Investment.INVESTMENT_TYPE_DYNAMICS_SELL, request.getBid(), user.getUserName(), httpRequest.getSession().getId(), null);
	        MessageToFormat m2 = InvestmentValidatorDynamics.deviationCheck(wllb, opp, Investment.INVESTMENT_TYPE_DYNAMICS_BUY,  request.getOffer(), user.getUserName(), httpRequest.getSession().getId(), null);
	        
			if(m1 != null || m2 != null) {
				rez.setErrorCode(ERROR_CODE_INV_VALIDATION);
				return rez;
			}
			ArrayList<Long> list = InvestmentsManagerBase.getUserOpenedInvestments(user.getId(), opp.getId(), 0);
			for(int i =0;i<list.size();i++) {
				Long investmentId = list.get(i);
				if(InvestmentsManagerBase.sellDynamicsInvestment(investmentId, 0l, request.getOffer(), request.getBid(), Writer.WRITER_ID_WEB, loginId, user, wllb.getRealLevel(), AnyoptionServiceServlet.getLevelsCache())){
					log.debug("Successfuly closed: "+ investmentId);
				} else { 
					log.debug("can not close" + investmentId);
				}
	        }
		}
        
	    return rez;
	}

	public static InvestmentMethodResult sellDynamicsInvestment(SellDynamicsInvestmentRequest request, OpportunityCache oc, WebLevelsCache levelsCache, HttpServletRequest httpRequest) {
		long loginId = 0L;
		HttpSession session = httpRequest.getSession();
		// get user
		User user = (User) session.getAttribute("user");
		try {
			if (session.getAttribute(ConstantsBase.LOGIN_ID) != null) {
				loginId = (Long) session.getAttribute(ConstantsBase.LOGIN_ID);
			}
		} catch (Exception e) {
			log.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}

        InvestmentMethodResult rez = validateRequest(user);
        OpportunityCacheBean opp;
		try {
			opp = oc.getOpportunity(request.getOpportunityId());
		} catch (SQLException e) {
			log.error("Cannot get opportunity:"+ request.getOpportunityId(), e);
			rez.setErrorCode(ERROR_CODE_UNKNOWN);
			return rez;
		}
		if(rez.getErrorCode() == 0 && opp != null) {
			// Deviation check for close
			WebLevelLookupBean wllb = new WebLevelLookupBean();
		    wllb.setSkinGroup(SkinsManagerBase.getSkin(user.getSkinId()).getSkinGroup());	            

	        Market market = MarketsManagerBase.getMarket(opp.getMarketId());
	        wllb.setOppId(opp.getId());
	        wllb.setFeedName(market.getFeedName());
	        AnyoptionServiceServlet.getLevelsCache().getLevels(wllb);
	        
	        double quote = request.getInvestmentTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_SELL ? request.getBid() : request.getOffer();
	        MessageToFormat m = InvestmentValidatorDynamics.deviationCheck(wllb, opp, request.getInvestmentTypeId(), quote, user.getUserName(), httpRequest.getSession().getId(), null);
			
			if(m != null) {
				rez.setErrorCode(ERROR_CODE_INV_VALIDATION);
				return rez;
			}
			// close for
	        if(InvestmentsManagerBase.sellDynamicsInvestment(request.getInvestmentId(), (long)(request.getFullAmount() / 100 ), request.getOffer(), request.getBid(), request.getWriterId(), loginId, user, wllb.getRealLevel(), AnyoptionServiceServlet.getLevelsCache())){
	        	Investment inv = new Investment();
	        	inv.setId(request.getInvestmentId());
	        	rez.setInvestment(inv);
	        } else {
	        	rez.setErrorCode(ERROR_CODE_UNKNOWN);
	        }
		}
        
	    return rez;
	}
	
	
	private static InvestmentMethodResult validateRequest(User user) {
		InvestmentMethodResult result = new InvestmentMethodResult();
		result.setErrorCode(ERROR_CODE_SUCCESS);
		// return error - no user logged
	    if (null == user || user.getId() == 0) {
	    	result.setErrorCode(ERROR_CODE_SESSION_EXPIRED);
	        return result;
	    }
	    if(!user.getProductViewFlags().get(ConstantsBase.HAS_DYNAMICS_FLAG)) {
	    	log.error("user :" + user.getId() + " is not dynamics enabled !");
	    	result.setErrorCode(ERROR_CODE_UNKNOWN);
	        return result;
	    }
	    	
        validateUserRegulationAndNonReg(user, result, VALIDATE_REGULATION_INVESTMENT);
        if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
            return result;
        }	   
		return result;
	}
	
	public static TermsPartFilesMethodResult getTermsFiles(TermsFileMethodRequest request) {
		log.debug("getTermsFile:" + request);
		TermsPartFilesMethodResult result = getTermsFilesBase(request, false);
		return result;
	}
	
	@PrintLogAnnotations(stopPrintDebugLog = true)
	public static InvestmentsMethodResult getUserInvestments(InvestmentsMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getUserInvestments: " + request);
		InvestmentsMethodResult result = new InvestmentsMethodResult();
		try {
			User user = (User) httpRequest.getSession().getAttribute("user");
			getUserInvestments(result, user, request.getFrom(), request.getTo(), request.isSettled(),
					request.getMarketId(), request.getSkinId(), request.getWriterId(), request.getUtcOffset(),
					request.getPeriod(), request.getStartRow(), request.getPageSize());
			initUser(result, user, request.getUtcOffset(), request.getWriterId());
		} catch (Exception e) {
			log.error("Error process invest.", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
		return result;
	}

	public static InvestmentMethodResult cancelInvestment(CancelInvestmentMethodRequest request, OpportunityCache oc, WebLevelsCache levelsCache, HttpServletRequest httpRequest) {
		InvestmentMethodResult result = new InvestmentMethodResult();
		long loginId = 0L;
		HttpSession session = httpRequest.getSession();
		String remoteUser = httpRequest.getRemoteUser();
		if (remoteUser == null) {
			log.debug("User is not logged");
			result.setErrorCode(ERROR_CODE_SESSION_EXPIRED);
			return result;
		}
		// get user		
		User user = (User) session.getAttribute("user");
		try {
			if (session.getAttribute(ConstantsBase.LOGIN_ID) != null) {
				loginId = (Long) session.getAttribute(ConstantsBase.LOGIN_ID);
			}
		} catch (Exception e) {
			log.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}

		if (null == user || user.getId() == 0) {
		   	result.setErrorCode(ERROR_CODE_SESSION_EXPIRED);
		   	return result;
		}

	    try {
	    		long cancelSecondsCumulative = (Integer.parseInt(CommonUtil.getProperty("cancel.seconds.client")) + Integer.parseInt(CommonUtil.getProperty("cancel.seconds.server")))* 1000;
	    		boolean success = CommonJSONService.cancelInvestment(request, user, oc, levelsCache, session, loginId, cancelSecondsCumulative);
	    		if(!success) {
	    			result.setErrorCode(ERROR_CODE_UNKNOWN);
	    		}
		} catch (SQLException e) {
			log.error("Error canceling "+ request, e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}

	    return result;
	}

	public static MethodResult changeCancelInvestmentCheckboxState(CancelInvestmentCheckboxStateMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("changeCancelInvestmentCheckboxState:" + request);
		MethodResult result = new MethodResult();
		HttpSession session = httpRequest.getSession();
		String remoteUser = httpRequest.getRemoteUser();
		if (remoteUser == null) {
			log.debug("User is not logged");
			result.setErrorCode(ERROR_CODE_SESSION_EXPIRED);
			return result;
		}
		User user = (User) session.getAttribute("user");
		changeCancelInvestmentCheckboxState(result, request, user);
		return result;
	}
	
	public static VisibleSkinListMethodResult getVisibleSkinsWeb (VisibleSkinMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getVisibleSkinsWeb:" + request);
		Long skinId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.SESSION_SKIN);
		if(skinId == null){
			skinId = request.getSkinId();
		}
		Skin s = SkinsManagerBase.getSkin(skinId);
		Long skinListTypeDetect = SkinsManagerBase.getSkinListType(request.getIp(), httpRequest, s);		
		VisibleSkinListMethodResult result = new VisibleSkinListMethodResult();
		try {
			ArrayList<VisibleSkinPair> list = new ArrayList<>();
			for (Long visibleSkinId : SkinsManagerBase.getVisibleSkins().get(skinListTypeDetect)) {
				if(!visibleSkinId.equals(skinId)){
					String prefix = SkinsManagerBase.getSkin(visibleSkinId).getLocale();
					String prettyURL = getPrettyURL(prefix, request.getUri(), visibleSkinId);
					list.add( result.new VisibleSkinPair(visibleSkinId, prettyURL, prefix));
				}
			}
			result.setVisibleSkins(list);			
			result.setRegulated(s.isRegulated());
		} catch (SQLException e) {
			log.error("Error when getVisibleSkins", e);
		}
		return result;
	}
	
	public static String CHANGE_SKIN_JSF = "skinChange.jsf";
	public static String CHANGE_SKIN_PARAM = "s";
	public static String CHANGE_SKIN_PRETTY_URL = "pageName";
	private static String getPrettyURL(String prefix, String uri, Long skinId){
		String reloadUrl = "";
		String prettyURL = PageNames.getPageNameStatic(prefix, uri);
		prettyURL = prettyURL.substring(1, prettyURL.length());
		if(!CommonUtil.isParameterEmptyOrNull(prettyURL)){
			prettyURL = "&" + CHANGE_SKIN_PRETTY_URL + "="+ prettyURL;
		}
		reloadUrl = getSubDomainVisibleSkin(prefix);
		reloadUrl += "/" + CHANGE_SKIN_JSF + "?" + CHANGE_SKIN_PARAM  + "=" + skinId + prettyURL + "&t=" + System.currentTimeMillis();
		return reloadUrl;
	}
	
    private static String getSubDomainVisibleSkin(String skin) {
    	String res = "";
    	if (skin.equals("en")) {
    		res = CommonUtil.getProperty("homepage.url");
    	} else {
    		String subDomain = ApplicationData.getSubDomainBySkin(skin, false);
    		if (CommonUtil.isParameterEmptyOrNull(subDomain)) {
    			subDomain = ApplicationData.getSubDomainBySkin(skin, true);
    		}
    		// FIXME: Tony - make this one work properly
            if (skin.equals(Skin.LOCALE_SUBDOMAIN_ITALIAN)) {
                subDomain = ApplicationData.getSkinById(Skin.SKIN_REG_IT).getSubdomain();
            }
    		if(!CommonUtil.isParameterEmptyOrNull(subDomain)) {
    			res = CommonUtil.getProperty("homepage.url").replace("www", subDomain);
    			if (skin.equals(Skin.LOCALE_SUBDOMAIN_ITALIAN)) {
    			    res = res.replace(Constants.HOST_ANYOPTION, Constants.HOST_ANYOPTION_IT);
    			}
    		} else {
    			res = CommonUtil.getProperty("homepage.url");
    		}    		
    	}
    	return res.substring(0, res.length() - 1);
    }
    
	public static UserQuestionnaireResult getUserSingleQuestionnaireDynamic(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getUserSingleQuestionnaireDynamic:" + request);
		User user = (User) httpRequest.getSession().getAttribute("user");

		return getUserSingleQuestionnaireDynamic(user, request.getWriterId());
    }
	
    public static UserMethodResult updateUserQuestionnaireAnswers(UpdateUserQuestionnaireAnswersRequest request, HttpServletRequest httpRequest) {
    	log.debug("updateUserQuestionnaireAnswers: " + request);
    	UserQuestionnaireResult result = new UserQuestionnaireResult();
    	try {
    		User user = (User) httpRequest.getSession().getAttribute("user");
    		if(user == null || user.getId() == 0 ) {
				result.setErrorCode(ERROR_CODE_UNKNOWN);
	            return result;
	        }

			return updateUserQuestionnaireAnswers(user, request, result);
		} catch (Exception e) {
			log.error("Error in update questionnaire answers", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
			return result;
		}
    }
    
    public static UserMethodResult updateSingleQuestionnaireDone(UserMethodRequest request, HttpServletRequest httpRequest) {
    	UserMethodResult result = new UserMethodResult();
    	try {
	    	log.debug("updateQuestionnaireDone: " + request);
	    	il.co.etrader.bl_vos.UserBase user = (il.co.etrader.bl_vos.UserBase) httpRequest.getSession().getAttribute("user");
    		if(user == null || user.getId() == 0 ) {
				result.setErrorCode(ERROR_CODE_UNKNOWN);
	            return result;
	        }
			result = updateQuestionnaireDone(user, user.getWriterId(), request.getUtcOffset(), QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME, httpRequest);
			
			//httpRequest.getSession().setAttribute(Constants.SESSION_AO_REGULATION_QUESTIONNAIRE_RESULT, );
			httpRequest.getSession().setAttribute(Constants.SESSION_IS_SINGLE_QUEST_DONE, true);
			
			if(result.getErrorCode() == ERROR_CODE_REG_SUSPENDED_QUEST_INCORRECT ||
				 result.getErrorCode() == ERROR_CODE_REGULATION_USER_RESTRICTED) {
					UsersManagerBase.sendMailAfterQuestAORegulation(result.getUserRegulation());
			}
			
			if(result.getUserRegulation().getPepState() == UserRegulationBase.PEP_AUTO_PROHIBITED) {
				UsersManagerBase.sendMailTemplateFile(ConstantsBase.TEMPLATE_REGULATION_PEP, (long)Writer.WRITER_ID_WEB, user, null, new String(), new String(), null);
			}
			return result;
	    } catch (Exception e) {
			log.error("Error in update [" + QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME + "] questionnaire status", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
			return result;
		}
    }

	public static OpenPositionsMethodResult getOpenPositions(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getOpenPositions: " + request);
		OpenPositionsMethodResult result = new OpenPositionsMethodResult();
		il.co.etrader.web.bl_vos.User user = (il.co.etrader.web.bl_vos.User) httpRequest.getSession()
																						.getAttribute(Constants.BIND_SESSION_USER);
		if (user == null || user.getId() == 0) {
			result.setErrorCode(ERROR_CODE_SESSION_EXPIRED);
			return result;
		}
		InvestmentsManager.loadOpenPositions(result, user.getId(), user.getSkinId());
		return result;
	}
}