package il.co.etrader.web.chart;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.util.Constants;

import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.MarketRate;
import com.anyoption.common.charts.ChartHistoryCache;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.util.OpportunityCache;
import com.anyoption.common.util.OpportunityCacheBean;

public class ChartDataServlet extends HttpServlet {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1709844672336463640L;
	private static final Logger log = Logger.getLogger(ChartDataServlet.class);

    public void init() throws ServletException {
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        long t = System.currentTimeMillis();

        ServletContext sc = request.getServletContext();

        long marketId = 0;
        long oppId = 0;
        int history = 0;
        OpportunityCacheBean opp = null;
        if (null != request.getParameter("marketId")) {
            try {
                marketId = Long.parseLong((String) request.getParameter("marketId"));
            } catch (Exception e) {
                // don't log the stack. Google is dumb and passing "undefined" for params
                log.warn("Can't parse marketId - " + e.getMessage());
            }
        }
        if (null != request.getParameter("oppId")) {
            try {
                oppId = Long.parseLong((String) request.getParameter("oppId"));
                if (oppId != 0) {
                    OpportunityCache oc = (OpportunityCache) sc.getAttribute(Constants.OPPORTUNITY_CACHE);
                    opp = oc.getOpportunity(oppId);
                }
            } catch (Exception e) {
                // don't log the stack. Google is dumb and passing "undefined" for params
                log.warn("Problem with the opp - " + e.getMessage());
            }
        }
        if (null != request.getParameter("history")) {
            try {
                history = Integer.parseInt((String) request.getParameter("history"));
            } catch (Exception e) {
                // don't log the stack. Google is dumb and passing "undefined" for params
                log.warn("Can't parse history - " + e.getMessage());
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("fillParams - marketId: " + marketId + " oppId: " + oppId + " history: " + history + " time to fill: " + (System.currentTimeMillis() - t));
        }

        PrintWriter pw = response.getWriter();
        pw.write("<chartData><settings><serverTime>");
        pw.write(String.valueOf(System.currentTimeMillis()));
        pw.write("</serverTime><timezoneOffset>");
        pw.write(String.valueOf(CommonUtil.getUtcOffsetInMin(CommonUtil.getUtcOffset(request.getSession(), request))));
        pw.write("</timezoneOffset><timeEstClose>");
        pw.write(null != opp ? String.valueOf(opp.getTimeEstClosing().getTime()) : "");
        pw.write("</timeEstClose><scheduled>");
        pw.write(null != opp ? String.valueOf(opp.getScheduled()) : "");
        pw.write("</scheduled><decimalPoint>");
        Market m = ApplicationData.getMarkets().get(marketId);
        pw.write(String.valueOf(null != m ? m.getDecimalPoint() : 0));
        pw.write("</decimalPoint><hasPreviousHour>");

        if (null != opp) {
            ChartHistoryCache chc = (ChartHistoryCache) sc.getAttribute("chartHistoryCache");
            Map<SkinGroup, List<MarketRate>> ratesMap = chc.getMarketHistory(marketId);
            List<MarketRate> l = null;
            if (ratesMap != null) {
            	l = ratesMap.get(ApplicationData.getSkinById(ApplicationData.getSkinId(request.getSession(), request)).getSkinGroup());
            }
            log.debug("rates from cache: " + (null != l ? l.size() : 0));
            if (null != l && l.size() > 0) {
                // Calculate current hour start time by the opp est closing time - 1 hour
                // because day opps are considered hour opps in the end of the day and their
                // opening time is in the morning
                long chst = opp.getTimeEstClosing().getTime() - 60 * 60 * 1000;
                while (chst > System.currentTimeMillis()) {
                    chst -= 60 * 60 * 1000;
                }
                pw.write(l.get(0).getRateTime().getTime() < chst ? "1" : "0");
                pw.write("</hasPreviousHour></settings><rates>");
                int sent = 0;
                MarketRate mr = null;
//                double graphRate = 0;
//                double dayRate = 0;
//                long skinId = ApplicationData.getSkinId(request.getSession(), request);
                
                for (int i = 0; i < l.size(); i++) {
                    mr = l.get(i);
//                    graphRate = skinId == Constants.SKIN_ETRADER ? mr.getGraphRate().doubleValue() : mr.getGraphRateAO().doubleValue();
//                    dayRate = skinId == Constants.SKIN_ETRADER ? mr.getDayRate().doubleValue() : mr.getDayRateAO().doubleValue();
                    if ((history == 0 ||
                            (history == 1 && mr.getRateTime().getTime() >= chst) ||
                            (history == 2 && mr.getRateTime().getTime() < chst)) &&
                        ((opp.getScheduled() == 1 && mr.getGraphRate().doubleValue() != 0) ||
                            (opp.getScheduled() != 1 && mr.getDayRate().doubleValue() != 0))) {
                        pw.write("<rate><time>");
                        pw.write(String.valueOf(mr.getRateTime().getTime()));
                        pw.write("</time><level>");
                        if (opp.getScheduled() == 1) {
                            pw.write(String.valueOf(mr.getGraphRate().doubleValue()));
                        } else {
                            pw.write(String.valueOf(mr.getDayRate().doubleValue()));
                        }
                        pw.write("</level></rate>");
                        sent++;
                    }
                }
                log.debug("rates sent: " + sent);
            } else {
                pw.write("0</hasPreviousHour></settings><rates>");
            }
        }

        pw.write("</rates></chartData>");
        pw.flush();
        pw.close();

        if (log.isDebugEnabled()) {
            log.debug("total time: " + (System.currentTimeMillis() - t));
        }
    }

    public void destroy() {
    }
}