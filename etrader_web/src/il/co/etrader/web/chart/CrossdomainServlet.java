package il.co.etrader.web.chart;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import il.co.etrader.web.util.Utils;

public class CrossdomainServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(CrossdomainServlet.class);
    
    public void init() throws ServletException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        long t = System.currentTimeMillis();

        response.setContentType("text/x-cross-domain-policy");
        response.setCharacterEncoding("UTF-8");
        try {
            PrintWriter pw = response.getWriter();
            String imgSubDomain = Utils.getPropertyByHost(request, "images.sub.domain");
            pw.write(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                    "<!DOCTYPE cross-domain-policy SYSTEM \"http://www.adobe.com/xml/dtds/cross-domain-policy.dtd\">" +
                    "<cross-domain-policy>" +
                        "<site-control permitted-cross-domain-policies=\"master-only\"/>" +
                        "<allow-access-from domain=\"" + imgSubDomain + "\"/>" +
                        "<allow-http-request-headers-from domain=\"" + imgSubDomain + "\" headers=\"*\"/>" +
                    "</cross-domain-policy>");
            pw.flush();
            pw.close();
        } catch (IOException ioe) {
            log.error("Can't write crossdomain.xml", ioe);
        }
        
        if (log.isDebugEnabled()) {
            log.debug("time: " + (System.currentTimeMillis() - t));
        }
    }

    public void destroy() {
    }
}