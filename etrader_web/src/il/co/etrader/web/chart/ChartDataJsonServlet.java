/**
 * 
 */
package il.co.etrader.web.chart;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.MarketRate;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.charts.ChartHistoryCache;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.jms.WLCOppsCacheBean;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.util.OpportunityCache;
import com.anyoption.common.util.OpportunityCacheBean;
import com.anyoption.common.util.PastExpiriesCache;

import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.InvestmentFormatter;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;

/**
 * Servlet writing chart data to the response in Json format
 * 
 * @author kirilim
 */
public class ChartDataJsonServlet extends HttpServlet {

	private static final long serialVersionUID = -2137929996015435594L;
	private static final Logger log = Logger.getLogger(ChartDataJsonServlet.class);

	public void init() throws ServletException {}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
		// Try to make sure that the chart data was not requested directly by someone else
		if (request.getHeader("referer") == null) {
			log.error("There is no Request Header Referer");
			PrintWriter pw = response.getWriter();
			pw.write("Access denied");
			pw.flush();
			pw.close();
			return;
		} else {
			String[] requestURLSplitted = request.getRequestURL().toString().split("/"); // 0 - protocol(i.e. http/https), 1 - empty,
																							// because of the //, 2 - domain
			String[] refererSplitted = request.getHeader("referer").split("/"); // same as above
			if (refererSplitted.length > 2 || requestURLSplitted.length > 2) {
				String requestURL = requestURLSplitted[2];
				String referer = refererSplitted[2];
				if (!requestURL.equals(referer)) {
					log.error("Request URL and Request Header Referer not equal. Request URL: " + requestURL + "; Referer: " + referer
								+ ". Returning nothing");
					return;
				}
			} else {
				log.error("Request URL and/or Request Header Referer too short. Request URL: " + requestURLSplitted.length + "; Referer: "
							+ refererSplitted.length + ". Returning nothing");
				return;
			}
		}

		long t = System.currentTimeMillis();
		response.setContentType("application/json;charset=UTF-8");
		response.addHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		String skinIdString = ((String) request.getSession().getAttribute("s"));
		if (skinIdString == null) {
			skinIdString = ApplicationData.getDefaultSkin(request);
		}
		Long skinId = Long.parseLong(skinIdString);
		long marketId = 0;
		long oppId = (request.getParameter("oppId") != null) ? Long.parseLong(request.getParameter("oppId")) : 0l;
		int history = 0;
		long typeId = (request.getParameter("typeId") != null) ? Long.parseLong(request.getParameter("typeId")) : 0l;
		int scheduled = (request.getParameter("scheduled") != null) ? Integer.valueOf(request.getParameter("scheduled")) : 0;
		if (null != request.getParameter("marketId")) {
			try {
				marketId = Long.parseLong((String) request.getParameter("marketId"));
				if (marketId == 0) {
					if (typeId == Opportunity.TYPE_REGULAR) {
						log.debug("marketId is 0. Taking home page 1st market.");
						marketId = getHomePageFirstMarket(request, skinId);
					} else {// Option +
						marketId = CommonJSONService.getOptionPlusMarketId(skinId);
					}
				}
			} catch (Exception e) {
				// don't log the stack. Google is dumb and passing "undefined" for params
				log.warn("Can't parse marketId - " + e.getMessage());
			}
		}
		if (null != request.getParameter("history")) {
			try {
				history = Integer.parseInt((String) request.getParameter("history"));
			} catch (Exception e) {
				// don't log the stack. Google is dumb and passing "undefined" for params
				log.warn("Can't parse history - " + e.getMessage());
			}
		}
		if (log.isDebugEnabled()) {
			log.debug("fillParams - marketId: " + marketId + " oppId: " + oppId + " history: " + history + " time to fill: "
						+ (System.currentTimeMillis() - t));
		}

		User user = (User) request.getSession().getAttribute("user");
		List<Investment> investments;
		try {
			if (user != null && user.getId() > 0) {
				long marketIdForInvestments = (typeId != Opportunity.TYPE_OPTION_PLUS) ? marketId : 0l;
				investments = InvestmentsManagerBase.getOpenedInvestmentsByUser(user.getId(), 0l, typeId, marketIdForInvestments);
			} else {
				investments = new ArrayList<Investment>();
			}
		} catch (SQLException e) {
			log.warn("Unable to load investments", e);
			investments = new ArrayList<Investment>();
		}
		composeChartData(response.getWriter(), marketId, request, investments, skinId, history, scheduled, oppId);

		if (log.isDebugEnabled()) {
			log.debug("total time: " + (System.currentTimeMillis() - t));
		}
	}

	public void destroy() {}

//	public static long getOptionPlusMarketId(long skinId) {
//		log.debug("marketId is 0. Calculating Option+ market for skin " + skinId);
//		long marketId;
//		try {
//			marketId = pickRandomMarket(skinId, true);
//			if (0 == marketId) {
//				marketId = pickRandomMarket(skinId, false);
//				if (0 == marketId) {
//					log.debug("No opened markets.");
//				}
//			}
//		} catch (SQLException e) {
//			log.debug("Can't get market", e);
//			return 0l;
//		}
//
//		return marketId;
//	}

	private long getHomePageFirstMarket(HttpServletRequest request, Long skinId) {
		ServletContext sc = request.getSession().getServletContext();
		WebLevelsCache wlc = (WebLevelsCache) sc.getAttribute("levelsCache");
		long[] hpm = wlc.getMarketsOnHomePage(skinId);
		return hpm[0];
	}

//	private static long pickRandomMarket(long skinId, boolean onlyOpened) throws SQLException {
//		ArrayList<Long> l = MarketsManagerBase.getOptionPlusCurrentMarket(skinId, onlyOpened);
//		if (l.size() > 0) {
//			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+0"));
//			int h = cal.get(Calendar.HOUR_OF_DAY);
//			int m = cal.get(Calendar.MINUTE);
//			cal.set(Calendar.HOUR_OF_DAY, 0);
//			cal.set(Calendar.MINUTE, 0);
//			cal.set(Calendar.SECOND, 0);
//			cal.set(Calendar.MILLISECOND, 0);
//			long seed = cal.getTimeInMillis() + (h * 60 + m) / 5;
//			if (log.isDebugEnabled()) {
//				log.debug("cal: " + cal.getTimeInMillis() + " h: " + h + " m: " + m + " seed: " + seed);
//			}
//			/**
//			 * The "random" number generated by the Random class depends on the seed. With the above calculation we make sure that on all
//			 * web servers we get the same seed for every 5 min period starting from 00:00 GMT (thus the same "random" number).
//			 * 
//			 * NOTE: large groups of consecutive seed numbers generate the same random number for a small interval
//			 */
//			Random rnd1 = new Random(seed);
//			Random rnd2 = new Random(rnd1.nextLong());
//			return l.get(rnd2.nextInt(l.size()));
//		} else {
//			return 0;
//		}
//	}
	
	public static String getChartData(long marketId, HttpServletRequest request, List<Investment> investments, Long skinId, int history) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintWriter pw = new PrintWriter(baos);
		composeChartData(pw, marketId, request, investments, skinId, history, 0, 0);
		return baos.toString();
	}
	
	private static void composeChartData(	PrintWriter pw, long marketId, HttpServletRequest request, List<Investment> investments,
											Long skinId, int history, int scheduled, long oppId) {
		ServletContext sc = request.getSession().getServletContext();
		WebLevelsCache wlc = (WebLevelsCache) sc.getAttribute("levelsCache");
		SortedSet<WLCOppsCacheBean> marketOpportunities = wlc.getMarketOpportunities(marketId);
		WLCOppsCacheBean opp = null;
		if (marketOpportunities != null && !marketOpportunities.isEmpty()) {
			Iterator<WLCOppsCacheBean> marketOpportunitiesIterator = wlc.getMarketOpportunities(marketId).iterator();
			if (oppId == 0 && scheduled < Opportunity.SCHEDULED_DAYLY) {
				opp = marketOpportunitiesIterator.next();
				if (opp.getState() > Opportunity.STATE_LAST_10_MIN) {
					boolean shouldChangeToNewOpp = false;
	        		WLCOppsCacheBean newOpp = null;
	        		if (marketOpportunitiesIterator.hasNext()) {
	        			newOpp = marketOpportunitiesIterator.next();
	        			if (newOpp.getSchedule() == 1) {
	        				shouldChangeToNewOpp = true;
	        			}
	        		}
					// investments check
					Iterator<Investment> investmentsIterator = investments.iterator();
					boolean changeOpp = true;
					while (investmentsIterator.hasNext()) {
						Investment investment = investmentsIterator.next();
						if (investment.getOpportunityId() == opp.getOpportunityId()) {
							changeOpp = false;
							break;
						}
					}
					if (changeOpp && shouldChangeToNewOpp) {
						opp = newOpp;
					}
				}
			} else if (oppId > 0) {
        		WLCOppsCacheBean tmp = null;
        		do {
        			tmp = marketOpportunitiesIterator.next();
        			if (tmp.getOpportunityId() == oppId) {
        				opp = tmp;
        				break;
        			}
        		} while (marketOpportunitiesIterator.hasNext());
			} else if (scheduled == Opportunity.SCHEDULED_DAYLY	|| scheduled == Opportunity.SCHEDULED_WEEKLY
						|| scheduled == Opportunity.SCHEDULED_MONTHLY || scheduled == Opportunity.SCHEDULED_QUARTERLY) {
				// there is a case when the weekly ends on the next month, the monthly opp will be before the weekly opp
				boolean applicantFound = false;
				while (marketOpportunitiesIterator.hasNext()) {
					WLCOppsCacheBean cacheOpp = marketOpportunitiesIterator.next();
					if (cacheOpp.getSchedule() == scheduled) {
						opp = cacheOpp;
						break;
					}
					if (!applicantFound && cacheOpp.getSchedule() > scheduled) {
						opp = cacheOpp;
						applicantFound = true;
					}
				}
			}
		}
		User user = (User) request.getSession().getAttribute("user");
		Market m = ApplicationData.getMarkets().get(marketId);
		// chartData begin
		pw.write("{");

		// 'settings' begin
		pw.write("'settings':{");

		// 'serverTime' begin
		pw.write("'serverTime':");
		pw.write(String.valueOf(System.currentTimeMillis()));
		// 'serverTime' end

		// 'timezoneOffset' begin
		pw.write(",'timezoneOffset':");
		pw.write(String.valueOf(CommonUtil.getUtcOffsetInMin(CommonUtil.getUtcOffset(request.getSession(), request))));
		// 'timezoneOffset' end

		Date cachedOppTimeFirstInvest = null;
		try {
			if (opp != null) {
				// 'timeFirstInvest' begin
				pw.write(",'timeFirstInvest':");
				OpportunityCache opportunitiesCache = (OpportunityCache) sc.getAttribute(Constants.OPPORTUNITY_CACHE);
				OpportunityCacheBean cachedOpportunity = opportunitiesCache.getOpportunity(opp.getOpportunityId());
				pw.write(String.valueOf(cachedOpportunity.getTimeFirstInvest().getTime()));
				cachedOppTimeFirstInvest = cachedOpportunity.getTimeFirstInvest();
				// 'timeFirstInvest' end
			}
		} catch (SQLException e) {
			log.debug("Unable to load time first invest for opportunity with id: " + opp.getOpportunityId(), e);
		}

		// 'timeEstClose' begin
		pw.write(",'timeEstClose':");
		pw.write(null != opp ? String.valueOf(opp.getTimeEstClosing().getTime()) : "''");
		// 'timeEstClose' end

		// 'scheduled' begin
		pw.write(",'scheduled':");
		pw.write(null != opp ? String.valueOf(opp.getSchedule()) : "''");
		// 'scheduled' end

		// 'decimalPoint' begin
		pw.write(",'decimalPoint':");
		pw.write(String.valueOf(null != m ? m.getDecimalPoint() : 0));
		// 'decimalPoint' end

		// 'balance' begin
		pw.write(",'balance':'");
		try {

			pw.write((user != null && user.getId() > 0)	? user.getBalanceTxt()
														: CommonUtil.displayAmount(0l, ApplicationData.getSkinById(skinId)
																										.getDefaultCurrencyId()));
		} catch (SQLException e) {
			log.debug("Can't get balance txt", e);
			pw.write("0.00");
		}
		pw.write("'");
		// 'balance' end

		// 'marketId' begin
		pw.write(",'marketId':");
		pw.write(String.valueOf(marketId));
		// 'marketId' end

		// 'opportunityId' begin
		pw.write(",'opportunityId':");
		pw.write((opp != null) ? String.valueOf(opp.getOpportunityId()) : "''");
		// 'opportunityId' end

		// 'hasPreviousHour' begin
		pw.write(",'hasPreviousHour':");

		if (opp != null) {
			ChartHistoryCache chc = (ChartHistoryCache) sc.getAttribute("chartHistoryCache");
			Map<SkinGroup, List<MarketRate>> ratesMap = chc.getMarketHistory(marketId);
			List<MarketRate> l = null;
			if (ratesMap != null) {
				l = ratesMap.get(ApplicationData.getSkinById(ApplicationData.getSkinId(request.getSession(), request)).getSkinGroup());
			}
			log.debug("rates from cache: " + (null != l ? l.size() : 0));
			if (null != l && l.size() > 0) {
				// Calculate current hour start time by the opp est closing time - 1 hour
				// because day opps are considered hour opps in the end of the day and their
				// opening time is in the morning
				long chst = opp.getTimeEstClosing().getTime() - 60 * 60 * 1000;
				while (chst > System.currentTimeMillis()) {
					chst -= 60 * 60 * 1000;
				}
				pw.write(l.get(0).getRateTime().getTime() < chst ? "1" : "0");
				// 'hasPreviousHour' end
				pw.write("},'investments':[");
				// 'settings' end, 'investments' begin

				if (user != null && user.getId() > 0) {
					Iterator<Investment> iterator = investments.iterator();
					while (iterator.hasNext()) {
						Investment investment = iterator.next();

						// investment begin, 'id' begin
						pw.write("{'id':");
						pw.write(String.valueOf(investment.getId()));
						// 'id' end

						// 'market' begin
						pw.write(",'market':'");
						pw.write(MarketsManagerBase.getMarketName(skinId, investment.getMarketId()));
						pw.write("'");
						// 'market' end

						// 'time' begin
						pw.write(",'time':");
						pw.write(String.valueOf(investment.getTimeCreated().getTime()));
						// 'time' end

						// 'levelTxt' begin
						pw.write(",'levelTxt':'");
						pw.write(InvestmentFormatter.getCurrentLevelTxt(investment));
						pw.write("'");
						// 'levelTxt' end

						// 'level' begin
						pw.write(",'level':");
						pw.write(String.valueOf(investment.getCurrentLevel()));
						// 'level' end

						// 'amount' begin
						pw.write(",'amount':");
						pw.write(String.valueOf(investment.getAmount()));
						// 'amount' end

						// 'type' begin
						pw.write(",'type':");
						pw.write(String.valueOf(investment.getTypeId()));
						// 'type' end

						// 'oddsWin' begin
						pw.write(",'oddsWin':");
						pw.write(String.valueOf(investment.getOddsWin()));
						// 'oddsWin' end

						// 'oddsLose' begin
						pw.write(",'oddsLose':");
						pw.write(String.valueOf(investment.getOddsLose()));
						// 'oddsLose' end

						// 'amountTxt' begin
						pw.write(",'amountTxt':'");
						pw.write(CommonUtil.displayAmount(investment.getAmount(), investment.getCurrencyId()));
						pw.write("'");
						// 'amountTxt' end

						// 'rollUpAmount' begin
						pw.write(",'rollUpAmount':");
						pw.write(String.valueOf(investment.getInsuranceAmountRU()));
						// 'rollUpAmount' end

						// 'marketId' begin
						pw.write(",'marketId':");
						pw.write(String.valueOf(investment.getMarketId()));
						// 'marketId' end

						// 'oppId' begin
						pw.write(",'oppId':");
						pw.write(String.valueOf(investment.getOpportunityId()));
						// 'id' end

						// 'timeEstClosing' begin
						pw.write(",'timeEstClosing':");
						pw.write(String.valueOf(investment.getTimeEstClosing().getTime()));
						// 'timeEstClosing' end

						String invEnd;
						if (iterator.hasNext()) {
							invEnd = "},";
						} else {
							invEnd = "}";
						}
						pw.write(invEnd);
						// investment end
					}
				}

				pw.write("],'rates':[");
				// 'investments' end, 'rates' begin
				int sent = 0;
				if (opp.getSchedule() < Opportunity.SCHEDULED_DAYLY) {
					MarketRate mr = null;
					for (int i = 0; i < l.size(); i++) {
						mr = l.get(i);
						if ((history == 0 || (history == 1 && mr.getRateTime().getTime() >= chst) || (history == 2 && mr.getRateTime()
																														.getTime() < chst))
								&& ((opp.getSchedule() == 1 && mr.getGraphRate().doubleValue() != 0) || (opp.getSchedule() != 1 && mr.getDayRate()
																																	.doubleValue() != 0))) {
							// rate begin
							pw.write("[");
							// time begin
							pw.write(String.valueOf(mr.getRateTime().getTime()));
							pw.write(",");
							// time end
	
							// level begin
							if (opp.getSchedule() == 1) {
								pw.write(String.valueOf(mr.getGraphRate().doubleValue()));
							} else {
								pw.write(String.valueOf(mr.getDayRate().doubleValue()));
							}
							pw.write(",");
							pw.write(mr.getLast() != null ? String.valueOf(mr.getLast().doubleValue()) : "0");
							pw.write(",");
							pw.write(mr.getAsk() != null ? String.valueOf(mr.getAsk().doubleValue()) : "0");
							pw.write(",");
							pw.write(mr.getBid() != null ? String.valueOf(mr.getBid().doubleValue()) : "0");
							// level end
							if (i < l.size() - 1) {
								pw.write("],");
							} else {
								pw.write("]");
							}
							// rate end
							sent++;
						}
					}
				} else if (opp.getSchedule() == Opportunity.SCHEDULED_DAYLY	|| opp.getSchedule() == Opportunity.SCHEDULED_WEEKLY
							|| opp.getSchedule() == Opportunity.SCHEDULED_MONTHLY || opp.getSchedule() == Opportunity.SCHEDULED_QUARTERLY) {
					Set<com.anyoption.common.beans.Opportunity> oppsCache;
					PastExpiriesCache expiriesCache = (PastExpiriesCache) sc.getAttribute(Constants.EXPIRIES_CACHE);
					switch (opp.getSchedule()) {
					case Opportunity.SCHEDULED_DAYLY:
						oppsCache = expiriesCache.getEndOfDayExpiries(opp.getMarketId());
						break;

					case Opportunity.SCHEDULED_WEEKLY:
						oppsCache = expiriesCache.getEndOfWeekExpiries(opp.getMarketId());
						break;

					case Opportunity.SCHEDULED_MONTHLY:
						oppsCache = expiriesCache.getEndOfMonthExpiries(opp.getMarketId());
						break;

					case Opportunity.SCHEDULED_QUARTERLY:
						if (cachedOppTimeFirstInvest != null) {
							oppsCache = expiriesCache.getEndOfQuarterExpiries(opp.getMarketId(), cachedOppTimeFirstInvest);
						} else {
							oppsCache = new TreeSet<>();
						}
						break;

					default:
						oppsCache = new TreeSet<>();
						break;
					}
					Iterator<com.anyoption.common.beans.Opportunity> iterator = oppsCache.iterator();
					while (iterator.hasNext()) {
						com.anyoption.common.beans.Opportunity cachedOpp = iterator.next();
						// rate begin
						pw.write("[");
						// time begin
						pw.write(String.valueOf(cachedOpp.getTimeEstClosing().getTime()));
						pw.write(",");
						// time end
						// level begin
						pw.write(String.valueOf(cachedOpp.getClosingLevel()));
						pw.write(",");
						pw.write((cachedOpp.getLastUpdateQuote().getLast() != null)
																						? String.valueOf(cachedOpp	.getLastUpdateQuote()
																												.getLast().doubleValue())
																					: "0");
						pw.write(",");
						pw.write((cachedOpp.getLastUpdateQuote().getAsk() != null)	? String.valueOf(cachedOpp	.getLastUpdateQuote()
																												.getAsk()
																												.doubleValue())
																					: "0");
						pw.write(",");
						pw.write((cachedOpp.getLastUpdateQuote().getBid() != null)	? String.valueOf(cachedOpp	.getLastUpdateQuote()
																												.getBid()
																												.doubleValue())
																					: "0");
						// level end
						if (iterator.hasNext()) {
							pw.write("],");
						} else {
							pw.write("]");
						}
						// rate end
						sent++;
					}
				}
				pw.write("]");
				// 'rates' end
				log.debug("rates sent: " + sent);
			} else {
				pw.write("0},'investments':[],'rates':[]");
				// 'hasPreviousHour' end, 'settings' end, empty 'investments', empty 'rates'
			}
		} /* Dead code - else {
			pw.write("0},'investments':[],'rates':[]");
			// 'hasPreviousHour' end, 'settings' end, empty 'investments', empty 'rates'
		}*/

		pw.write("}");
		// chartData end
		pw.flush();
		pw.close();
	}
}