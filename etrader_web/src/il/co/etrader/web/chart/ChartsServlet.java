package il.co.etrader.web.chart;

import il.co.etrader.chart.Chart;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.ChartsUpdaterMarket;
import com.anyoption.common.beans.MarketRate;
import com.anyoption.common.beans.base.Language;
import com.anyoption.common.charts.ChartsUpdater;
import com.anyoption.common.charts.ChartsUpdaterListener;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.managers.LanguagesManagerBase;

/**
 * Servlet that handle chart requests.
 *
 * @author Tony
 */
public class ChartsServlet extends HttpServlet implements ChartsUpdaterListener {
    private static final long serialVersionUID = 1L;

    private static final Logger log = Logger.getLogger(ChartsServlet.class);
    
    private Hashtable<String, ChartBean> charts;
    private ChartsUpdater updater;
    private ArrayList<Language> chartsLanguages;
    private String weCameFrom;
    
    public void init() throws ServletException {
        if (log.isDebugEnabled()) {
            log.debug("Start ChartsServlet init.");
        }     
        ResourceBundle bundle = ResourceBundle.getBundle("server");
        weCameFrom = bundle.getString("domain.url");
        
        charts = new Hashtable<String, ChartBean>();
        chartsLanguages = LanguagesManagerBase.getAllLanguagess();
        
        ServletContext servletContext = getServletContext();
        updater = (ChartsUpdater) servletContext.getAttribute("chartsUpdater");
        updater.addListener(this);

        // wait for the rates to be loaded
//        do {
//            try {
//                Thread.sleep(500);
//            } catch (Throwable t) {
//                // do nothing
//            }
//        } while (!updater.isLoaded() && updater.isRunning());

        if (log.isDebugEnabled()) {
            log.debug("End ChartsServlet init.");
        }
    }
    
    public void marketRates(ChartsUpdaterMarket m, Map<SkinGroup, List<MarketRate>> mrs) {
    	Iterator<Map.Entry<SkinGroup, List<MarketRate>>> iter = mrs.entrySet().iterator();
        List<MarketRate> ratesList = null;
        if (iter.hasNext()) {
        	ratesList = iter.next().getValue();
        }
        ResourceBundle bundle = null;
        if (weCameFrom.equalsIgnoreCase("anyoption.com")) {
            for (int j = 0; j < chartsLanguages.size(); j++) {
                if (chartsLanguages.get(j).getCode().equalsIgnoreCase("iw")) {
                    continue;
                }
                bundle = ResourceBundle.getBundle("MessageResources", new Locale(chartsLanguages.get(j).getCode()));
                updateChart(bundle, String.valueOf(m.getId()) + "_" + chartsLanguages.get(j).getCode(), ratesList);

            }
        } else { //etrader
            bundle = ResourceBundle.getBundle("messages");
            updateChart(bundle, String.valueOf(m.getId()) + "_heb", ratesList);
        }
    }

    private void updateChart(ResourceBundle bundle, String chartsKey, List<MarketRate> mrs) {
        Chart chart = null;
        ChartBean cb = charts.get(chartsKey);
        boolean created = false;
        if (null == cb) {
            String topRight = bundle.getString("history.chart.topright");
            String ffLine1 = bundle.getString("history.chart.first5min.line1");
            String ffLine2 = bundle.getString("history.chart.first5min.line2");
            cb = new ChartBean();
            if (weCameFrom.equalsIgnoreCase("anyoption.com")) {
                chart = new Chart(topRight, 318, 160, ffLine1, ffLine2);
            } else {
                chart = new Chart(topRight, 180, 90, ffLine1, ffLine2);
            }
            cb.setChart(chart);
            charts.put(chartsKey, cb);
            created = true;
        } else {
            chart = cb.getChart();
        }

        // since the last 30 min chart was done when we had 1 rate per min
        // to avoid refactor it since now we have 1 rate per 6 sec keep
        // adding to the charts one update per min
        int lastMinute = -1;
        int crrMinute;
        Calendar c = Calendar.getInstance();
        MarketRate rate = chart.getLastRate();
        if (null != rate) {
            c.setTime(rate.getRateTime());
            lastMinute = c.get(Calendar.MINUTE);
        }
        
        boolean rateAdded = false;
        for (int i = 0; i < mrs.size(); i++) {
            rate = mrs.get(i);
            c.setTime(rate.getRateTime());
            crrMinute = c.get(Calendar.MINUTE);
            if (crrMinute != lastMinute) {
                chart.addRate(rate);
                lastMinute = crrMinute;
                rateAdded = true;
            }
        }

        if (created || rateAdded) {
            log.trace("Update chart");
            cb.setChartImgBytes(chart.getLast30ImageBytes());
            cb.setLastModifiedTime(new Date());
        }
    }
    
    public void marketClosed(ChartsUpdaterMarket m) {
        if (weCameFrom.equalsIgnoreCase("anyoption.com")) {
            for (int j = 0; j < chartsLanguages.size(); j++) {
                if (chartsLanguages.get(j).getCode().equalsIgnoreCase("iw")) {
                    continue;
                }
                charts.remove(String.valueOf(m.getId()) + "_" + chartsLanguages.get(j).getCode());
            }
        } else { //etrader
            charts.remove(String.valueOf(m.getId()) + "_heb");
        }
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        // extract the requested name
        String uri = request.getRequestURI();
        if (log.isDebugEnabled()) {
            log.debug("URI requested: " + uri);
        }
        int lastSlash = uri.lastIndexOf("/");
        if (lastSlash != -1 && lastSlash < uri.length()) {
            String name = uri.substring(lastSlash + 1);
            ChartBean cb = charts.get(name);

            if (null == cb) {
                if (log.isDebugEnabled()) {
                    log.debug("404 for " + name);
                }
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }

//            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
            response.setHeader("Cache-control", "max-age=60, must-revalidate");
//            response.setHeader("Last-Modified", dateFormat.format(cb.getLastModifiedTime()));
//            response.setHeader("Date", dateFormat.format(new Date(System.currentTimeMillis())));
//            response.setHeader("Expires", dateFormat.format(new Date(System.currentTimeMillis() + 60000)));

            response.setContentType("image/png");
            response.setContentLength(cb.getChartImgBytes().length);

            OutputStream os = response.getOutputStream();
            os.write(cb.getChartImgBytes());
            os.flush();
            os.close();
        }
    }

    public void destroy() {
        updater.removeListener(this);
    }
}