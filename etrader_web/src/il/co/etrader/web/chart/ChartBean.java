package il.co.etrader.web.chart;

import java.util.Date;

import il.co.etrader.chart.Chart;

/**
 * Chart helper bean.
 * 
 * @author Tony
 */
public class ChartBean {
    private Chart chart;
    private byte[] chartImgBytes;
    private Date lastModifiedTime;
    private Date lastRateTime;
    
    public Chart getChart() {
        return chart;
    }

    public void setChart(Chart chart) {
        this.chart = chart;
    }

    public byte[] getChartImgBytes() {
        return chartImgBytes;
    }
    
    public void setChartImgBytes(byte[] chartImgBytes) {
        this.chartImgBytes = chartImgBytes;
    }
    
    public Date getLastModifiedTime() {
        return lastModifiedTime;
    }
    
    public void setLastModifiedTime(Date lastModifiedTime) {
        this.lastModifiedTime = lastModifiedTime;
    }

    public Date getLastRateTime() {
        return lastRateTime;
    }

    public void setLastRateTime(Date lastRateTime) {
        this.lastRateTime = lastRateTime;
    }
}