/**
 * 
 */
package il.co.etrader.web.chart;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_vos.User;

/**
 * Servlet writing user's balance to the response
 * 
 * @author kirilim
 */
public class ProfitLineJsonServlet extends HttpServlet {

	private static final long serialVersionUID = -6007374078446146620L;
	private static final Logger log = Logger.getLogger(ProfitLineJsonServlet.class);

	public void init() throws ServletException {
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		log.debug("Servlet begin");
		// Try to make sure that the profit line was not requested directly by someone else
		if (request.getHeader("referer") == null) {
			log.error("There is no Request Header Referer");
			PrintWriter pw = response.getWriter();
			pw.write("Access denied");
			pw.flush();
			pw.close();
			return;
		} else {
			String[] requestURLSplitted = request.getRequestURL().toString().split("/"); // 0 - protocol(i.e. http/https), 1 - empty,
																							// because of the //, 2 - domain
			String[] refererSplitted = request.getHeader("referer").split("/"); // same as above
			if (refererSplitted.length > 2 || requestURLSplitted.length > 2) {
				String requestURL = requestURLSplitted[2];
				String referer = refererSplitted[2];
				if (!requestURL.equals(referer)) {
					log.error("Request URL and Request Header Referer not equal. Request URL: " + requestURL + "; Referer: " + referer
								+ ". Returning nothing");
					return;
				}
			} else {
				log.error("Request URL and/or Request Header Referer too short. Request URL: " + requestURLSplitted.length + "; Referer: "
							+ refererSplitted.length + ". Returning nothing");
				return;
			}
		}

		response.setContentType("application/json;charset=UTF-8");
        response.addHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-store");

		User user = (User) request.getSession().getAttribute("user");
		PrintWriter pw = response.getWriter();
		// 'balance' begin
		pw.write("{'balance':'");
		long currencyId;
		String amount;
		if (user != null && user.getCurrencyId() != null) {
			currencyId = user.getCurrencyId();
			amount = CommonUtil.displayAmount(user.getBalance(), currencyId);
		} else {
			currencyId = ApplicationData.getSkinById(ApplicationData.getSkinId(request.getSession(), request)).getDefaultCurrencyId();
			amount = CommonUtil.displayAmount(0l, currencyId);
		}
		log.debug("Writing user balance of " + amount);
		pw.write(amount);
		pw.write("'}");
		// 'balance' end

		pw.flush();
        pw.close();
        log.debug("Servlet end");
	}

	public void destroy() {
    }
}