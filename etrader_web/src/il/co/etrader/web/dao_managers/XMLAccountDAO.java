package il.co.etrader.web.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.daos.DAOBase;

public class XMLAccountDAO extends DAOBase {
    public static boolean isLogin(Connection conn, String id, String password) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "id " +
                "FROM " +
                    "xml_api_accounts " +
                "WHERE " +
                    "id = ? AND " +
                    "password = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, id);
            pstmt.setString(2, password);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                return true;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return false;
    }
}