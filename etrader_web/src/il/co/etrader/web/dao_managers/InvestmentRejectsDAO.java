//package il.co.etrader.web.dao_managers;
//
//
//import il.co.etrader.dao_managers.InvestmentsDAOBase;
//import il.co.etrader.web.bl_managers.ApplicationData;
//import il.co.etrader.web.util.Constants;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//
//import javax.faces.context.FacesContext;
//
//import oracle.jdbc.OraclePreparedStatement;
//import oracle.jdbc.OracleResultSet;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.InvestmentRejects;
//import com.anyoption.common.beans.ServerConfiguration;
//
//public class InvestmentRejectsDAO extends InvestmentsDAOBase {
//    protected static Logger log = Logger.getLogger(InvestmentRejectsDAO.class);
//
//    /**
//     * Insert investment.
//     *
//     * @param conn the db connection to use
//     * @param inv Investment Rejects data to insert to db
//     * @throws SQLException
//     */
//    public static void insertInvestmentRejects(Connection conn, InvestmentRejects inv) throws SQLException  {
//        OraclePreparedStatement pstmt = null;
//        OracleResultSet rs = null;
//        FacesContext context=FacesContext.getCurrentInstance();
//        ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
//
//        try {
//	        String sql =
//	                "INSERT INTO INVESTMENT_REJECTS " +
//	                    "(id, user_id, opportunity_id, REJECT_TYPE_ID, SESSION_ID , REAL_LEVEL , PAGE_LEVEL , WWW_LEVEL , AMOUNT , RETURN , " +
//	                    "REJECT_ADDITIONAL_INFO , TIME_CREATED , WRITER_ID, RATE, TYPE_ID, OPPORTUNITY_TYPE, IS_FROM_GRAPH, SERVER_ID, IS_REJECTED_DURING_CLOSE, REFUND) " +
//	                "VALUES " +
//	                    "(SEQ_INVESTMENT_REJECTS.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, current_date, 1, ?, ?, ?, ?, ?, ?, ? )";
//
//	        pstmt = (OraclePreparedStatement)conn.prepareStatement(sql);
//	        pstmt.setLong(1, inv.getUserId());
//	        pstmt.setLong(2, inv.getOpportunityId());
//	        pstmt.setLong(3, inv.getRejectTypeId());
//	        pstmt.setString(4, inv.getSessionId());
//	        pstmt.setDouble(5, inv.getRealLevel() != null ? inv.getRealLevel() : 0);
//	        pstmt.setDouble(6, inv.getPageLevel() != null ? inv.getPageLevel() : 0);
//	        pstmt.setDouble(7, inv.getWwwLevel() != null ? inv.getWwwLevel() : 0);
//	        pstmt.setLong(8, inv.getAmount() != null ? inv.getAmount() : 0);
//	        pstmt.setFloat(9, inv.getReturnInv() != null ? inv.getReturnInv() : 0);
//	        pstmt.setString(10, inv.getRejectAdditionalInfo());
//	        pstmt.setDouble(11, inv.getRate());
//	        pstmt.setDouble(12, inv.getTypeId());
//	        pstmt.setInt(13, inv.getOpportunityType());
//	        pstmt.setInt(14, inv.getFromGraph());
//	        pstmt.setString(15, ServerConfiguration.getInstance().getServerName());
//	        pstmt.setBoolean(16, inv.isRejectedDuringClose());
//	        pstmt.setFloat(17, inv.getRefundInv() != null ? inv.getRefundInv() : 0);
//	        pstmt.executeUpdate();
//	        log.debug("insert Investment Rejects into db sucessfully");
//        } finally {
//            closeResultSet(rs);
//            closeStatement(pstmt);
//        }
//    }
//
//}