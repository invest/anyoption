package il.co.etrader.web.dao_managers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.anyoption.common.beans.MailBoxTemplate;
import com.anyoption.common.beans.MailBoxUser;

import il.co.etrader.bl_managers.MailBoxTemplateManagerBase;
import il.co.etrader.dao_managers.MailBoxUsersDAOBase;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_vos.MailBoxUserAttachment;
import oracle.jdbc.OracleResultSet;
import oracle.sql.BLOB;

public class MailBoxUsersDAO extends MailBoxUsersDAOBase{
	/**
     * Get User mailBox emails
     * @param conn
     * @param userId
     * @return
     * @throws SQLException
     */
    public static MailBoxUser getUserPopUpEmail(Connection conn, long userId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        MailBoxUser email = null;
        try{
            String sql =
            		"SELECT * " +
            		"FROM " +
	            		"(SELECT " +
	            			"X.*, " +
	            			"rownum as row_num " +
	            		"FROM " +
		                    "(SELECT " +
		                        "mbu.id, " +
		                        "mbu.time_created, " +
		                        "mbu.is_high_priority, " +
		                        "mbu.subject, " +
		                        "mbt.type_id, " +
		                        "mbu.popup_type_id, " +
		                        "mbu.transaction_id, " +
		                        "mbt.template, " +
		                        "mbs.sender, " +
		                        "mbu.free_text, " +
		                        "mbu.bonus_user_id, " +
		                        "mbu.template_parameters " +
		                    "FROM " +
		                        "mailbox_users mbu, " +
		                        "mailbox_templates mbt, " +
		                        "mailbox_senders mbs " +
		                    "WHERE " +
		                        "mbu.user_id = ? AND " +
		                        "mbu.template_id = mbt.id AND " +
		                        "mbu.sender_id = mbs.id AND " +
		                        "mbu.status_id = ? AND " +
		                        "mbu.popup_type_id in (2,3) " +  // popup types
		                    "ORDER BY " +
		                    	"mbu.time_created desc ) X " +
		                  ") " +
		               "WHERE " +
		               		"row_num = 1";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setLong(2, ConstantsBase.MAILBOX_STATUS_NEW);

            rs = pstmt.executeQuery();
            while (rs.next()) {
            	email = new MailBoxUser();
            	email.setId(rs.getLong("id"));
            	email.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
            	email.setSubject(rs.getString("subject"));
            	email.setSenderName(rs.getString("sender"));
            	email.setIsHighPriority(rs.getInt("is_high_priority") == 1 ? true : false);
            	email.setFreeText(rs.getString("free_text"));
            	email.setBonusUserId(rs.getLong("bonus_user_id"));
            	email.setPopupTypeId(rs.getLong("popup_type_id"));
            	email.setTransactionId(rs.getLong("transaction_id"));
            	MailBoxTemplate template = new MailBoxTemplate();
            	template.setTypeId(rs.getLong("type_id"));
            	template.setTemplate(rs.getString("template"));
            	String params = rs.getString("template_parameters");
            	if (params != null) {
            		template.setTemplate(MailBoxTemplateManagerBase.insertParameters(template.getTemplate(), params));
            	}
                email.setTemplate(template);
            }
        } finally {
            closeStatement(pstmt);
            closeResultSet(rs);
        }
        return email;
    }
    
    /**
	 * get the BLOB (PDF attachment) and save it to ByteArrayOutputStream
	 * @param conn
	 * @param attachmentId mailBox User Attachment id
	 * @throws SQLException
	 * @throws IOException
	 * Return the file name
	 */
	public static MailBoxUserAttachment getMailBoxAttachment(Connection conn, long attachmentId) throws SQLException, IOException {
        Statement stmt = null;
        ResultSet rset = null;
        BLOB attachment = null;
        MailBoxUserAttachment mailBoxAttachment = null;
        ByteArrayOutputStream outputStream;

        try {
            stmt = conn.createStatement();

            String sql =
                "SELECT " +
                	"attachment, " +
                	"attachment_name " +
                "FROM " +
                	"mailbox_users_attachments " +
                "WHERE " +
                	"id = " + attachmentId + " " +
                "FOR UPDATE";   // lock the record in the cursor result set. released after commit / rollBack

            rset = stmt.executeQuery(sql);
            if (rset.next()) {
            	mailBoxAttachment = new MailBoxUserAttachment();
	            attachment = ((OracleResultSet) rset).getBLOB("attachment");
	            mailBoxAttachment.setAttachmentName(rset.getString("attachment_name"));

	            long blobLength = attachment.length();
	            int chunkSize = attachment.getChunkSize();
	            byte[] binaryBuffer = new byte[chunkSize];
	            outputStream = new ByteArrayOutputStream((int)blobLength);

	            int bytesRead = 0;
	            for (long position = 1; position <= blobLength; position += chunkSize) {
	                bytesRead = attachment.getBytes(position, chunkSize, binaryBuffer);
	                outputStream.write(binaryBuffer, 0, bytesRead);  // write to outputStream
	            }
	            mailBoxAttachment.setAttachment(outputStream);
	            conn.commit();
            }
        } finally {
        	closeResultSet(rset);
            closeStatement(stmt);
        }
        return mailBoxAttachment;
	}
}
