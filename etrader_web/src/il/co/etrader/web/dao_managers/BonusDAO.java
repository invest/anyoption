//package il.co.etrader.web.dao_managers;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//public class BonusDAO extends com.anyoption.common.daos.BonusDAOBase {
//
//    public static int useNextInvestOnUs(Connection conn, long bonusUsersId, long amount) throws SQLException {
//        int updatedCount = 0;
//        PreparedStatement pstmt = null;
//        try {
//            String sql =
//                    "UPDATE " +
//                        "bonus_users " +
//                    "SET " +
//                        "bonus_state_id = 3, " +
//                        "time_used = current_date, " +
//                        "sum_invest_withdraw = wagering_parameter * ? " +
//                    "WHERE " +
//                        "id = ?";
//            pstmt = conn.prepareStatement(sql);
//            pstmt.setLong(1, amount);
//            pstmt.setLong(2, bonusUsersId);
//            updatedCount = pstmt.executeUpdate();
//        } finally {
//            closeStatement(pstmt);
//        }
//        return updatedCount;
//    }
//
//    /**
//     * Check if bonus on registration is available
//     * @param con db connection
//     * @param bonusId the bonus id on registration
//     * @return
//     * @throws SQLException
//     */
//    public static boolean isBonusUponRegistrationAvailable(Connection con, long bonusId) throws SQLException {
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        try {
//            String sql =
//            	"SELECT * " +
//            	"FROM " +
//            		"bonus " +
//            	"WHERE " +
//            		"id = ? AND " +
//            		"sysdate >= start_date AND " +
//            		"sysdate <= end_date ";
//            ps = con.prepareStatement(sql);
//            ps.setLong(1, bonusId);
//            rs = ps.executeQuery();
//            if (rs.next()) {
//            	return true;
//            }
//        } finally {
//        	closeResultSet(rs);
//        	closeStatement(ps);
//        }
//        return false;
//    }
//}