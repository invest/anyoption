package il.co.etrader.web.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.web.helper.TradingPageBoxBean;

/**
 * Ticker line DAO.
 *
 * @author Tony
 */
public class TickerDAO extends DAOBase {
    private static final Logger log = Logger.getLogger(TickerDAO.class);

    /**
     * Load specified number of markets details for specified skin sorted by
     * opened/suspended/closed/home page priority.
     *
     * @param conn
     * @param skinId
     * @return <code>ArrayList<TradingPageBoxBean></code>
     * @throws SQLException
     */
    public static ArrayList<TradingPageBoxBean> getSkinTickerMarkets(Connection conn, long skinId) throws SQLException {
        ArrayList<TradingPageBoxBean> l = new ArrayList<TradingPageBoxBean>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
            		"SELECT T.market_id, " +
            				  "  T.has_opened, " +
            				  "  T.is_suspended, " +
            				  "  T.ticker_priority, " +
            				  "  O1.closing_level last_day_closing_level, " +
            				  "  TO_CHAR(O1.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS last_day_closing_time, " +
            				  "  O2.closing_level last_closing_level, " +
            				  "  TO_CHAR(O2.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS last_closing_time, " +
            				  "  T.scheduled " +
            		"FROM " +
            			"(SELECT * " +
            			" FROM " +
            			"    (SELECT DISTINCT B.market_id, " +
            				  "      1 AS has_opened, " +
            				  "      D.is_suspended, " +
            				  "      B.ticker_priority, " +
            				  "      MIN(C.scheduled) AS scheduled " +
            			"    FROM skin_market_groups A, " +
            				  "      skin_market_group_markets B, " +
            				  "      opportunities C, " +
            				  "      markets D " +
            			"    WHERE A.skin_id = ? " +
            				  "    AND B.skin_market_group_id = A.id " +
            				  "    AND C.market_id = B.market_id " +
            				  "    AND D.id = C.market_id " +
            				  "    AND C.is_published = 1 " +
            				  "    AND C.time_est_closing > CURRENT_TIMESTAMP " +
            				  "    AND C.OPPORTUNITY_TYPE_ID = 1 " +
            				  "    AND B.TICKER_PRIORITY != 999 " +
            				  "    GROUP BY B.market_id, " +
            				  "      1, " +
            				  "      D.is_suspended, " +
            				  "      B.ticker_priority " +
            			"    UNION " +
            			"    SELECT DISTINCT B.market_id, " +
            				  "      0 AS has_opened, " +
            				  "      D.is_suspended, " +
            				  "      B.ticker_priority, " +
            				  "      NULL AS scheduled " +
            			"    FROM skin_market_groups A, " +
            				  "      skin_market_group_markets B, " +
            				  "      markets D, " +
            				  "      opportunities O " +
            			"    WHERE A.skin_id = ? " +
            				  "    AND B.skin_market_group_id = A.id " +
            				  "    AND O.market_id = B.market_id " +
            				  "    AND O.OPPORTUNITY_TYPE_ID  = 1 " +
            				  "    AND D.id = B.market_id " +
            				  "    AND B.TICKER_PRIORITY != 999 " +
            				  "    AND NOT EXISTS " +
            				  "      (SELECT id " +
            				  "      FROM opportunities " +
            				  "      WHERE market_id = D.id " +
            				  "      AND OPPORTUNITY_TYPE_ID = 1 " +
            				  "      AND time_est_closing > CURRENT_TIMESTAMP " +
            				  "      AND is_published = 1 " +
            				  "      ) " +
            				  "    GROUP BY B.market_id, " +
            				  "      D.is_suspended, " +
            				  "      B.ticker_priority " +
            				  "    ORDER BY has_opened DESC, " +
            				  "      is_suspended, " +
            				  "      ticker_priority " +
            				  "    ) " +
            				  "  WHERE rownum <= " +
            				  "    (SELECT ticker_assets_count FROM skins WHERE id = ?)) T " +
            				  "LEFT JOIN " +
            				  "  (SELECT o.closing_level, " +
            				  "    o.scheduled, " +
            				  "    o.market_id, " +
            				  "    o.time_est_closing " +
            				  "  FROM opportunities o " +
            				  "  WHERE NOT o.closing_level IS NULL " +
            				  "  AND o.opportunity_type_id  = 1 " +
            				  "  AND o.scheduled > 1 " +
            				  "  AND o.time_est_closing = " +
            				  "    (SELECT MAX(o2.time_est_closing) " +
            				  "    FROM opportunities o2 " +
            				  "    WHERE NOT o2.closing_level IS NULL " +
            				  "    AND o2.opportunity_type_id  = 1 " +
            				  "    AND o2.market_id = o.market_id " +
            				  "    AND o2.scheduled > 1 ) ) O1 " +
            				  "ON T.market_id = O1.market_id " +
            				  "LEFT JOIN " +
            				  "  (SELECT o.closing_level, " +
            				  "    o.scheduled, " +
            				  "    o.market_id, " +
            				  "    o.time_est_closing " +
            				  "  FROM opportunities o " +
            				  "  WHERE NOT o.closing_level IS NULL " +
            				  "  AND o.opportunity_type_id  = 1 " +
            				  "  AND o.time_est_closing = " +
            				  "    (SELECT MAX(o2.time_est_closing) " +
            				  "    FROM opportunities o2 " +
            				  "    WHERE NOT o2.closing_level IS NULL " +
            				  "    AND o2.opportunity_type_id  = 1 " +
            				  "    AND o2.market_id = o.market_id)) O2 " +
            				  "ON T.market_id   = O2.market_id " +
            				  "AND O2.scheduled = 1 " +
            				  "ORDER BY T.ticker_priority";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, skinId);
            pstmt.setLong(2, skinId);
            pstmt.setLong(3, skinId);
            rs = pstmt.executeQuery();
            TradingPageBoxBean b = null;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
            while (rs.next()) {
                try {
                    b = new TradingPageBoxBean();
                    b.setMarketId(rs.getLong("market_id"));
                    b.setScheduled(rs.getInt("scheduled"));
                    b.setHasOpened(rs.getBoolean("has_opened"));
                    b.setLastClosingLevel(rs.getBigDecimal("last_day_closing_level"));
                    String LastDayLastClosingTime = rs.getString("last_day_closing_time");
                    if (null != LastDayLastClosingTime) {
                        b.setLastClosingLevelTime(sdf.parse(LastDayLastClosingTime));
                    }
                    b.setLastLevel(rs.getBigDecimal("last_closing_level"));
                    String LastClosingTime = rs.getString("last_closing_time");
                    if (null != LastClosingTime) {
                        b.setLastLevelTime(sdf.parse(LastClosingTime));
                    }
                    b.setFixed(false);
                    // in case its new market and it was never closed....
                    if ((null == LastClosingTime) && (null == LastDayLastClosingTime)) {
                    	continue;
                    }
                    l.add(b);
                } catch (ParseException pe) {
                    log.error("Can't parse times.", pe);
                }
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
    }
}