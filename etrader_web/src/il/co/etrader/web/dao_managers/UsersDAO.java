package il.co.etrader.web.dao_managers;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.InvestmentRejects;
import com.anyoption.common.beans.MailBoxTemplate;
import com.anyoption.common.beans.MailBoxUser;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.util.AESUtil;

import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_managers.MailBoxTemplateManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.InvestmentRejectsManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;

public class UsersDAO extends UsersDAOBase{
/*    public static HashMap getPasswordDetails(Connection con, String name, long skinId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            String sql="select * from users where user_name = ? and skin_id = ?";

            ps = con.prepareStatement(sql);
            ps.setString(1, name);
            ps.setLong(2, skinId);
            rs=ps.executeQuery();
            if (rs.next()) {
                HashMap<String, Object> h = new HashMap<String, Object>();
                h.put("id", rs.getString("id"));
                try {
                	h.put("password", Encryptor.decryptStringToString(rs.getString("password")));
                } catch (CryptoException ce) {
        			throw new SQLException("CryptoException: " + ce.getMessage());
        		}
                h.put("email", rs.getString("email"));
                h.put("birthDate", rs.getDate("time_birth_date"));
                if (skinId == ConstantsBase.SKIN_ETRADER) {
                	try {
                		h.put("idNum", Encryptor.decryptStringToString(rs.getString("id_num")));
                	} catch (CryptoException e) {
						throw new SQLException(e.getMessage());
					}
                }
                return h;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return null;
    }
*/

	public static ArrayList<User> getPasswordDetails(Connection con, String email, String idNum, long skinId) throws Exception {
        ArrayList<User> arr = new ArrayList<User>();
		PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            String sql="select u.*, c.phone_code from users u, countries c " +
            	 	   " where c.id = u.country_id ";
            	sql += " and  upper(email) = upper(?)";
            	
            ps = con.prepareStatement(sql);
            ps.setString(1, email.toLowerCase());
            rs=ps.executeQuery();
            while (rs.next()) {
            	User usr = new User();
            	usr.setId(rs.getLong("id"));
            	usr.setPassword(AESUtil.decrypt(rs.getString("password")));
            	usr.setEmail(rs.getString("email"));
            	usr.setTimeBirthDate(rs.getDate("time_birth_date"));													
            	usr.setSkinId(rs.getLong("skin_id"));
            	usr.setUserName(rs.getString("user_name"));
            	usr.setMobileNumberValidated(rs.getInt("mobile_phone_validated"));
            	if(skinId == Skin.SKIN_ETRADER){
            		usr.setMobilePhone(rs.getString("mobile_phone").substring(3));
            		usr.setMobilePhonePrefix(rs.getString("mobile_phone").substring(0,3));
            	}else {
            		usr.setPhoneCountryPrefix(rs.getString("phone_code"));
            		usr.setMobilePhone(rs.getString("mobile_phone"));
            	}
            	usr.setCountryId(rs.getLong("country_id"));
            	arr.add(usr);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return arr;
    }

    public static int getFailedCount(Connection con, String username) throws SQLException{
        int failedCount = 0;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql = "select failed_count from users where user_name = upper(?)";
            ps = con.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();
            if (rs.next()) {
                failedCount = rs.getInt("failed_count");
                return failedCount;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return -1; // user does not exist
    }

    public static int getIsActive(Connection con, String username) throws SQLException {
        int isActive;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql = "select is_active from users where user_name = upper(?)";
            ps = con.prepareStatement(sql);
            ps.setString(1, username);
            rs=ps.executeQuery();
            if (rs.next()) {
                isActive = rs.getInt("is_active");
                return isActive;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return 0; // will show inactive error
    }

    /**
     * Set value to users.is_next_invest_on_us flag.
     *
     * @param conn
     * @param userId
     * @param isNextInvestOnUs
     * @throws SQLException
     */
    public static void setIsNextInvestOnUs(Connection conn, long userId, int isNextInvestOnUs) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                "UPDATE " +
                    "users " +
                "SET " +
                    "is_next_invest_on_us = ? " +
                "WHERE " +
                    "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, isNextInvestOnUs);
            pstmt.setLong(2, userId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

	public static int getIsNextInvestOnUs(Connection con, long userId) throws SQLException {
		int nextInvestOnUs = 0;
  		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
		    String sql="select is_next_invest_on_us from users where id=?";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				nextInvestOnUs = rs.getInt("is_next_invest_on_us");
				return nextInvestOnUs;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return 0; // dont know, so return no...
	}

	/**
	 * update utcOofset of the user
	 * this is done for the reason we need to know the up-to-date utcoffcet
	 * @param conn
	 * 		Db connection
	 * @param userId
	 * 		user id
	 * @throws SQLException
	 */
    public static void updateUtcOffset(Connection conn, long userId) throws SQLException {
        PreparedStatement pstmt = null;
        try {
    		String sql =
                "UPDATE " +
            		 "users " +
                "SET " +
                     "utc_offset = ? " +
                "WHERE " +
                     "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

//    public static Investment getLastInvest(Connection conn, long userId) throws SQLException {
//        Investment i = null;
//    	PreparedStatement pstmt = null;
//    	ResultSet rs = null;
//    	try{
//    		String sql =
//                "SELECT " +
//                    "id, " +
//                    "opportunity_id, " +
//                    "type_id, " +
//                    "time_created " +
//                "FROM " +
//                    "investments " +
//                "WHERE " +
//                    "user_id = ? " +
//                "ORDER BY " +
//                    "time_created desc";
//    		pstmt = conn.prepareStatement(sql);
//    		pstmt.setLong(1, userId);
//    		rs = pstmt.executeQuery();
//    		if (rs.next()) {
//                i = new Investment();
//                i.setId(rs.getLong("id"));
//                i.setOpportunityId(rs.getLong("opportunity_id"));
//                i.setTypeId(rs.getLong("type_id"));
//                i.setTimeCreated(new Date(rs.getTimestamp("time_created").getTime()));
//    		}
//    	} finally {
//    		closeStatement(pstmt);
//    		closeResultSet(rs);
//    	}
//    	return i;
//    }

    public static boolean hasTheSameInvestmentInTheLastSec(Connection conn, long userId, long oppId, long typeId, InvestmentRejects invRej) throws SQLException {
        boolean has = false;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try{
            String sql =
                    "SELECT " +
                        "id, ((current_date - time_created)*86400) secBetween " +
                    "FROM " +
                        "investments " +
                    "WHERE " +
                        "user_id = ? AND " +
                        "opportunity_id = ? AND " +
                        "type_id = ? AND " +
                        "current_date - time_created < (SELECT sec_between_investments FROM markets WHERE id = (SELECT market_id FROM opportunities WHERE id = ?)) / 86400";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setLong(2, oppId);
            pstmt.setLong(3, typeId);
            pstmt.setLong(4, oppId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                has = true;
                invRej.setRejectTypeId(Constants.INVESTMENT_REJECT_LAST_INV_IN_LESS_THEN_X_SEC);
                invRej.setRejectAdditionalInfo("Same investment in less:, secBetweenInvestments: " + rs.getLong("secBetween") + " , market secBetweenInvestments: " + invRej.getSecBetweenInvestments());
                InvestmentRejectsManager.insertInvestmentReject(invRej);
            }
        } finally {
            closeStatement(pstmt);
            closeResultSet(rs);
        }
        return has;
    }

    /**
     * Check if there is disabled record for the specified user / market covering the current moment.
     *
     * @param conn
     * @param userId
     * @param marketId
     * @return
     * @throws SQLException
     */
    public static boolean isUserMarketDisabled(Connection conn, long userId, long marketId, int scheduled, boolean isDev3) throws SQLException {
        boolean b = false;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try{
            String sql =
                    "SELECT " +
                        "id " +
                    "FROM " +
                        "user_market_disable " +
                    "WHERE " +
                        "user_id = ? AND ";
            if (isDev3) {
                 sql += "(market_id = ? OR market_id = 0) AND ";
            } else {
                 sql += "market_id = ? AND ";
            }
                 sql += "is_active = 1 AND " +
                        "current_date >= start_date AND " +
                        "current_date <= end_date AND " +
                        "(scheduled = ? OR scheduled = ? ) AND " +   // in case we have the same scheduled like the parameter, or
            			"is_dev3 = ?";									        // scheduled = 0 (all) - for all scheduled types
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setLong(2, marketId);
            pstmt.setInt(3, scheduled);
            pstmt.setInt(4, InvestmentsManagerBase.SCHEDULED_ALL);
            pstmt.setInt(5, isDev3 ? 1 : 0);

            rs = pstmt.executeQuery();
            if (rs.next()) {
                b = true;
            }
        } finally {
            closeStatement(pstmt);
            closeResultSet(rs);
        }
        return b;
    }

    /**
     * Update tierUserId after qualification
     * @param conn
     * @param userId user that qualified
     * @param tierUserId the tierId that the user qualified
     * @throws SQLException
     */
    public static void setTierUSerId(Connection conn, long userId, long tierUserId) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                "UPDATE " +
                    "users " +
                "SET " +
                    "tier_user_id = ? " +
                "WHERE " +
                    "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, tierUserId);
            pstmt.setLong(2, userId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    /**
     * Update is contact by sms flag
     * @param conn
     * @param userId
     * @param freeSms true if the user accepted to get sms messages from us
     * @throws SQLException
     */
    public static void updateFreeSms(Connection conn, long userId, long freeSms) throws SQLException {
    	PreparedStatement pstmt = null;
    	try {
            String sql =
                "UPDATE " +
                    "users " +
                "SET " +
                    "is_contact_by_sms = ? " +
                "WHERE " +
                    "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, freeSms);
            pstmt.setLong(2, userId);
            pstmt.executeUpdate();
    	} finally {
            closeStatement(pstmt);
        }
    }

    /**
     * Get userId by phone or email
     * @param con
     * @param phone
     * @param email
     * @return
     * @throws SQLException
     */
    public static long getUserByPhoneEmail(Connection con, String phone, String email, long countryId) throws SQLException {
    	 PreparedStatement pstmt = null;
         ResultSet rs = null;
         long userId = 0;
         try{
             String sql =
                     "SELECT " +
                         "id " +
                     "FROM " +
                         "users " +
                     "WHERE " +
                     	 "country_id = ? AND " +
                         "(mobile_phone = ? OR " +
                         "land_line_phone = ? ";

             if (null != email) {
            	 sql += "OR upper(email) = upper(?) ";
             }

             sql += ") ";


             pstmt = con.prepareStatement(sql);
             pstmt.setLong(1, countryId);
             pstmt.setString(2, phone);
             pstmt.setString(3, phone);
             if (null != email) {
            	 pstmt.setString(4, email);
             }
             rs = pstmt.executeQuery();
             if (rs.next()) {
            	 userId =  rs.getLong("id");
             }
         } finally {
             closeStatement(pstmt);
             closeResultSet(rs);
         }
         return userId;
    }

   /**
     * Save the first values of combinationId and dp
     * @param con
     * @param userId
     * @param combId
     * @param dp
     * @throws SQLException
     */
	public static void insertUserFirstVisit(Connection con, long userId, long combId, String dp) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql =
					"INSERT " +
						"INTO users_first_visit(" +
							"id, " +
							"user_id, " +
							"combination_id, " +
							"dynamic_param, " +
							"time_created) " +
						"VALUES " +
							"(SEQ_USERS_FIRST_VISIT.NEXTVAL,?,?,?,sysdate)";

			ps = (PreparedStatement) con.prepareStatement(sql);

			ps.setLong(1, userId);
			ps.setLong(2, combId);
			ps.setString(3, dp);
			ps.executeUpdate();
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}

    /**
     * Get User mailBox emails
     * @param conn
     * @param userId
     * @return
     * @throws SQLException
     */
    public static ArrayList<MailBoxUser> getUserMailBox(Connection conn, long userId, String searchFilter) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<MailBoxUser> emails = new ArrayList<MailBoxUser>();
        try{
            String sql =
                    "SELECT " +
                        "mbu.id, " +
                        "mbu.status_id, " +
                        "mbu.time_created, " +
                        "mbu.time_read, " +
                        "mbu.utc_offset_read, " +
                        "mbu.is_high_priority, " +
                        "mbu.subject, " +
                        "mbu.transaction_id, " +
                        "mbu.attachment_id, " +
                        "mbt.type_id, " +
                        "mbt.template, " +
                        "mbs.sender, " +
                        "mbu.free_text, " +
                        "mbu.writer_id, " +
                        "mbu.bonus_user_id, " +
                        "mbu.template_parameters, " +
                        "mut.attachment_name " +
                    "FROM " +
                        "mailbox_users mbu " +
                        	"LEFT JOIN mailbox_users_attachments mut on mbu.attachment_id = mut.id, " +
                        "mailbox_templates mbt, " +
                        "mailbox_senders mbs " +
                    "WHERE " +
                        "mbu.user_id = ? AND " +
                        "mbu.template_id = mbt.id AND " +
                        "mbu.sender_id = mbs.id AND " +
                        "mbu.status_id != ? " +
	                "ORDER BY " +
	                	"mbu.time_created desc ";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setLong(2, Constants.MAILBOX_STATUS_DETETED);

            rs = pstmt.executeQuery();
            while (rs.next()) {
            	if (!CommonUtil.isParameterEmptyOrNull(searchFilter)) {
            		searchFilter = searchFilter.toUpperCase();
            		if (rs.getString("subject").toUpperCase().indexOf(searchFilter) == -1 &&
            				CommonUtil.getMessage(rs.getString("sender"),null).toUpperCase().indexOf(searchFilter) == -1) {
            			continue;
            		}
            	}
            	MailBoxUser email = new MailBoxUser();
            	email.setId(rs.getLong("id"));
            	email.setStatusId(rs.getLong("status_id"));
            	email.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
            	email.setTimeRead(convertToDate(rs.getTimestamp("time_read")));
            	email.setUtcOffsetRead(rs.getString("utc_offset_read"));
            	email.setSubject(rs.getString("subject"));
            	email.setSenderName(rs.getString("sender"));
            	email.setIsHighPriority(rs.getInt("is_high_priority") == 1 ? true : false);
            	email.setFreeText(rs.getString("free_text"));
            	email.setWriterId(rs.getLong("writer_id"));
            	email.setBonusUserId(rs.getLong("bonus_user_id"));
            	email.setTransactionId(rs.getLong("transaction_id"));
            	email.setAttachmentId(rs.getLong("attachment_id"));
            	MailBoxTemplate template = new MailBoxTemplate();
            	template.setTypeId(rs.getLong("type_id"));
            	template.setTemplate(rs.getString("template"));
            	String params = rs.getString("template_parameters");
            	String attachmentName = rs.getString("attachment_name");
            	if (null != attachmentName) {
            		email.setAttachmentName(attachmentName);
            	}
		    	if (params != null) {
		    		template.setTemplate(MailBoxTemplateManagerBase.insertParameters(template.getTemplate(), params));
		    	}
                email.setTemplate(template);
                emails.add(email);
            }
        } finally {
            closeStatement(pstmt);
            closeResultSet(rs);
        }
        return emails;
    }

    public static void updateEmailsByUserId(Connection conn, long userId, String emails, long statusId) throws SQLException {
    	PreparedStatement pstmt = null;
    	try {
            String sql =
                "UPDATE " +
                    "mailbox_users " +
                "SET " +
                    "status_id = ? ";

            if (statusId == Constants.MAILBOX_STATUS_READ) {
            	sql +=
            		",time_read = sysdate, " +
            		"utc_offset_read = '" + CommonUtil.getUtcOffset() + "' ";
            }
            sql +=
              "WHERE " +
               	"user_id = ? AND " +
                 "id in ( " + emails + ") ";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, statusId);
            pstmt.setLong(2, userId);
            pstmt.executeUpdate();
    	} finally {
            closeStatement(pstmt);
        }
    }

    /**
     * Get all unread emails by userId
     * @param con
     * @param userId
     * @return
     * @throws SQLException
     */
    public static long getUnreadEmailsCount(Connection con, long userId) throws SQLException {
   	 	PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "count(id) as unreadEmails " +
                    "FROM " +
                        "mailbox_users " +
                    "WHERE " +
                    	"user_id = ? AND " +
                    	"status_id = ? ";

            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setLong(2, Constants.MAILBOX_STATUS_NEW);

            rs = pstmt.executeQuery();
            if (rs.next()) {
           	 return rs.getLong("unreadEmails");
            }
        } finally {
            closeStatement(pstmt);
            closeResultSet(rs);
        }
        return 0;
   }


    /**
     * check if we already sent email to this user with the current email
     * @param con
     * @param id user id
     * @param email user email
     * @return true if we sent email to this user or false
     * @throws SQLException
     */
    public static boolean isSentActivationEmail(Connection con, long id, String email) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                        "SELECT " +
                            "1 " +
                        "FROM " +
                            "email_authentication ea " +
                        "WHERE " +
                            "ea.user_id = ? AND " +
                            "ea.email like ? ";

            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, id);
            pstmt.setString(2, email);

            rs = pstmt.executeQuery();
            if (rs.next()) {
                return true;
            }
        } finally {
            closeStatement(pstmt);
            closeResultSet(rs);
        }
        return false;
    }

    /**
     * Increase email activation refused option
     * @param conn
     * @param userId
     * @throws SQLException
     */
    public static void increaseEmailRefused(Connection conn, long userId) throws SQLException {
    	PreparedStatement pstmt = null;
    	try {
            String sql =
                "UPDATE " +
                    "users " +
                "SET " +
                    "authorized_mail_refused_times = authorized_mail_refused_times + 1 " +
                "WHERE " +
                    "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.executeUpdate();
    	} finally {
            closeStatement(pstmt);
        }
    }

    /**
     * Return all transaction that got success status in the backgound (like jobs, notifications, etc.)
     * For running the first deposit pixel on each on of them
     *
     * @param con
     * @param userId
     * @return
     * @throws SQLException
     */
    public static ArrayList<Transaction> getDepositsForPixlRunning(Connection con, long userId) throws SQLException {
   	 	PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<Transaction> transactions = new ArrayList<Transaction>();
        try {
            String sql =

            	"SELECT " +
            	  "t.id, " +
            	  "t.amount,  " +
            	  "t.time_created, " +
            	  "u.currency_id " +
            	"FROM " +
            	  "users u, " +
            	  "transactions t, " +
            	  "transaction_types tt " +
            	"WHERE " +
            	  "u.id = ? AND " +
            	  "u.id = t.user_id AND " +
            	  "t.type_id = tt.id AND " +
            	  "t.status_id IN (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") AND " +
            	  "tt.class_type = ? AND " +
            	  "t.pixel_run_time IS NULL ";

            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setLong(2, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
            rs = pstmt.executeQuery();

            while (rs.next()) {
            	Transaction t = new Transaction();
            	t.setId(rs.getLong("id"));
            	t.setAmount(rs.getLong("amount"));
            	t.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
            	t.setCurrency(ApplicationData.getCurrencyById(rs.getInt("currency_id")));
            	transactions.add(t);
            }
        } finally {
            closeStatement(pstmt);
            closeResultSet(rs);
        }
        return transactions;
   }

    public static Transaction getFirstDepositForPixlRunning(Connection con, long userId) throws SQLException {
   	 	PreparedStatement pstmt = null;
        ResultSet rs = null;
        Transaction transaction = null;
        try {
            String sql =
            	"SELECT " +
            	  "t.id, " +
            	  "t.amount, " +
            	  "t.time_created, " +
            	  "u.currency_id, " +
            	  "t.pixel_run_time " +
            	"FROM " +
            	  "users u, " +
            	  "transactions t " +
//            	  "users_first_deposit ufd " +
            	"WHERE " +
            	  "u.id = ? AND " +
            	  "t.user_id = u.id AND " +
            	  "t.id = u.first_deposit_id  ";
//            	  "t.user_id = ufd.user_id AND " +
//            	  "t.id = ufd.first_deposit_id ";

            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();

            if (rs.next()) {
            	transaction = new Transaction();
            	transaction.setId(rs.getLong("id"));
            	transaction.setAmount(rs.getLong("amount"));
            	transaction.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
            	transaction.setCurrency(ApplicationData.getCurrencyById(rs.getInt("currency_id")));
            	transaction.setPixelRunTime(convertToDate(rs.getTimestamp("pixel_run_time")));
            }
        } finally {
            closeStatement(pstmt);
            closeResultSet(rs);
        }
        return transaction;
   }

    public static String getUserNameByEmail(Connection con, String email, long skinId) throws SQLException{
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try{
    		String sql = "SELECT u.user_name " +
    					 "FROM users u " +
    					 "WHERE upper(email) = upper(?) ";
    		ps = con.prepareStatement(sql);
    		ps.setString(1, email.toLowerCase());
    		rs = ps.executeQuery();
    		if(rs.next()){
    			return rs.getString("user_name");
    		}
    		return null;
    	} finally {
    		closeResultSet(rs);
    		closeStatement(ps);
    	}
    }

    /**
     * Get city coordinates from google.
     * On 'cities_google_coordinates' table we are saving all coordinates by city and country of the user.
     * @param con
     * @param user
     * @return
     * @throws SQLException
     */
    public static boolean getUserCityCoordinates(Connection  con, User user, String countryName) throws SQLException {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try {
    		String sql =" SELECT " +
    					"	* " +
    					" FROM " +
    					"	cities_google_coordinates cgc " +
    					" WHERE " +
    					"	(upper(cgc.user_city_name) = upper(?) OR cgc.google_city_name = ? )" +
    					"	AND upper(cgc.user_country_name) = upper(?) " +
    					"	AND cgc.google_city_latitude is not null " +
    					"	AND cgc.google_city_longtitude is not null " +
    					" ORDER BY " +
    					"	cgc.google_city_name NULLS LAST ";

    		ps = con.prepareStatement(sql);
    		ps.setString(1, user.getCityName());
    		ps.setString(2, user.getCityName());
    		ps.setString(3, countryName);
    		rs = ps.executeQuery();
    		if (rs.next()) {
    			String googleCityName = rs.getString("GOOGLE_CITY_NAME");
    			if (CommonUtil.isParameterEmptyOrNull(googleCityName)) {
    				return false;
    			}
    			String googleCityLatitude = rs.getString("GOOGLE_CITY_LATITUDE");
    			String googleCityLongtitude = rs.getString("GOOGLE_CITY_LONGTITUDE");
    			user.setCityFromGoogle(googleCityName);
    			user.setCityLatitude(googleCityLatitude);
    			user.setCityLongitude(googleCityLongtitude);
    			return true;
    		}
    		return false;
    	} finally {
    		closeResultSet(rs);
    		closeStatement(ps);
    	}
    }

    /**
     * check if user is suspend (in regulation terms) if yes return error msg (currently its in use only for iphone)
     * @param con
     * @param userName
     * @param password
     * @return null if not suspend, error msg if suspended the error msg is in the language of the user like he was logedin
     * @throws SQLException
     * @throws CryptoException
     * @throws UnsupportedEncodingException 
     * @throws BadPaddingException 
     * @throws IllegalBlockSizeException 
     * @throws NoSuchPaddingException 
     * @throws NoSuchAlgorithmException 
     * @throws InvalidKeyException 
     * @throws InvalidAlgorithmParameterException 
     */
	public static String isUserSuspend(	Connection con, String userName,
										String password)	throws SQLException, CryptoException, InvalidKeyException,
															NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
															BadPaddingException, UnsupportedEncodingException,
															InvalidAlgorithmParameterException {
		PreparedStatement ps = null;
    	ResultSet rs = null;
    	try {
    		String sql ="SELECT " +
    						"l.code, " +
                			"c.support_phone " +
    					"FROM " +
    						"users u, " +
    						"users_regulation ur, " +
    						"languages l, " +
    						"countries c " +
    					"WHERE " +
    						"upper(u.user_name) = ? AND " +
    						"u.password = ? AND " +
    						"u.is_active = 1 AND " +
    						"ur.user_id = u.id AND " +
    						"ur.suspended_reason_id > 0 AND " +
    						"l.id = u.language_id AND " +
    						"c.id = u.country_id";

    		ps = con.prepareStatement(sql);
    		ps.setString(1, userName.toUpperCase());
    		ps.setString(2, AESUtil.encrypt(new String(password)));
    		rs = ps.executeQuery();
    		if (rs.next()) {
    			Locale l = new Locale(rs.getString("code"), "EU");
    			String[] param = {rs.getString("support_phone")};
    			return CommonUtil.getMessage("CMS.error.iphone.suspend", param, l);
    		}
    		return null;
    	} finally {
    		closeResultSet(rs);
    		closeStatement(ps);
    	}
	}

	public static boolean updatePhoneVerifiedFlag(Connection con, long userId, String mobilePhone) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = "update users set mobile_phone_verified = 1 where id = ? and mobile_phone = ?";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setString(2, mobilePhone);
			return ps.executeUpdate() > 0;
		} finally {
			closeStatement(ps);
		}
	}

	public static String[] getTCTokenParts(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "select domain, end_date, is_canceled, partner_id from trading_central_access tca, trading_central_sites tcs where tca.site = tcs.ID "
						+ "and user_id = ? order by time_created desc";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return new String[] {	rs.getString("domain"), String.valueOf(rs.getDate("end_date").getTime()),
										String.valueOf(rs.getBoolean("is_canceled")), rs.getString("partner_id")};
			}
		} finally {
			closeResultSet(rs);
    		closeStatement(ps);
		}
		return null;
	}
}