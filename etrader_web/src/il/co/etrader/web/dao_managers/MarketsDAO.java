package il.co.etrader.web.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;

import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.daos.AssetIndexMarketDAO;
import com.anyoption.common.managers.MarketsManagerBase;

import il.co.etrader.dao_managers.MarketsDAOBase;
import il.co.etrader.util.SelectItemMarketsComparatorHebrewPriorityET;
import il.co.etrader.web.bl_vos.ProfitLineAssetSelectionBean;

/**
 * Web markets DAO.
 *
 * @author Tony
 */
public class MarketsDAO extends MarketsDAOBase {
    public static LinkedHashMap<Long, Market> getAllMarkets(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		LinkedHashMap<Long, Market> hm = new LinkedHashMap<Long,Market>();
		try {
			String sql =
                "SELECT " +
                    "id, " +
                    "display_name, " +
                    "decimal_point " +
				"FROM " +
                    "markets";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {
				hm.put(rs.getLong("id"), getMarketVOShort(rs));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return hm;
	}

    public static void updateMarketNameBySkin(Connection con, int skinId, Long marketId, String name, String shortName) throws SQLException {

		PreparedStatement ps = null;
        try {
        	String sql =
                "UPDATE " +
                	"market_name_skin " +
                "SET " +
                	"name = ?, " +
                	"short_name = ? " +
                "WHERE " +
	                "market_id = ? AND " +
	                "skin_id = ? ";
			ps = con.prepareStatement(sql);
			ps.setString(1, name);
			ps.setString(2, shortName);
			ps.setLong(3, marketId);
			ps.setLong(4, skinId);
			int updateRes = ps.executeUpdate();
			closeStatement(ps);

			if (updateRes == 0) {
    			sql =
                    "INSERT INTO " +
                    	"market_name_skin ( " +
                    		"id, " +
                    		"name, " +
                    		"short_name, " +
                    		"market_id, " +
                    		"skin_id" +
                    	") " +
                    "VALUES ( " +
                    	"seq_market_name_skin.NEXTVAL, " +
                    	"?, " +
                    	"?, " +
                    	"?, " +
                    	"? " +
                    ")";
    			ps = con.prepareStatement(sql);
    			ps.setString(1, name);
    			ps.setString(2, shortName);
    			ps.setLong(3, marketId);
    			ps.setLong(4, skinId);
    			ps.executeUpdate();
    			closeStatement(ps);
	    	}
        } finally {
			closeStatement(ps);
		}
	}

    public static ArrayList<ProfitLineAssetSelectionBean> getProfitLineAssetSelection(Connection conn, long skinId, long opportunityTypeId) throws SQLException {
        ArrayList<ProfitLineAssetSelectionBean> l = new ArrayList<ProfitLineAssetSelectionBean>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "E.id, " +
                    "E.display_name AS market_name, " +
                    "D.display_name AS group_name, " +
                    "C.display_name AS subgroup_display_name, " +
                    "G.id AS opportunity_id, " +
                    "G.scheduled, " +
                    "mns.name, " +
                    "E.feed_Name, " +
                    "(SELECT " +
                        "COUNT(ID) " +
                    "FROM " +
                        "opportunity_templates " +
                    "WHERE " +
                        "market_id = E.id AND " +
                        "scheduled = 1 AND " +
                        "is_active = 1 AND " +
                        "opportunity_type_id = ?) AS active_hour_templates, " +
                    "to_char(G.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing " +
                "FROM " +
                    "skin_market_groups A, " +
                    "skin_market_group_markets B LEFT JOIN " +
                    "market_display_groups C ON B.skin_display_group_id = C.id, " +
                    "market_groups D, " +
                    "markets E, " +
                    "opportunities G, " +
                    "market_name_skin mns " +
                "WHERE " +
                    "A.skin_id = ? AND " +
                    "A.id = B.skin_market_group_id AND " +
                    "A.market_group_id = D.id AND " +
                    "B.market_id = E.id AND " +
                    "EXISTS (SELECT id FROM opportunity_templates WHERE market_id = B.market_id AND is_active = 1) AND " +
                    "G.market_id = E.id AND " +
                    "G.is_published = 1 AND " +
                    "G.time_est_closing >= systimestamp AND " +
                    "G.opportunity_type_id = ? AND " +
                    "E.is_suspended = 0 AND " +
                    "mns.skin_id = A.skin_id AND " +
                    "mns.market_id = E.id " +
                "ORDER BY ";
            if (opportunityTypeId == Opportunity.TYPE_REGULAR) {
                sql +=
                    "A.priority, " +
                    "C.sort_priority, " +
                    "UPPER(mns.name), " +
                    "G.scheduled";
            } else {
                sql +=
                    "UPPER(mns.name)";
            }
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, opportunityTypeId);
            pstmt.setLong(2, skinId);
            pstmt.setLong(3, opportunityTypeId);
            rs = pstmt.executeQuery();
            long marketId = -1;
            long mId;
            Date tec;
            ProfitLineAssetSelectionBean b = null;
            while (rs.next()) {
                mId = rs.getLong("id");
                tec = getTimeWithTimezone(rs.getString("time_est_closing"));
                if (marketId != mId && (
                        rs.getInt("scheduled") == 1 ||
                        rs.getInt("active_hour_templates") > 0 ||
                        tec.getTime() - System.currentTimeMillis() <= 3600000)) {
                    marketId = mId;
                    b = new ProfitLineAssetSelectionBean();
                    b.setMarketId(rs.getLong("id"));
                    b.setGroup(rs.getString("group_name"));
                    b.setSubGroup(rs.getString("subgroup_display_name"));
                    b.setMarket(MarketsManagerBase.getMarketName(skinId, rs.getLong("id")));
                    b.setOpportunityId(rs.getLong("opportunity_id"));
                    b.setTimeEstClosing(tec);
                    b.setDisplayFeedName(AssetIndexMarketDAO.getDisplayFeedName(rs.getString("feed_Name")));
                    l.add(b);
                }
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
    }

    public static HashMap<Long, ArrayList<Market>> getETgroups(Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;

        HashMap<Long, ArrayList<Market>> groupsHM = new HashMap<Long, ArrayList<Market>>();
        try {
            String sql="SELECT " +
                          "smg.market_group_id, " +
                          "smgm.group_priority, " +
                          "m.display_name, " +
                          "mns.name " +
                        "FROM " +
                          "markets m, " +
                          "skin_market_group_markets smgm, " +
                          "skin_market_groups smg, " +
                          "market_name_skin mns " +
                        "WHERE " +
                          "smg.skin_id = " + Skin.SKIN_ETRADER + " AND " +
                          "smgm.skin_market_group_id = smg.id AND " +
                          "m.id = smgm.market_id AND " +
                          "mns.skin_id = smg.skin_id AND " +
                          "mns.market_id = smgm.market_id AND " +
                          "m.option_plus_market_id is null AND " +
                          "EXISTS ( " +
                            "SELECT " +
                              "1 " +
                            "FROM " +
                              "opportunity_templates ot " +
                            "WHERE " +
                              "ot.market_id = m.id AND " +
                              "ot.is_active = 1 AND " +
                              "ot.opportunity_type_id = 1 " +
                          ") " +
                        "ORDER BY " +
                          "smg.market_group_id, " +
                          "upper(mns.name) ";

            ps = con.prepareStatement(sql);

            rs = ps.executeQuery();
            long lastMarketGroupId = -1;
            ArrayList<Market> marketsList = null;
            while (rs.next()) {
                Market temp = new Market();
                temp.setGroupId(rs.getLong("market_group_id"));
                temp.setGroupPriority(rs.getInt("group_priority"));
                temp.setDisplayNameKey(rs.getString("display_name"));
                temp.setDisplayName(rs.getString("name"));
                if (lastMarketGroupId != temp.getGroupId()) {
                    if (lastMarketGroupId != -1) {
                        Collections.sort(marketsList, new SelectItemMarketsComparatorHebrewPriorityET());
                        groupsHM.put(lastMarketGroupId, marketsList);
                    }
                    lastMarketGroupId = temp.getGroupId();
                    marketsList = new ArrayList<Market>();
                }
                marketsList.add(temp);
            }
            Collections.sort(marketsList, new SelectItemMarketsComparatorHebrewPriorityET());
            if (lastMarketGroupId != -1) { //add the last group to the HM
                groupsHM.put(lastMarketGroupId, marketsList);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }

        return groupsHM;
    }
}