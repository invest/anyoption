package il.co.etrader.web.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.beans.ReutersQuotes;

import il.co.etrader.dao_managers.ReutersQuotesDAOBase;
import il.co.etrader.web.bl_vos.ReutersQuotesPopup;

/**
 * ReutersQuotes DAO.
 *
 * @author Eyal G
 */
public class ReutersQuotesDAO extends ReutersQuotesDAOBase {

	public static ReutersQuotesPopup getQuotesByOppId(Connection con, long oppId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ReutersQuotesPopup rqp = new ReutersQuotesPopup();
		try {
			String sql = "SELECT " +
						 	"m.display_name, " +
							"m.market_group_id, " +
							"m.decimal_point, " +
							"m.id as market_id, " +
							"o.scheduled, " +
							"o.closing_level, " +
							"o.opportunity_type_id, " +
							"rq.* " +
						"FROM " +
							"opportunities o, " +
							"markets m, " +
							"reuters_quotes rq " +
						"WHERE " +
							"o.market_id= m.id AND " +
							"o.id = rq.opportunity_id AND " +
							"o.id = ? " +
						"ORDER BY " +
							"rq.id ";



			ps = con.prepareStatement(sql);
	        ps.setLong(1, oppId);
			rs = ps.executeQuery();


			int count = 0;
			while (rs.next()) {
				if (count++ == 0) {
					rqp.setClosingLevel(rs.getDouble("closing_level"));
					rqp.setMarketDisplayName(rs.getString("display_name"));
					rqp.setMarketGroupId(rs.getLong("market_group_id"));
					rqp.setSchduled(rs.getLong("scheduled"));
					rqp.setDecimalPoint(rs.getInt("decimal_point"));
					rqp.setMarketId(rs.getLong("market_id"));
					rqp.setOpportunityTypeId(rs.getLong("opportunity_type_id"));
				}
				ReutersQuotes rq = new ReutersQuotes();
				rq.setId(rs.getLong("id"));
				rq.setAsk(rs.getBigDecimal("ask"));
				rq.setBid(rs.getBigDecimal("bid"));
				rq.setLast(rs.getBigDecimal("last"));
				rq.setTime(rs.getTimestamp("time"));
				rq.setOpportunityId(rs.getLong("opportunity_id"));
				rq.setDecimalPoint(rs.getInt("decimal_point"));
				rqp.getReutersQuotes().add(rq);
			}
			if (rqp.getReutersQuotes().size() == 3) {
				rqp.getReutersQuotes().remove(0);
				rqp.getReutersQuotes().remove(1);
			}
			if (rqp.getReutersQuotes().size() == 2) {
				rqp.getReutersQuotes().remove(0);
			}


		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return rqp;
	}

}