package il.co.etrader.web.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.MarketNameBySkin;
import com.anyoption.common.beans.OneTouchFields;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunityOddsType;
import com.anyoption.common.beans.ServerConfiguration;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.InvestmentType;
import com.anyoption.common.managers.MarketsManagerBase;

import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.dao_managers.InvestmentsDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.InvestmentFormatter;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_vos.GameRank;
import il.co.etrader.web.helper.SlipEntry;
import il.co.etrader.web.service.result.OpenPositionsMethodResult;
import il.co.etrader.web.util.Constants;

public class InvestmentsDAO extends InvestmentsDAOBase {
    protected static Logger log = Logger.getLogger(InvestmentsDAO.class);

    /**
     * Insert investment.
     *
     * @param conn the db connection to use
     * @param userId the user id
     * @param slipEntry take the oppId, choice
     * @param amount the investment amount
     * @param level the opp level at the time of invest
     * @param ip the ip from which the invest request came from
     * @param realLevel the current real level
     * @param wwwLevel the level the investment was checked against for acceptable deviation
     * @param optionPlusFee the fee when insert investment from option +
     * @param defaultAmountValue the current user default investment amount
     * @param bonus Odds Change Type Id if to mark the investment as (0 - regular, 1 next invest on us, 2 odds change percent, 3 odds change multiply)
     * @param utcOffset the UTC offset of the user when he did the investment (eg. GMT+02:00)
     * @return The id of the inserted investment.
     * @throws SQLException
     */
    public static long insertInvestment(
            Connection conn,
            long userId,
            SlipEntry slipEntry,
            long amount,
            double level,
            String ip,
            double realLevel,
            double wwwLevel,
            long bonusOddsChangeTypeId,
            String utcOffsetCreated,
            double rate,
            long bonusUsersId,
            int fromGraph,
            long optionPlusFee,
            long countryId,
            float oddsWin,
            float oddsLose,
            Long defaultAmountValue,
            boolean isLikeHourly,
            long loginID) throws SQLException {
        return insertInvestment(conn,
                    userId,
                    slipEntry.getOpportunity().getId(),
                    slipEntry.getChoice(),
                    amount,
                    level,
                    ip,
                    realLevel,
                    wwwLevel,
                    bonusOddsChangeTypeId,
                    utcOffsetCreated,
                    rate,
                    bonusUsersId,
                    oddsWin,
                    oddsLose,
                    0,
                    0,
                    fromGraph,
                    optionPlusFee,
                    1,
                    countryId,
                    defaultAmountValue,
                    isLikeHourly,
                    loginID);
    }

    /**
     * Insert investment.
     *
     * @param conn the db connection to use
     * @param userId the user id
     * @param oppId the opp id
     * @param choice up or down
     * @param amount the investment amount
     * @param level the opp level at the time of invest
     * @param ip the ip from which the invest request came from
     * @param realLevel the current real level
     * @param wwwLevel the level the investment was checked against for acceptable deviation
     * @param bonusOddsChangeTypeId to mark the investment as bonus odds change type (0 - regular, 1 - free, 2 - odds change percent, 3 - odds change multiply)
     * @param utcOffset the UTC offset of the user when he did the investment (eg. GMT+02:00)
     * @param overOddsWin odds for win
     * @param overOddsLose odds for lose
     * @param referenceInvestmentId the is of the investment that we copy the info from (0 if its new investment)
     * @param insuranceAmount for roll up insert the insurance amount
     * @param optionPlusFee the fee to pay when insert investment from option +
     * @param numberOfInvestments number of ivestments to insert in the current batch
     * @param defaultAmountValue the current user default investment amount
     * @return The id of the inserted investment.
     * @throws SQLException
     */
    public static long insertInvestment(
            Connection conn,
            long userId,
            long oppId,
            long choice,
            long amount,
            double level,
            String ip,
            double realLevel,
            double wwwLevel,
            long bonusOddsChangeTypeId,
            String utcOffsetCreated,
            double rate,
            long bonusUsersId,
            double overOddsWin,
            double overOddsLose,
            long referenceInvestmentId,
            double insuranceAmount,
            int fromGraph,
            long fee,
            int numberOfInvestments,
            long countryId,
            Long defaultAmountValue,
            boolean isLikeHourly,
            long loginID) throws SQLException {
        long id = 0;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        FacesContext context=FacesContext.getCurrentInstance();
        ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);

        try {
            String sql =
                    "INSERT INTO investments " +
                        "(id, user_id, opportunity_id, type_id, amount, current_level, odds_win, odds_lose, writer_id, ip, " +
                        "win, lose, house_result, is_settled, is_canceled, is_void, real_level, www_level, bonus_odds_change_type_id, utc_offset_created, rate, bonus_user_id, reference_investment_id, insurance_amount_ru, is_from_graph, server_id, option_plus_fee, time_created, country_id, group_inv_id, def_inv_amount, is_like_hourly, login_id) " +
                    "VALUES " +
                        "(SEQ_INVESTMENTS.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?, 0, 0, 0, 0, 0, 0, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            pstmt = conn.prepareStatement(sql);
            Date currentDate = new Date();
            log.debug("start loop");
            long seqGroupInvId = getSequenceNextVal(conn, "SEQ_INVESTMENTS_GROUP_INV_ID");

            for (int i = 0; i < numberOfInvestments; i++) {
		        pstmt.setLong(1, userId);
	            pstmt.setLong(2, oppId);
	            pstmt.setLong(3, choice);
	            pstmt.setLong(4, amount);
	            pstmt.setDouble(5, level);
	            pstmt.setDouble(6, overOddsWin);
	            pstmt.setDouble(7, overOddsLose);
	            pstmt.setLong(8, CommonUtil.getWebWriterId());
	            pstmt.setString(9, ip);
	            pstmt.setDouble(10, realLevel);
	            pstmt.setDouble(11, wwwLevel);
	            pstmt.setLong(12, bonusOddsChangeTypeId);
	            pstmt.setString(13, utcOffsetCreated);
	            pstmt.setDouble(14, rate);
	            if (bonusOddsChangeTypeId > 0 && bonusUsersId > 0) {
	                pstmt.setLong(15, bonusUsersId);
	            } else {
	                pstmt.setNull(15, Types.NUMERIC);
		        }
		        pstmt.setLong(16, referenceInvestmentId);
		        pstmt.setDouble(17, insuranceAmount);
		        pstmt.setInt(18, fromGraph);
		        pstmt.setString(19, ServerConfiguration.getInstance().getServerName());
		        pstmt.setLong(20, fee);
		        pstmt.setTimestamp(21, CommonUtil.convertToTimeStamp(currentDate));
		        pstmt.setLong(22,countryId);
		        pstmt.setLong(23,seqGroupInvId);
		        pstmt.setLong(24, defaultAmountValue);
		        pstmt.setInt(25, isLikeHourly == true ? 1 : 0);
	            if (loginID > 0 ) {
	                pstmt.setLong(26, loginID);
	            } else {
	                pstmt.setNull(26, Types.NUMERIC);
		        }
		        pstmt.addBatch();
		        log.debug("loop i = " + i);
            }
            log.debug("executeBatch");
            pstmt.executeBatch();
            log.debug("finish executeBatch");
            id = getSeqCurValue(conn, "SEQ_INVESTMENTS");
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return id;
    }
	  public static long getSequenceNextVal(Connection con,String seq) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {
			  String sql = "SELECT " + seq + ".nextval from dual";

			  ps = con.prepareStatement(sql);
			  rs = ps.executeQuery();
				if (rs.next()) {
					return rs.getLong(1);
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return 0;
	  }


    /**
     * Duplicated for etrader graph
     * Load todays opened investments for specified user. Only the fields needed for the trading page
     * right side history are loaded (market name, investment type, current level, est close time, market_id).
     *
     * @param conn
     * @param userId
     * @return <code>ArrayList<Investment></code>
     * @throws SQLException
     */
    public static ArrayList<Investment> getTodaysOpenedInvestmentsProfit(Connection conn, long userId) throws SQLException {
        ArrayList<Investment> l = new ArrayList<Investment>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
	                    "C.display_name, " +
	                    "A.utc_offset_created, " +
	                    "to_char(B.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, " +
	                    "to_char(B.time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_first_invest, " +
	                    "sum(A.amount) AS sumInv, " +
	                    "u.currency_id, " +
	                    "B.id oppId, " +
	                    "C.id marketId " +
                    "FROM " +
                        "investments A, " +
                        "opportunities B, " +
                        "markets C, " +
                        "users u " +
                    "WHERE " +
                        "A.opportunity_id = B.id AND " +
                        "B.market_id = C.id AND " +
                        "A.user_id = ? AND " +
                        "trunc(SYS_EXTRACT_UTC(B.time_est_closing) -1/24) = trunc(sysdate) AND " +
                        "A.is_settled = 0 AND " +
                        "A.is_canceled = 0 AND " +
                        "u.id = A.user_id AND " +
                        "A.is_void = 0 AND " +
                        "B.is_open_graph = 1 AND " +
                        "B.is_published = 1 AND " +
                        "investment_real_scheduled(A.id) = 1 AND " +
                        "A.type_id not in (" + InvestmentType.BUY + ", " + InvestmentType.SELL + ") " +
                   "GROUP BY " +
                        "time_est_closing, " +
                        "time_first_invest, " +
                        "A.utc_offset_created, " +
                        "C.display_name, " +
                        "u.currency_id, " +
                        "B.id, " +
                        "C.id " +
                    "ORDER BY " +
                        "B.time_est_closing ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
            //int cnt = 0;
            while (rs.next() /*&& cnt < 6*/) { // Oracle LIMIT clause (LOL)
                Investment i = new Investment();
                i.setMarketName(MarketsManagerBase.getMarketShortName(Skins.SKIN_REG_EN, rs.getLong("marketId")));
                i.setUtcOffsetCreated(rs.getString("utc_offset_created"));
                try {
                    i.setTimeEstClosing(localDateFormat.parse(rs.getString("time_est_closing")));
                    i.setTimeFirstInvest(localDateFormat.parse(rs.getString("time_first_invest")));
                    i.setUtcOffsetEstClosing(ApplicationData.getUserFromSession().getUtcOffset());
                } catch (Throwable t) {
                    log.error("Can't parse time est closing.", t);
                }
                i.setSumInvest(rs.getLong("sumInv"));
                i.setCurrencyId(rs.getLong("currency_id"));
                i.setOpportunityId(rs.getLong("oppId"));
                i.setMarketId(rs.getLong("marketId"));
                l.add(i);
                //cnt++;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
    }

    /**
     * Load todays opened investments for specified user. Only the fields needed for the trading page
     * right side history are loaded (market name, investment type, current level, est close time, market_id).
     *
     * @param conn
     * @param userId
     * @return <code>ArrayList<Investment></code>
     * @throws SQLException
     */
    public static ArrayList<Investment> getTodaysOpenedInvestments(Connection conn, long userId) throws SQLException {
        ArrayList<Investment> l = new ArrayList<Investment>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "A.id, " +
                        "C.display_name, " +
                        "A.type_id, " +
                        "A.current_level, " +
                        "A.utc_offset_created, "+
                        "to_char(B.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, " +
                        "B.market_id, " +
                        "A.amount, " +
                        "u.currency_id, " +
                        "A.opportunity_id, " +
                        "B.scheduled, " +
                        "B.is_open_graph " +
                    "FROM " +
                        "investments A, " +
                        "opportunities B, " +
                        "markets C, " +
                        "users u " +
                    "WHERE " +
                        "A.opportunity_id = B.id AND " +
                        "B.market_id = C.id AND " +
                        "A.user_id = ? AND " +
                        "trunc(SYS_EXTRACT_UTC(B.time_est_closing) -1/24) = trunc(sysdate) AND " +
                        "A.is_settled = 0 AND " +
                        "A.is_canceled = 0 AND " +
                        "u.id = A.user_id AND " +
                        "A.is_void = 0 AND " +
                        "A.type_id not in (" + InvestmentType.BUY + ", " + InvestmentType.SELL + ") " +
                    "ORDER BY " +
                        "B.time_est_closing, " +
                        "A.time_created";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
            int cnt = 0;
            while (rs.next() && cnt < 6) { // Oracle LIMIT clause (LOL)
                Investment i = new Investment();
                i.setId(rs.getLong("id"));
                i.setMarketName(MarketsManagerBase.getMarketShortName(Skins.SKIN_REG_EN, rs.getLong("market_id")));
                i.setTypeId(rs.getLong("type_id"));
                i.setCurrentLevel(rs.getDouble("current_level"));
                i.setCurrencyId(rs.getLong("currency_id"));
                i.setUtcOffsetCreated(rs.getString("utc_offset_created"));
                try {
                    i.setTimeEstClosing(localDateFormat.parse(rs.getString("time_est_closing")));
                    i.setUtcOffsetEstClosing(ApplicationData.getUserFromSession().getUtcOffset());
                } catch (Throwable t) {
                    log.error("Can't parse time est closing.", t);
                }

                if ( i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_ONE ) {
					OneTouchFields oneTouchFields= InvestmentsDAOBase.getOneTouchFields(conn, Integer.parseInt(rs.getString("opportunity_id")));
					i.setOneTouchDecimalPoint(oneTouchFields.getDecimalPoint());
					i.setOneTouchUpDown(oneTouchFields.getUpDown());
				}
                i.setMarketId(rs.getLong("market_id"));
                i.setAmount(rs.getLong("amount"));
                i.setScheduledId(rs.getLong("scheduled"));
                i.setOpenGraph(rs.getInt("is_open_graph")==1);
                i.setOpportunityId(rs.getLong("opportunity_id"));
                l.add(i);
                cnt++;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
    }

    protected static com.anyoption.common.beans.Investment getVOExtended(Connection conn, ResultSet rs) throws SQLException {
    	com.anyoption.common.beans.Investment i = getVO(rs);
        i.setMarketName(MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, rs.getLong("market_id")));
        i.setMarketId(rs.getLong("market_id"));
        i.setTypeName(CommonUtil.getMessage(rs.getString("type_name"), null));
        i.setClosingLevel(rs.getDouble("closing_level"));
        i.setTimeEstClosing(getTimeWithTimezone(rs.getString("time_est_closing")));
        i.setTimeLastInvest(getTimeWithTimezone(rs.getString("time_last_invest")));
        i.setTimeFirstInvest(getTimeWithTimezone(rs.getString("time_first_invest")));
        i.setUtcOffsetEstClosing(rs.getString("utc_offset_created"));
        if (rs.getString("time_act_closing") != null) {
            i.setTimeActClosing(getTimeWithTimezone(rs.getString("time_act_closing")));
            if (null != rs.getString("utc_offset_settled")) {
            	i.setUtcOffsetEstClosing(rs.getString("utc_offset_settled")); //the offset of time_act_closing of the opportunity is equal to offset of time_settled of the investment
            }
        }
        i.setScheduledId(rs.getLong("scheduled"));
        i.setUserName(rs.getString("username"));
        i.setUserFirstName(rs.getString("firstname"));
        i.setUserLastName(rs.getString("lastname"));
        i.setCurrencyId(rs.getLong("currency_id"));

        // set up_down field for one touch investments
        if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_ONE) {
            OneTouchFields oneTouchFields = InvestmentsDAOBase.getOneTouchFields(conn, i.getOpportunityId());
            i.setOneTouchDecimalPoint(oneTouchFields.getDecimalPoint());
            i.setOneTouchUpDown(oneTouchFields.getUpDown());
        }
        i.setRolledInvId(rs.getLong("rolled_inv_id"));
        return i;
    }

    public static ArrayList<com.anyoption.common.beans.Investment> getByUser(Connection con, long id, Date from, Date to, String isSettled, String marketGroup, boolean isBubble, long bubbleMarket, long skinId) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<com.anyoption.common.beans.Investment> list = new ArrayList<com.anyoption.common.beans.Investment>();
		if(isBubble){
			marketGroup = "0,0";
		}else{
			bubbleMarket = 0;
		}
		int market = Integer.valueOf(marketGroup.split(",")[1]);
		int group = Integer.valueOf(marketGroup.split(",")[0]);
		try {
			String sql =
					"SELECT " +
                        "i.*, " +
                        "m.display_name market_name, " +
                        "m.id market_id, " +
                        "int.display_name type_name, " +
                        "u.user_name username, " +
                        "u.first_name firstname, " +
                        "u.last_name lastname, " +
                        "to_char(o.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, " +
                        "to_char(o.time_last_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_last_invest, " +
                        "to_char(o.time_act_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_act_closing, " +
                        "to_char(o.time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_first_invest, " +
                        "o.closing_level, " +
                        "o.scheduled scheduled, " +
                        "o.current_level opp_curr_level," +
                        "u.currency_id, " +
                        "iref.d rolled_inv_id, " +
                        "o.is_open_graph," +
                        "t.amount transaction_amount, " +
                        "b.display_name bonus_display_name, " +
                        "so.id so_id, " +
                        "(CASE WHEN i.time_settled < SYS_EXTRACT_UTC(o.time_est_closing) " +
                        				" AND o.opportunity_type_id = " + Opportunity.TYPE_OPTION_PLUS + " " +
                        	 " THEN 1 ELSE 0 END) as is_option_plus, " +
                        "(CASE WHEN i.time_settled < SYS_EXTRACT_UTC(o.time_est_closing) " +
             				" AND o.opportunity_type_id = " + Opportunity.TYPE_DYNAMICS + " " +
             			" THEN 1 ELSE 0 END) as is_dynamics, " +				
                        " o.opportunity_type_id, " +
                        "(CASE WHEN EXISTS (SELECT 1 FROM reuters_quotes rq WHERE rq.opportunity_id = o.id) AND NOT EXISTS (SELECT 1 FROM log l WHERE l.key_value = o.id AND l.command = " + ConstantsBase.LOG_COMMANDS_RESETTLE_OPP + ") THEN 1 ELSE 0 END) AS haveReutersQuotes " +
					"FROM " +
                        "investments i " +
                        	"LEFT JOIN show_off so ON so.investment_id = i.id " +
                        	"LEFT JOIN transactions t ON t.bonus_user_id = i.bonus_user_id and i.bonus_odds_change_type_id > 0 " +
                        	"LEFT JOIN bonus_users bu ON bu.id = i.bonus_user_id and i.bonus_odds_change_type_id > 0 " +
                        		"LEFT JOIN bonus b ON bu.bonus_id = b.id " +
                        	"LEFT JOIN (select " +
				                        "   i2.id as d, " +
				                        "   i2.reference_investment_id as r " +
				                        "from " +
				                        "   investments i2 " +
				                        "where " +
				                        "   i2.reference_investment_id > 0 and " +
				                        "   i2.user_id = ?) iref ON i.id = iref.r, " +
                        "opportunities o, " +
                        "markets m, " +
                        "investment_types int, " +
                        "users u ";
			if (group > 0 && market == 0) {
				sql +=
				        ", skin_market_group_markets smgm, " +
                        "skin_market_groups smg ";
			}
			sql +=
                    "WHERE " +
                        "i.type_id = int.id " +
                        "AND u.id = i.user_id " +
                        "AND i.user_id = ? " +
                        "AND i.opportunity_id = o.id " +
                        "AND o.market_id = m.id " +
                        "AND i.time_created >= ? " +
                        "AND i.time_created <= ? " +
                        "AND int.id not in (" + InvestmentType.BUY + ", " + InvestmentType.SELL + ") ";
			if (isSettled != null && !isSettled.equals("")) {
				sql +=
                        "AND i.is_settled = " + Integer.parseInt(isSettled) + " ";
			}
			if (market > 0) {
				sql += "AND ( m.id = " + market + " OR m.option_plus_market_id = " + market + " ) ";
			}
			if (group > 0 && market == 0) {
				sql +=
                        "AND smgm.skin_market_group_id = smg.id " +
                        "AND smg.market_group_id = ? " +
					    "AND m.id = smgm.market_id " +
                        "AND smg.skin_id = ? ";
			}
			
			if(bubbleMarket > 0 && group == 0 && market == 0){
				sql += " AND m.id = " + bubbleMarket ;
			}
			
			if (isBubble) {
				sql += " AND i.type_id = " + com.anyoption.common.beans.base.Investment.TYPE_BUBBLE;
			}else{
				sql += " AND i.type_id <> " + com.anyoption.common.beans.base.Investment.TYPE_BUBBLE;
			}
			sql +=
                    " ORDER BY " +
                        "i.time_created desc";

			pstmt = con.prepareStatement(sql);
            pstmt.setLong(1,id);
            pstmt.setLong(2,id);
            pstmt.setTimestamp(3, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, CommonUtil.getUtcOffset())));
            pstmt.setTimestamp(4, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to), CommonUtil.getUtcOffset())));
            if (group > 0 && market == 0) {
            	pstmt.setInt(5, group);
            	pstmt.setLong(6, CommonUtil.getAppData().getSkinId());
            }
			rs = pstmt.executeQuery();
			while(rs.next()) {
				com.anyoption.common.beans.Investment i = new Investment();
				i = getVOExtended(con, rs);
				i.setMarketName(MarketsManagerBase.getMarketName(skinId, rs.getLong("market_id")));
				i.setOpenGraph(rs.getInt("is_open_graph")==1);
				i.setTransactionAmount(rs.getLong("transaction_amount"));
				i.setBonusDisplayName(rs.getString("bonus_display_name"));
				i.setHasShowOff(0 != rs.getLong("so_id"));
				i.setShowOffId(rs.getLong("so_id"));
                i.setOptionPlus(rs.getInt("is_option_plus") == 1);
                i.setDynamics(rs.getInt("is_dynamics") == 1);
                i.setOpportunityTypeId(rs.getLong("opportunity_type_id"));
                i.setHaveReutersQuote(rs.getInt("haveReutersQuotes") == 1);
                i.setOppCurrentLevel(rs.getDouble("opp_curr_level"));
				if (isBubble) {
					i.setBubbleLowLevel(rs.getDouble("bubble_low_level"));
					i.setBubbleHighLevel(rs.getDouble("bubble_high_level"));
					i.setBubbleStartTime(convertToDate(rs.getTimestamp("bubble_start_time")));
					i.setBubbleEndTime(convertToDate(rs.getTimestamp("bubble_end_time")));
				}
				list.add(i);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return list;
	}

//    public static ArrayList<com.anyoption.common.beans.Investment> getOpenedByUser(Connection con, long userId, long opportunityId, long typeId) throws SQLException {
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//		ArrayList<com.anyoption.common.beans.Investment> list = new ArrayList<com.anyoption.common.beans.Investment>();
//		try {
//			String sql =
//					"SELECT " +
//                        "i.*, " +
//                        "m.display_name market_name, " +
//                        "m.id market_id, " +
//                        "int.display_name type_name, " +
//                        "u.user_name username, " +
//                        "u.first_name firstname, " +
//                        "u.last_name lastname, " +
//                        "to_char(o.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, " +
//                        "to_char(o.time_last_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_last_invest, " +
//                        "to_char(o.time_act_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_act_closing, " +
//                        "to_char(o.time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_first_invest, " +
//                        "o.closing_level, " +
//                        "o.scheduled scheduled, " +
//                        "u.CURRENCY_ID, " +
//                        "iref.d rolled_inv_id " +
//                    "FROM " +
//                        "investments i, " +
//                        "opportunities o, " +
//                        "markets m, " +
//                        "investment_types int, " +
//                        "users u, " +
//                        "(select " +
//                        "   i2.id as d, " +
//                        "   i2.REFERENCE_INVESTMENT_ID as r " +
//                        "from " +
//                        "   investments i2 " +
//                        "where " +
//                        "   i2.REFERENCE_INVESTMENT_ID > 0 and " +
//                        "   i2.user_id = ?) iref " +
//					"WHERE " +
//					    "i.type_id = int.id " +
//                        "AND u.id = i.user_id " +
//						"AND i.user_id = ? " +
//						"AND i.opportunity_id = o.id " +
//                        "AND o.market_id = m.id " +
//                        "AND i.id = iref.r(+) " +
//					    "AND i.is_settled = 0 " +
//					    "AND o.is_published = 1 " + // only not settled investments
//					    "AND o.opportunity_type_id = ? " +
//                        (opportunityId > 0 ? "AND i.opportunity_id = ? " : "") +
//					"ORDER BY " +
//                        "i.time_created";
//			pstmt = con.prepareStatement(sql);
//			pstmt.setLong(1, userId);
//            pstmt.setLong(2, userId);
//            pstmt.setLong(3, typeId);
//            if (opportunityId > 0) {
//                pstmt.setLong(4, opportunityId);
//            }
//			rs = pstmt.executeQuery();
//			while(rs.next()) {
//				list.add(getVOExtended(con, rs));
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(pstmt);
//		}
//		return list;
//	}

    public static boolean isHasOpened(Connection conn, long userId) throws SQLException {
        boolean b = false;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "COUNT(id) AS count " +
                    "FROM " +
                        "investments  " +
                    "WHERE " +
                        "user_id = ? " +
                        "AND is_settled = 0"; // only not settled opps
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                b = rs.getInt("count") > 0;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return b;
    }

    public static void getInvestmentDetailsForPrint(Connection conn, SlipEntry se, long skinId) throws Exception {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try{
            String sql =
                    "SELECT " +
                        "A.amount - A.option_plus_fee as amount, " +
                        "A.type_id, " +
                        "A.current_level, " +
                        "A.odds_win, " +
                        "A.odds_lose, " +
                        "to_char(B.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, " +
                        "C.display_name, " +
                        "oot.up_down, " +
                        "B.market_id as market_id " +
                    "FROM " +
                        "investments A, " +
                        "opportunities B " +
                        	"LEFT JOIN opportunity_one_touch oot on B.id = oot.opportunity_id, " +
                        "markets C " +
                    "WHERE " +
                        "A.opportunity_id = B.id AND " +
                        "B.market_id = C.id AND " +
                        "A.user_id = ? AND " +
                        "A.id = ?";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, se.getUserId());
            pstmt.setLong(2, se.getInvestmentId());
            rs = pstmt.executeQuery();
            if (rs.next()) {
                se.setAmount(((double) rs.getLong("amount")) / 100);
                se.setChoice(rs.getInt("type_id"));
                Opportunity o = new Opportunity();
                o.setCurrentLevel(rs.getDouble("current_level"));
                SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
                o.setTimeEstClosing(localDateFormat.parse(rs.getString("time_est_closing")));
                OpportunityOddsType odds = new OpportunityOddsType();
                odds.setOverOddsWin(rs.getFloat("odds_win"));
                odds.setOverOddsLose(rs.getFloat("odds_lose"));
                o.setOddsType(odds);
                Market m = new Market();
                m.setDisplayName(MarketsManagerBase.getMarketName(skinId, rs.getLong("market_id")));
                m.setId(rs.getLong("market_id"));
                o.setMarket(m);
                String upDown = rs.getString("up_down");
                if (null != upDown && se.getChoice() == InvestmentsManagerBase.INVESTMENT_TYPE_ONE) {
                	o.setIsOneTouchUp(upDown.equals("1") ? true : false);
                }

                String[] params = new String[3];
                params[0] = o.getMarket().getDisplayName();
                if (m.getId() == ConstantsBase.MARKET_FACEBOOK_ID) {
                	params[0] = String.valueOf(o.getCurrentLevel());
                }
            	params[1] = String.valueOf(o.getCurrentLevel());
            	Calendar c = Calendar.getInstance();
            	c.setTime(o.getTimeEstClosing());

            	String oneTouchMsg = "";

            	 if (o.getIsOneTouchUp()) {
            		 oneTouchMsg = "onetouch.markets.msg.up";
//            		 if (m.getId() == ConstantsBase.MARKET_FACEBOOK_ID)  {
//            			 oneTouchMsg = "facebook.marketMsg1.above";
//            		 }
            	} else {
            		oneTouchMsg = "onetouch.markets.msg.down";
//            		if (m.getId() == ConstantsBase.MARKET_FACEBOOK_ID)  {
//           			 	oneTouchMsg = "facebook.marketMsg1.below";
//           		 	}
            	}

            	params[2] = c.get(Calendar.DAY_OF_MONTH) + "." + ( c.get(Calendar.MONTH) + 1 ) + "." + c.get(Calendar.YEAR);
           		se.setOneTouchMsg(CommonUtil.getMessage(oneTouchMsg,params));

                se.setOpportunity(o);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
    }
    /**
     * Update is_accepted_sms by investment id
     * @param con
     * @param invId investment to update
     * @throws SQLException
     */
    public static void updateAcceptedSmsById(Connection con, long invId) throws SQLException {
    	PreparedStatement ps = null;
    	try {
    		String sql =
    			"UPDATE " +
    				"investments " +
    			"SET " +
    				"is_accepted_sms = 1 " +
    			"WHERE " +
    				"id = ?";
    		ps = con.prepareStatement(sql);
    		ps.setLong(1, invId);
    		ps.executeUpdate();
    	} finally {
    		closeStatement(ps);
    	}
    }

    public static String getInsuranceInvesetmentsByUser(Connection conn, long id, long insuranceStartTime, long insuranceEndTime) throws SQLException {
        String result = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql = "select " +
                            "id, " +
                            "(select " +
                                    "count(1) " +
                             "from " +
                                "investments i " +
                             "where " +
                                "i.OPPORTUNITY_ID  in (select " +
                                                           "id " +
                                                       "from " +
                                                           "opportunities " +
                                                       "where " +
                                                           "is_published = 1 AND " +
                                                           "scheduled = 1 AND " +
                                                           "time_first_invest <= sysdate AND " +
                                                           "time_est_closing >= sysdate AND " +
                                                           "time_est_closing - to_dsinterval('0 00:' || ? || ':00')  > sysdate AND " +
                                                           "time_est_closing - to_dsinterval('0 00:' || ? || ':00')  <= sysdate) AND " +
                                "i.INSURANCE_FLAG = 1 AND " +
                                "i.IS_SETTLED = 0 AND " +
                                "i.user_id = ? ) as GM, " +
                               "(select " +
                                       "count(1) " +
                                "from " +
                                       "investments i " +
                                "where " +
                                       "i.OPPORTUNITY_ID  in (select " +
                                                                  "id " +
                                                              "from " +
                                                                  "opportunities " +
                                                              "where " +
                                                                  "is_published = 1 and " +
                                                                  "scheduled = 1 AND " +
                                                                  "time_first_invest <= sysdate AND " +
                                                                  "time_est_closing >= sysdate AND " +
                                                                  "time_est_closing - to_dsinterval('0 00:' || ? || ':00')  > sysdate AND " +
                                                                  "time_est_closing - to_dsinterval('0 00:' || ? || ':00')  <= sysdate) AND " +
                                       "i.INSURANCE_FLAG = 2 AND " +
                                       "i.IS_SETTLED = 0 AND " +
                                       "i.user_id = ? ) as RU " +
                         "from " +
                             "investments i " +
                         "where " +
                             "i.USER_ID = ? and " +
                             "i.IS_SETTLED = 0 and " +
                             "i.insurance_flag > 0 and " +
                             "i.OPPORTUNITY_ID in (select " +
                                                      "o.id " +
                                                  "from " +
                                                      "opportunities o " +
                                                  "where " +
                                                      "is_published = 1 and " +
                                                      "scheduled = 1 AND " +
                                                      "o.time_first_invest <= sysdate AND " +
                                                      "o.time_est_closing >= sysdate AND " +
                                                      "time_est_closing - to_dsinterval('0 00:' || ? || ':00')  > sysdate AND " +
                                                      "time_est_closing - to_dsinterval('0 00:' || ? || ':00')  <= sysdate)";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, insuranceEndTime);
            pstmt.setLong(2, insuranceStartTime);
            pstmt.setLong(3, id);
            pstmt.setLong(4, insuranceEndTime);
            pstmt.setLong(5, insuranceStartTime);
            pstmt.setLong(6, id);
            pstmt.setLong(7, id);
            pstmt.setLong(8, insuranceEndTime);
            pstmt.setLong(9, insuranceStartTime);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                if (rs.getInt("GM") > 0) {
                    result = rs.getString("id") + "_" + Constants.INSURANCE_GOLDEN_MINUTES;
                } else if (rs.getInt("RU") > 0) {
                    result = rs.getString("id") +  "_" + Constants.INSURANCE_ROLL_UP;
                }
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }

        return result;
    }


    /*************************************************************/
    /////////////////////// Calcalist!!! //////////////////////////
    /*************************************************************/
    public static ArrayList<GameRank> getGameUsersRanking(Connection con, long userId, int rowsNum,
    														 String fromDate, String toDate) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<GameRank> list = new ArrayList<GameRank>();
		try {
			String sql =
					"select user_id, first_name, last_name ,win_sum " +
					"from " +
							"(select user_id,first_name,last_name,win_sum " +
							"from " +
								"(select u.id user_id , u.first_name, u.last_name, SUM(i.win - i.lose) win_sum " +
								"from users u, investments i " +
								"where u.id = i.user_id " +
										"and u.user_name like '%/" + ConstantsBase.CAL_GAME_USER_NAME_SUFFIX.toUpperCase() + "' escape '/'" +
										"and i.time_created between to_date(?,'DD/MM/YYYY') and to_date(?,'DD/MM/YYYY')+ 1 " +
										"and i.is_canceled = 0 " +
										"and i.is_settled = 1 " +
								"group by u.id, u.first_name, u.last_name " +
								"having SUM(i.win- i.lose) > 0 " +
								"order by SUM(i.win- i.lose) desc) " +
							"where rownum <= ? " +

							"UNION " +

							"select u.id user_id, u.first_name, u.last_name, (case when SUM(i.win- i.lose) is null then 0 else SUM(i.win- i.lose) end) win_sum " +
							"from users u left join investments i on u.id = i.user_id " +
																		"and i.time_created between to_date(?,'DD/MM/YYYY') and to_date(?,'DD/MM/YYYY')+1 " +
																		"and i.is_canceled = 0 " +
																		"and i.is_settled = 1 " +
							"where  u.id = ? " +
							"group by u.id, u.first_name, u.last_name) " +
					"order by win_sum desc ";

			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, fromDate);
			pstmt.setString(2, toDate);
			pstmt.setInt(3, rowsNum);
            pstmt.setString(4, fromDate);
            pstmt.setString(5, toDate);
            pstmt.setLong(6, userId);

			rs = pstmt.executeQuery();
			int index = 1;
			while(rs.next()) {
				GameRank gameRank = new GameRank();

				long currUserId = rs.getLong("user_id");
				String firstName = rs.getString("first_name");
				String lastName = rs.getString("last_name");
				String shortName = firstName + " " + lastName.substring(0, 1);
				String fullName = firstName + " " + lastName;

				if (shortName.length() > ConstantsBase.RANKING_TABLE_SHORT_LENGTH){
					shortName = shortName.substring(0, ConstantsBase.RANKING_TABLE_SHORT_LENGTH);
				}
				if (fullName.length() > ConstantsBase.RANKING_TABLE_LONG_LENGTH){
					fullName = fullName.substring(0, ConstantsBase.RANKING_TABLE_LONG_LENGTH);
				}

				gameRank.setUserId(currUserId);
				gameRank.setUserName(shortName);
				gameRank.setUserNameFull(fullName);
				gameRank.setWinSum(CommonUtil.displayAmount(rs.getString("win_sum"), (int)ConstantsBase.CURRENCY_ILS_ID));

				if (currUserId == userId){
					gameRank.setRowColor(Constants.RANKING_RED_COLOR);
				} else {
					gameRank.setRowColor(Constants.EMPTY_STRING);
				}

				// if index is bigger than rowsNum it means the requested user wasn't in the list
				// so it's index shoul be blank
				if (index <= rowsNum){
					gameRank.setRank(String.valueOf(index++));
				}else{
					gameRank.setRank("");
				}

				list.add(gameRank);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return list;
	}

	public static long getSmsInvestmentLimit(Connection conn, Long currencyId) throws SQLException {
		long smsLimit = 0;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    " SELECT " +
                        " SMS_LIMIT " +
                    " FROM " +
                        " currencies " +
                    " WHERE " +
                        " id = " + currencyId;

            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            if (rs.next()) {
            	smsLimit = rs.getLong("SMS_LIMIT");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return smsLimit;
	}

	public static long addShowOff(Connection conn, long investmentId, int showOffNumber) throws SQLException {
		PreparedStatement pstmt = null;
		long showOffId = 0;
        try {
            String sql =
                    "INSERT " +
                    	"INTO show_off (id, investment_id, anyoption, facebook, twitter, show_off_number, time_created) " +
                    	"VALUES(SEQ_SHOW_OFF.NEXTVAL, ?, 1, 0, 0, ?, ?)";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, investmentId);
            pstmt.setLong(2, showOffNumber);
            pstmt.setTimestamp(3, CommonUtil.convertToTimeStamp(new Date()));
            pstmt.executeUpdate();
            showOffId = getSeqCurValue(conn, "seq_show_off");
        } finally {
            closeStatement(pstmt);
        }
        return showOffId;
	}

	/**
	 * Never send facebook == true && twitter == true in the same time
	 * @param conn
	 * @param investmentId
	 * @param facebook
	 * @param twitter
	 * @return
	 * @throws SQLException
	 */
	public static int updateShowOff(Connection conn, long investmentId, boolean facebook, boolean twitter) throws SQLException {
		if (!(facebook || twitter)) {
    		return 0;
    	}
		PreparedStatement pstmt = null;
		int ans = 0;
        try {
        	String sql = null;
        	if (facebook) {
	            sql = "UPDATE show_off " +
	            		"SET facebook = ? " +
	            		"WHERE  investment_id = ?";
        	}
        	if (twitter) {
        		sql = "UPDATE show_off " +
	        		"SET twitter = ? " +
	        		"WHERE  investment_id = ?";
        	}
            pstmt = conn.prepareStatement(sql);
            pstmt.setBoolean(1, true);
            pstmt.setLong(2, investmentId);

            ans = pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
        return ans;
	}

	public static List<com.anyoption.common.beans.Investment> getShowOffInvestments(Connection conn, long rowNum, boolean showOffLocale, String langCode, long skinId, boolean shortText) throws SQLException {
		List<com.anyoption.common.beans.Investment> l = new ArrayList<com.anyoption.common.beans.Investment>();
        Statement st = null;
        ResultSet rs = null;
        try {
            String sql =
                " SELECT * FROM (" +
                "SELECT  " +
                	"i.*, " +
                	" m.id as market_id, " +
                	"so.show_off_number, " +
                	"to_char(so.time_created, 'DD.MM.YYYY HH24:MI:SS') show_off_time, " +
                	"u.first_name, " +
                	"u.last_name, " +
                	"u.currency_id, " +
                	"it.display_name it_display_name, " +
                	"m.display_name m_display_name, " +
                	"l.code lang_code, " +
                	"u.skin_id " +
                " FROM " +
                	"investments i, " +
                	"show_off so, " +
                	"users u, " +
                	"investment_types it, " +
                	"opportunities op, " +
                	"markets m, " +
                	"skins s, " +
                	"languages l " +
                " WHERE " +
                	"i.id = so.investment_id AND " +
                	"i.user_id = u.id AND " +
                	"i.type_id = it.id AND " +
                	"i.opportunity_id = op.id AND " +
                	"op.market_id = m.id AND " +
                	"u.skin_id = s.id AND " +
                	"s.default_language_id = l.id ";

       	 ApplicationData ap = (ApplicationData) FacesContext.getCurrentInstance().getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(FacesContext.getCurrentInstance());
         if(ap.getSkinId() == Skin.SKIN_EN_US ||
        		 ap.getSkinId() == Skin.SKIN_ES_US) {
         	sql += " AND m.market_group_id <> " + Constants.MARKET_GROUP_COMMODITIES +
         	       " AND m.market_group_id <> " + Constants.MARKET_GROUP_CURRENCIES;
         }
         if (skinId > -1) {
            	sql +=
            		" AND u.skin_id = " + skinId;
            }
            sql +=
                " ORDER BY so.id DESC )" +
                " WHERE rownum <= " + rowNum;
            st = conn.createStatement();
            rs = st.executeQuery(sql);
            while (rs.next()) {
            	com.anyoption.common.beans.Investment i = getVO(rs);
            	i.setCountryId(rs.getLong("country_id"));
                i.setShowOffName(CommonUtil.capitalizeFirstLetters
                		(rs.getString("first_name") + " " + rs.getString("last_name").substring(0, 1)));
            	i.setCurrencyId(rs.getLong("currency_id"));
            	i.setTypeName(CommonUtil.getMessage(rs.getString("it_display_name"), null));
            	Locale locale = null;
            	if (showOffLocale) {
            		locale = new Locale(rs.getString("lang_code"));
            		i.setMarketName(MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, rs.getLong("market_id")));
            		i.setSkin(String.valueOf(rs.getLong("skin_id")));
	           		i.setSkin(String.valueOf(rs.getLong("skin_id")));
            	} else if (null != langCode) {
            		try {
            			locale = new Locale(langCode);
            		} catch (Exception e) {
						// do nothing!
					}
            	}
            	i.setMarketName(MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, rs.getLong("market_id")));
            	String dateDistance = null;
            	if (null == locale) {
            		dateDistance = CommonUtil.getDateDistance(i.getTimeCreated(), i.getTimeSettled(), null);
            	} else {
            		dateDistance = CommonUtil.getDateDistance(i.getTimeCreated(), i.getTimeSettled(), locale);
            	}
            	String[] tempStringArr = CommonUtil.getDateAndTimeShowOffAsArray(i.getTimeCreated(),i.getUtcOffsetCreated());
            	//tempstringArr[0] = date ,tempstringArr[2] = time
            	String[] strArr = new String[] {
            			CommonUtil.removeFractionFromAmount(CommonUtil.displayAmount(i.getAmount(), i.getCurrencyId())),
            			i.getTypeName(),
            			i.getMarketName(),
            			tempStringArr[0],
            			// TODO remove the cast
            			CommonUtil.removeFractionFromAmount(InvestmentFormatter.getRefundTxt(i)),
            			dateDistance,
            			tempStringArr[2]};
            	String showOffMessegeKey = null;
            	if (shortText) {
            		showOffMessegeKey = "showOff.twitter.sentence.";
            	} else {
            		showOffMessegeKey = "showOff.sentence.";
            	}
            	String showOffText;
            	if (null == locale) {
            		showOffText = CommonUtil.getMessage(showOffMessegeKey + String.valueOf(rs.getLong("show_off_number")), strArr);
            	} else {
            		showOffText = CommonUtil.getMessage(showOffMessegeKey + String.valueOf(rs.getLong("show_off_number")), strArr, locale);
            	}
            	i.setLastShowOffMessegeKey(showOffMessegeKey + String.valueOf(rs.getLong("show_off_number")));
            	i.setShowOffText(showOffText);
            	SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
            	i.setShowOffTime(sdf.parse(rs.getString("show_off_time")));
                l.add(i);
            }
        } catch (ParseException e) {
			throw new SQLException(e.getMessage());
		} finally {
            closeResultSet(rs);
            closeStatement(st);
        }
        return l;
	}

	public static com.anyoption.common.beans.Investment getShowOffInvestmentByInvId(Connection conn, long invId) throws SQLException {
		com.anyoption.common.beans.Investment inv = null;
       PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT  i.*, so.show_off_number, u.id user_id, u.first_name, u.last_name, " +
                    	"u.country_id, u.currency_id, it.display_name it_display_name, m.display_name m_display_name, l.code lang_code, u.skin_id " +
                	"FROM investments i, show_off so, users u, investment_types it, opportunities op, markets m, skins s, languages l " +
                	"WHERE i.id=so.investment_id AND i.user_id = u.id AND i.type_id=it.id AND " +
                		"i.opportunity_id=op.id AND op.market_id=m.id AND u.skin_id=s.id AND " +
                		"s.default_language_id=l.id AND i.id=? " +
                	"ORDER BY so.id DESC";
            ps = conn.prepareStatement(sql);
            ps.setLong(1, invId);
            rs = ps.executeQuery();
            if (rs.next()) {
            	com.anyoption.common.beans.Investment i = getVO(rs);
            	i.setCountryId(rs.getLong("country_id"));
                i.setShowOffName(CommonUtil.capitalizeFirstLetters
                		(rs.getString("first_name") + " " + rs.getString("last_name").substring(0, 1)));
            	i.setCurrencyId(rs.getLong("currency_id"));
            	i.setTypeName(CommonUtil.getMessage(rs.getString("it_display_name"), null));
            	Locale locale = new Locale(rs.getString("lang_code"));
            	i.setMarketName(MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, rs.getLong("market_id")));
            	i.setSkin(String.valueOf(rs.getLong("skin_id")));
            	String[] tempStringArr = CommonUtil.getDateAndTimeShowOffAsArray(i.getTimeCreated(),i.getUtcOffsetCreated());
            	//tempstringArr[0] = date ,tempstringArr[2] = time
            	((Investment)i).setShowOffText(CommonUtil.getMessage("showOff.sentence." + String.valueOf(rs.getLong("show_off_number")),
				new String[] {
					CommonUtil.removeFractionFromAmount(CommonUtil.displayAmount(i.getAmount(), i.getCurrencyId())),
					i.getTypeName(),
					i.getMarketName(),
					tempStringArr[0],
					CommonUtil.removeFractionFromAmount(InvestmentFormatter.getRefundTxt(i)),
					String.valueOf(CommonUtil.getDateDistance(i.getTimeCreated(), i.getTimeSettled(), locale)),
					tempStringArr[2]},
					locale));

                inv = i;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return inv;
	}

	public static com.anyoption.common.beans.Investment getShowOffInvestmentByShowOffIdForFacebook(Connection conn, long showOffId , boolean facebook) throws SQLException {
		com.anyoption.common.beans.Investment inv = null;
       PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT  i.*, so.show_off_number, u.id user_id, u.first_name, u.last_name, " +
                    	"u.country_id, u.currency_id, it.display_name it_display_name, m.display_name m_display_name, l.code lang_code, u.skin_id " +
                	"FROM investments i, show_off so, users u, investment_types it, opportunities op, markets m, skins s, languages l " +
                	"WHERE i.id=so.investment_id AND i.user_id = u.id AND i.type_id=it.id AND " +
                		"i.opportunity_id=op.id AND op.market_id=m.id AND u.skin_id=s.id AND " +
                		"s.default_language_id=l.id AND so.id=? " +
                	"ORDER BY so.id DESC";
            ps = conn.prepareStatement(sql);
            ps.setLong(1, showOffId);
            rs = ps.executeQuery();
            if (rs.next()) {
            	com.anyoption.common.beans.Investment i = getVO(rs);
            	i.setCountryId(rs.getLong("country_id"));
                i.setShowOffName(CommonUtil.capitalizeFirstLetters
                		(rs.getString("first_name") + " " + rs.getString("last_name").substring(0, 1)));
            	i.setCurrencyId(rs.getLong("currency_id"));
            	i.setTypeName(CommonUtil.getMessage(rs.getString("it_display_name"), null));
            	Locale locale = new Locale(rs.getString("lang_code"));
            	i.setMarketName(MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, rs.getLong("market_id")));
            	i.setSkin(String.valueOf(rs.getLong("skin_id")));
            	String[] tempStringArr = CommonUtil.getDateAndTimeShowOffAsArray(i.getTimeCreated(),i.getUtcOffsetCreated());
            	//tempstringArr[0] = date ,tempstringArr[2] = time
            	i.setShowOffFacebookText(CommonUtil.getMessage("showOff.twitter.sentence." + String.valueOf(rs.getLong("show_off_number")),
				new String[] {
					CommonUtil.removeFractionFromAmount(CommonUtil.displayAmount(i.getAmount(), i.getCurrencyId())),
					i.getTypeName(),
					i.getMarketName(),
					tempStringArr[0],
					CommonUtil.removeFractionFromAmount(InvestmentFormatter.getRefundTxt(i)),
					String.valueOf(CommonUtil.getDateDistance(i.getTimeCreated(), i.getTimeSettled(), locale)),
					tempStringArr[2]},
					locale));

                inv = i;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return inv;
	}

    public static boolean isGMBannerViewed(Connection conn, long userId) throws SQLException {
        boolean isViewed = false;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "1 " +
                "FROM " +
                    "golden_minutes_viewing gmv " +
                "WHERE " +
                    "gmv.type_id = 1 AND " +
                    "gmv.user_id = ? AND " +
                    "gmv.time_created >= sysdate - ((SELECT value FROM enumerators WHERE enumerator LIKE 'insurance_period_start_time' ) - (SELECT value FROM enumerators WHERE enumerator LIKE 'insurance_period_end_time'))/1440 ";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);

            rs = pstmt.executeQuery();
            if (rs.next()) {
                isViewed = true;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return isViewed;
    }

    /**
     * insert row that mark that this user saw the banner of golden minutes
     * @param conn
     * @param id user id
     * @return true if insert was good
     * @throws SQLException
     */
    public static boolean insertGMBannerViewed(Connection conn, long id) throws SQLException {
        boolean isUpdate = false;
        PreparedStatement pstmt = null;
        try {
            String sql =
                "INSERT INTO golden_minutes_viewing " +
                    "( " +
                        "id, " +
                        "user_id, " +
                        "type_id, " +
                        "time_created " +
                    ") " +
                "VALUES " +
                    "( " +
                        "seq_golden_minutes_viewing.NEXTVAL, " +
                        "?, " +
                        "1, " +
                        "sysdate " +
                    ") ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, id);
            pstmt.executeUpdate();
            isUpdate = true;
        } finally {
            closeStatement(pstmt);
        }
        return isUpdate;
    }


    public static ArrayList<Investment> getLastUserInvestments(Connection conn, long userId, long skinId) throws SQLException {
        ArrayList<Investment> l = new ArrayList<Investment>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<Long> temp = new ArrayList<Long>();
        try {
            String sql =
                    "SELECT " +
	                    "M.display_name, " +
	                    "M.id as marketId " +
                    "FROM " +
                        "investments I, " +
                        "opportunities O, " +
                        "markets M, " +
                        "opportunity_types OT, " +
                        "users U " +
                    "WHERE " +
                        "I.opportunity_id = O.id AND " +
                        "O.market_id = M.id AND " +
                        "I.user_id = ? AND " +
                        "U.id = I.user_id AND " +
                        "O.opportunity_type_id = OT.id AND " +
                        "OT.id = 1 " +
                    "ORDER BY " +
                        "I.time_created desc ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            int count = 0;
            while (rs.next() && count < 3) {
                Investment i = new Investment();
                i.setMarketName(MarketsManagerBase.getMarketName(skinId, rs.getLong("marketId")));
                i.setMarketId(rs.getLong("marketId"));
                if (!temp.contains(i.getMarketId())) {
                	l.add(i);
                	temp.add(i.getMarketId());
                	count++;
                }
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
    }

   /* public static Investment getShowOffInvestmentsHP(Connection conn, boolean showOffLocale, String langCode, long skinId, boolean shortText) throws SQLException {
		Investment l = new Investment();
        //Statement st = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
            		"SELECT * " +
            		"FROM " +
		                "(SELECT  " +
		                	"i.*, " +
		                	"so.show_off_number, " +
		                	"to_char(so.time_created, 'DD.MM.YYYY HH24:MI:SS') show_off_time, " +
		                	"u.first_name, " +
		                	"u.last_name, " +
		                	"u.country_id, " +
		                	"u.currency_id, " +
		                	"it.display_name it_display_name, " +
		                	"m.display_name m_display_name, " +
		                	"l.code lang_code, " +
		                	"u.skin_id " +
		                "FROM " +
		                	"investments i, " +
		                	"show_off so, " +
		                	"users u, " +
		                	"investment_types it, " +
		                	"opportunities op, " +
		                	"markets m, " +
		                	"skins s, " +
		                	"languages l " +
		                "WHERE " +
		                	"i.id = so.investment_id AND " +
		                	"i.user_id = u.id AND " +
		                	"i.type_id = it.id AND " +
		                	"i.opportunity_id = op.id AND " +
		                	"op.market_id = m.id AND " +
		                	"u.skin_id = s.id AND " +
		                	"s.default_language_id = l.id AND " +
		                	"u.skin_id = ? " +
		                	"order by so.time_created desc) " +
                	"WHERE rownum = 1";


			ApplicationData ap = (ApplicationData) FacesContext.getCurrentInstance().getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(FacesContext.getCurrentInstance());
			if(ap.getSkinId() == Constants.SKIN_EN_US ||
					ap.getSkinId() == Constants.SKIN_ES_US) {
				sql += " AND m.market_group_id <> " + Constants.MARKET_GROUP_COMMODITIES +
			 	      " AND m.market_group_id <> " + Constants.MARKET_GROUP_CURRENCIES;
			}

			pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, skinId);
	        rs = pstmt.executeQuery(sql);

        	l = getVO(rs);
        	l.setCountryId(rs.getLong("country_id"));
            l.setShowOffName(CommonUtil.capitalizeFirstLetters
            		(rs.getString("first_name") + " " + rs.getString("last_name").substring(0, 1)));
        	l.setCurrencyId(rs.getLong("currency_id"));
        	l.setTypeName(CommonUtil.getMessage(rs.getString("it_display_name"), null));
        	Locale locale = null;
        	if (showOffLocale) {
        		locale = new Locale(rs.getString("lang_code"));
        		l.setMarketName(CommonUtil.getMessage(rs.getString("m_display_name"), null, locale));
        		l.setSkin(String.valueOf(rs.getLong("skin_id")));
           		l.setSkin(String.valueOf(rs.getLong("skin_id")));
        	} else if (null != langCode) {
        		try {
        			locale = new Locale(langCode);
        		} catch (Exception e) {
					// do nothing!
				}
        	}
        	l.setMarketName(CommonUtil.getMessage(rs.getString("m_display_name"), null));
        	String dateDistance = null;
        	if (null == locale) {
        		dateDistance = CommonUtil.getDateDistance(l.getTimeCreated(), l.getTimeSettled(), null);
        	} else {
        		dateDistance = CommonUtil.getDateDistance(l.getTimeCreated(), l.getTimeSettled(), locale);
        	}
        	String[] tempStringArr = CommonUtil.getDateAndTimeShowOffAsArray(l.getTimeCreated(),l.getUtcOffsetCreated());
        	//tempstringArr[0] = date ,tempstringArr[2] = time
        	String[] strArr = new String[] {
        			CommonUtil.removeFractionFromAmount(l.getAmountTxt()),
        			l.getTypeName(),
        			l.getMarketName(),
        			tempStringArr[0],
        			CommonUtil.removeFractionFromAmount(l.getRefundTxt()),
        			dateDistance,
        			tempStringArr[2]};
        	String showOffMessegeKey = null;
        	if (shortText) {
        		showOffMessegeKey = "showOff.twitter.sentence.";
        	} else {
        		showOffMessegeKey = "showOff.sentence.";
        	}
        	String showOffText;
        	if (null == locale) {
        		showOffText = CommonUtil.getMessage(showOffMessegeKey + String.valueOf(rs.getLong("show_off_number")), strArr);
        	} else {
        		showOffText = CommonUtil.getMessage(showOffMessegeKey + String.valueOf(rs.getLong("show_off_number")), strArr, locale);
        	}
        	l.setShowOffText(showOffText);
        	l.setShowOffTime(rs.getString("show_off_time"));

        } catch (ParseException e) {
			throw new SQLException(e.getMessage());
		} finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
	}*/

    public static void updateTimeSettled(Connection conn, Date now, String ids) throws SQLException {
		PreparedStatement ps = null;
    	try {
    		String sql =
    			"UPDATE " +
    				"investments " +
    			"SET " +
    				"time_settled = ?, " +
    				"time_settled_year = EXTRACT(YEAR FROM GET_TIME_BY_PERIOD(?)), " +
    				"time_settled_month = EXTRACT(MONTH FROM GET_TIME_BY_PERIOD(?)) " +
    			"WHERE " +
    				"id in ( " + ids + " )";
    		ps = conn.prepareStatement(sql);
    		Timestamp t = CommonUtil.convertToTimeStamp(now);
    		ps.setTimestamp(1, t);
    		ps.setTimestamp(2, t);
    		ps.setTimestamp(3, t);
//    		ps.setString(4, ids);
    		ps.executeUpdate();
    	} finally {
    		closeStatement(ps);
    	}
	}

	/**
     * Load todays opened investments for specified user. Only the fields needed for the trading page
     * right side history are loaded (market name, investment type, current level, est close time, market_id).
     *
     * @param conn
     * @param userId
     * @return <code>ArrayList<Investment></code>
     * @throws SQLException
     */
    public static ArrayList<com.anyoption.common.beans.Investment> getAllBinary0100InvestmentsByOppId(Connection conn, long userId, long oppId) throws SQLException {
        ArrayList<com.anyoption.common.beans.Investment> l = new ArrayList<com.anyoption.common.beans.Investment>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                    "	o.id as opp_Id, " +
					"	i.amount, " +
					"   i.current_level, " +
					"	i.time_created, " +
					"	i.type_id, " +
					"	i.option_plus_fee, " +
					"	count(*) as count, " +
					"	o.opportunity_type_id as opp_type_id, " +
					"	(SELECT " +
							"sum(i2.amount-i2.option_plus_fee) " +
						"FROM " +
							"investments i2 " +
						"WHERE " +
							"i2.user_id = ? AND " +
							"i2.opportunity_id = ? AND " +
							"i2.is_settled = 0 AND " +
							"i2.is_canceled = 0 AND " +
							"i2.is_void = 0) as totalamount " +
					"FROM " +
					"	investments i, " +
					"	opportunities o " +
					"WHERE " +
					"	i.user_id = ? AND " +
					"	i.opportunity_id = ? AND " +
					"	i.opportunity_id = o.id AND " +
					"	i.is_settled = 0 AND " +
					"	i.is_canceled = 0 AND " +
					"	i.is_void = 0 " +
					"GROUP BY " +
					"	i.amount, " +
					"   i.current_level, " +
					"	i.time_created, " +
					"	i.type_id, " +
					"	i.option_plus_fee, " +
					"	o.opportunity_type_id, " +
					"	i.group_inv_id," +
					"	o.id " +
					"ORDER BY " +
					"	i.time_created desc ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setLong(2, oppId);
            pstmt.setLong(3, userId);
            pstmt.setLong(4, oppId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Investment i = new Investment();
                i.setAmount(rs.getLong("amount"));
                i.setCurrentLevel(rs.getDouble("current_level"));
                i.setTypeId(rs.getLong("type_id"));
                i.setOptionPlusFee(rs.getLong("option_plus_fee"));
                i.setSelectedAndInvestmentsCount(rs.getLong("count") * InvestmentsManagerBase.getBinary0100NumberOfContracts(i));
                i.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
                i.setSumInvest(rs.getLong("totalamount")); //i have to put it somewhere :)
                i.setOpportunityTypeId(rs.getLong("opp_type_id"));
                i.setOpportunityId(rs.getLong("opp_Id"));
                l.add(i);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
    }

	/**
     * List all the investments done on opportunity that are open and are not
     * settled, canceled or void.
     *
     * @param conn the db connection to use
     * @param oppId the id of the opp.
     * @param user id the user id to bring his investments
     * @param invTypeId the type of the investments not to bring
     * @param time created if its not null get only investment from this date and type
     * @return
     * @throws SQLException
     */
    public static ArrayList<com.anyoption.common.beans.Investment> getOpenBinary0100Investments(Connection conn, long userId, long oppId, long invTypeId, Date timeCreated) throws SQLException {
        ArrayList<com.anyoption.common.beans.Investment> list =  new ArrayList<com.anyoption.common.beans.Investment>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "A.*, " +
                        "B.closing_level, " +
                        "M.id as market_id, " +
                        "M.insurance_premia_percent, " +
                        "M.roll_up_premia_percent, " +
                        "M.display_name m_display_name, " +
                        "M.decimal_point m_decimal_point, " +
                        "it.display_name i_type_name, " +
                        "to_char(B.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, " +
                        "bu.odds_win as bonus_odds_win, " +
                        "bu.odds_lose as bonus_odds_lose, " +
                        "b.opportunity_type_id, " +
                        "b.current_level as opp_current_level " +
                    "FROM " +
                        "investments A left join bonus_users bu on A.bonus_user_id = bu.id, " +
                        "opportunities B, " +
                        "markets M, " +
                        "investment_types it " +
                    "WHERE " +
                        "A.opportunity_id = B.id AND "+
                        "M.id = B.market_id AND " +
                        "A.type_id = it.id AND " +
            			"B.time_act_closing IS NULL AND " +
                        "A.is_settled = 0 AND " +
                        "A.is_canceled = 0 AND " +
                        "A.is_void = 0 AND " +
                        "B.id = ? AND " +
                        "A.user_id = ? AND ";
             if (null != timeCreated) {
                 sql += "A.type_id = ? AND " +
                      	"A.time_created = ? ";
             } else {
                 sql += "A.type_id != ? ";
             }
                 sql += "ORDER BY " +
                    	"A.time_created";
            pstmt = conn.prepareStatement(sql);
            int index = 1;
            pstmt.setLong(index++, oppId);
            pstmt.setLong(index++, userId);
            pstmt.setLong(index++, invTypeId);
            if (null != timeCreated) {
            	pstmt.setTimestamp(index++, CommonUtil.convertToTimeStamp(timeCreated));
            }
            rs = pstmt.executeQuery();
            while(rs.next()) {
            	com.anyoption.common.beans.Investment i = getVO(rs);
                i.setClosingLevel(rs.getDouble("closing_level"));
                i.setBonusUserId(rs.getLong("bonus_user_id"));
                i.setMarketId(rs.getLong("market_id"));
                i.setInsurancePremiaPercent(rs.getDouble("insurance_premia_percent"));
                i.setRollUpPremiaPercent(rs.getDouble("roll_up_premia_percent"));
                i.setMarketName(rs.getString("m_display_name"));
                i.setDecimalPoint(rs.getLong("m_decimal_point"));
                i.setTypeName(rs.getString("i_type_name"));
                i.setBonusLoseOdds(rs.getDouble("bonus_odds_lose"));
                i.setBonusWinOdds(rs.getDouble("bonus_odds_win"));
                SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
                try {
					i.setTimeEstClosing(localDateFormat.parse(rs.getString("time_est_closing")));
				} catch (ParseException e) {
					log.error("Problem parsing timeEstClosing! " + e);
				}
				i.setOpportunityTypeId(rs.getLong("opportunity_type_id"));
				i.setOppCurrentLevel(rs.getDouble("opp_current_level"));
				list.add(i);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }

   /* public static Investment getShowOffInvestmentsHP(Connection conn, boolean showOffLocale, String langCode, long skinId, boolean shortText) throws SQLException {
		Investment l = new Investment();
        //Statement st = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
            		"SELECT * " +
            		"FROM " +
		                "(SELECT  " +
		                	"i.*, " +
		                	"so.show_off_number, " +
		                	"to_char(so.time_created, 'DD.MM.YYYY HH24:MI:SS') show_off_time, " +
		                	"u.first_name, " +
		                	"u.last_name, " +
		                	"u.country_id, " +
		                	"u.currency_id, " +
		                	"it.display_name it_display_name, " +
		                	"m.display_name m_display_name, " +
		                	"l.code lang_code, " +
		                	"u.skin_id " +
		                "FROM " +
		                	"investments i, " +
		                	"show_off so, " +
		                	"users u, " +
		                	"investment_types it, " +
		                	"opportunities op, " +
		                	"markets m, " +
		                	"skins s, " +
		                	"languages l " +
		                "WHERE " +
		                	"i.id = so.investment_id AND " +
		                	"i.user_id = u.id AND " +
		                	"i.type_id = it.id AND " +
		                	"i.opportunity_id = op.id AND " +
		                	"op.market_id = m.id AND " +
		                	"u.skin_id = s.id AND " +
		                	"s.default_language_id = l.id AND " +
		                	"u.skin_id = ? " +
		                	"order by so.time_created desc) " +
                	"WHERE rownum = 1";


			ApplicationData ap = (ApplicationData) FacesContext.getCurrentInstance().getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(FacesContext.getCurrentInstance());
			if(ap.getSkinId() == Constants.SKIN_EN_US ||
					ap.getSkinId() == Constants.SKIN_ES_US) {
				sql += " AND m.market_group_id <> " + Constants.MARKET_GROUP_COMMODITIES +
			 	      " AND m.market_group_id <> " + Constants.MARKET_GROUP_CURRENCIES;
			}

			pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, skinId);
	        rs = pstmt.executeQuery(sql);

        	l = getVO(rs);
        	l.setCountryId(rs.getLong("country_id"));
            l.setShowOffName(CommonUtil.capitalizeFirstLetters
            		(rs.getString("first_name") + " " + rs.getString("last_name").substring(0, 1)));
        	l.setCurrencyId(rs.getLong("currency_id"));
        	l.setTypeName(CommonUtil.getMessage(rs.getString("it_display_name"), null));
        	Locale locale = null;
        	if (showOffLocale) {
        		locale = new Locale(rs.getString("lang_code"));
        		l.setMarketName(CommonUtil.getMessage(rs.getString("m_display_name"), null, locale));
        		l.setSkin(String.valueOf(rs.getLong("skin_id")));
           		l.setSkin(String.valueOf(rs.getLong("skin_id")));
        	} else if (null != langCode) {
        		try {
        			locale = new Locale(langCode);
        		} catch (Exception e) {
					// do nothing!
				}
        	}
        	l.setMarketName(CommonUtil.getMessage(rs.getString("m_display_name"), null));
        	String dateDistance = null;
        	if (null == locale) {
        		dateDistance = CommonUtil.getDateDistance(l.getTimeCreated(), l.getTimeSettled(), null);
        	} else {
        		dateDistance = CommonUtil.getDateDistance(l.getTimeCreated(), l.getTimeSettled(), locale);
        	}
        	String[] tempStringArr = CommonUtil.getDateAndTimeShowOffAsArray(l.getTimeCreated(),l.getUtcOffsetCreated());
        	//tempstringArr[0] = date ,tempstringArr[2] = time
        	String[] strArr = new String[] {
        			CommonUtil.removeFractionFromAmount(l.getAmountTxt()),
        			l.getTypeName(),
        			l.getMarketName(),
        			tempStringArr[0],
        			CommonUtil.removeFractionFromAmount(l.getRefundTxt()),
        			dateDistance,
        			tempStringArr[2]};
        	String showOffMessegeKey = null;
        	if (shortText) {
        		showOffMessegeKey = "showOff.twitter.sentence.";
        	} else {
        		showOffMessegeKey = "showOff.sentence.";
        	}
        	String showOffText;
        	if (null == locale) {
        		showOffText = CommonUtil.getMessage(showOffMessegeKey + String.valueOf(rs.getLong("show_off_number")), strArr);
        	} else {
        		showOffText = CommonUtil.getMessage(showOffMessegeKey + String.valueOf(rs.getLong("show_off_number")), strArr, locale);
        	}
        	l.setShowOffText(showOffText);
        	l.setShowOffTime(rs.getString("show_off_time"));

        } catch (ParseException e) {
			throw new SQLException(e.getMessage());
		} finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
	}*/

    /**
     * Load todays opened investments for specified user. Only the fields needed for the trading page
     * right side history are loaded (market name, investment type, current level, est close time, market_id).
     *
     * @param conn
     * @param userId
     * @return <code>ArrayList<Investment></code>
     * @throws SQLException
     * @throws ParseException
     */
    public static ArrayList<Investment> getAllBinary0100InvestmentsByUserId(Connection conn, long userId, boolean isSettled, long marketId, Date day, String hour, long skinId) throws SQLException, ParseException {
        ArrayList<Investment> l = new ArrayList<Investment>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
		            	"SELECT " +
							"m.display_name, " +
							" max(i.id) as investment_id, " +
						    "o.current_level as opp_current_level, " +
						    "to_char(SYS_EXTRACT_UTC(o.time_est_closing), 'YYYY-MM-DD HH24:MI:SS.FF3') AS time_est_closing, " +
						    "o.opportunity_type_id, " +
						    "i.current_level, " +
						    "i.time_created, " +
						    "i.type_id, " +
						    "i.option_plus_fee, " +
							"i.settled_level, " +
						    "i.time_settled, " +
							"count(*) as count, " +
							"i.win, " +
						    "i.lose, " +
							"i.amount, " +
							"i.is_settled, " +
							"m.decimal_point as m_decimal_point, " +
							"u.currency_id, " +
							"CASE WHEN SYS_EXTRACT_UTC(o.time_est_closing) > i.time_settled THEN i.time_settled ELSE SYS_EXTRACT_UTC(o.time_est_closing) END AS time_settled_order, " +
							"i.opportunity_id, " +
							"o.market_id " +
						"FROM " +
							"investments i, " +
						    "opportunities o, " +
						    "markets m, " +
						    "users u, " +
						    "skin_market_groups smg, " +
		            		"skin_market_group_markets smgm " +
						"WHERE " +
							"smg.skin_id = ? AND " +
							"smg.id = smgm.skin_market_group_id AND " +
            				"smgm.market_id = o.market_id AND " +
							"i.user_id = ? AND " +
							"i.is_settled = ? AND " +
							"i.time_created between to_date(?,'ddMMyyyy') and to_date(?,'ddMMyyyy') + 86339/86400 AND " +
							"i.is_canceled = 0 AND " +
							"i.is_void = 0 AND " +
						    "i.type_id in (" + InvestmentType.BUY + ", " + InvestmentType.SELL + ") AND " +
						    "o.id = i.opportunity_id AND " +
						    "m.id = o.market_id AND " +
						    "u.id = i.user_id ";
            	//all and assets
	            if (marketId != 1 && marketId != 0) {
	            	sql += " AND m.id = ? ";
	            }

				if (!CommonUtil.isParameterEmptyOrNull(hour)){
					sql += "AND to_char(SYS_EXTRACT_UTC(o.time_est_closing), 'HH24:MI') = ? ";
				}
				sql +=	"GROUP BY " +
							"m.display_name, " +
						    "o.current_level, " +
						    "o.time_est_closing, " +
						    "o.opportunity_type_id, " +
						    "i.current_level, " +
						    "i.time_created, " +
						    "i.type_id, " +
						    "i.option_plus_fee, " +
							"i.settled_level, " +
						    "i.time_settled, " +
							"i.amount, " +
							"i.is_settled, " +
							"u.currency_id, " +
							"i.win, " +
							"i.lose, " +
							"m.decimal_point, " +
							"i.group_inv_id, " +
							"smgm.home_page_priority, " +
							"i.opportunity_id, " +
							"o.market_id " +
						"ORDER BY ";
						if (isSettled) {
							sql += "time_settled_order desc, ";
						} else {
							sql += "i.time_created desc, ";
						}
						sql += "smgm.home_page_priority";
            pstmt = conn.prepareStatement(sql);
            int ind = 1;
            pstmt.setLong(ind++, skinId);
            pstmt.setLong(ind++, userId);
            pstmt.setLong(ind++, isSettled ? 1 : 0);
            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");

            //The form is in open investments = checked.
            if (day == null) {
            	Date date = new Date();
            	pstmt.setString(ind++, sdf.format(date.getTime()));
            	pstmt.setString(ind++, sdf.format(date.getTime()));
            } else {
                pstmt.setString(ind++, sdf.format(day));
                pstmt.setString(ind++, sdf.format(day));
            }
          //all and assets
            if (marketId != 1 && marketId != 0) {
            	pstmt.setLong(ind++, marketId);
            }
            if (!CommonUtil.isParameterEmptyOrNull(hour)) {
            	pstmt.setString(ind++, hour);
            }
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Investment i = new Investment();
                i.setId(rs.getLong("investment_id"));
                i.setMarketName(MarketsManagerBase.getMarketName(skinId, rs.getLong("market_id")));
                i.setOppCurrentLevel(rs.getDouble("opp_current_level"));
                i.setTimeEstClosing(CommonUtil.getDateTimeFormaByTz(CommonUtil.getTimeWithoutTimezone(rs.getString("time_est_closing")), CommonUtil.getUtcOffset()));
                i.setOpportunityTypeId(rs.getLong("opportunity_type_id"));
                i.setCurrentLevel(rs.getDouble("current_level"));
                i.setTimeCreated(CommonUtil.getDateTimeFormaByTz(rs.getTimestamp("time_created"), CommonUtil.getUtcOffset()));
                i.setTypeId(rs.getLong("type_id"));
                i.setOptionPlusFee(rs.getLong("option_plus_fee"));
                i.setClosingLevel(rs.getDouble("settled_level"));
                if (isSettled && rs.getTimestamp("time_settled") != null) {
                	i.setTimeSettled(CommonUtil.getDateTimeFormaByTz(rs.getTimestamp("time_settled"), CommonUtil.getUtcOffset()));
                }
                i.setWin(rs.getLong("win"));
                i.setLose(rs.getLong("lose"));
                i.setAmount(rs.getLong("amount"));
                i.setInvestmentsCount(rs.getLong("count") * InvestmentsManagerBase.getBinary0100NumberOfContracts(i));
                i.setIsSettled(rs.getInt("is_settled"));
                i.setCurrencyId(rs.getLong("currency_id"));
                i.setDecimalPoint(rs.getLong("m_decimal_point"));
                i.setMarketId(rs.getLong("market_id"));
                i.setOpportunityId(rs.getLong("opportunity_id"));
                l.add(i);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
    }

    /**
     * The function return the name's and the id's of the markets binary0100 in which the user made invest on.
     * By his skin and his userID
     *
     * @param conn
     * @param userId
     * @param skinId
     * @return
     * @throws SQLException
     */
    public static ArrayList<SelectItem> getMarketsBinary0100UserInvestments(Connection conn, long userId, long skinId)
			throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();

		try {
			String sql = 	"SELECT " +
								"m.id as marketId, " +
								"m.display_name " +
		  			   		"FROM " +
		  			   			"markets m, " +
			  			   		"skin_market_groups smg, " +
			  			   		"skin_market_group_markets smgm, " +
			  			   		"investments i, " +
			  			   		"opportunities o " +
		  			   		"WHERE " +
		  			   			"i.is_settled = 1 and " +
		  			   			"smg.skin_id = ? and " +
		  			   			"smgm.market_id = m.id and " +
		  			   			"smg.id = smgm.skin_market_group_id and " +
		  			   			"i.opportunity_id = o.id and " +
		  			   			"o.market_id = m.id and " +
		  			   			"i.user_id = ? and " +
		  			   			"feed_name like '%Binary0-100' " +
	  			   			"GROUP BY " +
		  			   			"m.id, m.display_name";
			ps = conn.prepareStatement(sql);
			ps.setLong(1, skinId);
			ps.setLong(2, userId);
			rs = ps.executeQuery();
			while(rs.next()) {
				SelectItem ti = new SelectItem();
				ti.setLabel(rs.getString("display_name"));
				ti.setValue(rs.getLong("marketId"));
				list.add(ti);
			  }
		  }finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		return list;
	}

    /**
     * The function return the date of the user last invest.
     * If `isBinary0100 = true` then, return the last time invest date from binary0100 markets.
     *
     * @param conn
     * @param userId
     * @param skinId
     * @param isBinary0100
     * @return
     * @throws SQLException
     */
    public static Date getLastUserInvestmentDate(Connection conn, long userId, long skinId, boolean isBinary0100) throws SQLException {
    	PreparedStatement ps = null;
		ResultSet rs = null;
		Date userLastInvest = null;
		try {
			String sql = 	"SELECT " +
								"m.id, " +
								"m.display_name, " +
								"i.time_created " +
		  			   		"FROM " +
		  			   			"markets m, " +
			  			   		"skin_market_groups smg, " +
			  			   		"skin_market_group_markets smgm, " +
			  			   		"investments i, " +
			  			   		"opportunities o " +
		  			   		"WHERE " +
		  			   			"i.is_settled = 1 and " +
		  			   			"smg.skin_id = ? and " +
		  			   			"smgm.market_id = m.id and " +
		  			   			"smg.id = smgm.skin_market_group_id and " +
		  			   			"i.opportunity_id = o.id and " +
		  			   			"o.market_id = m.id and " +
		  			   			"i.user_id = ? ";
				if  (isBinary0100) {
					sql += " and feed_name like '%Binary0-100' ";
				}
					sql += " ORDER BY " +
								"i.time_created desc ";

			ps = conn.prepareStatement(sql);
			ps.setLong(1, skinId);
			ps.setLong(2, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				userLastInvest = convertToDate(rs.getTimestamp("time_created"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return userLastInvest;
	}

	/**
	 * This method return an array with all the expiry hours of the user investments by specific market and date.
	 *
	 * @param conn
	 * @param userId
	 * @param skinId
	 * @param marketId
	 * @param day
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	public static ArrayList<SelectItem> getExpiryHourByUserInvestments(Connection conn, long userId, long skinId, long marketId, Date day) throws SQLException, ParseException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();

		try {
			String sql = 	"SELECT " +
								"m.id as marketId," +
								"m.display_name, " +
								"to_char(o.time_est_closing, 'hh24:mi') as time_est_closing " +
		  			   		"FROM " +
		  			   			"markets m, " +
			  			   		"investments i, " +
			  			   		"opportunities o " +
		  			   		"WHERE " +
		  			   			"m.id = ? and	" +
		  			   			"i.opportunity_id = o.id and " +
		  			   			"o.market_id = m.id and " +
		  			   			"i.user_id = ? and " +
		  			   			"feed_name like '%Binary0-100' and " +
		  			   			" i.time_created between to_date(?,'ddMMyyyy') and to_date(?,'ddMMyyyy') + 86339/86400 " +
	  			   			"GROUP BY " +
		  			   			"m.id, " +
		  			   			"m.display_name, " +
		  			   			"o.time_est_closing " +
	  			   			"ORDER BY " +
	  			   				"o.time_est_closing ";
			ps = conn.prepareStatement(sql);
			ps.setLong(1, marketId);
			ps.setLong(2, userId);
            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
            ps.setString(3, sdf.format(day));
            ps.setString(4, sdf.format(day));
			rs = ps.executeQuery();
			while(rs.next()) {
				SelectItem ti = new SelectItem();
				ti.setLabel(rs.getString("time_est_closing"));
				ti.setValue(rs.getString("time_est_closing"));
				list.add(ti);
			  }
		  }finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		return list;
	}

	/**
	 * This function return the time estimate close Hour of the market .
	 *
	 * @param conn
	 * @param skinId
	 * @param currentMarketId
	 * @return
	 */
	public static String marketCurrentDisplayCloseHour(Connection conn, long userId, long skinId, long marketId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String userLastInvest = null;
		try {
			String sql =	"SELECT " +
								"m.id, " +
								"m.display_name, " +
								"ot.time_est_closing " +
							"FROM " +
								"markets m,  "+
								"skin_market_groups smg, " +
								"skin_market_group_markets smgm, "+
								"investments i, " +
								"opportunities o, " +
								"opportunity_templates ot " +
							"WHERE " +
								"m.id = ? and " +
						        "ot.market_id = m.id and " +
						        "ot.time_est_closing >= TO_CHAR(sysdate, 'hh24:mm') and " +
						        "smg.skin_id = ? and " +
						   		"smgm.market_id = m.id and " +
						   		"smg.id = smgm.skin_market_group_id and " +
						   		"i.opportunity_id = o.id and  " +
						   		"o.market_id = m.id and " +
						   		"i.user_id = ? and " +
						   		"feed_name like '%Binary0-100' " +
						   	"GROUP BY " +
						   		"m.id, " +
						   		"m.display_name, " +
						   		"ot.time_est_closing " +
						   	"ORDER BY " +
						   		"ot.time_est_closing ";

			ps = conn.prepareStatement(sql);
			ps.setLong(1, marketId);
			ps.setLong(2, skinId);
			ps.setLong(3, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				userLastInvest = rs.getString("time_est_closing");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return userLastInvest;
	}

	/**
     * Get last top trades for LIVE page (last day/week/month)
     * @param con
     * @param skinBusinessCaseId
     * @return
     * @throws SQLException
     */
    public static HashMap<Long, ArrayList<com.anyoption.common.beans.Investment>> getTopTrades(Connection con, String fromDate, long skinBusinessCaseId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<com.anyoption.common.beans.Investment> lastDayTopTrades = new ArrayList<com.anyoption.common.beans.Investment>();
		ArrayList<com.anyoption.common.beans.Investment> lastWeekTopTrades = new ArrayList<com.anyoption.common.beans.Investment>();
		ArrayList<com.anyoption.common.beans.Investment> lastMonthTopTrades = new ArrayList<com.anyoption.common.beans.Investment>();
		HashMap<Long, ArrayList<com.anyoption.common.beans.Investment>> topTrades = new HashMap<Long, ArrayList<com.anyoption.common.beans.Investment>>();
		Investment inv;
		try {
			String sql = "SELECT " +
							" t.*, " +
                            " o.market_id " +
						 " FROM " +
						 	" top_trades t, " +
                            " opportunities o " +
						 " WHERE " +
                            " o.id = t.opportunity_id " +
						 	" AND t.skin_business_case_id = ? " +
						 	" AND t.time_created > to_date('" + fromDate + "','DD/MM/YYYY') " +
						 	" AND rownum <= ? " +
						 " ORDER BY t.profit_user desc";
			ps = con.prepareStatement(sql);
			ps.setLong(1, skinBusinessCaseId);
			ps.setLong(2, Constants.LAST_TOP_TRADES);
			rs = ps.executeQuery();
			while(rs.next()) {
				inv = new Investment();
				int lastDaysType = (int)rs.getLong("last_days");
				inv.setCountryId(rs.getLong("country_id"));
				inv.setCountryName(rs.getString("country_display_name"));
				inv.setTimeEstClosing(convertToDate(rs.getTimestamp("time_est_closing")));
				inv.setNameOpportunityType(rs.getString("opportunity_type_name"));
				inv.setMarketName(MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, rs.getLong("market_id")));
				inv.setClosingLevel(rs.getDouble("closing_level"));
				inv.setProfitUser(rs.getLong("profit_user"));
				inv.setCityName(rs.getString("city_name"));
				inv.setOpportunityId(rs.getLong("opportunity_type_id"));
                inv.setMarketId(rs.getLong("market_id"));
				switch(lastDaysType) {
					case ConstantsBase.TOP_TRADES_TYPE_LAST_DAY:
						lastDayTopTrades.add(inv);
						break;
					case ConstantsBase.TOP_TRADES_TYPE_LAST_WEEK:
						lastWeekTopTrades.add(inv);
						break;
					case ConstantsBase.TOP_TRADES_TYPE_LAST_MONTH:
						lastMonthTopTrades.add(inv);
						break;
					default:
						lastDayTopTrades.add(inv);
						break;
				}
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		if (!lastDayTopTrades.isEmpty() && !lastWeekTopTrades.isEmpty() && !lastMonthTopTrades.isEmpty()) {
			topTrades.put(ConstantsBase.LIVE_AO_TOP_TRADES_LAST_DAY, lastDayTopTrades);
			topTrades.put(ConstantsBase.LIVE_AO_TOP_TRADES_LAST_WEEK, lastWeekTopTrades);
			topTrades.put(ConstantsBase.LIVE_AO_TOP_TRADES_LAST_MONTH, lastMonthTopTrades);
		}

		return topTrades;
	}

	public static boolean isHasOpenedInvByOpp(Connection conn, long userId, long oppId) throws SQLException {
		boolean b = false;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "COUNT(id) AS count " +
                    "FROM " +
                        "investments  " +
                    "WHERE " +
                        "user_id = ? AND " +
                        "opportunity_id = ? AND " +
                        "is_settled = 0"; // only not settled inv
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setLong(2, oppId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                b = rs.getInt("count") > 0;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return b;
	}


    /**
     * Return markets decimal point
     * @param conn
     * @return
     * @throws SQLException
     */
    public static HashMap<Long, String> getMarketsDecimalPoint(Connection conn) throws SQLException{
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	HashMap<Long, String> hm = new HashMap<Long, String>();
    	String sql =" SELECT " +
    				"	* " +
    				" FROM " +
    				"	markets ";
    	try {
	    	ps = conn.prepareStatement(sql);
	    	rs = ps.executeQuery();
	    	while(rs.next()){
	    		hm.put(rs.getLong("id"), rs.getString("decimal_point"));
	    	}
    	} finally {
    		closeResultSet(rs);
    		closeStatement(ps);
    	}
    	return hm;
    }

	public static boolean isHas0100Investments(Connection conn, long userId) throws SQLException {
		boolean b = false;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "COUNT(id) AS count " +
                    "FROM " +
                        "investments  " +
                    "WHERE " +
                        "user_id = ? AND " +
                        "type_id > 3 AND " +
                        "type_id < 6";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                b = rs.getInt("count") > 0;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return b;
	}

	public static void loadOpenPositions(Connection con, OpenPositionsMethodResult result, long userId,
										HashMap<Long, HashMap<Long, MarketNameBySkin>> mnbs, long skinId) throws SQLException {
		String sql = "select o.id as opportunity_id, m.id as market_id, m.display_name, "
						+ "to_char(o.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, o.scheduled, "
						+ "i.id as investment_id, i.amount, i.type_id, i.current_level, i.odds_win, i.odds_lose "
					+ "from opportunities o, markets m, investments i, users u "
					+ "where u.id = i.user_id "
						+ "and i.opportunity_id = o.id "
						+ "and o.market_id = m.id "
						+ "and i.is_settled = 0 "
						+ "and i.is_canceled = 0 "
						+ "and o.opportunity_type_id = " + Opportunity.TYPE_REGULAR + " "
						+ "and u.id = ? "
					+ "order by o.time_est_closing, i.time_created";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, userId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				String marketName = null;
				long mId = rs.getLong("market_id");
				if (mnbs.get(mId) != null && mnbs.get(mId).get(skinId) != null) {
					marketName = mnbs.get(mId).get(skinId).getName();
				}
				result.addOpenPosition(	rs.getLong("opportunity_id"), mId, marketName, getTimeWithTimezone(rs.getString("time_est_closing")),
										rs.getInt("scheduled"), rs.getLong("investment_id"), rs.getLong("amount"), rs.getLong("type_id"),
										rs.getDouble("current_level"), rs.getDouble("odds_win"), rs.getDouble("odds_lose"));
			}
		}
	}
}