package il.co.etrader.web.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.bl_vos.DeviceUniqueIdSkin;

public class DeviceUniqueIdsSkinDAO extends DAOBase {

    public static void update(Connection conn, String duid, long skinId) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                    "UPDATE " +
                        "device_unique_ids_skin " +
                    "SET " +
                        "skin_id = ?, " +
                        "time_modified = sysdate " +
                    "WHERE " +
                        "device_unique_id = ? ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, skinId);
            pstmt.setString(2, duid);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    public static void insert(Connection conn, String duid, long skinId, String userAgent, String ip, long countryId, long combId, String appVer) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                    "INSERT INTO " +
                        "device_unique_ids_skin(id, device_unique_id, skin_id, user_agent, time_created, time_modified, ip, country_id, combination_id, app_version) " +
                    "VALUES(seq_device_unique_ids_skin.nextval, ?, ?, ?, sysdate, sysdate, ?, ?, ?, ?) ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, duid);
            pstmt.setLong(2, skinId);
            pstmt.setString(3, userAgent);
            pstmt.setString(4, ip);
            pstmt.setLong(5, countryId);
            pstmt.setLong(6, combId);
            pstmt.setString(7, appVer);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    public static DeviceUniqueIdSkin getSkinId(Connection con, String duid) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        DeviceUniqueIdSkin record = null;
        try {
            String sql =
            	"SELECT " +
            		"id, " +
            		"skin_id, " +
            		"combination_id, " +
            		"app_version " +
            	"FROM " +
            		"device_unique_ids_skin " +
            	"WHERE " +
            		"device_unique_id = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, duid);
            rs = ps.executeQuery();
            if (rs.next()) {
            	record = new DeviceUniqueIdSkin();
            	record.setId(rs.getLong("id"));
            	record.setSkinId(rs.getLong("skin_id"));
            	record.setCombId(rs.getLong("combination_id"));
            	record.setAppVer(rs.getString("app_version"));
            }
        } finally {
        	closeResultSet(rs);
        	closeStatement(ps);
        }
        return record;
    }

    public static void updateCombinationId(Connection conn, String duid, long combId) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                    "UPDATE " +
                        "device_unique_ids_skin " +
                    "SET " +
                        "combination_id = ?, " +
                        "time_modified = sysdate " +
                    "WHERE " +
                        "device_unique_id = ? ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, combId);
            pstmt.setString(2, duid);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    public static void updateAppVer(Connection conn, String duid, String appVer) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                    "UPDATE " +
                        "device_unique_ids_skin " +
                    "SET " +
                        "app_version = ?, " +
                        "time_modified = sysdate " +
                    "WHERE " +
                        "device_unique_id = ? ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, appVer);
            pstmt.setString(2, duid);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }
}