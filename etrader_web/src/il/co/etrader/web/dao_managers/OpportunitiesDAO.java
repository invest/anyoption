package il.co.etrader.web.dao_managers;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.dao_managers.OpportunitiesDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ComparatorHebrewPriority;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.SelectItemComparatorHebrewPriority;
import il.co.etrader.web.bl_vos.Binary0100EventSelectionBean;
import il.co.etrader.web.bl_vos.LandingMarketDetails;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.TreeItem;
import com.anyoption.common.managers.MarketsManagerBase;


/**
 * Opportunities DAO.
 */
public class OpportunitiesDAO extends OpportunitiesDAOBase {
	protected static final Logger logger = Logger.getLogger(OpportunitiesDAO.class);
	// gets the markets and groups list for the last levels page

	public static HashMap<Long, String> getMarkets(Connection con, Date from, String oppTypeIdStr) throws SQLException, UnsupportedEncodingException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap<Long, String> hm = new HashMap<Long, String>();

		long skinId = CommonUtil.getAppData().getSkinId();
//		Calendar startD = Calendar.getInstance();
//		Calendar endD = Calendar.getInstance();
//		startD.setTime(d);
//		endD.setTime(d);
//		startD.set(Calendar.HOUR_OF_DAY, 0);
//		startD.set(Calendar.MINUTE, 0);
//		startD.set(Calendar.SECOND, 0);
//		endD.set(Calendar.HOUR_OF_DAY, 23);
//		endD.set(Calendar.MINUTE, 59);
//		endD.set(Calendar.SECOND, 59);
		Date to = new Date(from.getTime() + 86400000);

		try {
			String sql =
					" ((SELECT " +
							" smg.market_group_id||','||m.id id," +
							" m.display_name display_name , " +
							" smg.market_group_id market_group, " +
							" m.id market_id " +
					  " FROM " +
							" opportunities o, " +
							" markets m, " +
							" skin_market_group_markets smgm, " +
							" skin_market_groups smg," +
							" opportunity_templates op " +
					  " WHERE " +
					  		" o.market_id = m.id " +
					  		" and o.market_id = op.market_id " + 
					  		" and o.opportunity_type_id = op.opportunity_type_id " +
					  		" and op.is_active = 1" +
					  		" and o.time_act_closing is not null " +
					  		" and smgm.skin_market_group_id = smg.id " +
					        " and m.id = smgm.market_id " +
					        " and smg.skin_id=" + skinId + " " +
					        " and o.market_id in (select smgm.market_id " +
					        					" from skin_market_group_markets smgm,skin_market_groups smg " +
					        					" where smgm.skin_market_group_id = smg.id " +
					        						" and smg.skin_id=" + skinId + ") " +
					        " and o.time_est_closing >= ? " +
					        " and o.time_est_closing < ? " +
					        " and o.opportunity_type_id in (" + oppTypeIdStr + ") " +
			       " ) union " +
				   	 " (SELECT " +
				   	 		" mg.id||',0' id, " +
				   	 		" mg.display_name display_name , " +
				   	 		" mg.id market_group, " +
				   	 		" 0 market_id " +
				   	  " FROM " +
				   	 		" opportunities o, " +
				   	 		" markets m, " +
				   	 		" market_groups mg, " +
				   	 		" skin_market_group_markets smgm, " +
				   	 		" skin_market_groups smg, " +
				   	 		" opportunity_templates op " +
				   	  " WHERE " +
				   	  		" m.id = smgm.market_id " +
				   	  		" and smg.skin_id=" + skinId + " " +
				   	  		" and smgm.skin_market_group_id = smg.id " +
				   	  		" and mg.id = smg.market_group_id " +
				   	  		" and o.market_id=m.id " +
					  		" and o.market_id = op.market_id " + 
					  		" and o.opportunity_type_id = op.opportunity_type_id " +
					  		" and op.is_active = 1" +
				   	  		" and o.time_act_closing is not null " +
				   	  		" and o.market_id in (select smgm.market_id "+
												" from skin_market_group_markets smgm, skin_market_groups smg "+
												" where smgm.skin_market_group_id = smg.id "+
													" and smg.skin_id=" + skinId + ") " +
							" and o.time_est_closing >= ? " +
							" and o.time_est_closing < ? " +
							" and o.opportunity_type_id in (" + oppTypeIdStr + ") " +
				   " )) order by " +
						" market_group," +
						" market_id";

			ps = con.prepareStatement(sql);

//			ps.setTimestamp(1,CommonUtil.convertToTimeStamp(startD.getTime()));
//			ps.setTimestamp(2,CommonUtil.convertToTimeStamp(endD.getTime()));
//			ps.setTimestamp(3,CommonUtil.convertToTimeStamp(startD.getTime()));
//			ps.setTimestamp(4,CommonUtil.convertToTimeStamp(endD.getTime()));
            ps.setTimestamp(1,CommonUtil.convertToTimeStamp(from));
            ps.setTimestamp(2,CommonUtil.convertToTimeStamp(to));
            ps.setTimestamp(3,CommonUtil.convertToTimeStamp(from));
            ps.setTimestamp(4,CommonUtil.convertToTimeStamp(to));

			SelectItemGroup group1 = null;
			Map<Long, String> items = null;
			rs = ps.executeQuery();

			while(rs.next()) {
				if (rs.getString("id").endsWith(",0")) {
					if (items != null) {
                        Map<Long, String> sorted = new TreeMap<Long, String>(new ComparatorHebrewPriority(items));
						hm.putAll(sorted);
					}
					items = new LinkedHashMap<Long, String>();
					items.put(rs.getLong("id"), MarketsManagerBase.getMarketName(skinId, rs.getLong("id")));
				} else {
					items.put(rs.getLong("id"), MarketsManagerBase.getMarketName(skinId, rs.getLong("id")));
				}
			}

			if (items != null) {
                Map<Long, String> sorted = new TreeMap<Long, String>(new ComparatorHebrewPriority(items));
				hm.putAll(sorted);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return hm;
	}

	public static ArrayList<TreeItem> getMarketsAnyoption(Connection con, boolean is0100) throws SQLException, UnsupportedEncodingException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<TreeItem> list = new ArrayList<TreeItem>();

		long skinId = CommonUtil.getAppData().getSkinId();

		try {
			String sql =
					"SELECT DISTINCT (smg.MARKET_GROUP_ID || ',' || smgm.market_id) AS display_id, " +
						      "  m.id AS market_id, " +
						      "  smg.MARKET_GROUP_ID, " +
						      "  m.DISPLAY_NAME  AS market_name, " +
						      "  mg.DISPLAY_NAME AS group_name, " +
						      "  smgm.group_priority, " +
						      "  smg.SKIN_ID, " +
						      "  mns.name, " +
						      "  mdg2.sort_priority AS sub_group_sort_priority, " +
						      "  (SELECT mdg.display_name " +
						      "  FROM market_display_groups mdg " +
						      "  WHERE smgm.skin_display_group_id = mdg.id(+) " +
						      "  ) AS subGroup_display_name " +
						"FROM SKIN_MARKET_GROUP_MARKETS smgm, " +
						      "  markets m, " +
						      "  market_groups mg, " +
						      "  skin_market_groups smg, " +
						      "  market_display_groups mdg2, " +
//						      "  opportunities o, " +
						      "  opportunity_templates ot, " +
						      "  market_name_skin mns " +
						"WHERE " +
						      " smgm.market_id = m.id " +
						      " AND smgm.skin_market_group_id = smg.ID " +
						      " AND smg.MARKET_GROUP_ID = mg.ID " +
						      " AND smg.SKIN_ID = ? " +
						      " AND mns.skin_id = smg.skin_id " +
						      " AND mns.market_id = m.id " +
//						      " AND o.market_id = m.id " +
						      " AND ot.opportunity_type_id = " + Opportunity.TYPE_REGULAR + " " +
//						      " AND o.time_act_closing IS NOT NULL " +
						      " AND mdg2.id(+) = smgm.skin_display_group_id " +
						      " AND ot.scheduled = 2 " +
						      " AND ot.is_active = 1 " +
						      " AND m.id = ot.market_id " +
						      " AND m.id NOT IN (13, 25, 24, 23, 61, 12) " +
						      (is0100 ? " and m.feed_name like '%Binary0-100'" : "");
			sql+= "ORDER BY " +
				      "smg.MARKET_GROUP_ID, " +
				      "sub_group_sort_priority, " +
				      "upper(mns.name)";


			ps = con.prepareStatement(sql);
			ps.setLong(1,skinId);
			//ps.setTimestamp(1,CommonUtil.convertToTimeStamp(d));

			rs = ps.executeQuery();

			String group = null;
	        String geoGroup = null;

	        /*TreeItem nasdaqNDX = new TreeItem();
	        nasdaqNDX.setGroup_name("markets.groups.indices");
	        nasdaqNDX.setMarket_group_id(2);
	        nasdaqNDX.setMarket_subGroup_display_name("markets.displaygroups.us");
	        nasdaqNDX.setMarket_id(200);
	        nasdaqNDX.setMarket_name("markets.Nasdaq100.Index");
			boolean insert1T = true;*/
			while(rs.next()) {
				TreeItem temp = new TreeItem();
				temp.setGroupName(rs.getString("group_name"));
				temp.setMarketGroupId(Integer.parseInt(rs.getString("market_group_id")));
				temp.setMarketName(MarketsManagerBase.getMarketName(skinId, rs.getLong("market_id")));
				temp.setMarketSubGroupDisplayName(rs.getString("subgroup_display_name"));
				temp.setMarketId(Integer.parseInt(rs.getString("market_id")));
	            if (null == group || !group.equals(temp.getGroupName())) {
	                group = temp.getGroupName();
	                temp.setPrintGroup(true);
	            } else {
	                temp.setPrintGroup(false);
	            }
	            if (temp.getMarketGroupId() != ConstantsBase.MARKET_GROUP_CURRENCIES && temp.getMarketGroupId() != ConstantsBase.MARKET_GROUP_COMMODITIES
	            		&& (null == geoGroup || !geoGroup.equals(temp.getMarketSubGroupDisplayName()))) {
	                geoGroup = temp.getMarketSubGroupDisplayName();
	                temp.setPrintGeoGroup(true);
	            } else {
	                temp.setPrintGeoGroup(false);
	            }

				list.add(temp);
				/*if(rs.getString("group_name").equals("markets.groups.indices") &&
						rs.getString("subgroup_display_name").equals("markets.displaygroups.us") && insert1T
						){
					list.add(nasdaqNDX);
					insert1T = false;
				}*/
			}



		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return list;
	}


	// 	gets all the markets and groups list for a specific user

	public static ArrayList<SelectItem> getAllMarketsAndGroupsByUser(Connection con,long userId, boolean is0100) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();

		try {

			long skinId = CommonUtil.getAppData().getSkinId();
			String marketsSql = "(SELECT " +
									"distinct(m.id) " +
								  "FROM " +
								  	"markets m," +
								  	"opportunities o," +
								  	"investments i " +
								  "WHERE " +
								  	"i.user_id = ? AND " +
								  	"i.opportunity_id = o.id AND " +
								  	"o.market_id = m.id AND " +
								  	"o.opportunity_type_id <> ? " +
								  	(is0100 ? "AND m.feed_name like '%Binary0-100')" : ")");

			String groupsSQL = "(SELECT " +
									"distinct(mg.id) " +
								"FROM " +
									"market_groups mg," +
									"markets m," +
									"opportunities o," +
									"investments i, " +
									"skin_market_group_markets smgm, " +
									"skin_market_groups smg " +
							   "WHERE " +
							   	"i.user_id = ? AND " +
							   	"i.opportunity_id = o.id AND " +
							   	"o.market_id = m.id AND " +
							   	"o.opportunity_type_id <> ? AND " +
							   	"m.id = smgm.market_id AND " +
							   	"mg.id = smg.market_group_id AND " +
							   	"smgm.skin_market_group_id = smg.id AND " +
							   	"smg.skin_id= " + skinId + ")";

			String sql =
					"((SELECT " +
						"smg.market_group_id||','||m.id id," +
						"m.display_name display_name , " +
						"smg.market_group_id market_group, " +
						"m.id market_id  " +
					"FROM " +
						"markets m , " +
						"skin_market_group_markets smgm, " +
						"skin_market_groups smg " +
					"WHERE " +
						"smgm.skin_market_group_id = smg.id AND " +
						"smg.skin_id = " + skinId + "AND " +
						"m.id = smgm.market_id AND " +
						"m.id in " + marketsSql+

					") UNION " +

					"(SELECT " +
						"mg.id||',0' id," +
						"mg.display_name display_name ," +
						"mg.id market_group, " +
						"0 market_id  " +
					"FROM " +
						"market_groups mg " +
					"WHERE  " +
						"mg.id in " + groupsSQL;
			sql += "))";

			String sqlOneTouchMarkets = "SELECT " +
											"distinct(m.id)," +
											"m.display_name "+
										"FROM " +
											"markets m," +
											"opportunities o," +
											"investments i "+
										"WHERE " +
											"m.id in (" + ConstantsBase.ONE_TOUCH_MARKETS +") AND " +
											"o.market_id = m.id AND " +
											"i.user_id = ? AND " +
											"o.market_id = m.id AND " +
											"i.opportunity_id = o.id";

			PreparedStatement ps1T = con.prepareStatement(sqlOneTouchMarkets);
			ps1T.setLong(1,userId);
			ResultSet rs1T = ps1T.executeQuery();

			ps = con.prepareStatement(sql);
			ps.setLong(1,userId);
			ps.setLong(2, Opportunity.TYPE_OPTION_PLUS);
			ps.setLong(3,userId);
			ps.setLong(4, Opportunity.TYPE_OPTION_PLUS);

			SelectItemGroup group1 = null;
			ArrayList<SelectItem> items = null;
			rs = ps.executeQuery();

			while(rs.next()) {

				if (rs.getString("id").endsWith(",0")) {

					if (items != null) {
                        Collections.sort(items, new SelectItemComparatorHebrewPriority());
						SelectItem[] sitems = new SelectItem[items.size()];
						for (int i = 0; i < items.size(); i++) {
							sitems[i] = (SelectItem) items.get(i);
						}
						group1.setSelectItems(sitems);
						list.add(group1);
					}

					group1 = new SelectItemGroup(MarketsManagerBase.getMarketName(skinId, rs.getLong("id")));
					items = new ArrayList<SelectItem>();
					SelectItem si = new SelectItem();
					si.setValue(rs.getString("id"));
					si.setLabel(MarketsManagerBase.getMarketName(skinId, rs.getLong("id")));
					items.add(si);

				} else {

					SelectItem si = new SelectItem();
					si.setValue(rs.getString("id"));
					si.setLabel(MarketsManagerBase.getMarketName(skinId, rs.getLong("id")));
					items.add(si);
				}
			}
			//if there are oneTouch-specific markets - add them manually  to select
			boolean oneTgroupFound = false;
			while(rs1T.next()){
				for(Object curItm : list){
					if(curItm instanceof ArrayList){
						if(((SelectItem)curItm).getValue().equals("2,0")){
							oneTgroupFound = true;
							// add new SelectItem to the indices group
							SelectItem si = new SelectItem();
							si.setValue("2,"+rs1T.getString("id"));
							si.setLabel(MarketsManagerBase.getMarketName(skinId, rs.getLong("id")));
							((ArrayList)curItm).add(si);
						}
					}
					if(curItm instanceof SelectItemGroup){

						if(((SelectItemGroup)curItm).getSelectItems()[0].getValue().equals("2,0")){
							oneTgroupFound = true;
							// add new SelectItem to the indices group
							SelectItem si = new SelectItem();
							si.setValue("2,"+rs1T.getString("id"));
							si.setLabel(MarketsManagerBase.getMarketName(skinId, rs.getLong("id")));
							SelectItem[] temp = new SelectItem[((SelectItemGroup)curItm).getSelectItems().length+1];

							for(int i=0;i<((SelectItemGroup)curItm).getSelectItems().length;i++){
								temp[i] = ((SelectItemGroup)curItm).getSelectItems()[i];
							}
							temp[((SelectItemGroup)curItm).getSelectItems().length] = si;
							((SelectItemGroup)curItm).setSelectItems(temp);
						}
					}
					if(!oneTgroupFound){
						SelectItemGroup group = new SelectItemGroup(CommonUtil.getMessage("markets.groups.indices",null));
						SelectItem si = new SelectItem();
						si.setValue("2,"+ rs1T.getString("id"));
						si.setLabel(MarketsManagerBase.getMarketName(skinId, rs.getLong("id")));
						SelectItem[] itms = new SelectItem[1];
						itms[0] = si;
						group.setSelectItems(itms);
						items.add(group);
					}
				}

			}

			if (items != null) {
                Collections.sort(items, new SelectItemComparatorHebrewPriority());
				SelectItem[] sitems = new SelectItem[items.size()];
				for (int i = 0; i < items.size(); i++) {
					sitems[i] = (SelectItem) items.get(i);
				}
				group1.setSelectItems(sitems);
				list.add(group1);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return list;
	}

	/**
	 * Take opportunity disabled flag from the db.
	 *
	 * @param conn the db connection to use
	 * @param oppId the id of the opp to check
	 * @return The value of the "is_disabled" flag for this opp.
	 * @throws SQLException
	 */
	public static boolean isOpportunityDisabled(Connection conn, long oppId) throws SQLException {
		boolean disabled = false;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			String sql =
                "SELECT " +
                    "A.is_disabled, " +
                    "B.is_suspended " +
                "FROM " +
                    "opportunities A, " +
                    "markets B " +
                "WHERE " +
                    "A.market_id = B.id AND " +
                    "A.id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1,oppId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				disabled = rs.getBoolean("is_disabled") || rs.getBoolean("is_suspended");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return disabled;
	}

	/**
	 * Take opportunity current acceptable level deviation when placing
	 * investment (the difference between the current level from the cache and
	 * the level submitted from the page that we tolerate and will accept the
	 * investment).
	 *
	 * @param conn the db connection to use
	 * @param oppId the id of the opp to check for
	 * @return The opportunity curent acceptable level deviation.
	 * @throws SQLException
	 */
//	public static BigDecimal getOpportunityAcceptableLevelDeviation(Connection conn, long oppId) throws SQLException {
//        BigDecimal deviation = new BigDecimal(0);
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//		try {
//			String sql =
//			        "SELECT " +
//			            "B.acceptable_level_deviation " +
//                    "FROM " +
//                        "opportunities A, " +
//                        "markets B " +
//                    "WHERE " +
//                        "A.market_id = B.id AND " +
//                        "A.id = ?";
//			pstmt = conn.prepareStatement(sql);
//			pstmt.setLong(1,oppId);
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				deviation = rs.getBigDecimal("acceptable_level_deviation");
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(pstmt);
//		}
//		return deviation;
//	}


	public static boolean checkActiveUser(Connection conn, String name) throws SQLException {

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT 1 from investments i,users u where i.user_id=u.id and u.user_name='"+name.toUpperCase()+"'";
			pstmt = conn.prepareStatement(sql);

			rs = pstmt.executeQuery();
			if (rs.next()) {
				return true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return false;
	}

	/**
	 * Get all markets of oneTouch
	 * @param con
	 * @param firstInv
	 * @param lastInv
	 * @return
	 * @throws SQLException
	 * @throws UnsupportedEncodingException
	 */
	public static ArrayList<SelectItem> getOneTouchMarkets(Connection con, Date firstInv, Date estClosing) throws SQLException, UnsupportedEncodingException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		FacesContext context = FacesContext.getCurrentInstance();
        ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		try {
			String sql = " SELECT " +
							" distinct m.id, " +
							" m.display_name " +
            			 " FROM " +
            			 	" opportunities o, " +
            			 	" markets m , " +
            			 	" opportunity_one_touch ot, " +
            			 	" one_touch_markets_skins otms " +
            			 " WHERE " +
            			 	" o.market_id=m.id and " +
            			 	" o.id = ot.opportunity_id and " +
            			 	" o.time_act_closing is not null and " +
            			 	" o.opportunity_type_id = ? and "+
            			    " otms.skin_id = " + a.getSkinId() + " and " +
            			    " otms.market_id=m.id " ;

			if ( null != firstInv && null != estClosing ) {
	            	sql += " and trunc(o.time_first_invest)=trunc(?) and trunc(o.time_est_closing)=trunc(?) ";
	        }

			sql += " order by  m.display_name ";

			ps = con.prepareStatement(sql);

	        ps.setLong(1, Opportunity.TYPE_ONE_TOUCH);

	        if ( null != firstInv && null != estClosing ) {
	           	ps.setTimestamp(2, CommonUtil.convertToTimeStamp(firstInv));
	           	ps.setTimestamp(3, CommonUtil.convertToTimeStamp(estClosing));
	        }

			rs = ps.executeQuery();

			ArrayList items = null;

			while( rs.next() ) {
				SelectItem si = new SelectItem();
				si.setValue(String.valueOf(rs.getInt("id")));
				si.setLabel(MarketsManagerBase.getMarketName(a.getSkinId(), rs.getLong("id")));
				list.add(si);
			}
            Collections.sort(list, new SelectItemComparatorHebrewPriority());


		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return list;
	}

    /**
     * get opprtunity expiration time by investment id
     * @param conn
     * @param id - investment id
     * @return opp expiration date
     */
    public static Date getRunningOpportunityExpTimeByInvId(Connection conn, long id) throws Exception {
        Date expDate = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "to_char(o.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing " +
                "FROM " +
                    "opportunities o, " +
                    "investments i " +
                "WHERE " +
                    "o.id = i.opportunity_id AND " +
                    "i.id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                String tec = rs.getString("time_est_closing");
                SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
                expDate = localDateFormat.parse(tec);
            }

        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return expDate;
    }

    /**
     * Get markets closing levels for landingPage (Anyoption)
     * @param con
     * @param marketsList
     * @return
     * @throws SQLException
     * @throws ParseException
     */
	public static HashMap<Long, LandingMarketDetails> getClosingLevels(Connection con, String marketsList) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		HashMap<Long, LandingMarketDetails> hash = new HashMap<Long, LandingMarketDetails>();
		try {
			String sql =
					"SELECT " +
						"o.market_id, " +
						"to_char(o.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') as ftime_est_closing, " +
						"o.closing_level, " +
						"m.display_name " +
					 "FROM " +
					 	"opportunities o," +
					 	"markets m " +
					 "WHERE " +
						 "o.opportunity_type_id = ? AND " +
						 "o.closing_level IS NOT NULL AND "+
						 "o.time_est_closing IS NOT NULL AND "+
						 "o.time_est_closing = " +
							 				"(select max(time_est_closing) " +
							 				  "from opportunities o2 " +
							 				   "where o2.opportunity_type_id = ? AND " +
							 					      "o2.closing_level IS NOT NULL AND " +
							 					      "o2.time_est_closing IS NOT NULL and o2.market_id = o.market_id) AND " +
						 "m.id = o.market_id AND " +
						 "m.id IN ("+ marketsList +") ";

				pstmt = con.prepareStatement(sql);
				pstmt.setLong(1, Opportunity.TYPE_REGULAR);
				pstmt.setLong(2, Opportunity.TYPE_REGULAR);
				rs = pstmt.executeQuery();

				while(rs.next()) {
					LandingMarketDetails lmd = new LandingMarketDetails();
					lmd.setMarketId(rs.getLong("market_id"));
					lmd.setClosingLevel(rs.getBigDecimal("closing_level"));
					lmd.setMarketImageName("will" + lmd.getMarketId() + ".gif");
					hash.put(rs.getLong("market_id"), lmd);
				}
			} finally {
				closeResultSet(rs);
				closeStatement(pstmt);
			}
		return hash;
	}

	public static long getMarketCurrentOpportunity(Connection conn, long marketId, long opportunityTypeId) throws SQLException {
	    long oppId = 0;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "A.id " +
                "FROM " +
                    "opportunities A, " +
                    "markets B " +
                "WHERE " +
                    "A.market_id = B.id AND " +
                    "A.market_id = ? AND " +
                    "A.opportunity_type_id = ? AND " +
                    "A.is_published = 1 AND " +
//                    "B.is_suspended = 0 AND " +
                    "A.closing_level IS NULL " +
                "ORDER BY " +
                    "A.scheduled, " +
                    "A.time_est_closing";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, marketId);
            pstmt.setLong(2, opportunityTypeId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                oppId = rs.getInt("id");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return oppId;
	}

	public static long getMarketCurrentOpportunityBinary0100(Connection conn, long marketId) throws SQLException {
	    long oppId = 0;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "A.id " +
                "FROM " +
                    "opportunities A, " +
                    "markets B " +
                "WHERE " +
                    "A.market_id = B.id AND " +
                    "A.market_id = ? AND " +
                    "A.is_published = 1 AND " +
//                    "current_timestamp < A.time_last_invest AND " +
                    "A.current_level > 0 AND " + 
                    "A.is_suspended = 0 " + //opportunity not suspended
                "ORDER BY " +
                    "A.time_est_closing";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, marketId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                oppId = rs.getInt("id");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return oppId;
	}

//	public static ArrayList<Long> getOptionPlusCurrentMarket(Connection conn, long skinId, boolean onlyOpened) throws SQLException {
//	    ArrayList<Long> l = new ArrayList<Long>();
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        try {
//            String sql =
//            	"SELECT " +
//					"B.market_id " +
//				"FROM " +
//					"skin_market_groups A, " +
//					"skin_market_group_markets B, " +
//					"markets C, " +
//					"opportunities D " +
//				"WHERE " +
//					"A.skin_id = ? AND " +
//					"C.feed_name like '%Option+'  AND " + // Option+ markets
//					"A.id = B.skin_market_group_id AND " +
//					"B.market_id = C.id AND " +
//					"B.market_id = D.market_id AND " +
//					"D.is_published = 1 AND " +
//					"C.is_suspended = 0 AND " +
//					"D.closing_level IS NULL AND "+ 
//					"D.is_suspended = 0 "; // not suspended opportunity
//					
//			if (onlyOpened) {
//				sql += 
//					"AND current_timestamp < D.time_last_invest ";
//			}
//			sql += 
//				"GROUP BY " +
//				"B.market_id, " +
//				"B.home_page_priority " +
//			"ORDER BY " +
//				"B.home_page_priority";
//            pstmt = conn.prepareStatement(sql);
//            pstmt.setLong(1, skinId);
//            rs = pstmt.executeQuery();
//            while (rs.next()) {
//            	l.add(rs.getLong("market_id"));
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(pstmt);
//        }
//        return l;
//	}

//	public static long getBinary0100CurrentMarket(Connection conn, long skinId) throws SQLException {
//        long mId = 0;
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        try {
//            String sql =
//           		"SELECT " +
//            		"B.market_id " +
//            	"FROM " +
//            		"skin_market_groups A, " +
//            		"skin_market_group_markets B, " +
//            		"markets C, " +
//            		"opportunities D, " +
//            		"opportunity_types E " +
//            	"WHERE " +
//            		"A.skin_id = ? AND " +
//            		"A.id = B.skin_market_group_id AND " +
//            		"B.market_id = C.id AND " +
//            		"B.market_id = D.market_id AND " +
//            		"D.opportunity_type_id = E.id AND " +
//            		"E.product_type_id = 4 AND " + // Binary 0-100
//            		"D.is_published = 1 AND " +
//            		"C.is_suspended = 0 AND " +
//            		"D.closing_level IS NULL AND " +
//            		"D.current_level > 0 AND "+ 
//            		"D.is_suspended = 0 " + 	//opportunity not suspended
//            	"GROUP BY " +
//            		"B.market_id, " +
//            		"B.home_page_priority, " +
//            		"CASE WHEN current_timestamp < D.time_last_invest THEN 0 ELSE 1 END " +
//            	"ORDER BY " +
//            		"CASE WHEN current_timestamp < D.time_last_invest THEN 0 ELSE 1 END, " +
//            		"B.home_page_priority";
//            pstmt = conn.prepareStatement(sql);
//            pstmt.setLong(1, skinId);
//            rs = pstmt.executeQuery();
//            if (rs.next()) {
//            	mId = rs.getLong("market_id");
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(pstmt);
//        }
//        return mId;
//	}

	public static long getMarketCurrentOpenOpportunity(Connection conn, long marketId) throws SQLException {
	    long oppId = 0;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "A.id " +
                "FROM " +
                    "opportunities A " +
                "WHERE " +
                    "A.market_id = ? AND " +
                    "A.is_published = 1 AND " +
                    "A.closing_level IS NULL " +
                "ORDER BY " +
                    "A.scheduled, " +
                    "A.time_est_closing";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, marketId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                oppId = rs.getInt("id");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return oppId;
	}

	public static List<Binary0100EventSelectionBean> getBinary0100PublishedEvents(Connection conn, long skinId, long oppId) throws SQLException {
	    List<Binary0100EventSelectionBean> l = new ArrayList<Binary0100EventSelectionBean>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "o.id, " +
                        "o.current_level, " +
                        "o.opportunity_type_id, " +
                        "to_char(o.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') as time_est_closing, " +
                        "o.market_id, " +
                        "m.display_name, " +
                        "m.decimal_point " +
                    "FROM " +
                        "skin_market_groups mg, " +
                        "skin_market_group_markets mgm, " +
                        "markets m, " +
                        "opportunities o " +
                    "WHERE " +
                        "mg.skin_id = ? AND " +
                        "m.feed_name like '%Binary0-100' AND " + // Binary 0-100
                        "mg.id = mgm.skin_market_group_id AND " +
                        "mgm.market_id = m.id AND " +
                        "o.market_id = m.id AND " +
                        "o.is_published = 1 AND " +
//                        "m.is_suspended = 0 AND " +
                        "o.closing_level IS NULL AND " +
                        "o.id != ? AND " +
                        "o.current_level > 0 " +
                     "ORDER BY " +
                        "m.is_suspended, " +
                        "mgm.home_page_priority ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, skinId);
            pstmt.setLong(2, oppId);
            rs = pstmt.executeQuery();
            Binary0100EventSelectionBean b = null;
            while (rs.next()) {
                b = new Binary0100EventSelectionBean();
                b.setOpportunityId(rs.getLong("id"));
                b.setEventLevel(CommonUtil.formatLevelByDecimalPoint(rs.getDouble("current_level"), rs.getInt("decimal_point")));
                b.setOpportunityTypeId(rs.getLong("opportunity_type_id"));
                b.setTimeEstClosing(getTimeWithTimezone(rs.getString("time_est_closing")));
                b.setMarketId(rs.getLong("market_id"));
                b.setMarket(rs.getString("display_name"));
                l.add(b);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
	    return l;
	}

//	public static ArrayList<TreeItem> getClosedOpportunitiesMarketsBinary0100(Connection con) throws SQLException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		ArrayList<TreeItem> list = new ArrayList<TreeItem>();
//
//		long skinId = CommonUtil.getAppData().getSkinId();
//
//		try {
//			String sql =
//				 " SELECT " +
//						" distinct(smg.MARKET_GROUP_ID||','|| smgm.market_id) as display_id, " +
//						" m.id as market_id, " +
//						" smg.MARKET_GROUP_ID, " +
//						" m.DISPLAY_NAME as market_name, " +
//						" mg.DISPLAY_NAME as group_name, " +
//						" smgm.group_priority, " +
//						" smg.SKIN_ID, " +
//                        " mns.name, " +
//						" mdg2.sort_priority as sub_group_sort_priority, " +
//						" (   select  mdg.display_name " +
//						" from market_display_groups mdg " +
//						" where smgm.skin_display_group_id = mdg.id (+)) as subGroup_display_name " +
//			     " FROM " +
//				 		" SKIN_MARKET_GROUP_MARKETS smgm , " +
//				 		" markets m , " +
//				 		" market_groups mg , " +
//				 		" skin_market_groups smg, " +
//				 		" market_display_groups mdg2, " +
//				 		" opportunities o, " +
//				 		" opportunity_templates ot, " +
//                        " market_name_skin mns " +
//				 " WHERE " +
//				 		" smgm.market_id = m.id  " +
//				 		" and smgm.skin_market_group_id = smg.ID " +
//						" and smg.MARKET_GROUP_ID = mg.ID " +
//						" and smg.SKIN_ID = ? " +
//                        " and mns.skin_id = smg.skin_id " +
//                        " and mns.market_id = m.id " +
//						" and o.market_id = m.id " +
//						" and o.opportunity_type_id in (" + Opportunity.TYPE_BINARY_0_100_ABOVE + ", " + Opportunity.TYPE_BINARY_0_100_BELOW + ") " +
// 						" and ot.scheduled = 1 " +
// 						" and o.time_act_closing is not null " +
//						" and mdg2.id(+) = smgm.skin_display_group_id " +
//						" and ot.is_active=1 " +
//						" and ot.is_full_day=1 " +
//						" and m.id = ot.market_id " +
//						" and m.id NOT IN (13,25,24,23,61,12) " +
//				 " ORDER BY " +
//						" smg.MARKET_GROUP_ID, " +
//						" sub_group_sort_priority, " +
//                        " upper(mns.name) ";
//
//
//			ps = con.prepareStatement(sql);
//			ps.setLong(1,skinId);
//
//			rs = ps.executeQuery();
//
//			String group = null;
//	        String geoGroup = null;
//
//			while(rs.next()) {
//				TreeItem temp = new TreeItem();
//				temp.setGroupName(rs.getString("group_name"));
//				temp.setMarketGroupId(Integer.parseInt(rs.getString("market_group_id")));
//				temp.setMarketName(rs.getString("market_name"));
//				temp.setMarketSubGroupDisplayName(rs.getString("subgroup_display_name"));
//				temp.setMarketId(Integer.parseInt(rs.getString("market_id")));
//	            if (null == group || !group.equals(temp.getGroupName())) {
//	                group = temp.getGroupName();
//	                temp.setPrintGroup(true);
//	            } else {
//	                temp.setPrintGroup(false);
//	            }
//	            if (temp.getMarketGroupId() != ConstantsBase.MARKET_GROUP_CURRENCIES && temp.getMarketGroupId() != ConstantsBase.MARKET_GROUP_COMMODITIES
//	            		&& (null == geoGroup || !geoGroup.equals(temp.getMarketSubGroupDisplayName()))) {
//	                geoGroup = temp.getMarketSubGroupDisplayName();
//	                temp.setPrintGeoGroup(true);
//	            } else {
//	                temp.setPrintGeoGroup(false);
//	            }
//
//				list.add(temp);
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//
//		return list;
//	}

    public static Opportunity getUsersLastTradeBinary0100Event(Connection conn, long userId) throws SQLException {
        Opportunity opp = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sql =
                "SELECT " +
                    "B.id, " +
                    "B.market_id " +
                "FROM " +
                    "investments A, " +
                    "opportunities B, " +
                    "opportunity_types C " +
                "WHERE " +
                    "A.user_id = ? AND " +
                    "A.opportunity_id = B.id AND " +
                    "B.opportunity_type_id = C.id AND " +
                    "C.product_type_id = 4 AND " +
                    "current_timestamp < B.time_last_invest " +
                "ORDER BY " +
                    "A.id DESC";
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                opp = new Opportunity();
                opp.setId(rs.getLong("id"));
                opp.setMarketId(rs.getLong("market_id"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return opp;
    }
}