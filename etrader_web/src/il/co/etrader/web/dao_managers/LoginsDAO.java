package il.co.etrader.web.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import il.co.etrader.util.CommonUtil;

/**
 * Logins DAO.
 *
 * @author Asher
 */
public class LoginsDAO extends com.anyoption.common.daos.LoginsDAO {
	
	public static boolean logOut(Connection conn, long loginId) throws SQLException {
		PreparedStatement ps = null;
		boolean res;
        try {
            String sql =
            	" UPDATE logins " +
            	" SET logout_time=to_date(?,'DD/MM/YYYY HH24:MI:SS') " +
            	" WHERE id = ? ";

            ps = conn.prepareStatement(sql);
            ps.setString(1, CommonUtil.getDateAndTimeFormat(new Date()));
            ps.setLong(2, loginId);
            res = ps.executeUpdate() > 0;
        } finally {
        	closeStatement(ps);
        }
        return res;
	}
}