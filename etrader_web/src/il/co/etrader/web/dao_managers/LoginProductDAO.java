package il.co.etrader.web.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import il.co.etrader.bl_managers.LoginProductManager.LoginProductEnum;

/**
 * @author kiril.mutafchiev
 */
public class LoginProductDAO extends il.co.etrader.dao_managers.LoginProductDAO {

	public static LoginProductEnum getLoginProduct(Connection con, long skinId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "select login_product_type_id from login_product_map where skin_id = ? and is_enabled = 1";
			ps = con.prepareStatement(sql);
			ps.setLong(1, skinId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return LoginProductEnum.getById(rs.getInt("login_product_type_id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return null;
	}
}