package il.co.etrader.web.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.anyoption.common.beans.TierUser;

import il.co.etrader.dao_managers.TiersDAOBase;


/**
 * TiersDAO class.
 *
 * @author Kobi
 */
public class TiersDAO extends TiersDAOBase {


	/**
	   * Insert user to tier
	   * @param con db connection
	   * @param tu TierUser instance
	   * @throws SQLException
	   */
	  public static void insertIntoTierUser(Connection con, TierUser tu) throws SQLException {
		  PreparedStatement ps = null;

		  try {
			  String sql =
				  	"INSERT INTO " +
				  		"tier_users(id, user_id, tier_id, time_created, writer_id, points, qualification_time) " +
				  	"VALUES " +
				  		"(SEQ_TIER_USERS.NEXTVAL,?,?,sysdate,?,?,sysdate)";

			  ps = con.prepareStatement(sql);
			  ps.setLong(1, tu.getUserId());
			  ps.setLong(2, tu.getTierId());
			  ps.setLong(3, tu.getWriterId());
			  ps.setLong(4, tu.getPoints());
			  ps.executeUpdate();

			  tu.setId(getSeqCurValue(con, "SEQ_TIER_USERS"));

		  } finally {
			  closeStatement(ps);
		  }
	  }

}
