package il.co.etrader.web.dao_managers;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.bl_vos.DirectPaymentMapping;
import com.anyoption.common.util.CommonUtil;

import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.dao_managers.TransactionsDAOBase;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.TransactionsManager;


public class TransactionsDAO extends TransactionsDAOBase{


	public static ArrayList<Transaction> getByUserAndDates(Connection con,long id,Date from,Date to,
			String status, int internalType, String utcOffset) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<Transaction> list = new ArrayList<Transaction>();

		  try {
			    String sql = "SELECT " +
							    "t.*, " +
				                "ty.description typename, " +
				                "ty.class_type classtype, " +
				                "ts.description statusdesc, " +
				                "u.currency_id currency_id, " +
				                "pt.name paymentMethodName, " +
				                "pt.description directProviderName " +
			                "FROM " +
			                	"transactions t " +
			                	"left join  bonus_users bu on t.bonus_user_id = bu.id " +
			                         "left join bonus_types bt on bt.id = bu.type_id " +
			                    "left join payment_methods pt on t.payment_type_id = pt.id, " +
			                    "transaction_types ty, " +
			                    "transaction_statuses ts, " +
			                    "users u " +
			                 "WHERE " +
			                 	"t.type_id = ty.id and " +
			                 	"t.status_id = ts.id and " +
			                 	"t.user_id = ? and " +
			                 	"t.user_id = u.id and " +
			                 	"t.type_id <> ? and " +
			                 	"t.type_id <> ? and " +			                 	
					    		"t.time_created >= ? and " +
					    		"t.time_created <= ? and " +
					    		"t.status_id in (" + status + ") and " +
					    		"(bt.class_type_id is null or bt.class_type_id != " + ConstantsBase.BONUS_CLASS_TYPE_NEXT_INVEST_ON_US + ") " +
			                 "ORDER BY " +
			                 	"t.time_created desc";

				ps = con.prepareStatement(sql);
				ps.setLong(1, id);
				ps.setLong(2, internalType);
				ps.setLong(3, TransactionsManagerBase.TRANS_TYPE_CUP_INTERNAL_CREDIT); // TODO - quick fix before mini deploy !!!
				ps.setTimestamp(4, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, utcOffset)));
				ps.setTimestamp(5, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to), utcOffset)));

				rs=ps.executeQuery();

				while (rs.next()) {
					Transaction t = getVO(con,rs);
					t.setPaymentTypeDesc(rs.getString("directProviderName"));
					t.setPaymentTypeName(rs.getString("paymentMethodName"));
				    list.add(t);
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;

	  }

	public static boolean isFirstDeposit(Connection con,long userId) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {
			    String sql = "SELECT " +
			    				"count(*) num " +
			    			 "FROM " +
			    			 	"transactions t " +
			    			 "WHERE " +
			    			 	"t.user_id = ? AND " +
			    			 	"t.type_id IN ( " + TransactionsManager.TRANS_TYPE_CC_DEPOSIT + ","
			    			 				   + TransactionsManager.TRANS_TYPE_DIRECT_BANK_DEPOSIT + ","
			    			 				   + TransactionsManager.TRANS_TYPE_PAYPAL_DEPOSIT + ","
			    			 				   + TransactionsManager.TRANS_TYPE_CASHU_DEPOSIT + ","
			    			 				   + TransactionsManager.TRANS_TYPE_ACH_DEPOSIT + ","
			    			 				   + TransactionsManager.TRANS_TYPE_ENVOY_BANKING_DEPOSIT + ","
			    			 				   + TransactionsManager.TRANS_TYPE_ENVOY_ONLINE_DEPOSIT + ","
			    			 				   + TransactionsManager.TRANS_TYPE_MONEYBOOKERS_DEPOSIT + ","
			    			 				    + TransactionsManager.TRANS_TYPE_BAROPAY_DEPOSIT + ","
			    			 				   + TransactionsManager.TRANS_TYPE_WEBMONEY_DEPOSIT + ","
			    			 				   + TransactionsManager.TRANS_TYPE_UKASH_DEPOSIT + ","
			    			 				   + TransactionsManager.TRANS_TYPE_INATEC_IFRAME_DEPOSIT + ","
			    			 				   + TransactionsManager.TRANS_TYPE_EPG_CHECKOUT_DEPOSIT + 
			    			 				  ") AND " +
			    			 "t.status_id IN (" +TransactionsManager.TRANS_STATUS_SUCCEED + "," +
			    			 					TransactionsManager.TRANS_STATUS_PENDING + ") " +
			    			 "ORDER BY " +
			    			 	"time_created desc";

				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);
				rs = ps.executeQuery();
				if (rs.next()) {
					return !(rs.getLong("num") > 1);
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
		  return false;
	}

	/**
	 * Get all direct payment mapping options
	 * @param con db connection
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<DirectPaymentMapping> getDirectPaymentMapping(Connection con) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<DirectPaymentMapping> list = new ArrayList<DirectPaymentMapping>();

		try {
			String sql = "SELECT * FROM direct_payment_mapping ";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while(rs.next()) {
				long id = rs.getLong("id");
				long countryId = rs.getLong("country_id");
				long currencyId = rs.getLong("currency_id");
				long paymentId = rs.getLong("payment_id");
				list.add(new DirectPaymentMapping(id, countryId, currencyId, paymentId));
			}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	public static boolean isInatecNotNotifiedTrx(Connection con, long id) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql =
				"SELECT " +
					"id " +
				"FROM " +
					"transactions " +
				"WHERE " +
					"type_id = ? AND " +
					"status_id = ? AND " +
					"id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, TransactionsManagerBase.TRANS_TYPE_DIRECT_BANK_DEPOSIT);
			ps.setLong(2, TransactionsManagerBase.TRANS_STATUS_AUTHENTICATE);
			ps.setLong(3, id);
			rs = ps.executeQuery();

			if (rs.next()) {
				return true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return false;
	}
	
	/**
	 * @param con
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static boolean isInatecIframeNotNotifiedTrx(Connection con, long id) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean res = false;
		try {
			String sql =
				"SELECT " +
				"	id " +
				"FROM " +
				"	transactions " +
				"WHERE " +
				"	type_id = ? AND " +
				"	status_id = ? AND " +
				"	id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, TransactionsManagerBase.TRANS_TYPE_INATEC_IFRAME_DEPOSIT);
			ps.setLong(2, TransactionsManagerBase.TRANS_STATUS_AUTHENTICATE);
			ps.setLong(3, id);
			rs = ps.executeQuery();

			if (rs.next()) {
				res = true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return res;
	}
	
	public static long getCUPWithdrawalAmount(Connection con, long userId, boolean countBeforeCup) throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			String sql = 
					
				" SELECT " +
						" nvl( deposit_amount - withdraw_amount,0) as available_to_withdraw " + 
				" FROM " +
				" (" +
						" SELECT " +
								" nvl(sum(t.amount - t.credit_amount),0) as deposit_amount " + 
						" FROM " +
								" transactions t, clearing_providers cp " + 
						" WHERE " +
								" t.user_id = ? " + 
						" AND " +
								" (cp.id = t.clearing_provider_id and cp.is_active = 1 ) " +
						" AND " +
								" t.type_id = 38 " + // CUP_DEPOSIT
						" AND " +
								" t.status_id in  ( 2,7,8 ) " + 
						" AND " +
								" t.selector_id > ?" +
						" AND " + 
					            " (t.amount - t.credit_amount) > 0 " + 
				" ), " + 
				" ( " +
						" SELECT" + 
								" NVL(SUM (t.amount - t.internals_amount),0)  AS WITHDRAW_AMOUNT" + 
						" FROM " +
								" transactions t " + 
						" WHERE " +
								" t.user_id = ? " +
						" AND " + 
								" t.type_id = 39 " + // CUP_WITHDRAW
						" AND " +
								" (t.status_id = 4 or t.status_id = 9) " + 
						" AND " +
								" t.selector_id > ? " +           
				")"; 	

			ps = con.prepareStatement(sql);

			ps.setLong(1, userId);
			if(countBeforeCup) { // selector_id = -1 are transactions before CUP
				ps.setLong(2, -2); // include transactions before CUP
			} else { 
				ps.setLong(2, -1); // exclude transactions before CUP
			}
			ps.setLong(3, userId);
			if(countBeforeCup) {
				ps.setLong(4, -2);
			} else { 
				ps.setLong(4, -1);
			};
			

			rs = ps.executeQuery();

			if(rs.next()) {
				return rs.getLong("available_to_withdraw");
			}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return 0;
	}

	public static long getDeltaPayWithdrawalAmount(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = "SELECT " +
                			"NVL( DEPOSIT_AMOUNT - WITHDRAW_AMOUNT,0) AS AVAILABLE_TO_WITHDRAW " +
                		 "FROM " +
                		 	"(SELECT " +
                		 		"NVL(SUM(t.amount - t.credit_amount),0) AS DEPOSIT_AMOUNT " +
						    "FROM " +
							 	"transactions t " +
						    "WHERE " +
                             	"t.user_id = ? " +
						        "AND t.type_id = ? " +
                                "AND t.clearing_provider_id = ? " +
							    "AND status_id IN  (" + TransactionsManager.SUCCESS_DEPOSITS_TYPES + ")) , " +

							"(SELECT " +
                                 "NVL(SUM (t.amount - t.internals_amount),0)  AS WITHDRAW_AMOUNT " +
                            "FROM " +
                                 "transactions t " +
						    "WHERE " +
                                 "t.user_id = ? " +
								 "AND  t.type_id = ? " +
								 "AND ( t.status_id = ? OR t.status_id = ? ) " +
                                 "AND t.is_credit_withdrawal = 1)";

			ps = con.prepareStatement(sql);

			ps.setLong(1, userId);
			ps.setLong(2, TransactionsManager.TRANS_TYPE_CUP_CHINA_DEPOSIT);
			ps.setLong(3, ClearingManager.DELTAPAY_CHINA_PROVIDER_ID);
			ps.setLong(4, userId);
			ps.setLong(5, TransactionsManager.TRANS_TYPE_CUP_CHINA_WITHDRAW);
			ps.setLong(6, TransactionsManager.TRANS_STATUS_REQUESTED);
			ps.setLong(7, TransactionsManager.TRANS_STATUS_APPROVED);

			rs = ps.executeQuery();

			if(rs.next()) {
				return rs.getLong("AVAILABLE_TO_WITHDRAW");
			}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return 0;
	}	
	
	public static Transaction getLastDeltaPayDeposit(Connection con, long userId, int typeId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Transaction vo = null;
		try {
			String sql =
				"SELECT " +
	                "t.*, " +
	                "ty.description typename, " +
	                "ty.class_type classtype, " +
	                "u.currency_id currency_id, " +
	                "pt.name paymentMethodName, " +
	                "pt.description directProviderName " +
	            "FROM " +
	                "transactions t, " +
	                "transaction_types ty, " +
	                "users u, " +
	                "payment_methods pt " +
	            "WHERE " +
	                "t.user_id = u.id AND " +
	                "t.type_id = ty.id AND " +
	                "t.payment_type_id = pt.id (+) AND " +
	                "t.user_id = ? AND " +
					"t.type_id = ? " +
				"ORDER BY " +
					"t.time_created DESC";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, typeId);
			rs = ps.executeQuery();

			if (rs.next()) {
				vo = getVO(con, rs);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return vo;
	}
	

	public static long getWithdrawalAmountForProvider(Connection con, long user_id, long provider_id) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = "SELECT " +
                			"NVL( DEPOSIT_AMOUNT - WITHDRAW_AMOUNT,0) AS AVAILABLE_TO_WITHDRAW " +
                		 "FROM " +
                		 	"(SELECT " +
                		 		"NVL(SUM(t.amount - t.credit_amount),0) AS DEPOSIT_AMOUNT " +
						    "FROM " +
							 	"transactions t " +
						    "WHERE " +
                             	"t.user_id = ? " +
                                "AND t.clearing_provider_id = ? " +
							    "AND status_id IN  (" + TransactionsManager.SUCCESS_DEPOSITS_TYPES + ")) , " +
							    
							"(SELECT " +
                                 "NVL(SUM (t.amount - t.internals_amount),0)  AS WITHDRAW_AMOUNT " +
                            "FROM " +
                                 "transactions t " +
						    "WHERE " +
                                 "t.user_id = ? " +
								 "AND  t.clearing_provider_id = ? " +
								 "AND ( t.status_id = 4 OR t.status_id = 9 )) "; //TRANS_STATUS_REQUESTED && TRANS_STATUS_APPROVED
                                 

			ps = con.prepareStatement(sql);

			ps.setLong(1, user_id);
			ps.setLong(2, provider_id);
			ps.setLong(3, user_id);
			ps.setLong(4, provider_id);

			rs = ps.executeQuery();

			if(rs.next()) {
				return rs.getLong("AVAILABLE_TO_WITHDRAW");
			}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return 0;
	}
}