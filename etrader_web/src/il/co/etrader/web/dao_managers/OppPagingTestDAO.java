package il.co.etrader.web.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.daos.DAOBase;

public class OppPagingTestDAO extends DAOBase {
    public static int getOpportunitiesCount(Connection conn) throws SQLException {
        int count = 0;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "COUNT(id) AS Count " +
                "FROM " +
                    "opportunities";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                count = rs.getInt("count");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return count;
    }
    
    public static ArrayList<Opportunity> getOpportunities(Connection conn, int startRow, int pageSize) throws SQLException {
        ArrayList<Opportunity> l = new ArrayList<Opportunity>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "(SELECT " +
                        "A.*, " +
                        "rownum AS rownumIn " +
                    "FROM " +
                        "(SELECT " +
                            "* " +
                        "FROM " +
                            "opportunities " +
                        "ORDER BY " +
                            "time_est_closing, " +
                            "market_id) A) " +
                "WHERE " +
                    "rownumIn >= ? AND rownumIn < ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, startRow + 1);
            pstmt.setInt(2, startRow + 1 + pageSize);
            rs = pstmt.executeQuery();
            Opportunity o = null;
            while (rs.next()) {
                o = new Opportunity();
                o.setId(rs.getLong("id"));
                o.setTimeActClosing(new Date(rs.getTimestamp("time_act_closing").getTime()));
                o.setClosingLevel(rs.getDouble("closing_level"));
                l.add(o);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
    }
}