package il.co.etrader.web.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.bl_vos.CachePages;

public class CachePagesDAO extends DAOBase {

	public static ArrayList<CachePages> getAllCachePages(Connection con)
			throws SQLException {
		ArrayList<CachePages> list = new ArrayList<CachePages>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = "SELECT * FROM CACHE_PAGES ";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				CachePages cp = new CachePages();
				cp.setId(rs.getLong("ID"));
				cp.setPageName(rs.getString("PAGE_NAME"));
				cp.setCacheTime(rs.getLong("CACHE_TIME"));
				list.add(cp);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return list;
	}

}
