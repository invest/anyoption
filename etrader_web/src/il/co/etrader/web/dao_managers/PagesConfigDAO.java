package il.co.etrader.web.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.web.util.Constants;

public class PagesConfigDAO extends DAOBase {

    public static ArrayList<String> getMinisitePages(Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<String> minisitePage = new ArrayList<String>();
        try {
            String sql =
            	"SELECT " +
            		"page_name " +
            	"FROM " +
            		"pages_config " +
            	"WHERE " +
            		"is_minisite = ?";
            ps = con.prepareStatement(sql);
            ps.setLong(1, Constants.EXISTS);

            rs = ps.executeQuery();
            while (rs.next()) {
                minisitePage.add(rs.getString("page_name"));
            }
        } finally {
        	closeResultSet(rs);
        	closeStatement(ps);
        }
        return minisitePage;
    }
}