
package il.co.etrader.web.mbeans;


import java.io.Serializable;
import java.util.ArrayList;

import org.apache.log4j.Logger;

public class InvestmentsList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1123051866477559551L;
	private static final Logger log = Logger.getLogger(InvestmentsList.class);
	private ArrayList list;

	public InvestmentsList() {
	}

	public ArrayList getList() {
		return list;
	}
	public int getSize() {
		return list.size();
	}

	public void setList(ArrayList list) {
		this.list = list;
	}


}
