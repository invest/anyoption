package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.OpportunitiesManager;
import il.co.etrader.web.bl_vos.Binary0100EventSelectionBean;

@SuppressWarnings("serial")
public class Binary0100EventSelectionForm implements Serializable {
	private static final Logger log = Logger.getLogger(Binary0100EventSelectionForm.class);

    public List<Binary0100EventSelectionBean> getEvents() throws SQLException {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> pm = fc.getExternalContext().getRequestParameterMap();
        long opportunityId = 0;
        if (null != pm.get("opportunityId")) {
            try {
                opportunityId = Long.parseLong(pm.get("opportunityId"));
            } catch (NumberFormatException nfe) {
                log.error("Can't parse opportunityId: " + nfe.getMessage());
            }
        }
        log.debug("getEvents - opportunityId: " + opportunityId);
        ApplicationData a = (ApplicationData) fc.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(fc);
        return OpportunitiesManager.getBinary0100PublishedEvents(a.getSkinId(), opportunityId);
    }
}