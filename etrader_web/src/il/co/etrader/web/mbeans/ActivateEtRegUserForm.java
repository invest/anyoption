/**
 * 
 */
package il.co.etrader.web.mbeans;

import java.io.IOException;
import java.sql.SQLException;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.util.ConstantsBase;

/**
 * @author pavelt
 *
 */
public class ActivateEtRegUserForm {
	private static final Logger logger = Logger.getLogger(ActivateEtRegUserForm.class);

	private String activationLink;

	public void unsuspendEtUser() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		activationLink = (String) request.getParameter(ConstantsBase.REG_HASH_KEY);

		if (activationLink == null || activationLink.equals("")) {
			logger.debug("Hash key param is null or empty!");
		} else {
			UserRegulationManager.unsuspendUserViaActivationLink(activationLink);
		}
		
		try {
			response.sendRedirect(context.getExternalContext().getRequestContextPath() + "/jsp/login.jsf");
			context.responseComplete();
		} catch (IOException e) {
			logger.debug("Error redirect from activation page.");
		}
	}

	public String getActivationLink() {
		return activationLink;
	}

	public void setActivationLink(String activationLink) {
		this.activationLink = activationLink;
	}
}
