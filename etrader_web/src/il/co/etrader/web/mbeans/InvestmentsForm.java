package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.myfaces.custom.tree2.TreeNode;
import org.apache.myfaces.custom.tree2.TreeNodeBase;

import com.anyoption.common.beans.Opportunity;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.InvestmentsManager;
import il.co.etrader.web.bl_vos.GameRank;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.helper.OpportunityTreeNodeBase;
import il.co.etrader.web.util.Constants;

public class InvestmentsForm implements Serializable {
	private static final long serialVersionUID = -2822814275062706023L;

	private static final Logger log = Logger.getLogger(InvestmentsForm.class);

//    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM HH:mm");

    private ArrayList opportunities;

    // Calcalist Game
    private ArrayList<GameRank> gameRankingList;
    private GameRank currUserRanking;
    private boolean isUserOutOfRanking;
    String rankingTableType;
    private ArrayList<GameRank> gameWinners;
    int weekOfTheMonth;


	private ArrayList getOpportunities() throws SQLException {
        if (null == opportunities) {
//            opportunities = InvestmentsManager.getMostPopularOpportunities();
        }
        return opportunities;
    }

	public String getInitRankingType() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		rankingTableType = request.getParameter("period");
		return null;
	}

    public String getGroup() throws SQLException {
        StringBuffer sb = new StringBuffer();
        ArrayList l = getOpportunities();
        String lastMarket = "";
        Opportunity o = null;
        for (int i = 0; i < l.size(); i++) {
            o = (Opportunity) l.get(i);
            if (!o.getMarket().getFeedName().equals(lastMarket)) {
                lastMarket = o.getMarket().getFeedName();
                if (i != 0) {
                    sb.append(", ");
                }
                sb.append("\"").append(lastMarket).append("\"");
            }
        }
        return sb.toString();
    }

    public ArrayList getTreesData() throws SQLException {
        ArrayList list = new ArrayList();
        ArrayList l = getOpportunities();
        long lastGroupId = -1;
        String lastMarket = "";
        Opportunity o = null;
        TreeNode crrGroup = null;
        TreeNodeBase crrMarket = null;
        TreeNodeBase node = null;
        for (int i = 0; i < l.size(); i++) {
            o = (Opportunity) l.get(i);
            if (o.getMarket().getGroupId().longValue() != lastGroupId) {
                lastGroupId = o.getMarket().getGroupId().longValue();
                crrGroup = new TreeNodeBase("root", String.valueOf(lastGroupId), false);
                list.add(crrGroup);
            }
            if (!o.getMarket().getFeedName().equals(lastMarket)) {
                lastMarket = o.getMarket().getFeedName();
                crrMarket = new OpportunityTreeNodeBase("market", o.getMarket().getDisplayName(), false, o);
                crrGroup.getChildren().add(crrMarket);
            } else {
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM HH:mm");
                node = new OpportunityTreeNodeBase("opportunity", dateFormat.format(o.getTimeEstClosing()), true, o);
                crrMarket.getChildren().add(node);
            }
        }
        return list;
    }


    public TreeNode getTreeData() throws SQLException {
        ArrayList list = new ArrayList();
        ArrayList l = getOpportunities();
        long lastGroupId = -1;
        String lastMarket = "";
        Opportunity o = null;
        TreeNode crrGroup = null;
        TreeNodeBase crrMarket = null;
        TreeNodeBase node = null;
       // log.log(Level.DEBUG, "List size: " + l.size());
        for (int i = 0; i < l.size(); i++) {
            o = (Opportunity) l.get(i);
            if (o.getMarket().getGroupId().longValue() != lastGroupId) {
                lastGroupId = o.getMarket().getGroupId().longValue();
                crrGroup = new TreeNodeBase("root", String.valueOf(lastGroupId), false);
                list.add(crrGroup);
            }
            if (!o.getMarket().getFeedName().equals(lastMarket)) {
                lastMarket = o.getMarket().getFeedName();
                crrMarket = new OpportunityTreeNodeBase("market", o.getMarket().getDisplayName(), false, o);
                crrGroup.getChildren().add(crrMarket);
            } else {
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM HH:mm");
                node = new OpportunityTreeNodeBase("opportunity", dateFormat.format(o.getTimeEstClosing()), true, o);
                crrMarket.getChildren().add(node);
            }
        }
        return (null != crrGroup ? crrGroup : new TreeNodeBase("root", "1", true));
    }

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "InvestmentsForm ( "
	        + super.toString() + TAB
	        + "opportunities = " + this.opportunities + TAB
	        + " )";

	    return retValue;
	}

    /*************************************************************/
    /////////////////////// Calcalist!!! //////////////////////////
    /*************************************************************/

	public ArrayList<GameRank> getShortGameRankingList() throws SQLException {
		return getGameRankingList(ConstantsBase.RANKING_TABLE_ROW_NUM_SHORT,null);
	}

	public ArrayList<GameRank> getFullWeekGameRankingList() throws SQLException {
		GregorianCalendar gc = new GregorianCalendar();
		SimpleDateFormat dateFormater = new SimpleDateFormat("dd/MM/yyyy");
		gc.set(GregorianCalendar.DAY_OF_WEEK, GregorianCalendar.SUNDAY);
		String fromDate = dateFormater.format(gc.getTime());

		return getGameRankingList(ConstantsBase.RANKING_TABLE_ROW_NUM_LONG,fromDate);
	}

	public ArrayList<GameRank> getFullMonthGameRankingList() throws SQLException {
		GregorianCalendar gc = new GregorianCalendar();
		SimpleDateFormat dateFormater = new SimpleDateFormat("dd/MM/yyyy");
		gc.set(GregorianCalendar.DAY_OF_MONTH, 1);
		String fromDate = dateFormater.format(gc.getTime());

		return getGameRankingList(ConstantsBase.RANKING_TABLE_ROW_NUM_LONG,fromDate);
	}

	public ArrayList<GameRank> getFinalGameRankingList() throws SQLException {
		GregorianCalendar gc = new GregorianCalendar();
		SimpleDateFormat dateFormater = new SimpleDateFormat("dd/MM/yyyy");
		gc.set(GregorianCalendar.DAY_OF_MONTH, 1);
		String fromDate = dateFormater.format(gc.getTime());
		GregorianCalendar gcT = new GregorianCalendar();
		String toDate = dateFormater.format(gcT.getTime());
		return InvestmentsManager.getGameUsersRanking(0, 50, fromDate, toDate);
	}

    private ArrayList<GameRank> getGameRankingList(int rankingRowsNum,String fromDate) throws SQLException {
    	FacesContext context=FacesContext.getCurrentInstance();


		GregorianCalendar gc = new GregorianCalendar();
		SimpleDateFormat dateFormater = new SimpleDateFormat("dd/MM/yyyy");
		String toDate = dateFormater.format(gc.getTime());

		if (null == fromDate){
			if (rankingTableType.equals(Constants.RANKING_TYPE_WEEKLY)){
				gc.set(GregorianCalendar.DAY_OF_WEEK, GregorianCalendar.SUNDAY);
			} else if (rankingTableType.equals(Constants.RANKING_TYPE_MONTHLY)){
				gc.set(GregorianCalendar.DAY_OF_MONTH, 1);
			}

			// Game start date in format DD/MM/YYYY.
			fromDate = dateFormater.format(gc.getTime());
		}


		long userId = 0;

		if (null != context.getExternalContext().getRemoteUser()){
			User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
			userId = user.getId();
		}

		gameRankingList = InvestmentsManager.getGameUsersRanking(userId, rankingRowsNum, fromDate, toDate);

		if (null != gameRankingList && gameRankingList.size() > rankingRowsNum){
			// if we got more records than what we asked it means that the current user isn't
			// is out of ranking range so he will be in the last place of the list.
			currUserRanking = gameRankingList.remove(gameRankingList.size()-1);
			if (currUserRanking.getUserId() == userId){
				isUserOutOfRanking = true;
			}
		}

		return gameRankingList;
	}

    /**
     * get the number of seconds for the next reload of the ranking table
     * refresh time is done at xx:31 and xx:01
     * @return
     */
    public long getSettSecForReload() {
    	Calendar now = Calendar.getInstance();
    	int min = now.get(Calendar.MINUTE);
		Calendar c = Calendar.getInstance();
    	if (min > 0 && min < 31) {   // refresh on 31 min
    		c.set(Calendar.MINUTE, 31);
    	} else {  // refresh on 1 min
    		c.set(Calendar.MINUTE, 1);
    		if (min != 0) {
    			c.set(Calendar.HOUR_OF_DAY, c.get(Calendar.HOUR_OF_DAY) + 1);
    		}
    	}
    	return ((c.getTimeInMillis() - now.getTimeInMillis()) / 1000);
    }

    public boolean isUserOutOfRanking(){
    	return isUserOutOfRanking;
    }

	public GameRank getCurrUserRanking() {
		return currUserRanking;
	}

    public String getInitWinners() throws SQLException {
    	FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();

		GregorianCalendar gc = new GregorianCalendar();
		SimpleDateFormat dateFormater = new SimpleDateFormat("dd/MM/yyyy");

		String inputWeek = request.getParameter("week");

		if (null != inputWeek && !inputWeek.equals(ConstantsBase.EMPTY_STRING)){
			try {
				weekOfTheMonth = Integer.parseInt(inputWeek);
			} catch (Exception e) {
				log.log(Level.ERROR, "Error parsing week parameter " + e);
				weekOfTheMonth = getGameWeekForDisplay();
			}
		}else{
			weekOfTheMonth = getGameWeekForDisplay();
		}

		gc.set(GregorianCalendar.MONTH, GregorianCalendar.NOVEMBER);
		gc.set(GregorianCalendar.WEEK_OF_MONTH, weekOfTheMonth);
		gc.set(GregorianCalendar.DAY_OF_WEEK, GregorianCalendar.SUNDAY);

		String fromDate = dateFormater.format(gc.getTime());

		// Add only 6 days because the sql adds 1 to the end date
		gc.add(GregorianCalendar.DATE, 6);
		String toDate = dateFormater.format(gc.getTime());

		gameWinners = InvestmentsManager.getGameUsersRanking(0, ConstantsBase.WINNERS_NUM, fromDate, toDate);

		return null;
	}

    public GameRank getFirstPlace(){
    	GameRank rank;
    	if (gameWinners.size() < ConstantsBase.WINNERS_NUM ){
    		rank = new GameRank();
    		rank.setUserNameFull(CommonUtil.getMessage("calcalist.no.winner.yet", null));
    		rank.setWinSum("0");
    	} else {
    		rank = gameWinners.get(0);
    	}
    	return rank;
    }

    public GameRank getSecondPlace(){
    	GameRank rank;
    	if (gameWinners.size() < ConstantsBase.WINNERS_NUM ){
    		rank = new GameRank();
    		rank.setUserNameFull(CommonUtil.getMessage("calcalist.no.winner.yet", null));
    		rank.setWinSum("0");
    	} else {
    		rank = gameWinners.get(1);
    	}
    	return rank;
    }

    public GameRank getThirdPlace(){
    	GameRank rank;
    	if (gameWinners.size() < ConstantsBase.WINNERS_NUM ){
    		rank = new GameRank();
    		rank.setUserNameFull(CommonUtil.getMessage("calcalist.no.winner.yet", null));
    		rank.setWinSum("0");
    	} else {
    		rank = gameWinners.get(2);
    	}
    	return rank;
    }

	/**
	 * @return the weekOfTheMonth
	 */
	public int getWeekOfTheMonth() {
		return weekOfTheMonth;
	}

	/**
	 * @return the Game week
	 */
	public int getGameWeek() {
		int gameWeek;
		GregorianCalendar gcToday = new GregorianCalendar();

		// if the game ended
		if (isGameEnded()) {
			// set game week to last week
			gameWeek = ConstantsBase.GAME_LENGTH_WEEKS;
		} else {
			gameWeek = gcToday.get(GregorianCalendar.WEEK_OF_MONTH);
		}

		return gameWeek;
	}

	/**
	 * @return the Game week
	 */
	public boolean isGameEnded() {
		GregorianCalendar gcToday = new GregorianCalendar();
		GregorianCalendar gcGameEnd = new GregorianCalendar();

		// Set date to the game start date.
		gcGameEnd.set(GregorianCalendar.YEAR, 2009);
		gcGameEnd.set(GregorianCalendar.MONTH, GregorianCalendar.NOVEMBER);
		gcGameEnd.set(GregorianCalendar.DAY_OF_MONTH, 28);
		gcGameEnd.set(Calendar.HOUR_OF_DAY, 0);
		gcGameEnd.set(Calendar.MINUTE, 0);
		gcGameEnd.set(Calendar.SECOND, 0);

		// if the game ended
		if (gcToday.getTime().after(gcGameEnd.getTime())) {
			return true;
		}

		return false;
	}

	/**
	 * @return the Game week
	 */
	public int getGameWeekForDisplay() {
		int gameWeek;
		GregorianCalendar gcToday = new GregorianCalendar();

		// if the game ended
		if (isGameEnded()) {
			// set game week to last week
			gameWeek = ConstantsBase.GAME_LENGTH_WEEKS;
		} else {
			gameWeek = gcToday.get(GregorianCalendar.WEEK_OF_MONTH) - 1;
		}

		return gameWeek;
	}

}