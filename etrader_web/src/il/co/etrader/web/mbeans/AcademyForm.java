package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.net.URLDecoder;
import java.sql.SQLException;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;

public class AcademyForm implements Serializable {

	private static final long serialVersionUID = 7539895288982811114L;
	private static final Logger log = Logger.getLogger(AcademyForm.class);
	
	private long userStatus;

	public long getUserStatus() {
		FacesContext context = FacesContext.getCurrentInstance();
		User loggedUser = context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER, User.class);
		try {
			if (null == loggedUser || loggedUser.getId() == 0) {
				this.userStatus = Constants.ACADEMY_USER_STATUS_LOGOUT;
			} else {
				if (loggedUser.isHasDeposits()){
					this.userStatus = Constants.ACADEMY_USER_STATUS_HAS_DEPOSIT;
				} else {
					this.userStatus = Constants.ACADEMY_USER_STATUS_NO_DEPOSIT;
				}
			}
		} catch (SQLException sqle) {
			log.debug("Error getting user status: " + sqle);
		}
		return userStatus;
	}
	
	public String decodeURL(HttpSession session, HttpServletRequest request, HttpServletResponse response, String inputURL) throws Exception {
		return URLDecoder.decode(PageNames.getPageNameStatic(ApplicationData.getLinkLocalePrefix(session, request, response), inputURL), "UTF-8");
	}
	
	public String replaceTextOut(String inputText) {
		return inputText.replace("'", "`");
	}
	
	public String replaceTextIn(String inputText) {
		return inputText.replace("`", "'");
	}
}
