package il.co.etrader.web.mbeans;

import java.io.Serializable;

import javax.faces.model.DataModel;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;

import il.co.etrader.util.DataPage;
import il.co.etrader.util.PagedListDataModel;
import il.co.etrader.web.bl_managers.OppPagingTestManager;

public class OppPagingTestForm implements Serializable {
    private static final Logger log = Logger.getLogger(OppPagingTestForm.class);
    
    private static final long serialVersionUID = 1L;

    private DataModel opportunities;
    // Due to the JSF Life Cycle the getDataPage is called twice in "Restore View" phase and
    // in the "Render Response" one. So to save one "going to the db" at the expense of some
    // mem we can cache the data page
    private DataPage<Opportunity> dataPage;
    
    private DataPage<Opportunity> getDataPage(int startRow, int pageSize) {
        if (log.isDebugEnabled()) {
            log.debug("getDataPage - startRow: " + startRow + " pageSize: " + pageSize);
        }
        if (null == dataPage || dataPage.getStartRow() != startRow) {
            if (log.isDebugEnabled()) {
                log.debug("loading page from db...");
            }
            try {
                dataPage = OppPagingTestManager.getOpportunities(startRow, pageSize);
            } catch (Throwable t) {
                log.error("Can't load the page.", t);
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Cached dataPage.");
            }
        }
        return dataPage;
    }

    public DataModel getDataModel() {
        if (log.isDebugEnabled()) {
            log.debug("getDataModel");
        }
        if (null == opportunities) {
            opportunities = new LocalDataModel(10);
        }
        return opportunities;
    }

    private class LocalDataModel extends PagedListDataModel<Opportunity> {
        public LocalDataModel(int pageSize) {
            super(pageSize);
        }

        public DataPage<Opportunity> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPage(startRow, pageSize);
        }
    }
}