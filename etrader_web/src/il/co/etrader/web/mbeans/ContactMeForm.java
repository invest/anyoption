package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.anyoption.common.util.MarketingTracker;

import il.co.etrader.bl_managers.ContactsManager;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.MarketingCombination;
import il.co.etrader.bl_vos.MarketingPixel;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.SendContactEmail;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.UsersManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;

public class ContactMeForm implements Serializable {

	private static final long serialVersionUID = 7685421236133168439L;
	private static final Logger logger = Logger.getLogger(ContactMeForm.class);

	private String contactPrivateName;
	private String contactLastName;
    private String contactName;
    private String email;
    private String mobilePhone;
    private String mobilePhonePrefix;
    private boolean isValidBanner;
    private String skinId;
	private Long combId;
	private String combIdTxt;
	private String dynamicParameter;
	private String aff_sub1;
	private String aff_sub2;
	private String aff_sub3;
	private String language;
	private String code;
	private Skins skin;
	private ArrayList<SelectItem> countries;
	private Long countryId;
	private boolean fromIframe;

    public ContactMeForm() throws SQLException{

		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
		Locale locale = ap.getUserLocale();

    	// default values for anyoption
    	skin = ApplicationData.getSkinById(ap.getSkinId());
    	setSkinId(String.valueOf(skin.getId()));
    	String countryIdStr = null;
    	ArrayList<SelectItem> countriesFromApp = new ArrayList<SelectItem>();

    	// get combination name
		this.combId = UsersManager.getCombinationId(req);
		logger.log(Level.DEBUG, "get combinationId for email = " + combId);
		if ( null == combId ) {
			combId = skin.getDefaultCombinationId();
			logger.log(Level.DEBUG, "combId is null, take default value from skin: " + combId);
		}

		dynamicParameter = UsersManager.getDynamicParam(req);
		aff_sub1 = CommonUtil.getRequestParameter(req, Constants.AFF_SUB1);
		aff_sub2 = CommonUtil.getRequestParameter(req, Constants.AFF_SUB2);
		aff_sub3 = CommonUtil.getRequestParameter(req, Constants.AFF_SUB3);
		code="";
		
		if ( !CommonUtil.isHebrewSkin(skin.getId())) {   // just for AO
			countryIdStr = ap.getRealCountryId();
    		boolean takeFromSkin = false;

    		if ( null != countryIdStr && !countryIdStr.equals("0")) {  // found country
    			countryId = Long.valueOf(countryIdStr);
				code = ap.getCountryPhoneCodes(countryIdStr);
				if ( null == code || code.length() == 0 ) {  // phone code not exists
						takeFromSkin = true;
				}
    		} else {
    			takeFromSkin = true;
    		}

    		if ( takeFromSkin ) {   // problem to extract country phone code from ip
    			countryId = skin.getDefaultCountryId();
        		countryIdStr = String.valueOf(countryId);
    	    	code = ap.getCountryPhoneCodes(countryIdStr);
    		}

    		if(req.getRequestURL().indexOf("minisite") > -1){
    			contactName =  CommonUtil.getMessage("contact.firstname", null, locale);
    			email =  CommonUtil.getMessage("contact.email", null, locale);
    			mobilePhone = code;
    		}
	    	//mobilePhone = code;   // put the code in the field

    		// get language
    		language = ap.getUserLocale().getDisplayLanguage();

    		countriesFromApp.addAll(ap.getCountriesList());
    		
    	} else if (skin.getId() == Skin.SKIN_TLV){ //TLV skin
    		countryId = Constants.ISRAEL_COUNTRY_ID	;
    		contactName = CommonUtil.getMessage("banner.contactme.name",null);
    		mobilePhone = CommonUtil.getMessage("banner.contactme.phone", null);
    	} else { //Etrader _ User From Israel
    		countryId = Constants.ISRAEL_COUNTRY_ID	;
    		mobilePhone = "";
    	}
		
		if ((context.getViewRoot().getViewId().indexOf("contactmeHP") == -1 && context.getViewRoot().getViewId().indexOf("landingCallMe") == -1) || context.getViewRoot().getViewId().indexOf("landingCallMeNew") > -1 ) { // not for homePage/landing register callMe
	    	contactName = CommonUtil.getMessage("banner.contactme.name",null);
	    	email = CommonUtil.getMessage("banner.contactme.email", null);
	    	mobilePhone = CommonUtil.getMessage("banner.contactme.phone", null);
		}

		//For call me Iframe , we want to take the combId from the request
		if ((context.getViewRoot().getViewId().indexOf("callmeIframe") > -1)) {
		    String combIdRequest = req.getParameter(Constants.COMBINATION_ID);
		    if (!CommonUtil.isParameterEmptyOrNull(combIdRequest)) {
		    	this.combId = Long.valueOf(combIdRequest);
		    }
		    fromIframe = true;
		}
    	//ArrayList<SelectItem> countTemp = new ArrayList<SelectItem>();

		if(req.getRequestURL().indexOf("contactMeHPSmallAllLanguages") > -1) { //anyoption new design
			User user = null;
			try {
				user = (User)req.getSession().getAttribute(Constants.BIND_SESSION_USER);
			} catch (Exception e) {
				logger.error("Exception while trying to take user from the session" + e);
			}
			if (null != user && user.getId() != 0) { // if user logged in fill his details
				contactName = user.getFirstName();
				email = user.getEmail();
				
				String mobileRaw = user.getMobilePhone();
				if (CommonUtil.isHebrewSkin(skin.getId())
						&& mobileRaw != null
						&& mobileRaw.length() > 7) {
					String prefix = mobileRaw.startsWith("0")
												? mobileRaw.substring(0, 3)
												: "0" + mobileRaw.substring(0, 2);
					if (ApplicationData.getMobilePrefPlain().contains(prefix)) {
						mobilePhone = mobileRaw.substring(prefix.length());
						mobilePhonePrefix = prefix;
					} else {
						mobilePhone = mobileRaw;
					}
				} else {
					mobilePhone = mobileRaw;
				}
				
				countryId = user.getCountryId();
			} else { //logout user
				if (!CommonUtil.isParameterEmptyOrNull(contactName)) {
					contactName = contactName.toUpperCase();
				}
				if (!CommonUtil.isParameterEmptyOrNull(email)) {
					email =  email.toUpperCase();
				}
				if (!CommonUtil.isParameterEmptyOrNull(mobilePhone)) {
					mobilePhone = mobilePhone.toUpperCase();
				}
			}
		}

    	this.setCountries(countriesFromApp);
    }


    class AlphaComparator implements Comparator<SelectItem> {

        // Comparator interface requires defining compare method.
        public int compare(SelectItem a, SelectItem b) {
            //	  Sort countries in alphabetical order considering user's Locale
            //
        	String aLabel = a.getLabel();
        	String bLabel = b.getLabel();
        	int returnVal=0;

        	FacesContext context = FacesContext.getCurrentInstance();
        	ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);

        	try{
        		Collator collator = Collator.getInstance(ap.getUserLocale());
        		returnVal = collator.compare(aLabel, bLabel);
        	}catch(SQLException e){
        		logger.fatal("Error in sorting countriesList :"+e.getMessage());
        	}

        	return returnVal;
        }
    }


	/**
	 * Send contactme email
	 * For etrader
	 * @return
	 * 		String Navigation page
	 */
	public String sendContactEmail() {
		return sendContactEmail(Constants.NAV_CONTACTME_SUCCESS);
	}

	/**
	 * Send contactme email
	 * For etrader
	 * @return
	 * 		String Navigation page
	 */
	public String sendContactEmail(String navigateToPage) {

		try {

			if ( phoneValidationEtrader("error.name.letters") == false ) {   // validation problem
				return null;
			}
			Contact contactReq = new Contact();

			//for etrader populate phone with prefix
			contactReq.setPhone(this.mobilePhonePrefix + this.mobilePhone );
			contactReq.setType(Contact.CONTACT_ME_TYPE);
			contactReq.setWriterId(CommonUtil.getWebWriterId());
			contactReq.setText(this.contactName);
			contactReq.setCombId((this.combId).longValue());
			contactReq.setCountryId(Constants.COUNTRY_ID_IL);
			if (skin.getId() == Skin.SKIN_ETRADER){
				contactReq.setSkinId(Skin.SKIN_ETRADER);
			} else {
				contactReq.setSkinId(Skin.SKIN_TLV);
			}
			contactReq.setDynamicParameter(this.dynamicParameter);
			contactReq.setAff_sub1(aff_sub1);
			contactReq.setAff_sub2(aff_sub2);
			contactReq.setAff_sub3(aff_sub3);
			contactReq.setSkinId(Long.valueOf(skinId));
			contactReq.setCountryId(countryId);
			if (fromIframe) {
				contactReq.setEmail(email);
			}
			insertNewContact(contactReq);
			isValidBanner = true;  			//for off hours banner

			return navigateToPage;  	 //mean that all validate are good

		} catch (Exception e) {
			logger.error("Could not insert contact record.", e);
		}

		return null;
	}


	/**
	 * Send contactme email
	 * For anyoption
	 * @return
	 * 		String Navigation page
	 */
	public String sendContactEmailAnyoption(String successPage) {
		try {
			if ( phoneValidationAnyoption() == false ) {   // validation problem
				return null;
			}
	        isValidBanner = true;
			Contact contactReq = new Contact();
			//we need to take and store only mobile number,without the prefix but we get whole number from form
			if (skin.getId() != Skin.SKIN_ETRADER){
			contactReq.setPhone(this.mobilePhone.substring(this.mobilePhone.indexOf("-", 3)+1,this.mobilePhone.length()));
			}else {
			contactReq.setPhone(this.mobilePhonePrefix + this.mobilePhone );
			//successPage = Constants.NAV_CONTACTME_SUCCESS;
			}
			contactReq.setType(Contact.CONTACT_ME_TYPE);
			contactReq.setEmail(this.email);
			contactReq.setCountryId(countryId);
			contactReq.setSkinId(Long.parseLong(skinId));
			contactReq.setText(this.contactName);
			contactReq.setCombId((this.combId).longValue());
			contactReq.setWriterId(CommonUtil.getWebWriterId());
			contactReq.setDynamicParameter(this.dynamicParameter);
			contactReq.setAff_sub1(aff_sub1);
			contactReq.setAff_sub2(aff_sub2);
			contactReq.setAff_sub3(aff_sub3);
			FacesContext context = FacesContext.getCurrentInstance();
			HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
			contactReq.setDfaPlacementId(UsersManager.getDFAPlacementId(req));
			contactReq.setDfaCreativeId(UsersManager.getDFACreativeId(req));
			contactReq.setDfaMacro(UsersManager.getDFAMacro(req));

			insertNewContact(contactReq);

			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			response.sendRedirect(successPage + "?phone="+mobilePhone);
		} catch (Exception e) {
			logger.error("Could not insert new contact record.", e);
		}
		return null;
	}

	public String sendContactEmailAnyoptionBanner() {
		return sendContactEmailAnyoption("contactme_success.jsf");
	}

	public String sendContactEmailAnyoptionMinisite() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		Locale locale = ap.getUserLocale();
		if(contactName.equalsIgnoreCase(CommonUtil.getMessage("contact.firstname", null, locale)) ){
			String[] params = new String[1];
			params[0] = CommonUtil.getMessage("register.firstname",null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.required", params),null);
			context.addMessage("contactMeForm:firstName", fm);
			return null;
		}
		if(email.equalsIgnoreCase(CommonUtil.getMessage("contact.email", null, locale))){
			String[] params = new String[1];
			params[0] = CommonUtil.getMessage("register.email",null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.required", params),null);
			context.addMessage("contactMeForm:email", fm);
			return null;
		}
		if(!CommonUtil.isEmailValidUnix(email)){
			String[] params = new String[1];
			params[0] = CommonUtil.getMessage("register.email",null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.email", params),null);
			context.addMessage("contactMeForm:email", fm);
			return null;
		}
		if(CommonUtil.isParameterEmptyOrNull(mobilePhone)){
			String[] params = new String[1];
			params[0] = CommonUtil.getMessage("register.mobilephone",null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.required", params),null);
			context.addMessage("contactMeForm:mobilePhone", fm);
			return null;
		}
		if(!mobilePhone.matches("\\d+")){ 
			String[] params = new String[1];
			params[0] = CommonUtil.getMessage("register.mobilephone",null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.phone", params),null);
			context.addMessage("contactMeForm:mobilePhone", fm);
			return null;
		}
		if(mobilePhone.length() < 7 || mobilePhone.length() > 14){
			String[] params = new String[1];
			params[0] = CommonUtil.getMessage("register.mobilephone",null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.quick.start.mobile.phone", params),null);
			context.addMessage("contactMeForm:mobilePhone", fm);
			return null;
		}
		return sendContactEmailAnyoption("ContactMeSuccess.jsf");
	}

	public String sendEmailAnyoptionCallMeHP() {
		return sendContactEmailAnyoption("contactmeSuccessHP.jsf");
	}

    public String sendEmailAnyoptionCallMeHPSmall() {
        return sendContactEmailAnyoption("contactme_success_small_hp.jsf");
    }

    public String sendEmailAnyoptionCallMeLanding() {
        return sendContactEmailAnyoption("landingCallMeSuccess.jsf");
    }

    public String sendEmailAnyoptionCallMeLandingNew() {
        return sendContactEmailAnyoption("landingCallMeSuccessNew.jsf");
    }

	/**
	 * Insert contact
	 * @param contactReq Contact instance
	 * @throws SQLException
	 */
	private void insertNewContact(Contact contactReq) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);		
		Contact contact = null;
		PopulationEntryBase popUser = null;
		long contactId = ContactsManager.isExist(contactReq.getPhone(), contactReq.getEmail());

		long userId = 0;

		HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
		User user = null;
		if (req != null && req.getRequestURL().indexOf("contactMeHPSmallAllLanguages") > -1) { // anyoption new design 
			try {
				user = (User)req.getSession().getAttribute(Constants.BIND_SESSION_USER);
			} catch (Exception e) {
				logger.error("Exception while trying to take user from the session" + e);
			}
			if (null != user && user.getId() != 0) { // user logged in. get his userId
				userId = user.getId();
			}
		}
		if (userId == 0) {
			userId = UsersManager.getUserIdByPhoneEmail(contactReq.getPhone(), contactReq.getEmail(), contactReq.getCountryId());
		}

        contactReq.setIp(CommonUtil.getIPAddress());

		if (userId != 0) {
			popUser =  PopulationsManagerBase.getPopulationUserByUserId(userId);
		}
        		
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        try {
            MarketingTracker.checkExistCookieWhenInsertContact(request, response, contactReq.getEmail(), contactReq.getPhone());
        } catch (Exception e) {
            logger.error("Marketing Tracker Check exist Cookie smt", e);
        }

		if (contactId == 0) { // new contact
			contactReq.setUserId(userId);
			contact = ContactsManager.insertContactRequest(contactReq, false, true, user, null);
			contactId = contact.getId();  // for storing contactID later in cookie
			
			MarketingTracker.insertMarketingTracker(request, response, contactId, userId, Constants.MARKETING_TRACKING_CALL_ME);
			//MarketingETS.etsMarketingActivity(request, response, contactId, Constants.MARKETING_TRACKING_CALL_ME, contactReq.getEmail(), contactReq.getMobilePhone(), null, null, null, null, contactReq.getIp());
			
			try {
				if(contact.getSkinId() == Skin.SKIN_CHINESE){
					sendEmail("contact.first.email.zh", contactId);
				}else{
					sendEmail("contact.first.email",contactId);
				}
			} catch (Exception e) {
				logger.error("Could not send email.", e);
			}

			//Disable saving cookie on etrader Iframe - for security reasons when saveing cookie on IE with JSF on IFRAME
			if (!fromIframe) {				
				Cookie contactCookie = new Cookie(Constants.CONTACT_ID,contactId+"");
				contactCookie.setPath("/");
				contactCookie.setMaxAge(365 * 24 * 60 * 60 * 1000); // about year
				//sub domain
				String property = CommonUtil.getProperty(ConstantsBase.COOKIE_DOMAIN, null);
				if (null != property) {
					contactCookie.setDomain(property);
				}
				response.addCookie(contactCookie);
			}

		} else {   // existing contact, add update event
			if (userId != 0) {
				logger.info("going to update contactId: " + contactId + " with userId: " + userId);
				ContactsManager.updateContactRequest(contactId, userId);
			}
			if (null == popUser){
				popUser = PopulationsManagerBase.getPopulationUserByContactId(contactId);
			}
		}

		if (null != popUser){
			PopulationsManagerBase.updateContactMeEvent(popUser, contactId, ap.getSkinId(),userId,CommonUtil.getWebWriterId());
		}else{
			PopulationsManagerBase.insertIntoPopulation(contactId, ap.getSkinId(),userId,null,CommonUtil.getWebWriterId(), PopulationsManagerBase.POP_TYPE_CALLME, null);
		}
	}

	/**
	 * Send email function
	 * @param emailParam
	 * 		email property
	 * @throws Exception
	 */
	public void sendEmail(String emailParam,long contactId) throws Exception {

		HashMap<String, String> params = null;

		// collect the parameters we neeed for the email
		params = new HashMap<String, String>();
		String mobilePhoneMsg = mobilePhone;

		if (mobilePhonePrefix != null) {
			mobilePhoneMsg = mobilePhonePrefix + mobilePhoneMsg ;
		}

		params.put(SendContactEmail.PARAM_EMAIL, email);
		params.put(SendContactEmail.PARAM_FIRST_NAME, CommonUtil.capitalizeFirstLetters(contactName));
		params.put(SendContactEmail.PARAM_MOBILE, mobilePhoneMsg);
		if(contactId != 0){
			params.put(SendContactEmail.PARAM_CONTACT_ID, contactId+"");
		}
		FacesContext context = FacesContext.getCurrentInstance();
    	ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
    	if(countryId != null && countryId != 0) { //fix for ET
    		params.put(SendContactEmail.PARAM_COUNTRY, CommonUtil.getMessage(ap.getCountry(countryId).getDisplayName(), null));
    	}

		// Populate also server related params, in order to take out common functionality
		// Use params

		//String subject = Constants.CONTACT_ME_SUBJECT + combId;
		String subject = Constants.CONTACT_ME_SUBJECT;

		/*if (dynamicParameter != null){
			subject += ", " + Constants.CONTACT_ME_DYNMAIC_PARAM + dynamicParameter;
		}*/

		subject += ", " + 	Constants.CONTACT_ME_SUBJECT_LANG;
		if (!CommonUtil.isParameterEmptyOrNull(language)) {
			subject += language;
		}
		else {
			subject += Constants.CONTACT_ME_LANG_ISRAEL;
		}

		params.put(SendContactEmail.MAIL_FROM, CommonUtil.getProperty(emailParam, null));
		params.put(SendContactEmail.MAIL_TO, CommonUtil.getProperty(emailParam, null));
		params.put(SendContactEmail.MAIL_SUBJECT ,subject);

		if ( null != skin && !CommonUtil.isHebrewSkin(skin.getId())) {
			params.put(SendContactEmail.SUCCESS_MESSAGE, CommonUtil.getMessage("contact.mail.success", null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
		} else {
			params.put(SendContactEmail.SUCCESS_MESSAGE, CommonUtil.getMessage("contact.mail.success", null));
		}

		params.put(SendContactEmail.RETURN_PAGE, Constants.NAV_EMPTY_SSL);

		new SendContactEmail(params).start();

	}

	/**
	 * Send contact email for campaign
	 * @param responseStr
	 * 		String to response
	 * @param emailParam
	 * 		email parameter
	 * @param subjectParam
	 * 		subject parameter
	 * @param needCookie
	 * 		if save cookie needed
	 * @param cookieValue
	 * 		value of the referer cookie
	 * @return
	 */
	public String sendContactEmailCampaign(String responseStr, String emailParam,
			boolean needCookie, String cookieValue) {

		FacesContext context = FacesContext.getCurrentInstance();

		try {

			if ( phoneValidationEtrader("error.name.letters.capmaign") == false ) {   // validation problem
				return null;
			}

	        sendEmail(emailParam,0);

	        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();

//	        if ( needCookie ) {    comment on combination development
//	        	// prepare the combination cookie
//				Cookie cookie = new Cookie(Constants.COMBINATION_ID,cookieValue);
//				cookie.setPath("/");
//				cookie.setMaxAge(365 * 24 * 60 * 60 * 1000); // about year
//				response.addCookie(cookie);
//	        }

	        response.sendRedirect(responseStr);

		} catch (Exception e) {
			logger.error("Could not send email.", e);
		}

		return null;
	}


	/**
	 * Phone number validation
	 * For etrader, with phone prefix
	 *
	 * @param errorNameKey
	 * 		the error key for display
	 * @return
	 * 		false - validation error
	 */
	public boolean phoneValidationEtrader(String errorNameKey) {

		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage fm = null;
		String formName = "contactMeForm:";
		String phoneField = "myPhoneET";
		String phonePrefixField = "mobilePhonePref";

		if (validateName(errorNameKey, "banner.contactme.name", this.contactName) == null) {
			return false;
		}

        if (CommonUtil.isParameterEmptyOrNull(mobilePhonePrefix)) {
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Phone required.",
                    CommonUtil.getMessage("error.single.required", new Object[] {CommonUtil.getMessage("banner.contactme.phonePrefix", null)}));
			context.addMessage(formName + phonePrefixField, fm);

            return false;
		}

        if (CommonUtil.isParameterEmptyOrNull(mobilePhone)) {
        	 fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                     "Phone required.",
                     CommonUtil.getMessage("error.single.required", new Object[] {CommonUtil.getMessage("banner.contactme.phone", null)}));
        	 context.addMessage(formName + phoneField, fm);

             return false;
		}

        if (!mobilePhone.matches("^[0-9]{7}$")) {
        	logger.error("validation mobile failed");
            fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                      "Invalid phone.",
                      CommonUtil.getMessage("error.banner.contactme.phone", null));
            context.addMessage(formName + phoneField, fm);

            return false;
         }

        return true;
	}

	/**
	 * Phone number validation
	 * For anyoption, with phone code by country
	 * @return
	 * 		false - validation error
	 */
	public boolean phoneValidationAnyoption() {

		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage fm = null;
		String formName = "contactMeForm:";
		String phoneField = "myPhone" + ((skin.getId() == Skins.SKIN_ETRADER) ? "ET" : "");
		String nameField = "contactName";
		String emailField = "email";
		String countryField = "countriesList";

		if ( CommonUtil.isParameterEmptyOrNull(contactName) || contactName.equals(CommonUtil.getMessage("banner.contactme.name",null))) {
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Phone required.",
                    CommonUtil.getMessage("error.banner.contactme.required", new Object[] {CommonUtil.getMessage("banner.contactme.name", null)}));
			context.addMessage(formName + nameField, fm);

			return false;
		}

		if ((null == countryId || null != countryId && countryId.longValue() == 0)) {
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Country required.",
                    CommonUtil.getMessage("error.banner.contactme.required", new Object[] {CommonUtil.getMessage("banner.contactme.country", null)}));
			context.addMessage(formName + countryField, fm);

			return false;
		}

		// handle phone number
		String phone = mobilePhone;
		if ( phone.indexOf ("-") > -1 ) {   // input with phoneCode display
			int idx = phone.lastIndexOf("-");
			phone = phone.substring(idx+1);
		}

        if (CommonUtil.isParameterEmptyOrNull(phone) || phone.equals(CommonUtil.getMessage("banner.contactme.phone", null))) {
       	 fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Phone required.",
                    CommonUtil.getMessage("error.banner.contactme.required", new Object[] {CommonUtil.getMessage("banner.contactme.phone", null)}));
       	 context.addMessage(formName + phoneField , fm);

            return false;
		}

        if (CommonUtil.isParameterEmptyOrNull(phone) || !phone.matches("^[0-9]{7,}$")) {
            fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Invalid phone.",
                    CommonUtil.getMessage("error.banner.contactme.phone", null));
          context.addMessage(formName + phoneField, fm);

          return false;
		}

        if (CommonUtil.isParameterEmptyOrNull(email)) {
          	 fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                       "Email required.",
                       CommonUtil.getMessage("error.banner.contactme.required", new Object[] {CommonUtil.getMessage("banner.contactme.email", null)}));
          	 context.addMessage(formName + emailField, fm);

               return false;
   		}

        return true;
	}


	public String sendContactEmailCampaign() {
		return sendContactEmailCampaign("landing_campaign_success.jsf?phone="+getPhoneNumberStr(),
				"contact.first.email.campaign", false, null);
	}


	public String sendContactEmailStudentsCampaign() {
	   return sendContactEmailCampaign("landing_campaign_st_success.jsf",
			   "contact.students.campaign.email", false, null);
	}

//	public String sendContactEmailADMFastCampaign() {
//	        return sendContactEmailCampaign("landing_campaign_adm_fast_success.jsf?phone="+getPhoneNumberStr(), "contact.admfast.campaign.email",
//	        		 true, String.valueOf(ConstantsBase.ADS_MARKET_LEAD_FAST));
//	}

//	public String sendContactEmailADMDreamCampaign() {
//        return sendContactEmailCampaign("landing_campaign_adm_dream_success.jsf?phone="+getPhoneNumberStr(), "contact.admdream.campaign.email",
//        		true, String.valueOf(ConstantsBase.ADS_MARKET_LEAD_DREAM));
//	}

//	public String sendContactEmailDSNRFastCampaign() {
//        return sendContactEmailCampaign("landing_campaign_adm_dream_success.jsf", "contact.dsnrfast.campaign.email",
//        		true, String.valueOf(ConstantsBase.DSNR_LEAD_FAST));
//	}

//	public String sendContactEmailDSNRDreamCampaign() {
//        return sendContactEmailCampaign("landing_campaign_dsnr_dream_success.jsf", "contact.dsnrdream.campaign.email",
//        		true, String.valueOf(ConstantsBase.DSNR_LEAD_DREAM));
//	}

//	public String sendContactEmailWSFastCampaign() {
//        return sendContactEmailCampaign("landing_campaign_ws_fast_success.jsf", "contact.wesellfast.campaign.email",
//        		true, String.valueOf(ConstantsBase.WESELL_LEAD_FAST));
//	}

//	public String sendContactEmailWSDreamCampaign() {
//	       return sendContactEmailCampaign("landing_campaign_ws_dream_success.jsf", "contact.weselldream.campaign.email",
//	        		true, String.valueOf(ConstantsBase.WESELL_LEAD_DREAM));
//	}

	/**
	 * return the entire phone number
	 */
	public String getPhoneNumberStr() {

		String mobilePhoneMsg = mobilePhone;
		if (mobilePhonePrefix != null) {
			mobilePhoneMsg = mobilePhonePrefix + mobilePhoneMsg ;
		}
		return mobilePhoneMsg;

	}


	/**
	 * validate the name
	 * Return null if its not good and 1 if its good
	 */
	public String validateName(String errorKey, String nameType, String nameField) {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
            // Validate Input parameters, according to display order in the screen
			// Phone, Name, Email
            FacesMessage fm = null;

	        if (CommonUtil.isParameterEmptyOrNull(nameField)) {
	            fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
	                    "Name required.",
	                    CommonUtil.getMessage("error.single.required", new Object[] {CommonUtil.getMessage(nameType, null)}));
	            context.addMessage(null, fm);

	            return null;
	        }

	        String pattern = CommonUtil.getMessage("general.validate.letters.only", null);
	        if (!nameField.matches(pattern)) {
	            fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
	                    "Invalid name.",
	                    CommonUtil.getMessage(errorKey, null));
	            context.addMessage(null, fm);

	            return null;
	        }
	        //mean all validates are good
	        return "1";
		} catch (Exception e) {
			logger.error("Could not send email.", e);
		}

		return null;
	}

	public String validateMail() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
            // Validate Input parameters, according to display order in the screen
			// Phone, Name, Email
            FacesMessage fm = null;

	        if (CommonUtil.isParameterEmptyOrNull(email)) {
	            fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
	                    "Mail required.",
	                    CommonUtil.getMessage("error.single.required", new Object[] {CommonUtil.getMessage("contact.email", null)}));
	            context.addMessage(null, fm);

	            return null;
	        }

	        if (!CommonUtil.isEmailValidUnix(email)) {
	        	 fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
		                    "Invalid name.",
		                    CommonUtil.getMessage("error.banner.contactme.email", null));
		            context.addMessage(null, fm);

		            return null;
	        }

	        //mean all validates are good
	        return "1";
		} catch (Exception e) {
			logger.error("Could not send email.", e);
		}

		return null;
	}

	/**
	 * Get phone code string
	 * This code is set in the constructor
	 * @return
	 * 		phone code String
	 */
	public String getPhoneCodeStr() {
		if ( !CommonUtil.isHebrewSkin(skin.getId())) {
			return "00-" + code + "-";
		} else {
			return "";
		}
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

    public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

    public void setSkinId(String skinId) {
		this.skinId = skinId;
	}

    public String getSkinId(){
    	return this.skinId;
    }

	public String getMobilePhonePrefix() {
		return mobilePhonePrefix;
	}

	public void setMobilePhonePrefix(String mobilePhonePrefix) {
		this.mobilePhonePrefix = mobilePhonePrefix;
	}

	/**
	 * Constructs a <code>String</code> with all attributes in name = value format.
	 *
	 * @return a <code>String</code> representation of this object.
	 */
	public String toString() {
	    String TAB = System.getProperty("line.separator");

	    String retValue = "ContactFirstDepositForm ( "
	        + super.toString() + TAB
	        + "contactName = " + contactName + TAB
	        + "email = " + email + TAB
	        + "mobilePhone = " + mobilePhone + TAB
	        + " )";
	    return retValue;
	}

	public boolean isValidBanner() {
		return isValidBanner;
	}

	public void setValidBanner(boolean isValidBanner) {
		this.isValidBanner = isValidBanner;
	}



	public ArrayList<SelectItem> getCountries() {
		return countries;
	}



	public void setCountries(ArrayList<SelectItem> countries) {
		this.countries = countries;
	}


	public Long getCountryId() {
		return countryId;
	}


	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	/**
	 * Check and put contactMe pixel(http/https)
	 * In this pixel we take comId from cookie/session
	 * @return
	 * @throws SQLException
	 */
	public String getContactMePixel() throws SQLException {

		String pixel = "";
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();

		Long combId = UsersManager.getCombinationId(request);
		if (null != combId) {
			MarketingCombination comb = UsersManager.getCombinationById(combId.longValue());
			if (null != comb && CommonUtil.getAppData().getIsLive() &&
					null != comb.getCombPixels() && comb.getCombPixels().getId() != 0
						&& null != comb.getCombPixels().getPixels()) {
				for (MarketingPixel p : comb.getCombPixels().getPixels()) {
					if (p.getTypeId() == Constants.PIXEL_TYPE_CONTACTME) {
						if (request.getProtocol().indexOf(Constants.HTTPS_PROTOCOL) > -1) {
							pixel = p.getHttpsCode();
						} else if (request.getProtocol().indexOf(Constants.HTTP_PROTOCOL) > -1) {
							pixel = p.getHttpCode();
						} else {
							return "";
						}
					}
				}
				pixel = putParametersPixel(pixel, request);
			}
		}
		return pixel;
	}

 	/**
 	 * Set parameters into the pixel
 	 * @param pixel  pixel code
 	 * @return  pixel code with parameters setting
 	 */
 	public String putParametersPixel(String pixel, HttpServletRequest request) {
 		String newPixel = pixel;

 		// replace all parameters with values

 		if ( pixel.indexOf(Constants.MARKETING_PARAMETER_MOBIE) > -1 ) {
 			newPixel = newPixel.replace(Constants.MARKETING_PARAMETER_MOBIE, request.getParameter("phone"));
 		}

 		if ( pixel.indexOf(Constants.MARKETING_PARAMETER_DYNAMIC) > -1 ) {
 			newPixel = newPixel.replace(Constants.MARKETING_PARAMETER_DYNAMIC, this.dynamicParameter);
 		}

 		//change all & to &amp;
 		newPixel = newPixel.replace("&", "&amp;");

 		return newPixel;
 	}

    public String submitWAPContactMe() {
    	try {
    		logger.debug("sending wap conatact me");
    		this.combId = Long.parseLong(this.combIdTxt);
    	/*	if (mobilePhone.length() > 2) {
	    		try {
	    			long s = Long.parseLong(mobilePhone);
	    		} catch (Exception e) {
	    			mobilePhone = mobilePhone.substring(1);
				}
    		} */
			if ( phoneValidationEtrader("error.name.letters") == false ) {   // validation problem
				return null;
			}
			Contact contactReq = new Contact();

			//for etrader populate phone with prefix
			contactReq.setPhone(this.mobilePhonePrefix + this.mobilePhone );
			contactReq.setType(Contact.CONTACT_ME_TYPE);
			contactReq.setText(this.email);
			contactReq.setWriterId(CommonUtil.getWebWriterId());
			contactReq.setText(this.contactName);
			contactReq.setCombId((this.combId).longValue());
			contactReq.setCountryId(Constants.COUNTRY_ID_IL);
			contactReq.setSkinId(Skin.SKIN_ETRADER);
			contactReq.setDynamicParameter(this.dynamicParameter);
			contactReq.setAff_sub1(aff_sub1);
			contactReq.setAff_sub2(aff_sub2);
			contactReq.setAff_sub3(aff_sub3);
			contactReq.setCountryId(countryId);

			insertNewContact(contactReq);

			return Constants.NAV_CONTACTME_SUCCESS_WAP;  	 //mean that all validate are good

		} catch (Exception e) {
			logger.error("Could not insert contact record from mobile phone.", e);
		}

		return null;
    }

/*    public String submitWAPContactMe704() {
    	this.combId = ((long)704);
    	return submitWAPContactMe();
    }

    public String submitWAPContactMe705() {
    	this.combId = ((long)705);
    	return submitWAPContactMe();
    }*/

	/**
	 * @return the combIdTxt
	 */
	public String getCombIdTxt() {
		return combIdTxt;
	}


	/**
	 * @param combIdTxt the combIdTxt to set
	 */
	public void setCombIdTxt(String combIdTxt) {
		this.combIdTxt = combIdTxt;
	}

	public String changeParamForSendContanct(){
		this.contactName = this.contactPrivateName + " " + this.contactLastName;
		if (validateName("error.name.letters", "contact.firstname" , this.contactPrivateName) == null) {
			return null;
		}
		if (validateName("error.name.letters", "contact.lastname" , this.contactLastName) == null) {
			return null;
		}
		if ( phoneValidationEtrader("error.name.letters") == false ) {   // validation phone
			return null;
		}
		if (validateMail() == null) {
			return null;
		}
		return sendContactEmail(Constants.NAV_CONTACTME_IFRAME_SUCCESS);
	}


	public String getContactLastName() {
		return contactLastName;
	}


	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}


	public String getContactPrivateName() {
		return contactPrivateName;
	}


	public void setContactPrivateName(String contactPrivateName) {
		this.contactPrivateName = contactPrivateName;
	}

	public String getAff_sub1() {
		return aff_sub1;
	}

	public void setAff_sub1(String aff_sub1) {
		this.aff_sub1 = aff_sub1;
	}

	public String getAff_sub2() {
		return aff_sub2;
	}

	public void setAff_sub2(String aff_sub2) {
		this.aff_sub2 = aff_sub2;
	}

	public String getAff_sub3() {
		return aff_sub3;
	}

	public void setAff_sub3(String aff_sub3) {
		this.aff_sub3 = aff_sub3;
	}

	
}