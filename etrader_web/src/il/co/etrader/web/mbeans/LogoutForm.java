/**
 * 
 */
package il.co.etrader.web.mbeans;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Skin;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.LoginsManager;
import il.co.etrader.web.util.Constants;

/**
 * @author pavelhe
 *
 */
public class LogoutForm implements Serializable {
	
	private static final Logger logger = Logger.getLogger(LogoutForm.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = -1421163104231846211L;

	public String logout() throws IOException, SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		HttpSession session = request.getSession();
		
		String redirectSessionparam	= (String) session.getAttribute(Constants.FROM_REDIRECT);
		String fromLive 			= (String) session.getAttribute(Constants.FROM_LIVE);
		String redirectedToReg		= (String) session.getAttribute(Constants.REDIRECTED_TO_REGISTER);
		String utcOffset			= (String) session.getAttribute(Constants.UTC_OFFSET);
		String skin					= (String) session.getAttribute(Constants.SKIN_ID);
		Long loginId				= (Long) session.getAttribute(Constants.LOGIN_ID);
		String mobileDeviceUniqueId = (String)session.getAttribute(Constants.DEVICE_UNIQUE_ID);
		String mobileAppVersion		= (String) session.getAttribute(Constants.MOBILE_APP_VER);
		
		if (null != loginId) {
    		LoginsManager.logOut(loginId);
    	}

		logger.info("Invalidating session [" + session.getId()
						+ "] with attrubutes - fromLive [" + fromLive
												+ "], skin [" + skin + "]");
		try {
		    request.logout();
		} catch (Exception e) {
		    logger.error("Can't logout.", e);
		}
        session.invalidate();
		
		// Getting the new session after invalidation
		session = request.getSession();
		logger.info("Created new session [" + session.getId() + "]");
		
		if (CommonUtil.isMobile()) {
			if (null != mobileDeviceUniqueId) { // set the duid in the new session
				session.setAttribute(Constants.DEVICE_UNIQUE_ID, mobileDeviceUniqueId);
			}
			if (null != mobileAppVersion) {
				session.setAttribute(Constants.MOBILE_APP_VER, mobileAppVersion);
			}

			if (Constants.FROM_REDIRECT_ISRAEL.equalsIgnoreCase(redirectSessionparam)) {
				session.setAttribute(Constants.FROM_REDIRECT, Constants.FROM_REDIRECT_ISRAEL);
			}
			
			return Constants.NAV_MAIN;
		} else {
			long lSkin = -1;
			if (skin != null) {
				try {
					lSkin = Long.parseLong(skin);
				} catch(NumberFormatException e) {
					logger.warn("Skin id [s] in session is not parsable [" + skin + "]", e);
					return Constants.NAV_MAIN_WITH_REDIRECT;
				}
			}
			
			if(CommonUtil.isHebrewSkin(lSkin)) {
				return Constants.NAV_MAIN_WITH_REDIRECT;
			} else {
				String navigationCase = Constants.NAV_MAIN_WITH_REDIRECT;
		
				if (lSkin == Skin.SKIN_EN_US || lSkin == Skin.SKIN_ES_US) {
					// Return to normal skin after log out of US skin
					skin = String.valueOf(lSkin == Skin.SKIN_EN_US ? Skin.SKIN_ENGLISH : Skin.SKIN_SPAIN);
					logger.info("change cookie skin from : " + lSkin + " to : " + skin);
					ApplicationData.addCookie(Constants.SKIN_ID, skin, response, Constants.SKIN_ID_DESCRIPTION);
				} else if (lSkin != Skin.SKIN_CHINESE_VIP) {
					navigationCase = Constants.NAV_BUBBLES_PAGE;
				}
				
				if (null != redirectedToReg) {
					session.setAttribute(Constants.REDIRECTED_TO_REGISTER, redirectedToReg);
				}
				session.setAttribute(Constants.UTC_OFFSET, utcOffset);
				
				
				session.setAttribute(Constants.SKIN_ID, skin);
				if(fromLive != null && fromLive.equals(Constants.FROM_LIVE_PENDING)) {
					session.setAttribute(Constants.FROM_LIVE, Constants.FROM_LIVE_TRUE);
				} else {
					session.setAttribute(Constants.FROM_LIVE, Constants.FROM_LIVE_FALSE);
				}
				
				if (Constants.FROM_REDIRECT_ISRAEL.equalsIgnoreCase(redirectSessionparam)) {
					session.setAttribute(Constants.FROM_REDIRECT, Constants.FROM_REDIRECT_ISRAEL);
				}
				
				return navigationCase;
			}
		}
	}

}
