package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.service.CommonJSONService;

import il.co.etrader.bl_managers.MarketsManager;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.InvestmentsManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.chart.ChartDataJsonServlet;
import il.co.etrader.web.helper.TradingPageBoxBean;
import il.co.etrader.web.util.Constants;

/**
 * Any Option trading page backing bean.
 *
 * @author Tony
 */
public class TradingPageBean implements Serializable {
    private static final long serialVersionUID = 1L;

    private static final Logger log = Logger.getLogger(TradingPageBean.class);

    private static String israelMarkets;
    static {
        try {
            ArrayList<Long> l = MarketsManager.getIsraelMarketsIds();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < l.size(); i++) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append(l.get(i));
            }
            israelMarkets = sb.toString();
        } catch (Exception e) {
            log.error("Can't load Israel markets.", e);
        }
    }

    private TradingPageBoxBean[] selections;

    private TradingPageBoxBean[] getSelections() {
        if (null == selections) {
            selections = new TradingPageBoxBean[4];
            FacesContext fc = FacesContext.getCurrentInstance();
            HttpServletRequest request = (HttpServletRequest) fc.getExternalContext().getRequest();
            if (null != request.getParameter("m")) {
                try {
                    long marketId = Integer.parseInt(request.getParameter("m"));
                    log.debug("Setting marketId: " + marketId + " to box 1.");
                    selections[0] = new TradingPageBoxBean();
                    selections[0].setMarketId(marketId);
                    selections[0].setScheduled(1);
                    selections[0].setFixed(true);
                } catch (Exception e) {
                    log.error("Can't put market in box.", e);
                }
            }
        }
        return selections;
    }

    private ArrayHelperSlipBean[] getSlip() {
        ArrayHelperSlipBean[] s = new ArrayHelperSlipBean[4];
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) fc.getExternalContext().getRequest();
        Cookie[] cookies = request.getCookies();
        if (null != cookies) {
            for (int i = 0; i < cookies.length; i++) {
                if (cookies[i].getName().equals("slip")) {
                    try {
                        log.debug("got slip cookie - " + cookies[i].getValue());
                        String [] parts = cookies[i].getValue().split("_");
                        for (int j = 0; j < 4; j++) {
                            if (!parts[j * 2].equals("0")) {
                                s[j] = new ArrayHelperSlipBean(parts[j * 2], parts[j * 2 + 1]);
                            }
                        }
                    } catch (Throwable t) {
                        log.error("Can't process slip cookie - value: " + cookies[i].getValue(), t);
                    }
                }
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("No cookies");
            }
        }
        return s;
    }

    public String getHomePageMarket() {
        return getHomePageMarketsInt(1);
    }
    
    public String getHomePageAssetsList() {
        return getHomePageMarketsInt(20);
    }

    public String getHomePageMarkets() {
        return getHomePageMarketsInt(4);
    }

    public String getMobileMarkets() {
        return getHomePageMarketsInt(1);
    }

    private String getHomePageMarketsInt(int boxCount) {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) fc.getExternalContext().getRequest();
        ApplicationData ap = (ApplicationData) fc.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(fc);
        Long skinId = ap.getSkinId();

        ServletContext sc = request.getSession().getServletContext();
        WebLevelsCache wlc = (WebLevelsCache) sc.getAttribute("levelsCache");
        long[] hpm = wlc.getMarketsOnHomePage(skinId);
        long[] om = wlc.getOpenedMarkets();

        getSelections(); // make sure they are loaded

        String group = "";
        int j = 0;
        for (int i = 0; i < boxCount; i++) {
            if (i > 0) {
                group += ", ";
            }
            // if it is selected and is either opend or on the home page (if not opened but on home page it still should be showed)
            if (null != selections && i < selections.length && null != selections[i] && (isOneOf(selections[i].getMarketId(), om) || isOneOf(selections[i].getMarketId(), hpm))) {
                group += "'aotps_" + selections[i].getScheduled() + "_" + selections[i].getMarketId() + "'";
            } else {
                while (j < hpm.length && isSelected(hpm[j])) {
                    j++;
                }
                group += "'aotps_1_" + hpm[j] + "'";
                j++;
            }
        }
        log.debug("group: " + group);
        return group;
    }

    private boolean isOneOf(long m, long[] ms) {
        for (int i = 0; i < ms.length; i++) {
            if (ms[i] == m) {
                return true;
            }
        }
        return false;
    }

    private boolean isSelected(long m) {
        for (int i = 0; i < 4; i++) {
            if (null != selections[i] && selections[i].getMarketId() == m) {
                return true;
            }
        }
        return false;
    }

    public ArrayHelper getArrays() {
        ArrayHelper ah = new ArrayHelper();
        ah.setSelections(getSelections());
//        ah.setSlip(getSlip());
        return ah;
    }

    /**
     * check if the market is in the skin
     * @param marketId
     * @return true if its in the skin true else false
     */
    public boolean isMarketInSkin(long marketId) {
    	FacesContext context = FacesContext.getCurrentInstance();
  		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);

  		Long currentSkinId = ap.getSkinId();
  		Map<Long, String> skinMarketList;
		try {
			skinMarketList = MarketsManager.getSkinsMarkets(currentSkinId);
			if (skinMarketList.containsKey(marketId)) {
	  			return true;
	  		}
		} catch (SQLException sqle) {
			log.error("Cannot get market for skin ID " + currentSkinId, sqle);
		}
  		
  		return false;
    }

    public long getOptionPlusMarketId() {
    	FacesContext context = FacesContext.getCurrentInstance();
  		ApplicationData ap = context.getApplication().evaluateExpressionGet(context, Constants.BIND_APPLICATION_DATA, ApplicationData.class);
  		return CommonJSONService.getOptionPlusMarketId(ap.getSkinId());
    }

	public String getChartDataMarket() {
		return getChartData(1);
	}

	public String getChartDataMarkets() {
		return getChartData(4);
	}

	private String getChartData(int boxCount) {
		String result = "[";
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		ApplicationData ap = context.getApplication()
									.evaluateExpressionGet(context, Constants.BIND_APPLICATION_DATA, ApplicationData.class);
		ServletContext sc = request.getSession().getServletContext();
		WebLevelsCache wlc = (WebLevelsCache) sc.getAttribute("levelsCache");
		Long skinId = ap.getSkinId();
		long[] hpm = wlc.getMarketsOnHomePage(skinId);
		User user = (User) request.getSession().getAttribute("user");
		List<Investment> investments;
		int j = 0;
		for (int i = 0; i < boxCount; i++) {
			if (i > 0) {
				result += ", ";
			}
			try {
				if (user != null && user.getId() > 0) {
					investments = InvestmentsManager.getOpenedInvestmentsByUser(user.getId(), 0l, Opportunity.TYPE_REGULAR, hpm[j]);
				} else {
					investments = new ArrayList<Investment>();
				}
			} catch (SQLException e) {
				log.warn("Unable to load investments", e);
				investments = new ArrayList<Investment>();
			}
			result += ChartDataJsonServlet.getChartData(hpm[j++], request, investments, skinId, 1);
		}

		return result + "]";
	}

    class ArrayHelperSlipBean {
        private String type;
        private String amount;

        public ArrayHelperSlipBean(String type, String amount) {
            this.type = type;
            this.amount = amount;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public class ArrayHelper {
        private TradingPageBoxBean[] selections;
        private ArrayHelperSlipBean[] slip;

        public TradingPageBoxBean[] getSelections() {
            return selections;
        }

        public void setSelections(TradingPageBoxBean[] selections) {
            this.selections = selections;
        }

        public ArrayHelperSlipBean[] getSlip() {
            return slip;
        }

        public void setSlip(ArrayHelperSlipBean[] slip) {
            this.slip = slip;
        }

        public String getFixed() {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < 4; i++) {
                if (null != selections) {
                    sb.append(", ").append(null != selections[i] ? "true" : "false");
                }
            }
            return sb.toString();
        }

        public String getFull() {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < 4; i++) {
                if (null != selections) {
                    sb.append(", ").append(null != selections[i] ? "true" : "false");
                }
            }
            return sb.toString();
        }

        public String getMarkets() {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < 4; i++) {
                if (null != selections) {
                    sb.append(", ").append(null != selections[i] ? selections[i].getMarketId() : "0");
                }
            }
            return sb.toString();
        }

        public String getScheduled() {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < 4; i++) {
                if (null != selections) {
                    sb.append(", ").append(null != selections[i] ? selections[i].getScheduled() : "0");
                }
            }
            return sb.toString();
        }

        public String getSlipTypes() {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < 4; i++) {
                if (null != slip) {
                    sb.append(", ").append(null != slip[i] ? slip[i].getType() : "0");
                }
            }
            return sb.toString();
        }

        public String getSlipAmounts() {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < 4; i++) {
                if (null != slip) {
                    sb.append(", ").append(null != slip[i] ? slip[i].getAmount() : "0");
                }
            }
            return sb.toString();
        }

        public String getIsraelMarkets() {
            return israelMarkets;
        }
    }
}