package il.co.etrader.web.mbeans;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.AssetIndexMarket;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.MarketsManagerBase;

import il.co.etrader.bl_managers.AssetIndexManagerBase;
import il.co.etrader.bl_vos.AssetIndex;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.MarketsManager;
import il.co.etrader.web.helper.AssetIndexGroup;
import il.co.etrader.web.util.Constants;


public class MarketDetails implements java.io.Serializable{
    private static final long serialVersionUID = -3074435569761270202L;
    protected static final Logger logger = Logger.getLogger(MarketDetails.class);
    private ArrayList<Market> marketsList;
    private ArrayList<AssetIndexMarket> assetIndexMarketsList;
    private HashMap<Long,AssetIndex> assetIndex;
    private Market market;

    public MarketDetails() {

    }

    public ArrayList<ArrayList<Market>> getBankOptionMarkets() {
    	FacesContext context = FacesContext.getCurrentInstance();
        ApplicationData app = (ApplicationData)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);

        return MarketsManager.getBankOptionMarkets(app.getSkinId());
	}

    /**
     * @return
     */
    public ArrayList<Market> getMarkets(){


    	FacesContext context = FacesContext.getCurrentInstance();
        ApplicationData a = (ApplicationData)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
        marketsList = new ArrayList<Market>();

        //get all the markets of this skin
        try {
			for (Market market: MarketsManager.getAssetIndex(a.getSkinId()) ) {
				marketsList.add(market.clone());
			}
		} catch (SQLException sqle) {
			logger.error("Cannot load asset indexes with skin ID " + a.getSkinId(), sqle);
		}

        ArrayList<String> daysOfWeekMsgs = new ArrayList<String>(7);
        daysOfWeekMsgs.add(0,"assetIndex.sunday");
        daysOfWeekMsgs.add(1,"assetIndex.monday");
        daysOfWeekMsgs.add(2,"assetIndex.tuesday");
        daysOfWeekMsgs.add(3,"assetIndex.wednesday");
        daysOfWeekMsgs.add(4,"assetIndex.thursday");
        daysOfWeekMsgs.add(5,"assetIndex.friday");
        daysOfWeekMsgs.add(6,"assetIndex.saturday");

        //get market working days and friday hour
        try {
        	 assetIndex = AssetIndexManagerBase.getAll(a.getSkinId());
        } catch(Exception e) {
        	logger.error("Problem getting assetIndex", e);
        }

        String offset = CommonUtil.getUtcOffset();
        String startBreak = null;
        String endBreak = null;

        for(Market market:marketsList) {
            TimeZone tzMarket = TimeZone.getTimeZone(market.getTimeZone());
            AssetIndex asset = assetIndex.get(market.getId());
            String tradingDays = asset.getTradingDays();
            int firstDay = tradingDays.indexOf("1");
            int lastDay = tradingDays.lastIndexOf("1");

            String[] time = asset.getStartTime().split(":");
            int hour = Integer.parseInt(time[0]) + (tzMarket.inDaylightTime(new Date()) ? -1 : 0);
            int min = Integer.parseInt(time[1]);
            Calendar c = Calendar.getInstance();
            c.set(Calendar.DAY_OF_WEEK, firstDay + 1);
            c.set(Calendar.HOUR_OF_DAY, hour);
            c.set(Calendar.MINUTE, min);
            c.set(Calendar.SECOND, 00);
            firstDay = CommonUtil.getDateTimeFormaByTz(c.getTime(), offset).getDay();
            market.setTimeFirstInvest(CommonUtil.getTimeFormat(c.getTime(), offset));

            time = asset.getEndTime().split(":");
            hour = Integer.parseInt(time[0]) + (tzMarket.inDaylightTime(new Date()) ? -1 : 0);
            min = Integer.parseInt(time[1]);
            c.set(Calendar.DAY_OF_WEEK, lastDay + 1);
            c.set(Calendar.HOUR_OF_DAY, hour);
            c.set(Calendar.MINUTE, min);
            lastDay = CommonUtil.getDateTimeFormaByTz(c.getTime(), offset).getDay() ;
            market.setTimeEstClosing(CommonUtil.getTimeFormat(c.getTime(), offset));


            if (asset.getId() == 625 || asset.getId() == 634) {
            	logger.debug("asset " + asset.getEndBreakTime() + " s " + asset.getStartBreakTime());
            }
            if (asset.isFullDay()) {
                market.setTradingDays(CommonUtil.getMessage(daysOfWeekMsgs.get(firstDay),null) + " " + market.getTimeFirstInvest() + " - " + CommonUtil.getMessage(daysOfWeekMsgs.get(lastDay),null) + " " + market.getTimeEstClosing());
                market.setFullDay(true);
            } else if (!asset.isHaveBreak()){
                market.setTradingDays(CommonUtil.getMessage(daysOfWeekMsgs.get(firstDay),null) + " - " + CommonUtil.getMessage(daysOfWeekMsgs.get(lastDay),null) + " " + market.getTimeFirstInvest() + " - " + market.getTimeEstClosing());
                market.setFullDay(false);
            } else {
            	time = asset.getStartBreakTime().split(":");
                hour = Integer.parseInt(time[0]) + (tzMarket.inDaylightTime(new Date()) ? -1 : 0);
                min = Integer.parseInt(time[1]);
                c = Calendar.getInstance();
                c.set(Calendar.DAY_OF_WEEK, firstDay + 1);
                c.set(Calendar.HOUR_OF_DAY, hour);
                c.set(Calendar.MINUTE, min);
                c.set(Calendar.SECOND, 00);
                startBreak = CommonUtil.getTimeFormat(c.getTime(), offset);

                time = asset.getEndBreakTime().split(":");
                hour = Integer.parseInt(time[0]) + (tzMarket.inDaylightTime(new Date()) ? -1 : 0);
                min = Integer.parseInt(time[1]);
                c = Calendar.getInstance();
                c.set(Calendar.DAY_OF_WEEK, lastDay + 1);
                c.set(Calendar.HOUR_OF_DAY, hour);
                c.set(Calendar.MINUTE, min);
                c.set(Calendar.SECOND, 00);
                endBreak = CommonUtil.getTimeFormat(c.getTime(), offset);

            	market.setTradingDays(CommonUtil.getMessage(daysOfWeekMsgs.get(firstDay),null) + " - " + CommonUtil.getMessage(daysOfWeekMsgs.get(lastDay),null) + " " + market.getTimeFirstInvest() + " - " + startBreak + ", " + endBreak + " - " + market.getTimeEstClosing());
                market.setFullDay(false);
            }
        	try{

        		if (null != asset.getFridayTime()) {
                    time = asset.getFridayTime().split(":");
                    hour = Integer.parseInt(time[0]) + (tzMarket.inDaylightTime(new Date()) ? -1 : 0);
                    min = Integer.parseInt(time[1]);
                    c.set(Calendar.HOUR_OF_DAY, hour);
                    c.set(Calendar.MINUTE, min);
        			market.setSuspendedMessage(CommonUtil.getTimeFormat(c.getTime(), offset)); // storing friday time in unused suspendedMessage
        		}

        	} catch(Exception e) {
        		logger.error("Error parsing TIME_FIRST_INVEST :" +  market.getTimeFirstInvest() + "TIME_EST_CLOSING: "+market.getTimeEstClosing());
        	}
        	//0100 markets has option_plus_market_id
        	if (market.getFeedName().indexOf("Binary0-100") > -1) {
        		market.setFeedName(market.getFeedName().substring(0, market.getFeedName().length() - 12));
        	} else if (market.isOptionPlus()) {
            	market.setFeedName(market.getFeedName().substring(0, market.getFeedName().length() - 8));
            }
        	
        }
        
        for(Market market1:marketsList) {
            logger.debug(market1.getName());
        }        
        return marketsList;
    	//return market;
    }
    
    public ArrayList<AssetIndexMarket> getAssetIndexMarkets(){
    	ArrayList<AssetIndexMarket> list = null;
    	FacesContext context = FacesContext.getCurrentInstance();
        ApplicationData a = (ApplicationData)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
        try {
        	list = MarketsManager.getAssetIndexMarkets(a.getSkinId(), new HashMap<Long, List<AssetIndexMarket>>());
		} catch (SQLException sqle) {
			logger.error("Cannot load asset index markets with skin ID " + a.getSkinId(), sqle);
		}
        return list;
    }
    
    public List<AssetIndexMarket> getGroupAssetIndexMarket(long groupTypeId){
    	
    	FacesContext context = FacesContext.getCurrentInstance();
        ApplicationData a = (ApplicationData)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
        return ApplicationData.getSkinById(a.getSkinId()).getGroupAssetIndexMarket().get(groupTypeId);
    }
    
    public AssetIndexMarket getAssetIndexMarket(long marketId){    	
    	AssetIndexMarket assetIndexMarket = new AssetIndexMarket();
        for(AssetIndexMarket aim : getAssetIndexMarkets()){
        	if(aim.getMarket() != null){
               	if(aim.getMarket().getId() == marketId){
            		assetIndexMarket = aim;
            	}
        	}
        }        
        return assetIndexMarket;
    }
    
    public String  getMarketsHours(Market marketAssets){

    	String assetTradingDays = "";
    	String timeFirstInvest = marketAssets.getTimeFirstInvest();
    	String timeEstClosing = marketAssets.getTimeEstClosing();
    	FacesContext context = FacesContext.getCurrentInstance();
        ApplicationData a = (ApplicationData)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);

        ArrayList<String> daysOfWeekMsgs = new ArrayList<String>(7);
        daysOfWeekMsgs.add(0,"assetIndex.sunday");
        daysOfWeekMsgs.add(1,"assetIndex.monday");
        daysOfWeekMsgs.add(2,"assetIndex.tuesday");
        daysOfWeekMsgs.add(3,"assetIndex.wednesday");
        daysOfWeekMsgs.add(4,"assetIndex.thursday");
        daysOfWeekMsgs.add(5,"assetIndex.friday");
        daysOfWeekMsgs.add(6,"assetIndex.saturday");

        //get market working days and friday hour
        try {
        	 assetIndex = AssetIndexManagerBase.getAll(a.getSkinId());
        } catch(Exception e) {
        	logger.error("Problem getting assetIndex", e);
        }

        String offset = CommonUtil.getUtcOffset();
        String startBreak = null;
        String endBreak = null;

      
            TimeZone tzMarket = TimeZone.getTimeZone(marketAssets.getTimeZone());
            AssetIndex asset = assetIndex.get(marketAssets.getId());
            String tradingDays = asset.getTradingDays();
            int firstDay = tradingDays.indexOf("1");
            int lastDay = tradingDays.lastIndexOf("1");

            String[] time = asset.getStartTime().split(":");
            int hour = Integer.parseInt(time[0]) + (tzMarket.inDaylightTime(new Date()) ? -1 : 0);
            int min = Integer.parseInt(time[1]);
            Calendar c = Calendar.getInstance();
            c.set(Calendar.DAY_OF_WEEK, firstDay + 1);
            c.set(Calendar.HOUR_OF_DAY, hour);
            c.set(Calendar.MINUTE, min);
            c.set(Calendar.SECOND, 00);
            firstDay = CommonUtil.getDateTimeFormaByTz(c.getTime(), offset).getDay();
            timeFirstInvest = CommonUtil.getTimeFormat(c.getTime(), offset);

            time = asset.getEndTime().split(":");
            hour = Integer.parseInt(time[0]) + (tzMarket.inDaylightTime(new Date()) ? -1 : 0);
            min = Integer.parseInt(time[1]);
            c.set(Calendar.DAY_OF_WEEK, lastDay + 1);
            c.set(Calendar.HOUR_OF_DAY, hour);
            c.set(Calendar.MINUTE, min);
            lastDay = CommonUtil.getDateTimeFormaByTz(c.getTime(), offset).getDay() ;
            timeEstClosing = CommonUtil.getTimeFormat(c.getTime(), offset);


            if (asset.isFullDay()) {
            	assetTradingDays = CommonUtil.getMessage(daysOfWeekMsgs.get(firstDay),null) + " " + timeFirstInvest + " - " + CommonUtil.getMessage(daysOfWeekMsgs.get(lastDay),null) + " " + timeEstClosing;
            } else if (!asset.isHaveBreak()){
            	assetTradingDays = CommonUtil.getMessage(daysOfWeekMsgs.get(firstDay),null) + " - " + CommonUtil.getMessage(daysOfWeekMsgs.get(lastDay),null) + " " + timeFirstInvest + " - " + timeEstClosing;
            	//marketAssets.setFullDay(false);
            } else {
            	time = asset.getStartBreakTime().split(":");
                hour = Integer.parseInt(time[0]) + (tzMarket.inDaylightTime(new Date()) ? -1 : 0);
                min = Integer.parseInt(time[1]);
                c = Calendar.getInstance();
                c.set(Calendar.DAY_OF_WEEK, firstDay + 1);
                c.set(Calendar.HOUR_OF_DAY, hour);
                c.set(Calendar.MINUTE, min);
                c.set(Calendar.SECOND, 00);
                startBreak = CommonUtil.getTimeFormat(c.getTime(), offset);

                time = asset.getEndBreakTime().split(":");
                hour = Integer.parseInt(time[0]) + (tzMarket.inDaylightTime(new Date()) ? -1 : 0);
                min = Integer.parseInt(time[1]);
                c = Calendar.getInstance();
                c.set(Calendar.DAY_OF_WEEK, lastDay + 1);
                c.set(Calendar.HOUR_OF_DAY, hour);
                c.set(Calendar.MINUTE, min);
                c.set(Calendar.SECOND, 00);
                endBreak = CommonUtil.getTimeFormat(c.getTime(), offset);
                
                assetTradingDays = CommonUtil.getMessage(daysOfWeekMsgs.get(firstDay),null) + " - " + CommonUtil.getMessage(daysOfWeekMsgs.get(lastDay),null) + " " + marketAssets.getTimeFirstInvest() + " - " + startBreak + ", " + endBreak + " - " + marketAssets.getTimeEstClosing();
                //marketAssets.setFullDay(false);
            }             	  
        return assetTradingDays;
    }

    /**
     * for asset index in mobile
     * @return
     */
    public ArrayList<AssetIndexGroup> getMarketsMobile() {
        return getMarketsInGroups(2, true);
    }

    /**
     * for bank option in web
     * @return
     */
    public ArrayList<AssetIndexGroup> getMarketsAssetList() {
    	ArrayList<AssetIndexGroup> assetGroups = new ArrayList<AssetIndexGroup>();
    	int[] counter = new int[10];
        int max = 0;
        int group;
        for (Market m : getMarkets()) {
            group = m.getGroupId().intValue();
            if (group > counter.length - 1) {
                counter = Arrays.copyOf(counter, group + 1);
            }
            counter[group]++;
            if (max < counter[group]) {
                max = counter[group];
            }
        }
        int maxRow = Math.round(max/2);
        if (max % 2 != 0) {
            maxRow++;
        }
        //organized the assets in "BINARY_OPTIONS_MAX_COLUMNS" columns, equally.
        FacesContext context = FacesContext.getCurrentInstance();
        ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
        if (ap.getSkinId() == Skin.SKIN_EN_US || ap.getSkinId() == Skin.SKIN_ES_US) {
        	maxRow = howManyAssetsInEachColumn(max);
        }

        assetGroups = getMarketsInGroups(maxRow, false);
        ArrayList<AssetIndexGroup> assetGroupsChangeOrder = new ArrayList<AssetIndexGroup>(assetGroups.size());
        if (ap.getSkinId() != Skin.SKIN_EN_US && ap.getSkinId() != Skin.SKIN_ES_US && ap.getSkinId() != Skin.SKIN_CHINESE) {
        	assetGroupsChangeOrder.add(assetGroups.get(1)); // change order of the groups, to Indices, Currencies, Commodities, stocks.
        	assetGroupsChangeOrder.add(assetGroups.get(2));
        	assetGroupsChangeOrder.add(assetGroups.get(3));
        	assetGroupsChangeOrder.add(assetGroups.get(0));
        	return assetGroupsChangeOrder;
        }
        return assetGroups;
    }

    private int howManyAssetsInEachColumn(int numOfAssets) {
    	int numOfAssetsEachColumn = 0;
    	numOfAssetsEachColumn = numOfAssets / Constants.BINARY_OPTIONS_MAX_COLUMNS;
    	return numOfAssetsEachColumn;
    }

    public ArrayList<AssetIndexGroup> getMarketsInGroups(int columnNumber, boolean isMobile) {
//      list of groups markets contain no more then 9 markets per group
        HashMap<Long, ArrayList<Market>> groupsMarkets = new HashMap<Long, ArrayList<Market>>();
//      final list with all groups.
        ArrayList<AssetIndexGroup> assetGroups = new ArrayList<AssetIndexGroup>();
        for (Market m : getMarkets()) {
            if (!m.isOptionPlus()) {
                ArrayList<Market> groupMarkets = groupsMarkets.get(m.getGroupId());
                if (null == groupMarkets) {
                    groupMarkets = new ArrayList<Market>();
                }
                if (groupMarkets.size() == (isMobile ? Math.round(m.getGroupMarketsCounter() / columnNumber) : columnNumber)) { //each group columns number should be with number of markets devide to columnNumner param
                    addToAssetIndexGroup(assetGroups, m, groupMarkets);
                    groupMarkets = new ArrayList<Market>();  // initialize markets list
                }
                groupMarkets.add(m);
                groupsMarkets.put(m.getGroupId(), groupMarkets);
            }
        }
        for (Iterator<Long> itr = groupsMarkets.keySet().iterator(); itr.hasNext();) {
            ArrayList<Market> markets = groupsMarkets.get(new Long(itr.next()));
            if (markets.size() > 0) {
                addToAssetIndexGroup(assetGroups, markets.get(0), markets);
            }
        }

        return assetGroups;
    }

    /**
     * Add markets list to the relevant group in the final list.
     * @param assetGroups
     * @param m
     * @param groupMarkets
     */
    private void addToAssetIndexGroup(ArrayList<AssetIndexGroup> assetGroups, Market m, ArrayList<Market> groupMarkets) {
        AssetIndexGroup assetIndexGroup = null;
        for (AssetIndexGroup aig : assetGroups) {
            if (aig.getId() == m.getGroupId()) {
                assetIndexGroup = aig;
                break;
            }
        }
        if (null != assetIndexGroup) {
            assetIndexGroup.addNewMarketsList(groupMarkets);
        } else { //if we dont have asset Index Group object for this group make 1 and add his first 9 markets list
            assetIndexGroup = new AssetIndexGroup(m.getGroupName());
            assetIndexGroup.setId(m.getGroupId());
            assetIndexGroup.addNewMarketsList(groupMarkets);
            assetGroups.add(assetIndexGroup);
        }
    }

    /**
     * @return the market
     */
    public Market getMarket() {
        return market;
    }

    /**
     * @param market the market to set
     */
    public void setMarket(Market market) {
        this.market = market;
    }

	public String getDisplayGroupNameKeyTxt() {
		if (market == null || null == market.getDisplayNameKey()) {
			return "";
		}
		FacesContext context = FacesContext.getCurrentInstance();
        ApplicationData app = (ApplicationData)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
    	return MarketsManagerBase.getMarketName(app.getSkinId(), market.getId());
	}
	
	public int getMarketFromURL() {
		Market requestMarket = getMarketByAssetName();
		if(requestMarket != null ){
			return (int) requestMarket.getId();
		}
		return 0;
	}
	
	public long getMarketGroupFromURL() {
		Market requestMarket = getMarketByAssetName();
		if(requestMarket != null ){
			return requestMarket.getGroupId();
		}
		return 0l;
	}
	
	private Market getMarketByAssetName(){
		Market m = null;
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();

		for (AssetIndexMarket assetIndexMarket : getAssetIndexMarkets()) {			
			if(assetIndexMarket.getMarket() != null){
				if (assetIndexMarket.getMarket().getFeedName().equalsIgnoreCase(request.getParameter("assetName")) 
						&& assetIndexMarket.getAssetIndexInfo().getOpportunityType() == ConstantsBase.OPPORTUNITY_TYPE_BINARY_) {
					 m = assetIndexMarket.getMarket();
				}	
			}
		}
		return m;
	}
	
	
}