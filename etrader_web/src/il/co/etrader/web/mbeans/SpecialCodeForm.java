package il.co.etrader.web.mbeans;

import java.io.IOException;
import java.sql.SQLException;

import javax.faces.FacesException;
import javax.faces.application.Application;
import javax.faces.application.ViewHandler;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.util.Constants;


public class SpecialCodeForm {
	private static final Logger log = Logger.getLogger(SpecialCodeForm.class);

	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void registerCode() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		ExternalContext extContext = context.getExternalContext();
		// add cookie
		ApplicationData.addCookie(Constants.SPECIAL_CODE_PARAM, code, (HttpServletResponse)extContext.getResponse(), Constants.SPECIAL_CODE_DESCRIPTION);
		// add to session
		((HttpServletRequest)extContext.getRequest()).getSession().setAttribute(Constants.SPECIAL_CODE_PARAM, code);
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		Application app = context.getApplication();
		ViewHandler viewHandler = app.getViewHandler();
        String prefix = ap.getUserLocale().getLanguage();
        try {
            prefix = ap.getLinkLocalePrefix();
        } catch (Exception e) {
            log.error("Can't get language.", e);
        }
        String url = ap.getHomePageUrlWithoutLastSlash() + PageNames.getPageNameStatic(prefix, "/jsp/register.jsf");
		url = extContext.encodeResourceURL(url);
		try {
			extContext.redirect(url);
		} catch (IOException e) {
			throw new FacesException(e);
		}
	}


	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "SpecialCodeForm ( "
	        + super.toString() + TAB
	        + "code = " + this.code + TAB
	        + " )";
	    return retValue;
	}
}
