package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.clearing.CashUInfo;
import com.anyoption.common.managers.CurrencyRatesManagerBase;

import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.clearing.CashUClearingProvider;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.TransactionsManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;

public class CashUDepositForm extends DepositForm implements Serializable{
    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(CashUDepositForm.class);

	private CashUInfo cashUInfo;
	private boolean returnPage;


	public CashUDepositForm() throws Exception {
		super();
		User user = ApplicationData.getUserFromSession();
		cashUInfo = new CashUInfo(user.getUserName(), user.getCurrency().getCode(), user.getSkinId());
		returnPage = isReturn();
		logger.debug("CashUDepositForm: return page = " + returnPage);
	}

	/**
	 * cashU deposit
	 *
	 * @return
	 * @throws Exception
	 */
	public String insertDeposit() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		// prevent identical requests from being processed
		if (saveTokenIsInvalid()) {
			return logInvalidSaveAndReturn(logger, null);
		}
		generateSaveToken();
		User user = ApplicationData.getUserFromSession();
		ApplicationData gm = (ApplicationData) context.getApplication()
				.createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		Transaction t = TransactionsManager.insertDepositCashUPhase1(deposit, "CashUDepositForm", CommonUtil.getWebWriterId(),Constants.WEB_CONST, user, loginId);

		if (null != t) {
			if (logger.isDebugEnabled()) {
				logger.debug("updating user session details.");
			}
			cashUInfo.addTransactionData(t);
			String reURL = ClearingManager.getCashUUrl() + "?" + ClearingManager.getCashURequest(cashUInfo);
			logger.debug("CashUDepositForm: redirecting to: " + reURL + "\nwith CashU info: " + cashUInfo.toString());
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			response.sendRedirect(reURL);
		}
		return null;
	}

	/**
	 * cashU continue after deposit
	 *
	 * @return
	 * @throws Exception
	 */
	public String getFinishDeposit() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}

		User user = ApplicationData.getUserFromSession();
		Transaction t = TransactionsManager.insertDepositCashUPhase2(cashUInfo, user, loginId);

		if (null != t) {
            ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
            ve.getValue(context.getELContext());
            ve.setValue(context.getELContext(), t);
			if (logger.isDebugEnabled()) {
				logger.debug("updating user session details.");
			}
			TransactionsManager.updateUserByName(user.getUserName(), user);
			String link = receiptHttpsNav();
			/////// not in live ////////////
			ApplicationData gm = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
			link += ".jsf;" + gm.getSessionIdString();
			return link;
			////////////////////////////////
		} //else { // failed
		return "";
	}

	/**
	 * Constructs a <code>String</code> with all attributes in name = value format.
	 *
	 * @return a <code>String</code> representation of this object.
	 */
	public String toString() {
	    final String TAB = " \n ";
	    String retValue = "";
	    retValue = "CashUDepositForm ( "
	        + super.toString() + TAB
	        + cashUInfo.toString()
	        + " )";
	    return retValue;
	}

	public CashUInfo getCashUInfo() {
		return cashUInfo;
	}

	public void setCashUInfo(CashUInfo cashUInfo) {
		this.cashUInfo = cashUInfo;
	}

	public boolean isReturn() throws Exception {
		logger.info("Checking cashUDeposit page parameters...");
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String param = null;
		//boolean responseDetails = false;
		boolean error = false;
		param = request.getParameter(CashUClearingProvider.RESPONSE_PARAM_ORDER_ID);
		if (null != param) {
			//responseDetails = true;
			cashUInfo.setTransactionId(Long.valueOf(param));
			logger.debug(CashUClearingProvider.RESPONSE_PARAM_ORDER_ID + " = " + param);
		}
		else {
			error = true;
		}
		if (!error) {
			param = request.getParameter(CashUClearingProvider.RESPONSE_PARAM_STATUS);
			if (null != param) {
				//responseDetails = true;
				cashUInfo.setStatus(param);
				logger.debug(CashUClearingProvider.RESPONSE_PARAM_STATUS + " = " + param);
			}
			else {
				error = true;
			}
		}
		if (!error) {
			param = request.getParameter(CashUClearingProvider.RESPONSE_PARAM_STATUS_CODE);
			if (null != param) {
				//responseDetails = true;
				cashUInfo.setStatusCode(param);
				logger.debug(CashUClearingProvider.RESPONSE_PARAM_STATUS_CODE + " = " + param);
			}
			else {
				error = true;
			}
		}
		if (!error) {
			param = request.getParameter(CashUClearingProvider.RESPONSE_PARAM_STATUS_MESSAGE);
			if (null != param) {
				//responseDetails = true;
				cashUInfo.setStatusMessage(param);
				logger.debug(CashUClearingProvider.RESPONSE_PARAM_STATUS_MESSAGE + " = " + param);
			}
			else {
				error = true;
			}
		}
		if (!error) {
			param = request.getParameter(CashUClearingProvider.RESPONSE_PARAM_PASSKEY);
			if (null != param) {
				//responseDetails = true;
				try {
					String pass = String.valueOf(cashUInfo.getTransactionId()).toLowerCase() + ":" + "anyoption";
					MessageDigest m = MessageDigest.getInstance("MD5");
					byte[] data = pass.getBytes();
					m.update(data,0,data.length);
					BigInteger i = new BigInteger(1,m.digest());
					String myMD5 = String.format("%1$032X", i);
					if (myMD5.equals(param)) {
						cashUInfo.setPassKey(param);
					}
					else {
						error = true;
					}
				} catch (NoSuchAlgorithmException e) {
					error = true;
				}
				logger.debug(CashUClearingProvider.RESPONSE_PARAM_PASSKEY + " = " + param);
			}
			else {
				error = true;
			}
		}
		if (!error) {
			param = request.getParameter(CashUClearingProvider.RESPONSE_PARAM_ASTECH_ID);
			//responseDetails = true;
			cashUInfo.setAstechId(param);
			if (logger.isDebugEnabled()) {
				logger.debug(CashUClearingProvider.RESPONSE_PARAM_ASTECH_ID + " = " + param);
			}
		}
		return !error;
	}

	public String getConvertionMsg() throws Exception {
		User user = ApplicationData.getUserFromSession();
		double amountUSD = CurrencyRatesManagerBase.convertToBaseAmount(1, user.getCurrencyId(), new Date());
		return CommonUtil.getMessage("cashu.deposit.message.convertion",
				new String[] {user.getCurrency().getCode(),
				CommonUtil.displayAmount(CommonUtil.calcAmount(amountUSD), false, Constants.CURRENCY_USD_ID)});
	}

	public String getInitForm() {
		return "";
	}

	public boolean isReturnPage() {
		return returnPage;
	}

	public void setReturnPage(boolean returnPage) {
		this.returnPage = returnPage;
	}
}