/**
 * 
 */
package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.QuestionnaireGroup;
import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.QuestionnaireManagerBase;
import com.anyoption.common.managers.UserRegulationManager;

import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.bl_vos.UserRegulation;
import il.co.etrader.web.util.Constants;

/**
 * @author kirilim
 *
 */
public class RegOptQuestionnaireForm implements Serializable {

	/**
	 * 
	 */
	private static final Logger log = Logger.getLogger(RegOptQuestionnaireForm.class);
	private static final long serialVersionUID = -7328612313996696005L;
	private QuestionnaireGroup questionnaire;
	private List<QuestionnaireUserAnswer> userAnswers;
	private List<QuestionnaireUserAnswer> multipleUserAnswers;
	private List<QuestionnaireQuestion> questions;
	private Long[] checkedAnsweredIds;
	private User user;
	private final boolean submitted;
	
	
	public RegOptQuestionnaireForm() throws Exception{
		questionnaire = QuestionnaireManagerBase.getAllQuestionnaires().get(QuestionnaireGroup.REGULATION_OPTIONAL_QUESTIONNAIRE_NAME);
		user = ApplicationData.getUserFromSession();
		
		questions = findAndAddMultipleAnswerQuestion();
		
		userAnswers = QuestionnaireManagerBase.getUserAnswers(user.getId(), questions, Writer.WRITER_ID_WEB);
		multipleUserAnswers = userAnswers.subList(userAnswers.size() - 4, userAnswers.size());		
		checkedAnsweredIds = addCheckedAnsweredIds();
		FacesContext context = FacesContext.getCurrentInstance();
		UserRegulation userRegulation = (UserRegulation) context.getApplication()
																	.evaluateExpressionGet(context, Constants.BIND_USER_REGULATION, UserRegulation.class);
		submitted = userRegulation.isOptionalQuestionnaireDone();
	}

	/**
	 * @return
	 */
	public List<QuestionnaireQuestion> getQuestions() {
		return questions;
	}
	
	/**
	 * @return the userAnswers
	 */
	public List<QuestionnaireUserAnswer> getUserAnswers() {
		return userAnswers;
	}
	
	/**
	 * @return the multipleUserAnswers
	 */
	public List<QuestionnaireUserAnswer> getMultipleUserAnswers() {
		return multipleUserAnswers;
	}
	
	/**
	 * @return the checkedAnsweredIds
	 */
	public Long[] getCheckedAnsweredIds() {
		return checkedAnsweredIds;
	}
	
	public void setCheckedAnsweredIds(Long[] checkedAnsweredIds) {
		this.checkedAnsweredIds = checkedAnsweredIds;
	}

	public boolean isSubmitted() {
		return submitted;
	}

	/**
	 * @return
	 * @throws Exception
	 */
	public String insertQuestionnaire() throws Exception {
		log.info("Inserting questionnaire [" + questionnaire.getId() + ":" + questionnaire.getName()
				+ "]  for user [" + user.getId() + "]");
		
		FacesContext context = FacesContext.getCurrentInstance();
		UserRegulation userRegulation = (UserRegulation) context
															.getApplication()
																.evaluateExpressionGet(
																	context,
																	Constants.BIND_USER_REGULATION,
																	UserRegulation.class);
		if (userRegulation.getApprovedRegulationStep() >= UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE
				&& !userRegulation.isOptionalQuestionnaireDone()
				&& !userRegulation.isSuspended()) {
			for (QuestionnaireUserAnswer userAnswer : userAnswers) {
				userAnswer.setWriterId(Writer.WRITER_ID_WEB);
			}
			QuestionnaireManagerBase.updateUserAnswers(userAnswers, false);
			userRegulation.setOptionalQuestionnaireDone(true);
			userRegulation.setWriterId(Writer.WRITER_ID_WEB);
			UserRegulationManager.updateOptionalQuestionnaireStatus(userRegulation);
		} else {
			log.warn("User regulation status is in unappropriate state [" + userRegulation
						+ "] for [optional] questionnaire. Cannot sunmit form. Reloading...");
			return Constants.NAV_USER_QUESTIONNAIRE;
		}
		
		return Constants.NAV_TRADE_PAGE;
	}
	
	/**
	 * @return
	 * @throws Exception
	 */
	public String notNowLink() throws Exception {
		Thread.sleep(7000);
		return Constants.NAV_TRADE_PAGE;
	}
	
	public void validateTextField(FacesContext context, UIComponent comp, Object value) throws UnsupportedEncodingException {
		if (multipleUserAnswers.get(3).getAnswerId() != null) {
			CommonUtil.validateLettersAndNumbersOnly(context, comp, value);
		}
	} 
	
	private List<QuestionnaireQuestion> findAndAddMultipleAnswerQuestion() {
		QuestionnaireQuestion multipleAnswerQuestion = null;
		multipleAnswerQuestion = questionnaire.getQuestions().get(5);
		ArrayList<QuestionnaireQuestion> questions = new ArrayList<QuestionnaireQuestion> (questionnaire.getQuestions());
		questions.remove(multipleAnswerQuestion);
		questions.add(multipleAnswerQuestion);
		questions.add(multipleAnswerQuestion);
		questions.add(multipleAnswerQuestion);
		questions.add(multipleAnswerQuestion);
		
		return questions;
	}	
	
	private Long[] addCheckedAnsweredIds() {
		Long[] ids = new Long[multipleUserAnswers.size()];
		int index = 0;
		for (QuestionnaireUserAnswer userAnswer : multipleUserAnswers) {
			if (userAnswer.getAnswerId() != null) {
				ids[index++] = userAnswer.getAnswerId().longValue();
			}
		}
		
		return ids;
	}
}
