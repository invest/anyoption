package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.sql.SQLException;

import javax.faces.context.FacesContext;

import com.anyoption.common.beans.Fee;
import com.anyoption.common.managers.FeesManagerBase;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.util.Constants;

public class FeesForm implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7038367570635638451L;
	private long feeId;
	private long currencyId;
	
	public long getFeeId() {
		return feeId;
	}

	public void setFeeId(long feeId) {
		this.feeId = feeId;
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}
	
	public String getMaintenanceFeeWithSymbol() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		long currencyId = ap.getUserCurrencyId();
		long feeAmount = FeesManagerBase.getFee(Fee.MAINTENANCE_FEE, currencyId);
		return CommonUtil.displayAmount(feeAmount, true, currencyId);
	}
	
	public String getOptionPlusCommissionFeeWithSymbol() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		long currencyId = ap.getUserCurrencyId();
		long feeAmount = FeesManagerBase.getFee(Fee.OPTION_PLUS_COMMISSION_FEE, currencyId);
		return CommonUtil.displayAmount(feeAmount, true, currencyId);
	}

	public String getOptionPlusCommissionFee(boolean showCurrency) throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = context.getApplication().evaluateExpressionGet(context, Constants.BIND_APPLICATION_DATA, ApplicationData.class);
		long currencyId = ap.getUserCurrencyId();
		long feeAmount = FeesManagerBase.getFee(Fee.OPTION_PLUS_COMMISSION_FEE, currencyId);
		if (showCurrency) {
			return CommonUtil.displayAmount(feeAmount, showCurrency, currencyId);
		} else {
			return CommonUtil.displayAmountForInput(feeAmount, showCurrency, new Long(currencyId).intValue());
		}
	}

	public String toString(){
		final String ls = System.getProperty("line.separator");
		String retValue = "";
		retValue = "Fee ( "
		        + super.toString() + ls
		        + "fee_id = " + this.feeId + ls
		        + "currency_id = " + this.currencyId + ls
		        + " )";
		return retValue;
	}
	
}
