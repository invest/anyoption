package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.HashMap;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.jfree.util.Log;

import com.anyoption.common.beans.Skin;
import com.anyoption.common.beans.UserMigration;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.bl_vos.UserRegulation;
import il.co.etrader.web.util.Constants;

public class UserRegulationForm implements Serializable{
	
	private static final long serialVersionUID = 4409179889681832352L;
	private static final Logger logger = Logger.getLogger(UserRegulationForm.class);
	

	public static String getETRegulationPopup(UserRegulationBase userRegulation) {

		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context,Constants.BIND_USER, User.class);
		String msg = null;
		if(isETRegulationDone()){
			return msg;
		} else if(!user.isAcceptedTerms()){
			return msg;
		}
		
		if(userRegulation == null){
			try {
				userRegulation =  new UserRegulation();
			} catch (SQLException e) {
				logger.error("Can't ger userReulation user",e);
			}
		}
		
		if(userRegulation.getApprovedRegulationStep() == null){
			//user is not regulate
			return msg;
		}
				
		HashMap<String, String> hmReturnMsg = null;
		String[] params = new String[1];		
		params[0] = userRegulation.getTresholdLimit() / 100 + "";
		if(UserRegulationManager.isNeedShowBinaryOptionsKnowledge(userRegulation, user.getSkinId())){
			hmReturnMsg = new HashMap<String, String>();
			hmReturnMsg.put("CODE_ID", UserRegulationBase.ET_REGULATION_KNOWLEDGE_QUESTION_USER + "");
			hmReturnMsg.put("ERROR_MSG", "");
			logger.debug("Need knowledge question filled for userID:" + user.getId());
		} else if(UserRegulationManager.isNeedShowCapitalQuestionnaire(userRegulation, user.getSkinId())){
			hmReturnMsg = new HashMap<String, String>();
			hmReturnMsg.put("CODE_ID", UserRegulationBase.ET_CAPITAL_MARKET_QUESTIONNAIRE + "");
			hmReturnMsg.put("ERROR_MSG", "");
			logger.debug("Need knowledge question filled for userID:" + user.getId());
		} else if(UserRegulationManager.isSuspendAfterCapitalQuestionnaire(userRegulation, user.getSkinId())){
			hmReturnMsg = new HashMap<String, String>();
			hmReturnMsg.put("CODE_ID", userRegulation.getSuspendedReasonId() + "");
			hmReturnMsg.put("ERROR_MSG", "put.here.msg.suspend");
			logger.debug("The user is suspend:" + userRegulation.getSuspendedReasonId() + " userID:" + user.getId());
		} else if(UserRegulationManager.isSuspendTresholdLowGroup(userRegulation, user.getSkinId())){
			hmReturnMsg = new HashMap<String, String>();
			hmReturnMsg.put("CODE_ID", userRegulation.getSuspendedReasonId() + "");			
			hmReturnMsg.put("ERROR_MSG",CommonUtil.getMessage("CMS.popup.popup_capital_questionnaire_weight.code_block_11.text2", params));
			logger.debug("The user is suspend:" + userRegulation.getSuspendedReasonId() + " userID:" + user.getId());
		} else if(UserRegulationManager.isSuspendTresholdMediumGroup(userRegulation, user.getSkinId())){
			hmReturnMsg = new HashMap<String, String>();
			hmReturnMsg.put("CODE_ID", userRegulation.getSuspendedReasonId() + "");
			hmReturnMsg.put("ERROR_MSG",CommonUtil.getMessage("CMS.popup.popup_capital_questionnaire_weight.code_block_12.text2", params));
			logger.debug("The user is suspend:" + userRegulation.getSuspendedReasonId() + " userID:" + user.getId());
		}
		
		if(hmReturnMsg != null){
			Gson gson = new GsonBuilder().create();
			msg = gson.toJson(hmReturnMsg);
		}
		return msg;
	}
	
	
	public static boolean isETRegulationDone(){		
		boolean res = false;
		try {
			FacesContext context = FacesContext.getCurrentInstance();			
			HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
			res = (Boolean) session.getAttribute(Constants.SESSION_IS_ET_REGULATION_DONE);
		} catch (Exception e) {
			res = false;
		}
		return res; 
	}
	
	public static boolean isSingleQuestDone(){		
		boolean res = false;
		try {
			FacesContext context = FacesContext.getCurrentInstance();			
			HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
			res = (Boolean) session.getAttribute(Constants.SESSION_IS_SINGLE_QUEST_DONE);
		} catch (Exception e) {
			res = false;
		}
		return res; 
	}
	
	public static String getAORegulationPopup(UserRegulationBase userRegulation) {

		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context,Constants.BIND_USER, User.class);
		String msg = null;
		if(isAORegulationDone()){
			return msg;
		} else if(!user.isAcceptedTerms()){
			return msg;
		}
		
		if(userRegulation == null){
			try {
				userRegulation =  new UserRegulation();
			} catch (SQLException e) {
				logger.error("Can't ger userReulation user",e);
			}
		}
		
		//GB Checks
		if(SkinsManagerBase.getSkin(user.getSkinId()).getBusinessCaseId() == Skin.SKIN_BUSINESS_GOLDBEAM){
			try {
				if(UsersManagerBase.isNonRegUserSuspend(user.getId())){
					HashMap<String, String> hmReturnMsg = new HashMap<String, String>();
					hmReturnMsg.put("CODE_ID", CommonJSONService.ERROR_CODE_REGULATION_SUSPENDED + "");
					hmReturnMsg.put("ERROR_MSG", "regulation.user.suspended");
					logger.debug("The user GB manuel isSuspended. Error code: ERROR_CODE_REGULATION_SUSPENDED: " + userRegulation);
					Gson gson = new GsonBuilder().create();
					msg = gson.toJson(hmReturnMsg);
					return msg;
				}
			} catch (SQLException e) {
				logger.error("When check GB suspend", e);
			}	
		}
		
		if(userRegulation.getApprovedRegulationStep() == null 
				|| (!user.getSkin().isRegulated() && SkinsManagerBase.getSkin(user.getSkinId()).getBusinessCaseId() != Skin.SKIN_BUSINESS_GOLDBEAM)){
			//user is not regulate and not GB
			logger.error("User " + user.getId() + " is not regulate");
			return msg;
		}
				
		HashMap<String, String> hmReturnMsg = UserRegulationManager.getAORegulationState(userRegulation, user.getSkinId());
		
		if(UserRegulationManager.isPepProhibited(userRegulation, user.getSkinId())){
			if(hmReturnMsg == null){
				hmReturnMsg = new HashMap<String, String>();
			}			
			hmReturnMsg.put("IS_PEP", "true");
			hmReturnMsg.put("PEP_ERROR_MSG", "");
		}
		
		if(userRegulation.isTresholdBlock()){
			if(hmReturnMsg == null){
				hmReturnMsg = new HashMap<String, String>();
			}			
			hmReturnMsg.put("IS_TRESHOLD", "true");
			hmReturnMsg.put("TRESHOLD_ERROR_MSG", CommonUtil.displayAmount(userRegulation.getThresholdBlock(), true, user.getCurrencyId(), false , "###,###,##0"));
		}
		
		if(hmReturnMsg != null){
			Gson gson = new GsonBuilder().create();
			msg = gson.toJson(hmReturnMsg);
		}
		return msg;
	}
	
	public static boolean isAORegulationDone(){		
		boolean res = false;
		try {
//			FacesContext context = FacesContext.getCurrentInstance();			
//			HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
//			res = (Boolean) session.getAttribute(Constants.SESSION_IS_ET_REGULATION_DONE);
		} catch (Exception e) {
			res = false;
		}
		return res; 
	}
	
	public static int getPlatform() {
		
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context,Constants.BIND_USER, User.class);
		HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
		synchronized(session) {
			UserMigration userMigration = (UserMigration) session.getAttribute(Constants.BIND_USER_MIGRATION);
			if(user != null && user.getId() != 0 && userMigration == null) {
				try {
					userMigration = UsersManagerBase.getUserMigration(user.getId());
					if(userMigration != null) {
						session.setAttribute(Constants.BIND_USER_MIGRATION, userMigration);
						Log.debug("Show cross sale popup for user:"+ user.getId());
						return userMigration.getTargetPlatform();
					} else { 
						return 0;
					}
				} catch (SQLException e) {
					logger.error("Unable to load user:", e);
				}
			} 
		}
		return 0;
	}
	
}
