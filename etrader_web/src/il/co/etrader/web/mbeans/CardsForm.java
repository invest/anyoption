package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.base.Skin;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.TransactionsManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;
import il.co.etrader.web.util.FormBase;

public class CardsForm extends FormBase implements Serializable {

	private static final Logger logger = Logger.getLogger(CardsForm.class);

	private CreditCard card;
	private ArrayList cardsList;


	public CardsForm() throws Exception{

		User user=ApplicationData.getUserFromSession();
		cardsList=TransactionsManager.getVisibleCreditCardsByUser(user.getId());

	}

	public int getListSize() {

		return cardsList.size();
	}


	public String updateCard() throws Exception{
		FacesContext context=FacesContext.getCurrentInstance();
		Map requestParameters = context.getExternalContext().getRequestParameterMap();
		String expYear = (String)requestParameters.get("cardsForm:expYear");
		String expMonth = (String)requestParameters.get("cardsForm:expMonth");
   		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, 1);

		Calendar expCal = Calendar.getInstance();
		expCal.clear();
		expCal.set(Integer.parseInt(expYear)+2000, Integer.parseInt(expMonth), 1);

    	if (cal.after(expCal)) {

    		FacesMessage fm=new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.expdate",null),null);
    		context.addMessage("cardsForm:exp", fm);
    		return null;
    	}

    	String utcoffset = ApplicationData.getUserFromSession().getUtcOffset();
		TransactionsManager.updateCardDetails(card.getId(),card.getExpMonth(),card.getExpYear(),card.getCcPass(), utcoffset);

		User user=ApplicationData.getUserFromSession();
		cardsList=TransactionsManager.getVisibleCreditCardsByUser(user.getId());

		return Constants.NAV_CARDS;

	}
	public String delete() throws Exception{
		FacesContext context=FacesContext.getCurrentInstance();
		User user=ApplicationData.getUserFromSession();

		int count = 0;
		Iterator iter = cardsList.iterator();
		CreditCard c = null;

		while(iter.hasNext()) {
			c = (CreditCard) iter.next();
			if (c.isSelected()) {
				count++;
				break;
			}
		}
		if (count == 0) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.card.nonselected",null),null);
			context.addMessage(null,fm);
            
			return null;
		}
		if (count >1) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.card.toomany",null),null);
			context.addMessage(null,fm);
            
			return null;
		}

		user.setDeleted4digitCard(c.getCcNumberLast4());
		user.setDeletedCardId(c.getId());

		return Constants.NAV_DELETE_CC_APPROVE;
	}

	public String approvedDelete() throws Exception{

		User user=ApplicationData.getUserFromSession();

		//	prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, Constants.NAV_DELETE_CC_APPROVE);
        }
        this.generateSaveToken();

		TransactionsManager.deleteCard(user.getDeletedCardId());

		return getNavigationPage();
	}

	public String notApprovedDelete() throws Exception{
		FacesContext context=FacesContext.getCurrentInstance();
		//	 prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, Constants.NAV_EMPTY_MYACCOUNT);
        }
        this.generateSaveToken();

	    String msg=CommonUtil.getMessage("ccdelete.cancel",null);
     	FacesMessage fm=new FacesMessage(FacesMessage.SEVERITY_INFO,msg,null);
     	context.addMessage(null, fm);
        
		return getNavigationPage();
	}



	public ArrayList getCardsList() {
		return cardsList;
	}

	public CreditCard getCard() {
		return card;
	}


	public void setCard(CreditCard c) {

		this.card = c;
	}

	public void setCardsList(ArrayList cardsList) {
		this.cardsList = cardsList;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "CardsForm ( "
	        + super.toString() + TAB
	        + "card = " + this.card + TAB
	        + "cardsList = " + this.cardsList + TAB
	        + " )";

	    return retValue;
	}

	/**
	 * Get navigation page
	 * @return
	 * 		navigation String
	 * @throws Exception
	 */
	public String getNavigationPage() throws Exception {
		String nav = "";
        User user = ApplicationData.getUserFromSession();
    	if (user.getSkinId() == Skin.SKIN_ETRADER) {
    		nav =  Constants.NAV_EMPTY_MYACCOUNT;
    	} else if (CommonUtil.isMobile()) {
    		nav = Constants.NAV_MYACCOUNT_MENU;
    	} else {
    		nav =  Constants.NAV_EMPTY_MYACCOUNT_CARDS;
    	}
    	return nav;

	}

}
