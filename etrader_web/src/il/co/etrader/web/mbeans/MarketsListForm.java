package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.TreeItem;
import com.anyoption.common.beans.base.OpportunityOddsPair;

import il.co.etrader.bl_managers.OpportunitiesManagerBase;
import il.co.etrader.bl_managers.SkinsManagerBase;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.InvestmentsManager;
import il.co.etrader.web.bl_vos.User;

public class MarketsListForm implements Serializable {
	private static final long serialVersionUID = 1L;

	ArrayList<TreeItem> listClone;
	ArrayList<SelectItem> optionType;
	ArrayList<SelectItem> returnRefunSelector;
	ArrayList<OpportunityOddsPair> returnRefunSelectorStatic = new ArrayList<OpportunityOddsPair>();

	public MarketsListForm() {
		updateList();
		// TODO delete
		ArrayList<SelectItem> optionTypes = new ArrayList<SelectItem>();
		SelectItem closest = new SelectItem("1","opportunity.type.closest");
		optionTypes.add(closest);
		SelectItem endOfDay = new SelectItem("2","opportunity.type.endofday");
		optionTypes.add(endOfDay);
		SelectItem endOfWeek = new SelectItem("3","opportunity.type.endofweek");
		optionTypes.add(endOfWeek);
		SelectItem endOfMonth = new SelectItem("4","opportunity.type.endofmonth");
		optionTypes.add(endOfMonth);
		this.optionType = optionTypes;
	}

	/**
	 * Get markets List by skinId and Bulid a new formated list.
	 */
	public void updateList() {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData a = context.getApplication().evaluateExpressionGet(context, ConstantsBase.BIND_APPLICATION_DATA,
																			ApplicationData.class);
		try {
			listClone = SkinsManagerBase.getTreeItems(a.getSkinId(), false);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		returnRefunSelectorStatic = OpportunitiesManagerBase.getOpportunityOddsPairSelector();
	}

	public ArrayList<Investment> getLastUserInvestments() throws Exception {
		User user = ApplicationData.getUserFromSession();
		ArrayList<Investment> l = null;
		l = InvestmentsManager.getLastUserInvestments(user.getId(), user.getSkinId());

		return l;
	}

	/**
	 * Return formated list
	 */
	public ArrayList<TreeItem> getListClone() {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData a = context.getApplication().evaluateExpressionGet(context, ConstantsBase.BIND_APPLICATION_DATA,
																			ApplicationData.class);
		try {
			listClone = SkinsManagerBase.getTreeItems(a.getSkinId(), false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listClone;
	}

	public ArrayList<SelectItem> getOptionType() {
		return optionType;
	}

	public void setReturnRefunSelector(ArrayList<SelectItem> returnRefunSelector) {
		this.returnRefunSelector = returnRefunSelector;
	}

	public ArrayList<SelectItem> getReturnRefunSelector() {
		return returnRefunSelector;
	}

	public ArrayList<OpportunityOddsPair> getReturnRefunSelectorStatic() {
		return returnRefunSelectorStatic;
	}

	public void setOptionType(ArrayList<SelectItem> optionType) {
		this.optionType = optionType;
	}
}