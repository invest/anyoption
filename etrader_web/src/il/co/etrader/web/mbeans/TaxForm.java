package il.co.etrader.web.mbeans;


import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import il.co.etrader.bl_managers.TaxHistoryManagerBase;
import il.co.etrader.bl_vos.TaxHistory;
import il.co.etrader.tax.TaxUtil;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_vos.User;


public class TaxForm  implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -470569709924808045L;

	private String tax;
	private String win;
	private String lose;
	private String winLose;
	private String sumInvest;
	private long year;
	private long userId;
	private ArrayList<SelectItem> yearsList;
	private String percent;

	public TaxForm() throws Exception {

		tax = "";
		win = "";
		lose = "";
		winLose = "";

		User user = ApplicationData.getUserFromSession();

		userId = user.getId();
		// get previous year
		Date time = CommonUtil.getDateTimeFormaByTz(GregorianCalendar.getInstance().getTime(), user.getUtcOffset());
		Calendar c = Calendar.getInstance();
		c.setTime(time);
		year = c.get(Calendar.YEAR) - 1;

		c.setTime(CommonUtil.getDateTimeFormaByTz(user.getTimeCreated(), user.getUtcOffset()));
		long regYear = c.get(Calendar.YEAR);

		yearsList = new ArrayList<SelectItem>();
		for (long y=year;y>=regYear-1;y--){
			yearsList.add(new SelectItem(new Long(y),String.valueOf(y)));
		}
		updateFields();
	}


	/**
	 * @return the percent
	 */
	public String getPercent() {
		return percent;
	}


	/**
	 * @return the lose
	 */
	public String getLose() {
		return lose;
	}

	/**
	 * @param lose the lose to set
	 */
	public void setLose(String lose) {
		this.lose = lose;
	}

	/**
	 * @return the tax
	 */
	public String getTax() {
		return tax;
	}

	/**
	 * @param tax the tax to set
	 */
	public void setTax(String tax) {
		this.tax = tax;
	}

	/**
	 * @return the win
	 */
	public String getWin() {
		return win;
	}

	/**
	 * @param win the win to set
	 */
	public void setWin(String win) {
		this.win = win;
	}

	/**
	 * @return the winLose
	 */
	public String getWinLose() {
		return winLose;
	}

	/**
	 * @param winLose the winLose to set
	 */
	public void setWinLose(String winLose) {
		this.winLose = winLose;
	}

	/**
	 * @return the numInvest
	 */
	public String getSumInvest() {
		return sumInvest;
	}

	/**
	 * @param numInvest the numInvest to set
	 */
	public void setSumInvest(String sumInvest) {
		this.sumInvest = sumInvest;
	}


	public String getReportData() throws Exception{
		updateFields();
		return null;
	}

	/**
	 * Update tax form fields
	 * @return
	 * @throws SQLException
	 */
	public void updateFields(ValueChangeEvent ev) throws Exception {

		year = new Long(String.valueOf(ev.getNewValue())).longValue();

		updateFields();
	}


	public String updateFields() throws Exception{

		TaxHistory t1 = TaxHistoryManagerBase.get(userId, year, ConstantsBase.TAX_JOB_PERIOD1);
		TaxHistory t2 = TaxHistoryManagerBase.get(userId, year, ConstantsBase.TAX_JOB_PERIOD2);

		tax = CommonUtil.displayAmount((t1.getTax() + t2.getTax()), ConstantsBase.CURRENCY_ILS_ID);
		win = CommonUtil.displayAmount((t1.getWin() + t2.getWin()), ConstantsBase.CURRENCY_ILS_ID);
		lose = CommonUtil.displayAmount((t1.getLose() + t2.getLose()), ConstantsBase.CURRENCY_ILS_ID);
		winLose = CommonUtil.displayAmount((t1.getWin() + t2.getWin()) - (t1.getLose() + t2.getLose()),
					ConstantsBase.CURRENCY_ILS_ID);
		sumInvest = CommonUtil.displayAmount(t1.getSumInvest(),ConstantsBase.CURRENCY_ILS_ID);
		if (year < 2012) {
			sumInvest = CommonUtil.displayAmount(t2.getSumInvest(),ConstantsBase.CURRENCY_ILS_ID);
		}
		
		percent = ConstantsBase.TAX_PERCENTAGE_AFTER_2012;
		if (year < 2012) {
			percent = ConstantsBase.TAX_PERCENTAGE_BEFORE_2012;
		}

		return null;
	}


	/**
	 * @return the year
	 */
	public long getYear() {
		return year;
	}


	/**
	 * @param year the year to set
	 */
	public void setYear(long year) {
		this.year = year;
	}


	/**
	 * @return the yearsList
	 */
	public ArrayList<SelectItem> getYearsList() {
		return yearsList;
	}

}