package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;

import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.payments.WithdrawEntryInfo;
import com.anyoption.common.payments.WithdrawEntryResult;
import com.anyoption.common.payments.WithdrawUtil;

import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.TransactionsManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;
import il.co.etrader.web.util.FormBase;

public class PayPalWithdrawForm extends FormBase  implements Serializable{
	private static final long serialVersionUID = 1L;

	private static final Logger logger = Logger.getLogger(PayPalWithdrawForm.class);

	private Transaction transaction;

	public PayPalWithdrawForm () throws Exception {
		transaction = new Transaction();
	}

	/**
	 * Clear wire form
	 */
	public void clearForm() {
		transaction = new Transaction();
	}

	public String getInitForm() throws Exception{
		clearForm();
		return Constants.NAV_EMPTY_MYACCOUNT_PAYPAL_WITHDRAWAL;
	}

	/**
	 * Insert a new paypal withdrawal request
	 * @return
	 * @throws SQLException
	 */
	public String insert() throws Exception {
		// prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();

        User user = ApplicationData.getUserFromSession();
        String error = TransactionsManager.validateWithdrawForm(transaction.getPayPalWAmount(), user, Constants.ENUM_WITH_FEE, false, true, null, null, null, null, Writer.WRITER_ID_WEB);
        
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
        if (error == null) {
        	WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
        	if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
        		error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
        		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(error, null),null);
        		context.addMessage(null, fm);
        	}
        }
        
		if (error != null) {
			TransactionsManager.insertPayPalWithdrawal(transaction.getPayPalWAmount(), transaction.getPayPalEmail(), error, user, CommonUtil.getWebWriterId(), false, loginId);
			return null;
		}

		// add transaction to contex
        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
        ve.getValue(context.getELContext());
        ve.setValue(context.getELContext(), transaction);
		return Constants.NAV_PAYPAL_APPROVE;
	}

	/**
	 * Approve paypal withdrawal request
	 * @return
	 * @throws Exception
	 */
	public String approve() throws Exception {

		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		
		transaction = (Transaction)context.getApplication().createValueBinding(Constants.BIND_TRANSACTION).getValue(context);

		String amount = transaction.getPayPalWAmount();
		if (amount == null) {
			return Constants.NAV_EMPTY_MYACCOUNT_PAYPAL_WITHDRAWAL;
		}

		//	prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, Constants.NAV_WITHDRAW_APPROVE);
        }
        this.generateSaveToken();

        User user = ApplicationData.getUserFromSession();

        TransactionsManager.insertPayPalWithdrawal(transaction.getPayPalWAmount(), transaction.getPayPalEmail(), null, user, CommonUtil.getWebWriterId(), false, loginId);

		long amountLong = CommonUtil.calcAmount(amount);
 	    String[] params = new String[2];
 	   long feeVal = TransactionsManagerBase.getFeeByTranTypeId(user.getLimitIdLongValue(), TransactionsManagerBase.TRANS_TYPE_PAYPAL_WITHDRAW);

        if (user.getSkinId() != Skin.SKIN_ARABIC){
        	params[0] = CommonUtil.displayAmount(amountLong, user.getCurrencyId());
            params[1] = CommonUtil.displayAmount(feeVal, user.getCurrencyId());
		} else {
			params[0] = CommonUtil.displayAmountForInput(amountLong, false, user.getCurrencyId().intValue()) + " " + CommonUtil.getMessage(user.getCurrency().getSymbol(),null) + " ";
	        params[1] = CommonUtil.displayAmountForInput(feeVal, false, user.getCurrencyId().intValue()) + " " + CommonUtil.getMessage(user.getCurrency().getSymbol(),null) + " ";
		}

     	FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("paypal.withdrawal.success",params),null);
     	context.addMessage(null, fm);

     	clearForm();

		return Constants.NAV_EMPTY_MYACCOUNT_PAYPAL_WITHDRAWAL;
	}

	/**
	 * Cancle bank wire request
	 * @return
	 * @throws SQLException
	 */
	public String notApprove() throws SQLException{
		FacesContext context=FacesContext.getCurrentInstance();
		//	 prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, Constants.NAV_EMPTY_MYACCOUNT_PAYPAL_WITHDRAWAL);
        }
        this.generateSaveToken();

	    String msg = CommonUtil.getMessage("paypal.withdrawal.cancel",null);
     	FacesMessage fm=new FacesMessage(FacesMessage.SEVERITY_INFO,msg,null);
     	context.addMessage(null, fm);

		return Constants.NAV_EMPTY_MYACCOUNT_PAYPAL_WITHDRAWAL;
	}


	/**
	 * Return dinamic message for payPal withdrawal
	 * @return
	 * 		formated message
	 * @throws Exception
	 */
	public String getMessage() throws Exception {
	    User user = ApplicationData.getUserFromSession();
		String fee = "";
		long feeVal = TransactionsManagerBase.getFeeByTranTypeId(user.getLimitIdLongValue(), TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW);
		if (user.getSkinId() != Skin.SKIN_ARABIC){
			fee = CommonUtil.displayAmount(feeVal, user.getCurrencyId());
		} else {
			fee = CommonUtil.displayAmountForInput(feeVal, false, user.getCurrencyId().intValue()) + " " + CommonUtil.getMessage(user.getCurrency().getSymbol(),null) + " ";
		}
		return CommonUtil.getMessage("paypal.withdrawal.approve.message", new String[] {String.valueOf(fee)});
	}

	public String getConvertionMsg() throws NumberFormatException, SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		transaction = (Transaction)context.getApplication().createValueBinding(Constants.BIND_TRANSACTION).getValue(context);
		double amountUSD = CurrencyRatesManagerBase.convertToBaseAmount(Long.parseLong(transaction.getPayPalWAmount()),
				Constants.CURRENCY_TRY_ID, new Date());
		return CommonUtil.getMessage("paypal.withdrawal.approve.message.convertion",
				new String[] {CommonUtil.displayAmount(CommonUtil.calcAmount(amountUSD), true, Constants.CURRENCY_USD_ID)});
	}

	public String getTRYConvertionMsg() throws Exception {
		double amountUSD = CurrencyRatesManagerBase.convertToBaseAmount(1, Constants.CURRENCY_TRY_ID, new Date());
		return CommonUtil.getMessage("paypal.withdrawal.TRY.message.convertion",
				new String[] {CommonUtil.displayAmount(CommonUtil.calcAmount(amountUSD), false, Constants.CURRENCY_USD_ID)});
	}

	/**
	 * @return the transaction
	 */
	public Transaction getTransaction() {
		return transaction;
	}


	/**
	 * @param transaction the transaction to set
	 */
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

}
