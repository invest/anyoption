package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.util.Constants;

public class LandingRegisterForm extends RegisterForm  implements Serializable{

	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(LandingRegisterForm.class);

	public LandingRegisterForm() throws Exception {
		super();
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		Locale locale = ap.getUserLocale();
		firstName = CommonUtil.getMessage("register.firstname", null, locale);
		lastName = CommonUtil.getMessage("register.lastname", null, locale);
		email = CommonUtil.getMessage("register.email", null, locale);
		userName = CommonUtil.getMessage("register.username", null, locale);
		street = CommonUtil.getMessage("register.street", null, locale);
		zipCode = CommonUtil.getMessage("register.postcode", null, locale);
		cityNameAO = CommonUtil.getMessage("register.city", null, locale);
		birthDay = CommonUtil.getMessage("register.birthday.short", null, locale);
		birthMonth = CommonUtil.getMessage("register.birthmonth.short", null, locale);
		birthYear = CommonUtil.getMessage("register.birthyear.short", null, locale);
	}

	public String validateLandinfFields() throws Exception{
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		Locale locale = ap.getUserLocale();
		if (CommonUtil.isParameterEmptyOrNull(firstName) || firstName.equalsIgnoreCase(CommonUtil.getMessage("register.firstname", null, locale))) {
			String[] params = new String[1];
			params[0] = CommonUtil.getMessage("register.firstname",null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.required", params),null);
			context.addMessage("landingRegisterForm:firstName", fm);
			return null;
		} else if (CommonUtil.isParameterEmptyOrNull(lastName) || lastName.equalsIgnoreCase(CommonUtil.getMessage("register.lastname", null, locale))) {
			String[] params = new String[1];
			params[0] = CommonUtil.getMessage("register.lastname",null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.required", params),null);
			context.addMessage("landingRegisterForm:lastName", fm);
			return null;
		}  else if (CommonUtil.isParameterEmptyOrNull(userName) || userName.equalsIgnoreCase(CommonUtil.getMessage("register.username", null, locale))) {
			String[] params = new String[1];
			params[0] = CommonUtil.getMessage("register.username",null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.required", params),null);
			context.addMessage("landingRegisterForm:userName", fm);
			return null;
		}  else if (CommonUtil.isParameterEmptyOrNull(password) || password.equalsIgnoreCase(CommonUtil.getMessage("register.password", null, locale))) {
			String[] params = new String[1];
			params[0] = CommonUtil.getMessage("register.password",null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.required", params),null);
			context.addMessage("landingRegisterForm:password", fm);
			return null;
		} else if (CommonUtil.isParameterEmptyOrNull(password2) || password2.equalsIgnoreCase(CommonUtil.getMessage("register.retype", null, locale))) {
			String[] params = new String[1];
			params[0] = CommonUtil.getMessage("register.retype",null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.required", params),null);
			context.addMessage("landingRegisterForm:password2", fm);
			return null;
		} else if (CommonUtil.isParameterEmptyOrNull(street) || street.equalsIgnoreCase(CommonUtil.getMessage("register.street", null, locale))) {
			String[] params = new String[1];
			params[0] = CommonUtil.getMessage("register.street",null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.required", params),null);
			context.addMessage("landingRegisterForm:street", fm);
			return null;
		} else if (CommonUtil.isParameterEmptyOrNull(cityNameAO) || cityNameAO.equalsIgnoreCase(CommonUtil.getMessage("register.city", null, locale))) {
			String[] params = new String[1];
			params[0] = CommonUtil.getMessage("register.city",null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.required", params),null);
			context.addMessage("landingRegisterForm:cityNameAO", fm);
			return null;
		}

		return super.insertUser();
	}

}