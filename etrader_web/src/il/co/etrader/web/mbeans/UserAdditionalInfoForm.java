package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.UsersDetailsHistory;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.UsersManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;
import il.co.etrader.web.util.Utils;

public class UserAdditionalInfoForm extends AbstractDepositForm implements Serializable{
	private static final long serialVersionUID = 4409179889681832352L;

	private static final Logger logger = Logger.getLogger(UserAdditionalInfoForm.class);

	private String birthYear;
	private String birthMonth;
	private String birthDay;
	private String streetNo;	
	private String idNum;
	private String gender;
	
	private String firstName;
	private String lastName;
	private String mobilePhonePrefix;
	private String mobilePhone;
	
	public UserAdditionalInfoForm() throws SQLException {
		super();

		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context,Constants.BIND_USER, User.class);

		this.idNum = user.getIdNum() == null || user.getIdNum().trim().isEmpty() || user.getIdNum().contains(Constants.ID_NUM_DEFAULT_VALUE) 
				? CommonUtil.getMessage("register.idnum", null).toUpperCase() 
				: user.getIdNum();
	   
		this.streetNo = user.getStreetNo() == null || user.getStreetNo().trim().isEmpty() 
				? CommonUtil.getMessage("register.streetno", null).toUpperCase() 
				: user.getStreetNo();

		if(user.getTimeBirthDate() != null) {
			birthYear = user.getBirthYear();
			birthMonth = user.getBirthMonth();
			birthDay = user.getBirthDay();
		}
		
		if(!CommonUtil.isParameterEmptyOrNull(user.getGender())){
			this.gender = user.getGender();
		} else {
			this.gender = Constants.GENDER_MALE;
		}
		
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.mobilePhonePrefix = user.getMobilePhonePrefix();
		this.mobilePhone = user.getMobilePhoneSuffix();
	}
	

	public String upadateUserInfo() throws Exception {
		
		logger.debug("Try to update UserAdditionalInfo Data");
		
		logger.debug("Etrader user additional fields are: "
				+ " birthYear [" + birthYear
				+ "], birthMonth [" + birthMonth
				+ "], birthDay [" + birthDay
				+ "], streetNo [" + streetNo + "]"
				+ "], gender [" + gender + "]"
				+ "], idNum [" + idNum + "]"
				
				+ "], firstName [" + firstName + "]"
				+ "], lastName [" + lastName + "]"
				+ "], mobilePhonePrefix [" + mobilePhonePrefix + "]"
				+ "], mobilePhone [" + mobilePhone + "]");
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage fm = null;
        
		User user = ApplicationData.getUserFromSession();
		
        HashMap<Long, String> userDetailsBeforeChangedHM = UsersManager.getUserDetailsHM(user.getSkinId(), user.getStreet(), ConstantsBase.NON_SELECTED, user.getGender(), null, user.getMobilePhone(), null, false, false, user.getBirthDay(), null, null, null, user.getIdNum(), 
                ConstantsBase.NON_SELECTED, user.getFirstName(), user.getLastName(), null, null);
		
        if (user.getTimeBirthDate() == null) {
            if (!CommonUtil.isParameterEmptyOrNull(getBirthDay())
                    && !CommonUtil.isParameterEmptyOrNull(getBirthMonth())
                    && !CommonUtil.isParameterEmptyOrNull(getBirthYear())) {
                if (Utils.birthDateValidation(getBirthYear(), getBirthMonth(), getBirthDay())){
                    
                    Calendar c = new GregorianCalendar(Integer.parseInt(getBirthYear()),
                            Integer.parseInt(getBirthMonth()) - 1,
                            Integer.parseInt(getBirthDay()), 0, 1); 
                    user.setTimeBirthDate(new Date(c.getTimeInMillis()));
                } else {
                    
                    FacesContext contx = FacesContext.getCurrentInstance();
                            fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            CommonUtil.getMessage("error.register.birthdate", null), null);
                    contx.addMessage("userAdditionalInfoForm:birthDay", fm); 
                    logger.debug("Error of register birthdate ");
                    return null;
                }
            
            } else if (!CommonUtil.isParameterEmptyOrNull(getBirthDay())
                    || !CommonUtil.isParameterEmptyOrNull(getBirthMonth())
                    || !CommonUtil.isParameterEmptyOrNull(getBirthYear())) {
                FacesContext context2 = FacesContext.getCurrentInstance();               
                if (CommonUtil.isParameterEmptyOrNull(getBirthDay())) {
                            fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            CommonUtil.getMessage("CMS.registerContent.text.error.register.day", null), null);
                    context2.addMessage("userAdditionalInfoForm:birthDay", fm);
                }
                if (CommonUtil.isParameterEmptyOrNull(getBirthMonth())) {
                            fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            CommonUtil.getMessage("error", null), null);
                    context2.addMessage("userAdditionalInfoForm:birthMonth", fm);
                }
                if (CommonUtil.isParameterEmptyOrNull(getBirthYear())) {
                            fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            CommonUtil.getMessage("error", null), null);
                    context2.addMessage("userAdditionalInfoForm:birthYear", fm);
                }
                logger.debug("Wrong of register birthdate ");
                return null;
            }
        }		
		
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		logger.debug("Try to validate ID-Num");
			if(!UsersManager.validateIdNum(idNum, "userAdditionalInfoForm", context, ap, user.getId())){
				logger.debug("Error to validate ID-Num");
				return null;
			} else {
				user.setIdNum(idNum);
			}
		
		
		
		if (CommonUtil.isParameterEmptyOrNull(streetNo)){
			logger.debug("streetNo is empty!");
			return null;
		} else {
			user.setStreetNo(streetNo);
		}
		
		
		if (CommonUtil.isParameterEmptyOrNull(mobilePhonePrefix) || CommonUtil.isParameterEmptyOrNull(mobilePhone)){
			logger.debug("Mobile phone is empty!");
			return null;
		} else {
			// used for the new reg funnel returning the phone prefix as number and code as 050 is not a number it is 50
			if (!mobilePhonePrefix.startsWith("0")) {
				mobilePhonePrefix = "0" + mobilePhonePrefix;
			}
			user.setMobilePhone(mobilePhonePrefix + mobilePhone);
		}
		
		
		if (CommonUtil.isParameterEmptyOrNull(firstName) || CommonUtil.isParameterEmptyOrNull(lastName)){
			logger.debug("First/Last name  is empty!");
			return null;
		} else {
			user.setFirstName(firstName);
			user.setLastName(lastName);
		}		
		
		user.setGender(this.gender);
		
		UsersManager.updateAdditionalFieldsEtrader(user);
		logger.debug("Updated Etrader user additionl fields");
		
		updateUserFields(user, true);
		logger.debug("Updated user fields");
		
		// change user details.
		ArrayList<UsersDetailsHistory> usersDetailsHistoryList = null;
        try {
            HashMap<Long, String> userDetailsAfterChangedHM = UsersManager.getUserDetailsHM(user.getSkinId(), user.getStreet(), ConstantsBase.NON_SELECTED, user.getGender(), null, user.getMobilePhone(), null, false, false, user.getBirthDay(), null, null, user.getCityName(), 
            		user.getIdNum(), ConstantsBase.NON_SELECTED, user.getFirstName(), user.getLastName(), null, user.getTimeBirthDate());
            usersDetailsHistoryList = UsersManager.checkUserDetailsChange(userDetailsBeforeChangedHM, userDetailsAfterChangedHM);
        } catch (Exception e) {
            logger.error("problem to save user details history.", e);
        }		
		if(!user.isAcceptedTerms()){
			return Constants.NAV_AGREEMENT;
		}else{
			return Constants.NAV_FIRST_DEPOSIT;
		}
	}

	public String getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(String birthYear) {
		this.birthYear = birthYear;
	}

	public String getBirthMonth() {
		return birthMonth;
	}

	public void setBirthMonth(String birthMonth) {
		this.birthMonth = birthMonth;
	}

	public String getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}
	
	public String getStreetNo() {
		return streetNo;
	}

	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}

	public String getIdNum() {
		return idNum;
	}

	public void setIdNum(String idNum) {
		this.idNum = idNum;
	}


	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getMobilePhonePrefix() {
		return mobilePhonePrefix;
	}


	public void setMobilePhonePrefix(String mobilePhonePrefix) {
		this.mobilePhonePrefix = mobilePhonePrefix;
	}


	public String getMobilePhone() {
		return mobilePhone;
	}


	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}


}
