package il.co.etrader.web.mbeans;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.LimitationDeposits;
import com.anyoption.common.beans.PaymentDeposit;
import com.anyoption.common.beans.TierUser;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.clearing.EPGClearingProvider;
import com.anyoption.common.clearing.EPGInfo;
import com.anyoption.common.clearing.EzeebillInfo;
import com.anyoption.common.clearing.InatecIframeInfo;
import com.anyoption.common.clearing.UkashInfo;
import com.anyoption.common.enums.Writers;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.managers.LimitationDepositsManagerBase;
import com.anyoption.common.managers.TransactionsManagerBase.TransactionSource;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.payments.epg.CheckOutParams;
import com.anyoption.common.util.ClearingUtil;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_managers.TiersManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.clearing.EnvoyInfo;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.TransactionsManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.bl_vos.UserRegulation;
import il.co.etrader.web.util.Constants;

public class DepositForm extends AbstractDepositForm implements Serializable{
    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(DepositForm.class);

	private String cardId;
	protected String deposit;
	private long depositPoints;
	private String comments;
	private long paymentTypeId;
	private String bankSortCode;
	private String accountNum;
	private String beneficiaryName;
	private String paymentTypeParam;
	private String envoyResultMsg;
	private UkashInfo ukashInfo;
	private String countryA2Code;
	public boolean diners;
	private String inatecIframeResultMsg;
	
	//BaroPay Deposit
	private String baroSenderName;
	private String baroLandLinePhone;
	private ArrayList<SelectItem> baroPrefixPhone;
	private String prefixPhone;
	private String birthYear;
	private String birthMonth;
	private String birthDay;
	private String ccPass;
	
	private String netellerEmail;
	private String netellerCode;
	
	private String paymentSolution;

	public String getEnvoyResultMsg() {
		return envoyResultMsg;
	}

	public void setEnvoyResultMsg(String envoyResultMsg) {
		this.envoyResultMsg = envoyResultMsg;
	}

	public DepositForm() throws Exception {
		super();
		comments="";
		cardId="";
		deposit="";
		//paymentSolution = "Neteller";
		//EncryptorUpdaterManager.doUpdate();

		User user = ApplicationData.getUserFromSession();
		if (user.isInTier()) {
			depositPoints = user.getTierUser().getPoints();
		}
		 
		// set predefined deposit amount
		if (user.getSkinId() != Skin.SKIN_ETRADER) {
			if (ApplicationData.getPredefinedDepositAmount().containsKey(user.getCurrencyId())) {
		        LimitationDeposits ld = LimitationDepositsManagerBase.getLimitByUserId(user.getId());
		        if (ld.getDefaultDeposit() == 0) {
		        	deposit = ApplicationData.getPredefinedDepositAmount().get(user.getCurrencyId());
		        } else {
		        	deposit = Long.toString(ld.getDefaultDeposit()/100);
		        }
			}
		}
		// Initialize Ukash obj and get countryId of user for link to ukash site
		FacesContext context = FacesContext.getCurrentInstance();
		
		// FIXME QUICK FIX
//		if (context.getViewRoot().getViewId().indexOf("ukash") > -1) {
			ukashInfo = new UkashInfo();
			countryA2Code = ApplicationData.getCountryA2Code(user.getCountryId());
//		}

		// LivePerson - set forms parameters when redirected to failed page
		if (context.getViewRoot().getViewId().indexOf("/depositF") > -1) {
			HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
			if (null !=  session && null != session.getAttribute(Constants.FAILED_DEPOSITS) &&
					((String)session.getAttribute(Constants.FAILED_DEPOSITS)).equals(String.valueOf(Constants.LIVEPERSON_FAILED_DEPOSITS)) ) {
				Transaction t = (Transaction) context.getApplication().createValueBinding(Constants.BIND_TRANSACTION).getValue(context);
				if (null != t) {
					deposit = String.valueOf(t.getAmount()/100);
					cardId = String.valueOf(t.getCreditCardId());
					diners=false;
				}
			}
		}
		initBaroPayPrefixPhone();
		
		if (isSetDefaultFieldValues()) {
		    if (this.accountNum == null || this.accountNum.isEmpty()) {
	            this.accountNum = CommonUtil.getMessage("direct.banking.account.num", null).toUpperCase();
	        }
	        if (this.bankSortCode == null || this.bankSortCode.isEmpty()) {
	            this.bankSortCode = CommonUtil.getMessage("direct.banking.sort.code", null).toUpperCase();
	        }
	        if (this.beneficiaryName == null || this.beneficiaryName.isEmpty()) {
	            this.beneficiaryName = CommonUtil.getMessage("direct.banking.beneficiary.name", null).toUpperCase();
	        }
	        
	        if (this.deposit == null || this.deposit.isEmpty()) {
	            this.deposit = CommonUtil.getMessage("deposit.deposit", null).toUpperCase();
	        }
	        
	        if (this.baroSenderName == null || this.baroSenderName.isEmpty()) {
	            this.baroSenderName = CommonUtil.getMessage("baropay.deposit.sender.name", null).toUpperCase();
	        }
	        
	        if (this.baroLandLinePhone == null || this.baroLandLinePhone.isEmpty()) {
	            this.baroLandLinePhone = CommonUtil.getMessage("baropay.deposit.phone.label", null).toUpperCase();
	        }
	        
			if (this.ccPass == null || this.ccPass.isEmpty()) {
				this.ccPass = CommonUtil.getMessage("creditcards.ccPass", null).toUpperCase();
			}
	        
	        if (ukashInfo != null){
	            if (this.ukashInfo.getVoucherNumber() == null || this.ukashInfo.getVoucherNumber().isEmpty()) {
	                this.ukashInfo.setVoucherNumber(CommonUtil.getMessage("ukash.voucher.number", null).toUpperCase());
	            }
	            
	            if (this.ukashInfo.getVoucherValue() == null || this.ukashInfo.getVoucherValue().isEmpty()) {
	                this.ukashInfo.setVoucherValue(CommonUtil.getMessage("ukash.voucher.value", null).toUpperCase());
	            }
	        }
		}
      
	}

	public DepositForm(String amount, long cardId, String ccPass) throws Exception {
		this();
		deposit = amount;
		this.cardId = String.valueOf(cardId);
		this.ccPass = ccPass;
	}

	public int getCcListSize() throws Exception{
		return getCcList().size();
	}

	public ArrayList getCcList() throws Exception{
		User user = ApplicationData.getUserFromSession();
		return TransactionsManager.getDisplayCreditCardsByUser(user.getId());
	}

	/**
	 * Insert deposit
	 * @throws Exception
	 */
	public String insertDeposit() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
    	// prevent identical requests from being processed
//        if (saveTokenIsInvalid() ) {
//            return logInvalidSaveAndReturn(logger, null);
//        }
//        generateSaveToken();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
        User user = ApplicationData.getUserFromSession();
        //Regulation
   		UserRegulation userRegulation = new UserRegulation();
    	if(user.getSkin().isRegulated()){
    		//AO Regulation User can't make deposits(<> FD) if note done steps	
			if (userRegulation.getApprovedRegulationStep() != null && UserRegulationBase.REGULATION_FIRST_DEPOSIT == userRegulation.getApprovedRegulationStep() 
					&& (userRegulation.isQualified() || userRegulation.getRegulationVersion() == UserRegulationBase.REGULATION_USER_VERSION)
					|| userRegulation.isSuspended()
					|| UserRegulationManager.isRestrictedAO(userRegulation, user.getSkinId())) {
       			logger.debug("Can't insert deposit! Regulation steps are not done for user: " + userRegulation);
       			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.unapproved", null), null);
    			context.addMessage(null, fm);
    			return null;
        
			}
    	}else{
			//Check non reg suspend
			if(UsersManagerBase.isNonRegUserSuspend(user.getId())){
				logger.debug("Can't insert deposit! User is manual supend user: " + user.getId());
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.unapproved", null), null);
				context.addMessage(null, fm);
				return null;
			}	
    	}
    	
    	try {
			Long.parseLong(ccPass);
		} catch (NumberFormatException e) {
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.cvv", null));
			context.addMessage(null, msg);
			return null;
		}

    	if (ccPass.length() != 3) {
    		FacesMessage msg =new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.cvvLength",null),null);
    		context.addMessage(null, msg);
			return null;
    	}
    	
    	Transaction t = TransactionsManager.insertDeposit(deposit, cardId, CommonUtil.getWebWriterId(), user, Constants.WEB_CONST, null, false, true, null, -1, ccPass, loginId);
    		
		if (t != null) { // transaction succeeded
			if(t.getId()==3){
				
				setDiners(true);
				setDeposit(null);
				
				return null;}
            ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
            ve.getValue(context.getELContext());
            ve.setValue(context.getELContext(), t);
            if (t.getStatusId() == TransactionsManager.TRANS_STATUS_AUTHENTICATE) {
            	if(t.isRender() ) {
            		return Constants.NAV_3D_SECURE_RENDER;
            	} else {
            		return Constants.NAV_3D_SECURE;
                }
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("updating user session details.");
                }
    			TransactionsManager.updateUserByName(user.getUserName(),user);

    			return receiptHttpsNav();
    			//return Constants.NAV_RECEIPT;
            }
		} else {
			if (context.getViewRoot().getViewId().indexOf("/deposit") > -1 &&
					!CommonUtil.isMobile()) { // just for web
				String failedDeposits = (String) session.getAttribute(Constants.FAILED_DEPOSITS);
				if (null != failedDeposits) {
					long failedNum = Long.parseLong(failedDeposits);
					failedNum++;
					session.setAttribute(Constants.FAILED_DEPOSITS, String.valueOf((failedNum)));  // set to first failed deposit
					if (failedNum == Constants.LIVEPERSON_FAILED_DEPOSITS) {  // redirect to failed deposit page for livePerson roles
						t = new Transaction();
						t.setAmount(CommonUtil.calcAmount(deposit));
						t.setCreditCardId(new BigDecimal(cardId));
			            ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
			            ve.getValue(context.getELContext());
			            ve.setValue(context.getELContext(), t);
						return Constants.NAV_DEPOSIT_FAILED;
					}
				} else {
					session.setAttribute(Constants.FAILED_DEPOSITS, "1");  // set to first failed deposit
				}
			}
			return null;
		}
	}

	/**
	 * Convert Loyalty points to cash action
	 * @return
	 * @throws Exception
	 */
	public String convertPointsToCashDeposit() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
    	// prevent identical requests from being processed
//        if (saveTokenIsInvalid() ) {
//            return logInvalidSaveAndReturn(logger, null);
//        }
//        generateSaveToken();
        User user = ApplicationData.getUserFromSession();
        String error = TransactionsManagerBase.validatePointsToCashAction("pointsToCashForm", depositPoints, user);
        if (null != error) {
        	return null;
        } else {
        	TierUser tU = user.getTierUser();
        	boolean res = TransactionsManager.insertPointsToCashDeposit(depositPoints, user, CommonUtil.getWebWriterId(), loginId);
        	double amount = TiersManagerBase.convertPointsToCash(depositPoints, tU.getTierId());
        	if (res) {
        		long updatedPoints = tU.getPoints() - depositPoints;
		    	tU.setPoints(updatedPoints);
		    	tU.setWriterId(CommonUtil.getWebWriterId());
		    	user.setTierUser(tU);
		    	String params[] = new String[2];
		    	params[0] = String.valueOf(depositPoints);
		    	params[1] = CommonUtil.displayAmount(CommonUtil.calcAmount(amount), user.getCurrencyId());
                ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
                ve.getValue(context.getELContext());
                ve.setValue(context.getELContext(), user);
		    	FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("points.to.cash.action.succeed", params),null);
		    	context.addMessage(null, fm);

        	}
        }
        return null;
	}

	/**
	 * Get direct banking deposit payment types
	 * @return
	 */
//	public ArrayList<SelectItem> getDirectDepositPaymentTypes() {
//		FacesContext context = FacesContext.getCurrentInstance();
//		User u = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
//		return ApplicationData.getDirectPaymentByCountryACurrency(u.getCountryId(), u.getCurrencyId(), u.getSkinId());
//	}

	/**
	 * Powepay21 (Inatec) direct banking deposit
	 * Sofortuberweisung, giropay, eps
	 * @return 
	 * @throws Exception
	 */
	public String insertPowepay21Deposit() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
        User user = ApplicationData.getUserFromSession();
        
    	if(getCurrencyId() !=  ConstantsBase.CURRENCY_EUR_ID) {
    		logger.error("UNSUPPORTED CURRENCY FOR DIRECT DEPOSIT:"+user.getCurrencyId() );
   			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.inatec.deposit", null), null);
			context.addMessage(null, fm);
    		return null;
    	}

        //Regulation
   		UserRegulation userRegulation = new UserRegulation();
    	if(user.getSkin().isRegulated()){
    		//AO Regulation User can't make deposits(<> FD) if note done steps	
			if (userRegulation.getApprovedRegulationStep() != null && UserRegulationBase.REGULATION_FIRST_DEPOSIT == userRegulation.getApprovedRegulationStep() 
					&& (userRegulation.isQualified() || userRegulation.getRegulationVersion() == UserRegulationBase.REGULATION_USER_VERSION)
					|| userRegulation.isSuspended()
					|| UserRegulationManager.isRestrictedAO(userRegulation, user.getSkinId())) {
       			logger.error("Can't insert insertPowepay21Deposit! Regulation steps are not done for user: " + userRegulation);
       			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.unapproved", null), null);
    			context.addMessage(null, fm);
    			return null;
			}
    	} else {
			//Check non reg suspend
			if(UsersManagerBase.isNonRegUserSuspend(user.getId())){
				logger.debug("Can't insert insertPowepay21Deposit! User is manual supend user: " + user.getId());
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.unapproved", null), null);
				context.addMessage(null, fm);
				return null;
			}
    	}

		if (user.getTimeBirthDate() == null && isFirstDeposit()) {
			try {
				Calendar c = new GregorianCalendar(Integer.parseInt(birthYear), Integer.parseInt(birthMonth) - 1, Integer.parseInt(birthDay), 0, 1);
				user.setTimeBirthDate(new Date(c.getTimeInMillis()));
			} catch (NumberFormatException e) {
				logger.error("ivalid DOB from parts: birthYear- " + birthYear + " birthMonth-" + birthMonth + " birthDay-" + birthDay);
			}
		}
        
		updateUserFields(user, false);		
		
		if (CommonUtil.getMessage("direct.banking.account.num", null).toUpperCase().equals(accountNum)) {
			accountNum = null;
		}
    	ApplicationData appData = context.getApplication().evaluateExpressionGet(context, Constants.BIND_APPLICATION_DATA, ApplicationData.class);
		String hpUrl = appData.getHomePageUrl();
		Transaction t = TransactionsManager.insertPowerpay21Deposit("direct24_" + paymentTypeParam, hpUrl, deposit, CommonUtil.getWebWriterId(), user, Constants.WEB_CONST, false, bankSortCode, accountNum, paymentTypeId, beneficiaryName, loginId);

		reloadIsFirstDeposit();
		
		if (null != t) {
            ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
            ve.getValue(context.getELContext());
            ve.setValue(context.getELContext(), t);
            if(t.getStatusId()==TransactionsManager.TRANS_STATUS_AUTHENTICATE) {
            	CommonUtil.sendPermanentRedirect((HttpServletResponse) context.getExternalContext().getResponse(), t.getRedirectUrl());
            }
		}

		return null;
	}	
	
	
	public String insertNetellerDeposit() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
        User user = ApplicationData.getUserFromSession();
        
        //Regulation
   		UserRegulation userRegulation = new UserRegulation();
    	if(user.getSkin().isRegulated()){
    		//AO Regulation User can't make deposits(<> FD) if note done steps	
			if (userRegulation.getApprovedRegulationStep() != null && UserRegulationBase.REGULATION_FIRST_DEPOSIT == userRegulation.getApprovedRegulationStep() 
					&& (userRegulation.isQualified() || userRegulation.getRegulationVersion() == UserRegulationBase.REGULATION_USER_VERSION)
					|| userRegulation.isSuspended()
					|| UserRegulationManager.isRestrictedAO(userRegulation, user.getSkinId())) {
       			logger.error("Can't insert insertPowepay21Deposit! Regulation steps are not done for user: " + userRegulation);
       			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.unapproved", null), null);
    			context.addMessage(null, fm);
    			return null;
			}
    	} else {
			//Check non reg suspend
			if(UsersManagerBase.isNonRegUserSuspend(user.getId())){
				logger.debug("Can't insert insertPowepay21Deposit! User is manual supend user: " + user.getId());
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.unapproved", null), null);
				context.addMessage(null, fm);
				return null;
			}
    	}

		if (user.getTimeBirthDate() == null && isFirstDeposit()) {
			try {
				Calendar c = new GregorianCalendar(Integer.parseInt(birthYear), Integer.parseInt(birthMonth) - 1, Integer.parseInt(birthDay), 0, 1);
				user.setTimeBirthDate(new Date(c.getTimeInMillis()));
			} catch (NumberFormatException e) {
				logger.error("ivalid DOB from parts: birthYear- " + birthYear + " birthMonth-" + birthMonth + " birthDay-" + birthDay);
			}
		}
        
		updateUserFields(user, false);		
		
		Transaction t = TransactionsManagerBase.insertNetellerDeposit("neteller", deposit, CommonUtil.getWebWriterId(), user, isFirstDeposit(), netellerEmail, netellerCode, loginId);

		reloadIsFirstDeposit();
		
		return null;
	}	
	
	/**
	 * Inatec direct banking deposit
	 * OLD Sofortuberweisung, giropay, eps
	 * @return 
	 * @throws Exception
	 */
	public String insertDirectDeposit() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
    	// prevent identical requests from being processed
//        if (saveTokenIsInvalid() ) {
//            return logInvalidSaveAndReturn(logger, null);
//        }
//        generateSaveToken();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
        User user = ApplicationData.getUserFromSession();
		ApplicationData gm = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		String hpUrl = gm.getHomePageUrl();
		if (!CommonUtil.isMobile()) {
			hpUrl += Constants.WEB_DIRECTORY;
		} else {
			hpUrl += Constants.MOBILE_DIRECTORY;
		}	

		if (user.getTimeBirthDate() == null && isFirstDeposit()) {
			try {
				Calendar c = new GregorianCalendar(Integer.parseInt(birthYear), Integer.parseInt(birthMonth) - 1, Integer.parseInt(birthDay), 0, 1);
				user.setTimeBirthDate(new Date(c.getTimeInMillis()));
			} catch (NumberFormatException e) {
				logger.error("ivalid DOB from parts: birthYear- " + birthYear + " birthMonth-" + birthMonth + " birthDay-" + birthDay);
			}
		}
        
		updateUserFields(user, false);
		
		if (CommonUtil.getMessage("direct.banking.account.num", null).toUpperCase().equals(accountNum)) {
			accountNum = null;
		}
		
		Transaction t = TransactionsManager.insertDirectDeposit(null, "direct24_" + paymentTypeParam, deposit, CommonUtil.getWebWriterId(), user,
				Constants.WEB_CONST, false, bankSortCode, accountNum, paymentTypeId, beneficiaryName, hpUrl, loginId);

		reloadIsFirstDeposit();
		
		if (null != t) {
            ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
            ve.getValue(context.getELContext());
            ve.setValue(context.getELContext(), t);
			if (t.getStatusId() == TransactionsManager.TRANS_STATUS_AUTHENTICATE) {
				logger.info("Inatec direct banking deposit, redirect to bank site: " );
				return Constants.NAV_INATEC_BANKING;
			} else {   // succeed
                if (logger.isDebugEnabled()) {
                    logger.debug("updating user session details.");
                }
    			TransactionsManager.updateUserByName(user.getUserName(),user);
    			return receiptHttpsNav();
    			//return Constants.NAV_RECEIPT;
			}
		} else { // failed
			return null;
		}
	}

	/**
	 * Moneybookers deposit
	 * @return
	 * @throws Exception
	 */
	public String insertMoneybookersDeposit() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
    	// prevent identical requests from being processed
//        if (saveTokenIsInvalid() ) {
//            return logInvalidSaveAndReturn(logger, null);
//        }
//        generateSaveToken();
		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
        User user = ap.getUserFromSession();
        
        if (user.getTimeBirthDate() == null && isFirstDeposit()) {
			try {
				Calendar c = new GregorianCalendar(Integer.parseInt(birthYear), Integer.parseInt(birthMonth) - 1, Integer.parseInt(birthDay), 0, 1);
				user.setTimeBirthDate(new Date(c.getTimeInMillis()));
			} catch (NumberFormatException e) {
				logger.error("ivalid DOB from parts: birthYear- " + birthYear + " birthMonth-" + birthMonth + " birthDay-" + birthDay);
			}
		}
		
		updateUserFields(user, false);
        
		Transaction tran = TransactionsManager.insertMoneybookersDeposit(deposit, CommonUtil.getWebWriterId(),
				user, ap.getUserLocale().getLanguage(), ap.getHomePageUrl(), Constants.WEB_CONST, "moneybookersDepositForm", ap.getImagesPath(), loginId);
		
		reloadIsFirstDeposit();
		
		if (tran != null){
            ValueExpression veTran = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
            veTran.getValue(context.getELContext());
            veTran.setValue(context.getELContext(), tran);
            if (tran.getStatusId() != TransactionsManager.TRANS_STATUS_AUTHENTICATE){ //failed
            	return null;
            }
			if (logger.isDebugEnabled()) {
				logger.debug("updating user session details.");
			}
			return Constants.NAV_MONEYBOOKERS_DEPOSIT;
		}

		return null;
	}

	public String getDeltaPayDepositMin() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
        User user = ap.getUserFromSession();
        String minAmonut = TransactionsManagerBase.getDeltaPayDepositMin(user);

        return CommonUtil.getMessage("error.creditcard.mindeposit", new Object[]{minAmonut});
	}
	
	public String insertCUPDeposit() throws Exception{
		User user = ApplicationData.getUserFromSession();
		if(user.getCurrencyId() != Currency.CURRENCY_CNY_ID) {
			logger.error("Unsupported user currency : " + user.getCurrencyId());
			return null;
		}
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		
		if (user.getTimeBirthDate() == null && isFirstDeposit()) {
			try {
				Calendar c = new GregorianCalendar(Integer.parseInt(birthYear), Integer.parseInt(birthMonth) - 1, Integer.parseInt(birthDay), 0, 1);
				user.setTimeBirthDate(new Date(c.getTimeInMillis()));
			} catch (NumberFormatException e) {
				logger.error("ivalid DOB from parts: birthYear- " + birthYear + " birthMonth-" + birthMonth + " birthDay-" + birthDay);
			}
		}
		
		String ip = CommonUtil.getIPAddress();
		Transaction tran = TransactionsManagerBase.insertEzeebillDeposit(deposit, Writers.WEB.getId(), ip, user, "cupForm", loginId) ;
		if( tran != null && tran.getStatusId() != TransactionsManagerBase.TRANS_STATUS_FAILED) {
			ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
			EzeebillInfo  ezeebillInfo = new EzeebillInfo(tran.getId(), user.getId(), tran.getAmount(), ap.getRhomePageUrl());
			
		    ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_EZEEBILL_INFO, EzeebillInfo.class);
		    ve.getValue(context.getELContext());
		    ve.setValue(context.getELContext(), ezeebillInfo); 
		    
			return "ezeepayDepositNav";
		} else {
			//  return deposit limit error
			return null;
		}
	}
	

	/**
	 * CDPay deposit
	public String insertCDPayDeposit() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		return insertCDPayDeposit(new Distribution(DistributionType.CUP, 30, 0), loginId);
	}
	*/
	
	/**
	 * CDPay deposit
	 * @return
	 * @throws Exception
	
	public String insertCDPayDeposit(Distribution distribution, long loginId) throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
    	// prevent identical requests from being processed
//        if (saveTokenIsInvalid() ) {
//            return logInvalidSaveAndReturn(logger, null);
//        }
//        generateSaveToken();
		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
        User user = ApplicationData.getUserFromSession();
		Transaction tran = TransactionsManager.insertCDPayDeposit(deposit, CommonUtil.getWebWriterId(),
				user, ap.getUserLocale().getLanguage(), ap.getHomePageUrl(), Constants.WEB_CONST, "deltaPayForm", ap.getImagesPath(), distribution.getSelectorId(), loginId);
		
		reloadIsFirstDeposit();
		
		if (tran != null) {
            ValueExpression veTran = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
            veTran.getValue(context.getELContext());
            veTran.setValue(context.getELContext(), tran);
            if (tran.getStatusId() != TransactionsManager.TRANS_STATUS_AUTHENTICATE){ //failed
            	return null;
            }
			if (logger.isDebugEnabled()) {
				logger.debug("updating user session details.");
			}
			return Constants.NAV_CDPAY_DEPOSIT;
		}

		return null;
	}
	 */
	/**
	 * DeltaPay deposit
	 * @return
	 * @throws Exception
	 
	private String insertDeltaPayChinaDeposit(Distribution distribution, long loginId) throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		
    	// prevent identical requests from being processed
//        if (saveTokenIsInvalid() ) {
//            return logInvalidSaveAndReturn(logger, null);
//        }
//        generateSaveToken();
		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
        User user = ApplicationData.getUserFromSession();
		Transaction tran = TransactionsManager.insertDeltaPayChinaDeposit(deposit, CommonUtil.getWebWriterId(),
				user, ap.getUserLocale().getLanguage(), ap.getHomePageUrlHttps(), Constants.WEB_CONST, "deltaPayForm", ap.getImagesPath(), distribution.getSelectorId(), distribution.getProviderId(), loginId);
		
		reloadIsFirstDeposit();
		
		if (tran != null) {
            ValueExpression veTran = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
            veTran.getValue(context.getELContext());
            veTran.setValue(context.getELContext(), tran);
            if (tran.getStatusId() != TransactionsManager.TRANS_STATUS_AUTHENTICATE){ //failed
            	return null;
            }
			if (logger.isDebugEnabled()) {
				logger.debug("updating user session details.");
			}
			return Constants.NAV_DELTAPAY_DEPOSIT;
		}
		return null;
	}*/

	/**
	 * WebMoney deposit
	 * @return
	 * @throws Exception
	 */
//	public String insertWebMoneyDeposit() throws Exception {
//		FacesContext context = FacesContext.getCurrentInstance();
//		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
//		long loginId = 0L;
//		try {
//			if (session.getAttribute(Constants.LOGIN_ID) != null) {
//				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
//			}
//		} catch (Exception e) {
//			logger.error("Cannot get login ID from session!", e);
//			loginId = 0L;
//		}
//    	// prevent identical requests from being processed
////        if (saveTokenIsInvalid() ) {
////            return logInvalidSaveAndReturn(logger, null);
////        }
////        generateSaveToken();
//		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
//        User user = ApplicationData.getUserFromSession();
//        
//        if (user.getTimeBirthDate() == null && isFirstDeposit()) {
//			try {
//				Calendar c = new GregorianCalendar(Integer.parseInt(birthYear), Integer.parseInt(birthMonth) - 1, Integer.parseInt(birthDay), 0, 1);
//				user.setTimeBirthDate(new Date(c.getTimeInMillis()));
//			} catch (NumberFormatException e) {
//				logger.error("ivalid DOB from parts: birthYear- " + birthYear + " birthMonth-" + birthMonth + " birthDay-" + birthDay);
//			}
//		}
//        
//        updateUserFields(user, false);
//        
//        long clearingProviderId = ApplicationData.getWebMoneyClearingProviderId().get(user.getCurrencyId());
//		Transaction tran = TransactionsManager.insertWebMoneyDeposit(deposit, CommonUtil.getWebWriterId(), user,
//				ap.getHomePageUrl(),  Constants.WEB_CONST, "webMoneyDepositForm", clearingProviderId, loginId);
//		
//		reloadIsFirstDeposit();
//		
//		if (tran != null) {
//            ValueExpression veTran = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
//            veTran.getValue(context.getELContext());
//            veTran.setValue(context.getELContext(), tran);
//            if (tran.getStatusId() != TransactionsManager.TRANS_STATUS_AUTHENTICATE){ //failed
//            	return null;
//            }
//			if (logger.isDebugEnabled()) {
//				logger.debug("updating user session details.");
//			}
//			return Constants.NAV_WEBMONEY_DEPOSIT;
//		}
//
//		return null;
//	}

//	/**
//	 * PayPal deposit
//	 * @return
//	 * @throws Exception
//	 */
//	public String insertPayPalDeposit() throws Exception {
//		FacesContext context = FacesContext.getCurrentInstance();
//    	// prevent identical requests from being processed
//        if (saveTokenIsInvalid() ) {
//            return logInvalidSaveAndReturn(logger, null);
//        }
//        generateSaveToken();
//        User user = ApplicationData.getUserFromSession();
//		ApplicationData gm = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
//		Transaction t = TransactionsManager.insertPayPalDeposit(null, "payPalDepositForm", deposit, CommonUtil.getWebWriterId(), user,
//				Constants.WEB_CONST, false);
//
//		if (null != t) {
//			context.getApplication().createValueBinding(Constants.BIND_TRANSACTION).setValue(context, t);
//			if (t.getStatusId() == TransactionsManager.TRANS_STATUS_AUTHENTICATE) {
//				logger.info("PayPal deposit, redirect to PayPal login site: " );
//				return Constants.NAV_PAYPAL_LOGIN;
//			} else {
//				logger.info("PayPal first request must be ended with autenticate status!");
//				return null;
//			}
//		} else { // failed
//			return null;
//		}
//	}

	/**
	 * Envoy direct banking deposit
	 * @return
	 * @throws Exception
	 */
	public String insertDirectDepositEnvoy() throws Exception {
    	// prevent identical requests from being processed
//        if (saveTokenIsInvalid() ) {
//            return logInvalidSaveAndReturn(logger, null);
//        }
//        generateSaveToken();
		
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		User user = ApplicationData.getUserFromSession();
		
		if (user.getTimeBirthDate() == null && isFirstDeposit()) {
			try {
				Calendar c = new GregorianCalendar(Integer.parseInt(birthYear), Integer.parseInt(birthMonth) - 1, Integer.parseInt(birthDay), 0, 1);
				user.setTimeBirthDate(new Date(c.getTimeInMillis()));
			} catch (NumberFormatException e) {
				logger.error("ivalid DOB from parts: birthYear- " + birthYear + " birthMonth-" + birthMonth + " birthDay-" + birthDay);
			}
		}
		
		updateUserFields(user, false);
		
        setEnvoyInfoForDeposit(null);

		logger.info("Envoy deposit redirect. " );
		return Constants.NAV_ENVOY_DEPOSIT;
	}

	/**
	 * Envoy direct banking deposit
	 * @return
	 * @throws Exception
	 */
	public void setEnvoyInfoForDeposit(String paymentMethod) throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		envoyResultMsg = null;
        User user = ApplicationData.getUserFromSession();
		EnvoyInfo cInfo = new EnvoyInfo(user, new Transaction());
		if (!CommonUtil.isParameterEmptyOrNull(paymentMethod)){
			cInfo.setService(paymentMethod);
		}
        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_ENVOY_INFO, EnvoyInfo.class);
        ve.getValue(context.getELContext());
        ve.setValue(context.getELContext(), cInfo); // for saving redirectUrl
	}
	
	/**
	 * Inatec - IDEAL
	 * @return
	 * @throws Exception
	 */
	public String insertInatecIframeDeposit() {		
		//TODO create one exit point.
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
			long loginId = 0L;
			try {
				if (session.getAttribute(Constants.LOGIN_ID) != null) {
					loginId = (long) session.getAttribute(Constants.LOGIN_ID);
				}
			} catch (Exception e) {
				logger.error("Cannot get login ID from session!", e);
				loginId = 0L;
			}
			ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
			User user = ApplicationData.getUserFromSession();
			
			if (user.getTimeBirthDate() == null && isFirstDeposit()) {
				try {
					Calendar c = new GregorianCalendar(Integer.parseInt(birthYear), Integer.parseInt(birthMonth) - 1, Integer.parseInt(birthDay), 0, 1);
					user.setTimeBirthDate(new Date(c.getTimeInMillis()));
				} catch (NumberFormatException e) {
					logger.error("ivalid DOB from parts: birthYear- " + birthYear + " birthMonth-" + birthMonth + " birthDay-" + birthDay);
				}
			}
			
			updateUserFields(user, false);		
	        
	        InatecIframeInfo info  = TransactionsManager.insertInatecIframeDeposit(deposit, CommonUtil.getWebWriterId(), user, ap.getUserLocale().getLanguage(),
	        		ap.getHomePageUrlHttps(), Constants.WEB_CONST, "inatecIframeDepositForm", loginId);
	        	   
	        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_INATEC_IFRAME_INFO, InatecIframeInfo.class);
	        ve.getValue(context.getELContext());
	        ve.setValue(context.getELContext(), info); 
	        if (info != null && info.getTransactionStatusId() != TransactionsManager.TRANS_STATUS_AUTHENTICATE) { //failed
            	return null;
            }
	        
	        StringBuilder body = info.createBodyBuilder();
	        logger.info("about to POST inatec iframe request:  URL: "  + info.getRedirectURL() + " body: " +  body.toString());
	        String response = ClearingUtil.executePOSTRequest(info.getRedirectURL(), body.toString());

	        if (!CommonUtil.isParameterEmptyOrNull(response)) {
	        	logger.info("Response from server: " + response);
		        String responseDecode = URLDecoder.decode(response, "UTF-8");		        
		        String redirectURL = createRedirectURL(responseDecode); 
		        logger.info("Inatec iframe deposit redirect to: "  + redirectURL);
		        HttpServletResponse httpServletResponse = (HttpServletResponse) context.getExternalContext().getResponse();
		        CommonUtil.sendPermanentRedirect(httpServletResponse, redirectURL);
		        return null;
	        }	 
	        
		} catch (Exception e) {
			 logger.error("ERROR! insertInatecIframeDeposit", e);
			//TODO return  nice msg to client.
		}
        return Constants.NAV_INATEC_IFRAME_DEPOSIT;
	}
	
	/**
	 * @return
	 */
	public ArrayList<String> getEPGPaymentSolutions() {
		ArrayList<String> paymentSolutions = new ArrayList<String>();
		FacesContext context = FacesContext.getCurrentInstance();
		//String request = "";
		try {
			User user = ApplicationData.getUserFromSession();
			EPGInfo info = new EPGInfo();
			ClearingManager.setEPGDepositDetails(info, ClearingManager.EPG_CHECKOUT_PROVIDER_ID);
			int productId = ApplicationData.getEpgProductId((int)user.getSkin().getBusinessCaseId(), user.getPlatformId());
			CheckOutParams checkOutParams = new CheckOutParams(
					EPGClearingProvider.DEBIT, 
					info.getMerchantId(), 
					String.valueOf(productId), 
					user.getCurrency().getCode(),
					ApplicationData.getCountry(user.getCountryId()).getA2());
			paymentSolutions = EPGClearingProvider.getEPGPaymentSolutions(checkOutParams, info);
		} catch (Exception e) {
			logger.error("ERROR! get EPG Payment Solutions", e);
			//TODO some msg. faces msgs
		}
		return paymentSolutions;
	}
	
	public String insertEpgCheckoutDeposit() {		
		//TODO create one exit point.
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
			long loginId = 0L;
			try {
				if (session.getAttribute(Constants.LOGIN_ID) != null) {
					loginId = (long) session.getAttribute(Constants.LOGIN_ID);
				}
			} catch (Exception e) {
				logger.error("Cannot get login ID from session!", e);
				loginId = 0L;
			}
			ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
			User user = ApplicationData.getUserFromSession();
			
			if (user.getTimeBirthDate() == null && isFirstDeposit()) {
				try {
					Calendar c = new GregorianCalendar(Integer.parseInt(birthYear), Integer.parseInt(birthMonth) - 1, Integer.parseInt(birthDay), 0, 1);
					user.setTimeBirthDate(new Date(c.getTimeInMillis()));
				} catch (NumberFormatException e) {
					logger.error("ivalid DOB from parts: birthYear- " + birthYear + " birthMonth-" + birthMonth + " birthDay-" + birthDay);
				}
			}
			
			updateUserFields(user, false);		
	        setPaymentTypeId(paymentSolution);
			//TODO check info null
			PaymentDeposit pd = new PaymentDeposit(deposit, CommonUtil.getWebWriterId(), user, 
					Constants.WEB_CONST, "epgDepositForm", loginId, ClearingManager.EPG_CHECKOUT_PROVIDER_ID, 
					TransactionsManager.TRANS_TYPE_EPG_CHECKOUT_DEPOSIT,
					paymentSolution, (int)paymentTypeId, ap.getHomePageUrlHttps());
			EPGInfo info = TransactionsManager.insertEpgCheckoutDeposit(pd); 
	        				
	        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_EPG_INFO, EPGInfo.class);
	        ve.getValue(context.getELContext());
	        ve.setValue(context.getELContext(), info); 
	        if (info.getTransactionStatusId() != TransactionsManager.TRANS_STATUS_AUTHENTICATE) { //failed
            	return null;
            }        
	        HttpServletResponse httpServletResponse = (HttpServletResponse) context.getExternalContext().getResponse();
	        CommonUtil.sendPermanentRedirect(httpServletResponse, info.getAcsUrl());	             
		} catch (Exception e) {
			 logger.error("ERROR! insertInatecIframeDeposit", e);
		}
		return null;
        //return Constants.NAV_;
	}
	
	//FIXME need to create a map table between payment type to payment method.
	//Don't want to maintain this method.
	private void setPaymentTypeId(String paymentSolution) {
		switch (paymentSolution.toLowerCase()) {
			case "neteller" :
				this.paymentTypeId = TransactionsManager.PAYMENT_TYPE_NETELLER;
				break;
		}	
	}

	private String createRedirectURL(String responseDecode) {
		String redirectURL = "";
		String[] params = responseDecode.split("start_url=");
		if (params.length > 1) {
			redirectURL = params[1];
		}
    	return redirectURL;
		
		/*String redirectURL = "";
		String[] params = responseDecode.split("&");
    	HashMap<String, String> pairParams = new HashMap<String, String>();
    	for (String param : params) {
    		String[] pair = param.split("=");
    		String value = "";
    		if (pair.length > 1) {
    			value = pair[1];
    		}
    		pairParams.put(pair[0], value);
		}
    	redirectURL = pairParams.get("start_url");
    	return redirectURL;
    	
    	*/  	
	}

	/**
	 * @param paymentMethod
	 * @throws Exception
	 */
	//TODO may remove.
	public void setInatecIframeInfoForDeposit(String paymentMethod) throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		//inatecIframeResultMsg = null;
        User user = ApplicationData.getUserFromSession();
		InatecIframeInfo info = new InatecIframeInfo(user, new Transaction());
		if (!CommonUtil.isParameterEmptyOrNull(paymentMethod)){
			//info.setService(paymentMethod);
		}
        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_ENVOY_INFO, EnvoyInfo.class);
        ve.getValue(context.getELContext());
        ve.setValue(context.getELContext(), info); // for saving redirectUrl
	}	

	/**
	 * @return the cardId
	 */
	public String getCardId() {
		return cardId;
	}

	/**
	 * @param cardId the cardId to set
	 */
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	/**
	 * @return the deposit
	 */
	public String getDeposit() {
		return deposit;
	}

	/**
	 * @param deposit the deposit to set
	 */
	public void setDeposit(String deposit) {
		this.deposit = deposit;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the paymentTypeId
	 */
	public long getPaymentTypeId() {
		return paymentTypeId;
	}

	/**
	 * @param paymentTypeId the paymentTypeId to set
	 */
	public void setPaymentTypeId(long paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	}

	/**
	 * @return the bankSortCode
	 */
	public String getBankSortCode() {
		return bankSortCode;
	}

	/**
	 * @param bankSortCode the bankSortCode to set
	 */
	public void setBankSortCode(String bankSortCode) {
		this.bankSortCode = bankSortCode;
	}

	/**
	 * @return the accountNum
	 */
	public String getAccountNum() {
		return accountNum;
	}

	/**
	 * @param accountNum the accountNum to set
	 */
	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}

	/**
	 * @return the beneficiaryName
	 */
	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	/**
	 * @param beneficiaryName the beneficiaryName to set
	 */
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	/**
	 * @return the paymentTypeParam
	 */
	public String getPaymentTypeParam() {
		return paymentTypeParam;
	}

	/**
	 * @return the depositPoints
	 */
	public long getDepositPoints() {
		return depositPoints;
	}

	/**
	 * @param depositPoints the depositPoints to set
	 */
	public void setDepositPoints(long depositPoints) {
		this.depositPoints = depositPoints;
	}

	/**
	 * @return the ukashInfo
	 */
	public UkashInfo getUkashInfo() {
		return ukashInfo;
	}

	/**
	 * @param ukashInfo the ukashInfo to set
	 */
	public void setUkashInfo(UkashInfo ukashInfo) {
		this.ukashInfo = ukashInfo;
	}

	/**
	 * Set payment type parameter that put with param (paymentType)
	 * and set the paymentId for deposit operation.
	 * @param paymentTypeParam the paymentTypeParam to set
	 */
	public void setPaymentTypeParam(String paymentTypeParam) {
		this.paymentTypeParam = paymentTypeParam;
		if (paymentTypeParam.equals(String.valueOf(TransactionsManagerBase.PAYMENT_TYPE_GIROPAY))) {
			paymentTypeId = TransactionsManagerBase.PAYMENT_TYPE_GIROPAY;
		} else if (paymentTypeParam.equals(String.valueOf(TransactionsManagerBase.PAYMENT_TYPE_DIRECT24))) {
			paymentTypeId = TransactionsManagerBase.PAYMENT_TYPE_DIRECT24;
		} else if (paymentTypeParam.equals(String.valueOf(TransactionsManagerBase.PAYMENT_TYPE_EPS))) {
			paymentTypeId = TransactionsManagerBase.PAYMENT_TYPE_EPS;
		}
	}

	/**
	 * Return ukash msg that redirects you to Ukash site
	 * @return
	 * @throws Exception
	 */
	public String getUkashSiteMsg() throws Exception{
		String[] params = new String[3];
		//Put country code for supported countries
		if (null != countryA2Code){
			params[0] = countryA2Code;
			params[1] = ApplicationData.getLocaleList().get(countryA2Code).getLanguage();
			params[2] = "where-to-get";
			if (countryA2Code.equalsIgnoreCase("gb")){
				params[0] = "uk";
			} else if (countryA2Code.equalsIgnoreCase("cn")){
				params[0] = "zh";
				params[1] = "cn";
			} else if (countryA2Code.equalsIgnoreCase("gr")){
				params[1] = "gr";
			} else if (countryA2Code.equalsIgnoreCase("es")){
				params[1] = "es";
			} else if (countryA2Code.equalsIgnoreCase("ua")){
				params[0] = "ru";
				params[1] = "ru";
			} else if (countryA2Code.equalsIgnoreCase("se")){
				params[1] = "en";
			}
			//exmaple: "http://www.ukash.com/uk/en/where-to-get.aspx";
		} else {
			params[0] = "global";
			params[1] = "en";
			params[2] = "where-to-get/global";
			//exmaple: http://www.ukash.com/global/en/where-to-get/global.aspx
		}
		return CommonUtil.getMessage("ukash.description",params);
	}

	public String getUkashMinAmountErrMsg() {
		String[] params = new String[2];
		params[0] = String.valueOf(ukashInfo.getMinAmount());
		params[1] = CommonUtil.getMessage(CommonUtil.getCurrencySymbol(this.getCurrencyId()), null);
		return CommonUtil.getMessage("ukash.error.min_amount", params);
	}

	/**
	 * fix for IE6 SP1 bug : security warnning (You are about to be redirected to a connection that is not secure...)
	 * instead of http request, send https request with secure port in the header and with sessionid param.
	 * the sessionid param is required for saving the current session.
	 */
	public String receiptHttpsNav() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData gm = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		if (CommonUtil.getProperty("use.https").equals("true")) {
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			response.setHeader("port", "443");
			String root = Constants.WEB_DIRECTORY;
			if (CommonUtil.isMobile()) {
				root = Constants.MINISITE_DIRECTORY + "/jsp";
			} else if (gm.isAPIGUI()) {
				root = Constants.API_DIRECTORY;				
			}
			response.sendRedirect(gm.getHomePageUrlHttps() + root +"/pages/receipt.jsf;" + gm.getSessionIdString());
			return null;
		}
		return Constants.NAV_RECEIPT;
	}


	public String getTRYConvertionMsg() throws Exception {
		double amountUSD = CurrencyRatesManagerBase.convertToBaseAmount(1, Constants.CURRENCY_TRY_ID, new Date());
		return CommonUtil.getMessage("paypal.deposit.TRY.message.convertion",
				new String[] {CommonUtil.displayAmount(CommonUtil.calcAmount(amountUSD), false, Constants.CURRENCY_USD_ID)});
	}

	/**
	 * @return Fax number
	 * @throws Exception
	 */
	public String getFaxNumberMsg() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		String[] params = new String[2];
		params[0] = ap.getSupportFax();
		params[1] = "general_link_blue";
		if (CommonUtil.isMobile()) {
            return CommonUtil.getMessage("wire.deposit.text.mobile",params);
		}
		return CommonUtil.getMessage("wire.deposit.text",params);
	}

	/**
	 * Ukash deposit
	 *
	 * @return
	 * @throws Exception
	 */
	public String insertUkashDeposit() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		// prevent identical requests from being processed
//		if (saveTokenIsInvalid()) {
//			return logInvalidSaveAndReturn(logger, null);
//		}
//		generateSaveToken();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		User user = ApplicationData.getUserFromSession();
		
		if (user.getTimeBirthDate() == null && isFirstDeposit()) {
			try {
				Calendar c = new GregorianCalendar(Integer.parseInt(birthYear), Integer.parseInt(birthMonth) - 1, Integer.parseInt(birthDay), 0, 1);
				user.setTimeBirthDate(new Date(c.getTimeInMillis()));
			} catch (NumberFormatException e) {
				logger.error("ivalid DOB from parts: birthYear- " + birthYear + " birthMonth-" + birthMonth + " birthDay-" + birthDay);
			}
		}
		
		updateUserFields(user, false);
		
		ukashInfo.setUserCurrencyCode(ApplicationData.getCurrencyById(user.getCurrencyId()).getCode());
		ukashInfo.setPayeeName(user.getFirstName() + " " + user.getLastName());
		Transaction t = TransactionsManager.insertDepositUkash(String.valueOf(ukashInfo.getVoucherValue()), CommonUtil.getWebWriterId(), user, Constants.WEB_CONST, ukashInfo,
					CommonUtil.calcAmount(ukashInfo.getVoucherValue()),
					ApplicationDataBase.getCurrencyIdByCode(ukashInfo.getVoucherCurrency()), loginId)	;
		
		reloadIsFirstDeposit();
		
		if (null != t) {
            ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
            ve.getValue(context.getELContext());
            ve.setValue(context.getELContext(), t);
			if (logger.isDebugEnabled()) {
				logger.debug("updating user session details.");
			}
			TransactionsManager.updateUserByName(user.getUserName(), user);
			return receiptHttpsNav();
		} else { // failed
			return null;
		}
	}

	/**
	 * Constructs a <code>String</code> with all attributes in name = value format.
	 *
	 * @return a <code>String</code> representation of this object.
	 */
	public String toString() {
	    final String TAB = " \n ";
	    String retValue = "";
	    retValue = "DepositForm ( "
	        + super.toString() + TAB
	        + "cardId = " + this.cardId + TAB
	        + "deposit = " + this.deposit + TAB
	        + "comments = " + this.comments + TAB
	        + " )";
	    return retValue;
	}

	/**
	 * Get transaction status from session and redirect to relevant page (receipt or error page)
	 * @return
	 * @throws Exception 
	 */
	public String getNavToReceiptByTrxStatus() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		Transaction tran = null;

		logger.info("Enter getNavToReceiptByTrxStatus() take Transaction from session");
		ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
		tran = (Transaction) ve.getValue(context.getELContext());
		User user = ApplicationData.getUserFromSession();
        TransactionsManager.afterDepositAction(user, tran, TransactionSource.TRANS_FROM_WEB);
		
        if (null != tran && tran.getStatusId() == TransactionsManagerBase.TRANS_STATUS_SUCCEED){
        	logger.info("receiptHttpsNav for Transaction id:" + tran.getId());
        	return receiptHttpsNav();
        } else {
        	String errMsg = tran == null ? " trx is null": "trx is not succesful:"+tran.getStatusId();
        	logger.info("receiptHttpsNav :" + errMsg);
    		FacesMessage fm = null;
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.unapproved", null), null);
			context.addMessage(null, fm);
        	return null;
        }
	}
	
	/**
	 * Get transaction status from session and display the page (receipt or processing message)
	 * 
	 * @return
	 * @throws Exception 
	 */
	public String getProcessingTransactionMsg() throws Exception {
		logger.debug("START - getProcessingTransactionMsge take Transaction from session");
		FacesContext context = FacesContext.getCurrentInstance();
		Transaction tran = null;
		
		ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
		tran = (Transaction) ve.getValue(context.getELContext());
		User user = ApplicationData.getUserFromSession();
        TransactionsManager.afterDepositAction(user, tran, TransactionSource.TRANS_FROM_WEB);
		
        if (null != tran && tran.getStatusId() == TransactionsManagerBase.TRANS_STATUS_SUCCEED){
        	logger.info("receiptHttpsNav for Transaction id:" + tran.getId());
        	return receiptHttpsNav();
        } else {      	
        	String processingMsg = " We are processing your transaction. Transaction id " + tran.getId(); //FIXME msg props
        	logger.info("getProcessingTransactionMsge - no receipt:" + processingMsg);
    		FacesMessage fm = null;
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO, processingMsg, null);
			context.addMessage(null, fm);
        	return null;
        }
	}
	
	/**
	 * Get transaction status from Database and redirect to relevant page (only for Deltapay)
	 * @return
	 * @throws IOException
	 */
	public String getNavToReceiptByDeltaPayTrxStatus() throws IOException {
		Transaction tran = null;

		logger.info("Enter getNavToReceiptByDelataPayTrxStatus() take Transaction from db");
		try {
			User user = ApplicationData.getUserFromSession();
			tran = TransactionsManager.getLastDeltaPayDeposit(user.getId());
			TransactionsManager.afterDepositAction(user, tran, TransactionSource.TRANS_FROM_WEB);
		} catch (Exception e) {
			logger.error("getNavToReceiptByDelataPayTrxStatus : Can't get user or transaction\n " + e);
		}

        if (null != tran && tran.getStatusId() == TransactionsManagerBase.TRANS_STATUS_SUCCEED){
        	logger.info("receiptHttpsNav for Transaction id:" + tran.getId());
        	return receiptHttpsNav();
        } else {
        	String errMsg = tran == null ? " trx is null": "trx is not succesful:"+tran.getStatusId();
        	logger.info("receiptHttpsNav :" + errMsg);
        	FacesContext context = FacesContext.getCurrentInstance();
    		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.unapproved", null), null);
			context.addMessage(null, fm);
        	return null;
        }
	}

	public boolean isDiners() {
		return diners;
	}

	public void setDiners(boolean diners) {
		this.diners = diners;
	}
	
	/**
	 * BaroPay deposit
	 * @return
	 * @throws Exception
	 */
//	public String insertBaroPayDeposit() throws Exception {
//		FacesContext context = FacesContext.getCurrentInstance();
//		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
//		long loginId = 0L;
//		try {
//			if (session.getAttribute(Constants.LOGIN_ID) != null) {
//				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
//			}
//		} catch (Exception e) {
//			logger.error("Cannot get login ID from session!", e);
//			loginId = 0L;
//		}
//    	// prevent identical requests from being processed
////        if (saveTokenIsInvalid() ) {
////            return logInvalidSaveAndReturn(logger, null);
////        }
////        generateSaveToken();
//		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
//        User user = ApplicationData.getUserFromSession();
//        
//        updateUserFields(user, false);
//        
//		Transaction tran = TransactionsManager.insertBaroPayDeposit(deposit, CommonUtil.getWebWriterId(),
//				user, ap.getUserLocale().getLanguage(), ap.getHomePageUrl(), Constants.WEB_CONST, "baroPayDepositForm", ap.getImagesPath(), baroSenderName, prefixPhone + baroLandLinePhone, loginId);
//		
//		reloadIsFirstDeposit();
//		
//		if (tran != null) {
//			if (logger.isDebugEnabled()) {
//				logger.debug("updating user session details.");
//			}
//            ValueExpression veTran = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
//            veTran.getValue(context.getELContext());
//            veTran.setValue(context.getELContext(), tran);
//            if (tran.getStatusId() == TransactionsManager.TRANS_STATUS_AUTHENTICATE){ 
//            	//Baro Pay success message     
//    	    	FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("baropay.success.confirmation.msg", null),null);
//    	    	context.addMessage(null, fm);
//    	    	return Constants.NAV_BAROPAY_DEPOSIT_SUCCESS;
//            }
//		}
//		return null;
//	}

	/**
	 * @return the baroSenderName
	 */
	public String getBaroSenderName() {
		return baroSenderName;
	}

	/**
	 * @param baroSenderName the baroSenderName to set
	 */
	public void setBaroSenderName(String baroSenderName) {
		this.baroSenderName = baroSenderName;
	}

	/**
	 * @return the baroLandLinePhone
	 */
	public String getBaroLandLinePhone() {
		return baroLandLinePhone;
	}

	/**
	 * @param baroLandLinePhone the baroLandLinePhone to set
	 */
	public void setBaroLandLinePhone(String baroLandLinePhone) {
		this.baroLandLinePhone = baroLandLinePhone;
	}

	/**
	 * @return the baroPrefixPhone
	 */
	public ArrayList<SelectItem> getBaroPrefixPhone() {
		return baroPrefixPhone;
	}

	/**
	 * @param baroPrefixPhone the baroPrefixPhone to set
	 */
	public void setBaroPrefixPhone(ArrayList<SelectItem> baroPrefixPhone) {
		this.baroPrefixPhone = baroPrefixPhone;
	}

	/**
	 * @return the prefixPhone
	 */
	public String getPrefixPhone() {
		return prefixPhone;
	}

	/**
	 * @param prefixPhone the prefixPhone to set
	 */
	public void setPrefixPhone(String prefixPhone) {
		this.prefixPhone = prefixPhone;
	}
	
	public void initBaroPayPrefixPhone() {
		baroPrefixPhone = new ArrayList<SelectItem>();
		baroPrefixPhone.add(new SelectItem("010", "010"));
		baroPrefixPhone.add(new SelectItem("011", "011"));
		baroPrefixPhone.add(new SelectItem("016", "016"));
		baroPrefixPhone.add(new SelectItem("017", "017"));
		baroPrefixPhone.add(new SelectItem("018", "018"));
		baroPrefixPhone.add(new SelectItem("019", "019"));
	}

	/**
	 * @return the inatecIframeResultMsg
	 */
	public String getInatecIframeResultMsg() {
		return inatecIframeResultMsg;
	}

	/**
	 * @param inatecIframeResultMsg the inatecIframeResultMsg to set
	 */
	public void setInatecIframeResultMsg(String inatecIframeResultMsg) {
		this.inatecIframeResultMsg = inatecIframeResultMsg;
	}

	public String getBirthMonth() {
		return birthMonth;
	}

	public void setBirthMonth(String birthMonth) {
		this.birthMonth = birthMonth;
	}

	public String getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	public String getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(String birthYear) {
		this.birthYear = birthYear;
	}

	public String getCcPass() {
		return ccPass;
	}

	public void setCcPass(String ccPass) {
		this.ccPass = ccPass;
	}

	public String getNetellerEmail() {
		return netellerEmail;
	}

	public void setNetellerEmail(String netellerEmail) {
		this.netellerEmail = netellerEmail;
	}

	public String getNetellerCode() {
		return netellerCode;
	}

	public void setNetellerCode(String netellerCode) {
		this.netellerCode = netellerCode;
	}

	/**
	 * @return the paymentSolution
	 */
	public String getPaymentSolution() {
		return paymentSolution;
	}

	/**
	 * @param paymentSolution the paymentSolution to set
	 */
	public void setPaymentSolution(String paymentSolution) {
		this.paymentSolution = paymentSolution;
	}
}