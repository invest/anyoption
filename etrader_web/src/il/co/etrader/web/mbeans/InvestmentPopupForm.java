package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.sql.SQLException;

import javax.faces.context.FacesContext;

import com.anyoption.common.beans.Investment;

import il.co.etrader.web.bl_managers.InvestmentsManager;

public class InvestmentPopupForm implements Serializable {
    private Investment investment;
    
    public Investment getInvestment() throws SQLException {
        if (null == investment) {
            String iId = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("investmentId");
            investment = InvestmentsManager.getInvestmentDetails(Long.parseLong(iId));
        }
        return investment;
    }
    
    public double getInvestmentCurrentLevel() throws SQLException {
        return getInvestment().getCurrentLevelValue();
    }
    
    public long getMarketId() throws SQLException {
        return getInvestment().getMarketId();
    }
    
    public long getScheduled() throws SQLException {
        return getInvestment().getScheduledId();
    }
    
    public long getDecimalPoint() throws SQLException {
        return getInvestment().getDecimalPoint();
    }
}