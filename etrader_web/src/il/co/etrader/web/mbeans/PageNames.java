package il.co.etrader.web.mbeans;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.anyoption.common.util.ClearingUtil;

import il.co.etrader.util.CommonUtil;

public class PageNames {
	private static final Logger log = Logger.getLogger(PageNames.class);

	private static HashMap<String, String> pageNames;
	private static HashMap<String, String> oldNames;

	public static void init(ServletContext servletContext) {
		log.info("Loading page names...");
		try {
	        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	        InputStream is = servletContext.getResourceAsStream("/WEB-INF/pretty-config.xml");
	        Document doc = docBuilder.parse(is);
	        try {
	        	is.close();
	        } catch (IOException ioe) {
	        	log.error("Can't close.", ioe);
	        }
	        pageNames = new HashMap<String, String>();
	        oldNames = new HashMap<String, String>();
	        NodeList mappings = doc.getElementsByTagName("url-mapping");
	        for (int i = 0; i < mappings.getLength(); i++) {
	        	Element e = (Element) mappings.item(i);
	        	String pattern = ClearingUtil.getElementValue(e, "/pattern/*");
	        	String view = ClearingUtil.getElementValue(e, "/view-id/*");
	        	String mobileView = ClearingUtil.getElementValue(e, "/mobile-view-id/*");
	        	String skin = ClearingUtil.getElementValue(e, "/skin/*");
	        	pattern = URLEncoder.encode(pattern, "UTF-8");
	        	pattern = pattern.replace("%2F", "/"); // do not escape the slash
	        	pattern = pattern.replace("%23", "#"); //do not escape number sign
	        	if (log.isTraceEnabled()) {
	        		log.trace("Processing rule pattern: " + pattern + " view: " + view+ " mobileView: " + mobileView);
	        	}
	        	pageNames.put(skin + view, pattern);
	        	if (!CommonUtil.isParameterEmptyOrNull(mobileView)) {
	        		pageNames.put(skin + mobileView, pattern);
	        	}
	        	if (log.isTraceEnabled()) {
	        		log.trace("Processing rule pattern: " + pattern + " view: " + view+ " mobileView: " + mobileView);
	        	}
	        	String oldPattern = ClearingUtil.getElementValue(e, "/old-pattern/*");
	        	if (!CommonUtil.isParameterEmptyOrNull(oldPattern)) {
	                if (log.isTraceEnabled()) {
	                    log.trace("Processing rule old-pattern: " + oldPattern + " pattern: " + pattern);
	                }
	                oldNames.put(oldPattern, pattern);
	        	}
	        	NodeList mappingsOld = e.getElementsByTagName("old-view-id");
	        	for (int j = 0; j < mappingsOld.getLength(); j++) {
	        		Element eOld = (Element) mappingsOld.item(j);
		        	String eOldView = ClearingUtil.getElementValue(eOld, "/*");
		        	eOldView = URLEncoder.encode(eOldView, "UTF-8");
		        	eOldView = eOldView.replace("%2F", "/"); // do not escape the slash
		        	if (!CommonUtil.isParameterEmptyOrNull(eOldView)) {
		        		if (log.isTraceEnabled()) {
		        			log.trace("Processing rule old-view: " + eOldView  + " skin:" + skin + " view: " + view);
		        		}
		        		pageNames.put(skin + eOldView, pattern);
		        	}
	        	}
	        }
		} catch (Exception e) {
			log.error("Can't load page names.", e);
		}
	}

	public String getPageName(String lng, String viewId) {
	    return pageNames.get(lng + viewId);
	}

	public static String getPageNameStatic(String lng, String viewId) {
	    return pageNames.get(lng + viewId);
	}

    public HashMap<String, String> getPageNames() {
        return pageNames;
    }

    public static String getNewNameStatic(String pattern) {
        return oldNames.get(pattern);
    }

    public static boolean hasPattern(String pattern) {
        return pageNames.values().contains(pattern);
    }
}