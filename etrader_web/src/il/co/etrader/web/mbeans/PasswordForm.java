package il.co.etrader.web.mbeans;
import java.io.Serializable;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.util.CommonUtil;

import il.co.etrader.bl_managers.ResetPasswordManager;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.UsersManager;
import il.co.etrader.web.util.Constants;

public class PasswordForm  implements Serializable{
	
	private static final long serialVersionUID = -666843824729578020L;

	private static final Logger logger = Logger.getLogger(PasswordForm.class);
	
    private boolean smsOrEmail; //true email, false sms
    private boolean sentPassword;
    

	public PasswordForm() {
		smsOrEmail = true;
		sentPassword = false;
	}

    public String sendPassword() throws Exception {
    	FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData app = (ApplicationData) context.getApplication().getExpressionFactory().createValueExpression
       		(context.getELContext(), Constants.BIND_APPLICATION_DATA, ApplicationData.class).getValue(context.getELContext());    			

    	HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
    	
		String email = (String) session.getAttribute("emailForgotPass");
		String phone = (String) session.getAttribute("phoneForgotPass");
		String prefix = (String) session.getAttribute("prefixForgotPass");
    	
    	String resKey = UsersManagerBase.generateRandomKey();
    	String resLink = app.getHomePageUrlHttps() + "jsp/resetPassword.jsf?" + Constants.RESET_KEY + "=" + resKey;
    	
    	if(!CommonUtil.IsParameterEmptyOrNull(email) && !CommonUtil.IsParameterEmptyOrNull(phone) && !CommonUtil.IsParameterEmptyOrNull(prefix)) {
	    	if (UsersManager.sendResetPassword(email, resLink, prefix, phone, smsOrEmail)) {
	    		String userName = UsersManager.getUserNameByEmail(email, app.getSkinId());
	    		ResetPasswordManager.insert(userName, resKey);
	    		sentPassword = true;
	    		logger.info("successfuly sent password reset");
	    	}
    	}
    	
    	session.removeAttribute("emailForgotPass");
    	session.removeAttribute("phoneForgotPass");
    	session.removeAttribute("prefixForgotPass");
		
		return null;
    }

	public boolean isSmsOrEmail() {
		return smsOrEmail;
	}

	public void setSmsOrEmail(boolean smsOrEmail) {
		this.smsOrEmail = smsOrEmail;
	}

	public boolean isSentPassword() {
		return sentPassword;
	}

	public void setSentPassword(boolean sentPassword) {
		this.sentPassword = sentPassword;
	}
}
