/**
 * 
 */
package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.QuestionnaireGroup;
import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.QuestionnaireManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.util.ConstantsBase;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.RiskAlertsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.bl_vos.UserRegulation;
import il.co.etrader.web.util.Constants;

public class RegSingleQuestionnaireForm implements Serializable {

	private static final long serialVersionUID = -3528432977605796788L;
	
	private static final int IS_CORECT_QUESTIONNAIRE = 2;
	private static final int IS_RESTRICTED_OR_FAILED_QUESTIONNAIRE = 1;
	
	private static final Logger log = Logger.getLogger(RegSingleQuestionnaireForm.class);
	private boolean submitted;

	private QuestionnaireGroup questionnaire;
	private List<QuestionnaireUserAnswer> userAnswers;
	private User user;
	
	/**
	 * @throws Exception
	 */
	public RegSingleQuestionnaireForm() throws Exception {
		questionnaire = QuestionnaireManagerBase
							.getAllQuestionnaires()
								.get(QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME);
		user = ApplicationData.getUserFromSession();
		userAnswers = QuestionnaireManagerBase.getUserAnswers(user.getId(), questionnaire.getQuestions(), Writer.WRITER_ID_WEB);
			
		submitted = FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(UserRegulation.REDIRECT_AFTER_REG_MAND_QUEST) != null; //logic for 7 second message
	}
	
	/**
	 * @return
	 */
	public List<QuestionnaireQuestion> getQuestions() {
		return questionnaire.getQuestions();
	}
	
	/**
	 * @return the userAnswers
	 */
	public List<QuestionnaireUserAnswer> getUserAnswers() {
		return userAnswers;
	}

	public boolean isSubmitted() {
		return submitted;
	}

	public void setSubmitted(boolean submitted) {
		this.submitted = submitted;
	}

	/**
	 * @return
	 * @throws Exception
	 */
	public String insertQuestionnaire() throws Exception {
		log.info("Inserting questionnaire [" + questionnaire.getId() + ":" + questionnaire.getName()
					+ "]  for user [" + user.getId() + "]");
		for (QuestionnaireUserAnswer userAnswer : userAnswers) {
			userAnswer.setWriterId(Writer.WRITER_ID_WEB);
		}
		
		List<QuestionnaireQuestion> questions = questionnaire.getQuestions();
		
		QuestionnaireManagerBase.updateUserAnswers(userAnswers, false);
		
		FacesContext context = FacesContext.getCurrentInstance();
		UserRegulation userRegulation = (UserRegulation) context
															.getApplication()
																.evaluateExpressionGet(
																	context,
																	Constants.BIND_USER_REGULATION,
																	UserRegulation.class);
		if (userRegulation.getApprovedRegulationStep() < UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE
				&& !userRegulation.isSuspended()) {
			userRegulation.setApprovedRegulationStep(UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE);
			userRegulation.setWriterId(Writer.WRITER_ID_WEB);
			//no optional questions in version 2
			userRegulation.setOptionalQuestionnaireDone(true);
			UserRegulationManager.updateRegulationStep(userRegulation);
			
			int isCorrectQuest = IS_CORECT_QUESTIONNAIRE;
			long restQuest = 0;
			//Check Answers
			if(QuestionnaireManagerBase.isSingelQuestionnaireIncorrectFilled(user.getId())){
				restQuest = UserRegulationBase.AO_REGULATION_RESTRICTED_FAILED_Q_SCORE_GROUP_ID;
				isCorrectQuest = IS_RESTRICTED_OR_FAILED_QUESTIONNAIRE;
				IssuesManagerBase.insertIssueUserRegulationRestricted(user.getId(), IssuesManagerBase.ISSUE_ACTION_TYPE_RESTRICTED_FAILED_QUESTIONNAIRE, Writer.WRITER_ID_AUTO);
				log.debug("User " + user.getId() + " was suspended(" + userRegulation.getSuspendedReasonId() + "): Incorrect Filled answers");
			} else { 
				//Check for Restricted Answers
				restQuest = QuestionnaireManagerBase.isSingelQuestionnaireRestrictedFilled(user.getId());
			}
			
			if(restQuest > 0){
				userRegulation.setScoreGroup(restQuest);
				QuestionnaireManagerBase.updateUserScoreGroupOnly(userRegulation);
				il.co.etrader.bl_managers.UsersManagerBase.sendMailAfterQuestAORegulation(userRegulation);
				isCorrectQuest = IS_RESTRICTED_OR_FAILED_QUESTIONNAIRE;
				log.debug("User " + user.getId() + " was restricted");
			}
						
			
			Date timeAnswearCreated = QuestionnaireManagerBase.isUserAnswerNoOnPEPQuestion(user.getId());
			if(timeAnswearCreated != null ){
				log.debug("The userId:" + user.getId() + " is answered NO on PEP! Create Issue, Auto Prohibited, Send Emails");
				userRegulation.setPepState(UserRegulationBase.PEP_AUTO_PROHIBITED);
				QuestionnaireManagerBase.updateUserPepAnswer(userRegulation);
				IssuesManagerBase.insertUserRegulationPEPRestrictedIssue(user.getId(), Writer.WRITER_ID_WEB, "WEB", false);
				try {
					log.debug("Try to send support Pep Mail");
					QuestionnaireManagerBase.sendEmailInCaseUserAnswearNoOnPEPQuestion(user, timeAnswearCreated, false);
					if(userRegulation.isQuestionaireIncorrect()){
						log.debug("The user is faild quest. and not will send user Pep Mail: "+ userRegulation);
					}else{
						log.debug("Try to send User Pep Mail");
						UsersManagerBase.sendMailTemplateFile(ConstantsBase.TEMPLATE_REGULATION_PEP, Writer.WRITER_ID_WEB, user, null, new String(), new String(), null);
					}										
				} catch (Exception e) {
					log.error("When send PEP emails", e);
				}
			} else {
				userRegulation.setPepState(UserRegulationBase.PEP_NON_PROHIBITED);
				QuestionnaireManagerBase.updateUserPepAnswer(userRegulation);
			}
			
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(UserRegulation.REDIRECT_AFTER_REG_MAND_QUEST, new Boolean(true));
			submitted = true;
			
			//Check due documents Limit
			if(isCorrectQuest == IS_CORECT_QUESTIONNAIRE){
				UserRegulationManager.checkUserDueDocumentsDepLimit(userRegulation);
			}
			
			HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
			session.setAttribute(Constants.SESSION_AO_REGULATION_QUESTIONNAIRE_RESULT, isCorrectQuest);
			session.setAttribute(Constants.SESSION_IS_SINGLE_QUEST_DONE, true);
		} else {
			log.warn("User regulation status is in unappropriate state [" + userRegulation
						+ "] for [mandatory] questionnaire. Cannot sunmit form. Reloading...");
		}
		
		return Constants.NAV_TRADE_PAGE;
	}
	
}
