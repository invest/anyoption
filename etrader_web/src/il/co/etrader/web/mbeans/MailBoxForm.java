package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.util.ArrayList;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.MailBoxUser;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.UsersManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;

public class MailBoxForm implements Serializable {

	private static final long serialVersionUID = 1;

	private static final Logger logger = Logger.getLogger(MailBoxForm.class);

	private ArrayList<MailBoxUser> emails;
	private MailBoxUser email;
	private String searchFilter;

	public MailBoxForm() throws Exception {
		updateMailBox();
	}

	public int getListSize() {
		return emails.size();
	}

	public ArrayList getEmails() {
		return emails;
	}

	public void setEmails(ArrayList<MailBoxUser> emails) {
		this.emails = emails;
	}

	public String updateMailBox() throws Exception {
		User user = ApplicationData.getUserFromSession();
		emails = UsersManager.getUserMailBox(user.getId(), searchFilter);
		CommonUtil.setTablesToFirstPage();
		return Constants.NAV_MAILBOX;
	}

	public String delete() throws Exception {
		return markEmails(Constants.MAILBOX_STATUS_DETETED);
	}

	public String read() throws Exception {
		return markEmails(Constants.MAILBOX_STATUS_READ);
	}

	public String unRead() throws Exception {
		return markEmails(Constants.MAILBOX_STATUS_NEW);
	}

	public String markEmails(long statusId) throws Exception {
		String emailsIds = "";
		User user = ApplicationData.getUserFromSession();
		for (MailBoxUser e : emails) {
			if (e.isSelected()) {
				emailsIds += e.getId() + ",";
			}
		}
		if (emailsIds.length() > 0) {
			emailsIds = emailsIds.substring(0, emailsIds.length() - 1);
			UsersManager.updateEmailsByUserId(user.getId(), statusId, emailsIds);
			updateMailBox();
		}
		return null;
	}

	public String openTemplate() throws Exception {
		User user = ApplicationData.getUserFromSession();
		if (email.getStatusId() == Constants.MAILBOX_STATUS_NEW) {
			try {
				UsersManager.updateEmailsByUserId(user.getId(), Constants.MAILBOX_STATUS_READ, String.valueOf(email.getId()));
			} catch (Exception e) {
				logger.error("Problem updating emailId: " + email.getId());
			}
		}
		FacesContext context = FacesContext.getCurrentInstance();
		user.setEmailParameters(email);

        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_MAILBOX_USER, MailBoxUser.class);
        ve.getValue(context.getELContext());
		ve.setValue(context.getELContext(), email);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.sendRedirect(context.getExternalContext().getRequestContextPath() + "/jsp/pages/email.jsf");
		return null;
	}


	/**
	 * @return the MailBoxUser
	 */
	public MailBoxUser getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(MailBoxUser email) {
		this.email = email;
	}

	public String getSearchFilter() {
		return searchFilter;
	}

	public void setSearchFilter(String searchFilter) {
		this.searchFilter = searchFilter;
	}

}
