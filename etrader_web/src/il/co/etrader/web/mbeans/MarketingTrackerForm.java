package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.sql.SQLException;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.anyoption.common.util.MarketingTracker;

public class MarketingTrackerForm implements Serializable {

	private static final long serialVersionUID = -4466409719375468520L;

	public String getInsertClick() throws SQLException {
		FacesContext fc = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) fc.getExternalContext().getRequest();
		String staticPart = request.getParameter("static");
		String dinamicPArt = request.getParameter("dinamicPart");

		MarketingTracker.insertMarketingTrackerClick(staticPart, dinamicPArt);
		return null;
	}
}