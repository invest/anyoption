/**
 * 
 */
package il.co.etrader.web.mbeans;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.jfree.util.Log;

import com.anyoption.common.beans.File;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.managers.FilesManagerBase;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.TransactionsManager;
import il.co.etrader.web.bl_managers.UsersManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.bl_vos.UserRegulation;
import il.co.etrader.web.util.Constants;
import il.co.etrader.web.util.FormBase;

/**
 * @author kirilim
 *
 */
public class DocumentsForm extends FormBase implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -193370385257225357L;
	private static final Logger logger = Logger.getLogger(DocumentsForm.class);
	
/*---------- Constants for the text field ids in xhtml page ----------------*/
	private static final String UTILITY_BILL_FILENAME_ID = "utilityFileName";
	private static final String UTILITY_BILL_FILESTATUS_ID = "utilityFileStatus";
	private static final String PASSORID_FILENAME_ID = "passOrIdFileName";
	private static final String PASSORID_FILESTATUS_ID = "passOrIdFileStatus";
//	private static final String DRIVERS_LICENSE_FILENAME_ID = "driversLicenseFileName";
//	private static final String DRIVERS_LICENSE_FILESTATUS_ID = "driversLicenseFileStatus";
	private static final String BANKWIRE_FILENAME_ID = "bankwireFileName";
	private static final String BANKWIRE_FILESTATUS_ID = "bankwireFileStatus";
	private static final String CC_FRONT_FILENAME_ID = "ccFrontFileName";
	private static final String CC_FRONT_FILESTATUS_ID = "ccFrontFileStatus";
	private static final String CC_BACK_FILENAME_ID = "ccBackFileName";
	private static final String CC_BACK_FILESTATUS_ID = "ccBackFileStatus";
	@SuppressWarnings("unused")
	private static final String CC_FRONT_HIDDEN = "hiddenCardIdFront";
	@SuppressWarnings("unused")
	private static final String CC_BACK_HIDDEN = "hiddenCardIdBack";
	private static final long UPLOAD_LIMIT = 700000;
	
	private static int NUM_SYSTEM_UNDERSCORES = 5;
	private static int REJECT_REASON_NOT_REJECTED = 0;
/*---------------- End of constants ------------------- --------------------*/
	
	private UploadedFile utilityBillFile;
	private String utilityFileName;
	private String utilityFileStatus;
	private boolean utilityLocked;
	// TODO strange case, should be figured out
	private UploadedFile passOrIdFile;
	private String passOrIdFileName;
	private String passOrIdStatus;
	private boolean passOrIdLocked;
	private UploadedFile driversLicenseFile;
	private String driversFileName;
	private String driversFileStatus;
	private boolean driversLocked;
	// TODO should be in hashmap<ccid, arraylist<file>>
	private HashMap<Long, HashMap<Long, File>> ccFilesMap;
	private UploadedFile frontCCFile;
	private UploadedFile backCCFile;
	private UploadedFile bankwireFile;
	private String bankwireFileName;
//	private String bankwireFileStatus;
	private long passOrId;
	private String passOrIdStr;
	private String cardId;
	private String uploadedDocumentName;
	private String uploadedDocumentNameId;
	private String uploadedDocumentStatus;
	private String uploadedDocumentStatusId;	
	private String frontCcFileName;
	private String frontCcFileStatus;
	private boolean frontCcLocked;
	private String backCcFileName;
	private String backCcFileStatus;
	private boolean backCcLocked;
	private boolean showAllApprovedText;
	private String summaryText;
	private boolean showCcFields;
	private boolean showBankwireField;
	private boolean questionnaireDone;
	private final boolean isRegulated;
	
	public DocumentsForm() {
		// TODO get files from db
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER, User.class);
		
		Locale locale = getLocale();	
		
		ccFilesMap = new HashMap<Long, HashMap<Long,File>>();
		showCcFields = false;
		showBankwireField = TransactionsManager.getTransactionCountByType(user.getId(), TransactionsManagerBase.TRANS_TYPE_WIRE_DEPOSIT) == 0 ? false : true;
		isRegulated = user.getSkin().isRegulated();

		
		if (!isRegulated) {			
			questionnaireDone = true;
		} else {
			UserRegulation userRegulation = null;
			try {
				userRegulation = new UserRegulation();
			} catch (SQLException e) {
				logger.debug("Can't get userRegulation", e);
			}
			
			if (null != userRegulation && userRegulation.getApprovedRegulationStep() != null) {
				if(userRegulation.getApprovedRegulationStep() >= UserRegulation.REGULATION_MANDATORY_QUESTIONNAIRE_DONE){
					questionnaireDone = true;
				}
			}
		}
		
		frontCcFileName = CommonUtil.getMessage("CMS.documents.files.no_file", null, locale);
		backCcFileName = CommonUtil.getMessage("CMS.documents.files.no_file", null, locale);
		frontCcFileStatus = CommonUtil.getMessage("CMS.documents.files.file.missing", null, locale);
		frontCcLocked = false;
		backCcFileStatus = CommonUtil.getMessage("CMS.documents.files.file.missing", null, locale);
		backCcLocked = false;
		
		try {
			ArrayList<SelectItem> ccList = getCcList();
			if (!ccList.isEmpty()) {
				cardId = ccList.get(0).getValue().toString();
				modifyCcValues();
			}
		} catch (Exception e) {
			logger.debug("Can't get cc list by user", e);
		}
		
		if (bankwireFileName == null) {
			bankwireFileName = CommonUtil.getMessage("CMS.documents.files.no_file", null, locale);;
		}
		
		passOrIdStr = String.valueOf(passOrId);		
	}


	private String initStatus(String fileName, boolean regulated, boolean approved, boolean controlApproved) {
		Locale locale = getLocale();
		String msg = "";
		if (fileName != null) {
			if (!fileName.equals(CommonUtil.getMessage("CMS.documents.files.no_file", null, locale))) {
				summaryText = CommonUtil.getMessage("CMS.documents.files.upload.success", null, locale);
			}
		}
			
			int statusType = FilesManagerBase.initStatus(fileName, regulated, approved, controlApproved);
			if(statusType == File.STATUS_DONE){
				msg = CommonUtil.getMessage("CMS.documents.files.file.done", null, locale);
			}
			
			if(statusType == File.STATUS_IN_PROGRESS){
				msg = CommonUtil.getMessage("CMS.documents.files.file.inprocess", null, locale);
			}
			
			if(statusType == File.STATUS_REQUESTED){
				msg = CommonUtil.getMessage("CMS.documents.files.file.missing", null, locale);
			}
			return msg;
		}

//	private String initStatus(int fileStatusId, boolean approved) {
//		Locale locale = getLocale();
//		switch (fileStatusId) {
//		case File.STATUS_REQUESTED:
//			return CommonUtil.getMessage("CMS.documents.files.file.missing", null, locale);
//		case File.STATUS_IN_PROGRESS:
////			if (approved) {
////				return CommonUtil.getMessage("CMS.documents.files.file.pending", null, locale);
////			} else {
//				summaryText = CommonUtil.getMessage("CMS.documents.files.upload.success", null, locale);
//				return CommonUtil.getMessage("CMS.documents.files.file.inprocess", null, locale);
////			}
//		case File.STATUS_DONE:
//			return CommonUtil.getMessage("CMS.documents.files.file.done", null, locale);
//		case File.STATUS_INVALID:
//			return "";//CommonUtil.getMessage("", null, locale);
//		default:
//			return "";
//		}
//	}


	public String upload() {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER, User.class);
		Locale locale = getLocale();
		uploadedDocumentName = "";
		try {
			if (utilityBillFile != null) {
				if (utilityBillFile.getSize() > UPLOAD_LIMIT) {
					logger.debug("Upload limit reached!");
					summaryText = CommonUtil.getMessage("CMS.documents.files.upload.problem", null, locale);
					summaryText += " (reached limit of " + UPLOAD_LIMIT / 1000 + " kb)";
				} else if (!com.anyoption.common.util.Utils.validateFileExtension(utilityBillFile.getName())) {
					logger.debug("File extension not allowed: " + utilityBillFile.getName());
					summaryText = CommonUtil.getMessage("CMS.documents.files.upload.problem", null, locale);
				} else {
					File file = UsersManager.getUserFileByType(user.getId(), FileType.UTILITY_BILL, locale);
					deleteCurrentFile(file.getFileName());
					String name = utilityBillFile.getName();
					name = saveFile(name, utilityBillFile, user.getId(), FileType.UTILITY_BILL);
					FilesManagerBase.setFileDefaultValues(file, name);
					file.setUploaderId(Writer.WRITER_ID_WEB);
					file.setUploadDate(new Date());
					UsersManager.updateFile(file);
					FilesManagerBase.clearKSFileData(file.getId());
					uploadedDocumentName = getFileName(utilityBillFile.getName());
					uploadedDocumentStatus = CommonUtil.getMessage("CMS.documents.files.file.inprocess", null, locale);
					uploadedDocumentNameId = UTILITY_BILL_FILENAME_ID;
					uploadedDocumentStatusId = UTILITY_BILL_FILESTATUS_ID;
					summaryText = CommonUtil.getMessage("CMS.documents.files.upload.success", null, locale);
				}
			} else if (passOrIdFile != null) {
				if (passOrIdFile.getSize() > UPLOAD_LIMIT) {
					logger.debug("Upload limit reached!");
					summaryText = CommonUtil.getMessage("CMS.documents.files.upload.problem", null, locale);
					summaryText += " (reached limit of " + UPLOAD_LIMIT / 1000 + " kb)";
				} else if (!com.anyoption.common.util.Utils.validateFileExtension(passOrIdFile.getName())) {
					logger.debug("File extension not allowed: " + passOrIdFile.getName());
					summaryText = CommonUtil.getMessage("CMS.documents.files.upload.problem", null, locale);
				} else if (Long.parseLong(passOrIdStr) < 0) {
					logger.debug("No file chosen!");
					summaryText = CommonUtil.getMessage("CMS.documents.files.upload.problem", null, locale);
					uploadedDocumentStatus = CommonUtil.getMessage("CMS.documents.files.file.select_type", null, locale);
					uploadedDocumentNameId = PASSORID_FILENAME_ID;
					uploadedDocumentStatusId = PASSORID_FILESTATUS_ID;
				} else {
					long type = getFileTypeByKey(passOrIdStr);
					passOrId = Long.parseLong(passOrIdStr);
					File file = UsersManager.getUserFileByType(user.getId(), type, locale);
					deleteCurrentFile(file.getFileName());
					String name = passOrIdFile.getName();
					name = saveFile(name, passOrIdFile, user.getId(), type);
					FilesManagerBase.setFileDefaultValues(file, name);
					file.setUploaderId(Writer.WRITER_ID_WEB);
					file.setUploadDate(new Date());
					UsersManager.updateFile(file);
					FilesManagerBase.clearKSFileData(file.getId());
					if (isRegulated && type == FileType.PASSPORT) {
						File driversFile = UsersManager.getUserFileByType(user.getId(), FileType.DRIVER_LICENSE, locale);
						driversFile.setFileStatusId(File.STATUS_NOT_NEEDED);
						UsersManager.updateFile(driversFile);
						File idFile = UsersManager.getUserFileByType(user.getId(), FileType.USER_ID_COPY, locale);
						idFile.setFileStatusId(File.STATUS_NOT_NEEDED);
						UsersManager.updateFile(idFile);
					} else if (isRegulated && type == FileType.USER_ID_COPY) {
						File driversFile = UsersManager.getUserFileByType(user.getId(), FileType.DRIVER_LICENSE, locale);
						driversFile.setFileStatusId(File.STATUS_NOT_NEEDED);
						UsersManager.updateFile(driversFile);
						File passFile = UsersManager.getUserFileByType(user.getId(), FileType.PASSPORT, locale);
						passFile.setFileStatusId(File.STATUS_NOT_NEEDED);
						UsersManager.updateFile(passFile);
                    } else if (isRegulated && type == FileType.DRIVER_LICENSE) {
						File passFile = UsersManager.getUserFileByType(user.getId(), FileType.PASSPORT, locale);
                        passFile.setFileStatusId(File.STATUS_NOT_NEEDED);
                        UsersManager.updateFile(passFile);
						File idFile = UsersManager.getUserFileByType(user.getId(), FileType.USER_ID_COPY, locale);
                        idFile.setFileStatusId(File.STATUS_NOT_NEEDED);
                        UsersManager.updateFile(idFile);
                    }
					uploadedDocumentName = getFileName(passOrIdFile.getName());
					uploadedDocumentStatus = CommonUtil.getMessage("CMS.documents.files.file.inprocess", null, locale);
					uploadedDocumentNameId = PASSORID_FILENAME_ID;
					uploadedDocumentStatusId = PASSORID_FILESTATUS_ID;
					summaryText = CommonUtil.getMessage("CMS.documents.files.upload.success", null, locale);
				}			
			} else if (bankwireFile != null) {
				if (bankwireFile.getSize() > UPLOAD_LIMIT) {
					logger.debug("Upload limit reached!");
					summaryText = CommonUtil.getMessage("CMS.documents.files.upload.problem", null, locale);
					summaryText += " (reached limit of " + UPLOAD_LIMIT / 1000 + " kb)";
				} else if (!com.anyoption.common.util.Utils.validateFileExtension(bankwireFile.getName())) {
					logger.debug("File extension not allowed: " + bankwireFile.getName());
					summaryText = CommonUtil.getMessage("CMS.documents.files.upload.problem", null, locale);
				} else {
					String name = bankwireFile.getName();
					name = saveFile(name, bankwireFile, user.getId(), FileType.BANKWIRE_CONFIRMATION);
					//UsersManager.insertFile(user.getId(), FileType.BANKWIRE_CONFIRMATION, 0, name);
					List<File> files = UsersManager.getUserFilesByType(user.getId(), FileType.BANKWIRE_CONFIRMATION, locale);
					configureEmail(user, files.get(files.size() - 1).getId());
					uploadedDocumentName = getFileName(bankwireFile.getName());
					uploadedDocumentStatus = CommonUtil.getMessage("CMS.documents.files.file.inprocess", null, locale);
					uploadedDocumentNameId = BANKWIRE_FILENAME_ID;
					uploadedDocumentStatusId = BANKWIRE_FILESTATUS_ID;
					summaryText = CommonUtil.getMessage("CMS.documents.files.upload.success", null, locale);
				}
			} else if (frontCCFile != null) {
				if (cardId != null && !cardId.isEmpty() && !cardId.equals("0")) {
					if (frontCCFile.getSize() > UPLOAD_LIMIT) {
						logger.debug("Upload limit reached!");
						summaryText = CommonUtil.getMessage("CMS.documents.files.upload.problem", null, locale);
						summaryText += " (reached limit of " + UPLOAD_LIMIT / 1000 + " kb)";
					} else if (!com.anyoption.common.util.Utils.validateFileExtension(frontCCFile.getName())) {
						logger.debug("File extension not allowed: " + frontCCFile.getName());
						summaryText = CommonUtil.getMessage("CMS.documents.files.upload.problem", null, locale);
					} else {
						HashMap<Long, File> map = ccFilesMap.get(Long.valueOf(cardId));
						File file = map.get(FileType.CC_COPY_FRONT);
						deleteCurrentFile(file.getFileName());
						String name = frontCCFile.getName();
						name = saveFile(name, frontCCFile, user.getId(), FileType.CC_COPY_FRONT);
						FilesManagerBase.setFileDefaultValues(file, name);	
						file.setUploaderId(Writer.WRITER_ID_WEB);
						file.setUploadDate(new Date());
						UsersManager.updateFile(file);
						FilesManagerBase.clearKSFileData(file.getId());
						uploadedDocumentName = getFileName(frontCCFile.getName());
						uploadedDocumentStatus = CommonUtil.getMessage("CMS.documents.files.file.inprocess", null, locale);
						uploadedDocumentNameId = CC_FRONT_FILENAME_ID;
						uploadedDocumentStatusId = CC_FRONT_FILESTATUS_ID;
						summaryText = CommonUtil.getMessage("CMS.documents.files.upload.success", null, locale);
					}
				}
			} else if (backCCFile != null) {
				if (cardId != null && !cardId.isEmpty() && !cardId.equals("0")) {
					if (backCCFile.getSize() > UPLOAD_LIMIT) {
						logger.debug("Upload limit reached!");
						summaryText = CommonUtil.getMessage("CMS.documents.files.upload.problem", null, locale);
						summaryText += " (reached limit of " + UPLOAD_LIMIT / 1000 + " kb)";
					} else if (!com.anyoption.common.util.Utils.validateFileExtension(backCCFile.getName())) {
						logger.debug("File extension not allowed: " + backCCFile.getName());
						summaryText = CommonUtil.getMessage("CMS.documents.files.upload.problem", null, locale);
					} else {
						HashMap<Long, File> map = ccFilesMap.get(Long.valueOf(cardId));
						File file = map.get(FileType.CC_COPY_BACK);
						deleteCurrentFile(file.getFileName());
						String name = backCCFile.getName();
						name = saveFile(name, backCCFile, user.getId(), FileType.CC_COPY_BACK);
						FilesManagerBase.setFileDefaultValues(file, name);
						file.setUploaderId(Writer.WRITER_ID_WEB);
						file.setUploadDate(new Date());
						UsersManager.updateFile(file);
						FilesManagerBase.clearKSFileData(file.getId());
						uploadedDocumentName = getFileName(backCCFile.getName());
						uploadedDocumentStatus = CommonUtil.getMessage("CMS.documents.files.file.inprocess", null, locale);
						uploadedDocumentNameId = CC_BACK_FILENAME_ID;
						uploadedDocumentStatusId = CC_BACK_FILESTATUS_ID;
						summaryText = CommonUtil.getMessage("CMS.documents.files.upload.success", null, locale);
					}
				}
			}
		} catch (SQLException sqle) {
			logger.debug("Can't update file", sqle);
			summaryText = CommonUtil.getMessage("CMS.documents.files.upload.problem", null, locale);
		} catch (NullPointerException npe) {
			logger.debug("Can't update file", npe);
		} catch (IOException ioe) {
			 logger.debug("Can't update file", ioe);
			 uploadedDocumentStatus = CommonUtil.getMessage("CMS.documents.files.upload.problem", null, locale);
			 summaryText = CommonUtil.getMessage("CMS.documents.files.upload.problem", null, locale);
			 if( utilityBillFile != null){uploadedDocumentStatusId = UTILITY_BILL_FILESTATUS_ID;}
			 if (passOrIdFile != null) {uploadedDocumentStatusId = PASSORID_FILESTATUS_ID;}
			 if (bankwireFile != null) {uploadedDocumentStatusId = BANKWIRE_FILESTATUS_ID;}
			 if (frontCCFile != null) {uploadedDocumentStatusId = CC_FRONT_FILESTATUS_ID;}
			 if (backCCFile != null) {uploadedDocumentStatusId = CC_BACK_FILESTATUS_ID;}
		} catch (Exception e) {
			logger.debug("Can't update file", e);
			uploadedDocumentStatus = CommonUtil.getMessage("CMS.documents.files.upload.problem", null, locale);
			summaryText = CommonUtil.getMessage("CMS.documents.files.upload.problem", null, locale);
		}
		return "documentUpload";
	}

	private String saveFile(String name, UploadedFile uploadedFile, long userId, long fileType) throws IOException, Exception {
			name = modifyFileName(name);
			SimpleDateFormat sd = new SimpleDateFormat("ddMMyyyy_HHmmss");
			name = "user_" + userId + "_doc_" + fileType + "_" + sd.format(new Date()) + "_" + name;
			name = getSubDirName() + name;		
			BufferedInputStream bis;

			bis = new BufferedInputStream(uploadedFile.getInputStream(),4096);
			java.io.File targetFile = new java.io.File(CommonUtil.getProperty(Constants.FILES_PATH) + name); // destination
			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(targetFile),4096);
			int bt;
			while ((bt = bis.read()) != -1) {
				bos.write(bt);
			}
			InputStream in = new FileInputStream(targetFile);
			in.read();
			bos.close();
			bis.close();
			in.close();
		return name;
	}

	private String modifyFileName(String name) {
		int lastChar = name.lastIndexOf('\\');
		if (lastChar != -1) {
			name = name.substring(lastChar + 1);
		}
		lastChar = name.lastIndexOf('/');
		if (lastChar != -1) {
			name = name.substring(lastChar + 1);
		}
		name = name.replaceAll("\\?", "");
		name = name.replaceAll(":", "");
		name = name.replaceAll("\\*", "");
		name = name.replaceAll("<", "");
		name = name.replaceAll(">", "");
		name = name.replaceAll("\"", "");
		name = name.replaceAll("|", "");
		name = name.replaceAll("\\+", "");
		name = name.replaceAll("\\s", ""); 
		name = name.replaceAll("[^a-zA-Z0-9.-]", "");
		return name;
	}

	private void deleteCurrentFile(String fileName) {
		if (fileName == null || fileName.isEmpty()) {
			return;
		}
		java.io.File targetFile = new java.io.File(CommonUtil.getProperty(Constants.FILES_PATH) + fileName);
		boolean result = targetFile.delete();
		if (!result) {
			logger.warn("Couldn't delete file!");
		}
	}
	
	public void updateFileType(ValueChangeEvent event) {
		if ((long) event.getNewValue() < 0) {
			//do nothing when no file type chosen
			logger.warn("No file chosen");
			return;
		}
		long newFileType = 0;
		long oldFileType = 0;
		long notSelectedFileType = -1;
		long i = 0;
		
		for (@SuppressWarnings("unused") SelectItem item : getPassOrIdSiOrDrive()){
		    if ((Long) event.getNewValue() == i){
		        newFileType = getFileTypeByKey(event.getNewValue().toString());
		    } else if ((Long) event.getOldValue() == i){
		        oldFileType = getFileTypeByKey(event.getOldValue().toString());
		    } else {
		        notSelectedFileType = getFileTypeByKey( String.valueOf(i));
		    }		    		    		    
		    i++;
		}
		
//		passOrIdStr = String.valueOf(type);
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER, User.class);
		File notSelectedFile = null;

		Locale locale = getLocale();
		File newFile = UsersManager.getUserFileByType(user.getId(), newFileType, locale);
		File oldfFile = UsersManager.getUserFileByType(user.getId(), oldFileType, locale);
		if (newFile == null || oldfFile == null) {
			return;
		}
		// modifying the new file
		newFile.setApproved(false);
		newFile.setFileName(oldfFile.getFileName());
		newFile.setFileStatusId(File.STATUS_IN_PROGRESS);
		newFile.setUploaderId(oldfFile.getUploaderId());
		newFile.setUploadDate(oldfFile.getUploadDate());
		// modifying the old file
		oldfFile.setApproved(false);
		oldfFile.setFileName(null);
		oldfFile.setUploaderId(0l);
		oldfFile.setFileStatusId(File.STATUS_NOT_NEEDED);
	
		
		if (notSelectedFileType > -1){
		    notSelectedFile = UsersManager.getUserFileByType(user.getId(), notSelectedFileType, locale);
		    notSelectedFile.setApproved(false);
		    notSelectedFile.setFileName(null);
		    notSelectedFile.setFileStatusId(File.STATUS_NOT_NEEDED);
		}
		
		// update files
		try {
			UsersManager.updateFile(newFile);
			UsersManager.updateFile(oldfFile);
			if (notSelectedFile != null){
		         UsersManager.updateFile(notSelectedFile);
			}
		} catch (SQLException e) {
			logger.debug("Can't update file", e);
			FacesMessage fm = new FacesMessage(CommonUtil.getMessage("CMS.documents.files.upload.problem", null, locale));
			context.addMessage("uploadProblem", fm);
		}
	}
	
	public String modifyFileType() {
		passOrIdStr = String.valueOf(passOrId);
		return "modifyFileType";
	}
	
	public void updateCcDocuments(ValueChangeEvent event) {
		String ccId = (String) event.getNewValue();
		cardId = ccId;
	}
	
	public String modifyCcValues() {
		HashMap<Long, File> map = ccFilesMap.get(Long.valueOf(cardId));
		Locale locale = getLocale();
		if (map != null) {
			File frontFile = map.get(FileType.CC_COPY_FRONT);
			if (frontFile != null && frontFile.getFileName() != null) {
				frontCcFileName = getFileName(frontFile.getFileName());
			} else {
				frontCcFileName = CommonUtil.getMessage("CMS.documents.files.no_file", null, locale);
			}
			frontCcFileStatus = initStatus(frontFile.getFileName(), isRegulated, frontFile.isApproved(), frontFile.isControlApproved());
			frontCcLocked = FilesManagerBase.initLock((frontFile.isApproved() || frontFile.isControlApproved()), frontFile.isControlReject());
			File backFile = map.get(FileType.CC_COPY_BACK);
			if (backFile != null && backFile.getFileName() != null) {
				backCcFileName = getFileName(backFile.getFileName());
			} else {
				backCcFileName = CommonUtil.getMessage("CMS.documents.files.no_file", null, locale);
			}
			backCcFileStatus = initStatus(backFile.getFileName(), isRegulated, backFile.isApproved(), backFile.isControlApproved());
			backCcLocked = FilesManagerBase.initLock((backFile.isApproved() || backFile.isControlApproved()), backFile.isControlReject());
		} else {
			frontCcFileName = CommonUtil.getMessage("CMS.documents.files.no_file", null, locale);
			backCcFileName = CommonUtil.getMessage("CMS.documents.files.no_file", null, locale);
			frontCcFileStatus = CommonUtil.getMessage("CMS.documents.files.file.missing", null, locale);
			frontCcLocked = false;
			backCcFileStatus = CommonUtil.getMessage("CMS.documents.files.file.missing", null, locale);
			frontCcLocked = false;
		}
		return "modifyCcValues";
	}
	
	public boolean isTooltipDisplayed() {
		return !driversFileStatus.equals(initStatus(driversFileName, isRegulated, true, true));
	}

	private String getFileName(String fileName) {
		String name = fileName.substring(getNthOccurrenceIndex(fileName, '_', NUM_SYSTEM_UNDERSCORES) + 1, fileName.length());
		if (name.length() > 15) {
			String fileType = name.substring(name.lastIndexOf('.'), name.length());
			name = name.substring(0, 13 - fileType.length()) + ".." + fileType;
		}
		return name;
	}
	
	/**
	 * 
	 * @param str the {@link String} to search in
	 * @param c the char to search for
	 * @param n the count of occurrences
	 * @return the N-th index if found, otherwise -1
	 */
	private static int getNthOccurrenceIndex(String str, char c, int n) {
	    int pos = str.indexOf(c, 0);
	    while (n-- > 0 && pos != -1) {
	        pos = str.indexOf(c, pos + 1);
	    }
	    return pos;
	}
	
	private String getSubDirName() {
		SimpleDateFormat sdSubDir = new SimpleDateFormat("yyMM");
		String subDir = sdSubDir.format(new Date());
		java.io.File subDirFile = new java.io.File(CommonUtil.getProperty(Constants.FILES_PATH) + subDir);
		Boolean result = null;
		if (!subDirFile.exists()) {
			result = subDirFile.mkdir();
		}
		if (result != null && !result) {
			logger.warn("Cannot create subdir");
			subDir = "";
		}
		
		return subDir + "/";
	}

	private void configureEmail(User user, long fileId) {
		Hashtable<String, String> serverProperties = new Hashtable<String, String>();
		serverProperties.put("url", CommonUtil.getProperty("email.server"));
		serverProperties.put("auth", "true");
		serverProperties.put("user", CommonUtil.getProperty("email.uname"));
		serverProperties.put("pass", CommonUtil.getProperty("email.pass"));
		serverProperties.put("contenttype", "text/html; charset=UTF-8");

        Hashtable<String, String> email = new Hashtable<String, String>();
        email.put("subject", "Bankwire confiramtion file uploaded ");
        email.put("to", CommonUtil.getProperty("support.email"));
        email.put("from", ApplicationDataBase.getSupportEmailBySkinId(user.getSkinId()));
        email.put("body", "User ID: " + user.getId() + "<BR/>" +
        				  "User Name: " + user.getUserName().toUpperCase() + "<BR/>" +
        				  "File ID: " + fileId);

		CommonUtil.sendEmail(serverProperties, email, null);
	}
	
	private Locale getLocale() {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = context.getApplication().evaluateExpressionGet(context, Constants.BIND_APPLICATION_DATA, ApplicationData.class);
		Locale locale = null;
		try {
			locale = ap.getUserLocale();
		} catch (Exception e) {
			Log.error("Could not get the user locale. Taking a default one", e);
			locale = new Locale(ConstantsBase.LOCALE_DEFAULT);
		}
		
		return locale;
	}
	
	private long getFileTypeByKey(String key){	    
	    long value = 0;	    
	    if (key.equals("0")){
	        value =  FileType.PASSPORT;
	    } else if (key.equals("1")){
	        value =  FileType.USER_ID_COPY;
	    } else if (key.equals("2")){
	        value =  FileType.DRIVER_LICENSE;	        
	    }
        return value;
	}

	public ArrayList<SelectItem> getCcList() throws Exception {
		User user = ApplicationData.getUserFromSession();
		return TransactionsManager.getDisplayCreditCardsByUser(user.getId());
	}
	
	public UploadedFile getUtilityBillFile() {
		return utilityBillFile;
	}

	public void setUtilityBillFile(UploadedFile utilityBillFile) {
		this.utilityBillFile = utilityBillFile;
	}

	public ArrayList<SelectItem> getPassOrIdSiOrDrive() {
		Locale locale = getLocale();
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(0, CommonUtil.getMessage("CMS.documents.files.passport", null, locale)));
		list.add(new SelectItem(1, CommonUtil.getMessage("CMS.documents.files.id", null, locale)));
		if (isRegulated){
		    list.add(new SelectItem(2, CommonUtil.getMessage("CMS.documents.files.drivers_license", null, locale)));   
		}		
		return list;
	}

	public UploadedFile getPassOrIdFile() {
		return passOrIdFile;
	}

	public void setPassOrIdFile(UploadedFile passOrIdFile) {
		this.passOrIdFile = passOrIdFile;
	}

	public long getPassOrId() {
		return passOrId;
	}

	public void setPassOrId(long passOrId) {
		this.passOrId = passOrId;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public UploadedFile getFrontCCFile() {
		return frontCCFile;
	}

	public void setFrontCCFile(UploadedFile frontCCFile) {
		this.frontCCFile = frontCCFile;
	}

	public UploadedFile getBackCCFile() {
		return backCCFile;
	}

	public void setBackCCFile(UploadedFile backCCFile) {
		this.backCCFile = backCCFile;
	}

	public UploadedFile getBankwireFile() {
		return bankwireFile;
	}

	public void setBankwireFile(UploadedFile bankwireFile) {
		this.bankwireFile = bankwireFile;
	}

	public UploadedFile getDriversLicenseFile() {
		return driversLicenseFile;
	}

	public void setDriversLicenseFile(UploadedFile driversLicenseFile) {
		this.driversLicenseFile = driversLicenseFile;
	}

	public String getUtilityFileName() {
		return utilityFileName;
	}

	public void setUtilityFileName(String utilityFileName) {
		this.utilityFileName = utilityFileName;
	}

	public String getUtilityFileStatus() {
		return utilityFileStatus;
	}

	public void setUtilityFileStatus(String utilityFileStatus) {
		this.utilityFileStatus = utilityFileStatus;
	}

	public String getDriversFileName() {
		return driversFileName;
	}

	public void setDriversFileName(String driversFileName) {
		this.driversFileName = driversFileName;
	}

	public String getDriversFileStatus() {
		return driversFileStatus;
	}

	public void setDriversFileStatus(String driversFileStatus) {
		this.driversFileStatus = driversFileStatus;
	}

	public String getBankwireFileName() {
		return bankwireFileName;
	}

	public void setBankwireFileName(String bankwireFileName) {
		this.bankwireFileName = bankwireFileName;
	}

//	public String getBankwireFileStatus() {
//		return bankwireFileStatus;
//	}
//
//	public void setBankwireFileStatus(String bankwireFileStatus) {
//		this.bankwireFileStatus = bankwireFileStatus;
//	}

	public String getPassOrIdFileName() {
		return passOrIdFileName;
	}

	public void setPassOrIdFileName(String passOrIdFileName) {
		this.passOrIdFileName = passOrIdFileName;
	}

	public String getPassOrIdStatus() {
		return passOrIdStatus;
	}

	public void setPassOrIdStatus(String passOrIdStatus) {
		this.passOrIdStatus = passOrIdStatus;
	}

	public String getUploadedDocumentName() {
		return uploadedDocumentName;
	}

	public String getUploadedDocumentStatus() {
		return uploadedDocumentStatus;
	}

	public String getUploadedDocumentNameId() {
		return uploadedDocumentNameId;
	}

	public void setUploadedDocumentNameId(String uploadedDocumentNameId) {
		this.uploadedDocumentNameId = uploadedDocumentNameId;
	}

	public String getUploadedDocumentStatusId() {
		return uploadedDocumentStatusId;
	}

	public void setUploadedDocumentStatusId(String uploadedDocumentStatusId) {
		this.uploadedDocumentStatusId = uploadedDocumentStatusId;
	}

	public String getFrontCcFileName() {
		return frontCcFileName;
	}

	public void setFrontCcFileName(String frontCcFileName) {
		this.frontCcFileName = frontCcFileName;
	}

	public String getFrontCcFileStatus() {
		return frontCcFileStatus;
	}

	public void setFrontCcFileStatus(String frontCcFileStatus) {
		this.frontCcFileStatus = frontCcFileStatus;
	}

	public String getBackCcFileName() {
		return backCcFileName;
	}

	public void setBackCcFileName(String backCcFileName) {
		this.backCcFileName = backCcFileName;
	}

	public String getBackCcFileStatus() {
		return backCcFileStatus;
	}

	public void setBackCcFileStatus(String backCcFileStatus) {
		this.backCcFileStatus = backCcFileStatus;
	}

	public boolean isUtilityLocked() {
		return utilityLocked;
	}

	public void setUtilityLocked(boolean utilityLocked) {
		this.utilityLocked = utilityLocked;
	}

	public boolean isPassOrIdLocked() {
		return passOrIdLocked;
	}

	public void setPassOrIdLocked(boolean passOrIdLocked) {
		this.passOrIdLocked = passOrIdLocked;
	}

	public boolean isDriversLocked() {
		return driversLocked;
	}

	public void setDriversLocked(boolean driversLocked) {
		this.driversLocked = driversLocked;
	}

	public boolean isFrontCcLocked() {
		return frontCcLocked;
	}

	public void setFrontCcLocked(boolean frontCcLocked) {
		this.frontCcLocked = frontCcLocked;
	}

	public boolean isBackCcLocked() {
		return backCcLocked;
	}

	public void setBackCcLocked(boolean backCcLocked) {
		this.backCcLocked = backCcLocked;
	}

	public boolean isShowAllApprovedText() {
		return showAllApprovedText;
	}

	public void setShowAllApprovedText(boolean showAllApprovedText) {
		this.showAllApprovedText = showAllApprovedText;
	}

	public String getSummaryText() {
		return summaryText;
	}

	public void setSummaryText(String summaryText) {
		this.summaryText = summaryText;
	}

	public boolean isShowCcFields() {
		return showCcFields;
	}

	public void setShowCcFields(boolean showCcFields) {
		this.showCcFields = showCcFields;
	}

	public boolean isShowBankwireField() {
		return showBankwireField;
	}

	public void setShowBankwireField(boolean showBankwireField) {
		this.showBankwireField = showBankwireField;
	}

	public boolean isQuestionnaireDone() {
		return questionnaireDone;
	}

	public void setQuestionnaireDone(boolean questionnaireDone) {
		this.questionnaireDone = questionnaireDone;
	}

	public boolean isRegulated() {
		return isRegulated;
	}

	public String getPassOrIdStr() {
		return passOrIdStr;
	}

	public void setPassOrIdStr(String passOrIdStr) {
		this.passOrIdStr = passOrIdStr;
	}
}