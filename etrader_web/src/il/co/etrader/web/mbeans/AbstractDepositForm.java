/**
 * 
 */
package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.UsersDetailsHistory;
import com.anyoption.common.managers.CurrenciesManagerBase;

import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.TransactionsManager;
import il.co.etrader.web.bl_managers.UsersManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;
import il.co.etrader.web.util.FormBase;

/**
 * An abstract class that contains common logic for and should be parent of all deposit forms.
 * 
 * @author pavelhe
 *
 */
public abstract class AbstractDepositForm extends FormBase implements Serializable {

	private static final Logger logger = Logger.getLogger(AbstractDepositForm.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long currencyId;
	private String street;
	private String cityNameAO = "";
	private String zipCode;
	private Date birthDate;
	
	private boolean isFirstDeposit;
	private boolean isFirstDepositExcludeBonus;
	
	/**
	 * Constructor
	 * 
	 * @throws SQLException
	 */
	public AbstractDepositForm() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER, User.class);
		reloadIsFirstDeposit();
		this.currencyId = user.getCurrencyId();
		String zip = CommonUtil.isMobile() ? CommonUtil.getMessage("mobile_site.deposit.zip", null).toUpperCase() :
											 CommonUtil.getMessage("CMS.registerContent.label.register.postcode", null).toUpperCase();
		if (isShowUserFields() || user.getSkinId() == Skins.SKIN_ETRADER) {
			this.street = user.getStreet() == null || user.getStreet().trim().isEmpty()
							? CommonUtil.getMessage("CMS.registerContent.label.register.street", null).toUpperCase()
							: user.getStreet();
			this.cityNameAO = user.getCityName() == null || user.getCityName().trim().isEmpty()
					? CommonUtil.getMessage("CMS.registerContent.label.register.city", null).toUpperCase()
					: user.getCityName();
			this.zipCode = user.getZipCode() == null || user.getZipCode().trim().isEmpty()
					? zip
					: user.getZipCode();
			this.birthDate = user.getTimeBirthDate();		
		}
	}

	public AbstractDepositForm(long currencyId, String street, String cityNameAO, String zipCode, Date birthDate) throws SQLException {
		this();
		this.currencyId = currencyId;
		this.street = street;
		this.cityNameAO = cityNameAO;
		this.zipCode = zipCode;
		this.birthDate = birthDate;
	}
	
	/**
	 * Updates the user fields in case of first deposit - Currency, Street, City, ZIP
	 * 
	 * @param user
	 * @throws SQLException
	 */
	public void updateUserFields(User user, boolean isEtraderAdditionalInfo) throws SQLException {
		if (!isShowUserFields() && user.getSkinId() != Skins.SKIN_ETRADER) {
			/* !!! WARNING !!! Method exit point !!! */
			logger.trace("Skipping user fields update, isMobile [" + CommonUtil.isMobile()
									+ "], isFirstDeposit [" + isFirstDeposit + "]");
			return;
		}
		
		logger.debug("Setting user fields: currencyId [" + currencyId
										+ "], street [" + street
										+ "], zipCode [" + zipCode
										+ "], cityNameAO [" + cityNameAO + "]"
										+ "], birthDate [" + user.getTimeBirthDate() + "]");
		// change user details.
		ArrayList<UsersDetailsHistory> usersDetailsHistoryList = null;
        try {
            HashMap<Long, String> userDetailsBeforeChangedHM = UsersManager.getUserDetailsHM(user.getSkinId(), null, ConstantsBase.NON_SELECTED, null, null, null, null, false, false, null, null, null, null, null, 
                    ConstantsBase.NON_SELECTED, null, null, null, null);
            HashMap<Long, String> userDetailsAfterChangedHM = UsersManager.getUserDetailsHM(user.getSkinId(), street, ConstantsBase.NON_SELECTED, null, null, null, null, false, false, null, null, null, cityNameAO, 
                    null, ConstantsBase.NON_SELECTED, null, null, null, user.getTimeBirthDate());
            usersDetailsHistoryList = UsersManager.checkUserDetailsChange(userDetailsBeforeChangedHM, userDetailsAfterChangedHM);
        } catch (Exception e) {
            logger.error("problem to save user details history.", e);
        }
        
		long limitId = UsersManagerBase.updateUserFields(user.getId(),
															currencyId,
															street,
															zipCode,
															cityNameAO,
															user.getSkinId(),
															user.getTimeBirthDate());
		// change user details.
		if (null != usersDetailsHistoryList && !isEtraderAdditionalInfo) {
            UsersManagerBase.insertUsersDetailsHistory(Constants.WRITER_WEB_ID, user.getId(), user.getUserName(), usersDetailsHistoryList, String.valueOf(user.getClassId()));
        }
		
		if(!isEtraderAdditionalInfo){
			user.setLimitId(limitId);
			FacesContext context = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
			HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			String strCurrencyId = String.valueOf(currencyId);
			session.setAttribute(Constants.CURRENCY_ID, strCurrencyId);
			Cookie[] cs = request.getCookies();
			if (cs != null) {
				for (int i = 0; i < cs.length; i++) {
					if (cs[i].getName().equals(Constants.CURRENCY_ID)) {
						ApplicationData.addCookie(Constants.CURRENCY_ID,
													strCurrencyId,
													response,
													Constants.CURRENCY_ID_DESCRIPTION);
					}
				}
			}
			user.setCurrencyId(currencyId);
			user.setCurrency(CurrenciesManagerBase.getCurrency(currencyId));
		}
		
		user.setStreet(street);
		user.setZipCode(zipCode);
		user.setCityName(cityNameAO);
		
		logger.debug("User fields set successfully");
	}

	/**
	 * @return indicates whether user fields are shown <code>true</code> or not <code>false</code>.
	 */
	protected boolean isShowUserFields() {
		return isFirstDepositExcludeBonus;
	}
	
	/**
	 * @return indicates whether to initialize fields with default values <code>true</code> or not <code>false</code>.
	 */
	protected boolean isSetDefaultFieldValues() {
		FacesContext context = FacesContext.getCurrentInstance();
		return context.getViewRoot().getViewId().indexOf("/mobile/") < 0
					&& !CommonUtil.isMobile();
	}
	
	/**
	 * Reloads the isFirstDeposit flag used for showing user fields.
	 * 
	 * @throws SQLException
	 */
	protected void reloadIsFirstDeposit() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER, User.class);
		isFirstDeposit = TransactionsManager.isFirstPossibleDeposit(user.getId());
		isFirstDepositExcludeBonus = TransactionsManager.isFirstPossibleDepositExcludeBonus(user.getId());
	}

	/**
	 * @return the currencyId
	 */
	public Long getCurrencyId() {
		return currencyId;
	}

	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the cityNameAO
	 */
	public String getCityNameAO() {
		return cityNameAO;
	}

	/**
	 * @param cityNameAO the cityNameAO to set
	 */
	public void setCityNameAO(String cityNameAO) {
		this.cityNameAO = cityNameAO;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * @return the isFirstDeposit
	 */
	public boolean isFirstDeposit() {
		return isFirstDeposit;
	}

	public boolean isFirstDepositExcludeBonus() {
		return isFirstDepositExcludeBonus;
	}

	public void setFirstDepositExcludeBonus(boolean isFirstDepositExcludeBonus) {
		this.isFirstDepositExcludeBonus = isFirstDepositExcludeBonus;
	}
}