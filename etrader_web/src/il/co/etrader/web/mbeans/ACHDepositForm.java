package il.co.etrader.web.mbeans;

import java.io.Serializable;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.clearing.ACHInfo;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.TransactionsManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;

public class ACHDepositForm extends DepositForm implements Serializable{
    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(ACHDepositForm.class);

	private ACHInfo achInfo;


	public ACHDepositForm() throws Exception {
		super();
		User user = ApplicationData.getUserFromSession();
		achInfo = new ACHInfo(user);
		achInfo.setPayeeStateProvince(ApplicationData.getStateCode(user.getState()));
	}

	/**
	 * ACH deposit
	 *
	 * @return
	 * @throws Exception
	 */
	public String insertDeposit() throws Exception {
		// prevent identical requests from being processed
		if (saveTokenIsInvalid()) {
			return logInvalidSaveAndReturn(logger, null);
		}
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		generateSaveToken();
		User user = ApplicationData.getUserFromSession();
		Transaction t = TransactionsManager.insertDepositACH(null,
				"directDepositForm", deposit, CommonUtil.getWebWriterId(), user,
				Constants.WEB_CONST, achInfo, loginId);

		if (null != t) {
            ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
            ve.getValue(context.getELContext());
			ve.setValue(context.getELContext(), t);
			if (logger.isDebugEnabled()) {
				logger.debug("updating user session details.");
			}
			TransactionsManager.updateUserByName(user.getUserName(), user);
			return receiptHttpsNav();
		} else { // failed
			return null;
		}
	}

	/**
	 * Constructs a <code>String</code> with all attributes in name = value format.
	 *
	 * @return a <code>String</code> representation of this object.
	 */
	public String toString() {
	    final String ls = " \n ";
	    String retValue = "";
	    retValue = "DepositForm ( "
	        + super.toString() + ls
	        + "achInfo ( + ls" +
	        achInfo.toString() +
	        " )";
	    return retValue;
	}

	public ACHInfo getAchInfo() {
		return achInfo;
	}

	public void setAchInfo(ACHInfo achInfo) {
		this.achInfo = achInfo;
	}
}