package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.LimitationDeposits;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.LimitationDepositsManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.util.AESUtil;
import com.anyoption.common.util.AnyOptionCreditCardValidator;

import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.TransactionsManager;
import il.co.etrader.web.bl_managers.UsersManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.bl_vos.UserRegulation;
import il.co.etrader.web.util.Constants;
import il.co.etrader.web.util.Utils;


public class NewCardForm extends AbstractDepositForm implements Serializable{
	private static final long serialVersionUID = 4409179889681832352L;

	private static final Logger logger = Logger.getLogger(NewCardForm.class);

	private String typeId;
	private String ccNum;
	private String ccPass;
	private String expMonth;
	private String expYear;
	private String holderName;
	private String holderIdNum;
	private String deposit;
	protected String birthYear;
	protected String birthMonth;
	protected String birthDay;
	protected String streetNo;

	private boolean isCcError;  //for cc validation
	private boolean isExpDateError;
	private ArrayList<SelectItem> ccTypes;
	
	public NewCardForm() throws SQLException {
		super();
		FacesContext context = FacesContext.getCurrentInstance();
		// LivePerson - set forms parameters when redirected to failed page
		if (context.getViewRoot().getViewId().indexOf("/newCardF") > -1) {
			HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
			if (null !=  session && null != session.getAttribute(Constants.FAILED_DEPOSITS) && ((String)session.getAttribute(Constants.FAILED_DEPOSITS)).equals(String.valueOf(Constants.LIVEPERSON_FAILED_DEPOSITS))) {
				Transaction t = (Transaction) context.getApplication().createValueBinding(Constants.BIND_TRANSACTION).getValue(context);
				if (null != t) {
					CreditCard cc = t.getCreditCard();
					if (null != cc) {
						typeId = cc.getTypeIdStr();
						ccNum = String.valueOf(cc.getCcNumber());
						ccPass = cc.getCcPass();
						expMonth = cc.getExpMonth();
						expYear = cc.getExpYear();
						holderName = cc.getHolderName();
						deposit = String.valueOf(t.getAmount()/100);
					}
				}
			}
		}
		//necessary for the specific order for the credit cards
		ApplicationData appData = context.getApplication().evaluateExpressionGet(context, Constants.BIND_APPLICATION_DATA, ApplicationData.class);
		User user = context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER, User.class);
		
		// set predefined deposit amount
		if (user.getSkinId() != Skin.SKIN_ETRADER) {
			if (ApplicationData.getPredefinedDepositAmount().containsKey(user.getCurrencyId())) {
				LimitationDeposits ld = LimitationDepositsManagerBase.getLimitByUserId(user.getId());
		        if (ld.getDefaultDeposit() == 0) {
		        	deposit = ApplicationData.getPredefinedDepositAmount().get(user.getCurrencyId());
		        } else {
		        	deposit = Long.toString(ld.getDefaultDeposit()/100);
		        }
			}
		}
		
		if(CommonUtil.isUserSkinRegulated()){
			ccTypes = appData.getCreditCardTypesRegulation();
		}else if (user.getSkinId() == Skin.SKIN_ETRADER) {
			ccTypes = appData.getCreditCardTypes();
		}else {
			ccTypes = appData.getCreditCardTypesAo();
		}
		
		SelectItem ccAmex = null;
		for (SelectItem type : ccTypes) {
			if (Integer.valueOf((String) type.getValue()) == TransactionsManagerBase.CC_TYPE_AMEX) {
				ccAmex = type;
				break;
			}
		}
		ccTypes.remove(ccAmex);

		Collections.sort(ccTypes, new Comparator<SelectItem>() {

			@Override
			public int compare(SelectItem o1, SelectItem o2) {
				return o2.getLabel().compareTo(o1.getLabel());
			}
		});
		if(!CommonUtil.isUserSkinRegulated()){
			if (user.getSkinId() != Skin.SKIN_CHINESE_VIP) {
				if (user.getCurrencyId() != Constants.CURRENCY_ILS_ID && user.getCurrencyId() != Constants.CURRENCY_USD_ID && user.getCurrencyId() != Constants.CURRENCY_EUR_ID && user.getCurrencyId() != Constants.CURRENCY_GBP_ID) {
					// do nothing
				} else if (ccTypes.size() > 1) {
					ccTypes.add(ccTypes.size() - 1, ccAmex);
				} else {// in case something goes wrong, basically this should be unreachable
					ccTypes.add(ccAmex);
				}
			}
			
			if(user.getSkinId() == Skin.SKIN_CHINESE_VIP || (user.getSkinId() == Skin.SKIN_CHINESE && user.getCountryId() == Constants.CHINA_COUNTRY_ID_INT)) {
				for (SelectItem type : ccTypes) {
					if (Integer.valueOf((String) type.getValue()) == TransactionsManagerBase.CC_TYPE_MAESTRO) {
						ccTypes.remove(type);
						break;
					}
				}
			}
		}
		
		if(user.getCurrencyId() == ConstantsBase.CURRENCY_CNY_ID || user.getCurrencyId() == ConstantsBase.CURRENCY_KRW_ID) {
			for (SelectItem type : ccTypes) {
				if (Integer.valueOf((String) type.getValue()) == TransactionsManagerBase.CC_TYPE_MAESTRO) {
					ccTypes.remove(type);
					break;
				}
			}
		}
		
		if (isSetDefaultFieldValues()) {
			if (this.deposit == null || this.deposit.isEmpty()) {
				this.deposit = CommonUtil.getMessage("creditcards.deposit", null).toUpperCase();
			}
			if (this.ccNum == null || this.ccNum.isEmpty()) {
				this.ccNum = CommonUtil.getMessage("creditcards.ccNum", null).toUpperCase();
			}
			if (this.ccPass == null || this.ccPass.isEmpty()) {
				this.ccPass = CommonUtil.getMessage("creditcards.ccPass", null).toUpperCase();
			}
			if (this.holderName == null || this.holderName.isEmpty()) {
				this.holderName = CommonUtil.getMessage("creditcards.holderName", null).toUpperCase();
			}
			if (CommonUtil.isHebrewUserSkin() && (this.holderIdNum == null || this.holderIdNum.isEmpty())) {
				this.holderIdNum = CommonUtil.getMessage("creditcards.holderIdNum", null).toUpperCase();
			}
			if (user.getTimeBirthDate() != null) {
				birthYear = user.getBirthYear();
				birthMonth = user.getBirthMonth();
				birthDay = user.getBirthDay();
			}
			if (CommonUtil.isHebrewUserSkin() && (this.streetNo == null || this.streetNo.isEmpty())) {
				this.streetNo = CommonUtil.getMessage("register.streetno", null).toUpperCase();
			}
		}
	}

	public NewCardForm(	long typeId, long ccNumber, String ccPass, String expMonth, String expYear, String holderName,
						String holderId) throws SQLException {
		this();
		this.typeId = String.valueOf(typeId);
		ccNum = String.valueOf(ccNumber);
		this.ccPass = ccPass;
		this.expMonth = expMonth;
		this.expYear = expYear;
		this.holderName = holderName;
		holderIdNum = holderId;
	}

	public String insertCreditCardAndDeposit() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		FacesMessage fm = null;
		boolean firstNewCardChanged = false;
        
		User user = ApplicationData.getUserFromSession();		
		if (user.getTimeBirthDate() == null) {
            if (!CommonUtil.isParameterEmptyOrNull(getBirthDay())
                    && !CommonUtil.isParameterEmptyOrNull(getBirthMonth())
                    && !CommonUtil.isParameterEmptyOrNull(getBirthYear())) {
                if (Utils.birthDateValidation(getBirthYear(), getBirthMonth(), getBirthDay())){
                    
                    Calendar c = new GregorianCalendar(Integer.parseInt(getBirthYear()),
                            Integer.parseInt(getBirthMonth()) - 1,
                            Integer.parseInt(getBirthDay()), 0, 1); 
                    user.setTimeBirthDate(new Date(c.getTimeInMillis()));
                } else {
                    
                    FacesContext contx = FacesContext.getCurrentInstance();
                            fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            CommonUtil.getMessage("error.register.birthdate", null), null);
                    contx.addMessage("newCardForm:birthDay", fm); 
                    return null;
                }
            
            } else if (!CommonUtil.isParameterEmptyOrNull(getBirthDay())
                    || !CommonUtil.isParameterEmptyOrNull(getBirthMonth())
                    || !CommonUtil.isParameterEmptyOrNull(getBirthYear())) {
                FacesContext context2 = FacesContext.getCurrentInstance();               
                if (CommonUtil.isParameterEmptyOrNull(getBirthDay())) {
                            fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            CommonUtil.getMessage("CMS.registerContent.text.error.register.day", null), null);
                    context2.addMessage("newCardForm:birthDay", fm);
                }
                if (CommonUtil.isParameterEmptyOrNull(getBirthMonth())) {
                            fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            CommonUtil.getMessage("error", null), null);
                    context2.addMessage("newCardForm:birthMonth", fm);
                }
                if (CommonUtil.isParameterEmptyOrNull(getBirthYear())) {
                            fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            CommonUtil.getMessage("error", null), null);
                    context2.addMessage("newCardForm:birthYear", fm);
                }
                return null;
            }
        }

		if (!validateCCInput(context)) {
			return navigateAfterError();
		}
    	
    	//Regulation
    	UserRegulation userRegulation = new UserRegulation();
    	if(user.getSkin().isRegulated()){
    		//AO Regulation User can't make deposits(<> FD) if note done steps	
			if (userRegulation.getApprovedRegulationStep() != null && UserRegulationBase.REGULATION_FIRST_DEPOSIT == userRegulation.getApprovedRegulationStep() 
					&& (userRegulation.isQualified() || userRegulation.getRegulationVersion() == UserRegulationBase.REGULATION_USER_VERSION)
					|| userRegulation.isSuspended()
					|| UserRegulationManager.isRestrictedAO(userRegulation, user.getSkinId())) {
       			logger.debug("Can't insert deposit! Regulation steps are not done for user: " + userRegulation);
    			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.unapproved", null), null);
    			context.addMessage(null, fm);
    			return null;
			}
    	}else{
            //Check non reg suspend
       		if(UsersManagerBase.isNonRegUserSuspend(user.getId())){
       			logger.debug("Can't insert deposit! User is manual supend user: " + user.getId());
    			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.unapproved", null), null);
    			context.addMessage(null, fm);
    			return null;
       		}	
       	}
		
        if (logger.isDebugEnabled()) {
            logger.debug("Adding new card " + ccNum);
        }

		// cc validation
        if (!validateCreditCardNumber(context, user)) {
        	return navigateAfterError();
        }

		//	 prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();

		//FacesContext context=FacesContext.getCurrentInstance();

		ApplicationData gm = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);

		if (!CommonUtil.isParameterEmptyOrNull(ccNum) && user.getId() > 0){ //for repeat failure
			CreditCard currentCC = TransactionsManagerBase.getCreditCardByUserId(Long.valueOf(ccNum), Long.valueOf(user.getId()));
			if (null != currentCC){
				if ((CommonUtil.isHebrewSkin(user.getSkinId()) && !currentCC.getHolderIdNum().equals(holderIdNum)) ||
					!currentCC.getHolderName().equals(holderName) ||
					!currentCC.getExpMonth().equals(expMonth) ||
					!currentCC.getExpYear().equals(expYear)) {
					 firstNewCardChanged = true;
				}
			}
		}
		
		updateUserFields(user, false);
		UsersManager.insertUsersDetailsHistoryOneField(Constants.WRITER_WEB_ID, user.getId(), user.getUserName(), String.valueOf(user.getClassId()), UsersManager.USERS_DETAILS_HISTORY_TYPE_CREDIT_CARD_NUMBER, null, AESUtil.encrypt(ccNum));
		
		long writerId = CommonUtil.getWebWriterId();
    	if (CommonUtil.isMobile()) {
    		writerId = Writer.WRITER_ID_AO_MINISITE;
    	}	
    	Transaction t = TransactionsManager.insertCreditCardAndDeposit(typeId,ccNum,ccPass,expMonth,expYear,holderName,
					holderIdNum,deposit,writerId,user,Constants.WEB_CONST, firstNewCardChanged, true, loginId);

		reloadIsFirstDeposit();
		
		if (t != null) {
            ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
            ve.getValue(context.getELContext());
            ve.setValue(context.getELContext(), t);
            if (t.getStatusId() == TransactionsManager.TRANS_STATUS_AUTHENTICATE) {
            	if(t.isRender() ) {
            		return Constants.NAV_3D_SECURE_RENDER;
            	} else {
            		return Constants.NAV_3D_SECURE;
                }
            } else {
    			TransactionsManager.updateUserByName(user.getUserName(),user);

            	logger.debug("updating user session details.");            	            	
            	user.setFirstTimeDeposit(TransactionsManager.isFirstDeposit(user.getId()));

    			/**
    			 * fix for IE6 SP1 bug : security warnning (You are about to be redirected to a connection that is not secure...)
    			 * instead of http request, send https request with secure port in the header and with sessionid param.
    			 * the sessionid param is required for saving the current session.
    			 */
            	if (CommonUtil.getProperty("use.https").equals("true")) {
	    			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
	    			response.setHeader("port", "443");
	    			String root = Constants.WEB_DIRECTORY;
	    			if (CommonUtil.isMobile()) {
	    				root = Constants.MINISITE_DIRECTORY + "/jsp";
	    			}
	    			response.sendRedirect(gm.getHomePageUrlHttps() + root +"/pages/receipt.jsf;" + gm.getSessionIdString());
	    			return null;
            	} else {
            		return Constants.NAV_RECEIPT;
            	}
            }
		} else {
			return navigateAfterError();
		}
	}

	public boolean validateCreditCardNumber(FacesContext context, User user) throws Exception {
		if (!AnyOptionCreditCardValidator.validateCreditCardNumber(ccNum, user.getSkinId())) {
			if (logger.isDebugEnabled()) {
				logger.debug("Card number not valid");
			}
			setCcError(true);
			// For mobile: add the message error to the context
			if (!CommonUtil.isHebrewUserSkin() && CommonUtil.isMobile() && !AnyOptionCreditCardValidator.isDiners()) {
				context.addMessage(null, new FacesMessage(	FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.num", null),
															null));

			}
			if (AnyOptionCreditCardValidator.isDiners() || AnyOptionCreditCardValidator.isAmex()) {
				context.addMessage(null, new FacesMessage(	FacesMessage.SEVERITY_ERROR,
															CommonUtil.getMessage("error.creditcard.diners.notsupported", null), null));
			}
			return false;
		}
		return true;
	}

	public boolean validateCCInput(FacesContext context) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, 1);
		Calendar expCal = Calendar.getInstance();
		expCal.clear();
		expCal.set(Integer.parseInt(getExpYear()) + 2000, Integer.parseInt(getExpMonth()), 1);
		if (cal.after(expCal)) {
			context.addMessage("newCardForm:exp", new FacesMessage(	FacesMessage.SEVERITY_ERROR,
																	CommonUtil.getMessage("error.creditcard.expdateError", null), null));
			return false;
		}
		try {
			Long.parseLong(ccPass);
		} catch (NumberFormatException e) {
			context.addMessage("newCardForm:ccPass", new FacesMessage(CommonUtil.getMessage("error.cvv", null)));
			return false;
		}
		if (ccPass.length() != 3) {
			context.addMessage(	"newCardForm:ccPass",
								new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.cvvLength", null), null));
			return false;
		}

		return true;
	}

	public boolean isCheckCreditCard() throws Exception {
		User user=ApplicationData.getUserFromSession();

		ArrayList CCList = TransactionsManager.getDisplayCreditCardsByUser(user.getId());
		if ((CCList == null) || (CCList.size() == 0)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the typeId
	 */
	public String getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return the ccNum
	 */
	public String getCcNum() {
		return ccNum;
	}

	/**
	 * @param ccNum the ccNum to set
	 */
	public void setCcNum(String ccNum) {
		this.ccNum = ccNum;
	}

	/**
	 * @return the ccPass
	 */
	public String getCcPass() {
		return ccPass;
	}

	/**
	 * @param ccPass the ccPass to set
	 */
	public void setCcPass(String ccPass) {
		this.ccPass = ccPass;
	}

	/**
	 * @return the expMonth
	 */
	public String getExpMonth() {
		return expMonth;
	}

	/**
	 * @param expMonth the expMonth to set
	 */
	public void setExpMonth(String expMonth) {
		this.expMonth = expMonth;
	}

	/**
	 * @return the expYear
	 */
	public String getExpYear() {
		return expYear;
	}

	/**
	 * @param expYear the expYear to set
	 */
	public void setExpYear(String expYear) {
		this.expYear = expYear;
	}

	/**
	 * @return the holderName
	 */
	public String getHolderName() {
		return holderName;
	}

	/**
	 * @param holderName the holderName to set
	 */
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	/**
	 * @return the holderIdNum
	 */
	public String getHolderIdNum() {
		return holderIdNum;
	}

	/**
	 * @param holderIdNum the holderIdNum to set
	 */
	public void setHolderIdNum(String holderIdNum) {
		this.holderIdNum = holderIdNum;
	}

	/**
	 * @return the deposit
	 */
	public String getDeposit() {
		return deposit;
	}

	/**
	 * @param deposit the deposit to set
	 */
	public void setDeposit(String deposit) {
		this.deposit = deposit;
	}

	/**
	 * @return the isCcError
	 */
	public boolean isCcError() {
		return isCcError;
	}

	/**
	 * @param isCcError the isCcError to set
	 */
	public void setCcError(boolean isCcError) {
		this.isCcError = isCcError;
	}
	public void setExpDateError(boolean isExpDateError){
		this.isExpDateError = isExpDateError;
	}
	public boolean isExpDateError(){
		return isExpDateError;
	}


	/**
	 * Navigate after getting deposit error
	 * Saving session attribute for livePerson use, navigate to other page after X failed deposits
	 * @throws Exception
	 */
	public String navigateAfterError() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = ApplicationData.getUserFromSession();
		if (context.getViewRoot().getViewId().indexOf("/newCard") > -1 &&
				!CommonUtil.isMobile()) { // just for web
			HttpSession session = (HttpSession)context.getExternalContext().getSession(true);
			String failedDeposits = (String) session.getAttribute(Constants.FAILED_DEPOSITS);
			if (null != failedDeposits) {
				long failedNum = Long.parseLong(failedDeposits);
				failedNum++;
				session.setAttribute(Constants.FAILED_DEPOSITS, String.valueOf((failedNum)));  // set to first failed deposit
				if (failedNum == Constants.LIVEPERSON_FAILED_DEPOSITS) {  // redirect to failed deposit page for livePerson roles
					CreditCard cc = new CreditCard();
					cc.setTypeId(Long.valueOf(typeId).longValue());
					cc.setCcNumber(Long.parseLong(ccNum.replace(ConstantsBase.SPACE, ConstantsBase.EMPTY_STRING)));
					cc.setCcPass(ccPass);
					cc.setExpMonth(expMonth);
					cc.setExpYear(expYear);
					cc.setHolderName(holderName);
					Transaction t = new Transaction();
					t.setAmount(CommonUtil.calcAmount(deposit));
					t.setCreditCard(cc);
		            ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
		            ve.getValue(context.getELContext());
		            ve.setValue(context.getELContext(), t);
					return Constants.NAV_NEW_CARD_DEPOSIT_FAILED;
				}
			} else {
				session.setAttribute(Constants.FAILED_DEPOSITS, "1");  // set to first failed deposit
			}
		}
		return null;
	}


    public ArrayList<SelectItem> getCcTypes() {
        ArrayList<SelectItem> ccTypeUpper = new ArrayList<SelectItem>();
        for (SelectItem type : ccTypes) {
            type.setLabel(type.getLabel().toUpperCase());
            ccTypeUpper.add(type);
        }
        return ccTypeUpper;
    }

	public void setCcTypes(ArrayList<SelectItem> ccTypes) {
		this.ccTypes = ccTypes;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	@Override
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "NewCardForm ( "
	        + super.toString() + TAB
	        + "typeId = " + this.typeId + TAB
	        + "ccNum = " + CommonUtil.getNumberXXXX(this.ccNum) + TAB
	        + "ccPass = " + "*****" + TAB
	        + "expMonth = " + this.expMonth + TAB
	        + "expYear = " + this.expYear + TAB
	        + "holderName = " + this.holderName + TAB
	        + "holderIdNum = " + this.holderIdNum + TAB
	        + "deposit = " + this.deposit + TAB
	        + " )";

	    return retValue;
	}

	public String getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(String birthYear) {
		this.birthYear = birthYear;
	}

	public String getBirthMonth() {
		return birthMonth;
	}

	public void setBirthMonth(String birthMonth) {
		this.birthMonth = birthMonth;
	}

	public String getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}
	
	public String getStreetNo() {
		return streetNo;
	}

	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}
}
