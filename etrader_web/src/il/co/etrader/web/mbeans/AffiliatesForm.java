package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.util.HashMap;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.SendAffiliatesEmail;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.util.Constants;

public class AffiliatesForm implements Serializable{

	private static final long serialVersionUID = -5394629287641252957L;

	private static final Logger logger = Logger.getLogger(AffiliatesForm.class);

    private String firstName;
    private String lastName;
    private String company;
    private Long countryId;
    private String country;
    private String email;
    private String phone;
    private String webUrl;
    private String webDescription;
    private String uniqueVisitors;
    private String mainMarket;


    public AffiliatesForm() {
    	FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		Skins skin = ap.getSkinById(ap.getSkinId());
		String RealCountryId = ap.getRealCountryId();
		if (RealCountryId != null) {
			countryId = Long.valueOf(ap.getRealCountryId());
		}
    	if (null == countryId || countryId.longValue() == 0) {
    		countryId = Long.valueOf(skin.getDefaultCountryId());
    	}
    	webUrl="http://";

    }

   /**
	 * This method will send the contact email to user
	 */
	public String sendContactEmail(){

		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);

		country = ap.getCountry(countryId).getDisplayName();
		country = CommonUtil.getMessage(country,null);

		try {
			// collect the parameters we neeed for the email
			HashMap<String, String> params = new HashMap<String, String>();
			params.put(SendAffiliatesEmail.PARAM_FIRST_NAME, firstName);
			params.put(SendAffiliatesEmail.PARAM_LAST_NAME, lastName);
			params.put(SendAffiliatesEmail.PARAM_COMPANY, company);
			params.put(SendAffiliatesEmail.PARAM_COUNTRY, country);
			params.put(SendAffiliatesEmail.PARAM_EMAIL, email);
			params.put(SendAffiliatesEmail.PARAM_PHONE, phone);
			params.put(SendAffiliatesEmail.PARAM_WEB_URL, webUrl);
			params.put(SendAffiliatesEmail.PARAM_WEB_DESCRIPTION, webDescription);
			params.put(SendAffiliatesEmail.PARAM_UNIQUE_VISITORS, uniqueVisitors);
			params.put(SendAffiliatesEmail.PARAM_MAIN_MARKET, mainMarket);


			// Populate also server related params , in order to take out common functionality
			// Use params
			params.put(SendAffiliatesEmail.MAIL_TO, CommonUtil.getProperty("affilates.email", null));
			params.put(SendAffiliatesEmail.MAIL_SUBJECT , CommonUtil.getProperty("affilates.email.subject", null) + " - " + ap.getUserLocale().getDisplayLanguage());

			/*if ( null != skin && skin.getId() != ConstantsBase.SKIN_ETRADER ) {
				params.put(SendContactEmail.SUCCESS_MESSAGE, CommonUtil.getMessage("contact.mail.success",null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
			} else {
				params.put(SendContactEmail.SUCCESS_MESSAGE, CommonUtil.getMessage("contact.mail.success",null));
			}*/

			params.put(SendAffiliatesEmail.RETURN_PAGE, Constants.NAV_EMPTY_BANNERS);
			SendAffiliatesEmail s= new SendAffiliatesEmail(params);
			s.start();

			FacesMessage fm=new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("affilates.mail.success",null),null);
        	context.addMessage(null,fm);
            
			return Constants.NAV_EMPTY_BANNERS;

		} catch (Exception e) {
			e.printStackTrace();
			logger.fatal("Could not send email. " + e.getMessage());
		}
		return null;

	}


	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}



	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "ContactUsForm ( "
	        + super.toString() + TAB
	        + "firstName = " + this.firstName + TAB
	        + "lastName = " + this.lastName + TAB
	        + "company = " + this.company + TAB
	        + "country = " + this.country + TAB
	        + "email = " + this.email + TAB
	        + "phone = " + this.phone + TAB
	        + "webUrl = " + this.webUrl + TAB
	        + "webDescription = " + this.webDescription + TAB
	        + "uniqueVisitors = " + this.uniqueVisitors + TAB
	        + "mainMarket = " + this.mainMarket + TAB
	        + " )";
	    return retValue;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getMainMarket() {
		return mainMarket;
	}

	public void setMainMarket(String mainMarket) {
		this.mainMarket = mainMarket;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getUniqueVisitors() {
		return uniqueVisitors;
	}

	public void setUniqueVisitors(String uniqueVisitors) {
		this.uniqueVisitors = uniqueVisitors;
	}

	public String getWebDescription() {
		return webDescription;
	}

	public void setWebDescription(String webDescription) {
		this.webDescription = webDescription;
	}

	public String getWebUrl() {
		return webUrl;
	}

	public void setWebUrl(String webUrl) {
		this.webUrl = webUrl;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

}