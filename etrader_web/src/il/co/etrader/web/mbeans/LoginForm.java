package il.co.etrader.web.mbeans;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.el.ValueExpression;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.jfree.util.Log;

import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.Login;
import com.anyoption.common.beans.MailBoxUser;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.UserMigration;
import com.anyoption.common.beans.UsersAwardBonus;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.ApiPage;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.DeeplinkParamsToUrl;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.enums.DeviceFamily;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.DeeplinkParamsToUrlManagerBase;
import com.anyoption.common.managers.LoginsManager;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.managers.UserBalanceStepManager;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.managers.UsersAwardBonusManagerBase;
import com.anyoption.common.util.MarketingTracker;
import com.ocpsoft.pretty.faces.application.PrettyNavigationHandler;

import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bl_managers.EmailAuthenticationManagerBase;
import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.LoginProductManager.LoginProductEnum;
import il.co.etrader.bl_managers.RemarketingLoginsManagerBase;
import il.co.etrader.bl_managers.ResetPasswordManager;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.EmailAuthentication;
import il.co.etrader.bl_vos.InvestmentLimit;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.login.LoginInfoBean;
import il.co.etrader.login.LoginInfoThreadLocal;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.AdminManager;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.InvestmentsManager;
import il.co.etrader.web.bl_managers.LoginProductManager;
import il.co.etrader.web.bl_managers.UsersManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.bl_vos.UserRegulation;
import il.co.etrader.web.util.Constants;
import il.co.etrader.web.util.Utils;

public class LoginForm implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public static final String HIDDEN_USERNAME_KEY = "hiddenUsername";
	
	public static final long LOGIN_FROM_REGULAR	= 1;
	public static final long LOGIN_FROM_POPUP 	= 2;
	public static final long LOGIN_FROM_API 	= 3;

	private static final Logger logger = Logger.getLogger(LoginForm.class);

    private String username;
    private String password;
    private String msg;
    private String email;  // used for email activation
    private String apiPageRedirect;
    private boolean rememberMe = true;
    private EmailAuthentication ea;
    private long fingerPrint = 0L;
    private String dpn;

    public LoginForm() throws SQLException{
    	msg = "";
    	initAuth();
    }

    public LoginForm(String userName, String password) {
    	this.username = userName;
    	this.password = password;
	}

	public String login() throws Exception {
    	return login(Constants.LOGIN_FROM_REGULAR, Constants.LOGIN_PAGE);
    }
    
    public String loginWEB() throws Exception {
    	FacesContext context = FacesContext.getCurrentInstance();
    	//only if we came from web login
    	dpn = (String)((HttpServletRequest) context.getExternalContext().getRequest()).getSession().getAttribute(Constants.DPN);
    	String navPage = login(Constants.LOGIN_FROM_REGULAR, Constants.LOGIN_PAGE);
    	if (null != navPage) {
	    	//deep link page name
    		if (tryDeeplinkRedirect(dpn)) {
    			return null;
    		}
    	}
		return navPage;
    }

    public String login(long loginFrom, String pageName) throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
    	if (CommonUtil.isMobile()) {
    		try {
		    	String res = UsersManager.isUserSuspend(username, password);
		    	if (null != res) { //if user is supend show error msg
		    		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, res,null);
					context.addMessage(null, fm);
		    		return null;
		    	}
    		} catch (Exception e) {
    			logger.debug("Cannot get result from user suspend method", e);
    			throw e;
    		}
    	}
		String errorMessagesLabel = CommonUtil.isMobile() ? null : "login:errorlogin";
        ApplicationData ap = (ApplicationData) context.getApplication().evaluateExpressionGet(context, Constants.BIND_APPLICATION_DATA, ApplicationData.class);
        com.anyoption.common.beans.User user = new com.anyoption.common.beans.User();
    	try {
			UsersManager.loadUserByName(username, user);
		} catch (SQLException e) {
			logger.debug("Cannot load user", e);
		}
    	if (user.getSkinId() == 0 // no user
                || user.getSkinId() == Skin.SKIN_API //cant load api user in web
    			|| (ap.getSkinId() == Skin.SKIN_ETRADER && user.getSkinId() != Skin.SKIN_ETRADER) // etrader but non-etrader user
    			|| (ap.getSkinId() == Skin.SKIN_CHINESE_VIP && user.getSkinId() != Skin.SKIN_CHINESE_VIP) // vip168 but non-vip168 user
    			|| (ap.getSkinId() != Skin.SKIN_ETRADER
    					&& ap.getSkinId() != Skin.SKIN_CHINESE_VIP
    					&& (user.getSkinId() == Skin.SKIN_ETRADER
    						|| user.getSkinId() == Skin.SKIN_CHINESE_VIP)) /* ao but etrader or vip168 user */) {
    		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("header.login.error", null), null);
    		context.addMessage(errorMessagesLabel, fm);
    		return "";
    	}
    		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
    	if (!ApplicationData.getSkinById(user.getSkinId()).getSubdomain().equals(ApplicationData.getSkinById(ap.getSkinId()).getSubdomain())) {
    		String prefix = ApplicationData.getSkinById(user.getSkinId()).getSubdomain();
    		// TODO Here should be added the property file of the new host, not the current one
    		// TODO This is the production line, for test use the bottom line because we can't configure https locally
    		String newDomain = Utils.getPropertyByHost("homepage.url.https").replace("www", prefix);
    		// This line is for test purpose
//    		String newDomain = Utils.getPropertyByHost("homepage.url").replace("www", prefix);
    		if (user.getSkinId() == Skin.SKIN_REG_IT || user.getSkinId() == Skin.SKIN_ITALIAN) {
    			newDomain = newDomain.replace(Constants.HOST_ANYOPTION, Constants.HOST_ANYOPTION_IT);
    		}
            if (user.getSkinId() != Skin.SKIN_ITALIAN && user.getSkinId() != Skin.SKIN_REG_IT) {
                newDomain = newDomain.replace(Constants.HOST_ANYOPTION_IT, Constants.HOST_ANYOPTION);
            }
            String page = "jsp/" + pageName + ".jsf\"";
    		response.setContentType("text/html");
    		response.setCharacterEncoding("UTF-8");
    		try {
    			response.getWriter().write(
					"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">" +
					"<html>" +
						"<head></head>" +
						"<body>" +
							"<form action=\"" + newDomain + page + " method=\"post\" id=\"loginForm\">" +
								"<input name=\"hiddenUsername\" id=\"hiddenUsername\" type=\"hidden\" value=\"" + username + "\">" +
								"<input name=\"hiddenPassword\" id=\"hiddenPassword\" type=\"hidden\" value=\"" + password + "\">" +
								"<input name=\"hiddenFingerPrint\" id=\"hiddenFingerPrint\" type=\"hidden\" value=\"" + fingerPrint + "\">" +
								"<input name=\"page\" id=\"page\" type=\"hidden\" value=\"" + apiPageRedirect + "\">" +								
								"<input name=\"s\" id=\"s\" type=\"hidden\" value=\"" + user.getSkinId() + "\">" +
								"<input name=\"dpn\" id=\"dpn\" type=\"hidden\" value=\"" + dpn + "\">" +
								"<input name=\"redirectToDeposit\" id=\"redirectToDeposit\" type=\"hidden\" value=\"" + (String)((HttpServletRequest) context.getExternalContext().getRequest()).getSession().getAttribute("redirectToDeposit") + "\">" +
							"</form>" +
							"<script>document.getElementById('loginForm').submit();</script>" +
						"</body>" +
					"</html>");
    		} catch (IOException e) {
    			logger.debug("Can't change skin, response writer problem", e);
    			return "";
			}
    		((HttpServletRequest) context.getExternalContext().getRequest()).getSession().removeAttribute("redirectToDeposit");
    		context.responseComplete();
    		return null;
    	}
    	// TODO check if password has expired

    	HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
    	// statements before login
    	if (ap.getBooleanLoggedIn()) {
    		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("header.login.locksessionerror", null), null);
    		context.addMessage(errorMessagesLabel, fm);
    		return "";
        }	    	
    	if (user.getSkinId() == Skin.SKIN_ETRADER) {
    		password = password.toUpperCase();
    	}
    	
    	if(!CountryManagerBase.isCanLoginRegUser(user.getSkinId(), CommonUtil.getIPAddress(), user.getId())){
    		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Can't login", null);
    		context.addMessage(errorMessagesLabel, fm);
        	return "";
    	}
    	
    	HttpSession session = request.getSession();
				
		long combId;
		String combIdTxt = (String) session.getAttribute(Constants.COMBINATION_ID);
		if (null == combIdTxt || Long.valueOf(combIdTxt) == 0 || 
				AdminManager.getCombinationId(Long.valueOf(combIdTxt)) == null) {
			combId = ApplicationData.getSkinById(user.getSkinId()).getDefaultCombinationId();
		} else {
			combId = Long.valueOf(combIdTxt);
		}
    	
    	Login login = new Login();
		try {
			login.setLoginOffset(ap.getUtcOffset());
			login.setSuccess(true);
			login.setUserAgent(CommonUtil.getUserAgent());
			login.setDeviceUniqueId(null);
			login.setLoginFrom(loginFrom);
			login.setWriterId(Writer.WRITER_ID_WEB);
			login.setServerId(CommonUtil.getServerName().split(":")[1]);
			login.setFingerPrint(fingerPrint);
			login.setOsVersion(Constants.APPLICATION_SOURCE_WEB);
			login.setDeviceType(Constants.APPLICATION_SOURCE_WEB);
			login.setAppVersion(Constants.APPLICATION_SOURCE_WEB);
			login.setDeviceFamilyId(DeviceFamily.NONMOBILE.getId());
			login.setCombinationId(combId);
			login.setDynamicParam((String) session.getAttribute(Constants.DYNAMIC_PARAM));
			login.setAffSub1((String) session.getAttribute(Constants.AFF_SUB1));
			login.setAffSub2((String) session.getAttribute(Constants.AFF_SUB2));
			login.setAffSub3((String) session.getAttribute(Constants.AFF_SUB3));
		} catch (Exception e1) {
			logger.debug("can't init login object", e1);
		} 
		
		try {
			session.setAttribute(Constants.SESSION_SKIN, new Long(user.getSkinId()));
		} catch (Exception exc) {
			logger.debug("can't init SESSION_SKIN ", exc);
		}

    	try {
    		request.login(username, password);
    		/*
    		 * put in session user name and password
    		 * in order to login the user in jaas if the server is restarted
    		 * this functionality is disabled 
    		HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
    		session.setAttribute(AOJAASRealm.sessionUsername, username);
    		session.setAttribute(AOJAASRealm.sessionPassword, password);
    		*/
    		// put "remember me" cookie
    		try {
	    		Integer maxAge = new Integer(0);// expire immediately i.e. delete cookie
	    		if(rememberMe) {
	    			maxAge = new Integer(CommonUtil.getProperty("rememberme.period"));
	    		}
		   		 Map<String,Object> props = new HashMap<String, Object>();  
		         props.put("maxAge", maxAge);   
		         props.put("path","/");
		         //props.put("path", FacesContext.getCurrentInstance().getExternalContext().getRequestServerName());
		         FacesContext.getCurrentInstance().getExternalContext().addResponseCookie(CommonUtil.getProperty("rememberme.cookie"), username, props);
    		} catch (Throwable t) {
    			logger.debug("Can't set user cookie"+t.getMessage());
    		}

    	} catch (ServletException e) { // failed login, so actual failed count is user.getFailedCount() + 1
    		logger.debug("User failed login");
    		login.setSuccess(false);
    		String errorMessageKey = null;
    		// insert failed login in logins table
    		Object[] params = new Object[1];
			if ((user.getFailedCount() == ConstantsBase.WEB_FAILED_LOGIN_COUNT - 1) && (sendAutomaticEmailForResetPassword(user.getEmail(), user.getSkinId(), user.getUserName()))) {
				params[0] = user.getEmail();
				errorMessageKey = "reset.password.auto.error";
			} else {
				LoginInfoBean loginInfo = LoginInfoThreadLocal.getLoginInfo();
				if (loginInfo != null) {
					errorMessageKey = loginInfo.getErrorMessageKey();
				} else {
					errorMessageKey = "header.login.error";
					logger.warn("The login info from login module is null");
				}
				params = null;
			}
    		long userLastLoginId = LoginsManager.insertLogin(user.getUserName(), login);
    		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage(errorMessageKey, params), null);
    		context.addMessage(errorMessagesLabel, fm);
    		return "";
		} finally {
			LoginInfoThreadLocal.clear();
		}
    	
    	
    	// insert successful login in logins table
    	long loginId = LoginsManager.insertLogin(user.getUserName(), login);

		User loggedUser = context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER, User.class);
		try {
			initUser(loggedUser, loginId);
			// adding session attributes after login
			addAtributesAfterLogin(loggedUser.getSkinId(), loggedUser.getCountryId(), loggedUser.getCurrencyId(),
					loggedUser.getCombinationId(), loginId);
		} catch (Exception e) {
			logger.debug("Problem initialising user", e);
		}

		
		UserRegulation userRegulation = new UserRegulation();
		boolean isHasDeposit = loggedUser.isHasDeposits();
		boolean isSingleQuestDone = false;
		//ET Regulation Check
		boolean isETRegulation = false;
		boolean isETRegulationDone = false;	
		if(loggedUser.getSkinId() == Skin.SKIN_ETRADER && userRegulation.getApprovedRegulationStep() != null){
			isETRegulation = true;
			if(userRegulation.getApprovedRegulationStep() == UserRegulationBase.ET_REGULATION_DONE){
				isETRegulationDone = true;
			}
		}
		
		if(userRegulation.getApprovedRegulationStep() != null && loggedUser.getSkinId() != Skin.SKIN_ETRADER 
				&& userRegulation.getRegulationVersion() == 2
					&& userRegulation.getApprovedRegulationStep() >= UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE){
			isSingleQuestDone = true;
		}
		
		session.setAttribute(Constants.SESSION_IS_ET_REGULATION, isETRegulation);
		session.setAttribute(Constants.SESSION_IS_ET_REGULATION_DONE, isETRegulationDone);
		session.setAttribute(Constants.SESSION_IS_HAS_DEPOSIT, isHasDeposit);
		session.setAttribute(Constants.SESSION_IS_SINGLE_QUEST_DONE, isSingleQuestDone);
		session.setAttribute(Constants.SESSION_JUST_LOGGED_IN, true);
				
		if(user.getSkinId() == Skin.SKIN_ETRADER){
			if(UserRegulationManager.isNeedAdditionalInfo(loggedUser) && 
					((!loggedUser.isHasDeposits() && !isETRegulation) || isETRegulation)){
				Log.debug("redirect to page for additional info");
				return Constants.NAV_USER_ADDITIONAL_INFO;
			}			
			else if(isETRegulation && UserRegulationManager.isNeedShowCapitalQuestionnaire(userRegulation, loggedUser.getSkinId())){
				return Constants.NAV_USER_CAP_QUESTIONNAIRE;
			}
		} else {
			String lowInvestment = "";
			try {
				InvestmentLimit il = InvestmentsManager.getInvestmentLimit(ConstantsBase.OPPORTUNITY_TYPE_BINARY_, Opportunity.SCHEDULED_HOURLY, Market.MARKET_USD_EUR_ID, loggedUser.getCurrencyId(), loggedUser.getId());
				if (il != null) {
					long userMinInvestmentAmount = il.getMinAmount();
					if ((loggedUser.getBalance() < userMinInvestmentAmount) && isHasDeposit) {
						lowInvestment = String.valueOf(userMinInvestmentAmount);
						session.setAttribute(Constants.SESSION_MIN_INVEST_BALANCE, lowInvestment);
					}
				}
			} catch (SQLException sqle) {
				logger.debug("Problem getting users min investment limit", sqle);
			}
		}
		
		if ((!CommonUtil.isParameterEmptyOrNull((String)request.getSession().getAttribute("redirectToDeposit")) && 
				((String)request.getSession().getAttribute("redirectToDeposit")).equals(Constants.NAV_FIRST_DEPOSIT)) ||
					(!CommonUtil.isParameterEmptyOrNull(request.getParameter("redirectToDeposit")) &&
						request.getParameter("redirectToDeposit").equals(Constants.NAV_FIRST_DEPOSIT))) {
			request.getSession().removeAttribute("redirectToDeposit");
			if (loggedUser.isHasDeposits()){
				return Constants.NAV_OTHER_DEPOSIT;
			} else {
				return Constants.NAV_FIRST_DEPOSIT;
			}
		} else {
			if (loggedUser.isAcceptedTerms()) {
				if (!isHasDeposit) {
					if (loggedUser.getCountryId() == 44
							&& loggedUser.getCurrencyId() == 7) {
						return Constants.NAV_DELTAPAY_DEPOSIT_PAGE;
					} else {
						return Constants.NAV_FIRST_DEPOSIT;
					}

				} else{
					if (!userRegulation.isUserInDepositStep()) {
						Cookie platformRedirect = com.anyoption.common.util.CommonUtil.getCookie(request, "alp");//alp --> after login page
						if (platformRedirect == null) {
							String navigation = LoginProductManager.getLoginProductNavigation(user.getSkinId());
							ConfigurableNavigationHandler navHandler = (ConfigurableNavigationHandler) context	.getApplication()
																												.getNavigationHandler();
							loggedUser.setLoginProduct(navHandler	.getNavigationCase(context, "/jsp/*", navigation).getToViewId(context)
																	.replace(".xhtml", ".jsf"));
							return navigation;
						} else {
							String navPage = LoginProductManager.getProductToNavigationMap().get(LoginProductEnum.getById(Integer.valueOf(platformRedirect.getValue())));
							com.anyoption.common.util.CommonUtil.deleteCookie(platformRedirect, response);
							return navPage;
						}
					} else {
						return Constants.NAV_USER_QUESTIONNAIRE;
					}
				}
			} else {
				request.getSession().setAttribute("redirectAfterTerms", Constants.NAV_TRADE_PAGE);
				return Constants.NAV_AGREEMENT;
			}
		}
    }

	private boolean sendAutomaticEmailForResetPassword(String email, long skinId, String userName) {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData gm = (ApplicationData) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		String resKey = UsersManagerBase.generateRandomKey();
		String resLink = gm.getHomePageUrlHttps() + "jsp/resetPassword.jsf?" + ConstantsBase.RESET_KEY + "=" + resKey;
		try {
			email = email.toUpperCase();
			ArrayList<User> usrArr = null;
			usrArr = UsersManager.sendResetPassword(email, skinId);
			UsersManagerBase.sendMailTemplateFile(UsersManagerBase.TEMPLATE_PASSWORD_REMINDER_MAIL_ID, CommonUtil.getWebWriterId(), usrArr.get(0), false, resLink, null, null, null, null);
			ResetPasswordManager.insert(userName, resKey);
			return true;
		} catch (Exception ex) {
			logger.error("can't send password reminder automatic mail!! ", ex);
			return false;
		}
	}
	
    public void loginFromNewDomain(long loginFrom) throws Exception {
    	FacesContext context = FacesContext.getCurrentInstance();
    	username = context.getExternalContext().getRequestParameterMap().get(HIDDEN_USERNAME_KEY);
    	password = context.getExternalContext().getRequestParameterMap().get("hiddenPassword");
    	if (context.getExternalContext().getRequestParameterMap().get("hiddenFingerPrint") != null) {    		
    		fingerPrint = Long.parseLong(context.getExternalContext().getRequestParameterMap().get("hiddenFingerPrint"));
    	}
    	apiPageRedirect = context.getExternalContext().getRequestParameterMap().get("page");
    	if (username != null && password != null) {
    		if (loginFrom == LOGIN_FROM_POPUP) {
    			loginPopup(ConstantsBase.EMPTY_STRING);
    		} else if (loginFrom == LOGIN_FROM_API) {
    			if (apiPageRedirect.equalsIgnoreCase("null")) {
    				apiPageRedirect = null;
    			}
    			loginAPI(ConstantsBase.EMPTY_STRING, apiPageRedirect);
    		} else {
    			String outcome = login(Constants.LOGIN_FROM_REGULAR, Constants.LOGIN_PAGE);
    	    	String dpn = context.getExternalContext().getRequestParameterMap().get(Constants.DPN);
    	    	if (outcome == null || outcome.isEmpty()) {
    	    		outcome = Constants.NAV_LOGIN;
    	    	} else if (!tryDeeplinkRedirect(dpn)) { //if we r not from web or we r from web but no need deep link
    	    		PrettyNavigationHandler navHandler = (PrettyNavigationHandler) context.getApplication().getNavigationHandler();
    	    		navHandler.performNavigation(outcome);
    	    	}
    		}
    	}
    }

	private void initUser(User user, long loginId) throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
    	String remoteUser = context.getExternalContext().getRemoteUser();
    	if (remoteUser != null && !remoteUser.equals("")) {
    		UsersManager.getUserByName(remoteUser, user);
    		user.setCombination(UsersManager.getCombinationById(user.getCombinationId()));
    		user.setAffiliate(UsersManager.getAffiliateByKey(user.getAffiliateKey()));
    		user.setSubAffiliate(UsersManager.getSubAffiliateByKey(user.getSubAffiliateKey()));
    		try {
				UsersAwardBonus usersAwardBonus = UsersAwardBonusManagerBase.getByUserId(user.getId());
				if (null != usersAwardBonus) {
					BonusManagerBase.insertBonusWithIssue(user, usersAwardBonus.getId(), usersAwardBonus.getUsersAwardBonusGroup().getBonusId(), Constants.WRITER_WEB_ID, String.valueOf(IssuesManagerBase.ISSUE_SUBJECTS_INTERNAL), String.valueOf(IssuesManagerBase.ISSUE_STATUS_CLOSED), user.getId(), String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW), 
							ConstantsBase.ISSUE_TYPE_REGULAR, String.valueOf(IssuesManagerBase.ISSUE_ACTION_COMMENT), false, new Date(), ConstantsBase.OFFSET_GMT, usersAwardBonus.getUsersAwardBonusGroup().getDescription());
				}
			} catch (Exception e) {
				logger.debug("cant load users Award Bonus", e);
			}
    		try {
        		logger.debug("Trying to insert auto bonus upon login for login id " + loginId);
    	    	// Insert Automatic Bonus.
    	    	long autoBonusId = BonusManagerBase.getAutoBonusByLoginId(loginId);

    	    	if (autoBonusId > 0) {
    	    		BonusUsers bonusUser = new BonusUsers();
    	    		bonusUser.setUserId(user.getId());
    	    		bonusUser.setWriterId(user.getWriterId());
    	    		bonusUser.setBonusId(autoBonusId);

    	    		BonusManagerBase.insertBonusUser(bonusUser, user, user.getWriterId(), 0, 0, false);
    	    	}
        	} catch (Exception e) {
    			logger.error("Failed to insert auto bonus upon login for login Id " + loginId, e);
    		}
    		user.setEmailPopUp(UsersManager.getUserPopUpEmail(user.getId()));
            user.setSentActivationEmail(UsersManager.isSentActivationEmail(user.getId(), this.email));

            MailBoxUser emailPopUp = user.getEmailPopUp();
    		if (null != emailPopUp) {
    			if (emailPopUp.getPopupTypeId() == ConstantsBase.MAILBOX_POPUP_TYPE_EMAIL_ALERT) {  // alert popup
    				user.setEmailPopUpAlert(true);
    			} else {  // email content in popup
    				user.setEmailParameters(emailPopUp);
                    ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_MAILBOX_USER, MailBoxUser.class);
                    ve.getValue(context.getELContext());
                    ve.setValue(context.getELContext(), emailPopUp);
                    user.setEmailPopUpDisplay(true);
    			}
    		}

    		logger.debug("User " + username + " loaded on: " + new Date());
    		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
    		boolean remarketingRegMade = (String)session.getAttribute(Constants.REMARKETING_REGISTER) == null ? false : ((String)session.getAttribute(Constants.REMARKETING_REGISTER)).equalsIgnoreCase("true"); 
    		String fromRem = (String)session.getAttribute(Constants.FROM_REMARKETING) == null ? "" : "true";
    		if(!remarketingRegMade && fromRem.equalsIgnoreCase("true")){
    			RemarketingLoginsManagerBase.insertRemarektingLoginBytype(0, RemarketingLoginsManagerBase.REMARKETING_TYPE_LOGIN, user.getId(), null, null, null, 0, null);
    		}
    		if ( user.getId() > 0 ) {
    			user.setTimeLastLogin(new Date());
    			user.setUtcOffset(CommonUtil.getUtcOffset());

    			// get deposits that we should run pixels on them
        		user.setDepositForPixel(UsersManager.getFirstDepositForPixlRunning(user.getId()));
        		user.setDepositsForPixel(UsersManager.getDepositsForPixlRunning(user.getId()));

    	   		UsersManager.updateUser(user);

				if ( user.getSkinId() != Skin.SKIN_ETRADER ) {
					// set markets list
					MarketsListForm ml = context.getApplication().evaluateExpressionGet(context, Constants.BIND_MARKETS_LIST_FORM, MarketsListForm.class);
					ml.updateList();
					Markets0100ListForm ml0100 = context.getApplication().evaluateExpressionGet(context, Constants.BIND_MARKETS_0100_LIST_FORM, Markets0100ListForm.class);
					ml0100.updateList();
				}
				calculateUserCityCoordinates(user);
    		}
    	}
    	
    	//  customer visit type cookie for anyoption
    	if (user.getId() > 0 && user.getSkinId() != Skin.SKIN_ETRADER) {
			String cvt = UsersManager.getCustomerVisitType((HttpServletRequest) context.getExternalContext().getRequest());
			boolean isHasDeposits = user.isHasDeposits();
			if (null == cvt) {  // save cookie
				String cvtValue = Constants.CUSTOMER_VISIT_TYPE_NON_DEPOSITOR;
				if (isHasDeposits) {
					cvtValue = Constants.CUSTOMER_VISIT_TYPE_DEPOSITOR;
				}
				ApplicationData.addCookie(Constants.CUSTOMER_VISIT_TYPE, cvtValue, (HttpServletResponse) context.getExternalContext().getResponse(), Constants.CUSTOMER_VISIT_TYPE);
			} else {  // check cookie values for updating deposit status
				if (cvt.equals(Constants.CUSTOMER_VISIT_TYPE_NON_DEPOSITOR) && isHasDeposits) {
					ApplicationData.addCookie(Constants.CUSTOMER_VISIT_TYPE, Constants.CUSTOMER_VISIT_TYPE_DEPOSITOR, (HttpServletResponse) context.getExternalContext().getResponse(), Constants.CUSTOMER_VISIT_TYPE);
				} else if (cvt.equals(Constants.CUSTOMER_VISIT_TYPE_DEPOSITOR) && !isHasDeposits) {
					ApplicationData.addCookie(Constants.CUSTOMER_VISIT_TYPE, Constants.CUSTOMER_VISIT_TYPE_NON_DEPOSITOR, (HttpServletResponse) context.getExternalContext().getResponse(), Constants.CUSTOMER_VISIT_TYPE);
				}
			}
    	}
    	
    	//Marketing Tracker Login Activity
		if (user.getId() > 0 && remoteUser != null && !remoteUser.equals("")){
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			MarketingTracker.insertMarketingTracker(request, response, user.getContactId(), user.getId(), Constants.MARKETING_TRACKING_LOGIN);
			
//			MarketingETS.etsMarketingLoginUser(request, response, user.getId(), user.getFirstDepositId(), user.getmId(), 
//			                                        user.getCombinationId(), user.getDynamicParam(), user.getHttpReferer());
		}

		// One click initialization
		UserBalanceStepManager.loadUserBalanceStep(user, user.isHasDeposits(), Writer.WRITER_ID_WEB);
		try{ 
			UserMigration userMigration = UsersManagerBase.getUserMigration(user.getId());
			if(userMigration != null) {
				user.setBalancePlus(user.getBalance());
				ArrayList<Investment> investments = InvestmentsManagerBase.getOpenedInvestmentsByUser(user.getId(), 0, 0, 0);
				for(Investment i:investments) {
					user.setBalancePlus(user.getBalancePlus() + i.getAmount());
				}
				ArrayList<Transaction> pendingWithdrawals = TransactionsManagerBase.getPendingWithdraws(user.getId());
				for(Transaction t:pendingWithdrawals) {
					user.setBalancePlus(user.getBalancePlus() + t.getAmount());
				}
			}
		} catch (Throwable t) { 
			logger.error("Problem loading migration data", t);
		}
	}

	private void addAtributesAfterLogin(long skinId, long countryId, long currencyId, long combId, long loginId) {
    	FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
		Skins skin = ApplicationData.getSkinById(skinId);
		String languageCode = ApplicationData.getLanguage(skin.getDefaultLanguageId()).getCode();
		String countryCode = "EU";
		if (!skin.isRegulated()) {
			countryCode = ApplicationData.getCountry(countryId).getA2();
		}
		Locale locale = new Locale(languageCode, countryCode);
		session.setAttribute(Constants.USER_LOCALE, locale);
		logger.debug("Create locale instance after user logged-in and save in Session! languageCode: " + languageCode +
						" countryCode: " + countryCode);
		
		session.setAttribute(Constants.SKIN_ID, String.valueOf(skinId));
		session.setAttribute(Constants.CURRENCY_ID, String.valueOf(currencyId));
		session.setAttribute(Constants.LANGUAGE_ID, String.valueOf(skin.getDefaultLanguageId()));
		session.setAttribute(Constants.COMBINATION_ID, String.valueOf(combId));
		session.setAttribute(Constants.IS_REGULATED, String.valueOf(skin.isRegulated()));
		session.setAttribute(Constants.LOGIN_ID, new Long(loginId));
		
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		ApplicationData.addCookie(Constants.SKIN_ID, String.valueOf(skinId), response, Constants.SKIN_ID_DESCRIPTION);
		ApplicationData.addCookie(Constants.IS_REGULATED, String.valueOf(skin.isRegulated()), response, Constants.IS_REGULATED_DESCRIPTION);
		
		logger.debug("Save as session attributes skinId, currencyId after user login, skinId: " + skinId);
	}
	
	private void calculateUserCityCoordinates(User user) {
		boolean res = false;
		Country country = ApplicationData.getCountry(user.getCountryId());
    	if (user.getSkinId() != Skin.SKIN_ETRADER && user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
    		try {
    			res = UsersManager.getUserCityCoordinates(user, country.getName());
    		} catch (Exception e) {
    			logger.error("Error while calculate city coordinates" + e);
			}
    	}
    	if (!res) {
			user.setCityLatitude(country.getCapitalCityLatitude());
			user.setCityLongitude(country.getCapitalCityLongitude());
			user.setCityFromGoogle(country.getCapitalCity());
    	}		
	}

	/**
     * Initial login authentication action
     * @return
     * @throws SQLException
     */
    public void initAuth() throws SQLException {
    	FacesContext context = FacesContext.getCurrentInstance();
    	if (context.getViewRoot().getViewId().indexOf("loginAuth") > -1) {
			// insert login data
			HttpSession session = (HttpSession) context.getExternalContext().getSession(true);			
			username = (String) session.getAttribute(Constants.USER_NAME_AUTH_LOGIN);			    		
    	}
//    	if (context.getViewRoot().getViewId().indexOf("loginAuth") > -1) {    	
//	    	HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
//	    	String authKey = request.getParameter(Constants.AUTH_KEY);
//	    	ea = null;
//	    	if (null != authKey) {
//	    		ea = EmailAuthenticationManagerBase.get(authKey, true);
//	    		if (null != ea) {
//                    ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_EMAIL_AUTH, EmailAuthentication.class);
//                    ve.getValue(context.getELContext());
//                    ve.setValue(context.getELContext(), ea);
//	    		}
//	    		if (null == ea || ea.getClicksNum() > 1) {
//	        		addValidationMessage(context);
//	        		return;
//	    		}
//	    		GregorianCalendar yesterday = new GregorianCalendar();
//	    		yesterday.setTime(new Date());
//	    		yesterday.add(GregorianCalendar.DAY_OF_MONTH, -2);
//	    		if (ea.getTimeCreated().before(yesterday.getTime())) {
//	    			addValidationMessage(context);
//	    			return;
//	    		}
//	    		username = ea.getUserName();
//	    	} else {  // error - no authentication key found
//	    		addValidationMessage(context);
//	    		return;
//	    	}
//    	}
    	if (context.getViewRoot().getViewId().indexOf("emailAuth") > -1 ||
    			context.getViewRoot().getViewId().indexOf("emailExpiredAuth") > -1) {
    		ea = (EmailAuthentication) context.getApplication().createValueBinding(Constants.BIND_EMAIL_AUTH).getValue(context);
    		if (null != ea) {
    			email = ea.getEmail();
    		} else {
    			logger.info("Warnning! emailAuth instance not in session.");
    		}
    	}
    	if (context.getViewRoot().getViewId().indexOf("emailActivation") > -1) {
    		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
    		if (null != user && user.getId() != 0) {
    			email = user.getEmail();
    			ea = new EmailAuthentication();
    			ea.setEmail(email);
    			ea.setUserId(user.getId());
                ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_EMAIL_AUTH, EmailAuthentication.class);
                ve.getValue(context.getELContext());
                ve.setValue(context.getELContext(), ea);
    		} else {
    			logger.info("Warnning! user instance not in session.");
    		}
    	}
        if (context.getViewRoot().getViewId().indexOf("welcomePopup") > -1) {
            username = CommonUtil.getMessage("header.input.username", null);
        }
    }

	public void addValidationMessage(FacesContext context) {
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
				CommonUtil.getMessage("login.auth.header.validation", null), null);
		context.addMessage(null, fm);

	}

	// TODO add navigation case in the configuration file for this action
    /**
     * Login authentication
     * handle authentication after registration
     * @return
     * @throws Exception
     */
    public String loginAuthentication() throws Exception {
        FacesContext context = FacesContext.getCurrentInstance();
        String res = null;

    	// Check if the user allready loggedIn
    	HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
    	User userFromSession = (User)request.getSession().getAttribute(Constants.BIND_SESSION_USER);

    	if (null != userFromSession) {  // user allready in session
    		if (null == userFromSession.getUserName() ||
    				null != userFromSession.getUserName() && !userFromSession.getUserName().toUpperCase().equals(username.toUpperCase())) {
    			res = "header.login.error";
    		}
    	} else {  // user not loggedIn
    		res = login();
//    		res = UsersManager.performUserLogin(user, Constants.LOGIN_FROM_REGULAR);
    	}

    	if (null != res && !res.isEmpty()) {
    		ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(),
    				Constants.BIND_USER, User.class);
    		User userS = (User) ve.getValue(context.getELContext());

    		if (null == userS || (null != userS && !userS.getUserName().toUpperCase().equalsIgnoreCase(getUsername().toUpperCase()))) {
        		addValidationMessage(context);
        		// TODO handle user bean
        		return null;
    		}
    		userS.setAuthorizedMail(true);
			Date timeAuthorized = new Date();
    		// is First users' authorization update mail's time authorizes an user's first authorized time

			long mailId = 0;
			if (ea != null){
				mailId = ea.getId();
			}

    		if (userS.getTimeFirstAuthorized() == null){
            	UsersManager.updateIsAuthrized(username, 1, mailId,timeAuthorized, true);
            	userS.setTimeFirstAuthorized(timeAuthorized);
    		}else{
            	UsersManager.updateIsAuthrized(username, 1, mailId, timeAuthorized, false);
    		}
    		ve.setValue(context.getELContext(), userS);
    		if (userS.isHasDeposits()) {
    			return Constants.NAV_MAIN_WITH_REDIRECT;
    		} else {
    			userS.setFirstTimeRegister(true);
    			//UsersManager.sendMailTemplateFile(Template.WELCOME_MAIL_ID, CommonUtil.getWebWriterId(), user);
    			return Constants.NAV_FIRST_NEW_CARD_REDIRECT;
    		}
    	} else { // handle error
    		return res;
    	}
    }

    /**
     * Send and insert Activation email
     * @return
     * @throws Exception
     */
    public String sendEmailActivation() throws Exception {
    	FacesContext context = FacesContext.getCurrentInstance();
        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_EMAIL_AUTH, EmailAuthentication.class);
        EmailAuthentication ea = (EmailAuthentication) ve.getValue(context.getELContext());
    	if (null != ea && ea.getEmail() != null && ea.getUserId() != 0) {
    		boolean res = EmailAuthenticationManagerBase.validateSendActivation(ea.getUserId());
    		if (res) {
        		if (!email.equals(ea.getEmail())) {  // registerd email not equal to activation one
        			if (UsersManager.isEmailInUse(email, ea.getUserId())) {
        				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
        						CommonUtil.getMessage("error.register.email.inuse",	null), null);
        				context.addMessage(null, fm);

        				return null;
        			} else {  // update user email
        				UsersManager.updateEmail(ea.getUserId(), email);
        				ea.setEmail(email);
        				ve.setValue(context.getELContext(), ea);
        				if (context.getViewRoot().getViewId().indexOf("emailActivation") > -1) {  // for updating user instace
                            ValueExpression uve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
                            User user = (User) uve.getValue(context.getELContext());
        		    		user.setEmail(email);
        		    		uve.setValue(context.getELContext(), user);
        				}
        			}
        		}
        		UserBase user = UsersManager.getUserById(ea.getUserId());
        		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
        		String authLink = ap.getHomePageUrlHttps() + "jsp/loginAuth.jsf?";
        		EmailAuthenticationManagerBase.sendActivationEmail(authLink, CommonUtil.getWebWriterId(), user);
        		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("email.activation.sent.succes", null),null);
    			context.addMessage(null, fm);

        	}
    	} else {  // general error message
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.email.activation", null),null);
			context.addMessage(null, fm);

    	}
    	return null;
    }
    
    public String loginPopup(String pageName) throws Exception {
        //msg = UsersManager.login(username, password);
        FacesContext context = FacesContext.getCurrentInstance();
        ApplicationData ap = context.getApplication().evaluateExpressionGet(context, Constants.BIND_APPLICATION_DATA, ApplicationData.class);
        String res = login(Constants.LOGIN_FROM_NON_DEPOSITORS, pageName);
        if (null != res && !res.isEmpty()) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            User userFromSession = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
            String page = null;
            if (ApplicationData.isChineseSkinAndCountryAndCurrency()) {
            	page = "jsp/pages/deposit.jsf';";
            } else {
            	page = "jsp/pages/newCard.jsf';";
            }
            if (!userFromSession.isAuthorizedMail()) { //not authorized mail users will login into authorized mail page
                page = "jsp/emailActivation.jsf';";
            } else if (userFromSession.isHasDeposits()) {
                page = "jsp/trade_binary_options.jsf';";
            }

            String javaScriptText = "window.top.location.href = '" + ap.getRhomePageUrl() + page;
            // Add the Javascript to the rendered page's header for immediate execution
            AddResource addResource = AddResourceFactory.getInstance(facesContext);
            addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
            return null;
        } else { // handle error
            return null;
        }
    }
    
    /**
     * Login from API GUI 
     * @param pageName
     * @param redirectToPage
     * @return
     * @throws Exception
     */
    public String loginAPI(String loginPageName, String pageParam) throws Exception {
    	apiPageRedirect = pageParam;
        String res = login(Constants.LOGIN_FROM_REGULAR, loginPageName);
        if (!CommonUtil.isParameterEmptyOrNull(res)) {
        	FacesContext context = FacesContext.getCurrentInstance();
			//redirect to API page         	    	
			if (!CommonUtil.isParameterEmptyOrNull(pageParam)) {
				String pageRedirect = Constants.EMPTY_STRING;
				if (!CommonUtil.isParameterEmptyOrNull(pageParam)) { 
					long APIPageId = 0;
					try { 			
						APIPageId = Long.parseLong(pageParam);						
					} catch (Exception e) {
						logger.error("Can't find API page", e);
					}
					if (APIPageId > 0 && APIPageId <= ApplicationData.getAPIPagesList().size()) {
						pageRedirect = ApplicationData.getAPIPagesList().get(ApiPage.API_PAGE_DEPOSIT).getUrl();
						if (ApplicationData.getAPIPagesList().get(APIPageId).isCanManualRedirect()) {
							pageRedirect = ApplicationData.getAPIPagesList().get(APIPageId).getUrl() ;	
							System.out.println("pageRedirect" + pageRedirect);
						} 						 
					}										 
					//if (res != null && res.equalsIgnoreCase(Constants.NAV_USER_QUESTIONNAIRE)) {						
					//	pageRedirect = ApplicationData.getAPIPagesList().get(ApiPage.API_PAGE_QUESTIONNAIRE).getUrl();
						
					//}						
				}				
				HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
				session.setAttribute(Constants.GUI, "API"); // save API GUI				
				ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
				String redirect = ap.getHomePageUrl() + pageRedirect;
				try {
					HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
					response.sendRedirect(redirect);
				} catch (IOException e) {
					logger.error("Error while trying to redirect from API", e);
				}
			} else { // redirect to anyoption site by anyoption logic
				context.getApplication().getNavigationHandler().handleNavigation(context, null, res);
			}        	        	        	        	
        }
		return res;
    }
    
    /**
     * Redirect API session user to deposit page
     * @return
     * @throws Exception
     */
    public String redirectSessionUser() throws Exception {
    	try {
	    	User user = ApplicationData.getUserFromSession();
	    	if (user != null && user.getId() > 0) {
	    		logger.info("user was found in the session:" + user.getId() + " redirecting to deposit page") ;
	            FacesContext context = FacesContext.getCurrentInstance();
	    		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
	    		String redirect = ap.getHomePageUrl() + ApplicationData.getAPIPagesList().get(ApiPage.API_PAGE_DEPOSIT);
	    		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
	    		response.sendRedirect(redirect);	    		          	
	    	} 
    	} catch (Exception e) {
    		logger.error("Error while trying to take user from session and redirect ", e);
		}
    	return null;
    }

	public boolean isLoggedIn() {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		if (user.getId()>0) {
			return true;
		}
		return false;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getHelloTxt() {
		return CommonUtil.getMessage("header.loggedin.hello", null);
	}
	public String getBalanceTxt() {
		return CommonUtil.getMessage("header.loggedin.balance", null);
	}
	public String getInvestmentsTxt() {
		return CommonUtil.getMessage("header.myinvestments", null);
	}
	public String getMyAccountTxt() {
		return CommonUtil.getMessage("header.myaccount", null);
	}
	public String getDepositTxt() {
		return CommonUtil.getMessage("deposit.page", null);
	}
	public String getUsernameTxt() {
		return CommonUtil.getMessage("header.username", null);
	}
	public String getPasswordTxt() {
		return CommonUtil.getMessage("header.password", null);
	}
	public String getForgetTxt() {
		return CommonUtil.getMessage("header.forgotpassword", null);
	}
	public String getRegisterTxt() {
		return CommonUtil.getMessage("header.openaccount", null);
	}
	public String getLoginTxt() {
		return CommonUtil.getMessage("header.login", null);
	}
	public String getHomepageTxt() {
		return CommonUtil.getMessage("header.homepage", null);
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isRememberMe() {
		return rememberMe;
	}

	public void setRememberMe(boolean rememberMe) {
		this.rememberMe = rememberMe;
	}

	public long getFingerPrint() {
		return fingerPrint;
	}

	public void setFingerPrint(long fingerPrint) {
		this.fingerPrint = fingerPrint;
	}
	
	/**
	 * check if we need to redirect to deep link url
	 * @return false if no need true if need
	 * @throws SQLException
	 */
	public boolean tryDeeplinkRedirect(String dpn) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		if (!CommonUtil.isParameterEmptyOrNull(dpn)) { //if its not null go to this page
			DeeplinkParamsToUrl deeplinkParamsToUrl = DeeplinkParamsToUrlManagerBase.getAllCache().get(dpn);
			if (deeplinkParamsToUrl != null && deeplinkParamsToUrl.getAnyoptionUrl() != null) {
				HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
				CommonUtil.sendPermanentRedirect(response, context.getExternalContext().getRequestContextPath() + deeplinkParamsToUrl.getAnyoptionUrl());
				return true;
			}
		}
		return false;
	}
}