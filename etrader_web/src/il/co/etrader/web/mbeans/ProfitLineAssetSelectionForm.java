package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;

import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.MarketsManager;
import il.co.etrader.web.bl_vos.ProfitLineAssetSelectionBean;

public class ProfitLineAssetSelectionForm implements Serializable {
    private static final Logger log = Logger.getLogger(ProfitLineAssetSelectionForm.class);
    
    private long opportunityTypeId;
    
    public ProfitLineAssetSelectionForm() {
        opportunityTypeId = Opportunity.TYPE_REGULAR;
    }
    
    public String getFillParams() throws SQLException {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> pm = fc.getExternalContext().getRequestParameterMap();
        if (null != pm.get("typeId")) {
            opportunityTypeId = Long.parseLong(pm.get("typeId"));
        }
        log.debug("getFillParams - opportunityTypeId: " + opportunityTypeId);
        return "";
    }
    
    public ArrayList<ProfitLineAssetSelectionBean> getMarkets() throws SQLException {
        FacesContext fc = FacesContext.getCurrentInstance();
        ApplicationData a = (ApplicationData) fc.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(fc);
        return MarketsManager.getProfitLineAssetSelection(a.getSkinId(), opportunityTypeId);
    }
}