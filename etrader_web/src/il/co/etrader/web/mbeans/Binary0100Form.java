package il.co.etrader.web.mbeans;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.InvestmentFormatter;
import il.co.etrader.web.bl_managers.InvestmentsManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

//import com.anyoption.common.beans.BinaryZeroOneHundred;
import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.bl_vos.InvestmentType;
import com.anyoption.common.jms.WebLevelsCache;

public class Binary0100Form implements Serializable {
    /**
	 *
	 */
	private static final long serialVersionUID = 4207461500480287842L;

	private static Logger log = Logger.getLogger(Binary0100Form.class);
	private static final int NUM_OF_INVESTMENTSIN_PAGE = 21;
	private int numOfInvestmentsInPage;

	private long oppId;
	private ArrayList<Investment> investmentsList;
	private long investmentCount;
	private long investmentCountDisabled; //for disabled state
	private Investment investment;
	private double quote;
	private Currency userCurrency;
	private boolean isValidationError;
	private boolean isCloseContractSuccess;
	private String msg; //msg to the user if success or error
	private long oppState;
	private double currentLevel;
	private boolean isNewInv;
	private boolean showFee;

    public Binary0100Form() {
    	investmentsList = new ArrayList<Investment>();
    	oppState = 2;
    	currentLevel = 0;
    	msg = null;
    	setNumOfInvestmentsInPage(NUM_OF_INVESTMENTSIN_PAGE);
    	isValidationError = false;
    	isCloseContractSuccess = false;
    }

    public int getMessageDisappearTime() throws SQLException {
    	String enumerator = null;
		enumerator = CommonUtil.getEnum(Constants.ENUM_MESSAGE_DISAPPEAR,Constants.ENUM_MESSAGE_DISAPPEAR_CODE);

		return Integer.parseInt(enumerator) * 1000;
    }

    public String initAndUpdateList() {
    	currentLevel = 0;
    	msg = null;
    	setNumOfInvestmentsInPage(NUM_OF_INVESTMENTSIN_PAGE);
    	isValidationError = false;
    	isCloseContractSuccess = false;
    	return updateList();
    }

    public String updateList() {
    	FacesContext context = FacesContext.getCurrentInstance();
    	User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
    	int lastListSize = investmentsList.size();
    	long lastOppId = 0;
    	boolean typeChanged = isInvTypeBuy();
    	if (investmentsList.size() > 0) {
    		lastOppId = investmentsList.get(0).getOpportunityId();
    	}
    	try {
    		investmentsList = InvestmentsManager.getAllBinary0100InvestmentsByOppId(user.getId(), oppId);
		} catch (SQLException e) {
			log.warn("cant get investments for oppId: " + oppId + " user " + user.getId(), e);
		}
//		log.debug("investmentsList size = " + investmentsList != null ? investmentsList.get(0).getInvestmentsCount() : "null");
		userCurrency = user.getCurrency();
		isNewInv = (lastListSize < investmentsList.size() && lastListSize > 0 || (typeChanged != isInvTypeBuy() && lastListSize > 0 && oppId == lastOppId));
		HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
		Integer needFee = (Integer) session.getAttribute(Constants.SESSION_0100_NEED_FEE);
		if(needFee != null && needFee > 0 ) {
			showFee = true;
			session.setAttribute(Constants.SESSION_0100_NEED_FEE, 0);
		} else {
			showFee = false;
		}
		return null;
    }

	/**
	 * @return the investmentsList
	 */
	public ArrayList<Investment> getInvestmentsList() {
		return investmentsList;
	}

	/**
	 * @param investmentsList the investmentsList to set
	 */
	public void setInvestmentsList(ArrayList<Investment> investmentsList) {
		this.investmentsList = investmentsList;
	}

	/**
	 * @return the investmentCount
	 */
	public long getInvestmentCount() {
		return investmentCount;
	}

	/**
	 * @param investmentCount the investmentCount to set
	 */
	public void setInvestmentCount(long investmentCount) {
		this.investmentCount = investmentCount;
	}

	/**
	 * @return the investment
	 */
	public Investment getInvestment() {
		return investment;
	}

	/**
	 * @param investment the investment to set
	 */
	public void setInvestment(Investment investment) {
		this.investment = investment;
	}

	public String closeContracts() {
		log.debug("closeeee contract " + investment.getAmount() + " sdfsdf " + investment.getSelectedInvestmentsCount());
		FacesContext context = FacesContext.getCurrentInstance();
    	User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
    	String reply = null;
    	msg = null;
		try {
			ArrayList<Investment> openInvsToClose = InvestmentsManager.getOpenBinary0100Investments(user.getId(), oppId, investment.getTypeId(), investment.getTimeCreated());
            int step = InvestmentsManager.getBinary0100NumberOfContracts(openInvsToClose.get(0));
            log.debug("step: " + step);
			Opportunity opp = null;
	       	try {
	       		opp = InvestmentsManager.getRunningOpportunityById(oppId);
	       		if(opp.getQuoteParams() != null) {
//	       			opp.setBinaryZeroOneHundred(new BinaryZeroOneHundred(opp.getQuoteParams()));
	       		}
	       	} catch (SQLException e) {
				log.debug("cant load opp: " + oppId, e);
				reply = "0000" + CommonUtil.getMessage("error.investment", null);
			}
	       	if (null == reply) {
		       	double precision = Math.pow(10, ConstantsBase.RATE_PRECISISON);
		        double rawRate = CommonUtil.convertToBaseAmount(1, user.getCurrencyId(), new Date(System.currentTimeMillis()));
		        double rate  = Math.round(rawRate*precision)/precision;
		        WebLevelsCache levelsCache = (WebLevelsCache) context.getExternalContext().getApplicationMap().get("levelsCache");
				reply = SlipForm.closeContracts(openInvsToClose, (int) investment.getSelectedInvestmentsCount() / step, opp, user, investment.getTypeId() == InvestmentType.BUY ? InvestmentType.SELL : InvestmentType.BUY, quote, rate, levelsCache, currentLevel, step);
	       	}
			String[] params;
			isCloseContractSuccess = false;
			isValidationError = false;
			if (null == reply) {
				double amount = quote;
		        if (investment.getTypeId() == InvestmentType.SELL) {
		        	amount = 100 - quote;
		        }
		        amount = amount * (int)investment.getSelectedInvestmentsCount() * 100;
				params = new String[1];
				params[0] = CommonUtil.displayAmount(amount, true, user.getCurrencyId());
				if (isCurrencyRuble() && !CommonUtil.isParameterEmptyOrNull(params[0])) {
					params[0] = CommonUtil.removeFractionFromAmount(params[0]);
				}
				msg = CommonUtil.getMessage("binary0100.mainarea.transaction.Success", params);
				isCloseContractSuccess = true;
			} else {
				msg = reply.substring(4);
				isValidationError = true;
			}
			log.debug("replay: " + reply);
		} catch (SQLException e) {
			log.warn("cant close " + investment.getSelectedInvestmentsCount() + " contracts for user " + user.getId() + " oppId " + oppId + " type Id " + investment.getTypeId(), e);
			msg = CommonUtil.getMessage("error.investment", null);
			isValidationError = true;
		} catch (Exception e) {
			log.warn("cant close " + investment.getSelectedInvestmentsCount() + " contracts for user " + user.getId() + " oppId " + oppId + " type Id " + investment.getTypeId() + ". Probably the investment is expired", e);
			msg = CommonUtil.getMessage("error.investment", null);
			isValidationError = true;
		}
		return updateList();
	}

	/**
	 * @return the quote
	 */
	public double getQuote() {
		return quote;
	}

	/**
	 * @param quote the quote to set
	 */
	public void setQuote(double quote) {
		this.quote = quote;
	}

	/**
	 * reutrn the investment type so we know which quote to take offer or bid
	 */
	public boolean isInvTypeBuy() {
		if (investmentsList.size() > 0) {
			if (investmentsList.get(0).getTypeId() == InvestmentType.BUY) {
				return true;
			} else {
				return false;
			}
		}
		return false;//if no inv no metter if its true or false
	}

	/**
	 * reutrn the investment opp type is above or below
	 */
	public boolean isOppTypeAbove() {
		if (investmentsList.size() > 0) {
			if (investmentsList.get(0).getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_ABOVE) {
				return true;
			} else {
				return false;
			}
		}
		return false;//if no inv no metter if its true or false
	}

	public String getListSize() {
		return String.valueOf(investmentsList.size());
	}

	/**
	 * get the total investments amount formatted
	 * @return total amount formatted with currency
	 */
	public String getTotalAmount() {
		if (investmentsList.size() > 0) {
			long amount = investmentsList.get(0).getSumInvest();
			/*String type = "sold";
			if (investmentsList.get(0).getTypeId() == InvestmentType.BUY) {
				type = "bought";
			}*/
			String[] params = {CommonUtil.displayAmount(amount, userCurrency)};
			return CommonUtil.getMessage("binary0100.rightTable.total.risk", params);
		}
		return "";
	}
	
	/**
	 * @return total amount without decimal point (for currency rub)
	 */
	public String getTotalAmountExcludDecimal() {
		return CommonUtil.removeFractionFromAmount(getTotalAmount());
	}

	/**
	 * get the total investments amount formatted
	 * @return total amount formatted with currency
	 */
	public String getMaxReturn() {
		long invCount = 0;
		for (Investment investment : investmentsList) {
			invCount += investment.getInvestmentsCount();
		}
		if (invCount > 0) {
			String[] params = {CommonUtil.displayAmount(invCount * 10000, userCurrency)};
			return CommonUtil.getMessage("binary0100.rightTable.max.return", params);
		}
		return "";
	}
	
	/**
	 * get the total investments amount formatted without decimal point.
	 * @return total amount formatted with currency
	 */
	public String getMaxReturnExcludDecimal() {
		return CommonUtil.removeFractionFromAmount(getMaxReturn());
	}

	/**
	 * @return the oppId
	 */
	public long getOppId() {
		return oppId;
	}

	/**
	 * @param oppId the oppId to set
	 */
	public void setOppId(long oppId) {
		this.oppId = oppId;
	}

	public boolean isCloseContractSuccess() {
		return isCloseContractSuccess;
	}

	public void setCloseContractSuccess(boolean isCloseContractSuccess) {
		this.isCloseContractSuccess = isCloseContractSuccess;
	}

	public boolean isValidationError() {
		return isValidationError;
	}

	public void setValidationError(boolean isValidationError) {
		this.isValidationError = isValidationError;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public long getOppState() {
		return oppState;
	}

	public void setOppState(long oppState) {
		this.oppState = oppState;
	}

	public boolean isOppInOpenState() {
		if (oppState > Opportunity.STATE_CREATED && oppState < Opportunity.STATE_CLOSING_1_MIN || oppState == Opportunity.STATE_BINARY0100_UPDATING_PRICES) {
			return true;
		}
		return false;
	}

	/**
	 * @return the numOfInvestmentsInPage
	 */
	public int getNumOfInvestmentsInPage() {
		return numOfInvestmentsInPage;
	}

	/**
	 * @param numOfInvestmentsInPage the numOfInvestmentsInPage to set
	 */
	public void setNumOfInvestmentsInPage(int numOfInvestmentsInPage) {
		this.numOfInvestmentsInPage = numOfInvestmentsInPage;
	}

	public double getCurrentLevel() {
		return currentLevel;
	}

	public void setCurrentLevel(double currentLevel) {
		this.currentLevel = currentLevel;
	}

	public long getInvestmentCountDisabled() {
		return investmentCountDisabled;
	}

	public void setInvestmentCountDisabled(long investmentCountDisabled) {
		this.investmentCountDisabled = investmentCountDisabled;
	}

	public boolean isHadOpenInv() {
		FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        Calendar SessionDate = (Calendar) session.getAttribute(Constants.LAST_OPEN_INV_0100);
        Calendar now = Calendar.getInstance();
		if (null != SessionDate && SessionDate.after(now)) {
			return true;
		}
		return false;
	}

	public boolean isNewInv() {
		return isNewInv;
	}

	public void setNewInv(boolean isNewInv) {
		this.isNewInv = isNewInv;
	}

	public long getUserCurrencyId() {
	    if (null != userCurrency) {
	        return userCurrency.getId();
	    }
	    return 0;
	}

	public boolean isCurrencyRubleAndOpenState() {
		FacesContext context = FacesContext.getCurrentInstance();
    	User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
    	return user.getCurrencyId() == Constants.CURRENCY_RUB_ID && isOppInOpenState();
	}
	
	public boolean isCurrencyRuble() {
		FacesContext context = FacesContext.getCurrentInstance();
    	User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
    	return user.getCurrencyId() == Constants.CURRENCY_RUB_ID;
	}
	
	public String getSumFee() {
		long sumFee = 0;
		if(investmentsList.size()>0) {
			sumFee = InvestmentsManager.getTotalFee(investmentsList.get(0));
		}
		return CommonUtil.displayAmount(sumFee, userCurrency);
	}

	public boolean isShowFee() {
		return showFee;
	}

	public void setShowFee(boolean showFee) {
		this.showFee = showFee;
	}
	
	public String getCurrentLevel0100(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCurrentLevel0100(i);
	}

	public ArrayList<Long> getInvestmentsCountArray(Investment i) {
		if (i == null) {
			return new ArrayList<>();
		}
		User user = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(), Constants.BIND_USER, User.class);
		i.setCurrencyId(user.getCurrencyId());
		return InvestmentsManager.getInvestmentsCountArray(i);
	}

	public String getSelectedInvestmentsCountTxt(Investment i) {
		if (i == null) {
			return "";
		}
        return String.valueOf(i.getSelectedInvestmentsCount());
    }
}