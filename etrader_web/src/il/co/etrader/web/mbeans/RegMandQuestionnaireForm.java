/**
 * 
 */
package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.util.List;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.QuestionnaireGroup;
import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.QuestionnaireManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.util.ConstantsBase;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.RiskAlertsManagerBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.bl_vos.UserRegulation;
import il.co.etrader.web.util.Constants;

/**
 * @author pavelhe
 *
 */
public class RegMandQuestionnaireForm implements Serializable {
	private static final int ANSWER_YES_IDX = 0;
	private static final int ANSWER_NO_IDX = 1;
	private static final int QUESTION_FUNDS_ORIGIN_IDX = 4;
	private static final int QUESTION_PEP_IDX = 5;
	private static final int QUESTION_TRADE_WISH_IDX = 6;
	
	private static final Logger log = Logger.getLogger(RegMandQuestionnaireForm.class);
	private boolean submitted;
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private QuestionnaireGroup questionnaire;
	private List<QuestionnaireUserAnswer> userAnswers;
	private User user;
	
	/**
	 * @throws Exception
	 */
	public RegMandQuestionnaireForm() throws Exception {
		questionnaire = QuestionnaireManagerBase
							.getAllQuestionnaires()
								.get(QuestionnaireGroup.REGULATION_MANDATORY_QUESTIONNAIRE_NAME);
		user = ApplicationData.getUserFromSession();
		userAnswers = QuestionnaireManagerBase.getUserAnswers(user.getId(), questionnaire.getQuestions(), Writer.WRITER_ID_WEB);
		if (userAnswers.get(0).getAnswerId() == null) {
			userAnswers.get(0).setAnswerId(questionnaire.getQuestions().get(0).getAnswers().get(0).getId());	
		}
		if (userAnswers.get(1).getAnswerId() == null) {
			userAnswers.get(1).setAnswerId(questionnaire.getQuestions().get(1).getAnswers().get(0).getId());	
		}
		if (userAnswers.get(2).getAnswerId() == null) {
			userAnswers.get(2).setAnswerId(questionnaire.getQuestions().get(2).getAnswers().get(0).getId());	
		}
		if (userAnswers.get(3).getAnswerId() == null) {
			userAnswers.get(3).setAnswerId(questionnaire.getQuestions().get(3).getAnswers().get(0).getId());	
		}
		if (userAnswers.get(5).getAnswerId() == null) {
			userAnswers.get(5).setAnswerId(questionnaire.getQuestions().get(5).getAnswers().get(1).getId());	
		}	
		submitted = FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(UserRegulation.REDIRECT_AFTER_REG_MAND_QUEST) != null; //logic for 7 second message
	}
	
	/**
	 * @return
	 */
	public List<QuestionnaireQuestion> getQuestions() {
		return questionnaire.getQuestions();
	}
	
	/**
	 * @return the userAnswers
	 */
	public List<QuestionnaireUserAnswer> getUserAnswers() {
		return userAnswers;
	}

	public boolean isSubmitted() {
		return submitted;
	}

	public void setSubmitted(boolean submitted) {
		this.submitted = submitted;
	}

	/**
	 * @return
	 * @throws Exception
	 */
	public String insertQuestionnaire() throws Exception {
		log.info("Inserting questionnaire [" + questionnaire.getId() + ":" + questionnaire.getName()
					+ "]  for user [" + user.getId() + "]");
		for (QuestionnaireUserAnswer userAnswer : userAnswers) {
			userAnswer.setWriterId(Writer.WRITER_ID_WEB);
		}
		
		List<QuestionnaireQuestion> questions = questionnaire.getQuestions();
		
		// TODO Remove this line when question 5 QUESTION_FUNDS_ORIGIN is viewable again.
		userAnswers.get(QUESTION_FUNDS_ORIGIN_IDX)
						.setAnswerId(questions.get(QUESTION_FUNDS_ORIGIN_IDX).getAnswers().get(ANSWER_YES_IDX).getId());
		
		QuestionnaireManagerBase.updateUserAnswers(userAnswers, false);
		
		FacesContext context = FacesContext.getCurrentInstance();
		UserRegulation userRegulation = (UserRegulation) context
															.getApplication()
																.evaluateExpressionGet(
																	context,
																	Constants.BIND_USER_REGULATION,
																	UserRegulation.class);
		if (userRegulation.getApprovedRegulationStep() < UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE
				&& !userRegulation.isSuspended()) {
			log.warn("User [" + user.getId() + "] answered correct, moving to optional questionnaire...");
			userRegulation.setApprovedRegulationStep(UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE);
			userRegulation.setWriterId(Writer.WRITER_ID_WEB);
			UserRegulationManager.updateRegulationStep(userRegulation);
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(UserRegulation.REDIRECT_AFTER_REG_MAND_QUEST, new Boolean(true));
			submitted = true;
			log.warn("User [" + user.getId() + "] create new issue for regulation documents...");
			
			context.getAttributes().put(ConstantsBase.CREATE_ISSUE_FOR_REGULATION, "true");
			IssuesManagerBase.updatePendingRegulationIssuesForUser(user.getId());
			RiskAlertsManagerBase.createIssueForRegulation(user, true);
		} else {
			log.warn("User regulation status is in unappropriate state [" + userRegulation
						+ "] for [mandatory] questionnaire. Cannot sunmit form. Reloading...");
		}
		
		return Constants.NAV_USER_QUESTIONNAIRE;
	}
	
	public boolean isPEPanswer() {
		List<QuestionnaireQuestion> questions = questionnaire.getQuestions();
		Long PEAnswer = userAnswers.get(QUESTION_TRADE_WISH_IDX).getAnswerId();
		if (PEAnswer == null || PEAnswer==questions.get(QUESTION_TRADE_WISH_IDX).getAnswers().get(ANSWER_YES_IDX).getId()) {
			return true;
		} else {
			return false;
		}
	}

	public void setPEPanswer(boolean pEPanswer) {
		List<QuestionnaireQuestion> questions = questionnaire.getQuestions();
		if (pEPanswer) {
			userAnswers.get(QUESTION_TRADE_WISH_IDX).setAnswerId(questions.get(QUESTION_TRADE_WISH_IDX).getAnswers().get(ANSWER_YES_IDX).getId());
		} else {
			userAnswers.get(QUESTION_TRADE_WISH_IDX).setAnswerId(questions.get(QUESTION_TRADE_WISH_IDX).getAnswers().get(ANSWER_NO_IDX).getId());
		}
	}
	
}
