
package il.co.etrader.web.mbeans;


import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.managers.BubblesManager;
import com.anyoption.common.managers.InvestmentsManagerBase;
import com.anyoption.common.managers.RewardUserTasksManager;
import com.anyoption.common.managers.RewardUserTasksManager.TaskGroupType;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.InvestmentFormatter;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.InvestmentsManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;

public class InvestmentsListForm implements Serializable {
	private static final long serialVersionUID = 4238723728426540085L;
	public static final int ONE_MONTH_PERIOD = 1;
	public static final int TO_DATE_BEFORE_FROM = 0;
	public static final int TO_DATE_DIFF_MORE_THAN_ONE_MONTH_FROM_DATE = 2;
	public static final int TO_DATE_DIFF_LESS_THAN_ONE_MONTH_FROM_DATE = 1;
	private static final Logger logger = Logger.getLogger(InvestmentsListForm.class);

	private Date from;
	private Date to;
	private String isSettled;
	private String marketGroup;
	private String sortColumn;
	private String prevSortColumn;
    private boolean sortAscending = false;
    private String selectedAssetName="";
    private ArrayList marketList;
	private Investment investment;
	private long selectedInv;
	private int showOffSentence;
	private String showOffFacebookText;
	private String showOffTwitterText;
	private String lastShowOff;
	private int isBubble;
	private long bubbleMarket;

	public InvestmentsListForm() throws Exception{
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		GregorianCalendar gc = new GregorianCalendar();
		to = gc.getTime();
		if(!CommonUtil.isHebrewSkin(a.getSkinId())){ // add userOffset to date
			to = CommonUtil.getDateTimeFormaByTz(to, CommonUtil.getUtcOffset());
		}
		gc.add(GregorianCalendar.DAY_OF_MONTH, -30);
		from = gc.getTime();
		marketGroup = "0,0";
		isSettled="0";
		bubbleMarket = 0;
		HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
		lastShowOff = req.getParameter("lastShowOff");
		isBubble = 0;
		updateList();
	}

	public ArrayList getMarketGroupsList() throws Exception {
		User user=ApplicationData.getUserFromSession();
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		//decide by the request query if to include 1T markets or not

		// The check for etrader and mobile is removed, because the only other place where this is called 
		// is if we open the mobile app with a browser on a pc
//		if(a.getSkinId() == Skin.SKIN_ETRADER || CommonUtil.isMobile()){ //we are in Etrader | mobile
		marketList = InvestmentsManager.getAllMarketsAndGroupsByUser(user.getId(), false);
//		}else{
//			marketList = InvestmentsManager.getAllMarketsAndGroupsByUserAnyoption(false, false);
//		}
		return marketList;

	}

	public String updateList() throws Exception{
		return updateList(true);
	}

	public String updateList(boolean goToFirstPage) throws Exception{
		FacesContext context=FacesContext.getCurrentInstance();
		User user=ApplicationData.getUserFromSession();
		HttpServletRequest req = (HttpServletRequest)context.getExternalContext().getRequest();
		selectedAssetName = req.getParameter("investmentsContainer:selectedAssetName");
		if(selectedAssetName == null){
			selectedAssetName=new String(CommonUtil.getMessage("lastlevels.selectAsset", null));
		}
		if(selectedAssetName.indexOf("&amp;")>0)
			selectedAssetName = selectedAssetName.replace("&amp;", "&");
		InvestmentsList invList= (InvestmentsList)context.getApplication().createValueBinding(Constants.BIND_INVESTMENTS_LIST).getValue(context);
		
		compareToFromDates();

		invList.setList(InvestmentsManager.getInvestmentsByUser(user.getId(), from, to, isSettled, marketGroup, isBubble(), bubbleMarket, user.getSkinId()));

		if (goToFirstPage) {
			CommonUtil.setTablesToFirstPage();
		}

//		ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
//		if(a.getSkinId() == Skin.SKIN_ETRADER) { //we are in Etrader
//			marketList = InvestmentsManager.getAllMarketsAndGroupsByUser(user.getId(), false);
//		} else {
//			marketList = InvestmentsManager.getAllMarketsAndGroupsByUserAnyoption(true, false);
//		}
		return null;
	}

	public ArrayList getList() {
		FacesContext context=FacesContext.getCurrentInstance();
		InvestmentsList invList= (InvestmentsList)context.getApplication().createValueBinding(Constants.BIND_INVESTMENTS_LIST).getValue(context);
		return invList.getList();
	}

	public String getCurrentDate() throws Exception {
		User user = ApplicationData.getUserFromSession();
		return CommonUtil.getDateTimeFormatDisplay(new Date(), user.getUtcOffset());
	}

	public String navInvestment() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), "#{investment}", Investment.class);
        ve.getValue(context.getELContext());
        ve.setValue(context.getELContext(), investment);
		return Constants.NAV_INVESTMENT_INFO;
	}

	public String sortList() throws Exception {
		updateList();

		if (sortColumn.equals(prevSortColumn))
			sortAscending=!sortAscending;
		else
			sortAscending=true;

		prevSortColumn=sortColumn;
		if (sortColumn.equals("id"))
			Collections.sort(getList(),new IdComparator(sortAscending));

		if (sortColumn.equals("marketName"))
			Collections.sort(getList(),new MarketNameComparator(sortAscending));


		if (sortColumn.equals("currentLevel"))
			Collections.sort(getList(),new CurrentLevelComparator(sortAscending));

		if (sortColumn.equals("closingLevel"))
			Collections.sort(getList(),new ClosingLevelComparator(sortAscending));

		if (sortColumn.equals("timeCreated"))
			Collections.sort(getList(),new TimeCreatedComparator(sortAscending));

		if (sortColumn.equals("timeEstClosing"))
			Collections.sort(getList(),new TimeEstClosingComparator(sortAscending));

		if (sortColumn.equals("amount"))
			Collections.sort(getList(),new AmountComparator(sortAscending));

        if (sortColumn.equals("amountWithFees"))
            Collections.sort(getList(),new AmountWithFeesComparator(sortAscending));

		if (sortColumn.equals("refund"))
			Collections.sort(getList(),new RefundComparator(sortAscending));

		if (sortColumn.equals("refundUp"))
			Collections.sort(getList(),new RefundUpComparator(sortAscending));

		if (sortColumn.equals("refundDown"))
			Collections.sort(getList(),new RefundDownComparator(sortAscending));

		if (sortColumn.equals("bubbleReturnToWin")) {
			if(sortAscending){
				Collections.sort(getList(), (Investment op1, Investment op2) -> new Double(op1.getBubbleReturnToWin()).compareTo(new Double(op2.getBubbleReturnToWin())));
			}else{
				Collections.sort(getList(), (Investment op1, Investment op2) -> -new Double(op1.getBubbleReturnToWin()).compareTo(new Double(op2.getBubbleReturnToWin())));
			}
		}

		if (sortColumn.equals("bubbleReturnToLose")) {
			if(sortAscending){
				Collections.sort(getList(),(Investment op1, Investment op2) -> new Double(op1.getBubbleReturnToLose()).compareTo(new Double(op2.getBubbleReturnToLose())));
			}else{
				Collections.sort(getList(),(Investment op1, Investment op2) -> -new Double(op1.getBubbleReturnToLose()).compareTo(new Double(op2.getBubbleReturnToLose())));
			}
		}
		
		if (sortColumn.equals("bubbleBorders")) {
			Collections.sort(getList(), new BubbleBordersComparator(sortAscending));
		}

		CommonUtil.setTablesToFirstPage();

		return null;

	}

	public boolean isUpInvestment(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.isUpInvestment(i);
	}

	public boolean isOneTouchInv(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.getIsOneTouchInv(i);
	}

	public boolean isCallOption(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.getIsCallOption(i);
	}

	public boolean isRolledUp(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.isRolledUp(i);
	}

	public boolean isRollUpBought(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.isRollUpBought(i);
	}

	public boolean isGmBought(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.isGmBought(i);
	}

	public boolean isOptionPlusOpportunityTypeId(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.isOptionPlusOpportunityTypeId(i);
	}

	public String getAmountWithoutFeesTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(InvestmentsManagerBase.getAmountWithoutFees(i), i.getCurrencyId());
	}

    public String getInsuranceAmountRUTxt(Investment i) {
    	if (i == null) {
			return "";
		}
        return CommonUtil.displayAmount(i.getInsuranceAmountRU(), i.getCurrencyId());
    }

    public String getInsuranceAmountGMTxt(Investment i) {
    	if (i == null) {
			return "";
		}
        return CommonUtil.displayAmount(i.getInsuranceAmountGM(), i.getCurrencyId());
    }

	public String getOptionPlusFeeTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(i.getOptionPlusFee(), i.getCurrencyId());
	}

	public String getRefundUpTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(InvestmentsManagerBase.getRefundUp(i), i.getCurrencyId());
	}

	public String getRefundWithBonusTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getRefundWithBonusTxt(i);
	}

	public String getRefundDownTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(InvestmentsManagerBase.getRefundDown(i), i.getCurrencyId());
	}

	public String getClosingLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getClosingLevelTxt(i);
	}

	public String getCurrentLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCurrentLevelTxt(i);
	}

	public String getTimeEstClosingTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getTimeEstClosingTxt(i);
	}

	public String getBubbleStartTimeTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getBubbleStartTimeTxt(i);
	}

	public String getBubbleStartTimeDateOnlyTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getBubbleStartTimeDateOnlyTxt(i);
	}

	public static String getTimeCreatedForBubbleTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getTimeCreatedForBubbleTxt(i);
	}

	public String getBubbleEndTimeTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getBubbleEndTimeTxt(i);
	}

	public String getTimeSettledTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getTimeSettledTxt(i);
	}

	public String getAmountTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getAmountTxt(i);
	}

	public String getBubbleReturnToWinTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getBubbleReturnToWinTxt(i);
	}

	public String getBubbleReturnToLoseTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getBubbleReturnToLoseTxt(i);
	}

	public String getBonusName(Investment i) {
		if (i == null) {
			return "";
		}
		if (i.getBonusOddsChangeTypeId() > 0) {
			return CommonUtil.getMessage(i.getBonusDisplayName(), null);
		}
		return null;
	}

	public boolean isShowOffAllowed(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManager.isShowOffAllowed(i);
	}

	public long getUserProfit(Investment i) {
		if (i == null) {
			return 0l;
		}
		return InvestmentsManager.getUserProfit(i);
	}

	public boolean isBubbles(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManager.isBubbles(i);
	}

	public String getShowOffText(Investment i) throws SQLException {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getShowOffText(i);
	}

	public String getShowOffFacebookText(Investment i) throws SQLException {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getShowOffFacebookText(i);
	}

	public String getShowOffTwitterText(Investment i) throws SQLException {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getShowOffTwitterText(i);
	}

	public String getShowOffTwitterTextEncoded(Investment i) throws SQLException, UnsupportedEncodingException {
		if (i == null) {
			return "";
		}
		return URLEncoder.encode(InvestmentFormatter.getShowOffTwitterCleanText(i), "UTF-8");
	}

	private class RefundComparator implements Comparator {
		private boolean ascending;
		public RefundComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Investment op1=(Investment)o1;
   		 		Investment op2=(Investment)o2;
   		 		if (ascending){
   		 			return new Long(InvestmentsManagerBase.getRefundSort(op1)).compareTo(new Long(InvestmentsManagerBase.getRefundSort(op2)));
   		 		}
   		 	return -new Long(InvestmentsManagerBase.getRefundSort(op1)).compareTo(new Long(InvestmentsManagerBase.getRefundSort(op2)));
		}
  	 }


	private class RefundDownComparator implements Comparator {
		private boolean ascending;
		public RefundDownComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Investment op1=(Investment)o1;
   		 		Investment op2=(Investment)o2;
   		 		if (ascending){
   		 			return new Long(InvestmentsManagerBase.getRefundDown(op1)).compareTo(new Long(InvestmentsManagerBase.getRefundDown(op2)));
   		 		}
   		 	return -new Long(InvestmentsManagerBase.getRefundDown(op1)).compareTo(new Long(InvestmentsManagerBase.getRefundDown(op2)));
		}
  	 }

	private class RefundUpComparator implements Comparator {
		private boolean ascending;
		public RefundUpComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Investment op1=(Investment)o1;
   		 		Investment op2=(Investment)o2;
   		 		if (ascending){
   		 			return new Long(InvestmentsManagerBase.getRefundUp(op1)).compareTo(new Long(InvestmentsManagerBase.getRefundUp(op2)));
   		 		}
   		 	return -new Long(InvestmentsManagerBase.getRefundUp(op1)).compareTo(new Long(InvestmentsManagerBase.getRefundUp(op2)));
		}
  	 }

	private class AmountComparator implements Comparator {
		private boolean ascending;
		public AmountComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Investment op1=(Investment)o1;
   		 		Investment op2=(Investment)o2;
   		 		if (ascending){
   		 			return new Double(op1.getAmount()).compareTo(new Double(op2.getAmount()));
   		 		}
   		 	return -new Double(op1.getAmount()).compareTo(new Double(op2.getAmount()));
		}
  	 }

    private class AmountWithFeesComparator implements Comparator {
        private boolean ascending;
        public AmountWithFeesComparator(boolean asc) {
            ascending=asc;
        }
        public int compare(Object o1, Object o2) {
                Investment op1=(Investment)o1;
                Investment op2=(Investment)o2;
                if (ascending){
                    return new Double(InvestmentsManagerBase.getAmountWithFees(op1)).compareTo(new Double(InvestmentsManagerBase.getAmountWithFees(op2)));
                }
            return -new Double(InvestmentsManagerBase.getAmountWithFees(op1)).compareTo(new Double(InvestmentsManagerBase.getAmountWithFees(op2)));
        }
     }


	private class TimeEstClosingComparator implements Comparator {
		private boolean ascending;
		public TimeEstClosingComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Investment op1=(Investment)o1;
   		 		Investment op2=(Investment)o2;
   		 		if (op1.getTimeEstClosing()==null || op2.getTimeEstClosing()==null) {
   		 			return  InvestmentFormatter.getTimeEstClosingTxt(op1).compareTo(InvestmentFormatter.getTimeEstClosingTxt(op2));
   		 		}
   		 		if (ascending){
   		 			return op1.getTimeEstClosing().compareTo(op2.getTimeEstClosing());
   		 		}
   		 	return -op1.getTimeEstClosing().compareTo(op2.getTimeEstClosing());
		}
  	 }

	private class TimeCreatedComparator implements Comparator {
		private boolean ascending;
		public TimeCreatedComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Investment op1=(Investment)o1;
   		 		Investment op2=(Investment)o2;
   		 		if (op1.getTimeCreated()==null || op2.getTimeCreated()==null) {
				return CommonUtil	.getDateTimeFormatDisplay(op1.getTimeCreated(), op1.getUtcOffsetCreated())
									.compareTo(CommonUtil.getDateTimeFormatDisplay(op2.getTimeCreated(), op2.getUtcOffsetCreated()));
   		 		}

   		 		if (ascending){
   		 			return op1.getTimeCreated().compareTo(op2.getTimeCreated());
   		 		}
   		 	return -op1.getTimeCreated().compareTo(op2.getTimeCreated());
		}
  	 }
	
	private class BubbleBordersComparator implements Comparator {
		private boolean ascending;
		public BubbleBordersComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Investment op1=(Investment)o1;
   		 		Investment op2=(Investment)o2;
   		 		if (op1.getBubbleStartTime() == null || op2.getBubbleStartTime() == null) {
   		 			return  InvestmentFormatter.getBubbleStartTimeDateOnlyTxt(op1).compareTo(InvestmentFormatter.getBubbleStartTimeDateOnlyTxt(op2));
   		 		}

   		 		if (ascending){
   		 			return InvestmentFormatter.getBubbleStartTimeDateOnlyTxt(op1).compareTo(InvestmentFormatter.getBubbleStartTimeDateOnlyTxt(op2));
   		 		}
   		 	return -InvestmentFormatter.getBubbleStartTimeDateOnlyTxt(op1).compareTo(InvestmentFormatter.getBubbleStartTimeDateOnlyTxt(op2));
		}
  	 }

	private class ClosingLevelComparator implements Comparator {
		private boolean ascending;
		public ClosingLevelComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Investment op1=(Investment)o1;
   		 		Investment op2=(Investment)o2;
   		 		if (ascending){
   		 			return new Double(op1.getClosingLevel()).compareTo(new Double(op2.getClosingLevel()));
   		 		}
   		 	return -new Double(op1.getClosingLevel()).compareTo(new Double(op2.getClosingLevel()));
		}
  	 }

	private class CurrentLevelComparator implements Comparator {
		private boolean ascending;
		public CurrentLevelComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Investment op1=(Investment)o1;
   		 		Investment op2=(Investment)o2;
   		 		if (ascending){
   		 			return new Double(op1.getCurrentLevel()).compareTo(new Double(op2.getCurrentLevel()));
   		 		}
   		 	return -new Double(op1.getCurrentLevel()).compareTo(new Double(op2.getCurrentLevel()));
		}
  	 }


	private class MarketNameComparator implements Comparator {
		private boolean ascending;
		public MarketNameComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Investment op1=(Investment)o1;
   		 		Investment op2=(Investment)o2;
   		 		if (ascending){
   		 			return new String(op1.getMarketName()).compareTo(new String(op2.getMarketName()));
   		 		}
   		 	return -new String(op1.getMarketName()).compareTo(new String(op2.getMarketName()));
		}
  	 }


	private class IdComparator implements Comparator {
		private boolean ascending;
		public IdComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Investment op1=(Investment)o1;
   		 		Investment op2=(Investment)o2;
   		 		if (ascending){
   		 			return new Long(op1.getId()).compareTo(new Long(op2.getId()));
   		 		}
   		 	return -new Long(op1.getId()).compareTo(new Long(op2.getId()));
		}
  	 }


	public String getPrevSortColumn() {
		return prevSortColumn;
	}
	public void setPrevSortColumn(String prevSortColumn) {
		this.prevSortColumn = prevSortColumn;
	}
	public boolean isSortAscending() {
		return sortAscending;
	}
	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}
	public String getSortColumn() {
		return sortColumn;
	}
	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}


	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public String getIsSettled() {
		return isSettled;
	}

	public void setIsSettled(String isSettled) {
		this.isSettled = isSettled;
	}

	public Investment getInvestment() {
		return investment;
	}
	public void setInvestment(Investment investment) {
		this.investment = investment;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "InvestmentsListForm ( "
	        + super.toString() + TAB
	        + "from = " + this.from + TAB
	        + "to = " + this.to + TAB
	        + "isSettled = " + this.isSettled + TAB

	        + "investment = " + this.investment + TAB
	        + " )";

	    return retValue;
	}

	public String getMarketGroup() {
		return marketGroup;
	}

	public void setMarketGroup(String marketGroup) {
		this.marketGroup = marketGroup;
	}

//	public void setMarketList(ArrayList marketGroupsList) {
//		this.marketList = marketGroupsList;
//	}
//	public ArrayList getMarketList() {
//		return this.marketList ;
//	}

	public String getSelectedAssetName() {
		return selectedAssetName;
	}

	public void setSelectedAssetName(String selectedAssetName) {
		this.selectedAssetName = selectedAssetName;
	}

	public String addShowOff() throws Exception {
		long showOffId = 0;
		try {
			showOffId = InvestmentsManager.addShowOff(selectedInv, showOffSentence);
		} catch (SQLException e) {
			logger.error("ShowOff Error! ", e);
		}

		User user = ApplicationData.getUserFromSession();
		RewardUserTasksManager.rewardTasksHandler(TaskGroupType.SHOW_OFF, user.getId(), 0, showOffId, BonusManagerBase.class, Writer.WRITER_ID_WEB);
		
		return updateList(false);
	}

	public String updateShowOffFB() throws Exception {
		InvestmentsManager.updateShowOff(selectedInv, true, false);

		return null;
	}

	public String updateShowOffTwitter() throws Exception {
		InvestmentsManager.updateShowOff(selectedInv, false, true);

		return null;
	}

	public long getSelectedInv() {
		return selectedInv;
	}

	public void setSelectedInv(long selectedInv) {
		this.selectedInv = selectedInv;
	}

	public int getShowOffSentence() {
		return showOffSentence;
	}

	public void setShowOffSentence(int showOffSentence) {
		this.showOffSentence = showOffSentence;
	}

	public String getShowOffFacebookText() {
		return showOffFacebookText;
	}

	public void setShowOffFacebookText(String showOffShortText) {
		this.showOffFacebookText = showOffShortText;
	}

	public String getLastShowOff() {
		return lastShowOff;
	}

	public void setLastShowOff(String lastShowOff) {
		this.lastShowOff = lastShowOff;
	}

	public String getShowOffTwitterText() {
		return showOffTwitterText;
	}

	public void setShowOffTwitterText(String showOffTwitterText) {
		this.showOffTwitterText = showOffTwitterText;
	}

	public int getIsBubble() {
		return isBubble;
	}

	public void setIsBubble(int isBubble) {
		this.isBubble = isBubble;
	}
	
	public boolean isBubble() {
		if (getIsBubble() == 0) {
			return false;
		} else {
			return true;
		}
	}
	
	public boolean isTesterUser() throws Exception {
		User user = ApplicationData.getUserFromSession();
		if (user.getClassId() == 0 && !user.getIsActive().equals("0")) {
			return true;
		} else {
			return false;
		}
	}
	
	public int compareToFromDates() {

		LocalDate dateFrom = from.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDate dateTo = to.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		if (dateTo.isBefore(dateFrom)) {
			dateTo = LocalDate.now();
			dateFrom = dateTo.minusMonths(ONE_MONTH_PERIOD);
			Instant instantTo = dateTo.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
			Instant instantFrom = dateFrom.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
			setTo(Date.from(instantTo));
			setFrom(Date.from(instantFrom));
			return TO_DATE_BEFORE_FROM;
		}
		long days = ChronoUnit.DAYS.between(dateFrom, dateTo);
		if (days <= 31) {
			return TO_DATE_DIFF_LESS_THAN_ONE_MONTH_FROM_DATE;
		} else {
			dateFrom = dateTo.minusMonths(ONE_MONTH_PERIOD);
			Instant instant = dateFrom.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
			setFrom(Date.from(instant));
			return TO_DATE_DIFF_MORE_THAN_ONE_MONTH_FROM_DATE;
		}
	}
	
	
	public ArrayList<SelectItem> getBubbleMarkets() {
		ArrayList<SelectItem> bubbleMarkets = new ArrayList<SelectItem>();
		bubbleMarkets.add(new SelectItem(new Long(0), "lastlevels.selectAsset"));
		bubbleMarkets = CommonUtil.translateSI(bubbleMarkets);
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		bubbleMarkets.addAll(BubblesManager.getBubbleMarkets(a.getSkinId()));
		return bubbleMarkets;
	}

	public long getBubbleMarket() {
		return bubbleMarket;
	}

	public void setBubbleMarket(long bubbleMarket) {
		this.bubbleMarket = bubbleMarket;
	}
}
