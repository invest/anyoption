package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.MarketRate;
import com.anyoption.common.beans.MarketWidgetData;
import com.anyoption.common.beans.TreeItem;
import com.anyoption.common.beans.base.MarketWidget;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.charts.ChartHistoryCache;
import com.anyoption.common.charts.LevelHistoryCache;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.util.ConstantsBase;
import com.google.gson.Gson;

import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.MarketsManager;
import il.co.etrader.web.bl_vos.MarketWidgetChartJSON;
import il.co.etrader.web.bl_vos.MarketsWidgetJSON;
import il.co.etrader.web.util.Constants;

/**
 * @author EranL
 *
 */
public class MarketsWidgetDataForm implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(MarketsWidgetDataForm.class);
	private String openMarkets;
	private String locale;
	private long skinId;
	private ArrayList<TreeItem> assetDDList = new ArrayList<TreeItem>();
	
	/**
	 * Set skinId and locale
	 */
	public MarketsWidgetDataForm() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		Map<String, String> pm = context.getExternalContext().getRequestParameterMap();		
		Skins skin = null;
        try {
        	skinId = Long.valueOf((String) pm.get("s"));            	
        } catch (Exception e) { 
            log.warn("Can't parse skin id taking default value " + e.getMessage());
            skinId = Skin.SKIN_ENGLISH;        
        }
        skin = ApplicationData.getSkinById(skinId);
        if (skin == null) {
        	skinId = Skin.SKIN_ENGLISH;
        	skin = ApplicationData.getSkinById(skinId);
        } 
        session.setAttribute(Constants.SKIN_ID, String.valueOf(skinId));
		locale = ApplicationData.getLanguage(skin.getDefaultLanguageId()).getCode();	
	}
	
	
	/**
	 * Build hourly binary markets data for widget.
	 * In addition we will have to save open markets list   
	 * In the end send the result as JSON format.  
	 * The JSON object will contains: 
	 * A. Default market to display when the widget is started 
	 * B. Markets widget includes:
	 * 	1. Odds win
	 *  2. Time estimate closing in millisecond
	 *  3. Current level in double
	 *  4. Current level - formated String
	 *  5. Distribution for calls - will be randomly between 40% and 60%
	 *  6. Distribution for puts - will be randomly between 40% and 60%
	 *  7. Asset name
	 *  
	 * @return
	 */
	public String getMarketsData() {
		MarketsWidgetJSON result = new MarketsWidgetJSON();		
		FacesContext context = FacesContext.getCurrentInstance();	
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		ServletContext sc = request.getSession().getServletContext();
		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);				
		WebLevelsCache wlc = (WebLevelsCache) sc.getAttribute("levelsCache");
		ArrayList<MarketWidget> marketWidgetList = new  ArrayList<MarketWidget>();
		try {
			Locale loc = new Locale(locale);  	
			//default market
			long[] hpm = wlc.getMarketsOnHomePage(skinId);
			if (hpm != null && hpm.length > 0) {
				result.setDefaultMarketId(hpm[0]);	
			}		
			//open binary markets
			long[] martketsIds = wlc.getOpenedMarkets();	        	
			StringBuffer  openMarketsListBuffer = new StringBuffer();
			openMarketsListBuffer.append("{");      
			String delim = ConstantsBase.EMPTY_STRING;		
			ChartHistoryCache chc = (ChartHistoryCache) sc.getAttribute("chartHistoryCache");		
	        // markets per skin 
	        Map<Long, String> marketsSkinHM = MarketsManager.getSkinsMarkets(ap.getSkinId());
	        for (long marketId : martketsIds) {
	        	//check if market exists for skin 
	        	if (marketsSkinHM.get(marketId) == null) {
	        		continue;
	        	}
	        	MarketWidget marketWidget = new MarketWidget();           
	            marketWidget.setId(marketId);
	    		LevelHistoryCache levelHistoryCache = (LevelHistoryCache) sc.getAttribute("levelHistoryCache");
	    		Map<SkinGroup, List<MarketRate>> ratesMap = levelHistoryCache.getMarketHistory(marketId);
	    		List<MarketRate> marketRate = null;
	    		if (ratesMap != null) {
	    			marketRate = ratesMap.get(ApplicationData.getSkinById(skinId).getSkinGroup());  
	    			if (marketRate.size() == 0) { // in case no level was found
	    				continue;	
	    			}
	    			marketRate.get(marketRate.size() - 1); // current level    	
	    			marketWidget.setCurrentLevel(marketRate.get(marketRate.size() - 1).getRate().doubleValue());
	    			marketWidget.setCurrentLevelStr(CommonUtil.formatLevelByMarket(marketWidget.getCurrentLevel(), marketId));    			    			    		
	    		} else { // in case no level was found 
	    			continue;
	    		}	    			
	    		MarketWidgetData marketWidgetData = chc.getMarketWidgetData(marketId); 
	    		if (marketWidgetData != null) {
	    			marketWidget.setTimeEstClosingMillsec(marketWidgetData.getTimeEstClosingMillsec());
	    			marketWidget.setPageOddsWin(marketWidgetData.getPageOddsWin());
	    		} else {
	    			continue;
	    		}	    		
	    		marketWidget.setName(MarketsManagerBase.getMarketName(skinId, marketId));
	    		// set random call and put parameters between 40% and 60%
	    		Random rand = new Random();
	    		int randomNum = 40 + rand.nextInt(21);
	    		marketWidget.setCallsTrend(randomNum);
	    		marketWidget.setPutsTrend(100 - randomNum);
	    		marketWidgetList.add(marketWidget);
	    		openMarketsListBuffer.append(delim).append(marketId + ":" + marketId);
	    		delim = ",";
	        }   
	        assetDDList.addAll(ap.getSkin().getTreeItems());
		    openMarketsListBuffer.append("}");
		    openMarkets = openMarketsListBuffer.toString();
		} catch (Exception e) {
			log.error("Error while trying to getMarketsData", e);
			openMarkets = "{}";
		}
		String jsonResponse = "";
		try {						
			Gson gson = new Gson();						
			result.setMarkets(marketWidgetList.toArray(new MarketWidget[marketWidgetList.size()]));			
			// serialize the JSON response for client			
			jsonResponse = gson.toJson(result);
		} catch (Exception e) {
			log.error("Error while trying to send response ", e);
		}
		return jsonResponse;
	}
	
	/**
	 * Build market rates data for widget.   
	 * In the end send the result as JSON format.  
	 * @return
	 */
	public String getMarketRates() {
		FacesContext context = FacesContext.getCurrentInstance();
		Map<String, String> parameterMap = context.getExternalContext().getRequestParameterMap();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		ServletContext servletContext = request.getSession().getServletContext();
		LevelHistoryCache levelHistoryCache = (LevelHistoryCache) servletContext.getAttribute("levelHistoryCache");
		long marketId = 0;
        if (null != parameterMap.get("marketId")) {
            try {
                marketId = Long.parseLong((String) parameterMap.get("marketId"));
            } catch (Exception e) {               
                log.warn("Can't parse marketId - " + e.getMessage());
            }
        }
        MarketWidgetChartJSON result = new MarketWidgetChartJSON();
		Map<SkinGroup, List<MarketRate>> ratesMap = levelHistoryCache.getMarketHistory(marketId);
		if (ratesMap != null) {
	        log.debug("get chart history for: " + marketId);
	        HttpServletRequest req = (HttpServletRequest)context.getExternalContext().getRequest();	        
	        List<MarketRate> l = null;
	        l = ratesMap.get(ApplicationData.getSkinById(ApplicationData.getSkinId(req.getSession(), req)).getSkinGroup());
	        result.setMarketRate(l);
	        log.debug("return rates: " + (null != l ? l.size() : 0));
		}		 
		String jsonResponse = "";
		try {						
			Gson gson = new Gson();												
			// serialize the JSON response for client			
			jsonResponse = gson.toJson(result);
		} catch (Exception e) {
			log.error("Error while trying to send response ", e);
		}
		return jsonResponse;
	}


	/**
	 * @return the openMarkets
	 */
	public String getOpenMarkets() {
		return openMarkets;
	}

	/**
	 * @param openMarkets the openMarkets to set
	 */
	public void setOpenMarkets(String openMarkets) {
		this.openMarkets = openMarkets;
	}

	/**
	 * @return the locale
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * @param locale the locale to set
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}

	/**
	 * @return the assetDDList
	 */
	public ArrayList<TreeItem> getAssetDDList() {
		return assetDDList;
	}

	/**
	 * @param assetDDList the assetDDList to set
	 */
	public void setAssetDDList(ArrayList<TreeItem> assetDDList) {
		this.assetDDList = assetDDList;
	}
	
	
	
}