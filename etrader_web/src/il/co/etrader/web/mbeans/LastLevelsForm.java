package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.myfaces.custom.datascroller.HtmlDataScroller;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.TreeItem;
import com.anyoption.common.beans.base.Skin;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.MarketsManager;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.InvestmentsManager;

public class LastLevelsForm implements Serializable {
    /**
	 *
	 */
	private static final long serialVersionUID = 7705000093521688601L;

	private static final Logger log = Logger.getLogger(LastLevelsForm.class);

	private Date from;

	private String marketGroup;
	private String marketId;
	private int oppTypeId;
	private String oppTypeIdStr;

	private ArrayList<Opportunity> list;
	private ArrayList<Opportunity> oneTouchList;

	private Map<Long, String> marketGroupsList;
	private ArrayList<TreeItem> marketGroupsListTree;
	private ArrayList marketGroupsOneTouchList;
    private Map<String, String> marketGroupsOptionPlusList;

	private ArrayList oneTouchWeeks = new ArrayList();
	private ArrayList<Date> oneTouchDatesList;
	private int weekIdx;
	private String selectedAssetName="";
	private boolean isBinary0100;
	private boolean isDynamics;

	private HtmlDataScroller scroll = new HtmlDataScroller();

	/**
     * Resets the Scroller index to the given <code>index</code>
     * @param index
     */
    public void resetScrollerIndex(int index) {
        if(getScroll() != null && getScroll().isPaginator()) {
            getScroll().getUIData().setFirst(index);
        }
    }


	public LastLevelsForm() throws SQLException, UnsupportedEncodingException {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData a = (ApplicationData) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);

		Calendar c = Calendar.getInstance(TimeZone.getTimeZone(CommonUtil.getUtcOffset()));
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		from = c.getTime();
		log.debug("utcOffset: " + CommonUtil.getUtcOffset() + " from: " + from);
		marketGroup = "0,0";
		marketId = "0";
		selectedAssetName = CommonUtil.getMessage("lastlevels.selectAsset", null);
		weekIdx = -1; // default: all dates for oneTouch filter
		isBinary0100 = false;
		isDynamics = false;
		
		String page = context.getViewRoot().getViewId();
		if (page.indexOf("OneTouch") == -1) {   // last levels pages without 1T
			oppTypeId = (int)Opportunity.TYPE_REGULAR;
			oppTypeIdStr = Opportunity.TYPE_REGULAR_STAING;
			if (page.indexOf("lastLevelsOP") > -1 ) {
				oppTypeId = (int)Opportunity.TYPE_OPTION_PLUS;
				oppTypeIdStr = Opportunity.TYPE_OPTION_PLUS_STAING;
			}
			else if (page.indexOf("lastLevelsBinary0100") > -1 ) {
				oppTypeIdStr = Opportunity.TYPE_BINARY_0_100_STAING;
				isBinary0100 = true;
				isDynamics = false;
			} else if (page.indexOf("lastLevelsDynamics") > -1 ) {
				oppTypeId = (int)Opportunity.TYPE_DYNAMICS;
				oppTypeIdStr = String.valueOf(Opportunity.TYPE_DYNAMICS);
				isDynamics = true;
				isBinary0100 = false;
			}
			else {
				isBinary0100 = false;
				isDynamics = false;
			}
			updateList();
			
			if(CommonUtil.isMobile()){ //we are in Etrader
				marketGroupsList = InvestmentsManager.getClosedOpportunitiesMarkets(from, oppTypeIdStr);
			} else {
                if (page.indexOf("lastLevelsOP") > -1) {
                    HashMap<String, String> map = a.getOptionPlusMarketsList();
                	marketGroupsOptionPlusList = new LinkedHashMap<String, String>();
                    map.entrySet().stream().sorted(Map.Entry.<String, String>comparingByValue())
        	                .forEachOrdered(entry -> marketGroupsOptionPlusList.put(entry.getKey(), entry.getValue()));
                } else {
                	if (isBinary0100) {
                		Map<Long, String> map = new LinkedHashMap<Long, String>();
                		map.putAll(MarketsManager.getMarketsBinary0100(a.getSkinId()));
						if (null == marketGroupsList) {
							marketGroupsList = new LinkedHashMap<Long, String>();
						}
						map.entrySet().stream().sorted(Map.Entry.<Long, String>comparingByValue())
								.forEachOrdered(entry -> marketGroupsList.put(entry.getKey(), entry.getValue()));
                	} else if (isDynamics) {
                		Map<Long, String> map = new LinkedHashMap<Long, String>();
                		map.putAll(MarketsManager.getMarketsDynamics(a.getSkinId()));
                		if (null == marketGroupsList) {
							marketGroupsList = new LinkedHashMap<Long, String>();
						}
                		map.entrySet().stream().sorted(Map.Entry.<Long, String>comparingByValue())
						.forEachOrdered(entry -> marketGroupsList.put(entry.getKey(), entry.getValue()));
                	}
                	else {
                		if (a.getSkinId() == Skin.SKIN_ETRADER) {
                			marketGroupsList = InvestmentsManager.getClosedOpportunitiesMarkets(from, oppTypeIdStr);
                		} else {  
                			marketGroupsListTree = InvestmentsManager.getClosedOpportunitiesMarketsAnyoption();
                		}
                	}
                }
			}
		} else {  // for oneTouch last levels
			updateOneTouchList();
			bulidOneTouchRangeList();
		}
	}

	public String getTomorrowDate() {

		SimpleDateFormat sf=new SimpleDateFormat("dd-MM-yyyy");
		GregorianCalendar gc=new GregorianCalendar();
		gc.add(GregorianCalendar.DAY_OF_MONTH, 1);
		return sf.format(gc.getTime());
	}

	public Map<Long, String> getMarketGroupsList() {
		return marketGroupsList;
	}

	public void setMarketGroupsList(Map<Long, String> marketGroupsList) {
		this.marketGroupsList = marketGroupsList;
	}
	
	public ArrayList<TreeItem> getMarketGroupsListTree() {
		return marketGroupsListTree;
	}
	
	public void setMarketGroupsListTree(ArrayList<TreeItem> marketGroupsListTree) {
		this.marketGroupsListTree = marketGroupsListTree;
	}

	public int getListSize() {
		return list.size();
	}

	public int getListOneTouchSize() {
		return oneTouchList.size();
	}

	public void updateListEvent(ValueChangeEvent event) throws SQLException, UnsupportedEncodingException,ParseException {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		HttpServletRequest req = (HttpServletRequest)context.getExternalContext().getRequest();
	    String utcOffset = CommonUtil.getUtcOffset();
	    String val = req.getParameter("lastLevelsForm:from");
	    log.debug("utcOffset: " + utcOffset + " val: " + val);
		SimpleDateFormat sf = new SimpleDateFormat("dd.MM.yy");
        sf.setTimeZone(TimeZone.getTimeZone(utcOffset));
		from = sf.parse(val);
		log.debug("from: " + from);
		selectedAssetName = req.getParameter("lastLevelsForm:selectedAssetName");
		marketGroup = (String) event.getNewValue();
		if((isBinary0100 ||isDynamics) && a.getSkinId() != Skin.SKIN_ETRADER) {
			marketGroup = "0," + marketGroup;
		}
		updateList();
		if (!isBinary0100 && !isDynamics) {
			updateOneTouchList();
		}
	}

	/**
	 * Update list from mobile when we update the date
	 * @throws UnsupportedEncodingException
	 * @throws SQLException
	 */
	public void updateListButton() throws UnsupportedEncodingException, SQLException{
		updateList();
		updateOneTouchList();
	}

	public void updateSelectedAsset(ValueChangeEvent event) throws SQLException, UnsupportedEncodingException {
		selectedAssetName = (String) event.getNewValue();
		updateList();
	}

	/**
	 * Update oneTouch list after the Event
	 * @param event
	 * @throws SQLException
	 * @throws UnsupportedEncodingException
	 */
	public void updateOneTouchListEvent(ValueChangeEvent event) throws SQLException, UnsupportedEncodingException {
		marketId = (String) event.getNewValue();   // for oneTouch the value is (marketId, up/down)
		resetScrollerIndex(0);
		updateOneTouchList();
	}


	/**
	 * @return
	 * @throws SQLException
	 * @throws UnsupportedEncodingException
	 */
	public String updateList() throws SQLException, UnsupportedEncodingException {
		FacesContext context = FacesContext.getCurrentInstance();
		Date to = new Date(from.getTime() + 86400000);
		ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		list = InvestmentsManager.getClosedOpportunities(from, to, Long.valueOf(marketGroup.split(",")[1]), Long.valueOf(marketGroup.split(",")[0]), oppTypeIdStr);
		if(a.getSkinId() == Skin.SKIN_ETRADER || CommonUtil.isMobile()) { //we are in Etrader
			marketGroupsList = InvestmentsManager.getClosedOpportunitiesMarkets(from, oppTypeIdStr);
		}

		CommonUtil.setTablesToFirstPage();
		context.renderResponse();
		return null;
	}

	/**
	 * Get oneTouch opportunities list
	 *
	 * @throws SQLException
	 * @throws UnsupportedEncodingException
	 */
	public String updateOneTouchList() throws SQLException, UnsupportedEncodingException {
		resetScrollerIndex(0);
		// get dates by selected week
		Date firstInv = null;
		Date estClosing = null;

		if ( weekIdx > -1 ) {
			firstInv = oneTouchDatesList.get(weekIdx);
			estClosing = oneTouchDatesList.get(weekIdx+1);
		}
		marketGroupsOneTouchList = InvestmentsManager.getClosedOneTouchOpportunitiesMarkets(firstInv, estClosing);
		oneTouchList = InvestmentsManager.getClosedOneTouchOpportunities(firstInv,estClosing,Long.valueOf(marketId));
		return null;
	}


	public ArrayList getList() {
		return list;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public String getMarketGroup() {
		return marketGroup;
	}

	public void setMarketGroup(String marketGroup) {
		this.marketGroup = marketGroup;
	}

	public ArrayList getOneTouchList() {
		return oneTouchList;
	}

	/**
	 * @return the marketGroupsOneTouchList
	 */
	public ArrayList getMarketGroupsOneTouchList() {
		return marketGroupsOneTouchList;
	}

	/**
	 * @param marketGroupsOneTouchList the marketGroupsOneTouchList to set
	 */
	public void setMarketGroupsOneTouchList(ArrayList marketGroupsOneTouchList) {
		this.marketGroupsOneTouchList = marketGroupsOneTouchList;
	}

	/**
	 * @return the oneTouchWeeks
	 */
	public ArrayList getOneTouchWeeks() {
		return oneTouchWeeks;
	}

	/**
	 * @param oneTouchWeeks the oneTouchWeeks to set
	 */
	public void setOneTouchWeeks(ArrayList oneTouchWeeks) {
		this.oneTouchWeeks = oneTouchWeeks;
	}

	/**
	 * @return the weekIdx
	 */
	public int getWeekIdx() {
		return weekIdx;
	}

	/**
	 * @param weekIdx the weekIdx to set
	 */
	public void setWeekIdx(int weekIdx) {
		this.weekIdx = weekIdx;
	}

	/**
	 * @return the marketId
	 */
	public String getMarketId() {
		return marketId;
	}

	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(String marketId) {
		this.marketId = marketId;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "LastLevelsForm ( "
	        + super.toString() + TAB
	        + "from = " + this.from + TAB
	        + "marketGroup = " + this.marketGroup + TAB
	        + "list = " + this.list + TAB
	        + "marketGroupsList = " + this.marketGroupsList + TAB
	        + " )";

	    return retValue;
	}


	/**
	 * Build oneTouch weeks list box
	 * @throws SQLException
	 */
	public void bulidOneTouchRangeList() throws SQLException {

		oneTouchDatesList = InvestmentsManager.getClosedDatesOneOpportunities();
		oneTouchWeeks.add(new SelectItem(-1, CommonUtil.getMessage("general.all", null)));   // default
		for( int i = 0 ; i < oneTouchDatesList.size() ; i += 2 ) {
			// take first time and last time invest and create oneTouch dates list
			Date firstTimeInv = oneTouchDatesList.get(i);
			Date estClosing = oneTouchDatesList.get(i+1);
			SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
			formater.setTimeZone(TimeZone.getTimeZone(CommonUtil.getUtcOffset()));
			String dateRange = formater.format(firstTimeInv) + " " +
				CommonUtil.getMessage("opportunities.one.date.delimiter", null)
				+ " " + formater.format(estClosing);
			oneTouchWeeks.add(new SelectItem(i, dateRange));
		}

	}

	public HtmlDataScroller getScroll() {
		return scroll;
	}

	public void setScroll(HtmlDataScroller scroll) {
		this.scroll = scroll;
	}


	public String getSelectedAssetName() {
		return selectedAssetName;
	}


	public void setSelectedAssetName(String selectedAssetName) {
		this.selectedAssetName = selectedAssetName;
	}


    /**
     * @return the marketGroupsOptionPlusList
     */
    public Map<String, String> getMarketGroupsOptionPlusList() {
        return marketGroupsOptionPlusList;
    }


    /**
     * @param marketGroupsOptionPlusList the marketGroupsOptionPlusList to set
     */
    public void setMarketGroupsOptionPlusList(Map<String, String> marketGroupsOptionPlusList) {
        this.marketGroupsOptionPlusList = marketGroupsOptionPlusList;
    }

    /**
     * Update Option Plus list after the Event
     * @param event
     * @throws SQLException
     * @throws UnsupportedEncodingException
     */
    public void updateOptionPlusEvent(ValueChangeEvent event) throws SQLException, UnsupportedEncodingException {
        marketGroup = (String) event.getNewValue();   // market id
        resetScrollerIndex(0);
        updateList();
    }
    
    /**
     * Update Binary 0100 list after press submit
     * @throws SQLException
     * @throws UnsupportedEncodingException
     */
    public void updateBinary0100() throws UnsupportedEncodingException, SQLException {
    	if (!marketGroup.contains(",")) {
    		marketGroup = "0," + marketGroup;
    	}
        updateList();
    }
    
    /**
     * Return dates range of oneTouch opp from first_inv to last_inv
     *
     * @return
     */
    public String getOneTouchDateRange(Date timeFirstInvest, Date timeEstClosing) {
        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
        String dateRange = formater.format(timeFirstInvest)
                + " "
                + CommonUtil.getMessage("opportunities.one.date.delimiter", null) 
                + " " 
                + formater.format(timeEstClosing);
        return dateRange;
    }
    
    public String getPrintBinary0100Event(Opportunity op) {
        String result = null;
        String[] params = null;
        params=new String[3];
        params[0]=CommonUtil.getMarketName(op.getMarketId());
        params[1]=CommonUtil.formatLevelByMarket(op.getCurrentLevel(), op.getMarketId());
        params[2]=CommonUtil.getTimeFormat(op.getTimeEstClosing(), CommonUtil.getUtcOffset(ConstantsBase.EMPTY_STRING));

        if (op.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_ABOVE) {
          result = CommonUtil.getMessage("opportunity.above", params);
        }
        if (op.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_BELOW) {
            result = CommonUtil.getMessage("opportunity.below", params);
        }

        return result;
    }
    
}