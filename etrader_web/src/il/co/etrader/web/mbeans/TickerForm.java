package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.TickerManager;
import il.co.etrader.web.helper.TradingPageBoxBean;

/**
 * Ticker line backing bean.
 * 
 * @author Tony
 */
public class TickerForm implements Serializable {
    private static final long serialVersionUID = 1L;
    
    public ArrayList<TradingPageBoxBean> getTickerMarkets() throws SQLException {
        long skinId = CommonUtil.getAppData().getSkinId();
        return TickerManager.getSkinTickerMarkets(skinId);
    }
}