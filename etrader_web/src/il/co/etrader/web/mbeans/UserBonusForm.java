package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.UserRegulationManager;

import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.bl_vos.UserRegulation;
import il.co.etrader.web.util.Constants;

public class UserBonusForm implements Serializable {

	private static final long serialVersionUID = -5175882101950060699L;

	private static final Logger logger = Logger.getLogger(UserBonusForm.class);

	private List<BonusUsers> bonusList;
	private BonusUsers bonusUser;
	private Date from;
	private Date to;
	private long filterState;
	private long bonusId;

	public UserBonusForm() throws Exception {

		FacesContext context = FacesContext.getCurrentInstance();
		Map pm = context.getExternalContext().getRequestParameterMap();
		try {
			if (null != pm.get("stateId")) {
				filterState = Long.valueOf(pm.get("stateId").toString());
			}
			if (null != pm.get(Constants.BONUS_NAVIGATION_PARAM)) {
				bonusId = Long.parseLong(pm.get(Constants.BONUS_NAVIGATION_PARAM).toString());
			}
		} catch (Exception e) {
			logger.info("cant parse state id or bonusId: " + pm.get("stateId").toString() + "setting it to 0");
		}

		to = new Date();
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(to);
		to = gc.getTime();
		gc.add(GregorianCalendar.DAY_OF_MONTH, -30);
		from = gc.getTime();

		updateBonusList();
	}

	public int getListSize() {
		return bonusList.size();
	}

	public List<BonusUsers> getBonusList() {
		return bonusList;
	}

	public void setBonusList(List<BonusUsers> bonusList) {
		this.bonusList = bonusList;
	}

	public String updateBonusList() throws Exception {

		User user = ApplicationData.getUserFromSession();
		String utcOffset = CommonUtil.getUtcOffset();
		bonusList = BonusManagerBase.getAllUserBonus(user.getId(), from, to, filterState, utcOffset, ApplicationData.getCurrenciesList());
		bonusList = BonusManagerBase.getTurnoverParamsForBonus(bonusList);
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	// remove bonus for user by id
	public void cancelBonusById() throws Exception {
		User user = ApplicationData.getUserFromSession();
		bonusUser = BonusManagerBase.getBonusUserByBonusId(user.getId(), bonusId);
		if (null != bonusUser){
			approveCancel();
		}
	}

	/**
	 * Constructs a <code>String</code> with all attributes in name = value
	 * format.
	 *
	 * @return a <code>String</code> representation of this object.
	 */
	public String toString() {
		final String TAB = " \n ";

		String retValue = "";

		retValue = "UserBonusForm ( " + super.toString() + TAB + "bonusList = " + this.bonusList + " )";

		return retValue;
	}

	/**
	 * @return the bonusUser
	 */
	public BonusUsers getBonusUser() {
		return bonusUser;
	}

	/**
	 * @param bonusUser the bonusUser to set
	 */
	public void setBonusUser(BonusUsers bonusUser) {
		this.bonusUser = bonusUser;
	}

	public String navToCancelApprovePage() {
		bindBonus(bonusUser);

		return Constants.NAV_CANCEL_BONUS_APPROVE;
	}

	public String approveCancel() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		
		if (null == bonusUser) {
			bonusUser = getBonusFromSession();
		}

		BonusUsers tempBu = null;
		try{
			tempBu = BonusManagerBase.getBonusUserByBonusId(bonusUser.getUserId(), bonusUser.getBonusId());
		} catch (SQLException e) {
			logger.info("cant find bonus id: " + bonusUser.getId());
		}

		if (null != tempBu &&
				(tempBu.getBonusStateId() == ConstantsBase.BONUS_STATE_ACTIVE ||
				tempBu.getBonusStateId() == ConstantsBase.BONUS_STATE_GRANTED)){
			try {
				bonusUser.setWriterIdCancel(Writer.WRITER_ID_WEB);
				BonusManagerBase.cancelBonusToUser(bonusUser, Constants.BONUS_STATE_REFUSED, CommonUtil.getUtcOffset(), CommonUtil.getWebWriterId(), user.getSkinId(), CommonUtil.getIPAddress(), loginId);
			} catch (SQLException e) {
				logger.info("cant cancel bonus id: " + bonusUser.getId());
			}
			logger.info("bonus id: " + bonusUser.getId() + " canceled");
			bindBonus(new BonusUsers());
		}
		return Constants.NAV_BONUS;
	}

	public String notApproveCancel() {
		bindBonus(new BonusUsers());
		return Constants.NAV_BONUS;
	}

	public String navToDetailsPage() {
		bindBonus(bonusUser);

		return Constants.NAV_TERMS_BONUS;
	}

	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}

	private void bindBonus(BonusUsers b) {
		FacesContext context = FacesContext.getCurrentInstance();
        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_BONUS_USERS, BonusUsers.class);
        ve.getValue(context.getELContext());
        ve.setValue(context.getELContext(), b);
	}

	private BonusUsers getBonusFromSession() {
		FacesContext fc = FacesContext.getCurrentInstance();
		return (BonusUsers)fc.getApplication().createValueBinding(Constants.BIND_BONUS_USERS).getValue(fc);
	}

	/**
	 * @return the filterState
	 */
	public long getFilterState() {
		return filterState;
	}

	/**
	 * @param filterState the filterState to set
	 */
	public void setFilterState(long filterState) {
		this.filterState = filterState;
	}

	public ArrayList getBonusStatusSI() {
		return CommonUtil.translateSI(ApplicationData.getBonusStates());
	}

	public ArrayList getBonusStatusSINotTrans() {
		return ApplicationData.getBonusStates();
	}

	public long getBonusId() {
		return bonusId;
	}

	public void setBonusId(long bonusId) {
		this.bonusId = bonusId;
	}

	/**
	 * acceptBonusUser
	 * @return String
	 */
	public String acceptBonusUser() {
		User user = null;
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
			long loginId = 0L;
			try {
				if (session.getAttribute(Constants.LOGIN_ID) != null) {
					loginId = (long) session.getAttribute(Constants.LOGIN_ID);
				}
			} catch (Exception e) {
				logger.error("Cannot get login ID from session!", e);
				loginId = 0L;
			}
			user = ApplicationData.getUserFromSession();
			if (null != bonusUser){
				if(!isCanClaimBonusRegulation(user)){
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.minwithdraw1", null), null);
					context.addMessage(null, fm);
				}else{
					BonusManagerBase.acceptBonusUser(bonusUser, user, CommonUtil.getIPAddress(), loginId);
					updateBonusList();
				}
			}
		} catch (Exception e) {
			logger.info("Error: Problem to accept bonus user. userId: " + user.getId() + ", bonusUserId: " + bonusUser.getId());
		}
		return null;
	}
	
	private boolean isCanClaimBonusRegulation(User user) throws SQLException{
		boolean res = true;
		if(user.getSkin().isRegulated() && user.getSkinId() != Skin.SKIN_ETRADER){
			UserRegulation ur = new UserRegulation();
			if(UserRegulationManager.isRestrictedAO(ur, user.getSkinId())){
				res = false;
			}
		}		
		return res;
	}
}
