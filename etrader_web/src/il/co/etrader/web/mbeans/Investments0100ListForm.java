
package il.co.etrader.web.mbeans;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.managers.InvestmentsManagerBase;
import com.anyoption.common.managers.MarketsManagerBase;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.InvestmentFormatter;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.InvestmentsManager;
import il.co.etrader.web.bl_managers.MarketsManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;

/**
 * @author liors
 *
 */
public class Investments0100ListForm implements Serializable {
	private static final long serialVersionUID = 4238723728426540085L;

	private static final Logger logger = Logger.getLogger(Investments0100ListForm.class);

	private Date from;
	private String isSettled;
	//private String marketGroup;
	private String sortColumn;
	private String prevSortColumn;
    private boolean sortAscending = false;
    private String selectedAssetName;
//    private ArrayList marketList;
	private Investment investment;
	private long selectedInv;
	private int showOffSentence;
	private String showOffFacebookText;
	private String showOffTwitterText;
	private String lastShowOff;
	private String selectedExpiryHour;
	private long marketId; //market user selected from the market list.
	private long expiryHourId;
	private boolean clickSubmit = false;
	private boolean isOffHours0100;

	public Investments0100ListForm() throws Exception{
		FacesContext context = FacesContext.getCurrentInstance();
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(gc.getTime());
		//from = userLastInvestBinary0100Date();
		from = gc.getTime();
		expiryHourId = 0;
		//selectedExpiryHour = marketCurrentDisplayCloseHour();
		isSettled = "0";
		isOffHours0100 = false;

		InvestmentsList invList = (InvestmentsList)context.getApplication().createValueBinding(Constants.BIND_INVESTMENTS_LIST).getValue(context);
		invList.setList(new ArrayList());
	}

//	public ArrayList getMarketGroupsList() throws Exception {
//		User user=ApplicationData.getUserFromSession();
//		FacesContext context = FacesContext.getCurrentInstance();
//		ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
//		//decide by the request query if to include 1T markets or not
//
//		if (a.getSkinId() == Skin.SKIN_ETRADER || CommonUtil.isMobile()){ //we are in Etrader | mobile
//			marketList = InvestmentsManager.getAllMarketsAndGroupsByUser(user.getId(), true);
//		} else {
//			marketList = InvestmentsManager.getAllMarketsAndGroupsByUserAnyoption(false, true);
//		}
//		return marketList;
//
//	}

	public String updateList() throws Exception{
		setClickSubmit(true);
		return updateList(true);
	}

	public String updateList(boolean goToFirstPage) throws Exception{
		FacesContext context=FacesContext.getCurrentInstance();
		User user = ApplicationData.getUserFromSession();
		HttpServletRequest req = (HttpServletRequest)context.getExternalContext().getRequest();
		if (isOffHours0100) {
			isSettled = "1";
			from = getUserLastInvestBinary0100Date();
			isOffHours0100 = false;
		}
		selectedAssetName = req.getParameter("investments0100Container:selectedAssetName");
		if (selectedAssetName == null) {
			selectedAssetName = new String(CommonUtil.getMessage("lastlevels.selectAsset", null));
		}
		if (selectedAssetName.indexOf("&amp;") > 0)
			selectedAssetName = selectedAssetName.replace("&amp;", "&");
		InvestmentsList invList = (InvestmentsList)context.getApplication().createValueBinding(Constants.BIND_INVESTMENTS_LIST).getValue(context);
		invList.setList(InvestmentsManager.getAllBinary0100InvestmentsByUserId(user.getId(), isSettled.equalsIgnoreCase("1") ? true : false ,
				marketId, from, isSettled.equalsIgnoreCase("1") ? timeWithUserOffset(selectedExpiryHour) : null, user.getSkinId()));
		if (goToFirstPage) {
			CommonUtil.setTablesToFirstPage();
		}

//		ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
//		if(a.getSkinId() == Skin.SKIN_ETRADER) { //we are in Etrader
//			marketList = InvestmentsManager.getAllMarketsAndGroupsByUser(user.getId(), true);
//		} else {
//			marketList = InvestmentsManager.getAllMarketsAndGroupsByUserAnyoption(true, true);
//		}
		return null;
	}

	private String timeWithUserOffset(String time) {
		if (CommonUtil.isParameterEmptyOrNull(time)) {
			return "";
		}
		Date d = null;
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		try {
			d = sdf.parse(time);
		} catch (Exception e) {
			logger.error("error, can't parse string to date. 0100", e);
		}
		d = CommonUtil.getDateTimeFormat(d, CommonUtil.getUtcOffset());

		return sdf.format(d);
	}

	public ArrayList getList() {
		FacesContext context=FacesContext.getCurrentInstance();
		InvestmentsList invList= (InvestmentsList)context.getApplication().createValueBinding(Constants.BIND_INVESTMENTS_LIST).getValue(context);
		return invList.getList();
	}

	public String getCurrentDate() throws Exception {
		User user = ApplicationData.getUserFromSession();
		return CommonUtil.getDateTimeFormatDisplay(new Date(), user.getUtcOffset());
	}

	public String navInvestment() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), "#{investment}", Investment.class);
        ve.getValue(context.getELContext());
        ve.setValue(context.getELContext(), investment);
		return Constants.NAV_INVESTMENT_INFO;
	}

	public String sortList() throws Exception {
		updateList();

		if (sortColumn.equals(prevSortColumn))
			sortAscending=!sortAscending;
		else
			sortAscending=true;

		prevSortColumn=sortColumn;
		if (sortColumn.equals("id"))
			Collections.sort(getList(),new IdComparator(sortAscending));

		if (sortColumn.equals("marketName"))
			Collections.sort(getList(),new MarketNameComparator(sortAscending));


		if (sortColumn.equals("currentLevel"))
			Collections.sort(getList(),new CurrentLevelComparator(sortAscending));

		if (sortColumn.equals("closingLevel"))
			Collections.sort(getList(),new ClosingLevelComparator(sortAscending));

		if (sortColumn.equals("timeCreated"))
			Collections.sort(getList(),new TimeCreatedComparator(sortAscending));

		if (sortColumn.equals("timeEstClosing"))
			Collections.sort(getList(),new TimeEstClosingComparator(sortAscending));

		if (sortColumn.equals("amount"))
			Collections.sort(getList(),new AmountComparator(sortAscending));

        if (sortColumn.equals("amountWithFees"))
            Collections.sort(getList(),new AmountWithFeesComparator(sortAscending));

		if (sortColumn.equals("refund"))
			Collections.sort(getList(),new RefundComparator(sortAscending));

		if (sortColumn.equals("refundUp"))
			Collections.sort(getList(),new RefundUpComparator(sortAscending));

		if (sortColumn.equals("refundDown"))
			Collections.sort(getList(),new RefundDownComparator(sortAscending));

		CommonUtil.setTablesToFirstPage();

		return null;

	}


	private class RefundComparator implements Comparator {
		private boolean ascending;
		public RefundComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Investment op1=(Investment)o1;
   		 		Investment op2=(Investment)o2;
   		 		if (ascending){
   		 			return new Long(InvestmentsManagerBase.getRefundSort(op1)).compareTo(new Long(InvestmentsManagerBase.getRefundSort(op2)));
   		 		}
   		 	return -new Long(InvestmentsManagerBase.getRefundSort(op1)).compareTo(new Long(InvestmentsManagerBase.getRefundSort(op2)));
		}
  	 }


	private class RefundDownComparator implements Comparator {
		private boolean ascending;
		public RefundDownComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Investment op1=(Investment)o1;
   		 		Investment op2=(Investment)o2;
   		 		if (ascending){
   		 			return new Long(InvestmentsManagerBase.getRefundDown(op1)).compareTo(new Long(InvestmentsManagerBase.getRefundDown(op2)));
   		 		}
   		 	return -new Long(InvestmentsManagerBase.getRefundDown(op1)).compareTo(new Long(InvestmentsManagerBase.getRefundDown(op2)));
		}
  	 }

	private class RefundUpComparator implements Comparator {
		private boolean ascending;
		public RefundUpComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Investment op1=(Investment)o1;
   		 		Investment op2=(Investment)o2;
   		 		if (ascending){
   		 			return new Long(InvestmentsManagerBase.getRefundUp(op1)).compareTo(new Long(InvestmentsManagerBase.getRefundUp(op2)));
   		 		}
   		 	return -new Long(InvestmentsManagerBase.getRefundUp(op1)).compareTo(new Long(InvestmentsManagerBase.getRefundUp(op2)));
		}
  	 }

	private class AmountComparator implements Comparator {
		private boolean ascending;
		public AmountComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Investment op1=(Investment)o1;
   		 		Investment op2=(Investment)o2;
   		 		if (ascending){
   		 			return new Double(op1.getAmount()).compareTo(new Double(op2.getAmount()));
   		 		}
   		 	return -new Double(op1.getAmount()).compareTo(new Double(op2.getAmount()));
		}
  	 }

    private class AmountWithFeesComparator implements Comparator {
        private boolean ascending;
        public AmountWithFeesComparator(boolean asc) {
            ascending=asc;
        }
        public int compare(Object o1, Object o2) {
                Investment op1=(Investment)o1;
                Investment op2=(Investment)o2;
                if (ascending){
                    return new Double(InvestmentsManagerBase.getAmountWithFees(op1)).compareTo(new Double(InvestmentsManagerBase.getAmountWithFees(op2)));
                }
            return -new Double(InvestmentsManagerBase.getAmountWithFees(op1)).compareTo(new Double(InvestmentsManagerBase.getAmountWithFees(op2)));
        }
     }


	private class TimeEstClosingComparator implements Comparator {
		private boolean ascending;
		public TimeEstClosingComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Investment op1=(Investment)o1;
   		 		Investment op2=(Investment)o2;
   		 		if (op1.getTimeEstClosing()==null || op2.getTimeEstClosing()==null) {
   		 			return  InvestmentFormatter.getTimeEstClosingTxt(op1).compareTo(InvestmentFormatter.getTimeEstClosingTxt(op2));
   		 		}
   		 		if (ascending){
   		 			return op1.getTimeEstClosing().compareTo(op2.getTimeEstClosing());
   		 		}
   		 	return -op1.getTimeEstClosing().compareTo(op2.getTimeEstClosing());
		}
  	 }

	private class TimeCreatedComparator implements Comparator {
		private boolean ascending;
		public TimeCreatedComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Investment op1=(Investment)o1;
   		 		Investment op2=(Investment)o2;
   		 		if (op1.getTimeCreated()==null || op2.getTimeCreated()==null) {
				return CommonUtil	.getDateTimeFormatDisplay(op1.getTimeCreated(), op1.getUtcOffsetCreated())
									.compareTo(CommonUtil.getDateTimeFormatDisplay(op2.getTimeCreated(), op2.getUtcOffsetCreated()));
   		 		}

   		 		if (ascending){
   		 			return op1.getTimeCreated().compareTo(op2.getTimeCreated());
   		 		}
   		 	return -op1.getTimeCreated().compareTo(op2.getTimeCreated());
		}
  	 }

	private class ClosingLevelComparator implements Comparator {
		private boolean ascending;
		public ClosingLevelComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Investment op1=(Investment)o1;
   		 		Investment op2=(Investment)o2;
   		 		if (ascending){
   		 			return new Double(op1.getClosingLevel()).compareTo(new Double(op2.getClosingLevel()));
   		 		}
   		 	return -new Double(op1.getClosingLevel()).compareTo(new Double(op2.getClosingLevel()));
		}
  	 }

	private class CurrentLevelComparator implements Comparator {
		private boolean ascending;
		public CurrentLevelComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Investment op1=(Investment)o1;
   		 		Investment op2=(Investment)o2;
   		 		if (ascending){
   		 			return new Double(op1.getCurrentLevel()).compareTo(new Double(op2.getCurrentLevel()));
   		 		}
   		 	return -new Double(op1.getCurrentLevel()).compareTo(new Double(op2.getCurrentLevel()));
		}
  	 }


	private class MarketNameComparator implements Comparator {
		private boolean ascending;
		public MarketNameComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Investment op1=(Investment)o1;
   		 		Investment op2=(Investment)o2;
   		 		if (ascending){
   		 			return new String(op1.getMarketName()).compareTo(new String(op2.getMarketName()));
   		 		}
   		 	return -new String(op1.getMarketName()).compareTo(new String(op2.getMarketName()));
		}
  	 }



	/**
	 * This function return a list of binary0100 markets
	 * with the correct translation for the specific skin.
	 *
	 * @return
	 */
	public Map<Long, String> getMarketsZeroOneHundred() {
    	FacesContext context = FacesContext.getCurrentInstance();
        ApplicationData app = (ApplicationData)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);

    	return MarketsManager.getMarketsBinary0100(app.getSkinId());
	}

	/**
	 * This function return a list of bunary0100 markets which the user invest on
	 * with the correct translation for the specific skin.
	 *
	 * @return
	 * @throws Exception
	 */
	public ArrayList<SelectItem> getMarketsBinary0100UserInvestments() throws Exception {
		User user = ApplicationData.getUserFromSession();
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		ArrayList<SelectItem> marketsBinary0100 = new ArrayList<SelectItem>();
		marketsBinary0100 = InvestmentsManager.getMarketsBinary0100UserInvestments(user.getId(), a.getSkinId());

    	return CommonUtil.translateSI(marketsBinary0100);
	}

	/**
	 * This function return a list of bunary0100 markets by session skin.
	 * with the correct translation for the specific skin.
	 *
	 * @return
	 * @throws Exception
	 */
	public Map<Long, String> getMarketsBinary0100() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData app = (ApplicationData)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		Map<Long, String> marketsBinary0100 = new HashMap<Long, String>();
		marketsBinary0100.put(new Long(0),CommonUtil.getMessage("menu.strip.assets", null));
		marketsBinary0100.put(new Long(1),CommonUtil.getMessage("general.all", null));

    	marketsBinary0100.putAll(MarketsManager.getMarketsBinary0100(app.getSkinId()));
    	return marketsBinary0100;
	}

	/**
	 * This method get the expiry hour of a specific user investment by date and the market id.
	 *
	 * @return
	 * @throws Exception
	 */
	public ArrayList<SelectItem> getExpiryHourByUserInvestments() throws Exception {
		User user = ApplicationData.getUserFromSession();
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		ArrayList<SelectItem> expiryHour = new ArrayList<SelectItem>();
		expiryHour = InvestmentsManager.getExpiryHourByUserInvestments(user.getId(), a.getSkinId(), marketId, from);

		return expiryHour;
	}

	/**
	 * This method return a list with all the hours in the day within a half.
	 *
	 * @return
	 * @throws Exception
	 */
	public ArrayList<SelectItem> getExpiryHours() throws Exception {

		return ApplicationData.getHoursInDayByHalf();
	}


	/**
	 * This function return the date, of the user last investment in binary0100 markets.
	 *
	 * @return
	 * @throws Exception
	 */
	public Date getUserLastInvestBinary0100Date() throws Exception {
		Date userLastInvestDate = null;
		User user = ApplicationData.getUserFromSession();
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);

		userLastInvestDate = InvestmentsManager.getLastUserInvestmentDate(user.getId(), a.getSkinId(), true);
		if (userLastInvestDate == null) {
			GregorianCalendar gc = new GregorianCalendar();
			gc.setTime(gc.getTime());
			userLastInvestDate = gc.getTime();
		}
		return userLastInvestDate;
	}

	/**
	 * This function return the time estimate close Hour of specific market.
	 *
	 * Return
	 * @return
	 */
	private String marketCurrentDisplayCloseHour() throws Exception{
		String marketDisplayHour = "";
		User user = ApplicationData.getUserFromSession();
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);

		marketDisplayHour = InvestmentsManager.marketCurrentDisplayCloseHour(user.getId(), a.getSkinId(), marketId);

		return marketDisplayHour;
	}

	private class IdComparator implements Comparator {
		private boolean ascending;
		public IdComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Investment op1=(Investment)o1;
   		 		Investment op2=(Investment)o2;
   		 		if (ascending){
   		 			return new Long(op1.getId()).compareTo(new Long(op2.getId()));
   		 		}
   		 	return -new Long(op1.getId()).compareTo(new Long(op2.getId()));
		}
	}


	public String getPrevSortColumn() {
		return prevSortColumn;
	}
	public void setPrevSortColumn(String prevSortColumn) {
		this.prevSortColumn = prevSortColumn;
	}
	public boolean isSortAscending() {
		return sortAscending;
	}
	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}
	public String getSortColumn() {
		return sortColumn;
	}
	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}


	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public String getIsSettled() {
		return isSettled;
	}

	public void setIsSettled(String isSettled) {
		this.isSettled = isSettled;
	}

	public Investment getInvestment() {
		return investment;
	}
	public void setInvestment(Investment investment) {
		this.investment = investment;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "InvestmentsListForm ( "
	        + super.toString() + TAB
	        + "from = " + this.from + TAB
	        + "isSettled = " + this.isSettled + TAB

	        + "investment = " + this.investment + TAB
	        + " )";

	    return retValue;
	}

	/*public String getMarketGroup() {
		return marketGroup;
	}*/

	/*public void setMarketGroup(String marketGroup) {
		this.marketGroup = marketGroup;
	}*/

//	public void setMarketList(ArrayList marketGroupsList) {
//		this.marketList = marketGroupsList;
//	}
//	public ArrayList getMarketList() {
//		return this.marketList ;
//	}

	public String getSelectedAssetName() {
		return selectedAssetName;
	}

	public void setSelectedAssetName(String selectedAssetName) {
		this.selectedAssetName = selectedAssetName;
	}

	public String addShowOff() throws Exception {
		try {
			InvestmentsManager.addShowOff(selectedInv, showOffSentence);
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		return updateList(false);
	}

	public String updateShowOffFB() throws Exception {
		InvestmentsManager.updateShowOff(selectedInv, true, false);

		return null;
	}

	public String updateShowOffTwitter() throws Exception {
		InvestmentsManager.updateShowOff(selectedInv, false, true);

		return null;
	}

	public long getSelectedInv() {
		return selectedInv;
	}

	public void setSelectedInv(long selectedInv) {
		this.selectedInv = selectedInv;
	}

	public int getShowOffSentence() {
		return showOffSentence;
	}

	public void setShowOffSentence(int showOffSentence) {
		this.showOffSentence = showOffSentence;
	}

	public String getShowOffFacebookText() {
		return showOffFacebookText;
	}

	public void setShowOffFacebookText(String showOffShortText) {
		this.showOffFacebookText = showOffShortText;
	}

	public String getLastShowOff() {
		return lastShowOff;
	}

	public void setLastShowOff(String lastShowOff) {
		this.lastShowOff = lastShowOff;
	}

	public String getShowOffTwitterText() {
		return showOffTwitterText;
	}

	public void setShowOffTwitterText(String showOffTwitterText) {
		this.showOffTwitterText = showOffTwitterText;
	}

	/**
	 * @return the selectedExpiryHour
	 */
	public String getSelectedExpiryHour() {
		return selectedExpiryHour;
	}

	/**
	 * @param selectedExpiryHour the selectedExpiryHour to set
	 */
	public void setSelectedExpiryHour(String selectedExpiryHour) {
		this.selectedExpiryHour = selectedExpiryHour;
	}

	/**
	 * @return the marketId
	 */
	public long getMarketId() {
		return marketId;
	}

	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	/**
	 * @return the expiryHourId
	 */
	public long getExpiryHourId() {
		return expiryHourId;
	}

	/**
	 * @param expiryHourId the expiryHourId to set
	 */
	public void setExpiryHourId(long expiryHourId) {
		this.expiryHourId = expiryHourId;
	}

	/**
	 * @return the clickSubmit
	 */
	public boolean isClickSubmit() {
		return clickSubmit;
	}

	/**
	 * @param clickSubmit the clickSubmit to set
	 */
	public void setClickSubmit(boolean clickSubmit) {
		this.clickSubmit = clickSubmit;
	}

	/**
	 * @return the isOffHours0100
	 */
	public boolean isOffHours0100() {
		return isOffHours0100;
	}

	/**
	 * @param isOffHours0100 the isOffHours0100 to set
	 */
	public void setOffHours0100(boolean isOffHours0100) {
		this.isOffHours0100 = isOffHours0100;
	}

	public String getAmountMaxRiskTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getAmountMaxRiskTxt(i);
	}

	public String getAmountMaxRiskTxtExcludDecimal(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getAmountMaxRiskTxtExcludDecimal(i);
	}

	public String getMaxProfitTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getMaxProfitTxt(i);
	}

	public String getMaxProfitTxtExcludDecimal(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getMaxProfitTxtExcludDecimal(i);
	}

	public String getAmountReturnTxtExcludDecimal(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getAmountReturnTxtExcludDecimal(i);
	}

	public String getAmountReturnTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getAmountReturnTxt(i);
	}

	public String getInvestmentsAmount(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getInvestmentsAmount(i);
	}

	public String getInvestmentsAmountExcludDecimal(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getInvestmentsAmountExcludDecimal(i);
	}

	public String getCurrentLevel0100(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCurrentLevel0100(i);
	}

	public String getClosingLevel0100(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getClosingLevel0100(i);
	}

	public boolean isSettledBeforTime(Investment i) {
		if (i == null) {
			return false;
		}
		return il.co.etrader.bl_managers.InvestmentsManagerBase.isSettledBeforTime(i);
	}

	public String getTimeCreatedTxtByFormat(Investment i, String format) {
		if (i == null) {
			return "";
		}
        return CommonUtil.getTimeFormat(i.getTimeCreated(), i.getUtcOffsetCreated(), format);
    }

	public String getTimeEstClosingTxtByFormat(Investment i, String format) {
		if (i == null) {
			return "";
		}
        return CommonUtil.getTimeFormat(i.getTimeEstClosing(), i.getUtcOffsetCreated(), format);
    }

	public String getTimeSettledTxtByFormat(Investment i, String format) {
		if (i == null) {
			return "";
		}
		return CommonUtil.getTimeFormat(i.getTimeSettled(), i.getUtcOffsetCreated(), format);
	}

	public boolean isTypeBinary0100Above(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManager.isTypeBinary0100Above(i);
	}

	public boolean isTypeBinary0100Below(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManager.isTypeBinary0100Below(i);
	}

	public String getPrintBinary0100Event(Investment i) {
		if (i == null) {
			return "";
		}
		String result = null;
		String[] params = null;
		long skinId = 0L;
		User user = null;
		try {
			user = ApplicationData.getUserFromSession();
		} catch (Exception e) {
			logger.error("Cannot get user from session! ", e);
		}
		if (user != null) {
			skinId = user.getSkinId();
		} else {
			skinId = Skins.SKIN_REG_EN;
		}
		params = new String[3];
		params[0] = MarketsManagerBase.getMarketName(skinId, i.getMarketId());
		params[1] = CommonUtil.formatLevelByDecimalPoint(i.getOppCurrentLevel(), i.getDecimalPoint());
		params[2] = CommonUtil.getTimeFormat(i.getTimeEstClosing(), i.getUtcOffsetCreated(), "HH:mm");
		String messageKeyAbove = "opportunity.above";
		String messageKeyBelow = "opportunity.below";
		if (CommonUtil.getLocaleLanguage().contains(ConstantsBase.ETRADER_LOCALE) && params[0].toUpperCase().contains("DAX")) {
			messageKeyAbove = "opportunity.dax.above";
			messageKeyBelow = "opportunity.dax.below";
		}
		if (i.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_ABOVE) {
			result = CommonUtil.getMessage(messageKeyAbove, params);
		}
		if (i.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_BELOW) {
			result = CommonUtil.getMessage(messageKeyBelow, params);
		}
		return result;
	}

	public String getTotalFeeTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(InvestmentsManager.getTotalFee(i), i.getCurrencyId());
	}
}