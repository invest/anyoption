package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.sql.SQLException;

import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;

import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;

public class BankingForm  implements Serializable{

	private static final long serialVersionUID = 7178739734122885690L;

    private Long countryId;
    private Long currencyId;

	/**
	 * @throws Exception
	 */
	public BankingForm() throws Exception{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		User user = (User)request.getSession().getAttribute(Constants.BIND_SESSION_USER);
		if (null == user || (null != user && user.getId() == 0)) {   // user not in session
			Skins skin = ApplicationData.getSkinById(ap.getSkinId());
			countryId = Long.parseLong(ap.getRealCountryId());
			currencyId = Long.valueOf(skin.getDefaultCurrencyId());
			if (countryId == 0) {
				countryId = skin.getDefaultCountryId();
			}
		} else { // user in session
			countryId = user.getCountryId();
		}

		//EncryptorUpdaterManager.doCheck();
	}

	/**
	 * @return Fax number
	 * @throws Exception
	 */
	public String getFaxNumber() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		return ap.getSupportFaxeByCountryId(String.valueOf(countryId));
	}

    /**
     * @param event
     * @throws SQLException
     */
    public void updateCountry(ValueChangeEvent event) throws SQLException {
    	countryId = (Long)event.getNewValue();
    }

	/**
	 * @return the countryId
	 */
	public Long getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the currencyId
	 */
	public Long getCurrencyId() {
		return currencyId;
	}

	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	public boolean getIsRubel() {
		FacesContext context = FacesContext.getCurrentInstance();
        ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
        Skins skin = ApplicationData.getSkinById(ap.getSkinId());
        Long countryId =  Long.valueOf(skin.getDefaultCountryId());
        User user = null;
        if (context.getExternalContext().isUserInRole("web")) {
        	user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        }
        if (null != ap.getRealCountryId() && !ap.getRealCountryId().equals("0")) {
            countryId = Long.valueOf(ap.getRealCountryId());
        }
        if (skin.getId() == Skins.SKIN_RUSSIAN && null != countryId &&
        		countryId.toString().equals(Constants.RUSSIA_COUNTRY_ID) || (null != user && user.getCurrencyId() == ConstantsBase.CURRENCY_RUB_ID)) {
        	return true;
    	}
        return false;
	}

	public boolean getIsYuan() {
		FacesContext context = FacesContext.getCurrentInstance();
        ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
        Skins skin = ApplicationData.getSkinById(ap.getSkinId());
        Long countryId =  Long.valueOf(skin.getDefaultCountryId());
        User user = null;
        if (context.getExternalContext().isUserInRole("web")) {
        	user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        }
        if (null != ap.getRealCountryId() && !ap.getRealCountryId().equals("0")) {
            countryId = Long.valueOf(ap.getRealCountryId());
        }
        if (skin.getId() == Skins.SKIN_CHINESE && null != countryId &&
        		countryId.toString().equals(Constants.CHINA_COUNTRY_ID) || (null != user && user.getCurrencyId() == ConstantsBase.CURRENCY_CNY_ID)) {
        	return true;
    	}
        return false;
	}



}