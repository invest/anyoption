package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ReutersQuotesManager;
import il.co.etrader.web.bl_vos.ReutersQuotesPopup;

public class ReutersQuotesPopupForm implements Serializable{
    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(ReutersQuotesPopupForm.class);

	private ReutersQuotesPopup rqp;
	private long oppId;


	public ReutersQuotesPopupForm() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		Map pm = context.getExternalContext().getRequestParameterMap();
		if (null != pm.get("oppId")) {
			oppId = Long.valueOf(pm.get("oppId").toString());
		}
		rqp = ReutersQuotesManager.getQuotesByOppId(oppId);
	}
	
	public String getTimeByformat(Date time, String format) {
	    return CommonUtil.getTimeFormat(time, CommonUtil.getUtcOffset(ConstantsBase.EMPTY_STRING), format);
	}


	/**
	 * @return the rqp
	 */
	public ReutersQuotesPopup getRqp() {
		return rqp;
	}

	/**
	 * @param rqp the rqp to set
	 */
	public void setRqp(ReutersQuotesPopup rqp) {
		this.rqp = rqp;
	}
}