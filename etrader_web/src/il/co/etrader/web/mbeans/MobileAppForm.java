package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Country;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.sms.SMSException;
import com.anyoption.common.util.GenerateShortenURL;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.ContactsManager;
import il.co.etrader.bl_managers.SMSManagerBase;
import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.MarketingCombination;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.sms.SMS;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.SendTemplateEmail;
import il.co.etrader.web.bl_managers.AdminManager;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.UsersManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;

public class MobileAppForm implements Serializable{

    private static final long serialVersionUID = -5408147239508040626L;

    private static final Logger logger = Logger.getLogger(MobileAppForm.class);

    private ArrayList<SelectItem> countriesAllowedSMS;
    private long countryId;
    private String realCountryId;
    private String phoneNumber;
    private String phoneType;
    private String email;
    private String areaCodeEtrader;
    private String warAllAreaCodeEtrader;
   


    public MobileAppForm() throws SQLException {

		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);

        countriesAllowedSMS = new ArrayList<SelectItem>();

        for (Country country : ApplicationData.getCountries().values()) {
            if (country.isSmsAvailable()) {
                countriesAllowedSMS.add(new SelectItem(Long.valueOf(country.getId()).longValue(), CommonUtil.getMessage(country.getDisplayName(),null)));
            }
        }

        Collections.sort(countriesAllowedSMS, new CommonUtil.selectItemComparator());

        realCountryId = ap.getRealCountryId();
        if (null != realCountryId && !realCountryId.equals("0")) {  // found country
            countryId = Long.parseLong(realCountryId);
        }
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        phoneType = req.getParameter("phoneType");

        if (CommonUtil.isEtraderUserSkin()) {
        	realCountryId = String.valueOf(ConstantsBase.COUNTRY_ID_IL);
        	countryId = ConstantsBase.COUNTRY_ID_IL;
        }
        
        if (phoneNumber == null || "".equals(phoneNumber.trim())) {
        	phoneNumber = CommonUtil.getMessage("register.phone", null).toUpperCase();
        }
    }



    public String sendSms() {
    	boolean isEtrader = false;
        String msg = null;
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        String senderName = "anyoption";
        if (CommonUtil.isEtraderUserSkin()) {
        	isEtrader = true;
        	senderName = "etrader";
        	// workaround for war_all
        	if (null != warAllAreaCodeEtrader) {
        	    phoneNumber = warAllAreaCodeEtrader.substring(1) + phoneNumber;
        	} else {
        	    phoneNumber = phoneNumber.substring(1) + areaCodeEtrader;}
        }
        String senderNumber = null;
        if (null != phoneType) {
            msg = CommonUtil.getMessage("smartphone.msg." + phoneType, null);

	        long key_value = countryId;
	        String PhoneCode = ApplicationData.getCountryPhoneCodes(String.valueOf(countryId));
	        User user = (User)req.getSession().getAttribute(Constants.BIND_SESSION_USER);
	        Country country = ApplicationDataBase.getCountry(countryId);
	        if (country.isAlphaNumericSender()) {
            	senderName = country.getSupportPhone();
            	senderName = senderName.replaceAll("[^0-9]", "");
            	if(senderName.length() > ConstantsBase.MAX_SIZE_SENDER_NAME){
            		senderName = senderName.substring(PhoneCode.length());
            	}
            }
	        if (null != user && user.getId() > 0) { //user loged in
	        	key_value = user.getId();
	        	try {
	        	    senderNumber = CountryManagerBase.getById(user.getCountryId()).getSupportPhone();
	        	} catch (Exception e) {
	        	    logger.error("Can't get country support number.", e);
	        	}
	        } else {
                try {
                    senderNumber = CountryManagerBase.getById(countryId).getSupportPhone();
                } catch (Exception e) {
                    logger.error("Can't get country support number.", e);
                }
	        }

	        //DEV-7216 for etrader we want to send SMS as short link. need to build url in appsflyer format 
	        msg = getAppsflyerUrlMessage(isEtrader, msg);
	        try {
	            SMSManagerBase.sendTextMessage(senderName, senderNumber, PhoneCode + phoneNumber, msg, key_value, SMSManagerBase.SMS_KEY_TYPE_SMARTPHONE, ConstantsBase.MOBIVATE_PROVIDER, SMS.DESCRIPTION_DOWNLOAD, user.getMobileNumberValidation());
	        } catch (SMSException smse) {
	            logger.warn("Failed to send SMS.", smse);
	            return null;
	        }
	        return Constants.SEND_LINK_TO_MOBILE_SUCCESS;
        }
        return null;
    }

    public String sendEmail() {
        if (null == phoneType) {
            //TODO error msg
            return null;
        }
        FacesContext context = FacesContext.getCurrentInstance();
        ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
        long skinId = ap.getSkinId();
        Skins skin = ApplicationData.getSkinById(skinId);
        String langCode = ApplicationData.getLanguage(skin.getDefaultLanguageId()).getCode();
        HashMap<String,String> params = new HashMap<String,String>();
        params.put(SendTemplateEmail.PARAM_EMAIL, email);
        String subject = CommonUtil.getMessage("mobile.email.subject", null);
        String fileName = "smartPhone.download.email." + phoneType + ".html";
        Contact contactReq = new Contact();
        contactReq.setSkinId(skinId);
        insertContactRequest(contactReq);
        //params.put(SendTemplateEmail.PARAM_USER_FIRST_NAME, u.getFirstName());
        try {
            User u = (User) context.getExternalContext().getSessionMap().get(Constants.BIND_SESSION_USER);
            long recipientUserWriterId = Constants.WRITER_WEB_ID;
            if (null != u) {
            	recipientUserWriterId = u.getWriterId();
            }
            new SendTemplateEmail(params, subject, fileName, langCode, skinId, null, recipientUserWriterId, 0).start();
        } catch (Exception e) {
            logger.warn("Can't send smart phone download link email", e);
            return null;
        }
        return Constants.SEND_LINK_TO_MOBILE_EMAIL_SUCCESS;


    }

    public void insertContactRequest(Contact contactReq) {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        Long combId = null;
        String dynamicParameter  = null;
        Skins skin = ApplicationData.getSkinById(contactReq.getSkinId());

        try {
            combId = UsersManager.getCombinationId(req);
        } catch (Exception e) {
            logger.warn("Can't get combId from session or cookie", e);
        }

        if (null == combId) {
            combId = skin.getDefaultCombinationId();
            logger.log(Level.DEBUG, "combId is null, take default value from skin: " + combId);
        }

        try {
            dynamicParameter = UsersManager.getDynamicParam(req);
        } catch (Exception e) {
            logger.warn("Can't get dynamic param from session or cookie", e);
        }
        contactReq.setCountryId(countryId);

        contactReq.setType(Contact.CONTACT_US_DIRECT_DOWNLOAD_WEB_EMAIL);
        contactReq.setEmail(email);
        contactReq.setWriterId(CommonUtil.getWebWriterId());
        contactReq.setCombId(combId.longValue());
        contactReq.setDynamicParameter(dynamicParameter);
        contactReq.setAff_sub1(CommonUtil.getRequestParameter(req, Constants.AFF_SUB1));
        contactReq.setAff_sub2(CommonUtil.getRequestParameter(req, Constants.AFF_SUB2));
        contactReq.setAff_sub3(CommonUtil.getRequestParameter(req, Constants.AFF_SUB3));
        contactReq.setIp(CommonUtil.getIPAddress());
        contactReq.setPhoneType(phoneType);
        User user = (User)req.getSession().getAttribute(Constants.BIND_SESSION_USER);
        if (null != user && user.getId() > 0) { //user loged in
            contactReq.setUserId(user.getId());
        }

        try {
            ContactsManager.insertContactRequest(contactReq, false, true, user, null).getId();
        } catch (Exception e) {
            logger.warn("Can't insert contact request for direct download email", e);
        }
    }
    
    /**
     * Generate appsflyer URL link to include tracking data. In addition short the URL in order to fit in SMS msg
     * Currently will be only on etrader
     * @param isEtrader
     * @param msg
     * @return
     */
    private String getAppsflyerUrlMessage(boolean isEtrader, String msg) {
    	FacesContext context = FacesContext.getCurrentInstance();
    	HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
    	try {	      		
	    	User user = (User)req.getSession().getAttribute(Constants.BIND_SESSION_USER);
	    	long campaignId 		= 0; 
	    	long combinationId 		= 0;
	    	String dp 				= ConstantsBase.EMPTY_STRING;
	    	String utmSource 		= ConstantsBase.EMPTY_STRING;
	    	String utmMedium 		= ConstantsBase.EMPTY_STRING;
	    	String utmContent		= ConstantsBase.EMPTY_STRING;
	    	String appsflyerUrl 	= CommonUtil.getProperty("homepage.url"); 
	    	String appsflyerOS 	= ConstantsBase.EMPTY_STRING;
	    	if (isEtrader) { 	    		
	        	appsflyerOS = Constants.APPSFLYER_ANDROID_PATH;
	        	if (phoneType.equalsIgnoreCase(Constants.MOBILE_IPHONE_PATH)) {
	        		appsflyerOS = Constants.APPSFLYER_IOS_PATH;
	        	}
	    		// appsflyer URL format: http://www.etrader.co.il/appsflyerios?pid='+utm_source+'&c='+campaginid+'&af_siteid='+comb_id+'&af_sub1='+DP+'&af_sub2='+utm_medium+'&af_sub3='+utm_content
	    		appsflyerUrl = "http://www.etrader.co.il/";
	    		if (user != null && user.getId() > 0) {
	        		combinationId = user.getCombinationId();
	        		dp = user.getDynamicParam();
	    		}	        		
	    		if (combinationId == 0) {	        		
	    			ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
	    			combinationId = Long.valueOf(ap.getSessionCombinationId());	        			
	    			dp = UsersManager.getDynamicParam(req);
	    		}	        			        		
	    		MarketingCombination mc = AdminManager.getCombinationId(combinationId);
	    		campaignId = mc.getCampaignId();
	        	utmSource = mc.getSourceName();
	        	utmMedium = mc.getMediumName();
	        	utmContent = mc.getContentName();	            	
	    		appsflyerUrl += appsflyerOS + "?pid=" + utmSource + "&c=" + campaignId + "&af_siteid=" + combinationId + "&af_sub1=" + dp + "&af_sub2=" + utmMedium + "&af_sub3=" + utmContent;
	    		String shortURL = GenerateShortenURL.shorten(appsflyerUrl);
	    		if (!CommonUtil.isParameterEmptyOrNull(shortURL)) {
	    			msg = msg.replace("http://www.etrader.co.il/" + appsflyerOS , shortURL) ;	
	    		}	        		
	    	}
    	} catch (Exception e) {
    		logger.error("ERROR while trying to generate appsflyer URL ", e);
		}
    	return msg;
    }

    /**
     * if this user country can get sms from us
     * @return true if he can false if he cant
     */
    public boolean isSmsAllowed() {
        if (null != realCountryId && !realCountryId.equals("0")) {  // found country
            return ApplicationData.getCountries().get(Long.parseLong(realCountryId)).isSmsAvailable();
        }
        return false;
    }

    /**
     * @return the countriesAllowedSMS
     */
    public ArrayList<SelectItem> getCountriesAllowedSMS() {
        return countriesAllowedSMS;
    }

    /**
     * @param countriesAllowedSMS the countriesAllowedSMS to set
     */
    public void setCountriesAllowedSMS(ArrayList<SelectItem> countriesAllowedSMS) {
        this.countriesAllowedSMS = countriesAllowedSMS;
    }

    /**
     * @return the countryId
     */
    public long getCountryId() {
        return countryId;
    }

    /**
     * @param countryId the countryId to set
     */
    public void setCountryId(long countryId) {
        this.countryId = countryId;
    }

    public String getCountryPhoneCode() {
        if (null != realCountryId && !realCountryId.equals("0")) {  // found country
            return ApplicationData.getCountryPhoneCodes(realCountryId);
        }
        return "93";
    }

    /**
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber the phoneNumber to set
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * @return the phoneType
     */
    public String getPhoneType() {
        return phoneType;
    }

    /**
     * @param phoneType the phoneType to set
     */
    public void setPhoneType(String phoneType) {
        this.phoneType = phoneType;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }



	/**
	 * @return the areaCodeEtrader
	 */
	public String getAreaCodeEtrader() {
		return areaCodeEtrader;
	}



	/**
	 * @param areaCodeEtrader the areaCodeEtrader to set
	 */
	public void setAreaCodeEtrader(String areaCodeEtrader) {
		this.areaCodeEtrader = areaCodeEtrader;
	}



	public String getWarAllAreaCodeEtrader() {
		return warAllAreaCodeEtrader;
	}



	public void setWarAllAreaCodeEtrader(String warAllAreaCodeEtrader) {
		this.warAllAreaCodeEtrader = warAllAreaCodeEtrader;
	}

	
}