package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

//import com.anyoption.common.beans.BinaryZeroOneHundred;
import com.anyoption.common.beans.ExposureBean;
import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.InvestmentRejects;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunityType;
import com.anyoption.common.beans.ValidatorParamNIOU;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.bl_vos.InvestmentType;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.jms.WebLevelLookupBean;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.managers.FeesManagerBase;
import com.anyoption.common.managers.InvestmentRejectsManagerBase;
import com.anyoption.common.managers.InvestmentsManagerBase;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.managers.RewardUserTasksManager;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.managers.RewardUserTasksManager.TaskGroupType;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.managers.UsersManagerBase;
import com.copyop.common.enums.base.ProfileLinkCommandEnum;
import com.copyop.common.jms.CopyOpAsyncEventSender;
import com.copyop.common.jms.CopyOpEventSender;
import com.copyop.common.jms.events.InvCreateEvent;
import com.copyop.common.jms.events.ProfileManageEvent;
import com.copyop.common.jms.events.TournamentInvestmentEvent;

import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.RiskAlertsManagerBase;
import il.co.etrader.bl_vos.InvestmentLimit;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.InvestmentFormatter;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.InvestmentRejectsManager;
import il.co.etrader.web.bl_managers.InvestmentsManager;
import il.co.etrader.web.bl_managers.UsersManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.bl_vos.UserRegulation;
import il.co.etrader.web.helper.CloseContractsSums;
import il.co.etrader.web.helper.SlipEntry;
import il.co.etrader.web.util.Constants;

public class SlipForm implements Serializable {
    /**
	 *
	 */
	private static final long serialVersionUID = -1194626831191812841L;

	private static Logger log = Logger.getLogger(SlipForm.class);

    private long id;
    private Opportunity add;
//    private int choice;
    private ArrayList slip;
    private SlipEntry print;
    protected ArrayList OneTouchList;
    private ArrayList<SelectItem> unitsSelect;
    private ArrayList<SelectItem> etUnitsSelect;
    private long maxInv1Tunits = 0;
    protected long marketId;
    protected boolean hasOneTouchFacebookPlus = false;

    InvestmentLimit il;

    public SlipForm() {
    	/*FacesContext context = FacesContext.getCurrentInstance();
    	HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
    	isFacebook = false;
    	log.debug("url name " + request.getRequestURL().toString());
    	if (request.getRequestURL().toString().contains("facebook")) {
    		isFacebook = true;
    	}*/
    }

    public boolean getHasOneTouchFacebookPlus()  throws Exception {
		return InvestmentsManager.getHasOneTouchFacebookPlus();
  	}

    public long getOneTouchListSize()  throws Exception {
 	   // get oneTouch opp list ( its occurs when oneTouch page requested ).
 	    updateList();
 	    if (null == OneTouchList) {
 	        return 0;
 	    }
 	    return OneTouchList.size();
 	}

    public String updateList()throws Exception{
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		Locale l = ap.getUserLocale();
		OneTouchList = InvestmentsManager.getOneTouch(ap.getSkinId(), this.marketId, l);

		log.debug("getting opportunities for one touch: ");

		return null;
	}

    /**
     * @return Returns the id.
     */
    public long getId() {
        return id;
    }

    /**
     * @param id The id to set.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return Returns the add.
     */
    public Opportunity getAdd() {
        return add;
    }

    /**
     * @param add The add to set.
     */
    public void setAdd(Opportunity add) {
        this.add = add;
    }

    /**
     * @return Returns the choice.
     */
//    public int getChoice() {
//        return choice;
//    }

    /**
     * @param choice The choice to set.
     */
//    public void setChoice(int choice) {
//        this.choice = choice;
//    }

    /**
     * @return Returns the slip.
     */
    public ArrayList getSlip() {
        clearSuccessful();
//        clearIrrelevant();
        return slip;
    }

    public long getSlipSize() {
        if (null == slip) {
            return 0;
        }
        clearSuccessful();
//        clearIrrelevant();
        return slip.size();
    }

    public void clearSuccessful() {
        int i = 0;
        if (null != slip) {
            while (slip.size() > 0 && i < slip.size()) {
                SlipEntry se = (SlipEntry) slip.get(i);
                if (se.getInvestmentId() > 0) {
                    slip.remove(i);
                } else {
                    i++;
                }
            }
        }
    }

//    protected void clearIrrelevant() {
//        if (null == slip) {
//            return;
//        }
//        String viewId = FacesContext.getCurrentInstance().getViewRoot().getViewId();
//
//        int group = 0;
//        if (null == viewId) {
//            return;
//        } else if (viewId.indexOf("index") != -1) {
//            FacesContext context = FacesContext.getCurrentInstance();
//            String skinId = (String)context.getExternalContext().getSessionMap().get("skinId");
//            LevelsCache levelsCache = (LevelsCache) context.getExternalContext().getApplicationMap().get("levelsCache");
//            int i = 0;
//            while (slip.size() > 0 && i < slip.size()) {
//                SlipEntry se = (SlipEntry) slip.get(i);
//                if (!levelsCache.isOnHomePage(se.getOpportunity().getId(), Integer.parseInt(skinId))) {
//                    slip.remove(i);
//                } else {
//                    i++;
//                }
//            }
//            return;
//        } else if (viewId.indexOf("indices") != -1) {
//            group = 2;
//        } else if (viewId.indexOf("stocks") != -1) {
//            group = 3;
//        } else if (viewId.indexOf("currencies") != -1) {
//            group = 4;
//        } else if (viewId.indexOf("commodities") != -1) {
//            group = 5;
//        }
//        if (group == 0) {
//            return;
//        }
//        int i = 0;
//        while (slip.size() > 0 && i < slip.size()) {
//            SlipEntry se = (SlipEntry) slip.get(i);
//            if (se.getOpportunity().getMarket().getGroupId() != group) {
//                slip.remove(i);
//            } else {
//                i++;
//            }
//        }
//    }

    /**
     * @param slip The slip to set.
     */
    public void setSlip(ArrayList slip) {
        this.slip = slip;
    }

    public String getGroup() {
        StringBuffer sb = new StringBuffer();
        SlipEntry e = null;
        if (null != slip) {
            for (int i = 0; i < slip.size(); i++) {
                e = (SlipEntry) slip.get(i);
                if (i != 0) {
                    sb.append(", ");
                }
                sb.append("\"").append(e.getOpportunity().getMarket().getFeedName()).append("\"");
            }
        }
        return sb.toString();
    }

    public void addToSlip(long id, int choice) {
        if (null == slip) {
            slip = new ArrayList();
        }
        if (slip.size() < 3) {
            boolean found = false;
            SlipEntry e = null;
            for (int i = 0; i < slip.size(); i++) {
                e = (SlipEntry) slip.get(i);
                if (e.getOpportunity().getId() == id && e.getChoice() == choice) {
                    found = true;
                    log.warn("Trying to add to the slip existing entry id: " + id + " choice: " + choice);
                    break;
                }
            }
            if (!found) {
                try {
                    Opportunity o = InvestmentsManager.getRunningOpportunityById(id);
                    log.debug("adding: " + o.toString() + " choice: " + choice);
                    slip.add(new SlipEntry(o, choice));
//                    slip.add(new SlipEntry(InvestmentsManager.getRunningOpportunityById(id), choice));
                } catch (Throwable t) {
                    log.log(Level.WARN, "Can't load running opp.", t);
                }
            }
        } else {
            log.warn("Trying to add entries in full slip! Slip size: " + slip.size() + " id: " + id + " choice: " + choice);
            if (log.isDebugEnabled()) {
                log.debug("In the slip we have:");
                SlipEntry e = null;
                for (int i = 0; i < slip.size(); i++) {
                    e = (SlipEntry) slip.get(i);
                    log.debug("id: " + e.getOpportunity().getId() + " choice: " + e.getChoice());
                }
            }
        }
    }

    public void removeFromSlip(long id, int choice) {
        if (null == slip) {
            return;
        }
        SlipEntry e = null;
        for (int i = 0; i < slip.size(); i++) {
            e = (SlipEntry) slip.get(i);
            if (e.getOpportunity().getId() == id && e.getChoice() == choice) {
                slip.remove(i);
                break;
            }
        }
    }

    public String removeAllFromSlip() {
        slip = null;
        return "slip";
    }

    public String getremoveAllFromSlip() {
    	removeAllFromSlip();
    	return "";
    }

    /**
     * @return The total amount of not submitted investments in the slip.
     */
    private float getTotalAmount() {
        float total = 0;
        if (null != slip) {
            SlipEntry e = null;
            for (int i = 0; i < slip.size(); i++) {
                e = (SlipEntry) slip.get(i);
                if (!(e.getInvestmentId() > 0)) {
                    total += e.getAmount();
                }
            }
        }
        return total;
    }

    private static String deviationCheck(double wwwLevel, double realLevel, SlipEntry e, double pageLevel, double requestAmount, float pageOddsWin, int choice, String userName, String sessionId, String checkNumber, InvestmentRejects invRej) {
        if (wwwLevel != 0 && realLevel != 0) { // check if we have levels (else we have conn prob)
            double checkLevel = wwwLevel;
            BigDecimal deviation = e.getOpportunity().getMarket().getAcceptableDeviation();
            if (checkNumber.equalsIgnoreCase("3")) { //dev 3 check
                checkLevel = realLevel;
                deviation = e.getOpportunity().getMarket().getAcceptableDeviation3();
            }
            double crrSpread = Math.abs(checkLevel - pageLevel);
            double acceptableSpread = checkLevel * deviation.doubleValue();
            if (log.isDebugEnabled()) {
                log.debug("realLevel: " + realLevel +
                        " pageLevel: " + pageLevel +
                        " wwwLevel: " + wwwLevel +
                        " crr spread: " + crrSpread +
                        " deviation: " + deviation +
                        " acceptable spread: " + acceptableSpread);
            }
            if ((crrSpread <= acceptableSpread) ||
                    (checkLevel < pageLevel && choice == InvestmentType.CALL) ||
                    (checkLevel > pageLevel && choice == InvestmentType.PUT)) {

                if (log.isInfoEnabled() && crrSpread > acceptableSpread && checkLevel < pageLevel && choice == InvestmentType.CALL) {
                    log.info("Skip calculating deviation. CALL on higher level.");
                }
                if (log.isInfoEnabled() && crrSpread > acceptableSpread && checkLevel > pageLevel && choice == InvestmentType.PUT) {
                    log.info("Skip calculating deviation. PUT on lower level.");
                }

                return null;
            } else {
                log.warn("Unacceptable deviation" + checkNumber + "." +
                        " user: " + userName +
                        " sessionId: " + sessionId +
                        " market name: " + CommonUtil.getMarketName(e.getOpportunity().getMarketId()) +
                        " exp time: " + e.getOpportunity().getTimeEstClosing() +
                        " type id (CALL = 1, PUT = 2): " + choice +
                        " amount: " + requestAmount +
                        " return: " + pageOddsWin);
                String msgStart = "Unacceptable Deviation " + checkNumber;
                String msgEnd = "";
                if (checkNumber.length() == 0) { //dev 1 check
                	invRej.setRejectTypeId(Constants.INVESTMENT_REJECT_DEVIATION);
                } else if (checkNumber.equalsIgnoreCase("2")) { //dev 2 check
                	msgEnd = " , AmountForDev2: " + e.getOpportunity().getMarket().getAmountForDev2() + " , SecForDev2: " + e.getOpportunity().getMarket().getSecForDev2();
                	invRej.setRejectTypeId(Constants.INVESTMENT_REJECT_DEVIATION_2);
                } else { //dev 3 check
                    invRej.setRejectTypeId(Constants.INVESTMENT_REJECT_DEVIATION_3);
                }
                DecimalFormat sd = new DecimalFormat("###,###,##0.000000");
                invRej.setRejectAdditionalInfo(msgStart + ":, crrSpread: " + sd.format(crrSpread) + " , deviation: " + deviation + " , acceptableSpread: " + sd.format(acceptableSpread) + msgEnd);
                invRej.setRealLevel(realLevel);
                invRej.setWwwLevel(wwwLevel);
                InvestmentRejectsManager.insertInvestmentReject(invRej);
                return CommonUtil.getMessage("error.investment.deviation", null);
            }
        } else {
            log.warn("No www level or real level." +
                    " user: " + userName +
                    " sessionId: " + sessionId);
        }
        return CommonUtil.getMessage("error.investment", null);
    }

    private static String deviation3Check(double wwwLevel, double realLevel, SlipEntry e, double pageLevel, double requestAmount, float pageOddsWin, int choice, String userName, String sessionId, String checkNumber, InvestmentRejects invRej) {
        if (wwwLevel != 0 && realLevel != 0) { // check if we have levels (else we have conn prob)
            BigDecimal random = choice == InvestmentType.CALL ? e.getOpportunity().getMarket().getRandomFloor() : e.getOpportunity().getMarket().getRandomCeiling();
            BigDecimal realLevelWithRnd = random.multiply(new BigDecimal(realLevel)).add(new BigDecimal(realLevel));
            if (log.isDebugEnabled()) {
                log.debug("deviation 3 " +
                        " realLevel: " + realLevel +
                        " pageLevel: " + pageLevel +
                        " wwwLevel: " + wwwLevel +
                        " random: " + random.doubleValue() +
                        " real Level With random: " + realLevelWithRnd.doubleValue());
            }
            if (realLevelWithRnd.doubleValue() <= pageLevel && choice == InvestmentType.CALL ||
                    realLevelWithRnd.doubleValue() >= pageLevel && choice == InvestmentType.PUT) {
                if (log.isInfoEnabled() && realLevelWithRnd.doubleValue() <= pageLevel && choice == InvestmentType.CALL) {
                    log.info("Skip calculating deviation. CALL on higher level.");
                }
                if (log.isInfoEnabled() && realLevelWithRnd.doubleValue() >= pageLevel && choice == InvestmentType.PUT) {
                    log.info("Skip calculating deviation. PUT on lower level.");
                }
                return null;
            } else {
                log.warn("Unacceptable deviation 3." +
                        " user: " + userName +
                        " sessionId: " + sessionId +
                        " market name: " + CommonUtil.getMarketName(e.getOpportunity().getMarketId()) +
                        " exp time: " + e.getOpportunity().getTimeEstClosing() +
                        " type id (CALL = 1, PUT = 2): " + choice +
                        " amount: " + requestAmount +
                        " return: " + pageOddsWin);
                String msgStart = "Unacceptable Deviation 3 ";
                String msgEnd = "";
                invRej.setRejectTypeId(Constants.INVESTMENT_REJECT_DEVIATION_3);
                DecimalFormat sd = new DecimalFormat("###,###,##0.000000");
                invRej.setRejectAdditionalInfo(msgStart + ":, random: " + sd.format(random.doubleValue()) + " , real Level With random: " + sd.format(realLevelWithRnd.doubleValue()) + msgEnd);
                invRej.setRealLevel(realLevel);
                invRej.setWwwLevel(wwwLevel);
                InvestmentRejectsManager.insertInvestmentReject(invRej);
                return CommonUtil.getMessage("error.investment.deviation", null);
            }
        } else {
            log.warn("No www level or real level." +
                    " user: " + userName +
                    " sessionId: " + sessionId);
        }
        return CommonUtil.getMessage("error.investment", null);
    }

    /**
     * Submit an investment from the slip.
     *
     * @param id the id of the opp from the slip to submit
     * @param requestAmount the amount to invest
     * @param pageLevel the level that the user see on the page
     * @param pageOddsWin the odds win that the user see on the page
     * @param pageOddsLose the odds lose that the user see on the page
     * @param choice the choice on this opp that the user want to submit
     * @param all if this submit is part of "submit all" request or not (0 - single submit/1 - submit all)
     * @param utcOffset the UTC offset of the client in minutes (as returned from JS Date.getTimezoneOffset())
     * @return "OK" if the submit was successful else error message.
     */
    public static String submitFromSlip(long id, double requestAmount, double pageLevel, float pageOddsWin, float pageOddsLose, int choice, int all, int utcOffset, int fromGraph, Boolean dev2Second, long loginID) {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        String userName = context.getExternalContext().getRemoteUser();
        String sessionId = session.getId();
        if (userName != null) {
        	userName = userName.toUpperCase();
        }
        if (log.isDebugEnabled()) {
            log.debug("try to submit -" +
                    " id: " + id +
                    " amount: " + requestAmount +
                    " pageLevel: " + pageLevel +
                    " pageOddsWin: " + pageOddsWin +
                    " pageOddsLose: " + pageOddsLose +
                    " choice: " + choice +
                    " all: " + all +
                    " UTC offset: " + utcOffset +
                    " user: " + userName +
                    " sessionId: " + sessionId +
                    " fromGraph: " + fromGraph);
        }

       	if (!context.getExternalContext().isUserInRole("web")) {
            if (log.isInfoEnabled()) {
                log.info("Login first." +
                        " user: " + userName +
                        " sessionId: " + sessionId);
            }
            // the leading "1" of this message will make sure only 1 popup will show with this message
            return "1" + CommonUtil.getMessage("error.investment.login", null);
        }
       	ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
       	User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
       	
       	try {
       		UserRegulation userRegulation = new UserRegulation();
       		log.debug(" userRegulation.isUserBeforeMandQuestDoneStep() " + userRegulation.isUserBeforeMandQuestDoneStep()
       					+ " getApprovedRegulationStep() " + userRegulation.getApprovedRegulationStep()
       					+ " getSuspendedReasonId() " + userRegulation.getSuspendedReasonId()
       					+ " getScoreGroup() " + userRegulation.getScoreGroup()
       					+ " isKnowledgeQuestion() " + userRegulation.isKnowledgeQuestion()
       					+ " isQualified() " + userRegulation.isQualified());
       		
           	//AO Regulation           	
       		if (userRegulation.isUserInDepositStep() 
       				&& (userRegulation.isQualified() || userRegulation.getRegulationVersion() == UserRegulationBase.REGULATION_USER_VERSION)) {
       			log.debug("AO Regulation must be submit  Questionnaire user:" + userRegulation);
       			return CommonUtil.getMessage("error.investment.questionnaire", null);
       		} else if (userRegulation.isSuspended() 
       					|| UserRegulationManager.isRestrictedAO(userRegulation, user.getSkinId())
       						|| UserRegulationManager.isPepProhibited(userRegulation, user.getSkinId())
       							|| userRegulation.isTresholdBlock()) {
       			if (userRegulation.isSuspendedDueDocuments()) {
       				log.debug("AO Regulationu isSuspendedDueDocuments user:" + userRegulation);
       				return CommonUtil.getMessage("error.investment.user.suspended.docs", null);
       			} else {
       				log.debug("AO Regulationu is Suspended or Restricted or Pep Prohibited user:" + userRegulation);
       				String supportPhone = userRegulation.getSupportPhone();
       				return CommonUtil.getMessage("error.investment.user.suspended", new String [] {supportPhone});
       			}
       		}
       		
            //Check non reg suspend
       		if(!SkinsManagerBase.getSkin(user.getSkinId()).isRegulated() && UsersManagerBase.isNonRegUserSuspend(user.getId())){   				
   				log.debug("User is manual Suspended user:" + user.getId());
   				String supportPhone = userRegulation.getSupportPhone();
   				return CommonUtil.getMessage("error.investment.user.suspended", new String [] {supportPhone});
       		}
       	} catch (SQLException e2) {
       		log.error("Unable to check user regulation status", e2);
       		return CommonUtil.getMessage("error.investment", null);
       	}              	

        double rate = CurrencyRatesManagerBase.getInvestmentsRate(user.getCurrencyId());
        if (rate == 0d) {
        	log.error("submitFromSlip: Could not get currency rate - check DB connection pool. Failing investment");
        	return CommonUtil.getMessage("error.investment", null);
        }

        double convertAmount = new Float(requestAmount * 100).longValue() * rate;
        InvestmentRejects invRej = new InvestmentRejects(id,
														pageLevel,
														sessionId,
														requestAmount * 100,
														user.getId(),
														rate,
														pageOddsWin,
														pageOddsLose,
														choice,
														(int) Opportunity.TYPE_REGULAR,
														fromGraph,
														Writer.WRITER_ID_WEB);
        SlipEntry e = null;
        String msg = null;
        long invId = 0;
        try {
        	long s = System.currentTimeMillis();
            e = new SlipEntry(InvestmentsManager.getByIdForInvestmentValidation(id), choice);
            if (log.isDebugEnabled()) {
            	log.debug("TIME: getByIdForInvestmentValidation: " + (System.currentTimeMillis() - s));
            }
            if (e.getOpportunity().getOpportunityTypeId() == Opportunity.TYPE_OPTION_PLUS) {
//            	if (CommonUtil.isHebrewSkin(user.getSkinId())) {
//            		e.setOptionPlusFee(e.getOpportunity().getMarket().getOptionPlusFee());
//            	} else {
//            		if (user.getCurrencyId() == ConstantsBase.CURRENCY_RUB_ID) {
//            			e.setOptionPlusFee(ConstantsBase.OPTION_PLUS_FEE_RUB);
//            		} else if (user.getCurrencyId() == ConstantsBase.CURRENCY_CNY_ID) {
//                        e.setOptionPlusFee(ConstantsBase.OPTION_PLUS_FEE_YUAN);
//            		} else if (user.getCurrencyId() == ConstantsBase.CURRENCY_KRW_ID) {
//                        e.setOptionPlusFee(ConstantsBase.OPTION_PLUS_FEE_KOREAN);
//            		} else {
//            			e.setOptionPlusFee(ConstantsBase.OPTION_PLUS_FEE_OTHER);
//            		}
//            	}
            	e.setOptionPlusFee(FeesManagerBase.getFee(FeesManagerBase.OPTION_PLUS_COMMISSION_FEE, user.getCurrencyId()));
            }
            e.setAmount(requestAmount);
			if (!InvestmentsManager.validateCurrentLevel(pageLevel)) {
				return CommonUtil.getMessage("error.investment", null);
			}
            if (!InvestmentsManager.validateOpportunityType(e.getOpportunity(), Opportunity.TYPE_REGULAR, Opportunity.TYPE_OPTION_PLUS)) {
            	return CommonUtil.getMessage("error.investment", null);
            }

            if (!e.getOpportunity().isDisabled() && !e.getOpportunity().getMarket().isSuspended() && !e.getOpportunity().isSuspended()) {

                // First balance check to display the user a message for amount above balance
            	msg = InvestmentsManager.validateUsersBalance(user, e);
            	if(msg != null) {
                	if((CommonUtil.getProperty("copyop.isEnabled").equalsIgnoreCase("true"))) {
                		if(e.getOpportunity().getOpportunityTypeId() == Opportunity.TYPE_REGULAR) {
	            			if(user.getSkinId() != Skin.SKIN_ETRADER) {
	            				CopyOpEventSender.sendEvent(new ProfileManageEvent(ProfileLinkCommandEnum.LOWBALANCE_COPIERS, user.getId(), 1), CopyOpEventSender.PROFILE_MANAGE_DESTINATION_NAME);
	            				log.debug("Send to copyop");
	            			}
                		}
                	}
            	}

	           	if (null == msg) {
	            	msg = InvestmentsManager.validateInvestmentLimits(user, e, pageOddsWin, pageOddsLose, invRej);
	                if (log.isDebugEnabled()) {
	                    log.log(Level.DEBUG, "TIME: validateInvestmentLimits " + (System.currentTimeMillis() - s));
	                }
	           	}

	           	SkinGroup userSkinGroup = ApplicationData.getSkinById(user.getSkinId()).getSkinGroup();

                s = System.currentTimeMillis();
                WebLevelsCache levelsCache = (WebLevelsCache) context.getExternalContext().getApplicationMap().get("levelsCache");
                WebLevelLookupBean wllb = new WebLevelLookupBean();
                wllb.setOppId(id);
                wllb.setFeedName(e.getOpportunity().getMarket().getFeedName());
                wllb.setSkinGroup(userSkinGroup);
                levelsCache.getLevels(wllb);
                double realLevel = wllb.getRealLevel();
                if (log.isDebugEnabled()) {
                    log.log(Level.DEBUG, "TIME: get current level " + (System.currentTimeMillis() - s));
                }

               	if (null == msg && (dev2Second == null || !dev2Second)) {
               		msg = deviationCheck(wllb.getDevCheckLevel(), wllb.getRealLevel(), e, pageLevel, requestAmount, pageOddsWin, choice, userName, sessionId, "", invRej);
               	}

                if (null == msg && convertAmount > e.getOpportunity().getMarket().getAmountForDev3() * 100 && UsersManager.isUserMarketDisabled(user.getId(), e.getOpportunity().getMarketId(), e.getOpportunity().getScheduled(), true)) {
                    wllb = new WebLevelLookupBean();
                    wllb.setOppId(id);
                    wllb.setFeedName(e.getOpportunity().getMarket().getFeedName());
                    wllb.setSkinGroup(userSkinGroup);
                    levelsCache.getLevels(wllb);
                    realLevel = wllb.getRealLevel();
                    msg = deviationCheck(wllb.getDevCheckLevel(), wllb.getRealLevel(), e, pageLevel, requestAmount, pageOddsWin, choice, userName, sessionId, "3", invRej);
                    if (null != msg) {
                    	if(convertAmount > ConstantsBase.d3Threshold || InvestmentsManager.sendD3Alert(user.getId(), e.getOpportunity().getId(), rate, ConstantsBase.d3Threshold) ) {
                    		sendNotifyMail(userName, convertAmount, pageOddsWin, choice, e, Constants.CURRENCY_USD_ID, pageLevel, "deviation3", wllb.getRealLevel());
                    	}
                    }
                }


                if (null == msg) {
                    float amountForDev2 = e.getOpportunity().getMarket().getAmountForDev2();
                    Calendar currentTime = Calendar.getInstance();
                    Calendar nightStartTime = Calendar.getInstance();
                    Calendar nightEndTime = Calendar.getInstance();
                    nightStartTime.set(Calendar.HOUR_OF_DAY, 22);
                    nightStartTime.set(Calendar.MINUTE, 00);
                    nightStartTime.set(Calendar.SECOND, 00);
                    nightEndTime.set(Calendar.HOUR_OF_DAY, 06);
                    nightEndTime.set(Calendar.MINUTE, 00);
                    nightEndTime.set(Calendar.SECOND, 00);
                    if (nightStartTime.before(currentTime) || nightEndTime.after(currentTime)) { //if we are at night time mean btw 22:00 to 06:00 gmt
                        amountForDev2 = e.getOpportunity().getMarket().getAmountForDev2Night();
                    }

                    if (convertAmount > amountForDev2 * 100) {
                        if (dev2Second == null || dev2Second) { // the second request should wait for dev2 time and make another deviation check
                        	boolean valid = false;
                        	Map<?, ?> sessionParams = null;
                        	if (session.getAttribute(ConstantsBase.REQUEST_PARAMETER_ID + id) instanceof Map<?, ?>) {
                        		sessionParams = (Map<?, ?>) session.getAttribute(ConstantsBase.REQUEST_PARAMETER_ID + id);
                        		synchronized (sessionParams) {
                        			if (session.getAttribute(ConstantsBase.REQUEST_PARAMETER_ID + id) != null) {
		                    			if (sessionParams.get(ConstantsBase.REQUEST_PARAMETER_AMOUNT) != null
		                    					&& ((Double) sessionParams.get(ConstantsBase.REQUEST_PARAMETER_AMOUNT)).doubleValue()  == requestAmount * 100d
		                    					&& sessionParams.get(ConstantsBase.REQUEST_PARAMETER_PAGE_LEVEL) != null
		                    					&& ((Double) sessionParams.get(ConstantsBase.REQUEST_PARAMETER_PAGE_LEVEL)).doubleValue() == pageLevel
		                    					&& sessionParams.get(ConstantsBase.REQUEST_PARAMETER_PAGE_ODDS_WIN) != null
		                    					&& ((Float) sessionParams.get(ConstantsBase.REQUEST_PARAMETER_PAGE_ODDS_WIN)).floatValue() == pageOddsWin
		                    					&& sessionParams.get(ConstantsBase.REQUEST_PARAMETER_PAGE_ODDS_LOSE) != null
		                    					&& ((Float) sessionParams.get(ConstantsBase.REQUEST_PARAMETER_PAGE_ODDS_LOSE)).floatValue() == pageOddsLose
		                    					&& sessionParams.get(ConstantsBase.REQUEST_PARAMETER_CHOISE) != null
		                    					&& ((Integer) sessionParams.get(ConstantsBase.REQUEST_PARAMETER_CHOISE)).intValue() == choice
		                    					&& sessionParams.get(ConstantsBase.REQUEST_PARAMETER_TIME_BEGIN) != null) {
		                    				valid = true;
		                    			}
		                    			session.removeAttribute(ConstantsBase.REQUEST_PARAMETER_ID + id);
                        			}
                        		}
                        	}
                			if (dev2Second == null || valid) {
                				long time = e.getOpportunity().getMarket().getSecForDev2() * 1000;
                				if (dev2Second) {
                					time -= System.currentTimeMillis() - (Long) sessionParams.get(ConstantsBase.REQUEST_PARAMETER_TIME_BEGIN); // sec for dev2 minus elapsed time
                				}
								if (time > 0) {
									try {
										Thread.sleep(time);
									} catch (InterruptedException ie) {
										// do nothing
									}
								}
                				if (log.isDebugEnabled()) {
                					log.debug("Second deviation check.");
                				}
                				s = System.currentTimeMillis();
                				wllb = new WebLevelLookupBean();
                				wllb.setOppId(id);
                				wllb.setFeedName(e.getOpportunity().getMarket().getFeedName());
                				wllb.setSkinGroup(userSkinGroup);
                				levelsCache.getLevels(wllb);
                				if (log.isDebugEnabled()) {
                					log.log(Level.DEBUG, "TIME: get current level " + (System.currentTimeMillis() - s));
                				}
                				msg = deviationCheck(wllb.getDevCheckLevel(), wllb.getRealLevel(), e, pageLevel, requestAmount, pageOddsWin, choice, userName, sessionId, "2", invRej);
                			} else {
                        		msg = CommonUtil.getMessage("error.investment", null);
                        	}
                        } else { // the first request should return the actual seconds for dev2
                        	Map<String, Object> requestParams = new HashMap<String, Object>();
                        	requestParams.put(ConstantsBase.REQUEST_PARAMETER_AMOUNT, requestAmount * 100);
                        	requestParams.put(ConstantsBase.REQUEST_PARAMETER_PAGE_LEVEL, pageLevel);
                        	requestParams.put(ConstantsBase.REQUEST_PARAMETER_PAGE_ODDS_WIN, pageOddsWin);
                        	requestParams.put(ConstantsBase.REQUEST_PARAMETER_PAGE_ODDS_LOSE, pageOddsLose);
                        	requestParams.put(ConstantsBase.REQUEST_PARAMETER_CHOISE, choice);
                        	requestParams.put(ConstantsBase.REQUEST_PARAMETER_TIME_BEGIN, System.currentTimeMillis());
                        	if (session.getAttribute(ConstantsBase.REQUEST_PARAMETER_ID + id) != null) {
                        		log.warn("There is already parameter in session with key: " + ConstantsBase.REQUEST_PARAMETER_ID + id);
                        		if (session.getAttribute(ConstantsBase.REQUEST_PARAMETER_ID + id) instanceof Map<?, ?>) {
	                        		log.warn("Printing values");
	                    			Map<?, ?> map = (Map<?, ?>) session.getAttribute(ConstantsBase.REQUEST_PARAMETER_ID + id);
	                    			for (Object key : map.keySet()) {
	                    				log.warn(key + ": " + map.get(key));
	                    			}
                        		} else {
                        			log.warn("The attribute is not a map");
                        		}
                        	}
                        	session.setAttribute(ConstantsBase.REQUEST_PARAMETER_ID + id, requestParams);
                        	msg = ConstantsBase.DEV2_PARAMETER_NAME + e.getOpportunity().getMarket().getSecForDev2();
                        }
                    }
                }

                if (null == msg && UsersManager.isUserMarketDisabled(user.getId(), e.getOpportunity().getMarketId(), e.getOpportunity().getScheduled(), false)) {
                    log.warn("User Market Disabled..." + printDetails(userName, sessionId, e, choice, requestAmount, pageOddsWin, wllb, pageLevel));
                    invRej.setRejectTypeId(Constants.INVESTMENT_REJECT_USER_MARKET_DISABELD);
                    invRej.setRejectAdditionalInfo("User Market Disabled");
                    InvestmentRejectsManager.insertInvestmentReject(invRej);
                    msg = CommonUtil.getMessage("error.investment.disabled", null);
                }

				ExposureBean exposureBean = null;
				if (null == msg
					&& (exposureBean = InvestmentsManager.checkInvestmentExposure(	e.getOpportunity().getId(),
																					new Float(requestAmount * 100).longValue(),
																					convertAmount, choice, invRej, userSkinGroup,
																					e.getOpportunity().getMarketId(),
																					new Locale(CommonUtil.getLocaleLanguage()),
																					user.getCurrencyId(),
																					e	.getOpportunity()
																						.getScheduled(), user.getSkinId())).getErrorMessage() != null) {
					msg = exposureBean.getErrorMessage();
					if (exposureBean.isMaxExposureReached()) {
						log.warn("Exposure reached"
									+ printDetails(userName, sessionId, e, choice, requestAmount, pageOddsWin, wllb, pageLevel));
						sendNotifyMail(	userName, convertAmount, pageOddsWin, choice, e, Constants.CURRENCY_USD_ID, pageLevel, "exposure",
										null);
					}
				}

                String ip = CommonUtil.getIPAddress();
                if (null == msg && e.getOpportunity().getMarket().getSecBetweenInvestmentsSameIp() > 0 &&
                        UsersManager.hasTheSameInvestmentInTheLastSecSameIp(ip, e.getOpportunity().getMarket().getSecBetweenInvestmentsSameIp(),e.getOpportunity().getId(), invRej)) {
                    if (log.isInfoEnabled()) {
                        log.info("Submitting investment twice in less than " + e.getOpportunity().getMarket().getSecBetweenInvestmentsSameIp() + " sec from same ip:" +
                                " oppId: " + id +
                                " choice: " + choice +
                                " user: " + userName +
                                " sessionId: " + sessionId +
                                " ip: " + ip);
                    }
                    msg = CommonUtil.getMessage("error.investment.twicein10sec", null);
                }

                if (null == msg) {
                    invRej.setSecBetweenInvestments(e.getOpportunity().getMarket().getSecBetweenInvestments());
                    if (UsersManager.hasTheSameInvestmentInTheLastSec(user.getId(), id, choice, invRej)) {
                        if (log.isInfoEnabled()) {
                            log.info("Submitting the same investment twice in less than " + e.getOpportunity().getMarket().getSecBetweenInvestments() + " sec:" +
                                    " oppId: " + id +
                                    " choice: " + choice +
                                    " user: " + userName +
                                    " sessionId: " + sessionId);
                        }
                        msg = CommonUtil.getMessage("error.investment.twicein10sec", null);
                    }
                }

               	if (null == msg) {
               		msg = InvestmentsManager.validateOpportunity(user, e, pageOddsWin, pageOddsLose, invRej);
               	}

                // Second balance check to avoid negative balance by 2 investments
               	if (null == msg) {
               		msg = InvestmentsManager.validateUsersBalance(user, e);
               	}

               	//NIOU validation
               	if (null == msg) {
               		msg = InvestmentsManagerBase.validateNIOUTypeWithCashBalance(
               				new ValidatorParamNIOU(user, e.getOpportunity().getOddsType().getOverOddsWin(), 
               						e.getOpportunity().getOddsType().getOverOddsLose(), e.getOpportunity().getOpportunityTypeId(), 
               						new Double(e.getAmount() * 100).longValue(), pageOddsWin - 1,  1 - pageOddsLose), invRej);
               	}
               	
                if (null == msg) {

                    invId = InvestmentsManager.insertInvestment(user, e, pageLevel, ip, realLevel, wllb.getDevCheckLevel(),
                            CommonUtil.formatJSUTCOffsetToString(utcOffset), rate, fromGraph, pageOddsWin - 1, 1 - pageOddsLose, wllb.isClosingFlagClosest(), loginID);
                    e.setInvestmentId(invId);

                    e.getOpportunity().setCurrentLevel(pageLevel);


                	if(user.getSkinId() != Skin.SKIN_ETRADER) {
                    	if((CommonUtil.getProperty("copyop.isEnabled").equalsIgnoreCase("true"))) {
                    		if(e.getOpportunity().getOpportunityTypeId() == Opportunity.TYPE_REGULAR) {
                				float riskAppetite = ((float) (e.getAmount()/(e.getAmount()+(user.getBalance()/100))));
                				CopyOpAsyncEventSender.sendEvent(
																	new InvCreateEvent(	e.getOpportunity().getId(), invId, e.getChoice(),
																						requestAmount, pageLevel, pageOddsWin, pageOddsLose,
																						user.getId(), e.getOpportunity().getMarketId(),
																						rate, riskAppetite, CopyOpInvTypeEnum.SELF, -1,
																						Writer.WRITER_ID_WEB, user.getCurrencyId(),
																						e.getOpportunity().getTimeLastInvest().getTime(),
																						e.getOpportunity().getTimeEstClosing().getTime(),
																						e.getOpportunity().getScheduled()));
                    			log.debug("Send to copyop:"+invId);
                    		}
                    	}
                	}

                    int productTypeId = OpportunityType.PRODUCT_TYPE_BINARY;
                	if (e.getOpportunity().getOpportunityTypeId() == Opportunity.TYPE_OPTION_PLUS) {
                		productTypeId = OpportunityType.PRODUCT_TYPE_OPTION_PLUS;
                	}

                    if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
                		CommonUtil.sendNotifyForInvestment(
	        								e.getOpportunity().getId(),
	        								invId,
	        								convertAmount,
	        								new Double(requestAmount * 100d).longValue(),
	        								e.getChoice(),
	    									e.getOpportunity().getMarketId(),
	    									productTypeId,
	    									pageLevel,
	    									levelsCache,
	    									user,
	    									CopyOpInvTypeEnum.SELF);
                		
                		CopyOpEventSender.sendEvent(new TournamentInvestmentEvent(
                				CommonUtil.getProductTypeId((int) e.getOpportunity().getOpportunityTypeId()), 
                				convertAmount, user.getId(), user.getSkinId()), CopyOpEventSender.TOURNAMENT_INVESTMENT);
                    }

                    RewardUserTasksManager.rewardTasksHandler(TaskGroupType.INVESTMENT, user.getId(),  new Float(e.getAmount() * 100).longValue() + e.getOptionPlusFee(), invId, BonusManagerBase.class,
                    		Constants.WRITER_WEB_ID, productTypeId, e.getOpportunity().getOpportunityTypeId());
                }
            } else {
		if (log.isInfoEnabled()) {
		    log.info("Opportunity disabled - user: " + context.getExternalContext().getRemoteUser()
			    + " sessionId: " + session.getId() + " disabled: " + e.getOpportunity().isDisabled()
			    + " market suspended: " + e.getOpportunity().getMarket().isSuspended()
			    + " opportunity suspended: " + e.getOpportunity().isSuspended());
		}
                if (e.getOpportunity().isDisabled()) {
                    invRej.setRejectTypeId(Constants.INVESTMENT_REJECT_OPP_DISABELD);
                    invRej.setRejectAdditionalInfo("Disabled:, opp id: " + id );
                    InvestmentRejectsManager.insertInvestmentReject(invRej);
                }
		if (e.getOpportunity().isSuspended()) {
		    invRej.setRejectTypeId(ConstantsBase.INVESTMENT_REJECT_OPP_SUSPENDED);
		    invRej.setRejectAdditionalInfo("Suspended:, opp id: " + id);
		    InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
		}
                msg = CommonUtil.getMessage("error.investment.disabled", null);
            }
        } catch (Throwable t) {
            log.error("Failed to insert investment." +
                    " user: " + userName +
                    " sessionId: " + sessionId, t);
            msg = CommonUtil.getMessage("error.investment", null);
        }

        try {
			PopulationsManagerBase.invest(	user.getId(), null == msg ? true : false, CommonUtil.getWebWriterId(), null, null,
											new Float(requestAmount * 100).longValue() - e.getOptionPlusFee(), e.getInvestmentId(), e.getOpportunity().getOpportunityTypeId());
        } catch (Exception exp) {
        	log.warn("Problem with population invest event " + exp);
		}

        String reply = (null == msg ? "OK" + invId : msg);

//      Enable / disable sms upon invest
		if (null == msg) {
			try {
				long smsMinAmount = InvestmentsManager.getSmsInvestmentLimit(user.getCurrencyId());
				smsMinAmount = smsMinAmount / 100;
				if (requestAmount >= smsMinAmount && ap.isSmsAvaliableForUser()) {
					reply += "SMS=1";
				} else {
					reply += "SMS=0";
				}
			} catch (Exception exp) {
	        	log.warn("can't get SMS limit " + exp);
			}
		}
        return reply;
    }

    private static String printDetails(String userName, String sessionId, SlipEntry e, int choice, double requestAmount, float pageOddsWin, WebLevelLookupBean wllb, double pageLevel) {
		return  " user: " + userName +
		        " sessionId: " + sessionId +
		        " market name: " +CommonUtil.getMarketName(e.getOpportunity().getMarketId()) +
		        " exp time: " + e.getOpportunity().getTimeEstClosing() +
		        " type id (CALL = 1, PUT = 2): " + choice +
		        " amount: " + requestAmount +
		        " return: " + pageOddsWin +
		        " realLevel: " + wllb.getRealLevel() +
		        " pageLevel: " + pageLevel +
		        " wwwLevel: " + wllb.getDevCheckLevel();
	}

	public void updateAmount(long id, float amount, int choice) {
        if (null != slip) {
            SlipEntry e = null;
            for (int i = 0; i < slip.size(); i++) {
                e = (SlipEntry) slip.get(i);
                if (e.getOpportunity().getId() == id && e.getChoice() == choice) {
                    e.setAmount(amount);
                }
            }
        }
    }

    public void setEntryToPrint(long id) throws Exception {
        print = new SlipEntry();
        print.setInvestmentId(id);

        FacesContext context = FacesContext.getCurrentInstance();
        ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
        User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        print.setUserId(user.getId());
        long skinId = ap.getSkinId();
        InvestmentsManager.getInvestmentDetailsForPrint(print, skinId);
        if (log.isDebugEnabled()) {
            log.debug("Investment details for print: " + print);
        }
    }

    /**
     * @return the print
     */
    public SlipEntry getPrint() {
        return print;
    }

    public String getLsServer() {
        return CommonUtil.getProperty("lightstreamer.url");
    }

    /* for thew new login
    public String getLoggedIn() {
    	FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		if (user.getId()>0) {
			return "1";
		}
		return "0";

    }

    public boolean isUserLoggedIn() {
    	FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		if (user.getId()>0) {
			return true;
		}
		return false;
    }*/

    public String getLoggedIn() {
        FacesContext context = FacesContext.getCurrentInstance();
        return context.getExternalContext().isUserInRole("web") ? "1" : "0";
    }

    public boolean isUserLoggedIn() {
        FacesContext context = FacesContext.getCurrentInstance();
        return context.getExternalContext().isUserInRole("web");
    }

    public String getUpdateFrequencyLoggedIn() {
        return CommonUtil.getProperty("tradingpages.frequency.loggedin");
    }

    public String getUpdateFrequencyNotLoggedIn() {
        return CommonUtil.getProperty("tradingpages.frequency.notloggedin");
    }

    public boolean isHasOpened() throws Exception {
        FacesContext context = FacesContext.getCurrentInstance();
        Map rm = context.getExternalContext().getRequestMap();
        ArrayList<Investment> l = (ArrayList<Investment>) rm.get("opened_investments");
        if (null == l) {
            loadOpenedInvestments();
        }
        l = (ArrayList<Investment>) rm.get("opened_investments");
        if (null != l && l.size() > 0) {
            return true;
        }
        return false;
    }

    public ArrayList<Investment> getOpened() throws Exception {
        FacesContext context = FacesContext.getCurrentInstance();
        Map rm = context.getExternalContext().getRequestMap();
        ArrayList<Investment> l = (ArrayList<Investment>) rm.get("opened_investments");
        if (null == l) {
            loadOpenedInvestments();
        }
        return (ArrayList<Investment>) rm.get("opened_investments");
    }

    public long getTimeToFirstOpenedExpire() throws Exception {
        FacesContext context = FacesContext.getCurrentInstance();
        Map rm = context.getExternalContext().getRequestMap();
        ArrayList<Investment> l = (ArrayList<Investment>) rm.get("opened_investments");
        if (null == l) {
            loadOpenedInvestments();
        }
        l = (ArrayList<Investment>) rm.get("opened_investments");
        if (null != l && l.size() > 0) {
            Investment i = l.get(0);
            int advance = 1;
            if (i.getMarketId() == 14 ||
                    i.getMarketId() == 15 ||
                    i.getMarketId() == 16 ||
                    i.getMarketId() == 17 ||
                    i.getMarketId() == 18 ||
                    i.getMarketId() == 26) {
                advance = 65;
            }
            return (i.getTimeEstClosing().getTime() - System.currentTimeMillis()) / 1000 + advance;
        }
        return 10;
    }

    private void loadOpenedInvestments() throws Exception {
        FacesContext context = FacesContext.getCurrentInstance();
        User user = ApplicationData.getUserFromSession();
       	context.getExternalContext().getRequestMap().put("opened_investments", InvestmentsManager.getTodaysOpenedInvestments(user.getId()));
    }

    public boolean isHasAnyOpenedInvestments() throws SQLException{
    	FacesContext context = FacesContext.getCurrentInstance();
    	User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        return InvestmentsManager.isHasOpened(user.getId());
    }

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	@Override
	public String toString() {
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "SlipForm ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "add = " + this.add + TAB
//	        + "choice = " + this.choice + TAB
	        + "slip = " + this.slip + TAB
	        + "print = " + this.print + TAB
	        + " )";

	    return retValue;
	}

	  /**
     * Submit an investment from the slip.
     *
     * @param id the id of the opp from the slip to submit
     * @param requestAmount the amount to invest
     * @param pageLevel the level that the user see on the page
     * @param pageOddsWin the odds win that the user see on the page
     * @param pageOddsLose the odds lose that the user see on the page
     * @param choice the choice on this opp that the user want to submit
     * @param all if this submit is part of "submit all" request or not (0 - single submit/1 - submit all)
     * @param utcOffset the UTC offset of the client in minutes (as returned from JS Date.getTimezoneOffset())
	 * @param isLowBalanceError
     * @return "OK" if the submit was successful else error message.
     */
    public static String submitOneTouchFromSlip(long id, double requestAmount, double pageLevel, float pageOddsWin, float pageOddsLose, int choice, int all, int utcOffset, long loginID) {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (log.isDebugEnabled()) {
            log.debug("try to submit -" +
                    " id: " + id +
                    " amount: " + requestAmount +
                    " pageLevel: " + pageLevel +
                    " pageOddsWin: " + pageOddsWin +
                    " pageOddsLose: " + pageOddsLose +
                    " choice: " + choice +
                    " all: " + all +
                    " user: " + context.getExternalContext().getRemoteUser() +
                    " sessionId: " + session.getId());
        }

        // for the new login
        // User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        // if (user.getId()==0) {

       	if (!context.getExternalContext().isUserInRole("web")) {
            if (log.isInfoEnabled()) {
                log.info("Login first." +
                        " user: " + context.getExternalContext().getRemoteUser() +
                        " sessionId: " + session.getId());
            }
            // the leading "1" of this message will make sure only 1 popup will show with this message
            return "1" + CommonUtil.getMessage("error.investment.login", null);
        }
       	
       	User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

       	try {
       		UserRegulation userRegulation = new UserRegulation();
           	//AO Regulation      	
       		if (userRegulation.isUserInDepositStep() 
       				&& (userRegulation.isQualified() || userRegulation.getRegulationVersion() == userRegulation.REGULATION_USER_VERSION)) {
       			log.debug("AO Regulation must be submit  Questionnaire user:" + userRegulation);
       			return CommonUtil.getMessage("error.investment.questionnaire", null);
       		} else if (userRegulation.isSuspended() 
       					|| UserRegulationManager.isPepProhibited(userRegulation, user.getSkinId())
       						|| userRegulation.isTresholdBlock()) {
       			if (userRegulation.isSuspendedDueDocuments() || UserRegulationManager.isRestrictedAO(userRegulation, user.getSkinId())) {
       				log.debug("AO Regulationu isSuspendedDueDocuments user:" + userRegulation);
       				return CommonUtil.getMessage("error.investment.user.suspended.docs", null);
       			} else {
       				String supportPhone = userRegulation.getSupportPhone();
       				log.debug("AO Regulationu is Suspended or Restricted or Pep Prohibited user:" + userRegulation);
       				return CommonUtil.getMessage("error.investment.user.suspended", new String [] {supportPhone});
       			}
       		}
       		
            //Check non reg suspend
       		if(!SkinsManagerBase.getSkin(user.getSkinId()).isRegulated() && UsersManagerBase.isNonRegUserSuspend(user.getId())){   				
   				log.debug("User is manual Suspended user:" + user.getId());
   				String supportPhone = userRegulation.getSupportPhone();
   				return CommonUtil.getMessage("error.investment.user.suspended", new String [] {supportPhone});
       		}
       	} catch (SQLException e2) {
       		log.error("Unable to check user regulation status", e2);
       		return CommonUtil.getMessage("error.investment", null);
       	}

       	
        String msg = CommonUtil.getMessage("error.investment", null); // in case we don't find the inv to submit

        double rate = CurrencyRatesManagerBase.getInvestmentsRate(user.getCurrencyId());
        if (rate == 0d) {
        	log.error("submitOneTouchFromSlip: Could not get currency rate - check DB connection pool. Failing investment");
        	return CommonUtil.getMessage("error.investment", null);
        }

        InvestmentRejects invRej = new InvestmentRejects(id,
    														pageLevel,
    														session.getId(),
    														requestAmount * 100,
    														user.getId(),
    														rate,
    														pageOddsWin,
    														null,
    														choice,
    														(int) Opportunity.TYPE_ONE_TOUCH,
    														ConstantsBase.FROM_GRAPH_DEFAULT,
    														Writer.WRITER_ID_WEB);

        long invId = 0;
        SlipEntry e = null;
        try {
			Opportunity opp = InvestmentsManager.getOneTouchOpportunityById(id,
																			context	.getApplication()
																					.evaluateExpressionGet(	context,
																											ConstantsBase.BIND_APPLICATION_DATA,
																											ApplicationData.class)
																					.getUserLocale());
                    e = new SlipEntry(opp, choice);
                    invRej.setWwwLevel(opp.getCurrentLevel());
                    //checking current level with page level
                    if ( pageLevel != opp.getCurrentLevel() ) {
                    	log.log(Level.DEBUG, "Error Validation : current level: "+ opp.getCurrentLevel() +" not equal to page level: "+pageLevel);
                    	msg = CommonUtil.getMessage("error.investment.current.level.change", null);

                        invRej.setRejectTypeId(Constants.INVESTMENT_REJECT_DIFFRENT_LEVEL_ONE_TOUCH);

                        DecimalFormat sd = new DecimalFormat("###,###,##0.000000");
                        invRej.setRejectAdditionalInfo("diffrent level one touch:, crrSpread: " + sd.format(Math.abs(opp.getCurrentLevel() - pageLevel)));


                        InvestmentRejectsManager.insertInvestmentReject(invRej);

                    	return msg;
                    }


                    e.setAmount(requestAmount);
                    if (!InvestmentsManager.isOpportunityDisabled(e.getOpportunity().getId())) {
                    	long s = System.currentTimeMillis();

                    	if (log.isDebugEnabled()) {
            				log.log(Level.DEBUG, "TIME: get current level " + (System.currentTimeMillis() - s));
                        }

                            	s = System.currentTimeMillis();
        	                    String ip = CommonUtil.getIPAddress();
//								rewrite
        	                    msg = InvestmentsManager.validateOneTouchInvestment(user, e, pageOddsWin, pageOddsLose, invRej);
                                if (null == msg) {
                                	long maxAmountAllUsers = e.getOpportunity()
                                								.getSkinGroupMappings()
                                									.get(ApplicationData.getSkinById(user.getSkinId()).getSkinGroup())
                                										.getMaxExposure() * 100L;
                                	long amount = new Float(requestAmount * 100).longValue();
                                	double baseAmount = CommonUtil.convertToBaseAmount(amount, user.getCurrencyId(), new Date(System.currentTimeMillis()));
                                	long allUsersTotalInv = InvestmentsManager.getSumAllActiveInvestmentsByOppId(e.getOpportunity().getId());

                                	if((baseAmount + allUsersTotalInv) > maxAmountAllUsers ) {
                                    	if (log.isInfoEnabled()) {
                                    		log.info("All users total invest limit");
                                    	}
                                    	invRej.setRejectTypeId(Constants.INVESTMENT_REJECT_EXPOSURE);
                                        invRej.setRejectAdditionalInfo("Exposure Reached:, exposure: " + baseAmount + allUsersTotalInv + " maxExposure: " + maxAmountAllUsers);
                                        InvestmentRejectsManager.insertInvestmentReject(invRej);

                                    	sendNotifyMail(user.getUserName(), new Float(requestAmount * 100).longValue() * rate, pageOddsWin, choice, e, Constants.CURRENCY_USD_ID, pageLevel, "exposure", null);
                                    	msg = CommonUtil.getMessage("error.investment.deviation", null);
                                    }

                                    if (null == msg) {
	                                    if (log.isDebugEnabled()) {
	                        				log.log(Level.DEBUG, "TIME: total validation " + (System.currentTimeMillis() - s));
	                                    }

	                                    invId = InvestmentsManager.insertInvestment(user, e, pageLevel, ip, 0 , 0/*realLevel, wwwLevel*/, CommonUtil.formatJSUTCOffsetToString(utcOffset), rate, ConstantsBase.FROM_GRAPH_DEFAULT, e.getOpportunity().getOddsType().getOverOddsWin(), e.getOpportunity().getOddsType().getOverOddsLose(), false, loginID);

                                        if ((baseAmount + allUsersTotalInv) >= opp.getOneTouchOppExposure() && !user.isTestUser()){
                                            InvestmentsManager.updateOneTouchExposureReached(opp);
                                        }

	            	                    e.setInvestmentId(invId);
	                                    e.getOpportunity().setCurrentLevel(pageLevel);
                                    }
                                }
                    } else {
                        if (log.isInfoEnabled()) {
                            log.info("Opportunity disabled." +
                                    " user: " + context.getExternalContext().getRemoteUser() +
                                    " sessionId: " + session.getId());
                        }
                    	invRej.setRejectTypeId(Constants.INVESTMENT_REJECT_OPP_DISABELD);
                        invRej.setRejectAdditionalInfo("Disabled:, opp id: " + id );
                        InvestmentRejectsManager.insertInvestmentReject(invRej);
                        msg = CommonUtil.getMessage("error.investment.disabled", null);
                    }

        } catch (Throwable t) {
            log.error("Failed to insert investment." +
                    " user: " + context.getExternalContext().getRemoteUser() +
                    " sessionId: " + session.getId(), t);
            msg = CommonUtil.getMessage("error.investment", null);
        }

        try {
        	PopulationsManagerBase.invest(user.getId(), null == msg ? true : false, CommonUtil.getWebWriterId(), null, null, new Double(e.getAmount()).longValue(), e.getInvestmentId(), e.getOpportunity().getOpportunityTypeId());
        } catch (Exception ex) {
        	log.warn("Problem with population invest event " + ex);
		}
        try {
        	RiskAlertsManagerBase.riskAlertInvestmentHandler(CommonUtil.getWebWriterId(), user.getId(), invId);
	    } catch (Exception ex) {
	    	log.warn("Problem with Risk alert handler " + ex);
		}

        RewardUserTasksManager.rewardTasksHandler(TaskGroupType.INVESTMENT, user.getId(),  new Float(e.getAmount() * 100).longValue() + e.getOptionPlusFee(), invId, BonusManagerBase.class, Constants.WRITER_WEB_ID, OpportunityType.PRODUCT_TYPE_ONE_TOUCH, e.getOpportunity().getOpportunityTypeId());

        return (null == msg ? "OK" + invId : msg);
    }
    public static void sendNotifyMail(String userName,double convertedAmount,float pageOddsWin,int choice,SlipEntry inv, long currencyId, double pageLevel, String emailType, Double realLevel){
    	FacesContext context = FacesContext.getCurrentInstance();
    	ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
        User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
    	Hashtable serverProperties = new Hashtable();
    	log.info("Sending mail about " + emailType.toUpperCase() + " to group... ");
    	String titleType = (user.getClassId() == ConstantsBase.USER_CLASS_TEST) ? "TEST " : "";
    	if (inv.getOpportunity().getOpportunityTypeId() == OpportunityType.PRODUCT_TYPE_BINARY_0_100) {
    		titleType += "0-100 exposure REACHED";
    	}else {
            titleType += emailType.equalsIgnoreCase("deviation3") ? "D3: " : emailType + " REACHED: ";
    	}
    	String marketName = MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, inv.getOpportunity().getMarketId());
        if (inv.getOpportunity().getOpportunityTypeId() == OpportunityType.PRODUCT_TYPE_OPTION_PLUS) {
        	marketName += " +";
    	}
		serverProperties.put("url", CommonUtil.getProperty("email.server"));
		serverProperties.put("auth", "true");
		serverProperties.put("user", CommonUtil.getProperty("email.uname"));
		serverProperties.put("pass", CommonUtil.getProperty("email.pass"));

		Hashtable emailProperties = new Hashtable();
		//Put params from outside params
		//emailProperties.put("subject", CommonUtil.getProperty("contact.email.subject", null));

		emailProperties.put("from", CommonUtil.getProperty("email.from"));
		emailProperties.put("to",CommonUtil.getProperty(emailType + ".mailgroup"));
		emailProperties.put("subject", titleType + " - " + userName + " - " + ApplicationData.getInvType().get(choice) + " - " + marketName + " - " + ApplicationData.getInvScheduled().get(inv.getOpportunity().getScheduled()));
        DecimalFormat sd = new DecimalFormat("###,###,##0.000000");
		String body= new String();
		if (inv.getOpportunity().getOpportunityTypeId() == OpportunityType.PRODUCT_TYPE_BINARY_0_100 ) {
			body += " user: " + userName + "<br/>";
			body += " market name: " + marketName + "<br/>";
			body += " type: " + ApplicationData.getInvType().get(choice) + "<br/>";
			body += " price: "	+ CommonUtil.displayAmount(convertedAmount, true, currencyId) + "<br/>";
			body += " number of contracts: " + inv.getBinary0100OppCount();
			body += " amount per contract: " + inv.getBinary0100AvgAmount();
		} else {
			body += " user: " + userName + "<br/>";
			body += " market name: " + marketName + "<br/>";
			body += " scheduled: " + ApplicationData.getInvScheduled().get(inv.getOpportunity().getScheduled()) + "<br/>";
			body += " exp time: " + inv.getOpportunity().getTimeEstClosing() + "<br/>";
			body += " type: " + ApplicationData.getInvType().get(choice) + "<br/>";
			body += " amount: "	+ CommonUtil.displayAmount(convertedAmount, true, currencyId) + "<br/>";
			body += " return: " + pageOddsWin + "<br/>";
			body += " level: " + pageLevel + "<br/>";
			if (null != realLevel) { // dev 3 email
				body += " reuters level: " + sd.format(realLevel) + "<br/>";
				double crrSpread = Math.abs(realLevel - pageLevel);
				body += " spread: " + sd.format(crrSpread) + "<br/>";
				body += " acceptable level dev 3: "	+ sd.format(inv.getOpportunity().getMarket().getAcceptableDeviation3().doubleValue()) + "<br/>";
			}
		}
		emailProperties.put("body",body);

		CommonUtil.sendEmail(serverProperties, emailProperties, null);
    }
	public ArrayList getOneTouchList() {
		return OneTouchList;
	}
	public void setOneTouchList(ArrayList oneTouchList) {
		OneTouchList = oneTouchList;
	}


	//TODO: Removed the functions below for the next deploy! (ET suffix)

    /**
     * Duplicated to ET, for not upload classes when upload the graph
     * @return
     * @throws SQLException
     */
    public boolean isHasOpenedProfit() throws Exception {
        FacesContext context = FacesContext.getCurrentInstance();
        Map rm = context.getExternalContext().getRequestMap();
        ArrayList<Investment> l = (ArrayList<Investment>) rm.get("opened_investments_profit");
        if (null == l) {
            loadOpenedInvestmentsProfit();
        }
        l = (ArrayList<Investment>) rm.get("opened_investments_profit");
        if (null != l && l.size() > 0) {
            return true;
        }
        return false;
    }

    /**
     * Duplicated to ET, for not upload classes when upload the graph
     * @return
     * @throws SQLException
     */
    public ArrayList<Investment> getOpenedProfit() throws Exception {
        FacesContext context = FacesContext.getCurrentInstance();
        Map rm = context.getExternalContext().getRequestMap();
        ArrayList<Investment> l = (ArrayList<Investment>) rm.get("opened_investments_profit");
        if (null == l) {
            loadOpenedInvestmentsProfit();
        }
        return (ArrayList<Investment>) rm.get("opened_investments_profit");
    }

    private void loadOpenedInvestmentsProfit() throws Exception {
        FacesContext context = FacesContext.getCurrentInstance();
        User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
       	context.getExternalContext().getRequestMap().put("opened_investments_profit", InvestmentsManager.getTodaysOpenedInvestmentsProfit(user.getId()));
    }

    public long getTimeToFirstOpenedExpireProfit() throws Exception {
        FacesContext context = FacesContext.getCurrentInstance();
        Map rm = context.getExternalContext().getRequestMap();
        ArrayList<Investment> l = (ArrayList<Investment>) rm.get("opened_investments_profit");
        if (null == l) {
            loadOpenedInvestmentsProfit();
        }
        l = (ArrayList<Investment>) rm.get("opened_investments_profit");
        if (null != l && l.size() > 0) {
            Investment i = l.get(0);
            int advance = 1;
            if (i.getMarketId() == 14 ||
                    i.getMarketId() == 15 ||
                    i.getMarketId() == 16 ||
                    i.getMarketId() == 17 ||
                    i.getMarketId() == 18 ||
                    i.getMarketId() == 26) {
                advance = 65;
            }
            long secLeft = (i.getTimeEstClosing().getTime() - System.currentTimeMillis()) / 1000;
            if (secLeft > 3600) { //if there is more then an hour
            	secLeft -= 3600; //refresh when its an hour
            }
            return secLeft + advance;
        }
        return 10;
    }

    /**
     * @return the unitsSelect
     */
    public ArrayList<SelectItem> getUnitsSelect() {
        return unitsSelect;
    }

    /**
     * @param unitsSelect the unitsSelect to set
     */
    public void setUnitsSelect() {
        SelectItem itm = new SelectItem();
        unitsSelect = new ArrayList<SelectItem>();
        for (int i = 1; i <= 5; i++) {
            itm = new SelectItem(i+"",i+"");
            unitsSelect.add(itm);
        }
        if (il.getMaxAmount() > 500) {
            for (int i = 10; i <= maxInv1Tunits; i += 5) {
                itm = new SelectItem(i+"",i+"");
                unitsSelect.add(itm);
            }
        }
    }


    /**
     * @return the etUnitsSelect
     * @throws
     * @throws Exception
     */
    public ArrayList<SelectItem> getEtUnitsSelect() throws Exception {
        FacesContext context = FacesContext.getCurrentInstance();
        ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
        int oneTouchUnitPriceET = ConstantsBase.ONE_TOUCH_ET_UNIT_PRICE;//each unit in 1T ET = 100 ils
        long userId = 0;
        if (context.getExternalContext().isUserInRole("web")) {
            User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
            userId = user.getId();
        }
        il = InvestmentsManager.getInvestmentLimit(Opportunity.TYPE_ONE_TOUCH, 0, 0, ap.getRealCurrencyId(), userId);
        long maxUnits = il.getMaxAmount()/ 100 / oneTouchUnitPriceET;
        if (maxInv1Tunits != maxUnits){
            maxInv1Tunits = maxUnits;
            setUnitsSelect();
        }

        return getUnitsSelect();
    }

    /**
     * @param etUnitsSelect the etUnitsSelect to set
     */
    public void setEtUnitsSelect(ArrayList<SelectItem> etUnitsSelect) {
        this.etUnitsSelect = etUnitsSelect;
    }

    public String getMaxInvAmount() throws Exception {  //for 1T general terms
        FacesContext context = FacesContext.getCurrentInstance();
        ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
        InvestmentLimit il = InvestmentsManager.getInvestmentLimit(Opportunity.TYPE_ONE_TOUCH, 0, 0, ap.getRealCurrencyId(), 0);
        long maxInvAmount = il.getMaxAmount() / 100;
        return String.valueOf(maxInvAmount);
    }

    public String getUpdateMarketId(long marketId) {
    	this.marketId = marketId;
    	return "";
    }

	/**
	 * @param hasOneTouchFacebookPlus the hasOneTouchFacebookPlus to set
	 */
	public void setHasOneTouchFacebookPlus(boolean hasOneTouchFacebookPlus) {
		this.hasOneTouchFacebookPlus = hasOneTouchFacebookPlus;
	}

	public static String submitBinary0100(long oppId, long contractType, double quote, int count, int utcOffset, double currentLevel, long loginID) throws SQLException {
	    /*
	     * Icon codes:
	     * 00 - V (success)
	     * 01 - ! (exclamation)
	     * 02 - X (error)
	     * 03 -   (empty)
	     *
	     * Error codes:
	     * 00 - success
	     * 01 - binary0100.mainarea.error.logged.out
	     * 02 - binary0100.mainarea.transaction.failure
	     * 03 - binary0100.mainarea.potential.profit
	     * 04 - binary0100.asset.disabled.dont.have.opp
	     * 05 - binary0100.asset.disabled.have.opp
	     * 06 - binary0100.mainarea.sufficient.funds
	     * 07 - error.investment.questionnaire
	     * 08 - error.investment.user.suspended
	     * 09 - error.investment.user.suspended.docs
	     */
		FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        String userName = context.getExternalContext().getRemoteUser();
        String sessionId = session.getId();
        if (userName != null) {
        	userName = userName.toUpperCase();
        }
        if (log.isDebugEnabled()) {
            log.debug("try to submit -" +
                    " oppId: " + oppId +
                    " contractType: " + contractType +
                    " quote: " + quote +
                    " count: " + count +
                    " utcOffset: " + utcOffset);
        }

       	if (!context.getExternalContext().isUserInRole("web")) {
            if (log.isInfoEnabled()) {
                log.info("Login first." +
                        " user: " + userName +
                        " sessionId: " + sessionId);
            }
            // the leading "1" of this message will make sure only 1 popup will show with this message
            return "0301" + CommonUtil.getMessage("binary0100.mainarea.error.logged.out", null);
        }
       	
       	User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

       	try {
       		UserRegulation userRegulation = new UserRegulation();
       		
       		//AO Regulation
       		if (userRegulation.isUserInDepositStep() 
       				&& (userRegulation.isQualified() || userRegulation.getRegulationVersion() == userRegulation.REGULATION_USER_VERSION)) {
       			log.debug("AO Regulation must be submit  Questionnaire user:" + userRegulation);
       			return "0307" + CommonUtil.getMessage("error.investment.questionnaire", null);       			
       		} else if (userRegulation.isSuspended() 
       					|| UserRegulationManager.isRestrictedAO(userRegulation, user.getSkinId())
       						|| UserRegulationManager.isPepProhibited(userRegulation, user.getSkinId())
       							|| userRegulation.isTresholdBlock()){
       			if(userRegulation.isSuspendedDueDocuments()) {
       				log.debug("AO Regulationu isSuspendedDueDocuments user:" + userRegulation);
       				return "0309" + CommonUtil.getMessage("error.investment.user.suspended.docs", null);
       			} else {
       				String supportPhone = userRegulation.getSupportPhone();
       				log.debug("AO Regulation is Suspended or Restricted or Pep Prohibited user:" + userRegulation);
       				return "0308" + CommonUtil.getMessage("error.investment.user.suspended",
							new String [] {supportPhone});
       			}
       		}
       		
            //Check non reg suspend
       		if(!SkinsManagerBase.getSkin(user.getSkinId()).isRegulated() && UsersManagerBase.isNonRegUserSuspend(user.getId())){
   				String supportPhone = userRegulation.getSupportPhone();
   				log.debug("User is manual Suspended user:" + user.getId());
   				return "0308" + CommonUtil.getMessage("error.investment.user.suspended",
						new String [] {supportPhone});
       		}           	
       	} catch (SQLException e2) {
       		log.error("Unable to check user regulation status", e2);
       		return "0302" + CommonUtil.getMessage("error.investment", null);
       	}       	

       	Opportunity opp = null;
       	try {
       		opp = InvestmentsManager.getRunningOpportunityById(oppId);
       		if(opp.getQuoteParams() != null) {
//       			opp.setBinaryZeroOneHundred(new BinaryZeroOneHundred(opp.getQuoteParams()));
       		}
       	} catch (SQLException e) {
			log.debug("cant load opp: " + oppId, e);
			return "0302" + CommonUtil.getMessage("binary0100.mainarea.transaction.failure", null);
		}       	

       	// validate that contracts count match one of the valid steps
       	int contractsStep = 0;
       	try {
       	    contractsStep = Investment.BINARY0100_CONTRACTS_STEPS[user.getCurrencyId().intValue()];
       	} catch (Exception e) {
       	    log.error("No contracts steps for this currency", e);
       	    return "0302" + CommonUtil.getMessage("binary0100.mainarea.transaction.failure", null);
       	}
        if (count <= 0 || count % contractsStep != 0 || count / contractsStep > Investment.BINARY0100_CONTRACTS_STEPS_COUNT) {
            log.warn("Attempt to open/close " + count + " contracts.");
            return "0302" + CommonUtil.getMessage("binary0100.mainarea.transaction.failure", null);
        }

        int steps = count / contractsStep;

        double rate = CurrencyRatesManagerBase.getInvestmentsRate(user.getCurrencyId());
        if (rate == 0d) {
        	log.error("Binary0100: Could not get currency rate - check DB connection pool. Failing investment");
        	return "0302" + CommonUtil.getMessage("binary0100.mainarea.transaction.failure", null);
        }

        WebLevelsCache levelsCache = (WebLevelsCache) context.getExternalContext().getApplicationMap().get("levelsCache");
        int needToOpen = steps;
        int needToClose = 0;
        ArrayList<Investment> openInvsToClose = null;
       	try {
			openInvsToClose = InvestmentsManager.getOpenBinary0100Investments(user.getId(), opp.getId(), contractType, null);
			if (openInvsToClose.size() > 0) {
				if (openInvsToClose.size() > steps) {
					needToClose = steps;
				} else {
					needToClose = openInvsToClose.size();
				}
				needToOpen = steps - needToClose;
			}
		} catch (SQLException e) {
			log.error("cant get open investment to close", e);
			return "0302" + CommonUtil.getMessage("binary0100.mainarea.transaction.failure", null);
		}

		double amountForClose = 0;
		double amountForOne = 0;
		if (contractType == InvestmentType.BUY) {
		    amountForOne = quote;
			amountForClose = (100 - quote) * contractsStep;
		} else { //sell
		    amountForOne = (100 - quote);
			amountForClose = quote * contractsStep;
		}
        double amount = amountForOne * contractsStep;

		long amountDB = new Float(amount * 100).longValue();

		log.debug("contractsStep: " + contractsStep + " steps: " + steps + " needToClose: " + needToClose + " needToOpen: " + needToOpen
		        + " amountForClose: " + amountForClose + " amountForOne: " + amountForOne + " amount: " + amount + " openInvsToClose: " + (null != openInvsToClose ? openInvsToClose.size() : "null"));

		InvestmentRejects invRej = new InvestmentRejects(oppId,
															currentLevel,
															sessionId,
															(double) amountDB,
															user.getId(),
															rate,
															(float) amountForClose,
															null,
															(int) contractType,
															(int) opp.getOpportunityTypeId(),
															ConstantsBase.FROM_GRAPH_FLASH,
															needToOpen,
															needToClose,
															Writer.WRITER_ID_WEB);

		if ((contractType == InvestmentType.BUY && quote == 100) || (contractType == InvestmentType.SELL && quote == 0)) {
			if (log.isInfoEnabled()) {
                log.info("Potential profit is 0" +
                        " user: " + context.getExternalContext().getRemoteUser() +
                        " sessionId: " + session.getId() +
                        "contractType: " + contractType +
                        "quote: " + quote);
			}
			return "0303" + CommonUtil.getMessage("binary0100.mainarea.potential.profit", null);
		}

		if (opp.isDisabled()) {
			if (log.isInfoEnabled()) {
                log.info("Opportunity disabled." +
                        " user: " + context.getExternalContext().getRemoteUser() +
                        " sessionId: " + session.getId());
			}
			invRej.setRejectTypeId(Constants.INVESTMENT_REJECT_OPP_DISABELD);
            invRej.setRejectAdditionalInfo("Disabled:, opp id: " + oppId );
            InvestmentRejectsManager.insertInvestmentReject100(invRej);
			return "0104" + CommonUtil.getMessage("binary0100.asset.disabled.dont.have.opp", null);
		}

		if (opp.getMarket().isSuspended()) {
			if (log.isInfoEnabled()) {
                log.info("Opportunity suspended." +
                        " user: " + context.getExternalContext().getRemoteUser() +
                        " sessionId: " + session.getId());
			}
			return "0302" + CommonUtil.getMessage("binary0100.mainarea.transaction.failure", null);
		}

		int openedInvestments;
		// This is done because the method is ignoring the given investment type
		if (contractType == InvestmentType.BUY) {
			openedInvestments = InvestmentsManager	.getOpenBinary0100Investments(user.getId(), opp.getId(), InvestmentType.SELL, null)
													.size();
		} else {
			openedInvestments = InvestmentsManager.getOpenBinary0100Investments(user.getId(), opp.getId(), InvestmentType.BUY, null).size();
		}
		if (needToOpen + openedInvestments > opp.getMaxInvAmountCoeffPerUser() * Investment.BINARY0100_CONTRACTS_STEPS_COUNT) {
			log.debug("Max investments amount reached, userId: "+ user.getId() + ", active investments: " + openedInvestments
						+ ", need to open more: " + needToOpen + ", limit: "
						+ opp.getMaxInvAmountCoeffPerUser() * Investment.BINARY0100_CONTRACTS_STEPS_COUNT);
			int rejetcTypeId = ConstantsBase.INVESTMENT_REJECT_INV_SUM_LIMIT;
			invRej.setRejectTypeId(rejetcTypeId);
			InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
			return "0302"
					+ CommonUtil.getMessage("error.investment.sum.limit",
											new String[] {	MarketsManagerBase.getMarketName(user.getSkinId(), opp.getMarketId()),
															CommonUtil.getTimeFormat(opp.getTimeEstClosing(), CommonUtil.getUtcOffset())});
		}

		try {
			// 0100 fee
			long fee = 0;
			if(user.getSkinId() != Skin.SKIN_ES_US){
				fee = FeesManagerBase.getFee(FeesManagerBase.ZERO_ONE_HUNDRED_COMMISSION_FEE, user.getCurrencyId());
			}

			long amountDBTotal = amountDB * needToOpen;
			String reply = InvestmentsManager.validateUsersBalance(user, amountDBTotal + fee * count, opp);
			if (null != reply) { //user balance problem
				return "0106" + CommonUtil.getMessage("binary0100.mainarea.sufficient.funds", null);
			}

            WebLevelLookupBean wllb = new WebLevelLookupBean();
            wllb.setOppId(oppId);
            wllb.setFeedName(opp.getMarket().getFeedName());
            wllb.setSkinGroup(ApplicationData.getSkinById(user.getSkinId()).getSkinGroup());
            levelsCache.getLevels(wllb);
            reply = deviationCheckBinary0100(wllb, quote, contractType, opp.getMarket().getAcceptableDeviation().doubleValue(), opp.getTypeId(), invRej);
			if (null != reply) {
				return "0302" + CommonUtil.getMessage("binary0100.mainarea.transaction.failure", null);
			}

			//check if we pass the cThreashold
			if (wllb.isAutoDisable() && needToOpen > 0) {
				log.warn("Trading halt " + wllb.getInvestmentsRejectInfo());
				invRej.setRejectTypeId(Constants.INVESTMENT_REJECT_EXPOSURE_0_100);
                invRej.setRejectAdditionalInfo("Trading halt:, " + wllb.getInvestmentsRejectInfo());
                InvestmentRejectsManager.insertInvestmentReject100(invRej);
                SlipEntry e  = new SlipEntry(InvestmentsManager.getByIdForInvestmentValidation(oppId),(int) contractType);
                e.setBinary0100OppCount(count);
                e.setBinary0100AvgAmount(amountForOne);
                sendNotifyMail(user.getUserName(), amount, new Float(0), (int)contractType, e, user.getCurrencyId(), 0, "exposure", null);
                boolean hasOpenInv = false;
                try {
                	hasOpenInv = InvestmentsManager.isHasOpenedInvByOpp(user.getId(), oppId);
                } catch (Throwable t) {
                	log.debug("cant check if have open inv: user id " + user.getId() + " oppid " + oppId);
                }
				if (!hasOpenInv) {
					return "0104" + CommonUtil.getMessage("binary0100.asset.disabled.dont.have.opp", null);
				}
				return "0105" + CommonUtil.getMessage("binary0100.asset.disabled.have.opp", null);
			}

			if (needToOpen > 0 && UsersManager.isUserMarketDisabled(user.getId(), opp.getMarketId(), opp.getScheduled(), false)) {
                log.warn("User Market Disabled...   user id = " + user.getId() + " market id = " + opp.getMarketId() + " scheduled = " + opp.getScheduled());
                invRej.setRejectTypeId(Constants.INVESTMENT_REJECT_USER_MARKET_DISABELD);
                invRej.setRejectAdditionalInfo("User Market Disabled:");
                InvestmentRejectsManager.insertInvestmentReject100(invRej);
                return "0302" + CommonUtil.getMessage("binary0100.mainarea.transaction.failure", null);
            }

			if (!opp.isPublished()) {
				if (log.isDebugEnabled()) {
                    log.debug("Opportunity not published.");
                }
                return "0302" + CommonUtil.getMessage("binary0100.asset.suspended", null);
			}
			if (needToOpen > 0 && System.currentTimeMillis() < opp.getTimeFirstInvest().getTime()) {
                if (log.isDebugEnabled()) {
                    log.debug("Opportunity not opened.");
                }
                return "0302" + CommonUtil.getMessage("binary0100.mainarea.transaction.failure", null);
            }
            if (System.currentTimeMillis() > opp.getTimeLastInvest().getTime()) {
                if (log.isDebugEnabled()) {
                    log.debug("Opportunity expired.");
                }
                return "0302" + CommonUtil.getMessage("binary0100.mainarea.transaction.failure", null);
            }

            reply = InvestmentsManager.validateUsersBalance(user, amountDBTotal + fee * count, opp);
			if (null != reply) { //user balance problem
				return "0106" + CommonUtil.getMessage("binary0100.mainarea.sufficient.funds", null);
			}
			// 0100 fee
			CloseContractsSums total = InvestmentsManager.openCloseBinary0100Investments(needToClose, openInvsToClose, amountForClose, user, oppId,
															contractType, amountDB, needToOpen, amountForOne, wllb, utcOffset, rate, opp, fee, contractsStep, loginID);
			session.setAttribute(Constants.SESSION_0100_NEED_FEE, needToOpen);
			// send amount WITHOUT fee
            if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
            	if (needToClose > 0) {
                	CommonUtil.sendNotifyForInvestment(
	    							opp.getId(),
	    							(-1d) * (needToClose * contractsStep * rate),
	    							(-1d) * total.getTotalAmount(),
	    							-1L, // not needed if negative
	    							contractType == InvestmentType.BUY ? InvestmentType.SELL : InvestmentType.BUY,
									opp.getMarket().getId(), OpportunityType.PRODUCT_TYPE_BINARY_0_100,
									currentLevel,
									levelsCache,
									user,
									total.getTotalAbove(),
									total.getTotalBelow(),
									CopyOpInvTypeEnum.SELF);
            	}
            	if (needToOpen > 0) {
            		log.debug("opp time est close " + opp.getTimeEstClosing());
            		Calendar sessionDate = (Calendar) session.getAttribute(Constants.LAST_OPEN_INV_0100);
            		Calendar oppDate = Calendar.getInstance();
            		oppDate.setTime(opp.getTimeEstClosing());
            		if (null == sessionDate || sessionDate.before(oppDate)) {
            			log.debug("SessionDate time est close " + (null == sessionDate ? "null" : sessionDate.getTime()));
            			session.setAttribute(Constants.LAST_OPEN_INV_0100, oppDate);
            		}
            		double aboveTotal = 0;
            		double belowTotal = 0;

            		if (opp.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_ABOVE) {
            			if (contractType == InvestmentType.BUY) {
            				aboveTotal = (-1L * ((100 - amountForOne) * (contractsStep * needToOpen) * rate) * 100);
            				belowTotal = (amountDBTotal * rate);
            			} else if (contractType == InvestmentType.SELL) {
            				aboveTotal = (amountDBTotal * rate);
            				belowTotal = (-1L * ((100 - amountForOne) * (contractsStep * needToOpen) * rate) * 100);
            			}
            		} else { //below
            			if (contractType == InvestmentType.BUY) {
            				aboveTotal = (amountDBTotal * rate);
            				belowTotal = (-1L * ((100 - amountForOne) * (contractsStep * needToOpen) * rate) * 100);
            			} else if (contractType == InvestmentType.SELL) {
            				aboveTotal = (-1L * ((100 - amountForOne) * (contractsStep * needToOpen) * rate) * 100);
            				belowTotal = (amountDBTotal * rate);
            			}
            		}
            		CommonUtil.sendNotifyForInvestment(
            						oppId,
            						needToOpen * contractsStep * rate,
            						amountDBTotal * rate,
            						amountDBTotal,
            						contractType,
            						opp.getMarket().getId(),
            						OpportunityType.PRODUCT_TYPE_BINARY_0_100,
            						currentLevel,
            						levelsCache,
            						user,
            						aboveTotal,
            						belowTotal,
									CopyOpInvTypeEnum.SELF);
            	}
            }
            double price = quote;
	        if (contractType == InvestmentType.BUY) {
	        	price = 100 - quote;
	        }
	        price = price * needToClose * 100 * contractsStep;
            if ((needToOpen == 0 || (needToClose > 0 && price > amountDBTotal)) && price > 0) { //we only close contracts or we add amount to user balance and price > 0
				String[] params = new String[1];
				params[0] = CommonUtil.displayAmount(price, true, user.getCurrencyId());
				if (user.getCurrencyId() == Constants.CURRENCY_RUB_ID && !CommonUtil.isParameterEmptyOrNull(params[0])) {
					params[0] = CommonUtil.removeFractionFromAmount(params[0]);
				}
				return "0000" + CommonUtil.getMessage("binary0100.mainarea.transaction.Success", params);
			}
            return "0000";
		} catch (Exception e) {
            log.error("Failed to insert investment." +
                    " user: " + userName +
                    " sessionId: " + sessionId, e);
            return "0302" + CommonUtil.getMessage("binary0100.mainarea.transaction.failure", null);
        }
	}

	/**
	 * validations for closing contracts
	 * @param openInvsToClose
	 * @param needToClose
	 * @param opp
	 * @param user
	 * @param contractType
	 * @param quote
	 * @param rate
	 * @param levelsCache
	 * @param currentLevel
	 * @return
	 * @throws SQLException
	 */
	public static String closeContracts(ArrayList<com.anyoption.common.beans.Investment> openInvsToClose, int needToClose, Opportunity opp, User user, long contractType, double quote, double rate, WebLevelsCache levelsCache, double currentLevel, int step) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);

        /*
         * See submitBinary0100 for the list of error codes.
         */
		double amount = quote * step;
        long notifyContractType = InvestmentType.BUY;
        if (contractType == InvestmentType.BUY) {
        	notifyContractType = InvestmentType.SELL;
        	amount = (100 - quote) * step;
        }

        long amountDB = new Float(amount * 100).longValue();

        if ((contractType == InvestmentType.BUY && quote == 100) || (contractType == InvestmentType.SELL && quote == 0)) {
			if (log.isInfoEnabled()) {
                log.info("Potential profit is 0" +
                        " user: " + context.getExternalContext().getRemoteUser() +
                        " sessionId: " + session.getId() +
                        "contractType: " + contractType +
                        "quote: " + quote);
			}
			return "0303" + CommonUtil.getMessage("binary0100.mainarea.potential.profit", null);
		}

		InvestmentRejects invRej = new InvestmentRejects(opp.getId(),
															currentLevel,
															session.getId(),
															(double) amountDB,
															user.getId(),
															rate,
															(float) amount,
															null,
															(int) contractType,
															(int) opp.getOpportunityTypeId(),
															ConstantsBase.FROM_GRAPH_FLASH,
															0,
															needToClose,
															Writer.WRITER_ID_WEB);

		if (opp.isDisabled()) {
			if (log.isInfoEnabled()) {
                log.info("Opportunity disabled = " + opp.isDisabled());
			}
			invRej.setRejectTypeId(Constants.INVESTMENT_REJECT_OPP_DISABELD);
            invRej.setRejectAdditionalInfo("Disabled:, opp id: " + opp.getId() );
            InvestmentRejectsManager.insertInvestmentReject100(invRej);
			return "0104" + CommonUtil.getMessage("binary0100.asset.disabled.dont.have.opp", null);
		}

		if (opp.getMarket().isSuspended()) {
			if (log.isInfoEnabled()) {
                log.info("Opportunity suspended = " + opp.getMarket().isSuspended());
			}
			return "0202" + CommonUtil.getMessage("binary0100.mainarea.transaction.failure", null);
		}

		WebLevelLookupBean wllb = new WebLevelLookupBean();
        wllb.setOppId(opp.getId());
        wllb.setFeedName(opp.getMarket().getFeedName());
        wllb.setSkinGroup(ApplicationData.getSkinById(user.getSkinId()).getSkinGroup());
        levelsCache.getLevels(wllb);
		String reply = deviationCheckBinary0100(wllb, quote, contractType, opp.getMarket().getAcceptableDeviation().doubleValue(), opp.getTypeId(), invRej);
		if (null != reply) {
			return "0202" + CommonUtil.getMessage("binary0100.mainarea.transaction.failure", null);
		}

        if (System.currentTimeMillis() > opp.getTimeLastInvest().getTime()) {
            if (log.isDebugEnabled()) {
                log.debug("Opportunity expired.");
            }
            return "0202" + CommonUtil.getMessage("binary0100.mainarea.transaction.failure", null);
        }


		CloseContractsSums total = InvestmentsManager.settleBinary0100Investments(needToClose, openInvsToClose, amount, wllb.getDevCheckLevel(), opp);
        if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
        	CommonUtil.sendNotifyForInvestment(
			    					opp.getId(),
			    					(-1d) * (needToClose * step * rate),
			    					(-1d) * total.getTotalAmount(),
			    					-1L, // not needed if negative
			    					notifyContractType,
			    					opp.getMarket().getId(),
			    					OpportunityType.PRODUCT_TYPE_BINARY_0_100,
			    					currentLevel,
			    					levelsCache,
			    					user,
			    					total.getTotalAbove(),
			    					total.getTotalBelow(),
									CopyOpInvTypeEnum.SELF);
        }
        return null;
	}

	private static String deviationCheckBinary0100(WebLevelLookupBean wllb, double quote, long contractType, double tickDeviation, long oppType, InvestmentRejects invRej) {
		if (wllb.getBid() != -1 && wllb.getOffer() != -1) { // check if we have levels (else we have conn prob)
			double checkLevel = contractType == InvestmentType.SELL ? wllb.getBid() : wllb.getOffer();
    //        BigDecimal deviation = new BigDecimal(String.valueOf(tickDeviation));
            double crrSpread = Math.abs(checkLevel - quote);
            double acceptableSpread = tickDeviation;
            if (log.isDebugEnabled()) {
                log.debug("contractType: " + contractType +
                        " checkLevel: " + checkLevel +
                        " quote: " + quote +
                        " crr spread: " + crrSpread +
                        " deviation: " + tickDeviation +
                        " acceptable spread: " + acceptableSpread);
            }
            if ((crrSpread <= acceptableSpread) /*||
                    (checkLevel < quote && contractType == InvestmentType.BUY && oppType == Opportunity.TYPE_BINARY_0_100_ABOVE) ||
                    (checkLevel > quote && contractType == InvestmentType.BUY && oppType == Opportunity.TYPE_BINARY_0_100_BELOW) ||
                    (checkLevel > quote && contractType == InvestmentType.SELL && oppType == Opportunity.TYPE_BINARY_0_100_ABOVE) ||
                    (checkLevel < quote && contractType == InvestmentType.SELL && oppType == Opportunity.TYPE_BINARY_0_100_BELOW)*/) {
/*
            	if (log.isInfoEnabled() && crrSpread > acceptableSpread) {
                	if (checkLevel < quote && contractType == InvestmentType.BUY && oppType == Opportunity.TYPE_BINARY_0_100_ABOVE) {
                		log.info("Skip calculating deviation. BUY on higher level opp type above.");
                	} else if (checkLevel > quote && contractType == InvestmentType.BUY && oppType == Opportunity.TYPE_BINARY_0_100_BELOW) {
                		log.info("Skip calculating deviation. BUY on lower level opp type below.");
                	} else if (checkLevel > quote && contractType == InvestmentType.SELL && oppType == Opportunity.TYPE_BINARY_0_100_ABOVE) {
                		log.info("Skip calculating deviation. SELL on lower level opp type above.");
                	} else if (checkLevel < quote && contractType == InvestmentType.SELL && oppType == Opportunity.TYPE_BINARY_0_100_BELOW) {
                		log.info("Skip calculating deviation. SELL on higher level opp type below.");
                	}
                }
*/
                return null;
            } else {
                log.warn("Unacceptable deviation binary 0-100.");
                String msgStart = "Unacceptable Deviation 0-100";
                invRej.setRejectTypeId(Constants.INVESTMENT_REJECT_DEVIATION_0_100);
                DecimalFormat sd = new DecimalFormat("###,###,##0.000000");
                invRej.setRejectAdditionalInfo(msgStart + ":, crrSpread: " + sd.format(crrSpread) + " , deviation: " + tickDeviation + " , acceptableSpread: " + sd.format(acceptableSpread) + " , checkLevel: " + checkLevel + " , quote: " + quote);
                invRej.setRealLevel(wllb.getRealLevel());
                invRej.setWwwLevel(wllb.getDevCheckLevel());
                InvestmentRejectsManager.insertInvestmentReject100(invRej);
                return CommonUtil.getMessage("error.investment.deviation", null);
            }
        } else {
            log.warn("No www level or real level.");
        }
        return CommonUtil.getMessage("error.investment", null);
	}

	public static float getWinOdds0100(double amount, int count) {
		BigDecimal profit = new BigDecimal(String.valueOf(amount)).multiply(new BigDecimal(String.valueOf(count)));
		BigDecimal maxReturn =  new BigDecimal("100").multiply(new BigDecimal(String.valueOf(count)));
		if (maxReturn.subtract(profit).doubleValue() == 0) {
			return 0;
		}
		BigDecimal oddsWin = new BigDecimal(String.valueOf(maxReturn.doubleValue() / (maxReturn.subtract(profit).doubleValue())));
		return oddsWin.setScale(2, RoundingMode.HALF_UP).floatValue();
	}

	public String getCurrentLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCurrentLevelTxt(i);
	}

	public String getAmountTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getAmountTxt(i);
	}

	public String getSumInvestWithText(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.getMessage("right.nav.investments.sum.invest", null) + CommonUtil.displayAmount(i.getSumInvest(), i.getCurrencyId());
	}
}