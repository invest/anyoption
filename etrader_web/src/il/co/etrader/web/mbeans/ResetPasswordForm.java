package il.co.etrader.web.mbeans;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import il.co.etrader.bl_managers.ResetPasswordManager;
import il.co.etrader.bl_vos.ResetPassword;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.UsersManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;

public class ResetPasswordForm  implements Serializable{

	private static final long serialVersionUID = -5386375405132264387L;
	private String userName;
    private String password;
    private String rePassword;
    private ResetPassword rp;
    private boolean showFields;

	public ResetPasswordForm() throws SQLException, IOException {
		showFields = true;
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
		String resKeyParam = req.getParameter(Constants.RESET_KEY);
		if (null == resKeyParam) {
			// no reset param
			addValidationMessage(context);
			showFields = false;
			return;
		}
		rp = ResetPasswordManager.get(resKeyParam, true);
		if (null == rp) {
			// the key is not in DB
			addValidationMessage(context);
			showFields = false;
			return;
		}
		if (rp.getClicksNum() > 1) {
			addValidationMessage(context);
			showFields = false;
			return;
		}
		GregorianCalendar yesterday = new GregorianCalendar();
		yesterday.setTime(new Date());
		yesterday.add(GregorianCalendar.DAY_OF_MONTH, -1);
		if (rp.getTimeCreated().before(yesterday.getTime())) {
			addValidationMessage(context);
			showFields = false;
			return;
		}
		//req.getSession().setAttribute(Constants.SKIN_ID, String.valueOf(rp.getSkinId()));
		userName = rp.getUserName();
	}

	public void addValidationMessage(FacesContext context) {
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
				CommonUtil.getMessage("password.reset.header.validation", null), null);
		context.addMessage(null, fm);

	}

    public String changePassword() throws Exception {
    	User user = new User();
    	UsersManager.getByUserId(rp.getUserId(), user);
    	showFields = true;
    	if (UsersManager.changePass(null, password, user, true)) {
			LoginForm loginForm = new LoginForm();
			loginForm.setUsername(user.getUserName());
			loginForm.setPassword(user.getPassword());
			loginForm.login();
			showFields = false;
    	}
    	
    	return null;
    }

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "resetPasswordForm ( "
	        + super.toString() + TAB
	        + "userName = " + this.userName + TAB
	        + "newPassword = " + "*****" + TAB
	        + "rePassword = " + "*****" + TAB
	        + " )";

	    return retValue;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String newPassword) {
		this.password = newPassword;
	}

	public String getRePassword() {
		return rePassword;
	}

	public void setRePassword(String rePassword) {
		this.rePassword = rePassword;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmptyString() {
		return "";
	}

	public boolean isShowFields() {
		return showFields;
	}

	public void setShowFields(boolean invalidLink) {
		this.showFields = invalidLink;
	}
}
