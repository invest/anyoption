/**
 * 
 */
package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.QuestionnaireGroup;
import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.QuestionnaireManagerBase;
import com.anyoption.common.managers.UserRegulationManager;

import il.co.etrader.bl_vos.Writer;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.bl_vos.UserRegulation;
import il.co.etrader.web.util.Constants;

public class RegCapQuestionnaireForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5143591023789452915L;
	/**
	 * 
	 */
	private static final Logger log = Logger.getLogger(RegCapQuestionnaireForm.class);
	private QuestionnaireGroup questionnaire;
	private List<QuestionnaireUserAnswer> userAnswers;
	private User user;
	private boolean submitted;
	
	
	public RegCapQuestionnaireForm() throws Exception{
		UserRegulation userRegulation = new UserRegulation();
		if(userRegulation.getApprovedRegulationStep() >= userRegulation.ET_REGULATION_KNOWLEDGE_QUESTION_USER){
			questionnaire = QuestionnaireManagerBase.getAllQuestionnaires().get(QuestionnaireGroup.REGULATION_CAPITAL_QUESTIONNAIRE_NAME);
			user = ApplicationData.getUserFromSession();		
			userAnswers = QuestionnaireManagerBase.getUserAnswers(user.getId(), questionnaire.getQuestions(), Writer.WRITER_ID_WEB);
			if(userRegulation.getApprovedRegulationStep() == userRegulation.ET_CAPITAL_MARKET_QUESTIONNAIRE){
				submitted = true;
			}
		}
	}

	public String insertQuestionnaire() throws Exception {
		log.info("Inserting questionnaire [" + questionnaire.getId() + ":" + questionnaire.getName()
				+ "]  for user [" + user.getId() + "]");
		

		UserRegulation userRegulation = new UserRegulation();
		if (userRegulation.getApprovedRegulationStep() == UserRegulationBase.ET_REGULATION_KNOWLEDGE_QUESTION_USER
				&& !userRegulation.isSuspended()) {
			for (QuestionnaireUserAnswer userAnswer : userAnswers) {
				userAnswer.setWriterId(Writer.WRITER_ID_WEB);
				userAnswer.setScore(QuestionnaireManagerBase.getAnswertScore(questionnaire, userAnswer));
			}
			QuestionnaireManagerBase.updateUserAnswers(userAnswers, true);
			
			if(canSubmit()){
				long scoreGroup = QuestionnaireManagerBase.updateUserAnswersScoreGroup(user.getId());
				
				userRegulation.setApprovedRegulationStep(UserRegulationBase.ET_CAPITAL_MARKET_QUESTIONNAIRE);
				userRegulation.setWriterId(Writer.WRITER_ID_WEB);
				UserRegulationManager.updateRegulationStep(userRegulation);
				
				if(scoreGroup == UserRegulationBase.ET_REGULATION_HIGH_SCORE_GROUP_ID){
					userRegulation.setApprovedRegulationStep(UserRegulationBase.ET_REGULATION_DONE);
					userRegulation.setWriterId(Writer.WRITER_ID_WEB);
					UserRegulationManager.updateRegulationStep(userRegulation);					
				}
				
				if(scoreGroup == UserRegulationBase.ET_REGULATION_LOW_SCORE_GROUP_ID){
    				if(il.co.etrader.bl_managers.UsersManagerBase.sendActivationMailForSuspend(user.getId(), false)){
    					log.debug("Send activation mail after LOW GROUP Score userId" + user.getId());
    				} else{
    					log.error("Can't send Activation Mail For Suspend userId:" + user.getId());    					
    				}
					return Constants.NAV_USER_CAP_QUESTIONNAIRE;
				}
			} else {
				log.error("CAPITAL MARKET QUESTIONNAIRE not full - cannot mark it as DONE");
				return Constants.NAV_USER_CAP_QUESTIONNAIRE;
			}
		} else {
			log.warn("User regulation status is in unappropriate state [" + userRegulation
						+ "] for [capital] questionnaire. Cannot submit form");
		}
		
		return Constants.NAV_TRADE_PAGE;
	}
	
	public List<QuestionnaireQuestion> getQuestions() {
		return questionnaire.getQuestions();
	}

	public List<QuestionnaireUserAnswer> getUserAnswers() {
		return userAnswers;
	}
	
	public boolean canSubmit() {
		try {
			return QuestionnaireManagerBase.isAllCapitalAnswFilled(user.getId());
		} catch (SQLException e) {
			log.error("Can't get CapitalAnswFilled", e);
			return false;
		}
	}

	public boolean isSubmitted() {
		return submitted;
	}

	public void setSubmitted(boolean submitted) {
		this.submitted = submitted;
	}

}
