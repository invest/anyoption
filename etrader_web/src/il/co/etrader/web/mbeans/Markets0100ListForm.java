package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.util.ArrayList;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import com.anyoption.common.beans.TreeItem;

import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;

public class Markets0100ListForm implements Serializable {
    private static final long serialVersionUID = 1L;

    ArrayList<TreeItem> listClone;
    ArrayList<SelectItem> optionType;

    public Markets0100ListForm() {
    	updateList();
    	ArrayList<SelectItem> optionTypes = new ArrayList<SelectItem>();
		SelectItem closest = new SelectItem("1","opportunity.type.closest");
		optionTypes.add(closest);
		SelectItem endOfDay = new SelectItem("2","opportunity.type.endofday");
		optionTypes.add(endOfDay);
		SelectItem endOfWeek = new SelectItem("3","opportunity.type.endofweek");
		optionTypes.add(endOfWeek);
		SelectItem endOfMonth = new SelectItem("4","opportunity.type.endofmonth");
		optionTypes.add(endOfMonth);
		this.optionType = optionTypes;
    }

    /**
     * Get markets List by skinId and
     * Bulid a new formated list.
     */
    public void updateList() {
    	FacesContext context = FacesContext.getCurrentInstance();
        ApplicationData a = (ApplicationData)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
        listClone = a.getSkinById(a.getSkinId()).getTreeItems();
    }

    /**
     * Return formated list
     */
    public ArrayList<TreeItem> getListClone() {
    	return listClone;
    }

	public ArrayList<SelectItem> getOptionType() {
		return optionType;
	}

	public void setOptionType(ArrayList<SelectItem> optionType) {
		this.optionType = optionType;
	}
}