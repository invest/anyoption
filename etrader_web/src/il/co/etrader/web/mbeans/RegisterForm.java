package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.Platform;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.UsersDetailsHistory;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.RewardUserTasksManager;
import com.anyoption.common.managers.RewardUserTasksManager.TaskGroupType;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.sms.RegisterSMSThread;
import com.anyoption.common.util.AESUtil;

import il.co.etrader.bl_managers.AdminManagerBase;
import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bl_managers.EmailAuthenticationManagerBase;
import il.co.etrader.bl_managers.RemarketingLoginsManagerBase;
import il.co.etrader.bl_managers.SMSManagerBase;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.TransactionsManager;
import il.co.etrader.web.bl_managers.UsersManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.bl_vos.UserRegulation;
import il.co.etrader.web.util.Constants;
import il.co.etrader.web.util.Utils;

/**
 * @author liors
 *
 */
public class RegisterForm  implements Serializable{

	private static final long serialVersionUID = 7178739734122885690L;
    private static final Logger log = Logger.getLogger(RegisterForm.class);      

    protected String userName;
    protected String oldPassword;
    protected String password;
    protected String password2;
    protected String firstName;
    protected String lastName;
    protected String street;
    protected String streetNo;
    protected long cityId;
    protected String zipCode;
    protected String email;
    protected String birthYear;
    protected String birthMonth;
    protected String birthDay;
    protected boolean contactByEmail;
    protected String mobilePhonePrefix;
    protected String mobilePhone;
    protected String landLinePhonePrefix;
    protected String landLinePhone;
    protected String gender;
    protected String idNum;
    protected boolean terms;
    protected boolean contactBySms;
    protected String contactIssue;
    protected String comments;
    protected String cityName;
    private long fingerPrint = 0L;

    protected boolean receiveUpdate;

    // for anyOption
    protected Long currencyId;
    protected Long languageId;
    protected Long countryId;
    protected Long skinId;
    protected String cityNameAO = "";
    protected String utcOffsetCreated;
    protected Long stateId;

    // default phone code
    protected String landPhoneCode = "";
    protected String mobilePhoneCode = "";

    // user phone code (for personal page)
    protected String landUserPhoneCode = "";
    protected String mobileUserPhoneCode = "";
    protected long contactId;

    // Mobile properties
    protected int pageSection = 1;
    public static final int MOBILE_SECTION1 = 1;
    public static final int MOBILE_SECTION2 = 2;
    public static final int MOBILE_SECTION3 = 3;

    protected boolean agreementMode;
    protected Date birthDayDate;
    private String from;
    private boolean tempWCApproval = true; 
    
    protected boolean marketingTracking;
    
    public static final String FORBIDDEN_REGISTER_REDIRECT = "forbiddenRegisterRedirect";
    private final String mobilePhoneField;
    private final String emailField;
    
    protected User userDetails;
    protected HashMap<Long, String> userDetailsBeforeChangedHM;
    protected int platformId;
    
    protected boolean regularReportMail; 
    
	public RegisterForm() throws Exception{

		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		from = request.getParameter("from");
		if (CommonUtil.isParameterEmptyOrNull(from)) {
			from = (String) request.getSession().getAttribute("redirectAfterTerms");
		} else {
	    	request.getSession().setAttribute("redirectAfterTerms", from);
		}
    	// default
		contactByEmail = true;
		contactBySms = true;
		receiveUpdate = true;
		marketingTracking = false;
		regularReportMail = true;
		
		handleParamsFromLanding();

    	// get default values by skin
    	Skins skin = ApplicationData.getSkinById(ap.getSkinId());
    	setSkinId(Long.valueOf(skin.getId()));
    	currencyId = Long.valueOf(skin.getDefaultCurrencyId());
    	languageId = Long.valueOf(skin.getDefaultLanguageId());
    	countryId =  Long.valueOf(skin.getDefaultCountryId());

    	// For Mobile
    	if (CommonUtil.isMobile()) {
	    	Calendar c = Calendar.getInstance(TimeZone.getTimeZone(CommonUtil.getUtcOffset()));
	    	c.set(Calendar.HOUR_OF_DAY, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
	    	c.set(Calendar.DAY_OF_MONTH, 1);
	    	c.set(Calendar.MONTH, Calendar.JANUARY);
	    	c.set(Calendar.YEAR, 1980);
	    	birthDayDate = c.getTime();
    	}

    	stateId = new Long(0);  // default

    	User user = (User)request.getSession().getAttribute(Constants.BIND_SESSION_USER);
    	if (!CommonUtil.isHebrewSkin(skin.getId())) { // anyoption
            //getInit();
            setSkinId(Long.valueOf(skin.getId()));
            if (null != ap.getRealCountryId() ) {
            	countryId = Long.valueOf(ap.getRealCountryId());
            }
            //ap.getRealCountryId() might return null and countryId already gets country by skin
        	if (null == countryId || countryId.longValue() == 0) {
        		countryId = Long.valueOf(skin.getDefaultCountryId());}

    		String code = ApplicationData.getCountryPhoneCodes(String.valueOf(countryId));
	    	landPhoneCode = code;
	    	mobilePhoneCode = code;

	    	// set default currency to dollar, for all countries except spain
	    	if ( skin.getId() == Skin.SKIN_SPAIN ) {
	    		if ( null != countryId && !countryId.toString().equals(Constants.SPAIN_COUNTRY_ID)) {
	    			currencyId = Long.valueOf(ConstantsBase.CURRENCY_USD_ID);
	    		}
	    	} else if (skin.getId() == Skin.SKIN_RUSSIAN && null != countryId && countryId.toString().equals(Constants.RUSSIA_COUNTRY_ID)){
	    		//Set default currency to ruble if country is Russia
	        	currencyId = Long.valueOf(ConstantsBase.CURRENCY_RUB_ID);
	    	} else if (skin.getId() == Skin.SKIN_CHINESE && null != countryId && countryId.toString().equalsIgnoreCase(Constants.CHINA_COUNTRY_ID)) {
	    		//Set default currency to cny currency
	        	currencyId = Long.valueOf(ConstantsBase.CURRENCY_CNY_ID);
	    	} else if(skin.getId() == Skin.SKIN_KOREAN && null != countryId && countryId == ConstantsBase.COUNTRY_ID_SOUTH_KOREA) {
	    		currencyId = ConstantsBase.CURRENCY_KRW_ID;
        	} 

	    	if (null != user && user.getId() > 0 ){
	    		mobilePhone = user.getMobilePhone();
	    		landLinePhone = user.getLandLinePhone();
	    	}

	    	// VSP - chat id
			String chatId = request.getParameter(Constants.VSP_CHAT_ID);
			if (null != chatId) {
				session.setAttribute(Constants.VSP_CHAT_ID, chatId);
			}

    	}
    	gender = "M";
    	agreementMode = false;
		String page = context.getViewRoot().getViewId();
		// Set default values for register lite landing page
		if (!CommonUtil.isParameterEmptyOrNull(page) &&  page.indexOf("registerLite") > -1) {  
			street = "unknown";
			cityNameAO = "unknown"; 
			currencyId = Long.valueOf(ConstantsBase.CURRENCY_CNY_ID); 
			countryId = ConstantsBase.COUNTRY_ID_CHINA;
    		String code = ApplicationData.getCountryPhoneCodes(String.valueOf(countryId));
	    	landPhoneCode = code;
	    	mobilePhoneCode = code;
    	}
		
		// not in use here, so we initialze them with nulls
		mobilePhoneField = null;
		emailField = null;
		if (null != user && user.getId() > 0) {
		    userDetailsBeforeChangedHM = UsersManager.getUserDetailsHM(user.getSkinId(), user.getStreet(), user.getCountryId(), user.getGender(), user.getEmail(), user.getMobilePhone(), user.getLandLinePhone(), 
		            user.getContactByEmail(), user.isContactBySMS(), user.getBirthDay(), user.getBirthMonth(), user.getBirthYear(), user.getCityName(), user.getIdNum(), user.getState(), user.getFirstName(), 
		            user.getLastName(), user.getPassword(), null);
		}
	}

	/**
	 * Constructor that registers a user with the new registration funnel
	 * 
	 * @param email user email
	 * @param emailField email html component id to pass to js for validation
	 * @param firstName first name of the user
	 * @param lastName last name of the user
	 * @param countryId id of the users country
	 * @param mobilePhone mobile phone
	 * @param mobilePhoneField mobile phone html component id to pass to js for validation
	 * @param mobilePhonePrefix only valid for etrader skin
	 * @param password user password
	 * @param passwordConfirm retyped user password
	 * @param idNum the Israeli personal ID number (only for ETrader)
	 */
	public RegisterForm(String email, String emailField, String firstName, String lastName, String countryId, String mobilePhone,
							String mobilePhoneField, String mobilePhonePrefix, String password, String passwordConfirm, String idNum, String fingerPrint) {
		this.userName = email;
		this.email = email;
		this.emailField = emailField;
		this.firstName = firstName;
		this.lastName = lastName;
		this.countryId = Long.valueOf(countryId);
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = context.getApplication().evaluateExpressionGet(context, Constants.BIND_APPLICATION_DATA, ApplicationData.class);
		skinId = ap.getSkinId();
		Skins skin = ApplicationData.getSkinById(skinId);
		languageId = Long.valueOf(skin.getDefaultLanguageId());
		currencyId = Long.valueOf(skin.getDefaultCurrencyId());
		this.mobilePhone = mobilePhone;
		this.mobilePhoneField = mobilePhoneField;
		this.mobilePhonePrefix = (this.countryId != Constants.COUNTRY_ID_IL)
									? CountryManagerBase.getSortedCountriesMap(skinId).get(this.countryId).getPhoneCode()
									: mobilePhonePrefix;
		this.password = password;
		this.password2 = passwordConfirm;
		this.idNum = idNum;
		if (fingerPrint != null) {			
			this.setFingerPrint(Long.valueOf(fingerPrint));
		}
		// default		
		terms = true;
		contactByEmail = true;
		contactBySms = true;
		receiveUpdate = true;
		marketingTracking = false;
		regularReportMail = true;
		
    	// set default currency to dollar, for all countries except spain
    	if (skin.getId() == Skin.SKIN_SPAIN) {
    		if (null != countryId && !countryId.toString().equals(Constants.SPAIN_COUNTRY_ID)) {
    			currencyId = Long.valueOf(ConstantsBase.CURRENCY_USD_ID);
    		}
    	} else if (skin.getId() == Skin.SKIN_RUSSIAN && null != countryId && countryId.toString().equals(Constants.RUSSIA_COUNTRY_ID)) {
    		//Set default currency to ruble if country is Russia
        	currencyId = Long.valueOf(ConstantsBase.CURRENCY_RUB_ID);
    	} else if (ap.isChineseSkin() && null != countryId && countryId.toString().equalsIgnoreCase(Constants.CHINA_COUNTRY_ID)) {
    		//Set default currency to cny currency
        	currencyId = Long.valueOf(ConstantsBase.CURRENCY_CNY_ID);
    	}

    	String page = context.getViewRoot().getViewId();
    	// Set default values for register lite landing page
		if (!CommonUtil.isParameterEmptyOrNull(page) &&  page.indexOf("registerLite") > -1) {
			street = "unknown";
			cityNameAO = "unknown"; 
			currencyId = Long.valueOf(ConstantsBase.CURRENCY_CNY_ID); 
			this.countryId = ConstantsBase.COUNTRY_ID_CHINA;
			String code = ApplicationData.getCountryPhoneCodes(String.valueOf(countryId));
	    	landPhoneCode = code;
	    	mobilePhoneCode = code;
		}
		
		// For Mobile LP with register funnel
    	if (CommonUtil.isMobile()) {
	    	Calendar c = Calendar.getInstance(TimeZone.getTimeZone(CommonUtil.getUtcOffset()));
	    	c.set(Calendar.HOUR_OF_DAY, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
	    	c.set(Calendar.DAY_OF_MONTH, 1);
	    	c.set(Calendar.MONTH, Calendar.JANUARY);
	    	c.set(Calendar.YEAR, 1980);
	    	birthDayDate = c.getTime();
    	}
    	
    	try {
            userDetailsBeforeChangedHM = UsersManager.getUserDetailsHM(skinId, null, ConstantsBase.NON_SELECTED, null, null, null, null, false, false, null, null, null, null, null, ConstantsBase.NON_SELECTED, null, null, null, null);
        } catch (Exception e) {
            log.error("Error when get user details HM", e);
        }
	}

	/**
	 * Check and set paraemeters from landing pages
	 * @throws UnsupportedEncodingException
	 * @throws SQLException
	 */
	private void handleParamsFromLanding() throws UnsupportedEncodingException, SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();

	    if (!CommonUtil.isParameterEmptyOrNull(request.getQueryString())) {
			String s = URLDecoder.decode(request.getQueryString(), "utf-8");
			//***********Function to break the NVP string into a HashMap*********************//
			 HashMap<String, String> nvp = new HashMap<String, String>();
		      StringTokenizer stTok = new StringTokenizer( s, "&");
		      while (stTok.hasMoreTokens())  {
		    	  try{
			          StringTokenizer stInternalTokenizer = new StringTokenizer(stTok.nextToken(), "=");
		              String key = URLDecoder.decode(stInternalTokenizer.nextToken(), "UTF-8");
		              String value = URLDecoder.decode( stInternalTokenizer.nextToken(), "UTF-8");
		              nvp.put(key.toUpperCase(), value);
		    	  } catch (Exception e) { // avoid receiving parameters without a value
		    		  log.error("Parameter without value after landing page");
				}
		      }
			//*******************************************************************************//

			if (!CommonUtil.isParameterEmptyOrNull(nvp.get("FIRSTNAME"))) {
				firstName = nvp.get("FIRSTNAME");
			}
			if (!CommonUtil.isParameterEmptyOrNull(nvp.get("LAST_NAME"))) {
				lastName = nvp.get("LAST_NAME");
			}
			if (!CommonUtil.isParameterEmptyOrNull(nvp.get("EMAIL"))) {
				email = nvp.get("EMAIL");
			}
			if (!CommonUtil.isParameterEmptyOrNull(nvp.get("MOBILEPHONECODE"))) {
				mobilePhoneCode = nvp.get("MOBILEPHONECODE");
			}
			if (!CommonUtil.isParameterEmptyOrNull(nvp.get("MOBILEPHONE"))) {
				mobilePhone = nvp.get("MOBILEPHONE");
			}
			if (!CommonUtil.isParameterEmptyOrNull(nvp.get("RECIVEUPDATES"))) {
				contactBySms = Boolean.parseBoolean(nvp.get("RECIVEUPDATES"));
				contactByEmail = contactBySms;
				receiveUpdate =  contactByEmail;
			}
			if (CommonUtil.isHebrewUserSkin() && !CommonUtil.isParameterEmptyOrNull(nvp.get("MOBILEPHONEPREF"))){
				mobilePhonePrefix = nvp.get("MOBILEPHONEPREF");
			}
			
			if (!CommonUtil.isParameterEmptyOrNull(nvp.get("MARKETINGTRACKING"))) {
				marketingTracking = Boolean.parseBoolean(nvp.get("MARKETINGTRACKING"));
			}
	    }
	}

    /**
     * check if need redirect each time page is loaded for israeli users in ao register page
     * @return
     * @throws Exception
     */
//    public String getInit() throws Exception {
//        FacesContext context = FacesContext.getCurrentInstance();
//        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
//        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
//        ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
//        Skins skin = ApplicationData.getSkinById(ap.getSkinId());
//        setSkinId(Long.valueOf(skin.getId()));
//
//
//
//
//        if (!CommonUtil.isHebrewSkin(skin.getId())) { // anyoption
//            String fromIsrael = null;
//            if(ap.getRealCountryId() != null){
//                countryId = Long.valueOf(ap.getRealCountryId());
//                //redirect users from iran or israel to error page
//                fromIsrael = (String)session.getAttribute(Constants.FROM_REDIRECT);
//                if ((null == fromIsrael || (null != fromIsrael && !fromIsrael.equalsIgnoreCase(Constants.FROM_REDIRECT_ISRAEL)))
//                        && (countryId == Constants.COUNTRY_ID_IL || countryId == Constants.COUNTRY_ID_IRAN) && !CommonUtil.getIPAddress().equals("212.150.171.253") && !CommonUtil.getIPAddress().equals("212.150.171.222") && !CommonUtil.getIPAddress().equals("172.16.100.0") && !CommonUtil.getIPAddress().equals("82.166.29.184") && !CommonUtil.getIPAddress().equals("82.166.29.181")) {
//
//                    String root = Constants.WEB_DIRECTORY;
//                    if (CommonUtil.isMobile()) {
//                        root = Constants.MOBILE_DIRECTORY;
//                    }
//                    response.sendRedirect(ap.getHomePageUrl() + root + Constants.ERROR_PAGE_FORBIDDEN_REGISTER);
//                }
//            }
//        }
//
//        validateForbiddenNetreferRegisterRedirect(response, ap, context);
//
//        return "";
//    }
//
    public String validateForbiddenNetreferRegisterRedirect(HttpServletResponse response, ApplicationData ap, FacesContext context) throws Exception {
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        Cookie[] cookiesArr = request.getCookies();
        String combIdCookieValue = "";

        if (cookiesArr != null) {
            for (int i = 0; i < cookiesArr.length; i++) {
                if (cookiesArr[i].getName().equals(Constants.COMBINATION_ID)) {
                    log.log(Level.DEBUG, "found combid cookie! combid:" + cookiesArr[i].getValue());
                    combIdCookieValue = cookiesArr[i].getValue();
                }
            }
        }

        if (!CommonUtil.isParameterEmptyOrNull(combIdCookieValue)){
            if (ApplicationDataBase.isNetreferCombination(Long.valueOf(combIdCookieValue))) { // check if user in from Netrefer(certain countries)
            	String realCountryId = ap.getRealCountryId();
            	if(null != realCountryId && Long.valueOf(ap.getRealCountryId()) > 0) {
                    countryId =  Long.valueOf(ap.getRealCountryId());
                    Country country = ApplicationDataBase.getCountry(countryId);
                    if (country.isNetreferRegBlock() && !ApplicationData.isAllowAffilate(request)) {
                        String root = Constants.WEB_DIRECTORY;
                        if (CommonUtil.isMobile()) {
                            root = Constants.MOBILE_DIRECTORY;
                        }
                        log.debug("Redirecting to :" + ap.getHomePageUrl() + root + Constants.ERROR_PAGE_FORBIDDEN_REGISTER);
                    	String path = ap.getHomePageUrl() + root + Constants.ERROR_PAGE_FORBIDDEN_REGISTER + Constants.ERROR_PAGE_FORBIDDEN_REGISTER_PARAM_NETREFER;
                    	FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, path, FORBIDDEN_REGISTER_REDIRECT);
                    	context.addMessage("", msg);
//                            response.sendRedirect(ap.getHomePageUrl() + root + Constants.ERROR_PAGE_FORBIDDEN_REGISTER + Constants.ERROR_PAGE_FORBIDDEN_REGISTER_PARAM_NETREFER);
//                            context.responseComplete();
                    }
                }
            }
        }
        return "";
    }
    
    public boolean validateForbiddenCountry(ApplicationData ap, FacesContext context, long skinId, Long regFormCountryId){
      //Server side validation for AO from new funnel short reg
        boolean res = true;
        String fromIsrael = null;        
        Long countryId = 0L;

        if(ap.getRealCountryId() != null){
            countryId =  Long.valueOf(ap.getRealCountryId());
            HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
            fromIsrael = (String)session.getAttribute(Constants.FROM_REDIRECT);
            String ipAddress = CommonUtil.getIPAddress(); 
            if (CountryManagerBase.isCountryForbidenAndIp(ipAddress, ap.getSkinId(), regFormCountryId, Long.valueOf(ap.getRealCountryId()))) {
                String root = Constants.WEB_DIRECTORY;
                if (CommonUtil.isMobile()) {
                    root = Constants.MOBILE_DIRECTORY;
                }
                String forbiddenRegisterUrl = ap.getHomePageUrl() + root;
                if (countryId == Constants.COUNTRY_ID_US) {
                    forbiddenRegisterUrl += Constants.ERROR_PAGE_FORBIDDEN_REGISTER + "?forbidUSA=true";
                } else if (ap.getSkinId() == Skin.SKIN_ARABIC) {
                    forbiddenRegisterUrl += Constants.ERROR_PAGE_FORBIDDEN_REGISTER + "?forbidAR=true";
                } else {
                    forbiddenRegisterUrl += Constants.ERROR_PAGE_FORBIDDEN_REGISTER + Constants.ERROR_PAGE_FORBIDDEN_REGISTER_PARAM_ISRAEL_IRAN;
                }

                    log.debug("ipAddress=" + ipAddress + " Redirecting to :" + forbiddenRegisterUrl);
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, forbiddenRegisterUrl, FORBIDDEN_REGISTER_REDIRECT);
                    context.addMessage("", msg);
                    res = false;
            }

        }
        return res;
    }

    public String insertUser() throws Exception {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);

    	this.setPlatformId(Platform.ETRADER);
    	if (CommonUtil.isParameterEmptyOrNull(this.idNum)) {
    		this.idNum = Constants.ID_NUM_DEFAULT_VALUE;
    		if(this.skinId != Skins.SKIN_ETRADER){
        		this.setPlatformId(Platform.ANYOPTION);
    		}
    	}

        validateForbiddenNetreferRegisterRedirect(response, ap, context);
        try {
            if(this.skinId != Skin.SKIN_ETRADER && !validateForbiddenCountry(ap, context, skinId, countryId)){ //do not change order as validateForbiddenCountry also return navigation case
                return "";
            }
        } catch (Exception ex) {
            log.error("when validateForbiddenCountry", ex);
        }

    	setUtcOffsetCreated(CommonUtil.getUtcOffset(ConstantsBase.EMPTY_STRING));
    	if ( null != countryId && countryId != ConstantsBase.COUNTRY_ID_US ) {
    		stateId = new Long(0);
    	}

    	HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
    	Cookie[] cookiesArr = request.getCookies();
		Cookie contactCookie = null;
		if(cookiesArr != null){
			for(Cookie c : cookiesArr){
				if(c.getName().equals(Constants.CONTACT_ID)){
					contactCookie = c;
					//break;
				}
			}
		}

		if(contactCookie != null){
			contactId = Long.parseLong(contactCookie.getValue());
		}

		// For mobile use
		if (CommonUtil.isMobile()) {
			Calendar c = Calendar.getInstance(TimeZone.getTimeZone(CommonUtil.getUtcOffset()));
			c.setTime(birthDayDate);
			birthDay = String.valueOf(c.get(Calendar.DAY_OF_MONTH));
		    birthMonth = String.valueOf(c.get(Calendar.MONTH) + 1);
		    birthYear = String.valueOf(c.get(Calendar.YEAR));
		}

		//for anyoption we want to add recieve update
		if (!CommonUtil.isHebrewUserSkin() && !CommonUtil.isMobile()) {
			contactByEmail = receiveUpdate;
			contactBySms = contactByEmail;
		}
		
		skinId = SkinsManagerBase.getSkinIdInsertUser(CommonUtil.getIPAddress(), countryId, skinId);
		request.setAttribute(Constants.PRETTY_SKIN_ID_ATR, skinId);
		ap.getSaveAnyOptionCookie();
		
		// change user details.
		HashMap<Long, String> userDetailsAfterChangedHM = UsersManager.getUserDetailsHM(this.getSkinId(), this.getStreet(), this.getCountryId(), this.getGender(), this.getEmail(), this.getMobilePhone(), 
		        this.getLandLinePhone(), this.isContactByEmail(), this.isContactBySms(), this.getBirthDay(), this.getBirthMonth(), this.getBirthYear(), this.getCityName(), this.getIdNum(), this.getStateId(), 
		        CommonUtil.capitalizeFirstLetters(this.getFirstName()), CommonUtil.capitalizeFirstLetters(this.getLastName()), this.getPassword(), null);
		ArrayList<UsersDetailsHistory> usersDetailsHistoryList = UsersManager.checkUserDetailsChange(userDetailsBeforeChangedHM, userDetailsAfterChangedHM);
		
		//ET Regulation when insert user terms must be false
		if(this.skinId == Skins.SKIN_ETRADER){
			this.terms = false;
		}
		boolean res = UsersManager.insertUser(this);

		if (res) {
			
			// set currency
			if (!CommonUtil.isHebrewUserSkin()) {
				ap.setCurrencyAfterRegistration(currencyId);
				// initial fromLanding attribute
				HttpSession session = request.getSession(true);
				if (null != session.getAttribute(Constants.REGISTER_FROM_CONTACTUS_LANDING)) {
					session.removeAttribute(Constants.REGISTER_FROM_CONTACTUS_LANDING);
					session.removeAttribute(Constants.REGISTER_FROM_CONTACTUS_LANDING_FN);
					session.removeAttribute(Constants.REGISTER_FROM_CONTACTUS_LANDING_LN);
				}
			}
		
			HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
			session.setAttribute(ConstantsBase.REMARKETING_REGISTER, "true");
			
			User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
			
			RewardUserTasksManager.rewardTasksHandler(TaskGroupType.REGISTRAITION, user.getId(), 0, user.getId(), BonusManagerBase.class, Writer.WRITER_ID_WEB);
			
			RemarketingLoginsManagerBase.insertRemarektingLoginBytype(0, 
					RemarketingLoginsManagerBase.REMARKETING_TYPE_REGISTER, user.getId(), user.getEmail(), user.getMobilePhone(), user.getLandLinePhone(), user.getSkinId(), null);  
			
			if (null != usersDetailsHistoryList) {
                UsersManagerBase.insertUsersDetailsHistory(Constants.WRITER_WEB_ID, user.getId(), user.getUserName(), usersDetailsHistoryList, String.valueOf(user.getClassId()));
            }
			new RegisterSMSThread(user, ap.getUserLocale(), user.getPlatformId(), Writer.WRITER_ID_WEB).start();
			if (CommonUtil.isHebrewUserSkin() ||
					CommonUtil.isMobile()) {
				return Constants.NAV_FIRST_NEW_CARD_REDIRECT;
			} else {
				if (context.getViewRoot().getViewId().indexOf("register") > -1){
					return Constants.NAV_FIRST_NEW_CARD_REDIRECT;
				} else {//when coming from landing pages
					return Constants.NAV_FIRST_NEW_CARD_REDIRECT;
				}
			}
		}
		else {
			return null;
		}
	}
 
    private boolean validateEmailIfExistInDB(String emailNew){
     		
    	try {
				return UsersManager.testIfThisEmailExistInDB(emailNew);
			} catch (SQLException e) {
				e.printStackTrace();
			}
    		
     return false;	
    }

   /**
    * Update user details
    * @return
    * @throws Exception
    */
   public String updateUserDetails() throws Exception{

	   User user = ApplicationData.getUserFromSession();
	   long userId = user.getId();
	   String newMobilePhone;
	   String newLandLinePhone;
	   boolean isNeedAuthorization = false;
	   boolean verifyPhone = false;

	   if (CommonUtil.isHebrewSkin(user.getSkinId())) {
		   newMobilePhone =  user.getMobilePhonePrefix() + user.getMobilePhoneSuffix();
		   newLandLinePhone = user.getLandLinePhonePrefix() + user.getLandLinePhoneSuffix();

	       if ( user.getCountryId() != ConstantsBase.COUNTRY_ID_US ) {
	    		 user.setState(new Long(0));
	       }
	   } else {  // AO
		   	newMobilePhone = handleLeadingZero(mobilePhone);
		    newLandLinePhone = handleLeadingZero(landLinePhone);

			user.setCountry(ApplicationDataBase.getCountry(user.getCountryId()));

			UserBase oldUser = UsersManager.getUserById(userId);
			//if email was changed, handle users authorization
		    if (!oldUser.getEmail().equalsIgnoreCase(user.getEmail())){
				//isNeedAuthorization = false;	
    			//user.setAuthorizedMail(true);
    			//user.setAuthorizedMailRefusedTimes(0);
		    	if(!validateEmailIfExistInDB(user.getEmail())){
		    		FacesContext contx = FacesContext.getCurrentInstance();
                    FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        CommonUtil.getMessage("error.register.email.inuse", null), null);
                    contx.addMessage("personalForm:email", fm); 
                    return null;
		    	}
		    	
			}
			if (!oldUser.getMobilePhone().equals(newMobilePhone) || oldUser.getCountryId() != user.getCountryId()) {
				verifyPhone = true;
			}
	   }

	   UsersManager.checkDetailsChange(userId, newMobilePhone + newLandLinePhone, user.getMobilePhone() + user.getLandLinePhone(), ConstantsBase.USER_CHANGE_DETAILS_PHONE_NUMBER);

	   user.setMobilePhone(newMobilePhone);
	   user.setLandLinePhone(newLandLinePhone);

		if (contactByEmail) {
			user.setIsContactByEmail(1);
		}
		else {
			user.setIsContactByEmail(0);
		}
		if (contactBySms) {
			user.setIsContactBySMS(1);
		}
		else {
			user.setIsContactBySMS(0);
		}
		

		
        if (user.getTimeBirthDate() == null) {
            if (!CommonUtil.isParameterEmptyOrNull(getBirthDay())
                    && !CommonUtil.isParameterEmptyOrNull(getBirthMonth())
                    && !CommonUtil.isParameterEmptyOrNull(getBirthYear())) {
                if (Utils.birthDateValidation(getBirthYear(), getBirthMonth(), getBirthDay())){
                    
                    Calendar c = new GregorianCalendar(Integer.parseInt(getBirthYear()),
                            Integer.parseInt(getBirthMonth()) - 1,
                            Integer.parseInt(getBirthDay()), 0, 1); 
                    user.setTimeBirthDate(new Date(c.getTimeInMillis()));
                } else {
                    
                    FacesContext contx = FacesContext.getCurrentInstance();
                    FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            CommonUtil.getMessage("error.register.birthdate", null), null);
                    contx.addMessage("personalForm:birthDay", fm); 
                    return null;
                }
            
            } else if (!CommonUtil.isParameterEmptyOrNull(getBirthDay())
                    || !CommonUtil.isParameterEmptyOrNull(getBirthMonth())
                    || !CommonUtil.isParameterEmptyOrNull(getBirthYear())) {
                FacesContext context2 = FacesContext.getCurrentInstance();               
                if (CommonUtil.isParameterEmptyOrNull(getBirthDay())) {
                    FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            CommonUtil.getMessage("CMS.registerContent.text.error.register.day", null), null);
                    context2.addMessage("personalForm:birthDay", fm);
                }
                if (CommonUtil.isParameterEmptyOrNull(getBirthMonth())) {
                    FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            CommonUtil.getMessage("error", null), null);
                    context2.addMessage("personalForm:birthMonth", fm);
                }
                if (CommonUtil.isParameterEmptyOrNull(getBirthYear())) {
                    FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            CommonUtil.getMessage("error", null), null);
                    context2.addMessage("personalForm:birthYear", fm);
                }
                return null;
            }
        }
        HashMap<Long, String> userDetailsAfterChangedHM = UsersManager.getUserDetailsHM(user.getSkinId(), user.getStreet(), user.getCountryId(), user.getGender(), user.getEmail(), user.getMobilePhone(), 
                user.getLandLinePhone(), user.getContactByEmail(), user.isContactBySMS(), user.getBirthDay(), user.getBirthMonth(), user.getBirthYear(), user.getCityName(), user.getIdNum(), user.getState(), 
                user.getFirstName(), user.getLastName(), user.getPassword(), null);
        ArrayList<UsersDetailsHistory> usersDetailsHistoryList = UsersManager.checkUserDetailsChange(userDetailsBeforeChangedHM, userDetailsAfterChangedHM);
       
	   boolean res = UsersManager.updateUserDetails(user);

		if (res) {
		    if (null != usersDetailsHistoryList) {
                UsersManagerBase.insertUsersDetailsHistory(Constants.WRITER_WEB_ID, user.getId(), user.getUserName(), usersDetailsHistoryList, String.valueOf(user.getClassId()));
            }
			// set the change details password to unvalid.
			user.setChangeDetailsPassValidated(false);

			FacesContext context = FacesContext.getCurrentInstance();
			ApplicationData ap = context.getApplication().evaluateExpressionGet(context, Constants.BIND_APPLICATION_DATA, ApplicationData.class);
			if (isNeedAuthorization){
	    		String authLink = ap.getHomePageUrlHttps() + "jsp/loginAuth.jsf?";
	    		try {
	    			EmailAuthenticationManagerBase.sendActivationEmail(authLink, CommonUtil.getWebWriterId(), user);
	    			user.setEmailRemindMeLater(false);
	    			user.setSentActivationEmail(true);
				} catch (Exception e2) {
					log.error("can't send activation mail!! ", e2);
				}
			}
			if (verifyPhone) {
				SMSManagerBase.sendPhoneNumberVerificationMessage(user, ap.getUserLocale(), Constants.PLATFORM_ID_ANYOPTION);
			}
			if ( user.getSkinId() == Skin.SKIN_ETRADER ) {
				return Constants.NAV_EMPTY_MYACCOUNT;
			}
			else {
				return Constants.NAV_EMPTY_MYACCOUNT_PERSONAL;
			}
		}
		else {
			return null;
		}
	}

   public String changePass() throws Exception{

	   	User user = ApplicationData.getUserFromSession();

	   	String password = user.getPassword();
	   	String newPassword = this.getPassword();
	   	boolean checkPassNeeded = user.getIsNeedChangePassword();
	   	boolean res = UsersManager.changePass(this,user);

		if (res) {
	        if(user.getSkinId() == Skin.SKIN_ETRADER){
	            password = password.toUpperCase();
	            newPassword = newPassword.toUpperCase();
	        }
	        try {
	            UsersManagerBase.insertUsersDetailsHistoryOneField(Constants.WRITER_WEB_ID, user.getId(), user.getUserName(), String.valueOf(user.getClassId()), UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_PASSWORD, 
	                    AESUtil.encrypt(password), AESUtil.encrypt(newPassword));
	        } catch (CryptoException ce) {
	            throw new SQLException("CryptoException: " + ce.getMessage());
	        }
	        if (checkPassNeeded) {
				return Constants.NAV_PASS_OUTCOME;
			}
			if ( user.getSkinId() == Skin.SKIN_ETRADER ) {
				return Constants.NAV_EMPTY_MYACCOUNT;
			}
			else {
				return Constants.NAV_EMPTY_MYACCOUNT_PASS;
			}
		}
		else {
			return null;
		}
	}

   public String getTaxBalanceNote() {
	   GregorianCalendar gc = new GregorianCalendar();
	   String[] params = new String[1];
	   Date timeWithOffset = CommonUtil.getDateTimeFormaByTz(gc.getTime(), CommonUtil.getUtcOffset());
	   gc.setTime(timeWithOffset);

//	   if (gc.get(GregorianCalendar.MONTH) >= GregorianCalendar.JULY) {
//		   params[0]="01.07."+gc.get(GregorianCalendar.YEAR);
//		   return CommonUtil.getMessage("balance.taxbalance.note", params);
//	   }
	   params[0]="01.01."+gc.get(GregorianCalendar.YEAR);
	   return CommonUtil.getMessage("balance.taxbalance.note", params);
   }


    /**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the password2
	 */
	public String getPassword2() {
		return password2;
	}

	/**
	 * @param password2 the password2 to set
	 */
	public void setPassword2(String password2) {
		this.password2 = password2;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the cityId
	 */

	public long getCityId() {

		return cityId;
	}

	public void setCityId(long id) throws SQLException{
		this.cityId=id;
	}

	/**
	 * Return cityName
	 * for etrader skin - take name by cityId
	 * for non etrader skin - take name from page
	 * @return
	 * 		city name
	 */
	public String getCityName() {
    	FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData a = (ApplicationData)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		String n = null;
		if (CommonUtil.isHebrewSkin(a.getSkinId())) {
			n = a.getCities().get(String.valueOf(cityId));
			if (n == null) {
				return cityName;
			}
		}
		else { // not etrader skin
			n = cityNameAO;
		}
    	return n;
    }

	public void setCityName(String name) throws SQLException {

		cityId = AdminManagerBase.getCityIdByName(name);
		this.cityName = name;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}


	/**
	 * @return the birthYear
	 */
	public String getBirthYear() {
		return birthYear;
	}

	/**
	 * @param birthYear the birthYear to set
	 */
	public void setBirthYear(String birthYear) {
		this.birthYear = birthYear;
	}

	/**
	 * @return the birthMonth
	 */
	public String getBirthMonth() {
		return birthMonth;
	}

	/**
	 * @param birthMonth the birthMonth to set
	 */
	public void setBirthMonth(String birthMonth) {
		this.birthMonth = birthMonth;
	}

	/**
	 * @return the birthDay
	 */
	public String getBirthDay() {
		return birthDay;
	}

	/**
	 * @param birthDay the birthDay to set
	 */
	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	/**
	 * @return the contactByEmail
	 */
	public boolean isContactByEmail() {
		return contactByEmail;
	}

	/**
	 * @param contactByEmail the contactByEmail to set
	 */
	public void setContactByEmail(boolean contactByEmail) {
		this.contactByEmail = contactByEmail;
	}

	/**
	 * @return the contactBySms
	 */
	public boolean isContactBySms() {
		return contactBySms;
	}

	/**
	 * @param contactBySms the contactBySms to set
	 */
	public void setContactBySms(boolean contactBySms) {
		this.contactBySms = contactBySms;
	}

	/**
	 * @return the mobilePhonePrefix
	 */
	public String getMobilePhonePrefix() {
		return mobilePhonePrefix;
	}

	/**
	 * @param mobilePhonePrefix the mobilePhonePrefix to set
	 */
	public void setMobilePhonePrefix(String mobilePhonePrefix) {
		this.mobilePhonePrefix = mobilePhonePrefix;
	}

	/**
	 * @return the mobilePhone
	 */
	public String getMobilePhone() {
		return mobilePhone;
	}

	/**
	 * @param mobilePhone the mobilePhone to set
	 */
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	/**
	 * @return the landLinePhonePrefix
	 */
	public String getLandLinePhonePrefix() {
		return landLinePhonePrefix;
	}

	/**
	 * @param landLinePhonePrefix the landLinePhonePrefix to set
	 */
	public void setLandLinePhonePrefix(String landLinePhonePrefix) {
		this.landLinePhonePrefix = landLinePhonePrefix;
	}

	/**
	 * @return the landLinePhone
	 */
	public String getLandLinePhone() {
		return landLinePhone;
	}

	/**
	 * @param landLinePhone the landLinePhone to set
	 */
	public void setLandLinePhone(String landLinePhone) {
		this.landLinePhone = landLinePhone;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the idNum
	 */
	public String getIdNum() {
		return idNum;
	}

	/**
	 * @param idNum the idNum to set
	 */
	public void setIdNum(String idNum) {
		this.idNum = idNum;
	}

	/**
	 * @return the terms
	 */
	public boolean isTerms() {
		return terms;
	}

	/**
	 * @param terms the terms to set
	 */
	public void setTerms(boolean terms) {
		this.terms = terms;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getStreetNo() {
		return streetNo;
	}

	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}

	public String getContactIssue() {
		return contactIssue;
	}

	public void setContactIssue(String contactIssue) {
		this.contactIssue = contactIssue;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

//	public void setCurrencyId(Long currencyId) {
//		this.currencyId = currencyId;
//	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	@Override
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "RegisterForm ( "
	        + super.toString() + TAB
	        + "userName = " + this.userName + TAB
	        + "oldPassword = " + "*****" + TAB
	        + "password = " + "*****" + TAB
	        + "password2 = " + "*****" + TAB
	        + "currencyId = " + this.currencyId + TAB
	        + "firstName = " + this.firstName + TAB
	        + "lastName = " + this.lastName + TAB
	        + "street = " + this.street + TAB
	        + "streetNo = " + this.streetNo + TAB
	        + "cityId = " + this.cityId + TAB
	        + "zipCode = " + this.zipCode + TAB
	        + "email = " + this.email + TAB
	        + "birthYear = " + this.birthYear + TAB
	        + "birthMonth = " + this.birthMonth + TAB
	        + "birthDay = " + this.birthDay + TAB
	        + "contactByEmail = " + this.contactByEmail + TAB
	        + "mobilePhonePrefix = " + this.mobilePhonePrefix + TAB
	        + "mobilePhone = " + this.mobilePhone + TAB
	        + "landLinePhonePrefix = " + this.landLinePhonePrefix + TAB
	        + "landLinePhone = " + this.landLinePhone + TAB
	        + "gender = " + this.gender + TAB
	        + "idNum = " + this.idNum + TAB
	        + "terms = " + this.terms + TAB
	        + "contactIssue = " + this.contactIssue + TAB
	        + "comments = " + this.comments + TAB
	        + "cityName = " + this.cityName + TAB
	        + "countryId = " + this.countryId + TAB
	        + "languageId = " + this.languageId + TAB
	        + "skinId = " + this.skinId + TAB
	        + "contactBySms = " + this.contactBySms + TAB
	        + " )";

	    return retValue;
	}
	/**
	 * @return the currencyIdTest
	 */
	public Long getCurrencyId() {
		return currencyId;
	}
	/**
	 * @param currencyIdTest the currencyIdTest to set
	 */
	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the languageId
	 */
	public Long getLanguageId() {
		return languageId;
	}
	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(Long languageId) {
		this.languageId = languageId;
	}

	/**
	 * @return the countryId
	 */
	public Long getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the skinId
	 */
	public Long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinIdthe skinId to set
	 */
	public void setSkinId(Long skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the cityNameAO
	 */
	public String getCityNameAO() {
		return cityNameAO;
	}

	/**
	 * @param cityNameAO the cityNameAO to set
	 */
	public void setCityNameAO(String cityNameAO) {
		this.cityNameAO = cityNameAO;
	}

	/**
	 * @return the utcOffsetCreated
	 */
	public String getUtcOffsetCreated() {
		return utcOffsetCreated;
	}

	/**
	 * @param utcOffsetCreated the utcOffsetCreated to set
	 */
	public void setUtcOffsetCreated(String utcOffsetCreated) {
		this.utcOffsetCreated = utcOffsetCreated;
	}

	public Long getStateId() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}

	/**
	 * @return the opposite of contactBySms
	 * @throws Exception
	 */
	public boolean isContactBySmsNot() throws Exception {
		 User user = ApplicationData.getUserFromSession();
		 if ( user.getIsContactBySMS() == 1 ) {
			 return false;
		 }
		 else {
			 return true;
		 }
	}

	/**
	 * @param contactBySms the opposite of contactBySms to set
	 */
	public void setContactBySmsNot(boolean contactBySmsNot) {
		this.contactBySms = !contactBySmsNot;
	}

	/**
	 * @return the opposite of contactBySms
	 * @throws Exception
	 */
	public boolean isContactByEmailNot() throws Exception {
		 User user = ApplicationData.getUserFromSession();
		 if ( user.getIsContactByEmail() == 1 ) {
			 return false;
		 }
		 else {
			 return true;
		 }
	}

	/**
	 * @param contactBySms the opposite of contactBySms to set
	 */
	public void setContactByEmailNot(boolean contactByEmailNot) {
		this.contactByEmail = !contactByEmailNot;
	}

	/**
	 * @return the landPhoneCode
	 */
	public String getLandPhoneCode() {
		return landPhoneCode;
	}

	/**
	 * @return the PhoneCode by countryId of the user
	 * @throws Exception
	 */
	public String getUserPhoneCode() throws Exception {
    	FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData a = (ApplicationData)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		User user = ApplicationData.getUserFromSession();
		String code = a.getCountryPhoneCodes(String.valueOf(user.getCountryId()));
		return code;
	}

	/**
	 * @param landPhoneCode the landPhoneCode to set
	 */
	public void setLandPhoneCode(String landPhoneCode) {
		this.landPhoneCode = landPhoneCode;
	}

	/**
	 * @return the mobilePhoneCode
	 */
	public String getMobilePhoneCode() {
		return mobilePhoneCode;
	}

	/**
	 * @param mobilePhoneCode the mobilePhoneCode to set
	 */
	public void setMobilePhoneCode(String mobilePhoneCode) {
		this.mobilePhoneCode = mobilePhoneCode;
	}

	/**
	 * @return the landUserPhoneCode
	 * @throws Exception
	 */
	public String getLandUserPhoneCode() throws Exception {
		landUserPhoneCode = getUserPhoneCode();
		return landUserPhoneCode;
	}

	/**
	 * @param landUserPhoneCode the landUserPhoneCode to set
	 */
	public void setLandUserPhoneCode(String landUserPhoneCode) {
		this.landUserPhoneCode = landUserPhoneCode;
	}

	/**
	 * @return the mobileUserPhoneCode
	 * @throws Exception
	 */
	public String getMobileUserPhoneCode() throws Exception {
		mobileUserPhoneCode = getUserPhoneCode();
		return mobileUserPhoneCode;
	}

	/**
	 * @param mobileUserPhoneCode the mobileUserPhoneCode to set
	 */
	public void setMobileUserPhoneCode(String mobileUserPhoneCode) {
		this.mobileUserPhoneCode = mobileUserPhoneCode;
	}

	/**
	 * Check if need to display tax form link or not
	 * 		in balance page
	 * @return
	 * 		true - need to display link to income tax form
	 */
	public boolean isNeedToShow() {
		//User user = ApplicationData.getUserFromSession();
		Calendar gc = Calendar.getInstance();
		if ( gc.get(Calendar.MONTH) == Constants.TAX_FORM_DISPLAY_MONTH ) {
			return true;
		}

		return false;
	}

	/**
	 * Removing leading zero
	 * @param phone
	 * @return
	 */
	public String handleLeadingZero(String phone) {
		String newPhone = phone;
		if ( !CommonUtil.isParameterEmptyOrNull(phone) && phone.indexOf("0") == 0 ) {   // leading zero (index 0)
			newPhone = phone.substring(1);
		}
		return newPhone;
	}

	public long getContactId() {

		return contactId;
	}

	public void setContactId(long contactId) {
		this.contactId = contactId;
	}

	/**
	 * This is the function thet checks whether the user is allowed to change his personal details
	 * (in case he entered the right passwod) and by using it in 'personal' page we know whether
	 * to redisplay the request for password or the details of the user.
	 * @throws Exception
	 */
	public boolean isValidatePass() throws Exception {
		User user = ApplicationData.getUserFromSession();
		return user.getChangeDetailsPassValidated();
	}

	/**
	 * Validating the user's password before allowing him to change his personal details
	 * @throws Exception
	 */
	public void validatePass(FacesContext context, UIComponent comp, Object value) throws Exception {

		User user = ApplicationData.getUserFromSession();
		boolean res = UsersManager.validatePass(user, (String)value);

		user.setChangeDetailsPassValidated(res);

		if(!res){
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.personal.wrongPass", null));
			throw new ValidatorException(msg);
		}
	}

	public String acceptTerms() throws Exception{
		if (terms){
			User user = ApplicationData.getUserFromSession();
			user.setAcceptedTerms(true);
			UsersManager.updateAcceptTerms(user.getId());
			
			//ET Regulation after terms and user no FD
			if(user.isEtrader()){
				if(TransactionsManager.isFirstPossibleDepositExcludeBonus(user.getId())){
					if(regularReportMail) {
						UserRegulation userRegulationET = new UserRegulation();
						userRegulationET.setUserId(user.getId());
						userRegulationET.setRegularReportMail(regularReportMail);
						UserRegulationManager.updateRegularReportMail(userRegulationET);
					}
					return Constants.NAV_FIRST_DEPOSIT;
				}
			}
			
			if (CommonUtil.isParameterEmptyOrNull(from)) {
				return Constants.NAV_MAIN;
			} else {
				if(user.isEtrader()) {
					if (from.equals("indices")) {
						return Constants.NAV_ET_INDICES;
					} else if(from.equals("oneTouch")){
						return Constants.NAV_ET_ONE_TOUCH;
					} else if(from.equals("stocks")) {
						return Constants.NAV_ET_STOCKS;
					} else if(from.equals("currencies")) {
						return Constants.NAV_ET_CURRENCIES;
					} else if(from.equals("commodities")) {
						return Constants.NAV_ET_COMMODITIES;
					} else if(from.equals("foreignstocks")) {
						return Constants.NAV_ET_FOREIGNSTOCKS;
					} else if(from.equals("oneTouch")) {
						return Constants.NAV_ET_ONE_TOUCH;
					} else if(from.equals("optionPlus")) {
						return Constants.NAV_ET_OPTION_PLUS;
					} else if(from.equals("pages")) {
						return Constants.NAV_OTHER_DEPOSIT;
					} if(from.equals("newAppPage")) {
						return Constants.NAV_ET_NEW_APP_PAGE;
					} else {
						return Constants.NAV_MAIN;
					}
				} else {
					UserRegulation ur = new UserRegulation();
					if (ur.isUserInDepositStep()) {
						// user is regulated, not suspended and in deposit step
						return Constants.NAV_USER_QUESTIONNAIRE;
					} else if (from.equals("tradePage")) {
						return Constants.NAV_TRADE_PAGE;
					} else if(from.equals("appPage")) {
						return Constants.NAV_APP_PAGE;
					} else if (from.equals("pages")){
						return Constants.NAV_OTHER_DEPOSIT;
					} else if (from.equals("oneTouch")){
						return Constants.NAV_AO_ONE_TOUCH;
					} else if (from.equals("optionPlus")){
						return Constants.NAV_AO_OPTION_PLUS;
					} else if (from.equals("depositMenu")){
						return Constants.NAV_DEPOSIT_MENU;
					} else if (from.equals("API")){
						return "/API/pages/deposit.xhtml";						
					} else {
						return Constants.NAV_MAIN;
					}
				}
			}
		}else{
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					CommonUtil.getMessage("error.register.terms", null),null);
			context.addMessage("termsForm:terms", fm);
			return null;
		}
	}

	public boolean fillContextWithErrorMessages() {
		// TODO Auto-generated method stub
		return false;
	}
	
	/**
	 * @return the pageSection
	 */
	public int getPageSection() {
		return pageSection;
	}

	/**
	 * @param pageSection the pageSection to set
	 */
	public void setPageSection(int pageSection) {
		this.pageSection = pageSection;
	}

	/**
	 * Mobile registration
	 * @return
	 * @throws Exception
	 */
	public String nextStepReg() throws Exception {
		pageSection++;
		if (pageSection > MOBILE_SECTION3) {
            String nav = insertUser();
            if (null != nav) {  // success
                pageSection = 1;
            }
			//TODO: add clear form properties after the insert
			return nav;
		} else {
			return null;
		}
	}

	public String back() {
		pageSection--;
		return null;
	}

	public boolean isAgreementMode() {
		return agreementMode;
	}

	public void setAgreementMode(boolean agreementMode) {
		this.agreementMode = agreementMode;
	}

	public String setAgreementModeTrue() {
		setAgreementMode(true);
		return null;
	}

	public String setAgreementModeFalse() {
		setAgreementMode(false);
		return null;
	}

	public boolean isXmassBonusTime(){
		return UsersManager.isXmassBonusTime(skinId.intValue());
	}

	/**
	 * @return the birthDayDate
	 */
	public Date getBirthDayDate() {
		return birthDayDate;
	}

	/**
	 * @param birthDayDate the birthDayDate to set
	 */
	public void setBirthDayDate(Date birthDayDate) {
		this.birthDayDate = birthDayDate;
	}

	/**
	 * @return the receiveUpdate
	 */
	public boolean isReceiveUpdate() {
		return receiveUpdate;
	}

	/**
	 * @param receiveUpdate the receiveUpdate to set
	 */
	public void setReceiveUpdate(boolean receiveUpdate) {
		this.receiveUpdate = receiveUpdate;
	}

	public boolean getTempWCApproval() {
		return tempWCApproval;
	}

	public void setTempWCApproval(boolean tempWCApproval) {
		this.tempWCApproval = tempWCApproval;
	}

	/**
	 * @return the marketingTraking
	 */
	public boolean isMarketingTracking() {
		return marketingTracking;
	}

	/**
	 * @param marketingTraking the marketingTraking to set
	 */
	public void setMarketingTracking(boolean marketingTracking) {
		this.marketingTracking = marketingTracking;
	}

	public String getMobilePhoneField() {
		return mobilePhoneField;
	}

	public String getEmailField() {
		return emailField;
	}
	
	public long getFingerPrint() {
		return fingerPrint;
	}

	public void setFingerPrint(long fingerPrint) {
		this.fingerPrint = fingerPrint;
	}

	/**
	 * @return the platformId
	 */
	public int getPlatformId() {
		return platformId;
	}

	/**
	 * @param platformId the platformId to set
	 */
	public void setPlatformId(int platformId) {
		this.platformId = platformId;
	}

	public boolean isRegularReportMail() {
		return regularReportMail;
	}

	public void setRegularReportMail(boolean regularReportMail) {
		this.regularReportMail = regularReportMail;
	}
	
}