package il.co.etrader.web.mbeans;

import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.util.Constants;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.MarketRate;
import com.anyoption.common.charts.ChartHistoryCache;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.util.OpportunityCache;
import com.anyoption.common.util.OpportunityCacheBean;

public class ChartDataForm implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 8983675967228403323L;

	private static final Logger log = Logger.getLogger(ChartDataForm.class);
    
    private long marketId;
    private long oppId;
    private OpportunityCacheBean opp;
    
    public String getFillParams() {
        long t = System.currentTimeMillis();
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> pm = fc.getExternalContext().getRequestParameterMap();
        if (null != pm.get("marketId")) {
            try {
                marketId = Long.parseLong((String) pm.get("marketId"));
            } catch (Exception e) {
                // don't log the stack. Google is dumb and passing "undefined" for params
                log.warn("Can't parse marketId - " + e.getMessage());
            }
        }
        if (null != pm.get("oppId")) {
            try {
                oppId = Long.parseLong((String) pm.get("oppId"));
                if (oppId != 0) {
                    ServletContext sc = (ServletContext) fc.getExternalContext().getContext();
                    OpportunityCache oc = (OpportunityCache) sc.getAttribute(Constants.OPPORTUNITY_CACHE);
                    opp = oc.getOpportunity(oppId);
                }
            } catch (Exception e) {
                // don't log the stack. Google is dumb and passing "undefined" for params
                log.warn("Problem with the opp - " + e.getMessage());
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("fillParams - marketId: " + marketId + " oppId: " + oppId + " time to fill: " + (System.currentTimeMillis() - t));
        }
        return "";
    }
    
    public List<MarketRate> getHistory() {
        log.debug("get chart history for: " + marketId);
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest)fc.getExternalContext().getRequest();
        ServletContext sc = (ServletContext) fc.getExternalContext().getContext();
        ChartHistoryCache chc = (ChartHistoryCache) sc.getAttribute("chartHistoryCache");
        Map<SkinGroup, List<MarketRate>> ratesMap = chc.getMarketHistory(marketId);
        List<MarketRate> l = null;
        if (ratesMap != null) {
        	l = ratesMap.get(ApplicationData.getSkinById(ApplicationData.getSkinId(req.getSession(), req)).getSkinGroup());
        }
        log.debug("return rates: " + (null != l ? l.size() : 0));
        return l;
    }

    public OpportunityCacheBean getOpp() {
        return opp;
    }
    
    public long getMarketDecimalPoint() {
        Market m = ApplicationData.getMarkets().get(marketId);
        if (null != m) {
            return m.getDecimalPoint();
        } else {
            return 0;
        }
    }
}