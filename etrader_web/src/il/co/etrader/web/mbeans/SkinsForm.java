package il.co.etrader.web.mbeans;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Locale;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Skin;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.util.Constants;
import il.co.etrader.web.util.FormBase;

public class SkinsForm extends FormBase implements Serializable{

	private static final Logger logger = Logger.getLogger(SkinsForm.class);

    private String skinId;

    public SkinsForm() throws SQLException{

    }

    /**
     * Change skin id from skins menu
     * @return
     * 		navigate to the requested page with new Locale
     * 		and set markets list
     * @throws SQLException
     * @throws IOException
     */
	public String changeSkin() throws SQLException, IOException {

        FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);

		String currentSkinId = (String)context.getExternalContext().getSessionMap().get(Constants.SKIN_ID);
		if ( this.skinId.equals(currentSkinId) ) {  // same skinId in session
			return null;
		}

		Locale l = ap.createLocaleBySkinId(Long.valueOf(this.skinId), true, null, null, null);

		// set markets list
		MarketsListForm ml = (MarketsListForm)context.getApplication().createValueBinding(Constants.BIND_MARKETS_LIST_FORM).getValue(context);
		ml.updateList();
		Markets0100ListForm ml0100 = (Markets0100ListForm)context.getApplication().createValueBinding(Constants.BIND_MARKETS_0100_LIST_FORM).getValue(context);
		ml0100.updateList();

        String prefix = l.getLanguage();
        try {
            prefix = ApplicationData.getLinkLocalePrefix(request.getSession(), request, response);
        } catch (Exception e) {
            logger.error("Can't get language.", e);
        }

		// In case that we are in pages that related just German skin and we change to other skin - redirect to home page
		if (null != l &&
				isJustGermanSkinPages(request) &&
				((!this.skinId.equals(String.valueOf(Skin.SKIN_GERMAN))) &&
						currentSkinId.equals(String.valueOf(Skin.SKIN_GERMAN))) )  {
			//response.sendRedirect(ap.getHomePageUrl() + "jsp/index.jsf?s=" + this.skinId);
			response.sendRedirect(ap.getHomePageUrlWithoutLastSlash() +
					PageNames.getPageNameStatic(prefix, "/jsp/index.jsf") + "?s=" + this.skinId);
			context.responseComplete();
			return null;
		}

		// note: we have savsState in lastLevels form, then after redirect we will not create new form(jsf navigation solved it)
		if (null != l &&
				request.getRequestURL().toString().indexOf(Constants.LAST_LEVELS_PAGE) == -1 &&
				request.getRequestURL().toString().indexOf(Constants.LAST_LEVELS_ONE_TOUCH_PAGE) == -1) {

			String reloadUrl = request.getRequestURL().toString();
			String uri = request.getRequestURI();
			String reloadURLWithoutURI = request.getRequestURL().toString();
	        if (!request.getContextPath().equals("") && uri.startsWith(request.getContextPath())) {
	            uri = uri.substring(request.getContextPath().length());
	        }
			String prettyURL = PageNames.getPageNameStatic(prefix, uri);
			if (null != prettyURL) {
				reloadURLWithoutURI = reloadURLWithoutURI.replace(request.getRequestURI(), "");
				reloadUrl = reloadURLWithoutURI + request.getContextPath() + prettyURL;
				logger.debug("Redirecting to: " + reloadUrl + " after skin change");
			}
			response.sendRedirect(reloadUrl);
		} else if ( null != l ) {
			return Constants.NAV_LAST_LEVELS;
		}

		return null;
	}

	/**
	 * @return the skinId
	 */
	public String getSkinId() {
		return skinId;
	}

	/**
	 * @param skin the skinId to set
	 */
	public void setSkinId(String skinId) {
		this.skinId = skinId;
	}

	/**
	 * Check if we are in GermanSkin pages that we dont have in other skins
	 * @param request
	 * @return
	 */
	public boolean isJustGermanSkinPages(HttpServletRequest request) {
		StringBuffer requestUrl = request.getRequestURL();
		if (requestUrl.indexOf("optionshandel.jsf") > -1 ||
				requestUrl.indexOf("digitale_optionen.jsf") > -1 ||
				requestUrl.indexOf("binaere_optionen.jsf") > -1 ||
				requestUrl.indexOf("day_trading.jsf") > -1 ||
				requestUrl.indexOf("online_trading.jsf") > -1 ||
				requestUrl.indexOf("optionen_lexikon.jsf") > -1 ||
				requestUrl.indexOf("forex.jsf") > -1 ||
				requestUrl.indexOf("indizes.jsf") > -1 ) {

			return true;
		}
		return false;
	}

	public String getChangeSkinUrl(String prefix) {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String reloadUrl = request.getRequestURL().toString();
		String uri = request.getRequestURI();
		String reloadURLWithoutURI = request.getRequestURL().toString();
        if (!request.getContextPath().equals("") && uri.startsWith(request.getContextPath())) {
            uri = uri.substring(request.getContextPath().length());
        }
		String prettyURL = PageNames.getPageNameStatic(prefix, uri);
		String prettyURLParam = getParamFromPretty(prettyURL);
		prettyURL = prettyURL + prettyURLParam;
		if (null != prettyURL) {
			reloadURLWithoutURI = reloadURLWithoutURI.replace(request.getRequestURI(), "");
			reloadUrl = reloadURLWithoutURI + request.getContextPath() + prettyURL;
			reloadUrl = getSubDomain(prefix);
			if (prefix.equals("en")) {
				reloadUrl += "/en/homepage";
			}
			reloadUrl += prettyURL;
		}
		return reloadUrl;
	}
	
	public String getParamFromPretty (String prettyURL) {
		if (prettyURL != null && prettyURL.equalsIgnoreCase("/assets/")) {
			FacesContext context = FacesContext.getCurrentInstance();
			HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
			return request.getParameter("assetName");
		}
		return "";
	}

    public String getSubDomain(String skin) {
    	String res = "";
    	if (skin.equals("en")) {
    		res = CommonUtil.getProperty("homepage.url");
    	} else {
    		String subDomain = ApplicationData.getSubDomainBySkin(skin, false);
    		if (CommonUtil.isParameterEmptyOrNull(subDomain)) {
    			subDomain = ApplicationData.getSubDomainBySkin(skin, true);
    		}
    		// FIXME: Tony - make this one work properly
            if (skin.equals(Skin.LOCALE_SUBDOMAIN_ITALIAN)) {
                subDomain = ApplicationData.getSkinById(Skin.SKIN_REG_IT).getSubdomain();
            }
    		if(!CommonUtil.isParameterEmptyOrNull(subDomain)) {
    			res = CommonUtil.getProperty("homepage.url").replace("www", subDomain);
    			if (skin.equals(Skin.LOCALE_SUBDOMAIN_ITALIAN)) {
    			    res = res.replace(Constants.HOST_ANYOPTION, Constants.HOST_ANYOPTION_IT);
    			}
    		} else {
    			res = CommonUtil.getProperty("homepage.url");
    		}
    		
    	}
    	return res.substring(0, res.length() - 1);
    }
}