package il.co.etrader.web.mbeans;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.anyoption.common.beans.Investment;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.InvestmentFormatter;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.InvestmentsManager;
import il.co.etrader.web.util.Constants;

public class ShowOff {

	private List<Investment>list = null;
	private long rowNum = 100;
	private boolean showOffLocale = false;
	private String langCode = null;
	private boolean shortText = false;
	private long skinId = -1;
	private String textShowOff;
	private String nameShowOff;
	private long countryId;


	public void updateList() throws SQLException {
		list = InvestmentsManager.getShowOffInvestmentsList(rowNum, showOffLocale, langCode, skinId, shortText);
	}

	public List<Investment> getList() throws SQLException {
		if (null == list)	{
			updateList();
		}
		return list;
	}

	public void setList(List<Investment> list) {
		this.list = list;
	}

	public int getSize() {
		if (null == list) {
			return 0;
		}
		return list.size();
	}
	
	public String getFromParameter() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        String param = req.getParameter("i");
        if (null != param) {
        	long l = Long.valueOf(param);
        	return InvestmentFormatter.getShowOffFacebookText(InvestmentsManager.getShowOffInvestmentByInvId(l));
        }

        return null;
	}

	public String getFromParameterShowOff() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        String param = req.getParameter("so");
        if (null != param) {
        	long l = Long.valueOf(param);
        	return InvestmentFormatter.getShowOffFacebookText(InvestmentsManager.getShowOffInvestmentByShowOffIdForFacebook(l, true));
        }

        return null;
	}

	public String getRssItems() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
		String localeParam = req.getParameter(Constants.SH_RSS_LOCALE);
		String linkParam = req.getParameter(Constants.SH_RSS_LINK);
		String numParam = req.getParameter(Constants.SH_RSS_NUMBER);
		String shortParam = req.getParameter(Constants.SH_RSS_SHORT);
		String skinParam = req.getParameter(Constants.SH_RSS_SKIN);
		if (null != localeParam) {
			if (localeParam.equals("")) {
				showOffLocale = true;
			} else {
				langCode = localeParam;
			}
		}
		if (null == linkParam) {
			linkParam = "";
		} else {
			linkParam = " " + linkParam;
		}
		if (null == numParam) {
			rowNum = 10;
		} else {
			try {
				rowNum = Integer.valueOf(numParam);
			} catch (Exception e) {
				rowNum = 10;
			}
		}
		if (null != shortParam) {
			try {
				shortText = Boolean.valueOf(shortParam);
			} catch (Exception e) {
				// remains false
			}
		}
		if (null != skinParam) {
			try {
				skinId = Long.valueOf(skinParam);
			} catch (Exception e) {
				// remains -1
			}
		}
		String linkURL = ap.getHomePageUrl() + "jsp/showOff.jsf";
		String rss = "";
		updateList();
		for (int i=0; i<list.size(); i++) {
			SimpleDateFormat sd = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z");
			rss +=
				"<item>\n" +
				"	<title>" + list.get(i).getShowOffName() + "</title>\n" +
				"	<description>" + InvestmentFormatter.getShowOffText(list.get(i)) + linkParam + "</description>\n" +
				"	<link>" + linkURL + "</link>\n" +
				"	<guid isPermaLink=\"false\">" + list.get(i).getId() + "</guid>\n" +
				"	<pubDate>" + sd.format(list.get(i).getShowOffTime()) + "</pubDate>\n" +
				"</item>\n";
		}

		rss = rss.replaceAll("&", "&amp;");

		return rss;
	}

	public String getTextShowOff() {
		return textShowOff;
	}

	public void setTextShowOff(String textShowOff) {
		this.textShowOff = textShowOff;
	}

	public String getNameShowOff() {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		Investment invest = ap.isShowOff(rowNum, showOffLocale, langCode, shortText);
		// get name and text by skin.
		String dateDistance = null;
		Locale locale = null;
		if (null != langCode) {
    		try {
    			locale = new Locale(langCode);
    		} catch (Exception e) {
				// do nothing!
			}
		}
    	if (null == locale) {
    		dateDistance = CommonUtil.getDateDistance(invest.getTimeCreated(), invest.getTimeSettled(), null);
    	} else {
    		dateDistance = CommonUtil.getDateDistance(invest.getTimeCreated(), invest.getTimeSettled(), locale);
    	}
    	String[] tempStringArr = CommonUtil.getDateAndTimeShowOffAsArray(invest.getTimeCreated(),invest.getUtcOffsetCreated());
    	//tempstringArr[0] = date ,tempstringArr[2] = time
    	String[] strArr = new String[] {
    			CommonUtil.removeFractionFromAmount(CommonUtil.displayAmount(invest.getAmount(), invest.getCurrencyId())),
    			invest.getTypeName(),
    			invest.getMarketName(),
    			tempStringArr[0],
    			CommonUtil.removeFractionFromAmount(InvestmentFormatter.getRefundTxt(invest)),
    			dateDistance,
    			tempStringArr[2]};
    	String showOffMessegeKey = null;
    	if (shortText) {
    		showOffMessegeKey = "showOff.twitter.sentence.";
    	} else {
    		showOffMessegeKey = "showOff.sentence.";
    	}
    	String showOffText;
    	if (null == locale) {
    		showOffText = CommonUtil.getMessage(invest.getLastShowOffMessegeKey(), strArr);
    	} else {
    		showOffText = CommonUtil.getMessage(invest.getLastShowOffMessegeKey(), strArr, locale);
    	}
    	textShowOff = showOffText;
    	nameShowOff = invest.getShowOffName();
		return nameShowOff;
	}

	public void setNameShowOff(String nameShowOff) {
		this.nameShowOff = nameShowOff;
	}

	public String getCountryId() {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		Investment invest = ap.isShowOff(rowNum, showOffLocale, langCode, shortText);
		// get name and text by skin.
		String dateDistance = null;
		Locale locale = null;
		if (null != langCode) {
    		try {
    			locale = new Locale(langCode);
    		} catch (Exception e) {
				// do nothing!
			}
		}
    	if (null == locale) {
    		dateDistance = CommonUtil.getDateDistance(invest.getTimeCreated(), invest.getTimeSettled(), null);
    	} else {
    		dateDistance = CommonUtil.getDateDistance(invest.getTimeCreated(), invest.getTimeSettled(), locale);
    	}
    	String[] tempStringArr = CommonUtil.getDateAndTimeShowOffAsArray(invest.getTimeCreated(),invest.getUtcOffsetCreated());
    	//tempstringArr[0] = date ,tempstringArr[2] = time
    	String[] strArr = new String[] {
    			CommonUtil.removeFractionFromAmount(CommonUtil.displayAmount(invest.getAmount(), invest.getCurrencyId())),
    			invest.getTypeName(),
    			invest.getMarketName(),
    			tempStringArr[0],
    			CommonUtil.removeFractionFromAmount(InvestmentFormatter.getRefundTxt(invest)),
    			dateDistance,
    			tempStringArr[2]};
    	String showOffMessegeKey = null;
    	if (shortText) {
    		showOffMessegeKey = "showOff.twitter.sentence.";
    	} else {
    		showOffMessegeKey = "showOff.sentence.";
    	}
    	String showOffText;
    	if (null == locale) {
    		showOffText = CommonUtil.getMessage(invest.getLastShowOffMessegeKey(), strArr);
    	} else {
    		showOffText = CommonUtil.getMessage(invest.getLastShowOffMessegeKey(), strArr, locale);
    	}
    	textShowOff = showOffText;
    	nameShowOff = invest.getShowOffName();
    	countryId = invest.getCountryId();
		return String.valueOf(countryId);
	}
	
	public String getNameShowOffLive() {
		return nameShowOff;
	}

	public String getShowOffText(Investment i) throws SQLException {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getShowOffText(i);
	}
}