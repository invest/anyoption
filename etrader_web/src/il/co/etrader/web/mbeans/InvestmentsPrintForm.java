package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.jfree.util.Log;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.managers.InvestmentsManagerBase;
import com.anyoption.common.managers.MarketsManagerBase;

import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.InvestmentFormatter;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;

/**
 * @author kirilim
 *
 */
public class InvestmentsPrintForm implements Serializable {

	/**
	 * 
	 */
	private static final Logger logger = Logger.getLogger(InvestmentsPrintForm.class);
	private static final long serialVersionUID = 2112774534758812539L;
	private boolean isBinary;
	private Date from;
	private Date to;

	public InvestmentsPrintForm() {
		Map<String, String> requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		isBinary = (Integer.valueOf(requestMap.get("isBinary")) == 1) ? true : false;
		from = new Date(Long.parseLong(requestMap.get("from")));
		if (!isBinary) {
			to = new Date(Long.parseLong(requestMap.get("to")));
		}
	}
	
	public String getPrintTopText() {
		Locale locale = getLocale();		
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		String[] params = new String[2];
		params[0] = sdf.format(from);
		if (!isBinary && !from.equals(to)) {
			params[1] = sdf.format(to);
			return CommonUtil.getMessage("investments.print.top.text", params, locale);
		} else {
			return CommonUtil.getMessage("investments.print.top.text0100", params, locale);
		}
	}
	
	public String getBinary0100BoughtText(String timeCreated) {
		Locale locale = getLocale();
		return CommonUtil.getMessage("investments.print.top.binary0100.bought", new String[] {timeCreated}, locale);
	}
	
	public String getBinary0100SoldText(String timeCreated) {
		Locale locale = getLocale();
		return CommonUtil.getMessage("investments.print.top.binary0100.sold", new String[] {timeCreated}, locale);
	}
	
	private Locale getLocale() {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = context.getApplication().evaluateExpressionGet(context, Constants.BIND_APPLICATION_DATA, ApplicationData.class);
		Locale locale = null;
		try {
			locale = ap.getUserLocale();
		} catch (Exception e) {
			Log.error("Could not get the user locale. Taking a default one", e);
			locale = new Locale(ConstantsBase.LOCALE_DEFAULT);
		}
		
		return locale;
	}

	public boolean isOneTouchInv(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.getIsOneTouchInv(i);
	}

	public boolean isRolledUp(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.isRolledUp(i);
	}

	public boolean isGmBought(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.isGmBought(i);
	}

	public String getRefundUpTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(InvestmentsManagerBase.getRefundUp(i), i.getCurrencyId());
	}

	public String getRefundWithBonusTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getRefundWithBonusTxt(i);
	}

	public String getRefundDownTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(InvestmentsManagerBase.getRefundDown(i), i.getCurrencyId());
	}

	public String getClosingLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getClosingLevelTxt(i);
	}

	public String getCurrentLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCurrentLevelTxt(i);
	}

	public String getTimeEstClosingTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getTimeEstClosingTxt(i);
	}

	public String getAmountTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getAmountTxt(i);
	}

	public String getAmountMaxRiskTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getAmountMaxRiskTxt(i);
	}

	public String getAmountMaxRiskTxtExcludDecimal(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getAmountMaxRiskTxtExcludDecimal(i);
	}

	public String getMaxProfitTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getMaxProfitTxt(i);
	}

	public String getMaxProfitTxtExcludDecimal(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getMaxProfitTxtExcludDecimal(i);
	}

	public String getInvestmentsAmount(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getInvestmentsAmount(i);
	}

	public String getInvestmentsAmountExcludDecimal(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getInvestmentsAmountExcludDecimal(i);
	}

	public String getCurrentLevel0100(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCurrentLevel0100(i);
	}

	public String getClosingLevel0100(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getClosingLevel0100(i);
	}

	public boolean isSettledBeforTime(Investment i) {
		if (i == null) {
			return false;
		}
		return il.co.etrader.bl_managers.InvestmentsManagerBase.isSettledBeforTime(i);
	}

	public String getTimeCreatedTxtByFormat(Investment i, String format) {
		if (i == null) {
			return "";
		}
        return CommonUtil.getTimeFormat(i.getTimeCreated(), i.getUtcOffsetCreated(), format);
    }

	public String getTimeEstClosingTxtByFormat(Investment i, String format) {
		if (i == null) {
			return "";
		}
        return CommonUtil.getTimeFormat(i.getTimeEstClosing(), i.getUtcOffsetCreated(), format);
    }

	public String getPrintBinary0100Event(Investment i) {
		if (i == null) {
			return "";
		}
		long skinId = 0L;
		User user = null;
		try {
			user = ApplicationData.getUserFromSession();
		} catch (Exception e) {
			logger.error("Cannot get user from session! ", e);
		}
		if (user != null) {
			skinId = user.getSkinId();
		} else {
			skinId = Skins.SKIN_REG_EN;
		}
		String result = null;
		String[] params = null;
		params = new String[3];
		params[0] = MarketsManagerBase.getMarketName(skinId, i.getMarketId());
		params[1] = CommonUtil.formatLevelByDecimalPoint(i.getOppCurrentLevel(), i.getDecimalPoint());
		params[2] = CommonUtil.getTimeFormat(i.getTimeEstClosing(), i.getUtcOffsetCreated(), "HH:mm");
		String messageKeyAbove = "opportunity.above";
		String messageKeyBelow = "opportunity.below";
		if (CommonUtil.getLocaleLanguage().contains(ConstantsBase.ETRADER_LOCALE) && params[0].toUpperCase().contains("DAX")) {
			messageKeyAbove = "opportunity.dax.above";
			messageKeyBelow = "opportunity.dax.below";
		}
		if (i.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_ABOVE) {
			result = CommonUtil.getMessage(messageKeyAbove, params);
		}
		if (i.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_BELOW) {
			result = CommonUtil.getMessage(messageKeyBelow, params);
		}
		return result;
	}
}