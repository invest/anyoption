package il.co.etrader.web.mbeans;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.EmailValidator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.CountryManagerBase;

import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.SendContactEmail;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.ContactsManager;
import il.co.etrader.web.bl_managers.UsersManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;

public class ContactUsForm implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2434624250561919321L;

	private static final Logger logger = Logger.getLogger(ContactUsForm.class);

    private String userName=null;
    private String firstName;
    private String lastName;
    private String email;
    private String mobilePhonePrefix;
    private String mobilePhone;
    private String contactIssue;
    private String comments;
    private Long countryId;
    private String language;
    private String supportPhone;
    private String supportFax;
    private ArrayList<SelectItem> countries;
    private Skins skin;
	private Long combId;
	private String dynamicParameter;
	private String aff_sub1;
	private String aff_sub2;
	private String aff_sub3;

	private String landMobilePhoneCode;
	private String landMobilePhone;
	private boolean reciveUpdates = true;
	
	private final String emailField;
	private final String firstNameField;
	private final String lastNameField;
	private final String mobilePhoneField;
	
	private Long mobileCountryId;

    public ContactUsForm() throws SQLException {

		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
		Locale locale = ap.getUserLocale();

		countries = new ArrayList<SelectItem>();
		contactIssue = Constants.EMPTY_STRING;
    	// default values for anyoption
    	skin = ap.getSkinById(ap.getSkinId());
    	String countryId = null;
    	String countryIdStr = null;

    	if (!CommonUtil.isHebrewSkin(skin.getId())) {

	    	boolean takeFromSkin = false;
    		String code = "";

    		countryId = ap.getRealCountryId();
    		if ( null != countryId && !countryId.equals("0")) {  // found country
				code = ap.getCountryPhoneCodes(countryId);
				this.countryId = Long.valueOf(countryId);
				this.mobileCountryId = Long.valueOf(countryId);
				if ( null == code || code.length() == 0 ) {  // phone code not exists
						takeFromSkin = true;
				}
    		} else {
    			takeFromSkin = true;
    		}

    		if ( takeFromSkin ) {   // problem to extract country phone code from ip
        		countryIdStr = String.valueOf(skin.getDefaultCountryId());
    	    	code = ap.getCountryPhoneCodes(countryIdStr);
    	    	this.countryId = new Long(0);
    	    	this.mobileCountryId = new Long(0);
    		}

    		if(req.getRequestURL().indexOf("minisite") > -1){
    			firstName =  CommonUtil.getMessage("contact.firstname", null, locale);
    			lastName =  CommonUtil.getMessage("contact.lastname", null, locale);
    			email =  CommonUtil.getMessage("contact.email", null, locale);
    			comments = CommonUtil.getMessage("contact.comments", null, locale);
    		}

    		mobilePhone = code;   // put the code in the field
    		landMobilePhoneCode = code;

    		// get language
    		language = ap.getUserLocale().getDisplayLanguage();

    	}

    	// get combination name
		this.combId = UsersManager.getCombinationId(req);
		logger.log(Level.DEBUG, "get combinationId for email = " + combId);
		if ( null == combId ) {
			combId = skin.getDefaultCombinationId();
			logger.log(Level.DEBUG, "combId is null, take default value from skin: " + combId);
		}

		dynamicParameter = UsersManager.getDynamicParam(req);
		aff_sub1 = CommonUtil.getRequestParameter(req, Constants.AFF_SUB1);
		aff_sub2 = CommonUtil.getRequestParameter(req, Constants.AFF_SUB2);
		aff_sub3 = CommonUtil.getRequestParameter(req, Constants.AFF_SUB3);

    	ArrayList<SelectItem> countriesFromApp = ap.getSupportCountries();
    	ArrayList<SelectItem> countTemp = new ArrayList<SelectItem>();
    	countTemp.add(new SelectItem(new Long(0),""));

    	for(SelectItem si:countriesFromApp){
			Long value = (Long)si.getValue();
			if (value != Constants.COUNTRY_ID_IL && value != Constants.COUNTRY_ID_IRAN){ // remove Israel from anyoption
				countTemp.add(new SelectItem(si.getValue(), CommonUtil.getMessage(si.getLabel(),null) ));
			}
    	}
    	Collections.sort(countTemp,new AlphaComparator());
    	this.setCountries(countTemp);

    	if (ap.isUsSkins()){
    		this.countryId = ConstantsBase.COUNTRY_ID_US;
    		this.mobileCountryId = ConstantsBase.COUNTRY_ID_US;
    	}

    	if(req.getRequestURL().indexOf("quickStart") > -1){
			firstName = CommonUtil.getMessage("register.firstname.caps", null);
			lastName = CommonUtil.getMessage("register.lastname.caps", null);
			email = CommonUtil.getMessage("register.email.caps", null);
			landMobilePhone = CommonUtil.getMessage("register.phone.caps", null);
		}

    	// not in use here, so we initialze them with nulls
    	emailField = null;
    	firstNameField = null;
    	lastNameField = null;
    	mobilePhoneField = null;
    }

    // TODO should be changed when whole logic is done
    public ContactUsForm(String email, String emailField, String firstName, String firstNameField, String lastName, String lastNameField,
    						String countryId, String mobilePhone, String mobilePhoneField, String mobilePhonePrefix, Long skinId) {
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.landMobilePhone = mobilePhone;
		this.countryId = Long.valueOf(countryId);
		this.landMobilePhoneCode = (this.countryId != Constants.COUNTRY_ID_IL)
									? CountryManagerBase.getSortedCountriesMap(skinId).get(this.countryId).getPhoneCode()
									: mobilePhonePrefix;
		this.emailField = emailField;
		this.firstNameField = firstNameField;
		this.lastNameField = lastNameField;
		this.mobilePhoneField = mobilePhoneField;
	}

	class AlphaComparator implements Comparator<SelectItem> {

        // Comparator interface requires defining compare method.
        public int compare(SelectItem a, SelectItem b) {
            //	  Sort countries in alphabetical order considering user's Locale
            //
        	String aLabel = a.getLabel();
        	String bLabel = b.getLabel();
        	int returnVal=0;

        	FacesContext context = FacesContext.getCurrentInstance();
        	ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);

        	try{
        		Collator collator = Collator.getInstance(ap.getUserLocale());
        		returnVal = collator.compare(aLabel, bLabel);
        	}catch(SQLException e){
        		logger.fatal("Error in sorting countriesList :"+e.getMessage());
        	}

        	return returnVal;
        }
    }


   /**
	 * This method will send the contact email to user
	 */
	public String sendContactEmail() throws Exception{

		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		String tempMobile = this.mobilePhone;
		Contact contactReq = new Contact();
		boolean fromDownloadPage = false;
		User user = null;
		

		 if (firstName == null) { // logged-in

			 user = ApplicationData.getUserFromSession();

			 this.email = user.getEmail();
			 this.userName = user.getUserName();
			 this.firstName = user.getFirstName();
			 this.lastName = user.getLastName();
			 this.mobilePhonePrefix = user.getMobilePhonePrefix();
			 this.mobilePhone = user.getMobilePhoneSuffix();

			 if (!CommonUtil.isHebrewSkin(user.getSkinId()))  { // AO - add country code
				 String code = ap.getCountryPhoneCodes(String.valueOf(user.getCountryId()));
				 tempMobile=this.mobilePhone;
				 this.mobilePhone = code + mobilePhone;
			 }
		 } else { //user is not logged in  - need to remove phonePrefix from phone
			 if (!CommonUtil.isHebrewSkin(skin.getId()) ){
				 this.mobilePhonePrefix = ap.getCountryPhoneCodes(String.valueOf(this.countryId));
				 if(this.mobilePhonePrefix!=null){
					 if(this.mobilePhone.indexOf(this.mobilePhonePrefix) != -1 && this.mobilePhone.indexOf(this.mobilePhonePrefix) == 0) // if user has not deleted prefix from input
						 tempMobile = this.mobilePhone.substring(this.mobilePhone.indexOf(this.mobilePhonePrefix)+this.mobilePhonePrefix.length(), this.mobilePhone.length());
				 }else{
					 tempMobile = this.mobilePhone;
				 }
			 }
		 }

		contactReq.setWriterId(CommonUtil.getWebWriterId());
		contactReq.setType(Contact.CONTACT_US_TYPE);

		if (CommonUtil.isHebrewSkin(skin.getId())) {
			contactReq.setPhone(mobilePhonePrefix+mobilePhone);
			contactReq.setCountryId(Constants.ISRAEL_COUNTRY_ID);
		} else {   // for anyoption
			contactReq.setPhone(tempMobile);
			contactReq.setCountryId(countryId);
			String page = context.getViewRoot().getViewId();
			if (!CommonUtil.isParameterEmptyOrNull(page) &&  page.indexOf("appContactUs") > -1) {   // contact us page for mobile users that didn't download the application yet
				fromDownloadPage = true;
				contactReq.setWriterId(Writer.WRITER_ID_MOBILE);
				contactReq.setType(Contact.CONTACT_US_DIRECT_DOWNLOAD_MOBILE);
			}
			if (CommonUtil.isMobileUserAgent()){
				contactReq.setUserAgent(CommonUtil.getUserAgent());
			}
		}

		contactReq.setEmail(email);
		contactReq.setSkinId(ap.getSkinId());
		contactReq.setText(firstName + " " + lastName);
		contactReq.setCombId((this.combId).longValue());
		contactReq.setDynamicParameter(this.dynamicParameter);
		contactReq.setAff_sub1(aff_sub1);
		contactReq.setAff_sub2(aff_sub2);
		contactReq.setAff_sub3(aff_sub3);
		contactReq.setIp(CommonUtil.getIPAddress());
		HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
		contactReq.setDfaPlacementId(UsersManager.getDFAPlacementId(req));
		contactReq.setDfaCreativeId(UsersManager.getDFACreativeId(req));
		contactReq.setDfaMacro(UsersManager.getDFAMacro(req));

		long contactId = ContactsManager.insertContactRequest(contactReq, false, true, user, null).getId(); // for storing contactID later in cookie
		try {
			// collect the parameters we neeed for the email
			HashMap<String, String> params = new HashMap<String, String>();
			params.put(SendContactEmail.PARAM_EMAIL, email);
			params.put(SendContactEmail.PARAM_USER_NAME, userName);
			params.put(SendContactEmail.PARAM_FIRST_NAME, CommonUtil.capitalizeFirstLetters(firstName));
			params.put(SendContactEmail.PARAM_LAST_NAME, CommonUtil.capitalizeFirstLetters(lastName));
			params.put(SendContactEmail.PARAM_CONTACT_ID, String.valueOf(contactId));
			if ( CommonUtil.isHebrewSkin(skin.getId()) ) {
				params.put(SendContactEmail.PARAM_MOBILE, mobilePhonePrefix+"-"+mobilePhone);
			}
			else {   // for anyoption
				params.put(SendContactEmail.PARAM_MOBILE, mobilePhone);
			}

			// add language for the email
			contactIssue += " ,Language: ";
			if (!CommonUtil.isParameterEmptyOrNull(language)) {
				contactIssue += language;
			}
			else {
				contactIssue += Constants.CONTACT_ME_LANG_ISRAEL;
			}

			params.put(SendContactEmail.PARAM_ISSUE, contactIssue);
			params.put(SendContactEmail.PARAM_COMMENTS, comments);


			// Populate also server related params , in order to take out common functionality
			// Use params
			params.put(SendContactEmail.MAIL_FROM, email);
			params.put(SendContactEmail.MAIL_TO, CommonUtil.getProperty("contact.email", null));

			// Change subject for mobile site / mobile application / download page
			if (!CommonUtil.isHebrewSkin(skin.getId())){
				String mobileSubject = ConstantsBase.EMPTY_STRING;
				String device = ConstantsBase.EMPTY_STRING;
				if (CommonUtil.isMobile()){ // iphone or android application
					mobileSubject = Constants.MOBILE_CONTACTUS_APPLICATION_SUBJECT_ADDITION;
					if (ap.isIphoneApp() || ap.isIphone()){
						device = "iPhone";
					} else if (ap.isAndroidApp() || ap.isAndroid()){
						device = "Android";
					}
				}
				if (CommonUtil.isMobileUserAgent()){
					params.put(SendContactEmail.PARAM_DEVICE, device + " User agent: " + CommonUtil.getUserAgent());
				}
				if (fromDownloadPage) {   // contact us page for mobile users that didn't download the application yet
					mobileSubject = Constants.MOBILE_CONTACTUS_DOWNLOADPAGE_SUBJECT_ADDITION;
				}
				params.put(SendContactEmail.MAIL_SUBJECT , CommonUtil.getProperty("contact.email.subject", null) + mobileSubject);
			} else {
				params.put(SendContactEmail.MAIL_SUBJECT , CommonUtil.getProperty("contact.email.subject", null));
			}

			if ( null != skin && !CommonUtil.isHebrewSkin(skin.getId())) {
				params.put(SendContactEmail.SUCCESS_MESSAGE, CommonUtil.getMessage("contact.mail.success",null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
			} else {
				params.put(SendContactEmail.SUCCESS_MESSAGE, CommonUtil.getMessage("contact.mail.success",null));
			}

			params.put(SendContactEmail.RETURN_PAGE, Constants.NAV_EMPTY_SSL);
			SendContactEmail s= new SendContactEmail(params);
			s.start();


			FacesMessage fm=new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("contact.mail.success",null),null);
        	context.addMessage(null,fm);

        	HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			Cookie contactCookie = new Cookie(Constants.CONTACT_ID,contactId+"");
			contactCookie.setPath("/");
			contactCookie.setMaxAge(365 * 24 * 60 * 60 * 1000); // about year
			//sub domain
			String property = CommonUtil.getProperty(ConstantsBase.COOKIE_DOMAIN, null);
			if (null != property) {
				contactCookie.setDomain(property);
			}
			response.addCookie(contactCookie);
			if (fromDownloadPage){ //download application page from mobile
				return Constants.NAV_DOWNLOADPAGE_CONTACTUS;
			} else if (req.getRequestURL().indexOf("mobile_site") > -1 || req.getRequestURL().indexOf("mobile_ng") > -1 ){
				return null;
			} else if(!CommonUtil.isMobile()) { //mobile
				return Constants.NAV_EMPTY_SSL;
			} else if (req.getRequestURL().indexOf("minisite") > -1 ){
				return Constants.NAV_CONTACTUS_MINISITE;
			} else {
				return Constants.NAV_CONTACTUS_NAV;
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.fatal("Could not send email. " + e.getMessage());
		}
		return null;

	}

	public String sendContactEmailMinisite() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		Locale locale = ap.getUserLocale();
		if(firstName.equalsIgnoreCase(CommonUtil.getMessage("contact.firstname", null, locale)) ){
			String[] params = new String[1];
			params[0] = CommonUtil.getMessage("register.firstname",null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.required", params),null);
			context.addMessage("contactForm:firstName", fm);
			return null;
		}
		if(lastName.equalsIgnoreCase(CommonUtil.getMessage("contact.lastname", null, locale)) ){
			String[] params = new String[1];
			params[0] = CommonUtil.getMessage("register.lastname",null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.required", params),null);
			context.addMessage("contactForm:lastName", fm);
			return null;
		}
		if(lastName.equalsIgnoreCase(CommonUtil.getMessage("contact.lastname", null, locale)) ){
			String[] params = new String[1];
			params[0] = CommonUtil.getMessage("register.lastname",null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.required", params),null);
			context.addMessage("contactForm:lastName", fm);
			return null;
		}
		if(email.equalsIgnoreCase(CommonUtil.getMessage("contact.email", null, locale)) ){
			String[] params = new String[1];
			params[0] = CommonUtil.getMessage("register.email",null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.required", params),null);
			context.addMessage("contactForm:email", fm);
			return null;
		}
		if(!CommonUtil.isEmailValidUnix(email)){
			String[] params = new String[1];
			params[0] = CommonUtil.getMessage("register.email",null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.email", params),null);
			context.addMessage("contactForm:email", fm);
			return null;
		}
		if(CommonUtil.isParameterEmptyOrNull(mobilePhone)){
			String[] params = new String[1];
			params[0] = CommonUtil.getMessage("register.mobilephone",null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.required", params),null);
			context.addMessage("contactForm:mobilePhone", fm);
			return null;
		}
		if(!mobilePhone.matches("\\d+")){
			String[] params = new String[1];
			params[0] = CommonUtil.getMessage("register.mobilephone",null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.phone", params),null);
			context.addMessage("contactForm:mobilePhone", fm);
			return null;
		}
		if(mobilePhone.length() < 7 || mobilePhone.length() > 14){
			String[] params = new String[1];
			params[0] = CommonUtil.getMessage("register.mobilephone",null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.quick.start.mobile.phone", params),null);
			context.addMessage("contactForm:mobilePhone", fm);
			return null;
		}

		return sendContactEmail();
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobilePhonePrefix() {
		return mobilePhonePrefix;
	}

	public void setMobilePhonePrefix(String mobilePhonePrefix) {
		this.mobilePhonePrefix = mobilePhonePrefix;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getContactIssue() {
		return contactIssue;
	}

	public void setContactIssue(String contactIssue) {
		this.contactIssue = contactIssue;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the supportPhone
	 */
	public Long getCountryId() {
		return countryId;
	}

	/**
	 * @param supportPhone the supportPhone to set
	 */
	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the supportFax
	 */
	public String getSupportFax() {
		if (skin.getId() == Skin.SKIN_ETRADER){
			supportFax = CommonUtil.getMessage("contact.fax.default", null) ;
		}
		return supportFax;
	}

	/**
	 * @param supportFax the supportFax to set
	 */
	public void setSupportFax(String supportFax) {
		this.supportFax = supportFax;
	}

	/**
	 * @return the supportPhone
	 */
	public String getSupportPhone() {
		if (skin.getId() == Skin.SKIN_ETRADER){
			supportPhone = CommonUtil.getMessage("contact.phone.default", null) ;
		}
		if (skin.getId() == Skin.SKIN_CHINESE_VIP){
			supportPhone = CommonUtil.getMessage("contact.phone.default.qiquan", null);
		}
		return supportPhone;
	}

	/**
	 * @param supportPhone the supportPhone to set
	 */
	public void setSupportPhone(String supportPhone) {
		this.supportPhone = supportPhone;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "ContactUsForm ( "
	        + super.toString() + TAB
	        + "userName = " + this.userName + TAB
	        + "firstName = " + this.firstName + TAB
	        + "lastName = " + this.lastName + TAB
	        + "email = " + this.email + TAB
	        + "mobilePhonePrefix = " + this.mobilePhonePrefix + TAB
	        + "mobilePhone = " + this.mobilePhone + TAB
	        + "contactIssue = " + this.contactIssue + TAB
	        + "comments = " + this.comments + TAB
	        + " )";

	    return retValue;
	}

	public ArrayList<SelectItem> getCountries() {
		return countries;
	}

	public void setCountries(ArrayList<SelectItem> countries) {
		this.countries = countries;
	}

    /**
     * get the support start and end time with user offset
     * @return the time "xx:xx - xx:xx" with user offcet
     */
    public String getSupportTime() {
        FacesContext context = FacesContext.getCurrentInstance();
        ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
        long userSkinId = ap.getSkinId();
        return ap.getStartEndTimeWithUserOffset(ApplicationData.getSkinById(userSkinId).getSupportStartTime(), ApplicationData.getSkinById(userSkinId).getSupportEndTime());
    }

    /**
     * get the support start and end time message with user offset
     * @return the support start and end time message
     */
    public String getSupportTimeTxt() {
        FacesContext context = FacesContext.getCurrentInstance();
        ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
        Skins s = ApplicationData.getSkinById(ap.getSkinId());
        Skins sEN = ApplicationData.getSkinById(Skin.SKIN_ENGLISH);
        String[] hours = new String[4];
        hours[0] = ap.getTimeWithUserOffset(s.getSupportStartTime());
        hours[1] = ap.getTimeWithUserOffset(s.getSupportEndTime());
        hours[2] = ap.getTimeWithUserOffset(sEN.getSupportStartTime());
        hours[3] = ap.getTimeWithUserOffset(sEN.getSupportEndTime());
        return CommonUtil.getMessage("contact.working.hours", hours);
    }

	/**
	 * @return the landMobilePhone
	 */
	public String getLandMobilePhone() {
		return landMobilePhone;
	}


	/**
	 * @param landMobilePhone the landMobilePhone to set
	 */
	public void setLandMobilePhone(String landMobilePhone) {
		this.landMobilePhone = landMobilePhone;
	}


	/**
	 * @return the landMobilePhoneCode
	 */
	public String getLandMobilePhoneCode() {
		return landMobilePhoneCode;
	}


	/**
	 * @param landMobilePhoneCode the landMobilePhoneCode to set
	 */
	public void setLandMobilePhoneCode(String landMobilePhoneCode) {
		this.landMobilePhoneCode = landMobilePhoneCode;
	}

	/**
	 * Handle contact us operation for teller landing pages
	 * @return
	 */
	public String handleTellerContact() {

		return null;
	}


	/**
	 * @return the reciveUpdates
	 */
	public boolean isReciveUpdates() {
		return reciveUpdates;
	}


	/**
	 * @param reciveUpdates the reciveUpdates to set
	 */
	public void setReciveUpdates(boolean reciveUpdates) {
		this.reciveUpdates = reciveUpdates;
	}

	public String handleLandingContact() throws SQLException, IOException {
		FacesContext context = FacesContext.getCurrentInstance();
        String page = context.getViewRoot().getViewId();
        boolean checkEmail = true;
        //we want to remove email validation from some landing pages
        if (!CommonUtil.isParameterEmptyOrNull(page) && page.indexOf("_valid.xhtml")> -1) {
        	checkEmail = false;
        }
		if (UsersManager.validateContactUsLandingForm(this, checkEmail)) {
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
			HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
			ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);

	        String prefix = ap.getUserLocale().getLanguage();
	        try {
	            prefix = ApplicationData.getLinkLocalePrefix(request.getSession(), request, response);
	        } catch (Exception e) {
	            logger.error("Can't get language.", e);
	        }
			//for optemotional LP type B, need to validate empty field
	        if (!CommonUtil.isParameterEmptyOrNull(page) && (page.indexOf("/LP_opt_B")> -1 || page.indexOf("/LP_anyoption_optemotionals")> -1 || page.indexOf("/LP_Google_AO_RMBonus_Clean")> -1)) { //
				Locale locale = ap.getUserLocale();
				if (CommonUtil.isParameterEmptyOrNull(firstName.trim()) || firstName.equalsIgnoreCase(CommonUtil.getMessage("register.firstname", null, locale))) {
					String[] params = new String[1];
					params[0] = CommonUtil.getMessage("register.firstname",null);
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.required", params),null);
					context.addMessage("contactForm:firstName", fm);
					return null;
				}
				if (CommonUtil.isParameterEmptyOrNull(lastName.trim()) || lastName.equalsIgnoreCase(CommonUtil.getMessage("register.lastname", null, locale))) {
					String[] params = new String[1];
					params[0] = CommonUtil.getMessage("register.lastname",null);
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.required", params),null);
					context.addMessage("contactForm:lastName", fm);
					return null;
				}


	        }

            if (!CommonUtil.isParameterEmptyOrNull(page) && page.indexOf("/LP_anyoption_optemotionals")> -1) {
                Locale locale = ap.getUserLocale();
                if (CommonUtil.isParameterEmptyOrNull(mobilePhone.trim()) || mobilePhone.equalsIgnoreCase(CommonUtil.getMessage("register.mobilephone", null, locale))) {
                    String[] params = new String[1];
                    params[0] = CommonUtil.getMessage("register.mobilephone",null);
                    FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.required", params),null);
                    context.addMessage("contactForm:mobilePhone", fm);
                    return null;
                }
                if (CommonUtil.isParameterEmptyOrNull(email.trim()) || email.equalsIgnoreCase(CommonUtil.getMessage("register.email", null, locale))) {
                    String[] params = new String[1];
                    params[0] = CommonUtil.getMessage("register.email",null);
                    FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.banner.contactme.required", params),null);
                    context.addMessage("contactForm:email", fm);
                    return null;
                }
            }


			String url = context.getExternalContext().getRequestContextPath() + "/jsp/registerAfterLanding.jsf?";

			String params =
			"firstName="+ URLEncoder.encode(firstName, "UTF-8") +
			"&last_Name=" + URLEncoder.encode(lastName, "UTF-8") +
			"&email=" + email +
			"&mobilePhoneCode=" + landMobilePhoneCode +
			"&mobilePhone=" + landMobilePhone +
			"&reciveUpdates=" + reciveUpdates +
			"&regAtt=true";

			String countryId = ap.getRealCountryId();

			if(CommonUtil.isHebrewUserSkin()){
				params +=	"&mobilePhonePref=" + mobilePhonePrefix;
				landMobilePhone = mobilePhonePrefix + landMobilePhone;
				countryId = null; //in order to take default country for etrader (Israel)
			}
			
			String landingQuery = (String) session.getAttribute(Constants.LANDING_QUERY_STRING);
			if (!CommonUtil.isParameterEmptyOrNull(landingQuery)) {
				params += "&" + landingQuery;
			}

			ContactsManager.insertRegAttempt(email, firstName, lastName, countryId,
					landMobilePhone, null, reciveUpdates, reciveUpdates, true, session, false, false);

			//Save in session if we came from short landing page
			if (null == (String) session.getAttribute(Constants.REGISTER_FROM_CONTACTUS_LANDING) && !CommonUtil.isParameterEmptyOrNull(firstName)
					&& !CommonUtil.isParameterEmptyOrNull(lastName) && !CommonUtil.isParameterEmptyOrNull(landMobilePhone)){
				session.setAttribute(Constants.REGISTER_FROM_CONTACTUS_LANDING, "true");
			}

//			HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
//			HttpServletResponse resp = (HttpServletResponse) context.getExternalContext().getResponse();
//			ValueExpression ve1 = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.REGISTER_ATTEMPT, Contact.class);
//	        Contact regAttempt = (Contact) ve1.getValue(context.getELContext());
//	        long contactId=regAttempt.getId();
//	        MarketingTracker.checkExistCookieWhenInsertContact(request, response, regAttempt.getEmail(), regAttempt.getMobilePhone());
//			MarketingTracker.insertMarketingTracker(req, resp, contactId, 0, Constants.MARKETING_TRACKING_SHORT_REG);
//			
//			MarketingETS.etsMarketingActivity(request, response, contactId, Constants.MARKETING_TRACKING_SHORT_REG);

			String encodedUrl = response.encodeRedirectURL(url);
			response.sendRedirect(encodedUrl + params);
			return null;
		} else {
			return null;
		}
	}

	public String handleQuickStartContact() throws Exception {
		String params = validateAndCreateQuickStartContact();
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		if (params == null) {
			return null;
		} else if (!params.isEmpty()) {
			String url = context.getExternalContext().getRequestContextPath() + "/jsp/quickStartSuceess.jsf?";
			String encodedUrl = response.encodeRedirectURL(url);
			response.sendRedirect(encodedUrl + params);
			return null;
		} else {
			String url = context.getExternalContext().getRequestContextPath() + "/jsp/quickStartSuceess.jsf?";
			String encodedUrl = response.encodeRedirectURL(url);
			response.sendRedirect(encodedUrl);
			return null;
		}
	}

	public void insertQuickStartError(String key, String error){
		FacesContext context = FacesContext.getCurrentInstance();
		String[] params = new String[1];
		params[0] = CommonUtil.getMessage(key, null);
		FacesMessage fm =  new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage(error, params), null);
		context.addMessage("contactUsForm:generalError", fm);
	}

	public void insertQuickStartErrorWithField(String key, String error, String param){
		FacesContext context = FacesContext.getCurrentInstance();
		String[] params = new String[1];
		params[0] = CommonUtil.getMessage(key, null);
		FacesMessage fm =  new FacesMessage(FacesMessage.SEVERITY_ERROR, param + ": " + CommonUtil.getMessage(error, params), null);
		context.addMessage("contactUsForm:generalError", fm);
	}

	public String handleLandingMobileContact() throws SQLException, IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        String page = context.getViewRoot().getViewId();
        boolean checkEmail = true;

        if (UsersManager.validateContactUsLandingForm(this, checkEmail)) {
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
            HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
            ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);

            String url = "";
            if (page.equals("/jsp/etrader_globes_mobile.xhtml")){
                 url = context.getExternalContext().getRequestContextPath() + "/jsp/LP/mobile/etrader_globes_mobile_success.jsf?";
            } else if (page.equals("/jsp/etrader_testimonials_mobile.xhtml")){
                 url = context.getExternalContext().getRequestContextPath() + "/jsp/LP/mobile/etrader_testimonials_mobile_success.jsf?";
            }

            String countryId = ap.getRealCountryId();
            landMobilePhone = mobilePhonePrefix + landMobilePhone;

            ContactsManager.insertRegAttempt(email, firstName, lastName, countryId,
                    landMobilePhone, null, reciveUpdates, reciveUpdates, true, session, false, false);
            //Save in session if we came from short landing page
            if (null == (String) session.getAttribute(Constants.REGISTER_FROM_CONTACTUS_LANDING) && !CommonUtil.isParameterEmptyOrNull(firstName)
                    && !CommonUtil.isParameterEmptyOrNull(lastName) && !CommonUtil.isParameterEmptyOrNull(landMobilePhone)){
                session.setAttribute(Constants.REGISTER_FROM_CONTACTUS_LANDING, "true");
            }

//			HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
//			HttpServletResponse resp = (HttpServletResponse) context.getExternalContext().getResponse();
//			ValueExpression ve1 = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.REGISTER_ATTEMPT, Contact.class);
//	        Contact regAttempt = (Contact) ve1.getValue(context.getELContext());
//	        long contactId=regAttempt.getId();
//	        MarketingTracker.checkExistCookieWhenInsertContact(request, response, regAttempt.getEmail(), regAttempt.getMobilePhone());
//			MarketingTracker.insertMarketingTracker(req, resp, contactId, 0, Constants.MARKETING_TRACKING_SHORT_REG);
//			
//			MarketingETS.etsMarketingActivity(request, response, contactId, Constants.MARKETING_TRACKING_SHORT_REG);

            String encodedUrl = response.encodeRedirectURL(url);
            response.sendRedirect(encodedUrl);
            return null;
        } else {
            return null;
        }
    }
	
	/**
	 * This method creates validates and creates quick start contact and is used in logic for old quick start form
	 * 
	 * @return null if there is validation error, empty string if all the contact fields are left empty, 
	 * the parameters of the created contact
	 * @throws SQLException if there is problem with user locale
	 * @throws UnsupportedEncodingException if the parameter encoding fails
	 */
	public String validateAndCreateQuickStartContact() throws SQLException, UnsupportedEncodingException {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = context.getApplication().evaluateExpressionGet(context, Constants.BIND_APPLICATION_DATA, ApplicationData.class);
		Locale locale = ap.getUserLocale();
		String pattern = CommonUtil.getMessage("general.validate.letters.only", null);
		String params = null;
		if (!firstName.equalsIgnoreCase(CommonUtil.getMessage("register.firstname.caps", null)) ||
				!lastName.equalsIgnoreCase(CommonUtil.getMessage("register.lastname.caps", null)) ||
				!email.equalsIgnoreCase(CommonUtil.getMessage("register.email.caps", null)) ||
				!landMobilePhone.equalsIgnoreCase(CommonUtil.getMessage("register.phone.caps", null))) {
			if (CommonUtil.isParameterEmptyOrNull(firstName.trim()) || firstName.equalsIgnoreCase(CommonUtil.getMessage("register.firstname", null, locale))) {
				insertQuickStartError("register.firstname", "error.banner.contactme.required");
				return null;
			} else if (!firstName.matches(pattern)){
				insertQuickStartErrorWithField("register.firstname", "register.letters.only", CommonUtil.getMessage("register.firstname", null, locale));
				return null;
			}
			if (CommonUtil.isParameterEmptyOrNull(lastName.trim()) || lastName.equalsIgnoreCase(CommonUtil.getMessage("register.lastname", null, locale))) {
				insertQuickStartError("register.lastname", "error.banner.contactme.required");
				return null;
			} else if (!lastName.matches(pattern)){
				insertQuickStartErrorWithField("register.lastname", "register.letters.only", CommonUtil.getMessage("register.lastname", null, locale));
				return null;
			}
			 if (CommonUtil.isParameterEmptyOrNull(email.trim()) || email.equalsIgnoreCase(CommonUtil.getMessage("register.email", null, locale))) {
				 insertQuickStartError("register.email.caps", "error.banner.contactme.required");
	             return null;
	         }  else if (!CommonUtil.isEmailValidUnix(email)){
	        	 insertQuickStartError("register.email.caps", "org.apache.myfaces.Email.INVALID");
	             return null;
	         }
			 if (CommonUtil.isParameterEmptyOrNull(landMobilePhone.trim()) || landMobilePhone.equalsIgnoreCase(CommonUtil.getMessage("register.phone.caps", null, locale))) {
	             insertQuickStartError("register.mobilephone", "error.banner.contactme.required");
				 return null;
	         } else if(!landMobilePhone.matches("\\d+")){
	        	 insertQuickStartError("register.mobilephone", "error.banner.contactme.phone");
	        	 return null;
	         } else if (landMobilePhone.length() < 7 || landMobilePhone.length() > 20){
	        	 insertQuickStartError("register.mobilephone", "error.quick.start.mobile.phone");
				 return null;
	         }
			 
			 params = "firstName="+ URLEncoder.encode(firstName, "UTF-8") +
						"&last_Name=" + URLEncoder.encode(lastName, "UTF-8") +
						"&email=" + email +
						"&mobilePhonePref=" + landMobilePhoneCode +
						"&mobilePhone=" + landMobilePhone +
						"&regAtt=true";
			 createQuickStartContact(context, ap.getRealCountryId());
			
		} else { // all fields are empty
			params = "";
		}
		
		return params;
	}
	
	/**
	 * This method creates quick start contact and is used for the new quick sign up form
	 * 
	 * @param context the FacesContext
	 * @param realCountryId the real country id from ApplicationData
	 * @throws SQLException if marketing tracking insert fails
	 */
	public void createQuickStartContact(FacesContext context, String realCountryId) throws SQLException {
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);		

		String countryId = realCountryId;
		
		ApplicationData ap = context.getApplication()
		        .evaluateExpressionGet(context, Constants.BIND_APPLICATION_DATA, ApplicationData.class);
		if(ap.getSkinId() == Skins.SKIN_ETRADER) {
			if (!landMobilePhoneCode.startsWith("0")) {
				landMobilePhoneCode = "0" + landMobilePhoneCode;
			}
			landMobilePhone = landMobilePhoneCode + landMobilePhone;
		}
		ContactsManager.insertRegAttempt(email, firstName, lastName, countryId, landMobilePhone, null, reciveUpdates,
											reciveUpdates, false, session, false, true);
		//Save in session if we came from short landing page
		if (null == (String) session.getAttribute(Constants.REGISTER_FROM_QUICK_START) && !CommonUtil.isParameterEmptyOrNull(firstName)
				&& !CommonUtil.isParameterEmptyOrNull(lastName) && !CommonUtil.isParameterEmptyOrNull(landMobilePhone)) {
			session.setAttribute(Constants.REGISTER_FROM_QUICK_START, "true");
		}
		
//		ValueExpression ve1 = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(),
//																										Constants.REGISTER_ATTEMPT,
//																										Contact.class);
//		Contact regAttempt = (Contact) ve1.getValue(context.getELContext());
//		long contactId=regAttempt.getId();
//		MarketingTracker.checkExistCookieWhenInsertContact(request, response, regAttempt.getEmail(), regAttempt.getMobilePhone());
//		MarketingTracker.insertMarketingTracker(request, response, contactId, 0, Constants.MARKETING_TRACKING_SHORT_REG);
//		
//		MarketingETS.etsMarketingActivity(request, response, contactId, Constants.MARKETING_TRACKING_SHORT_REG);
	}
	
	/**
	 * Fills the faces context with error messages from the contact us form. This is used 
	 * in the new quick sign up form to validate the errors
	 * 
	 * @param locale the user locale from ApplicationData
	 * @return true if any errors are added to the context, false otherwise
	 */
	public boolean fillContextWithErrorMessages(Locale locale) {
		String pattern = CommonUtil.getMessage("general.validate.letters.only", null);
		boolean result = false;
		if (CommonUtil.isParameterEmptyOrNull(firstName.trim()) || firstName.equalsIgnoreCase(CommonUtil.getMessage("register.firstname", null, locale))) {
			addContactErrorMessage("register.firstname", "error.banner.contactme.required", firstNameField);
			result = true;
		} else if (!firstName.matches(pattern)){
			addContactErrorMessage("register.firstname", "register.letters.only", firstNameField);
			result = true;
		}
		if (CommonUtil.isParameterEmptyOrNull(lastName.trim()) || lastName.equalsIgnoreCase(CommonUtil.getMessage("register.lastname", null, locale))) {
			addContactErrorMessage("register.lastname", "error.banner.contactme.required", lastNameField);
			result = true;
		} else if (!lastName.matches(pattern)){
			addContactErrorMessage("register.lastname", "register.letters.only", lastNameField);
			result = true;
		}
		 if (CommonUtil.isParameterEmptyOrNull(email.trim()) || email.equalsIgnoreCase(CommonUtil.getMessage("register.email", null, locale))) {
			 addContactErrorMessage("register.email.caps", "error.banner.contactme.required", emailField);
			 result = true;
         } else if (!EmailValidator.getInstance().isValid(email) || !CommonUtil.isEmailValidUnix(email)) {
        	 addContactErrorMessage("register.email.caps", "org.apache.myfaces.Email.INVALID", emailField);
        	 result = true;
         }
		 if (CommonUtil.isParameterEmptyOrNull(landMobilePhone.trim()) || landMobilePhone.equalsIgnoreCase(CommonUtil.getMessage("register.phone.caps", null, locale))) {
			 addContactErrorMessage("register.mobilephone", "error.banner.contactme.required", mobilePhoneField);
			 result = true;
         } else if(!landMobilePhone.matches("\\d+")){
        	 addContactErrorMessage("register.mobilephone", "error.banner.contactme.phone", mobilePhoneField);
        	 result = true;
         } else if (landMobilePhone.length() < 7 || landMobilePhone.length() > 20){
        	 addContactErrorMessage("register.mobilephone", "error.quick.start.mobile.phone", mobilePhoneField);
        	 result = true;
         }
		 
		 return result;
	}
	
	private void addContactErrorMessage(String key, String error, String field){
		FacesContext context = FacesContext.getCurrentInstance();
		String[] params = new String[1];
		params[0] = CommonUtil.getMessage(key, null);
		FacesMessage fm =  new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage(error, params), field);
		context.addMessage("", fm);
	}

	/**
	 * @return the mobileCountryId
	 */
	public Long getMobileCountryId() {
		return mobileCountryId;
	}

	/**
	 * @param mobileCountryId the mobileCountryId to set
	 */
	public void setMobileCountryId(Long mobileCountryId) {
		this.mobileCountryId = mobileCountryId;
	}

	public String getAff_sub1() {
		return aff_sub1;
	}

	public void setAff_sub1(String aff_sub1) {
		this.aff_sub1 = aff_sub1;
	}

	public String getAff_sub2() {
		return aff_sub2;
	}

	public void setAff_sub2(String aff_sub2) {
		this.aff_sub2 = aff_sub2;
	}

	public String getAff_sub3() {
		return aff_sub3;
	}

	public void setAff_sub3(String aff_sub3) {
		this.aff_sub3 = aff_sub3;
	}
	
	
}