package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.MarketRate;
import com.anyoption.common.charts.LevelHistoryCache;
import com.anyoption.common.enums.SkinGroup;

import il.co.etrader.web.bl_managers.ApplicationData;

public class LevelHistoryForm implements Serializable {
    private static final Logger log = Logger.getLogger(LevelHistoryForm.class);
    
    private long marketId;
    
    public String getOppIdFromParam() {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> pm = fc.getExternalContext().getRequestParameterMap();
        if (null != pm.get("marketId")) {
            marketId = Long.parseLong((String) pm.get("marketId"));
            log.debug("set marketId: " + marketId);
        }
        return "";
    }
    
    public List<MarketRate> getHistory() {
        log.debug("get history for: " + marketId);
        FacesContext fc = FacesContext.getCurrentInstance();
        ServletContext sc = (ServletContext) fc.getExternalContext().getContext();
        HttpServletRequest req = (HttpServletRequest) fc.getExternalContext().getRequest();
        LevelHistoryCache lhc = (LevelHistoryCache) sc.getAttribute("levelHistoryCache");
        Map<SkinGroup, List<MarketRate>> ratesMap = lhc.getMarketHistory(marketId);
        List<MarketRate> l = null;
        if (ratesMap != null) {
        	l = ratesMap.get(ApplicationData.getSkinById(ApplicationData.getSkinId(req.getSession(), req)).getSkinGroup());
        }
        log.debug("return rates: " + (null != l ? l.size() : 0));
        return l;
    }
}