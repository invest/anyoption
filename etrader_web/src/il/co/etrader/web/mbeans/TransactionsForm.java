package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.el.ValueExpression;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIData;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.bl_vos.WireBase;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.TransactionsManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;

public class TransactionsForm implements Serializable {
	private static final long serialVersionUID = 5460789526167955790L;

	private Date from;
	private Date to;
	private ArrayList<Transaction> transactionsList;
	private String sortColumn;
	private String prevSortColumn;
	private boolean sortAscending = false;
	private Transaction transaction;

	public TransactionsForm() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		to = new Date();
		if(!CommonUtil.isHebrewSkin(a.getSkinId())){ // add userOffset to date
			to = CommonUtil.getDateTimeFormaByTz(to, CommonUtil.getUtcOffset());
		}
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(to);

		to = gc.getTime();
		gc.add(GregorianCalendar.DAY_OF_MONTH, -7);
		from = gc.getTime();
		updateTransactionsList();
	}

	public void openReceipt(ActionEvent event) throws SQLException {

		FacesContext facesContext = FacesContext.getCurrentInstance();
		Transaction tran = null;
		UIComponent tmpComponent = event.getComponent();
		while(null != tmpComponent && !(tmpComponent instanceof UIData)) {
			tmpComponent = tmpComponent.getParent();
		}
		if (tmpComponent != null && (tmpComponent instanceof UIData)) {
			Object tmpRowData = ((UIData) tmpComponent).getRowData();
			if (tmpRowData instanceof Transaction) {
				tran = (Transaction) tmpRowData;
				if (tran.isCcDeposit()) {
					CreditCard cc = TransactionsManager.getCreditCard(tran.getCreditCardId());
					tran.setCc4digit(cc.getCcNumberLast4());
				}
                if (tran.isWireDeposit()) {
                    WireBase wire = TransactionsManager.getWire(tran.getWireId());
                    tran.setWire(wire);
                }
                ValueExpression ve = facesContext.getApplication().getExpressionFactory().createValueExpression(facesContext.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
                ve.getValue(facesContext.getELContext());
                ve.setValue(facesContext.getELContext(), tran);
			}
		}

		final String viewId = Constants.NAV_RECEIPT_PRINT;

		// This is the proper way to get the view's url
		ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);
		actionUrl = actionUrl.replace(".xhtml", ".jsf");

		String javaScriptText = "window.open('" + actionUrl + "', 'receiptpopup', 'menubar=no,resizable=no,location=no,toolbar=no,width=500,height=500');";

		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	}

	public int getListSize() {
		return transactionsList.size();
	}

	public ArrayList<Transaction> getTransactionsList() {
		return transactionsList;
	}

	public String updateTransactionsList() throws Exception {

		User user=ApplicationData.getUserFromSession();

		transactionsList = TransactionsManager.getTransactionsByUserAndDates(user.getId(), from, to, CommonUtil.getUtcOffset());
		CommonUtil.setTablesToFirstPage();

		return null;
	}

	/*
	 * public String navTransactions() throws SQLException{
	 * updateTransactionsList(); return Constants.NAV_TRANSACTIONS; }
	 */
	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	/**
	 * Constructs a <code>String</code> with all attributes in name = value
	 * format.
	 *
	 * @return a <code>String</code> representation of this object.
	 */
	public String toString() {
		final String TAB = " \n ";

		String retValue = "";

		retValue =
				"TransactionsForm ( " + super.toString() + TAB + "from = " + this.from + TAB + "to = " + this.to + TAB + "transactionsList = " + this.transactionsList + TAB + "transaction = " + this.transaction + TAB + " )";

		return retValue;
	}

	public String sortList() {
		if (sortColumn.equals(prevSortColumn))
			sortAscending = !sortAscending;
		else
			sortAscending = true;

		prevSortColumn = sortColumn;

		if (sortColumn.equals("id"))
			Collections.sort(transactionsList, new IdComparator(sortAscending));

		if (sortColumn.equals("timeCreated"))
			Collections.sort(transactionsList, new TimeCreatedComparator(sortAscending));

		if (sortColumn.equals("desc"))
			Collections.sort(transactionsList, new DescComparator(sortAscending));

		if (sortColumn.equals("credit"))
			Collections.sort(transactionsList, new CreditComparator(sortAscending));

		if (sortColumn.equals("debit"))
			Collections.sort(transactionsList, new DebitComparator(sortAscending));

		CommonUtil.setTablesToFirstPage();

		return null;

	}

	private class TimeCreatedComparator implements Comparator {
		private boolean ascending;

		public TimeCreatedComparator(boolean asc) {
			ascending = asc;
		}

		public int compare(Object o1, Object o2) {
			Transaction op1 = (Transaction) o1;
			Transaction op2 = (Transaction) o2;
			if (op1.getTimeCreated() == null || op2.getTimeCreated() == null) {
				return op1.getTimeCreatedTxt().compareTo(op2.getTimeCreatedTxt());
			}

			if (ascending) {
				return op1.getTimeCreated().compareTo(op2.getTimeCreated());
			}
			return -op1.getTimeCreated().compareTo(op2.getTimeCreated());
		}
	}

	private class IdComparator implements Comparator {
		private boolean ascending;

		public IdComparator(boolean asc) {
			ascending = asc;
		}

		public int compare(Object o1, Object o2) {
			Transaction op1 = (Transaction) o1;
			Transaction op2 = (Transaction) o2;
			if (ascending) {
				return new Long(op1.getId()).compareTo(new Long(op2.getId()));
			}
			return -new Long(op1.getId()).compareTo(new Long(op2.getId()));
		}
	}

	private class DescComparator implements Comparator {
		private boolean ascending;

		public DescComparator(boolean asc) {
			ascending = asc;
		}

		public int compare(Object o1, Object o2) {
				Transaction op1=(Transaction)o1;
				Transaction op2=(Transaction)o2;

   		 		if (ascending){
   		 			return new String(op1.getTypeName()).compareTo(new String(op2.getTypeName()));
   		 		}
   		 	return -new String(op1.getTypeName()).compareTo(new String(op2.getTypeName()));
		}
	}

	private class DebitComparator implements Comparator {
		private boolean ascending;

		public DebitComparator(boolean asc) {
			ascending = asc;
		}

		public int compare(Object o1, Object o2) {
			Transaction op1 = (Transaction) o1;
			Transaction op2 = (Transaction) o2;
			Double amount1 = new Double(op1.getAmount());
			Double amount2 = new Double(op2.getAmount());

			if (op1.getCredit()) {
				if (ascending) {
					amount1 = Double.MAX_VALUE;
				} else {
					amount1 = Double.MIN_VALUE;
				}
			}
			if (op2.getCredit()) {
				if (ascending) {
					amount2 = Double.MAX_VALUE;
				} else {
					amount2 = Double.MIN_VALUE;
				}
			}

			if (ascending) {
				return new Double(amount1).compareTo(new Double(amount2));
			}
			return -new Double(amount1).compareTo(new Double(amount2));
		}
	}

	private class CreditComparator implements Comparator {
		private boolean ascending;

		public CreditComparator(boolean asc) {
			ascending = asc;
		}

		public int compare(Object o1, Object o2) {
			Transaction op1 = (Transaction) o1;
			Transaction op2 = (Transaction) o2;
			Double amount1 = new Double(op1.getAmount());
			Double amount2 = new Double(op2.getAmount());

			if (!op1.getCredit()) {
				if (ascending) {
					amount1 = Double.MAX_VALUE;
				} else {
					amount1 = Double.MIN_VALUE;
				}
			}
			if (!op2.getCredit()) {
				if (ascending) {
					amount2 = Double.MAX_VALUE;
				} else {
					amount2 = Double.MIN_VALUE;
				}
			}

			if (ascending) {
				return new Double(amount1).compareTo(new Double(amount2));
			}
			return -new Double(amount1).compareTo(new Double(amount2));
		}
	}

	public String getPrevSortColumn() {
		return prevSortColumn;
	}

	public void setPrevSortColumn(String prevSortColumn) {
		this.prevSortColumn = prevSortColumn;
	}

	public boolean isSortAscending() {
		return sortAscending;
	}

	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	public String getSortColumn() {
		return sortColumn;
	}

	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}
}
