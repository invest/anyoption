package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.TransactionsManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;
import il.co.etrader.web.util.FormBase;


public class ReverseWithdrawForm extends FormBase implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -7210902098031660674L;

	private static final Logger logger = Logger.getLogger(ReverseWithdrawForm.class);

	private Date from ;
	private Date to;
	private ArrayList transactionsList;

	private boolean afterReverse;


	public ReverseWithdrawForm() throws Exception{
		afterReverse=false;
		updateTransactionsList();
	}

	public ArrayList getTransactionsList() {
		return transactionsList;
	}

	public int getListSize() {
		return transactionsList.size();
	}

	public void setTransactionsList(ArrayList transactionsList) {
		this.transactionsList = transactionsList;
	}

	public void updateTransactionsList() throws Exception{
		User user=ApplicationData.getUserFromSession();

		transactionsList=TransactionsManager.getPendingWithdraws(user.getId());
		afterReverse=false;
		CommonUtil.setTablesToFirstPage();

	}
	public String reverseWithdraw() throws Exception{
		//	 prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
        	updateTransactionsList();
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();
        FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}

        User user=ApplicationData.getUserFromSession();

		TransactionsManager.reverseWithdraw(transactionsList,user, loginId);
		updateTransactionsList();
		afterReverse=true;
		return null;
	}


	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public boolean isAfterReverse() {
		return afterReverse;
	}

	public void setAfterReverse(boolean afterReverse) {
		this.afterReverse = afterReverse;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "ReverseWithdrawForm ( "
	        + super.toString() + TAB
	        + "from = " + this.from + TAB
	        + "to = " + this.to + TAB
	        + "transactionsList = " + this.transactionsList + TAB
	        + "afterReverse = " + this.afterReverse + TAB
	        + " )";

	    return retValue;
	}





}
