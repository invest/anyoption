package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.QuestionnaireGroup;
import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.BaroPayManagerBase;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.payments.WithdrawEntryInfo;
import com.anyoption.common.payments.WithdrawEntryResult;
import com.anyoption.common.payments.WithdrawUtil;

import il.co.etrader.bl_managers.AdminManagerBase;
import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Limit;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.TransactionsManager;
import il.co.etrader.web.bl_managers.UsersManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;
import il.co.etrader.web.util.FormBase;

public class WithdrawForm extends FormBase  implements Serializable{

	private static final long serialVersionUID = 2790964752798137364L;

	private static final Logger logger = Logger.getLogger(WithdrawForm.class);

	private static final int WITHDRAW_TYPE_REGULAR = 1;
	private static final int WITHDRAW_TYPE_CC = 2;
	private static final int WITHDRAW_TYPE_ENVOY = 3;
	private static final int WITHDRAW_TYPE_WEBMONEY = 4;
	private static final int WITHDRAW_TYPE_DELTAPAY_CHINA = 5;
	private static final int WITHDRAW_TYPE_CDPAY = 6;
	private static final int WITHDRAW_TYPE_BARO_PAY = 7;
	private static final int WITHDRAW_TYPE_CUP = 8;

	private String name;
	private String amount;
	private String street;
	private String streetNo;
	private long cityId;
	private String zipCode;
	private String comments;
	private String cityName;

	// visa withdrawl
	private HashMap<Long, CreditCard> ccHash;
	private String cardId;
	private int withdrawType;
	private boolean isCcWithdrawalEnabled;		  // for display links issues

	private boolean bonusError = false;

	// Envoy Withdraw
	private String envoyAccountNum;
	private long envoyWithdrawMethodId;
	private String envoyApprovePage;
	private String envoyResultPage;
	private ArrayList<SelectItem> envoyAccountNumList;

	//WebMoney Withdraw
	private ArrayList<SelectItem> webMoneyPurseList;
	private String webMoneyPurse;

	private Long sumOfDeltaPayDeposits;
	private boolean diners;
	// CUP deposits - withdrawals
	long cupMaxAmountWithdraw = 0;
	// when calculating amount for withdraw, count or not deltapay transactions before CUP distro
	boolean countOldDeltapayTransactions = false;
	
	// BaroPay
	private String bankNameTxt;
	private String accountOwnerTxt;
	private String accountNum;
	// Rewards plan
	private boolean isHavingRewardExemptFee;
	
	//Withdraw survey
	private Long userAnswerId;
	private String textAnswer;

	public WithdrawForm () throws Exception {
		initCUPWithdraw();
		comments="";
		ccHash = new HashMap<Long, CreditCard>();
		withdrawType = WITHDRAW_TYPE_REGULAR;
		isCcWithdrawalEnabled = false;
		bonusError = false;
		envoyAccountNum = "";
		webMoneyPurse = "";
		setDiners(false);
		bankNameTxt = "";
		accountOwnerTxt = "";
		accountNum = "";
		isHavingRewardExemptFee = false;
	}

	/**
	 * Insert cheque withdrawal just for etrader
	 * @return
	 * @throws Exception
	 */
	public String insertWithdraw() throws Exception {
		// prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
        User user = ApplicationData.getUserFromSession();

		if (!TransactionsManager.validateCityWithdrawForm(cityName,cityId)) {
			return null;
		}

		String error = TransactionsManager.validateWithdrawForm(amount, user, Constants.ENUM_WITH_FEE, false,true, null, null, null, null, Writer.WRITER_ID_WEB);

		if (error == null) {
			//Block withdrawal by cheque/Bank wire attempt if user has non visible CC
			Limit limit = UsersManager.getLimitById(user.getLimitIdLongValue());
			HashMap<Long,CreditCard> cc = TransactionsManagerBase.getDisplayWithrawalCreditCardsByUser(user.getId(), limit.getMinWithdraw(), user.getSkinId(), false);
			if (cc.size() > 0) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.minwithdraw1", null),null);
				context.addMessage(null, fm);

				error = "error.withdraw.card.not.visible";
			}
			
			WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
			if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
				error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(error, null),null);
				context.addMessage(null, fm);
			}
		}

		if (error != null) {
			TransactionsManager.insertWithdraw(amount,cityId, cityName, name,street,streetNo,zipCode,error,user,CommonUtil.getWebWriterId(),false, loginId, null);
			if (error.equals("bonus.cannot.withdraw.error")) {
				bonusError = true;
			} else {
				bonusError = false;
			}
			return null;
		}

		return Constants.NAV_WITHDRAW_APPROVE;    // allways display general fee message

	}

	/**
	 * Approve withdrawal
	 * @return
	 * @throws Exception
	 */
	public String approveWithdraw() throws Exception{
		String nav = getNavigationPage();
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}

		//	prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, Constants.NAV_WITHDRAW_APPROVE);
        }
        this.generateSaveToken();

        User user = ApplicationData.getUserFromSession();

		if (amount == null) {
			nav = getNavigationPageForBackButton();
			return nav;
		}

        String error = null;
        if (isCcWithdrawal()) {
    		long ccMinWithdraw = TransactionsManagerBase.getCreditCardMinWithdraw(user.getCurrencyId());
    		if( CommonUtil.calcAmount(amount) <= ccMinWithdraw) {
    			error = "error.minwithdraw1";
    		}
    		if (error == null) {
    			error = TransactionsManager.validateWithdrawForm(amount, user, Constants.ENUM_WITH_FEE, true, false, ccHash.get(Long.valueOf(cardId)), null, null, null, Writer.WRITER_ID_WEB);
    		}
        } else if (isDeltaPayWithdrawal() || isCdpayWithdrawal() || isCUPWithdrawal()) { // deltapay & cdpay -> keep for backwards compatibility
        	error = TransactionsManager.validateWithdrawForm(amount, user, Constants.ENUM_WITH_FEE, true,true, null, null, null, sumOfDeltaPayDeposits, Writer.WRITER_ID_WEB);
        } else if (isBaroPayWithdrawal()) {
        	error = TransactionsManager.validateWithdrawForm(amount, user, Constants.ENUM_WITH_FEE, false,true, null, null, null, null, Writer.WRITER_ID_WEB);
        } else { // cc withdrawal
        	error = TransactionsManager.validateWithdrawForm(amount, user, Constants.ENUM_WITH_FEE, false,true, null, null, null, null, Writer.WRITER_ID_WEB);
        }

		if (error != null) {
	     	FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("error.unexpected",null),null);
	     	context.addMessage(null, fm);

	     	clearWithdrawForm();
			return nav;
		}

		// cancel all bonuses that can be canceled or return error if something goes wrong
		List<BonusUsers> bonusUsers = BonusManagerBase.getAllUserBonus(user.getId(), null, null, 0, CommonUtil.getUtcOffset(), new ArrayList<>(Arrays.asList(CurrenciesManagerBase.getCurrency(user.getCurrencyId()))));
		for (BonusUsers bu : bonusUsers) {
			if (bu.isCanCanceled()) {
				error = BonusManagerBase.cancelBonus(bu, CommonUtil.getUtcOffset(), Writer.WRITER_ID_WEB, user.getSkinId(), user, loginId, CommonUtil.getIPAddress());
			}
			if (error != null && error.equals(BonusManagerBase.ERROR_DUMMY)) {
				logger.error("Cannot cancel bonus with id: " + bu.getId());
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("error.creditcard.unapproved",null),null);
		     	context.addMessage(null, fm);
		     	clearWithdrawForm();
				return nav;
			} else {
				error = "";
			}
		}

		int tranTypeId = 0;
		String successMsgKey ="";
		QuestionnaireUserAnswer questUserAnswer = null;
		if (userAnswerId != null && userAnswerId > 0) {
			questUserAnswer = new QuestionnaireUserAnswer();
			questUserAnswer.setAnswerId(userAnswerId);
			questUserAnswer.setTimeCreated(new Date());
			questUserAnswer.setGroupId(QuestionnaireGroup.WITHDRAW_SURVEY_ID);
			questUserAnswer.setQuestionId(QuestionnaireQuestion.WITHDRAW_SURVEY_QUESTION);
			if (!CommonUtil.isParameterEmptyOrNull(textAnswer)) {
				questUserAnswer.setTextAnswer(textAnswer);
			}
			questUserAnswer.setUserId(user.getId());
			questUserAnswer.setWriterId(Writer.WRITER_ID_WEB);
		}
		
		if (isCcWithdrawal()) {
			tranTypeId = TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW;
			error =  TransactionsManager.insertCreditCardWithdraw(amount, ccHash.get(Long.valueOf(cardId)),name,null,user,CommonUtil.getWebWriterId(),false, null, loginId, questUserAnswer);
			if ( error != null ) {   // error with cc
				return nav;
			}
			successMsgKey = "withdraw.cc.success";
			if (isHavingRewardExemptFee) {
				successMsgKey = "rewards.withdraw.cc.success";
			}
		} else if (isEnvoyWithdrawal()){
			TransactionsManager.insertWithdrawEnvoyService(amount, envoyWithdrawMethodId, user, CommonUtil.getWebWriterId(), false, error, envoyAccountNum, loginId, questUserAnswer);
			tranTypeId = TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW;
			successMsgKey = "withdraw.cc.success";
			if (isHavingRewardExemptFee) {
				successMsgKey = "rewards.withdraw.envoy.success";
			}
		} else if (isWebMoneyWithdrawal()){
			TransactionsManager.insertWebMoneyWithdraw(amount, error, user, CommonUtil.getWebWriterId(), false, webMoneyPurse, loginId, questUserAnswer);
			tranTypeId = TransactionsManagerBase.TRANS_TYPE_WEBMONEY_WITHDRAW;
			successMsgKey = "withdraw.cc.success";
			if (isHavingRewardExemptFee) {
				successMsgKey = "rewards.withdraw.envoy.success";
			}
		} 
		/*else if(isDeltaPayWithdrawal()) { // keep for reference
			TransactionsManager.insertDeltaPayChinaWithdraw(amount, error, user, CommonUtil.getWebWriterId(), false);
			tranTypeId = TransactionsManagerBase.TRANS_TYPE_DELTAPAY_CHINA_WITHDRAW;
			successMsgKey = "withdraw.cc.success";
		} else if(isCdpayWithdrawal()) { // keep for reference
			TransactionsManager.insertCDPayWithdraw(amount, error, user, CommonUtil.getWebWriterId(), false);
			tranTypeId = TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_WITHDRAW;
			successMsgKey = "withdraw.cc.success"; 
		} */
		else if(isCUPWithdrawal()) {
			TransactionsManager.insertCUPWithdraw(amount, error, user, CommonUtil.getWebWriterId(), false, loginId, questUserAnswer);
			tranTypeId = TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_WITHDRAW;
			successMsgKey = "withdraw.cc.success";
			if (isHavingRewardExemptFee) {
				successMsgKey = "rewards.withdraw.cc.success";
			}
			
		} else if(isBaroPayWithdrawal()) {
			TransactionsManager.insertBaroPayWithdraw(amount, error, user, CommonUtil.getWebWriterId(), false, bankNameTxt, accountOwnerTxt, accountNum, loginId, questUserAnswer);
			tranTypeId = TransactionsManagerBase.TRANS_TYPE_BAROPAY_WITHDRAW;
			successMsgKey = "withdraw.cc.success";
			if (isHavingRewardExemptFee) {
				successMsgKey = "rewards.withdraw.cc.success";
			}
		} else {
			TransactionsManager.insertWithdraw(amount,cityId, cityName,name,street,streetNo,zipCode,null,user,CommonUtil.getWebWriterId(),false, loginId, questUserAnswer);
			tranTypeId = TransactionsManagerBase.TRANS_TYPE_CHEQUE_WITHDRAW;
			successMsgKey = "withdraw.success";
			if (isHavingRewardExemptFee) {
				successMsgKey = "rewards.withdraw.success";
			}
		}
		long amountInCents = CommonUtil.calcAmount(amount);
		String[] params = new String[2];
		long feeVal = TransactionsManagerBase.getTransactionHomoFee(amountInCents, user.getCurrencyId(), tranTypeId);
		String fee = "";
		long amountLong = CommonUtil.calcAmount(amount);

		if (user.getSkinId() != Skin.SKIN_ARABIC){
        	params[0] = CommonUtil.displayAmount(amountLong, user.getCurrencyId());
        	fee = CommonUtil.displayAmount(feeVal, user.getCurrencyId());
		}else{
			params[0] = CommonUtil.displayAmountForInput(amountLong, false, user.getCurrencyId().intValue()) + " " + CommonUtil.getMessage(user.getCurrency().getSymbol(),null) + " ";
			fee = CommonUtil.displayAmountForInput(feeVal, false, user.getCurrencyId().intValue()) + " " + CommonUtil.getMessage(user.getCurrency().getSymbol(),null) + " ";
		}
        params[1] = fee;
        String msg ="";
       	msg = CommonUtil.getMessage(successMsgKey,params);

     	FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,msg,null);
     	context.addMessage(null, fm);

     	WithdrawEntryResult postWithdrawEntryResult = WithdrawUtil.postWithdrawEntry(new WithdrawEntryInfo(user));
     	
     	clearWithdrawForm();

		return nav;
	}

	/**
	 * User not approved the withdrawal
	 * @return
	 * @throws Exception
	 */

	public String notApproveWithdraw() throws Exception {
		FacesContext context=FacesContext.getCurrentInstance();
		//	 prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, Constants.NAV_EMPTY_MYACCOUNT);
        }
        this.generateSaveToken();

	    String msg = CommonUtil.getMessage("withdraw.cancel",null);
     	FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,msg,null);
     	context.addMessage(null, fm);
     	amount = null;

     	withdrawType = WITHDRAW_TYPE_REGULAR;;

     	return getNavigationPage();
	}

	public String getInitWithdrawForm() throws Exception {
		User user = ApplicationData.getUserFromSession();
		initCUPWithdraw();
		name = user.getFirstName()+" "+user.getLastName();
		street = user.getStreet();
		streetNo = user.getStreetNo();
		cityId = user.getCityId();
		zipCode = user.getZipCode();
		amount = "";
		cardId = "";
		withdrawType = WITHDRAW_TYPE_REGULAR;
		bonusError = false;
		bankNameTxt = "";
		accountOwnerTxt = "";
		accountNum = "";
		return Constants.NAV_WITHDRAW;
	}

	public void clearWithdrawForm() throws SQLException {
		name = "";
		street = "";
		streetNo = "";
		cityId = 0;
		zipCode = "";
		amount = null;
		cardId = "";
		withdrawType = WITHDRAW_TYPE_REGULAR;
		bonusError = false;
		ccHash = new HashMap<Long, CreditCard>();
	}

	public String getInitMonetaWithdraw() throws Exception{
		envoyWithdrawMethodId = TransactionsManagerBase.PAYMENT_TYPE_MONETA;
		envoyApprovePage = Constants.NAV_WITHDRAW_APPROVE_MONETA;
		envoyResultPage = Constants.NAV_WITHDRAW_RESULT_MONETA;
		initEnvoyAccountNumList(envoyWithdrawMethodId);
		return null;
	}

	public String getInitWebmoneyWithdraw() throws Exception{
		User user = ApplicationData.getUserFromSession();
		// Direct integration with webMoney for ruble currency
		if (user.getCurrencyId() != ConstantsBase.CURRENCY_RUB_ID) {
			envoyWithdrawMethodId = TransactionsManagerBase.PAYMENT_TYPE_WEB_MONEY;
			envoyApprovePage = Constants.NAV_WITHDRAW_APPROVE_WEBMONEY;
			envoyResultPage = Constants.NAV_WITHDRAW_RESULT_WEBMONEY;
			initEnvoyAccountNumList(envoyWithdrawMethodId);
		} else {
			webMoneyPurseList = TransactionsManagerBase.getWebMoneyPursesByUserId(user.getId());
			if (null != webMoneyPurseList && webMoneyPurseList.size() > 0 ) {
				webMoneyPurse = webMoneyPurseList.get(0).toString();
			}
		}
		return null;
	}

	private void initEnvoyAccountNumList(long paymentTypeId) throws Exception{
		User user = ApplicationData.getUserFromSession();
		envoyAccountNumList = TransactionsManagerBase.getEnvoyAccountNumsByUserIdList(user.getId(),paymentTypeId);
		envoyAccountNum = "";
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the cityId
	 */
	public long getCityId() {
		return cityId;
	}

	/**
	 * @param cityId the cityId to set
	 */
	public void setCityId(long id) {
		if (CommonUtil.isEtraderUserSkin()) {
			this.cityId = id;
		} else {
			this.cityId=ConstantsBase.ANYOPTION_CITY_ID;
		}

	}

	public String getCityName() {
    	FacesContext context=FacesContext.getCurrentInstance();
		ApplicationDataBase a= (ApplicationDataBase)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		String n=(String)a.getCities().get(String.valueOf(cityId));
		if (n==null || cityId==ConstantsBase.ANYOPTION_CITY_ID)
			return cityName;
    	return n;
    }

	public void setCityName(String name) throws SQLException {

		cityId=AdminManagerBase.getCityIdByName(name);
		this.cityName = name;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getStreetNo() {
		return streetNo;
	}

	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "WithdrawForm ( "
	        + super.toString() + TAB
	        + "name = " + this.name + TAB
	        + "amount = " + this.amount + TAB
	        + "street = " + this.street + TAB
	        + "streetNo = " + this.streetNo + TAB
	        + "cityId = " + this.cityId + TAB
	        + "zipCode = " + this.zipCode + TAB
	        + "comments = " + this.comments + TAB
	        + "cityName = " + this.cityName + TAB
	        + "credit card = " +this.cardId + TAB
	        + " )";

	    return retValue;
	}

	/**
	 * Calculate the sum of the deposit and
	 * @return the formated amount of maximum withdrawal or "" in case we dont have any deposits to withrawal from
	 
	public String getMaxAmoutForDeltaPayChinaCCWithdrawal() throws Exception {
		User user = ApplicationData.getUserFromSession();
		sumOfDeltaPayDeposits = TransactionsManager.getDeltaPayWithdrawalAmount(user.getId());
		String formatedAmount = "";
		if (sumOfDeltaPayDeposits > 0) {
			String langCode = ApplicationData.getLanguage(user.getSkin().getDefaultLanguageId()).getCode();
            Locale locale = new Locale(langCode);
			formatedAmount = CommonUtil.displayAmount(sumOfDeltaPayDeposits, user.getCurrencyId());
			formatedAmount = CommonUtil.getMessage("error.maxwithdraw", new Object[] {formatedAmount}, locale);
		}
		return formatedAmount;
	}*/

	/**
	 * Check if the user have at least 1 cc that allow to withdrawal
	 * @return true if there is at least 1 credit card for withdrawal
	 */
	public boolean getIsHaveCcForWithdrawal() throws Exception {
		User user = ApplicationData.getUserFromSession();
		Limit limit = UsersManager.getLimitById(user.getLimitId());
		ccHash = TransactionsManager.getDisplayWithrawalCreditCardsByUser(user.getId(), limit.getMinWithdraw(), user.getSkinId(), true);

		if (ccHash.size() > 0) {
			isCcWithdrawalEnabled = true;
			return true;
		} else {
			isCcWithdrawalEnabled = false;
		}
		return false;
	}

	/**
	 * @return the cardId
	 */
	public String getCardId() {
		return cardId;
	}

	/**
	 * @param cardId the cardId to set
	 */
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	/**
	 * @return the cc withdrawal display list
	 * @throws Exception
	 */
	public ArrayList<SelectItem> getCcList() throws Exception {
		User user = ApplicationData.getUserFromSession();
		return TransactionsManagerBase.getDisplayWithrawalCreditCardsByUser(ccHash, user.getCurrency(), ConstantsBase.WEB_CONST);
	}

	public void initCUPWithdraw(long userId) throws Exception {
		cupMaxAmountWithdraw = TransactionsManager.getCUPWithdrawalAmount(userId, false)/100;
		logger.debug(" cupMaxAmountWithdraw:"+cupMaxAmountWithdraw);
	}

	public void initCUPWithdraw() throws Exception {
		User user = null;
		try{
			user = ApplicationData.getUserFromSession();
			if(user.getSkin().isRegulated()||user.getCurrencyId()!=ConstantsBase.CURRENCY_CNY_ID) {
				// no need to calculate 
				return;
			}
			if(user.getBalance() == 0){ 
				// actually take the current balance
				user.setBalance(UsersManager.getBalance(user.getId())); 
			}
		} catch (Exception e) {
			logger.debug("Can't init Cup withdraw - no user");
			return;
		}
		cupMaxAmountWithdraw = TransactionsManager.getCUPWithdrawalAmount(user.getId(), false)/100;
		long a = user.getBalance()/100;
		cupMaxAmountWithdraw = Math.min(a, cupMaxAmountWithdraw);
	}
	
	/**
	 * DeltaPay China withdrawal
	 * @return
	 * @throws Exception
	 */
	public String insertCUPWithdraw() throws Exception {

		// prevent identical requests from being processed
        if (this.saveTokenIsInvalid()) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();

        withdrawType = WITHDRAW_TYPE_CUP; // to save withdrawal type
        User user = ApplicationData.getUserFromSession();
        String error = null;
        if ( Long.parseLong(amount) > cupMaxAmountWithdraw) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.withdraw.highAmount", null),null);
			FacesContext.getCurrentInstance().addMessage("withdrawForm:amount", fm);
			error = "error.transaction.withdraw.highamount";
		} else {
			error = TransactionsManager.validateWithdrawForm(this.amount, user, Constants.ENUM_WITH_FEE, true,true, null, null, null, null, Writer.WRITER_ID_WEB);
		}

        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
        if (error == null) {
        	WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
        	if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
        		error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
        		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(error, null),null);
        		context.addMessage(null, fm);
        	}
        }
        
		if (error != null) { // if there is error
			TransactionsManager.insertCUPWithdraw(this.amount, error, user, CommonUtil.getWebWriterId(), false, loginId, null);
			if (error.equals("bonus.cannot.withdraw.error")) {
				bonusError = true;
			} else {
				bonusError = false;
			}
			return null;
		}

		return Constants.NAV_WITHDRAW_APPROVE;    // allways display general fee message
	}
	
	/**
	 * DeltaPay China withdrawal
	 * @return
	 * @throws Exception
	 */
	public String insertDeltaPayChinaWithdraw() throws Exception {

		// prevent identical requests from being processed
        if (this.saveTokenIsInvalid()) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();

        withdrawType = WITHDRAW_TYPE_DELTAPAY_CHINA; // to save withdrawal type
        User user = ApplicationData.getUserFromSession();

		String error = TransactionsManager.validateWithdrawForm(amount, user, Constants.ENUM_WITH_FEE, true,true, null, null, null, sumOfDeltaPayDeposits, Writer.WRITER_ID_WEB);
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		if (error == null) {
			WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
			if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
				error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(error, null),null);
				context.addMessage(null, fm);
			}
		}
		
		if (error != null) { // if there is error
			TransactionsManager.insertDeltaPayChinaWithdraw(amount, error, user, CommonUtil.getWebWriterId(), false, loginId);
			if (error.equals("bonus.cannot.withdraw.error")) {
				bonusError = true;
			} else {
				bonusError = false;
			}
			return null;
		}

		return Constants.NAV_WITHDRAW_APPROVE;    // allways display general fee message
	}

	/**
	 * Credit Card withdrawal
	 * @return
	 * @throws Exception
	 */
	public String insertCreditCardWithdraw() throws Exception {

		// prevent identical requests from being processed
        if (this.saveTokenIsInvalid()) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();

        withdrawType = WITHDRAW_TYPE_CC; // to save withdrawal type
        User user = ApplicationData.getUserFromSession();
        FacesContext context = FacesContext.getCurrentInstance();
        
        CreditCard card = ccHash.get(Long.valueOf(cardId));
        if (card.getType().getId()==3){
        	TransactionsManager.stopDinersWithdraw(card.getId(),user.getId());
        	setDiners(true);
        	setAmount(null);
			return null;
        }
		long ccMinWithdraw = TransactionsManagerBase.getCreditCardMinWithdraw(user.getCurrencyId());
		String error = null;
		if( CommonUtil.calcAmount(amount) <= ccMinWithdraw) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.minwithdraw", null),null);
			context.addMessage("withdrawForm:amount", fm);
			error = "error.transaction.withdraw.minamounnt";		
		}
		
		if(error == null) {
			error = TransactionsManager.validateWithdrawForm(amount, user, Constants.ENUM_WITH_FEE, true,false, card, null, null, null, Writer.WRITER_ID_WEB);
		}
		
		
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}

		if (null == error) {  // check card expiration
			error = TransactionsManagerBase.validateCardExpiration(card);
		}

		if (error == null) {
			WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
			if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
				error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(error, null),null);
				context.addMessage(null, fm);
			}
		}

		if (error != null) { // if there is error
			TransactionsManager.insertCreditCardWithdraw(amount, card,name,error,user,CommonUtil.getWebWriterId(),false, null, loginId, null);
			if (error.equals("bonus.cannot.withdraw.error")) {
				bonusError = true;
			} else {
				bonusError = false;
			}
			return null;
		}

		return Constants.NAV_WITHDRAW_APPROVE;    // allways display general fee message
	}

	/**
	 * @return withdrawType == WITHDRAW_TYPE_CC;
	 */
	public boolean isCcWithdrawal() {
		return withdrawType == WITHDRAW_TYPE_CC;
	}

	public boolean isDeltaPayWithdrawal() {
		return withdrawType == WITHDRAW_TYPE_DELTAPAY_CHINA;
	}
	
	public boolean isCdpayWithdrawal() {
		return withdrawType == WITHDRAW_TYPE_CDPAY;
	}

	/**
	 * @return withdrawType == WITHDRAW_TYPE_REGULAR;
	 */
	public boolean isRegularWithdrawal() {
		return withdrawType == WITHDRAW_TYPE_REGULAR;
	}

	/**
	 * @return withdrawType == WITHDRAW_TYPE_ENVOY;
	 */
	public boolean isEnvoyWithdrawal() {
		return withdrawType == WITHDRAW_TYPE_ENVOY;
	}
	
	public boolean isCUPWithdrawal() {
		return withdrawType == WITHDRAW_TYPE_CUP;
	}

	
	/**
	 * @return the isCcWithdrawalEnabled
	 */
	public boolean isCcWithdrawalEnabled() {
		return isCcWithdrawalEnabled;
	}

	/**
	 * @return withdrawType == WITHDRAW_TYPE_WEBMONEY;
	 */
	public boolean isWebMoneyWithdrawal() {
		return withdrawType == WITHDRAW_TYPE_WEBMONEY;
	}

	/**
	 * @param isCcWithdrawalEnabled the isCcWithdrawalEnabled to set
	 */
	public void setCcWithdrawalEnabled(boolean isCcWithdrawalEnabled) {
		this.isCcWithdrawalEnabled = isCcWithdrawalEnabled;
	}

	/**
	 * Return dinamic message for cc withdrawal approve
	 * @return
	 * 		formated message
	 * @throws Exception
	 */
	public String getCcApproveMessage() throws Exception {
		return composeApproveMessage("withdraw.cc.approve.message.low.amount", TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW);
	}

	/**
	 * Return dinamic message for envoy withdrawal approve
	 * @return
	 * 		formated message
	 * @throws Exception
	 */
	public String getEnvoyApproveMessage() throws Exception {
		return composeApproveMessage("withdraw.cc.approve.message.low.amount", TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW);
	}

	/**
	 * Return dinamic message for cheque withdrawal approve
	 * @return
	 * 		formated message
	 * @throws Exception
	 */
	public String getApproveMessage() throws Exception {
		return composeApproveMessage("withdraw.cc.approve.message.low.amount", TransactionsManagerBase.TRANS_TYPE_CHEQUE_WITHDRAW);
	}

	/**
	 * Return dinamic message for WebMoney withdrawal approve
	 * @return
	 * 		formated message
	 * @throws Exception
	 */
	public String getWebMoneyApproveMessage() throws Exception {
		return composeApproveMessage("withdraw.cc.approve.message.low.amount", TransactionsManagerBase.TRANS_TYPE_WEBMONEY_WITHDRAW);
	}

	private String composeApproveMessage(String approveMsg, int transactionType) throws Exception {
		User user = ApplicationData.getUserFromSession();
//		// Rewards plan
//		if (RewardManager.isHasBenefit(String.valueOf(RewardBenefit.REWARD_BENEFIT_EXEMPT_WITHDRAWAL_FEE), user.getId())) {
//			isHavingRewardExemptFee = true;
//			return CommonUtil.getMessage("rewards.free.commission.approve.message", null);
//		}
        long feeVal;
        String[] params = new String[2];

		if (amount == null) {
			String nav = getNavigationPageForBackButton();
			return nav;
		}
		long amountInCents = CommonUtil.calcAmount(amount);
		feeVal = TransactionsManager.getTransactionHomoFee(amountInCents, user.getCurrencyId(), transactionType);
        
		String fee = null;

		if (user.getSkinId() != Skin.SKIN_ARABIC) {
        	fee = CommonUtil.displayAmount(feeVal, user.getCurrencyId());
		} else {
			fee = CommonUtil.displayAmountForInput(feeVal, false, user.getCurrencyId().intValue()) + " " +
					CommonUtil.getMessage(user.getCurrency().getSymbol(),null) + " ";
		}

        params[0] = String.valueOf(fee);
		return CommonUtil.getMessage(approveMsg, params);
	}

	/**
	 * Get navigation page
	 * @return
	 * 		navigation String
	 * @throws Exception
	 */
	public String getNavigationPage() throws Exception {

		String nav = "";
        User user = ApplicationData.getUserFromSession();

        if (isEnvoyWithdrawal()){
        	nav = envoyResultPage;
        }else if (user.getSkinId() == Skin.SKIN_ETRADER) {
    		nav =  Constants.NAV_EMPTY_MYACCOUNT;
    	}else {
    		nav =  Constants.NAV_EMPTY_MYACCOUNT_WITHDRAW;
    	}

    	return nav;
	}
	
	public String getNavigationPageForBackButton() throws Exception {
		String nav = "";
		
		if (getIsHaveCcForWithdrawal() && cupMaxAmountWithdraw <= 0) {
			nav = Constants.NAV_WITHDRAW_CC;
			return nav;
		}
		if (cupMaxAmountWithdraw>0) {
			nav = Constants.NAV_WITHDRAW_DELTA_PAY;
			return nav;
		}
		if (!getIsHaveCcForWithdrawal() && cupMaxAmountWithdraw <= 0) {
			nav = Constants.NAV_WITHDRAW_WIRE;
			return nav;
		}
		return nav;
	}

	/**
	 * @return the ccHash
	 */
	public HashMap<Long, CreditCard> getCcHash() {
		return ccHash;
	}

	/**
	 * @param ccHash the ccHash to set
	 */
	public void setCcHash(HashMap<Long, CreditCard> ccHash) {
		this.ccHash = ccHash;
	}

	/**
	 * Check if need to display more withdrawal options: in case the user have
	 * credit card withdrawal option(Credit) with amount < balance and
	 * don't have cft credit card for withdrawal.
	 * @return true - in case we need to display link for another withdrawal option
	 * @throws Exception
	 */
	public boolean isWithdrawalMoreOptionNeeded() throws Exception {
		User user = ApplicationData.getUserFromSession();
		Iterator<Long> iter = ccHash.keySet().iterator();
		while(iter.hasNext()) {
			long ccId = iter.next();
			CreditCard card = ccHash.get(ccId);
			if (card.isCftAvailable()) {       // CFT - can withdrwal any amount
				return false;
			}
			if (!card.isCftAvailable() && card.getCreditAmount() >= user.getBalance()) { // Credit with creditAmount >= balance
				return false;
			}
		}
		return true;
	}

	/**
	 * @return the bonusError
	 */
	public boolean isBonusError() {
		return bonusError;
	}

	/**
	 * @param bonusError the bonusError to set
	 */
	public void setBonusError(boolean bonusError) {
		this.bonusError = bonusError;
	}

	/**
	 * insertEnvoyWithdraw
	 * @return
	 * @throws Exception
	 */
	public String insertEnvoyWithdraw() throws Exception {

		// prevent identical requests from being processed
        if (this.saveTokenIsInvalid()) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();
        withdrawType = WITHDRAW_TYPE_ENVOY;

        User user = ApplicationData.getUserFromSession();
		String error = TransactionsManager.validateWithdrawForm(amount, user, Constants.ENUM_WITH_FEE, false,true, null, null, null, null, Writer.WRITER_ID_WEB);
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		if (error == null) {
			WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
			if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
				error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(error, null),null);
				context.addMessage(null, fm);
			}
		}
		
		if (error != null) {
			TransactionsManager.insertWithdrawEnvoyService(amount, envoyWithdrawMethodId, user, CommonUtil.getWebWriterId(), false, error, envoyAccountNum, loginId, null);

			if (error.equals("bonus.cannot.withdraw.error")) {
				bonusError = true;
			} else {
				bonusError = false;
			}
			return null;
		}

		return envoyApprovePage;    // allways display general fee message
	}

	/**
	 * Insert WebMoney Withdraw
	 * @return
	 * @throws Exception
	 */
	public String insertWebMoneyWithdraw() throws Exception {
		// prevent identical requests from being processed
        if (this.saveTokenIsInvalid()) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();
        withdrawType = WITHDRAW_TYPE_WEBMONEY;
        User user = ApplicationData.getUserFromSession();
		String error = TransactionsManager.validateWithdrawForm(amount, user, Constants.ENUM_WITH_FEE, false,true, null, null, null, null, Writer.WRITER_ID_WEB);
	
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		if (error == null) {
			WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
			if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
				error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(error, null),null);
				context.addMessage(null, fm);
			}
		}
		
		if (error != null) {
			TransactionsManager.insertWebMoneyWithdraw(amount, error, user, CommonUtil.getWebWriterId(), false, webMoneyPurse, loginId, null);
			return null;
		}
		return Constants.NAV_WITHDRAW_APPROVE_WEBMONEY;
	}

	/**
	 * @return the envoyAccountNumList
	 */
	public ArrayList<SelectItem> getEnvoyAccountNumList() {
		return envoyAccountNumList;
	}

	/**
	 * @param envoyAccountNumList the envoyAccountNumList to set
	 */
	public void setEnvoyAccountNumList(ArrayList<SelectItem> envoyAccountNumList) {
		this.envoyAccountNumList = envoyAccountNumList;
	}

	/**
	 * @return the envoyAccountNum
	 */
	public String getEnvoyAccountNum() {
		return envoyAccountNum;
	}

	/**
	 * @param envoyAccountNum the envoyAccountNum to set
	 */
	public void setEnvoyAccountNum(String envoyAccountNum) {
		this.envoyAccountNum = envoyAccountNum;
	}

	/**
	 * @return the webMoneyPurseList
	 */
	public ArrayList<SelectItem> getWebMoneyPurseList() {
		return webMoneyPurseList;
	}

	/**
	 * @param webMoneyPurseList the webMoneyPurseList to set
	 */
	public void setWebMoneyPurseList(ArrayList<SelectItem> webMoneyPurseList) {
		this.webMoneyPurseList = webMoneyPurseList;
	}

	/**
	 * @return the webMoneyPurse
	 */
	public String getWebMoneyPurse() {
		return webMoneyPurse;
	}

	/**
	 * @param webMoneyPurse the webMoneyPurse to set
	 */
	public void setWebMoneyPurse(String webMoneyPurse) {
		this.webMoneyPurse = webMoneyPurse;
	}

	public int getCcListSize() throws Exception {
		return getCcList().size();
	}


	public boolean isDiners() {
		return diners;
	}

	public void setDiners(boolean diners) {
		this.diners = diners;
	}

	public long getCUPcreditAmountAllow() {
		return cupMaxAmountWithdraw;
	}

	public void setCUPcreditAmountAllow(long cUPcreditAmountAllow) {
		cupMaxAmountWithdraw = cUPcreditAmountAllow;
	}
	
	/**
	 * Insert CDPay Withdraw
	 * @return
	 * @throws Exception
	 */
	public String insertCDPayWithdraw() throws Exception {
		// prevent identical requests from being processed
        if (this.saveTokenIsInvalid()) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();
        withdrawType = WITHDRAW_TYPE_CDPAY;
        User user = ApplicationData.getUserFromSession();
		String error = TransactionsManager.validateWithdrawForm(amount, user, Constants.ENUM_WITH_FEE, false,true, null, null, null, null, Writer.WRITER_ID_WEB);
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		if (error == null) {
			WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
			if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
				error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(error, null),null);
				context.addMessage(null, fm);
			}
		}
		
		if (error != null) {
			TransactionsManager.insertCDPayWithdraw(amount, error, user, CommonUtil.getWebWriterId(), false, loginId);
			return null;
		}
		return Constants.NAV_WITHDRAW_APPROVE_CDPAY;
	}
	
	/**
	 * Return dinamic message for CDPay withdrawal approve
	 * @return
	 * 		formated message
	 * @throws Exception
	 * 
	 */
	public String getCdpayApproveMessage() throws Exception {
		return composeApproveMessage("withdraw.cc.approve.message.low.amount", TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_WITHDRAW);
	}

	public long getCupMaxAmountWithdraw() {
		
		try {
			initCUPWithdraw();
		} catch (Exception e) {
			logger.debug("Can't get cupMaxAmountWithdraw " + e);
		}

		return cupMaxAmountWithdraw;
	}

	public void setCupMaxAmountWithdraw(long cupMaxAmountWithdraw) {
		this.cupMaxAmountWithdraw = cupMaxAmountWithdraw;
	}

	/**
	 * Insert BaroPay Withdraw
	 * @return
	 * @throws Exception
	 */
	public String insertBaroPayWithdraw() throws Exception {
		// prevent identical requests from being processed
        if (this.saveTokenIsInvalid()) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();
        withdrawType = WITHDRAW_TYPE_BARO_PAY; // to save withdrawal type
        User user = ApplicationData.getUserFromSession();
		String error = TransactionsManager.validateWithdrawForm(amount, user, Constants.ENUM_WITH_FEE, false,true, null, null, null, null, Writer.WRITER_ID_WEB);
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		if (error == null) {
			WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
			if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
				error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(error, null),null);
				context.addMessage(null, fm);
			}
		}
		
		if (error != null) {
			TransactionsManager.insertBaroPayWithdraw(amount, error, user, CommonUtil.getWebWriterId(), false, bankNameTxt, accountOwnerTxt, accountNum, loginId, null);
			if (error.equals("bonus.cannot.withdraw.error")) {
				bonusError = true;
			} else {
				bonusError = false;
			}
			return null;
		}
		return Constants.NAV_WITHDRAW_APPROVE;
	}
	
	public boolean isHaveBaroPaySuccessDeposit() throws Exception {
		User user = ApplicationData.getUserFromSession();
		try {			
			return BaroPayManagerBase.isHaveBaroPaySuccessDeposit(user.getId()); 
		} catch (Exception e) {
			logger.error("Error while getting BaroPay Success Deposits for UserId:" + user.getId(), e);
		}
		return false;
	}
	
	/**
	 * @return withdrawType == WITHDRAW_TYPE_BARO_PAY;
	 */
	public boolean isBaroPayWithdrawal() {
		return withdrawType == WITHDRAW_TYPE_BARO_PAY;
	}

	/**
	 * @return the bankNameTxt
	 */
	public String getBankNameTxt() {
		return bankNameTxt;
	}

	/**
	 * @param bankNameTxt the bankNameTxt to set
	 */
	public void setBankNameTxt(String bankNameTxt) {
		this.bankNameTxt = bankNameTxt;
	}

	/**
	 * @return the accountOwnerTxt
	 */
	public String getAccountOwnerTxt() {
		return accountOwnerTxt;
	}

	/**
	 * @param accountOwnerTxt the accountOwnerTxt to set
	 */
	public void setAccountOwnerTxt(String accountOwnerTxt) {
		this.accountOwnerTxt = accountOwnerTxt;
	}

	/**
	 * @return the accountNum
	 */
	public String getAccountNum() {
		return accountNum;
	}

	/**
	 * @param accountNum the accountNum to set
	 */
	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}

	public Long getUserAnswerId() {
		return userAnswerId;
	}

	public void setUserAnswerId(Long userAnswerId) {
		this.userAnswerId = userAnswerId;
	}

	public String getTextAnswer() {
		return textAnswer;
	}

	public void setTextAnswer(String textAnswer) {
		this.textAnswer = textAnswer;
	}
	
}