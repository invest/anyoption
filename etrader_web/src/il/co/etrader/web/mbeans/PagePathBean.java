package il.co.etrader.web.mbeans;

import java.io.Serializable;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import il.co.etrader.web.util.Constants;
import il.co.etrader.web.util.UAgentInfo;

/**
 * Page Path Bean for pretty
 * @author eyalo
 */
public class PagePathBean implements Serializable{

	private static final long serialVersionUID = 1L;

	/**
	 * getTemplateFolderName
	 * @return template folder name
	 */
	public String getPathByDetectDevice(String pageName) {
        FacesContext context = FacesContext.getCurrentInstance();
        		
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
        UAgentInfo uagentInfo = null;
        String path = "/jsp/" + pageName;
        String requestUrl = request.getRequestURL().toString();
        if (session != null) {        	
        	uagentInfo = (UAgentInfo) session.getAttribute(Constants.UAGENTINFO);
        	if (null == uagentInfo) {
        		uagentInfo = new UAgentInfo(context);
        		session.setAttribute(Constants.UAGENTINFO, uagentInfo);
        	}
//        	if (uagentInfo.initCompleted && (uagentInfo.isTierIphone || uagentInfo.isTierTablet) && !(requestUrl.indexOf(Constants.HOST_ETRADER) > -1)) {
//        		return "/mobile_ng/index.html";
//        	}
        }
        return path;
	}

	/**
	 * get Trade Binary Options page
	 * @return Page Path
	 */
	public String getTradeBinaryOptions() {
		return getPathByDetectDevice("trade_binary_options.jsf");
	}

	/**
	 * get About Us page
	 * @return Page Path
	 */
	public String getAboutUs() {
		return getPathByDetectDevice("aboutus.jsf");
	}

	/**
	 * get Asset Index page
	 * @return Page Path
	 */
	public String getAssetIndex() {
		return getPathByDetectDevice("assetIndex.jsf");
	}

	/**
	 * get Faq page
	 * @return Page Path
	 */
	public String getFaq() {
		return getPathByDetectDevice("faq.jsf");
	}

	/**
	 * get Benefits page
	 * @return Page Path
	 */
	public String getBenefits() {
		return getPathByDetectDevice("benefits-of-binary-option-trading.jsf");
	}

    /**
     * get index page
     * @return Page Path
     */
    public String getIndex() {
        return getPathByDetectDevice("index.jsf");
    }

    /**
     * get privacy page
     * @return Page Path
     */
    public String getPrivacy() {
        return getPathByDetectDevice("privacy.jsf");
    }

    /**
     * get strategy page
     * @return Page Path
     */
    public String getStrategy() {
        return getPathByDetectDevice("strategy.jsf");
    }

    /**
     * get what are binary options page
     * @return Page Path
     */
    public String getWhatAreBinaryOptions() {
        return getPathByDetectDevice("what-are-binary-options.jsf");
    }

    /**
     * get live page
     * @return Page Path
     */
    public String getLive() {
        return getPathByDetectDevice("live.jsf");
    }
    
    /**
	 * get How To Trade page
	 * @return Page Path
	 */
	public String getHowToTrade() {
		return getPathByDetectDevice("how-to-trade-binary-options.jsf");
	}
	
	/**
	 * get security page
	 * @return Page Path
	 */
	public String getSecurity() {
		return getPathByDetectDevice("security.jsf");
	}
	
	/**
	 * get contactUs page
	 * @return Page Path
	 */
	public String getContactUs() {
		return getPathByDetectDevice("contactus.jsf");
	}
	
	/**
	 * get banking page
	 * @return Page Path
	 */
	public String getBanking() {
		return getPathByDetectDevice("banking.jsf");
	}
	
	/**
	 * get register page
	 * @return page path
	 */
	public String getRegister() {
		return getPathByDetectDevice("register.jsf");
	}


}
