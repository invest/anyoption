package il.co.etrader.web.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.BankBranches;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.QuestionnaireGroup;
import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.payments.WithdrawEntryInfo;
import com.anyoption.common.payments.WithdrawEntryResult;
import com.anyoption.common.payments.WithdrawUtil;
import com.anyoption.common.util.Utils;

import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.WireBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.TransactionsManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;
import il.co.etrader.web.util.FormBase;

public class WireForm extends FormBase  implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = -2501268342307298932L;

	private static final Logger logger = Logger.getLogger(WireForm.class);

	private WireBase wire = new WireBase();
	private long bankWireWithdrawalId;
	private String bankOtherNameTxt;

	// Rewards plan
	private boolean isHavingRewardExemptFee;

	//Withdraw survey
	private Long userAnswerId;
	private String textAnswer;
	
	public WireForm () throws Exception {
		//wire = new WireBase();
		isHavingRewardExemptFee = false;
	}


	/**
	 * Clear wire form
	 */
	public void clearWireForm() {
		wire = new WireBase();
	}

	public String getInitWireForm() throws Exception{
		wire= new WireBase();
		return Constants.NAV_EMPTY_MYACCOUNT_WIRE;
	}

	/**
	 * @return the wire
	 */
	public WireBase getWire() {
		return wire;
	}

	/**
	 * @param wire the wire to set
	 */
	public void setWire(WireBase wire) {
		this.wire = wire;
	}


	/**
	 * Insert a new Bank Wire request
	 * @return
	 * @throws SQLException
	 */
	public String insert() throws Exception {

		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData gm = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		
		if (gm.getSkinId() != Skin.SKIN_ETRADER) {
			if (Utils.isParameterEmptyOrNull(wire.getBankNameTxt())) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("error.mandatory",null),null);
	            context.addMessage("wireForm:bankNameTxt", fm);
	            return null;
			}
		}
		
		if (gm.getSkinId() == Skin.SKIN_ETRADER) {
            boolean isGoodbranchCode = false;
            for (BankBranches bankBranch : ApplicationData.getBankBranches()) {
                if (wire.getBankId().equalsIgnoreCase(bankBranch.getBankId().toString()) && //check that this bank and branch code are real
                        wire.getBranch().equals(bankBranch.getBranchCode().toString())) {
                    isGoodbranchCode = true;
                    break;
                }
            }
            if (!isGoodbranchCode) {
                FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("error.branch.code",null),null);
                context.addMessage("wireForm:branchNumber", fm);
                return null;
            }

            for (SelectItem bank : gm.getBanksSI()) {
                if (wire.getBankId().equals(bank.getValue().toString())) {
                    wire.setBankNameTxt(bank.getLabel().toString());
                    break;
                }
            }

        }

        // prevent identical requests from being processed

        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();

        User user = ApplicationData.getUserFromSession();

        String error = TransactionsManager.validateWireForm(wire.getAmount(), user, Constants.ENUM_WITH_FEE,true, Writer.WRITER_ID_WEB);
        
		if (error == null) {
			WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
			if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
				error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(error, null),null);
				context.addMessage(null, fm);
			}
		}

		if (error != null) {
			TransactionsManager.insertBankWireWithdraw(wire, error, user, CommonUtil.getWebWriterId(), false, false, loginId, null);
			return null;
		}

		// add wire to contex
        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_WIRE, WireBase.class);
        ve.getValue(context.getELContext());
        ve.setValue(context.getELContext(), wire);
        session.setAttribute("userAnswerId", userAnswerId);
        session.setAttribute("textAnswer", textAnswer);
        session.setAttribute("wireAmount", wire.getAmount());
		return Constants.NAV_BANK_WIRE_APPROVE;
	}

	/**
	 * Approve bank wire request with fee
	 * @return
	 * @throws Exception
	 */
	public String approve() throws Exception {

		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		wire = (WireBase)context.getApplication().createValueBinding(Constants.BIND_WIRE).getValue(context);
     
		Long userAnswerId = (Long)   session.getAttribute("userAnswerId");
	   	String textAnswer = (String) session.getAttribute("textAnswer");
		
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}

		String amount = wire.getAmount();
		if (amount == null) {
			return Constants.NAV_WITHDRAW_WIRE;
		}

		ApplicationData gm =(ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);

		//	prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, Constants.NAV_WITHDRAW_APPROVE);
        }
        this.generateSaveToken();

        User user = ApplicationData.getUserFromSession();

        String error;
       	error = TransactionsManager.validateWireForm(amount, user, Constants.ENUM_WITH_FEE,true, Writer.WRITER_ID_WEB);

		if (error != null) {
	     	FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("error.unexpected",null),null);
	     	context.addMessage(null, fm);

	     	clearWireForm();
			return Constants.NAV_EMPTY_MYACCOUNT_WIRE;
		}

		// cancel all bonuses that can be canceled or return error if something goes wrong
		List<BonusUsers> bonusUsers = BonusManagerBase.getAllUserBonus(	user.getId(), null, null, 0, CommonUtil.getUtcOffset(),
																		new ArrayList<>(Arrays.asList(CurrenciesManagerBase.getCurrency(user.getCurrencyId()))));
		for (BonusUsers bu : bonusUsers) {
			if (bu.isCanCanceled()) {
				error = BonusManagerBase.cancelBonus(	bu, CommonUtil.getUtcOffset(), Writer.WRITER_ID_WEB, user.getSkinId(), user, loginId,
														CommonUtil.getIPAddress());
			}
			if (error != null && error.equals(BonusManagerBase.ERROR_DUMMY)) {
				logger.error("Cannot cancel bonus with id: " + bu.getId());
				FacesMessage fm = new FacesMessage(	FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("error.creditcard.unapproved", null),
													null);
				context.addMessage(null, fm);
				clearWireForm();
				return getNavigationPage();
			} else {
				error = null;
			}
		}

		QuestionnaireUserAnswer questUserAnswer = null;
		if (userAnswerId != null && userAnswerId > 0) {
			questUserAnswer = new QuestionnaireUserAnswer();
			questUserAnswer.setAnswerId(userAnswerId);
			questUserAnswer.setTimeCreated(new Date());
			questUserAnswer.setGroupId(QuestionnaireGroup.WITHDRAW_SURVEY_ID);
			questUserAnswer.setQuestionId(QuestionnaireQuestion.WITHDRAW_SURVEY_QUESTION);
			if (!CommonUtil.isParameterEmptyOrNull(textAnswer)) {
				questUserAnswer.setTextAnswer(textAnswer);
			}
			questUserAnswer.setUserId(user.getId());
			questUserAnswer.setWriterId(Writer.WRITER_ID_WEB);
		}
		TransactionsManager.insertBankWireWithdraw(wire, error, user, gm.getWriter(), false, false, loginId, questUserAnswer);

		long amountLong = CommonUtil.calcAmount(amount);
		long feeVal = TransactionsManagerBase.getFeeByTranTypeId(user.getLimitIdLongValue(), TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW);
 	    String[] params = new String[2];


        if (user.getSkinId() != Skin.SKIN_ARABIC){
        	params[0] = CommonUtil.displayAmount(amountLong, user.getCurrencyId());
            params[1] = CommonUtil.displayAmount(feeVal, user.getCurrencyId());
		}else{
			params[0] = CommonUtil.displayAmountForInput(amountLong, false, user.getCurrencyId().intValue()) + " " + CommonUtil.getMessage(user.getCurrency().getSymbol(),null) + " ";
	        params[1] = CommonUtil.displayAmountForInput(feeVal, false, user.getCurrencyId().intValue()) + " " + CommonUtil.getMessage(user.getCurrency().getSymbol(),null) + " ";
		}

        String msg = CommonUtil.getMessage("bank.wire.success",params);
        // Rewards plan
 		if (isHavingRewardExemptFee) {
 			msg = CommonUtil.getMessage("rewards.free.commission.bank.wire.success",params);
 		}
     	FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null);
     	context.addMessage(null, fm);

     	WithdrawEntryResult postWithdrawEntryResult = WithdrawUtil.postWithdrawEntry(new WithdrawEntryInfo(user));
     	
     	session.removeAttribute("userAnswerId");
     	session.removeAttribute("textAnswer");
     	clearWireForm();



		return getNavigationPage();
	}

	/**
	 * Cancle bank wire request
	 * @return
	 * @throws SQLException
	 */
	public String notApprove() throws Exception{

		FacesContext context=FacesContext.getCurrentInstance();
		//	 prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, Constants.NAV_EMPTY_MYACCOUNT_WIRE);
        }
        this.generateSaveToken();

	    String msg = CommonUtil.getMessage("bank.wire.cancel",null);
     	FacesMessage fm=new FacesMessage(FacesMessage.SEVERITY_INFO,msg,null);
     	context.addMessage(null, fm);

     	wire.setAmount(null);

		return getNavigationPage();
	}


	/**
	 * Return dinamic message for bank wire approve
	 * @return
	 * 		formated message
	 * @throws Exception
	 */
	public String getBankWireApproveMessage() throws Exception {
        User user = ApplicationData.getUserFromSession();
        FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData gm = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
//        // Rewards plan
// 		ArrayList<Long> rewardBenefitList = new ArrayList<Long>();
// 		rewardBenefitList.add(RewardBenefit.REWARD_BENEFIT_WIRE_TRANSFER_FREE_COMMISSION);
// 		rewardBenefitList.add(RewardBenefit.REWARD_BENEFIT_EXEMPT_WITHDRAWAL_FEE);
// 		if (RewardManager.isHasBenefit(CommonUtil.getArrayToString(rewardBenefitList), user.getId())) {
// 			isHavingRewardExemptFee = true;
// 			return CommonUtil.getMessage("rewards.free.commission.approve.message", null);
// 		}
		//String maxAmount = CommonUtil.displayAmount(TransactionsManager.getMaxAmountForBankWireFee(user.getSkinId(), user.getCurrencyId()), user.getCurrency());
        long feeVal;
        String wireValue = session.getAttribute("wireAmount").toString();
        String[] params = new String[2];

		if (wireValue == null || wireValue.isEmpty()) {
			String nav = Constants.NAV_EMPTY_MYACCOUNT_WIRE;;
			return nav;
		}
		long amountInCents = CommonUtil.calcAmount(wireValue);
		feeVal = TransactionsManager.getTransactionHomoFee(amountInCents, user.getCurrencyId(), TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW);
        
		String fee = null;

		if (user.getSkinId() != Skin.SKIN_ARABIC) {
        	fee = CommonUtil.displayAmount(feeVal, user.getCurrencyId());
		} else {
			fee = CommonUtil.displayAmountForInput(feeVal, false, user.getCurrencyId().intValue()) + " " +
					CommonUtil.getMessage(user.getCurrency().getSymbol(),null) + " ";
		}

        params[0] = String.valueOf(fee);
		return CommonUtil.getMessage("withdraw.cc.approve.message.low.amount", params);
	}

    /**
     * Get navigation page
     * @return
     *      navigation String
     * @throws Exception
     */
    public String getNavigationPage() throws Exception {

        String nav = null;
        User user = ApplicationData.getUserFromSession();

        if (user.getSkinId() == Skin.SKIN_ETRADER) {
            nav =  Constants.NAV_EMPTY_MYACCOUNT;
        } else {
            nav =  Constants.NAV_EMPTY_MYACCOUNT_WIRE;
        }
        return nav;
    }

    /**
     * get bank wire withdrawal.
     * @return bankWireWithdrawal as SelectItem
     * @throws Exception
     */
    public ArrayList<SelectItem> getBankWireWithdrawal() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData app = (ApplicationData)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		ArrayList<SelectItem> bankWireWithdrawal = new ArrayList<SelectItem>();
		bankWireWithdrawal.addAll(CommonUtil.translateSI(ApplicationData.getSkinById(app.getSkinId()).getBankWireWithdraw()));
    	return bankWireWithdrawal;
	}


	/**
	 * @return the bankWireWithdrawalId
	 */
	public long getBankWireWithdrawalId() {
		return bankWireWithdrawalId;
	}


	/**
	 * @param bankWireWithdrawalId the bankWireWithdrawalId to set
	 */
	public void setBankWireWithdrawalId(long bankWireWithdrawalId) {
		this.bankWireWithdrawalId = bankWireWithdrawalId;
		wire.setBankId(String.valueOf(bankWireWithdrawalId));
		try {
			ArrayList<SelectItem> bankWireWithdraw = getBankWireWithdrawal();
			for (SelectItem si : bankWireWithdraw) {
				if (bankWireWithdrawalId == (Long)si.getValue()) {
					wire.setBankNameTxt(si.getLabel());
					break;
				}
			}
		} catch (Exception e) {
			logger.error("error, can't get label from bank wire withdrawal", e);
		}
	}


	/**
	 * @return the bankOtherNameTxt
	 */
	public String getBankOtherNameTxt() {
		return bankOtherNameTxt;
	}


	/**
	 * @param bankOtherNameTxt the bankOtherNameTxt to set
	 */
	public void setBankOtherNameTxt(String bankOtherNameTxt) {
		this.bankOtherNameTxt = bankOtherNameTxt;
		if (getBankWireWithdrawalId() == ConstantsBase.BANK_WIRE_OTHER_BANK) {
			wire.setBankNameTxt(bankOtherNameTxt);
		}
	}
	
	public void validateWireAmount(FacesContext context, UIComponent comp, Object value) {
		try {
			User user = ApplicationData.getUserFromSession();
			String amount = (String) value;
			amount.replaceAll("\\s", "");
			String regex = "^\\d+([\\.]?\\d+)?$";
			if (!amount.matches(regex)) {
				((UIInput) comp).setValid(false);
				FacesMessage message = new FacesMessage(CommonUtil.getMessage("javax.faces.validator.LongRangeValidator.TYPE_detail", null));
				context.addMessage(comp.getClientId(context), message);
			}
		} catch (Exception e) {
			((UIInput) comp).setValid(false);
			FacesMessage message = new FacesMessage("Invalid number");
			context.addMessage(comp.getClientId(context), message);
			logger.error("Can not get User from session, while validating wire amount.", e);
		}
	}
    
	public Long getUserAnswerId() {
		return userAnswerId;
	}

	public void setUserAnswerId(Long userAnswerId) {
		this.userAnswerId = userAnswerId;
	}

	public String getTextAnswer() {
		return textAnswer;
	}

	public void setTextAnswer(String textAnswer) {
		this.textAnswer = textAnswer;
	}
}
