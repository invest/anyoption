package il.co.etrader.web.mbeans;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.OpportunitiesManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.MarketRate;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.charts.LevelHistoryCache;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.managers.FeesManagerBase;
import com.anyoption.common.managers.OpportunitiesManagerBase;
import com.anyoption.common.util.OpportunityCache;
import com.anyoption.common.util.OpportunityCacheBean;

@SuppressWarnings("serial")
public class Binary0100DataForm implements Serializable {
    private static final Logger log = Logger.getLogger(Binary0100DataForm.class);

    private long marketId;
    private long oppId;
    private long fee;
    private int currencyId;
    private OpportunityCacheBean opportunity;

    public String getFillParams() throws SQLException {
        long t = System.currentTimeMillis();
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> pm = fc.getExternalContext().getRequestParameterMap();
        if (null != pm.get("marketId")) {
            marketId = Long.parseLong((String) pm.get("marketId"));
        }
        
    	HttpServletRequest request = (HttpServletRequest) fc.getExternalContext().getRequest();
    	if (request.getRemoteUser() != null) {
           	User user = (User) fc.getApplication().createValueBinding(Constants.BIND_USER).getValue(fc);
           	currencyId =  user.getCurrencyId().intValue();
           	fee = FeesManagerBase.getFee(FeesManagerBase.ZERO_ONE_HUNDRED_COMMISSION_FEE, currencyId);
            // first try to get the last trade event
           	if (marketId == 0) {
               	Opportunity o = OpportunitiesManager.getUsersLastTradeBinary0100Event(user.getId());
               	if (null != o) {
               	    log.debug("Found last trade event.");
               	    marketId = o.getMarketId();
               	    oppId = o.getId();
               	}
           	}
    	}
        
        // default market select if no last trade
        if (marketId == 0) {
            log.debug("marketId is 0. Taking Binary 0-100 default market.");
            ApplicationData ap = (ApplicationData) fc.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(fc);
            Long skinId = ap.getSkinId();
            marketId = OpportunitiesManagerBase.getBinary0100CurrentMarket(skinId);
        }
        if (oppId == 0 && null != pm.get("oppId")) {
            oppId = Long.parseLong((String) pm.get("oppId"));
        }
        if (oppId == 0) {
            log.debug("oppId is 0. Taking market 1st above opp.");
            oppId = OpportunitiesManager.getMarketCurrentOpportunityBinary0100(marketId);
        }
        log.debug("getFillParams - marketId: " + marketId + " oppId: " + oppId + " fee: "+ fee + " time to fill: " + (System.currentTimeMillis() - t));
        return "";
    }
    
    public Date getCurrentTime() {
        return new Date();
    }
    
    public long getUtcOffsetInMin() {
        return CommonUtil.getUtcOffsetInMin(CommonUtil.getUtcOffset());
    }
    
    public long getMarketId() {
        return marketId;
    }
    
    public long getOppId() {
        return oppId;
    }
    
    public long getFee() {
		return fee;
	}
    public String getFeeTxt() {
    	return CommonUtil.displayAmount(""+fee, currencyId);
    }
	public OpportunityCacheBean getOpportunity() throws SQLException {
        if (null == opportunity && oppId != 0) {
            FacesContext fc = FacesContext.getCurrentInstance();
            ServletContext sc = (ServletContext) fc.getExternalContext().getContext();
            OpportunityCache oc = (OpportunityCache) sc.getAttribute(Constants.OPPORTUNITY_CACHE);
            opportunity = oc.getOpportunity(oppId);
        }
        return opportunity;
    }
    
    public List<MarketRate> getHistory() throws SQLException {
        FacesContext fc = FacesContext.getCurrentInstance();
        ServletContext sc = (ServletContext) fc.getExternalContext().getContext();
        HttpServletRequest req = (HttpServletRequest) fc.getExternalContext().getRequest();
        LevelHistoryCache lhc = (LevelHistoryCache) sc.getAttribute("levelHistoryCache");
        
        Map<SkinGroup, List<MarketRate>> ratesMap = lhc.getMarketHistory(marketId);
        List<MarketRate> l = null;
        if (ratesMap != null) {
        	l = ratesMap.get(ApplicationData.getSkinById(ApplicationData.getSkinId(req.getSession(), req)).getSkinGroup());
        }
        log.debug("got rates from history: " + (null != l ? l.size() : 0));
        OpportunityCacheBean o = getOpportunity();
        if (null != l && l.size() > 0 && null != o) {
            while (l.size() > 0 && l.get(0).getRateTime().getTime() < o.getTimeFirstInvest().getTime()) {
                l.remove(0);
            }
        }
        log.debug("return rates: " + (null != l ? l.size() : 0));
        return l;
    }
    
    public int getMaxContracts() {
        FacesContext fc = FacesContext.getCurrentInstance();
        ApplicationData ap = (ApplicationData) fc.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(fc);
        return (int) Math.floor(10.0 / (CommonUtil.convertToBaseAmount(10, ap.getRealCurrencyId(), new Date()) / 10.0));
    }
}