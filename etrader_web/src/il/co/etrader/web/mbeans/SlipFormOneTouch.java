package il.co.etrader.web.mbeans;

import java.util.ArrayList;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.managers.UsersManagerBase;

import il.co.etrader.bl_vos.InvestmentLimit;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.InvestmentsManager;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.bl_vos.UserRegulation;
import il.co.etrader.web.util.Constants;

public class SlipFormOneTouch extends SlipForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 558204492569955260L;

	private static Logger log = Logger.getLogger(SlipFormOneTouch.class);

    private int oneTouchUnitPriceAO;
    private ArrayList<SelectItem> unitsSelect;
    private long oneTouchAOInvId = 0;//holds opp id after successful investment
    private int oneTouchAOunits = 2;
    private double requestAmountAO = 0;
    private long returningOppId = 0; // for 1T in AO : to know the ID off the OPP that was inserted
    private double lastInvCurrentLevel;
    private int lastInvUnits;
    private boolean loginErrorFlag1TAO = false;
    private long maxInv1Tunits = 0;
    private InvestmentLimit il;
    private boolean submitingFlag = false;
    private boolean regulationError = false;
    private String regulationMessageTxt;
    private String regulationLinkValue;
    private boolean showCancelInvestment = false;

	/**
	 * Flag that is true if the error is because of low user balance. It is set in the validation of investment through the slip form
	 */
    private boolean lowBalanceError;


	public SlipFormOneTouch() throws Exception {
		super();
		FacesContext context = FacesContext.getCurrentInstance();
        ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
        Skins skin = ApplicationData.getSkinById(ap.getSkinId());
        Long countryId =  Long.valueOf(skin.getDefaultCountryId());
        User user = null;
        if (context.getExternalContext().isUserInRole("web")) {
        	user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        }
        if (null != ap.getRealCountryId()) {
            countryId = Long.valueOf(ap.getRealCountryId());
        }

        oneTouchUnitPriceAO = ConstantsBase.ONE_TOUCH_AO_UNIT_PRICE;//each unit in 1T AO = $50
        if (ap.getSkinId() == Skin.SKIN_TLV) { //in TLV unit is like ET 500
            oneTouchUnitPriceAO = ConstantsBase.ONE_TOUCH_TLV_UNIT_PRICE;
        }
        if (skin.getId() == Skin.SKIN_RUSSIAN && null != countryId &&
        		countryId.toString().equals(Constants.RUSSIA_COUNTRY_ID) || (null != user && user.getCurrencyId() == ConstantsBase.CURRENCY_RUB_ID)) {
        	oneTouchUnitPriceAO = ConstantsBase.ONE_TOUCH_AO_RUB_UNIT_PRICE;
    	}
        if (skin.getId() == Skin.SKIN_CHINESE && null != countryId &&
        		countryId.toString().equals(Constants.CHINA_COUNTRY_ID) || (null != user && user.getCurrencyId() == ConstantsBase.CURRENCY_CNY_ID)) {
        	oneTouchUnitPriceAO = ConstantsBase.ONE_TOUCH_AO_CNY_UNIT_PRICE;
    	}
        if (skin.getId() == Skin.SKIN_TURKISH && null != countryId &&
        		countryId.toString().equals(Constants.TURKEY_COUNTRY_ID) || (null != user && user.getCurrencyId() == ConstantsBase.CURRENCY_TRY_ID)) {
        	oneTouchUnitPriceAO = ConstantsBase.ONE_TOUCH_AO_TRY_UNIT_PRICE;
    	}
        if (skin.getId() == Skin.SKIN_KOREAN && null != countryId &&
        		countryId.toString().equals(Constants.SOUTH_KOREA_COUNTRY_ID) || (null != user && user.getCurrencyId() == ConstantsBase.CURRENCY_KRW_ID)) {
        	oneTouchUnitPriceAO = ConstantsBase.ONE_TOUCH_AO_KR_UNIT_PRICE;
    	}
        if (skin.getId() == Skin.SKIN_SWEDISH_REG && null != countryId &&
        		countryId == Constants.COUNTRY_ID_SWEDEN || (null != user && user.getCurrencyId() == ConstantsBase.CURRENCY_SEK_ID)) {
        	oneTouchUnitPriceAO = ConstantsBase.ONE_TOUCH_AO_SEK_UNIT_PRICE;
    	}
        
        if (null != user && user.getCurrencyId() == ConstantsBase.CURRENCY_ZAR_ID) {
        	oneTouchUnitPriceAO = ConstantsBase.ONE_TOUCH_AO_ZAR_UNIT_PRICE;
    	}
        
        if (null != user && user.getCurrencyId() == ConstantsBase.CURRENCY_CZK_ID) {
        	oneTouchUnitPriceAO = ConstantsBase.ONE_TOUCH_AO_CZK_UNIT_PRICE;
    	}
        
        if (null != user && user.getCurrencyId() == ConstantsBase.CURRENCY_PLN_ID) {
        	oneTouchUnitPriceAO = ConstantsBase.ONE_TOUCH_AO_PLN_UNIT_PRICE;
    	}
        
        if (ap.getSkinId() == Skin.SKIN_ETRADER) {
            oneTouchUnitPriceAO = ConstantsBase.ONE_TOUCH_ET_UNIT_PRICE;
        }

        il = InvestmentsManager.getInvestmentLimit(Opportunity.TYPE_ONE_TOUCH, 0, 0, ap.getRealCurrencyId(), 0);
        maxInv1Tunits = il.getMaxAmount() / 100 / oneTouchUnitPriceAO;
        setUnitsSelect();
        updateList();
        lowBalanceError = new Boolean(false);
	}
	
	public ArrayList getOneTouchList() {
		return OneTouchList;
	}

	public void setOneTouchList(ArrayList oneTouchList) {
		OneTouchList = oneTouchList;
	}

	public void updateOneTouchListAO()throws Exception{
		oneTouchAOInvId  = 0;
		setOneTouchAOunits(lastInvUnits);
		updateList();
	}

	public String updateList() throws Exception{
		unitsSelect = new ArrayList<SelectItem>();
		setUnitsSelect();
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		OneTouchList = InvestmentsManager.getOneTouch(ap.getSkinId(), marketId, ap.getUserLocale());
		log.debug("getting opportunities for one touch: " + marketId);

		return null;
	}

	public String submitOneTouchFromSlipAO(){
		if (!submitingFlag ){
		submitingFlag = true;

		String result = "";
		FacesContext context = FacesContext.getCurrentInstance();
		// clear the flag for the next submit
		lowBalanceError = false;

		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			log.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		log.debug("1TOUCH FROM AO : "+request.getParameter("oneTouchAO:optionId") + " " + request.getParameter("oneTouchAO:price") + " "+
				   request.getParameter("oneTouchAO:odds_win")+" "+ request.getParameter("oneTouchAO:odds_lose")+" "+
				   request.getParameter("oneTouchAO:pageLvl")+ " "+request.getParameter("oneTouchAO:units"));
		long id = Long.parseLong(request.getParameter("oneTouchAO:optionId"));
		double requestAmount = oneTouchUnitPriceAO * Double.parseDouble(request.getParameter("oneTouchAO:units"));
		requestAmountAO = requestAmount;
		lastInvUnits = Integer.parseInt(request.getParameter("oneTouchAO:units"));
		double pageLevel = Double.parseDouble(request.getParameter("oneTouchAO:pageLvl"));
		float pageOddsWin = Float.parseFloat(request.getParameter("oneTouchAO:odds_win"));
		float pageOddsLose = Float.parseFloat(request.getParameter("oneTouchAO:odds_lose"));
		int utcOffset = Integer.parseInt(request.getParameter("oneTouchAO:utcOffset"));
		setLoginErrorFlag1TAO(false);
		if (!context.getExternalContext().isUserInRole("web")) {
            // the leading "1" of this message will make sure only 1 popup will show with this message
			setLoginErrorFlag1TAO(true);
			this.setReturningOppId(id);
			this.lastInvCurrentLevel = pageLevel;
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.investment.notLogin1", null),null);
			context.addMessage(null, fm);
			submitingFlag = false;
			return null;
        }

       	try {
       		UserRegulation userRegulation = new UserRegulation();
           	long skinId = ApplicationData.getUserFromSession().getSkinId();
           	long userId = ApplicationData.getUserFromSession().getId();
       		
           	//AO Regulation
       		if (userRegulation.isUserInDepositStep()) {
       			log.debug("AO Regulation must be submit  Questionnaire user:" + userRegulation);
       			regulationLinkValue = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/jsp/pages/questionnaire.jsf";
       			regulationMessageTxt = CommonUtil.getMessage("error.investment.onetouch.questionnaire", null);
       		} else if (userRegulation.isSuspended() 
       				|| UserRegulationManager.isRestrictedAO(userRegulation, skinId)
       					|| UserRegulationManager.isPepProhibited(userRegulation, skinId)
       						|| userRegulation.isTresholdBlock()) {       			
       			if (userRegulation.isSuspendedDueDocuments()) {
       				log.debug("AO Regulationu isSuspendedDueDocuments user:" + userRegulation);
       				Skins userSkin = ApplicationData.getUserFromSession().getSkin();
           			Locale locale = new Locale(ApplicationData.getLanguage(userSkin.getDefaultLanguageId()).getCode(), "EU");
           			String supportEmail = userSkin.getSupportEmail();
           			regulationLinkValue = "mailto:" + supportEmail;
           			regulationMessageTxt = CommonUtil.getMessage("error.investment.onetouch.user.suspended.docs", null);
       			} else {
       				log.debug("AO Regulationu is Suspended or Restricted or Pep Prohibited user:" + userRegulation);
       				Skins userSkin = ApplicationData.getUserFromSession().getSkin();
					Locale locale = new Locale(ApplicationData.getLanguage(userSkin.getDefaultLanguageId()).getCode(), "EU");
					String supportEmail = userSkin.getSupportEmail();
					regulationLinkValue = "mailto:" + supportEmail;
					String supportPhone = userRegulation.getSupportPhone();
					regulationMessageTxt = CommonUtil.getMessage("error.investment.onetouch.user.suspended", new String [] {supportEmail, supportPhone}, locale);
       			}
       		}
       		
            //Check non reg suspend
       		User user = ApplicationData.getUserFromSession();
       		if(!SkinsManagerBase.getSkin(user.getSkinId()).isRegulated() && UsersManagerBase.isNonRegUserSuspend(user.getId())){   				
   				log.debug("User  is manual Suspended user:" + user.getId());
   				Skins userSkin = ApplicationData.getUserFromSession().getSkin();
				Locale locale = new Locale(ApplicationData.getLanguage(userSkin.getDefaultLanguageId()).getCode(), "EU");
				String supportEmail = userSkin.getSupportEmail();
				regulationLinkValue = "mailto:" + supportEmail;
				String supportPhone = userRegulation.getSupportPhone();
				regulationMessageTxt = CommonUtil.getMessage("error.investment.onetouch.user.suspended", new String [] {supportEmail, supportPhone}, locale);
 		}
       		
       		if (regulationMessageTxt != null) {
       			regulationError = true;
       			this.setReturningOppId(id);
           		submitingFlag = false;
           		//return null;
       		}       		
       	} catch (Exception e2) {
       		log.error("Unable to check user regulation status", e2);
       		FacesMessage fm1 = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.investment", null), null);
       		this.setReturningOppId(id);
       		context.addMessage(null, fm1);
       		submitingFlag = false;
       		return null;
       	}

		try {
			if (ApplicationData.getUserFromSession().isAcceptedTerms() == false) {
				response.sendRedirect(context.getExternalContext().getRequestContextPath() + "/jsp/terms.jsf?from=oneTouch");
				return null;
			}
		} catch (Exception e) {
			log.error("Can't get user", e);
		}

		if(lastInvUnits < 0 ){ //user didn't choose any units
			setOneTouchAOInvId(0);
			setOneTouchAOunits(0);
			lastInvUnits = 0;
			this.setReturningOppId(id);
			this.lastInvCurrentLevel = pageLevel;
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("onetouch.chooseError", null),null);
			context.addMessage(null, fm);
			submitingFlag = false;
			return null;
		}
		if (!InvestmentsManager.validateCurrentLevel(pageLevel)) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.investment", null), null);
			context.addMessage(null, fm);
			submitingFlag = false;
			return null;
		}
		result = submitOneTouchFromSlip(id,requestAmount,pageLevel,pageOddsWin,pageOddsLose,3,1,utcOffset, loginId);
		this.setReturningOppId(id);
		this.lastInvCurrentLevel = pageLevel;
		// need to check if investment passed OK
		if(result.substring(0,2).equals("OK")){//invested successfully
			setOneTouchAOInvId(Long.parseLong(result.substring(2)));
			showCancelInvestment = submitingFlag;
			submitingFlag = false;
			setOneTouchAOunits(lastInvUnits);
		}else{ // error investing
			setOneTouchAOInvId(0);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, result, null);
			context.addMessage(null, fm);
			submitingFlag = false;
		}

		}
		//if there was an error - leave the error msg
		return null;
	}


	public int getOneTouchUnitPriceAO() {

		return oneTouchUnitPriceAO;
	}

	public void setOneTouchUnitPriceAO(int oneTouchUnitPriceAO) {
		this.oneTouchUnitPriceAO = oneTouchUnitPriceAO;
	}

	public ArrayList<SelectItem> getUnitsSelect() {
		return unitsSelect;
	}

	public void setUnitsSelect() {
		SelectItem itm = new SelectItem("-1",CommonUtil.getMessage("onetouch.choose", null));
		unitsSelect = new ArrayList<SelectItem>();
		unitsSelect.add(itm);
		for(int i=1;i<=maxInv1Tunits;i++){
			itm = new SelectItem(i+"",i+"");
			unitsSelect.add(itm);
		}
	}

	public long getOneTouchAOInvId() {
		return oneTouchAOInvId;
	}

	public void setOneTouchAOInvId(long oneTouchAOInvId) {
		this.oneTouchAOInvId = oneTouchAOInvId;
	}

	public int getOneTouchAOunits() {
		return oneTouchAOunits;
	}

	public void setOneTouchAOunits(int oneTouchAOunits) {
		this.oneTouchAOunits = oneTouchAOunits;
	}

	public double getRequestAmountAO() {
		return requestAmountAO;
	}

	public void setRequestAmountAO(double requestAmountAO) {
		this.requestAmountAO = requestAmountAO;
	}

	public long getReturningOppId() {
		return returningOppId;
	}

	public void setReturningOppId(long returningOppId) {
		this.returningOppId = returningOppId;
	}

	public boolean isLoginErrorFlag1TAO() {
		return loginErrorFlag1TAO;
	}

	public void setLoginErrorFlag1TAO(boolean loginErrorFlag1TAO) {
		this.loginErrorFlag1TAO = loginErrorFlag1TAO;
    }

	public String getMaxInvAmount() throws Exception {  //for 1T general terms
        FacesContext context = FacesContext.getCurrentInstance();
        ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
        InvestmentLimit il = InvestmentsManager.getInvestmentLimit(Opportunity.TYPE_ONE_TOUCH, 0, 0, ap.getRealCurrencyId(), 0);
        long maxInvAmount = il.getMaxAmount() / 100;
        return String.valueOf(maxInvAmount);
	}

	public double getLastInvCurrentLevel() {
		return lastInvCurrentLevel;
	}

	public void setLastInvCurrentLevel(double lastInvCurrentLevel) {
		this.lastInvCurrentLevel = lastInvCurrentLevel;
	}

	public int getLastInvUnits() {
		return lastInvUnits;
	}

	public void setLastInvUnits(int lastInvUnits) {
		this.lastInvUnits = lastInvUnits;
	}

	/**
	 * @return the regulationError
	 */
	public boolean isRegulationError() {
		return regulationError;
	}

	/**
	 * @param regulationError the regulationError to set
	 */
	public void setRegulationError(boolean regulationError) {
		this.regulationError = regulationError;
	}

	/**
	 * @return the regulationMessageTxt
	 */
	public String getRegulationMessageTxt() {
		return regulationMessageTxt;
	}

	/**
	 * @param regulationMessageTxt the regulationMessageTxt to set
	 */
	public void setRegulationMessageTxt(String regulationMessageTxt) {
		this.regulationMessageTxt = regulationMessageTxt;
	}

	/**
	 * @return the regulationLinkValue
	 */
	public String getRegulationLinkValue() {
		return regulationLinkValue;
	}

	/**
	 * @param regulationLinkValue the regulationLinkValue to set
	 */
	public void setRegulationLinkValue(String regulationLinkValue) {
		this.regulationLinkValue = regulationLinkValue;
	}
	
	/**
	 * @return true if the error is because of low balance
	 */
	public boolean isLowBalanceError() {
		return lowBalanceError;
	}

	/**
	 * @param lowBalanceError the lowBalanceError to set
	 */
	public void setLowBalanceError(boolean lowBalanceError) {
		this.lowBalanceError = lowBalanceError;
	}
	
	public boolean isShowCancelInvestment() {
		return showCancelInvestment;
	}

	public void setShowCancelInvestment(boolean showCancelInvestment) {
		this.showCancelInvestment = showCancelInvestment;
	}
}