package il.co.etrader.web.helper;

import java.io.Serializable;
import java.util.ArrayList;

import com.anyoption.common.beans.Market;

public class AssetIndexGroup implements Serializable {

    private static final long serialVersionUID = 8347275750897963361L;

    private String name;
    private long id;

    private ArrayList<ArrayList<Market>> list;

    public AssetIndexGroup() {

    }

    public AssetIndexGroup(String name) {
        this.name = name;
        this.list = new ArrayList<ArrayList<Market>>();
    }

    public AssetIndexGroup(String name, ArrayList<ArrayList<Market>> list) {
        this.name = name;
        this.list = list;
    }

    /**
     * Add markets list of 9 markets
     * @param markets
     */
    public void addNewMarketsList(ArrayList<Market> markets) {
        list.add(markets);
    }

    /**
     * @return the list
     */
    public ArrayList<ArrayList<Market>> getList() {
        return list;
    }

    /**
     * @param list the list to set
     */
    public void setList(ArrayList<ArrayList<Market>> list) {
        this.list = list;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

}