package il.co.etrader.web.helper;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

/**
 * Fix problem after submit with non english letters inside the input boxes
 * @author EranL
 */
public class CharacterEncodingFilter implements Filter {

	 private static final Logger log = Logger.getLogger(CharacterEncodingFilter.class);

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
      HttpServletRequest httpRequest = (HttpServletRequest) request;
      String uri = httpRequest.getRequestURI();
      log.trace("URI: " + uri + " context: " + httpRequest.getContextPath());
      // Exit quickly if for some reason the current request is non-HTTP
      if (!(request instanceof HttpServletRequest)) {
          chain.doFilter(request, response);
          return;
      }

      //pages which contains non english letters inside the input boxes
      if (uri.endsWith("quickStart.jsf") || uri.endsWith("newFullRegPL.jsf")) {
	    	request.setCharacterEncoding("UTF-8");
      }
      chain.doFilter(request, response);
      return;
    }

	public void destroy() {
		// TODO Auto-generated method stub

	}

	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}


}