/**
 *
 */
package il.co.etrader.web.helper;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import il.co.etrader.util.CommonUtil;

/**
 * Html redirect filter
 * For redirecting html pages for new pages
 *
 * @author Kobi
 *
 */
public class HtmlRedirectFilter implements Filter {

	private static final Logger log = Logger.getLogger(HtmlRedirectFilter.class);

	public void destroy() {
		// do nothing
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		StringBuffer requestUrl = httpRequest.getRequestURL();

		// Exit quickly if for some reason the current request is non-HTTP
		if (!(request instanceof HttpServletRequest) || requestUrl.indexOf("/intl") == -1) {
		  chain.doFilter(request, response);
		  return;
		}

		if (requestUrl.indexOf("/intl") > -1) {		  
			CommonUtil.sendPermanentRedirect(httpResponse, requestUrl.substring(0, requestUrl.indexOf("/intl")));
			return;
		}
		
		/*
		ApplicationData ap = (ApplicationData)httpRequest.getSession().getServletContext().getAttribute(Constants.BIND_SESSION_APPLICATIONDATA);
		// redirect arabic & italian pages to index with s param
		if (requestUrl.indexOf("arabic.html") > -1 ||
				requestUrl.indexOf("italian.html") > -1 ||
				requestUrl.indexOf("german.html") > -1 ||
				requestUrl.indexOf("russian.html") > -1) {
			long sParam = Skin.SKINS_DEFAULT;
			String lang = Constants.LOCALE_DEFAULT;
			if (requestUrl.indexOf("arabic.html") > -1) {
				sParam = Skin.SKIN_ARABIC;
				lang = Constants.ARABIC_LOCALE;
			} else if (requestUrl.indexOf("italian.html") > -1) {
				sParam = Skin.SKIN_ITALIAN;
				lang = Constants.ITALIAN_LOCALE;
			} else if (requestUrl.indexOf("german.html") > -1) {
				sParam = Skin.SKIN_GERMAN;
				lang = Constants.GERMAN_LOCALE;
			} else if (requestUrl.indexOf("russian.html") > -1) {
				sParam = Skin.SKIN_RUSSIAN;
				lang = Constants.RUSSIAN_LOCALE;
			}
	        String prefix = lang;
	        try {
	            prefix = ApplicationData.getLinkLocalePrefix(httpRequest.getSession(), httpRequest, httpResponse);
	        } catch (Exception e) {
	            log.error("Can't get language.", e);
	        }
			CommonUtil.sendPermanentRedirect(httpResponse,
					ap.getRhomePageUrl().substring(0, ap.getRhomePageUrl().length()-1) +
						PageNames.getPageNameStatic(prefix, "/jsp/index.jsf") + "?s=" + sParam);
			return;
		} else {
			chain.doFilter(request, response);
			return;
		}*/
	}

	public void init(FilterConfig arg0) throws ServletException {
		// do nothing
	}

}
