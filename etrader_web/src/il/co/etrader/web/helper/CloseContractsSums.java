package il.co.etrader.web.helper;

import java.io.Serializable;

public class CloseContractsSums implements Serializable {

    private static final long serialVersionUID = 8347275750897963361L;

    private double totalAmount;
    private double totalAbove;
    private double totalBelow;

    public CloseContractsSums() {

    }

	public double getTotalAbove() {
		return totalAbove;
	}

	public void setTotalAbove(double totalAbove) {
		this.totalAbove = totalAbove;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalBelow() {
		return totalBelow;
	}

	public void setTotalBelow(double totalBelow) {
		this.totalBelow = totalBelow;
	}

}