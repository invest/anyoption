package il.co.etrader.web.helper;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.Locale;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Skin;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.mbeans.PageNames;
import il.co.etrader.web.util.Constants;

public class RedirectOldLinksFilter implements Filter {
    private static final Logger log = Logger.getLogger(RedirectOldLinksFilter.class);
    public void init(FilterConfig arg0) throws ServletException {
        // do nothing
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        String uri = httpRequest.getRequestURI();
        String uriToLowerCase = uri.toLowerCase();
      //file types that need to be cached should be included here and in the chain in the next if- this place is catching the redirects of the pretty that do not have pattern
        if (!uriToLowerCase.endsWith(".css") 
        		&& !uriToLowerCase.endsWith(".js")
        		&& !uriToLowerCase.endsWith(".mp3")
        		&& !uriToLowerCase.endsWith(".ogg")
        		&& !uriToLowerCase.endsWith(".gif") 
        		&& !uriToLowerCase.endsWith(".png") 
        		&& !uriToLowerCase.endsWith(".jpg") 
        		&& !uriToLowerCase.endsWith(".swf")
        		//jsf and html are processed in the phase listener
        		&& !uriToLowerCase.endsWith(".jsf") 
        		&& !uriToLowerCase.endsWith(".html")) {
        	httpResponse.addHeader("Pragma", "no-cache");
        	httpResponse.setHeader("Cache-Control", "no-store");
        }
        HttpSession session = httpRequest.getSession();
        String contextPath = httpRequest.getContextPath();
        log.trace("URI: " + uri + " context: " + httpRequest.getContextPath());
        // Exit quickly if for some reason the current request is non-HTTP
        if (!(request instanceof HttpServletRequest)) {
            chain.doFilter(request, response);
            return;
        }

        if (uri.endsWith(".css") || uri.endsWith(".jpg") ||
        		uri.endsWith(".swf") || uri.endsWith(".gif")
        		|| uri.endsWith(".mp3") || uri.endsWith(".ogg")
        		|| uri.endsWith(".png") || uri.endsWith(".html")
        		|| uri.endsWith("ajax.jsf") || uri.endsWith(".js")
        		|| uri.endsWith(".ico") || uri.endsWith(".js.jsf") || uri.endsWith(".xml")
        		|| uri.endsWith("moneybookersStatusURL.jsf") || uri.endsWith("wmResult.jsf")
        		|| uri.endsWith("baroPayStatusURL.jsf") || uri.endsWith("deltaPayStatusResponse.jsf")
        		|| uri.contains("/pages/") || uri.endsWith("registerAfterLanding.jsf") || uri.endsWith("register.jsf")
        		|| uri.endsWith("landing.jsf") || uri.endsWith("landingInternal.jsf") || uri.endsWith("resetPassword.jsf") || uri.endsWith("contactme.jsf")
        		|| uri.endsWith("right_side_banners.jsf") || uri.endsWith("right_side_banners_old.jsf")
        		|| uri.endsWith("header.jsf") || uri.endsWith("loginAuth.jsf") || uri.contains("termsBonus") || uri.endsWith("login.jsf") || uri.endsWith("tmp_login_page.jsf")
        		|| uri.contains("70_generic") || uri.contains("LP_") || uri.endsWith("quickStart.jsf") || uri.endsWith("quickStart_HP.jsf")
        		|| uri.endsWith("newFullRegPL.jsf") || uri.endsWith("mobileLanding.jsf") || uri.endsWith("mobileLandingStartApp.jsf")
        		|| uri.endsWith("passwordLP.jsf") || uri.endsWith("contactMeHPSmallAllLanguages.jsf") || uri.endsWith("registerLite.jsf")
        		|| uri.endsWith("ContactMe.jsf")|| uri.endsWith("ContactUs.jsf") || uri.contains("/jsp/widget/")
        		|| uri.endsWith("quickStart186w.jsf")
        		|| uri.endsWith("EPGStatusURL.jsf") || uri.endsWith("other_deposit_success.jsf") || uri.endsWith("other_deposit_fail.jsf")
        		|| uri.endsWith("db_check.jsf") || uri.endsWith("wlc_check.jsf") || uri.endsWith("heapSize_check.jsf")
        		|| uri.endsWith("register_from_funnel.jsf")) {
        	
            //pages which contains non english letters inside the input boxes
            if (uri.endsWith("quickStart.jsf") || uri.endsWith("newFullRegPL.jsf") 
            		|| uri.endsWith("quickStart_HP.jsf") || uri.endsWith("LP_Google_AO_RMBonus_Clean.jsf")
            		|| uri.endsWith("passwordLP.jsf") || uri.endsWith("contactMeHPSmallAllLanguages.jsf")
            		|| uri.endsWith("ContactMe.jsf")|| uri.endsWith("ContactUs.jsf")|| uri.endsWith("quickStart186w.jsf")
            		|| uri.endsWith("register_from_funnel.jsf")
            		|| uri.contains("termsBonus")) {
      	    	request.setCharacterEncoding("UTF-8");
            }
            chain.doFilter(request, response);
            return;
        }
        //if user local env we need to cut the "/anyoption/" part beacuse we do not have
        //this part in test and live
        if (!contextPath.equalsIgnoreCase("") && uri.startsWith(contextPath)) {
           uri = uri.substring(contextPath.length());
        }

        String url = httpRequest.getRequestURL().toString();
	    String prefix = "www";
	    String skin = null;
        String serverNameWithSubDomain = httpRequest.getServerName();
        log.info("Request server name: " + httpRequest.getServerName());
        //get local port number and none local port
        int port = httpRequest.getLocalPort();
	    String header = httpRequest.getHeader("port");

	    if (header != null) {
	        try {
	            port = Integer.parseInt(header.trim());
	        } catch (Exception e) {
	            log.error("Can't parse port header value.", e);
	        }
	    }

    	String user = httpRequest.getRemoteUser();

	    try {
            Locale locale = (Locale) httpRequest.getSession().getAttribute(Constants.USER_LOCALE);
            String[] urlParametersurl = url.split("//");
            String ur = urlParametersurl[1]; // url without http://

            if (null == locale && null != ur) {
            	// pattern: http://www.anyoption.com/locale/pageName
            	// and locale not in session
    	        if(uri.startsWith("/en-us/") ||
    		            uri.startsWith("/en-es/")) {
    		        	skin = uri.substring(1,6);
    	        }
    	        if (uri.startsWith("/turkey/") ||
    		            uri.startsWith("/ar/") ||
    		            uri.startsWith("/es/") ||
    		            uri.startsWith("/de/") ||
    		            uri.startsWith("/it/") ||
    		            uri.startsWith("/ru/") ||
    		            uri.startsWith("/fr/") ||
    		            uri.startsWith("/nl/") ||
    		            uri.startsWith("/se/") ||
    		            uri.startsWith("/cs/") ||
    		            uri.startsWith("/pl/") ||
    		            uri.startsWith("/pt/") ||
    		            uri.startsWith("/zh/")) {

    		        	skin = uri.substring(1,3);
    	        }

    	        if (!CommonUtil.isParameterEmptyOrNull(skin) && null == user) {
                	String redirect = "";
                	serverNameWithSubDomain = getSubDomain(skin, session, httpRequest);
                	redirect = (port == 80 ? "http://" : "https://") + serverNameWithSubDomain + uri.substring(skin.length() + 1) ;
                	log.info("Requested URL: " + url + " without locale redirect 301 to: " + redirect);
            		CommonUtil.sendPermanentRedirect(httpResponse, redirect);
    	        	return;
    	        }


    	        // pattern: http://<locale>.anyoption.com/pageName
            	if(!ur.substring(0,3).equals("www")) {
	                if (ur.substring(0,5).equals("en-us")) {
	                    skin = "en-us";
	                } else {
	                    if (ur.substring(0,5).equals("es-us")) {
	                        skin = "en-es";
	                    } else {
	                    	if(ur.equals("tr") || ur.equals("ar") || ur.equals("es") || ur.equals("de") || ur.equals("it")  
	                    			|| ur.equals("nl") || ur.equals("se") || ur.equals("cs") || ur.equals("pl")
	                    			|| ur.equals("pt") || ur.equals("turkey") || ur.equals("ru") || ur.equals("fr")
	                    			|| ur.substring(0,2).equals("zh"))
	                        skin = ur.substring(0,2);
	                    }
	                }
                }

            	if (CommonUtil.isParameterEmptyOrNull(skin)) {
            		skin = ApplicationData.getLinkLocalePrefix(httpRequest.getSession(), httpRequest, httpResponse);
            	} else {
            		httpRequest.getSession().setAttribute(Constants.USER_LOCALE, new Locale(skin));
            	}
            } else {
	        	if (null == httpRequest.getParameter(Constants.COMBINATION_ID)) {  // not from landing pages
	        		//get skin
	        		skin = ApplicationData.getLinkLocalePrefix(httpRequest.getSession(), httpRequest, httpResponse);
	        		if (!skin.equalsIgnoreCase("iw")) {
		        		if(skin.equalsIgnoreCase("en")) {
		        			prefix = "www";
		        		} else {
		        			prefix = skin;
		        		}
	        		}
	        	}
            }
        } catch (Exception e) {
            log.error("Can't get language.", e);
        }


        if (uri.endsWith("sitemap.jsf") || uri.endsWith("/sitemap_" + skin + ".jsf")) {
        	if (skin.equalsIgnoreCase("en") || uri.endsWith(skin + ".jsf")) {
        		chain.doFilter(request, response);
        		return;
        	} else {
            	String redirect = "";
            	serverNameWithSubDomain = getSubDomain(skin, session, httpRequest);
            	redirect = (port == 80 ? "http://" : "https://") + serverNameWithSubDomain + "/sitemap_" + skin + ".jsf";
            	log.info("Sitemap redirect 301 to: " + redirect);
        		CommonUtil.sendPermanentRedirect(httpResponse, redirect);
	        	return;
        	}
        }

        if (null == user) {
        	String skinFromUrl = null;
        	String newUri = null;
        	boolean redirect301 = false;
        	// pattern: http://www.anyoption.com/locale/pageName
        	// and locale in session!
	        if(uri.startsWith("/en-us/") ||
	            uri.startsWith("/es-us/")) {
	        	redirect301 = true;
	        	skinFromUrl = uri.substring(1,6);
	        	newUri = uri.substring(6);
	        }
	        if (uri.startsWith("/turkey/") ||
		            uri.startsWith("/ar/") ||
		            uri.startsWith("/es/") ||
		            uri.startsWith("/de/") ||
		            uri.startsWith("/it/") ||
		            uri.startsWith("/ru/") ||
		            uri.startsWith("/fr/") ||
		            uri.startsWith("/nl/") ||
		            uri.startsWith("/se/") ||
		            uri.startsWith("/cs/") ||
		            uri.startsWith("/pl/") ||
		            uri.startsWith("/pt/") ||
		            uri.startsWith("/zh/")) {
	        	redirect301 = true;
	        	skinFromUrl = uri.substring(1,3);
	        	newUri = uri.substring(3);
	        }
	        if (uri.startsWith("/en/")) {
	        	skinFromUrl = "www";
	        	newUri = uri.substring(3);
	        }

	        if (redirect301) {
	        	serverNameWithSubDomain = getSubDomain(skinFromUrl, session, httpRequest);
	        	String redirect = "";
	        	if (!newUri.equals("/")) {
	        		redirect = (port == 80 ? "http://" : "https://") + serverNameWithSubDomain + newUri;
	        	} else {
	        		redirect = (port == 80 ? "http://" : "https://") + serverNameWithSubDomain;
	        	}
	        	log.info("Requested URL: " + url + " with locale redirect 301 to: " + redirect);
	        	CommonUtil.sendPermanentRedirect(httpResponse, redirect);
	        	return;
	        }

	        // After changing skin from skin selector
	        if (uri.startsWith("/en/homepage")) {
	        	skinFromUrl = "www";
	        	newUri = uri.substring(12);
	        	serverNameWithSubDomain = getSubDomain(skinFromUrl, session, httpRequest);
	        	String redirect = "";
	        	if (!newUri.equals("/")) {
	        		redirect = (port == 80 ? "http://" : "https://") + serverNameWithSubDomain + newUri;
	        	} else {
	        		redirect = (port == 80 ? "http://" : "https://") + serverNameWithSubDomain;
	        	}
				httpRequest.getSession().setAttribute(Constants.USER_LOCALE, new Locale("en"));
	        	httpResponse.sendRedirect(redirect);
	        	return;
	        }


	        newUri = PageNames.getPageNameStatic(skin, uri);
        	// pattern: http://www.anyoption.com/jsp/pageName.jsf
        	// need to change his subDomain and to pretty pattern
	        if (!CommonUtil.isParameterEmptyOrNull(newUri)) {
	        	serverNameWithSubDomain = getSubDomain(skin, session, httpRequest);
	        	String redirect = "";
	        	if (!newUri.equals("/")) {
	        		redirect = (port == 80 ? "http://" : "https://") + serverNameWithSubDomain + newUri;
	        	} else {
	        		redirect = (port == 80 ? "http://" : "https://") + serverNameWithSubDomain;
	        	}
	        	log.info("Requested URL: " + url + " without locale and with pretty redirect 301 to: " + redirect);
	        	CommonUtil.sendPermanentRedirect(httpResponse, redirect);
	        	return;
	        }

        }

        String uriDecoded = URLDecoder.decode(uri, "UTF-8");
        //ru old pattern
        String newPattern = PageNames.getNewNameStatic(uriDecoded);
        if (null != newPattern) {
            String redirect = (port == 80 ? "http://" : "https://") + httpRequest.getServerName() + contextPath + newPattern;
            log.info("Redirect old-pattern: " + uri + " to: " + redirect);
            CommonUtil.sendPermanentRedirect(httpResponse, redirect);
            return;
        }

        if (uri.endsWith("/") && !PageNames.hasPattern(uriDecoded)) {
            if (!uri.equalsIgnoreCase("/jsp/pages/") && !uri.equalsIgnoreCase("/attachments/") && !uri.equalsIgnoreCase("/faces/")) { // ignores configurated servlets
                String redirect = (port == 80 ? "http://" : "https://") + httpRequest.getServerName() + contextPath;
                log.info("Removing trailing / from: " + uri + " to: " + redirect);
                CommonUtil.sendPermanentRedirect(httpResponse, redirect);
                return;
            }
           }

        if (null == PageNames.getPageNameStatic("en", uri)) {   // don't continue if it's not a pretty page.
            chain.doFilter(request, response);                  // fix encoding problem in inputText after submit forms
            return;
        }

        if (httpResponse.isCommitted()) {
           // the getLinkLocalePrefix use ApplicationData.getUserLocale which may result in redirect
            return;
        }
        String name = PageNames.getPageNameStatic(prefix, uri);
        log.trace("URI: " + uri + " prefix: " + prefix + " name: " + name + " port: " + port);
        if (null != name) {
        	// handle skin param
        	String quer = httpRequest.getQueryString();
        	String skinStr = httpRequest.getParameter(Constants.SKIN_ID);
        	if (null != skinStr) {
        		String newPrefix;
       		Long s = Long.valueOf(skinStr);
        		if (null != s) {
        			long languageId = ApplicationData.getSkinById(s).getDefaultLanguageId();
        			String lang = ApplicationData.getLanguage(languageId).getCode();
        			if (s == Skin.SKIN_EN_US || s == Skin.SKIN_ES_US) {
        				long countryId = ApplicationData.getSkinById(s).getDefaultCountryId();
        				newPrefix = lang + "-" + ApplicationData.getCountry(countryId).getA2().toLowerCase();
        		    } else {
        		    	newPrefix = lang;
        		    }
        			 String pageName = PageNames.getPageNameStatic(newPrefix, uri);
        			 if (null != pageName) {
        				 name = pageName;
        				 // remove s from QS
        				 StringTokenizer stTok = new StringTokenizer(quer, "&");
        				 String querWithoutSkin = "";
        				 while (stTok.hasMoreTokens())  {
        					 String p = stTok.nextToken();
        					 if (!p.startsWith("s=")) {
        						 querWithoutSkin += p + "&";
        					 }
        				 }
        				 if (querWithoutSkin.length() > 0) {
        					 querWithoutSkin = querWithoutSkin.substring(0, querWithoutSkin.length()-1);
        				 }
        				 quer = querWithoutSkin;
        				 httpRequest.getSession(true).setAttribute(Constants.SKIN_ID_FROM_LANDING, skinStr);
        				 log.trace("Changes due to skin param, prefix: " + prefix + " name: " + skinStr);
        			 }
        		}
        	}
        	if (null != quer && quer.length() > 1) {
        		name += "?" + quer;
        	}

            String redirect = (port == 80 ? "http://" : "https://") + serverNameWithSubDomain + contextPath + name;
            log.info("Requested URI: " + uri + " name for it: " + name + " redirect: " + redirect);
            CommonUtil.sendPermanentRedirect(httpResponse, redirect);
            return;
        } else {
            chain.doFilter(request, response);
        }
    }

	public void destroy() {
        // do nothing
    }

    public String getSubDomain(String prefix, HttpSession session, HttpServletRequest request) {
    	String[] urlParametersurl = CommonUtil.getProperty("homepage.url").split("//");
        String ur = urlParametersurl[1];
    	if (CommonUtil.isParameterEmptyOrNull(prefix)) {
    		return ur.substring(0, ur.length() - 1);
    	}
    	if (prefix.equalsIgnoreCase("www") || prefix.equalsIgnoreCase("en")) {
    		return ur.substring(0, ur.length() - 1);
    	} else {
    	    Long skinId = ApplicationData.getSkinId(session, request);
    		String subDomain = ApplicationData.getSubDomainBySkin(prefix, ApplicationData.getSkinById(skinId).isRegulated());
    		if(!CommonUtil.isParameterEmptyOrNull(subDomain)) {
    			ur = ur.replace("www", subDomain);
    			if (skinId == Skin.SKIN_ITALIAN || skinId == Skin.SKIN_REG_IT) {
    			    ur = ur.replace(Constants.HOST_ANYOPTION, Constants.HOST_ANYOPTION_IT);
    			}
    		} else {
    			ur = ur.replace("www", prefix);
    		}
    		return ur.substring(0, ur.length() - 1);
    	}
    }
}