package il.co.etrader.web.helper;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import il.co.etrader.util.CommonUtil;

public class TradingPageBoxBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TradingPageBoxBean.class);
    private long marketId;
    private int scheduled;
    private boolean fixed;
    private boolean hasOpened;
    private BigDecimal lastClosingLevel;
    private Date lastClosingLevelTime;
    private BigDecimal lastLevel;
    private Date lastLevelTime;
    private int decimal;

    public int getDecimal() {
        return decimal;
    }

    public void setDecimal(int decimal) {
        this.decimal = decimal;
    }

    public boolean isFixed() {
        return fixed;
    }

    public void setFixed(boolean fixed) {
        this.fixed = fixed;
    }

    public boolean isHasOpened() {
        return hasOpened;
    }

    public void setHasOpened(boolean hasOpened) {
        this.hasOpened = hasOpened;
    }

    public BigDecimal getLastClosingLevel() {
        return lastClosingLevel;
    }

    public void setLastClosingLevel(BigDecimal lastClosingLevel) {
        this.lastClosingLevel = lastClosingLevel;
    }

    public Date getLastClosingLevelTime() {
        return lastClosingLevelTime;
    }

    public void setLastClosingLevelTime(Date lastClosingLevelTime) {
        this.lastClosingLevelTime = lastClosingLevelTime;
    }

    public BigDecimal getLastLevel() {
        return lastLevel;
    }

	public String getLastLevelFmt() {
        if (null != lastLevel) {
            return CommonUtil.formatRealLevelByMarket(lastLevel.doubleValue(), marketId);
        }
        return CommonUtil.formatRealLevelByMarket(lastClosingLevel.doubleValue(), marketId);
    }

    public void setLastLevel(BigDecimal lastLevel) {
        this.lastLevel = lastLevel;
    }

    public Date getLastLevelTime() {
        return lastLevelTime;
    }

	public String getLastLevelTimeFmt() {
        if (null != lastLevel) {
            return formatLastLevelTime(lastLevelTime);
        }
        return formatLastLevelTime(lastClosingLevelTime);
    }

    private String formatLastLevelTime(Date time) {

        String utcOffset = CommonUtil.getUtcOffset();
        if (CommonUtil.isToday(time, utcOffset)) {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            sdf.setTimeZone(TimeZone.getTimeZone(utcOffset));
            return sdf.format(time);
        }

        SimpleDateFormat sdf = new SimpleDateFormat("(dd.MM)");
        sdf.setTimeZone(TimeZone.getTimeZone(utcOffset));
        return sdf.format(time);

    }

    public void setLastLevelTime(Date lastLevelTime) {
        this.lastLevelTime = lastLevelTime;
    }

    public long getMarketId() {
        return marketId;
    }

    public void setMarketId(long marketId) {
        this.marketId = marketId;
    }

    public String getMarketName() {
        return CommonUtil.getMarketName(marketId);
    }

    public int getScheduled() {
        return scheduled;
    }

    public void setScheduled(int scheduled) {
        this.scheduled = scheduled;
    }

    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "TradingPageBoxBean:" + ls +
            "marketId: " + marketId + ls +
            "scheduled: " + scheduled + ls +
            "fixed: " + fixed + ls +
            "hasOpened: " + hasOpened + ls +
            "lastClosingLevel: " + lastClosingLevel + ls +
            "lastClosingLevelTime: " + lastClosingLevelTime + ls +
            "lastLevel: " + lastLevel + ls +
            "lastLevelTime: " + lastLevelTime + ls +
            "decimal: " + decimal + ls;
    }
    public boolean getIsCommodityOrArabicMarket(){
    	if(marketId==137 || marketId==20 || marketId==138 || marketId==21 || marketId==248 || marketId==249 || marketId==250 )
    		return true;
    	else
    		return false;
    }
}