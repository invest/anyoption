package il.co.etrader.web.helper;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import com.anyoption.common.beans.Opportunity;

public class SlipEntry implements Serializable {
    private Opportunity opportunity;
    private int choice;
    private double amount;
    private long investmentId;
    private String oneTouchMsg;
    private long userId;
    private long optionPlusFee; // also 0100 fee
    private long binary0100OppCount;
    private double binary0100AvgAmount;

    public SlipEntry() {
        opportunity = null;

    }
    public SlipEntry(Opportunity o, int c) {
        opportunity = o;
        choice = c;
        investmentId = 0;
        optionPlusFee = 0;
    }

    /**
     * @return Returns the opportunity.
     */
    public Opportunity getOpportunity() {
        return opportunity;
    }

    /**
     * @param opportunity The opportunity to set.
     */
    public void setOpportunity(Opportunity opportunity) {
        this.opportunity = opportunity;
    }

    /**
     * @return Returns the choice.
     */
    public int getChoice() {
        return choice;
    }

    /**
     * @param choice The choice to set.
     */
    public void setChoice(int choice) {
        this.choice = choice;
    }

    public String getChoiceText() {
        return choice == 1 ? "call" : "put";
    }

    public String getLevelClass() {
        if (opportunity.getClosingLevel() < opportunity.getCurrentLevel()) {
            return "invest_level_up_left";
        }
        if (opportunity.getClosingLevel() == opportunity.getCurrentLevel()) {
            return "invest_level_close_left";
        }
        return "invest_level_down_left";
    }

    /**
     * @return Returns the amount.
     */
    public double getAmount() {
        return amount;
    }

    public String getAmountTxt() {
    	DecimalFormat sd = new DecimalFormat("###,###,##0.00");
    	return sd.format(amount);
    }

    public String getAmountWinTxt() {
//    	DecimalFormat sd = new DecimalFormat("###,###,##0.00");
    	BigDecimal am = new BigDecimal(Double.toString(amount));
    	BigDecimal oW = new BigDecimal(Float.toString(opportunity.getOddsType().getOverOddsWin()));

        return am.multiply(new BigDecimal("1").add(oW)).setScale(2, RoundingMode.HALF_UP).toString();
    	//return doubleToTxt(amount * (1 + opportunity.getOddsType().getOverOddsWin())); //sd.format(amount * (1 + opportunity.getOddsType().getOverOddsWin()));
    }

    public String getAmountLoseTxt() {
    	BigDecimal am = new BigDecimal(Double.toString(amount));
    	BigDecimal oL = new BigDecimal(Float.toString(opportunity.getOddsType().getOverOddsLose()));

        return am.multiply(new BigDecimal("1").subtract(oL)).setScale(2, RoundingMode.HALF_UP).toString();
    }

    public String getAmountInput() {
      //  DecimalFormat numberFormat = new DecimalFormat("#0.##");
        //return investmentId > 0 ? numberFormat.format(amount) : "<input type=\"text\" onBlur=\"javascript:twoDecimals(this);\" onkeyup=\"updateWinLose(this, event); return false;\" onFocus=\"javascript:this.select();\" size=\"10\" maxlength=\"9\" class=\"input_text\" value=\"" + numberFormat.format(amount) + "\" />";
    	return String.valueOf(amount);
    }

    /**
     * @param amount The amount to set.
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the investmentId
     */
    public long getInvestmentId() {
        return investmentId;
    }

    /**
     * @param investmentId the investmentId to set
     */
    public void setInvestmentId(long investmentId) {
        this.investmentId = investmentId;
    }

    public String getDisplaySuccess() {
        return investmentId > 0 ? "block" : "none";
    }

    public String getDisplayNoSuccess() {
        return investmentId > 0 ? "none" : "block";
    }
    
    public String getOneTouchMsg() {
        return oneTouchMsg;
    }
    
    public void setOneTouchMsg(String s) {
        this.oneTouchMsg = s;
    }

    public long getUserId() {
        return userId;
    }
    
    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "SlipeEntry:" + ls +
            "choice: " + choice + ls +
            "amount: " + amount + ls +
            "investmentId: " + investmentId + ls +
            "userId: " + userId + ls +
            "option plus fee: " + optionPlusFee + ls +
            (null != opportunity ? opportunity.toString() : "opportunity: null") + ls;
    }

    /**
     * @return the optionPlusFee
     */
    public long getOptionPlusFee() {
        return optionPlusFee;
    }
    /**
     * @param optionPlusFee the optionPlusFee to set
     */
    public void setOptionPlusFee(long optionPlusFee) {
        this.optionPlusFee = optionPlusFee;
    }
	public long getBinary0100OppCount() {
		return binary0100OppCount;
	}
	public void setBinary0100OppCount(long binary0100OppCount) {
		this.binary0100OppCount = binary0100OppCount;
	}
	public double getBinary0100AvgAmount() {
		return binary0100AvgAmount;
	}
	public void setBinary0100AvgAmount(double binary0100AvgAmount) {
		this.binary0100AvgAmount = binary0100AvgAmount;
	}
}