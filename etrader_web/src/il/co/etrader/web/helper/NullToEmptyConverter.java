package il.co.etrader.web.helper;

import javax.faces.component.EditableValueHolder;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import il.co.etrader.util.ConstantsBase;

/**
 * Request parameters have the unwanted behaviour that they are still been sent even if they does
 * not contain any value. In the Java side they will be translated as an empty String. Besides, JSF
 * has the unwanted behaviour that it sets a non-required String property with the empty String
 * value anyway instead of setting it with null.
 * <p>
 * This converter will convert any null to empty String so that - in case of an empty String. Define the converter in faces-config.xml as follows:
 * <pre>
 * <converter>
 *     <converter-id>NullToEmptyConverter</converter-id>
 *     <converter-class>mymodel.EmptyToNullConverter</converter-class>
 * </converter>
 * NOTE: this converter doesn't work in JSF 1.1, but in JSF 1.2 or newer only, because JSF 1.1
 * by design ignores any converter which is associated with java.lang.String.
 *
 *
 */
public class NullToEmptyConverter implements Converter {

    /**
     * Returns empty String value instead of null and if component is an instance of EditableValueHolder,
     * then also set its submitted value to null. This affects under each UIInput.
     * @see javax.faces.convert.Converter#getAsObject(
     *      javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.String)
     */
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
        if (value == null || value.trim().length() == 0) {
            if (component instanceof EditableValueHolder) {
                ((EditableValueHolder) component).setSubmittedValue(null);
            }
            return ConstantsBase.EMPTY_STRING;
        }
        return value;
    }

    /**
     * Does nothing special.
     * @see javax.faces.convert.Converter#getAsString(
     *      javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
     */
    public String getAsString(FacesContext facesContext, UIComponent component, Object value) {
        return value == null ? null : value.toString();
    }

}
