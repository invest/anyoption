package il.co.etrader.web.helper;

import java.util.HashMap;
import java.util.Locale;

import javax.faces.context.FacesContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.EmailValidator;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Skin;
import com.google.gson.Gson;

import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.util.Constants;
import il.co.etrader.web.util.Utils;

public class LandingDirectServlet extends HttpServlet {

	private static final long serialVersionUID = 2546988534821504778L;
	
	private static final String FUNNEL_PASSWORD_CONFIRM	= "funnel_passwordConfirm";
	private static final String FUNNEL_PASSWORD			= "funnel_password";
	private static final String FUNNEL_MOBILE_PHONE		= "funnel_mobilePhone";
	private static final String FUNNEL_EMAIL			= "funnel_email";
	private static final String FUNNEL_LAST_NAME		= "funnel_lastName";
	private static final String FUNNEL_FIRST_NAME		= "funnel_firstName";
	private static final String FUNNEL_PHONE_PREFIX		= "funnel_phonePrefix"; 
	private static final String FUNNEL_COUNTRY_ID		= "funnel_countryId";
	
	private static final Logger log = Logger.getLogger(LandingDirectServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        handleRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        handleRequest(request, response);
    }

    private void handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
		if (request.getRemoteUser() != null) {
			request.logout();
			request.getSession().invalidate();
			request.getSession(); // to create new session
		}
        request.setCharacterEncoding("UTF-8");
        String firstName		= (String) request.getParameter(FUNNEL_FIRST_NAME);
        String lastName			= (String) request.getParameter(FUNNEL_LAST_NAME);
        String email			= (String) request.getParameter(FUNNEL_EMAIL);
        String mobilePhone		= (String) request.getParameter(FUNNEL_MOBILE_PHONE);
        String password			= (String) request.getParameter(FUNNEL_PASSWORD);
		String passwordConfirm	= (String) request.getParameter(FUNNEL_PASSWORD_CONFIRM);
		String phonePrefix		= (String) request.getParameter(FUNNEL_PHONE_PREFIX);
		String countryId		= (String) request.getParameter(FUNNEL_COUNTRY_ID);

		
        long skinId = Skin.SKIN_REG_EN;
        try {
        	skinId = Long.parseLong(request.getParameter(Constants.SKIN_ID));
        } catch (Exception e) {
            log.info("Can't parse s param. Use default skin 2.");
        }
        Skins skin = ApplicationData.getSkinById(skinId);
        request.getSession().setAttribute(Constants.SKIN_ID_FROM_LANDING, ""+skinId);

        String errorJson = validateRegistration(firstName, lastName, email, mobilePhone, password, passwordConfirm, phonePrefix, skin);
        String valuesJson = valuesToJson(firstName, lastName, email, mobilePhone, countryId, phonePrefix);
        
        HttpSession session = request.getSession();
        session.setAttribute("errorJson", errorJson);
        session.setAttribute("valuesJson", valuesJson);
        
        
        
      //handle affsubs and dynamic parameter
        String dynamicParam = (String) session.getAttribute(Constants.DYNAMIC_PARAM);
 		String affSub1 = (String) session.getAttribute(Constants.AFF_SUB1);
		String affSub2 = (String) session.getAttribute(Constants.AFF_SUB2);
		String affSub3 = (String) session.getAttribute(Constants.AFF_SUB3);
		String dpn = (String) session.getAttribute(Constants.DPN);
		
		// handle dynamic parameter
   		if ( null == dynamicParam ) {  // not in session
			dynamicParam = (String) request.getParameter(Constants.DYNAMIC_PARAM);
			if ( null != dynamicParam ) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.DYNAMIC_PARAM, dynamicParam);
			}
		}
		
		if ( null == affSub1 ) {  // not in session
   			affSub1 = (String) request.getParameter(Constants.AFF_SUB1);
			if ( null != affSub1 ) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.AFF_SUB1, affSub1);
			}
		}
   		if ( null == affSub2 ) {  // not in session
   			affSub2 = (String) request.getParameter(Constants.AFF_SUB2);
			if ( null != affSub2 ) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.AFF_SUB2, affSub2);
			}
		}
   		if ( null == affSub3 ) {  // not in session
   			affSub3 = (String) request.getParameter(Constants.GCLID);
			if ( null != affSub3 ) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.AFF_SUB3, Constants.GCLID + "_" + affSub3);
			}
		}
   		if (null == dpn) {  // not in session
   			dpn = (String) request.getParameter(Constants.DPN);
			if (null != dpn) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.DPN, dpn);
			}
		}
   		
		if(errorJson.length()==0) {
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/jsp/pages/firstNewCardRedirect.jsf?register_from_short_reg=1");
			rd.forward(request, response);
		} else {		
			String path = Utils.getPropertyByHost(request, "homepage.url.https") + "jsp/register.jsf";
			String encodedUrl = response.encodeRedirectURL(path);
			response.sendRedirect(encodedUrl);
		}
    }

	public static String valuesToJson(String firstName, String lastName, String email, String landMobilePhone, String countryId, String phonePrefix) {
		HashMap<String, String> values = new  HashMap<String, String>();
		values.put(FUNNEL_FIRST_NAME, firstName);
		values.put(FUNNEL_LAST_NAME, lastName);
		values.put(FUNNEL_EMAIL, email);
		values.put(FUNNEL_MOBILE_PHONE, landMobilePhone);
		values.put(FUNNEL_PHONE_PREFIX, phonePrefix);
		values.put(FUNNEL_COUNTRY_ID, countryId);
		Gson gson = new Gson();
		String jsonValues = gson.toJson(values);
		return jsonValues;
	}   
   
	public String validateRegistration(String firstName, String lastName, String email, String landMobilePhone, String password, String passwordConfirm, String phonePrefix, Skin skin) {
		HashMap<String, String> errors = new  HashMap<String, String>();
		String langCode = ApplicationData.getLanguage(skin.getDefaultLanguageId()).getCode();
		Locale locale = new Locale(langCode);
		String patternLetters = CommonUtil.getMessage("general.validate.letters.only", null, locale);
		String patternLettersAndNumbers = CommonUtil.getMessage("general.validate.letters.and.numbers", null, locale);

		if (CommonUtil.isParameterEmptyOrNull(firstName.trim())) {
			errors.put(FUNNEL_FIRST_NAME, CommonUtil.getMessage("error.banner.contactme.required", null, locale));
		} else if (!firstName.matches(patternLetters)){
			errors.put(FUNNEL_FIRST_NAME, CommonUtil.getMessage("register.letters.only", null, locale));
		}
		if (CommonUtil.isParameterEmptyOrNull(lastName.trim())) {
			errors.put(FUNNEL_LAST_NAME, CommonUtil.getMessage("error.banner.contactme.required", null, locale));
		} else if (!lastName.matches(patternLetters)){
			errors.put(FUNNEL_LAST_NAME, CommonUtil.getMessage("register.letters.only", null, locale));
		}
		 if (CommonUtil.isParameterEmptyOrNull(email.trim())) {
			 errors.put(FUNNEL_EMAIL, CommonUtil.getMessage("error.banner.contactme.required", null, locale));
         } else if (!EmailValidator.getInstance().isValid(email) || !CommonUtil.isEmailValidUnix(email)) {
        	 errors.put(FUNNEL_EMAIL, CommonUtil.getMessage("org.apache.myfaces.Email.INVALID", null, locale));
         }
		 if (CommonUtil.isParameterEmptyOrNull(landMobilePhone.trim())) {
			 errors.put(FUNNEL_MOBILE_PHONE, CommonUtil.getMessage("error.banner.contactme.required", null, locale));
         } else if(!landMobilePhone.matches("\\d+")){
        	 errors.put(FUNNEL_MOBILE_PHONE, CommonUtil.getMessage("error.banner.contactme.phone", null, locale));
         } else if (landMobilePhone.length() < 7 || landMobilePhone.length() > 20){
        	 errors.put(FUNNEL_MOBILE_PHONE, CommonUtil.getMessage("error.quick.start.mobile.phone", null, locale));
         }
		 if(CommonUtil.isParameterEmptyOrNull(password) || password.length()>9 || password.length() <6) {
			 errors.put(FUNNEL_PASSWORD, CommonUtil.getMessage("register.password.message", null, locale));
		 } else if (!password.matches(patternLettersAndNumbers)){
			 errors.put(FUNNEL_PASSWORD, CommonUtil.getMessage("register.password.message", null, locale));
		 } else if(!password.equals(passwordConfirm)) {
			 errors.put(FUNNEL_PASSWORD, CommonUtil.getMessage("error.register.password.match", null, locale));
		 } 
		 if(skin.getId() == 1) {
			 if(CommonUtil.isParameterEmptyOrNull(phonePrefix.trim())) {
				 errors.put(FUNNEL_PHONE_PREFIX, CommonUtil.getMessage("error.register.prefix", null, locale));
			 }
		 }
		if(errors.size() >0 ) {
			Gson gson = new Gson();
			String errorsJsonMap = gson.toJson(errors);
			return errorsJsonMap;
		} else {
			return "";
		}
	}
}
