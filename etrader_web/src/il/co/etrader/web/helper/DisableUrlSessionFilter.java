/**
 *
 */
package il.co.etrader.web.helper;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.log4j.Logger;

import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;

/**
 * Disable url session filter
 * For removing jsessionid param from the url's
 * The solution is to create a servlet filter which will intercept calls to
 * HttpServletRequest.encodeURL() and skip the generation of session.
 *
 * @author Kobi
 *
 */
public class DisableUrlSessionFilter implements Filter {

	private static final Logger logger = Logger.getLogger(DisableUrlSessionFilter.class);

	public void destroy() {
		// do nothing
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// Exit quickly if for some reason the current request is non-HTTP
		if (!(request instanceof HttpServletRequest)) {
		  chain.doFilter(request, response);
		  return;
		}

		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		// filter validations
		if (httpRequest.getRequestURL().indexOf("ajax.jsf") > -1 ||
				httpRequest.getRequestURL().indexOf("/pages/") > -1 ||
				httpRequest.getRequestURL().indexOf("header.jsf") > -1 ||
				httpRequest.getRequestURL().indexOf("header_loggedin.jsf") > -1 ||
				httpRequest.getRequestURL().indexOf("homepage_banners.jsf") > -1 ||
				httpRequest.getRequestURL().indexOf("right_side_trading.jsf") > -1 ||
				httpRequest.getRequestURL().indexOf("right_side_investments_inner.jsf") > -1 ||
				httpRequest.getRequestURL().indexOf("ticker.jsf") > -1 ||
				httpRequest.getRequestURL().indexOf("contactme.jsf") > -1 ||
				httpRequest.getRequestURL().indexOf("contactme_success.jsf") > -1 ||
				httpRequest.getRequestURL().indexOf("rssFeed.jsf") > -1 ||
				httpRequest.getRequestURL().indexOf("right_side_investments.jsf") > -1 ||
				httpRequest.getRequestURL().indexOf("investments_profit_box.jsf") > -1 ||
				httpRequest.getRequestURL().indexOf("right_side_banners.jsf") > -1 ||
				httpRequest.getRequestURL().indexOf("right_side_banners_old.jsf") > -1 ||
				httpRequest.getRequestURL().indexOf("errorPage.jsf") > -1 ) {
			chain.doFilter(request, response);
			return;
		}

		User user = (User)httpRequest.getSession().getAttribute(Constants.BIND_SESSION_USER);
		if ( null != user && user.getId() != 0) {   // user in session
			chain.doFilter(request, response);
			return;
		}

		// invalidate any sessions that are backed by a URL-encoded session
		//TODO: think if we need / want this situation!
		/*if (httpRequest.isRequestedSessionIdFromURL()) {
		  HttpSession session = httpRequest.getSession();
		  if (session != null) {
			  session.invalidate();
		  }
		}*/

		// To disable the default URL-encoding functionality, we need to wrap the existing HttpServletResponse
		HttpServletResponseWrapper wrappedResponse
	    	= new HttpServletResponseWrapper(httpResponse) {
				  public String encodeRedirectUrl(String url) { return url; }
				  public String encodeRedirectURL(String url) { return url; }
				  public String encodeUrl(String url) { return url; }
				  public String encodeURL(String url) { return url; }
				};

		chain.doFilter(request, wrappedResponse);
	}

	public void init(FilterConfig arg0) throws ServletException {
		// do nothing
	}

}
