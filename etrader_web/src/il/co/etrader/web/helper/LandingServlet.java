package il.co.etrader.web.helper;

import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Skin;

import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.util.Constants;

@SuppressWarnings("serial")
public class LandingServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(LandingServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        handleRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        handleRequest(request, response);
    }

    private void handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        request.setCharacterEncoding("UTF-8");
        String url = request.getRequestURL().toString();
        String query = request.getQueryString();
        log.info("Request: " + url + "?" + query);

        long s = Skin.SKIN_REG_EN;
        try {
            s = Long.parseLong(request.getParameter(Constants.SKIN_ID));
        } catch (Exception e) {
            log.info("Can't parse s param. Use default skin 2.");
        }
        Skins skin = ApplicationData.getSkinById(s);
        if (url.indexOf("www1") > -1) {
            url = url.replace("www1", skin.getSubdomain());
        } else {
            url = url.replace("www", skin.getSubdomain()); // for hk.168qiquan.com it will do nothing, for etrader will replace "www" with "www"
        }
        if ((!CommonUtil.isParameterEmptyOrNull(request.getParameter("pageR")) && request.getParameter("pageR").equals("deposit")) || 
        		(!CommonUtil.isParameterEmptyOrNull(request.getParameter("pageRedirect")) && request.getParameter("pageRedirect").equals("deposit")))
        {
        	request.getSession().setAttribute("redirectToDeposit", Constants.NAV_FIRST_DEPOSIT);
        }
        if (url.indexOf("landing.jsf") > -1) {
            url = url.replace("landing.jsf", "landingInternal.jsf");
        } else { // assume the only other possible case is paramAfterLanding.html
            url = url.replace("paramAfterLanding.html", "paramAfterLandingInternal.html");

            boolean isNoForm = request.getParameter("isNoForm") != null;
            if (!isNoForm) {
                //get parameters from landing page in order to pass it to register page
                String firstName = request.getParameter("first_name");
                String lastName = request.getParameter("last_name");
                String email = request.getParameter("email");
                String mobilePhone = request.getParameter("mobile_m");
                String mobilePhoneCode = request.getParameter("mobile_c");
                boolean reciveUpdates = request.getParameter("agree") != null;          

                String params =
                "firstName="+ URLEncoder.encode(firstName, "UTF-8") +
                "&last_Name=" + URLEncoder.encode(lastName, "UTF-8") +
                "&email=" + email +
                "&mobilePhoneCode=" + mobilePhoneCode +
                "&mobilePhone=" + mobilePhone +
                "&reciveUpdates=" + reciveUpdates +
                "&regAtt=true" +
                "&marketingTracking=true";
                if (null != query) {
                    query = params + "&" + query;
                } else {
                    query = params;
                }
            }
        }
        if (null != query) {
            url = url + "?" + query;
        }
        log.info("Redirect: " + url);
        response.sendRedirect(url);
    }
}