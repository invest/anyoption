package il.co.etrader.web.helper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunityType;
import com.anyoption.common.beans.Tournament;
import com.anyoption.common.beans.TournamentUsersRank;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.QuestionnaireGroup;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.bl_vos.OptionPlusQuote;
import com.anyoption.common.clearing.DeltaPayInfo;
import com.anyoption.common.clearing.EzeebillClearingProvider;
import com.anyoption.common.clearing.InatecIframeInfo;
import com.anyoption.common.clearing.Powerpay21ClearingProvider;
import com.anyoption.common.encryption.HMAC;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.anyoption.common.jms.WebLevelLookupBean;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.managers.IssuesManagerBase;
import com.anyoption.common.managers.QuestionnaireManagerBase;
import com.anyoption.common.managers.RewardUserTasksManager;
import com.anyoption.common.managers.RewardUserTasksManager.TaskGroupType;
import com.anyoption.common.managers.TournamentManagerBase;
import com.anyoption.common.managers.TournamentUsersRankManagerBase;
import com.anyoption.common.managers.TransactionsManagerBase.TransactionSource;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.util.GoldenMinute.GoldenMinuteType;
import com.anyoption.common.util.MarketingTracker;
import com.anyoption.common.util.OpportunityCache;
import com.anyoption.common.util.OpportunityCacheBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ocpsoft.pretty.PrettyContext;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.TiersManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.CachePages;
import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.InvestmentLimit;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.ContactsManager;
import il.co.etrader.web.bl_managers.DeviceUniqueIdsSkinManager;
import il.co.etrader.web.bl_managers.InvestmentsManager;
import il.co.etrader.web.bl_managers.TransactionsManager;
import il.co.etrader.web.bl_managers.UsersManager;
import il.co.etrader.web.bl_vos.LandingMarketDetails;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.bl_vos.UserRegulation;
import il.co.etrader.web.mbeans.ContactUsForm;
import il.co.etrader.web.mbeans.DepositForm;
import il.co.etrader.web.mbeans.LoginForm;
import il.co.etrader.web.mbeans.PageNames;
import il.co.etrader.web.mbeans.RegisterForm;
import il.co.etrader.web.mbeans.SlipForm;
import il.co.etrader.web.mbeans.WithdrawForm;
import il.co.etrader.web.service.AnyoptionServiceServlet;
import il.co.etrader.web.util.Constants;
import il.co.etrader.web.util.Utils;


public class ETraderPhaseListener implements PhaseListener {
    private static final long serialVersionUID = 1L;

    private static final Logger log = Logger.getLogger(ETraderPhaseListener.class);
    private static final String sessionToken = "MULTI_PAGE_MESSAGES_SUPPORT";
    private static AnyoptionServiceServlet anyoptionServlet;

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.ANY_PHASE;
	}

	@Override
	public void beforePhase(PhaseEvent event) {
        if (log.isTraceEnabled()) {
            log.trace("BeforePhase: " + event.getPhaseId());
        }
		
		boolean forceHttps = ApplicationData.isForceHttps();
		
		String useHttps = "false";
		// test comment
		useHttps = CommonUtil.getProperty("use.https");

		FacesContext context = event.getFacesContext();
		this.saveMessages(context);

		if (event.getPhaseId() == PhaseId.RENDER_RESPONSE) {
			if (!context.getResponseComplete()) {
				this.restoreMessages(context);
			}
		}

		if (event.getPhaseId() == PhaseId.RESTORE_VIEW) {
			HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			String requestURL = request.getRequestURL().toString();

			if (requestURL.matches(".*/AnyoptionService/.*")) {
				if (anyoptionServlet == null) {
					anyoptionServlet = new AnyoptionServiceServlet();
				}
				anyoptionServlet.outerDoPost(request, response);
				context.responseComplete();
				// TODO if the request doesn't complete normally, there is possibility to make an endless cycle
				return;
			}

			if (requestURL.indexOf("com.ocpsoft.dynaView") != -1) {
		         log.warn("Detected dynaview.");
		         return;
			}

			//Set max age for cache pages
			String requestURI = request.getRequestURI();
            boolean foundCachePages = false;
            String maxAge = "max-age=0";
            for (CachePages cp :  ApplicationData.getCachePages()) {
            	if (requestURI.indexOf(cp.getPageName()) > -1) {
            		maxAge = "max-age=" + cp.getCacheTime();
            		foundCachePages = true;
            	}
            }
            if (foundCachePages){
            	response.setHeader("Cache-Control", maxAge);
            } else {
        		response.addHeader("Pragma", "no-cache");
        		response.setHeader("Cache-Control", "no-store");
            }

            //set vary header
            for (String minisitePageName : ApplicationData.getMinisitePages()) {
                if (requestURI.indexOf(minisitePageName) > -1) {
                    response.setHeader("Vary", "User-Agent");
                    break;
                }
            }

			String quer = request.getQueryString();
			if(quer!=null ){

				if( quer.indexOf(Constants.FROM_MOVIE) > -1) {
					request.getSession().setAttribute(Constants.FIRST_TIME_VISIT, "true");
					request.getSession().setAttribute(Constants.CLEAR_FIRST_TIME_VISIT, "false");
				}

			}

			User user = (User)request.getSession().getAttribute(Constants.BIND_SESSION_USER);

			// recognize client locale for AO
			String requestUrl = request.getRequestURL().toString();
			String fromLive = (String) request.getSession().getAttribute(Constants.FROM_LIVE);

			if(fromLive != null && fromLive.equals("true")) {
				if(requestUrl.indexOf("/jsp/trade_binary_options.jsf") >- 1) {
					try {
						request.getSession().setAttribute(Constants.FROM_LIVE, Constants.FROM_LIVE_FALSE);
						String redirect = requestUrl.replaceAll("/jsp/trade_binary_options.jsf", "/jsp/live.jsf");
						response.sendRedirect(redirect);
						context.responseComplete();
						return;
					} catch (IOException e) {
						log.debug("Error redirect from US live page.");
					}
				}
			}

			if(requestUrl.indexOf("/jsp/live.jsf") > -1 ) {
				request.getSession().setAttribute(Constants.FROM_LIVE, Constants.FROM_LIVE_PENDING);
			} else if(requestUrl.indexOf("/header_loggedin.jsf")>-1
					|| requestUrl.indexOf("ajax.jsf")>-1
					|| requestUrl.indexOf("apppage.jsf")>-1
					|| requestUrl.indexOf("header.jsf")>-1) {
				//ignore
			}
			else {
				request.getSession().setAttribute(Constants.FROM_LIVE, Constants.FROM_LIVE_FALSE);
			}

			if(requestUrl.toLowerCase().indexOf("es-us") > -1 || requestUrl.toLowerCase().indexOf("en-us") > -1 ){
				if(requestUrl.indexOf("/jsp/live.jsf")>-1) {
					try {
						String redirect = requestUrl.replaceAll("/jsp/live.jsf", "/jsp/trade_binary_options.jsf");
						response.sendRedirect(redirect);
						context.responseComplete();
						return;
					} catch (IOException e) {
						log.debug("Error redirect from US live page.");
					}

				}
			}
			
			//ET Regulation redirects
				if(null != user && user.getId() > 0 && (requestUrl.indexOf("/jsp/pages/newCard.jsf") > -1 || requestUrl.indexOf("/jsp/pages/deposit.jsf") > -1 || requestUrl.indexOf("/jsp/terms.jsf") > -1)) {					
					
					HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
					boolean isETRegulation = (Boolean) session.getAttribute(Constants.SESSION_IS_ET_REGULATION);
					boolean isHasDeposit = (Boolean) session.getAttribute(Constants.SESSION_IS_HAS_DEPOSIT);
					
					if(UserRegulationManager.isNeedAdditionalInfo(user) && 
							((!isHasDeposit && !isETRegulation) || isETRegulation)){												
						String redirect = "";
						String additionalInfoPageUrl = "/jsp/pages/register_additional.jsf";
						
						if(requestUrl.indexOf("/jsp/pages/newCard.jsf") > -1){
							redirect = requestUrl.replaceAll("/jsp/pages/newCard.jsf", additionalInfoPageUrl);
						} else if(requestUrl.indexOf("/jsp/pages/deposit.jsf") > -1){
							redirect = requestUrl.replaceAll("/jsp/pages/deposit.jsf", additionalInfoPageUrl);
						} else {
							redirect = requestUrl.replaceAll("/jsp/terms.jsf", additionalInfoPageUrl);
						}
						
						try {
							log.debug("Redirect for Additional Info page");
							response.sendRedirect(redirect);							
							context.responseComplete();
							return;
						} catch (IOException e) {
							log.debug("Error redirect from newCard/terms page.");
						}
					}
				}

			if (requestUrl.indexOf("miopciones") > -1) {
				try {
					response.setStatus(301);
					response.sendRedirect(requestUrl.toString().replaceAll("miopciones", "anyoption"));
					return;
				} catch (IOException ioe) {
					log.warn("Error with redirect from miopciones.com to anyoption.com, ", ioe);
				}
			}

			//Track user parameters that download anyoption app.
			if (requestUrl.indexOf("mobileLanding.jsf") > -1) {
				ApplicationData app = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
				try {
					app.saveMobileLandingCookie();
					response.sendRedirect(Constants.ANYOPTION_ITUNES_IPHONE_APP);
                    context.responseComplete();
                    return;
				} catch (Exception e) {
					log.error("ERROR!! Can't save anyoption cookie mobile " + e);
				}
			}

			//Open anyoption Iphone application on user Iphone with the tracking parameters.
			if (requestUrl.indexOf("mobileLandingStartApp.jsf") > -1) {
				ApplicationData app = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
				StringBuffer cookieQueryString = new StringBuffer();
				try {
					cookieQueryString = app.getCookieQueryString();
					cookieQueryString.insert(0, Constants.START_ANYOPTION_IPHONE_APP);
					response.sendRedirect(cookieQueryString.toString());
                    context.responseComplete();
                    return;
				} catch (Exception e) {
					log.error("ERROR!! Can't send cookie query string to iphone app" + e);
				}
			}

			// Only if it's landing page, save http refere in session.
			// When we gets to landingInternal.jsf we redirecting to landingSaveClick. so in this point we want to save HTTP_REFERE
			if (requestUrl.indexOf("landingInternal.jsf") > -1) {
				String httpReferer = (String) request.getSession().getAttribute(Constants.HTTP_REFERE_LANDING);
				if (httpReferer == null) {
					httpReferer = request.getHeader(Constants.HTTP_REFERE);
			 		try {
			    		if (!CommonUtil.isParameterEmptyOrNull(httpReferer)) {
			    			// save in session
			    			request.getSession().setAttribute(Constants.HTTP_REFERE_LANDING, URLEncoder.encode(httpReferer, "UTF-8"));
			    			ApplicationData.addCookie(Constants.HTTP_REFERE_COOKIE, httpReferer, response, Constants.HTTP_REFERE_LANDING);

			    		}
					} catch (UnsupportedEncodingException e) {
						log.error("Error! problem to encode http referer: " + httpReferer);
					}
				}
			}

			// add Marketing Tracking click
			if ((requestUrl.indexOf("registerAfterLanding.jsf") < 0 && requestUrl.indexOf("landingInternal.jsf") > -1)) {
				try {
					//set marketing tracker cookie for affiliate
					MarketingTracker.checkMarketingTrackerCookie(request, response, false);
				} catch (Exception e) {
					log.info("Problem When add Marketing Tracker Click " + e);
				}
			}

			//	when we came from static LP we want to save contact in the session and update his IP and country
			if (requestUrl.indexOf("registerAfterLanding.jsf") > -1) {
				Long staticLPContactId = (Long)request.getSession().getAttribute(Constants.STATIC_LANDING_PAGE_CONTACT_ID);
				if (null != staticLPContactId) {
					Contact contact = null;
					try {
						contact = ContactsManager.getContactByID(staticLPContactId.longValue());
					} catch (SQLException e1) {
						log.error("Error! problem getting contactid " + e1);
						context.responseComplete();
						return;
					}
					String ip = CommonUtil.getIPAddress();
					contact.setIp(ip);
					//for anyoption get country
					if (requestUrl.indexOf(Constants.HOST_ANYOPTION) > -1 || requestUrl.indexOf(Constants.HOST_ANYOPTION_IT) > -1) {
						String countryCode = CommonUtil.getCountryCodeByIp(ip);
						long countryId = 0;
						if (!CommonUtil.isParameterEmptyOrNull(countryCode)){
							// get Country id from Db
							try {
								countryId = CountryManagerBase.getIdByCode(countryCode);
							} catch (SQLException e) {
								log.warn("Error! problem getting countryId by code : " + e);
								countryId = 0;
							}
						}
						if (countryId == 0) {
							String skinId = (String)request.getSession().getAttribute(Constants.SKIN_ID_FROM_LANDING);
							if (skinId == null) { //take default skin
								skinId = String.valueOf(Skin.SKIN_REG_EN);
							}
							countryId = ApplicationData.getSkinById(Long.valueOf(skinId)).getDefaultCountryId();
						}
						request.getSession().setAttribute(Constants.COUNTRY_ID, String.valueOf(countryId));
						ApplicationData.addCookie(Constants.COUNTRY_ID, String.valueOf(countryId), response, Constants.COUNTRY_ID_DESCRIPTION);
						contact.setCountryId(countryId);
					}
					try {
						ContactsManager.updateContact(contact);
					} catch (Exception e) {
						log.warn("Error! Can't update contact from static LP. " + e);
					}
					//save contact in session
			        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.REGISTER_ATTEMPT, Contact.class);
			        ve.getValue(context.getELContext());
			        ve.setValue(context.getELContext(), contact);
				}
			}
			if(null == user && !(requestUrl.indexOf("regForm_map.jsf") > -1 || requestUrl.indexOf("register_content_funnel.jsf") > -1)) {
//	            if (requestUrl.indexOf("index.jsf") > -1) {
	            	 //set marketing tracker cookie for free users
	 				MarketingTracker.checkMarketingTrackerCookie(request, response, true);
	 				//MarketingETS.etsMarketingOrganic(request, response);
//	            }
			}

			if (requestUrl.indexOf(Constants.HOST_ANYOPTION) > -1
					|| requestUrl.indexOf(Constants.HOST_MIOPCIONES) > -1
					|| requestUrl.indexOf(Constants.HOST_TLV_TRADE) > -1
					|| requestUrl.indexOf(Constants.HOST_VIP168) > -1
					|| requestUrl.indexOf(Constants.HOST_ANYOPTION_IT) > -1
					|| requestUrl.indexOf(Constants.HOST_ANYOPTION_TR) > -1) {
				if(null == user ) {
		            if (requestUrl.indexOf("index.jsf") > -1) {
		                String skin = "en";
		                String subDomainFromSkin = "www";
		                String urlDomain = Constants.EMPTY_STRING;
		                try {
//		                    Locale locale = (Locale) request.getSession().getAttribute(Constants.USER_LOCALE);
//	                        String localeForSubDomain = getSkinBySubdomain(request);
	                        urlDomain = request.getServerName();

//		                    if (null == locale && null != localeForSubDomain && !localeForSubDomain.isEmpty() && !localeForSubDomain.equals("www")) { //locale not in session
		                        // we want to save if it's a regulation url. later on we want to save the new skin by locale ( the problem we are dealing that we have same locale but different skin)
//		                        	String ip = CommonUtil.getIPAddress(request);
//		                        	String countryCode = CommonUtil.getCountryCodeByIp(ip);
//		            				//countryCode = "GB";
//		            				long countryId = CountryManagerBase.getIdByCode(countryCode);
//		            				boolean isRegCountry = ApplicationData.getRegulatedCountriesList().contains(countryId);
//		                        	boolean isRegulatedSkin = false;
//			                        if (isRegCountry) {
//			                        	isRegulatedSkin = true;
//			                        }
//			                        String localeForSubDomain = ApplicationData.getLocaleBySubDomain(subDomainFromUrl, false);
//		                        	request.getSession().setAttribute(Constants.USER_LOCALE, new Locale(localeForSubDomain));
		                        	//request.getSession().setAttribute(Constants.IS_REGULATED, false);
//		                        } else {
		                        skin = ApplicationData.getLinkLocalePrefix(request.getSession(), request, response);
//		                    }
		                } catch (SQLException e) {
		                    e.printStackTrace();
		                }

		                String domain = CommonUtil.getProperty("cookie.domain", ".anyoption.com").substring(1); // pattern: anyoption.com
		                // we will 301 redirect from anyoption.com to www.anyoption.com and then 302 redirect if needed to subdomain
		                if (urlDomain.startsWith(domain) && !CommonUtil.isParameterEmptyOrNull(urlDomain)) { //Check if user hit pattern: http://anyoption.com/
	                		request.getSession().setAttribute(Constants.USER_LOCALE, null);
		                	String redirect = CommonUtil.getProperty("homepage.url");
                        	log.info("Requested URL: " + requestUrl.toString() + "   redirect 301 to: " + redirect);
							CommonUtil.sendPermanentRedirect(response, redirect);
	                        return;
	                    }

		                if (requestUrl.toString().contains("www.") && !skin.equals("en") && skin.compareTo("en-us") != 0) { // pattern: http://www.anyoption.com/ with skin different form English
		                	subDomainFromSkin = ApplicationData.getSubDomainBySkin(skin, CommonUtil.isUserSkinRegulated());
		                	String redirect = CommonUtil.getProperty("homepage.url").replace("www", subDomainFromSkin);
		                	if (skin.equals("it")) { // FIXME: Tony: rewrite the damn skin selection logic
		                	    redirect = redirect.replace(Constants.HOST_ANYOPTION, Constants.HOST_ANYOPTION_IT);
		                	}
							try {
								log.info("Requested URL: " + requestUrl.toString() + "   redirect 302 to: " + redirect);
								response.sendRedirect(redirect);
		                        return;
							} catch (IOException e) {
								e.printStackTrace();
							}
		                }
	                }
	            }


				// handle Adobe cross domain security config
				if (requestUrl.indexOf("crossdomain.xml") > -1) {
					response.setContentType("text/x-cross-domain-policy");
					response.setCharacterEncoding("UTF-8");
					try {
						response.getWriter().write(
								"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
								"<!DOCTYPE cross-domain-policy SYSTEM \"http://www.adobe.com/xml/dtds/cross-domain-policy.dtd\">" +
								"<cross-domain-policy>" +
									"<site-control permitted-cross-domain-policies=\"master-only\"/>" +
									"<allow-access-from domain=\"" + Utils.getPropertyByHost("images.sub.domain") + "\"/>" +
									"<allow-http-request-headers-from domain=\"" + Utils.getPropertyByHost("images.sub.domain") + "\" headers=\"*\"/>" +
								"</cross-domain-policy>");
					} catch (IOException ioe) {
						log.error("Can't write crossdomain.xml", ioe);
					}
					context.responseComplete();
					return;
				}

				// Redirect unregistered customers to register page
				/*if (requestUrl.indexOf("index.jsf") > -1) {
					String redirectedToReg = (String) request.getSession().getAttribute(Constants.REDIRECTED_TO_REGISTER);
					if (null == redirectedToReg) {  // user not redirected yet
						try {
							String host = new URL(request.getRequestURL().toString()).getHost();
							if (null != host && !host.startsWith(Constants.SUBDOMAIN_MOBILE_ANYOPTION + ".")) {
								Date timeFirstVisit = UsersManager.getTimeFirstVisit(request, false);
								if (null != timeFirstVisit) {
									Calendar now = Calendar.getInstance();
									Calendar timeFVC = Calendar.getInstance();
									timeFVC.setTime(timeFirstVisit);
									timeFVC.add(Calendar.DAY_OF_MONTH, 1);
									if (now.after(timeFVC)) {
										if (null == UsersManager.getCustomerVisitType(request)) {
											request.getSession().setAttribute(Constants.REDIRECTED_TO_REGISTER, "true");
											response.sendRedirect(context.getExternalContext().getRequestContextPath() + "/jsp/register.jsf");
											context.responseComplete();
											return;
										}
									}
								}
							}
						} catch (Exception e) {
							log.warn("Problem redirecting unregistered customer ", e);
						}
					}
				}*/
				String url = requestUrl.toString();
				String[] urlParametersurl = url.split("//");
				String ur = urlParametersurl[1];
				String skin = Skin.LOCALE_SUBDOMAIN_ENGLISH;
				String skinL = Skin.LOCALE_SUBDOMAIN_ENGLISH;
				long skinNum = Skin.SKIN_ENGLISH;
				String skinBySubdomain = getSkinBySubdomain(request);
				if (skinBySubdomain != null && !skinBySubdomain.isEmpty()) {
                    skin = skinBySubdomain;
                }

		    	String hiddenUsername = context.getExternalContext().getRequestParameterMap().get(LoginForm.HIDDEN_USERNAME_KEY);
				if (!CommonUtil.isParameterEmptyOrNull(skin)
						&& CommonUtil.isParameterEmptyOrNull(context.getExternalContext().getRemoteUser())
						&& (requestUrl.indexOf("login.jsf") < 0 || hiddenUsername == null) // not a login request
						&& (skin.compareTo(Skin.LOCALE_SUBDOMAIN_EN_US) == 0
								|| skin.compareTo(Skin.LOCALE_SUBDOMAIN_ES_US) == 0)) {
					if (skin.compareTo(Skin.LOCALE_SUBDOMAIN_EN_US) == 0) {
						skinL = Skin.LOCALE_SUBDOMAIN_ENGLISH;
						skinNum = Skin.SKIN_ENGLISH;
						if (ur.contains(Skin.LOCALE_SUBDOMAIN_EN_US)) {
							ur = ur.replace(Skin.LOCALE_SUBDOMAIN_EN_US, "www");
						}
					} else if (skin.compareTo(Skin.LOCALE_SUBDOMAIN_ES_US) == 0) {
						skinL = Skin.LOCALE_SUBDOMAIN_SPAIN;
						skinNum = Skin.SKIN_SPAIN;
						if (ur.contains(Skin.LOCALE_SUBDOMAIN_ES_US)) {
							ur = ur.replace(Skin.LOCALE_SUBDOMAIN_ES_US, "www");
						}
					}
					request.getSession().setAttribute(Constants.USER_LOCALE, new Locale(skinL));
					//request.getSession().setAttribute(Constants.SKIN_ID, String.valueOf(skinNum));
					log.info("Requested URL: " + requestUrl.toString() + "  redirect 301 to: " + urlParametersurl[0] + "//" + ur);
					CommonUtil.sendPermanentRedirect(response, urlParametersurl[0] + "//" + ur);
					context.responseComplete();
					return;
				}

				if (requestUrl.indexOf(Constants.HOST_TLV_TRADE) == -1) {  // not in use for TLV TRADE
						//	redirect our affiliates page to affiliates site
						if (requestUrl.indexOf("affiliates.jsf") > -1) {
							CommonUtil.sendPermanentRedirect(response, "http://affiliates.anyoption.com");
							context.responseComplete();
							return;
						}

						if ((requestUrl.indexOf("binary0100.jsf") > -1 || requestUrl.indexOf("bitcoin-options.jsf") > -1) && !CommonUtil.isUserSkinRegulated()) {
							skinNum = Skin.SKIN_ENGLISH;
							skinL = "";
							ApplicationData app = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);

							url = requestUrl.toString();
							skin = Skin.LOCALE_SUBDOMAIN_ENGLISH;
							skinBySubdomain = getSkinBySubdomain(request);

	                        if (skinBySubdomain != null && !skinBySubdomain.isEmpty()) {
		                        skin = skinBySubdomain;
	                        }

							if (skin.equals("ar") || skin.equals("en-us") || skin.equals("es-us") || skin.equals("it")) {
								if (skin.equals("ar")) {
									skinNum = 4;
									skinL = "ar";
								} else if (skin.equals("en-us")) {
									skinNum = 13;
									skinL = "en-us";
								} else if (skin.equals("es-us")) {
									skinNum = 14;
									skinL = "es-us";
								} else if (skin.equals("it")) {
									skinNum = 9;
									skinL = "it";
								}
								request.getSession().setAttribute(Constants.USER_LOCALE, new Locale(skinL));
								CommonUtil.sendPermanentRedirect(response, context.getExternalContext().getRequestContextPath() + "/jsp/trade_binary_options.jsf?s=" + skinNum);
								context.responseComplete();
								return;
							}
						}

					// redirect virtual tour or personlize page to demo page
					if (requestUrl.indexOf("virtual_tour.jsf") > -1 || requestUrl.indexOf("personalize.jsf") > -1) {
						CommonUtil.sendPermanentRedirect(response, context.getExternalContext().getRequestContextPath() + "/jsp/trading_box_manual.jsf");
						context.responseComplete();
						return;
					}

					// redirect profit line page to newFeatures page
					if (requestUrl.indexOf("profitline.jsf") > -1) {
						CommonUtil.sendPermanentRedirect(response, context.getExternalContext().getRequestContextPath() + "/jsp/newFeatures.jsf");
						context.responseComplete();
						return;
					}

					if(requestUrl.indexOf("download-mobile-trading.jsf") > -1 || requestUrl.indexOf("appContactUs.jsf") > -1
							|| requestUrl.indexOf("appBinary.jsf") > -1 || requestUrl.indexOf("appAboutUs.jsf") > -1){
						CommonUtil.sendPermanentRedirect(response, context.getExternalContext().getRequestContextPath() + "/minisite/index.jsf");
						context.responseComplete();
						return;
					}

					// Mobile use
					if ((requestUrl.indexOf("jsp/errorPage.jsf") > -1 && requestUrl.indexOf(Constants.MINISITE_DIRECTORY + "/jsp/errorPage.jsf") == -1) ||
							requestUrl.indexOf("jsp/index_error.jsf") > -1) {
						ApplicationData app = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
						if (app.isAPIGUI()) {
							CommonUtil.sendPermanentRedirect(response, context.getExternalContext().getRequestContextPath() + "/API/errorPage.jsf");
						}
						if (CommonUtil.isMobile()) {
							String page = "errorPage.jsf";
							if (requestUrl.indexOf("index_error.jsf") > -1) {
								page = "index_error.jsf";
							}
							CommonUtil.sendPermanentRedirect(response, context.getExternalContext().getRequestContextPath() + "/" + Constants.MINISITE_DIRECTORY + "/jsp/" + page);
						}
					}
				}

				//Because of the subdomain we have filter that replacing the URL. So we need to save anyotpion click (url parameters) on that tier
				if (requestUrl.indexOf("landingInternal.jsf") > -1) {
					ApplicationData app = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
					try {
						app.getSaveAoClickIndex();
					} catch (Exception e) {
						log.error("ERROR!! Cant save AO click " + e);
					}
				}

                 //redirect users from iran or israel or netrefer to error page
				if (requestUrl.indexOf("register.jsf") > -1 || requestUrl.indexOf("registerAfterLanding.jsf") > -1) {
                    String fromIsrael = null;
                    ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
                    Long countryId = 0L;
                    Long skinId = ap.getSkinId();
                    if(ap.getRealCountryId() != null){
                        countryId =  Long.valueOf(ap.getRealCountryId());
                        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
                        fromIsrael = (String)session.getAttribute(Constants.FROM_REDIRECT);
                        String ipAddress = CommonUtil.getIPAddress();                          
                        if (CountryManagerBase.isCountryForbidenAndIp(ipAddress, skinId, null, countryId)) {
                            String root = Constants.WEB_DIRECTORY;
                            if (CommonUtil.isMobile()) {
                                root = Constants.MOBILE_DIRECTORY;
                            }
                            String forbiddenRegisterUrl = ap.getHomePageUrl() + root;
                        	if (countryId == Constants.COUNTRY_ID_US) {
                        		forbiddenRegisterUrl += Constants.ERROR_PAGE_FORBIDDEN_REGISTER + "?forbidUSA=true";
                        	} else if (ap.getSkinId() == Skin.SKIN_ARABIC) {
                        		forbiddenRegisterUrl += Constants.ERROR_PAGE_FORBIDDEN_REGISTER + "?forbidAR=true";
                        	} else {
                        		forbiddenRegisterUrl += Constants.ERROR_PAGE_FORBIDDEN_REGISTER + Constants.ERROR_PAGE_FORBIDDEN_REGISTER_PARAM_ISRAEL_IRAN;
                        	}
                            try {
                            	log.debug("ipAddress=" + ipAddress + " Redirecting to :" + forbiddenRegisterUrl);
                                response.sendRedirect(forbiddenRegisterUrl);
                                context.responseComplete();
                                return;
	                        } catch (IOException ioe) {
	                        	log.warn("Error with redirect from register to errorPageForbiddenRegister, ", ioe);
	                        }
                        }

                    }
//                  redirect users from Netrefer to error page
                    Cookie[] cookiesArr = request.getCookies();
                    String combIdCookieValue = "";

                    if (cookiesArr != null) {
                        for (int i = 0; i < cookiesArr.length; i++) {
                            if (cookiesArr[i].getName().equals(Constants.COMBINATION_ID)) {
                                log.log(Level.DEBUG, "found combid cookie! combid:" + cookiesArr[i].getValue());
                                combIdCookieValue = cookiesArr[i].getValue();
                            }
                        }
                    }

                    if (!CommonUtil.isParameterEmptyOrNull(combIdCookieValue)){
                        if (ApplicationDataBase.isNetreferCombination(Long.valueOf(combIdCookieValue))) { // check if user in from Netrefer(certain countries)
                            if (countryId > 0) {
                            	Country country = ApplicationDataBase.getCountry(countryId);
                                if (country.isNetreferRegBlock() && !ApplicationData.isAllowAffilate(request)) {
                                    String root = Constants.WEB_DIRECTORY;
                                    if (CommonUtil.isMobile()) {
                                        root = Constants.MOBILE_DIRECTORY;
                                    }
                                    log.debug("Redirecting to :" + ap.getHomePageUrl() + root + Constants.ERROR_PAGE_FORBIDDEN_REGISTER);
                                    try {
                                        response.sendRedirect(ap.getHomePageUrl() + root + Constants.ERROR_PAGE_FORBIDDEN_REGISTER + Constants.ERROR_PAGE_FORBIDDEN_REGISTER_PARAM_NETREFER);
                                        context.responseComplete();
                                        return;
                                    } catch (IOException ioe) {
                                        log.warn("Error with redirect from register to errorPageForbiddenRegister, ", ioe);
                                    }
                                }
                            }
                        }
                    }
                }

				if (requestedPages(requestUrl)) {

						ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
						try {
							if (null == user || (null != user && user.getId() == 0)) {   // user not in session
								if (requestUrl.indexOf(Constants.HOST_TLV_TRADE) == -1) {
								    if (requestUrl.indexOf(Constants.HOST_VIP168) == -1) {
    									// Check if it's Pretty request for saving the correct skin by pretty URL
    									PrettyContext contextP = PrettyContext.getCurrentInstance();
    									if (contextP.isPrettyRequest() && null == request.getParameter(Constants.SKIN_ID)) {
    										skin = "en";

    				                        boolean isRegulated = false;
    				                        boolean isRegulatedSession = false;
    				                        String isRegulatedCookie = "";

    				                		Cookie[] cs = request.getCookies();
    				                		if (cs != null) {
    				                			for (int i = 0; i < cs.length; i++) {
    				                				if (cs[i].getName().equals(Constants.IS_REGULATED)) {
    				                					isRegulatedCookie = cs[i].getValue();
    				                				}
    				                			}
    				                		}

    				                		if(!CommonUtil.isParameterEmptyOrNull(isRegulatedCookie)) {
    				                			isRegulatedSession = Boolean.parseBoolean(isRegulatedCookie);
    				                		}

    				                        if ((String)request.getSession().getAttribute(Constants.IS_REGULATED) != null) {
    				                        	isRegulatedSession = Boolean.valueOf((String)request.getSession().getAttribute(Constants.IS_REGULATED));
    				                        }

				                        	String skinBySubDomain = getSkinBySubdomain(request);
					                        if (null != skinBySubDomain) {
					                        	skin = skinBySubDomain;
					                        }
    				                        if (isRegulatedSession && ApplicationData.getRegulatedSkinsList().contains(skin)) {
    				                        	isRegulated = true;
    				                        }
    										Long skinIdFromURL = ApplicationData.getSkinByLanguageCode(skin, isRegulated);
    										if (null != skinIdFromURL) {
    					    					request.setAttribute(Constants.PRETTY_SKIN_ID_ATR, skinIdFromURL);
    										}
    									}
								    }
									ap.getSaveAnyOptionCookie();
								} else {
									ap.getSaveTLVCookie();
								}
							}
						} catch (Exception e) {
							log.warn("Error with saving user recognition values, ", e);
						}

						String mobileAppPage = ap.gethomePageMobileUrl();
						String minisitePage = ap.getHomePageMinisiteUrl();
						mobileAppPage = mobileAppPage.substring(0,mobileAppPage.length() - 9); //remove index.jsf


						// save that we came from mobile app, saving duid (device unique id)
						if (requestUrl.indexOf("iphoneapp.jsf") > -1 &&	CommonUtil.isMobile()) {
							/*if (ap.isAndroid()) {
								//we dont want anymore webview in android redirect to force update page
						    	try {
									response.sendRedirect(mobileAppPage + "android/update-mobile-trading.jsf");
									return;
								} catch (IOException e) {
									log.info("Problem redirect to force update for android users", e);
								}
							}*/
							ap.handleMobileAppRequest(request, response, quer, context, user);
							return;
						}

						// Mobile redirect
						String host = null;
						boolean isMobileApp = false;
						try {
							String device = ConstantsBase.EMPTY_STRING;
							host = new URL(request.getRequestURL().toString()).getHost();


                            if (ap.isAndroid()){
                            	device = "android/";
                            } else if (ap.isIphone()) {
                            	device = "iphone/";
                            }

                            mobileAppPage += device + "download-mobile-trading.jsf";

							if (host.indexOf(Constants.HOST_ANYOPTION) > -1) {
								/*isMobileApp = CommonUtil.isMobileApp();
								if (host.startsWith(Constants.SUBDOMAIN_MOBILE_ANYOPTION + ".") &&
											(!isMobileApp)) {  // redirect to web
										response.sendRedirect(mobileAppPage);
										return;
								}*/
	                            boolean isContinue = Boolean.parseBoolean(request.getParameter("isContinue"));
	                            boolean fromApp = Boolean.parseBoolean(request.getParameter("fromApp"));
	                            if (isContinue) { // if the user pressed on the button 'continue to website' in minisite page.
	                            	request.getSession().setAttribute("isContinue", "true");
	                            	response.sendRedirect(ap.gethomePageWebUrl());
	                            	context.responseComplete();
	                            	return;
	                            }
	                            if (fromApp) {
	                            	request.getSession().setAttribute("isContinueFromApp", "true");
	                            }
	                            String continueToMiniSite = (String)request.getSession().getAttribute("isContinue");
	                            String continueToMinisiteFromApp = (String)request.getSession().getAttribute("isContinueFromApp");

	                            if (CommonUtil.isParameterEmptyOrNull(continueToMiniSite) && CommonUtil.isParameterEmptyOrNull(continueToMinisiteFromApp)) {
	                            	if(!ap.isIphoneApp()){
										// redirect old appPage from mobile to 'download-mobile-trading' page
		            					if (host.startsWith(Constants.SUBDOMAIN_MOBILE_ANYOPTION + ".") &&
		            							requestUrl.indexOf("mobile/appPage.jsf") > -1 &&
		            							CommonUtil.isMobileUserAgent()) {
		            						CommonUtil.sendPermanentRedirect(response, minisitePage);
		            						context.responseComplete();
		            						return;
		            					}
	                            	}

//										if (!host.startsWith(Constants.SUBDOMAIN_MOBILE_ANYOPTION + ".") && CommonUtil.isMobileUserAgent()) {
//											boolean fromMobileLP = false;
//											if (quer != null && quer.indexOf(Constants.FROM_MOBILE_LP) > -1) {
//												fromMobileLP = true;
//											}
//											if (!fromMobileLP) {
//												if (ap.isIphone() || ap.isAndroid()) {
//													url = "";
//													if (ap.getIsLive()) {
//														url = "http://cdn.anyoption.com/landing.shtml?pageR=Mobile_LP&s=" + ap.getSkinId();
//													} else {
//														url = "http://cdn.testenv.anyoption.com/landing.shtml?pageR=Mobile_LP&s=" + ap.getSkinId();
//													}
//													response.sendRedirect(url);
//												} else {
//													response.sendRedirect(minisitePage);
//												}
//
//												context.responseComplete();
//												return;
//
//
//											}
//										}

	                            	if(!ap.isIphoneApp()){
			                            if(host.startsWith(Constants.SUBDOMAIN_MOBILE_ANYOPTION + ".") &&
			                            		CommonUtil.isMobileUserAgent() &&
			                            		requestUrl.indexOf("minisite") == -1 ){
			                            	response.sendRedirect(minisitePage);
			                            	context.responseComplete();
			                            	return;
			                            }
	                            	}
	                            }
							}
							/*dev-1845 comment by Natalia German
							else if(requestUrl.indexOf(Constants.HOST_VIP168) > -1){
								if(CommonUtil.isMobileUserAgent()){
									url = "";
									url = "/mobile/index.jsf";
	                            	response.sendRedirect(url);
								}
							}
							*/
						} catch (Exception eHost) {
							log.info("Problem checking Mobile redirect:");
							log.info("URL: " + request.getRequestURL().toString() + " host: " + host + " isMobileApp: " + isMobileApp);
							log.info("Problem checking Mobile redirect", eHost);
						}
				}
			} else if (requestUrl.indexOf(Constants.HOST_ETRADER) > -1) {  //redirect for old etrader pages to etrder home page
				ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);

				//Because of the subdomain we have filter that replacing the URL. So we need to save anyotpion click (url parameters) on that tier
				if (requestUrl.indexOf("landingInternal.jsf") > -1) {
					try {
						ap.getSaveClick();
					} catch (Exception e) {
						log.error("ERROR!! Cant save ET click " + e);
					}
				}

				if(requestedPages(requestUrl) && (null == user || (null != user && user.getId() == 0))){
					try {
						ap.getSaveCookie();
					} catch (Exception e) {
						log.error("ERROR!! Cant save ET cookie " + e);
					}
				}

        		if(ap.getSkinId() == Skins.SKIN_ETRADER && CommonUtil.isMobileUserAgent()) {
        			boolean fromApp = Boolean.parseBoolean(request.getParameter("fromApp"));
                    if (fromApp) {
                    	request.getSession().setAttribute("isContinueFromApp", "true");
                    }
                    boolean isRedirectET = false;
                    String userAgent = CommonUtil.getUserAgent();
                    if (null != userAgent && userAgent.matches(".*(android|ip(hone|od|ad)).*") && requestUrl.indexOf("etActivationPage.jsf") == -1) {
                    	isRedirectET = true;
                    }
        			String continueToMinisiteFromApp = (String)request.getSession().getAttribute("isContinueFromApp");
	                if ((CommonUtil.isParameterEmptyOrNull(continueToMinisiteFromApp)) && isRedirectET) {
	        			String url = "";
	        			if(ap.getIsLive()) {
	        				url = "http://cdn.etrader.co.il/landing.shtml?pageR=Mobile_LP&s=1";
	        			} else {
	        				url = "http://cdn.testenv.etrader.co.il/landing.shtml?pageR=Mobile_LP&s=1";
	        			}
	        			try {
							response.sendRedirect(url);
						} catch (IOException e) {
							log.info("Problem with redirect etrader to mobile LP");
						}
	        			context.responseComplete();
	                    return;
	        		} else if(ap.isIphone()){
						//redirect to ET mobile (not exist)
					} else {
						//redirect to minisite (not exist in IW)
						// String minisitePage = ap.getHomePageMinisiteUrl();
						// response.sendRedirect(minisitePage);
					}
        		}

                if (requestUrl.indexOf("etraderIndices.jsf") > -1 ||
                        requestUrl.indexOf("calcalist_game_agreement.jsf") > -1 ||
                        requestUrl.indexOf("charts.jsf") > -1 ||
                        requestUrl.indexOf("tour2.jsf") > -1 ||
                        requestUrl.indexOf("tour4.jsf") > -1 ||
                        requestUrl.indexOf("tour3.jsf") > -1 ||
                        requestUrl.indexOf("move_to_etrader.jsf") > -1 ||
                        requestUrl.indexOf("landingCalcalistGame.jsf") > -1) {
                    CommonUtil.sendPermanentRedirect(response, context.getExternalContext().getRequestContextPath() + "/jsp/index.jsf");
                    context.responseComplete();
                    return;
                }
            }

			// Check if you are in calcalist domain
			ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
			if (null != ap && ap.getSkinId() == Skin.SKIN_ETRADER && requestUrl.indexOf(ap.getCalcalistGameDomain()) > -1) {
				request.getSession().setAttribute(Constants.CALCALIST_GAME, "true");
			}

			if((requestUrl.indexOf("newCard.jsf") > -1 || requestUrl.indexOf("trade_binary_options.jsf") > -1)
					&& ap.getSkinId() != Skin.SKIN_ETRADER && !CommonUtil.isMobile()) {

				String url = requestUrl.toString();
				String[] urlParametersurl = url.split("//");
				String skin = "en";
				String skinFromUser = "";
				try {
					skinFromUser = ApplicationData.getLinkLocalePrefix(request.getSession(), request, response);
				} catch (SQLException e) {
					log.error("Can't get skin", e);
				}


            	String subDomainFromUrl = request.getServerName().substring(0, request.getServerName().indexOf('.'));
            	String urlWithoutSubDomain = request.getServerName().substring(request.getServerName().indexOf('.'));
            	boolean isRegulatedSkin = CommonUtil.isUserSkinRegulated();
                skin = ApplicationData.getLocaleBySubDomain(subDomainFromUrl, isRegulatedSkin);

                if(!CommonUtil.isParameterEmptyOrNull(skinFromUser) && !CommonUtil.isParameterEmptyOrNull(skin) && !skin.equals(skinFromUser)) {
                	try {
                    	if(skinFromUser.equals("en")) {
                    		log.info("need to redirect to suitable domain after login - from all skins to en: from " + skin + " to " + skinFromUser);
                    		String newUrl = urlParametersurl[0] + "//www" + urlWithoutSubDomain;
                    		response.sendRedirect(newUrl);
							return;
                    	}

                		String domain = ApplicationData.getSubDomainBySkin(skin, isRegulatedSkin);
                		String domainFromUser = ApplicationData.getSubDomainBySkin(skinFromUser, isRegulatedSkin);
                		if (!CommonUtil.isParameterEmptyOrNull(domain) && !CommonUtil.isParameterEmptyOrNull(domainFromUser)) {
            				log.info("need to redirect to suitable domain after login - from skin to skin: from " + skin + " to " + skinFromUser);
                    		String newUrl = requestUrl.toString().replace("//" + domain + ".", "//" + domainFromUser + "."); //replace only the subdomain prefix, not all the appearances of "domain" variable in requestUrl.
							response.sendRedirect(newUrl);
							return;
                		}
					} catch (IOException e) {
						log.error("Can't redirect after login", e);
					}
                }
			}
			//User user = (User)request.getSession().getAttribute(Constants.BIND_SESSION_USER);

			// Prevent logged in user from going to some pages
			if (null != user && user.getId() > 0) {   // user not in session
				if (requestUrl.indexOf("register.jsf") > -1 ||
					requestUrl.indexOf("/password.jsf") > -1 ||
					requestUrl.indexOf("/resetPassword.jsf") > -1
					|| requestUrl.indexOf("/login.jsf") > -1
					) {

				    try {

                        String redirectPage = "/jsp/index.jsf";
                        String logRedMsg = "home page";

                        if(user.getSkinId() != Skin.SKIN_CHINESE && requestUrl.indexOf("register.jsf") > -1 ){
                            String prefix = "";
                            try {
                                prefix = ApplicationData.getLinkLocalePrefix(request.getSession(), request, response);
                            } catch (Exception e) {
                                log.error("Can't get language.", e);
                            }
                            redirectPage = PageNames.getPageNameStatic(prefix,"/jsp/trade_binary_options.jsf");
                            logRedMsg = "BO page:" + redirectPage;
                        }

                        log.debug("Prevent logged in user from going to " + requestUrl + " - url redirect to " + logRedMsg);
                        response.sendRedirect(context.getExternalContext().getRequestContextPath() + redirectPage);
                        context.responseComplete();
                        return;
                    } catch (IOException e) {
                        log.debug("Prevent logged in user from going to " + requestUrl + " - url direction failde.");
                    }
				}
			}

//			// check that the user is in the right domain.
//			if (null != user){
//				log.debug("check! user" + user.getUserName());
//				log.debug("check! url" + requestUrl);
//
//				boolean needToRedirect = false;
//				if (user.getUserName().indexOf(ConstantsBase.CAL_GAME_USER_NAME_SUFFIX) > -1){ // calcalist user
//
//					if (null != ap && requestUrl.indexOf(ap.getCalcalistGameDomain()) == -1) { // not calcalist domain
//						needToRedirect = true;
//					}
//				} else { // real user
//
//					if (null != ap && requestUrl.indexOf(ap.getCalcalistGameDomain()) > -1) { // not real domain
//						needToRedirect = true;
//					}
//				}
//
//				if(needToRedirect){
//					log.debug("user doesn't match to his domain - redirect to relevant home page");
//					try {
//						response.sendRedirect(ap.getHomePageUrl());
//					} catch (IOException e) {
//						log.debug("couldn't redirect to relevant home page");
//					}
//					context.responseComplete();
//					return;
//				}
//			}

			// If trying to get into one of the pages check whether the user has accept the term and for AO users authorized email.
			if (requestUrl.indexOf("/pages") > -1) {
				if ((null != user  && user.getId() > 0 )&& !user.isAcceptedTerms() && !UserRegulationManager.isNeedAdditionalInfo(user)) {
					StringBuffer url = request.getRequestURL();
					log.debug("Terms Check - url before replace " + url.toString());
					try {
						log.debug("Terms Check - url redirect to terms.jsf page");
						//request.getSession().setAttribute("redirectAfterTerms", "pages");
						String path = "/jsp/terms.jsf?from=pages";
						if (ap.isAPIGUI()) {
							path = "/API/terms.jsf?from=API";
						}
						response.sendRedirect(context.getExternalContext().getRequestContextPath() + path);
						context.responseComplete();
						return;
					} catch (IOException e) {
						log.debug("Terms Check - url direction failde.");
					}
				}
				//  check that we r in ao site and user didnt authorized his email and not mobile and not hebrew skin
                /*if (requestUrl.indexOf(Constants.HOST_ANYOPTION) > -1 && null != user && !user.isAuthorizedMail() &&
                        !CommonUtil.isMobile() && !CommonUtil.isHebrewUserSkin()) {
                    StringBuffer url = request.getRequestURL();
                    log.debug("Email authorize check - url before replace " + url.toString());
                    try {
                        if (!user.isSentActivationEmail()) {
                            EmailAuthenticationManagerBase.sendActivationEmail(ap.getHomePageUrlHttps() + "jsp/loginAuth.jsf?", CommonUtil.getWebWriterId(), user);
                            user.setSentActivationEmail(true);
                        }
                        if (!user.isEmailRemindMeLater()) {
                        	if (user.getAuthorizedMailRefusedTimes() < Constants.ACTIVATION_EMAIL_DEPO_MAX_REFUSED_TIMES) {
	                            log.debug("Email authorize check - url redirect to emailActivation.jsf page");
	                            response.sendRedirect(context.getExternalContext().getRequestContextPath() + "/jsp/emailActivation.jsf");
	                            context.responseComplete();
	                            return;
                        	}
                        }
                    } catch (Exception e) {
                        log.debug("Email authorize check - url direction failde.");
                    }
                }*/
			}
			if (CommonUtil.isMobile()) {
				if (requestUrl.indexOf("/mobile/depositMenu.jsf") > -1
						|| requestUrl.indexOf("/mobile/myaccountMenu.jsf") > -1) {
					if (!user.isAcceptedTerms()) {
						try {
							log.debug("Terms Check - url redirect to terms.jsf page");
							//request.getSession().setAttribute("redirectAfterTerms", "pages");
							response.sendRedirect(context.getExternalContext().getRequestContextPath() + "/mobile/terms.jsf");
							context.responseComplete();
							return;
						} catch (IOException e) {
							log.debug("Terms Check - url direction failde.");
						}
						context.responseComplete();
					}
				}
			}
			
			if (requestUrl.indexOf("/trading-bubbles") > -1 && !CommonUtil.getProperty("isHasBubbles.always.flag").equalsIgnoreCase("true") && !(null != user && user.getId() > 0 && user.getProductViewFlags().get(ConstantsBase.HAS_BUBBLES_FLAG))) {
				try {
					response.sendRedirect(context.getExternalContext().getRequestContextPath() + "/jsp/trade_binary_options.jsf");
					context.responseComplete();
					return;
				} catch (IOException e) {
					log.debug("Bubbles url direction failed.");
				}
			}

			if(!forceHttps){
				if (useHttps.equals("true")) {


					if (quer == null) {
						quer = "";
					}
					String view = request.getRequestURL() + "?" + quer;
					// log.debug(view);
					int desiredPort = 0;

					// To use current page port	(without any change)
					if (view.indexOf("header_loggedin.jsf")>-1 ||
						view.indexOf("header.jsf")>-1 ||
						view.indexOf("homepage_banners.jsf") > -1 ||
						view.indexOf("right_side_trading.jsf") > -1
							|| view.indexOf("ajax.jsf") > -1
							|| view.indexOf("ajax.jsf?countryId") > -1
							|| view.indexOf("ajax.jsf?cardId") > -1
							|| view.indexOf("ajax.jsf?setUTCOffset") > -1
							|| view.indexOf("right_side_investments_inner.jsf") > -1
							|| view.indexOf("banner3.jsf") > -1
							|| view.indexOf("anyoption_agreement.jsf") > -1
							|| view.indexOf("agreement.jsf") > -1
							|| view.indexOf("ticker.jsf") > -1
							|| view.indexOf("tickerWrapper.jsf") > -1
							|| view.indexOf("contactme.jsf") > -1
							|| view.indexOf("rssFeed.jsf") > -1
							|| view.indexOf("contactme_success.jsf") > -1
							|| view.indexOf("right_side_investments") > -1
							|| view.indexOf("investments_profit_box") > -1
							|| view.indexOf("right_side_banners") > -1
							|| view.indexOf("right_side_banners_old") > -1
							|| view.indexOf("etrader_agreement") > -1
							|| view.indexOf("calcalist_game_agreement") > -1
							|| view.indexOf("right_side_game_ranking") > -1
							|| view.indexOf("ajax.jsf?getClockAndTxt=true") > -1
							|| view.indexOf("errorPage.jsf") > -1
							// Live person menu button images (for each status)
							|| view.indexOf("/images/livePerson/reponline.gif") > -1
							|| view.indexOf("/images/livePerson/repoffline.gif") > -1
							|| view.indexOf("/images/livePerson/repoccupied.gif") > -1
							|| view.indexOf("/images/livePerson/transparent.gif") > -1
							//  Live person constactUs button images (for each status)
							|| view.indexOf("/livePerson/contactUsBtn/reponline.gif") > -1
							|| view.indexOf("/livePerson/contactUsBtn/repoffline.gif") > -1
							|| view.indexOf("/livePerson/contactUsBtn/repoccupied.gif") > -1
							// Regulation popup page
							|| view.indexOf("ao_Regulation_popup.jsf") > -1
							|| view.indexOf("headerContent.jsf") > -1
							|| view.indexOf("header_loggedin_table") > -1
							|| view.indexOf("regForm_map") > -1
							|| view.indexOf("register_content") > -1
							|| view.indexOf("register_from_funnel.jsf") > -1
							|| view.indexOf("login_strip.jsf") > -1
							|| view.indexOf("functions_with_jsp.jsf") > -1
							|| view.indexOf("js_vars.jsf") > -1
							//Internal mail popup content
							|| view.indexOf("emailPopup.jsf") > - 1
							|| view.indexOf("emailIframe.jsf") > - 1
							|| view.indexOf("regulationPopupCheck.jsf") > -1
							|| view.indexOf("capital_questionnaire_pop.jsf") > -1
							|| view.indexOf("change_pass_pop.jsf") > -1
							|| view.indexOf("passPopup.jsf") > -1
							|| view.indexOf("single_questionnaire_pop.jsf") > -1
							|| view.indexOf("documents_pop.jsf") > -1
							
							) {
						return; // dont change it
					}
					if (Constants.PORT_HTTP == 0 || Constants.PORT_HTTPS == 0) {
						Constants.PORT_HTTP = Integer.valueOf(CommonUtil.getProperty("http.port"));
						Constants.PORT_HTTPS = Integer.valueOf(CommonUtil.getProperty("https.port"));
						log.debug("http port=" + Constants.PORT_HTTP + ","+ "https port=" + Constants.PORT_HTTPS);
					}

					// To change the port of the page(http/https)
					if (view.indexOf("cards.jsf") > -1
							|| view.indexOf("newCard.jsf") > -1
							|| view.indexOf("cc_delete_approve.jsf") > -1
							|| view.indexOf("firstNewDeposit.jsf") > -1
							|| view.indexOf("/withdraw.jsf") > -1
							|| view.indexOf("reverseWithdraw.jsf") > -1
							|| view.indexOf("withdraw_selection.jsf") > -1
							|| view.indexOf("creditCard_withdraw.jsf") > -1
							|| view.indexOf("changePass.jsf") > -1
							|| view.indexOf("personal.jsf") > -1
							|| view.indexOf("afterRegister.jsf") > -1
							|| view.indexOf("empty_https.jsp") > -1
							|| view.indexOf("deposit.jsf") > -1
							|| view.indexOf("direct24.jsf") > -1
							|| view.indexOf("ACH.jsf") > -1
							|| view.indexOf("payPalDeposit.jsf") > -1
							|| view.indexOf("envoyBankSpain.jsf") > -1
							|| view.indexOf("cashU.jsf") > -1
							|| view.indexOf("payPalWithdrawal.jsf") > -1
							|| view.indexOf("eps.jsf") > -1
							|| view.indexOf("giropay.jsf") > -1
							|| view.indexOf("ajax.jsf?username") > -1
							|| view.indexOf("balance.jsf") > -1
							|| view.indexOf("editCard.jsf") > -1
							//|| view.indexOf("register.jsf") > -1
							|| view.indexOf("registerCalGame.jsf") > -1
							|| view.indexOf("contactus") > -1
							|| view.indexOf("not_available.jsf") > -1
							|| view.indexOf("j_security_check") > -1
							|| view.indexOf("receipt.jsf") > -1
							|| view.indexOf("receipt_content.jsf") > -1
							|| view.indexOf("receiptFirstDeposit.jsf") > -1
							|| view.indexOf("empty_ssl.jsf") > -1
							|| view.indexOf("receipt_print.jsf") > -1
							|| view.indexOf("header_register.jsf") > -1
							|| view.indexOf("firstNewCardRedirect.jsf") > -1
							|| view.indexOf("firstNewCard.jsf") > -1
							|| (view.indexOf("wire.jsf") > -1 && (view.indexOf("anyoption.com") > -1 || view.indexOf("miopciones.com") > -1))
							|| view.indexOf("banners_no_bottom") > -1
							|| view.indexOf("/livePerson/howmayi.jpg") > -1
							|| view.indexOf("/livePerson/leave_msg.jpg") > -1
							|| view.indexOf("/livePerson/send.gif") > -1
							|| view.indexOf("/livePerson/send2.gif") > -1
							|| view.indexOf("images/paypal_header.gif") > -1
							|| view.indexOf("/mobile/login.jsf") > -1
							|| view.indexOf("/password.jsf") > -1
							|| view.indexOf("/loginAuth.jsf") > -1
							|| view.indexOf("/resetPassword.jsf") > -1
							|| view.indexOf("ajax.jsf?emailRegisterAttempts") > -1
							|| view.indexOf("ajax.jsf?emailValidation") > -1
							|| view.indexOf("moneybookers.jsf") > -1
							|| view.indexOf("webmoney.jsf") > -1
							|| view.indexOf("deltaPayChina_withdraw.jsf") > -1
							|| view.indexOf("fearutes2013_pop.jsf") > -1
							|| view.indexOf("registerLite.jsf") > -1
							|| view.indexOf("baroPay.jsf") > -1
							|| view.indexOf("firstCUPDeposit.jsf") > -1
							|| view.indexOf("login.jsf") > -1
							|| view.indexOf("APILogin.jsf") > -1
							|| view.indexOf("paymentMethodSelector.jsf") > -1
							|| view.indexOf("paymentMethodSelector_first.jsf") > -1
							|| view.indexOf("epg_deposit_content.jsf") > -1
							|| view.indexOf("envoyDeposit_content_noDesign.jsf") > -1
							|| view.indexOf("envoyDepositForward.jsf") > -1
							|| view.indexOf("register_additional.jsf") > -1
							|| view.indexOf("terms.jsf") > -1
							|| view.indexOf("agreement.jsf") > -1
							|| view.indexOf("inatecIframeDeposit_content_noDesign.jsf") > -1
							|| view.indexOf("inatecIframeUrlNotification.jsf") > -1
							|| view.indexOf("inatecIframeUrlInternal.jsf") > -1
							|| view.indexOf("inatecIframeUrlReturn.jsf") > -1
							|| view.indexOf("regulationPopupCheck.jsf") > -1
							|| view.indexOf("my_settings.jsf") > -1
							)
					{

						desiredPort = Constants.PORT_HTTPS;

					} else {

						desiredPort = Constants.PORT_HTTP;

					}


					int usingPort = Constants.PORT_HTTP;
					Enumeration e = request.getHeaderNames();
					while (e.hasMoreElements()) {

						String header = (String) e.nextElement();

						// log.debug(header + ": " + request.getHeader(header));

						if (header.equalsIgnoreCase("port")) {
							usingPort = Integer.parseInt(request.getHeader(header).trim());
							break;

						}

					}

					log.debug(view + " " + usingPort + " desired port="+ desiredPort);

					if (desiredPort != usingPort) {
						StringBuffer url = request.getRequestURL();
						log.debug("url before replace " + url.toString());

						if (desiredPort == Constants.PORT_HTTPS) {// SSL - secure
							// page
							url.replace(0, "http".length(), "https");
							log.debug("url after replace " + url.toString());
						} else { // non secure page
							// nothing to replace anyway we get http
						}
						String query = "";
						if (request.getQueryString() != null) {
							query = "?" + request.getQueryString();
							String j_username = request.getParameter("j_username");
							if (j_username != null) {
								query += "&j_username=" + j_username;
							}
						}

						try {
							log.debug("redirecting to: " + url.toString() + query);
							response.sendRedirect(url.toString() + query);
							context.responseComplete();
							return;

						} catch (IOException io) {
							log.error("can't redirect to http/https!!! " + url, io);
						}
					}
				}
			}
		}

	}

	private boolean requestedPages(String requestUrl) {
		return requestUrl.indexOf("index.jsf") > -1 ||
				requestUrl.indexOf("trade_binary_options.jsf") > -1 ||
				requestUrl.indexOf("lastLevels.jsf") > -1 ||
				requestUrl.indexOf("faq.jsf") > -1 ||
				requestUrl.indexOf("assetIndex.jsf") > -1 ||
				requestUrl.indexOf("contactus.jsf") > -1 ||
				requestUrl.indexOf("aboutus.jsf") > -1 ||
				requestUrl.indexOf("security.jsf") > -1 ||
				requestUrl.indexOf("general_terms.jsf") > -1 ||
				requestUrl.indexOf("register.jsf") > -1 ||
				requestUrl.indexOf("password.jsf") > -1 ||
			//	requestUrl.indexOf("virtual_tour.jsf") > -1 ||
				requestUrl.indexOf("banking.jsf") > -1 ||
				requestUrl.indexOf("trading_box_manual.jsf") > -1 ||
				requestUrl.indexOf("affiliates.jsf") > -1 ||
				requestUrl.indexOf("privacy.jsf") > -1 ||
			//	requestUrl.indexOf("personalize.jsf") > -1 ||
				requestUrl.indexOf("press.jsf") > -1 ||
				requestUrl.indexOf("oneTouch.jsf") > -1 ||
				requestUrl.indexOf("optionshandel.jsf") > -1 ||
				requestUrl.indexOf("digitale_optionen.jsf") > -1 ||
				requestUrl.indexOf("binaere_optionen.jsf") > -1 ||
				requestUrl.indexOf("day_trading.jsf") > -1 ||
				requestUrl.indexOf("online_trading.jsf") > -1 ||
				requestUrl.indexOf("optionen_lexikon.jsf") > -1 ||
				requestUrl.indexOf("forex.jsf") > -1 ||
				requestUrl.indexOf("indizes.jsf") > -1 ||
				requestUrl.indexOf("what-are-binary-options.jsf") > -1 ||
				requestUrl.indexOf("how-to-trade-binary-options.jsf") > -1 ||
				requestUrl.indexOf("benefits-of-binary-option-trading.jsf") > -1 ||
				requestUrl.indexOf("options_trading_anyoption.jsf") > -1 ||
				requestUrl.indexOf("oneTouchTerms.jsf") > -1 ||
				requestUrl.indexOf("specialCode.jsf") > -1 ||
				requestUrl.indexOf("newFeatures.jsf") > -1 ||
				requestUrl.indexOf("ultimateBanner.jsf") > -1 ||
				requestUrl.indexOf("editorialBanner.jsf") > -1 ||
				requestUrl.indexOf("iphoneapp.jsf") > -1 ||
				requestUrl.indexOf("indexBanner.jsf") > -1 ||
				requestUrl.indexOf("loginAuth.jsf") > -1 ||
				requestUrl.indexOf("changeSkin.jsf") > -1 ||
				requestUrl.indexOf("resetPassword.jsf") > -1 ||
				requestUrl.indexOf("mobile/appPage.jsf") > -1 ||
				requestUrl.indexOf("/70_generic") > -1 ||
				requestUrl.indexOf("showOff.jsf") > -1 ||
				requestUrl.indexOf("appPage.jsf") > -1 ||
				requestUrl.indexOf("facebook-options.jsf") > -1 ||
				requestUrl.indexOf("optionPlus.jsf") > -1 ||
				requestUrl.indexOf("binary0100.jsf") > -1 ||
				requestUrl.indexOf("fullRegPL.jsf") > -1 || //landing page
				requestUrl.indexOf("newFullRegPL.jsf") > -1 || //landing page
				requestUrl.indexOf("/LP_") > -1 || //landing page
				requestUrl.indexOf("advertorialRu.jsf") > -1 ||
				requestUrl.indexOf("live.jsf") > -1 ||
				requestUrl.indexOf("bitcoin-options.jsf") > -1 ||
				requestUrl.indexOf("eur-usd-options.jsf") > -1 ||
				requestUrl.indexOf("king_candy_crush_options.jsf") > -1 ||
				requestUrl.indexOf("privacy_content.jsf") > -1 ||
				requestUrl.indexOf("aboutus_content.jsf") > -1 ||
				requestUrl.indexOf("security_content.jsf") > -1 ||
				requestUrl.indexOf("general_terms_content.jsf") > -1 ||
				requestUrl.indexOf("risk_disclosure_content.jsf") > -1 ||
				requestUrl.indexOf("termsBonus1.jsf") > -1 ||
				requestUrl.indexOf("termsBonus2.jsf") > -1 ||
				requestUrl.indexOf("termsBonus3.jsf") > -1 ||
				requestUrl.indexOf("termsBonus4.jsf") > -1 ||
				requestUrl.indexOf("termsBonus5.jsf") > -1 ||
				requestUrl.indexOf("termsBonus6.jsf") > -1 ||
				requestUrl.indexOf("termsBonus7.jsf") > -1 ||
				requestUrl.indexOf("termsBonus9.jsf") > -1 ||
				requestUrl.indexOf("termsBonus10.jsf") > -1 ||
				requestUrl.indexOf("termsBonus11.jsf") > -1 ||
				requestUrl.indexOf("termsBonus12.jsf") > -1 ||
				requestUrl.indexOf("termsBonus13.jsf") > -1 ||
				requestUrl.indexOf("registerLite.jsf") > -1 ||
				requestUrl.indexOf("assetIndexInfo_content.jsf") > -1 ||
				requestUrl.indexOf("anyoption_agreement.jsf") > -1 ||
				requestUrl.indexOf("agreement.jsf") > -1 ||
				requestUrl.indexOf("agreement_et.jsf") > -1 ||
				requestUrl.indexOf("register_content.jsf") > -1 ||
				requestUrl.indexOf("APILogin.jsf") > -1 ||
				requestUrl.indexOf("login.jsf") > -1 ||
				requestUrl.indexOf("register_content_funnel.jsf") > -1||
				requestUrl.indexOf("trading-academy.jsf") > -1 ||
				requestUrl.indexOf("trading-academy-introduction.jsf") > -1 ||
				requestUrl.indexOf("trading-psychology.jsf") > -1 ||
				requestUrl.indexOf("financial-management.jsf") > -1 ||
				requestUrl.indexOf("market-analysis.jsf") > -1 ||
				requestUrl.indexOf("beginners-video-tutorials.jsf") > -1 ||
				requestUrl.indexOf("advanced-video-tutorials.jsf") > -1 ||
				requestUrl.indexOf("anyoption-product-guide.jsf") > -1 ||
		        requestUrl.indexOf("strategy.jsf") > -1 ||
		        requestUrl.indexOf("binary-options.jsf") > -1 ||
				requestUrl.indexOf("binary-options-eBook.jsf") > -1 ||
				requestUrl.indexOf("islamic-trading-page.jsf") > -1 ||
				requestUrl.indexOf("register_wm.jsf") > -1
				|| requestUrl.indexOf("powerpay21Status.jsf") > -1

				;
	}

	private String getSkinBySubdomain(HttpServletRequest request) {
		String subDomainFromUrl = request.getServerName().substring(0, request.getServerName().indexOf('.'));
        String skinBySubDomain = ApplicationData.getLocaleBySubDomain(subDomainFromUrl, false);
        if (null == skinBySubDomain) {
        	// make sure we are not missing the www1
        	skinBySubDomain = ApplicationData.getLocaleBySubDomain(subDomainFromUrl, true);
        }
        return skinBySubDomain;
	}

	@Override
	public void afterPhase(PhaseEvent event) {
        if (log.isTraceEnabled()) {
            log.trace("AfterPhase: " + event.getPhaseId());
        }

        if (event.getPhaseId() != PhaseId.RENDER_RESPONSE) {
            this.saveMessages(event.getFacesContext());
        }

		if (event.getPhaseId() == PhaseId.RESTORE_VIEW) {
            FacesContext context = event.getFacesContext();
            StringBuffer requestURL = ((HttpServletRequest)context.getExternalContext().getRequest()).getRequestURL();
            if (requestURL.indexOf("com.ocpsoft.dynaView") != -1) {
                log.warn("Detected dynaview.");
                return;
            }

            HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
            Long loginId = 0L;
            Map<String, Object> sm = context.getExternalContext().getSessionMap();
            Map<String, String> pm = context.getExternalContext().getRequestParameterMap();

            String ip = CommonUtil.getIPAddress();
            if (log.isInfoEnabled()) {
            	try {
	            	String user = context.getExternalContext().getRemoteUser();
	            	String page = (null != context.getViewRoot() ? context.getViewRoot().getViewId() : null);
	            	String sId = session.getId();
	                log.info("User: " + user
	                        + " IP: " + ip
	                        + " Page: " + page
	                        + " Session: " + sId
	                        + " requestURL: " + requestURL);
            	} catch (Exception e) {
            		log.error("Ups...", e);
            	}
            }

            if (log.isTraceEnabled()) {
                String ls = System.getProperty("line.separator");
                //log.trace(ls + ls + "Id: " + context.getViewRoot().getId());
                //log.trace(ls + ls + "ViewId: " + context.getViewRoot().getViewId());
                // log.trace(ls + ls + "ClientId: " + context.getViewRoot().getClientId(context));
                StringBuffer sb = new StringBuffer();
                sb.append(ls).append(ls).append("SessionMap:").append(ls);
                for (Iterator i = sm.keySet().iterator(); i.hasNext();) {
                    String key = (String) i.next();
                    sb.append(key).append(": ").append(sm.get(key)).append(ls);
                }
                log.trace(sb.toString());
                sb = new StringBuffer();
                sb.append(ls).append(ls).append("ParameterMap:").append(ls);
                for (Iterator i = pm.keySet().iterator(); i.hasNext();) {
                    String key = (String) i.next();
                    sb.append(key).append(": ").append(pm.get(key)).append(ls);
                }
                log.trace(sb.toString());
            }

			// For Stress BEGIN
			// For Stress BEGIN
			// For Stress BEGIN
			// For Stress BEGIN
			/*
			 * if (null != pm.get("stress")) {
			 *
			 * if (log.isDebugEnabled()) { log.log(Level.DEBUG, "INSERT STRESS
			 * INVESTMENT "); } String reply = null; SlipForm sf = new
			 * SlipForm();
			 *
			 * long opId = Long.valueOf((String) pm.get("opId")); int ch =
			 * Integer.valueOf((String) pm.get("choice")); long amt =
			 * Long.valueOf((String) pm.get("amount"));
			 *
			 * log.debug("prepare slip: opId: " + opId + " choice:" + ch + "
			 * amount:" + amt);
			 *
			 * //long s=System.currentTimeMillis(); sf.addToSlip(opId, ch);
			 * //long etime=System.currentTimeMillis(); //log.log(Level.DEBUG,
			 * "TIME: add to slip, "+(etime-s));
			 *
			 * sf.updateAmount(opId, amt, ch);
			 *
			 * if (null != sf) { try { DecimalFormat numberFormat5 = new
			 * DecimalFormat("#,##0.00000"); reply = sf.submitFromSlip(opId,
			 * numberFormat5.parse((String)
			 * pm.get("slipEntryLevel")).doubleValue(),
			 * Float.parseFloat((String) pm.get("slipEntryOddWin")), Float
			 * .parseFloat((String) pm.get("slipEntryOddLose")),
			 * Integer.parseInt((String) pm.get("choice")),
			 * Integer.parseInt((String) pm.get("all"))); } catch (Throwable t) {
			 * log.log(Level.WARN, "Ops...", t); } } if (log.isDebugEnabled()) {
			 * log.debug("Response: " + reply); }
			 *
			 * HttpServletResponse response = (HttpServletResponse)
			 * context.getExternalContext().getResponse();
			 * response.setContentType("text/xml; charset=UTF-8");
			 * response.setHeader("Cache-Control", "no-cache"); if (null ==
			 * reply) { response.setStatus(HttpServletResponse.SC_NO_CONTENT); }
			 * else { try { response.getWriter().write(reply); } catch
			 * (Exception e) { log.log(Level.ERROR, "Can't write reply.", e); } }
			 * context.responseComplete();
			 *  }
			 */

			// For Stress TEST COMPLETE
			// For Stress TEST COMPLETE
			// For Stress TEST COMPLETE
			// For Stress TEST COMPLETE
			// For Stress TEST COMPLETE

            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();

            if (null != pm.get("username")) {
				log.debug("handle user name");
                response.setContentType("text/xml");
                response.setCharacterEncoding("utf-8");

				try {
					String out = UsersManager.validateUserName((String) pm.get("username"));
					response.getWriter().write("<errormsg>" + out + "</errormsg>");
				} catch (Exception e) {
					log.log(Level.ERROR,
							"Can't write reply or SQL EXCEPTION!!.", e);
				}

				context.responseComplete();
			}

            if (null != pm.get("emailValidation")) {
				log.debug("handle email AJAX call");
                response.setContentType("text/xml");
                response.setCharacterEncoding("utf-8");
				try {
					String out = "&#160;";
					if (UsersManager.isEmailInUse((String) pm.get("emailValidation"), 0)) {
						out = CommonUtil.getMessage("error.register.email.inuse", null);
					}
					response.getWriter().write("<emailErrormsg>" + out + "</emailErrormsg>");
				} catch (Exception e) {
					log.log(Level.ERROR,
							"Can't write reply or SQL EXCEPTION for email AJAX call!", e);
				}
				context.responseComplete();
			}

            //email validation for static LP
            if (null != pm.get("landingEmailValidation")) {
				log.debug("handle email AJAX call");
                response.setContentType("text/xml");
                response.setCharacterEncoding("utf-8");
				try {
	                String email = (String) pm.get("landingEmailValidation");
	                long skinId = Skin.SKIN_REG_EN;
	                if (null != pm.get("s")) {
	                	skinId = Long.valueOf((String) pm.get("s"));
	                }
	                String langCode = ApplicationData.getLanguage(ApplicationData.getSkinById(skinId).getDefaultLanguageId()).getCode();
	                Locale locale = new Locale(langCode);
					String out = "valid";
					if (!CommonUtil.isEmailValidUnix(email)) {
						out = CommonUtil.getMessage("error.banner.contactme.email", null, locale);
					} else if (UsersManager.isEmailInUse(email, 0)) {
						out = CommonUtil.getMessage("error.register.email.inuse", null, locale);
					}
					response.getWriter().write("<emailErrormsg>" + out + "</emailErrormsg>");
				} catch (Exception e) {
					log.log(Level.ERROR,
							"Can't write reply or SQL EXCEPTION for email AJAX call!", e);
				}
				context.responseComplete();
			}

			// for phone code request from register page
			if (null != pm.get("countryId")) {
				log.debug("handle country id from registration page");
//				HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
				response.setContentType("text/xml");
				response.setCharacterEncoding("utf-8");
//				response.setHeader("Cache-Control", "no-cache");

				try {
					//ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
					String out = ApplicationData.getCountryPhoneCodes((String) pm.get("countryId"));
					response.getWriter().write("<phoneCode>" + out + "</phoneCode>");
				} catch (Exception e) {
					log.log(Level.ERROR, "Can't write reply for phoneCode request!!.", e);
				}
				context.responseComplete();
			}

			// for credit card request from withdrawal page
			if (null != pm.get("cardId")) {
				log.debug("handle cardId from creditCardWithdrawal page");
//				HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
				response.setContentType("text/xml");
				response.setCharacterEncoding("utf-8");
//				response.setHeader("Cache-Control", "no-cache");

				try {
					String out = "";
					CreditCard cc = null;
					WithdrawForm withdawalForm = (WithdrawForm) context.getApplication().createValueBinding(Constants.BIND_WITHDRAWAL_FORM).getValue(context);
					if (null != withdawalForm) {
						HashMap<Long, CreditCard> ccHash = withdawalForm.getCcHash();
						cc = ccHash.get(Long.valueOf((String)pm.get("cardId")));
						if (null != cc) {
							if (cc.isCreditEnabled()) {
								User user = ApplicationData.getUserFromSession();
								String[] params = new String[1];
						        params[0] = String.valueOf(CommonUtil.displayAmount(cc.getCreditAmount(), user.getCurrencyId()));
						        out = CommonUtil.getMessage("withdrawals.credit.option.msg", params);
							}
						}
					}
					response.getWriter().write("<creditMsg>" + out + "</creditMsg>");
				} catch (Exception e) {
					log.log(Level.ERROR,
							"Can't write reply for credit card request!!.", e);
				}
				context.responseComplete();
			}

			// for phone and fax request from contactUs page
			if (null != pm.get("countryIdForSupport")) {
				log.debug("handle country id from ContactUs page");
//				HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
				response.setContentType("text/xml");
				response.setCharacterEncoding("utf-8");
//				response.setHeader("Cache-Control", "no-cache");

				try {
					String phone = "";
					String fax = "";
					String phoneCode = "";
					if ( ((String)pm.get("countryIdForSupport")).equals(Constants.CONTACTUS_DEFAULT_COUNTRY)  ) {
						phone = CommonUtil.getMessage("contact.phone.default", null);
						fax = CommonUtil.getMessage("contact.fax.default", null);
					}
					else {
						//ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
						phone = ApplicationData.getSupportPhoneByCountryId((String) pm.get("countryIdForSupport"));
						fax = ApplicationData.getSupportFaxeByCountryId((String) pm.get("countryIdForSupport"));
						phoneCode = ApplicationData.getCountryPhoneCodes((String) pm.get("countryIdForSupport"));
					}
					response.getWriter().write("<SupportPhoneFax>" + phone + "," + fax + ","+phoneCode+ "</SupportPhoneFax>");
				} catch (Exception e) {
					log.log(Level.ERROR,
							"Can't write reply for support phone and fax request!!.", e);
				}
				context.responseComplete();
			}

            if (null != pm.get("setUTCOffset")) {
                try {
//      				HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        			response.setContentType("text/xml");
        			response.setCharacterEncoding("utf-8");
//        			response.setHeader("Cache-Control", "no-cache");

        			String utcOffset = "";

        			ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
        			if (CommonUtil.isHebrewSkin(ap.getSkinId())) { // for etrader/TLV - put always GMT+02 / GMT+03
	        			TimeZone tzEt = TimeZone.getTimeZone("Israel");
	        			utcOffset = ConstantsBase.UTCOFFSET_DEFAULT_PERIOD2;
	        			if (tzEt.inDaylightTime(new Date())) {
	        				utcOffset = ConstantsBase.UTCOFFSET_DEFAULT_PERIOD1;
	        			}
        			} else {   // for anyoption, dynamic by user offset
        				utcOffset = CommonUtil.formatJSUTCOffsetToString(Integer.parseInt((String) pm.get("setUTCOffset")));
        			}

                    sm.put(Constants.UTC_OFFSET, utcOffset);
                    if (log.isInfoEnabled()) {
                        log.info("Set utcOffset in session: " + utcOffset);
                    }

                    // respone for saving value in the session
        			response.getWriter().write("<offset>" + utcOffset + "</offset>");

       				context.responseComplete();

                } catch (Throwable t) {
                    log.error("Can't save UTC offset.", t);
                }
            }

//          for insurance clock and txt
            if (null != pm.get("getClockAndTxt")) {
                log.debug("handle clock and txt for insurance banner");
//                HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
                response.setContentType("text/xml");
                response.setCharacterEncoding("utf-8");
//                response.setHeader("Cache-Control", "no-cache");

                try {
                    String replay = ApplicationData.getInsuranceInvesetmentsByUser();
                    log.debug("clock and txt replay " + replay);
                    response.getWriter().write("<time>" + replay + "</time>");
                } catch (Exception e) {
                    log.log(Level.ERROR,
                            "Can't write reply for clock and txt for insurance banner!!.", e);
                }
                context.responseComplete();
            }

            if (pm.get("numUserAnswers") != null) {
            	log.debug("Updating questionnaire user answers [" + pm + "]");
            	try {
	            	int numUserAnswers = Integer.parseInt((String) pm.get("numUserAnswers"));

	            	String returnParam = null;
	            	boolean isETRegulation = (Boolean) session.getAttribute(Constants.SESSION_IS_ET_REGULATION);
	            	QuestionnaireGroup questionnaireGroup = null;
	            	if (CommonUtil.isUserSkinRegulated() || isETRegulation) {
	            		QuestionnaireGroup group = QuestionnaireManagerBase
            										.getGroupByUserAnswerId(
        												Long.parseLong((String) pm.get("userAnswer0Id")));
	            		UserRegulation ur = new UserRegulation();
	            		questionnaireGroup = group;
	            		if (((QuestionnaireGroup.REGULATION_MANDATORY_QUESTIONNAIRE_NAME.equals(group.getName()) || QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME.equals(group.getName()))
									&& ur.getApprovedRegulationStep() >= UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE)
								|| (QuestionnaireGroup.REGULATION_OPTIONAL_QUESTIONNAIRE_NAME.equals(group.getName())
									&& ur.isOptionalQuestionnaireDone())
								|| (QuestionnaireGroup.REGULATION_CAPITAL_QUESTIONNAIRE_NAME.equals(group.getName())
									&& ur.getApprovedRegulationStep() == UserRegulationBase.ET_CAPITAL_MARKET_QUESTIONNAIRE)) {
	            			log.error("Trying to update answers of questionnaire [" + group.getName() + "] that is already submitted");
	            			returnParam = "error";
	            		}
	            	}

	            	if (returnParam == null) {
		            	String userAnswerId;
		            	String answerId;
		            	QuestionnaireUserAnswer userAnswer;
		            	List<QuestionnaireUserAnswer> userAnswers = new ArrayList<QuestionnaireUserAnswer>();
		            	for (int i = 0; i < numUserAnswers; i++) {
		            		userAnswerId = (String) pm.get("userAnswer" + i + "Id");
		            		if (userAnswerId != null) {
		            			userAnswer = new QuestionnaireUserAnswer();
		            			userAnswer.setUserAnswerId(Long.parseLong(userAnswerId));
		            			answerId =  (String) pm.get("answer" + i + "Id");
		            			if (answerId != null) {
		            				userAnswer.setAnswerId(Long.parseLong(answerId));
		            				userAnswer.setTextAnswer((String) pm.get("answer" + i + "Text"));
		            			}
		            			userAnswer.setWriterId(Writer.WRITER_ID_WEB);
		            			userAnswers.add(userAnswer);
		            			if(isETRegulation && userAnswer.getAnswerId() != null){
		            				userAnswer.setScore(QuestionnaireManagerBase.getAnswertScore(questionnaireGroup, userAnswer));
		            			}
		            		}
		            	}
		            	if (userAnswers.size() > 0) {
		            		QuestionnaireManagerBase.updateUserAnswers(userAnswers, isETRegulation);
		            		returnParam = "ok";
		            		if(isETRegulation && QuestionnaireManagerBase.isAllCapitalAnswFilled(ApplicationData.getUserFromSession().getId())){
		            			returnParam = "canSubmit";
		            		}
		            		log.debug("User answers updated successfully");
		            	} else {
		            		returnParam = "error";
		            		log.debug("No user answers to update");
		            	}

	            	}

	            	finishAJAXRequest(context, "application/x-www-form-urlencoded; charset=UTF-8", returnParam);
            	} catch (Exception e) {
            		log.error("Unable to process questionnaire AJAX request", e);
            		finishAJAXRequest(context, "application/x-www-form-urlencoded; charset=UTF-8", "error");
            	}
            }

			if (null != pm.get("slipEntryId") || null != pm.get("box") || null != pm.get("insuranceAmount") || null != pm.get("opp_time_exp")) {
                checkRequestMethod(context, session);
				String reply = null;
//				SlipForm sf = (SlipForm) context.getApplication().createValueBinding("#{slipForm}").getValue(context);
				// SlipForm sf = (SlipForm) sm.get("slipForm");
				if (/*null != sf && */null == pm.get("insuranceAmount") && null == pm.get("opp_time_exp")) {
					try {
						String cmd = (String) pm.get("command");
						if (null != cmd && !cmd.trim().equals("")) {
							long id = Long.parseLong((String) pm.get("slipEntryId"));
							if (log.isEnabledFor(Level.DEBUG)) {
								String strChoice = (String) pm.get("choice");
                                log.log(Level.DEBUG, "AJAX command: " + cmd +
                                        " id: " + id +
                                        " choice: " + strChoice +
                                        " user: " + context.getExternalContext().getRemoteUser() +
                                        " sessionId: " + session.getId());
							}
/*							if (cmd.equals("add")) {
								int choice = Integer.parseInt((String) pm.get("choice"));
								sf.addToSlip(id, choice);
							} else if (cmd.equals("update")) {
								int choice = Integer.parseInt((String) pm.get("choice"));
								String am = (String) pm.get("slipEntryAmount");
								if (null == am || am.trim().equals("")) {
									sf.updateAmount(id, 0, choice);
								} else {
									float amount = Float.parseFloat(am);
									sf.updateAmount(id, amount, choice);
								}
							} else if (cmd.equals("remove")) {
								int choice = Integer.parseInt((String) pm.get("choice"));
								sf.removeFromSlip(id, choice);
								if (choice == -1) {
									// we get choice -1 when we try to remove
									// successful entry in the client slip
									sf.clearSuccessful();
								}
							} else*/ if (cmd.equals("submit")) {
								User user = null;
								try {
									user = ApplicationData.getUserFromSession();
								} catch (Exception e) {
									log.error("Can't get user");
								}
								if (null != user && user.isAcceptedTerms() == false) {
									finishAJAXRequest(context, "application/x-www-form-urlencoded; charset=UTF-8", "redirectToTerms");
									return;
								}
								int choice = Integer.parseInt((String) pm.get("choice"));
                                String am = (String) pm.get("amount");
                                double amount = 0;
                                try {
                                    amount = Double.parseDouble(am);
                                } catch (Throwable t) {
                                    log.warn("Invalid amount: " + am);
                                }

								DecimalFormat numberFormat5 = new DecimalFormat("#,##0.00000", DecimalFormatSymbols.getInstance(Locale.US));
								
								if (session != null) {
									try {
										loginId = (Long)session.getAttribute(Constants.LOGIN_ID) == null ? 0L : (Long)session.getAttribute(Constants.LOGIN_ID);
									} catch (Exception e) {
										log.error("Cannot get login ID from session!", e);
										loginId = 0L;
									}
						    	}
								try {
									String view = null;
									if (event.getPhaseId() == PhaseId.RESTORE_VIEW) {
										HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
										view = request.getRequestURL().toString();
									}
                                    int utcOffset = -120;
                                    if (null != pm.get(Constants.UTC_OFFSET)) {
                                        utcOffset = Integer.parseInt((String) pm.get(Constants.UTC_OFFSET));
                                    } else {
                                        if (log.isInfoEnabled()) {
                                            log.info("Using default GMT+2 offset");
                                        }
                                    }
									if (null != view && view.indexOf("oneTouchAjax.jsf") > -1) {
										reply = SlipForm.submitOneTouchFromSlip(
	                                            id,
	                                            amount,
												numberFormat5.parse((String) pm.get("slipEntryLevel")).doubleValue(),
												Float.parseFloat((String) pm.get("slipEntryOddWin")),
												Float.parseFloat((String) pm.get("slipEntryOddLose")),
												choice,
												Integer.parseInt((String) pm.get("all")),
                                                utcOffset,
                                                loginId);
									} else {
										reply = SlipForm.submitFromSlip(
												id,
	                                            amount,
												numberFormat5.parse((String) pm.get("slipEntryLevel")).doubleValue(),
												Float.parseFloat((String) pm.get("slipEntryOddWin")),
												Float.parseFloat((String) pm.get("slipEntryOddLose")),
												choice,
												Integer.parseInt((String) pm.get("all")),
                                                utcOffset,
                                                Integer.parseInt((String) pm.get("fromGraph")),
                                                new Boolean((String) pm.get("dev2Second")),
                                                loginId);
									}


								} catch (NumberFormatException nfe) {
									log.warn(
                                            "Invalid investment request - slipEntryLevel: " + pm.get("slipEntryLevel") +
                                            " slipEntryOddWin: " + pm.get("slipEntryOddWin") +
                                            " slipeEntryOddLose: " + pm.get("slipEntryOddLose") +
                                            " all: " + pm.get("all") + " " + nfe.getMessage());
								}
//							} else if (cmd.equals("removeall")) {
//								sf.removeAllFromSlip();
//							} else if (cmd.equals("submitall")) {
							} else if (cmd.equals("time")) {
								HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
								OpportunityCache oc = (OpportunityCache) request.getServletContext().getAttribute(Constants.OPPORTUNITY_CACHE);
								OpportunityCacheBean opp = oc.getOpportunity(id);
                                long timeLeft = 0;
                                long minLeft = 0;
                                long secLeft = 0;
                                if (null != opp && null != opp.getTimeLastInvest()) {
                                    timeLeft = opp.getTimeLastInvest().getTime() - System.currentTimeMillis();
                                    minLeft = Math.round(timeLeft / 60000);
                                    secLeft = Math.round((timeLeft % 60000) / 1000);
    								if (minLeft < 0 || secLeft < 0) {
    									log.error("Stelled data in Lightstreamer!!! Requested time left for oppId: " + id +
                                                " minLeft: " + minLeft +
                                                " secLeft: " + secLeft);
    								}
    								if (minLeft < 0) {
    									minLeft = 0;
    								}
    								if (secLeft < 0) {
    									secLeft = 0;
    								}
                                } else {
                                    log.warn("Time command for opp that is most likely already closed!!!");
                                }
								reply = id + "_" + minLeft + "_" + secLeft;
								if (log.isDebugEnabled()) {
									log.debug("Time left for oppId: " + id + " reply: " + reply);
								}
//							} else if (cmd.equals("print")) {
//								sf.setEntryToPrint(id);
							} else {
								if (log.isEnabledFor(Level.WARN)) {
									log.log(Level.WARN,	"Unknown AJAX command: " + cmd);
								}
							}
						}
					} catch (Throwable t) {
						log.log(Level.WARN, "Problem processing AJAX request.",	t);
						try {
							log.log(Level.DEBUG, "AJAX command: " + pm.get("command") +
							     " id: " + pm.get("slipEntryId") +
							     " choice: " + pm.get("choice") +
							     " user: " + context.getExternalContext().getRemoteUser() +
							     " sessionId: " + session.getId());
						} catch (Exception e) {
							log.log(Level.DEBUG, "cant print pm error" , e);
						}
						reply = CommonUtil.getMessage("error.investment.deviation", null);
					}
				}

                if (null != pm.get("insuranceAmount")) {
                    if (null != pm.get("invId") && null != pm.get("insuranceType") && null !=pm.get("rfType")) {
                        try {
                        	
                        	
                        	double insurancePremiaPercen;
                        	User user = null;
                        	Investment investment;
                        	String replyValidate = null;

                        	String[] invIdArray = ((String)pm.get("invId")).split("_");
                            String[] insuranceTypeArray = ((String)pm.get("insuranceType")).split("_");
                            String[] insuranceAmountArray = ((String)pm.get("insuranceAmount")).split("_");
                            String[] gmTypeArray = ((String)pm.get("rfType")).split("_");
                            
                        	GoldenMinuteType gmType = GoldenMinuteType.STANDARD;
                        	if(gmTypeArray[0].equals(GoldenMinuteType.ADDITIONAL.name())){
                        		gmType = GoldenMinuteType.ADDITIONAL;
                        	}
                            
                            for(int i = 0; i < invIdArray.length; i++) {
                                try {
                                	replyValidate = null;
                                	long invId = Long.parseLong(invIdArray[i].trim());
                                    long insuranceType = Long.parseLong(insuranceTypeArray[i].trim());
                                    long insuranceAmount = new BigDecimal(insuranceAmountArray[i].trim()).multiply(new BigDecimal("100")).longValue();
                                    
                                    log.info("starting insurance amount buying for investment id: " + invId + " insurance Amount: " + insuranceAmount + " insurance Type: " + insuranceType);
                                    //checking if user is loged in
                                    if (!context.getExternalContext().isUserInRole("web")) {
                                        if (log.isInfoEnabled()) {
                                            log.info("Login first." +
                                                    " user: " + context.getExternalContext().getRemoteUser() +
                                                    " sessionId: " + session.getId());
                                        }
                                        // the leading "1" of this message will make sure only 1 msg show to user
                                        reply += "_1" + CommonUtil.getMessage("error.investment.login", null);
                                    } else {

                                        //get the investment to buy insurance for
                                        boolean isGM = insuranceType == ConstantsBase.INSURANCE_GOLDEN_MINUTES ? true : false;
                                        user = ApplicationData.getUserFromSession();
                                        investment = InvestmentsManagerBase.getInvestmentToSettle(invId, false, isGM, user.getId());
                                        if (investment != null) { //if the investment is not settled yet
                                        	//validate insurance buy
                                        	replyValidate = InvestmentsManager.validateInsuranceBuy(user, insuranceAmount, investment, gmType);
                                            if (null == replyValidate) {
	                                        	insurancePremiaPercen = (insuranceType == ConstantsBase.INSURANCE_GOLDEN_MINUTES ? investment.getInsurancePremiaPercent() : investment.getRollUpPremiaPercent());
	                                        	long dbInsuranceAmount = Math.round(InvestmentsManagerBase.getAmountWithoutFees(investment) * insurancePremiaPercen);
	                                        	log.debug("insurancePremiaPercen " + insurancePremiaPercen + "  investment.getAmount() " + investment.getAmount() + " dbInsuranceAmount=" + dbInsuranceAmount);
	                                            // check that page insurance amount is right
	                                            if (dbInsuranceAmount != insuranceAmount) {
	                                                log.info("page insurance amount: " + insuranceAmount + " not equle to db insurance amount: " + investment.getAmount() * insurancePremiaPercen);
	                                                reply += "_" + CommonUtil.getMessage("golden.error.wrong.amount", null);
	                                            } else {
	                                            	WebLevelsCache levelsCache = (WebLevelsCache) context.getExternalContext().getApplicationMap().get("levelsCache");
                                                	double convertAmount =  investment.getAmount() * investment.getRate();
	                                                if (insuranceType == ConstantsBase.INSURANCE_GOLDEN_MINUTES) {
	                                                    //buy the insurance and settle the investment
	                                                	if (session != null) {
	                                                		try {
	                                                			loginId = (Long)session.getAttribute(Constants.LOGIN_ID) == null ? 0L : (Long)session.getAttribute(Constants.LOGIN_ID);
	                                                		} catch (Exception e) {
	                                                			log.error("Cannot get login ID from session!", e);
	                                                			loginId = 0L;
	                                                		}
	                                                	}
	                                                	InvestmentsManager.boughtGoldenMinutesInvestment(invId, insuranceAmount, investment, user, loginId);
	                                                	if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
	                                                		CommonUtil.sendNotifyForInvestment(
                                            							investment.getOpportunityId(),
                                            							investment.getId(),
                                            							(-1d) * convertAmount,
                                            							(-1L) * investment.getAmount(),
                                            							investment.getTypeId(),
                                            							investment.getMarketId(),
                                            							OpportunityType.PRODUCT_TYPE_BINARY,
                                            							investment.getCurrentLevel(),
                                            							levelsCache,
                                            							user,
                                            							CopyOpInvTypeEnum.of(investment.getCopyOpTypeId()));
                                                        }
	                                                    log.info("bought golden minutes insurance for user: " + context.getExternalContext().getRemoteUser() + " invId: " + invId + " insuranceAmount: " + insuranceAmount + " insurance type: " + insuranceType);

	                                                } else {
	                                                    //we count on this that we have next opp id :)
	                                                    Opportunity nextOpp = InvestmentsManager.getNextHourlyOppToOpenByOppId(investment.getOpportunityId());
                                                        if (nextOpp == null) {    // null means that it is golden minutes
                                                            log.info("Next opportunity is null");
                                                            reply += "_" + CommonUtil.getMessage("error.investment", null);
                                                        } else {
                                                        	if (session != null) {
                                                        		try {
                                                        			loginId = (Long)session.getAttribute(Constants.LOGIN_ID) == null ? 0L : (Long)session.getAttribute(Constants.LOGIN_ID);
                                                        		} catch (Exception e) {
                                                        			log.error("Cannot get login ID from session!", e);
                                                        			loginId = 0L;
                                                        		}
                                                        	}
                                                            long nextOppId = nextOpp.getId();
                                                            InvestmentsManager.rollUpInvestment(investment, user, insuranceAmount, nextOppId, loginId);
                                                            if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
    	                                                		CommonUtil.sendNotifyForInvestment(
	    	                                                					nextOppId,
	    	                                                					investment.getId(),
	    	                                                					convertAmount,
	    	                                                					investment.getAmount(),
	    	                                                					investment.getTypeId(),
	    	                                                					investment.getMarketId(),
	    	                                                					OpportunityType.PRODUCT_TYPE_BINARY,
	    	                                                					investment.getCurrentLevel(),
	    	                                                					levelsCache,
	    	                                                					user,
	                                                							CopyOpInvTypeEnum.of(investment.getCopyOpTypeId()));
                                                            }
                                                            log.info("bought roll up insurance for user: " + context.getExternalContext().getRemoteUser() + " invId: " + invId + " insuranceAmount: " + insuranceAmount + " insurance type: " + insuranceType);
                                                        }
	                                                }
	                                                if (null == reply) {
		                                                RewardUserTasksManager.rewardTasksHandler(TaskGroupType.INVESTMENT_PREMIA, user.getId(),  insuranceAmount, invId, BonusManagerBase.class, Constants.WRITER_WEB_ID, OpportunityType.PRODUCT_TYPE_BINARY, Opportunity.TYPE_REGULAR);
		                                                reply += "_OK" + invId;
	                                                }
	                                            }
                                            } else {
                                            	reply += "_" + replyValidate;
                                            }
                                        } else { // investment is already settled
                                            log.info("trying to buy insurance for settled investment! investment id: " + invId);
                                            reply += "_" + CommonUtil.getMessage("error.investment", null);
                                        }
                                    }
                                } catch (Exception e) {
                                    log.info("cant buy insurance for invId: " + invIdArray[i] + " insuranceAmount: " + insuranceAmountArray[i] + "00" + " insurance type: " + insuranceTypeArray[i], e);
                                    reply += "_" + CommonUtil.getMessage("error.investment", null);
                                }
                            }
                        } catch(Exception e) {
                            log.info("cant buy insurance for invId: " + pm.get("invId").toString() + " insuranceAmount: " + pm.get("insuranceAmount").toString() + " insurance type: " + pm.get("insuranceType") ,e);
                            reply += "_" + CommonUtil.getMessage("error.investment", null);;
                        }

                    } else {
                        log.debug("no investment id or insurance type");
                        reply += "_" + CommonUtil.getMessage("error.investment", null);
                    }
                    //remove the begining "null_"
                    reply = reply.substring(5);
                } else if (null != pm.get("opp_time_exp")) {
                    long id = 0;

                    try {
                        id = Long.valueOf(pm.get("opp_time_exp").toString());
                    } catch (Exception e) {
                        log.info("cant parse investment id " + pm.get("opp_time_exp").toString(), e);
                    }
                    if (id != 0) {
                        reply = ApplicationData.getInsuranceClockTimeByInvId(id);
                    }
                    if (log.isDebugEnabled()) {
                        log.debug("Time left for buy insurance investment id: " + id + " reply: " + reply);
                    }
                }

                finishAJAXRequest(context, "application/x-www-form-urlencoded; charset=UTF-8", reply);
			}

            //this ajax is to store in db when user first see the insurance banner OR when he first time click on open detail button in the banner
            if (null != pm.get("insuranceBanner")) {
                String reply = "insuranceBanner failed to insert";
                if (!context.getExternalContext().isUserInRole("web")) {
                    if (log.isInfoEnabled()) {
                        log.info("Login first." +
                                " user: " + context.getExternalContext().getRemoteUser() +
                                " sessionId: " + session.getId());
                    }
                    // the leading "1" of this message will make sure only 1 msg show to user
                    reply = "insuranceBanner failed user not loged in";
                } else { //if user is loged in
                    try {
                        int bannerStartTime = Integer.valueOf(CommonUtil.getEnum("insurance_period_start_time", "insurance_period_start_time"));
                        int bannerEndTime = Integer.valueOf(CommonUtil.getEnum("insurance_period_end_time", "insurance_period_end_time"));
                        Calendar currentDate = Calendar.getInstance();
                        int min  = -1 * (1 + bannerStartTime - bannerEndTime);
                        currentDate.add(Calendar.MINUTE, min); //add extra 1 min
                        Calendar sessionDate = (Calendar)session.getAttribute("insuranceBannerDate");
                        log.debug("user " + context.getExternalContext().getRemoteUser() + " checking session date " + sessionDate + " cuurent date " + currentDate.getTime().toString());
                        if (null == sessionDate || null != sessionDate && sessionDate.before(currentDate)) { //if we didnt see the banner or we saw it more then 5 min ago continue
                            User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
                            log.debug("checking db " + user.getId());
                            boolean isViewed = InvestmentsManager.isGMBannerViewed(user.getId());
                            log.debug("isViewed " + isViewed);
                            if (!isViewed) {
                                log.debug("insert into db " + user.getId());
                                if (InvestmentsManager.insertGMBannerViewed(user.getId())) { //if we insert the row save it also in the session
                                    session.setAttribute("insuranceBannerDate", Calendar.getInstance());
                                    log.debug("success in db");
                                    reply = "insuranceBanner done";
                                }
                                reply = "faild to insert into db";
                            }
                            reply = "user already saw this banner db check";
                        }
                        reply = "user already saw this banner session date check";
                    } catch (Exception e) {
                        log.error("Cant insert golden minutes banner view" , e);
                    }
                }
                finishAJAXRequest(context, "text/xml; charset=UTF-8", reply);
            }
            
//            if(requestURL.indexOf("fibonatixStatus") > -1){
//				FibonatixClearingProvider fibonatix =  (FibonatixClearingProvider) ClearingManager.getClearingProviders().get(ClearingManager.FIBONATIX_3D_PROVIDER_ID);
//            	log.info(pm);
//            	if(fibonatix.isValidCall(pm.get("control"), pm.get("status"), pm.get("orderid"), pm.get("client_orderid"))) {
//            		try{
//	            		long transactionId = Long.parseLong(pm.get("client_orderid"));
//	            		
//	            		Transaction t = TransactionsManagerBase.getTransaction(transactionId);
//	            		if(t != null) {
//		            		if(t.getStatusId()==TransactionsManagerBase.TRANS_STATUS_SUCCEED) {
//		            			try {
//		            				PopulationsManagerBase.deposit(t.getUserId(), true, CommonUtil.getWebWriterId(), t);
//		            			} catch (Exception e) {
//		            				log.warn("Problem with population succeed deposit event " + e);
//		            			}
//		                        try {
//		                        	TransactionsManagerBase.afterTransactionSuccess(t.getId(), t.getUserId(), t.getAmount(), CommonUtil.getWebWriterId(), loginId);
//		                        } catch (Exception e) {
//		                            log.error("Problem processing bonuses after success transaction", e);
//		                        }
//		                    } else {
//	
//		                    	FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.inatec.deposit", null), null);
//		   						context.addMessage(null, fm);
//		   						
//		                    	try {
//		            				PopulationsManagerBase.deposit(t.getUserId(), false, CommonUtil.getWebWriterId(),t);
//		            			} catch (Exception e) {
//		            				log.warn("Problem with population failed deposit event " + e);
//		            			}
//		                    }
//		                    ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
//		                    ve.setValue(context.getELContext(), t);
//		                    
//		                    User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
//		    				if(user == null || user.getId() == 0) {
//		    					String  userName = il.co.etrader.bl_managers.UsersManagerBase.getUserNameById(t.getUserId());
//		    					user = new User();
//		    					user.setUserName(userName);
//		    				}
//		                    TransactionsManager.afterDepositAction(user, t, TransactionSource.TRANS_FROM_WEB);
//	            		} else {
//	            			log.error("FAILED TO UPDATE FIBONATIX TRX");
//	            		}
//	                } catch (Throwable t) {
//	                    log.error("Failed to process 3D Secure authentication response.", t);
//	                }
//	                response.setStatus(HttpServletResponse.SC_OK);
//            	}
//            }

			if(requestURL.indexOf("powerpay21Status") > -1){ // SofortÜberweisung
				log.info("powerpay21 call");
				Powerpay21ClearingProvider powerpay21 =  (Powerpay21ClearingProvider) ClearingManager.getClearingProviders().get(ClearingManager.POWERPAY21_PROVIDER_ID_APS);
				/*if(powerpay21.isValidSofort(pm)) {
					String txid = pm.get("orderid");
					log.debug("Transaction id :" + txid);
					String providerTrxId = pm.get("transactionid");
					log.debug("Provider transaction id :" + providerTrxId);
					
					long transactionId = Long.parseLong(txid);
					
					ApplicationData app = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
					boolean isTest = app.getIsTest();

        			Transaction transaction = TransactionsManagerBase.updatePowerPayTransactionStatus(loginId,transactionId, powerpay21.isSuccessSofort(pm, isTest), providerTrxId,pm.get("errormessage"));
           			try {
           				if(transaction != null) {
           					boolean success = (transaction.getStatusId() == TransactionsManagerBase.TRANS_STATUS_SUCCEED);
           					if(success) {
           						TransactionsManagerBase.afterTransactionSuccess(transaction.getId(), transaction.getUserId(), transaction.getAmount(), transaction.getWriterId(), loginId);
	           					// add transaction to context
	           			        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
	           			        ve.getValue(context.getELContext());
	           			        ve.setValue(context.getELContext(), transaction);
           					} else {
           						FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.inatec.deposit", null), null);
           						context.addMessage(null, fm);
           					}
           					PopulationsManagerBase.deposit(transaction.getUserId(), success, transaction.getWriterId(), transaction);
           				}
            		} catch (Exception e) {
            			log.error("Problem with population deposit event ", e);
            		}        			
				} else*/ if(powerpay21.isValidEps(pm)) {
					String txid = pm.get("TXID");
					log.debug("Transaction id :" + txid);
					
					long transactionId = Long.parseLong(txid);
        			Transaction transaction = TransactionsManagerBase.updatePowerPayTransactionStatus(loginId,transactionId, powerpay21.isSuccessEPS(pm), "", pm.get("ERRMSG"));
           			try {
           				if(transaction != null) {
           					boolean success = (transaction.getStatusId() == TransactionsManagerBase.TRANS_STATUS_SUCCEED);
           					if(success) {
           						TransactionsManagerBase.afterTransactionSuccess(transaction.getId(), transaction.getUserId(), transaction.getAmount(), transaction.getWriterId(), loginId);
	           					// add transaction to context
	           			        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
	           			        ve.getValue(context.getELContext());
	           			        ve.setValue(context.getELContext(), transaction);
           					} else {
           						FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.inatec.deposit", null), null);
           						context.addMessage(null, fm);
           					}
           					PopulationsManagerBase.deposit(transaction.getUserId(), success, transaction.getWriterId(), transaction);
           				}
            		} catch (Exception e) {
            			log.error("Problem with population deposit event ", e);
            		}        			
				} else {
					log.error("Invalid powerpay status" + pm);
				}
			}
            
            //ezeeBill status URL
            if (requestURL.indexOf("ezeeBillStatus") != -1) {
            	log.info("Going to check transaction status from ezeeBillStatus");
            	try {
            		if (session != null) {
            			try {
            				loginId = (Long)session.getAttribute(Constants.LOGIN_ID) == null ? 0L : (Long)session.getAttribute(Constants.LOGIN_ID);
            			} catch (Exception e) {
            				log.error("Cannot get login ID from session!", e);
            				loginId = 0L;
            			}
            		}
            		if(ezeebillRequestIsComplete(pm) && HMAC.isValid(pm, EzeebillClearingProvider.hashKey))
            		{
            			log.debug("Transaction status :" + pm.get("txn_status"));
            			Transaction transaction = TransactionsManagerBase.updateEzeebillTransactionStatus(
            																								loginId, 
            																								Long.parseLong(pm.get("merch_txn_id")), 
            																								Long.parseLong(pm.get("amount")), 
            																								Long.parseLong(pm.get("txn_response_code")), 
            																								Long.parseLong(pm.get("txn_no")), 
            																								pm.get("txn_message")
            																							 );
	           			try {
	           				if(transaction != null) {
	           					boolean success = (transaction.getStatusId() == TransactionsManagerBase.TRANS_STATUS_SUCCEED);
	           					if(success) {
		           					// add transaction to context
		           			        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
		           			        ve.getValue(context.getELContext());
		           			        ve.setValue(context.getELContext(), transaction);
	           					}
	           					PopulationsManagerBase.deposit(transaction.getUserId(), success, transaction.getWriterId(), transaction);
	           				}
	            		} catch (Exception e) {
	            			log.error("Problem with population deposit event ", e);
	            		}
            		} else {
            			log.error("Invalid update request from ezeebill:" + pm);
            		}
        		} catch (Throwable t) {
                    log.error("Failed to process ezeebill Status URL response.", t);
                }
            }
            
            if (null != pm.get("flashInvSubmit")) {
				User user;
				try {
					user = ApplicationData.getUserFromSession();
					if (user.isAcceptedTerms() == false) {
						response.sendRedirect(CommonUtil.getProperty("homepage.url") + "jsp/terms.jsf?optionPlus");
						context.responseComplete();
					}
				} catch (Exception e1) {
					log.error("Can't get user", e1);
				}

                checkRequestMethod(context, session);
                String reply = null;
                try {
                    DecimalFormat numberFormat5 = new DecimalFormat("#,##0.00000");
                    if (session != null) {
                		try {
                			loginId = (Long)session.getAttribute(Constants.LOGIN_ID) == null ? 0L : (Long)session.getAttribute(Constants.LOGIN_ID);
                		} catch (Exception e) {
                			log.error("Cannot get login ID from session!", e);
                			loginId = 0L;
                		}
                    }
                    reply = SlipForm.submitFromSlip(
                            Long.parseLong((String) pm.get("oppId")),
                            Float.parseFloat((String) pm.get("amount")),
                            numberFormat5.parse((String) pm.get("slipEntryLevel")).doubleValue(),
                            Float.parseFloat((String) pm.get("slipEntryOddWin")),
                            Float.parseFloat((String) pm.get("slipEntryOddLose")),
                            Integer.parseInt((String) pm.get("choice")),
                            Integer.parseInt((String) pm.get("all")),
//			 	Integer.parseInt((String) pm.get(Constants.UTC_OFFSET)),
                            (int) CommonUtil.getUtcOffsetInMin(CommonUtil.getUtcOffset()),
                            ConstantsBase.FROM_GRAPH_FLASH,
                            null,
                            loginId);
                    reply = "reply=" + URLEncoder.encode(reply, "UTF-8"); // "ISO-8859-1"
                } catch (Exception e) {
                    log.error("Failed to process flash submit request.", e);
                }
                finishAJAXRequest(context, "application/x-www-form-urlencoded;charset=UTF-8", reply); // application/x-www-form-urlencoded;charset=ISO-8859-1
            }

            if (null != pm.get("flashSell")) {
            	// Keep the sell button press time to validate price expiration.
            	Date sellTime = new Date();

            	checkRequestMethod(context, session);
                String reply = null;
                try {
                    long invId = Long.parseLong((String) pm.get("investmentId"));
                    double price = Double.parseDouble((String) pm.get("price"));
                    //checking if user is loged in
                    if (!context.getExternalContext().isUserInRole("web")) {
                        if (log.isInfoEnabled()) {
                            log.info("Login first." +
                                    " user: " + context.getExternalContext().getRemoteUser() +
                                    " sessionId: " + session.getId());
                        }
                        // the leading "1" of this message will make sure only 1 msg show to user
                        reply = CommonUtil.getMessage("error.investment.login", null);
                    } else {
                        //get the investment
                    	User user = ApplicationData.getUserFromSession();
                        Investment investment = InvestmentsManagerBase.getInvestmentToSettle(invId, false, false, user.getId());
                        if (investment != null) { //if the investment is not settled yet
                        	invId = investment.getId();

                            long userId = investment.getUserId();
                            OptionPlusQuote quote = InvestmentsManagerBase.getLastQuote(userId, invId, price, sellTime);
                            if (quote != null) {
                            	Date timeQuoteExpired = quote.getTimeQuoteExpired();

                            	if (sellTime.before(timeQuoteExpired)){
                            		long quoteId = quote.getId();
	                            	Opportunity o = InvestmentsManager.getRunningOpportunityById(investment.getOpportunityId());
	                                if (System.currentTimeMillis() < o.getTimeFirstInvest().getTime()) {
	                                    if (log.isDebugEnabled()) {
	                                        log.debug("Opportunity not opened.");
	                                    }
	                                    reply = CommonUtil.getMessage("error.investment.notopened", null);
	                                } else if (System.currentTimeMillis() > o.getTimeLastInvest().getTime()) {
										log.debug("Opportunity expired.");
										reply = CommonUtil.getMessage("error.investment.expired", null);
									}
	                                if (null == reply) {
	                                    long priceLong = (long)(price*100);
	                                    investment.setAcceptedSms(false); // make sure no SMS is sent when sell
	                                    investment.setClosingLevel(quote.getQuoteLevel());
	                                    investment.setTimeQuoted(quote.getTimeQuoted());
	                                    if (session != null) {
	                                		try {
	                                			loginId = (Long)session.getAttribute(Constants.LOGIN_ID) == null ? 0L : (Long)session.getAttribute(Constants.LOGIN_ID);
	                                		} catch (Exception e) {
	                                			log.error("Cannot get login ID from session!", e);
	                                			loginId = 0L;
	                                		}
	                                    }
	                                    InvestmentsManager.settleInvestment(invId, 1, 0, investment, priceLong, loginId);
	                                    reply = "OK";
	                                    log.info("option plus for invId: " + invId + " sell successfully reply: OK");
	                                    InvestmentsManager.updatePurchasedQuote(quoteId);
	                                    if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
                                        	WebLevelsCache levelsCache = (WebLevelsCache) context.getExternalContext().getApplicationMap().get("levelsCache");
                                        	long totalAmount = investment.getAmount() - investment.getOptionPlusFee();
                                        	double convertAmount = totalAmount * investment.getRate();
                                    		CommonUtil.sendNotifyForInvestment(
                                    					investment.getOpportunityId(),
                                    					investment.getId(),
                                    					(-1d) * convertAmount,
                                    					(-1L) * totalAmount,
                                    					investment.getTypeId(),
                                    					investment.getMarketId(),
                                    					OpportunityType.PRODUCT_TYPE_OPTION_PLUS,
                                    					investment.getCurrentLevel(),
                                    					levelsCache,
                                    					user,
                                    					CopyOpInvTypeEnum.of(investment.getCopyOpTypeId()));
                                        }
	                                }
                            	}else{
                                	String msg = "Quote is expired, params: \n" +
				                					"User id: " + userId + "\n" +
				                					"Investment id: " + invId + "\n" +
				                					"Price: " + price + "\n" +
				                					"Quote Time: " + quote.getTimeQuoted() + "\n" +
				                					"Sell Time: " + sellTime;
				                    log.debug(msg);
				                    reply = "ERROR";
                            	}
                            } else {
                            	String msg = "Couldn't find matching quote to the followin params: \n" +
                            					"User id: " + userId + "\n" +
                            					"Investment id: " + invId + "\n" +
                            					"Price: " + price;
                                log.debug(msg);
			                    reply = "ERROR";
                            }
                        } else {
                            log.debug("investment already settled inv id: "  + invId);
                            reply = CommonUtil.getMessage("error.investment.expired", null);
                        }
                    }
                    reply = "reply=" + URLEncoder.encode(reply, "UTF-8"); // "ISO-8859-1"
                } catch (Exception e) {
                    log.error("Failed to process flash sell request.", e);
                    reply = CommonUtil.getMessage("error.investment", null);
                }
                finishAJAXRequest(context, "application/x-www-form-urlencoded;charset=UTF-8", reply); // application/x-www-form-urlencoded;charset=ISO-8859-1
            }

            if (null != pm.get("flashGetPrice")) {
                checkRequestMethod(context, session);
                String reply = null;
                double price = 0;
                try {
                    long invId = Long.parseLong((String) pm.get("investmentId"));
                    User user = ApplicationData.getUserFromSession();
                    Investment investment = InvestmentsManagerBase.getInvestmentToSettle(invId, false, false, user.getId());
                    if (null != investment) {
                    	HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
						OpportunityCache oc = (OpportunityCache) request.getServletContext().getAttribute(Constants.OPPORTUNITY_CACHE);
						OpportunityCacheBean opp = oc.getOpportunity(investment.getOpportunityId());
						Market market = com.anyoption.common.managers.MarketsManagerBase.getMarket(investment.getMarketId());
                        if (null != opp) {

                            WebLevelsCache levelsCache = (WebLevelsCache) context.getExternalContext().getApplicationMap().get("levelsCache");
                            WebLevelLookupBean wllb = new WebLevelLookupBean();
                            wllb.setOppId(opp.getId());
                            wllb.setFeedName(market.getFeedName());
                            wllb.setSkinGroup(ApplicationData.getSkinById(user.getSkinId()).getSkinGroup());
                            levelsCache.getLevels(wllb);
                            double promil = 0;
                            BigDecimal etLevel = new BigDecimal(String.valueOf(wllb.getDevCheckLevel()));
                            etLevel = etLevel.round(new MathContext(etLevel.precision() - etLevel.scale() + Integer.parseInt(String.valueOf(market.getDecimalPoint())), RoundingMode.HALF_UP));
                            log.debug("etrader level from service = " + wllb.getDevCheckLevel() + " after round = " + etLevel.doubleValue());
                            if (etLevel.doubleValue() != 0) { //if level from servies != 0
	                            if (investment.getTypeId() == 1) {
	                                promil = etLevel.doubleValue() - investment.getCurrentLevel();
	                            } else {
	                                promil = investment.getCurrentLevel() - etLevel.doubleValue();
	                            }
	                            promil = promil / investment.getCurrentLevel();
	                            promil = promil * 1000;
	                            Date timeQuoted = new Date();
	                            double minPass = (timeQuoted.getTime() - opp.getTimeFirstInvest().getTime()) / 60000;
	                            if (minPass >= 0 && minPass < 55) {
	                                log.debug("etrader level after round: " + etLevel.doubleValue() + " inv level: " + investment.getCurrentLevel() + " promil = " + promil + " minutes pass = " + minPass);
	                                BigDecimal invAmount = (new BigDecimal(investment.getAmount()).subtract(new BigDecimal(String.valueOf(investment.getOptionPlusFee())))).divide(new BigDecimal(String.valueOf(100)));
	                                price = InvestmentsManager.calcOptionsPlusePrice(promil, minPass, opp.getMarketId(), invId, timeQuoted, invAmount, etLevel.doubleValue());
                                    reply = String.valueOf(price);
	                                session.setAttribute(invId + "_price", price);
	                                session.setAttribute(invId + "_date", timeQuoted);
	                                session.setAttribute(invId + "_level", etLevel.doubleValue());
	                            } else {
	                                log.debug("min pass error: " + minPass);
	                                reply = CommonUtil.getMessage("error.investment", null);
	                            }
                            } else { //level from service = 0 return error
                        	  	log.debug("level from service is equel to 0! level: " + etLevel.doubleValue());
                        	  	reply = CommonUtil.getMessage("error.investment", null);
                            }
                        } else {
                            log.debug("opp not found maybe settled opp id: " + investment.getOpportunityId());
                            reply = CommonUtil.getMessage("error.investment", null);
                        }
                    } else {
                        log.debug("investment not found maybe settled investment id: " + invId);
                        reply = CommonUtil.getMessage("error.investment", null);
                    }
                    reply = "reply=" + URLEncoder.encode(reply, "UTF-8"); // "ISO-8859-1"
                } catch (Exception e) {
                    log.error("Failed to process flash sell request.", e);
                }
                finishAJAXRequest(context, "application/x-www-form-urlencoded;charset=UTF-8", reply); // application/x-www-form-urlencoded;charset=ISO-8859-1
            }

            if (null != pm.get("flashBinary0100Submit")) {
                checkRequestMethod(context, session);
                String reply = null;
                try {
                	if (session != null) {
                		try {
                			loginId = (Long)session.getAttribute(Constants.LOGIN_ID) == null ? 0L : (Long)session.getAttribute(Constants.LOGIN_ID);
                		} catch (Exception e) {
                			log.error("Cannot get login ID from session!", e);
                			loginId = 0L;
                		}
                	}
                	long oppId = Long.parseLong((String) pm.get("oppId"));
                	long contractType = Long.parseLong((String) pm.get("contractType"));
	                double quote = Double.parseDouble((String) pm.get("quote"));
	                int count = Integer.parseInt((String) pm.get("count"));
	                int utcOffset = Integer.parseInt((String) pm.get("utcOffset"));
	                double currentLevel = Double.parseDouble(((String) pm.get("currentLevel")).replace(",", ""));

	                reply = SlipForm.submitBinary0100(oppId, contractType, quote, count, utcOffset, currentLevel, loginId);
                } catch (Exception e) {
                	log.error("Failed to process insert investment 0-100 reply 02.", e);
                	//reply = "02" + CommonUtil.getMessage("binary0100.mainarea.transaction.failure", null);
                	reply = CommonUtil.getMessage("binary0100.mainarea.transaction.failure", null);
                }
                try {
                    reply = "reply=" + URLEncoder.encode(reply, "UTF-8");
                } catch (Exception e) {
                	log.error("Failed to process insert investment 0-100.", e);
				}
                finishAJAXRequest(context, "application/x-www-form-urlencoded;charset=UTF-8", reply); // application/x-www-form-urlencoded;charset=ISO-8859-1
            }

            if (null != pm.get("turnSmsOn") || null != pm.get("turnSmsOnFlash")) {   // update isAcceptedSms field for getting sms expiration message
            	String reply = "ok";
            	try {
            		long invId = Long.parseLong((String) pm.get("invId"));
					InvestmentsManager.updateAcceptedSmsById(invId);
					// update user get sms flag
					User user = ApplicationData.getUserFromSession();
					if (!user.isContactBySMS()) {
						user.setIsContactBySMS(1);
                        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
                        ve.getValue(context.getELContext());
                        ve.setValue(context.getELContext(), user);
//						context.getApplication().createValueBinding(Constants.BIND_USER).setValue(context, user);
						UsersManager.updateFreeSms(user.getId(), 1);
					}
				} catch (Exception e) {
					reply = "notOk";
					log.error("Failed to process isAcceptedSms update submit request.", e);
				}
				if (null != pm.get("turnSmsOnFlash")) {
				    reply = "reply=" + reply;
				}
				finishAJAXRequest(context, "application/x-www-form-urlencoded", reply);
            }

            if (requestURL.indexOf("threeDSecureResult") != -1) {
                ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
                Transaction t = (Transaction) ve.getValue(context.getELContext());
            		if( t != null ){
            			Transaction reload;
						try {
							reload = TransactionsManagerBase.getTransaction(t.getId());
							if(reload.getStatusId() != TransactionsManagerBase.TRANS_STATUS_PENDING && reload.getStatusId() != TransactionsManagerBase.TRANS_STATUS_SUCCEED) {
								FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.3d", null), null);
								context.addMessage(null, fm);
							}
						} catch (SQLException e) {
							log.error("Unable to finish 3d transactions", e);
						}
            			
            		} else {
            			log.error("Empty transactions for 3d finish");
            		}
            }

            if ((null != pm.get("investmentId")) && context.getViewRoot().getViewId().indexOf("invSuccessPrint") != -1 ) {
                log.info("Investment success print.");
                try {
                	SlipForm sf = new SlipForm();
                	if(context.getViewRoot().getViewId().indexOf("invSuccessPrintOneTouch") != -1){
                		sf = (SlipForm) context.getApplication().createValueBinding("#{slipForm1T}").getValue(context);
                	}else{
                		sf = (SlipForm) context.getApplication().createValueBinding("#{slipForm}").getValue(context);
                	}
                    sf.setEntryToPrint(Long.parseLong((String) pm.get("investmentId")));
                } catch (Throwable t) {
                    log.error("Failed to prepare investment success print.", t);
                }
            }

            if ((null != pm.get("txid") && null != pm.get("cs")) && requestURL.indexOf("inatecDepositResult") != -1) {
                log.info("Inatec bank site response.");
                try {
                    String txId = (String) pm.get("txid");
                    String cs = (String) pm.get("cs");
                    ApplicationData ad = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
                    ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
                    Transaction t = (Transaction) ve.getValue(context.getELContext());
                    if (TransactionsManager.isInatecNotNotifiedTrx(t.getId())) {
                    	if (session != null) {
                    		try {
                    			loginId = (Long)session.getAttribute(Constants.LOGIN_ID) == null ? 0L : (Long)session.getAttribute(Constants.LOGIN_ID);
                    		} catch (Exception e) {
                    			log.error("Cannot get login ID from session!", e);
                    			loginId = 0L;
                    		}
                    	}
	                    TransactionsManagerBase.finishInatecTransaction(t, txId, cs, CommonUtil.getWebWriterId(), ApplicationData.getUserFromSession().getUtcOffset(),true, loginId);
	                    ve.setValue(context.getELContext(), t);
	                    User user = ApplicationData.getUserFromSession();
	                    TransactionsManager.afterDepositAction(user, t, TransactionSource.TRANS_FROM_WEB);
	                    if (t.getStatusId() != TransactionsManagerBase.TRANS_STATUS_SUCCEED &&
	                    		t.getStatusId() != TransactionsManagerBase.TRANS_STATUS_IN_PROGRESS) {
	                        DepositForm df = (DepositForm) context.getApplication().createValueBinding(Constants.BIND_DEPOSIT_FORM).getValue(context);
	                        df.setDeposit(CommonUtil.displayAmountForInput(t.getAmount(), false, 0));
	                        df.setPaymentTypeId(t.getPaymentTypeId());
	                        df.setAccountNum(t.getInatecAccountNum());
	                        df.setBankSortCode(t.getInatecBankSortCode());
	                        df.setBeneficiaryName(t.getBeneficiaryName());
	                    }
                    } else {
                    	log.info("Inatec Transaction " + t.getId() + ", allready notified!");
                    }
                } catch (Throwable t) {
                    log.error("Failed to process Inatec bank response.", t);
                }
           }
			if (requestURL.indexOf("APILogin.jsf") > -1 && null != pm.get("login_token")) {
				String pageParamToRedirect =  Constants.EMPTY_STRING;
				boolean redirectToHP = true;
				try {
					HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
					User sessionUser = (User)request.getSession().getAttribute(Constants.BIND_SESSION_USER);
					if (sessionUser == null) {
						String loginToken = (String)pm.get("login_token");
						pageParamToRedirect = (String)pm.get("page");
						long userId = Long.valueOf((String)pm.get("user_id"));
						log.info("Login token from request:" + loginToken + " userId: " + userId +  " page:" + pageParamToRedirect);
						if (!CommonUtil.isParameterEmptyOrNull(loginToken)) {
							com.anyoption.common.beans.base.User user = UsersManagerBase.getUserByLoginToken(loginToken, userId);
							if (user != null) {
								log.debug("APILogin user was found userName:" + user.getUserName());
								LoginForm loginForm = new LoginForm();
								session.setAttribute(Constants.SKIN_ID, String.valueOf(Skin.SKIN_REG_EN));
								session.setAttribute(Constants.SESSION_SKIN, Skin.SKIN_REG_EN);
								loginForm.setUsername(user.getUserName());
								loginForm.setPassword(user.getPassword());
								loginForm.loginAPI("APILogin", pageParamToRedirect);
								if (context.getMessageList().isEmpty()) {
									redirectToHP = false;
								}
							}
						}
					}
				} catch (Exception e) {
					redirectToHP = true;
					log.error("Error while trying to get login token", e);
				}
				//redirect to HP
				if (redirectToHP) {
					ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
					String redirect = ap.getHomePageUrl();
						try {
						response.sendRedirect(redirect);
					} catch (IOException e) {
						log.error("Error while trying to redirect from login token", e);
					}
				}
			}

            //deltaPay
            if (null != pm.get("status") && null != requestURL && requestURL.indexOf("deltaPayStatusURL") != -1) {
                log.debug("DeltaPay Response : \n" + pm);
                String tranIdString = (String) pm.get("order_id"); //our transactionId
                log.debug("The deltaPay order_id is: " + tranIdString);
                long tranIdLong = 0;
                Transaction tran = null;
                if (!CommonUtil.isParameterEmptyOrNull(tranIdString)) {
                	try {
                		tranIdLong = Long.valueOf(tranIdString);
                	} catch (Exception e) {
                		log.error("Can't parse the tranId from string " + tranIdString + " to long \n" + e);
					}
                	try {
						tran = TransactionsManagerBase.getTransaction(tranIdLong);
					} catch (SQLException e) {
						log.error("Cant get transaction id from DB \n" + e);
					}
                } else {
                	log.error("TransactionId is empty from deltaPay response");
                }

                if (null != tran && (tran.getClearingProviderId() == ClearingManager.DELTAPAY_CHINA_PROVIDER_ID || tran.getClearingProviderId() == ClearingManager.DELTAPAY_CHINA_TEST_PROVIDER_ID )) {
     	            DeltaPayInfo delta = (DeltaPayInfo) context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_DELTAPAY_INFO, DeltaPayInfo.class).getValue(context.getELContext());
     	            try {
     	            	UserBase user = ApplicationData.getUserFromSession();
     	            	if (session != null) {
							try {
								loginId = (Long) session.getAttribute(Constants.LOGIN_ID) == null ? 0L : (Long) session.getAttribute(Constants.LOGIN_ID);
							} catch (Exception e) {
								log.error("Cannot get login ID from session!", e);
								loginId = 0L;
							}
     	            	}
     	            	tran = TransactionsManagerBase.updateDeltaPayTransaction(pm, delta.getErrorMessageFromRequset(), tran, user, delta,
     	            			Writer.WRITER_ID_WEB, delta.isFirstDeposit(), delta.getFullAmountStr(), ConstantsBase.WEB_CONST, loginId);
     	            	if (null != tran) {
     	            		log.debug("Going to save transactionId: " + tran.getId() + " with status:"+tran.getStatusId()+" in the session\n ");
                            ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
                            ve.getValue(context.getELContext());
                			ve.setValue(context.getELContext(), tran);
     	            	}
     	            } catch (Exception e) {
						log.error("Cant finish deposit transaction after china provider (DeltaPay) error \n " + e);
					}
                } else if (null != tran) {
	                log.error("Failed to process DeltaPay, Got response from deltaPay but transaction with diffrent Provider:\n" + tran);
                } else {
                	log.error("Can't find transaction from deltaPay Response ");
                }
                context.responseComplete();
            }

            if (null != pm.get("status") && null != requestURL && requestURL.indexOf("deltaPayStatusResponse") != -1) {
                log.debug("Server to Server notification - DeltaPay Response : \n" + pm);
                String tranIdString = (String) pm.get("order_id"); //our transactionId
                log.debug("The deltaPay order_id is: " + tranIdString);
                long tranIdLong = 0;
                Transaction tran = null;
                if (!CommonUtil.isParameterEmptyOrNull(tranIdString)) {
                	try {
                		tranIdLong = Long.valueOf(tranIdString);
                	} catch (Exception e) {
                		log.error("Can't parse the tranId from string " + tranIdString + " to long \n" + e);
					}
                	try {
						tran = TransactionsManagerBase.getTransaction(tranIdLong);
					} catch (SQLException e) {
						log.error("Cant get transaction id from DB \n" + e);
					}
                } else {
                	log.error("TransactionId is empty from deltaPay response");
                }

                if (null != tran && (tran.getClearingProviderId() == ClearingManager.DELTAPAY_CHINA_PROVIDER_ID || tran.getClearingProviderId() == ClearingManager.DELTAPAY_CHINA_TEST_PROVIDER_ID )) {
     	            DeltaPayInfo delta = (DeltaPayInfo) context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_DELTAPAY_INFO, DeltaPayInfo.class).getValue(context.getELContext());
     	            try {
     	            	UserBase user = UsersManager.getUserById(tran.getUserId());
     	            	if (session != null) {
							try {
								loginId = (Long) session.getAttribute(Constants.LOGIN_ID) == null ? 0L : (Long) session.getAttribute(Constants.LOGIN_ID);
							} catch (Exception e) {
								log.error("Cannot get login ID from session!", e);
								loginId = 0L;
							}
     	            	}
     	            	tran = TransactionsManagerBase.updateDeltaPayTransaction(pm, delta.getErrorMessageFromRequset(), tran, user, delta,
     	            			(int)tran.getWriterId(), delta.isFirstDeposit(), delta.getFullAmountStr(), ConstantsBase.WEB_CONST, loginId);
     	            	if (null != tran) {
     	            		log.debug("Going to save transactionId: " + tran.getId() + " with status:"+tran.getStatusId()+" in the session\n ");
                            ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
                            ve.getValue(context.getELContext());
                			ve.setValue(context.getELContext(), tran);
     	            	}
     	            } catch (Exception e) {
						log.error("Cant finish deposit transaction after china provider (DeltaPay) error \n " + e);
					}
                } else if (null != tran) {
	                log.error("Failed to process DeltaPay, Got response from deltaPay but transaction with diffrent Provider:\n" + tran);
                } else {
                	log.error("Can't find transaction from deltaPay Response ");
                }
                context.responseComplete();
            }

            if (null != pm.get("status") && null != requestURL && requestURL.indexOf("deltaPayStatusWithdrawResponse") != -1) {
                log.debug("Server to Server notification - DeltaPay Withdraw Response : \n" + pm);

            }


            //Moneybookers status URL
            if (null != pm.get("pay_from_email")&& null != pm.get("transaction_id") && context.getViewRoot().getViewId().indexOf("moneybookersStatusURL.xhtml") != -1) {
            	log.info("Going to check transaction status from Moneybookers");
            	try {
            		if (session != null) {
            			try {
            				loginId = (Long)session.getAttribute(Constants.LOGIN_ID) == null ? 0L : (Long)session.getAttribute(Constants.LOGIN_ID);
            			} catch (Exception e) {
            				log.error("Cannot get login ID from session!", e);
            				loginId = 0L;
            			}
            		}
            		TransactionsManagerBase.updateMoneybookerTransactionStatus(pm, loginId);
        		} catch (Throwable t) {
                    log.error("Failed to process Moneybooker Status URL response.", t);
                }
                response.setStatus(HttpServletResponse.SC_OK);
                context.responseComplete();
            }

            //MoneyBookers cancel URL
            if (null != context && null != context.getViewRoot() && null != context.getViewRoot().getViewId() && context.getViewRoot().getViewId().indexOf("moneybookersCancelURL.xhtml") != -1) {
	    		log.info("Going to cancel Moneybookers transaction");
	    		try {
		            ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
		            Transaction tran = (Transaction) ve.getValue(context.getELContext());
		            TransactionsManagerBase.moneybookersCancelTransaction(tran, CommonUtil.getWebWriterId());
	            } catch (Throwable t) {
                    log.error("Failed to cancel Moneybookers transaction." , t);
                }
            }

            //Moneybookers return URL
            if (null != pm.get("msid")&& null != pm.get("transaction_id") && context.getViewRoot().getViewId().indexOf("moneybookersReturnURL.xhtml") != -1) {
            	log.info("User returned from Moneybookers site - (returnURL)");
            	try {
            		String msid = (String) pm.get("msid");
            		String transaction_id = (String) pm.get("transaction_id");
            		Transaction tran = TransactionsManagerBase.moneybookersReturnURL(transaction_id, msid);
            		if (null != tran){
                        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
                        ve.getValue(context.getELContext());
            			ve.setValue(context.getELContext(), tran);
            		}
            	} catch (Throwable t) {
                    log.error("Failed to process Moneybooker return URL response.", t);
                }
            }

            //WebMoney Result URL
//            if (null != pm.get("LMI_PAYMENT_NO") && null != requestURL && requestURL.indexOf("wmResult") != -1) {
//            	log.info("Going to check Result URL from WebMoney");
//            	try {
//            		if (session != null) {
//            			try {
//            				loginId = (Long)session.getAttribute(Constants.LOGIN_ID) == null ? 0L : (Long)session.getAttribute(Constants.LOGIN_ID);
//            			} catch (Exception e) {
//            				log.error("Cannot get login ID from session!", e);
//            				loginId = 0L;
//            			}
//            		}
//            		TransactionsManagerBase.updateWebMoneyTransactionStatus(pm, loginId);
//        		} catch (Throwable t) {
//                    log.error("Failed to process WebMoney Result URL response.", t);
//                }
//        		context.responseComplete();
//            }

            //WebMoney Success URL
//            if (null != pm.get("LMI_PAYMENT_NO") && null != requestURL && requestURL.indexOf("wmSuccess") != -1) {
//            	log.info("Going to check success URL from WebMoney");
//            	try {
//            		String transactionId = (String) pm.get("LMI_PAYMENT_NO");
//            		if (null != transactionId && !transactionId.isEmpty()) {
//            			Transaction tran = TransactionsManagerBase.webMoneySuccessURL(transactionId);
//                		if (null != tran){
//                            ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
//                            ve.getValue(context.getELContext());
//                			ve.setValue(context.getELContext(), tran);
//                		}
//            		} else {
//            			log.warn("Warning transactionId from WebMoney is empty or null, LMI_PAYMENT_NO=" + transactionId);
//            		}
//            	} catch (Throwable t) {
//                    log.error("Failed to process success URL response from WebMoney.", t);
//                }
//            	context.responseComplete();
//            }

            //WebMoney Fail URL
            if (null != pm.get("LMI_PAYMENT_NO") && null != requestURL && requestURL.indexOf("wmFail") != -1) {
            	log.info("Going to check Fail URL from WebMoney");
            	try {
            		String transactionId = (String) pm.get("LMI_PAYMENT_NO");
            		log.debug("Going to set transaction status to fail tranId=" + transactionId);
            		if (null != transactionId && !transactionId.isEmpty()) {
            			TransactionsManagerBase.updateWebMoneyTransactionFailURL(Long.valueOf(transactionId));
            		} else {
            			log.warn("Warning transactionId from WebMoney is empty or null, LMI_PAYMENT_NO=" + transactionId);
            		}
        		} catch (Throwable t) {
                    log.error("Failed to process WebMoney Fail URL response." + t.getStackTrace());
                }
        		context.responseComplete();
            }

//            //BaroPay return URL
//            if (null != context && null != context.getViewRoot() && null != context.getViewRoot().getViewId() && context.getViewRoot().getViewId().indexOf("baroPayStatusURL.xhtml") != -1) {
//                HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
//                String responseXML = null;
//                try {
//            	    InputStream is = request.getInputStream();
//                    InputStreamReader isr = new InputStreamReader(is, "ISO-8859-8");
//                    BufferedReader br = new BufferedReader(isr);
//                    StringBuffer xmlResp = new StringBuffer();
//                    String line = null;
//                    while ((line = br.readLine()) != null) {
//                 	   xmlResp.append(line);
//                    }
//                    responseXML = xmlResp.toString();
//                    log.log(Level.DEBUG, "Response received.");
//                    log.log(Level.DEBUG, "Response from BaroPay: " + responseXML);
//                } catch (Exception e) {
//                	log.error("Failed to process BaroPay response." + e);
//                }
//                log.info("Going to check Result URL from BaroPay.");
//                try {
//                	if (session != null) {
//                		try {
//                			loginId = (Long)session.getAttribute(Constants.LOGIN_ID) == null ? 0L : (Long)session.getAttribute(Constants.LOGIN_ID);
//                		} catch (Exception e) {
//                			log.error("Cannot get login ID from session!", e);
//                			loginId = 0L;
//                		}
//                	}
//            	    TransactionsManagerBase.updateBaroPayTransactionStatus(responseXML, loginId);
//                } catch (Throwable t) {
//                	log.error("Failed to process BaroPay Result URL response.", t);
//                }
//                context.responseComplete();
//           }

          //BaroPay Withdraw return URL
//            if (null != context && null != context.getViewRoot() && null != context.getViewRoot().getViewId() && context.getViewRoot().getViewId().indexOf("baroPayWithdrawStatusURL.xhtml") != -1) {
//                HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
//                String responseXML = null;
//                try {
//            	    InputStream is = request.getInputStream();
//                    InputStreamReader isr = new InputStreamReader(is, "ISO-8859-8");
//                    BufferedReader br = new BufferedReader(isr);
//                    StringBuffer xmlResp = new StringBuffer();
//                    String line = null;
//                    while ((line = br.readLine()) != null) {
//                 	   xmlResp.append(line);
//                    }
//                    responseXML = xmlResp.toString();
//                    log.log(Level.DEBUG, "Response received.");
//                    log.log(Level.DEBUG, "Response from BaroPay: " + responseXML);
//                } catch (Exception e) {
//                	log.error("Failed to process BaroPay response." + e);
//                }
//                context.responseComplete();
//           }
//
//           if (null != pm.get("token") && null != pm.get("PayerID") && context.getViewRoot().getViewId().indexOf("payPalSetExpressCheckoutReturn") != -1) {
//        	   log.info("PayPal setExpressCheckout depositReturn response.");
//        	   try {
//        		   String token = (String) pm.get("token");
//        		   String payerId = (String) pm.get("PayerID");
//	               ApplicationData ad = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
//                   ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
//                   Transaction t = (Transaction) ve.getValue(context.getELContext());
//	               User user = ApplicationData.getUserFromSession();
//					if (session != null) {
//						try {
//							loginId = (Long) session.getAttribute(Constants.LOGIN_ID) == null ? 0L : (Long) session.getAttribute(Constants.LOGIN_ID);
//						} catch (Exception e) {
//							log.error("Cannot get login ID from session!", e);
//							loginId = 0L;
//						}
//					}
//	               TransactionsManagerBase.finishPayPalTransaction(t, CommonUtil.getWebWriterId(), user.getUtcOffset(), token, payerId, user.getCurrency().getNameKey(), loginId);
//	               ve.setValue(context.getELContext(), t);
//                   TransactionsManager.afterDepositAction(user, t, TransactionSource.TRANS_FROM_WEB);
//                   if (t.getStatusId() == TransactionsManagerBase.TRANS_STATUS_FAILED) {
//                	   DepositForm df = (DepositForm) context.getApplication().createValueBinding(Constants.BIND_DEPOSIT_FORM).getValue(context);
//                       df.setDeposit(CommonUtil.displayAmountForInput(t.getAmount(), false, 0));
//                   }
//        	   } catch (Throwable t) {
//                   log.error("Failed to process payPalSetExpressCheckoutReturn response.", t);
//               }
//           }

           if (null != context && null != context.getViewRoot() && null != context.getViewRoot().getViewId() && context.getViewRoot().getViewId().indexOf("payPalSetExpressCheckoutCancel") != -1) {
        	   log.info("PayPal setExpressCheckout depositCancel response.");
        	   try {
	        	   Transaction t = (Transaction) context.getApplication().createValueBinding(Constants.BIND_TRANSACTION).getValue(context);
	        	   TransactionsManagerBase.PayPalExpressCheckoutCancel(t, CommonUtil.getWebWriterId(), ApplicationData.getUserFromSession().getUtcOffset());
	        	   DepositForm df = (DepositForm) context.getApplication().createValueBinding(Constants.BIND_DEPOSIT_FORM).getValue(context);
	               df.setDeposit(CommonUtil.displayAmountForInput(t.getAmount(), false, 0));
        	   } catch (Throwable t) {
                   log.error("Failed to process payPalSetExpressCheckoutCancel response.", t);
               }
           }

  		   if (null != pm.get("pointsToConvert")) {
				log.debug("handle calculate amount by Loyalty points");
				response.setContentType("text/xml");
				response.setCharacterEncoding("utf-8");
				String out = "";
				try {
					User u = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
					long points = Long.parseLong((String)pm.get("pointsToConvert"));
					long tierId = u.getTierUser().getTierId();
					double amount = TiersManagerBase.convertPointsToCash(points, tierId);
					out = CommonUtil.displayAmount(CommonUtil.calcAmount(amount), true, u.getCurrencyId());
					response.getWriter().write("<convertedAmount>" + out + "</convertedAmount>");
				} catch (Exception e) {
					log.log(Level.ERROR, "Can't write reply for amount by points request!!.", e);
				}
				context.responseComplete();
			}

//          if (null != context.getViewRoot() && null != context.getViewRoot().getViewId() &&
//        		  context.getViewRoot().getViewId().indexOf("envoyNotify") != -1) {
//          if (null != requestURL && requestURL.indexOf("envoyNotify") != -1) {
//               log.warn("Envoy bank site get Pay-In Notification response!");
//               try {
//            	   log.info("asasas. ip is: " + ip);
//            	   // Check notification source IP
//            	   if (ProvidersManager.isAllowProviderIPs(ProvidersIP.PROVIDER_GROUP_ENVOY, ip) 
//            			   || GeneralManager.getAllowIPList().contains(ip)) {
//            		   log.info("Getting request: ");
//            		   HttpServletRequest notificationRequest = (HttpServletRequest) context.getExternalContext().getRequest();
//	            	   InputStream inputStream = notificationRequest.getInputStream();
//	            	   InputStreamReader isr = new InputStreamReader(inputStream, "UTF-8");
//	            	   BufferedReader br = new BufferedReader(isr);
//	                   StringBuffer xmlReq = new StringBuffer();
//	                   String line = null;
//	                   while ((line = br.readLine()) != null) {
//	                	   xmlReq.append(line);
//	                   }
//	                   String requestXmlString = xmlReq.toString();
//
//	                   log.info("Response received. " + requestXmlString);
//
//	                   log.info("Sending SUCCESS: ");
//		       	       // Send success respond (for getting the notification)
//		               response.setContentType("text/xml");
//		       		   response.setCharacterEncoding("UTF-8");
//
//		       		   try {
//		       			   response.getWriter().write(ConstantsBase.ENVOY_NOTIFY_SUCCESS);
//		       		   } catch (Exception e) {
//		       			   log.log(Level.ERROR,"Error! Can't write reply!!.", e);
//		       		   }
//		       		   log.info("write SUCCESS: ");
//
//		       		   context.responseComplete();
//		       		   log.info("responseComplete: ");
//
//			       		if (session != null) {
//			       			try {
//			       				loginId = (Long)session.getAttribute(Constants.LOGIN_ID) == null ? 0L : (Long)session.getAttribute(Constants.LOGIN_ID);
//			       			} catch (Exception e) {
//			       				log.error("Cannot get login ID from session!", e);
//			       				loginId = 0L;
//			       			}
//			       		}
//	            	   ClearingManager.handleNotifications(requestXmlString, loginId);
//
//            	   } else {
//            		   log.error("Error: ip " + ip + "doesn't belong to Envoy or to allow ip list");
//            	   }
//	           } catch (Throwable t) {
//	               log.error("Error: Failed to process Envoy Notifications.", t);
//	           }
//          }
//          if (requestURL.indexOf("envoyOnlineDepositResult") != -1) {
//        	  // Return from Envoy 1 Click app.
//               log.warn("Envoy bank site get Deposit Result!");
//               String message = null;
//               try {
//            	   User user = context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER, User.class);
//            	   Transaction tran = context.getApplication().evaluateExpressionGet(context, Constants.BIND_TRANSACTION, Transaction.class);
//            	   TransactionsManager.afterDepositAction(user, tran, TransactionSource.TRANS_FROM_WEB);
//
//            	   String comments = null;
//            	   String status = (String)pm.get("status");
//
//            	   if (status.equals(ConstantsBase.ENVOY_NOTIFY_SUCCESS)){
//            		   message = CommonUtil.getMessage("envoy.after.success.deposit", null);
//
//                   } else if (status.equals(ConstantsBase.ENVOY_NOTIFY_ERROR)){
//                	   message = (String)pm.get("customerMessage");
//
//                   } else if (status.equals(ConstantsBase.ENVOY_NOTIFY_CANCELLED)){
//            		   log.debug("Transaction canceled : Redirect to envoyDeposit");
//	   				   response.sendRedirect(context.getExternalContext().getRequestContextPath() + "/jsp/pages/envoyDepositContent.jsf");
//	   				   context.responseComplete();
//
//                   } else if (status.equals(ConstantsBase.ENVOY_NOTIFY_OPEN)){
//                	   message = CommonUtil.getMessage("envoy.after.open.deposit", null);
//
//            	   } else if (status.equals(ConstantsBase.ENVOY_NOTIFY_EXPIRED)){
//            		   message = CommonUtil.getMessage("envoy.after.expire.deposit", null);
//
//            	   } else if (status.equals(ConstantsBase.ENVOY_NOTIFY_FAILURE)){
//            		   message = CommonUtil.getMessage("envoy.after.fail.deposit", null);
//            	   } else {
//            		   message = CommonUtil.getMessage("envoy.after.fail.deposit", null);
//            		   comments = "Error status " + status + " is not valid.";
//                	   log.error(comments);
//                   }
//	           } catch (Throwable t) {
//	               log.error("Failed to process Envoy Pay-In Notifications.", t);
//	           }
//
//	           DepositForm df = (DepositForm) context.getApplication().createValueBinding(Constants.BIND_DEPOSIT_FORM).getValue(context);
//		 	   df.setEnvoyResultMsg(message);
//          }
//
//          if (requestURL.indexOf("envoyBankingDepositResult") != -1) {
//              log.warn("Envoy bank site get Deposit Result! Fast Bank Transfer");
//	          try {
//					log.debug("Redirect to envoyDeposit");
////					HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
//					response.sendRedirect(context.getExternalContext().getRequestContextPath() + "/jsp/pages/envoyDepositContent.jsf");
//					context.responseComplete();
//				} catch (IOException e) {
//					log.debug("Redirect to envoyDepositMessage failde.");
//				}
//         }
//
//         if (requestURL.indexOf("envoyDepositForward") != -1) {
//            log.warn("Envoy Specific Method was chosen.");
//
//			String paymentMethod = (String)pm.get("method");
//
//			if (!CommonUtil.isParameterEmptyOrNull(paymentMethod)){
//				log.debug("Redirect to envoy method " + paymentMethod);
//				DepositForm df = (DepositForm) context.getApplication().createValueBinding(Constants.BIND_DEPOSIT_FORM).getValue(context);
//
//				try{
//					df.setEnvoyInfoForDeposit(paymentMethod);
//				}catch (Exception e) {
//					log.error("can't redirect to envoy method " + paymentMethod + " "+ e);
//				}
//			}
//
//         }

		if ( null != pm.get("replaceMarket")) {  // handel market change from indexBanner requests
			log.debug("handle indexBanner market replace request.");
			response.setContentType("text/xml");
			response.setCharacterEncoding("utf-8");
			Long marketIdPar = Long.valueOf((String)pm.get("replaceMarket"));
			try {
				ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
				HashMap<Long, LandingMarketDetails> lMarketsList = ApplicationData.getMarketsClosingLevels();
				Long nextMarketId = lMarketsList.get(marketIdPar).getNextMarketId();
				LandingMarketDetails md = lMarketsList.get(nextMarketId);//applicationData.userLocale.language}_#{applicationData.skinId}
				String imagePath = context.getExternalContext().getRequestContextPath() +
									"/images/ao/" +
									ap.getUserLocale().getLanguage() +
									"/indexBanner/"  +
									md.getMarketImageName();
				response.getWriter().write("<res>" + CommonUtil.formatLevelByMarket(md.getClosingLevel().doubleValue(), md.getMarketId())
						+ "|" + md.getMarketId() + "|" + md.getMarketName() + "|" + imagePath + "</res>");
			} catch (Exception e) {
				log.log(Level.ERROR, "Can't write reply for indexBanner market replace request, currMarket: " + marketIdPar, e);
			}
			context.responseComplete();
		}

		if (null != pm.get("changeSkinDuid")) {  // handle skin change from mobile
			log.debug("handle skinChange from mobileApp");
			response.setContentType("text/xml");
			response.setCharacterEncoding("utf-8");
			long skinId = Long.parseLong((String)pm.get("changeSkinDuid"));
			try {
				String duid = (String) session.getAttribute(Constants.DEVICE_UNIQUE_ID);
				if (null != duid) {
					DeviceUniqueIdsSkinManager.update(duid, skinId);
					response.getWriter().write("<res>" + skinId + "</res>");
				} else {
					log.error("Error! changeSkinId duid is null! not saved in DB");
					response.getWriter().write("<res>" + skinId + "</res>");
				}
			} catch (Exception e) {
				log.log(Level.ERROR, "Can't write reply for changeSkin request, skinId: " + skinId, e);
			}
			context.responseComplete();
		}

		// handle Register attempts
        if (!CommonUtil.isParameterEmptyOrNull((String)pm.get("emailRegisterAttempts")) || null != pm.get("landLinePhoneRegisterAttempts") || null != pm.get("mobilePhoneRegisterAttempts") || null != pm.get("quickStartEmailRegisterAttempts") ) {
        	log.debug("Handle register attempts");
            response.setContentType("text/xml");
            response.setCharacterEncoding("utf-8");
            String email = "";
            boolean isQuickStart = false;
            if(pm.get("quickStartEmailRegisterAttempts") != null){
            	email = (String)pm.get("quickStartEmailRegisterAttempts");
            	isQuickStart = true;
            } else {
            	email = (String)pm.get("emailRegisterAttempts");
            }
        	String firstName = (String)pm.get("firstNameRegisterAttempts");
        	String lastName = (String)pm.get("lastNameRegisterAttempts");
        	String countryId = (String)pm.get("countryRegisterAttempts");
        	String mobilePhone = (String)pm.get("mobilePhoneRegisterAttempts");
        	String landLinePhone = (String)pm.get("landLinePhoneRegisterAttempts");
        	boolean contactEmailRegisterAttempts = Boolean.parseBoolean((String)pm.get("contactEmailRegisterAttempts"));
        	boolean contactSMSRegisterAttempts = Boolean.parseBoolean((String)pm.get("contactSMSRegisterAttempts"));
        	boolean isShortForm = Boolean.parseBoolean((String)pm.get("isShortForm"));

        	if(CommonUtil.isParameterEmptyOrNull(email)
        			&& CommonUtil.isParameterEmptyOrNull(mobilePhone)
        				&& CommonUtil.isParameterEmptyOrNull(landLinePhone)){
        		log.debug("!!!Send ajax call insertRegAttempt with empty: EMAIL and PHONE!!!");
        	} else {
            	il.co.etrader.web.bl_managers.ContactsManager.insertRegAttempt(email, firstName, lastName, countryId, mobilePhone, landLinePhone,
            			contactEmailRegisterAttempts, contactSMSRegisterAttempts, isShortForm, session, true, isQuickStart);
        	}

			   //Insert Marketing Tracking

//		        try {
//					boolean insertMarketingTracking = Boolean.parseBoolean((String)pm.get("marketingTracking"));
//					if (insertMarketingTracking){
//			        	HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
//						ValueExpression ve1 = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.REGISTER_ATTEMPT, Contact.class);
//				        Contact contact = (Contact) ve1.getValue(context.getELContext());
//				        MarketingTracker.checkExistCookieWhenInsertContact(request, response, contact.getEmail(), contact.getMobilePhone());
//						MarketingTracker.insertMarketingTracker(request, response, contact.getId(), 0, Constants.MARKETING_TRACKING_SHORT_REG);
//
//						MarketingETS.etsMarketingActivity(request, response, contact.getId(), Constants.MARKETING_TRACKING_SHORT_REG);
//					}
//			} catch (SQLException e) {
//					log.debug("exception in MarketingTrackingInsert insert", e);
//				}
			context.responseComplete();
		}

		//	Handle save click for etrader
        if (!CommonUtil.isParameterEmptyOrNull((String)pm.get("queryString")) && !CommonUtil.isParameterEmptyOrNull(Constants.DYNAMIC_PARAM)) {
        	log.debug("Handle dynamic parameters from Landing page");
        	// TODO - aff sub ??? 
        	// get dynamic parameter.
        	String requestDP = (String)pm.get(Constants.DYNAMIC_PARAM);
 			try {
    			if (!CommonUtil.isParameterEmptyOrNull(requestDP)) {
    				// encode dynamic parameter.
    				String dynamicParamEncoded;
					dynamicParamEncoded = URLEncoder.encode(requestDP, "UTF-8");
					// save in cookie and session
    				ApplicationData.addCookie(Constants.DYNAMIC_PARAM, dynamicParamEncoded, response, Constants.DYNAMIC_PARAM_DESCRIPTION);
    				session.setAttribute(Constants.DYNAMIC_PARAM, dynamicParamEncoded);
    				if ((String)pm.get(Constants.FROM_REMARKETING) != null && ((String)pm.get(Constants.FROM_REMARKETING)).equalsIgnoreCase("true")) {
    					// encode dynamic parameter.
    					session.setAttribute(Constants.REMARKETING_DYNAMIC_PARAM, dynamicParamEncoded);
    				}


    			}
			} catch (UnsupportedEncodingException e) {
				log.error("Error encoding dynamic param", e);
			}
			context.responseComplete();
		}
        
//    	get all tournaments
        if (null != pm.get("listTournaments")) {
        	log.debug("get all tournaments");
        	try {
        		response.setCharacterEncoding("utf-8");
    	        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
    	        long skinId = ApplicationData.getSkinId(session, request);
    			User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
    			Long userId = null;
    			if (user.getId() != 0) {
    				skinId = user.getSkinId();
    				userId = user.getId();
    			}
    			ArrayList<Tournament> list = new ArrayList<Tournament>();
    			for (Tournament tournament : TournamentManagerBase.getAllTournamentsCache()) {
    				if (tournament.isOpen() && tournament.getSkins().contains(skinId)) {
    					ArrayList<TournamentUsersRank> users = TournamentUsersRankManagerBase.getTournamentUsersWithUser(tournament, userId);
    					tournament.setUsers(users.toArray(new TournamentUsersRank[users.size()]));
    					list.add(tournament);
    				}
    			}
    			Gson gson = new GsonBuilder().create();
//    			msg = gson.toJson(list);
    			response.getWriter().write(gson.toJson(list));
    			
    		} catch (Exception e) {
    			log.error("cant get tournaments", e);
    		}
			context.responseComplete();
		}

/*
        if (!CommonUtil.IsParameterEmptyOrNull((String)pm.get("remarketing"))
        	&& !CommonUtil.IsParameterEmptyOrNull(Constants.DYNAMIC_PARAM)
        	&& !CommonUtil.IsParameterEmptyOrNull(Constants.COMBINATION_ID)) {// save r_dp & r_combid in case of remarketing parameter

        	log.debug("Handle remarketing dynamic parameters from Landing page");
        	String remarketingBool = (String)pm.get("remarketing");
        	if(remarketingBool.equalsIgnoreCase("true")){
        		session.setAttribute(Constants.FROM_REMARKETING, "true");
        		session.setAttribute(Constants.REMARKETING_COMBINATION_ID,(String)pm.get(Constants.COMBINATION_ID));
	        	// get dynamic parameter.
	        	String requestDP = (String)pm.get(Constants.DYNAMIC_PARAM);
	 			try {
	    			if (!CommonUtil.IsParameterEmptyOrNull(requestDP)) {
	    				// encode dynamic parameter.
	    				String dynamicParamEncoded;
						dynamicParamEncoded = URLEncoder.encode(requestDP, "UTF-8");
						// save in session
	    				session.setAttribute(Constants.REMARKETING_DYNAMIC_PARAM, dynamicParamEncoded);
	    			}
				} catch (UnsupportedEncodingException e) {
					log.error("Error encoding dynamic param", e);
				}
        	}
		}
*/

		//Save user name on login authentication page - Baidu
		if (requestURL != null && requestURL.indexOf("loginAuth.jsf") > -1 && null != pm.get("userName")) {
			String userName =  (String)pm.get("userName");
			session.setAttribute(Constants.USER_NAME_AUTH_LOGIN, userName);
		}

        // handle Call me iframe from Etrader.
        if (!CommonUtil.isParameterEmptyOrNull((String)pm.get("emailCallmeIframe"))) {
        	log.debug("Handle call me iframe");
            response.setContentType("text/xml");
            response.setCharacterEncoding("utf-8");
        	String email = (String)pm.get("emailCallmeIframe");
        	String firstName = (String)pm.get("firstNameCallmeIframe");
        	String lastName = (String)pm.get("lastNameCallmeIframe");
           	String mobilePhone = (String)pm.get("mobilePhoneCallmeIframe");
           	String combIdRequest = (String)pm.get("combIdIframe");

           	Long combId;
		    if (CommonUtil.isParameterEmptyOrNull(combIdRequest)) {
	        	ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
	        	Long combIdDefualt = ap.getSkinById(ap.getSkinId()).getDefaultCombinationId();
		    	combId = combIdDefualt;
		    } else {
		    	combId = Long.valueOf(combIdRequest);
		    }

           	Contact contactReq = new Contact();
           	contactReq.setEmail(email);
           	contactReq.setFirstName(firstName);
           	contactReq.setLastName(lastName);
           	contactReq.setText(firstName + " " + lastName);
           	contactReq.setPhone(mobilePhone);
           	contactReq.setType(Contact.CONTACT_ME_TYPE);
			contactReq.setWriterId(CommonUtil.getWebWriterId());
			contactReq.setCountryId(Constants.COUNTRY_ID_IL);
			contactReq.setSkinId(Skin.SKIN_ETRADER);
			contactReq.setCombId(combId);
        	try {
				ContactsManager.insertNewContactFromIframe(contactReq);
			} catch (SQLException e) {
				log.error("handle Call me iframe from Etrader", e);
			}
			context.responseComplete();
		}

        if(!CommonUtil.isParameterEmptyOrNull((String)pm.get("amountForLive"))) {
        	log.debug("Handle amount conversion from live");
            response.setContentType("text/xml");
            response.setCharacterEncoding("utf-8");

            try {
	    		User user = ApplicationData.getUserFromSession();
	            double rate = CurrencyRatesManagerBase.getInvestmentsRate(user.getCurrencyId());
	            long amount = Math.round(Double.parseDouble((String) pm.get("amountForLive")) / rate);
	            amount = (amount / 100L) * 100L; // remove the digits after the decimal point

	            InvestmentLimit il = InvestmentsManager.getInvestmentLimit(
	            											Opportunity.TYPE_REGULAR,
															(long) Opportunity.SCHEDULED_HOURLY,
															Long.parseLong((String) pm.get("marketId")),
															user.getCurrencyId(),
															user.getId());

	            String amountStr = CommonUtil.displayAmountForInput((il.getMinAmount() < amount ? amount : il.getMinAmount()),
	            												false,
	            												user.getCurrencyId().intValue());

	            finishAJAXRequest(context, "application/x-www-form-urlencoded; charset=UTF-8", amountStr);
            } catch (Exception e) {
    			log.error("Unable to handle amount conversion from live", e);
    			finishAJAXRequest(context, "application/x-www-form-urlencoded; charset=UTF-8", "-1");
    		}
			return;
        }
             
        //inatecIframe    
        //TODO ignore lower case
        if (null != context && null != context.getViewRoot() && null != context.getViewRoot().getViewId() && 
    			(context.getViewRoot().getViewId().contains("inatecIframeUrlNotification") || context.getViewRoot().getViewId().contains(InatecIframeInfo.INTERNAL_PAGE_NAME))) {       	
        	log.info("START, Inatec Iframe response notification - inatecIframeUrlNotification. with: " + pm);
        	
        	String txid = (String)pm.get("TXID");
        	if (!CommonUtil.isParameterEmptyOrNull(txid)) {
	             try {
	                 long transactionId =  Long.valueOf(txid);
	                 String cs = (String) pm.get("cs");
	                 //TODO check checksum.
	                 if (TransactionsManager.isInatecIframeNotNotifiedTrx(transactionId)) {
							if (session != null) {
								try {
									loginId = (Long) session.getAttribute(Constants.LOGIN_ID) == null ? 0L : (Long) session.getAttribute(Constants.LOGIN_ID);
								} catch (Exception e) {
									log.error("Cannot get login ID from session!", e);
									loginId = 0L;
								}
							}
	                	 TransactionsManagerBase.updateInatecIframeTransaction(transactionId, cs, pm, loginId);
	                 } else {
	                 	log.info("Inatec Iframe Transaction " + transactionId + ", allready notified!");           
	                 }
	             } catch (Exception e) {
	                 log.error("Failed to process inatecIframe return URL response.", e);
	             }
        	} else {      		
        		log.info("Problem, Inatec Iframe response. NO parameters found as required in the API.");        		
        	}
            log.info("END, Inatec Iframe response. with: " + pm);
        }
        
        if (null != context && null != context.getViewRoot() && null != context.getViewRoot().getViewId() &&
        		/*context.getViewRoot().getViewId().contains(InatecIframeInfo.FAILED_PAGE_NAME)) || 
        		context.getViewRoot().getViewId().contains(InatecIframeInfo.INTERNAL_PAGE_NAME)  ||
        		context.getViewRoot().getViewId().contains(InatecIframeInfo.SUCCESS_PAGE_NAME)*/ 
        		(context.getViewRoot().getViewId().contains(InatecIframeInfo.RETURN_PAGE_NAME))) {       	
        	log.info("START, Inatec Iframe response view. with: " + pm);
        	
            String message = null;
            try {
            	User user = context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER, User.class);
         	   	Transaction tran = context.getApplication().evaluateExpressionGet(context, Constants.BIND_TRANSACTION, Transaction.class);
         	   	TransactionsManager.afterDepositAction(user, tran, TransactionSource.TRANS_FROM_WEB);	   
     	   		message = CommonUtil.getMessage("inatecIframe.after.success.deposit", null);
    		} catch (Throwable t) {
               log.error("Failed to process Envoy Pay-In Notifications.", t);
    		}

	        DepositForm df = (DepositForm) context.getApplication().createValueBinding(Constants.BIND_DEPOSIT_FORM).getValue(context);
		 	df.setInatecIframeResultMsg(message);  
            log.info("END, Inatec Iframe response. with: " + pm);
        }

        //CDPay server return URL.
        if (null != context && null != context.getViewRoot() && null != context.getViewRoot().getViewId() &&
        		context.getViewRoot().getViewId().indexOf("cdpayServerReturnURL.xhtml") != -1 && pm.get("transactionid") != null) {
        	log.debug("cdpay, 100, START: in 'cdpayServerReturnURL' statement ");
        	log.info("Check CDPay resualt transaction ");
        	try {
					if (session != null) {
						try {
							loginId = (Long) session.getAttribute(Constants.LOGIN_ID) == null ? 0L : (Long) session.getAttribute(Constants.LOGIN_ID);
						} catch (Exception e) {
							log.error("Cannot get login ID from session!", e);
							loginId = 0L;
						}
					}
        		TransactionsManagerBase.updateCDPayTransaction(pm, loginId);
    		} catch (Throwable t) {
                log.error("Failed to process CDPay Status URL response.", t);
            }
            response.setStatus(HttpServletResponse.SC_OK);
            context.responseComplete();
            log.debug("199, END: in 'cdpayServerReturnURL' statement ");
        }

        //CDPay return URL
        if (null != context && null != context.getViewRoot() && null != context.getViewRoot().getViewId() &&
        		context.getViewRoot().getViewId().indexOf("cdpayReturnURL.xhtml") != -1 && pm.get("merchant_order") != null) {
        	log.debug("cdpay, 200, START: in 'cdpayReturnURL' statement ");
        	log.info("User returned from CDPay site - returnURL");
        	try {
        		Transaction tran = TransactionsManagerBase.getCDPayReturnTransaction(pm);
        		if (tran != null) {
                    ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_TRANSACTION, Transaction.class);
                    ve.getValue(context.getELContext());
        			ve.setValue(context.getELContext(), tran);
        		}
        	} catch (Throwable t) {
                log.error("Failed to process CDPay return URL response.", t);
            }
        	log.debug("cdpay, 299, END: in 'cdpayReturnURL' statement ");
        }

        // register_content.jsf ajax call
        if (pm.get("register_from_short_reg") != null) {
        	
        	// if coming from LP clean session 
        	session.removeAttribute("errorJson");
        	session.removeAttribute("valuesJson");
        	
        	String email = (String) pm.get("funnel_email");
        	String firstName = (String) pm.get("funnel_firstName");
        	String lastName = (String) pm.get("funnel_lastName");
        	String countryId = (String) pm.get("funnel_countryId");
        	String mobilePhone = (String) pm.get("funnel_mobilePhone");
        	String mobilePhonePrefix = (String) pm.get("funnel_phonePrefix");
        	String password = (String) pm.get("funnel_password");
        	String passwordConfirm = (String) pm.get("funnel_passwordConfirm");
        	String fingerPrint = (String) pm.get("funnel_fingerPrint");
        	ApplicationData ap = context.getApplication().evaluateExpressionGet(context, Constants.BIND_APPLICATION_DATA, ApplicationData.class);
        	ContactUsForm contactUsForm = new ContactUsForm(email, "funnel_email", firstName, "funnel_firstName", lastName, "funnel_lastName",
        														countryId, mobilePhone, "funnel_mobilePhone", mobilePhonePrefix, ap.getSkinId());
        	
			try {
				// TODO make validations before creating a contact
				boolean hasContactErrors = contactUsForm.fillContextWithErrorMessages(ap.getUserLocale());
				if (!hasContactErrors) {
					contactUsForm.createQuickStartContact(context, ap.getRealCountryId());
					// TODO pass all parameters, sync logic with the created contact and pass field ids for the error messages
					RegisterForm registerForm = new RegisterForm(
															email,
															"funnel_email",
															firstName,
															lastName,
															countryId,
															mobilePhone,
															"funnel_mobilePhone",
															mobilePhonePrefix,
															password,
															passwordConfirm,
															"",
															fingerPrint);
					try {
						// validations are made in insert method
						registerForm.insertUser();
					} catch (Exception e) {
						log.error("Couldn't insert user", e);
					}
				}
			} catch (SQLException e) {
				log.error("Failed to insert marketing tracker", e);
			}
        }
        //Catch AJAX ET Regulation
        if (pm.get("binaryOptionsKnowledgeConfirm") != null) {
        	User user = null;
        	try {
        		user = ApplicationData.getUserFromSession();
			} catch (Exception e) {
				log.error("Can't get user when binaryOptionsKnowledgeConfirm", e);
				return;
			}
        	if(user != null && user.getId() > 0){
        		int confirmed = -1;
        		try {
            			confirmed = Integer.parseInt((String) pm.get("confirmed"));
            			IssuesManagerBase.insertIssueUserRegulationKnowledgeQuestion(user.getId(), confirmed);
				} catch (Exception e) {
					log.error("Can't insert ET Regulation Isseu with params:" + pm.get("confirmed") + " for userId:" + user.getId(), e);
					return;
				}
        		log.debug("Inserted ET Regulation Isseu with params:" + pm.get("confirmed") + " for userId:" + user.getId());
        	} else {
        		log.debug("The user is not in Session!");
        	}
        	
        	return;
        } else if (pm.get("unsuspendETReg") != null){
        	User user = null;
        	try {
        		user = ApplicationData.getUserFromSession();
			} catch (Exception e) {
				log.error("Can't get user when binaryOptionsKnowledgeConfirm", e);
				return;
			}
        	if(user != null && user.getId() > 0){
        		try {
        				if(!il.co.etrader.bl_managers.UsersManagerBase.sendActivationMailForSuspend(user.getId(), true)){
        					log.error("Can't send Activation Mail For Suspend userId:" + user.getId());
        				} else {
        					log.error("Send Activation Mail For Suspend userId:" + user.getId());
        				}
				} catch (Exception e) {
					log.error("Can't insert ET Regulation Isseu with params:" + pm.get("confirmed") + " for userId:" + user.getId(), e);
					return;
				}        		
        	}  else {
        		log.debug("The user is not in Session!");
        	}
        	return;
        }
	}
}

    private void checkRequestMethod(FacesContext context, HttpSession session) {
        String requestMethod = null;
        try {
            HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
            requestMethod = request.getMethod();
        } catch (Exception e) {
            log.error("Can't check request method.", e);
        }
        if (log.isDebugEnabled()) {
            log.log(Level.DEBUG, "AJAX!!! Request method: " + requestMethod);
        }
        if (null != requestMethod && !requestMethod.equals("POST")) {
            log.warn("User: " + context.getExternalContext().getRemoteUser() + " sessionId: " + session.getId() + " trying " + requestMethod + " requests.");
        }

    }

    private void finishAJAXRequest(FacesContext context, String contentType, String reply) {
        if (log.isDebugEnabled()) {
            log.debug("Response: " + reply);
        }
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.setContentType(contentType);
        response.setHeader("Cache-Control", "no-cache");
        if (null == reply) {
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        } else {
            try {
                response.getWriter().write(reply);
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't write reply.", e);
            }
        }
        context.responseComplete();
    }

    @SuppressWarnings("unchecked")
    private int saveMessages(final FacesContext facesContext) {
        List<FacesMessage> messages = new ArrayList<FacesMessage>();
        for (Iterator<FacesMessage> iter = facesContext.getMessages(null); iter.hasNext();) {
            messages.add(iter.next());
            iter.remove();
        }

        if (messages.size() == 0) {
            return 0;
        }

        Map<String, Object> sessionMap = facesContext.getExternalContext().getSessionMap();
        List<FacesMessage> existingMessages = (List<FacesMessage>) sessionMap.get(sessionToken);
        if (existingMessages != null) {
            existingMessages.addAll(messages);
        } else {
            sessionMap.put(sessionToken, messages);
        }
        return messages.size();
    }

    @SuppressWarnings("unchecked")
    private int restoreMessages(final FacesContext facesContext) {
        Map<String, Object> sessionMap = facesContext.getExternalContext().getSessionMap();
        List<FacesMessage> messages = (List<FacesMessage>) sessionMap.remove(sessionToken);

        if (messages == null) {
            return 0;
        }

        int restoredCount = messages.size();
        for (Object element : messages) {
            facesContext.addMessage(null, (FacesMessage) element);
        }
        return restoredCount;
    }

    private boolean ezeebillRequestIsComplete(Map<String, String> pm) {
    	return 
    	pm.containsKey("secure_hash") &&
		pm.containsKey("merch_id") &&
    	pm.containsKey("currency") &&
		pm.containsKey("pay_type") &&
		pm.containsKey("txn_status") &&
    	pm.containsKey("merch_txn_id") && 
    	pm.containsKey("amount") &&
		pm.containsKey("txn_response_code");
    }
    
}
