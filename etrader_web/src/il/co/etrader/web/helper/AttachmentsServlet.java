package il.co.etrader.web.helper;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import il.co.etrader.web.bl_managers.UsersManager;
import il.co.etrader.web.bl_vos.MailBoxUserAttachment;
import il.co.etrader.web.bl_vos.User;
import il.co.etrader.web.util.Constants;

/**
 * Handle mailBox attachments
 * get the mailBoxAttachmentId as parameter and return the attachment in the response
 *
 * @author Kobi
 *
 */
public class AttachmentsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger.getLogger(AttachmentsServlet.class);

	protected static Hashtable<String, String> mimeTypes = new Hashtable<String, String>();

	static {
		 mimeTypes.put("pdf", "application/pdf");
	}


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        String uri = request.getRequestURI();
        if (log.isDebugEnabled()) {
            log.debug("URI requested: " + uri);
        }
        int lastSlash = uri.lastIndexOf("/");
        if (lastSlash != -1 && lastSlash < uri.length()) {
            String attachmentId = request.getParameter("id");  // attachmentId

            MailBoxUserAttachment attachment = null;
			try {
				attachment = UsersManager.getMailBoxAttchment(Long.parseLong(attachmentId));
			} catch (Exception e) {
				log.error("Problem getting mailBox attachment! ", e);
			}

			ByteArrayOutputStream baos = attachment.getAttachment();

       		if (null == baos || baos.size() == 0) {  // file not found
                    log.debug("404 for getting attachment: " + attachment.getAttachmentName());
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
		    }

       		// security check: if fileName not contains the userId - return error file not found
       		User user = (User) request.getSession().getAttribute(Constants.BIND_SESSION_USER);
       		if (null != user && user.getId() > 0) {
       			if (null != attachment.getAttachmentName()) {
       				if (attachment.getAttachmentName().indexOf(String.valueOf(user.getId())) == -1) {
       	       		  log.debug("getting attachment - userId not in the fileName");
       	              response.sendError(HttpServletResponse.SC_NOT_FOUND);
       	              return;
       				}
       			}
       		} else {
       		  log.debug("getting attachment - user not found!");
              response.sendError(HttpServletResponse.SC_NOT_FOUND);
              return;
       		}


	        // set the content type
	        String fileExtention = attachment.getAttachmentName().substring(attachment.getAttachmentName().lastIndexOf(".") + 1);
	        String contentType = null;
	        try {
	        	if(null != fileExtention && null != mimeTypes.get(fileExtention)) {
	        		contentType = mimeTypes.get(fileExtention);
	        	}
	        } catch (Exception e) {
				log.warn("mime type not found! " + fileExtention);
				contentType = null;
			}

            response.setHeader("Cache-control", "max-age=60, must-revalidate");
	        if (null != contentType) {
	        	response.setContentType(contentType);
	        }
	        response.setHeader("Content-disposition", "attachment; filename=" + attachment.getAttachmentName());
            response.setContentLength(baos.size());

            OutputStream os = response.getOutputStream();
            os.write(baos.toByteArray());
            os.flush();
            os.close();
        }
    }



}