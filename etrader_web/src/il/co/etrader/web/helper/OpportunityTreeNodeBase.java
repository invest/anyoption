package il.co.etrader.web.helper;

import org.apache.myfaces.custom.tree2.TreeNodeBase;

import com.anyoption.common.beans.Opportunity;

public class OpportunityTreeNodeBase extends TreeNodeBase {
    private Opportunity opportunity;

    public OpportunityTreeNodeBase(String facet, String label, boolean leaf, Opportunity o) {
        super(facet, label, leaf);
        opportunity = o;
    }
    
    /**
     * @return Returns the opportunity.
     */
    public Opportunity getOpportunity() {
        return opportunity;
    }

    /**
     * @param opportunity The opportunity to set.
     */
    public void setOpportunity(Opportunity opportunity) {
        this.opportunity = opportunity;
    }
}