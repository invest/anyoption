package il.co.etrader.web.helper;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.Ad;

import il.co.etrader.bl_vos.MarketingCombination;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.AdminManager;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.ContactsManager;
import il.co.etrader.web.bl_managers.UsersManager;
import il.co.etrader.web.util.Constants;

/**
 * Hande parameters after static landing page.
 * After static landing page we want to insert contact and save param in the session (later on we want to save it on the cookie)
 *
 * @author EranL
 *
 */
public class StaticLandingParamEtraderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(StaticLandingParamEtraderServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
    	saveData(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
    	saveData(request, response);
    }

    protected void saveData(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
    	 String uri = request.getRequestURI();
         if (log.isDebugEnabled()) {
             log.debug("Handle parameters after static landing page. URI requested: " + uri);
         }

  		HttpSession session = request.getSession();
 		request.setCharacterEncoding("UTF-8");
 		boolean isNoForm = request.getParameter("isNoForm") == null? false:true;

 		// Create url to the site
		String path = CommonUtil.getProperty("homepage.url") + "jsp/";
		
		String skinId = (String) request.getParameter("s");
		if (CommonUtil.isParameterEmptyOrNull(skinId)) {
			skinId = String.valueOf(Skin.SKIN_ETRADER);
		}
		
		String url = path + "registerAfterLanding.jsf?";
		String pageRedirect = request.getParameter("pageRedirect");
		if (!CommonUtil.isParameterEmptyOrNull(pageRedirect)) {
			url = pageRedirect + ".jsf?";
		}

		// Reset params
		String params = ConstantsBase.EMPTY_STRING;
		String firstName = ConstantsBase.EMPTY_STRING;
		String lastName = ConstantsBase.EMPTY_STRING;
		String email = ConstantsBase.EMPTY_STRING;
		String mobilePhone = ConstantsBase.EMPTY_STRING;
		String mobilePhonePrefix = ConstantsBase.EMPTY_STRING;
		boolean reciveUpdates = false;

 		if (!isNoForm) {
			//get parameters from landing page in order to pass it to register page
			firstName 			= (String) request.getParameter("first_name");
			lastName 			= (String) request.getParameter("last_name");
			email 				= (String) request.getParameter("email");
			mobilePhone 		= (String) request.getParameter("mobile_m");
			mobilePhonePrefix 	= (String) request.getParameter("mobilePhonePref");
			reciveUpdates = request.getParameter("agree") == null? false:true;			

			params =
				"firstName="+ URLEncoder.encode(firstName, "UTF-8") +
				"&last_Name=" + URLEncoder.encode(lastName, "UTF-8") +
				"&email=" + email +
				"&mobilePhone=" + mobilePhone +
				"&reciveUpdates=" + reciveUpdates +
				"&regAtt=true" +
				"&mobilePhonePref=" + mobilePhonePrefix;
			
	 		if (mobilePhonePrefix != null) {
	 			mobilePhone =  mobilePhonePrefix + mobilePhone;
	 		}
			
 		}
 				
 		String quer = request.getQueryString();
		if (!CommonUtil.isParameterEmptyOrNull(quer)) {
			params += "&" + quer;
		}

		String landingHttpRefererCookie = null;
		String landingFirstTimeVisitCookie = null;
		String firstVisitCookie = null;

 		boolean cookieFound = false;
 		boolean skinCookie = false;
 		Cookie refererCookie = null;
 		boolean combIdCookieFound = false;
 		String skinValue = String.valueOf(Skin.SKIN_ETRADER);
 		String combIdCookieValue = "";
 		String dynamicParamCookieValue = null;
 		String httpRefererCookieValue = null;
 		Date timeFirstVisit = null;
 		Cookie[] cs = request.getCookies();
 		if (cs != null) {
 			for (int i = 0; i < cs.length; i++) {
 				if (cs[i].getName().equals(Constants.SKIN_ID)) {
 					skinCookie = true;
 					skinValue = cs[i].getValue();   // save skinId cookie value
 				} else if (cs[i].getName().equals(Constants.COMBINATION_ID)) {
 					log.log(Level.DEBUG, "found cmobid cookie! combid:" + cs[i].getValue());
 					combIdCookieFound = true;
 					combIdCookieValue = cs[i].getValue();
 				} else if (cs[i].getName().equals(Constants.REFERRER)) {
 					cookieFound = true;
 					refererCookie = cs[i];
 				} else if (cs[i].getName().equals(Constants.DYNAMIC_PARAM)) {
 					dynamicParamCookieValue = cs[i].getValue();
 				} else if (cs[i].getName().equals(Constants.HTTP_REFERE_COOKIE)) {
 					httpRefererCookieValue = cs[i].getValue();
 				} else if (cs[i].getName().equals(Constants.LANDING_HR_COOKIE)) {
					landingHttpRefererCookie = cs[i].getValue();
				} else if (cs[i].getName().equals(Constants.LANDING_FIRST_VISIT_COOKIE)) {
					landingFirstTimeVisitCookie = cs[i].getValue();
				} else if (cs[i].getName().equals(Constants.TIME_FIRST_VISIT_COOKIE)) {
					firstVisitCookie = cs[i].getValue();
				}
 			}
 		}

 		if  (skinCookie) {  // found skin cookie
 			log.log(Level.DEBUG, "found skinId cookie, value: " + skinValue);
 		}
 		//handle skinId and save it on cookie and session
		if ( skinCookie == false || !skinValue.equals(Skin.SKIN_ETRADER) ) {  // skinId not in cookie
			ApplicationData.addCookie(Constants.SKIN_ID, String.valueOf(Skin.SKIN_ETRADER), request, response, Constants.SKIN_ID_DESCRIPTION);
			session.setAttribute(Constants.SKIN_ID, String.valueOf(Skin.SKIN_ETRADER));
			log.debug("Saving skinId value in Session!");
		}

		//handle first time visit.
		if (null == firstVisitCookie) {
			String decodedTimeFirstVisit = null;
			try {
				decodedTimeFirstVisit = URLDecoder.decode(landingFirstTimeVisitCookie, "UTF-8");
			} catch (Exception e) {
				log.warn("Error! problem in decode landingFirstTimeVisitCookie to UTF-8", e);
			}
			//SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm dd/MM/yy");
			SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yy");
			try {
				timeFirstVisit = dateFormat.parse(decodedTimeFirstVisit);
			} catch (ParseException e1) {
				log.warn("Error! problem in parsing time first visit", e1);
			} catch (Exception e) {
				log.error("Error! problem with timeFirstVisit cookie", e);
			}
			if (null != decodedTimeFirstVisit) {
				try {
					ApplicationData.addCookie(Constants.TIME_FIRST_VISIT_COOKIE, decodedTimeFirstVisit, request, response, Constants.TIME_FIRST_VISIT_COOKIE);
				} catch (Exception e) {
					log.warn("Error! problem in saving landing first time visit");
				}
			}
		}


 		// handle http referrer
 		String httpReferer = (String) session.getAttribute(Constants.HTTP_REFERE_LANDING);
 		if ( null == httpReferer ) {  // not in session
 			// try to take cookie.
 			httpReferer = httpRefererCookieValue;
 			if (null == httpReferer && null != landingHttpRefererCookie) {
				try {
					session.setAttribute(Constants.HTTP_REFERE_LANDING, landingHttpRefererCookie);
					ApplicationData.addCookie(Constants.HTTP_REFERE_COOKIE, landingHttpRefererCookie, request, response, Constants.HTTP_REFERE_LANDING);
				} catch (Exception e) {
					log.warn("Error! problem in saving landing http referrer");
				}
			}
 		}

 		// handle dynamic parameter
 		String dynamicParam = (String) session.getAttribute(Constants.DYNAMIC_PARAM);
 		String dynamicParamEncoded = null;
 		if (dynamicParam == null) {
 			//trying to take dp from cookie
 			dynamicParam = dynamicParamCookieValue;
 			if (dynamicParam == null) {
	        	log.debug("Handle dynamic parameters from Landing page");
	        	// get dynamic parameter from request.
	        	String requestDP = (String) request.getParameter(Constants.DYNAMIC_PARAM);
	 			try {
	 				if (!CommonUtil.isParameterEmptyOrNull(requestDP)) {
	    				// encode dynamic parameter.	    				
						dynamicParamEncoded = URLEncoder.encode(requestDP, "UTF-8");
						// save in cookie and session
	    				ApplicationData.addCookie(Constants.DYNAMIC_PARAM, dynamicParamEncoded, request, response, Constants.DYNAMIC_PARAM_DESCRIPTION);
	    				session.setAttribute(Constants.DYNAMIC_PARAM, dynamicParamEncoded);
	    			}
				} catch (UnsupportedEncodingException e) {
					log.error("Error encoding dynamic param", e);
				}
 			} else if (!dynamicParam.equalsIgnoreCase("undefined")) {
 				session.setAttribute(Constants.DYNAMIC_PARAM, dynamicParam);
 			}
 		}
 		
 		//handle affsubs
 		String affSub1 = (String) session.getAttribute(Constants.AFF_SUB1);
		String affSub2 = (String) session.getAttribute(Constants.AFF_SUB2);
		String affSub3 = (String) session.getAttribute(Constants.AFF_SUB3);
		
		if ( null == affSub1 ) {  // not in session
   			affSub1 = (String) request.getParameter(Constants.AFF_SUB1);
			if ( null != affSub1 ) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.AFF_SUB1, affSub1);
			}
		}
   		if ( null == affSub2 ) {  // not in session
   			affSub2 = (String) request.getParameter(Constants.AFF_SUB2);
			if ( null != affSub2 ) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.AFF_SUB2, affSub2);
			}
		}
   		if ( null == affSub3 ) {  // not in session
   			affSub3 = (String) request.getParameter(Constants.GCLID);
			if ( null != affSub3 ) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.AFF_SUB3, Constants.GCLID + "_" + affSub3);
			}
		}

 		// handle show movie parameter
 		String showMovieParam = (String) request.getParameter(Constants.LANDING_PAGE_SHOW_MOVIE);
 		if (null != showMovieParam && showMovieParam.equals("false")) {
 			session.setAttribute(Constants.LANDING_PAGE_SHOW_MOVIE, "false");
 		}

 		//try to take referrer and combid from session
 		String referrer = (String) session.getAttribute(Constants.REFERRER);
 		String combId = (String) session.getAttribute(Constants.COMBINATION_ID);
 		if (combId != null) {
 			// combination id exist in session! dont do nothing...
 			permanentRedirect(response, url, params);
 			return;
 		}
 		if (referrer != null) {
 			// referrer exist in session! add combination id
 			try {
	 			session.setAttribute(Constants.COMBINATION_ID, UsersManager.getCombIdByAdId(referrer));
	 			permanentRedirect(response, url, params);
	 			return;
			} catch (SQLException e) {
				log.error("Error with referrer", e);
			}
 		}

 		//try to take referrer and combid from request
 		referrer = request.getParameter(Constants.REFERRER);
 		combId = request.getParameter(Constants.COMBINATION_ID);
 		// check referrer & combination
 		if ( null == referrer && null == combId ) {
 			log.warn("No combination or referrer");
 			permanentRedirect(response, url, params);
 			return;
 		}
 		
	    //handle remarketing data
		try {			
			if (request.getParameter(Constants.FROM_REMARKETING) != null && request.getParameter(Constants.FROM_REMARKETING).equalsIgnoreCase("true")) {
		    	session.setAttribute(Constants.FROM_REMARKETING, "true");
		    	session.setAttribute(Constants.REMARKETING_COMBINATION_ID, combId);	
				// encode dynamic parameter.
		    	if (!CommonUtil.isParameterEmptyOrNull(dynamicParamEncoded)) {
		    		session.setAttribute(Constants.REMARKETING_DYNAMIC_PARAM, dynamicParamEncoded);
		    	}
			}
		} catch (Exception e) {
			log.warn("Error! problem in saving remarketingParam in the session");
		}
	
 		//first try to take co
 		if ( null != referrer ) {
 			long r = 0;
 			try {
 				r = Long.valueOf(referrer);
 			} catch (Exception e) {
 				permanentRedirect(response, url, params);
 				return;
 			}
 			try {
	 			Ad ad = AdminManager.getAd(r);
	 			if (ad == null) {
	 				log.debug("Ad ID:" + r + " does not exists! no record inserted.");
	 				permanentRedirect(response, url, params);
	 				return;
	 			} else {
	 				String combIdMap = UsersManager.getCombIdByAdId(referrer);
	 				combId = combIdMap;  // save for cookie
	 			}
 			} catch (Exception e) {
 				log.error("Exception when trying to take ad from DB");
 			}

 		} else if ( null != combId ) {
 			long combination = 0;
 			try {
 				combination = Long.valueOf(combId);
 			} catch (Exception e) {
 				permanentRedirect(response, url, params);
 				return;
 			}

 			try {
	 			MarketingCombination comb = AdminManager.getCombinationId(combination);
	 			if ( null == comb ) {
	 				log.debug("Combination ID:" + combination + " does not exists! no record inserted.");
	 				permanentRedirect(response, url, params);
	 				return;
	 			} else {
	 				combId = String.valueOf(combination);
	 			}
 			} catch (Exception e) {
 				log.error("Exception when trying to take combination from DB");
 				permanentRedirect(response, url, params);
 				return;
			}
 		}


 		// if we dont have combId cookie or we do have but it's different from the one on session and from default add cookie
 		if (!combIdCookieFound || (combIdCookieFound && !combId.equals(combIdCookieValue) &&
 									!combId.equals(String.valueOf(ApplicationData.getSkinById(Skin.SKIN_ETRADER).getDefaultCombinationId()))) ){
 			log.log(Level.DEBUG, "saving combinations id in cookie and session"); 					
 			// also put the combination in the session
 			try {
 				combId = String.valueOf(ApplicationData.getCombIdWithSPLogicStatic(request, Integer.valueOf(skinId)));
 			} catch (Exception e) {
 				log.log(Level.WARN, "saving combinations id in cookie and session");
			}
 			//  add combid cookie and session 	
 			session.setAttribute(Constants.COMBINATION_ID, combId);
 			ApplicationData.addCookie(Constants.COMBINATION_ID, combId, request, response, Constants.COMBINATION_ID);
 		}

 		if (!cookieFound && !combIdCookieFound) {
 			session.setAttribute(Constants.CLEAR_FIRST_TIME_VISIT,"false");
 		} else if ( cookieFound ) { // found r cookie
 			//deleteCookie(refererCookie, response);   // delete r cookie
 		}

 		String ip = CommonUtil.getIPAddress();
		String utcOffset = CommonUtil.getIsraelUtcOffset();
		session.setAttribute(Constants.COMBINATION_ID, combId);

		if (!isNoForm) {
			ContactsManager.insertContactStaticLP(email, firstName, lastName, Constants.ISRAEL_COUNTRY_ID, mobilePhone, reciveUpdates,
					null, null, landingHttpRefererCookie, timeFirstVisit, Integer.valueOf(skinId), request, response, ip, utcOffset);
		}

		session.setAttribute(Constants.SKIN_ID_FROM_LANDING, skinId);
		session.setAttribute(Constants.REGISTER_FROM_CONTACTUS_LANDING, "true");
		String encodedUrl = response.encodeRedirectURL(url);
		response.sendRedirect(encodedUrl + params);
    }

	private void permanentRedirect(HttpServletResponse response, String url, String params) {
		if (params != ConstantsBase.EMPTY_STRING) {
			params = params + "&marketingTracking=true";
		}
		CommonUtil.sendPermanentRedirect(response, url + params);
	}

}