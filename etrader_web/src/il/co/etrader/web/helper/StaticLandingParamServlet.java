package il.co.etrader.web.helper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Skin;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.ContactsManager;
import il.co.etrader.web.util.Constants;
import il.co.etrader.web.util.Utils;

/**
 * Hande parameters after static landing page.
 * After static landing page we want to insert contact and save param in the session (later on we want to save it on the cookie)
 *
 * @author EranL
 *
 */
public class StaticLandingParamServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(StaticLandingParamServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
    	saveData(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
    	saveData(request, response);
    }


    private void saveData(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.setCharacterEncoding("UTF-8");
	   	String reqUrl = request.getRequestURL().toString();
        String quer = request.getQueryString();
	    if (log.isDebugEnabled()) {
	        log.debug("Requested: " + reqUrl + "?" + quer);
	    }

	    HttpSession session = request.getSession();
		boolean isNoForm = request.getParameter("isNoForm") == null? false:true;
		String httpRefererCookie = null;
		String landingHttpRefererCookie = null;
		String landingTimeZoneOffsetCookie = null;
		String landingFirstTimeVisitCookie = null;
		String firstVisitCookie = null;
		boolean isFromFB = request.getParameter("isFromFB") == null ? false : true;
		String isMobileStr = request.getParameter("isMobile");
		boolean isMobile = isMobileStr == null ? false : Boolean.valueOf(isMobileStr);
		

		String skinId = (String) request.getParameter("s");
		long skinIdLong = 0;
		if (CommonUtil.isParameterEmptyOrNull(skinId)) {
			skinId = String.valueOf(Skin.SKIN_REG_EN);		
		}
		
		skinIdLong = Long.parseLong(skinId);
		//create url to the site
		String path = Utils.getPropertyByHost(request, "homepage.url") + "jsp/";
		String paths = Utils.getPropertyByHost(request, "homepage.url.https") + "jsp/";
		if (skinIdLong > 0) {
			Skin skin = ApplicationData.getSkinById(skinIdLong);
			String subDomain = ApplicationData.getSubDomainBySkin(skin.getKeySubdomain(), skin.isRegulated());
			path = path.replace("www", subDomain);
            paths = paths.replace("www", subDomain);
		}
				
		String url = paths;
		if (isFromFB) {
			if (isMobile) {
				url = url.replace("jsp/", "").concat("?");
			} else {
				url += "registerAfterLanding.jsf?";
			}
		} else {
			url += "registerAfterLanding.jsf?";
		}
		
		String pageRedirect = request.getParameter("pageRedirect");
		if (!CommonUtil.isParameterEmptyOrNull(pageRedirect)) {
			if (pageRedirect.equals("deposit")) {
				url = "pages/" + pageRedirect + ".jsf?";
			} else {
				url = pageRedirect + ".jsf?";
			}
		}

		//Reset params
		String params = ConstantsBase.EMPTY_STRING;
		String firstName = ConstantsBase.EMPTY_STRING;
		String lastName = ConstantsBase.EMPTY_STRING;
		String email = ConstantsBase.EMPTY_STRING;
		String mobilePhone = ConstantsBase.EMPTY_STRING;
		boolean reciveUpdates = false;
		
		if (!isNoForm) {
			//get parameters from landing page in order to pass it to register page
            Map<String, String> mpp = parseGETQueryString(quer);

            firstName = mpp.get("firstName");
			lastName = mpp.get("last_Name");
			email = mpp.get("email");
//			String landMobilePhoneCode = "";
			mobilePhone = request.getParameter("mobilePhone");
			reciveUpdates = "true".equals(request.getParameter("reciveUpdates"));

//			params =
//			"firstName="+ URLEncoder.encode(firstName, "UTF-8") +
//			"&last_Name=" + URLEncoder.encode(lastName, "UTF-8") +
//			"&email=" + email +
//			"&mobilePhoneCode=" + landMobilePhoneCode +
//			"&mobilePhone=" + mobilePhone +
//			"&reciveUpdates=" + reciveUpdates +
//			"&regAtt=true";
		}

		if (!CommonUtil.isParameterEmptyOrNull(quer)) {
//			params += "&" + quer;
		    params = quer;
		}
		String decodedTimeFirstVisit = null;
		Date timeFirstVisit = null;
		if (!isFromFB) {
			Cookie[] cs = request.getCookies();
			if (cs != null) {
				for (int i = 0; i < cs.length; i++) {
					if (cs[i].getName().equals(Constants.LANDING_HR_COOKIE)) {
						landingHttpRefererCookie = cs[i].getValue();
					} else if (cs[i].getName().equals(Constants.HTTP_REFERE_COOKIE)) {
						httpRefererCookie = cs[i].getValue();
					} else if (cs[i].getName().equals(Constants.LANDING_TIMEZONE_OFFSET_COOKIE)) {
						landingTimeZoneOffsetCookie = cs[i].getValue();
					} else if (cs[i].getName().equals(Constants.LANDING_FIRST_VISIT_COOKIE)) {
						landingFirstTimeVisitCookie = cs[i].getValue();
					} else if (cs[i].getName().equals(Constants.TIME_FIRST_VISIT_COOKIE)) {
						firstVisitCookie = cs[i].getValue();
					}
				}
			}
			
			try {
				decodedTimeFirstVisit = URLDecoder.decode(landingFirstTimeVisitCookie, "UTF-8");
			} catch (Exception e) {
				log.warn("Error! problem in decode landingFirstTimeVisitCookie to UTF-8");
			}
			//SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm dd/MM/yy");
			SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yy");
			
			try {
				timeFirstVisit = dateFormat.parse(decodedTimeFirstVisit);
			} catch (ParseException e1) {
				log.warn("Error! problem in parsing timeFirstVisit");
			} catch (Exception e) {
				log.error("Error! problem with timeFirstVisit cookie", e);
			}
		}
		String combId = (String) session.getAttribute(Constants.COMBINATION_ID);
		String dynamicParam = (String) session.getAttribute(Constants.DYNAMIC_PARAM);
		String affSub1 = (String) session.getAttribute(Constants.AFF_SUB1);
		String affSub2 = (String) session.getAttribute(Constants.AFF_SUB2);
		String affSub3 = (String) session.getAttribute(Constants.AFF_SUB3);
		String subAffKey = (String) session.getAttribute(Constants.SUB_AFF_KEY_PARAM);
		String tradeDoubler = (String) session.getAttribute(Constants.TRADE_DOUBLER);
		String dpn = (String) session.getAttribute(Constants.DPN);
		// Double click parameters
		String dfaPlacmentId = (String) session.getAttribute(Constants.DFA_PLACEMENT_ID_VALUE);
		String dfaCreativeId = (String) session.getAttribute(Constants.DFA_CREATIVE_ID_VALUE);
		String dfaMacro = (String) session.getAttribute(Constants.DFA_MACRO_VALUE);
		String remarketingParam = (String) session.getAttribute(Constants.FROM_REMARKETING);
		

		// handle dynamic parameter
   		if ( null == dynamicParam ) {  // not in session
			dynamicParam = (String) request.getParameter(Constants.DYNAMIC_PARAM);
			if ( null != dynamicParam ) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.DYNAMIC_PARAM, dynamicParam);
			}
		}
   		
   		if ( null == affSub1 ) {  // not in session
   			affSub1 = (String) request.getParameter(Constants.AFF_SUB1);
			if ( null != affSub1 ) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.AFF_SUB1, affSub1);
			}
		}
   		if ( null == affSub2 ) {  // not in session
   			affSub2 = (String) request.getParameter(Constants.AFF_SUB2);
			if ( null != affSub2 ) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.AFF_SUB2, affSub2);
			}
		}
   		if ( null == affSub3 ) {  // not in session
   			affSub3 = (String) request.getParameter(Constants.GCLID);
			if ( null != affSub3 ) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.AFF_SUB3, Constants.GCLID + "_" + affSub3);
			}
		}

		// handle sub affiliate parameter
		if ( null == subAffKey ) {  // not in session
			subAffKey = (String)request.getParameter(Constants.SUB_AFF_KEY_PARAM);
			if ( null != subAffKey ) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.SUB_AFF_KEY_PARAM, subAffKey.toLowerCase());
			}
		}

		// handle trade doubler parameter
		if (null == tradeDoubler) {  // not in session
			tradeDoubler = (String)request.getParameter(Constants.TRADE_DOUBLER_PARAM);
			if (null != tradeDoubler) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.TRADE_DOUBLER, tradeDoubler);
			}
		}
		
		// handle trade doubler parameter
		if (null == dpn) {  // not in session
   			dpn = (String) request.getParameter(Constants.DPN);
			if (null != dpn) {
				// save in session ( create the cookie in getSaveAnyOptionCookie function )
				session.setAttribute(Constants.DPN, dpn);
			}
		}

		// DFA placement
		if (null == dfaPlacmentId) {
			dfaPlacmentId = request.getParameter(Constants.DFA_PLACEMENT_ID);
			if (null != dfaPlacmentId) {
				session.setAttribute(Constants.DFA_PLACEMENT_ID_VALUE, dfaPlacmentId);
			}
		}

		// DFA dfaCreative
		if (null == dfaCreativeId) {
			dfaCreativeId = request.getParameter(Constants.DFA_CREATIVE_ID);
			if (null != dfaCreativeId) {
				session.setAttribute(Constants.DFA_CREATIVE_ID_VALUE, dfaCreativeId);
			}
		}

		// DFA dfaMacro
		if (null == dfaMacro) {
			dfaMacro = request.getParameter(Constants.DFA_MACRO);
			if (null != dfaMacro) {
				session.setAttribute(Constants.DFA_MACRO_VALUE, dfaMacro);
			}
		}
		if (combId != null) {
//			if (params != ConstantsBase.EMPTY_STRING) {
//				params = params + "&marketingTracking=true";
//			}			
			// combination id exist in session! dont do nothing...
			log.info("Combination id exist in session! going to redirect to: " + url + params);
			CommonUtil.sendPermanentRedirect(response, url + params);
			return;
		}

		// handle http referer
		if (null == httpRefererCookie) {
			if (null != landingHttpRefererCookie) {
				try {
					session.setAttribute(Constants.HTTP_REFERE_LANDING, landingHttpRefererCookie);
					ApplicationData.addCookie(Constants.HTTP_REFERE_COOKIE, landingHttpRefererCookie, request, response, Constants.HTTP_REFERE_LANDING);
				} catch (Exception e) {
					log.warn("Error! problem in saving landing http referrer");
				}
			}
		}

		//handle first time visit.
		if (null == firstVisitCookie) {
			if (null != decodedTimeFirstVisit) {
				try {
					session.setAttribute(Constants.LANDING_FIRST_VISIT_COOKIE, decodedTimeFirstVisit);
					ApplicationData.addCookie(Constants.TIME_FIRST_VISIT_COOKIE, decodedTimeFirstVisit, request, response, Constants.TIME_FIRST_VISIT_COOKIE);
				} catch (Exception e) {
					log.warn("Error! problem in saving landing first time visit");
				}
			}
		}
		
		combId = request.getParameter(Constants.COMBINATION_ID);
		
	    //handle remarketing data
		if (null == remarketingParam) {
			try {			
				if (request.getParameter(Constants.FROM_REMARKETING) != null && request.getParameter(Constants.FROM_REMARKETING).equalsIgnoreCase("true")) {
			    	session.setAttribute(Constants.FROM_REMARKETING, "true");
			    	session.setAttribute(Constants.REMARKETING_COMBINATION_ID, combId);	
			    	if (!CommonUtil.isParameterEmptyOrNull(dynamicParam)) {
						// encode dynamic parameter.
						String dynamicParamEncoded = URLEncoder.encode(dynamicParam, "UTF-8");
						session.setAttribute(Constants.REMARKETING_DYNAMIC_PARAM, dynamicParamEncoded);
			    	}
				}
			} catch (Exception e) {
				log.warn("Error! problem in saving remarketing params in the session");
			}
		}	   

		// check combination
		if (null == combId) {
//			if (params != ConstantsBase.EMPTY_STRING) {
//				params = params + "&marketingTracking=true";
//			}	
			log.info("Combination id not found! going to redirect to: " + url + params);
			CommonUtil.sendPermanentRedirect(response, url + params);
			return;
		}

		Long affiliateKey = ApplicationData.getAffIdFromDynamicParameter(dynamicParam);
		Long subAffiliateKey;
		try {
			subAffiliateKey = Long.valueOf(subAffKey);
		} catch (Exception e) {
			subAffiliateKey = null;
		}

		String ip = CommonUtil.getIPAddress();
		// we can't take ip on servlet. so for now we are taking the default
		// countryId for skin and on the register page we are updating his countryId
		long countryId = ApplicationData.getSkinById(Long.valueOf(skinId)).getDefaultCountryId();

		String utcOffset = Constants.OFFSET_GMT;
		if (null != landingTimeZoneOffsetCookie) {
			utcOffset = CommonUtil.formatJSUTCOffsetToString(Integer.valueOf(landingTimeZoneOffsetCookie));
		}

		session.setAttribute(Constants.COMBINATION_ID, combId);		
		if (!isNoForm) {
			ContactsManager.insertContactStaticLP(email, firstName, lastName, countryId, mobilePhone, reciveUpdates,
					affiliateKey, subAffiliateKey, landingHttpRefererCookie, timeFirstVisit, Integer.valueOf(skinId), request, response, ip, utcOffset);
		}


		session.setAttribute(Constants.SKIN_ID_FROM_LANDING, skinId);
		session.setAttribute(Constants.REGISTER_FROM_CONTACTUS_LANDING, "true");
		String encodedUrl = response.encodeRedirectURL(url);
		if (isFromFB && isMobile) {
			params += "#register";
		}
		log.info("Going to redirect to: " + encodedUrl + params);
		response.sendRedirect(encodedUrl + params);
    }
    
    // FIXME: Tony - move this method to com.anyoption.common.utils.CommonUtil and remove it from
    // RegisterForm.init
    // ApplicationData.isArriveFromContactusLanding
    // and maybe more
    public static Map<String, String> parseGETQueryString(String query) throws UnsupportedEncodingException {
        String s = URLDecoder.decode(query, "utf-8");
        HashMap<String, String> nvp = new HashMap<String, String>();
        StringTokenizer st = new StringTokenizer(s, "&");
        while (st.hasMoreTokens()) {
            StringTokenizer stInt = new StringTokenizer(st.nextToken(), "=");
            String key = null;
            if (stInt.hasMoreTokens()) {
                key = stInt.nextToken();
            }
            String value = null;
            if (stInt.hasMoreTokens()) {
                value = stInt.nextToken();
            }
            if (null != value) {
                nvp.put(key, value);
            } else {
                log.info("Parameter without value <" + key + ">");
            }
        }
        return nvp;
    }
}