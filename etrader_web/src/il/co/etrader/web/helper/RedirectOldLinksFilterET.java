package il.co.etrader.web.helper;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.mbeans.PageNames;

public class RedirectOldLinksFilterET implements Filter {
	 private static final Logger log = Logger.getLogger(RedirectOldLinksFilterET.class);
	    public void init(FilterConfig arg0) throws ServletException {
	        // do nothing
	    }
	    
	    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
	    	HttpServletRequest httpRequest = (HttpServletRequest) request;
	        HttpServletResponse httpResponse = (HttpServletResponse) response;
	        String uri = httpRequest.getRequestURI();
	        String uriToLowerCase = uri.toLowerCase();
	        //file types that need to be cached should be included here- this place is catching the redirects of the pretty that do not have pattern
	        if (!uriToLowerCase.endsWith(".css") 
	        		&& !uriToLowerCase.endsWith(".js")
	        		&& !uriToLowerCase.endsWith(".mp3")
	        		&& !uriToLowerCase.endsWith(".ogg")
	        		&& !uriToLowerCase.endsWith(".gif") 
	        		&& !uriToLowerCase.endsWith(".png") 
	        		&& !uriToLowerCase.endsWith(".jpg") 
	        		&& !uriToLowerCase.endsWith(".swf")
	        		//jsf and html are processed in the phase listener
	        		&& !uriToLowerCase.endsWith(".jsf") 
	        		&& !uriToLowerCase.endsWith(".html")) {
	        	httpResponse.addHeader("Pragma", "no-cache");
	        	httpResponse.setHeader("Cache-Control", "no-store");
	        }
	        String contextPath = httpRequest.getContextPath();
	        
	        log.trace("URL: " + uri + " context: " + contextPath);
	        //if user local env we need to cut the "/etrader/" part beacuse we do not have
	        //this part in test and live
	        if (!contextPath.equalsIgnoreCase("") && uri.startsWith(contextPath)) {
	           uri = uri.substring(contextPath.length());
	        }
	        // Exit quickly if for some reason the current request is non-HTTP
	        if (!(request instanceof HttpServletRequest)) {
	            chain.doFilter(request, response);
	            return;
	        }
	        int port = httpRequest.getLocalPort();
		    String header = httpRequest.getHeader("port");

		    if (header != null) {
		        try {
		            port = Integer.parseInt(header.trim());
		        } catch (Exception e) {
		            log.error("Can't parse port header value.", e);
		        }
		    }
	        String name = PageNames.getPageNameStatic("iw", uri);
	        if (null != name) {
	        	String redirect = (port == 80 ? "http://" : "https://") + httpRequest.getServerName() + contextPath + name;
	            log.info("Requested URI: " + uri + " name for it: " + name + " redirect: " + redirect);
	            CommonUtil.sendPermanentRedirect(httpResponse, redirect);
	            return;
	        } else {
	            chain.doFilter(request, response);
	        }
	    }
	    
	    public void destroy() {
	        // do nothing
	    }
}
