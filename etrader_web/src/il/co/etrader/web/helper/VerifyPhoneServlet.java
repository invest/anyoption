package il.co.etrader.web.helper;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.util.CommonUtil;

import il.co.etrader.bl_managers.SMSManagerBase;
import il.co.etrader.web.bl_managers.UsersManager;

/**
 * @author kiril.mutafchiev
 */
public class VerifyPhoneServlet extends HttpServlet {

	private static final long serialVersionUID = 6548811977756940174L;
	private static final Logger log = Logger.getLogger(VerifyPhoneServlet.class);

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
		verifyPhone(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
		super.doPost(request, response);
	}

	private void verifyPhone(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String uri = request.getRequestURI();
		log.debug("verifyPhone: uri = " + uri);
		String paramsStr = uri.substring(uri.lastIndexOf("/") + 1);
		String[] params = paramsStr.split(SMSManagerBase.SMS_VERIFY_PHONE_DELIMITER);
		long userId = Long.valueOf(params[0]);
		String mobilePhone = params[1];
		Locale locale = new Locale(params[2]);
		log.debug("Parsed parameters: " + userId + ", " + mobilePhone + ", " + locale);
		boolean res = UsersManager.updatePhoneVerifiedFlag(userId, mobilePhone);
		String msgKey = CommonUtil.getMessage(locale, res ? "sms.verify.phone.success" : "error.minwithdraw1", null);
		byte[] data = msgKey.getBytes("UTF-8");
		OutputStream os = response.getOutputStream();
		response.setContentType("text/plain");
		response.setHeader("Content-Length", String.valueOf(data.length));
		os.write(data, 0, data.length);
		os.flush();
		os.close();
	}
}