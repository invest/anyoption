package il.co.etrader.web.helper;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Locale;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Skin;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.managers.SkinsManagerBase;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.service.AnyoptionService;
import il.co.etrader.web.util.Constants;

public class SkinLocaleFilter implements Filter {
    private static final Logger log = Logger.getLogger(SkinLocaleFilter.class);
    public void init(FilterConfig arg0) throws ServletException {

    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException { 	
    	HttpServletRequest httpRequest = (HttpServletRequest) request;
        String uri = httpRequest.getRequestURI();
    	if((!httpRequest.getRequestURI().contains("ajax.jsf")
    			&& !uri.endsWith(".js")
    			&& !uri.endsWith(".ico")    			
    			&& !uri.endsWith(".jpg")
    			&& !uri.endsWith(".gif")
    			&& !uri.endsWith(".png")
    			&& !uri.endsWith(".css")
    			&& !uri.endsWith(".ogg")
    			&& !uri.endsWith(".mp3")
    			&& !uri.endsWith(".gif")
    			&& !uri.endsWith(".swf"))
    			//exclude several JS files used from LP
    			|| uri.endsWith("functions_with_jsp.js")
    			|| uri.endsWith("js_vars.js")
    			|| uri.endsWith("regForm_map.js")
    			){    		    	   
    		log.debug("doFiler SkinLocaleFilter URL:" + httpRequest.getRequestURL());
	    	HttpServletResponse httpResponse = (HttpServletResponse) response;
	        HttpSession session = httpRequest.getSession();
	        
	        //Check if user is change skin from skinSelector dropDown
	        boolean isSkinChange = false;
	        Long changeToSkinId = null;
	        String redirectToPage = "";
	        try {
	        	String methodReq = uri.substring(uri.lastIndexOf("/") + 1);
	        	if(methodReq.contains(AnyoptionService.CHANGE_SKIN_JSF)){
	        		isSkinChange = true;
	        		changeToSkinId = new Long(request.getParameter(AnyoptionService.CHANGE_SKIN_PARAM));
	        		redirectToPage = request.getParameter(AnyoptionService.CHANGE_SKIN_PRETTY_URL);
	        		if(redirectToPage == null){
	        			redirectToPage = "";
	        		}
	        		redirectToPage = httpRequest.getRequestURL().toString().replace(methodReq, redirectToPage);
	        		log.debug("Skin is change to[" + changeToSkinId + "] and need redirect to[" + redirectToPage +"]");	        		
	        	}
			} catch (Exception e) {
				isSkinChange = false;
				log.error("Can't check if skin is changed");
			}	        
	        
			String remoteUser = httpRequest.getRemoteUser();						
			Locale locale = (Locale) httpRequest.getSession().getAttribute(Constants.USER_LOCALE);
			Long cookieSkinId = getSkinId(session, httpRequest);		
			Long sParam = getSparam(session, httpRequest);
			Long sessionSkinId = (Long) httpRequest.getSession().getAttribute(Constants.SESSION_SKIN);
			Long countryIdDetectByIp = null;		
			Long subdomainSkinId = getSubdomainLang(httpRequest, sessionSkinId);
	
			//Check if we have skin In session 
			if(sessionSkinId == null){
				countryIdDetectByIp = getCountryFromIp(httpRequest);			
			}
			
			if(isSkinChange){
				sParam = changeToSkinId;
			}
			
			Skin skin = SkinsManagerBase.defineSkinNoUser(remoteUser, sParam, subdomainSkinId, locale, cookieSkinId, countryIdDetectByIp, sessionSkinId);
			if(skin != null){
				if(sessionSkinId == null || cookieSkinId != skin.getId() || sessionSkinId != skin.getId()){
					setCookieSessionData(session, httpRequest, httpResponse, skin, countryIdDetectByIp);
					log.debug("Set skin:" + skin.getId());
				}
			}
			
			if(isSkinChange){
				CommonUtil.sendPermanentRedirect(httpResponse, redirectToPage);
				return;
			}
			
    	}    	
		chain.doFilter(request, response);
    }
    
    private Long getSkinId(HttpSession session, HttpServletRequest request){
		Long skinId = null;
			// getting skin ID from cookie
		Cookie[] cs = request.getCookies();
		Cookie cookie = null;
		if (cs != null) {
			for (int i = 0; i < cs.length; i++) {
				if (cs[i].getName().equals(Constants.SKIN_ID)) {
					log.log(Level.TRACE, "found skinId cookie!");
					cookie = cs[i];
					break;
				}
			}
		}
		if (null != cookie && cookie.getValue() != null
				&& !cookie.getValue().equals("")
				&& cookie.getValue().indexOf("null") == -1) {
			try {
				skinId = Long.valueOf(cookie.getValue());
			} catch (Exception e) {
				if (log.isEnabledFor(Level.WARN)) {
					log.warn("SkinId cookie with strange value!", e);
				}
			}
		}		
		if (null == skinId) {
			String strSkinId = (String)session.getAttribute(Constants.SKIN_ID);
			if(strSkinId != null){
				skinId = new Long(strSkinId);
			}
		}
				
		return skinId;
    }
    
    private Long getSparam(HttpSession session, HttpServletRequest request){
    	Long sParam = null;
    	
		String skinIdParameter = request.getParameter(Constants.SKIN_ID);   // get skinId from request
		if (CommonUtil.isParameterEmptyOrNull(skinIdParameter)) {  // search for s param from landing
			skinIdParameter = (String) session.getAttribute(Constants.SKIN_ID_FROM_LANDING);
			if (null != skinIdParameter) { // clean the attribute
				session.setAttribute(Constants.SKIN_ID_FROM_LANDING, null);
			}
		}
		
		if(skinIdParameter != null){
			try {
				sParam = Long.parseLong(skinIdParameter);
			} catch (Exception e) {
				log.error("Can't pars S param:" + skinIdParameter + " to long", e);
			}
		}		
		return sParam;
    }
    
	private Long getSubdomainLang(HttpServletRequest request, Long sessionSkinId) {
		Long skinId = null;
		boolean isRegulated = true;
		Skin sessionSkin = null;
		if(sessionSkinId != null){
			sessionSkin = SkinsManagerBase.getSkin(sessionSkinId);
			isRegulated = sessionSkin.isRegulated();
		}
		
		String subDomainFromUrl = "anyoption.com";
		try{
			subDomainFromUrl = request.getServerName().substring(0, request.getServerName().indexOf('.'));
		}catch(Exception e) {}
        String skinBySubDomain = ApplicationData.getLocaleBySubDomain(subDomainFromUrl, isRegulated);
        if (null == skinBySubDomain) {
        	// make sure we are not missing the www1
        	skinBySubDomain = ApplicationData.getLocaleBySubDomain(subDomainFromUrl, !isRegulated);
        	skinId = ApplicationData.getSkinByLanguageCode(skinBySubDomain, !isRegulated);
        } else {
        	skinId = ApplicationData.getSkinByLanguageCode(skinBySubDomain, isRegulated);
        } 
        
        if(skinId == null){
        	skinId = 16l;
        	log.debug("Can't detect skinId in getSubdomainLang set Default Id:16");
        }
        Skin subDomainSkin = SkinsManagerBase.getSkin(skinId);
        //Check if lang are equals get from session
        if(null != sessionSkinId && null != subDomainFromUrl && sessionSkin.getLocale().equals(subDomainSkin.getLocale())){
        	skinId = sessionSkinId;
        }
        return skinId;
	}
	
	private Long getCountryFromIp(HttpServletRequest httpRequest){
		Long countryId = null;
		try {
			String ip = CommonUtil.getIPAddress(httpRequest);    	
			String countryCode = CommonUtil.getCountryCodeByIp(ip); 
			if(!CommonUtil.isParameterEmptyOrNull(countryCode)){
				countryId = CountryManagerBase.getIdByCode(countryCode);
				if(countryId == 0){
					countryId = null;
				}
			}			
		} catch (Exception e) {
			log.error("Can't get countryId from IP", e);
		}
		return countryId;
	}
	
	private void setCookieSessionData(HttpSession session, HttpServletRequest request, HttpServletResponse response, Skin skin, Long countryId){
		ApplicationData.addCookie(Constants.SKIN_ID, String.valueOf(skin.getId()), request, response, Constants.SKIN_ID_DESCRIPTION);
		ApplicationData.addCookie(Constants.CURRENCY_ID, String.valueOf(skin.getDefaultCurrencyId()), request, response, Constants.CURRENCY_ID_DESCRIPTION);
		ApplicationData.addCookie(Constants.LANGUAGE_ID, String.valueOf(skin.getDefaultLanguageId()), request, response, Constants.LANGUAGE_ID_DESCRIPTION);
		if(countryId != null){
			ApplicationData.addCookie(Constants.COUNTRY_ID, String.valueOf(countryId), request, response, Constants.COUNTRY_ID_DESCRIPTION);
			session.setAttribute(Constants.COUNTRY_ID, String.valueOf(countryId));
		}		
		ApplicationData.addCookie(Constants.IS_REGULATED, String.valueOf(skin.isRegulated()), request, response, Constants.IS_REGULATED_DESCRIPTION);
		// add to session		
		session.setAttribute(Constants.SESSION_SKIN, new Long(skin.getId()));
		session.setAttribute(Constants.SKIN_ID, String.valueOf(skin.getId()));
		session.setAttribute(Constants.CURRENCY_ID, String.valueOf(skin.getDefaultCurrencyId()));
		session.setAttribute(Constants.LANGUAGE_ID, String.valueOf(skin.getDefaultLanguageId()));
		session.setAttribute(Constants.IS_REGULATED, String.valueOf(skin.isRegulated()));
		
		try {
			setUserLocale(session, skin);
		} catch (SQLException e) {			
			log.error("Can't set User Locale ", e);
		}		
		log.debug("Saving SKIN values in Cookie and Session! , skinId: " + skin.getId());
	}

	private void setUserLocale(HttpSession session, Skin skin) throws SQLException {
		String counrtyCode = CountryManagerBase.getCodeById(skin.getDefaultCountryId());
		String languageCode = LanguagesManagerBase.getCodeById(skin.getDefaultLanguageId());
		if (skin.isRegulated()) {
		    session.setAttribute(Constants.USER_LOCALE, new Locale(languageCode, "EU"));
		    log.debug("Set USER_LOCALE:" + languageCode + "_EU");
		} else {
            session.setAttribute(Constants.USER_LOCALE, new Locale(languageCode, counrtyCode));
            log.debug("Set USER_LOCALE:" + languageCode + "_" + counrtyCode);
		}
	}

	public void destroy() {
        // do nothing
    }
}