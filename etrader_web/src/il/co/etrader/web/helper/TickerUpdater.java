package il.co.etrader.web.helper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.bl_managers.TickerManager;

public class TickerUpdater extends Thread {
	private static final Logger log = Logger.getLogger(TickerUpdater.class);

    private HashMap<Long, ArrayList<TradingPageBoxBean>> tickerMarkets;
    private int tickerMarketsRefreshPeriod;
    private ArrayList<Integer> skinsList;
    private boolean running;

	public TickerUpdater() {
		/*log.info("TickerUpdater init...");
		try {
			tickerMarketsRefreshPeriod = (int) CommonUtil.getPropertyLong("tickerMarkets.reload.period");
			String sl = CommonUtil.getProperty("tickerMarkets.reload.skins");
			StringTokenizer st = new StringTokenizer(sl, ",");
			skinsList = new Long[st.countTokens()];
			for (int i = 0; i < skinsList.length; i++) {
				skinsList[i] = Long.parseLong(st.nextToken().trim());
			}
			log.info("TickerUpdater initialized with - refresh period: " + tickerMarketsRefreshPeriod + " skins: " + sl);
			tickerMarkets = new HashMap<Long, ArrayList<TradingPageBoxBean>>();
		} catch (Exception e) {
			log.error("Error init TickerUpdater.", e);
		}*/
	}

	public TickerUpdater(ArrayList<Skins> skinsList2) {
        log.info("TickerUpdater init...");
        try {
            tickerMarketsRefreshPeriod = (int) CommonUtil.getPropertyLong("tickerMarkets.reload.period");
            skinsList = new ArrayList<Integer>();
            for (Skins skin : skinsList2) {
                if (skin.isLoadTickerMarkets()) {
                    skinsList.add(skin.getId());
                }
            }
            log.info("TickerUpdater initialized with - refresh period: " + tickerMarketsRefreshPeriod + " skins: " + skinsList);
            tickerMarkets = new HashMap<Long, ArrayList<TradingPageBoxBean>>();
        } catch (Exception e) {
            log.error("Error init TickerUpdater.", e);
        }
    }

    public void run() {
        Thread.currentThread().setName("TickerUpdater");
        if (skinsList.size() > 0) {
        	running = true;
        } else {
        	running = false;
        }
        Date nextLoadTime = new Date();
        log.info("TickerUpdater started.");
		while (running) {
			try {
				if (nextLoadTime.getTime() <= System.currentTimeMillis()) {
					log.info("Loading ticker markets");
                    for (Integer skin : skinsList) {
						log.debug("Loading ticker markets for skin: " + skin.intValue());
						tickerMarkets.put(skin.longValue(), TickerManager.getSkinTickerMarkets(skin.longValue()));
					}

		    		Calendar nextLoad = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		    		if (nextLoad.get(Calendar.MINUTE) >= 05 && nextLoad.get(Calendar.MINUTE) < 35) {
		    			nextLoad.set(Calendar.MINUTE, 35);
		            } else {
		            	if (nextLoad.get(Calendar.MINUTE) >= 35) {
		            		nextLoad.add(Calendar.HOUR_OF_DAY, 1);
		            	}
		            	nextLoad.set(Calendar.MINUTE, 05);
		            }
		    		nextLoadTime = nextLoad.getTime();
				} else {
					log.debug("Not time to load yet.");
				}
				Thread.sleep(nextLoadTime.getTime() - System.currentTimeMillis());
			} catch (Exception e) {
				log.error("Error in TickerUpdater.", e);
			}
		}
		log.info("TickerUpdater done.");
	}

	public void stopTickerUpdater() {
		running = false;
		this.interrupt();
	}

	public ArrayList<TradingPageBoxBean> getTickerMarkets(long skinId) {
		return tickerMarkets.get(skinId);
	}
}