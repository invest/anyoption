package il.co.etrader.web.helper;

import java.sql.SQLException;
import java.util.HashMap;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.apache.log4j.Logger;

import com.anyoption.common.charts.ChartHistoryCache;
import com.anyoption.common.charts.ChartsUpdater;
import com.anyoption.common.charts.LevelHistoryCache;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.jmx.AmqJMXMonitor;
import com.anyoption.common.jmx.DbJMXMonitor;
import com.anyoption.common.jmx.LevelServiceJMXMonitor;
import com.anyoption.common.jmx.cassandra.JMXCassandraConnectionReset;
import com.anyoption.common.jobs.JobsScheduler;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.managers.DBUtil;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.util.OpportunityCache;
import com.anyoption.common.util.PastExpiriesCache;
import com.copyop.common.jms.CopyOpAsyncEventSender;
import com.copyop.common.managers.ManagerBase;

import il.co.etrader.bl_managers.JobsManagerBase;
import il.co.etrader.jmx.JMXCacheCleanParts;
import il.co.etrader.service.WabServletContextListener;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.mbeans.PageNames;
import il.co.etrader.web.util.Constants;
import il.co.etrader.web.util.JMXCacheClean;

/**
 * Handle the LevelsCache init and shutdown. We need the levels cache so we can
 * take from there the current opportunity level when we insert investment.
 *
 * @author Tony
 */
public class ETServletContextListener extends WabServletContextListener {
    private static Logger log = Logger.getLogger(ETServletContextListener.class);

    private JMXCacheClean JmxCacheClean;
    private String JmxCacheCleanName;
    
    private JMXCacheCleanParts JmxCacheCleanParts;
    private String JmxCacheCleanNameParts;
    private JMXCassandraConnectionReset jmxCassandraReset;
    
    private LevelServiceJMXMonitor dfMonitor;
    private AmqJMXMonitor amqMonitor;
    private DbJMXMonitor dbMonitor;
    /**
     * Notification that the application is started
     *
     * @param event
     */
    public void contextInitialized(ServletContextEvent event) {
        super.contextInitialized(event);

        try {
            ApplicationData.init();
        } catch (SQLException sqle) {
            log.error("Can't load ApplicationData.", sqle);
        }

        ServletContext servletContext = event.getServletContext();

        String runJobs = servletContext.getInitParameter(JobsManagerBase.CONTEXT_PARAM_RUN_JOBS);
        if (null != runJobs && runJobs.equalsIgnoreCase("true")) {
        	String application = servletContext.getInitParameter(JobsManagerBase.CONTEXT_PARAM_RUN_APPLICATION);
            JobsScheduler js = new JobsScheduler(application);
            js.start();
            servletContext.setAttribute(JobsManagerBase.CONTEXT_PARAM_RUN_JOBS, js);
        }

        PageNames.init(servletContext);
        String monitoring = servletContext.getInitParameter("com.anyoption.monitoring.suffix");
        try {
			WebLevelsCache levelsCache = (WebLevelsCache) servletContext.getAttribute("levelsCache");
			if (levelsCache == null) {
				levelsCache = new WebLevelsCache(	initialContextFactory, providerURL, connectionFactory, queue, topic, msgPoolSize,
													recoveryPause, null);
				levelsCache.registerJMX(jmxName);
				servletContext.setAttribute("levelsCache", levelsCache);
			}

            LevelServiceJMXMonitor ljm = new LevelServiceJMXMonitor(levelsCache, monitoring);
            ljm.registerJMX();
            
            amqMonitor = new AmqJMXMonitor(monitoring);
            amqMonitor.initConnectionFactory();
            amqMonitor.registerJMX();

            if (log.isInfoEnabled()) {
                log.info("WebLevelsCahce bound to the context");
            }
        } catch (Exception e) {
            log.error("Failed to init WebLevelsCache", e);
        }

		try {
			dbMonitor = new DbJMXMonitor(DBUtil.getDataSource(), monitoring);
			dbMonitor.registerJMX();
		} catch (SQLException e1) {
			log.error("Can't register db monitor", e1);
		}

        /*
        TickerUpdater tickerUpdater = new TickerUpdater(ApplicationData.getSkinsList());
        tickerUpdater.start();
        servletContext.setAttribute(Constants.TICKER_UPDATER, tickerUpdater);
        */

        HashMap<String, Object> objectShare = null;
        try {
            Context initCtx = new InitialContext();
            Context envCtx = (Context) initCtx.lookup("java:comp/env");
            objectShare = (HashMap<String, Object>) envCtx.lookup("anyoption/share");
            log.info("Lookedup object share: " + objectShare);
        } catch (Exception e) {
            log.fatal("Cannot lookup object share", e);
        }

        ChartsUpdater chartsUpdater = null;
        if (null != objectShare) {
        	chartsUpdater = (ChartsUpdater) objectShare.get("chartsUpdater");
        	log.info("Got from share chartsUpdater: " + chartsUpdater);
        }
        if (null == chartsUpdater) {
        	log.info("Creating new chartsUpdater.");
        	chartsUpdater = new ChartsUpdater();
        	objectShare.put("chartsUpdater", chartsUpdater);
        }
        servletContext.setAttribute("chartsUpdater", chartsUpdater);

        LevelHistoryCache levelHistoryCache = null;
        if (null != objectShare) {
        	levelHistoryCache = (LevelHistoryCache) objectShare.get("levelHistoryCache");
        	log.info("Got from share levelHistoryCache: " + levelHistoryCache);
        }
        if (null == levelHistoryCache) {
        	log.info("Creating new levelHistoryCache");
        	levelHistoryCache = new LevelHistoryCache();
        	chartsUpdater.addListener(levelHistoryCache);
        	objectShare.put("levelHistoryCache", levelHistoryCache);
        }
        servletContext.setAttribute("levelHistoryCache", levelHistoryCache);

        ChartHistoryCache chartHistoryCache = null;
        if (null != objectShare) {
        	chartHistoryCache = (ChartHistoryCache) objectShare.get("chartHistoryCache");
        	log.info("Got from share chartHistoryCache: " + chartHistoryCache);
        }
        if (null == chartHistoryCache) {
        	log.info("Creating new chartHistoryCache");
        	chartHistoryCache = new ChartHistoryCache();
        	chartsUpdater.addListener(chartHistoryCache);
        	objectShare.put("chartHistoryCache", chartHistoryCache);
        }
        servletContext.setAttribute("chartHistoryCache", chartHistoryCache);

        OpportunityCache oppCache = new OpportunityCache();
        servletContext.setAttribute(Constants.OPPORTUNITY_CACHE, oppCache);

        PastExpiriesCache expiriesCache = new PastExpiriesCache();
        servletContext.setAttribute(Constants.EXPIRIES_CACHE, expiriesCache);
        
        CurrencyRatesManagerBase.loadInvestmentRartesCache();
        
        JmxCacheCleanName = servletContext.getInitParameter("il.co.etrader.web.util.JMX_NAME");
        try {
        	JmxCacheClean = new JMXCacheClean();
        	JmxCacheClean.registerJMX(JmxCacheCleanName);
        } catch (Exception e) {
        	log.error("Can't register JmxCacheClean.", e);
		}
        
        JmxCacheCleanNameParts = servletContext.getInitParameter("il.co.etrader.jmx.JMX_NAME_PART_CACHE");
        try {
        	JmxCacheCleanParts = new JMXCacheCleanParts();
        	JmxCacheCleanParts.registerJMX(JmxCacheCleanNameParts);
        } catch (Exception e) {
        	log.error("Can't register JmxCacheCleanPart!!!", e);
		}
        
		String contactPointsStr = servletContext.getInitParameter("com.copyop.nodes.CONTACT_POINTS");
		if (contactPointsStr != null) {
			try {
				String[] contactPoints = contactPointsStr.split(",");
				String keyspace = servletContext.getInitParameter("com.copyop.session.keyspace");
				ManagerBase.init(contactPoints, keyspace);
			} catch (Exception e) {
				log.error("Can't initialize " + ManagerBase.class, e);
			}
			try {
				String jmxCassandraResetJMXName = servletContext.getInitParameter("com.anyoption.common.jmx.cassandra.JMX_NAME");
				jmxCassandraReset = new JMXCassandraConnectionReset();
				jmxCassandraReset.registerJMX(jmxCassandraResetJMXName);
			} catch (Exception e) {
				log.error("Cannot register jmxCassandraReset.", e);
			}
		}
		
		try { 
			UsersManagerBase.cleanAllTokens();
		} catch ( SQLException ex ) {
			log.error("Can't clean tokens:", ex);
		}
    }

    /**
     * Notification that the application is about to shutdown
     *
     * @param event
     */
    public void contextDestroyed(ServletContextEvent event) {
        super.contextDestroyed(event);

        ServletContext servletContext = event.getServletContext();

        ChartsUpdater chartsUpdater = (ChartsUpdater) servletContext.getAttribute("chartsUpdater");
		// potential problem, if only one application is stopped or reloaded the charts updater will stop and not run again
        chartsUpdater.stopChartsUpdater();

        /*
        TickerUpdater tickerUpdater = (TickerUpdater) servletContext.getAttribute(Constants.TICKER_UPDATER);
        tickerUpdater.stopTickerUpdater();
        */

        OpportunityCache oppCache = (OpportunityCache) servletContext.getAttribute(Constants.OPPORTUNITY_CACHE);
        oppCache.stopOpportunityCacheCleaner();
        
        ((PastExpiriesCache) servletContext.getAttribute(Constants.EXPIRIES_CACHE)).stopPastExpiriesCache();

        JobsScheduler js = (JobsScheduler) servletContext.getAttribute(JobsManagerBase.CONTEXT_PARAM_RUN_JOBS);
        if (null != js) {
            js.stopJobsScheduler();
        }
        
        try {
        	JmxCacheClean.unregisterJMX();
        } catch (Exception e) {
            log.error("Can't unregister JmxCacheClean from JNDI.", e);
        }
        
        try {
        	jmxCassandraReset.unregisterJMX();
        } catch (Exception e) {
        	log.error("Cannot unregister Cassandra reset.", e);
        }
        
        try {
        	boolean result = ManagerBase.closeCluster();
        	log.info("Cluster closing result: " + result);
        } catch (Exception e) {
        	log.error("Problem closing cluster", e);
        }
        
        dfMonitor.unregisterJMX();
        amqMonitor.unregisterJMX();
        dbMonitor.unregisterJMX();
        CopyOpAsyncEventSender.stop();
    }
}