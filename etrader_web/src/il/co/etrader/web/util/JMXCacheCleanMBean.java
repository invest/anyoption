package il.co.etrader.web.util;

/**
 * MBean interface implemented by the JMXCacheClean to alow managing clear objects from cache over JMX.
 *
 * @author Eyal O
 */
public interface JMXCacheCleanMBean {

	/**
	 * clear the cache for last top trades - live.
	 */
	public void clearLastTopTrades();

	public boolean init();
	public boolean reset();
}
