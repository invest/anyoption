package il.co.etrader.web.util;

import java.util.ArrayList;
import java.util.List;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;

public class FacesMessagesListener extends FacesMessageListenerAbstract {

	public List getFacesMessageList() {
		FacesContext context = FacesContext.getCurrentInstance();
		MessagesList list = (MessagesList) context.getApplication().createValueBinding(Constants.BIND_MESSAGES_LIST).getValue(context);
		return list.getList();
	}

	public void setFacesMessageList(List facesMessageList) {
		FacesContext context = FacesContext.getCurrentInstance();
        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_MESSAGES_LIST, MessagesList.class);
        MessagesList list = (MessagesList) ve.getValue(context.getELContext());
		list.setList((ArrayList) facesMessageList);
		ve.setValue(context.getELContext(), list);
	}
}