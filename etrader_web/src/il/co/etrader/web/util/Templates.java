package il.co.etrader.web.util;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

public class Templates {
	
	public static void main(String[] args) {
		traversFilePath(args[0], args[1], args[2]);
//		traversFilePath("C:/_WorkSpaces/anyoption/frontEnd/anyoptoin_minisite/", "C:/work/tools/tomcat-7.0.12/webapps/anyoption/mobile_ng/app/html/", "app/");
	}
	
	public static void traversFilePath(String filesPath, String finalDestinationPath, String prefix) {
		int pathLength = filesPath.length();
		File path = new File(filesPath);
		File[] files = path.listFiles();
		File template = new File(finalDestinationPath + "/GeneralTemplate.html");
		if(template.exists()){
			template.delete();
		}
		for (File file : files) {
			if (file.isFile()) {
				if (file.getName().endsWith(".html")) {
					concatenateHTMLTemplates(file.toString(), template, pathLength, prefix);
				}
			} else {
				recursiveTravers(file.toString(), template, pathLength, prefix);
			}
		}
	}
	
	public static void recursiveTravers(String filesPath, File template, int pathLength, String prefix) {
		File path = new File(filesPath);
		File[] files = path.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				recursiveTravers(file.toString(), template, pathLength, prefix);
			} else if (file.isFile() && file.getName().endsWith(".html")) {
				concatenateHTMLTemplates(file.toString(), template, pathLength, prefix);
			}
		}
	}
	
	public static void concatenateHTMLTemplates(String path, File template, int pathLength, String prefix) {
		File dir = new File(path);
		String fileId = "";
		if (pathLength < dir.getParent().length()) {
			fileId = dir.getParent().substring(pathLength).replace("\\", "/") +  "/";
		}
		String fileName = dir.getName();
		InputStream ins = null;
		Reader reader = null; 
		BufferedReader br = null;
		PrintWriter out = null;
		BufferedWriter bw = null;
		FileWriter fw = null;
		
		try {
			fw = new FileWriter(template, true);
		    bw = new BufferedWriter(fw);
		    out = new PrintWriter(bw);
			String s;
			ins = new FileInputStream(dir);
			reader = new InputStreamReader(ins, "UTF-8");
		    br = new BufferedReader(reader);
		    out.print("<script type=\"text/ng-template\" id=\"" + prefix + fileId + fileName + "\">");
		    while ((s = br.readLine()) != null) {
		    	if(!(s.isEmpty() || s.trim().equals("") || s.trim().equals("\n"))) {
		    		out.print(s.replace("\t", ""));
		    	}
		    }
		    out.print("</script>");
			ins.close();
			reader.close();
			br.close();
			out.close();
			bw.close();
			fw.close();
		} catch (UnsupportedEncodingException uee) {
			uee.printStackTrace();
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			try {
				if(ins!=null){
					ins.close();
				}
				if(reader!=null) {
					reader.close();
				}
				if(br!=null){
					br.close();
				}
				if(out!=null){
					out.close();
				}
				if(bw!=null){
					bw.close();
				}
				if(fw!=null){
					fw.close();
				}
			} catch (IOException ioe) {
				
			}
		}
	}

}