package il.co.etrader.web.util;

import il.co.etrader.util.CommonUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public class PasswordRecoveryFieldsValidator implements Validator {

	private static final  String REG_EXP_NUMBER_ONLY = "^[0-9]*$";
	private final static String numbersOnlyError = "CMS.error.numbers.only";
	private final static String lessThanMinimum = "CMS.error.less.than.minimum";
	private final static String moreThanMaximum = "CMS.error.more.than.maximum";

	@Override
	public void validate(FacesContext context, UIComponent uIComponent,
			Object inputValue) throws ValidatorException {
		String value = (String) inputValue;
		UIComponent uIC = uIComponent;
		String[] params = null;
		if (uIC.getId().equals("mobilePhoneCode") && null != value) {
			Pattern r = Pattern.compile(REG_EXP_NUMBER_ONLY);
			Matcher matcher = r.matcher(value);
			if (!matcher.matches()) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage(numbersOnlyError, null), null);
				context.addMessage("passwordForm:" + uIC.getId(), fm);
				return;
			}
			if (value.length() < 1) {
				params = new String[1];
				params[0] = String.valueOf(1);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage(lessThanMinimum, params), null);
				context.addMessage("passwordForm:" + uIC.getId(), fm);
				return;
			}
			if (value.length() > 4) {
				params = new String[1];
				params[0] = String.valueOf(4);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage(moreThanMaximum, params), null);
				context.addMessage("passwordForm:" + uIC.getId(), fm);
				return;
			}
		}

		if (uIC.getId().equals("mobilePhone") && null != value) {
			Pattern r = Pattern.compile(REG_EXP_NUMBER_ONLY);
			Matcher matcher = r.matcher(value);
			if (!matcher.matches()) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage(numbersOnlyError, null), null);
				context.addMessage("passwordForm:" + uIC.getId(), fm);
				return;
			}
			if (value.length() < 7) {
				params = new String[1];
				params[0] = String.valueOf(7);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage(lessThanMinimum, params), null);
				context.addMessage("passwordForm:" + uIC.getId(), fm);
				return;
			}
			if (value.length() > 20) {
				params = new String[1];
				params[0] = String.valueOf(20);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage(moreThanMaximum, params), null);
				context.addMessage("passwordForm:" + uIC.getId(), fm);
				return;
			}
		}
	}
}
