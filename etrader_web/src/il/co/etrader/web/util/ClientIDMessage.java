package il.co.etrader.web.util;

import javax.faces.application.FacesMessage;

public class ClientIDMessage {

	private String clientId;
	private FacesMessage message;

	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public FacesMessage getMessage() {
		return message;
	}
	public void setMessage(FacesMessage message) {
		this.message = message;
	}



}
