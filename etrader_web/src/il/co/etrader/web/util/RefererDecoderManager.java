package il.co.etrader.web.util;

import java.util.ArrayList;

public class RefererDecoderManager {

	private ArrayList<RefererDecoder> decoders;
	
	public RefererDecoderManager(boolean usePredefined) {
		decoders = new ArrayList<RefererDecoder>();
		if(usePredefined) {
			addDecoder(new RefererDecoder("google", "q="));
			addDecoder(new RefererDecoder("yahoo", "p="));
			addDecoder(new RefererDecoder("bing", "q="));
			addDecoder(new RefererDecoder("ask", "q="));
			addDecoder(new RefererDecoder("baidu", "wd="));
			addDecoder(new RefererDecoder("naver", "query="));
			addDecoder(new RefererDecoder("daum", "q="));
			addDecoder(new RefererDecoder("yandex", "text="));
			addDecoder(new RefererDecoder("search", "q="));
		}
	}
	
	public void addDecoder(RefererDecoder rd) {
		decoders.add(rd);
	}
	
	public String decodeAll(String token) {
		for(RefererDecoder rd : decoders) {
			String query = rd.decode(token);
			if(query != null && query.length()>0){
				// there should be only one match
				return query;
			}
		}
		// no match
		return null;
	}

	public ArrayList<RefererDecoder> getDecoders() {
		return decoders;
	}
	
}
