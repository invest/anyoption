package il.co.etrader.web.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

public class JsonTexts {
	
	public static void main(String[] args) {
		traversFilePath(args[0], args[1]);
	  }
	
	public static void traversFilePath(String filesPath, String finalDestinationPath) {
		File path = new File(filesPath);
		FilenameFilter textFilter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				String lowercaseName = name.toLowerCase();
				if (lowercaseName.endsWith(".properties")) {
					return true;
				} else {
					return false;
				}
			}
		};
		File[] files = path.listFiles(textFilter);
		for (File file : files) {
			propertyFileToJson(file.toString(), finalDestinationPath);
		}
	}
	
	public static void propertyFileToJson(String path, String finalPath) {
		File dir = new File(path);
		String pathname = finalPath;
		String fileName = dir.getName();
		int index = fileName.lastIndexOf(".");
		fileName = fileName.substring(0, index);
		fileName += ".json";
		pathname += fileName;
		File finalFile = new File(pathname);
		InputStream ins = null;
		Reader rrr = null; 
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
		System.out.println(fileName);
		String s="";
		String key="";
		String value="";
		try {
			ins = new FileInputStream(dir);
			rrr = new InputStreamReader(ins, "UTF-8");
		    br = new BufferedReader(rrr);
		    String json = "{";
		    while ((s = br.readLine()) != null) {
		    	if(!(s.isEmpty() || s.trim().equals("") || s.trim().equals("\n"))) {
		    	if(s.startsWith("#")){
		    		if(s.startsWith("#stop including in json file")){
		    			break;
		    		} else if(s.startsWith("#{")){
		    			s = s.replace("#{","\"");
		    			s = s.replace("#","\"");
		    			json += s + ":{";
		    		} else if (s.equals("#}#")){
		    			json = json.substring(0, json.length()-1);
		    			json += "},";
		    		}else {
		    			continue;
		    		}
		    	} else {
		    		int equalsIndex = s.indexOf("=");
		    		if (equalsIndex < 0) {
		    			continue;
		    		}
		    		key = s.substring(0, equalsIndex);
		    	    value = s.substring(equalsIndex + 1, s.length());
		    		value = value.replace("\"", "&quot;");
		    		value = value.replace("\\", "\\\\");
		    		value = value.replace("	", "");
		    		json += "\""+ key +"\":\""+ value + "\",";
		    	}
		    }
		    }
		    json = json.substring(0, json.length()-1);
			json += "}";
			sb.append(json);
			
			BufferedWriter writer = new BufferedWriter(
										new OutputStreamWriter(
												new FileOutputStream(finalFile), "UTF-8"));
			writer.write(sb.toString());
			writer.close();
			ins.close();
			rrr.close();
			br.close();
		} catch (UnsupportedEncodingException uee) {
			uee.printStackTrace();
			System.out.println(fileName+": "+s);
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
			System.out.println(fileName+": "+s);
		} catch (IOException ioe) {
			ioe.printStackTrace();
			System.out.println(fileName+": "+s);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(fileName+": "+s+" key: "+key+" value: "+value);
		}
	}

}
