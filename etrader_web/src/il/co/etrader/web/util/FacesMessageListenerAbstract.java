package il.co.etrader.web.util;

/*
 * Copyright 2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import org.apache.log4j.Logger;

/**
 * This class is registered as a listener to all the JSF phases. <br>
 * <br>
 * When the phaseId is INVOKE_APPLICATION, then AFTER that phase we take all
 * FacesMessage objects from the JSF context and we store them in some temporal
 * thread-safe storage (e.g. the HttpSession). <br>
 * <br>
 * Then again, when the phaseId is RENDER_RESPONSE, then BEFORE that phase we
 * take from the temporary storage the list of FacesMessage objects that could
 * have been stored in a previous phase (if any), and reinject them as messages
 * in the current FacesContext object. <br>
 * <br>
 * In order for this listener to work, it must be extended providing the way to
 * get and set the temporal list of JSF messages, through implementing the two
 * abstract methods provided. <br>
 * <br>
 * This mechanism is strictly necessary, for example, when doing a redirect from
 * one JSF view to another, as JSF messages are lost during the redirection,
 * because the FacesContext object is removed and created again.
 *
 * @see http://www.theserverside.com/articles/article.tss?l=RedirectAfterPost
 * @author Enrique Medina
 * @version $ $Date: 2008-01-15 15:18:41 $
 */
public abstract class FacesMessageListenerAbstract implements PhaseListener {

	private static final Logger log = Logger.getLogger(FacesMessageListenerAbstract.class);

	// Abstract methods to get and set a list of JSF messages.
	public abstract List getFacesMessageList();

	public abstract void setFacesMessageList(List facesMessageList);

	// Before rendering, take out of the temporary storage all the messages.
	public void beforePhase(PhaseEvent event) {

			if (event.getPhaseId() == PhaseId.RENDER_RESPONSE) {
				FacesContext facesContext = event.getFacesContext();

				Iterator iter = facesContext.getMessages();
			    while (iter.hasNext()) {
			         iter.remove();
			    }

				List messages = this.getFacesMessageList();

				if (messages != null) {
					for (Iterator it = messages.iterator(); it.hasNext();) {
						ClientIDMessage msg = (ClientIDMessage) it.next();

						facesContext.addMessage(msg.getClientId(), msg.getMessage());
					}

					this.setFacesMessageList(null);
				}
			}

	}

	// After invoking application logic (actions), save all the
	// messages to a temporary storage.
	public void afterPhase(PhaseEvent event) {
		if (event.getPhaseId() == PhaseId.INVOKE_APPLICATION) {
			FacesContext facesContext = event.getFacesContext();
			List messages = new ArrayList();

			Iterator it = facesContext.getClientIdsWithMessages();

			while(it.hasNext()) {
				String id = (String) it.next();
				Iterator imsg = null;
				imsg = facesContext.getMessages(id);

				while(imsg.hasNext()) {
					ClientIDMessage msg = new ClientIDMessage();
					msg.setClientId(id);
					msg.setMessage((FacesMessage) imsg.next());

					messages.add(msg);
				}
			}

			this.setFacesMessageList(messages);
		}
	}

	// Listen in all the JSF phases.
	public PhaseId getPhaseId() {
		return PhaseId.ANY_PHASE;
	}

}