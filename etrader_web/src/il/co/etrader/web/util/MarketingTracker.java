//package il.co.etrader.web.util;
//
//import il.co.etrader.web.bl_managers.ApplicationData;
//
//import java.sql.SQLException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.TimeZone;
//
//import javax.servlet.http.Cookie;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.base.MarketingTracking;
//import com.anyoption.common.beans.base.MarketingTrackingCookieDynamic;
//import com.anyoption.common.beans.base.MarketingTrackingCookieStatic;
//import com.anyoption.common.managers.MarketingTrackingDAOManager;
//import com.anyoption.common.util.CommonUtil;
//import com.anyoption.common.util.ConstantsBase;
//
//public class MarketingTracker {
//
//	private static final Logger log = Logger.getLogger(MarketingTracker.class);
//	
//	private static final String MARKETING_TRACKING_STATIC_COOKIE = "smt";
//	private static final String DELETED_COOKIE_VALUES = "deleted";
//	private static final String DEFAUL_STATIC_COOKIE = "dfs";
//	private static final long STATIC = 0;
//	private static final long DYNAMIC = 1;
//	private static int FOURTY_FIVE_CHECK_SUM = 45;
//	
//	private static String defaultCookie = "";
//	private static String requestHeadCookie = "";
//			
//	private static MarketingTrackingCookieStatic getStaticCookiePart(Cookie marketingTrackerCookie, HttpServletRequest request, HttpServletResponse response) {
//
//		MarketingTrackingCookieStatic mtcStatic = new MarketingTrackingCookieStatic();
//		mtcStatic = getStaticParsedValue(marketingTrackerCookie.getValue());		
//		if (mtcStatic == null){
//			mtcStatic = setDeafultMarketingCookie(request, response);
//			log.info("Marketing Tracker getStaticCookiePart set Default Value:" + mtcStatic + " and Header cookies value:" + requestHeadCookie );
//		}		
//		return mtcStatic;
//	}
//	
//	private static MarketingTrackingCookieStatic getStaticParsedValue(String staticPartHex){		
//		MarketingTrackingCookieStatic mtcStatic = new MarketingTrackingCookieStatic();
//		String staticPart = null;
//		try {
//			String decodedVal = new String(CommonUtil.hexStringToByteArray(staticPartHex), "UTF-8"); 
//			staticPart = decodedVal;
//			int msStaticParseIndex = staticPart.indexOf("MS{");			
//			
//			mtcStatic.setMs(staticPart.substring(msStaticParseIndex + 3, staticPart.indexOf("^CS{")));
//			mtcStatic.setCs(staticPart.substring(staticPart.indexOf("^CS{") + 4, staticPart.indexOf("^HTTP{")));
//			mtcStatic.setHttp(staticPart.substring(staticPart.indexOf("^HTTP{") + 6, staticPart.indexOf("^DP{")));
//			mtcStatic.setDp(staticPart.substring(staticPart.indexOf("^DP{") + 4, staticPart.indexOf("^TS{")));
//
//			String ts = staticPart.substring(staticPart.indexOf("^TS{") + 4, staticPart.length());
//			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd/MM/yy");
//			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
//			mtcStatic.setTs(sdf.parse(ts));
//		} catch (Exception e) {
//			log.info("Marketing Tracker Catch when getStaticParsedValue with value:" + staticPartHex);
//			mtcStatic = null;
//		}
//		return mtcStatic;
//	}	
//	
//	private static MarketingTrackingCookieDynamic getDynamicParsedValue(String clickValue) {
//
//		MarketingTrackingCookieDynamic mtcDynamic = new MarketingTrackingCookieDynamic();
//		String td = "";
//		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd/MM/yy");
//		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
//		try {
//			String dynamic = new String(CommonUtil.hexStringToByteArray(clickValue), "UTF-8");
//			mtcDynamic.setHttp(dynamic.substring(dynamic.indexOf("HTTP{") + 5, dynamic.indexOf("^CD{")));
//			mtcDynamic.setCd(dynamic.substring(dynamic.indexOf("^CD{") + 4, dynamic.indexOf("^TD{")));
//
//			td = dynamic.substring(dynamic.indexOf("^TD{") + 4, dynamic.indexOf("^DP{"));
//			mtcDynamic.setTd(sdf.parse(td));
//			mtcDynamic.setDp(dynamic.substring(dynamic.indexOf("^DP{") + 4, dynamic.length()));
//		} catch (Exception e) {
//			log.info("Marketing Tracker Catch when getDynamicParsedValue with Header cookies value:" + requestHeadCookie + " and cookie value:" + clickValue);
//			mtcDynamic = null;
//		}
//		return mtcDynamic;
//	}
//	
//	private static MarketingTrackingCookieStatic setDeafultMarketingCookie(HttpServletRequest request, HttpServletResponse response){
//		MarketingTrackingCookieStatic mtcStatic = new MarketingTrackingCookieStatic();
//		try{			
//			HttpSession session = request.getSession();
//			String mId = DEFAUL_STATIC_COOKIE + session.getId();			
//			String combId = (String) request.getParameter(Constants.COMBINATION_ID);
//			if (null == combId) {
//				combId = Long.toString(ApplicationData.getSkinById(ApplicationData.getSkinId(session, request)).getDefaultCombinationId());;
//			}
//			String dynamicParam = (String) request.getParameter(Constants.DYNAMIC_PARAM);
//			String httpRefferer = (String) request.getHeader(Constants.HTTP_REFERE);
//			Date date = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime();
//			String dateForDisplay = new SimpleDateFormat("HH:mm:ss dd/MM/yy").format(date);
//			
//			mtcStatic.setMs(mId);
//			mtcStatic.setCs(combId);
//			mtcStatic.setHttp(httpRefferer);
//			mtcStatic.setDp(dynamicParam);
//			mtcStatic.setTs(date);
//	
//			String params = "MS{" + mId + "^CS{" + combId + "^HTTP{" + httpRefferer + "^DP{" + dynamicParam + "^TS{" + dateForDisplay;
//			params = CommonUtil.byteArrayToHexString(params.getBytes("UTF-8"));
//			deleteMarketingTrackerCookie(request, response);
//			CommonUtil.addCookie(MARKETING_TRACKING_STATIC_COOKIE, params, response, MARKETING_TRACKING_STATIC_COOKIE);
//			if (log.isDebugEnabled()) {
//				log.debug("Marketing Tracker Cookie create default cookie with params : " + params);
//			}
//			setCookieInRequestAttribute(request, params);
//		} catch (Exception e) {
//			log.error("Marketing Tracker setDeafultMarketingCookie", e);
//		}		
//		return mtcStatic;		
//	}	
//	
//	private static ArrayList<MarketingTracking> getMarketingTrackerList(Cookie marketingTracker, HttpServletRequest request, HttpServletResponse response, 
//			long contactId, long userId, long type, String midByUserId) {
//		
//		ArrayList<MarketingTracking> list = new ArrayList<MarketingTracking>();
//		try {			
//			MarketingTrackingCookieStatic mtcStatic = getStaticCookiePart(marketingTracker, request, response);
//			if(mtcStatic != null){
//				// Add Activity				
//					MarketingTracking mtActivity = new MarketingTracking();
//					mtActivity.setmId(mtcStatic.getMs());
//					mtActivity.setCombinationId(new Long(mtcStatic.getCs()));
//					mtActivity.setTimeStatic(mtcStatic.getTs());
//					mtActivity.setHttpReferer(mtcStatic.getHttp());
//					mtActivity.setDyanmicParameter(mtcStatic.getDp());
//	
//					mtActivity.setCombinationIdDynamic(0);
//					mtActivity.setTimeDynamic(Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime());
//					mtActivity.setHttpRefererDynamic("");
//					mtActivity.setDyanmicParameterDynamic("");
//					mtActivity.setContactId(contactId);
//					mtActivity.setUserId(userId);
//					mtActivity.setMarketingTrackingActivityId(type);
//	
//					list.add(mtActivity);
//			}
//		} catch (Exception e) {
//			log.error("Marketing Tracker getMarketingTrackerList with Header cookies value:" + requestHeadCookie + " and cookie : " + marketingTracker.getValue() + " Exc: ", e);
//		}
//		return list;
//	}
//	
//	private static ArrayList<MarketingTracking> getMarketingTrackerListForClick(String staticPart, String dynamicPart) {		
//		ArrayList<MarketingTracking> list = new ArrayList<MarketingTracking>();
//		try {			
//			MarketingTrackingCookieStatic mtcStatic = getStaticParsedValue(staticPart);
//			MarketingTrackingCookieDynamic mtcDynamic = getDynamicParsedValue(dynamicPart);			
//			if(mtcStatic != null){				
//				MarketingTracking mtS = new MarketingTracking();
//				mtS.setmId(mtcStatic.getMs());
//				mtS.setCombinationId(new Long(mtcStatic.getCs()));
//				mtS.setTimeStatic(mtcStatic.getTs());
//				mtS.setHttpReferer(mtcStatic.getHttp());
//				mtS.setDyanmicParameter(mtcStatic.getDp());
//
//				if (mtcDynamic != null){									
//				mtS.setCombinationIdDynamic(new Long(mtcDynamic.getCd()));
//				mtS.setTimeDynamic(mtcDynamic.getTd());
//				mtS.setHttpRefererDynamic(mtcDynamic.getHttp());
//				mtS.setDyanmicParameterDynamic(mtcDynamic.getDp());
//				mtS.setContactId(0);
//				mtS.setUserId(0);
//				mtS.setMarketingTrackingActivityId(Constants.MARKETING_TRACKING_CLICK);
//				} else {
//					mtS.setMarketingTrackingActivityId(-1);
//				}
//				list.add(mtS);					
//			}
//		} catch (Exception e) {
//			log.error("Marketing Tracker getMarketingTrackerListForClick with Static part:" + staticPart + " and Dynamic part : " + dynamicPart + " Exc: ", e);
//		}
//		return list;
//	}
//	
//	private static boolean afterFourtyFiveDays(Cookie marketingTracker, HttpServletRequest request, HttpServletResponse response) {
//		long daysBetween = 0;
//		boolean result = false;
//		try {
//			MarketingTrackingCookieStatic mtcStatic = getStaticCookiePart(marketingTracker, request, response);
//			Date date = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime();
//			long millsPerDay = 1000 * 60 * 60 * 24;
//			long d2 = date.getTime() / millsPerDay;
//			long d1 = mtcStatic.getTs().getTime() / millsPerDay;
//			daysBetween = d2 - d1;
//			if (daysBetween > FOURTY_FIVE_CHECK_SUM) {
//				if (!MarketingTrackingDAOManager.isHaveActivity(mtcStatic.getMs())) {
//					result = true;
//				}
//			}
//		} catch (Exception e) {
//			log.error(
//					"Marketing Tracker afterFourtyFiveDays with Header cookies value:" + requestHeadCookie + " and cookie : " + marketingTracker.getValue() + " Exc: ", e);
//		}
//		return result;
//	}
//	
//	private static void checkCookieAfterLogin(HttpServletRequest request, HttpServletResponse response, String midByUserId, Cookie marketingTracker, long contactId, long userId) {
//		String newStaticRow = "";
//		try {
//			MarketingTrackingCookieStatic mtcStatic = getStaticCookiePart(marketingTracker, request, response);			
//			if (!midByUserId.equals(mtcStatic.getMs())) {
//				mtcStatic.setMs(midByUserId);
//				String dateForDisplay = new SimpleDateFormat("HH:mm:ss dd/MM/yy").format(mtcStatic.getTs());
//				newStaticRow = "MS{" + mtcStatic.getMs() + "^CS{"+ mtcStatic.getCs() + "^HTTP{" + mtcStatic.getHttp() + "^DP{" + mtcStatic.getDp() + "^TS{" + dateForDisplay;
//				newStaticRow = CommonUtil.byteArrayToHexString(newStaticRow.getBytes("UTF-8"));
//				deleteMarketingTrackerCookie(request, response);
//				CommonUtil.addCookie(MARKETING_TRACKING_STATIC_COOKIE, newStaticRow, response, MARKETING_TRACKING_STATIC_COOKIE);
//				setCookieInRequestAttribute(request, newStaticRow);
//				// Add record for noExist users														
//				ArrayList<MarketingTracking> list = getMarketingTrackerListForClick(newStaticRow, null);
//				if (list.size() > 0) {
//					list.get(0).setContactId(contactId);
//					list.get(0).setUserId(userId);
//					MarketingTrackingDAOManager.insertMarketingTracking(list);								
//				}
//				
//			}
//		} catch (Exception e) {
//			log.error("Marketing Tracker checkCookieAfterLogin with Header cookies value:" + requestHeadCookie + " and cookie : " + marketingTracker.getValue() + " Exc: ", e);
//		}
//	}
//	
//	private static Cookie getMarketingTracker(HttpServletRequest request, String cookieName) {
//		
//		Cookie marketingTracker = null;
//		Cookie[] cs = request.getCookies();
//		try {
//			if (cs != null) {
//
//				for (int i = 0; i < cs.length; i++) {
//					if (cs[i].getName().equals(cookieName)) {
//						marketingTracker = cs[i];
//					}
//				}
//			}
//		} catch (Exception e) {
//			log.error("Marketing Tracker getMarketingTracker :" + requestHeadCookie  +" Exc: ", e);
//		}
//		return marketingTracker;
//	}
//	
//	private static void createMarketingTrackerCookie(HttpServletRequest request, HttpServletResponse response) {
//		String params = getParams(request, STATIC);
//		CommonUtil.addCookie(MARKETING_TRACKING_STATIC_COOKIE, params, response, MARKETING_TRACKING_STATIC_COOKIE);
//		if (log.isDebugEnabled()) {
//			log.debug("Marketing Tracker Cookie create with params : " + params);
//		}
//		setCookieInRequestAttribute(request, params);		
//	}
//	
//	private static void setCookieInRequestAttribute(HttpServletRequest request, String params) {
//		Cookie cookie = new Cookie(MARKETING_TRACKING_STATIC_COOKIE, null);		
//		try {
//			Cookie mtCookie = new Cookie(MARKETING_TRACKING_STATIC_COOKIE, params);
//			mtCookie.setPath("/");
//			mtCookie.setMaxAge(365 * 24 * 60 * 60 * 1000); // about year
//			// sub domain
//			String property = CommonUtil.getConfig(ConstantsBase.COOKIE_DOMAIN, null);
//			if (null != property) {
//				mtCookie.setDomain(property);
//			}			
//			cookie = mtCookie;
//		} catch (Exception e) {
//			log.error("Marketing Tracker setCookieInRequestAttribute with params : " + params , e);
//		}
//		request.setAttribute(MARKETING_TRACKING_STATIC_COOKIE, cookie);
//	}
//	
//	private static String getParams(HttpServletRequest request, long type) {
//		String params = null;
//		try{			
//			HttpSession session = request.getSession();
//			String mId = defaultCookie + session.getId();			
//			String combId = (String) request.getParameter(Constants.COMBINATION_ID);
//			if (null == combId) {
//				combId =Long.toString(ApplicationData.getSkinById(ApplicationData.getSkinId(session, request)).getDefaultCombinationId());
//			}
//			String dynamicParam = (String) request.getParameter(Constants.DYNAMIC_PARAM);
//			String httpRefferer = (String) request.getHeader(Constants.HTTP_REFERE);
//			Date date = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime();
//			String dateForDisplay = new SimpleDateFormat("HH:mm:ss dd/MM/yy").format(date);
//	
//			if (type == STATIC) {
//				params = "MS{" + mId + "^CS{" + combId + "^HTTP{" + httpRefferer + "^DP{" + dynamicParam + "^TS{" + dateForDisplay;
//			} else {
//				params = "HTTP{" + httpRefferer + "^CD{" + combId + "^TD{" + dateForDisplay + "^DP{" + dynamicParam;
//	
//			}
//			params = CommonUtil.byteArrayToHexString(params.getBytes("UTF-8"));			
//			
//		} catch (Exception e) {
//			log.error("Marketing Tracker getParams", e);
//		}
//		return params;
//	}
//	
//	private static void deleteMarketingTrackerCookie(HttpServletRequest request, HttpServletResponse response) {				
//		Cookie[] cs = request.getCookies();
//		try {
//			if (cs != null) {
//
//				for (int i = 0; i < cs.length; i++) {					
//					if (cs[i].getName().contains(MARKETING_TRACKING_STATIC_COOKIE)){
//						cs[i].setPath("/");
//						// sub domain
//						String property = CommonUtil.getConfig(ConstantsBase.COOKIE_DOMAIN, null);
//						if (null != property) {
//							cs[i].setDomain(property);
//						}
//						cs[i].setValue(DELETED_COOKIE_VALUES);
//						CommonUtil.deleteCookie(cs[i], response);					
//					}
//				}
//			}
//		} catch (Exception e) {
//			log.error("Marketing Tracker getMarketingTracker :" + requestHeadCookie  +" Exc: ", e);
//		}
//	}	
//	
//	private static void deleteOldCookie(HttpServletRequest request, HttpServletResponse response) {
//		String[] cookiesForDelete = { "mtc", "TRC" };		
//		for (String cookieName : cookiesForDelete) {
//			Cookie oldCookie = getMarketingTracker(request, cookieName);
//			try {
//				if (oldCookie != null) {
//					oldCookie.setPath("/");
//					// sub domain
//					String property = CommonUtil.getConfig(ConstantsBase.COOKIE_DOMAIN, null);
//					if (null != property) {
//						oldCookie.setDomain(property);
//					}
//					oldCookie.setValue("");
//					CommonUtil.deleteCookie(oldCookie, response);
//				}
//			} catch (Exception e) {
//				log.error("Marketing Tracker deleteOldCookie: ", e);
//			}
//		}
//	}	
//	
//	private static void replaceMarketingTrackerCookieAfterDaysCheck(Cookie marketingTracker, HttpServletRequest request, HttpServletResponse response) throws SQLException {				
//		MarketingTrackingCookieStatic staticPartFromCookie = getStaticCookiePart(marketingTracker, request, response);
//		MarketingTrackingCookieStatic staticPartFromDB = MarketingTrackingDAOManager.getMarketingTrackingCookieStatic(staticPartFromCookie.getMs());
//		if (staticPartFromDB != null) {
//			String dateForDisplay = new SimpleDateFormat("HH:mm:ss dd/MM/yy").format(staticPartFromDB.getTs());
//			String params = "MS{" + staticPartFromDB.getMs() + "^CS{" + staticPartFromDB.getCs() + "^HTTP{" + staticPartFromDB.getHttp() + "^DP{" + staticPartFromDB.getDp() + "^TS{" + dateForDisplay;
//			try {
//				params = CommonUtil.byteArrayToHexString(params.getBytes("UTF-8"));
//			} catch (Exception e) {
//				log.error("Marketing Tracker byteArrayToHexString ", e);
//			}
//			deleteMarketingTrackerCookie(request, response);
//			CommonUtil.addCookie(MARKETING_TRACKING_STATIC_COOKIE, params, response, MARKETING_TRACKING_STATIC_COOKIE);
//			if (log.isDebugEnabled()) {
//				log.debug("Marketing Tracker Cookie replaceMarketingTrackerCookieAfterActivity params : " + params);
//			}
//			setCookieInRequestAttribute(request, params);
//		} else {
//			deleteMarketingTrackerCookie(request, response);
//			createMarketingTrackerCookie(request, response);
//		}
//	}
//	
//	private static void replaceMarketingTrackerCookieAfterActivity(HttpServletRequest request, HttpServletResponse response, String mId) throws SQLException {		
//		
//		MarketingTrackingCookieStatic staticPart = MarketingTrackingDAOManager.getMarketingTrackingCookieStatic(mId);
//		if (staticPart != null) {
//			String dateForDisplay = new SimpleDateFormat("HH:mm:ss dd/MM/yy").format(staticPart.getTs());
//			String params = "MS{" + staticPart.getMs() + "^CS{" + staticPart.getCs() + "^HTTP{" + staticPart.getHttp() + "^DP{" + staticPart.getDp() + "^TS{" + dateForDisplay;
//			
//			try {
//				params = CommonUtil.byteArrayToHexString(params.getBytes("UTF-8"));
//			} catch (Exception e) {
//				log.error("Marketing Tracker byteArrayToHexString ", e);
//			}
//			deleteMarketingTrackerCookie(request, response);
//			CommonUtil.addCookie(MARKETING_TRACKING_STATIC_COOKIE, params, response, MARKETING_TRACKING_STATIC_COOKIE);
//			if (log.isDebugEnabled()) {
//				log.debug("Marketing Tracker Cookie replaceMarketingTrackerCookieAfterActivity params : " + params);
//			}
//			setCookieInRequestAttribute(request, params);
//		}
//	}	
//
//	private static void addClickMarketingTrackerCookie(Cookie marketingTracker, HttpServletRequest request) throws SQLException {		
//		String staticParts  = marketingTracker.getValue();
//		String dynamicParts = getParams(request, DYNAMIC);
//		insertMarketingTrackerClick(staticParts, dynamicParts);
//	}	
//
//
//	public static MarketingTracking getStaticMarketingTracker(HttpServletRequest request,HttpServletResponse response) {
//		MarketingTracking mtStatic = new MarketingTracking();
//		Cookie marketingTracker = getMarketingTracker(request, MARKETING_TRACKING_STATIC_COOKIE);	
//		try { 			
//			MarketingTrackingCookieStatic mtcStatic = getStaticCookiePart(marketingTracker, request, response);
//			
//			mtStatic.setmId(mtcStatic.getMs());
//			mtStatic.setCombinationId(new Long(mtcStatic.getCs()));
//			mtStatic.setHttpReferer(mtcStatic.getHttp());
//			mtStatic.setDyanmicParameter(mtcStatic.getDp());
//		} catch (Exception e) {
//			log.error("Marketing Tracker getStaticMarketingTracker with Header cookies value:" + requestHeadCookie + " and cookie : " + marketingTracker.getValue() + " Exc: ", e);
//		}
//		return mtStatic;		
//	}
//	
//	public static void checkMarketingTrackerCookie(HttpServletRequest request, HttpServletResponse response, boolean doNotAddClickIfCookieExist) {
//		Cookie marketingTracker = getMarketingTracker(request, MARKETING_TRACKING_STATIC_COOKIE);
//		try {
//			requestHeadCookie = request.getHeader("Cookie");
//			deleteOldCookie(request, response);
//
//			if (null != marketingTracker) {
//				
//				if (marketingTracker.getValue().contains((DELETED_COOKIE_VALUES))){
//					marketingTracker = (Cookie) request.getAttribute(MARKETING_TRACKING_STATIC_COOKIE);
//				}
//				
//				if (!doNotAddClickIfCookieExist) {
//					if (log.isDebugEnabled()) {
//						log.debug("checkMarketingTrackerCookie  Begin addClickMarketingTrackerCookie: ");
//					}
//					addClickMarketingTrackerCookie(marketingTracker, request);
//				}
//
//				if (log.isDebugEnabled()) {
//					log.debug("checkMarketingTrackerCookie  Begin fourtyFiveDaysCheck: ");
//				}
//				if (afterFourtyFiveDays(marketingTracker, request, response)) {
//					replaceMarketingTrackerCookieAfterDaysCheck(marketingTracker, request, response);
//				}
//			} else {
//				if (log.isDebugEnabled()) {
//					log.debug("checkMarketingTrackerCookie  Begin createMarketingTrackerCookie: ");
//				}
//				createMarketingTrackerCookie(request, response);
//			}
//		} catch (Exception e) {
//			log.error(
//					"Marketing Tracker checkMarketingTrackerCookie with Header cookies value:" + requestHeadCookie + " and cookie :  " + marketingTracker.getValue() + " Exc: ", e);
//		}
//	}
//	
//	public static void insertMarketingTrackerClick(String staticPart, String dynamicPart) throws SQLException {				
//		try {										
//			ArrayList<MarketingTracking> list = getMarketingTrackerListForClick(staticPart, dynamicPart);
//			if (list.size() > 0) {
//				MarketingTrackingDAOManager.insertMarketingTracking(list);								
//			}
//		} catch (Exception e) {
//			log.error("Marketing Tracker insertMarketingTrackerClick with Static part : " + staticPart + " and Dynamic part : " + dynamicPart + " Exc: ", e);
//		}
//	}
//
//	public static void insertMarketingTracker(HttpServletRequest request, HttpServletResponse response, long contactId, long userId, long type)
//			throws SQLException {
//		
//		Cookie marketingTracker = getMarketingTracker(request, MARKETING_TRACKING_STATIC_COOKIE);
//		boolean cookieForChange = false;
//		try {
//			requestHeadCookie = request.getHeader("Cookie");
//			deleteOldCookie(request, response);
//			
//			if (marketingTracker ==  null) {				
//				defaultCookie = "DEF";
//				createMarketingTrackerCookie(request, response);
//				defaultCookie = "";
//				if (log.isDebugEnabled()) {
//					log.debug("InsertMarketingTracker add default cookie and set Atrribute");
//				}
//			} else if(marketingTracker.getValue().contains((DELETED_COOKIE_VALUES))) {
//				marketingTracker = (Cookie) request.getAttribute(MARKETING_TRACKING_STATIC_COOKIE);
//			}			
//			
//			String midByUserId = null;									
//			if (type == Constants.MARKETING_TRACKING_LOGIN) {
//				midByUserId = MarketingTrackingDAOManager.getMidByUserId(userId);
//				checkCookieAfterLogin(request, response, midByUserId, marketingTracker, contactId, userId ); 
//			}
//			
//			if (log.isDebugEnabled()) {
//				log.debug("InsertMarketingTracker try to getMarketingTrackerList with type: " + type);
//			}
//						
//			ArrayList<MarketingTracking> list = getMarketingTrackerList(marketingTracker, request, response, contactId, userId, type, midByUserId);
//			if (list.size() > 0 && type != Constants.MARKETING_TRACKING_LOGIN) {
//				cookieForChange = afterFourtyFiveDays(marketingTracker, request, response );
//				
//				MarketingTrackingDAOManager.insertMarketingTracking(list);
//				if (cookieForChange) {					
//					replaceMarketingTrackerCookieAfterActivity(request, response, list.get(0).getmId());
//				}
//				
//			}
//		} catch (Exception e) {
//			log.error("Marketing Tracker insertMarketingTracker with Header cookies value: " + requestHeadCookie + " and cookie : " + marketingTracker.getValue() + " Exc: ", e);
//		}
//	}
//}
