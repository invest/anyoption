package il.co.etrader.web.util;

import java.sql.SQLException;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.anyoption.common.managers.UsersManagerBase;

import org.apache.log4j.Logger;

public class WebSessionListener implements HttpSessionListener {
	private static Logger log = Logger.getLogger(WebSessionListener.class);
	
	@Override
	public void sessionCreated(HttpSessionEvent sessionEvent) {
		
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent sessionEvent) {
		HttpSession session = sessionEvent.getSession();
		
	  	if(session.getAttribute("token") != null) {
	  		log.debug("Destroying session");
    		String token = (String) session.getAttribute("token");
    		try {
				UsersManagerBase.deleteToken(token);
			} catch (SQLException e) {
				log.error("Can't delete token:" + token);
			}
	  	}
	}

}