package il.co.etrader.web.util;

import il.co.etrader.util.ConstantsBase;

public class Constants extends ConstantsBase {

    public static int PORT_HTTP=0;
    public static int PORT_HTTPS=0;

	public static final String REFERRER = "r";

	public static final String FROM_REMARKETING = "remarketing";
	public static final String FIRST_COMBINATION_ID = "f_combid";

	public static final String PRETTY_SKIN_ID_ATR = "skinFURL";
	public static final String LANGUAGE_ID = "l";
	public static final String CURRENCY_ID = "c";
	public static final String USER_LOCALE = "userLocale";
	public static final String ETRADER_REDIRECT = "redirect";
	public static final String COUNTRY_ID = "countryId";

	public static final String FIRST_DYNAMIC_PARAM = "f_dp";
	public static final String REMARKETING_DYNAMIC_PARAM = "r_dp";
	public static final String SPECIAL_CODE_PARAM = "spcCode";
	public static final String FROM_REDIRECT = "from";
	public static final String FROM_REDIRECT_ISRAEL = "israel";
	public static final String LANDING_PAGE_REDIRECT = "pageR";
	public static final String LANDING_PAGE_SHOW_MOVIE = "sMovie";
	
	public static final String TRADE_DOUBLER = "TRADEDOUBLER";
	public static final String TRADE_DOUBLER_PARAM = "tduid";
	public static final String LIVEPERSON_CHAT_ID = "lpChatId";
	public static final String CLICK_ID = "clickId";
	public static final String FAILED_DEPOSITS = "failedDeposits";
	public static final String CUSTOMER_VISIT_TYPE = "cvt";
	public static final String MOBILE_APP_VER = "appVer";

	public static final String HTTP_REFERE_LANDING = "RefererLanding";
	public static final String HTTP_REFERE_SESSION = "RefererInSession";
	public static final String HTTP_REFERE_COOKIE = "hr";
	public static final String SKIN_ID_FROM_LANDING = "sfl";
	public static final String REGISTER_FROM_CONTACTUS_LANDING = "fromCULanding";
	public static final String REGISTER_FROM_QUICK_START = "fromQuickStartHP";
	public static final String REGISTER_FROM_CONTACTUS_LANDING_FN = "fromCULandingFirstN";
	public static final String REGISTER_FROM_CONTACTUS_LANDING_LN = "fromCULandingLastN";
	public static final String REGISTER_ATTEMPT = "#{registerAttempt}";
	public static final String BIND_REMARKETING_LOGIN = "#{RemarketingLogin}";
	public static final String REDIRECTED_TO_REGISTER = "redToReg";
	public static final String LANDING_QUARY_COOKIE = "landing_quary";
	public static final String LANDING_HR_COOKIE = "landing_hr";
	public static final String LANDING_TIMEZONE_OFFSET_COOKIE = "landing_TZOffset";
	public static final String STATIC_LANDING_PAGE_CONTACT_ID = "lpContactId";
	public static final String LANDING_FIRST_VISIT_COOKIE = "landing_firstVisit";
	public static final String USER_NAME_AUTH_LOGIN = "userNameAuthLogin";
	public static final String IS_REGULATED = "regulated";
	public static final String MARKETING_TRACKER_COOKIE = "TRC";
	public static final String MTC_DELIMITER_OUT = ";";
	public static final String MTC_DELIMITER_IN = "|";
	public static final String FROM_LIVE = "FROM_LIVE";
	public static final String FROM_LIVE_PENDING = "pending";
	public static final String FROM_LIVE_TRUE = "true";
	public static final String FROM_LIVE_FALSE = "false";
	public static final String FORCE_REGULATION_COUNTRY = "countryA2";
	
	// Double click DFA
	public static final String DFA_PLACEMENT_ID = "placmentId";
	public static final String DFA_PLACEMENT_ID_VALUE = "dfaPlaId";
	public static final String DFA_CREATIVE_ID = "creativeId";
	public static final String DFA_CREATIVE_ID_VALUE = "dfaCreId";
	public static final String DFA_MACRO = "macro";
	public static final String DFA_MACRO_VALUE = "dfaMacro";

	public static final String CUSTOMER_VISIT_TYPE_DEPOSITOR = "1";
	public static final String CUSTOMER_VISIT_TYPE_NON_DEPOSITOR = "2";

	public static final String REFERRER_DESCRIPTION = "referer";
	public static final String SKIN_ID_DESCRIPTION = "skinId";
	public static final String LANGUAGE_ID_DESCRIPTION = "languageId";
	public static final String CURRENCY_ID_DESCRIPTION = "currencyId";
	public static final String COUNTRY_ID_DESCRIPTION = "countryId";
	public static final String DYNAMIC_PARAM_DESCRIPTION = "dynamicParam";
	public static final String REMARKETING_DYNAMIC_PARAM_DESCRIPTION = "remarketingDynamicParam";
	public static final String SUB_AFFILIATE_ID_DESCRIPTION = "subAffiliateId";
	public static final String SPECIAL_CODE_DESCRIPTION = "specialCode";
	public static final String VSP_CHAT_ID = "chat_id";
	public static final String IS_REGULATED_DESCRIPTION = "isRegulated";
	public static final String GUI = "GUI";

	public static final String ISRAEL_LOCALE_LANGUAGE = "iw";
	public static final String ISRAEL_LOCALE_COUNTRY = "IL";
	
	public static final String SWEDISH_LOCALE_LANGUAGE = "se";
	public static final String SWEDISH_LOCALE_COUNTRY = "SE";
	
	public static final String CHECH_LOCALE_LANGUAGE = "cs";
	public static final String CHECH_LOCALE_COUNTRY = "CZ";
	
	public static final String NORWEGIAN_LOCALE_LANGUAGE = "no";
	public static final String NORWEGIAN_LOCALE_COUNTRY = "NO";
	
	public static final String POLISH_LOCALE_LANGUAGE = "pl";
	public static final String POLISH_LOCALE_COUNTRY = "PL";	

	public static final String WEB_ROLE = "web";

	public static final String SECURE_PATH = "/pages/";

	public static final String BIND_NEW_CARD_FORM = "#{newCardForm}";
	public static final String BIND_TRANSACTION = "#{transaction}";
	public static final String BIND_WIRE = "#{wire}";
	public static final String BIND_MARKETS_LIST_FORM = "#{marketsListForm}";
	public static final String BIND_MARKETS_0100_LIST_FORM = "#{markets0100ListForm}";
    public static final String BIND_DEPOSIT_FORM = "#{depositForm}";
    public static final String BIND_WITHDRAWAL_FORM = "#{withdrawForm}";
    public static final String BIND_BONUS_USERS = "#{bonusUsers}";
    public static final String BIND_MAILBOX_USER = "#{mailBoxUser}";

	public static final String NAV_RECEIPT = "receipt";
    public static final String NAV_3D_SECURE = "threeDSecure";
    public static final String NAV_3D_SECURE_RENDER = "threeDSecureRender";
    public static final String NAV_INATEC_BANKING = "inatecBankingNav";
    public static final String NAV_ENVOY_DEPOSIT = "envoyDepositNav";
    public static final String NAV_INATEC_IFRAME_DEPOSIT = "inatecIframeDepositNav";
    public static final String NAV_ENVOY_WITHDRAW = "envoyWithdrawNav";
    public static final String NAV_PAYPAL_LOGIN = "paypalLoginNav";
    public static final String NAV_PAYPAL_SET_EXPRESS_CHECKOUT = "setExpressCheckout";
	public static final String NAV_CARDS = "cards";
	public static final String NAV_FIRST_DEPOSIT = "firstDeposit";
	public static final String NAV_OTHER_DEPOSIT = "otherDeposit";
	public static final String NAV_INVESTMENTS = "investments";
	public static final String NAV_MAIN = "main";
	public static final String NAV_MAIN_WITH_REDIRECT = "mainWithRedirect";
	public static final String NAV_TRADE_PAGE = "tradePage";
	public static final String NAV_DOCUMENT_PAGE = "userDocuments";
	public static final String NAV_LOGIN = "login";
	public static final String NAV_REGISTER = "register";
	public static final String NAV_EMPTY_BANNERS = "emptyBanners";
	public static final String NAV_EMPTY = "empty";
	public static final String NAV_EMPTY_SSL = "emptyssl";
	public static final String NAV_CASHU_DEPOSIT = "cashUDepositNav";
	public static final String NAV_CONTACTUS_NAV = "contactusNav";
	public static final String NAV_CONTACTUS_MINISITE = "ContactUs";
	public static final String NAV_DOWNLOADPAGE_CONTACTUS = "contactusDownloadPage";
	public static final String NAV_MONEYBOOKERS_DEPOSIT = "moneybookersDepositNav";
	public static final String NAV_WEBMONEY_DEPOSIT = "webMoneyDepositNav";
	public static final String NAV_APP_PAGE = "appPage";
	public static final String NAV_INDICES= "indices";
	public static final String NAV_ET_INDICES= "indices";
	public static final String NAV_ET_STOCKS= "stocks";
	public static final String NAV_ET_CURRENCIES= "currencies";
	public static final String NAV_ET_COMMODITIES= "commodities";
	public static final String NAV_ET_FOREIGNSTOCKS= "foreignstocks";
	public static final String NAV_ET_ONE_TOUCH= "oneTouch";
	public static final String NAV_ET_OPTION_PLUS= "optionPlus";
	public static final String NAV_ET_NEW_APP_PAGE= "new_app_page";
	public static final String NAV_AO_ONE_TOUCH= "oneTouch";
	public static final String NAV_AO_OPTION_PLUS= "optionPlus";
	public static final String NAV_BAROPAY_DEPOSIT = "baroPayDepositNav";
	public static final String NAV_BAROPAY_DEPOSIT_SUCCESS = "baroPayDepositSuccess";	
	public static final String NAV_BUBBLES_PAGE = "bubbles";
	public static final String NAV_DYNAMICS = "dynamics";
	public static final String NAV_LONG_TERM = "longTerm";

	public static final String NAV_AFTER_REGITSER = "afterRegister";
	public static final String NAV_FIRST_NEW_CARD = "firstNewCard";
	public static final String NAV_FIRST_NEW_CARD_REDIRECT = "firstNewCardRedirect";
	public static final String NAV_TRANSACTIONS = "transactions";
	public static final String NAV_EMPTY_MYACCOUNT = "myaccountEmpty";
	public static final String NAV_EMPTY_MYACCOUNT_WITHDRAW = "myaccountEmptyWithdrawal";
	public static final String NAV_EMPTY_MYACCOUNT_WIRE = "myaccountEmptyWire";
	public static final String NAV_EMPTY_MYACCOUNT_PAYPAL_WITHDRAWAL = "myaccountEmptyPaypal";
	public static final String NAV_EMPTY_MYACCOUNT_PERSONAL = "myaccountEmptyPersonal";
	public static final String NAV_EMPTY_MYACCOUNT_PASS = "myaccountEmptyPass";
	public static final String NAV_EMPTY_MYACCOUNT_DEPOSIT = "myaccountEmptyDeposit";
	public static final String NAV_EMPTY_MYACCOUNT_CARDS = "myaccountEmptyCards";
	public static final String NAV_MYACCOUNT_MENU = "myaccountMenu";
	public static final String NAV_PASS_OUTCOME = "passPopup";

	public static final String NAV_WITHDRAW_WIRE = "wire";
	public static final String NAV_WITHDRAW_DELTA_PAY = "withdrawDeltaPay";
	public static final String NAV_WITHDRAW_CC = "withdrawCreditCard";
	public static final String NAV_WITHDRAW = "withdraw";
	public static final String NAV_WITHDRAW_APPROVE = "withdrawApprove";
	public static final String NAV_WITHDRAW_APPROVE_MONETA = "withdrawApproveMoneta";
	public static final String NAV_WITHDRAW_APPROVE_WEBMONEY = "withdrawApproveWebmoney";
	public static final String NAV_WITHDRAW_APPROVE_CDPAY = "withdrawApproveCDPay";
	public static final String NAV_WITHDRAW_RESULT_MONETA = "withdrawResultMoneta";
	public static final String NAV_WITHDRAW_RESULT_WEBMONEY = "withdrawResultWebmoney";
	public static final String NAV_BANK_WIRE_APPROVE = "bankWireApprove";
	public static final String NAV_DELETE_CC_APPROVE = "ccDeleteApprove";
	public static final String NAV_PAYPAL_APPROVE = "payPalApprove";
	public static final String NAV_DOUBLE_SUBMI = "doubleSubmit";
	public static final String NAV_SKINS = "skins";
	public static final String NAV_CANCEL_BONUS_APPROVE = "cancelBonusApprove";
	public static final String NAV_BONUS = "bonus";
	public static final String NAV_TERMS_BONUS = "termsBonus";
	public static final String NAV_CONTACTME_SUCCESS = "contactMeSuccess";
	public static final String NAV_CONTACTME_IFRAME_SUCCESS = "contactMeIframeSuccess";
	public static final String NAV_CONTACTME_SUCCESS_WAP = "contactMeSuccessWap";
	public static final String NAV_RECEIPT_PRINT= "/jsp/pages/receipt_print";
	public static final String NAV_MAILBOX = "mailBoxNav";
	public static final String NAV_INVESTMENT_INFO = "investmentInfo";
	public static final String NAV_DEPOSIT_FAILED = "depositFailed";
	public static final String NAV_NEW_CARD_DEPOSIT_FAILED = "insertCardAndDepositFailed";
	public static final String NAV_EMAIL_AUTHENTICATION_INFO = "emailAuth";
	public static final String NAV_EMAIL_AUTHENTICATION_INFO_LANDING = "emailAuthLanding";
	public static final String NAV_AGREEMENT = "agreement";
	public static final String NAV_DEPOSIT_MENU = "depositMenu";
	public static final String NAV_USER_QUESTIONNAIRE = "userQuestionnaire";
	public static final String NAV_DELTAPAY_DEPOSIT = "deltaPayDepositNav";
	public static final String NAV_DELTAPAY_DEPOSIT_PAGE = "deltaPayDepositPageNav";	
	public static final String NAV_USER_ADDITIONAL_INFO = "userAditionalInfo";
	public static final String NAV_USER_CAP_QUESTIONNAIRE = "userCapQuestionnaire";
	
	public static final String ENUM_CONTACT_ISSUES = "contact_issues";

    public static final String JNDI_LEVELS_CACHE_PATH = "java:levelscache";

    public static final int MAX_ATTEMPTS_WITHOUT_USER = 9;

    public static final int TAX_FORM_DISPLAY_MONTH = 0;  // January

    public static final String CONTACT_ME_SUBJECT = "Contact me, ";
    public static final String CONTACT_ME_DYNMAIC_PARAM = "Dynamic Parameter: ";
    public static final String CONTACT_ME_SUBJECT_LANG = "Language: ";
    public static final String CONTACT_ME_LANG_ISRAEL = "Hebrew";

    public static final String SPAIN_COUNTRY_ID = "192";
    public static final Long ISRAEL_COUNTRY_ID = 1L;
    public static final String RUSSIA_COUNTRY_ID = "172";
    public static final String CHINA_COUNTRY_ID = "44";
    public static final int CHINA_COUNTRY_ID_INT = 44;
    public static final String TAIWAN_COUNTRY_ID = "203";
    public static final String HONG_KONG_COUNTRY_ID = "94";
    public static final String TURKEY_COUNTRY_ID = "212";
    public static final String SOUTH_KOREA_COUNTRY_ID = "191";

	public static final String CONTACTUS_DEFAULT_COUNTRY = "0";

	public static final String NAV_LAST_LEVELS = "lastlevels";
	public static final String LAST_LEVELS_PAGE = "lastLevels.jsf";
	public static final String LAST_LEVELS_ONE_TOUCH_PAGE = "lastLevelsOneTouch.jsf";

	public static final String CONSECUTIVE_INV_TIME = "consect_inv";
	public static final String MOVIE_COOKIE = "movie";
	public static final String CLEAR_FIRST_TIME_VISIT = "clearFirstTimeVisit";
	public static final String FIRST_TIME_VISIT = "firstTimeVisit";
	public static final String FROM_MOVIE = "from=movie";
	public static final String FROM_LANDING = "landing.jsf";
	public static final String FROM_MOBILE_LP = "from=mobilelp";
	public static final String TIME_FIRST_VISIT_COOKIE = "timeFirstVisit";

    public static final long INVESTMENT_MAX_LIMIT_CALCALIST = 100000;

	public static final String ERROR_PAGE_FORBIDDEN_REGISTER = "/errorPageForbiddenRegister.jsf";
    public static final String ERROR_PAGE_FORBIDDEN_REGISTER_PARAM_NETREFER = "?net=true";
    public static final String ERROR_PAGE_FORBIDDEN_REGISTER_PARAM_ISRAEL_IRAN = "?net=false";
	public static final String CONTACT_ID="contactId";

	public static final long MARKETING_PAYMENT_TYPE_CPL_REGISTRATION = 5;
	public static final long MARKETING_PAYMENT_TYPE_CPL_CONTACTME = 6;
	public static final long MARKETING_PAYMENT_TYPE_CPC = 3;
	public static final long MARKETING_PAYMENT_TYPE_CPA = 1;    // first deposit

	public static final String HTTP_PROTOCOL = "HTTP";
	public static final String HTTPS_PROTOCOL = "HTTPS";
	public static final int MAX_1T_UNITS = 40;

	public static final String IS_HAS_VERSION2_SKIN = "isHasVersion2Skin";
	public static final String NEW_SKIN_USERS_SPLIT = "svSpl";

	public static final long REGISTRATION_BONUS_NEXT_INVEST = 90;

	// Calcalist
	public static final String RANKING_TYPE_WEEKLY = "weekly";
	public static final String RANKING_TYPE_MONTHLY = "monthly";
	public static final String RANKING_RED_COLOR = "color:#990020;";



	// params in the queryString when redirecting from landing to index
	public static final String GOOGLE_PARAM_PREFIX = "utm_";
	public static final String SKIN_PARAM_PREFIX = "s=";
	public static final String GOOGLE_PARAM_ADWORDS_FOR_ANALYTICS = "gclid=";

	public static final String AO_HOME_PAGE_DEFAULT = "index.jsf";
	public static final String AO_HOME_PAGE_BANNER = "indexBanner.jsf";
	public static final String AO_HOME_PAGE_ULTIMATE_BANNER = "ultimateBanner.jsf";
	public static final String AO_HOME_PAGE_EDITORIAL_BANNER = "editorialBanner.jsf";
	public static final String AO_HOME_PAGE_EDITORIAL_2 = "editorial_2.jsf";
	public static final String AO_HOME_PAGE_ADVERTORIAL_RU = "advertorialRu.jsf";
	public static final String AO_HOME_PAGE_CONTACTUS_03_11 = "70_generic_short.jsf";  // AO_HOME_PAGE_70_GENERIC_SHORT_V1
	public static final String AO_HOME_PAGE_FULL_REGISTER_PL = "fullRegPL.jsf"; //landing page

	// parameters for show off RSS
	public static final String SH_RSS_LOCALE = "locale";
	public static final String SH_RSS_LINK = "link";
	public static final String SH_RSS_NUMBER = "num";
	public static final String SH_RSS_SHORT = "short";
	public static final String SH_RSS_SKIN = "skin";

	public static final String BONUS_NAVIGATION_PARAM = "bId";

	// mailbox parameters
	public static final String MAILBOX_PARAM_FREE_TEXT = "freeText";
	public static final String MAILBOX_PARAM_BONUS_LINK = "bonusLink";

	// bonus banner enumerators
	public static final String ENUM_BONUS_ID_CODE = "bonus_id";
	public static final String ENUM_BONUS_BANNER_ENUMERATOR = "bonus_banner";

	// tax popup enumerators
	public static final String ENUM_TAX_POPUP_CODE = "tax_popup_date";
	public static final String ENUM_TAX_POPUP_ENUMERATOR = "tax_popup";

	//birthday 4 popup enumerators
	public static final String ENUM_BIRTHDAY_POPUP_LOGIN_CODE = "birthday4_popup_date";
	public static final String ENUM_BIRTHDAY_POPUP_LOGIN_ENUMERATOR = "birthday4_popup";
	
	//new_features popup enumerators
	public static final String ENUM_NEW_FEATURES_POPUP_LOGIN_CODE = "new_features_popup_date";
	public static final String ENUM_NEW_FEATURES_POPUP_LOGIN_ENUMERATOR = "new_features_popup";	
	
	public static final String ENUM_MESSAGE_DISAPPEAR = "message_disappear";
	public static final String ENUM_MESSAGE_DISAPPEAR_CODE = "Z";
	public static final String ENUM_ONE_CLICK_DISAPPEAR = "oneClick_disappear";
	public static final String ENUM_ONE_CLICK_DISAPPEAR_CODE = "X";

	public static final String WEB_DIRECTORY = "jsp";
	public static final String MOBILE_DIRECTORY = "mobile";
	public static final String MINISITE_DIRECTORY = "mobile_ng";
	public static final String API_DIRECTORY = "API";

	public static final String GOOGLE_DP_KWORD = "kword";

	public static final long LIVEPERSON_FAILED_DEPOSITS = 2;

	public static final String ATTACHMENTS_PATH = "attachments";
	//asset index OPTION+
	public static final String OPTION_PLUS_EURO_USD_START_DATE = "22:00";
	public static final String OPTION_PLUS_EURO_USD_END_DATE = "20:15";
	public static final String OPTION_PLUS_OIL_START_DATE = "07:45";
	public static final String OPTION_PLUS_OIL_END_DATE = "20:15";
	public static final String OPTION_PLUS_SP_START_DATE = "07:15";
	public static final String OPTION_PLUS_SP_END_DATE = "21:45";
	public static final String OPTION_PLUS_DAX_START_DATE = "07:45";
	public static final String OPTION_PLUS_DAX_END_DATE = "18:45";
	public static final String OPTION_PLUS_IBEX_START_DATE = "08:15";
	public static final String OPTION_PLUS_IBEX_END_DATE = "16:15";
    public static final String OPTION_PLUS_RTS_FUT_START_DATE = "06:45";
    public static final String OPTION_PLUS_RTS_FUT_END_DATE = "14:15";
    public static final String OPTION_PLUS_AUD_USD_START_DATE = "22:00";
    public static final String OPTION_PLUS_AUD_USD_END_DATE = "20:15";
    public static final String OPTION_PLUS_KOSPI_START_DATE = "00:15";
    public static final String OPTION_PLUS_KOSPI_END_DATE = "05:45";

    public static final String MOBILE_IPHONE_CSS = "iphone";
    public static final String MOBILE_ANDROID_CSS = "android";

    public static final String MOBILE_IPHONE_PATH = "iphone";
    public static final String MOBILE_ANDROID_PATH = "android";

    public static final String SEND_LINK_TO_MOBILE_SUCCESS = "sendLinkToMobileSuccess";
    public static final String SEND_LINK_TO_MOBILE_EMAIL_SUCCESS = "sendLinkToMobileEmailSuccess";

    public static final String TICKER_UPDATER = "tickerUpdater";
    public static final String MOBILE_CONTACTUS_DOWNLOADPAGE_SUBJECT_ADDITION = " sent from mobile";
    public static final String MOBILE_CONTACTUS_APPLICATION_SUBJECT_ADDITION = " sent from mobile app";

    public static final String SHOW_NON_DEPOSITOR_POPUP = "show non depositor popup";
    public static final String NON_DEPOSITOR_POPUP_TIME = "ndpTime";
    public static final String ENUM_NON_DEP_ENUMERATOR = "non_deposit_time_milis";
    public static final String ENUM_NON_DEP_CODE = "non_depost_time";
    public static final long NON_DEPOSITOR_POPUP_TIMEOUT = 20000;
    public static final String OPPORTUNITY_CACHE = "opportunityCache";
    public static final String EXPIRIES_CACHE = "expiriesCache";

    public static final long LOGIN_FROM_REGULAR = 0;
    public static final long LOGIN_FROM_NON_DEPOSITORS = 1;

    public static final long MARKETING_SOURCE_ID_GOOGLE = 25;
    public static final long MARKETING_PAYMENT_RECIPIENT_ID_GOOGLE = 25;

    public static final int BINARY_OPTIONS_MAX_COLUMNS = 8;

    public static final String TEMPLATES_DIRECTORY = "templates";
    public static final String TEMPLATES_DIRECTORY_CHINESE = "templates_zh";
    public static final String TEMPLATES_DIRECTORY_OLD_DESIGN = "templates_old";

    public static final String TLV_RSS_URL = "tlv.rss.feed.url";
    public static final String RSS_URL = "rss.feed.url";
    public static final String ZH1_RSS_URL = "zh1.rss.feed.url";
    public static final String ZH2_RSS_URL = "zh2.rss.feed.url";
    public static final String ZH3_RSS_URL = "zh3.rss.feed.url";
    public static final String ZH4_RSS_URL = "zh4.rss.feed.url";

    public static final String LAST_OPEN_INV_0100 = "0100LastOpenInv";
	
	public static final String FILES_PATH = "files.path";
	public final static int WRITER_WEB_ID = 1;
	public static final String BANNER_NAME = "banner.name";
	public static final String BANNER_PATH = "banner.path";
	
	public static final String LOGIN_PAGE="login";
	
	public static final int LAST_TOP_TRADES = 15; // Hour, day and month.
	
	//for Academy
	public static final long ACADEMY_USER_STATUS_LOGOUT = 1;
	public static final long ACADEMY_USER_STATUS_NO_DEPOSIT = 2;
	public static final long ACADEMY_USER_STATUS_HAS_DEPOSIT = 3;
    public static final String UAGENTINFO = "uagentinfo";
    
    public static final String SESSION_0100_NEED_FEE = "0100fee";
    
    public static final String APPSFLYER_IOS_PATH 		= "appsflyerios";
    public static final String APPSFLYER_ANDROID_PATH 	= "appsflyerandroid";
    
    public static final String SESSION_IS_ET_REGULATION = "isEtRegulation";
    public static final String SESSION_IS_ET_REGULATION_DONE = "isEtRegulationDone";
    public static final String SESSION_IS_HAS_DEPOSIT = "isHasDeposit";
    public static final String SESSION_IS_SINGLE_QUEST_DONE = "isSQuestDone";
    
    public static final String SESSION_AO_REGULATION_QUESTIONNAIRE_RESULT = "aoRegQuestRes";
    
    public static final String SESSION_MIN_INVEST_BALANCE = "minInvestBalance";
    
    public static final String TRADE_BOX_GOOGLE_PATTERN = "trad.box.google.design.pattern";
    
}