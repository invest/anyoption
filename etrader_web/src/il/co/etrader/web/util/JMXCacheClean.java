package il.co.etrader.web.util;

import il.co.etrader.web.bl_managers.ApplicationData;

import java.lang.management.ManagementFactory;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.log4j.Logger;

public class JMXCacheClean implements JMXCacheCleanMBean {
	public static final Logger log = Logger.getLogger(JMXCacheClean.class);

	protected String jmxName;

	/**
	 * clear the cache for last top trades - live.
	 */
	public void clearLastTopTrades() {
		ApplicationData.clearLastTopTrades();
	}

	public boolean init() {
		try {
			ApplicationData.init();
			return true;
		} catch (Exception e) {
			log.debug("Can't init ApplicationData !", e);
			return false;
		}
	}
	
	public boolean reset() {
		try {
			ApplicationData.reset();
			return true;
		} catch (Exception e) {
			log.debug("Can't reset ApplicationData !", e);
			return false;
		}
	}
	/**
     * Register the instance in the platform MBean server under given name.
     *
     * @param name the name under which to register
     */
    public void registerJMX(String name) {
        jmxName = name;
        try {
            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
            ObjectName on = new ObjectName(jmxName);
            mbs.registerMBean(this, on);
            if (log.isInfoEnabled()) {
                log.info("JMXCacheClean MBean registered - name: " + jmxName);
            }
        } catch (Exception e) {
            log.error("Failed to register in the MBean server.", e);
        }
    }

    /**
     * Unregister the instance from the platform MBean server. Use the name
     * under which it was registered.
     */
    public void unregisterJMX() {
        try {
            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
            ObjectName on = new ObjectName(jmxName);
            mbs.unregisterMBean(on);
            if (log.isInfoEnabled()) {
                log.info("JMXCacheClean MBean unregistered - name: " + jmxName);
            }
        } catch (Exception e) {
            log.error("Failed to unregister from the MBean server.", e);
        }
    }
}
