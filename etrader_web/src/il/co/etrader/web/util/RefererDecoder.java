package il.co.etrader.web.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.apache.commons.lang.StringUtils;
import org.jboss.logging.Logger;

public  class RefererDecoder {
	Logger log = Logger.getLogger(RefererDecoder.class);
	
	String keyWord		= null;
	String start		= null;
	final static String AND_PARAM		= "&";
	final static String START_PARAM		= "?";
	
	
	/*
	 * Searches in http_referrer string for an url encoded query string
	 *  
	 * if the keyword exists substract the string between 
	 * start and end and decode it
	 */
	public RefererDecoder(String keyWord, String start){
		this.keyWord = keyWord;
		this.start = start;

		if(keyWord == null || start == null) {
			throw new IllegalArgumentException("Parameters must not be null.");
		}
	}
	
	public String decode(String token){
		if(token == null) {
			return null;
		}
		
		if(StringUtils.containsIgnoreCase(token, keyWord)){
			int startPosition = Math.max(token.indexOf(AND_PARAM+start), token.indexOf(START_PARAM+start));
			if(startPosition>=0) {
				int startIndex = startPosition+start.length()+1;
				int endIndex = token.indexOf(AND_PARAM, startIndex);
				
				if(endIndex < 0) {
					endIndex = token.length();
				}
				
				try {
					String query = token.substring(startIndex, endIndex);
					return URLDecoder.decode(query, "UTF-8").trim();
				} catch (IndexOutOfBoundsException ex1) {
					log.debug("Can't parse token.");
				}
				catch (UnsupportedEncodingException ex) {
					log.debug("The Character Encoding is not supported.");
				}
			}
		}
		
		return null;
	}
	
	public static String convertWith(String src, String encoding) throws Exception {
	    String result = new String( src.getBytes(), encoding );
        return result;
	}
	
 /*
  * example usage 
	public static void main(String args[]) throws Exception {
		//String test = "http://www.google.ru/url?sa=t&rct=j&q=%D0%B1%D0%B8%D0%BD%D0%B0%D1%80%D0%BD%D1%8B%D0%B5%20%D0%BE%D0%BF%D1%86%D0%B8%D0%BE%D0%BD%D1%8B&source=web&cd=7&ved=0CF4QFjAG&url=http%3A%2F%2Fru.anyoption.com%2F&ei=vESJUJ3DF9CRhQfX2oHoDg&usg=AFQjCNFLehROiigXKrlSIGdhm8";
		//String test = "http://www.google.it/aclk?sa=l&ai=C4vzYB-tzTYvEJMm_hAeGq_yPCbKLz_UBmr2Qqxnd-92kKwgAEAEoA1D6ip2Z-P____8BYP2ynISgEqAB5sqO7QPIAQGqBBxP0M4fdtHTmR1kGsPCTjvbX18cQ0lsD0ppUSV9&sig=AGiWqtyrbnE313Ow-syPdvav3o5DdkUS-A&adurl=http://www.anyoption.com/jsp/landing.jsf%3Fcombid%3D2826%26s%3D2%26dp%3Dgoaowwen01_1_goaowwen01_1ad02&rct=j&q=anyoption";
		//String test = "http://www.google.com.hk/search?q=���������&client=aff-360daohang&hl=zh-CN&ie=gb2312&newwindow=1";
		//String test = "http://www.google.com.hk/search?hl=zh-CN&newwindow=1&safe=strict&q=�����ڻ���˾������ַ,�޼۵����ܹ�ʵ����Щ�뷨����&btnG=Google 搜索&aq=f&aqi=&aql=&oq=&gs_rfai=";
		//String test = "http://www.google.com.hk/search?q=�������ûǮ����ô׬Ǯ���&opt-webpage=on&client=aff-360daohang&hl=zh-CN&ie=gb2312&newwindow=1";
		String test = "http://www.google.com.hk/search?q=�� ׬Ǯ&opt-webpage=on&client=aff-cs-360se&hl=zh-CN&ie=gbk&newwindow=1";
		System.out.println(URLDecoder.decode(RefererDecoder.convertWith(test, "gbk"), "UTF-8"));
		
		// RefererDecoder rd = new RefererDecoder("google", "q=");
		//System.out.println(rd.decode(test));
	}
	*/
  
	
}
