package il.co.etrader.web.util;

import il.co.etrader.util.CommonUtil;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

/**
 * Required converter
 * Used for displaying custom messages for required validation.
 * custom validator is not called when in the field is null, therefore i used converter.
 *
 * To use:
 * define the converter in etrader-config.xml and add attribute in the input tag: requiredLable="something"
 *
 * @author Kobi
 *
 */
public class RequiredConvertor implements Converter, Serializable {
	private static final long serialVersionUID = 1L;

	public Object getAsObject(FacesContext context, UIComponent component, String target) throws ConverterException {
		if (!context.getRenderResponse()) {
			if (CommonUtil.isParameterEmptyOrNull(target)) {
				String[] params = null;
				String lable = CommonUtil.getMessage(((String)component.getAttributes().get("requiredLable")), null);
				if (null != lable) {
					params = new String[1];
					params[0] = lable;
				}
				String errorMsg = CommonUtil.getMessage("error.single.required", params);
				FacesMessage message = new FacesMessage();
		        message.setDetail(errorMsg);
		        message.setSummary(errorMsg);
		        message.setSeverity(FacesMessage.SEVERITY_ERROR);
		        throw new ConverterException(message);
			}
		}
		return target;
	}

	public String getAsString(FacesContext context, UIComponent component, Object target) throws ConverterException {
		if (null != target) {
			return String.valueOf(target);
		}
		return "";
	}

}
