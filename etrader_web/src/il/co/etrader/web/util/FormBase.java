package il.co.etrader.web.util;


import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.Visit;

import java.util.Random;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

public class FormBase {

	private static final Logger logger = Logger.getLogger(FormBase.class);


	 /**
     * Generates a new save token, saving it to a field in Visit. Guaranteed to not
     * generate a 0 (0 is used to denote an expired token).<br/><br/>
     *
     * This token is used to make sure that
     * actions that modify data in a way that should not be immediately repeated.
     * Call this method prior to rendering a page that will submit the non-repeatable
     * request.  Then before saving any data, call saveTokenIsInvalid() to see if the
     * save should be executed.  If the token is valid, expire it and proceed with
     * saving.<br/>
     *
     * The view that submits an unrepeatable request should have the tag:<br/>
     *      <h:inputHidden value="#{visit.saveToken}" /><br/>
     * in it.  Visit.getSaveToken() will set this field with the active token when the
     * page is rendered.  Visit.setSaveToken() will set a received token field, which
     * can then be compared to the active token to find out whether a save should be
     * performed.
     */

    protected void generateSaveToken() {
        logger.debug("generateSaveToken()");
        Random random = new Random();
        long token = random.nextLong();
        while (token == 0) {
            token = random.nextLong();
        }

        this.getVisit().setActiveToken(token);
        this.getVisit().setReceivedToken(0);
        this.getVisit().setUri("");
    }

    /**
     * Checks the save token to see if it is valid.
     *
     * @true if the save token is invalid.  It is invalid if either the received or the active
     * tokens in Visit are zero, or if the tokens do not match.
     */
    protected boolean saveTokenIsInvalid() {
        if (logger.isDebugEnabled() ) {
            logger.debug("saveTokenIsInvalid():\nactive token: " + this.getVisit().getActiveToken() + "\nrecv'd token: " + this.getVisit().getReceivedToken() );
        }
        boolean isValid = this.getVisit().tokensMatchAndAreNonZero();
        // return the inverse because this method is called "saveTokenIsInvalid"
        return !isValid;
    }

    /**
     * Sets active token to zero, preventing any saves by methods that check for valid save token
     * before committing a change until generateSaveToken() is called again.
     */
    protected void expireSaveToken() {
        logger.debug("expireSaveToken()");
        this.getVisit().setActiveToken(0);
    }
    /**
     * Logs an info message saying that a save action was not performed because of invalid save
     * token.  Returns given String as outcome.
     *
     * @param logger for subclass calling this method
     * @param outcome
     * @return outcome
     */
    protected String logInvalidSaveAndReturn(Logger subclassLogger, String outcome) {
        if (subclassLogger.isInfoEnabled() ) {
            subclassLogger.info("The save token was not valid.  Returning outcome: '" + outcome + "'.");
        }
        return outcome;
    }
    // Used by JSF managed bean creation facility
    public Visit getVisit() {
		FacesContext context=FacesContext.getCurrentInstance();
		return ((Visit)context.getApplication().createValueBinding(ConstantsBase.BIND_VISIT).getValue(context));

    }
    // Used by JSF managed bean creation facility
    //public void setVisit(Visit visit) {
        //this.visit = visit;
    //}


}
