package il.co.etrader.web.util;

import java.io.File;
import java.sql.SQLException;
import java.util.Calendar;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.mbeans.PageNames;

/**
 * web utils
 */
public class Utils extends CommonUtil {

    private static final Logger log = Logger.getLogger(Utils.class);

    public static String getPropertyByHost(String key) {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        return getPropertyByHost(request, key);
    }

    public static String getPropertyByHost(String key, String defval) {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        return getPropertyByHost(request, key, defval);
    }

	public static String getPropertyByHost(HttpServletRequest request, String key) {
	    return getPropertyByHost(request, key, "?" + key + "?");
	}
	
	/** If 18 years of age; In order to make deposit or nor
	 * 
	 * @param birthYear
	 * @param birthMonth
	 * @param birthDay
	 * @return if == 18 or > 18 years old
	 */
    public static Boolean birthDateValidation(String birthYear, String birthMonth, String birthDay) {
	boolean validate = true;
	try {
	    Calendar cal = Calendar.getInstance();
	    cal.clear();
	    cal.set(Integer.parseInt(birthYear), Integer.parseInt(birthMonth), Integer.parseInt(birthDay));
	    cal.add(Calendar.YEAR, 18);
	    Calendar curCal = Calendar.getInstance();
	    curCal.add(Calendar.MONTH, 1);
	    // if the current day is NOT after the 18th birth date, ergo can not make deposit since < 18
	    if (!(curCal).after(cal)) {
		validate = false;
	    }
	} catch (Exception e) {
	}
	return validate;
    }

	/**
	 * Get applicationDatainstance
	 * @return
	 * 		appplicationData instance
	 */
	public static ApplicationData getAppData() {
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			ApplicationData appData = (ApplicationData) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
			return appData;
		} catch (Exception npe) {
			return null;
		}
	}

    public boolean isAndoridDevice() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
        UAgentInfo uagentInfo = (UAgentInfo) session.getAttribute(Constants.UAGENTINFO);
        if (null == uagentInfo) {
            uagentInfo = new UAgentInfo(context);
            session.setAttribute(Constants.UAGENTINFO, uagentInfo);
        }
        return uagentInfo.isAndroid;
    }

    public boolean isIosDevice() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
        UAgentInfo uagentInfo = (UAgentInfo) session.getAttribute(Constants.UAGENTINFO);
        if (null == uagentInfo) {
            uagentInfo = new UAgentInfo(context);
            session.setAttribute(Constants.UAGENTINFO, uagentInfo);
        }
        return uagentInfo.detectIos();
    }

    /**
     * @return url app download link according to the device.
     */
    //TODO unit test - ios link.
    public String getUrlDownloadApp() {
		String marketUrl = "";
        try {
        	marketUrl = getAppData().getHomePageUrlWithoutLastSlash() + PageNames.getPageNameStatic(getAppData().getPrefixBySkin(), "/jsp/register.jsf");
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

		if (isAndoridDevice()) {
			marketUrl = getAppData().getAppUrlWithParameters(Constants.ANYOPTION_PLAY_STORE_ANDROID_APP);
		} else if (isIosDevice()) {
			marketUrl = getAppData().getAppUrlWithParameters(Constants.ANYOPTION_ITUNES_IPHONE_APP);
		}
		return marketUrl;
	}

    public Investment getTopTradersBySchedulePosition(String schedule, int pos) {
        if (null == getAppData().getLastTopTrades(schedule)) {
            getAppData().getUpdateLastTopTrades();
        }
        return getAppData().getLastTopTrades(schedule).get(pos);
    }

    public String getDateSettledLiveAO(Investment i){
		return CommonUtil.getDateFormat(i.getTimeEstClosing(), i.getUtcOffsetSettled());
	}

	public String getTimeSettledLiveAO(Investment i) {
		return CommonUtil.getTimeFormat(i.getTimeEstClosing());
	}

    public String getMarketImage(String marketId, String envImagePath, String imageType) {
        String fullPath = CommonUtil.getProperty("anyoption.path") + envImagePath + marketId + '.' + imageType;
        log.debug("full Path " + fullPath);
        File imageOnFileSystem = new File(fullPath);
        if (imageOnFileSystem.exists()) {
            return envImagePath + marketId + '.' + imageType;
        }
        return envImagePath + "default.png";
    }

    public int getSizeTopTradersBySchedule(String schedule) {
        if (null == getAppData().getLastTopTrades(schedule)) {
            getAppData().getUpdateLastTopTrades();
        }
        return getAppData().getLastTopTrades(schedule).size();
    }
}
