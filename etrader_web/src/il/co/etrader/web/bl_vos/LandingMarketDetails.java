package il.co.etrader.web.bl_vos;

import il.co.etrader.util.CommonUtil;

import java.io.Serializable;
import java.math.BigDecimal;


public class LandingMarketDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	private long marketId;
	private String marketImageName;
	private BigDecimal closingLevel;
	private Long nextMarketId;

	/**
	 * @return the marketId
	 */
	public long getMarketId() {
		return marketId;
	}

	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	/**
	 * @return the marketImageName
	 */
	public String getMarketImageName() {
		return marketImageName;
	}

	/**
	 * @param marketImageName the marketImageName to set
	 */
	public void setMarketImageName(String marketImageName) {
		this.marketImageName = marketImageName;
	}

	/**
	 * @return the closingLevel
	 */
	public BigDecimal getClosingLevel() {
		return closingLevel;
	}

	/**
	 * @param closingLevel the closingLevel to set
	 */
	public void setClosingLevel(BigDecimal closingLevel) {
		this.closingLevel = closingLevel;
	}

    public String getMarketName() {
        return CommonUtil.getMarketName(marketId);
    }

	/**
	 * @return the nextMarketId
	 */
	public Long getNextMarketId() {
		return nextMarketId;
	}

	/**
	 * @param nextMarketId the nextMarketId to set
	 */
	public void setNextMarketId(Long nextMarketId) {
		this.nextMarketId = nextMarketId;
	}

}