package il.co.etrader.web.bl_vos;
import il.co.etrader.bl_vos.MarketingCombination;
import il.co.etrader.bl_vos.MarketingPixel;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.UsersManager;
import il.co.etrader.web.util.Constants;

import java.io.Serializable;
import java.sql.SQLException;

import javax.faces.context.FacesContext;

public class PixelGenerator implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1068785315292140519L;
	private MarketingCombination combination;

	/**
	 * Return the home page pixels
	 * @return
	 * @throws SQLException
	 */
	public String gethomePagePixel() throws SQLException {
		String pixel = "";
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		String combinationId = ap.getSessionCombinationId();

		if (null == combination || (null != combination && null != combinationId && combination.getId() != Long.parseLong(combinationId))) {
			//	get combination from user when combinationId is not from the defualt combination
			if (null != combinationId && !ap.getDefualtCombination().contains(combinationId) && combinationId != ""){
				combination = UsersManager.getCombinationById(Long.valueOf(combinationId));
			}
		}

		if (null != combination && null != combination.getCombPixels() && combination.getCombPixels().getId() != 0
					&& null != combination.getCombPixels().getPixels()) {
			for (MarketingPixel p : combination.getCombPixels().getPixels()) {
				if (p.getTypeId() == ConstantsBase.PIXEL_TYPE_HOMEPAGE) {
					pixel += p.getHttpCode() + "\n";
			 		pixel = pixel.replace("&", "&amp;");
				}
			}
		}
		return pixel;
	}

	/**
	 * Return the registerlanding pages pixels
	 * @return
	 * @throws SQLException
	 */
	public String getRegLandingPixel() throws SQLException {
		String pixel = "";
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		String combinationId = ap.getSessionCombinationId();

		if (null == combination || (null != combination && null != combinationId && combination.getId() != Long.parseLong(combinationId))) {
			//	get combination from user when combinationId is not from the defualt combination
			if (null != combinationId && !ap.getDefualtCombination().contains(combinationId)){
				combination = UsersManager.getCombinationById(Long.valueOf(combinationId));
			}
		}
		if (null != combination && null != combination.getCombPixels() && combination.getCombPixels().getId() != 0
					&& null != combination.getCombPixels().getPixels()) {
			String tmp = "";
			for (MarketingPixel p : combination.getCombPixels().getPixels()) {
				if (p.getTypeId() == ConstantsBase.PIXEL_TYPE_REG_LANDINGPAGE) {
					tmp = p.getHttpsCode() + "\n";
			 		pixel += putParametersPixel(tmp, ap);
				}
			}
		}
		return pixel;
	}


	/**
	 * Convert parameters to real value
	 * @param pixel
	 * @param app
	 * @return
	 */
	public String putParametersPixel(String pixel, ApplicationData app){
		String newPixel = pixel;
		// replace all parameters with values
 		if (newPixel.indexOf(Constants.MARKETING_PARAMETER_CONTACTID) > -1) {
 			newPixel = newPixel.replace(Constants.MARKETING_PARAMETER_CONTACTID, app.getRegisterAttemptId());
 		}
 		//change all & to &amp;
 		newPixel = newPixel.replace("&", "&amp;");
 		return newPixel;
	}

}
