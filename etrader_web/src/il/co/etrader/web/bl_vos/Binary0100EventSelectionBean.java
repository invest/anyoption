package il.co.etrader.web.bl_vos;

import il.co.etrader.util.ConstantsBase;
import il.co.etrader.web.bl_managers.ApplicationData;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.context.FacesContext;

@SuppressWarnings("serial")
public class Binary0100EventSelectionBean implements Serializable {
    private long opportunityId;
    private long marketId;
    private String market;
    private String eventLevel;
    private long opportunityTypeId;
    private Date timeEstClosing;
    
    public long getOpportunityId() {
        return opportunityId;
    }
    
    public void setOpportunityId(long opportunityId) {
        this.opportunityId = opportunityId;
    }
    
    public long getMarketId() {
        return marketId;
    }

    public void setMarketId(long marketId) {
        this.marketId = marketId;
    }

    public String getMarket() {
        return market;
    }
    
    public void setMarket(String market) {
        this.market = market;
    }
    
    public String getEventLevel() {
        return eventLevel;
    }

    public void setEventLevel(String eventLevel) {
        this.eventLevel = eventLevel;
    }

    public long getOpportunityTypeId() {
        return opportunityTypeId;
    }

    public void setOpportunityTypeId(long opportunityTypeId) {
        this.opportunityTypeId = opportunityTypeId;
    }

    public Date getTimeEstClosing() {
        return timeEstClosing;
    }
    
    public void setTimeEstClosing(Date timeEstClosing) {
        this.timeEstClosing = timeEstClosing;
    }
    
    public String getTimeEstClosingFmt() {
        FacesContext fc = FacesContext.getCurrentInstance();
        ApplicationData a = (ApplicationData) fc.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(fc);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        sdf.setTimeZone(a.getTimeZone());
        return sdf.format(timeEstClosing);
    }
}