//package il.co.etrader.web.bl_vos;
//
//import il.co.etrader.util.ConstantsBase;
//
//import java.util.Date;
//
//import org.apache.log4j.Logger;
//
//public class InvestmentRejects {
//
//	private static final Logger logger = Logger.getLogger(InvestmentRejects.class);
//
//	private long userId;
//	private long opportunityId;
//	private int rejectTypeId;
//	private String sessionId;
//	private Double realLevel;
//	private Double pageLevel;
//	private Double wwwLevel;
//	private Long amount;
//	private Float returnInv;
//	private String rejectAdditionalInfo;
//	private Date timeCreated;
//	private int writerId;
//	private int secBetweenInvestments;
//	private Double rate;
//	private int typeId;
//	private int opportunity_type;
//    private int fromGraph;
//    private boolean isRejectedDuringClose;
//    private int needToOpen;
//    private int needToClose;
//    private Float refundInv;
//
//
//	/**
//	 * @return the rate
//	 */
//	public Double getRate() {
//		return rate;
//	}
//
//	/**
//	 * @param rate the rate to set
//	 */
//	public void setRate(Double rate) {
//		this.rate = rate;
//	}
//
//	public InvestmentRejects() {
//		 sessionId = null;
//		 rejectAdditionalInfo = null;
//		 realLevel = null;
//		 pageLevel = null;
//		 wwwLevel = null;
//		 returnInv = null;
//		 refundInv = null;
//		 fromGraph = ConstantsBase.FROM_GRAPH_DEFAULT;
//	}
//
//	public InvestmentRejects(long oppId, double pageLevel1, String sessionId1, Double invAmount, long userId1, double userRate, float pageOddsWin, Float pageOddsLose, int choice, int opp_type, int fromGraph) {
//
//	    this(oppId, pageLevel1, sessionId1, invAmount, userId1, userRate, pageOddsWin, pageOddsLose, choice, opp_type, fromGraph, 1, 0);
//	}
//
//	public InvestmentRejects(long oppId, double pageLevel1, String sessionId1, Double invAmount, long userId1, double userRate, Float pageOddsWin, Float pageOddsLose, int choice, int opp_type, int fromGraph, int needToOpen, int needToClose) {
//		 rejectAdditionalInfo = null;
//		 realLevel = null;
//		 wwwLevel = null;
//
//		this.opportunityId = oppId;
//	    this.pageLevel = pageLevel1;
//	    this.sessionId = sessionId1;
//	    this.amount = invAmount.longValue();
//	    this.userId = userId1;
//	    this.returnInv = pageOddsWin;
//	    this.refundInv = pageOddsLose;
//	    this.rate = userRate;
//	    this.typeId = choice;
//	    this.opportunity_type = opp_type;
//	    this.fromGraph = fromGraph;
//	    this.needToClose = needToClose;
//	    this.needToOpen = needToOpen;
//	    this.isRejectedDuringClose = false;
//	}
//
//	/**
//	 * @return the amount
//	 */
//	public Long getAmount() {
//		return amount;
//	}
//	/**
//	 * @param amount the amount to set
//	 */
//	public void setAmount(Long amount) {
//		this.amount = amount;
//	}
//	/**
//	 * @return the opportunityId
//	 */
//	public long getOpportunityId() {
//		return opportunityId;
//	}
//	/**
//	 * @param opportunityId the opportunityId to set
//	 */
//	public void setOpportunityId(long opportunityId) {
//		this.opportunityId = opportunityId;
//	}
//	/**
//	 * @return the pageLevel
//	 */
//	public Double getPageLevel() {
//		return pageLevel;
//	}
//	/**
//	 * @param pageLevel the pageLevel to set
//	 */
//	public void setPageLevel(Double pageLevel) {
//		this.pageLevel = pageLevel;
//	}
//	/**
//	 * @return the realLevel
//	 */
//	public Double getRealLevel() {
//		return realLevel;
//	}
//	/**
//	 * @param realLevel the realLevel to set
//	 */
//	public void setRealLevel(Double realLevel) {
//		this.realLevel = realLevel;
//	}
//	/**
//	 * @return the rejectAdditionalInfo
//	 */
//	public String getRejectAdditionalInfo() {
//		return rejectAdditionalInfo;
//	}
//	/**
//	 * @param rejectAdditionalInfo the rejectAdditionalInfo to set
//	 */
//	public void setRejectAdditionalInfo(String rejectAdditionalInfo) {
//		this.rejectAdditionalInfo = rejectAdditionalInfo;
//	}
//	/**
//	 * @return the rejectTypeId
//	 */
//	public int getRejectTypeId() {
//		return rejectTypeId;
//	}
//	/**
//	 * @param rejectTypeId the rejectTypeId to set
//	 */
//	public void setRejectTypeId(int rejectTypeId) {
//		this.rejectTypeId = rejectTypeId;
//	}
//	/**
//	 * @return the returnInv
//	 */
//	public Float getReturnInv() {
//		return returnInv;
//	}
//	/**
//	 * @param returnInv the returnInv to set
//	 */
//	public void setReturnInv(Float returnInv) {
//		this.returnInv = returnInv;
//	}
//	/**
//	 * @return the secBetweenInvestments
//	 */
//	public int getSecBetweenInvestments() {
//		return secBetweenInvestments;
//	}
//	/**
//	 * @param secBetweenInvestments the secBetweenInvestments to set
//	 */
//	public void setSecBetweenInvestments(int secBetweenInvestments) {
//		this.secBetweenInvestments = secBetweenInvestments;
//	}
//	/**
//	 * @return the sessionId
//	 */
//	public String getSessionId() {
//		return sessionId;
//	}
//	/**
//	 * @param sessionId the sessionId to set
//	 */
//	public void setSessionId(String sessionId) {
//		this.sessionId = sessionId;
//	}
//	/**
//	 * @return the timeCreated
//	 */
//	public Date getTimeCreated() {
//		return timeCreated;
//	}
//	/**
//	 * @param timeCreated the timeCreated to set
//	 */
//	public void setTimeCreated(Date timeCreated) {
//		this.timeCreated = timeCreated;
//	}
//	/**
//	 * @return the userId
//	 */
//	public long getUserId() {
//		return userId;
//	}
//	/**
//	 * @param userId the userId to set
//	 */
//	public void setUserId(long userId) {
//		this.userId = userId;
//	}
//	/**
//	 * @return the writerId
//	 */
//	public int getWriterId() {
//		return writerId;
//	}
//	/**
//	 * @param writerId the writerId to set
//	 */
//	public void setWriterId(int writerId) {
//		this.writerId = writerId;
//	}
//	/**
//	 * @return the wwwLevel
//	 */
//	public Double getWwwLevel() {
//		return wwwLevel;
//	}
//	/**
//	 * @param wwwLevel the wwwLevel to set
//	 */
//	public void setWwwLevel(Double wwwLevel) {
//		this.wwwLevel = wwwLevel;
//	}
//
//	/**
//	 * @return the typeId
//	 */
//	public int getTypeId() {
//		return typeId;
//	}
//
//	/**
//	 * @param typeId the typeId to set
//	 */
//	public void setTypeId(int typeId) {
//		this.typeId = typeId;
//	}
//
//	/**
//	 * @return the opportunity_type
//	 */
//	public int getOpportunity_type() {
//		return opportunity_type;
//	}
//
//	/**
//	 * @param opportunity_type the opportunity_type to set
//	 */
//	public void setOpportunity_type(int opportunity_type) {
//		this.opportunity_type = opportunity_type;
//	}
//
//	public boolean isRejectedDuringClose() {
//		return isRejectedDuringClose;
//	}
//
//	public void setRejectedDuringClose(boolean isRejectedDuringClose) {
//		this.isRejectedDuringClose = isRejectedDuringClose;
//	}
//
//	public int getNeedToClose() {
//		return needToClose;
//	}
//
//	public void setNeedToClose(int needToClose) {
//		this.needToClose = needToClose;
//	}
//
//	public int getNeedToOpen() {
//		return needToOpen;
//	}
//
//	public void setNeedToOpen(int needToOpen) {
//		this.needToOpen = needToOpen;
//	}
//
//	/**
//	 * @return the fromGraph
//	 */
//	public int getFromGraph() {
//		return fromGraph;
//	}
//
//	/**
//	 * @param fromGraph the fromGraph to set
//	 */
//	public void setFromGraph(int fromGraph) {
//		this.fromGraph = fromGraph;
//	}
//
//    /**
//     * @return the refundInv
//     */
//    public Float getRefundInv() {
//        return refundInv;
//    }
//
//    /**
//     * @param refundInv the refundInv to set
//     */
//    public void setRefundInv(Float refundInv) {
//        this.refundInv = refundInv;
//    }
//}