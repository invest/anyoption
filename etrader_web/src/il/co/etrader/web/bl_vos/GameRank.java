/**
 *
 */
package il.co.etrader.web.bl_vos;

/**
 * @author Eliran
 *
 */
public class GameRank {
	private long userId;
	private String userName;
	private String userNameFull;
	private String winSum;
	private String rank;
	private String rowColor;


	public String getRowColor() {
		return rowColor;
	}
	public void setRowColor(String rowColor) {
		this.rowColor = rowColor;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}

	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getWinSum() {
		return winSum;
	}
	public void setWinSum(String winSum) {
		this.winSum = winSum;
	}
	/**
	 * @return the userNameFull
	 */
	public String getUserNameFull() {
		return userNameFull;
	}
	/**
	 * @param userNameFull the userNameFull to set
	 */
	public void setUserNameFull(String userNameFull) {
		this.userNameFull = userNameFull;
	}

}
