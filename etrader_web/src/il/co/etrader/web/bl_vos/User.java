package il.co.etrader.web.bl_vos;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.LimitationDeposits;
import com.anyoption.common.beans.MailBoxUser;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.Tier;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.BaroPayRequest;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.InvestmentLimit;
import com.anyoption.common.managers.BaroPayManagerBase;
import com.anyoption.common.managers.FeesManagerBase;
import com.anyoption.common.managers.LimitationDepositsManagerBase;
import com.anyoption.common.util.Pixel;
import com.anyoption.common.util.Sha1;
import com.anyoption.common.util.TCToken;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_managers.TaxHistoryManagerBase;
import il.co.etrader.bl_managers.TiersManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_managers.WritersManagerBase;
import il.co.etrader.bl_vos.Limit;
import il.co.etrader.bl_vos.MarketingAffilate;
import il.co.etrader.bl_vos.MarketingCombination;
import il.co.etrader.bl_vos.MarketingPixel;
import il.co.etrader.bl_vos.MarketingSubAffiliate;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.clearing.EnvoyInfo;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.SendTemplateEmail;
import il.co.etrader.util.TransactionFormater;
import il.co.etrader.web.bl_managers.ApplicationData;
import il.co.etrader.web.bl_managers.InvestmentsManager;
import il.co.etrader.web.bl_managers.TransactionsManager;
import il.co.etrader.web.bl_managers.UsersManager;
import il.co.etrader.web.util.Constants;
import il.co.etrader.web.util.Utils;

public class User extends UserBase{

	private static final long serialVersionUID = -5296490843146958685L;

	private static final Logger logger = Logger.getLogger(User.class);

	private boolean firstTimeLogin;
	private boolean firstTimeRegister;
	private boolean firstTimeDeposit;
	private long deletedCardId;
	private String deleted4digitCard;
	private boolean haveRegistrationBonus;

	private MarketingCombination combination;
	private MarketingAffilate affiliate;
	private MarketingSubAffiliate subAffiliate;
	private boolean changeDetailsPassValidated;
	private MailBoxUser emailPopUp;
	private boolean emailPopUpDisplay;
	private boolean emailPopUpAlert;
    private boolean isSentActivationEmail;
    private boolean isEmailRemindMeLater;
    private boolean isHasDeposits;
    private ArrayList<Transaction> depositsForPixel;  // deposits for Google analytics code
    private Transaction depositForPixel;    // first deposit that we didn't run pixels on it e.g. deposits that ocuurs in the backgound like backend, online notification, bank wire etc.
    public boolean firstTimeRegPopUp;
    private String phoneCountryPrefix;
    private long fingerPrint;
    
    private Long minInvest;
    private Long ftd;
    private String tcToken;
    private String loginProduct;
	
   
    public User() {
    	firstTimeLogin = true;
    	firstTimeRegister = false;
    	changeDetailsPassValidated = false;
		setFirstTimeRegPopUp(true);
	}

	/**
	 * Check and return first deposit pixels that we didn't run through the web (e.g. Envoy, cc from backend etc.)
	 * @return
	 * @throws SQLException
	 */
	public String getFirstDepositBackgoundPixels() throws SQLException {
		String pixels = "";
		String ls = System.getProperty("line.separator");
		if (isFirstBackgroundDeposit()) {
			pixels += getFirstDepositPixel(depositForPixel) + ls;
			if (skinId != Skin.SKIN_ETRADER) {
				pixels += getFirstDepositAffiliatePixel(depositForPixel) + ls +
							getFirstDepositSubAffiliatePixel(depositForPixel) + ls;
			}
			depositForPixel.setPixelRunTime(new Date());
			TransactionsManagerBase.updatePixelRunTime(depositForPixel.getId());
		}
		return pixels;
	}

	/**
	 * Return true if neeed to run first pixel deposit that occured in the background
	 * @return
	 */
	public boolean isFirstBackgroundDeposit() {
		if (null != depositForPixel && depositForPixel.getPixelRunTime() == null) {
			return true;
		}
		return false;
	}

	/**
	 * Check and return list of deposits that we didn't run through the web (e.g. Envoy, cc from backend etc.)
	 * And we should operate Google Analytics pixel on them
	 */
	public ArrayList<Transaction> getDepositsForPixel() {
		return depositsForPixel;
	}

	/**
	 * Return true if neeed to run Google Analytics code
	 * @return
	 */
	public boolean isBackgroundDeposits() {
		if (null != depositsForPixel && depositsForPixel.size() > 0) {
			return true;
		}
		return false;
	}

	/**
	 * Update transactions for not running the Google Analytics code again
	 * @return
	 * @throws SQLException
	 */
	public String getUpdateBackDepositsAfterDone() throws SQLException {
		if (isBackgroundDeposits()) {
			String ids = "";
			for (Transaction t : depositsForPixel) {
				ids += t.getId() + ",";
			}
			ids = ids.substring(0, ids.length()-1);
			TransactionsManagerBase.updatePixelRunTimeByList(ids);
			depositsForPixel.clear();
		}
		return null;
	}

	@Override
	public String getBalanceTxt() throws SQLException {
		//logger.debug("getting user Balance from DB.");
		long l = UsersManager.getBalance(this.getId());
		balance = l;
		if (null == currency) {
			logger.warn("Problem getting userBalnace, currency instance is null, userId: " + this.id);
			return "";
		} else {
			return CommonUtil.displayAmount(l, currency.getId());
		}
	}

	public boolean getNextInvestOnUs() throws SQLException{
		return UsersManager.getIsNextInvestOnUs(this.getId());
	}

	
	public String getZeroOneHundredFeeTxt(){
		long displayFee = 5;
		try {
			displayFee = FeesManagerBase.getFee(FeesManagerBase.ZERO_ONE_HUNDRED_COMMISSION_FEE, currencyId);
		} catch (SQLException e) {
			logger.debug("Can't load 0100 fee 111", e);
		}
    	return CommonUtil.displayAmount(""+displayFee, currencyId.intValue());
	}
	@Override
	public boolean isHasBonus() {
		boolean flag = false;
		try {
			String utcOffset = CommonUtil.getUtcOffset();
			flag = BonusManagerBase.getAllUserBonus(this.id, null, null, 0, utcOffset, ApplicationData.getCurrenciesList()).size() > 0 ? true : false;
		} catch (SQLException e) {
			logger.debug("cant check if user have bonus", e);
		}
		return flag;
	}

	/**
	 * is user have bonus
	 * @return
	 */
	public boolean isHasBonusActiveOrGranted() {
		boolean hasBonus = false;
		try {
			hasBonus =  BonusManagerBase.isHasBonusActiveOrGranted(this.id);
		} catch (SQLException e) {
			logger.debug("cant check if user have bonus in state active or granted", e);
		}
		return hasBonus;
	}

	@Override
	public String getTaxBalanceTxt() throws SQLException{
		//logger.debug("getting user Tax Balance from DB.");
		long l=UsersManager.getTaxBalance(this.getId());
		taxBalance=l;
		return CommonUtil.displayAmount(l, currencyId);
	}


	public boolean isFirstTimeLogin() {
		return firstTimeLogin;
	}
	public String getSetFirstTimeLogin() {
		firstTimeLogin=false;
		return "";
	}

	@Override
	public String getUserName() {
		if (userName==null) {
			return "";
		}

		return userName.toLowerCase();
	}

	public String getDeleted4digitCard() {
		return deleted4digitCard;
	}

	public void setDeleted4digitCard(String deleted4digitCard) {
		this.deleted4digitCard = deleted4digitCard;
	}

	public long getDeletedCardId() {
		return deletedCardId;
	}

	public void setDeletedCardId(long deletedCardId) {
		this.deletedCardId = deletedCardId;
	}

	public String getUnSetFirstTimeRegister() {
		firstTimeRegister=false;
		return "";
	}
	public String getUnSetFirstTimeDeposit() {
		firstTimeDeposit=false;
		return "";
	}

	public boolean getIsFirstTimeRegister() {
		return firstTimeRegister;
	}

	public String getSetFirstTimeRegister() {
		firstTimeRegister = true;
		return "";
	}

	/**
	 * Check if it's first deposit for pixel running
	 * Get transaction id from the context to check that it's the coorect first deposit
	 * @throws SQLException
	 */
	public boolean getIsFirstTimeDeposit() throws SQLException {
		firstTimeDeposit = false;
		FacesContext context = FacesContext.getCurrentInstance();
		Transaction currentDeposit = (Transaction) context.getApplication().createValueBinding(Constants.BIND_TRANSACTION).getValue(context);
		Transaction firstDeposit = UsersManager.getFirstDepositForPixlRunning(id);
		if (null == firstDeposit ||
				(null != firstDeposit &&
				 firstDeposit.getPixelRunTime() == null &&
				 firstDeposit.getId() == currentDeposit.getId())) {
			firstTimeDeposit = true;
		}
		return firstTimeDeposit;
	}

	public void setFirstTimeRegister(boolean firstTimeRegister) {
		this.firstTimeRegister = firstTimeRegister;
	}

	public boolean isFirstTimeDeposit() {
		return firstTimeDeposit;
	}

	public void setFirstTimeDeposit(boolean firstTimeDeposit) {
		this.firstTimeDeposit = firstTimeDeposit;
	}

	/**
     * return String that represent the tax balance message for this user.
     * the message inculding 2 parameters. first is that date of period2
     * and the second is the tax balance of this user for mid-year.
     */
    public String getTaxBalanceForPerionOneTxt() throws Exception{

    	// takes the tax on taxHistory for this user
    	long taxPeriod = TaxHistoryManagerBase.getTaxBalanceForPerionOneTxt(this.id);

    	if ( taxPeriod == 0 ) { // if user not have mid-year tax
    		return "";
    	}

    	//create the format for the message
    	String[] params = new String[2];
    	params[0] = CommonUtil.getFormatDatePeriodOne();
    	params[1] = CommonUtil.displayAmount(taxPeriod, currencyId);

    	return CommonUtil.getMessage("balance.taxbalance.noticed", params);
    }

	/**
	 * get  Iban depends on user currency id
	 * @return Iban
	 */
	public String getIban() {
		String curr = ConstantsBase.EMPTY_STRING;
		switch (currencyId.intValue()) {
		case (int)ConstantsBase.CURRENCY_USD_ID:
				curr = ConstantsBase.CURRENCY_USD_IBAN;
				break;
		case (int)ConstantsBase.CURRENCY_TRY_ID:
				curr = ConstantsBase.CURRENCY_TRY_IBAN;
				break;
		case (int)ConstantsBase.CURRENCY_EUR_ID:
				curr = ConstantsBase.CURRENCY_EUR_IBAN;
				break;
		case (int)ConstantsBase.CURRENCY_GBP_ID:
				curr = ConstantsBase.CURRENCY_GBP_IBAN;
				break;
		default:
			break;
		}
		return curr;
	}
	/**
	 * get bank account number depends on user currency id
	 * @return the bank account number
	 */
	public String getAccountNumber() {
		String curr = ConstantsBase.EMPTY_STRING;
		switch (currencyId.intValue()) {
		case (int)ConstantsBase.CURRENCY_USD_ID:
				curr = ConstantsBase.CURRENCY_USD_ACCOUNT_NUMBER;
				break;
		case (int)ConstantsBase.CURRENCY_TRY_ID:
				curr = ConstantsBase.CURRENCY_TRY_ACCOUNT_NUMBER;
				break;
		case (int)ConstantsBase.CURRENCY_EUR_ID:
				curr = ConstantsBase.CURRENCY_EUR_ACCOUNT_NUMBER;
				break;
		case (int)ConstantsBase.CURRENCY_GBP_ID:
				curr = ConstantsBase.CURRENCY_GBP_ACCOUNT_NUMBER;
				break;
		default:
			break;
		}
		return curr;
	}

	/**
	 * Get free invest min amount from user's limit
	 * formated by currency id of the user
	 * @return
	 * 		Formated amount
	 * @throws NumberFormatException
	 * @throws SQLException
	 */
	public String getNextInvestMinAmount() throws NumberFormatException, SQLException {
		String amount = "";
		Limit l =  UsersManager.getLimitById(limitId);
		amount = CommonUtil.displayAmount(l.getFreeInvestMin(), true, currencyId);
		return amount;
	}

	/**
	 * Get free invest max amount from user's limit
	 * formated by currency id of the user
	 * @return
	 * 		Formated amount
	 * @throws NumberFormatException
	 * @throws SQLException
	 */
	public String getNextInvestMaxAmount() throws NumberFormatException, SQLException {
		String amount = "";
		Limit l =  UsersManager.getLimitById(limitId);
		amount = CommonUtil.displayAmount(l.getFreeInvestMax(), true, currencyId);
		return amount;
	}
	public boolean getIsUSUser(){
		if(countryId==220){ // United States
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return the combination
	 */
	public MarketingCombination getCombination() {
		return combination;
	}

	/**
	 * @param combination the combination to set
	 */
	public void setCombination(MarketingCombination combination) {
		this.combination = combination;
	}

	/**
	 * Check and put registration pixel(https)
	 * @return
	 */
	public String getRegistrationPixel() {
		String pixel = "";
		logger.debug("Start getRegistrationPixel");
		logger.debug("firstTimeRegister=" + firstTimeRegister);
		if (null != combination) {
			logger.debug("combination=" + combination.getId());			
		}	
		if (null != combination && firstTimeRegister &&
				null != combination.getCombPixels() && combination.getCombPixels().getId() != 0
					&& null != combination.getCombPixels().getPixels()) {
			for (MarketingPixel p : combination.getCombPixels().getPixels()) {
				if (p.getTypeId() == ConstantsBase.PIXEL_TYPE_REGISTRATION) {
					pixel = p.getHttpsCode();
					pixel = putParametersPixel(pixel, null);
				}
			}
		}
		if (skinId == Skin.SKIN_ETRADER) {
			getUnSetFirstTimeRegister();
		}
		logger.debug("END getRegistrationPixel=" + pixel);
		return pixel;
	}

	/**
	 * Check and put affiliate registration pixel(https)
	 * @return
	 */
	public String getRegistrationAffiliatePixel() {
		logger.debug("Start getRegistrationAffiliatePixel");
		String pixel = "";
		logger.debug("firstTimeRegister=" + firstTimeRegister);
		if (null != affiliate) {
			logger.debug("affiliate=" + affiliate.getId());			
		}
		if (null != affiliate && firstTimeRegister &&
				null != affiliate.getAffiliatePixels() && affiliate.getAffiliatePixels().getId() != 0
					&& null != affiliate.getAffiliatePixels().getPixels()) {
			for (MarketingPixel p : affiliate.getAffiliatePixels().getPixels()) {
				if (p.getTypeId() == ConstantsBase.PIXEL_TYPE_REGISTRATION) {
					pixel = p.getHttpsCode();
					pixel = putParametersPixel(pixel, null);
				}
			}
		}
		logger.debug("End getRegistrationAffiliatePixel=" + pixel);
		return pixel;
	}

	/**
	 * Check and put sub affiliate registration pixel(https)
	 * @return
	 */
	public String getRegistrationSubAffiliatePixel() {
		String pixel = "";
		logger.debug("Start getRegistrationSubAffiliatePixel");
		logger.debug("firstTimeRegister=" + firstTimeRegister);
		if (null != subAffiliate) {
			logger.debug("subAffiliate=" + subAffiliate.getId());			
		}		
		if (null != subAffiliate && firstTimeRegister &&
				null != subAffiliate.getSubAffiliatePixels() && subAffiliate.getSubAffiliatePixels().getId() != 0
					&& null != subAffiliate.getSubAffiliatePixels().getPixels()) {
			for (MarketingPixel p : subAffiliate.getSubAffiliatePixels().getPixels()) {
				if (p.getTypeId() == ConstantsBase.PIXEL_TYPE_REGISTRATION) {
					pixel = p.getHttpsCode();
					pixel = putParametersPixel(pixel, null);
				}
			}
		}
		getUnSetFirstTimeRegister();
		logger.debug("END getRegistrationSubAffiliatePixel=" + pixel);
		return pixel;
	}

	/**
	 * Check and put first deposit pixel(https)
	 * @return
	 */
	public String getFirstDepositPixel(Transaction t) {
		logger.debug("Start getFirstDepositPixel");
		String pixel = "";
		logger.debug("firstTimeDeposit=" + firstTimeDeposit);
		try {
			combination = UsersManager.getCombinationByUserId(id);
		} catch (SQLException e) {
			logger.debug("Can't load combination for user id = " + id, e);
		}
		if (null != combination) {
			logger.debug("combination=" + combination.getId());			
		}			
		if (null != combination && !Utils.getAppData().getIsLocal() && (firstTimeDeposit || null != t) &&
				null != combination.getCombPixels() && combination.getCombPixels().getId() != 0
					&& null != combination.getCombPixels().getPixels()) {
			for (MarketingPixel p : combination.getCombPixels().getPixels()) {
				if (p.getTypeId() == ConstantsBase.PIXEL_TYPE_FIRST_DEPOSIT) {
					pixel = p.getHttpsCode();
					pixel = putParametersPixel(pixel, t);
				}
			}
		}
		if (skinId == Skin.SKIN_ETRADER && null == t) {  // in Ao we trying to generate affiliate / subAffiliate pixels
			getUnSetFirstTimeDeposit();						  // the transaction != null when we are running backgound pixels
		}
		// update cvt cookie for anyoption users
		if (skinId != Skin.SKIN_ETRADER) {
			ApplicationData.addCookie(Constants.CUSTOMER_VISIT_TYPE,
					Constants.CUSTOMER_VISIT_TYPE_DEPOSITOR,
					(HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse(),
					Constants.CUSTOMER_VISIT_TYPE);

		}
		logger.debug("End getFirstDepositPixel=" + pixel);
		return pixel;
	}

	/**
	 * wrapper function for combination first deposit pixel(https)
	 * @return
	 */
	public String getFirstDepositPixel() {
		return getFirstDepositPixel(null);
	}

	/**
	 * Check and put affiliate first deposit pixel(https)
	 * @return
	 */
	public String getFirstDepositAffiliatePixel(Transaction t) {
		logger.debug("Start getFirstDepositAffiliatePixel");
		String pixel = "";
		logger.debug("firstTimeDeposit=" + firstTimeDeposit);
		try {
			affiliate = UsersManager.getAffiliateByUserId(id);
		} catch (SQLException e) {
			logger.debug("Can't load combination for user id = " + id, e);
		}
		if (null != affiliate) {
			logger.debug("affiliate=" + affiliate.getId());			
		}			
		if (null != affiliate && !Utils.getAppData().getIsLocal() && (firstTimeDeposit || null != t) &&
				null != affiliate.getAffiliatePixels() && affiliate.getAffiliatePixels().getId() != 0
					&& null != affiliate.getAffiliatePixels().getPixels()) {
			for (MarketingPixel p : affiliate.getAffiliatePixels().getPixels()) {
				if (p.getTypeId() == ConstantsBase.PIXEL_TYPE_FIRST_DEPOSIT) {
					pixel = p.getHttpsCode();
					pixel = putParametersPixel(pixel, t);
				}
			}
		}
		logger.debug("END FirstDepositAffiliatePixel=" + pixel);
		return pixel;
	}

	/**
	 * wrapper function for affiliate first deposit pixel(https)
	 * @return
	 */
	public String getFirstDepositAffiliatePixel() {
		return getFirstDepositAffiliatePixel(null);
	}

	/**
	 * Check and put sub affiliate first deposit pixel(https)
	 * @return
	 */
	public String getFirstDepositSubAffiliatePixel(Transaction t) {
		logger.debug("Start getFirstDepositSubAffiliatePixel");
		String pixel = "";
		logger.debug("firstTimeDeposit=" + firstTimeDeposit);
		if (null != subAffiliate) {
			logger.debug("subAffiliate=" + subAffiliate.getId());			
		}		
		if (null != subAffiliate && !Utils.getAppData().getIsLocal() && (firstTimeDeposit || null != t) &&
				null != subAffiliate.getSubAffiliatePixels() && subAffiliate.getSubAffiliatePixels().getId() != 0
					&& null != subAffiliate.getSubAffiliatePixels().getPixels()) {
			for (MarketingPixel p : subAffiliate.getSubAffiliatePixels().getPixels()) {
				if (p.getTypeId() == ConstantsBase.PIXEL_TYPE_FIRST_DEPOSIT) {
					pixel = p.getHttpsCode();
					pixel = putParametersPixel(pixel, t);
				}
			}
		}
		if (t == null) {
			getUnSetFirstTimeDeposit();
		}
		logger.debug("END FirstDepositSubAffiliatePixel=" + pixel);
		return pixel;
	}

	/**
	 * wrapper function for subAffiliate first deposit pixel(https)
	 * @return
	 */
	public String getFirstDepositSubAffiliatePixel() {
		return getFirstDepositSubAffiliatePixel(null);
	}

 	/**
 	 * Set parameters into the pixel
 	 * @param pixel  pixel code
 	 * @return  pixel code with parameters setting
 	 */
 	public String putParametersPixel(String pixel, Transaction t) {
		if (null == t) {
			FacesContext context = FacesContext.getCurrentInstance();
			t = (Transaction)context.getApplication().createValueBinding(Constants.BIND_TRANSACTION).getValue(context);
		}
		return Pixel.replaceParametersPixel(pixel, this, t);
 	}

	/**
	 * @return the isChangeDetailsPassValidated
	 */
	public boolean getChangeDetailsPassValidated() {
		return changeDetailsPassValidated;
	}

	/**
	 * @param changeDetailsPassValidated the changeDetailsPassValidated to set
	 */
	public void setChangeDetailsPassValidated(boolean changeDetailsPassValidated) {
		this.changeDetailsPassValidated = changeDetailsPassValidated;
	}

 	/**
 	 * Check skin alignment
 	 * @return
 	 */
 	public boolean isLeftAlignSkin() {
 		if ( skinId == Skin.SKIN_ARABIC) {
 			return false;
 		}
 		return true;
 	}

	/**
	 * @return the haveRegistrationBonus
	 */
	public boolean isHaveRegistrationBonus() {
		return haveRegistrationBonus;
	}

	/**
	 * @param haveRegistrationBonus the haveRegistrationBonus to set
	 */
	public void setHaveRegistrationBonus(boolean haveRegistrationBonus) {
		this.haveRegistrationBonus = haveRegistrationBonus;
	}

	/**
	 * UnSet haveRegistrationBonus
	 * @return
	 */
	public String getUnSetHaveRegistrationBonus() {
		haveRegistrationBonus=false;
		return "";
	}

    public String getPassHash() throws Exception {
        if (null == password) {
            return "";
        }
        return Sha1.encode(password);
    }

	/**
	 * Get amount by points convert
	 * formula: points * pointsToCashRate * (1 + convertBenefitPercent)
	 * @return
	 * @throws SQLException
	 */
	public String getPointsConvertAmountTxt() throws SQLException {
		String amount = "";
		if ( null != tierUser) {
			double amountV = TiersManagerBase.convertPointsToCash(tierUser.getPoints(), tierUser.getTierId());
			amount = CommonUtil.displayAmount(CommonUtil.calcAmount(amountV), true, currencyId);
		}
		return amount;
	}

	/**
	 * Load tierUSer info
	 * @return
	 * @throws SQLException
	 */
	public String getLoadTierInfo() throws SQLException {
		this.tierUser = TiersManagerBase.loadTierUserByUserId(id);
		return null;
	}

	/**
	 * is user tier includind free sms
	 * @return
	 */
	public boolean isTierFreeSms() {
		if (null != tierUser) {
			Tier t = TiersManagerBase.getTierById(tierUser.getTierId());
			if (t.isFreeSms()) {
				return true;
			}
		}
		return false;
	}

	public String getEnvoyUserReference() throws Exception{
		EnvoyInfo cInfo = new EnvoyInfo(this,new Transaction());

		return cInfo.getCustomerRef();
	}

	/**
	 * @return the affiliate
	 */
	public MarketingAffilate getAffiliate() {
		return affiliate;
	}

	/**
	 * @param affiliate the affiliate to set
	 */
	public void setAffiliate(MarketingAffilate affiliate) {
		this.affiliate = affiliate;
	}

	/**
	 * @return the subAffiliate
	 */
	public MarketingSubAffiliate getSubAffiliate() {
		return subAffiliate;
	}

	/**
	 * @param subAffiliate the subAffiliate to set
	 */
	public void setSubAffiliate(MarketingSubAffiliate subAffiliate) {
		this.subAffiliate = subAffiliate;
	}

	public long getUnReadEmails() throws SQLException {
		return UsersManager.getUnreadEmailsCount(this.id);
	}

	public boolean isHaveNewEmails() throws SQLException {
		if (getUnReadEmails() > 0) {
			return true;
		}
		return false;
	}

	public String deleteEmail() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		MailBoxUser mbu = (MailBoxUser) context.getApplication().createValueBinding(Constants.BIND_MAILBOX_USER).getValue(context);
		UsersManager.updateEmailsByUserId(this.id, Constants.MAILBOX_STATUS_DETETED, String.valueOf(mbu.getId()));
		return Constants.NAV_MAILBOX;
	}

	public boolean isEmailPopUpDisplay() {
		return emailPopUpDisplay;
	}

	public String getSetEmailPopUpDisplay() throws SQLException {
		emailPopUpDisplay = false;
		UsersManager.updatePopTypeId(emailPopUp.getId(), Constants.MAILBOX_POPUP_TYPE_NONE);
		return "";
	}

	public boolean isEmailPopUpAlert() {
		return emailPopUpAlert;
	}

	public String getSetEmailPopUpAlert() throws SQLException {
		emailPopUpAlert = false;
		UsersManager.updatePopTypeId(emailPopUp.getId(), Constants.MAILBOX_POPUP_TYPE_NONE);
		return "";
	}

	/**
	 * Set mailBox email parameters
	 * @param email
	 * @throws Exception
	 */
	public void setEmailParameters(MailBoxUser email) throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		String template = email.getTemplate().getTemplate();

		if (null != email.getFreeText()) {
			template = template.replace(Constants.MAILBOX_PARAM_FREE_TEXT, email.getFreeText());
		}

		if (email.getTemplate().getTypeId() == Constants.MAILBOX_TYPE_BONUS) {  // generate bonus link
			
			template = template.replace(Constants.MAILBOX_PARAM_BONUS_LINK, ap.getHomePageUrl() +
					"jsp/pages/bonuses.jsf?" + Constants.BONUS_NAVIGATION_PARAM + "=" + email.getBonusUserId());
		}

		if (email.getTemplate().getTypeId() == Constants.MAILBOX_TYPE_BONUS_ROUND_BALANCE) {  // generate bonus link
			long minInvAmount = InvestmentsManagerBase.getUserMinInvestmentLimit(this.currencyId);
			template = template.replace("${minInvAmount}",CommonUtil.displayAmount(minInvAmount, this.currencyId));
		}

		Date userTimeCreated = CommonUtil.getDateTimeFormaByTz(this.getTimeCreated(), this.getUtcOffset());
		String userCreatedDate = CommonUtil.getDateFormat(userTimeCreated);
		String userCreatedTime = CommonUtil.getTimeFormat(userTimeCreated);

		Date mailSentDate = CommonUtil.getDateTimeFormaByTz(email.getTimeCreated(), this.getUtcOffset());
		String mailSentDateTxt = CommonUtil.getDateFormat(mailSentDate);
		String mailSentTimeTxt = CommonUtil.getTimeFormat(mailSentDate);

		template = template.replace("${currency}",CommonUtil.getMessage(CommonUtil.getCurrencySymbol(this.currencyId), null));
		template = template.replace("${userFirstName}",this.getFirstName());
		template = template.replace("${userLastName}",this.getLastName());
		template = template.replace("${userName}",this.getUserName());
		template = template.replace("${userId}", String.valueOf(this.getId()));
		template = template.replace("${issueActionsDate}", mailSentDateTxt);
		template = template.replace("${issueActionsTime}", mailSentTimeTxt);
		template = template.replace("${userTimeCreatedDate}", userCreatedDate);
		template = template.replace("${userTimeCreatedTime}", userCreatedTime);
		

		String writerNickNameFirst = WritersManagerBase.getWriter(email.getWriterId()).getNickNameFirst();
		String writerNickNameLast = WritersManagerBase.getWriter(email.getWriterId()).getNickNameLast();
		
		if(CommonUtil.isParameterEmptyOrNull(writerNickNameFirst)){
			writerNickNameFirst = UsersManagerBase.getWriterNickName(this.getId(), true);
		}
		
		if(CommonUtil.isParameterEmptyOrNull(writerNickNameLast)){
			writerNickNameLast = UsersManagerBase.getWriterNickName(this.getId(), false);
		}
		
		if (!CommonUtil.isParameterEmptyOrNull(writerNickNameFirst)) {
			if (!CommonUtil.validateHebrewLettersOnly(writerNickNameFirst)) {
				template = template.replace("${writerNickName}", writerNickNameFirst + writerNickNameLast);
			} else {
				template = template.replace("${writerNickName}",
											CommonUtil.getMessage("mailbox.sender.default.representative.first.name", null)
													+ CommonUtil.getMessage("mailbox.sender.default.representative.last.name", null));
			}
		} else {
			template = template.replace("${writerNickName}",
										CommonUtil.getMessage("mailbox.sender.default.representative.first.name", null)
												+ CommonUtil.getMessage("mailbox.sender.default.representative.last.name", null));
		}

		String supportPhone = ApplicationDataBase.getSupportPhoneByCountryId(String.valueOf(this.countryId));
		String supportFax = ApplicationDataBase.getSupportFaxeByCountryId(String.valueOf(this.countryId));
		if (null == supportPhone) {  // get default
			supportPhone = ApplicationDataBase.getSupportPhoneByCountryId(String.valueOf(Constants.COUNTRY_ID_US));
			supportFax = ApplicationDataBase.getSupportFaxeByCountryId(String.valueOf(Constants.COUNTRY_ID_US));
		}
		template = template.replace("${supportPhone}", supportPhone);
		template = template.replace("${supportFax}", supportFax);
		template = template.replace("${password}",this.password);
		template = template.replace("${email}",this.email);

		String code = ApplicationDataBase.getCountryPhoneCodes(String.valueOf(this.countryId));
		String phone = code + "-" + this.mobilePhone;
		template = template.replace("${mobilePhone}", phone);

		template = template.replace("{userFirstName}",this.getFirstName());
		template = template.replace("{username}",this.getUserName());
		template = template.replace("${bonusExpDays}", String.valueOf(ConstantsBase.BONUS_EMAIL_BEFORE_EXP_DAYS));

		if(template.indexOf("${" + SendTemplateEmail.PARAM_FOUR_CONDITION + "}") > -1) {
			if(TransactionsManagerBase.getLastXCCTransactionsCountByUser(this.getId(), 180) > 0) {
				template = template.replace("${" + SendTemplateEmail.PARAM_FOUR_CONDITION + "}", CommonUtil.getMessage("templates.four.condition", null));
			} else {
				template = template.replace("${" + SendTemplateEmail.PARAM_FOUR_CONDITION + "}", "");
			}
		}
		
		// check for currency
		String bankTransferDetails = "";
		int currencyForTemplate = getCurrencyId().intValue();
		if (!ApplicationDataBase.getSkinById(skinId).isRegulated()) {
			switch (currencyForTemplate) {
			case (int) ConstantsBase.CURRENCY_EUR_ID:
				bankTransferDetails = CommonUtil.getMessage("bank.transfer.details.boxB", null, ap.getUserLocale());
				break;
			case (int) ConstantsBase.CURRENCY_AUD_ID:
			case (int) ConstantsBase.CURRENCY_ZAR_ID:
			case (int) ConstantsBase.CURRENCY_CZK_ID:
			case (int) ConstantsBase.CURRENCY_PLN_ID:
				bankTransferDetails = CommonUtil.getMessage("bank.transfer.details.boxF", null, ap.getUserLocale());
				break;
			case (int) ConstantsBase.CURRENCY_USD_ID:
			case (int) ConstantsBase.CURRENCY_GBP_ID:
			case (int) ConstantsBase.CURRENCY_TRY_ID:
			case (int) ConstantsBase.CURRENCY_CNY_ID:
			case (int) ConstantsBase.CURRENCY_RUB_ID:
			case (int) ConstantsBase.CURRENCY_KRW_ID:
			case (int) ConstantsBase.CURRENCY_SEK_ID:
					bankTransferDetails = CommonUtil.getMessage("bank.transfer.details.boxA", null,  ap.getUserLocale());
					break;
			}
		} else if (ApplicationDataBase.getSkinById(skinId).isRegulated() && currencyForTemplate == ConstantsBase.CURRENCY_EUR_ID) {
			
			bankTransferDetails = CommonUtil.getMessage("bank.transfer.details.boxC", null, ap.getUserLocale());
			
		} else if (ApplicationDataBase.getSkinById(skinId).isRegulated() && (currencyForTemplate == ConstantsBase.CURRENCY_EUR_ID ||
				currencyForTemplate == ConstantsBase.CURRENCY_USD_ID || currencyForTemplate == ConstantsBase.CURRENCY_GBP_ID)) {
			bankTransferDetails = CommonUtil.getMessage("bank.transfer.details.boxE", null,  ap.getUserLocale());
		} else if(currencyForTemplate == ConstantsBase.CURRENCY_ZAR_ID || currencyForTemplate == ConstantsBase.CURRENCY_CZK_ID
				|| currencyForTemplate == ConstantsBase.CURRENCY_PLN_ID){
			bankTransferDetails = CommonUtil.getMessage("bank.transfer.details.boxF", null,  ap.getUserLocale());
		}
		template = template.replace("${bankTransferDetails}", bankTransferDetails);

		long trxId = email.getTransactionId();
		if (trxId != 0) {
			Transaction t;
			try {
				t = TransactionsManager.getTransaction(trxId);
				if (t.isCcDeposit() ||
						t.getTypeId() == TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW) {
					CreditCard cc = TransactionsManager.getCreditCard(t.getCreditCardId());
					t.setCc4digit(cc.getCcNumberLast4());
				}
			} catch (SQLException e) {
				logger.error("Problem getting mailBox transactionId!");
				throw(e);
			}

			if (null != t.getTimeSettled()) {
				template = template.replace("${dateDDMMYYYY}", new SimpleDateFormat("dd.MM.yy").format(t.getTimeSettled()));
			}
			template = template.replace("${cancelWithdrawalDays}", String.valueOf(Constants.CANCEL_WITHDRAWAL_DAYS_FOR_CANCEL));
			template = template.replace("${userAddressNumber}",this.getStreetNo());
			template = template.replace("${userAddressStreet}",this.getStreet());
			template = template.replace("${userAddressCity}",this.getCityName());
			template = template.replace("${userAddressZipCode}",this.getZipCode());
			template = template.replace("${transactionAmount}", CommonUtil.displayAmount(t.getAmount(), t.getCurrency().getId())); ///TODO: amount before fee?!?!?
			template = template.replace("${userAddress}", getStreet()+" "+getStreetNo()+" "+getCityName()+" "+getZipCode());
			template = template.replace("${receiptNum}", String.valueOf(t.getId()));

			if (t.getTypeId() == TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW  || t.getTypeId() == TransactionsManagerBase.TRANS_TYPE_EFT_WITHDRAW) { // wire/EFT transaction
				template = template.replace("${transactionBankName}", t.getWire().getBankNameTxt());
			} else if (t.getTypeId() == TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW)  {
				template = template.replace("${transactionPan}", t.getCc4digit());
			} else if (t.getTypeId() == TransactionsManagerBase.TRANS_TYPE_BAROPAY_WITHDRAW) {
				BaroPayRequest baroPayRequest = BaroPayManagerBase.getBaroPayRequest(trxId);
				template = template.replace("${transactionBankName}", baroPayRequest.getBankName());
				template = template.replace("${transferAmount}", CommonUtil.displayAmount(t.getAmount(), t.getCurrency().getId()));
				template = template.replace("${userFullName}", this.getFirstName() + " " + this.getLastName());
			}

			template = template.replace("${date}", TransactionFormater.getTimeCreatedTxt(t));
			template = template.replace("${dateOnly}", CommonUtil.getDateFormat(t.getTimeCreated(), t.getUtcOffsetCreated()));
			template = template.replace("${amount}", CommonUtil.displayAmount(t.getAmount(), t.getCurrency().getId()));

			if (template.indexOf("${footer}"  ) > -1) {  // deposit via Backend
				String key = null;
				String[] params = new String[1];
				if (t.isCcDeposit()) {  // cc deposit email
					key = "receipt.footer.creditcard.mb";
					params[0] = String.valueOf(t.getCc4digit());
				} else { // bank wire deposit email
					key = "receipt.footer.bank.mb";
					params[0] = String.valueOf(t.getId());
				}
				template = template.replace("${footer}", CommonUtil.getMessage(key, params));
			}
		}


		email.getTemplate().setTemplate(template);
	}


	/**
	 * Update Last login from DB for bonus banner
	 * @return
	 */
	public String getUpdateLastLoginFromDB(){
		this.setTimeLastLoginFromDb(new Date());
		return null;
	}

	public String getFirstNameMobile() {
		if (null == firstName) {
			return "";
		}
		if (firstName.length() > 6) {
			return (firstName.substring(0,6) + "...");
		}
		return firstName;
	}

    /**
     * @return the isSentActivationEmail
     */
    public boolean isSentActivationEmail() {
        return isSentActivationEmail;
    }

    /**
     * @param isSentActivationEmail the isSentActivationEmail to set
     */
    public void setSentActivationEmail(boolean isSentActivationEmail) {
        this.isSentActivationEmail = isSentActivationEmail;
    }

    /**
     * @return the isEmailRemindMeLater
     */
    public boolean isEmailRemindMeLater() {
        return isEmailRemindMeLater;
    }

    /**
     * @param isEmailRemindMeLater the isEmailRemindMeLater to set
     */
    public void setEmailRemindMeLater(boolean isEmailRemindMeLater) {
        this.isEmailRemindMeLater = isEmailRemindMeLater;
    }

    /**
	 * @return the isHasDeposits
     * @throws SQLException
	 */
	public boolean isHasDeposits() throws SQLException {
		isHasDeposits = !TransactionsManagerBase.isFirstDeposit(id, 0);
		return isHasDeposits;
	}

	/**
	 * @param isHasDeposits the isHasDeposits to set
	 */
	public void setHasDeposits(boolean isHasDeposits) {
		this.isHasDeposits = isHasDeposits;
	}

	/**
     * Update user details after refusing to email activation
     * @return
     * @throws SQLException
     */
    public String increaseEmailRefTimes() throws SQLException {
    	this.authorizedMailRefusedTimes++;
    	UsersManager.increaseEmailRefused(this.id);
    	this.isEmailRemindMeLater = true;
    	FacesContext context = FacesContext.getCurrentInstance();
        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
        ve.getValue(context.getELContext());
        ve.setValue(context.getELContext(), this);
//    	context.getApplication().createValueBinding(Constants.BIND_USER).setValue(context, this);
    	return Constants.NAV_MAIN_WITH_REDIRECT;
    }

    /**
     * Return true in case that the user is autorized or a depositor user that didn't autorized his email before...
     * @return
     */
    public boolean isEmailAutorizedLogic() {
    	if (isAuthorizedMail ||
    			(!isAuthorizedMail && isEmailRemindMeLater) ||
    			(!isAuthorizedMail && !isEmailRemindMeLater && authorizedMailRefusedTimes >= Constants.ACTIVATION_EMAIL_DEPO_MAX_REFUSED_TIMES) ) {
    		return true;
    	}
    	return false;
    }
    
	public boolean getCanEditEmail(){
		return !userName.equalsIgnoreCase(email);
	}

    public boolean isFirstTimeRegPopUp() {
		return firstTimeRegPopUp;
	}

	public void setFirstTimeRegPopUp(boolean firstTimeRegPopUp) {
		this.firstTimeRegPopUp = firstTimeRegPopUp;
	}

	public MailBoxUser getEmailPopUp() {
		return emailPopUp;
	}

	public void setEmailPopUp(MailBoxUser emailPopUp) {
		this.emailPopUp = emailPopUp;
	}

	public void setEmailPopUpAlert(boolean emailPopUpAlert) {
		this.emailPopUpAlert = emailPopUpAlert;
	}

	public void setEmailPopUpDisplay(boolean emailPopUpDisplay) {
		this.emailPopUpDisplay = emailPopUpDisplay;
	}

	public Transaction getDepositForPixel() {
		return depositForPixel;
	}

	public void setDepositForPixel(Transaction depositForPixel) {
		this.depositForPixel = depositForPixel;
	}

	public void setDepositsForPixel(ArrayList<Transaction> depositsForPixel) {
		this.depositsForPixel = depositsForPixel;
	}

    public boolean getCanEditGender() {
        return CommonUtil.isParameterEmptyOrNull(gender);
    }
    
    public boolean getCanEditTimeBirthDate() {
        if (timeBirthDate == null) {
            return true;
        } else {
            return false;
        }
    }

	public String getPhoneCountryPrefix() {
		return phoneCountryPrefix;
	}

	public void setPhoneCountryPrefix(String phoneCountryPrefix) {
		this.phoneCountryPrefix = phoneCountryPrefix;
	}

	public long getFingerPrint() {
		return fingerPrint;
	}

	public void setFingerPrint(long fingerPrint) {
		this.fingerPrint = fingerPrint;
	}
	
	public boolean isHas0100Investments() {
		try {
			return InvestmentsManager.isHas0100Investments(id);
		} catch (SQLException e) {
			logger.debug("Can't count 0100 investments", e);
			return false;
		}
	}

	public Long getMinInvest() {
		if(minInvest == null) {
			InvestmentLimit limit = InvestmentsManagerBase.getUserLimit(id, currencyId, Opportunity.TYPE_REGULAR);
			if(limit!=null) {
				minInvest = limit.getMinAmount();
			}
		}
		return minInvest;
	}

	public Long getFtd() {
		if(ftd == null) {
			try {
				LimitationDeposits ld = LimitationDepositsManagerBase.getLimitByUserId(id);
				if(ld!=null) {
					ftd = ld.getMinimumFirstDeposit();
				}
			} catch (SQLException e) {
				logger.debug("no ftd info", e);
			}
		}
		return ftd;
	}
	
	public Timestamp getTimeCreatedTS(){
		Timestamp t = CommonUtil.convertToTimeStamp(getTimeCreated());
		return t;
	}
	
	public String getTCToken() {
		if (tcToken == null) {
			// basic token
			String tcSite = "anyoptionbasic.tradingcentral.com";
			long tcValidity = System.currentTimeMillis();
			String tcPartnerId = "anyoptionbasic";
			if (firstDepositId > 0) {
				tcSite = "anyoptionplatinumplus.tradingcentral.com";
				tcPartnerId = "anyoptionplatinumplus";
//				String[] tokenParts = UsersManager.getTCTokenParts(id);
//				if (tokenParts == null || tokenParts.length != 4) {
//					logger.warn("Incorect TC access parameters. Generating basic token");
//				} else if (!Boolean.valueOf(tokenParts[2]) && Long.valueOf(tokenParts[1]) > new Date().getTime()) {
//					try {
//						tcSite = tokenParts[0];
//						tcValidity = Long.valueOf(tokenParts[1]);
//						tcPartnerId = tokenParts[3];
//					} catch (NumberFormatException e) {
//						logger.debug("Unable to parse TC validity. Generating basic token", e);
//						tcSite = "anyoptionbasic.tradingcentral.com";
//						tcValidity = System.currentTimeMillis();
//						tcPartnerId = "anyoptionbasic";
//					}
//				}
			}
			String tokenParams = tcPartnerId	+ TCToken.TCTOKEN_SEPARATOR + getId() + TCToken.TCTOKEN_SEPARATOR
									+ ApplicationData.getLanguage(getLanguageId()).getCode() + TCToken.TCTOKEN_SEPARATOR
									+ tcValidity / 1000l;
			logger.debug("String to be encoded for TCToken" + tokenParams);
			String tokenParamsEnc = TCToken.getToken(tokenParams);
			tcToken = tcSite + "/login.asp?token=" + tokenParamsEnc;
		}
		return tcToken;
	}

	public String getLoginProduct() {
		return loginProduct;
	}

	public void setLoginProduct(String loginProduct) {
		this.loginProduct = loginProduct;
	}

	public String getSumInvestTxt() {
		if (null == currency) {
			logger.warn("Problem getting SumInvest, currency instance is null, userId: " + this.id);
			return "";
		} else {
			return CommonUtil.displayAmount(getBalancePlus(), currency.getId());
		}
	}
	
}