/**
 *
 */
package il.co.etrader.web.bl_vos;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.web.util.Constants;

import java.util.ArrayList;

import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ReutersQuotes;
import com.anyoption.common.managers.MarketsManagerBase;

/**
 * @author Eyal G
 *
 */
public class ReutersQuotesPopup {
	private long schduled;
	private String marketDisplayName;
	private long marketGroupId;
	private double closingLevel;
	private ArrayList<ReutersQuotes> reutersQuotes;
	private int decimalPoint;
	private long marketId;
	private long opportunityTypeId;

	public ReutersQuotesPopup() {
		reutersQuotes = new ArrayList<ReutersQuotes>();
	}

	/**
	 * @return the closingLevel
	 */
	public double getClosingLevel() {
		return closingLevel;
	}
	/**
	 * @param closingLevel the closingLevel to set
	 */
	public void setClosingLevel(double closingLevel) {
		this.closingLevel = closingLevel;
	}
	/**
	 * @return the marketDisplayName
	 */
	public String getMarketDisplayName() {
		return marketDisplayName;
	}
	/**
	 * @param marketDisplayName the marketDisplayName to set
	 */
	public void setMarketDisplayName(String marketDisplayName) {
		this.marketDisplayName = marketDisplayName;
	}
	/**
	 * @return the marketGroupId
	 */
	public long getMarketGroupId() {
		return marketGroupId;
	}
	/**
	 * @param marketGroupId the marketGroupId to set
	 */
	public void setMarketGroupId(long marketGroupId) {
		this.marketGroupId = marketGroupId;
	}
	/**
	 * @return the reutersQuotes
	 */
	public ArrayList<ReutersQuotes> getReutersQuotes() {
		return reutersQuotes;
	}
	/**
	 * @param reutersQuotes the reutersQuotes to set
	 */
	public void setReutersQuotes(ArrayList<ReutersQuotes> reutersQuotes) {
		this.reutersQuotes = reutersQuotes;
	}
	/**
	 * @return the schduled
	 */
	public long getSchduled() {
		return schduled;
	}
	/**
	 * @param schduled the schduled to set
	 */
	public void setSchduled(long schduled) {
		this.schduled = schduled;
	}

	/**
	 * @return the decimalPoint
	 */
	public int getDecimalPoint() {
		return decimalPoint;
	}

	/**
	 * @param decimalPoint the decimalPoint to set
	 */
	public void setDecimalPoint(int decimalPoint) {
		this.decimalPoint = decimalPoint;
	}

	public String getMarketDisplayNameTxt(long skinId) {
		if (null == marketDisplayName) {
			return "";
		}
		return MarketsManagerBase.getMarketName(skinId, marketId);
	}

	public String getHeaderLine(long skinId) {
		return getMarketDisplayNameTxt(skinId) + " = " + getExpiryFormula() + " = " + getClosingLevelTxt();
	}

	public String getExpiryFormula() {
		if (opportunityTypeId == Opportunity.TYPE_REGULAR) {
			if (marketGroupId == Constants.MARKET_GROUP_ABROAD_STOCKS_LONG
					|| marketGroupId == Constants.MARKET_GROUP_STOCKS_LONG) {
				if (schduled == Opportunity.SCHEDULED_HOURLY) {
					return "[(LAST+ASK+BID)/3]";
				} else {
					return "[Last value]";
				}
			} else if (marketGroupId == Constants.MARKET_GROUP_INDICES_LONG) {
				if (marketId == Market.MARKET_NASDAQF_ID ||
					marketId == Market.MARKET_KLSE_FUTURE_ID ||
					marketId == Market.MARKET_SP_FUTURE_ID ||
					marketId == Market.MARKET_DAX_FUTURE_ID ||
					marketId == Market.MARKET_ISE30_INDEX_ID ||
					marketId == Market.MARKET_RTS_FUTURE_ID ||
					marketId == Market.MARKET_CAC_FUTURE_ID){
					return "[(LAST+ASK+BID)/3]";
				} else {
					return "[Last value]";
				}
			} else if (marketGroupId == Constants.MARKET_GROUP_COMMODITIES_LONG) {
				if (marketId == Market.MARKET_OIL_ID) {
					return "[Last value]";
				} else {
					return "[(ASK+BID)/2]";
				}
			} else if (marketGroupId == Constants.MARKET_GROUP_CURRENCIES_LONG) {
					if (marketId == Market.MARKET_BITCOIN_USD_ID || marketId == Market.MARKET_LITECOIN_USD_ID){
						return CommonUtil.getMessage("assetIndex.expiryLevel.bitcoin", null);
					}
				return "[(ASK+BID)/2]";
			}
		} else { //option +
			if (marketGroupId == Constants.MARKET_GROUP_STOCKS_LONG
					|| marketGroupId == Constants.MARKET_GROUP_ABROAD_STOCKS_LONG) {
				return "[Last value]";
			} else if (marketGroupId == Constants.MARKET_GROUP_INDICES_LONG) {
				if (marketId == Market.MARKET_SP_FUTURE_PLUS_ID ||
					marketId == Market.MARKET_DAX_FUTURE_PLUS_ID ||
					marketId == Market.MARKET_RTS_FUTURE_PLUS_ID ||
					marketId == Market.MARKET_CAC_FUTURE_PLUS_ID){
					return "[(LAST+ASK+BID)/3]";
				} else {
					return "[Last value]";
				}
			} else if (marketGroupId == Constants.MARKET_GROUP_COMMODITIES_LONG) {
				if (marketId == Market.MARKET_OIL_PLUS_ID || marketId == Market.MARKET_SILVER_PLUS_ID) {
					return "[Last value]";
				} else {
					return "[(ASK+BID)/2]";
				}
			} else if (marketGroupId == Constants.MARKET_GROUP_CURRENCIES_LONG) {
				return "[(ASK+BID)/2]";
			}
		}
		return "";
	}

	public String getClosingLevelTxt() {
		return CommonUtil.formatLevelByMarket(closingLevel, 0/*not in use*/, decimalPoint);
	}

	/**
	 * @return the marketId
	 */
	public long getMarketId() {
		return marketId;
	}

	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	/**
	 * @return the opportunityTypeId
	 */
	public long getOpportunityTypeId() {
		return opportunityTypeId;
	}

	/**
	 * @param opportunityTypeId the opportunityTypeId to set
	 */
	public void setOpportunityTypeId(long opportunityTypeId) {
		this.opportunityTypeId = opportunityTypeId;
	}

	public String getLongTermMsg() {
		if (schduled == Opportunity.SCHEDULED_DAYLY) {
			return CommonUtil.getMessage("CMS.reuters.quotes.end.of.day", null);
		} else if (schduled == Opportunity.SCHEDULED_WEEKLY) {
			return CommonUtil.getMessage("CMS.reuters.quotes.end.of.week", null);
		} else if (schduled == Opportunity.SCHEDULED_MONTHLY) {
			return CommonUtil.getMessage("CMS.reuters.quotes.end.of.month", null);
		} else {
			return CommonUtil.getMessage("CMS.reuters.quotes.end.of.quarter", null);
		}
	}

	public boolean shouldShowTime() {
		if (marketId == Market.MARKET_OIL_ID ||
				Market.MARKETS_FUTURES.contains(marketId) ||
				schduled == Opportunity.SCHEDULED_HOURLY) {
			return true;
		} else {
			return false;
		}
	}
}
