package il.co.etrader.web.bl_vos;

import java.io.Serializable;
import java.util.Date;

public class ProfitLineAssetSelectionBean implements Serializable {
    private long marketId;
    private String group;
    private String subGroup;
    private String market;
    private long opportunityId;
    private Date timeEstClosing;
    private String displayFeedName;
    
    public String getGroup() {
        return group;
    }
    
    public void setGroup(String group) {
        this.group = group;
    }
    
    public String getSubGroup() {
        return subGroup;
    }
    
    public void setSubGroup(String subGroup) {
        this.subGroup = subGroup;
    }
    
    public long getMarketId() {
        return marketId;
    }

    public void setMarketId(long marketId) {
        this.marketId = marketId;
    }

    public String getMarket() {
        return market;
    }
    
    public void setMarket(String market) {
        this.market = market;
    }
    
    public long getOpportunityId() {
        return opportunityId;
    }
    
    public void setOpportunityId(long opportunityId) {
        this.opportunityId = opportunityId;
    }

    public Date getTimeEstClosing() {
        return timeEstClosing;
    }

    public void setTimeEstClosing(Date timeEstClosing) {
        this.timeEstClosing = timeEstClosing;
    }

	public String getDisplayFeedName() {
		return displayFeedName;
	}

	public void setDisplayFeedName(String displayFeedName) {
		this.displayFeedName = displayFeedName;
	}
}