package il.co.etrader.web.bl_vos;

import java.io.ByteArrayOutputStream;


/**
 * Attachment of the internal mailBox
 *
 * @author Kobi
 *
 */
public class MailBoxUserAttachment implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	private long id;
	private String attachmentName;
	private ByteArrayOutputStream attachment;

	/**
	 * @return the attachment
	 */
	public ByteArrayOutputStream getAttachment() {
		return attachment;
	}

	/**
	 * @param attachment the attachment to set
	 */
	public void setAttachment(ByteArrayOutputStream attachment) {
		this.attachment = attachment;
	}

	/**
	 * @return the attachmentName
	 */
	public String getAttachmentName() {
		return attachmentName;
	}

	/**
	 * @param attachmentName the attachmentName to set
	 */
	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
}
