package il.co.etrader.web.bl_vos;

import java.io.Serializable;
import java.util.List;

import com.anyoption.common.beans.MarketRate;

/**
 * @author EranL
 *
 */
public class MarketWidgetChartJSON implements Serializable {
	private static final long serialVersionUID = 1L; 
	private List<MarketRate> marketRate;
	
	/**
	 * @return the marketRate
	 */
	public List<MarketRate> getMarketRate() {
		return marketRate;
	}
	/**
	 * @param marketRate the marketRate to set
	 */
	public void setMarketRate(List<MarketRate> marketRate) {
		this.marketRate = marketRate;
	}
	
}
