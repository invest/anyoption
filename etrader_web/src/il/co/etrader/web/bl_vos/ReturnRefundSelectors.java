package il.co.etrader.web.bl_vos;

public class ReturnRefundSelectors {
	
	private long selectorID;
	private long returnSelector;
	private long refundSelector;
	
	public  ReturnRefundSelectors(long selectorID,long returnSelector, long refundSelector){
		this.selectorID=selectorID;
		this.returnSelector=returnSelector;
		this.refundSelector=refundSelector;		
	}
	
	public long getSelectorID() {
		return selectorID;
	}
	
	public void setSelectorID(long selectorID) {
		this.selectorID = selectorID;
	}
	
	public long getReturnSelector() {
		return returnSelector;
	}
	public void setReturnSelector(long returnSelector) {
		this.returnSelector = returnSelector;
	}
	
	public long getRefundSelector() {
		return refundSelector;
	}
	public void setRefundSelector(long refundSelector) {
		this.refundSelector = refundSelector;
	}
}
