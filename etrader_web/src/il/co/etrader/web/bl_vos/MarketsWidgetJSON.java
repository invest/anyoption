package il.co.etrader.web.bl_vos;

import java.io.Serializable;

import com.anyoption.common.beans.base.MarketWidget;

/**
 * @author EranL
 *
 */
public class MarketsWidgetJSON implements Serializable {
	private static final long serialVersionUID = 1L;
	private long defaultMarketId;
	private MarketWidget[] markets;
	
	/**
	 * @return the defaultMarketId
	 */
	public long getDefaultMarketId() {
		return defaultMarketId;
	}
	/**
	 * @param defaultMarketId the defaultMarketId to set
	 */
	public void setDefaultMarketId(long defaultMarketId) {
		this.defaultMarketId = defaultMarketId;
	}
	/**
	 * @return the markets
	 */
	public MarketWidget[] getMarkets() {
		return markets;
	}
	/**
	 * @param markets the markets to set
	 */
	public void setMarkets(MarketWidget[] markets) {
		this.markets = markets;
	}
		
}
