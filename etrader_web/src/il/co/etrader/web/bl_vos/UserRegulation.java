package il.co.etrader.web.bl_vos;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.util.CommonUtil;

import java.sql.SQLException;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.UserRegulationManager;

public class UserRegulation extends UserRegulationBase {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2685745625455774106L;
	private User user;
	public static String REDIRECT_AFTER_REG_MAND_QUEST = "redirectAfterRegMandQuest";
	
	public UserRegulation() throws SQLException {

		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String ru = request.getRemoteUser();
		if (null != ru && ru.length() > 0 ) {
			user = (User) context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
			this.setUserId(user.getId());
			UserRegulationManager.getUserRegulation(this);
		}
		
	}

	public boolean getShowWCErrPage() throws SQLException {
         if(null !=user && user.getId() > 0) {
			if ((CommonUtil.isUserSkinRegulated()) && (this.isSuspended())) {
				return true;
			}
         }
		return false;
	}

	public String getSupportPhone() throws SQLException {
         if(null !=user && user.getId() > 0) {
			return ApplicationDataBase.getSupportPhoneByCountryId(String.valueOf(user.getSkin().getDefaultCountryId()));
         }
         return null;
	}

	public boolean isUserInDepositStep() {
		return CommonUtil.isUserSkinRegulated()
				&& UserRegulationBase.REGULATION_FIRST_DEPOSIT == getApprovedRegulationStep()
				&& !isSuspended();
	}
	
	public boolean isUserInMandQuestStep() {
		return CommonUtil.isUserSkinRegulated()
				&& UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE == getApprovedRegulationStep()
				&& !isSuspended();
	}
	// The step when all documents are upload
	public boolean isUserInDocumentRecieved() {
		return CommonUtil.isUserSkinRegulated()
				&& UserRegulationBase.REGULATION_ALL_DOCUMENTS_RECEIVED == getApprovedRegulationStep()
				&& !isSuspended();
	}
	// User is in the step "all documents are upload" and user's skin is 19 DE
	public boolean isUserIsDeSkinAndHaveDocRecived() {
		return CommonUtil.isUserSkinRegulated()
				&&isUserInDocumentRecieved()&&
				(user.getSkinId()==Skin.SKIN_REG_DE);
	}
	
	public boolean getShouldShowQuestionnaireMenu() {
		return CommonUtil.isUserSkinRegulated()
				&& UserRegulationBase.REGULATION_USER_REGISTERED != getApprovedRegulationStep()
				&& UserRegulationBase.REGULATION_WC_APPROVAL != getApprovedRegulationStep();
	}

	public boolean getShouldShowOptionalQuestionnaire() {
		return CommonUtil.isUserSkinRegulated()
				&& getApprovedRegulationStep() >= UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE
				&& !isSuspended();
	}
	
	public boolean isRedirectAfterRegMandQuest() {
		return FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(REDIRECT_AFTER_REG_MAND_QUEST) != null;
	}
	
	
	public void clearRedirectAfterRegMandQuest() {
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(REDIRECT_AFTER_REG_MAND_QUEST);
	}

	public boolean isUserBeforeMandQuestDoneStep() {
		return CommonUtil.isUserSkinRegulated()
				&& UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE > getApprovedRegulationStep();
	} 
	
	public boolean isShowPopUpForDocuments() {
		boolean result = false;
		if(user!=null && user.getId() > 0) {
			if(user.firstTimeRegPopUp && super.isSuspendedDueDocuments()) {
				result =  true;
				user.setFirstTimeRegPopUp(false);
			}
		 }
		return result;
	}
	
	public Integer getRegulationVersion () {
		if (super.getRegulationVersion() != null) {
			return super.getRegulationVersion();
		} else {
			return 0;
		}
	}
	
}
