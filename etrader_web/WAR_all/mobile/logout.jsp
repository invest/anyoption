<%@page import="il.co.etrader.web.bl_managers.ApplicationData"%>
<%@page import="il.co.etrader.web.util.Constants" %>

<%
	String redirectSessionparam2 = (String)session.getAttribute(Constants.FROM_REDIRECT);
	String duid = (String)session.getAttribute(Constants.DEVICE_UNIQUE_ID);
	String appVer = (String) session.getAttribute(Constants.MOBILE_APP_VER);
	ApplicationData ap = (ApplicationData)application.getAttribute("applicationData");
	ap.updateLogout(session);

	String url = "/mobile/index.jsf";

	request.getSession().invalidate();

	if (null != duid) { // set the duid in the new session
		request.getSession().setAttribute(Constants.DEVICE_UNIQUE_ID, duid);
	}
	if (null != appVer) {
		request.getSession().setAttribute(Constants.MOBILE_APP_VER, appVer);
	}

	if (null != redirectSessionparam2 && redirectSessionparam2.equals("israel")) {
		url += "?from=israel";
	}

	response.sendRedirect(request.getContextPath() + url);

%>