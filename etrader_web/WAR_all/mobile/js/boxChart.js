var CHART_OFFSET = 20;
var CHART_TIME_MARKER = 5;

var COLOR_BORDER = "rgb(168,168,168)";
var COLOR_TEXT = "rgb(0,0,0)";
var COLOR_CALL_PUT = "rgb(187,187,187)";
var COLOR_CHART = "rgb(2,132,214)";
var COLOR_CRR_LVL_LINES = "rgb(255,0,0)";
var COLOR_CRR_LVL_SQUARE = "rgb(2,88,145)";
//var COLOR_ARROWS = "rgb(0,0,0)";
var COLOR_ARROWS = "rgb(5,89,151)";

var COLOR_FADE_BORDER = "rgb(168,168,168)";
var COLOR_FADE_TEXT = "rgb(153,153,153)";
var COLOR_FADE_CALL_PUT = "rgb(241,241,241)";
var COLOR_FADE_CHART = "rgb(153,205,238)";
var COLOR_FADE_CRR_LVL_LINES = "rgb(242,159,159)";
var COLOR_FADE_CRR_LVL_SQUARE = "rgb(160,189,206)";
//var COLOR_FADE_ARROWS = "rgb(153,153,153)";
var COLOR_FADE_ARROWS = "rgb(155,188,213)";

//Calculate period length in seconds.
function calcPeriodInSec(periodStart, periodEnd) {
	return Math.round((periodEnd.getTime() - periodStart.getTime()) / 1000);
};

function fixTimeZone(d, timezoneOffset) {
	var tzDiff = d.getTimezoneOffset() - timezoneOffset;
	d.setMinutes(d.getMinutes() + tzDiff);
};

function fixTime(d, serverTimeOffset, timezoneOffset) {
	d.setSeconds(d.getSeconds() + serverTimeOffset);
	fixTimeZone(d, timezoneOffset);
};

function ChartRate(time, level, chartXPosition) {
	this.time = time;
	this.level = level;
	this.chartXPosition = chartXPosition;
}

function padToTwoDigit(n) {
	if (n < 10) {
		return "0" + n;
	}
	return "" + n;
};

// Format a Date to hh:mm:ss or hh:mm.
function formatToString(d, addSeconds) {
	var str = padToTwoDigit(d.getHours()) + ":" + padToTwoDigit(d.getMinutes());
	if (addSeconds) {
		str = str + ":" + padToTwoDigit(d.getSeconds())
	}
	return str;
};

function extractDecimalPoint(level) {
	if (level.indexOf(".") != -1) {
		return level.length - 1 - level.indexOf(".");
	} else if (level.indexOf(",") != -1) {
		return level.length - 1 - level.indexOf(",");
	}
	return 0;
};

function correctFloatingPointError(number) {
	var correction = Math.pow(10, 5);
	return Math.round(correction * number) / correction;
};

function addThousandsSeparators(s) {
	if (s.length <= 3) {
		return s;
	}
	var i = s.length % 3;
	var tmp = "";
	if (i != 0) {
		tmp = s.substring(0, i);
	}
	do {
		if (tmp.length > 0) {
			tmp = tmp + ",";
		}
		if (i + 3 < s.length) {
			tmp = tmp + s.substring(i, i + 3);
		} else {
			tmp = tmp + s.substring(i);
		}
		i = i + 3;
	} while (i < s.length);
	return tmp;
};

//Format number with pattern #,##0.00 (the number of digits after the decimal point is specified).
function formatDecimalPoint(val, decPoint) {
	var tmp = correctFloatingPointError(val);
	var tmpTxt = "" + tmp;

	// here we handle only positive values ;)
	var intPart = Math.floor(tmp);
	var fractPart = tmp - intPart;
	var i;
	if (fractPart > 0) {
		var fractPartDecimal = extractDecimalPoint(tmpTxt);
		tmpTxt = tmpTxt.substring(tmpTxt.indexOf("."));
		if (fractPartDecimal.valueOf() == decPoint.valueOf()) { // we are ok
			return addThousandsSeparators("" + intPart) + tmpTxt;
		}
		if (fractPartDecimal.valueOf() < decPoint.valueOf()) { // pad with 0s
			var padding = "";
			for (i = fractPartDecimal; i < decPoint; i++) {
				padding = padding + "0";
			}
			return addThousandsSeparators("" + intPart) + tmpTxt + padding;
		}
		return formatDecimalPoint(Math.round(tmp * Math.pow(10, decPoint)) / Math.pow(10, decPoint), decPoint);
	} else if (decPoint > 0) {
		var fractPartStr = ".";
		for (i = 0; i < decPoint; i++) {
			fractPartStr = fractPartStr + "0";
		}
		return addThousandsSeparators("" + intPart) + fractPartStr;
	}
	return addThousandsSeparators(tmpTxt);
};

/*-------------------------------------------------------------
mc.dashTo is a metod for drawing dashed (and dotted) 
lines. I made this to extend the lineTo function because it
doesn't have the cutom line types that the in program
line tool has. To make a dotted line, specify a dash length
between .5 and 1.
-------------------------------------------------------------*/
function dashTo(context, startx, starty, endx, endy, len, gap) {
	// ==============
	// mc.dashTo() - by Ric Ewing (ric@formequalsfunction.com) - version 1.2 - 5.3.2002
	// 
	// startx, starty = beginning of dashed line
	// endx, endy = end of dashed line
	// len = length of dash
	// gap = length of gap between dashes
	// ==============
	//
	// if too few arguments, bail
//	if (arguments.length < 6) {
//		return false;
//	}
	// calculate the legnth of a segment
	var seglength = len + gap;
	// calculate the length of the dashed line
	var deltax = endx - startx;
	var deltay = endy - starty;
	var delta = Math.sqrt((deltax * deltax) + (deltay * deltay));
	// calculate the number of segments needed
	var segs = Math.floor(Math.abs(delta / seglength));
	// get the angle of the line in radians
	var radians = Math.atan2(deltay, deltax);
	// start the line here
	var cx = startx;
	var cy = starty;
	// add these to cx, cy to get next seg start
	deltax = Math.cos(radians) * seglength;
	deltay = Math.sin(radians) * seglength;
	var n;
	// loop through each seg
	for (n = 0; n < segs; n++) {
		context.moveTo(cx, cy);
		context.lineTo(cx + Math.cos(radians) * len, cy + Math.sin(radians) * len);
		cx += deltax;
		cy += deltay;
	}
	// handle last segment as it is likely to be partial
	context.moveTo(cx, cy);
	delta = Math.sqrt((endx - cx) * (endx - cx) + (endy - cy) * (endy - cy));
	if (delta > len) {
		// segment ends in the gap, so draw a full dash
		context.lineTo(cx + Math.cos(radians) * len, cy + Math.sin(radians) * len);
	} else if (delta > 0) {
		// segment is shorter than dash so only draw what is needed
		context.lineTo(cx + Math.cos(radians) * delta, cy + Math.sin(radians) * delta);
	}
	// move the pen to the end position
	context.moveTo(endx, endy);
};

function callPutRect(context, startx, starty, width, height, gap) {
	var i;
	for (i = 0; i < width + height; i += gap) {
		context.moveTo(startx + (i > height ? i - height : 0), starty + (i < height ? i : height));
		context.lineTo(startx + (i < width ? i : width), starty + (i > width ? i - width : 0));
	}
};

function isInside(x, y, startx, starty, width, height) {
	if (x >= startx && x <= startx + width && y >= starty && y <= starty + height) {
		return true;
	}
	return false;
};

function BoxChart(marketId, oppId, canvasId) {
	this.secPerPixel = 1;
	
	this.marketId = marketId;
	this.oppId = oppId;
	this.oppState = 0;
	this.canvasId = canvasId;
//	this.canvas = document.getElementById(canvasId);
//	this.canvasContext = this.canvas.getContext("2d");
	this.canvas = null;
	this.canvasContext = null;
	this.xmlHttp = null;

	this.serverTimeOffset = 0;
	this.timezoneOffset = 0;
	this.timeEstClosing = new Date();
	this.scheduled = 1;
	this.decimalPoint = 0;
	this.rates = null;
	this.dataLoaded = false;

	this.displayStartTime = new Date();
	this.firstSec = 0;
	this.ratesStartTime = new Date();
	this.yMinVal = -1;
	this.yMaxVal = -1;
	this.currentLevel = 0;
	this.currentLevelTime = new Date();
	this.displayOffset = 0;
	this.waitingForExpiry = false;
	this.call = false;
	this.put = false;
}

BoxChart.prototype.chartDataLoaded = function() {
	if (this.readyState == 4) {
		if (this.status == 200) {
			this.chart.canvas = document.getElementById(this.chart.canvasId);
//			log("td width: " + this.chart.canvas.parentNode.offsetWidth + " td height: " + this.chart.canvas.parentNode.offsetHeight);
			this.chart.canvas.setAttribute('width', this.chart.canvas.parentNode.offsetWidth - 5);
			this.chart.canvas.setAttribute('height', this.chart.canvas.parentNode.offsetHeight - 5);
//			log("canvas width: " + this.chart.canvas.width + " canvas height: " + this.chart.canvas.height);
			var tmpBox = document.getElementById(this.chart.oppId);
//			log("box width: " + tmpBox.offsetWidth + " box height: " + tmpBox.offsetHeight);
			this.chart.canvasContext = this.chart.canvas.getContext("2d");
			this.chart.secPerPixel = 3600 / (this.chart.canvas.width - 2 * CHART_OFFSET); 
			
			var xmlDoc = this.responseXML;

			// load settings
			var serverTime = new Date();
			serverTime.setTime(new Number(xmlDoc.getElementsByTagName("serverTime")[0].childNodes[0].nodeValue));
			this.chart.serverTimeOffset = calcPeriodInSec(new Date(), serverTime);
			this.chart.timezoneOffset = new Number(xmlDoc.getElementsByTagName("timezoneOffset")[0].childNodes[0].nodeValue);
			this.chart.timeEstClosing.setTime(new Number(xmlDoc.getElementsByTagName("timeEstClose")[0].childNodes[0].nodeValue));
			fixTimeZone(this.chart.timeEstClosing, this.chart.timezoneOffset);
			this.chart.scheduled = new Number(xmlDoc.getElementsByTagName("scheduled")[0].childNodes[0].nodeValue);
			this.chart.decimalPoint = new Number(xmlDoc.getElementsByTagName("decimalPoint")[0].childNodes[0].nodeValue);
			
			// load rates
			var crrt = new Date();
			fixTime(crrt, this.chart.serverTimeOffset, this.chart.timezoneOffset);
			this.chart.displayStartTime.setTime(this.chart.timeEstClosing.getTime());
			do {
				this.chart.displayStartTime.setHours(this.chart.displayStartTime.getHours() - 1);
			} while (crrt.getTime() < this.chart.displayStartTime.getTime());
	
			this.chart.rates = new Array();
			var xmlRates = xmlDoc.getElementsByTagName("rate");
			if (xmlRates.length > 0) {
				var i;
				for (i = 0; i < xmlRates.length; i++) {
					var rateTime = new Date();
					rateTime.setTime(new Number(xmlRates[i].getElementsByTagName("time")[0].childNodes[0].nodeValue));
					fixTimeZone(rateTime, this.chart.timezoneOffset);
					var rateLevel = new Number(xmlRates[i].getElementsByTagName("level")[0].childNodes[0].nodeValue);
					this.chart.rates[i] = new ChartRate(rateTime, rateLevel, 0);
				}
				this.chart.currentLevel = this.chart.rates[this.chart.rates.length - 1].level;
			} else {
				this.chart.firstSec = 0;
				this.chart.ratesStartTime.setTime(this.chart.displayStartTime.getTime());
			}
			this.chart.cleanUnneededRates();
//			this.chart.displayOffset = (calcPeriodInSec(this.chart.ratesStartTime, this.chart.displayStartTime) - this.chart.firstSec) / this.chart.secPerPixel;
			
			this.chart.dataLoaded = true;
			this.chart.repaint();

			this.chart.canvas.addEventListener('mouseup', this.chart.onMouseUp, false);
			this.chart.canvas.chart = this.chart;
		} else {
			// TODO: show error and retry
		}
		this.chart.xmlHttp = null;
		this.chart = null;
	}
};

BoxChart.prototype.repaint = function() {
	this.canvasContext.clearRect(0, 0, this.canvas.width, this.canvas.height);
	this.canvasContext.strokeStyle = (!this.waitingForExpiry ? COLOR_BORDER : COLOR_FADE_BORDER);
	this.canvasContext.strokeRect(CHART_OFFSET, 0, this.canvas.width - 2 * CHART_OFFSET, this.canvas.height - CHART_OFFSET);

	var tenMinWidth = (this.canvas.width - 2 * CHART_OFFSET) / 6;
	var i;
	this.canvasContext.beginPath();
	for (i = 0; i < 7; i++) {
		this.canvasContext.strokeStyle = (!this.waitingForExpiry ? COLOR_BORDER : COLOR_FADE_BORDER);
		this.canvasContext.moveTo(CHART_OFFSET + i * tenMinWidth, this.canvas.height - CHART_OFFSET + 1);
		this.canvasContext.lineTo(CHART_OFFSET + i * tenMinWidth, this.canvas.height - CHART_OFFSET + 6);
	}
	this.canvasContext.stroke();

	this.canvasContext.beginPath();
//	this.canvasContext.fillStyle = (!this.waitingForExpiry ? COLOR_TEXT : COLOR_FADE_TEXT);
	this.canvasContext.strokeStyle = (!this.waitingForExpiry ? COLOR_TEXT : COLOR_FADE_TEXT);
	this.canvasContext.font = "11px Verdana";
	this.canvasContext.textBaseline = "top";
	var timeMeasures = this.canvasContext.measureText("00:00");
	var xTimes = new Date();
	xTimes.setTime(this.displayStartTime.getTime());
	for (i = 0; i < 7; i++) {
//		this.canvasContext.fillText(formatToString(xTimes, false), CHART_OFFSET + i * tenMinWidth - timeMeasures.width / 2, this.canvas.height - (timeMeasures.height != undefined ? timeMeasures.height : 15));
		this.canvasContext.strokeText(formatToString(xTimes, false), CHART_OFFSET + i * tenMinWidth - timeMeasures.width / 2, this.canvas.height - (timeMeasures.height != undefined ? timeMeasures.height : 15));
		xTimes.setMinutes(xTimes.getMinutes() + 10);
	}
	this.canvasContext.stroke();

	if (this.dataLoaded == true) {
		var ySpread = this.yMaxVal - this.yMinVal;
		
		var ct = new Date();
		fixTime(ct, this.serverTimeOffset, this.timezoneOffset);
		if (null != this.rates && this.rates.length > 0 && ct.getTime() < this.rates[this.rates.length - 1].time.getTime()) {
			// double check that the cursor is not positioned before last rate from the history
			ct = this.rates[this.rates.length - 1].time;
		}
		var secFromStart = calcPeriodInSec(this.ratesStartTime, ct) - this.firstSec;
		var crrX = CHART_OFFSET + secFromStart / this.secPerPixel - this.displayOffset;
	//	if (!getCanScrollForward() && crrX > paintArea._x + paintArea._width) {
	//		crrX = paintArea._x + paintArea._width;
	//	}
		var crrY = 1 + (this.canvas.height - CHART_OFFSET) * (1 - (this.currentLevel - this.yMinVal) / ySpread);
	/*
		_root.mcCrrH._y = crrY;
		if (paintArea._x < crrX && crrX <= paintArea._x + paintArea._width) {
			if (rates.length > 0) {
				paintArea.lineTo(crrX - paintArea._x, 1 + paintArea._height * (1 - (rates[rates.length - 1].getLevel() - yMinVal) / (yMaxVal - yMinVal)));
			}
			_root.mcCrrV._x = crrX;
			_root.icnCrrLvl._x = crrX - _root.icnCrrLvl._width / 2;
			_root.icnCrrLvl._y = crrY - _root.icnCrrLvl._height / 2;
		} else {
			_root.mcCrrV._x = -1000;
			_root.icnCrrLvl._x = -1000;
		}
	*/

		if (this.call || this.put) {
			this.canvasContext.beginPath();
			this.canvasContext.strokeStyle = (!this.waitingForExpiry ? COLOR_CALL_PUT : COLOR_FADE_CALL_PUT);
			if (this.call) {
				callPutRect(this.canvasContext, CHART_OFFSET + 1, 1, this.canvas.width - 2 * CHART_OFFSET - 2, crrY - 2, 5);
				if (crrY > 15) {
					var callWidth = this.canvasContext.measureText("CALL").width; 
					this.canvasContext.strokeText("CALL", (this.canvas.width - callWidth) / 2, crrY / 2 - 5);
				}
			}
			if (this.put) {
				callPutRect(this.canvasContext, CHART_OFFSET + 1, crrY + 1, this.canvas.width - 2 * CHART_OFFSET - 2, this.canvas.height - CHART_OFFSET - crrY - 2, 5);
				if (this.canvas.height - CHART_OFFSET - crrY > 15) {
					var putWidth = this.canvasContext.measureText("PUT").width; 
					this.canvasContext.strokeText("PUT", (this.canvas.width - putWidth) / 2, crrY + (this.canvas.height - CHART_OFFSET - crrY) / 2 - 5);
				}
			}
			this.canvasContext.stroke();
		}

		this.canvasContext.beginPath();
		this.canvasContext.strokeStyle = (!this.waitingForExpiry ? COLOR_CHART : COLOR_FADE_CHART);
		if (this.rates.length > 0) {
			var positioned = false;
			var prevChartY;
			for (i = 0; i < this.rates.length; i++) {
				var chartX = this.rates[i].chartXPosition;
				var chartY = this.rates[i].level;
				if (this.displayOffset < chartX && chartX - this.displayOffset < this.canvas.width - 2 * CHART_OFFSET - 1) {
					if (!positioned) {
						this.canvasContext.moveTo(CHART_OFFSET + chartX - this.displayOffset, 1 + (this.canvas.height - CHART_OFFSET) * (1 - (chartY - this.yMinVal) / ySpread));
						positioned = true;
					} else {
//						this.canvasContext.lineTo(CHART_OFFSET + chartX - this.displayOffset, 1 + (this.canvas.height - CHART_OFFSET) * (1 - (chartY - this.yMinVal) / ySpread));
						this.canvasContext.lineTo(CHART_OFFSET + chartX - this.displayOffset, 1 + (this.canvas.height - CHART_OFFSET) * (1 - (prevChartY - this.yMinVal) / ySpread));
						this.canvasContext.lineTo(CHART_OFFSET + chartX - this.displayOffset, 1 + (this.canvas.height - CHART_OFFSET) * (1 - (chartY - this.yMinVal) / ySpread));
					}
					prevChartY = chartY;
				}
			}
		}
		// connect the chart with the cursor
		if (crrX < this.canvas.width - CHART_OFFSET - 1) {
			this.canvasContext.lineTo(crrX, 1 + (this.canvas.height - CHART_OFFSET) * (1 - (prevChartY - this.yMinVal) / ySpread));
		}
		this.canvasContext.stroke();
	
		this.canvasContext.beginPath();
		this.canvasContext.strokeStyle = (!this.waitingForExpiry ? COLOR_CRR_LVL_LINES : COLOR_FADE_CRR_LVL_LINES);
		dashTo(this.canvasContext, CHART_OFFSET, crrY, this.canvas.width - CHART_OFFSET, crrY, 1, 4); // horizontal
		if (crrX < this.canvas.width - CHART_OFFSET) {
			dashTo(this.canvasContext, crrX, 1, crrX, this.canvas.height - CHART_OFFSET, 1, 4); // vertical
		}
		this.canvasContext.stroke();

		if (crrX < this.canvas.width - CHART_OFFSET) {
			this.canvasContext.fillStyle = (!this.waitingForExpiry ? COLOR_CRR_LVL_SQUARE : COLOR_FADE_CRR_LVL_SQUARE);
			this.canvasContext.fillRect(crrX - 2, crrY - 2, 4, 4);
		}

		// back arrow
		if (this.getCanScrollBack(this)) {
			var arrowX = 9;
//			var arrowY = this.canvas.height - CHART_OFFSET - 20;
			var arrowY = (this.canvas.height - CHART_OFFSET) / 2;
			this.canvasContext.beginPath();
			this.canvasContext.strokeStyle = (!this.waitingForExpiry ? COLOR_ARROWS : COLOR_FADE_ARROWS);
			this.canvasContext.fillStyle = (!this.waitingForExpiry ? COLOR_ARROWS : COLOR_FADE_ARROWS);
			this.canvasContext.moveTo(arrowX, arrowY);
			this.canvasContext.lineTo(arrowX + 7, arrowY - 11);
			this.canvasContext.lineTo(arrowX + 7, arrowY + 11);
			this.canvasContext.lineTo(arrowX, arrowY);
			this.canvasContext.fill();
			this.canvasContext.stroke();
/*
			this.canvasContext.moveTo(arrowX, arrowY);
			this.canvasContext.lineTo(arrowX, arrowY + 10);
			this.canvasContext.moveTo(arrowX + 7, arrowY + 2);
			this.canvasContext.lineTo(arrowX + 7, arrowY + 8);
			this.canvasContext.moveTo(arrowX + 1, arrowY + 5);
			this.canvasContext.lineTo(arrowX + 6, arrowY);
			this.canvasContext.lineTo(arrowX + 6, arrowY + 10);
			this.canvasContext.lineTo(arrowX + 1, arrowY + 5);
			this.canvasContext.fill();
			this.canvasContext.stroke();
*/
			
		}

		// forward arrow
		if (this.getCanScrollForward(this)) {
			var arrowX = this.canvas.width - CHART_OFFSET + 10;
//			var arrowY = this.canvas.height - CHART_OFFSET - 20;
			var arrowY = (this.canvas.height - CHART_OFFSET) / 2;
			this.canvasContext.beginPath();
			this.canvasContext.strokeStyle = (!this.waitingForExpiry ? COLOR_ARROWS : COLOR_FADE_ARROWS);
			this.canvasContext.fillStyle = (!this.waitingForExpiry ? COLOR_ARROWS : COLOR_FADE_ARROWS);
			this.canvasContext.moveTo(arrowX, arrowY);
			this.canvasContext.lineTo(arrowX - 7, arrowY - 11);
			this.canvasContext.lineTo(arrowX - 7, arrowY + 11);
			this.canvasContext.lineTo(arrowX, arrowY);
			this.canvasContext.fill();
			this.canvasContext.stroke();
/*
			this.canvasContext.moveTo(arrowX + 7, arrowY);
			this.canvasContext.lineTo(arrowX + 7, arrowY + 10);
			this.canvasContext.moveTo(arrowX, arrowY + 2);
			this.canvasContext.lineTo(arrowX, arrowY + 8);
			this.canvasContext.moveTo(arrowX + 6, arrowY + 5);
			this.canvasContext.lineTo(arrowX + 1, arrowY);
			this.canvasContext.lineTo(arrowX + 1, arrowY + 10);
			this.canvasContext.lineTo(arrowX + 6, arrowY + 5);
			this.canvasContext.fill();
			this.canvasContext.stroke();
*/
		}
	}

//	this.canvasContext.fillStyle = (!this.waitingForExpiry ? COLOR_TEXT : COLOR_FADE_TEXT);
	this.canvasContext.strokeStyle = (!this.waitingForExpiry ? COLOR_TEXT : COLOR_FADE_TEXT);
	if (this.yMaxVal > 0) {
		this.canvasContext.beginPath();
		this.canvasContext.textBaseline = "top";
//		this.canvasContext.fillText(formatDecimalPoint(this.yMaxVal, this.decimalPoint), CHART_OFFSET + 5, 5);
		this.canvasContext.strokeText(formatDecimalPoint(this.yMaxVal, this.decimalPoint), CHART_OFFSET + 5, 5);
		this.canvasContext.stroke();
	}

	if (this.yMinVal > 0) {
		this.canvasContext.beginPath();
		this.canvasContext.textBaseline = "bottom";
//		this.canvasContext.fillText(formatDecimalPoint(this.yMinVal, this.decimalPoint), CHART_OFFSET + 5, this.canvas.height - CHART_OFFSET - 5);
		this.canvasContext.strokeText(formatDecimalPoint(this.yMinVal, this.decimalPoint), CHART_OFFSET + 5, this.canvas.height - CHART_OFFSET - 5);
		this.canvasContext.stroke();
	}
};
/*
BoxChart.prototype.resize = function() {
	this.canvas.setAttribute('width', window.innerWidth - 20);
	this.canvas.setAttribute('height', window.innerHeight - 20);
	this.canvasContext.width = window.innerWidth - 20;
	this.canvasContext.height = window.innerHeight - 20;

	if (window.innerWidth > 2 * CHART_OFFSET) {
		this.secPerPixel = 3600 / (window.innerWidth - 2 * CHART_OFFSET - 20);
	} else {
		this.secPerPixel = 3600;
	}
	if (this.dataLoaded) {
		this.cleanUnneededRates();
	}

	this.repaint();
};
*/
BoxChart.prototype.updateItem = function(pLevel, pState, pTime) {
	if (!this.dataLoaded) {
		return;
	}
	var crrState = new Number(pState);
	var crrLvl = new Number(pLevel.replace(",", ""));
	if (crrLvl == 0 || crrState < 2 || crrState > 6 || (this.waitingForExpiry && this.oppState == 6)) {
		return;
	}
	this.currentLevel = crrLvl;
	this.currentLevelTime.setTime(new Number(pTime));
	fixTimeZone(this.currentLevelTime, this.timezoneOffset);

	if (this.yMinVal == -1 || crrLvl < this.yMinVal) {
		this.yMinVal = crrLvl;
	}
	if (this.yMaxVal == -1 || crrLvl > this.yMaxVal) {
		this.yMaxVal = crrLvl;
	}
	this.ensureYOffset();
	this.takeRateSample();
//	this.repaint();
	
	this.oppState = crrState;
	if (this.oppState > 3) {
		this.hideCall();
		this.hidePut();
	}
};

BoxChart.prototype.takeRateSample = function() {
	if (this.rates.length == 0 || (this.currentLevelTime.getTime() - this.rates[this.rates.length - 1].time.getTime()) / 1000 > this.secPerPixel) {
		var secFromStart = calcPeriodInSec(this.ratesStartTime, this.currentLevelTime) - this.firstSec;
		var t = new Date();
		t.setTime(this.currentLevelTime.getTime());
		this.rates[this.rates.length] = new ChartRate(t, this.currentLevel, secFromStart / this.secPerPixel);
	}
	this.repaint();
};

BoxChart.prototype.init = function() {
	this.xmlHttp = getXMLHttp();
	if (null != this.xmlHttp) {
		this.xmlHttp.chart = this;
		this.xmlHttp.onreadystatechange = this.chartDataLoaded;
		this.xmlHttp.open("GET", context_path + "/jsp/xml/chartData.xml?marketId=" + this.marketId + "&oppId=" + this.oppId, true);
		this.xmlHttp.send();
	}
}

BoxChart.prototype.cleanUnneededRates = function() {
	var crrt = new Date();
	fixTime(crrt, this.serverTimeOffset, this.timezoneOffset);
	this.ratesStartTime.setTime(this.timeEstClosing.getTime());
	do {
		this.ratesStartTime.setHours(this.ratesStartTime.getHours() - 1);
	} while (crrt.getTime().valueOf() < this.ratesStartTime.getTime().valueOf());
	if (this.rates[0].time.getTime() < this.ratesStartTime.getTime()) {
		this.ratesStartTime.setHours(this.ratesStartTime.getHours() - 1);
	}

//	log("Cleaning old hour rates - length: " + this.rates.length + " yMinVal: " + this.yMinVal + " yMaxVal: " + this.yMaxVal + " startTime: " + this.ratesStartTime);
	var newRates = new Array();
	var newYMinVal;
	var newYMaxVal;
	var i;
	for (i = 0; i < this.rates.length; i++) {
		if (this.rates[i].time.getTime() > this.ratesStartTime.getTime()) {
			if (newRates.length == 0) {
				this.firstSec = this.rates[i].time.getSeconds();
				newYMinVal = this.rates[i].level;
				newYMaxVal = newYMinVal;
			}
			newRates[newRates.length] = this.rates[i];
			newRates[newRates.length - 1].chartXPosition = (calcPeriodInSec(this.ratesStartTime, this.rates[i].time) - this.firstSec) / this.secPerPixel;
			if (newRates.length > 1) {
				if (this.rates[i].level < newYMinVal) {
					newYMinVal = this.rates[i].level;
				}
				if (this.rates[i].level > newYMaxVal) {
					newYMaxVal = this.rates[i].level;
				}
			}
		}
	}
	this.rates = newRates;
	if (newRates.length > 0) {
		var offset = (newYMaxVal - newYMinVal) * 0.02;
		this.yMinVal = newYMinVal - offset;
		this.yMaxVal = newYMaxVal + offset;
	} else {
		this.yMinVal = -1;
		this.yMaxVal = -1;
	}

	this.displayOffset = (calcPeriodInSec(this.ratesStartTime, this.displayStartTime) - this.firstSec) / this.secPerPixel;
//	log("After cleaning - length: " + this.rates.length + " yMinVal: " + this.yMinVal + " yMaxVal: " + this.yMaxVal + " startTime: " + this.ratesStartTime);
};

BoxChart.prototype.ensureYOffset = function() {
	// don't let the current level line go too close to the top/bottom of the chart
	var tooClose = (this.yMaxVal - this.yMinVal) * 0.07;
	if (tooClose == 0) {
		tooClose = this.yMinVal * 0.0001;
	}
	if (this.currentLevel < this.yMinVal + tooClose) {
		var newMinVal = Math.min(this.yMinVal, this.currentLevel);
		this.yMinVal = newMinVal - (this.yMaxVal - newMinVal) * 0.07;
		if (this.yMinVal == this.currentLevel) {
			this.yMinVal = this.yMinVal - tooClose;
		}
	}
	if (this.currentLevel > this.yMaxVal - tooClose) {
		var newMaxVal = Math.max(this.yMaxVal, this.currentLevel);
		this.yMaxVal = newMaxVal + (newMaxVal - this.yMinVal) * 0.07;
		if (this.yMaxVal == this.currentLevel) {
			this.yMaxVal = this.yMaxVal + tooClose;
		}
	}
};

BoxChart.prototype.showWaitingForExpiry = function() {
	this.waitingForExpiry = true;
};

BoxChart.prototype.showCall = function() {
	this.call = true;
};

BoxChart.prototype.hideCall = function() {
	this.call = false;
};

BoxChart.prototype.showPut = function() {
	this.put = true;
};

BoxChart.prototype.hidePut = function() {
	this.put = false;
};

BoxChart.prototype.getCanScrollBack = function(chart) {
	if (chart.displayOffset > 100) {
		return true;
	}
	return false;
};

BoxChart.prototype.getCanScrollForward = function(chart) {
	var ct = new Date();
	fixTime(ct, chart.serverTimeOffset, chart.timezoneOffset);
	if (ct.getTime() > chart.timeEstClosing.getTime()) {
		ct.setTime(chart.timeEstClosing.getTime());
	}
	// if the display start time is more than an hour old and the closing is not late
	// we must be showing the previous hour
	ct.setHours(ct.getHours() - 1);
	if (chart.displayStartTime.getTime() < ct.getTime()) {
		return true;
	}
	return false;
};

BoxChart.prototype.scrollBack = function(chart) {
	chart.displayStartTime.setHours(chart.displayStartTime.getHours() - 1);
	chart.displayOffset = (calcPeriodInSec(chart.ratesStartTime, chart.displayStartTime) - chart.firstSec) / chart.secPerPixel;
	chart.repaint();
//	initXLabels();
};

BoxChart.prototype.scrollForward = function(chart) {
	chart.displayStartTime.setHours(chart.displayStartTime.getHours() + 1);
	chart.displayOffset = (calcPeriodInSec(chart.ratesStartTime, chart.displayStartTime) - chart.firstSec) / chart.secPerPixel;
	chart.repaint();
//	initXLabels();
};

BoxChart.prototype.onMouseUp = function(e) {
	log("onMouseUp");
	var x, y;
	// Get the mouse position relative to the canvas element.
	if (e.layerX || e.layerX == 0) { // Firefox
		x = e.layerX;
		y = e.layerY;
	} else if (e.offsetX || e.offsetX == 0) { // Opera
		x = e.offsetX;
		y = e.offsetY;
	}

	log("x: " + x + " y: " + y + " this: " + this.chart.oppId);
	// back button
	if (isInside(x, y, 0, (this.chart.canvas.height - CHART_OFFSET) / 2 - 15, CHART_OFFSET, 30)) {
		if (this.chart.getCanScrollBack(this.chart)) {
			this.chart.scrollBack(this.chart);
		}
	}
	// forward button
	if (isInside(x, y, this.chart.canvas.width - CHART_OFFSET, (this.chart.canvas.height - CHART_OFFSET) / 2 - 15, CHART_OFFSET, 30)) {
		if (this.chart.getCanScrollForward(this.chart)) {
			this.chart.scrollForward(this.chart);
		}
	}
};

function resize() {
//	alert("window.orientation: " + window.orientation);
	var newLayout = window.orientation != undefined ? window.orientation == 0 || window.orientation == 180 : window.innerWidth < window.innerHeight;
	if (portrait != newLayout) {
		portrait = newLayout;
		changePageOrientation();
	}
	try {
		var i;
		for (i = 0; i < boxesOpportunities.length; i++) {
			if (null != boxCharts[i]) {
				var tmpBox = document.getElementById(boxCharts[i].oppId);
				boxCharts[i].canvas.setAttribute('width', tmpBox.offsetWidth - (portrait ? 5 : 175));
				boxCharts[i].canvas.setAttribute('height', 150);
				boxCharts[i].secPerPixel = 3600 / (boxCharts[i].canvas.width - 2 * CHART_OFFSET);
				boxCharts[i].cleanUnneededRates();
				boxCharts[i].repaint();
			}
		}
	} catch (e) {
		log(e);
	}
};
window.onresize = resize;

function refreshCharts() {
	for (i = 0; i < boxesOpportunities.length; i++) {
		try {
			boxCharts[i].repaint();
		} catch (e) {
			log(e);
		}
	}
}

// Profit Line has 6 sec per pixel where we have 600 pixels. Normally here we will not have much more than 300 that is 12 sec per pixel
setInterval("refreshCharts()", 10000);

//var refreshChartsIntervalId = 0;
//function startRefreshCharts() {
//	try {
//		if (refreshChartsIntervalId > 0) {
//			clearInterval(refreshChartsIntervalId);
//		}
//		var minSecPerPixel = -1;
//		...
//	} catch (e) {
//		log(e);
//	}
//}

//resize();

function changePageOrientation() {
	for (i = 0; i < boxesOpportunities.length; i++) {
		var box = document.getElementById("box" + i);
		for (l = 0; l < boxesOpportunities[i].length; l++) {
			var id = boxesOpportunities[i][l];
			var marketId = document.getElementById(id + "_marketId").innerHTML;
			var state = document.getElementById(id + "_" + SUBSCR_FLD_ET_STATE).innerHTML;
			var lastInvest = document.getElementById(id + "_" + SUBSCR_FLD_LAST_INVEST).innerHTML;
			var levelColor = document.getElementById(id + "_" + SUBSCR_FLD_AO_LEVEL).attributes["class"].value;
			
			var tmpValues = new Array();
			var tmp;
			for (m = 1; m <= schema.length; m++) {
				tmp = document.getElementById(id + "_" + m);
				if (null != tmp) {
					tmpValues[m] = tmp.innerHTML;
				} else {
					tmpValues[m] = null;
				}
			}
			
			box.removeChild(document.getElementById(id));

			var template = document.getElementById("template_opportunity" + (portrait ? "_portrait" : ""));
			var str = template.innerHTML.replace(/'oppId'/g, id);
			str = str.replace(/'marketId'/g, marketId);
			var tmpDiv = document.getElementById("tmpdiv");
			tmpDiv.innerHTML = str;

			setMobileSelectsCurrentValues(id,
					marketId,
					tmpValues[SUBSCR_FLD_ET_SCHEDULED],
					tmpValues[SUBSCR_FLD_FILTER_SCHEDULED],
					tmpValues[SUBSCR_FLD_ET_EST_CLOSE]);

			var opp = document.getElementById(id);
//			box.style.height = portrait ? "414px" : "247px";
			
			box.appendChild(opp);
			tmpDiv.innerHTML = "";

			changeBoxState(
					id,
					marketId,
					state,
					tmpValues[SUBSCR_FLD_ET_EST_CLOSE],
					tmpValues[SUBSCR_FLD_LAST_INVEST],
					tmpValues[SUBSCR_FLD_ET_SUSPENDED_MESSAGE]);

			for (m = 1; m <= schema.length; m++) {
				tmp = document.getElementById(id + "_" + m);
				if (null != tmp) {
					tmp.innerHTML = tmpValues[m];
				}
			}

			document.getElementById(id + "_" + SUBSCR_FLD_AO_LEVEL).attributes["class"].value = levelColor;
			
			boxCharts[i].canvas = document.getElementById(id + "_chart");
			boxCharts[i].canvasContext = boxCharts[i].canvas.getContext("2d");
			boxCharts[i].canvas.addEventListener('mouseup', boxCharts[i].onMouseUp, false);
			boxCharts[i].canvas.chart = boxCharts[i];
		}
	}
}