function g(id){return document.getElementById(id);}
function isUndefined(el) {
	return (typeof el == 'undefined' || el == null);
}

window.addEventListener('message',function(event) {
	var bubblesServer = (isLive) ? 'https://web.visualtrade.com' : 'https://ao.visualtrade.com';
	if(event.origin !== bubblesServer) return;
	if (!isUndefined(event.data.respData)) {
		var url = '?';
		url += 'errorCode=' + event.data.respData.errorCode;
		url += '&errorMessages=' + encodeURIComponent(event.data.respData.errorMessages);
		
		window.history.pushState({}, "Bubbles", "/mobile/bubbles.xhtml" + url);
	}
},false)

window.addEventListener("orientationchange", function() {
	setBgr(window.orientation);
}, false);

window.addEventListener("load", function() {
	setBgr(window.orientation);
	if (isActive) {
		g('bubbles_holder').style.display = "block";
		g('coming_soon_bgr').style.display = "none";
	} else {
		g('bubbles_holder').style.display = "none";
		g('coming_soon_bgr').style.display = "block";
	}
}, false);

function setBgr(orientation) {
	if (!isActive) {
		if (orientation == 0) {
			g('coming_ver').style.display = "block";
			g('coming_hor').style.display = "none";
		} else {
			g('coming_ver').style.display = "none";
			g('coming_hor').style.display = "block";
		}
	}
}