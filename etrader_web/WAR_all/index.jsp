<%@page import="il.co.etrader.web.bl_managers.AdminManager,
				il.co.etrader.util.CommonUtil,
				il.co.etrader.web.bl_managers.ApplicationData;"%>


getLocalHostUrl()
<% AdminManager.runTestQuery();   %>


<%
    String strHeaderName = "host";
    java.util.HashMap urls = new java.util.HashMap();

    //urls.put("anyoption.com","http://www.anyoption.com/jsp/index.jsf");
    urls.put("live.anyoption.com","http://www.anyoption.com/jsp/index.jsf?from=israel");

    String host = request.getHeader(strHeaderName);
	String url="";
    if (host != null && host.length() != 0 && urls.get(host.toLowerCase()) != null) {
       host = host.toLowerCase();
       url = (String)urls.get(host);
    } else if (host != null && host.length() != 0 &&
    		!host.toLowerCase().equalsIgnoreCase("www.anyoption.com") &&
    		!host.toLowerCase().equalsIgnoreCase("www.tlvtrade.co.il") &&
    		!host.toLowerCase().equalsIgnoreCase("wwtest.tlvtrade.co.il") &&
    		!host.toLowerCase().equalsIgnoreCase("www.miopciones.com") &&
    		!host.toLowerCase().equalsIgnoreCase("www.testenv.anyoption.com") &&
    		!host.toLowerCase().equalsIgnoreCase("wwtest.miopciones.com")) {
    	url = CommonUtil.getProperty("homepage.url") + "jsp/index.jsf";
        try { // redirect per pretty URL
            String prefix = "en";
        	prefix = ApplicationData.getLinkLocalePrefix(session, request, response);
        	url = CommonUtil.getProperty("homepage.url") + prefix + "/";
        } catch (Exception e) {}
    } else {
        url = request.getContextPath()+"/jsp/index.jsf";
        try { // redirect per pretty URL
        	String prefix = "en";
        	prefix = ApplicationData.getLinkLocalePrefix(session, request, response);
        	url = request.getContextPath()+ "/" + prefix + "/";
        } catch (Exception e) {}
    }
    response.setStatus(301);
    response.setHeader( "Location", url);
    response.setHeader( "Connection", "close" );
%>

