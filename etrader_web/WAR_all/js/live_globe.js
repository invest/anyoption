var LI_CITY 							= 3;  //City
var LI_COUNTRY 							= 4;  // Country
var LI_ASSET 							= 5;  // Asset
var LI_PROD_TYPE						= 6;  // binary/+/100
var LI_INV_TYPE 						= 7;  // call/put/buy/sell
var LI_LEVEL 							= 8;  // 1.12345
var LI_AMOUNT 							= 9;  // formatted for display investment amount in user currency
var LI_TIME 							= 10; // number of millis since epoch
var LI_LAT 								= 11;
var LI_LONG 							= 12;
var LI_MARKET_NAME						= 13;
var LI_MARKET_DECIMAL					= 14;
var LI_COUNTRY_NAME						= 15;

var currentInvestment;
//@
function formatValues_globe(itemUpdate) {
	
	if (itemUpdate == null) {
		return;
	}
	
    //investment detail
    investment = new Object();
	investment.city 			= itemUpdate.getValue("LI_CITY");
	investment.country 			= getCountryName(itemUpdate.getValue("LI_COUNTRY"));
	investment.level 			= itemUpdate.getValue("LI_LEVEL");
	investment.lat 				= itemUpdate.getValue("LI_LAT");
	investment.lng 				= itemUpdate.getValue("LI_LONG");
	investment.asset_name 		= getMarketName(itemUpdate.getValue("LI_ASSET"));
	investment.amount			= itemUpdate.getValue("LI_AMOUNT");
	investment.imageFlagGlobe	= "<div><img src = '" + getCountryFlagImage(itemUpdate.getValue("LI_COUNTRY")) + "' width='15px' height='16px' /></div>";
	investment.imageFlag		= "<img src = '" + getCountryFlagImage(itemUpdate.getValue("LI_COUNTRY")) + "' width='15px' height='16px' />";
	investment.prodType			= "<span class='icon_set_1 icon_set_1_marcet_"+itemUpdate.getValue("LI_PROD_TYPE")+"'></span>";
	if (getInvTypeImage(itemUpdate.getValue("LI_INV_TYPE")) != '') {
		investment.invType			= "<img src = '" + getInvTypeImage(itemUpdate.getValue("LI_INV_TYPE")) + "' width='10px' height='10px' style='margin-right:5px;margin-left:5px;'  />";
	}else {
		investment.invType = '';
	}
	investment.triangle			= "<img src = '" + imagePath + "/globe_corner.png" + "' width='18px' height='9px' />";

	var c = {"name":"Placeholder","coords":[investment.lat, investment.lng]};

	if (investment != null && investment.level != null && investment.level != 'undefined') {
		var num = getMarketsDecimalPoint(itemUpdate.getValue("LI_ASSET"));
		//investment.level = new Number(investment.level).toFixed(num);		
		var str = investment.level;
		var strReplace = str.replace(',','');
		var newNum = new Number(strReplace).toFixed(num);		
		investment.level = addThousandsCommaSeparators(newNum.toString());						
	}
	
	if (investment != null && investment.amount != null && investment.amount != 'undefined') {
		var amount = itemUpdate.getValue("LI_AMOUNT");
		if(amount.indexOf("руб.") > -1) {
			var temp = amount.replace("руб.", "").trim();
			amount = "руб." + temp;
		}
		investment.amount = amount;
	}
	
	if (supportWebGL) {
		var goToLocation = false;
		if (currentInvestment == null || currentInvestment == 'undefined' || (investment.lat != currentInvestment.lat || investment.lng != currentInvestment.lng)) {
			goToLocation = true;
		}
		currentInvestment = investment;
		globe.insertNewInvestment(c, investment, goToLocation);
	} else {
		showInvestmentNotWebGL(investment);
	}
}

//@@
//function updateItem_globe(item, updateInfo) {
//	if (item == null) {
//		return;
//	}
//	if (updateInfo == null) {
//		return;
//	}
//	//add field to change market display name
//	if (updateInfo.isValueChanged(LI_ASSET)) {
//		updateInfo.addField(LI_MARKET_NAME, getMarketName(updateInfo.getNewValue(LI_ASSET)));
//		updateInfo.addField(LI_MARKET_DECIMAL, getMarketsDecimalPoint(updateInfo.getNewValue(LI_ASSET)));
//	}
//
//	//add field to change country display name
//	if (updateInfo.isValueChanged(LI_COUNTRY)) {
//		updateInfo.addField(LI_COUNTRY_NAME, getCountryName(updateInfo.getNewValue(LI_COUNTRY)));
//	}
//}
function getMarketName(marketId) {
	if (marketsDisplayName[marketId]) {
		return marketsDisplayName[marketId];
	}
	return "";
}
function getCountryName(countryId) {
	for (var i = 0; i < countriesDisplayName.length; i++) {
		if (countriesDisplayName[i][0] == countryId) {
			return countriesDisplayName[i][1];
		}
	}
	return "";
}
function getMarketsDecimalPoint(marketId) {
	for (var i = 0; i < marketsDecimalPoint.length; i++) {
		if (marketsDecimalPoint[i][0] == marketId) {
			return marketsDecimalPoint[i][1];
		}
	}
	return "5";
}
function getCountryFlagImage(countryId) {
	return imagePath_flags + "/flags/" + countryId + ".png";
}
function getProdTypeImage(typeId) {
	var TypeUrl = image_context_path + "/icon_BO.png";
	if(typeId == 3) {
		TypeUrl = image_context_path + "/optionPlus_icon_sm.png";
	}
	if(typeId == 4 || typeId == 5) {
		TypeUrl = image_context_path + "/0100_icon_sm.png";
	}
	if(typeId == 6) {
		TypeUrl = image_context_path + "/bubbles_icon_sm.png";
	}
	if(typeId == 7 || typeId == 8) {
		TypeUrl = image_context_path + "/dynamics_icon_sm.png";
	}
	return TypeUrl;
}
function getInvTypeImage(typeId) {
	var TypeUrl = "";
	if(typeId == 1 || typeId == 4 || typeId == 7) {
		 TypeUrl = image_context_path + "/arrowTable_Call.png";
	}
	if(typeId == 2 || typeId == 5 || typeId == 8) {
		TypeUrl = image_context_path + "/arrowTable_Put.png";
	}
	return TypeUrl;
}
function addThousandsCommaSeparators(str) {
	var strRightToDot = str.substring(0, str.indexOf('.'));
	var strLeftToDot = str.substring(str.indexOf('.'));
	
	var newStrRightToDot = strRightToDot.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	var newNum = newStrRightToDot + strLeftToDot;
	return newNum;
}