var LTT_KEY		= "key";//1;
var LTT_COMMAND	= "command";//2;
var LTT_MARKET1	= "LTT_MARKET1";//3; // Market 1
var LTT_OPP_ID1	= "LTT_OPP_ID1";//4; // currentOpportunity 1
var LTT_TREND1	= "LTT_TREND1";//5; // percentOfCalls 1 (A number for example 0.33 will mean 33% CALLs and 67% PUTs)
var LTT_MARKET2	= "LTT_MARKET2";//6; // market 2
var LTT_OPP_ID2	= "LTT_OPP_ID2";//7; // currentOpportunity 2
var LTT_TREND2	= "LTT_TREND2";//8; // percentOfCalls 2 (A number for example 0.33 will mean 33% CALLs and 67% PUTs)
var LTT_MARKET3	= "LTT_MARKET3";//9; // market 3
var LTT_OPP_ID3	= "LTT_OPP_ID3";//10; // currentOpportunity 3
var LTT_TREND3	= "LTT_TREND3";//11; // percentOfCalls 3 (A number for example 0.33 will mean 33% CALLs and 67% PUTs)

var LTT_MARKET_NAME1 ;
var LTT_MARKET_NAME2 ;
var LTT_MARKET_NAME3 ;
var LTT_TREND_CALL1 ;
var LTT_TREND_PUT1 ;
var LTT_TREND_CALL2 ;
var LTT_TREND_PUT2 ;
var LTT_TREND_CALL3 ;
var LTT_TREND_PUT3 ;

var trendsCallWidth_1;
var trendsPutWidth_1;
var trendsCallWidth_2;
var trendsPutWidth_2;
var trendsCallWidth_3;
var trendsPutWidth_3;

function updateTrendsVisual(callProc,trendNum,domNode,marketID){
	try{
	var elms = domNode.getElementsByTagName('*');
	for(var i=0; i<elms.length; i++){
		if(elms[i].id == 'trends_graph_proc_call_'+trendNum){
			var tradeBox_trends_graph_proc_call = elms[i];
		}else if(elms[i].id == 'trends_graph_sep_'+trendNum){
			var tradeBox_trends_graph_sep = elms[i];
		}else if(elms[i].id == 'trends_graph_proc_put_'+trendNum){
			var tradeBox_trends_graph_proc_put = elms[i];
		}else if(elms[i].id == 'trends_graph_put_'+trendNum){
			var trends_graph_put = elms[i];
		}else if(elms[i].id == 'trends_graph_main_'+trendNum){
			var trends_graph_main = elms[i];
		}
	}
	if(marketID == '0'){
		trends_graph_main.style.display = "none";
	}
	else{
		if(callProc !=null && callProc != "0") { 
			trends_graph_main.style.display = "block";
			var trends_call = Math.round(callProc * 100);
			var trends_put = 100 - trends_call;
			
			// var tradeBox_trends_graph_proc_call = g('trends_graph_proc_call_'+trendNum);
			// var tradeBox_trends_graph_sep = g('trends_graph_sep_'+trendNum);
			if(trends_call === 0){
				tradeBox_trends_graph_proc_call.style.display = "none";
				tradeBox_trends_graph_sep.style.display = "none";
			}else{
				tradeBox_trends_graph_sep.style.display = "block";
				tradeBox_trends_graph_proc_call.style.display = "block";
				tradeBox_trends_graph_proc_call.innerHTML = trends_call+"%";
				tradeBox_trends_graph_proc_call.style.left = (trends_call/2)+"%";
			}
			// var tradeBox_trends_graph_proc_put = g('trends_graph_proc_put_'+trendNum);
			if(trends_put === 0){
				tradeBox_trends_graph_proc_put.style.display = "none";
			}else{
				tradeBox_trends_graph_proc_put.style.display = "block";
				tradeBox_trends_graph_proc_put.innerHTML = trends_put+"%";
				tradeBox_trends_graph_proc_put.style.right = (trends_put/2)+"%";					
			}
			
			trends_graph_put.style.width = trends_put+'%';
		}
	}
	}catch(e){

	}
}

function updateItem_live_trade(key, visualUpdate, domNode) {
	try{
	//add field to change display market's name.
	var marketID1 = visualUpdate.getChangedFieldValue(LTT_MARKET1); 
	if (marketID1!=null && marketID1 != "0" ) {
		LTT_MARKET_NAME1 = getMarketName(marketID1);
		visualUpdate.setCellValue("LTT_MARKET_NAME1", LTT_MARKET_NAME1);
		visualUpdate.setCellValue("LTT_MARKET1", marketID1);
	}
	var marketID2 = visualUpdate.getChangedFieldValue(LTT_MARKET2); 
	if (marketID2!=null && marketID2 != "0" ) {
		LTT_MARKET_NAME2 = getMarketName(marketID2);
		visualUpdate.setCellValue("LTT_MARKET_NAME2", LTT_MARKET_NAME2);
		visualUpdate.setCellValue("LTT_MARKET2", marketID2);
	}
	var marketID3 = visualUpdate.getChangedFieldValue(LTT_MARKET3); 
	if (marketID3!=null && marketID3 != "0" ) {
		LTT_MARKET_NAME3 = getMarketName(marketID3);
		visualUpdate.setCellValue("LTT_MARKET_NAME3", LTT_MARKET_NAME3);
		visualUpdate.setCellValue("LTT_MARKET3", marketID3);
	}
	
	//add field to call and put trends.
	var table_trading = document.getElementById("trends");
	//first market.
	var trend1 = visualUpdate.getChangedFieldValue(LTT_TREND1);
	//if ((trend1!=null && trend1 != "0")) {
		updateTrendsVisual(trend1,1,domNode,marketID1);
		 // calculatePercentage(trend1, 1);
		 // visualUpdate.setCellValue("LTT_TREND_PUT1", LTT_TREND_PUT1);
		 // visualUpdate.setCellValue("LTT_TREND_CALL1", LTT_TREND_CALL1);
	//}
	//second market.
	var trend2 = visualUpdate.getChangedFieldValue(LTT_TREND2);
	//if (trend2!=null && trend2 != "0") {
		updateTrendsVisual(trend2,2,domNode,marketID2);
		 // calculatePercentage(trend2, 2);
		 // visualUpdate.setCellValue("LTT_TREND_PUT2", LTT_TREND_PUT2);
		 // visualUpdate.setCellValue("LTT_TREND_CALL2", LTT_TREND_CALL2);
	//}
	//third market.
	var trend3 = visualUpdate.getChangedFieldValue(LTT_TREND3);
	//if (trend3!=null && trend3 != "0") {
		updateTrendsVisual(trend3,3,domNode,marketID3);
		 // calculatePercentage(trend3, 3);
		 // visualUpdate.setCellValue("LTT_TREND_PUT3", LTT_TREND_PUT3);
		 // visualUpdate.setCellValue("LTT_TREND_CALL3", LTT_TREND_CALL3);
	//}
	
	}catch(e){

	}
	try{
		if (checkAllClose()) {
			//show off hour banner 0
			switchLiveTrends(0);
		} else {
			//show the market table 1
			switchLiveTrends(1);
		}
	} catch(e){
		logIt({'type':3,'msg':e});
	}
	/*
//function formatValues_live_trade(item, itemUpdate) {
	//change chart when first maket changed.
	var trends_line_1 = getChildOfType(domNode, "DIV", 1);
	var trends_chart_1 = getChildOfType(trends_line_1, "DIV", 2);
	var chart_call_1 = getChildOfType(trends_chart_1, "DIV", 1);
	var chart_put_1 = getChildOfType(trends_chart_1, "DIV", 3);
	chart_call_1.style.width = trendsCallWidth_1;
	chart_put_1.style.width = trendsPutWidth_1;
	if (trendsCallWidth_1 == "0%" || trendsPutWidth_1 == "0%") {
		getChildOfType(trends_chart_1, "DIV", 2).style.display = 'none';
	} else {
		getChildOfType(trends_chart_1, "DIV", 2).style.display = 'block';
	}
	
	//change chart when second maket changed.
	var trends_line_2 = getChildOfType(domNode, "DIV", 2);
	var trends_chart_2 = getChildOfType(trends_line_2, "DIV", 2);
	var chart_call_2 = getChildOfType(trends_chart_2, "DIV", 1);
	var chart_put_2 = getChildOfType(trends_chart_2, "DIV", 3);
	chart_call_2.style.width = trendsCallWidth_2;
	chart_put_2.style.width = trendsPutWidth_2;
	if (trendsCallWidth_2 == "0%" || trendsPutWidth_2 == "0%") {
		getChildOfType(trends_chart_2, "DIV", 2).style.display = 'none';
	} else {
		getChildOfType(trends_chart_2, "DIV", 2).style.display = 'block';
	}
	
	//change chart when third maket changed.
	var trends_line_3 = getChildOfType(domNode, "DIV", 3);
	var trends_chart_3 = getChildOfType(trends_line_3, "DIV", 2);
	var chart_call_3 = getChildOfType(trends_chart_3, "DIV", 1);
	var chart_put_3 = getChildOfType(trends_chart_3, "DIV", 3);
	chart_call_3.style.width = trendsCallWidth_3;
	chart_put_3.style.width = trendsPutWidth_3;
	if (trendsCallWidth_3 == "0%" || trendsPutWidth_3 == "0%") {
		getChildOfType(trends_chart_3, "DIV", 2).style.display = 'none';
	} else {
		getChildOfType(trends_chart_3, "DIV", 2).style.display = 'block';
	}
	
	trends_line_1.style.display = 'block';
	trends_line_2.style.display = 'block';
	trends_line_3.style.display = 'block';
	// print only exist market.
	if ("0" == marketID1) {
		trends_line_1.style.display = 'none';
	}
	if ("0" == marketID2) {
		trends_line_2.style.display = 'none';
	}
	if ("0" == marketID3) {
		trends_line_3.style.display = 'none';
	}
	*/
}//formatValues_live_trade

function getMarketName(marketId) {
	if (marketsDisplayName[marketId]) {
		return marketsDisplayName[marketId].replace('&amp;', '&');
	}
	return "";
}
var intreval;
function sendTrendToTradeBox(el,action) {
	var marketId = parseInt(el.parentNode.getElementsByTagName('span')[0].innerHTML);
	fromGraph = 3; //Trading Trends page.
	showMarketsMenu_box = 0;
	goToByScroll('tradeBox_main_0 ', -30);
	selectMarket(marketId);
}

function simulatePutOrCallTrends(invTypeId) {//removed
	try {
		var boxTemp = document.getElementById('box0');
		var index;
		boxTemp = getChildOfType(boxTemp, "DIV", 1);
		boxTemp = getChildOfType(boxTemp, "DIV", 2);
		boxTemp = getChildOfType(boxTemp, "DIV", 1);
		if (!(getChildOfType(boxTemp, "IMG", 1).src.toString().indexOf('trade_loading.gif') > -1)) {
			if (invTypeId == 1) {
				index = 1;
			} else {
				index = 2;
			}
			var img = getChildOfType(boxTemp, "IMG", index);
			var imgSrc = img.src.toString();
			
			if(imgSrc.indexOf('call.gif') > -1 || imgSrc.indexOf('put.gif') > -1) {
				clearInterval(intreval);
				if (typeof img.onclick == "function") {
		    		img.onclick.apply(boxTemp);
				}
				var tradeBox = document.getElementById('box0');
				//window.scroll(0,findPos(tradeBox));
			}
		}
	} catch(e) {
		clearInterval(intreval);
	}
}

function findPos(obj) {
	var curtop = 0;
	if (obj.offsetParent) {
		do {
			curtop += obj.offsetTop;
		} while (obj = obj.offsetParent);
	return [curtop];
	}
}

function switchLiveTrends(flag) {//moved
	var liveTradingTrends =  g("liveTradingTrends");
	var liveTradingTrendsOffHoursLogin = g("liveTradingTrendsOffHoursLogin");
	if (flag == 0) {
		liveTradingTrends.style.display = 'none';
		liveTradingTrendsOffHoursLogin.style.display = 'block';	
	} else {
		liveTradingTrends.style.display = 'block';
		liveTradingTrendsOffHoursLogin.style.display = 'none';
	}
	
	var trendsBox = g("trendsBox");
	if(trendsBox != null) {
		if(flag == 0) {
			if (userLogin) {
				trendsBox.style.height = "350px";
			}
		} else {
			trendsBox.style.height = "220px";
		}
	}
	switchLiveGlobe(flag);
}

function switchLiveGlobe(flag) {//moved
	var globeLogout =  g("globeLogout");
	var globeLogoutOffHours = g("globeLogoutOffHours");
	if(globeLogout != null){
		if (flag == 0) {
			globeLogout.style.display = 'none';
			globeLogoutOffHours.style.display = 'block';	
		} else {
			globeLogout.style.display = 'block';
			globeLogoutOffHours.style.display = 'none';
		}
	}
}

function calculatePercentage(trendsCall, i) {//removed
	trendsCall  = trendsCall * 100;
	var trends_put = 100 - trendsCall;
	var trends_call_percent = Math.round(trendsCall) + "%";
	var trends_put_percent = Math.round(trends_put) + "%";
	
	if (trendsCall != "0") {
		if(i==1) trendsCallWidth_1 = (Math.round(trendsCall) - 1) + "%";
		if(i==2) trendsCallWidth_2 = (Math.round(trendsCall) - 1) + "%";
		if(i==3) trendsCallWidth_3 = (Math.round(trendsCall) - 1) + "%";
		
	} else {
		if(i==1) trendsCallWidth_1 = "0%";
		if(i==2) trendsCallWidth_2 = "0%";
		if(i==3) trendsCallWidth_3 = "0%";

		trends_call_percent = '';
	}
	if (trends_put != "0") {
		if(i==1) trendsPutWidth_1 = (Math.round(trends_put) - 1) + "%";
		if(i==2) trendsPutWidth_2 = (Math.round(trends_put) - 1) + "%";
		if(i==3) trendsPutWidth_3 = (Math.round(trends_put) - 1) + "%";
	} else {
		if(i==1) trendsPutWidth_1 = "0%";
		if(i==2) trendsPutWidth_2 = "0%";
		if(i==3) trendsPutWidth_3 = "0%";
		
		trends_put_percent = '';
	}
	if(i=1) {
		LTT_TREND_CALL1= trends_call_percent ;
		LTT_TREND_PUT1= trends_put_percent ;
	}
	if(i=2) {
		LTT_TREND_CALL2= trends_call_percent ;
		LTT_TREND_PUT2= trends_put_percent ;
	}
	if(i=3) {
		LTT_TREND_CALL3= trends_call_percent ;
		LTT_TREND_PUT3= trends_put_percent ;
	}
}