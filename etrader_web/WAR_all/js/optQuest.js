/*********** optQuest.js ************/

function updateOptQuestValues() {
	var dataa = "numUserAnswers=" + (questionsIdArr.length + 3);
	var item;
	var ii = 0;
	var index = -1;
	var hasAnswer;
	for(var i = 0; i < questionsIdArr.length; i++) {
		item = document.getElementById("regOptQuestionnaireForm:question_" + questionsIdArr[i][0]);
		hasAnswer = false;
		if (item.type!=null && item.type!=undefined) {
			if (item.value!=null && item.value!=undefined && item.value!="") {
				dataa += "&userAnswer" + ii + "Id=" + questionsIdArr[i][1];
				dataa += "&answer" + ii + "Id=" + item.value;				
			} else {
				dataa += "&userAnswer" + ii + "Id=" + questionsIdArr[i][1];
			}	
			ii++;
		} else {
			var items = item.getElementsByTagName("input");
			var k = 0;
			for (var j = 0; j < items.length; j++) {
				if (items[j].type == "checkbox") {
					if (items[j].checked) {
						dataa += "&userAnswer" + ii + "Id=" + questionsIdArr[i][1][j];
						dataa += "&answer" + ii + "Id=" + items[j].value;
						index = ii;
					} else {
						if (k == 3) { // index of the check box with text field
							var inputTextVal = document.getElementById("regOptQuestionnaireForm:other_text").value;
							if (inputTextVal != undefined && inputTextVal!= null &&  inputTextVal != "") {
								items[k].checked = true;
								dataa += "&userAnswer" + ii + "Id=" + questionsIdArr[i][1][j];
								dataa += "&answer" + ii + "Id=" + items[j].value;
								index = ii;
							} else {
								dataa += "&userAnswer" + ii + "Id=" + questionsIdArr[i][1][j];
								index = -1;
							}
						} else {
							dataa += "&userAnswer" + ii + "Id=" + questionsIdArr[i][1][j];
							index = -1;
						}
					}
					ii++;
					k++;
				} 
			}
		}
	}
	if (index != -1) {
		dataa += "&answer" + index + "Text=" + encodeURIComponent(document.getElementById("regOptQuestionnaireForm:other_text").value);
	}	
	
	$.ajax({
	   type: "POST",
	   url: "ajax.jsf",
	   data: dataa,
	   success:  function(result) {
		   if (result != null && result == "error") {
			   location.reload();
		   }
   		}
	});
}

function identifyCheckbox(id, checkbox) {
	var item = document.getElementById("regOptQuestionnaireForm:question_" + id);
	var items = item.getElementsByTagName("input");
	if (items[items.length - 1] == checkbox) {
		document.getElementById("regOptQuestionnaireForm:other_text").value="";
	}	
}

function showNotNowMessage() {
	var div = document.getElementById("not_now_message");
	div.style.display="block";
}
