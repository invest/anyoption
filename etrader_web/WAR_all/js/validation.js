var emailValidationRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

function validate() {
	var errormsg = null;
	errormsg = validateUserName(document.forms['login'].elements['login:j_username'].value);
	if (errormsg == null) {
		errormsg = validatePassword(document.forms['login'].elements['login:j_password'].value);
	}
	if (errormsg == null) {
		document.forms['login'].onsubmit = function(){return true;}
		myfaces.oam.submitForm('login','login:hiddenbtn');		
	} else {
		document.getElementById('login:errorlogin').innerHTML = errormsg;
		try {
			document.getElementById('login:errorloginMsg').innerHTML = "";
		} catch (eee) {
			//do nothing sometimes we dont have this msg so we will get null
		}
	}
}

function validateUserName(username) {
    var error = null;
    var illegalChars = /[\W_]/; // allow only letters and numbers

    if (username == "") {
        error = emptyfield;
    } else if (!emailValidationRegEx.test(username)) {
	    if (username.length < 6) {
	        error = usernameSmall;
	    } else if (illegalChars.test(username)) {
	        error = invalidUsername;
	    }
    }

   return error;
}

function validatePassword(password) {
    var error = null;
    var illegalChars = /[\W_]/; // allow only letters and numbers

    if (password == "") {
        error = emptyfield;
    } else if (password.length < 6) {
        error = passwordSmall;
    } else if (illegalChars.test(password)) {
        error = lettersandnumbers;
    }

   return error;
}