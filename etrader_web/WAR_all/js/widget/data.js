var allBinaryMarketArray = new Array();
var defaultMarketId;
var currentMarketId;
var offhours = true;

//navigate to anyoption site
function navigateToPage(page) {
	var market = '';	
	if (page == null || page == undefined) {
		page = 'trade_binary_options';
	}	
	if (page == 'trade_binary_options') {
		market = '&m=' + currentMarketId;
	}
	var fullURL = parent.document.URL;
	var queryString = fullURL.substring(fullURL.indexOf('?') + 1, fullURL.length);	
	var win = window.open(anyoptionHPURL + 'jsp/landing.jsf?pageR=' + page + market + '&' + queryString, '_blank');
	win.focus();	
}
			
// format the expiration date			
function formatDate(est_date) {
	var currdate = new Date();
	var min = est_date.getMinutes();
	if (est_date.getDate() == currdate.getDate() && est_date.getMonth() == currdate.getMonth() && est_date.getFullYear() == currdate.getFullYear()) {
		if (new Number(est_date.getMinutes()) < new Number("10")) {
			min = "0" + est_date.getMinutes();
		}
		est_date = est_date.getHours() + ":" + min + " " + bundle_msg_today;
	} else {
		if (new Number(est_date.getMinutes()) < new Number("10")) {
			min = "0" + est_date.getMinutes();
		}
		var year = new String(est_date.getFullYear());
		est_date = est_date.getHours() + ":" + min + ", " + est_date.getDate() + "." + (est_date.getMonth()+1) + "." + year.substring(2);
	}
	return est_date;
} 


// get element 
function getElm(id) {
	return document.getElementById(id);
}

// hide/display asset list 
function displayAssetList() {
	var assetList = getElm('treeM');
	var status = assetList.style.display;
	if (status == 'block') {
		assetList.style.display = 'none';
	} else {
		assetList.style.display = 'block';
	}
}		

// build the markets list
function buildMarketsListData() {
	defaultMarketId = JSONData.defaultMarketId;
	for (var i = 0; i < JSONData.markets.length; i++) {
		var market = new Object();
		market.id 						= JSONData.markets[i].id;
		market.groupId 					= JSONData.markets[i].groupId;
		market.name 					= JSONData.markets[i].name;
		market.currentLevel  			= JSONData.markets[i].currentLevel;
		market.currentLevelStr  		= JSONData.markets[i].currentLevelStr;
		market.opportunityId 			= JSONData.markets[i].opportunityId; 
		market.timeEstClosingMillsec 	= JSONData.markets[i].timeEstClosingMillsec;
		market.pageOddsWin 				= JSONData.markets[i].pageOddsWin;					
		market.callsTrend 				= JSONData.markets[i].callsTrend;
		market.putsTrend 				= JSONData.markets[i].putsTrend;
		allBinaryMarketArray[market.id] = market; 
	}
	if (allBinaryMarketArray.length  > 0) {
		offhours = false;
	}

} 
			
// remove closed assets 
function removeAsset() {
	var treeM = getElm('treeM');
	var treeOptions = treeM.getElementsByTagName("li");

	var state;
	var optionMarketId;
	var subgroup;
	var hasOpenMarkets = true;
	for(var i=0; i<treeOptions.length; i++){
		state=null;
		optionMarketId = treeOptions[i].getAttribute('data-marketId');
		if (optionMarketId === null) {
			var newSubgroup = treeOptions[i].getAttribute('data-subgroup');
			if (newSubgroup != null) {
				if (!hasOpenMarkets) {
					getElm(subgroup).style.display = "block";
				}
				hasOpenMarkets = false;
				subgroup = newSubgroup;
			}
			continue;
		}
		state = openMarkets[optionMarketId]; 		
		if((state == null) || (state == 0)){
			treeOptions[i].style.display = "none";
		} else {
			hasOpenMarkets = true;
			treeOptions[i].style.display = "block";
			treeOptions[i].className += " menu_item_active";
		}
	}
	if (!hasOpenMarkets) {
		getElm(subgroup).style.display = "block";
	}
}	
			
// update asset details
function updateAssetDetails(marketId) {
	try {
		//market expiration
		var marketExpiration = getElm('marketExpiration');
		if (marketExpiration != null) {
			var market = allBinaryMarketArray[marketId];		
			if (market != null && allBinaryMarketArray[marketId].timeEstClosingMillsec != null) {
				marketExpiration.innerHTML 	= formatDate(new Date(allBinaryMarketArray[marketId].timeEstClosingMillsec));
			}
		}
		// select asset list
		var treeM = getElm('treeM');
		if (treeM != null) {
			treeM.style.display = 'none';
		}
		// asset level
		var assetLevel = getElm('assetLevel');
		if (assetLevel != null) {
			assetLevel.innerHTML = allBinaryMarketArray[marketId].currentLevelStr;
		}
		// asset name
		var assetName = getElm('assetName')
		if (assetName != null) {
			assetName.innerHTML = allBinaryMarketArray[marketId].name  ;
		}		
		// market distribution
		var callPutDivider = getElm('callPutDivider')
		if (callPutDivider != null) {
			getElm('callTradingDiv').style.width 	= allBinaryMarketArray[marketId].callsTrend + '%';
			getElm('callTradingTxt').innerHTML 		= allBinaryMarketArray[marketId].callsTrend + '%';
			getElm('callPutDivider').style.left 	= allBinaryMarketArray[marketId].callsTrend + '%';
			getElm('putTradingDiv').style.width 	= allBinaryMarketArray[marketId].putsTrend + '%';				
			getElm('putTradingTxt').innerHTML 		= allBinaryMarketArray[marketId].putsTrend + '%';	
		}		
		// profit
		var profit = getElm('profit');
		if (profit != null) {
			getElm('profit').innerHTML = allBinaryMarketArray[marketId].pageOddsWin + '%';
		}					
		currentMarketId 							= marketId
	} catch (e) {
		offhours = true;
		displayBox();
	}
}

// change the test of call/put to up/down
function callPut_sw() {
	var haveCallPutButton = false;
	this.call = getElm('tradeBox_call_btn_text');
	if (this.call != null) {
		this.call_txt_1 = this.call.getAttribute('date-text_1');
		this.call_txt_2 = this.call.getAttribute('date-text_2');
		this.put = getElm('tradeBox_put_btn_text');
		this.put_txt_1 = this.put.getAttribute('date-text_1');
		this.put_txt_2 = this.put.getAttribute('date-text_2');
		this.flag = true;
		var self = this;
		haveCallPutButton = true;
	}
	if (haveCallPutButton) {
		function switchIt() {
			if(self.flag) {
				self.call.innerHTML = self.call_txt_1;
				self.put.innerHTML = self.put_txt_1;
				self.flag = false;
			}
			else {
				self.flag = true;
				self.call.innerHTML = self.call_txt_2;
				self.put.innerHTML = self.put_txt_2;
			}
		}
		setInterval(switchIt,2000);
	}
}

//display trade box / offhours banner
function displayBox() {	
	if (offhours) {
		getElm('offhours_box').style.display = 'block';
		getElm('trade_box').style.display = 'none';
	} else {
		getElm('trade_box').style.display = 'block';
		getElm('offhours_box').style.display = 'none';
	} 		
}

buildMarketsListData();
displayBox();
if (!offhours) {
	removeAsset();
	updateAssetDetails(defaultMarketId);
	callPut_sw();
} 