			var OPPORTUNITY_STATE_CREATED					= 1;
			var OPPORTUNITY_STATE_OPENED					= 2;
			var OPPORTUNITY_STATE_LAST_10_MIN				= 3;
			var OPPORTUNITY_STATE_CLOSING_1_MIN				= 4;
			var OPPORTUNITY_STATE_CLOSING					= 5;
			var OPPORTUNITY_STATE_CLOSED					= 6;
			var OPPORTUNITY_STATE_DONE						= 7;
			var OPPORTUNITY_STATE_PAUSED					= 8;
			var OPPORTUNITY_STATE_SUSPENDED					= 9;
			var OPPORTUNITY_STATE_WAITING_TO_PAUSE  		= 10;
			var OPPORTUNITY_STATE_HIDDEN_WAITING_TO_EXPIRY	= 11;

			var SUBSCR_FLD_KEY						= "key";//1;
			var SUBSCR_FLD_COMMAND					= "command";//2;
			var SUBSCR_FLD_ET_NAME					= "ET_NAME";//3; //market id. now we get the name according to market id from message resources
			var SUBSCR_FLD_ET_ID					= "ET_ID";//3; //market id. now we get the name according to market id from message resources
			var SUBSCR_FLD_ET_LEVEL					= level_skin_group_id;//4;
			var SUBSCR_FLD_ET_ODDS					= "ET_ODDS";//5;
			var SUBSCR_FLD_ET_EST_CLOSE				= "ET_EST_CLOSE";//6;
			var SUBSCR_FLD_ET_OPP_ID				= "ET_OPP_ID";//7;
			var SUBSCR_FLD_ET_ODDS_WIN				= "ET_ODDS_WIN";//8;
			var SUBSCR_FLD_ET_ODDS_LOSE				= "ET_ODDS_LOSE";//9;
			var SUBSCR_FLD_ET_STATE					= "ET_STATE";//10;
			var SUBSCR_FLD_ET_CLR					= level_color;//11;
			var SUBSCR_FLD_PRIORITY					= "ET_GROUP_PRIORITY";//12;
			var SUBSCR_FLD_ET_GROUP_CLOSE			= "ET_GROUP_CLOSE";//13;
			var SUBSCR_FLD_ET_SCHEDULED				= "ET_SCHEDULED";//14;
			var SUBSCR_FLD_ET_RND_FLOOR				= "ET_RND_FLOOR";//15;
			var SUBSCR_FLD_ET_RND_CEILING			= "ET_RND_CEILING";//16;
			var SUBSCR_FLD_ET_GROUP					= "ET_GROUP";//17;
			var SUBSCR_FLD_ET_SUSPENDED_MESSAGE		= "ET_SUSPENDED_MESSAGE";//18;
			var SUBSCR_FLD_ET_AO_STATES				= "AO_STATES";//19;
			var SUBSCR_FLD_ET_AO_TS					= "AO_TS";//20;
//			var SUBSCR_FLD_ET_EST_CLOSE_COPY		= "";//21;
//			var SUBSCR_FLD_OLD_STATE				= "";//22;
//			var SUBSCR_FLD_GROUP_CLOSE_CHANGED		= "";//23;
//			var SUBSCR_FLD_STATE_CHANGED			= "";//24;
//			var SUBSCR_FLD_COLOR_CHANGED			= "";//25;
//			var SUBSCR_FLD_CURRENT_LEVEL			= "";//26;
//			var SUBSCR_FLD_ET_DISPLAY_NAME			= "";//27; //Display name to show in the table;
//			var SUBSCR_FLD_CLOSE_TIME				= "";//28;
//			var SUBSCR_FLD_ET_EST_CLOSE_CHANGED		= "";//29; //if we have est. time changed
//			var SUBSCR_FLD_SCHEDULED_HOURLY			= "";//30; //for sorting the lists according to priority and hourly

			// TODO start
			var isCalcalistAndGameEnded;
			// TODO end
			var errLogInDisplayed;
			var marketsTimestamp = 0; //last time we have changed the markets states
			var marketStates = new Array(0); //last open markets if its size is zero mean we dont have open markets

			var userPickArray = [];
			var isHourlyPickedArray = [];
			var opportunityOnPage = [];

			// the class attribute name. there us diff btw IE (className) and FF (class)
			var classAtr = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'className' : 'class';
			// in IE 8.0 it is like in FireFox
			classAtr = navigator.appVersion.toLowerCase().indexOf("msie 8.0") != -1 ? 'class' : classAtr;

			var strHref = window.location.href;

			//how much rows of opps we have on the trade page table
			var rowsCount = 0;

			function getXMLHttp() {
				var xmlHttp;
				try {
					// Firefox, Opera 8.0+, Safari
					xmlHttp = new XMLHttpRequest();
				} catch (e) {
					// Internet Explorer
					try {
						xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
					} catch (e) {
						try {
							xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
						} catch (e) {
							alert("Your browser does not support AJAX!");
							return false;
						}
					}
				}
				return xmlHttp;
			}

			function addToServerSlip(id, choice) {
				xmlHttp = getXMLHttp();
				if (null == xmlHttp) {
					return;
				}
				xmlHttp.open("POST", "ajax.jsf", true);
				xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				xmlHttp.send("command=add&slipEntryId=" + id + "&choice=" + choice);
			}

			function updateServerSlip(id, amount, choice) {
				xmlHttp = getXMLHttp();
				if (null == xmlHttp) {
					return;
				}
				xmlHttp.open("POST", "ajax.jsf", true);
				xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				xmlHttp.send("command=update&slipEntryId=" + id + "&slipEntryAmount=" + amount + "&choice=" + choice);
			}

			var entSubmitted = 1;

			function submitFromServerSlip(id, lvl, oddWin, oddLose, choice, all, amount) {
				var subSS = new SubSS(id, lvl, oddWin, oddLose, choice, all, amount);
				function SubSS(id, lvl, oddWin, oddLose, choice, all, amount) {
					this.id = id;
					this.lvl = lvl;
					this.oddWin = oddWin;
					this.oddLose = oddLose;
					this.choice = choice;
					this.all = all;
					this.amount = amount;
					SubSS.ticker = function() {
						var xmlHttp = getXMLHttp();
						if (null == xmlHttp) {
							return false;
						}
						xmlHttp.onreadystatechange = function() {
							if (xmlHttp.readyState == 4) {
								canSubmit = true;
								if (xmlHttp.responseText.length > 2) {
									if (xmlHttp.responseText.substring(0, 2) == "OK") {
										var smsEnable = 0;
										var investmentId = xmlHttp.responseText.substring(2);
										if (xmlHttp.responseText.indexOf("SMS=")> -1) {
											investmentId = xmlHttp.responseText.substring(0,xmlHttp.responseText.length-5);
											investmentId = investmentId.substring(2);
											// check sms field
											if (xmlHttp.responseText.substring(xmlHttp.responseText.length-1) == "1") {
												smsEnable = 1;
											}
										}
										successFromSlip(subSS.id, subSS.choice, investmentId, smsEnable);
									} else if (xmlHttp.responseText.substring(0, 1) == "1") {
										if (!errLogInDisplayed) {
											errLogInDisplayed = true;
											//alert(xmlHttp.responseText.substring(1));
											errorInSlipNotLogin(subSS.id, subSS.choice);
											unfreezeInSlip(subSS.id, subSS.choice);
											// refresh the header to see he is not logged in
											document.getElementById('header').contentWindow.location.reload();
										}
									} else {
										errorInSlip(subSS.id, subSS.choice, xmlHttp.responseText);
										unfreezeInSlip(subSS.id, subSS.choice);
									}
								} else {
									errorInSlip(subSS.id, subSS.choice, xmlHttp.responseText);
									unfreezeInSlip(subSS.id, subSS.choice);
								}
							}
						}
						xmlHttp.open("POST", "ajax.jsf", true);
						xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
						xmlHttp.send("command=submit&slipEntryId=" + subSS.id +
									 "&slipEntryLevel=" + subSS.lvl +
									 "&slipEntryOddWin=" + subSS.oddWin +
									 "&slipEntryOddLose=" + subSS.oddLose +
									 "&choice=" + subSS.choice +
									 "&all=" + subSS.all +
									 "&amount=" + subSS.amount +
									 "&utcOffset=" + sessionUtcOffset /*new Date().getTimezoneOffset()*/);
					}
				}
				setTimeout(SubSS.ticker, entSubmitted * 1000);
				entSubmitted++;
			}

			function removeFromServerSlip(id, choice) {
				xmlHttp = getXMLHttp();
				if (null == xmlHttp) {
					return;
				}
				xmlHttp.open("POST", "ajax.jsf", true);
				xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				xmlHttp.send("command=remove&slipEntryId=" + id + "&choice=" + choice);
			}

			function removeAllFromServerSlip() {
				xmlHttp = getXMLHttp();
				if (null == xmlHttp) {
					return;
				}
				xmlHttp.open("POST", "ajax.jsf", true);
				xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				xmlHttp.send("command=removeall&slipEntryId=0");
			}

			var successWorking = false;
			function successFromSlip(id, choice, invId, smsEnable) {
				if (successWorking) {
					setTimeout("successFromSlip('" + id + ", " + choice + ", " + invId + "');", 100);
					return;
				}
				successWorking = true;
				var crrEntry;
				var crrTbls;
				var entId;
				var entChoice;
				var newEntry;
				var newTbls;
				var tbodySms;
				var trSms;
				var tmpl;
				var samount;
				var slip = document.getElementById("slip");
				for (var i = 1; i <= slipCount; i++) {

					crrEntry = getChildOfType(slip, "DIV", i);
					crrTbls = crrEntry.getElementsByTagName("TABLE");
					if (getEntryLabel(crrTbls[1], 1) == 0) { // not success
						entId = getEntryValue(crrTbls[1], 1);
						entChoice = getEntryValue(crrTbls[2], 13);

						if (id == entId && choice == entChoice) {
							// copy the template
							// do it this way so we go around the "Unknown runtime error in IE when editing the innerHTML of a table"

							newEntry = document.createElement("span");
							newEntry.innerHTML = document.getElementById("template_success").innerHTML;
							newEntry = newEntry.getElementsByTagName("DIV")[0];

							newTbls = newEntry.getElementsByTagName("TABLE");
							setEntryValue(newTbls[1], 1, id);
							setSuccessInvId(newTbls[2], invId);
							setEntryValue(newTbls[3], 1, getEntryValue(crrTbls[2], 2));
							setEntryValue(newTbls[4], 1, getEntryValue(crrTbls[2], 3));
							copyLevelColor(crrTbls[2], newTbls[4]);
							setEntryValue(newTbls[5], 1, getEntryValue(crrTbls[2], 4));
							setEntryValue(newTbls[6], 1, getEntryValue(crrTbls[2], 5));
							samount = getEntryAmount(crrTbls[2]);
							samount = decRound(samount, 2);
							setEntryValue(newTbls[7], 1, samount + getILS());
							setEntryValue(newTbls[8], 1, getEntryValue(crrTbls[2], 8));
							setEntryValue(newTbls[9], 1, getEntryValue(crrTbls[2], 9));

							// display sms check box
							if (smsEnable) {
								tbodySms = getChildOfType(newTbls[2], "TBODY", 1);
								trSms = getChildOfType(tbodySms, "TR", 12);
								trSms.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';
							}

							if (getEntryValue(crrTbls[2], 13) == 2) { // if put swap labels
								tmpl = getEntryData(newTbls[8], 1, 1);
								setEntryData(newTbls[8], 1, getEntryData(newTbls[9], 1, 1), 1);
								setEntryData(newTbls[9], 1, tmpl, 1);
							}

							slip.insertBefore(newEntry, crrEntry);
							slip.removeChild(crrEntry);
							break;
						}
					}
				}
				successWorking = false;

				// refresh balance
//				document.getElementById('header').src = document.getElementById('header').src;
				document.getElementById('header').contentWindow.location.reload();
				// refresh left side my investemnts
				document.getElementById('left_side_investments').contentWindow.location.reload();

			}

			function setSuccessInvId(tbl, invId) {
				var tbody = getChildOfType(tbl, "TBODY", 1);
				var tr = getChildOfType(tbody, "TR", 2);
				var td = getChildOfType(tr, "TD", 2);
				var span = getChildOfType(td, "SPAN", 2);
				span.innerHTML = invId;
			}

			function copyLevelColor(crrTbl, newTbl) {
				var newTbody = getChildOfType(newTbl, "TBODY", 1);
				var newTr = getChildOfType(newTbody, "TR", 1);
				var newTd = getChildOfType(newTr, "TD", 3);
				var newAtts = newTd.attributes;
				var newCls = newAtts.getNamedItem("class");

				var crrTbody = getChildOfType(crrTbl, "TBODY", 1);
				var crrTr = getChildOfType(crrTbody, "TR", 3);
				var crrTd = getChildOfType(crrTr, "TD", 3);
				var crrAtts = crrTd.attributes;
				var crrCls = crrAtts.getNamedItem("class");

				newCls.value = crrCls.value;
			}

			function getEntryAmount(entry) {
				var tbody = getChildOfType(entry, "TBODY", 1);
				var tr = getChildOfType(tbody, "TR", 6);
				var td = getChildOfType(tr, "TD", 3);
				var inp = getChildOfType(td, "INPUT", 1);
				return inp.value;
			}

			function errorInSlip(id, choice, msg) {
				var slip = document.getElementById("slip");
				var crrEntry;
				var crrTbls;
				var entId;
				var entChoice;
			//	alert(slip.innerHTML);
				for (var i = 1; i <= slipCount; i++) {
					crrEntry = getChildOfType(slip, "DIV", i);
					crrTbls = crrEntry.getElementsByTagName("TABLE");
					if (getEntryLabel(crrTbls[1], 1) == 0) {
						entId = getEntryValue(crrTbls[1], 1);
						entChoice = getEntryValue(crrTbls[2], 13);
						if (id == entId && choice == entChoice) {
							setEntryLabel(crrTbls[2], 1, msg);
							if (!isRowOn(crrTbls[2], 1)) {
								toggleRow(crrTbls[2], 1);
							}
							return;
						}
					}
				}
			}

			function errorInSlipNotLogin(id, choice) {
				var slip = document.getElementById("slip");
				var crrEntry;
				var crrTbls;
				var entId;
				var entChoice;
			//	alert(slip.innerHTML);
				for (var i = 1; i <= slipCount; i++) {
					crrEntry = getChildOfType(slip, "DIV", i);
					crrTbls = crrEntry.getElementsByTagName("TABLE");
					if (getEntryLabel(crrTbls[1], 1) == 0) {
						entId = getEntryValue(crrTbls[1], 1);
						entChoice = getEntryValue(crrTbls[2], 13);
						if (id == entId && choice == entChoice) {
							if (!isRowOn(crrTbls[2], 14)) {
								toggleRow(crrTbls[2], 14);
							}
							if (isRowOn(crrTbls[2], 10)) {
								toggleRow(crrTbls[2], 10);
							}
							return;
						}
					}
				}
			}
//fixed for new design
			function unfreezeInSlip(id, choice) {
				var slip = document.getElementById("slip");
				var crrEntry;
				var crrTbls;
				var entId;
				var entChoice;
				var mTbl;
				var tbls;
				var tbody;
				var i;
				var row;
				var oppId;
				var rowTd;
				var rowTable;
				var rowTbody;
				var rowTr;
				var oppTr;
				var oppTrTable;
				var oppTrTableBody;
				var oppTrTableBodyTr;
				var oppTrTableBodyTrTd;
				var oppTrTableBodyTrTdChild;
				var tmp;

				for (var j = 1; j <= slipCount; j++) {
					crrEntry = getChildOfType(slip, "DIV", j);
					crrTbls = crrEntry.getElementsByTagName("TABLE");
					if (getEntryLabel(crrTbls[1], 1) == 0) {
						entId = getEntryValue(crrTbls[1], 1);
						entChoice = getEntryValue(crrTbls[2], 13);
						if (id == entId && choice == entChoice) {
							setEntryData(crrTbls[1], 1, 0, 1); // not submitting

							mTbl = document.getElementById("markets_table");
							tbls = mTbl.getElementsByTagName("TABLE");
							tbody = getChildOfType(tbls[1], "TBODY", 1);
							i = 5;
							do {
								row = getChildOfType(tbody, "TR", i);
								if (null != row) {
									oppId = getOppValue(row, 6);
									if (oppId == id) {
										setEntryValue(crrTbls[2], 3, getOppLevel(row));
										rowTd = getChildOfType(row, "TD", 1);
										rowTable = getChildOfType(rowTd, "TABLE", 1);
										rowTbody = getChildOfType(rowTable, "TBODY", 1);
										rowTr = getChildOfType(rowTbody, "TR", 1);
										oppTr = getChildOfType(rowTr, "TD", 2);
										oppTrTable = getChildOfType(oppTr, "TABLE", 1);
										oppTrTableBody = getChildOfType(oppTrTable, "TBODY", 1);
										oppTrTableBodyTr = getChildOfType(oppTrTableBody, "TR", 1);
										oppTrTableBodyTrTd = getChildOfType(oppTrTableBodyTr, "TD", 1);
										oppTrTableBodyTrTdChild = getChildOfType(oppTrTableBodyTrTd, "IMG", 1);
										if (oppTrTableBodyTrTdChild == null) {
											oppTrTableBodyTrTdChild = getChildOfType(oppTrTableBodyTrTd, "DIV", 1);
										}
										copyLevelColorToSlip(getChildOfType(oppTrTableBodyTrTd, "IMG", 1), crrTbls[2]);
										break;
									}
								} else {
									break;
								}
								i = i + 1;
							} while (true);

							tmp = getChildOfType(crrTbls[2], "TBODY", 1);
							tmp = getChildOfType(tmp, "TR", 10);
							tmp = getChildOfType(tmp, "TD", 2);
							getChildOfType(tmp, "A", 1).style.display = 'block';
							getChildOfType(tmp, "IMG", 1).style.display = 'none';

							return;
						}
					}
				}
			}

			function toggleRow(entry, rowIndex) {
				var tbody = getChildOfType(entry, "TBODY", 1);
				var tr = getChildOfType(tbody, "TR", rowIndex);
				if (tr.style.display == 'none') {
					tr.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';
				} else {
					tr.style.display = 'none';
				}
			}

			function isRowOn(entry, rowIndex) {
				var tbody = getChildOfType(entry, "TBODY", 1);
				var tr = getChildOfType(tbody, "TR", rowIndex);
				if (tr.style.display == 'none') {
					return false;
				} else {
					return true;
				}
			}

			function removeInput(entry) {
				var tbody = getChildOfType(entry, "TBODY", 1);
				var tr = getChildOfType(tbody, "TR", 8);
				var td = getChildOfType(tr, "TD", 3);
				var inp = getChildOfType(td, "INPUT", 1);
				var val = inp.value;
				td.innerHTML = val;
			}

			function startCountDown(display, min, sec) {
				var countDown = new CountDown(display, min, sec);
				function CountDown(display, min, sec) {
					this.display = display;
					this.min = min;
					this.sec = sec;
//					log("Start count down - display: " + " row oppId: " + getOppValue(this.display.parentNode.parentNode, 6) + " min: " + min + " sec: " + sec);
					CountDown.ticker = function() {
						if (countDown.sec > 0) {
							countDown.sec = countDown.sec - 1;
						} else {
							if (countDown.min > 0) {
								countDown.min = countDown.min - 1;
								countDown.sec = 59;
							}
						}
						countDown.display.innerHTML = padToTwoDig(countDown.min) + ":" + padToTwoDig(countDown.sec);
						if (countDown.min > 0 || countDown.sec > 0) {
							setTimeout(CountDown.ticker, 1000);
						} else {
							if (!updatesFrequencyFlag) {
								try {
								/*	if (HOME_PAGE) {
										setOppValue(countDown.display.parentNode.parentNode, 14, OPPORTUNITY_STATE_CLOSING_1_MIN);
										formatValuesClosing1Min(countDown.display.parentNode.parentNode);
									} else {*/
								//		setOppValue(countDown.display.parentNode.parentNode, 14, OPPORTUNITY_STATE_CLOSING);
		//								alert('test' + countDown.display.parentNode.parentNode.innerHTML);
		//								logg("finish");
										formatValuesClosing(countDown.display.parentNode.parentNode);
								//	}
								} catch (e) {
									// do nothing. most likely the state changed by ls
//									alert("problem timer over " + e);
								}
							}
						}
					}
				}
				setTimeout(CountDown.ticker, 1000);
			}

			function padToTwoDig(val) {
				if (val < 10) {
					return "0" + val;
				}
				return val;
			}

			var timesToGet = 1;

			function getTimeLeft(oppId, span) {
				var timeGetter = new TimeGetter(oppId, span);
				function TimeGetter(oppId, span) {
					this.oppId = oppId;
					this.span = span;
					this.xmlHttp = null;

					//log("Get time left - oppId: " + oppId + " row oppId: " + getOppValue(this.span.parentNode.parentNode, 6) + " timesToGet: " + timesToGet);
					this.ticker = function() {
						//log("Tick. oppId: " + timeGetter.oppId);
						timeGetter.xmlHttp = getXMLHttp();
						if (null == timeGetter.xmlHttp) {
							return false;
						}
						timeGetter.xmlHttp.onreadystatechange = function() {
							if (timeGetter.xmlHttp.readyState == 4) {
								var rez = timeGetter.xmlHttp.responseText;
								var id = rez.substring(0, rez.indexOf("_"));
								rez = rez.substring(rez.indexOf("_") + 1);
								var min = rez.substring(0, rez.indexOf("_"));
								var sec = rez.substring(rez.indexOf("_") + 1);
								var s = span;

								//log("Time received - rez: " + rez + " id: " + id + " min: " + min + " sec: " + sec + " row oppId: " + getOppValue(s.parentNode.parentNode, 6));
								if (s) {
									startCountDown(s, min, sec);
								}
								timesToGet--;
							}
						}
						//log("Request the time for oppId: " + timeGetter.oppId);
						timeGetter.xmlHttp.open("POST", "ajax.jsf", true);
						timeGetter.xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
						timeGetter.xmlHttp.send("command=time&slipEntryId=" + timeGetter.oppId);
					}
				}
				setTimeout(timeGetter.ticker, 1000 * timesToGet);
				timesToGet++;
//				if (timesToGet >= 12) {
//					timesToGet = 1;
//				}
			}

			//fixed for new design
			function get10MinTimeSpan(oppId) {
				var oppsTable = document.getElementById("oppsTable");
				var tbody = getChildOfType(oppsTable, "TBODY", 1);
				var id;
				var rowTd;
				var rowTable;
				var rowTbody;
				var rowTr;
				var td;
				var i = 5;
				var tr;
				while (tr = getChildOfType(tbody, "TR", i)) {
					id = getOppValue(tr, 6);
					if (oppId == id) {
						rowTd = getChildOfType(tr, "TD", 1);
						rowTable = getChildOfType(rowTd, "TABLE", 1);
						rowTbody = getChildOfType(rowTable, "TBODY", 1);
						rowTr = getChildOfType(rowTbody, "TR", 1);
						td = getChildOfType(rowTr, "TD", 5);
						return getChildOfType(td, "SPAN", 1);
					}
					i++;
				}
//				alert("Can't find the time span for " + oppId);
			}

			function printFromServerSlip(id) {
				xmlHttp = getXMLHttp();
				if (null == xmlHttp) {
					return;
				}
				xmlHttp.open("POST", "ajax.jsf", true);
				xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				xmlHttp.send("command=print&slipEntryId=" + id);
			}

			function getChildOfType(node, childType, order) {
				try{
					var pos = 0;
					for (var i = 0; i < node.childNodes.length; i++) {
						if (node.childNodes[i].nodeName == childType) {
							pos++;
							if (pos == order) {
								return node.childNodes[i];
							}
						}
					}
				}catch(e){
					//EROORRRRRR!!!
				}
			}
//fixed for new design
			function getOppValue(opp, field) {
				var td;
				if (opp.id.indexOf("opp") > -1) {
					var rowTd = getChildOfType(opp, "TD", 1);
					var rowTable = getChildOfType(rowTd, "TABLE", 1);
					var rowTbody = getChildOfType(rowTable, "TBODY", 1);
					var rowTr = getChildOfType(rowTbody, "TR", 1);
					td = getChildOfType(rowTr, "TD", field);
				} else {
					td = getChildOfType(opp, "TD", field);
				}
				var div = getChildOfType(td, "DIV", 1);

				return div.innerHTML;
			}
//fixed for new design
			function getOppLevel(opp) {
				var td;
				if (opp.id.indexOf("opp") > -1) {
					var rowTd = getChildOfType(opp, "TD", 1);
					var rowTable = getChildOfType(rowTd, "TABLE", 1);
					var rowTbody = getChildOfType(rowTable, "TBODY", 1);
					var rowTr = getChildOfType(rowTbody, "TR", 1);
					td = getChildOfType(rowTr, "TD", 2);
				} else {
					td = getChildOfType(opp, "TD", 2);
				}
				var table = getChildOfType(td, "TABLE", 1);
				var tbody = getChildOfType(table, "TBODY", 1);
				var tr = getChildOfType(tbody, "TR", 2);
				if (tr == null) {
					tr = getChildOfType(tbody, "TR", 1);
				}
				return getOppValue(tr, 1);
			}

			function getEntryData(entry, field, col) {
				var tbody = getChildOfType(entry, "TBODY", 1);
				var tr = getChildOfType(tbody, "TR", field);
				var td = getChildOfType(tr, "TD", col);
				return td.innerHTML;
			}

			function setEntryData(entry, field, val, col, clr) {
				var tbody = getChildOfType(entry, "TBODY", 1);
				var tr = getChildOfType(tbody, "TR", field);
				var td = getChildOfType(tr, "TD", col);
				if (clr != null) {
					var atts = td.attributes;
					var cls = atts.getNamedItem("class");
					if (clr == 0) {
						cls.value = "invest_level_down_left";
					} else if (clr == 1) {
						cls.value = "invest_level_close_left";
					} else if (clr == 2) {
						cls.value = "invest_level_up_left";
					}
				}
				td.innerHTML = val;
			}

			function getEntryValue(entry, field) {
				return getEntryData(entry, field, 3);
			}

			function setEntryValue(entry, field, val, clr) {
				setEntryData(entry, field, val, 3, clr);
			}

			function getEntryLabel(entry, field) {
				return getEntryData(entry, field, 2);
			}

			function setEntryLabel(entry, field, val) {
				setEntryData(entry, field, val, 2);
			}

			function getILS() {
				return document.getElementById("template_ils").innerHTML;
			}

			function addToSlip(link, type) {
				if (canSubmit) {
					var opp = link.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode;
					var oppId = getOppValue(opp, 6);

					var slip = document.getElementById("slip");
					if (slipCount >= 1) {
						var oldEntry = getChildOfType(slip, "DIV", 1);
						var oldEntryTbls = oldEntry.getElementsByTagName("TABLE");
						var slipOppId = getEntryValue(oldEntryTbls[1], 1);
						//if this slip had graph open its have put or call marked to remove them
						var chartIframe = document.getElementById("chart" + slipOppId);
						if (chartIframe != null) {
							if (oppId != slipOppId) {
								chartIframe.contentWindow.hidePut();
								chartIframe.contentWindow.hideCall();
							}
						}

						slip.removeChild(oldEntry);
						slipCount = slipCount - 1;
					}



					if (slipCount > 0) {
						var crrEntry;
						var crrTbls;
						var entId;
						var successful;
						for (var i = 1; i <= slipCount; i++) {
							crrEntry = getChildOfType(slip, "DIV", i);
							crrTbls = crrEntry.getElementsByTagName("TABLE");
							entId = getEntryValue(crrTbls[0], 1);
							if (oppId == entId) {
								successful = getEntryLabel(crrTbls[0], 1);
								if (successful == 0) {
									if (getEntryValue(crrTbls[1], 13) == type) {
										alert(bundle_msg_intheslip);
										return;
									}
								} else {
									slip.removeChild(crrEntry);
									slipCount = slipCount - 1;
									if (slipCount > 0) {
										updateSlipCaptions();
										updateSlipTotals();
									} else {
										slip.innerHTML = "";
									}

	//								removeFromServerSlip(oppId, -1);
									break;
								}
							}
						}
					} else {
						if (updatesFrequencyFlag == 1) {
							var topLeftBanner = parent.document.getElementById("top_left");
							if (null != topLeftBanner) {
							//	alert(topLeftBanner.parentNode.innerHTML);
								// topLeftBanner.style.display = 'none';
								topLeftBanner.parentNode.parentNode.style.display = 'none';
							}
						} else {
							var LeftBanners = parent.document.getElementById("logedOut_banners");
							if (null != LeftBanners) {
							//	alert(topLeftBanner.parentNode.innerHTML);
								// topLeftBanner.style.display = 'none';
								LeftBanners.style.display = 'none';
							}
						}
					}

					var oppName = getOppValue(opp, 1);
					var oppLevel = getOppLevel(opp);
					var oppOdds = getOppValue(opp, 3);
					var oppClose;
					//now in group pages we have select box so we need diff logic the home page
					if (strHref.indexOf("index.jsf") > -1) { //for home
						oppClose = getOppValue(opp, 4);
					} else { // for group pages
						var selectArray = opp.getElementsByTagName("SELECT");
						var elSel = selectArray[0];
						oppClose = elSel.options[elSel.selectedIndex].text;
					}
					var oppWin = getOppValue(opp, 7);
					var oppLose = getOppValue(opp, 8);

					// copy the template
					// do it this way so we go around the "Unknown runtime error in IE when editing the innerHTML of a table"
					var newEntry = document.createElement("span");
					newEntry.innerHTML = document.getElementById("template").innerHTML;
					newEntry = newEntry.getElementsByTagName("DIV")[0];

					var tbls = newEntry.getElementsByTagName("TABLE");


					// fill the template
					setEntryValue(tbls[1], 1, oppId);
				//	setEntryLabel(tbls[1], 2, getEntryLabel(tbls[1], 2) + ' ' + (slipCount + 1));
					setEntryValue(tbls[2], 2, oppName);
					setEntryValue(tbls[2], 3, oppLevel);
					copyLevelColorToSlip(link, tbls[2]);
					setEntryValue(tbls[2], 4, document.getElementById(type == 1 ? "template_slip_call" : "template_slip_put").innerHTML);
					setEntryValue(tbls[2], 5, oppClose);
					if (type == 2) {
						swapUpDownCaptions(tbls[2]);
					}

					setEntryValue(tbls[2], 11, oppWin);
					setEntryValue(tbls[2], 12, oppLose);
					setEntryValue(tbls[2], 13, type);
					var slipTable = tbls[2];

					var div1 = getChildOfType(slip, "DIV", 1);
					slip.insertBefore(newEntry, div1);

					slipCount++;

	//				scroll(0, 0);

					//take out low resulation position the slip in place
					var fTop = $(".invest_header").offset({ scroll: false }).top;
					var fTopStop = $("#stopFloating").offset({ scroll: false }).top - 457;
					if (window.screen.width > 904 && $(window).scrollTop() > fTop && $(window).scrollTop() <= fTopStop) {
						var fLeft = $(".floatingLeft").offset({ scroll: false }).left;
						if (navigator.userAgent.toLowerCase().indexOf("msie") == -1) {
							fLeft += 1;
						}
						//var fTop = $(".invest_header").offset({ scroll: false }).top;
						$("#slip").css("position", "absolute");
						$("#slip").css("top", $(window).scrollTop());
						$("#slip").css("left", fLeft);
					} else if ($(window).scrollTop() > fTopStop) {
						$("#slip").css("position", "absolute");
		//				var nFtopPos = fTopStop - $(window).scrollTop();
						$("#slip").css("top", fTopStop);
						$("#slip").css("left", fLeft);
					}

					//set focus on the input field. do it twice to make sure IE will set focus
					var rtbody = getChildOfType(slipTable, "TBODY", 1)
					var ttr = getChildOfType(rtbody, "TR", 6);
					var inputField = ttr.getElementsByTagName("INPUT");
					inputField[0].focus();
					inputField[0].focus();
				}
//				addToServerSlip(oppId, type);
			}

			function copyLevelColorToSlip(link, newTbl) {
				var newTbody = getChildOfType(newTbl, "TBODY", 1);
				var newTr = getChildOfType(newTbody, "TR", 3);
				var newTd = getChildOfType(newTr, "TD", 3);
				var newAtts = newTd.attributes;
				var newCls = newAtts.getNamedItem("class");

				var crrTbody = link.parentNode.parentNode.parentNode;
				var crrTr = getChildOfType(crrTbody, "TR", 2);
				var crrTd = getChildOfType(crrTr, "TD", 1);
				var crrAtts = crrTd.attributes;
				var crrCls = crrAtts.getNamedItem("class");

				newCls.value = crrCls.value;
				if (crrCls.value == 'invest_level_down') {
					newCls.value = 'invest_level_down_left';
				} else if (crrCls.value == 'invest_level_close') {
					newCls.value = 'invest_level_close_left';
				} else {
					newCls.value = 'invest_level_up_left';
				}
			}

			function swapUpDownCaptions(item) {
				var tmpLbl = getEntryLabel(item, 8);
				setEntryLabel(item, 8, getEntryLabel(item, 9));
				setEntryLabel(item, 9, tmpLbl);
			}

			function getSlipEntryCaption(template) {
				var templ = document.getElementById(template);
				var tbls = templ.getElementsByTagName("TABLE");
				return getEntryLabel(tbls[0], 2);
			}

			function updateSlipCaptions() {
				var slip = document.getElementById("slip");
				for (var i = 1; i <= slipCount; i++) {
					var crrEntry = getChildOfType(slip, "DIV", i);
					var crrTbls = crrEntry.getElementsByTagName("TABLE");
					if (getEntryLabel(crrTbls[0], 1) == 0) {
						setEntryLabel(crrTbls[0], 2, getSlipEntryCaption("template") + " " + (slipCount - i + 1));
					} else {
						setEntryLabel(crrTbls[0], 2, getSlipEntryCaption("template_success") + " " + (slipCount - i + 1));
					}
				}
			}

			function removeFromSlip(link) {
				var span = document.getElementById("slip");
				var entry = link;
				for (var i = 0; i < 5; i++) {
					entry = entry.parentNode;
				}

				var tbls = entry.getElementsByTagName("TABLE");
				var oppId = getEntryValue(tbls[0], 1);
				var successful = getEntryLabel(tbls[0], 1);
				var choice = -1;
				if (successful == 0) {
					choice = getEntryValue(tbls[1], 13);
				}
				for (var i = 0; i < 4; i++) { //to remove all the slip we must be on the div
					entry = entry.parentNode;
				}
				span.removeChild(entry);
				slipCount = slipCount - 1;

				// put here the show banner ;)
				if (updatesFrequencyFlag == 1) {
					var topLeftBanner = parent.document.getElementById("top_left");
					if (null != topLeftBanner) {
						topLeftBanner.parentNode.parentNode.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';
					}
				} else {
					span.innerHTML = document.getElementById("emptyslip").innerHTML;
				}

				//if (slipCount == 0) {
				//	span.innerHTML = document.getElementById("emptyslip").innerHTML;
				//} else {
				//	updateSlipCaptions();
				//	updateSlipTotals();
				//}

				//removeFromServerSlip(oppId, choice);

				//if this slip had graph open its have put or call marked to remove them
				var chartIframe = document.getElementById("chart" + oppId);
				if (chartIframe != null) {
					chartIframe.contentWindow.hidePut();
					chartIframe.contentWindow.hideCall();
				}

				//take out low resulation
				if (window.screen.width > 904 && strHref.indexOf("oneTouch.jsf") == -1 && strHref.indexOf("facebook-options.jsf") == -1) {
					var fLeft = $(".floatingLeft").offset({ scroll: false }).left;
					if (navigator.userAgent.toLowerCase().indexOf("msie") == -1) {
						fLeft += 1;
					}
					var fTop = $(".invest_header").offset({ scroll: false }).top;
					$("#slip").css("position", "absolute");
					$("#slip").css("top", fTop - 2);
					$("#slip").css("left", fLeft);
				}

			}

			function removeAllFromSlip() {
				var slip = document.getElementById("slip");
				for (var i = 1; i <= slipCount; i++) {
					var crrEntry = getChildOfType(slip, "DIV", 1);
					slip.removeChild(crrEntry);
				}
				slipCount = 0;
				slip.innerHTML = document.getElementById("emptyslip").innerHTML;

//				removeAllFromServerSlip();
			}

			var canSubmit = true;
			function submitFromSlip(button) {
				var entry = button;
				for (var i = 0; i < 9; i++) { //was 9 with the divs
					entry = entry.parentNode;
				}
				var tbls = entry.getElementsByTagName("TABLE");
				var oppId = getEntryValue(tbls[0], 1);
				var choice = getEntryValue(tbls[1], 13);

				var mTblT = document.getElementById("markets_table");
				var tblsT = mTblT.getElementsByTagName("TABLE");
				var tbodyT = getChildOfType(tblsT[1], "TBODY", 1);
				var rowT;
				var oppIdT;
				var status;
				var i = 5;
				do {
					rowT = getChildOfType(tbodyT, "TR", i);
					if (null != rowT) {
						oppIdT = getOppValue(rowT, 6);
						if (oppIdT == oppId) {
							status = getOppValue(rowT, 14);
							if ( status > OPPORTUNITY_STATE_LAST_10_MIN ) {
								errorInSlip(oppId, choice, bundle_msg_slipExpired);
								return;
							}
							break;
						}

					} else { //not in the page any more (close);
						errorInSlip(oppId, choice, bundle_msg_slipExpired);
						return;
					}
					i = i + 1;
				} while (true);

				if (canSubmit) {
					canSubmit = false;

					errLogInDisplayed = false;

					setEntryData(tbls[0], 1, 1, 1); // mark as submitting. no more change to slip level

					var oppLvl = getEntryValue(tbls[1], 3);
					var oppOddsWin = getEntryValue(tbls[1], 11);
					var oppOddsLose = getEntryValue(tbls[1], 12);

					var amount = getEntryAmount(tbls[1]);

					setEntryLabel(tbls[1], 1, ""); // clear error
					if (isRowOn(tbls[1], 1)) {
						toggleRow(tbls[1], 1);
					}

					button.style.display = 'none';
					var img = getChildOfType(button.parentNode, "IMG", 1);
					img.style.display = 'block';

					entSubmitted = 1;
					submitFromServerSlip(oppId, oppLvl, oppOddsWin, oppOddsLose, choice, 0, amount);
					entSubmitted = 1;

				}
			}

			function submitAllFromSlip() {
				if (canSubmit) {
					canSubmit = false;

					errLogInDisplayed = false;
					entSubmitted = 1;
					var crrEntry = getChildOfType(slip, "DIV", i);
					var tbls;
					var oppId;
					var oppLvl;
					var oppOddsWin ;
					var oppOddsLose;
					var choice;
					var amount;
					var slip = document.getElementById("slip");
					for (var i = 1; i <= slipCount; i++) {
						crrEntry = getChildOfType(slip, "DIV", i);
						tbls = crrEntry.getElementsByTagName("TABLE");
						if (getEntryLabel(tbls[0], 1) == 0) {
							oppId = getEntryValue(tbls[0], 1);
							oppLvl = getEntryValue(tbls[1], 3);
							oppOddsWin = getEntryValue(tbls[1], 11);
							oppOddsLose = getEntryValue(tbls[1], 12);
							choice = getEntryValue(tbls[1], 13);
							amount = getEntryAmount(tbls[1]);

							setEntryLabel(tbls[1], 1, ""); // clear error
							if (isRowOn(tbls[1], 1)) {
								toggleRow(tbls[1], 1);
							}

							setEntryData(tbls[0], 1, 1, 1); // mark as submitting

							submitFromServerSlip(oppId, oppLvl, oppOddsWin, oppOddsLose, choice, 1, amount);
						}
					}
					entSubmitted = 1;
				}
			}

			function updateSlipTotals() {
				var slip = document.getElementById("slip");
				var totalAmount = new Number(0);
				var totalWin = new Number(0);
				var crrEntry;
				var crrTbls;
				var success;
				var entAmount;
				var entWin;
				var tbody;
				var tr;
				var td;
				var inp;

				for (var i = 1; i <= slipCount; i++) {
					crrEntry = getChildOfType(slip, "DIV", i);
					crrTbls = crrEntry.getElementsByTagName("TABLE");

					success = getEntryLabel(crrTbls[0], 1);
					entAmount = 0;
					entWin = 0;
					if (success == 0) {
						tbody = getChildOfType(crrTbls[1], "TBODY", 1);
						tr = getChildOfType(tbody, "TR", 6);
						td = getChildOfType(tr, "TD", 3);
						inp = getChildOfType(td, "INPUT", 1);
						entAmount = inp.value;

						entWin = new Number(entAmount) * new Number(getEntryValue(crrTbls[1], 11));
//					} else {
//						entAmount = getEntryValue(crrTbls[6], 1);
//
//						entWin = getEntryValue(crrTbls[7], 1);
					}

					totalAmount = totalAmount + new Number(entAmount);
					totalWin = totalWin + new Number(entWin);
				}
				var slipTotals = getChildOfType(slip, "DIV", slipCount + 1);
				var slipTotalsTbls = slipTotals.getElementsByTagName("TABLE");
				setEntryValue(slipTotalsTbls[0], 1, decRound(totalAmount, 2) + getILS());
				setEntryValue(slipTotalsTbls[0], 2, decRound(totalWin, 2) + getILS());
			}

			function printSuccess(invId) {
				window.open(context_path + "/jsp/single_pages/invSuccessPrint.jsf?investmentId=" + invId, "invSuccessPrint", "width=498,height=427,menubar=1,toolbar=0,scrollbars=0");
			}

			function setClass(elm, newClass) {
				var atts = elm.attributes;
				var cls = atts.getNamedItem("class");
				cls.value = newClass;
			}

			function changeImages(td, img1, img2, lvlClass) {
				var table = getChildOfType(td, "TABLE", 1);
				var tbody = getChildOfType(table, "TBODY", 1);

				var trs = tbody.getElementsByTagName("TR");
				if (trs.length == 1) {
					var newTr = document.createElement("tr");
					var newTd = document.createElement("td");
					var al = document.createAttribute("align");
					al.value = "center";
					newTd.setAttributeNode(al);
					newTr.appendChild(newTd);
					tbody.insertBefore(newTr, trs[0]);

					newTr = document.createElement("tr");
					newTd = document.createElement("td");
					al = document.createAttribute("align");
					al.value = "center";
					newTd.setAttributeNode(al);
					newTr.appendChild(newTd);
					tbody.appendChild(newTr);
				}

				var tr = getChildOfType(tbody, "TR", 1);
				var td1 = getChildOfType(tr, "TD", 1);
				td1.innerHTML = img1;
				tr = getChildOfType(tbody, "TR", 2);
				td1 = getChildOfType(tr, "TD", 1);
				setClass(td1, lvlClass);
				tr = getChildOfType(tbody, "TR", 3);
				td1 = getChildOfType(tr, "TD", 1);
				td1.innerHTML = img2;
			}


		var offsetTableArray = [];
		function updateItem_hp(key, updateInfo, item, dynaGrid) {
			try{
				if(updateInfo != null) {
					var SUBSCR_FLD_SCHEDULED_HOURLY;
					if ((updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_GROUP_CLOSE) & 1 != 0) && (updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_STATE) >= 2) && (updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_STATE) <= 5)) {
						SUBSCR_FLD_SCHEDULED_HOURLY = 2 + updateInfo.getChangedFieldValue(SUBSCR_FLD_PRIORITY);
					} else if (updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_STATE) >= 6 || updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_STATE) == 1) {
						SUBSCR_FLD_SCHEDULED_HOURLY = 4 + updateInfo.getChangedFieldValue(SUBSCR_FLD_PRIORITY);
					} else {
						SUBSCR_FLD_SCHEDULED_HOURLY = 6 + updateInfo.getChangedFieldValue(SUBSCR_FLD_PRIORITY);
					}
					if (updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_LEVEL) == 0) {
						SUBSCR_FLD_SCHEDULED_HOURLY = 8 + updateInfo.getChangedFieldValue(SUBSCR_FLD_PRIORITY);
					} 
	
					var SUBSCR_FLD_CLOSE_TIME;
					if (updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_STATE) != OPPORTUNITY_STATE_SUSPENDED && updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_STATE) != null) { //check if its not suspend msg and its state change
						var tmpDate = updateInfo.getChangedFieldValue("ET_EST_CLOSE");
						var est_date = new Date(tmpDate); 
						if (!isNaN(est_date.getHours())) {
							est_date = formatDateAssetsList(est_date, updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_STATE));
							if (updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_STATE) == OPPORTUNITY_STATE_HIDDEN_WAITING_TO_EXPIRY) {
								var est_dateAr = est_date.split(" ");
								est_date = est_dateAr[0];
								est_date += "<br/>" + bundle_msg_waiting_end;
							}
							SUBSCR_FLD_CLOSE_TIME = est_date;
							SUBSCR_FLD_ET_EST_CLOSE = est_date;
							try{
								updateInfo.setCellValue("expires", SUBSCR_FLD_CLOSE_TIME);
							}catch(e){}
							try{						
								updateInfo.setCellValue("available", SUBSCR_FLD_CLOSE_TIME);
							}catch(e){}
						}
	
					} else if (updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_STATE) == OPPORTUNITY_STATE_SUSPENDED) {
						var suspendMsg = '';
						if (updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_SUSPENDED_MESSAGE) != null) {
							suspendMsg = updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_SUSPENDED_MESSAGE).split("s");
						}
						var suspendMsgString =  suspendMsgUpperLine[new Number(suspendMsg[0]) - 1];
	
						SUBSCR_FLD_CLOSE_TIME = suspendMsgString;
						SUBSCR_FLD_ET_EST_CLOSE_COPY = suspendMsgString;
						try{
							updateInfo.setCellValue("expires", SUBSCR_FLD_CLOSE_TIME);
						}catch(e){}
						try{						
							updateInfo.setCellValue("available", SUBSCR_FLD_CLOSE_TIME);
						}catch(e){}
					}
					var SUBSCR_FLD_OLD_STATE = dynaGrid.getValue(key, SUBSCR_FLD_ET_STATE);
	
					//add field to change display name
					var marketId = updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_NAME);
					if(marketId != null) {
						updateInfo.setCellValue(SUBSCR_FLD_ET_ID, marketId);
						var SUBSCR_FLD_ET_DISPLAY_NAME = getMarketName(marketId);
						updateInfo.setCellValue(SUBSCR_FLD_ET_NAME, SUBSCR_FLD_ET_DISPLAY_NAME);
					}
					
					if (updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_LEVEL) != 0) {
						if (!userLogin) {
							if (updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_LEVEL) != null) {
								var level = updateInfo.getCellValue(SUBSCR_FLD_ET_LEVEL);
								var level_float = parseFloat(updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_LEVEL).replace(/,/g,''));
								var levelArray = level.split('.');
								var levelDecimalLength = (levelArray.length > 1) ? levelArray[1].length : 0;
								if (!offsetTableArray[updateInfo.getCellValue(SUBSCR_FLD_ET_ID)]) {
									var min = 0.0015;
									var max = 0.0025;
									var sign = [-1,1][Math.random()*2|0];
									offsetTableArray[updateInfo.getCellValue(SUBSCR_FLD_ET_ID)] = +(sign * (level_float * (Math.random() * (max - min) + min))).toFixed(levelDecimalLength);
								}
								updateInfo.setCellValue(SUBSCR_FLD_ET_LEVEL, numberWithCommas((level_float + offsetTableArray[updateInfo.getCellValue(SUBSCR_FLD_ET_ID)]).toFixed(levelDecimalLength)));
							}
						}
					}
					
					//update open market array so we will know if there is open markets
					updateOpenMarkets(updateInfo);
	
	  				if (strHref.indexOf("index.jsf") > -1) {
	  					if (updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_AO_STATES) || null == updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_AO_STATES) || isCalcalistAndGameEnded) {
	  						if (checkAllClose() || isCalcalistAndGameEnded) {
	  							//show off hour banner
	  							if (isCalcalist) {
	  								document.getElementById('assetsFooter').style.display='none';
	  							}
	  							switchTable(0);
	  						} else { //show the market table
		  						if (isCalcalist) {
		  							document.getElementById('assetsFooter').style.display=navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table';
		  						}
								switchTable(1);
	  						}
	  					}
	  				} else {
		  				if (updateInfo.getChangedFieldValue(SUBSCR_FLD_COMMAND) == "DELETE") {
							//if deleted is hourly look for the next hourly on the page to show;
							if (updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_SCHEDULED) == 1) {
								showSelectedRow(updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_NAME), updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_OPP_ID));
							}
						}
						if (updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_AO_STATES)!=null) {
							
							if (updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_STATE) == OPPORTUNITY_STATE_CREATED || updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_STATE) == OPPORTUNITY_STATE_SUSPENDED || updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_STATE) == OPPORTUNITY_STATE_HIDDEN_WAITING_TO_EXPIRY || isCalcalistAndGameEnded) {
		  						if (checkAllClose() || isCalcalistAndGameEnded) { //hide the profit line button
									profitLineBtn.style.display = "none";
		  						}
		  					} 
	//						else { //show the profit line button
	//	  						var profitLineBtn = document.getElementById("profitLineButton"); //profit line open button
	//							profitLineBtn.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'inline';
	//	  					}
	  					}
					}
					if (updateInfo.getChangedFieldValue(SUBSCR_FLD_COMMAND) == "DELETE") {
						rowsCount--;
						opportunityOnPage[updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_OPP_ID)] = undefined;
						if (rowsCount == 0) {
							var mTbl = document.getElementById("markets_table");
							var tbls = mTbl.getElementsByTagName("TABLE");
							var tbody = getChildOfType(tbls[1], "TBODY", 1);
							var row = getChildOfType(tbody, "TR", 2);
							row.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';
						}
					}
	
					if (updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_STATE) != null) {
						var state = updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_STATE);				
						var oldState = updateInfo.getChangedFieldValue(SUBSCR_FLD_OLD_STATE);
						if (state == OPPORTUNITY_STATE_WAITING_TO_PAUSE) {
							formatValuesWaitingToPause(item, updateInfo);
							//hide it and exit
							return;
						} else if (state == OPPORTUNITY_STATE_CREATED) {
							formatValuesCreated(item, updateInfo);
	
						} else if (state == OPPORTUNITY_STATE_OPENED) {
							formatValuesOpened(item, updateInfo);
	
						} else if (state == OPPORTUNITY_STATE_LAST_10_MIN) {
							//logg("OPPORTUNITY_STATE_LAST_10_MIN");
							if (oldState == OPPORTUNITY_STATE_CREATED || oldState == OPPORTUNITY_STATE_PAUSED || oldState == OPPORTUNITY_STATE_SUSPENDED) {
								formatValuesOpened(item, updateInfo);
							}
							formatValues10Min(item, updateInfo);
	
						} else if (state == OPPORTUNITY_STATE_CLOSING_1_MIN) {
							formatValuesClosing(item, updateInfo);
						} else if (state == OPPORTUNITY_STATE_CLOSING) {
							formatValuesClosing(item, updateInfo);
						} else if (state == OPPORTUNITY_STATE_CLOSED) {
							formatValuesClosing(item, updateInfo);
						} else if (state == OPPORTUNITY_STATE_SUSPENDED ) {										
							var td = getChildOfType(item, "TD", 5);
							td.colSpan = 2;
							td = getChildOfType(item, "TD", 6);
							td.style.display = "none";										
						}
						var scheduled = updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_SCHEDULED);
						
						if (skinId == 3) {
						
							var myregexp = /[^%]*/;
							var match = myregexp.exec(updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_ODDS));
							if (match != null) {
								var td = getChildOfType(item, "TD", 3);
								td.innerHTML = '%' + match[0];			
							}
						}
						
					}
	
					formatValuesChangeColors(item, updateInfo);
				}
			} catch(e){
				 logIt({'type':3,'msg':e});
			}

	}

			function formatValuesWaitingToPause(item, itemUpdate) {
				item.style.display = 'none';
			}

			function formatValuesNotOpened(item, itemUpdate) {


				item.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';
				try {
					setClass(item, "line_invest_with_border");
				} catch(r) {
					item.setAttribute(classAtr, "line_invest_with_border");
				}
				var td = getChildOfType(item, "TD", 1);
				setClass(td, "invest_asset_not_opened");
				td = getChildOfType(item, "TD", 2);
				changeImages(td, "<img src=\"" + context_path + "/images/call_invClose.gif\" alt=\"\" width=\"52\" height=\"24\" border=\"0\">", "<img src=\"" + context_path + "/images/put_invClose.gif\" alt=\"\" width=\"52\" height=\"24\" border=\"0\">", "invest_level_not_open");
				td = getChildOfType(item, "TD", 3);
				setClass(td, "invest_odds_not_opened");
				td = getChildOfType(item, "TD", 4);
				td.style.display = 'none';
				td = getChildOfType(item, "TD", 5);
				td.style.display = 'none';
				td = getChildOfType(item, "TD", 9);

				var tbl1 = getChildOfType(td, "TABLE", 1);
				var tbb1 = getChildOfType(tbl1, "TBODY", 1);
				var tbr1 = getChildOfType(tbb1, "TR", 1);
				var tbd1 = getChildOfType(tbr1, "TD", 2);
				var tbl2 = getChildOfType(tbd1, "TABLE", 1);
				var tbb2 = getChildOfType(tbl2, "TBODY", 1);
				var tbr2 = getChildOfType(tbb2, "TR", 2);
				var tbd2 = getChildOfType(tbr2, "TD", 1);
				var spn1 = getChildOfType(tbd2, "SPAN", 1);

				var state = itemUpdate.getChangedFieldValue(SUBSCR_FLD_ET_STATE);
				setClass(tbd2, state == OPPORTUNITY_STATE_SUSPENDED ? "inv_closed_red" : "inv_closed");
				if (state == OPPORTUNITY_STATE_SUSPENDED) {
					spn1.style.display = 'none';
				} else {
					spn1.style.display = 'block';
					if (state == OPPORTUNITY_STATE_HIDDEN_WAITING_TO_EXPIRY) {
						spn1.innerHTML = bundle_msg_waiting;
					}
				}

				td.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-cell';

				td = getChildOfType(item, "TD", 1);
				var chartImg = getChildOfType(td, "IMG", 1);
				try{
					if (navigator.userAgent.toLowerCase().indexOf("msie") != -1) { //IE
						var atts = chartImg.attributes;
						var cls = atts.getNamedItem("src");
						cls.value = context_path + "/images/chart_closed_disabled.gif";
					} else { //FF
						chartImg.setAttribute("src", context_path + "/images/chart_closed_disabled.gif");
					}
					chartImg.style.cursor = "";
				} catch(r) {

				}
			}

			function formatValuesOpened(item, itemUpdate) {
				td = getChildOfType(item, "TD", 6);
				td.firstChild.src = imgPathA + "/icon_settled.png";
				setClass(td, "hp_assets_subtable_line");
			}

			function formatValuesCreated(item, itemUpdate) {
				td = getChildOfType(item, "TD", 5);
				td.style.display = "none";
				td = getChildOfType(item, "TD", 6);
				td.style.display = "none";

				var td = getChildOfType(item, "TD", 4);
				td.style.display = "";
			}

			function formatValues10Min(item, itemUpdate) {
				var td = getChildOfType(item, "TD", 6);
				td.innerHTML = document.getElementById("template_10min").innerHTML;
				setClass(td, "table_line_nd exptime_nd");
				var span = getChildOfType(td, "SPAN", 1);
				getTimeLeft(itemUpdate.getChangedFieldValue(SUBSCR_FLD_ET_OPP_ID), span);

			}

			function formatValuesClosing1Min(item, itemUpdate) {
				var atts = item.attributes;
				var cls = atts.getNamedItem("class");
				cls.value = "invest_close";

				//if we r opeening this page with this state we must get group id from LS
				var groupId;
				if (itemUpdate != null) {
					groupId = itemUpdate.getChangedFieldValue(SUBSCR_FLD_ET_GROUP);
				} else {
					groupId = getOppValue(item, 15);
				}

				var td = getChildOfType(item, "TD", 5);
				if (groupId == 2) {
					td.innerHTML = document.getElementById("template_closing_1_min_ind").innerHTML;
				} else if (groupId == 3) {
					td.innerHTML = document.getElementById("template_closing_1_min_stk").innerHTML;
				} else if (groupId == 4) {
					td.innerHTML = document.getElementById("template_closing_1_min_crr").innerHTML;
				} else if (groupId == 5) {
					td.innerHTML = document.getElementById("template_closing_1_min_cmd").innerHTML;
				} else {
					td.innerHTML = document.getElementById("template_closing_1_min_frn").innerHTML;
				}

				td = getChildOfType(item, "TD", 2);
				var table = getChildOfType(td, "TABLE", 1);
				var tbody = getChildOfType(table, "TBODY", 1);
				var trs = tbody.getElementsByTagName("TR");
				if (trs.length == 3) {
					tbody.removeChild(trs[0]);
					tbody.removeChild(trs[1]);
				}

				td = getChildOfType(item, "TD", 1);
				var chartImg = getChildOfType(td, "IMG", 1);
				try{
					chartImg.setAttribute("src", context_path + "/images/chart_closed.gif");
				} catch(r) {
					//got excption but its working
				}

				var oppId;
				if (itemUpdate != null) {
					oppId = itemUpdate.getChangedFieldValue(SUBSCR_FLD_ET_OPP_ID);
				} else {
					oppId = getOppValue(item, 6);
				}

				var chartIframe = document.getElementById("chart" + oppId);
				if (chartIframe != null) {
					chartIframe.contentWindow.showWaitingForExpiry();
				}
			}

			function formatValuesClosing(item, itemUpdate) {
//			logg("start formatValuesClosing");
				td = getChildOfType(item, "TD", 6);
//				logg("start1");
				var clockImg = getChildOfType(td, "IMG", 1);
				clockImg.setAttribute("src", imgPathA + "/" + locale_SkinId + "/icon_ExpW.gif");
//				td.firstChild.src
//				logg("start2");
				var spans = td.getElementsByTagName("SPAN");
				if (spans.length > 0) {
				//logg("start4");
					td.removeChild(spans[0]);
				}
//				logg("start5");
				if (null != itemUpdate && itemUpdate.getChangedFieldValue(SUBSCR_FLD_ET_LEVEL) == 0) {
					setClass(td,"table_line_nd avWaiting_nd");
				}
//				logg("endd");
				
			}

			function formatValuesClosed(item, itemUpdate) {
				try{
					var atts = item.attributes;
					var cls = atts.getNamedItem("class");
					cls.value = "invest_close";

					var td = getChildOfType(item, "TD", 5);
					td.innerHTML = "\u00a0";

					td = getChildOfType(item, "TD", 2);
					var table = getChildOfType(td, "TABLE", 1);
					var tbody = getChildOfType(table, "TBODY", 1);
					var trs = tbody.getElementsByTagName("TR");
					if (trs.length == 3) {
						tbody.removeChild(trs[0]);
						tbody.removeChild(trs[1]);
					}
					trs = tbody.getElementsByTagName("TR");
					if (trs.length == 1) {
						var newTr = document.createElement("tr");
						var newTd = document.createElement("td");
						var al = document.createAttribute("align");
						al.value = "center";
						newTd.setAttributeNode(al);
						newTd.innerHTML = "<img src=\"" + context_path + "/images/stamp.gif\" alt=\"\" width=\"66\" height=\"12\" border=\"0\" />";
						newTr.appendChild(newTd);
						tbody.appendChild(newTr);
					}
					td = getChildOfType(item, "TD", 1);
					var chartImg = getChildOfType(td, "IMG", 1);
				} catch(e) {
					  logIt({'type':3,'msg':e});
				}


				var tdName = getChildOfType(item, "TD", 1);
				var imgChart = getChildOfType(tdName, "IMG", 1);
				imgChart.style.visibility = "hidden";

				var chartIframe = document.getElementById("chart" + itemUpdate.getChangedFieldValue(SUBSCR_FLD_ET_OPP_ID));
//				alert("state changed to closed, closing graph chartIframe " + chartIframe);
				if (chartIframe != null) {
//					alert("state changed to closed, closing graph");
					openChart(null, item);
				}

			}

			function formatValuesChangeColors(item, itemUpdate) {
				try{
						if (itemUpdate.getChangedFieldValue(SUBSCR_FLD_ET_CLR) != null) {
							var state = itemUpdate.getChangedFieldValue(SUBSCR_FLD_ET_STATE);
							if (state == OPPORTUNITY_STATE_CREATED || state == OPPORTUNITY_STATE_PAUSED || state == OPPORTUNITY_STATE_SUSPENDED || state == OPPORTUNITY_STATE_HIDDEN_WAITING_TO_EXPIRY) {
								return;
							}
							var td = getChildOfType(item, "TD", 2);
							var div = getChildOfType(td, "DIV", 1);
							var clrChange = itemUpdate.getChangedFieldValue(SUBSCR_FLD_ET_CLR);
							if (clrChange == 0) {
								div.style.color = "#ca0004";
							} else if (clrChange == 1) {
								div.style.color = "#727272";
							} else if (clrChange == 2) {
								div.style.color = "#2AA600";
							}
						}
				}catch(e) {
					logIt({'type':3,'msg':e});
				}
			}

			function updateMarketsSelection(visibleRow) {
				var mSel = document.getElementById("select_market");
				var market = mSel.options[mSel.selectedIndex].value;

	//			var tSel = document.getElementById("select_time");
	//			var time = tSel.options[tSel.selectedIndex].value;

				var mTbl = document.getElementById("markets_table");
				var tbls = mTbl.getElementsByTagName("TABLE");
				var tbody = getChildOfType(tbls[1], "TBODY", 1);
				var visible = 0;
				// when loop over the rows show only one per market when filtered by time
				var lastMarket = '00';
				var priority;
				var close;
				var marketId;
				var state;
				var row;
				var i = 5;
				do {
					row = getChildOfType(tbody, "TR", i);
					if (null != row) {
						priority = getOppValue(row, 10).substring(1, 3);
						close = getOppValue(row, 11);
						marketId = getOppValue(row, 16);
						state = getOppValue(row, 17);
						if ((market == '00' || market == priority) && (state != 10) && ((close & userPickArray[marketId]) != 0 || state == OPPORTUNITY_STATE_CREATED || state == OPPORTUNITY_STATE_SUSPENDED || state == OPPORTUNITY_STATE_PAUSED)) {
							row.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';
							visible = 1;
							lastMarket = priority;
						} else {
							row.style.display = 'none';
						}
					} else {
						break;
					}
					i = i + 1;
				} while (true);

				mTbl = document.getElementById("markets_table");
				tbls = mTbl.getElementsByTagName("TABLE");
				tbody = getChildOfType(tbls[1], "TBODY", 1);
				row = getChildOfType(tbody, "TR", 3);
				if (visible == 0 && visibleRow != true) {
					row.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';
				} else {
					row.style.display = 'none';
				}
			}

			function checkUpdatesFrequency() {
//				alert(updatesFrequencyFlag);
				if (updatesFrequencyFlag == 0) {
					lsPage.removeTable("opps");
//					alert(updatesFrequencyIn);
					newTable.setRequestedMaxFrequency(updatesFrequencyIn);
					lsPage.addTable(newTable, "opps");
					updatesFrequencyFlag = 1;
					// bugId 2029 start
					clearInterval(rndUpdtTimer);
					clearInterval(rndCopyTimer);
					// bugId 2029 end
					showBuyButton("10","14");
				}
			}

			//bugId 2029 start
			//start random update if not log in
			if (updatesFrequencyFlag == 0) {
				var rndUpdtTimer = setInterval ( "updateLevelRandomly()", 5000 );
				var rndCopyTimer = setInterval ( "copyRealLevel()", 900000 ); //900000
			}



			// set opp value used in 2029 and 2050
			//parm opp: the row of the opp in the table
			//parm field: the number of the TD we want to change
			//parm value: the new value
			//parm level: 1 if its new level then format it like we write level on the web. 0 if its not level
			function setOppValue(opp, field, value, level) {
				var td = getChildOfType(opp, "TD", field);
			//	alert("td " + td);
				var div = getChildOfType(td, "DIV", 1);
				if (div == null) {
					return;
				}
			//	alert("div " + div);
			//	alert(div.innerHTML + " new: " + addCommas(convertStringToFloat(div.innerHTML) + parseFloat(value), div.innerHTML));
				if (level == 1) {
					div.innerHTML = (addCommas(addRandomValue(div.innerHTML, parseFloat(value)), div.innerHTML));
				} else {
					div.innerHTML = value;
				}
			}

			//set opp level
			//parm opp: the row of the opp in the table
			//parm value: the value for the new level
			//parm flag: format the values or not
			function setOppLevel(opp, value, flag) {
			//	alert("set");
				var td = getChildOfType(opp, "TD", 2);
				var table = getChildOfType(td, "TABLE", 1);
				var tbody = getChildOfType(table, "TBODY", 1);
				var tr = getChildOfType(tbody, "TR", 2);
				setOppValue(tr, 1, value, flag);
			}

			//set opp level and 15 min level
			//parm opp: the row of the opp in the table
			//parm value: the value for the new level
			//parm flag: format the values or not
			function setOppLevelAnd15MinLevel(opp, value, flag) {
				/*try {
				var td;
				if (opp.id.indexOf("opp") > -1) {
					var rowTd = getChildOfType(opp, "TD", 1);
					var rowTable = getChildOfType(rowTd, "TABLE", 1);
					var rowTbody = getChildOfType(rowTable, "TBODY", 1);
					var rowTr = getChildOfType(rowTbody, "TR", 1);
					td = getChildOfType(rowTr, "TD", 2);
				} else {
					td = getChildOfType(opp, "TD", 2);
				}

				var table = getChildOfType(td, "TABLE", 1);
				var tbody = getChildOfType(table, "TBODY", 1);
				var tr = getChildOfType(tbody, "TR", 2);
				if (null == tr) {
					tr = getChildOfType(tbody, "TR", 1);
				}


				if (value == 0) {
					value = "<img src='../images/wait.gif' border='0' />";
				}
				setOppValue(tr, 1, value , flag);
				setOppValue(tr, 3, value , flag);
				updateSlipLevel(getOppValue(opp, 6), getOppValue(tr, 1));
				} catch(r) {

				}*/
			}

			//use to update the level of the opps randomly
			function updateLevelRandomly() {
				var row;
				var state;
				var oppId;
				var chartIframe;

				var mTbl = document.getElementById("markets_table");
				if (null != mTbl) {
					var tbls = mTbl.getElementsByTagName("TABLE");
					var tbody = getChildOfType(tbls[1], "TBODY", 1);
					var i = 5;
					do {
						row = getChildOfType(tbody, "TR", i);
						if (null != row ) {
							state = getOppValue(row, 14);
							oppId = getOppValue(row, 6);
							chartIframe = document.getElementById("chart" + oppId);
							if (state != OPPORTUNITY_STATE_CREATED  &&  state != OPPORTUNITY_STATE_PAUSED && state != OPPORTUNITY_STATE_SUSPENDED && state != OPPORTUNITY_STATE_CLOSED && state != OPPORTUNITY_STATE_WAITING_TO_PAUSE && state != OPPORTUNITY_STATE_HIDDEN_WAITING_TO_EXPIRY && chartIframe == null) {
								if ((state != OPPORTUNITY_STATE_CLOSING) &&
									(state != OPPORTUNITY_STATE_CLOSING_1_MIN) ){
									setOppLevelRandomChange(row, getRandom(row));
									updateSlipLevel(getOppValue(row, 6), getOppLevel(row));

								} else {
									if (getOppLevel(row).indexOf("wait") == -1) {
										updateState4(row, getRandom(row));
									}
								}
							}
						} else {
							break;
						}
						i = i + 1;
					} while (true);
				}
			}

			function setOppLevelRandomChange(opp, random) {
				var td;
				if (opp.id.indexOf("opp") > -1) {
					var rowTd = getChildOfType(opp, "TD", 1);
					var rowTable = getChildOfType(rowTd, "TABLE", 1);
					var rowTbody = getChildOfType(rowTable, "TBODY", 1);
					var rowTr = getChildOfType(rowTbody, "TR", 1);
					td = getChildOfType(rowTr, "TD", 2);
				} else {
					td = getChildOfType(opp, "TD", 2);
				}

				var table = getChildOfType(td, "TABLE", 1);
				var tbody = getChildOfType(table, "TBODY", 1);
				var tr = getChildOfType(tbody, "TR", 2);

				var td1 = getChildOfType(tr, "TD", 1); // the real level to show
				var div1 = getChildOfType(td1, "DIV", 1);
				var td2 = getChildOfType(tr, "TD", 3); // the 15 min level to copy from
				var div2 = getChildOfType(td2, "DIV", 1);
				if (null == div1 || null == div2) {
					return;
				}

				if (div2.innerHTML.indexOf("wait") == -1) {
					div1.innerHTML = addCommas(addRandomValue(div2.innerHTML, parseFloat(random)), div2.innerHTML);
				} else {
					div1.innerHTML = div2.innerHTML;
				}


			}

			//add random value to the last level
		    function addRandomValue(str, value) {
		    	var start;
		    	var end;
                //delete commas ","
                var iStr = str;
                var index = iStr.indexOf(",");
            //    alert("b4 loop: " + iStr);
                while (index != -1) {
                    start = iStr.substring(0, index);
                    end = iStr.substring(index + 1);
                    iStr = start + end;
                    index = iStr.indexOf(",");
                }
                //count digits after zero before adding random value
                iStr += '';
        //        alert("Str after loop: " + iStr);
                var c2Length = 0;
                var x = iStr.split('.');
                var x2 = x[1];
                var x2Length = x2.length;
                //adding the random value
//                var result = parseFloat(iStr) + parseFloat(value);
                var result = parseFloat(iStr) + parseFloat(iStr) *
				parseFloat(value); // Tony - the random value is a fraction not absolute value

                //add missing zero
                result = result.toFixed(x2Length);
                return result;
            }



			//round the number with howmuch digits after the dot. ie: x.xxx or x.xxxx
			//takeing it from the last level that was shown (the first level comeing from LS is rounded)
			function round(num ,oldLevel) {
				oldLevel += '';
				c2Length = 0;

				x = oldLevel.split('.');
				x2 = x[1];
				c2Length = x2.length;

				return decRound(num,c2Length);

			}

			// add the commas and round the number
			//parm nStr: the new level
			//parm oldLevel: the old level to comper the round format
			function addCommas(nStr, oldLevel) {
			//	alert("b4 nStr " + nStr + " old level " + oldLevel);
				//nStr = round(nStr, oldLevel);
			//	alert("nStr " + nStr + " old level " + oldLevel);
				nStr += '';
				x = nStr.split('.');
				x1 = x[0];
				x2 = x.length > 1 ? '.' + x[1] : '';
				var rgx = /(\d+)(\d{3})/;
				while (rgx.test(x1)) {
					x1 = x1.replace(rgx, '$1' + ',' + '$2');
				}
				return x1 + x2;
			}

			//the random change to the level when user not login
			//parm row: the row of the opp
			function getRandom(row){
				var rnd = Math.random();
		//		alert(" random " + rnd + "  random with cell and floor:  " + (rnd * (0.0002 - (-0.0002)) + (-0.0002)) + " 10 + randomCF  " + 10 + parseFloat(rnd * (0.0002 - (-0.0002)) + (-0.0002)) );
				var rndFloor = parseFloat(getOppValue(row, 12));
				var rndCeiling = parseFloat(getOppValue(row, 13));
				if ( rndCeiling == 0 && rndFloor == 0 ) {
					rndFloor = -0.0001;
					rndCeiling = 0.0001;
				}
				var fnRnd = rnd * (rndCeiling - (rndFloor)) + (rndFloor);
			//	alert("floor: " + rndFloor + " cell: " + rndCeiling + "rnd " + rnd + " frand " + fnRnd );
			//	return rnd * (0.0002 - (-0.0002)) + (-0.0002);
				return fnRnd;
			}

			//update the sleep with the random level every change
			//parm: oppId: the opp id to copy
			//parm: oppLevel the new level to write in the sleep
			function updateSlipLevel(oppId, oppLevel) {
				var slip = document.getElementById("slip");
				var crrEntry;
				var crrTbls;
				var entId;
				for (var i = 1; i <= slipCount; i++) {
					crrEntry = getChildOfType(slip, "DIV", i);
					crrTbls = crrEntry.getElementsByTagName("TABLE");
					entId = getEntryValue(crrTbls[1], 1);

					if (oppId == entId) {
						if (getEntryLabel(crrTbls[1], 1) == 0 && getEntryData(crrTbls[1], 1, 1) == 0) { // if not success & not submitting
							setEntryValue(crrTbls[2], 3, oppLevel);
						}
						break;
					}
				}
			}

			//copy real level from hidden TD to shown TD
			//useing in the timer every 15 min
			function copyRealLevel(){

				var mTbl = document.getElementById("markets_table");
				var tbls = mTbl.getElementsByTagName("TABLE");
				var tbody = getChildOfType(tbls[1], "TBODY", 1);
				var row;
				var state;
				var i = 5;
				do {
					row = getChildOfType(tbody, "TR", i);
					if (null != row) {
						state = getOppValue(row, 14);
						if (state != OPPORTUNITY_STATE_CREATED && state != OPPORTUNITY_STATE_PAUSED && state != OPPORTUNITY_STATE_SUSPENDED && state != OPPORTUNITY_STATE_CLOSED && state != OPPORTUNITY_STATE_WAITING_TO_PAUSE && state != OPPORTUNITY_STATE_HIDDEN_WAITING_TO_EXPIRY) {
							if ((state != OPPORTUNITY_STATE_CLOSING) &&
								(state != OPPORTUNITY_STATE_CLOSING_1_MIN) ){

								setOppLevelAnd15MinLevel(row, getHiddenOppLevel(row,2), 0);
								updateSlipLevel(getOppValue(row,6),getOppLevel(row));
							} else {
								updateState4(row, getHiddenOppLevel(row, 1));
							}
						}
					} else {
						break;
					}
					i = i + 1;
				} while (true);
			}

			//return the value of the hidden TD with the lightstreamer level value
			//parm opp: row
			//parm trNum: 1 if its state 4 or 5, else 2
			function getHiddenOppLevel(opp, trNum) {
				var td;
				if (opp.id.indexOf("opp") > -1) {
					var rowTd = getChildOfType(opp, "TD", 1);
					var rowTable = getChildOfType(rowTd, "TABLE", 1);
					var rowTbody = getChildOfType(rowTable, "TBODY", 1);
					var rowTr = getChildOfType(rowTbody, "TR", 1);
					td = getChildOfType(rowTr, "TD", 2);
				} else {
					td = getChildOfType(opp, "TD", 2);
				}

				var table = getChildOfType(td, "TABLE", 1);
				var tbody = getChildOfType(table, "TBODY", 1);
				var tr = getChildOfType(tbody, "TR", trNum);
				return getOppValue(tr, 2);
			}

			//update the level and sleep for state number 4 (dom was changed)
			//parm: opp is the row
			//parm value: random value to add
			function updateState4(opp, value) {
				var td;

				if (opp.id.indexOf("opp") > -1) {
					var rowTd = getChildOfType(opp, "TD", 1);
					var rowTable = getChildOfType(rowTd, "TABLE", 1);
					var rowTbody = getChildOfType(rowTable, "TBODY", 1);
					var rowTr = getChildOfType(rowTbody, "TR", 1);
					td = getChildOfType(rowTr, "TD", 2);
				} else {
					td = getChildOfType(opp, "TD", 2);
				}

				var table = getChildOfType(td, "TABLE", 1);
				var tbody = getChildOfType(table, "TBODY", 1);
				var tr = getChildOfType(tbody, "TR", 1);

				var td1 = getChildOfType(tr, "TD", 1); // the real level to show
				var div1 = getChildOfType(td1, "DIV", 1);
				var td2 = getChildOfType(tr, "TD", 3); // the 15 min level to copy from

				var div2 = getChildOfType(td2, "DIV", 1);

				if (null == div1 || null == div2) {
					return;
				}
				if (div2.innerHTML.indexOf("wait") == -1) {
					div1.innerHTML = addCommas(addRandomValue(div2.innerHTML, parseFloat(value)), div2.innerHTML);
					updateSlipLevel(getOppValue(opp,6),getOppValue(tr,1));
				} else {
					div1.innerHTML = div2.innerHTML;
				}
			}

			//bugId 2029 end

			//check if all opps on home page are closed ( state 1 created)
			//return 1 if all close else 0
			function checkAllClose() {
				for (j = 0; j < marketStates.length; j++) {
					if (marketStates[j][1] > 0 && getMarketName(marketStates[j][0]) != "") {
						return 0;
					}
				}
				return 1;
			}

			var logging = false;
			function log(msg) {
				if (!logging) {
					return;
				}
				try {
					var log = document.getElementById("log");
					log.innerHTML = log.innerHTML + getCurrentTimeFormatted() + " " + msg + "<br />";
				} catch (e) {
					// do nothing
				}
			}

			function getCurrentTimeFormatted() {
				var tf = "";
				try {
					var d = new Date();
					tf = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDay() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds() + "." + d.getMilliseconds();
				} catch (e) {
					// do nothing
				}
				return tf;
			}

		function clearLog() {
			if (!logging) {
				return;
			}
			try {
				var log = document.getElementById("log");
				log.innerHTML = "";
			} catch (e) {
				// do nothing
			}
		}

	// history charts
	var HINTS_CFG = {
		'follow'     : false,
		'smart'      : false,
		'margin'     : 0, // 10
		'gap'        : 0, // 20
		'align'      : 'trtr',
		'css'        : '',
		'show_delay' : 10,
		'hide_delay' : 10,
		'IEtrans'    : ['blendTrans(DURATION=.3)', 'blendTrans(DURATION=.3)'],
		'opacity'    : 80
	};
	var chartItems = [
		'<div id="historyChartDiv"></div>'
	];
	var chartHint = new THints(chartItems, HINTS_CFG);

	function showHistoryChart(img) {
		var td = img.parentNode;
		var oppState = getOppValue(td.parentNode, 14);
		if (oppState == OPPORTUNITY_STATE_CREATED || oppState >= OPPORTUNITY_STATE_CLOSING_1_MIN ) {
			return;
		}
		var marketId = getOppValue(td.parentNode, 16);
		log("show marketId: " + marketId);
		var tsd = document.getElementById("historyChartDiv");
		tsd.innerHTML = '<img src="../charts/' + marketId + '_heb" border="1" width="180" height="90" />';
		chartHint.show(0, getChildOfType(td.parentNode,"TD",1));
	}

	function hideHistoryChart(img) {
		var td = img.parentNode;
		var oppState = getOppValue(td.parentNode, 14);
		if (oppState == OPPORTUNITY_STATE_CREATED || oppState >= OPPORTUNITY_STATE_CLOSING_1_MIN) {
			return;
		}
		var marketId = getOppValue(td.parentNode, 16);
		log("hide marketId: " + marketId);
		chartHint.hide(0, getChildOfType(td.parentNode, "TD", 1));
	}

	function getUTCOffset() {
		var d = new Date();
		var localHour = d.getHours();
		var utcHour = d.getUTCHours();
		var offset = localHour - utcHour;
		if (offset < -12) {
			offset += 24;
		}
		if (offset > 12) {
			offset -= 24;
		}
		return offset;
	}

	function adjustFromUTCToLocal(toAdj) {
		var adjM = toAdj.getMinutes() - new Date().getTimezoneOffset();
		var adjH = 0;
		if (adjM >= 0) {
			adjH = Math.floor(adjM / 60);
			adjM = adjM % 60;
		} else {
			adjH = Math.floor(Math.abs(adjM) / 60);
			if (Math.abs(adjM) % 60 != 0) {
				adjH += 1;
				adjM = 60 - Math.abs(adjM) % 60;
			} else {
				adjM = Math.abs(adjM) % 60;
			}
			adjH = -adjH;
		}
		toAdj.setHours(toAdj.getHours() + adjH);
		toAdj.setMinutes(adjM);
		return toAdj;
	}

	function formatDateAssetsList(est_date, state) {
	
		est_date = adjustFromUTCToLocal(est_date);
	
		var currdate = new Date();
		var min = est_date.getMinutes();
		var ruChar = document.getElementById('russionChar').innerHTML;
		if (est_date.getDate() == currdate.getDate() && est_date.getMonth() == currdate.getMonth() && est_date.getFullYear() == currdate.getFullYear()) {
			if (new Number(est_date.getMinutes()) < new Number("10")) {
				min = "0" + est_date.getMinutes();
			}
			if (skinId == 10 && state == OPPORTUNITY_STATE_CREATED) {
				est_date = ruChar + est_date.getHours() + ":" + min + ", " + bundle_msg_today;
			} else {
				est_date = est_date.getHours() + ":" + min + " " + bundle_msg_today;
			}
			
		} else {
			if (new Number(est_date.getMinutes()) < new Number("10")) {
				min = "0" + est_date.getMinutes();
			}
			var year = new String(est_date.getFullYear());
			if (skinId == 10 && state == OPPORTUNITY_STATE_CREATED) {
				est_date = est_date.getDate() + "." + (est_date.getMonth()+1) + "." + year.substring(2) + ", " + ruChar + est_date.getHours() + ":" + min;
			} else {		
				est_date = est_date.getHours() + ":" + min + ", " + est_date.getDate() + "." + (est_date.getMonth()+1) + "." + year.substring(2);
			}				
		}
		return est_date;
	}

// ************************************************
// function needed for the homepage movie (Daniella)
// ************************************************
	// checking if to show the movie or not,setting styles

	function checkMovieCookie(){
		if(checkFirstTimeVisit == true ){
				showMovie = true;
				checkFirstTimeVisit = false;
		}else{
			showMovie = false;

		}

		var movieCookie = document.cookie;

		if(showMovie && strHref.indexOf("index.jsf") > -1){
			document.getElementById('assetsFooter').style.display='none';
			switchTable(2);
		}else{
			document.getElementById('assetsFooter').style.display=navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table';
			document.getElementById('markets_table').style.display=navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table';
			showMovie=false;
		}

	}

var isInternetExplorer = navigator.appName.indexOf("Microsoft") != -1;
// Handle all the FSCommand messages in a Flash movie.

function etrader_fc_DoFSCommand(command, args) {
	var etraderObj = isInternetExplorer ? document.all.etrader_fc : document.etrader_fc;

		if(command=='closeFlash'){
			document.getElementById('assetsFooter').style.display=navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table';
			if(strHref.indexOf("oneTouch") > -1){
				closeFlashOneTouch();
			} else {
				checkFirstTimeVisit=false;
				showMovie = false;
				if (checkAllClose() && marketsTimestamp > 0) {
					switchTable(0);
	  			} else {
	  				switchTable(1);
	  			}
			}
	}

}
// Hook for Internet Explorer.
if (navigator.appName && navigator.appName.indexOf("Microsoft") != -1 && navigator.userAgent.indexOf("Windows") != -1 && navigator.userAgent.indexOf("Windows 3.1") == -1) {
	document.write('<script language=\"VBScript\"\>\n');
	document.write('On Error Resume Next\n');
	document.write('Sub etrader_fc_FSCommand(ByVal command, ByVal args)\n');
	document.write('	Call etrader_fc_DoFSCommand(command, args)\n');
	document.write('End Sub\n');
	document.write('</script\>\n');
}
// ********************************************************
// END OF function needed for the homepage movie (Daniella)
// ********************************************************

function showBuyButton(msg, button) {
	var slip = document.getElementById("slip");
	var crrEntry;
	var crrTbls;
	//	alert(slip.innerHTML);
		for (var i = 1; i <= slipCount; i++) {
			crrEntry = getChildOfType(slip, "DIV", i);
			crrTbls = crrEntry.getElementsByTagName("TABLE");
			if (getEntryLabel(crrTbls[1], 1) == 0) {
				if (isRowOn(crrTbls[2], button)) {
					toggleRow(crrTbls[2], button);
				}
				if (!isRowOn(crrTbls[2], msg)) {
					toggleRow(crrTbls[2], msg);
				}
			}
		}
}

// Update is_accepted_sms to investment for getting expiration message
function updateInvestSms(element) {
	var sendSms = element.checked;
	var tBody = element.parentNode.parentNode.parentNode;
	var trInv = getChildOfType(tBody, "TR", 2);
	var tdInv = getChildOfType(trInv, "TD", 2);
	var invId = getChildOfType(tdInv, "SPAN", 2).innerHTML;

	var xmlHttp = getXMLHttp();
	if (null == xmlHttp) {
		return false;
	}
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState == 4) {
			if (xmlHttp.responseText.length >= 2) {
				if (xmlHttp.responseText.substring(0, 2) == "ok") {
					var td = element.parentNode;
					var sp = getChildOfType(td, "SPAN", 1);
					sp.innerHTML = acceptSmsMessage;
					//element.style.display = 'none';
					element.disabled = true;
				} else {
					alert('failed');
				}
			}
		}
	}
	xmlHttp.open("POST", "ajax.jsf", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.send("turnSmsOn&invId=" + invId );
}

function insertOption(row, txt, scheduled, marketId) {
	var selectArray = row.getElementsByTagName("SELECT");
	var elSel = selectArray[0];

	var elOptNew = document.createElement('option');
	elOptNew.text = txt;
	elOptNew.value = -1;

	elSel.options[elSel.selectedIndex] = elOptNew;
	elOptNew.selected = "1";

	elSel.remove(scheduled);

	for (var i = 0; i <= marketsWithoutLongTerm.length; i++) {
		if (marketsWithoutLongTerm[i] == marketId) {
			elSel.remove(2); //weekly
			elSel.remove(2); //monthly
			break;
		}
	}
}

function removeOption(row, value){
  var selectArray = row.getElementsByTagName("SELECT");
  var elSel = selectArray[0];
  var i;
  for (i = elSel.length - 1; i>=0; i--) {
    if (elSel.options[i].value == value) {
      elSel.remove(i);
    }
  }
}

function newUpdateMarketsSelection(timeList) {
	var time = timeList.options[timeList.selectedIndex].value;
	if (time == -1) {
		return;
	}
	var row = timeList.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode;
	var marketId = getOppValue(timeList.parentNode.parentNode, 16);
	userPickArray[marketId] = time;
	row.style.display = 'none';
	var oppId = getOppValue(row, 6);
	var chartIframe = document.getElementById("chart" + oppId);
	if (chartIframe != null) {
		openChart(null, row);
	}
	var newRow = showSelectedRow(marketId);
	var newRowScheduled = getOppValue(newRow, 18);

	var deleteScheduled = newRowScheduled;
	switch(time) {
		case "1": //hourly
		  if (newRowScheduled != 1) {
		  	deleteScheduled = 1;
		  }
		  break;
		case "2": //daily
		  if (newRowScheduled != 2) {
		  	deleteScheduled = 2;
		  }
		  break;
		case "4": //weekly
		  if (newRowScheduled != 3) {
		  	deleteScheduled = 3;
		  }
		  break;
		case "8": //monthly
		  if (newRowScheduled != 4) {
		  	deleteScheduled = 4;
		  }
		  break;
	}
	var selectArray = newRow.getElementsByTagName("SELECT");
	var newRowSL = selectArray[0];
//	if (!goodPick) { //we look for scheduled X and got opp with scheduled Y bcoz the opps was marged
		var tempSL = document.getElementById("temp_select_list").getElementsByTagName("SELECT")[0];
		var elOptNew;
		var i;
		for (i = newRowSL.length - 1; i >= 1; i--) { //delete all the option leave only the first 1 with the closing time
			newRowSL.remove(i);
		}

		for (i = 0; i < tempSL.length; i++) {
			elOptNew = document.createElement('option');
			elOptNew.text = tempSL.options[i].text;
			elOptNew.value = tempSL.options[i].value;
			newRowSL.options[i+1] = elOptNew;
		}

		newRowSL.remove(1);
		newRowSL.remove(deleteScheduled);

		for (var i = 0; i <= marketsWithoutLongTerm.length; i++) {
			if (marketsWithoutLongTerm[i] == marketId) {
				newRowSL.remove(2); //weekly
				newRowSL.remove(2); //monthly
				break;
			}
		}

		var oppState = getOppValue(newRow, 14);
		if (oppState == OPPORTUNITY_STATE_CLOSING) {
			if (newRowScheduled == 2 || newRowScheduled == 3) {
				if (newRowSL.length > 2) {
					newRowSL.remove(3); //remove the monthly for sure
					if (newRowScheduled == 2) { //if its daily remove the weekly 2
						newRowSL.remove(2);
					}
				}
			}
		}
//	}
	newRowSL.options[0].selected = "1";
	newRow.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';

}

//show selected row or return it depneds where we came from
//if we came from updateItem just show it
//if we came from row filter return the row
function showSelectedRow(marketId, oppId) {
	var mTbl = document.getElementById("markets_table");
	var tbls = mTbl.getElementsByTagName("TABLE");
	var tbody = getChildOfType(tbls[1], "TBODY", 1);
	var i = 5;
	var rowMarketId;
	var close;
	var row;
	var selectArray;
	var elSel;
	do {
		row = getChildOfType(tbody, "TR", i);
		if (null != row) {
			rowMarketId = getOppValue(row, 16);
			rowOppId = getOppValue(row, 6);
			close = getOppValue(row, 11);
			if ((rowMarketId == marketId) && ((close & userPickArray[marketId]) != 0) && oppId != rowOppId) {
				if (null != oppId) { //if we came from updateitem its mean we got command delete
					selectArray = row.getElementsByTagName("SELECT");
					elSel = selectArray[0];
					elSel.options[0].selected = "1";
					row.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';
				} else { //if user select from the filter
					return row;
				}
				break;
			}
		} else {
			break;
		}
		i = i + 1;
	} while (true);
}

//open the chart under the row into iframe with id = chart<opp id) example chart3
function openChart(image, row) {
	var trInfo;
	if (row != null) {
		var rowTd = getChildOfType(row, "TD", 1);
		var rowTable = getChildOfType(rowTd, "TABLE", 1);
		var rowTbody = getChildOfType(rowTable, "TBODY", 1);
		trInfo = getChildOfType(rowTbody, "TR", 1);
		var tdName = getChildOfType(trInfo, "TD", 1);
		image = getChildOfType(tdName, "IMG", 1);
	} else {
		trInfo = image.parentNode.parentNode;
	}
	var oppState = getOppValue(trInfo, 14);
	if (oppState > OPPORTUNITY_STATE_CREATED && oppState <= OPPORTUNITY_STATE_DONE) { //only in this states need to open the chart
		var marketId = getOppValue(trInfo, 16);
		var oppId = getOppValue(trInfo, 6);
		var tbody = trInfo.parentNode;
		var trChart = getChildOfType(tbody, "TR", 2);
		var tdChart = getChildOfType(trChart, "TD", 1);
		var iframeChart = getChildOfType(tdChart, "IFRAME", 1);
		var atts = trInfo.attributes;
		var cls = atts.getNamedItem("class");
		if (iframeChart == null) {
			tdChart.innerHTML = "<iframe id='chart" + oppId + "' src='" + context_path + "/jsp/chartPopup.jsf?marketId=" + marketId + "&oppId=" + oppId + "&command=" + oppState + "' width='490' height='300' frameborder='0'></iframe>";
			image.src = context_path + "/images/chart_open.gif";
			if (oppState >= OPPORTUNITY_STATE_CLOSING_1_MIN) {
				cls.value = "invest_close_without_border";
			} else {
				cls.value = "bottom_border_white";
			}
			trChart.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';
		} else {
			tdChart.innerHTML = "";
			image.src = context_path + "/images/chart_closed.gif";
			if (oppState >= OPPORTUNITY_STATE_CLOSING_1_MIN) {
				cls.value = "invest_close";
			} else {
				cls.value = "bottom_border";
			}
			trChart.style.display = "none";
			fixSlipPosition();
		}
	}
}


function engineStatusChange(chngStatus) {
	log("ENGINE state changed " + chngStatus);
}

function updateOpenMarkets(updateInfo) {
	var timeStamp = updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_AO_TS);

	// handle market states
	if (updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_AO_STATES) != null) {
		log("market states changed: " + updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_AO_STATES));
		var ms = new Array(0);
		var aoStates = updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_AO_STATES);
		if (null != aoStates) {
			ms = updateInfo.getChangedFieldValue(SUBSCR_FLD_ET_AO_STATES).split("|");
		}

		if (timeStamp > marketsTimestamp) {
			log("market states updated.");
			if (ms.length > 0 && ms[0].length > 0) {
				var newMarketStates = new Array(ms.length);
				for (i = 0; i < ms.length; i++) {
					var cms = new Array(2);
					cms[0] = ms[i];
					cms[1] = 1;
					newMarketStates[i] = cms;
				}
				marketStates = newMarketStates;
			} else {
				marketStates = new Array(0);
			}

			marketsTimestamp = timeStamp;
		}
	}
}

function getMarketName(marketId) {
	if (marketsDisplayName[marketId]) {
		return marketsDisplayName[marketId];
	}
	return "";
}

