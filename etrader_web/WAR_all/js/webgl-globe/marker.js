createMarker = function() {
	//marker container
	var marker = document.createElement('div');
	marker.className = 'marker-container';
	marker.id = 'main-marker-container';
	var markerIn = document.createElement('div');
	markerIn.className = 'marker';
	marker.appendChild(markerIn);

	var info = document.createElement('div');
	info.className = 'INFORMATIONAL-BOX';
	markerIn.appendChild(info);
    var siteList = document.createElement('span');
    siteList.className = 'site-list';
    info.appendChild(siteList);
    //investment info
	var investment_info = document.createElement('div');
	investment_info.id = "investment_info";
	siteList.appendChild(investment_info);
	var city_country = document.createElement('div');
	city_country.id = "city_country";
	siteList.appendChild(city_country);
	//point to the country
	var triangle_img = document.createElement('div');
	triangle_img.id = "triangle_img";
	markerIn.appendChild(triangle_img);
	return marker;
};