/**********aoCookie.js*************/
var scriptNamAoCookie = "aoCookie";
function readAOCookieValue(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0) ==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) {
		var res =  c.substring(nameEQ.length,c.length);
		res = res.replace('"','');
		return res.replace('"','');
		}
	}
	return null;
}

function createAOCookie(name,value,days, aoCookieDomain) {
	if (days!=-1) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/;domain="+aoCookieDomain;
}

function setAOCookieValue(paramName, paramValue, aoCookieDomain){
	var aoValue = "";
	var isSet = false;
	try{
		var aoCookie = readAOCookieValue('ao');
		var newParam = paramName + '|' + paramValue + 'V3L';
		if(aoCookie == null){
			aoValue = newParam;
			isSet = true;			
		} else {
		 var parts = aoCookie.split("V3L");
		 for (var i = 0; i < parts.length; i++) {
		 		var paramParts = parts[i].split('|');
		 		if (paramParts[0] == paramName) {
		 			aoValue = aoValue + paramValue;
		 			isSet = true;
		 		} else if (paramParts[0].length !=0 ){
		 			aoValue = aoValue +  parts[i] + "V3L";
		 		}
			}			
		}
		if(!isSet){
			aoValue = aoValue + newParam;
		}
		createAOCookie("ao",aoValue, 16910, aoCookieDomain);
	} catch (exsp){
		createAOCookie("ao","", 0 );
	}
}

function getAOCookieValue(paramName){
	var aoValue = "";
	var aoCookie = readAOCookieValue('ao');
	try{
		if(aoCookie == null){
			return aoValue;			
		} else {
		 var parts = aoCookie.split("V3L");
		 for (var i = 0; i < parts.length; i++) {
		 		var paramParts = parts[i].split('|');
		 		if (paramParts[0] == paramName) {
		 			aoValue =paramParts[1];
		 		}
			}			
		}
	} catch (exsp){
		createAOCookie("ao","", 0 );
	}	
	return aoValue;
}
