/*********** mandQuest.js ************/

function updateMandQuestValues() {
	var dataa = "numUserAnswers=" + questionsIdArr.length;
	var radios;
	var hasAnswer;
	for(var i = 0; i < questionsIdArr.length; i++) {
		if (i == 4) {
			dataa += "&userAnswer" + i + "Id=" + questionsIdArr[i][1];
			dataa += "&answer" + i + "Id=" + originQuestionDefaultAnswer;
			continue;
		} 
		if( i == 6 ) {
			dataa += "&userAnswer" + i + "Id=" + questionsIdArr[i][1];
			var checkBox = document.getElementById("regMandQuestionnaireForm:question_" + questionsIdArr[i][0]);
			if (checkBox.checked) {
				dataa += "&answer" + i + "Id=" + question6answerTrue;
			} else {
				dataa += "&answer" + i + "Id=" + question6answerFalse;
			}
			continue;
		}
		
		radios = document.getElementsByName("regMandQuestionnaireForm:question_" + questionsIdArr[i][0]);
		hasAnswer = false;
		for(var j = 0; j < radios.length; j++) {
			if (radios[j].checked) {
				hasAnswer = true;
				dataa += "&userAnswer" + i + "Id=" + questionsIdArr[i][1];
				dataa += "&answer" + i + "Id=" + radios[j].value;
				break;
			}
		}
		if (!hasAnswer) {
			dataa += "&userAnswer" + i + "Id=" + questionsIdArr[i][1];
		}
	}
	
	$.ajax({
	   type: "POST",
	   url: "ajax.jsf",
	   data: dataa,
	   success:  function(result) {
			   if (result != null && result == "error") {
				   location.reload();
			   }
   			}
	});
}

function showNotNowMessage() {
	var div = document.getElementById("not_now_message");
	div.style.display="block";
}

function showRegOptQuestMessage(link) {
	showNotNowMessage();
	setTimeout(function () {window.top.location.href=link}, 7000);
}
