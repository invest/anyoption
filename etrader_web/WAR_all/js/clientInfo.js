
function getCookieValue(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function createCookie(name,value,days) {
	if (days!=-1) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	var cookieDomain = document.domain;
	document.cookie = name+"="+value+expires+"; path=/;domain="+cookieDomain;
}

function isNotSendInfo(){
	var res = false;
	var aoCookie = getCookieValue('ao');
	try{
		if(aoCookie.indexOf('ci|y') != -1){
			res = false;
		}
	} catch (exsp){
		res = true;
	}
	return res;
}

function setAOCookieCI(){
	var prValue = getCookieValue('ao');
	var cookieValue;
	if(prValue != null){
		cookieValue = getCookieValue('ao') + 'ci|yV3L';
	} else {
			cookieValue = 'ci|yV3L';
	}	 
	createCookie('ao', cookieValue, 16910);
}

function callEtsCI(userAgent, userResolution, platform, etsCookie) {
	var iframe = document.createElement("iframe");
	iframe.src=etsserv + "info" + "?useragent=" + userAgent + "&userresolution=" + userResolution + "&mid=" + etsCookie + "&platform=" + platform;
	iframe.id="etsInfo_iframe";
	iframe.name="etsInfo_iframe";
	iframe.title="etsInfo_iframe";
	iframe.style.display="none";
	document.body.appendChild(iframe);
}

function getClientInfo(){
	try{
		if (isNotSendInfo()){
			var userAgent = navigator.userAgent;
			var userResolution = screen.width + "x" + screen.height;
			var platform = navigator.platform;
			callEtsCI(userAgent, userResolution, platform, getCookieValue('ets'));
			setAOCookieCI();
		}
	} catch(err) {
	
	}
}

getClientInfo();