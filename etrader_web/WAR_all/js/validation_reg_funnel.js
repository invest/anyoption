var CARD_TYPE_AMEX = '5';
var CARD_PASS_LENGTH_AMEX = 4;
var CARD_PASS_LENGTH = 3;

var SKIN_ETRADER = 1;

var translatedTxt = {
	"error.cvv":"error.cvv", 
	"error.cvvLengthAmex":"error.cvvLengthAmex",
	"error.cvvLength":"error.cvvLength",
	"error.creditcard.num":"error.creditcard.num",
	"error.creditcard.expdateError":"error.creditcard.expdateError",
	"error.creditcard.diners.notsupported":"error.creditcard.diners.notsupported",
	"error.letters.only":"error.letters.only",
	"error.english.letters.only":"error.english.letters.only",
	"CMS.registerContent.text.register.letters.only":"CMS.registerContent.text.register.letters.only",
	"error.street":"error.street",
	"error.iban":"error.iban",
	"org.apache.myfaces.Equal.INVALID_detail":"org.apache.myfaces.Equal.INVALID_detail",
	"CMS.registerContent.text.error.englishandnumbers.only":"CMS.registerContent.text.error.englishandnumbers.only",
	"error.lettersandnumbers.only":"error.lettersandnumbers.only",
	"error.password.smallsize":"error.password.smallsize",
	"error.password.bigsize":"error.password.bigsize",
	"javax.faces.component.UIInput.REQUIRED_detail":"javax.faces.component.UIInput.REQUIRED_detail",
	"javax.faces.validator.LongRangeValidator.TYPE_detail":"javax.faces.validator.LongRangeValidator.TYPE_detail",
	"javax.faces.validator.LengthValidator.MAXIMUM_detail":"javax.faces.validator.LengthValidator.MAXIMUM_detail",
	"javax.faces.validator.LengthValidator.MINIMUM_detail":"javax.faces.validator.LengthValidator.MINIMUM_detail",
	"error.register.email.inuse":"error.register.email.inuse",
	"org.apache.myfaces.Email.INVALID_detail":"org.apache.myfaces.Email.INVALID_detail",
	"error.banner.contactme.email":"error.banner.contactme.email",
	"error.register.month":"error.register.month",
	"error.register.year":"error.register.year",
	"javax.faces.validator.RegexValidator.NOT_MATCHED":"javax.faces.validator.RegexValidator.NOT_MATCHED",
	"error.register.terms":"error.register.terms",
	"error.register.forbidden":"error.register.forbidden",
	"ukash.voucher.error" : "ukash.voucher.error",
	
	"no.such.limit" : "No limit for that currency",
	"error.creditcard.mindeposit" : "error.creditcard.mindeposit",
	"error.creditcard.maxdeposit" : "error.creditcard.maxdeposit",
	"error.creditcard.deposit.maxday" : "error.creditcard.deposit.maxday",
	"error.creditcard.deposit.maxmonth" : "error.creditcard.deposit.maxmonth",
	
	"error.idnum" : "error.idnum",
	"error.idnumLength" : "error.idnumLength",
	"error.register.useridnum.inuse" : "error.register.useridnum.inuse",
	
	"error.register.year" : "error.register.year",
	"error.register.month" : "error.register.month",
	"error.register.day" : "error.register.day",

	"general.validate.letters.only":/^['a-zA-Z ]+$/,
	"general.validate.street":/^['0-9 a-zA-Z. \" -]+$/,
	"general.validate.name":/^['a-zA-Z ]+$/
};

var REGEX_GENERAL_VALIDATE_LETTERS_ONLY = "general.validate.letters.only";
var REGEX_GENERAL_VALIDATE_STREET = "general.validate.street";
var REGEX_GENERAL_VALIDATE_NAME = "general.validate.name";

var MSG_ERROR_INCORRECT_CITY = "CMS.register_content.text.tooltip.incorrectCity";
var MSG_ERROR_CVV = "error.cvv";
var MSG_ERROR_CVVLENGTH_AMEX = "error.cvvLengthAmex";
var MSG_ERROR_CVVLENGTH = "error.cvvLength";
var MSG_ERROR_CREDITCARD_NUM = "error.creditcard.num";
var MSG_ERROR_CREDITCARD_EXPDATEERROR = "error.creditcard.expdateError";
var MSG_ERROR_CREDITCARD_DINERS_NOTSUPPORTED = "error.creditcard.diners.notsupported";
var MSG_ERROR_LETTERS_ONLY = "error.letters.only";
var MSG_ERROR_ENGLISH_LETTERS_ONLY = "error.english.letters.only";
var MSG_ERROR_REGISTER_LETTERS_ONLY = "CMS.registerContent.text.register.letters.only";
var MSG_ERROR_STREET = "error.street";
var MSG_ERROR_IBAN= "error.iban";
var MSG_RETYPE_PASS_NOT_EQUAL = "org.apache.myfaces.Equal.INVALID_detail";
var MSG_ERROR_ENGLISHANDNUMBERS_ONLY = "CMS.registerContent.text.error.englishandnumbers.only";
var MSG_ERROR_LETTERSANDNUMBERS_ONLY = "error.lettersandnumbers.only";
var MSG_ERROR_PASSWORD_SMALLSIZE = "error.password.smallsize";
var MSG_ERROR_PASSWORD_BIGSIZE = "error.password.bigsize";
var MSG_ERROR_MANDATORY_FIELD = "javax.faces.component.UIInput.REQUIRED_detail";
var MSG_ERROR_INVALID_NUMBER = "javax.faces.validator.LongRangeValidator.TYPE_detail";
var MSG_ERROR_MAX_LENGTH = "javax.faces.validator.LengthValidator.MAXIMUM_detail";
var MSG_ERROR_MIN_LENGTH = "javax.faces.validator.LengthValidator.MINIMUM_detail";
var MSG_ERROR_EMAIL_INUSE = "error.register.email.inuse";
var MSG_ERROR_INVALID_EMAIL = "org.apache.myfaces.Email.INVALID_detail";
var MSG_ERROR_INVALID_EMAIL_DOMAIN = "error.banner.contactme.email";
var MSG_ERROR_REGISTER_MONTH = "error.register.month";
var MSG_ERROR_REGISTER_YEAR = "error.register.year";
var MSG_ERROR_AMOUNT_INVALID_NUMBER = "javax.faces.validator.RegexValidator.NOT_MATCHED";
var MSG_ERROR_ACCEPT_TERMS = "error.register.terms";
var MSG_ERROR_UKASH_VAUCHER = "ukash.voucher.error";
var MSG_ERROR_ID_NUM = "error.idnum";
var MSG_ERROR_ID_NUM_LENGTH = "error.idnumLength";
var MSG_ERROR_REGISTER_USERIDNUM_INUSE = "error.register.useridnum.inuse";
var MSG_ERROR_BIRTH_YEAR = "error.register.year";
var MSG_ERROR_BIRTH_MONTH = "error.register.month";
var MSG_ERROR_BIRTH_DAY = "error.register.day";
var MSG_ERROR_REGISTER_FORBIDDEN = "error.register.forbidden";

var nameValidationPattern = translatedTxt[REGEX_GENERAL_VALIDATE_NAME];
var nameValidationPattern_for_replace = new RegExp((nameValidationPattern+'').replace(/\//g,'').replace('^[','[^').replace('+$',''),'g');
var streetValidationPattern = translatedTxt[REGEX_GENERAL_VALIDATE_STREET];
var lettersOnlyPattern = translatedTxt[REGEX_GENERAL_VALIDATE_LETTERS_ONLY];

var lettersAndNumbersPattern  = /^[0-9a-zA-Z]+$/;
var phoneRegEx  = /^\d{7,20}$/;
var phoneRegEx_for_replace  = /\D/g;
var emailValidationRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var numberRegEx = /^\d+$/;
var englishLettersOnlyRegEx = /^['a-zA-Z ]+$/;

var idPrefix = "funnel_";
if(global_deposit_prefix != ""){
	idPrefix = global_deposit_prefix;
}
var errorSignSuffix = "error_sign";
var errorTextSuffix = "error_txt";
var errorTextSuffix_in = "error_txt_in";
var errorOkSuffix = "error_ok";
var singleErrorTagId = "validation_error_txt";

var isShowSingleErrorText = false;
var isShowErrors = true;
var isAsync = true;

var errorMap = new LinkedHashMap();

if(typeof mobile_lp == 'undefined'){
	mobile_lp = false;
}

// -1 for infinity
var showMaxErrors = 1;
if(typeof register_page !== 'undefined' && register_page === true) {
	showMaxErrors = -1;
}

function g(id) {
	return document.getElementById(id);
}

function trim(str) {
    str = str.replace(/^\s+/, '');
    for (var i = str.length - 1; i >= 0; i--) {
        if (/\S/.test(str.charAt(i))) {
            str = str.substring(0, i + 1);
            break;
        }
    }
    return str;
}

function isEmpty(val) {
	return trim(val) === "";
}

function isNumber(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}
function isInt(n) {
	return !isNaN(parseInt(n)) && isFinite(n);
}

function getTagDefaultValue(tag) {
	return (tag.getAttribute('data-def') != undefined)?tag.getAttribute('data-def'):(tag.alt != '')?tag.alt:tag.defaultValue;
}

String.prototype.format = function() {
    var args = arguments;
    return this.replace(/\{(\d+)\}/g,
    					function() {
    						return args[arguments[1]];
    					});
};

function getErrorId(tag) {
	return ((tag.getAttribute('data-errors_id') != undefined) ? (idPrefix + tag.getAttribute('data-errors_id')) : tag.id);
}

function checkError(msg, tag) {
	var errorId = getErrorId(tag);
	var single_err_check = compare2dArray(single_errors_map,errorId);
	if(single_err_check[0]){
		var errSignTag = g(single_errors_map[single_err_check[1]][0] + "_error");
	}
	else{
		var errSignTag = g(errorId + "_error");
	}
	var errTxtTag = null;
	if (msg === null) {
		if(single_err_check[0]){
			single_errors_map_holder[single_err_check[1]][single_err_check[2]] = '1';
			errSignTag.className = check_funnel_error_map(single_err_check[1]);
		}
		else{
			errSignTag.className = "funnel_error_good";
		}
		if(tag.nodeName.toLowerCase() == 'select'){
			tag.parentNode.className = tag.parentNode.className.replace(/ error/g,'');
			tag.parentNode.className = tag.parentNode.className.replace(/ active/g,'');
			tag.parentNode.className = tag.parentNode.className.replace(/ correct/g,'');
			tag.parentNode.className += " correct";
		}else{
			tag.className = tag.className.replace(/ error/g,'');
			tag.className = tag.className.replace(/ active/g,'');
			tag.className = tag.className.replace(/ correct/g,'');
			tag.className += " correct";
		}
		if (errorMap.remove(tag.id) !== null) {
			if (!isShowSingleErrorText) {
				errTxtTag = g(errorId + "_" + errorTextSuffix);
				errTxtTag.innerHTML = "";
				errTxtTag.style.display = "none";
				errTxtTag = g(errorId + "_" + errorTextSuffix_in);
				errTxtTag.innerHTML = "";
				errTxtTag.style.display = "none";
			}
		}
	} else {
		errorMap.remove(tag.id);
		if(single_err_check[0]){
			single_errors_map_holder[single_err_check[1]][single_err_check[2]] = '2';
			errSignTag.className = check_funnel_error_map(single_err_check[1]);
		}
		else{
			errSignTag.className = "funnel_error_hide";
		}
		if(tag.nodeName.toLowerCase() == 'select'){
			tag.parentNode.className = tag.parentNode.className.replace(/ error/g,'');
			tag.parentNode.className = tag.parentNode.className.replace(/ active/g,'');
			tag.parentNode.className = tag.parentNode.className.replace(/ correct/g,'');
			tag.parentNode.className += " correct";
		}else{
			tag.className = tag.className.replace(/ error/g,'');
			tag.className = tag.className.replace(/ active/g,'');
			tag.className = tag.className.replace(/ correct/g,'');
			tag.className += " correct";
		}
		errorMap.put(tag.id, {"errorId":errorId, "msg":msg});
	}
	
	if(isShowErrors) {
		showErrors();
	}
}

function showErrors() {
	try{
		var errTxtTagBlocked = g("funnel_blocked_error_txt");
		errTxtTagBlocked.innerHTML = "";
	} catch(e) {
		//do nothing sometimes we dont have this msg so we will get null
	}
	try{
		if(typeof mobile_lp != 'undefined' && mobile_lp == true) {
			showMaxErrors = -1;
		}
				
		if (errorMap.size() > 0) {
			var keys = errorMap.keys();
			var errTxtTag = null;
			if (isShowSingleErrorText) {
				errTxtTag = g(singleErrorTagId);
				errTxtTag.innerHTML = errorMap.get(keys[errorMap.size() - 1]).msg;
				errTxtTag.style.display = "";
			}
			var id = null;
			var errSignTag = null;
			var errTxtTag_last = null;
			var el_last = null;
			for (var errIdx = errorMap.size() - 1; errIdx >= 0; errIdx--) {
				id = keys[errIdx];
				var single_err_check = compare2dArray(single_errors_map,errorMap.get(id).errorId);
				if(single_err_check[0]){
					errSignTag = g(single_errors_map[single_err_check[1]][0] + "_error");
				}
				else{
					errSignTag = g(errorMap.get(id).errorId + "_error");
				}
				
				if (showMaxErrors > 0 && (errorMap.size() - errIdx) > showMaxErrors) {
					if(single_err_check[0]){
						single_errors_map_holder[single_err_check[1]][single_err_check[2]] = '2';
						errSignTag.className = check_funnel_error_map(single_err_check[1]);
					}
					else{
						errSignTag.className = "funnel_error_hide";
					}
					var el = g(id);
					if(el.nodeName.toLowerCase() == 'select'){
						el.parentNode.className = el.parentNode.className.replace(/ error/g,'');
						el.parentNode.className = el.parentNode.className.replace(/ active/g,'');
						el.parentNode.className = el.parentNode.className.replace(/ correct/g,'');
						el.parentNode.className += " correct";
					}else{
						el.className = el.className.replace(/ error/g,'');
						el.className = el.className.replace(/ active/g,'');
						el.className = el.className.replace(/ correct/g,'');
						el.className += " correct";
					}
					if (!isShowSingleErrorText) {
						errTxtTag = g(errorMap.get(id).errorId + "_" + errorTextSuffix);
						errTxtTag.innerHTML = "";
						errTxtTag.style.display = "none";
						errTxtTag = g(errorMap.get(id).errorId + "_" + errorTextSuffix_in);
						errTxtTag.innerHTML = "";
						errTxtTag.style.display = "none";
					}
				} else {
					if(single_err_check[0]){
						single_errors_map_holder[single_err_check[1]][single_err_check[2]] = '0';
						errSignTag.className = check_funnel_error_map(single_err_check[1]);
					}
					else{
						errSignTag.className = "funnel_error_bad";
					}
					el_last = g(id);
					if(el_last.nodeName.toLowerCase() == 'select'){
						el_last.parentNode.className = el_last.parentNode.className.replace(/ correct/g,'');
						el_last.parentNode.className = el_last.parentNode.className.replace(/ error/g,'');
						el_last.parentNode.className += " error";
					}else{
						el_last.className = el_last.className.replace(/ error/g,'');
						el_last.className += " error";
					}
					el_last.className = el_last.className.replace(/ correct/g,'');
					if (!isShowSingleErrorText) {
						errTxtTag_last = g(errorMap.get(id).errorId + "_" + errorTextSuffix);
						errTxtTag_last.innerHTML = errorMap.get(id).msg;
						errTxtTag_last.style.display = "";
					
						errTxtTag = g(errorMap.get(id).errorId + "_" + errorTextSuffix_in);
						errTxtTag.innerHTML = errorMap.get(id).msg;
						errTxtTag.style.display = "";
					}
				}
			}
			//connect input and error msg with dotted line
			if (!isShowSingleErrorText) {
				if(errTxtTag_last != null){
					var funnel_error_line = g('funnel_error_line');
					if(typeof funnel_error_line != "undefined" && funnel_error_line != null){
						var top = find_top_level(el_last,'data-funnel_row').offsetTop+(el_last.offsetHeight/2);
						var bottom = errTxtTag_last.offsetTop+(errTxtTag_last.offsetHeight/2);
						funnel_error_line.style.top = top+'px';
						funnel_error_line.style.display = 'block';
						funnel_error_line.style.height = bottom-top+"px";
					}
				}
			}
		}
		else{
			try{
				g('funnel_error_line').style.display = 'none';
			}catch(e){}
		}
	}catch(e){
		logIt({'type':3,'msg':e});
	}
}
function find_top_level(el,data_label){
	while(el){
		if(el.getAttribute(data_label) == 't'){
			return el;
		}
		else{
			el = el.parentNode;
		}
	}
	return false;
}
function showErrorsBlocked(marked){
	var errDiv = g("funnel_error_list").getElementsByTagName("span");	
	for( var i = 0; i<errDiv.length; i++ ){
		errDiv[i].innerHTML="";
	}       
	if(marked){
		var errSignTag = g("funnel_drop_h");
		errSignTag.className = "funnel_drop_h error";
	}	

	errTxtTag = g("funnel_blocked_error_txt");
	errTxtTag.innerHTML = translatedTxt[MSG_ERROR_REGISTER_FORBIDDEN];
	errTxtTag.style.display = "";
	
	var btn_open_account = g('btn_open_account');
	btn_open_account.className = btn_open_account.className.replace(" disabled","");
	btn_open_account.onclick = function(){funnel_submit();return false;}
}
function checkEmpty(tag, msgKey) {
	var val = tag.value;
	var def = getTagDefaultValue(tag);
	if (isEmpty(val)||(val === def)) {
		if (!isShowErrors) {
			checkError(translatedTxt[msgKey], tag);
		} else {
			errorMap.remove(tag.id);
			var errorId = getErrorId(tag);
			var single_err_check = compare2dArray(single_errors_map,errorId);
			if(single_err_check[0]){
				var errSignTag = g(single_errors_map[single_err_check[1]][0] + "_error");
			}
			else{
				var errSignTag = g(errorId + "_error");
			}
			if(single_err_check[0]){
				single_errors_map_holder[single_err_check[1]][single_err_check[2]] = '2';
				errSignTag.className = check_funnel_error_map(single_err_check[1]);
			}
			else{
				errSignTag.className = "funnel_error_hide";
			}
			
			var errTxtTag = g(errorId + "_" + errorTextSuffix);
			errTxtTag.innerHTML = "";
			errTxtTag.style.display = "none";
			
			errTxtTag = g(errorId + "_" + errorTextSuffix_in);
			errTxtTag.innerHTML = "";
			errTxtTag.style.display = "none";
			
			if(tag.nodeName.toLowerCase() == 'select'){
				tag.parentNode.className = tag.parentNode.className.replace(/ error/g,'');
				tag.parentNode.className = tag.parentNode.className.replace(/ active/g,'');
			}else{
				tag.className = tag.className.replace(/ error/g,'');
				tag.className = tag.className.replace(/ active/g,'');
			}
		}
		return true;
	}
	
	return false;
}

function validateEmail(tag) {
	if (tag == null) {
		tag = g(idPrefix + "email");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	var val = tag.value;
	if (emailValidationRegEx.test(val)) {
		validateEmailService(tag);
		/* !!! WARNING !!! Method Exit point! */
		return;
	} else {
		msg = translatedTxt[MSG_ERROR_INVALID_EMAIL];
	}
	checkError(msg, tag);
}

function validateEmailService(emailTag) {
	xmlHttp = getXMLHttp();
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState == 4) {
			var msg = null;
			if (xmlHttp.status === 200) {
				var result = JSON.parse(xmlHttp.responseText).returnCode;
				switch (result) {
					case 0:
						// OK
						break;
					case 1:
						msg = translatedTxt[MSG_ERROR_INVALID_EMAIL];
						break;
					case 2:
						msg = translatedTxt[MSG_ERROR_INVALID_EMAIL_DOMAIN];
						break;
					case 3:
						msg = translatedTxt[MSG_ERROR_EMAIL_INUSE].replace('{0}', passwordPage);
						break;
				}
			} else {
				msg = translatedTxt[MSG_ERROR_INVALID_EMAIL];
			}
			checkError(msg, emailTag);
		}
	}
	xmlHttp.open("POST", context_path_json+"/jsonService/AnyoptionService/validateEmail", isAsync);
	xmlHttp.setRequestHeader("Content-Type", "json");
	xmlHttp.setRequestHeader("Accept-Language","en-US,en;q=0.5");
	xmlHttp.send(JSON.stringify({"email" : emailTag.value}));
}

function validateFirstName(tag) {
	if (tag == null) {
		tag = g(idPrefix + "firstName");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	var val = tag.value;
	if (!nameValidationPattern.test(val) || val == '') {
		msg = translatedTxt[MSG_ERROR_LETTERS_ONLY].format("");
	}
	checkError(msg, tag);
}

function validateLastName(tag) {
	if (tag == null) {
		tag = g(idPrefix + "lastName");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	var val = tag.value;
	if (!nameValidationPattern.test(val)) {
		msg = translatedTxt[MSG_ERROR_LETTERS_ONLY].format("");
	}
	checkError(msg, tag);
}

function validateEtraderPhoneCode() {
	var msg = null;
	var tag = g(idPrefix + "drop_h");
	var val =  g(idPrefix + "phonePrefix").value;
	
	if (val == null || trim(val) === "" || !isNumber(val)) {
		msg = translatedTxt[MSG_ERROR_MANDATORY_FIELD];
	}
	
	checkError(msg, tag);
}

function validatePhoneNumberTag(tag) {
	if (tag == null) {
		tag = g(idPrefix + "mobilePhone");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	var val = tag.value;
	var defVal = getTagDefaultValue(tag);
	if (numberRegEx.test(val)) {
		if (val.length < 7) {
			msg = translatedTxt[MSG_ERROR_MIN_LENGTH].format("7", defVal);
		} else if (skinId === SKIN_ETRADER) {
			if (val.length > 7) {
				msg = translatedTxt[MSG_ERROR_MAX_LENGTH].format("7", defVal);
			}
		} else if (val.length > 20) {
			msg = translatedTxt[MSG_ERROR_MAX_LENGTH].format("20", defVal);
		}
	} else {
		msg = translatedTxt[MSG_ERROR_INVALID_NUMBER];
	}
	
	checkError(msg, tag);
}

function validatePassword(tag) {
	if (tag == null) {
		tag = g(idPrefix + "password");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	var val = tag.value;
	if (lettersAndNumbersPattern.test(val)) {
		if (val.length < 6) {
			msg = translatedTxt[MSG_ERROR_PASSWORD_SMALLSIZE];
		} else if(val.length > 9) {
			msg = translatedTxt[MSG_ERROR_PASSWORD_BIGSIZE];
		}
	} else {
		msg = translatedTxt[MSG_ERROR_LETTERSANDNUMBERS_ONLY];
	}
	checkError(msg, tag);
}

function validatePasswordConfirm(tag) {
	if (tag == null) {
		tag = g(idPrefix + "passwordConfirm");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	if (g(idPrefix + "password").value !== tag.value) {
		msg = translatedTxt[MSG_RETYPE_PASS_NOT_EQUAL];
	}
	checkError(msg, tag);
}

function validateStreet(tag) {
	if (tag == null) {
		tag = g(idPrefix + "street");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	if(!streetValidationPattern.test(tag.value)) {
		msg = translatedTxt[MSG_ERROR_STREET];
	}
	checkError(msg, tag);
}

function validateCityName(tag) {
	if (tag == null) {
		tag = g(idPrefix + "cityNameAO");
	}
	
	if (checkEmpty(tag, MSG_ERROR_REGISTER_LETTERS_ONLY)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	if (!lettersOnlyPattern.test(tag.value)) {
		msg = translatedTxt[MSG_ERROR_LETTERS_ONLY];
	}
	checkError(msg, tag);
}

function validateCityNameET(tag) {
	if (tag == null) {
		tag = g(idPrefix + "cityNameAO");
	}
	
	if (checkEmpty(tag, MSG_ERROR_REGISTER_LETTERS_ONLY)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	if(citiesArr.indexOf(tag.value) == -1) {
		msg = translatedTxt[MSG_ERROR_INCORRECT_CITY];
	}
	checkError(msg, tag);
}

function validateCCType(tag) {
	if (tag == null) {
		tag = g(idPrefix + "typeId");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	checkError(null, tag);
}

function validateCCSelect(tag) {
	if (tag == null) {
		tag = g(idPrefix + "ccNum");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	checkError(null, tag);
}

function validateCCNumber(tag) {
	if (tag == null) {
		tag = g(idPrefix + "ccNum");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	validateCCNumberService(tag);
}

function validateCCNumberService(ccNumTag) {
	xmlHttp = getXMLHttp();
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState == 4) {
			var msg = null;
			if (xmlHttp.status === 200) {
				var result = JSON.parse(xmlHttp.responseText);
				if (!result.valid) {
					if (result.diners || result.amex) {
						msg = translatedTxt[MSG_ERROR_CREDITCARD_DINERS_NOTSUPPORTED];
					} else {
						msg = translatedTxt[MSG_ERROR_CREDITCARD_NUM];
					}
				}
			} else {
				msg = translatedTxt[MSG_ERROR_CREDITCARD_NUM];
			}
			checkError(msg, ccNumTag);
		}
	}
	xmlHttp.open("POST", context_path_json+"/jsonService/AnyoptionService/validateCC", isAsync);
	xmlHttp.setRequestHeader("Content-Type", "json");
	xmlHttp.setRequestHeader("Accept-Language","en-US,en;q=0.5");
	xmlHttp.send(JSON.stringify({"ccNumber" : ccNumTag.value, "skinId" : skinId}));
}

// CVV code
function validateCCPass(tag) {
	if (tag == null) {
		tag = g(idPrefix + "ccPass");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	var val = tag.value;
	if (numberRegEx.test(val)) {
		var ccTypeTag = g(idPrefix + "typeId");
		if (ccTypeTag != null) {
			if (typeof ccTypeTag !== "undefined") {
				if (!isEmpty(ccTypeTag.value)) {
					if (val.length !== CARD_PASS_LENGTH) {
						msg = translatedTxt[MSG_ERROR_CVVLENGTH];
					}
				} else {
					// do nothing
				}
			} else {
				alert("CC type not defined");
			}
		} else {
			if (val.length !== CARD_PASS_LENGTH) {
				msg = translatedTxt[MSG_ERROR_CVVLENGTH];
			}
		}
	} else {
		// not a number
		msg = translatedTxt[MSG_ERROR_CVV];
	}
	checkError(msg, tag);
}

function validateExpMonth(tag) {
	if (tag == null) {
		tag = g(idPrefix + "expMonth");
	}
	
	if (checkEmpty(tag, MSG_ERROR_REGISTER_MONTH)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	checkError(null, tag);
}

function validateExpYear(tag) {
	if (tag == null) {
		tag = g(idPrefix + "expYear");
	}
	
	if (checkEmpty(tag, MSG_ERROR_REGISTER_YEAR)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	checkError(null, tag);
}

function validateExpDate(monthTag, yearTag){
	if((monthTag != null) && (yearTag != null)){
		var msg = null;
		var currentMonth = new Date().getMonth() + 1;
		var currentYear = parseInt((new Date().getFullYear() + "").slice(2));
		if(((parseInt(monthTag.value) < currentMonth) && (parseInt(yearTag.value) == currentYear)) || (parseInt(yearTag.value) < currentYear)){
			msg = translatedTxt[MSG_ERROR_CREDITCARD_EXPDATEERROR];
		}
		checkError(msg, monthTag);
		checkError(msg, yearTag);
	}
}

function validateHolderName(tag) {
	if (tag == null) {		
		tag = g(idPrefix + "holderName");
		if(skinId == 1){
			tag = g(idPrefix + "holderName1");
		}
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	var val = tag.value;
	if (skinId === SKIN_ETRADER) {
		if (!lettersOnlyPattern.test(val)) {
			msg = translatedTxt[MSG_ERROR_LETTERS_ONLY];
		}
	} else {
		if (!lettersOnlyPattern.test(val)) {
			msg = translatedTxt[MSG_ERROR_ENGLISH_LETTERS_ONLY];
		}
	}
	checkError(msg, tag);
}

function validateZipCode(tag) {
	if (tag == null) {
		tag = g(idPrefix + "zipCode");
	}
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	checkError(null, tag);
}

function validateHolderIdNum(tag) {
	if (tag == null) {
		tag = g(idPrefix + "holderIdNum");
	}
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}	
	var msg = null;
	var val = tag.value;
	if(!numberRegEx.test(val)) {
		msg = translatedTxt[MSG_ERROR_ID_NUM];
	} else {
		if(val < 1000) {
			msg = translatedTxt[MSG_ERROR_ID_NUM];
		} else {
			if(val.length < 9) {
				msg = translatedTxt[MSG_ERROR_ID_NUM_LENGTH];
			} else {
				var idNum1 = val.substring(0, 1) * 1;
				var idNum2 = val.substring(1, 2) * 2;
				var idNum3 = val.substring(2, 3) * 1;
				var idNum4 = val.substring(3, 4) * 2;
				var idNum5 = val.substring(4, 5) * 1;
				var idNum6 = val.substring(5, 6) * 2;
				var idNum7 = val.substring(6, 7) * 1;
				var idNum8 = val.substring(7, 8) * 2;
				var idNum9 = val.substring(8, 9) * 1;				
				if (idNum1 > 9) {
					idNum1 = (idNum1 % 10) + 1;
				}
				if (idNum2 > 9) {
					idNum2 = (idNum2 % 10) + 1;
				}
				if (idNum3 > 9) {
					idNum3 = (idNum3 % 10) + 1;
				}
				if (idNum4 > 9) {
					idNum4 = (idNum4 % 10) + 1;
				}
				if (idNum5 > 9) {
					idNum5 = (idNum5 % 10) + 1;
				}
				if (idNum6 > 9) {
					idNum6 = (idNum6 % 10) + 1;
				}
				if (idNum7 > 9) {
					idNum7 = (idNum7 % 10) + 1;
				}
				if (idNum8 > 9) {
					idNum8 = (idNum8 % 10) + 1;
				}
				if (idNum9 > 9) {
					idNum9 = (idNum9 % 10) + 1;
				}				
				var sumval = idNum1 + idNum2 + idNum3 + idNum4 + idNum5 + idNum6 + idNum7 + idNum8 + idNum9;
				sumval = sumval % 10;
				if(sumval > 0) {
					msg = translatedTxt[MSG_ERROR_ID_NUM];
				}
			}
		}
	}
	if((msg == null) && ((typeof inUseHolderIdNum != "undefined") && inUseHolderIdNum && (val == inUseHolderIdNum))){
		msg = translatedTxt[MSG_ERROR_REGISTER_USERIDNUM_INUSE];
		g(getErrorId(tag) + "Error").style.display = "none";
	}
	checkError(msg, tag);
}

function validateDepositAmount(tag) {
	if (tag == null) {
		tag = g(idPrefix + "deposit");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	if (!isNumber(tag.value)) {
		msg = translatedTxt[MSG_ERROR_AMOUNT_INVALID_NUMBER];
	}
	checkError(msg, tag);
}

function validateCheckbox() {
	var terms = g(idPrefix + "terms");
	
	var msg = null;
	
	if (!terms.checked) {
		msg = translatedTxt[MSG_ERROR_ACCEPT_TERMS];
	}
	checkError(msg, terms);
}

function validateDirectAccNum(tag) {
	if (tag == null) {
		tag = g(idPrefix + "accNum");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	if (!numberRegEx.test(tag.value)) {
		msg = translatedTxt[MSG_ERROR_INVALID_NUMBER];
	} 
	checkError(msg, tag);
}

function validateIBAN(tag) {
	if (tag == null) {
		tag = g(idPrefix + "accNum");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	var val = tag.value;
	if (!lettersAndNumbersPattern.test(val)) {
		msg = translatedTxt[MSG_ERROR_IBAN];
	} 
	checkError(msg, tag);
}

function validateSortCode(tag) {
	if (tag == null) {
		tag = g(idPrefix + "sortCode");
	}
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	checkError(null, tag);
}

function validateBenefiName(tag) {
	if (tag == null) {
		tag = g(idPrefix + "benefiName");
	}
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	checkError(null, tag);
}

function validateSenderName(tag) {
	if (tag == null) {
		tag = g(idPrefix + "senderName");
	}
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	checkError(null, tag);
}

function validateBaroLandLinePhone(tag) {
	if (tag == null) {
		tag = g(idPrefix + "landLinePhone");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	var val = tag.value;
	var defVal = getTagDefaultValue(tag);
	if (numberRegEx.test(val)) {
		if (val.length < 7) {
			msg = translatedTxt[MSG_ERROR_MIN_LENGTH].format("7", defVal);
		} else if (val.length > 8) {
			msg = translatedTxt[MSG_ERROR_MAX_LENGTH].format("8", defVal);
		}
	} else {
		msg = translatedTxt[MSG_ERROR_INVALID_NUMBER];
	}
	checkError(msg, tag);
}

function validateUkashVoucherNumber(tag) {
	if (tag == null) {
		tag = g(idPrefix + "voucherNumber");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	var val = tag.value;
	if (!isNumber(val)) { // double value
		msg = translatedTxt[MSG_ERROR_INVALID_NUMBER];
	} else if (val.length !== 19) {
		msg = translatedTxt[MSG_ERROR_UKASH_VAUCHER];
	}
	checkError(msg, tag);
}
function validateBirthYear(tag) {
	if (tag == null) {
		tag = g(idPrefix + "birthYear");
	}
	if (checkEmpty(tag, MSG_ERROR_BIRTH_YEAR)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	var msg = null;
	checkError(msg, tag);
}
function validateBirthMonth(tag) {
	if (tag == null) {
		tag = g(idPrefix + "birthMonth");
	}
	if (checkEmpty(tag, MSG_ERROR_BIRTH_MONTH)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	var msg = null;
	checkError(msg, tag);
}
function validateBirthDay(tag) {
	if (tag == null) {
		tag = g(idPrefix + "birthDay");
	}
	if (checkEmpty(tag, MSG_ERROR_BIRTH_DAY)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	var msg = null;
	checkError(msg, tag);
}
function passGoodKeys(keyId){
	var allowedKeys = [
		8,//backspace
		9,//tab
		13,//enter
		16,//shift
		17,//ctr
		18,//alt
		33,//pageUp
		34,//pageDown
		35,//end
		36,//home
		37,//left arrow
		38,//up arrow
		39,//right arrow
		40,//down arrow
		45,//insert
		46,//delete
	]
	if(allowedKeys.indexOf(keyId) === -1){
		return false;
	}
	return true;
}
function limitLetters(event,position){
	var field = event.target || event.srcElement;
	var tooltip = g(field.getAttribute('data-letters_tooltip'));
	if(nameValidationPattern.test(field.value)){
		tooltip.style.display = "none";
	}
	else{
		if(!passGoodKeys(event.keyCode)){
			tooltip.style.display = "block";
			tooltip.className = "funnel_tooltip_h"+position;
			tooltip.style.top = -(tooltip.offsetHeight/2)+(field.offsetHeight/2)+'px';
			field.value = field.value.replace(nameValidationPattern_for_replace,'');
			// event.preventDefault();
	    }
	}
}
function limitNumbers(event){
	var field = event.target || event.srcElement;
	var tooltip = g(field.getAttribute('data-letters_tooltip'));
	var tooltip_in1 = g(field.getAttribute('data-letters_tooltip')+"_1");
	var tooltip_in2 = g(field.getAttribute('data-letters_tooltip')+"_2");
	if(numberRegEx.test(field.value)){
		tooltip_in1.style.display = "block";
		tooltip_in2.style.display = "none";
		tooltip.style.top = -(tooltip.offsetHeight/2)+(field.offsetHeight/2)+'px';
	}
	else{
		if(!passGoodKeys(event.keyCode)){
			tooltip_in1.style.display = "none";
			tooltip_in2.style.display = "block";
			tooltip.style.top = -(tooltip.offsetHeight/2)+(field.offsetHeight/2)+'px';
			field.value = field.value.replace(phoneRegEx_for_replace,'');
			// event.preventDefault();
	    }	
	}
}
function hide_tooltip(field){
	var tooltip = g(field.getAttribute('data-letters_tooltip'));
	tooltip.style.display = "none";
}

function limitNumbersDepositField(event, ttip) {
	var field = event.target || event.srcElement;
	var tooltip = g(ttip);
	if(!numberRegEx.test(field.value)){
		tooltip.style.display = "block";
		field.value = field.value.replace(phoneRegEx_for_replace,'');
		tooltip.style.top = -(tooltip.offsetHeight/2)+(field.offsetHeight/2)+'px';
	}
	else{
		tooltip.style.display = "none";
	}
}

function limitNumbersDepositFieldNoTooltip(event) {
	var field = event.target || event.srcElement;
	if(!numberRegEx.test(field.value)){
		field.value = field.value.replace(phoneRegEx_for_replace,'');
	}
}
function hideTooltipDepositField(id){
	var tooltip = g(id);
	tooltip.style.display = "none";
}
if(typeof funnel_locale == 'undefined'){
	var funnel_locale = '';
}
addScriptToHead('regForm_map.js?s='+skinId + "&locale=" + funnel_locale,'initRegForm_map&version=cvsTimeStamp');