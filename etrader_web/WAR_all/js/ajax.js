/*file: ajax.js*/
function getXMLHttp() {
	var xmlHttp;
	try {
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	} catch (e) {
		// Internet Explorer
		try {
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
				alert("Your browser does not support AJAX!");
				return false;
			}
		}
	}
	return xmlHttp;
}
function AJAXInteraction(url, callback) {
	var req = init();
	req.onreadystatechange = processRequest;

	function init() {
		if (window.XMLHttpRequest) {
			return new XMLHttpRequest();
		} else if (window.ActiveXObject) {
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
	}

	function processRequest () {
		// readyState of 4 signifies request is complete
		if (req.readyState == 4) {
			// status of 200 signifies sucessful HTTP call
			if (req.status == 200) {
				if (callback) {callback(req.responseXML);}
			}
		}
	}

	this.doGet = function() {
		// make a HTTP GET request to the URL asynchronously
		req.open("GET", url, true);
		req.send(null);
	}
}
function AJAXInteractionTxt(url, callback) {
  	var req = init();
    req.onreadystatechange = processRequest;

    function init() {
   		if (window.XMLHttpRequest) {
        	return new XMLHttpRequest();
        } else if (window.ActiveXObject) {
        	return new ActiveXObject("Microsoft.XMLHTTP");
        }
    }

    function processRequest () {
		// readyState of 4 signifies request is complete
		if (req.readyState == 4) {
			// status of 200 signifies sucessful HTTP call
			if (req.status == 200) {
				if (callback) {callback(req.responseText);}
			}
		}
    }

    this.doGet = function() {
		// make a HTTP GET request to the URL asynchronously
		req.open("GET", url, true);
		req.send(null);
   	}
}
function aJaxCall_fn(params){//url,params to post,type,callback,xml,async
	var type = (typeof params.type != 'undefined')?params.type:'GET';
	var post_params = (typeof params.params != 'undefined')?params.params:null;
	var async = (typeof params.async != 'undefined')?params.async:true;
	var xml = (typeof params.xml != 'undefined')?params.xml:false;
	
	var req = init();
	req.onreadystatechange = processRequest;

	function init() {
		if (window.XMLHttpRequest) {
			return new XMLHttpRequest();
		} else if (window.ActiveXObject) {
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
	}

    function processRequest () {
		// readyState of 4 signifies request is complete
		if (req.readyState == 4) {
			// status of 200 signifies sucessful HTTP call
			if (req.status == 200) {
				if (params.callback) {
					if(xml){
						params.callback(req.responseXML);
					}else{
						params.callback(req.responseText);
					}
				}
			}
		}
    }

    this.doGet = function() {
		req.open(type, params.url, async);//type[post,get]
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(post_params);
   	}
}

function aJaxCall_json(params){//url,params to post,type,callback,xml,async
	var type = (typeof params.type != 'undefined')?params.type:'GET';
	var post_params = (typeof params.params != 'undefined')?params.params:null;
	var async = (typeof params.async != 'undefined')?params.async:true;
	var xml = (typeof params.xml != 'undefined')?params.xml:false;
	
	var req = init();
	req.onreadystatechange = processRequest;

	function init() {
		if (window.XMLHttpRequest) {
			return new XMLHttpRequest();
		} else if (window.ActiveXObject) {
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
	}

    function processRequest () {
		// readyState of 4 signifies request is complete
		if (req.readyState == 4) {
			// status of 200 signifies sucessful HTTP call
			if (req.status == 200) {
				if (params.callback) {
					if(xml){
						params.callback(req.responseXML);
					}else{
						params.callback(req.responseText);
					}
				}
			}
		}
    }

    this.doGet = function() {
		req.open(type, params.url, async);//type[post,get]
		req.setRequestHeader("Content-Type", "application/json");
		req.send(post_params);
   	}
}