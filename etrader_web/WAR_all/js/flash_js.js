/*file: flash_js.js*/
//here should be all js for creating flash objects
function anyClockZh(pagescheme,context,milis,offset,dst,skinPath,clockHeight){
	document.write('<object  classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="'+pagescheme+'://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0"  width="150" height="'+clockHeight+'" id="testV2" align="middle">');
	document.write('<param name="movie" value="'+context+'/images/'+skinPath +'/aoClockYear4digit.swf" />');
	document.write('<param name="flashvars" value="timeMs='+milis+'&amp;utc='+offset+'&amp;dst='+dst+'" />');
	document.write('<param name="quality" value="high" /><param name="bgcolor" value="#FFFFFF" />');
	document.write('<param name="wmode" value="transparent" />');
	document.write('<embed src="'+context+'/images/'+skinPath+'/aoClockYear4digit.swf" flashvars="timeMs='+milis+'&amp;utc='+offset+'&amp;dst='+dst+'" quality="high" bgcolor="#FFFFFF" style="padding-bottom: 1px;"  width="150" height="'+clockHeight+'" name="testV2" align="middle" wmode="transparent" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="'+pagescheme+'://www.macromedia.com/go/getflashplayer" />');
	document.write('</object>');
}
//here should be all the js function that are called from flash or are calling ac function in the flash
function getMovie(movieName){
	if (window.document[movieName]) {
		return window.document[movieName];
	}
	if (navigator.appName.indexOf('Microsoft Internet')==-1) {
		if (document.embeds && document.embeds[movieName]) {
			return document.embeds[movieName];
		}
	}
	else { // if (navigator.appName.indexOf("Microsoft Internet")!=-1)
		return document.getElementById(movieName);
	}
}

function reloadInvs() {
	getMovie('flashObject').reloadInvestments();
}
function removeInvestment(id) {
	return getMovie('flashObject').removeInvestment(id);
}
function checkEscape(e) {
	if (!e) {
		e = event;
	}
	if (e.keyCode == 27) {
		return false;
	}
}

var table0100;
function binary0100AddTable(marketId, opportunityTypeId) {
	if (lsClient && lsEngineReady) {
		var group0100 = ["bz_" + marketId + "_" + opportunityTypeId];
		var schema0100 = ["key", "command", "BZ_OFFER", "BZ_BID", "BZ_STATE", "BZ_CURRENT", "AO_TS", "BZ_SUSPENDED_MESSAGE","AO_OPP_STATE"];

		table0100 = new Subscription("COMMAND", group0100, schema0100);
		table0100.setDataAdapter("JMS_ADAPTER");
		table0100.setRequestedSnapshot("yes");
		table0100.setRequestedMaxFrequency(1.0);
		table0100.addListener({
	          onSubscription: function() {
	        	  logIt({'type':1,'msg':'SUBSCRIBED table0100'});
	          },
	          onUnsubscription: function() {
	            logIt({'type':1,'msg':'UNSUBSCRIBED table0100'});
	          }, 
	          onItemUpdate: function(updateObject) {
	        	  updateItem0100(updateObject.getItemPos(), updateObject, updateObject.getItemName());
	          }
	        });

		lsClient.subscribe(table0100);
		return true;
	} else {
		return false;
	}
}
function updateItem0100(itemPos, updateInfo, itemName) {
	getMovie('flashObject').binary0100OnItemUpdateIU(
		updateInfo.getValue(1),
		updateInfo.isValueChanged(5),
		updateInfo.getValue(5),
		updateInfo.getValue(8),
		updateInfo.getValue(6),
		updateInfo.getValue(7),
		updateInfo.getValue(3),
		updateInfo.getValue(4),
		updateInfo.getValue(9)
	);
}
function binary0100RemoveTable() {
	lsClient.unsubscribe(table0100);
}
function getFlashRequestPath() {
	var domain = window.top.location.href;
	if (domain.substr(0, 7) == "http://") {
		domain = domain.substr(7);
	}
	if (domain.substr(0, 8) == "https://") {
		domain = domain.substr(8);
	}
	if (domain.indexOf("/") != -1) {
		domain = domain.substr(0, domain.indexOf("/"));
	}
	var path = (forceHttps ? 'https' : 'http') + "://" + domain + context_path;
	var index = (path.indexOf("undefined") > -1) ? path.indexOf("undefined") : (path.indexOf("null") > -1) ? path.indexOf("null") : -1;

	if (index > -1) {
		path = path.substr(0, index);
	}
	return path;
}
function b0100_history_flash_select(MarketId,OpportunityId){
	getMovie('flashObject').changeMarketFromOutside(MarketId,OpportunityId);
}