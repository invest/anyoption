var userNameErrorMsg="";
function validateUserName() {
	var target = g("registerForm:userName");
	if(target.value.length>0){
		var url = "ajax.jsf?username=" + encodeURIComponent(target.value);
		var ajax = new AJAXInteraction(url, validateCallback);
		ajax.doGet();
	}
}
function trim(str) {
	return ltrim(rtrim(str));
}
function validateCallback(responseXML) {
   var msg = responseXML.getElementsByTagName("errormsg")[0].firstChild.nodeValue;
   var check = g("registerForm:userNameError");
   if (check != null) {
	  check.innerHTML = '';
   }
   var mdiv = g("userNameMessage");
   userNameErrorMsg = msg;

   // set the style on the div to invalid
   mdiv.className = "error_messages";
   mdiv.innerHTML = msg;

   if (msg.length<3) {
		mdiv.className = "left_align";
		mdiv.innerHTML='';
	}
}
/*Change the phone code by the value of the country*/
function changePhoneCodeByCountry() {
	var countryId = g("registerForm:countriesList").value;
	g('registerForm:statesList').parentNode.parentNode.style.display='block';
	g('registerForm:statesLabel').parentNode.parentNode.style.display='block';
	var url = "ajax.jsf?countryId=" + countryId;
	var ajax = new AJAXInteraction(url, changePhoneCodeByCountryCallback);
	ajax.doGet();
}
/*callBack function for changePhoneCodeByCountry()*/
function changePhoneCodeByCountryCallback(responseXML) {
   var codeValue = responseXML.getElementsByTagName("phoneCode")[0].firstChild.nodeValue;
   var phoneCodeElm = g("registerForm:mobilePhoneCode");
   phoneCodeElm.value = codeValue;
   var phoneCodeElmMobile = g("registerForm:landPhoneCode");
   phoneCodeElmMobile.value = codeValue;
}
//	phone code keyDown
//  allow tab key
function phoneKeys(e) {
	var keynum;

	if(window.event) { // IE
		keynum = e.keyCode;
	}
	else if(e.which) {  // Netscape/Firefox/Opera
		keynum = e.which;
	}
	if (keynum != 9) {  // tab key
		return false;
	}
}
function showAgreemen(link){
	window.open(link.toString(),'','scrollbars=yes,height=600,width=970,resizable=yes,toolbar=yes,screenX=0,left=1024,screenY=0,top=600');
}
function validatePage() {
	if (skinId == 8) {
		var cellphoneNumber = g("registerForm:mobilePhone");
		var contactBySms = g("registerForm:contactBySms");
		if (contactBySms.checked == true && cellphoneNumber.value.length == 0) {
			var mobilePhoneMessage = g("mobilePhoneMessage");
			mobilePhoneMessage.innerHTML = noPhoneErrorMsg;
			mobilePhoneMessage.className = "error_messages";
			g("registerForm:savelink").src = "#{applicationData.imagesPath}/#{applicationData.userLocale.language}_#{applicationData.skinId}/submit_big_u.jpg";
			g("registerForm:savelink").disabled = false;
			return false;
		}
	}
	return myfaces.oam.submitForm('registerForm','registerForm:hiddenbtn');
}
function validateEmail() {
	var target = g("registerForm:email");
	if (target.value.length > 0){
		var url = "ajax.jsf?emailValidation=" + encodeURIComponent(target.value);
		var ajax = new AJAXInteraction(url, validateEmailCallback);
		ajax.doGet();
	}
}
function validateEmailCallback(responseXML) {
   var msg = responseXML.getElementsByTagName("emailErrormsg")[0].firstChild.nodeValue;
   var check = g("registerForm:emailError");
   if (check != null) {
	  check.innerHTML = '';
   }
   var mdiv = g("emailMessage");
   // set the style on the div to invalid
   mdiv.className = "error_messages";
   mdiv.innerHTML = msg;
}
function getMarketingTracking() {
	var isMarketingTrackingFrm = false;
	try {
		isMarketingTrackingFrm = "#{registerForm.marketingTracking}";
	} catch(e) {
		isMarketingTrackingFrm = false;
	}
	
	return isMarketingTrackingFrm;
}
/*insert into register attempts AJAX call*/
function insertUpdateRegisterAttempts() {
	var isMarketingTracking = getMarketingTracking();
	var email = g("registerForm:email");
	var firstName = g("registerForm:firstName");
	var lastName = g("registerForm:lastName");
	var countryId = g("registerForm:countriesList");
	var landLinePhone = g("registerForm:landLinePhone");
	var mobilePhone = g("registerForm:mobilePhone");
	var receiveUpdate = g("registerForm:receiveUpdate");
	if (email.value.length > 0 || landLinePhone.value.length > 6 || mobilePhone.value.length > 6 ){
		var dataa = "emailRegisterAttempts=" + encodeURIComponent(email.value) + "&firstNameRegisterAttempts="
					+ encodeURIComponent(firstName.value) + "&lastNameRegisterAttempts=" + encodeURIComponent(lastName.value)
					+ "&countryRegisterAttempts=" + encodeURIComponent(countryId.value) + "&mobilePhoneRegisterAttempts=" + encodeURIComponent(mobilePhone.value)
					+ "&landLinePhoneRegisterAttempts=" + encodeURIComponent(landLinePhone.value)
					+ "&contactEmailRegisterAttempts=true" 
					+ "&contactSMSRegisterAttempts=true"
					+ "&isShortForm=" + isShortForm
					+ "&marketingTracking=" + isMarketingTracking;
		$.ajax({
			   type: "POST",
			   url: "ajax.jsf",
			   data: dataa
		});

/*    	var ajax = new AJAXInteraction(url, null);
		ajax.doGet();
*/
	}
}
function hideCountryList(){
	if (g("registerForm:countriesList")) {
		var countryId = g("registerForm:countriesList").value;
	} else if (g("registerForm:countriesListRegulated")) {
		var countryId = g("registerForm:countriesListRegulated").value;
		g("registerForm:countriesListRegulated").id="registerForm:countriesList";
	}
	else {
		var countryId = 219;
	}
	if(countryId != 220 ){
		g("registerForm:statesList").parentNode.style.display="none";
	}
}
/*Change the phone code by the value of the country*/
function changePhoneCodeByCountry() {
	var countryId = g("registerForm:countriesList").value;
	if(countryId == 220){
		g('registerForm:statesList').parentNode.parentNode.style.display='';
		g('registerForm:statesLabel').parentNode.parentNode.style.display='';	
	}else{
		g("registerForm:statesList").parentNode.parentNode.style.display="none";
		g("registerForm:statesLabel").parentNode.parentNode.style.display="none";
	}
	var url = "ajax.jsf?countryId=" + countryId;
	var  ajax = new AJAXInteraction(url, changePhoneCodeByCountryCallback);
	ajax.doGet();
}