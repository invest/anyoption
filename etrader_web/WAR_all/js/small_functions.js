/*file: small_functions.js*/

/*small function that could be used in site*/
/*shorted document.getElementById() it's only g() in order to be used very easy even inline*/
function g(id){return document.getElementById(id);}
/*submit a form by id*/
function submit_form(id){g(id).submit();}
/*console.log() shorted*/
function log(){
	if(typeof do_log == "undefined"){var do_log = false;}
	if(do_log){
		console.log(arguments);
	}
}
function addEvent(el,e,fn){
	if(el.addEventListener){
		el.addEventListener(e, fn, false);
	}else{
		el.attachEvent('on'+e, fn);
	}
}
function remEvent(el,e,fn){
	if(el.removeEventListener){
		el.removeEventListener(e, fn, false);
	}else{
		el.detachEvent('on'+e, fn);
	}
}
//use ?logType=num to load page with loggin on
var logIt_type = 3;//0:do not log,1:log info,2:log warnings,3:log errors,4:log all
var logIt_type_timer = null; 
function logIt(params){//type,msg
	if((params.type === 1) && (logIt_type === 1 || logIt_type === 4)){//info
		console.info(params.msg);
	}
	else if((params.type === 2) && (logIt_type === 2 || logIt_type === 4)){//warnings
		console.warn(params.msg);
	}
	else if((params.type == 3) && (logIt_type === 3 || logIt_type === 4)){//errors
		console.error(params.msg);
	}
	// console.log('test.log') - just info
	// console.debug('test.debug') - just info
	// console.info('test.info') - with info ico
	// console.warn('test.warn') - with warning info
	// console.error('test.error') - with error ico
	if(logIt_type_timer == null){ 
		logIt_type_timer = setInterval(function(){console.clear()},1000*60*5);
	}
}

//disable annoying demo Lightstreamer alert popup
(function() {
	var proxied = window.alert;
	window.alert = function() {
		if (arguments[0].toLowerCase().search('lightstreamer') > -1) {
			logIt({'type':1,'msg':arguments});
		} else {
			return proxied.apply(this, arguments);
		}
	};
})();

if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
    'use strict';
    if (this == null) {
      throw new TypeError();
    }
    var n, k, t = Object(this),
        len = t.length >>> 0;

    if (len === 0) {
      return -1;
    }
    n = 0;
    if (arguments.length > 1) {
      n = Number(arguments[1]);
      if (n != n) { // shortcut for verifying if it's NaN
        n = 0;
      } else if (n != 0 && n != Infinity && n != -Infinity) {
        n = (n > 0 || -1) * Math.floor(Math.abs(n));
      }
    }
    if (n >= len) {
      return -1;
    }
    for (k = n >= 0 ? n : Math.max(len - Math.abs(n), 0); k < len; k++) {
      if (k in t && t[k] === searchElement) {
        return k;
      }
    }
    return -1;
  };
}
/*input on focus delete the value*/
function inpFocus(field,noblur){
	var def = (field.getAttribute('data-def') != undefined)?field.getAttribute('data-def'):(field.alt != '')?field.alt:field.defaultValue;
	var pass = (field.getAttribute('data-p') != undefined)?true:false;
	var dir_f = (field.getAttribute('data-dir_f') != undefined)?field.getAttribute('data-dir_f'):'';//foreced dir
	var tooltip = (field.getAttribute('data-tooltip') != undefined)?g(field.getAttribute('data-tooltip')):'';
	if(field.value == ""){
		field.value = def;
		if(pass){field.type = 'text';}
		if(dir_f != ''){field.dir = '';}
		field.className = field.className.replace(/active/g,'');
	}
	else if(field.value.toLowerCase() == def.toLowerCase()){
		field.value = "";
		if(pass){field.type = 'password';}
		if(dir_f != ''){field.dir = dir_f;}
		field.className += " active";
		field.className = field.className.replace(/correct/g,'');
	}
	else{
		field.className += " active";
		field.className = field.className.replace(/correct/g,'');
	}
	if(field.getAttribute('data-tooltip_show') == 't'){
		if(tooltip != ""){
			tooltip.style.display = "none";
			field.setAttribute('data-tooltip_show','');
		}	
	}
	else{
		if(tooltip != ""){
			if(tooltip.id == 'funnel_tooltip_phone'){
				if(field.value == ""){
					var tinf = g('funnel_tooltip_phone_1');
					var terr = g('funnel_tooltip_phone_2');
					tinf.style.display = "block";
					terr.style.display = "none";
				}
			}
			tooltip.style.display = "block";
			tooltip.style.top = -(tooltip.offsetHeight/2)+(field.offsetHeight/2)+'px';
			field.setAttribute('data-tooltip_show','t');
		}	
	}
	if(noblur == undefined){
		field.onblur = function(){
			inpFocus(field,noblur);
			eval(field.getAttribute('onblur'));
		}
	}
}
function inpFocusShort(field,noblur){
	var def = (field.getAttribute('data-def') != undefined)?field.getAttribute('data-def'):(field.alt != '')?field.alt:field.defaultValue;
	field.value = def;
	if(field.getAttribute('data-p') != null){
		field.type = "text";		
	}
}
function checkBox(th){
	var inp = th.getElementsByTagName('input')[0];
	if(inp.checked){
		inp.checked = false;
		th.className = th.className.replace(/ checked/g,'');
	}
	else{
		inp.checked = true;
		th.className += " checked";
	}
}
/*GET current time formated*/
function getCurrentTimeFormatted() {
	var tf = "";
	try {
		var d = new Date();
		tf = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDay() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds() + "." + d.getMilliseconds();
	} catch (e) {
		// do nothing
	}
	return tf;
}
function openEmailAlert() {
	g('emailAlertDiv').style.display = 'block';
}
function show_tab(th,tabContent){
	var lis = th.parentNode.getElementsByTagName('li');
	for(var i=0;i<lis.length;i++){
		lis[i].className = lis[i].className.replace(/ active/gi,'');
	}
	th.className += " active";
	
	var tabContent = g(tabContent);
	var lis = tabContent.getElementsByTagName('li');
	for(var i=0;i<lis.length;i++){
		lis[i].className = lis[i].className.replace(/hov/gi,'');
	}
	g('content_'+th.id).className += " hov";
}
function simple_show_and_hide(id){
	var el = g(id);
	if(el.style.display == "block"){
		el.style.display = "none";
	}
	else{
		el.style.display = "block";
	}
}
/*Pop up for documents. Gets parameter of the class with the image*/
function pop_doc(cls, cont_id){
	if(g('pop_doc_bgr') == undefined){
		var bgr = document.createElement('div');
		bgr.className = "pop_doc_bgr";
		bgr.id = "pop_doc_bgr";
		bgr.onclick = function(){pop_doc_close();}
		window.document.body.appendChild(bgr);
		
		var holder = document.createElement('div');
		holder.id = "pop_doc_holder";
		window.document.body.appendChild(holder);
	}
	else{
		var holder = g('pop_doc_holder');
	}
	holder.className = "pop_doc_holder "+cls;
	if(typeof cont_id != 'undefined'){
		holder.innerHTML = g(cont_id).innerHTML;
	}
	else{
		holder.innerHTML = '';
	}
	var ss = document.createElement('span');
	ss.className = "pop_doc_close icon_set_1";
	ss.onclick = function(){pop_doc_close();}
	holder.appendChild(ss);
	
	window.scrollTo(0,0);
	gd_fade('pop_doc_bgr',0,100);
	gd_fade('pop_doc_holder',0,100);
}
function pop_doc_close(){
	gd_fade('pop_doc_bgr',100,0);
	gd_fade('pop_doc_holder',100,0);
}
/*fade*/
var gd_fade_t = new Array();
function gd_fade(id,from,to){
	var el = g(id);
	clearTimeout(gd_fade_t[id]);
	if(from > to){var dir = "down";}
	else{var dir = "up";el.style.display = "block";}
	var left = Math.abs(from-to);
	function doIt(){
		if(left > 0){
			if(dir == "up"){
				from += 10;
			}
			else{
				from -= 10;
			}
			el.style.opacity = from/100;
			el.style.filter = "alpha(opacity="+from+")";
			left -= 10;
			gd_fade_t[id] = setTimeout(doIt,30);
		}
		else{
			if(dir == "down"){
				el.style.display = "none";
			}
		}
	}
	doIt();
}
/*fade END*/
function cloneId(id,where){
	var el = g(id);
	var w = g(where);
	var clone = el.cloneNode(true);
	w.appendChild(clone);
}
/*foote strip*/
function footer_strip(id){
	var d = g(id);
	var img = [];
	img[0] = g(id+'_1');
	var imgs = img[0].getElementsByTagName('span');
	var sum = 0;
	for(var i=0;i<imgs.length;i++){
		sum += imgs[i].offsetWidth+30;
	}
	img[0].style.width = sum+"px";
	cloneId(id+'_1',id);
	d.getElementsByTagName('div')[1].id = id+'_2';
	img[1] = d.getElementsByTagName('div')[1];
	img[0].style.left = img[0].offsetWidth+"px";
	
	var c = 0;
	var w = img[0].offsetWidth;
	var hw = d.offsetWidth;
	var inc = 1;
	var curr = img[1];
	var curr2 = img[0];
	var st = true;
	var doSetTimeout = function(){
		if(((Math.abs(curr.offsetLeft)+hw+inc) > w)&&(st)){
			curr2.style.left = hw+"px";
			st = false;
		}
		if(!st){
			curr2.style.left = curr2.offsetLeft-inc+"px";
		}
		curr.style.left = curr.offsetLeft-inc+"px";
		if(Math.abs(curr.offsetLeft) >= w){
			var curr3 = curr;
			curr = curr2;
			curr2 = curr3;
			st = true;
		}
		setTimeout(doSetTimeout,40);
	}
	doSetTimeout();
}
/*live person*/
function livePersonType(){
	var dim = getScreenDim();
	if(dim[0] > 1215){
		g('livePersonMainDiv').className += " big";
	}
	else{
		g('theBody').className = "lowRes";
	}
}
/*screen dimenstions*/
function getScreenDim(){
	var rtn = new Array();
	rtn[0] = 0;rtn[1] = 0;
	if (document.body && document.body.offsetWidth) {
		rtn[0] = document.body.offsetWidth;
		rtn[1] = document.body.offsetHeight;
	}
	if (document.compatMode=='CSS1Compat' && document.documentElement && document.documentElement.offsetWidth ) {
		rtn[0] = document.documentElement.offsetWidth;
		rtn[1] = document.documentElement.offsetHeight;
	}
	if (window.innerWidth && window.innerHeight) {
		rtn[0] = window.innerWidth;
		rtn[1] = window.innerHeight;
	}
	return rtn;
}
/*accordion*/
var accordion_last_open = null;
var accordion_last_timer = new Array();
function accordion(th){
	if((accordion_last_open != null)&&(accordion_last_open != th)){
		accordion(accordion_last_open);
	}
	var top = getTopLevelByData(th,'data-top','t');
	var ul = top.getElementsByTagName('ul')[0];
	var title = top.getElementsByTagName('span')[0];
	if(ul.style.display == "block"){
		var dir = 'up';
		var scr_left = ul.offsetHeight;
		title.className = title.className.replace(/hov/gi,'');
		accordion_last_open = null;
	}
	else{
		ul.style.display = "block";
		var scr_left = ul.offsetHeight;
		ul.style.height = 0;
		var dir = 'down';
		title.className += " hov";
		accordion_last_open = th;
	}
	
	var inc = 0;
	var doSetTimeout = function(){
		if(scr_left > 200){
			inc = Math.round(scr_left*0.20);
		}
		else if(scr_left > 2){
			inc = Math.round(scr_left*0.30);
		}
		if(scr_left > 0){
			if(dir == "up"){
				ul.style.height = ul.offsetHeight-inc+"px";
			}
			else{
				ul.style.height = ul.offsetHeight+inc+"px";
			}
			scr_left -= inc;
			accordion_last_timer[th] = setTimeout(doSetTimeout,30);
		}
		else{
			if(dir == "up"){
				ul.style.display = "none";
				ul.style.height = "auto";
			}
		}
	}
	doSetTimeout();
}
function getTopLevelByData(el,data_t,data){
	while(el.getAttribute){
		if(el.getAttribute(data_t) == data){
			return el;
		}
		el = el.parentNode;
	}
}
function getTopLevelById(el,id){
	while(el){
		if(el.id == id){
			return el;
		}
		el = el.parentNode;
	}
}
function show_oneClick(sec,dir){
	var el = g('oneClick0100');
	if(dir == 'rtl'){
		var left = el.offsetWidth-el.offsetLeft;
	}else{
		var left = el.offsetWidth+el.offsetLeft;
	}
	var inc = 5;
	var doSetTimeout = function(){
		if(dir == "ltr"){
			el.style.left = el.offsetLeft-inc+'px';
		}else{
			el.style.left = el.offsetLeft+inc+'px';
		}
		if(left >= inc){left-=inc;}
		else{inc = left;}
		if(left > inc){
			setTimeout(doSetTimeout,40);
		}
	}
	doSetTimeout();
	function hideIt(){
		el.style.display = "none";
	}
	setTimeout(hideIt,sec);
}
function openPopUp(id,bgr){
	g(id).style.display = 'block';
	if(typeof bgr == 'string'){
		g(bgr).style.display = 'block';	
	}
}
function gd_closePopUp(id,bgr){
	g(id).style.display = 'none';
	if(typeof bgr == 'string'){
		g(bgr).style.display = 'none';	
	}
}
/*Change the phone and fax numbers by the value of the country*/
function changePhoneAndFaxByCountry() {
	var countryId = document.getElementById("contactForm:countriesList").value;
	var url = "ajax.jsf?countryIdForSupport=" + countryId;
	var ajax = new AJAXInteraction(url, changePhoneAndFaxByCountryCallback);
	ajax.doGet();
}
/*callBack function for changePhoneAndFaxByCountry()*/
function changePhoneAndFaxByCountryCallback(responseXML) {
	var msg = responseXML.getElementsByTagName("SupportPhoneFax")[0].firstChild.nodeValue;
	var splitMsg = msg.split(",");
	//alert(msg[0]);
	//alert(msg[1]);
	var phoneElm = document.getElementById("phone");
	var faxElm = document.getElementById("fax");
	var mobile = document.getElementById("contactForm:mobilePhone");
	phoneElm.innerHTML = splitMsg[0];
	faxElm.innerHTML = splitMsg[1];
	if (splitMsg.length >2 && mobile !=null){
		mobile.value = splitMsg[2];
	}
}
//TO DO - remove the link and use global var from js_vars.jsf
function open_send_sms_mail_pop(phoneType, popupType){
	var url = g("thickBoxLink" + popupType).href;
	var linkExtra = url.lastIndexOf('?');
	if (linkExtra > -1) {
		url = url.substring(0,linkExtra);
	}
	url += "?phoneType=" + phoneType+ "&KeepThis=true&TB_iframe=true&height=420&width=480&modal=true";
	g('sms_iframe').src = url;
	g('sms_iframe').style.display = "block";
	if(!g('black_bgr')){
		var s = document.createElement('div');
		s.className = "black_bgr";
		s.id = "black_bgr";
		s.onclick = function(){close_send_sms_mail_pop()}
		window.document.body.appendChild(s);
	}
	g('black_bgr').style.display = "block";
	return false;
}
function close_send_sms_mail_pop(){
	g('black_bgr').style.display = "none";
	g('sms_iframe').style.display = "none";
}
/*FOR REFACTOR*/
var timerID = new Array();
var startTime = new Array();
var obj = new Array();
var endHeight = new Array();
var moving = new Array();
var dir = new Array();
var timerlen = 5;
var slideAniLen = 250;
var assets_last_label = '';
var assets_last_div_id = '';
function slidedownAssets(obj){
	var objname = 'mar' + obj.getAttribute('data-market-id');
	obj.className = "asset_select";
	if((assets_last_label != "")&&(assets_last_label != obj)){
		assets_last_label.className = "asset_name";
		slideup(assets_last_div_id);
	}
	assets_last_label = obj;
	assets_last_div_id = objname;

	if(document.getElementById(objname).style.display != "none"){
		obj.className="asset_name";
		moving[objname] = true;
		dir[objname] = "up";
		startslide(objname);
    }else{
		moving[objname] = true;
		dir[objname] = "down";
		startslide(objname);
    }
}
function slideup(objname){
	if(moving[objname])
			return;

	if(document.getElementById(objname).style.display == "none")
			return; // cannot slide up something that is already hidden

	moving[objname] = true;
	dir[objname] = "up";
	startslide(objname);
}

function startslide(objname){
	obj[objname] = document.getElementById(objname);
	obj[objname].style.display = "block";
	endHeight[objname] = obj[objname].offsetHeight;
	obj[objname].style.display = "none";
	startTime[objname] = (new Date()).getTime();

	if(dir[objname] == "down"){
			//obj[objname].style.height = "1px";
	}
	obj[objname].style.display = "block";
	timerID[objname] = setInterval('slidetick(\'' + objname + '\');',timerlen);
}
function slidetick(objname){
	var elapsed = (new Date()).getTime() - startTime[objname];

	if (elapsed > slideAniLen)
		endSlide(objname)
	else {
		var d =Math.round(elapsed / slideAniLen * endHeight[objname]);
		if(dir[objname] == "up")
				d = endHeight[objname] - d;

		obj[objname].style.height = d + "px";
	}

	return;
}
function endSlide(objname){
	clearInterval(timerID[objname]);

	if(dir[objname] == "up")
			obj[objname].style.display = "none";

	obj[objname].style.height = endHeight[objname] + "px";

	delete(moving[objname]);
	delete(timerID[objname]);
	delete(startTime[objname]);
	delete(endHeight[objname]);
	delete(obj[objname]);
	delete(dir[objname]);

	return;
}
/*FOR REFACTOR END*/
function selectAsset_past(element,marketId){
	var val = element.innerHTML;
	g('selectedAssetNameDisplay').innerHTML = val;
	g('lastLevelsForm:selectedAssetName').value = val;
	g('lastLevelsForm:marketGroup').value=marketId;
	g('lastLevelsForm').submit();
}
var feature_accordion_last = '';
var feature_accordion_last_n = '';
function feature_accordion(th,n){
	if((feature_accordion_last != "")&&(feature_accordion_last != th)){
		feature_accordion(feature_accordion_last,feature_accordion_last_n);
	}
	var sn = g('snipped_'+n);
	var txt = g('text_'+n);
	if(txt.style.display == "none"){
		var tmp = sn.offsetHeight;
		sn.style.display = "none";
		txt.style.display = "block";
		var scr_left = txt.offsetHeight-tmp;
		txt.style.height = tmp+'px';
		th.innerHTML = th.getAttribute('data-close');
		feature_accordion_last = th;
		feature_accordion_last_n = n;
		var dir = 'down';
	}
	else{
		sn.style.display = "block";
		var tmp = sn.offsetHeight;
		sn.style.display = "none";
		var scr_left = txt.offsetHeight-tmp;
		th.innerHTML = th.getAttribute('data-open');
		feature_accordion_last = '';
		feature_accordion_last_n = '';
		var dir = 'up';
	}
	
	var inc = 0;
	var doSetTimeout = function(){
		if(scr_left > 200){
			inc = Math.round(scr_left*0.20);
		}
		else if(scr_left > 2){
			inc = Math.round(scr_left*0.30);
		}
		if(scr_left > 0){
			if(dir == "up"){
				txt.style.height = txt.offsetHeight-inc+"px";
			}
			else{
				txt.style.height = txt.offsetHeight+inc+"px";
			}
			scr_left -= inc;
			accordion_last_timer[th] = setTimeout(doSetTimeout,30);
		}
		else{
			if(dir == "up"){
				txt.style.display = "none";
				txt.style.height = "auto";
				sn.style.display = "block";
			}
		}
	}
	doSetTimeout();
}
function fetureAutoOpen(){
	var hash = window.location.hash.replace('#','');
	if(hash == newFeaturesOpenLive){
		feature_accordion(g('link_'+newFeaturesOpenLive),newFeaturesOpenLive);
	}
	else if(hash == newFeaturesOpenChooseProfit){
		feature_accordion(g('link_'+newFeaturesOpenChooseProfit),newFeaturesOpenChooseProfit);
	}
	else if(hash == newFeaturesOpenTrendProfit){
		feature_accordion(g('link_'+newFeaturesOpenTrendProfit),newFeaturesOpenTrendProfit);
	}
	else if(hash == newFeaturesOpenTakeProfit){
		feature_accordion(g('link_'+newFeaturesOpenTakeProfit),newFeaturesOpenTakeProfit);
	}
	else if (hash == newFeaturesOpenProfitLine){
		feature_accordion(g('link_'+newFeaturesOpenProfitLine),newFeaturesOpenProfitLine);
	}
	else if (hash == newFeaturesOpenRollForward){
		feature_accordion(g('link_'+newFeaturesOpenRollForward),newFeaturesOpenRollForward);
	}
	else if (hash == newFeaturesOpenSMS) {
		feature_accordion(g('link_'+newFeaturesOpenSMS),newFeaturesOpenSMS);
	}
	else if (hash == newFeaturesOpenShowOff) {
		feature_accordion(g('link_'+newFeaturesOpenShowOff),newFeaturesOpenShowOff);
	}
}
function showLastTopTrades(tab){
	tab--;
	var ul = g('top5_selector');
	var lis = ul.getElementsByTagName('li');

	for(var i=0; i<lis.length; i++){
		lis[i].className = lis[i].className.replace('hov','');
		g("lastTopTrades_"+i).style.display = "none";
	}
	g("lastTopTrades_"+tab).style.display = "block";
	lis[tab].className += " hov";
}

function collectAllPressed(elm) {
	var list = document.getElementsByTagName("input");
	for (var i = 0; i < list.length; i++ ) {
		var e = list[i];
		if (e.type == "checkbox" && e.id.indexOf("mailBoxForm") > -1) {
			e.checked = elm.checked;
		}
	}
}

var checkBoxPreesed = false;

function updateFlag(elm) {
	checkBoxPreesed = true;
	var allElm = document.getElementById("selectAll");
	if (allElm.checked) {
		if (!elm.checked) {
			allElm.checked = false;
		}
	}
}

/*open template when pressing on the row without pressing on the checkBox*/
function openTemplate(rowNum) {
	if (checkBoxPreesed == false) {
		var linkVar = document.getElementById("mailBoxForm:data:" + rowNum + ":hiddenbtn");
		linkVar.onclick();
	} else {
		checkBoxPreesed = false;
	}
}

function changeCurrency(toHide, toShow) {
	divToHide = document.getElementById(toHide + "Div");
	divToHide.style.display = "none"
	divToShow = document.getElementById(toShow + "Div");
	divToShow.style.display = 'inline';
}

function changeCardCallback(responseXML) {
   var mdiv = document.getElementById("withdrawForm:creditDiv");
   var msg = responseXML.getElementsByTagName("creditMsg")[0];
   if ( msg.firstChild != null ) {
	   msg = msg.firstChild.nodeValue;
	   mdiv.innerHTML = msg;
   } else {
		mdiv.innerHTML = "";
   }
   var errorMsg = document.getElementById("withdrawForm:cardIdError")
   if (errorMsg != null) {
		 errorMsg.innerHTML = '';;
   }

}
function changeCard() {
    var cardId = document.getElementById("withdrawForm:ccNum").value;
    if (cardId != 0) {
	    var url = "ajax.jsf?cardId=" + cardId;
	    var ajax = new AJAXInteraction(url, changeCardCallback);
	    ajax.doGet();
	}
}
function addScriptToHead(filename,scr_id,folder){
	if(!g(scr_id)){
		if(typeof folder == 'undefined'){
			var folder_new = '';
		}
		else{
			var folder_new = folder;
		}
		if(typeof funnel_version == 'undefined'){
			funnel_version = '0';
		}
		var js = document.createElement('script');
		js.id = scr_id;
		var pref = (filename.indexOf('?') > -1)?'&':'?';
		js.src = context_path+folder_new + "/js/" + filename + pref + "ver="+funnel_version;
		document.getElementsByTagName('head')[0].appendChild(js);
	}
}
var ua = navigator.userAgent.toLowerCase();
var funnel_div_id = '';
var register_lp = false;
function initRegForm(id,skinId){
	funnel_div_id = id;
	addScriptToHead('regForm_all.js?version=cvsTimeStamp','initRegForm');
	//regForm_all.js loads map.js
	//map.js loads validation_reg_funnel.js
	//validation_reg_funnel.js loads regForm_map.jsf
		
	//Add json lib. for IE7
	if (ua.indexOf('msie 7.0') >= 0){  
		addScriptToHead('json2.js?version=cvsTimeStamp','initRegFormJson');
	}
}
var global_deposit_prefix = '';
var add_to_single_errors_map_out = [];
function initDeposit(deposit_prefix){
	global_deposit_prefix = deposit_prefix;
	addScriptToHead('regForm_all.js?version=cvsTimeStamp','initRegForm');
	//regForm_all.js loads map.js
	//map.js loads validation_reg_funnel.js
	//validation_reg_funnel.js loads regForm_map.jsf
}
function cont_us_show(show){
	if(show){
		g('cont_us_phones').style.display = 'block';
		g('cont_us_right_banner_opend').style.display = 'block';
		g('cont_us_right_banner_closed').style.display = 'none';
	}else{
		g('cont_us_phones').style.display = 'none';
		g('cont_us_right_banner_opend').style.display = 'none';
		g('cont_us_right_banner_closed').style.display = 'block';
	}
}
function submitWait(th){
	th.onclick = function (){return false;}
	th.className += " disabled";
}
var paymentMethod_menu = '';
function open_deposit(th,deposit_prefix){
	try{
		errorMap.clear();
	}catch(e){}
	global_deposit_prefix = deposit_prefix;
	try{
		idPrefix = deposit_prefix;
	}catch(e){}
	var el = th.parentNode.getElementsByTagName('li')[1];
	if(el.style.display == "none"){
		el.style.display = "block";
		var title = th.getElementsByTagName('span')[0];
		title.className += " paymentMethods_title_selected";
	}else{
		el.style.display = "none";
		var title = th.getElementsByTagName('span')[0];
		title.className = "paymentMethods_title";
	}
	if((paymentMethod_menu != '')&&(paymentMethod_menu != th)){
		var el = paymentMethod_menu.parentNode.getElementsByTagName('li')[1];
		el.style.display = "none";
		var title = paymentMethod_menu.getElementsByTagName('span')[0];
		title.className = "paymentMethods_title";
	}
	paymentMethod_menu = th;
}
function set_deposit_prefix(deposit_prefix){
	global_deposit_prefix = deposit_prefix;
	try{
		idPrefix = deposit_prefix;
	}catch(e){}
}
function activateTradeBox(element) {
	var td = getChildOfType(element, "TD", 8);
	var div = getChildOfType(td, "DIV", 1);
	var marketId = div.innerHTML;
	window.top.location.href = context_path + tradePageN + "?marketId=" + marketId;
}
function showPopupIfLastLoginBeforeDeploy(){
	var startOfValidityDate = new Date();
	startOfValidityDate.setFullYear(2014,3,27); // date of deploy : 27.04.2014
	var endOfValidityDate = new Date();
	endOfValidityDate.setFullYear(2014,6,27); // date of deploy : 27.05.2014
	var today = new Date();
	if(today<endOfValidityDate && today>startOfValidityDate) {
		var timeFirstVisitCookie = getCookie("onClickVisit");
		if (timeFirstVisitCookie == null || timeFirstVisitCookie == "") {
			document.cookie = "onClickVisit=true;expires=" + endOfValidityDate.toUTCString();
			pop_doc('oneclick_popup_container oneclick_popup_container_'+userLanguage);
		}	
	}
}
function showPopup0100commission(){
	var startOfValidityDate = new Date();
	startOfValidityDate.setFullYear(2014,6,27); // date of deploy : 27.07.2014
	var endOfValidityDate = new Date();
	endOfValidityDate.setFullYear(2014,9,28); // date of deploy : 28.10.2014
	var today = new Date();
	if(today<endOfValidityDate && today>startOfValidityDate) {
		var timeFirstVisitCookie = getCookie("0100Commssion");
		if (timeFirstVisitCookie == null || timeFirstVisitCookie == "") {
			document.cookie = "0100Commssion=true;expires=" + endOfValidityDate.toUTCString();
			openPopUp('binary0100_commission_popup','binary0100_commission_popup_bgr');
		}	
	}
}
function getCookie(c_name){
	var c_value = document.cookie;
	var c_start = c_value.indexOf(" " + c_name + "=");
	if (c_start == -1) {
	  	c_start = c_value.indexOf(c_name + "=");
	}
	if (c_start == -1) {
	  	c_value = null;
	} else {
	  	c_start = c_value.indexOf("=", c_start) + 1;
	  	var c_end = c_value.indexOf(";", c_start);
	  	if (c_end == -1) {
			c_end = c_value.length;
	  	}
		c_value = unescape(c_value.substring(c_start,c_end));
	}
	return c_value;
}
function createIframe(id,className,url,width,height){
	if(!g(id)){
		var iframe = document.createElement('iframe');
		iframe.id = id;
		iframe.scrolling = "no";
		document.body.appendChild(iframe);
	}
	else{
		var iframe = g(id);
	}
	iframe.style.display = "block";
	iframe.className = className;
	iframe.src = "about:blank";
	iframe.src = url;
	iframe.style.width = width+"px";
	iframe.style.height = height+"px";
	iframe.allowtransparency = true;
	return iframe;
}
function createBackground(callBack,id,className){
	if(typeof id == 'undefined'){var id = 'background';}
	if(typeof className == 'undefined'){var className = 'black_bgr';}
	
	if(!g(id)){
		var bgr = document.createElement('div');
		bgr.className = className;
		bgr.id = id;
		bgr.onclick = function(){eval(callBack)}
		window.document.body.appendChild(s);
	}
	else{
		var bgr = g(id);
	}
	return bgr;
}
function zxcPos(obj){
	var rtn=[0,0];
	while(obj){
		rtn[0]+=obj.offsetLeft;
		rtn[1]+=obj.offsetTop;
		obj=obj.offsetParent;
	}
	return rtn;
}
function absolutePosition(obj_click,obj_pos,direction,offsetL,offsetT){//direction:tl,tr,bl,br
	if(typeof  obj_click == "string"){obj_click = g(obj_click);}
	if(typeof  obj_pos == "string"){obj_pos = g(obj_pos);}
	if(typeof  offsetL == "undefined"){offsetL = 0;}
	if(typeof  offsetT == "undefined"){offsetT = 0;}
	var pos = zxcPos(obj_click);
	obj_pos.style.display = "block";
	switch(direction){
		case 'tr':
			var offsetL_in = -obj_pos.offsetWidth+obj_click.offsetWidth;
			var offsetT_in = 0;
			break;
		case 'bl':
			var offsetL_in = 0;
			var offsetT_in = obj_click.offsetHeight;
			break;
		case 'br':
			var offsetL_in = obj_pos.offsetWidth+obj_click.offsetWidth;
			var offsetT_in = obj_pos.offsetHeight+obj_click.offsetheight;
			break;
		default:
			var offsetL_in = 0;
			var offsetT_in = 0;
			break;
	}
	obj_pos.style.display = "none";
	obj_pos.style.left = pos[0]+offsetL+offsetL_in+'px';
	obj_pos.style.top = pos[1]+offsetT+offsetT_in+'px';
}
function showRQHint(obj,oppId,direction){
	var url = "//" + window.location.hostname + context_path + "/jsp/single_pages/reuters_quotes_popup.jsf?oppId=" + oppId;
	var iframe = createIframe('showRQHint','hintsReutersClass',url,385,140);
	iframe.onload = autoResize('showRQHint');
	iframe.onmouseout = function(){this.style.display = "none";}
	absolutePosition(obj,iframe,direction);
	iframe.style.display = "block";
}
function autoResize(id){
	var newheight; var newwidth;
	if(document.getElementById){
		newheight=document.getElementById(id).contentWindow.document.body.scrollHeight;
		newwidth=document.getElementById(id).contentWindow.document.body.scrollWidth;
	}
	document.getElementById(id).style.height= newheight + "px";
	document.getElementById(id).style.width= newwidth + "px";
}
function printSuccessOneTouch(invId, oneTUpDown) {
	window.open(context_path + "/jsp/single_pages/invSuccessPrintOneTouch.jsf?investmentId=" + invId + "&typeIn=" + oneTUpDown, "invSuccessPrint", "width=498,height=427,menubar=1,toolbar=0,scrollbars=0");
}
function binary0100_general_exp_pop_up() {
	window.location = "/trading-assets";
}
if (window.top.location.hostname == 'etrader.co.il') {
	window.top.location.href = window.top.location.href.replace("etrader.co.il", "www.etrader.co.il");
}
function toolTipAlign(th){
	var tooltip = th.getElementsByTagName('span')[0];
	tooltip.style.top = -(tooltip.offsetHeight-th.offsetHeight)/2+'px';
}
/* Handles all actions upon bank name change */
function handleBankNameChange() {
	if (null != document.getElementById("wireForm:bankNameList")) {
		var bankNameId = document.getElementById("wireForm:bankNameList").value;
		if (bankNameId == 40) { // other bank
			if(navigator.userAgent.toLowerCase().indexOf("msie") != -1) {
				document.getElementById("wireForm:bankNameLabel").parentNode.style.display="block";
			} else {
				document.getElementById("wireForm:bankNameLabel").parentNode.style.display="table-row";
			}
		} else {
			document.getElementById("wireForm:bankNameLabel").parentNode.style.display="none";
		}
	}
}
function displayBeneficiaryNameTooltip() {
	var beneficiaryName = document.getElementById("wireForm:beneficiaryName");
	var beneficiaryNameError = document.getElementById("wireForm:beneficiaryNameError");
	var beneficiaryTooltipGroup = document.getElementById("wireForm:beneficiaryTooltipGroup");
	if (beneficiaryName != null) {
		if (beneficiaryName.value == '') {
			if (beneficiaryNameError.innerHTML != null && beneficiaryNameError.innerHTML != '') {
				beneficiaryTooltipGroup.style.visibility = "hidden";
			} else {
				beneficiaryTooltipGroup.style.visibility = "visible";
			}
		}
	}
}
/*start*/
function openElement(params){
	var closed = false;
	if(typeof params.id != "undefined"){
		if(closeOnClickOut_params.current_id == params.id){
			closeElement({'id':closeOnClickOut_params.current_id});
			closed = true;
		}else{
			g(params.id).style.display = "block";
		}
	}
	else if(typeof params.span != "undefined"){
		if(closeOnClickOut_params.current_el == params.span){
			closeElement({'id':closeOnClickOut_params.current_id,'span':closeOnClickOut_params.current_el});
			closed = true;
		}else{
			var ul = params.span.parentNode.getElementsByTagName('ul')[0];
			params.span.className += " active";
			ul.style.display = "block";
			if((typeof ul.id == 'undefined')||(typeof ul.id == null)||(ul.id == '')){
				params.id = 'rand_id_'+Math.round(Math.random()*1000000);
				ul.id = params.id;
			}else{
				params.id = ul.id;
			}
		}
	}
	if(params.autoClose && !closed){
		setTimeout(function(){closeOnClickOut({'id':params.id,'span':params.span,'callBack':params.callBack});},20);
	}
}
function closeElement(params){//el,top_id,id
	if(typeof params.last != "undefined"){
		if(closeOnClickOut_params.current_id != null){
			closeElement({'id':closeOnClickOut_params.current_id,'span':closeOnClickOut_params.current_el});
		}
	}
	else if(typeof params.id != "undefined"){//close by id
		g(params.id).style.display = "none";
		if(typeof params.span != "undefined"){//close drop down menu with ul
			params.span.className = params.span.className.replace(/ active/g,'');
		}
	}
	else if(typeof params.el != "undefined"){//close simple pop up
		params.el.parentNode.style.display = "none";
	}
	else if((typeof params.top_id != "undefined")&&(params.top_id != "")){//close using this and top level
		getTopLevelById(params.el,params.top_id).style.display = "none";
	}
	remEvent(window.document.body,'click',closeOnClickOutMain);
	closeOnClickOut_params = {'current_id':null,'current_el':null,'callBack':null};
}
var closeOnClickOut_params = {'current_id':null,'current_el':null,'callBack':null};
function closeOnClickOut(params){//id,callBack,el
	closeOnClickOut_params = {'current_id':params.id,'current_el':params.span,'callBack':params.callBack};
	addEvent(window.document.body,'click',closeOnClickOutMain);
}
function closeOnClickOutMain(e){
	var t = e.target || e.srcElement;
	while(t){
		if(t.id == closeOnClickOut_params.current_id){
			return false;
		}
		else{
			t=t.parentNode;
		}
	}
	if(typeof closeOnClickOut_params.callBack != "undefined"){
		eval(closeOnClickOut_params.callBack);
		remEvent(window.document.body,'click',closeOnClickOutMain);
	}
	else{
		closeElement({'id':closeOnClickOut_params.current_id,'span':closeOnClickOut_params.current_el});
	}
	closeOnClickOut_params = {'current_id':null,'current_el':null,'callBack':null};
}
/*end*/
function level_elements(ids){
	var max_height = 0;
	for(var i=0; i<ids.length; i++){
		if (ids[i] != '') {
			var el = g(ids[i]);
			if(el && el.offsetHeight > max_height){
				max_height = el.offsetHeight;
			}
		}
	}
	for(var i=0; i<ids.length; i++){
		if (ids[i] != '') {
			g(ids[i]).style.height = max_height+'px';
		}
	}
}
function error_msg_inp_match(id){
	var els = g(id).getElementsByTagName('span');
	var errorCount = 0;
	for(var i=0; i<els.length; i++){
		if(els[i].getAttribute('data-inp_id') != null && els[i].getAttribute('data-inp_id') != undefined){
			if(g(els[i].getAttribute('data-inp_id')) != null && typeof g(els[i].getAttribute('data-inp_id')) != 'undefined'){
				var inp = g(els[i].getAttribute('data-inp_id'));
				if(els[i].innerHTML != ""){
					inp.className += " error";
					errorCount++;
				}
				else{
					inp.className = inp.className.replace(/ error/g,'');
				}
			}
		}
	}
}

function shouldChangeAcc(){
	var iKnowYouCookie = getCookie("AOETUsername");
	var userNameInput = document.getElementById("login:j_username");
	var rememberMeCheckBox = document.getElementById("login:rememberMeCheckBox");
	var cookieVal = "";
	if (iKnowYouCookie != null && iKnowYouCookie != "") {
		cookieVal = iKnowYouCookie.replace(/"/g, "");
	}
	
	if(rememberMeCheckBox.checked){
		if(userNameInput.value != cookieVal){
			createCookie("AOETUsername", '"' + userNameInput.value + '"', "365*2");
		}
	}else{
		document.cookie = "AOETUsername=;expires=;path=/";
	}
}

function changeAcc() {
	document.cookie = "AOETUsername=;expires=;path=/";
	//location.reload(); - causes firefox to display alert
	window.location.href = window.location.href;
}

function do_we_know_you() {
	var iKnowYouCookie = getCookie("AOETUsername");
	
	var rememberMeCheckBox = document.getElementById("login:rememberMeCheckBox");
	var userNameInput = document.getElementById("login:j_username");
	
	var userNameLabel = document.getElementById("login:userNameLabel");
	
	if(skinId == 1){
		var changeAccount = document.getElementById("changeAccount");
		var userNameOutput = document.getElementById("login:j_username2");
	}
	
	if (iKnowYouCookie != null && iKnowYouCookie != "") {
		iKnowYouCookie = iKnowYouCookie.replace(/"/g, "");
		
		userNameInput.value = iKnowYouCookie;
		//rememberMeCheckBox.checked = true;
		
		if(skinId == 1){
			userNameOutput.innerHTML = iKnowYouCookie;
			userNameOutput.style.display = "";
			userNameInput.style.display = "none";
			userNameLabel.style.display = "none";
			changeAccount.style.display = "";
		}
	} else {
		userNameInput.value = "";
		//rememberMeCheckBox.checked = false;
		
		if(skinId == 1){
			userNameOutput.innerHTML = "";
			userNameOutput.style.display = "none";
			userNameInput.style.display = "";
			userNameInput.className="login_input vaM ml10";
			userNameLabel.style.display = "";
			changeAccount.style.display = "none";
		}
	}
}
function level_special_steps(){
	var holders = [];
	var arrorws = [];
	for(var i=0; i<=3; i++){
		holders[i] = g('special_step_h_'+i);
		if(i<3){arrorws[i] = g('special_step_'+i);}
		if(i > 0){
			if(i<3){
				arrorws[i-1].style.height = (holders[i].offsetTop+arrorws[i].parentNode.offsetTop - 10) - (holders[i-1].offsetTop+arrorws[i-1].parentNode.offsetTop+arrorws[i-1].parentNode.offsetHeight+10)+'px';
			}
			else{
				arrorws[i-1].style.height = (holders[i].offsetTop - 10) - (holders[i-1].offsetTop+arrorws[i-1].parentNode.offsetTop+arrorws[i-1].parentNode.offsetHeight+10)+'px';
			}
		}
	}
}

function level_onetouch_steps(){
	for(var i=0; i<2; i++){
		var offset1 = $("#onet-dot-" + (i+1)).offset().top;
		var offset2 = $("#onet-dot-" + (i+2)).offset().top;
		var positionTop = $("#onet-dot-" + (i+1)).position().top;
		var circleHeight = $("#onet-dot-" + (i+1)).outerHeight();
		$("#oneTouch-banner-arrow-" + (i+1)).css("height", (offset2 - offset1 - circleHeight - 4));
		$("#oneTouch-banner-arrow-" + (i+1)).css("top", (positionTop + circleHeight + 2));
	}
}


//TO DO use global fnc
//video id: access level (1,2,3)
var vod_map = {
	1:1,4:1,12:2,13:2,5:3,6:3,51:3,
	7:3,8:3,23:3,22:3,36:3,40:3,43:3,44:3,
	9:1,10:1,11:1,35:1,24:1,25:1
}
function openPopupVOD(video,url){
	g('acad_vod_title_pop').innerHTML = g('acad_vod_title_'+video).innerHTML;
	g('acad_vod_text_pop').innerHTML = g('acad_vod_text_'+video).innerHTML;
	g('popupVOD').style.display = "block";
	if(g('bo_trading_links') != null){
		g('bo_trading_links').href = url;
	}
	if(vod_status == 1 && vod_status < vod_map[video]){
		g('reg_log_popUp_small').style.display = "block";
		g('deposit_popUp_small').style.display = "none";
		g('acad_vod_err_img_bg').style.display = "block";
		g('vin').style.display = "none";
	} else if(vod_status == 2 && vod_status < vod_map[video]) {
		g('reg_log_popUp_small').style.display = "none";
		g('deposit_popUp_small').style.display = "block";
		g('acad_vod_err_img_bg').style.display = "block";
		g('vin').style.display = "none";
	} else if(vod_status == 3 && vod_status < vod_map[video]) {
		g('reg_log_popUp_small').style.display = "none";
		g('deposit_popUp_small').style.display = "block";
		g('acad_vod_err_img_bg').style.display = "block";
		g('vin').style.display = "none";
	} else {
		g('reg_log_popUp_small').style.display = "none";
		g('deposit_popUp_small').style.display = "none";
		g('acad_vod_err_img_bg').style.display = "none";
		g('vin').style.display = "block";
	}
	g('vin').innerHTML = '<center><div id="vid_player">flash here</div></center>';
	loadVideo('vin',video);
}
function closePopupVOD(){
	g('popupVOD').style.display = "none";
}
function show_ebookPop(){
	if(vod_status == 1){
		g('regLogPop_ebook').style.display = "block";
		g('reg_log_popUp_content').style.display = "block";
		g('noDeposit_popUp_content').style.display = "none";
	}else{
		g('acad_popUps').style.display = "block";
		g('reg_log_popUp_content').style.display = "none";
		g('noDeposit_popUp_content').style.display = "none";
	}
}
function close_eBookPop(){
	g('regLogPop_ebook').style.display = "none";
	g('acad_popUps').style.display = "none";
}
function guidesVideoHover(el,show){
	if(show){
		if(el.getAttribute('data-active') != ""){
			g('acad_tr_guides_main_img_h').style.display = "block";
		}
		el.onmouseout = function(){
			guidesVideoHover(el,false);
		}
	}else{
		g('acad_tr_guides_main_img_h').style.display = "";
	}
}
function showVideo(){
	var title = g('trading_guide_title').innerHTML;
	g('acad_tr_guides_header').innerHTML = title;
	g('acad_vod_txt_header').innerHTML = title;
	g('acad_vod_txt').innerHTML = g('trading_guide_text').innerHTML;
	g('acad_popUps_bgr').style.display = "block";
	g('acad_popUps').style.display = "block";
}
function closeVideo(){
	g('acad_popUps_bgr').style.display = "none";
	g('acad_popUps').style.display = "none";
}
var eBook_map ={2:2,3:3,4:3}
function openPopup_eBook(book){
	if(vod_status == 1 && vod_status < eBook_map[book]){
		g('regLogPop_ebook').style.display = "block";
		g('reg_log_popUp_content').style.display = "block";
		g('noDeposit_popUp_content').style.display = "none";
		return false;
	}
	else if(vod_status == 2 && vod_status < eBook_map[book]){
		g('regLogPop_ebook').style.display = "block";
		g('reg_log_popUp_content').style.display = "none";
		g('noDeposit_popUp_content').style.display = "block";
		return false;
	}
}
//trading box
var OPPORTUNITY_STATE = {
	'created'			: 1,						//done
	'opened'			: 2,//open
	'last_10_min'		: 3,//open
	'closing_1_min'		: 4,//w8ting for expiry
	'closing'			: 5,//w8ting for expiry
	'closed'			: 6,//expired
	'done'				: 7,//delete				//done
	'paused'			: 8,						//done
	'suspended'			: 9,						//done
	'waiting_to_pause'  : 10,						//done
	'hidden_waiting_to_expiry'  : 11,
	'binary0100_updating_prices'  : 12
}
var SUBSCR_FLD = {
	'key'						: 'key',//1
	'command'					: 'command',//2
	'et_name'					: 'ET_NAME',//3
	'ao_level'					: level_skin_group_id,//4
	'et_odds'					: 'ET_ODDS',//5
	'et_est_close'				: 'ET_EST_CLOSE',//6
	'et_opp_id'					: 'ET_OPP_ID',//7
	'et_odds_win'				: 'ET_ODDS_WIN',//8
	'et_odds_lose'				: 'ET_ODDS_LOSE',//9
	'et_state'					: 'ET_STATE',//10
	'ao_clr'					: level_color,//11
	'ao_clr_dsp'				: 'ao_clr',//11
	'priority'					: 'ET_PRIORITY',//12
	'et_group_close'			: 'ET_GROUP_CLOSE',//13
	'et_scheduled'				: 'ET_SCHEDULED',//14
	'et_rnd_floor'				: 'ET_RND_FLOOR',//15
	'et_rnd_ceiling'			: 'ET_RND_CEILING',//16
	'et_group'					: 'ET_GROUP',//17
	'et_suspended_message'		: 'ET_SUSPENDED_MESSAGE',//18
	'ao_markets'				: 'AO_HP_MARKETS_' + skinId,//19
	'ao_states'					: 'AO_STATES',//20
	'last_invest'				: 'ET_LAST_INV',//22
	'time_stamp'				: 'AO_TS',//23
	'ao_flags'					: 'AO_FLAGS',//24
	'ao_flags_dsp'				: 'ao_flags',
	'odds_group'				: 'ET_ODDS_GROUP',//25
	'ao_calls_trend'			: ao_calls_trend,//26
	'ao_calls_trend_dep'		: 'ao_calls_trend',
	'mobile_market_list_index'	: 'mmListIndex',
	'filter_new_scheduled'		: 'FILTER_NEW_SCHEDULED',
	'bz_offer'					: 'BZ_OFFER',
	'bz_bid'					: 'BZ_BID',
	'bz_state'					: 'BZ_STATE',
	'bz_current'				: 'BZ_CURRENT',
	'bz_suspended_message'		: 'BZ_SUSPENDED_MESSAGE',
	'ao_opp_state'				: 'AO_OPP_STATE',
	'bz_event'					: 'BZ_EVENT',
	'bz_last_inv'				: 'BZ_LAST_INV',
	'bz_est_close'				: 'BZ_EST_CLOSE',
	'bz_market_id'				: 'BZ_MARKET_ID',
	'ask'						: 'ASK',
	'bid'						: 'BID',
	'last'						: 'LAST'
}

// var ELEMENT_TYPE_TABLE					= 0;
// var ELEMENT_TYPE_ROW					= 1;
// var ELEMENT_TYPE_CELL					= 2;

var ls_marketStates = new Array();//marketStates
var ls_marketsTimestamp = 0;//marketsTimestamp
var ls_isNotLogInError = false;//isNotLogInError - TO DO
var ls_strHref = window.location.href;//strHref
var fromGraph = 0; //No from LIVE-AO, profitline, option + or binary 0-100
var isLiveAOPage = false;

var markestInBox = new Array();//markets
function marketToBox(marketId, reseted){
	for(var i=0; i<box_objects.length; i++){
		if(box_objects[i] != null || box_objects[i] != undefined){
			if (!isUndefined(reseted) && reseted) {
				if(box_objects[i].market.reset_market_id == marketId){
					return i;
				}
			} else {
				if(box_objects[i].market.market_id == marketId){
					return i;
				}
			}
		}
	}
	return -1;
}
function checkBoxesInvestment(){
	var legend = $('#binary-legend');
	for (var i=0; i<box_objects.length; i++) {
		if (!isUndefined(box_objects[i])) {
			if (box_objects[i].allInvestments.length > 0) {
				legend.show();
				return true;
			}
		}
	}
	legend.hide();
	return false;
}
var box_objects = new Array();
function box_obj(params){
	try{
		logIt({'type':1,'msg':'init box: '+params.box});
		var self = this;
		this.logOutOffset = null;
		this.chart = null;
		this.box = params.box;
		this.boxType = params.boxType;
		this.assetPage = params.assetPage;
		new callPut_sw(this.box);
		
		this.boxFull = false;
		this.submited = false;
		this.inProgress = false;
		this.optionChanged = true;//if on update the oppId is the same
		this.oppId = 0;
		this.oppArrPos = 0;
		this.marketChanged = false;

		this.market = {};//boxesMarkets
		this.boxOpportunities = [];//boxesOpportunities {'ET_OPP_ID':124323423,'ET_STATE':'DELETE'}
		
		this.if_correct = 0;
		this.if_not_currect = 0;
		this.invest = parseFloat(params.invest.replace(/,/g,''));//amount to invest
		this.profit = 0;
		this.refund = 0;
		this.serverOffset = new Date().getTimezoneOffset()*60*1000*(-1);
		this.timeLastInv_mil = 0;
		this.timeClosing_mil = 0;
		this.chartPointsInv = [];
		this.chartPointsInv_last = {};
		this.lastSecondsToInvest = 600;//seconds to closing 10*60
		this.seconds_to_quot = 4000;//milisconds to quoat 4*1000
		
		this.allPoints = [];
		this.allPointsInv = [];
		this.allInvestments = [];
		this.marketId = 0;
		this.marketIdManual = false;
		this.tradeBox_level_html = g('tradeBox_level_'+this.box);
		
		this.firstZoom = false;
		this.chartVissibleArea = {start:0,start_zoomed:0,end:0};
		this.stateReallyChanged = null;
		this.decimalPoint = 3;
		this.duplicatedTo = null;
		this.lastUpdateInfo = null;
		this.investmentId = null;
		this.delayChartHistory = false;
		this.chart_properties = {
			height:								165,
			yAxis_1:							160,
			band_bgr:							'chart_plotBand.png',
			band_bgr_width:						14,
			band_bgr_height:					6,
			popUp_fsize1:						'11px',
			popUp_fsize2:						'11px',
			popUp_fsize3:						'9px',
			chart_lineColor:					'#0284d6',
			chart_win:							'#fabd00',
			chart_lose:							'#000000',
			chart_noChange:						'#04618e',
			chart_current:						'#093856',
			chart_current_with_inv:				'white',
			chart_plotBand_inTheMoney:			'#093755',
			chart_plotBand_inTheMoney_current:	'#43abef',
			chart_plotBand_outTheMoney:			'#305b77',
			chart_plotBand_outTheMoney_current:	'#959595',
			chart_marker:						'#7aab53',
			chart_marker_hover:					'#7aab53',
			chart_marker_with_inv:				'#7aab53',
			chart_marker_hover_with_inv:		'#7aab53',
			timer_bgrColor:						'#c2e1f4',
			timer_timeElapsedColor:				'#3d98d4',
			timer_bgrLastTenColor:				'#e74310',
			timer_timeLastTenElapsedColor:		'#c0360c',
			plotLinesColor:						'#e74310',
			plotLinesColor_with_inv:			'#e64310',
			plotLinesColor_inv:					'#f2b702',
			tooltip_borderColor:				'#156c96',
			tooltip_bgrColor:					'#fafdff',
			tooltip_timeTxtColor:				'#04618e',
			tooltip_assetTxtColor:				'#e74310',
			tooltip_levelTxtColor:				'#04618e',
			timeLineTxtColor:					'#04618e',
			timeLineTxtPosition:				-10,
			plotBandMouseOver:					'rgba(78,162,198,0.2)'
		};
		if(this.boxType != 1){
			this.chart_properties.height							= 285;
			this.chart_properties.yAxis_1							= 275;
			this.chart_properties.band_bgr							= 'chart_plotBand_big.png';
			this.chart_properties.band_bgr_width					= 24;
			this.chart_properties.band_bgr_height					= 11;
			this.chart_properties.popUp_fsize1						= '14px';
			this.chart_properties.popUp_fsize2						= '18px';
			this.chart_properties.timeLineTxtPosition				= -15;
			
			this.chart_properties.chart_plotBand_inTheMoney=			'#022838';
			this.chart_properties.chart_plotBand_inTheMoney_current=	'#0675a3';
			this.chart_properties.chart_plotBand_outTheMoney=			'#2e2e2e';
			this.chart_properties.chart_plotBand_outTheMoney_current=	'#777777';
			
			this.chart_properties.chart_lose=							'#cacbcb';
			this.chart_properties.plotLinesColor_with_inv=				'#5cb3ed';
		}
		
		this.chart_point_properties = {
			enabled: true,
			symbol: 'square',
			fillColor: this.chart_properties.chart_current,
			lineColor: this.chart_properties.chart_current,
			radius: 3
		};
		
		g('tradeBox_error_footer_close_'+this.box).onclick = function(){
			handleMessages({'type':'other','errorCode':''});
		}
		//reset box params
		function resetBox(cleanState){
			self.boxFull = false;
			self.submited = false;
			self.inProgress = false;
			self.optionChanged = true;
			self.chartPointsInv = [];
			
			self.logOutOffset = null;
			
			self.profit = 0;
			self.refund = 0;
			self.timeLastInv_mil = 0;
			self.timeClosing_mil = 0;
			self.allPoints = [];
			self.allPointsInv = [];
			
			if (!isUndefined(chartHistory) && !isUndefined(chartHistory[self.box]) && !isUndefined(chartHistory[self.box].settings.opportunityId)) {
				self.oppId = chartHistory[self.box].settings.opportunityId;
			} else {
				self.oppId = 0;
				// self.marketId = 0;
				self.market.reset_market_id = 0;
			}
			
			self.alternateOpps = [];
			self.quorterlyOpps = [];
			self.marketChanged = false;
			
			if(cleanState){
				changeStates({'state':''});//clean loading
				resetGraph()//reset graph
			}
		}
		this.resetBox = resetBox;
		
		function getNewMarket() {
			var typeId = 0;
			switch(self.boxType) {
				case 1:
				case 2: typeId = 1;break;
				case 3: typeId = 3;break;
			}
			var url = context_path + "/jsp/xml/chartDataJson.jsf?marketId=0&typeId="+typeId+"&history=1";
				
			var ajaxCall = new aJaxCall_fn({'url':url,'callback':getNewMarketResponse});
			ajaxCall.doGet();
		}
		this.getNewMarket = getNewMarket;
		
		function getNewMarketResponse(data) {
			try{
				if (typeof data != 'object') {
					data = eval('('+ data +')');
				}
				chartHistory[self.box] = data;
				selectMarket(data.settings.marketId);
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//add default invest amount
		g('tradeBox_inv_amount_'+this.box).innerHTML = formatAmount(this.invest,3);
		
		//add onclick event to exp dropdown
		// this.addFnToExpDrop = addFnToExpDrop;
		function addFnToExpDrop(){
			try{
				// logIt({'type':1,'msg':'addFnToExpDrop box: '+self.box});
				var tradeBox_menu_exp = g('tradeBox_menu_exp_' + self.box);
				var lis = tradeBox_menu_exp.getElementsByTagName('li');
				for(var i=0; i<lis.length; i++){
					if (lis[i].getAttribute('data-value') != null) {
						lis[i].onclick = function() {
							if (!self.inProgress) {
								updateExpTimeOpp(this);
								// var type = '';
								// switch (this.getAttribute('data-value')) {
									// case '1': type = 'Nearest'; break;
									// case '2': type = 'EndDay'; break;
									// case '3': type = 'EndWeek'; break;
									// case '4': type = 'EndMnth'; break;
								// }
							}
						}
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		if(this.boxType == 1){
			addFnToExpDrop();
		}
		
		if(this.boxType == 3){
			var tradeBox_slipCommision = g('tradeBox_slipCommision_'+self.box);
			tradeBox_slipCommision.innerHTML = tradeBox_slipCommision.innerHTML.replace('{0}','<span id="tradeBox_slipCommision_in_'+self.box+'"></span>')
		}
		else{
			g('tradeBox_profit_drop_amount_' + self.box).onclick = function(){
				openElement({'span':this,'autoClose':true});
			}
		}
		var tradeBox_dev2_msg = g('tradeBox_dev2_msg_'+self.box);
		tradeBox_dev2_msg.innerHTML = tradeBox_dev2_msg.innerHTML.replace('{0}','<span id="tradeBox_dev2_msg_in_'+self.box+'"></span>')

		//updateExpTimeOpp
		//this.updateExpTimeOpp = updateExpTimeOpp;
		function updateExpTimeOpp(el){
			try{
				closeElement({'last':true});
				var scheduled = el.getAttribute('data-value');
				var nh_oppId = el.getAttribute('data-opp-id');
				if (nh_oppId === null || self.market.schedule != scheduled) {
					handleMessages({'type':'popup','errorCode':4});
					group[self.box] = self.market.type+"_" + scheduled + "_" + self.marketId;

					self.boxOpportunities = [];
					self.market.schedule = scheduled;
					resetBox(true);

					if (!isUndefined(nh_oppId)) {
						self.oppId = nh_oppId;
					}

					lsClient.unsubscribe(tradingBox);
					tradingBox = connectToLS({'name':'tradingBox','subscriptionType':subscriptionType,'group':group,'schema':schema,'dataAdapterName':dataAdapterName,'callBack':ls_updateItem});
				} else {
					var forceStateWaiting = false;
					self.resetBox(true);
					self.oppId = nh_oppId;
					
					self.oppArrPos = searchJsonKeyInArray(self.boxOpportunities,'ET_OPP_ID',self.oppId);
					
					self.updateVisual({'stateReallyChanged':true,'oppChanged':true,'callsTrendChanged':true,'aoFlagsChanged':true});//update with new oppId
					
					self.updateState(//changeBoxState
						{'oppId':self.oppId,
							'ao_level':self.boxOpportunities[self.oppArrPos][SUBSCR_FLD.ao_level],
							'et_state':self.boxOpportunities[self.oppArrPos][SUBSCR_FLD.et_state],
							'et_est_close':self.boxOpportunities[self.oppArrPos][SUBSCR_FLD.et_est_close],
							'last_invest':self.boxOpportunities[self.oppArrPos][SUBSCR_FLD.last_invest],
							'et_suspended_message':self.boxOpportunities[self.oppArrPos][SUBSCR_FLD.et_suspended_message],
							'forceStateWaiting': forceStateWaiting
						}
					);
					
					if(self.boxType != 1){
						tradeBox_getBalace(self.box);
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		//add onclick event to invest dropdpown
		// this.addFnToInvestDrop = addFnToInvestDrop;
		function addFnToInvestDrop(){
			try{
				logIt({'type':1,'msg':'addFnToInvestDrop box: '+self.box});
				var lis = g('tradeBox_invest_drop_ul_'+self.box).getElementsByTagName('li');
				var flag = true;
				for(var i=0; i<lis.length; i++){
					if(lis[i].getAttribute('data-amount') != null){
						var amount = parseFloat(lis[i].getAttribute('data-amount').replace(/,/g,''));
						lis[i].innerHTML = formatAmount(amount,2);
						lis[i].setAttribute('data-i', i);
						lis[i].onclick = function(){
							updateInvest(this);
						}
						if(amount == self.invest){
							lis[i].className += " active";
							var flag = false;
						}
					}
				}
				//fill custom amount
				// if(flag){
					// var tradeBox_invest_drop_custom = g('tradeBox_invest_drop_custom_'+self.box);
					// tradeBox_invest_drop_custom.parentNode.className += " active";
					// tradeBox_invest_drop_custom.value = self.invest;
				// }
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		addFnToInvestDrop();
		
		//update invest amount
		// this.updateInvest = updateInvest;
		function updateInvest(el){
			try{
				logIt({'type':1,'msg':'updateInvest box: '+self.box});
				self.invest = parseFloat(el.getAttribute('data-amount').replace(/,/g,''));
				g('tradeBox_inv_amount_'+self.box).innerHTML = formatAmount(self.invest,3);
				updateProfitAmount();
				clearActiveList(el.parentNode.getElementsByTagName('li'));
				el.className += " active";
				var tradeBox_invest_drop_custom = g('tradeBox_invest_drop_custom_'+self.box);
				tradeBox_invest_drop_custom.parentNode.className = tradeBox_invest_drop_custom.parentNode.className.replace(/ active/g,'');
				tradeBox_invest_drop_custom.value = tradeBox_invest_drop_custom.defaultValue;
				closeElement({'last':true});
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//add onclick event to invest dropdpown
		// this.addFnToProfitDrop = addFnToProfitDrop;
		function addFnToProfitDrop(){
			try{
				logIt({'type':1,'msg':'addFnToProfitDrop box: '+self.box});
				var invest_lis = g('tradeBox_profit_drop_ul_'+self.box).getElementsByTagName('li');
				var n = 1;
				for(var i=0; i<invest_lis.length; i++){
					if(invest_lis[i].getAttribute('data-profit') != null){
						invest_lis[i].setAttribute('data-i', n);
						n++;
						invest_lis[i].onclick = function(){
							updateProfit(this);
						}
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//update invest amount
		// this.updateProfit = updateProfit;
		function updateProfit(el){
			try{
				logIt({'type':1,'msg':'updateProfit box: '+self.box});
				self.profit = parseFloat(el.getAttribute('data-profit'));
				self.refund = parseFloat(el.getAttribute('data-refund'));
				g('tradeBox_profit_procent_'+self.box).innerHTML = self.profit+'%';
				updateProfitAmount();
				clearActiveList(el.parentNode.getElementsByTagName('li'));
				el.className += " active";
				closeElement({'last':true});
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}

		//update if win and loose amount
		function updateProfitAmount(){
			try{
				logIt({'type':1,'msg':'updateProfitAmount box: '+self.box});
				self.if_correct = self.invest+(self.invest*self.profit/100);
				g('tradeBox_profit_amount_'+self.box).innerHTML = formatAmount(self.if_correct,1);
				self.if_not_currect = self.invest*self.refund/100;
				g('tradeBox_incorrect_amount_'+self.box).innerHTML = formatAmount(self.if_not_currect,1);
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.updateProfitAmount = updateProfitAmount;
		
		//custom investment input
		g('tradeBox_invest_drop_custom_'+this.box).onkeyup = function(e){
			e = e || window.event;
			var code = (e.keyCode ? e.keyCode : e.which);
			if(code == 13){
				closeElement({'last':true});
				return false;
			}
			if(isLeftCurrencySymbol){
				this.value = currencySymbol + checkValue(this.value);
			}
			else{
				// this.value = checkValue(this.value) + currencySymbol;
				this.value = checkValue(this.value);
			}
			if(this.value == currencySymbol){
				this.value = '';
			}
			
			if(this.value != "" && this.value != currencySymbol){
				customInvAmount(this);
			}
		}
		g('tradeBox_invest_drop_custom_'+this.box).onclick = function() {
			this.value = '';
		}
		// this.customInvAmount = customInvAmount;
		function customInvAmount(el){
			try{
				logIt({'type':1,'msg':'customInvAmount box: '+self.box});
				self.invest = parseFloat(el.value.replace(currencySymbol,''));
				g('tradeBox_inv_amount_'+self.box).innerHTML = formatAmount(self.invest,3);
				updateProfitAmount();
				clearActiveList(el.parentNode.parentNode.getElementsByTagName('li'));
				el.parentNode.className += " active";
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//popup errors
		this.currentErrors = {'pop':'hidden','other':'','chartHeader':'hidden'};
		//[type:popup]0:no error,1:not available,2:login message,3:deposit message,4:loading; 
		//[type:other]0:no error,1:invest error,2:wait for exp,3:invest confirm 
		//[type:chartHeader]1:time_invest,2:exp_level,3:waiting_for_exp
		function handleMessages(params){
			try{
				logIt({'type':1,'msg':{'msg':'handleMessages box: '+self.box,'params':params}});
				if(params.type == 'popup'){
					var holder = g('tradeBox_pop_holder_'+self.box);
					var errorsPop = 'hidden';
					switch(params.errorCode){
						case 1:
							errorsPop = 'notAv';
							holder.className = holder.className.replace(self.currentErrors.pop,errorsPop);
							self.currentErrors.pop = errorsPop;
							g('tradeBox_pop_notAv_'+self.box).innerHTML = g('tradeBox_pop_notAv_'+self.box).innerHTML.replace('{expTime}',params.expTime);
							break;
						case 5:
							errorsPop = 'suspended';
							holder.className = holder.className.replace(self.currentErrors.pop,errorsPop);
							self.currentErrors.pop = errorsPop;
							g('tradeBox_pop_suspended_'+self.box).innerHTML = params.errorMsg;
							break;
						case 2:
							errorsPop = 'login';
							holder.className = holder.className.replace(self.currentErrors.pop,errorsPop);
							self.currentErrors.pop = errorsPop;
							break;
						case 3:
							errorsPop = 'noDeposit';
							holder.className = holder.className.replace(self.currentErrors.pop,errorsPop);
							self.currentErrors.pop = errorsPop;
							break;
						case 4:
							errorsPop = 'loading';
							holder.className = holder.className.replace(self.currentErrors.pop,errorsPop);
							self.currentErrors.pop = errorsPop;
							resetBox(false);
							break;
						case 6:
							errorsPop = 'dev2';
							holder.className = holder.className.replace(self.currentErrors.pop,errorsPop);
							self.currentErrors.pop = errorsPop;
							g('tradeBox_dev2_msg_in_' + self.box).innerHTML = numberWithCommas(params.level);
							startDEV2Timer(params.time);
							break;
						case 7:
							errorsPop = 'cashDeposit';
							holder.className = holder.className.replace(self.currentErrors.pop,errorsPop);
							var cashDepositMsg = g('tradeBox_pop_cashDeposit_msg_' + self.box);
							cashDepositMsg.innerHTML = cashDepositMsg.getAttribute('data-msgs').replace('{0}', params.errorMsg);
							self.currentErrors.pop = errorsPop;
							break;
						default:
							errorsPop = 'hidden';
							holder.className = holder.className.replace(self.currentErrors.pop,errorsPop);
							self.currentErrors.pop = errorsPop;
							break;
					}
				}
				else if(params.type == 'other'){
					if(self.currentErrors.other != ""){
						self.currentErrors.other.style.display = "none";
					}
					if (!isUndefined(self.currentErrors.jEl)) {
						self.currentErrors.jEl.removeClass(self.currentErrors.jCLass);
						self.currentErrors.jEl = null;
					}
					switch(params.errorCode){
						case 1:
							var tmp = g('tradeBox_error_footer_'+self.box);
							tmp.style.display = "block";
							g('tradeBox_error_footer_txt_'+self.box).innerHTML = params.errorMsg;
							self.currentErrors.other = tmp;
							clearTimeout(self.closeSlipTimer);
							self.closeSlipTimer = setTimeout(function(){handleMessages({'type':'other','errorCode':''});},5000);
							break;
						case 2:
							var tmp = g('tradeBox_waiting_for_exp_'+self.box);
							tmp.style.display = "block";
							self.currentErrors.other = tmp;
							break;
						case 3:
							var tmp = g('tradeBox_after_invest_'+self.box);
							tmp.style.display = "block";
							self.currentErrors.other = tmp;
							investConfirmFill(params.result);
							break;
						case 4:
							var tmp = $('#chart-overlays_' + self.box);
							tmp.addClass('cancel-inv');
							self.currentErrors.jEl = tmp;
							self.currentErrors.jCLass = 'cancel-inv';
							cancelInvFill(params.result);
							break;
						case 5:
							var tmp = $('#chart-overlays_' + self.box);
							tmp.addClass('cancel-inv-msg');
							self.currentErrors.jEl = tmp;
							self.currentErrors.jCLass = 'cancel-inv-msg';
							break;
						case 6:
							var tmp = $('#chart-overlays_' + self.box);
							tmp.addClass('cancel-inv-error');
							self.currentErrors.jEl = tmp;
							self.currentErrors.jCLass = 'cancel-inv-error';
							break;
					}
				}
				else if(params.type == 'chartHeader'){
					var holder = g('tradeBox_chartHeader_'+self.box);
					var chartHeader = 'hidden';
					switch(params.errorCode){
						case 1:
							// if(self.timeLeftToInvest < 60*60){
								chartHeader = 'time_invest';
								holder.className = holder.className.replace(self.currentErrors.chartHeader,chartHeader);
								self.currentErrors.chartHeader = chartHeader;
							// }
							break;
						case 2:
							chartHeader = 'exp_level';
							holder.className = holder.className.replace(self.currentErrors.chartHeader,chartHeader);
							self.currentErrors.chartHeader = chartHeader;
							g('tradeBox_exp_level_value_'+self.box).innerHTML = params.ao_level;
							break;
						case 3:
							chartHeader = 'waiting_for_exp';
							holder.className = holder.className.replace(self.currentErrors.chartHeader,chartHeader);
							self.currentErrors.chartHeader = chartHeader;
							break;
						default:
							chartHeader = 'hidden';
							holder.className = holder.className.replace(self.currentErrors.chartHeader,chartHeader);
							self.currentErrors.chartHeader = chartHeader;
							break;
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.handleMessages = handleMessages;
		//loading by default
		handleMessages({'type':'popup','errorCode':4});
		//add close fnc
		g('tradeBox_pop_login_close_'+this.box).onclick = function(){
			handleMessages({'type':'popup'});
		}
		g('tradeBox_pop_noDeposit_close_'+this.box).onclick = function(){
			handleMessages({'type':'popup'});
		}
		g('tradeBox_pop_cashDeposit_close_'+this.box).onclick = function(){
			handleMessages({'type':'popup'});
		}
		
		//investConfirmFill
		this.closeSlipTimer = null;
		function investConfirmFill(result){
			try{
				logIt({'type':1,'msg':{'msg':'investConfirmFill box: '+self.box,'result':result}});
				var tradeBox_AI_type = g('tradeBox_AI_type_'+self.box);
				tradeBox_AI_type.innerHTML = (self.chartPointsInv_last.type == 1)?tradeBox_AI_type.getAttribute('data-call'):tradeBox_AI_type.getAttribute('data-put');
				switch(result.color){
					case '0':var color = " below";break;
					case '1':var color = " same";break;
					case '2':var color = " above";break;
				}
				g('tradeBox_AI_level_'+self.box).innerHTML = numberWithCommas(result.level);
				g('tradeBox_AI_level_'+self.box).className = color;
				g('tradeBox_AI_expires_'+self.box).innerHTML = formatDate(new Date(self.boxOpportunities[self.oppArrPos].ET_EST_CLOSE), false);
				g('tradeBox_AI_option_id_'+self.box).innerHTML = self.investmentId;
				if(self.boxType == 3){
					var totalInv = self.invest+commsionFee;
				}else{
					var totalInv = self.invest;
				}
				g('tradeBox_AI_amount_'+self.box).innerHTML = formatAmount(totalInv);
				g('tradeBox_AI_correct_'+self.box).innerHTML = formatAmount(self.if_correct,0);
				g('tradeBox_AI_incorrect_'+self.box).innerHTML = formatAmount(self.if_not_currect,0);
				g('tradeBox_sms_'+self.box).style.display = (result.sms == 0)?'none':'block';
				g('tradeBox_sms_inp_'+self.box).checked = false;
				g('tradeBox_sms_inp_'+self.box).disabled = false;
				var tradeBox_sms_txt = g('tradeBox_sms_txt_'+self.box);
				tradeBox_sms_txt.innerHTML = tradeBox_sms_txt.getAttribute('data-defaultMsg');
				g('tradeBox_sms_inp_'+self.box).onclick = function(){
					updateInvestSms();
				}
				if(self.boxType == 3){
					g('tradeBox_slipCommision_in_'+self.box).innerHTML = commsionFeeWithSign;
					g('tradeBox_sms_'+self.box).style.display = "none";
				}
				clearTimeout(self.closeSlipTimer);
				self.closeSlipTimer = setTimeout(function(){handleMessages({'type':'other'});},10000);
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.investConfirmFill = investConfirmFill;

		//cancelInvFill
		g('submit-link_' + this.box).onclick = function() {
			cancelInv();
		}
		
		function cancelInvFill(result) {
			try{
				logIt({'type':1,'msg':{'msg':'cancelInvFill box: '+self.box,'result':result}});
				
				startCancelTimeout();
				
				$('#cancel-inv-invest-arrow_' + self.box).removeClass('call');
				$('#cancel-inv-invest-arrow_' + self.box).removeClass('put');
				$('#cancel-inv-invest-arrow_' + self.box).addClass((self.currentStates === "call_processing") ? 'call' : 'put');
				
				$('#cancel-inv-level_' + self.box).html(numberWithCommas(result.level));
				
				if(self.boxType == 3){
					var totalInv = self.invest+commsionFee;
				}else{
					var totalInv = self.invest;
				}
				
				$('#cancel-inv-amount_' + self.box).html(formatAmount(totalInv));
				
				var comission = formatAmount(Math.floor((parseFloat(self.invest) * 0.1) * 100)/100, 2);
				$('#cancelInvText_' + self.box).html($('#cancelInvText_' + self.box).attr('data-text').replace('{amount}', comission));

			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.investConfirmFill = investConfirmFill;
		
		function startCancelTimeout() {
			var cancelTimeOut = self.cancelTimeOut * 1000;
			var end = new Date().getTime() + (cancelTimeOut);
			self.cancelTimeLeft = cancelTimeOut;
			function doIt(){
				self.cancelTimeLeftPercent = 100 - (self.cancelTimeLeft / cancelTimeOut) * 100;
				if (self.cancelTimeLeftPercent > 100) {self.cancelTimeLeftPercent = 100;}
				g('cancel-inv-time-left-percent_' + self.box).style.width = self.cancelTimeLeftPercent + '%';
				
				if (self.cancelTimeLeft > 0) {
					self.cancelTimeLeft = end - new Date().getTime();
					if (self.cancelTimeLeft < 0) {
						self.cancelTimeLeft = 0;
					}
					self.cancelInvTimerLoader = setTimeout(doIt, 10);
				}
				var seconds = Math.floor(self.cancelTimeLeft / 1000);
				if (seconds == 0) {
					seconds = '00';
				} else if (seconds < 10) {
					seconds = '0' + seconds;
				}
				var milSec = Math.round(self.cancelTimeLeft - (seconds * 1000));
				if (milSec == 0) {
					milSec = '000';
				} else if (milSec < 10) {
					milSec = '00' + milSec;
				} else if (milSec < 100) {
					milSec = '0' + milSec;
				}
				//g('cancel-inv-time-left-sec_' + self.box).innerHTML = '00:' + seconds + ':' + milSec;
				g('cancel-inv-time-left-sec_' + self.box).innerHTML = '00:' + seconds;
			}
			doIt();
		}
		this.startCancelTimeout = startCancelTimeout;
		
		this.cancelInvOneTime = false;
		function cancelInv() {
			if (!self.cancelInvOneTime) {
				self.cancelInvOneTime = true;
				clearTimeout(self.cancelInvTimer);
				clearTimeout(self.cancelInvTimerLoader);

				var InvestmentMethodResult = jsonClone(getUserMethodRequest(), {
					investmentId:self.investmentId,
					opportunityId: self.boxOpportunities[self.oppArrPos].ET_OPP_ID,
					amount: self.invest * Math.pow(10,currencyDecimalPoint),
					pageLevel: self.chartPointsInv_last.level,
					oddWin: (1+(self.profit/100)),
					oddLose: (self.refund/100),
					choice: self.chartPointsInv_last.type,
					fromGraph: fromGraph
				});
				$.ajax({
					url     : context_path + aoJsonUrl + "cancelInvestment",
					type    : "POST",
					dataType: "json",
					data    : JSON.stringify(InvestmentMethodResult),
					contentType: "application/json;charset=utf-8",
					success : function(data) {
						//Successful call
						if (data.errorCode == 0) {
							changeStates({'state':'cancel-inv-msg'});
							if(self.boxType != 1){
								tradeBox_getBalace(self.box);
							}
							else{
								refreshHeader();
							}
							self.cancelInvMsgsOverlayTimer = setTimeout(function() {
								hideCancelInvMsg();
							}, 3000);
							self.inProgress = false;
							self.cancelInvOneTime = false;
							refreshInvProfitBox();
						} else {
							self.inProgress = false;
							self.cancelInvOneTime = false;
							
							changeStates({'state':'confirm','sms':self.smsEnable,'level':self.chartPointsInv_last.level,'color':self.chartPointsInv_last.color});
							
							addInvToChart();
						}
					},
					error   : function( xhr, err ) {
						//Failed call
						self.inProgress = false;
						self.cancelInvOneTime = false;
						
						changeStates({'state':'confirm','sms':self.smsEnable,'level':self.chartPointsInv_last.level,'color':self.chartPointsInv_last.color});
						
						addInvToChart();
					}
				});
			}
		}
		
		g('cancel-cool-close_' + this.box).onclick = function() {
			hideCancelInvMsg();
		}
		
		function hideCancelInvMsg() {
			handleMessages({'type':'other'})
		}
		this.hideCancelInvMsg = hideCancelInvMsg;

		g('hide-forever_' + this.box).onclick = function() {
			disableCanacelInv();
		}
		
		function disableCanacelInv() {
			$.ajax({
				url     : context_path + aoJsonUrl + "changeCancelInvestmentCheckboxState",
				type    : "POST",
				dataType: "json",
				data    : JSON.stringify(getMethodRequest()),
				contentType: "application/json;charset=utf-8",
				success : function(data) {
					//Successful call
					showCnacelInv = false;
				},
				error   : function( xhr, err ) {
					//Failed call

				}
			});
		}
		this.disableCanacelInv = disableCanacelInv;
		
		
		//add onclick event to call and put
		// this.addDisableCallPutButn = addDisableCallPutButn;
		function addDisableCallPutButn(disable){
			try{
				logIt({'type':1,'msg':'addDisableCallPutButn box: '+self.box+' - disable: '+disable});
				if(!disable){
					g('tradeBox_call_btn_'+self.box).onclick = function(){
						changeStates({'state':'call_processing'});
					}
					g('tradeBox_put_btn_'+self.box).onclick = function(){
						changeStates({'state':'put_processing'});
					}
				}else{
					g('tradeBox_call_btn_'+self.box).onclick = function(){
						return false;
					}
					g('tradeBox_put_btn_'+self.box).onclick = function(){
						return false;
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		addDisableCallPutButn();
		
		//change call put button
		// this.chengeStatesBtn = chengeStatesBtn;
		this.currentCallState = 'active';
		this.currentPutState = 'active';
		function chengeStatesBtn(state){//state{active,call_processing,off,put_processing}
			try{
				logIt({'type':1,'msg':'chengeStatesBtn box: '+self.box+' - state: '+state});
				var call = g('tradeBox_call_btn_'+self.box);
				var put = g('tradeBox_put_btn_'+self.box);
				switch(state){
					case 'call_processing':
						call.className = call.className.replace(self.currentCallState,'proc_on');self.currentCallState = 'proc_on';
						put.className = put.className.replace(self.currentPutState,'proc_off');self.currentPutState = 'proc_off';
						addDisableCallPutButn(true);
						break;
					case 'put_processing':
						call.className = call.className.replace(self.currentCallState,'proc_off');self.currentCallState = 'proc_off';
						put.className = put.className.replace(self.currentPutState,'proc_on');self.currentPutState = 'proc_on';
						addDisableCallPutButn(true);
						break;
					case 'off':
						call.className = call.className.replace(self.currentCallState,'off');self.currentCallState = 'off';
						put.className = put.className.replace(self.currentPutState,'off');self.currentPutState = 'off';
						addDisableCallPutButn(true);
						break;
					default:
						call.className = call.className.replace(self.currentCallState,'active');self.currentCallState = 'active';
						put.className = put.className.replace(self.currentPutState,'active');self.currentPutState = 'active';
						addDisableCallPutButn(false);
						break;
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//change state
		this.currentStates = {'state':'','errorCode':0,'errorType':''};
		function changeStates(params){//state{default,call_processing,put_processing,error,confirm,waiting_for_exp,expired}
			try{
				logIt({'type':1,'msg':{'msg':'changeStates box: '+self.box,'result':params.result}});
				switch(params.state){
					case 'call_processing'://press call button
						shouldShowRegulationPopup("trade_call", function(){
							optimoveReportTradeEvent({from: "trade_call", box: self});
							chengeStatesBtn(params.state);
							disableInvest(true);
							if((self.chart != null) || (self.boxOpportunities.length > 0 && self.boxOpportunities[self.oppArrPos].ET_SCHEDULED != '1')){
								doInvest(1,false);
								self.currentStates = params.state;
							}
							else{
								changeStates({'state':''});
							}
						});
						break;
					case 'put_processing'://press put button
						shouldShowRegulationPopup("trade_put", function(){
							optimoveReportTradeEvent({from: "trade_put", box: self});
							chengeStatesBtn(params.state);
							disableInvest(true);
							if((self.chart != null) || (self.boxOpportunities.length > 0 && self.boxOpportunities[self.oppArrPos].ET_SCHEDULED != '1')){
								doInvest(2,false);
								self.currentStates = params.state;
							}
							else{
								changeStates({'state':''});
							}
						});
						break;
					case 'error'://invest returned error
						handleMessages({'type':params.errorType,'errorCode':params.errorCode,'errorMsg':params.errorMsg});
						disableInvest(false);
						chengeStatesBtn('active');
						self.currentStates = params.state;
						break;
					case 'confirm'://confirm invest
						chengeStatesBtn('active');
						handleMessages({'type':'other','errorCode':3,'result':{'sms':params.sms,'level':params.level,'color':params.color}});
						disableInvest(false);
						self.currentStates = params.state;
						break;
					case 'cancel-inv'://cancel inv
						chengeStatesBtn('active');
						handleMessages({'type':'other','errorCode':4,'result':{'level':params.level,'color':params.color}});
						disableInvest(false);
						self.currentStates = params.state;
						break;
					case 'cancel-inv-msg'://cancel inv msg
						chengeStatesBtn('active');
						handleMessages({'type':'other','errorCode':5});
						disableInvest(false);
						self.currentStates = params.state;
						break;
					case 'cancel-inv-error'://cancel inv error
						chengeStatesBtn('active');
						handleMessages({'type':'other','errorCode':6});
						disableInvest(false);
						self.currentStates = params.state;
						break;
					case 'waiting_for_exp'://waiting for expiry
						chengeStatesBtn('off');
						disableInvest(true);
						handleMessages({'type':'chartHeader','errorCode':3});
						self.currentStates = params.state;
						break;
					case 'expired'://expired
						chengeStatesBtn('off');
						disableInvest(true);
						handleMessages({'type':'chartHeader','errorCode':2,'ao_level':params.ao_level});
						self.currentStates = params.state;
						break;
					default:
						chengeStatesBtn('active');
						disableInvest(false);
						handleMessages({'type':'popup'});
						handleMessages({'type':'other'});
						handleMessages({'type':'chartHeader','errorCode':1});
						self.currentStates = params.state;
						break;
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.changeStates = changeStates;
		
		// disable drop downs for invest and profit
		// this.disableInvest = disableInvest;
		this.disableInvestCurrent = 'active';
		function disableInvest(disable){
			try{
				logIt({'type':1,'msg':'disableInvest box: '+self.box+' - disable: '+disable});
				var state = '';
				var tradeBox_bottom = g('tradeBox_bottom_'+self.box);
				var tradeBox_incorrect = g('tradeBox_incorrect_'+self.box);
				if(disable){
					state = 'disabled';
				}
				else{
					state = 'active';
				}
				tradeBox_bottom.className = tradeBox_bottom.className.replace(self.disableInvestCurrent,state);
				tradeBox_incorrect.className = tradeBox_incorrect.className.replace(self.disableInvestCurrent,state);
				self.disableInvestCurrent = state;
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}

		//invest
		// this.doInvest = doInvest;
		this.doInvestSubmitParams = '';
		function doInvest(type,dev2){
			try{
				if (!userLogin && $('#header').get(0).contentWindow.header_login) {
					location.reload();
				} else if (userLogin) {
					if(!dev2){
						self.submited = true;
						self.inProgress = true;
						clearTimeout(self.closeSlipTimer);
						handleMessages({'type':'other'});
						// if((self.market.schedule == '1') || (self.market.schedule == 2) || (self.boxType == 3)){
							var investLevel = self.boxOpportunities[self.oppArrPos].ao_level_float;
							var investColor = self.boxOpportunities[self.oppArrPos].ao_clr_dsp;
							self.chartPointsInv_last = {
								type: type,
								date: (self.chart != null) ? self.lastUpdate.x : '',
								level: investLevel,
								color: investColor,
								dev2: false,
								ask: self.boxOpportunities[self.oppArrPos].ASK,
								bid: self.boxOpportunities[self.oppArrPos].BID,
								last: self.boxOpportunities[self.oppArrPos].LAST
							};
						// }
						self.doInvestSubmitParams = "command=submit" +
									"&slipEntryId=" + self.boxOpportunities[self.oppArrPos].ET_OPP_ID +
									"&slipEntryLevel=" + self.chartPointsInv_last.level +
									"&slipEntryOddWin=" + (1+(self.profit/100)) +
									"&slipEntryOddLose=" + (self.refund/100) +
									"&choice=" + type +
									"&all=1" +
									"&amount=" + self.invest +
									"&utcOffset=" + new Date().getTimezoneOffset() +
									"&fromGraph=" + fromGraph;
					}else{
						self.doInvestSubmitParams += "&dev2Second=true";
						self.chartPointsInv_last.dev2 = true;
					}
					logIt({'type':1,'msg':'doInvest box: '+self.box+' - params: '+self.doInvestSubmitParams});
					var ajaxCall = new aJaxCall_fn({'type':'POST','url':context_path + '/ajax.jsf','params':self.doInvestSubmitParams,'callback':doInvestCallback});
					ajaxCall.doGet();
				} else {
					changeStates({'state':'error','errorType':'popup','errorCode':2});
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//invest callback
		// this.doInvestCallback = doInvestCallback;
		// status:{'0':'sthing','1':'not login','2':'some error','3':'OK','4':'redirect'}
		function doInvestCallback(response){
			try{
				logIt({'type':1,'msg':{'msg':'doInvestCallback box: '+self.box,'response':response}});
				if(response.substring(0, 4) == "DEV2"){
					self.investmentId = 0;
					var splitStr = response.split('_');
					var timeDEV2 = splitStr[0].split('=');
					if (!showCnacelInv) {
						handleMessages({'type':'popup','errorCode':6,'level':self.chartPointsInv_last.level,'time':timeDEV2[1]});
						doInvest('',true);
					} else {
						self.cancelTimeOut = (parseInt(cancelInvTimeOut) > parseInt(timeDEV2[1])) ? cancelInvTimeOut : timeDEV2[1];
						changeStates({'state':'cancel-inv','level':self.chartPointsInv_last.level,'color':self.chartPointsInv_last.color});

						self.cancelInvTimer = setTimeout(function() {
							doInvest('',true);
						}, self.cancelTimeOut * 1000);
					}
				}else if(response == "redirectToTerms"){
					window.location = context_path + "/jsp/terms.jsf?from=tradePage";
				}else{
					handleMessages({'type':'popup'});
					self.submited = false;
					self.tradeBox_level_html.innerHTML = self.boxOpportunities[self.oppArrPos].ao_level;
					if(response.length > 2){
						if(response.substring(0, 2) == "OK"){//3
							self.cancelTimeOut = cancelInvTimeOut;
							if(self.boxType != 1){
								tradeBox_getBalace(self.box);
							}
							else{
								refreshHeader();
							}

							clearTimeout(self.investOverlayTimer);
							
							refreshInvProfitBox();
							self.smsEnable = 0;
							var investmentId = response.substring(2);
							if (investmentId.indexOf("SMS=") > -1) {
								investmentId = investmentId.substring(0, investmentId.length - 5);
								// check sms field
								if (response.substring(response.length-1) == "1") {
									self.smsEnable = 1;
								}
							}
							self.investmentId = investmentId;
							
							if (!showCnacelInv || self.chartPointsInv_last.dev2) {
								changeStates({'state':'confirm','sms':self.smsEnable,'level':self.chartPointsInv_last.level,'color':self.chartPointsInv_last.color});
								
								addInvToChart();
							} else {
								changeStates({'state':'cancel-inv','level':self.chartPointsInv_last.level,'color':self.chartPointsInv_last.color});

								self.cancelInvTimer = setTimeout(function() {
									changeStates({'state':'confirm','sms':self.smsEnable,'level':self.chartPointsInv_last.level,'color':self.chartPointsInv_last.color});
									
									addInvToChart()
									
								}, self.cancelTimeOut * 1000);
							}
							
						}
						else if(response.substring(0, 1) == "1"){//1
							changeStates({'state':'error','errorType':'popup','errorCode':2});
							refreshHeader();
							self.inProgress = false;
						}
						else if(response.substring(0, 9) == "NODEPOSIT"){//no deposit
							changeStates({'state':'error','errorType':'popup','errorCode':3});
							self.inProgress = false;
						}
						else if(response.substring(0, 16) == "NO_CASH_FOR_NIOU"){//cash deposit
							changeStates({'state':'error','errorType':'popup','errorCode':7,'errorMsg':formatAmount(response.replace('NO_CASH_FOR_NIOU=', ''), 2, true)});
							self.inProgress = false;
						}
						else if(response.substring(0, 10) == "DEPOSITLOW"){//not enought money
							changeStates({'state':'error','errorType':'popup','errorCode':3});
							self.inProgress = false;
						}
						else{//2
							if (self.chartPointsInv_last.dev2) {
								changeStates({'state':'cancel-inv-error'});
								setTimeout(function() {
									$('#chart-overlays_' + self.box).removeClass('cancel-inv-error');
								}, 2000);
							} else {
								changeStates({'state':'error','errorType':'other','errorCode':1,'errorMsg':response.replace('ERROR-', '')});
							}
							self.inProgress = false;
						}
					}else{//0
						changeStates({'state':'error','errorType':'other','errorCode':1,'errorMsg':response});
						self.inProgress = false;
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		function addInvToChart() {
			self.chartPointsInv_last.point = 'point_'+self.investmentId;
			addPointToChart(
				self.chartPointsInv_last.date,
				self.chartPointsInv_last.level,
				self.chartPointsInv_last.point,
				self.chartPointsInv_last.ask,
				self.chartPointsInv_last.bid,
				self.chartPointsInv_last.last,
				1
			);
			self.chartPointsInv.push(self.chartPointsInv_last);
			redrowChart();

			chartGetHistory(1,true);
			
			if((self.boxType != 3) && window.lsOpenPositionsConfig && lsOpenPositionsConfig.investCallback){
				lsOpenPositionsConfig.investCallback(self);
			}
			
			setDefaultValueToAllBoxes();
			self.inProgress = false;
		}
		
		function startDEV2Timer(time){
			try{
				var seconds_to_wait = time * 1000;
				var bgr = g('tradeBox_dev2_progress_'+self.box);
				var date_end = new Date().getTime() + seconds_to_wait;
				var time_left = seconds_to_wait;
				function doIt(){
					bgr.style.width = 100-(time_left/seconds_to_wait)*100+'%';
					
					if(time_left > 0){
						var interval = 10;
						time_left = date_end - new Date().getTime();
						self.dev2Timer = setTimeout(doIt,interval);
					}
				}
				doIt();
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		function setDefaultValueToAllBoxes(){
			for(var box=0; box<group.length; box++){
				if(box != self.box){
					var lis = g('tradeBox_invest_drop_ul_'+box).getElementsByTagName('li');
					clearActiveList(lis);
					for(var i=0; i<lis.length; i++){
						if(lis[i].getAttribute('data-amount') != null){
							var amount = parseFloat(lis[i].getAttribute('data-amount').replace(/,/g,''));
							if(amount == self.invest){
								lis[i].className += " active";
								var flag = false;
							}
						}
					}
					g('tradeBox_inv_amount_'+box).innerHTML = formatAmount(self.invest,3);
					box_objects[box].invest = self.invest;
					box_objects[box].updateProfitAmount();
				}
			}
		}
		//time left
		this.time_invest_d = g('tradeBox_time_invest_d_'+this.box);
		this.time_invest_h = g('tradeBox_time_invest_'+this.box);
		// this.updateTimeLeft = updateTimeLeft;
		this.timeLeftTimer_var = null;
		function updateTimeLeft(){
			try{
				self.timeLastInv_mil = adjustFromUTCToLocal(new Date(self.boxOpportunities[self.oppArrPos].ET_LAST_INV)).getTime();
				self.timeClosing_mil = adjustFromUTCToLocal(new Date(self.boxOpportunities[self.oppArrPos].ET_EST_CLOSE)).getTime();
				
				if (self.optionChanged && self.chart) {
					self.chartVissibleArea.start = self.chartVissibleArea.start_zoomed = self.timeClosing_mil-(1000*60*60)+self.serverOffset;
					self.chartVissibleArea.end = self.timeClosing_mil+(1000*60*10)+self.serverOffset;
	
					setChartExtremes(self.chartVissibleArea.start, self.chartVissibleArea.end);
				}

				clearTimeout(self.timeLeftTimer_var);
				timeLeftTimer();
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		function timeLeftTimer(){
			try{
				//time left in miliseconds
				self.timeLeftToInvest = (self.timeLastInv_mil - new Date().getTime() - serverOffsetMillis)/1000;
				
				if (self.timeLeftToInvest < 0) {
					self.timeLeftToInvest = 0;
				}
				
				//minutes
				var min = Math.floor(self.timeLeftToInvest / 60);
				//hours
				var hours = 0;
				if (min > 60) {
					hours = Math.floor(min / 60);
					min = min - (hours * 60);
				}
				//days
				var days = 0;
				if (hours > 23) {
					days = Math.ceil(hours / 24);
					hours = hours - (days * 24);
				}
				
				if (min < 10){
					var minDsp = '0'+min;
				} else {
					var minDsp = min;
				}
				
				//seconds
				var sec = Math.floor(self.timeLeftToInvest - (min * 60));
				var secInt = sec;
				if (sec < 10) {
					sec = '0' + sec;
				}

				if (days > 0) {
					var day_txt
					if (days == 1) {
						day_txt = txts.day;
					} else {
						day_txt = txts.days;
					}
					self.time_invest_d.innerHTML = '<span class="in">' + days + '</span>' + day_txt;
				} else if (hours > 0) {
					self.time_invest_d.innerHTML = '<span class="in">' + hours + ":" + minDsp + '</span>' + txts.hours;
				} else {
					self.time_invest_d.innerHTML = '<span class="in">' + minDsp + ":" + sec + '</span>' + txts.minutes;
				}
				
				self.time_invest_h.className = self.time_invest_h.className.replace(/ lastMin/g,'');
				
				if(self.boxOpportunities.length > 0){
					if((self.boxOpportunities[self.oppArrPos].ET_STATE == OPPORTUNITY_STATE.opened) || (self.boxOpportunities[self.oppArrPos].ET_STATE == OPPORTUNITY_STATE.last_10_min) || 
						(self.boxOpportunities[self.oppArrPos].ET_STATE == OPPORTUNITY_STATE.created)){
						if(((min == 0) && (secInt == 0)) || (min > 60) || !userLogin){
							handleMessages({'type':'chartHeader','errorCode':''});
						}
						else if(self.timeLeftToInvest <= self.lastSecondsToInvest){
							self.time_invest_h.className += " lastMin";
							handleMessages({'type':'chartHeader','errorCode':1});
						}
						if((min != 0) && (secInt != 0) && userLogin){
							handleMessages({'type':'chartHeader','errorCode':1});
						}
					}
				}
				self.timeLeftTimer_var = setTimeout(timeLeftTimer,1000);
				updateChartTimeLeft();
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//receive update from ls
		function updateState(params){
			try{
				if (params.oppId == self.boxOpportunities[self.oppArrPos].ET_OPP_ID){//make changes only if the update is for the current oppId
					logIt({'type':1,'msg':"state changed - id: " + params.oppId + " marketId: " + self.marketId + " state: " + params.et_state + " time: " + params.last_invest + " timeLastInvest: " + params.et_est_close + " suspendMessage: " + params.et_suspended_message});
				
					if(params.et_state == OPPORTUNITY_STATE.created || params.et_state == OPPORTUNITY_STATE.paused || params.et_state == OPPORTUNITY_STATE.waiting_to_pause 
						|| self.boxOpportunities[self.oppArrPos].FILTER_NEW_SCHEDULED === '00000'){//state 1,8,10 or all opportunities are closed
						handleMessages({'type':'popup','errorCode':1,'expTime':formatDate(new Date(params.et_est_close), false)});
						// self.marketIdManual = false;
						self.stateDps = 'inactive';
					}
					else if(params.et_state >= OPPORTUNITY_STATE.opened && params.et_state <= OPPORTUNITY_STATE.closed){//between state 2 and 6
						if((params.et_state == OPPORTUNITY_STATE.opened) || (params.et_state == OPPORTUNITY_STATE.last_10_min)){//state 2,3 - open
							self.stateDps = 'active';
							if (params.et_state == OPPORTUNITY_STATE.last_10_min) {
								self.stateDps = 'before-expiry';
							}
							if(!self.submited){
								changeStates({'state':''});
							}
						}
						else if((params.et_state == OPPORTUNITY_STATE.closing_1_min) || (params.et_state == OPPORTUNITY_STATE.closing)){//state 4,5 - waiting for expiry
							if (!isUndefined(self.allInvestmentsForOpp) && self.allInvestmentsForOpp.length == 0 && self.alternateOpps.length > 0 && !params.forceStateWaiting) {
								setNextOpp();
							} else {
								self.stateDps = 'waiting';
								changeStates({'state':''});//clean loading
								changeStates({'state':'waiting_for_exp'});
							}
						} else {//state 6 - expired
							if (!isUndefined(self.allInvestmentsForOpp) && self.allInvestmentsForOpp.length == 0 && self.alternateOpps.length > 0 && !params.forceStateWaiting) {
								setNextOpp();
							} else {
								self.stateDps = 'inactive';
								playSound(3);
								changeStates({'state':''});//clean loading
								changeStates({'state':'expired','ao_level':params.ao_level});
							}
						}
					}
					else {//state 9,10
						self.stateDps = 'inactive';
						if(params.et_suspended_message == null){
							params.et_suspended_message = self.boxOpportunities[self.oppArrPos][SUBSCR_FLD.et_suspended_message];
						}
						var sperator = "<br/>";
						var arr = params.et_suspended_message.split("s");
						var str = suspendMsgUpperLine[new Number(arr[0]) - 1] + sperator + suspendMsgLowerLine[new Number(arr[1]) - 1];
						if (arr.length == 3 && arr[2].length > 0) {
							str += " " + formatDate(new Date(arr[2]), false);
						}
						handleMessages({'type':'popup','errorCode':5,'errorMsg':str}); 
					}
					
					try{
						angular.element('#BinaryOptionController').scope().updateState(self.stateDps, self.box, self.marketId);
					} catch(e) {
						//we are not on trading area
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.updateState = updateState;
		
		function setNextOpp() {
			self.stateDps = 'active';
			var index = searchJsonKeyInArray(self.alternateOpps, 'oppId', self.oppId);
			var nextOpp = 0;
			if (index > -1) {
				nextOpp = index + 1;
				if (nextOpp == self.alternateOpps.length) {nextOpp = self.alternateOpps.length-1;}
			}
			logIt({'type':1,'msg':'Alternate Hourly Option change: oppId = ' + self.alternateOpps[nextOpp].oppId});
			
			var tmp_oppId = self.alternateOpps[nextOpp].oppId;
			self.resetBox(true);
			self.oppId = tmp_oppId;
			self.oppArrPos = searchJsonKeyInArray(self.boxOpportunities,'ET_OPP_ID',self.oppId);
			
			self.updateVisual({'stateReallyChanged':true,'oppChanged':true,'callsTrendChanged':true,'aoFlagsChanged':true});//update with new oppId
			
			if(self.boxType != 1){
				tradeBox_getBalace(self.box);
			}
		}
		
		//update Profit/Refun Dropdown menu
		function updateProfitRefunDrop(){
			try{
				self.optionChanged = false;
				if (self.boxOpportunities[self.oppArrPos].ET_ODDS_GROUP != null) {
					var odds_group = self.boxOpportunities[self.oppArrPos].ET_ODDS_GROUP.trim().split(' ');
					self.odds_group_selected = '';
					for (var i = 0; i < odds_group.length; i++) {
						if (odds_group[i].search('d') > -1) {
							self.odds_group_selected = i;
							odds_group[i] = odds_group[i].replace('d', '');
						}
					}
					var tradeBox_profit_drop_amount = g('tradeBox_profit_drop_amount_' + self.box)
					if(odds_group.length <= 0){
						tradeBox_profit_drop_amount.onclick = function(){
							return false;
						}
						tradeBox_profit_drop_amount.className = tradeBox_profit_drop_amount.className.replace(/ disabled/g,'');
						tradeBox_profit_drop_amount.className += " disabled";
						return;
					}
					g('tradeBox_profit_drop_amount_' + self.box).onclick = function(){
						openElement({'span':this,'autoClose':true});
					}
					tradeBox_profit_drop_amount.className = tradeBox_profit_drop_amount.className.replace(/ disabled/g,'');
					var ul = g('tradeBox_profit_drop_ul_'+self.box);
					var lis = ul.getElementsByTagName('li');
					for(var i=1; i<lis.length; i++){
						if(i <= odds_group.length){
							var profit = returnRefund_all[odds_group[i-1]].profit;
							var refund = returnRefund_all[odds_group[i-1]].refund;
							lis[i].setAttribute('data-profit',profit);
							lis[i].setAttribute('data-refund',refund);
							var span = lis[i].getElementsByTagName('span');
							span[0].innerHTML = profit+'%';
							span[1].innerHTML = refund+'%';
							if (self.odds_group_selected !== '') {
								if (self.odds_group_selected == (i - 1)) {
									lis[i].className = " active";
									self.profit = profit;
									self.refund = refund;
								} else {
									lis[i].className = lis[i].className.replace(/ active/,'');
								}
							} else {
								if (profit == self.profit && refund == self.refund) {
									lis[i].className = " active";
								} else {
									lis[i].className = lis[i].className.replace(/ active/,'');
								}
							}
						}
						else{
							ul.removeChild(lis[i]);
						}
					}
					
					for(i; i<=odds_group.length; i++){
						var profit = returnRefund_all[odds_group[i-1]].profit;
						var refund = returnRefund_all[odds_group[i-1]].refund;
						if (self.odds_group_selected != '') {
							if (self.odds_group_selected == (i - 1)) {
								var className = " active";
								self.profit = profit;
								self.refund = refund;
							} else {
								var className = "";
							}
						} else {
							var className = (profit == self.profit && refund == self.refund)?" active":"";
						}
						
						ul.innerHTML += '<li class="'+className+'" data-profit="'+profit+'" data-refund="'+refund+'"><span class="tradeBox_profit_drop_left tradeBox_profit_drop_left_'+userLanguage+'">'+profit+'%</span><span class="tradeBox_profit_drop_right tradeBox_profit_drop_right_'+userLanguage+'">'+refund+'%</span></li>';
					}
					
					addFnToProfitDrop();
					g('tradeBox_profit_procent_'+self.box).innerHTML = self.profit+'%';
					updateProfitAmount();
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//update exp dropdown
		function updateExpDropAltHour(){
			try {
				var $ul = $('#tradeBox_menu_exp_' + self.box);
				var $lis = $ul.find('li');
				self.alternateOpps = [];
				if (!isUndefined(self.boxOpportunities[self.oppArrPos].AO_NH) && self.boxOpportunities[self.oppArrPos].AO_NH != '') {
					self.alternateOpps = self.boxOpportunities[self.oppArrPos].AO_NH.split(';')
						.map(function(current) {
							var temp = current.split('|');
							return {
								oppId: temp[0],
								expTime: adjustFromUTCToLocal(new Date(temp[1]))
							}
						});

					var $altHours = $ul.find('._altHours');
					if (self.alternateOpps.length > $altHours.length) {
						for (var i = $altHours.length; i < self.alternateOpps.length; i++) {
							$('<li class="_altHours" data-standart="0" data-value="1"></li>').insertBefore($lis[1]);
						}
					} else if ($altHours.length > self.alternateOpps.length) {
						for (var i = $altHours.length - 1; i >= self.alternateOpps.length; i--) {
							$altHours[i].remove();
						}
					}
					
					var index = 0;
					$ul.find('._altHours')
						.each(function() {
							setProperties($(this), index);
							index++;
						});
						
					
					function setProperties($element, index) {
						var date = self.alternateOpps[index].expTime;
						var h = date.getHours();
						if (h < 9) {h = '0' + h;}
						var m = date.getMinutes();
						if (m < 9) {m = '0' + m;}
						
						$element.html($element.parent().attr('data-text').replace('{est_closing}', h + ':' + m));
						$element.attr('data-opp-id', self.alternateOpps[index].oppId);
						
						$element.unbind();
						$element.click(function() {
							if (!self.inProgress) {
								updateExpTimeOpp(this);
							}
						})
					}
				} else {
					var $altHours = $ul.find('._altHours');
					for (var i = 0; i < $altHours.length; i++) {
						$altHours[i].remove();
					}
				}
				
				//quorterly
				self.quorterlyOpps = [];
				var mask = self.boxOpportunities[self.oppArrPos].FILTER_NEW_SCHEDULED;
				var maskSchedule = mask[mask.length - 5];
				if (!isUndefined(self.boxOpportunities[self.oppArrPos].AO_NQ) && self.boxOpportunities[self.oppArrPos].AO_NQ != '' && maskSchedule == '1') {
					self.quorterlyOpps = self.boxOpportunities[self.oppArrPos].AO_NQ.split(';')
						.map(function(current) {
							var temp = current.split('|');
							return {
								oppId: temp[0],
								expTime: adjustFromUTCToLocal(new Date(temp[1]))
							}
						});

					var $quorterly = $ul.find('._quorterly');
					if (self.quorterlyOpps.length > $quorterly.length) {
						for (var i = $quorterly.length; i < self.quorterlyOpps.length; i++) {
							$ul.append($('<li class="_quorterly" data-standart="0" data-value="6"></li>'));
						}
					} else if ($quorterly.length > self.quorterlyOpps.length) {
						for (var i = $quorterly.length - 1; i >= self.quorterlyOpps.length; i--) {
							$quorterly[i].remove();
						}
					}
					
					var index = 0;
					$ul.find('._quorterly')
						.each(function() {
							setPropertiesQuorterly($(this), index);
							index++;
						});
						
					
					function setPropertiesQuorterly($element, index) {
						var date = self.quorterlyOpps[index].expTime;
						var y = date.getFullYear();
						var m = date.getMonth();
						var q = Math.floor(m / 4) + 1;
						
						$element.html($element.parent().attr('data-text-quorterly') + ' Q' + q + ' ' + y);
						$element.attr('data-opp-id', self.quorterlyOpps[index].oppId);
						$element.attr('id', 'quorterly-' + self.quorterlyOpps[index].oppId);
						
						$element.unbind();
						$element.click(function() {
							if (!self.inProgress) {
								updateExpTimeOpp(this);
							}
						})
					}
				} else {
					var $quorterly = $ul.find('._quorterly');
					for (var i = 0; i < $quorterly.length; i++) {
						$quorterly[i].remove();
					}
				}
				updateExpDrop();
			} catch(e) {
				logIt({'type':3,'msg':e});
			}
		}
	
		//update expiry dropdown
		function updateExpDrop(){
			try{
				if(self.boxType != 1 && self.boxType != 4){
					return;
				}
				var scheduled = self.boxOpportunities[self.oppArrPos].FILTER_NEW_SCHEDULED;
				var lis = g('tradeBox_menu_exp_'+self.box).getElementsByTagName('li');
				var index = scheduled.length;
				for (var i = 0; i < lis.length; i++) {
					var type = lis[i].getAttribute('data-value');
					if (type == 6) {type = 5;}
					
					if (scheduled[index - parseInt(type)] == '1') {
						lis[i].style.display = "block";
					} else {
						lis[i].style.display = "none";
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}

		//put value into the html
		function updateVisual(params){
			try{
				if(self.boxOpportunities.length > 0){
					self.boxFull = true;
					//update market name
					if(self.boxType == 1){
						if (self.boxOpportunities[self.oppArrPos]) {
							g('tradeBox_marketName_'+self.box).innerHTML = self.boxOpportunities[self.oppArrPos].ET_NAME;
							
							var howToBanner_marketName = g('howToBanner_marketName');
							if(howToBanner_marketName != null){
								howToBanner_marketName.innerHTML = self.boxOpportunities[self.oppArrPos].ET_NAME;
							}
						}
					}
					//update current level
					if(self.boxOpportunities[self.oppArrPos].ao_level == 0){
						self.tradeBox_level_html.innerHTML = "<img src='"+context_path+"/images/other/wait.gif' />";
					}
					else{
						if(!self.submited){
							calcLevelOffset(self.boxOpportunities[self.oppArrPos].ao_level_float);
							self.tradeBox_level_html.innerHTML = numberWithCommas((self.boxOpportunities[self.oppArrPos].ao_level_float + self.logOutOffset).toFixed(self.decimalPoint));
						}
					}
					//update exp time
					if(self.boxType == 1){
						if(self.boxOpportunities[self.oppArrPos].ET_STATE != OPPORTUNITY_STATE.created){
							var text = '';
							switch(parseInt(self.market.schedule)) {
								case 1: text = formatDate(new Date(self.boxOpportunities[self.oppArrPos].ET_EST_CLOSE), true); break;
								case 2: 
								case 3: 
								case 4: text = $('#txt-schedule-' + self.market.schedule).html(); break;
								case 6: text = $('#quorterly-' + self.oppId).html(); break;
							}
							//g('trade_box_exp_time_'+self.box).innerHTML = text;
							g('trade_box_exp_time_'+self.box).innerHTML = formatDate(new Date(self.boxOpportunities[self.oppArrPos].ET_EST_CLOSE), true);
						}
					}
					//update default profit refound
					if(self.boxType == 1){
						if(self.boxOpportunities[self.oppArrPos].ET_STATE == OPPORTUNITY_STATE.created || self.boxOpportunities[self.oppArrPos].ET_STATE == OPPORTUNITY_STATE.waiting_to_pause || 
							self.boxOpportunities[self.oppArrPos].ET_STATE == OPPORTUNITY_STATE.paused || self.boxOpportunities[self.oppArrPos].ET_STATE == OPPORTUNITY_STATE.suspended){
							g('tradeBox_menu_exp_h_'+self.box).style.display = "none";
						}
						else{
							g('tradeBox_menu_exp_h_'+self.box).style.display = "block";
						}
					}
					if(self.profit === 0 || self.refund === 0){
						self.profit = Math.round((parseFloat(self.boxOpportunities[self.oppArrPos].ET_ODDS_WIN)-1)*100);
						self.refund = Math.round(parseFloat(self.boxOpportunities[self.oppArrPos].ET_ODDS_LOSE)*100);
					}
					//update level colours
					if(self.boxOpportunities[self.oppArrPos].ET_STATE != OPPORTUNITY_STATE.created && self.boxOpportunities[self.oppArrPos].ET_STATE != OPPORTUNITY_STATE.paused && 
						self.boxOpportunities[self.oppArrPos].ET_STATE != OPPORTUNITY_STATE.waiting_to_pause && self.boxOpportunities[self.oppArrPos].ET_STATE != OPPORTUNITY_STATE.suspended){
						switch(self.boxOpportunities[self.oppArrPos].ao_clr_dsp){
							case '0':var color = " below";break;
							case '1':var color = " same";break;
							case '2':var color = " above";break;
						}
						self.tradeBox_level_html.className = self.tradeBox_level_html.className.replace(/ below/g,'');
						self.tradeBox_level_html.className = self.tradeBox_level_html.className.replace(/ above/g,'');
						self.tradeBox_level_html.className = self.tradeBox_level_html.className.replace(/ same/g,'');
						self.tradeBox_level_html.className += color;
					}
					//update flags (show/hide trends)
					if(this.boxType == 1){
						if (params.aoFlagsChanged || params.stateReallyChanged) {
							var tradeBox_trends = g('tradeBox_trends_'+self.box);
							if(self.boxOpportunities[self.oppArrPos].ET_STATE == OPPORTUNITY_STATE.opened || self.boxOpportunities[self.oppArrPos].ET_STATE == OPPORTUNITY_STATE.last_10_min){
								if(((self.boxOpportunities[self.oppArrPos].AO_FLAGS) & 2) != 0) {
									tradeBox_trends.style.display = "block";
								}
								else{
									tradeBox_trends.style.display = "none";
								}
							}
							else {
								tradeBox_trends.style.display = "none";
							}
						}
					}
					//udpdate trend diagram
					if(this.boxType == 1){
						if(params.callsTrendChanged && self.boxOpportunities[self.oppArrPos].ao_calls_trend_dep != null) {
							var trends_call = Math.round(self.boxOpportunities[self.oppArrPos].ao_calls_trend_dep * 100);
							var trends_put = 100 - trends_call;
							
							var tradeBox_trends_graph_proc_call = g('tradeBox_trends_graph_proc_call_'+self.box);
							var tradeBox_trends_graph_sep = g('tradeBox_trends_graph_sep_'+self.box);
							if(trends_call === 0){
								tradeBox_trends_graph_proc_call.style.display = "none";
								tradeBox_trends_graph_sep.style.display = "none";
							}else{
								tradeBox_trends_graph_sep.style.display = "block";
								tradeBox_trends_graph_proc_call.style.display = "block";
								tradeBox_trends_graph_proc_call.innerHTML = trends_call+"%";
								tradeBox_trends_graph_proc_call.style.left = (trends_call/2)+"%";
							}
							var tradeBox_trends_graph_proc_put = g('tradeBox_trends_graph_proc_put_'+self.box);
							if(trends_put === 0){
								tradeBox_trends_graph_proc_put.style.display = "none";
							}else{
								tradeBox_trends_graph_proc_put.style.display = "block";
								tradeBox_trends_graph_proc_put.innerHTML = trends_put+"%";
								tradeBox_trends_graph_proc_put.style.right = (trends_put/2)+"%";					
							}
							
							g('tradeBox_trends_graph_put_'+self.box).style.width = trends_put+'%';
						}
					}
					
					//update current return
					updateCurrentReturn();
				
					updateTimeLeft();
					//update profit/refund dropdown
					if(self.optionChanged){
						if(this.boxType != 3){
							g('tradeBox_profit_procent_'+self.box).innerHTML = self.profit+'%';
							updateProfitRefunDrop();
						}
						else{
							g('tradeBox_profit_dsp_'+self.box).innerHTML = self.profit+"%";
							updateProfitAmount();
						}
						if(this.boxType == 1){
							updateExpDrop();
						}
					}
					updateExpDropAltHour();
					if (self.chart == null) {
						initGraph();
					}
				}
				else{
					handleMessages({'type':'popup','errorCode':4});
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.updateVisual = updateVisual;
		
		//init graph
		function initGraph(){
			if ((navigator.appVersion.indexOf("MSIE 7.") != -1) || (navigator.appVersion.indexOf("MSIE 8.") != -1)) {
				var color = 'white';
			}
			else{
				var color = 'transparent';
			}
			try{
				self.chart = new Highcharts.StockChart({
					credits: {
						enabled: false
					},
					chart: {
						renderTo: 'tradeBox_chart_'+self.box,
						margin: 0,
						height: self.chart_properties.height,
						backgroundColor: color,
						panning: false
					},
					rangeSelector : {
						enabled : false,
						selected: 0
					},
					scrollbar : {
						enabled : false
					},
					navigator : {
						enabled : false
					},
					xAxis: [{
						ordinal: false,
						type: 'datetime',
						dateTimeLabelFormats: {
							day: '%H:%M'
						},
						min: self.timeClosing_mil-(1000*60*60*2)-(1000*60*10)+self.serverOffset,//2 hours
						max: self.timeClosing_mil+(1000*60*10)+self.serverOffset,
						labels: {
							x: 0,
							y: self.chart_properties.timeLineTxtPosition,
							style: {
								color: self.chart_properties.timeLineTxtColor
							},
							format: '{value:%H:%M}',
							//tickInterval: 1000 * 60 * 20
						},
						gridLineColor: color,
						lineWidth: 0,
						endOnTick: false
					},{
						ordinal: false,
						type: 'datetime',
						min: self.timeClosing_mil-(1000*60*60*2)-(1000*60*10)+self.serverOffset,//2 hours
						max: self.timeClosing_mil+(1000*60*10)+self.serverOffset,
						labels: {
							enabled:false
						},
						gridLineColor: color,
						lineWidth: 0,
						endOnTick: false
					},{
						ordinal: false,
						type: 'datetime',
						dateTimeLabelFormats: {
							day: '%H:%M'
						},
						min: self.timeClosing_mil-(1000*60*60*2)-(1000*60*10)+self.serverOffset,//2 hours
						max: self.timeClosing_mil+(1000*60*10)+self.serverOffset,
						labels: {
							enabled: false
						},
						lineWidth: 0,
						endOnTick: false
					},{
						ordinal: false,
						type: 'datetime',
						dateTimeLabelFormats: {
							day: '%H:%M'
						},
						min: self.timeClosing_mil-(1000*60*60*2)-(1000*60*10)+self.serverOffset,//2 hours
						max: self.timeClosing_mil+(1000*60*10)+self.serverOffset,
						labels: {
							enabled: false
						},
						lineWidth: 0,
						endOnTick: false
					}],
					yAxis: [{
						height: self.chart_properties.yAxis_1,
						labels: {
							enabled:false
						},
						gridLineColor: color,
						lineWidth: 0
					},{
						xAxis: 1,
						height:self.chart_properties.height-self.chart_properties.yAxis_1,
						top:self.chart_properties.yAxis_1
					},{
						height: self.chart_properties.yAxis_1,
						labels: {
							enabled:false
						},
						gridLineColor: color,
						lineWidth: 0
					},{
						height: self.chart_properties.yAxis_1,
						labels: {
							enabled:false
						},
						lineWidth: 0
					}],
					series : [{
						type: 'line',
						lineWidth: 1,
						color: self.chart_properties.chart_lineColor,
						name: self.boxOpportunities[self.oppArrPos].ET_NAME,
						turboThreshold:0,
						data : [],
						zIndex: 12,
						dataGrouping: {
							enabled: false
						},
						states: {
							hover: {
								enabled: true,
								halo: {
									size: 0
								}
							}
						}
					},{
						yAxis: 1,
						xAxis: 1,
						type: 'line',
						lineWidth: 1,
						name: '',
						zIndex: 12
					},{
						type: 'line',
						lineWidth: 0,
						turboThreshold:0,
						data : [],
						zIndex: 13,
						dataGrouping: {
							enabled:false
						}
					},{
						type: 'line',
						lineWidth: 0,
						turboThreshold:0,
						data : [],
						zIndex: 14,
						dataGrouping: {
							enabled:false
						},
						marker: self.chart_point_properties,
						states: {
							hover: {
								enabled: false,
								halo: {
									size: 0
								}
							}
						}

					}],
					tooltip: {
						hideDelay:0,
						backgroundColor: self.chart_properties.tooltip_bgrColor,
						borderColor: self.chart_properties.tooltip_borderColor,
						useHTML: true,
						style:{
							zIndex:10
						},
						zIndex:10,
						formatter: function() {
							var num = Math.pow(10,self.decimalPoint);
							function roundLevel(level) {
								return Math.round(level*(num))/num;
							}
							var level = roundLevel(this.y);
							
							var temp = '';
							if (skinRegulated && userLogin) {
								var temp = (!isUndefined(this.points[0].point.ask) && this.points[0].point.ask != '') ? 'ASK:' + roundLevel(this.points[0].point.ask) : '';
								temp += (!isUndefined(this.points[0].point.bid) && this.points[0].point.bid != '') ? ((temp != '') ? " | " : '') + 'BID:' + roundLevel(this.points[0].point.bid) : '';
								temp += (!isUndefined(this.points[0].point.last) && this.points[0].point.last != '') ? ((temp != '') ? " | " : '') + 'LAST:' + roundLevel(this.points[0].point.last) : '';
							}
							
							var str = '<span class="taL ffDef">';
							var date = (!isUndefined(this.points[0].point.org_x) && this.points[0].point.org_x != '') ? this.points[0].point.org_x : this.x;
							str += '<span style="display:block;color:'+self.chart_properties.tooltip_timeTxtColor+';font-size:'+self.chart_properties.popUp_fsize1+';">'+formatDate(new Date(date), false, 'back')+'</span>';
							str += '<span style="color:'+self.chart_properties.tooltip_assetTxtColor+';font-size:'+self.chart_properties.popUp_fsize1+';">'+self.boxOpportunities[self.oppArrPos].ET_NAME+': </span>';
							str += '<span style="color:'+self.chart_properties.tooltip_levelTxtColor+';font-size:'+self.chart_properties.popUp_fsize2+';font-weight:bold;">'+level+"</span>";
								str += '<span style="display:block;color:'+self.chart_properties.tooltip_timeTxtColor+';font-size:'+self.chart_properties.popUp_fsize3+';">';
								str += temp;
								str += '</span>';
							str += "</span>";
							return str;
						}
					}
				});
				self.chartVissibleArea.start = self.chartVissibleArea.start_zoomed = self.timeClosing_mil-(1000*60*60)+self.serverOffset;
				self.chartVissibleArea.end = self.timeClosing_mil+(1000*60*10)+self.serverOffset;

				setChartExtremes(self.chartVissibleArea.start, self.chartVissibleArea.end);
				chartGetHistory(1,false);
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.initGraph = initGraph;
		
		function setChartExtremes(start, end, flag, ani) {
			self.chart.xAxis[0].setExtremes(start, end, flag, ani);
			self.chart.xAxis[1].setExtremes(start, end, flag, ani);
			self.chart.xAxis[2].setExtremes(start, end, flag, ani);
			self.chart.xAxis[3].setExtremes(start, end, flag, ani);
			
			var format = '{value:%H:%M}';
			var tickInterval = 1000 * 60 * 20;
			switch(parseInt(self.market.schedule)) {
				case 1: format = '{value:%H:%M}';tickInterval = 1000 * 60 * 20;break;
				case 2: format = '{value:%H:%M}';tickInterval = 1000 * 60 * 60 * 2;break;
				case 3: format = '{value:%d/%m}';tickInterval = 1000 * 60 * 60 * 24 * 2;break;
				case 4: format = '{value:%d/%m}';tickInterval = 1000 * 60 * 60 * 24 * 5;break;
				case 6: format = '{value:%d/%m}';tickInterval = 1000 * 60 * 60 * 24 * 23;break;
			}
			
			var tickPixelInterval = 60;
			if(self.boxType == 2 || self.boxType == 3){
				tickPixelInterval  = 70;
			}
			
			self.chart.xAxis[0].update({
				labels:{
					format: format
				},
//				tickInterval: tickInterval,
				tickPixelInterval: tickPixelInterval
			});
		}
		
		//get history
		function chartGetHistory(part,inv){
			try {
				var timeout = Math.floor(Math.random() * 2000);
				if (self.delayChartHistory) {
					timeout = Math.floor(Math.random() * 9000);
					self.delayChartHistory = false;
				}
				logIt({'type':1,'msg':'chart timeout: '+timeout});
				setTimeout(function() {
					self.showOnlyIvestments = inv;
					self.showHistoryPart = part;
					if (!isUndefined(chartHistory[self.box]) && chartHistory[self.box].settings.marketId == self.marketId && part == 1) {
						addHistoryToChart(chartHistory[self.box]);
						chartHistory[self.box] = null;
					} else {
						var typeId = 0;
						switch(self.boxType) {
							case 1:
							case 2: typeId = 1;break;
							case 3: typeId = 3;break;
						}
						var url = context_path + "/jsp/xml/chartDataJson.jsf?marketId="+self.marketId+"&typeId="+typeId+"&history="+part+"&scheduled="+self.market.schedule+"&oppId="+self.oppId;
							
						var ajaxCall = new aJaxCall_fn({'url':url,'callback':addHistoryToChart});
						ajaxCall.doGet();
					}
				}, timeout);
			} catch(e) {
				logIt({'type':3,'msg':e});
			}
		}
		this.chartGetHistory = chartGetHistory;
		
		//add history to chart
		// this.addHistoryToChart = addHistoryToChart;
		function addHistoryToChart(resp){
			try{
				if (typeof resp != 'object') {
					resp = eval('('+resp+')');
				}
				if(resp.settings.marketId == self.marketId){
					if (self.oppId != resp.settings.opportunityId && self.marketChanged) {
						self.oppId = resp.settings.opportunityId;
					}
					self.marketChanged = false;
					if(!self.showOnlyIvestments){
						self.decimalPoint = resp.settings.decimalPoint;
						if (self.market.schedule > 1) {
							$('#tradeBox_chart_zoom_h_'+self.box).hide();
						} else {
							$('#tradeBox_chart_zoom_h_'+self.box).show();
						}
						
						var orgPoint = '';
						if (resp.rates.length > 0 && resp.rates[0][0] < resp.settings.timeFirstInvest && self.market.schedule == 2) {//last day closing level
							orgPoint = resp.rates[0][0] + self.serverOffset;
							resp.rates[0][0] = resp.settings.timeFirstInvest;
							self.endOfDayLastDayLevel = true;
						} else {
							self.endOfDayLastDayLevel =  false;
						}
						
						for (var i = 0; i < resp.rates.length; i++) {
							addPointToChart(
								resp.rates[i][0] + self.serverOffset,
								resp.rates[i][1],
								'',
								(resp.rates[i][3] != 0) ? resp.rates[i][3] : '',
								(resp.rates[i][4] != 0) ? resp.rates[i][4] : '',
								(resp.rates[i][2] != 0) ? resp.rates[i][2] : '',
								0,
								(i == 0) ? orgPoint : ''
							);
						}
						if(resp.rates.length > 0){
							if (self.market.schedule > 1) {
								var offset = 0;
								switch(parseInt(self.market.schedule)) {
									case 2: offset = (1000*60*60*0.1);break;
								}
								self.chartVissibleArea.start = self.chartVissibleArea.start_zoomed = resp.rates[0][0] + self.serverOffset - offset;
								self.chartVissibleArea.end = self.timeClosing_mil + self.serverOffset;

								setChartExtremes(self.chartVissibleArea.start, self.chartVissibleArea.end);
							} else {
								var lastPoint = resp.rates[resp.rates.length-1][0] + self.serverOffset;
								if (lastPoint < self.chartVissibleArea.start && self.showHistoryPart == '1') {
									self.chartVissibleArea.start = self.chartVissibleArea.start_zoomed = resp.rates[0][0] + self.serverOffset;
									self.chartVissibleArea.end = self.chartVissibleArea.start + (1000*60*70);
									
									setChartExtremes(self.chartVissibleArea.start, self.chartVissibleArea.end);
								} else if(lastPoint < self.chartVissibleArea.start && self.showHistoryPart == '2') {
									self.chartVissibleArea.start_zoomed = resp.rates[0][0] + self.serverOffset;
									
									setChartExtremes(self.chartVissibleArea.start, self.chartVissibleArea.end);
									updateChartTimeLeft();
								}
							}
						}
					}
					if(self.boxType != 1){
						var ul = g('tradeBox_profit_invs_'+self.box);
						ul.innerHTML = "";
					}
					self.chartPointsInv = [];
					self.allInvestments = resp.investments;
					self.allInvestmentsForOpp = [];
					var totalInv = 0;

					for (var i=0; i<resp.investments.length; i++) {
						if(self.boxType != 1){
							var li = document.createElement('li');
							li.className = "tradeBox_profit_invs_li";
							li.setAttribute('data-marketId',resp.investments[i].marketId);
							if(self.boxType == 2){
								li.onclick = function(){
									closeProfitLine_box();
									openProfitLine_box(this.getAttribute('data-marketId'));
								}
							}
							else if(self.boxType == 3){
								li.onclick = function(){
									selectMarket(this.getAttribute('data-marketId'));
								}
							}
							ul.appendChild(li);
							
							var span = document.createElement('span');
							span.className = "tradeBox_profit_invs_tooltip";
							span.innerHTML = optionPlus_openAsset;
							li.appendChild(span);
							
							var span = document.createElement('span');
							span.className = "tradeBox_profit_invs_c1 vaM";
							span.innerHTML = resp.investments[i].market;
							li.appendChild(span);
							
							var arrow = document.createElement('i');
							switch(resp.investments[i].type){
								case 1:var cls = 'call_win';break;
								case 2:var cls = 'put_win';break;
							}
							arrow.className = "tradeBox_img tradeBox_profit_invs_c2 "+cls+" vaM";
							arrow.id = "tradeBox_invest_list_"+resp.investments[i].id;
							arrow.setAttribute('data-lastClass','');
							
							var levelTxt = document.createElement('span');
							levelTxt.className = "tradeBox_profit_invs_c3 vaM";
							levelTxt.innerHTML = resp.investments[i].levelTxt;
							
							if(skinId != '1'){
								li.appendChild(arrow);
								li.appendChild(levelTxt);
							}else{
								li.appendChild(levelTxt);
								li.appendChild(arrow);
							}
							
							var span = document.createElement('span');
							span.className = "tradeBox_profit_invs_c4 vaM";
							span.innerHTML = resp.investments[i].amountTxt;
							li.appendChild(span);
							
							var span = document.createElement('span');
							span.className = "tradeBox_profit_invs_c5 vaM";
							span.innerHTML = formatDateHours(resp.investments[i].timeEstClosing,false);
							li.appendChild(span);
							if((self.boxType == 3) && (self.marketId == resp.investments[i].marketId)){
								var span = document.createElement('span');
								span.setAttribute('data-oppId',resp.investments[i].id);
								span.className = "tradeBox_profit_invs_c6 vaM cP "+userLanguage;
								span.innerHTML = optionPlus_getQuote;
								span.onclick = function(){
									changeQuotState({'state':1,'oppId':this.getAttribute('data-oppId')});
								}
								li.appendChild(span);
							}
						}
						if(!self.showOnlyIvestments){
							if(resp.investments[i].oppId == self.boxOpportunities[self.oppArrPos].ET_OPP_ID){
								addPointToChart(
									resp.investments[i].time + self.serverOffset,
									resp.investments[i].level, 
									'point_' + resp.investments[i].id,
									'',
									'',
									'',
									1
								);
							}
						}
						if(resp.investments[i].oppId == self.boxOpportunities[self.oppArrPos].ET_OPP_ID){
							self.allInvestmentsForOpp.push(resp.investments[i]);
							self.chartPointsInv.push({
								'point':'point_'+resp.investments[i].id,
								'type':resp.investments[i].type,
								'time':resp.investments[i].time,
								'level':resp.investments[i].level,
								'amount':resp.investments[i].amount,
								'oddsWin':resp.investments[i].oddsWin,
								'oddsLose':resp.investments[i].oddsLose,
								'marketId':resp.investments[i].marketId,
								'oppId':resp.investments[i].oppId
							});
							// addPointToChart(resp.investments[i].time,resp.investments[i].level,'point_'+resp.investments[i].id,1);
							var num = Math.pow(10,currencyDecimalPoint);
							totalInv += (resp.investments[i].amount/num);
							// if(self.boxType == 3){
								// totalInv -= commsionFee;
							// }
						}
					}
					updateCurrentReturn();
					if(currencyDecimalPoint == 0){
						totalInv = totalInv/100;
					}
					if(self.boxType != '1'){
						g('profitLine_total_inv_val_' + self.box).innerHTML = formatAmount(totalInv,0);
					}
					self.chartPointsInv = self.chartPointsInv.sort(function(a,b) {
						return a.level < b.level;
					});
					if(!self.showOnlyIvestments){
						redrowChart();
					}
					
					chartBandsIvestments();
					changeChartColors();
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		function changeChartColors() {
			try {
				if (self.market.schedule == 1) {
					self.chart.series[0].update({
						lineWidth: 1,
						states: {
							hover: {
								lineWidth: 2
							}
						}
					})
				} else {
					self.chart.series[0].update({
						lineWidth: 2,
						states: {
							hover: {
								lineWidth: 3
							}
						}
					})
				}
				
				if (self.chartPointsInv.length > 0) {
					self.chart.series[0].color = "white";
					self.chart.series[0].update({
						lineColor: 'white'
					})
					self.chart.xAxis[0].update({
							labels:{
								style:{
									color:'white'
								}
							}
						});
					self.chart_point_properties.fillColor = self.chart_properties.chart_current_with_inv;
					self.chart_point_properties.lineColor = self.chart_properties.chart_current_with_inv;
					self.chart.series[3].update({marker: self.chart_point_properties});
				} else {
					self.chart.series[0].color = self.chart_properties.chart_lineColor;
					self.chart.series[0].update({
						lineColor: self.chart_properties.chart_lineColor
					})
					self.chart.xAxis[0].update({
							labels:{
								style:{
									color:self.chart_properties.timeLineTxtColor
								}
							}
						});
					self.chart_point_properties.fillColor = self.chart_properties.chart_current;
					self.chart_point_properties.lineColor = self.chart_properties.chart_current;
					self.chart.series[3].update({marker: self.chart_point_properties});
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		//in future if we realy do have all markets levels (not in use now)
		function updateInvest_list(){
			try{
				for(var i=0; i<self.allInvestments.length; i++){
					var class_part_1 = (type == '1')?'call_':'put_';
					var class_part_2 = (win)?'win':'loose';
					var class_part = class_part_1+class_part_2;
					var span = g('tradeBox_invest_list_'+self.allInvestments[i].id);
					span.className = span.className.replace(span.getAttribute('data-lastClass'),'');
					span.className += " "+class_part;
					span.setAttribute('data-lastClass'," "+class_part)
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		//using all points redrow the chart after ordering the points
		function redrowChart(){
			try{
				self.allPoints = self.allPoints.sort(function(a,b){
					return a.x - b.x;
				});
				self.allPointsInv = self.allPointsInv.sort(function(a,b){
					return a.x - b.x;
				});

				self.chart.series[0].setData(self.allPoints, true);
				self.chart.series[2].setData(self.allPointsInv, true);
				self.chart.series[3].setData([{
					x : self.lastUpdate.x,
					y : +(self.lastUpdate.y + self.logOutOffset).toFixed(self.decimalPoint),
				}], true);
				
				var marker = {}
				if (self.market.schedule == 1) {
					marker = {
						marker: {
							enabled: false
						}
					}
				} else {
					if (self.allPointsInv.length > 0) {
						marker = {
							marker: {
								enabled: true,
								fillColor: self.chart_properties.chart_marker_with_inv,
								lineColor: '#fff',
								lineWidth: 1,
								radius: 3,
								states: {
									hover: {
										fillColor: self.chart_properties.chart_marker_hover_with_inv
									}
								}
							}
						}
					} else {
						marker = {
							marker: {
								enabled: true,
								fillColor: self.chart_properties.chart_marker,
								lineColor: '#fff',
								lineWidth: 1,
								radius: 3,
								states: {
									hover: {
										fillColor: self.chart_properties.chart_marker_hover
									}
								}
							}
						}
					}
				}
				
				self.chart.series[0].update(marker);
				
				if (self.endOfDayLastDayLevel && self.market.schedule == 2) {
					var point = self.chart.series[0].data[0];
					if (point != null) {
						point.update({
							marker:{
								enabled: true,
								symbol: 'square',
								fillColor: '#0284d6',
								lineColor: '#0284d6',
								radius: 3
							}
						});
					}
				}
				
				// if (self.market.schedule == 1) {
					// addSquareToLastPoint();
				// }
				
				updateChartInvArrow();
				changeChartColors();
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		if(this.boxType == 3){
			g('tradeBox_offer_sell_btn_cancel_'+this.box).onclick = function(){
				changeQuotState({'state':0});
			}
			g('tradeBox_offer_sell_btn_cancel2_'+this.box).onclick = function(){
				changeQuotState({'state':0});
			}
			g('tradeBox_offer_sell_btn_cancel3_'+this.box).onclick = function(){
				changeQuotState({'state':0});
			}
			g('tradeBox_offer_sell_btn_sell_'+this.box).onclick = function(){
				changeQuotState({'state':6});
			}
			g('tradeBox_offer_sell_btn_qetNewQuot_'+this.box).onclick = function(){
				changeQuotState({'state':1,'oppId':self.getQuot_state.lastQuot_oppId});
			}
			g('tradeBox_offer_sell_btn_qetNewQuot2_'+this.box).onclick = function(){
				changeQuotState({'state':1,'oppId':self.getQuot_state.lastQuot_oppId});
			}
		}
		//change different stetes of quot popup
		this.getQuot_state = {'className':'hidden','lastQuot_oppId':0};
		this.quotTimer = null;
		function changeQuotState(params){
			try{
				clearTimeout(self.quotTimer);
				clearTimeout(self.hideQuotExpired);
				var popup = g('tradeBox_offer_'+self.box);
				switch(params.state){//0:hidden,1:loading get price,2:selling,3:expired,4:sold,5:error,6:loading selling
					case 1:
						if(self.boxOpportunities[self.oppArrPos].ET_STATE == '2' || self.boxOpportunities[self.oppArrPos].ET_STATE == '3'){
							var newClass = 'loading';
							popup.className = popup.className.replace(self.getQuot_state.className,newClass);
							self.getQuot_state.className = newClass;
							
							self.getQuot_state.lastQuot_oppId = params.oppId;
							var paramsAjax = "flashGetPrice=1" + "&investmentId=" + params.oppId;
							var ajaxCall = new aJaxCall_fn({'type':'POST','url':context_path + '/jsp/system/ajax.jsf','params':paramsAjax,'callback':getQuot_Callback});
							ajaxCall.doGet();
						}else{
							changeQuotState({});
						}
						break;
					case 6: 
						var newClass = 'loading';
						popup.className = popup.className.replace(self.getQuot_state.className,newClass);
						self.getQuot_state.className = newClass;
						
						var paramsAjax = "flashSell=1&investmentId=" + self.getQuot_state.lastQuot_oppId + "&price=" + self.getQuot_state.price;
						var ajaxCall = new aJaxCall_fn({'type':'POST','url':context_path + '/jsp/system/ajax.jsf','params':paramsAjax,'callback':sellQuot_Callback});
						ajaxCall.doGet();
						break;
					case 2: 
						var newClass = 'sell';
						popup.className = popup.className.replace(self.getQuot_state.className,newClass);
						self.getQuot_state.className = newClass;
						g('tradeBox_offer_sell_amount_'+self.box).innerHTML = formatAmount(params.quotPrice,1);
						self.getQuot_state.price = params.quotPrice;
						break;
					case 3: 
						var newClass = 'expired';
						popup.className = popup.className.replace(self.getQuot_state.className,newClass);
						self.getQuot_state.className = newClass;
						self.hideQuotExpired = setTimeout(function(){changeQuotState({});},5000);
						break;
					case 4: 
						var newClass = 'sold';
						popup.className = popup.className.replace(self.getQuot_state.className,newClass);
						self.getQuot_state.className = newClass;
						
						g('tradeBox_offer_sold_amount_'+self.box).innerHTML = formatAmount(self.getQuot_state.price,1);
						setTimeout(function(){changeQuotState({'state':0})},3000);
						break;
					case 5: 
						var newClass = 'error';
						popup.className = popup.className.replace(self.getQuot_state.className,newClass);
						self.getQuot_state.className = newClass;
						g('tradeBox_offer_error_msg_'+self.box).innerHTML = params.error.replace(/\+/g,' ');
						break;
					default:
						var newClass = 'hidden';
						popup.className = popup.className.replace(self.getQuot_state.className,newClass);
						self.getQuot_state.className = newClass;
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.changeQuotState = changeQuotState;
		
		//fnc after ajax call for quot price
		function getQuot_Callback(response){
			try{
				var resp = response.split('=');
				if(!isNaN(resp[1])){
					changeQuotState({'state':2,'quotPrice':decodeURIComponent(resp[1])});
					startQuotTimer();
				}
				else{
					changeQuotState({'state':5,'error':decodeURIComponent(resp[1])});
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		// this.getQuot_Callback = getQuot_Callback;
		
		//fnc after ajax call for sell quot
		function sellQuot_Callback(response){
			try{
				var resp = response.split('=');
				if(resp[1] == 'OK'){
					setTimeout(function(){tradeBox_getBalace(self.box);},500);
					var point = self.chart.get('point_' + self.getQuot_state.lastQuot_oppId);
					if(point != null){
						point.remove(false,false);
					}
					chartGetHistory(1,true);
					changeQuotState({'state':4});
				}
				else{
					changeQuotState({'state':5,'error':resp[1]});
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.sellQuot_Callback = sellQuot_Callback;
		
		function startQuotTimer(){
			try{
				var t = g('tradeBox_offer_sell_timer_time_'+self.box);
				var t2 = g('tradeBox_offer_sell_timer_time2_'+self.box);
				var bgr = g('tradeBox_offer_sell_timer_bgr_'+self.box);
				var popup = g('tradeBox_offer_'+self.box);
				var date_end = new Date().getTime() + self.seconds_to_quot;
				var time_left = self.seconds_to_quot;
				function doIt(){
					var min = Math.floor(time_left/1000);
					if(min < 10){min = '0'+min;}
					var sec = Math.floor((time_left-(min*1000))/10);
					if(sec < 10){sec = '0'+sec;}
					t.innerHTML = t2.innerHTML = min+":"+sec;
					bgr.style.width = 100-(time_left/self.seconds_to_quot)*100+'%';
					
					if(time_left > 0){
						var interval = 10;
						time_left = date_end - new Date().getTime();
						self.quotTimer = setTimeout(doIt,interval);
					}
					else{
						changeQuotState({'state':3});
					}
				}
				doIt();
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//calculate level offset
		function calcLevelOffset(level) {
			if (self.logOutOffset == null && !userLogin) {
				var min = 0.0015;
				var max = 0.0025;
				// var sign = Math.ceil(Math.random() - 0.5) * 2 - 1;
				var sign = [-1,1][Math.random()*2|0];
				var levelArray = level.toString().split('.');
				self.decimalPoint = (levelArray.length > 1) ? levelArray[1].length : 0;
				self.logOutOffset = +(sign * (level * (Math.random() * (max - min) + min))).toFixed(self.decimalPoint);//random 1.5-2.5
			}
		}
		
		//add point to chart
		function addPointToChart(x_val, y_val, point_id, ask, bid, last, type, original_x){//type: 0-chart, 1-investment
			try {
				calcLevelOffset(y_val);
				var pointInfo = {
					x : x_val,
					y : +(y_val + self.logOutOffset).toFixed(self.decimalPoint),
					id: point_id,
					marker: {enabled: (self.market.schedule == 1) ? false : true},
					ask: ask,
					bid: bid,
					last: last,
					org_x: (!isUndefined(original_x) && original_x != '') ? original_x : ''
				}
				if (type == 0) {
					self.allPoints.push(pointInfo);
				} else {
					self.allPointsInv.push(pointInfo);
				}
			} catch(e) {
				logIt({'type':3,'msg':e});
			}
		}
		
		// function addSquareToLastPoint(){
			// try{
				// var l = self.allPoints.length;
				// if(l > 0){
					// if(l < 20){
						// var l2 = l;
					// }
					// else{
						// var l2 = 10;
					// }
					// for(var i=l-l2; i<l; i++){
						// self.allPoints[i].marker = {enabled: false};
						// if(self.allPoints[i].id == 'lastPoint'){
							// self.allPoints[i].id = '';
						// }
					// }
				// }
				// self.allPoints[l-1].marker = self.chart_point_properties;
				// if(self.allPoints[l-1].id == ""){
					// self.allPoints[l-1].id = 'lastPoint';
				// }
			// }catch(e){
				// logIt({'type':3,'msg':e});
			// }
		// }
		//update graph
		function updateGraph(){
			try{
				if(self.boxOpportunities[self.oppArrPos].ao_level_float != 0 && !self.submited){
					if(self.chart != null){
						logIt({'type':1,'msg':'UPDATE CHART box: '+self.box+' level:'+self.boxOpportunities[self.oppArrPos].ao_level_float});
						
						var time = parseInt(self.boxOpportunities[self.oppArrPos].AO_TS);
						var level = self.boxOpportunities[self.oppArrPos].ao_level_float;
						//check if the point is in graph's visible area
						var update_time = time+self.serverOffset;
						if(update_time > self.timeClosing_mil + (1000*60*10) + self.serverOffset - (1000*60*60)){
							self.chartVissibleArea.start_zoom = self.chartVissibleArea.start;
							self.chartVissibleArea.start = self.timeClosing_mil-(1000*60*60)+self.serverOffset;
							self.chartVissibleArea.end = self.timeClosing_mil+(1000*60*10)+self.serverOffset;
						}
						else if(update_time > self.chartVissibleArea.end-(1000*60*10)){
							self.chartVissibleArea.start_zoom = self.chartVissibleArea.start;
							self.chartVissibleArea.start = update_time-(1000*60*5);
							self.chartVissibleArea.end = self.chartVissibleArea.start + (1000*60*60);
							
							setChartExtremes(self.chartVissibleArea.start, self.chartVissibleArea.end);
						}
						self.lastUpdate = {
							x: update_time,
							y: level
						}
						//add point to chart
						if (self.market.schedule == 1) {
							addPointToChart(
								self.lastUpdate.x,
								self.lastUpdate.y,
								'',
								(self.boxOpportunities[self.oppArrPos].ASK != null) ? self.boxOpportunities[self.oppArrPos].ASK : '',
								(self.boxOpportunities[self.oppArrPos].BID != null) ? self.boxOpportunities[self.oppArrPos].BID : '',
								(self.boxOpportunities[self.oppArrPos].LAST != null) ? self.boxOpportunities[self.oppArrPos].LAST : '',
								0
							);
						}
						redrowChart();
						
						self.chart.xAxis[0].removePlotLine('plot-line-x');
						self.chart.xAxis[0].addPlotLine({
							// value: time+self.serverOffset,
							value: self.lastUpdate.x,
							width: 1,
							color: (self.chartPointsInv.length == 0)?self.chart_properties.plotLinesColor:self.chart_properties.plotLinesColor_with_inv,
							dashStyle: 'ShortDash',
							zIndex:2,
							id: 'plot-line-x'
						});
						self.chart.yAxis[0].removePlotLine('plot-line-y');
						self.chart.yAxis[0].addPlotLine({
							value: self.lastUpdate.y + self.logOutOffset,
							width: 1,
							color: (self.chartPointsInv.length == 0)?self.chart_properties.plotLinesColor:self.chart_properties.plotLinesColor_with_inv,
							dashStyle: 'ShortDash',
							zIndex:2,
							id: 'plot-line-y'
						});
						if(chartBands_last.btn != ""){
							chartBands(chartBands_last.btn,chartBands_last.add,true);
						}
						chartBandsIvestments();
					}
					//TO DO in future
					// updateInvest_list();
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.updateGraph = updateGraph;
		
		//check all invests and update points
		//this.updateChartInvArrow = updateChartInvArrow;
		function updateChartInvArrow(){
			try{
				for(var i=0; i<self.chartPointsInv.length; i++){
					var point = self.chart.get(self.chartPointsInv[i].point);
					if(point != null){
						if(self.chartPointsInv[i].type == 1){
							var type = 'triangle';
							if(self.chartPointsInv[i].level < self.boxOpportunities[self.oppArrPos].ao_level_float){
								var color = self.chart_properties.chart_win;
							}
							else if(self.chartPointsInv[i].level > self.boxOpportunities[self.oppArrPos].ao_level_float){
								var color = self.chart_properties.chart_lose;
							}
							else{
								var color = self.chart_properties.chart_noChange;
							}
						}
						else{
							var type = 'triangle-down';
							if(self.chartPointsInv[i].level > self.boxOpportunities[self.oppArrPos].ao_level_float){
								var color = self.chart_properties.chart_win;
							}
							else if(self.chartPointsInv[i].level < self.boxOpportunities[self.oppArrPos].ao_level_float){
								var color = self.chart_properties.chart_lose;
							}
							else{
								var color = self.chart_properties.chart_noChange;
							}
						}
						point.update({
							marker:{
								enabled: true,
								symbol: type,
								fillColor: color,
								lineColor: color,
								lineWidth: 1,
								radius: 7,
								states: {
									hover: {
										fillColor: 'black',
										radius: 7
									}
								}
							}
						});
					}
					else{
						logIt({'type':3,'msg':'Point id cannot be found'});
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//erase all data from the graph and get history
		function resetGraph(){
			try{
				if(self.chart != null){
					self.chart.destroy();
					self.chart = null;
				}
				self.firstZoom = false;
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.resetGraph = resetGraph;
		
		//add mouseover/out event to call put button
		var chartBands_last = {'btn':'','add':''};
		g('tradeBox_call_btn_'+self.box).onmouseover = function(){
			chartBands(1,true,false);
		}
		g('tradeBox_call_btn_'+self.box).onmouseout = function(){
			chartBands(1,false,false);
		}
		g('tradeBox_put_btn_'+self.box).onmouseover = function(){
			chartBands(2,true,false);
		}
		g('tradeBox_put_btn_'+self.box).onmouseout = function(){
			chartBands(2,false,false);
		}
		
		//profit line button
		if(this.boxType == 1){
			g('tradeBox_profitLine_'+self.box).onclick = function(){
				profitLine(self.oppId);
			}
		}
		g('tradeBox_print_'+self.box).onclick = function(){
			printSuccess(self.investmentId);
		}
		//open profitline
		function profitLine(oppId){
			openProfitLine_box(self.marketId, oppId);
		}
		
		//chartBands
		// this.chartBands = chartBands;
		this.hasPlotBand = false;
		function chartBands(btn,add,force){
			try{
				if(self.chart != null){
					self.chart.yAxis[0].removePlotBand('plot-band-1');
					self.chart.yAxis[0].removePlotBand('plot-band-2');

					if (add) {
						chartBands_last.btn = btn;
						chartBands_last.add = add;
						var from,to;
						if(btn == 1){
							from = self.chart.yAxis[0].max;
							to = self.lastUpdate.y;
						}else{
							from = self.lastUpdate.y;
							to = self.chart.yAxis[0].min;
						}
						self.chart.yAxis[0].addPlotBand({
							from: from,
							to: to,
							// color: {
								// pattern: image_context_path+'/'+self.chart_properties.band_bgr,
								// width: self.chart_properties.band_bgr_width,
								// height: self.chart_properties.band_bgr_height
							// },
							zIndex:1,
							color: self.chart_properties.plotBandMouseOver,
							id: 'plot-band-'+btn,
							zIndex:2
						});
					}
					else{
						chartBands_last.btn = '';
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		this.chartBandsNum = 0;
		this.soundType_last = 0;
		function chartBandsIvestments(){
			try{
				checkBoxesInvestment();
				if(self.chartBandsNum > 0){
					for(var i=0; i<=self.chartBandsNum; i++){
						self.chart.yAxis[0].removePlotBand('plot-band-inv-'+i);
						if(i > 0){
							self.chart.yAxis[0].removePlotLine('plot-line-x-inv-'+i);
						}
					}
				}
				self.chartBandsNum = self.chartPointsInv.length;
				if(self.chartPointsInv.length > 0){
					var soundType = 1;//in
					for(var i=0; i<=self.chartPointsInv.length; i++){
						if(i == 0){
							var from = self.chart.yAxis[0].max;
							var to = self.chartPointsInv[i].level;
						}
						else if(i == self.chartPointsInv.length){
							var from = self.chartPointsInv[i-1].level;
							var to = self.chart.yAxis[0].min;
						}
						else{
							var from = self.chartPointsInv[i-1].level;
							var to = self.chartPointsInv[i].level;	
						}
						var color = (isProfitable(from,to))?
							(from > self.lastUpdate.y && self.lastUpdate.y > to)?self.chart_properties.chart_plotBand_inTheMoney_current:self.chart_properties.chart_plotBand_inTheMoney:
							(from > self.lastUpdate.y && self.lastUpdate.y > to)?self.chart_properties.chart_plotBand_outTheMoney_current:self.chart_properties.chart_plotBand_outTheMoney;
						;
						if(color == self.chart_properties.chart_plotBand_outTheMoney_current){
							soundType = 2;//out
						}
						self.chart.yAxis[0].addPlotBand({
							from: from,
							to: to,
							// color: {
								// pattern: image_context_path+'/'+self.chart_properties.band_bgr,
								// width: self.chart_properties.band_bgr_width,
								// height: self.chart_properties.band_bgr_height
							// },
							zIndex:1,
							color: color,
							id: 'plot-band-inv-'+i
						});
						if(i > 0){
							self.chart.yAxis[0].addPlotLine({
								value: from,
								width: 1,
								color: self.chart_properties.plotLinesColor_inv,
								dashStyle: 'ShortDash',
								zIndex:2,
								id: 'plot-line-x-inv-'+i
							});
						}
					}
					if(self.soundType_last != soundType){
						self.soundType_last = soundType;
						playSound(soundType);
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		function isProfitable(bottomLevel,topLevel){
			try{
				var totalAmount = 0;
				var totalProfit = 0;
				var checkLevel = bottomLevel + (topLevel - bottomLevel) / 2;
				
				for (var i = 0; i < self.chartPointsInv.length; i++) {
					var num = Math.pow(10,currencyDecimalPoint);
					var amount = self.chartPointsInv[i].amount/num;
					totalAmount += amount;
					if ((checkLevel < self.chartPointsInv[i].level && self.chartPointsInv[i].type == 1) || (self.chartPointsInv[i].level < checkLevel && self.chartPointsInv[i].type == 2)) {
						totalProfit += amount * (1 - self.chartPointsInv[i].oddsLose);
					} else {
						totalProfit += amount * (1 + self.chartPointsInv[i].oddsWin);
					}
				}
				return totalProfit > totalAmount;
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		function getColor(crrLvl){
			try{
				var current = false;
				if (bottomLevel < crrLvl && crrLvl < topLevel) {
					current = true;
				}
				if (profitable) {
					if (current) {
						return _root.CLR_WIN_CRR;
					} else {
						return _root.CLR_WIN;
					}
				} else {
					if (current) {
						return _root.CLR_LOSE_CRR;
					} else {
						return _root.CLR_LOSE;
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//play a sound on state chenge
		if(this.boxType != 1){
			g('tradeBox_audio').onclick = function(){
				tradeBox_mute(this)
			}
		}
		function playSound(soundType){
			if(self.boxType == '1'){return;}
			if(self.audioMute){return;}
			var fileName = '';
			switch(soundType){
				case 1: fileName = 'oneclick_in';break;
				case 2: fileName = 'oneclick_out';break;
				case 3: fileName = 'oneclick_close';break;
				default:return;
			}
			var player = g('tradeBox_audio_' + self.box);
			player.pause();
			g('tradeBox_audio_mp3_' + self.box).src = image_context_path_clean + '/audio/' + fileName + '.mp3';
			g('tradeBox_audio_ogg_' + self.box).src = image_context_path_clean + '/audio/' + fileName + '.ogg';
			player.load();
			player.play();
		}
		this.audioMute = false;
		function tradeBox_mute(el){
			if(!self.audioMute){
				self.audioMute = true;
				el.className += ' mute';
			}
			else{
				self.audioMute = false;
				el.className = el.className.replace(/ mute/g,'');
			}
		}
		//updateChartTimeLeft
		function updateChartTimeLeft(){
			try{
				if(self.chart != null && self.chart != undefined){
					self.chart.xAxis[1].removePlotBand('plot-band-timer-bgr');
					self.chart.xAxis[1].addPlotBand({
						from: self.chartVissibleArea.start_zoomed,
						to: self.timeClosing_mil+(1000*60*10)+self.serverOffset,
						color: self.chart_properties.timer_bgrColor,
						id: 'plot-band-timer-bgr',
						zIndex:1
					});
					self.chart.xAxis[1].removePlotBand('plot-band-timer-normal');
					self.chart.xAxis[1].addPlotBand({
						from: self.chartVissibleArea.start_zoomed,
						to: parseInt(self.boxOpportunities[self.oppArrPos].AO_TS)+self.serverOffset,
						color: self.chart_properties.timer_timeElapsedColor,
						id: 'plot-band-timer-bgr',
						zIndex:1
					});
					if(self.boxOpportunities[self.oppArrPos].AO_TS >= (self.timeLastInv_mil-(1000*self.lastSecondsToInvest))){
						self.chart.xAxis[1].removePlotBand('plot-band-timer-red_bgr');
						self.chart.xAxis[1].addPlotBand({
							from: self.timeLastInv_mil-(1000*self.lastSecondsToInvest)+self.serverOffset,
							to: self.timeLastInv_mil+self.serverOffset,
							color: self.chart_properties.timer_bgrLastTenColor,
							id: 'plot-band-timer-red_bgr',
							zIndex:1
						});
						self.chart.xAxis[1].removePlotBand('plot-band-timer-last');
						self.chart.xAxis[1].addPlotBand({
							from: self.timeLastInv_mil-(1000*self.lastSecondsToInvest)+self.serverOffset,
							to: parseInt(self.boxOpportunities[self.oppArrPos].AO_TS)+self.serverOffset,
							color: self.chart_properties.timer_timeLastTenElapsedColor,
							id: 'plot-band-timer-last',
							zIndex:1
						});
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//add click event to zoom btns
		g('tradeBox_chart_zoom_plus_'+self.box).onclick = function(){
			chartZoom(false);
		}
		g('tradeBox_chart_zoom_minus_'+self.box).onclick = function(){
			chartZoom(true);
		}
		
		//chart zoom
		function chartZoom(plus){
			try{
				if(!self.firstZoom && plus){
					chartGetHistory(2,false);
					self.firstZoom = true;
				}
				else{
					if(plus){
						var min = self.chartVissibleArea.start_zoomed
					}
					else{
						var min = self.chartVissibleArea.start
					}
					var max = self.chartVissibleArea.end
					
					var ani = true;
					if(self.chartPointsInv.length > 0){
						ani = false;
					}
					setChartExtremes(min,max,true,ani);
					chartBandsIvestments();
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//getBalace if optionPlus
		if(this.boxType == 3){
			tradeBox_getBalace(self.box);
		}
		
		if(this.boxType != 1){
			g('profitLine_marketDrop_title_'+this.box).onclick = function(){
				openElement({'span':this,'autoClose':true});
				getBigMarketsDropDown();
			}
		}
		
		//getBigMarketsDropDown profitline and optionplus
		function getBigMarketsDropDown(){
			try{
				var profitLine_marketDrop_title_txt = g('profitLine_marketDrop_title_txt_' + self.box);
				if(profitLine_marketDrop_title_txt.innerHTML != txt_loading){
					self.profitLine_marketDrop_title_txt_default = profitLine_marketDrop_title_txt.innerHTML;
				}
				profitLine_marketDrop_title_txt.innerHTML = txt_loading;
				var ul = g('profitLine_marketDrop_ul_'+self.box);
				ul.innerHTML = "";
				var typeId = (self.boxType == 3)?3:1;
				var url = context_path + "/jsp/xml/profitLineAssetSelect.jsf?typeId="+typeId; 
				var ajaxCall = new aJaxCall_fn({'url':url,'callback':getBigMarketsDropDown_callBack});
				ajaxCall.doGet();
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		function getBigMarketsDropDown_callBack(response){
			try{
				var resp = eval('('+response+')');
				var ul = g('profitLine_marketDrop_ul_'+self.box);
				ul.innerHTML = "";
				var group = "";
				var subgroup = "";
				ul.innerHTML = '<li class="marketsMenuFilter_li" id="marketsMenuFilter_li_'+self.box+'"><input id="marketsMenuFilter_'+self.box+'" class="funnel_drop_ul_search_inp_big" /></li>';
				g('marketsMenuFilter_'+self.box).onkeyup = function(){
					filterDropDowns(event,"getBigMarketsDropDown_reset("+self.box+")");
				}
				setTimeout(function(){g('marketsMenuFilter_'+self.box).focus();},100);
				
				for(var i=0; i<resp.length; i++){
					if(self.boxType == '2'){
						if(group != resp[i].group){
							var li = document.createElement('li');
							li.className = 'profitLine_marketDrop_groupname';
							li.innerHTML = resp[i].group;
							ul.appendChild(li);
							group = resp[i].group;
						}
						if(subgroup != resp[i].subGroup){
							var li = document.createElement('li');
							li.className = 'profitLine_marketDrop_subgroupname';
							li.innerHTML = resp[i].subGroup;
							ul.appendChild(li);
							subgroup = resp[i].subGroup;
						}
					}
					var li = document.createElement('li');
					li.className = 'profitLine_marketDrop_element' + ((self.marketId == resp[i].marketId)?' active':'');
					li.setAttribute('data-marketId',resp[i].marketId);
					li.setAttribute('data-search','t');
					li.setAttribute('data-feedName', resp[i].displayFeedName);
					li.setAttribute('data-oppId', resp[i].opportunityId);
					
					if (self.boxType == 2) {
						li.onclick = function(){
							if (!self.inProgress) {
								closeProfitLine_box();
								g('profitLine_marketDrop_title_txt_'+self.box).innerHTML = ls_getMarketName(this.getAttribute('data-marketId'));
								self.tradeBox_level_html.innerHTML = txt_loading;
								if (!isUndefined(self.assetPage) && self.assetPage) {
									window.location = context_path + "/assets/" + this.getAttribute('data-feedName');
								} else {
									openProfitLine_box(this.getAttribute('data-marketId'), this.getAttribute('data-oppId'));
								}
								closeElement({'last':true});
							}
						}
					}
					else if(self.boxType == 3){
						li.onclick = function(){
							selectMarket(this.getAttribute('data-marketId'));
							closeElement({'last':true});
						}
					}
					var span = document.createElement('span');
					span.className = 'fL';
					span.innerHTML = resp[i].market;
					li.appendChild(span);
					var span = document.createElement('span');
					span.className = 'fR mr10';
					span.innerHTML = resp[i].timeEstClosing;
					li.appendChild(span);
					
					ul.appendChild(li);
				}
				g('profitLine_marketDrop_title_txt_' + self.box).innerHTML = self.profitLine_marketDrop_title_txt_default;
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		// this.getBigMarketsDropDown = getBigMarketsDropDown;
		
		//send sms on expire
		function updateInvestSms(){
			try{
				var params = "turnSmsOn&invId=" + self.investmentId;
				logIt({'type':1,'msg':'updateInvestSms box: '+self.box});
				var ajaxCall = new aJaxCall_fn({'type':'POST','url':context_path + '/ajax.jsf','params':params,'callback':updateInvestSmsCallback});
				ajaxCall.doGet();
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		//TO DO GET NORMAL JSON RESPONSE
		function updateInvestSmsCallback(response){
			try{
				if (response.substring(0, 2) == "ok") {
					g('tradeBox_sms_inp_'+self.box).disabled = true;
					g('tradeBox_sms_txt_'+self.box).innerHTML = acceptSmsMessage;
				} else {
					//do nothing we dont have text and not sure we want :)
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		function updateCurrentReturn(){
			try{
				if(self.boxType != 1){
					var totalProfit = 0;
					
					for (var i = 0; i < self.chartPointsInv.length; i++) {
						if(self.marketId == self.chartPointsInv[i].marketId){
							var num = Math.pow(10,currencyDecimalPoint);
							if(currencyDecimalPoint == 0){
								num = 100;
							}
							var amount = (self.chartPointsInv[i].amount/num);
							if(self.boxType == 3){
								amount -= commsionFee;
							}
							if ((self.boxOpportunities[self.oppArrPos].ao_level_float <= self.chartPointsInv[i].level && self.chartPointsInv[i].type == 1) || 
								(self.chartPointsInv[i].level <= self.boxOpportunities[self.oppArrPos].ao_level_float && self.chartPointsInv[i].type == 2)) {
								totalProfit += amount * (1 - self.chartPointsInv[i].oddsLose);
							} else {
								totalProfit += amount * (1 + self.chartPointsInv[i].oddsWin);
							}
						}
					}
					g('profitLine_curr_rtn_val_' + self.box).innerHTML = formatAmount(totalProfit,0);
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
	}catch(e){
		logIt({'type':3,'msg':e});
	}
}
function callPut_sw(n){
	try{
		logIt({'type':1,'msg':'init call/put text switch for box '+n});
		this.n = n;
		this.call = g('tradeBox_call_btn_text_'+n);
		this.call_txt_1 = this.call.getAttribute('date-text_1');
		this.call_txt_2 = this.call.getAttribute('date-text_2');
		this.put = g('tradeBox_put_btn_text_'+n);
		this.put_txt_1 = this.put.getAttribute('date-text_1');
		this.put_txt_2 = this.put.getAttribute('date-text_2');
		this.flag = true;
		var self = this;
		
		function switchIt(){
			if(self.flag){
				self.call.innerHTML = self.call_txt_1;
				self.put.innerHTML = self.put_txt_1;
				self.flag = false;
			}
			else{
				self.flag = true;
				self.call.innerHTML = self.call_txt_2;
				self.put.innerHTML = self.put_txt_2;
			}
		}
		setInterval(switchIt,2000);
	}catch(e){
		logIt({'type':3,'msg':e});
	}
}
function formatAmount(amount, centsPart, withoutDecimalPoint, round) {//centsPart: 1-upper span,2-no cents if 00,0-default show full format with .00,3- upper span without 00
	try{
		if (!isNaN(amount)) {
			var rtn = '';
			if (amount.toString().charAt(0) == '-') {
				rtn = '-';
				amount *= -1;
			}
			if (typeof withoutDecimalPoint != 'undefined' && withoutDecimalPoint) {
				amount = amountToFloat(amount);
			}
			if (typeof round == 'undefined' || !round || amount < 1000 ) {
				amount = Math.round(amount*100)/100;
				var tmp = amount.toString().split('.');
				var decimal = parseInt(tmp[0]);
				var cents = (typeof tmp[1] != 'undefined')?tmp[1]:0;
				if((cents === '') || (cents === 0)){cents = '00';}
				else if(cents.length == '1'){cents = cents + '0';}
				else if(cents.length == '2' && parseInt(cents) < 10){cents = '0' + parseInt(cents);}
				
				if(isLeftCurrencySymbol){
					rtn += currencySymbol;
				}
				rtn += numberWithCommas(decimal);
				if(currencyDecimalPoint == 0){
					//Absolutly nothing. Just chill!
				}
				else if((centsPart == 1) || ((centsPart == 3) && (parseInt(cents) != 0))){
					rtn += "<span class='cents'>"+cents+"</span>";
				}
				else if((centsPart == 3) && (parseInt(cents) == 0)){
					//Absolutly nothing. Just chill!
				}
				else if((centsPart == 2) && (parseInt(cents) == 0)){
					//Absolutly nothing. Just chill!
				}
				else{
					rtn += "."+cents;
				}
				if(!isLeftCurrencySymbol){
					rtn += currencySymbol;
				}
			} else {
				if(isLeftCurrencySymbol){
					rtn += currencySymbol;
				}
				rtn += (Math.round(amount / 100)) / 10 + "K";
				if(!isLeftCurrencySymbol){
					rtn += currencySymbol;
				}
			}
			return rtn;
		} else {
			return ' ';
		}
	} catch(e) {
		logIt({'type':3,'msg':e});
	}
}
function amountToFloat(amount) {
	return (amount/Math.pow(10, currencyDecimalPoint));
}
function numberWithCommas(x) {
	x = x.toString().replace(/,/g,'');
	var tmp = x.split('.');
    var rtn = tmp[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	if(typeof tmp[1] != 'undefined'){
		rtn += '.' + tmp[1];
	}
	return rtn;
}
function checkValue(val){
	try{
		var vals = val.split('.');
		var regD = /\D/g;
		vals[0] = vals[0].replace(regD,'');
		vals[0] = vals[0].substr(0,8);
		var rtn = vals[0];
		if(typeof vals[1] != 'undefined'){
			vals[1] = vals[1].replace(regD,'');
			vals[1] = vals[1].substr(0,2);
			
			rtn += '.';
			
			if(vals[1] >= 0){
				rtn += vals[1];
			}
		}
		return rtn;
	}catch(e){
		logIt({'type':3,'msg':e});
	}
}
function checkChars(obj) {//from tradePage.js
	var validchars = "0123456789";
	var sc = ".";
	var scused = false;
	var newval = "";
	var currval;
	var allowed = 0;
	var allowed_before_dot = 0;

	currval = obj.value;

	for (var x = 0; x < currval.length; x++) {
        var currchar = currval.charAt(x);
        for (var y = 0; y < validchars.length; y++) {
            var c = validchars.charAt(y);
            if (currchar == c) {
                if (scused) {
                    allowed++;
                    if (allowed > 2) {
                        break;
                    } else {
                        newval += currchar;
                        break;
                    }
                } else {
                    newval += currchar;
                    break;
                }
            } else if (!scused) {
                if (currchar == sc) {
                    scused = true;
                    newval += currchar;
                }
            }
        }
    }
    obj.value = newval;
}
function clearActiveList(lis){
	for(var i=0; i<lis.length; i++){
		lis[i].className = lis[i].className.replace(/ active/g,'');
	}
}
function adjustFromUTCToLocal(toAdj) {//from tradePage.js
	var adjM = toAdj.getMinutes() - new Date().getTimezoneOffset();
	var adjH = 0;
	if (adjM >= 0) {
		adjH = Math.floor(adjM / 60);
		adjM = adjM % 60;
	} else {
		adjH = Math.floor(Math.abs(adjM) / 60);
		if (Math.abs(adjM) % 60 != 0) {
			adjH += 1;
			adjM = 60 - Math.abs(adjM) % 60;
		} else {
			adjM = Math.abs(adjM) % 60;
		}
		adjH = -adjH;
	}
	toAdj.setHours(toAdj.getHours() + adjH);
	toAdj.setMinutes(adjM);
	return toAdj;
}
function formatDate(est_date, isTitle, offset) {//from tradePage.js
	if (offset == undefined) {
		est_date = adjustFromUTCToLocal(est_date);
	} else if (offset){
		est_date = new Date(est_date.getTime()+new Date().getTimezoneOffset()*60*1000);
	} else if (offset == 'back'){
		est_date = new Date(est_date.getTime() - new Date().getTimezoneOffset()*60*1000);
	}

	var currdate = new Date();
	var min = est_date.getMinutes();
	var ruCharBox = 'РІ';
	if(ruCharBox != null){
		ruCharBox.innerHTML;
	}
	if (est_date.getDate() == currdate.getDate() && est_date.getMonth() == currdate.getMonth() && est_date.getFullYear() == currdate.getFullYear()) {
		if (new Number(est_date.getMinutes()) < new Number("10")) {
			min = "0" + est_date.getMinutes();
		}
		if (skinId == 10 && !isTitle) {
			est_date = ruCharBox + est_date.getHours() + ":" + min + ", " + bundle_msg_today;
		} else {
			est_date = est_date.getHours() + ":" + min + " " + bundle_msg_today;
		}

	} else {
		if (new Number(est_date.getMinutes()) < new Number("10")) {
			min = "0" + est_date.getMinutes();
		}
		var year = new String(est_date.getFullYear());
		var month = est_date.getMonth()+1;
		if (month < 10) {
			month = '0' + month;
		}
		if (skinId == 10 && !isTitle) {
			est_date = est_date.getDate() + "." + month + "." + year.substring(2) + ", " + ruCharBox + est_date.getHours() + ":" + min;
		} else {
			est_date = est_date.getHours() + ":" + min + ", " + est_date.getDate() + "." + month + "." + year.substring(2);
		}
	}
	return est_date;
}
var BO_default_markets;
function ls_updateItem(updateInfo) {
	var itemPos = updateInfo.getItemPos();
	var itemName = updateInfo.getItemName();
	var timeStamp = updateInfo.getValue(SUBSCR_FLD.time_stamp);
	
	// handle market states
	if(updateInfo.isValueChanged(SUBSCR_FLD.ao_states)){
		logIt({'type':1,'msg':'Market states changed: '+updateInfo.getValue(SUBSCR_FLD.ao_states)});
		if(timeStamp > ls_marketsTimestamp){
			logIt({'type':1,'msg':'Market states updated.'});
			var ms = new Array();
			var aoStates = updateInfo.getValue(SUBSCR_FLD.ao_states);
			if((aoStates != null) && (aoStates != '')){
				ls_marketStates = aoStates.split("|");
			}

			if (ls_marketStates.length === 0) {//show off hour banner
				logIt({'type':1,'msg':'No open markets. Show offhours.'});
				ls_switchTable(0);
				if (livePage) {
					ls_switchLiveTrends(0);
					ls_switchLiveTable(0);
				}
			} else {//show the boxes
				logIt({'type':1,'msg':'There are '+ls_marketStates.length+' open markets'});
				ls_switchTable(1);
				if (livePage) {
					ls_switchLiveTrends(1);
					ls_switchLiveTable(1);
				}
			}
		}
	}

	// handle markets
	if((updateInfo.isValueChanged(SUBSCR_FLD.ao_markets)) || (markestInBox.length === 0)){
		logIt({'type':1,'msg':'Markets changed: ' + updateInfo.getValue(SUBSCR_FLD.ao_markets)});
		if (markestInBox.length === 0) {// in the first update init the two empty arrays
			for(i = 0; i < group.length; i++){
				var mv = group[i].split("_");//example:aotps_1_5
				if(mv[0] == "aotps"){
					markestInBox[i] = box_objects[i].market = {'market_id':mv[2], 'reset_market_id':mv[2], 'schedule':mv[1], 'type':mv[0]};
					box_objects[i].marketId = parseInt(mv[2]);
				}else{
					markestInBox[i] = box_objects[i].market = {'market_id':mv[1], 'reset_market_id':mv[2], 'schedule':1, 'type':mv[0]};
					box_objects[i].marketId = parseInt(mv[1]);
				}
				if(box_objects[i].duplicatedTo != null){
					box_objects[box_objects[i].duplicatedTo].market = {'market_id':box_objects[i].market.market_id, 'reset_market_id': box_objects[i].market.market_id,'schedule':box_objects[i].market.schedule,'type':box_objects[i].market.type};
					box_objects[box_objects[i].duplicatedTo].marketId = parseInt(box_objects[i].market.market_id);
				}
			}
		}
		logIt({'type':1,'msg':{'msg':'Markets in boxes','currentMarkestInBox':markestInBox}});

		var ms = updateInfo.getValue(SUBSCR_FLD.ao_markets).split("|");
		BO_default_markets = ms;
		if(timeStamp > ls_marketsTimestamp){
			logIt({'type':1,'msg':'Markets updated'});
			for (i = 0; i < group.length; i++) {
				if(box_objects[i].boxType != 3){
					if(!box_objects[i].marketIdManual){
						markestInBox[i] = {'market_id':ms[i],'reset_market_id':ms[i],'schedule':1,'type':group[i].split('_')[0]};
						box_objects[i].marketId = parseInt(ms[i]);
						if(box_objects[i].duplicatedTo != null){
							box_objects[box_objects[i].duplicatedTo].marketId = parseInt(ms[i]);
						}
					}
				}
			}
			if(typeof box_objects[i] != 'undefined' && box_objects[i].boxType != 3){
				ls_scheduleStartBoxes();
			}
		}
	}

	if (timeStamp > ls_marketsTimestamp) {
		ls_marketsTimestamp = timeStamp;
	}

	// update the boxes
	for (i = 0; i < group.length; i++) {
		if (itemName === group[i]) {
			var num = i;
			box_objects[i].lastUpdateInfo = updateInfo;
			var updateValueChanges = {};
			for (var j = 0; j < schema.length; j++) {
				updateValueChanges[schema[j]] = updateInfo.isValueChanged(schema[j]);
			}
			ls_updateItemNonVisual(i, itemPos, updateInfo, updateValueChanges);
			if(box_objects[num].duplicatedTo != null){
				ls_updateItemNonVisual(box_objects[num].duplicatedTo, itemPos, updateInfo, updateValueChanges);
			}
			break;
		}
	}
}
function ls_updateItemNonVisual(boxNumber, itemPos, updateInfo, updateValueChanges) {//from tradePage.js
	try{
		var et_group_closeAllowed = [3,6,7,12,14,15];
		if(updateInfo.getValue(SUBSCR_FLD.et_scheduled) == box_objects[boxNumber].market.schedule || et_group_closeAllowed.indexOf(updateInfo.getValue(SUBSCR_FLD.et_group_close) != -1)){
			logIt({'type':1,'msg':'updateItemNonVisual box:'+boxNumber});
			var oppId = parseInt(updateInfo.getValue(SUBSCR_FLD.et_opp_id));
			var filter_new_scheduled = parseInt(updateInfo.getValue(SUBSCR_FLD.filter_new_scheduled));
			var oppState = updateInfo.getValue(SUBSCR_FLD.et_state);
			var oppArrPos = searchJsonKeyInArray(box_objects[boxNumber].boxOpportunities,'ET_OPP_ID',oppId);
			
			//if the opportunity is not active and state 'delete' comes - do nothing
			if(oppArrPos === -1 && updateInfo.getValue(SUBSCR_FLD.command) === "DELETE"){//state DELETE unset
				logIt({'type':1,'msg':'DELETE unset opp '+oppId});
				return;
			}
			
			//if we have the opportunity and delete state comes
			if(updateInfo.getValue(SUBSCR_FLD.command) === "DELETE"){//state DELETE
				logIt({'type':1,'msg':'DELETE '+oppId});
				box_objects[boxNumber].boxOpportunities.splice(oppArrPos,1);
				
				if (box_objects[boxNumber].oppArrPos == oppArrPos) {
					box_objects[boxNumber].resetBox(true);
					box_objects[boxNumber].oppArrPos = 0;
					box_objects[boxNumber].marketChanged = true;
					
					box_objects[boxNumber].updateVisual({'stateReallyChanged':true,'oppChanged':true,'callsTrendChanged':true,'aoFlagsChanged':true});//update with new oppId
					
					if(box_objects[boxNumber].boxType != 1){
						tradeBox_getBalace(boxNumber);
					}
					box_objects[boxNumber].delayChartHistory = true;
				}
				
				return;
			}
			
			var added = false;
			if (oppArrPos === -1) {//ADD
				logIt({'type':1,'msg':'ADD '+oppId});
				added = true;

				if(box_objects[boxNumber].boxOpportunities.length > 0){
					if(oppState > box_objects[boxNumber].boxOpportunities[0].ET_STATE || 
						new Date(updateInfo.getValue(SUBSCR_FLD.et_est_close)) < new Date(box_objects[boxNumber].boxOpportunities[0].ET_EST_CLOSE)){
						box_objects[boxNumber].boxOpportunities.unshift({'ET_OPP_ID':oppId,'ET_STATE':oppState,'FILTER_NEW_SCHEDULED': filter_new_scheduled});
						box_objects[boxNumber].optionChanged = true;
					}
					else{//if new opp is with lower state then the one active one
						box_objects[boxNumber].boxOpportunities.push({'ET_OPP_ID':oppId,'ET_STATE':oppState,'FILTER_NEW_SCHEDULED': filter_new_scheduled});
						box_objects[boxNumber].optionChanged = false;
					}
				}
				else{//if first opp for this market
					box_objects[boxNumber].boxOpportunities[0] = {'ET_OPP_ID':oppId,'ET_STATE':oppState,'FILTER_NEW_SCHEDULED': filter_new_scheduled};
				}
			}
			
			oppArrPos = searchJsonKeyInArray(box_objects[boxNumber].boxOpportunities,'ET_OPP_ID',oppId);
			
			//update array index of the active opportunity
			var waitForCorrectOppId = false;
			if (box_objects[boxNumber].oppId != 0) {
				box_objects[boxNumber].oppArrPos = searchJsonKeyInArray(box_objects[boxNumber].boxOpportunities,'ET_OPP_ID',box_objects[boxNumber].oppId);
				if (box_objects[boxNumber].oppArrPos == -1) {
					box_objects[boxNumber].oppArrPos = 0;
					waitForCorrectOppId = true;
				}
			} else {
				box_objects[boxNumber].oppArrPos = 0;
			}
			
			var stateReallyChanged = updateValueChanges[SUBSCR_FLD.et_state];
			// if(box_objects[boxNumber].boxOpportunities[oppArrPos][SUBSCR_FLD.et_state] == updateInfo.getValue(SUBSCR_FLD.et_state)){
				// stateReallyChanged = false;
			// }
			var oppChanged = updateValueChanges[SUBSCR_FLD.et_opp_id];
			// if(box_objects[boxNumber].boxOpportunities[oppArrPos][SUBSCR_FLD.et_opp_id] == updateInfo.getValue(SUBSCR_FLD.et_opp_id)){
				// oppChanged = false;
			// }
			var callsTrendChanged = updateValueChanges[SUBSCR_FLD.ao_calls_trend];
			// if(box_objects[boxNumber].boxOpportunities[oppArrPos][SUBSCR_FLD.ao_calls_trend_dsp] == updateInfo.getValue(SUBSCR_FLD.ao_calls_trend)){
				// callsTrendChanged = false;
			// }
			var aoFlagsChanged = updateValueChanges[SUBSCR_FLD.ao_flags];
			// if(box_objects[boxNumber].boxOpportunities[oppArrPos][SUBSCR_FLD.ao_flags_dsp] == updateInfo.getValue(SUBSCR_FLD.ao_flags)){
				// aoFlagsChanged = false;
			// }
			
			if ((added || stateReallyChanged) && !waitForCorrectOppId) {
				box_objects[boxNumber].updateState(//changeBoxState 
					{'oppId':oppId,
						'ao_level':updateInfo.getValue(SUBSCR_FLD.ao_level),
						'et_state':updateInfo.getValue(SUBSCR_FLD.et_state),
						'et_est_close':updateInfo.getValue(SUBSCR_FLD.et_est_close),
						'last_invest':updateInfo.getValue(SUBSCR_FLD.last_invest),
						'et_suspended_message':updateInfo.getValue(SUBSCR_FLD.et_suspended_message),
						'forceStateWaiting': false
					}
				);
			}

			// if(box_objects[boxNumber].submited){return;}
			
			for (i = 0; i < schema.length; i++) {
				var valueFromUpdate = updateInfo.getValue(schema[i]);
				if(schema[i] === SUBSCR_FLD.et_name){
					valueFromUpdate = ls_getMarketName(valueFromUpdate);
				}
				else if (schema[i] === SUBSCR_FLD.ao_level) {
					box_objects[boxNumber].boxOpportunities[oppArrPos].ao_level = valueFromUpdate;
					box_objects[boxNumber].boxOpportunities[oppArrPos].ao_level_float = parseFloat(valueFromUpdate.replace(/,/g,''));
				}
				if(schema[i] === SUBSCR_FLD.ao_clr){
					box_objects[boxNumber].boxOpportunities[oppArrPos].ao_clr_dsp = valueFromUpdate;
				}
				if (schema[i] === SUBSCR_FLD.ao_calls_trend) {//trends
					box_objects[boxNumber].boxOpportunities[oppArrPos].ao_calls_trend_dep = valueFromUpdate;
				}
				
				box_objects[boxNumber].boxOpportunities[oppArrPos][schema[i]] = valueFromUpdate;
			}
			if(box_objects[boxNumber].boxType != 1){
				g('profitLine_marketDrop_title_txt_'+boxNumber).innerHTML = box_objects[boxNumber].boxOpportunities[box_objects[boxNumber].oppArrPos].ET_NAME+' - '+formatDateHours(box_objects[boxNumber].boxOpportunities[box_objects[boxNumber].oppArrPos].ET_EST_CLOSE,true);
			}
			if(oppId == box_objects[boxNumber].boxOpportunities[box_objects[boxNumber].oppArrPos].ET_OPP_ID && !waitForCorrectOppId){
				// handle force susspend of opportunity
				if (box_objects[boxNumber].boxOpportunities[box_objects[boxNumber].oppArrPos].AO_OPP_STATE == '1') {
					getNewMarket();
					return;
				}
				box_objects[boxNumber].updateVisual({'stateReallyChanged':stateReallyChanged,'oppChanged':oppChanged,'callsTrendChanged':callsTrendChanged,'aoFlagsChanged':aoFlagsChanged});
				
				
				box_objects[boxNumber].updateGraph();
			}
		}
	}catch(e){
		logIt({'type':3,'msg':e});
	}
}
function ls_switchTable(flag) {//from tradePage.js
	try{
		var markets_table = g("markets_table");
		var off_hours_banner = g("off_hours_banner");
		var off_hours_banner_home = g("off_hours_banner_home");
		var pop_one_touch = g("pop-one-touch");
		var popup_1t = g("popup-1t");
		
		// var howToHpBanner = g("howToHpBanner");//TO DO remove (only ZH)
		if(markets_table != null && off_hours_banner != null){
			var profitLineDiv = g("profitLineDiv"); //profit line open button
			if (flag === 1) {//show trade boxes
				markets_table.style.display = 'block';
				off_hours_banner.style.display = 'none';
				if (off_hours_banner_home != null) {
					off_hours_banner.style.display = 'none';
					pop_one_touch.style.display = 'none';
					popup_1t.style.display = 'none';
					off_hours_banner_home.style.display = 'none';
				}

				// howToHpBanner.style.display = 'block';
				if (profitLineDiv != null) {
					profitLineDiv.style.display = 'block';
				}
			}
			else {//show off hour banner
				markets_table.style.display = 'none';
				off_hours_banner.style.display = 'block';
				if (off_hours_banner_home != null) {
					off_hours_banner.style.display = 'block';
					pop_one_touch.style.display = 'block';
					popup_1t.style.display = 'block';
					off_hours_banner_home.style.display = 'block';
				}
				
				// howToHpBanner.style.display = 'block';
				if (profitLineDiv != null) {
					profitLineDiv.style.display = 'none';
				}
			}
		}
	}catch(e){
		logIt({'type':3,'msg':e});
	}

}
function ls_switchLiveTrends(flag) {//from live_page_trands.js
	try{
		var liveTradingTrends =  g("liveTradingTrends");
		var liveTradingTrendsOffHoursLogin = g("liveTradingTrendsOffHoursLogin");
		if (flag === 0) {
			liveTradingTrends.style.display = 'none';
			liveTradingTrendsOffHoursLogin.style.display = 'block';	
		} else {
			liveTradingTrends.style.display = 'block';
			liveTradingTrendsOffHoursLogin.style.display = 'none';
		}
		
		var trendsBox = g("trendsBox");
		if(trendsBox != null) {
			if(flag === 0) {
				trendsBox.style.height = "350px";
			} else {
				trendsBox.style.height = "220px";
			}
		}
		ls_switchLiveGlobe(flag);
	}catch(e){
		logIt({'type':3,'msg':e});
	}

}
function ls_switchLiveGlobe(flag) {//from live_page_trands.js
	try{
		var globeLogout =  g("globeLogout");
		var globeLogoutOffHours = g("globeLogoutOffHours");
		if (flag === 0) {
			globeLogout.style.display = 'none';
			globeLogoutOffHours.style.display = 'block';	
		} else {
			globeLogout.style.display = 'block';
			globeLogoutOffHours.style.display = 'none';
		}
	}catch(e){
		logIt({'type':3,'msg':e});
	}

}
function ls_switchLiveTable(flag) {//from live.js
	var investmentsTable =  g("liveInvestmentsTable");
	var offHourTable = g("liveInvestmentsOffHours");
	if (flag == 0) {
		investmentsTable.style.display = 'none';
		offHourTable.style.display = 'block';
	} else {
		investmentsTable.style.display = 'block';
		offHourTable.style.display = 'none';
	}
	
}
var boxCleanTimeout = null;
function ls_scheduleStartBoxes() {//from tradePage.js
	if (boxCleanTimeout === null) {
		logIt({'type':1,'msg':'Schedule Start Boxes'});
		boxCleanTimeout = setTimeout(ls_cleanBoxes, 10000 + Math.floor(Math.random() * 30000));
	}
}
function ls_cleanBoxes() {//from tradePage.js
	boxCleanTimeout = null;
	logIt({'type':1,'msg':'Cleaning boxes'});
	if (markestInBox.length > 0) {
		try {
			var haveCleaned = false;
			for (var i = 0; i < group.length; i++) {
				logIt({'type':1,'msg':"box: " + i + " full: " + box_objects[i].boxFull + " currentMarket: " + box_objects[i].market.market_id + " market: " + markestInBox[i].market_id});
				if ((box_objects[i].boxFull && !box_objects[i].marketIdManual && box_objects[i].market.market_id != markestInBox[i].market_id)) {
					box_objects[i].resetBox(true);//reset the box
					box_objects[i].boxOpportunities = [];
					box_objects[i].handleMessages({'type':'popup','errorCode':4});//set the box in loading mode
					haveCleaned = true;
				}
				if (!box_objects[i].boxFull) {
					haveCleaned = true;
				}
			}
			if (haveCleaned) {
				lsClient.unsubscribe(tradingBox);
				ls_startBoxes();
			}
		} catch (err) {
			logIt({'type':3,'msg':'Problem in cleanBoxes: ' + err});
		}
	}
}
function ls_startBoxes() {//from tradePage.js
	logIt({'type':1,'msg':'Start boxes'});
	var j = 0;
	for (var i = 0; i < group.length; i++) {
		if (!box_objects[i].boxFull) {
			log("starting box: " + i + " market: " + markestInBox[i].market_id + " schedule: " + markestInBox[i].schedule);
			if(markestInBox[i].type == 'aotps'){
				group[i] = markestInBox[i].type + "_" + markestInBox[i].schedule + "_" + markestInBox[i].market_id;
			}else{
				group[i] = markestInBox[i].type + "_" + markestInBox[i].market_id;
			}
			box_objects[i].market = {'market_id':markestInBox[i].market_id,'reset_market_id':markestInBox[i].market_id,'schedule':markestInBox[i].schedule,'type':markestInBox[i].type};
			box_objects[i].boxFull = true;
		}
	}
	tradingBox = connectToLS({'name':'tradingBox','subscriptionType':subscriptionType,'group':group,'schema':schema,'dataAdapterName':dataAdapterName,'callBack':ls_updateItem});
}
function searchJsonKeyInArray(arr,key,val){
	for(var i=0; i<arr.length; i++){
		if(arr[i][key] == val){
			return i;
		}
	}
	return -1;
}
function ls_getMarketName(marketId) {//from tradePage.js
	if (marketsDisplayName[marketId]) {
		return marketsDisplayName[marketId];
	}
	return "";
}
var showMarketsMenu_box = 0;
function showMarketsMenu(obj,box,evt) {//from menuMarketSelect.js
	g('marketsMenuFilter').value = g('marketsMenuFilter').defaultValue;
	showMarketsMenu_box = box;
	var treeM = g('treeM');
	selectActiveMarkets(treeM,box);
	absolutePosition(obj,treeM,'bl');
	treeM.style.width = obj.offsetWidth-2+"px";
	openElement({'id':'treeM','autoClose':true})
	g('marketsMenuFilter_li').style.position = "absolute";
	setTimeout(function(){g('marketsMenuFilter').focus();},100);
}
function selectActiveMarkets(el,box) {//from menuMarketSelect.js
	var ls_marketStates_rev = [];
	for(j=0;j<ls_marketStates.length;j++) {
		ls_marketStates_rev[ls_marketStates[j]] = 1;
	}
	var lis = el.getElementsByTagName("li");
	var marketId = box_objects[box].market.market_id;
	var last_group_close = null, last_subgroup_close = null, subGroup_els = [], subGroup_els_all = 0;

	for(var i=0; i<lis.length; i++){
		lis[i].style.display = "block";

		if(lis[i].getAttribute('data-close') != null){
			continue;
		}
		var group_el = lis[i].getAttribute('data-group');
		if(group_el != null){
			last_group_close = group_el;
			if(subGroup_els.length == subGroup_els_all){
				for(var n=0; n<subGroup_els.length; n++){
					if(subGroup_els[n] != ''){
						g(subGroup_els[n] + '_title').style.display = "none";
						g(subGroup_els[n]).style.display = "none";
					}
				}
			}
			subGroup_els = [];
			subGroup_els_all = 0;
			continue;
		}
		var subgroup_el = lis[i].getAttribute('data-subgroup');
		if(subgroup_el != null){
			last_subgroup_close = subgroup_el;
			var subGroup_els_last = subGroup_els.length;
			subGroup_els[subGroup_els.length] = last_subgroup_close;
			subGroup_els_all++;
			continue;
		}
		var optionMarketId = lis[i].getAttribute('data-marketId');
		if(optionMarketId != null){
			if(ls_marketStates_rev[optionMarketId] == null){
				lis[i].style.display = "none";
				lis[i].setAttribute('data-search', '');
			}
			else{
				subGroup_els.splice(subGroup_els_last,1);
				lis[i].setAttribute('data-search', 't');
				lis[i].className = lis[i].className.replace(/ active/g,'');
				if(marketId == optionMarketId) {
					lis[i].className += ' active';
				}
				g(last_group_close).style.display = "none";
				if(last_subgroup_close != null){
					g(last_subgroup_close).style.display = "none";
				}
			}
		}
	}
}
function selectMarket(marketId, killAdvancedSearch, reseted) {
	if (isUndefined(killAdvancedSearch)) {
		if (angular.element('#BinaryOptionController').length > 0) {
			angular.element('#BinaryOptionController').scope().globalMarketDelete(false);
		}
	}
	closeElement({'last':true});
	if(box_objects[showMarketsMenu_box].marketId != marketId && !box_objects[showMarketsMenu_box].inProgress) {
		var boxId = marketToBox(marketId, reseted);
		if(boxId != -1){
			alert(marketMenuError);
			return false;
		}
		boxId = showMarketsMenu_box;
		if(box_objects[boxId].boxType  == '1'){
			g('tradeBox_marketName_'+boxId).innerHTML = ls_getMarketName(marketId);
		}else{
			g('profitLine_marketDrop_title_txt_'+boxId).innerHTML = ls_getMarketName(marketId);
		}
		box_objects[boxId].tradeBox_level_html.innerHTML = txt_loading;
		
		g('tradeBox_chart_longTerm_'+boxId).style.display = "none";
		g('tradeBox_chart_main_'+boxId).style.display = "block";

		var schedule = 1;
		box_objects[boxId].resetBox(true);
		box_objects[boxId].market.market_id = marketId;
		box_objects[boxId].market.reset_market_id = marketId;
		box_objects[boxId].market.schedule = schedule;
		box_objects[boxId].marketIdManual = true;
		box_objects[boxId].marketId = marketId;
		box_objects[boxId].boxOpportunities = [];
		box_objects[boxId].marketChanged = true;
		
		if(box_objects[boxId].market.type == 'aotps'){
			group[boxId] = box_objects[boxId].market.type + "_"+schedule + "_" + marketId;
		}else{
			group[boxId] = box_objects[boxId].market.type + "_"+ marketId;
		}

		lsClient.unsubscribe(tradingBox);
		tradingBox = connectToLS({'name':'tradingBox','subscriptionType':subscriptionType,'group':group,'schema':schema,'dataAdapterName':dataAdapterName,'callBack':ls_updateItem});
		
		//when we change market - the default for the schedule will be the closest
		if (isLiveAOPage) {
			fromGraph = 2; //Live page - trade box.
		} else {
			fromGraph = 0; //No from LIVE-AO, profitline, option + or binary 0-100
		}
		return true;
	} else {
		return false;
	}
}
var profitLineBox = {'box':null,'duplicateOf':null};
function openProfitLine_box_callBack(response){
	var resp = eval('('+response+')');
	openProfitLine_box(resp.settings.marketId, resp.settings.opportunityId);
}
function openProfitLine_box(marketId, oppId){
	var profitLine_h = g('profitLine_h');
	if(profitLine_h != null){
		window.scrollTo(0,0);
		g('profitLine_h_bgr').style.display = "block";
		g('profitLine_h').style.display = "block";
		if(marketId == '0'){
			var url = context_path + "/jsp/xml/profitLineData.jsf?marketId=0&oppId=0&typeId=1";
			var ajaxCall = new aJaxCall_fn({'url':url,'callback':openProfitLine_box_callBack});
			ajaxCall.doGet();
			return true;
		}
		var boxId = profitLineBox.box = profitLineBoxId;
		tradeBox_getBalace(boxId);
		var groupId = null;
		for(i = 0; i < group.length; i++){
			var market = group[i].split("_");//example:aotps_1_5
			if((market[1] == 1) && (market[2] == marketId)){
				groupId = i;
				break;
			}
		}	
		if(groupId == null){
			var type = 'aotps';
			profitLineBox.duplicateOf = null;
			box_objects[boxId].market = {'market_id':marketId,'reset_market_id':marketId,'schedule':1,'type':type};
			box_objects[boxId].marketIdManual = true;
			box_objects[boxId].marketId = marketId;
			box_objects[boxId].boxOpportunities = [];
			box_objects[boxId].resetBox(false);
			if (!isUndefined(oppId)) {
				box_objects[boxId].oppId = oppId;
			}
			box_objects[boxId].marketChanged = true;
			if(type == 'aotps'){
				var group_el = type + "_1_" + marketId;
			}else{
				var group_el = type + "_" + marketId;
			}
			
			group.push(group_el);
			if(!lsEngineReady){
				lsSubscribeParams.group_tradeBox = group;
				connectToLS_global(lsSubscribeParams)
			}
			else{
				lsClient.unsubscribe(tradingBox);
				tradingBox = connectToLS({'name':'tradingBox','subscriptionType':subscriptionType,'group':group,'schema':schema,'dataAdapterName':dataAdapterName,'callBack':ls_updateItem});
			}
		}
		else{
			var oppArrPos = searchJsonKeyInArray(box_objects[groupId].boxOpportunities, 'ET_OPP_ID', oppId);
			if (oppArrPos < 0) {
				oppArrPos = 0;
			}
			
			profitLineBox.duplicateOf = groupId;
			box_objects[boxId].resetBox(false);
			box_objects[boxId].changeStates({'state':''});
			box_objects[boxId].market = cloneObj(box_objects[groupId].market);
			box_objects[boxId].marketIdManual = true;
			box_objects[groupId].marketIdManual = true;
			box_objects[groupId].duplicatedTo = boxId;
			box_objects[boxId].boxFull = true;
			box_objects[boxId].marketId = cloneObj(box_objects[groupId].marketId);
			if (!isUndefined(oppId)) {
				box_objects[boxId].oppId = oppId;
			}
			box_objects[boxId].oppArrPos = oppArrPos;
			
			box_objects[boxId].profit = cloneObj(box_objects[groupId].profit);
			box_objects[boxId].refund = cloneObj(box_objects[groupId].refund);
			box_objects[boxId].boxOpportunities = cloneObj(box_objects[groupId].boxOpportunities);
			
			if (box_objects[boxId].boxOpportunities.length > 0) {
				box_objects[boxId].timeLastInv_mil = adjustFromUTCToLocal(new Date(box_objects[boxId].boxOpportunities[oppArrPos].ET_LAST_INV)).getTime();
				box_objects[boxId].timeClosing_mil = adjustFromUTCToLocal(new Date(box_objects[boxId].boxOpportunities[oppArrPos].ET_EST_CLOSE)).getTime();
				
				if(box_objects[boxId].chart == null){
					box_objects[boxId].initGraph();
				}
			}
			
			
			var updateValueChanges = {};
			for (var j = 0; j < schema.length; j++) {
				updateValueChanges[schema[j]] = box_objects[groupId].lastUpdateInfo.isValueChanged(schema[j]);
			}
			ls_updateItemNonVisual(boxId, '', box_objects[groupId].lastUpdateInfo, updateValueChanges);
			
			g('profitLine_marketDrop_title_txt_'+boxId).innerHTML = box_objects[boxId].boxOpportunities[box_objects[boxId].oppArrPos].ET_NAME+' - '+formatDateHours(box_objects[boxId].boxOpportunities[box_objects[boxId].oppArrPos].ET_EST_CLOSE,true);
		}
	}
	else{
		// window.location = context_path + tradePageN+"?profitLineMarketId="+marketId;
		function getProfitLine_calBack(response){
			var div = document.createElement('div');
			div.innerHTML = response;
			document.body.appendChild(div);
			
			profitLineBoxId = boxId;
			box_objects[boxId] = new box_obj({
				'box':boxId,
				'invest': defaultInvest,
				'boxType':2
			});
			openProfitLine_box(marketId);
		}
		
		addScriptToHead('highstock.js','highstock')
		var boxId = group.length;
		var url = context_path + "/jsp/html_elements/profitLinePopUp.jsf?s=" + skinId + "&boxId=" + boxId;
		var ajaxCall = new aJaxCall_fn({'url':url,'callback':getProfitLine_calBack});
		ajaxCall.doGet();
	}
}
function getBigMarketsDropDown_reset(box){
	var lis = g('profitLine_marketDrop_ul_' + box).getElementsByTagName('li');
	for(var i=1; i<lis.length; i++){
		lis[i].style.display = "block";
	}
}
function formatDateHours(date,addOffset){
	var date = new Date(date);
	if(addOffset){
		date = new Date(date.getTime()-new Date().getTimezoneOffset()*60*1000);
	}
	var min = date.getMinutes();
	if (new Number(date.getMinutes()) < new Number("10")) {
		min = "0" + date.getMinutes();
	}
	return date.getHours() + ":" + min;
}
function closeProfitLine_box(){
	if (profitLineBox.box != null) {
		if(profitLineBox.duplicateOf != null){
			box_objects[profitLineBox.duplicateOf].duplicatedTo = null;
			box_objects[profitLineBox.duplicateOf].marketIdManual = false;
		}
		else{
			group.pop();
			lsClient.unsubscribe(tradingBox);
			tradingBox = connectToLS({'name':'tradingBox','subscriptionType':subscriptionType,'group':group,'schema':schema,'dataAdapterName':dataAdapterName,'callBack':ls_updateItem});
		}
		box_objects[profitLineBox.box].resetBox(true);
		// box_objects[profitLineBox.box].market.market_id = '';
		g('profitLine_h').style.display = "none";
		g('profitLine_h_bgr').style.display = "none";
	}
}
var tradeBox_getBalace_boxId = 0;
function tradeBox_getBalace(boxId){
	tradeBox_getBalace_boxId = boxId;
	refreshHeader();
	var url = context_path + "/jsp/xml/profitLineBalance.jsf";
	var ajaxCall = new aJaxCall_fn({'url':url,'callback':tradeBox_getBalace_callBack});
	ajaxCall.doGet();
}
function tradeBox_getBalace_callBack(response){
	var resp = eval('('+response+')');
	g('profitLine_balance_val_' + tradeBox_getBalace_boxId).innerHTML = resp.balance;
}
var refreshHeader_timer = null;
function refreshHeader(){
	// g('header').contentWindow.location.reload(true);
	clearTimeout(refreshHeader_timer);
	refreshHeader_timer = setTimeout(doIt,1000);
	function doIt(){
		g('header').src = g('header').src;
	}
}
function refreshInvProfitBox(){
	var invBox = document.getElementById('right_side_investments');
	if (invBox != null) {
		invBox.contentWindow.location.reload();
	}
}
//print slip success
function printSuccess(invId) {
	window.open(context_path + "/jsp/single_pages/invSuccessPrint.jsf?investmentId=" + invId, "invSuccessPrint", "width=498,height=427,menubar=1,toolbar=0,scrollbars=0");
}
//connect to ls
var lsEngineReady = false,lsClient,subscriptionType,dataAdapterName,lsSubscribeParams = {},
	tradingBox,group,schema,
	lsShoppingBag,groupShoppigBag,schemaShoppigBag,
	lsOptionPlus,groupOptionPlus,schemaOptionPlus,
	lsTrends,groupTrends,schemaTrends;
function connectToLS(params){//['name':'','subscriptionType':subscriptionType,'group':group,'schema':schema,'dataAdapterName':dataAdapterName]
	logIt({'type':1,'msg':'connectToLS: '+params.name});
	var obj = new Subscription(params.subscriptionType,params.group,params.schema);
	obj.setDataAdapter(params.dataAdapterName);
	obj.setRequestedSnapshot("yes");
	obj.setRequestedMaxFrequency(1.0);
	obj.addListener({
		  onSubscription: function() {
			  logIt({'type':1,'msg':'SUBSCRIBED: '+params.name});
		  },
		  onUnsubscription: function() {
			  logIt({'type':1,'msg':'UNSUBSCRIBED: '+params.name});
		  },
		  onItemUpdate: function (updateObject) { 
			logIt({'type':1,'msg':'ItemUpdate: '+params.name});
			params.callBack(updateObject);
		  }
		});
	lsClient.subscribe(obj);
	return obj;
}
function connectToLS_global(params){
	lsSubscribeParams = params;
	lsClient = configureLsConnection();
	lsClient.connect();
	lsEngineReady = true;
	//trading boxes
	subscriptionType = "COMMAND";
	dataAdapterName = "JMS_ADAPTER";
	group = params.group_tradeBox;
	//REMOVE ET_GROUP,ET_SCHEDULED
	//CHECK ET_GROUP_CLOSE
	schema = ["key", "command", "ET_NAME", level_skin_group_id, "ET_ODDS", "ET_EST_CLOSE", "ET_OPP_ID", "ET_ODDS_WIN", "ET_ODDS_LOSE", "ET_STATE", level_color, "ET_PRIORITY", 
		"ET_GROUP_CLOSE", "ET_SCHEDULED", "ET_RND_FLOOR", "ET_RND_CEILING", "ET_GROUP", "ET_SUSPENDED_MESSAGE", "AO_HP_MARKETS_" + skinId, "AO_STATES", "ET_LAST_INV", 
		"AO_TS", "AO_FLAGS", "ET_ODDS_GROUP", ao_calls_trend, "FILTER_NEW_SCHEDULED", "AO_OPP_STATE","ASK", "BID", "LAST", "AO_NH", "AO_NQ"];
		
	if(typeof params.group_tradeBox == 'object'){
		var marketId = getUrlValue('marketId');
		if (typeof params.fixedBox != 'undefined' && params.fixedBox) {
			box_objects[0].marketIdManual = true;
		} else if(marketId != "" && group.indexOf('aotps_1_'+marketId) == -1){
			group[0] = 'aotps_1_'+marketId;
			box_objects[0].marketIdManual = true;
			box_objects[0].oppId = 0;
		}
		tradingBox = connectToLS({'name':'tradingBox','subscriptionType':subscriptionType,'group':group,'schema':schema,'dataAdapterName':dataAdapterName,'callBack':ls_updateItem});				
	}
	if((typeof params.group_shoppingBag == 'object') && (userLogin)){
		groupShoppigBag = params.group_shoppingBag;
		schemaShoppigBag = ["key", "command", "SB_MARKET_ID", "SB_OPP_TYPE", "SB_LEVEL", "SB_AMOUNT", "SB_TIME_CLOSE"];
			
		lsShoppingBag = connectToLS({'name':'lsShoppingBag','subscriptionType':subscriptionType,'group':groupShoppigBag,'schema':schemaShoppigBag,'dataAdapterName':dataAdapterName,'callBack':ls_shoppingBag});				
	}
	if(typeof params.group_optionPlus == 'object'){
		group = params.group_optionPlus;
		if(group[0] == 'op_0'){
			g('markets_table').style.display = 'none';
			g('off_hours_banner').style.display = 'block';
		}
		else{
			var marketId = getUrlValue('marketId');
			if(marketId != ""){
				group[0] = 'op_'+marketId;
			}
			tradingBox = connectToLS({'name':'tradingBox','subscriptionType':subscriptionType,'group':group,'schema':schema,'dataAdapterName':dataAdapterName,'callBack':ls_updateItem});				
		}
	}
}

//shopping bag
var shopingBagArr = [];
function isExistOppInSB(oppId){
	var res = false;
	for (var i = 0; i < shopingBagArr.length; i++) {
		if(shopingBagArr[i].oppId === oppId) {
			res = true;
		}
	}
	return res;
}
function addOrDeleteShoppingBagArr(shopingBagObj){
	if(shopingBagObj.command.toLowerCase() != "delete"){
		if(!isExistOppInSB(shopingBagObj.oppId)){
			shopingBagArr.push(shopingBagObj);
			sortShoppingBagArr();
		}
	} else {
		shopingBagArr.length = 0;		
	}
}
function sortShoppingBagArr(){
		shopingBagArr.sort(function(a, b) {
		var marketNameA=a.marketName.toLowerCase(), marketNameB=b.marketName.toLowerCase();
		var oppTypeA=a.oppType, oppTypeB=b.oppeTyp;	
		
		if(oppTypeA == 2){
			oppTypeA = 99;
		}
		
		if(oppTypeB == 2){
			oppTypeA = 99;
		}
				    
	    if (marketNameA > marketNameB) return  1;
		if (marketNameA < marketNameB) return -1;
		
		if (oppTypeA > oppTypeB) return  1;
		if (oppTypeA < oppTypeB) return -1;
	});
}
function formatHM(hourMin){
	var hm = hourMin;
	if (hm < 10) {
		hm = '0' + hm;
	} else {
		hm = hm + '';
	}
	return hm;
}
function getCloseTimeShopingBag(timeClose){
	var targetTime = new Date(timeClose);	
	var timeZoneOfSet = targetTime.getTimezoneOffset();
	targetTime.setMinutes(targetTime.getMinutes() + (timeZoneOfSet*-1));
	return formatHM(targetTime.getHours()) + ":" + formatHM(targetTime.getMinutes());
}
function getFormatShopingBagAmont(amount){
	var res = "";
	if(amount != 0){
		res = formatAmount(amount, 2, true);
	}	
	return  res;
}
function getSBRedirectUrl(marketId, oppType){
	var redirectUrl;
	switch (oppType)
  {
  case "1":
    redirectUrl = tradePageN + "?marketId=" + marketId;
    break;
  case "2":
    redirectUrl = oneTouchPageN + "?marketId=" + marketId;
    break;
  case "3":
    redirectUrl = optionPlusPageN + "?marketId=" + marketId;
    break;
  case "4":
    redirectUrl = binary0100PageN + "?marketId=" + marketId;
    break;
  case "5":
    redirectUrl = binary0100PageN + "?marketId=" + marketId;
    break;
  case "7":
    redirectUrl = dynamicsPageN + "?marketId=" + marketId;
    break;
  }
  
  return redirectUrl;
}
function closeShopingBag(){
	g('shop_bag_popUp').style.top = -g('shop_bag').offsetHeight+'px';
	lsClient.sendMessage(groupShoppigBag);
}
function showShoppingBag(){
	if(shopingBagArr.length > 0){
		refreshHeader();
		var offset=(skinId == 15)?114:0;
		g('shop_bag_popUp').style.top = offset+'px';
		var  shop_bag_info_holder = g('shop_bag_info_holder');
		shop_bag_info_holder.innerHTML='';
		var oppTypeClass = '';
		for (var i = 0; i < shopingBagArr.length; i++) {
			switch (shopingBagArr[i].oppType) {
			  case "1":oppTypeClass = 'inv_binary';break;//BO
			  case "2":oppTypeClass = 'inv_1touch';break;//onetouch
			  case "3":oppTypeClass = 'inv_plus inv_plus_shop_bag';break;//optionPlus
			  case "4":oppTypeClass = 'inv_binary_0_100';break;//0100
			  case "5":oppTypeClass = 'inv_binary_0_100';break;//0100
			  case "7":oppTypeClass = 'inv_dynamics';break;//dynamics
			}
			shop_bag_info_holder.innerHTML += '<div class="shop_bag_info"><a href="'+shopingBagArr[i].redirectUrl+'"><span class="shop_bag_asset">'+shopingBagArr[i].marketName+'</span><span class="shop_bag_icon inv_p '+oppTypeClass+'"></span><span class="shop_bag_expiry_level">'+shopingBagArr[i].level+'</span></a></div>';
			
			if(shopingBagArr[i].amount === ''){
				g('shop_bag_amount_currency').style.display = "none";
			}else{
				g('shop_bag_amount_currency').style.display = "block";
				g('shop_bag_txt_amount').innerHTML = shopingBagArr[i].amount;
			}
			g('shop_bag_time_expiry').innerHTML = shopingBagArr[i].timeClose;
		}
	}
	else{
		g('shop_bag_popUp').style.top = -g('shop_bag').offsetHeight+'px';
	}
}
function ls_shoppingBag(updateInfo){	
	shopingBag = new Object();
	shopingBag.oppId =  updateInfo.getValue(1);
	shopingBag.command =  updateInfo.getValue(2);
	shopingBag.marketId = updateInfo.getValue("SB_MARKET_ID");
	shopingBag.marketName = ls_getMarketName(updateInfo.getValue("SB_MARKET_ID"));
	shopingBag.oppType = updateInfo.getValue("SB_OPP_TYPE");
	shopingBag.level = updateInfo.getValue("SB_LEVEL");
	shopingBag.amount = getFormatShopingBagAmont(updateInfo.getValue("SB_AMOUNT"));
	shopingBag.timeClose = getCloseTimeShopingBag(updateInfo.getValue("SB_TIME_CLOSE"));
	shopingBag.redirectUrl = getSBRedirectUrl(updateInfo.getValue("SB_MARKET_ID"), updateInfo.getValue("SB_OPP_TYPE"));
		
	addOrDeleteShoppingBagArr(shopingBag);
	showShoppingBag();
	return true;
}

//how to banner
function show_how_to_banner(el){
	var stete = el.getAttribute('data-stete');
	if(stete == 'close'){
		el.className += " open";
		g('guide_notes_overlay_container').style.display = "block";
		g('tradeBox_main_0').style.opacity="0.3";
		g('tradeBox_main_0').style.filter="alpha(opacity=30)";
		el.setAttribute('data-stete','open');
	}else{
		el.className = el.className.replace(/ open/g,'');
		g('guide_notes_overlay_container').style.display = "none";
		g('tradeBox_main_0').style.opacity="1";
		g('tradeBox_main_0').style.filter="alpha(opacity=100)";
		el.setAttribute('data-stete','close');
	}
}
function sendToTradeBox(info){
	var flag = false;
	var td = info.parentNode;
	var tr = td.parentNode;
	var assetTD = getChildOfType(tr, "TD", 3);
	var assetId = getChildOfType(assetTD, "SPAN", 1).innerHTML;
	var oppId = getChildOfType(assetTD, "SPAN", 2).innerHTML;
	var invTypeTD = getChildOfType(tr, "TD", 4);
	var invTypeId = getChildOfType(invTypeTD, "SPAN", 1).innerHTML;
	fromGraph = 4; //Live Trades page.
	showMarketsMenu_box = 0;
	selectMarket(assetId);
}
function loadMarketInBox(marketId,box){
	if(typeof group == 'object'){
		var market_type = group[0].split('_')[0];
		if(market_type == 'aotps'){
			window.scrollTo(0,0);
			if((marketId == undefined) || (marketId == null)){
				marketId = getUrlValue('marketId');
			}
			if((box == undefined) || (box == null)){
				box = 0;
			}
			if(marketId != ""){
				showMarketsMenu_box = box;
				selectMarket(marketId);
			}
		}
		else{
			loadMarketInBox_redirect(marketId);
		}
	}
	else{
		loadMarketInBox_redirect(marketId);
	}
}
function loadMarketInBox_redirect(marketId){
	window.location = context_path + tradePageN+"?marketId="+marketId;
}
function getUrlValue(key){
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
		if(key == pair[0]){
			return decodeURIComponent(pair[1]);
		}
    }
    return '';
}
function cloneObj(obj){
    if(obj == null || typeof(obj) != 'object')
        return obj;

    var temp = obj.constructor(); // changed

    for(var key in obj)
        temp[key] = cloneObj(obj[key]);
    return temp;
}
var filterDropDowns_current = '';
var filterDropDowns_current_fnc = '';
function filterDropDowns(e, resetFnc){
	e = e || window.event;
	var obj = e.target || e.srcElement;
	
	var arrPressed = false;
	if(e.keyCode == '38'){//up
		filterDropDowns_current = (filterDropDowns_current === '' || filterDropDowns_current == 0)?0:--filterDropDowns_current;
		arrPressed = true;
	}
	else if(e.keyCode == '40'){//down
		filterDropDowns_current = (filterDropDowns_current === '')?0:++filterDropDowns_current;arrPressed = true;
	}
	else if(e.keyCode == '13'){//enter
		eventFire(filterDropDowns_current_fnc,'click');
	}
	else{
		filterDropDowns_current = '';
	}
	
	var visible_li_num = 0;
	var li = obj.parentNode;
	li.style.position = 'static';
	var list = li.parentNode.getElementsByTagName("li");
	var inp_val = obj.value.toLowerCase();
	var last_in_list = [];
	if(inp_val.length > 0 || (inp_val.length == 0 && resetFnc == "") || arrPressed){
		for (var i = 1; i < list.length; i++) {
			if(list[i].getAttribute('data-search') != null && list[i].getAttribute('data-search') != ''){
				var match = list[i].innerHTML.replace(/(<([^>]+)>)/ig,"").toLowerCase();
				if(inp_val.length > 0){
					if(inp_val == match.substring(0,inp_val.length)){
						list[i].style.display = 'block';
						
						if(filterDropDowns_current == visible_li_num && arrPressed){
							list[i].className += ' active';
							(filterDropDowns_current_fnc = list[i]);
						}
						else if(arrPressed){
							list[i].className = list[i].className.replace(/ active/g,'');
						}
						last_in_list[visible_li_num] = list[i];
						visible_li_num++;
					}
					else{
						list[i].style.display = 'none';
					}
				}
				else{
					if(resetFnc == ""){
						list[i].style.display = 'block';
					}
					if(filterDropDowns_current == visible_li_num && arrPressed){
						var ul
						var visTop = list[i].parentNode.offsetHeight + li.scrollTop - list[i].offsetHeight;
						list[i].scrollIntoView(false);

						list[i].className += ' active';
						(filterDropDowns_current_fnc = list[i]);
					}
					else if(arrPressed){
						list[i].className = list[i].className.replace(/ active/g,'');
					}
					last_in_list[visible_li_num] = list[i];
					visible_li_num++;
				}
			}
			else if(!arrPressed){
				list[i].style.display = 'none';
			}
		}
		if(filterDropDowns_current >= visible_li_num && arrPressed){
			last_in_list[last_in_list.length - 1].className += ' active';
			filterDropDowns_current = last_in_list.length-1;
		}
	}
	else if(resetFnc != ""){
		eval(resetFnc);
	}
}
function detectCCtype(card, fullCheck){
	var cardTypes = {
		unknown: 'unknown',
		visa: 2,
		mastercard: 1,
		solo: 'solo',
		switch_type: 'switch',
		maestro: 6,
		amex: 5,
		discover: 'discover',
		diners: 3,
		isracard: 4,
		allcards: 'allcards',
		cup: 7
	}
	card = card.toString().replace(/-/g,'').replace(/ /g,'');
	//visa - starts with 4
	var part = parseInt(card.substr(0, 1));
	if(part == 4){
		if(fullCheck){
			if((card.length == 13 || card.length == 16) && luhnValidate(card)){
				return cardTypes.visa;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.visa;
	}
	//Mastercard - starts with 51,52,53,54,55
	var part = parseInt(card.substr(0, 2));
	if(part >= 51 && part <= 55){
		if(fullCheck){
			if(card.length == 16 && luhnValidate(card)){
				return cardTypes.mastercard;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.mastercard;
	}
	//Solo - starts with 6334,6767
	var part = parseInt(card.substr(0, 4));
	if(part == 6334 || part == 6767){
		if(fullCheck){
			if(card.length >= 16 && card.length <= 19 && luhnValidate(card)){
				return cardTypes.solo;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.solo;
	}
	//Switch - starts with 4903,4905,4911,4936,6333,6759 or 564182,633110
	var part_arr = [4903,4905,4911,4936,6333,6759];
	var part2_arr = [564182,633110];
	var part = parseInt(card.substr(0, 4));
	var part2 = parseInt(card.substr(0, 6));
	if(part_arr.indexOf(part) > -1 || part2_arr.indexOf(part2) > -1){
		if(fullCheck){
			if(card.length >= 16 && card.length <= 19 && luhnValidate(card)){
				return cardTypes.switch_type;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.switch_type;
	}
	//Maestro - starts with 50,56,57,58,59,60,62,63,64,67,90
	var part_arr = [50,56,57,58,59,60,62,63,64,67,90];
	var part = parseInt(card.substr(0, 2));
	if(part_arr.indexOf(part) > -1){
		if(fullCheck){
			if(card.length >= 16 && card.length <= 18 && luhnValidate(card)){
				return cardTypes.maestro;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.maestro;
	}
	//Amex - starts with 34,37
	var part_arr = [34,37];
	var part = parseInt(card.substr(0, 2));
	if(part_arr.indexOf(part) > -1){
		if(fullCheck){
			if(card.length == 15 && luhnValidate(card)){
				return cardTypes.amex;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.amex;
	}
	//Discover - starts with 6011
	var part_arr = [6011];
	var part = parseInt(card.substr(0, 4));
	if(part_arr.indexOf(part) > -1){
		if(fullCheck){
			if(card.length == 16 && luhnValidate(card)){
				return cardTypes.discover;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.discover;
	}
	//Diners - starts with 300,301,302,303,304,305 or 36
	var part_arr = [300,301,302,303,304,305];
	var part2_arr = [36];
	var part = parseInt(card.substr(0, 3));
	var part2 = parseInt(card.substr(0, 2));
	if(part_arr.indexOf(part) > -1 || part2_arr.indexOf(part2) > -1){
		if(fullCheck){
			if(card.length == 14 && luhnValidate(card)){
				return cardTypes.diners;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.diners;
	}
	//CUP - starts with 62
	var part_arr = [62];
	var part = parseInt(card.substr(0, 2));
	if(part_arr.indexOf(part) > -1){
		if(fullCheck){
			if(card.length >= 16 && card.length <= 19 && luhnValidate(card)){
				return cardTypes.cup;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.diners;
	}
	//Isracard - complicated :)
	if(fullCheck){
		var cardIsr = card;
		if(cardIsr.length == 8){
			cardIsr = "0" + cardIsr;
		}
		else if(cardIsr.length > 9){
			cardIsr = cardIsr.substr(0, 9);
		}
		
		if(cardIsr.length == 9){
			var diff = "987654321",
				sum = 0,
				a = 0,
				b = 0,
				c = 0;
			for(var i=1;i<9;i++){
				sum += parseInt(diff.substr(i,1))*parseInt(cardIsr.substr(i,1));
			}
			if(sum % 11 == 0){
				return cardTypes.isracard;
			}
		}
	}
	//allcard
	if(card.length >= 12 && card.length <= 20 && luhnValidate(card)){
		return cardTypes.allcards;
	}
	return cardTypes.unknown;
}

// The Luhn algorithm is basically a CRC type
// system for checking the validity of an entry.
// All major credit cards use numbers that will
// pass the Luhn check. Also, all of them are based
// on MOD 10.

function luhnValidate(value) {
  // accept only digits, dashes or spaces
	if (/[^0-9-\s]+/.test(value)) return false;
 
	// The Luhn Algorithm. It's so pretty.
	var nCheck = 0, nDigit = 0, bEven = false;
	value = value.replace(/\D/g, "");
 
	for (var n = value.length - 1; n >= 0; n--) {
		var cDigit = value.charAt(n),
			  nDigit = parseInt(cDigit, 10);
 
		if (bEven) {
			if ((nDigit *= 2) > 9) nDigit -= 9;
		}
 
		nCheck += nDigit;
		bEven = !bEven;
	}
 
	return (nCheck % 10) == 0;
}

function detectCCtype_call(el){
	var el_i = el.parentNode.getElementsByTagName('i')[0];
	if(el_i.getAttribute('data-defaultClass') == null){
		el_i.setAttribute('data-defaultClass',el_i.className);
	}
	var ccType = detectCCtype(el.value);
	el_i.className = el_i.getAttribute('data-defaultClass') + " cardType_" + ccType;
	g('newCardForm:typeId').value = ccType;
}
function showAssetListMyInv(el){
	var assetsListMyInv = g('assetsListMyInv');
	absolutePosition(el,assetsListMyInv,'bl');
	assetsListMyInv.style.display = "block";
}
function selectAsset(obj,marketId){
	g("investmentsContainer:selectedAssetNameDisplay").innerHTML = obj.innerHTML;
	g("investmentsContainer:selectedAssetName").value = obj.innerHTML;
	g("investmentsContainer:marketGroup").value = marketId;
	return myfaces.oam.submitForm("investmentsContainer","investmentsContainer:hiddenbtn");
}

//binary 0-100
var startB0100_commisition_timer_first = true;
var startB0100_commisition_timer_obj = null;
function startB0100_commisition_timer(el,reset){
	var iframe = g('openInvestments').contentWindow;
	if(startB0100_commisition_timer_first || startB0100_commisition_timer_obj == null){
		iframe .g('b0100_commission').className += " hidden";
		startB0100_commisition_timer_first = false;		
	}
	if(reset){
		try{
			iframe.g('b0100_commission').className = iframe.g('b0100_commission').className.replace(/ hidden/g,'');
			clearTimeout(startB0100_commisition_timer_obj);
			startB0100_commisition_timer_obj = setTimeout(function(){
				g('openInvestments').contentWindow.g('b0100_commission').className += ' hidden';
				startB0100_commisition_timer_first = true;
				startB0100_commisition_timer_obj = null;
			},10000);
		}catch(e){}
	}
}
//simulate a click
function eventFire(el, etype){
	if (el.fireEvent) {
		(el.fireEvent('on' + etype));
	} else {
		var evObj = document.createEvent('Events');
		evObj.initEvent(etype, true, false);
		el.dispatchEvent(evObj);
	}
}

function beautifySelect(id){
	var sel = g(id);
	var wrapper = sel.parentNode;
	wrapper.innerHTML += "<span id=" + id + "_span>" + sel.options[sel.selectedIndex].text + "</span>";

	if(g(id).addEventListener){
		g(id).addEventListener('change',function() {g(id + '_span').innerHTML = this.options[this.selectedIndex].text;},false);
	}else{
		g(id).attachEvent("onchange", function() {
			g(id + '_span').innerHTML = g(id).options[g(id).selectedIndex].text;}
		);
	}
}
/*OP demo*/
function open_OP_demo(){
	g('OP_demo_popBgr').style.display = "block";
	g('OP_demo_holder').style.display = "block";
	start_OP_demo();
}
function close_OP_demo(){
	g('OP_demo_popBgr').style.display = "none";
	g('OP_demo_holder').style.display = "none";
}
var start_OP_demo_timer = null;
function start_OP_demo(){
	clearTimeout(start_OP_demo_timer);
	var main = g('OP_demo_holder');
	next_OP_demo_curr = 0;
	main.className = main.className.replace(new RegExp(main.getAttribute('data-class'),"g"),'OP_demo_holder_slide_0');
	main.setAttribute('data-class','OP_demo_holder_slide_0');
	var main_nav_li = g('slider1_nav_OP').getElementsByTagName('li');
	for(var i = 0; i < main_nav_li.length; i++){
		main_nav_li[i].className = main_nav_li[i].className.replace(/ hov/g,'');
		main_nav_li[i].className = main_nav_li[i].className.replace(/ passed/g,'');
		if(i == 0){
			main_nav_li[i].className += " hov";
		}
		main_nav_li[i].setAttribute('data-i',i);
		main_nav_li[i].onclick = function(){
			next_OP_demo(this);
		}
	}
	start_OP_demo_timer = setTimeout(function(){next_OP_demo('')},5000);
}
var next_OP_demo_curr = 0;
function next_OP_demo(el){
	clearTimeout(start_OP_demo_timer);
	var next = next_OP_demo_curr;
	if(el != ''){
		next = el.getAttribute('data-i').toString();
	}
	else{
		var lis_l = g('slider1_nav_OP').getElementsByTagName('li').length;
		next++;
		if(next >= lis_l){
			next = 0;
			return;
		}
	}
	if(next_OP_demo_curr != next){
		var main = g('OP_demo_holder');
		main.className = main.className.replace(new RegExp(main.getAttribute('data-class'),"g"),'OP_demo_holder_slide_' + next);
		main.setAttribute('data-class','OP_demo_holder_slide_' + next);
		var main_nav_li = g('slider1_nav_OP').getElementsByTagName('li');
		
		for(var i = 0;i<main_nav_li.length;i++){
			var className = main_nav_li[i].className;
			className = className.replace(/ hov/g,'');
			className = className.replace(/ passed/g,'');
			main_nav_li[i].className = className;
			if(i < next){
				main_nav_li[i].className += " passed";
			}
			else if(i == next){
				main_nav_li[i].className += " hov";
			}
		}
		
		next_OP_demo_curr = next;
	}
	start_OP_demo_timer = setTimeout(function(){next_OP_demo('')},5000);
}

function jsonClone(o1, o2) {
	var json = cloneObj(o1);
	for (var key in o2) {
		json[key] = o2[key];
	}
	return json;
}

$(function(){
	$("*[data-currency-holder='left'] input").css("padding-left", $("*[data-currency-holder='left'] *[data-currency-symbol='1']").outerWidth() + 8);
	$("*[data-currency-holder='right'] input").css("padding-right", $("*[data-currency-holder='right'] *[data-currency-symbol='1']").outerWidth() + 8);
	headerMenu();
	$("html").click(function(event){closeDropdowns(event)});
});

function closeDropdowns(event) {
	if ($(event.target).closest('.opened').length > 0) {
		event.stopPropagation();
	} else {
		$('.opened').find("ul.list").slideUp();
		$('.opened').removeClass('opened');
	}
	return;
}

//////////////////// Regulation checks ////////////////////

function shouldCheckRegulationAO(){
	var result = false;
	if(userLogin && !aoRegulationDone){
		result = true;
	}
	return result;
}

function shouldShowRegulationPopup(callFrom, callback){
	if((skinId != 1) && shouldCheckRegulationAO()){
		$.ajax({
			url     : context_path + "/jsp/single_pages/regulationPopupCheck.jsf?ts="+Date.now(),
			type    : "GET",
			dataType: "text",
			data    : "",
			success : function( data ) {
				//Successful call
				parseRegulationResponse(data, callFrom, callback);
			},
			error   : function( xhr, err ) {
				//Failed call
				logIt({'type':3,'msg':'Failed request for shouldShowRegulationPopup (call from ' + callFrom + '): ' + err});
			}
		});
	}else{
		if (typeof callback != 'undefined') {
			callback();
		}
	}
	return false;
}

function parseRegulationResponse(data, callFrom, callback){
	var noPopup = false;
	var respObj = (typeof data == 'object');
	var dataJSON;
	if(respObj || data.trim() != ""){
		if (!respObj) {
			dataJSON = $.parseJSON(data.trim().replace(/&quot;/gi, '"'));
		} else {
			dataJSON = data;
		}
		if(dataJSON){
			//not used at the moment, it might be returned by the server in the future when the user is PEP
			var CODE_ID_PEP = 307;
			var CODE_ID_TRESHOLD_BLOCK = 308;
			
			if (dataJSON.TRESHOLD_ERROR_MSG != '' && g('treshold_error_msg')) {
				g('treshold_error_msg').innerHTML = g('treshold_error_msg').innerHTML.replace('{0}', dataJSON.TRESHOLD_ERROR_MSG);
			}
			if (dataJSON.IS_TRESHOLD == 'true' && isUndefined(dataJSON.CODE_ID)) {
				dataJSON.CODE_ID = CODE_ID_TRESHOLD_BLOCK;
			}
			if(skinId == 1){
				if(dataJSON.CODE_ID == 103){
					showBinaryOptionsKnowledgePopup();
				}else if(dataJSON.CODE_ID == 104){
					if(!isCapitalQuestionnairePage){
						showCapitalQuestionnairePopup();
					}
				}else if(dataJSON.CODE_ID == 10){
					showCapitalQuestionnaireWeightPopup(dataJSON.CODE_ID);
				}else if(dataJSON.CODE_ID == 11){
					showCapitalQuestionnaireWeightPopup(dataJSON.CODE_ID, dataJSON.ERROR_MSG);
				}else if(dataJSON.CODE_ID == 12){
					showCapitalQuestionnaireWeightPopup(dataJSON.CODE_ID, dataJSON.ERROR_MSG);
				}else{
					logIt({'type':2,'msg':'Unknown code received for shouldShowRegulationPopup (call from ' + callFrom + '): ' + dataJSON});
				}
			}else{
				//skipCheck is used in exclusive cases where the popup should not be showed, such as allowing a user to make a withdraw in case the user is suspended
				var skipCheck = false;
				
				//overridePepPopup is used when the check is after an investment attempt - PEP has priority over other popups then
				var overridePepPopup = ((callFrom == 'trade_call' || callFrom == 'trade_put' || callFrom == 'one_touch' || callFrom == 'binary0100' || callFrom == 'ao_login' || callFrom == 'bubbles' || callFrom == "dynamics call or put") && (typeof dataJSON.IS_PEP != 'undefined') && (dataJSON.IS_PEP == 'true'));
				var overrideTreshHoldPopup = ((callFrom == 'trade_call' || callFrom == 'trade_put' || callFrom == 'one_touch' || callFrom == 'binary0100' || callFrom == 'ao_login' || callFrom == 'bubbles') && (typeof dataJSON.IS_TRESHOLD != 'undefined') && (dataJSON.IS_TRESHOLD == 'true'));
				
				if((dataJSON.CODE_ID == 305 || dataJSON.CODE_ID == 313) && ((callFrom == "withdraw_page") || (callFrom == "withdraw_submit"))){
					skipCheck = true;
				}
				if((dataJSON.CODE_ID == 306) && ((callFrom != "ao_login") && (callFrom != "ao_questionnaire_submitted") && (callFrom != "ao_is_knowledge_confirmed") && (callFrom != "receipt_first_deposit_page") && (callFrom != "receipt_deposit_page") && (callFrom != "powerpay") && !overridePepPopup)){
					skipCheck = true;
				}
				if ((dataJSON.CODE_ID == 301 || dataJSON.CODE_ID == 313) && (callFrom == 'withdraw_submit' || callFrom == 'withdraw_page')) {
					skipCheck = true;
				}
				
				if(!skipCheck){
					if (dataJSON.CODE_ID == 314 || dataJSON.CODE_ID == 315) {
						showAORegulationStatusPopup(dataJSON.CODE_ID, dataJSON.userMessage, callFrom);
						closeLowBalancePopup();
					} else if(dataJSON.CODE_ID == 209 || dataJSON.CODE_ID == 419 || dataJSON.CODE_ID == 309){ //low banace, not login, only bonus money
						showAORegulationStatusPopup(dataJSON.CODE_ID, dataJSON.userMessage,callFrom);
					} else if(dataJSON.CODE_ID == 300){ //REGULATION_MISSING_QUESTIONNAIRE
						if(!isSingleQuestionnairePage){ //No need to show the popup with the questionnaire if we are on the page of the questionnaire
							showSingleQuestionnairePopup();
						}
					}else if(dataJSON.CODE_ID == 301){ //REGULATION_SUSPENDED
						showAORegulationStatusPopup(dataJSON.CODE_ID, dataJSON.ERROR_MSG);
					}else if(dataJSON.CODE_ID == 302){ //REGULATION_SUSPENDED_DOCUMENTS
						if (overridePepPopup){ //PEP has priority in case of documents
							showAORegulationStatusPopup(CODE_ID_PEP);
						} else if (overrideTreshHoldPopup) {
							showAORegulationStatusPopup(CODE_ID_TRESHOLD_BLOCK);
						}else{
							showDocumentsPopup(dataJSON.CODE_ID);
						}
					}else if(dataJSON.CODE_ID == 303){ //USER_NOT_REGULATED
						showAORegulationStatusPopup(dataJSON.CODE_ID, dataJSON.ERROR_MSG);
					}else if(userInfo.countryId == countries.spain && (dataJSON.CODE_ID == 304 || dataJSON.CODE_ID == 305)){ //suspended due to cnmv
						showAORegulationStatusPopup('suspended-cnmv', dataJSON.ERROR_MSG);
					}else if(dataJSON.CODE_ID == 313){ //suspended due to cnmv - require documents
						showAORegulationStatusPopup(dataJSON.CODE_ID, dataJSON.ERROR_MSG);
					}else if(dataJSON.CODE_ID == 304){ //REGULATION_USER_RESTRICTED
						showAORegulationStatusPopup(dataJSON.CODE_ID, dataJSON.ERROR_MSG);
					}else if(dataJSON.CODE_ID == 305){ //REG_SUSPENDED_QUEST_INCORRECT
						showAORegulationStatusPopup(dataJSON.CODE_ID, dataJSON.ERROR_MSG);
					}else if(dataJSON.CODE_ID == 306){ //SHOW_WARNING_DUE_DOCUMENTS
						if (overridePepPopup){ //PEP has priority in case of documents
							showAORegulationStatusPopup(CODE_ID_PEP);
						} else if (overrideTreshHoldPopup) {
							showAORegulationStatusPopup(CODE_ID_TRESHOLD_BLOCK);
						}else{
							showDocumentsPopup(dataJSON.CODE_ID);
						}
					}else if(overridePepPopup){ //The call was from investment and PEP is true
						showAORegulationStatusPopup(CODE_ID_PEP);
					}else if(overrideTreshHoldPopup){ //The call was from investment and tresh hold is true
						showAORegulationStatusPopup(CODE_ID_TRESHOLD_BLOCK);
					}else if(!overridePepPopup && (typeof dataJSON.IS_PEP != 'undefined') && dataJSON.IS_PEP && (callFrom == "ao_questionnaire_submitted" || callFrom == "ao_is_knowledge_confirmed")){ //Show PEP popup if the call is not from investment AND it is after submitting questionnaire or after restricted popup
						showAORegulationStatusPopup(CODE_ID_PEP);
					}else if(!overrideTreshHoldPopup && (typeof dataJSON.IS_TRESHOLD != 'undefined') && dataJSON.IS_TRESHOLD && (callFrom == "ao_questionnaire_submitted" || callFrom == "ao_is_knowledge_confirmed")){ //Show tresh hold popup if the call is not from investment AND it is after submitting questionnaire or after restricted popup
						showAORegulationStatusPopup(CODE_ID_TRESHOLD_BLOCK);
					}else if(!overridePepPopup && (typeof dataJSON.IS_PEP != 'undefined') && dataJSON.IS_PEP){ // do not show popup for deposit/withdraw/etc.
						noPopup = parseRegulationResponseSkipPopup(callback);
					}else if(!overrideTreshHoldPopup && (typeof dataJSON.IS_TRESHOLD != 'undefined') && dataJSON.IS_TRESHOLD){ // do not show popup for deposit/withdraw/etc.
						noPopup = parseRegulationResponseSkipPopup(callback);
					}else{
						logIt({'type':2,'msg':'Unknown code received for shouldShowAORegulationPopup (call from ' + callFrom + '): ' + dataJSON});
					}
				}else{
					noPopup = parseRegulationResponseSkipPopup(callback, true);
				}
			}
		}else{
			//Failed response
			logIt({'type':3,'msg':'Unknown response received for shouldShowRegulationPopup (call from ' + callFrom + '): ' + data.trim().replace(/&quot;/gi, '"')});
		}
	}else{
		noPopup = parseRegulationResponseSkipPopup(callback);
	}
	return noPopup;
}
function parseRegulationResponseSkipPopup(callback, skipWithdrawUserDetails){
	var txid = getUrlValue('txid');
	if ((justLoggedInInt == 1 || txid != '') && !isAfterKnowledgeCheckPopUp && (isUndefined(skipWithdrawUserDetails) || !skipWithdrawUserDetails)) {
		function exec() {
			if (!isUndefined(angular.element('#bootstrapId').scope())){
				angular.element('#bootstrapId').scope().showWithdrawUserDetails();
			} else {
				setTimeout(function() {
					exec();
				}, 500);
			}
		}
		exec();
	}
	if (typeof callback != 'undefined') {
		callback();
	}
	return true;
}
var isAfterKnowledgeCheckPopUp = false;
function sendIsKnowledgeCheck(flag){
	var isKnowledge = flag;
	var data = {userId: userInfo.id, isKnowledge: isKnowledge};
	isAfterKnowledgeCheckPopUp = true;
	$.ajax({
		url     : context_path + aoJsonUrl + "updateIsKnowledgeQuestion",
		type    : "POST",
		dataType: "json",
		data    : JSON.stringify(data),
		contentType: "application/json;charset=utf-8",
		success : function(data) {
			//Successful call
			if(isKnowledge){
				shouldShowRegulationPopup("ao_is_knowledge_confirmed", function(){});
			} else {
				showAORegulationStatusPopup('no-thanks');
			}
		},
		error   : function( xhr, err ) {
			//Failed call
			if(isKnowledge){
				shouldShowRegulationPopup("ao_is_knowledge_confirmed", function(){});
			} else {
				showAORegulationStatusPopup('no-thanks');
			}
		}
	});
}
function unblockUserForThreshold() {
	var data = {userId: userInfo.id};
	$.ajax({
		url     : context_path + aoJsonUrl + "unblockUserForThreshold",
		type    : "POST",
		dataType: "json",
		data    : JSON.stringify(data),
		contentType: "application/json;charset=utf-8",
		success : function(data) {
			//Successful call
			closeAORegulationStatusPopup();
		},
		error   : function( xhr, err ) {
			//Failed call
			closeAORegulationStatusPopup();
		}
	});
}

function showCapitalQuestionnairePopup(){
	var capitalQuestionnairePopupLocation = context_path + "/jsp/single_pages/capital_questionnaire_pop.jsf";
	var iframeEl = g('popUpCapitalQuestionnaireContent');
	iframeEl.onload = function(){
		var iframeDoc = iframeEl.contentDocument ? iframeEl.contentDocument : (iframeEl.contentWindow ? iframeEl.contentWindow.document : iframeEl.document);
		iframeEl.style.height = $(iframeDoc.getElementById('popup_capital_questionnaire_main_holder')).outerHeight() + 20 + "px";
		iframeEl.style.width = $(iframeDoc.getElementById('popup_capital_questionnaire_main_holder')).outerWidth() + "px";
	}
	iframeEl.src = capitalQuestionnairePopupLocation;
	showBinaryOptionsKnowledgeOverlay();
	g('popUpCapitalQuestionnaire').style.display = "block";
}

function showCapitalQuestionnaireWeightPopup(code, msg){
	showBinaryOptionsKnowledgeOverlay();
	$(".popUpCapitalQuestionnaireWeightCodeBlock").css("display", "none");
	if(code == 11 || code == 12){
		g('popUpCapitalQuestionnaireWeightCodeBlock_'+code+'_text2').innerHTML = msg;
	}
	if(g('popUpCapitalQuestionnaireWeightCodeBlock_'+code)){
		g('popUpCapitalQuestionnaireWeightCodeBlock_'+code).style.display = "block";
	}
	g('popUpCapitalQuestionnaireWeight').style.display = "block";
}

function showBinaryOptionsKnowledgePopup(){
	showBinaryOptionsKnowledgeOverlay();
	g('popUpBinaryOptionsKnowledge').style.display = "block";
}

function showBinaryOptionsKnowledgeOverlay(){
	if(!g('popUpBinaryOptionsKnowledgeOverlay')){
		var topOffset = $(".header_menu_h").offset().top + $(".header_menu_h").outerHeight();
		var bottomOffset = $(".footer").outerHeight();
		var overlay = document.createElement("div");
		overlay.id = "popUpBinaryOptionsKnowledgeOverlay";
		overlay.style.position = "absolute";
		overlay.style.top = topOffset + "px";
		overlay.style.zIndex = "4";
		overlay.style.left = "0px";
		overlay.style.width = "100%";
		overlay.style.height = document.body.offsetHeight - topOffset - bottomOffset + "px";
		overlay.style.backgroundColor = "rgba(0,0,0,0.8)";
		document.body.appendChild(overlay);
	}else{
		g('popUpBinaryOptionsKnowledgeOverlay').style.display = "block";
	}
}

function removeBinaryOptionsKnowledgeOverlay(){
	if(g('popUpBinaryOptionsKnowledgeOverlay').parentNode){
		g('popUpBinaryOptionsKnowledgeOverlay').parentNode.removeChild(g('popUpBinaryOptionsKnowledgeOverlay'));
	}else if(g('popUpBinaryOptionsKnowledgeOverlay')){
		g('popUpBinaryOptionsKnowledgeOverlay').style.display = "none";
	}
}

function binaryOptionsKnowledgeConfirm(isConfirmed){
	g('popUpBinaryOptionsKnowledge').style.display = "none";
	if(isConfirmed){
		$.ajax({
			url     : context_path + "/jsp/system/ajax.jsf",
			type    : "POST",
			dataType: "text",
			data    : "binaryOptionsKnowledgeConfirm=1&confirmed=1",
			success : function( data ) {
				//Successful call
				window.location = binaryOptionsKnowledgeAfterYesLink;
			},
			error   : function( xhr, err ) {
				//Failed call
			}
		});
		removeBinaryOptionsKnowledgeOverlay();
	}else{
		$.ajax({
			url     : context_path + "/jsp/system/ajax.jsf",
			type    : "POST",
			dataType: "text",
			data    : "binaryOptionsKnowledgeConfirm=1&confirmed=0",
			success : function( data ) {
				//Successful call
			},
			error   : function( xhr, err ) {
				//Failed call
			}
		});
		g('popUpBinaryOptionsKnowledgeAfterNo').style.display = "block";
	}
}

function binaryOptionsKnowledgeAfterNoApproval(){
	removeBinaryOptionsKnowledgeOverlay();
	g('popUpBinaryOptionsKnowledgeAfterNo').style.display = "none";
	window.location = binaryOptionsKnowledgeAfterNoLink;
	return false;
}


function capitalQuestionnaireWeightApproval(){
	removeBinaryOptionsKnowledgeOverlay();
	g('popUpCapitalQuestionnaireWeight').style.display = "none";
	window.location = capitalQuestionnaireWeightLink;
	return false;
}

function capitalQuestionnaireWeightSendEmail(){
	$.ajax({
		url     : context_path + "/jsp/system/ajax.jsf",
		type    : "POST",
		dataType: "text",
		data    : "unsuspendETReg=1",
		success : function( data ) {
			//Successful call
			capitalQuestionnaireWeightApproval();
		},
		error   : function( xhr, err ) {
			//Failed call
			capitalQuestionnaireWeightApproval();
		}
	});
}



///// AO Regulation questionnaire /////
$(window).on("hashchange", function(event){
	if(window.location.hash == "#showSingleQuestionnaire"){
		showSingleQuestionnairePopup();
		window.location.hash = "";
	}
});
function showSingleQuestionnairePopup(){
	// var singleQuestionnairePopupLocation = context_path + "/jsp/single_pages/single_questionnaire_pop.jsf";
	function checkIt() {
		if (!isUndefined(angular.element('#bootstrapId').scope())) {
			angular.element('#bootstrapId').scope().setQuestionnairePopupType(true);
			showAORegulationOverlay();
		} else {
			setTimeout(function(){checkIt()}, 500);
		}
	}
	checkIt();
	// var iframeEl = g('popUpSingleQuestionnaireContent');
	// iframeEl.onload = function(){
		// var iframeDoc = iframeEl.contentDocument ? iframeEl.contentDocument : (iframeEl.contentWindow ? iframeEl.contentWindow.document : iframeEl.document);
		// iframeEl.style.height = $(iframeDoc.getElementById('popup_single_questionnaire_main_holder')).outerHeight() + 70 + "px";
		// iframeEl.style.width = $(iframeDoc.getElementById('popup_single_questionnaire_main_holder')).outerWidth() + "px";
	// }
	// iframeEl.src = singleQuestionnairePopupLocation;
}

function closeSingleQuestionnairePopup(){
	top.gd_closePopUp('popUpSingleQuestionnaire');
	top.removeAORegulationOverlay();
}

function showAORegulationStatusPopup(code, msg, callFrom){
	showAORegulationOverlay();
	$(".popUpAORegulationStatusCodeBlock").css("display", "none");
	$('#popUpAORegulationStatus').removeClass('aLittleBiggerPopUp bubbles-popUp-content low-balance not-login aLittleSmallerPopup');
	$('#popUpAORegulationStatusClose').show();
	var type = "";
	if(code == 0){
		type = "passed";
	}else if(code == "suspended-cnmv"){
		type = "suspended-cnmv";
	}else if(code == 313){
		type = "suspended-cnmv-docs";
	}else if(code == 304 || code == 305){
		type = "restricted";
		$('#popUpAORegulationStatus').addClass('aLittleBiggerPopUp');
//	}else if(code == 305){
//		type = "blocked";
	}else if(code == 'no-thanks'){
		type = "restricted_no_thanks";
		$('#popUpAORegulationStatus').addClass('aLittleSmallerPopup');
		$('#popUpAORegulationStatusClose').hide();
	}else if(code == 307){
		type = "blocked_pep";
	}else if(code == 308){
		type = "restricted-loss";
		$('#popUpAORegulationStatus').addClass('aLittleBiggerPopUp');
	} else if(code == 309) {
		$('#bonus_balance_only_error').html(msg);
		type = "309";
	} else if(code == 419) {
		type = "not-login";
		if (!isUndefined(callFrom) && callFrom == 'bubbles') {
			$('#popUpAORegulationStatus').addClass('bubbles-popUp-content not-login');
		}
	} else if(code == 209) {
		type = "low-balance";
		if (!isUndefined(callFrom) && callFrom == 'bubbles') {
			$('#popUpAORegulationStatus').addClass('bubbles-popUp-content low-balance');
		}
	} else if (code == 314 || code == 315) {
		type = code;
	} else {
		type = "unknown";
	}
	if(g('popUpAORegulationStatusCodeBlock_'+type)){
		g('popUpAORegulationStatusCodeBlock_'+type).style.display = "block";
	}
	g('popUpAORegulationStatus').style.display = "block";
}

function closeAORegulationStatusPopup(){
	top.gd_closePopUp('popUpAORegulationStatus');
	top.removeAORegulationOverlay();
}

function showDocumentsPopup(code){
	var type = "";
	if(code == 306){
		type = "restricted";
	}else if(code == 302){
		type = "blocked";
	}
	function checkIt() {
		if (!isUndefined(angular.element('#bootstrapId').scope())) {
			angular.element('#bootstrapId').scope().setDocumentsPopupType(type);
			showAORegulationOverlay();
			g('popUpDocuments').style.display = "block";
		} else {
			setTimeout(function(){checkIt()}, 500);
		}
	}
	checkIt();
}

function closeDocumentsPopup(){
	top.gd_closePopUp('popUpDocuments');
	top.removeAORegulationOverlay();
}

function showAORegulationOverlay(){
	if(!g('popUpAORegulationOverlay')){
		var topOffset = 0;
		var bottomOffset = 0;
		var overlay = document.createElement("div");
		overlay.id = "popUpAORegulationOverlay";
		overlay.style.position = "absolute";
		overlay.style.top = topOffset + "px";
		overlay.style.zIndex = "14";
		overlay.style.left = "0px";
		overlay.style.width = "100%";
		overlay.style.height = document.body.offsetHeight - topOffset - bottomOffset + "px";
		overlay.style.backgroundColor = "rgba(0,0,0,0.6)";
		document.body.appendChild(overlay);
	}else{
		g('popUpAORegulationOverlay').style.display = "block";
	}
}

function removeAORegulationOverlay(){
	if(g('popUpAORegulationOverlay').parentNode){
		g('popUpAORegulationOverlay').parentNode.removeChild(g('popUpAORegulationOverlay'));
	}else if(g('popUpAORegulationOverlay')){
		g('popUpAORegulationOverlay').style.display = "none";
	}
}
var transactionTypeId = 0;
function shouldShowWithdrawResetBonusPopup(callback, transactionTypeId){
	var UserMethodRequest = {
		utcOffset: new Date().getTimezoneOffset(), 
		writerId: 1, 
		locale: '',
		encrypt: false, 
		isLogin: true, 
		afterDeposit: false,
		transactionTypeId: transactionTypeId,
		ccId: $('#withdrawForm\\:ccNum').val()
	}
	
	if ($('#withdrawForm\\:ccNum').val() == '') {//skip call if there is no cc selected
		callback();
		return;
	}
	$(document).ready(function(){
		$.ajax({
			url     : context_path + aoJsonUrl + "getWithdrawBonusRegulationState",
			type    : "POST",
			dataType: "json",
			data    : JSON.stringify(UserMethodRequest),
			contentType: "application/json;charset=utf-8",
			success : function(data) {
				//Successful call
				openWithdrawResetBonusPopup(data, callback);
			},
			error   : function( xhr, err ) {
				//Failed call
			}
		});
	});
}
function openWithdrawResetBonusPopup(data, callback) {
	if (data.state == 'NO_OPEN_BONUSES') {
		callback();
	} else if (data.state == 'REGULATED_OPEN_BONUSES') {
		$('#popUpWithdrawResetBonusYes').on('click', function(){
			callback();
			return false;
		});
		showAORegulationOverlay();
		$('#popUpWithdrawResetBonus').css('display', 'block');		
	} else  if (data.state == 'NOT_REGULATED_OPEN_BONUSES') {
		showAORegulationOverlay();
		$('#popUpWithdrawResetBonusError').css('display', 'block');	
	}
}
function closeWithdrawResetBonusPopup(id){
	removeAORegulationOverlay();
	$('#' + id).css('display', 'none');
}
function insertWithdrawCancel(el){
	var InsertWithdrawCancelMethodRequest = {
		utcOffset: new Date().getTimezoneOffset(), 
		writerId: 1, 
		locale: '',
		encrypt: false, 
		isLogin: true, 
		afterDeposit: false,
		transactionTypeId: transactionTypeId,
		ccId: $('#withdrawForm\\:ccNum').val()
	}
	
	$(document).ready(function(){
		$.ajax({
			url     : context_path + aoJsonUrl + "insertWithdrawCancel",
			type    : "POST",
			dataType: "json",
			data    : JSON.stringify(InsertWithdrawCancelMethodRequest),
			contentType: "application/json;charset=utf-8",
			success : function(data) {
				//Successful call
				window.location = el.href;
			},
			error   : function( xhr, err ) {
				//Failed call
			}
		});
	});
}
//used in flash 0-100
function get_isETRegulation(){
	return isETRegulation;
}
//check where ever something is undefined or null
function isUndefined(el) {
	return (typeof el == 'undefined' || el == null);
}

function Slider(params){
	//Add a custom easing function
	$.extend(
		jQuery.easing,
		{
			sliderEasingFunction: function( p ) {
				function easeIn(p){
					return Math.pow( p, 3 );
				}
				return p < 0.5 ?
					easeIn( p * 2 ) / 2 :
					1 - easeIn( p * -2 + 2 ) / 2;
			}
		}
	);
	
	//Set the variables
	var self = this;
	this.parentEl = params.parentEl;
	this.slidesClass = params.slidesClass;
	this.duration = typeof params.duration != "undefined" ? (parseInt(params.duration) > 0 ? parseInt(params.duration) : 1500) : 1500;
	this.timeout = typeof params.timeout != "undefined" ? (parseInt(params.timeout) > 0 ? parseInt(params.timeout) : 8000) : 8000;
	this.direction = typeof params.direction != "undefined" ? (params.direction == "rtl" ? "rtl" : "ltr") : "rtl";
	this.autostart = typeof params.autostart != "undefined" ? (params.autostart != true ? false : true) : true;
	this.startNow = typeof params.startNow != "undefined" ? (params.startNow != true ? false : true) : true;
	
	this.currentSlide = 0;
	
	this.slides = $("#"+this.parentEl).find("."+this.slidesClass);
	this.offset = $("#"+this.parentEl).outerWidth();
	
	this.initializeSlides = function(startSlide){
		for(var i = 0; i < self.slides.length; i++){
			var offset = 0;
			if((i - startSlide) > 0){
				offset = (i - startSlide)*self.offset;
			}else if((i - startSlide) < 0){
				offset = (i + self.slides.length - startSlide)*self.offset;
			}
			$(self.slides[i]).css("left", offset + "px");
			$(self.slides[i]).css("display", "block");
		}
	}
	
	this.setSlide = function(toSlide){
		if((toSlide >= 0) && (toSlide < self.slides.length)){
			for(var i = 0; i < self.slides.length; i++){
				$(self.slides[i]).stop();
			}
			this.initializeSlides(toSlide);
		}
	}
	
	this.slide = function(){
		for(var i = 0; i < self.slides.length; i++){
			(function(i){
				$(self.slides[i]).animate({
					left: "-=" + self.offset
				}, {
					duration: self.duration, 
					easing: "sliderEasingFunction", 
					complete: function(){
						if(parseInt($(self.slides[i]).css("left")) < 0){
							$(self.slides[i]).css("left", ((self.slides.length - 1)*self.offset) + "px");
						}
						if(parseInt($(self.slides[i]).css("left")) == 0){
							self.currentSlide = i;
						}
					}
				});
			})(i);
		}
	}
	
	this.start = function(){
		if(self.startNow){
			self.slide();
		}
		self.interval = setInterval(function(){
			self.slide();
		}, self.timeout);
	}
	
	this.stop = function(){
		clearInterval(self.interval);
	}
	
	//Prepare the slider
	this.initializeSlides(0);
	if(this.autostart){
		this.start();
	}
}
function Tournaments(){
	this.firstTournamentsLoad = true;
	
	this.loadTournaments = function(){
		var params = "listTournaments=1";
		$.ajax({
			type: "POST",
			url: "ajax.jsf",
			data: params,
			success:  function(result) {
				firstTournamentsLoad = false;
				generateTournaments($.parseJSON(result));
			}
		});
	}

	function generateTournaments(tournaments){
		$("#tournaments_holder").css("height", $("#tournaments_holder").outerHeight());
		$("#tournaments_holder").html("");
		for(var i = 0; i < tournaments.length; i++){
			if(i > 0){
				var divider = document.createElement("div");
				divider.className = "tournaments_divider";
				document.getElementById("tournaments_holder").appendChild(divider);
			}
			var t = tournaments[i];
			var holder = generateTournament(t);
			document.getElementById("tournaments_holder").appendChild(holder);
		}
		$("#tournaments_holder").css("height", "");
	}

	function generateTournament(t){
		var holder = document.createElement("div");
		holder.id = "tournament_" + t.id;
		holder.className = "tournament_holder";
		
		var rightSide = generateRightSide(t);
		holder.appendChild(rightSide);
		
		var mainSideHolder = generateMainSide(t);
		holder.appendChild(mainSideHolder);
		
		setTimeout(function(){
			if($(rightSide).outerHeight() < $(mainSideHolder).outerHeight()){
				$(rightSide).css("height", $(mainSideHolder).outerHeight());
			}else{
				$(mainSideHolder).css("height", $(rightSide).outerHeight());
			}
		}, 0);
		
		return holder;
	}

	function generateRightSide(t){
		var rightSide = document.createElement("div");
		rightSide.className = "tournaments_right_side";
		
		var tName = document.createElement("div");
		tName.className = "tournaments_tournament_name";
		tName.innerHTML = t.name;
		rightSide.appendChild(tName);
		
		for(var i = 0; i < t.tournamentSkinsLangList.length; i++){
			if(t.tournamentSkinsLangList[i].tabLabel.toLowerCase() == userLanguage){
				var tDescription = document.createElement("div");
				tDescription.className = "tournaments_tournament_description";
				tDescription.innerHTML = t.tournamentSkinsLangList[i].prizeText;
				rightSide.appendChild(tDescription);
				
				var divider = document.createElement("div");
				divider.className = "tournaments_right_side_divider";
				rightSide.appendChild(divider);
				
				if((typeof t.tournamentSkinsLangList[i].prizeImage != "undefined") && (t.tournamentSkinsLangList[i].prizeImage != "")){
					var img = document.createElement("img");
					img.className = "tournaments_right_side_img";
					img.src = t.tournamentSkinsLangList[i].prizeImage;
					rightSide.appendChild(img);
				}		
			}
		}
		
		return rightSide;
	}
	
	function generateMainSide(t){
		var mainSideHolder = document.createElement("div");
		mainSideHolder.className = "tournaments_main_holder";
		
		var mainSide = document.createElement("div");
		mainSide.className = "tournaments_main";
		
		var mainSideTable = document.createElement("div");
		mainSideTable.className = "tournaments_main_table";
		
		var header = generateHeader(t);
		mainSideTable.appendChild(header);
		
		var users = generateUsers(t, mainSideTable);
		
		mainSide.appendChild(mainSideTable);
		
		mainSideHolder.appendChild(mainSide);
		
		return mainSideHolder;
	}
	
	function generateHeader(t){
		var header = document.createElement("div");
		header.className = "tournaments_header";
		
		var rankingDiv = document.createElement("div");
		rankingDiv.className = "tournaments_header_ranking";
		rankingDiv.innerHTML = tournaments_txt_ranking;
		header.appendChild(rankingDiv);
		
		var nameDiv = document.createElement("div");
		nameDiv.className = "tournaments_header_name";
		nameDiv.innerHTML = tournaments_txt_name;
		header.appendChild(nameDiv);
		
		var countryDiv = document.createElement("div");
		countryDiv.className = "tournaments_header_country";
		countryDiv.innerHTML = tournaments_txt_country;
		header.appendChild(countryDiv);
		
		var scoreDiv = document.createElement("div");
		scoreDiv.className = "tournaments_header_score";
		scoreDiv.innerHTML = tournaments_txt_total;
		header.appendChild(scoreDiv);
		
		return header;
	}
	
	function generateUsers(t, users){
		for(var i = 0; i < t.Users.length; i++){
			if(i >= t.numberOfUsersToShow.usersToShow){
				break;
			}
			
			var userRow = generateUserRow(t, i);
			users.appendChild(userRow);
		}
		
		if(t.Users.length > t.numberOfUsersToShow.usersToShow){
			for(var i = t.numberOfUsersToShow.usersToShow; i < t.Users.length; i++){
				if(userLogin && (userInfo.id > 0) && (userInfo.id == t.Users[i].userId)){
					var divider = document.createElement("div");
					divider.className = "tournaments_user_current_user_divider";
					users.appendChild(divider);
					
					var userRow = generateUserRow(t, i);
					users.appendChild(userRow);
					
					break;
				}
			}
		}
		
		return users;
	}
	
	function generateUserRow(t, i){
		var userRow = document.createElement("div");
		userRow.className = "tournaments_user_row";
		if(userLogin && (userInfo.id > 0) && (userInfo.id == t.Users[i].userId)){
			userRow.className += " tournaments_user_row_current_user";
		}
		
		var rankHolder = document.createElement("div");
		rankHolder.className = "tournaments_user_rank_holder";
		var rankDiv = document.createElement("div");
		rankDiv.className = "tournaments_user_rank tournaments_user_rank_" + (i + 1);
		rankDiv.innerHTML = ((i > 2) ? "#" : "") + t.Users[i].rank;
		rankHolder.appendChild(rankDiv);
		userRow.appendChild(rankHolder);
		
		var nameHolder = document.createElement("div");
		nameHolder.className = "tournaments_user_name_holder";
		var nameDiv = document.createElement("div");
		nameDiv.className = "tournaments_user_name";
		nameDiv.innerHTML = t.Users[i].firstName + " " + t.Users[i].lastName;
		nameDiv.title = t.Users[i].firstName + " " + t.Users[i].lastName;
		nameHolder.appendChild(nameDiv);
		userRow.appendChild(nameHolder);
		
		var countryHolder = document.createElement("div");
		countryHolder.className = "tournaments_user_country_holder";
		var countryImg = document.createElement("img");
		countryImg.className = "tournaments_user_country";
		countryImg.src = image_context_path_clean + "/flags/" + t.Users[i].countryId + ".png";
		countryHolder.appendChild(countryImg);
		userRow.appendChild(countryHolder);
		
		var scoreHolder = document.createElement("div");
		scoreHolder.className = "tournaments_user_score_holder";
		var scoreDiv = document.createElement("div");
		scoreDiv.className = "tournaments_user_score";
		scoreDiv.innerHTML = t.Users[i].score;
		scoreHolder.appendChild(scoreDiv);
		userRow.appendChild(scoreHolder);
		
		return userRow;
	}
	
	function trimText(el, holder){
		$(el).each(function(){
			var stopTrimming = false;
			var appendSuffix = false;
			var i = 0;
			while(!stopTrimming){
				if(i > 500){break;}
				if($(this).width() <= $(holder).width()){
					stopTrimming = true;
				}else{
					$(this).html($(this).html().slice(0,-1));
					appendSuffix = true;
				}
				i++;
			}
			if(appendSuffix){
				$(this).html($(this).html().slice(0,-3));
				$(this).html($(this).html() + "...");
			}
		});
	}

}
function headerMenu() {
	$('#headerMenu').find('li')
		.mouseenter(function() {
			stopMenuAnimations();
			$(this).find('ul').slideDown(200);
		})
		.mouseleave(function() {
			$(this).find('ul').fadeOut(200);
		});
	function stopMenuAnimations(){
		$('#headerMenu li').each(function(index, el){
			$(el).find('ul').stop(false, true);
		});
	}
}

var balancePopupId = 'balancePopup';
$(document).ready(function(){
	$('#'+balancePopupId).mouseenter(function(event){
		$(this).attr('data-hovering', '1');
	});
});
function openCacheBonusPopup(params) {
	if (!isUndefined(params.iframe)) {
		params.offsetEl = $('#' + params.iframe);
	}
	params.popUpId = balancePopupId;
	
	var e = params.e;
	var invId = 0;
	if (!isUndefined(e)) {
		invId = $(e.target).data('invid');
		if (isUndefined(invId)) {
			invId = 0;
		}
	}
	
	var DepositBonusBalanceMethodRequest = {
		userId: userInfo.id,
		investmentId: invId,
		balanceCallType: params.balanceCallType
	}
	
	$.ajax({
		url     : context_path + aoJsonUrl + "getUserDepositBonusBalance",
		type    : "POST",
		dataType: "json",
		data    : JSON.stringify(DepositBonusBalanceMethodRequest),
		contentType: "application/json;charset=utf-8",
		success : function(data) {
			//Successful call
			openCacheBonusPopupSuccess(params, data);
		},
		error   : function( xhr, err ) {
			//Failed call
		}
	});
}
function openCacheBonusPopupSuccess(params, data) {
	if (data.errorCode == 0) {
		$('#depositCashBalance').html(formatAmount(data.depositBonusBalanceBase.depositCashBalance, 0, true));
		$('#bonusBalance').html(formatAmount(data.depositBonusBalanceBase.bonusBalance, 0, true));
		$('#bonusProfit').html(formatAmount(data.depositBonusBalanceBase.bonusWinnings, 0, true));
		var total = formatAmount(data.depositBonusBalanceBase.totalBalance, 0, true);
		$('#totalBalance').html(total);
		var $totalTxtHolder = $('#totalTxtHolder');
		$totalTxtHolder.removeClass('total amount return');
		
		if (params.balanceCallType == balanceCallTypes.total) {
			$('#bonusProfit').parent().css('display', '');
			$('.balancePopUp_info').css('display', '');
			$totalTxtHolder.addClass('total');
			if (data.depositBonusBalanceBase.show) {
				params.offsetEl.contents().find('#loginStrip_balance').html(total);
				if (isUndefined(params.position) || params.position) {
					$('#' + params.popUpId).removeClass('small_popUp');
					positionToolTip(params);
				}
				params.offsetEl.contents().find('#balanceIcon').show();
			} else {
				params.offsetEl.contents().find('#balanceIcon').hide();
			}
		} else {
			$('#bonusProfit').parent().css('display', 'none');
			$('.balancePopUp_info').css('display', 'none');
			if (params.balanceCallType == balanceCallTypes.open_amount || params.balanceCallType == balanceCallTypes.settled_amount) {
				$totalTxtHolder.addClass('amount');
			} else {
				$totalTxtHolder.addClass('return');
			}
			if (data.depositBonusBalanceBase.show) {
				if (isUndefined(params.position) || params.position) {
					$('#' + params.popUpId).addClass('small_popUp');
					positionToolTip(params);
				}
			} else {
				params.offsetEl.contents().find('.icon_set_1_info').hide();
			}
		}
	}
}
function positionToolTip(params) {
	var hardOffset = 5;
	var e = params.e;
	var $popup = $('#' + params.popUpId);
	
	$(e.target).mouseleave(function() {
		setTimeout(function(){
			if($('#'+balancePopupId).attr('data-hovering') != 1){
				$popup.fadeOut(200);
			}else{
				$('#'+balancePopupId).mouseleave(function(event){
					$(this).attr('data-hovering', '0');
					$popup.fadeOut(200);
				});
			}
		}, 50);
	});
	
	var offset = {
		l: 0,
		t: 0
	};
	
	var $popupArrow = $popup.find('._arrow');
	
	var $screen = {
		w: $(window).width(),//width
		h: $(window).height(),//height
		t: $(document).scrollTop(),//scroll top
		l: $(document).scrollLeft()//scroll left
	}
	var $popUpSize = {
		w: $popup.width(),
		h: $popup.height()
	}
	
	if (!isUndefined(params.offsetEl)) {
		offset = {
			l: params.offsetEl.offset().left,
			t: params.offsetEl.offset().top
		}
	}
	
	var left = offset.l + $(e.target).offset().left + (e.target.offsetWidth / 2) - ($popUpSize.w / 2);
	var top = offset.t + $(e.target).offset().top + e.target.offsetHeight;
	
	var arrowOffeset = {
		left: 0,
		right: 0
	};
	
	if ($screen.w < $popUpSize.w || left < 0) {
		left = hardOffset;
		arrowOffeset.left = left + $(e.target).offset().left + (e.target.offsetWidth / 2) - ($popupArrow.width() / 2) - (hardOffset * 2);
		if (arrowOffeset.left < hardOffset) {
			arrowOffeset.left = hardOffset;
		} else {
			arrowOffeset.right = 'auto';
		}
	} else {
		if ((left + $popUpSize.w + hardOffset) > ($screen.w - $screen.l)) {
			left = $screen.w - $popUpSize.w - hardOffset;
			arrowOffeset.left = $(e.target).offset().left - left + (e.target.offsetWidth / 2) - ($popupArrow.width() / 2);
			if ((arrowOffeset.left + $popupArrow.width() + hardOffset) > $popup.width()) {
				arrowOffeset.left = 'auto';
				arrowOffeset.right = hardOffset;
			} else{
				arrowOffeset.right = 'auto';
			}
		}
	}
	
	$popupArrow.css('left', arrowOffeset.left).css('right', arrowOffeset.right);
	
	$popupArrow.removeClass('bottom top');
	if ($screen.h < $popUpSize.h || top < 0) {
		top = 0;
	} else {
		if ((top + $popUpSize.h + hardOffset) > ($screen.t + $screen.h)) {
			top -= ($popUpSize.h + e.target.offsetHeight + ($popupArrow.height() / 2));
			$popupArrow.addClass('bottom');
		} else {
			top += (Math.sqrt(2*$popupArrow.height()*$popupArrow.height()) / 2);
			$popupArrow.addClass('top');
		}
	}
	
	$popup.css('left', left).css('top', top);	
	$popup.fadeIn(200);
}


function revert_escape(el, elTo){
	if(typeof elTo == "undefined"){
		elTo = el;
	}
	$('#'+elTo).html($('#'+el).text().replace('&lt;', '<').replace('&gt;', '>'));
}

function unescapeAmp(str){
	return str.replace(/&amp;/g, "&");
}
function unescapeStuff(str) {
	return str.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">");
}
function inIframe(){
	try{
		return window.self !== window.top;
	}catch(e){
		return true;
	}
}

function showChangePassOverlay(){
	if(!g('popUpChangePassOverlay')){
		var topOffset = 0;
		var bottomOffset = 0;
		var overlay = document.createElement("div");
		overlay.id = "popUpChangePassOverlay";
		overlay.style.position = "absolute";
		overlay.style.top = topOffset + "px";
		overlay.style.zIndex = "100";
		overlay.style.left = "0px";
		overlay.style.width = "100%";
		overlay.style.height = document.body.offsetHeight - topOffset - bottomOffset + "px";
		overlay.style.backgroundColor = "rgba(0,0,0,0.6)";
		document.body.appendChild(overlay);
	}else{
		g('popUpChangePassOverlay').style.display = "block";
	}
}
function removeChangePassOverlay(){
	if(g('popUpChangePassOverlay').parentNode){
		g('popUpChangePassOverlay').parentNode.removeChild(g('popUpChangePassOverlay'));
	}else if(g('popUpChangePassOverlay')){
		g('popUpChangePassOverlay').style.display = "none";
	}
}
if(inIframe()){
	if(!userInfo.isNeedChangePassword && top.userInfo.isNeedChangePassword){
		top.gd_closePopUp('popUpChangePass');
		top.removeChangePassOverlay();
		window.top.location.reload();
	}
}

var setDirtyState = false;
function setDirty() {
	setDirtyState = true;
}
function updateDefaultDepositAmount(el,id) {
	if (!setDirtyState) {
		g(id).value = predefinedDepositAmountJsonMap[el.value];
	}
}

window.addEventListener('message',function(event) {
	var bubblesServer = (isLive) ? 'https://web.visualtrade.com' : 'https://ao.visualtrade.com'; 
	if(event.origin !== bubblesServer) return;
	if (event.data.updateBalance) {
		refreshHeader();
	}
	if (!isUndefined(event.data.respData) && event.data.respData.errorCode != 0) {
		var data = {
			CODE_ID: event.data.respData.errorCode,
			ERROR_MSG: event.data.respData.errorMessages,
			IS_PEP: (event.data.respData.errorCode == 307) ? 'true' : 'false',
			IS_TRESHOLD: (event.data.respData.errorCode == 308) ? 'true' : 'false',
			TRESHOLD_ERROR_MSG: (isUndefined(event.data.respData.errorMessages) ? '' : formatAmount(event.data.respData.errorMessages[0].message,2,true))
		};
		parseRegulationResponse(data, 'bubbles', function() {});
	}
},false)


function checDates(fromId, toId) {
	var from = new Date(fixDate(g(fromId).value));
	var to = new Date(fixDate(g(toId).value));
	var fromMax = new Date(to).setDate(new Date(to).getDate() - 32);
	if (from.getTime() > to.getTime()) {
		g('error_holder').style.display = 'block';
	} else if (from.getTime() <= fromMax) {
		g('error_holder').style.display = 'block';
	} else {
		return myfaces.oam.submitForm('investmentsContainer','investmentsContainer:hiddenbtn');
	}
}
function fixDate(date) {
	var dateSplit = date.split('.');
	var year = (dateSplit[2] > 70 && dateSplit[2] <= 99) ? Number('19' + dateSplit[2]) : Number('20' + dateSplit[2]);
	return year + '/' + dateSplit[1] + '/' + dateSplit[0];
}
function endsWith(str, suffix){
	var url = str.split('#');
	return url[0].substr(-suffix.length) === suffix;
}

function optimoveTradePageName(params){
	var tradePageName = "";
	
	if(endsWith(window.location.href, homePageN)){
		if(params.box.boxType == 2){
			tradePageName = "Home - Profit line";
		}else{
			tradePageName = "Home";
		}
	}else if(endsWith(window.location.href, tradePageN)){
		if(params.box.boxType == 2){
			tradePageName = "Binary options - Profit line";
		}else{
			tradePageName = "Binary options";
		}
	}else if(endsWith(window.location.href, optionPlusPageN)){
		tradePageName = "Option plus";
	}else if(endsWith(window.location.href, binary0100PageN)){
		tradePageName = "Binary 0-100";
	}else if(endsWith(window.location.href, oneTouchPageN)){
		tradePageName = "One touch";
	}else if(window.location.href.indexOf("/assets/") > 0){ //Asset single page
		tradePageName = "Assets";
	}else if(params.from == "longterm"){ //Long term page
		tradePageName = "Longterm";
	}else if(endsWith(window.location.href, investmentsPageN)){
		if(params.box.boxType == 2){
			tradePageName = "Investments - Profit line";
		}else{
			tradePageName = "Investments";
		}
	}else if(endsWith(window.location.href, livePageN)){
		if(params.box.boxType == 2){
			tradePageName = "Live - Profit line";
		}else{
			tradePageName = "Live";
		}
	}else if(params.box && params.box.boxType == 2){
		tradePageName = "Profit line";
	}else{
		
	}
	
	return tradePageName;
}

function optimoveReportGeneralEvent(eid, optimoveParams){
	if((skinId != 1) && userLogin){
		if(typeof optimoveParams == "undefined"){
			optimoveParams = {};
		}
		/*//Temporary fix - at the moment Optimove only supports one parameter and it must be named "x"... very mature
		optimoveParams.platform = "AO";
		if(skinId == 1){
			optimoveParams.platform = "ET";
		}
		*/
		optimoveParams.x = "AO";
		if(skinId == 1){
			optimoveParams.x = "ET";
		}
		if(optimoveActive){
			OptiRealApi.reportEvent( eid, optimoveParams, userInfo.id + "_U", optimoveTid);
		}
	}
}

function optimoveReportTradeEvent(params){
	if((skinId != 1) && userLogin){
		var pageName = optimoveTradePageName(params);
		var assetName = "";
		if(params.from == "one_touch"){
			assetName = marketsName[params.marketId];
		}else if(params.from == "longterm"){
			assetName = marketsName[params.marketId];
		}else if(params.from == "0-100"){
			assetName = marketsName[params.marketId];
		}else{
			assetName = marketsName[params.box.marketId];
		}
		/*//Temporary fix - at the moment Optimove only supports one parameter and it must be named "x"... very mature
		var optimoveParams = {pageName: pageName, assetName: assetName};
		optimoveParams.platform = "AO";
		if(skinId == 1){
			optimoveParams.platform = "ET";
		}
		*/
		var optimoveParams = {x: assetName};
		if(optimoveActive){
			OptiRealApi.reportEvent( optimoveEvents.trade, optimoveParams, userInfo.id + "_U", optimoveTid);
		}
	}
}

function showDefaultIco(el) {
	var groupId = el.getAttribute('data-groupId');
	var marketId = el.getAttribute('data-marketId');
	el.src = el.src.replace('asset' + marketId, 'gicn' + groupId); 
}

function openLoginLockedPopup(){
	if(g('popUpLoginAccountLocked')){
		g('popUpLoginAccountLocked').style.display = 'block';
		showGeneralOverlay();
	}
}

function closeLoginLockedPopup(){
	if(g('popUpLoginAccountLocked')){
		g('popUpLoginAccountLocked').style.display = 'none';
		removeGeneralOverlay();
	}
}

function showGeneralOverlay(zIndex){
	if(!g('popUpGeneralOverlay')){
		if(isUndefined(zIndex) || (parseInt(zIndex) == '')){
			zIndex = '11';
		}
		var topOffset = 0;
		var bottomOffset = 0;
		var overlay = document.createElement("div");
		overlay.id = "popUpGeneralOverlay";
		overlay.style.position = "absolute";
		overlay.style.top = topOffset + "px";
		overlay.style.zIndex = zIndex;
		overlay.style.left = "0px";
		overlay.style.width = "100%";
		overlay.style.height = document.body.offsetHeight - topOffset - bottomOffset + "px";
		overlay.style.backgroundColor = "rgba(0,0,0,0.6)";
		document.body.appendChild(overlay);
	}else{
		g('popUpGeneralOverlay').style.display = "block";
	}
}
function removeGeneralOverlay(){
	if(g('popUpGeneralOverlay')){
		if(g('popUpGeneralOverlay').parentNode){
			g('popUpGeneralOverlay').parentNode.removeChild(g('popUpGeneralOverlay'));
		}else{
			g('popUpGeneralOverlay').style.display = "none";
		}
	}
}

function openLowBalancePopup(){
	if(g('popUpLowBalance')){
		g('popUpLowBalance').style.display = 'block';
		showGeneralOverlay(90);
	}
}

function closeLowBalancePopup(){
	if(g('popUpLowBalance')){
		g('popUpLowBalance').style.display = 'none';
		removeGeneralOverlay();
	}
}

function resetErrorMsgs(data) {
	if (typeof data.stopClear == 'undefined' || !data.stopClear) {
		for (var i = 0; i < error_stack.length; i++) {
			var error_holder = g(error_stack[i]);
			if (error_holder != null) {
				g(error_stack[i]).className = g(error_stack[i]).className.replace(/ error/g, '');
			}
			var error_holder = g(error_stack[i] + '-error');
			if (error_holder != null) {
				g(error_stack[i] + '-error').remove();
			}
		}
		error_stack = [];
	}
}

var isWireForm = false;
function setWithdrawSurveyAnswers(updateAnswers){
	var formPrefix = 'withdrawForm';
	if(isWireForm){
		formPrefix = 'wireForm';
	}
	if(!updateAnswers){
		g(formPrefix + ':userAnswerId').value = '0';
		g(formPrefix + ':textAnswer').value = '';
		$('#wireForm\\:userAnswerId').trigger('input');
		$('#wireForm\\:textAnswer').trigger('input');
	}else{
		var numChecked = 0;;
		$('.withdraw_survey_popUp_options input').each(function(index, el){
			if(el.checked){
				numChecked++;
				g(formPrefix + ':userAnswerId').value = el.value;
				if(el.value == 405){
					var otherText = g('withdraw_survey_popUp_other').value;
					otherText = otherText.replace(/"/g, "'");
					g(formPrefix + ':textAnswer').value = otherText;
				}
			}
		});
		$('#wireForm\\:userAnswerId').trigger('input');
		$('#wireForm\\:textAnswer').trigger('input');
		if(numChecked == 0){
			return false;
		}
		return true;
	}
}

function showWithdrawSurveyError(){
	g('popUpWithdrawSurveyError').style.display = 'block';
}

function hideWithdrawSurveyError(){
	g('popUpWithdrawSurveyError').style.display = 'none';
}

function openWithdrawSurveyPopup(){
	if(g('withdrawForm:ccNum') && (g('withdrawForm:ccNum').value == '' || g('withdrawForm:amount').value == '')){
		closeWithdrawSurveyPopup(false);
	}else if(g('withdrawForm:ccNum')){
		if(g('popUpWithdrawSurvey')){
			g('popUpWithdrawSurvey').style.display = 'block';
			showGeneralOverlay(90);
		}
	}else{//Wire form
		if(g('popUpWithdrawSurvey')){
			g('popUpWithdrawSurvey').style.display = 'block';
			showGeneralOverlay(90);
		}
	}
}

function closeWithdrawSurveyPopup(setAnswers){
	if(g('popUpWithdrawSurvey')){
		hideWithdrawSurveyError();
		if(setAnswers){
			if(setWithdrawSurveyAnswers(true)){
				g('popUpWithdrawSurvey').style.display = 'none';
				removeGeneralOverlay();
				openWithdrawSurveyConfirmationPopup();
			}else{
				showWithdrawSurveyError();
			}
		}else{
			g('popUpWithdrawSurvey').style.display = 'none';
			removeGeneralOverlay();
			setWithdrawSurveyAnswers(false);
			withdrawContinue();
		}
	}
}

function openWithdrawSurveyConfirmationPopup(){
	if(g('popUpWithdrawSurveyConfirmation')){
		g('popUpWithdrawSurveyConfirmation').style.display = 'block';
		showGeneralOverlay(90);
	}
}

function closeWithdrawSurveyConfirmationPopup(){
	if(g('popUpWithdrawSurveyConfirmation')){
		g('popUpWithdrawSurveyConfirmation').style.display = 'none';
		removeGeneralOverlay();
	}
	withdrawContinue();
}

function withdrawContinue(){
	if (!isUndefined(window.GMwithdraw) && GMwithdraw) {
		angular.element('#GmWithdrawalController').scope().submitWithdrow();
	} else if(isWireForm){
		myfaces.oam.submitForm('wireForm','wireForm:button_wire');
	}else{
		myfaces.oam.submitForm('withdrawForm','withdrawForm:withdrawBtn');
	}
}

function getForgetPasswordContacts() {
	$.ajax({
		url     : context_path + aoJsonUrl + 'getForgetPasswordContacts',
		type    : "POST",
		dataType: "json",
		data    : JSON.stringify({"userName":$('#username').val()}),
		contentType: "application/json;charset=utf-8",
		success : function( data ) {
			//Successful call
			if (data.errorCode == 0) {
				$('#phone').html(data.obfuscatedPhone);
				$('#email').html(data.obfuscatedEmail);
				$('#step1').hide();
				$('#step2').show();
			} else {
				$('#user-name-holder').addClass('error');
			}
		},
		error   : function( xhr, err ) {
			//Failed call
			//logIt({'type':3,'msg':'Failed request for shouldShowRegulationPopup (call from ' + callFrom + '): ' + err});
		}
	});
}
function replaceBackSlash() {
	$('#username').attr("placeholder", $('#username').attr('placeholder').replace('&#92;','\\'));
}

$(document).ready(function(){
	repositionAnchors();
});
function repositionAnchors(){
	var topOffset = $(".header_fixed").outerHeight();
	$(".anchor").css("display", "block");
	$(".anchor").css("height", topOffset);
	$(".anchor").css("margin-top", (-1)*topOffset);
}
function filterTitle(str){
	var result = "";
	if(str){
		result = str.replace(/\"/g, "");
	}
	return result;
}
function getTerms(params){//termsType, isRegulated, containerId, showMenu
	var container;
	if(!params.containerId){
		container = document.body;
	} else {
		container = g(params.containerId);
	}
	
	$.ajax({
		url     : context_path + aoJsonUrl + 'getTermsFiles',
		type    : "POST",
		dataType: "json",
		data    : JSON.stringify({"platformIdFilter":"2", "skinIdFilter":skinId, "termsType":params.termsType}),
		contentType: "application/json;charset=utf-8",
		success : function( data ) {
			//Successful call
			data.filesList.sort(function(a, b) {
				return parseInt(a.orderNum) - parseInt(b.orderNum);
			});
			var menu = document.createElement("ul");
			menu.className = "general_terms_menu";
			if(params.isRegulated){
				menu.className += " general_terms_menu_reg";
			}
			
			var div = document.createElement("div");
			for(var i = 0; i < data.filesList.length; i++){
				var anchor = document.createElement("a");
				anchor.className = "anchor";
				anchor.name = data.filesList[i].title ? filterTitle(data.filesList[i].title) : data.filesList[i].id;
				div.appendChild(anchor);
				anchor = null;
				var part = document.createElement("div");
				part.innerHTML = data.filesList[i].html;
				div.appendChild(part);
				part = null;
				var li = document.createElement("li");
				var a = document.createElement("a");
				a.href = "#" + (data.filesList[i].title ? filterTitle(data.filesList[i].title) : data.filesList[i].id);
				var linkTitle = data.filesList[i].title;
				if(!linkTitle){
					$(data.filesList[i].html).find(".general_terms_h2:eq(0)").each(function(index, el){
						linkTitle = el.innerHTML;
					});
					if(!linkTitle){
						$(data.filesList[i].html).find(".general_terms_h1:eq(0)").each(function(index, el){
							linkTitle = el.innerHTML;
						});
					}
				}
				if(linkTitle){
					a.innerHTML = linkTitle;
					a.className = "anchor-link";
					li.appendChild(a);
					menu.appendChild(li);
				}
				a = null;
				li = null;
			}
			if (params.showMenu) {
				container.appendChild(menu);
			}
			container.appendChild(div);
			menu = null;
			div = null;
			repositionAnchors();
			$(container).removeClass("terms-loading");
			
			if (!isUndefined(params.callBack)) {
				params.callBack();
			}
			
			var anchor = '';
			window.location.search.substring(1)
				.split('&')
				.map(function(current) {
					var split = current.split('=');
					if (split[0] == 'anchor') {
						anchor = split[1];
					}
				})
			if (anchor != "") {
				setTimeout(function() {
					goToByScroll(anchor, 0);
				}, 1000)
			}
		},
		error   : function( xhr, err ) {
			//Failed call
		}
	});
}

var skinCurrencies = [];
function getUserCurencies(id) {
	$.ajax({
		url     : '/jsonService' + aoJsonUrl + "getSkinCurrencies",
		type    : "POST",
		dataType: "json",
		data    : JSON.stringify({skinId: skinId}),
		contentType: "application/json;charset=utf-8",
		success : function(data) {
			//Successful call
			skinCurrencies = data.currency;
			try{
				$('#' + id + '_span').html(skinCurrencies[searchJsonKeyInArray(skinCurrencies, 'id', $('#' + id).val())].symbol);
			} catch(e) {
				logIt({'type': 3, 'msg': e});
			}
		},
		error   : function( xhr, err ) {
			//Failed call
		}
	});
}
function beautifySelectCurency(id){
	var sel = g(id);
	var wrapper = sel.parentNode;
	wrapper.innerHTML += "<span id=" + id + "_span>" + sel.options[sel.selectedIndex].text + "</span>";

	if(g(id).addEventListener){
		g(id).addEventListener('change',function() {g(id + '_span').innerHTML = skinCurrencies[searchJsonKeyInArray(skinCurrencies, 'id', this.value)].symbol;},false);
	}else{
		g(id).attachEvent("onchange", function() {
			g(id + '_span').innerHTML = skinCurrencies[searchJsonKeyInArray(skinCurrencies, 'id', g(id).value)].symbol;}
		);
	}
}

function redirectEtHp(){
	window.location = context_path + "/jsp/index_et.jsf";
}
/*long term cancel*/
var cancelInvTimerLongTerm = null;
function showCancelLongTerm() {
	if (showCnacelInv) {
		var comission = formatAmount(parseInt($('#cancelInvAmountLongTerm').html().replace(/&nbsp;/g,'')) * 0.1, 2);
		$('#cancelInvText').html($('#cancelInvText').html().replace('{amount}', comission));
		$('#chart-overlays').addClass('cancel-inv');
		startCancelTimeoutLongTerm();
		cancelInvTimerLongTerm = setTimeout(function() {
			hideCancelInvMsg();
		},cancelInvTimeOut  * 1000);
	}
}

var cancelInvTimerLoaderLongterm = null
function startCancelTimeoutLongTerm() {
	var cancelTimeOut = cancelInvTimeOut  * 1000;
	var end = new Date().getTime() + (cancelTimeOut);
	var cancelTimeLeft = cancelTimeOut;
	function doIt(){
		var cancelTimeLeftPercent = 100 - (cancelTimeLeft / cancelTimeOut) * 100;
		if (cancelTimeLeftPercent > 100) {cancelTimeLeftPercent = 100;}
		g('cancel-inv-time-left-percent').style.width = cancelTimeLeftPercent + '%';
		
		if (cancelTimeLeft > 0) {
			cancelTimeLeft = end - new Date().getTime();
			if (cancelTimeLeft < 0) {
				cancelTimeLeft = 0;
			}
			cancelInvTimerLoaderLongterm = setTimeout(doIt, 10);
		}
		var seconds = Math.floor(cancelTimeLeft / 1000);
		if (seconds == 0) {
			seconds = '00';
		} else if (seconds < 10) {
			seconds = '0' + seconds;
		}
		var milSec = Math.round(cancelTimeLeft - (seconds * 1000));
		if (milSec == 0) {
			milSec = '000';
		} else if (milSec < 10) {
			milSec = '00' + milSec;
		} else if (milSec < 100) {
			milSec = '0' + milSec;
		}
		//g('cancel-inv-time-left-sec').innerHTML = '00:' + seconds + ':' + milSec;
		g('cancel-inv-time-left-sec').innerHTML = '00:' + seconds;
	}
	doIt();
}

var cancelInvOneTime = false;
function cancelInvLongTerm(id) {
	if (!cancelInvOneTime) {
		cancelInvOneTime = true;
		
		clearTimeout(cancelInvTimerLongTerm);
		clearTimeout(self.cancelInvTimerLoaderLongterm);

		var InvestmentMethodResult = jsonClone(getUserMethodRequest(), {
			investmentId:id
		});
		$.ajax({
			url     : context_path + aoJsonUrl + "cancelInvestment",
			type    : "POST",
			dataType: "json",
			data    : JSON.stringify(InvestmentMethodResult),
			contentType: "application/json;charset=utf-8",
			success : function(data) {
				if (data.errorCode == 0) {
					//Successful call
					$('#chart-overlays').removeClass('cancel-inv');
					$('#chart-overlays').addClass('cancel-inv-msg');
					setTimeout(function() {
						hideCancelInvMsg();
					}, 2000);
					$('._closeLongTerm > a').trigger('click');
					cancelInvOneTime = false;
				} else {
					hideCancelInvMsg();
				}
			},
			error   : function( xhr, err ) {
				//Failed call
				cancelInvOneTime = false;
			}
		});
	}
}

function hideCancelInvMsg() {
	$('#chart-overlays').removeClass('cancel-inv cancel-inv-msg');
}

function disableCanacelInv() {
	$.ajax({
		url     : context_path + aoJsonUrl + "changeCancelInvestmentCheckboxState",
		type    : "POST",
		dataType: "json",
		data    : JSON.stringify(getMethodRequest()),
		contentType: "application/json;charset=utf-8",
		success : function(data) {
			//Successful call
			showCnacelInv = false;
		},
		error   : function( xhr, err ) {
			//Failed call

		}
	});
}
function closeEpgPopUp(){
	top.gd_closePopUp('popUpEpgDeposit');
}
function checkNetellerCurrency() {
	var error = $('#currencyRestriction');
	var currency = $('#epgDepositForm\\:currenciesList');
	var paymentSolutions = $('#epgDepositForm\\:paymentSolutions');
	var paymentSolutionsWrapper = $('#paymentSolutionsWrapper');
	var currenciesListWrapper = $('#currenciesListWrapper');
	
	if (paymentSolutions.val() == 'Neteller' && (currency.val() == 5 || currency.val() == 11  || currency.val() == 12)) {
		error.css('display','block');
		paymentSolutions.prop('disabled', true);
		paymentSolutionsWrapper.addClass('disabled-select');
		currenciesListWrapper.addClass('error');
	} else {
		error.css('display','none');
		paymentSolutions.prop('disabled', false);
		paymentSolutionsWrapper.removeClass('disabled-select');
		currenciesListWrapper.removeClass('error');
	}
}
function resizeIframe(id){
	try {
		var obj = g(id);
		obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
	} catch (e) {
		obj.style.height = 430 + 'px';
		//do nothing
	}
}
function selectWithImages(idSelect, idSelectorHolder) {
	var select = $('#' + idSelect);
	var selectorHolder = $('#' + idSelectorHolder);
	var selectorHolderDropDown = selectorHolder.find('._dropDownSelector');
	var selectedValue = selectorHolder.find('._selectedValue');
	selectedValue.on('click', function() {
		if (!select.is(":disabled")) {
			selectorHolderDropDown.toggle();
		}
	})

	select.each(function(index ) {
		var str = '<li data-value="' + $(this).val() + '">';
		str += '<i class="payment-img ' + $(this).val() + '"></i>';
		str += '</li>';

		selectorHolderDropDown.append(str);

		if (index == 0) {
			selectedValue.append('<i class="payment-img ' + $(this).val() + '">');
		}
	});

	selectorHolderDropDown.find('li').on('click', function() {
		selectTheSelect(this, idSelect, idSelectorHolder);
	});
}
function selectTheSelect(el, idSelect, idSelectorHolder) {
	$('#' + idSelect).val($(el).attr('data-value'));
	$('#' + idSelectorHolder).find('._selectedValue').html('');
	$('#' + idSelectorHolder).find('._selectedValue').append('<i class="payment-img ' + $(el).attr('data-value') + '">');
}
function goToByScroll(id) {
	$.extend(
		jQuery.easing,
		{
			bubblesEasingFunction: function( p ) {
				function easeIn(p){
					return Math.pow( p, 4 );
				}
				return p < 0.5 ?
					easeIn( p * 2 ) / 2 :
					1 - easeIn( p * -2 + 2 ) / 2;
			}
		}
	);
	$('html,body').animate({scrollTop: $("#"+id).offset().top}, 2000, 'bubblesEasingFunction');
}

var BANNERS_LINK_TYPE_POPUP = 1;
function addSlidesToSlider(sliderId, slides){
	if(!sliderId || !slides || !Array.isArray(slides) || (Array.isArray(slides) && slides.length == 0)){
		return;
	}
	slides.sort(function(a, b) {
		return parseInt(a.position) - parseInt(b.position);
	});
	for(var i = (slides.length - 1); i >= 0; i--){
		if(!slides[i].imageName){
			continue;
		}
		
		var li = document.createElement('li');
		li.className = 'slider_li';
		$(li).attr('data-li', 't');
		
		var a = document.createElement('a');
		a.href = slides[i].imageUrl;
		if(slides[i].linkTypeId == BANNERS_LINK_TYPE_POPUP){
			a.target = '_blank';
		}
		
		var img = document.createElement('img');
		img.src = context_path + '/bannerSliderImages/' + slides[i].imageName;
		
		a.appendChild(img);
		li.appendChild(a);
		
		$('#' + sliderId).prepend(li);
		
		var navLi = document.createElement('li');
		$('#' + sliderId + '_nav').prepend(navLi);
	}
	$('#' + sliderId + '_nav li').each(function(index, el){
		if(index == 0){
			el.className = ' hov';
		}else{
			el.className = '';
		}
	});
}

function closeAnimatedPopUp(id, el) {
	$('#' + id).addClass('animatedOnce zoomOutUp');
	setTimeout(function() {
		window.location = 'http://' + location.hostname + el.getAttribute('data-url');

	}, 1000);
}

function noValue(el) {
	el.value = '';
}

function funnyMainMenu (id){
	$("#headerMenu").lavaLamp({
		fx: "easeOutBack",
		speed: 700,
		click: function(event, menuItem) {
			return false;
		}
	});
}

function refreshOnExpiry() {
	if (userLogin && !$('#header').get(0).contentWindow.header_login) {
		location.reload();
	}
}

function openTechnicalAnalysis(el, currentLink){
	if(isHasDeposits){
		el.href = getTechnicalAnalysisRedirectPopupLink(currentLink);
	}else{
		openTechnicalAnalysisDepositPopup();
		return false;
	}
	return true;
}
function getTechnicalAnalysisRedirectPopupLink(currentLink){
	var delimiter = '?';
	if(currentLink.indexOf('/login') > -1){
		currentLink = homePageN;
	}
	if(currentLink.indexOf('?') > -1){
		delimiter = '&';
	}
	return currentLink + delimiter + 'open-techincal-analysis-redirect-popup=1';
}
function openTechnicalAnalysisDepositPopup(){
	if(g('popUpTechnicalAnalysisDeposit')){
		g('popUpTechnicalAnalysisDeposit').style.display = 'block';
	}
}
function openTechnicalAnalysisRedirectPopup(href, redirectTimeout){
	var redirectTimeout = (typeof redirectTimeout == 'undefined') ? 3000 : redirectTimeout;
	if(g('popUpTechnicalAnalysisRedirect')){
		g('popUpTechnicalAnalysisRedirect').style.display = 'block';
		setTimeout(function(){
			window.location = href;
		}, redirectTimeout);
	}
}

function tradeAttemptLogin(el, e, targetLink){
	e.preventDefault();
	
	var link = '';
	if(el && !targetLink){
		link = el.href;
	}
	if(targetLink){
		link = targetLink;
	}
	
	//Create the cookie
	var cookieName = 'alp';//After landing page
	var cookieValue = '';
	if(requestURI.indexOf('/jsp/index.jsf') > -1 || requestURI.indexOf('/jsp/trade_binary_options.jsf') > -1){
		cookieValue = 1;
	}else if(requestURI.indexOf('/jsp/oneTouch.jsf') > -1){
		cookieValue = 2;
	}else if(requestURI.indexOf('/jsp/optionPlus.jsf') > -1){
		cookieValue = 3;
	}else if(requestURI.indexOf('/jsp/trading-bubbles.jsf') > -1){
		cookieValue = 4;
	}else if(requestURI.indexOf('/jsp/trading-dynamics.jsf') > -1){
		cookieValue = 5;
	}else if(requestURI.indexOf('/jsp/eur_usd_options.jsf') > -1 || requestURI.indexOf('/jsp/oil_options.jsf') > -1 || requestURI.indexOf('/jsp/gold_options.jsf') > -1 || requestURI.indexOf('/jsp/dax_options.jsf') > -1 || requestURI.indexOf('/jsp/sandp500_options.jsf') > -1){
		cookieValue = 6;
	}
	
	createCookie(cookieName, '' + cookieValue + '', "1");
	
	window.location = link;
}

function getTheJobDone(platformState) {
	setTimeout(function() {
		if (platformState !== 0) {
			$('#migration-popUp-step1').show();
			$('#migration-popUp-step1').css('display', 'block');
			if (platformState === 1) {
				$('#wrapper').addClass('invest');
				$('#invest').show();
				$('#invest').css('display', 'block');
			} else {
				$('#webtrade').show();
				$('#webtrade').css('display', 'block');
			}
		}
	}, 1000)
}

function showMig(step) {
	$('#migration-popUp-step1').hide();
	if (step === 2) {
		$('#migration-popUp-step2').show();
		if (platformState === 1) {
			$('#wrapper2').addClass('invest');
			$('#invest2').show();
			$('#amount').val(userInfo.balancePlus);
		} else {
			$('#webtrade2').show();
		}
	}
}
function submitStage2() {
	if (parseFloat($('#amount').val()) < 0) {
			$('#errorAmount').show();
			$('#amount').addClass('error-amount-br');
	} else {
		if (platformState === 1) {
			if ( $('#cb1_1').is(':checked') && $('#cb1_3').is(':checked') && $('#cb1_4').is(':checked')) {
				if ($('#cb1_2').is(':checked')) {
					realSubmitStage2(true);
				} else {
					realSubmitStage2(false);
				}
			} else {
				$('#step2_error1').hide();
			}
		} else {
			if ( $('#cb2_1').is(':checked') && $('#cb2_3').is(':checked') && $('#cb2_4').is(':checked') ) {
				if ($('#cb2_2').is(':checked')) {
					realSubmitStage2(true);
				} else {
					realSubmitStage2(false);
				}
			} 
		}
	}
}

function realSubmitStage2(flag) {
	var amount = $('#amount').val();
	if(isNaN(validateCrossSellAmount(amount)) || validateCrossSellAmount(amount) < 0 || amount == ''){
		showCrossSellAmountError();
		return;
	}
	$('#submitStage2_1').attr("disabled","disabled");
	$('#submitStage2_2').attr("disabled","disabled");
	$('#errorUnexpected').hide();
	$('#errorUnexpectedNetwork').hide();
	$.ajax({
		url     : context_path + aoJsonUrl + "saveUserMigration",
		type    : "POST",
		dataType: "json",
		data    : JSON.stringify({acceptMail: flag, amount: validateCrossSellAmount(amount)}),
		contentType: "application/json;charset=utf-8",
		success : function(data) {
			if  ( data.errorCode == 0 ) {
				$('#migration-popUp-step2').hide();
				$('#migration-popUp-step3').show();
				if (platformState === 1) {
					$('#wrapper3').addClass('invest');
					$('#invest3').show();
				} else {
					$('#webtrade3').show();
				}
			} else if (data.errorCode == 200) {
				showCrossSellAmountError();
			} else if (data.errorCode == 7003) {
				showCrossSellMinAmountError();	
			} else {
				showCrossSellServerError();
			}
		},
		error   : function( xhr, err ) {
			showCrossSellNetworkError();
		}
	});
}
function validateCrossSellAmount(amount){
	amount = (amount + '').replace(/,/g, '.');
	return parseFloat(amount);
}
function showCrossSellAmountError(){
	$('#errorAmount').show();
	$('#amount').addClass('error-amount-br');
	$('#submitStage2_1').removeAttr("disabled");
}
function showCrossSellMinAmountError(){
	$('#errorMinAmount').show();
	$('#amount').addClass('error-amount-br');
	$('#submitStage2_1').removeAttr("disabled");
}
function showCrossSellServerError(){
	$('#errorUnexpected').show();
	$('#submitStage2_1').removeAttr("disabled");
}
function showCrossSellNetworkError(){
	$('#errorUnexpectedNetwork').show();
	$('#submitStage2_1').removeAttr("disabled");
}
function hideCrossSellAmountError(){
	$('#errorAmount').hide();
	$('#amount').removeClass('error-amount-br');
}
function hideCrossSellMinAmountError(){
	$('#errorMinAmount').hide();
	$('#amount').removeClass('error-amount-br');
}
function clearError() {
	$('#errorAmount').hide();
	$('#amount').removeClass('error-amount-br');
}
function clearMinAmountError() {
	$('#errorMinAmount').hide();
	$('#amount').removeClass('error-amount-br');
}
function openAoDownPopup(){
	if(g('popUpAoDown')){
		g('popUpAoDown').style.display = 'block';
		showGeneralOverlay(19);
	}
}

function closeAoDownPopup(){
	if(g('popUpAoDown')){
		g('popUpAoDown').style.display = 'none';
		removeGeneralOverlay();
	}
}