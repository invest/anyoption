	/********************************************Assets table - anyoption home page********************************************/
	
	if (skinId != 13 && skinId != 14) {
		//Indices
		subscribeTemplate('2', '4',"oppsIndices");
	
		//Commodities
		subscribeTemplate('5', "4","oppsCommodities");
	
		//Currencies (Forex)
		subscribeTemplate('4', '4',"oppsCurrencies");
	}
	//Stocks
	subscribeTemplate('6', '8',"oppsStocks");

	function subscribeTemplate (groupNum, maxRow, tableId) {
		var group1 = ["group_" + skinId.toString() + "_" + groupNum];
		var schema1 = ["key", "command", "ET_NAME", level_skin_group_id, "ET_ODDS",
		               "ET_EST_CLOSE", "ET_OPP_ID", "ET_ODDS_WIN", "ET_ODDS_LOSE", "ET_STATE",
		               level_color, "ET_GROUP_PRIORITY", "ET_GROUP_CLOSE", "ET_SCHEDULED", "ET_RND_FLOOR",
		               "ET_RND_CEILING", "ET_GROUP", "ET_SUSPENDED_MESSAGE", "AO_STATES", "AO_TS"];
		//DynaMetapushTable to DynaGrid
		var newTable = new Subscription("COMMAND",group1,schema1);
		newTable.setDataAdapter("JMS_ADAPTER");
		newTable.setRequestedSnapshot("yes");
		newTable.setRequestedMaxFrequency(0.07);

		//newTable.setMetapushFields(2, 1);
		//newTable.setMetapushSort(30,false,true);
		//newTable.onChangingValues = formatValues_hp;
		//newTable.onItemUpdate = updateItem_hp;
		
		
		var dynaGrid = new DynaGrid(tableId,true);
		dynaGrid.setNodeTypes(["div", "span"]);
		dynaGrid.setMaxDynaRows(maxRow);
		dynaGrid.isHtmlInterpretationEnabled(true);
		dynaGrid.setAutoCleanBehavior(true, false);
		dynaGrid.addListener({
		    // merged updateItem_hp formatValues_hp;
			onVisualUpdate: function(key, visualUpdate, domNode){
				updateItem_hp(key, visualUpdate, domNode, dynaGrid);
			}
		});

		newTable.addListener(dynaGrid);
		lsClient.subscribe(newTable);
		
	}