var timerlen = 5;
var slideAniLen = 250;

var timerID = new Array();
var startTime = new Array();
var obj = new Array();
var endHeight = new Array();
var moving = new Array();
var dir = new Array();

// take class atribute FF or IE
var Browser = {
  Version: function() {
    var version = 999; // we assume a sane browser
    if (navigator.appVersion.indexOf("MSIE") != -1)
      // bah, IE again, lets downgrade version number
      version = parseFloat(navigator.appVersion.split("MSIE")[1]);
    return version;
  }
}
var classAtr = (navigator.userAgent.toLowerCase().indexOf("msie") != -1 && Browser.Version() < 8) ? 'className' : 'class';

// for faq
var lastDiv = null;
var lastLink = null;
function slidedownQes(objname, link){

    if(moving[objname])
            return;

    // classAtr - global atribute class for FF or IE
	link.setAttribute(classAtr, "general_link_10_active");

	if ( lastDiv != null && lastDiv.id != objname ) {  // last != object(current div id)
		slideup(lastDiv.id);   // close div
	}

	if ( lastLink != null && lastDiv.id != objname) {
		lastLink.setAttribute(classAtr, "general_link_10");   // remove color
	}

	lastDiv = document.getElementById(objname);
	lastLink = link;

	if (document.getElementById(objname).style.display != "none" )    {
       	 	moving[objname] = true;
        	dir[objname] = "up";
        	startslide(objname);
    } else {

    	    moving[objname] = true;
        	dir[objname] = "down";
        	startslide(objname);
    }

}


function slidedownQesBanking(objname, link){

    if(moving[objname])
            return;

	// classAtr - global atribute class for FF or IE

	link.setAttribute(classAtr, "bankingLabel_active");

	if ( lastDiv != null && lastDiv.id != objname ) {  // last != object(current div id)
		slideup(lastDiv.id);   // close div
	}

	if ( lastLink != null ) {
		lastLink.setAttribute(classAtr, "bankingLabel");   // remove color
	}

	lastDiv = document.getElementById(objname);
	if (lastLink != link){
		lastLink = link;
	} else {
	 	lastLink = null;
	}
	if (document.getElementById(objname).style.display != "none" )    {
       	 	moving[objname] = true;
        	dir[objname] = "up";
        	startslide(objname);
    } else {

    	    moving[objname] = true;
        	dir[objname] = "down";
        	startslide(objname);
    }

}



function slidedown(objname){
        if(moving[objname])
                return;

        if(document.getElementById(objname).style.display != "none")
        {
       	 	moving[objname] = true;
        	dir[objname] = "up";
        	startslide(objname);
        } else {
    	    moving[objname] = true;
        	dir[objname] = "down";
        	startslide(objname);
        }
}

function slideup(objname){
        if(moving[objname])
                return;

        if(document.getElementById(objname).style.display == "none")
                return; // cannot slide up something that is already hidden

        moving[objname] = true;
        dir[objname] = "up";
        startslide(objname);
}

function startslide(objname){
        obj[objname] = document.getElementById(objname);
		obj[objname].style.display = "block";
        endHeight[objname] = obj[objname].offsetHeight;
		obj[objname].style.display = "none";
        startTime[objname] = (new Date()).getTime();

        if(dir[objname] == "down"){
                //obj[objname].style.height = "1px";
        }
        obj[objname].style.display = "block";
        timerID[objname] = setInterval('slidetick(\'' + objname + '\');',timerlen);
}

function slidetick(objname){
        var elapsed = (new Date()).getTime() - startTime[objname];

        if (elapsed > slideAniLen)
                endSlide(objname)
        else {
                var d =Math.round(elapsed / slideAniLen * endHeight[objname]);
                if(dir[objname] == "up")
                        d = endHeight[objname] - d;

                obj[objname].style.height = d + "px";
        }

        return;
}

function endSlide(objname){
        clearInterval(timerID[objname]);

        if(dir[objname] == "up")
                obj[objname].style.display = "none";

        obj[objname].style.height = endHeight[objname] + "px";

        delete(moving[objname]);
        delete(timerID[objname]);
        delete(startTime[objname]);
        delete(endHeight[objname]);
        delete(obj[objname]);
        delete(dir[objname]);

        return;
}

var liveLastDiv = null;
var liveLastLink = null;
function slidedownFaq(objname, link){

    if(moving[objname])
            return;

    // classAtr - global atribute class for FF or IE
	link.setAttribute(classAtr, "live_link_12_active");

	if ( liveLastDiv != null && liveLastDiv.id != objname ) {  // last != object(current div id)
		slideup(liveLastDiv.id);   // close div
	}

	if ( liveLastLink != null && liveLastLink.id != objname) {
		liveLastLink.setAttribute(classAtr, "live_link_12");   // remove color
	}

	liveLastDiv = document.getElementById(objname);
	liveLastLink = link;

	if (document.getElementById(objname).style.display != "none" )    {
       	 	moving[objname] = true;
        	dir[objname] = "up";
        	startslide(objname);
    } else {

    	    moving[objname] = true;
        	dir[objname] = "down";
        	startslide(objname);
    }

}