// one touch functions
var submiting = false;
var isSet = false;

function showSlip(srcObj,optionId){
	submitOneTouchFromServerSlip(optionId);
}
function submitOneTouchFromServerSlip(params){
	var optionId = params.itemId;
	var marketId = params.marketId;
	var isOneTouchUp = params.isOneTouchUp;
	var from = params.from;
	shouldShowRegulationPopup("one_touch", function(){
		if (submiting){return false};
		submiting = true;
		optimoveReportTradeEvent({from: from, marketId: marketId, isUp: isOneTouchUp});
		g("oneTouchAO:optionId").value=optionId;
		g("oneTouchAO:odds_win").value=document.getElementById("odds_win_"+optionId).value;
		g("oneTouchAO:odds_lose").value=document.getElementById("odds_lose_"+optionId).value;
		g("oneTouchAO:pageLvl").value=document.getElementById("pageLvl_"+optionId).value;
		g("oneTouchAO:units").value=document.getElementById("select_"+optionId).value;
		g("oneTouchAO:utcOffset").value=new Date().getTimezoneOffset();
		myfaces.oam.submitForm('oneTouchAO','oneTouchAO:hiddenSubmit');
		submiting = false;
	});
}
function errorInSlipNotLogin(optionId){
	document.getElementById("loginErrorTR_"+optionId).style.display = "";
}
function closeReceipt(){
	document.getElementById("oneTouchAO:closeReceipt").value="1";
}
