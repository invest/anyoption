/*file: gd_add_scroller.js*/
function g(id){return document.getElementById(id);}
zxcPos = function(obj){
	var rtn=[0,0];
	while(obj){
		rtn[0]+=obj.offsetLeft;
		rtn[1]+=obj.offsetTop;
		obj=obj.offsetParent;
	}
	return rtn;
}
var add_scroller_contr = new Array();
function add_scroller(id,holder){
	var e = g(id);
	var holder = g(holder);
	
	try{
		holder.style.position = 'absolute';
		holder.style.left = '-9999px';
		holder.style.display = 'block';
	}catch(e){}
	var h = e.scrollHeight;
	var h2 = e.offsetHeight;
	e.style.width = e.parentNode.offsetWidth+"px";
	try{
		holder.style.position = '';
		holder.style.left = '';
		holder.style.display = '';
	}catch(e){}
	if(h2 < h){
		var s = document.createElement('span');
		s.className = "scroller_m";
		e.parentNode.onmouseover = function(){s.className += " hov";}
		e.parentNode.onmouseout = function(){s.className = s.className.replace(/hov/gi,'');add_scroller_contr[id] = false;}
		e.parentNode.appendChild(s);
		var ss = document.createElement('span');
		ss.className = "scroller_m_in";
		s.appendChild(ss);
		
		ss.onmousedown = function(){add_scroller_drag_st(id);return false;}
		s.onmousemove = function(){add_scroller_drag(s,ss,id,e);return false;}
		s.onmouseup = function(){add_scroller_drag_end(id)}
		// s.onclick = function(){add_scroller_drag_st(id);add_scroller_drag(s,ss,id,e);return false;}
		

		var of = (h2/h)*100;
		
		ss.style.height = of+"%";
		e.onscroll = function(){
			add_scroller_move1(e,ss);
		}
		document.onmousemove = getMousePos;
	}
}
function add_scroller_move1(e,ss){
	var s = ss.parentNode;
	ss.style.top = ((e.scrollTop/e.scrollHeight)*100)+"%";
}
function add_scroller_drag_st(id){
	add_scroller_contr[id] = true;
}
function add_scroller_drag(s,ss,id,e){
	if(add_scroller_contr[id]){
		var p = zxcPos(s);
		var np = mouseY-p[1];
		var hf = (ss.offsetHeight/2);
		if((np-hf>0)&&(np+hf<s.offsetHeight)){
			ss.style.top = np-hf+"px";
		}
		e.scrollTop = ((ss.offsetTop/s.offsetHeight)*e.scrollHeight);
	}
}
function add_scroller_drag_end(id){
	add_scroller_contr[id] = false;
}
var mouseX=0;var mouseY=0;
function getMousePos(e){
	if(!e){
		var e = window.event||window.Event;
	}
	if('undefined'!=typeof e.pageX){
		mouseX = e.pageX;
		mouseY = e.pageY;
		moEl = e.target;
	}
	else{
		mouseX = e.clientX + document.documentElement.scrollLeft;
		mouseY = e.clientY + document.documentElement.scrollTop;
		moEl = event.srcElement;
	}
	if(mouseX < 0){mouseX = 0;}
	if(mouseY < 0){mouseY = 0;}
}
function hide_el(id){
	g(id).style.display = "none";
}