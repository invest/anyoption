if(typeof window.mobile_home_page == 'undefined'){
	var mobile_home_page = false;
}
if(typeof window.register_lp == 'undefined'){
	var register_lp = false;
}
if(typeof window.register_force_valid == 'undefined'){
	var register_force_valid = false;
}

//duplicate from small_functions.js for LPs
function isUndefined(el) {
	return (typeof el == 'undefined' || el == null);
}
//duplicate form small function for LPs
function aJaxCall_json(params){//url,params to post,type,callback,xml,async
	var type = (typeof params.type != 'undefined')?params.type:'GET';
	var post_params = (typeof params.params != 'undefined')?params.params:null;
	var async = (typeof params.async != 'undefined')?params.async:true;
	var xml = (typeof params.xml != 'undefined')?params.xml:false;
	
	var req = init();
	req.onreadystatechange = processRequest;

	function init() {
		if (window.XMLHttpRequest) {
			return new XMLHttpRequest();
		} else if (window.ActiveXObject) {
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
	}

    function processRequest () {
		// readyState of 4 signifies request is complete
		if (req.readyState == 4) {
			// status of 200 signifies sucessful HTTP call
			if (req.status == 200) {
				if (params.callback) {
					if(xml){
						params.callback(req.responseXML);
					}else{
						params.callback(req.responseText);
					}
				}
			}
		}
    }

    this.doGet = function() {
		req.open(type, params.url, async);//type[post,get]
		req.setRequestHeader("Content-Type", "application/json");
		req.send(post_params);
   	}
}
function showAgreemen(){
	//they don't want to link the country dropdown to the terms anymore, comment this part for now
	var general_terms_url = general_terms_link.toString()/*+general_terms_link_cid.toString()*/;
	if(mobile_lp) {
		general_terms_url += "&from=mobilelp";		
	}
	window.open(general_terms_url,'','scrollbars=yes,height=600,width=970,resizable=yes,toolbar=yes,screenX=0,left=1024,screenY=0,top=600');
}
function checkCountryId(){
	var iframe = document.createElement("iframe");
	var domain = document.domain.replace('cdn.','');
	domain = domain.replace('bgtestenv.','');
	domain = domain.replace('testenv.','');
	//iframe.src="http://cdn."+domain+"/getIp.php?dom="+domain;
	if (register_lp) {
		iframe.src = (forceHttps ? 'https' : 'http') + "://cdn."+domain+"/getIp.php?dom="+domain;
	} else {
		if(domain == 'etrader.co.il') {
			iframe.src = (forceHttps ? 'https' : 'http') + "://cdn-t."+domain+"/getIp.php?dom="+domain;
		} else {
			iframe.src = (forceHttps ? 'https' : 'http') + "://images."+domain+"/getIp.php?dom="+domain;//ex cdnak
		}
	}
	iframe.id="getIp_iframe";
	iframe.name="getIp_iframe";
	iframe.title="getIp_iframe";
	iframe.style.display="none";
	document.body.appendChild(iframe);
	// register_country_select(25,true);
}
var funnel_countryId = '';
function register_country_select(el, fromUrl, setCountryId){
	var countryId;
	if (!isUndefined(setCountryId)) {
		countryId = setCountryId;
	} else {
		countryId = el.getAttribute('data-countryId');
	}
	if(fromUrl){
		countryIdByIp = countryId;
		if(skinId != 1) {
			if(isBlockedCountry(countryIdByIp)){
				countryId = firstCountryId;
			}
		}
		if(typeof getQueryVariables_array['funnel_countryId'] != 'undefined'){
			countryId = getQueryVariables_array['funnel_countryId'];
		}
		general_terms_link_cid = "?refresh=true&cid=1";
		g(funnel_countryId_prefix+'funnel_countryId').value = 1;
	}
	if((skinId == 1)&&(!fromUrl)){
		g('funnel_phonePrefix').value = countryId;
		g(funnel_countryId_prefix+'funnel_countryId').value = 1;
		g('funnel_drop_country').innerHTML = "";
		g('funnel_drop_countryId').innerHTML = '0'+countryId;
		single_errors_map_holder[1][2] = '1';
		checkError(null, g('funnel_drop_h'));
	}
	if(skinId != 1) {
		g("funnel_drop_h").className = "funnel_drop_h";
		g(funnel_countryId_prefix+'funnel_countryId').value = countryId;
		g('funnel_drop_country').innerHTML = countryMiniMap[countryId].a2;
		g('funnel_drop_countryId').innerHTML = "+" + countryMiniMap[countryId].phoneCode;
		general_terms_link_cid = "?refresh=true&cid="+countryId;
		single_errors_map_holder[1][2] = '1';
	}
	var reg_drop_ul = g('funnel_drop_ul').getElementsByTagName('li');
	for(var i=0; i<reg_drop_ul.length; i++){
		reg_drop_ul[i].className = reg_drop_ul[i].className.replace(/active/gi,'');
	}
	try{
		g('funnel_drop_li_'+countryId).className += " active";
	}catch(e){}
	reloadCountries('drop_ul_filter');
	funnel_drop_close();
}
var isAllowIp;
function allowIp(flag){
	isAllowIp = flag;
	return isAllowIp;
}
function isBlockedCountry(countryId){
	var res = false;
	switch (countryId) {
			case 1:
			case 99:
			case 220:
				res = true;
				break;
			}
	return res;			
}
function getQueryVariables() {
	var rtn = new Array();
	rtn[0] = 0;
	if (typeof valuesJson != 'undefined' && valuesJson != '') {
		if (valuesJson.funnel_email != "") {
			rtn['email'] = valuesJson.funnel_email;
		}
		if (valuesJson.funnel_firstName != "") {
			rtn['firstName'] = valuesJson.funnel_firstName;
		}
		if (valuesJson.funnel_lastName != "") {
			rtn['last_Name'] = valuesJson.funnel_lastName;
		}
		if (valuesJson.funnel_countryId != "") {
			rtn['funnel_countryId'] = valuesJson.funnel_countryId;
		}
		if (valuesJson.funnel_mobilePhone != "") {
			rtn['mobilePhone'] = valuesJson.funnel_mobilePhone;
		}
		if (valuesJson.funnel_phonePrefix != "") {
			rtn['mobilePhonePref'] = valuesJson.funnel_phonePrefix;
		}
	} else {
		var fields_from_url = ['email','firstName','last_Name','funnel_countryId','mobilePhone','mobilePhonePref'];
	    var query = window.location.search.substring(1);
	    var vars = query.split('&');
	    for (var i = 0; i < vars.length; i++) {
	        var pair = vars[i].split('=');
			if(fields_from_url.indexOf(pair[0]) > -1){
				rtn[pair[0]] = decodeURIComponent(pair[1]);
				rtn[0]++;
			}
	    }
	}
    return rtn;
}
var getQueryVariables_array = getQueryVariables();
function get_funnel_html(){
	//getQueryVariables()
	if(!g('funnel_firstName')){
		var ajax_url = context_path_ajax + "/jsp/html_elements/register_content_funnel.html?s=" + skinId + "&lp=" + register_lp;
		if(mobile_lp){
			ajax_url = context_path_ajax + "/jsp/html_elements/register_content_funnel_mobile.html?s=" + skinId + "&from=mobilelp";
		}
		ajax_url  += (funnel_locale != '')?"&locale=" + funnel_locale:"";
		function ajaxCallBack(response){
			g(funnel_div_id).innerHTML = response;
			// checkCountryId();
			getSortedCountries();
			if(typeof isShortForm == "undefined"){var isShortForm = 'true';}
			g('register_from_short_reg').value = (isShortForm === 'false')?"false":"true";
			funnel_fill_inputs();
			if(mobile_home_page){
				var countries = document.getElementById("funnel_drop_h");
				countries.onmouseover = null;
				countries.onmouseout = null;
				var countries = null;
			}
		}
		var ajaxCall = new AJAXInteractionTxt(ajax_url, ajaxCallBack);
		ajaxCall.doGet();
	}else{
		getSortedCountries();
		// checkCountryId();
		if(typeof isShortForm == "undefined"){var isShortForm = 'true';}
		g('register_from_short_reg').value = (isShortForm === 'false')?"false":"true";
		funnel_fill_inputs();
		if(getQueryVariables_array[0] > 1){
			funnel_submit();
			try {
				g('full_reg_top_text_default').style.display='none';
				g('full_reg_top_text_alt').style.display='block';
			} catch(e) {;}
		}
	}
	if (typeof errorJson != 'undefined' && errorJson != '' && typeof register_page != 'undefined' && register_page) {
		funnel_results(errorJson);
	}
}
function getSortedCountries() {
	var paramsAjax = JSON.stringify({skinId: skinId});
	var path = '';
	if (register_lp) {
		path = context_path_json;
	} else {
		path = context_path;
	}
	var ajaxCall = new aJaxCall_json({'type':'POST','url':path + '/AnyoptionService/getSortedCountries','params':paramsAjax,'callback':getSortedCountriesCallBack});
	ajaxCall.doGet();
}
function getSortedCountriesCallBack(data) {
	data = eval('('+ data +')');
	var dropDownUl = g('funnel_drop_ul');
	countryMiniMap = data.countriesMap;
	for (var key in data.countriesMap) {
       if (data.countriesMap.hasOwnProperty(key)) {
			var li = document.createElement('li');
			li.id = 'funnel_drop_li_' + key;
			li.setAttribute('data-countryId', key);
			li.innerHTML = data.countriesMap[key].displayName;
			li.onclick = function() {
				register_country_select(this, false);
			}
			
			dropDownUl.appendChild(li);
       }
    }
	var defaultCountyId = '';
	if (isUndefined(data.countriesMap[data.ipDetectedCountryId])) {
		defaultCountyId = data.skinDefautlCointryId;
	} else {
		defaultCountyId = (data.ipDetectedCountryId != 0) ? data.ipDetectedCountryId : data.skinDefautlCointryId; 
	}
	register_country_select('',true, defaultCountyId);
}
function funnel_fill_inputs(){
	try {
		var funnel_form_skinId = g('funnel_form_skinId');
		if (typeof funnel_form_skinId != 'undefined' && funnel_form_skinId != null) {
			var s_cookie = getCookie('s');
			funnel_form_skinId.value = (s_cookie != null) ? s_cookie : getUrlValue('s'); 
		}
	} catch(e) {}
	if(typeof getQueryVariables_array['firstName'] != 'undefined'){
		g('funnel_firstName').value = getQueryVariables_array['firstName'];
	}
	if(typeof getQueryVariables_array['last_Name'] != 'undefined'){
		g('funnel_lastName').value = getQueryVariables_array['last_Name'];
	}
	if(typeof getQueryVariables_array['email'] != 'undefined'){
		g('funnel_email').value = getQueryVariables_array['email'];
	}
	if(typeof getQueryVariables_array['email'] != 'undefined'){
		g('funnel_email').value = getQueryVariables_array['email'];
	}
	if(typeof getQueryVariables_array['mobilePhonePref'] != 'undefined'){
		g('funnel_phonePrefix').value = getQueryVariables_array['mobilePhonePref'];
		g('funnel_drop_countryId').innerHTML = '0'+parseInt(getQueryVariables_array['mobilePhonePref'],10);
		g('funnel_drop_country').innerHTML = '';
	}
	if(typeof getQueryVariables_array['mobilePhone'] != 'undefined'){
		var sub = '';
		if(typeof getQueryVariables_array['mobilePhoneCode'] != 'undefined'){
			getQueryVariables_array['mobilePhoneCode'] = getQueryVariables_array['mobilePhoneCode'].toString().replace('+','');
			if(getQueryVariables_array['mobilePhoneCode'].length > 3){
				sub = getQueryVariables_array['mobilePhoneCode'].substr(1,10);
			}
		}
		
		g('funnel_mobilePhone').value = sub+getQueryVariables_array['mobilePhone'].toString();
	}
}
/* insert into register attempts AJAX call */
function insertUpdateRegisterAttempts() {
	//if(!register_lp){//do not inser contact for lps
		var isMarketingTracking = (typeof getQueryVariables_array['marketingTracking'] != 'undefined')?getQueryVariables_array['marketingTracking']:false;
		
		var firstName = g("funnel_firstName");
		var lastName = g("funnel_lastName");
		var email = g("funnel_email");
		var countryId = g("funnel_countryId");
		var landMobilePhone = g("funnel_mobilePhone");
		var landMobilePhonePref = g("funnel_phonePrefix");
	
		if((emailValidationRegEx.test(email.value))||(landMobilePhone.value.length > 6 && isInt(landMobilePhone.value))){
			var dataString = '';
			if(typeof isShortForm == undefined){var isShortForm = 'true';}
			dataString += (isShortForm === 'false')?"emailRegisterAttempts":"quickStartEmailRegisterAttempts";
	
			dataString += "=" + encodeURIComponent((email.value !== email.defaultValue)?email.value:'')
				+ "&firstNameRegisterAttempts="	+ encodeURIComponent((firstName.value !== firstName.defaultValue)?firstName.value:'')
				+ "&lastNameRegisterAttempts=" + encodeURIComponent((lastName.value !== lastName.defaultValue)?lastName.value:'')
				+ "&mobilePhoneRegisterAttempts=" + encodeURIComponent((landMobilePhone.value !== landMobilePhone.defaultValue)?landMobilePhonePref.value+landMobilePhone.value:'')
				+ "&countryRegisterAttempts=" + encodeURIComponent(countryId.value)
				+ "&isShortForm=" + isShortForm
				+ "&marketingTracking=" + isMarketingTracking;
			$.ajax({
				type: "POST",
				url: context_path_ajax+"/jsp/ajax.jsf",
				data: dataString
			});
		}
	//}
}
function funnel_submit(){
	var funnel_fingerPrint = g('funnel_fingerPrint'); 
	if(funnel_fingerPrint.value == ''){
		funnel_fingerPrint.value = new Fingerprint().get();
	}
	var btn_open_account = g('btn_open_account');
	btn_open_account.className += " disabled";
	btn_open_account.onclick = function(){return false;}
	
	if(skinId != 1){
		var selectedCountryId = parseInt(g('funnel_countryId').value);
		if((isBlockedCountry(countryIdByIp) && !isAllowIp) || isBlockedCountry(selectedCountryId)){	    
			showErrorsBlocked(isBlockedCountry(selectedCountryId));
			return;					
		}
	}	
	isShowErrors = false;
	isAsync = false;
	if(skinId != 1){
		validateCheckbox();
	}
	validatePasswordConfirm();
	validatePassword();
	validatePhoneNumberTag();
	if(skinId == 1) {
		validateEtraderPhoneCode();
	}
	//test only
	validateEmail();
	if(mobile_home_page/* || register_lp*/ || register_force_valid){
		checkEmpty(g("funnel_email"), MSG_ERROR_MANDATORY_FIELD);
	}
	validateLastName();
	validateFirstName();
	isAsync = true;
	isShowErrors = true;
	if(typeof register_page == 'undefined'){register_page = false;}
	
	if((errorMap.size() <= 2) || register_page || mobile_lp || mobile_home_page/* || register_lp*/ || register_force_valid){
		showErrors();
	}	
	if (errorMap.size() == 0) {
		appendHiddenInput(g('funnel_form'), "combid", getUrlValue("combid"));
		appendHiddenInput(g('funnel_form'), "dp", getUrlValue("dp"));
		appendHiddenInput(g('funnel_form'), "aff_sub1", getUrlValue("aff_sub1"));
		appendHiddenInput(g('funnel_form'), "aff_sub2", getUrlValue("aff_sub2"));
		if(getUrlValue("gclid") != ""){
			appendHiddenInput(g('funnel_form'), "gclid", getUrlValue("gclid"));
		}else{
			appendHiddenInput(g('funnel_form'), "aff_sub3", getUrlValue("aff_sub3"));
		}
		g('funnel_form').submit();
	} else {
		handle_error_count(errorMap.size());
	}
}
function appendHiddenInput(parentEl, param, value){
	var newInput = document.createElement("input");
	newInput.type = "hidden";
	newInput.name = param;
	newInput.value = value;
	parentEl.appendChild(newInput);
}
function getUrlValue(key){
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
		if(key == pair[0]){
			return decodeURIComponent(pair[1]);
		}
    }
    return '';
}
function funnel_results(jsonRes){
	var count = 0;
	for (var key in jsonRes) {
		if (jsonRes.hasOwnProperty(key)) {
			if(key == jsonRes[key]){
				g('funnel_global_error_txt').innerHTML = jsonRes[key];
				count++;
			}
			else if(key == "forbiddenRegisterRedirect"){
				window.location = jsonRes[key];
				return;
			}else{
				var field = g(key);
				field.className = field.className.replace(/correct/g,'');
				field.className += " error";
				var error_msg_holder = g(key+"_error_txt");
				error_msg_holder.innerHTML = jsonRes[key];
				error_msg_holder.style.display = "";
				var funnel_error_line = g('funnel_error_line');
				if(typeof funnel_error_line != "undefined" && funnel_error_line != null){
					var top = find_top_level(field,'data-funnel_row').offsetTop+(field.offsetHeight/2);
					var bottom = error_msg_holder.offsetTop+(error_msg_holder.offsetHeight/2);
					funnel_error_line.style.top = top+'px';
					funnel_error_line.style.display = 'block';
					funnel_error_line.style.height = bottom-top+"px";
				}
				
				error_msg_holder = g(key+"_error_txt_in");
				error_msg_holder.innerHTML = jsonRes[key];
				error_msg_holder.style.display = "";
				
				var single_err_check = compare2dArray(single_errors_map,key);
				if(single_err_check[0]){
					var errSignTag = g(single_errors_map[single_err_check[1]][0] + "_error");
				}
				else{
					var errSignTag = g(key + "_error");
				}
				errSignTag.className = "funnel_error_bad";
				
				count++;
			}
		}
	}
	handle_error_count(count);
}
function handle_error_count(count){
	if(typeof register_page == 'undefined'){register_page = false;}
	if(mobile_lp) {
		if(count == 0) {
			var protocol = window.location.protocol;
			var host = window.location.host;
			var path_array = window.location.pathname.split("/");
			var path_folder = "";
			for(var i = 1; i < path_array.length - 1; i++) {
				path_folder += (path_array[i] + "/");
			}
			var success_page = "success_" + getLangCodeBySkinId(skinId) + ".shtml?" + queryString;
			var redirect_url = protocol + "//" + host + "/" + path_folder + success_page;
			window.location = redirect_url;
		}else{
			var btn_open_account = g('btn_open_account');
			btn_open_account.className = btn_open_account.className.replace(" disabled","");
			btn_open_account.onclick = function(){funnel_submit();return false;}
			return false;
		}
	}else{
		if(count === 0){//you are registered and logged -> redirecting to deposit (or "thank you page" if mobile LP)
			if(skinId != 1){
				if(!mobile_home_page){
					window.location = deposit_page_url;
				}else{
					window.location = mobile_deposit_page_url;
				}
			}else{
				window.location = register_additional_first_page_url;
			}
		}else if((count < 3) || mobile_home_page/* || register_lp*/ || register_force_valid){//you are not so bad, stay on page and fill correctly
			var btn_open_account = g('btn_open_account');
			btn_open_account.className = btn_open_account.className.replace(" disabled","");
			btn_open_account.onclick = function(){funnel_submit();return false;}
			return false;
		}else{//you cannot be more wrong go to full reg
			if(!register_page){//if short reg
				var firstName = g("funnel_firstName");
				var lastName = g("funnel_lastName");
				var email = g("funnel_email");
				var countryId = g("funnel_countryId");
				var phonePrefix = g("funnel_phonePrefix");
				var landMobilePhone = g("funnel_mobilePhone");
				
				var url = '';
				url += ((email.value !== "")&&(email.value !== email.defaultValue))?((url === '')?"?":"&")+"email=" + encodeURIComponent(email.value):'';
				url += ((firstName.value !== "")&&(firstName.value !== firstName.defaultValue))?((url === '')?"?":"&")+"firstName=" + encodeURIComponent(firstName.value):'';
				url += ((lastName.value !== "")&&(lastName.value !== lastName.defaultValue))?((url === '')?"?":"&")+"last_Name=" + encodeURIComponent(lastName.value):'';
				url += (countryId.value != "")?((url === '')?"?":"&")+"funnel_countryId=" + encodeURIComponent(countryId.value):'';
				url += (phonePrefix.value != "")?((url === '')?"?":"&")+"mobilePhonePref=" + encodeURIComponent(phonePrefix.value):'';
				url += ((landMobilePhone.value !== "")&&(landMobilePhone.value !== landMobilePhone.defaultValue))?((url === '')?"?":"&")+"mobilePhone=" + encodeURIComponent(landMobilePhone.value):'';
				window.location = register_page_url+url;		
			}
			else{
				var btn_open_account = g('btn_open_account');
				btn_open_account.className = btn_open_account.className.replace(" disabled","");
				btn_open_account.onclick = function(){funnel_submit();return false;}
			}
		}
	}
}
function userAdditional_submit(){
	//window.location = accept_agreement_page_url;
	
	var btn = g('btn_userAdditional');
	btn.className += " disabled";
//	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(skinId == 1) {
		validateBirthYear(g('userAdditionalInfoForm:birthYear'));
		validateBirthMonth(g('userAdditionalInfoForm:birthMonth'));
		validateBirthDay(g('userAdditionalInfoForm:birthDay'));
	}
	validateZipCode(g('userAdditionalInfoForm:zipCode'));
	if (skinId != 1) {
		validateCityName();	
	} else {
		validateCityNameET(g('userAdditionalInfoForm:cityNameAO'));
	}
	validateStreet(g('userAdditionalInfoForm:street'));
	if(skinId == 1){
		var streetnoTag = g('userAdditionalInfoForm:streetno');
		var streetnoVal = streetnoTag.value;
		var streetnoMsg = null;
		if (streetnoVal == null || trim(streetnoVal) === "" || streetnoVal == streetnoTag.alt) {
			streetnoMsg = translatedTxt[MSG_ERROR_MANDATORY_FIELD];
		}
		checkError(streetnoMsg, streetnoTag);
	}
	if(skinId == 1 && isETRegulation){
		validateHolderIdNum(g('userAdditionalInfoForm:idNum'));
	}
	if(skinId == 1) {
		validatePhoneNumberTag(g('userAdditionalInfoForm:mobilePhoneSuffix'));
		//validateEtraderPhoneCode(g('userAdditionalInfoForm:mobilePhonePref'));
		var phoneCodeTag = g('userAdditionalInfoForm:mobilePhonePref');
		var phoneCodeVal = phoneCodeTag.value;
		var phoneCodeMsg = null;
		if (phoneCodeVal == null || trim(phoneCodeVal) === "" || !isNumber(phoneCodeVal)) {
			phoneCodeMsg = translatedTxt[MSG_ERROR_MANDATORY_FIELD];
		}
		checkError(phoneCodeMsg, phoneCodeTag);
	}
	validateLastName(g('userAdditionalInfoForm:lastName'));
	validateFirstName(g('userAdditionalInfoForm:firstName'));
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('userAdditionalInfoForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('userAdditionalInfoForm','userAdditionalInfoForm:hiddenUserAdditional');
	}
	else{
		var btn = g('btn_userAdditional');
		btn.className = btn.className.replace(" disabled",'');
//		btn.onclick = function(){userAdditional_submit();}
	}
}
function regAcceptAgreement_submit(){
	window.location = deposit_page_url_et;
}
function newCard_submit(full){
	var btn = g('btn_newCard');
	btn.className += " disabled";
	//btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full && (skinId != 1)){
		validateBirthYear();
		validateBirthMonth();
		validateBirthDay();
		validateZipCode();
		if (skinId != 1) {
			validateCityName();	
		} else {
			validateCityNameET();
		}
		validateStreet();			
	}
	validateHolderName();
	if(skinId == 1){
		validateHolderIdNum();
	}
	validateExpYear();
	validateExpMonth();
	if(skinId == 1){
		validateExpDate(g('newCardForm:expMonth'), g('newCardForm:expYear'));
	}
	validateCCPass();
	validateCCNumber();
	validateCCType();
	validateDepositAmount(null);
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('newCardForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('newCardForm','newCardForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_newCard');
		btn.className = btn.className.replace(" disabled",'');
		//btn.onclick = function(){newCard_submit(full);}
	}
}
function direct24_submit(full, paymentType){
	shouldShowRegulationPopup('direct24_submit', function(){
		var btn = g('btn_direct24_'+paymentType);
		btn.className += " disabled";
//	btn.onclick = function(){return false;}
		isShowErrors = false;
		isAsync = false;
		if(full){
			validateBirthYear();
			validateBirthMonth();
			validateBirthDay();
			validateZipCode();
			validateCityName();	
			validateStreet();
		}
		validateDepositAmount(null);
		validateBenefiName();
		validateSortCode();
		// used in both powerpay and directpay
		///validateDirectAccNum();
		isAsync = true;
		isShowErrors = true;
		showErrors();
		if (errorMap.size() == 0) {
			g('direct24_'+paymentType).onsubmit = function(){return true;}
			return myfaces.oam.submitForm('direct24_'+paymentType,'direct24_'+paymentType+':hiddenDeposit');
		}
		else{
			var btn = g('btn_direct24_'+paymentType);
			btn.className = btn.className.replace(" disabled",'');
//		btn.onclick = function(){direct24_submit(full, paymentType);}
		}
	})
}
function neteller_submit(full){
	shouldShowRegulationPopup('neteller_submit', function(){
		var btn = g('btn_depositForm');
		btn.className += " disabled";
//	btn.onclick = function(){return false;}
		isShowErrors = false;
		isAsync = false;
		if(full){
			validateBirthYear();
			validateBirthMonth();
			validateBirthDay();
			validateZipCode();
			validateCityName();	
			validateStreet();
		}
		//validateDepositAmount(null);
		isAsync = true;
		isShowErrors = true;
		showErrors();
		if (errorMap.size() == 0) {
			g('btn_depositForm').onsubmit = function(){return true;}
			return myfaces.oam.submitForm('depositForm','depositForm:hiddenDeposit');
		}
		else{
			var btn = g('depositForm');
			btn.className = btn.className.replace(" disabled",'');
//		btn.onclick = function(){direct24_submit(full, paymentType);}
		}
	})
}
function eps_submit(full){
	var btn = g('btn_epsForm');
	btn.className += " disabled";
//	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateBirthYear();
		validateBirthMonth();
		validateBirthDay();
		validateZipCode();
		validateCityName();	
		validateStreet();
	}
	validateDepositAmount(null);
	validateBenefiName();
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('direct24_4').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('direct24_4','direct24_4:hiddenDeposit');
	}
	else{
		var btn = g('btn_epsForm');
		btn.className = btn.className.replace(" disabled",'');
//		btn.onclick = function(){eps_submit(full);}
	}
}
function baropay_submit(full){
	var btn = g('btn_baropay');
	btn.className += " disabled";
//	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateBirthYear();
		validateBirthMonth();
		validateBirthDay();
		validateZipCode();
		validateCityName();	
		validateStreet();
	}
	validateBaroLandLinePhone();
	validateSenderName();
	validateDepositAmount(null);
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('baroPayDepositForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('baroPayDepositForm','baroPayDepositForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_baropay');
		btn.className = btn.className.replace(" disabled",'');
//		btn.onclick = function(){baropay_submit(full);}
	}
}
function ukash_submit(full){
	var btn = g('btn_ukashFrom');
	btn.className += " disabled";
//	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateBirthYear();
		validateBirthMonth();
		validateBirthDay();
		validateZipCode();
		validateCityName();
		validateStreet();
	}	
	validateDepositAmount(g('ukashFrom:voucherValue'));
	validateUkashVoucherNumber();
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('ukashFrom').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('ukashFrom','ukashFrom:hiddenDeposit');
	}
	else{
		var btn = g('btn_ukashFrom');
		btn.className = btn.className.replace(" disabled",'');
//		btn.onclick = function(){ukash_submit(full);}
	}
}
function webmoney_submit(full){
	var btn = g('btn_webmoney');
	btn.className += " disabled";
//	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateBirthYear();
		validateBirthMonth();
		validateBirthDay();
		validateZipCode();
		validateCityName();
		validateStreet();
	}
	validateDepositAmount(null);
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('webMoneyDepositForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('webMoneyDepositForm','webMoneyDepositForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_webmoney');
		btn.className = btn.className.replace(" disabled",'');
//		btn.onclick = function(){webmoney_submit(full);}
	}
}
function envoyDeposit_submit(full){
	var btn = g('btn_envoyDeposit');
	btn.className += " disabled";
//	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateBirthYear();
		validateBirthMonth();
		validateBirthDay();
		validateZipCode();
		validateCityName();
		validateStreet();
	}
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('directDepositForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('directDepositForm','directDepositForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_envoyDeposit');
		btn.className = btn.className.replace(" disabled",'');
//		btn.onclick = function(){envoyDeposit_submit(full);}
	}
}
function moneybookers_submit(full){
	var btn = g('btn_moneybookers');
	btn.className += " disabled";
//	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateBirthYear();
		validateBirthMonth();
		validateBirthDay();
		validateZipCode();
		validateCityName();
		validateStreet();	
	}
	validateDepositAmount(null);
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('moneybookersDepositForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('moneybookersDepositForm','moneybookersDepositForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_moneybookers');
		btn.className = btn.className.replace(" disabled",'');
//		btn.onclick = function(){moneybookers_submit(full);}
	}
}
function epg_deposit_submit(full){
	var btn = g('btn_epg');
	btn.className += " disabled";
//	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateBirthYear();
		validateBirthMonth();
		validateBirthDay();
		validateZipCode();
		validateCityName();
		validateStreet();	
	}
	validateDepositAmount(null);
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('epgDepositForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('epgDepositForm','epgDepositForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_epg');
		btn.className = btn.className.replace(" disabled",'');
//		btn.onclick = function(){epg_deposit_submit(full);}
	}
}
function cup_submit(full){
	var btn = g('btn_cup');
	btn.className += " disabled";
//	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateBirthYear();
		validateBirthMonth();
		validateBirthDay();
		validateZipCode();
		validateCityName();	
		validateStreet();
	}
	validateDepositAmount(null);
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('cupForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('cupForm','cupForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_cup');
		btn.className = btn.className.replace(" disabled",'');
//		btn.onclick = function(){cup_submit(full);}
	}
}

function inatecIfrmaeDeposit_submit(full){
	var btn = g('btn_inatecIframeDeposit');
	btn.className += " disabled";
//	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateBirthYear();
		validateBirthMonth();
		validateBirthDay();
		validateZipCode();
		validateCityName();
		validateStreet();
	}	
	validateDepositAmount(null);
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('inatecIframeDepositForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('inatecIframeDepositForm','inatecIframeDepositForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_inatecIframeDeposit');
		btn.className = btn.className.replace(" disabled",'');
//		btn.onclick = function(){inatecIfrmaeDeposit_submit(full);}
	}
}

function oldCard_submit(full){
	var btn = g('btn_oldCard');
	btn.className += " disabled";
//	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	validateCCSelect();
	validateCCPass();
	validateDepositAmount(null);
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('depositForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('depositForm','depositForm:hiddenDeposit');
	} else {
		var btn = g('btn_oldCard');
		btn.className = btn.className.replace(" disabled",'');
//		btn.onclick = function(){oldCard_submit(full);}
	}
}
//function used to clear the form after clicking back button
function reColorFields(){
	try{
		inpFocusShort(g('funnel_firstName'),'t');
		inpFocusShort(g('funnel_lastName'),'t');
		inpFocusShort(g('funnel_email'),'t');
		inpFocusShort(g('funnel_mobilePhone'),'t');
		inpFocusShort(g('funnel_password'),'t');
		inpFocusShort(g('funnel_passwordConfirm'),'t');
		if(g('funnel_terms').checked){
			g('funnel_terms').parentNode.className += " checked";
		}
		errorMap.clear();
		showErrors();
	}catch(e){;}
}
function funnel_drop(th){
	th.parentNode.className += " active";
	//if skinId = 1 (etrader) bypass
	if (skinId != 1 ){
		setFocusOnDropDown();
	}
}
var funnel_drop_timer = null;
function funnel_drop_close(){
	clearTimeout(funnel_drop_timer);
	function doIt(){
		var el = g('funnel_drop_h');
		el.className = el.className.replace(/active/g,'');
		//reloadCountries('drop_ul_filter');
	}
	funnel_drop_timer = setTimeout(doIt,100);
}
function funnel_drop_keep_open(){
	clearTimeout(funnel_drop_timer);
}
function filterCountries(obj){
	var li = obj.parentNode;
	li.style.position = 'static';
	var list = li.parentNode.getElementsByTagName("li");
	var ex = obj.value.toLowerCase();
	for (var i = 1; i < list.length; i++) {
	     var match = list[i].innerHTML.toLowerCase();
	     if (ex.length > 0){
		   if(ex == match.substring(0,ex.length)){
		      list[i].style.display = 'block';
		   }else{
		      list[i].style.display = 'none';
		   }
	     }else{
		    list[i].style.display = 'block';
		 } 
	 } 
}
function reloadCountries(id){
	var element = g(id);
	element.value = '';
	var li = element.parentNode;
	li.style.position = 'absolute';
	var list = li.parentNode.getElementsByTagName("li");
	for (var i = 1; i < list.length; i++) {
		list[i].style.display = 'block';
    } 
}
function setFocusOnDropDown(){
    var element = g('drop_ul_filter');
	element.focus();
}
function compare2dArray(arr,str){
	for(var i=0;i<arr.length;i++){
		for(var ii=0;ii<arr[i].length;ii++){
			if(arr[i][ii] == str){
				return new Array(true,i,ii);
			}
		}
	}
	return new Array(false,0,0);
}
function check_funnel_error_map(index){
	var count = single_errors_map[index].length;
	var bad = 0;
	var good = 0;
	var notchecked = 0;
	for(var i=1; i<count; i++){
		if(single_errors_map_holder[index][i] == '1'){good++;}
		else if(single_errors_map_holder[index][i] == '0'){bad++;}
		else{notchecked++;}
	}
	if(bad > 0){return 'funnel_error_bad';}
	else if(good === (count-1)){return 'funnel_error_good';}
	else{return 'funnel_error_hide'}
}
function clearForm(){
	var form = g('funnel_form');
	if(form){
		var els = form.getElementsByTagName('input');
		for(var i = 0; i < els.length; i++){
			if(els[i].type == 'password'){
				els[i].type = 'text';
			}
			els[i].value = getTagDefaultValue(els[i]);
		}
		checkBox(g('funnel_terms_h_in'));
		var btn_open_account = g('btn_open_account');
		btn_open_account.className = btn_open_account.className.replace(" disabled","");
		btn_open_account.onclick = function(){funnel_submit();return false;}
		btn_open_account = null;
		els = null;
	}
	form = null;
}
addScriptToHead('fingerprint.js?version=cvsTimeStamp','initRegForm_fingerprint');
addScriptToHead('map.js?version=cvsTimeStamp','initRegForm_hashmap');
