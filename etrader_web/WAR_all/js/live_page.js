//Get from lightstreamer
var FLD_KEY				= "key";//1;
var FLD_COMMAND			= "command";// 2;
var LI_CITY				= "LI_CITY";//3 //City
var LI_COUNTRY			= "LI_COUNTRY";//4 // Country
var LI_ASSET			= "LI_ASSET";//5 // Asset
var LI_PROD_TYPE		= "LI_PROD_TYPE";//6 // binary/+/100
var LI_INV_TYPE			= "LI_INV_TYPE";//7 // call/put/buy/sell
var LI_LEVEL			= "LI_LEVEL";//8 // 1.12345
var LI_AMOUNT			= "LI_AMOUNT";//9 // formatted for display investment amount in user currency
var LI_TIME				= "LI_TIME";//10 // number of millis since epoch
var LI_INV_KEY			= "LI_INV_KEY";//11;
var LI_OPP_ID			= "LI_OPP_ID";//12; // opportunity id
var LI_CONVERTED_AMOUNT	= "LI_CONVERTED_AMOUNT";//13; //amount of the investment in USD

var lineNum = 0;

/* 
 * handle updates to live page trades ( active only to logged user )
 */ 
function updateItem_live_investments(key, visualUpdate, domNode) {
	//get asset display name
	var marketName;
	var marketID = visualUpdate.getChangedFieldValue(LI_ASSET);

	if (marketID != null) {
		marketName = getMarketName(marketID);
		
		if(marketID == 15) {
			marketName = "EUR/USD";
		}
		
		if(marketID == 3
				|| marketID == 12
				|| marketID == 13
				|| marketID == 15
				|| marketID == 22
				|| marketID == 23
				|| marketID == 24
				|| marketID == 25
				|| marketID == 61
				|| marketID == 350
				|| marketID == 435
				|| marketID == 436
				|| marketID == 562
				|| marketID == 567) {
			marketName = "Tel Aviv 35";
		}
		
		visualUpdate.setCellValue("LI_ASSET_DISPLAY_NAME", marketName);
		visualUpdate.setCellValue("LI_ASSET", marketID);
	}
	

// TODO	
//	if (checkAllClose()) {
//		//show off hour banner 0
//		switchLiveTable(0);
//	} else {
//		//show the market table 1
//		switchLiveTable(1);
//	}
	
	//function formatValue_live_investments (item, itemUpdate) {
		var tdImageFlag = getChildOfType(domNode, "TD", 1);
		var countryName = getCountryName(visualUpdate.getChangedFieldValue(LI_COUNTRY));	
		var city = visualUpdate.getChangedFieldValue(LI_CITY);
		
		if(city.length > 23) {
			city = city.substring(0, 20) + "...";
		}
		tdImageFlag.innerHTML = "<div><img title='" + countryName + "' src = '" + getFlagImage(visualUpdate.getChangedFieldValue(LI_COUNTRY)) + "' /> " + city + "</div>";	
		
		var tdImageType = getChildOfType(domNode, "TD", 2);

		var info = getProdTypeInfo(visualUpdate.getChangedFieldValue(LI_PROD_TYPE));
		tdImageType.innerHTML = '<center><span class="'+info.className+' tooltipHolder"><span class="theTooltip wsN">'+info.title+'</span></span></center>';
		
		var tdInvType = getChildOfType(domNode, "TD", 4);
		tdInvType.innerHTML = "<div>";
		if (getInvTypeImage(visualUpdate.getChangedFieldValue(LI_INV_TYPE)) != ''){
			tdInvType.innerHTML += "<img src = '" + getInvTypeImage(visualUpdate.getChangedFieldValue(LI_INV_TYPE)) + "' /> ";
		}
		tdInvType.innerHTML += visualUpdate.getChangedFieldValue(LI_LEVEL) + "</div><span style='display:none;'>" +visualUpdate.getChangedFieldValue(LI_INV_TYPE) + "</span>";
			
		if(visualUpdate.getChangedFieldValue(LI_PROD_TYPE) != 1) {
			var tdBinaryOption = getChildOfType(domNode, "TD", 6).style.visibility="hidden";
		}
		
		var table = document.getElementById("investments_table_live");
		var tableRow = table.getElementsByTagName("TR");
		
		for (var i = 1; i < tableRow.length; i++) {
			if(i % 2 == 0) {
				tableRow[i].style.background = "#ebebeb";
			} else {
				tableRow[i].style.background = "";
			}
		}
	}


function getMarketName(marketId) {
	if (marketsDisplayName[marketId]) {
		return marketsDisplayName[marketId];
	}
	return "";
}

function getCountryName(countryId) {
	for (var i = 0; i < countriesDisplayName.length; i++) {
		if (countriesDisplayName[i][0] == countryId) {
			return countriesDisplayName[i][1];
		}
	}
	return "";
}

function getFlagImage(countryId) {
	var flagUrl = imagePath + "/flags/" + countryId + ".png";
	return flagUrl;
}
		 		 
function getProdTypeInfo(typeId) {
	typeId = parseInt(typeId);
	var rtn = {
		className: '',
		title: ''
	};
	switch(typeId) {
		case 1://binary option
			rtn.className = 'inv_binary inv_p';
			rtn.title = txts.binaryOption;
			break;
		case 3://option plus
			rtn.className = 'inv_plus inv_p';
			rtn.title = txts.optionPlus;
			break;
		case 4://0100
		case 5:
			rtn.className = 'inv_0100 icon_set_1';
			rtn.title = txts.binary0100;
			break;
		case 6://bubbles
			rtn.className = 'inv_bubbles icon_set_1';
			rtn.title = txts.bubbles;
			break;
		case 7://dynamics
		case 8:
			rtn.className = 'inv_dynamics inv_p';
			rtn.title = txts.dynamics;
			break;
	}
	return rtn;
}

function getInvTypeImage(typeId) {
	var TypeUrl = "";
	if(typeId == 1 || typeId == 4 || typeId == 7) {
		 TypeUrl = image_context_path + "/arrowTable_Call.png";
	}
	if(typeId == 2 || typeId == 5 || typeId == 8) {
		TypeUrl = image_context_path + "/arrowTable_Put.png";
	}
	return TypeUrl;
}

function getChildOfType(node, childType, order) {
	var pos = 0;
	for (var i = 0; i < node.childNodes.length; i++) {
		if (node.childNodes[i].nodeName == childType) {
			pos++;
			if (pos == order) {
				return node.childNodes[i];
			}
		}
	}
}

function showPurchaseElement(isShow, infoId, devNum) {
    var msg = getChildOfType(infoId, "DIV", devNum);
	if (isShow == 'true') {
	    msg.style.display = "block";
	} else {
    	msg.style.display = "none";
	}
}
var intreval;
function sendToTradeBox(info){//moved
	var flag = false;
	var td = info.parentNode;
	var tr = td.parentNode;
	var assetTD = getChildOfType(tr, "TD", 3);
	var assetId = getChildOfType(assetTD, "SPAN", 1).innerHTML;
	var oppId = getChildOfType(assetTD, "SPAN", 2).innerHTML;
	var invTypeTD = getChildOfType(tr, "TD", 4);
	var invTypeId = getChildOfType(invTypeTD, "SPAN", 1).innerHTML;
	fromGraph = 4; //Live Trades page.
	showMarketsMenu_box = 0;
	selectMarket(assetId);
}

function simulatePutOrCall(tr, invTypeId, assetId) {
	try {
		var boxTemp = document.getElementById('box0');
		var index;
		boxTemp = getChildOfType(boxTemp, "TABLE", 1);
		boxTemp = getChildOfType(boxTemp, "TBODY", 1);
		boxTemp = getChildOfType(boxTemp, "TR", 2);
		boxTemp = getChildOfType(boxTemp, "TD", 1);
		boxTemp = getChildOfType(boxTemp, "DIV", 1);
		boxTemp = getChildOfType(boxTemp, "TABLE", 1);
		boxTemp = getChildOfType(boxTemp, "TBODY", 1);
		boxTemp = getChildOfType(boxTemp, "TR", 1);
		boxTemp = getChildOfType(boxTemp, "TD", 1);
		boxTemp = getChildOfType(boxTemp, "TABLE", 1);
		boxTemp = getChildOfType(boxTemp, "TBODY", 1);
		if (invTypeId == 1) {
			index = 3;
		} else {
			index = 5;
		}
		boxTemp = getChildOfType(boxTemp, "TR", index);
		boxTemp = getChildOfType(boxTemp, "TD", 1);
		var img = getChildOfType(boxTemp, "IMG", 1);
		var imgSrc = img.src.toString();
		
		if(imgSrc.indexOf('call.gif') > -1 || imgSrc.indexOf('put.gif') > -1) {
			clearInterval(intreval);
			if (typeof img.onclick == "function") {
	    		img.onclick.apply(boxTemp);
			}
			var amountTextBox = document.getElementById('box0').getElementsByTagName("input")[0];
			
			var amount = getChildOfType(tr, "TD", 5);
			var args = "amountForLive=" + getChildOfType(amount, "SPAN", 1).textContent
							+ "&marketId=" + assetId;
			
			var request = $.ajax({
			   type: "POST",
			   url: "ajax.jsf",
			   data: args,
			   success: function(msg) {
				   if (msg != null && msg != "-1") {
						amountTextBox.value = msg;
						if (typeof amountTextBox.onkeyup == "function") {
							var amountTextBoxId = $(amountTextBox).attr('id');
							amountTextBoxId = amountTextBoxId.substring(0, amountTextBoxId.indexOf('_'));
							updateWinLoseInt(amountTextBox, amountTextBoxId);
						}
				   }
			   }
			});
			var tradeBox = document.getElementById('box0');
			window.scroll(0,findPos(tradeBox));
		}
	} catch(e) {
		clearInterval(intreval);
	}
}

function findPos(obj) {
	var curtop = 0;
	if (obj.offsetParent) {
		do {
			curtop += obj.offsetTop;
		} while (obj = obj.offsetParent);
	return [curtop];
	}
}

function switchLiveTable(flag) {//moved
	var investmentsTable =  document.getElementById("liveInvestmentsTable");
	var offHourTable = document.getElementById("liveInvestmentsOffHours");
	var trade = document.getElementById("tradeBox");
	var marketsTable = document.getElementById("markets_table");
	var graphOffHours = document.getElementById("tradeOffHours");
	
	if (flag == 0) {
		investmentsTable.style.display = 'none';
		offHourTable.style.display = 'block';
		trade.style.display = 'none';
		markets_table.style.display = 'none';
		graphOffHours.style.display = 'block';		
	} else {
		investmentsTable.style.display = 'block';
		offHourTable.style.display = 'none';
		trade.style.display = 'block';
		markets_table.style.display = 'block';
		graphOffHours.style.display = 'none';
	}
	
}
