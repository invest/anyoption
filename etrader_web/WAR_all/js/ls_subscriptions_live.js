	/******************************************** Subscriptions Live********************************************/

	//var groupSkin = "live_investments" + userLanguage
	var groupSkin = "live_investmentsall";
	var group_trades = ['live_investmentsall'];
	var schema_trades = ["key", "command", "LI_CITY", "LI_COUNTRY", "LI_ASSET", "LI_PROD_TYPE", "LI_INV_TYPE", "LI_LEVEL", "LI_AMOUNT", "LI_TIME", "LI_INV_KEY", "LI_OPP_ID", "LI_CONVERTED_AMOUNT"];
	//DynaMetapushTable to DynaGrid
	var aoTable_trades = new Subscription("COMMAND", group_trades, schema_trades);
	aoTable_trades.setDataAdapter("JMS_ADAPTER");
	aoTable_trades.setRequestedSnapshot("yes");
	aoTable_trades.setRequestedMaxFrequency(1.0);

	//aoTable_trades.setMetapushFields(2, 1);
	//aoTable_trades.setMetapushSort(10,true,false,false);
try{
	if (userLogin) {
		var dynaGridTrades = new DynaGrid("livetrades",true);
		dynaGridTrades.setNodeTypes(["div", "span"]);
		dynaGridTrades.setMaxDynaRows(29);
		dynaGridTrades.setHtmlInterpretationEnabled(true);
		dynaGridTrades.setAutoCleanBehavior(true, false);
		dynaGridTrades.addListener({
		    //  merged updateItem_live_investments formatValue_live_investments;
			onVisualUpdate: function(key, visualUpdate, domNode){
				updateItem_live_investments(key, visualUpdate, domNode);
			}
		});
	
		aoTable_trades.addListener(dynaGridTrades);
		lsClient.subscribe(aoTable_trades);
	}
}catch(e){
	logIt({'type':3,'msg':new Error().stack});
	logIt({'type':3,'msg':e});
}
	var skinBusinessCase = 2; //SKIN_BUSINESS_ANYOPTION 
	if (skinId == 1) {
		skinBusinessCase = 1; //SKIN_BUSINESS_ETRADER
	}
	var group_trends = ["trends_" + skinBusinessCase]; 
	var schema_trends = ["key", "merge", 
		"LTT_MARKET1", "LTT_OPP_ID1", "LTT_TREND1",
		"LTT_MARKET2", "LTT_OPP_ID2", "LTT_TREND2", 
		"LTT_MARKET3", "LTT_OPP_ID3", "LTT_TREND3"];
	//DynaMetapushTable to DynaGrid 
	var aoTable_trends = new Subscription("MERGE", group_trends, schema_trends);
	aoTable_trends.setDataAdapter("JMS_ADAPTER");
	aoTable_trends.setRequestedSnapshot("yes");
	aoTable_trends.setRequestedMaxFrequency(1.0);
	//aoTable_trends.onItemUpdate = updateItem_live_trade;
	//aoTable_trends.onChangingValues = formatValues_live_trade;
	//aoTable_trends.setMaxDynaRows(3);
	//aoTable_trends.setMetapushFields(2, 1);

	var dynaGridTrends = new DynaGrid("trends",true);
	dynaGridTrends.setNodeTypes(["div", "span"]);
	dynaGridTrends.setMaxDynaRows(3);
	dynaGridTrends.setHtmlInterpretationEnabled(true);
	dynaGridTrends.setAutoCleanBehavior(true, false);
	dynaGridTrends.addListener({
	    // merged updateItem_live_trade formatValues_live_trade;
		onVisualUpdate: function(key, visualUpdate, domNode){
			updateItem_live_trade(key, visualUpdate, domNode);
		}
	});
	aoTable_trends.addListener(dynaGridTrends);
	lsClient.subscribe(aoTable_trends);