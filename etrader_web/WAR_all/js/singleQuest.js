/*********** singleQuest.js ************/

function updateSingleQuestValues() {
	var dataa = "numUserAnswers=" + (questionsIdArr.length + 3);
	var item;
	var ii = 0;
	var index = -1;
	var hasAnswer;
	for(var i = 0; i < questionsIdArr.length; i++) {
		item = document.getElementById("regSingleQuestionnaireForm:question_" + questionsIdArr[i][0]);
		hasAnswer = false;
		if (item.type!=null && item.type!=undefined) {
			if (item.value!=null && item.value!=undefined && item.value!="") {
				dataa += "&userAnswer" + ii + "Id=" + questionsIdArr[i][1];
				dataa += "&answer" + ii + "Id=" + item.value;				
			} else {
				dataa += "&userAnswer" + ii + "Id=" + questionsIdArr[i][1];
			}
			if(i == 6){
				if(document.getElementById("regSingleQuestionnaireForm:other_text").value != ""){
					dataa += "&answer" + ii + "Text=" + document.getElementById("regSingleQuestionnaireForm:other_text").value;
				}
			}
			ii++;
		}
	}
	if (index != -1) {
		dataa += "&answer" + index + "Text=" + encodeURIComponent(document.getElementById("regSingleQuestionnaireForm:other_text").value);
	}	
	
	$.ajax({
	   type: "POST",
	   url: "ajax.jsf",
	   data: dataa,
	   success:  function(result) {
		   if (result != null && result == "error") {
			   location.reload();
		   }
   		}
	});
}

function identifyCheckbox(id, checkbox) {
	var item = document.getElementById("regSingleQuestionnaireForm:question_" + id);
	var items = item.getElementsByTagName("input");
	if (items[items.length - 1] == checkbox) {
		document.getElementById("regSingleQuestionnaireForm:other_text").value="";
	}	
}

function showNotNowMessage() {
	var div = document.getElementById("not_now_message");
	div.style.display="block";
}

function checkOther(el, otherEl, otherValue){
	if(el.value == otherValue){
		$("#"+otherEl).css("display", "block");
	}else{
		$("#"+otherEl).css("display", "none");
		$("#"+otherEl).find("input").val("");
	}
}

function validate(checkOnly){
	var checkboxResult = true;
	var selectResult = true;
	
	for(var i = 0; i < checkboxIDs.length; i++){
		change_checkbox(document.getElementById(checkboxIDs[i].id), false);
		if(document.getElementById("regSingleQuestionnaireForm:" + checkboxIDs[i].id).value == ""){
			$(document.getElementById("regSingleQuestionnaireForm:" + checkboxIDs[i].id).parentNode).find("i.checkbox").addClass("error");
		}
	}
	if(document.getElementById("regSingleQuestionnaireForm:question_92").value == ""){
		checkboxResult = false;
	}
	
	
	if(!checkboxResult){
		document.getElementById("error_must_accept").style.display = "block";
	}else{
		document.getElementById("error_must_accept").style.display = "none";
	}
	
	var item;
	for(var i = 7; i < (questionsIdArr.length - 1); i++) {
		item = document.getElementById("regSingleQuestionnaireForm:question_" + questionsIdArr[i][0]);
		if (item.value==null || item.value==undefined || item.value=="") {
			$(item.parentNode).addClass("error");
			$(item.parentNode).on("change", function(event){
				$(this).removeClass("error");
				validate(true);
			});
			selectResult = false;
		}else{
			$(item.parentNode).removeClass("error");
		}
	}
	
	if(!selectResult){
		document.getElementById("error_mandatory").style.display = "block";
	}else{
		document.getElementById("error_mandatory").style.display = "none";
	}
	
	if (checkOnly) {
		return false;
	} else {
		return (checkboxResult && selectResult);
	}
}

function change_checkbox(el, update){
	for(var i = 0; i < checkboxIDs.length; i++){
		if(el.id == checkboxIDs[i].id){
			if(el.checked){
				document.getElementById("regSingleQuestionnaireForm:"+el.id).value = checkboxIDs[i].checkedValue;
				$(el.parentNode).find("i.checkbox").removeClass("error");
			}else{
				if (el.id == 'question_93') {
					document.getElementById("regSingleQuestionnaireForm:"+el.id).value = checkboxIDs[i].unCheckedValue;
				} else {
					document.getElementById("regSingleQuestionnaireForm:"+el.id).value = "";
				}
			}
		}
	}
	if(update){
		updateSingleQuestValues();
	}
}

function next_step(step, checkOnly){
	if(step == 1){
		var item;
		var canProceed = true;
		for(var i = 0; i < 7; i++) {
			item = document.getElementById("regSingleQuestionnaireForm:question_" + questionsIdArr[i][0]);
			if (item.value==null || item.value==undefined || item.value=="") {
				canProceed = false;
				$(item.parentNode).addClass("error");
				$(item.parentNode).on("change", function(event){
					$(this).removeClass("error");
					next_step(1, true);
				});
				//break;
			}
		}
		if(canProceed){
			document.getElementById("error_mandatory").style.display = "none";
			if (!checkOnly) {
				document.getElementById("single_questionnaire_step_1").style.display = "none";
				document.getElementById("single_questionnaire_step_2").style.display = "block";
			}
		}else{
			document.getElementById("error_mandatory").style.display = "block";
		}
	}
	return false;
}

$(document).ready(function(){
	$(".errorMsg").each(function(index, el){
		if($(el).html() != ""){
			if(document.getElementById($(el).attr("id").replace("error", "question"))){
				$(document.getElementById($(el).attr("id").replace("error", "question")).parentNode).addClass("error");
				document.getElementById($(el).attr("id").replace("error", "question")).onchange = function(event){
					$(document.getElementById($(el).attr("id").replace("error", "question")).parentNode).removeClass("error");
					$(el).css("display", "none");
				}
			}
		}
	});
});