aoApp.controller('AoController', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile) {
	$rootScope.topIndex = false;
	$rootScope.showDocumentsPopupType = '';
	
	replaceParamsGT['${complaint_form}'] = replaceParamsGT['${complaint_form}'].replace('{locale}', skinMap[skinMap[settings.skinId].skinIdTexts].locale);
	
	$scope.setDocumentsPopupType = function(type) {
		$rootScope.showDocumentsPopupType = type;
		$rootScope.$apply();
	}
	$rootScope.showQuestionnairePopup = false;
	$scope.setQuestionnairePopupType = function(show) {
		$rootScope.showQuestionnairePopup = show;
		if (!show) {
			g('popUpAORegulationOverlay').style.display = "none";
		}
		$rootScope.$apply();
	}
	if (!settings.isLive) {
		$("body").attr('data-isdead','true');
	}

	chartOnload();

	$rootScope.ready = false;//need to set ready when we have a user
	$scope.markets = [];
	function waitForIt() {
		if (isUndefined($rootScope.$state.current.needLogin)) {
			setTimeout(function(){waitForIt()},50);
		} else {
			// $scope.selectSkin();
		}
	}
	// waitForIt();
	
	//stuf that should come from getUser or something
	$rootScope.user = {
		defaultAmount: parseFloat(defaultInvest),
		predefValues: [],
		showCancelInvestment: showCnacelInv
	}
	$rootScope.zeroAmount = formatAmount(0, 1);
	$rootScope.serverOffset = serverTimeOffset;
	$rootScope.skinSelektor = [];
	
	$rootScope.user.defaultAmountDsp = formatAmount($rootScope.user.defaultAmount, 0);
	if (!isUndefined(predefValues)) {
		for (var i = 0; i < predefValues.length; i++) {
			$rootScope.user.predefValues.push({
				predefValue: predefValues[i].predefValue,
				predefValueDsp: formatAmount(predefValues[i].predefValue, 0)
			});
		}
	}
	$rootScope.UTCOffset = new Date().getTimezoneOffset()*60*1000*(-1);

	//get list of numbers from one number to another
	$scope.Range = function(start, end) {
		var result = [];
		if (start < end) {
			for (var i = start; i <= end; i++) {
				if (i < 10) {
					result.push('0' + i);
				}
				else {
					result.push(i);
				}
			}
		} else {
			for (var i = start; i >= end; i--) {
				if (i < 10) {
					result.push('0' + i);
				}
				else {
					result.push(i);
				}
			}
		}
		return result;
	};

	//get translation from key
	$rootScope.getMsgs = function(key, params) {
		if (typeof key != 'object') {
			if ($scope.msgs.hasOwnProperty(key)) {
				if (typeof params != 'undefined') {
					if (!isUndefined(params._noTrust) && params._noTrust) {
						return $scope.msgsParam($scope.msgs[key], params);
					} else {
						return $sce.trustAsHtml($scope.msgsParam($scope.msgs[key], params));
					}
					// return $sce.parseAsResourceUrl($scope.msgsParam($scope.msgs[key], params));
					// return $scope.msgsParam($scope.msgs[key], params);
				} else {
					return $scope.msgs[key];
				}
			} else {
				return "?msgs[" + key + "]?";
			}
		} else {
			if ($scope.msgs.hasOwnProperty(key[0])) {
				if ($scope.msgs[key[0]].hasOwnProperty(key[1])) {
					if (typeof params != 'undefined') {
						return $sce.trustAsHtml($scope.msgsParam($scope.msgs[key[0]][key[1]], params));
					} else {
						return $scope.msgs[key[0]][key[1]];
					}
				} else {
					return "?msgs[" + key[0] + '.' + key[1] + "]?";
				}
			} else {
				return "?msgs[" + key[0] + "]?";
			}
		
		}
	}
	
	//get msg with param
	$scope.msgsParam = function(msg, params){//TODO: it's called multiple times for no good reason
		for (var key in params) {
			if (params.hasOwnProperty(key)) {
				if(params[key] && params[key].replace){
					msg = msg.replace(new RegExp('{' + key + '}', 'g'), params[key].replace(new RegExp('\\$', 'g'), '$$$$')); //Fix for IE and $0, $1, etc. problems with RegEx
				}else{
					msg = msg.replace(new RegExp('{' + key + '}', 'g'), params[key]);
				}
			}
		}
		return msg;
	}

	$scope.checkIfLogged = function() {
		return settings.loggedIn;
	}

	$scope.checkNoIndex = function() {
		if (typeof $rootScope.$state.current.robotIndex == 'undefined') {
			return true;
		} else {
			return $rootScope.$state.current.robotIndex;
		}
	}

	$scope.formatDateNow = function(date) {
		var rtn = '';
		if (parseInt(date) == date) {
			date = new Date(parseInt(date));
		} else {
			date = new Date(date);
		}
		
		var date_h = date.getHours();
		date_h = (date_h < 10) ? '0' + date_h : date_h;
		
		var date_m = date.getMinutes();
		date_m = (date_m < 10) ? '0' + date_m : date_m;
		
		var today = new Date();
		var yesterday = new Date(today);
		yesterday.setDate(today.getDate() - 1);
		
		if (date.getUTCFullYear() == today.getUTCFullYear() 
			&& date.getUTCMonth() == today.getUTCMonth()
			&& date.getUTCDate() == today.getUTCDate()) {
			rtn = $rootScope.getMsgs('time_today', {time: date_h + ":" + date_m});
		} else if (yesterday.getUTCFullYear() == yesterday.getUTCFullYear() 
			&& date.getUTCMonth() == yesterday.getUTCMonth()
			&& date.getUTCDate() == yesterday.getUTCDate()) {
			rtn = $rootScope.getMsgs('yesterday_today', {time: date_h + ":" + date_m});
		} else {
			var tmp = '';
			if (date.getUTCFullYear() != today.getUTCFullYear()) {
				tmp += date.getYear() + '.';
			}
			var month = date.getMonth()+1;
			tmp += (month < 10) ? '0' + month : month;
			var day = date.getDate();
			tmp += "." + ((day < 10) ? '0' + day : day);
			tmp += " " + date_h + ":" + date_m;
			rtn = tmp;
		}
		return rtn;
	}


	$scope.msgs = msgsAo;//{}
	var getMsgsJson_first = true;
	//get all translations (bundle)
	$scope.getMsgsJson = function() {
		$scope.initRequestStatus.getMsgsJson = false;
		//skinMap[skinMap[settings.skinId].skinIdTexts].locale - seems legit :D
		var msgsUrl = settings.msgsPath + settings.msgsFileName + skinMap[skinMap[settings.skinId].skinIdTexts].locale + ((getMsgsJson_first)?"":settings.msgsEUsufix) + settings.msgsExtension;
		$http.get(msgsUrl)
			.success(function(data, status) {
				if (typeof data == 'object') {
					if (getMsgsJson_first) {
						$scope.msgs = data;
					} else {
						$scope.msgs = jsonConcat($scope.msgs, data);
						if (!settings.loggedIn) {
							$scope.initRequestStatus.getMsgsJson = true;
						}
					}
					if (settings.isRegulated && getMsgsJson_first) {
						getMsgsJson_first = false;
						$scope.getMsgsJson();
					} else {
						$scope.initRequestStatus.getMsgsJson = true;
					}
					oldTexts.bundle_msg_today = $scope.msgs['investment.time.today'];//TO DO remove
					
					if (!isUndefined($rootScope.$state.current.metadataKey)) {
						$rootScope.metadataTitle = $rootScope.$state.current.metadataKey;
					} else {
						$rootScope.metadataTitle = 'index';
					}
				}
			})
			.error(function( data, status ) {
				$rootScope.handleNetworkError(data);
			});
	}

	//get all markets
	$scope.binaryMarketsCount = 0;
	$scope.getMarketGroups = function() {
		// if (isUndefined($scope.markets)) {
			$scope.initRequestStatus.getMarketGroups = false;
			$http.post(settings.jsonLinkNew + 'getMarketGroups', getMethodRequest())
				.then(function(data) {
					data = data.data;
					$scope.marketGroups = data;
					$scope.markets = [];
					$scope.marketsObj = [];
					$rootScope.dynamicsMarkets = [];
					$rootScope.binaryMarkets = [];
					for (var i = 0; i < $scope.marketGroups.groups.length; i++) {
						$scope.marketGroups.groups[i].index = i;
						$scope.marketGroups.groups[i].filterInactive = {active: true};
						$scope.marketGroups.groups[i].showInactiveStates = false;
						for (var j = 0; j < $scope.marketGroups.groups[i].markets.length; j++) {
							if ($scope.marketGroups.groups[i].id != 100 && $scope.marketGroups.groups[i].id != 99) {
								$scope.marketGroups.groups[i].markets[j].binaryIndex = $scope.binaryMarketsCount;
								$scope.binaryMarketsCount++;
							}
							$scope.marketGroups.groups[i].markets[j].index = j;
							$scope.markets[$scope.marketGroups.groups[i].markets[j].id] = $scope.marketGroups.groups[i].markets[j];
							//$scope.marketsObj.push($scope.marketGroups.groups[i].markets[j]);
							
							if ($scope.marketGroups.groups[i].id == 100) {
								$rootScope.dynamicsMarkets.push($scope.marketGroups.groups[i].markets[j].id);
							} else if ($scope.marketGroups.groups[i].id < 99) {
								$rootScope.binaryMarkets.push($scope.marketGroups.groups[i].markets[j]);
							}
						}
					}

					$scope.initRequestStatus.getMarketGroups = true;
				})
				.catch(function(data) {
					data = data.data;
					$rootScope.handleNetworkError(data);
				});
		// }
	}
	
	
	$scope.checkInitStatus = function() {
		for (var key in $scope.initRequestStatus) {
			if (!$scope.initRequestStatus[key]) {
				setTimeout(function() {
					$scope.checkInitStatus();
				}, 50);
				return;
			}
		}
		$rootScope.ready = $scope.ready = true;
		$rootScope.$apply();
	}
	$scope.requestsStatus = {}//0 - requested, 1 - responded error 2 - success
	$scope.initRequestStatus = {}
	$scope.initRequests = function() {
		$rootScope.ready = false;
		// $scope.getMsgsJson();
		$scope.getMarketGroups();
		// startClock();
		
		$scope.checkInitStatus();
	}
	
	$scope.initRequests();//get all needed resources
	
	$scope.checkError = function(_form, field, error) {
		return (_form[field]) && ((_form.$submitted && _form[field].$error[error]) || (_form[field].$touched && _form[field].$dirty && _form[field].$error[error]));
	}
	
	$scope.getMonths = function() {
		if (isUndefined($scope.monthsDsp)) {
			$scope.monthsDsp = [];
			for (var i = 1; i <= 12; i++) {
				$scope.monthsDsp.push({
					id: (i < 10) ? '0' + i : i,
					displayName: $rootScope.getMsgs('month-' + i)
				});
			}
		}
		return $scope.monthsDsp;
	}

	$scope.setFocus = function(id) {
		var counter = 20;
		function doIt() {
			var el = g(id);
			if (isUndefined(el)) {
				if (counter > 0) {
					setTimeout(function() {
						doIt();
					}, 200);
					counter--;
				}
			} else {
				el.focus();
			}
		}
		doIt();
	}

	$rootScope.handleNetworkError = function(data) {
		// alert('Network error (aka 404, 500 etc)');
		logIt({'type':1,'msg':data});
	}
	window.error_stack = [];
	$rootScope.handleErrors = function(data, name) {//returns true on error
		$scope.requestsStatus[data.serviceName] = 1;
		if (data == '') {
			logIt({'type':3,'msg':name});
			logIt({'type':3,'msg':data});
			// alert('Response is empty string. Check eclipse console for exeptions.');
			return true;
		} else if (data.errorCode == null) {
			logIt({'type':3,'msg':name});
			logIt({'type':3,'msg':data});
			alert('Error code is null. Check eclipse console for exeptions. ');
			return true;
		} else if (data.errorCode > errorCodeMap.success) {//0
			var defMsg = $rootScope.getMsgs('error-' + data.errorCode);
			logIt({'type':3,'msg':name + " (" + defMsg + ")"});
			logIt({'type':3,'msg':data});
			var globalErrorField;
			if (typeof data.globalErrorField != 'undefined') {
				globalErrorField = g(data.globalErrorField);
			} else if (g('globalErrorField') != null) {
				globalErrorField = g('globalErrorField');
			} else {
				globalErrorField = document.createElement('span');
				globalErrorField.id = 'globalErrorField';
				document.body.appendChild(globalErrorField);
			}
			globalErrorField.innerHTML = '';
			resetErrorMsgs(data);
			if (data.errorCode == errorCodeMap.regulation_suspended) { //susspended user 301
				try {
					// $rootScope.closeCopyOpPopup();
					// $rootScope.openCopyOpPopup({type: 'info', body: 'error.investment.user.suspended', header: 'info-popup-header'});
				} catch(e) {}
			} else if (data.errorCode == errorCodeMap.withdrawal_close) { //weekend withdraw 110
				return false;
			} else if (data.errorCode == errorCodeMap.regulation_missing_questionnaire) { //regulation missing questionnaire 300
				try {
					// $rootScope.closeCopyOpPopup();
					// $rootScope.openQuestionary();
				} catch(e) {}
			} else if (data.errorCode == errorCodeMap.regulation_suspended_documents) { // 302
				try {
					// $rootScope.closeCopyOpPopup();
					// $rootScope.stateMachine(data);
				} catch(e) {}
			} else if (data.errorCode == errorCodeMap.regulation_user_restricted) { // 304
				try {
					// $rootScope.closeCopyOpPopup();
					// $rootScope.openCopyOpPopup({type: 'regulationStatus', errorCode: data.errorCode, txtStatus: 'restricted'});
				} catch(e) {}
			} else if (data.errorCode == errorCodeMap.reg_suspended_quest_incorrect) { // 305
				try {
					// $rootScope.closeCopyOpPopup();
					// $rootScope.openCopyOpPopup({type: 'regulationStatus', errorCode: data.errorCode, txtStatus: 'blocked'});
				} catch(e) {}
			} else if (data.errorCode == errorCodeMap.regulation_user_pep_prohibited) { // 307
				try {
					// $rootScope.closeCopyOpPopup();
					// $rootScope.openCopyOpPopup({type: 'regulationStatus', errorCode: data.errorCode, txtStatus: 'blocked-pep'});
				} catch(e) {}
			} else if (data.errorCode == errorCodeMap.regulation_user_is_treshold_block) { // 308
				try {
					// $rootScope.closeCopyOpPopup();
					// $rootScope.openCopyOpPopup({type: 'regulationStatus', errorCode: data.errorCode, txtStatus: 'restricted-loss'});
				} catch(e) {}
			} else if (data.errorCode == errorCodeMap.user_not_regulated) { //not regulated 303
				alert('User not regulated');
			} else if (data.errorCode == errorCodeMap.session_expired) { //session expired 419
				if (isUndefined(data.loginPage)) {
					try {
						// $rootScope.changeHeader(false);
						// $rootScope.$state.go('ln.login', {ln: settings.skinLanguage});
						// $rootScope.initSettings();
					} catch(e) {}
				}
			} else if (data.errorCode == errorCodeMap.not_accepted_terms) { //terms not accepted 701
				// $rootScope.$state.go('ln.anyoption', {ln: settings.skinLanguage});
				return false;
			} else if (data.errorCode == errorCodeMap.user_removed) { //removed user 800
				// $rootScope.openCopyOpPopup({
					// type: 'info', 
					// header: 'error',
					// body: 'error',
					// onclose: function(){angular.element('html').scope().$state.go('ln.home.a', {ln: settings.skinLanguage});}
				// });
				if (!data.loginNotCheck) {
					// $rootScope.loginCheck();
				}
				return false;
			} else if (data.errorCode == errorCodeMap.copyop_max_copiers) { //fully copyed 1001
				// $rootScope.openCopyOpPopup({type: 'fullyCopyied'});
			} else {
				if (data.userMessages != null) {
					for (var i = 0; i < data.userMessages.length; i++) {
						if (data.userMessages[i].field == '' || data.userMessages[i].field == null) {
							globalErrorField.innerHTML += "<span>" + data.userMessages[i].message + "</span>";
						} else {
							var input = g(data.userMessages[i].field);
							if (input != null) {
								input.className += " error";
							}
							var error_el_id = data.userMessages[i].field + '-error';
							error_stack.push(data.userMessages[i].field);
							var err_place = g(error_el_id);
							if (err_place != null) {
								err_place.innerHTML = data.userMessages[i].message;
							} else {
								var field_place = g(data.userMessages[i].field + '-container');
								if (field_place != null) {
									var el = document.createElement('span');
									el.id = error_el_id;
									el.className = 'error-message icon';
									el.innerHTML = data.userMessages[i].message;
									field_place.appendChild(el);
								} else {
									globalErrorField.innerHTML += "<span>" + data.userMessages[i].message + "</span>";
								}
							}
						}
					}
				} else {
					logIt({'type':3,'msg':'unhandled errorCode: ' + data.errorCode});
					globalErrorField.innerHTML += "<span>" + defMsg + "</span>";
				}
			}
			return true;
		} else {
			logIt({'type':1,'msg':data});
			if (!data.loginNotCheck) {
				// $rootScope.loginCheck();
			}
			return false;
		}
	}
	
	$rootScope.connectToLS = function(params) {//['name':'','subscriptionType':subscriptionType,'group':group,'schema':schema,'dataAdapterName':dataAdapterName]
		logIt({'type':1,'msg':'connectToLS: '+params.name});
		var obj = new Subscription(params.subscriptionType, params.group, params.schema);
		obj.setDataAdapter(params.dataAdapterName);
		obj.setRequestedSnapshot("yes");
		obj.setRequestedMaxFrequency(1.0);
		obj.addListener({
			  onSubscription: function() {
				  logIt({'type':1,'msg':'SUBSCRIBED: '+params.name});
			  },
			  onUnsubscription: function() {
				  logIt({'type':1,'msg':'UNSUBSCRIBED: '+params.name});
			  },
			  onItemUpdate: function (updateObject) { 
				logIt({'type':1,'msg':'ItemUpdate: '+params.name});
				params.callBack(updateObject);
			  }
			});
		$rootScope.lsClient.subscribe(obj);
		return obj;
	}
	
	$rootScope.connectToLS_global = function() {
		if (isUndefined($rootScope.lsClient)) {
			$rootScope.lsClient = configureLsConnection();
			$rootScope.lsClient.connect();
		}
	}
	
	$scope.goToByScroll = function(id, adjust) {
		goToByScroll(id, adjust);
	}

	$rootScope.showWithdrawUserDetailsPopup = false;
	$scope.showWithdrawUserDetails = function() {
		var WithdrawUserDetailsMethodRequest = jsonClone(getMethodRequest(), {
			withdrawUserDetails: {
				userId: userInfo.id
			}
		});
			
		$http.post(settings.commonServiceLink  + '/WithdrawUserDetailsServices/showWithdrawUserDetails', WithdrawUserDetailsMethodRequest)
			.then(function(data) {
				data = data.data;
				if (!$rootScope.handleErrors(data, 'showWithdrawUserDetails')) {
					$rootScope.showWithdrawUserDetailsPopup = data.showPopup;
				}
			})
			.catch(function(data) {
				handleNetworkError(data);
			});
	}

	//File init
	$scope.usingFlash = FileAPI && FileAPI.upload != null;
	$scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
	$scope.fileTooBig = false;
}]);

aoApp.controller('skinSelectorController', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;
	
	$scope.getVisibleSkinsWeb = function() {
		if ($rootScope.skinSelektor.length == 0) {
			var VisibleSkinMethodRequest = jsonClone(getMethodRequest(), {
				uri: requestURINoContextPath
			});
			$http.post(settings.jsonLinkNew + 'getVisibleSkinsWeb', VisibleSkinMethodRequest)
				.then(function(data) {
					data = data.data;
					if (!$rootScope.handleErrors(data, 'getVisibleSkinsWeb')) {
						$rootScope.skinSelektor = data.visibleSkins;
					}
				})
				.catch(function(data) {
					data = data.data;
					$rootScope.handleNetworkError(data);
				})
		}
	}
}]);