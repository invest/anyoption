aoApp.controller('regulationDocuments', ['$rootScope', '$scope', '$http', '$timeout', '$upload', function($rootScope, $scope, $http, $timeout, $upload) {
	$scope.ready = true;
	$scope.currentDocType = 0;
	$scope.popupState = 0;//0 default, 1 blocked
	$scope.selects = {
		creditCard: {},
		proofOfId: {}
	}
	
	$scope.proofOfIdClicked = false;
	$scope.setProofOfIdClicked = function(val){
		$scope.proofOfIdClicked = val;
	}
	$scope.checkProofOfIdClicked = function(){
		return ($scope.proofOfIdClicked && $scope.selects.proofOfId.id == '');
	}
	
	function waitForIt() {
		if (!$rootScope.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			$scope.init();
		}
	}

	$scope.uploadComplateMsgs = false;
	
	$scope.passParams = function(params) {
		$scope.popUpType = params.type;
		$scope.config = params;
		$scope.popupState = params.popupState;
	}

	$scope.init = function() {
		$scope.creditCards = [];
		$scope.proofOfId = [
			{id: '', docTitle: $rootScope.getMsgs('choose-type')}
		];
		
		$scope.selects.proofOfId = $scope.proofOfId[0];
		
		$scope.restOfDocs = [];
		
		var UserDocumentsMethodRequest = jsonClone(getUserMethodRequest(), {
			userId: userInfo.id
		})
		
		$http.post(settings.commonServiceLink + '/UserDocumentsServices/getUserDocuments', UserDocumentsMethodRequest)
			.then(function(data) {
				data = data.data;
				if (!$rootScope.handleErrors(data, 'getUserDocuments')) {
					$scope.userDocuments = data.userDocuments;
					$scope.documentUploadResult();
				}
			})
			.catch(function(data) {
				data = data.data;
				$scope.loading = false;
				handleNetworkError(data);
			})
	}

	$scope.documentUploadResult = function() {
		//document statuses
		$scope.userDocuments = $scope.userDocuments.map(function(current) {
			if (!current.hideField) {
				current.docStatus = $scope.switchStatus(current);
				return current;
			}
		});
		
		var proofOfIdList = [fileType.user_id_copy, fileType.passport];
		var creditCardsList = [fileType.cc_copy_front, fileType.cc_copy_back];
		for (var i = 0; i < $scope.userDocuments.length; i++) {
			switch($scope.userDocuments[i].fileTypeId) {
				case fileType.user_id_copy: $scope.userDocuments[i].docTitle = $rootScope.getMsgs('id');break;
				case fileType.passport: $scope.userDocuments[i].docTitle = $rootScope.getMsgs('passport');break;
				case fileType.utility_bill: $scope.userDocuments[i].docTitle = $rootScope.getMsgs('utility-bill');break;
			}
			if (proofOfIdList.indexOf($scope.userDocuments[i].fileTypeId) > -1) {
				$scope.proofOfId.push($scope.userDocuments[i]);
				if ($scope.userDocuments[i].statusMsgId > 1 && !$scope.selects.proofOfId.isApproved) {
					$scope.selects.proofOfId = $scope.proofOfId[$scope.proofOfId.length - 1];
				}
			} else if (creditCardsList.indexOf($scope.userDocuments[i].fileTypeId) > -1) {
				var index = searchJsonKeyInArray($scope.creditCards, 'ccId', $scope.userDocuments[i].ccId);
				if (index == -1) {
					$scope.creditCards.push({
						ccId: $scope.userDocuments[i].ccId,
						ccName: $scope.userDocuments[i].ccName,
						ccType: $scope.userDocuments[i].ccType
					});
					index = $scope.creditCards.length-1;
				}
				
				if ($scope.userDocuments[i].fileTypeId == fileType.cc_copy_front) {
					$scope.creditCards[index].front = $scope.userDocuments[i];
				} else if ($scope.userDocuments[i].fileTypeId == fileType.cc_copy_back) {
					$scope.creditCards[index].back = $scope.userDocuments[i];
				}
			} else {
				$scope.restOfDocs.push($scope.userDocuments[i]);
			}
		}

		if ($scope.creditCards.length > 0) {
			$scope.selects.creditCard = $scope.creditCards[0];
		}
	}
	
	$scope.switchStatus = function(docData) {
		var rtn = {
			fileName: (!isUndefined(docData.fileNameForClient)) ? docData.fileNameForClient : $rootScope.getMsgs('no-file'),
			isLock: docData.isLock
		}
		
		switch (docData.statusMsgId) {
			case fileStatus.not_selected: 
				rtn.className = 'error-message icon';
				rtn.txt = $rootScope.getMsgs('select-type');
				break;
			case fileStatus.requested: 
				rtn.className = 'error-message icon';
				rtn.txt = $rootScope.getMsgs('missing');
				break;
			case fileStatus.in_progress: 
				rtn.className = 'info-message';
				rtn.txt = $rootScope.getMsgs('waiting-approval');
				break;
			case fileStatus.done: 
				rtn.className = 'ok-message icon';
				rtn.txt = $rootScope.getMsgs('approved');
				break;
			case fileStatus.invalid: 
				rtn.className = 'error-message icon';
				rtn.txt = $rootScope.getMsgs('invalid');
				break;
			case fileStatus.not_needed: 
				rtn.className = '';
				rtn.txt = '';
				break;
		}
		
		return rtn;
	}
	
	$scope.usingFlash = FileAPI && FileAPI.upload != null;
	$scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
	$scope.fileTooBig = false;
	
	$scope.abort = function(index) {
		$scope.upload[index].abort(); 
		$scope.upload[index] = null;
	};
	
	$scope.onFileSelect = function($files, docType, doc) {
		$scope.currentDocType = docType;
		$scope.ccId = 0;
		$scope.selectedDoc = doc;
		$scope.selectedDoc.loading = true;
		if (docType == fileType.cc_copy_front || docType == fileType.cc_copy_back) {
			$scope.ccId = $scope.selects.creditCard.ccId;
		}
		//Check if the file is too big, alert and return in case it is
		var hasLargerFile = false;
		$scope.fileTooBig = false;
		for ( var i = 0; i < $files.length; i++) {
			if($files[i].size / 1000000  >= settings.documentSizeLimit){
				hasLargerFile = true;
			}
		}
		if(hasLargerFile){
			//alert($rootScope.getMsgs('file-too-big'));
			$scope.fileTooBig = true;
			$timeout(function(){$scope.fileTooBig = false;}, 3000);
			return;
		}
		
		// closeElement({id:'popUp-avatar'});
		$scope.selectedFiles = [];
		$scope.progress = [];
		if ($scope.upload && $scope.upload.length > 0) {
			for (var i = 0; i < $scope.upload.length; i++) {
				if ($scope.upload[i] != null) {
					$scope.upload[i].abort();
				}
			}
		}
		$scope.upload = [];
		$scope.uploadResult = [];
		$scope.selectedFiles = $files;
		$scope.dataUrls = [];
		for ( var i = 0; i < $files.length; i++) {
			var $file = $files[i];
			if ($scope.fileReaderSupported && $file.type.indexOf('image') > -1) {
				var fileReader = new FileReader();
				fileReader.readAsDataURL($files[i]);
				var loadFile = function(fileReader, index) {
					fileReader.onload = function(e) {
						$timeout(function() {
							$scope.dataUrls[index] = e.target.result;
							// if (index == 0) {
								// $rootScope.uploadedImage = $scope.dataUrls[index];
							// }
						});
					}
				}(fileReader, i);
			}
			$scope.progress[i] = -1;
			$scope.start(i);
		}
	};
	
	$scope.start = function(index) {
		if ($scope.selectedFiles[index].size / 1000000  < settings.documentSizeLimit) {
			$scope.progress[index] = 0;
			$scope.errorMsg = null;
			//$upload.upload()
			 var params = '?';
			 params += 'fileType=' + $scope.currentDocType;
			 params += '&fileName=' + $scope.selectedFiles[index].name;
			 params += '&ccId=' + $scope.ccId;
			 params += '&writerId=' + settings.writerId;
			 params = encodeURI(params);
			$scope.upload[index] = $upload.upload({
				url: context_path + '/UploadDocumentsService' + params,
				method: 'POST',
				file: $scope.selectedFiles[index],
				fileFormDataName: 'myFile',
				data: {
					fileType: $scope.currentDocType,
					fileName: $scope.selectedFiles[index].name,
					ccId: $scope.ccId,
					writerId: settings.writerId
				}
			});
			$scope.upload[index].then(function(response) {
				$timeout(function() {
					$scope.uploadResult.push(response.data);
					$scope.uploadComplate(response.data);
				});
			}, function(response) {
				if (response.status > 0) $scope.$parent.errorMsg = $scope.errorMsg = response.status + ': ' + response.data;
				$scope.selectedDoc.loading = false;
			}, function(evt) {
				// Math.min is to fix IE which reports 200% sometimes
				$scope.$parent.progress = $scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
				// $scope.selectedDoc.loading = false;
			});
			$scope.upload[index].xhr(function(xhr){
	//				xhr.upload.addEventListener('abort', function() {console.log('abort complete')}, false);
			});
		} else {
			alert($rootScope.getMsgs('file-too-big'));
		}
	};
	
	$scope.uploadComplate = function(data) {
		$scope.selectedDoc.loading = false;
		if (!$rootScope.handleErrors(data, 'UploadDocumentsService')) {
			var filenameFormated
			if ($scope.selectedFiles[0].name.length <= 13) {
				filenameFormated = $scope.selectedFiles[0].name;
			} else {
				var ext = $scope.selectedFiles[0].name.split('.');
				ext = '...' + ext[ext.length-1];
				filenameFormated = $scope.selectedFiles[0].name.substr(0, 9) + ext;
			}
			
			$scope.selectedDoc.docStatus = $scope.switchStatus({
				fileNameForClient: filenameFormated,
				statusMsgId: fileStatus.in_progress,
				isLock: $scope.selectedDoc.isLock
			});
			
			$scope.uploadComplateMsgs = true;
			$timeout(function() {
				$scope.uploadComplateMsgs = false;
			},3000);
		}
	}
	
	if (localStorage) {
		$scope.s3url = localStorage.getItem("s3url");
		$scope.AWSAccessKeyId = localStorage.getItem("AWSAccessKeyId");
		$scope.acl = localStorage.getItem("acl");
		$scope.success_action_redirect = localStorage.getItem("success_action_redirect");
		$scope.policy = localStorage.getItem("policy");
		$scope.signature = localStorage.getItem("signature");
	}
	$scope.success_action_redirect = $scope.success_action_redirect || window.location.protocol + "//" + window.location.host;
	$scope.jsonPolicy = $scope.jsonPolicy || '{\n  "expiration": "2020-01-01T00:00:00Z",\n  "conditions": [\n    {"bucket": "angular-file-upload"},\n    ["starts-with", "$key", ""],\n    {"acl": "private"},\n    ["starts-with", "$Content-Type", ""],\n    ["starts-with", "$filename", ""],\n    ["content-length-range", 0, 524288000]\n  ]\n}';
	$scope.acl = $scope.acl || 'private';
	
	
	waitForIt();
}]);