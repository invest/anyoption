aoApp.controller('BinaryBankController', ['$rootScope', '$scope', '$http', '$timeout', '$interval', function($rootScope, $scope, $http, $timeout, $interval) {
	$scope.binaryBank = [];
	function waitForIt() {
		if (!$rootScope.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			init();
		}
	}
	
	function init() {
		$scope.binaryMarketsBankCols = Math.round($scope.binaryMarketsCount / 7);
		$scope.binaryBank.push({markets:[]});
		for (var i = 0; i < $scope.marketGroups.groups.length; i++) {
			if ($scope.marketGroups.groups[i].id != 100 && $scope.marketGroups.groups[i].id != 99) {
				if ($scope.binaryBank[$scope.binaryBank.length-1].markets.length + 3 > $scope.binaryMarketsBankCols) {
					$scope.binaryBank.push({markets:[]});
				}
				
				$scope.binaryBank[$scope.binaryBank.length-1].markets.push({
					title: $scope.marketGroups.groups[i].name,
					type: 0
				});

				for (var j = 0; j < $scope.marketGroups.groups[i].markets.length; j++) {
					if ($scope.binaryBank[$scope.binaryBank.length-1].markets.length > $scope.binaryMarketsBankCols) {
						$scope.binaryBank.push({markets:[]});
					}
					$scope.binaryBank[$scope.binaryBank.length-1].markets.push({
						title: $scope.marketGroups.groups[i].markets[j].displayName,
						newMarket: $scope.marketGroups.groups[i].markets[j].newMarket,
						twentyFourSeven: $scope.marketGroups.groups[i].markets[j].twentyFourSeven,
						id: $scope.marketGroups.groups[i].markets[j].id,
						type: 1
					});
				}
			}
		}
		$scope.$apply();
	}
	$scope.loadMarketInBox = function(marketId, box) {
		loadMarketInBox(marketId, box);
	}

	waitForIt();
}]);

aoApp.controller('AssetIndexController', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.cols = [];
	function waitForIt() {
		if (!$rootScope.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			$scope.getAssetIndexMarketsPerSkin();
		}
	}
	
	$scope.assetIndexMarkets = [];//type 1: grpup, type 2: country, type 3: market
	$scope.lastConfig = {
		groupName: '',
		displayGroupNameKey: '',
		opportunityTypeId: ''
	};
	
	$scope.getAssetIndexMarketsPerSkin = function() {
		$http.post(settings.commonServiceLink + '/AssetIndexServices/getAssetIndexMarketsPerSkin', getMethodRequest())
			.then(function(data) {
				data = data.data;
				$scope.assetIndexGeneral = {
					tooltipData: data.tooltipData,
					tooltipDataUnderTable: data.tooltipDataUnderTable,
					assetIndexFormulasBySkin: data.assetIndexFormulasBySkin
				}
				
				var i = 0;
				$.each(data.marketList, function(k, v) {
					if (v.opportunityTypeId == 1) {
						if ($scope.lastConfig.groupName != v.market.groupName) {
							if (!isUndefined(v.market.groupName)) {
								$scope.assetIndexMarkets.push({
									title: $rootScope.getMsgs(v.market.groupName),
									type: 1
								});
								$scope.lastConfig.groupName = v.market.groupName;
							}
						}
						
						if ($scope.lastConfig.displayGroupNameKey != v.market.displayGroupNameKey) {
							if (!isUndefined(v.market.displayGroupNameKey)) {
								$scope.assetIndexMarkets.push({
									title: $rootScope.getMsgs(v.market.displayGroupNameKey),
									type: 2
								});
								$scope.lastConfig.displayGroupNameKey = v.market.displayGroupNameKey;
							}
						}
					} else if (v.opportunityTypeId != 6) {
						if ($scope.lastConfig.opportunityTypeId != v.opportunityTypeId) {
							$scope.assetIndexMarkets.push({
								title: $rootScope.getMsgs('type-name-' + v.opportunityTypeId),
								type: 1
							});
							$scope.lastConfig.opportunityTypeId = v.opportunityTypeId;
						}
					}
					
					var temp = {
						type: 3,
						opportunityType: v.opportunityTypeId,
						id: v.market.id,
						marketGroupId: v.marketGroupId,
						title: v.market.displayName,
						description: v.marketAssetIndexInfo.marketDescription,
						descriptionPage: v.marketAssetIndexInfo.marketDescriptionPage,
						additionalText: v.marketAssetIndexInfo.additionalText,
						symbol: v.market.feedName,
						tradingTime: '',
						newMarket: v.market.newMarket,
						is24h7: v.marketAssetIndexInfo.is24h7,
						formulas: []
					}

					var startDay = -1;
					var endDay = -1;
					for (var d = 0; d < 7; d++) {
						if (v.marketAssetIndexInfo.tradingDays[d] == 1 && startDay == -1) {
							startDay = d;
						}
						if (v.marketAssetIndexInfo.tradingDays[d] == 1) {
							endDay = d;
						}
					}
					
					var orgStartDate = new Date(v.marketAssetIndexInfo.startTime);
					var startDate = new Date(new Date(orgStartDate.setDate(orgStartDate.getDate() - orgStartDate.getDay() + startDay)).getTime() - (orgStartDate.getTimezoneOffset() * 60 * 1000));//crazy shit
					
					var orgEndDate = new Date(v.marketAssetIndexInfo.endTime);
					var endDate = new Date(new Date(orgEndDate.setDate(orgEndDate.getDate() - orgEndDate.getDay() + endDay)).getTime() - (orgEndDate.getTimezoneOffset() * 60 * 1000));//crazy shit
				
					temp.tradingTime += $rootScope.getMsgs('day-' + startDate.getDay()) + ' - ' + $rootScope.getMsgs('day-' + endDate.getDay());
					temp.tradingTime += ' ' + startDate.getHours() + ':' + (startDate.getMinutes() < 10 ? '0' + startDate.getMinutes() : startDate.getMinutes()) + '-' + endDate.getHours() + ':' + (endDate.getMinutes() < 10 ? '0' + endDate.getMinutes() : endDate.getMinutes());
					
					for (var t = 0; t < v.marketAssetIndexInfo.expiryFormulaCalculations.length; t++) {
						if (t == 0) {
							temp.formulas.push(v.marketAssetIndexInfo.expiryFormulaCalculations[t]);
							temp.formulas[temp.formulas.length - 1].sameNextRowColl1 = false;
							temp.formulas[temp.formulas.length - 1].sameNextRowColl2 = false;
							temp.formulas[temp.formulas.length - 1].formulaIndex = searchJsonKeyInArray(data.assetIndexFormulasBySkin, 'formulaId', v.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryFormulaId);
						} else if (v.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryTypeId == 2) {
							temp.formulas.push(v.marketAssetIndexInfo.expiryFormulaCalculations[t]);
							temp.formulas[temp.formulas.length - 1].formulaIndex = searchJsonKeyInArray(data.assetIndexFormulasBySkin, 'formulaId', v.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryFormulaId);
							
							temp.formulas[temp.formulas.length - 1].sameNextRowColl1 = false;
							temp.formulas[temp.formulas.length - 1].sameNextRowColl2 = false;
							if (v.marketAssetIndexInfo.expiryFormulaCalculations[t].reutersField == v.marketAssetIndexInfo.expiryFormulaCalculations[0].reutersField) {
								temp.formulas[temp.formulas.length - 1].sameNextRowColl1 = true;
								temp.formulas[0].sameNextRowColl1 = true;
							}
							if ($scope.assetIndexGeneral.assetIndexFormulasBySkin[temp.formulas[temp.formulas.length - 1].formulaIndex].formulaTranslation == $scope.assetIndexGeneral.assetIndexFormulasBySkin[temp.formulas[0].formulaIndex].formulaTranslation) {
								temp.formulas[temp.formulas.length - 1].sameNextRowColl2 = true;
								temp.formulas[0].sameNextRowColl2 = true;
							}
						} else if (v.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryTypeId == 3) {
							temp.formulas[temp.formulas.length - 1].expiryTypeId = 3;
						}
					}
					if (v.opportunityTypeId != 6) {
						$scope.assetIndexMarkets.push(temp);
					}
					i++;
				});
				
				
				$scope.AssetIndexCols = Math.round($scope.assetIndexMarkets.length / 2);
				$scope.cols.push({markets:[]});
				
				for (var i = 0; i < $scope.assetIndexMarkets.length; i++) {
					if ($scope.cols[$scope.cols.length - 1].markets.length > $scope.AssetIndexCols) {
						$scope.cols.push({markets:[]});
					}
					
					$scope.cols[$scope.cols.length-1].markets.push($scope.assetIndexMarkets[i]);
				}
				
				if (!isUndefined(window.marketId)) {
					var arrayIndex = searchJsonKeyInArray($scope.assetIndexMarkets, 'id', marketId);
					if (arrayIndex != -1) {
						$scope.selectedMarket = $scope.assetIndexMarkets[arrayIndex];
					}
				}
			})
			.catch(function(data) {
				data = data.data;
				$rootScope.handleNetworkError(data);
			});
	}

	waitForIt();
}]);

aoApp.controller('generalTermsCtr', ['$rootScope', '$scope', '$http', '$timeout', function($rootScope, $scope, $http, $timeout) {
	$scope.TERMS_TYPE_AGREEMENT = 1;
	$scope.TERMS_TYPE_GENERAL_TERMS = 2;
	$scope.TERMS_TYPE_RISK_DISCLOSURE = 3;
	$scope.menuShownBeforeId = 0;
	
	$scope.ready = false;
	$scope.loading = true;
	$scope.termsContent = '';
	// function waitForIt() {
		// if (!$scope.$parent.ready) {
			// $timeout(function(){waitForIt()},50);
		// } else {
			// init();
		// }
	// }
	
	$scope.passParams = function(params){
		$scope.params = params;
		// waitForIt();
		init();
	}
	
	function init() {
		$scope.loading = true;
		var MethodRequest = jsonClone(getUserMethodRequest(), {
			"platformIdFilter": 2,
			"skinIdFilter": skinId,
			"termsType": $scope.params.termsType
		});
		
		$http.post(settings.jsonLinkNew + 'getTermsFiles', MethodRequest)
			.then(function(data) {
				data = data.data;
				if (!$rootScope.handleErrors(data, 'getUserDepositBonusBalance')) {
					data.filesList.sort(function(a, b) {
						return parseInt(a.orderNum) - parseInt(b.orderNum);
					});
					
					$scope.isRegulated = skinRegulated;
					$scope.filesList = data.filesList;
					
					for (var i = 0; i < $scope.filesList.length; i++) {
						$scope.filesList[i].html = multipleReplace(replaceParamsGT, $scope.filesList[i].html);
						var linkTitle = $scope.filesList[i].title;
						if (!linkTitle) {
							$('<div>' + $scope.filesList[i].html + '</div>').find(".general_terms_h2:eq(0)").each(function(index, el) {
								linkTitle = el.innerHTML;
							});
							if (!linkTitle) {
								$('<div>' + $scope.filesList[i].html + '</div>').find(".general_terms_h1:eq(0)").each(function(index, el) {
									linkTitle = el.innerHTML;
								});
							}
						}
						if (linkTitle) {
							$scope.filesList[i].menuTitle = linkTitle;
						}
					}
				}
				$scope.loading = false;
				repositionAnchors();
				if (!isUndefined($rootScope.$state.params.id)) {
					$timeout(function() {
						$scope.goToByScroll($rootScope.$state.params.id, -50);
					}, 1000)
				}
			})
			.catch(function(data) {
				data = data.data;
				$rootScope.handleNetworkError(data);
			});
	}
}]);
