aoApp.controller('DynamicsController', ['$rootScope', '$scope', '$http', '$timeout', '$interval', function($rootScope, $scope, $http, $timeout, $interval) {
	$scope.marketsInfo = {};//updated date for ls and history {info:{},opps:[],temp:{}}
	$scope.chartsMarkets = [100000, 100001, 100002, 100003];//0-3 : id of markets loaded in charts
	$scope.chartsMarketsManual = [true, false, false, false];//0-3 : is the market selected by the user or by the system
	$scope.initReady = {
		inv: false,
		bigBox: false
	};
	$scope.info = {
		hasOpenMarkets: false,
		dynamicsOpen: null
	};
	$rootScope.inProgress = false;

	function waitForIt() {
		if (isUndefined($rootScope.$state.current.needLogin) || !$rootScope.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			init();
		}
	}
	
	function init() {
		if (settings.userLogin) {
			$scope.getUserInvestments();
		} else {
			$scope.initReady.inv = true;
		}
		
		$scope.connectLs();
		$timeout(function() {
			$scope.checkSmallChartsPriority();
		},2000);
		
		//only the big chart
		$scope.getChartDataCommon({
			boxId: 0,
			marketId: 0
		});
	}
	
	$scope.checkSmallChartsPriority = function() {
		if (!$scope.initReady.inv || !$scope.initReady.bigBox) {
			$timeout(function() {
				$scope.checkSmallChartsPriority();
			},500);
			return;
		}
		var smallCharts = [];
		var marketsWithInv = [];//id,timeFirstInv
		var marketsWithoutInv = [];//id,timeFirstInv
		
		$.each($scope.marketsInfo, function(k, v) {
			if (v.info.active) {
				//arrange markets that have investments
				if (v.opps[v.info.currentOppArrayIndex].investments.length > 0) {
					if (marketsWithInv.length == 0) {
						marketsWithInv.push({
							id: v.info.marketId,
							timeFirstInv: v.opps[v.info.currentOppArrayIndex].investments[0].timeCreated
						});
					} else {
						var added = false;
						for (var i = 0; i < marketsWithInv.length; i++) {
							if (marketsWithInv[i].timeFirstInv > v.opps[v.info.currentOppArrayIndex].investments[0].timeCreated) {
								marketsWithInv.splice(i, 0, {
									id: v.info.marketId,
									timeFirstInv: v.opps[v.info.currentOppArrayIndex].investments[0].timeCreated
								});
								added = true;
								break;
							}
						}
						
						if (!added) {
							marketsWithInv.push({
								id: v.info.marketId,
								timeFirstInv: v.opps[v.info.currentOppArrayIndex].investments[0].timeCreated
							});
						}
					}
				} else {
					if (marketsWithoutInv.length == 0) {
						marketsWithoutInv.push({
							id: v.info.marketId,
							timeLeft: v.opps[v.info.currentOppArrayIndex].timeLeft.seconds
						});
					} else {
						var added = false;
						for (var i = 0; i < marketsWithoutInv.length; i++) {
							if (marketsWithoutInv[i].timeLeft > v.opps[v.info.currentOppArrayIndex].timeLeft.seconds) {
								marketsWithoutInv.splice(i, 0, {
									id: v.info.marketId,
									timeLeft: v.opps[v.info.currentOppArrayIndex].timeLeft.seconds
								});
								added = true;
								break;
							}
						}
						
						if (!added) {
							marketsWithoutInv.push({
								id: v.info.marketId,
								timeLeft: v.opps[v.info.currentOppArrayIndex].timeLeft.seconds
							});
						}
					}
				}
				
			}
		});
		
		chartsMarketsIndex = 1;
		var index = 0;
		while(chartsMarketsIndex < 4) {
			if (marketsWithInv.length > index) {
				//if the market is not selected by the user, the market is not already loaded and the market is not the one from the big chart
				var indexTemp = index;
				var added = false;
				var indexOfMarkets = $.inArray(marketsWithInv[index].id, $scope.chartsMarkets);
				var marketManuelSelected = false;
				if (indexOfMarkets > -1) {
					marketManuelSelected = $scope.chartsMarketsManual[indexOfMarkets];
				}
				
				if (!$scope.chartsMarketsManual[chartsMarketsIndex] && $scope.chartsMarkets[chartsMarketsIndex] != marketsWithInv[index].id && $scope.chartsMarkets[0] != marketsWithInv[index].id && !marketManuelSelected) {
					$scope.selectMarket($scope.marketsInfo[marketsWithInv[index].id], chartsMarketsIndex, false);
					added = true;
				}
				if ($scope.chartsMarkets[chartsMarketsIndex] == marketsWithInv[index].id || $scope.chartsMarkets[0] == marketsWithInv[index].id || added || marketManuelSelected) {
					index++;
					added = false;
				}
				if ($scope.chartsMarkets[0] != marketsWithInv[indexTemp].id) {
					chartsMarketsIndex++;
				}
			} else {
				break;
			}
		}
		
		var index = 0;
		while(chartsMarketsIndex < 4) {
			if (marketsWithoutInv.length > index) {
				//if the market is not selected the user, the market is not already loaded and the market is not the one from the big chart
				var indexTemp = index;
				var added = false;
				var indexOfMarkets = $.inArray(marketsWithoutInv[index].id, $scope.chartsMarkets);
				var marketManuelSelected = false;
				if (indexOfMarkets > -1) {
					marketManuelSelected = $scope.chartsMarketsManual[indexOfMarkets];
				}
				
				if (!$scope.chartsMarketsManual[chartsMarketsIndex] && $scope.chartsMarkets[chartsMarketsIndex] != marketsWithoutInv[index].id && $scope.chartsMarkets[0] != marketsWithoutInv[index].id && !marketManuelSelected) {
					$scope.selectMarket($scope.marketsInfo[marketsWithoutInv[index].id], chartsMarketsIndex, false);
					added = true;
				}
				if ($scope.chartsMarkets[chartsMarketsIndex] == marketsWithoutInv[index].id || $scope.chartsMarkets[0] == marketsWithoutInv[index].id || added || marketManuelSelected) {
					index++;
					added = false;
				}
				if ($scope.chartsMarkets[0] != marketsWithoutInv[indexTemp].id) {
					chartsMarketsIndex++;
				}
			} else {
				break;
			}
		}
		
		for (var i = chartsMarketsIndex; i < 4; i++) {
			if (!isUndefined(angular.element($('#dynamics-chart-' + i))) && !isUndefined(angular.element($('#dynamics-chart-' + i)).scope())) {
				angular.element($('#dynamics-chart-' + i)).scope().updateChart({command: 'off-hours'});
			}
		}
	}
	
	//moved
	$scope.getUserInvestments = function () {
		var InvestmentsMethodRequest = jsonClone(getUserMethodRequest(), {
				isSettled: false,
				startRow: 0
			})
		$http.post(settings.jsonLinkNew + 'getUserInvestments', InvestmentsMethodRequest)
			.then(function(data) {
				data = data.data;
				if (!$rootScope.handleErrors(data, 'getUserInvestments')) {
					for (var i = 0; i < data.investments.length; i++) {
						var investment = data.investments[i];
						var marketId = investment.marketId;
						var oppId = investment.opportunityId;
						
						//if there is no update for this opp
						if ($.inArray(marketId, $rootScope.dynamicsMarkets) > -1) {
							investment.timeCreated = adjustFromUTCToLocal(new Date(investment.timeCreated));
							
							if (isUndefined($scope.marketsInfo[marketId]) || $scope.marketsInfo[marketId].opps.length == 0) {
								if (isUndefined($scope.marketsInfo[marketId])) {
									$scope.marketsInfo[marketId] = $scope.createMarketObj();
								}
							
								//move all data to a temp place for use after ls is connected
								if (isUndefined($scope.marketsInfo[marketId].temp)) {
									$scope.marketsInfo[marketId].temp = {
										investments: []
									};
								}
								if (investment.opportunityTypeId == 7) {//opp type 7 = dynamics
									$scope.marketsInfo[marketId].temp.investments.push(investment);
								}
							} else {
								//find which opp is this info for
								var oppArrayIndex = searchJsonKeyInArray($scope.marketsInfo[marketId].opps, 'oppId', oppId);
								if (oppArrayIndex != -1) {
									var invIndex = searchJsonKeyInArray($scope.marketsInfo[marketId].opps[oppArrayIndex].investments, 'id', investment.id);
									if (invIndex == -1) {
										investment = $scope.calculateInvProfit(investment, marketId, oppArrayIndex);
										investment.timeCreated = adjustFromUTCToLocal(new Date(investment.timeCreated));
										$scope.marketsInfo[marketId].opps[oppArrayIndex].investments.push(investment);
									}
								}
							}
						}
					}
					$scope.initReady.inv = true;
				}
			})
			.catch(function(data) {
				data = data.data;
				$scope.initReady.inv = true;
				$rootScope.handleNetworkError(data);
			});
	}
	
	//moved
	$scope.getChartDataCommon = function (params) {//boxId, marketId
		var ChartDataRequest = jsonClone(getUserMethodRequest(),{
				box: params.boxId, 
				marketId:params.marketId, 
				opportunityId: 0, 
				opportunityTypeId: opportunityTypeId.dynamics
			});
		$http.post(settings.jsonLinkNew + 'getChartDataCommon', ChartDataRequest)
			.then(function(data) {
				data = data.data;
				if (!$rootScope.handleErrors(data, 'getChartDataCommon')) {
					var marketId = data.marketId;
					var oppId = data.opportunityId;
					if (marketId > 0) {
						$scope.chartsMarkets[params.boxId] = marketId;
						
						//if there is no update for this opp
						if (isUndefined($scope.marketsInfo[marketId]) || $scope.marketsInfo[marketId].opps.length == 0) {
							$scope.marketsInfo[marketId] = $scope.createMarketObj();
						
							//move all data to a temp place for use after ls is connected
							$scope.marketsInfo[marketId].temp = data;
						
							//set basic info about the market
							$scope.marketsInfo[marketId].info = {
								marketId: marketId,
								marketName: $scope.markets[marketId].displayName,
								decimalPoint: $scope.markets[marketId].decimalPoint,
								currentOppId: 0,
								currentOppArrayIndex: -1
							};
						} else {
							//find which opp is this info for
							var oppArrayIndex = searchJsonKeyInArray($scope.marketsInfo[marketId].opps, 'oppId', oppId);
							$scope.addHistoryToOpp(marketId, oppArrayIndex, data);
						}
						
						if (params.boxId == 0 && $scope.marketsInfo[marketId].opps[$scope.marketsInfo[marketId].info.currentOppArrayIndex].investments.length > 0) {
							$timeout(function() {
								angular.element($('#DynamicsOpenMarkets')).scope().selectOpenOption($scope.marketsInfo[marketId]);
							}, 500);
						}
					}
					$scope.initReady.bigBox = true;
				}
			})
			.catch(function(data) {
				data = data.data;
				$scope.initReady.bigBox = true;
				$rootScope.handleNetworkError(data);
			})
	}
	
	$scope.addHistoryToOpp = function(marketId, oppArrayIndex, data) {
		var timeEstClosing = adjustFromUTCToLocal(new Date(data.timeEstClosing));
		var timeMarketOpen = new Date(new Date(timeEstClosing).setHours(timeEstClosing.getHours() - 1));
		//add history to the specific opportunity
		if (!isUndefined(data.rates) && !isUndefined(data.ratesTimes)) {
			for (var i=0; i < data.rates.length; i++) {
				if (data.ratesTimes[i] > timeMarketOpen.getTime()) {
					$scope.addPointToChart(
						marketId, 
						oppArrayIndex, 
						data.ratesTimes[i] + $rootScope.UTCOffset, 
						data.rates[i], 
						'', 
						{enabled: false},
						(data.asks != null) ? data.asks[i] : '',
						(data.bids != null) ? data.bids[i] : '',
						(data.lasts != null) ? data.lasts[i] : ''
					);
				}
			}
		}
		
		//set all investments for the oppId
		if (oppArrayIndex > -1) {
			$scope.marketsInfo[marketId].opps[oppArrayIndex].investments = (isUndefined(data.investments) ? [] : data.investments);
		}
		
		//add investments to the history
		if (!isUndefined(data.investments) && data.investments.length > 0 && oppArrayIndex > -1) {
			for (var i = 0; i < data.investments.length; i++) {
				if (data.investments[i].opportunityId == $scope.marketsInfo[marketId].opps[oppArrayIndex].oppId) {
					data.investments[i].timeCreated = adjustFromUTCToLocal(new Date(data.investments[i].timeCreated));
					var timeInv = data.investments[i].timeCreated.getTime();
					if (timeInv > timeMarketOpen.getTime()) {
						data.investments[i] = $scope.calculateInvProfit(data.investments[i], marketId, oppArrayIndex);

						$scope.addPointToChart(
							marketId, 
							oppArrayIndex, 
							timeInv + $rootScope.UTCOffset, 
							data.investments[i].realLevel, 
							'point_' + data.investments[i].id,
							invest_point,
							'',
							'',
							''
						);
					}
				}
			}
			var chartsMarketsIndex = $scope.chartsMarkets.indexOf(marketId);
			if (chartsMarketsIndex > -1 && oppArrayIndex == $scope.marketsInfo[marketId].info.currentOppArrayIndex) {
				if (!isUndefined(angular.element($('#dynamics-chart-' + chartsMarketsIndex)).scope())) {
					angular.element($('#dynamics-chart-' + chartsMarketsIndex)).scope().chartColors();
				}
			}
		}
	}
	
	//moved
	$scope.calculateInvProfit = function(investment, marketId, oppArrayIndex) {
		var invEndDate = $scope.marketsInfo[marketId].opps[oppArrayIndex].times.invEndDate
		investment.if_correct_profit = getProfit({
			dir: (investment.typeId == 7) ? true : false,
			percent: investment.price,
			amount: amountToFloat(investment.amount)
		});
		
		investment.if_correct_profitAmount = investment.if_correct_profit + amountToFloat(investment.amount);
		investment.if_correct_profitAmountDsp = formatAmount(investment.if_correct_profitAmount, 2);
		investment.amountDsp = formatAmount(investment.amount, 2, true);
		
		var tempEnd = milSecToMinFormat(invEndDate - investment.timeCreated - $rootScope.serverOffset);
		investment.timePercent = 100 - ((100 / ($scope.marketsInfo[marketId].opps[oppArrayIndex].times.timeChart * 60)) * (tempEnd.seconds + ($scope.marketsInfo[marketId].opps[oppArrayIndex].times.timeForClosing * 60) + ($rootScope.serverOffset / 1000)));
		
		if (investment.timePercent > 100) {
			investment.timePercent = 100;
		}
		
		return investment;
	}
		
	$scope.connectLs = function () {
		$rootScope.connectToLS_global();
		
		function initLs() {
			if ($rootScope.dynamicsMarkets.length > 0) {
				var groupDynamics = [];
				for (var i = 0; i < $rootScope.dynamicsMarkets.length; i++) {
					groupDynamics.push("bz_" + $rootScope.dynamicsMarkets[i] + '_' + opportunityTypeId.dynamics);
				}
				
				$rootScope.lsConnections.dynamics = $rootScope.connectToLS({
					name: 'dynamics',
					subscriptionType: settings.subscriptionType,
					group: groupDynamics,
					schema: settings.schemaDynamics,
					dataAdapterName: settings.dataAdapterName,
					callBack: $scope.ls_updateItem_dynamics
				});
			} else {
				$timeout(function() {
					initLs();
				}, 100);
			}
		}
		
		initLs();
	}
	
	$scope.ls_updateItem_dynamics = function (data) {
		var marketIdIndex = data.getItemPos() - 1;
		var marketId = $rootScope.dynamicsMarkets[marketIdIndex];
		
		//find if this update is for one of the charts
		var chartsMarketsIndex = $scope.chartsMarkets.indexOf(marketId);
		
		if (isUndefined($scope.marketsInfo[marketId])) {
			$scope.marketsInfo[marketId] = $scope.createMarketObj();
		}

		var oppId = parseInt(data.getValue(SUBSCR_FLD.key));//opportunity id
		var oppState = data.getValue(SUBSCR_FLD.bz_state);//opportunity state
		var estClose = adjustFromUTCToLocal(new Date(data.getValue(SUBSCR_FLD.bz_est_close)));//time closing
		var oppArrayIndex = searchJsonKeyInArray($scope.marketsInfo[marketId].opps, 'oppId', oppId);
		
		//if the opportunity is not active and state 'delete' comes - do nothing
		if (oppArrayIndex === -1 && data.getValue(SUBSCR_FLD.command) === "DELETE"){//state DELETE unset
			logIt({'type':1, 'msg':'DELETE unset opp ' + oppId});
			return;
		}
		
		//if we have the opportunity and delete state comes
		if (data.getValue(SUBSCR_FLD.command) === "DELETE") {//state DELETE
			logIt({'type':1, 'msg':'DELETE timer:' + oppId});
			//delay the delete in case it is a drop in the ls
			$scope.marketsInfo[marketId].opps[oppArrayIndex].deleteDelay = $timeout(function() {
				logIt({'type':1, 'msg':'DELETE ' + oppId});

				$scope.marketsInfo[marketId].opps.splice(oppArrayIndex, 1);
				
				$scope.findWhenDynamicsOpen();
				
				//push delete to box if delete is for market from the box
				if (chartsMarketsIndex > -1 && oppArrayIndex == $scope.marketsInfo[marketId].info.currentOppArrayIndex) {
					$scope.getChartDataCommon({
						boxId: chartsMarketsIndex,
						marketId: marketId
					});
					
					$timeout(function() {
						angular.element($('#dynamics-chart-' + chartsMarketsIndex)).scope().updateChart({command: 'delete'});
						$scope.checkSmallChartsPriority();
					},0);
				}
				
				$scope.setCurrentOpp(marketId);
			},10000);
			return;
		}
		
		//ADD new opp
		if (oppArrayIndex === -1) {
			logIt({'type':1, 'msg':'ADD ' + oppId});

			if ($scope.marketsInfo[marketId].opps.length > 0) {
				if (oppState > $scope.marketsInfo[marketId].opps[0].ls[SUBSCR_FLD.bz_state]) {
					$scope.marketsInfo[marketId].opps.unshift({
						oppId: oppId,
						ls: $scope.schemaWalk(data),
						investments: []
					});
				} else {//if new opp is with lower state then the one active one
					$scope.marketsInfo[marketId].opps.push({
						oppId: oppId,
						ls: $scope.schemaWalk(data),
						investments: []
					});
				}
			} else {//if first opp for this market
				$scope.marketsInfo[marketId].opps[0] = {
					oppId: oppId,
					ls: $scope.schemaWalk(data),
					investments: []
				};
			}
			
			oppArrayIndex = searchJsonKeyInArray($scope.marketsInfo[marketId].opps, 'oppId', oppId);
			
			//calculate all times for this opp (open, close, waiting for exp, etc)
			$scope.marketsInfo[marketId].opps[oppArrayIndex].times = $scope.setOppTimeDim(
				estClose,
				$scope.marketsInfo[marketId].opps[oppArrayIndex].ls[SUBSCR_FLD.bz_last_inv]
			);
			
			//move history from temp to the correct opp
			if (!isUndefined($scope.marketsInfo[marketId].temp) && $scope.marketsInfo[marketId].temp.opportunityId == oppId) {
				$scope.addHistoryToOpp(marketId, oppArrayIndex, $scope.marketsInfo[marketId].temp);
				$scope.marketsInfo[marketId].temp = null;
			}
			
			//if there is no chart history for this market set basic info
			if (isUndefined($scope.marketsInfo[marketId].info.marketId)) {
				$scope.marketsInfo[marketId].info.marketId = marketId;
				$scope.marketsInfo[marketId].info.marketName = $scope.markets[marketId].displayName;
				$scope.marketsInfo[marketId].info.decimalPoint = $scope.markets[marketId].decimalPoint;
			}
			
			$scope.findWhenDynamicsOpen();
		} else {
			//if random ls reset comes to live - do not delete the opp
			$timeout.cancel($scope.marketsInfo[marketId].opps[oppArrayIndex].deleteDelay);
			
			//get last state
			var temp = $scope.marketsInfo[marketId].opps[oppArrayIndex].ls.stateDsp;

			if (!$scope.marketsInfo[marketId].info.freeze) {
				$scope.marketsInfo[marketId].opps[oppArrayIndex].ls = $scope.schemaWalk(data);
			}
			
			//check if state actually changed
			if (temp != $scope.marketsInfo[marketId].opps[oppArrayIndex].ls.stateDsp) {
				$scope.marketsInfo[marketId].opps[oppArrayIndex].ls.stateChanged = true;
			}
		}
		
		//update array position of the current oppid in the array
		var temp = $scope.marketsInfo[marketId].info.currentOppArrayIndex;
		
		$scope.setCurrentOpp(marketId);
		
		var hasCurrentOppArrayIndexChanged = false;
		if (temp != $scope.marketsInfo[marketId].info.currentOppArrayIndex) {
			hasCurrentOppArrayIndexChanged = true;
		}
		
		if (chartsMarketsIndex > -1 && hasCurrentOppArrayIndexChanged && !isUndefined(temp) && temp > -1) {
			$scope.getChartDataCommon({
				boxId: chartsMarketsIndex,
				marketId: marketId
			});
			
			$timeout(function() {
				angular.element($('#dynamics-chart-' + chartsMarketsIndex)).scope().updateChart({command: 'delete'});
				$timeout(function() {
					$scope.checkSmallChartsPriority();
				}, 1000);
			},0);
		}
		
		$scope.isMarketOpen(marketId);
		
		$scope.calculateInvestmentsSell(marketId, oppArrayIndex);

		$scope.checkForActiveMarkets();
		
		//push offhours to all boxes
		if (!$scope.info.hasOpenMarkets) {
			$timeout(function() {
				for (var i = 0; i < $scope.chartsMarkets.length; i++) {
					if (!isUndefined(angular.element($('#dynamics-chart-' + i))) && !isUndefined(angular.element($('#dynamics-chart-' + i)).scope())) {
						angular.element($('#dynamics-chart-' + i)).scope().updateChart({command: 'off-hours'});
					}
				}
			},0);
		}
	
		//make changes only if the update is for the current oppId and box
		if (chartsMarketsIndex > -1 && $scope.marketsInfo[marketId].info.currentOppArrayIndex == oppArrayIndex) {
			//update if level is different from zero
			if ($scope.marketsInfo[marketId].opps[oppArrayIndex].ls[SUBSCR_FLD.bz_current] != 0) {
				var chartPoints = $scope.marketsInfo[marketId].opps[oppArrayIndex].chartPoints;
				
				//if state changes from created to open - get history
				if ($scope.marketsInfo[marketId].opps[oppArrayIndex].ls.stateChanged && $scope.marketsInfo[marketId].opps[oppArrayIndex].ls.stateDsp == 'open') {
					$scope.getChartDataCommon({
						boxId: chartsMarketsIndex,
						marketId: marketId
					});
				}
				
				//add the new point only if it's new
				if (isUndefined(chartPoints) || chartPoints[chartPoints.length-1].x < $scope.marketsInfo[marketId].opps[oppArrayIndex].ls.time.getTime() + $rootScope.UTCOffset) {
					$scope.addPointToChart(
						marketId,
						oppArrayIndex,
						$scope.marketsInfo[marketId].opps[oppArrayIndex].ls.time.getTime() + $rootScope.UTCOffset,
						$scope.marketsInfo[marketId].opps[oppArrayIndex].ls[SUBSCR_FLD.bz_current],
						'',
						{enabled: false},
						($scope.marketsInfo[marketId].opps[oppArrayIndex].ls[SUBSCR_FLD.ask] != null) ? $scope.marketsInfo[marketId].opps[oppArrayIndex].ls[SUBSCR_FLD.ask] : '',
						($scope.marketsInfo[marketId].opps[oppArrayIndex].ls[SUBSCR_FLD.bid] != null) ? $scope.marketsInfo[marketId].opps[oppArrayIndex].ls[SUBSCR_FLD.bid] : '',
						($scope.marketsInfo[marketId].opps[oppArrayIndex].ls[SUBSCR_FLD.last] != null) ? $scope.marketsInfo[marketId].opps[oppArrayIndex].ls[SUBSCR_FLD.last] : ''
					);
				}
				
				//push update to the chart box
				$timeout(function() {
					angular.element($('#dynamics-chart-' + chartsMarketsIndex)).scope().updateChart({command: 'update'});
				},0);
			}
		}
	}
	
	$scope.schemaWalk = function(data) {
		var temp = {};
		for (var i = 0; i < settings.schemaDynamics.length; i++) {
			var valueFromUpdate = data.getValue(settings.schemaDynamics[i]);
			
			if (settings.schemaDynamics[i] === SUBSCR_FLD.key) {
				temp.oppId = parseInt(valueFromUpdate);
				continue;
			} else if (settings.schemaDynamics[i] === SUBSCR_FLD.bz_offer) {
				valueFromUpdate = parseFloat(valueFromUpdate);
				if (valueFromUpdate == 0) {
					temp.profitBuyPercent = 0;
				} else {
					temp.profitBuyPercent = calculateOffer(valueFromUpdate);
				}
			} else if (settings.schemaDynamics[i] === SUBSCR_FLD.bz_bid) {
				valueFromUpdate = parseFloat(valueFromUpdate);
				if (valueFromUpdate == 0) {
					temp.profitSellPercent = 0;
				} else {
					temp.profitSellPercent = calculateBid(valueFromUpdate);
				}
			} else if (settings.schemaDynamics[i] === SUBSCR_FLD.bz_current || settings.schemaDynamics[i] === SUBSCR_FLD.bz_event) {
				valueFromUpdate = parseFloat(valueFromUpdate.replace(/,/g,''));
				if (settings.schemaDynamics[i] === SUBSCR_FLD.bz_current) {
					temp.bz_levelDsp = numberWithCommas(valueFromUpdate.toFixed($scope.markets[data.getValue(SUBSCR_FLD.bz_market_id)].decimalPoint));
				} else {
					temp.bz_eventDsp = numberWithCommas(valueFromUpdate.toFixed($scope.markets[data.getValue(SUBSCR_FLD.bz_market_id)].decimalPoint));
				}
			} else if (settings.schemaDynamics[i] === SUBSCR_FLD.time_stamp) {
				temp.time = new Date(parseInt(valueFromUpdate));
				continue;
			} else if (settings.schemaDynamics[i] === SUBSCR_FLD.ao_opp_state) {
				valueFromUpdate = parseInt(valueFromUpdate);
			} else if (settings.schemaDynamics[i] === SUBSCR_FLD.bz_last_inv || settings.schemaDynamics[i] === SUBSCR_FLD.bz_est_close) {
				valueFromUpdate = adjustFromUTCToLocal(new Date(valueFromUpdate));
			} else if (settings.schemaDynamics[i] === SUBSCR_FLD.bz_state) {
				//active
				if (valueFromUpdate == OPPORTUNITY_STATE.opened || 
						valueFromUpdate == OPPORTUNITY_STATE.last_10_min || 
						valueFromUpdate == OPPORTUNITY_STATE.closing_1_min || 
						valueFromUpdate == OPPORTUNITY_STATE.closing ||
						valueFromUpdate == OPPORTUNITY_STATE.binary0100_updating_prices) {
					temp.active = true;
				} else {
					temp.active = false;
					temp.dateOpens = formatDate(new Date(data.getValue(SUBSCR_FLD.bz_est_close)), false);
				}
				//open
				if (valueFromUpdate == OPPORTUNITY_STATE.opened || valueFromUpdate == OPPORTUNITY_STATE.last_10_min || valueFromUpdate == OPPORTUNITY_STATE.binary0100_updating_prices) {
					temp.open = true;
				} else {
					temp.open = false;
				}
				//waiting to expire
				if (valueFromUpdate == OPPORTUNITY_STATE.closing_1_min || valueFromUpdate == OPPORTUNITY_STATE.closing) {
					temp.waiting = true;
				} else {
					temp.waiting = false;
				}
				switch(parseInt(valueFromUpdate)) {
					case OPPORTUNITY_STATE.created://1
					case OPPORTUNITY_STATE.done://7
					case OPPORTUNITY_STATE.paused://8
					case OPPORTUNITY_STATE.suspended://9
					case OPPORTUNITY_STATE.hidden_waiting_to_expiry://11
					case OPPORTUNITY_STATE.waiting_to_pause: temp.stateDsp = 'off-hours';break;//10
					case OPPORTUNITY_STATE.opened://2
					case OPPORTUNITY_STATE.last_10_min: temp.stateDsp = 'open';break;//3
					case OPPORTUNITY_STATE.closing_1_min://4
					case OPPORTUNITY_STATE.closing: temp.stateDsp = 'waiting';break;//5
					case OPPORTUNITY_STATE.closed: temp.stateDsp = 'expired';break;//6
					case OPPORTUNITY_STATE.binary0100_updating_prices: temp.stateDsp = 'updating-prices';break;//12
				}
			}

			temp[settings.schemaDynamics[i]] = valueFromUpdate;
		}
		temp.maxProfitPercent = (temp.profitBuyPercent > temp.profitSellPercent) ? temp.profitBuyPercent : temp.profitSellPercent;
		temp.stateChanged = false;

		return temp;
	}
	
	$scope.setCurrentOpp = function(marketId) {
		if ($scope.marketsInfo[marketId].opps.length > 0) {
			//decide which opp to display (in case we are in waiting for exp and there are 2)
			var currentOppArrayIndex = 0;
			//if we have more the one opp and closest closest closing is in waiting for exp and there are no investments - go for the open one
			if ($scope.marketsInfo[marketId].opps.length > 1 && $scope.marketsInfo[marketId].opps[0].ls[SUBSCR_FLD.bz_state] > 4 && $scope.marketsInfo[marketId].opps[0].investments.length == 0) {
				currentOppArrayIndex = 1;
			}
			
			$scope.marketsInfo[marketId].info.currentOppArrayIndex = currentOppArrayIndex;
			$scope.marketsInfo[marketId].info.currentOppId = $scope.marketsInfo[marketId].opps[currentOppArrayIndex].oppId;
		}
	}
	
	$scope.setOppTimeDim = function(timeEstClosing, timeLastInvest) {
		var temp = {
			invStartDate: new Date(new Date(timeEstClosing).setHours(timeEstClosing.getHours() - 1)),
			waitingForExpireDate: timeLastInvest,
			invEndDate: timeEstClosing,
			chartEndDate: new Date(new Date(timeEstClosing).setMinutes(timeEstClosing.getMinutes() + 4)),
		}
		
		temp.timeChart = milSecToMin(temp.chartEndDate - temp.invStartDate);
		var percent = 100 / temp.timeChart;
		temp.timeForInv = milSecToMin(temp.waitingForExpireDate - temp.invStartDate);
		temp.timeForInvPercent = percent * temp.timeForInv;
		temp.timeForWaiting = milSecToMin(temp.invEndDate - temp.waitingForExpireDate);
		temp.timeForWaitingPercent = percent * temp.timeForWaiting;
		temp.timeForClosing = milSecToMin(temp.chartEndDate - temp.invEndDate);
		temp.timeForClosingPercent = percent * temp.timeForClosing;
		
		return temp;
	}
	
	$scope.updateTimeLeft = function() {
		$.each($scope.marketsInfo, function(k, v) {
			for (var i = 0; i < v.opps.length; i++) {
				if (v.opps.length > 0) {
					v.opps[i].timeLeft = $scope.calcTimeLeft(v.opps[i].times);
				}
			}
		});
	}
	$scope.timeUpdate = $interval($scope.updateTimeLeft, 1000);
	
	$scope.calcTimeLeft = function(times) {
		var temp = milSecToMinFormat(times.waitingForExpireDate - new Date() - $rootScope.serverOffset);//for some reason there is 30 seconds difference ?!?!? - remove if needed on production
		var tempEnd = milSecToMinFormat(times.invEndDate - new Date() -  $rootScope.serverOffset);//for some reason there is 30 seconds difference ?!?!? - remove if needed on production

		temp.percent = 100 - ((100 / (times.timeChart * 60)) * (tempEnd.seconds + (times.timeForClosing * 60)));
		return temp;
	}
	
	$scope.addPointToChart = function(marketId, oppArrayIndex, x_val, y_val, point_id, marker, ask, bid, last) {
		if (oppArrayIndex > -1) {
			if (isUndefined($scope.marketsInfo[marketId].opps[oppArrayIndex].chartPoints)) {
				$scope.marketsInfo[marketId].opps[oppArrayIndex].chartPoints = [];
			}
			$scope.marketsInfo[marketId].opps[oppArrayIndex].chartPoints.push({
				x : x_val,
				y : +y_val.toFixed($scope.marketsInfo[marketId].info.decimalPoint),
				id: point_id,
				marker: marker,
				ask: ask,
				bid: bid,
				last: last
			});
		}
	}
	
	$scope.checkForActiveMarkets = function() {
		$.each($scope.marketsInfo, function(k, v) {
			for (var i = 0; i < v.opps.length; i++) {
				if (v.opps[i].ls.active) {
					$scope.info.hasOpenMarkets = true;
					return;
				}
			}
		});
	}
	
	$scope.findWhenDynamicsOpen = function() {
		$scope.info.dynamicsOpen = null;
		$scope.info.dynamicsOpenMarket = 0;
		$.each($scope.marketsInfo, function(k, v) {
			for (var i = 0; i < v.opps.length; i++) {
				if (v.opps[i].ls[SUBSCR_FLD.bz_state] == '1') {
					if ($scope.info.dynamicsOpen == null || v.opps[i].ls[SUBSCR_FLD.bz_est_close] < $scope.info.dynamicsOpen) {
						$scope.info.dynamicsOpen = v.opps[i].ls[SUBSCR_FLD.bz_est_close];
						$scope.info.dynamicsOpenMarket = v.info.marketId;
					}
				}
			}
		});
	}
	
	$scope.isMarketOpen = function(marketId) {//market is open if at least one opp is in active state
		for (var i = 0; i < $scope.marketsInfo[marketId].opps.length; i++) {
			if ($scope.marketsInfo[marketId].opps[i].ls.active) {
				$scope.marketsInfo[marketId].info.active = true;
				return;
			}
		}
		$scope.marketsInfo[marketId].info.active = false;
	}
	
	$scope.calculateInvestmentsSell = function(marketId, oppArrayIndex) {
		var opp = $scope.marketsInfo[marketId].opps[oppArrayIndex];
		$scope.marketsInfo[marketId].info.firstInvTime = new Date(0);
		
		if (oppArrayIndex == $scope.marketsInfo[marketId].info.currentOppArrayIndex) {
			if (!isUndefined(opp.investments) && opp.investments.length > 0) {
				$scope.marketsInfo[marketId].info.hasInvs = true;
			} else {
				$scope.marketsInfo[marketId].info.hasInvs = false;
			}
		}
		
		opp.ls.total_invested_buy = 0;
		opp.ls.total_return_buy = 0;
		opp.ls.total_close_for_buy = 0;
		opp.ls.total_inv_buy = 0;
		
		opp.ls.total_invested_sell = 0;
		opp.ls.total_return_sell = 0;
		opp.ls.total_close_for_sell = 0;
		opp.ls.total_inv_sell = 0;
		
		opp.ls.total_inv = 0;
		
		
		if (!isUndefined(opp.investments)) {
			for (var i = 0; i < opp.investments.length; i++) {
				var amount = amountToFloat(opp.investments[i].amount);
				opp.ls.total_inv += amount;
				var contracts = 0;
				var close_for = 0;
				
				if (opp.investments[i].typeId == 7) {//above
					contracts = (opp.investments[i].price > 0) ? Math.round((amount / opp.investments[i].price) * 10000) / 10000 : amount;
					close_for = opp.ls[SUBSCR_FLD.bz_bid] * contracts;
					opp.ls.total_invested_buy += amount;
					opp.ls.total_return_buy += opp.investments[i].if_correct_profitAmount;
					opp.ls.total_close_for_buy += close_for;
					opp.ls.total_inv_buy++;
					opp.investments[i].winIfExpire = (opp.ls[SUBSCR_FLD.bz_current] > opp.ls[SUBSCR_FLD.bz_event]);
				} else {//below
					contracts = (opp.investments[i].price > 0) ?  Math.round((amount / (100 - opp.investments[i].price)) * 10000) / 10000 : 0;
					close_for = (100 - opp.ls[SUBSCR_FLD.bz_offer]) * contracts;
					opp.ls.total_invested_sell += amount;
					opp.ls.total_return_sell += opp.investments[i].if_correct_profitAmount;
					opp.ls.total_close_for_sell += close_for;
					opp.ls.total_inv_sell++;
					opp.investments[i].winIfExpire = (opp.ls[SUBSCR_FLD.bz_current] < opp.ls[SUBSCR_FLD.bz_event]);
				}
				opp.investments[i].close_for = close_for;
				opp.investments[i].close_forDsp = formatAmount(close_for, 2);
				opp.investments[i].close_forDspCents = formatAmount(close_for, 1);
				opp.investments[i].winIfCloseNow = (amount < opp.investments[i].close_for);
			}
		}
		
		opp.ls.total_invested_buyDsp = formatAmount(opp.ls.total_invested_buy, 2);
		opp.ls.total_return_buyDsp = formatAmount(opp.ls.total_return_buy, 2);
		opp.ls.total_return_buyDspCents = formatAmount(opp.ls.total_return_buy, 1);
		opp.ls.total_close_for_buyDsp = formatAmount(opp.ls.total_close_for_buy, 1);
		opp.ls.total_buy_win = (opp.ls.total_invested_buy < opp.ls.total_close_for_buy);
		
		opp.ls.total_invested_sellDsp = formatAmount(opp.ls.total_invested_sell, 2);
		opp.ls.total_return_sellDsp = formatAmount(opp.ls.total_return_sell, 2);
		opp.ls.total_return_sellDspCents = formatAmount(opp.ls.total_return_sell, 1);
		opp.ls.total_close_for_sellDsp = formatAmount(opp.ls.total_close_for_sell, 1);
		opp.ls.total_sell_win = (opp.ls.total_invested_sell < opp.ls.total_close_for_sell);
		
		opp.ls.total_close_all_for = opp.ls.total_close_for_buy + opp.ls.total_close_for_sell;
		opp.ls.total_close_all_forDsp = formatAmount(opp.ls.total_close_all_for, 2);
		opp.ls.total_close_all_forDspCents = formatAmount(opp.ls.total_close_all_for, 1);
		opp.ls.total_invDsp = formatAmount(opp.ls.total_inv, 2);
		opp.ls.total_invDspCents = formatAmount(opp.ls.total_inv, 1);
		opp.ls.total_win = (opp.ls.total_inv < opp.ls.total_close_all_for);
	}
	
	$scope.closeFor = function(inv) {
		$scope.marketsInfo[inv.marketId].info.freeze = true;
		var oppArrayIndex = searchJsonKeyInArray($scope.marketsInfo[inv.marketId].opps, 'oppId', inv.opportunityId);
		var SellDynamicsInvestmentRequest = jsonClone(getUserMethodRequest(), {
			investmentId: inv.id,
			opportunityId: inv.opportunityId,
			opportunityTypeId: inv.opportunityTypeId,
			fullAmount: inv.amount,
			sellAmount:inv.amount,
			bid: $scope.marketsInfo[inv.marketId].opps[oppArrayIndex].ls[SUBSCR_FLD.bz_bid],
			offer: $scope.marketsInfo[inv.marketId].opps[oppArrayIndex].ls[SUBSCR_FLD.bz_offer]
		});
		$http.post(settings.jsonLinkNew + 'sellDynamicsInvestment', SellDynamicsInvestmentRequest)
			.then(function(data) {
				data = data.data;
				var oppArrayIndex = searchJsonKeyInArray($scope.marketsInfo[inv.marketId].opps, 'oppId', inv.opportunityId);
				var invArrayIndex = searchJsonKeyInArray($scope.marketsInfo[inv.marketId].opps[oppArrayIndex].investments, 'id', inv.id);
				if (!$rootScope.handleErrors(data, 'sellDynamicsInvestment')) {
					refreshHeader();
					
					$scope.marketsInfo[inv.marketId].opps[oppArrayIndex].investments[invArrayIndex].optionStatus = true;
					$scope.marketsInfo[inv.marketId].opps[oppArrayIndex].investments[invArrayIndex].optionStatusText = $rootScope.getMsgs('option_was_closed',{amount: formatAmount(inv.amount, 2, true)});
					
					$timeout(function() {
						$scope.marketsInfo[inv.marketId].info.freeze = false;
						$scope.marketsInfo[inv.marketId].opps[oppArrayIndex].investments.splice(invArrayIndex, 1);
						$scope.calculateInvestmentsSell(inv.marketId, oppArrayIndex);
						
						var chartsMarketsIndex = $scope.chartsMarkets.indexOf(inv.marketId);
						var currentOppArrayIndex = searchJsonKeyInArray($scope.marketsInfo[inv.marketId].opps, 'oppId', $scope.marketsInfo[inv.marketId].info.currentOppId);
						
						if (chartsMarketsIndex > -1 && currentOppArrayIndex > -1) {
							var pointIndex = searchJsonKeyInArray($scope.marketsInfo[inv.marketId].opps[oppArrayIndex].chartPoints, 'id', 'point_' + inv.id);
							$scope.marketsInfo[inv.marketId].opps[oppArrayIndex].chartPoints.splice(pointIndex, 1);
							angular.element($('#dynamics-chart-' + chartsMarketsIndex)).scope().chartColors();
							angular.element($('#dynamics-chart-' + chartsMarketsIndex)).scope().redrowChart();
						}
					}, 2000);
				} else {
					$scope.marketsInfo[inv.marketId].opps[oppArrayIndex].investments[invArrayIndex].optionStatus = true;
					$scope.marketsInfo[inv.marketId].opps[oppArrayIndex].investments[invArrayIndex].optionStatusText = $rootScope.getMsgs('generalError');
					$scope.marketsInfo[inv.marketId].info.freeze = false;
					
					$timeout(function() {
						$scope.marketsInfo[inv.marketId].opps[oppArrayIndex].investments[invArrayIndex].optionStatus = false;
					}, 2000);
				}
			})
			.catch(function(data) {
				data = data.data;
				$rootScope.handleNetworkError(data);
			})
	}
		
	$scope.closeAllFor = function(marketId, typeId) {
		$scope.marketsInfo[marketId].info.freeze = true;
		var oppId = $scope.marketsInfo[marketId].opps[0].ls.oppId;
		var oppArrayIndex = searchJsonKeyInArray($scope.marketsInfo[marketId].opps, 'oppId', oppId);
		var SellDynamicsInvestmentRequest = jsonClone(getUserMethodRequest(), {
			opportunityId: oppId,
			// opportunityTypeId: inv.opportunityTypeId,
			typeId: typeId,
			bid: $scope.marketsInfo[marketId].opps[oppArrayIndex].ls[SUBSCR_FLD.bz_bid],
			offer: $scope.marketsInfo[marketId].opps[oppArrayIndex].ls[SUBSCR_FLD.bz_offer]
		});
		$http.post(settings.jsonLinkNew + 'sellAllDynamicsInvestments', SellDynamicsInvestmentRequest)
			.then(function(data) {
				data = data.data;
				if (!$rootScope.handleErrors(data, 'sellAllDynamicsInvestments')) {
					refreshHeader();
					var oppArrayIndex = searchJsonKeyInArray($scope.marketsInfo[marketId].opps, 'oppId', oppId);
					var chartsMarketsIndex = $scope.chartsMarkets.indexOf(marketId);
					var currentOppArrayIndex = searchJsonKeyInArray($scope.marketsInfo[marketId].opps, 'oppId', $scope.marketsInfo[marketId].info.currentOppId);
			
					if (typeId == 0) {
						$scope.optionStatus = true;
						$scope.optionStatusText = $rootScope.getMsgs('option_was_closed',{amount: $scope.marketsInfo[marketId].opps[currentOppArrayIndex].ls.total_invDsp});
					} else {
						var amount = 0;
						for (var i = $scope.marketsInfo[marketId].opps[oppArrayIndex].investments.length - 1; i >= 0; i--) {
							if ($scope.marketsInfo[marketId].opps[oppArrayIndex].investments[i].typeId == typeId) {
								amount += $scope.marketsInfo[marketId].opps[oppArrayIndex].investments[i].amount;
							}
						}
						
						$scope.marketsInfo[marketId].info.optionStatusText = $rootScope.getMsgs('option_was_closed',{amount: formatAmount(amount, 2, true)});
						
						if (typeId == 7) {
							$scope.marketsInfo[marketId].info.optionStatusCall = true;
						} else {
							$scope.marketsInfo[marketId].info.optionStatusPut = true;
						}
					}
					
					$timeout(function() {
						$scope.optionStatus = false;
						$scope.marketsInfo[marketId].info.freeze = false;
						if (typeId == 0) {
							if (chartsMarketsIndex > -1 && currentOppArrayIndex > -1) {
								for (var i = $scope.marketsInfo[marketId].opps[oppArrayIndex].investments.length - 1; i >= 0; i--) {
									var invId = $scope.marketsInfo[marketId].opps[oppArrayIndex].investments[i].id;
									var invArrayIndex = searchJsonKeyInArray($scope.marketsInfo[marketId].opps[oppArrayIndex].investments, 'id', invId);
									$scope.marketsInfo[marketId].opps[oppArrayIndex].investments.splice(invArrayIndex, 1);
									$scope.calculateInvestmentsSell(marketId, oppArrayIndex);
									
									var pointIndex = searchJsonKeyInArray($scope.marketsInfo[marketId].opps[oppArrayIndex].chartPoints, 'id', 'point_' + invId);
									$scope.marketsInfo[marketId].opps[oppArrayIndex].chartPoints.splice(pointIndex, 1);
								}
								angular.element($('#dynamics-chart-' + chartsMarketsIndex)).scope().chartColors();
								angular.element($('#dynamics-chart-' + chartsMarketsIndex)).scope().redrowChart();
							}
							$scope.marketsInfo[marketId].opps[oppArrayIndex].investments = [];
						} else {
							for (var i = $scope.marketsInfo[marketId].opps[oppArrayIndex].investments.length - 1; i >= 0; i--) {
								if ($scope.marketsInfo[marketId].opps[oppArrayIndex].investments[i].typeId == typeId) {
									var invId = $scope.marketsInfo[marketId].opps[oppArrayIndex].investments[i].id;
									var invArrayIndex = searchJsonKeyInArray($scope.marketsInfo[marketId].opps[oppArrayIndex].investments, 'id', invId);
									$scope.marketsInfo[marketId].opps[oppArrayIndex].investments.splice(invArrayIndex, 1);
									$scope.calculateInvestmentsSell(marketId, oppArrayIndex);
									
									if (chartsMarketsIndex > -1 && currentOppArrayIndex > -1) {
										var pointIndex = searchJsonKeyInArray($scope.marketsInfo[marketId].opps[oppArrayIndex].chartPoints, 'id', 'point_' + invId);
										$scope.marketsInfo[marketId].opps[oppArrayIndex].chartPoints.splice(pointIndex, 1);
									}
								}
							}
							
							if (chartsMarketsIndex > -1 && currentOppArrayIndex > -1) {
								angular.element($('#dynamics-chart-' + chartsMarketsIndex)).scope().chartColors();
								angular.element($('#dynamics-chart-' + chartsMarketsIndex)).scope().redrowChart();
							}
							
							$scope.marketsInfo[marketId].info.optionStatusCall = false;
							$scope.marketsInfo[marketId].info.optionStatusPut = false;
						}
					}, 2000);
				} else {
					$scope.marketsInfo[marketId].info.optionStatusText = $rootScope.getMsgs('generalError');
					if (typeId == 0) {
						$scope.optionStatus = true;
						$scope.optionStatusText = $scope.marketsInfo[marketId].info.optionStatusText;
						$timeout(function() {
							$scope.optionStatus = false;
						}, 2000);
					} else if (typeId == 7) {
						$scope.marketsInfo[marketId].info.optionStatusCall = true;
						$timeout(function() {
							$scope.marketsInfo[marketId].info.optionStatusCall = false;
						}, 2000);
					} else {
						$scope.marketsInfo[marketId].info.optionStatusPut = true;
						$timeout(function() {
							$scope.marketsInfo[marketId].info.optionStatusPut = false;
						}, 2000);
					}
					$scope.marketsInfo[marketId].info.freeze = false;
				}
			})
			.catch(function(data, status, headers, config) {
				$rootScope.handleNetworkError(data);
			})
	}
	
	$scope.selectMarket = function(market, box, manual, event) {
		if (!$rootScope.inProgress) {
			var boxMarketIndex = $.inArray(market.info.marketId, $scope.chartsMarkets);
			
			if (box == 0) {
				angular.element($('#DynamicsOpenMarkets')).scope().selectOpenOption(market);
			}
			
			if (boxMarketIndex == -1) {
				$scope.chartsMarkets[box] = market.info.marketId;
				setMarketInBox(box);
				if (manual) {
					$scope.chartsMarketsManual[box] = true;
				}
				if (box == 0) {
					$scope.checkSmallChartsPriority();
				}
			} else {
				if (boxMarketIndex != box) {
					if (box == 0) {
						$scope.chartsMarketsManual[boxMarketIndex] = false;
						
						var chartsMarketsCopy = cloneObj($scope.chartsMarkets);
					
						chartsMarketsCopy[box] = market.info.marketId;
						//set not existing market
						chartsMarketsCopy[boxMarketIndex] = 100000 + boxMarketIndex;

						$scope.chartsMarkets = cloneObj(chartsMarketsCopy);
					} else {
						var chartsMarketsCopy = cloneObj($scope.chartsMarkets);
						var usedMarketId = $scope.chartsMarkets[box];
						
						chartsMarketsCopy[box] = market.info.marketId;
						chartsMarketsCopy[boxMarketIndex] = usedMarketId;
						
						$scope.chartsMarkets = cloneObj(chartsMarketsCopy);
					}
					
					setMarketInBox(box);
					if (manual) {
						$scope.chartsMarketsManual[box] = true;
					}

					//load market only if we do not change the big chart
					setMarketInBox(boxMarketIndex);
						
					// if (manual && box == 0) {
						// $scope.chartsMarketsManual[boxMarketIndex] = false;
					// }
					
					if (box == 0) {
						$scope.checkSmallChartsPriority();
					}
				}
			}
			if (!isUndefined(event)) {
				$(event.target).closest('ul.list').slideUp();
			}
		}
		
		function setMarketInBox(box) {
			if ($scope.chartsMarkets[box] < 100000) {
				$scope.getChartDataCommon({
					boxId: box,
					marketId: $scope.chartsMarkets[box]
				});
			}
			
			$timeout(function() {
				angular.element($('#dynamics-chart-' + box)).scope().updateChart({command: 'delete'});
				angular.element($('#dynamics-chart-' + box)).scope().params.boxId = box;
			},0);
		}
	}
	
	//moved
	$scope.createMarketObj = function() {
		//opps:{oppId:number,ls:{},timeLeft:{},times:{},investments:[{}],chartPoints:[{}]}
		return {
			info: {},
			opps: [],
			temp: null
		};
	}
	
	waitForIt();
}]);

aoApp.controller('DynamicsChartController', ['$rootScope', '$scope', '$http', '$timeout', function($rootScope, $scope, $http, $timeout) {
	$scope.chart = null;
	$scope.walkthroughClass = '';
	$scope.walkthroughStep = 0;
	
	$scope.filterAll = {info:{active:true}};
	$scope.order = 'info.marketName';
	
	$scope.params = {};
	$scope.c = {};//short for chart: keeps all need info for the current instance of the chart
	$scope.market = {};//reference to $scope.marketsInfo[marketId]
	$scope.opp = {
		ls: {
			total_close_all_forDsp: formatAmount(0, 2),
			total_close_all_forDspCents: formatAmount(0, 1)
		}
	};//reference to $scope.marketsInfo[marketId].opps[current oppId]
	
	function waitForIt() {
		if (isUndefined($rootScope.$state.current.needLogin)) {
			setTimeout(function(){waitForIt()},50);
		} else {
			// $scope.init();
		}
	}
	
	$scope.passParams = function(params) {
		$scope.resetChart();
		$scope.params = params;
		$scope.params.boxId = params.boxId;
		
		$scope.chart_properties = {};
		if ($scope.params.type == 'small') {
			$scope.chart_properties = jsonClone(chart_properties, chart_properties_small);
		} else {
			$scope.chart_properties = chart_properties;
		}
	}
	
	$scope.updateChart = function(params) {
		if (params.command == 'delete') {
			$scope.c.chartOnline = false;
		}
		
		if (!$scope.c.chartOnline) {
			$scope.c.marketId = $scope.chartsMarkets[$scope.params.boxId];
			$scope.market = $scope.marketsInfo[$scope.c.marketId];
			$scope.c.showPopUp = false;
		}
		
		if (!isUndefined($scope.marketsInfo[$scope.c.marketId])) {
			$scope.c.currentOppArrayIndex = $scope.marketsInfo[$scope.c.marketId].info.currentOppArrayIndex;
			$scope.opp = $scope.marketsInfo[$scope.c.marketId].opps[$scope.c.currentOppArrayIndex];
		} else if (params.command != 'off-hours') {
			return;
		}
		
		if (!isUndefined($scope.opp)) {
			if (params.command == 'delete' && $scope.opp.ls[SUBSCR_FLD.bz_state] != 1) {
				$scope.resetChart();
				return;
			} else if (params.command == 'off-hours' || $scope.c.marketId > 100000 || $scope.opp.ls[SUBSCR_FLD.bz_state] == 1) {
				$scope.showOffHours();
				return;
			}
			

			$scope.c.levelStatus = $scope.setLevelStatus($scope.opp.ls[SUBSCR_FLD.bz_current], $scope.opp.ls[SUBSCR_FLD.bz_event]);
			$scope.updateProfit();
			$scope.updateState();
			
			if (!$scope.c.chartOnline) {
				$scope.initChart();
				$scope.c.chartOnline = true;
			}
			
			$scope.redrowChart();
		} else {
			$scope.showOffHours();
		}
	}
	
	$scope.showOffHours = function() {
		var tempDate = $scope.opp.ls.dateOpens;
		$scope.resetChart();
		if (isUndefined($scope.info.dynamicsOpenMarket)) {
			$scope.c.available_at = tempDate;
		} else if (!isUndefined($scope.info.dynamicsOpen)) {
			$scope.c.available_at = formatDate($scope.info.dynamicsOpen, false, false);
		}
		
		$scope.c.suspend_msgs = $rootScope.getMsgs('available_at', {expTime: $scope.c.available_at})
		$scope.c.popUpState = 'off-hours';
		$scope.c.state = 'off-hours';
		$scope.c.showPopUp = true;
		return;
	}
	
	$scope.initChart = function() {
		$scope.chart = new Highcharts.StockChart({
			credits: {
				enabled: false
			},
			chart: {
				renderTo: 'chart-' + $scope.params.boxId,
				margin: 0,
				height: $scope.chart_properties.height,
				backgroundColor: 'transparent',
				panning: false
			},
			rangeSelector : {
				enabled : false,
				selected: 0
			},
			scrollbar : {
				enabled : false
			},
			navigator : {
				enabled : false
			},
			xAxis: [{
				ordinal: false,
				type: 'datetime',
				dateTimeLabelFormats: {
					day: '%H:%M'
				},
				min: $scope.opp.times.invStartDate.getTime() + $rootScope.UTCOffset,//1 hours
				max: $scope.opp.times.chartEndDate.getTime() + $rootScope.UTCOffset,
				labels: {
					x: 0,
					y: $scope.chart_properties.timeLineTxtPosition
				},
				gridLineColor: 'transparent',
				lineWidth: 0,
				endOnTick: false
			}],
			yAxis: [{
				height: $scope.chart_properties.yAxis_1,
				labels: {
					enabled:false
				},
				gridLineColor: 'transparent',
				lineWidth: 0
			}],
			series : [{
				type: 'line',
				lineWidth: 1,
				color: $scope.chart_properties.chart_lineColor,
				name: $scope.market.info.marketName,
				turboThreshold:0,
				data : [],
				zIndex: 12,
				dataGrouping: {
					enabled:false
				}
			}],
			tooltip: {
				backgroundColor: null,
				borderColor: null,
				borderWidth: 0,
				shadow: false,
				useHTML: true,
				style:{
					zIndex:10
				},
				zIndex:10,
				formatter: function() {
					var num = Math.pow(10,$scope.market.info.decimalPoint);
					var level = Math.round(this.y*(num))/num;
					
					var temp = '';
					if (skinRegulated && userLogin) {
						var temp = (this.points[0].point.ask != '') ? 'ASK:' + this.points[0].point.ask : '';
						temp += (this.points[0].point.bid != '') ? ((temp != '') ? " | " : '') + 'BID:' + this.points[0].point.bid : '';
						temp += (this.points[0].point.last != '') ? ((temp != '') ? " | " : '') + 'LAST:' + this.points[0].point.last : '';
					}
					
					var className = ($scope.opp.investments.length > 0) ? 'with-inv' : '';
					
					var str = '<div class="chartTooltip '+className+'">';
					str += '<span class="tooltip-date">'+formatDate(new Date(this.x), false, 'back')+'</span>';
					str += '<div><span class="tooltip-market">'+$scope.market.info.marketName+': </span>';
					str += '<span class="tooltip-level">'+numberWithCommas(level)+"</span></div>";
						str += '<div class="tooltip-ask">';
						str += temp;
						str += '</div>';
					str += "</div>";
					return str;
				}
			}
		});
		$scope.chartColors();
	}
	
	$scope.chartColors = function() {
		if (!isUndefined($scope.chart)) {
			var timeLineTxtColor = chart_properties.timeLineTxtColor;
			
			var tooltip_borderColor = chart_properties.tooltip_borderColor;
			var tooltip_bgrColor = chart_properties.tooltip_bgrColor;
			var tooltip_timeTxtColor = chart_properties.tooltip_timeTxtColor;
			var tooltip_assetTxtColor = chart_properties.tooltip_assetTxtColor;
			var tooltip_levelTxtColor = chart_properties.tooltip_levelTxtColor;
	
			if ($scope.opp.investments.length > 0) {
				timeLineTxtColor = chart_properties.timeLineTxtColor_with_inv;
				
				tooltip_borderColor = chart_properties_with_inv.tooltip_borderColor;
				tooltip_bgrColor = chart_properties_with_inv.tooltip_bgrColor;
				tooltip_timeTxtColor = chart_properties_with_inv.tooltip_timeTxtColor;
				tooltip_assetTxtColor = chart_properties_with_inv.tooltip_assetTxtColor;
				tooltip_levelTxtColor = chart_properties_with_inv.tooltip_levelTxtColor;
			}
			
			$scope.chart.xAxis[0].update({
				labels:{
					style:{
						color:timeLineTxtColor
					}
				}
			});
			$scope.chart.tooltip.options.borderColor = tooltip_borderColor;
			$scope.chart.tooltip.options.backgroundColor = tooltip_bgrColor;
		}
	}
	
	$scope.resetChart = function() {
		$scope.c = {
			marketId: 0,
			invAmount: $rootScope.user.defaultAmount,
			invAmountDsp: $rootScope.user.defaultAmountDsp,
			invCustomAmount: '',
			chartOnline: false,
			showPopUp: true,
			popUpState: 'loading',
			state: 'loading',
			submitted: false,
			overlayClass: '',
			showChart: true,
			generalError: false,
			generalErrorMsg: ''
		};
		
		$scope.opp = {
			ls: {
				BZ_EVENT: '',
				maxProfitPercent: 0,
				profitBuyPercent: 0,
				profitBuyAmount: formatAmount(0, 1),
				profitSellPercent: 0,
				profitSellAmount: formatAmount(0, 1),
				refundBuyAmount: formatAmount(0, 2),
				refundSellAmount: formatAmount(0, 2),
				profitProcent: 50,
				total_close_all_forDsp: formatAmount(0, 2),
				total_close_all_forDspCents: formatAmount(0, 1),
				total_close_all_for: 0
			},
			investments: []
		}
		
		if ($scope.chart != null) {
			$scope.chart.destroy();
			$scope.chart = null;
		}
	}
	
	$scope.changeInvestAmount = function(amount, event) {
		$scope.c.invAmount = amount.predefValue
		$scope.c.invAmountDsp = amount.predefValueDsp
		$scope.updateProfit();
		$(event.target).closest('ul.list').slideUp();
	}
	
	$scope.customInvAmount = function(event) {
		if (event.which === 13) {
			$(event.target).closest('ul.list').slideUp();
			return;
		}
		
		if (isLeftCurrencySymbol) {
			$scope.c.invCustomAmount = currencySymbol + checkValue($scope.c.invCustomAmount);
		} else {
			$scope.c.invCustomAmount = checkValue($scope.c.invCustomAmount);
		}
		
		if ($scope.c.invCustomAmount == currencySymbol){
			$scope.c.invCustomAmount = '';
		}
		
		if ($scope.c.invCustomAmount != "" && $scope.c.invCustomAmount != currencySymbol) {
			$scope.c.invAmount = parseFloat($scope.c.invCustomAmount.replace(currencySymbol,''));
			$scope.c.invAmountDsp = $scope.c.invCustomAmount;
			$scope.updateProfit();
		}
	}
	
	$scope.updateProfit = function() {
		$scope.opp.ls.profitBuyAmount = formatAmount($scope.c.invAmount + ($scope.opp.ls.profitBuyPercent * $scope.c.invAmount / 100), 1);
		$scope.opp.ls.refundBuyAmount = formatAmount(0, 2);
		$scope.opp.ls.profitSellAmount = formatAmount($scope.c.invAmount + ($scope.opp.ls.profitSellPercent * $scope.c.invAmount / 100), 1);
		$scope.opp.ls.refundSellAmount = formatAmount(0, 2);
		$scope.opp.ls.profitProcent = 100 / ($scope.opp.ls.profitBuyPercent + $scope.opp.ls.profitSellPercent) * $scope.opp.ls.profitSellPercent
	}
	
	//using all points redrow the chart after ordering the points
	$scope.redrowChart = function() {
		if (!isUndefined($scope.opp.chartPoints)) {
			$scope.opp.chartPoints = $scope.opp.chartPoints.sort(function(a,b) {
				return a.x - b.x;
			});
			$scope.addSquareToLastPoint();
			var min = Math.min.apply(Math,$scope.opp.chartPoints.map(function(o){return o.y;}));
			var max = Math.max.apply(Math,$scope.opp.chartPoints.map(function(o){return o.y;}));
			
			var minD = Math.abs(min - $scope.opp.ls[SUBSCR_FLD.bz_event]);
			var maxD = Math.abs(max - $scope.opp.ls[SUBSCR_FLD.bz_event]);
			
			var diff = (minD > maxD) ? minD : maxD;
			
			var max = $scope.opp.ls[SUBSCR_FLD.bz_event] + diff;
			var min = $scope.opp.ls[SUBSCR_FLD.bz_event] - diff;
			
			$scope.chart.series[0].setData($scope.opp.chartPoints, true);
			
			$scope.chart.yAxis[0].setExtremes(min, max);
			
			$scope.chart.xAxis[0].removePlotLine('plot-line-x');
			$scope.chart.xAxis[0].addPlotLine({
				// value: time+self.serverOffset,
				value: $scope.chart.series[0].points[$scope.chart.series[0].points.length-1].x,
				width: 1,
				color: ($scope.opp.investments.length == 0) ? $scope.chart_properties.plotLinesColor : $scope.chart_properties.plotLinesColor_with_inv,
				dashStyle: 'ShortDash',
				zIndex:2,
				id: 'plot-line-x'
			});
			$scope.chart.yAxis[0].removePlotLine('plot-line-y');
			$scope.chart.yAxis[0].addPlotLine({
				value: $scope.chart.series[0].points[$scope.chart.series[0].points.length-1].y,
				width: 1,
				color: ($scope.opp.investments.length == 0) ? $scope.chart_properties.plotLinesColor : $scope.chart_properties.plotLinesColor_with_inv,
				dashStyle: 'ShortDash',
				zIndex:2,
				id: 'plot-line-y'
			});
		}
	}
	
	$scope.addSquareToLastPoint = function() {
		var l = $scope.opp.chartPoints.length;
		
		if (l > 0) {
			if (l < 100) {
				var l2 = l;
			} else {
				var l2 = 100;
			}
			
			for (var i=l-l2; i<l; i++) {
				if ($scope.opp.chartPoints[i].id.substring(0, 6) != 'point_') {
					$scope.opp.chartPoints[i].marker = {enabled: false};
					if ($scope.opp.chartPoints[i].id == 'lastPoint') {
						$scope.opp.chartPoints[i].id = '';
					}
				}
			}
		}
		
		var point = last_point;
		if ($scope.opp.investments.length > 0) {
			point = jsonClone(last_point, last_point_with_inv);
		}
		
		$scope.opp.chartPoints[l-1].marker = point;
		
		if ($scope.opp.chartPoints[l-1].id == "") {
			$scope.opp.chartPoints[l-1].id = 'lastPoint';
		}
	}
	
	$scope.setLevelStatus = function(current, target) {
		var temp = {};
		if (current > target) {
			temp = {//up (above)
				position: true,
				className: 'call',
				text: $rootScope.getMsgs('above')
			};
		} else if (current < target) {
			temp = {//down (below)
				position: false,
				className: 'put',
				text: $rootScope.getMsgs('below')
			};
		} else {
			temp = {//nada
				position: false,
				className: '',
				text: ''
			};
		}
		if (current == target) {
			temp.same = true;
		} else {
			temp.same = false;
		}
		return temp;
	}
	
	$scope.handleElements = function(params) {
		if (!isUndefined(params.state)) {
			$scope.c.state = params.state;
		}
		
		if (!isUndefined(params.popUpState)) {
			$scope.c.popUpState = params.popUpState;
		}
		
		if (($scope.c.popUpState == 'login' || $scope.c.popUpState == 'no-money') && params.state == 'open') {
			$scope.c.showPopUp = true;
		} else if (!isUndefined(params.showPopUp)) {
			$scope.c.showPopUp = params.showPopUp;
		}
		//if it's not specified - hide overlay
		if (!isUndefined(params.showOverlay)) {
			$scope.c.showOverlay = params.showOverlay;
			if (params.showOverlay) {
				$scope.c.expRows = {//reset all rows
					level: false,
					total: false,
					returnAmount: false,
					amount: false,
					profit: false,
					ifCorrect: false,
					ifIncorrect: false
				}
				
				if (params.state == 'expired') {
					$scope.c.eventLevelStatus = $scope.c.levelStatus;
					if ($scope.opp.investments.length == 0) {//option-expired-no
						$scope.c.overlayClass = 'option-expired-no';
						$scope.c.expRows.level = true;
					} else {
						if ($scope.c.eventLevelStatus.position) {
							$scope.c.total_return = $scope.opp.ls.total_return_buy;
						} else {
							$scope.c.total_return = $scope.opp.ls.total_return_sell;
						}
						
						$scope.c.win = ($scope.c.total_return > $scope.opp.ls.total_inv);
						if ($scope.c.eventLevelStatus.same) {
							$scope.c.win = false;
						}
						
						if ($scope.c.win) {
							$scope.c.overlayClass = 'option-expired-in';
							$scope.c.profit = formatAmount($scope.c.total_return - $scope.opp.ls.total_inv, 2);
						} else {
							$scope.c.overlayClass = 'option-expired-out';
						}
						$scope.c.total_returnDsp = formatAmount($scope.c.total_return, 2);
						
						$scope.c.expRows.level = true;
						$scope.c.expRows.total = true;
						$scope.c.expRows.returnAmount = true;
					}
				} else if (params.overlayState == 'invest') {
					$scope.c.overlayClass = 'option-purchased';
					$scope.c.expRows.amount = true;
					$scope.c.expRows.profit = true;
					$scope.c.expRows.ifCorrect = true;
					$scope.c.expRows.ifIncorrect = true;
				} else {
					if (params.overlayState == 'cancel-inv') {
						$scope.startCancelTimeout();
					}
					$scope.c.overlayClass = params.overlayState;
				}
			} else {
				$scope.c.overlayClass = '';
			}
		}
	}
	
	$scope.updateState = function() {
		//state 1,8,9,10,11,12 and opp is not closed
		if ($scope.opp.ls[SUBSCR_FLD.bz_state] == OPPORTUNITY_STATE.created || 
				$scope.opp.ls[SUBSCR_FLD.bz_state] == OPPORTUNITY_STATE.paused || 
				$scope.opp.ls[SUBSCR_FLD.bz_state] == OPPORTUNITY_STATE.waiting_to_pause || 
				$scope.opp.ls[SUBSCR_FLD.bz_state] == OPPORTUNITY_STATE.suspended || 
				$scope.opp.ls[SUBSCR_FLD.bz_state] == OPPORTUNITY_STATE.hidden_waiting_to_expiry || 
				$scope.opp.ls[SUBSCR_FLD.ao_opp_state] == 1) {
			if (!isUndefined($scope.opp.ls[SUBSCR_FLD.bz_suspended_message])) {
				$scope.c.suspend_msgs = parseSuspendMsgs($scope.opp.ls[SUBSCR_FLD.bz_suspended_message]);
			} else {
				$scope.c.available_at = formatDate($scope.opp.ls[SUBSCR_FLD.bz_est_close], false);
				$scope.c.suspend_msgs = $rootScope.getMsgs('available_at', {expTime: $scope.c.available_at})
			}
			$scope.handleElements({
				showPopUp: true,
				popUpState: 'off-hours'
			});
		} else if ($scope.opp.ls[SUBSCR_FLD.bz_state] >= OPPORTUNITY_STATE.opened && $scope.opp.ls[SUBSCR_FLD.bz_state] <= OPPORTUNITY_STATE.closed) {//between state 2 and 6
			if ($scope.opp.ls[SUBSCR_FLD.bz_state] == OPPORTUNITY_STATE.opened || 
					$scope.opp.ls[SUBSCR_FLD.bz_state] == OPPORTUNITY_STATE.last_10_min){//state 2,3 - open
					$scope.handleElements({
						showPopUp: false,
						state: 'open'
					})
			} else if ($scope.opp.ls[SUBSCR_FLD.bz_state] == OPPORTUNITY_STATE.closing_1_min || $scope.opp.ls[SUBSCR_FLD.bz_state] == OPPORTUNITY_STATE.closing){//state 4,5 - waiting for expiry
				$scope.handleElements({
					state: 'waiting',
					showPopUp: true,
					popUpState: 'waiting'
				});
			} else {//state 6 - expired
				$scope.handleElements({
					state: 'expired',
					showOverlay: true,
					showPopUp: false,
				})
			}
		} else {//12
			$scope.handleElements({
				state: 'updating-prices',
				showPopUp: true,
				popUpState: 'updating-prices'
			});
		}
	}

	$scope.insertInvestment = function(params) {//dir (true(up)/false(down))
		if ($scope.c.state != 'open' || $scope.c.submitted) {//only invest in state 2,3 (open) or not submitted
			return;
		} else if (!settings.userLogin) {
			$scope.handleElements({
				showPopUp: true,
				popUpState: 'login'
			});
			return;
		} 
		// else if ($scope.c.invAmount == 0) {//TODO check if balance is enough 
			// $scope.handleElements({
				// showPopUp: true,
				// popUpState: 'no-money'
			// });
			// return;
		// }
		
		shouldShowRegulationPopup('dynamics call or put', function() {
			$rootScope.user.defaultAmount = $scope.c.invAmount;
			$rootScope.user.defaultAmountDsp = formatAmount($rootScope.user.defaultAmount, 0);
		
			$scope.c.submitted = true;
			$scope.c.submitDir = (params.dir) ? 7 : 8;
			$rootScope.inProgress = true;
			var InsertInvestmentMethodRequest = jsonClone(getUserMethodRequest(), {
				opportunityId: $scope.opp.oppId,
				requestAmount: $scope.c.invAmount,
				pageLevel: $scope.opp.ls[SUBSCR_FLD.bz_current],
				price: (params.dir) ? $scope.opp.ls[SUBSCR_FLD.bz_offer] : $scope.opp.ls[SUBSCR_FLD.bz_bid],//offer or bid from ls bz_offer, bg_bid
				choice: (params.dir) ? 7 : 8,
				isFromGraph: false,
				ipAddress: '',
				apiExternalUserReference: '',
				defaultAmountValue: $scope.c.invAmount*100
			});
			
			$http.post(settings.jsonLinkNew + 'insertDynamicsInvestment', InsertInvestmentMethodRequest)
				.then(function(data) {
					data = data.data;
					// if (!$rootScope.handleErrors(data, 'insertDynamicsInvestment')) {}//TODO
					if ($scope.handleErrorsInvest(data)) {
						$scope.investment = data.investment;
						refreshHeader();
						$timeout.cancel($scope.c.investOverlayTimer);
						
						var percent = data.investment.price;//offer or bid
						var profit = getProfit({
							dir: (data.investment.typeId == 7) ? true : false,
							percent: percent,
							amount: amountToFloat(data.investment.amount)
						});
						$scope.c.lastInvestment = {
							invId: data.investment.id,
							amount: formatAmount(data.investment.amount, 2, true),
							cancelCommission: formatAmount(Math.floor((data.investment.amount * 0.1))/100, 2),
							profitPercent: (data.investment.typeId == 7) ? calculateOffer(percent) : calculateBid(percent),
							if_correct: formatAmount(profit + amountToFloat(data.investment.amount), 2),
							if_incorect: formatAmount(0, 2)
						}
						
						$scope.c.eventLevelStatus = {
							position: (data.investment.typeId == 7) ? true : false,
							className: (data.investment.typeId == 7) ? 'call' : 'put',
							text: (data.investment.typeId == 7) ? $rootScope.getMsgs('above') : $rootScope.getMsgs('below')
						}
	
						if (!showCnacelInv) {
							$scope.handleElements({
								overlayState: 'invest',
								showOverlay: true
							});
							
							$scope.addInvToChart();
							
							$scope.c.investOverlayTimer = $timeout(function() {
								$scope.handleElements({
									showOverlay: false
								});
							}, 10000);
						} else {
							$scope.handleElements({
								overlayState: 'cancel-inv',
								showOverlay: true
							});
							$scope.cancelInvTimer = $timeout(function() {
								$scope.handleElements({
									overlayState: 'invest',
									showOverlay: true
								});
								
								$scope.addInvToChart()
								
								$scope.c.investOverlayTimer = $timeout(function() {
									$scope.handleElements({
										showOverlay: false
									});
								}, 10000);
							}, cancelInvTimeOut * 1000);
						}
					} else {
						$scope.c.submitted = false;
						$scope.c.submitDir = 0;
						$rootScope.inProgress = false;
					}
				})
				.catch(function(data, status, headers, config) {
					$rootScope.handleNetworkError(data);
					$scope.c.submitted = false;
					$scope.c.submitDir = 0;
					$rootScope.inProgress = false;
				})
		});
	}
	
	$scope.addInvToChart = function() {
		$scope.investment.timeCreated = adjustFromUTCToLocal(new Date($scope.investment.timeCreated));
		
		$scope.investment = $scope.calculateInvProfit($scope.investment, $scope.c.marketId, $scope.c.currentOppArrayIndex);
		
		$scope.marketsInfo[$scope.c.marketId].opps[$scope.c.currentOppArrayIndex].investments.push($scope.investment);
		
		$scope.calculateInvestmentsSell($scope.c.marketId, $scope.c.currentOppArrayIndex);
		
		//add investment to the chart
		$scope.addPointToChart(
			$scope.c.marketId, 
			$scope.c.currentOppArrayIndex, 
			$scope.investment.timeCreated.getTime() + $rootScope.UTCOffset, 
			parseFloat($scope.investment.level.replace(/,/g,'').replace(/ /g,'')),
			'point_' + $scope.investment.id,
			invest_point,
			'',
			'',
			''
		);
		$scope.chartColors();
		$scope.redrowChart();
		
		$scope.c.submitted = false;
		$scope.c.submitDir = 0;
		$rootScope.inProgress = false;
		
		angular.element($('#DynamicsOpenMarkets')).scope().selectOpenOption($scope.marketsInfo[$scope.c.marketId]);
	}
	
	$scope.startCancelTimeout = function() {
		var cancelTimeOut = cancelInvTimeOut * 1000;
		var end = new Date().getTime() + (cancelTimeOut);
		$scope.cancelTimeLeft = cancelTimeOut;
		function doIt(){
			$scope.cancelTimeLeftPercent = 100 - ($scope.cancelTimeLeft / cancelTimeOut) * 100;
			if ($scope.cancelTimeLeftPercent > 100) {$scope.cancelTimeLeftPercent = 100;}
			
			if ($scope.cancelTimeLeft > 0) {
				$scope.cancelTimeLeft = end - new Date().getTime();
				$scope.cancelInvTimerLoader = $timeout(doIt, 10);
			}
			var seconds = Math.floor($scope.cancelTimeLeft / 1000);
			if (seconds == 0) {
				seconds = '00';
			} else if (seconds < 10) {
				seconds = '0' + seconds;
			}
			var milSec = Math.round($scope.cancelTimeLeft - (seconds * 1000));
			if (milSec == 0) {
				milSec = '000';
			} else if (milSec < 10) {
				milSec = '00' + milSec;
			} else if (milSec < 100) {
				milSec = '0' + milSec;
			}
			$scope.cancelTimeLeftDsp = seconds;
			// $scope.cancelTimeLeftDsp = seconds + ':' + milSec;
		}
		doIt();
	}
	
	$scope.cancelInvOneTime = false;
	$scope.cancelInv = function() {
		if (!$scope.cancelInvOneTime) {
			$scope.cancelInvOneTime = true;
			
			$timeout.cancel($scope.cancelInvTimerLoader);
			$timeout.cancel($scope.cancelInvTimer);
			var InvestmentMethodResult = jsonClone(getUserMethodRequest(), {
				investmentId: $scope.c.lastInvestment.invId
			});
			$http.post(settings.jsonLinkNew + 'cancelInvestment', InvestmentMethodResult)
				.then(function(data) {
					data = data.data;
					if (!$rootScope.handleErrors(data, 'cancelInvestment')) {
						$scope.handleElements({
							overlayState: 'cancel-inv-msg',
							showOverlay: true
						});
						refreshHeader();
						$scope.c.cancelInvMsgsOverlayTimer = $timeout(function() {
							$scope.hideCancelInvMsg();
						}, 3000);
						$scope.c.submitted = false;
						$scope.c.submitDir = 0;
						$rootScope.inProgress = false;
					} else {
						$scope.c.submitted = false;
						$scope.c.submitDir = 0;
						$rootScope.inProgress = false;
						$scope.cancelInvOneTime = false;
						$scope.handleElements({
							overlayState: 'invest',
							showOverlay: true
						});
						
						$scope.addInvToChart();
						
						$scope.c.investOverlayTimer = $timeout(function() {
							$scope.handleElements({
								showOverlay: false
							});
						}, 10000);
					}
					$scope.cancelInvOneTime = false;
				})
				.catch(function(data) {
					data = data.data;
					$rootScope.handleNetworkError(data);
					$scope.c.submitted = false;
					$scope.c.submitDir = 0;
					$rootScope.inProgress = false;
					$scope.cancelInvOneTime = false;
					$scope.handleElements({
						overlayState: 'invest',
						showOverlay: true
					});
					
					$scope.addInvToChart();
					
					$scope.c.investOverlayTimer = $timeout(function() {
						$scope.handleElements({
							showOverlay: false
						});
					}, 10000);
				})
		}
	}
	
	$scope.hideCancelInvMsg = function() {
		$scope.handleElements({
			showOverlay: false
		});
	}
	
	$scope.disableCanacelInv = function() {
		var cancelInvMethodRequest = jsonClone(getUserMethodRequest(), {});
		$http.post(settings.jsonLinkNew + 'changeCancelInvestmentCheckboxState', cancelInvMethodRequest)
			.then(function(data) {
				data = data.data;
				if ($scope.handleErrorsInvest(data)) {
					showCnacelInv = false;
				}
			})
			.catch(function(data) {
				data = data.data;
				$rootScope.handleNetworkError(data);
			})
	}
	
	$scope.handleErrorsInvest = function(data) {
		if (data == '' || data.errorCode == null) {
			alert("Something's wrong :)");
			return false;
		} else if (data.errorCode > errorCodeMap.success) {
			if (data.errorCode == errorCodeMap.session_expired) {
				$scope.handleElements({
					showPopUp: true,
					popUpState: 'login'
				});
				return false;
			} else if (data.errorCode == errorCodeMap.validation_low_balance) {
				$scope.handleElements({
					showPopUp: true,
					popUpState: 'no-money'
				});
			} else {
				$timeout.cancel($scope.c.generalErrorTimer);
				$scope.c.generalError = true;
				var msg = '';
				if (!isUndefined(data.userMessages)) {
					for (var i = 0; i < data.userMessages.length; i++) {
						msg += "<span>" + data.userMessages[i].message + "</span>";
					}
				} else {
					logIt({'type':3,'msg':'unhandled errorCode: ' + data.errorCode});
					msg += "<span>Error: " + data.errorCode + "</span>";
				}
				$scope.c.generalErrorMsg = msg;
				$scope.c.generalErrorTimer = $timeout(function() {
					$scope.c.generalError = false;
				},5000);
			}
		} else {
			return true;
		}
	}
	
	$scope.displayBand = function(params) {//dir (true(up)/false(down))
		if (params.hide) {
			$scope.c.bandClass = '';
		} else if (params.dir) {
			$scope.c.bandClass = 'call';
		} else {
			$scope.c.bandClass = 'put';
		}
	}
	
	$scope.showChart = function() {
		$scope.c.showChart = !$scope.c.showChart;
	}

	$scope.wt_open = function() {
		$scope.walkthroughClass = 'step-1';
		$scope.walkthroughStep = 1;
		$scope.wt_text = $rootScope.getMsgs('dynamics_Walkthrough_t1');
	}
	
	$scope.wt_next = function() {
		$scope.walkthroughStep++;
		if ($scope.walkthroughStep <= 5) {
			$scope.walkthroughClass = 'step-' + $scope.walkthroughStep;
			$scope.wt_text = $rootScope.getMsgs('dynamics_Walkthrough_t' + $scope.walkthroughStep);
		} else {
			$scope.wt_close();
		}
	}
	
	$scope.wt_close = function() {
		$scope.walkthroughClass = '';
		$scope.walkthroughStep = 0;
	}
	
	var hasWTshowt = localStorage.getItem("dynamicsWT");
	if (!hasWTshowt) {
		$scope.wt_open();
		localStorage.setItem("dynamicsWT", true);
	}
	
	waitForIt();
}]);

aoApp.controller('MarketListController', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	function waitForIt() {
		if (isUndefined($rootScope.$state.current.needLogin)) {
			setTimeout(function(){waitForIt()},50);
		} else {
			$scope.changeMarketsTab('time-left');
		}
	}
	
	$scope.changeMarketsTab = function(tab) {
		$scope.tab = tab;
		
		if (tab == 'time-left') {
			$scope.filterAll = {info:{active:true}};
			$scope.order = 'opps[info.currentOppArrayIndex].timeLeft.seconds';
		} else {
			$scope.filterAll = {};
			$scope.order = 'info.marketName';
		}
	}
	
	waitForIt();
}]);

aoApp.controller('DynamicsOpenMarketsController', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.marketId = null;
	
	$scope.filterAll = {info:{hasInvs:true}};
	$scope.order = 'info.marketName';
	
	function waitForIt() {
		if (isUndefined($rootScope.$state.current.needLogin)) {
			setTimeout(function(){waitForIt()},50);
		} else {
			//get first market
			if (isUndefined(Object.keys($scope.marketsInfo)[0])) {
				setTimeout(function(){waitForIt()},50);
			} else {
				$scope.marketId = Object.keys($scope.marketsInfo)[0];
			}
		}
	}
	
	$scope.passParams = function(params) {
		$scope.chartSettings = params;
	}
	
	$scope.selectOpenOption = function(market) {
		$scope.marketId = market.info.marketId;
	}
	
	waitForIt();
}]);