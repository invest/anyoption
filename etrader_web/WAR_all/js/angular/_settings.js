// 'use strict';
var logIt_type = 3;
var settings = {
	domain: window.location.host,
	jsonLink: '/jsonService' + aoJsonUrl,
	jsonLinkNew: context_path + aoJsonUrl,
	commonServiceLink: context_path + commonService,
	msgsPath: 'msgs/',
	msgsFileName: 'MessageResources_',
	msgsEUsufix: '_EU',
	msgsExtension: '.json',
	protocol: window.location.href.split('/')[0],
	writerId: 1,
	skinId: skinId,
	skinIdTexts: 2,
	skinLanguage: 'en',
	timeFirstVisit: getCookie('timeFirstVisit'),
	avatarSizeLimit: 1,//MB
	documentSizeLimit: 0.7,//MB
	loggedIn: false,
	/*fbAppId: 832674160084280,*/
	fbAppScope: 'public_profile,email,user_friends',
	numOfMaleAvatars: 100,
	numOfFemaleAvatars: 50,
	serverTime: serverTime,
	serverOffsetMillis: serverTime - new Date().getTime(),
	dstOffset: 0,
	optionsLimit: 4,
	resultsOnPage: 15,
	maxAssetSelected: 10,
	shortYearFormat: new Date().getFullYear().toString().slice(-2),
	defaultLanguage: 16,
	loginDetected: false,
	url_params: window.location.search,
	// shareLink: 'https://www.copyop.com',
	sessionTimeout: (1000*60*31),//31 min
	// appsflyerios: '//www.copyop.com/appsflyerios',
	// appsflyerandroid: '//www.copyop.com/appsflyerandroid',
	recaptchaKey: '6Lf_jA0TAAAAANleUF7cUhAEcSCb3etKQg4ak5lU',
	isLive: isLive,
	userLogin: userLogin,
	subscriptionType: 'COMMAND',
	dataAdapterName: 'JMS_ADAPTER',
	schemaDynamics: [
		SUBSCR_FLD.key, 
		SUBSCR_FLD.command, 
		SUBSCR_FLD.bz_offer, 
		SUBSCR_FLD.bz_bid, 
		SUBSCR_FLD.bz_state, 
		SUBSCR_FLD.bz_current, //level
		SUBSCR_FLD.time_stamp, 
		SUBSCR_FLD.bz_suspended_message,
		SUBSCR_FLD.ao_opp_state,
		SUBSCR_FLD.bz_event,
		SUBSCR_FLD.bz_last_inv,
		SUBSCR_FLD.bz_est_close,
		SUBSCR_FLD.bz_market_id,
		SUBSCR_FLD.ask,
		SUBSCR_FLD.bid,
		SUBSCR_FLD.last
	]
}
settings.domain_parts = settings.domain.split('.');
settings.shortYear = Number(settings.shortYearFormat);
settings.shortYearFuture = settings.shortYear + 20;

if (settings.timeFirstVisit == null) {
	var m = new Date();
	var dateString = (m.getUTCMonth()+1) + ' ' + m.getUTCDate() + ',' + m.getUTCFullYear() + ' ' + m.getUTCHours() + ':' +m.getUTCMinutes() + ':' + m.getUTCSeconds();
	//setCookie('timeFirstVisit', dateString, 365);
}

var utcOffset = new Date().getTimezoneOffset();
var jsonObjects = {}
jsonObjects.methodRequest = {
	utcOffset: utcOffset, 
	writerId: settings.writerId, 
	locale: ''
}
function getMethodRequest() {
	return jsonClone(jsonObjects.methodRequest, {skinId: settings.skinId});
}
try {
	jsonObjects.userMethodRequest = {
		encrypt: false, 
		isLogin: false, 
		afterDeposit: false,
		// fingerPrint: new Fingerprint().get()
	}
} catch (e){}
function getUserMethodRequest() {
	return jsonClone(jsonObjects.userMethodRequest, getMethodRequest());
}
function getProfileMethodRequest(params) {
	return jsonClone(getUserMethodRequest(), {requestedUserId: params.userId, resetPaging: params.resetPaging});
}
try {
	jsonObjects.insertUserMethodRequest = {//combines ao and copyop InsertUserMethodRequest method
		register: {
			// timeFirstVisit: settings.timeFirstVisit,
			timeFirstVisit: 'Oct 28, 2014 9:46:38 AM',
			calcalistGame: false,
			skinId: 1,
			stateId: 0,
			languageId: 0,
			currencyId: 0,
			// registerAttemptId: settings.registerAttemptId,
			contactId: 0,
			cityId: 0,
			contactByEmail: true,
			contactBySms: true,
			terms: true,
			deviceUniqueId: ''
		},
		marketingStaticPart: '',
		mId: '',
		etsMId: '',
		utmSource: '',
		avatar: '',
		avatarDsp: '',
		fbId: '',
		sendMail: false,
		sendSms: false,
		isBaidu: false,
		// fingerPrint: new Fingerprint().get()
	}
} catch (e){}
function getInsertUserMethodRequest() {
	return jsonClone(jsonObjects.insertUserMethodRequest, getMethodRequest());
}
var payment_type = {
	direct24: 2,//PAYMENT_TYPE_DIRECT24
	giropay: 3,//PAYMENT_TYPE_GIROPAY
	eps: 4//PAYMENT_TYPE_EPS
}
var oldTexts = {}//TODO refactor
if(typeof window.returnRefund_all == 'undefined'){
	var returnRefund_all = [];
}
var defaultMarkets = [];//TODO refactor!

var skinMap = {
	'1': {
		subdomain:'www',
		locale: 'iw',
		isRgulated: false,
		languageId: 1,
		skinSelector: false,
		isAllowed: false,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-Zא-ת ]+$"
	},
	'2': {
		subdomain:'www',
		locale: 'en',
		isRgulated: false,
		rgulatedSkinId: 16,
		languageId: 2,
		isAllowed: true,
		skinSelector: true,
		skinIdTexts: 2,
		regEx_letters: "^['a-zA-Z ]+$"
	},
	'3': {
		subdomain:'tr',
		locale: 'tr',
		isRgulated: false,
		languageId: 3,
		skinSelector: false,
		isAllowed: false,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-ZİıÖöÜüÇçĞğŞş ]+$"
	},
	'4': {
		subdomain:'ar',
		locale: 'ar',
		isRgulated: false,
		languageId: 4,
		skinSelector: false,
		isAllowed: false,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-Z ]+$"
	},
	'5': {
		subdomain:'es',
		locale: 'es',
		isRgulated: false,
		rgulatedSkinId: 18,
		languageId: 5,
		isAllowed: true,
		skinSelector: true,
		skinIdTexts: 5,
		regEx_letters: "^['a-zA-ZÁáÉéÍíÑñÓóÚúÜü ]+$"
	},
	'8': {
		subdomain:'de',
		locale: 'de',
		isRgulated: false,
		rgulatedSkinId: 19,
		languageId: 8,
		isAllowed: true,
		skinSelector: true,
		skinIdTexts: 8,
		regEx_letters: "^['a-zA-ZäÄöÖüÜß ]+$"
	},
	'9': {
		subdomain:'it',
		locale: 'it',
		isRgulated: false,
		rgulatedSkinId: 20,
		languageId: 9,
		isAllowed: true,
		skinSelector: true,
		skinIdTexts: 9,
		regEx_letters: "^['a-zA-ZÀàÁáÈèÉéÌìÍíÒòÓóÙùÚú ]+$"
	},
	'10': {
		subdomain:'ru',
		locale: 'ru',
		isRgulated: false,
		languageId: 10,
		skinSelector: false,
		isAllowed: false,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-ZА-Яа-яЁё ]+$"
	},
	'12': {
		subdomain:'fr',
		locale: 'fr',
		isRgulated: false,
		rgulatedSkinId: 21,
		languageId: 12,
		isAllowed: true,
		skinSelector: true,
		skinIdTexts: 12,
		regEx_letters: "^['a-zA-Zéèàùâêîôûëïç ]+$"
	},
	'13': {
		subdomain:'en-us',
		locale: 'en-us',
		isRgulated: false,
		languageId: 2,
		isAllowed: true,
		skinSelector: false,
		skinIdTexts: 13,
		regEx_letters: "^['a-zA-Z ]+$"
	},
	'14': {
		subdomain:'es-us',
		locale: 'es-us',
		isRgulated: false,
		languageId: 5,
		isAllowed: true,
		skinSelector: false,
		skinIdTexts: 14,
		regEx_letters: "^['a-zA-ZÁáÉéÍíÑñÓóÚúÜü ]+$"
	},
	'15': {
		subdomain:'sg',
		locale: 'sg',
		isRgulated: false,
		languageId: 15,
		skinSelector: false,
		isAllowed: false,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-Z一-龠 ]+$"
	},
	'16': {
		subdomain:'www',
		locale: 'en',
		isRgulated: true,
		languageId: 2,
		isAllowed: true,
		skinSelector: false,
		skinIdTexts: 16,
		regEx_letters: "^['a-zA-Z ]+$"
	},
	'17': {
		subdomain:'kr',
		locale: 'kr',
		isRgulated: false,
		languageId: 17,
		skinSelector: false,
		isAllowed: false,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-Z\u1100-\u11FF\uAC00-\uD7AF\u3130-\u318F ]+$"
	},
	'18': {
		subdomain:'es',
		locale: 'es',
		isRgulated: true,
		languageId: 5,
		isAllowed: true,
		skinSelector: false,
		skinIdTexts: 18,
		regEx_letters: "^['a-zA-ZÁáÉéÍíÑñÓóÚúÜü ]+$"
	},
	'19': {
		subdomain:'de',
		locale: 'de',
		isRgulated: true,
		languageId: 8,
		isAllowed: true,
		skinSelector: false,
		skinIdTexts: 19,
		regEx_letters: "^['a-zA-ZäÄöÖüÜß ]+$"
	},
	'20': {
		subdomain:'www1',
		locale: 'it',
		isRgulated: true,
		languageId: 9,
		isAllowed: true,
		skinSelector: false,
		skinIdTexts: 20,
		regEx_letters: "^['a-zA-ZÀàÁáÈèÉéÌìÍíÒòÓóÙùÚú ]+$"
	},
	'21': {
		subdomain:'fr',
		locale: 'fr',
		isRgulated: true,
		languageId: 12,
		isAllowed: true,
		skinSelector: false,
		skinIdTexts: 21,
		regEx_letters: "^['a-zA-Zéèàùâêîôûëïç ]+$"
	},
	'22': {
		subdomain:'hk',
		locale: 'hk',
		isRgulated: false,
		languageId: 15,
		skinSelector: false,
		isAllowed: false,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-Z一-龠 ]+$"
	},
	'23': {
		subdomain:'nl',
		locale: 'nl',
		isRgulated: true,
		defaultSkinId: 23,
		languageId: 23,
		skinSelector: false,
		isAllowed: false,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-Zéëï ]+$"
	},
	'24': {
		subdomain:'se',
		locale: 'sv',
		isRgulated: true,
		defaultSkinId: 24,
		languageId: 24,
		skinSelector: false,
		isAllowed: false,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-ZÅÄÖåäö ]+$"
	},
	'25': {
		subdomain:'www',
		locale: 'en',
		isRgulated: false,
		rgulatedSkinId: 16,
		defaultSkinId: 16,
		languageId: 25,
		isAllowed: true,
		skinSelector: true,
		skinIdTexts: 2,
		regEx_letters: "^['a-zA-Z ]+$"
	},
	'26': {
		subdomain:'es',
		locale: 'es',
		isRgulated: false,
		rgulatedSkinId: 18,
		defaultSkinId: 18,
		languageId: 26,
		isAllowed: true,
		skinSelector: true,
		skinIdTexts: 5,
	},
	'27': {
		subdomain:'cs',
		locale: 'cs',
		isRgulated: true,
		defaultSkinId: 27,
		languageId: 27,
		isAllowed: true,
		skinSelector: true,
		skinIdTexts: 27,
		regEx_letters: "^['a-zA-Z ]+$"
	},
	'28': {
		subdomain:'pl',
		locale: 'pl',
		isRgulated: true,
		defaultSkinId: 28,
		languageId: 28,
		isAllowed: true,
		skinSelector: true,
		skinIdTexts: 28,
		regEx_letters: "^['a-zA-Z ]+$"
	},
	'29': {
		subdomain:'pt',
		locale: 'pt',
		isRgulated: true,
		defaultSkinId: 29,
		languageId: 29,
		isAllowed: true,
		skinSelector: true,
		skinIdTexts: 29,
		regEx_letters: "^['a-zA-Z ]+$"
	},
	'30': {
		subdomain:'ar',
		locale: 'ar',
		isRgulated: false,
		defaultSkinId: 30,
		languageId: 30,
		isAllowed: true,
		skinSelector: true,
		skinIdTexts: 30,
		regEx_letters: "^['a-zA-Z ]+$"
	}
}
var currencyConst = {
	all: 0,//CURRENCY_ALL_ID
	ils: 1,//CURRENCY_ILS_ID
	usd: 2,//CURRENCY_USD_ID
	eur: 3,//CURRENCY_EUR_ID
	gbp: 4,//CURRENCY_GBP_ID
	'try': 5,//CURRENCY_TRY_ID
	rub: 6,//CURRENCY_RUB_ID
	cny: 7,//CURRENCY_CNY_ID
	kr: 8,//CURRENCY_KR_ID
	sek: 9,//CURRENCY_sek_ID
	aud: 10,//CURRENCY_aud_ID
	zar: 11,//CURRENCY_zar_ID
	czk: 12,
	pln: 13,
}
var currencyMap = {
	'0': {
		shortName: 'all',
	},
	'1': {
		shortName: 'ils',
	},
	'2': {
		shortName: 'usd',
	},
	'3': {
		shortName: 'eur',
	},
	'4': {
		shortName: 'gbp',
	},
	'5': {
		shortName: 'try',
	},
	'6': {
		shortName: 'rub',
	},
	'7': {
		shortName: 'krw',
	},
	'8': {
		shortName: 'cny',
	},
	'9': {
		shortName: 'sek',
	},
	'10': {
		shortName: 'aud',
	},
	'11': {
		shortName: 'zar',
	},
	'12': {
		shortName: 'czk',
	},
	'13': {
		shortName: 'pln',
	}
}
var errorCodeMap = {
	success: 0,
	withdrawal_close: 110,
	invalid_input: 200,
	invalid_account: 201,
	dup_request: 202,
	missing_param: 203,
	login_failed: 205,
	inv_validation: 206,
	general_validation: 207,
	validation_without_field: 208,
	validation_low_balance: 209,
	transaction_failed: 210,
	cc_not_supported: 211,
	invalid_captch: 212,
	no_cash_for_niou: 213,
	regulation_missing_questionnaire: 300,
	regulation_suspended: 301,
	regulation_suspended_documents: 302,
	user_not_regulated: 303,
	regulation_user_restricted: 304,
	reg_suspended_quest_incorrect: 305,
	regulation_user_pep_prohibited: 307,
	regulation_user_is_treshold_block: 308,

	questionnaire_not_full: 400,
	questionnaire_not_supported: 401,
	questionnaire_already_done: 402,
	session_expired: 419,
	opp_not_exists: 500,
	file_too_large: 600,
	user_removed: 800,
	user_blocked: 801,
	unknown: 999,

	et_regulation_additional_info: 700,
	not_accepted_terms: 701,
	et_regulation_knowledge_confirm: 702,
	et_regulation_capital_questionnaire_not_done: 703,
	et_regulation_suspended_low_score: 704,
	et_regulation_suspended_low_treshold: 705,
	et_regulation_suspended_medium_treshold: 706,

	copyop_max_copiers: 1001,

	bubbles_invalid_signature: 3000
}

var userRegulationBase = {
	regulation_user_registered: 0, 
	regulation_wc_approval: 1,
	regulation_first_deposit: 2,
	regulation_mandatory_questionnaire_done: 3,
	regulation_all_documents_received: 5,
	regulation_all_documents_support_approval: 6,
	regulation_control_approved_user: 7,
	
	et_regulation_knowledge_question_user: 103,
	et_capital_market_questionnaire: 104,
	et_regulation_done: 105,
	
	et_regulation_low_score_group_calc: 9,
	et_regulation_medium_score_group_calc: 17,
	
	et_regulation_unsuspended_score_group_id: 0,
	et_regulation_high_score_group_id: 1,
	et_regulation_medium_score_group_id: 2,
	et_regulation_low_score_group_id: 3,	
	
	ao_regulation_restricted_score_group_id: 4,
	ao_regulation_restricted_low_x_score_group_id: 5,
	ao_regulation_restricted_high_y_score_group_id: 6,

	not_suspended: 0,
	suspended_by_world_check: 1,
	suspended_mandatory_questionnaire_not_filled: 2,
	suspended_due_documents: 3,
	suspended_manually: 4,
	suspended_mandatory_questionnaire_filled_incorrect: 5,

	non_prohibited: 0,
	auto_prohibited: 1,
	writer_prohibited: 2,
	approved_by_compliance: 3,
	false_classification: 4,

	et_suspend_low_score_group: 10,
	et_suspend_treshehold_low_score_group: 11,
	et_suspend_treshehold_medium_score_group: 12,
	
	regulation_user_version: 2, 

	days_from_qualified_time: 30
}

var fileStatus = {
	not_selected: 0,
	requested: 1,
	in_progress: 2,
	done: 3,
	invalid: 4,
	not_needed: 5
}
var fileType = {
	user_id_copy: 1,
	passport: 2,
	cc_copy: 4,
	bankwire_confirmation: 12,
	withdraw_form: 13,
	power_of_attorney: 16,
	account_closing: 17,
	cc_holder_id: 18,
	ssn: 20,
	driver_license: 21,
	utility_bill: 22,
	cc_copy_front: 23,
	cc_copy_back: 24
}

var balanceCallTypes = {
	total: 1,//CALL_TOTAL_AMOUNT_BALANCE
	open_amount: 2,//CALL_OPEN_INVESTMENTS_AMOUNT_BALANCE
	settled_amount: 3,//CALL_SETTLED_INVESTMENTS_AMOUNT_BALANCE
	settled_return: 4//CALL_SETTLED_INVESTMENTS_RETURN_BALANCE
}

var optimoveTid = 'e6d329373bfb6eb36a22036097673bb059c4c348c2e8dd85dcd75de1071072a9';
var optimoveEvents = {
	login: 1,
	pageVisit: 3,
	loss: 5,
	win: 6,
	deposit: 7,
	withdraw: 8,
	trade: 9
}
var opportunityTypeId = {
	dynamics: 7
}

//regExp
var regEx_lettersAndNumbers = /^[0-9a-zA-Z]+$/;
var regEx_nickname = /^[a-zA-Z0-9!@#$%\^:;"']{1,11}$/;
var regEx_nickname_reverse = /[^a-zA-Z0-9!@#$%\^:;"']/g;
var regEx_phone  = /^\d{7,20}$/;
var regEx_digits = /^\d+$/;
var regEx_digits_reverse  = /\D/g;
var regEx_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var regEx_englishLettersOnly = /^['a-zA-Z ]+$/;
var regEx_lettersOnly = "^['\\p{L} ]+$";




var chart_properties = {
	height:								310,
	popUp_fsize1:						'11px',
	popUp_fsize2:						'11px',
	popUp_fsize3:						'9px',
	chart_lineColor:					'#5b8dab',
	tooltip_borderColor:				'#156c96',
	tooltip_bgrColor:					'#fafdff',
	tooltip_timeTxtColor:				'#04618e',
	tooltip_assetTxtColor:				'#e74310',
	tooltip_levelTxtColor:				'#04618e',
	timeLineTxtColor:					'#167cad',
	timeLineTxtColor_with_inv:			'#fff',
	timeLineTxtPosition:				-10,
	plotLinesColor:						'#4f86a5',
	plotLinesColor_with_inv:			'#fff'
};
var chart_properties_with_inv = {
	tooltip_borderColor:				'#99bdd1',
	tooltip_bgrColor:					'#0d3b56',
	tooltip_timeTxtColor:				'#fff',
	tooltip_assetTxtColor:				'#fabd00',
	tooltip_levelTxtColor:				'#fff'
}

var chart_properties_small = {
	height:								170,
	timeLineTxtPosition:				-5,
}

var last_point = {
	enabled: true,
	symbol: 'circle',
	fillColor: '#4f86a5',
	lineColor: '#4f86a5',
	radius: 4
};
var last_point_with_inv = {
	fillColor: '#fff',
	lineColor: '#fff'
}
var invest_point = {
	enabled: true,
	symbol: 'circle',
	fillColor: '#fff',
	lineColor: '#fff',
	radius: 3
}

var replaceParamsGT = {
	'${assets}': '<div ng-include="\'/asset-table.html\'" ng-controller="AssetIndexController"></div>',
	'${index_page}': homeFullLink,
	'${privacy_link}': privacyFullLink,
	'${complaint_form}': image_context_path_clean + '/pdf/complaint-form/complaint-form-{locale}.pdf'
}