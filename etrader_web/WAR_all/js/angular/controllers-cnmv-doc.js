aoApp.controller('CnmvDocCtrl', ['$rootScope', '$scope', '$http', '$timeout', '$upload', function($rootScope, $scope, $http, $timeout, $upload) {
	$scope.ready = true;
	$scope.isSubmitting = false;
	$scope.isUploaded = false;
	$scope.cnmvDocType = 38;
	
	$scope.doc = {
		fileTypeId: $scope.cnmvDocType,
		docTitle: ''
	};
	
	$scope.submit = function(){
		if(!$scope.isUploaded){
			return false;
		}
		closeAORegulationStatusPopup();
		shouldShowRegulationPopup("ao_is_knowledge_confirmed", function(){});
		return false;
	}
	
	$scope.usingFlash = FileAPI && FileAPI.upload != null;
	$scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
	$scope.fileTooBig = false;
	
	$scope.abort = function(index) {
		$scope.upload[index].abort(); 
		$scope.upload[index] = null;
	};
	
	$scope.onFileSelect = function($files, docType, doc) {
		$scope.currentDocType = docType;
		$scope.ccId = 0;
		$scope.selectedDoc = doc;
		$scope.selectedDoc.loading = true;
		//Check if the file is too big, alert and return in case it is
		var hasLargerFile = false;
		$scope.fileTooBig = false;
		$scope.fileUploadError = false;
		for ( var i = 0; i < $files.length; i++) {
			if($files[i].size / 1000000  >= settings.documentSizeLimit){
				hasLargerFile = true;
			}
		}
		if(hasLargerFile){
			//alert($rootScope.getMsgs('file-too-big'));
			$scope.selectedDoc.loading = false;
			$scope.fileTooBig = true;
			$timeout(function(){$scope.fileTooBig = false;}, 3000);
			return;
		}
		
		// closeElement({id:'popUp-avatar'});
		$scope.selectedFiles = [];
		$scope.progress = [];
		if ($scope.upload && $scope.upload.length > 0) {
			for (var i = 0; i < $scope.upload.length; i++) {
				if ($scope.upload[i] != null) {
					$scope.upload[i].abort();
				}
			}
		}
		$scope.upload = [];
		$scope.uploadResult = [];
		$scope.selectedFiles = $files;
		$scope.dataUrls = [];
		for ( var i = 0; i < $files.length; i++) {
			var $file = $files[i];
			if ($scope.fileReaderSupported && $file.type.indexOf('image') > -1) {
				var fileReader = new FileReader();
				fileReader.readAsDataURL($files[i]);
				var loadFile = function(fileReader, index) {
					fileReader.onload = function(e) {
						$timeout(function() {
							$scope.dataUrls[index] = e.target.result;
							// if (index == 0) {
								// $rootScope.uploadedImage = $scope.dataUrls[index];
							// }
						});
					}
				}(fileReader, i);
			}
			if ($scope.fileReaderSupported) {
				$scope.uploadedFileName = $file.name;
			}
			$scope.progress[i] = -1;
			$scope.isSubmitting = true;
			$scope.start(i);
		}
	};
	
	$scope.start = function(index) {
		if ($scope.selectedFiles[index].size / 1000000  < settings.documentSizeLimit) {
			$scope.progress[index] = 0;
			$scope.errorMsg = null;
			//$upload.upload()
			 var params = '?';
			 params += 'fileType=' + $scope.currentDocType;
			 params += '&fileName=' + $scope.selectedFiles[index].name;
			 params += '&ccId=' + $scope.ccId;
			 params += '&writerId=' + settings.writerId;
			 params = encodeURI(params);
			$scope.upload[index] = $upload.upload({
				url: context_path + '/UploadDocumentsService' + params,
				method: 'POST',
				file: $scope.selectedFiles[index],
				fileFormDataName: 'myFile',
				data: {
					fileType: $scope.currentDocType,
					fileName: $scope.selectedFiles[index].name,
					ccId: $scope.ccId,
					writerId: settings.writerId
				}
			});
			$scope.upload[index].then(function(response) {
				$timeout(function() {
					$scope.uploadResult.push(response.data);
					$scope.uploadComplete(response.data);
				});
			}, function(response) {
				if (response.status > 0) $scope.$parent.errorMsg = $scope.errorMsg = response.status + ': ' + response.data;
				$scope.selectedDoc.loading = false;
				$scope.isSubmitting = false;
			}, function(evt) {
				// Math.min is to fix IE which reports 200% sometimes
				$scope.$parent.progress = $scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
				// $scope.selectedDoc.loading = false;
			});
			$scope.upload[index].xhr(function(xhr){
	//				xhr.upload.addEventListener('abort', function() {console.log('abort complete')}, false);
			});
		} else {
			alert($rootScope.getMsgs('file-too-big'));
		}
	};
	
	$scope.uploadComplete = function(data) {
		$scope.selectedDoc.loading = false;
		$scope.isSubmitting = false;
		if (!$rootScope.handleErrors(data, 'UploadDocumentsService')) {
			$scope.isUploaded = true;
		} else {
			$scope.fileUploadError = true;
			$timeout(function(){$scope.fileUploadError = false;}, 3000);
		}
	}
	
	if (localStorage) {
		$scope.s3url = localStorage.getItem("s3url");
		$scope.AWSAccessKeyId = localStorage.getItem("AWSAccessKeyId");
		$scope.acl = localStorage.getItem("acl");
		$scope.success_action_redirect = localStorage.getItem("success_action_redirect");
		$scope.policy = localStorage.getItem("policy");
		$scope.signature = localStorage.getItem("signature");
	}
	$scope.success_action_redirect = $scope.success_action_redirect || window.location.protocol + "//" + window.location.host;
	$scope.jsonPolicy = $scope.jsonPolicy || '{\n  "expiration": "2020-01-01T00:00:00Z",\n  "conditions": [\n    {"bucket": "angular-file-upload"},\n    ["starts-with", "$key", ""],\n    {"acl": "private"},\n    ["starts-with", "$Content-Type", ""],\n    ["starts-with", "$filename", ""],\n    ["content-length-range", 0, 524288000]\n  ]\n}';
	$scope.acl = $scope.acl || 'private';
	
}]);