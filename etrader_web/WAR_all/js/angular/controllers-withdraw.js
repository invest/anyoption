aoApp.controller('MyAccountMenuController', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.menuPermissions = {
		withdraw: {}
	}
	function waitForIt() {
		if (!$rootScope.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			$scope.getPermissionsToDisplay();
		}
	}
	
	$scope.getPermissionsToDisplay = function() {
		var GmWithdrawalPermissionToDisplayRequest = jsonClone(getMethodRequest(), {
			userId: userInfo.id
		});
		
		$http.post(settings.commonServiceLink + '/GmWithdrawalServices/getPermissionsToDisplay', GmWithdrawalPermissionToDisplayRequest)
			.then(function(data) {
				data = data.data;
				if (!$rootScope.handleErrors(data, 'getPermissionsToDisplay')) {
					$scope.menuPermissions.withdraw.direct24 = data.displayDirect24;
					$scope.menuPermissions.withdraw.eps = data.displayEPS;
					$scope.menuPermissions.withdraw.giropay = data.displayGiropay;
					$scope.menuPermissions.withdraw.ideal = data.displayIdeal;
				}
			})
			.catch(function(data) {
				$rootScope.handleNetworkError(data);
			})
	}
	waitForIt();
}]);

aoApp.controller('GmWithdrawalController', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.form = {}
	$scope.ready = true;
	$rootScope.topIndex = true;
	$scope.popupState = 0;
	
	if (requestURI.search('withdraw-direct24') > -1) {
		$scope.gmType = 59;
	}  else if (requestURI.search('withdraw-eps') > -1) {
		$scope.gmType = 61;
	}  else if (requestURI.search('withdraw-giropay') > -1) {
		$scope.gmType = 60;
	}  else if (requestURI.search('withdraw-ideal') > -1) {
		$scope.gmType = 63;
	}
	
	function waitForIt() {
		if (!$rootScope.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			
		}
	}
	
	$scope.submitGMwithdraw = function(_form) {
		$scope._form = _form;//because stuff
		if ($scope._form.$valid) {
			shouldShowRegulationPopup('withdraw_submit', function(){
				shouldShowWithdrawResetBonusPopup(function(){
					openWithdrawSurveyPopup();
				}, 2);
			});
			return false;
		} else {
			return false;
		}
	}
	
	$scope.submitWithdrow = function() {
		if ($scope._form.$valid) {
			$scope.GmWithdrawalRequest = jsonClone(getMethodRequest(), {
				beneficiaryName: $scope.form.beneficiaryName,
				swiftBicCode: ($scope.gmType == 61 || $scope.gmType == 63) ? $scope.form.swiftBicCode : null,// EPS, iDeal only
				iban: ($scope.gmType == 61 || $scope.gmType == 63) ? $scope.form.iban : null,//EPS, iDeal only
				accountNumber: ($scope.gmType != 63) ? parseInt($scope.form.accountNumber) : null,
				bankName: ($scope.gmType != 63) ? $scope.form.bankName : null,
				branchNumber: ($scope.gmType != 63) ? parseInt($scope.form.branchNumber) : null,
				branchAddress: ($scope.gmType != 63) ? $scope.form.branchAddress : null,
				amount: parseInt($scope.form.amount),
				gmType: $scope.gmType, // EPS value->61, Giropay value->60, Direct24 value->59, ideal value->63
				userId: userInfo.id,
				answerId: ($scope.form.userAnswerId == 0) ? null : $scope.form.userAnswerId,
				textAnswer: $scope.form.textAnswer
			});
			
			$http.post(settings.commonServiceLink + '/GmWithdrawalServices/validateGMWithdraw', $scope.GmWithdrawalRequest)
				.then(function(data) {
					data = data.data;
					if (!$rootScope.handleErrors(data, 'validateGMWithdraw')) {
						$scope.popupState = 1;
						$scope.textPopOne = $rootScope.getMsgs('withdraw.cc.approve.message.low.amount')
						$scope.textPopOne = $scope.textPopOne.replace('{0}', formatAmount(data.fee, 0, true));
					}
				})
				.catch(function(data) {
					$rootScope.handleNetworkError(data);
				})
		} else {
			logIt({'type':3,'msg':$scope._form.$error});
		}
	}
	
	$scope.insertWithdrawCancel = function() {
		// var InsertWithdrawCancelMethodRequest = jsonClone(getUserMethodRequest(), {
			// transactionTypeId: $scope.gmType
		// });
		
		// $http.post(settings.jsonLinkNew + 'insertWithdrawCancel', InsertWithdrawCancelMethodRequest)
			// .then(function(data) {
				// data = data.data;
				// if (!$rootScope.handleErrors(data, 'insertWithdrawCancel')) {
					$scope.popupState = 2;
					$scope.textPopTwo = $rootScope.getMsgs('withdraw.cancel');
				// }
			// })
			// .catch(function(data) {
				// $rootScope.handleNetworkError(data.data);
			// })
			// .finally(function() {
				// $scope.loading = false;
			// })
	}
	
	$scope.insertWithdrawBank = function() {
		if (!$scope.loading) {
			$scope.loading = true;
			
			$http.post(settings.commonServiceLink + '/GmWithdrawalServices/insertGmWithdraw', $scope.GmWithdrawalRequest)
				.then(function(data) {
					data = data.data;
					if (!$rootScope.handleErrors(data, 'insertWithdrawBank')) {
						$scope.popupState = 2;
						$scope.textPopTwo = $rootScope.getMsgs('bank.wire.success');
						$scope.textPopTwo = $scope.textPopTwo.replace('{0}', formatAmount($scope.form.amount, 0));
					}
				})
				.catch(function(data) {
					$rootScope.handleNetworkError(data.data);
				})
				.finally(function() {
					$scope.loading = false;
				})
		}
	}
	
	$scope.reload = function() {
		location.reload();
	}
	
	waitForIt();
}]);

aoApp.controller('WithdrawUserDetailsController', ['$rootScope', '$scope', '$http', '$timeout', '$upload', function($rootScope, $scope, $http, $timeout, $upload) {
	$scope.form = {
		type: 2
	}
	$scope.ready = true;
	
	function waitForIt() {
		if (!$rootScope.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			$scope.getWithdrawUserDetails();
		}
	}
	
	$scope.getWithdrawUserDetails = function() {
		var WithdrawUserDetailsMethodRequest = jsonClone(getMethodRequest(), {
			withdrawUserDetails: {
				userId: userInfo.id
			}
		});
			
		$http.post(settings.commonServiceLink  + '/WithdrawUserDetailsServices/getWithdrawUserDetails', WithdrawUserDetailsMethodRequest)
			.then(function(data) {
				data = data.data;
				if (!$rootScope.handleErrors(data, 'getWithdrawUserDetails')) {
					$scope.updateId == null;
					if (data.withdrawUserDetails != null) {
						$scope.form = jsonClone($scope.form, data.withdrawUserDetails);
						$scope.updateId = data.withdrawUserDetails.id;
					}
				}
			})
			.catch(function(data) {
				$rootScope.handleNetworkError(data);
			})
	}
	
	$scope.insertUpdateWithdrawUserDetailas = function(_form) {
		if($scope.isSubmitting){
			return false;
		}
		$scope.isSubmitting = true;
		if (_form.$valid) {
			$scope.form.userId = userInfo.id;
			var WithdrawUserDetailsMethodRequest = jsonClone(getMethodRequest(), {
				withdrawUserDetails: $scope.form,
				withdrawUserDetailsType: $scope.form.type
			});
			
			var url = ($scope.updateId != null) ? 'updateWithdrawUserDetils' : 'insertWithdrawUserDetailas';
			$http.post(settings.commonServiceLink  + '/WithdrawUserDetailsServices/'+url, WithdrawUserDetailsMethodRequest)
				.then(function(data) {
					data = data.data;
					if (!$rootScope.handleErrors(data, url)) {
						if ($scope.form.type == 1) {
							if ($scope.selectedFiles.length > 0) {
								$scope.send();
							} else {
								$rootScope.showWithdrawUserDetailsPopup = false;
								$scope.isSubmitting = false;
							}
						} else {
							$rootScope.showWithdrawUserDetailsPopup = false;
							$scope.isSubmitting = false;
						}
					}
				})
				.catch(function(data) {
					$scope.isSubmitting = false;
					$rootScope.handleNetworkError(data);
				})
		} else {
			$scope.isSubmitting = false;
			logIt({'type':3,'msg':$scope._form.$error});
		}
	}
	
	$scope.skipedWithdrawBankUserDetils = function() {
		var WithdrawUserDetailsMethodRequest = jsonClone(getMethodRequest(), {
			withdrawUserDetails: {
				userId: userInfo.id
			}
		});
		
		$http.post(settings.commonServiceLink  + '/WithdrawUserDetailsServices/skipedWithdrawBankUserDetils', WithdrawUserDetailsMethodRequest)
			.then(function(data) {
				data = data.data;
				if (!$rootScope.handleErrors(data, 'skipedWithdrawBankUserDetils')) {
					$rootScope.showWithdrawUserDetailsPopup = false;
					$scope.isSubmitting = false;
				}
			})
			.catch(function(data) {
				$scope.isSubmitting = false;
				$rootScope.handleNetworkError(data);
			})
	}
	
	waitForIt();

	$scope.shortenFileName = function(str){
		var maxLength = 8;
		var dotPos = str.lastIndexOf('.');
		if(dotPos != -1){
			var name = str.substring(0, dotPos);
			var extension = str.substring(dotPos);
			if(name.length > maxLength){
				name = name.substring(0, maxLength) + '...';
			}
			str = name + extension;
		}
		return str;
	}

	//Handle file upload
	$scope.selectedFiles = [];
	$scope.selectedSlideKey = null;
	$scope.progress = [];
	$scope.abort = function(index) {
		$scope.upload[index].abort(); 
		$scope.upload[index] = null;
	};
	
	$scope.onFileSelect = function($files, slideKey) {
		//Check if the file is too big, alert and return in case it is
		var hasLargerFile = false;
		$scope.fileTooBig = false;
		for ( var i = 0; i < $files.length; i++) {
			if($files[i].size / 1000000 >= settings.documentSizeLimit){
				hasLargerFile = true;
			}
		}
		if(hasLargerFile){
			//alert($rootScope.getMsgs('file-too-big'));
			$scope.fileTooBig = true;
			$timeout(function(){$scope.fileTooBig = false;}, 3000);
			return;
		}
		
		if ($scope.upload && $scope.upload.length > 0) {
			for (var i = 0; i < $scope.upload.length; i++) {
				if ($scope.upload[i] != null) {
					$scope.upload[i].abort();
				}
			}
		}
		$scope.upload = [];
		$scope.uploadResult = [];
		$scope.selectedFiles = $files;
		$scope.selectedSlideKey = slideKey;
		$scope.dataUrls = [];
		$scope.uploadedImage;
		for ( var i = 0; i < $files.length; i++) {
			var $file = $files[i];
			if ($scope.fileReaderSupported && $file.type.indexOf('image') > -1) {
				$scope.uploadedFileName = $file.name;
				var fileReader = new FileReader();
				fileReader.readAsDataURL($files[i]);
				var loadFile = function(fileReader, index) {
					fileReader.onload = function(e) {
						$timeout(function() {
							$scope.dataUrls[index] = e.target.result;
							if (index == 0) {
								$scope.uploadedImage = $scope.dataUrls[index];
							}
						});
					}
				}(fileReader, i);
			}else if($scope.fileReaderSupported){
				$scope.uploadedFileName = $file.name;
			}
		}
	};
	
	$scope.start = function(index) {
		if ($scope.selectedFiles[index].size / 1000000  < settings.documentSizeLimit) {
			$scope.progress[index] = 0;
			$scope.errorMsg = null;
			//$upload.upload()
			var params = '?';
			params += 'fileType=35&';
			params += 'writerId=1&';
			params += 'fileName=' + $scope.selectedFiles[index].name;
			params = encodeURI(params);
			$scope.upload[index] = $upload.upload({
				url: context_path + '/UploadDocumentsService' + params,
				method: 'POST',
				file: $scope.selectedFiles[index],
				fileFormDataName: 'myFile',
				data: {
					fileName: $scope.selectedFiles[index].name
				}
			});
			$scope.upload[index].then(function(response) {
				$timeout(function() {
					$scope.uploadResult.push(response);
					$scope.uploadComplete(response);
				});
			}, function(response) {
				if (response.status > 0){
					$scope.uploadedImage = null;
					$rootScope.addGlobalErrorMsg($rootScope.getMsgs('error.unknown'));
				}
			}, function(evt) {
				// Math.min is to fix IE which reports 200% sometimes
				$scope.$parent.progress = $scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
			});
			$scope.upload[index].xhr(function(xhr){
	//				xhr.upload.addEventListener('abort', function() {console.log('abort complete')}, false);
			});
		} else {
			$scope.uploadedImage = null;
			$rootScope.addGlobalErrorMsg($rootScope.getMsgs('file-too-big'));
		}
	};
	
	$scope.uploadComplete = function(data) {
		$rootScope.showWithdrawUserDetailsPopup = false;
		$scope.isSubmitting = false;
	}
	
	if (localStorage) {
		$scope.s3url = localStorage.getItem("s3url");
		$scope.AWSAccessKeyId = localStorage.getItem("AWSAccessKeyId");
		$scope.acl = localStorage.getItem("acl");
		$scope.success_action_redirect = localStorage.getItem("success_action_redirect");
		$scope.policy = localStorage.getItem("policy");
		$scope.signature = localStorage.getItem("signature");
	}
	$scope.success_action_redirect = $scope.success_action_redirect || window.location.protocol + "//" + window.location.host;
	$scope.jsonPolicy = $scope.jsonPolicy || '{\n  "expiration": "2020-01-01T00:00:00Z",\n  "conditions": [\n    {"bucket": "angular-file-upload"},\n    ["starts-with", "$key", ""],\n    {"acl": "private"},\n    ["starts-with", "$Content-Type", ""],\n    ["starts-with", "$filename", ""],\n    ["content-length-range", 0, 524288000]\n  ]\n}';
	$scope.acl = $scope.acl || 'private';
	
	$scope.send = function(){
		for ( var i = 0; i < $scope.selectedFiles.length; i++) {
			$scope.progress[i] = -1;
			$scope.start(i);
		}
	}
}]);
