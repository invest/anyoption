aoApp.controller('BinaryOptionController', ['$rootScope', '$scope', '$http', '$timeout', '$interval', function($rootScope, $scope, $http, $timeout, $interval) {
	$scope.ready = true;
	$scope.selectedMarkets = [];
	$scope.searchPlaceHolder = '';
	$scope.input = {
		search: ''
	}
	function waitForIt() {
		if (isUndefined($rootScope.$state.current.needLogin) || !$rootScope.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			// alert($rootScope.binaryMarkets);
			init();
		}
	}
	
	function init() {

	}
	
	$scope.selectMarketSearch = function(market, box) {
		$scope.input.search = "";
		if (searchJsonKeyInArray($scope.selectedMarkets, 'id', market.id) == -1 && $scope.selectedMarkets.length < 4) {
			showMarketsMenu_box = isUndefined(box) ? $scope.selectedMarkets.length : box;
			var indexMarketInBox = marketToBox(market.id);
			if (!~indexMarketInBox) {// == -1 - in case this market is not in any of the 4 boxes
				if (selectMarket(market.id, true, true)) {;
					$scope.pushMarket(market);
					box_objects[showMarketsMenu_box].marketIdManual = true;
				}
			} else {
				if (indexMarketInBox == showMarketsMenu_box) {//in case market is already selected in the same box
					box_objects[showMarketsMenu_box].marketIdManual = true;
					$scope.pushMarket(market);
				} else {
					var marketIdOfbox = box_objects[showMarketsMenu_box].marketId;
					box_objects[indexMarketInBox].resetBox(true);//reset the box that was holding the desired market
					selectMarket(market.id, true, true);//put the desired market in the correct box
					box_objects[showMarketsMenu_box].marketIdManual = true;
					//now we have to put something in the resetted box
					showMarketsMenu_box = indexMarketInBox;
					selectMarket(marketIdOfbox, true, true);

					$scope.pushMarket(market);
				}
			}
		}
		$('#search-input').focus();
	}
	
	$scope.setFocus = function() {
		$('#search-input').focus();
	}
	
	$scope.closeSearch = function() {
		$timeout(function(){
			$scope.input.search = "";
		}, 300);
	}
	
	$scope.pushMarket = function(market) {
		market.stateDps = box_objects[showMarketsMenu_box].stateDps;
		$scope.selectedMarkets.push(market);
		$scope.checkMaxSize();
	}
	
	$scope.deleteMarket = function(market) {
		var index = searchJsonKeyInArray($scope.selectedMarkets, 'id', market.id);
		if (index > -1) {
			var tempArray = $scope.selectedMarkets.slice(0);//make a copy
			tempArray.splice(index, 1);
			$scope.selectedMarkets.splice(index);
			$scope.checkMaxSize();
			
			for (var i = index; i < tempArray.length; i++) {
				$scope.selectMarketSearch(tempArray[i], i);
			}
			
			function findFreeMarket(box) {
				var newMarketId = BO_default_markets[box];
				if (!~marketToBox(newMarketId)) {// == -1
					showMarketsMenu_box = box;
					selectMarket(newMarketId, true, true);
					box_objects[showMarketsMenu_box].marketIdManual = false;
				} else {
					box++;
					if (box >= BO_default_markets.length) {
						box = 0;
					}
					findFreeMarket(box);
				}
			}
			
			findFreeMarket(tempArray.length);
		}
	}
	
	$scope.globalMarketDelete = function(fullReset) {
		$timeout(function() {
			$scope.selectedMarkets.length = 0;
			$scope.checkMaxSize();
		})
		if (fullReset) {
			for(var i = 0; i < box_objects.length; i++) {
				box_objects[i].resetBox(true);
			}
			for(var i = 0; i < box_objects.length; i++) {
				showMarketsMenu_box = i;
				selectMarket(BO_default_markets[i], true, true);
			}
		}
	}
	
	$scope.isSelectedMarket = false;
	$scope.isSelectedMarketFull = false;
	$scope.checkMaxSize = function() {
		$scope.isSelectedMarketFull = false;
		$scope.isSelectedMarket = true;
		if ($scope.selectedMarkets.length == 4) {
			$scope.input.search = "";
			$scope.searchPlaceHolder = '(' + $rootScope.getMsgs('max-4-items') + ')';
			$scope.isSelectedMarketFull = true;
		} else if ($scope.selectedMarkets.length == 0) {
			$scope.searchPlaceHolder = $rootScope.getMsgs('advanced-search-title');
			$scope.isSelectedMarket = false;
		} else {
			var word = ($scope.selectedMarkets.length == 1) ? 'item' : 'items';
			$scope.searchPlaceHolder = '(' + $scope.selectedMarkets.length + ' ' + $rootScope.getMsgs(word) + ')';
		}
	}
	$scope.checkMaxSize();
	
	$scope.updateState = function(stateDps, box, marketId) {
		if (box < $scope.selectedMarkets.length) {
			if (marketId == $scope.selectedMarkets[box].id) {
				$scope.selectedMarkets[box].stateDps = stateDps;
				$scope.$apply();
			} else {
				$scope.deleteMarket($scope.selectedMarkets[box]);
			}
		}
	}
	
	$scope.keyPressSearch = function($event) {
		if ($scope.selectedMarkets.length == 4) {
			if ($event.keyCode == 8) {
				$scope.deleteMarket($scope.selectedMarkets[$scope.selectedMarkets.length-1]);
			} else {
				$scope.input.search = "";
			}
		} else {
			if ($event.keyCode == 8 && $scope.input.search.length == 0 && $scope.selectedMarkets.length > 0) {
				$scope.deleteMarket($scope.selectedMarkets[$scope.selectedMarkets.length-1]);
			}
		}
	}

	waitForIt();
}]);