aoApp.controller('settingsCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;
	
	$scope.set = function() {
		var CancelInvestmentCheckboxStateMethodRequest = jsonClone(getUserMethodRequest(), {
			showCancelInvestment: !$rootScope.user.showCancelInvestment
		});
		$http.post(settings.jsonLinkNew + 'changeCancelInvestmentCheckboxState', CancelInvestmentCheckboxStateMethodRequest)
			.then(function(data) {
				data = data.data;
				if (!$rootScope.handleErrors(data, 'changeCancelInvestmentCheckboxState')) {
					$rootScope.user.showCancelInvestment = !$rootScope.user.showCancelInvestment;
				}
			})
			.catch(function(data) {
				data = data.data;
				$rootScope.handleNetworkError(data);
			})
	}
}]);