function chartOnload() {
	$('html').delegate('._toggleMenu', 'click', function(event) {
		if ($(event.target).closest('ul.list').length > 0) {
			event.stopPropagation();
		} else {
			if ($(this).hasClass('opened')) {
				$(this).removeClass('opened');
				$(this).find("ul.list").slideUp();
			} else {
				$(this).addClass('opened');
				$(this).find("ul.list").slideDown();
			}
		}
	});
}
function milSecToMin(milSec) {
	return milSec / 60000;
}
function milSecToMinFormat(milSec) {
	if (milSec < 0) {
		milSec = 0;
	}
	var tmp = {};
	var sec = tmp.seconds = Math.floor(milSec / 1000);
	
	var min = 0;
	if (sec > 60) {
		min = Math.floor(sec / 60);
		sec = sec - (min * 60);
	}
	
	var hours = 0;
	if (min > 60) {
		hours = Math.floor(min / 60);
		min = min - (hours * 60);
	}
	
	secDsp = (sec < 10) ? '0' + sec : sec;
	minDsp = (min < 10) ? '0' + min : min;

	if (hours > 0) {
		tmp.formatted = hours + ':';
		tmp.formattedNoZero = hours + ':';
	} else {
		tmp.formatted = '';
		tmp.formattedNoZero = '';
	}
	
	tmp.formatted += minDsp + ':' + secDsp;
	tmp.formattedNoZero += min + ':' + sec;
	tmp.min = min;
	tmp.hours = hours;
	return tmp;
}
function parseSuspendMsgs(suspended_message) {
	var sperator = "<br/>";
	var arr = suspended_message.split("s");
	var str = suspendMsgUpperLine[new Number(arr[0]) - 1] + sperator + suspendMsgLowerLine[new Number(arr[1]) - 1];
	if (arr.length == 3 && arr[2].length > 0) {
		str += " " + formatDate(new Date(arr[2]), false);
	}
	return str;
}

function calculateOffer(value) {
	var temp = 0;
	if (value > 0) {
		temp = Math.round(((100 - value) / value) * 1000000) / 10000;
	}
	return temp;
}
function calculateBid(value) {
	var temp = 0;
	if (value > 0) {
		temp = Math.round(value / (100 - value) * 1000000) / 10000;
	}
	return temp;
}
function getProfit(params) {//dir:(true, false), percent: (bid or offer value), amount
	if (params.dir) {//above (offer)
		return calculateOffer(params.percent) * params.amount / 100;
	} else {
		return calculateBid(params.percent) * params.amount / 100;
	}
}
function offsetToLs(date, offset) {
	return new Date(date.getTime() + offset);
}
function goToByScroll(id, adjust) {
	adjust = (!isUndefined(adjust)) ? adjust : 0; 
	$('html,body').animate({scrollTop: $("#"+id).offset().top + adjust}, 'slow');
}
function hideSmallCharts() {
	$('#small-charts').slideToggle();
}

function jsonIndexOf(json, val) {
	$.each(json, function(k, v) {
		if (k == val) {
			return k;
		}
	});
	return -1;
}

function multipleReplace(map, string) {
	$.each(map, function(k, v) {
		string = string.split(k).join(v);
	});
	return string;
}