var aoApp = angular.module('aoApp', [
	'ui.router',
	'angularFileUpload',
	'ngSanitize',
	'ngAnimate',
	'ngScrollbars',
	// 'vcRecaptcha'
])
.value('$anchorScroll', angular.noop)
.run(['$rootScope', '$state', '$stateParams', '$interval', 
	function ($rootScope, $state, $stateParams, $interval) {
		// It's very handy to add references to $state and $stateParams to the $rootScope
		// so that you can access them from any scope within your applications.For example,
		// <li ng-class='{ active: $state.includes('contacts.list') }'> will set the <li>
		// to active whenever 'contacts.list' or one of its decendents is active.
		$rootScope.$state = $state;
		$rootScope.$stateParams = $stateParams;
		
		$rootScope.initSettings = function() {
			$rootScope.userProfile = '';//currently viewing user (myCopyopProfile or getCopyopProfile)
			$rootScope.usersInfo = [];//info for other users (copying, watching etc)
			$rootScope.popupInfo = {};//info about currently opend popups if needed
			$rootScope.openOptions = [];//list of all open assets
			$rootScope.afterDeposit = false;
			
			$rootScope.skinCurrencies = [];//array Currencies for current skin
			
			$rootScope.lsConnections = {
				dynamics: null
			}
			$rootScope.lsClient;

			$rootScope.recaptchaKey = settings.recaptchaKey;
			
			// $rootScope.balanceCallTypes = balanceCallTypes;
			
			// $rootScope.isMobile = (detectDevice() != "");
		}
		$rootScope.initSettings();
		
		$rootScope.dynamicsMarkets = [];
		
		$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			if (fromState.url == '^' || fromState.url == '/') {
				$rootScope.afterRefresh = true;
			} else {
				$rootScope.afterRefresh = false;
			}
			if (toState.needLogin == 2 && !settings.loggedIn && settings.loginDetected) {
				$rootScope.$state.go('ln.index', {ln: $rootScope.$state.params.ln});
				return;
			}
			if (!isUndefined(toState.noMobile) && toState.noMobile && detectDevice() != '') {
				$rootScope.$state.go('ln.index', {ln: $rootScope.$state.params.ln});
				return;
			}
			window.scrollTo(0, 0);
			//redirect links that should not be accessed
			if (!isUndefined($rootScope.$state.current.redirectTo)) {
				$rootScope.$state.go($rootScope.$state.current.redirectTo, {ln: $rootScope.$state.params.ln});
			}
		});
		
		$rootScope.$on('$stateChangeStart', 
			function(event, toState, toParams, fromState, fromParams) {
				//change browser title
				if (!isUndefined(toState.metadataKey)) {
					$rootScope.metadataTitle = toState.metadataKey;
				} else {
					$rootScope.metadataTitle = 'index';
				}
				
				//close any open ppups
				 // $rootScope.closeCopyOpPopup({all:true});
				
				//LS stuff that not working properly
				// if (toState.name.search('ln.in.a.asset') == -1) {
					// if (!isUndefined(assetsls) && assetsls.isSubscribed()) {
						// lsClient.unsubscribe(assetsls);
					// }
					// if (!isUndefined(tradingBox) && tradingBox.isSubscribed()) {
						// lsClient.unsubscribe(tradingBox);
						// markestInBox = [];
						// if (typeof group != 'undefined') {
							// for (var i = 0; i < group.length; i++) {
								// box_objects[i].resetBox(false);
							// }
						// }
						// box_objects = [];
						// group = [];
					// }
				// }
				error_stack = [];
				// if (settings.loggedIn && fromState.needLogin > 1) {
					// $scope.getCopyopUser({onStateChange: true});
				// }
			})
	}]
)
.config(['$httpProvider', '$stateProvider', '$urlRouterProvider','$locationProvider',
	function ($httpProvider, $stateProvider, $urlRouterProvider, $locationProvider) {
		//$httpProvider.defaults.useXDomain = true;
		delete $httpProvider.defaults.headers.common['X-Requested-With'];
		delete $httpProvider.defaults.headers.post['Content-Type'];
		$httpProvider.defaults.withCredentials = true;
		// Use $urlRouterProvider to configure any redirects (when) and invalid urls (otherwise).

		// Use $stateProvider to configure your states.
		$stateProvider
			.state('ln', {
				url: '/:ln',
				'abstract': true,
				needLogin: 1,//0 - no, 1 - doesn't matter, 2-yes
			})
			.state('ln.dynamics', {
				url: '/dynamics',
				needLogin: 1
			})
			.state('dynamics', {
				url: '',
				// templateUrl: 'html/minisite/template.html',
				needLogin: 1//0 - no, 1 - doesn't matter, 2-yes
			})
			.state('asset', {
				url: '/:market_id',
				// templateUrl: 'html/minisite/template.html',
				needLogin: 1//0 - no, 1 - doesn't matter, 2-yes
			})
			.state('general-terms', {
				url: '/general-terms/:id',
				// templateUrl: 'html/minisite/template.html',
				needLogin: 1//0 - no, 1 - doesn't matter, 2-yes
			})
			.state('withdraw-giropay', {
				url: '/withdraw-giropay',
				// templateUrl: 'html/minisite/template.html',
				needLogin: 2//0 - no, 1 - doesn't matter, 2-yes
			})
			.state('withdraw-direct24', {
				url: '/withdraw-direct24',
				// templateUrl: 'html/minisite/template.html',
				needLogin: 2//0 - no, 1 - doesn't matter, 2-yes
			})
			.state('withdraw-eps', {
				url: '/withdraw-eps',
				// templateUrl: 'html/minisite/template.html',
				needLogin: 2//0 - no, 1 - doesn't matter, 2-yes
			})
			.state('documents', {
				url: '/documents',
				// templateUrl: 'html/minisite/template.html',
				needLogin: 2//0 - no, 1 - doesn't matter, 2-yes
			})


		// enable html5Mode for pushstate ('#'-less URLs)
		if (settings.isLive) {
			// $locationProvider.html5Mode(true).hashPrefix('!');
		}
	}]);
	
aoApp.config(['$compileProvider', function ($compileProvider) {
	// $compileProvider.debugInfoEnabled(false);
}]);

//preload combined template file
/*
aoApp.factory('$templateCache', ['$cacheFactory', '$http', '$injector', function($cacheFactory, $http, $injector) {
	var cache = $cacheFactory('templates');
	var allTplPromise;

	return {
		get: function(url) {
			var fromCache = cache.get(url);
			if (url.search('minisite') > 0) {
				return;
			}

			// already have required template in the cache
			if (fromCache) {
				return fromCache;
			}

			// first template request ever - get the all tpl file
			if (!allTplPromise) {
				allTplPromise = $http.get('html/templates/GeneralTemplate.html')
					.then(function(response) {
						// compile the response, which will put stuff into the cache
						$injector.get('$compile')(response.data);
						return response;
					});
			}

			// return the all-tpl promise to all template requests
			return allTplPromise
				.then(function(response) {
					return {
						status: response.status,
						data: cache.get(url),
						headers: response.headers
					};
				});
		},

		put: function(key, value) {
			cache.put(key, value);
		}
	};
}]);
*/ 