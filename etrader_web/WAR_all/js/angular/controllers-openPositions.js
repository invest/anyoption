var lsOpenPositionsConfig = {
	lsOpenPositions: null,
	updateCallback: null,
	investCallback: null,
	group: ''
};

aoApp.controller('OpenPositionsController', ['$rootScope', '$scope', '$http', '$timeout', '$interval', function($rootScope, $scope, $http, $timeout, $interval) {
	var _this = this;
	
	_this.openInvestments = [];
	_this.marketOpportunities = {};
	
	_this.scrollbarsConfig = {
		autoHideScrollbar: false,
		theme: 'dark',
		advanced:{
			updateOnContentResize: true
		},
		/*scrollButtons: false,*/
		scrollInertia: 100,
		axis: 'y'
	};
	
	
	_this.formatAmount = formatAmount;
	_this.formatExpiry = formatExpiry;
	_this.isExpired = isExpired;
	_this.getInvestmentsCurrentSum = getInvestmentsCurrentSum;
	_this.isChartDisabled = isChartDisabled;
	_this.openChart = openChart;
	_this.getInvestmentProfitLoss = getInvestmentProfitLoss;
	_this.getTotalInvestmentProfitLoss = getTotalInvestmentProfitLoss;
	_this.isInvestmentGroupWinning = isInvestmentGroupWinning;
	_this.isInvestmentWinning = isInvestmentWinning;
	_this.isTotalInvestmentWinning = isTotalInvestmentWinning;
	
	waitForIt();
	
	
	function waitForIt() {
		if (!$rootScope.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			init();
		}
	}
	
	function init() {
		getOpenInvestments();
	}
	
	
	
	function getOpenInvestments(){
		_this.loading = true;
		var MethodRequest = jsonClone(getUserMethodRequest(), {});
		$http.post(settings.jsonLinkNew + 'getOpenPositions', MethodRequest)
			.then(function(data) {
				_this.loading = false;
				
				data = data.data;
				if (!$rootScope.handleErrors(data, 'getOpenInvestments')) {
					_this.openInvestments = data.openPositions;
				}
				
				ls_subscribe();
			})
			.catch(function(data) {
				data = data.data;
				$rootScope.handleNetworkError(data);
			});
	}
	
	function ls_subscribe(){
		_this.marketOpportunities = {};
		
		var lsGroup = [];
		for(var i = 0; i < _this.openInvestments.length; i++){
			lsGroup.push('aotps_' + _this.openInvestments[i].scheduled + '_' + _this.openInvestments[i].marketId);
		}
		
		lsOpenPositionsConfig.updateCallback = ls_openPositionsUpdate;
		lsOpenPositionsConfig.investCallback = ls_openPositionsInvest;
		lsOpenPositionsConfig.group = lsGroup;
		
		lsOpenPositionsConfig.lsOpenPositions = connectToLS({'name':'openPositions','subscriptionType':subscriptionType,'group':lsOpenPositionsConfig.group,'schema':schema,'dataAdapterName':dataAdapterName,'callBack':lsOpenPositionsConfig.updateCallback});
	}
	
	function ls_openPositionsUpdate(updateInfo){
		var updateInfo = updateInfo;
		
		var openPositionsUpdate = {};
		openPositionsUpdate.opportunityId = updateInfo.getValue(SUBSCR_FLD.et_opp_id);
		openPositionsUpdate.marketId = updateInfo.getValue(SUBSCR_FLD.et_name);
		openPositionsUpdate.level = updateInfo.getValue(SUBSCR_FLD.ao_level);
		openPositionsUpdate.scheduled = updateInfo.getValue(SUBSCR_FLD.et_scheduled);
		openPositionsUpdate.estClosing = updateInfo.getValue(SUBSCR_FLD.et_est_close);
		
		if(updateInfo.getValue('command') == 'DELETE'){
			//Remove opportunity from the list that is used to check the closest closing opportunity
			if(_this.marketOpportunities[openPositionsUpdate.marketId]){
				var index = -1;
				for(var i = 0; i < _this.marketOpportunities[openPositionsUpdate.marketId].length; i++){
					if(_this.marketOpportunities[openPositionsUpdate.marketId][i].oppId == openPositionsUpdate.opportunityId){
						index = i;
						break;
					}
				}
				if(index >= 0){
					_this.marketOpportunities[openPositionsUpdate.marketId].splice(index, 1);
				}
			}
			
			return;
		}
		
		var state = updateInfo.getValue(SUBSCR_FLD.et_state);
		if(state == OPPORTUNITY_STATE.opened || state == OPPORTUNITY_STATE.last_10_min || state == OPPORTUNITY_STATE.closing_1_min || state == OPPORTUNITY_STATE.closing){
			//Add opportunity to the list that is used to check the closest closing opportunity
			if(!_this.marketOpportunities[openPositionsUpdate.marketId]){
				_this.marketOpportunities[openPositionsUpdate.marketId] = [];
			}
			var exists = false;
			for(var i = 0; i < _this.marketOpportunities[openPositionsUpdate.marketId].length; i++){
				if(_this.marketOpportunities[openPositionsUpdate.marketId][i].oppId == openPositionsUpdate.opportunityId){
					exists = true;
					break;
				}
			}
			if(!exists){
				_this.marketOpportunities[openPositionsUpdate.marketId].push({oppId: openPositionsUpdate.opportunityId, estClose: openPositionsUpdate.estClosing});
			}
			
			//Update the level
			$scope.$apply(function(){
				for(var i = 0; i < _this.openInvestments.length; i++){
					if(_this.openInvestments[i].opportunityId == openPositionsUpdate.opportunityId){
						_this.openInvestments[i]._currentLevel = openPositionsUpdate.level;
					}
				}
			});
		}
	}
	
	function ls_openPositionsInvest(investment){
		var investmentGroupFound = false;
		var investmentOppId = investment.boxOpportunities[investment.oppArrPos].ET_OPP_ID;
		var investmentScheduled = investment.boxOpportunities[investment.oppArrPos].ET_SCHEDULED;
		var newInvestment = {
			id: investment.investmentId,
			currentLevel: investment.chartPointsInv_last.level,
			amount: investment.invest*100,
			typeId: investment.chartPointsInv_last.type,
			oddsWin: investment.profit/100,
			oddsLose: 1 - investment.refund/100
		};
		$scope.$apply(function(){
			for(var i = 0; i < _this.openInvestments.length; i++){
				if(_this.openInvestments[i].opportunityId == investmentOppId){
					_this.openInvestments[i].investments.push(newInvestment);
					investmentGroupFound = true;
					break;
				}
			}
			if(!investmentGroupFound){
				var investmentGroup = {
					marketId: investment.marketId,
					marketName: investment.boxOpportunities[investment.oppArrPos].ET_NAME,
					timeEstClosing: investment.boxOpportunities[investment.oppArrPos].ET_EST_CLOSE,
					opportunityId: investmentOppId,
					scheduled: investmentScheduled,
					investments: []
				};
				investmentGroup.investments.push(newInvestment);
				
				_this.openInvestments.push(investmentGroup);
				
				lsClient.unsubscribe(lsOpenPositionsConfig.lsOpenPositions);
				ls_subscribe();
			}
			//Sort by closing time
			_this.openInvestments.sort(function(a, b){
				return new Date(a.timeEstClosing).getTime() > new Date(b.timeEstClosing).getTime();
			});
		});
	}
	
	function isChartDisabled(investmentsGroup){
		if(!investmentsGroup){
			return;
		}
		
		var notLatest = false;
		
		if(_this.marketOpportunities[investmentsGroup.marketId]){
			var now = new Date();
			now.setHours(now.getHours() + now.getTimezoneOffset()/60);
			var groupClosingTime = new Date(investmentsGroup.timeEstClosing);
			for(var i = 0; i < _this.marketOpportunities[investmentsGroup.marketId].length; i++){
				var closingTime = new Date(_this.marketOpportunities[investmentsGroup.marketId][i].estClose);
				if(closingTime.getTime() > now.getTime() && closingTime.getTime() < groupClosingTime.getTime()){
					notLatest = true;
					break;
				}
			}
		}else{
			//Default to not latest
			notLatest = true;
		}
		
		return notLatest || investmentsGroup.scheduled != 1;
	}
	
	function openChart(investmentsGroup){
		if(!_this.isChartDisabled(investmentsGroup)){
			window.top.openProfitLine_box(investmentsGroup.marketId, investmentsGroup.opportunityId);
		}
	}
	
	function formatExpiry(expiry){
		var result = '';
		
		var d = new Date(expiry);
		d.setHours(d.getHours() - d.getTimezoneOffset()/60);
		var now = new Date();
		
		result = padZeros(d.getHours()) + ':' + padZeros(d.getMinutes());
		if(d.getFullYear() == now.getFullYear() && d.getMonth() == now.getMonth() && d.getDate() == now.getDate()){
			//Don't show date for today
		}else{
			//result = d.getFullYear() + '.' + padZeros(d.getMonth() + 1) + '.' + padZeros(d.getDate()) + ' ' + result;
			result += ' ' + padZeros(d.getDate()) + '.' + padZeros(d.getMonth() + 1) + '.' + (d.getFullYear() + '').substring(2);
		}
		
		return result;
		
		function padZeros(num){
			if((num + '').length == 1){
				num = '0' + num;
			}
			return num;
		}
	}
	
	function isExpired(investmentsGroup){
		var closingTime = new Date(investmentsGroup.timeEstClosing);
		closingTime.setHours(closingTime.getHours() - closingTime.getTimezoneOffset()/60);
		var now = new Date();
		if(closingTime.getTime() < now.getTime()){
			for(var i = 0; i < _this.openInvestments.length; i++){
				if(_this.openInvestments[i].opportunityId == investmentsGroup.opportunityId){
					_this.openInvestments.splice(i, 1);
				}
			}
		}
	}
	
	function getInvestmentProfitLoss(investmentsGroup, investment, formatted){
		if(!investment){
			return;
		}
		
		if(!investmentsGroup._currentLevel){
			return;
		}
		
		var result = 0;
		
		if(_this.isInvestmentWinning(investmentsGroup, investment)){
			result = investment.amount*investment.oddsWin;
		}else{
			result = investment.amount*(1 - investment.oddsLose) - investment.amount;
		}
		
		if(formatted){
			var sign = '';
			if(result > 0){
				sign = '+';
			}
			result = sign + formatAmount(result/100, 2);
		}
		
		return result;
	}
	
	function getInvestmentsCurrentSum(investmentsGroup, formatted){
		var sum = 0;
		
		for(var i = 0; i < investmentsGroup.investments.length; i++){
			sum += _this.getInvestmentProfitLoss(investmentsGroup, investmentsGroup.investments[i]) || 0;
		}
		
		if(formatted){
			var sign = '';
			if(sum > 0){
				sign = '+';
			}
			sum = sign + formatAmount(sum/100, 2);
		}
		
		return sum;
	}
	
	function getTotalInvestmentProfitLoss(formatted){
		var sum = 0;
		
		for(var i = 0; i < _this.openInvestments.length; i++){
			sum += _this.getInvestmentsCurrentSum(_this.openInvestments[i]) || 0;
		}
		
		if(formatted){
			var sign = '';
			if(sum > 0){
				sign = '+';
			}
			sum = sign + formatAmount(sum/100, 2);
		}
		
		return sum;
	}
	
	function isInvestmentWinning(investmentsGroup, investment){
		if((parseStringToFloat(investmentsGroup._currentLevel) > parseStringToFloat(investment.currentLevel) && investment.typeId == 1) || (parseStringToFloat(investmentsGroup._currentLevel) < parseStringToFloat(investment.currentLevel) && investment.typeId != 1)){
			return true;
		}
		return false;
	}
	
	function isInvestmentGroupWinning(investmentsGroup){
		return _this.getInvestmentsCurrentSum(investmentsGroup) > 0;
	}
	
	function isTotalInvestmentWinning(){
		return _this.getTotalInvestmentProfitLoss() > 0;
	}
	
	function parseStringToFloat(str){
		str = str + '';
		return parseFloat(str.replace(/,/gi, ''));
	}
	
}]);