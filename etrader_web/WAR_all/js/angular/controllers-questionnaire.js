aoApp.controller('userSingleQuestionnaireCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;
	$scope.activeScreen = 0;
	$scope.screensComplete = [];
	$scope.hasError = true;

	function waitForIt() {
		if (!$rootScope.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			$scope.init();
		}
	}
	
	$scope.regulationQuestNext = function() {
		$scope.checkStatus(true);
		if ($scope.screensComplete[$scope.activeScreen]) {
			$scope.activeScreen++
			$scope.checkStatus(false);
		}
	}
	
	$scope.isRegulationQuestionaireCalled = false;
	$scope.init = function() {
		if($scope.isRegulationQuestionaireCalled){
			return;
		}
		$scope.isRegulationQuestionaireCalled = true;
		$http.post(context_path + aoJsonUrl + 'getUserSingleQuestionnaireDynamic', getUserMethodRequest())
			.then(function(data) {
				data = data.data;
				if (!$rootScope.handleErrors(data, 'getUserSingleQuestionnaireDynamic')) {
					if (data.errorCode == errorCodeMap.regulation_suspended) {
						$scope.errorMsg = data.userMessages[0].message;
					} else {
						$scope.hasError = false;
						
						$scope.screens = [];
						$scope.userAnswers = data.userAnswers;
						
						for (var i = 0; i < data.questions.length; i++) {
							var screen = data.questions[i].screen - 1;
							if (isUndefined($scope.screens[screen])) {
								$scope.screens[screen] = [];
							}
							
							$scope.screensComplete[screen] = false;
							
							if (data.questions[i].questionType == 'DROPDOWN') {
								data.questions[i].answers.unshift({
									id: null, 
									translation: $rootScope.getMsgs('select')
								})
							} else if (data.questions[i].questionType == 'CHECKBOX') {
								if (data.questions[i].answers[0].name.search('no') > -1) {//if no is the first answer
									var el = data.questions[i].answers.pop();//remove the last one (yes)
									data.questions[i].answers.unshift(el);//and add it in the begining, before no
								}
							}
							
							var index = searchJsonKeyInArray(data.userAnswers, 'questionId', data.questions[i].id);
							data.questions[i].index = i;
							
							var indexAnswer = searchJsonKeyInArray(data.questions[i].answers, 'id', data.userAnswers[index].answerId);
							
							var indexAnswerReal = 0;
							if (index > -1 && data.userAnswers[index].answerId != null && indexAnswer > -1) {
								indexAnswerReal = indexAnswer;
							} else if (data.questions[i].defaultAnswerId == 0) {
								indexAnswerReal = 0;
							} else {
								indexAnswerReal = searchJsonKeyInArray(data.questions[i].answers, 'id', data.questions[i].defaultAnswerId);
							}
							
							if (data.questions[i].questionType == 'DROPDOWN') {
								data.questions[i].userAnswer = data.questions[i].answers[indexAnswerReal];
								data.questions[i].userAnswer.textAnswer = data.userAnswers[index].textAnswer;
							} else if (data.questions[i].questionType == 'CHECKBOX') {
								data.questions[i].translatation = data.questions[i].translatation.replace('link_risk', link_risk).replace('link_agreement', link_agreement).replace('link_privacy', link_privacy);
								
								data.questions[i].userAnswer = {
									id: data.questions[i].answers[indexAnswerReal].id
								}
							}
							
							$scope.screens[screen].push(data.questions[i]);
						}
						
						$scope.checkStatus(false);
					}
				}
			})
			.catch(function(data) {
				data = data.data;
				$rootScope.handleNetworkError(data);
			})
	}
	
	$scope.checkStatus = function(showErrors) {
		for (var i = 0; i < $scope.screens.length; i++) {
			var questionsAnswered = 0;
			var questionsTotal = 0;
			for (var n = 0; n < $scope.screens[i].length; n++) {
				if ($scope.screens[i][n].mandatory) {
					questionsTotal++;
				}
				if (
					$scope.screens[i][n].mandatory && (
						($scope.screens[i][n].questionType == 'DROPDOWN' && $scope.screens[i][n].userAnswer.id != null) || 
						($scope.screens[i][n].questionType == 'CHECKBOX' && $scope.screens[i][n].userAnswer.id == $scope.screens[i][n].answers[0].id)
					)
				) {
					questionsAnswered++;
					$scope.screens[i][n].hasError = false;
				} else if (showErrors && $scope.screens[i][n].mandatory) {
					$scope.screens[i][n].hasError = true;
				} else {
					$scope.screens[i][n].hasError = false;
				}
				
				var index = searchJsonKeyInArray($scope.userAnswers, 'questionId', $scope.screens[i][n].id);
				if (index > -1) {
					$scope.userAnswers[index].answerId = $scope.screens[i][n].userAnswer.id;
					$scope.userAnswers[index].textAnswer = $scope.screens[i][n].userAnswer.textAnswer;
				}
			}
			if (questionsTotal == questionsAnswered) {
				$scope.screensComplete[i] = true;
			} else {
				$scope.screensComplete[i] = false;
			}
		}
	}
	
	$scope.submitSQuestionnaire = function(submit, _form) {
		if (submit) {
			$scope.checkStatus(true);
			$scope.loading = true;
		} else {
			$scope.checkStatus(false);
		}
		
		var UpdateUserQuestionnaireAnswersRequest = jsonClone(getUserMethodRequest(), {
			userAnswers: $scope.userAnswers
		});
		
		$http.post(context_path + aoJsonUrl + 'updateUserQuestionnaireAnswers', UpdateUserQuestionnaireAnswersRequest)
			.then(function(data) {
				data = data.data;
				$scope.loading = false;
				if (!$rootScope.handleErrors(data, 'updateUserQuestionnaireAnswers')) {
					if (submit && $scope.screensComplete[$scope.screensComplete.length-1]) {
						$scope.updateSingleQuestionnaireDone(_form);
					}
					//$rootScope.handleSuccess(data);
				}
			})
			.catch(function(data) {
				$scope.loading = false;
				$rootScope.handleNetworkError(data);
			})
	}
	
	$scope.updateSingleQuestionnaireDone = function(_form) {
		if (!$scope.loading) {
			$scope.loading = true;
			
			$http.post(context_path + aoJsonUrl + 'updateSingleQuestionnaireDone', getUserMethodRequest())
				.then(function(data) {
					data = data.data;
					$scope.loading = false;
					// if (!$rootScope.handleErrors(data, 'updateSingleQuestionnaireDone')) {
						eraseCookie(aoRegulationCookieName);
						window.location.replace(capitalQuestionnaireWeightLink);
					// }
				})
				.catch(function(data) {
					data = data.data;
					$scope.loading = false;
					$rootScope.handleNetworkError(data);
				})
		}
	}

	waitForIt();
}]);