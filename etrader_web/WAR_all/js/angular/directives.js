aoApp.directive('autoFocus', ['$timeout', function($timeout) {
	return {
		restrict: 'A',
		link: function($scope, $element) {
			$timeout(function() {
				$element[0].focus();
			});
		}
	}
}]);

aoApp.directive('compareTo', function() {
	return {
		require: "ngModel",
		scope: {
			otherModelValue: "=compareTo"
		},
		link: function(scope, element, attributes, ngModel) {
			ngModel.$validators.compareTo = function(value) {
				return value == scope.otherModelValue;
			};

			scope.$watch("otherModelValue", function() {
				ngModel.$validate();
			});
		}
	};
});

aoApp.directive('validCcNum', function () { 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				ngModel.$setValidity('validCcNum', !isNaN(detectCCtype(value, true)));
				return value;
			});
		}
	};
});

aoApp.directive('validEmail', function () { 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				ngModel.$setValidity('validEmail', regEx_email.test(value));
				return value;
			});
		}
	};
});

aoApp.directive('restrictNickname', function() {
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				// ngModel.$setValidity('nickname', (regEx_nickname.test(value)));
				value = value.replace(regEx_nickname_reverse, '');
				elem[0].value = value;
				ngModel.$setViewValue(value);
				return value;
			});
		}
	};
});

aoApp.directive('restrictDigits', function() {
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				value = value.replace(regEx_digits_reverse, '');
				elem[0].value = value;
				ngModel.$setViewValue(value);
				return value;
			});
		}
	};
});

aoApp.directive('restrictFloat', function() {
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				var val = value.replace(',', '.').split('.');
				value = val[0].replace(regEx_digits_reverse, '');
				if (!isUndefined(val[1])) {
					value += '.' + val[1].replace(regEx_digits_reverse, '');
				}
				elem[0].value = value;
				ngModel.$setViewValue(value);
				return value;
			});
		}
	};
});

aoApp.directive('restrictLetters', function() {
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				//var regEx_letters_reverse = new RegExp((skinMap[settings.skinId].regEx_letters+'').replace(/\//g,'').replace('^[','[^').replace('+$',''),'g');
				var regEx_letters_reverse = new XRegExp((regEx_lettersOnly+'').replace(/\//g,'').replace('^[','[^').replace('+$',''),'g');
				value = value.replace(regEx_letters_reverse, '');
				elem[0].value = value;
				ngModel.$setViewValue(value);
				return value;
			});
		}
	};
});

aoApp.directive('device', ['$timeout', function($timeout) {
	return {
		restrict: 'A',
		link: function($scope, $element) {
			$timeout(function() {
				var device = detectDevice();
				if (device != '') {
					$element.addClass('mobile-device');
				}
				$element.addClass(device);
			});
		}
	}
}]);

aoApp.directive('compile', ['$compile', function($compile) {
	// directive factory creates a link function
	return function(scope, elem, attrs) {
		scope.$watch(
			function(scope) {
				// watch the 'compile' expression for changes
				return scope.$eval(attrs.compile);
			},
			function(value) {
				// when the 'compile' expression changes
				// assign it into the current DOM
				elem.html(value);

				// compile the new DOM and link it to the current
				// scope.
				// NOTE: we only compile .childNodes so that
				// we don't get into infinite loop compiling ourselves
				$compile(elem.contents())(scope);
			}
		);
	}
}]);

aoApp.directive('fallbackSrc', function () {
	var fallbackSrc = {
		link: function postLink(scope, iElement, iAttrs) {
			iElement.bind('error', function() {
				if(iAttrs.fallbackSrc != ""){
					angular.element(this).attr("src", iAttrs.fallbackSrc);
					angular.element(this).css("visibility", "visible");
				}else{
				//	angular.element(this).css("visibility", "hidden"); //Causes problems, sometimes, with some markets
				}
			});
		}
	}
	return fallbackSrc;
});

aoApp.directive('loading', ['$timeout','$compile', function($timeout, $compile) {
    return {
        restrict: "A",
        link: function(scope,element,attrs,ngCtrl) {
			scope.ready;
			var loader = angular.element('<i class="loading-section-el" ng-class="{dN: ready}" ng-hide="ready" ></i>');
			element.addClass('loading-section');
			element.append($compile(loader)(scope));
        }
    };
}]);

aoApp.directive('staticInclude', ['$http', '$templateCache', '$compile', function($http, $templateCache, $compile) {
    return function(scope, element, attrs) {
        var templatePath = attrs.staticInclude;
        $http.get(templatePath, { cache: $templateCache }).then(function(response) {
            var contents = element.html(response).contents();
            $compile(contents)(scope);
        });
    };
}]);

/*
//DO NOT UNCOMMENT, IT'S GOING TO BLAW
//Can be used instead of the watch of popUps in controllers.js

aoApp.directive('popups', ['$compile', '$timeout', function(compile, timeout){
	return {
		restrict: 'E',
		link: function(scope, element, attrs) {
			showPopups();
			scope.$watch('popUps', function(){
				showPopups();
			}, true);
			
			function showPopups(){
				for(var i = 0; i < scope.popUps.length; i++) {
					var template = '<div data-popup ng-include="\'' + scope.popUps[i].url + '\'" ng-controller="' + scope.popUps[i].ctr + '" ng-init="passParams(popUps[' + i + '].config)"></div>';
					var cTemplate = compile(template)(scope);
					element.append(cTemplate);
				}
				timeout(function(){
					console.log(element.children().length, scope.popUps.length);
					if(scope.popUps.length == 0 && element.children().length != 0){
						element.empty();
					//	element.remove();
					}else{
						var childCount = 0;
						element.children().each(function(index, el){
							childCount++;
							if(childCount > scope.popUps.length){
								el.remove();
							}
						});
					}
				}, 0);
			}
		}
	}
}]);
*/

aoApp.filter('toArray', function () {
    'use strict';

    return function (obj) {
        if (!(obj instanceof Object)) {
            return obj;
        }

        return Object.keys(obj).map(function (key) {
            return Object.defineProperty(obj[key], '$key', {__proto__: null, value: key});
        });
    }
});
aoApp.filter('firstLetterSearch', function () {
	function strStartsWith(str, prefix) {
		return (str+"").toLowerCase().indexOf(prefix.toLowerCase()) === 0;
	}
	
	return function(items, params) {
		var filtered = [];

	    angular.forEach(items, function(item) {
	    	if(strStartsWith(item[params.filed], params.value)){
	    		filtered.push(item);
	    	}
	    });

	    return filtered;
	}
});