/*********** quickStart.js ************/
function SetEnd(TB) {
	if (TB.createTextRange) {
		var FieldRange = TB.createTextRange();
		FieldRange.moveStart('character', TB.value.length);
		FieldRange.collapse();
		FieldRange.select();
	}
}
var changePhoneCodeByCountry_form = '';
function changePhoneCodeByCountry(form) {
	changePhoneCodeByCountry_form = form;
	var countryId = document.getElementById(form+":countriesList").value;
	var url = "ajax.jsf?countryId=" + countryId;
	var ajax = new AJAXInteraction(url, changePhoneCodeByCountryCallback);
	ajax.doGet();
}
function changePhoneCodeByCountryCallback(responseXML) {
   var codeValue = responseXML.getElementsByTagName("phoneCode")[0].firstChild.nodeValue;
   var phoneCodeElm = document.getElementById(changePhoneCodeByCountry_form +":mobilePhoneCode");
   phoneCodeElm.value = codeValue;
}
function gowait(elementId, elementTostartId, picname) {
	document.getElementById(elementId).src = picname;

	window.setTimeout( function() {
		var wg = document.getElementById(elementId);
		wg.src = wg.src; }, 50);

	document.getElementById(elementId).disabled = true; // disable click on commandButton
}
function validateEmail() {
	var target = document.getElementById("contactUsForm:email");
	if (target.value.length > 0){
		var url = "ajax.jsf?emailValidation=" + encodeURIComponent(target.value);
		var ajax = new AJAXInteraction(url, validateEmailCallback);
		ajax.doGet();
	}
}
function validateEmailCallback(responseXML) {
   var msg = responseXML.getElementsByTagName("emailErrormsg")[0].firstChild.nodeValue;
   var check = document.getElementById("contactUsForm:emailError");
   if (check != null) {
	  check.innerHTML = '';
   }
   var mdiv = document.getElementById("emailMessage");
   // set the style on the div to invalid
   mdiv.className = "error_messagess";
   mdiv.innerHTML = msg;
}
/*
	insert into register attempts AJAX call
*/
function insertUpdateRegisterAttempts() {
	var email = document.getElementById("contactUsForm:email");
	var firstName = document.getElementById("contactUsForm:firstName");
	var lastName = document.getElementById("contactUsForm:lastName");
	var countryId = document.getElementById("contactUsForm:countriesList");
	var landMobilePhoneCode = document.getElementById("contactUsForm:landMobilePhoneCode");
	var landMobilePhone = document.getElementById("contactUsForm:landMobilePhone");

	if (email.value.length > 0 ){
		var dataa = "quickStartEmailRegisterAttempts=" + encodeURIComponent(email.value)
			+ "&firstNameRegisterAttempts="	+ encodeURIComponent(firstName.value)
			+ "&lastNameRegisterAttempts=" + encodeURIComponent(lastName.value)
			+ "&countryRegisterAttempts=" + encodeURIComponent(countryId.value);
			if(landMobilePhoneCode != null){
				dataa += "&mobilePhoneRegisterAttempts=" + encodeURIComponent(landMobilePhoneCode.value + landMobilePhone.value) ;
			}
		$.ajax({
			type: "POST",
			url: "ajax.jsf",
			data: dataa
		});
	}
}
function checkForErrors(firstname,lastname,email,mobilephone,phone){
	var errorElement = g("contactUsForm:errorFromS").innerHTML;
	if (errorElement.indexOf(firstname) > -1){
		g("contactUsForm:firstName").className += " error";
	} else if (errorElement.indexOf(lastname) > -1){
		g("contactUsForm:lastName").className += " error";
	} else if (errorElement.indexOf(email) > -1){
		g("contactUsForm:email").className += " error";
	} else if ((errorElement.indexOf(mobilephone) > -1 || (errorElement.indexOf(phone) > -1)) ){
		g("contactUsForm:myPhone").className += " error";
	}
}