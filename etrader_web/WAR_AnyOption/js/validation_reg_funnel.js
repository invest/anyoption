var CARD_TYPE_AMEX = '5';
var CARD_PASS_LENGTH_AMEX = 4;
var CARD_PASS_LENGTH = 3;

var SKIN_ETRADER = 1;

var translatedTxt = {
	"error.cvv":"error.cvv", 
	"error.cvvLengthAmex":"error.cvvLengthAmex",
	"error.cvvLength":"error.cvvLength",
	"error.creditcard.num":"error.creditcard.num",
	"error.creditcard.diners.notsupported":"error.creditcard.diners.notsupported",
	"error.letters.only":"error.letters.only",
	"error.english.letters.only":"error.english.letters.only",
	"CMS.registerContent.text.register.letters.only":"CMS.registerContent.text.register.letters.only",
	"error.street":"error.street",
	"org.apache.myfaces.Equal.INVALID_detail":"org.apache.myfaces.Equal.INVALID_detail",
	"CMS.registerContent.text.error.englishandnumbers.only":"CMS.registerContent.text.error.englishandnumbers.only",
	"error.lettersandnumbers.only":"error.lettersandnumbers.only",
	"error.password.smallsize":"error.password.smallsize",
	"error.password.bigsize":"error.password.bigsize",
	"javax.faces.component.UIInput.REQUIRED_detail":"javax.faces.component.UIInput.REQUIRED_detail",
	"javax.faces.validator.LongRangeValidator.TYPE_detail":"javax.faces.validator.LongRangeValidator.TYPE_detail",
	"javax.faces.validator.LengthValidator.MAXIMUM_detail":"javax.faces.validator.LengthValidator.MAXIMUM_detail",
	"javax.faces.validator.LengthValidator.MINIMUM_detail":"javax.faces.validator.LengthValidator.MINIMUM_detail",
	"error.register.email.inuse":"error.register.email.inuse",
	"org.apache.myfaces.Email.INVALID_detail":"org.apache.myfaces.Email.INVALID_detail",
	"error.banner.contactme.email":"error.banner.contactme.email",
	"error.register.month":"error.register.month",
	"error.register.year":"error.register.year",
	"javax.faces.validator.RegexValidator.NOT_MATCHED":"javax.faces.validator.RegexValidator.NOT_MATCHED",
	"error.register.terms":"error.register.terms",
	"error.register.forbidden":"error.register.forbidden",
	"ukash.voucher.error" : "ukash.voucher.error",
	
	"no.such.limit" : "No limit for that currency",
	"error.creditcard.mindeposit" : "error.creditcard.mindeposit",
	"error.creditcard.maxdeposit" : "error.creditcard.maxdeposit",
	"error.creditcard.deposit.maxday" : "error.creditcard.deposit.maxday",
	"error.creditcard.deposit.maxmonth" : "error.creditcard.deposit.maxmonth",

	"general.validate.letters.only":/^['a-zA-Z ]+$/,
	"general.validate.street":/^['0-9 a-zA-Z. \" -]+$/,
	"general.validate.name":/^['a-zA-Z ]+$/
};

var REGEX_GENERAL_VALIDATE_LETTERS_ONLY = "general.validate.letters.only";
var REGEX_GENERAL_VALIDATE_STREET = "general.validate.street";
var REGEX_GENERAL_VALIDATE_NAME = "general.validate.name";

var MSG_ERROR_CVV = "error.cvv";
var MSG_ERROR_CVVLENGTH_AMEX = "error.cvvLengthAmex";
var MSG_ERROR_CVVLENGTH = "error.cvvLength";
var MSG_ERROR_CREDITCARD_NUM = "error.creditcard.num";
var MSG_ERROR_CREDITCARD_DINERS_NOTSUPPORTED = "error.creditcard.diners.notsupported";
var MSG_ERROR_LETTERS_ONLY = "error.letters.only";
var MSG_ERROR_ENGLISH_LETTERS_ONLY = "error.english.letters.only";
var MSG_ERROR_REGISTER_LETTERS_ONLY = "CMS.registerContent.text.register.letters.only";
var MSG_ERROR_STREET = "error.street";
var MSG_RETYPE_PASS_NOT_EQUAL = "org.apache.myfaces.Equal.INVALID_detail";
var MSG_ERROR_ENGLISHANDNUMBERS_ONLY = "CMS.registerContent.text.error.englishandnumbers.only";
var MSG_ERROR_LETTERSANDNUMBERS_ONLY = "error.lettersandnumbers.only";
var MSG_ERROR_PASSWORD_SMALLSIZE = "error.password.smallsize";
var MSG_ERROR_PASSWORD_BIGSIZE = "error.password.bigsize";
var MSG_ERROR_MANDATORY_FIELD = "javax.faces.component.UIInput.REQUIRED_detail";
var MSG_ERROR_INVALID_NUMBER = "javax.faces.validator.LongRangeValidator.TYPE_detail";
var MSG_ERROR_MAX_LENGTH = "javax.faces.validator.LengthValidator.MAXIMUM_detail";
var MSG_ERROR_MIN_LENGTH = "javax.faces.validator.LengthValidator.MINIMUM_detail";
var MSG_ERROR_EMAIL_INUSE = "error.register.email.inuse";
var MSG_ERROR_INVALID_EMAIL = "org.apache.myfaces.Email.INVALID_detail";
var MSG_ERROR_INVALID_EMAIL_DOMAIN = "error.banner.contactme.email";
var MSG_ERROR_REGISTER_MONTH = "error.register.month";
var MSG_ERROR_REGISTER_YEAR = "error.register.year";
var MSG_ERROR_AMOUNT_INVALID_NUMBER = "javax.faces.validator.RegexValidator.NOT_MATCHED";
var MSG_ERROR_ACCEPT_TERMS = "error.register.terms";
var MSG_ERROR_UKASH_VAUCHER = "ukash.voucher.error";
var MSG_ERROR_REGISTER_FORBIDDEN = "error.register.forbidden";

var nameValidationPattern = translatedTxt[REGEX_GENERAL_VALIDATE_NAME];
var nameValidationPattern_for_replace = new RegExp((nameValidationPattern+'').replace(/\//g,'').replace('^[','[^').replace('+$',''),'g');
var streetValidationPattern = translatedTxt[REGEX_GENERAL_VALIDATE_STREET];
var lettersOnlyPattern = translatedTxt[REGEX_GENERAL_VALIDATE_LETTERS_ONLY];

var lettersAndNumbersPattern  = /^[0-9a-zA-Z]+$/;
var phoneRegEx  = /^\d{7,20}$/;
var phoneRegEx_for_replace  = /\D/g;
var emailValidationRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var numberRegEx = /^\d+$/;
var englishLettersOnlyRegEx = /^['a-zA-Z ]+$/;

var idPrefix = "funnel_";
if(global_deposit_prefix != ""){
	idPrefix = global_deposit_prefix;
}
var errorSignSuffix = "error_sign";
var errorTextSuffix = "error_txt";
var errorTextSuffix_in = "error_txt_in";
var errorOkSuffix = "error_ok";
var singleErrorTagId = "validation_error_txt";

var isShowSingleErrorText = false;
var isShowErrors = true;
var isAsync = true;

var errorMap = new LinkedHashMap();

// -1 for infinity
var showMaxErrors = 1;
if(typeof register_page !== 'undefined' && register_page === true) {
	showMaxErrors = -1;
}

function g(id) {
	return document.getElementById(id);
}

function trim(str) {
    str = str.replace(/^\s+/, '');
    for (var i = str.length - 1; i >= 0; i--) {
        if (/\S/.test(str.charAt(i))) {
            str = str.substring(0, i + 1);
            break;
        }
    }
    return str;
}

function isEmpty(val) {
	return trim(val) === "";
}

function isNumber(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}

function getTagDefaultValue(tag) {
	return (tag.getAttribute('data-def') != undefined)?tag.getAttribute('data-def'):(tag.alt != '')?tag.alt:tag.defaultValue;
}

String.prototype.format = function() {
    var args = arguments;
    return this.replace(/\{(\d+)\}/g,
    					function() {
    						return args[arguments[1]];
    					});
};

function getErrorId(tag) {
	return ((tag.getAttribute('data-errors_id') != undefined) ? (idPrefix + tag.getAttribute('data-errors_id')) : tag.id);
}

function checkError(msg, tag) {
	var errorId = getErrorId(tag);
	var errSignTag = g(errorId + "_error");
	var errTxtTag = null;
	if (msg === null) {
		errSignTag.className = "funnel_error_good";
		tag.className = tag.className.replace(/error/g,'');
		tag.className = tag.className.replace(/active/g,'');
		tag.className += " correct";
		if (errorMap.remove(tag.id) !== null) {
			if (!isShowSingleErrorText) {
				errTxtTag = g(errorId + "_" + errorTextSuffix);
				errTxtTag.innerHTML = "";
				errTxtTag.style.display = "none";
				errTxtTag = g(errorId + "_" + errorTextSuffix_in);
				errTxtTag.innerHTML = "";
				errTxtTag.style.display = "none";
			}
		}
	} else {
		errorMap.remove(tag.id);
		errSignTag.className = "funnel_error_hide";
		tag.className = tag.className.replace(/error/g,'');
		tag.className = tag.className.replace(/active/g,'');
		tag.className += " correct";
		errorMap.put(tag.id, {"errorId":errorId, "msg":msg});
	}
	
	if(isShowErrors) {
		showErrors();
	}
}

function showErrors() {
	try{
		var errTxtTagBlocked = g("funnel_blocked_error_txt");
		errTxtTagBlocked.innerHTML = "";	
	} catch(e) {
		//do nothing sometimes we dont have this msg so we will get null
	}

	if (errorMap.size() > 0) {
		var keys = errorMap.keys();
		var errTxtTag = null;
		if (isShowSingleErrorText) {
			errTxtTag = g(singleErrorTagId);
			errTxtTag.innerHTML = errorMap.get(keys[errorMap.size() - 1]).msg;
			errTxtTag.style.display = "";
		}
		var id = null;
		var errSignTag = null;
		for (var errIdx = errorMap.size() - 1; errIdx >= 0; errIdx--) {
			id = keys[errIdx];
			errSignTag = g(errorMap.get(id).errorId + "_error");
			if (showMaxErrors > 0 && (errorMap.size() - errIdx) > showMaxErrors) {
				errSignTag.className = "funnel_error_hide";
				var el = g(id);
				el.className = el.className.replace(/error/g,'');
				el.className = el.className.replace(/active/g,'');
				el.className += " correct";
				if (!isShowSingleErrorText) {
					errTxtTag = g(errorMap.get(id).errorId + "_" + errorTextSuffix);
					errTxtTag.innerHTML = "";
					errTxtTag.style.display = "none";
					errTxtTag = g(errorMap.get(id).errorId + "_" + errorTextSuffix_in);
					errTxtTag.innerHTML = "";
					errTxtTag.style.display = "none";
				}
			} else {
				errSignTag.className = "funnel_error_bad";
				var el = g(id);
				el.className += " error";
				el.className = el.className.replace(/correct/g,'');
				if (!isShowSingleErrorText) {
					errTxtTag = g(errorMap.get(id).errorId + "_" + errorTextSuffix);
					errTxtTag.innerHTML = errorMap.get(id).msg;
					errTxtTag.style.display = "";
					errTxtTag = g(errorMap.get(id).errorId + "_" + errorTextSuffix_in);
					errTxtTag.innerHTML = errorMap.get(id).msg;
					errTxtTag.style.display = "";
				}
			}
		}
	}
}
function showErrorsBlocked(marked){
	var errDiv = g("funnel_error_list").getElementsByTagName("span");	
	for( var i = 0; i<errDiv.length; i++ ){
		errDiv[i].innerHTML="";
	}       
	if(marked){
		var errSignTag = g("funnel_drop_h");
		errSignTag.className = "funnel_drop_h error";
	}	

	errTxtTag = g("funnel_blocked_error_txt");
	errTxtTag.innerHTML = translatedTxt[MSG_ERROR_REGISTER_FORBIDDEN];
	errTxtTag.style.display = "";
	
	var btn_open_account = g('btn_open_account');
	btn_open_account.className = btn_open_account.className.replace(" disabled","");
	btn_open_account.onclick = function(){funnel_submit();return false;}
}
function checkEmpty(tag, msgKey) {
	var val = tag.value.toUpperCase();
	var def = getTagDefaultValue(tag).toUpperCase();
	if (isEmpty(val)||(val === def)) {
		if (!isShowErrors) {
			checkError(translatedTxt[msgKey], tag);
		} else {
			errorMap.remove(tag.id);
			var errorId = getErrorId(tag);
			var errSignTag = g(errorId + "_error");
			errSignTag.className = "funnel_error_hide";
			
			var errTxtTag = g(errorId + "_" + errorTextSuffix);
			errTxtTag.innerHTML = "";
			errTxtTag.style.display = "none";
			
			errTxtTag = g(errorId + "_" + errorTextSuffix_in);
			errTxtTag.innerHTML = "";
			errTxtTag.style.display = "none";
			
			tag.className = tag.className.replace(/error/g,'');
			tag.className = tag.className.replace(/active/g,'');
		}
		return true;
	}
	
	return false;
}

function validateEmail(tag) {
	if (tag == null) {
		tag = g(idPrefix + "email");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	var val = tag.value;
	if (emailValidationRegEx.test(val)) {
		validateEmailService(tag);
		/* !!! WARNING !!! Method Exit point! */
		return;
	} else {
		msg = translatedTxt[MSG_ERROR_INVALID_EMAIL];
	}
	checkError(msg, tag);
}

function validateEmailService(emailTag) {
	xmlHttp = getXMLHttp();
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState == 4) {
			var msg = null;
			if (xmlHttp.status === 200) {
				var result = JSON.parse(xmlHttp.responseText).returnCode;
				switch (result) {
					case 0:
						// OK
						break;
					case 1:
						msg = translatedTxt[MSG_ERROR_INVALID_EMAIL];
						break;
					case 2:
						msg = translatedTxt[MSG_ERROR_INVALID_EMAIL_DOMAIN];
						break;
					case 3:
						msg = translatedTxt[MSG_ERROR_EMAIL_INUSE];
						break;
				}
			} else {
				msg = translatedTxt[MSG_ERROR_INVALID_EMAIL];
			}
			checkError(msg, emailTag);
		}
	}
	xmlHttp.open("POST", context_path_ajax+"/jsonService/AnyoptionService/validateEmail", isAsync);
	xmlHttp.setRequestHeader("Content-Type", "json");
	xmlHttp.setRequestHeader("Accept-Language","en-US,en;q=0.5");
	xmlHttp.send(JSON.stringify({"email" : emailTag.value}));
}

function validateFirstName(tag) {
	if (tag == null) {
		tag = g(idPrefix + "firstName");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	var val = tag.value;
	if (!nameValidationPattern.test(val)) {
		msg = translatedTxt[MSG_ERROR_LETTERS_ONLY].format("");
	}
	checkError(msg, tag);
}

function validateLastName(tag) {
	if (tag == null) {
		tag = g(idPrefix + "lastName");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	var val = tag.value;
	if (!nameValidationPattern.test(val)) {
		msg = translatedTxt[MSG_ERROR_LETTERS_ONLY].format("");
	}
	checkError(msg, tag);
}

function validatePhoneNumberTag(tag) {
	if (tag == null) {
		tag = g(idPrefix + "mobilePhone");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	var val = tag.value;
	var defVal = getTagDefaultValue(tag);
	if (numberRegEx.test(val)) {
		if (val.length < 7) {
			msg = translatedTxt[MSG_ERROR_MIN_LENGTH].format("7", defVal);
		} else if (val.length > 20) {
			msg = translatedTxt[MSG_ERROR_MAX_LENGTH].format("20", defVal);
		}
	} else {
		msg = translatedTxt[MSG_ERROR_INVALID_NUMBER];
	}
	
	checkError(msg, tag);
}

function validatePassword(tag) {
	if (tag == null) {
		tag = g(idPrefix + "password");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	var val = tag.value;
	if (lettersAndNumbersPattern.test(val)) {
		if (val.length < 6) {
			msg = translatedTxt[MSG_ERROR_PASSWORD_SMALLSIZE];
		} else if(val.length > 9) {
			msg = translatedTxt[MSG_ERROR_PASSWORD_BIGSIZE];
		}
	} else {
		msg = translatedTxt[MSG_ERROR_LETTERSANDNUMBERS_ONLY];
	}
	checkError(msg, tag);
}

function validatePasswordConfirm(tag) {
	if (tag == null) {
		tag = g(idPrefix + "passwordConfirm");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	if (g(idPrefix + "password").value !== tag.value) {
		msg = translatedTxt[MSG_RETYPE_PASS_NOT_EQUAL];
	}
	checkError(msg, tag);
}

function validateStreet(tag) {
	if (tag == null) {
		tag = g(idPrefix + "street");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	if(!streetValidationPattern.test(tag.value)) {
		msg = translatedTxt[MSG_ERROR_STREET];
	}
	checkError(msg, tag);
}

function validateCityName(tag) {
	if (tag == null) {
		tag = g(idPrefix + "cityNameAO");
	}
	
	if (checkEmpty(tag, MSG_ERROR_REGISTER_LETTERS_ONLY)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	if (!lettersOnlyPattern.test(tag.value)) {
		msg = translatedTxt[MSG_ERROR_LETTERS_ONLY];
	}
	checkError(msg, tag);
}

function validateCCType(tag) {
	if (tag == null) {
		tag = g(idPrefix + "typeId");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	checkError(null, tag);
}

function validateCCSelect(tag) {
	if (tag == null) {
		tag = g(idPrefix + "ccNum");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	checkError(null, tag);
}

function validateCCNumber(tag) {
	if (tag == null) {
		tag = g(idPrefix + "ccNum");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	validateCCNumberService(tag);
}

function validateCCNumberService(ccNumTag) {
	xmlHttp = getXMLHttp();
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState == 4) {
			var msg = null;
			if (xmlHttp.status === 200) {
				var result = JSON.parse(xmlHttp.responseText);
				if (!result.valid) {
					if (result.diners) {
						msg = translatedTxt[MSG_ERROR_CREDITCARD_DINERS_NOTSUPPORTED];
					} else {
						msg = translatedTxt[MSG_ERROR_CREDITCARD_NUM];
					}
				}
			} else {
				msg = translatedTxt[MSG_ERROR_CREDITCARD_NUM];
			}
			checkError(msg, ccNumTag);
		}
	}
	xmlHttp.open("POST", "/jsonService/AnyoptionService/validateCC", isAsync);
	xmlHttp.setRequestHeader("Content-Type", "json");
	xmlHttp.setRequestHeader("Accept-Language","en-US,en;q=0.5");
	xmlHttp.send(JSON.stringify({"ccNumber" : ccNumTag.value}));
}

// CVV code
function validateCCPass(tag) {
	if (tag == null) {
		tag = g(idPrefix + "ccPass");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	var val = tag.value;
	if (numberRegEx.test(val)) {
		var ccTypeTag = g(idPrefix + "typeId");
		if (typeof ccTypeTag !== "undefined") {
			if (!isEmpty(ccTypeTag.value)) {
				if (ccTypeTag.value === CARD_TYPE_AMEX) {
					if (val.length !== CARD_PASS_LENGTH_AMEX) {
						msg = translatedTxt[MSG_ERROR_CVVLENGTH_AMEX];
					}
				} else if (val.length !== CARD_PASS_LENGTH) {
					msg = translatedTxt[MSG_ERROR_CVVLENGTH];
				}
			} else {
				// do nothing
			}
		} else {
			alert("CC type not defined");
		}
	} else {
		// not a number
		msg = translatedTxt[MSG_ERROR_CVV];
	}
	checkError(msg, tag);
}

function validateExpMonth(tag) {
	if (tag == null) {
		tag = g(idPrefix + "expMonth");
	}
	
	if (checkEmpty(tag, MSG_ERROR_REGISTER_MONTH)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	checkError(null, tag);
}

function validateExpYear(tag) {
	if (tag == null) {
		tag = g(idPrefix + "expYear");
	}
	
	if (checkEmpty(tag, MSG_ERROR_REGISTER_YEAR)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	checkError(null, tag);
}

function validateHolderName(tag) {
	if (tag == null) {
		tag = g(idPrefix + "holderName");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	var val = tag.value;
	if (skinId === SKIN_ETRADER) {
		if (!lettersOnlyPattern.test(val)) {
			msg = translatedTxt[MSG_ERROR_LETTERS_ONLY];
		}
	} else {
		if (!englishLettersOnlyRegEx.test(val)) {
			msg = translatedTxt[MSG_ERROR_ENGLISH_LETTERS_ONLY];
		}
	}
	checkError(msg, tag);
}

function validateZipCode(tag) {
	if (tag == null) {
		tag = g(idPrefix + "zipCode");
	}
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	checkError(null, tag);
}

function validateDepositAmount(tag) {
	if (tag == null) {
		tag = g(idPrefix + "deposit");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	if (!isNumber(tag.value)) {
		msg = translatedTxt[MSG_ERROR_AMOUNT_INVALID_NUMBER];
	}
	checkError(msg, tag);
}

function validateCheckbox() {
	var terms = g(idPrefix + "terms");
	
	var msg = null;
	if (!terms.checked) {
		msg = translatedTxt[MSG_ERROR_ACCEPT_TERMS];
	}
	checkError(msg, terms);
}

function validateDirectAccNum(tag) {
	if (tag == null) {
		tag = g(idPrefix + "accNum");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	if (!numberRegEx.test(tag.value)) {
		msg = translatedTxt[MSG_ERROR_INVALID_NUMBER];
	}
	checkError(msg, tag);
}

function validateSortCode(tag) {
	if (tag == null) {
		tag = g(idPrefix + "sortCode");
	}
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	checkError(null, tag);
}

function validateBenefiName(tag) {
	if (tag == null) {
		tag = g(idPrefix + "benefiName");
	}
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	checkError(null, tag);
}

function validateSenderName(tag) {
	if (tag == null) {
		tag = g(idPrefix + "senderName");
	}
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	checkError(null, tag);
}

function validateBaroLandLinePhone(tag) {
	if (tag == null) {
		tag = g(idPrefix + "landLinePhone");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	var val = tag.value;
	var defVal = getTagDefaultValue(tag);
	if (numberRegEx.test(val)) {
		if (val.length < 7) {
			msg = translatedTxt[MSG_ERROR_MIN_LENGTH].format("7", defVal);
		} else if (val.length > 8) {
			msg = translatedTxt[MSG_ERROR_MAX_LENGTH].format("8", defVal);
		}
	} else {
		msg = translatedTxt[MSG_ERROR_INVALID_NUMBER];
	}
	checkError(msg, tag);
}

function validateUkashVoucherNumber(tag) {
	if (tag == null) {
		tag = g(idPrefix + "voucherNumber");
	}
	
	if (checkEmpty(tag, MSG_ERROR_MANDATORY_FIELD)) {
		/* !!! WARNING !!! Method Exit point! */
		return;
	}
	
	var msg = null;
	var val = tag.value;
	if (!isNumber(val)) { // double value
		msg = translatedTxt[MSG_ERROR_INVALID_NUMBER];
	} else if (val.length !== 19) {
		msg = translatedTxt[MSG_ERROR_UKASH_VAUCHER];
	}
	checkError(msg, tag);
}
function passGoodKeys(keyId){
	var allowedKeys = [
		8,//backspace
		9,//tab
		13,//enter
		16,//shift
		17,//ctr
		18,//alt
		46,//delete
		36,//home
		35,//end
	]
	if(allowedKeys.indexOf(keyId) === -1){
		return false;
	}
	return true;
}
function limitLetters(event,position){
	var field = event.target;
	var tooltip = g(field.getAttribute('data-letters_tooltip'));
	if(nameValidationPattern.test(field.value)){
		tooltip.style.display = "none";
	}
	else{
		tooltip.style.display = "block";
		tooltip.className = "funnel_tooltip_h"+position;
		tooltip.style.top = -(tooltip.offsetHeight/2)+(field.offsetHeight/2)+'px';
		field.value = field.value.replace(nameValidationPattern_for_replace,'');
		// event.preventDefault();
	}
}
function limitNumbers(event){
	var field = event.target;
	var tooltip = g(field.getAttribute('data-letters_tooltip'));
	var tooltip_in1 = g(field.getAttribute('data-letters_tooltip')+"_1");
	var tooltip_in2 = g(field.getAttribute('data-letters_tooltip')+"_2");
	if(numberRegEx.test(field.value)){
		tooltip_in1.style.display = "block";
		tooltip_in2.style.display = "none";
		tooltip.style.top = -(tooltip.offsetHeight/2)+(field.offsetHeight/2)+'px';
	}
	else{
		tooltip_in1.style.display = "none";
		tooltip_in2.style.display = "block";
		tooltip.style.top = -(tooltip.offsetHeight/2)+(field.offsetHeight/2)+'px';
		field.value = field.value.replace(phoneRegEx_for_replace,'');
		// event.preventDefault();
	}
}
function hide_tooltip(field){
	var tooltip = g(field.getAttribute('data-letters_tooltip'));
	tooltip.style.display = "none";
}
addScriptToHead('regForm_map.jsf?s='+skinId,'initRegForm_map');