		function getNewChart(item, xStart, yRef) {
			chart = new ChartLine();

			chart.setPointClass("investmentPopupPoint");
			chart.setLineClass("investmentPopupLine");
			xStart = new String(xStart);
			yRef = new String(yRef);

			maxX = new Number(xStart) + 60;
			minX = xStart;
			log("minX: " + minX + " maxX: " + maxX);
			gTable.positionXAxis(minX, maxX);

			minY = yRef.replace(",", ".") * 0.9999;
			maxY = yRef.replace(",", ".") * 1.0001;
			chart.setYAxis(item, "#price_value", true);
			chart.positionYAxis(minY, maxY);

			var lblFrmttr = {};
			lblFrmttr.formatValue = formatYlbl;
			chart.setYLabels(3, "investmentPopupYLevel", lblFrmttr);

			return chart;
		}

		function onChartUpdate(item, upOb) {
			if (upOb == null) {
				log("No object");
				return;
			}

			// check for a void update, so as to ignore it;
			// the case is possible when testing the Remote version of the Data Adapter
			// in bundle with the Robust version of the Proxy Adapter
			if (upOb.getNewValue("AO_LEVEL") == null) {
				log("No AO_LEVEL");
				return;
			}

			upOb.addField("#seconds", getCurrentSeconds(), true);
			var newY = yPosition(upOb.getNewValue("AO_LEVEL"));
			if (newY < minY || newY > maxY) {
				if (newY < minY) {
					minY = newY;
					maxY = investmentCurrentLevel + investmentCurrentLevel - minY;
				}
				if (newY > maxY) {
					maxY = newY;
					minY = investmentCurrentLevel - maxY + investmentCurrentLevel;
				}
				gLine.positionYAxis(minY, maxY);
			}
			upOb.addField("#price_value", newY, true);
			log("Update - AO_LEVEL: " + upOb.getOldValue("AO_LEVEL") + " seconds: " + upOb.getNewValue("#seconds") + " price: " + upOb.getNewValue("#price_value"));
			var aValue = upOb.getOldValue("AO_LEVEL");
			if (aValue == null) { //first update for this item
				log("New chart");
				if (!gLine) {
					var fx = upOb.getNewValue("#seconds");
//					var fy = upOb.getNewValue("#price_value");
					gLine = getNewChart(item, fx, investmentCurrentLevel);
					gTable.addLine(gLine, "t" + item);
				}
			}
		}

		function getCurrentSeconds() {
			var d = new Date();
			return (d.getHours() * 3600 + d.getMinutes() * 60 + d.getSeconds());
		}

		function yPosition(yValue) {
			var y = new String(yValue);
			if (y.indexOf(",") > -1 ) {
				var y = y.replace(",", "");
			}
			return new Number(y);
		}

		function formatYlbl (val) {
			return formatDecimal(val, decimalPoint, true);
		}

		function formatXlbl (val) {
			return formatTime(getTime(val));
		}

		function getTime(secondsStr) {
			var hours = Math.floor(secondsStr / (60 * 60));
			var seconds = secondsStr - (hours * (60 * 60));
			var minutes = Math.floor(seconds / 60);
			var seconds = Math.round(seconds - (minutes * 60));
		
			if (minutes.toString().length < 2) {
				minutes = ":0" + minutes;
			} else {
				minutes = ":" + minutes;
			}
		
			if (seconds.toString().length < 2) {
				seconds = ":0" + seconds;
			} else {
				seconds = ":" + seconds;
			}
		
			return hours +  minutes + seconds;
		}


  // Formatting Functions
  function formatTime(val) {
    var a = new Number(val.substring(0,val.indexOf(":")));
    if (a > 12) {
      a -= 12;
    }
    var b = val.substring(val.indexOf(":"),val.length);
    return a + b;
  }

  function formatDecimal(value, decimals, keepZero) {
    var mul = new String("1");
    var zero = new String("0");
    for (var i = decimals; i > 0; i--) {
      mul += zero;
    }
    value = Math.round(value * mul);
    value = value / mul;
    var strVal = new String(value);
    if (!keepZero) {
      return strVal;
    }

    var nowDecimals = 0;
    var dot = strVal.indexOf(".");
    if (dot == -1) {
      strVal += ".";
    } else {
      nowDecimals = strVal.length - dot - 1;
    }
    for (var i = nowDecimals; i < decimals; i++) {
      strVal = strVal + zero;
    }
    return strVal;
  }
