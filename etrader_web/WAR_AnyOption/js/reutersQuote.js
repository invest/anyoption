var alignC = 'tlbr';
if (skinId == 4 || skinId == 11) {   // arabic skin
	alignC = 'trbl';
}
var classAtr = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'className' : 'class';
// configuration variable for the hint object, these setting will be shared among all hints created by this object
var RQ_HINTS_CFG = {
	'smart'      : true, // don't go off screen, don't overlap the object in the document
	'margin'     : 10, // minimum allowed distance between the hint and the window edge (negative values accepted)
	'gap'        : 10, // minimum allowed distance between the hint and the origin (negative values accepted)
	'align'      : alignC, // align of the hint and the origin (by first letters origin's top|middle|bottom left|center|right to hint's top|middle|bottom left|center|right)
	//'css'        : 'hintsReutersClass', // a style class name for all hints, applied to DIV element (see style section in the header of the document)
	'show_delay' : 0, // a delay between initiating event (mouseover for example) and hint appearing
	'hide_delay' : 200, // a delay between closing event (mouseout for example) and hint disappearing
	'follow'     : false, // hint follows the mouse as it moves
	'z-index'    : 100, // a z-index for all hint layers
	'IEfix'      : false, // fix IE problem with windowed controls visible through hints (activate if select boxes are visible through the hints)
	'IEtrans'    : ['blendTrans(DURATION=.3)', null], // [show transition, hide transition] - nice transition effects, only work in IE5+
	'opacity'    : 100 // opacity of the hint in %%
};
// text/HTML of the hints
var newrqHintDiv = [
	"<div id='newRQHint' style='background-color: transparent;'></div>"
];

var rqHintOpen = new THints (newrqHintDiv, RQ_HINTS_CFG);
function showRQHint(oppId) {
	var iframe = document.createElement('iframe');
	iframe.setAttribute("class", "hintsReutersClass");
	iframe.scrolling = "no";
	iframe.src = "http://" + window.location.hostname + context_path + "/jsp/reuters_quotes_popup.jsf?oppId=" + oppId;
	iframe.style.width = 385 + "px";
//	iframe.style.height = 210 + "px";
	iframe.frameBorder = "0";   //  For other browsers.
    iframe.setAttribute("frameBorder", "0");   //  For other browsers (just a backup for the above).
  //iframe.contentWindow.document.body.style.border = "none";   //  For IE.
	document.getElementById('newRQHint').innerHTML = "";
	document.getElementById('newRQHint').appendChild(iframe);

	rqHintOpen.show(0);
}

function hideRQHint() {
	rqHintOpen.hide();
}