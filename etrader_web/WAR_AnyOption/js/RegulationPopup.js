 function closeRegulationPopup() {
  		var	elem = document.getElementById('boxes');
  	    elem.parentNode.removeChild(elem);
  		$('#mask_tm').hide();
  		$('.window_tm').hide();
  		if(document.getElementById('FlashID')){
  			document.getElementById('FlashID').style.display = "block";
  		} 
  		window.location.reload();
 }
function showRegulationPopUp(htmlPath,flag){
    	     	 
     if(flag){
     var id = '#dialog';
     
	//Get the screen height and width
	var maskHeight = $(document).height();
	var maskWidth = $(window).width();

	//Set heigth and width to mask to fill up the whole screen
	$('#mask').css({'width':maskWidth,'height':maskHeight});
	
	//transition effect		
	$('#mask').fadeIn(1000);	
	$('#mask').fadeTo("slow",0.8);	

	//Get the window height and width
	var winH = $(window).height();
	var winW = $(window).width();


    //load Template in dialog div
    //$(id).load("#{facesContext.externalContext.requestContextPath}/jsp/ao_Regulation_popup.html")
	$(id).load(htmlPath);
	//Set the popup window to center
	$(id).css('top',  winH/2-$(id).height()/2);
	$(id).css('left', winW/2-$(id).width()/2);

	//transition effect
	$(id).fadeIn(2000); 	
 
     }
   }