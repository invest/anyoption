// THIS FILE WAS COPY TO: common_before.js


//put the golden div in the right position on the page345
//! useing tigra hint!
function positionDiv(positionDiv, showDiv, left, top) {
	var mTd = positionDiv;
	var conDiv = showDiv;
	if (null != mTd && null != conDiv) {
		$(conDiv).css("left", f_getPositionDiv(mTd, 'Left') + left);
		$(conDiv).css("top", f_getPositionDiv(mTd, 'Top') - top);
	}
}

//position the banner in the right place on the page
function f_getPositionDiv (e_elemRef, s_coord) {
//	log("f_getPosition e_elemRef: " + e_elemRef + " s_coord: " + s_coord);

	var n_pos = 0, n_offset,
		e_elem = e_elemRef;

	while (e_elem) {
		n_offset = e_elem["offset" + s_coord];
		n_pos += n_offset;
		e_elem = e_elem.offsetParent;
	}

	// margin correction in some browsers (not supported for GM)
	if (false) {
		n_pos += parseInt(document.body[s_coord.toLowerCase() + 'Margin']);
	}

	e_elem = e_elemRef;
	while (e_elem != document.body) {
		n_offset = e_elem["scroll" + s_coord];
		if (n_offset && e_elem.style.overflow == 'scroll')
			n_pos -= n_offset;
		e_elem = e_elem.parentNode;
	}

//	log("f_getPosition end.");
	return n_pos;
}