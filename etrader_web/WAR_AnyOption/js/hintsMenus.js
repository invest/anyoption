/*********** hintsMenus.js ************/

		// skins menu

		var alignSK = 'bltl';

		if (skinId == 4 || skinId == 11) {   // arabic skin
			alignSK = 'bltl';
		}
		var HINTS_SK = {
			'smart'      : false, // don't go off screen, don't overlap the object in the document
			'margin'     : 0, // minimum allowed distance between the hint and the window edge (negative values accepted)
			'gap'        : 0, // minimum allowed distance between the hint and the origin (negative values accepted)
			'align'      : alignSK, // align of the hint and the origin (by first letters origin's top|middle|bottom left|center|right to hint's top|middle|bottom left|center|right)
			'css'        : 'skins_menu', // a style class name for all hints, applied to DIV element (see style section in the header of the document)
			'show_delay' : 0, // a delay between initiating event (mouseover for example) and hint appearing
			'hide_delay' : 200, // a delay between closing event (mouseout for example) and hint disappearing
			'follow'     : false, // hint follows the mouse as it moves
			'z-index'    : 30, // a z-index for all hint layers
			'IEfix'      : false, // fix IE problem with windowed controls visible through hints (activate if select boxes are visible through the hints)
			'IEtrans'    : ['blendTrans(DURATION=.3)', null], // [show transition, hide transition] - nice transition effects, only work in IE5+
			'opacity'    : 99 // opacity of the hint in %%
		};
		// text/HTML of the hints
		var skinsMenuDiv = [
			'<div id="skinSho" style="width:80px;display:block"></div>'
		];

		var skinsMenuOpen = new THints (skinsMenuDiv, HINTS_SK);
		function showSelectionSkins(link, evt) {
			evt.cancelBubble = true;
			var isVisible = false;
	        if (document.getElementById('skinSho').parentNode.style.display == 'block') {
	        	isVisible = true;
	        }
	        hideMenus();
	        if (!isVisible) {
				document.getElementById('skinSho').innerHTML = document.getElementById('skinsM').innerHTML;
				skinsMenuOpen.show(0, link);
			}
		}

		function hideSelectionSkins(link) {
			skinsMenuOpen.hide();
		}

		var alignM = 'bltl';
		var alignS = 'bltl';

		if (window.top.location.href.indexOf(homePageN) > -1 && (skinId == 4 || skinId == 11)) {   // used just in the index ,arabic skin
			alignM = 'bltl';
			alignS = 'bltl';
		}

		var HINTS_marketMenu = {
			'smart'      : false, // don't go off screen, don't overlap the object in the document
			'margin'     : 0, // minimum allowed distance between the hint and the window edge (negative values accepted)
			'gap'        : 0, // minimum allowed distance between the hint and the origin (negative values accepted)
			'align'      : alignM, // align of the hint and the origin (by first letters origin's top|middle|bottom left|center|right to hint's top|middle|bottom left|center|right)
			'css'        : 'tree_bkgnd', // a style class name for all hints, applied to DIV element (see style section in the header of the document)
			'show_delay' : 0, // a delay between initiating event (mouseover for example) and hint appearing
			'hide_delay' : 0, // a delay between closing event (mouseout for example) and hint disappearing
			'follow'     : false, // hint follows the mouse as it moves
			'z-index'    : 109, // a z-index for all hint layers
			'IEfix'      : false, // fix IE problem with windowed controls visible through hints (activate if select boxes are visible through the hints)
			'IEtrans'    : ['blendTrans(DURATION=.3)', null], // [show transition, hide transition] - nice transition effects, only work in IE5+
			'opacity'    : 0 // opacity of the hint in %%
		};

		var HINTS_SCHEDULE = {
			'smart'      : false, // don't go off screen, don't overlap the object in the document
			'margin'     : 0, // minimum allowed distance between the hint and the window edge (negative values accepted)
			'gap'        : 0, // minimum allowed distance between the hint and the origin (negative values accepted)
			'align'      : alignS, // align of the hint and the origin (by first letters origin's top|middle|bottom left|center|right to hint's top|middle|bottom left|center|right)
			'css'        : 'tree_bkgnd', // a style class name for all hints, applied to DIV element (see style section in the header of the document)
			'show_delay' : 0, // a delay between initiating event (mouseover for example) and hint appearing
			'hide_delay' : 0, // a delay between closing event (mouseout for example) and hint disappearing
			'follow'     : false, // hint follows the mouse as it moves
			'z-index'    : 143, // a z-index for all hint layers
			'IEfix'      : false, // fix IE problem with windowed controls visible through hints (activate if select boxes are visible through the hints)
			'IEtrans'    : ['blendTrans(DURATION=.3)', null], // [show transition, hide transition] - nice transition effects, only work in IE5+
			'opacity'    : 99 // opacity of the hint in %%
		};
		// text/HTML of the hints
		var treeMenuDiv = [
			'<div style="width:148px;height:170px;overflow-y:scroll;overflow-x:hidden;z-index: 105;" id="treeSho" ></div>'
		];
		var scheduleMenuDiv = [
			'<div style="width:120px;" onmouseup="hideSelectionSchedule(this);" id="scheduleMenuSho"></div>'
		]
		
		var selectorMenuDiv = [
			'<div style="width:130px; onmouseup="hideSelectionSchedule(this);" id="selectorMenuSho"></div>'
		]

		var treeMenuOpen = new THints (treeMenuDiv, HINTS_marketMenu);
		var scheduleMenu = new THints (scheduleMenuDiv,HINTS_SCHEDULE);
		var selectorMenu = new THints (selectorMenuDiv,HINTS_SCHEDULE);
		
		var TigraBoxMarketId = null;
		function showSelectionTree(link, marketId, evt) {
	        evt.cancelBubble = true;
	        var isVisible = false;
	        if (document.getElementById('treeSho').parentNode.style.display == 'block') {
	        	isVisible = true;
	        }
			hideMenus();
			if (!isVisible) { //if it was visible dont show it
				document.getElementById('treeSho').innerHTML = "";
				setCss(marketId);
				document.getElementById('treeSho').innerHTML = document.getElementById('treeM').innerHTML;
				treeMenuOpen.show(0, link);
				//save the opp id so we will know which box to replace
				TigraBoxMarketId = marketId;
			}
		}

		function hideSelectionTree(link) {
			treeMenuOpen.hide(0,link);
		}

		//show the markets select box in the mobile
		//setting focus in the mobile make the select box to display like u click on it.
		//select box id is : oppid_listName
		//@elm the element we click on
		//@oppId the opp id in the box that the select box is
		function showMarketsSelect(elm, oppId, marketId) {
			var marketSelectL = elm.parentNode.getElementsByTagName("SELECT")[0];
			markClosedMarket(marketSelectL, marketId);
			marketSelectL.style.display = "block";
			marketSelectL.focus();
			marketSelectL.style.display = "none";
		}

		//b4 showing the market list mark which the close market with -
		function markClosedMarket(marketSelectL, marketId) {
			var state;
			for (var i = 0; i < marketSelectL.options.length; i++) {
				state = null;
				optionMarketId = marketSelectL.options[i].value;
				for (var j = 0; j < marketStates.length; j++) {
					if (marketStates[j][0] == optionMarketId) {
	   					state = marketStates[j][1];
	   					break;
	   				}
	   			}
	   			var closePosition = marketSelectL.options[i].text.indexOf(" " + marketStateClose);
	   			if (closePosition > -1) {
	   				marketSelectL.options[i].text = marketSelectL.options[i].text.substring(0, closePosition);
	   			}
	   			if ((null == state) || (state == 0)) {
					marketSelectL.options[i].text = marketSelectL.options[i].text + " " + marketStateClose;
	   			}

			    if (marketSelectL.options[i].value == marketId) {
			    	marketSelectL.options[i].selected = true;
			    }
		  	}
		}

		//show the scheduled select box in the mobile
		//setting focus in the mobile make the select box to display like u click on it.
		//select box id is : oppid_listName
		//@oppId the opp id in the box that the select box is
		//@listName the name of the select box u want to show
		function showMarketsScheduled(oppId, listName) {
			var marketSelectL = document.getElementById(oppId + "_" + listName);
			removeScheduledOptions(marketSelectL, oppId);
			marketSelectL.style.display = "block";
			marketSelectL.focus();
			marketSelectL.style.display = "none";
		}

		//change market from mobile trade page
		//@list the list of the scheduleds
		//@marketId the market id of the market that in the box
		//@oppId the oppId of the current box
		function changeMarket(list, marketId, oppId) {
			TigraBoxMarketId = marketId;
			if (isMobileParm) {
				lastSelectedMarket = document.getElementById(oppId + "_" + 25); //the last index that was selected
				lastSelectedMarketlist = list;
			}
			nodeClick(list.value);
		}

		function showSelectionSchedule(link, marketId, oppId, evt) {
			evt.cancelBubble = true;
			var isVisible = false;
	        if (document.getElementById('scheduleMenuSho').parentNode.style.display == 'block') {
	        	isVisible = true;
	        }
	        hideMenus();
	        if (!isVisible) {
				TigraBoxMarketId = marketId;
				market_id = marketId;
				document.getElementById('scheduleMenuSho').innerHTML = document.getElementById('scheduleMenu').innerHTML;
				var scheduleMenuShoDiv = document.getElementById('scheduleMenuSho');
				var scheduleOptions = scheduleMenuShoDiv.getElementsByTagName("TR");
				var filterScheduled = document.getElementById(oppId + "_" + SUBSCR_FLD_FILTER_SCHEDULED).innerHTML;
				switch(filterScheduled) {
					case "1":
					  hideElm(scheduleOptions[1]);
					  hideElm(scheduleOptions[2]);
					  hideElm(scheduleOptions[3]);
					  break;
					case "2":
					  hideElm(scheduleOptions[2]);
					  hideElm(scheduleOptions[3]);
					  break;
					case "3":
					  hideElm(scheduleOptions[3]);
					  break;
					case "4":
					  break;
					default:
				}
				scheduleMenu.show(0, link);
			}
			
			if (isLiveAOPage) {
		    	fromGraph = 2; //Live page - trade box.
		    } else {
		    	fromGraph = 0; //No from LIVE-AO, profitline, option + or binary 0-100
		    }
		}
		
		function showRefundSelector(link, marketId, oppId, evt) {
			var selectorsID = document.getElementById(oppId+'_25').innerHTML;
			var arrSelectorsID = selectorsID.split(" ");
			var returnArr = new Array();
			var refundArr = new Array();			
			var tmpN;
			hideMenus();
			
			var multArr= new Array( arrSelectorsID.length-1); 
			for (i = 0; i < multArr.length; ++ i){
				multArr [i] = new Array(2);
			}
			
	        for (var l = 0; l < arrSelectorsID.length-1; l++) {
	        	tmpN = arrSelectorsID[l];
	        	var labelElementRetrnTmp = document.getElementById(tmpN+"_returnSpan").getElementsByTagName("label");
				var labelElementRefundTmp = document.getElementById(tmpN+"_refundSpan").getElementsByTagName("label");
				var returnValueTmp = labelElementRetrnTmp[0].innerHTML;
				var refundValueTmp = labelElementRefundTmp[0].innerHTML;  				
	        		        		 
	        	multArr[l][0]=parseInt(returnValueTmp);
	        	multArr[l][1]=parseInt(refundValueTmp);
	        	        	
	        }
	        
	        multArr = multArr.sort(function(a, b) { return (a[1] < b[1] ? -1 : (a[1] > b[1] ? 1 : 0)); });	        	        
			evt.cancelBubble = true;
			var tmp;
		    var returnId = oppId + '_8';
            var returnVal = document.getElementById(returnId).innerHTML;
            returnVal = returnVal- 1;                
			var refundId = oppId+'_9';
			var refundVal = document.getElementById(refundId).innerHTML;
			refundVal = refundVal*100;
			var tmpCurentPct = document.getElementById(oppId+'_5').innerHTML.replace("%","");				
			document.getElementById('selectorMenuSho').innerHTML = document.getElementById('selectorMenu').innerHTML;
			document.getElementById("returnSelectOppId").innerHTML = oppId;
			var table = document.getElementById("selector");
			
			for (var p = 0; p < multArr.length; p++) {
				var table = document.getElementById("selector");
				var row = table.insertRow(p+1);
				var onClikcMetod = "returnSelector("+multArr[p][0]+","+multArr[p][1]+",this);";								
				row.setAttribute("onmouseout", "changeColorsBackSelector(this);");
				row.setAttribute("onmouseover", "changeColorsSelector(this);");
				row.setAttribute("onclick",onClikcMetod);
				row.setAttribute("class","hand");
				row.setAttribute("style","background-color: rgb(255, 255, 255);Font-family: Verdana;Font-side: 10px;Font-weight: normal;Color: #093755;Padding-bottom: 9px;height: 15px;Line-height: 15px;Text-align: center;");									
				if(tmpCurentPct == multArr[p][0]){
					row.style.backgroundColor ='#d7ebf8';row.style.color = "#0284d6"
				}
				var cell1 = row.insertCell(0);
				var cell2 = row.insertCell(1);
				cell1.setAttribute("align","center");
				cell2.setAttribute("align","center");
				var returnOut = selectorRetunrPct.replace("{0}", multArr[p][0]);
				var refundOut = selectorRetunrPct.replace("{0}", multArr[p][1]);; 
				cell1.innerHTML = returnOut;
				cell2.innerHTML = refundOut;        	
			}
			if (skinId == 8 || skinId == 9){
				document.getElementById('selectorMenuSho').style.width = "142px";
			} else if(skinId == 12){
				document.getElementById('selectorMenuSho').style.width = "184px";
			}			
			selectorMenu.show(0, link);			
		}

		function hideSelectionSchedule(link) {
			scheduleMenu.hide(0,link);
		}

		//remove scheduled option that not avilable for this optio
		//@oppId the box opp id
		function removeScheduledOptions(list, oppId) {
			var filterScheduled = document.getElementById(oppId + "_" + SUBSCR_FLD_FILTER_SCHEDULED).innerHTML;
			var Scheduled = document.getElementById(oppId + "_" + SUBSCR_FLD_ET_SCHEDULED).innerHTML;
			list.options[Scheduled - 1].selected = true;
			if (list.length > filterScheduled) {
				switch(filterScheduled) {
					case "1":
						list.remove(3);
						list.remove(2);
						list.remove(1);
					  	break;
					case "2":
						list.remove(3);
						list.remove(2);
					  	break;
					case "3":
						list.remove(3);
					  	break;
					case "4":
					  	break;
					default:
				}
			}
		}

		//change market scheduled from mobile trade page
		//@list the list of the scheduleds
		//@marketId the market id of the market that in the box
		function changeScheduled(list, marketId) {
			TigraBoxMarketId = marketId;
			scheduleSelect(list.value);

		}

		function getInternetExplorerVersion() {
			// Returns the version of Internet Explorer or a -1
			// (indicating the use of another browser).
			var rv = -1; // Return value assumes failure.
			if (navigator.appName == 'Microsoft Internet Explorer') {
			    var ua = navigator.userAgent;
			 	var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
		    	if (re.exec(ua) != null)
					rv = parseFloat( RegExp.$1 );
			  }
			  return rv;
		}

		var alignI = 'mlml';
		var gapIE = -20;
		if (window.top.location.href.indexOf("register.jsf") > -1 && skinId == 4) {
			alignI = 'mrmr';
			var version = getInternetExplorerVersion();
			if (version > -1 && version < 8) {
				gapIE = 90;
			}
		}

		var HINTS_REG = {
			'smart'      : true, // don't go off screen, don't overlap the object in the document
			'margin'     : 0, // minimum allowed distance between the hint and the window edge (negative values accepted)
			'gap'        : gapIE, // minimum allowed distance between the hint and the origin (negative values accepted)
			'align'      : alignI, // align of the hint and the origin (by first letters origin's top|middle|bottom left|center|right to hint's top|middle|bottom left|center|right)
			'css'        : 'hintsClass', // a style class name for all hints, applied to DIV element (see style section in the header of the document)
			'show_delay' : 0, // a delay between initiating event (mouseover for example) and hint appearing
			'hide_delay' : 200, // a delay between closing event (mouseout for example) and hint disappearing
			'follow'     : false, // hint follows the mouse as it moves
			'z-index'    : 0, // a z-index for all hint layers
			'IEfix'      : false, // fix IE problem with windowed controls visible through hints (activate if select boxes are visible through the hints)
			'IEtrans'    : ['blendTrans(DURATION=.3)', null], // [show transition, hide transition] - nice transition effects, only work in IE5+
			'opacity'    : 99 // opacity of the hint in %%
		};
		// text/HTML of the hints
		var currHintDiv = [
			'<div id="regHint"></div>'
		];

		var hintOpen = new THints (currHintDiv, HINTS_REG);
		function showCurrHint(link, divHint) {
			document.getElementById('regHint').innerHTML = document.getElementById(divHint).innerHTML;
			hintOpen.show(0, link);
		}

		function hideCurrHint(link) {
			hintOpen.hide();
		}

		//New anyoption design.
		function showCurrHint_nd(link, divHint, id, pic_path) {
			var i_pic = document.getElementById(id);
			i_pic.src = pic_path;
			showCurrHint(link, divHint)
		}

		function hideCurrHint_nd(link, id, pic_path) {
			var i_pic = document.getElementById(id);
			i_pic.src = pic_path;
			hideCurrHint(link);
		}

// the class attribute name. there us diff btw IE (className) and FF (class)
var classAtr = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'className' : 'class';
// in IE 8.0 it is like in FireFox
classAtr = navigator.appVersion.toLowerCase().indexOf("msie 8.0") != -1 ? 'class' : classAtr;
		function setCss(marketId) {
			//alert(marketStates);
			var divTreeM = document.getElementById('treeM');
			var treeOptions = divTreeM.getElementsByTagName("A");

			$('.close_area').css("display","block");
			var state;
			var optionMarketId;
			for (i=0;i<treeOptions.length;i++) {
				state=null;
				optionMarketId = treeOptions[i].id.split('m')[1];
				for (j=0;j<marketStates.length;j++) {
			//		alert(marketStates[j][0] + " = " + optionMarketId);
					if (marketStates[j][0]==optionMarketId) {
	   					state = marketStates[j][1];
	   					break;
	   				}
	   			}
	   			if ((null == state) || (state == 0)) {
	   				//alert("0   " + state);
	   				treeOptions[i].parentNode.parentNode.parentNode.style.display = "none";
//	   				treeOptions[i].setAttribute(classAtr, "menu_item_notActive");
	   				clearSelectedStyle(treeOptions[i].parentNode.parentNode.parentNode, false);
  					if (marketId == optionMarketId) {
	  					setSelectedStyle(treeOptions[i].parentNode.parentNode.parentNode);
  					}
	   			} else {
	   				//alert("1   " + state);
	   				treeOptions[i].parentNode.parentNode.parentNode.style.display = "block";

	   				var closedArea = document.getElementById(treeOptions[i].getElementsByTagName('DIV')[0].innerHTML);
	   				if (null != closedArea) {
	   					closedArea.style.display = "none";
	   				}
	   				treeOptions[i].setAttribute(classAtr, "menu_item_active");
	   				clearSelectedStyle(treeOptions[i].parentNode.parentNode.parentNode, true);
  					if (marketId == optionMarketId) {
	  					setSelectedStyle(treeOptions[i].parentNode.parentNode.parentNode);
  					}
	   			}
			}
		}

function setSelectedStyle(element) {
	element.style.backgroundColor = '#0284d6';
	if (navigator.userAgent.toLowerCase().indexOf("msie") != -1) {
		element.children[0].getElementsByTagName("label")[0].style.color = "white";
	} else {
		element.childNodes[1].getElementsByTagName("label")[0].style.color = "white";
	}
}

function clearSelectedStyle(element, isActive) {
	element.style.backgroundColor = '#ffffff';
	var color = "#a1a1a1";
	if (isActive) {
		color = "#093755";
	}
	if (navigator.userAgent.toLowerCase().indexOf("msie") != -1) {
		element.children[0].getElementsByTagName("label")[0].style.color = color;
	} else {
		element.childNodes[1].getElementsByTagName("label")[0].style.color = color;
	}
}


// Assets list for Past Expiries


		var HINTS_ASSETS_LIST = {
			'smart'      : false, // don't go off screen, don't overlap the object in the document
			'margin'     : 0, // minimum allowed distance between the hint and the window edge (negative values accepted)
			'gap'        : 0, // minimum allowed distance between the hint and the origin (negative values accepted)
			'align'      : 'brbr', // align of the hint and the origin (by first letters origin's top|middle|bottom left|center|right to hint's top|middle|bottom left|center|right)
			'css'        : 'tree_bkgnd', // a style class name for all hints, applied to DIV element (see style section in the header of the document)
			'show_delay' : 0, // a delay between initiating event (mouseover for example) and hint appearing
			'hide_delay' : 200, // a delay between closing event (mouseout for example) and hint disappearing
			'follow'     : false, // hint follows the mouse as it moves
			'z-index'    : 100, // a z-index for all hint layers
			'IEfix'      : false, // fix IE problem with windowed controls visible through hints (activate if select boxes are visible through the hints)
			'IEtrans'    : ['blendTrans(DURATION=.3)', null], // [show transition, hide transition] - nice transition effects, only work in IE5+
			'opacity'    : 99 // opacity of the hint in %%
		};
		var HINTS_ASSETS_LIST_ARABIC = {
			'smart'      : false, // don't go off screen, don't overlap the object in the document
			'margin'     : 0, // minimum allowed distance between the hint and the window edge (negative values accepted)
			'gap'        : 6, // minimum allowed distance between the hint and the origin (negative values accepted)
			'align'      : 'bltl', // align of the hint and the origin (by first letters origin's top|middle|bottom left|center|right to hint's top|middle|bottom left|center|right)
			'css'        : 'tree_bkgnd', // a style class name for all hints, applied to DIV element (see style section in the header of the document)
			'show_delay' : 0, // a delay between initiating event (mouseover for example) and hint appearing
			'hide_delay' : 200, // a delay between closing event (mouseout for example) and hint disappearing
			'follow'     : false, // hint follows the mouse as it moves
			'z-index'    : 100, // a z-index for all hint layers
			'IEfix'      : false, // fix IE problem with windowed controls visible through hints (activate if select boxes are visible through the hints)
			'IEtrans'    : ['blendTrans(DURATION=.3)', null], // [show transition, hide transition] - nice transition effects, only work in IE5+
			'opacity'    : 99 // opacity of the hint in %%
		};
		// text/HTML of the hints
		var assetsListDiv = ['<div style=" width:159px;height:227px;overflow-y:scroll;overflow-x:hidden;" id="assetsList"></div>'];
		var assetsListDivArabic = ['<div style=" width:165px;height:227px;overflow-y:scroll;overflow-x:hidden;" id="assetsListArabic"></div>'];

		var assetListOpen = new THints (assetsListDiv, HINTS_ASSETS_LIST);
		var assetListOpenArabic = new THints (assetsListDivArabic, HINTS_ASSETS_LIST_ARABIC);


		function showAssetList(link,skinId) {
			if(skinId == '4'){
				var parentTr = document.getElementById("arrowDown").parentNode; //curr TD
				var showTd = document.getElementById("arrowDown");
			} else {
				var parentTr = document.getElementById("assetListOpenBy").parentNode; //curr TD
				var showTd = document.getElementById("assetListOpenBy");
			}

			if(skinId!='4' && skinId!='11'){
				document.getElementById('assetsList').innerHTML = document.getElementById('assetL').innerHTML;
				assetListOpen.show(0, showTd);
			}else{
				document.getElementById('assetsListArabic').innerHTML = document.getElementById('assetL').innerHTML;
				assetListOpenArabic.show(0, showTd);
			}


		}

		function hideAssetList(link) {
			var showTd = document.getElementById('lastLevelsForm:selectedAssetNameDisplay');
			assetListOpen.hide(0,showTd);
		}


// Assets list for My investments


		var HINTS_ASSETS_LIST_MY_INV = {
			'smart'      : false, // don't go off screen, don't overlap the object in the document
			'margin'     : 0, // minimum allowed distance between the hint and the window edge (negative values accepted)
			'gap'        : 2, // minimum allowed distance between the hint and the origin (negative values accepted)
			'align'      : 'brtr', // align of the hint and the origin (by first letters origin's top|middle|bottom left|center|right to hint's top|middle|bottom left|center|right)
			'css'        : 'tree_bkgnd', // a style class name for all hints, applied to DIV element (see style section in the header of the document)
			'show_delay' : 0, // a delay between initiating event (mouseover for example) and hint appearing
			'hide_delay' : 200, // a delay between closing event (mouseout for example) and hint disappearing
			'follow'     : false, // hint follows the mouse as it moves
			'z-index'    : 100, // a z-index for all hint layers
			'IEfix'      : false, // fix IE problem with windowed controls visible through hints (activate if select boxes are visible through the hints)
			'IEtrans'    : ['blendTrans(DURATION=.3)', null], // [show transition, hide transition] - nice transition effects, only work in IE5+
			'opacity'    : 99 // opacity of the hint in %%
		};
		var HINTS_ASSETS_LIST_MY_INV_ARABIC = {
			'smart'      : false, // don't go off screen, don't overlap the object in the document
			'margin'     : 0, // minimum allowed distance between the hint and the window edge (negative values accepted)
			'gap'        : 5, // minimum allowed distance between the hint and the origin (negative values accepted)
			'align'      : 'brtl', // align of the hint and the origin (by first letters origin's top|middle|bottom left|center|right to hint's top|middle|bottom left|center|right)
			'css'        : 'tree_bkgnd', // a style class name for all hints, applied to DIV element (see style section in the header of the document)
			'show_delay' : 0, // a delay between initiating event (mouseover for example) and hint appearing
			'hide_delay' : 200, // a delay between closing event (mouseout for example) and hint disappearing
			'follow'     : false, // hint follows the mouse as it moves
			'z-index'    : 100, // a z-index for all hint layers
			'IEfix'      : false, // fix IE problem with windowed controls visible through hints (activate if select boxes are visible through the hints)
			'IEtrans'    : ['blendTrans(DURATION=.3)', null], // [show transition, hide transition] - nice transition effects, only work in IE5+
			'opacity'    : 99 // opacity of the hint in %%
		};
		// text/HTML of the hints
		var assetsListMyInvDiv = [
			'<div style=" width:159px;height:227px;overflow-y:scroll;overflow-x:hidden;" id="assetsListMyInv"></div>'
		];
		var assetsListMyInvDivArabic = [
			'<div style=" width:165px;height:227px;overflow-y:scroll;overflow-x:hidden;" id="assetsListMyInvArabic" dir="rtl"></div>'
		];

		var assetListOpenMyInv = new THints (assetsListMyInvDiv, HINTS_ASSETS_LIST_MY_INV);
		var assetListOpenMyInvArabic = new THints (assetsListMyInvDivArabic, HINTS_ASSETS_LIST_MY_INV_ARABIC);



		function showAssetListMyInv(link,skinId) {

			if(skinId == '4'){
				var parentTr = document.getElementById("arrowDown").parentNode; //curr TD
				var showTd = document.getElementById('arrowDown');
			} else {
				var parentTr = document.getElementById("assetListOpenBy").parentNode; //curr TD
				var showTd = document.getElementById('assetListOpenBy');
			}

			if (skinId!='4' && skinId!='11'){
				document.getElementById('assetsListMyInv').innerHTML = document.getElementById('assetMyInv').innerHTML;
				assetListOpenMyInv.show(0, showTd);
			}else{
				document.getElementById('assetsListMyInvArabic').innerHTML = document.getElementById('assetMyInv').innerHTML;
				assetListOpenMyInvArabic.show(0, showTd);
			}
		}

		function hideAssetListMyInv(link) {
			var parentTr = link.parentNode.parentNode;
			var showTd = getChildOfTypeHint(parentTr,"TD",2);
			assetListOpenMyInv.hide(0,showTd);
		}



function getChildOfTypeHint(node, childType, order) {
	var pos = 0;
	for (var i = 0; i < node.childNodes.length; i++) {
		if (node.childNodes[i].nodeName == childType) {
			pos++;
			if (pos == order) {
				return node.childNodes[i];
			}
		}
	}
}

// change color functions for skins menu selection

var prevColor;  // last color


// SECURED hint for register page
var HINTS_SECURED = {
			'smart'      : false, // don't go off screen, don't overlap the object in the document
			'margin'     : 0, // minimum allowed distance between the hint and the window edge (negative values accepted)
			'gap'        : 115, // minimum allowed distance between the hint and the origin (negative values accepted)
			'align'      : 'bltr', // align of the hint and the origin (by first letters origin's top|middle|bottom left|center|right to hint's top|middle|bottom left|center|right)
			'css'        : 'hintsClass', // a style class name for all hints, applied to DIV element (see style section in the header of the document)
			'show_delay' : 0, // a delay between initiating event (mouseover for example) and hint appearing
			'hide_delay' : 200, // a delay between closing event (mouseout for example) and hint disappearing
			'follow'     : false, // hint follows the mouse as it moves
			'z-index'    : 100, // a z-index for all hint layers
			'IEfix'      : false, // fix IE problem with windowed controls visible through hints (activate if select boxes are visible through the hints)
			'IEtrans'    : ['blendTrans(DURATION=.3)', null], // [show transition, hide transition] - nice transition effects, only work in IE5+
			'opacity'    : 99 // opacity of the hint in %%
		};
		// text/HTML of the hints
		var securedHintDiv = [
			'<div style=" width:280px;height:65px;" id="securedHintDiv"></div>'
		];

		var secured = new THints (securedHintDiv, HINTS_SECURED);


		function showSecuredHint(link) {
			document.getElementById('securedHintDiv').innerHTML = document.getElementById('secureHint').innerHTML;
			var parentTr = document.getElementById("showHintTD").parentNode; //curr TD
			var showTd = document.getElementById('showHintTD');
			secured.show(0, showTd);

		}

		function hideSecuredHint(link) {
			var parentTr = link.parentNode.parentNode;
			var showTd = getChildOfTypeHint(parentTr,"TD",2);
			secured.hide(0,showTd);
		}

		function hideElm(elm) {
			elm.style.display = "none";
		}

		function hideMenus() {
			try {
				skinsMenuOpen.hide();
			} catch (e) {

			}
			try {
				scheduleMenu.hide();
			} catch (e) {

			}
			try {
				treeMenuOpen.hide();
			} catch (e) {

			}
			try {
				selectorMenu.hide();
			} catch (e) {

			}
		}

		/* Landing page with registration */
		var alignLand = 'mlmr';
		var HINTS_LAND_REG = {
			'smart'      : true, // don't go off screen, don't overlap the object in the document
			'margin'     : 0, // minimum allowed distance between the hint and the window edge (negative values accepted)
			'gap'        : 160, // minimum allowed distance between the hint and the origin (negative values accepted)
			'align'      : alignLand, // align of the hint and the origin (by first letters origin's top|middle|bottom left|center|right to hint's top|middle|bottom left|center|right)
			'css'        : 'hintsClass', // a style class name for all hints, applied to DIV element (see style section in the header of the document)
			'show_delay' : 0, // a delay between initiating event (mouseover for example) and hint appearing
			'hide_delay' : 100, // a delay between closing event (mouseout for example) and hint disappearing
			'follow'     : false, // hint follows the mouse as it moves
			'z-index'    : 0, // a z-index for all hint layers
			'IEfix'      : false, // fix IE problem with windowed controls visible through hints (activate if select boxes are visible through the hints)
			'IEtrans'    : ['blendTrans(DURATION=.3)', null], // [show transition, hide transition] - nice transition effects, only work in IE5+
			'opacity'    : 99 // opacity of the hint in %%
		};
		// text/HTML of the hints
		var landHintDiv = [
			'<div id="regLandHint"></div>'
		];

		var landHintOpen = new THints (landHintDiv, HINTS_LAND_REG);
		function showLandHint(link, divHint) {
			document.getElementById('regLandHint').innerHTML = document.getElementById(divHint).innerHTML;
			landHintOpen.show(0, link);
		}

		function hideLandHint(link) {
			landHintOpen.hide();
		}

		/* Landing page with registration */
		alignLand = 'tctc';
		var HINTS_NEW_LAND_REG = {
			'smart'      : true, // don't go off screen, don't overlap the object in the document
			'margin'     : 0, // minimum allowed distance between the hint and the window edge (negative values accepted)
			'gap'        : 30, // minimum allowed distance between the hint and the origin (negative values accepted)
			'align'      : alignLand, // align of the hint and the origin (by first letters origin's top|middle|bottom left|center|right to hint's top|middle|bottom left|center|right)
			'css'        : 'hintsClass', // a style class name for all hints, applied to DIV element (see style section in the header of the document)
			'show_delay' : 0, // a delay between initiating event (mouseover for example) and hint appearing
			'hide_delay' : 100, // a delay between closing event (mouseout for example) and hint disappearing
			'follow'     : false, // hint follows the mouse as it moves
			'z-index'    : 100, // a z-index for all hint layers
			'IEfix'      : false, // fix IE problem with windowed controls visible through hints (activate if select boxes are visible through the hints)
			'IEtrans'    : ['blendTrans(DURATION=.3)', null], // [show transition, hide transition] - nice transition effects, only work in IE5+
			'opacity'    : 99 // opacity of the hint in %%
		};
		// text/HTML of the hints
		var newLandHintDiv = [
			'<div id="newRegLandHint"></div>'
		];

		var landNewHintOpen = new THints (newLandHintDiv, HINTS_NEW_LAND_REG);
		function showNewLandHint(link, divHint) {
			document.getElementById('newRegLandHint').innerHTML = document.getElementById(divHint).innerHTML;
			showPos = document.getElementById('landingRegisterForm:savelink');
			landNewHintOpen.show(0, showPos);
		}

		function hideNewLandHint(link) {
			landNewHintOpen.hide();
		}

