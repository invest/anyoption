	/******************************************** Subscriptions Live********************************************/

	//var groupSkin = "live_investments" + userLanguage
	var groupSkin = "live_investmentsall";
	var group_trades = [groupSkin];
	var schema_trades = ["key", "command", "LI_CITY", "LI_COUNTRY", "LI_ASSET", "LI_PROD_TYPE", "LI_INV_TYPE", "LI_LEVEL", "LI_AMOUNT", "LI_TIME", "LI_INV_KEY", "LI_OPP_ID", "LI_CONVERTED_AMOUNT"];
	var aoTable_trades = new DynaMetapushTable(group_trades, schema_trades, "COMMAND");
	aoTable_trades.setDataAdapter("JMS_ADAPTER");
	aoTable_trades.setSnapshotRequired(true);
	aoTable_trades.setRequestedMaxFrequency(1.0);
	aoTable_trades.setPushedHtmlEnabled(true);
	aoTable_trades.setMetapushFields(2, 1);
	aoTable_trades.setClearOnDisconnected(false);
	aoTable_trades.setMaxDynaRows(29);
	aoTable_trades.setMetapushSort(10,true,false,false);
	aoTable_trades.onItemUpdate = updateItem_live_investments;
	aoTable_trades.onChangingValues = formatValue_live_investments;
	lsPage.addTable(aoTable_trades, "livetrades");
	

	var group_trends = ["trends"]; 
	var schema_trends = ["key", "merge", "LTT_MARKET1", "LTT_OPP_ID1", "LTT_TREND1", "LTT_MARKET2", "LTT_OPP_ID2", "LTT_TREND2", "LTT_MARKET3", "LTT_OPP_ID3", "LTT_TREND3"];
	var aoTable_trends = new DynaMetapushTable(group_trends, schema_trends, "MERGE");
	aoTable_trends.setDataAdapter("JMS_ADAPTER");
	aoTable_trends.setSnapshotRequired(true);
	aoTable_trends.setRequestedMaxFrequency(1.0);
	aoTable_trends.onItemUpdate = updateItem_live_trade;
	aoTable_trends.onChangingValues = formatValues_live_trade;
	aoTable_trends.setMaxDynaRows(3);
	aoTable_trends.setMetapushFields(2, 1);
	lsPage.addTable(aoTable_trends, "trends");	
