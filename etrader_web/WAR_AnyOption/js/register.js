/*********** register.js ************/

	var form = 'registerForm';

	/* set form name in case it's different from registerForm */
	function setForm(myForm){
		form = myForm;
	}

	/* Change the phone code by the value of the country */
	function changePhoneCodeByCountry(state) {
	    var countryId = document.getElementById(form + ":countriesList").value;
	    if(countryId == 220){
    		document.getElementById(state).style.display='inline';
		} else {
	    	document.getElementById(state).style.display="none";
	    }
	    var url = "ajax.jsf?countryId=" + countryId;
	    var ajax = new AJAXInteraction(url, changePhoneCodeByCountryCallback);
	    ajax.doGet();
	}

	/* callBack function for changePhoneCodeByCountry() */
	function changePhoneCodeByCountryCallback(responseXML) {
	   var codeValue = responseXML.getElementsByTagName("phoneCode")[0].firstChild.nodeValue;
   	   var phoneCodeElm = document.getElementById(form + ":mobilePhoneCode");
   	   var phoneCodeElmMobile = document.getElementById(form + ":landPhoneCode");
   	   if (form == 'landingRegisterForm'){
		   phoneCodeElm.value = codeValue;
      	   phoneCodeElmMobile.value = codeValue;
   	   } else {
		   phoneCodeElm.value =  codeValue;
      	   phoneCodeElmMobile.value = codeValue;
   	   }
	}

	//	phone code keyDown
	//  allow tab key
	function phoneKeys(e) {
		var keynum;
		if(window.event) { // IE
		  keynum = e.keyCode;
  		}
  		else if(e.which) {  // Netscape/Firefox/Opera
		  keynum = e.which;
	    }
		if (keynum != 9) {  // tab key
			return false;
	  	}
	}

	/* Show anyoption agreemen popup*/
	function showAgreemen(link){
	    window.open(link.toString(),'','scrollbars=yes,height=600,width=970,resizable=yes,toolbar=yes,screenX=0,left=1024,screenY=0,top=600');
	}

	/* insert into register attempts AJAX call */
	function insertUpdateRegisterAttempts(emailMsg, firstMsg, lastMsg) {
		var email = document.getElementById(form + ":email");
		var firstName = document.getElementById(form + ":firstName");
		var lastName = document.getElementById(form + ":lastName");
		var countryId = document.getElementById(form + ":countriesList");
		var landLinePhone = document.getElementById(form + ":landLinePhone");
		var mobilePhone = document.getElementById(form + ":mobilePhone");
		var isContactByEmail = document.getElementById(form + ":contactByEmail");
		var isContactBySMS = document.getElementById(form + ":contactBySms");
		var realEmail = email.value;
		var realFirstName = firstName.value;
		var realLastName = lastName.value;
		if (form == 'landingRegisterForm') {
			if (email.value == emailMsg ){
				realEmail = '';
			}
			if (firstName.value == firstMsg){
				realFirstName = '';
			}
			if (lastName.value == lastMsg){
				realLastName = '';
			}
		}
		if (realEmail.length > 0 || landLinePhone.value.length > 6 || mobilePhone.value.length > 6 ){
	    	var dataa = "emailRegisterAttempts=" + encodeURIComponent(realEmail) + "&firstNameRegisterAttempts="
	    				+ encodeURIComponent(realFirstName) + "&lastNameRegisterAttempts=" + encodeURIComponent(realLastName)
	    				+ "&countryRegisterAttempts=" + encodeURIComponent(countryId.value) + "&mobilePhoneRegisterAttempts=" + encodeURIComponent(mobilePhone.value)
		    			+ "&landLinePhoneRegisterAttempts=" + encodeURIComponent(landLinePhone.value)
		    			+ "&contactEmailRegisterAttempts=" + isContactByEmail.checked
	     				+ "&contactSMSRegisterAttempts=" + isContactBySMS.checked;

			$.ajax({
				   type: "POST",
				   url: "ajax.jsf",
				   data: dataa
			});

	/*    	var ajax = new AJAXInteraction(url, null);
	    	ajax.doGet();
	*/
	   }
	}
