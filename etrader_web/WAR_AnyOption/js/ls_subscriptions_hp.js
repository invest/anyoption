	/********************************************Assets table - anyoption home page********************************************/
	
	if (skinId != 13 && skinId != 14) {
		//Indices
		subscribeTemplate('2', '4',"oppsIndices");
	
		//Commodities
		subscribeTemplate('5', "4","oppsCommodities");
	
		//Currencies (Forex)
		subscribeTemplate('4', '4',"oppsCurrencies");
	}
	//Stocks
	subscribeTemplate('6', '8',"oppsStocks");

	function subscribeTemplate (groupNum, maxRow, tableId) {
		var group1 = ["group_" + skinId.toString() + "_" + groupNum];
		var schema1 = ["key", "command", "ET_NAME", "AO_LEVEL", "ET_ODDS", "ET_EST_CLOSE", "ET_OPP_ID", "ET_ODDS_WIN", "ET_ODDS_LOSE", "ET_STATE", "AO_CLR", "ET_GROUP_PRIORITY", "ET_GROUP_CLOSE", "ET_SCHEDULED", "ET_RND_FLOOR", "ET_RND_CEILING", "ET_GROUP", "ET_SUSPENDED_MESSAGE", "AO_STATES", "AO_TS"];
		var newTable = new DynaMetapushTable(group1, schema1, "COMMAND");
		newTable.setDataAdapter("JMS_ADAPTER");
		newTable.setSnapshotRequired(true);
		newTable.setRequestedMaxFrequency(0.07);
		newTable.setPushedHtmlEnabled(true);
		newTable.setClearOnDisconnected(false);
		newTable.setMaxDynaRows(maxRow);
		newTable.setMetapushFields(2, 1);
		newTable.setMetapushSort(30,false,true);
		newTable.onChangingValues = formatValues_hp;
		newTable.onItemUpdate = updateItem_hp;
		lsPage.addTable(newTable, tableId);
	}
