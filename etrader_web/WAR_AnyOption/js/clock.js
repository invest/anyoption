/*********** clock.js ************/
	var time = new Date();
	var offset = time.getTimezoneOffset()*60*1000*(-1);
	var diffTimeMs = null;
	var dateFormat = new Date();
	var day = dateFormat.getDate();
	var month = dateFormat.getMonth();
	var year = dateFormat.getFullYear();
	var MONTH_NAMES=new Array('01','02','03','04','05','06','07','08','09','10','11','12');
	var monthName = MONTH_NAMES[month];
	function tS(){
		var x = new Date();
		
		if (null == diffTimeMs) {
			diffTimeMs = serverTime - x.getTime();
		}
		var diffTimeZoneOffsetMs = offset + (x.getTimezoneOffset() * 60 * 1000);

		x.setTime(x.getTime() + diffTimeMs + diffTimeZoneOffsetMs + dstOffset);
		return x;
	}
	function lZ(x){ return (x>9)?x:'0'+x; }
	function dT(){ document.getElementById('anyoptionClock').innerHTML=eval(oT); setTimeout('dT()',1000); }
	var oT="lZ(tS().getHours())+':'+lZ(tS().getMinutes())+':'+lZ(tS().getSeconds()) + ', ' + day + '.' + monthName + '.' + year ";
	//if(!document.all){ window.onload=dT; }else{ dT(); }
	dT();
	
	var timeStr = getTimeString();
	var clockId = document.getElementById("anyoptionClock");
	clockId.innerHTML = timeStr;

	function getTimeString() {
		return lZ(tS().getHours())+':'+lZ(tS().getMinutes())+':'+lZ(tS().getSeconds()) + ', ' + day + '.' + monthName +'.' + year ;
	}