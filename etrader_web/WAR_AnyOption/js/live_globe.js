var LI_CITY 							= 3;  //City
var LI_COUNTRY 							= 4;  // Country
var LI_ASSET 							= 5;  // Asset
var LI_PROD_TYPE						= 6;  // binary/+/100
var LI_INV_TYPE 						= 7;  // call/put/buy/sell
var LI_LEVEL 							= 8;  // 1.12345
var LI_AMOUNT 							= 9;  // formatted for display investment amount in user currency
var LI_TIME 							= 10; // number of millis since epoch
var LI_LAT 								= 11;
var LI_LONG 							= 12;
var LI_MARKET_NAME						= 13;
var LI_MARKET_DECIMAL					= 14;
var LI_COUNTRY_NAME						= 15;

var currentInvestment;
//@
function formatValues_globe(item, itemUpdate) {
	if (item == null) {
		return;
	}
	if (itemUpdate == null) {
		return;
	}

    //investment detail
    investment = new Object();
	investment.city 			= itemUpdate.getServerValue(LI_CITY);
	investment.country 			= itemUpdate.getServerValue(LI_COUNTRY_NAME);
	investment.level 			= itemUpdate.getServerValue(LI_LEVEL);
	investment.lat 				= itemUpdate.getServerValue(LI_LAT);
	investment.lng 				= itemUpdate.getServerValue(LI_LONG);
	investment.asset_name 		= itemUpdate.getServerValue(LI_MARKET_NAME);
	investment.amount			= itemUpdate.getServerValue(LI_AMOUNT);
	investment.imageFlagGlobe	= "<div><img src = '" + getCountryFlagImage(itemUpdate.getServerValue(LI_COUNTRY)) + "' width='15px' height='16px' /></div>";
	investment.imageFlag		= "<img src = '" + getCountryFlagImage(itemUpdate.getServerValue(LI_COUNTRY)) + "' width='15px' height='16px' />";
	investment.prodType			= "<span style='vertical-align: top;'><img src = '" + getProdTypeImage(itemUpdate.getServerValue(LI_PROD_TYPE)) +  "' width='16px' height='16px' style='margin-right:5px;margin-left:5px;' /></span>";
	investment.invType			= "<img src = '" + getInvTypeImage(itemUpdate.getServerValue(LI_INV_TYPE)) + "' width='10px' height='10px' style='margin-right:5px;margin-left:5px;'  />";
	investment.triangle			= "<img src = '" + imagePath + "/globe_corner.png" + "' width='18px' height='9px' />";

	var c = {"name":"Placeholder","coords":[investment.lat, investment.lng]};

	if (investment != null && investment.level != null && investment.level != 'undefined') {
		var num = itemUpdate.getServerValue(LI_MARKET_DECIMAL);
		//investment.level = new Number(investment.level).toFixed(num);		
		var str = investment.level;
		var strReplace = str.replace(',','');
		var newNum = new Number(strReplace).toFixed(num);		
		investment.level = addThousandsCommaSeparators(newNum.toString());						
	}
	
	if (investment != null && investment.amount != null && investment.amount != 'undefined') {
		var amount = itemUpdate.getServerValue(LI_AMOUNT);
		if(amount.indexOf("руб.") > -1) {
			var temp = amount.replace("руб.", "").trim();
			amount = "руб." + temp;
		}
		investment.amount = amount;
	}
	
	if (supportWebGL) {
		var goToLocation = false;
		if (currentInvestment == null || currentInvestment == 'undefined' || (investment.lat != currentInvestment.lat || investment.lng != currentInvestment.lng)) {
			goToLocation = true;
		}
		currentInvestment = investment;
		globe.insertNewInvestment(c, investment, goToLocation);
	} else {
		showInvestmentNotWebGL(investment);
	}
}

//@@
function updateItem_globe(item, updateInfo) {
	if (item == null) {
		return;
	}
	if (updateInfo == null) {
		return;
	}
	//add field to change market display name
	if (updateInfo.isValueChanged(LI_ASSET)) {
		updateInfo.addField(LI_MARKET_NAME, getMarketName(updateInfo.getNewValue(LI_ASSET)));
		updateInfo.addField(LI_MARKET_DECIMAL, getMarketsDecimalPoint(updateInfo.getNewValue(LI_ASSET)));
	}

	//add field to change country display name
	if (updateInfo.isValueChanged(LI_COUNTRY)) {
		updateInfo.addField(LI_COUNTRY_NAME, getCountryName(updateInfo.getNewValue(LI_COUNTRY)));
	}
}


function getMarketName(marketId) {
	for (var i = 0; i < marketsDisplayName.length; i++) {
		if (marketsDisplayName[i][0] == marketId) {
			return marketsDisplayName[i][1];
		}
	}
	return "";
}

function getCountryName(countryId) {
	for (var i = 0; i < countriesDisplayName.length; i++) {
		if (countriesDisplayName[i][0] == countryId) {
			return countriesDisplayName[i][1];
		}
	}
	return "";
}

function getMarketsDecimalPoint(marketId) {
	for (var i = 0; i < marketsDecimalPoint.length; i++) {
		if (marketsDecimalPoint[i][0] == marketId) {
			return marketsDecimalPoint[i][1];
		}
	}
	return "5";
}

function getCountryFlagImage(countryId) {
	return imagePath + "/flags/" + countryId + ".png";
}

function getProdTypeImage(typeId) {
	var TypeUrl = imagePath + "/icon_BO.png";
	if(typeId == 3) {
		TypeUrl = imagePath + "/optionPlus_icon_sm.png";
	}
	if(typeId == 4 || typeId == 5) {
		TypeUrl = imagePath + "/0100_icon_sm.png";
	}
	return TypeUrl;
}

function getInvTypeImage(typeId) {
	var TypeUrl = "";
	if(typeId == 1 || typeId == 4) {
		 TypeUrl = imagePath + "/arrowTable_Call.png";
	}
	if(typeId == 2 || typeId == 5) {
		TypeUrl = imagePath + "/arrowTable_Put.png";
	}
	return TypeUrl;
}

function addThousandsCommaSeparators(str) {
	var strRightToDot = str.substring(0, str.indexOf('.'));
	var strLeftToDot = str.substring(str.indexOf('.'));
	
	var newStrRightToDot = strRightToDot.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	var newNum = newStrRightToDot + strLeftToDot;
	return newNum;
}