/****************** livePerson.js ******************/

	function displayHideCallMe(isShowSmall) {
		var call_me_small = document.getElementById("callme_sm");
		var call_me_big = document.getElementById("callme_big");
		if (isShowSmall) {
			call_me_small.style.display = "block";
			call_me_big.style.display = "none";
		} else {
			call_me_small.style.display = "none";
			call_me_big.style.display = "block";
		}
	}
