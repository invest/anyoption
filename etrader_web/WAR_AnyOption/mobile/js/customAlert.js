
// constants to define the title of the alert and button text.
//var ALERT_TITLE = "Oops!";
var ALERT_BUTTON_TEXT = "OK";


/*
	Create custom alert message.
	title - title of the message
	txt - the message
	btnTxt - the button text, default is ALERT_BUTTON_TEXT
*/
function customAlert(title, txt, btnTxt) {
	// shortcut reference to the document object
	d = document;

	// if the modalContainer object already exists in the DOM, bail out.
	if(d.getElementById("modalContainer")) return;

	containerDiv = d.getElementsByTagName("body")[0].appendChild(d.createElement("div"));
	containerDiv.id = "containerDiv";
	containerDiv.setAttribute("class", "container_alert_popup");
	containerDiv.style.width = window.innerWidth + "px";
	containerDiv.style.left = "0px";
	containerDiv.style.top = "0px";
	containerDiv.style.height = getMaxHeightByOrientation();

	// create the modalContainer div as a child of the BODY element
	mObj = d.getElementsByTagName("body")[0].appendChild(d.createElement("div"));
	mObj.id = "modalContainer";
	mObj.setAttribute("class", "alert_popup");

	var sWidth = window.innerWidth;
	var sHight = window.innerHeight;
	//alert(sWidth);
	//alert(String(Math.round((sWidth-290)/2)));
	mObj.style.left = String(Math.round((sWidth-290)/2))+"px";
	mObj.style.top = String(Math.round((sHight-188)/2) + getScrollXY()[1]) +"px";

    // Table creation (div wrapper for positioning the message in the middle)
    alertT = mObj.appendChild(d.createElement("table"));
    alertT.setAttribute("height", "130px");
    alertT.setAttribute("width", "100%");
    alertTB = alertT.appendChild(d.createElement("tbody"));
    alertTR = alertTB.appendChild(d.createElement("tr"));
    alertTD = alertTR.appendChild(d.createElement("td"));
    alertTR.setAttribute("valign", "middle");
    alertTD.setAttribute("valign", "middle");
    alertTD.setAttribute("align", "center");

	// create the DIV that will be the alert
	/*alertObj = alertTD.appendChild(d.createElement("div"));
	alertObj.id = "alertBox";*/
	//alertObj.setAttribute("class", "alert_popup_msg");

	// create an H1 element as the title bar
	/*h1 = alertTD.appendChild(d.createElement("h1"));
	h1.appendChild(d.createTextNode(title)); */

	// create a paragraph element to contain the txt argument
	msg = alertTD.appendChild(d.createElement("p"));
	msg.setAttribute("class", "alert_popup_content");
	msg.innerHTML = txt;

	// create an anchor element to use as the confirmation button.
	btnDiv = mObj.appendChild(d.createElement("div"));
	btnDiv.id = "closeDiv"
	btnDiv.style.width = "100%";
	btnDiv.style.height = "30px";
	btnDiv.setAttribute("align", "center");

	btn = btnDiv.appendChild(d.createElement("input"));
	btn.id = "closeBtn";
	btn.setAttribute("class","nav_button_blue alert_popup_button_addition");
	btn.style.width = "100%";
	btn.style.height = "30px";
	btn.setAttribute("type", "button");
	var buttonText = ALERT_BUTTON_TEXT;
	if (btnTxt != null) {
		buttonText = btnTxt;
	}
	btn.setAttribute("value", buttonText);
	btn.setAttribute("align", "center");
	btn.onclick = function() { removeCustomAlert();return false; }

	// add event handlers for disable user touch
	disableTouch();
}

// removes the custom alert from the DOM
function removeCustomAlert() {
	document.getElementsByTagName("body")[0].removeChild(document.getElementById("modalContainer"));
	document.getElementsByTagName("body")[0].removeChild(document.getElementById("containerDiv"));
	enableTouch();
	if (typeof isNotLogInError != "undefined" && isNotLogInError) {
		isNotLogInError = false;
		window.location.href = context_path + '/mobile/register.jsf';
	}
}
