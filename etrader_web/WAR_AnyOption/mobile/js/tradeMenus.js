//b4 showing the market list mark which the close market with -
function markClosedMarket(marketSelectL, marketId) {
	var state;
	for (var i = 0; i < marketSelectL.options.length; i++) {
		state = null;
		optionMarketId = marketSelectL.options[i].value;
		for (var j = 0; j < marketStates.length; j++) {
			if (marketStates[j][0] == optionMarketId) {
  					state = marketStates[j][1];
  					break;
  				}
  			}
  			var closePosition = marketSelectL.options[i].text.indexOf(" " + marketStateClose);
  			if (closePosition > -1) {
  				marketSelectL.options[i].text = marketSelectL.options[i].text.substring(0, closePosition);
  			}
  			if ((null == state) || (state == 0)) {
			marketSelectL.options[i].text = marketSelectL.options[i].text + " " + marketStateClose;
  			}

	    if (marketSelectL.options[i].value == marketId) {
	    	marketSelectL.options[i].selected = true;
	    }
  	}
}

//change market from mobile trade page
//@list the list of the scheduleds
//@marketId the market id of the market that in the box
//@oppId the oppId of the current box
function changeMarket(list, marketId, oppId) {
	TigraBoxMarketId = marketId;
	if (isMobileParm) {
		lastSelectedMarket = document.getElementById(oppId + "_" + 25); //the last index that was selected
		lastSelectedMarketlist = list;
	}
	nodeClick(list.value);
}


//remove scheduled option that not avilable for this optio
//@oppId the box opp id
function removeScheduledOptions(list, oppId) {
	var filterScheduled = document.getElementById(oppId + "_" + SUBSCR_FLD_FILTER_SCHEDULED).innerHTML;
	var Scheduled = document.getElementById(oppId + "_" + SUBSCR_FLD_ET_SCHEDULED).innerHTML;
	list.options[Scheduled - 1].selected = true;
	if (list.length > filterScheduled) {
		switch(filterScheduled) {
			case "1":
				list.remove(3);
				list.remove(2);
				list.remove(1);
			  	break;
			case "2":
				list.remove(3);
				list.remove(2);
			  	break;
			case "3":
				list.remove(3);
			  	break;
			case "4":
			  	break;
			default:
		}
	}
}

//change market scheduled from mobile trade page
//@list the list of the scheduleds
//@marketId the market id of the market that in the box
function changeScheduled(list, marketId) {
	TigraBoxMarketId = marketId;
	scheduleSelect(list.value);

}
