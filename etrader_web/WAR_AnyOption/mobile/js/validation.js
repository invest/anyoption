function validate() {
	var userName = document.getElementById("loginForm:userName").value;
	var password = document.getElementById("loginForm:password").value;
	if (userName != "" && password != "") {
		var errormsg = null;
		errormsg = validateUserName(userName);
		if (errormsg == null) {
			errormsg = validatePassword(password);
		}
		if (errormsg == null) {
			 return myfaces.oam.submitForm('loginForm','loginForm:hiddenbtn');
		} else {
			//alert(errormsg);
			if (isAndroidApp) {
				window.AlertInterface.showAlert(errormsg);
			} else if (isIPhoneApp && appVer > '1.0') {
				window.location = "alertinterface://" + encodeURI(errormsg);
			} else {
				customAlert(errormsg);
			}
			return false;
		}
	} else {
		 return myfaces.oam.submitForm('loginForm','loginForm:hiddenbtn');
	}
}

function validateUserName(username) {
    var error = null;
    var illegalChars = /[\W_]/; // allow only letters and numbers

    if (username == "") {
        error = emptyfield;
    } else if (username.length < 6) {
        error = usernameSmall;
    } else if (illegalChars.test(username)) {
    	if (lettersandnumbers.indexOf("{0}") > -1 ) {
	        error = userNameTxt + ":" + lettersandnumbers.substr(3,lettersandnumbers.length-1);
	    }
    }

   return error;
}

function validatePassword(password) {
    var error = null;
    var illegalChars = /[\W_]/; // allow only letters and numbers

    if (password == "") {
        error = emptyfield;
    } else if (password.length < 6) {
        error = passwordSmall;
    } else if (illegalChars.test(password)) {
	    if (lettersandnumbers.indexOf("{0}") > -1 ) {
		    error = passTxt + ":" +  lettersandnumbers.substr(3,lettersandnumbers.length-1);
		}
    }

   return error;
}