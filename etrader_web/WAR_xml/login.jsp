<% response.addHeader("Cache-Control", "no-cache"); response.addHeader("Pragma", "no-cache"); %><%@ taglib uri="/WEB-INF/core_jstl.tld" prefix="c" %><%@ page contentType="text/xml; charset=UTF-8" %><%@ page pageEncoding="UTF-8" %><?xml version="1.0" encoding="UTF-8"?>
<binaryOptions>
	<jsp:useBean id="reqProc" class="il.co.etrader.web.xml.XMLlogin" scope="request" />
	<response id="${reqProc.id}">
		<error>
			<code>${reqProc.errorCode}</code>
			<description>${reqProc.errorMessage}</description>
		</error>
	</response>
</binaryOptions>