
function g(id){return document.getElementById(id);}
function fBlur(field){
	if(field.value == ""){
		field.value = field.defaultValue;
	}
	else if(field.value == field.defaultValue){
		field.value = "";
	}
	field.onblur = function(){
		fBlur(field);
	}
}
var gd_slider_v2_t2 = null;
function gd_play_slider(id,id_nav,start){
	var img = g(id);
	var imgs = img.getElementsByTagName('img');
	var div = img.getElementsByTagName('div')[0];
	var n = g(id_nav);
	// var sum = 0;
	for(var i=0;i<imgs.length;i++){
		var s = document.createElement('span');
		if(i == 0){s.className = "hov";}
		n.appendChild(s);
		n.appendChild(document.createTextNode(''));
		// sum += imgs[i].offsetWidth;
	}
	// div.style.width = sum+"px";
	if(start){
		gd_slider_st2(id,id_nav,419);
	}
}
function gd_slider_st2(id,id_nav,d){
	var doSetTimeout2 = function(){
		gd_slider(id,id_nav,d);
	}
	gd_slider_v2_t2 = setTimeout(doSetTimeout2,5000);
}
var gd_slider_v2_t1 = null;
function gd_slider(id,id_nav,d){
	var div = g(id).getElementsByTagName('div')[0];
	var c_left = div.offsetLeft;

	if((div.offsetWidth+div.offsetLeft-(d)) <= 0){
		var scr_left = div.offsetWidth-(d);
		var dir = "left";
	}
	else{
		var scr_left = d;
		var dir = "right";
	}
	var n = g(id_nav).getElementsByTagName('span');
	for(var i=0;i<n.length;i++){
		if(n[i].className == "hov"){
			n[i].className = "";
			if((i+1) >= n.length){n[0].className = "hov";}
			else{n[i+1].className = "hov";}
			break;
		}
	}

	var inc = 0;
	var doSetTimeout = function(){
		inc = Math.round(scr_left*0.2);
		if(inc < 1){inc = 1};
		if(dir == "right"){
			div.style.left = div.offsetLeft-inc+"px";
		}
		else{
			div.style.left = div.offsetLeft+inc+"px";
		}
		scr_left -= inc;

		if(scr_left > 0){
			gd_slider_v2_t1 = setTimeout(doSetTimeout,30);
		}
		else{
			gd_slider_st2(id,id_nav,d);
		}
	}
	doSetTimeout();
}
var gd_c_tab = 1;
function gd_ch_tab(th,tab){
	var n = g('gd_tabs1_h').getElementsByTagName('span');
	for(var i=0;i<n.length;i++){
		n[i].className = "";
	}
	th.className = "hov";
	g('id'+gd_c_tab).style.display = "none";
	gd_c_tab = tab;
	g('id'+tab).style.display = "block";
	gd_height(g('id'+tab));
}
function toplev(th){
	var el = th;
	while(el){
		if(el.className.search('_TOP_LEV_') >= 0){return el;}
		else{el = el.parentNode;}
	}
	return false;
}
var gd_ctr_tab2 = new Array();
function gd_oc(th,lvl){
	var cont = th.parentNode.getElementsByTagName('ul')[0];
	if(cont.style.display == "block"){
		cont.style.display = "none";
		var span = th.getElementsByTagName('span')[0];
		span.className = span.className.replace('hov','_noHov_');
	}
	else{
		cont.style.display = "block";
		gd_height(cont);
		var span = th.getElementsByTagName('span')[0];
		span.className = span.className.replace('_noHov_','hov');
	}
	if((gd_ctr_tab2[lvl] != undefined)&&(gd_ctr_tab2[lvl] != th)){
		gd_ctr_tab2[lvl].parentNode.getElementsByTagName('ul')[0].style.display = "none";
		gd_ctr_tab2[lvl].getElementsByTagName('span')[0].className = gd_ctr_tab2[lvl].getElementsByTagName('span')[0].className.replace('hov','_noHov_');
	}
	gd_ctr_tab2[lvl] = th;
}
function gd_height(el){
	var els = el.getElementsByTagName('*');
	for(var i=0;i<els.length;i++){
		if(els[i].className.search('show_text') >= 0){
			els[i].style.height = els[i].getElementsByTagName('*')[0].offsetHeight+"px";
		}
		if(els[i].className.search('tab2_tabs') >= 0){
			els[i].parentNode.style.paddingTop = ((els[i].parentNode.offsetHeight-els[i].offsetHeight)/2)+'px';
		}
	}
}
function gd_fix_m_h(id){
	var el = g(id).getElementsByTagName('span');
	var h = 0;
	var w = Math.round(g(id).offsetWidth/3);
	for(var i=0;i<el.length;i++){
		el[i].style.width = w-3+"px";
	}
	for(var i=0;i<el.length;i++){
		if(el[i].offsetHeight > h){h = el[i].offsetHeight;}
	}
	for(var i=0;i<el.length;i++){
		var hh = el[i].offsetHeight;
		var p = ((h-hh)/2);
		var pb = p;
		if((p % 2) != 0){p = Math.round(p);pb = p-1;}
		// console.log(p,pb);
		el[i].style.paddingTop = p+7+'px';
		el[i].style.paddingBottom = pb+7+'px';
	}
}



//NEW CODE
function get_scr_size_y(){
	if(document.all){return(document.documentElement.clientHeight);}
	else{return(window.innerHeight);}
}
function get_scr_size_x(){
	if(document.all){return(document.body.clientWidth);}
	else{return(window.innerWidth);}
}
function has3d(){
    return ('WebKitCSSMatrix' in window && 'm11' in new WebKitCSSMatrix());
}
function isNothing(v){
	if(v == ''){return true;}
	else{return false;}
}

var supp_3d = false;
var supp_2d = false;
var tr_pf = '';
if(has3d()){supp_3d = true;}

var scrH,scrW;
var f_14,f_17,f_21,f_25,f_27,but_h25,but_h23,f_30,f_18,f_25,f_17_5;//fonts sizes
var but_h25_p,but_h23_p;
var h_29,h_51,h_60;//heights
var w_150,w_420;//widths
var slider1_w,slider1_otn=1.15427,slider2_otn=1.45993;

var ua = navigator.userAgent.toLowerCase();
if(ua.indexOf("chrome")!=-1){
	var s = document.createElement('meta')
	s.name = "viewport";
	s.content = "width="+screen.width+"; user-scalable=no;";
	document.getElementsByTagName('head')[0].appendChild(s);
	// g('meta1').setAttribute('content',"width="+screen.width+";");
	// alert('chrome');
}
else if(ua.indexOf("iphone")!=-1){
	var s = document.createElement('meta')
	s.name = "viewport";
	s.content = "width=device-width; initial-scale=0.5; minimum-scale=0.5; maximum-scale=0.5; user-scalable=no;";
	document.getElementsByTagName('head')[0].appendChild(s);
	// g('meta1').content = "width=device-width; initial-scale=0.5; minimum-scale=0.5; maximum-scale=0.5; user-scalable=no;";
}
else{
	var s = document.createElement('meta')
	s.name = "viewport";
	s.content = "width="+screen.width+"; user-scalable=no;";
	document.getElementsByTagName('head')[0].appendChild(s);
	// g('meta1').content = "width="+screen.width+"; user-scalable=no;";
	// g('meta1').setAttribute('content',"width="+screen.width+"; user-scalable=no;");
}
function init_js(){
	if(ua.indexOf("iphone")==-1){
		g('body').style.width = screen.width+"px";
		scrW = screen.width;
		scrH = screen.height;
	}
	else{
		scrW = get_scr_size_x();
		scrH = get_scr_size_y();
	}
	
	if(scrW > scrH){var d = scrH;}
	else{var d = scrW;}

	if(d<241){
		h_29 = 25;h_51 = 45;h_60=50;h_27=23;
		f_14 = 12;f_17 = 13;f_21=18;f_27=14;f_25=15;but_h25=25;f_30=25;f_20=20;f_18=18;but_h23=21;f_17_5=18;
		w_150=80;w_420=230;
		but_h25_p=18;but_h23_p=21;
		try{
			g('hide_top').style.display = "none";
		}catch(e){}
	}
	else if((d >= 241)&&(d < 321)){
		h_29 = 27;h_51 = 48;h_60=55;h_27=25;
		f_14 = 13;f_17 = 15;f_21=19;f_27=15;f_25=18;but_h25=25;f_30=28;f_20=24;f_18=20;but_h23=23;f_17_5=20;
		w_150=97;w_420=270;
		but_h25_p=18;but_h23_p=20;
		try{
			g('hide_top').style.display = "none";
		}catch(e){}
	}
	else if((d >= 321)&&(d < 481)){//S2
		h_29 = 29;h_51 = 51;h_60=60;h_27=27;
		f_14 = 14;f_17 = 17;f_21=21;f_27=21;f_25=25;but_h25=25;f_30=30;f_20=26;f_18=23;but_h23=25;f_17_5=23;
		w_150=150;w_420=420;
		but_h25_p=18;but_h23_p=18;
	}
	else if((d >= 481)&&(d < 601)){
		h_29 = 30;h_51 = 53;h_60=62;h_27=29;
		f_14 = 15;f_17 = 18;f_21=23;f_27=28;f_25=27;but_h25=29;f_30=32;f_20=30;f_18=28;but_h23=30;f_17_5=28;
		w_150=170;w_420=420;
		but_h25_p=15;but_h23_p=13;
	}
	else if((d >= 601)&&(d < 801)){//S3
		h_29 = 33;h_51 = 60;h_60=65;h_27=31;
		f_14 = 18;f_17 = 25;f_21=26;f_27=30;f_25=33;but_h25=35;f_30=33;f_20=36;f_18=34;but_h23=35;f_17_5=30;
		w_150=190;w_420=600;
		but_h25_p=12;but_h23_p=11;
	}
	else if((d >= 801)&&(d < 1081)){
		h_29 = 35;h_51 = 62;h_60=70;h_27=33;
		f_14 = 20;f_17 = 28;f_21=28;f_27=34;f_25=35;but_h25=38;f_30=35;f_20=40;f_18=34;but_h23=38;f_17_5=34;
		w_150=210;w_420=620;
		but_h25_p=10;but_h23_p=9;
	}
	else{
		h_29 = 37;h_51 = 64;h_60=75;h_27=35;
		f_14 = 21;f_17 = 30;f_21=30;f_27=36;f_25=37;but_h25=43;f_30=36;f_20=42;f_18=36;but_h23=38;f_17_5=36;
		w_150=220;w_420=620;
		but_h25_p=7;but_h23_p=9;
	}
	
	// g('header').style.padding = tb_hed+"px "+lr_pad+"px";
	
	var b = g('gd_cont');
	// b.style.width = scrW+"px";
	var el = b.getElementsByTagName('*');
	for(var i=0;i<el.length;i++){
		//font sizes
		if(el[i].className.search('NoNe_f') >= 0){}
		else if(el[i].className.search('f_14') >= 0){el[i].style.fontSize = f_14+"px";}
		else if(el[i].className.search('f_17') >= 0){el[i].style.fontSize = f_17+"px";}
		else if(el[i].className.search('f_20') >= 0){el[i].style.fontSize = f_20+"px";}
		else if(el[i].className.search('f_21') >= 0){el[i].style.fontSize = f_21+"px";}
		else if(el[i].className.search('f_25') >= 0){el[i].style.fontSize = f_25+"px";}
		else if(el[i].className.search('f_27') >= 0){el[i].style.fontSize = f_27+"px";}
		else if(el[i].className.search('but_h25') >= 0){el[i].style.fontSize = but_h25+"px";el[i].style.paddingTop = but_h25_p+"px";}
		else if(el[i].className.search('but_h23') >= 0){el[i].style.fontSize = but_h23+"px";el[i].style.paddingTop = but_h23_p+"px";}
		else if(el[i].className.search('f_30') >= 0){el[i].style.fontSize = f_30+"px";}
		else if(el[i].className.search('f_18') >= 0){el[i].style.fontSize = f_18+"px";}
		//el heights
		if(el[i].className.search('NoNe_h') >= 0){}
		else if(el[i].className.search('h_27') >= 0){el[i].style.height = h_27+"px";}
		else if(el[i].className.search('h_29') >= 0){el[i].style.height = h_29+"px";}
		else if(el[i].className.search('h_51') >= 0){el[i].style.height = h_51+"px";}
		else if(el[i].className.search('h_60') >= 0){el[i].style.height = h_60+"px";}
		//el widths
		if(el[i].className.search('NoNe_w') >= 0){}
		else if(el[i].className.search('w_150') >= 0){el[i].style.width = w_150+"px";}
		else if(el[i].className.search('w_420') >= 0){el[i].style.width = w_420+"px";}

		if(el[i].className.search('tab2_tabs') >= 0){
			el[i].parentNode.style.paddingTop = ((el[i].parentNode.offsetHeight-el[i].offsetHeight)/2)+'px';
		}
		if(el[i].className.search('sl_1') >= 0){
			el[i].style.width = w_420+"px";
			el[i].style.height = w_420/slider1_otn+"px";
		}
		if(el[i].className.search('sl_2') >= 0){
			el[i].style.width = w_420+"px";
			el[i].style.height = w_420/slider2_otn+"px";
		}
	}

	for(var i=0;i<el.length;i++){
		if(el[i].className.search('show_text') >= 0){
			el[i].style.height = el[i].getElementsByTagName('*')[0].offsetHeight+"px";
		}
	}
	
	try{
		var gd_lang_t_in = g('gd_lang_t_in');
		var padd = (gd_lang_t_in.parentNode.offsetHeight-gd_lang_t_in.offsetHeight)/2;
		if(padd > 1){
			gd_lang_t_in.style.paddingTop = padd+"px";
			gd_lang_t_in.style.paddingBottom = padd+"px";
		}
	}
	catch(e){}
	if(isNothing(b.style.msTransform)){supp_2d = true;tr_pf = 'msTransform';tr_pf2 = 'msTransition';}
	else if(isNothing(b.style.webkitTransform)){supp_2d = true;tr_pf = 'webkitTransform';tr_pf2 = 'webkitTransition';}
	else if(isNothing(b.style.oTransform)){supp_2d = true;tr_pf = 'transform';tr_pf2 = 'transition';}
	else if(isNothing(b.style.mozTransform)){supp_2d = true;tr_pf = 'mozTransform';tr_pf2 = 'mozTransition';}
	else if(isNothing(b.style.transform)){supp_2d = true;tr_pf = 'transform';tr_pf2 = 'transition';}
	
	// scrW = get_scr_size_x();
	// scrH = get_scr_size_y();
	
	gd_cssSlider('gd_slider');
	g('gd_cont').addEventListener("touchstart", close_lng, false);
}
var gd_cssSlider_t = new Array();
function gd_cssSlider(id){
	var nav = g(id+'_nav');
	if((nav.getElementsByTagName('span').length) == 0){
		var div = g(id);
		var lis = div.getElementsByTagName('li');
		div.getElementsByTagName('ul')[0].style.width = div.offsetWidth*lis.length+"px";
		div.setAttribute('data-w',div.offsetWidth);
		
		for(var i=0;i<lis.length;i++){
			var s = document.createElement('span');
			if(i == 0){s.className = "hov";}
			nav.appendChild(s);
		}
		div.addEventListener("touchstart", ts_slider, false);
		div.addEventListener("touchmove", tm_slider, false);
		div.addEventListener("touchend", te_slider, false);
		div.addEventListener("touchcancel", te_slider, false);
		div.addEventListener("touchleave", te_slider, false);
		gd_cssSlider_t[id] = setTimeout(function(){gd_cssSlider_play(id)},5000);
	}
}
function gd_cssSlider_play(id){
	var ul = g(id).getElementsByTagName('ul')[0];
	var lis = ul.getElementsByTagName('li');
	var w = parseInt(ul.parentNode.getAttribute('data-w'));
	var c = parseInt(ul.getAttribute('data-c'));
	c++;
	if(c >= lis.length){c = 0;}
	var l = -c*w;
	ul.setAttribute('data-c',c);
	ul.style.webkitTransition = "all 0.5s";
	ul.style.transition = "all 0.5s";
	ul.style[tr_pf] = (supp_3d)?"translate3d("+l+"px, 0px,0px)":"translate("+l+"px, 0px)";
	ul.setAttribute('data-trl',l);
	var spans = g(id+'_nav').getElementsByTagName('span');
	for(var i=0;i<spans.length;i++){
		if(i == c){spans[i].className = "hov";}
		else{spans[i].className = "";}
	}
	
	gd_cssSlider_t[id] = setTimeout(function(){gd_cssSlider_play(id)},5000);
}
var touch_st = 0;
var touch_stT = 0;
var org_pos = 0;
function ts_slider(evt){
// alert('st');
	var ul = toplev(evt.target);
	clearTimeout(gd_cssSlider_t[ul.parentNode.id]);
	stop_scrol = false;
	// evt.preventDefault();
	ul.style.webkitTransition = "";
	ul.style.transition = "";
	org_pos = -(parseInt(ul.getAttribute('data-c'))*parseInt(ul.parentNode.getAttribute('data-w')));
	touch_st = evt.changedTouches[0].pageX;
	touch_stT = evt.changedTouches[0].clientY;
}
var scroll_hor = false;
var scroll_ver = false;
function tm_slider(evt){
	var ul = toplev(evt.target);
	var touches = evt.changedTouches[0];
	if(!scroll_hor){
		if((touch_stT-touches.clientY) < 40){
			scroll_ver = false;
			if((touch_st - touches.pageX) > 40){
				evt.preventDefault();
				scroll_hor = true;
			}
		}
		else{
			scroll_ver = true;
		}
	}
	if(!scroll_ver){
		evt.preventDefault();
		var l = org_pos-(touch_st - touches.pageX);
		// console.log(org_pos);
		if((l < 0)&&((l-ul.parentNode.offsetWidth+ul.offsetWidth) > 0)){
			ul.style[tr_pf] = (supp_3d)?"translate3d("+l+"px, 0px,0px)":"translate("+l+"px, 0px)";
			ul.setAttribute('data-trl',l);
		}
	}
	// console.log(scroll_hor);
}
function te_slider(evt){
	// alert('');
	var ul = toplev(evt.target);
	scroll_ver = false;
	scroll_hor = false;
	// evt.preventDefault();
	var touches = evt.changedTouches[0];
	// var dif = touch_st - touches.pageX;
	var l = parseInt(ul.getAttribute('data-trl'))*-1;
	var sum = 0;
	var prev_li = ul.getElementsByTagName('li');
	for(var i=0;i<prev_li.length;i++){
		if((sum < l)&&(l < sum+prev_li[i].offsetWidth)){
			if((sum + (prev_li[i].offsetWidth/2)) < l){var nl = -(sum+prev_li[i].offsetWidth);}
			else{var nl = -sum;}
			break;
		}
		sum += prev_li[i].offsetWidth;
	}
	// if((l-(ul.parentNode.offsetWidth*2)) < -20){
		ul.style.webkitTransition = "all 0.5s";
		ul.style.transition = "all 0.5s";
		ul.style[tr_pf] = (supp_3d)?"translate3d("+nl+"px, 0px,0px)":"translate("+nl+"px, 0px)";
		var c = (nl/parseInt(ul.parentNode.getAttribute('data-w')))*-1;
		if(isNaN(c)){c=0;}
		// console.log(c);
		ul.setAttribute('data-c',c);
		var spans = g(ul.parentNode.id+'_nav').getElementsByTagName('span');
		for(var i=0;i<spans.length;i++){
			if(i == c){spans[i].className = "hov";}
			else{spans[i].className = "";}
		}
		gd_cssSlider_t[ul.parentNode.id] = setTimeout(function(){gd_cssSlider_play(ul.parentNode.id)},5000);
	// }
}
function show_lng(th){
	th.className += ' hov';
}
function close_lng(evt){
	var el = evt.target;
	while(el){
		// console.log(el);
		// try{
			if(el.className.search('TOP_LNG') >= 0){return false;}
			else if(el.className.search('gd_cont') >= 0){g('TOP_LNG').className = g('TOP_LNG').className.replace('hov','');return false;}
			else{el = el.parentNode;}
		// }catch(e){;}
	}
	return false;
}



/*OTHER*/
function AJAXInteraction(url, callback){
	var req = init();
	req.onreadystatechange = processRequest;

	function init(){
		if (window.XMLHttpRequest) {
			return new XMLHttpRequest();
		} else if (window.ActiveXObject) {
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
	}

	function processRequest () {
	  // readyState of 4 signifies request is complete
	  if (req.readyState == 4) {
		// status of 200 signifies sucessful HTTP call
		if (req.status == 200) {
		  if (callback) callback(req.responseXML);
		}
	  }
	}

	this.doGet = function() {
	  // make a HTTP GET request to the URL asynchronously
	  req.open("GET", url, true);
	  req.send(null);
	}
}
/*Change the phone and fax numbers by the value of the country*/
function changePhoneAndFaxByCountry() {
	var countryId = document.getElementById("contactMeForm:countriesList").value;
	var url = "ajax.jsf?countryIdForSupport=" + countryId;
	var ajax = new AJAXInteraction(url, changePhoneAndFaxByCountryCallback);
	ajax.doGet();
}
/*callBack function for changePhoneAndFaxByCountry()*/
function changePhoneAndFaxByCountryCallback(responseXML) {
   var msg = responseXML.getElementsByTagName("SupportPhoneFax")[0].firstChild.nodeValue;
   var splitMsg = msg.split(",");
   //alert(msg[0]);
   //alert(msg[1]);
   //var phoneElm = document.getElementById("phone");
   //var faxElm = document.getElementById("fax");
   var mobile = document.getElementById("contactMeForm:mobilePhone").value = splitMsg[2];
   //phoneElm.innerHTML = splitMsg[0];
   //faxElm.innerHTML = splitMsg[1];
   //if (splitMsg.length >2 && mobile !=null){
  //     mobile.value = splitMsg[2];
  // }
}


