var LTT_KEY = 1;
var LTT_COMMAND = 2;
var LTT_MARKET1 = 3; // Market 1
var LTT_OPP_ID1 = 4; // currentOpportunity 1
var LTT_TREND1 = 5; // percentOfCalls 1 (A number for example 0.33 will mean 33% CALLs and 67% PUTs)
var LTT_MARKET2 = 6; // market 2
var LTT_OPP_ID2 = 7; // currentOpportunity 2
var LTT_TREND2 = 8; // percentOfCalls 2 (A number for example 0.33 will mean 33% CALLs and 67% PUTs)
var LTT_MARKET3 = 9; // market 3
var LTT_OPP_ID3 = 10; // currentOpportunity 3
var LTT_TREND3 = 11; // percentOfCalls 3 (A number for example 0.33 will mean 33% CALLs and 67% PUTs)
var LTT_MARKET_NAME1 = 12;
var LTT_MARKET_NAME2 = 13;
var LTT_MARKET_NAME3 = 14;
var LTT_TREND_CALL1 = 15;
var LTT_TREND_PUT1 = 16;
var LTT_TREND_CALL2 = 17;
var LTT_TREND_PUT2 = 18;
var LTT_TREND_CALL3 = 19;
var LTT_TREND_PUT3 = 20;

var trendsCallWidth_1;
var trendsPutWidth_1;
var trendsCallWidth_2;
var trendsPutWidth_2;
var trendsCallWidth_3;
var trendsPutWidth_3;

//@@
function updateItem_live_trade(item, updateInfo) {
	
	//add field to change display market's name.
	if (updateInfo.isValueChanged(LTT_MARKET1)) {
		updateInfo.addField(LTT_MARKET_NAME1, getMarketName(updateInfo.getNewValue(LTT_MARKET1)));
	}
	if (updateInfo.isValueChanged(LTT_MARKET2)) {
		updateInfo.addField(LTT_MARKET_NAME2, getMarketName(updateInfo.getNewValue(LTT_MARKET2)));
	}
	if (updateInfo.isValueChanged(LTT_MARKET3)) {
		updateInfo.addField(LTT_MARKET_NAME3, getMarketName(updateInfo.getNewValue(LTT_MARKET3)));
	}
	
	//add field to call and put trends.
	var table_trading = document.getElementById("trends");
	//first market.
	if (updateInfo.isValueChanged(LTT_TREND1)) {
		var trends_call = updateInfo.getNewValue(LTT_TREND1) * 100;
		var trends_put = 100 - trends_call;
		var trends_call_percent = Math.round(trends_call) + "%";
		var trends_put_percent = Math.round(trends_put) + "%";
		if (trends_call != "0") {
			trendsCallWidth_1 = (Math.round(trends_call) - 1) + "%";
		} else {
			trendsCallWidth_1 = "0%";
			trends_call_percent = '';
		}
		if (trends_put != "0") {
			trendsPutWidth_1 = (Math.round(trends_put) - 1) + "%";
		} else {
			trendsPutWidth_1 = "0%";
			trends_put_percent = '';
		}
		updateInfo.addField(LTT_TREND_CALL1, trends_call_percent);
		updateInfo.addField(LTT_TREND_PUT1, trends_put_percent);
	}
	//second market.
	if (updateInfo.isValueChanged(LTT_TREND2)) {
		var trends_call = updateInfo.getNewValue(LTT_TREND2) * 100;
		var trends_put = 100 - trends_call;
		var trends_call_percent = Math.round(trends_call) + "%";
		var trends_put_percent = Math.round(trends_put) + "%";
		if (trends_call != "0") {
			trendsCallWidth_2 = (Math.round(trends_call) - 1) + "%";
		} else {
			trendsCallWidth_2 = "0%";
			trends_call_percent = '';
		}
		if (trends_put != "0") {
			trendsPutWidth_2 = (Math.round(trends_put) - 1) + "%";
		} else {
			trendsPutWidth_2 = "0%";
			trends_put_percent = '';
		}
		updateInfo.addField(LTT_TREND_CALL2, trends_call_percent);
		updateInfo.addField(LTT_TREND_PUT2, trends_put_percent);
	}
	//third market.
	if (updateInfo.isValueChanged(LTT_TREND3)) {
		var trends_call = updateInfo.getNewValue(LTT_TREND3) * 100;
		var trends_put = 100 - trends_call;
		var trends_call_percent = Math.round(trends_call) + "%";
		var trends_put_percent = Math.round(trends_put) + "%";
		if (trends_call != "0") {
			trendsCallWidth_3 = (Math.round(trends_call) - 1) + "%";
		} else {
			trendsCallWidth_3 = "0%";
			trends_call_percent = '';
		}
		if (trends_put != "0") {
			trendsPutWidth_3 = (Math.round(trends_put) - 1) + "%";
		} else {
			trendsPutWidth_3 = "0%";
			trends_put_percent = '';
		}
		updateInfo.addField(LTT_TREND_CALL3, trends_call_percent);
		updateInfo.addField(LTT_TREND_PUT3, trends_put_percent);
	}
	
	if (checkAllClose()) {
		//show off hour banner 0
		switchLiveTrends(0);
	} else {
		//show the market table 1
		switchLiveTrends(1);
	}
	
}//updateItem_live_trade

//@
function formatValues_live_trade(item, itemUpdate) {
	//change chart when first maket changed.
	var trends_line_1 = getChildOfType(item, "DIV", 1);
	var trends_chart_1 = getChildOfType(trends_line_1, "DIV", 3);
	var chart_call_1 = getChildOfType(trends_chart_1, "DIV", 1);
	var chart_put_1 = getChildOfType(trends_chart_1, "DIV", 3);
	chart_call_1.style.width = trendsCallWidth_1;
	chart_put_1.style.width = trendsPutWidth_1;
	if (trendsCallWidth_1 == "0%" || trendsPutWidth_1 == "0%") {
		getChildOfType(trends_chart_1, "DIV", 2).style.display = 'none';
	} else {
		getChildOfType(trends_chart_1, "DIV", 2).style.display = 'block';
	}
	
	//change chart when second maket changed.
	var trends_line_2 = getChildOfType(item, "DIV", 2);
	var trends_chart_2 = getChildOfType(trends_line_2, "DIV", 3);
	var chart_call_2 = getChildOfType(trends_chart_2, "DIV", 1);
	var chart_put_2 = getChildOfType(trends_chart_2, "DIV", 3);
	chart_call_2.style.width = trendsCallWidth_2;
	chart_put_2.style.width = trendsPutWidth_2;
	if (trendsCallWidth_2 == "0%" || trendsPutWidth_2 == "0%") {
		getChildOfType(trends_chart_2, "DIV", 2).style.display = 'none';
	} else {
		getChildOfType(trends_chart_2, "DIV", 2).style.display = 'block';
	}
	
	//change chart when third maket changed.
	var trends_line_3 = getChildOfType(item, "DIV", 3);
	var trends_chart_3 = getChildOfType(trends_line_3, "DIV", 3);
	var chart_call_3 = getChildOfType(trends_chart_3, "DIV", 1);
	var chart_put_3 = getChildOfType(trends_chart_3, "DIV", 3);
	chart_call_3.style.width = trendsCallWidth_3;
	chart_put_3.style.width = trendsPutWidth_3;
	if (trendsCallWidth_3 == "0%" || trendsPutWidth_3 == "0%") {
		getChildOfType(trends_chart_3, "DIV", 2).style.display = 'none';
	} else {
		getChildOfType(trends_chart_3, "DIV", 2).style.display = 'block';
	}
	
	trends_line_1.style.display = 'block';
	trends_line_2.style.display = 'block';
	trends_line_3.style.display = 'block';
	// print only exist market.
	if ("0" == itemUpdate.getServerValue(LTT_MARKET1)) {
		trends_line_1.style.display = 'none';
	}
	if ("0" == itemUpdate.getServerValue(LTT_MARKET2)) {
		trends_line_2.style.display = 'none';
	}
	if ("0" == itemUpdate.getServerValue(LTT_MARKET3)) {
		trends_line_3.style.display = 'none';
	}
	
}//formatValues_live_trade

function getMarketName(marketId) {
	for (var i = 0; i < marketsDisplayName.length; i++) {
		if (marketsDisplayName[i][0] == marketId) {
			return marketsDisplayName[i][1];
		}
	}
	return "";
}
var intreval;
function sendTrendToTradeBox(info, invTypeId) {
	
	var div = info.parentNode;
	var div1 = getChildOfType(div, "DIV", 1);
	var span = getChildOfType(div1, "SPAN", 1);	
	var assetId = span.textContent;
	fromGraph = 3; //Trading Trends page.
	tickerClickLive(assetId,'1','1');
	intreval = setInterval(function (){simulatePutOrCallTrends(invTypeId)},1000);
}

function simulatePutOrCallTrends(invTypeId) {
	try {
		var boxTemp = document.getElementById('box0');
		var index;
		boxTemp = getChildOfType(boxTemp, "TABLE", 1);
		boxTemp = getChildOfType(boxTemp, "TBODY", 1);
		boxTemp = getChildOfType(boxTemp, "TR", 2);
		boxTemp = getChildOfType(boxTemp, "TD", 1);
		boxTemp = getChildOfType(boxTemp, "DIV", 1);
		boxTemp = getChildOfType(boxTemp, "TABLE", 1);
		boxTemp = getChildOfType(boxTemp, "TBODY", 1);
		boxTemp = getChildOfType(boxTemp, "TR", 1);
		boxTemp = getChildOfType(boxTemp, "TD", 1);
		boxTemp = getChildOfType(boxTemp, "TABLE", 1);
		boxTemp = getChildOfType(boxTemp, "TBODY", 1);
		if (invTypeId == 1) {
			index = 3;
		} else {
			index = 5;
		}
		boxTemp = getChildOfType(boxTemp, "TR", index);
		boxTemp = getChildOfType(boxTemp, "TD", 1);
		var img = getChildOfType(boxTemp, "IMG", 1);
		var imgSrc = img.src.toString();
		
		if(imgSrc.indexOf('call.gif') > -1 || imgSrc.indexOf('put.gif') > -1) {
			clearInterval(intreval);
			if (typeof img.onclick == "function") {
	    		img.onclick.apply(boxTemp);
			}
			var tradeBox = document.getElementById('box0');
			window.scroll(0,findPos(tradeBox));
		}
	} catch(e) {
		clearInterval(intreval);
	}
}

function findPos(obj) {
	var curtop = 0;
	if (obj.offsetParent) {
		do {
			curtop += obj.offsetTop;
		} while (obj = obj.offsetParent);
	return [curtop];
	}
}

function switchLiveTrends(flag) {
	var liveTradingTrends =  document.getElementById("liveTradingTrends");
	var liveTradingTrendsOffHoursLogin = document.getElementById("liveTradingTrendsOffHoursLogin");
	if (flag == 0) {
		liveTradingTrends.style.display = 'none';
		liveTradingTrendsOffHoursLogin.style.display = 'block';	
	} else {
		liveTradingTrends.style.display = 'block';
		liveTradingTrendsOffHoursLogin.style.display = 'none';
	}
	
	var trendsBox = document.getElementById("trendsBox");
	if(trendsBox != null) {
		if(flag == 0) {
			trendsBox.style.height = "350px";
		} else {
			trendsBox.style.height = "220px";
		}
	}
	switchLiveGlobe(flag);
}

function switchLiveGlobe(flag) {
	var globeLogout =  document.getElementById("globeLogout");
	var globeLogoutOffHours = document.getElementById("globeLogoutOffHours");
	if (flag == 0) {
		globeLogout.style.display = 'none';
		globeLogoutOffHours.style.display = 'block';	
	} else {
		globeLogout.style.display = 'block';
		globeLogoutOffHours.style.display = 'none';
	}
}