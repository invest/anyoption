function showAgreemen(){
	var general_terms_url = general_terms_link.toString()+general_terms_link_cid.toString();
	if(mobile_lp) {
		general_terms_url += "&from=mobilelp";		
	}
	window.open(general_terms_url,'','scrollbars=yes,height=600,width=970,resizable=yes,toolbar=yes,screenX=0,left=1024,screenY=0,top=600');
}
function checkCountryId(){
	var iframe = document.createElement("iframe");
	var domain = document.domain.replace('cdn.','');
	domain = domain.replace('testenv.','');
	//iframe.src="http://cdn."+domain+"/getIp.php?dom="+domain;
	iframe.src="http://cdn."+domain+"/getIp.php?dom="+domain;
	iframe.id="getIp_iframe";
	iframe.name="getIp_iframe";
	iframe.title="getIp_iframe";
	iframe.style.display="none";
	document.body.appendChild(iframe);
	// register_country_select(25,true);
}
var funnel_countryId = '';
function register_country_select(countryId,fromUrl){
	if(fromUrl){
		countryIdByIp = countryId;
		if(skinId != 1) {
			if(isBlockedCountry(countryIdByIp)){
				countryId = firstCountryId;
			}
		}
		if(typeof getQueryVariables_array['funnel_countryId'] != 'undefined'){
			countryId = getQueryVariables_array['funnel_countryId'];
		}
		general_terms_link_cid = "?refresh=true&cid=1";
		g('funnel_countryId').value = 1;
	}
	if((skinId == 1)&&(!fromUrl)){
		g('funnel_phonePrefix').value = countryId;
		g('funnel_countryId').value = 1;
		g('funnel_drop_country').innerHTML = "";
		g('funnel_drop_countryId').innerHTML = '0'+countryId;
		single_errors_map_holder[1][2] = '1';
		checkError(null, g('funnel_drop_h'));
	}
	if(skinId != 1) {
		g("funnel_drop_h").className = "funnel_drop_h";
		g('funnel_countryId').value = countryId;
		g('funnel_drop_country').innerHTML = countryMiniMap[countryId].a2;
		g('funnel_drop_countryId').innerHTML = "+" + countryMiniMap[countryId].phoneCode;
		general_terms_link_cid = "?refresh=true&cid="+countryId;
		single_errors_map_holder[1][2] = '1';
	}
	var reg_drop_ul = g('funnel_drop_ul').getElementsByTagName('li');
	for(var i=0; i<reg_drop_ul.length; i++){
		reg_drop_ul[i].className = reg_drop_ul[i].className.replace(/active/gi,'');
	}
	try{
		g('funnel_drop_li_'+countryId).className += " active";
	}catch(e){}
	reloadCountries('drop_ul_filter');
	funnel_drop_close();
}
var isAllowIp;
function allowIp(flag){
	isAllowIp = flag;
	return isAllowIp;
}
function isBlockedCountry(countryId){
	var res = false;
	switch (countryId) {
			case 1:
			case 97:
			case 99:
			case 220:
				res = true;
				break;
			}
	return res;			
}
function getQueryVariables() {
	var fields_from_url = ['email','firstName','last_Name','funnel_countryId','mobilePhone','mobilePhonePref'];
	var rtn = new Array();
	rtn[0] = 0;
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
		if(fields_from_url.indexOf(pair[0]) > -1){
			rtn[pair[0]] = decodeURIComponent(pair[1]);
			rtn[0]++;
		}
    }
    return rtn;
}
var getQueryVariables_array = getQueryVariables();
function get_funnel_html(){
	getQueryVariables()
	if(!g('funnel_firstName')){
		var ajax_url = context_path_ajax + "/jsp/html_elements/register_content_funnel.jsf?s=" + skinId + "&locale=" + funnel_locale;
		if(mobile_lp){
			ajax_url = context_path_ajax + "/jsp/html_elements/register_content_funnel_mobile.jsf?s=" + skinId + "&from=mobilelp" + "&locale=" + funnel_locale;
		}
		$.ajax({
			url: ajax_url,
			success:function(result){
				g(funnel_div_id).innerHTML = result;
				checkCountryId();
				if(typeof isShortForm == "undefined"){var isShortForm = 'true';}
				g('register_from_short_reg').value = (isShortForm === 'false')?"false":"true";
				funnel_fill_inputs();
			}
		});
	}else{
		checkCountryId();
		if(typeof isShortForm == "undefined"){var isShortForm = 'true';}
		g('register_from_short_reg').value = (isShortForm === 'false')?"false":"true";
		funnel_fill_inputs();
		if(getQueryVariables_array[0] > 1){
			funnel_submit();
			try {
				g('full_reg_top_text_default').style.display='none';
				g('full_reg_top_text_alt').style.display='block';
			} catch(e) {;}
		}
	}
}
function funnel_fill_inputs(){
	if(typeof getQueryVariables_array['firstName'] != 'undefined'){
		g('funnel_firstName').value = getQueryVariables_array['firstName'];
	}
	if(typeof getQueryVariables_array['last_Name'] != 'undefined'){
		g('funnel_lastName').value = getQueryVariables_array['last_Name'];
	}
	if(typeof getQueryVariables_array['email'] != 'undefined'){
		g('funnel_email').value = getQueryVariables_array['email'];
	}
	if(typeof getQueryVariables_array['email'] != 'undefined'){
		g('funnel_email').value = getQueryVariables_array['email'];
	}
	if(typeof getQueryVariables_array['mobilePhonePref'] != 'undefined'){
		g('funnel_phonePrefix').value = getQueryVariables_array['mobilePhonePref'];
		g('funnel_drop_countryId').innerHTML = '0'+parseInt(getQueryVariables_array['mobilePhonePref'],10);
		g('funnel_drop_country').innerHTML = '';
	}
	if(typeof getQueryVariables_array['mobilePhone'] != 'undefined'){
		var sub = '';
		if(typeof getQueryVariables_array['mobilePhoneCode'] != 'undefined'){
			getQueryVariables_array['mobilePhoneCode'] = getQueryVariables_array['mobilePhoneCode'].toString().replace('+','');
			if(getQueryVariables_array['mobilePhoneCode'].length > 3){
				sub = getQueryVariables_array['mobilePhoneCode'].substr(1,10);
			}
		}
		
		g('funnel_mobilePhone').value = sub+getQueryVariables_array['mobilePhone'].toString();
	}
}
/* insert into register attempts AJAX call */
function insertUpdateRegisterAttempts() {
	if(!register_lp){
		var isMarketingTracking = (typeof getQueryVariables_array['marketingTracking'] != 'undefined')?getQueryVariables_array['marketingTracking']:false;
		
		var firstName = g("funnel_firstName");
		var lastName = g("funnel_lastName");
		var email = g("funnel_email");
		var countryId = g("funnel_countryId");
		var landMobilePhone = g("funnel_mobilePhone");
		var landMobilePhonePref = g("funnel_phonePrefix");
	
		if((emailValidationRegEx.test(email.value))||(landMobilePhone.value.length > 6)){
			var dataString = '';
			if(typeof isShortForm == undefined){var isShortForm = 'true';}
			dataString += (isShortForm === 'false')?"emailRegisterAttempts":"quickStartEmailRegisterAttempts";
	
			dataString += "=" + encodeURIComponent((email.value !== email.defaultValue)?email.value:'')
				+ "&firstNameRegisterAttempts="	+ encodeURIComponent((firstName.value !== firstName.defaultValue)?firstName.value:'')
				+ "&lastNameRegisterAttempts=" + encodeURIComponent((lastName.value !== lastName.defaultValue)?lastName.value:'')
				+ "&mobilePhoneRegisterAttempts=" + encodeURIComponent((landMobilePhone.value !== landMobilePhone.defaultValue)?landMobilePhonePref+landMobilePhone.value:'')
				+ "&countryRegisterAttempts=" + encodeURIComponent(countryId.value)
				+ "&isShortForm=" + isShortForm
				+ "&marketingTracking=" + isMarketingTracking;
			$.ajax({
				type: "POST",
				url: context_path_ajax+"/jsp/ajax.jsf",
				data: dataString
			});
		}
	}
}
function funnel_submit(){
	var btn_open_account = g('btn_open_account');
	btn_open_account.className += " disabled";
	btn_open_account.onclick = function(){return false;}
		
	var selectedCountryId = parseInt(g('funnel_countryId').value);
	if(((isBlockedCountry(countryIdByIp) && !isAllowIp) || isBlockedCountry(selectedCountryId)) && skinId != 1) {	    
		showErrorsBlocked(isBlockedCountry(selectedCountryId));
		return;					
	}
	isShowErrors = false;
	isAsync = false;
	if(skinId == 1) {
		validateHolderIdNum(g(idPrefix + "idNum"));
	}
	validateCheckbox();
	validatePasswordConfirm();
	validatePassword();
	validatePhoneNumberTag();
	if(skinId == 1) {
		validateEtraderPhoneCode();
	}
	validateEmail();
	validateLastName();
	validateFirstName();
	isAsync = true;
	isShowErrors = true;
	if(typeof register_page == 'undefined'){register_page = false;}
	if((errorMap.size() <= 2)||(register_page)||(mobile_lp)){
		showErrors();
	}	
	if (errorMap.size() == 0) {
		g('funnel_form').submit();
	} else {
		handle_error_count(errorMap.size());
	}
}
function funnel_results(jsonRes){
	var count = 0;
	for (var key in jsonRes) {
		if (jsonRes.hasOwnProperty(key)) {
			if(key == "forbiddenRegisterRedirect"){
				window.location = jsonRes[key];
				return;
			}else{
				var field = g(key);
				field.className = field.className.replace(/correct/g,'');
				field.className += " error";
				var error_msg_holder = g(key+"_error_txt");
				error_msg_holder.innerHTML = jsonRes[key];
				error_msg_holder.style.display = "";
				error_msg_holder = g(key+"_error_txt_in");
				error_msg_holder.innerHTML = jsonRes[key];
				error_msg_holder.style.display = "";
				
				var single_err_check = compare2dArray(single_errors_map,key);
				if(single_err_check[0]){
					var errSignTag = g(single_errors_map[single_err_check[1]][0] + "_error");
				}
				else{
					var errSignTag = g(errorMap.get(id).errorId + "_error");
				}
				errSignTag.className = "funnel_error_bad";

				count++;
			}
		}
	}
	handle_error_count(count);
}
function handle_error_count(count){
	if(typeof register_page == 'undefined'){register_page = false;}
	if(mobile_lp) {
		if(count == 0) {
			var protocol = window.location.protocol;
			var host = window.location.host;
			var path_array = window.location.pathname.split("/");
			var path_folder = "";
			for(var i = 1; i < path_array.length - 1; i++) {
				path_folder += (path_array[i] + "/");
			}
			var success_page = "success_" + getLangCodeBySkinId(skinId) + ".shtml?" + queryString;
			var redirect_url = protocol + "//" + host + "/" + path_folder + success_page;
			window.location = redirect_url;
		}else{
			var btn_open_account = g('btn_open_account');
			btn_open_account.className = btn_open_account.className.replace(" disabled","");
			btn_open_account.onclick = function(){funnel_submit();return false;}
			return false;
		}
	}else{
		if(count === 0){//you are registered and logged -> redirecting to deposit (or "thank you page" if mobile LP)
			window.location = deposit_page_url;
		}else if(count < 3){//you are not so bad, stay on page and fill correctly
			var btn_open_account = g('btn_open_account');
			btn_open_account.className = btn_open_account.className.replace(" disabled","");
			btn_open_account.onclick = function(){funnel_submit();return false;}
			return false;
		}else{//you cannot be more wrong go to full reg
			if(!register_page){//if short reg
				var firstName = g("funnel_firstName");
				var lastName = g("funnel_lastName");
				var email = g("funnel_email");
				var countryId = g("funnel_countryId");
				var phonePrefix = g("funnel_phonePrefix");
				var landMobilePhone = g("funnel_mobilePhone");
				
				var url = '';
				url += ((email.value !== "")&&(email.value !== email.defaultValue))?((url === '')?"?":"&")+"email=" + encodeURIComponent(email.value):'';
				url += ((firstName.value !== "")&&(firstName.value !== firstName.defaultValue))?((url === '')?"?":"&")+"firstName=" + encodeURIComponent(firstName.value):'';
				url += ((lastName.value !== "")&&(lastName.value !== lastName.defaultValue))?((url === '')?"?":"&")+"last_Name=" + encodeURIComponent(lastName.value):'';
				url += (countryId.value != "")?((url === '')?"?":"&")+"funnel_countryId=" + encodeURIComponent(countryId.value):'';
				url += (phonePrefix.value != "")?((url === '')?"?":"&")+"mobilePhonePref=" + encodeURIComponent(phonePrefix.value):'';
				url += ((landMobilePhone.value !== "")&&(landMobilePhone.value !== landMobilePhone.defaultValue))?((url === '')?"?":"&")+"mobilePhone=" + encodeURIComponent(landMobilePhone.value):'';
				window.location = register_page_url+url;		
			}
			else{
				var btn_open_account = g('btn_open_account');
				btn_open_account.className = btn_open_account.className.replace(" disabled","");
				btn_open_account.onclick = function(){funnel_submit();return false;}
			}
		}
	}
}
function newCard_submit(full){
	var btn = g('btn_newCard');
	btn.className += " disabled";
	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateZipCode();
		validateCityName();
		validateStreet();
	}
	validateHolderName();
	if(skinId == 1){
		validateHolderIdNum();
		if(full) {
			validateBirthYear();
			validateBirthMonth();
			validateBirthDay();
		}
	}
	validateExpYear();
	validateExpMonth();
	validateCCPass();
	validateCCNumber();
	validateCCType();
	validateDepositAmount(null);
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('newCardForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('newCardForm','newCardForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_newCard');
		btn.className = btn.className.replace(" disabled",'');
		btn.onclick = function(){newCard_submit(full);}
	}
}
function direct24_submit(full, paymentType){
	var btn = g('btn_direct24_'+paymentType);
	btn.className += " disabled";
	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateZipCode();
		validateCityName();
		validateStreet();
	}
	validateDepositAmount(null);
	validateBenefiName();
	validateSortCode();
	validateDirectAccNum();
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('direct24_'+paymentType).onsubmit = function(){return true;}
		return myfaces.oam.submitForm('direct24_'+paymentType,'direct24_'+paymentType+':hiddenDeposit');
	}
	else{
		var btn = g('btn_direct24_'+paymentType);
		btn.className = btn.className.replace(" disabled",'');
		btn.onclick = function(){direct24_submit(full, paymentType);}
	}
}
function eps_submit(full){
	var btn = g('btn_epsForm');
	btn.className += " disabled";
	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateZipCode();
		validateCityName();
		validateStreet();
	}
	validateDepositAmount(null);
	validateBenefiName();
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('direct24_4').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('direct24_4','direct24_4:hiddenDeposit');
	}
	else{
		var btn = g('btn_epsForm');
		btn.className = btn.className.replace(" disabled",'');
		btn.onclick = function(){eps_submit(full);}
	}
}
function baropay_submit(full){
	var btn = g('btn_baropay');
	btn.className += " disabled";
	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateZipCode();
		validateCityName();
		validateStreet();
	}
	validateBaroLandLinePhone();
	validateSenderName();
	validateDepositAmount(null);
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('baroPayDepositForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('baroPayDepositForm','baroPayDepositForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_baropay');
		btn.className = btn.className.replace(" disabled",'');
		btn.onclick = function(){baropay_submit(full);}
	}
}
function ukash_submit(full){
	var btn = g('btn_ukashFrom');
	btn.className += " disabled";
	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateZipCode();
		validateCityName();
		validateStreet();
	}
	validateUkashVoucherNumber();
	validateDepositAmount(g('ukashFrom:voucherValue'));
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('ukashFrom').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('ukashFrom','ukashFrom:hiddenDeposit');
	}
	else{
		var btn = g('btn_ukashFrom');
		btn.className = btn.className.replace(" disabled",'');
		btn.onclick = function(){ukash_submit(full);}
	}
}
function webmoney_submit(full){
	var btn = g('btn_webmoney');
	btn.className += " disabled";
	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateZipCode();
		validateCityName();
		validateStreet();
	}
	validateDepositAmount(null);
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('webMoneyDepositForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('webMoneyDepositForm','webMoneyDepositForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_webmoney');
		btn.className = btn.className.replace(" disabled",'');
		btn.onclick = function(){webmoney_submit(full);}
	}
}
function envoyDeposit_submit(full){
	var btn = g('btn_envoyDeposit');
	btn.className += " disabled";
	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateZipCode();
		validateCityName();
		validateStreet();
	}
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('directDepositForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('directDepositForm','directDepositForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_envoyDeposit');
		btn.className = btn.className.replace(" disabled",'');
		btn.onclick = function(){envoyDeposit_submit(full);}
	}
}
function moneybookers_submit(full){
	var btn = g('btn_moneybookers');
	btn.className += " disabled";
	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateZipCode();
		validateCityName();
		validateStreet();
	}
	validateDepositAmount(null);
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('moneybookersDepositForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('moneybookersDepositForm','moneybookersDepositForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_moneybookers');
		btn.className = btn.className.replace(" disabled",'');
		btn.onclick = function(){moneybookers_submit(full);}
	}
}
function deltapay_submit(full){
	var btn = g('btn_deltapay');
	btn.className += " disabled";
	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateZipCode();
		validateCityName();
		validateStreet();
	}
	validateDepositAmount(null);
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('deltaPayForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('deltaPayForm','deltaPayForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_deltapay');
		btn.className = btn.className.replace(" disabled",'');
		btn.onclick = function(){deltapay_submit(full);}
	}
}
function oldCard_submit(full){
	var btn = g('btn_oldCard');
	btn.className += " disabled";
	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	validateCCSelect();
	validateDepositAmount(null);
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('depositForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('depositForm','depositForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_oldCard');
		btn.className = btn.className.replace(" disabled",'');
		btn.onclick = function(){oldCard_submit(full);}
	}
}
//function used to clear the form after clicking back button
function reColorFields(){
	try{
		inpFocusShort(g('funnel_firstName'),'t');
		inpFocusShort(g('funnel_lastName'),'t');
		inpFocusShort(g('funnel_email'),'t');
		inpFocusShort(g('funnel_mobilePhone'),'t');
		inpFocusShort(g('funnel_password'),'t');
		inpFocusShort(g('funnel_passwordConfirm'),'t');
		if(g('funnel_terms').checked){
			g('funnel_terms').parentNode.className += " checked";
		}
		errorMap.clear();
		showErrors();
	}catch(e){;}
}
function funnel_drop(th){
	th.parentNode.className += " active";
	setFocusOnDropDown();
}
var funnel_drop_timer = null;
function funnel_drop_close(){
	clearTimeout(funnel_drop_timer);
	function doIt(){
		var el = g('funnel_drop_h');
		el.className = el.className.replace(/active/g,'');
	}
	funnel_drop_timer = setTimeout(doIt,100);
}
function funnel_drop_keep_open(){
	clearTimeout(funnel_drop_timer);
}
function filterCountries(obj){
	var li = obj.parentNode;
	li.style.position = 'static';
	var list = li.parentNode.getElementsByTagName("li");
	for (var i = 1; i < list.length; i++) {
	     var match = list[i].innerHTML.toLowerCase();
	     var ex = obj.value.toLowerCase();
	     if (ex.length > 0){
		   if(ex == match.substring(0,ex.length)){
		      list[i].style.display = 'block';
		   }else{
		      list[i].style.display = 'none';
		   }
	     }else{
		    list[i].style.display = 'block';
		 } 
	 } 
}
function reloadCountries(id){
	var element = g(id);
	element.value = '';
	var li = element.parentNode;
	li.style.position = 'absolute';
	var list = li.parentNode.getElementsByTagName("li");
	for (var i = 1; i < list.length; i++) {
		list[i].style.display = 'block';
    } 
}
function setFocusOnDropDown(){
    var element = document.getElementById('drop_ul_filter');
	element.focus();
}
function compare2dArray(arr,str){
	for(var i=0;i<arr.length;i++){
		for(var ii=0;ii<arr[i].length;ii++){
			if(arr[i][ii] == str){
				return new Array(true,i,ii);
			}
		}
	}
	return new Array(false,0,0);
}
function check_funnel_error_map(index){
	var count = single_errors_map[index].length;
	var bad = 0;
	var good = 0;
	var notchecked = 0;
	for(var i=1; i<count; i++){
		if(single_errors_map_holder[index][i] == '1'){good++;}
		else if(single_errors_map_holder[index][i] == '0'){bad++;}
		else{notchecked++;}
	}
	if(bad > 0){return 'funnel_error_bad';}
	else if(good === (count-1)){return 'funnel_error_good';}
	else{return 'funnel_error_hide'}
}
addScriptToHead('map.js','initRegForm_hashmap');
