//
function binary0100_i_pop_up(horizontal, vertical) {
	var pop = document.getElementById("iMessage");
	pop.style.visibility = "visible";
	var pic = document.getElementById("infoPic");
	positionDiv(pic, pop, horizontal, vertical);
}

function binary0100_info_pop_up() {
	var pop = document.getElementById("popUp");
	if (pop == null) {
		var pop = window.parent.document.getElementById("popUp");
	}
	pop.style.top = "28%";
	pop.style.right = "33%";
	pop.style.left = "33%";
	pop.style.visibility = "visible";
	var pic = document.getElementById("big_i");
}

function binary0100_general_pop_up() {
	var pop = document.getElementById("popUp");
	if (pop == null) {
		var pop = window.parent.document.getElementById("popUp");
	}
	//pop.style.top = "80%";
	pop.style.bottom = "50px";
	pop.style.right = "33%";
	pop.style.left = "33%";
	pop.style.visibility = "visible";
	var pic = document.getElementById("big_i");
	
	var temp = document.getElementById("investments0100WhatIsBinary");
	
	temp.contentWindow.goToGenralTerms();	
			
}

function binary0100_general_exp_pop_up() {
	var pop = document.getElementById("popUp");
	if (pop == null) {
		var pop = window.parent.document.getElementById("popUp");
	}
	//pop.style.top = "80%";
	pop.style.bottom = "50px";
	pop.style.right = "33%";
	pop.style.left = "33%";
	pop.style.visibility = "visible";
	var pic = document.getElementById("big_i");
	
	var temp = document.getElementById("investments0100WhatIsBinary");
	
	temp.contentWindow.goToGenralTerms();
	temp.contentWindow.goToExpiryTab();
				
}
//put the golden div in the right position on the page345
//! useing tigra hint!
function positionDiv(positionDiv, showDiv, left, top) {
	var mTd = positionDiv;
	var conDiv = showDiv;
	if (null != mTd && null != conDiv) {
		$(conDiv).css("left", f_getPositionDiv(mTd, 'Left') + left);
		$(conDiv).css("top", f_getPositionDiv(mTd, 'Top') - top);
	}
}

//position the banner in the right place on the page
function f_getPositionDiv (e_elemRef, s_coord) {
//	log("f_getPosition e_elemRef: " + e_elemRef + " s_coord: " + s_coord);

	var n_pos = 0, n_offset,
		e_elem = e_elemRef;

	while (e_elem) {
		n_offset = e_elem["offset" + s_coord];
		n_pos += n_offset;
		e_elem = e_elem.offsetParent;
	}

	// margin correction in some browsers (not supported for GM)
	if (false) {
		n_pos += parseInt(document.body[s_coord.toLowerCase() + 'Margin']);
	}

	e_elem = e_elemRef;
	while (e_elem != document.body) {
		n_offset = e_elem["scroll" + s_coord];
		if (n_offset && e_elem.style.overflow == 'scroll')
			n_pos -= n_offset;
		e_elem = e_elem.parentNode;
	}

//	log("f_getPosition end.");
	return n_pos;
}

/*********** selectTag.js ************/
function selectTag(showContent, selfObj) {

    var tag = document.getElementById("tags_1").getElementsByTagName("li");
    var taglength = tag.length;
    var imageNumClass = showContent.charAt( showContent.length-1 );
    var times = 0;
    for (i = 0; i < taglength; i++){
        tag[i].className = "offTag";
    }
    
    try{
    	selfObj.firstElementChild.className = "selectTag";
    	}
    catch(err){
    	selfObj.firstChild.className = "selectTag";
    	}	

    for (i = 0; j = document.getElementById("tagContent" + i); i++) {
        j.style.display = "none";
        times++;
    }
    if(times == 2){
    	i++;
    	j = document.getElementById("tagContent" + i);
    	j.style.display = "none";
    }

    document.getElementById(showContent).style.display = "block";
    document.getElementById("tags_1").setAttribute("class", "tags_image" + imageNumClass);

}

function showIBox(isShow, infoId, picId, picname) {
    
	if (isShow == 'true') {
		document.getElementById(infoId).style.display="block";
	} else {
		document.getElementById(infoId).style.display="none";
	}
	var btn_img = document.getElementById(picId); // switch picture
	btn_img.src = picname;
}

function showElement(isShow, infoId, devNum) {
    var trends_line = infoId.parentNode;
	if (isShow == 'true') {
		var info_msg_call = getChildOfType(trends_line, "DIV", devNum);
    	info_msg_call.style.display = "block";
	} else {
		var info_msg_put = getChildOfType(trends_line, "DIV", devNum);
    	info_msg_put.style.display = "none";
	}
}
