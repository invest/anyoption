/* ProfitLine banner
   Use jQuery just for IE6 */

if (navigator.appVersion.toLowerCase().indexOf("msie 6.0") != -1) {

	// change position from fixed to absolute
	document.getElementById("profitLineDiv").style.position = "absolute";

	$(document).ready(function(){
		$(window).scroll(function(){
			fixProfitLineBtnPosition();
		});
	});

	function fixProfitLineBtnPosition() {
	   $("#profitLineDiv").css("position", "absolute");
	   $("#profitLineDiv").css("top", $(window).scrollTop()+189);
	   $("#profitLineDiv").css("left", 0);
	}
}