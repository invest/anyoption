﻿var phoneString = '';
var emailString = '';
function validateErrorMessages(){
	var span_phone_good = document.getElementById('mobile_good_pic');
    var span_phone_bad = document.getElementById('mobile_bad_pic');
    var span_email_good = document.getElementById('email_good_pic');
    var span_email_bad = document.getElementById('email_bad_pic');
    span_phone_good.style.display = "none";
    span_phone_bad.style.display = "none";
    span_email_good.style.display = "none";
    span_email_bad.style.display = "none";
    var errMsgPhone = document.getElementById('passwordForm:phoneError');
	var errMsgEmail = document.getElementById('passwordForm:emailError');
	var errMsgPhoneCode = document.getElementById('passwordForm:phoneErrorCode');
	var errMsgPhonePrefix = document.getElementById('passwordForm:phoneErrorPrefix');
	var phInput = document.getElementById('passwordForm:mobilePhone');
	var phCodeInput = document.getElementById('passwordForm:mobilePhoneCode');
	var emInput = document.getElementById('passwordForm:email');
	var phPrefixInput = document.getElementById('passwordForm:mobilePhonePref');
	if(errMsgPhone.innerHTML.length > 0){
        span_phone_bad.style.display = "inline-block";
        span_phone_bad.style.verticalAlign = "middle";
        phInput.style.borderWidth = "1px";
        if(skinId !== 1){
        	phInput.style.borderColor = "#eb9343";
        }else{
        	phInput.style.borderColor = "#de4343";
        }
       if(emInput.value.length == 0 && errMsgEmail.innerHTML.length == 0 && phInput.value.length == 0){
    	   errMsgEmail.style.display = "inline-block";
    	   errMsgEmail.className = 'error_messages';
    	   errMsgEmail.innerHTML = errMsgPhone.innerHTML;
       } 
		}
	if(errMsgEmail.innerHTML.length > 0){
        span_email_bad.style.display = "inline-block";
        span_email_bad.style.verticalAlign = "middle";
        emInput.style.borderWidth = "1px";
        if(skinId !== 1){
        	emInput.style.borderColor = "#eb9343";
        }else{
        	emInput.style.borderColor = "#de4343";
        }
	}
	if(errMsgPhoneCode != null && errMsgPhoneCode.innerHTML.length > 0){
		span_phone_bad.style.display = "inline-block";
        span_phone_bad.style.verticalAlign = "middle";
        phCodeInput.style.borderWidth = "1px";
        if(skinId !== 1){
        	phCodeInput.style.borderColor = "#eb9343";
        }else{
        	phCodeInput.style.borderColor = "#de4343";
        }
	}
	if(errMsgPhonePrefix != null && errMsgPhonePrefix.innerHTML.length > 0){
		span_phone_bad.style.display = "inline-block";
        span_phone_bad.style.verticalAlign = "middle";
        phPrefixInput.style.borderWidth = "1px";
        if(skinId !== 1){
        	phPrefixInput.style.borderColor = "#eb9343";
        }else{
        	phPrefixInput.style.borderColor = "#de4343";
        }
	}
	if(phInput.value.length > 0 && errMsgPhone.innerHTML.length == 0 && phInput.value != phoneString){
        span_phone_good.style.display = "inline-block";
        span_phone_good.style.verticalAlign = "middle";
		}
	if(emInput.value.length > 0 && errMsgEmail.innerHTML.length == 0 && emInput.value != emailString){
	    span_email_good.style.display = "inline-block";
		span_email_good.style.verticalAlign = "middle";
	}	
}
function setOnfocusToControls(){
	document.getElementById('passwordForm:mobilePhone').setAttribute('onfocus','clearDefaultValueOnFocus(id)');
	document.getElementById('passwordForm:email').setAttribute('onfocus','clearDefaultValueOnFocus(id)');
}        
function clearDefaultValueOnFocus(id){
	var control= document.getElementById(id);
	if(control.value == emailString){
		control.value = "";
	}
	if(control.value == phoneString){
		control.value = "";
	}
}	
function setOnBlurToControls(){
		document.getElementById('passwordForm:mobilePhone').setAttribute('onblur','fillDefaultValueOnBlur(id)');
		document.getElementById('passwordForm:email').setAttribute('onblur','fillDefaultValueOnBlur(id)');
}
function fillDefaultValueOnBlur(id){
		var control = document.getElementById(id);
		if(control.value == "" && control.id == 'passwordForm:email'){
			control.value = emailString;
		}
		if(control.value == "" && control.id == 'passwordForm:mobilePhone'){
			control.value = phoneString;
		}
}		
function getDefaultsOnLoad(){
	     if (phoneString == ''){
		     phoneString = document.getElementById('passwordForm:mobilePhone').value;
	     }
	     if (emailString == ''){
		     emailString = document.getElementById('passwordForm:email').value		
	     }
}