// history charts
	var alignC = 'trtr';
	if (skinId == 4) {   // arabic skin
		alignC = 'tltl';
	}
	var HINTS_CHART = {
		'follow'     : false,
		'smart'      : false,
		'margin'     : 0, // 10
		'gap'        : 0, // 20
		'align'      : alignC,
		'css'        : '',
		'show_delay' : 10,
		'hide_delay' : 10,
		'z-index'    : 100,
		'IEfix'      : false,
		'IEtrans'    : ['blendTrans(DURATION=.3)', 'blendTrans(DURATION=.3)'],
		'opacity'    : 80
	};

	var chartItems = [
		'<div id="historyChartDiv"></div>'
	];


	var chartHint = new THints(chartItems, HINTS_CHART);

	function showHistoryChart(link) {

		var item = link;
		for (var i = 0; i < 10; i++) {
	    	item = item.parentNode;
		}
		var oppState = getLSValue(getOppTRSF(item, 1, 5, null, 5));//getOppValue(td.parentNode, 17);
		if (oppState == OPPORTUNITY_STATE_CREATED || oppState >= OPPORTUNITY_STATE_CLOSING_1_MIN) {
			return;
		}
		var marketId = getLSValue(getOppTRSF(item, 1, 5, null, 8)); //getOppValue(td.parentNode, 16);
		log("show marketId: " + marketId);
		var tsd = document.getElementById("historyChartDiv");
		tsd.innerHTML = '<img src="../charts/' + marketId + '_' + userLanguage + '" border="1" width="318" height="160" />';
		chartHint.show(0, link);
	}

	function hideHistoryChart(link) {
		var item = link;
		for (var i = 0; i < 10; i++) {
	    	item = item.parentNode;
		}
		var oppState = getLSValue(getOppTRSF(item, 1, 5, null, 5));
		if (oppState == OPPORTUNITY_STATE_CREATED || oppState >= OPPORTUNITY_STATE_CLOSING_1_MIN) {
			return;
		}
		var marketId = getLSValue(getOppTRSF(item, 1, 5, null, 8));
		log("hide marketId: " + marketId);
		chartHint.hide(0, link);
	}