
		var rndUpdtTimer = setInterval ( "updateLevelRandomly()", 2000 );

		//loop over all the box and change the box and slip level randomly
		function updateLevelRandomly() {
			setOppLevelRandomChange(getRandom());
		}

		function setOppLevelRandomChange(random) {
			var div1 = document.getElementById('oilLevelReal'); // the real level to show
			var div2 = document.getElementById('oilLevel');
			div2.innerHTML = addCommas(addRandomValue(div1.innerHTML, parseFloat(random)), div1.innerHTML);
		}

		//add random value to the last level
		function addRandomValue(str, value) {
				//delete commas ","
				var iStr = str;
				var index = iStr.indexOf(",");
				while (index != -1) {
					var start = iStr.substring(0, index);
					var end = iStr.substring(index + 1);
					iStr = start + end;
					index = iStr.indexOf(",");
				}
				//count digits after zero before adding random value
				iStr += '';
				var c2Length = 0;
				var x = iStr.split('.');
				var x2 = x[1];
				var x2Length = x2.length;
				//adding the random value
				var result = parseFloat(iStr) + parseFloat(iStr) * parseFloat(value); // Tony - the random value is a fraction not absolute value

				//add missing zero
				result = result.toFixed(x2Length);
				return result;
		}

		// add the commas and round the number
		//parm nStr: the new level
		//parm oldLevel: the old level to comper the round format
		function addCommas(nStr, oldLevel) {
			nStr += '';
			x = nStr.split('.');
			x1 = x[0];
			x2 = x.length > 1 ? '.' + x[1] : '';
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(x1)) {
				x1 = x1.replace(rgx, '$1' + ',' + '$2');
			}
			return x1 + x2;
		}

		//the random change to the level when user not login
		//parm row: the row of the opp
		function getRandom(){
			var rnd = Math.random();
			var rndFloor = -0.0001;
			var rndCeiling = 0.0001;
			var fnRnd = rnd * (rndCeiling - (rndFloor)) + (rndFloor);
			return fnRnd;
		}

		//update the sleep with the random level every change
		//parm: oppId: the opp id to copy
		//parm: oppLevel the new level to write in the sleep

		//set opp level and 15 min level and update the slip
		//parm opp: the row of the opp in the table
		//parm value: the value for the new level
		function setOppLevelAnd15MinLevel(opp, value) {

			var div1 = document.getElementById('oilLevel'); // the real level to show

			div1.innerHTML = value;
		}

		//copy real level from hidden TD to shown TD
		//useing in the timer every 15 min


		function replaceMarket() {
			var marketId = document.getElementById('marketId').innerHTML;
			var url = "ajax.jsf?replaceMarket=" + marketId;
		    var ajax = new AJAXInteraction(url, replaceMarketCallback);
			ajax.doGet();
		}

		function replaceMarketCallback(responseXML) {
	 		var ans = responseXML.getElementsByTagName("res")[0].firstChild.nodeValue;
	 	  	var results = ans.split("|");  // level, marketId, marketName, imagePath
			/*alert(results[0]);
			alert(results[1]);
			alert(results[2]);
			alert(results[3]);*/
			document.getElementById('oilLevel').innerHTML = results[0];
			document.getElementById('oilLevelReal').innerHTML = results[0];
			document.getElementById('marketName').innerHTML = results[2];
			document.getElementById('marketId').innerHTML = results[1];
			document.getElementById('marketText').src = results[3];
		}