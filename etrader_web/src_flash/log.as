﻿var logging = false;
if (pClassId == 0 && pCalcGame == "false") {
	logging = true;
}

logTextArea._x = -1000;
logTextArea.html = false;
var lineSeparator:String = newline;
// logTextArea.html = true;
// var lineSeparator:String = "<br>";
if (!logging) {
	btnShowHideLog._x = -1000;
	bntLogClear._visible = false;
}

btnShowHideLog.onPress = function() {
	if (logging && logTextArea._x == -1000) {
		logTextArea._x = 0;
	} else {
		logTextArea._x = -1000;
	}
};

function writeToLog(msg:String):Void {
	if (logging) {
		logTextArea.text = logTextArea.text + new Date() + " " + msg + lineSeparator;
	}
}

function logHttpStatus(httpStatus:Number):Void {
	writeToLog("HTTP Status: " + httpStatus);
}

function logResponseBody(src:String):Void {
	writeToLog("Server Response: " + src);
	if (src == undefined) {
		this.onLoad(false);
	} else {
		this.parseXML(src);
		this.loaded = true;
		this.onLoad(true);
	}
}

bntLogClear.onPress = function() {
	if (logging) {
		logTextArea.text = "";
	}
};