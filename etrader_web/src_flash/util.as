﻿// Calculate period length in seconds.
function calcPeriodInSec(periodStart:Date, periodEnd:Date):Number {
	return Math.round((periodEnd.getTime() - periodStart.getTime()) / 1000);
};

function calcPeriodInMin(periodStart:Date, periodEnd:Date):Number {
	return Math.round(calcPeriodInSec(periodStart, periodEnd) / 60);
};

function addDay(time:Date, d:Number):Void {
	time.setDate(time.getDate() + d);
};

function addHours(time:Date, h:Number):Void {
	time.setHours(time.getHours() + h);
//	var newH:Number = time.getHours() + h;
//	if (h > 0) {
//		time.setHours(newH % 24);
//		if (newH > 23) {
//			addDay(time, Math.floor(newH / 24));
//		}
//	}
//	if (h < 0) {
//		if (newH >= 0) {
//			time.setHours(newH);
//		} else {
//			time.setHours(24 + newH % 24);
//			addDay(time, Math.floor(newH / 24));
//		}
//	}
};

function addMinutes(time:Date, min:Number):Void {
	time.setMinutes(time.getMinutes() + min);
//	var newMin:Number = time.getMinutes() + min;
//	if (min > 0) {
//		time.setMinutes(newMin % 60);
//		if (newMin > 59) {
//			addHours(time, Math.floor(newMin / 60));
//		}
//	}
//	if (min < 0) {
//		if (newMin >= 0) {
//			time.setMinutes(newMin);
//		} else {
//			time.setMinutes(60 + newMin % 60);
//			addHours(time, Math.floor(newMin / 60));
//		}
//	}
};

function addSeconds(time:Date, sec:Number):Void {
	time.setSeconds(time.getSeconds() + sec);
//	var newSec:Number = time.getSeconds() + sec;
//	if (sec > 0) {
//		time.setSeconds(newSec % 60);
//		if (newSec > 59) {
//			addMinutes(time, Math.floor(newSec / 60));
//		}
//	}
//	if (sec < 0) {
//		if (newSec >= 0) {
//			time.setSeconds(newSec);
//		} else {
//			time.setSeconds(60 + newSec % 60);
//			addMinutes(time, Math.floor(newSec / 60));
//		}
//	}
};

function fixTimeZone(d:Date):Void {
	var tzDiff:Number = d.getTimezoneOffset() - timezoneOffset;
	addMinutes(d, tzDiff);
};

function fixTime(d:Date):Void {
	addSeconds(d, serverTimeOffset);
	fixTimeZone(d);
};

function padToTwoDigit(n:Number):String {
	if (n < 10) {
		return "0" + n;
	}
	return "" + n;
};

// Format a Date to hh:mm:ss or hh:mm.
function formatToString(d:Date, addSeconds:Boolean):String {
	var str:String = padToTwoDigit(d.getHours()) + ":" + padToTwoDigit(d.getMinutes());
	if (addSeconds) {
		str = str + ":" + padToTwoDigit(d.getSeconds())
	}
	return str;
};

function extractDecimalPoint(level:String):Number {
	if (level.indexOf(".") != -1) {
		return level.length - 1 - level.indexOf(".");
	} else if (level.indexOf(",") != -1) {
		return level.length - 1 - level.indexOf(",");
	}
	return 0;
};

//function correctFloatingPointError(number:Number, precision:int = 5):Number {
function correctFloatingPointError(number:Number):Number {
	//default returns (10000 * number) / 10000
	//should correct very small floating point errors

	var correction:Number = Math.pow(10, 5);
	return Math.round(correction * number) / correction;
};

function addThousandsSeparators(s:String):String {
	if (s.length <= 3) {
		return s;
	}
	var i:Number = s.length % 3;
	var tmp:String = "";
	if (i != 0) {
		tmp = s.substring(0, i);
	}
	do {
		if (tmp.length > 0) {
			tmp = tmp + ",";
		}
		if (i + 3 < s.length) {
			tmp = tmp + s.substring(i, i + 3);
		} else {
			tmp = tmp + s.substring(i);
		}
		i = i + 3;
	} while (i < s.length);
	return tmp;
};

// Format number with pattern #,##0.00 (the number of digits after the decimal point is specified).
function formatDecimalPoint(val:Number, decPoint:Number):String {
	trace("formatDecimalPoint - val: " + val + " decPoint: " + decPoint);
	var tmp:Number = correctFloatingPointError(val);
	var tmpTxt:String = "" + tmp;
//	trace("formatDecimalPoint - val: " + tmp + " decPoint: " + decPoint);

	// here we handle only positive values ;)
	var intPart:Number = Math.floor(tmp);
	var fractPart:Number = tmp - intPart;
	var i:Number;
	if (fractPart > 0) {
		var fractPartDecimal:Number = extractDecimalPoint(tmpTxt);
		trace("intPart: " + intPart + " fractPart: " + fractPart + " fractPartDecimal: " + fractPartDecimal);
		tmpTxt = tmpTxt.substring(tmpTxt.indexOf("."));
		if (fractPartDecimal.valueOf() == decPoint.valueOf()) { // we are ok
			return addThousandsSeparators("" + intPart) + tmpTxt;
		}
		if (fractPartDecimal.valueOf() < decPoint.valueOf()) { // pad with 0s
			var padding:String = "";
			for (i = fractPartDecimal; i < decPoint; i++) {
				padding = padding + "0";
			}
			return addThousandsSeparators("" + intPart) + tmpTxt + padding;
		}
		return formatDecimalPoint(Math.round(tmp * Math.pow(10, decPoint)) / Math.pow(10, decPoint), decPoint);
	} else if (decPoint > 0) {
		var fractPartStr:String = ".";
		for (i = 0; i < decPoint; i++) {
			fractPartStr = fractPartStr + "0";
		}
		return addThousandsSeparators("" + intPart) + fractPartStr;
	}
	return addThousandsSeparators(tmpTxt);
};

function formatAmount(val:Number):String {
	return formatAmountRound(val, false);
};

function formatAmountRound(val:Number, round:Boolean):String {
	var decFormatted:String = formatDecimalPoint(val, currencyDecimalPoint);
	if (round && currencyDecimalPoint > 0) {
		var cents:String = decFormatted.substring(decFormatted.length - currencyDecimalPoint, decFormatted.length);
		if (onlyZeroes(cents)) {
			decFormatted = decFormatted.substring(0, decFormatted.length - currencyDecimalPoint - 1);
		}
	}
	if (currencyLeftSymbol) {
		return currencySign + decFormatted;
	} else {
		return decFormatted + currencySign;
	}
};

function onlyZeroes(str:String):Boolean {
	var i:Number = 0;
	for (i = 0; i < str.length; i++) {
		if (str.charAt(i) != '0') {
			return false;
		}
	}
	return true;
}

// parse string in format MM/DD/YYYY HH24:MI
function parseDate(s:String):Date {
	s = s.replace(" ", "/");
	s = s.replace(":", "/");
	var arr:Array = s.split("/");
	return new Date(Date.UTC(arr[2], arr[0], arr[1], arr[3], arr[4]));
};


function getRandomParam():String {
	return  "" + new Date().getTime() + Math.round(Math.random() * 1000)
};