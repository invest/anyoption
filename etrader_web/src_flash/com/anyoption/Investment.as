﻿import mx.controls.Label;

class com.anyoption.Investment {
	private var id:Number;
	private var marketId:Number;
	private var market:String;
	private var oppId:Number;
	private var timeEstClosing:Date;
	private var timeCreated:Date;
	private var levelTxt:String;
	private var level:Number;
	private var amountTxt:String;
	private var amount:Number;
	private var rollUpAmount:Number;
	private var type:Number;
	private var oddsWin:Number;
	private var oddsLose:Number;
	private var levelIcon:MovieClip;
	private var levelIconXPosition:Number;
	
	public function loadFromXMLNode(node:XMLNode) {
		id = node.childNodes[0].childNodes[0].nodeValue;
		market = node.childNodes[1].childNodes[0].nodeValue;
		timeCreated = new Date(new Number(node.childNodes[2].childNodes[0].nodeValue));
		levelTxt = node.childNodes[3].childNodes[0].nodeValue;
		var tmpLvlTxt = node.childNodes[4].childNodes[0].nodeValue;
		tmpLvlTxt = tmpLvlTxt.replace(",", "."); // Tony: if the decimal point is "," make it ".". That's for my local env
		level = new Number(tmpLvlTxt);
		amountTxt = node.childNodes[5].childNodes[0].nodeValue;
		amount = new Number(amountTxt);
		amount = amount / 100;
		type = new Number(node.childNodes[6].childNodes[0].nodeValue);
		oddsWin = new Number(node.childNodes[7].childNodes[0].nodeValue);
		oddsLose = new Number(node.childNodes[8].childNodes[0].nodeValue);
		amountTxt = node.childNodes[9].childNodes[0].nodeValue;
		rollUpAmount = new Number(node.childNodes[10].childNodes[0].nodeValue);
		rollUpAmount = rollUpAmount / 100;
		marketId = new Number(node.childNodes[11].childNodes[0].nodeValue);
		oppId = new Number(node.childNodes[12].childNodes[0].nodeValue);
		timeEstClosing = new Date(new Number(node.childNodes[13].childNodes[0].nodeValue));
		_root.writeToLog("Created Investments from node - id: " + id +
			  " marketId: " + marketId +
			  " market: " + market +
			  " oppId: " + oppId +
			  " timeEstClosing: " + timeEstClosing +
			  " timeCreated: " + timeCreated +
			  " levelTxt: " + levelTxt +
			  " level: " + level +
			  " amountTxt: " + amountTxt +
			  " amount: " + amount +
			  " type: " + type +
			  " oddsWin: " + oddsWin +
			  " oddsLose: " + oddsLose +
			  " rollUpAmount: " + rollUpAmount);
	}
	
	public function getId():Number {
		return id;
	}
	
	public function setId(id:Number):Void {
		this.id = id;
	}
	
	public function getMarketId():Number {
		return marketId;
	}
	
	public function setMarketId(marketId:Number):Void {
		this.marketId = marketId;
	}
	
	public function getMarket():String {
		return market;
	}
	
	public function setMarket(market:String):Void {
		this.market = market;
	}
	
	public function getOppId():Number {
		return oppId;
	}
	
	public function setOppId(oppId:Number):Void {
		this.oppId = oppId;
	}
	
	public function getTimeEstClosing():Date {
		return timeEstClosing;
	}
	
	public function setTimeEstClosing(timeEstClosing:Date):Void {
		this.timeEstClosing = timeEstClosing;
	}
	
	public function getTimeCreated():Date {
		return timeCreated;
	}
	
	public function setTimeCreated(timeCreated:Date):Void {
		this.timeCreated = timeCreated;
	}
	
	public function getLevelTxt():String {
		return levelTxt;
	}
	
	public function setLevelTxt(levelTxt:String):Void {
		this.levelTxt = levelTxt;
		level = new Number(levelTxt);
	}
	
	public function getLevel():Number {
		return level;
	}
	
	public function setLevel(level:Number):Void {
		this.level = level;
	}
	
	public function getAmountTxt():String {
		return amountTxt;
	}
	
	public function setAmountTxt(amountTxt:String):Void {
		this.amountTxt = amountTxt;
	}
	
	public function getAmount():Number {
		return amount;
	}
	
	public function setAmount(amount:Number):Void {
		this.amount = amount;
	}
	
	public function getType():Number {
		return type;
	}
	
	public function setType(type:Number):Void {
		this.type = type;
	}
	
	public function getOddsWin():Number {
		return oddsWin;
	}
	
	public function setOddsWin(oddsWin:Number):Void {
		this.oddsWin = oddsWin;
	}
	
	public function getOddsLose():Number {
		return oddsLose;
	}
	
	public function setOddsLose(oddsLose:Number):Void {
		this.oddsLose = oddsLose;
	}
	
	public function setLevelIcon(levelIcon:MovieClip):Void {
		this.levelIcon = levelIcon;
	}
	
	public function getLevelIcon():MovieClip {
		return levelIcon;
	}
	
	public function setLevelIconXPosition(levelIconXPosition:Number):Void {
		this.levelIconXPosition = levelIconXPosition
	}
	
	public function getLevelIconXPosition():Number {
		return levelIconXPosition;
	}
	
	public function setRollUpAmount(rollUpAmount:Number):Void {
		this.rollUpAmount = rollUpAmount;
	}
	
	public function getRollUpAmount():Number {
		return rollUpAmount;
	}
	
	public function getReturnAt(currentLevel:Number):Number {
		if ((type == 1 && currentLevel > level) ||
				(type == 2 && currentLevel < level)) { // win
			return (amount - rollUpAmount) * (1 + oddsWin);
		} else if ((type == 1 && currentLevel < level) ||
					 (type == 2 && currentLevel > level)) { // lose
			return (amount - rollUpAmount) * (1 - oddsLose);
		} else { // tie left
			return (amount - rollUpAmount);
		}
	}
}