﻿import mx.core.UIComponent

class com.anyoption.ReturnRefundCellRenderer extends UIComponent {
	var returnLabel:MovieClip;
	var refundLabel:MovieClip;
	
	function ReturnRefundCellRenderer() {
		// do nothing
	}

	function createChildren():Void {
		returnLabel = createObject("Label", "lblReturn", 1);
		refundLabel = createObject("Label", "lblRefund", 2);
		size();
	}

	function size():Void {
		returnLabel._x = 0;
		returnLabel._y = 0;
		returnLabel._width = __width / 2;
		returnLabel._height = 20;
		
		refundLabel._x = __width / 2;
		refundLabel._y = 0;
		refundLabel._width = __width / 2;
		refundLabel._height = 20;
	}

	function setValue(str:String, item:Object, sel:Boolean):Void {
		returnLabel._visible = (item != undefined);
		refundLabel._visible = (item != undefined);
		if (item != undefined) {
			if (item.data == "") {
				returnLabel.setStyle("color", _root.COLOR_RETURN_REFUND_HEADER);
				returnLabel.setStyle("fontWeight", "bold");
				refundLabel.setStyle("color", _root.COLOR_RETURN_REFUND_HEADER);
				refundLabel.setStyle("fontWeight", "bold");
			} else {
				returnLabel.setStyle("color", _root.COLOR_RETURN_REFUND_TEXT);
				returnLabel.setStyle("fontWeight", "none");
				refundLabel.setStyle("color", _root.COLOR_RETURN_REFUND_TEXT);
				refundLabel.setStyle("fontWeight", "none");
			}
			returnLabel.setStyle("textAlign", "center");
			refundLabel.setStyle("textAlign", "center");
			returnLabel.text = item.returnTxt;
			refundLabel.text = item.refundTxt;
		}
	}
}