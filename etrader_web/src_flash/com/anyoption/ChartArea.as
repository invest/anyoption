﻿class com.anyoption.ChartArea {
	var bottomLevel:Number;
	var topLevel:Number;
	var profitable:Boolean;
	var color:Number;
	var rect:MovieClip;
	var index:Number;
	var indexTotal:Number;
	var topLabelVisible:Boolean;
	var noInvestments:Boolean;
	
	public function ChartArea(paintArea:MovieClip, bottomLevel:Number, topLevel:Number, investments:Array, index:Number, indexTotal:Number) {
		this.bottomLevel = bottomLevel;
		this.topLevel = topLevel;
		this.profitable = isProfitable(investments);
		this.noInvestments = indexTotal == 0;
		this.index = index;
		this.indexTotal = indexTotal;
		rect = paintArea.createEmptyMovieClip("chartArea_" + index, 1 + index); // paintArea.getNextHighestDepth()
		topLabelVisible = true;
//		_root.writeToLog("ChartArea - bottomLevel: " + bottomLevel + " topLevel: " + topLevel + " index: " + index + " profitable: " + profitable);
	}
	
	public function paint(x:Number, y:Number, width:Number, height:Number, yMinVal:Number, yMaxVal:Number, currentLevel:Number, topLabel:TextField, selectedInList:Boolean) {
		var spread:Number = yMaxVal - yMinVal;
		rect._x = x;
		rect._y = y + height * (yMaxVal - topLevel) / spread;
//		if (index == 0) {
		if (!_root.optionPlus || index < indexTotal) {
			if (!_root.optionPlus && _root.skinId == 1 && index == indexTotal) {
				topLabel._y = rect._y;
			} else {
				topLabel._y = rect._y - _root.LABEL_HEIGHT / 2;
			}
		}
//		} else if (index == indexTotal) {
//			topLabel._y = rect._y;
//		} else {
//			topLabel._y = rect._y - _root.LABEL_HEIGHT / 2;
//			if (topLabel._y < 0) {
//				topLabel._y += _root.LABEL_HEIGHT;
//			}
//		}
		if (topLabelVisible) {
			topLabel.text = _root.formatDecimalPoint(topLevel, _root.decimalPoint);
		}
//		_root.writeToLog("area: " + rect._name + " x: " + rect._x + " y: " + rect._y + " width: " + width + " height: " + (height * (topLevel - bottomLevel) / spread));
		rect.clear();
		var rectHeight:Number = height * (topLevel - bottomLevel) / spread;
		drawRectangle(rect, width, rectHeight, getColor(currentLevel), 100);
		if (bottomLevel < currentLevel && currentLevel < topLevel && !noInvestments) {
			_root.gradient._y = _root.mcPaintArea._y + rect._y;
			_root.gradient._height = rectHeight;
		}
		if (index != indexTotal) {
			if (selectedInList) {
				// rect.lineStyle(2, _root.LINE_COLOR);
				_root.zoom_mc._x = _root.mcPaintArea._x + (_root.optionPlus ? 0 : _root.LABEL_WIDTH);
				_root.zoom_mc._y = _root.mcPaintArea._y + rect._y + 1;
			} else {
				rect.lineStyle(1, _root.LINE_COLOR);
				rect.dashTo(0, 0, width, 0, 3, 2);
			}
		}
	}
	
	private function isProfitable(investments:Array):Boolean {
		var totalAmount:Number = 0;
		var totalProfit:Number = 0;
		var checkLevel:Number = bottomLevel + (topLevel - bottomLevel) / 2;
		
		var i:Number;
		for (i = 0; i < investments.length; i++) {
			if (investments[i].getMarketId().valueOf() == _root.pMarketId.valueOf()) {
				totalAmount += investments[i].getAmount();
				if ((checkLevel < investments[i].getLevel() && investments[i].getType() == 1) ||
						(investments[i].getLevel() < checkLevel && investments[i].getType() == 2)) {
					totalProfit += investments[i].getAmount() * (1 - investments[i].getOddsLose());
				} else {
					totalProfit += investments[i].getAmount() * (1 + investments[i].getOddsWin());
				}
			}
		}
//		trace("isProfitable - checkLevel: " + checkLevel + " totalAmount: " + totalAmount + " totalProfit: " + totalProfit);
		return totalProfit > totalAmount;
	}

	public function getColor(crrLvl:Number):Number {
		if (noInvestments) {
			return _root.CLR_NO_INVESTMENTS;
		}
		var current:Boolean = false;
		if (bottomLevel < crrLvl && crrLvl < topLevel) {
			current = true;
		}
		if (profitable) {
			if (current) {
				return _root.CLR_WIN_CRR;
			} else {
				return _root.CLR_WIN;
			}
		} else {
			if (current) {
				return _root.CLR_LOSE_CRR;
			} else {
				return _root.CLR_LOSE;
			}
		}
	}
	
	public function drawRectangle(target:MovieClip, boxWidth:Number, boxHeight:Number, fillColor:Number, fillAlpha:Number):Void {
//		trace("drawRectangle - target: " + target._name + " _x: " + target._x + " _y: " + target._y + " boxWidth: " + boxWidth + " boxHeight: " + boxHeight);
		with (target) {
			beginFill(fillColor, fillAlpha);
			moveTo(0, 0);
			lineTo(boxWidth, 0);
			lineTo(boxWidth, boxHeight);
			lineTo(0, boxHeight);
			lineTo(0, 0);
			endFill();
		}
	}
	
	public function drawRectangleXY(target:MovieClip, x:Number, y:Number, boxWidth:Number, boxHeight:Number, fillColor:Number, fillAlpha:Number):Void {
		with (target) {
			beginFill(fillColor, fillAlpha);
			moveTo(x, y);
			lineTo(x + boxWidth, y);
			lineTo(x + boxWidth, y + boxHeight);
			lineTo(x, y + boxHeight);
			lineTo(x, y);
			endFill();
		}
	}
	
	public function setBottomLevel(bottomLevel:Number):Void {
		this.bottomLevel = bottomLevel;
	}
	
	public function getBottomLevel():Number {
		return bottomLevel;
	}
	
	public function setTopLevel(topLevel:Number):Void {
		this.topLevel = topLevel;
	}
	
	public function getTopLevel():Number {
		return topLevel;
	}
	
	public function isProf():Boolean {
		return profitable;
	}
	
	public function cleanup():Void {
		rect.removeMovieClip();
	}
	
	public function getTopLabelVisible():Boolean {
		return topLabelVisible;
	}
	
	public function setTopLabelVisible(topLabelVisible:Boolean):Void {
		this.topLabelVisible = topLabelVisible;
	}
}