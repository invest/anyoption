﻿class com.anyoption.ReturnRefundPair {
	private var id:Number;
	private var returnValue:Number;
	private var refundValue:Number;
	
	public function setId(id:Number):Void {
		this.id = id;
	}
	
	public function getId():Number {
		return id;
	}
	
	public function setReturnValue(returnValue:Number):Void {
		this.returnValue = returnValue;
	}
	
	public function getReturnValue():Number {
		return returnValue;
	}
	
	public function setRefundValue(refundValue:Number):Void {
		this.refundValue = refundValue;
	}
	
	public function getRefundValue():Number {
		return refundValue;
	}
}