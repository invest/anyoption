﻿import mx.core.UIComponent

class com.anyoption.StopLossCellRenderer extends UIComponent {
	var button:MovieClip;
	var amount:MovieClip;
	var market:MovieClip;
	var time:MovieClip;
	var icon:MovieClip;
	var level:MovieClip;
	var separator:MovieClip;
	var getCellIndex:Function;
	var countDown:MovieClip;
	
	function StopLossCellRenderer() {
		// do nothing
	}

	function createChildren():Void {
		button = createObject("GetPriceButton", "button", 1);
		button.onPress = showGetPrice;
		amount = createObject("Label", "label", 2);
		market = createObject("Label", "market", 3);
		time = createObject("Label", "time", 4);
		icon = createObject("callIcon", "icon", 5);
		level = createObject("Label", "level", 6);
		separator = createObject("InvestmentsListSeparator", "separator", 7);
		countDown = createObject("ListItemProgress", "countDown", 8);
		size();
	}

	function size():Void {
		button._x = (__width / 3 - button._width) / 2 + (_root.rtl ? 0 : __width * 2 / 3);
		if(_root.skinId == 1){
			button._y = (30 - button._height) / 2;
		}
		else{
			button._y = (40 - button._height) / 2;
		}
		
		amount._x = __width / 3 - 3;
		amount._y = 0;
		amount.setSize(__width / 3 + 6, 40 / 2);
		time._x = amount._x;
		if(_root.skinId == 1){
			time._y = 13;
		}
		else{
			time._y = 20;
		}
		time.setSize(__width / 3, 20);
		
		market._x = __width * (_root.rtl ? 2 : 0) / 3;
		market._y = 0;
		if (_root.skinId == 1){
			market.setStyle('color',0x282828);
			market.fontFamily = "Tahoma";
			amount.setStyle('color',0x282828);
			amount.fontFamily = "Tahoma";
			time.setStyle('color',0x282828);
			time.fontFamily = "Tahoma";
			level.setStyle('color',0x282828);
			level.fontFamily = "Tahoma";
		}
		market.setSize(__width / 3, 20);
		icon._x = (_root.rtl ? __width * 2 / 3 : __width / 3 - 10);
		if(_root.skinId == 1){
			icon._y = 18;
		}else{
			icon._y = 25;
		}
		level._x = (_root.rtl ? icon._x + 10 : 0);
		if(_root.skinId == 1){
			level._y = 13;
		}else{
			level._y = 20;
		}
		level.setSize(__width / 3 - 10, 20);
		
		separator._x = -2;
		separator._y = 39;
		
		 // 24 is the width of the animation. Can be avoided if animation has fixed width on all frames
		countDown._x = (__width / 3 - countDown._width) / 2 + (_root.rtl ? 0 : __width * 2 / 3);
		countDown._y = (40 - countDown._height) / 2;
	}

	function setValue(str:String, item:Object, sel:Boolean):Void {
		var inv = _root.chart.getInvestments()[getCellIndex().itemIndex];
		var isSold:Boolean = inv.getId() == _root.soldInvestmentId;
		var ct:Date = new Date();
		_root.chart.fixTime(ct);
		var timeSinceCreated:Number = ct.getTime() - inv.getTimeCreated().getTime();
		var showCountDown:Boolean = timeSinceCreated < 6000;
//		_root.writeToLog("isSold: " + isSold + " _root.soldInvestmentId: " + _root.soldInvestmentId +
//				" getCellIndex().itemIndex: " + getCellIndex().itemIndex +
//				" _root.chart.getInvestments()[getCellIndex().itemIndex].getId(): " + _root.chart.getInvestments()[getCellIndex().itemIndex].getId());
		button._visible = (item != undefined) && (_root.oppState == 2 || _root.oppState == 3) && !isSold && !showCountDown;
		if (_root.shouldLoadTexts) {
			button.getQuote.setNewTextFormat(button.getQuote.getTextFormat(1, 2));
			button.getQuote.text = _root.textOPGetQuote;
		}
		amount._visible = (item != undefined);
		market._visible = (item != undefined);
		time._visible = (item != undefined);
		icon._visible = (item != undefined);
		level._visible = (item != undefined);
		separator._visible = (item != undefined);
		countDown._visible = (item != undefined) && showCountDown;
		if (countDown._visible) {
			startCountDown(countDown, timeSinceCreated);
		} else {
			countDown.onEnterFrame = null;
		}
		if (item != undefined) {
			time.setStyle("textAlign", "center");
			amount.setStyle("textAlign", "center");
			market.setStyle("textAlign", (_root.rtl ? "right" : "left"));
			level.setStyle("textAlign", (_root.rtl ? "right" : "left"));
			amount.text = item.amount;
			market.text = item.market;
			time.text = item.time;
			level.text = item.level;
			
			if (item.data == "call") {
				var x = icon._x;
				var y = icon._y;
				destroyObject("icon");
				icon = createObject("callIcon", "icon", 5);
				icon._x = x;
				icon._y = y;
			}
			if (item.data == "put") {
				var x = icon._x;
				var y = icon._y;
				destroyObject("icon");
				icon = createObject("putIcon", "icon", 5);
				icon._x = x;
				icon._y = y;
			}
			
			item.sellButton = button;
			item.lblMarket = market;
		}
	}
	
	function showGetPrice() {
		if (_parent.listOwner.selectedIndex != _parent.getCellIndex().itemIndex || _root.getPricePopup._x < 0) {
			_parent.listOwner.selectedIndex = _parent.getCellIndex().itemIndex;
			_root.getOptionPlusSellPrice();
		}
	}
	
	function startCountDown(cntDown, timeSinceCreated):Void {
		_root.writeToLog("StopLossCellRenderer.startCountDown: " + timeSinceCreated);
		cntDown.counter = Math.floor((6000 - timeSinceCreated) / 50);
		cntDown.onEnterFrame = function () {
			this.counter -= 1;
			if (this.counter <= 0) {
				_root.writeToLog("StopLossCellRenderer.countDownTicker done index: " + this._parent.getCellIndex().itemIndex);
				this.onEnterFrame = null;
	
				var inv = _root.chart.getInvestments()[this._parent.getCellIndex().itemIndex];
				var isSold:Boolean = inv.getId() == _root.soldInvestmentId;
				this._parent.button._visible = (_root.oppState == 2 || _root.oppState == 3) && !isSold;
				this._visible = false;
			}
		}
	}
}