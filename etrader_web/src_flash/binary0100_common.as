﻿_root.writeToLog("binary0100_common.as");

tfExpiry._visible = false;
mcError._visible = false;

// keep the design
tfCurrentLevel.setNewTextFormat(tfCurrentLevel.getTextFormat(1, 2));
tfBuyLbl.setNewTextFormat(tfBuyLbl.getTextFormat(1, 2));
tfBuyReturn.html = true;
tfBuyMaxProfit.html = true;
tfBuyMaxRisk.html = true;
tfBuyNowLbl.setNewTextFormat(tfBuyNowLbl.getTextFormat(1, 2));
tfSellLbl.setNewTextFormat(tfSellLbl.getTextFormat(1, 2));
tfSellReturn.html = true;
tfSellMaxProfit.html = true;
tfSellMaxRisk.html = true;
tfSellNowLbl.setNewTextFormat(tfSellNowLbl.getTextFormat(1, 2));
tfSellNowLblDisabled.setNewTextFormat(tfSellNowLblDisabled.getTextFormat(1, 2));

_root.writeToLog("tfBuyLbl: " + tfBuyLbl + "txtBuy: " + txtBuy);

// set texts
tfBuyLbl.text = txtBuy;
tfBuyOptionsLbl.text = txtBuyOptions;
tfBuyNowLbl.text = txtBuyNow;
tfSellLbl.text = txtSell;
tfSellOptionsLbl.text = txtBuyOptions;
tfSellNowLbl.text = txtSellNow;
tfSellNowLblDisabled.text = txtSellNow;
tfSellNowLblDisabled._visible = false;
mcITooltipBuy._visible = false;
mcITooltipBuy.tfMsg.text = txtITooltipBuy;
mcITooltipSell._visible = false;
mcITooltipSell.tfMsg.text = txtITooltipSell;

cbOtherEvents.removeAll();
cbOtherEvents.addItem({label:txtOtherEvents, data:1});
cbOtherEvents.selectedIndex = 0;
