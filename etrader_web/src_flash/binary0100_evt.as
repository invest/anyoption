﻿if (opportunityTypeId == OPPORTUNITY_TYPE_ABOVE) {
	_root.mcBgLb._height = _root.mcMidScreen._y - _root.mcBgLb._y;
	_root.mcBgDb._height = 0;
	_root.mcBgLy._y = _root.mcMidScreen._y;
	_root.mcBgLy._height = _root.mcChart._y + _root.mcChart._height - _root.mcMidScreen._y;
	_root.mcBgDy._height = 0;
} else {
	_root.mcBgLy._height = _root.mcMidScreen._y - _root.mcBgLy._y;
	_root.mcBgDy._height = 0;
	_root.mcBgLb._y = _root.mcMidScreen._y;
	_root.mcBgLb._height = _root.mcChart._y + _root.mcChart._height - _root.mcMidScreen._y;
	_root.mcBgDb._height = 0;
}
mcTimeLineOpen._width = 0;
mcTimeLineLast5Min._width = 0;
mcTimeLineWaitingForExpiry._width = 0;
if (currentOppState != OPP_STATE_WAITING_FOR_EXPIRY_1_MIN && currentOppState != OPP_STATE_WAITING_FOR_EXPIRY) {
	mcWaitingForExpiry._visible = false;
}
btnBuyNowDisabled._visible = false;
btnSellNowDisabled._visible = false;
mcBuyOptionsBorder._visible = false;
mcSellOptionsBorder._visible = false;

// set texts
tfStartTimeLbl.text = txtStartTime;
tfLastTradeTimeLbl.text = txtLastTradeTime;
tfExpiryTimeLbl.text = txtExpiryTime;

updateEvent();

// -----------------------------------------------------------------------------------------
// Buy/Sell stuff
// -----------------------------------------------------------------------------------------

btnBuy.onPress = function() {
	submitInvestment(CONTRACT_TYPE_BUY);
};

btnBuyNow.onPress = function() {
	submitInvestment(CONTRACT_TYPE_BUY);
};

btnSell.onPress = function() {
	submitInvestment(CONTRACT_TYPE_SELL);
};

btnSellNow.onPress = function() {
	submitInvestment(CONTRACT_TYPE_SELL);
};

btnBuy.onRollOver = function() {
	mcBuyOptionsBorder._visible = true;
};

btnBuy.onRollOut = function() {
	mcBuyOptionsBorder._visible = false;
};

btnBuyNow.onRollOver = function() {
	mcBuyOptionsBorder._visible = true;
};

btnBuyNow.onRollOut = function() {
	mcBuyOptionsBorder._visible = false;
};

btnSell.onRollOver = function() {
	mcSellOptionsBorder._visible = true;
};

btnSell.onRollOut = function() {
	mcSellOptionsBorder._visible = false;
};

btnSellNow.onRollOver = function() {
	mcSellOptionsBorder._visible = true;
};

btnSellNow.onRollOut = function() {
	mcSellOptionsBorder._visible = false;
};

// -----------------------------------------------------------------------------------------
// Event stuff
// -----------------------------------------------------------------------------------------

btnSelectEvent.onPress = function () {
	if (eventsLoading || submitting) {
		return;
	}
	
	// close the select box
	if (lstSelectEvent._visible) {
		lstSelectEvent._visible = false;
		return;
	}
	
	eventsLoading = true;
	_root.writeToLog("Loading events for select...");
	var eventsXml:XML = new XML();
	eventsXml.ignoreWhite = true;
	eventsXml.onHTTPStatus = logHttpStatus;
//	eventsXml.onData = logResponseBody;
	eventsXml.onLoad = function(success:Boolean) {
		_root.writeToLog("Loading events " + success);
		if (success) {
			lstSelectEvent.removeAll();
			_root.writeToLog("Events count: " + eventsXml.childNodes[0].childNodes.length);
			var i;
			for (i = 0; i < eventsXml.childNodes[0].childNodes.length; i++) {
				var eventId = eventsXml.childNodes[0].childNodes[i].childNodes[0].childNodes[0].nodeValue;
				var marketId = eventsXml.childNodes[0].childNodes[i].childNodes[1].childNodes[0].nodeValue;
				var eventLabel = eventsXml.childNodes[0].childNodes[i].childNodes[2].childNodes[0].nodeValue;
				lstSelectEvent.addItem({label:eventLabel, data:eventId, market:marketId});
				_root.writeToLog("Item data: " + eventId + " label: " + eventLabel + " market: " + marketId);
			}
			lstSelectEvent._visible = true;
		} else {
			_root.writeToLog("Loading events failed " + eventsXml.status);
		}
		eventsLoading = false;
	}
	eventsXml.load(requestsPath + "/jsp/xml/binary0100EventSelect.xml?opportunityId=" + pOpportunityId + "&t=" + getRandomParam());
};

// -----------------------------------------------------------------------------------------
// Other stuff
// -----------------------------------------------------------------------------------------

btnChart.onRollOver = function() {
//	_root.writeToLog("Show chart");
	icnCrrLvl._visible = true;
	chart.setFreeze(false);
	chart.repaint();
};

btnChart.onRollOut = function() {
//	_root.writeToLog("Hide chart");
	icnCrrLvl._visible = false;
	chart.setFreeze(true);
	mcChart.clear();
};

icnBuyInfo.onRollOver = function() {
	_root.writeToLog("i buy over");
	_root.mcITooltipBuy._visible = true;
};

icnBuyInfo.onRollOut = function() {
	_root.writeToLog("i buy out");
	_root.mcITooltipBuy._visible = false;
};

icnSellInfo.onRollOver = function() {
	_root.writeToLog("i sell over");
	_root.mcITooltipSell._visible = true;
};

icnSellInfo.onRollOut = function() {
	_root.writeToLog("i sell out");
	_root.mcITooltipSell._visible = false;
};

// Create Listener Object.
var cbListener:Object = new Object();

// Assign function to Listener Object.
cbListener.change = function(event_obj:Object) {
	_root.writeToLog("Combo changed");
	_root.updateReturnAndProfit();
};

// Add Listener.
cbBuyOptions.addEventListener("change", cbListener);
cbSellOptions.addEventListener("change", cbListener);

// connectLightstreamer();