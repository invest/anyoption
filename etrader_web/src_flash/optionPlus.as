﻿var OPTION_PLUS_FEE_RUB:Number = 10;
var OPTION_PLUS_FEE_YUAN:Number = 1;
var OPTION_PLUS_FEE_WON:Number = 500;

var textOPCommission:String;
var textOPOfferValid:String;
var textOPBuy:String;
var textOPNoThanks:String;
var textOPGetPriceNote:String;
var textOPGetPriceSold:String;
var textOPGetPriceSuccess:String;
var textOPGetPriceError:String;
var textOPSlipConfNote:String;
var textOPGetQuote:String;
var textOPPleaseWait:String;

var optionPlusSellPrice:String = "";
var soldInvestmentId:Number = 0;
var closeSuccessPopupCounter:Number = 0;
var getInvestmentPriceAfterMarketSwitch:Number = undefined;

var LIST_COVER_X = listCover._x;
listCover._x = -1000;
var READ_MORE_BUTTON_X = readMoreButton._x;
readMoreButton._x = -1000;
var READ_MORE_TEXT_X = readmoreText._x;
readmoreText._x = -1000;
var OFFHOURS_MSG_CLICK_X = offhours.offhoursMsgClick._x;

function clearTextsOptionPlus():Void {
	_root.header.headerText.setNewTextFormat(_root.header.headerText.getTextFormat(1, 2));
	_root.header.headerText.text = "";
};

function loadTextsOptionPlus(xmlNode:XMLNode):Void {
	var OPTION_PLUS_TEXTS_START_INDEX:Number = 42;
	// _root.header.headerText.setNewTextFormat(_root.header.headerText.getTextFormat(1, 2));
	_root.header.headerText.text = xmlNode.childNodes[OPTION_PLUS_TEXTS_START_INDEX].childNodes[0].nodeValue;
	textOPCommission = xmlNode.childNodes[OPTION_PLUS_TEXTS_START_INDEX + 1].childNodes[0].nodeValue;
	_root.captionCommission.text = (_root.shouldLoadTexts ? textOPCommission + " " : "") + _root.formatAmount(_root.OPTION_PLUS_FEE);
	textOPBuy = xmlNode.childNodes[OPTION_PLUS_TEXTS_START_INDEX + 2].childNodes[0].nodeValue;
	textOPNoThanks = xmlNode.childNodes[OPTION_PLUS_TEXTS_START_INDEX + 3].childNodes[0].nodeValue;
	textOPGetPriceNote = xmlNode.childNodes[OPTION_PLUS_TEXTS_START_INDEX + 4].childNodes[0].nodeValue;
	textOPGetPriceSold = xmlNode.childNodes[OPTION_PLUS_TEXTS_START_INDEX + 5].childNodes[0].nodeValue;
	textOPGetPriceSuccess = xmlNode.childNodes[OPTION_PLUS_TEXTS_START_INDEX + 6].childNodes[0].nodeValue;
	textOPGetPriceError = xmlNode.childNodes[OPTION_PLUS_TEXTS_START_INDEX + 7].childNodes[0].nodeValue;
	textOPOfferValid = xmlNode.childNodes[OPTION_PLUS_TEXTS_START_INDEX + 8].childNodes[0].nodeValue;
	_root.offhours.offhoursMsg.setNewTextFormat(_root.offhours.offhoursMsg.getTextFormat(1, 2));
	_root.offhours.offhoursMsg.text = xmlNode.childNodes[OPTION_PLUS_TEXTS_START_INDEX + 9].childNodes[0].nodeValue;
	_root.offhours.offhoursMsgClick.setNewTextFormat(_root.offhours.offhoursMsgClick.getTextFormat(1, 2));
	_root.offhours.offhoursMsgClick.text = xmlNode.childNodes[OPTION_PLUS_TEXTS_START_INDEX + 10].childNodes[0].nodeValue;
	textOPSlipConfNote = xmlNode.childNodes[OPTION_PLUS_TEXTS_START_INDEX + 11].childNodes[0].nodeValue;
	textOPGetQuote = xmlNode.childNodes[OPTION_PLUS_TEXTS_START_INDEX + 12].childNodes[0].nodeValue;
	textOPPleaseWait = xmlNode.childNodes[OPTION_PLUS_TEXTS_START_INDEX + 13].childNodes[0].nodeValue;
	_root.readmoreText.setNewTextFormat(_root.readmoreText.getTextFormat(1, 2));
	_root.readmoreText.text = xmlNode.childNodes[OPTION_PLUS_TEXTS_START_INDEX + 14].childNodes[0].nodeValue;
};

function loadSettingsOptionPlus(xmlNode:XMLNode):Void {
	if (!_root.shouldLoadTexts) {
		_root.captionCommission.text = _root.currencySign + _root.formatDecimalPoint(_root.OPTION_PLUS_FEE, 2);
	}
	if (loggedIn) {
		readMoreButton._x = READ_MORE_BUTTON_X;
		readmoreText._x = READ_MORE_TEXT_X;
	}
};

function positionGetPricePopup() {
//	_root.writeToLog("lstInvestments.selectedIndex: " + lstInvestments.selectedIndex +
//			" lstInvestments.vPosition: " + lstInvestments.vPosition +
//			" lstInvestments.rowCount: " + lstInvestments.rowCount);
	if (lstInvestments.selectedIndex >= lstInvestments.vPosition && lstInvestments.selectedIndex < lstInvestments.vPosition + lstInvestments.rowCount) {
		getPricePopup._y = lstInvestments._y + (lstInvestments.selectedIndex - lstInvestments.vPosition) * 40 - (getPricePopup._height - 40);
	} else {
		getPricePopup._y = -1000;
	}
	_root.listCover._x = LIST_COVER_X;
};

function getOptionPlusSellPrice():Void {
	if (submitting) {
		return;
	}
	_root.closeGetPricePopup();

	if (soldInvestmentId > 0) {
		removeInvestment(soldInvestmentId);
		soldInvestmentId = 0;
	}
	
	if (_root.chart.getInvestments()[_root.lstInvestments.selectedIndex].getMarketId().valueOf() != _root.pMarketId.valueOf()) {
		getInvestmentPriceAfterMarketSwitch = _root.lstInvestments.selectedIndex;
		_root.pMarketId = _root.chart.getInvestments()[_root.lstInvestments.selectedIndex].getMarketId();
		_root.pOpportunityId = 0;
		_root.reloadData();
		return;
	}
	
	submitting = true;
	writeToLog("GetPrice...");

	_root.getPricePopup.gotoAndStop(2);
	_root.getPricePopup.pleaseWait.pleaseWaitText.setNewTextFormat(_root.getPricePopup.pleaseWait.pleaseWaitText.getTextFormat(1, 2));
	_root.getPricePopup.pleaseWait.pleaseWaitText.text = _root.textOPPleaseWait;
	if (_root.getPriceOnTheLeft) {
		_root.getPricePopup._x = _root.lstInvestments._x - _root.getPricePopup._width - 5;
	} else {
		_root.getPricePopup._x = _root.lstInvestments._x + _root.lstInvestments._width + 5;
	}
	_root.positionGetPricePopup();
	
	var getPrice:LoadVars = new LoadVars();
	getPrice.flashGetPrice = "1";
	getPrice.investmentId = _root.chart.getInvestments()[_root.lstInvestments.selectedIndex].getId();
	getPrice.onHTTPStatus = function(httpStatus:Number) {
	    writeToLog("GetPrice HTTP status is: " + httpStatus);
	};
	getPrice.onLoad = function(success:Boolean) {
		if (success) {
			writeToLog("GetPrice Success - " + getPrice.reply);
			optionPlusSellPrice = getPrice.reply;
			var sellPriceNumber:Number = new Number(getPrice.reply);
			writeToLog("sellPriceNumber: " + sellPriceNumber);
			if (!isNaN(sellPriceNumber)) {
//				_root.getPricePopup.price.text = "";
				_root.getPricePopup.gotoAndStop(1);
				_root.getPricePopup.price.setNewTextFormat(_root.getPricePopup.price.getTextFormat(1, 2));
				_root.getPricePopup.price.text = formatAmountTxt(optionPlusSellPrice);
				_root.getPricePopup.buy.setNewTextFormat(_root.getPricePopup.buy.getTextFormat(1, 2));
				_root.getPricePopup.buy.text = _root.textOPBuy;
				_root.getPricePopup.nothanks.setNewTextFormat(_root.getPricePopup.nothanks.getTextFormat(1, 2));
				_root.getPricePopup.nothanks.text = _root.textOPNoThanks;
				_root.getPricePopup.offerValid.setNewTextFormat(_root.getPricePopup.offerValid.getTextFormat(1, 2));
				_root.getPricePopup.offerValid.text = _root.textOPOfferValid;
				_root.getPricePopup.note.setNewTextFormat(_root.getPricePopup.note.getTextFormat(1, 2));
				_root.getPricePopup.note.text = _root.textOPGetPriceNote;
				_root.getPricePopup.time.setNewTextFormat(_root.getPricePopup.time.getTextFormat(1, 2));
				_root.getPricePopup.time.text = "";
				_root.startGetPriceCountdown();
			} else {
				writeToLog("Got error from the server");
				_root.getPricePopup.gotoAndStop(4);
				_root.getPricePopup.error.setNewTextFormat(_root.getPricePopup.error.getTextFormat(1, 2));
				_root.getPricePopup.error.text = optionPlusSellPrice;
			}
		} else {
			writeToLog("GetPrice Fail");
			_root.getPricePopup.gotoAndStop(4);
			_root.getPricePopup.error.setNewTextFormat(_root.getPricePopup.error.getTextFormat(1, 2));
			_root.getPricePopup.error.text = _root.textOPGetPriceError;
		}
		submitting = false;
	}
	getPrice.sendAndLoad(requestsPath + "/jsp/system/ajax.jsf", getPrice, "POST");
}

function startGetPriceCountdown():Void {
	_root.getPricePopup.countDownCounter = 4 * 20; // 4 sec x 20 fps
	_root.getPricePopup.onEnterFrame = getPriceCountdownTicker;
};

function getPriceCountdownTicker():Void {
	_root.getPricePopup.countDownCounter -= 1;
	_root.getPricePopup.time.text = _root.chart.padToTwoDigit(Math.floor(_root.getPricePopup.countDownCounter / 20)) + "." + _root.chart.padToTwoDigit((_root.getPricePopup.countDownCounter % 20) * 5);
	var tf:TextFormat = new TextFormat();
	tf.letterSpacing = 4;
	tf.bold = true;
	_root.getPricePopup.countdown.time.setTextFormat(tf);
	if (_root.getPricePopup.countDownCounter <= 0) {
//		_root.writeToLog("getPriceCountdownTicker done");
		_root.closeGetPricePopup();
	}
};
	
function sellOption() {
	if (submitting) {
		return;
	}
	submitting = true;

	_root.getPricePopup.onEnterFrame = null;
	writeToLog("SellOption...");

	_root.getPricePopup.gotoAndStop(2);
	
	var sell:LoadVars = new LoadVars();
	sell.flashSell = "1";
	sell.investmentId = _root.chart.getInvestments()[_root.lstInvestments.selectedIndex].getId();
	sell.price = optionPlusSellPrice;
	sell.onHTTPStatus = function(httpStatus:Number) {
	    writeToLog("SellOption HTTP status is: " + httpStatus);
	};
	sell.onLoad = function(success:Boolean) {
		if (success) {
			writeToLog("SellOption Success - " + sell.reply);
			if (sell.reply == "OK") {
				soldInvestmentId = _root.chart.getInvestments()[_root.lstInvestments.selectedIndex].getId();
				_root.getPricePopup.gotoAndStop(3);
				_root.getPricePopup.price.setNewTextFormat(_root.getPricePopup.price.getTextFormat(1, 2));
				_root.getPricePopup.price.text = formatAmountTxt(optionPlusSellPrice);
				_root.getPricePopup.sold.setNewTextFormat(_root.getPricePopup.sold.getTextFormat(1, 2));
				_root.getPricePopup.sold.text = _root.textOPGetPriceSold;
				_root.getPricePopup.success.setNewTextFormat(_root.getPricePopup.success.getTextFormat(1, 2));
				_root.getPricePopup.success.text = _root.textOPGetPriceSuccess.replace("{0}", formatAmountTxt(optionPlusSellPrice));
				_root.lstInvestments.getItemAt(_root.lstInvestments.selectedIndex).sellButton._visible = false;
				_root.lstInvestments.getItemAt(_root.lstInvestments.selectedIndex).sellButton._x = -1000;
				_root.startCloseSuccessPopupCounter();
			} else {
				_root.getPricePopup.gotoAndStop(4);
				_root.getPricePopup.error.setNewTextFormat(_root.getPricePopup.error.getTextFormat(1, 2));
				_root.getPricePopup.error.text = _root.textOPGetPriceError;
			}
		} else {
			writeToLog("SellOption Fail");
			_root.getPricePopup.gotoAndStop(4);
			_root.getPricePopup.error.setNewTextFormat(_root.getPricePopup.error.getTextFormat(1, 2));
			_root.getPricePopup.error.text = _root.textOPGetPriceError;
		}
		submitting = false;
	}
	sell.sendAndLoad(requestsPath + "/jsp/system/ajax.jsf", sell, "POST");
};

function closeGetPricePopup() {
//	_root.writeToLog("closeGetPricePopup");
	if (_root.soldInvestmentId > 0) {
		_root.removeInvestment(_root.soldInvestmentId);
		_root.soldInvestmentId = 0;
		
		_root.refreshPageHeader();
	}
	_root.getPricePopup.onEnterFrame = null;
	_root.getPricePopup._x = -1000;
	
	_root.listCover._x = -1000;
};

function startCloseSuccessPopupCounter():Void {
	closeSuccessPopupCounter = 5 * 20; // 5 sec x 20pfs
	_root.getPricePopup.onEnterFrame = closeSuccessPopupTicker;
};

function closeSuccessPopupTicker():Void {
	closeSuccessPopupCounter -= 1;
	if (closeSuccessPopupCounter <= 0) {
		_root.closeGetPricePopup();
	}
};

btnOpenDemo.onPress = function() {
	getURL("javascript:newWindow = window.open('" + requestsPath + "/jsp/optionPlusHowItWorksPopup.jsf','howItWorksPopup','menubar=no,resizable=no,location=no,toolbar=no,width=" + Stage.width + ",height=" + Stage.height + "');newWindow.focus();void(0);");
};

readMoreButton.onPress = function() {
	getURL(requestsPath + "/jsp/optionPlusFeature.jsf", "_top");
};
