var server = 'local';

settings.jsonLink = '/backend/AnyoptionService/';
settings.backendJsonLink = '/backend/BackendService/';
settings.commonJsonLink = '/backend/BackendService/CommonService/';
settings.backendFileUploadLink = '/backend/UploadBEDocumentsService/';
settings.commonFileUploadLink = '/backend/UploadDocumentsService/';
settings.backendPromotionsUploadLink = '/backend/UploadBannerSliderImageService/';
settings.backendPromotionsImagesLink = '/backend/bannerSliderImages/';
settings.jsonImagegLink = '/backend/';

settings.isLive = false;
settings.logIt_type = 4;