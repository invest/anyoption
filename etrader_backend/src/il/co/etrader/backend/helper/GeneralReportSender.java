package il.co.etrader.backend.helper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Skin;

import il.co.etrader.backend.bl_managers.IssuesManager;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.mbeans.GeneralReportForm;
import il.co.etrader.backend.mbeans.RegLeadsForm;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_vos.IssueReport;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * Send file to mail for general in control role.
 * 
 * @author eyalo
 *
 */
public class GeneralReportSender extends Thread {

	private static final Logger logger = Logger.getLogger(GeneralReportSender.class);

	private final String DELEMITER = ",";

	private GeneralReportForm generalReportForm;
	private int reportType;
	private String filename;
	private WriterWrapper writer;
	private String mailRecipient;

	public GeneralReportSender(GeneralReportForm form, int reportType, String filename, WriterWrapper writer) {
		super();
		this.generalReportForm = form;
		this.reportType = reportType;
		this.filename = filename;
		this.writer = writer;
	}

	public GeneralReportSender(RegLeadsForm form, int reportType, String filename, WriterWrapper writer) {
		super();
		this.reportType = reportType;
		this.filename = filename;
		this.writer = writer;
	}


	public void run() {
		String writersOffset = writer.getUtcOffset();

		Date startDate = null;
		Date endDate = null;
		ArrayList<String> writerFilter = new ArrayList<String>();
		writerFilter.add(ConstantsBase.ALL_REP);
		long skinId = 0;
		long skinBusinessCaseId = 0;
		long sourceId = 0;
		long landingPage = 0;

		ArrayList<IssueReport> issueActionReport = null;
	
		File file;
		String htmlBuffer = null;
		Writer output = null;
		String errorStr = null;
		File attachment = null;
		mailRecipient = writer.getWriter().getEmail();
		String env = CommonUtil.getProperty(ConstantsBase.CURRENT_SYSTEM , "false");
		String filePath = filename;

		if (generalReportForm != null){
			startDate = generalReportForm.getStartDate();
			endDate = generalReportForm.getEndDate();
		}
		String writers = getArrayToString(writerFilter);

		if (!env.equals("local")){
			filePath = "/usr/share/tomcat7/files/" + filePath;
		}

		try {
			long reportSize = 0;
			if (Constants.SALES_SUPPORT_REPORT_TYPE_ISSUE_ACTION == reportType){
				issueActionReport = IssuesManager.getIssueActionsUpdatedByRepresentatives(startDate, CommonUtil.addDay(endDate), writersOffset, writers);
				reportSize = issueActionReport.size();
			}
			if (errorStr == null) {
		        //Set the filename
				boolean isHaveData = false;

				if (reportSize > 0){
					file = new File(filePath);
					//output = new BufferedWriter(new FileWriter(file));
					output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));

					output.append("User id");
					output.append(DELEMITER);
					output.append("User name");
					output.append(DELEMITER);
					output.append("Days with no activity");
					output.append(DELEMITER);
					
					output.append("\n");

			        for (IssueReport row:issueActionReport){
		        		isHaveData = true;

						output.append("TEST!!!!!");
						output.append(DELEMITER);
						output.append("TEST!!!!!");
						output.append(DELEMITER);
						output.append("TEST!!!!!");
						output.append(DELEMITER);

						output.append("\n");
		        	}
			    	output.flush();
			        output.close();
				} 

				if (!isHaveData){
					errorStr = "No data was Found.";
				}
			}

		}catch (Exception e) {
			errorStr = "There was a problem in processing your report " + e.toString();
			if (env.equals("live")){
				mailRecipient = mailRecipient + ";qa@etrader.co.il";
			}
        	logger.error(errorStr,e);
		}


        if (errorStr != null) {
        	htmlBuffer = errorStr + "<br/><br/>";
        	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        	htmlBuffer += " Parameters: <br/> " +
        				  " Start Date: " + sdf.format(startDate) + " <br/> " +
        				  " End Date: " + sdf.format(endDate) + " <br/> " +
        				  " Skin Id: " + skinId + " <br/> " +
        				  " Business Skin Id: " + skinBusinessCaseId + " <br/> " +
        				  " Source Id: " + sourceId +" <br/> " +
        				  " Landing Page Id: " + landingPage;

        }else{
            attachment = new File(filePath);
            htmlBuffer = "The report is attached to this mail";
        }

        // send email
        Hashtable<String, String> serverProperties = new Hashtable<String, String>();
		serverProperties.put("url", CommonUtil.getProperty("email.server"));
		serverProperties.put("auth", "true");
		serverProperties.put("user", CommonUtil.getProperty("email.uname"));
		serverProperties.put("pass", CommonUtil.getProperty("email.pass"));
		serverProperties.put("contenttype", "text/html; charset=UTF-8");

        Hashtable<String, String> email = new Hashtable<String, String>();
        email.put("subject", filename);
        email.put("to", mailRecipient);

        String senderEmailAddress = mailRecipient;
        if (env.equals("live")){
	        if (CommonUtil.isHebrewSkin(skinId) || skinBusinessCaseId == Skin.SKIN_BUSINESS_ETRADER){
	        	senderEmailAddress = CommonUtil.getProperty("email.from");
	        }else{
	        	senderEmailAddress = CommonUtil.getProperty("email.from.anyoption");
	        }
        }
        email.put("from", senderEmailAddress);
        email.put("body", htmlBuffer);
        File []attachments={attachment};
		CommonUtil.sendEmail(serverProperties, email, attachments);
	}
	
	/**
     * return 0 if all selected else return string of list
     * @param temp
     * @return
     */
    public String getArrayToString(List<String> temp) {
        String list = ConstantsBase.EMPTY_STRING;
        for (int i = 0; i < temp.size(); i++) {
            list += temp.get(i) + ",";
        }
        return list.substring(0,list.length()-1);
    }
}
