package il.co.etrader.backend.helper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.PopulationEntriesManager;
import il.co.etrader.backend.bl_vos.PopulationEntry;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.mbeans.TrackingForm;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class TrackingReportSender extends Thread {

	private static final Logger logger = Logger.getLogger(TrackingReportSender.class);

	private TrackingForm trackingReportForm;
	private String filename;
	private String mailRecipient;
	private WriterWrapper wr;
	private Boolean isRetentionSelected;

	public TrackingReportSender(TrackingForm form, String filename, WriterWrapper wr, Boolean isRetentionSelected) {
		super();
		this.trackingReportForm = form;
		this.filename = filename;
		this.wr = wr;
		this.isRetentionSelected = isRetentionSelected;
	}

	public void run() {
		List<PopulationEntry> report = null;

		Long userId = trackingReportForm.getUserId();
		long skinId = trackingReportForm.getSkinId();
		long writerId = wr.getWriter().getId();
		int entryType = ConstantsBase.POP_ENTRY_TYPE_TRACKING;
		Date from = trackingReportForm.getFrom();
		Date to = trackingReportForm.getTo();
		boolean isAssigned = trackingReportForm.isAssignFilter();
		String username = null ;
		String priorityId = trackingReportForm.getPriorityId();
		long retentionTeamId = trackingReportForm.getRetentionTeamId();


		long userIdVal =0;
		if (null != userId){
			userIdVal = userId.longValue();
		}
		if (username != null && username.equals(ConstantsBase.EMPTY_STRING)){
			username = null;
		}

		if (isRetentionSelected) {
			writerId = wr.getWriter().getId();
		} else {
			writerId= trackingReportForm.getWriterFilter();
		}

		File file;
		String htmlBuffer = null;
		Writer output = null;
		String errorStr = null;
		File attachment = null;
		mailRecipient = wr.getWriter().getEmail();
		String env = CommonUtil.getProperty(ConstantsBase.CURRENT_SYSTEM , "false");
		String filePath = filename;

		try{

		if (this.trackingReportForm != null){
			report = PopulationEntriesManager.getPopulationEntriesByEntryType(null,skinId, writerId,
					ConstantsBase.POP_ENTRY_TYPE_TRACKING,from,to,null,null,
					0,isAssigned, userIdVal, username, 0, priorityId, wr, 0, 0, null, 0, "0", false, false, 0, 0, 0, retentionTeamId, null, null);
		}
		} catch (Exception e) {
			logger.error("error loading db ..."  ,e);
		}

		if (!env.equals("local")){
			filePath = "/usr/share/tomcat7/files/" + filePath;
		}

		try {

			if (errorStr == null) {

				boolean isHaveData = false;

				if (report.size() > 0){

					file = new File(filePath);
					output = new BufferedWriter(new FileWriter(file));

						output.append("Entry ID");
						output.append(",");
						output.append("User ID");
						output.append(",");
						output.append("Contact ID");
						output.append(",");
						output.append("Qualification Time");
						output.append(",");
						output.append("User Name");
						output.append(",");
						output.append("Representative");
						output.append(",");
						output.append("Population Now");
						output.append(",");
						output.append("Population Tracked");
						output.append(",");
						output.append("Action Type");
						output.append(",");
						output.append("Tracking Time");

						output.append("\n");


				        for (PopulationEntry row:report){
				        		isHaveData = true;

								output.append(String.valueOf(row.getCurrEntryId()));
								output.append(",");
								output.append(String.valueOf(row.getUserId()));
								output.append(",");
								output.append(String.valueOf(row.getContactId()));
								output.append(",");
								output.append(row.getQualificationTimeTxt(wr.getUtcOffset(), " "));
								output.append(",");
								output.append(row.getPopulationUserName());
								output.append(",");
								output.append(row.getAssignWriterName());
								output.append(",");
								output.append(row.getCurrPopulationName());
								output.append(",");
								output.append(row.getOldPopulationName());
								output.append(",");
								output.append(row.getActionTypeName(new Locale(Constants.LOCALE_DEFAULT)));
								output.append(",");
								output.append(row.getEntryTypeActionTimeTxt(wr.getUtcOffset(), " "));

								output.append("\n");
				        }

				    	output.flush();
				        output.close();
				}
				if (!isHaveData){
					errorStr = "No data was Found.";
				}
			}

		}catch (Exception e) {
			errorStr = "There was a problem in processing your report " + e.toString();
			if (env.equals("live")){
				mailRecipient = mailRecipient + ";qa@etrader.co.il";
			}
        	logger.error(errorStr,e);
		}


        if (errorStr != null) {
        	htmlBuffer = errorStr + "<br/><br/>";
        	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        	htmlBuffer += " Parameters: <br/> " +
        				  " start Date: " + sdf.format(from) + " <br/> " +
        				  " end Date: " + sdf.format(to) + " <br/> " +
        				  " skin Id: " + skinId + " <br/> " +
			    		  " writerId: " + writerId  + " <br/> " +
			    		  " entryType: " + entryType + " <br/> " +
			    		  " isAssigned: " + isAssigned + " <br/> " +
			    		  " userIdVal: " + userIdVal + " <br/> " +
			    		  " username: " + username  + " <br/> " +
			    		  " priorityId: " + priorityId  + " <br/> ";
        }else{
            attachment = new File(filePath);
            htmlBuffer = "The report is attached to this mail";
        }

        // send email
        Hashtable<String, String> serverProperties = new Hashtable<String, String>();
		serverProperties.put("url", CommonUtil.getProperty("email.server"));
		serverProperties.put("auth", "true");
		serverProperties.put("user", CommonUtil.getProperty("email.uname"));
		serverProperties.put("pass", CommonUtil.getProperty("email.pass"));
		serverProperties.put("contenttype", "text/html; charset=UTF-8");

        Hashtable<String, String> email = new Hashtable<String, String>();
        email.put("subject", filename);
        email.put("to", mailRecipient);

        String senderEmailAddress = mailRecipient;
        if (env.equals("live")){
	        if (CommonUtil.isHebrewSkin(skinId)){
	        	senderEmailAddress = CommonUtil.getProperty("email.from");
	        }else{
	        	senderEmailAddress = CommonUtil.getProperty("email.from.anyoption");
	        }
        }
        email.put("from", senderEmailAddress);
        email.put("body", htmlBuffer);
        File []attachments={attachment};
		CommonUtil.sendEmail(serverProperties, email, attachments);
	}
}
