/**
 *
 */
package il.co.etrader.backend.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.util.CommonUtil;

/**
 * docs servlet
 *
 * @author Kobi
 *
 */
public class DocsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 private static final Logger log = Logger.getLogger(DocsServlet.class);

	 protected static Hashtable<String, String> mimeTypes = new Hashtable<String, String>();

	 static {
		 mimeTypes.put("gif", "image/gif");
		 mimeTypes.put("png", "image/png");
		 mimeTypes.put("jpg", "image/jpeg");
		 mimeTypes.put("bmp", "image/bmp");
		 mimeTypes.put("doc", "application/msword");
		 mimeTypes.put("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
		 mimeTypes.put("mht", "message/rfc822");
		 mimeTypes.put("msg", "application/vnd.ms-outlook");
		 mimeTypes.put("pdf", "application/pdf");
		 mimeTypes.put("ppt", "application/mspowerpoint");
		 mimeTypes.put("pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
		 mimeTypes.put("rtf", "application/rtf");
		 mimeTypes.put("tif", "image/tiff");
		 mimeTypes.put("txt", "text/plain");
		 mimeTypes.put("xls", "application/vnd.ms-excel");
		 mimeTypes.put("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		 mimeTypes.put("xps", "application/vnd.ms-xpsdocument");
		 mimeTypes.put("zip", "application/zip");

	 }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        String uri = request.getRequestURI();
        if (log.isDebugEnabled()) {
            log.debug("URI requested: " + uri);
        }
        int lastSlash = uri.lastIndexOf(CommonUtil.getProperty(Constants.FILES_SERVER_PATH));
        if (lastSlash != -1 && lastSlash < uri.length()) {
            String name = uri.substring(lastSlash + CommonUtil.getProperty(Constants.FILES_SERVER_PATH).length());    // file name
            name = URLDecoder.decode(name, "UTF-8");
            
            String isNewBe = request.getParameter("isNewBe");            
            
            if(name.contains("user_") && isNewBe == null){
            	if(!writerCanSeeDocument(request)){
            		 if (log.isDebugEnabled()) {
                         log.debug("403 for file" + name + " Writer can not see the document!");
                     }
                     response.sendError(HttpServletResponse.SC_FORBIDDEN);
                     return;
            	}
            }
             
            String fileName = new String(CommonUtil.getProperty(Constants.FILES_PATH) + name);
            File file = new File(fileName);
            FileInputStream fis = null;
            try {
            	fis = new FileInputStream(file);
            } catch(Exception e) {
            	log.debug("File not found " + name, e);
            	response.sendError(HttpServletResponse.SC_NOT_FOUND);
            	return;
            }

			long length = file.length();

			if (null == fis || length == 0) {  // file not found
                if (log.isDebugEnabled()) {
                    log.debug("404 for " + name);
                }
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                fis.close();
                return;
		    }

			 //	Create the byte array to hold the data
	         byte[] bytes = new byte[(int)length];

	         // Read in the bytes
	         int offset = 0;
	         int numRead = 0;
	         while ( (offset < bytes.length) &&
	        		 	( (numRead = fis.read(bytes, offset, bytes.length-offset)) >= 0) ) {

	            offset += numRead;
	        }

	        // Ensure all the bytes have been read in
	        if (offset < bytes.length) {
	           log.warn("Not all bytes have been readed!");
	        }

	        fis.close();

	        // set the content type
	        String fileExtention = name.substring(name.lastIndexOf(".") + 1);
	        String contentType = null;
	        try {
	        	if(null != fileExtention && null != mimeTypes.get(fileExtention)) {
	        		contentType = mimeTypes.get(fileExtention);
	        	}
	        } catch (Exception e) {
				log.warn("mime type not found! " + fileExtention);
				contentType = null;
			}

            response.setHeader("Cache-control", "max-age=60, must-revalidate");
	        if (null != contentType) {
	        	response.setContentType(contentType);
	        }
            response.setContentLength(bytes.length);

            OutputStream os = response.getOutputStream();
            os.write(bytes);
            os.flush();
            os.close();
        }
    }
    
    private static boolean writerCanSeeDocument(HttpServletRequest request){
    	boolean result = false;
    	HttpSession session = request.getSession();
  		WriterWrapper ww = (WriterWrapper) session.getAttribute(Constants.BIND_SESSION_WRITER_WRAPPER);
  		ArrayList<Integer> writerSkinsId = ww.getSkins();
  		User user = (User) session.getAttribute(Constants.BIND_SESSION_USER);
  		for (int writerSkinId :  writerSkinsId){
  			if(writerSkinId == user.getSkinId()){
  				result = true;
  			}
  		}
    	return result;
    }    
}
