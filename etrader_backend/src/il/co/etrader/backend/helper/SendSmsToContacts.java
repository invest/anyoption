package il.co.etrader.backend.helper;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.MailBoxTemplate;

import il.co.etrader.backend.bl_managers.ContactsManager;
import il.co.etrader.backend.bl_managers.MailBoxTemplatesManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_managers.PromotionsManager;
import il.co.etrader.bl_managers.SMSManagerBase;
import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.Promotion;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class SendSmsToContacts extends Thread{
	private static final Logger logger = Logger.getLogger(SendSmsToContacts.class);

	private ArrayList<Contact> contactsList;

	private MailBoxTemplate template;
	private String message;
	private HttpSession session;
	private Writer writer;
	
	public SendSmsToContacts(HttpSession session, Writer w) {
		setName("SendSmsToContacts");
		this.session = session;
		this.writer = w;
	}


	public void run(){
		try {
			Promotion p = PromotionsManager.getLastPromotion(writer);
			setParametersFromLastPromotion(p);
		
			// SEND SMS
			contactsList = ContactsManager.getContactsListByPromotionId(p.getId());
			session.setAttribute(Constants.PROMOTION_TOTAL_SENDED_TO_USERS, contactsList.size() + "_0");
			SMSManagerBase.sendSmsForContacts(contactsList, message, writer.getId(), p.getId());
			session.setAttribute(ConstantsBase.PROMOTION_TOTAL_SENDED_TO_USERS, contactsList.size() + "_100" );
			PromotionsManager.updatePromotionEntriesContactStatus(p.getId(), ConstantsBase.PROMOTION_STATUS_ID_SENDED);
			PromotionsManager.updateStatus(Constants.PROMOTION_STATUS_ID_SENDED, p.getId());
		
			sendConfirmationMailToWriter(p, Constants.SEND_SMS);

			logger.debug("Sent to " + contactsList.size() + " users");
		} catch (SQLException e) {
			logger.debug("error - can not change status", e);
			session.setAttribute(ConstantsBase.PROMOTION_TOTAL_SENDED_TO_USERS, contactsList.size() + "_ERROR" );
		} catch (Exception e) {
			logger.debug("error - can not change status", e);
			session.setAttribute(ConstantsBase.PROMOTION_TOTAL_SENDED_TO_USERS, contactsList.size() + "_ERROR" );
		}
	}
	
	void sendConfirmationMailToWriter(Promotion promotion, int sendType) {
		String email = writer.getEmail();
        String subject = "Send SMS done.";
        String body = "Promotion name: "+promotion.getPromotionName();
        body += "\r\nTemplate : "+template.getName();
        body += "\r\nSend type: "+ (sendType==1?"sms":"email");
        body += "\r\nTotal users selected: "+contactsList.size();
        
        CommonUtil.sendEmailMsg(email, email, subject, body);
	}


	public void setParametersFromLastPromotion(Promotion p) throws SQLException {
		this.message = p.getMessage();
		this.template = MailBoxTemplatesManager.getTemplateById(p.getTemplateId());
	}

	public ArrayList<Contact> getcontactsList() {
		return contactsList;
	}

	public void setcontactsList(ArrayList<Contact> contactsList) {
		this.contactsList = contactsList;
	}

}
