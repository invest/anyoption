package il.co.etrader.backend.helper;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.MailBoxTemplate;

import il.co.etrader.backend.bl_managers.MailBoxTemplatesManager;
import il.co.etrader.backend.bl_managers.MailBoxUsersManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PromotionsManager;
import il.co.etrader.bl_managers.SMSManagerBase;
import il.co.etrader.bl_vos.Promotion;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class SendSmsOrEmailToUsers extends Thread{
	private static final Logger logger = Logger.getLogger(SendSmsOrEmailToUsers.class);

	private ArrayList<UserBase> usersList;
	private int sendType;
	private MailBoxTemplate template;
	private String message;
	private HttpSession session;
	private Writer writer;
	private int issueSubjectId;
	
	public SendSmsOrEmailToUsers(HttpSession session, Writer w) {
		setName("SendSmsOrEmailToUsers");
		this.session = session;
		this.writer = w;
		this.issueSubjectId = (Integer) session.getAttribute(Constants.SELECTED_ISSUE_SUBJECT_ID);
		session.removeAttribute(Constants.SELECTED_ISSUE_SUBJECT_ID);
	}


	public void run(){
		try {
			Promotion p = PromotionsManager.getLastPromotion(writer);
			setParametersFromLastPromotion(p);
			
			if(sendType == Constants.SEND_SMS){
				// SEND SMS
				usersList =	UsersManager.getUsersListByPromotionId(p.getId(), true);
				session.setAttribute(Constants.PROMOTION_TOTAL_SENDED_TO_USERS, usersList.size() + "_0");
				SMSManagerBase.sendSms(usersList, message, session, writer.getId(), p.getId(), issueSubjectId);
				session.setAttribute(ConstantsBase.PROMOTION_TOTAL_SENDED_TO_USERS, usersList.size() + "_100" );
			} else {
				//SEND EMAIL
				usersList =	UsersManager.getUsersListByPromotionId(p.getId(), false);
		    	session.setAttribute(Constants.PROMOTION_TOTAL_SENDED_TO_USERS, usersList.size() + "_0");
				MailBoxUsersManager.insertEmail(template, p.getWriterId(), p.getId());
				session.setAttribute(Constants.PROMOTION_TOTAL_SENDED_TO_USERS, usersList.size() + "_50");
				IssuesManagerBase.createSmsOrMailIssue(usersList, session, IssuesManagerBase.ISSUE_CHANNEL_EMAIL, IssuesManagerBase.ISSUE_ACTION_EMAIL, issueSubjectId, message, writer.getId());
				session.setAttribute(ConstantsBase.PROMOTION_TOTAL_SENDED_TO_USERS, usersList.size() + "_100" );
			}
			PromotionsManager.updatePromotionEntriesStatus(p.getId(), ConstantsBase.PROMOTION_STATUS_ID_SENDED);
			PromotionsManager.updateStatus(Constants.PROMOTION_STATUS_ID_SENDED, p.getId());
		
			sendConfirmationMailToWriter(p, sendType);

			logger.debug("Sent to " + usersList.size() + " users");
		} catch (SQLException e) {
			logger.debug("error - can not change status", e);
			session.setAttribute(ConstantsBase.PROMOTION_TOTAL_SENDED_TO_USERS, usersList.size() + "_ERROR" );
		} catch (Exception e) {
			logger.debug("error - can not change status", e);
			session.setAttribute(ConstantsBase.PROMOTION_TOTAL_SENDED_TO_USERS, usersList.size() + "_ERROR" );
		}
	}
	
	void sendConfirmationMailToWriter(Promotion promotion, int sendType) {
		String email = writer.getEmail();
        String subject = "Send SMS/EMAIL done.";
        String body = "Promotion name: "+promotion.getPromotionName();
        body += "\r\nTemplate : "+template.getName();
        body += "\r\nSend type: "+ (sendType==1?"sms":"email");
        body += "\r\nTotal users selected: "+usersList.size();
        
        CommonUtil.sendEmailMsg(email, email, subject, body);
	}


	public void setParametersFromLastPromotion(Promotion p) throws SQLException {
		this.message = p.getMessage();
		this.sendType = p.getSendType();
		this.template = MailBoxTemplatesManager.getTemplateById(p.getTemplateId());
	}

	public ArrayList<UserBase> getUsersList() {
		return usersList;
	}

	public void setUsersList(ArrayList<UserBase> usersList) {
		this.usersList = usersList;
	}

}
