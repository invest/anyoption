package il.co.etrader.backend.helper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.TransactionReport;

import il.co.etrader.backend.bl_managers.IssuesManager;
import il.co.etrader.backend.bl_managers.PopulationEntriesManager;
import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_vos.PopulationEntryReport;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.mbeans.RegLeadsForm;
import il.co.etrader.backend.mbeans.SalesSupportReportForm;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_vos.IssueReport;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * Send file to mail for sales support in control role.
 * 
 * @author eyalo
 *
 */
public class SalesSupportReportSender extends Thread {

	private static final Logger logger = Logger.getLogger(SalesSupportReportSender.class);

	private final String DELEMITER = ",";

	private SalesSupportReportForm salesSupportReportForm;
	private int reportType;
	private String filename;
	private WriterWrapper writer;
	private String mailRecipient;

	public SalesSupportReportSender(SalesSupportReportForm form, int reportType, String filename, WriterWrapper writer) {
		super();
		this.salesSupportReportForm = form;
		this.reportType = reportType;
		this.filename = filename;
		this.writer = writer;
	}

	public SalesSupportReportSender(RegLeadsForm form, int reportType, String filename, WriterWrapper writer) {
		super();
		this.reportType = reportType;
		this.filename = filename;
		this.writer = writer;
	}


	public void run() {
		String writersOffset = writer.getUtcOffset();

		Date startDate = null;
		Date endDate = null;
		List<String> writerFilter = new ArrayList<String>();
		writerFilter.add(ConstantsBase.ALL_REP);
		long skinId = 0;
		long skinBusinessCaseId = 0;
		long sourceId = 0;
		long landingPage = 0;
		Locale local = new Locale(Constants.LOCALE_DEFAULT);

		ArrayList<IssueReport> issueActionReport = null;
		ArrayList<PopulationEntryReport> populationEntryReport = null;
		ArrayList<TransactionReport> transactionReport = null;

		File file;
		String htmlBuffer = null;
		Writer output = null;
		String errorStr = null;
		File attachment = null;
		mailRecipient = writer.getWriter().getEmail();
		String env = CommonUtil.getProperty(ConstantsBase.CURRENT_SYSTEM , "false");
		String filePath = filename;

		if (salesSupportReportForm != null){
			startDate = salesSupportReportForm.getStartDate();
			endDate = salesSupportReportForm.getEndDate();
			writerFilter = salesSupportReportForm.getWriterFilter();
		}
		String writers = getArrayToStringLists(writerFilter);


		if (!env.equals("local")){
			filePath = "/usr/share/tomcat7/files/" + filePath;
		}

		try {
			long reportSize = 0;
			if (Constants.SALES_SUPPORT_REPORT_TYPE_ISSUE_ACTION == reportType){
				issueActionReport = IssuesManager.getIssueActionsUpdatedByRepresentatives(startDate, CommonUtil.addDay(endDate), writersOffset, writers);
				reportSize = issueActionReport.size();
			}else if (Constants.SALES_SUPPORT_REPORT_TYPE_CONVERSION_CALL == reportType){
				populationEntryReport = PopulationEntriesManager.getConversionLeadCalled(startDate, CommonUtil.addDay(endDate), writers);
				reportSize = populationEntryReport.size();
			}else if (Constants.SALES_SUPPORT_REPORT_TYPE_TRANSACTION == reportType){
				transactionReport = TransactionsManager.getTransactionInsertedManually(startDate, CommonUtil.addDay(endDate), writers);
				reportSize = transactionReport.size();
			}
			if (errorStr == null) {
		        //Set the filename
				boolean isHaveData = false;

				if (reportSize > 0){
					file = new File(filePath);
					//output = new BufferedWriter(new FileWriter(file));
					output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF8"));

					if (Constants.SALES_SUPPORT_REPORT_TYPE_ISSUE_ACTION == reportType){

						output.append("Issue id");
						output.append(DELEMITER);
						output.append("Issue action id");
						output.append(DELEMITER);
						output.append("Date of the call (GMT)");
						output.append(DELEMITER);
						output.append("Channel");
						output.append(DELEMITER);
						output.append("Rep");
						output.append(DELEMITER);
						output.append("User id");
						output.append(DELEMITER);
						output.append("User name");
						output.append(DELEMITER);
						output.append("reached statuses");
						output.append(DELEMITER);
						output.append("Subjects");
						output.append(DELEMITER);
						output.append("issue stats");
						output.append(DELEMITER);
						output.append("Reaction/Activity");
						output.append(DELEMITER);
						output.append("comments");
						output.append(DELEMITER);
						output.append("User's mobile phone");
						output.append(DELEMITER);
						output.append("User's land line phone");
						output.append(DELEMITER);
						output.append("User's email");
						output.append(DELEMITER);
						output.append("Contact id");
						output.append(DELEMITER);
						output.append("Contact's mobile phone");
						output.append(DELEMITER);
						output.append("Contact's land line phone");
						output.append(DELEMITER);
						output.append("Contact's email");

						output.append("\n");


				        for (IssueReport row:issueActionReport){
			        		isHaveData = true;

							output.append(String.valueOf(row.getIssueId()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getActionId()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getActionTime()));
							output.append(DELEMITER);
							output.append(CommonUtil.getMessage(row.getChannel().getName(), null, local));
							output.append(DELEMITER);
							output.append(row.getWriterName());
							output.append(DELEMITER);
							output.append(String.valueOf(row.getUserId()));
							output.append(DELEMITER);
							output.append(row.getUserName() == null ? "" : row.getUserName());
							output.append(DELEMITER);
							output.append(CommonUtil.getMessage(row.getReachedStatus().getName(), null, local));
							output.append(DELEMITER);
							output.append(CommonUtil.getMessage(row.getIssueSubject(), null, local));
							output.append(DELEMITER);
							output.append(CommonUtil.getMessage(row.getIssueStats(), null, local));
							output.append(DELEMITER);
							output.append(CommonUtil.getMessage(row.getReaction().getName(), null, local));
							output.append(DELEMITER);
							output.append(row.getComments().replace("\r\n", " ").replace(DELEMITER, " "));
							output.append(DELEMITER);
							output.append(row.getUsersMobilePhone() == null ? "" : row.getUsersMobilePhone());
							output.append(DELEMITER);
							output.append(row.getUsersLandLinePhone() == null ? "" : row.getUsersLandLinePhone());
							output.append(DELEMITER);
							output.append(row.getUsersEmail() == null ? "" : row.getUsersEmail());
							output.append(DELEMITER);
							output.append(String.valueOf(row.getContactId()));
							output.append(DELEMITER);
							output.append(row.getContactsMobilePhone() == null ? "" : row.getContactsMobilePhone());
							output.append(DELEMITER);
							output.append(row.getContactLandLinePhone() == null ? "" : row.getContactLandLinePhone());
							output.append(DELEMITER);
							output.append(row.getContactEmail());
							output.append(DELEMITER);

							output.append("\n");
			        	}
				    	output.flush();
				        output.close();
					} else if (Constants.SALES_SUPPORT_REPORT_TYPE_CONVERSION_CALL == reportType){	
					
						output.append("Issue action id");
						output.append(DELEMITER);
						output.append("Date of the call (GMT)");
						output.append(DELEMITER);
						output.append("Channel");
						output.append(DELEMITER);
						output.append("Rep");
						output.append(DELEMITER);
						output.append("User name");
						output.append(DELEMITER);
						output.append("User's mobile phone");
						output.append(DELEMITER);
						output.append("User's land line phone");
						output.append(DELEMITER);
						output.append("User's email");
						output.append(DELEMITER);
						output.append("Contact id");
						output.append(DELEMITER);
						output.append("Contact's mobile phone");
						output.append(DELEMITER);
						output.append("Contact's land line phone");
						output.append(DELEMITER);
						output.append("Contact's email");
						output.append("\n");

				        for (PopulationEntryReport row:populationEntryReport){
				        		isHaveData = true;

								output.append(String.valueOf(row.getActionId()));
								output.append(DELEMITER);
								output.append(String.valueOf(row.getActionTime()));
								output.append(DELEMITER);
								output.append(CommonUtil.getMessage(row.getChannelName(), null, local));
								output.append(DELEMITER);
								output.append(row.getWriterName());
								output.append(DELEMITER);
								output.append(row.getUserName() == null ? "" : row.getUserName());
								output.append(DELEMITER);
								output.append(row.getUsersMobilePhone() == null ? "" : row.getUsersMobilePhone());
								output.append(DELEMITER);
								output.append(row.getUsersLandLinePhone() == null ? "" : row.getUsersLandLinePhone());
								output.append(DELEMITER);
								output.append(row.getUsersEmail() == null ? "" : row.getUsersEmail());
								output.append(DELEMITER);
								output.append(String.valueOf(row.getContactId()) == null ? "" : String.valueOf(row.getContactId()));
								output.append(DELEMITER);
								output.append(row.getContactsMobilePhone() == null ? "" : row.getContactsMobilePhone());
								output.append(DELEMITER);
								output.append(row.getContactLandLinePhone() == null ? "" : row.getContactLandLinePhone());
								output.append(DELEMITER);
								output.append(row.getContactEmail() == null ? "" : row.getContactEmail());
								output.append(DELEMITER);

								output.append("\n");
				        	
				        }

				    	output.flush();
				        output.close();

					}else if (Constants.SALES_SUPPORT_REPORT_TYPE_TRANSACTION == reportType){

						output.append("Transaction id");
						output.append(DELEMITER);
						output.append("Transaction date");
						output.append(DELEMITER);
						output.append("Rep");
						output.append(DELEMITER);
						output.append("Issue action id");
						output.append(DELEMITER);
						output.append("Time inserted");
						output.append(DELEMITER);
						output.append("Comment");
						output.append(DELEMITER);
						output.append("Mobile phone");
						output.append(DELEMITER);
						output.append("Land line phone");
						output.append(DELEMITER);
						output.append("\n");

				        for (TransactionReport row:transactionReport){

			        		isHaveData = true;

			        		output.append(String.valueOf(row.getTransactionId()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getTransactionDate()));
							output.append(DELEMITER);
							output.append(row.getWriterName());
							output.append(DELEMITER);
							output.append(String.valueOf(row.getIssueActionId()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getTimeInserted()));
							output.append(DELEMITER);
							output.append(row.getIssueComment().replace("\r\n", " ").replace(DELEMITER, " "));
							output.append(DELEMITER);
							output.append(row.getUsersMobilePhone() == null ? "" : row.getUsersMobilePhone());
							output.append(DELEMITER);
							output.append(row.getUsersLandLinePhone() == null ? "" : row.getUsersLandLinePhone());
							output.append(DELEMITER);
							output.append("\n");
			        	}

				    	output.flush();
				        output.close();

					}
				}

				if (!isHaveData){
					errorStr = "No data was Found.";
				}
			}

		}catch (Exception e) {
			errorStr = "There was a problem in processing your report " + e.toString();
			if (env.equals("live")){
				mailRecipient = mailRecipient + ";qa@etrader.co.il";
			}
        	logger.error(errorStr,e);
		}


        if (errorStr != null) {
        	htmlBuffer = errorStr + "<br/><br/>";
        	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        	htmlBuffer += " Parameters: <br/> " +
        				  " Start Date: " + sdf.format(startDate) + " <br/> " +
        				  " End Date: " + sdf.format(endDate) + " <br/> " +
        				  " Skin Id: " + skinId + " <br/> " +
        				  " Business Skin Id: " + skinBusinessCaseId + " <br/> " +
        				  " Source Id: " + sourceId +" <br/> " +
        				  " Landing Page Id: " + landingPage;

        }else{
            attachment = new File(filePath);
            htmlBuffer = "The report is attached to this mail";
        }

        // send email
        Hashtable<String, String> serverProperties = new Hashtable<String, String>();
		serverProperties.put("url", CommonUtil.getProperty("email.server"));
		serverProperties.put("auth", "true");
		serverProperties.put("user", CommonUtil.getProperty("email.uname"));
		serverProperties.put("pass", CommonUtil.getProperty("email.pass"));
		serverProperties.put("contenttype", "text/html; charset=UTF-8");

        Hashtable<String, String> email = new Hashtable<String, String>();
        email.put("subject", filename);
        email.put("to", mailRecipient);

        String senderEmailAddress = mailRecipient;
        if (env.equals("live")){
	        if (CommonUtil.isHebrewSkin(skinId) || skinBusinessCaseId == Skin.SKIN_BUSINESS_ETRADER){
	        	senderEmailAddress = CommonUtil.getProperty("email.from");
	        }else{
	        	senderEmailAddress = CommonUtil.getProperty("email.from.anyoption");
	        }
        }
        email.put("from", senderEmailAddress);
        email.put("body", htmlBuffer);
        File []attachments={attachment};
		CommonUtil.sendEmail(serverProperties, email, attachments);
	}
	
	/**
     * return 0 if all selected else return string of list
     * @param temp
     * @return
     */
    public String getArrayToStringLists(List<String> temp) {
        String list = ConstantsBase.EMPTY_STRING;
        for (int i = 0; i < temp.size(); i++) {
        	String rep = String.valueOf(temp.get(i));
        	if(rep.equals(ConstantsBase.ALL_REP)){
        		return ConstantsBase.ALL_REP;
        	}
            list += rep + ",";
        }
        return list.substring(0,list.length()-1);
    }
   
}
