package il.co.etrader.backend.helper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Skin;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.PopulationEntriesManager;
import il.co.etrader.backend.bl_vos.PopulationEntry;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.mbeans.PopulationsEntriesForm;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class EnteriesReportSender extends Thread {

	private static final Logger logger = Logger.getLogger(EnteriesReportSender.class);

	private PopulationsEntriesForm enteriesReportForm;
	private String filename;
	private String mailRecipient;
	private WriterWrapper wr;
	private boolean isRetention;
	private boolean isVoiceSpin;
	private ApplicationData ap;

	public EnteriesReportSender(PopulationsEntriesForm form, String filename, WriterWrapper wr, boolean isRetension, Boolean isVoiceSpin, ApplicationData ap) {
		super();
		this.enteriesReportForm = form;
		this.filename = filename;
		this.wr = wr;
		this.isRetention = isRetension;
		this.isVoiceSpin = isVoiceSpin;
		this.ap = ap;
	}

	public void run() {
		try { 
					
			List<PopulationEntry> report = null;
	
			String populationTypes = enteriesReportForm.getPopulationTypes();
			String skinIds = enteriesReportForm.getSkins();
			long businessSkin = enteriesReportForm.getBusinessCaseId();
			long languageId =  enteriesReportForm.getLanguageId();
			long assignedWriterId = enteriesReportForm.getWriterId();
			long campaignId = enteriesReportForm.getCampaignId();
			String countries = enteriesReportForm.getIncludeCountries();
			Date fromDate = enteriesReportForm.getFrom();
			Date toDate = enteriesReportForm.getTo();
			int colorType = enteriesReportForm.getRetentionStatus();
			boolean isAssigned = enteriesReportForm.isAssignFilter();
			Long userId = enteriesReportForm.getUserId();
			String userName = enteriesReportForm.getUsername();
			List<String> timeZ = enteriesReportForm.getTimeZone();
			String timeZone = enteriesReportForm.getArrayToString(timeZ);
			String priorityId = null;
			String excludeCountries = enteriesReportForm.getExcludeCountries();
			boolean hideNoAnswer = enteriesReportForm.isHideNoAnswer();
			String callsCount = enteriesReportForm.getCallsCount();
			String countriesGroup = enteriesReportForm.getCountriesGroup();
			String includeAffiliates = enteriesReportForm.getIncludeCountries();
			String excludeAffiliates = enteriesReportForm.getExcludeCountries();
			long writerFilter = enteriesReportForm.getWriterFilter();
			long populationDept = 0;
			if (isVoiceSpin) {
			    populationDept = enteriesReportForm.getPopulationDept();
			}
			long lastSalesDepRepId = enteriesReportForm.getLastSalesDepRepId();
		    Date fromLogin = enteriesReportForm.getFromLogin();
		    Date toLogin = enteriesReportForm.getToLogin();
		    long userRank = enteriesReportForm.getUserRank();
		    
		    long contactId = 0;
		    if (enteriesReportForm.getContactId() != null){
		        contactId = enteriesReportForm.getContactId();
		    }
		    boolean isNotCalledPops = enteriesReportForm.isNotCalledPops();
		    int campaignPriority = enteriesReportForm.getCampaignPriority();
		    boolean isSpecialCare = enteriesReportForm.isSpecialCare();
		    long lastLoginInXMinutes = enteriesReportForm.getLastLoginInXMinutes();
		    boolean balanceBelowMinInvestAmount = enteriesReportForm.isBalanceBelowMinInvestAmount();
		    boolean madeDepositButdidntMakeInv24HAfter = enteriesReportForm.isMadeDepositButdidntMakeInv24HAfter();
		    long userStatusId = enteriesReportForm.getUserStatusId();
		    
		    long lastCallerWriterId = enteriesReportForm.getLastCallerWriterId();
		    long lastChatWriterId = enteriesReportForm.getLastChatWriterId();
		    int issueActionTypeId = enteriesReportForm.getIssueActionTypeId();
		    long retentionTeamId = enteriesReportForm.getRetentionTeamId();
		    long everCalled = enteriesReportForm.getEverCalled();
		    long countryGroupTierId = enteriesReportForm.getCountryGroupTierId();
	
			File file;
			String htmlBuffer = null;
			Writer output = null;
			String errorStr = null;
			File attachment = null;
			mailRecipient = wr.getWriter().getEmail();
			String env = CommonUtil.getProperty(ConstantsBase.CURRENT_SYSTEM , "false");
			String filePath = filename;
	
			if (!enteriesReportForm.isAssignFilter()) {
				writerFilter = 0;
			}
	
			if (isRetention) {
				assignedWriterId = writerFilter;
			}
	
			long userIdVal = 0;
			if (null != userId){
				userIdVal = userId.longValue();
			}
	
			enteriesReportForm.updateCountriesToString();
	
			// User name and User Id number are after trim so "only spaces" case will be ignored.
			if (userName != null && userName.equals(ConstantsBase.EMPTY_STRING)){
				userName = null;
			}
	
			try {
				if (enteriesReportForm != null){
					report = PopulationEntriesManager.getPopulationEntriesAll(populationTypes, skinIds, businessSkin, languageId, assignedWriterId, campaignId, countries,
							 fromDate, toDate, colorType, isAssigned, userIdVal, userName, contactId, timeZone, priorityId,
						     excludeCountries, hideNoAnswer, callsCount, countriesGroup, isNotCalledPops, includeAffiliates, excludeAffiliates, lastSalesDepRepId, campaignPriority, wr, isSpecialCare, fromLogin , toLogin, 
						     lastLoginInXMinutes, balanceBelowMinInvestAmount, madeDepositButdidntMakeInv24HAfter, userStatusId, populationDept, userRank, lastCallerWriterId, issueActionTypeId, lastChatWriterId, ConstantsBase.ALL_FILTER_ID,
						     retentionTeamId, everCalled, "", ConstantsBase.ALL_PLATFORMS_FILTER_ID, countryGroupTierId);
				}
			} catch (Exception e) {
				logger.error("error loading db ..."  ,e);
			}
	
			if (!env.equals("local")){
				filePath = "/usr/share/tomcat7/files/" + filePath;
			}
	
			try {
	
				if (errorStr == null) {
	
					boolean isHaveData = false;
	
					if (report.size() > 0){
	
						file = new File(filePath);
						output = new BufferedWriter(new FileWriter(file));
	
						if (!isVoiceSpin) {
							output.append("Entry ID");
							output.append(",");
							output.append("User ID");
							output.append(",");
							output.append("Contact ID");
							output.append(",");
							output.append("Qualification Time");
							output.append(",");
							output.append("Population");
							output.append(",");
							output.append("Language");
							output.append(",");
							output.append("Country");
							output.append(",");
							output.append("Campaign");
							output.append(",");
							output.append("Last Caller");
							output.append(",");
							output.append("Last Call Time");
							output.append(",");
							output.append("Last Dep Rep");
							output.append(",");
							output.append("Calls");
							output.append(",");
							output.append("Phone number");
							output.append(",");
							output.append("Country");
							output.append(",");
							output.append("Brand");
							output.append("\n");
	
	
					        for (PopulationEntry row:report){
					        		isHaveData = true;
	
									output.append(String.valueOf(row.getCurrEntryId()));
									output.append(",");
									output.append(String.valueOf(row.getUserId()));
									output.append(",");
									output.append(String.valueOf(row.getContactId()));
									output.append(",");
									output.append(row.getQualificationTimeTxt(wr.getUtcOffset(), " "));
									output.append(",");
									output.append(row.getCurrPopulationName());
									output.append(",");
									output.append(row.getLanguageId() != 0 ? CommonUtil.getMessage(ApplicationData.getLanguage(row.getLanguageId()).getDisplayName(), null, new Locale(Constants.LOCALE_DEFAULT)) : "");
									output.append(",");
									output.append(row.getCountryWithOffsetDiff(wr.getUtcOffset()));
									output.append(",");
									output.append(row.getCampaignIdAndPriorty());
									output.append(",");
									output.append(row.getLastCallWriter());
									output.append(",");
									output.append(row.getLastCallTimeTxt(wr.getUtcOffset()) == null ? "" : row.getLastCallTimeTxt(wr.getUtcOffset(), " "));
									output.append(",");
									output.append(row.getLastSalesDepRep() == null ? "" : row.getLastSalesDepRep());
									output.append(",");
									output.append(String.valueOf(row.getCallsCounts()));
									output.append(",");
									output.append(getPhone(row.getPhone()));
									output.append(",");
									output.append(getCountryWithOffsetDiff(ap, wr, row.getCountryId(), row.getCountryOffset()));
									output.append(",");
									output.append(getUserBrand(ap, row.getUserBrandId()));
									output.append("\n");
					        }
						} else { // voice spin
						    output.append("Entry ID");
	                        output.append(",");
	                        output.append("Mobile Phone");
	                        output.append(",");
	                        output.append("Land Lind Phone");
	                        output.append(",");
	                        output.append("Name");
	
	                        output.append("\n");
	
	                        for (PopulationEntry row:report){
	                            isHaveData = true;
	                            output.append(String.valueOf(row.getCurrEntryId()));
	                            output.append(",");
	                            
	                            long countryId = row.getCountryId();
	                            String prefix = null;
	                            if (countryId != 0) {
	                                prefix = CommonUtil.getPrefixFromCountry(countryId);
	                            }
	                            String mobilePhone = row.getMobilePhone();
	                            if (!Utils.isParameterEmptyOrNull(mobilePhone) && !prefix.equals("972")){
	                                mobilePhone = prefix.concat(mobilePhone);
	                            }
	                            String landLindPhone = row.getLandLindPhone();
	                            if (!Utils.isParameterEmptyOrNull(landLindPhone) && !prefix.equals("972")){
	                                landLindPhone = prefix.concat(landLindPhone);
	                            }
	                            
	                            output.append(mobilePhone);
	                            output.append(",");
	                            output.append(landLindPhone);
	                            output.append(",");
	                            output.append(row.getPopulationUserName());
	                            output.append("\n");
	                        }
						}
	
					    	output.flush();
					        output.close();
					}
					if (!isHaveData){
						errorStr = "No data was Found.";
					}
				}
	
			}catch (Exception e) {
				errorStr = "There was a problem in processing your report " + e.toString();
				if (env.equals("live")){
					mailRecipient = mailRecipient + ";qa@etrader.co.il";
				}
	        	logger.error(errorStr,e);
			}
	
	
	        if (errorStr != null) {
	        	htmlBuffer = errorStr + "<br/><br/>";
	        	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	        	htmlBuffer += " Parameters: <br/> " +
						        	"Populations type: " + populationTypes +  "<br />" +
						    		"Skin ids: " + skinIds +  "<br />" +
						    		"businessSkin: " + businessSkin  +  "<br />" +
						    		"languageId: " + languageId +  "<br />" +
						    		"assignedWriterId: " + assignedWriterId +  "<br />" +
						    		"campaignId: " + campaignId+  "<br />" +
						    		"countries: " + countries +  "<br />" +
						    		"fromDate: " + sdf.format(fromDate) +  "<br />" +
						    		"toDate: " + sdf.format(toDate) +  "<br />" +
						    		"colorType: " + colorType +  "<br />" +
						    		"isAssigned: " + isAssigned +  "<br />" +
						    		"userId: " + userId +  "<br />" +
						    		"userName: " + userName +  "<br />" +
						    		"timeZone: " + timeZone +  "<br />" +
						    		"priorityId: " + priorityId +  "<br />" +
						    		"excludeCountries: " + excludeCountries +  "<br />" +
						    		"hideNoAnswer: " + hideNoAnswer +  "<br />" +
						    		"callsCount: " + callsCount +  "<br />" +
						    		"countriesGroup: " + countriesGroup +  "<br />" +
						    		"excludeAffiliates: " + excludeAffiliates +  "<br />" +
						    		"includeAffiliates: " + includeAffiliates +  "<br />";
	        }else{
	            attachment = new File(filePath);
	            htmlBuffer = "The report is attached to this mail";
	        }
	
	        // send email
	        Hashtable<String, String> serverProperties = new Hashtable<String, String>();
			serverProperties.put("url", CommonUtil.getProperty("email.server"));
			serverProperties.put("auth", "false");
			serverProperties.put("user", CommonUtil.getProperty("email.uname"));
			serverProperties.put("pass", CommonUtil.getProperty("email.pass"));
			serverProperties.put("contenttype", "text/html; charset=UTF-8");
	
	        Hashtable<String, String> email = new Hashtable<String, String>();
	        email.put("subject", filename);
	        email.put("to", mailRecipient);
	
	        String senderEmailAddress = CommonUtil.getProperty("email.from.anyoption");
	        if (env.equals("live")){
	        	// TODO debug
	        	if (skinIds != null){
			        if (/*CommonUtil.isHebrewSkin(skinId)*/ skinIds.equals(Skin.SKIN_ETRADER) || businessSkin == Skin.SKIN_BUSINESS_ETRADER){
			        	senderEmailAddress = CommonUtil.getProperty("email.from");
			        }
	        	}
	        }
	        email.put("from", senderEmailAddress);
	        email.put("body", htmlBuffer);
	        File []attachments={attachment};
			CommonUtil.sendEmail(serverProperties, email, attachments);
		
		}catch (Exception e) {
        	logger.error("When run ReportSender ",e);
		}
	}
	
	/**
	 * Get country with offset diff
	 * @param ap
	 * @param wr
	 * @param countryId
	 * @param countryOffset
	 * @return country with offset as String
	 */
	public String getCountryWithOffsetDiff(ApplicationData ap, WriterWrapper wr, long countryId, String countryOffset) {
		return getCountry(ap, countryId) + Utils.getUtcDifference(wr.getUtcOffset(), countryOffset);
	}
	
	/**
	 * Get country
	 * @param ap
	 * @param countryId
	 * @return country name as String
	 */
	public String getCountry(ApplicationData ap, long countryId) {
		if (null == ap.getCountry(countryId)) {
			return "";
		}
		return ap.getCountry(countryId).getName();
	}
	
	/**
	 * Get user brand
	 * @param ap
	 * @param userBrandId
	 * @return user brand as String
	 */
	public String getUserBrand(ApplicationData ap, long userBrandId) {
		if (userBrandId > 0){
			return ap.getPlatformsMap().get(userBrandId);
		}
		return ConstantsBase.EMPTY_STRING;
	}
	
	/**
	 * Get phone
	 * @param phone
	 * @return phone String
	 */
	public String getPhone(String phone) {
		if (!Utils.isParameterEmptyOrNull(phone)) {
			return phone;
		}
		return ConstantsBase.EMPTY_STRING;
	}
}
