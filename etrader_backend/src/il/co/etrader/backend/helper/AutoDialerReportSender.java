package il.co.etrader.backend.helper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_vos.PopulationEntry;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.mbeans.SalesEntriesForm;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * Auto Dialer Report Sender
 * @author eyalo
 * This class send to writer's mail relevant data for Auto Dialer.
 */
public class AutoDialerReportSender extends Thread {
	/* Parameters */
	private static final Logger logger = Logger.getLogger(AutoDialerReportSender.class);
	private SalesEntriesForm enteriesForm;
	private String filename;
	private String mailRecipient;
	private WriterWrapper wr;

	/**
	 * Constructor AutoDialerReportSender
	 * @param form
	 * @param filename
	 * @param wr
	 * @param isRetension
	 * @param isVoiceSpin
	 */
	public AutoDialerReportSender(SalesEntriesForm form, String filename, WriterWrapper wr) {
		super();
		this.enteriesForm = form;
		this.filename = filename;
		this.wr = wr;
	}

	/**
	 * Function run the thread.
	 */
	public void run() {
		try {
			List<PopulationEntry> report = enteriesForm.getEntriesList();
			long languageId = enteriesForm.getLanguageId();
			File file;
			String htmlBuffer = null;
			Writer output = null;
			String errorStr = null;
			File attachment = null;
			mailRecipient = wr.getWriter().getEmail();
			String env = CommonUtil.getProperty(ConstantsBase.CURRENT_SYSTEM , "false");
			String filePath = filename;
			if (!env.equals("local")){
				filePath = "/usr/share/tomcat7/files/" + filePath;
			}
			try {
				boolean isHaveData = false;
				if (report.size() > 0){
					file = new File(filePath);
					output = new BufferedWriter(new FileWriter(file));
				    output.append("Entry ID");
                    output.append(",");
                    output.append("Mobile Phone");
                    output.append(",");
                    output.append("Land Lind Phone");
                    output.append(",");
                    output.append("Name");
                    output.append("\n");
                    for (PopulationEntry row:report){
                        isHaveData = true;
                        long countryId = row.getCountryId();
                        String prefix = null;
                        if (countryId != 0) {
                            prefix = CommonUtil.getPrefixFromCountry(countryId);
                        }
                        String mobilePhone = row.getMobilePhone();
                        if (!Utils.isParameterEmptyOrNull(mobilePhone) && !prefix.equals("972")){
                            mobilePhone = prefix.concat(mobilePhone);
                        }
                        String landLindPhone = row.getLandLindPhone();
                        if (!Utils.isParameterEmptyOrNull(landLindPhone) && !prefix.equals("972")){
                            landLindPhone = prefix.concat(landLindPhone);
                        }
                        output.append(String.valueOf(row.getCurrEntryId()));
                        output.append(",");
                        output.append(mobilePhone);
                        output.append(",");
                        output.append(landLindPhone);
                        output.append(",");
                        output.append(row.getPopulationUserName());
                        output.append("\n");
                    }
			    	output.flush();
			        output.close();
				}
				if (!isHaveData){
					errorStr = "No data was Found.";
				}
			} catch (Exception e) {
				errorStr = "There was a problem in processing your report " + e.toString();
				if (env.equals("live")){
					mailRecipient = mailRecipient + ";qa@etrader.co.il";
				}
	        	logger.error(errorStr,e);
			}
	        if (errorStr != null) {
	        	htmlBuffer = errorStr + "<br/><br/>";
	        } else {
	            attachment = new File(filePath);
	            htmlBuffer = "The report is attached to this mail";
	        }
	        // send email
	        Hashtable<String, String> serverProperties = new Hashtable<String, String>();
			serverProperties.put("url", CommonUtil.getProperty("email.server"));
			serverProperties.put("auth", "false");
			serverProperties.put("user", CommonUtil.getProperty("email.uname"));
			serverProperties.put("pass", CommonUtil.getProperty("email.pass"));
			serverProperties.put("contenttype", "text/html; charset=UTF-8");
	
	        Hashtable<String, String> email = new Hashtable<String, String>();
	        email.put("subject", filename);
	        email.put("to", mailRecipient);
	        String senderEmailAddress = CommonUtil.getProperty("email.from.anyoption");
	        if (env.equals("live")){
	        	if (languageId == Constants.LANGUAGES_HEBREW) {
	        		senderEmailAddress = CommonUtil.getProperty("email.from");
	        	}
	        }
	        email.put("from", senderEmailAddress);
	        email.put("body", htmlBuffer);
	        File []attachments = {attachment};
			CommonUtil.sendEmail(serverProperties, email, attachments);
		} catch (Exception e) {
        	logger.error("When run ReportSender ",e);
		}
	}
}
