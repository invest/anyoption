package il.co.etrader.backend.util;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.commons.io.FileUtils;

import com.anyoption.backend.service.userdocumentsscreens.UserDocumentsScreenManager;

import il.co.etrader.backend.bl_managers.WritersManager;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.cms.CMSConstants;
import il.co.etrader.backend.cms.CMSConstants.fileType;
import il.co.etrader.backend.cms.CVSImpl;
import il.co.etrader.backend.cms.CacheReloader;
import il.co.etrader.backend.cms.FtpImpl;
import il.co.etrader.backend.cms.common.CommonCMSBean;
import il.co.etrader.backend.cms.common.CommonCMSBean.CmsStage;
import org.apache.log4j.Logger;

public class ETraderSessionListener implements HttpSessionListener{

	private static Logger log = Logger.getLogger(ETraderSessionListener.class);
	@Override
	public void sessionCreated(HttpSessionEvent sessionEvent) {
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent sessionEvent) {
		System.out.println("Destroying session:");
		HttpSession session = sessionEvent.getSession();
		Object  obj  = session.getAttribute(CMSConstants.COMMON_CMS_BEAN_NAME);
		if(obj!=null){
			CommonCMSBean commonCMSBean = (CommonCMSBean)obj;
			CVSImpl cvs = (CVSImpl) session.getAttribute("CVS");
			FtpImpl ftp = (FtpImpl) session.getAttribute("Ftp");
			CacheReloader cacheReloader = (CacheReloader) session.getAttribute("Jmx");
			
			if(commonCMSBean.getCurrentCmsImageStage()==CmsStage.EDIT) {
				cvs.unedit(commonCMSBean.getCurrentCvsImagePath(), commonCMSBean.getCvsUserName(), commonCMSBean.getCvsPassword());
			}
			if(commonCMSBean.getCurrentCmsPropertyStage()==CmsStage.EDIT) {
				cvs.unedit(commonCMSBean.getCurrentCvsPropertyPath(), commonCMSBean.getCvsUserName(), commonCMSBean.getCvsPassword());
			}
			if(commonCMSBean.getCurrentCmsImageStage()==CmsStage.PREVIEW ) {
				undoPreview(cvs, ftp, cacheReloader, commonCMSBean.getCmsCheckoutDir(), commonCMSBean.getCurrentCvsImagePath(), commonCMSBean.getCvsUserName(), commonCMSBean.getCvsPassword(), fileType.image);
			}
			if(commonCMSBean.getCurrentCmsPropertyStage()==CmsStage.PREVIEW ) {
				undoPreview(cvs, ftp, cacheReloader, commonCMSBean.getCmsCheckoutDir(), commonCMSBean.getCurrentCvsPropertyPath(), commonCMSBean.getCvsUserName(), commonCMSBean.getCvsPassword(), fileType.property);
			}
			
			// clean working dir
			try {
				FileUtils.deleteDirectory(new File(commonCMSBean.getCmsCheckoutDir()));
			} catch (IOException e) {
				// TODO handle exception
				e.printStackTrace();
			}
			
			try { 
			    WriterWrapper writer = (WriterWrapper) session.getAttribute("writer");
			    if (WritersManager.getWritersCMS(writer.getWriter().getId()) > 0) {
			        WritersManager.setWritersCMS(-1, writer.getWriter().getId());
                } 
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
		}
		try {
			UserDocumentsScreenManager.unlockDocumentStatusBySession(session.getId());
		} catch (Exception er) {
			log.error("When unclock userDoc", er);
		}
		System.out.println("Session Destroyed.");
	}
	
	
	private void undoPreview(CVSImpl cvs, FtpImpl ftp, CacheReloader cacheReloader, String checkoutDir, String path, String user, String pass, fileType fType) {
		// delete current file
		File currentFile = new File(checkoutDir+path);
		boolean deleteFile = currentFile.delete();
		// checkout - get current version from cvs
		cvs.checkoutFile(checkoutDir, path, user, pass);
		// publish the current version from cvs
		ftp.preview(checkoutDir, path, fType, user, pass);
		// Unedit - call cvs module to release files
		cvs.unedit(path, user, pass);
		if(fType == fileType.property) {
			// Call JMX to refresh the cache of the bundle
			cacheReloader.clearTestEnvCache("anyoption");
		}

	}
}