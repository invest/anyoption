package il.co.etrader.backend.bl_managers;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.bl_vos.PopulationEntryBase;

import il.co.etrader.backend.bl_vos.AssignWriter;
import il.co.etrader.backend.bl_vos.PopulationEntry;
import il.co.etrader.backend.bl_vos.PopulationEntryReport;
import il.co.etrader.backend.bl_vos.PopulationUsersAssignmentInfo;
import il.co.etrader.backend.bl_vos.SkinAssigns;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.dao_managers.PopulationEntriesDAO;
import il.co.etrader.backend.mbeans.SalesEntriesForm;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.PopulationEntriesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.PopulationDelay;
import il.co.etrader.dao_managers.PopulationsDAOBase;
import il.co.etrader.population.PopulationHandlersException;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.DataPage;

public class PopulationEntriesManager extends PopulationEntriesManagerBase {

	private static final Logger logger = Logger.getLogger(PopulationEntriesManager.class);
	public static final int MAX_CALLS_COUNT_CURR_POP = 100;
	public static final int MAX_CALLS_COUNT_GENERAL = 500;
	public static final String ENUM_TOTAL_REP_PORTFOLIO_ENUMERATOR = "total_rep_portfolio";
	public static final String ENUM_TOTAL_REP_PORTFOLIO_CODE = "limit_total_rep_portfolio";
	
	public static DataPage<PopulationEntry> getPopulationEntries(
			String populationTypes, Long skinId, long businessSkin, long languageId,
			long assignedWriterId, long campaignId,  String countries, Date fromDate,
			Date toDate, int colorType, boolean isAssigned, long userId, String userName, long contactId,
			int startRow, int pageSize, long supportMoreOptionsType, String timeZone, String excludeCountries,
			boolean hideNoAnswer, String callsCount, String countriesGroup, boolean isNotCalledPops,
			String includeAffiliates, String excludeAffiliates, int verifiedUsers, long lastSalesDepRepId, int campaignPriority,
			boolean isSpecialCare, Date fromLogin, Date toLogin, long lastLoginInXMinutes, boolean balanceBelowMinInvestAmount,
			boolean madeDepositButdidntMakeInv24HAfter, long populationEntryStatusId, long populationDept, long userRankId, 
			long lastCallerWriterId, boolean qualificationTimeFilter, int issueActionTypeId, long lastChatWriterId, long reachCustomerFlag,
			long retentionTeamId, long everCalled, String sortAmountColumn, String userPlatform, long countryGroupTierId) throws SQLException {
		
		return getPopulationEntries(populationTypes, skinId.toString(), businessSkin, languageId, assignedWriterId,
				campaignId, countries, fromDate, toDate, colorType, isAssigned, userId, userName, contactId,
				startRow, pageSize, supportMoreOptionsType, timeZone, excludeCountries, hideNoAnswer, callsCount,
				countriesGroup, isNotCalledPops, includeAffiliates, excludeAffiliates, verifiedUsers, lastSalesDepRepId,
				campaignPriority, isSpecialCare, fromLogin, toLogin, lastLoginInXMinutes, balanceBelowMinInvestAmount,
				madeDepositButdidntMakeInv24HAfter, populationEntryStatusId, populationDept, userRankId,
				lastCallerWriterId, qualificationTimeFilter, issueActionTypeId, lastChatWriterId, reachCustomerFlag,
				retentionTeamId, everCalled, sortAmountColumn, userPlatform, countryGroupTierId);
	}
	
	/**
	 * Returns populationEntries list
	 * @param skinId
	 * @param languageId
	 * @param startRow
	 * @param pageSize
	 * @param isNotCalledPops TODO
	 * @param lastSalesDepRepId
	 * @param campaignPriority
	 * @param isSpecialCare
	 * @param lastCallerWriterId
	 * @param sortAmountColumn 
	 * @param assignedTo - writer Id which these pop' entries are assigned to
	 * @throws SQLException
	 */
	public static DataPage<PopulationEntry> getPopulationEntries(
			String populationTypes, String skinIds, long businessSkin, long languageId,
			long assignedWriterId, long campaignId,  String countries, Date fromDate,
			Date toDate, int colorType, boolean isAssigned, long userId, String userName, long contactId,
			int startRow, int pageSize, long supportMoreOptionsType, String timeZone, String excludeCountries,
			boolean hideNoAnswer, String callsCount, String countriesGroup, boolean isNotCalledPops,
			String includeAffiliates, String excludeAffiliates, int verifiedUsers, long lastSalesDepRepId, int campaignPriority,
			boolean isSpecialCare, Date fromLogin, Date toLogin, long lastLoginInXMinutes, boolean balanceBelowMinInvestAmount,
			boolean madeDepositButdidntMakeInv24HAfter, long populationEntryStatusId, long populationDept, long userRankId,
			long lastCallerWriterId, boolean qualificationTimeFilter, int issueActionTypeId, long lastChatWriterId, long reachCustomerFlag,
			long retentionTeamId, long everCalled, String sortAmountColumn, String userPlatform, long countryGroupTierId) throws SQLException {

		FacesContext context = FacesContext.getCurrentInstance();
		WriterWrapper wr = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);

		Connection con = getSecondConnection();
		ArrayList<PopulationEntry> list = null;
		DataPage<PopulationEntry> dataPage = null;
		int modelSize;

		try {
			logger.debug("Before getPopulationEntries" );
			list = PopulationEntriesDAO.getPopulationEntries(con, populationTypes, skinIds, businessSkin, languageId,
					assignedWriterId, campaignId, countries, fromDate, toDate, colorType, isAssigned, userId,
					userName, contactId, startRow, pageSize,  supportMoreOptionsType, timeZone, null, excludeCountries, hideNoAnswer,
					callsCount, countriesGroup, isNotCalledPops, includeAffiliates, excludeAffiliates, verifiedUsers, lastSalesDepRepId,
					campaignPriority, wr, isSpecialCare, fromLogin , toLogin, lastLoginInXMinutes, balanceBelowMinInvestAmount, madeDepositButdidntMakeInv24HAfter,
					populationEntryStatusId, populationDept, userRankId, lastCallerWriterId, qualificationTimeFilter, issueActionTypeId, lastChatWriterId, reachCustomerFlag,
					retentionTeamId, everCalled, sortAmountColumn, userPlatform, countryGroupTierId);
			logger.debug("After getPopulationEntries" );
			logger.debug("Before getPopulationEntriesCount" );
			modelSize = PopulationEntriesDAO.getPopulationEntriesCount(con, populationTypes, skinIds, businessSkin, languageId,
					assignedWriterId, campaignId, countries, fromDate, toDate, colorType, isAssigned, userId,
					userName, contactId, supportMoreOptionsType, timeZone, null, excludeCountries, hideNoAnswer, callsCount,countriesGroup,
					isNotCalledPops, includeAffiliates, excludeAffiliates, verifiedUsers, lastSalesDepRepId, campaignPriority, isSpecialCare,
					fromLogin, toLogin, lastLoginInXMinutes, balanceBelowMinInvestAmount, madeDepositButdidntMakeInv24HAfter, populationEntryStatusId, populationDept,
					userRankId, lastCallerWriterId,qualificationTimeFilter, issueActionTypeId, lastChatWriterId, reachCustomerFlag,
					retentionTeamId, everCalled, sortAmountColumn, userPlatform, countryGroupTierId);
			logger.debug("After getPopulationEntriesCount" );
			dataPage = new DataPage<PopulationEntry>(modelSize, startRow, list);
		}
		finally {
			closeConnection(con);
		}
		return dataPage;
	}


	public static ArrayList<PopulationEntry> getPopulationEntriesByEntryType(
			String populationTypes, long skinId, long assignedWriterId, int entryType,
			Date fromDate, Date toDate, Date fromLogin, Date toLogin,  int actionTypeId, boolean isAssigned,
			long userId, String userName, long contactId, String priorityId, WriterWrapper wr, long lastLoginInXMinutes, long campaignId,
			String timeZone, long userStatusId, String countriesGroup, boolean balanceBelowMinInvestAmount,
			boolean madeDepositButdidntMakeInv24HAfter, int colorType, long salesType, long userRankId, long retentionTeamId, String affiliateKey, String entryTypesList) throws SQLException{
		Connection con = getSecondConnection();
		ArrayList<PopulationEntry> list = null;
		try {
			list = PopulationEntriesDAO.getPopulationEntriesByEntryType(con, skinId, assignedWriterId, entryType, fromDate,
					toDate, fromLogin, toLogin, actionTypeId, isAssigned, userId, userName, contactId, entryTypesList, priorityId, wr, lastLoginInXMinutes, campaignId,
					timeZone, userStatusId, countriesGroup, populationTypes, balanceBelowMinInvestAmount,
					madeDepositButdidntMakeInv24HAfter, colorType, salesType, userRankId, retentionTeamId, affiliateKey);
		}
		finally {
			closeConnection(con);
		}
		return list;
	}

	public static ArrayList<PopulationEntry> getPopulationEntriesAll(
			String populationTypes, Long skinId, long businessSkin, long languageId,
			long assignedWriterId, long campaignId,  String countries, Date fromDate,
			Date toDate, int colorType, boolean isAssigned, long userId, String userName,
			String timeZone, String priorityId, String excludeCountries, boolean hideNoAnswer,
			String callsCount, String countriesGroup, String includeAffiliates, String excludeAffiliates, WriterWrapper wr,
			Date fromLogin , Date toLogin, long populationDept) {
		return getPopulationEntriesAll(populationTypes, skinId, businessSkin, languageId,
				assignedWriterId, campaignId, countries, fromDate, toDate, colorType, isAssigned,
				userId, userName, timeZone, priorityId, excludeCountries, hideNoAnswer, callsCount,
				countriesGroup, includeAffiliates, excludeAffiliates, wr, fromLogin, toLogin, populationDept);
	}
	
	public static ArrayList<PopulationEntry> getPopulationEntriesAll(
			String populationTypes, String skinIds, long businessSkin, long languageId,
			long assignedWriterId, long campaignId,  String countries, Date fromDate,
			Date toDate, int colorType, boolean isAssigned, long userId, String userName, long contactId,
			String timeZone, String priorityId, String excludeCountries, boolean hideNoAnswer,
			String callsCount, String countriesGroup, boolean isNotCalledPops, String includeAffiliates, String excludeAffiliates, long lastSalesDepRepId, 
			int campaignPriority, WriterWrapper wr,  boolean isSpecialCare, Date fromLogin ,  Date toLogin, long lastLoginInXMinutes, boolean balanceBelowMinInvestAmount, boolean madeDepositButdidntMakeInv24HAfter, 
			long populationEntryStatusId, long populationDept, long userRankId, long lastCallerWriterId, int issueActionTypeId, long lastChatWriterId, long reachCustomerFlag, long retentionTeamId, long everCalled, String sortAmountColumn, String userPlatform, 
			long countryGroupTierId) throws SQLException{
		Connection con = getSecondConnection();
		ArrayList<PopulationEntry> list = null;
		try {
			list = PopulationEntriesDAO.getPopulationEntries(con, populationTypes, skinIds, businessSkin, languageId,
					assignedWriterId, campaignId, countries, fromDate, toDate, colorType, isAssigned, userId,
						userName, contactId, 0, 0, 0, timeZone, priorityId, excludeCountries, hideNoAnswer, callsCount,
						countriesGroup, isNotCalledPops, includeAffiliates, excludeAffiliates, 0, lastSalesDepRepId, 
						campaignPriority, wr, isSpecialCare, fromLogin ,toLogin, lastLoginInXMinutes, balanceBelowMinInvestAmount, madeDepositButdidntMakeInv24HAfter, populationEntryStatusId, populationDept, userRankId,
						lastCallerWriterId, true, issueActionTypeId, lastChatWriterId, reachCustomerFlag, retentionTeamId, everCalled, 
						sortAmountColumn, userPlatform, countryGroupTierId);
		}
		finally {
			closeConnection(con);
		}
		return list;
	}

	/**
	 * Collect assignments
	 * @param repId collect all assignments for specific rep,
	 * 		  in case it's null, collect all assignments for all rep's
	 * @param popEntryType  populationEntryType id
	 * @throws SQLException
	 * @throws PopulationHandlersException
	 */
	public static void collectTasks(ArrayList<Long> repList, int popEntryType, long writerId, long skinId, long retentionTeamId, long salesType) throws SQLException, PopulationHandlersException {
		Connection con = null;
		try {
			con = getConnection();
			String entriesListType = Integer.toString(popEntryType);
			if (popEntryType == ConstantsBase.POP_ENTRY_TYPE_COLLECT_ALL) {
				entriesListType = String.valueOf(ConstantsBase.POP_ENTRY_TYPE_GENERAL) + "," +
									String.valueOf(ConstantsBase.POP_ENTRY_TYPE_TRACKING) + "," +
									String.valueOf(ConstantsBase.POP_ENTRY_TYPE_CALLBACK);
			}
			if (null == repList) {  // collectAll action
				collectTask(con, writerId, 0, popEntryType, repList, entriesListType, skinId, retentionTeamId, salesType);
			} else {  // list of rep's
				for (Long repId : repList) {
					collectTask(con, writerId, repId.longValue(), popEntryType, repList, entriesListType, skinId, retentionTeamId, salesType);
				}
			}
		} catch (Exception exp) {
			logger.error("Exception in Collect action ", exp);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Collect action
	 * @param con
	 * @param writerId the action writer
	 * @param repId rep for collect
	 * @param popEntryType populdation entry type to collect
	 * @param repList rep's list to collect, if null - collect to all rep's
	 * @param entriesListType if popEntryType is 0 then send list of entry types
	 * @throws SQLException
	 * @throws PopulationHandlersException
	 */
	public static void collectTask(Connection con, long writerId, long repId, int popEntryType, ArrayList<Long> repList,
			String entriesListType, long skinId, long retentionTeamId, long salesType) throws SQLException, PopulationHandlersException {

		FacesContext context = FacesContext.getCurrentInstance();
		WriterWrapper wr = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
		
		ArrayList<PopulationEntry> entries = PopulationEntriesDAO.getEntriesForCollect(con, skinId, repId, Utils.getWriter().getSkinsToString(), salesType, entriesListType);

		logger.info("Going to collect tasks, numOfTasks: " + entries.size());

		for (PopulationEntry e : entries) {
			if (!e.isEntryLocked()) {  // collect just unlocked entries
				try {
					con.setAutoCommit(false);
					if (e.getEntryTypeId() == ConstantsBase.POP_ENTRY_TYPE_GENERAL) {
						PopulationsManagerBase.insertIntoPopulationEntriesHis(con, e.getCurrEntryId(), writerId,
								PopulationsManagerBase.POP_ENT_HIS_STATUS_CANCEL_ASSIGNE, e.getAssignWriterId(), 0);
						UsersManagerBase.insertUsersDetailsHistoryOneField(writerId, e.getUserId(), e.getUserName(), String.valueOf(e.getUserClassId()), 
		    					UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_RETENTION_REP, String.valueOf(e.getAssignWriterId()), ConstantsBase.UNASSIGN_RETENTION_REP);
					} else if (e.getEntryTypeId() == ConstantsBase.POP_ENTRY_TYPE_CALLBACK ||
								e.getEntryTypeId() == ConstantsBase.POP_ENTRY_TYPE_TRACKING) {   // need to take oldPopEntryId

						PopulationsManagerBase.insertIntoPopulationEntriesHis(con, e.getOldPopEntryId(), writerId,
								PopulationsManagerBase.POP_ENT_HIS_STATUS_CANCEL_ASSIGNE, e.getAssignWriterId(), 0);
						UsersManagerBase.insertUsersDetailsHistoryOneField(writerId, e.getUserId(), e.getUserName(), String.valueOf(e.getUserClassId()), 
		    					UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_RETENTION_REP, String.valueOf(e.getAssignWriterId()), ConstantsBase.UNASSIGN_RETENTION_REP);
					}

					PopulationsDAOBase.updateAssignWriter(con, e.getPopulationUserId(), PopulationsManagerBase.POP_PUBLIC_ASSIGN, false);
					logger.info("Collect action for popUserId: " + e.getPopulationUserId() + " is done.");
					con.commit();
				} catch (SQLException exp) {
					logger.error("Problem in Collect action! " + exp);
					try {
					    con.rollback();
					} catch (Throwable it) {
					   	logger.error("Can't rollback.", it);
					}
					throw exp;
				} finally {
					try {
						con.setAutoCommit(true);
					} catch (Exception expA) {
						logger.error("Problem backing to autoCommit! " + expA);
					}
				}
			}
			// unlock
			/*if (e.isEntryLocked()) {
				PopulationsManagerBase.unLock(con, e, writerId);
			}*/
		}
	}


	public static boolean lockEntry(PopulationEntry populationEntry, boolean isVoiceSpin) throws SQLException,PopulationHandlersException {
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage fm;
		WriterWrapper w = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
		long writerId = w.getWriter().getId();

		long populationEntryIdToLock = populationEntry.getCurrEntryId();

		if (populationEntryIdToLock == 0){
			populationEntryIdToLock = populationEntry.getOldPopEntryId();
		}

		PopulationEntryBase entryBaseForLock = PopulationsManagerBase.getPopulationEntryByEntryId(populationEntryIdToLock);
		try {
		    boolean isLocked = entryBaseForLock.isEntryLocked();
			if (!isLocked || isVoiceSpin) {
			    // Case only when it is voice spin and user locked.
                if (isLocked) {
                    unLockEntry(populationEntry);
                }
				User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
				// if user is in decline pop, disable locks for sales (except from the germans). if it's from voice spin, lock entry.
				if (!(user.isRetentionSelected() && (entryBaseForLock.getCurrPopualtionDeptId() == ConstantsBase.DEPARTMENT_SUPPORT)) || entryBaseForLock.getCurrPopualtionTypeId() == PopulationsManagerBase.POP_TYPE_DECLINE_LAST || isVoiceSpin) {
					PopulationsManagerBase.lock(entryBaseForLock, writerId);
					populationEntry.setLockHistoryId(entryBaseForLock.getLockHistoryId());
					ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_POPULATION_ENTRY, PopulationEntry.class);
					ve.getValue(context.getELContext());
					ve.setValue(context.getELContext(), populationEntry);
					return true;
				} else {
					fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("retention.entry.cant.lock.decline", null),null);
					context.addMessage(null, fm);

				}
			} else {
				fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("retention.entry.locked.do.unlock", null),null);
				context.addMessage(null, fm);

			}
		} catch (Throwable t) {
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("retention.entry.lock.failed", null),null);
			context.addMessage(null, fm);

			throw new PopulationHandlersException("Problem processing Locking! ", t);
		}
		return false;
	}


	public static void unLockEntry(PopulationEntry populationEntry) throws SQLException,PopulationHandlersException {
		FacesContext context=FacesContext.getCurrentInstance();
		FacesMessage fm;
		long lockHistoryId = populationEntry.getLockHistoryId();

		if (lockHistoryId > 0){
			PopulationEntry lockedEntry = PopulationsManager.getLockedEntryByLockHisId(lockHistoryId);

			long writerId = Utils.getWriter().getWriter().getId();

			try {
//				boolean isManager = (user.isRetentionMSelected());
				boolean res = PopulationsManagerBase.unLock(lockedEntry, writerId);
				if (res) {
					// clear populationEntry instance
				    ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_POPULATION_ENTRY, PopulationEntry.class);
				    ve.getValue(context.getELContext());
					PopulationEntry entry = new PopulationEntry();
					ve.setValue(context.getELContext(), entry);
					// clear user session instance
					ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
					User u = (User) ve.getValue(context.getELContext());
					u.initValues();
					ve.setValue(context.getELContext(), u);
					if (logger.isDebugEnabled()) {
			        	logger.debug("entry unlocked.");
			        }
				}
			} catch (Throwable t) {
				fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("retention.entry.unlock.failed", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

				throw new PopulationHandlersException("Problem processing unLocking! ", t);
			}
		}
	}

	/**
	 * Load entry to session and navigate to entrySrip page
	 * @return
	 * @throws SQLException
	 */
	public static String loadEntry(PopulationEntry populationEntry) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		PopulationEntry entry = (PopulationEntry) context.getApplication().createValueBinding(Constants.BIND_POPULATION_ENTRY).getValue(context);
		entry.loadEntry(populationEntry);
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		return user.selectPopulationEntry(populationEntry.getUserId(), populationEntry.getContactId());
	}


	/**
	 * Check if the writer is already locking an entry
	 * @param writerId the writer for input param
	 * @return true if locking
	 * @throws PopulationHandlersException
	 */
	public static PopulationEntry getWriterLockedEntry(long writerId) throws PopulationHandlersException {
		Connection con = null;
		try {
			con = getConnection();
		    return PopulationEntriesDAO.getWriterLockedEntry(con, writerId);
		} catch (Throwable t) {
			throw new PopulationHandlersException("Problem processing isLocked! ", t);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * get Master Search Users
	 * @param con db connection
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<PopulationEntry> getMasterSearchUsers(
			long userId, String username, long contactId, boolean isLockedEntriesSearch) throws SQLException{
		Connection con = null;
		try {
			con = getSecondConnection();
			return PopulationEntriesDAO.getMasterSearchUsers(con, userId, username, contactId, isLockedEntriesSearch);
		} catch (SQLException t) {
			logger.error("Problem processing getMasterSearchUsers! ", t);
			throw t;
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * assign entries action. give entries to assigned writers
	 * @param entries selected population entries list to handle
	 * @param skinsAssigns skinsAssigns list writers tree to assign
	 * @throws SQLException
	 */
	public static long giveTasks(ArrayList<PopulationEntry> entries, ArrayList<SkinAssigns> skinsAssigns) throws SQLException {
		Connection con = null;
		long assignedTasks = 0;
		int lastEntryId = 0;
		FacesContext context = FacesContext.getCurrentInstance();
		WriterWrapper wr = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
		try {
			con = getConnection();
			logger.info("Going to give tasks, numOfEntries: " + entries.size());
			con.setAutoCommit(false);
			for (int y = 0 ; y < skinsAssigns.size() && lastEntryId < entries.size() ; y++) {
				SkinAssigns sa = skinsAssigns.get(y);
				for (AssignWriter w : sa.getWriters()) {
					if (w.isAssign() && null != w.getTasks() && w.getTasks() > 0) {
						for (int i = lastEntryId ; i < (w.getTasks() + lastEntryId) && i < entries.size() ; i++) {
							PopulationEntry entry = entries.get(i);
			    			PopulationsManagerBase.insertIntoPopulationEntriesHis(con, entry.getCurrEntryId(), wr.getWriter().getId(),
			    					PopulationsManagerBase.POP_ENT_HIS_STATUS_ASSIGNED, w.getWriterId(), 0);
			        		PopulationsDAOBase.updateAssignWriter(con, entry.getPopulationUserId(), w.getWriterId(), false);
			        		logger.info("Assign action for currEntryId: " + entry.getCurrEntryId() + " is done.");
			        		assignedTasks++;
						}
						lastEntryId += w.getTasks();
					}
				}
			}
			con.commit();
		} catch (Exception exp) {
			logger.error("Exception in Assign action ", exp);
		    try {
				con.rollback();
				assignedTasks = 0;
		    } catch (Throwable it) {
		    	logger.error("Can't rollback.", it);
			}
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}
		return assignedTasks;
	}

	/**
	 * Assign action
	 * in case the entry locked, unlock done before the assign
	 * @param isSingleAssign TODO
	 * @return
	 * @throws PopulationHandlersException
	 * @throws SQLException
	 */
    public static boolean assign(PopulationEntry populationEntry, boolean isSingleAssign, long lastAssignWriterId, boolean isVoiceSpin, boolean isBulkAssign, long writerId) throws PopulationHandlersException, SQLException {
    	FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage fm;
		long populationEntryId=populationEntry.getCurrEntryId();
		int popEntryTypeId = populationEntry.getEntryTypeId();
		boolean res = false;
		String msg;

		if(populationEntry.getAssignWriterId() > 0 ) {

			if ((ConstantsBase.POP_ENTRY_TYPE_REVIEW == popEntryTypeId ||
					ConstantsBase.POP_ENTRY_TYPE_REMOVE_FROM_SALES == popEntryTypeId ||
					ConstantsBase.POP_ENTRY_TYPE_DELAY == popEntryTypeId) && !isVoiceSpin){

				msg = CommonUtil.getMessage("retention.entry.assign.error.cant.assign.for.entry.type", null);


			} else{

	    		if (!PopulationsManagerBase.isLocked(populationEntry.getPopulationUserId()) || isVoiceSpin) {

	    			//case when users are not in any population
	    			if (populationEntry.getCurrEntryId() == 0){
	    				populationEntryId = populationEntry.getOldPopEntryId();
	    			}
	    			PopulationsManagerBase.insertIntoPopulationEntriesHis(populationEntryId, writerId,
	    					PopulationsManagerBase.POP_ENT_HIS_STATUS_ASSIGNED, populationEntry.getAssignWriterId(), 0);

	    			UsersManagerBase.insertUsersDetailsHistoryOneField(writerId, populationEntry.getUserId(), populationEntry.getUserName(), String.valueOf(populationEntry.getUserClassId()), 
	    					UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_RETENTION_REP, String.valueOf(lastAssignWriterId), String.valueOf(populationEntry.getAssignWriterId()));	    			
	        		PopulationsManagerBase.updateAssignWriter(populationEntry.getPopulationUserId(), populationEntry.getAssignWriterId(), isBulkAssign);
	        		msg = CommonUtil.getMessage("retention.entry.assign.success", null);
	        		res = true;
	    		} else {
	    			msg = CommonUtil.getMessage("retention.entry.locked.do.unlock", null);
	    		}
			}
	    } else {
	    	msg = CommonUtil.getMessage("retention.entry.writer.not.selected", null);
		}

		if((isSingleAssign || !res) && !isVoiceSpin) {
			msg = "Entry " + populationEntryId + ": " + msg;
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO,msg,null);
			context.addMessage(null, fm);
		}

    	return res;
    }


    /**
	 * Cancel assign action
	 * in case the entry locked, unlock done before the cancel
     * @param isSingleCancel TODO
     * @return
     * @throws PopulationHandlersException
     * @throws SQLException
     */
    public static boolean cancelAssign(PopulationEntry populationEntry, boolean isSingleCancel, boolean isVoiceSpin) throws PopulationHandlersException, SQLException {
    	FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage fm;
		long populationEntryId=populationEntry.getCurrEntryId();
		boolean res = false;

		if (PopulationsManagerBase.isLocked(populationEntry.getCurrEntryId()) == false || isVoiceSpin) {

			//case when users are not in any population
			if (populationEntry.getCurrEntryId() == 0){
				populationEntryId = populationEntry.getOldPopEntryId();
			}

			WriterWrapper w = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
			long writerId = w.getWriter().getId();
			cancelAssign(populationEntry, writerId, populationEntryId);
			if (isSingleCancel){
	    		fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("retention.entry.cancel.assign.success", null),null);
	    		context.addMessage(null, fm);
			}
			res = true;

    	} else {
    		fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("retention.entry.locked.do.unlock", null),null);
    		context.addMessage(null, fm);

    	}

    	return res;
    }


    /**
     * Transfer from DAA population to DAA Sales population
     * @param popUserEntry
     * @param newEntryTypeId
     * @param action
     * @throws Exception
     */
    public static void transferDaaToSales (PopulationEntryBase popUserEntry, int newEntryTypeId, IssueAction action) throws Exception{

    	Connection conn = null;
        int removeStatus = 0;
        long newActionId = action.getActionId();
        long issuePopEntryId = popUserEntry.getOldPopEntryId();
        long writerId = Utils.getWriter().getWriter().getId();
        long assignedWriterId = popUserEntry.getAssignWriterId();

    	try{
    		conn = getConnection();
		    conn.setAutoCommit(false);
			popUserEntry = PopulationEntriesManager.getPopulationEntryByEntryId(issuePopEntryId);
    		removeStatus = PopulationsManagerBase.POP_ENT_HIS_STATUS_REVIEW_REMOVE_CURR_POP;
    		PopulationsManagerBase.insertIntoPopulationEntriesHis(conn,popUserEntry.getCurrEntryId(), writerId, removeStatus, assignedWriterId, newActionId);
			popUserEntry.setCurrEntryId(0);
			popUserEntry.setLockHistoryId(0);
            PopulationEntriesDAO.updatePopulationUser(conn, popUserEntry);
            conn.commit();
    	} catch (Exception e) {
    		logger.error("Exception in Update Population! ", e);
			try {
				conn.rollback();
			} catch (Throwable it) {
				logger.error("Can't rollback.", it);
			}
    	} finally {
            try {
                conn.setAutoCommit(true);
          } catch (Exception e) {
                logger.error("Can't set back to autocommit.", e);
           }
          closeConnection(conn);
		}
	}

    /**
	 * Check if user is in marketing with no disply
	 * @param userId
	 * @return marketing pop entry id
     * @throws SQLException
	 */
	public static long getUserNoDisplayMarketingPopEntryId(long userId) throws SQLException  {
		Connection con = null;
		try {
			con = getConnection();
		    return PopulationEntriesDAO.getUserNoDisplayMarketingPopEntryId(con, userId);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Update Entry IsDisplay flag
	 * @param userId
	 * @param isDisplay
	 * @throws SQLException
	 */
	public static void updateUserEntryIsDisplay(long entryId, boolean isDisplay) throws SQLException  {
		Connection con = null;
		try {
			con = getConnection();
		    PopulationEntriesDAO.updateUserEntryIsDisplay(con, entryId, isDisplay);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Return pop user delay by type
	 * @param popUserId
	 * @param delayType
	 * @throws SQLException
	 */
	public static PopulationDelay getPopUsersDelayByDelayType(long popUserId, int delayType, long actionId) {
		Connection con = null;
		PopulationDelay delay = null;

		try {
			con = getConnection();
			delay = PopulationEntriesDAO.getPopulationDelayForPopUser(con,popUserId);
		}catch (Exception e) {
			logger.error("couldn't find pop users delay for pop user id: " + popUserId, e);
		} finally {
			closeConnection(con);
		}

		if (null != delay){

			if (delay.getDelayType() == delayType){
				delay.setDelayCount(delay.getDelayCount() + 1);
			}else{
				// change delay
				delay.setDelayType(delayType);
				delay.setDelayCount(1);
			}
		}else{
			// insert a new delay
			delay = new PopulationDelay();
			delay.setPopUserId(popUserId);
			delay.setDelayType(delayType);
		}

		delay.setDelayActionId(actionId);
		return delay;
	}

	/** 
	 * check if is Assignment limit Ok and user rank is newbies
	 * @param writerId
	 * @param skinId
	 * @param userRankId
	 * @param userStatusId
	 * @return
	 * @throws SQLException
	 */
	public static PopulationUsersAssignmentInfo isAssignmentOk(long writerId, long skinId, long userRankId, long userStatusId) throws SQLException {
    	Connection con = null;
    	boolean isAssignOk = false;
    	boolean reachedTotalLimit = false;
    	try {
    	    logger.info("Trying to assign to writer id: " + writerId + " entry with skin id: " + skinId + " rank id: " + userRankId + " and status id: " + userStatusId);
    	    if ((UsersManagerBase.USER_STATUS_COMA != userStatusId && Utils.isWriterInitialStageRetention(writerId)) || (UsersManagerBase.USER_STATUS_COMA == userStatusId && Utils.isWriterComaRetention(writerId))) {
    	        con = getSecondConnection();
    	        long totalRepPortfolio = PopulationEntriesDAO.getTotalRepPortfolio(con, writerId);
    	        if (totalRepPortfolio >= Long.valueOf(CommonUtil.getEnum(ENUM_TOTAL_REP_PORTFOLIO_ENUMERATOR, ENUM_TOTAL_REP_PORTFOLIO_CODE))) {
    	        	reachedTotalLimit = true;
    	        }
                boolean assignLimitByRankAndStatus = true;
                if (!reachedTotalLimit && UsersManagerBase.USER_STATUS_COMA == userStatusId) {
                	assignLimitByRankAndStatus = PopulationEntriesDAO.checkAssignLimitByRankAndStatus(con, writerId, skinId, userRankId, userStatusId);	
                }                
                if (!reachedTotalLimit && assignLimitByRankAndStatus) {
                	isAssignOk = true;
                }
    	    }
	        PopulationUsersAssignmentInfo puai = new PopulationUsersAssignmentInfo();
	        puai.setAssignmentOk(isAssignOk);
	        puai.setReachedTotalLimit(reachedTotalLimit);
	        return puai;
    	} finally {
    		closeConnection(con);
    	}
	}


	public static boolean reachedTotalEntries(long writerId, long skinId) throws SQLException {
		Connection con = null;
    	try {
    		con = getConnection();
    		return PopulationEntriesDAO.reachedTotalEntries(con, writerId, skinId);
    	} finally {
    		closeConnection(con);
    	}
	}

	/**
	 * The function return all conversion leads that were called (reached or not reached)
	 *
	 * @param writerId
	 * @return ArrayList<PopulationEntryReport>
	 */
	public static ArrayList<PopulationEntryReport> getConversionLeadCalled(Date startDate, Date endDate, String writerFilter) {
		ArrayList<PopulationEntryReport> vo = new ArrayList<PopulationEntryReport>();
		Connection con = null;
		try {
			con = getConnection();
			return PopulationEntriesDAO.getConversionLeadCalled(con, startDate, endDate, writerFilter);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeConnection(con);
		}
		return vo;
	}
	
	/**
	 * Check if count success deposit ok
	 * @param userId
	 * @param userStatusId
	 * @return true if count success deposit ok
	 * @throws SQLException
	 */
	public static boolean isCountSuccessDepositOk(long userId, long userStatusId) throws SQLException  {
		Connection con = null;
		try {
			con = getConnection();
			if (UsersManagerBase.USER_STATUS_COMA == userStatusId || UsersManagerBase.USER_STATUS_SLEEPER == userStatusId) {
				if (PopulationEntriesDAO.getCountSuccessDepositByUser(con, userId) == ConstantsBase.COUNT_ONE_DEPOSIT) {
					return true;
				}
			} else {
				return true;
			}
		} finally {
			closeConnection(con);
		}
		return false;
	}
	
	/**
	 * Get entries conversion.
	 * @param entriesForm
	 * @return ArrayList<PopulationEntry>
	 * @throws SQLException
	 */
	public static ArrayList<PopulationEntry> getEntriesConversion(SalesEntriesForm entriesForm) throws SQLException {
		Connection con = null;
		try {
			con = getSecondConnection();
			return PopulationEntriesDAO.getEntriesConversion(con, entriesForm);
		} finally {
			closeConnection(con);
		}
		
	}
	
	public static ArrayList<PopulationEntry> getPopulationEntriesForReassign(long writerId, long salesType, String entryTypesList, long userRankId, String userStatus) throws SQLException {
		Connection con = null;
		try {
			con = getSecondConnection();
		    return PopulationEntriesDAO.getPopulationEntriesForReassign(con, writerId, salesType, entryTypesList, userRankId, userStatus);
		} catch (Throwable t) {
			throw new SQLException("Problem get population entries for reassign! ", t);
		} finally {
			closeConnection(con);
		}
	}
}