package il.co.etrader.backend.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_vos.LoginProductBean;
import il.co.etrader.backend.dao_managers.LoginProductDAO;

/**
 * @author kiril.mutafchiev
 */
public class LoginProductManager extends il.co.etrader.bl_managers.LoginProductManager {

	private static final Logger log = Logger.getLogger(LoginProductManager.class);
	private static Map<LoginProductEnum, String> loginProductTypes;

	/**
	 * Returns a map containing all of the available product types that can be set to load on login from web
	 * 
	 * @return a map with the available product types
	 */
	public static Map<LoginProductEnum, String> getLoginProductTypes() {
		if (loginProductTypes == null) {
			loginProductTypes = new HashMap<>();
			StringBuilder builder = new StringBuilder();
			for (LoginProductEnum product : LoginProductEnum.values()) {
				for (String part : product.name().split("_")) {
					builder.append(part.substring(0, 1)).append(part.substring(1, part.length()).toLowerCase()).append(" ");
				}
				loginProductTypes.put(product, builder.toString().trim());
				builder.setLength(0);
			}
		}
		return loginProductTypes;
	}

	/**
	 * Returns a map containing skin id as key and {@link LoginProductBean} as value, that holds data for the login product.
	 * 
	 * @return a map with login product data for every skin
	 * @see LoginProductBean
	 */
	public static Map<Long, LoginProductBean> getLoginProductMap() {
		Connection con = null;
		try {
			con = getConnection();
			return LoginProductDAO.getLoginProductMap(con);
		} catch (SQLException e) {
			log.debug("Unable to load login product map", e);
			return null;
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Updates the login product for the given skins. The {@code enabled} parameter determines whether or not the login product rule should
	 * be applied. The method returns {@code true} if the update was successful.
	 * 
	 * @param skinIds a list with the ids of the skins to be updated
	 * @param product the login product that will be set for the given skins
	 * @param enabled {@code true} if the login product rule should be applied
	 * @param writerId the id of the writer that is executing the update
	 * @return {@code true} if the update was successful, {@code false} otherwise
	 */
	public static boolean updateLoginProducts(List<Long> skinIds, LoginProductEnum product, boolean enabled, long writerId) {
		Connection con = null;
		try {
			con = getConnection();
			return LoginProductDAO.updateLoginProducts(con, skinIds, product, enabled, writerId);
		} catch (SQLException e) {
			log.debug("Unable to update login products", e);
			return false;
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Returns automatic login product that will be calculated if there is no manual login product configured.
	 * 
	 * @return the {@code String} with the calculation of the automatic login product
	 */
	public static String getAutoLoginProduct() {
		StringBuilder builder = new StringBuilder();
		Locale locale = new Locale("en");
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_WEEK, AUTO_WEEKEND_START_DAY_OF_WEEK);
		String startName = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT_FORMAT, locale);
		cal.set(Calendar.DAY_OF_WEEK, AUTO_WEEKEND_END_DAY_OF_WEEK);
		String endName = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT_FORMAT, locale);
		builder	.append("If a manual rule is not enabled, automatic rule is calculated ").append(startName).append(" ")
				.append(AUTO_WEEKEND_START_HOUR).append(":").append(AUTO_WEEKEND_START_MINUTE).append(" to ").append(endName).append(" ")
				.append(AUTO_WEEKEND_END_HOUR).append(":").append(AUTO_WEEKEND_END_MINUTE).append(" - ")
				.append(loginProductTypes.get(AUTO_WEEKEND_PRODUCT)).append(", otherwise - ")
				.append(loginProductTypes.get(AUTO_NOT_WEEKEND_PRODUCT)).append(". All times are in ").append(LOGIN_PRODUCT_TIME_ZONE)
				.append(" time zone.");
		return builder.toString();
	}
}