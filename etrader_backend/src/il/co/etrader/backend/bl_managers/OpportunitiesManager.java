package il.co.etrader.backend.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Exchange;
import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.backend.dao_managers.OpportunitiesDAO;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;

public class OpportunitiesManager extends BaseBLManager {

    private static final Logger logger = Logger.getLogger(OpportunitiesManager.class);

    /**
     * Gets if given opportunity is suspended
     * 
     * @param oppID Opportunity id
     * @return <code>1 || 0 || 2</code> if suspended or not or something wrong
     * @throws SQLException
     */
    public static int isSuspendedOpp(long oppID) throws SQLException {
	Connection conn = getConnection();
	try {
	    return OpportunitiesDAO.getOppIsSuspended(conn, oppID);
	} finally {
	    closeConnection(conn);
	}
    }

    /**
     * Sends to DB opportunity to be changed suspended status
     * 
     * @param oppID		opportunity ID
     * @param isSuspended 	the 1 or 0 flag set
     * @throws SQLException
     */
    public static void setOppSuspDB(long oppID, int isSuspended) throws SQLException {
	Connection conn = getConnection();
	FacesContext context = FacesContext.getCurrentInstance();
	FacesMessage fm = null;
	try {
	    OpportunitiesDAO.setOpportunitySuspended(conn, oppID, isSuspended);
	    if (isSuspended == 1) {
		fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
			CommonUtil.getMessage("opportunities.suspended.success", null, Utils.getWriterLocale(context)),
			null);
		context.addMessage(null, fm);
	    } else if (isSuspended == 0) {
		fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
			CommonUtil.getMessage("opportunities.suspended.reverse", null, Utils.getWriterLocale(context)),
			null);
		context.addMessage(null, fm);
	    }
	} finally {
	    fm = null;
	    closeConnection(conn);
	}
    }
    
	public static HashMap<Long, Exchange> getExchanges() throws SQLException {
		HashMap<Long, Exchange> exchanges = new HashMap<Long, Exchange>();
		Connection conn = getConnection();
		try {
			exchanges = OpportunitiesDAO.getExchanges(conn);
		} finally {
			closeConnection(conn);
		}
		return exchanges;
	}
}