package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.bl_vos.OpportunityQuotes0100;
import il.co.etrader.backend.dao_managers.OpportunityQuotes0100DAO;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import com.anyoption.common.managers.BaseBLManager;

public class OpportunityQuotes0100Manager extends BaseBLManager {
	public static void insertOpportunityQuotes0100(OpportunityQuotes0100 vo) throws SQLException {
		Connection con = getConnection();
		try {
			OpportunityQuotes0100DAO.insertOpportunityQuotes0100(con, vo);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList<OpportunityQuotes0100> updateList(Date from, long marketId, long oppId) throws Exception {
		Connection con = getConnection();
		ArrayList<OpportunityQuotes0100> list = null;
		try {
			list = OpportunityQuotes0100DAO.updateList(con, from, marketId, oppId);
		} finally {
			closeConnection(con);
		}
		return list;
	}
}