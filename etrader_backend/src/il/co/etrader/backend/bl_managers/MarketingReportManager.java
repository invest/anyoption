package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.bl_vos.RegLeadsDetails;
import il.co.etrader.backend.bl_vos.RegLeadsTotals;
import il.co.etrader.backend.bl_vos.RemarketingReport;
import il.co.etrader.backend.dao_managers.MarketingReportDAO;
import il.co.etrader.bl_vos.MarketingReport;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import com.anyoption.common.managers.BaseBLManager;

public class MarketingReportManager extends BaseBLManager {

    public static ArrayList<MarketingReport> getAll(long skinId, long skinBusinessCaseId,
				Date startDate, Date endDate, String offset,long sourceId, long landingPage, long marketingCampaignManager, long writerIdForSkin) throws SQLException {
		Connection con = getSecondConnection();
		try {
			return MarketingReportDAO.getAll(con, skinId, skinBusinessCaseId, startDate, endDate, offset,sourceId, landingPage, marketingCampaignManager, writerIdForSkin);
		} finally {
			closeConnection(con);
		}
	}
    
    public static ArrayList<MarketingReport> getBaseReportByDate(long skinId, long skinBusinessCaseId, 
                                                         Date startDate, Date endDate, String offset, int reportType, long sourceId,long marketingCampaignManager, long writerIdForSkin)
            throws SQLException {
        Connection con = getSecondConnection();
        try {
            return MarketingReportDAO.getBaseReportByDate(con, skinId,skinBusinessCaseId, startDate, endDate, offset, reportType, sourceId, marketingCampaignManager, writerIdForSkin);
        } finally {
            closeConnection(con);
        }
    }
    

    public static ArrayList<MarketingReport> getHouseWinReport(long skinId, long skinBusinessCaseId,
			Date startDate, Date endDate, String offset,long sourceId, long landingPage) throws SQLException {
		Connection con = getConnection();
		try {
			return MarketingReportDAO.getHouseWinReport(con, skinId, skinBusinessCaseId, startDate, endDate, offset,sourceId, landingPage);
		} finally {
			closeConnection(con);
		}
    }

	public static ArrayList<MarketingReport> getGoogleReport(long skinId, long skinBusinessCaseId,
			  											Date startDate, Date endDate, String offset, int reportType, long sourceId, long marketingCampaignManager, long writerIdForSkin) throws SQLException {
		Connection con = getSecondConnection();
		try {
			return MarketingReportDAO.getGoogleReport(con, skinId, skinBusinessCaseId, startDate, endDate, offset,reportType,sourceId, marketingCampaignManager, writerIdForSkin);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList<RegLeadsTotals> getRegLeadsTotalReport(long skinId, long skinBusinessCaseId,
				Date startDate, Date endDate, long campaignId, long combId, long writerIdForSkin) throws SQLException {
		Connection con = getConnection();
		try {
			return MarketingReportDAO.getRegLeadsTotals(con, skinId, skinBusinessCaseId, startDate, endDate, campaignId, combId, writerIdForSkin);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList<RegLeadsDetails> getRegLeadsDetails(long skinId, long skinBusinessCaseId,
			Date startDate, Date endDate, long campaignId, long combId, int regLeadsType, boolean isForReport) throws SQLException {

		Connection con = getConnection();
		try {
			return MarketingReportDAO.getRegLeadsDetails(con, skinId, skinBusinessCaseId, startDate, endDate, campaignId, combId,regLeadsType, isForReport);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList<RemarketingReport> getRemarketingReportDetails(Date startDate, Date endDate, long skinId,long skinBusinessCaseId,long sourceId, long writerIdForSkin, long marketingCampaignManager) throws SQLException{
		Connection con = getConnection();
		try {
			return MarketingReportDAO.getRemarketingReportDetails(con,startDate,endDate,skinId,skinBusinessCaseId,sourceId, writerIdForSkin, marketingCampaignManager);		
		}finally {
			closeConnection(con);
		}
		
	}
	
	public static ArrayList<MarketingReport> getMarketingHttpRefererReport(Date startDate, Date endDate, long skinId, long sourceId, long marketingCampaignManager, long writerIdForSkin) throws SQLException{
		Connection con = getSecondConnection();
		try {
			return MarketingReportDAO.getMarketingHttpRefererReport(con, startDate, endDate, skinId, sourceId, marketingCampaignManager, writerIdForSkin);		
		}finally {
			closeConnection(con);
		}
	}

}
