package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.bl_vos.CopyopCoins;
import il.co.etrader.backend.dao_managers.CopyopCoinsBalanceHistoryDAO;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.copyop.common.enums.CoinsActionTypeEnum;
import com.copyop.common.managers.CoinsManager;
import com.copyop.common.managers.ProfileCountersManager;
import com.copyop.common.managers.ProfileManager;
import com.datastax.driver.core.Session;

/**
 * @author eranl
 *
 */
public class CopyopCoinsManager extends CoinsManager {

	private static final Logger log = Logger.getLogger(CopyopCoinsManager.class);
	
	/**
	 * Get coins balance history by user
	 * @param userId
	 * @param coinsStateId
	 * @param from
	 * @param to
	 * @return
	 */
	public static ArrayList<CopyopCoins> getCoinsBalanceHistoryByUser(long userId, int coinsStateId, Date from, Date to) {
		Session session = getSession(); 
		return CopyopCoinsBalanceHistoryDAO.getCoinsBalanceHistoryByUser(session, userId, coinsStateId, from, to);
	}
	
	/**
	 * Sum coins by action type
	 * @param userId
	 * @param actionTypeId
	 * @return
	 */
	public static long sumCoinsByActionType(long userId, int actionTypeId) {
		Session session = getSession();
		return CopyopCoinsBalanceHistoryDAO.sumCoinsAmountByType(session, userId, actionTypeId);
	}
	
	/**
	 * Sum coins with 'converted' status
	 * @param userId
	 * @return
	 */
	public static long sumConvertedCoins(long userId) {
		Session session = getSession();
		long converted = Math.abs(CopyopCoinsBalanceHistoryDAO.sumCoinsAmountByType(session, userId, CoinsActionTypeEnum.CONVERT.getId()));
		long regranted = Math.abs(CopyopCoinsBalanceHistoryDAO.sumMissedCoinsRegrant(session, userId)); 
		return converted + regranted; 
	}
	
	/**
	 * Sum coins with 'missed' status
	 * @param userId
	 * @return
	 */
	public static long sumMissedCoins(long userId) {
		Session session = getSession();
		return CopyopCoinsBalanceHistoryDAO.sumMissedCoinsWithoutRegrant(session, userId); 
	}
	
	/**
	 * Re-Grant coins to user 
	 * @param coins
	 * @param userId
	 * @param coinsBalanceHistoryId
	 * @param writerId
	 * @return
	 */
	public static boolean updateBalanceHistoryRegrant(long coins, long userId, UUID coinsBalanceHistoryId, int writerId) {		
		ProfileCountersManager.increaseProfileCoinsBalance(userId, coins);
		ProfileManager.updateTimeResetCoinsNow(userId);
		Session session = getSession();
		return CopyopCoinsBalanceHistoryDAO.updateBalanceHistoryRegrant(session, userId, coinsBalanceHistoryId, writerId);
	}

}