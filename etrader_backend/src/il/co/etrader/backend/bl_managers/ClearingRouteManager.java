package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.bl_vos.ClearingRoute;
import il.co.etrader.backend.dao_managers.ClearingRouteDAO;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.managers.BaseBLManager;

/**
 * @author liors
 *
 */
public class ClearingRouteManager extends BaseBLManager {
    	
	/**
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<ClearingRoute> getClearingRoutes() throws SQLException {
		Connection conn = getConnection();
		try{
			return ClearingRouteDAO.getClearingRoutes(conn);
		} finally {
			closeConnection(conn);
		}
	}

	/**
	 * @param clearingRoute
	 * @return
	 * @throws SQLException
	 */
	public static boolean insertClearingRoute(ClearingRoute clearingRoute) throws SQLException {
		Connection connection = getConnection();
		try{
			return ClearingRouteDAO.insertClearingRoute(connection, clearingRoute);
		} finally {
			closeConnection(connection);
		}
	}

	public static boolean updateClearingRoute(ClearingRoute clearingRoute) throws SQLException {
		Connection connection = getConnection();
		try{
			return ClearingRouteDAO.updateClearingRoute(connection, clearingRoute);
		} finally {
			closeConnection(connection);
		}
	}
}