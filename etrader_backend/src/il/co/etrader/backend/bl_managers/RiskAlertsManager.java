package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.dao_managers.RiskAlertsDAO;
import il.co.etrader.bl_managers.RiskAlertsManagerBase;
import il.co.etrader.bl_vos.RiskAlert;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

public class RiskAlertsManager extends RiskAlertsManagerBase{

	private static final Logger logger = Logger.getLogger(RiskAlertsManagerBase.class);

	/**
	 * Get Risk alerts list
	 * @param from
	 * @param to
	 * @param timeType
	 * @param userName
	 * @param skinId
	 * @param classId
	 * @param riskAlertsTypeDescription
	 * @param succeededTrx
	 * @param transactionTypeId
	 * @return
	 */
	public static ArrayList<RiskAlert> getRiskAlertsList(Date from, Date to, String timeType, String userName,
			ArrayList<Integer> skinIds, long classId, ArrayList<Long> riskAlertsTypeDescriptions, long succeededTrx, int transactionTypeId, int statusTypeId){
	    Connection con = null;
	    try {
	        con = getSecondConnection();
	        return RiskAlertsDAO.getRiskAlertList(con, from, to, timeType, userName, skinIds, classId,
	        		riskAlertsTypeDescriptions, succeededTrx, transactionTypeId, statusTypeId);
	    } catch (Exception e) {
	    	logger.error("Error get risk alert list!" + e.getMessage() + e.getStackTrace());
	    } finally {
	        closeConnection(con);
	    }
	return null;
	}
	
	/**
	 * Set risk alert as reviewed
	 * @param riskAlertId
	 * @return
	 */
	public static void updateReviewedRiskAlert(long riskAlertId) {
		Connection con = null;
		try {
			con = getConnection();
			RiskAlertsDAO.updateReviewedRiskAlert(con, riskAlertId);
		} catch (Exception e) {
			logger.error("Error updating risk alert as reviewed!" + e.getMessage() + e.getStackTrace());
		} finally {
			closeConnection(con);
		}
	}
}