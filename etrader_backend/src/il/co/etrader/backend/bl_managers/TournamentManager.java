/**
 * 
 */
package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.dao_managers.TournamentDAO;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Tournament;
import com.anyoption.common.beans.TournamentBannerFile;
import com.anyoption.common.beans.TournamentsBanners;
import com.anyoption.common.managers.TournamentManagerBase;

/**
 * @author pavelt
 *
 */
public class TournamentManager extends TournamentManagerBase {
	private static final Logger logger = Logger.getLogger(TournamentManager.class);

	public static boolean insertUpdateTournament(Tournament tournament) {

		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			if (tournament.getId() == 0) {
				 TournamentDAO.insertTournament(conn, tournament);
				 TournamentDAO.insertTournamentSkinLang(conn, tournament);
				 conn.commit();
				return true;
			} else {
				TournamentDAO.updateTournament(conn, tournament);
				TournamentDAO.updateTournamentSkinLang(conn, tournament);
				conn.commit();
				return true;
			}
			
		}catch(Exception e){
    		logger.error("Exception in insert/update tournament due to:  ", e);
			try {
				conn.rollback();
			} catch (Throwable it) {
				logger.error("Can't rollback.", it);
			}
			return false;
		}finally {
			closeConnection(conn);
		}
	}
	
	public static boolean updateTournamentIsActive(Tournament tournament) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return TournamentDAO.updateTournamentIsActive(conn, tournament);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static void insertUpdateTournamentsBanners(TournamentsBanners tournamentsBanners) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			ArrayList<TournamentBannerFile> files = tournamentsBanners.getFiles();
			if (files.get(0).getId() == 0) {// check if any of the files has ID
											// which is not 0
				TournamentDAO.insertTournamentsBanners(conn, tournamentsBanners);
			} else {
				TournamentDAO.updateTournamentsBanners(conn, tournamentsBanners);
			}
		} finally {
			closeConnection(conn);
		}
	}
}
