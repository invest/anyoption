package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.bl_vos.ReutersQuotesBE;
import il.co.etrader.backend.dao_managers.ReutersQuotesBeDAO;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;

public class ReutersQuotesManager extends BaseBLManager {
	//TODO: extends from ReutersQuotesManagerBase
	private static final Logger logger = Logger.getLogger(ReutersQuotesManager.class);

	public static ArrayList<ReutersQuotesBE> getAllData(long marketId, long scheduledId, Date from, Date to) throws SQLException {
	  	Connection con = getConnection();
	  	try {
	  		return ReutersQuotesBeDAO.getAllData(con, marketId, scheduledId, from, to);
	  	} finally {
	  		closeConnection(con);
	  	}
	}
}