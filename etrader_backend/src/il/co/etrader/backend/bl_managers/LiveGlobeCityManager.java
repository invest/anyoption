package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.dao_managers.LiveGlobeCityDAO;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.LiveGlobeCityManagerBase;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.SQLException;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.LiveGlobeCity;

public class LiveGlobeCityManager extends LiveGlobeCityManagerBase {
	
	private static final Logger logger = Logger.getLogger(LiveGlobeCityManager.class);
	
	public static boolean updateInsertLiveGlobeCity( LiveGlobeCity lc) throws SQLException {
		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Insert/Update  LiveGlobeCity: " + ls + lc.toString());
		}
		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();
		try {

			if (lc.getId() == 0) {
				// insert
				LiveGlobeCityDAO.insertLiveGlobeCity(con, lc) ;
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.insert.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

			} else {
				// update
				LiveGlobeCityDAO.updateLiveGlobeCity(con, lc) ;
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);
			}

			if (logger.isEnabledFor(Level.DEBUG)) {
				String ls = System.getProperty("line.separator");
				logger.log(Level.DEBUG, "Insert/Update  LiveGlobeCity: finished ok. " + ls);
			}

		} finally {
			closeConnection(con);
		}
		return true;
	}

}
