package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.dao_managers.ConstantInvestmentsStreamDAO;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.ConstantInvestmentsStreamManagerBase;
import il.co.etrader.bl_vos.ConstantInvestmentsStream;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.SQLException;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class ConstantInvestmentsStreamManager extends ConstantInvestmentsStreamManagerBase{
	private static final Logger logger = Logger.getLogger(ConstantInvestmentsStreamManager.class);
	
	public static boolean updateInsertLiveGlobeCity( ConstantInvestmentsStream lc) throws SQLException {
		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Insert/Update  ConstantInvestmentsStream: " + ls + lc.toString());
		}
		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();
		try {

			if (lc.getId() == 0) {
				// insert
				ConstantInvestmentsStreamDAO.insertConstantInvestmentsStream(con, lc) ;
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.insert.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

			} else {
				// update
				ConstantInvestmentsStreamDAO.updateConstantInvestmentsStream(con, lc) ;
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

			}

			if (logger.isEnabledFor(Level.DEBUG)) {
				String ls = System.getProperty("line.separator");
				logger.log(Level.DEBUG, "Insert/Update  ConstantInvestmentsStream: finished ok. " + ls);
			}

		} finally {
			closeConnection(con);
		}
		return true;
	}
	
	public static Boolean isExistStreamName(ConstantInvestmentsStream lc) throws SQLException {
	    Connection con = getConnection();
		try {
			return ConstantInvestmentsStreamDAO.isExistStreamName(con, lc);
		} finally {
			closeConnection(con);
		}
	}
}
