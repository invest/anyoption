/**
 * 
 */
package il.co.etrader.backend.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.DeeplinkParamsToUrl;
import com.anyoption.common.managers.DeeplinkParamsToUrlManagerBase;

import il.co.etrader.backend.dao_managers.DeeplinkParamsToUrlDAO;

/**
 * @author Eyal Goren
 *
 */
public class DeeplinkParamsToUrlManager extends DeeplinkParamsToUrlManagerBase {
	private static final Logger logger = Logger.getLogger(DeeplinkParamsToUrlManager.class);

	public static ArrayList<DeeplinkParamsToUrl> getAllArrayList() throws SQLException {
		return new ArrayList<DeeplinkParamsToUrl>(getAll().values());
	}
	
	public static void insertUpdate(DeeplinkParamsToUrl deeplinkParamsToUrl) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			if (deeplinkParamsToUrl.getId() == 0) {
				DeeplinkParamsToUrlDAO.insert(conn, deeplinkParamsToUrl);
			} else {
				DeeplinkParamsToUrlDAO.update(conn, deeplinkParamsToUrl);
			}
		} finally {
			closeConnection(conn);
		}
	}
}
